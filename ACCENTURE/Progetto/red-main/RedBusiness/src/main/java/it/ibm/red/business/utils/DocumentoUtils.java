package it.ibm.red.business.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoStrutturaNodoEnum;
import it.ibm.red.business.exception.RedException;

/**
 * The Class DocumentoUtils.
 *
 * @author CPIERASC
 * 
 *         Utility per i documenti.
 */
public final class DocumentoUtils {

	/**
	 * Costruttore.
	 */
	private DocumentoUtils() {
	}

	/**
	 * Metodo per verificare se la categoria in input è relativa ad un documento in
	 * entrata.
	 * 
	 * @param categoria
	 *            categoria da valutare
	 * @return true se è in entrata, false altrimenti
	 */
	public static boolean isCategoriaDocumentoEntrata(final Integer categoria) {
		final CategoriaDocumentoEnum catEnum = CategoriaDocumentoEnum.get(categoria);

		return CategoriaDocumentoEnum.ENTRATA.equals(catEnum);
	}

	/**
	 * Metodo per verificare se la categoria in input è relativa ad un documento in
	 * uscita.
	 * 
	 * @param categoria
	 *            categoria da valutare
	 * @return true se è in uscita, false altrimenti
	 */
	public static boolean isCategoriaDocumentoUscita(final Integer categoria) {

		final CategoriaDocumentoEnum catEnum = CategoriaDocumentoEnum.get(categoria);

		return CategoriaDocumentoEnum.USCITA.equals(catEnum);

	}

	/**
	 * Metodo che restituisce il titolario, concatenando l'indice di classificazione
	 * all'identificativo dell'aoo.
	 * 
	 * @param idAoo
	 * @param indiceClassificazione
	 * @return
	 */
	public static String getTitolario(final Long idAoo, final String indiceClassificazione) {
		return idAoo + "_" + indiceClassificazione;
	}

	/**
	 * Compatta tutte le assegnazioni.
	 * 
	 * @param assegnazioniCompetenza
	 * @param assegnazioniConoscenza
	 * @param assegnazioniContributo
	 * @param assegnazioniFirma
	 * @param assegnazioniSigla
	 * @param assegnazioniVisto
	 * @param assegnazioniFirmaMultipla
	 * @return
	 */
	public static String[] compattaAssegnazioni(final String[] assegnazioniCompetenza,
			final String[] assegnazioniConoscenza, final String[] assegnazioniContributo,
			final String[] assegnazioniFirma, final String[] assegnazioniSigla, final String[] assegnazioniVisto,
			final String[] assegnazioniFirmaMultipla) {
		String[] assegnazioni = new String[0];
		final List<String> assegnazioniList = new ArrayList<>();

		// Assegnazioni per competenza
		aggiungiAssegnazione(assegnazioniList, assegnazioniCompetenza, TipoAssegnazioneEnum.COMPETENZA);


		// Assegnazioni per conoscenza
		aggiungiAssegnazione(assegnazioniList, assegnazioniConoscenza, TipoAssegnazioneEnum.CONOSCENZA);


		// Assegnazioni per contributo
		aggiungiAssegnazione(assegnazioniList, assegnazioniContributo, TipoAssegnazioneEnum.CONTRIBUTO);


		// Assegnazioni per firma
		aggiungiAssegnazione(assegnazioniList, assegnazioniFirma, TipoAssegnazioneEnum.FIRMA);


		// Assegnazioni per sigla
		aggiungiAssegnazione(assegnazioniList, assegnazioniSigla, TipoAssegnazioneEnum.SIGLA);


		// Assegnazione per visto
		aggiungiAssegnazione(assegnazioniList, assegnazioniVisto, TipoAssegnazioneEnum.VISTO);

		
		// Assegnazione per firma multipla
		aggiungiAssegnazione(assegnazioniList, assegnazioniFirmaMultipla, TipoAssegnazioneEnum.FIRMA_MULTIPLA);

		assegnazioni = assegnazioniList.toArray(assegnazioni);

		return assegnazioni;
	}
	
	
	private static void aggiungiAssegnazione(final List<String> assegnazioniList, final String[] assegnazioni, final TipoAssegnazioneEnum tipoAssegnazione) {
		if (assegnazioni != null && assegnazioni.length > 0) {
			for (final String assegnazione : assegnazioni) {
				assegnazioniList.add(assegnazione + "," + tipoAssegnazione.getId() + ",");
			}
		}
	}
	

	/**
	 * Restituisce un array di assegnazioni a partire dalla lista di DTO, filtrando
	 * la lista per il tipo di assegnazione in input.
	 * 
	 * @param tipoAssegnazione
	 * @param inputAssegnazioni
	 * @return
	 */
	public static String[] initArrayAssegnazioni(final TipoAssegnazioneEnum tipoAssegnazione, final Collection<AssegnazioneDTO> inputAssegnazioni) {
		String[] assegnazioniArray = new String[0];

		if (!CollectionUtils.isEmpty(inputAssegnazioni)) {
			final List<String> assegnazioniList = new ArrayList<>();
			String assegnazioneString = Constants.EMPTY_STRING;
			
			for (final AssegnazioneDTO assegnazione : inputAssegnazioni) {
				if (tipoAssegnazione.equals(assegnazione.getTipoAssegnazione())) {
					// Assegnazione a Utente
					if (assegnazione.getUtente() != null) {
						assegnazioneString = assegnazione.getUtente().getIdUfficio() + "," + assegnazione.getUtente().getId();
						// Assegnazione a Ufficio
					} else if (assegnazione.getUfficio() != null) {
						assegnazioneString = assegnazione.getUfficio().getId() + ",0";
					}
					assegnazioniList.add(assegnazioneString);
				}
			}

			assegnazioniArray = assegnazioniList.toArray(assegnazioniArray);
		}

		return assegnazioniArray;
	}

	/**
	 * Ricerca nella lista allNodi tutti i nodi che hanno il tipoStruttura contenuto
	 * in enumArgs.
	 * 
	 * Ritorna la lista ricercata oppure la sua differenza rispetto al totale in
	 * base al valore booleano inclusivo o meno
	 * 
	 * Questo metodo viene usato per invocare le response che si aspettono dei nodi
	 * ufficio
	 * 
	 * @param allNodi
	 *            lista tra cui ricercare, la lista non viene modificata.
	 * @param inclusivo
	 *            true se restituire l'insieme intersezione dei nodi che soddisfano
	 *            i requisiti, false per l'insieme differenza
	 * @param enumArgs
	 *            lista dei tipiStruttura da ricercare
	 * @return Una lista di id di nodiOrganigramma filtrata secondo i criteri
	 *         selezionati
	 */
	public static List<String> filterByTipoStruttura(final List<AssegnazioneDTO> allNodi, final Boolean inclusivo,
			final TipoStrutturaNodoEnum... enumArgs) {
		if (enumArgs.length == 0) {

			List<String> toreturn;
			if (Boolean.FALSE.equals(inclusivo)) {
				toreturn = allNodi.stream().map(c -> c.getUfficio().getId().toString()).collect(Collectors.toList());
			} else {
				toreturn = new ArrayList<>(0);
			}
			return toreturn;
		}

		final EnumSet<TipoStrutturaNodoEnum> tipiStruttura = EnumSet.of(enumArgs[0], enumArgs);
		final Predicate<AssegnazioneDTO> predicate = Boolean.TRUE.equals(inclusivo) ? c -> tipiStruttura.contains(c.getTipoStrutturaNodo())
				: c -> !tipiStruttura.contains(c.getTipoStrutturaNodo());
		return allNodi.stream().filter(predicate).map(c -> c.getUfficio().getId().toString())
				.collect(Collectors.toList());
	}

	/**
	 * Metodo che clona un oggetto.
	 * 
	 * @param object
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static Serializable clonaObject(final Serializable object){
		
		try {
			ByteArrayOutputStream outputBuffer;
			ByteArrayInputStream inputBuffer;
			Object objectCloned = null;
	
			outputBuffer = new ByteArrayOutputStream();
			ObjectOutputStream out = new ObjectOutputStream(outputBuffer);
			out.writeObject(object);
			out.flush();
			out.close();
	
			inputBuffer = new ByteArrayInputStream(outputBuffer.toByteArray());
	
			final ObjectInputStream in = new ObjectInputStream(inputBuffer);
			objectCloned = in.readObject();
			in.close();
	
			return (Serializable) objectCloned;
		} catch (IOException | ClassNotFoundException e) {
			throw new RedException(e);
		}
	}

	/**
	 * Verifica se l'evento il cui id è passato in input (eventType) è il primo di
	 * un flusso parallelo (richiesta contributo, richiesta visto, assegnazione per
	 * conoscenza, etc.).
	 * 
	 * @param eventType
	 *            id dell'evento su cui eseguire la verifica
	 * @return true se l'evento è il primo di un flusso parallelo, false altrimenti
	 */
	public static boolean isEventStartFlussoParalleloTimeline(final String eventType) {
		return (eventType != null && (EventTypeEnum.RICHIESTA_CONTRIBUTO.getValue().equals(eventType)
				|| EventTypeEnum.RICHIEDI_CONTRIBUTO.getValue().equals(eventType)
				|| EventTypeEnum.ASSEGNAZIONE_VISTO.getValue().equals(eventType)
				|| EventTypeEnum.VISTATO.getValue().equals(eventType)
				|| EventTypeEnum.RICHIESTA_VISTO.getValue().equals(eventType)
				|| EventTypeEnum.ASSEGNAZIONE_CONOSCENZA.getValue().equals(eventType)));
	}

}