package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ILogDAO;
import it.ibm.red.business.dto.ClientDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class LogDAO.
 *
 * @author CPIERASC
 * 
 * 	Dao per la gestione dei log.
 */
@Repository
public class LogDAO extends AbstractDAO implements ILogDAO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Insert log download.
	 *
	 * @param idUtente the id utente
	 * @param idRuolo the id ruolo
	 * @param idUfficio the id ufficio
	 * @param documentTitle the document title
	 * @param contentDescription the content description
	 * @param client the client
	 * @param connection the connection
	 */
	@Override
	public final void insertLogDownload(final Long idUtente, final Long idRuolo, final Long idUfficio, final String documentTitle,
			final String contentDescription, final ClientDTO client, final String source, final Connection connection) {
		PreparedStatement ps = null;
		try {
			String fields = "IDAUDIT, IDUTENTE, IDRUOLO, IDNODO, DOCUMENTTITLE, CONTENT_DESCRIPTION, IP, OS, BROWSER, USER_AGENT";
			String values = "SEQ_AUDIT_DOWNLOAD.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?";
			if (!StringUtils.isNullOrEmpty(source)) {
				fields += ", SOURCE";
				values += ", ?";
			}
			String insertQuery = "INSERT INTO AUDIT_DOWNLOAD( " + fields + " ) VALUES ( " + values + " )";
			ps = connection.prepareStatement(insertQuery);
			int n = 1;

			ps.setLong(n++, idUtente);
			ps.setLong(n++, idRuolo);
			ps.setLong(n++, idUfficio);
			ps.setString(n++, sanitize(documentTitle));
			if (contentDescription != null) {
				ps.setString(n++, sanitize(contentDescription));
			} else {
				ps.setNull(n++, Types.VARCHAR);
			}
			if (client.getIp() != null) {
				ps.setString(n++, sanitize(client.getIp()));
			} else {
				ps.setNull(n++, Types.VARCHAR);
			}
			if (client.getOs() != null) {
				ps.setString(n++, sanitize(client.getOs()));
			} else {
				ps.setNull(n++, Types.VARCHAR);
			}
			if (client.getBrowser() != null) {
				ps.setString(n++, sanitize(client.getBrowser()));
			} else {
				ps.setNull(n++, Types.VARCHAR);
			}
			if (client.getUserAgent() != null) {
				ps.setString(n++, sanitize(client.getUserAgent()));
			} else {
				ps.setNull(n++, Types.VARCHAR);
			}
			if (!StringUtils.isNullOrEmpty(source)) {
				ps.setString(n++, source);
			}
			ps.execute();
		} catch (SQLException e) {
			throw new RedException("Errore durante l'inserimento dell'audit del download ", e);
		} finally {
			closeStatement(ps, null);
		}
		
	}

}
