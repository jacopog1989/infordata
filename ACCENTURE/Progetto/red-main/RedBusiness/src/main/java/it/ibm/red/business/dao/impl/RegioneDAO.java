package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IRegioneDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.dto.RegioneDTO;

/**
 * DAO gestione regioni.
 */
@Repository
public class RegioneDAO extends AbstractDAO implements IRegioneDAO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 3484086644683607905L;

	/**
	 * Id regione.
	 */
	private static final String REGIONE_ID = "IDREGIONEISTAT";

	/**
	 * Label.
	 */
	private static final String REGIONE_DENOMINAZIONE = "DENOMINAZIONE";

	/**
	 * Gets the all.
	 *
	 * @param connection the connection
	 * @return the all
	 */
	@Override
	public List<RegioneDTO> getAll(final Connection connection) {
		List<RegioneDTO> regioni = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM regione";
		try {
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				RegioneDTO regTemp = new RegioneDTO();
				regTemp.setIdRegione(rs.getString(REGIONE_ID));
				regTemp.setDenominazione(rs.getString(REGIONE_DENOMINAZIONE));
				regioni.add(regTemp);
			}
		} catch (SQLException e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return regioni;
	}

	/**
	 * Gets regione.
	 *
	 * @param connection
	 *            the connection
	 * @param idRegione
	 *            id
	 * @return return regione
	 */
	@Override
	public RegioneDTO get(final Long idRegione, final Connection connection) {
		RegioneDTO regione = new RegioneDTO();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM regione WHERE IDREGIONEISTAT = ?";
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, idRegione.toString());
			
			rs = ps.executeQuery();
			while (rs.next()) {
				regione.setIdRegione(rs.getString(REGIONE_ID));
				regione.setDenominazione(rs.getString(REGIONE_DENOMINAZIONE));
			}
		} catch (SQLException e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return regione;
	}

	/**
	 * @see it.ibm.red.business.dao.IRegioneDAO#getRegioni(java.sql.Connection,
	 *      java.lang.String).
	 */
	@Override
	public List<RegioneDTO> getRegioni(final Connection conn, final String query) {
		List<RegioneDTO> regioni = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM regione where lower(denominazione) like '%?%' ";
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, query.toLowerCase());
			rs = ps.executeQuery();
			while (rs.next()) {
				RegioneDTO regTemp = new RegioneDTO();
				regTemp.setIdRegione(rs.getString(REGIONE_ID));
				regTemp.setDenominazione(rs.getString(REGIONE_DENOMINAZIONE));
				regioni.add(regTemp);
			}
		} catch (SQLException e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return regioni;
	}

	/**
	 * @see it.ibm.red.business.dao.IRegioneDAO#getRegioneByNome(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public RegioneDTO getRegioneByNome(final String nomeRegione, final Connection connection) {
		RegioneDTO regione = new RegioneDTO();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM regione WHERE DENOMINAZIONE = ?";
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, nomeRegione);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				regione.setIdRegione(rs.getString(REGIONE_ID));
				regione.setDenominazione(rs.getString(REGIONE_DENOMINAZIONE));
			}
		} catch (SQLException e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return regione;
	}
	
}
