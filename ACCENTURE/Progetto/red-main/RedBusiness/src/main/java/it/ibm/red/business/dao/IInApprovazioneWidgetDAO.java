package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

/**
 * Interfaccia DAO approvazione widget.
 */
public interface IInApprovazioneWidgetDAO extends Serializable {
	
	/**
	 * Ottiene gli ids dei nodi dall'id dell'utente e dall'id dell'aoo.
	 * @param idUtente
	 * @param idAOO
	 * @param flag
	 * @param connection
	 * @return lista degli ids dei nodi
	 */
	List<Long> getIdNodiFromIdUtenteandIdAOO(Long idUtente, Long idAOO, boolean flag, Connection connection);

}
