package it.ibm.red.business.service.ws.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.concrete.CreaDocumentoRedEntrataAbstractSRV;
import it.ibm.red.business.service.flusso.IAzioniFlussoBaseSRV;
import it.ibm.red.business.service.ws.ICreaDocumentoEntrataWsSRV;
import it.ibm.red.business.service.ws.auth.DocumentServiceAuthorizable;
import it.ibm.red.webservice.model.documentservice.dto.Servizio;
import it.ibm.red.webservice.model.documentservice.types.documentale.Allegato;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCreazioneDocumentoEntrataType;

/**
 * @author m.crescentini
 * 
 * Servizio per la creazione di un documento in entrata tramite web service.
 *
 */
@Service
public class CreaDocumentoEntrataWsSRV extends CreaDocumentoRedEntrataAbstractSRV implements ICreaDocumentoEntrataWsSRV {

	private static final long serialVersionUID = -3933447329746707313L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CreaDocumentoEntrataWsSRV.class.getName());

	/**
	 * Servizio utente.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * Servizio azioni.
	 */
	@Autowired
	private IAzioniFlussoBaseSRV azioniFlussoBaseSRV;

	/**
	 * DAO aoo.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * @see it.ibm.red.business.service.concrete.CreaDocumentoRedAbstractSRV#getProvenienza().
	 */
	@Override
	protected ProvenienzaSalvaDocumentoEnum getProvenienza() {
		return ProvenienzaSalvaDocumentoEnum.WEB_SERVICE;
	}
	
	/**
	 * @see it.ibm.red.business.service.ws.facade.ICreaDocumentoWsFacadeSRV#creaDocumentoEntrataRed
	 *      (it.ibm.red.webservice.model.documentservice.types.messages.RedCreazioneDocumentoEntrataType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_CREAZIONE_DOCUMENTO_ENTRATA)
	public final EsitoSalvaDocumentoDTO creaDocumentoEntrataRed(final RedWsClient client, final RedCreazioneDocumentoEntrataType docEntrataInput) {
		EsitoSalvaDocumentoDTO esito = new EsitoSalvaDocumentoDTO();
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), true);
			
			// Si recupera l'AOO associato al client
			final Aoo aoo = aooDAO.getAoo(client.getIdAoo(), con);
			
			if (aoo != null) {
				// Utente creatore
				final UtenteDTO utenteCreatore = utenteSRV.getById(docEntrataInput.getIdUtenteCreatore().longValue(), con);
									
				// Allegati
				final List<AllegatoDTO> allegati = getAllegati(docEntrataInput.getAllegati(), aoo.getCodiceAoo());
				
				// Assegnatario
				final Long idUtenteAssegnatario = docEntrataInput.getAssegnatarioCompetenza().getIdUtente() != null 
						? docEntrataInput.getAssegnatarioCompetenza().getIdUtente().longValue() : null;
				final Long idUfficioAssegnatario = docEntrataInput.getAssegnatarioCompetenza().getIdGruppo().longValue();
				
				// ### Creazione del documento
				esito = creaDocumentoRedEntrataDaContent(docEntrataInput.getDataHandler(), docEntrataInput.getDocType(), docEntrataInput.getNomeFile(), 
						docEntrataInput.getOggetto(), utenteCreatore, docEntrataInput.getIndiceClassificazione(), docEntrataInput.getIdTipologiaDocumento(), 
						docEntrataInput.getIdTipologiaProcedimento(), Long.valueOf(docEntrataInput.getIdMittente()), allegati, idUtenteAssegnatario, 
						idUfficioAssegnatario, docEntrataInput.getNumeroProtocollo(), docEntrataInput.getAnnoProtocollo(), 
						docEntrataInput.getDataProtocollo() != null ? docEntrataInput.getDataProtocollo().toGregorianCalendar().getTime() : null, 
						docEntrataInput.getFlusso(), aoo, con);
				
				if (esito.isEsitoOk()) {
					// ### Si eseguono le azioni successive alla creazione del documento per il flusso indicato in input (se presente)
					azioniFlussoBaseSRV.doAfterCreazioneDocumento(Integer.parseInt(esito.getDocumentTitle()), docEntrataInput.getFlusso(), con);
					
					commitConnection(con);
					
					LOGGER.info("Documento in entrata creato correttamente. Document Title: " + esito.getDocumentTitle() 
							+ ", Numero Documento: " + esito.getNumeroDocumento());
				} else {
					final String erroreSalvaDocumento = esito.getErrori().get(0).getMessaggio();
					LOGGER.error("Si è verificato un errore durante il salvataggio del documento: " + erroreSalvaDocumento);
					throw new RedException("Errore durante il salvataggio del documento: " + erroreSalvaDocumento);
				}
			} else {
				LOGGER.error("AOO associato al client WS con ID " + client.getIdClient() + " non trovato");
				throw new RedException("AOO associato al client WS con ID " + client.getIdClient() + " non trovato");
			}
			
		} catch (final Exception e) {
			rollbackConnection(con);
			LOGGER.error("Si è verificato un errore durante la creazione del documento: " + e.getMessage(), e);
			throw new RedException("Errore durante la creazione del documento: " + e.getMessage());
		} finally {
			closeConnection(con);
		}
		
		return esito;
	}


	/**
	 * @param allegatiInput
	 * @return
	 */
	private List<AllegatoDTO> getAllegati(final List<Allegato> allegatiInput, final String codiceAoo) {
		final List<AllegatoDTO> allegati = new ArrayList<>();
		
		if (!CollectionUtils.isEmpty(allegatiInput)) {
			AllegatoDTO allegato = null;
			final Integer idTipologiaDocumento = Integer.parseInt(PropertiesProvider.getIstance().getParameterByString(codiceAoo + ".tipologia.generica.default"));
			for (final Allegato all : allegatiInput) {
				allegato = new AllegatoDTO(all.getNomeFile(), all.getDataHandler(), all.getDocType(), all.getOggetto(), false, 
						all.isMantieniFormatoOriginale(), idTipologiaDocumento);
				
				allegati.add(allegato);
			}
		}
		
		return allegati;
	}
	
}