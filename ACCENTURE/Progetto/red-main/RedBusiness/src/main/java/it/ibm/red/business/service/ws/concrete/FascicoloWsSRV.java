package it.ibm.red.business.service.ws.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.Document;

import it.ibm.red.business.dto.FascicoloWsDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.FascicoloRedFnDTO;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.service.ITitolarioSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.concrete.AbstractService;
import it.ibm.red.business.service.ws.IFascicoloWsSRV;
import it.ibm.red.business.service.ws.auth.DocumentServiceAuthorizable;
import it.ibm.red.webservice.model.documentservice.dto.Servizio;
import it.ibm.red.webservice.model.documentservice.types.messages.RedAssociaFascicoloATitolarioType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCambiaStatoFascicoloType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCreaFascicoloType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedEliminaFascicoloType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedFascicolazioneDocumentoType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedModificaFascicoloType;

/**
 * 
 * @author a.dilegge
 *
 */
@Service
public class FascicoloWsSRV extends AbstractService implements IFascicoloWsSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 6088787481287153939L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FascicoloWsSRV.class.getName());

	/**
	 * Service.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IFascicoloSRV fascicoloSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ISecuritySRV securitySRV;

	/**
	 * Service.
	 */
	@Autowired
	private ITitolarioSRV titolarioSRV;

	/**
	 * @see it.ibm.red.business.service.ws.facade.IFascicoloWsFacadeSRV#
	 *      redFascicolazioneDocumento
	 *      (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedFascicolazioneDocumentoType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_FASCICOLAZIONE_DOCUMENTO)
	public void redFascicolazioneDocumento(final RedWsClient client, final RedFascicolazioneDocumentoType request) {
		IFilenetCEHelper fceh = null;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final UtenteDTO utente = utenteSRV.getById(request.getIdUtente().longValue(), con);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fascicoloSRV.fascicola(request.getIdFascicolo().toString(), request.getDocumentTitle(), 0, client.getIdAoo(), fceh, con);
		} catch (final RedException e) {
			LOGGER.error("Si è verificato un errore durante la fascicolazione del documento", e);
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la fascicolazione del documento: " + e.getMessage(), e);
			throw new RedException("Errore durante la fascicolazione del documento: " + e.getMessage(), e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}

	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IFascicoloWsFacadeSRV#
	 *      redEliminaFascicolo (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedEliminaFascicoloType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_ELIMINA_FASCICOLO)
	public void redEliminaFascicolo(final RedWsClient client, final RedEliminaFascicoloType request) {
		IFilenetCEHelper fceh = null;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final Aoo aoo = aooSRV.recuperaAoo(client.getIdAoo().intValue(), con);
			final AooFilenet aooFilenet = aoo.getAooFilenet();

			// Accesso come p8admin
			fceh = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getConnectionPoint(), aooFilenet.getObjectStore(), aooFilenet.getIdClientAoo()), aoo.getIdAoo());

			final boolean fascicoloEliminato = fascicoloSRV.eliminaFascicolo(request.getIdFascicolo(), client.getIdAoo(), fceh);
			if (!fascicoloEliminato) {
				throw new RedException("Impossibile eliminare il fascicolo: non è stato trovato, oppure contiene dei documenti");
			}
		} catch (final RedException e) {
			LOGGER.error("Si è verificato un errore durante l'eliminazione del fascicolo", e);
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la fascicolazione del documento: " + e.getMessage(), e);
			throw new RedException("Errore durante la fascicolazione del documento: " + e.getMessage(), e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IFascicoloWsFacadeSRV#
	 *      redCambiaStatoFascicolo
	 *      (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedCambiaStatoFascicoloType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_CAMBIA_STATO_FASCICOLO)
	public void redCambiaStatoFascicolo(final RedWsClient client, final RedCambiaStatoFascicoloType request) {
		IFilenetCEHelper fceh = null;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final UtenteDTO utente = utenteSRV.getById(request.getIdUtente().longValue(), null, request.getIdnodo().longValue(), con);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final FilenetStatoFascicoloEnum statoFascicolo = FilenetStatoFascicoloEnum.getEnumById(request.getStato());
			fascicoloSRV.aggiornaStato(request.getIdFascicolo(), statoFascicolo, utente.getIdAoo(), fceh);
		} catch (final RedException e) {
			LOGGER.error("Si è verificato un errore durante l'aggiornamento dello stato del fascicolo", e);
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'aggiornamento dello stato del fascicolo: " + e.getMessage(), e);
			throw new RedException("Errore durante durante l'aggiornamento dello stato del fascicolo: " + e.getMessage(), e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IFascicoloWsFacadeSRV#
	 *      redModificaFascicolo (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedModificaFascicoloType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_MODIFICA_FASCICOLO)
	public void redModificaFascicolo(final RedWsClient client, final RedModificaFascicoloType request) {
		IFilenetCEHelper fceh = null;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final UtenteDTO utente = utenteSRV.getById(request.getIdUtente().longValue(), null, request.getIdnodo().longValue(), con);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final FilenetStatoFascicoloEnum statoFascicolo = FilenetStatoFascicoloEnum.getEnumById(request.getStato());
			fascicoloSRV.aggiornaMetadati(request.getIdFascicolo(), statoFascicolo, request.getOggetto(), utente.getIdAoo(), fceh);
		} catch (final RedException e) {
			LOGGER.error("Si è verificato un errore durante l'aggiornamento dei metadati del fascicolo", e);
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'aggiornamento dei metadati del fascicolo: " + e.getMessage(), e);
			throw new RedException("Errore durante durante l'aggiornamento dei metadati del fascicolo: " + e.getMessage(), e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IFascicoloWsFacadeSRV#
	 *      redAssociaFascicoloATitolario
	 *      (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedAssociaFascicoloATitolarioType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_ASSOCIA_FASCICOLO_A_TITOLARIO)
	public void redAssociaFascicoloATitolario(final RedWsClient client, final RedAssociaFascicoloATitolarioType request) {
		IFilenetCEHelper fceh = null;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final UtenteDTO utente = utenteSRV.getById(request.getIdUtente().longValue(), null, request.getIdnodo().longValue(), con);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final TitolarioDTO titolario = titolarioSRV.getNodoByIndice(utente.getIdAoo(), request.getIdTitolario());
			fascicoloSRV.associaATitolarioAggiornaNPS(utente, request.getIdFascicolo(), titolario.getIndiceClassificazione(), titolario.getDescrizione());
		} catch (final RedException e) {
			LOGGER.error("Si è verificato un errore durante l'associazione del fascicolo al titolario", e);
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'associazione del fascicolo al titolario: " + e.getMessage(), e);
			throw new RedException("Errore durante durante l'associazione del fascicolo al titolario: " + e.getMessage(), e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IFascicoloWsFacadeSRV#redCreaFascicolo
	 *      (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedCreaFascicoloType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_CREA_FASCICOLO)
	public String redCreaFascicolo(final RedWsClient client, final RedCreaFascicoloType request) {
		IFilenetCEHelper fceh = null;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final UtenteDTO utente = utenteSRV.getById(request.getIdUtente().longValue(), null, request.getIdnodo().longValue(), con);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final ListSecurityDTO securityFascicolo = securitySRV.getSecurityPuntuale(request.getIdnodo().longValue(), request.getIdUtente().longValue(), con);
			final FascicoloRedFnDTO fascicolo = fascicoloSRV.creaFascicolo(utente, request.getIdTitolario(), request.getOggetto(), null, securityFascicolo, null, fceh, con);

			return fascicolo.getIdFascicolo();
		} catch (final RedException e) {
			LOGGER.error("Si è verificato un errore durante la creazione del fascicolo", e);
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la creazione del fascicolo: " + e.getMessage(), e);
			throw new RedException("Errore durante durante la creazione del fascicolo: " + e.getMessage(), e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ws.IFascicoloWsSRV#transformToFascicoliWs
	 *      (com.filenet.api.collection.DocumentSet, java.sql.Connection).
	 */
	@Override
	public List<FascicoloWsDTO> transformToFascicoliWs(final DocumentSet fascicoliFilenet, final Connection con) {
		return transformToFascicoliWs(fascicoliFilenet, false, null, false, null, con);
	}

	/**
	 * @see it.ibm.red.business.service.ws.IFascicoloWsSRV#transformToFascicoliWs
	 *      (com.filenet.api.collection.DocumentSet, boolean, java.util.Set,
	 *      boolean, it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public List<FascicoloWsDTO> transformToFascicoliWs(final DocumentSet fascicoliFilenet, final boolean withMetadati, final Set<String> inMetadatiDaRecuperare,
			final boolean withSecurity, final IFilenetCEHelper fceh, final Connection con) {
		List<FascicoloWsDTO> fascicoliWsList = null;

		if (fascicoliFilenet != null && !fascicoliFilenet.isEmpty()) {
			fascicoliWsList = new ArrayList<>();

			FascicoloWsDTO fascicoloWs = null;
			final Iterator<?> itFascicoliFilenet = fascicoliFilenet.iterator();
			while (itFascicoliFilenet.hasNext()) {
				fascicoloWs = transformToFascicoloWs((Document) itFascicoliFilenet.next(), withMetadati, inMetadatiDaRecuperare, withSecurity, fceh, con);

				if (fascicoloWs != null) {
					fascicoliWsList.add(fascicoloWs);
				}
			}
		}

		return fascicoliWsList;
	}

	/**
	 * @see it.ibm.red.business.service.ws.IFascicoloWsSRV#transformToFascicoloWs
	 *      (com.filenet.api.core.Document, boolean, java.util.Set, boolean,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public FascicoloWsDTO transformToFascicoloWs(final Document fascicoloFilenet, final boolean withMetadati, final Set<String> inMetadatiDaRecuperare,
			final boolean withSecurity, final IFilenetCEHelper fceh, final Connection con) {
		final Map<ContextTrasformerCEEnum, Object> context = new EnumMap<>(ContextTrasformerCEEnum.class);
		context.put(ContextTrasformerCEEnum.RECUPERA_SECURITY, withSecurity);
		context.put(ContextTrasformerCEEnum.RECUPERA_METADATI, withMetadati);
		if (withMetadati) {
			Set<String> metadatiDaRecuperare = null;
			if (!CollectionUtils.isEmpty(inMetadatiDaRecuperare)) {
				metadatiDaRecuperare = inMetadatiDaRecuperare;
			} else {
				metadatiDaRecuperare = fceh.getClassDefinitionPropertiesNameWithoutSystemProps(fascicoloFilenet.getClassName());
			}
			context.put(ContextTrasformerCEEnum.CLASS_DEFINITION_PROPERTIES, metadatiDaRecuperare);
		}

		return TrasformCE.transform(fascicoloFilenet, TrasformerCEEnum.FROM_DOCUMENT_TO_FASCICOLO_WS, context, con);
	}

}