package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class EventoLogDTO.
 *
 * @author mcrescentini
 * 
 * 	DTO per modellare gli eventi da tracciare (EventoLog)
 */
public class EventoLogDTO implements Serializable {

	/**
	 * The Constant serialVersionUID.
	 */
 	private static final long serialVersionUID = -2355498096255266751L;

	
	/**
	 * Identificativo documento.
	 */
	private int idDocumento;
	
	/**
	 * Ufficio mittente.
	 */
	private Long idUfficioMittente;
	
	/**
	 * Utente mittente.
	 */
	private Long idUtenteMittente;
	
	/**
	 * Ufficio destinatario.
	 */
	private Long idUfficioDestinatario;
	
	/**
	 * Utente destinatario.
	 */
	private Long idUtenteDestinatario;
	
	/**
	 * Data assegnazione.
	 */
	private Date dataAssegnazione;
	
	/**
	 * Data ultimo evento.
	 */
	private Date dataUltimoEvento;
	
	/**
	 * Tipo evento.
	 */
	private String eventType;
	
	/**
	 * Motivo assegnazione.
	 */
	private String motivazioneAssegnazione;
	
	/**
	 * Tipo assegnazione.
	 */
	private int idTipoAssegnazione;
	
	/**
	 * Numero workflow.
	 */
	private String workFlowNumber;
	
	/**
	 * Workflow padre.
	 */
	private String workFlowNumberPadre;
	
	/**
	 * Costruttore.
	 */
	public EventoLogDTO() {
		super();
	}


	/**
	 * @param idDocumento
	 * @param idUfficioMittente
	 * @param idUtenteMittente
	 * @param idUfficioDestinatario
	 * @param idUtenteDestinatario
	 * @param dataAssegnazione
	 * @param eventType
	 * @param motivazioneAssegnazione
	 * @param idTipoAssegnazione
	 */
	public EventoLogDTO(final int idDocumento, final Long idUfficioMittente, final Long idUtenteMittente, final Long idUfficioDestinatario, final Long idUtenteDestinatario, final Date dataAssegnazione, 
			final String eventType, final String motivazioneAssegnazione, final int idTipoAssegnazione) {
		super();
		this.idDocumento = idDocumento;
		this.idUfficioMittente = idUfficioMittente;
		this.idUtenteMittente = idUtenteMittente;
		this.idUfficioDestinatario = idUfficioDestinatario;
		this.idUtenteDestinatario = idUtenteDestinatario;
		this.dataAssegnazione = dataAssegnazione;
		this.eventType = eventType;
		this.motivazioneAssegnazione = motivazioneAssegnazione;
		this.idTipoAssegnazione = idTipoAssegnazione;
	}
	
	/**
	 * Recupera l'id del documento.
	 * @return id del documento
	 */
	public int getIdDocumento() {
		return idDocumento;
	}
	
	/**
	 * Imposta l'id del documento.
	 * @param idDocumento id del documento
	 */
	public void setIdDocumento(final int idDocumento) {
		this.idDocumento = idDocumento;
	}
	
	/**
	 * Recupera l'id dell'ufficio mittente.
	 * @return id dell'ufficio mittente
	 */
	public Long getIdUfficioMittente() {
		return idUfficioMittente;
	}
	
	/**
	 * Imposta l'id dell'ufficio mittente.
	 * @param idUfficioMittente id dell'ufficio mittente
	 */
	public void setIdUfficioMittente(final Long idUfficioMittente) {
		this.idUfficioMittente = idUfficioMittente;
	}
	
	/**
	 * Recupera l'id dell'utente mittente.
	 * @return id dell'utente mittente
	 */
	public Long getIdUtenteMittente() {
		return idUtenteMittente;
	}
	
	/**
	 * Imposta l'id dell'utente mittente.
	 * @param idUtenteMittente id dell'utente mittente
	 */
	public void setIdUtenteMittente(final Long idUtenteMittente) {
		this.idUtenteMittente = idUtenteMittente;
	}
	
	/**
	 * Recupera l'id dell'ufficio destinatario.
	 * @return id dell'ufficio destinatario
	 */
	public Long getIdUfficioDestinatario() {
		return idUfficioDestinatario;
	}
	
	/**
	 * Imposta l'id dell'ufficio destinatario.
	 * @param idUfficioDestinatario id dell'ufficio destinatario
	 */
	public void setIdUfficioDestinatario(final Long idUfficioDestinatario) {
		this.idUfficioDestinatario = idUfficioDestinatario;
	}
	
	/**
	 * Recupera l'id dell'utente destinatario.
	 * @return id dell'utente destinatario
	 */
	public Long getIdUtenteDestinatario() {
		return idUtenteDestinatario;
	}
	
	/**
	 * Imposta l'id dell'utente destinatario.
	 * @param idUtenteDestinatario id dell'utente destinatario
	 */
	public void setIdUtenteDestinatario(final Long idUtenteDestinatario) {
		this.idUtenteDestinatario = idUtenteDestinatario;
	}
	
	/**
	 * Recupera la data di assegnazione.
	 * @return data di assegnazione
	 */
	public Date getDataAssegnazione() {
		return dataAssegnazione;
	}
	
	/**
	 * Imposta la data di assegnazione.
	 * @param dataAssegnazione data di assegnazione
	 */
	public void setDataAssegnazione(final Date dataAssegnazione) {
		this.dataAssegnazione = dataAssegnazione;
	}
	
	/**
	 * Recupera la data dell'ultimo evento.
	 * @return data dell'ultimo evento
	 */
	public Date getDataUltimoEvento() {
		return dataUltimoEvento;
	}

	/**
	 * Imposta la data dell'ultimo evento.
	 * @param dataUltimoEvento data dell'ultimo evento
	 */
	public void setDataUltimoEvento(final Date dataUltimoEvento) {
		this.dataUltimoEvento = dataUltimoEvento;
	}

	/**
	 * Recupera il tipo di evento.
	 * @return tipo di evento
	 */
	public String getEventType() {
		return eventType;
	}
	
	/**
	 * Imposta il tipo di evento.
	 * @param eventType tipo di evento
	 */
	public void setEventType(final String eventType) {
		this.eventType = eventType;
	}
	
	/**
	 * Recupera la motivazione dell'assegnazione.
	 * @return motivazione dell'assegnazione
	 */
	public String getMotivazioneAssegnazione() {
		return motivazioneAssegnazione;
	}
	
	/**
	 * Imposta la motivazione dell'assegnazione.
	 * @param motivazioneAssegnazione motivazione dell'assegnazione
	 */
	public void setMotivazioneAssegnazione(final String motivazioneAssegnazione) {
		this.motivazioneAssegnazione = motivazioneAssegnazione;
	}
	
	/**
	 * Recupera l'id del tipo di assegnazione.
	 * @return id del tipo di assegnazione
	 */
	public int getIdTipoAssegnazione() {
		return idTipoAssegnazione;
	}
	
	/**
	 * Imposta l'id del tipo di assegnazione.
	 * @param idTipoAssegnazione id del tipo di assegnazione
	 */
	public void setIdTipoAssegnazione(final int idTipoAssegnazione) {
		this.idTipoAssegnazione = idTipoAssegnazione;
	}

	/**
	 * Recupera il numero del WF.
	 * @return numero del WF
	 */
	public String getWorkFlowNumber() {
		return workFlowNumber;
	}

	/**
	 * Imposta il numero del WF.
	 * @param workFlowNumber numero del WF
	 */
	public void setWorkFlowNumber(final String workFlowNumber) {
		this.workFlowNumber = workFlowNumber;
	}

	/**
	 * Recupera il numero del WF padre.
	 * @return numero del WF padre
	 */
	public String getWorkFlowNumberPadre() {
		return workFlowNumberPadre;
	}

	/**
	 * Imposta il numero del WF padre.
	 * @param workFlowNumberPadre numero del WF padre
	 */
	public void setWorkFlowNumberPadre(final String workFlowNumberPadre) {
		this.workFlowNumberPadre = workFlowNumberPadre;
	}
}