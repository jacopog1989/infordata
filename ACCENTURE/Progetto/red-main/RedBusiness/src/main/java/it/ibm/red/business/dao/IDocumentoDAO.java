package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

import it.ibm.red.business.dto.GestioneLockDTO;
import it.ibm.red.business.enums.StatoLavorazioneEnum;

/**
 * Dao documento.
 * 
 * @author mcrescentini
 */
public interface IDocumentoDAO extends Serializable {
	
	/**
	 * Effettua la insert del documento rendendolo persistente sulla base dati.
	 * 
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param idTipologiaDocumento
	 *            Identificativo della tipologia documentale.
	 * @param registroRiservato
	 *            Flag associato al registro riservato.
	 * @param connection
	 *            Connessione al database.
	 * @return Esito della insert sul database.
	 */
	int insert(Long idAoo, Integer idTipologiaDocumento, int registroRiservato, Connection connection);
	
	/**
	 * Generazione identificativo documento ad uso degli allegati che non vengono
	 * inseriti in tabella documento.
	 * 
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param connection
	 *            Connessione al database.
	 * @return Id generato dalla sequence.
	 */
	int getNextId(Long idAoo, Connection connection);

	/**
	 * Aggiorna la tipologia documento identificata dall'
	 * {@code idTipologiaDocumento}.
	 * 
	 * @param idDocumento
	 *            Identificativo del documento.
	 * @param idTipologiaDocumento
	 *            Identificativo della tipologia documentale.
	 * @param con
	 *            Connessione al database.
	 */
	void aggiornaTipologiaDocumento(int idDocumento, int idTipologiaDocumento, Connection con);
	
	/**
	 * Inserisce lo stato di lavorazione del documento identificato dall'
	 * {@code idDocumento}.
	 * 
	 * @param idDocumento
	 *            Identificativo del documento.
	 * @param idUfficio
	 *            Identificativo dell'ufficio.
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @param statoLavorazione
	 *            Stato lavorazione da inserire.
	 * @param con
	 *            Connessione al database.
	 */
	void inserisciStatoDocumentoLavorazione(int idDocumento, Long idUfficio, Long idUtente, StatoLavorazioneEnum statoLavorazione, Connection con);

	/**
	 * Rimuove gli item relativi al nodo-utente per il documento identificato dall'
	 * {@code idDocumento}.
	 * 
	 * @param idDocumento
	 *            Identificativo del documento.
	 * @param idUfficio
	 *            Identificativo dell'ufficio.
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @param con
	 *            Connessione al database.
	 */
	void rimuoviItemNodoUtente(int idDocumento, Long idUfficio, Long idUtente, Connection con);

	/**
	 * Rimuove gli item relativi al nodo per il documento identificato dall'
	 * {@code idDocumento}.
	 * 
	 * @param idDocumento
	 *            Identificativo del documento.
	 * @param idUfficio
	 *            Identificativo dell'ufficio.
	 * @param con
	 *            Connessione al database.
	 */
	void rimuoviItemNodo(int idDocumento, Long idUfficio, Connection con);

	/**
	 * Aggiorna lo stato di lavorazione del documento identificato dall'
	 * {@code idDocumento}.
	 * 
	 * @param idDocumento
	 *            Identificativo del documento.
	 * @param idUfficio
	 *            Identificativo dell'ufficio.
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @param statoLavorazione
	 *            Stato lavorazione da aggiornare.
	 * @param con
	 *            Connessione al database.
	 */
	void aggiornaStatoDocumentoLavorazione(int idDocumento, Long idUfficio, Long idUtente, StatoLavorazioneEnum statoLavorazione, Connection con);
	
	/**
	 * Ottiene il lock del documento identificato dal {@code documentTitle}.
	 * 
	 * @param documentTitle
	 *            Identificativo del documento.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param statoLock
	 *            Identificativo dello stato del lock.
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @param conn
	 *            Connessione al database.
	 * @return Lock del documento, {@code null} nessun lock, {@code false} lockato
	 *         da un altro utente, {@code true} lock me stesso.
	 */
	GestioneLockDTO getLockDocumento(Integer documentTitle, Long idAoo, Integer statoLock, Long idUtente, Connection conn);
	
	/**
	 * Elimina il lock del documento identificato dal {@code documentTitle}.
	 * 
	 * @param documentTitle
	 *            Identificativo del documento.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param statoLock
	 *            Stato del lock da cancellare.
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @param conn
	 *            Connessione al database.
	 * @return {@code true} o {@code false} in base all'esito della delete.
	 */
	boolean deleteLockDocumento(Integer documentTitle, Long idAoo, Integer statoLock, Long idUtente, Connection conn);

	/**
	 * Ottiene il lock massivo dei documenti identificati dai {@code documentTitle}.
	 * 
	 * @param documentTitle
	 *            Identificativi dei documenti.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param statoLock
	 *            Stato del lock specificato.
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @param conn
	 *            Connessione al database.
	 * @return Lock dei documenti.
	 */
	GestioneLockDTO getLockDocumentoMassivo(List<Integer> documentTitle, Long idAoo, Integer statoLock, Long idUtente,
			Connection conn);

	/**
	 * Ottiene il lock dei documenti master identificati dai {@code documentTitle}.
	 * 
	 * @param documentTitle
	 *            Identificativo del documento.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param conn
	 *            Connessione al database.
	 * @return mappa document title lock del documento
	 */
	HashMap<Long, GestioneLockDTO> getLockDocumentiMaster(List<String> documentTitle, Long idAoo, Connection conn);

	/**
	 * Inserisce il lock del documento identificato dal {@code documentTitle}.
	 * 
	 * @param documentTitle
	 *            Identificativo del documento.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @param statoLock
	 *            Stato del lock.
	 * @param isIncarico
	 *            Flag <em> in carico </em>.
	 * @param nome
	 *            Nome utente.
	 * @param cognome
	 *            Cognome utente.
	 * @param conn
	 *            Connessione al database.
	 * @return Esito della insert.
	 */
	boolean insertLockDocumento(Integer documentTitle, Long idAoo, Long idUtente, Integer statoLock, boolean isIncarico,
			String nome, String cognome, Connection conn);

	/**
	 * Aggiorna il lock del documento identificato dal {@code documentTitle}.
	 * 
	 * @param documentTitle
	 *            Identificativo del documento.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param statoLock
	 *            Stato del lock.
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @param isIncarico
	 *            Flag <em> in carico </em>.
	 * @param nome
	 *            Nome utente.
	 * @param cognome
	 *            Cognome utente.
	 * @param conn
	 *            Connessione al database.
	 * @return Esito della insert.
	 */
	boolean updateLockDocumento(Integer documentTitle, Long idAoo, Integer statoLock, Long idUtente, boolean isIncarico,
			String nome, String cognome, Connection conn);

	/**
	 * Aggiorna l'utente per il documento identificato dall' {@code idDocumento}.
	 * 
	 * @param idDocumento
	 *            Identificativo del documento.
	 * @param idUfficio
	 *            Identificativo dell'ufficio.
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @param con
	 *            Connessione al database.
	 */
	void annullaUtenteDocumento(int idDocumento, Long idUfficio, Long idUtente, Connection con);

	/**
	 * Verifica la presenza di item relativi al nodo per il documento identificato
	 * dall' {@code idDocumento}.
	 * 
	 * @param idDocumento
	 *            Identificativo del documento.
	 * @param idUfficio
	 *            Identificativo dell'ufficio.
	 * @param con
	 *            Connessione al database.
	 * @return {@code true} se ha almeno un item relativo al nodo, {@code false}
	 *         altrimenti.
	 */
	boolean hasItemNodo(int idDocumento, Long idUfficio, Connection con);
}