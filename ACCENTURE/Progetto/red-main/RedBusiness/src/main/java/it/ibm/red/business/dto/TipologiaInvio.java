package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * DTO che descrive la tipologia invio.
 */
public class TipologiaInvio implements Serializable  {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -7270107623678782193L;

	/**
	 * Identificativo tipologia invio.
	 */
	private Integer idTipologiaInvio;

	/**
	 * Restituisce l'id della tipologia invio.
	 * @return id tipologia invio
	 */
	public Integer getIdTipologiaInvio() {
		return idTipologiaInvio;
	}

	/**
	 * Imposta l'id della tipologia invio.
	 * @param idTipologiaInvio
	 */
	public void setIdTipologiaInvio(final Integer idTipologiaInvio) {
		this.idTipologiaInvio = idTipologiaInvio;
	}
}
