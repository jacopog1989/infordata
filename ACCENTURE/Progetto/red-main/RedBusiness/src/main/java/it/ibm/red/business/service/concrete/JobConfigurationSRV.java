package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IJobConfigurationDAO;
import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IJobConfigurationSRV;

/**
 * The Class JobConfigurationSRV.
 *
 * @author a.dilegge
 * 
 *         Servizio gestione job.
 */
@Service
@Component
public class JobConfigurationSRV extends AbstractService implements IJobConfigurationSRV {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -1720173749210830823L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(JobConfigurationSRV.class.getName());

	/**
	 * Dao gestione job.
	 */
	@Autowired
	private IJobConfigurationDAO jobConfigurationDAO;

	/**
	 * Recupero di tutti i job configurati su db.
	 * 
	 * @return
	 */
	@Override
	public final List<JobConfigurationDTO> getAll() {
		List<JobConfigurationDTO> output = null;
		Connection connection =  null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			output = jobConfigurationDAO.getAll(connection);
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero dei job dal DB: " + e.getMessage(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
		return output;
	}

}
