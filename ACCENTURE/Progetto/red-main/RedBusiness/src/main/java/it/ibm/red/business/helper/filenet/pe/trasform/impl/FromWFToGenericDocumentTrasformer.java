package it.ibm.red.business.helper.filenet.pe.trasform.impl;

import java.util.Date;

import filenet.vw.api.VWException;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.NotaDTO;
import it.ibm.red.business.dto.PEDocumentoDTO;
import it.ibm.red.business.enums.ColoreNotaEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoOperazioneLibroFirmaEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.DateUtils;

/**
 * Trasformer da WF al Generic Document - Abstract.
 * @param <T>
 */
public abstract class FromWFToGenericDocumentTrasformer<T extends PEDocumentoDTO> extends FromWFToDocumentoAbstractTrasformer<T> {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 5061030663262501962L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromWFToGenericDocumentTrasformer.class.getName());
	
	/**
	 * Properties.
	 */
	private PropertiesProvider pp;
	
	FromWFToGenericDocumentTrasformer(final TrasformerPEEnum inEnumKey) {
		super(inEnumKey);
		pp = PropertiesProvider.getIstance();
	}
	
	/**
	 * @see it.ibm.red.business.helper.filenet.pe.trasform.impl.FromWFToDocumentoAbstractTrasformer#createAbstract(filenet.vw.api.VWWorkObject,
	 *      java.lang.String, java.lang.String, java.lang.Integer, java.lang.String,
	 *      it.ibm.red.business.enums.DocumentQueueEnum, java.lang.Integer,
	 *      java.lang.Integer).
	 */
	@Override
	T createAbstract(final VWWorkObject object, final String inWobNumber, final String idDocumento, final Integer tipoAssegnazioneId,
			final String codaCorrente, final DocumentQueueEnum queue, final Integer idNodoDestinatario, final Integer idUtenteDestinatario) {

		String[] responses = object.getStepResponses();
		String stepName = object.getStepName();
		String fClass = (String) getMetadato(object, Constants.PEProperty.VW_CLASS);
		String dataCreazioneWF = DateUtils.dateToString((Date) getMetadato(object, Constants.PEProperty.IW_LAUNCHDATE), null);
		String subject = (String) getMetadato(object, Constants.PEProperty.F_SUBJECT);
		Boolean bFirmaFig = false;
		if ("RED_FirmaRagGenDelloStato".equalsIgnoreCase(fClass)) {
			Integer firmaDigRGS = (Integer) getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.FIRMA_DIG_RGS_WF_METAKEY));
			if (firmaDigRGS != null && firmaDigRGS > 0) {
				bFirmaFig = true;
			}
		}

		Boolean urgente = false;
		Integer flagUrgente = (Integer) getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY));
		if (flagUrgente != null && flagUrgente == 1) {
			urgente = true;
		}
		
		Integer flagIterManuale = (Integer) getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.FLAG_ITER_MANUALE_WF_METAKEY));
		Integer idFascicolo = (Integer) getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY));
		String motivazioneAssegnazione = (String) getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY));
	
		int tipoFirma = 0;
		
		Integer tipoFirmaInteger = (Integer) getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_FIRMA_FN_METAKEY));
		if (tipoFirmaInteger != null) {
			tipoFirma = tipoFirmaInteger.intValue();
		}
		
		String [] metadatiName = object.getFieldNames();
		boolean isElencoLibroFirma = false;
		for (int i = 0; i < metadatiName.length; i++) {
			if (metadatiName[i].equals(pp.getParameterByKey(PropertiesNameEnum.ELENCO_LIBROFIRMA_METAKEY))) {
				isElencoLibroFirma = true;
				break;
			}
		}

		String wobNumber = inWobNumber;
		try {
			wobNumber = object.getWorkObjectNumber();
		} catch (VWException e) {
			getLogger().error(" Errore nel recupero di wobNumber o Nome Coda ", e);
			LOGGER.error(e);
		}
		
		String[] elencoLibroFirma = null;
		if (isElencoLibroFirma) {
			elencoLibroFirma = (String[]) getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.ELENCO_LIBROFIRMA_METAKEY));
		}
		Integer count = (Integer)  getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.COUNT_METAKEY));
		TipoOperazioneLibroFirmaEnum tolf = TipoOperazioneLibroFirmaEnum.get("" + tipoAssegnazioneId);
		Integer flagRenderizzato = (Integer) getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.FLAG_RENDERIZZATO_METAKEY));
		Boolean flagRenderizzatoBool = Boolean.FALSE;
		if (flagRenderizzato != null) {
			flagRenderizzatoBool = (flagRenderizzato.intValue() == 1);
		}
		Boolean firmaCopiaConforme = (Boolean) getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.FIRMA_COPIA_CONFORME_METAKEY));
		String nota = (String) getMetadato(object, "F_Comment");
		
		Integer contributiRichiesti = null;
		Integer contributiPervenuti = null;
		
		if (fClass.startsWith("Contributi_Ufficio")) {
			contributiRichiesti = (Integer) getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.CONTRIBUTI_RICHIESTI_METAKEY));
			contributiPervenuti = (Integer) getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.CONTRIBUTI_PERVENUTI_METAKEY));
		}
		
		Integer annullaTemplateInt = (Integer) getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.ANNULLA_TEMPLATE_METAKEY));
		boolean annullaTemplate = false;
		if (annullaTemplateInt != null) {
			annullaTemplate = (annullaTemplateInt.intValue() == 1);
		}
		
		Boolean firmaAsincronaAvviata = (Boolean) getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.FLAG_FIRMA_ASINCRONA_AVVIATA_WF_METAKEY));

		return createGeneric(object, idDocumento, tolf, wobNumber, queue, idUtenteDestinatario, 
				idNodoDestinatario, bFirmaFig, motivazioneAssegnazione, responses, idFascicolo, codaCorrente, tipoAssegnazioneId, 
				flagIterManuale, subject, elencoLibroFirma, count, dataCreazioneWF, flagRenderizzatoBool, firmaCopiaConforme, urgente, stepName, 
				ColoreNotaEnum.getNotaDTO(nota), contributiRichiesti, contributiPervenuti, tipoFirma, annullaTemplate, firmaAsincronaAvviata);			
	}
	
	/**
	 * Esegue la creazione del generic.
	 * 
	 * @param object
	 *            object
	 * @param idDocumento
	 *            identificativo documento
	 * @param tolf
	 *            tolf
	 * @param wobNumber
	 *            wob number
	 * @param documentQueueEnum
	 *            coda di lavoro
	 * @param idUtenteDestinatario
	 *            id utente destinatario
	 * @param idNodoDestinatario
	 *            id nodo utente destinatario
	 * @param bFirmaFig
	 *            flag firma fig
	 * @param motivazioneAssegnazione
	 *            motivazione assegnazione
	 * @param responses
	 *            response
	 * @param idFascicolo
	 *            identificativo fascicolo
	 * @param codaCorrente
	 *            coda corrente
	 * @param tipoAssegnazioneId
	 *            id tipo assegnazione
	 * @param flagIterManuale
	 *            flag iter manuale
	 * @param subject
	 *            subject
	 * @param elencoLibroFirma
	 *            elenco libro firma
	 * @param count
	 *            count
	 * @param dataCreazioneWF
	 *            data creazione del workflow
	 * @param flagRenderizzatoBool
	 *            flag renderizzato
	 * @param firmaCopiaConforme
	 *            firma copia conforme
	 * @param urgente
	 *            flag urgente
	 * @param stepName
	 *            step name
	 * @param notaDTO
	 *            nota
	 * @param contributiRichiesti
	 *            contributi richiesti
	 * @param contributiPervenuti
	 *            contributi pervenuti
	 * @param tipoFirma
	 *            tipo firma
	 * @param annullaTemplate
	 *            annulla template
	 * @param firmaAsincronaAvviata
	 *            flag firma asincrona avviata
	 * @return T
	 */
	abstract T createGeneric(VWWorkObject object, String idDocumento, TipoOperazioneLibroFirmaEnum tolf, String wobNumber,
			DocumentQueueEnum documentQueueEnum, Integer idUtenteDestinatario, Integer idNodoDestinatario,
			Boolean bFirmaFig, String motivazioneAssegnazione, String[] responses, Integer idFascicolo,
			String codaCorrente, Integer tipoAssegnazioneId, Integer flagIterManuale, String subject,
			String[] elencoLibroFirma, Integer count, String dataCreazioneWF, Boolean flagRenderizzatoBool,
			Boolean firmaCopiaConforme, Boolean urgente, String stepName, NotaDTO notaDTO, Integer contributiRichiesti, 
			Integer contributiPervenuti, int tipoFirma, boolean annullaTemplate, Boolean firmaAsincronaAvviata);
	
	
	protected abstract REDLogger getLogger();
}