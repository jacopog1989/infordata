package it.ibm.red.business.service.facade;

import java.io.Serializable;

/**
 * Facade del servizio Igepa.
 */
public interface IIgepaFacadeSRV extends Serializable {

	/**
	 * Inserisce il docuemento.
	 * @param oggetto
	 * @param nomeFile
	 * @param idNodoDestinatario
	 * @param idUtenteDestinatario
	 * @param numProtocollo
	 * @param dataProtocollo
	 * @param content
	 * @param contentType
	 * @param contentAll
	 * @param contentTypeAll
	 * @param nomeAll
	 * @param aliasMittente
	 * @return id del documento
	 */
	String insertDocumento(String oggetto, String nomeFile, int idNodoDestinatario, int idUtenteDestinatario,
			int numProtocollo, String dataProtocollo, byte[] content, String contentType, byte[] contentAll,
			String contentTypeAll, String nomeAll, String aliasMittente);

	/**
	 * Inserisce il documento OPF.
	 * 
	 * @param idNodoDest
	 * @param idUtenteDest
	 * @param content
	 * @param contentType
	 * @param numeroRichiesta
	 * @param annoRichiesta
	 * @param indiceClassificazione
	 * @param numeroConto
	 * @param sezione
	 * @param dataProtocollo
	 * @param numeroProtocollo
	 * @param tipodocumento
	 * @param tipoProcedimento
	 * @param oggetto
	 * @param enteMittente
	 * @param fileName
	 * @return id del documento
	 */
	String insertDocumentoOPF(int idNodoDest, Integer idUtenteDest, byte[] content, String contentType, Integer numeroRichiesta, String annoRichiesta, 
			String indiceClassificazione, String numeroConto, String sezione, String dataProtocollo, Integer numeroProtocollo, Integer tipodocumento,
			Integer tipoProcedimento, String oggetto, String enteMittente, String fileName);

	/**
	 * Allega la richiesta IGEPA.
	 * @param annoRichiesta
	 * @param numeroRichiesta
	 * @param numeroProtocollo
	 * @param oggetto
	 * @param tipologia
	 * @param filename
	 * @param content
	 * @param mimeType
	 * @return id del documento
	 */
	String allegaRichiestaIgepa(String annoRichiesta, Integer numeroRichiesta, Integer numeroProtocollo, String oggetto,
			Integer tipologia, String filename, byte[] content, String mimeType); 
}
