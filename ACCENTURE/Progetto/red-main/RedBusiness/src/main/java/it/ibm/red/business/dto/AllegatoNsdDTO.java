package it.ibm.red.business.dto;

/**
 * @author VINGENITO
 *
 */
public class AllegatoNsdDTO extends AbstractDTO {

	 
	private static final long serialVersionUID = 5774314838862570928L;

	/**
	 * Nome file.
	 */
	private String nomeFile;
	
	/**
	 * Content type.
	 */
	private String contentType;

	/**
	 * Content.
	 */
	private byte[] content;
	
	/**
	 * Identificativo documento.
	 */
	private String idDocumentale;
	
	/**
	 * @return the nomeFile
	 */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * @param nomeFile the nomeFile to set
	 */
	public void setNomeFile(final String nomeFile) {
		this.nomeFile = nomeFile;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(final String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return the content
	 */
	public byte[] getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(final byte[] content) {
		this.content = content;
	}

	/**
	 * @return the idDocumentale
	 */
	public String getIdDocumentale() {
		return idDocumentale;
	}
	
	/**
	 * @param idDocumentale the idDocumentale to set
	 */
	public void setIdDocumentale(final String idDocumentale) {
		this.idDocumentale = idDocumentale;
	}
}
