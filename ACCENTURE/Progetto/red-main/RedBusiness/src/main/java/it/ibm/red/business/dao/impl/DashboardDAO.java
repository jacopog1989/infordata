package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IDashboardDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Widget;

/**
 * DAO dashboard.
 */
@Repository
public class DashboardDAO extends AbstractDAO implements IDashboardDAO {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DashboardDAO.class.getName());

	/**
	 * @see it.ibm.red.business.dao.IDashboardDAO#salvaPreferenzeWdg(java.lang.Long,
	 *      java.lang.Long, java.lang.Long, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public void salvaPreferenzeWdg(final Long wdgId, final Long utenteId, final Long ruoloUtente, final Long posizione, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
				final String query = "INSERT INTO WDG_UTENTE (WDG_ID,UTENTE_ID,ROLE_ID,POSIZIONE) VALUES("
						+ wdgId + ','
						+ utenteId + ','
						+ ruoloUtente + ','
						+ posizione
						+ ")";
				ps = connection.prepareStatement(query);
				rs = ps.executeQuery();
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Errore durante il salvataggio delle preferenze", e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IDashboardDAO#rimuoviPreferenze(java.lang.Long,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public void rimuoviPreferenze(final Long wdgId, final Long utenteId, final Connection connection) {
		PreparedStatement ps = null;
		final String query = "DELETE FROM WDG_UTENTE WHERE WDG_ID = ? AND UTENTE_ID = ? ";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, wdgId.intValue());
			ps.setInt(2, utenteId.intValue());
			ps.executeUpdate();
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Errore durante la rimozione delle preferenze", e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IDashboardDAO#rimuoviPreferenzeWidget(java.lang.Long,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public void rimuoviPreferenzeWidget(final Long utenteId, final Long ruoloId, final Connection connection) {
		PreparedStatement ps = null;
		final String query = "DELETE FROM WDG_UTENTE WHERE UTENTE_ID = ? AND ROLE_ID = ? ";
		try {
			ps = connection.prepareStatement(query);
			ps.setLong(1, utenteId);
			ps.setLong(2, ruoloId); 
			ps.executeUpdate();
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Errore durante la rimozione delle preferenze", e);
		} finally {
			closeStatement(ps);
		}
	}
	
	/**
	 * @see it.ibm.red.business.dao.IDashboardDAO#getWidgetPreferenze(java.lang.Long,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<Widget> getWidgetPreferenze(final Long utenteId, final Long utenteRuolo, final Connection connection) {
		final List<Widget> widgets = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		try { 
			query += "SELECT DISTINCT wu.wdg_id," + "wu.utente_id," + "wr.role_id," + "wr.eliminabile," + "wu.posizione "
				  + "FROM wdg_utente wu JOIN wdgrole wr ON wu.wdg_id = wr.wdg_id "
				  + "WHERE wu.UTENTE_ID = ? AND wr.ROLE_ID = ? "
				  + "ORDER BY wu.posizione";
			ps = connection.prepareStatement(query);
			ps.setInt(1, utenteId.intValue());
			ps.setInt(2, utenteRuolo.intValue()); 
			rs = ps.executeQuery();
			while (rs.next()) {
				final Widget wdg = new Widget(rs.getInt("WDG_ID"), rs.getBoolean("ELIMINABILE"), rs.getInt("UTENTE_ID"), rs.getInt("ROLE_ID"), rs.getInt("POSIZIONE"));
				widgets.add(wdg);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Errore durante il caricamento dei widget", e);
		} finally {
			closeStatement(ps, rs);
		}
		return widgets;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IDashboardDAO#getWidgetRuolo(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public List<Widget> getWidgetRuolo(final Long idRuolo, final Connection connection) {
		final List<Widget> widgets = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try { 
			final String query = "select wdg_id," + "role_id," + "eliminabile " 
								+ "from wdgrole " 
								+ "where role_id = ?";
			ps = connection.prepareStatement(query);
			ps.setInt(1, idRuolo.intValue());
			rs = ps.executeQuery();
			while (rs.next()) {
				final Widget wdg = new Widget(rs.getInt("WDG_ID"), rs.getInt("ROLE_ID"), rs.getBoolean("ELIMINABILE"));
				widgets.add(wdg);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Errore durante il caricamento dei widget in base al ruolo", e);
		} finally {
			closeStatement(ps, rs);
		}
		return widgets;
	}

}
