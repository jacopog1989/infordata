package it.ibm.red.business.dto;
 
/**
 * DTO per la gestione delle assegnazioni competenza NPS.
 */
public class AssCompetenzaDestNpsDfWSDTO extends AbstractDTO {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = -1246918872654012899L;
	
	/**
	 * Codice amministrazione.
	 */
	private String codiceAmm;
	
	/**
	 * Denominazione amministrazione.
	 */
	private String denominazioneAmm;
	
	/**
	 * Codice aoo.
	 */
	private String codiceAoo;
	
	/**
	 * Denominazione aoo.
	 */
	private String denominazioneAoo;
	
	/**
	 * Codice UO.
	 */
	private String codUO;
	
	/**
	 * Denominazione UO.
	 */
	private String denominazioneUO;

	/**
	 * Chiave esterna operatore.
	 */
	private String chiaveEsternaOperatore;
	
	/**
	 * Nome.
	 */
	private String nome;
	
	/**
	 * Cognome.
	 */
	private String cognome;
	
	/**
	 * Email.
	 */
	private String email;
	
	/**
	 * Codice fiscale.
	 */
	private String codFiscale;

	/**
	 * @return the codiceAmm
	 */
	public String getCodiceAmm() {
		return codiceAmm;
	}

	/**
	 * @param codiceAmm the codiceAmm to set
	 */
	public void setCodiceAmm(final String codiceAmm) {
		this.codiceAmm = codiceAmm;
	}

	/**
	 * @return the denominazioneAmm
	 */
	public String getDenominazioneAmm() {
		return denominazioneAmm;
	}

	/**
	 * @param denominazioneAmm the denominazioneAmm to set
	 */
	public void setDenominazioneAmm(final String denominazioneAmm) {
		this.denominazioneAmm = denominazioneAmm;
	}

	/**
	 * @return the codiceAoo
	 */
	public String getCodiceAoo() {
		return codiceAoo;
	}

	/**
	 * @param codiceAoo the codiceAoo to set
	 */
	public void setCodiceAoo(final String codiceAoo) {
		this.codiceAoo = codiceAoo;
	}

	/**
	 * @return the denominazioneAoo
	 */
	public String getDenominazioneAoo() {
		return denominazioneAoo;
	}

	/**
	 * @param denominazioneAoo the denominazioneAoo to set
	 */
	public void setDenominazioneAoo(final String denominazioneAoo) {
		this.denominazioneAoo = denominazioneAoo;
	}

	/**
	 * @return the codUO
	 */
	public String getCodUO() {
		return codUO;
	}

	/**
	 * @param codUO the codUO to set
	 */
	public void setCodUO(final String codUO) {
		this.codUO = codUO;
	}

	/**
	 * @return the denominazioneUO
	 */
	public String getDenominazioneUO() {
		return denominazioneUO;
	}

	/**
	 * @param denominazioneUO the denominazioneUO to set
	 */
	public void setDenominazioneUO(final String denominazioneUO) {
		this.denominazioneUO = denominazioneUO;
	}

	/**
	 * @return the chiaveEsternaOperatore
	 */
	public String getChiaveEsternaOperatore() {
		return chiaveEsternaOperatore;
	}

	/**
	 * @param chiaveEsternaOperatore the chiaveEsternaOperatore to set
	 */
	public void setChiaveEsternaOperatore(final String chiaveEsternaOperatore) {
		this.chiaveEsternaOperatore = chiaveEsternaOperatore;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(final String nome) {
		this.nome = nome;
	}

	/**
	 * @return the cognome
	 */
	public String getCognome() {
		return cognome;
	}

	/**
	 * @param cognome the cognome to set
	 */
	public void setCognome(final String cognome) {
		this.cognome = cognome;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * @return the codFiscale
	 */
	public String getCodFiscale() {
		return codFiscale;
	}

	/**
	 * @param codFiscale the codFiscale to set
	 */
	public void setCodFiscale(final String codFiscale) {
		this.codFiscale = codFiscale;
	}
	  
	
	
}



