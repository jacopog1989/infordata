package it.ibm.red.business.enums;

/**
 * The Enum FontFamilyEnum.
 *
 * @author CPIERASC
 * 
 *         Enum utilizzato per specificare il font da utilizzare in fase di
 *         firma.
 */
public enum FontFamilyEnum {
	
	/**
	 * Font courier.
	 */
	COURIER("Courier"),
	
	/**
	 * Font courier bold.
	 */
	COURIER_BOLD("Courier-Bold"),
	
	/**
	 * Font courier italic.
	 */
	COURIER_OBLIQUE("Courier-Oblique"),
	
	/**
	 * Font courier bold italic.
	 */
	COURIER_BOLD_OBLIQUE("Courier-BoldOblique"),
	
	/**
	 * Font helvetica.
	 */
	HELVETICA("Helvetica"),
	
	/**
	 * Font helvetica grassetto.
	 */
	HELVETICA_BOLD("Helvetica-Bold"),
	
	/**
	 * Font helvetica italico.
	 */
	HELVETICA_OBLIQUE("Helvetica-Oblique"),
	
	/**
	 * Font helvetica grassetto italico.
	 */
	HELVETICA_BOLD_OBLIQUE("Helvetica-BoldOblique"),
	
	/**
	 * Valore.
	 */
	TIMES_ROMAN("Times-Roman"),

	/**
	 * Valore.
	 */
	TIMES_BOLD("Times-Bold"),

	/**
	 * Valore.
	 */
	TIMES_ITALIC("Times-Italic"),

	/**
	 * Valore.
	 */
	TIMES_ITALIC_BOLD("Times-BoldItalic"),

	/**
	 * Valore.
	 */
	ZAPFDINGBATS("ZapfDingbats"),
	
	/**
	 * Font symbol.
	 */
	SYMBOL("Symbol");

	/**
	 * Valore associato.
	 */
	private String value;

	/**
	 * Costruttore.
	 * 
	 * @param inValue	valore associato
	 */
	FontFamilyEnum(final String inValue) {
		this.value = inValue;
	}

	/**
	 * Getter valore associato.
	 * 
	 * @return	valore associato
	 */
	public String getValue() {
		return value;
	}

}
