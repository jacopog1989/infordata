package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.service.ITipoProcedimentoSRV;

/**
 * The Class TipoProcedimentoSRV.
 *
 * @author CPIERASC
 * 
 *         Servizio per la gestione del tipo procedimento.
 */
@Service
@Component
public class TipoProcedimentoSRV extends AbstractService implements ITipoProcedimentoSRV {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TipoProcedimentoSRV.class.getName());
	
	/**
	 * Tipo procedimento DAO.
	 */
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;

	
	/**
	 * @return
	 */
	@Override
	public final Collection<TipoProcedimentoDTO> getAll() {
		Collection<TipoProcedimentoDTO> outputDTO = new ArrayList<>();
		Connection connection =  null;
		
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			outputDTO = getAll(connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei tipi di procedimenti ", e);
		} finally {
			closeConnection(connection);
		}
		
		return outputDTO;
	}
	
	
	@Override
	public final Collection<TipoProcedimentoDTO> getAll(final Connection connection) {
		final Collection<TipoProcedimentoDTO> outputDTO = new ArrayList<>();

		final Collection<TipoProcedimento> tipi = tipoProcedimentoDAO.getAll(connection);
		if (tipi != null) {
			for (final TipoProcedimento tp : tipi) {
				outputDTO.add(new TipoProcedimentoDTO(tp));
			}
		}
		
		return outputDTO;
	}
	
	/**
	 * @see it.ibm.red.business.service.ITipoProcedimentoSRV#getWorkflowSubjectProcessoGenericoUscita(java.util.Collection).
	 */
	@Override
	public String getWorkflowSubjectProcessoGenericoUscita(final Collection<TipoProcedimentoDTO> tipiProcedimento) {
		String workflowSubject = Constants.EMPTY_STRING;
		
		for (final TipoProcedimentoDTO tp : tipiProcedimento) {
			if (tp.getFlagGenericoEntrata() == 0 && tp.getFlagGenericoUscita() == 1) {
				workflowSubject = tp.getWorkflowSubject();
				break;
			}
		}
		
		return workflowSubject;
	}
	
	/**
	 * @see it.ibm.red.business.service.ITipoProcedimentoSRV#getWorkflowSubjectProcessoGenericoEntrata(java.util.Collection).
	 */
	@Override
	public String getWorkflowSubjectProcessoGenericoEntrata(final Collection<TipoProcedimentoDTO> tipiProcedimento) {
		String workflowSubject = Constants.EMPTY_STRING;
		
		for (final TipoProcedimentoDTO tp : tipiProcedimento) {
			if (tp.getFlagGenericoEntrata() == 1 && tp.getFlagGenericoUscita() == 0) {
				workflowSubject = tp.getWorkflowSubject();
				break;
			}
		}
		
		return workflowSubject;
	}
	
	/**
	 * @see it.ibm.red.business.service.ITipoProcedimentoSRV#getWorkflowNameProcessoGenericoEntrata(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public String getWorkflowNameProcessoGenericoEntrata(final Long idAoo, final Connection connection) {
		String workflowName = Constants.EMPTY_STRING;
		
		final Collection<TipoProcedimentoDTO> tipiProcedimento = getTipiProcedimentoByIdAoo(idAoo, connection);
		
		for (final TipoProcedimentoDTO tp : tipiProcedimento) {
			if (tp.getFlagGenericoEntrata() == 1 && tp.getFlagGenericoUscita() == 0) {
				workflowName = tipoProcedimentoDAO.getWorkflowNameByIdTipoProcedimento(tp.getTipoProcedimentoId(), connection);
				break;
			}
		}
		
		return workflowName;
	}
	
	/**
	 * @see it.ibm.red.business.service.ITipoProcedimentoSRV#getWorkflowSubjectProcessoGenericoUscita(java.lang.Long).
	 */
	@Override
	public String getWorkflowSubjectProcessoGenericoUscita(final Long idAoo) {
		return getWorkflowSubjectProcessoGenericoUscita(getTipiProcedimentoByIdAoo(idAoo));
	}
	
	/**
	 * @see it.ibm.red.business.service.ITipoProcedimentoSRV#getWorkflowSubjectProcessoGenericoEntrata(java.lang.Long).
	 */
	@Override
	public String getWorkflowSubjectProcessoGenericoEntrata(final Long idAoo) {
		return getWorkflowSubjectProcessoGenericoEntrata(getTipiProcedimentoByIdAoo(idAoo));
	}
	
	
	/**
	 * 
	 * @param idAoo
	 * @return
	 */
	@Override
	public Collection<TipoProcedimentoDTO> getTipiProcedimentoByIdAoo(final Long idAoo) {
		Connection connection = null;
		Collection<TipoProcedimentoDTO> output = new ArrayList<>();
		
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			output = getTipiProcedimentoByIdAoo(idAoo, connection);
		} catch (final Exception e) {
			LOGGER.error(e);
		} finally {
			closeConnection(connection);
		}
		
		return output;
	}
	
	/**
	 * Restituisce le tipologia procedimento configurate e valide per un'AOO
	 * identificate dall' <code> idAoo </code>.
	 * 
	 * @see it.ibm.red.business.service.ITipoProcedimentoSRV#getTipiProcedimentoByIdAoo(java.lang.Long,
	 *      java.sql.Connection).
	 * 
	 * @param idAoo
	 *            identificativo AOO
	 * @param connection
	 *            connessione al database
	 * @return collezione delle tipologia procedimento
	 */
	@Override
	public Collection<TipoProcedimentoDTO> getTipiProcedimentoByIdAoo(final Long idAoo, final Connection connection) {
		final Collection<TipoProcedimentoDTO> output = new ArrayList<>();
		final Collection<TipoProcedimentoDTO> tipi = getAll(connection);
		
		for (final TipoProcedimentoDTO t : tipi) {
			if (t.getIdAoo() == idAoo) {
				output.add(t);
			}
		}
		
		return output;
	}

	/**
	 * Restituisce il tipo procedimento identificato dall'
	 * <code> idProcedimento </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.ITipoProcedimentoFacadeSRV#getTipoProcedimentoById(java.lang.Integer).
	 * @param idProcedimento
	 *            identificativo tipo procedimento
	 * @return tipo porcedimento recuperato se esistente, null altrimenti
	 */
	@Override
	public TipoProcedimentoDTO getTipoProcedimentoById(final Integer idProcedimento) {
		Connection connection = null;
		TipoProcedimentoDTO output = null;
		
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			output = getTipoProcedimentoById(connection, idProcedimento);
		} catch (final Exception e) {
			LOGGER.error(e);
		} finally {
			closeConnection(connection);
		}
		
		return output;
	}

	
	/**
	 * Restituisce il tipo procedimento identificato dall'
	 * <code> idProcedimento </code>.
	 * 
	 * @param idProcedimento
	 *            identificativo tipo procedimento
	 * @param connection
	 *            connessione al database
	 * @return tipo porcedimento recuperato se esistente, null altrimenti
	 */
	@Override
	public TipoProcedimentoDTO getTipoProcedimentoById(final Connection connection, final Integer idProcedimento) {
		return new TipoProcedimentoDTO(tipoProcedimentoDAO.getTPCompletobyId(connection, idProcedimento));
	}

	
	/**
	 * Restituisce una collection contenente tutti i tipi procedimento validi per
	 * un'AOO identificata dall' <code> idAoo </code> il cui tipo categoria è uguale
	 * a <code> tipoAcquisizione </code>. Quando la tipologia esiste per più di un
	 * tipo categoria questa viene considerata solo una volta.
	 * 
	 * @see it.ibm.red.business.service.ITipoProcedimentoSRV#getTipiProcedimentoByIdAooFiltered(java.lang.Long,
	 *      it.ibm.red.business.enums.TipoCategoriaEnum).
	 * @param idAoo
	 *            identificativo Area organizzativa
	 * @param tipoAcquisizione
	 *            tipo categoria specificato, @see TipoCategoriaEnum
	 * @return collezione dei tipi procedimento recuperati
	 */
	@Override
	public Collection<TipoProcedimentoDTO> getTipiProcedimentoByIdAooFiltered(final Long idAoo, final TipoCategoriaEnum tipoAcquisizione) {
		Connection connection = null;
		Collection<TipoProcedimentoDTO> output = new ArrayList<>();
		final Collection<TipoProcedimentoDTO> results = new ArrayList<>();
		final Collection<String> resultsToString = new ArrayList<>();
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			output = getTipiProcedimentoByIdAoo(idAoo, connection);
			for (final TipoProcedimentoDTO tipo : output) {
				// Serve ad impedire di inserire due volte un procedimento che esiste in entrata e in uscita
				if (!resultsToString.contains(tipo.getDescrizione())) {
					if (tipoAcquisizione.equals(TipoCategoriaEnum.ENTRATA_ED_USCITA)) {
						if (isEntrataUscita(output, tipo)) {
							results.add(tipo);
							resultsToString.add(tipo.getDescrizione());
						}
					} else if (tipoAcquisizione.equals(TipoCategoriaEnum.ENTRATA)) {
						if (tipo.getFlagGenericoEntrata() == 1 && tipo.getFlagGenericoUscita() == 0) {
							results.add(tipo);
							resultsToString.add(tipo.getDescrizione());
						}
					} else if (tipoAcquisizione.equals(TipoCategoriaEnum.USCITA) 
							&& tipo.getFlagGenericoEntrata() == 0 && tipo.getFlagGenericoUscita() == 1) {
						results.add(tipo);
						resultsToString.add(tipo.getDescrizione());
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
		} finally {
			closeConnection(connection);
		}
		
		return results;
	}
	
	
	/**
	 * Restituisce le tipologie dei procedimenti associati ad una tipologia
	 * documento identificata dall' <code> idTipologiaDocumento </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.ITipoProcedimentoFacadeSRV#getTipiProcedimentoByTipologiaDocumento(java.lang.Integer).
	 * @param idTipologiaDocumento
	 *            identificativo tipologia documento
	 * @return lista delle tipologia procedimento recuperate
	 */
	@Override
	public List<TipoProcedimento> getTipiProcedimentoByTipologiaDocumento(final Integer idTipologiaDocumento) {
		List<TipoProcedimento> output = null;
		
		if (idTipologiaDocumento != null) {
			Connection connection = null;
			
			try {
				connection = setupConnection(getDataSource().getConnection(), false);
				
				output = tipoProcedimentoDAO.getTipiProcedimentoByTipologiaDocumento(connection, idTipologiaDocumento);
			} catch (final SQLException e) {
				LOGGER.error(e.getMessage(), e);
			} finally {
				closeConnection(connection);
			}
		}
		
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.ITipoProcedimentoSRV#getTipiProcedimentoByTipologiaDocumento(java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public List<TipoProcedimento> getTipiProcedimentoByTipologiaDocumento(final Integer idTipologiaDocumento, final Connection connection) {
		List<TipoProcedimento> output = null;
		
		if (idTipologiaDocumento != null) {
			output = tipoProcedimentoDAO.getTipiProcedimentoByTipologiaDocumento(connection, idTipologiaDocumento);
		}
		
		return output;
	}
	
	/**
	 * Esegue il salvataggio sulla base dati di una tipologia procedimento.
	 * 
	 * @see it.ibm.red.business.service.facade.ITipoProcedimentoFacadeSRV#save(it.ibm.red.business.dto.TipoProcedimentoDTO).
	 * @param procedimento
	 *            caratteristiche del procedimento da salvare
	 * @return identificativo del tipo procedimento salvato
	 */
	@Override
	public Long save(final TipoProcedimentoDTO procedimento) {
		Long idProcedimento = 0L;
		if (procedimento != null) {
			Connection connection = null;
			try {
				connection = setupConnection(getDataSource().getConnection(), false);
				idProcedimento = tipoProcedimentoDAO.save(procedimento, connection);
			} catch (final Exception e) {
				LOGGER.error(e.getMessage(), e);
			} finally {
				closeConnection(connection);
			}
		}
		return idProcedimento;
	}
	
	/**
	 * Restituisce l'identificativo del tipo procedimento associato al procedimento
	 * caratterizzate dalla descrizione definita da <code> descrizione </code> e
	 * associato alla tipologia documento definita da
	 * <code> idTipologiaDocumento </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.ITipoProcedimentoFacadeSRV#getTipiProcedimentoByTipologiaDocumento(java.lang.Integer).
	 * @param descrizione
	 *            descrizione tipo procedimento
	 * @param idTipologiaDocumento
	 *            identificativo univoco tipologia documento
	 * @return identificativo tipo procedimento recuperato se esistente,
	 *         <code> null </code> altrimenti
	 */
	@Override
	public final Long getIdTipoProcedimentoByDescrizioneAndTipologiaDocumento(final String descrizione, final Integer idTipologiaDocumento) {
		Long output = null;
		
		if (idTipologiaDocumento != null) {
			Connection con = null;
			
			try {
				con = setupConnection(getDataSource().getConnection(), false);
				
				output = tipoProcedimentoDAO.getIdTipoProcedimentoByDescrizioneAndTipologiaDocumento(descrizione, idTipologiaDocumento, con);
				
			} catch (final SQLException e) {
				LOGGER.error("Errore nel recupero del tipo procedimento: " + descrizione + " associato alla tipologia documento: " + idTipologiaDocumento, e);
				throw new RedException("Errore nel recupero del tipo procedimento: " + descrizione + " associato alla tipologia documento: " + idTipologiaDocumento, e);
			} finally {
				closeConnection(con);
			}
		}
		
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITipoProcedimentoFacadeSRV#getDescTipoProcedimentoById(java.lang.Integer).
	 */
	@Override
	public String getDescTipoProcedimentoById(final Integer idTipoProcedimento) {
		String output = null;
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			output = tipoProcedimentoDAO.getTPbyId(con, idTipoProcedimento).getDescrizione();
		} catch (final SQLException e) {
			LOGGER.error("Errore nel recupero del tipo procedimento", e);
			throw new RedException("Errore nel recupero del tipo procedimento", e);
		} finally {
			closeConnection(con);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.ITipoProcedimentoSRV#getProcedimentoByFlags(java.lang.Long,
	 *      int, int).
	 */
	@Override
	public Collection<TipoProcedimentoDTO> getProcedimentoByFlags(final Long idAoo, final int flagGenericoEntrata, final int flagGenericoUscita) {
		Connection connection = null;
		Collection<TipoProcedimentoDTO> output = new ArrayList<>();
		final Collection<TipoProcedimentoDTO> results = new ArrayList<>();
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			output = getTipiProcedimentoByIdAoo(idAoo, connection);
			
			for (final TipoProcedimentoDTO tipo : output) {
				if (tipo.getFlagGenericoEntrata() == flagGenericoEntrata && tipo.getFlagGenericoUscita() == flagGenericoUscita) {
					results.add(tipo);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
		} finally {
			closeConnection(connection);
		}
		
		return results;
	}
	
	/**
	 * Metodo che consente di verificare il tipo categoria del tipo procedimento.
	 * Il metodo verifica il numero di occorrenze del procedimento in una lista, se è entrata e uscita esisterà due volte
	 * 
	 * @param procedimento 	 	Il procedimento per cui occorre conoscere se è un tipo procedimento Entrata e Uscita
	 * @param tipi 				Occorre la lista di tutti i procedimenti perché un procedimento entrata e uscita è un procedimento che
	 * 							esiste in duplice copia, uno gestisce l'entrata e uno l'uscita.
	 * */
	private static boolean isEntrataUscita(final Collection<TipoProcedimentoDTO> tipi, final TipoProcedimentoDTO procedimento) {
		final List<String> descrTipi = new ArrayList<>();
		
		for (final TipoProcedimentoDTO p : tipi) {
			descrTipi.add(p.getDescrizione());
		}
		
		int numeroProcedimentiOmonimi = 0;
		for (final String descr : descrTipi) {
			if (procedimento.getDescrizione().equals(descr)) {
				numeroProcedimentiOmonimi++;
			}
		}
		
		return (numeroProcedimentiOmonimi == 2);
	}
	
}
