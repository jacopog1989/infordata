package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.Date;

import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.service.facade.IProtocolliEmergenzaFacadeSRV;

/**
 * The Interface IProtocolliEmergenzaSRV.
 *
 * @author a.dilegge
 * 
 *         Servizio gestione protocolli di emergenza.
 */
public interface IProtocolliEmergenzaSRV extends IProtocolliEmergenzaFacadeSRV {

	/**
	 * Registra sulla base dati le informazioni di emergenza acquisite per il documento in input
	 * 
	 * @param idDocumento
	 * @param idAoo
	 * @param codiceAoo
	 * @param numeroProtocolloEmergenza
	 * @param annoProtocolloEmergenza
	 * @param dataProtocolloEmergenza
	 * @param tipoProtocollo
	 * @param oggetto
	 * @param tipologiaDocumento
	 * @param conn
	 * @return restituisce le informazioni relative al protocollo di emergenza salvate
	 */
	ProtocolloNpsDTO registraInfoEmergenza(String idDocumento, Long idAoo, String codiceAoo, 
			Integer numeroProtocolloEmergenza, Integer annoProtocolloEmergenza, Date dataProtocolloEmergenza, 
			Integer tipoProtocollo, String oggetto, String tipologiaDocumento, Connection conn);
	
	/**
	 * Verifica se il protocollo in input è un emergenza non ancora riconciliata con l'ufficiale
	 * 
	 * @param idProtocolloEmergenza
	 * @param connection
	 * @return true se esiste, false altrimenti
	 */
	boolean isEmergenzaNotUpdated(String idProtocolloEmergenza, Connection connection);
	
	/**
	 * Verifica se il protocollo d'emergenza in input è già stato acquisito in precedenza
	 * 
	 * @param numeroProtocolloEmergenza
	 * @param annoProtocolloEmergenza
	 * @param idAoo
	 * @param connection
	 * @return true se esiste, false altrimenti
	 */
	boolean isProtocolloEmergenzaPresent(Integer numeroProtocolloEmergenza, Integer annoProtocolloEmergenza, Long idAoo, Connection connection);
	
	
	/**
	 * Recupera dalla base dati le informazioni emergenza per un dato documento e le inserisce in un DTO di protocollo NPS.
	 * 
	 * @param idDocumento
	 * @param oggetto
	 * @param tipologiaDocumento
	 * @param idAoo
	 * @param conn
	 * @return
	 */
	ProtocolloNpsDTO getInfoEmergenzaPerProtocolloNps(String idDocumento, String oggetto, String tipologiaDocumento, Long idAoo, Connection conn);
	
}