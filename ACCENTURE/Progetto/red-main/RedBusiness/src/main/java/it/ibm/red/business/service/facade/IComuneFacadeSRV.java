package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.ComuneDTO;

/**
 * Facade del servizio di gestione comuni.
 */
public interface IComuneFacadeSRV extends Serializable {

	/**
	 * Ottiene tutti i comuni configurati sul database.
	 * 
	 * @return Comuni configurati.
	 */
	Collection<ComuneDTO> getAll();

	/**
	 * Ottiene il comune identificato dall' {@code id}.
	 * 
	 * @param id
	 *            Identificativo del comune recuperato.
	 * @return Comune recuperato.
	 */
	ComuneDTO get(String id);

	/**
	 * Ottiene i comuni dalla provincia identificata dall' {@code idProvincia}.
	 * 
	 * @param idProvincia
	 *            Identificativo della provincia.
	 * @param query
	 * @return Lista di comuni recuperati.
	 */
	List<ComuneDTO> getComuni(Long idProvincia, String query);

}
