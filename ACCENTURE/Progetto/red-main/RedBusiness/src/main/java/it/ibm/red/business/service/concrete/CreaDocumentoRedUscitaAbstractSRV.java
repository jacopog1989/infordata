package it.ibm.red.business.service.concrete;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants.Modalita;
import it.ibm.red.business.dao.ICasellaPostaleDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.TemplateMetadatoDTO;
import it.ibm.red.business.dto.TemplateMetadatoValueDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.FunzionalitaEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.enums.TipoAllaccioEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.enums.TipoNotificaAzioneNPSEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.NodoUtenteRuolo;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.ICreaDocumentoRedUscitaSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.IMailSRV;
import it.ibm.red.business.service.IRubricaSRV;
import it.ibm.red.business.service.ITemplateDocUscitaSRV;
import it.ibm.red.business.service.ITipologiaDocumentoSRV;
import it.ibm.red.business.utils.DateUtils;

/**
 * @author m.crescentini
 * 
 *         Servizio per la creazione di documenti in uscita.
 *
 */
@Service
public abstract class CreaDocumentoRedUscitaAbstractSRV extends CreaDocumentoRedAbstractSRV implements ICreaDocumentoRedUscitaSRV {

	private static final long serialVersionUID = 2990278329136142444L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CreaDocumentoRedUscitaAbstractSRV.class.getName());

	/**
	 * Service.
	 */
	@Autowired
	private ITemplateDocUscitaSRV templateDocUscitaSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ITipologiaDocumentoSRV tipologiaDocumentoSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IRubricaSRV rubricaSRV;

	/**
	 * Dao.
	 */
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IMailSRV mailSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IFascicoloSRV fascicoloSRV;

	/**
	 * Dao.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private ICasellaPostaleDAO casellaPostaleDAO;

	/**
	 * @see it.ibm.red.business.service.ICreaDocumentoUscitaSRV#
	 *      creaDocumentoRedUscitaRispostaDaTemplate(java.lang.String,
	 *      it.ibm.red.business.dto.FascicoloDTO, java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 *      java.util.List, java.lang.String, java.lang.String, java.lang.Long,
	 *      java.lang.Long, java.lang.Long,
	 *      it.ibm.red.business.persistence.model.Aoo,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public EsitoSalvaDocumentoDTO creaDocumentoRedUscitaRispostaDaTemplate(final String idDocumentoEntrata, final UtenteDTO utenteProtocollatore,
			final String indiceClassificazione, final String guidTemplate, final String nomeTemplate, final String oggettoTemplate, final String testoTemplate,
			final List<DestinatarioRedDTO> destinatari, final String casella, final String mittenteMail, final String oggetto, final Long idUtenteAssegnatario,
			final Long idRuoloAssegnatario, final Long idUfficioAssegnatario, final Aoo aoo, final IFilenetCEHelper fceh, final Connection con) {
		LOGGER.info("creaDocumentoRedUscitaRispostaDaTemplate -> ID documento entrata [" + idDocumentoEntrata + "], oggetto documento uscita [" + oggetto + "]");
		// Recupero dell'utente assegnatario
		final UtenteDTO utenteAssegnatario = getUtente(idUtenteAssegnatario, idRuoloAssegnatario, idUfficioAssegnatario, con);

		// Recupero del documento da creare
		final DetailDocumentRedDTO documento = getDocumentoRedInUscitaRispostaDaTemplate(idDocumentoEntrata, utenteProtocollatore, indiceClassificazione, guidTemplate,
				nomeTemplate, oggettoTemplate, testoTemplate, utenteAssegnatario, destinatari, casella, oggetto, aoo.getIdAoo(), fceh, con);

		// Recupero dei parametri di creazione
		final SalvaDocumentoRedParametriDTO parametri = getParametriUscita();

		return creaDocumentoRed(documento, parametri, utenteProtocollatore);
	}

	/**
	 * @see it.ibm.red.business.service.ICreaDocumentoUscitaSRV#
	 *      creaDocumentoRedUscitaDaContent(byte[], java.lang.String,
	 *      java.lang.String, it.ibm.red.business.dto.UtenteDTO, java.lang.String,
	 *      java.lang.Integer, java.lang.Integer, java.lang.Integer,
	 *      it.ibm.red.business.enums.TipoAssegnazioneEnum,
	 *      it.ibm.red.business.enums.MomentoProtocollazioneEnum, java.util.List,
	 *      java.util.List, java.util.List, java.lang.String, java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.Long,
	 *      java.lang.Long, it.ibm.red.business.persistence.model.Aoo,
	 *      java.sql.Connection).
	 */
	@Override
	public EsitoSalvaDocumentoDTO creaDocumentoRedUscitaDaContent(final byte[] content, final String contentType, final String nomeFile, final UtenteDTO utenteCreatore,
			final FascicoloDTO fascicoloProcedimentale, final String indiceClassificazione, final Integer idTipologiaDocumento, final Integer idTipoProcedimento,
			final Integer idIterApprovativo, final TipoAssegnazioneEnum tipoAssegnazione, final MomentoProtocollazioneEnum momentoProtocollazione,
			final List<AllegatoDTO> allegati, final List<RispostaAllaccioDTO> allacci, final List<DestinatarioRedDTO> contattiDestinatari, final String oggetto,
			final String oggettoMail, final String testoMail, final String casellaMittenteMail, final String mittenteMail, final Long idUtenteAssegnatario,
			final Long idUfficioAssegnatario, final String codiceFlusso, final Aoo aoo, final Connection con) {
		LOGGER.info("creaDocumentoRedUscitaDaContent -> Nome file [" + nomeFile + "], oggetto [" + oggetto + "], iter [" + idIterApprovativo + "], ufficio creatore ["
				+ utenteCreatore.getIdUfficio() + "], utente creatore [" + utenteCreatore.getId() + "]");

		// Destinatari
		final List<DestinatarioRedDTO> destinatariRed = new ArrayList<>();
		final StringBuilder mailDestinatari = new StringBuilder();
		final StringBuilder mailDestinatariCC = new StringBuilder();
		gestisciDestinatariRed(destinatariRed, mailDestinatari, mailDestinatariCC, contattiDestinatari, utenteCreatore, con);

		// Documento principale
		final FileDTO contentPrincipale = new FileDTO(nomeFile, content, contentType);

		// Recupero del documento da creare
		final DetailDocumentRedDTO documento = getDocumentoRedInUscita(utenteCreatore.getIdUfficio(), oggetto, contentPrincipale, idUtenteAssegnatario, idUfficioAssegnatario,
				fascicoloProcedimentale, indiceClassificazione, idTipologiaDocumento, idTipoProcedimento, idIterApprovativo, tipoAssegnazione, momentoProtocollazione,
				allegati, allacci, destinatariRed, mailDestinatari.toString(), mailDestinatariCC.toString(), casellaMittenteMail, oggettoMail, testoMail, codiceFlusso, null,
				null, null, null, null, null, null, con);

		// Recupero dei parametri di creazione
		final SalvaDocumentoRedParametriDTO parametri = getParametriUscita();

		return creaDocumentoRed(documento, parametri, utenteCreatore);
	}

	/**
	 * @param idDocumentoEntrata
	 * @param utenteProtocollatore
	 * @param guidTemplate
	 * @param nomeTemplate
	 * @param oggettoTemplate
	 * @param testoTemplate
	 * @param utenteResponsabile
	 * @param indiceClassificazione
	 * @param contattiDestinatari
	 * @param casellaMail
	 * @param oggetto
	 * @param idAoo
	 * @param fceh
	 * @param con
	 * @return
	 */
	private DetailDocumentRedDTO getDocumentoRedInUscitaRispostaDaTemplate(final String idDocumentoEntrata, final UtenteDTO utenteProtocollatore,
			final String indiceClassificazione, final String guidTemplate, final String nomeTemplate, final String oggettoTemplate, final String testoTemplate,
			final UtenteDTO utenteResponsabile, final List<DestinatarioRedDTO> contattiDestinatari, final String casellaMail, final String oggetto, final Long idAoo,
			final IFilenetCEHelper fceh, final Connection con) {
		// Destinatari
		final List<DestinatarioRedDTO> destinatariRed = new ArrayList<>();
		final StringBuilder mailDestinatari = new StringBuilder();
		final StringBuilder mailDestinatariCC = new StringBuilder();
		gestisciDestinatariRed(destinatariRed, mailDestinatari, mailDestinatariCC, contattiDestinatari, utenteProtocollatore, con);

		// Documento principale da template
		final FileDTO contentPrincipale = getDocumentoPrincipaleUscita(guidTemplate, nomeTemplate, testoTemplate, utenteResponsabile, destinatariRed, oggettoTemplate, con);

		// Fascicolo
		final FascicoloDTO fascicolo = fascicoloSRV.getFascicoloProcedimentale(idDocumentoEntrata, idAoo.intValue(), utenteProtocollatore);

		// Recupero dell'ufficio assegnatario
		final Nodo ufficioUtente = nodoDAO.getNodo(utenteResponsabile.getIdUfficio(), con);

		// Tipologia documento di default
		final Integer idTipologiaDocumento = tipologiaDocumentoSRV.getTipologiaDocumentalePredefinita(ufficioUtente.getAoo().getIdAoo(), ufficioUtente.getAoo().getCodiceAoo(),
				TipoCategoriaEnum.USCITA, con);

		// Tipo procedimento di default
		final List<TipoProcedimento> tipiProcedimentoByTipologiaDocumento = tipoProcedimentoDAO.getTipiProcedimentoByTipologiaDocumento(con, idTipologiaDocumento);
		final Integer idTipoProcedimento = (int) tipiProcedimentoByTipologiaDocumento.get(0).getTipoProcedimentoId();

		// Allegati - Recupero mail originale da inserire come allegato
		final List<AllegatoDTO> allegati = new ArrayList<>();
		final AllegatoDTO mailOriginale = getMailOriginale(idDocumentoEntrata, idAoo, idTipologiaDocumento, TipoCategoriaEnum.USCITA, utenteResponsabile);
		if (mailOriginale != null) {
			allegati.add(mailOriginale);
		}

		// Allaccio
		final List<RispostaAllaccioDTO> allacci = new ArrayList<>();
		final Document allaccioFilenet = fceh.getDocumentByDTandAOO(idDocumentoEntrata, idAoo);
		final DetailDocumentRedDTO detailAllaccioDTO = TrasformCE.transform(allaccioFilenet, TrasformerCEEnum.FROM_DOCUMENTO_TO_LIGHT_DETAIL_RED);

		final RispostaAllaccioDTO allaccio = new RispostaAllaccioDTO(null, idDocumentoEntrata, TipoAllaccioEnum.RISPOSTA.getTipoAllaccioId(), 1, true);
		allaccio.setFascicolo(fascicolo);
		allaccio.setDocumentoDaChiudere(true);
		allaccio.setDocument(detailAllaccioDTO);
		allacci.add(allaccio);

		return getDocumentoRedInUscita(utenteProtocollatore.getIdUfficio(), oggetto, contentPrincipale, utenteResponsabile.getId(), utenteResponsabile.getIdUfficio(),
				fascicolo, indiceClassificazione, idTipologiaDocumento, idTipoProcedimento, IterApprovativoDTO.ITER_FIRMA_MANUALE, TipoAssegnazioneEnum.FIRMA,
				MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD, allegati, allacci, destinatariRed, mailDestinatari.toString(), mailDestinatariCC.toString(), casellaMail,
				"MEF - RGS - N. PROTOCOLLO", testoTemplate, null, null, null, null, null, null, null, null, con);
	}

	/**
	 * @param destinatariRed
	 * @param mailDestinatari
	 * @param mailDestinatariCC
	 * @param inDestinatari
	 * @param con
	 */
	private void gestisciDestinatariRed(final List<DestinatarioRedDTO> destinatariRed, final StringBuilder mailDestinatari, final StringBuilder mailDestinatariCC,
			final List<DestinatarioRedDTO> inDestinatari, final UtenteDTO utenteCreatore, final Connection con) {
		LOGGER.info("gestisciDestinatariRed -> START");

		if (!CollectionUtils.isEmpty(inDestinatari)) {
			for (final DestinatarioRedDTO inDestinatario : inDestinatari) {

				final Long idUfficioDestinatario = inDestinatario.getIdNodo();

				// ID ufficio specificato, si tratta di un destinatario INTERNO
				if (idUfficioDestinatario != null && idUfficioDestinatario > 0) {

					final Nodo ufficioDestinatario = nodoDAO.getNodo(idUfficioDestinatario, con);

					if (ufficioDestinatario != null) {
						final Contatto contattoDestinatarioInterno = new Contatto();

						String aliasContatto = ufficioDestinatario.getDescrizione();

						final Long idUtenteDestinatario = inDestinatario.getIdUtente();
						if (idUtenteDestinatario != null && idUtenteDestinatario > 0) {
							final Utente utenteDestinatario = utenteDAO.getUtente(idUtenteDestinatario, con);

							if (utenteDestinatario != null) {
								contattoDestinatarioInterno.setNome(utenteDestinatario.getNome());
								contattoDestinatarioInterno.setCognome(utenteDestinatario.getCognome());

								aliasContatto += " - " + utenteDestinatario.getNome() + " " + utenteDestinatario.getCognome();
							} else {
								throw new RedException("Nessun utente presente con ID [" + idUtenteDestinatario + "]");
							}
						}

						contattoDestinatarioInterno.setAliasContatto(aliasContatto);

						inDestinatario.setContatto(contattoDestinatarioInterno);

						destinatariRed.add(inDestinatario);

					} else {
						throw new RedException("Nessun ufficio presente con ID [" + idUfficioDestinatario + "]");
					}

					// Contatto in input specificato, si tratta di un destinatario ESTERNO
				} else if (inDestinatario.getContatto() != null) {

					final Contatto inContattoDestinatario = inDestinatario.getContatto();

					// ID contatto specificato: il destinatario è presente in rubrica
					if (inContattoDestinatario.getContattoID() != null) {
						final Long idContattoDestinatario = inDestinatario.getContatto().getContattoID();

						// Si recupera il contatto completo di tutte le informazioni dalla rubrica
						final Contatto contattoDestinatario = rubricaSRV.getContattoByID(idContattoDestinatario, con);

						if (contattoDestinatario != null) {
							impostaContattoDestinatario(destinatariRed, mailDestinatari, mailDestinatariCC, inDestinatario, contattoDestinatario);
						} else {
							throw new RedException("Nessun contatto non presente con ID [" + idContattoDestinatario + "]");
						}

						// ID destinatario NON specificato: il destinatario deve essere creato on the
						// fly
					} else {
						// Si cerca il destinatario in rubrica...
						final Contatto nuovoContattoDestinatario = rubricaSRV.insertContattoFlussoAutomatico(inContattoDestinatario, utenteCreatore, con);

						impostaContattoDestinatario(destinatariRed, mailDestinatari, mailDestinatariCC, inDestinatario, nuovoContattoDestinatario);
					}
				} else {
					throw new RedException("Impossibile determinare la tipologia (INTERNO/ESTERNO) di destinatario per uno dei destinatari in input");
				}
			}

			// Rimozione del ";" finale
			if (mailDestinatari.length() > 1) {
				mailDestinatari.substring(0, mailDestinatari.length() - 1);
			}
			if (mailDestinatariCC.length() > 1) {
				mailDestinatariCC.substring(0, mailDestinatariCC.length() - 1);
			}
		}

		LOGGER.info("gestisciDestinatariRed -> END");
	}

	/**
	 * @param destinatariRed
	 * @param mailDestinatari
	 * @param mailDestinatariCC
	 * @param inDestinatario
	 * @param contattoDestinatario
	 */
	private void impostaContattoDestinatario(final List<DestinatarioRedDTO> destinatariRed, final StringBuilder mailDestinatari, final StringBuilder mailDestinatariCC,
			final DestinatarioRedDTO inDestinatario, final Contatto contattoDestinatario) {
		// Si impostano le stringhe mail dei destinatari TO/CC per il tab Spedizione
		impostaMailsDestinatari(inDestinatario, contattoDestinatario, mailDestinatari, mailDestinatariCC);

		inDestinatario.setContatto(contattoDestinatario);

		destinatariRed.add(inDestinatario);
	}

	/**
	 * @param inDestinatario
	 * @param contattoDestinatario
	 * @param mailDestinatari
	 * @param mailDestinatariCC
	 * @return
	 */
	private void impostaMailsDestinatari(final DestinatarioRedDTO inDestinatario, final Contatto contattoDestinatario, final StringBuilder mailDestinatari,
			final StringBuilder mailDestinatariCC) {
		if (ModalitaDestinatarioEnum.TO.equals(inDestinatario.getModalitaDestinatarioEnum())) {

			if (StringUtils.isNotBlank(contattoDestinatario.getMail())) {
				mailDestinatari.append(contattoDestinatario.getMail()).append(";");
			} else if (StringUtils.isNotBlank(contattoDestinatario.getMailPec())) {
				mailDestinatari.append(contattoDestinatario.getMailPec()).append(";");
			}

		} else {

			if (StringUtils.isNotBlank(contattoDestinatario.getMail())) {
				mailDestinatariCC.append(contattoDestinatario.getMail()).append(";");
			} else if (StringUtils.isNotBlank(contattoDestinatario.getMailPec())) {
				mailDestinatariCC.append(contattoDestinatario.getMailPec()).append(";");
			}

		}
	}

	/**
	 * Costruisce il DTO del documento in uscita da creare.
	 * 
	 * @param idUfficioCreatore
	 * @param oggetto
	 * @param contentPrincipale
	 * @param idUtenteAssegnatario
	 * @param idUfficioAssegnatario
	 * @param fascicolo
	 * @param indiceClassificazione
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param idIterApprovativo
	 * @param tipoAssegnazione
	 * @param momentoProtocollazione
	 * @param allegati
	 * @param allacci
	 * @param destinatariRed
	 * @param mailDestinatari
	 * @param mailDestinatariCC
	 * @param casellaMail
	 * @param oggettoMail
	 * @param testoMail
	 * @param codiceFlusso
	 * @param con
	 * @param idProtocollo
	 * @param numeroProtocollo
	 * @param annoProtocollo
	 * @param dataProtocollo
	 * @param tipoProtocollo
	 * @param metadatiEstesi
	 * @param sottoCategoriaUscita
	 * @return
	 */
	private DetailDocumentRedDTO getDocumentoRedInUscita(final Long idUfficioCreatore, final String oggetto, final FileDTO contentPrincipale, final Long idUtenteAssegnatario,
			final Long idUfficioAssegnatario, final FascicoloDTO fascicolo, final String indiceClassificazione, final Integer idTipologiaDocumento,
			final Integer idTipoProcedimento, final Integer idIterApprovativo, final TipoAssegnazioneEnum tipoAssegnazione,
			final MomentoProtocollazioneEnum momentoProtocollazione, final List<AllegatoDTO> allegati, final List<RispostaAllaccioDTO> allacci,
			final List<DestinatarioRedDTO> destinatariRed, final String mailDestinatari, final String mailDestinatariCC, final String casellaMail, final String oggettoMail,
			final String testoMail, final String codiceFlusso, final String idProtocollo, final Integer numeroProtocollo, final Integer annoProtocollo,
			final Date dataProtocollo, final Integer tipoProtocollo, final List<MetadatoDTO> metadatiEstesi, final SottoCategoriaDocumentoUscitaEnum sottoCategoriaUscita,
			final Connection con) {
		final DetailDocumentRedDTO document = new DetailDocumentRedDTO();

		document.setFormatoDocumentoEnum(FormatoDocumentoEnum.ELETTRONICO);
		document.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_USCITA.getIds()[0]);
		document.setMomentoProtocollazioneEnum(momentoProtocollazione);
		document.setIdIterApprovativo(idIterApprovativo);
		document.setTipoAssegnazioneEnum(tipoAssegnazione);
		document.setSottoCategoriaUscita(sottoCategoriaUscita);

		// Tipologia documento
		document.setIdTipologiaDocumento(idTipologiaDocumento);

		// Tipo procedimento
		document.setIdTipologiaProcedimento(idTipoProcedimento);

		// Metadati estesi
		document.setMetadatiEstesi(metadatiEstesi);

		// Content
		if (contentPrincipale != null) {
			document.setContent(contentPrincipale.getContent());
			document.setNomeFile(contentPrincipale.getFileName());
			document.setMimeType(contentPrincipale.getMimeType());
		}
		// Allegati
		if (!CollectionUtils.isEmpty(allegati)) {
			document.setAllegati(allegati);
		}

		// Allacci
		if (!CollectionUtils.isEmpty(allacci)) {
			document.setAllacci(allacci);
		}

		// Oggetto
		document.setOggetto(oggetto);

		// Assegnazione
		if (idUtenteAssegnatario != null && idUfficioAssegnatario != null) {
			NodoUtenteRuolo utenteUfficio = null;
			UtenteDTO utenteAssegnatario = null;
			if (idUtenteAssegnatario != null) {
				utenteUfficio = utenteDAO.getUtenteUfficioByIdUtenteAndIdNodo(idUtenteAssegnatario, idUfficioAssegnatario, con);
				if (utenteUfficio == null) {
					throw new RedException("Impossibile recuperare l'assegnatario: utente con ID [" + idUtenteAssegnatario + "] non presente, oppure ufficio con ID ["
							+ idUfficioAssegnatario + "] non presente, " + "oppure utente non associato all'ufficio indicato");
				} else {
					if (utenteUfficio.getUtente() != null && utenteUfficio.getUtente().getDataDisattivazione() != null) {
						throw new RedException("Impossibile recuperare l'utente assegnatario con ID [" + idUtenteAssegnatario + "]: utente disattivato");
					} else {
						utenteAssegnatario = new UtenteDTO();
						utenteAssegnatario.setId(idUtenteAssegnatario);
						utenteAssegnatario.setIdUfficio(idUfficioAssegnatario);
					}
				}
			}

			UfficioDTO ufficioAssegnatario = null;

			Nodo ufficioAss = null;
			if (utenteUfficio != null && utenteUfficio.getNodo() != null) {
				ufficioAss = utenteUfficio.getNodo();
			} else {
				ufficioAss = nodoDAO.getNodo(idUfficioAssegnatario, con);
			}
			if (ufficioAss == null) {
				throw new RedException("Impossibile recuperare l'ufficio assegnatario con ID [" + idUfficioAssegnatario + "]: ID non presente");
			} else if (ufficioAss.getDataDisattivazione() != null) {
				throw new RedException("Impossibile recuperare l'ufficio assegnatario con ID [" + idUfficioAssegnatario + "]: ufficio disattivato");
			} else {
				ufficioAssegnatario = new UfficioDTO(idUfficioAssegnatario, null);
			}

			document.setAssegnazioni(new ArrayList<>());
			document.getAssegnazioni()
					.add(new AssegnazioneDTO(null, ((TipoAssegnazioneEnum.SPEDIZIONE.equals(tipoAssegnazione)) ? TipoAssegnazioneEnum.COMPETENZA : tipoAssegnazione), null,
							null, null, utenteAssegnatario, ufficioAssegnatario));
		}

		// Indice di classificazione e fascicoli
		if (fascicolo != null) {
			final List<FascicoloDTO> fascicoli = new ArrayList<>();
			fascicoli.add(fascicolo);
			document.setFascicoli(fascicoli);
		} else {
			// Se il fascicolo non è passato in input, si deve impostare manualmente la
			// descrizione del fascicolo procedimentale
			final TipologiaDocumentoDTO tipologiaDocumento = tipologiaDocumentoSRV.getById(idTipologiaDocumento, con);

			if (tipologiaDocumento != null) {
				document.setDescrizioneFascicoloProcedimentale(tipologiaDocumento.getDescrizione() + " - " + oggetto);
			} else {
				throw new RedException(
						"Impossibile impostare la descrizione del fascicolo procedimentale per il nuovo documento in uscita: " + "tipologia documento non presente");
			}
		}

		if (StringUtils.isNotBlank(indiceClassificazione)) {
			document.setIndiceClassificazioneFascicoloProcedimentale(indiceClassificazione);
		} else if (fascicolo != null) {
			if (StringUtils.isNotBlank(fascicolo.getIndiceClassificazione())) {
				document.setIndiceClassificazioneFascicoloProcedimentale(fascicolo.getIndiceClassificazione());
			} else {
				throw new RedException("Impossibile impostare l'indice di classificazione del fascicolo procedimentale per il nuovo documento in uscita");
			}
		}

		// Destinatari
		document.setDestinatari(destinatariRed);

		final boolean spedizioneMail = TipoAssegnazioneEnum.SPEDIZIONE.equals(document.getTipoAssegnazioneEnum())
				&& (document.getIdIterApprovativo() == null || document.getIdIterApprovativo() == 0);
		document.setSpedizioneMail(spedizioneMail);

		// Mail spedizione
		if (StringUtils.isNotBlank(casellaMail) && StringUtils.isNotBlank(mailDestinatari) && StringUtils.isNotBlank(oggettoMail) && StringUtils.isNotBlank(testoMail)) {

			// Si verifica che la casella postale specificata in input sia configurata per
			// l'ufficio dell'utente creatore e per la spedizione
			final boolean casellaPostaleAbilitata = casellaPostaleDAO.getAbilitazioniCasellaPostale(casellaMail, idUfficioCreatore, FunzionalitaEnum.TAB_SPEDIZIONE, con);

			if (casellaPostaleAbilitata) {
				final EmailDTO mailSpedizione = new EmailDTO();
				mailSpedizione.setMittente(casellaMail);
				mailSpedizione.setDestinatari(mailDestinatari);
				mailSpedizione.setDestinatariCC(mailDestinatariCC);
				mailSpedizione.setOggetto(oggettoMail);
				mailSpedizione.setTesto(testoMail);

				document.setMailSpedizione(mailSpedizione);
			} else {
				throw new RedException("La casella postale " + casellaMail + " non esiste oppure non è visibile dall'utente creatore");
			}
		}

		// Codice flusso
		document.setCodiceFlusso(codiceFlusso);

		// Protocollo
		if (StringUtils.isNotBlank(idProtocollo)) {
			document.setIdProtocollo(idProtocollo);
			document.setNumeroProtocollo(numeroProtocollo);
			document.setDataProtocollo(dataProtocollo);
			document.setAnnoProtocollo(annoProtocollo);
			document.setTipoProtocollo(tipoProtocollo);
		}

		return document;
	}

	/**
	 * @param idDocumentoEntrata
	 * @param idAoo
	 * @param fceh
	 * @param idTipologiaDocumento
	 * @param tipoCategoria
	 * @param utente
	 * @return
	 * @throws IOException
	 */
	protected AllegatoDTO getMailOriginale(final String idDocumentoEntrata, final Long idAoo, final Integer idTipologiaDocumento, final TipoCategoriaEnum tipoCategoria,
			final UtenteDTO utente) {

		IFilenetCEHelper fceh = null;
		AllegatoDTO allegato = null;

		try {
			final String username = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.SYSTEM_ADMIN_USERNAME_KEY);

			fceh = FilenetCEHelperProxy.newInstance(username, utente.getFcDTO().getPassword(), utente.getFcDTO().getUri(), utente.getFcDTO().getStanzaJAAS(),
					utente.getFcDTO().getObjectStore(), utente.getIdAoo());

			final Document documentoEntrata = fceh.getDocumentByDTandAOO(idDocumentoEntrata, idAoo);
			final String nomeEmail = mailSRV.getNomeMailOriginale(documentoEntrata);

			final DocumentoAllegabileDTO mailOriginale = mailSRV.getMailOriginale(idDocumentoEntrata, nomeEmail, idAoo, fceh);

			if (mailOriginale != null) {
				allegato = new AllegatoDTO(null, FormatoAllegatoEnum.ELETTRONICO.getId(), idTipologiaDocumento, null, null, null, true, false, null, null,
						mailOriginale.getNomeFile(), mailOriginale.getContentType(), null, IOUtils.toByteArray(mailOriginale.getContenuto().getInputStream()), null, null,
						null, null, true, tipologiaDocumentoSRV.getComboTipologieByAooAndTipoCategoriaAttivi(tipoCategoria, utente.getIdAoo()), false, false);
			}

		} catch (final IOException e) {
			LOGGER.error("Si è verificato un errore durante la creazione della mail originale in entrata come allegato", e);
			throw new RedException("Errore durante la creazione della mail originale in entrata come allegato: " + e.getMessage(), e);
		} finally {
			popSubject(fceh);
		}

		return allegato;
	}

	/**
	 * @param guidTemplate
	 * @param nomeTemplate
	 * @param testoTemplate
	 * @param utenteResponsabile
	 * @param destinatari
	 * @param oggetto
	 * @param conn
	 * @return
	 */
	protected FileDTO getDocumentoPrincipaleUscita(final String guidTemplate, final String nomeTemplate, final String testoTemplate, final UtenteDTO utenteResponsabile,
			final List<DestinatarioRedDTO> destinatari, final String oggetto, final Connection conn) {
		final String dataDocumento = DateUtils.dateToString(new Date(), "ddMMyyyy_HHmmss");
		final String nomeFilePrincipaleUscita = nomeTemplate + "_" + dataDocumento + ".pdf";

		// Gestione metadati dinamici
		final List<TemplateMetadatoValueDTO> metadati = new ArrayList<>();
		final List<TemplateMetadatoDTO> metadatiTemplate = templateDocUscitaSRV.getMetadati(guidTemplate, conn);
		for (final TemplateMetadatoDTO metadatoTemplate : metadatiTemplate) {

			final TemplateMetadatoValueDTO metadato = creaMetadatoValue(metadatoTemplate);
			metadato.setValue(templateDocUscitaSRV.getMetadatoValue(metadatoTemplate.getDefaultValue(), destinatari, oggetto, null, null, utenteResponsabile, metadatoTemplate.getIdMetadatoIntestazione()));

			if ("%TESTO%".equals(metadato.getTemplateValue())) {
				metadato.setValue(testoTemplate);
			}

			metadati.add(metadato);

		}

		return templateDocUscitaSRV.generaDocumento(utenteResponsabile, guidTemplate, nomeFilePrincipaleUscita, metadati);
	}

	/**
	 * @param metadatoTemplate
	 * @return
	 */
	private static TemplateMetadatoValueDTO creaMetadatoValue(final TemplateMetadatoDTO metadatoTemplate) {
		final TemplateMetadatoValueDTO metadato = new TemplateMetadatoValueDTO();

		metadato.setIdTemplate(metadatoTemplate.getIdTemplate());
		metadato.setIdMetadato(metadatoTemplate.getIdMetadato());
		metadato.setLabel(metadatoTemplate.getLabel());
		metadato.setOrdine(metadatoTemplate.getOrdine());
		metadato.setMaxLength(metadatoTemplate.getMaxLength());
		metadato.setDefaultValue(metadatoTemplate.getDefaultValue());
		metadato.setTestoPredefinito(metadatoTemplate.getTestoPredefinito());
		metadato.setTipoMetadato(metadatoTemplate.getTipoMetadato());
		metadato.setTemplateValue(metadatoTemplate.getTemplateValue());
		metadato.setTemplateLabel(metadatoTemplate.isTemplateLabel());
		metadato.setTemplateTesto(metadatoTemplate.isTemplateTesto());
		metadato.setTestiPredefiniti(metadatoTemplate.getTestiPredefiniti());

		return metadato;
	}

	/**
	 * @return
	 */
	protected SalvaDocumentoRedParametriDTO getParametriUscita() {
		final SalvaDocumentoRedParametriDTO parametri = new SalvaDocumentoRedParametriDTO();

		parametri.setModalita(Modalita.MODALITA_INS);
		parametri.setContentVariato(true);

		return parametri;
	}

	/**
	 * Crea un documento RED in uscita da NPS.
	 * 
	 * @param tipoAzione
	 *            tipo azione documento creato
	 * @param content
	 *            content documento creato
	 * @param contentType
	 *            content type del documento creato
	 * @param nomeFile
	 *            nome file del documento creato
	 * @param guidLastVersion
	 *            guid dell'ultima versione del documento creato
	 * @param utenteCreatore
	 *            utente creato del documento
	 * @param fascicoloProcedimentale
	 *            fascicolo procedimentale del documento creato
	 * @param indiceClassificazione
	 *            indice di classificazione del documento creato
	 * @param idTipologiaDocumento
	 *            identificativo della tipologia documento
	 * @param idTipoProcedimento
	 *            identificativo della tipologia procedimento
	 * @param idIterApprovativo
	 *            identificativo iter approvativo del documento creato
	 * @param tipoAssegnazione
	 *            tipo assegnazione del documento creato
	 * @param momentoProtocollazione
	 *            momento protocollazione del documento creato
	 * @param allegati
	 *            allegati del documento creato
	 * @param allacci
	 *            allacci del documento creato
	 * @param contattiDestinatari
	 *            contatti destinatari del documento creato
	 * @param oggetto
	 *            oggetto del documento creato
	 * @param oggettoMail
	 *            oggetto mail del documento creato
	 * @param testoMail
	 *            testo mail del documento creato
	 * @param casellaMittenteMail
	 *            casella mail del mittente del documento creato
	 * @param idUtenteAssegnatario
	 *            identificativo dell'utente assegnatario
	 * @param idUfficioAssegnatario
	 *            identificativo dell'ufficio assegnatario
	 * @param codiceFlusso
	 *            codice flusso del documento creato
	 * @param idProtocollo
	 *            identificativo protocollo
	 * @param numeroProtocollo
	 *            numero protocollo
	 * @param annoProtocollo
	 *            anno protocollo
	 * @param dataProtocollo
	 *            data protocollo
	 * @param tipoProtocollo
	 *            tipo protocollo
	 * @param metadatiEstesi
	 *            metadati estesi del documento creato
	 * @param sottoCategoriaUscita
	 *            sottocategoria del documento in uscita
	 * @param con
	 *            connessione al database
	 * @return esito della creazione
	 */
	protected EsitoSalvaDocumentoDTO creaDocumentoRedUscitaDaNPS(final TipoNotificaAzioneNPSEnum tipoAzione, final byte[] content, final String contentType, final String nomeFile, 
			final String guidLastVersion, final UtenteDTO utenteCreatore, final FascicoloDTO fascicoloProcedimentale, final String indiceClassificazione, 
			final Integer idTipologiaDocumento, final Integer idTipoProcedimento, final Integer idIterApprovativo, final TipoAssegnazioneEnum tipoAssegnazione, 
			final MomentoProtocollazioneEnum momentoProtocollazione, final List<AllegatoDTO> allegati, final List<RispostaAllaccioDTO> allacci, 
			final List<DestinatarioRedDTO> contattiDestinatari, final String oggetto, final String oggettoMail, final String testoMail, final String casellaMittenteMail, final Long idUtenteAssegnatario,
			final Long idUfficioAssegnatario, final String codiceFlusso, final String idProtocollo, final Integer numeroProtocollo,
			final Integer annoProtocollo, final Date dataProtocollo, final Integer tipoProtocollo, final List<MetadatoDTO> metadatiEstesi,
			final SottoCategoriaDocumentoUscitaEnum sottoCategoriaUscita, final Connection con) {
		LOGGER.info("creaDocumentoRedUscitaDaNPS -> Nome file [" + nomeFile + "], oggetto [" + oggetto + "], iter [" + idIterApprovativo + "], ufficio creatore ["
				+ utenteCreatore.getIdUfficio() + "], utente creatore [" + utenteCreatore.getId() + "]");

		// Destinatari
		final List<DestinatarioRedDTO> destinatariRed = new ArrayList<>();
		final StringBuilder mailDestinatari = new StringBuilder();
		final StringBuilder mailDestinatariCC = new StringBuilder();
		gestisciDestinatariRed(destinatariRed, mailDestinatari, mailDestinatariCC, contattiDestinatari, utenteCreatore, con);

		// Documento principale
		FileDTO contentPrincipale = null;
		if (content != null) {
			contentPrincipale = new FileDTO(nomeFile, content, contentType);
		}

		// Recupero del documento da creare
		final DetailDocumentRedDTO documento = getDocumentoRedInUscita(utenteCreatore.getIdUfficio(), oggetto, contentPrincipale, idUtenteAssegnatario, idUfficioAssegnatario,
				fascicoloProcedimentale, indiceClassificazione, idTipologiaDocumento, idTipoProcedimento, idIterApprovativo, tipoAssegnazione, momentoProtocollazione,
				allegati, allacci, destinatariRed, mailDestinatari.toString(), mailDestinatariCC.toString(), casellaMittenteMail, oggettoMail, testoMail, codiceFlusso,
				idProtocollo, numeroProtocollo, annoProtocollo, dataProtocollo, tipoProtocollo, metadatiEstesi, sottoCategoriaUscita, con);

		// Recupero dei parametri di creazione
		final SalvaDocumentoRedParametriDTO parametri = getParametriUscita();
		if (contentPrincipale == null) {
			
			if(TipoNotificaAzioneNPSEnum.RISPONDI_A.equals(tipoAzione)){
				parametri.setVelinaAsDocPrincipale(true);
			}
			
		} else {
			
			if(TipoNotificaAzioneNPSEnum.AGGIORNAMENTO_COLLEGATI.equals(tipoAzione)){
				parametri.setContentFromNPS(true);
				parametri.setGuidLastVersionNPSDocPrincipale(guidLastVersion);
			}
			
		}

		return creaDocumentoRed(documento, parametri, utenteCreatore);
	}

}