package it.ibm.red.business.enums;

import java.util.ArrayList;
import java.util.Collection;

import it.ibm.red.business.dto.ThemeDTO;

/**
 * The Enum ThemeEnum.
 *
 * @author CPIERASC
 * 
 *         Enum dei temi.
 */
public enum ThemeEnum {

	/**
	 * Tema CUPERTINO.
	 */
	CUPERTINO(0, "Cupertino", "cupertino");

	/**
	 * Tema di default.
	 */
	public static final ThemeEnum DEFAULT_THEME = CUPERTINO;

	/**
	 * Identificativo tema.
	 */
	private int id;
	
	/**
	 * Nome da mostrare.
	 */
	private String displayName;
	
	/**
	 * Nome di servizio.
	 */
	private String name;

	/**
	 * Costruttore.
	 * 
	 * @param inId			identificativo
	 * @param inDisplayName	display name
	 * @param inName		name
	 */
	ThemeEnum(final int inId, final String inDisplayName, final String inName) {
		id = inId;
		displayName = inDisplayName;
		name = inName;
	}

	/**
	 * Getter id.
	 * 
	 * @return	id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Getter display name.
	 * 
	 * @return	display name
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Getter name.
	 * 
	 * @return	name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Recupero themes.
	 * 
	 * @return	collezione temi
	 */
	public static Collection<ThemeDTO> getThemes() {
		Collection<ThemeDTO> output = new ArrayList<>();
		for (ThemeEnum t:ThemeEnum.values()) {
			output.add(new ThemeDTO(t));
		}
		return output;
	}
	
	/**
	 * Recupero default theme: il tema di default è quello con identificatore pari a 0.
	 * 
	 * @return	tema di default
	 */
	public static ThemeEnum getDefaultTheme() {
		ThemeEnum output = null;
		for (ThemeEnum t:ThemeEnum.values()) {
			if (t.getId() == 0) {
				output = t;
				break;
			}
		}
		return output;
	}
}