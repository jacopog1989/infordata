package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.ComuneDTO;

/**
 * Interfaccia DAO comune.
 */
public interface IComuneDAO extends Serializable {

	/**
	 * Ottiene tutti i comuni.
	 * 
	 * @param conn
	 *            Connessione al database.
	 * @return comuni Comuni recuperati.
	 */
	Collection<ComuneDTO> getAll(Connection conn);

	/**
	 * Ottiene il comune identificato dall' {@code idComune}.
	 * 
	 * @param idComune
	 *            Identificativo del comune
	 * @param conn
	 *            Connessione al database.
	 * @return Comune recuperato.
	 */
	ComuneDTO get(String idComune, Connection conn);

	/**
	 * Ottiene il comune identificato dal {@code nomeComune}.
	 * 
	 * @param nomeComune
	 *            Nome del comune da recuperare.
	 * @param conn
	 *            Connessione al database.
	 * @return Comune recuperato.
	 */
	ComuneDTO getComuneByNome(String nomeComune, Connection conn);

	/**
	 * Ottiene i comuni associati alla provincia identificati dall'
	 * {@code idProvincia}.
	 * 
	 * @param idProvincia
	 *            Identificativo della provincia a cui sono associati i comuni
	 *            recuperati.
	 * @param query
	 *            Descrizione dei comuni.
	 * @param conn
	 *            Connessione al database.
	 * @return Lista di comuni recuperati associati alla provincia identificata
	 *         dall' {@code idProvincia}.
	 */
	List<ComuneDTO> getComuni(Long idProvincia, String query, Connection conn);
}
