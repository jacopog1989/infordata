package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignStepResultDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.SignModeEnum;
import it.ibm.red.business.enums.SignStrategyEnum;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.enums.SignTypeEnum.SignTypeGroupEnum;
import it.ibm.red.business.service.asign.step.StatusEnum;
import it.ibm.red.business.service.asign.step.StepEnum;
import it.ibm.red.business.service.facade.IASignFacadeSRV;

/**
 * Interfaccia service di firma asincrona.
 */
public interface IASignSRV extends IASignFacadeSRV {

	/**
	 * Effettua la insert dell'item di firma asincrona consentendone il controllo
	 * del processo step by step.
	 * 
	 * @param wobNumber
	 *            wob number del documento in firma
	 * @param documentTitle
	 *            identificativo del documento in firma
	 * @param numeroDocumento
	 *            numero documento in firma
	 * @param numeroProtocollo
	 *            numero protocollo del documento in firma se già protocollato,
	 *            <code> null </code> altrimenti
	 * @param annoProtocollo
	 *            anno protocollo del documento in firma se già protocollato,
	 *            <code> null </code> altrimenti
	 * @param utenteFirmatario
	 *            utente responsabile per la firma del documento
	 * @param signMode
	 *            modalità di firma, @see SignMode
	 * @param signType
	 *            tipologia di firma, @see SignTypeGroupEnum
	 * @param queue
	 *            coda in cui è presente l'item, @see DocumentQueueEnum
	 * @param connection
	 *            connession al database
	 * @return identificativo dell'item inserito
	 */
	Long insertItem(String wobNumber, String documentTitle, Integer numeroDocumento, Integer numeroProtocollo, Integer annoProtocollo, UtenteDTO utenteFirmatario, 
			SignModeEnum signMode, SignTypeGroupEnum signType, DocumentQueueEnum queue, Connection connection);
	
	/**
	 * Restituisce il primo item lavorabile in base alla priorità degli items. Per
	 * lavorabile si intende un item nello stato <code> PRESO_IN_CARICO </code>
	 * quindi mai lavorato prima.
	 * 
	 * @return identificato dell'item lavorabile
	 */
	Long getFirstItem();
	
	/**
	 * Recupera tutti gli item pronti per essere elaborati per la prima volta,
	 * quindi nello stato <code> StatusEnum.PRESO_IN_CARICO </code>.
	 * 
	 * @param idAoo
	 *            identificativo dell'AOO
	 * @return lista degli id degli item per cui è possibile eseguire il play
	 */
	List<Long> getPlayableItems(Long idAoo);
	
	/**
	 * Effettua l'aggiornamento dello step sostituendone i valori modificati.
	 * 
	 * @param lastResult
	 *            informazioni dell'item aggiornato, @see ASignStepResultDTO
	 */
	void updateStep(ASignStepResultDTO lastResult);
	
	/**
	 * Effettua l'aggiornamento dello stato sostituendone i valori modificati.
	 * 
	 * @param idItem
	 *            identificativo item per la quale occorre aggiornare lo stato
	 * @param status
	 *            nuovo stato da impostare, @see StatusEnum
	 */
	void updateStatus(Long idItem, StatusEnum status);
	
	/**
	 * Restituisce le informazioni sullo step di firma asincrona.
	 * 
	 * @param idItem
	 *            identificativo dell'item per il quale occorre conoscer
	 * @param step
	 *            step per la quale occorre conoscere le informazione, @see StepEnum
	 * @return informazioni sullo step, @see ASignStepResultDTO
	 */
	ASignStepResultDTO getInfo(Long idItem, StepEnum step);
	
	/**
	 * Restituisce le informazioni su tutti gli step di firma asincrona raggiunti
	 * dall'item identificato dal <code> idItem </code>.
	 * 
	 * @param idItem
	 *            identificativo item
	 * @return lista degli step result
	 */
	List<ASignStepResultDTO> getInfos(Long idItem);
	
	/**
	 * Restituisce il primo item resumable in base alla priorità degli items.
	 * 
	 * @return ASignItemDTO tutte le informazioni dell'item recuperato
	 */
	ASignItemDTO getFirstResumableItem();
	
	/**
	 * Recupera tutti gli item nello stato: StatusEnum.RETRY.
	 * 
	 * @param idAoo
	 *            identificativo dell'AOO
	 * @return lista degli item per i quali è necessario eseguire un retry
	 */
	List<ASignItemDTO> getResumableItems(Long idAoo);
	
	/**
	 * Restituisce true se il documento associato all'item identificato dall'
	 * <code> idItem </code> risulta già protocollato e quindi è già stato stacco un
	 * numero protocollo per lo stesso.
	 * 
	 * @param idItem
	 *            identificativo dell'item per il quale occorre conoscere lo stato
	 *            di protocollazione del documento
	 * @return true se il documento associato risulta protocollato, false altrimenti
	 */
	Boolean isProtocollato(Long idItem);
	
	/**
	 * Restituisce true se esiste una registrazione ausiliaria valida e completa su
	 * un documento associato all'item identificato da: <code> item </code>.
	 * 
	 * @param item
	 *            item di firma asincrona per il quale occorre sapere se esiste una
	 *            registrazione ausiliaria
	 * @return true se esiste una registrazione ausiliaria, false altrimenti
	 */
	Boolean hasRegistrazioneAusiliaria(ASignItemDTO item);
	
	/**
	 * Imposta il numero dei retry associati ad un item identificato dall'
	 * <code> idItem </code>.
	 * 
	 * @param idItem
	 *            identificativo dell'item
	 * @param n
	 *            numero di retry da impostare
	 * @param bResetDate
	 *            se true effettua un reset della data, se false la data viene
	 *            aggiornata al momento di inserimento
	 */
	void setRetry(Long idItem, Integer n, Boolean bResetDate);

	/**
	 * Recupera il tipo di strategia firma dell'item identificato dall'
	 * <code> idItem </code>.
	 * 
	 * @param idItem
	 *            identificativo dell'item
	 * @return strategia di firma, @see SignStrategyEnum
	 */
	SignStrategyEnum getSignStrategy(Long idItem);
	
	/**
	 * Restituisce gli item esistenti nella coda specificata da <code> queue </code>
	 * che siano visibili ed esistenti su FileNet e che quindi non sono ancora stati
	 * firmati.
	 * 
	 * @param utente
	 *            utente in sessione
	 * @param queue
	 *            coda di lavorazione che può essere: NSD (Libro Firma) o
	 *            DD_DA_FIRMARE (Libro Firma FEPA)
	 * @return Lista di ASignItemDTO
	 */
	List<ASignItemDTO> getItems(UtenteDTO utente, DocumentQueueEnum queue);

	/**
	 * Restituisce gli item per i quali il procedimento di firma abbia raggiunto uno
	 * stato di errore, @see StatusEnum.
	 * 
	 * @param idAoo
	 *            identificativo dell'item
	 * @return lista degli item in errore
	 */
	List<ASignItemDTO> getItemsDTFailed(Long idAoo);
	
	/**
	 * Restituisce una mappa che associa ad ogni <code> wob number </code> l'item di
	 * riferimento per la quale il procedimento di firma asincrona è stato avviato.
	 * 
	 * @param signTransactionId
	 *            transactionId dell'operazione di firma
	 * @param wobNumbers
	 *            per la quale occorre recuperare gli item
	 * @param utente
	 *            utente in sessione
	 * @return mappa di associazione wobnumbers - item
	 */
	Map<String, ASignItemDTO> filterWorkflowForAsyncSignAlreadyStarted(String signTransactionId, Collection<String> wobNumbers, UtenteDTO utente);
	
	/**
	 * Rimuove dal buffer dei documenti firmati l'item identificato dal
	 * <code> wobNumber </code>.
	 * 
	 * @param wobNumber
	 *            associato all'item di riferimento
	 * @param connection
	 *            connessione al database
	 */
	void removeFromSignedDocsBuffer(String wobNumber, Connection connection);

	/**
	 * Per ogni documento lavorato con successo: 1) esegue le operazioni da svolgere
	 * immediatamente dopo la firma di content afferenti a flussi che coinvolgono
	 * NPS (e.g. "inviaAtto" per flusso AUT o Atto Decreto UCB); 2) aggiorna sul CE
	 * i content del documento principale e degli eventuali allegati, aggiungendone
	 * una nuova versione; 3) inserisce un item nella coda di firma asincrona.
	 * 
	 * @param signTransactionId
	 *            transactionId dell'operazione di firma
	 * @param signOutcomes
	 *            esiti della firma
	 * @param utenteFirmatario
	 *            utente responsabile alla firma dei documenti
	 * @param signMode
	 *            modalità della firma, @see SignMode
	 * @param signType
	 *            tipologia della firma, @see SignTypeEnum
	 * @param copiaConforme
	 *            flag associato alla copia conforme
	 */
	void gestisciContentsFirmati(String signTransactionId, Collection<EsitoOperazioneDTO> signOutcomes, UtenteDTO utenteFirmatario, SignModeEnum signMode,
			SignTypeEnum signType, boolean copiaConforme);

	/**
	 * Restituisce true se l'id item è afferente ad un documento assegnato per firma
	 * multipla, altrimenti restituisce false.
	 * 
	 * @param idItem
	 *            identificativo dell'item
	 * @return true se l'item è associato ad un iter di firma multipla, false
	 *         altrimenti
	 */
	Boolean isIterFirmaMultipla(Long idItem);

	/**
	 * Memorizza in un'apposita tabella del DB i dati che devono essere conservati
	 * attraverso l'esecuzione degli step di firma.
	 * 
	 * @param con
	 *            connessione al database
	 * @param idItem
	 *            identificativo dell'item
	 * @param dataToSave
	 *            informazioni da memorizzare sulla base dati
	 */
	void saveAndOverrideItemData(Connection con, Long idItem, Map<String, String> dataToSave);
	
	/**
	 * Recupero destinatari per inoltro mail comunicazione errore.
	 * 
	 * @param item
	 *            informazioni sull'item per il quale recuperare i destinatari
	 * @return lista delle mail dei destinatari
	 */
	List<String> recuperaEmailDestinatari(ASignItemDTO item);
	
	/**
	 * Imposta il flag che definisce la comunicazione dell'errore con gli utenti.
	 * Quando un tentativo di firma per un documento fallisce, viene comunicato agli
	 * utenti interessati, questo metodo consente di impostare il flag a true quando
	 * la comunicazione è avvenuta per evitare di comunicarlo molteplici volte.
	 * 
	 * @param idItem
	 *            identificativo dell'item per la quale aggiornare il flag di
	 *            comunicazione
	 * @param comunicato
	 *            flag che definisce lo stato di avvenuta comunicazione
	 */
	void setIsNotified(Long idItem, boolean comunicato);

	/**
	 * Restituisce l'id dell'item ancora non <code> Elaborato </code> associato al
	 * documento definito dall'<code> idDocumento </code>.
	 * 
	 * @param idDocumento
	 *            identificativo del documento
	 * @param idAoo
	 *            identificativo dell'Area organizzativa
	 * @return id item ancora in lavorazione associato al documento identificato
	 *         dall' <code> idDocumento </code>
	 */
	Long getIdItemFromIdDocumento(Long idDocumento, Long idAoo);
	
	/**
	 * Restituisce true se il document title è associato ad un item in stato
	 * <code> StatusEnum.ELABORATO </code> e quindi con una firma già presente.
	 * 
	 * @param documentTitle
	 *            identificativo del documento
	 * @return true se il documento identificato dal <code> documentTitle </code>
	 *         risulta già firmato, false altrimenti
	 */
	boolean isAlreadySigned (String documentTitle);

}