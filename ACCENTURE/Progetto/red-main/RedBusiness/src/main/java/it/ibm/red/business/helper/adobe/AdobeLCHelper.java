package it.ibm.red.business.helper.adobe;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.w3c.dom.Element;
import org.xhtmlrenderer.extend.FSImage;
import org.xhtmlrenderer.extend.ReplacedElement;
import org.xhtmlrenderer.extend.ReplacedElementFactory;
import org.xhtmlrenderer.extend.UserAgentCallback;
import org.xhtmlrenderer.layout.LayoutContext;
import org.xhtmlrenderer.pdf.ITextFSImage;
import org.xhtmlrenderer.pdf.ITextImageElement;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.render.BlockBox;
import org.xhtmlrenderer.simple.extend.FormSubmissionListener;

import com.adobe.idp.Document;
import com.adobe.idp.dsc.DSCException;
import com.adobe.idp.dsc.InvocationRequest;
import com.adobe.idp.dsc.InvocationResponse;
import com.adobe.idp.dsc.clientsdk.ServiceClient;
import com.adobe.idp.dsc.clientsdk.ServiceClientFactory;
import com.adobe.idp.dsc.clientsdk.ServiceClientFactoryProperties;
import com.adobe.livecycle.generatepdf.client.CreatePDFResult;
import com.adobe.livecycle.generatepdf.client.GeneratePdfServiceClient;
import com.itextpdf.text.pdf.codec.Base64;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Chunk;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.html.simpleparser.StyleSheet;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.Adobe;
import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AttachmentDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoDocumentoMockEnum;
import it.ibm.red.business.exception.AdobeException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.adobe.dto.ConversioneOutputDTO;
import it.ibm.red.business.helper.adobe.dto.CountSignsOutputDTO;
import it.ibm.red.business.helper.adobe.dto.approvazioni.FormType;
import it.ibm.red.business.helper.adobe.dto.approvazioni.jaxb.JAXBSerializer;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IMockSRV;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.ServerUtils;

/**
 * The Class AdobeLCHelper.
 *
 * @author CPIERASC
 * 
 *         Helper per la gestione di Adobe LiveCycle.
 */
public final class AdobeLCHelper implements IAdobeLCHelper {

	/**
	 * Label postilla.
	 */
	private static final String POSTILLA = "postilla";

	/**
	 * Label - DONE.
	 */
	private static final String DONE = ": DONE";

	/**
	 * Nome file pdf temporaneo.
	 */
	private static final String FILENAME_TEMP_PDF = "temp.pdf";

	/**
	 * Keyword: invoke.
	 */
	private static final String INVOKE_LITERAL = "invoke";

	/**
	 * Parametro documento ingresso.
	 */
	private static final String DOC_IN_PARAM = "docIN";

	/**
	 * Messaggio standard recall processo Adobe.
	 */
	private static final String ADOBE_RECALL_MESSAGE = "Richiamato il processo Adobe [";

	/**
	 * Messaggio di riferimento al documento.
	 */
	private static final String FOR_DOC_LITERAL = "] per il documento ";

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AdobeLCHelper.class.getName());

	/**
	 * Factory.
	 */
	private transient ServiceClientFactory serviceClientFactory;

	/**
	 * Propery provider.
	 */
	private final PropertiesProvider pp;

	/**
	 * Servizio mock.
	 */
	private IMockSRV mockSRV;

	/*
	 * CAMPOFIRMA campofirma DSC_CREDENTIAL_PASSWORD redcoll01
	 * DSC_CREDENTIAL_USERNAME redcoll DSC_DEFAULT_EJB_ENDPOINT
	 * iiop://10.38.67.0:9100 DSC_DEFAULT_SOAP_ENDPOINT
	 * http://adoberedcoll.tesoro.it DSC_SERVER_TYPE WebLogic DSC_TRANSPORT_PROTOCOL
	 * SOAP MAX_CONVERT_RETRY 1 PARAMETER_VALUE_CONVERT_PDF EASYFLOW
	 * PARAMETER_VALUE_CONVERT_PDFA SCS PDF.PREVIEW.MAXNUMPAGE 5 PREVIEW.ENABLE
	 * FALSE PROCESS_NAME_CONVERT_PDF FWS/convertToPDFFws
	 * PROCESS_NAME_GENERATE_COVER WFP/getCoverSpedizione PROCESS_NAME_MERGE_PDF
	 * WFP/mergePDFspedizione PROCESS_NAME_POSTILLA FWS/insertPostillaFws
	 * PROCESS_NAME_TAG_PDF FWS/prepareForSignFws PROCESS_NAME_VERIFYSIGN
	 * FWS/verifySignFws PROCESS_NAME_WATERMARK FWS/insertStampigliaturaFws
	 * adobe.tempFilePath /share/websphere/wfp2/fws/adobe/temp adobePDFSettings
	 * EASYFLOW_PDF fileTypeSettings EASYFLOW_FT securitySettings EASYFLOW_SEC
	 */

	/**
	 * Costruttore.
	 */
	public AdobeLCHelper() {
		pp = PropertiesProvider.getIstance();
		final String soapEndpoint = pp.getParameterByKey(PropertiesNameEnum.DSC_DEFAULT_SOAP_ENDPOINT_ADOBEKEY);
		final String serverType = pp.getParameterByKey(PropertiesNameEnum.DSC_SERVER_TYPE_ADOBEKEY);
		final String username = pp.getParameterByKey(PropertiesNameEnum.DSC_CREDENTIAL_USERNAME_ADOBEKEY);
		final String password = pp.getParameterByKey(PropertiesNameEnum.DSC_CREDENTIAL_PASSWORD_ADOBEKEY);
		if (soapEndpoint == null || serverType == null || username == null || password == null) {
			throw new RedException("Non sono presenti sul DB le credenziali di accesso per i servizi AdobeLC");
		}
		final Properties connectionProps = new Properties();
		connectionProps.setProperty(ServiceClientFactoryProperties.DSC_TRANSPORT_PROTOCOL, ServiceClientFactoryProperties.DSC_SOAP_PROTOCOL);
		connectionProps.setProperty(ServiceClientFactoryProperties.DSC_DEFAULT_SOAP_ENDPOINT, soapEndpoint);
		connectionProps.setProperty("DSC_SERVER_TYPE", serverType);
		connectionProps.setProperty("DSC_CREDENTIAL_USERNAME", username);
		connectionProps.setProperty("DSC_CREDENTIAL_PASSWORD", password);
		serviceClientFactory = ServiceClientFactory.createInstance(connectionProps);

		if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
				&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
			mockSRV = ApplicationContextProvider.getApplicationContext().getBean(IMockSRV.class);
		}

	}

	/**
	 * Predisposizione firma digitale.
	 *
	 * @param siglaVisto1 the sigla visto 1
	 * @param siglaVisto2 the sigla visto 2
	 * @param idDocumento the id documento
	 * @param protocollo  the protocollo
	 * @param postilla    the postilla
	 * @param content     the content
	 * @return the byte[]
	 */
	@Override
	public byte[] predisposizioneFirmaDigitale(final String siglaVisto1, final String siglaVisto2, final String idDocumento, final String protocollo, final String postilla,
			final byte[] content) {
		try {

			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[predisposizioneFirmaDigitale] Non è possibile comunicare con Adobe LC in regime mock");
				final FileDTO doc = mockSRV.getDocumento(TipoDocumentoMockEnum.PDF_PREDISPOSIZIONE_FIRMA_DIGITALE, ServerUtils.getServerFullName());
				return doc.getContent();
			}

			LOGGER.info(ADOBE_RECALL_MESSAGE + Adobe.PROCESS_NAME_PREDISPOSIZIONE_FIRMA + "] per il documento con ID=" + idDocumento);

			final ServiceClient serviceClient = serviceClientFactory.getServiceClient();

			final Document inDoc = new Document(content);
			inDoc.setContentTypeFromBasename();
			inDoc.passivate();

			final Map<String, Object> params = new HashMap<>();
			params.put("inDoc", inDoc);
			params.put("inIddocumento", idDocumento);
			params.put("inPostilla", postilla);
			params.put("inProtocollo", protocollo);
			params.put("inSiglaVisto1", siglaVisto1);
			params.put("inSiglaVisto2", siglaVisto2);

			final InvocationRequest esRequest = serviceClientFactory.createInvocationRequest(Adobe.PROCESS_NAME_PREDISPOSIZIONE_FIRMA, INVOKE_LITERAL, params, true);
			final InvocationResponse inResp = serviceClient.invoke(esRequest);

			LOGGER.info(ADOBE_RECALL_MESSAGE + Adobe.PROCESS_NAME_PREDISPOSIZIONE_FIRMA + FOR_DOC_LITERAL + idDocumento + DONE);

			final Document outDoc = (Document) inResp.getOutputParameter("outDoc");
			final FileItem fi = (new DiskFileItemFactory()).createItem(outDoc.getAttribute("file").toString(), outDoc.getContentType(), false,
					outDoc.getAttribute("file").toString());

			IOUtils.copy(new ByteArrayInputStream(readDocument(outDoc)), fi.getOutputStream());
			outDoc.dispose();
			return fi.get();
		} catch (final Exception e) {
			LOGGER.error("Errore durante la predisposizione della firma digitale sul documento con ID=" + idDocumento, e);
			throw new AdobeException(e);
		}
	}
	
	/**
	 * Inserisce la postilla con il processo adobe LC.
	 * 
	 * @param idDocumento
	 * @param postilla
	 * @param content
	 * 
	 * @return il file modificato
	 * 
	 */
	@Override
	public byte[] predisposizionePostilla(final String idDocumento,  final String postilla, final byte[] content) {
		try {

			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[predisposizioneFirmaDigitale] Non è possibile comunicare con Adobe LC in regime mock");
				final FileDTO doc = mockSRV.getDocumento(TipoDocumentoMockEnum.PDF_PREDISPOSIZIONE_FIRMA_DIGITALE, ServerUtils.getServerFullName());
				return doc.getContent();
			}
			
			final ServiceClient serviceClient = serviceClientFactory.getServiceClient();

			final Map<String, Object> params = new HashMap<>();
			params.put(POSTILLA, postilla);
			params.put(DOC_IN_PARAM, content);

			final InvocationRequest esRequest = serviceClientFactory.createInvocationRequest(Adobe.PROCESS_NAME_POSTILLA, INVOKE_LITERAL, params, true);
			final InvocationResponse inResp = serviceClient.invoke(esRequest);

			LOGGER.info(ADOBE_RECALL_MESSAGE + Adobe.PROCESS_NAME_POSTILLA + FOR_DOC_LITERAL + idDocumento + DONE);

			final Document outDoc = (Document) inResp.getOutputParameter("docOUT");
			final FileItem fi = (new DiskFileItemFactory()).createItem(outDoc.getAttribute("file").toString(), outDoc.getContentType(), false,
					outDoc.getAttribute("file").toString());

			IOUtils.copy(new ByteArrayInputStream(readDocument(outDoc)), fi.getOutputStream());
			outDoc.dispose();
			return fi.get();
		} catch (final Exception e) {
			LOGGER.error("Errore durante la predisposizione della postilla sul documento con ID=" + idDocumento, e);
			throw new AdobeException(e);
		}
	}

	/**
	 * Metodo per il recupero del contenuto di un documento.
	 * 
	 * @param inDoc documento
	 * @return content del documento
	 */
	public static byte[] readDocument(final Document inDoc) {
		// Determine the total number of bytes to read
		int docLength = (int) inDoc.length();
		final byte[] byteDoc = new byte[docLength];

		// Set up the reading position
		int pos = 0;

		// Loop through the document information, 2048 bytes at a time
		while (docLength > 0) {
			// Read the next chunk of information - Set up the chunk size to prevent a
			// potential memory leak
			final int toRead = Math.min(Adobe.ADOBE_BUFFER_SIZE, docLength);
			final int bytesRead = inDoc.read(pos, byteDoc, pos, toRead);

			// Handle the exception in case data retrieval failed
			if (bytesRead == -1) {

				inDoc.doneReading();
				inDoc.dispose();
				throw new AdobeException("Data retrieval failed!");

			}
			// Update the reading position and number of bytes remaining
			pos += bytesRead;
			docLength -= bytesRead;
		}
		// The document information has been successfully read
		inDoc.doneReading();
		inDoc.dispose();
		return byteDoc;
	}

	/**
	 * Predispone ed effettua la conversione in PDF del doc in input secondo i
	 * parametri inviati in firma al metodo.
	 * 
	 * @param doc
	 * @param preview                   se true, viene generata anche la preview
	 * @param protocollo                la stringa di protocollo da stampigliare sul
	 *                                  documento
	 * @param altezzaFooter
	 * @param spaziaturaFirma
	 * @param commaSeparatedIdFirmatari
	 * @param numeroDocumento
	 * 
	 * @return il documento convertito e, in caso preview sia true, anche la preview
	 * 
	 * @throws AdobeException
	 */
	@Override
	public ConversioneOutputDTO convertiInPdf(final FileItem doc, final boolean preview, final String protocollo, final Integer altezzaFooter, final Integer spaziaturaFirma,
			final String commaSeparatedIdFirmatari, final Integer numeroDocumento, final String documentTitle) {
		ConversioneOutputDTO result = null;

		// Nome del processo
		final String processName = Adobe.PROCESS_NAME_PREDISPOSIZIONE_DOCUMENTONSD;

		try {
		
			result = new ConversioneOutputDTO();

			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[convertiInPdf] Non è possibile comunicare con Adobe LC in regime mock");

				final FileDTO documento = mockSRV.getDocumento(TipoDocumentoMockEnum.PDF_TRASFORMAZIONE_NO_TIMBRO, ServerUtils.getServerFullName());

				final FileItem fi = (new DiskFileItemFactory()).createItem(documento.getFileName(), documento.getMimeType(), false, documento.getFileName());
				IOUtils.copy(new ByteArrayInputStream(documento.getContent()), fi.getOutputStream());
				result.setDocumento(fi);
				if (preview) {
					final FileItem fipreview = (new DiskFileItemFactory()).createItem(documento.getFileName(), documento.getMimeType(), false, documento.getFileName());
					IOUtils.copy(new ByteArrayInputStream(documento.getContent()), fipreview.getOutputStream());
					result.setDocumento(fipreview);
					result.setPreview(fipreview);
				}

				return result;
			}

			LOGGER.info(ADOBE_RECALL_MESSAGE + processName + FOR_DOC_LITERAL + doc.getName());
			final ServiceClient serviceClient = serviceClientFactory.getServiceClient();

			final Document inDoc = new Document(doc.getInputStream());
			inDoc.setAttribute("file", getFileName4Adobe(doc.getName(), documentTitle));
			inDoc.setContentTypeFromBasename();
			inDoc.passivate();

			final Map<String, Object> params = new HashMap<>(10);
			params.put("inDoc", inDoc);
			params.put("preview", (preview ? BooleanFlagEnum.SI.getEnglishValue() : BooleanFlagEnum.NO.getEnglishValue()));

			if (protocollo != null) {
				params.put("protocollo", protocollo);
			}
			if (altezzaFooter != null) {
				params.put("altezzaFooter", altezzaFooter);
			}
			if (spaziaturaFirma != null) {
				params.put("spaziaturaFirma", spaziaturaFirma);
			}
			if (commaSeparatedIdFirmatari != null) {
				params.put("firmatariStringList", commaSeparatedIdFirmatari);
			}
			if (numeroDocumento != null) {
				params.put("idDocumento", numeroDocumento);
			}

			final InvocationRequest esRequest = serviceClientFactory.createInvocationRequest(processName, INVOKE_LITERAL, params, true);
			final InvocationResponse inResp = serviceClient.invoke(esRequest);

			LOGGER.info(ADOBE_RECALL_MESSAGE + processName + FOR_DOC_LITERAL + doc.getName() + DONE);

			final String fileNameTrasformato = getFileNameTrasformato(doc.getName());

			final Document outDoc = (Document) inResp.getOutputParameter(Constants.Adobe.CONVERSIONE_OUTDOC);
			final FileItem fi = (new DiskFileItemFactory()).createItem(fileNameTrasformato, outDoc.getContentType(), false, fileNameTrasformato);
			IOUtils.copy(new ByteArrayInputStream(readDocument(outDoc)), fi.getOutputStream());
			result.setDocumento(fi);
			outDoc.dispose();
			if (inResp.getOutputParameters().get(Constants.Adobe.CONVERSIONE_PREVIEWDOC) != null) {
				final Document previewDoc = (Document) inResp.getOutputParameter(Constants.Adobe.CONVERSIONE_PREVIEWDOC);
				final FileItem fi2 = (new DiskFileItemFactory()).createItem(fileNameTrasformato, previewDoc.getContentType(), false, fileNameTrasformato);
				IOUtils.copy(new ByteArrayInputStream(readDocument(previewDoc)), fi2.getOutputStream());
				result.setPreview(fi2);
				previewDoc.dispose();
			} else {
				LOGGER.warn("!!!! PREVIEW PARAMETER: " + preview + "  - PROCESS OUTPUT VALUE FOR " + Constants.Adobe.CONVERSIONE_PREVIEWDOC + " IS EMPTY !!!");
			}

			inDoc.dispose();
		} catch (IOException | DSCException e) {
			LOGGER.error("Adobe [" + processName + FOR_DOC_LITERAL + doc.getName(), e);
			throw new AdobeException(e.getMessage(), e);
		}

		return result;
	}

	/**
	 * Metodo che conta i campi firma e le firme del documento in input.
	 * 
	 * @param document
	 * @return
	 * @throws AdobeException
	 */
	@Override
	public CountSignsOutputDTO countsSignedFields(final InputStream document) {
		final CountSignsOutputDTO output = new CountSignsOutputDTO();
		final String processName = Adobe.PROCESS_NAME_COUNT_SIGNED_FIELDS;

		try {

			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[countsSignedFields] Non è possibile comunicare con Adobe LC in regime mock");

				output.setNumSignFields(0);
				output.setNumSigns(0);
				output.setNumEmptySignFields(0);
				return output;

			}

			final Document adobeDoc = new Document(document);
			adobeDoc.setContentTypeFromBasename();
			adobeDoc.passivate();

			LOGGER.info(ADOBE_RECALL_MESSAGE + processName + FOR_DOC_LITERAL + adobeDoc.getFile().getName());

			final Map<String, Object> params = new HashMap<>();
			params.put("doc", adobeDoc);

			final ServiceClient serviceClient = serviceClientFactory.getServiceClient();
			final InvocationRequest esRequest = serviceClientFactory.createInvocationRequest(processName, INVOKE_LITERAL, params, true);

			final InvocationResponse inResp = serviceClient.invoke(esRequest);

			output.setNumSignFields((Integer) inResp.getOutputParameter(Adobe.NUM_SIGN_FIELDS));
			output.setNumSigns((Integer) inResp.getOutputParameter(Adobe.NUM_SIGN));
			output.setNumEmptySignFields((Integer) inResp.getOutputParameter(Adobe.NUM_EMPTY_SIGN_FIELDS));
		} catch (final Exception e) {
			LOGGER.error("Adobe[" + processName + "]", e);
			throw new AdobeException(e);
		}

		return output;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.helper.adobe.IAdobeLCHelper#hasTagFirmatario
	 * (org.apache.commons.fileupload.FileItem)
	 */
	@Override
	public int hasTagFirmatario(final FileItem doc) throws IOException {
		return hasTagFirmatario(doc.getName(), doc.getInputStream());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.helper.adobe.IAdobeLCHelper#hasTagFirmatario
	 * (it.ibm.red.business.dto.AllegatoDTO)
	 */
	@Override
	public int hasTagFirmatario(final AllegatoDTO doc) {
		return hasTagFirmatario(doc.getNomeFile(), new ByteArrayInputStream(doc.getContent()));
	}

	@Override
	public int hasTagFirmatario(final AttachmentDTO doc) {
		return hasTagFirmatario(doc.getFileName(), new ByteArrayInputStream(doc.getContent()));
	}

	private int hasTagFirmatario(final String nomeFile, final InputStream is) {
		int isPresent = -1;
		final String processName = Adobe.FIND_TAG_PROCESS_NAME;

		try {

			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[hasTagFirmatario] Non è possibile comunicare con Adobe LC in regime mock");
				return isPresent;
			}

			LOGGER.info(ADOBE_RECALL_MESSAGE + processName + FOR_DOC_LITERAL + nomeFile);
			final ServiceClient serviceClient = serviceClientFactory.getServiceClient();

			final Document adobeDoc = new Document(is);
			adobeDoc.setAttribute("file", nomeFile);
			adobeDoc.setContentTypeFromBasename();
			adobeDoc.passivate();

			final Map<String, Object> params = new HashMap<>(10);
			params.put(DOC_IN_PARAM, adobeDoc);
			params.put("tag", Adobe.TAG_FIRMATARIO);

			final InvocationRequest esRequest = serviceClientFactory.createInvocationRequest(processName, INVOKE_LITERAL, params, true);
			final InvocationResponse inResp = serviceClient.invoke(esRequest);

			isPresent = (Integer) inResp.getOutputParameter(Adobe.OUTPDF_TAG_PRESENT);
		} catch (final Exception e) {
			LOGGER.error("Adobe[" + processName + FOR_DOC_LITERAL + nomeFile, e);
			throw new AdobeException(e);
		}

		return isPresent;
	}

	@Override
	public InputStream htmlToPdf(final byte[] htmlContent) {
		InputStream pdfInputStream = null;

		try {
			final String sContent = new String(htmlContent, StandardCharsets.UTF_8);

			final ITextRenderer itext = new ITextRenderer();

			itext.getSharedContext().setReplacedElementFactory(new RedImageReplacedElementFactory(itext.getSharedContext().getReplacedElementFactory()));

			itext.setDocumentFromString(sContent);
			itext.layout();

			final ByteArrayOutputStream bos = new ByteArrayOutputStream();
			itext.createPDF(bos, true);
			pdfInputStream = new ByteArrayInputStream(bos.toByteArray());
			LOGGER.info("htmlToPdf -> Contenuto convertito correttamente da HTML a PDF.");
		} catch (final Exception e) {
			LOGGER.error("htmlToPdf -> Errore durante la conversione da HTML a PDF.", e);
			throw new AdobeException("htmlToPdf -> Errore durante la conversione da HTML a PDF.", e);
		}

		return pdfInputStream;
	}

	/**
	 * Factory per i replaced element.
	 */
	public class RedImageReplacedElementFactory implements ReplacedElementFactory {

		/**
		 * Base64.
		 */
		private static final String BASE64_ATTRIBUTE = "base64,";
		
		/**
		 * Factory.
		 */
		private final ReplacedElementFactory superFactory;

		/**
		 * Costruttore di RedImageReplacedElementFactory.
		 * 
		 * @param superFactory
		 */
		public RedImageReplacedElementFactory(final ReplacedElementFactory superFactory) {
			this.superFactory = superFactory;
		}

		/**
		 * Crea un replaced element.
		 */
		@Override
		public ReplacedElement createReplacedElement(final LayoutContext layoutContext, final BlockBox blockBox, final UserAgentCallback userAgentCallback, final int cssWidth,
				final int cssHeight) {

			final Element element = blockBox.getElement();
			if (element == null) {
				return null;
			}

			final String nodeName = element.getNodeName();
			final String src = element.getAttribute("src");
			if ("img".equals(nodeName) && src != null && src.indexOf(BASE64_ATTRIBUTE) != -1) {

				final String base64encoded = src.substring(src.indexOf(BASE64_ATTRIBUTE) + BASE64_ATTRIBUTE.length());
				final byte[] decodedBytes = Base64.decode(base64encoded);

				try (InputStream input = new ByteArrayInputStream(decodedBytes)) {

					final byte[] bytes = IOUtils.toByteArray(input);
					final com.lowagie.text.Image image = com.lowagie.text.Image.getInstance(bytes);
					final FSImage fsImage = new ITextFSImage(image);

					if ((cssWidth != -1) || (cssHeight != -1)) {
						fsImage.scale(cssWidth, cssHeight);
					}
					return new ITextImageElement(fsImage);

				} catch (IOException | BadElementException e) {
					LOGGER.warn("Errore in fase di rendering delle immagini", e);
				}
			}

			return superFactory.createReplacedElement(layoutContext, blockBox, userAgentCallback, cssWidth, cssHeight);
		}

		/**
		 * Effettua il reset di {@link #superFactory}.
		 */
		@Override
		public void reset() {
			superFactory.reset();
		}

		/**
		 * Rimuove l'elemento da {@link #superFactory}.
		 */
		@Override
		public void remove(final Element e) {
			superFactory.remove(e);
		}

		/**
		 * Imposta il form submission listener su {@link #superFactory}.
		 */
		@Override
		public void setFormSubmissionListener(final FormSubmissionListener listener) {
			superFactory.setFormSubmissionListener(listener);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.ibm.red.business.helper.adobe.IAdobeLCHelper#insertStampigliatura(java.io.
	 * InputStream, java.lang.String)
	 */
	@Override
	public InputStream insertWatermark(final InputStream documento, final String testo) {
		InputStream output = null;

		try {

			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[insertWatermark] Non è possibile comunicare con Adobe LC in regime mock");
				final FileDTO doc = mockSRV.getDocumento(TipoDocumentoMockEnum.PDF_ANNULLAMENTO, ServerUtils.getServerFullName());
				output = new ByteArrayInputStream(doc.getContent());
				return output;
			}

			final Document documentoAdobe = new Document(documento);

			final FileItem fi = insertStampigliatura(documentoAdobe, testo);

			output = fi.getInputStream();
		} catch (final IOException e) {
			LOGGER.error("Si è verificato un errore durante l'inserimento della seguente stampigliatura: " + testo, e);
			throw new RedException("Errore durante l'inserimento della seguente stampigliatura: " + testo, e);
		}

		return output;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.ibm.red.business.helper.adobe.IAdobeLCHelper#insertStampigliatura(byte[],
	 * java.lang.String)
	 */
	@Override
	public byte[] insertWatermark(final byte[] documento, final String testo) {
		final Document documentoAdobe = new Document(documento);
		documentoAdobe.setContentTypeFromBasename();
		documentoAdobe.passivate();

		final FileItem fi = insertStampigliatura(documentoAdobe, testo);

		return fi.get();
	}

	/**
	 * @param documento
	 * @param testo
	 * @return
	 */
	private FileItem insertStampigliatura(final Document documento, final String testo) {
		FileItem fi = null;

		try {

			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[insertStampigliatura] Non è possibile comunicare con Adobe LC in regime mock");

				final FileDTO doc = mockSRV.getDocumento(TipoDocumentoMockEnum.PDF_INSERT_STAMPIGLIATURA, ServerUtils.getServerFullName());
				fi = (new DiskFileItemFactory()).createItem(doc.getFileName(), doc.getMimeType(), false, doc.getFileName());
				IOUtils.copy(new ByteArrayInputStream(doc.getContent()), fi.getOutputStream());
				return fi;
			}

			final ServiceClient serviceClient = serviceClientFactory.getServiceClient();

			final Map<String, Object> params = new HashMap<>(3);
			params.put("inDoc1", documento);
			params.put("stampigliatura", testo);

			final InvocationRequest esRequest = serviceClientFactory.createInvocationRequest(Adobe.PROCESS_NAME_INSERT_STAMPIGLIATURA_FWS, INVOKE_LITERAL, params, true);
			final InvocationResponse inResp = serviceClient.invoke(esRequest);

			final Document outDoc = (Document) inResp.getOutputParameter("newDoc");
			fi = (new DiskFileItemFactory()).createItem(null, ContentType.PDF, false, FILENAME_TEMP_PDF);
			IOUtils.copy(outDoc.getInputStream(), fi.getOutputStream());
			outDoc.dispose();
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento della seguente stampigliatura: " + testo, e);
			throw new AdobeException(e.getMessage(), e);
		}

		return fi;
	}

	/**
	 * Genera il pdf delle approvazioni da stampigliare.
	 * 
	 * @param codiceAoo              dell'utente che esegue l'operazione
	 * @param numeroDocumento        identificativo che l'utente da al documento
	 * @param descrizioneIspettorato descrizione testuale dell'ispettorato
	 * @param approvazioneList       Una lista di approvazioni da stampigliare
	 * @return
	 */
	@Override
	public InputStream generateApprovazioniPdf(final String codiceAoo, final FormType form) {
		InputStream approvazioniPdf = null;

		try {

			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[generateApprovazioniPdf] Non è possibile comunicare con Adobe LC in regime mock");
				final FileDTO doc = mockSRV.getDocumento(TipoDocumentoMockEnum.PDF_APPROVAZIONI, ServerUtils.getServerFullName());
				approvazioniPdf = new ByteArrayInputStream(doc.getContent());
				return approvazioniPdf;
			}

			final ServiceClient serviceClient = serviceClientFactory.getServiceClient();
			final InputStream formIO = JAXBSerializer.marshal(form).getInputStream();
			final Document document = new Document(formIO);
			document.setContentType("text/xml");
			document.passivate();

			final Map<String, Object> params = new HashMap<>();
			params.put("xmldata", document);

			String processName = pp.getParameterByString(codiceAoo + PropertiesNameEnum.STAMPA_APPROVAZIONI_ADOBE_PROCESS_NAME.getKey());
			if (StringUtils.isBlank(processName)) {
				processName = Adobe.PROCESS_NAME_STAMPA_APPROVAZIONI_DEFAULT;
			}

			LOGGER.info("INVOKE " + processName);
			final InvocationRequest esRequest = serviceClientFactory.createInvocationRequest(processName, INVOKE_LITERAL, params, true);
			final InvocationResponse inResp = serviceClient.invoke(esRequest);
			LOGGER.info(processName + " DONE ");
			LOGGER.info(" retrieving output ");

			final Document outDoc = (Document) inResp.getOutputParameter("outpdf");
			approvazioniPdf = outDoc.getInputStream();
			outDoc.dispose();
		} catch (final Exception e) {
			LOGGER.error("Errore durante la generazione dei Pdf", e);
			throw new AdobeException(e.getMessage(), e);
		}

		return approvazioniPdf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.ibm.red.business.helper.adobe.IAdobeLCHelper#insertPostillaCopiaConforme(
	 * java.lang.String, byte[])
	 */
	@Override
	public byte[] insertPostillaCopiaConforme(final String reason, final byte[] content) {
		byte[] output = null;

		try {

			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[insertPostillaCopiaConforme] Non è possibile comunicare con Adobe LC in regime mock");
				final FileDTO doc = mockSRV.getDocumento(TipoDocumentoMockEnum.PDF_POSTILLA_COPIA_CONFORME, ServerUtils.getServerFullName());
				return doc.getContent();
			}

			LOGGER.info(ADOBE_RECALL_MESSAGE + Adobe.PROCESS_NAME_POSTILLA_COPIA_CONFORME + "] per il documento");
			final ServiceClient serviceClient = serviceClientFactory.getServiceClient();

			final Document inDoc = new Document(content);
			inDoc.setContentTypeFromBasename();
			inDoc.passivate();

			final Map<String, Object> params = new HashMap<>();
			params.put("inDoc1", inDoc);
			params.put("stampigliatura", reason);

			final InvocationRequest esRequest = serviceClientFactory.createInvocationRequest(Adobe.PROCESS_NAME_POSTILLA_COPIA_CONFORME, INVOKE_LITERAL, params, true);
			final InvocationResponse inResp = serviceClient.invoke(esRequest);

			final Document outDoc = (Document) inResp.getOutputParameter("newDoc");
			final FileItem fi = (new DiskFileItemFactory()).createItem(null, ContentType.PDF, false, FILENAME_TEMP_PDF);

			IOUtils.copy(new ByteArrayInputStream(readDocument(outDoc)), fi.getOutputStream());
			outDoc.dispose();

			output = fi.get();
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento della postilla per copia conforme", e);
			throw new AdobeException(e);
		}

		return output;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.ibm.red.business.helper.adobe.IAdobeLCHelper#trasformazionePDF(java.lang.
	 * String, byte[], boolean)
	 */
	@Override
	public byte[] trasformazionePDF(final String fileName, final byte[] content, final boolean flagPDFA, final String documentTitle) {
		byte[] output = null;

		final String fileTypeSettings = pp.getParameterByKey(PropertiesNameEnum.FILE_TYPE_SETTINGS_ADOBE_KEY);
		final String adobePDFSettings = pp.getParameterByKey(PropertiesNameEnum.ADOBE_PDF_SETTINGS_ADOBE_KEY);
		final String securitySettings = pp.getParameterByKey(PropertiesNameEnum.SECURITY_SETTINGS_ADOBE_KEY);

		try {

			final String fileName4Adobe = getFileName4Adobe(fileName, documentTitle);

			if (FileUtils.isHtmlFile(fileName)) {
				if (flagPDFA) {
					throw new RedException("Non è prevista la conversione in PDF/A di HTML.");
				}

				output = htmlToPdfTrasformazionePDF(content);
			} else {

				if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
						&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
					LOGGER.warn("[trasformazionePDF] Non è possibile comunicare con Adobe LC in regime mock");

					final FileDTO doc = mockSRV.getDocumento(TipoDocumentoMockEnum.PDF_TRASFORMAZIONE_TIMBRO, ServerUtils.getServerFullName());
					output = doc.getContent();
					return output;
				}

				if (flagPDFA) {
					output = convertiInPDFConProcesso(pp.getParameterByKey(PropertiesNameEnum.PARAM_APPLICAZIONE_PROCESS_PDF_A_ADOBE_KEY), fileName, content);
				} else {
					final Document adobeInDocument = new Document(content);

					final GeneratePdfServiceClient pdfGenClient = new GeneratePdfServiceClient(serviceClientFactory);
					final CreatePDFResult result = pdfGenClient.createPDF2(adobeInDocument, fileName4Adobe, fileTypeSettings, adobePDFSettings, securitySettings, null, null);

					final Document adobeOutDocument = result.getCreatedDocument();
					output = readDocument(adobeOutDocument);
					adobeInDocument.dispose();
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la conversione in PDF del documento: " + fileName, e);
			throw new AdobeException(e.getMessage(), e);
		}

		return output;
	}

	private byte[] convertiInPDFConProcesso(final String applicazione, final String fileName, final byte[] content) {
		byte[] output = null;

		try {
			// Parametri di input
			final Map<String, Object> parameters = new HashMap<>();
			parameters.put("inDocument", content);
			parameters.put("estensione", FilenameUtils.getExtension(fileName));
			parameters.put("applicazione", applicazione);

			final InvocationRequest request = serviceClientFactory.createInvocationRequest(Adobe.PROCESS_NAME_CONVERT_PDF_A, INVOKE_LITERAL, parameters, true);
			final InvocationResponse response = serviceClientFactory.getServiceClient().invoke(request);

			final Document adobeOutDocument = (Document) response.getOutputParameter("outDocument");
			output = readDocument(adobeOutDocument);
			adobeOutDocument.dispose();
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la conversione in PDF tramite il processo: " + Adobe.PROCESS_NAME_CONVERT_PDF_A, e);
			throw new AdobeException(e.getMessage(), e);
		}

		return output;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.helper.adobe.IAdobeLCHelper#insertCampoFirma
	 * (byte[], java.lang.String, java.lang.String, java.util.List,
	 * java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public byte[] insertCampoFirma(final byte[] contentOriginale, final String contentType, final String nomeFile, final List<Integer> firmatari, final Integer altezzaFooter,
			final Integer spaziaturaFirma) {
		byte[] output = null;

		try {
			if (firmatari == null || firmatari.size() > 3 || firmatari.isEmpty()) {
				throw new RedException("Il processo Adobe prevede la presenza di un numero di campi firma compreso tra 1 e 3.");
			}

			final FileItem doc = new DiskFileItemFactory().createItem(nomeFile, contentType, false, nomeFile);
			IOUtils.copy(new ByteArrayInputStream(contentOriginale), doc.getOutputStream());

			final ConversioneOutputDTO adobeOutput = convertiInPdf(doc, false, null, altezzaFooter, spaziaturaFirma, StringUtils.join(firmatari, ","), null,
					UUID.randomUUID().toString());

			if (adobeOutput != null && adobeOutput.getDocumento().getSize() > 0) {
				output = adobeOutput.getDocumento().get();
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'inserimento del campo firma.", e);
			throw new AdobeException(e.getMessage(), e);
		}

		return output;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.helper.adobe.IAdobeLCHelper#insertCampoFirmaFws
	 * (byte[], java.util.List, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public byte[] insertCampoFirmaFws(final byte[] content, final List<Integer> firmatari, final Integer altezzaFooter, final Integer spaziaturaFirma) {
		byte[] output = null;

		try {
			if (firmatari == null || firmatari.size() > 3 || firmatari.isEmpty()) {
				throw new RedException("Il processo Adobe prevede la presenza di un numero di campi firma compreso tra 1 e 3.");
			}

			// Parametri di input
			final Map<String, Object> parameters = new HashMap<>();
			parameters.put(DOC_IN_PARAM, content);
			parameters.put("firmatari", firmatari);
			if (altezzaFooter != null) {
				parameters.put("altezzaFooter", altezzaFooter);
			}
			if (spaziaturaFirma != null) {
				parameters.put("spaziaturaFirma", spaziaturaFirma);
			}
			// Se non vengono forniti firmatari il processo va in errore, se vengono forniti
			// i successivi parametri sono ignorati, ma se non forniti il processo va in
			// errore.
			parameters.put("protocolloPreFirma", "N");
			parameters.put("tagFirma", "S");
			parameters.put(POSTILLA, "N");

			// Il processo crea tanti campi firma quanti sono gli id dei firmatari; i nomi
			// dei campi firma saranno del tipo campofirma_n dove n è l'id del firmatario
			// n_esimo.
			// Invocazione del servizio
			final InvocationRequest request = serviceClientFactory.createInvocationRequest(Adobe.PROCESS_NAME_INSERISCI_CAMPO_FIRMA, INVOKE_LITERAL, parameters, true);
			final InvocationResponse response = serviceClientFactory.getServiceClient().invoke(request);

			// Recupero del documento
			final Document adobeOutDocument = (Document) response.getOutputParameter("doc");
			output = readDocument(adobeOutDocument);
			adobeOutDocument.dispose();
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'inserimento del campo firma.", e);
			throw new AdobeException(e.getMessage(), e);
		}

		return output;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.helper.adobe.IAdobeLCHelper#insertApprovazione
	 * (byte[], java.lang.String, java.lang.String, java.lang.String, boolean)
	 */
	@Override
	public final byte[] insertApprovazione(final byte[] contentOriginale, final String contentType, final String testoApprovazione, 
			final boolean toRight) {
		byte[] outputContent = null;

		try {

			if (toRight) {
				outputContent = predisposizioneFirmaDigitale(null, testoApprovazione, null, " ", null, contentOriginale);
			} else {
				outputContent = predisposizioneFirmaDigitale(testoApprovazione, null, null, " ", null, contentOriginale);
			}

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il processo di Adobe LC per la stampigliatura dell'approvazione", e);
			throw new AdobeException("Errore durante il processo di Adobe LC per la stampigliatura dell'approvazione", e);
		}

		return outputContent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.helper.adobe.IAdobeLCHelper#insertIdDocumento
	 * (byte[], java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	public byte[] insertIdDocumento(final byte[] contentOriginale, final String contentType, final String nomeFile, final Integer idDocumento) {
		byte[] outputContent = null;

		try {
			final FileItem doc = new DiskFileItemFactory().createItem(nomeFile, contentType, false, nomeFile);
			IOUtils.copy(new ByteArrayInputStream(contentOriginale), doc.getOutputStream());

			final ConversioneOutputDTO adobeOutput = convertiInPdf(doc, false, null, null, null, null, idDocumento, UUID.randomUUID().toString());

			if (adobeOutput != null && adobeOutput.getDocumento().getSize() > 0) {
				outputContent = adobeOutput.getDocumento().get();
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il processo di Adobe LC per la stampigliatura dell'ID documento", e);
			throw new AdobeException("Errore durante il processo di Adobe LC per la stampigliatura dell'ID documento", e);
		}

		return outputContent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.helper.adobe.IAdobeLCHelper#insertProtocollo
	 * (byte[], java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public byte[] insertProtocollo(final byte[] contentOriginale, final String contentType, final String nomeFile, final String protocollo) {
		byte[] outputContent = null;

		try {
			final FileItem doc = new DiskFileItemFactory().createItem(nomeFile, contentType, false, nomeFile);
			IOUtils.copy(new ByteArrayInputStream(contentOriginale), doc.getOutputStream());

			final ConversioneOutputDTO adobeOutput = convertiInPdf(doc, false, protocollo, null, null, null, null, UUID.randomUUID().toString());

			if (adobeOutput != null && adobeOutput.getDocumento().getSize() > 0) {
				outputContent = adobeOutput.getDocumento().get();
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il processo di Adobe LC per la stampigliatura del protocollo", e);
			throw new AdobeException("Errore durante il processo di Adobe LC per la stampigliatura del protocollo", e);
		}

		return outputContent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.helper.adobe.IAdobeLCHelper#insertPostilla (byte[],
	 * java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public byte[] insertPostilla(final byte[] contentOriginale, final String contentType, final String nomeFile, final String postilla) {
		byte[] outputContent = null;

		try {
			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED)) && 
					"true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[insertPostilla] Non è possibile comunicare con Adobe LC in regime mock");

				final FileDTO documento = mockSRV.getDocumento(TipoDocumentoMockEnum.PDF_INSERT_POSTILLA, ServerUtils.getServerFullName());
				return documento.getContent();
			}

			LOGGER.info(ADOBE_RECALL_MESSAGE + Adobe.PROCESS_NAME_POSTILLA + "] per il documento");

			final ServiceClient serviceClient = serviceClientFactory.getServiceClient();

			final Map<String, Object> params = new HashMap<>();
			params.put(POSTILLA, postilla);

			// Se il content type del content originale non è PDF, si converte quest'ultimo
			// in formato PDF
			if (!ContentType.PDF.equalsIgnoreCase(contentType)) {
				params.put(DOC_IN_PARAM, trasformazionePDF(nomeFile, contentOriginale, false, UUID.randomUUID().toString()));
			} else {
				params.put(DOC_IN_PARAM, contentOriginale);
			}

			final InvocationRequest esRequest = serviceClientFactory.createInvocationRequest(Adobe.PROCESS_NAME_POSTILLA, INVOKE_LITERAL, params, true);
			final InvocationResponse inResp = serviceClient.invoke(esRequest);

			final Document outDoc = (Document) inResp.getOutputParameter("docOUT");
			final FileItem fi = (new DiskFileItemFactory()).createItem(null, ContentType.PDF, false, FILENAME_TEMP_PDF);
			IOUtils.copy(new ByteArrayInputStream(readDocument(outDoc)), fi.getOutputStream());
			outDoc.dispose();

			outputContent = fi.get();
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il processo di Adobe LC per la stampigliatura della postilla", e);
			throw new AdobeException("Errore durante il processo di Adobe LC per la stampigliatura della postilla", e);
		}

		return outputContent;
	}

	/**
	 * Esegue la trasformazione del file html il cui contenuto è {@code buf} in un
	 * file pdf. Il metodo esegue la trasformazione tentando diverse modalità di
	 * trasformazione utilizzando il metodo:
	 * {@link #tryTransformation(byte[], Whitelist)}.
	 * 
	 * @param buf
	 *            Content del file Html da trasformare.
	 * @return Byte array del PDF generato o byte array vuoto se la trasformazione
	 *         non è riuscita.
	 */
	private byte[] htmlToPdfTrasformazionePDF(final byte[] buf) {

		try {

			byte[] tempBuf = tryTransformation(buf, Whitelist.relaxed());

			if (tempBuf == null || tempBuf.length == 0) {
				tempBuf = tryTransformation(buf, Whitelist.basicWithImages());
			}

			if (tempBuf == null || tempBuf.length == 0) {
				tempBuf = tryTransformation(buf, Whitelist.basic());
			}

			if (tempBuf == null || tempBuf.length == 0) {
				tempBuf = tryTransformation(buf, Whitelist.simpleText());
			}

			if (tempBuf == null || tempBuf.length == 0) {
				tempBuf = tryTransformation(buf, Whitelist.none());
			}

			return tempBuf;

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la trasformazione di HTML in PDF utilizzando tutte le modalità.", e);
			throw e;
		}

	}
	
	/**
	 * Esegue la trasformazione del file html il cui content è {@code buf} in un
	 * file PDF utilizzando la modalità specificata da {@code transformationType}.
	 * 
	 * @param buf
	 *            Content del file html da trasformare.
	 * @param transformationType
	 *            Modalità di trasformazione da adottare.
	 * @return Byte array del pdf generato o un byte array vuoto se la trasformazione non
	 *         va a buon fine.
	 */
	private byte[] tryTransformation(byte[] buf, Whitelist transformationType) {
		byte [] transformedContent = {};
		try {
			transformedContent = htmlToPdfTrasformazionePDF(buf, transformationType);
		} catch (final Exception e) {
			LOGGER.warn("Si è verificato un errore durante la trasformazione di HTML in PDF in [" + transformationType + "] mode.", e);
		}
		return transformedContent;
	}

	private byte[] htmlToPdfTrasformazionePDF(final byte[] buf, final Whitelist cleanMode) {
		byte[] output = null;

		try {
			String replacedString = new String(buf);
			replacedString = Jsoup.clean(replacedString, cleanMode);

			final ByteArrayInputStream content = new ByteArrayInputStream(replacedString.getBytes());
			final com.lowagie.text.Document pdfDocument = new com.lowagie.text.Document();
			final Reader htmlreader = new BufferedReader(new InputStreamReader(content));
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			com.lowagie.text.pdf.PdfWriter.getInstance(pdfDocument, baos);
			pdfDocument.open();
			final StyleSheet css = new StyleSheet();
			css.loadTagStyle("body", "face", "Garamond");
			final ArrayList<?> arrayElementList = HTMLWorker.parseToList(htmlreader, css);

			if (arrayElementList.isEmpty()) {

				pdfDocument.add(new Chunk(""));

			} else {
				for (int i = 0; i < arrayElementList.size(); ++i) {
					final com.lowagie.text.Element e = (com.lowagie.text.Element) arrayElementList.get(i);
					pdfDocument.add(e);
				}
			}
			pdfDocument.close();

			output = baos.toByteArray();

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la trasformazione di HTML in PDF.", e);
			throw new RedException(e);
		}

		return output;
	}

	private String getFileName4Adobe(final String fileName, final String documentTitle) {
		if (StringUtils.isEmpty(documentTitle)) {
			return fileName;
		}

		final String extension = FilenameUtils.getExtension(fileName);
		String fileName4Adobe = documentTitle;
		if (!StringUtils.isEmpty(extension)) {
			fileName4Adobe = fileName4Adobe + "." + extension;
		}

		return fileName4Adobe;
	}

	private static String getFileNameTrasformato(final String fileName) {
		return FilenameUtils.getBaseName(fileName) + ".pdf";
	}
}