package it.ibm.red.business.enums;

/**
 * Tutti i tipi di accesso alle funzionalità di Evo.
 */
public enum AccessoFunzionalitaEnum {
	
	/**
	 * Identificativo funzionalità.
	 */
	MENUSCRIVANIA,
	
	/**
	 * Identificativo funzionalità.
	 */
	LIBROFIRMA,
	
	/**
	 * Identificativo funzionalità.
	 */
	LIBROFIRMADELEGATO,
	
	/**
	 * Identificativo funzionalità.
	 */
	DALAVORARE,
	
	/**
	 * Identificativo funzionalità.
	 */
	INSOSPESO,
	
	/**
	 * Identificativo funzionalità.
	 */
	STORNATI,
	
	/**
	 * Identificativo funzionalità.
	 */
	PROCEDIMENTI,
	
	/**
	 * Identificativo funzionalità.
	 */
	MAIL,
	
	/**
	 * Identificativo funzionalità.
	 */
	MENUUFFICIO,
	
	/**
	 * Identificativo funzionalità.
	 */
	CORRIERE,
	
	/**
	 * Identificativo funzionalità.
	 */
	ASSEGNATE,
	
	/**
	 * Identificativo funzionalità.
	 */
	CHIUSE,
	
	/**
	 * Identificativo funzionalità.
	 */
	GIROVISTI,
	
	/**
	 * Identificativo funzionalità.
	 */
	SPEDIZIONE,
	
	/**
	 * Identificativo funzionalità.
	 */
	MENUARCHIVIO,
	
	/**
	 * Identificativo funzionalità.
	 */
	ATTI,
	
	/**
	 * Identificativo funzionalità.
	 */
	RISPOSTA,
	
	/**
	 * Identificativo funzionalità.
	 */
	MOZIONE,
	
	/**
	 * Identificativo funzionalità.
	 */
	FALDONI,
	
	/**
	 * Identificativo funzionalità.
	 */
	MENUCREAZIONE,
	
	/**
	 * Identificativo funzionalità.
	 */
	DOCINGRESSO,
	
	/**
	 * Identificativo funzionalità.
	 */
	DOCUSCITA,
	
	/**
	 * Identificativo funzionalità.
	 */
	FALDONE,
	
	/**
	 * Identificativo funzionalità.
	 */
	MENURICERCA,
	
	/**
	 * Identificativo funzionalità.
	 */
	DOCUMENTI,
	
	/**
	 * Identificativo funzionalità.
	 */
	FASCICOLI,
	
	/**
	 * Identificativo funzionalità.
	 */
	RICERCASIGI,
	
	/**
	 * Identificativo funzionalità.
	 */
	RICERCAFEPA,
	
	/**
	 * Identificativo funzionalità.
	 */
	RICERCAPROTOCOLLO,
	
	/**
	 * Identificativo funzionalità.
	 */
	RICERCANPS,
	
	/**
	 * Identificativo funzionalità.
	 */
	STAMPAREGISTRO,
	
	/**
	 * Identificativo funzionalità.
	 */
	MENURUBRICA,
	
	/**
	 * Identificativo funzionalità.
	 */
	RUBRICA,
	
	/**
	 * Identificativo funzionalità.
	 */
	MENUEVENTI,
	
	/**
	 * Identificativo funzionalità.
	 */
	CALENDARIO,
	
	/**
	 * Identificativo funzionalità.
	 */
	SOTTOSCRIZIONI,
	
	/**
	 * Identificativo funzionalità.
	 */
	NOTIFICHE,
	
	/**
	 * Identificativo funzionalità.
	 */
	MENUCONSULTA,
	
	/**
	 * Identificativo funzionalità.
	 */
	ORGANIGRAMMA,
	
	/**
	 * Identificativo funzionalità.
	 */
	CLASSIFICAZIONEFASCICOLI,
	
	/**
	 * Identificativo funzionalità.
	 */
	MENUUTENTE,
	
	/**
	 * Identificativo funzionalità.
	 */
	UFFICIO,
	
	/**
	 * Identificativo funzionalità.
	 */
	PERSONALIZZA,
	
	/**
	 * Identificativo funzionalità.
	 */
	HELP,
	
	/**
	 * Identificativo funzionalità.
	 */
	LOGOUT,
	
	/**
	 * Identificativo funzionalità.
	 */
	MENUFEPA,
	
	/**
	 * Identificativo funzionalità.
	 */
	FATTURE,
	
	/**
	 * Identificativo funzionalità.
	 */
	DECRETI,
	
	/**
	 * Identificativo funzionalità.
	 */
	DSR,
	
	/**
	 * Identificativo funzionalità.
	 */
	MENUDASHBOARD,
	
	/**
	 * Identificativo funzionalità.
	 */
	CAMBIOUFFICIO,
//	FATTURE_ELETTRONICHE,
	
	/**
	 * Identificativo funzionalità.
	 */
	DECRETI_DIRIGENTE,
	
	/**
	 * Identificativo funzionalità.
	 */
	DECRETI_DECRETI_DIRIGENTE, //union di decreti e decreti_dirigente
	
	/**
	 * Identificativo funzionalità.
	 */
	REPORT,
	
	/**
	 * Identificativo funzionalità.
	 */
	PROTOCOLLA_DSR,
	
	/**
	 * Identificativo funzionalità.
	 */
	SCADENZARIO,
	
	/**
	 * Identificativo funzionalità.
	 */
	MULTI_NOTE,
	
	/**
	 * Identificativo funzionalità.
	 */
	CANCELLAZIONE_NOTE,
	
	/**
	 * Identificativo funzionalità.
	 */
	DOCUMENTI_CARTACEI,
	
	/**
	 * Identificativo funzionalità.
	 */
	DOCUMENTI_CARTACEI_ACQUISITI_ELIMINATI,
	
	/**
	 * Identificativo funzionalità.
	 */
	DASHBOARD,
	
	/**
	 * Identificativo funzionalità.
	 */
	ASSEGNAZIONE_DELEGHE,
	
	/**
	 * Identificativo funzionalità.
	 */
	RICERCA_REGISTRAZIONI_AUSILIARIE,
	
	/**
	 * Identificativo funzionalità.
	 */
	WIZARD_UCB,
	
	/**
	 * Identificativo funzionalità.
	 */
	RICERCANPSSENZAACL,
	
	/**
	 * Identificativo funzionalità.
	 */
	MODIFICA_METADATI_MINIMI;
	
}
