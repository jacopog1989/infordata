package it.ibm.red.business.service.concrete;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;

/**
 * The Class AbstractService.
 *
 * @author CPIERASC
 * 
 *         Servizio astratto.
 */
@Service
@Component
public abstract class AbstractService implements Serializable {

	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 870034310469705743L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AbstractService.class.getName());

	/**
	 * Datasource.
	 */
	@Autowired
	@Qualifier("DataSource")
	private transient DataSource dataSource;

	/**
	 * Datasource filenet.
	 */
	@Autowired
	@Qualifier("FilenetDataSource")
	private transient DataSource filenetDataSource;

	/**
	 * Costruttore.
	 */
	protected AbstractService() {
		LOGGER.info("ABSTRACT SRV AbstractService(): Creazione Servizio [" + this.getClass() + "]");
	}

	/**
	 * Getter.
	 * 
	 * @return	datasource database applicativo
	 */
	public DataSource getDataSource() {
		return dataSource;
	}

	/**
	 * Getter.
	 * 
	 * @return	datasouce database filenet
	 */
	public DataSource getFilenetDataSource() {
		return filenetDataSource;
	}

	/**
	 * 
	 * Setup della connessione.
	 * 
	 * @param connection			connessione da gestire
	 * @param inTransactionActive	flag transazione attiva
	 * @return						connessione creata
	 * @throws SQLException			eccezione generabile in fase di setup
	 */
	protected static final Connection setupConnection(final Connection connection, final Boolean inTransactionActive) throws SQLException {
		if (Boolean.TRUE.equals(inTransactionActive)) {
		    // Le operazioni sul DB sono impostate in maniera sequenziale
		    // (No accessi simultanei)
		    connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		    // Auto-commit disabilitato
		    connection.setAutoCommit(false);
		}
		return connection;
	}
	
	/**
	 * 
	 * Chiude la connessione al DB, in maniera safe.
	 *
	 * @param connection
	 *            - Connessione al DB
	 */
	public static final void closeConnection(final Connection connection) {
		try {
			if (connection != null && !connection.isClosed()) {
				connection.close();
			}
		} catch (SQLException e) {
			LOGGER.warn("Chiusura della connessione non risciuta: " + e.getMessage(), e);
		}
	}

	/**
	 * Chiude la transazione al DB.
	 *
	 * @param connection
	 *            - Connessione al DB
	 */
	public static final void closeTransaction(final Connection connection) {
		try {
			if (connection != null && !connection.isClosed()) {
				// salvataggio dati
				connection.commit();
				// Auto-commit attivato
				connection.setAutoCommit(true);
			}
		} catch (SQLException e) {
			LOGGER.warn("Chiusura della transazione non risciuta: " + e.getMessage(), e);
		}
	}

	/**
	 * Commit e chiusura della connessione al DB.
	 *
	 * @param connection
	 *            - Connessione al DB
	 * @throws SQLException
	 *             the SQL exception
	 */
	public static final void commitConnection(final Connection connection) throws SQLException {
		try {
			if (connection != null && !connection.isClosed()) {
				// Salvataggio dei dati su DB
				connection.commit();
			}
		} finally {
			closeConnection(connection);
		}
	}
	
	/** 
	 * Rollback e chiusura della connessione al DB. 
	 * @param connection - Connessione al DB
	 */
	public static final void rollbackConnection(final Connection connection) {
		try {
			if (connection != null && !connection.isClosed()) {
				connection.rollback();
			}
		} catch (SQLException e) {
			LOGGER.warn("Rollback non riuscito: " + e.getMessage(), e);
		}
	}
	
	
	/**
	 * Chiusura della sessione utente del CE FileNet.
	 * 
	 * @param fceh
	 */
	public static final void popSubject(final IFilenetCEHelper fceh) {
		if (fceh != null) {
			fceh.popSubject();
		}
	}
	
	/**
	 * Chiusura della sessione utente verso il PE FileNet.
	 * 
	 * @param fpeh
	 */
	public static final void logoff(final FilenetPEHelper fpeh) {
		if (fpeh != null) {
			fpeh.logoff();
		}
	}

	 
}
