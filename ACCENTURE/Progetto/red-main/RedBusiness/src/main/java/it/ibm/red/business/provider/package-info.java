/**
 * @author CPIERASC
 *
 *	In questo package sono contenuti oggetti che forniscono accesso a specifiche entità di dominio, in particolare è
 *	presente il property provider che consente di disaccoppiare le property con cui configurare il comportamento del
 *	sistema. In pratica sul RDBMS vengono salvate le associazioni chiave-valore, le chiavi verranno poi riportate nel
 *	codice, e tramite il property provider potremmo fare riferimento a queste chiavi per ottenere in automatico il loro
 *	valore (in pratica tale oggetto maschera la logica di accesso al database per il recupero del valore di una chiave).
 *
 */
package it.ibm.red.business.provider;
