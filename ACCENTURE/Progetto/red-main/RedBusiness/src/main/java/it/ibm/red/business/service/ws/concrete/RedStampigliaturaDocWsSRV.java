package it.ibm.red.business.service.ws.concrete;

import java.io.ByteArrayInputStream;
import java.sql.Connection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.dto.DocumentoWsDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.helper.signing.SignHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.persistence.model.PkHandler;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.ITrasformazionePDFSRV;
import it.ibm.red.business.service.concrete.AbstractService;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.business.service.ws.IDocumentoWsSRV;
import it.ibm.red.business.service.ws.IRedStampigliaturaDocWsSRV;
import it.ibm.red.business.service.ws.IStampigliaturaDocWsSRV;
import it.ibm.red.business.service.ws.auth.DocumentServiceAuthorizable;
import it.ibm.red.webservice.model.documentservice.dto.Servizio;
import it.ibm.red.webservice.model.documentservice.types.documentale.Posizione;
import it.ibm.red.webservice.model.documentservice.types.messages.BaseRedContentRequestType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciApprovazioneType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciCampoFirmaType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciIdType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciPostillaType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciProtEntrataType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciProtType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciProtUscitaType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciWatermarkType;

/**
 * 
 * @author m.crescentini
 *
 *         Servizio per la stampigliatura dei documenti (protocollo
 *         entrata/uscita, numero documento, postilla, campo firma), sia tramite
 *         l'apposizione di timbri (firme tecniche) sia tramite i processi di
 *         Adobe LiveCycle.
 */
@Service
public class RedStampigliaturaDocWsSRV extends AbstractService implements IRedStampigliaturaDocWsSRV {

	private static final long serialVersionUID = -2802901963270876312L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RedStampigliaturaDocWsSRV.class.getName());

	/**
	 * Servizio.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IDocumentoWsSRV documentoWsSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IStampigliaturaDocWsSRV stampigliaturaDocWsSRV;

	
	/**
	 * Servizio.
	 */
	@Autowired
	private ISignFacadeSRV signSRV;
	
	/**
	 * Servizio.
	 */
	@Autowired
	private ITrasformazionePDFSRV trasfPDFSRV;

	/**
	 * @see it.ibm.red.business.service.ws.facade.IStampigliaturaDocWsFacadeSRV#
	 *      inserisciStampigliaturaProtEntrata
	 *      (it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedInserisciProtEntrataType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_INSERISCI_PROT_ENTRATA)
	public final DocumentoWsDTO redStampigliaProtocolloEntrata(final RedWsClient client, final RedInserisciProtEntrataType requestStampigliaProtEntrata) {
		return stampigliaDocumento(client, requestStampigliaProtEntrata, "la stampigliatura del protocollo in entrata");
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IStampigliaturaDocWsFacadeSRV#
	 *      stampigliaProtocolloUscita
	 *      (it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedInserisciProtUscitaType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_INSERISCI_PROT_USCITA)
	public final DocumentoWsDTO redStampigliaProtocolloUscita(final RedWsClient client, final RedInserisciProtUscitaType requestStampigliaProtUscita) {
		return stampigliaDocumento(client, requestStampigliaProtUscita, "la stampigliatura del protocollo in uscita");
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IStampigliaturaDocWsFacadeSRV#
	 *      inserisciCampoFirma (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedInserisciCampoFirmaType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_INSERISCI_CAMPO_FIRMA)
	public final DocumentoWsDTO redInserisciCampoFirma(final RedWsClient client, final RedInserisciCampoFirmaType requestInserisciCampoFirma) {
		return stampigliaDocumento(client, requestInserisciCampoFirma, "l'inserimento del campo firma");
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IStampigliaturaDocWsFacadeSRV#
	 *      stampigliaIdDocumento
	 *      (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedInserisciIdType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_INSERISCI_ID_DOCUMENTO)
	public final DocumentoWsDTO redStampigliaIdDocumento(final RedWsClient client, final RedInserisciIdType requestStampigliaIdDocumento) {
		return stampigliaDocumento(client, requestStampigliaIdDocumento, "la stampigliatura dell'ID documento");
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IStampigliaturaDocWsFacadeSRV#
	 *      stampigliaPostilla (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedInserisciPostillaType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_INSERISCI_POSTILLA)
	public final DocumentoWsDTO redStampigliaPostilla(final RedWsClient client, final RedInserisciPostillaType requestStampigliaPostilla) {
		return stampigliaDocumento(client, requestStampigliaPostilla, "la stampigliatura della postilla");
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IStampigliaturaDocWsFacadeSRV#
	 *      stampigliaWatermark (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedInserisciWatermarkType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_INSERISCI_WATERMARK)
	public DocumentoWsDTO redStampigliaWatermark(final RedWsClient client, final RedInserisciWatermarkType requestStampigliaWatermark) {
		return stampigliaDocumento(client, requestStampigliaWatermark, "la stampigliatura del watermark");
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IStampigliaturaDocWsFacadeSRV#
	 *      stampigliaApprovazione
	 *      (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedInserisciApprovazioneType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_INSERISCI_APPROVAZIONE)
	public DocumentoWsDTO redStampigliaApprovazione(final RedWsClient client, final RedInserisciApprovazioneType requestStampigliaApprovazione) {
		return stampigliaDocumento(client, requestStampigliaApprovazione, "la stampigliatura dell'approvazione");
	}

	/**
	 * @param client
	 * @param requestStampigliatura
	 * @param operazione
	 * @return
	 */
	private DocumentoWsDTO stampigliaDocumento(final RedWsClient client, final BaseRedContentRequestType requestStampigliatura, final String operazione) {
		DocumentoWsDTO documentoWs = null;
		Connection con = null;
		IFilenetCEHelper fceh = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final Aoo aoo = aooSRV.recuperaAoo(client.getIdAoo().intValue(), con);

			final AooFilenet aooFilenet = aoo.getAooFilenet();
			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), aoo.getIdAoo());

			final Document docFilenet = fceh.getDocumentForDownload(FilenetCEHelper.DOCUMENT, String.valueOf(requestStampigliatura.getDocumentTitle()));

			if (docFilenet != null) {
				// Per poter apporre la stampigliatura, il file deve essere in formato PDF
				if (ContentType.PDF.equalsIgnoreCase(docFilenet.get_MimeType())) {
					byte[] contentStampigliato = null;

					final byte[] contentOriginale = FilenetCEHelper.getDocumentContentAsByte(docFilenet);
					final String nomeFile = (String) TrasformerCE.getMetadato(docFilenet, PropertiesNameEnum.NOME_FILE_METAKEY);

					// Protocollo entrata
					if (requestStampigliatura instanceof RedInserisciProtEntrataType) {

						contentStampigliato = stampigliaProtocollo((RedInserisciProtType) requestStampigliatura, contentOriginale, docFilenet.get_MimeType(), nomeFile,
								docFilenet.getClassName(), aoo.getPkHandlerTimbro(), aoo.getSignerTimbro(), aoo.getPinTimbro(), true, aoo.getDisableUseHostOnly(),
								aoo.isConfPDFAPerHandler());

						// Protocollo uscita
					} else if (requestStampigliatura instanceof RedInserisciProtUscitaType) {

						contentStampigliato = stampigliaProtocollo((RedInserisciProtType) requestStampigliatura, contentOriginale, docFilenet.get_MimeType(), nomeFile,
								docFilenet.getClassName(), aoo.getPkHandlerTimbro(), aoo.getSignerTimbro(), aoo.getPinTimbro(), false, aoo.getDisableUseHostOnly(),
								aoo.isConfPDFAPerHandler());

						// Campo firma
					} else if (requestStampigliatura instanceof RedInserisciCampoFirmaType) {

						contentStampigliato = inserisciCampoFirma((RedInserisciCampoFirmaType) requestStampigliatura, contentOriginale, docFilenet.get_MimeType(), nomeFile,
								aoo.getPkHandlerTimbro(), aoo.getAltezzaFooter(), aoo.getSpaziaturaFirma());

						// ID (numero) documento
					} else if (requestStampigliatura instanceof RedInserisciIdType) {

						contentStampigliato = stampigliaIdDocumento((RedInserisciIdType) requestStampigliatura, contentOriginale, docFilenet.get_MimeType(), nomeFile,
								aoo.getPkHandlerTimbro(), aoo.getSignerTimbro(), aoo.getPinTimbro(), aoo.getDisableUseHostOnly(), aoo.isConfPDFAPerHandler(),
								aoo.getDescrizione());

						// Postilla
					} else if (requestStampigliatura instanceof RedInserisciPostillaType) {

						//non è necessario controllare se la postilla è attiva visto che il servizio è fatto apposta per generare la postilla.
						
						byte[] imageFirma = signSRV.getImageFirmaAOO (aoo.getIdAoo(), aoo.isConfPDFAPerHandler());
						boolean onlyFirstPage = aoo.getPostillaSoloPag1()!=null && aoo.getPostillaSoloPag1() == 1;
						String postillaAdobe = null;
						if(aoo.getPkHandlerTimbro() == null ) {
							//se non ho l'handler configurato metto postilla con adobe
							postillaAdobe = trasfPDFSRV.getTestoPostilla(aoo.getIdAoo());
						}
						contentStampigliato = stampigliaPostilla(contentOriginale, docFilenet.get_MimeType(), nomeFile, aoo.getPkHandlerTimbro(), aoo.getSignerTimbro(),
							aoo.getPinTimbro(), aoo.getDisableUseHostOnly(), aoo.isConfPDFAPerHandler(),onlyFirstPage, imageFirma, postillaAdobe);
						
						// Approvazione
					} else if (requestStampigliatura instanceof RedInserisciApprovazioneType) {

						contentStampigliato = stampigliaApprovazione((RedInserisciApprovazioneType) requestStampigliatura, contentOriginale, docFilenet.get_MimeType(),
								aoo.getPkHandlerTimbro(), aoo.getSignerTimbro(), aoo.getPinTimbro(), aoo.getDisableUseHostOnly(), aoo.isConfPDFAPerHandler());

						// Watermark
					} else if (requestStampigliatura instanceof RedInserisciWatermarkType) {

						contentStampigliato = stampigliaWatermark((RedInserisciWatermarkType) requestStampigliatura, contentOriginale);

						// Gestione annullamento documento

					}

					// Si inserisce una nuova versione del documento su FileNet, con il content
					// stampigliato
					final Document nuovaVersioneDocFilenet = fceh.addVersion(docFilenet, new ByteArrayInputStream(contentStampigliato), null, nomeFile, ContentType.PDF,
							aoo.getIdAoo());

					if (Boolean.TRUE.equals(requestStampigliatura.isReturnDocument())) {
						documentoWs = documentoWsSRV.transformToDocumentoWs(nuovaVersioneDocFilenet, true, false, aoo.getIdAoo(), fceh, con);
					} else {
						documentoWs = new DocumentoWsDTO(nuovaVersioneDocFilenet.get_MajorVersionNumber());
					}

				} else {
					LOGGER.error("Il documento con document title: " + requestStampigliatura.getDocumentTitle() + " non è in formato PDF");
					throw new RedException("Il documento con document title: " + requestStampigliatura.getDocumentTitle() + " non è in formato PDF");
				}
			} else {
				LOGGER.error("Documento con document title: " + requestStampigliatura.getDocumentTitle() + " non trovato per l'AOO [" + aoo.getCodiceAoo() + "]");
				throw new RedException("Documento con document title: " + requestStampigliatura.getDocumentTitle() + " non trovato per l'AOO [" + aoo.getCodiceAoo() + "]");
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante " + operazione + ": " + e.getMessage(), e);
			throw new RedException("Errore durante " + operazione + ": " + e.getMessage(), e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}

		return documentoWs;
	}

	/**
	 * @param requestStampigliaProtocollo
	 * @param contentOriginale
	 * @param contentType
	 * @param nomeFile
	 * @param classeDocumentale
	 * @param pkHandlerTimbro
	 * @param signerTimbro
	 * @param pinTimbro
	 * @param isEntrata
	 * @return
	 */
	private byte[] stampigliaProtocollo(final RedInserisciProtType requestStampigliaProtocollo, final byte[] contentOriginale, final String contentType, final String nomeFile,
			final String classeDocumentale, final PkHandler pkHandlerTimbro, final String signerTimbro, final String pinTimbro, final boolean isEntrata,
			final boolean disableUseHostOnly, final boolean confPDFA) {
		byte[] contentConProtocollo = null;

		// Se il PK Handler Timbro dell'AOO è configurato, si stampiglia il protocollo
		// con un timbro apposto tramite PK
		if (pkHandlerTimbro != null) {
			// START VI PK HANDLER
			final SignHelper sh = new SignHelper(pkHandlerTimbro.getHandler(), pkHandlerTimbro.getSecurePin(), disableUseHostOnly);

			// Allegato
			if (PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY).equals(classeDocumentale)) {

				if (isEntrata) {
					contentConProtocollo = sh.applicaTimbroProtocolloAllegatoEntrata(signerTimbro, pinTimbro, contentOriginale,
							requestStampigliaProtocollo.getProtocolloStampigliatura(), confPDFA);
				} else {
					contentConProtocollo = sh.applicaTimbroProtocolloAllegatoUscita(signerTimbro, pinTimbro, contentOriginale,
							requestStampigliaProtocollo.getProtocolloStampigliatura(), confPDFA);
				}

				// Documento principale
			} else {

				if (isEntrata) {
					contentConProtocollo = sh.applicaTimbroProtocolloPrincipaleEntrata(signerTimbro, pinTimbro, contentOriginale,
							requestStampigliaProtocollo.getProtocolloStampigliatura(), confPDFA);
				} else {
					contentConProtocollo = sh.applicaTimbroProtocolloPrincipaleUscita(signerTimbro, pinTimbro, contentOriginale,
							requestStampigliaProtocollo.getProtocolloStampigliatura(), confPDFA);
				}
			}

			// ...altrimenti, la stampigliatura di protocollo viene effettuata tramite
			// l'apposito processo di Adobe LC
		} else {

			// Si richiama il processo Adobe
			contentConProtocollo = stampigliaturaDocWsSRV.stampigliaProtocollo(contentOriginale, contentType, nomeFile,
					requestStampigliaProtocollo.getProtocolloStampigliatura(), isEntrata);

		}

		return contentConProtocollo;
	}

	/**
	 * @param requestInserisciCampoFirma
	 * @param contentOriginale
	 * @param contentType
	 * @param nomeFile
	 * @param pkHandlerTimbro
	 * @param altezzaFooter
	 * @param spaziaturaFirma
	 * @return
	 */
	private byte[] inserisciCampoFirma(final RedInserisciCampoFirmaType requestInserisciCampoFirma, final byte[] contentOriginale, final String contentType,
			final String nomeFile, final PkHandler pkHandlerTimbro, final Integer altezzaFooter, final Integer spaziaturaFirma) {
		byte[] contentConCampoFirma = null;

		// Se il PK Handler Timbro dell'AOO è configurato, si inserisce il campo firma
		// tramite iText
		if (pkHandlerTimbro != null) {

			contentConCampoFirma = PdfHelper.inserisciCampiFirma(contentOriginale, requestInserisciCampoFirma.getFirmatari(), altezzaFooter, spaziaturaFirma, false, true);

			// ...altrimenti, si inserisce richiamando l'apposito processo di Adobe LC
		} else {

			// Si richiama il processo Adobe
			contentConCampoFirma = stampigliaturaDocWsSRV.inserisciCampoFirma(contentOriginale, contentType, nomeFile, requestInserisciCampoFirma.getFirmatari(),
					altezzaFooter, spaziaturaFirma);

		}

		return contentConCampoFirma;
	}

	/**
	 * @param requestStampigliaIdDocumento
	 * @param contentOriginale
	 * @param contentType
	 * @param nomeFile
	 * @param pkHandlerTimbro
	 * @param signerTimbro
	 * @param pinTimbro
	 * @param disableUseHostOnly
	 * @param confPDFA
	 * @param descrizioneAoo
	 * @return
	 */
	private byte[] stampigliaIdDocumento(final RedInserisciIdType requestStampigliaIdDocumento, final byte[] contentOriginale, final String contentType, final String nomeFile,
			final PkHandler pkHandlerTimbro, final String signerTimbro, final String pinTimbro, final boolean disableUseHostOnly, final boolean confPDFA,
			final String descrizioneAoo) {
		byte[] contentConIdDocumento = null;

		// Se il PK Handler Timbro dell'AOO è configurato, si stampiglia l'ID del
		// documento con un timbro apposto tramite PK
		if (pkHandlerTimbro != null) {
			final SignHelper sh = new SignHelper(pkHandlerTimbro.getHandler(), pkHandlerTimbro.getSecurePin(), disableUseHostOnly);

			contentConIdDocumento = sh.applicaTimbroNumeroDocumento(signerTimbro, pinTimbro, contentOriginale, requestStampigliaIdDocumento.getIdDocumentoStampigliatura(),
					confPDFA, descrizioneAoo);

			// ...altrimenti, la stampigliatura dell'ID viene effettuata tramite l'apposito
			// processo di Adobe LC
		} else {

			// Si richiama il processo Adobe
			contentConIdDocumento = stampigliaturaDocWsSRV.stampigliaIdDocumento(contentOriginale, contentType, nomeFile,
					requestStampigliaIdDocumento.getIdDocumentoStampigliatura());

		}

		return contentConIdDocumento;
	}

	/**
	 * @param contentOriginale
	 * @param contentType
	 * @param nomeFile
	 * @param pkHandlerTimbro
	 * @param signerTimbro
	 * @param pinTimbro
	 * @return
	 */
	private byte[] stampigliaPostilla(final byte[] contentOriginale, final String contentType, final String nomeFile, final PkHandler pkHandlerTimbro,
			final String signerTimbro, final String pinTimbro, final boolean disableUseHostOnly, final boolean confPDFA, final boolean onlyFirstPage, byte[]imageFirma, String postillaAdobe) {
		byte[] contentConPostilla = null;

		// Se il PK Handler Timbro dell'AOO è configurato, si stampiglia la postila con
		// un timbro apposto tramite PK
		if (pkHandlerTimbro != null) {
			// START VI PK HANDLER
			final SignHelper sh = new SignHelper(pkHandlerTimbro.getHandler(), pkHandlerTimbro.getSecurePin(), disableUseHostOnly);

			contentConPostilla = sh.applicaTimbroPostilla(signerTimbro, pinTimbro, contentOriginale, confPDFA,onlyFirstPage,  imageFirma);

			// ...altrimenti, la stampigliatura della postilla viene effettuata tramite
			// l'apposito processo di Adobe LC
		} else {

			// Si richiama il processo Adobe
			contentConPostilla = stampigliaturaDocWsSRV.stampigliaPostilla(contentOriginale, contentType, nomeFile, postillaAdobe);

		}

		return contentConPostilla;
	}

	/**
	 * N.B. La stampigliatura del watermark viene effettuata solo tramite l'apposito
	 * processo di Adobe Live Cccle, che rompe le eventuali firme presenti nel
	 * documento.
	 * 
	 * @param requestStampigliaWatermark
	 * @param contentOriginale
	 * @return
	 */
	private byte[] stampigliaWatermark(final RedInserisciWatermarkType requestStampigliaWatermark, final byte[] contentOriginale) {
		return stampigliaturaDocWsSRV.stampigliaWatermark(contentOriginale, requestStampigliaWatermark.getTesto());
	}

	/**
	 * @param requestStampigliaApprovazione
	 * @param contentOriginale
	 * @param contentType
	 * @param pkHandlerTimbro
	 * @param signerTimbro
	 * @param pinTimbro
	 * @param disableUseHostOnly
	 * @param confPDFA
	 * @return
	 */
	private byte[] stampigliaApprovazione(final RedInserisciApprovazioneType requestStampigliaApprovazione, final byte[] contentOriginale, final String contentType,
			final PkHandler pkHandlerTimbro, final String signerTimbro, final String pinTimbro, final boolean disableUseHostOnly,
			final boolean confPDFA) {
		byte[] contentConApprovazione = null;

		// Se il PK Handler Timbro dell'AOO è configurato, si stampiglia l'approvazione
		// con un timbro apposto tramite PK
		if (pkHandlerTimbro != null) {
			// START VI PK HANDLER
			final SignHelper sh = new SignHelper(pkHandlerTimbro.getHandler(), pkHandlerTimbro.getSecurePin(), disableUseHostOnly);

			// Gestione della posizione dell'approvazione all'interno del documento
			boolean toRight = false;
			if (Posizione.RIGHT.equals(requestStampigliaApprovazione.getPosizione())) {
				toRight = true;
			}

			contentConApprovazione = sh.applicaTimbroApprovazione(signerTimbro, pinTimbro, contentOriginale, requestStampigliaApprovazione.getTesto(), toRight, confPDFA);

			// ...altrimenti, la stampigliatura dell'approvazione viene effettuata tramite
			// l'apposito processo di Adobe LC
		} else {

			// Si richiama il processo Adobe
			contentConApprovazione = stampigliaturaDocWsSRV.stampigliaApprovazione(contentOriginale, contentType, 
					requestStampigliaApprovazione.getTesto(), requestStampigliaApprovazione.getPosizione());
			
		}

		return contentConApprovazione;
	}

}