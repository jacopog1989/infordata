package it.ibm.red.business.service;

import java.util.List;

import it.ibm.red.business.dto.DetailFascicoloRedDTO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.service.facade.IDetailFascicoloFacadeSRV;

/**
 * Interface del servizio di gestione detail fascicolo.
 */
public interface IDetailFascicoloSRV extends IDetailFascicoloFacadeSRV {
	
	/**
	 * Trasforma il fascicolo nel suo dettaglio.
	 * @param fascicoloDTO
	 * @return dettaglio del fascicolo
	 */
	DetailFascicoloRedDTO fromFascicoloDtoToDetailFascicoloRedDTO(FascicoloDTO fascicoloDTO);
	
	/**
	 * Ottiene i documenti tramite il fascicolo.
	 * @param idFascicolo
	 * @param idAOO
	 * @param fceh
	 * @return lista dei documenti del fascicolo
	 */
	List<DocumentoFascicoloDTO> getDocumentiByIdFascicolo(Integer idFascicolo, Long idAOO, IFilenetCEHelper fceh);
	
	/**
	 * @param idFascicolo
	 * @param idAOO
	 * @param fceh
	 * @return
	 */
	List<FaldoneDTO> getFaldoniByIdFascicolo(String idFascicolo, Long idAOO, IFilenetCEHelper fceh);

}
