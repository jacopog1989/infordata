/**
 * 
 */
package it.ibm.red.business.service;

import java.sql.Connection;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.service.facade.ITracciaDocumentiFacadeSRV;

/**
 * @author APerquoti
 *
 */
public interface ITracciaDocumentiSRV extends ITracciaDocumentiFacadeSRV {

	/**
	 * Rimuove il documento.
	 * @param idUfficio
	 * @param idUtente
	 * @param idRuolo
	 * @param idDocumento
	 * @param con
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO rimuoviDocumento(Long idUfficio, Long idUtente, Long idRuolo, int idDocumento, Connection con);

	/**
	 * Traccia il documento.
	 * @param idUfficio
	 * @param idUtente
	 * @param idRuolo
	 * @param idDocumento
	 * @param con
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO tracciaDocumento(Long idUfficio, Long idUtente, Long idRuolo, int idDocumento, Connection con);

	/**
	 * Verifica se il documento è tracciato.
	 * @param utente
	 * @param idDocumento
	 * @param con
	 * @return true o false
	 */
	boolean isDocumentoTracciato(UtenteDTO utente, int idDocumento, Connection con);
	
}