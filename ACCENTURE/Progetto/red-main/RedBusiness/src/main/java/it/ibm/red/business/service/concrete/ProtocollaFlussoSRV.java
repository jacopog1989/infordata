package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.service.IProtocollaFlussoSRV;

/**
 * Service che gestisce la protocollazione flusso.
 */
@Service
public class ProtocollaFlussoSRV extends AbstractService implements IProtocollaFlussoSRV {
	
	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = -6862535433263116774L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ProtocollaFlussoSRV.class.getName());
	
	/**
	 * DAO.
	 */
	@Autowired
	private INodoDAO nodoDAO;
	
	
	/**
	 * @see it.ibm.red.business.service.facade.IProtocollaFlussoFacadeSRV#getIdNodoByDescAndCodiceAoo(java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public Long getIdNodoByDescAndCodiceAoo(final String descrizioneNodo, final String codiceAoo) {
		Long out = null;
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			
			out = nodoDAO.getIdNodoByDescAndCodiceAoo(descrizioneNodo, codiceAoo, con);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'ID nodo dalla sua descrizione e dal codice AOO. Nodo: " + descrizioneNodo, e);
			throw new RedException("Errore durante il recupero dell'ID nodo dalla sua descrizione e dal codice AOO. Nodo: " + descrizioneNodo, e);
		} finally {
			closeConnection(con);
		}
		
		return out;
	}


	/**
	 * @see it.ibm.red.business.service.facade.IProtocollaFlussoFacadeSRV#getWFSospeso(java.lang.String,
	 *      it.ibm.red.business.persistence.model.AooFilenet).
	 */
	@Override
	public String getWobNumberSospeso(final String documentTitle, final AooFilenet aooFilenet) {
		String out = null;
		FilenetPEHelper fpeh = null;
		
		try {
			final List<Long> dts = new ArrayList<>();
			dts.add(Long.parseLong(documentTitle));
			
			fpeh = new FilenetPEHelper(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(),
					aooFilenet.getStanzaJaas(), aooFilenet.getConnectionPoint());
			
			final VWRosterQuery q = fpeh.getDocumentWorkFlowsByIdDocumenti(dts);
			while (q.hasNext()) {
				final VWWorkObject wf = (VWWorkObject) q.next();
				if (DocumentQueueEnum.SOSPESO.getName().equals(wf.getCurrentQueueName())) {
					out = wf.getWorkflowNumber();
					break;
				}
			}
		} finally {
			logoff(fpeh);
		}
		
		return out;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IProtocollaFlussoFacadeSRV#getWFSospeso(java.lang.String,
	 *      it.ibm.red.business.persistence.model.AooFilenet).
	 */
	@Override
	public String getQueueNameWorkflowPrincipale(final String documentTitle, final AooFilenet aooFilenet) {
		VWWorkObject workflowPrincipale = null;
		FilenetPEHelper fpeh = null;
		String queueName = null;
		try {
			fpeh = new FilenetPEHelper(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(),
					aooFilenet.getStanzaJaas(), aooFilenet.getConnectionPoint());
			
			workflowPrincipale = fpeh.getWorkflowPrincipale(documentTitle, aooFilenet.getIdClientAoo());
			
			if (workflowPrincipale != null) {
				queueName = workflowPrincipale.getCurrentQueueName();
			}
		} finally {
			logoff(fpeh);
		}
		
		return queueName;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IProtocollaFlussoFacadeSRV#getDocumentTitleByIdProcesso(
	 *      it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.enums.TipoContestoProceduraleEnum,
	 *      java.lang.String).
	 */
	@Override
	public String getDocumentTitleByIdProcesso(final AooFilenet aooFilenet, final TipoContestoProceduraleEnum flusso, final String idProcesso) {
		String out = null;
		IFilenetCEHelper fceh = null;
		
		try {
			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), aooFilenet.getAoo().getIdAoo());
			
			out = fceh.getDocumentTitleByIdProcesso(flusso, idProcesso);
		} finally {
			popSubject(fceh);
		}
		
		return out;
	}

}