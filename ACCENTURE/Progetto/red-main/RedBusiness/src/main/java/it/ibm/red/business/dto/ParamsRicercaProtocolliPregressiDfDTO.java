package it.ibm.red.business.dto;

import java.util.Date;

/**
 * The Class ParamsRicercaProtocolliPregressiDfDTO.
 *
 * @author VINGENITO
 */

public class ParamsRicercaProtocolliPregressiDfDTO extends AbstractDTO {

	private static final long serialVersionUID = -6709118116367270434L;

	/**
	 * Oggetto
	 */
	private String oggetto;
	
	/**
	 * Anno protocollo 
	 */
	private Integer annoProtocollo;
	
	/**
	 * Ideficativo Protocollo  
	 */
	private Long idProtocollo;
	
	/**
	 * Flag anno protocollo disabilitato 
	 */
	private boolean annoProtocolloDisabled;
	
	/**
	 * Numero protocollo da 
	 */
	private Integer numProtocolloDa;
	
	/**
	 * Numero protocollo a 
	 */
	private Integer numProtocolloA;
	
	/**
	 * Data protocollazione da 
	 */
	private Date dataProtocollazioneDa;
	 
	/**
	 * Data protocollazione a 
	 */
	private Date dataProtocollazioneA;
	 
	/**
	 * Identificativo registro 
	 */
	private String idRegistro;
	
	/**
	 * Modalita
	 */
	private String modalita;
	  
	/**
	 * Tipologia documento 
	 */
	private String tipologiaDocumento;
	 
	/**
	 * Mittente destinatario protocollo nsd  
	 */
	private MittDestProtocolloNsdDTO mittDestProtocolloNsdDTO;

	/** 
	 *
	 * @return the oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/** 
	 *
	 * @param oggetto the oggetto to set
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/** 
	 *
	 * @return the annoProtocollo
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/** 
	 *
	 * @param annoProtocollo the annoProtocollo to set
	 */
	public void setAnnoProtocollo(final Integer annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/** 
	 *
	 * @return the idProtocollo
	 */
	public Long getIdProtocollo() {
		return idProtocollo;
	}

	/** 
	 *
	 * @param idProtocollo the idProtocollo to set
	 */
	public void setIdProtocollo(final Long idProtocollo) {
		this.idProtocollo = idProtocollo;
	}

	/** 
	 *
	 * @return the annoProtocolloDisabled
	 */
	public boolean isAnnoProtocolloDisabled() {
		return annoProtocolloDisabled;
	}

	/** 
	 *
	 * @param annoProtocolloDisabled the annoProtocolloDisabled to set
	 */
	public void setAnnoProtocolloDisabled(final boolean annoProtocolloDisabled) {
		this.annoProtocolloDisabled = annoProtocolloDisabled;
	}

	/** 
	 *
	 * @return the numProtocolloDa
	 */
	public Integer getNumProtocolloDa() {
		return numProtocolloDa;
	}

	/** 
	 *
	 * @param numProtocolloDa the numProtocolloDa to set
	 */
	public void setNumProtocolloDa(final Integer numProtocolloDa) {
		this.numProtocolloDa = numProtocolloDa;
	}

	/** 
	 *
	 * @return the numProtocolloA
	 */
	public Integer getNumProtocolloA() {
		return numProtocolloA;
	}

	/** 
	 *
	 * @param numProtocolloA the numProtocolloA to set
	 */
	public void setNumProtocolloA(final Integer numProtocolloA) {
		this.numProtocolloA = numProtocolloA;
	}

	/** 
	 *
	 * @return the dataProtocollazioneDa
	 */
	public Date getDataProtocollazioneDa() {
		return dataProtocollazioneDa;
	}

	/** 
	 *
	 * @param dataProtocollazioneDa the dataProtocollazioneDa to set
	 */
	public void setDataProtocollazioneDa(final Date dataProtocollazioneDa) {
		this.dataProtocollazioneDa = dataProtocollazioneDa;
	}

	/** 
	 *
	 * @return the dataProtocollazioneA
	 */
	public Date getDataProtocollazioneA() {
		return dataProtocollazioneA;
	}

	/** 
	 *
	 * @param dataProtocollazioneA the dataProtocollazioneA to set
	 */
	public void setDataProtocollazioneA(final Date dataProtocollazioneA) {
		this.dataProtocollazioneA = dataProtocollazioneA;
	}

	/** 
	 *
	 * @return the idRegistro
	 */
	public String getIdRegistro() {
		return idRegistro;
	}

	/** 
	 *
	 * @param idRegistro the idRegistro to set
	 */
	public void setIdRegistro(final String idRegistro) {
		this.idRegistro = idRegistro;
	}

	/** 
	 *
	 * @return the modalita
	 */
	public String getModalita() {
		return modalita;
	}

	/** 
	 *
	 * @param modalita the modalita to set
	 */
	public void setModalita(final String modalita) {
		this.modalita = modalita;
	}

	/** 
	 *
	 * @return the tipologiaDocumento
	 */
	public String getTipologiaDocumento() {
		return tipologiaDocumento;
	}

	/** 
	 *
	 * @param tipologiaDocumento the tipologiaDocumento to set
	 */
	public void setTipologiaDocumento(final String tipologiaDocumento) {
		this.tipologiaDocumento = tipologiaDocumento;
	}
 

	/** 
	 *
	 * @return the mittDestProtocolloNsdDTO
	 */
	public MittDestProtocolloNsdDTO getMittDestProtocolloNsdDTO() {
		return mittDestProtocolloNsdDTO;
	}

	/** 
	 *
	 * @param mittDestProtocolloNsdDTO the mittDestProtocolloNsdDTO to set
	 */
	public void setMittDestProtocolloNsdDTO(final MittDestProtocolloNsdDTO mittDestProtocolloNsdDTO) {
		this.mittDestProtocolloNsdDTO = mittDestProtocolloNsdDTO;
	}
	 
	
}
