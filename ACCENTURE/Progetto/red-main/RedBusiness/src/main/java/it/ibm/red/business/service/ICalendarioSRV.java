package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.SearchEventiDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.EventoCalendarioEnum;
import it.ibm.red.business.persistence.model.Evento;
import it.ibm.red.business.service.facade.ICalendarioFacadeSRV;

/**
 * Interface del service per la gestione del calendar.
 */
public interface ICalendarioSRV extends ICalendarioFacadeSRV {

	/**
	 * Ottiene gli eventi del calendario.
	 * @param searchEventiDTO - eventi da ricercare
	 * @param utente
	 * @param idEnte - id dell'ente
	 * @param con
	 * @return mappa lista di eventi per evento del calendario
	 */
	Map<EventoCalendarioEnum, List<Evento>> getEventiForCalendario(SearchEventiDTO searchEventiDTO, UtenteDTO utente, Integer idEnte, Connection con);

	/**
	 * Ottiene il conteggio degli eventi del calendario.
	 * @param searchEventiDTO - eventi da ricercare
	 * @param utente
	 * @param idEnte - id dell'ente
	 * @param con
	 * @return contatore
	 */
	Integer getCountEventiForCalendarioHomepage(SearchEventiDTO searchEventiDTO, UtenteDTO utente, Integer idEnte, Connection con);
}