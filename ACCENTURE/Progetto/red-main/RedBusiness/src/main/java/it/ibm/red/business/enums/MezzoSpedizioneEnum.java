package it.ibm.red.business.enums;

/**
 * @author Simone Lacarpia
 * 
 * Cartaceo(1) o Elettronico(2) 
 *
 */
public enum MezzoSpedizioneEnum {
	
	/**
	 * 
	 */
	NON_SPECIFICATO(0),
	/**
	 * 
	 */
	CARTACEO(1),
	/**
	 * 
	 */
	ELETTRONICO(2);
	
	/**
	 * 
	 */
	private Integer id;
	
	/**
	 * @param id
	 */
	MezzoSpedizioneEnum(final Integer id) {
		this.id = id;
	}

	/**
	 * @return
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * @param id
	 * @return
	 */
	public static MezzoSpedizioneEnum getById(final Integer id) {
		MezzoSpedizioneEnum output = NON_SPECIFICATO; 
		if (id == null) {
			return output;
		}
		
		for (MezzoSpedizioneEnum m : MezzoSpedizioneEnum.values()) {
			if (m.getId().equals(id)) {
				output = m;
				break;
			}
		}
		
		return output;
	}

	/**
	 * Restituisce il mezzo di spedizione associato alla tipologia della spedizione.
	 * @param tipologiaSpedizione
	 * @return mezzo spedizione
	 */
	public static MezzoSpedizioneEnum getMezzoSpedizioneByTipologiaSpedizione(final TipologiaDestinatariEnum tipologiaSpedizione) {
		MezzoSpedizioneEnum output = NON_SPECIFICATO; 

		if (TipologiaDestinatariEnum.PEC.equals(tipologiaSpedizione) || TipologiaDestinatariEnum.PEO.equals(tipologiaSpedizione)) {
			output = MezzoSpedizioneEnum.ELETTRONICO;
		} else if (TipologiaDestinatariEnum.CARTACEO.equals(tipologiaSpedizione) || TipologiaDestinatariEnum.FAX.equals(tipologiaSpedizione)) {
			output = MezzoSpedizioneEnum.CARTACEO;
		}
		
		return output;
	}
}
