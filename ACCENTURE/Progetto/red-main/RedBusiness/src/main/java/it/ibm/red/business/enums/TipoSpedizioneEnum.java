package it.ibm.red.business.enums;

import java.util.Collection;
import java.util.List;

import it.ibm.red.business.constants.Constants;

/**
 * The Enum TipoSpedizioneEnum.
 *
 * @author CPIERASC
 * 
 *         Enum dei possibili tipi di spedizione di un documento.
 */
public enum TipoSpedizioneEnum {
	
	/**
	 * Non definito.
	 */
	IGNOTO(-1, "Ignoto"),
	
	/**
	 * Interno.
	 */
	INTERNO(0, "Interno"),

	/**
	 * Cartaceo. 
	 * Sottoinsieme del tipoDestinatario esterno
	 */
	CARTACEO(1, "Cartaceo"),

	/**
	 * Elettronico.
	 * Sottoinsieme del tipoDestinatario esterno
	 */
	ELETTRONICO(2, "Elettronico");

	/**
	 * Identificativo azione.
	 */
	private Integer id;
	/**
	 * Descrizione azione.
	 */
	private String descrizione;
	
	/**
	 * Costruttore.
	 * 
	 * @param inId			identificativo
	 * @param inDescrizione	descrizione
	 */
	TipoSpedizioneEnum(final Integer inId, final String inDescrizione) {
		this.id = inId;
		this.descrizione = inDescrizione;
	}
	
	/**
	 * @see ISignSRV -> getTipoSpedizione.
	 * @param destinatari
	 * @return output
	 * @deprecated
	 */
	@Deprecated
	public static int getIdTipoSpedizione(final List<String[]> destinatari) {
		int output;
		boolean bElettronico = false;
		boolean bCartaceo = false;
		boolean bInterni = false;
		
		if (destinatari != null) {
			
			// Ciclo i destinatari di un documento per determinare l'id spedizione 
			for (final String[] destSplit : destinatari) {
				Integer idTipoSpedizione;
				
				final String tipoDestinatario = destSplit[3];
				
				// Prima determino che tipo di destinatario è (Esterno/Interno)
				if (Constants.TipoDestinatario.ESTERNO.equals(tipoDestinatario)) {
					
					// Se esterno determino se è (Elettronico/Cartaceo)
					idTipoSpedizione = Integer.parseInt(destSplit[2]);
					if (Constants.TipoSpedizione.MEZZO_ELETTRONICO.equals(idTipoSpedizione)) {
						bElettronico = true;
					} else {
						bCartaceo = true;
					}
				} else {
					bInterni = true;
				}
				
				//se risulta sia elettronico che cartaceo interrompo
				if (bElettronico && bCartaceo) {
					break;
				}
			}
		}
		
		// Una volta ciclato la totalita' dei destinatari determino se e' (Interno/Elettronico/Cartaceo/Ignoto)
		if ((bElettronico && bCartaceo) || (bInterni && (bElettronico || bCartaceo))) {
			output = TipoSpedizioneEnum.CARTACEO.getId();
		} else if (bElettronico) {
			output = TipoSpedizioneEnum.ELETTRONICO.getId();
		} else if (bInterni) {
			output = TipoSpedizioneEnum.INTERNO.getId();
		} else {
			output = TipoSpedizioneEnum.CARTACEO.getId();
		}
		
		return output;
	}

	/**
	 * Getter descrizione.
	 * 
	 * @return	descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Getter identificativo.
	 * 
	 * @return	identificativo
	 */
	public Integer getId() {
		return id;
	}
	
	
	/**
	 * Verifica che sia ignoto verificando una incoerenza tra i metadati del documento.
	 * 
	 * @param assegnatariDocumento corrisponde al metadato del documento elencoLibroFirma
	 * @param metadatiCount corrisponde al metadati del docuemnto count
	 * @return il risultato della verifica
	 */
	public boolean isInvalid(final Collection<String> assegnatariDocumento, final Integer metadatiCount) {
		return !metadatiCount.equals(assegnatariDocumento.size());
	}

}
