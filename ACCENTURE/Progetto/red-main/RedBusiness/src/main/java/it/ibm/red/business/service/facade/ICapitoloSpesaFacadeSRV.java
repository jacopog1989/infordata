package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.CapitoloSpesaDTO;
import it.ibm.red.business.dto.RicercaCapitoloSpesaDTO;


/**
 * Facade capitoli di spesa service.
 *
 * @author mcrescentini
 */
public interface ICapitoloSpesaFacadeSRV extends Serializable {

	/**
	 * Rende persistente sulla base dati il {@code capitoloSpesa}.
	 * 
	 * @param capitoloSpesa
	 *            Capitolo spesa da inserire sul database.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa a cui deve essere associato
	 *            il capitolo spesa inserito.
	 */
	void inserisci(CapitoloSpesaDTO capitoloSpesa, Long idAoo);
	
	/**
	 * Esegue la ricerca dei capitoli di spesa a partire dai parametri impostati dal
	 * DTO di ricerca.
	 * 
	 * @param ricercaDTO
	 *            Parametri della ricerca.
	 * @return Capitoli di spesa recuperati dalla ricerca.
	 */
	Collection<CapitoloSpesaDTO> ricerca(RicercaCapitoloSpesaDTO ricercaDTO);
	
}