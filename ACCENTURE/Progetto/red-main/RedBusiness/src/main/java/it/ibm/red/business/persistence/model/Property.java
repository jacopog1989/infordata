package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * The Class Property.
 *
 * @author CPIERASC
 * 
 *         Entità che mappa la tabella PROPERTY.
 */
public class Property implements Serializable {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Chiave.
	 */
	private String key;

	/**
	 * Valore.
	 */
	private String value;
	
	/**
	 * Costruttore.
	 * 
	 * @param inKey		chiave
	 * @param inValue	valore
	 */
	public Property(final String inKey, final String inValue) {
		super();
		this.key = inKey;
		this.value = inValue;
	}
	
	/**
	 * Getter chiave.
	 * 
	 * @return	chiave
	 */
	public final String getKey() {
		return key;
	}

	/**
	 * Getter valore.
	 * 
	 * @return	valore
	 */
	public final String getValue() {
		return value;
	}
}
