package it.ibm.red.business.enums;

/**
 * Enum che modella tutte le possibili modalità di match con un metadato di tipo String.
 */
public enum StringMatchModeEnum {

	/**
	 * Valore.
	 */
	START_WITH("S", "INIZIA"),
	
	/**
	 * Valore.
	 */
	CONTAINS("C", "CONTIENE"),
	
	/**
	 * Valore.
	 */
	END_WITH("E", "FINISCE");

	
	/**
	 * Codice.
	 */
	private String code;
	
	/**
	 * Descrizione.
	 */
	private String description;

	/**
	 * Costruttore enum.
	 * @param code
	 * @param description
	 */
	StringMatchModeEnum(final String code, final String description) {
		this.code = code;
		this.description = description;
	}
	
	/**
	 * Restituisce il codice dell'enum.
	 * @return codice enum
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * Restituisce la descrizione dell'enum
	 * @return descrizione
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Restituisce l'enum associato al codice in ingresso.
	 * @param inCode
	 * @return enum associata al codice
	 */
	public static StringMatchModeEnum get(final String inCode) {
		StringMatchModeEnum output = null;
		
		for (final StringMatchModeEnum e: StringMatchModeEnum.values()) {
			if (e.getCode().equalsIgnoreCase(inCode)) {
				output = e;
				break;
			}
		}
		
		return output;
	}

}
