package it.ibm.red.business.dao.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.IRuoloDAO;
import it.ibm.red.business.dto.DelegatoDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Ruolo;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class RuoloDAO.
 *
 * @author CPIERASC
 * 
 * 	Dao per la gestione dei ruoli.
 */
@Repository
public class RuoloDAO extends AbstractDAO implements IRuoloDAO {

	private static final String AND = " AND ";

	private static final String DATADISATTIVAZIONE = "DATADISATTIVAZIONE";

	private static final String IDRUOLO = "IDRUOLO";

	private static final String DATAATTIVAZIONE = "DATAATTIVAZIONE";

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Messaggio errore recupero deleghe assegnate. Occorre appendere id ruolo al messaggio.
	 */
	private static final String ERROR_RECUPERO_DELEGHE_ASSEGNATE_MSG = "Errore durante il recupero delle Deleghe assegnate con idRuolo: ";

    /**
     * Logger.
     */
	private static final REDLogger LOGGER = REDLogger.getLogger(RuoloDAO.class);

	/**
	 * Dao per la gestione delle aoo.
	 */
	@Autowired
	private IAooDAO aooDAO;
	
	/**
	 * Gets the ruolo.
	 *
	 * @param idRuolo the id ruolo
	 * @param connection the connection
	 * @return the ruolo
	 */
	@Override
	public final Ruolo getRuolo(final Long idRuolo, final Connection connection) {
		return getRuolo(idRuolo, null, connection);
	}
	
	@Override
	public final Ruolo getRuolo(final Long idRuolo, final Aoo aoo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			final String querySQL = "SELECT r.* FROM Ruolo r WHERE r.idRuolo= ?";
			ps = connection.prepareStatement(querySQL);
			ps.setLong(index, idRuolo);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				final Long idRuoloDB = rs.getLong(IDRUOLO);
				final Date dataAttivazione = rs.getDate(DATAATTIVAZIONE);
				final Date dataDisattivazione = rs.getDate(DATADISATTIVAZIONE);
				final String nomeRuolo = rs.getString("NOMERUOLO");
				final BigDecimal permessi = rs.getBigDecimal("PERMESSI");
				final Long idAoo = rs.getLong("IDAOO");
				closeStatement(ps, rs);
				ps = null;
				rs = null;
				
				Aoo aooRuolo = aoo;
				if (aooRuolo == null) {
					aooRuolo = aooDAO.getAoo(idAoo, connection);
				}
				
				return new Ruolo(idRuoloDB,
						aooRuolo,
						dataAttivazione,
						dataDisattivazione,
						nomeRuolo,
						permessi);
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero del ruolo tramite id=" + idRuolo, e);
		} finally {
			closeStatement(ps, rs);
		}
		return null;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IRuoloDAO#getCategoriaByRuolo(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public int getCategoriaByRuolo(final Long idRuolo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int categoria = -1;
		
		try {
			final int index = 1;
			ps = connection.prepareStatement("SELECT idcategoria FROM ruolocategoria WHERE idruolo = ?");
			ps.setLong(index, idRuolo);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				categoria = rs.getInt("idcategoria");
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero della categoria per il ruolo: " + idRuolo, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return categoria;
	}
	
	
	@Override
	public final List<Ruolo> getListaRuoliAttivati(final Long idAoo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<Ruolo> ruoli = new ArrayList<>();
		
		try {
			final Aoo aooRuolo = aooDAO.getAoo(idAoo, con);
			
			int index = 1;
			ps = con.prepareStatement("SELECT * FROM ruolo WHERE idaoo = ? AND datadisattivazione IS NULL");
			ps.setLong(index++, idAoo);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				final Ruolo ruolo = new Ruolo(rs.getLong(IDRUOLO), aooRuolo, rs.getDate(DATAATTIVAZIONE), rs.getDate(DATADISATTIVAZIONE), 
						rs.getString("NOMERUOLO"), rs.getBigDecimal("PERMESSI"));
				
				ruoli.add(ruolo);
			}
		} catch (final SQLException e) {
			throw new RedException("getListaRuoliAttivati -> Errore durante il recupero dei ruoli attivati per l'AOO: " + idAoo, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return ruoli;
	}

	/**
	 * @see it.ibm.red.business.dao.IRuoloDAO#addRuoloToUtente(java.lang.Long,
	 *      java.lang.Long, java.lang.Long, int, java.util.Date, java.util.Date,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public boolean addRuoloToUtente(final Long idNodo, final Long idUtente, final Long idRuolo, final int predefinito, final Date dataAttivazione, final Date dataDisattivazione, final String motivoDelega, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder();
		boolean output = false;
		boolean executeUpdate = false;
		int index = 1;
		
		try {
			sb.append("SELECT nur.* ")
			  .append("FROM NODOUTENTERUOLO nur , UTENTE u ")
			  .append("WHERE u.idutente = nur.idutente AND nur.idnodo = ? AND u.idutente = ? AND nur.IDRUOLO = ?");
			
			ps = connection.prepareStatement(sb.toString());
			
			ps.setLong(index++, idNodo);
			ps.setLong(index++, idUtente);
			ps.setLong(index++, idRuolo);
			
			rs = ps.executeQuery();

			// Verica se è già presente lo stesso ruolo per poter fare un update invece che una insert.
			if (rs.next()) {
				executeUpdate = Boolean.TRUE;
			}
			
			sb = new StringBuilder();
			      
			if (executeUpdate) {
				sb.append("UPDATE NODOUTENTERUOLO ")
				  .append("SET DATADISATTIVAZIONE = ?, DATAATTIVAZIONE = ?, MOTIVAZIONE = ? ")
				  .append("WHERE IDNODO = ? AND IDUTENTE = ? AND IDRUOLO = ?");
				
				closeStatement(ps);
				ps = connection.prepareStatement(sb.toString());
				
				index = 1;
				ps.setDate(index++, new java.sql.Date(dataDisattivazione.getTime()));
				ps.setDate(index++, new java.sql.Date(dataAttivazione.getTime()));
				ps.setString(index++, motivoDelega);
				ps.setLong(index++, idNodo);
				ps.setLong(index++, idUtente);
				ps.setLong(index++, idRuolo);
				
				final int result = ps.executeUpdate();
				
				if (result == 1) {
					output = true;
				}
			} else {
				// SE NON è GIà PRESENTE UN RUOLO VIENE INSERITO
				sb.append("INSERT INTO NODOUTENTERUOLO (idnodo, idutente, idruolo, predefinito, motivazione, dataattivazione, datadisattivazione) ")
				  .append("VALUES (?,?,?,?,?,?,?)");
				
				closeStatement(ps);
				ps = connection.prepareStatement(sb.toString());
				
				index = 1;
				ps.setLong(index++, idNodo);
				ps.setLong(index++, idUtente);
				ps.setLong(index++, idRuolo);
				ps.setInt(index++, predefinito);
				ps.setString(index++, motivoDelega);
				ps.setDate(index++, new java.sql.Date(dataAttivazione.getTime()));
				
				if (dataDisattivazione != null) {
					ps.setDate(index, new java.sql.Date(dataDisattivazione.getTime()));
				}
				
				final int result = ps.executeUpdate();
				
				if (result == 1) {
					output = true;
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'associazione del Ruolo con id: " + idRuolo + " per l'utente con id: " + idUtente + " e idNodo: " + idNodo, e);
			throw new RedException("Errore durante l'associazione del Ruolo con id: " + idRuolo + " per l'utente con id: " + idUtente + " e idNodo: " + idNodo, e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IRuoloDAO#getAssegnazioniDelegato(java.lang.Long,
	 *      java.util.List, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public Collection<DelegatoDTO> getAssegnazioniDelegato(final Long idRuolo, final List<Long> idsUffici, final Long idAoo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final StringBuilder sb = new StringBuilder();
		final Collection<DelegatoDTO> output = new ArrayList<>();
		int index;
		
		try {

			sb.append("SELECT nur.IDNODO, n.DESCRIZIONE, u.IDUTENTE, u.NOME, u.COGNOME, nur.DATAATTIVAZIONE, nur.DATADISATTIVAZIONE, nur.MOTIVAZIONE ")
			  .append("FROM NODOUTENTERUOLO nur , UTENTE u, NODO n ")
			  .append("WHERE nur.dataDisattivazione >= sysdate AND u.idutente = nur.idutente AND n.IDNODO = nur.IDNODO AND nur.IDRUOLO = ? ")
			  .append(AND).append("n.IDAOO = ?");
			
			if (idsUffici != null && !idsUffici.isEmpty()) {
				sb.append(AND).append(StringUtils.createInCondition("nur.IDNODO", idsUffici.iterator(), true));
			}
			
			
			ps = con.prepareStatement(sb.toString());
			
			index = 1;
			ps.setLong(index++, idRuolo);
			ps.setLong(index++, idAoo);

			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				final DelegatoDTO delegato = new DelegatoDTO(rs.getLong("IDUTENTE"), rs.getString("NOME"), rs.getString("COGNOME"), 
						rs.getLong("IDNODO"), rs.getString("DESCRIZIONE"), rs.getDate(DATAATTIVAZIONE), rs.getDate(DATADISATTIVAZIONE), rs.getString("MOTIVAZIONE")); 
				
				output.add(delegato);
			}
			
			
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_DELEGHE_ASSEGNATE_MSG + idRuolo, e);
			throw new RedException(ERROR_RECUPERO_DELEGHE_ASSEGNATE_MSG + idRuolo, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IRuoloDAO#removeRuoloFromUtente(java.lang.Long,
	 *      java.lang.Long, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public Boolean removeRuoloFromUtente(final Long idNodo, final Long idUtente, final Long idRuolo, final Connection connection) {
		PreparedStatement ps = null;
		final StringBuilder sb = new StringBuilder();
		Boolean output = Boolean.FALSE;
		int index = 1;
		
		try {
			
			sb.append("DELETE FROM NODOUTENTERUOLO ")
			  .append("WHERE IDNODO = ? AND IDUTENTE = ? AND IDRUOLO = ?");
			
			ps = connection.prepareStatement(sb.toString());
			
			index = 1;
			ps.setLong(index++, idNodo);
			ps.setLong(index++, idUtente);
			ps.setLong(index, idRuolo);
			
			final int result = ps.executeUpdate();
			
			if (result == 1) {
				output = Boolean.TRUE;
			}
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante la rimozione della Delega assegnata per l'utente con idUtente: " + idUtente + ". idNodo: " + idNodo + ". idRuolo: " + idRuolo + ". ", e);
			throw new RedException("Errore durante la rimozione della Delega assegnata per l'utente con idUtente: " + idUtente + ". idNodo: " + idNodo + ". idRuolo: " + idRuolo + ". ", e);
		} finally {
			closeStatement(ps);
		}
		
		return output;
	}
	
	@Override
	public final List<Long> getListaRuoliUtenteUfficio(final Long idUtente, final Long idNodo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<Long> ruoli = new ArrayList<>();
		
		try {
			
			
			int index = 1;
			ps = con.prepareStatement("SELECT idruolo FROM nodoutenteruolo WHERE idutente = ? AND idnodo = ? AND datadisattivazione IS NULL order by predefinito desc");
			ps.setLong(index++, idUtente);
			ps.setLong(index++, idNodo);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				ruoli.add(rs.getLong(IDRUOLO));
			}
		} catch (final SQLException e) {
			throw new RedException("getListaRuoliUtenteUfficio -> Errore durante il recupero dei ruoli", e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return ruoli;
	}

	/**
	 * @see it.ibm.red.business.dao.IRuoloDAO#getDelegatoLibroFirma(java.lang.Long,
	 *      java.lang.Long, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public DelegatoDTO getDelegatoLibroFirma(final Long idRuolo, final Long idNodo, final Long idAOO, final Connection con) {
		final StringBuilder sb = new StringBuilder();
		PreparedStatement ps = null;
		DelegatoDTO output = null;
		ResultSet rs = null;
		int index;
		
		try {

			sb.append("SELECT nur.IDNODO, n.DESCRIZIONE, u.IDUTENTE, u.NOME, u.COGNOME, nur.DATAATTIVAZIONE, nur.DATADISATTIVAZIONE, nur.MOTIVAZIONE ")
			  .append("FROM NODOUTENTERUOLO nur , UTENTE u, NODO n ")
			  .append("WHERE ")
			  .append("(nur.dataDisattivazione >= sysdate or nur.dataDisattivazione is null)")
			  .append(AND).append("u.idutente = nur.idutente")
			  .append(AND).append("n.IDNODO = nur.IDNODO")
			  .append(AND).append("nur.IDRUOLO = ?")
			  .append(AND).append("n.IDNODO = ?")
			  .append(AND).append("n.IDAOO = ?");
			
			
			
			ps = con.prepareStatement(sb.toString());
			
			index = 1;
			ps.setLong(index++, idRuolo);
			ps.setLong(index++, idNodo);
			ps.setLong(index++, idAOO);

			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				output = new DelegatoDTO(rs.getLong("IDUTENTE"), rs.getString("NOME"), rs.getString("COGNOME"), rs.getLong("IDNODO"), 
										 rs.getString("DESCRIZIONE"), rs.getDate(DATAATTIVAZIONE), rs.getDate(DATADISATTIVAZIONE), rs.getString("MOTIVAZIONE")); 
			}
			
			
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_DELEGHE_ASSEGNATE_MSG + idRuolo, e);
			throw new RedException(ERROR_RECUPERO_DELEGHE_ASSEGNATE_MSG + idRuolo, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return output;
	}
	
	
}