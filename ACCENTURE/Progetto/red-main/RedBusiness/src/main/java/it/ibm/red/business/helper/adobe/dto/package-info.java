/**
 * Il package contiene i DTO di input ed output dei metodi dell'AdobeLCHelper
 * 
 * @author a.dilegge
 *
 */
package it.ibm.red.business.helper.adobe.dto;