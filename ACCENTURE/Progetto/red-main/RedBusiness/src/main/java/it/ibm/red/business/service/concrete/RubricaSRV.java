package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.IndependentObjectSet;
import com.filenet.api.core.CustomObject;

import it.ibm.red.business.dao.IComuneDAO;
import it.ibm.red.business.dao.IContattoDAO;
import it.ibm.red.business.dao.INotificaContattoDAO;
import it.ibm.red.business.dao.IProvinciaDAO;
import it.ibm.red.business.dao.IRegioneDAO;
import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.ComuneDTO;
import it.ibm.red.business.dto.ProvinciaDTO;
import it.ibm.red.business.dto.RegioneDTO;
import it.ibm.red.business.dto.RicercaRubricaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.StatoNotificaContattoEnum;
import it.ibm.red.business.enums.StatoNotificaUtenteEnum;
import it.ibm.red.business.enums.TipoOperazioneEnum;
import it.ibm.red.business.enums.TipoRubricaEnum;
import it.ibm.red.business.enums.TipologiaIndirizzoEmailEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.NotificaContatto;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IRubricaSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * @author rbitto
 *
 */
@Service
@Component
public class RubricaSRV extends AbstractService implements IRubricaSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 3714934865649351849L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RubricaSRV.class.getName());

	/**
	 * Messaggio errore ricerca contatti o gruppi.
	 */
	private static final String ERROR_RICERCA_CONTATTI_MSG = "Errore durante la ricerca dei contatti/gruppi";

	/**
	 * OTF.
	 */
	private static final Integer ON_THE_FLY = 1;

	/**
	 * DAO gestione utenti.
	 */
	@Autowired
	private IContattoDAO contattoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private INotificaContattoDAO notificaContattoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IComuneDAO comuneDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IProvinciaDAO provinciaDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IRegioneDAO regioneDAO;

	@Override
	public final Contatto getContattoByID(final Long idContatto) {
		Contatto contatto = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			contatto = contattoDAO.getContattoByID(idContatto, connection);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RICERCA_CONTATTI_MSG, e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
		return contatto;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#getContattoByID(boolean,
	 *      java.lang.Long).
	 */
	@Override
	public Contatto getContattoByID(final boolean consideraGruppi, final Long idContatto) {
		Connection connection = null;
		Contatto contatto = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			contatto = contattoDAO.getContattoByID(consideraGruppi, idContatto, connection);
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return contatto;
	}

	/**
	 * @see it.ibm.red.business.service.IRubricaSRV#getContattoByID(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public final Contatto getContattoByID(final Long idContatto, final Connection connection) {
		Contatto contatto = null;

		try {
			contatto = contattoDAO.getContattoByID(idContatto, connection);
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		}

		return contatto;
	}

	@Override
	public final List<Contatto> getPreferiti(final Long idUfficio, final Long idAoo) {
		return getPreferiti(idUfficio, idAoo, true);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#getPadreContattoIPA(it.ibm.red.business.persistence.model.Contatto).
	 */
	@Override
	public Contatto getPadreContattoIPA(final Contatto contattoIPA) {
		Connection connection = null;
		Contatto padre = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			padre = contattoDAO.getPadreContattoIPA(contattoIPA, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore riscontrato durante il recupero del padre contatto IPA: " + contattoIPA.getIdentificativo(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return padre;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#getPreferiti(java.lang.Long,
	 *      java.lang.Long, boolean).
	 */
	@Override
	public final List<Contatto> getPreferiti(final Long idUfficio, final Long idAoo, final boolean consideraGruppi) {
		Connection connection = null;
		List<Contatto> preferiti = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			preferiti = contattoDAO.getContattiPreferiti(idUfficio, idAoo, consideraGruppi, connection);
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return preferiti;
	}

	@Override
	public final List<Contatto> getContattiRubricaMailPerAlias(final String casellaPostale, final String aliasDescrizione) {
		Connection connection = null;
		List<Contatto> gruppi = null;
		List<Contatto> contatti = null;
		final List<Contatto> listacontatti = new ArrayList<>();

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			contatti = contattoDAO.getContattiMailFromAlias(casellaPostale, aliasDescrizione, connection);
			gruppi = contattoDAO.getListaGruppoEmailByAlias(casellaPostale, aliasDescrizione, connection);

			if (contatti != null) {
				listacontatti.addAll(contatti);
			}
			if (gruppi != null) {
				listacontatti.addAll(gruppi);
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_RICERCA_CONTATTI_MSG, e);
			throw new RedException(ERROR_RICERCA_CONTATTI_MSG, e);
		} finally {
			closeConnection(connection);
		}

		return listacontatti;
	}

	@Override
	public final List<Contatto> getContattiByEmail(final String casellaPostale, final String emailContatto) {
		Connection connection = null;
		List<Contatto> contatti = null;
		final List<Contatto> listacontatti = new ArrayList<>();

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			contatti = contattoDAO.getContattiByEmail(casellaPostale, emailContatto, connection);
			if (contatti != null) {
				listacontatti.addAll(contatti);
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_RICERCA_CONTATTI_MSG, e);
			throw new RedException(ERROR_RICERCA_CONTATTI_MSG, e);
		} finally {
			closeConnection(connection);
		}

		return listacontatti;
	}

	@Override
	public final List<Contatto> getContattiPreferitiPerAlias(final String aliasDescrizione, final Long idUfficio, final Long idAOO) {
		List<Contatto> contatti = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			contatti = contattoDAO.getContattiPreferitiPerAlias(aliasDescrizione, idUfficio, idAOO, connection);

		} catch (final Exception e) {
			LOGGER.error(ERROR_RICERCA_CONTATTI_MSG, e);
			throw new RedException(ERROR_RICERCA_CONTATTI_MSG);
		} finally {
			closeConnection(connection);
		}

		return contatti;
	}

	@Override
	public final void rimuoviPreferito(final Long idUfficio, final Long contattoID, final TipoRubricaEnum tipoRubrica) {
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			contattoDAO.rimuoviPreferito(idUfficio, contattoID, tipoRubrica, connection);

			commitConnection(connection);
		} catch (final Exception e) {
			LOGGER.error(e);
			rollbackConnection(connection);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	@Override
	public final void aggiungiAiPreferiti(final Long idUfficio, final Long contattoID, final TipoRubricaEnum tipoRubrica, final Long idUtente) {
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			contattoDAO.aggiungiAiPreferiti(idUfficio, contattoID, tipoRubrica, idUtente, connection);

			commitConnection(connection);
		} catch (final Exception e) {
			LOGGER.error(e);
			rollbackConnection(connection);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#getAllGruppi(java.lang.Long).
	 */
	@Override
	public List<Contatto> getAllGruppi(final Long idUfficio) {
		List<Contatto> ricercaContatti = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			ricercaContatti = contattoDAO.getAllGruppi(idUfficio, connection);
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return ricercaContatti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#ricerca(java.lang.Long,
	 *      it.ibm.red.business.dto.RicercaRubricaDTO, boolean).
	 */
	@Override
	public List<Contatto> ricerca(final Long idUfficio, final RicercaRubricaDTO ricercaContattoObj, final boolean acceptNullValues) {
		List<Contatto> ricercaContatti = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			ricercaContatti = contattoDAO.ricerca(idUfficio, ricercaContattoObj, acceptNullValues, connection);
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return ricercaContatti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#ricercaCampoSingolo(java.lang.String,
	 *      java.lang.Long, java.lang.Integer, boolean, boolean, boolean, boolean,
	 *      java.lang.String).
	 */
	@Override
	public List<Contatto> ricercaCampoSingolo(final String value, final Long idUfficio, final Integer idAOO, final boolean ricercaRED, final boolean ricercaIPA,
			final boolean ricercaMEF, final boolean ricercaGruppo, final String topN) {
		return ricercaCampoSingolo(value, idUfficio, idAOO, ricercaRED, ricercaIPA, ricercaMEF, ricercaGruppo, topN, null, null, false);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#ricercaCampoSingolo(java.lang.String,
	 *      java.lang.Long, java.lang.Integer, boolean, boolean, boolean, boolean,
	 *      java.lang.String, java.util.ArrayList, java.lang.Long, boolean).
	 */
	@Override
	public List<Contatto> ricercaCampoSingolo(final String value, final Long idUfficio, final Integer idAOO, final boolean ricercaRED, final boolean ricercaIPA,
			final boolean ricercaMEF, final boolean ricercaGruppo, final String topN, final ArrayList<Long> contattiDaEscludere, final Long idUfficioGruppo,
			final boolean mostraPrimaPreferitiUfficio) {
		List<Contatto> ricercaContatti = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			ricercaContatti = contattoDAO.ricercaCampoSingolo(value, idUfficio, idAOO, ricercaRED, ricercaIPA, ricercaMEF, ricercaGruppo, topN, contattiDaEscludere,
					idUfficioGruppo, mostraPrimaPreferitiUfficio, connection);
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return ricercaContatti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#ricerca(java.lang.Long,
	 *      it.ibm.red.business.dto.RicercaRubricaDTO, boolean,
	 *      java.sql.Connection).
	 */
	@Override
	public List<Contatto> ricerca(final Long idUfficio, final RicercaRubricaDTO ricercaContattoObj, final boolean acceptNullValues, final Connection connection) {
		return contattoDAO.ricerca(idUfficio, ricercaContattoObj, acceptNullValues, connection);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#ricerca(java.lang.Long,
	 *      it.ibm.red.business.dto.RicercaRubricaDTO).
	 */
	@Override
	public List<Contatto> ricerca(final Long idUfficio, final RicercaRubricaDTO ricercaContattoObj) {
		List<Contatto> ricercaContatti = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			ricercaContatti = contattoDAO.ricerca(idUfficio, ricercaContattoObj, connection);
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return ricercaContatti;
	}

	/**
	 * @see it.ibm.red.business.service.IRubricaSRV#ricerca(java.lang.Long,
	 *      it.ibm.red.business.dto.RicercaRubricaDTO, java.sql.Connection).
	 */
	@Override
	public List<Contatto> ricerca(final Long idUfficio, final RicercaRubricaDTO ricercaContattoObj, final Connection connection) {
		return contattoDAO.ricerca(idUfficio, ricercaContattoObj, connection);
	}

	/**
	 * @see it.ibm.red.business.service.IRubricaSRV#retrieveContattoFromMail(java.lang.String,
	 *      java.lang.Integer).
	 */
	@Override
	public Contatto retrieveContattoFromMail(final String email, final Integer idAoo) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			final RicercaRubricaDTO ricercaDTO = new RicercaRubricaDTO();
			ricercaDTO.setRicercaRED(true);
			ricercaDTO.setRicercaIPA(true);
			ricercaDTO.setRicercaMEF(true);
			ricercaDTO.setMail(email);
			ricercaDTO.setIdAoo(idAoo);

			final List<Contatto> contatti = contattoDAO.ricerca(null, ricercaDTO, true, connection);

			if (contatti != null && !contatti.isEmpty()) {
				commitConnection(connection);
				return contatti.get(0);
			}

			// email non presente in base dati => crea contatto nuovo
			final Contatto contatto = new Contatto();
			contatto.setNome(email);
			contatto.setMail(email);
			contatto.setCognome(email);
			contatto.setPubblico(0);
			contatto.setContattocasellapostale((long) 0);
			contatto.setTipoPersona("F");
			contatto.setDatacreazione(new Date());
			contatto.setIdAOO((long) idAoo);
			contatto.setOnTheFly(ON_THE_FLY);
			contatto.setDatadisattivazione(new Date());

			final Long idContatto = contattoDAO.insertContatto(contatto, connection);
			contatto.setContattoID(idContatto);
			commitConnection(connection);

			return contatto;

		} catch (final Exception e) {
			rollbackConnection(connection);
			LOGGER.error("Errore durante il recupero dell'ID contatto", e);
			throw new RedException("Errore durante il recupero dell'ID contatto", e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IRubricaSRV#inserisci(java.lang.Long,
	 *      it.ibm.red.business.persistence.model.Contatto, java.sql.Connection,
	 *      boolean).
	 */
	@Override
	public Long inserisci(final Long idUfficio, final Contatto inserisciContattoItem, final Connection connection, final boolean checkUnivocitaMail) {
		Long contattoId = null;

		try {
			inserisciContattoItem.setOnTheFly(ON_THE_FLY);

			checkEsistenzaContattoRED(inserisciContattoItem, connection);
			if (checkUnivocitaMail) {
				final boolean isInModifica = false;
				checkMailPerContattoGiaPresentePerAoo(inserisciContattoItem, isInModifica);
			}

			contattoId = contattoDAO.inserisci(idUfficio, inserisciContattoItem, connection);

		} catch (final Exception e) {
			throw new RedException("Errore durante l'inserimento del contatto: " + e.getMessage(), e);
		}

		return contattoId;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#inserisciContattoPerRichiestaCreazione(it.ibm.red.business.persistence.model.Contatto,
	 *      boolean).
	 */
	@Override
	public Long inserisciContattoPerRichiestaCreazione(final Contatto contatto, final boolean dataDisattivazionePresente) {
		Long contattoId = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			if (dataDisattivazionePresente) {
				contatto.setDatadisattivazione(new Date());
			}
			contatto.setDatacreazione(new Date());
			contatto.setOnTheFly(ON_THE_FLY);
			contatto.setPubblico(1);
			contattoId = contattoDAO.insertContatto(contatto, connection);

			commitConnection(connection);
		} catch (final Exception e) {
			rollbackConnection(connection);
			throw new RedException("Errore durante la richiesta di inserimento del contatto", e);
		} finally {
			closeConnection(connection);
		}

		return contattoId;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#inserisci(java.lang.Long,
	 *      it.ibm.red.business.persistence.model.Contatto, boolean).
	 */
	@Override
	public Long inserisci(final Long idUfficio, final Contatto inserisciContattoItem, final boolean checkUnivocitaMail) {
		Long contattoId = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			checkEsistenzaContattoRED(inserisciContattoItem, connection);
			if (checkUnivocitaMail) {
				final boolean isInModifica = false;
				checkMailPerContattoGiaPresentePerAoo(inserisciContattoItem, isInModifica);
			}
			contattoId = contattoDAO.inserisci(idUfficio, inserisciContattoItem, connection);

			commitConnection(connection);
		} catch (final Exception e) {
			rollbackConnection(connection);
			throw new RedException("Errore durante l'inserimento del contatto", e);

		} finally {
			closeConnection(connection);
		}

		return contattoId;
	}

	private void checkEsistenzaContattoRED(final Contatto contattoToCheck, final Connection connection) {
		final boolean esiste = contattoDAO.checkEsistenzaContattoRED(contattoToCheck, connection);

		if (esiste) {
			throw new RedException("Un contatto con le stesse informazioni è già presente all'interno del sistema per questa AOO");
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#checkMailPerContattoGiaPresentePerAoo(it.ibm.red.business.persistence.model.Contatto,
	 *      boolean).
	 */
	@Override
	public void checkMailPerContattoGiaPresentePerAoo(final Contatto contattoToCheck, final boolean isInModifica) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final boolean esiste = contattoDAO.checkMailPerContattoGiaPresentePerAoo(contattoToCheck, connection, isInModifica);

			if (esiste) {
				throw new RedException("Un contatto con la stessa email è già presente all'interno del sistema per questa AOO");
			}
		} catch (final Exception ex) {
			throw new RedException(ex);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#modifica(it.ibm.red.business.persistence.model.Contatto,
	 *      it.ibm.red.business.persistence.model.NotificaContatto, boolean).
	 */
	@Override
	public void modifica(final Contatto contatto, final NotificaContatto notifica, final boolean checkUnivocitaMail) {
		modifica(contatto, notifica, false, checkUnivocitaMail);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#modifica(it.ibm.red.business.persistence.model.Contatto,
	 *      it.ibm.red.business.persistence.model.NotificaContatto, boolean,
	 *      boolean).
	 */
	@Override
	public void modifica(final Contatto contatto, final NotificaContatto notifica, final boolean isApprovaRichiestaCreazione, final boolean checkUnivocitaMail) {
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			checkEsistenzaContattoRED(contatto, connection);
			if (checkUnivocitaMail) {
				final boolean isInModifica = true;
				checkMailPerContattoGiaPresentePerAoo(contatto, isInModifica);
			}

			if("G".equals(contatto.getTipoPersona())) {
				contatto.setCognome("");
				contatto.setTitolo("");
			} 
			
			if(!isApprovaRichiestaCreazione) {
				contattoDAO.modifica(contatto, connection);
			} else {
				contattoDAO.modifica(contatto, connection, isApprovaRichiestaCreazione);
			}
			if (notifica != null) {
				if (!notifica.getApprovatoConModifiche()) {
					updateStatoNotifica(notifica, StatoNotificaContattoEnum.ESEGUITA, null, connection);
				} else {
					updateStatoNotifica(notifica, StatoNotificaContattoEnum.APPROVATO_CON_MODIFICHE, null, connection);
				}
			}

			commitConnection(connection);
		} catch (final Exception e) {
			rollbackConnection(connection);
			final String errorCode = "MAILESISTENTE";
			if (e.getMessage().contains(errorCode)) {
				throw new RedException(errorCode + "Non è possibile modificare il contatto in quanto già è presente un indirizzo con la stessa mail", e);
			} else {
				throw new RedException("Errore durante la modifica del contatto", e);
			}
		} finally {
			closeConnection(connection);
		}
	}

	private void updateStatoNotifica(final NotificaContatto notifica, final StatoNotificaContattoEnum statoNotifica, final String motivoRifiuto, final Connection connection) {
		if (notifica != null) {
			notificaContattoDAO.updateStatoNotifica(notifica.getIdNotificaContatto(), statoNotifica.getNome(), motivoRifiuto, connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#updateStatoNotifica(it.ibm.red.business.persistence.model.NotificaContatto,
	 *      it.ibm.red.business.enums.StatoNotificaContattoEnum, java.lang.String).
	 */
	@Override
	public void updateStatoNotifica(final NotificaContatto notifica, final StatoNotificaContattoEnum statoNotifica, final String motivoRifiuto) {
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			updateStatoNotifica(notifica, statoNotifica, motivoRifiuto, connection);

			commitConnection(connection);
		} catch (final Exception e) {
			rollbackConnection(connection);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	private void removeNotifiche(final List<NotificaContatto> altreNotificheCollegate, final Connection connection) {
		if (altreNotificheCollegate != null) {
			for (final NotificaContatto notificaCorrente : altreNotificheCollegate) {
				notificaContattoDAO.removeNotifica(notificaCorrente.getIdNotificaContatto(), connection);
			}
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#elimina(java.lang.Long,
	 *      it.ibm.red.business.persistence.model.NotificaContatto, java.util.List).
	 */
	@Override
	public void elimina(final Long idContatto, final NotificaContatto notifica, final List<NotificaContatto> altreNotificheCollegate) {
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			contattoDAO.eliminaContatto(idContatto, connection);
			updateStatoNotifica(notifica, StatoNotificaContattoEnum.ESEGUITA, null, connection);
			removeNotifiche(altreNotificheCollegate, connection);

			commitConnection(connection);
		} catch (final Exception e) {
			rollbackConnection(connection);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#notificaModificaContatto(it.ibm.red.business.persistence.model.NotificaContatto,
	 *      it.ibm.red.business.persistence.model.Contatto).
	 */
	@Override
	public void notificaModificaContatto(final NotificaContatto notifica, final Contatto contattoVecchio) {
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			final Contatto contattoNuovo = notifica.getNuovoContatto();

			final boolean flagTipoPersonaModificato = getFlagTipoPersonaModificato(contattoVecchio, contattoNuovo);
			tipoPersonaModificato(notifica, contattoVecchio, connection, contattoNuovo, flagTipoPersonaModificato);

			final boolean flagNomeModificato = getFlagNomeModificato(contattoVecchio, contattoNuovo);
			nomeModificato(notifica, contattoVecchio, connection, contattoNuovo, flagNomeModificato);

			final boolean flagCognomeModificato = getFlagCognomeModificato(contattoVecchio, contattoNuovo);
			cognomeModificato(notifica, contattoVecchio, connection, contattoNuovo, flagCognomeModificato);
			
			final boolean flagAliasContattoModificato = getFlagAliasContattoModificato(contattoVecchio, contattoNuovo);
			aliasContattoModificato(notifica, contattoVecchio, connection, contattoNuovo, flagAliasContattoModificato);
			
			final boolean flagIndirizzoModificato = getFlagIndirizzoModificato(contattoVecchio, contattoNuovo);
			flagIndirizzoModificato(notifica, contattoVecchio, connection, contattoNuovo, flagIndirizzoModificato);

			final boolean flagCAPModificato = getFlagCAPModificato(contattoVecchio, contattoNuovo);
			flagCAPModificato(notifica, contattoVecchio, connection, contattoNuovo, flagCAPModificato);

			final boolean flagTelefonoModificato = getFlagTelefonoModificato(contattoVecchio, contattoNuovo);
			flagTelefonoModificato(notifica, contattoVecchio, connection, contattoNuovo, flagTelefonoModificato);

			final boolean flagCellulareModificato = getFlagCellulareModificato(contattoVecchio, contattoNuovo);
			flagCellulareModificato(notifica, contattoVecchio, connection, contattoNuovo, flagCellulareModificato);

			final boolean flagMailModificata = getFlagMailModificata(contattoVecchio, contattoNuovo);
			flagMailModificata(notifica, contattoVecchio, connection, contattoNuovo, flagMailModificata);

			final boolean flagMailPecModificata = getFlagMailPecModificata(contattoVecchio, contattoNuovo);
			flagMailPecModificata(notifica, contattoVecchio, connection, contattoNuovo, flagMailPecModificata);

			final boolean flagFaxModificato = getFlagFaxModificato(contattoVecchio, contattoNuovo);
			flagFaxModificato(notifica, contattoVecchio, connection, contattoNuovo, flagFaxModificato);

			final boolean flagIdRegioneIstatModificato = getFlagIdRegioneIstatModificato(contattoVecchio, contattoNuovo);
			flagIdRegioneIstatModificato(notifica, contattoVecchio, connection, contattoNuovo, flagIdRegioneIstatModificato);

			final boolean flagIdProvinciaIstatModificato = getFlagIdProvinciaIstatModificato(contattoVecchio, contattoNuovo);
			flagIdProvinciaIstatModificato(notifica, contattoVecchio, connection, contattoNuovo, flagIdProvinciaIstatModificato);

			final boolean flagIdComuneIstatModificato = getFlagIdComuneIstatModificato(contattoVecchio, contattoNuovo);
			flagIdComuneIstatModificato(notifica, contattoVecchio, connection, contattoNuovo, flagIdComuneIstatModificato);

			final boolean flagTitoloModificato = getFlagTitoloModificato(contattoVecchio, contattoNuovo);
			flagTitoloModificato(notifica, contattoVecchio, connection, contattoNuovo, flagTitoloModificato);

			commitConnection(connection);
		} catch (final Exception e) {
			rollbackConnection(connection);
			LOGGER.error("Si è verificato un errore durante la creazione della notifica", e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	private static boolean getFlagTitoloModificato(final Contatto contattoVecchio, final Contatto contattoNuovo) {
		return (StringUtils.isNullOrEmpty(contattoNuovo.getTitolo()) && !StringUtils.isNullOrEmpty(contattoVecchio.getTitolo()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getTitolo()) && StringUtils.isNullOrEmpty(contattoVecchio.getTitolo()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getTitolo()) && !contattoNuovo.getTitolo().equalsIgnoreCase(contattoVecchio.getTitolo()));
	}

	private static boolean getFlagIdComuneIstatModificato(final Contatto contattoVecchio, final Contatto contattoNuovo) {
		return (StringUtils.isNullOrEmpty(contattoNuovo.getIdComuneIstat())
				&& !StringUtils.isNullOrEmpty(contattoVecchio.getIdComuneIstat()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getIdComuneIstat()) && StringUtils.isNullOrEmpty(contattoVecchio.getIdComuneIstat()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getIdComuneIstat())
						&& !contattoNuovo.getIdComuneIstat().equalsIgnoreCase(contattoVecchio.getIdComuneIstat()));
	}

	private static boolean getFlagIdProvinciaIstatModificato(final Contatto contattoVecchio, final Contatto contattoNuovo) {
		return (StringUtils.isNullOrEmpty(contattoNuovo.getIdProvinciaIstat())
				&& !StringUtils.isNullOrEmpty(contattoVecchio.getIdProvinciaIstat()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getIdProvinciaIstat()) 
						&& StringUtils.isNullOrEmpty(contattoVecchio.getIdProvinciaIstat()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getIdProvinciaIstat())
						&& !contattoNuovo.getIdProvinciaIstat().equalsIgnoreCase(contattoVecchio.getIdProvinciaIstat()));
	}

	private static boolean getFlagIdRegioneIstatModificato(final Contatto contattoVecchio, final Contatto contattoNuovo) {
		return (StringUtils.isNullOrEmpty(contattoNuovo.getIdRegioneIstat())
				&& !StringUtils.isNullOrEmpty(contattoVecchio.getIdRegioneIstat()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getIdRegioneIstat()) && StringUtils.isNullOrEmpty(contattoVecchio.getIdRegioneIstat()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getIdRegioneIstat())
						&& !contattoNuovo.getIdRegioneIstat().equalsIgnoreCase(contattoVecchio.getIdRegioneIstat()));
	}

	private static boolean getFlagFaxModificato(final Contatto contattoVecchio, final Contatto contattoNuovo) {
		return (StringUtils.isNullOrEmpty(contattoNuovo.getFax()) && !StringUtils.isNullOrEmpty(contattoVecchio.getFax()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getFax()) && StringUtils.isNullOrEmpty(contattoVecchio.getFax()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getFax()) && !contattoNuovo.getFax().equalsIgnoreCase(contattoVecchio.getFax()));
	}

	private static boolean getFlagMailPecModificata(final Contatto contattoVecchio, final Contatto contattoNuovo) {
		return (StringUtils.isNullOrEmpty(contattoNuovo.getMailPec()) 
				&& !StringUtils.isNullOrEmpty(contattoVecchio.getMailPec()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getMailPec()) && StringUtils.isNullOrEmpty(contattoVecchio.getMailPec()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getMailPec()) 
						&& !contattoNuovo.getMailPec().equalsIgnoreCase(contattoVecchio.getMailPec()));
	}

	private static boolean getFlagMailModificata(final Contatto contattoVecchio, final Contatto contattoNuovo) {
		return (StringUtils.isNullOrEmpty(contattoNuovo.getMail()) && !StringUtils.isNullOrEmpty(contattoVecchio.getMail()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getMail()) && StringUtils.isNullOrEmpty(contattoVecchio.getMail()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getMail()) && !contattoNuovo.getMail().equalsIgnoreCase(contattoVecchio.getMail()));
	}

	private static boolean getFlagCellulareModificato(final Contatto contattoVecchio, final Contatto contattoNuovo) {
		return (StringUtils.isNullOrEmpty(contattoNuovo.getCellulare()) 
				&& !StringUtils.isNullOrEmpty(contattoVecchio.getCellulare()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getCellulare()) && StringUtils.isNullOrEmpty(contattoVecchio.getCellulare()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getCellulare()) 
						&& !contattoNuovo.getCellulare().equalsIgnoreCase(contattoVecchio.getCellulare()));
	}

	private static boolean getFlagTelefonoModificato(final Contatto contattoVecchio, final Contatto contattoNuovo) {
		return (StringUtils.isNullOrEmpty(contattoNuovo.getTelefono()) 
				&& !StringUtils.isNullOrEmpty(contattoVecchio.getTelefono()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getTelefono()) && StringUtils.isNullOrEmpty(contattoVecchio.getTelefono()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getTelefono()) 
						&& !contattoNuovo.getTelefono().equalsIgnoreCase(contattoVecchio.getTelefono()));
	}

	private static boolean getFlagCAPModificato(final Contatto contattoVecchio, final Contatto contattoNuovo) {
		return (StringUtils.isNullOrEmpty(contattoNuovo.getCAP()) && !StringUtils.isNullOrEmpty(contattoVecchio.getCAP()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getCAP()) && StringUtils.isNullOrEmpty(contattoVecchio.getCAP()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getCAP()) && !contattoNuovo.getCAP().equalsIgnoreCase(contattoVecchio.getCAP()));
	}

	private static boolean getFlagIndirizzoModificato(final Contatto contattoVecchio, final Contatto contattoNuovo) {
		return (StringUtils.isNullOrEmpty(contattoNuovo.getIndirizzo()) 
				&& !StringUtils.isNullOrEmpty(contattoVecchio.getIndirizzo()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getIndirizzo()) && StringUtils.isNullOrEmpty(contattoVecchio.getIndirizzo()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getIndirizzo()) 
						&& !contattoNuovo.getIndirizzo().equalsIgnoreCase(contattoVecchio.getIndirizzo()));
	}

	private static boolean getFlagAliasContattoModificato(final Contatto contattoVecchio, final Contatto contattoNuovo) {
		return (StringUtils.isNullOrEmpty(contattoNuovo.getAliasContatto())
				&& !StringUtils.isNullOrEmpty(contattoVecchio.getAliasContatto()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getAliasContatto()) && StringUtils.isNullOrEmpty(contattoVecchio.getAliasContatto()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getAliasContatto())
						&& !contattoNuovo.getAliasContatto().equalsIgnoreCase(contattoVecchio.getAliasContatto()));
	}

	private static boolean getFlagCognomeModificato(final Contatto contattoVecchio, final Contatto contattoNuovo) {
		return (StringUtils.isNullOrEmpty(contattoNuovo.getCognome()) 
				&& !StringUtils.isNullOrEmpty(contattoVecchio.getCognome()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getCognome()) && StringUtils.isNullOrEmpty(contattoVecchio.getCognome()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getCognome()) 
						&& !contattoNuovo.getCognome().equalsIgnoreCase(contattoVecchio.getCognome()));
	}

	private static boolean getFlagNomeModificato(final Contatto contattoVecchio, final Contatto contattoNuovo) {
		return (StringUtils.isNullOrEmpty(contattoNuovo.getNome()) && !StringUtils.isNullOrEmpty(contattoVecchio.getNome()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getNome()) && StringUtils.isNullOrEmpty(contattoVecchio.getNome()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getNome()) && !contattoNuovo.getNome().equalsIgnoreCase(contattoVecchio.getNome()));
	}

	private static boolean getFlagTipoPersonaModificato(final Contatto contattoVecchio, final Contatto contattoNuovo) {
		return (StringUtils.isNullOrEmpty(contattoNuovo.getTipoPersona())
				&& !StringUtils.isNullOrEmpty(contattoVecchio.getTipoPersona()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getTipoPersona()) && StringUtils.isNullOrEmpty(contattoVecchio.getTipoPersona()))
				|| (!StringUtils.isNullOrEmpty(contattoNuovo.getTipoPersona()) 
						&& !contattoNuovo.getTipoPersona().equalsIgnoreCase(contattoVecchio.getTipoPersona()));
	}

	private void flagTitoloModificato(final NotificaContatto notifica, final Contatto contattoVecchio, final Connection connection, final Contatto contattoNuovo,
			final boolean flagTitoloModificato) {
		if (flagTitoloModificato) {
			notifica.setOperazioneEnum(TipoOperazioneEnum.MODIFICA_TITOLO_CONTATTO);
			notifica.setVecchioValore(contattoVecchio.getTitolo());
			notifica.setNuovoValore(contattoNuovo.getTitolo());
			insertNotifica(notifica, connection);
		}
	}

	private void flagIdComuneIstatModificato(final NotificaContatto notifica, final Contatto contattoVecchio, final Connection connection, final Contatto contattoNuovo,
			final boolean flagIdComuneIstatModificato) {
		if (flagIdComuneIstatModificato) {
			notifica.setOperazioneEnum(TipoOperazioneEnum.MODIFICA_COMUNE_CONTATTO);
			String vecchioContattoDenominazioneComune = null;
			if (!StringUtils.isNullOrEmpty(contattoVecchio.getIdComuneIstat())) {
				vecchioContattoDenominazioneComune = comuneDAO.get(contattoVecchio.getIdComuneIstat(), connection).getDenominazione();
			}
			notifica.setVecchioValore(vecchioContattoDenominazioneComune);
			notifica.setNuovoValore(contattoNuovo.getComune());
			insertNotifica(notifica, connection);
		}
	}

	private void flagIdProvinciaIstatModificato(final NotificaContatto notifica, final Contatto contattoVecchio, final Connection connection, final Contatto contattoNuovo,
			final boolean flagIdProvinciaIstatModificato) {
		if (flagIdProvinciaIstatModificato) {
			notifica.setOperazioneEnum(TipoOperazioneEnum.MODIFICA_PROVINCIA_CONTATTO);
			String vecchioContattoDenominazioneProvincia = null;
			if (!StringUtils.isNullOrEmpty(contattoVecchio.getIdProvinciaIstat())) {
				vecchioContattoDenominazioneProvincia = provinciaDAO.getProvincia(contattoVecchio.getIdProvinciaIstat(), connection).getDenominazione();
			}
			notifica.setVecchioValore(vecchioContattoDenominazioneProvincia);
			notifica.setNuovoValore(contattoNuovo.getProvincia());
			insertNotifica(notifica, connection);
		}
	}

	private void flagIdRegioneIstatModificato(final NotificaContatto notifica, final Contatto contattoVecchio, final Connection connection, final Contatto contattoNuovo,
			final boolean flagIdRegioneIstatModificato) {
		if (flagIdRegioneIstatModificato) {
			String vecchioContattoDenominazioneRegione = null;
			if (!StringUtils.isNullOrEmpty(contattoVecchio.getIdRegioneIstat())) {
				vecchioContattoDenominazioneRegione = regioneDAO.get(Long.parseLong(contattoVecchio.getIdRegioneIstat()), connection).getDenominazione();
			}
			notifica.setOperazioneEnum(TipoOperazioneEnum.MODIFICA_REGIONE_CONTATTO);
			notifica.setVecchioValore(vecchioContattoDenominazioneRegione);
			notifica.setNuovoValore(contattoNuovo.getRegione());
			insertNotifica(notifica, connection);
		}
	}

	private void flagFaxModificato(final NotificaContatto notifica, final Contatto contattoVecchio, final Connection connection, final Contatto contattoNuovo,
			final boolean flagFaxModificato) {
		if (flagFaxModificato) {
			notifica.setOperazioneEnum(TipoOperazioneEnum.MODIFICA_FAX_CONTATTO);
			notifica.setVecchioValore(contattoVecchio.getFax());
			notifica.setNuovoValore(contattoNuovo.getFax());
			insertNotifica(notifica, connection);
		}
	}

	private void flagMailPecModificata(final NotificaContatto notifica, final Contatto contattoVecchio, final Connection connection, final Contatto contattoNuovo,
			final boolean flagMailPecModificata) {
		if (flagMailPecModificata) {
			notifica.setOperazioneEnum(TipoOperazioneEnum.MODIFICA_MAILPEC_CONTATTO);
			notifica.setVecchioValore(contattoVecchio.getMailPec());
			notifica.setNuovoValore(contattoNuovo.getMailPec());
			insertNotifica(notifica, connection);
		}
	}

	private void flagMailModificata(final NotificaContatto notifica, final Contatto contattoVecchio, final Connection connection, final Contatto contattoNuovo,
			final boolean flagMailModificata) {
		if (flagMailModificata) {
			notifica.setOperazioneEnum(TipoOperazioneEnum.MODIFICA_MAILPEO_CONTATTO);
			notifica.setVecchioValore(contattoVecchio.getMail());
			notifica.setNuovoValore(contattoNuovo.getMail());
			insertNotifica(notifica, connection);
		}
	}

	private void flagCellulareModificato(final NotificaContatto notifica, final Contatto contattoVecchio, final Connection connection, final Contatto contattoNuovo,
			final boolean flagCellulareModificato) {
		if (flagCellulareModificato) {
			notifica.setOperazioneEnum(TipoOperazioneEnum.MODIFICA_CELLULARE_CONTATTO);
			notifica.setVecchioValore(contattoVecchio.getCellulare());
			notifica.setNuovoValore(contattoNuovo.getCellulare());
			insertNotifica(notifica, connection);
		}
	}

	private void flagTelefonoModificato(final NotificaContatto notifica, final Contatto contattoVecchio, final Connection connection, final Contatto contattoNuovo,
			final boolean flagTelefonoModificato) {
		if (flagTelefonoModificato) {
			notifica.setOperazioneEnum(TipoOperazioneEnum.MODIFICA_TELEFONO_CONTATTO);
			notifica.setVecchioValore(contattoVecchio.getTelefono());
			notifica.setNuovoValore(contattoNuovo.getTelefono());
			insertNotifica(notifica, connection);
		}
	}

	private void flagCAPModificato(final NotificaContatto notifica, final Contatto contattoVecchio, final Connection connection, final Contatto contattoNuovo,
			final boolean flagCAPModificato) {
		if (flagCAPModificato) {
			notifica.setOperazioneEnum(TipoOperazioneEnum.MODIFICA_CAP_CONTATTO);
			notifica.setVecchioValore(contattoVecchio.getCAP());
			notifica.setNuovoValore(contattoNuovo.getCAP());
			insertNotifica(notifica, connection);
		}
	}

	private void flagIndirizzoModificato(final NotificaContatto notifica, final Contatto contattoVecchio, final Connection connection, final Contatto contattoNuovo,
			final boolean flagIndirizzoModificato) {
		if (flagIndirizzoModificato) {
			notifica.setOperazioneEnum(TipoOperazioneEnum.MODIFICA_INDIRIZZO);
			notifica.setVecchioValore(contattoVecchio.getIndirizzo());
			notifica.setNuovoValore(contattoNuovo.getIndirizzo());
			insertNotifica(notifica, connection);
		}
	}

	private void aliasContattoModificato(final NotificaContatto notifica, final Contatto contattoVecchio, final Connection connection, final Contatto contattoNuovo,
			final boolean flagAliasContattoModificato) {
		if (flagAliasContattoModificato) {
			notifica.setOperazioneEnum(TipoOperazioneEnum.MODIFICA_DESCRIZIONE_ALIAS_CONTATTO);
			notifica.setVecchioValore(contattoVecchio.getAliasContatto());
			notifica.setNuovoValore(contattoNuovo.getAliasContatto());
			insertNotifica(notifica, connection);
		}
	}

	private void cognomeModificato(final NotificaContatto notifica, final Contatto contattoVecchio, final Connection connection, final Contatto contattoNuovo,
			final boolean flagCognomeModificato) {
		if (flagCognomeModificato) {
			notifica.setOperazioneEnum(TipoOperazioneEnum.MODIFICA_COGNOME_CONTATTO);
			notifica.setVecchioValore(contattoVecchio.getCognome());
			notifica.setNuovoValore(contattoNuovo.getCognome());
			insertNotifica(notifica, connection);
		}
	}

	private void nomeModificato(final NotificaContatto notifica, final Contatto contattoVecchio, final Connection connection, final Contatto contattoNuovo,
			final boolean flagNomeModificato) {
		if (flagNomeModificato) {
			notifica.setOperazioneEnum(TipoOperazioneEnum.MODIFICA_NOME_CONTATTO);
			notifica.setVecchioValore(contattoVecchio.getNome());
			notifica.setNuovoValore(contattoNuovo.getNome());
			insertNotifica(notifica, connection);
		}
	}

	private void tipoPersonaModificato(final NotificaContatto notifica, final Contatto contattoVecchio, final Connection connection, final Contatto contattoNuovo,
			final boolean flagTipoPersonaModificato) {
		if (flagTipoPersonaModificato) {
			notifica.setOperazioneEnum(TipoOperazioneEnum.MODIFICA_TIPO_CONTATTO);
			if (contattoVecchio.getTipoPersona() != null) {
				if ("F".equalsIgnoreCase(contattoVecchio.getTipoPersona())) {
					notifica.setVecchioValore("Fisica");
				} else if ("G".equalsIgnoreCase(contattoVecchio.getTipoPersona())) {
					notifica.setVecchioValore("Giuridica");
				}
			}

			if (contattoNuovo.getTipoPersona() != null) {
				if ("F".equalsIgnoreCase(contattoNuovo.getTipoPersona())) {
					notifica.setNuovoValore("Fisica");
				} else if ("G".equalsIgnoreCase(contattoNuovo.getTipoPersona())) {
					notifica.setNuovoValore("Giuridica");
				}
			}

			insertNotifica(notifica, connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#creaGruppo(it.ibm.red.business.persistence.model.Contatto,
	 *      java.util.Collection, java.lang.Long, java.lang.Long).
	 */
	@Override
	public void creaGruppo(final Contatto gruppo, final Collection<Contatto> contattiSelezionati, final Long idUfficio, final Long idUtente) {
		Connection connection = null;
		try {
			// Come su RED controllo se il gruppo già esiste tra quelli attivi, se esiste ma
			// è disattivato lo creo lo stesso
			connection = setupConnection(getDataSource().getConnection(), true);

			contattoDAO.creaGruppo(gruppo, contattiSelezionati, idUfficio, idUtente, connection);

			commitConnection(connection);
		} catch (final Exception e) {
			LOGGER.error(e);
			rollbackConnection(connection);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#modificaGruppo(it.ibm.red.business.persistence.model.Contatto,
	 *      java.util.List, java.util.List, java.lang.Long).
	 */
	@Override
	public void modificaGruppo(final Contatto gruppo, final List<Contatto> contattiSelezionati, final List<Contatto> contattidaRimuovere, final Long idUfficio) {
		Connection connection = null;

		try {
			// Controllo se il nome del gruppo non è già utilizzato, in quanto non dovrei
			// avere duplicati tra quelli attivi, vedi creaGruppo
			connection = setupConnection(getDataSource().getConnection(), true);
			contattoDAO.modificaGruppo(gruppo, contattiSelezionati, contattidaRimuovere, idUfficio, connection);

			commitConnection(connection);
		} catch (final Exception e) {
			LOGGER.error(e);
			rollbackConnection(connection);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#rimuoviDAGruppo(it.ibm.red.business.persistence.model.Contatto,
	 *      java.util.List).
	 */
	@Override
	public void rimuoviDAGruppo(final Contatto gruppo, final List<Contatto> contattiDaRimuovere) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			contattoDAO.rimuoviDAGruppo(gruppo, contattiDaRimuovere, connection);
			commitConnection(connection);
		} catch (final Exception e) {
			LOGGER.error(e);
			rollbackConnection(connection);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#eliminaGruppo(it.ibm.red.business.persistence.model.Contatto,
	 *      java.lang.Long).
	 */
	@Override
	public void eliminaGruppo(final Contatto gruppo, final Long idUfficio) {
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			// Come su RED non elimino l'associazione tra il gruppo e il contatto
			contattoDAO.eliminaGruppo(gruppo, connection);
			contattoDAO.rimuoviPreferito(idUfficio, gruppo.getContattoID(), gruppo.getTipoRubricaEnum(), connection);

			commitConnection(connection);
		} catch (final Exception e) {
			LOGGER.error(e);
			rollbackConnection(connection);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#notificaEliminazioneContatto(it.ibm.red.business.persistence.model.NotificaContatto).
	 */
	@Override
	public void notificaEliminazioneContatto(final NotificaContatto notifica) {
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			insertNotifica(notifica, connection);

			commitConnection(connection);
		} catch (final Exception e) {
			rollbackConnection(connection);
			LOGGER.error("Si è verificato un errore durante l'inserimento della notifica contatto", e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	private void insertNotifica(final NotificaContatto notifica, final Connection connection) {
		notificaContattoDAO.insertNotificaContatto(notifica, connection);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#getNotifiche(java.lang.Long,
	 *      java.lang.String).
	 */
	@Override
	public List<NotificaContatto> getNotifiche(final Long idContatto, final String statoNotifica) {
		List<NotificaContatto> listaNotifiche = new ArrayList<>();
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			listaNotifiche = notificaContattoDAO.getListaNotificheByStato(idContatto, statoNotifica, connection);
		} catch (final Exception e) {
			LOGGER.error("getNotifiche -> Si è verificato un errore durante il caricamento delle notifiche contatto", e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return listaNotifiche;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#getNotificheAdmin(java.lang.Long,
	 *      java.lang.Long, java.lang.String, int, java.lang.Integer,
	 *      java.lang.Long).
	 */
	@Override
	public List<NotificaContatto> getNotificheAdmin(final Long idNodo, final Long idAoo, final String tipoNotifica, final int numGiorni, final Integer numRow,
			final Long idUtente) {
		Connection connection = null;
		List<NotificaContatto> listaNotifiche = new ArrayList<>();

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			if (NotificaContatto.TIPO_NOTIFICA_RICHIESTA.equalsIgnoreCase(tipoNotifica)) {
				listaNotifiche = notificaContattoDAO.getListaNotificheRichieste(numGiorni, numRow, idAoo, idUtente, connection);
			} else if (NotificaContatto.TIPO_NOTIFICA_EVASA.equalsIgnoreCase(tipoNotifica)) {
				listaNotifiche = notificaContattoDAO.getListaNotificheEvase(idNodo, numGiorni, numRow, connection);
			}
		} catch (final Exception e) {
			LOGGER.error("getNotificheAdmin -> Si è verificato un errore durante il caricamento delle notifiche", e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return listaNotifiche;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#getNotifiche(java.lang.Long,
	 *      int, int, java.lang.String).
	 */
	@Override
	public List<NotificaContatto> getNotifiche(final Long idNodo, final int numGiorni, final int numRow, final String utente) {
		List<NotificaContatto> listaNotifiche = new ArrayList<>();
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			listaNotifiche = notificaContattoDAO.getListaNotifiche(idNodo, numGiorni, numRow, connection, utente);
		} catch (final Exception e) {
			LOGGER.error("getNotifiche -> Si è verificato un errore durante il caricamento delle notifiche", e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return listaNotifiche;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#getStruttureILivello().
	 */
	@Override
	public List<String> getStruttureILivello() {
		Connection connection = null;
		List<String> primoLivello;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			primoLivello = contattoDAO.caricaIlivello(connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il caricamento delle strutture di I livello", e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return primoLivello;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#getStruttureIILivello(java.lang.String).
	 */
	@Override
	public List<String> getStruttureIILivello(final String idStrutturaILivello) {
		Connection connection = null;
		List<String> secondoLivello;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			secondoLivello = contattoDAO.caricaIIlivello(idStrutturaILivello, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il caricamento delle strutture di II livello", e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return secondoLivello;
	}

	/**
	 * Restituisce la struttura del terzo livello.
	 * 
	 * @param idStrutturaIILivello
	 * @return terzo livello
	 */
	@Override
	public List<String> getStruttureIIILivello(final String idStrutturaIILivello) {
		Connection connection = null;
		List<String> terzoLivello;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			terzoLivello = contattoDAO.caricaIIIlivello(idStrutturaIILivello, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il caricamento delle strutture di III livello", e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return terzoLivello;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#getContattoFromNotifica(it.ibm.red.business.persistence.model.NotificaContatto).
	 */
	@Override
	public Contatto getContattoFromNotifica(final NotificaContatto notifica) {
		Connection connection = null;
		Contatto contatto = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			contatto = contattoDAO.ricercaContattoPerModificaByID(notifica.getIdContatto(), connection);

			if (notifica.getOperazioneEnum().equals(TipoOperazioneEnum.MODIFICA_CAP_CONTATTO)) {
				contatto.setCAP(notifica.getNuovoValore());
			} else if (notifica.getOperazioneEnum().equals(TipoOperazioneEnum.MODIFICA_COGNOME_CONTATTO)) {
				contatto.setCognome(notifica.getNuovoValore());
			} else if (notifica.getOperazioneEnum().equals(TipoOperazioneEnum.MODIFICA_DESCRIZIONE_ALIAS_CONTATTO)) {
				contatto.setAliasContatto(notifica.getNuovoValore());
			} else if (notifica.getOperazioneEnum().equals(TipoOperazioneEnum.MODIFICA_FAX_CONTATTO)) {
				contatto.setFax(notifica.getNuovoValore());
			} else if (notifica.getOperazioneEnum().equals(TipoOperazioneEnum.MODIFICA_INDIRIZZO)) {
				contatto.setIndirizzo(notifica.getNuovoValore());
			} else if (notifica.getOperazioneEnum().equals(TipoOperazioneEnum.MODIFICA_MAILPEC_CONTATTO)) {
				contatto.setMailPec(notifica.getNuovoValore());
			} else if (notifica.getOperazioneEnum().equals(TipoOperazioneEnum.MODIFICA_MAILPEO_CONTATTO)) {
				contatto.setMail(notifica.getNuovoValore());
			} else if (notifica.getOperazioneEnum().equals(TipoOperazioneEnum.MODIFICA_NOME_CONTATTO)) {
				contatto.setNome(notifica.getNuovoValore());
			} else if (notifica.getOperazioneEnum().equals(TipoOperazioneEnum.MODIFICA_TELEFONO_CONTATTO)) {
				contatto.setTelefono(notifica.getNuovoValore());
			} else if (notifica.getOperazioneEnum().equals(TipoOperazioneEnum.MODIFICA_CELLULARE_CONTATTO)) {
				contatto.setCellulare(notifica.getNuovoValore());
			} else if (notifica.getOperazioneEnum().equals(TipoOperazioneEnum.MODIFICA_TIPO_CONTATTO)) {
				contatto.setTipoPersona(notifica.getNuovoValore());
			} else if (notifica.getOperazioneEnum().equals(TipoOperazioneEnum.MODIFICA_COMUNE_CONTATTO)) {
				modificaComuneContatto(notifica, connection, contatto);
			} else if (notifica.getOperazioneEnum().equals(TipoOperazioneEnum.MODIFICA_PROVINCIA_CONTATTO)) {
				modificaProvinciaContatto(notifica, connection, contatto);
			} else if (notifica.getOperazioneEnum().equals(TipoOperazioneEnum.MODIFICA_REGIONE_CONTATTO)) {
				modificaRegioneContatto(notifica, connection, contatto);
			} else if (notifica.getOperazioneEnum().equals(TipoOperazioneEnum.MODIFICA_TITOLO_CONTATTO)) {
				contatto.setTitolo(notifica.getNuovoValore());
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il caricamento del contatto.", e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
		return contatto;
	}

	private void modificaComuneContatto(final NotificaContatto notifica, Connection connection, Contatto contatto) {
		final String comune = notifica.getNuovoValore();
		final ComuneDTO comuneDTO = comuneDAO.getComuneByNome(comune, connection);
		contatto.setComune(comune);
		contatto.setIdComuneIstat(comuneDTO.getIdComune());
		contatto.setComuneObj(comuneDTO);
	}
	
	private void modificaProvinciaContatto(final NotificaContatto notifica, Connection connection, Contatto contatto) {
		final String provincia = notifica.getNuovoValore();
		final ProvinciaDTO provinciaDTO = provinciaDAO.getProvinciaByNome(provincia, connection);
		contatto.setProvincia(provincia);
		contatto.setIdProvinciaIstat(provinciaDTO.getIdProvincia());
		contatto.setProvinciaObj(provinciaDTO);
	}
	
	private void modificaRegioneContatto(final NotificaContatto notifica, Connection connection, Contatto contatto) {
		final String regione = notifica.getNuovoValore();
		final RegioneDTO regioneDTO = regioneDAO.getRegioneByNome(regione, connection);
		contatto.setRegione(regione);
		contatto.setIdRegioneIstat(regioneDTO.getIdRegione());
		contatto.setRegioneObj(regioneDTO);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#getContattiChildIPA(it.ibm.red.business.persistence.model.Contatto).
	 */
	@Override
	public List<Contatto> getContattiChildIPA(final Contatto contattoIpa) {
		Connection con = null;
		List<Contatto> listaContatti = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			listaContatti = new ArrayList<>();
			listaContatti.addAll(contattoDAO.ricercaFigliContattoIPA(contattoIpa, con));
		} catch (final Exception e) {
			LOGGER.error("getContattiChild(Contatto contattoIpa): Si è verificata un'anomalia con la connessione al DB", e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		return listaContatti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#getContattiCasellePostaliFromName(java.util.List,
	 *      java.lang.String).
	 */
	@Override
	public List<Contatto> getContattiCasellePostaliFromName(final List<CasellaPostaDTO> casellePostali, final String nomeContatto) {
		Connection con = null;
		final List<Contatto> listaContatti = new ArrayList<>();

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			for (final CasellaPostaDTO casellaPostale : casellePostali) {
				final List<Contatto> contattiTemp = contattoDAO.getContattiMailFromAlias(casellaPostale.getIndirizzoEmail(), nomeContatto, con);
				for (final Contatto contatto : contattiTemp) {
					if (contatto.isContattoInGruppo(listaContatti) < 0) {
						listaContatti.add(contatto);
					}
				}

			}
		} catch (final Exception e) {
			LOGGER.error("Attenzione, errore durante il recupero della lista dei contatti", e);
			throw new RedException("Attenzione,errore durante il recupero della lista dei contatti");
		} finally {
			closeConnection(con);
		}

		return listaContatti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#eliminaContattoMail(long).
	 */
	@Override
	public int eliminaContattoMail(final long idContatto) {
		Connection con = null;
		int result = 0;
		try {
			con = setupConnection(getDataSource().getConnection(), true);
			result = contattoDAO.eliminaContattoEmail(idContatto, con);

			commitConnection(con);
		} catch (final Exception e) {
			rollbackConnection(con);
			LOGGER.error("Attenzione, errore nell'eliminazione del contatto", e);
			throw new RedException("Errore durante l'eliminazione del contatto");
		} finally {
			closeConnection(con);
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#updateContattoCasellaPostale(it.ibm.red.business.persistence.model.Contatto,
	 *      java.util.List).
	 */
	@Override
	public long updateContattoCasellaPostale(final Contatto contatto, final List<CasellaPostaDTO> casellePostali) {
		Connection con = null;
		long updated = 0;

		try {
			con = setupConnection(getDataSource().getConnection(), true);
			updated = contattoDAO.updateContattoCasellaPostale(contatto, con);
			contattoDAO.eliminaAssociazioneContattoCasellaPostale(contatto.getContattoID(), con);
			for (final CasellaPostaDTO casella : casellePostali) {
				contattoDAO.associaContattoACasellaPostale(contatto.getContattoID(), casella.getIndirizzoEmail(), con);
			}

			commitConnection(con);
		} catch (final Exception e) {
			rollbackConnection(con);
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		return updated;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#inserisciContattoCasellaPostale(it.ibm.red.business.persistence.model.Contatto,
	 *      java.util.List).
	 */
	@Override
	public long inserisciContattoCasellaPostale(final Contatto c, final List<CasellaPostaDTO> casellePostali) {
		Connection connection = null;
		long idContatto;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			idContatto = contattoDAO.inserisciContattoCasellaPostale(c, connection);
			for (final CasellaPostaDTO casellaPostale : casellePostali) {
				contattoDAO.associaContattoACasellaPostale(idContatto, casellaPostale.getIndirizzoEmail(), connection);
			}
			commitConnection(connection);
		} catch (final Exception e) {
			rollbackConnection(connection);
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return idContatto;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#esportaContatti(java.util.List,
	 *      java.lang.Long).
	 */
	@Override
	public List<Contatto> esportaContatti(final List<Contatto> listaContatti, final Long idAOO) {
		final List<Contatto> listaContattiNonEsportati = new ArrayList<>();
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), true);
			for (final Contatto contatto : listaContatti) {
				if (checkContattoDaEsportare(contatto, listaContattiNonEsportati, con)) {
					contatto.setDatacreazione(new Date());
					contatto.setPubblico(1);
					contatto.setIdAOO(idAOO);
					contattoDAO.insertContatto(contatto, con);
				}
			}

			commitConnection(con);
		} catch (final Exception e) {
			rollbackConnection(con);
			LOGGER.error("Errore durante l'esporazione dei contatti", e);
			throw new RedException("Errore durante l'esporazione dei contatti", e);
		} finally {
			closeConnection(con);
		}

		return listaContattiNonEsportati;
	}

	private boolean checkContattoDaEsportare(final Contatto contatto, final List<Contatto> listaContattiNonEsportati, final Connection con) {
		boolean result = false;
		if (StringUtils.isNullOrEmpty(contatto.getNome())) {
			contatto.setMotivoEsportazione("Campo Nome mancante");
			listaContattiNonEsportati.add(contatto);
		} else if ("F".equalsIgnoreCase(contatto.getTipoPersona()) && StringUtils.isNullOrEmpty(contatto.getCognome())) {
			contatto.setMotivoEsportazione("Campo Cognome mancante");
			listaContattiNonEsportati.add(contatto);
		} else {

			final boolean esiste = contattoDAO.checkEsistenzaContattoRED(contatto, con);

			if (esiste) {
				contatto.setMotivoEsportazione("Contatto già presente");
				listaContattiNonEsportati.add(contatto);
			} else {
				result = true;
			}
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#inserisciGruppoEmail(it.ibm.red.business.persistence.model.Contatto,
	 *      java.util.List, java.util.List).
	 */
	@Override
	public int inserisciGruppoEmail(final Contatto gruppoEmail, final List<CasellaPostaDTO> casellePostali, final List<Contatto> contattiDaAssociare) {
		int idGruppoInserito = 0;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), true);
			idGruppoInserito = contattoDAO.inserisciGruppoEmail(gruppoEmail, con);
			if (idGruppoInserito > 0 && casellePostali != null) {
				for (final CasellaPostaDTO caselle : casellePostali) {
					if (caselle != null) {
						contattoDAO.associaCasellaPostaleGruppo(idGruppoInserito, caselle.getIndirizzoEmail(), con);
					}
				}
				for (final Contatto contatto : contattiDaAssociare) {
					contattoDAO.associaContattoGruppoMail(idGruppoInserito, contatto.getContattoID(), con);
				}
			}

			commitConnection(con);
		} catch (final Exception e) {
			rollbackConnection(con);
			LOGGER.error("Attenzione, errore durante l'inserimento del gruppo nella casella postale: ", e);
			throw new RedException("Errore durante la creazione del gruppo");
		} finally {
			closeConnection(con);
		}

		return idGruppoInserito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#getListaGruppoEmailByNomeECasellaPostale(java.util.List,
	 *      java.lang.String).
	 */
	@Override
	public List<Contatto> getListaGruppoEmailByNomeECasellaPostale(final List<CasellaPostaDTO> casellePostali, final String nome) {
		Connection con = null;
		final List<Contatto> listaGruppi = new ArrayList<>();
		List<Contatto> listaGruppiTemp = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			for (final CasellaPostaDTO casella : casellePostali) {
				listaGruppiTemp = contattoDAO.getListaGruppoEmailByNomeEmail(casella.getIndirizzoEmail(), nome, con);
				if (listaGruppiTemp != null && !listaGruppiTemp.isEmpty()) {
					for (final Contatto gruppoEmail : listaGruppiTemp) {
						if (gruppoEmail.isContattoInGruppo(listaGruppi) < 0) {
							listaGruppi.add(gruppoEmail);
						}

					}
				}
			}

		} catch (final Exception e) {
			LOGGER.error("Attenzione, errore durante il recupero dei gruppi: ", e);
			throw new RedException("Errore durante il recupero dei gruppi");
		} finally {
			closeConnection(con);
		}

		return listaGruppi;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#getContattiAssociati(it.ibm.red.business.persistence.model.Contatto).
	 */
	@Override
	public Collection<Contatto> getContattiAssociati(final Contatto gruppoEmail) {
		Connection con = null;
		Collection<Contatto> listaContatti = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			listaContatti = contattoDAO.getListaContattiGruppoEmail(gruppoEmail.getContattoID(), con);
		} catch (final Exception e) {
			LOGGER.error("Attenzione, errore durante il recupero dei contatti associati al gruppo ", e);
			throw new RedException("Errore durante il recupero dei contatti associati al gruppo");
		} finally {
			closeConnection(con);
		}

		return listaContatti;

	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#getListaCasellaPostaleGruppoEmail(long).
	 */
	@Override
	public Collection<String> getListaCasellaPostaleGruppoEmail(final long idContatto) {
		Connection con = null;
		Collection<String> email = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			email = contattoDAO.getListaCasellaPostaleGruppoEmail(idContatto, con);
		} catch (final Exception e) {
			LOGGER.error("errore durante il recupero delle caselle postali ", e);
			throw new RedException("errore durante il recupero delle caselle postali");
		} finally {
			closeConnection(con);
		}

		return email;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#eliminaGruppoEmail(java.lang.Long).
	 */
	@Override
	public int eliminaGruppoEmail(final Long idGruppoEmail) {
		Connection con = null;
		int result = 0;
		try {
			con = setupConnection(getDataSource().getConnection(), true);
			result = contattoDAO.eliminaGruppoEmail(idGruppoEmail, con);

			commitConnection(con);
		} catch (final Exception e) {
			rollbackConnection(con);
			LOGGER.error("Attenzione, errore nell'eliminazione del gruppo", e);
			throw new RedException("Attenzione, errore nell'eliminazione del gruppo");
		} finally {
			closeConnection(con);
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#updateGruppoEmail(it.ibm.red.business.persistence.model.Contatto,
	 *      java.util.List, java.util.List).
	 */
	@Override
	public int updateGruppoEmail(final Contatto gruppoEmail, final List<CasellaPostaDTO> casellePostali, final List<Contatto> contattiDaAssociare) {
		int updated = 0;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), true);

			updated = contattoDAO.updateGruppoEmail(gruppoEmail, con);
			contattoDAO.eliminaAssociazioneContattiGruppoEmail(gruppoEmail.getContattoID(), con);
			contattoDAO.eliminaAssociazioneCaselleGruppoEmail(gruppoEmail.getContattoID(), con);
			for (final CasellaPostaDTO caselle : casellePostali) {
				if (caselle != null) {
					contattoDAO.associaCasellaPostaleGruppo(gruppoEmail.getContattoID(), caselle.getIndirizzoEmail(), con);
				}
			}
			for (final Contatto contatto : contattiDaAssociare) {
				contattoDAO.associaContattoGruppoMail(gruppoEmail.getContattoID(), contatto.getContattoID(), con);
			}

			commitConnection(con);
		} catch (final Exception e) {
			rollbackConnection(con);
			LOGGER.error("Attenzione, errore durante l'aggiornamento del gruppo ", e);
			throw new RedException("Errore durante l'aggiornamento del gruppo");
		} finally {
			closeConnection(con);
		}

		return updated;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#getContattoFromMail(java.lang.String,
	 *      int, java.lang.Long).
	 */
	@Override
	public List<Contatto> getContattoFromMail(final String mailMittente, final int idAoo, final Long idUfficio) {
		List<Contatto> listaContatti = null;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			final RicercaRubricaDTO ricercaContattoObj = new RicercaRubricaDTO();
			ricercaContattoObj.setRicercaRED(true);
			ricercaContattoObj.setRicercaIPA(true);
			ricercaContattoObj.setRicercaMEF(true);
			ricercaContattoObj.setRicercaGruppo(false);
			ricercaContattoObj.setMail(mailMittente);
			ricercaContattoObj.setIdAoo(idAoo);
			listaContatti = contattoDAO.ricerca(idUfficio, ricercaContattoObj, con);
		} catch (final Exception e) {
			LOGGER.error("Attenzione, errore durante l'aggiornamento del gruppo ", e);
			throw new RedException("Errore durante l'aggiornamento del gruppo");
		} finally {
			closeConnection(con);
		}

		return listaContatti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#getGruppoEmailByID(java.lang.Long).
	 */
	@Override
	public Contatto getGruppoEmailByID(final Long idGruppo) {
		Connection connection = null;
		Contatto contatto = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			contatto = contattoDAO.getGruppoEmailById(idGruppo, connection);
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return contatto;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#getCountListaNotificheRichieste(java.lang.Long,
	 *      int).
	 */
	@Override
	public int getCountListaNotificheRichieste(final Long idAoo, final int numGiorni) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			return notificaContattoDAO.getCountListaNotificheRichieste(idAoo, numGiorni, connection);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#getContattoFromMailInterop(java.lang.String).
	 */
	@Override
	public Contatto getContattoFromMailInterop(final String mailMittente) {
		Contatto contattoRicerca = null;
		Connection conn = null;

		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			// ricerco il contatto prima su red
			if (mailMittente != null) {
				contattoRicerca = contattoDAO.ricercaRedInteroperabilita(mailMittente, conn);
				if (contattoRicerca == null) {
					// se non lo trovo cerco su ipa
					contattoRicerca = contattoDAO.ricercaIPAInteroperabilita(mailMittente, conn);
					if (contattoRicerca == null) {
						// se non lo trovo cerco su mef
						contattoRicerca = contattoDAO.ricercaMEFInteroperabilita(mailMittente, conn);
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Attenzione, errore durante la ricerca del contatto", e);
			throw new RedException("Attenzione, errore durante la ricerca del contatto");
		} finally {
			closeConnection(conn);
		}

		return contattoRicerca;
	}

	/**
	 * @see it.ibm.red.business.service.IRubricaSRV#inserisciNotificaCreazioneAutomaticaContatto(it.ibm.red.business.persistence.model.Contatto,
	 *      java.lang.Long, java.lang.String, it.ibm.red.business.dto.UtenteDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public void inserisciNotificaCreazioneAutomaticaContatto(final Contatto contatto, final Long idContatto, final String tipoRubrica, final UtenteDTO utenteCreatore,
			final Connection connection) {
		try {
			final NotificaContatto notifica = new NotificaContatto();
			notifica.setNuovoContatto(contatto);
			notifica.setTipoNotifica(NotificaContatto.TIPO_NOTIFICA_RICHIESTA);
			notifica.setAlias(contatto.getAliasContatto());
			notifica.setCognome(contatto.getCognome());
			notifica.setNome(contatto.getNome());
			notifica.setMail(contatto.getMail());
			notifica.setMailPec(contatto.getMailPec());
			notifica.setDataOperazione(new Date());
			notifica.setIdContatto(idContatto);
			notifica.setIdNodoModifica(0L);
			notifica.setIdUtenteModifica(0L);
			notifica.setNote("Creazione automatica del contatto");
			notifica.setTipologiaContatto(tipoRubrica);
			notifica.setUtente(utenteCreatore.getUsername());
			notifica.setStato(StatoNotificaContattoEnum.IN_ATTESA);
			notifica.setOperazioneEnum(TipoOperazioneEnum.CREAZIONE_AUTOMATICA_CONTATTO);
			notifica.setIdAoo(utenteCreatore.getIdAoo());

			insertNotifica(notifica, connection);

		} catch (final Exception e) {
			LOGGER.error("Errore nell'inserimento della notifica di creazione automatica del contatto", e);
			throw new RedException(e);
		}
	}

	@Override
	public final Contatto insertContattoFlussoAutomatico(final Contatto inContatto, final UtenteDTO utenteCreatore) {
		Contatto out = null;
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			out = insertContattoFlussoAutomatico(inContatto, utenteCreatore, con);
		} catch (final Exception e) {
			LOGGER.error("Errore nella gestione del contatto.", e);
			throw new RedException("Errore nella gestione del contatto: " + e.getMessage(), e);
		} finally {
			closeConnection(con);
		}

		return out;

	}

	@Override
	public final Contatto insertContattoFlussoAutomatico(final Contatto inContatto, final UtenteDTO utenteCreatore, final Connection con) {
		Contatto out = null;
		try {
			out = inContatto;
			out.setIdAOO(utenteCreatore.getIdAoo());

			// Inserimento in rubrica del nuovo contatto
			// ID Ufficio è NULL perché il nuovo contatto non viene aggiunto ai preferiti
			final Long idNuovoContatto = inserisci(null, out, con, utenteCreatore.getCheckUnivocitaMail());

			// Si imposta l'ID nel contatto
			out.setContattoID(idNuovoContatto);

			LOGGER.info("Creato un nuovo contatto con ID [" + idNuovoContatto + "] per l'entità con nome [" + inContatto.getNome() + "] e cognome [" + inContatto.getCognome()
					+ "]. Tipo persona: " + inContatto.getTipoPersona());

		} catch (final Exception e) {
			LOGGER.error("Errore nella gestione del contatto.", e);
			throw new RedException("Errore nella gestione del contatto: " + e.getMessage(), e);
		}

		return out;

	}

	/**
	 * @see it.ibm.red.business.service.IRubricaSRV#insertContattoOnTheFly(it.ibm.red.business.persistence.model.Contatto,
	 *      java.lang.Long).
	 */
	@Override
	public Contatto insertContattoOnTheFly(final Contatto contatto, final Long idAoo) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			contatto.setPubblico(0);
			contatto.setDatacreazione(new Date());

			contatto.setOnTheFly(ON_THE_FLY);
			contatto.setDatadisattivazione(new Date());
			// Se provengo dalla protocollazione devo controllare che l'alias sia uguale
			// altrimenti
			// Devo procedere alla creazione dell'onTheFly
			if (!StringUtils.isNullOrEmpty(contatto.getAliasContattoProtocollazione()) && contatto.getAliasContattoProtocollazione().equals(contatto.getAliasContatto())) {
				contatto.setOnTheFly(0);
				contatto.setDatadisattivazione(null);
			}

			// Nel caso in cui modifichiamo la mail inline dobbiamo creare un cont sempre
			// fisico. Questo flag viene settato lato frontend nel javascript come onthefly
			// per evitare caricamento
			if (contatto.isSbiancaTipologiaPersona()) {
				contatto.setTipoPersona(null);
			}

			if (contatto.getTipoPersona() == null) {
				contatto.setTipoPersona("F");
			}
			contatto.setIdAOO(idAoo);
			contatto.setIPA(0);

			if (!StringUtils.isNullOrEmpty(contatto.getAliasContatto())) {
				// Se sono presenti nome e cognome sul contatto uso quelli altrimenti
				// metto l'alias - Per l'onthefly
				if (StringUtils.isNullOrEmpty(contatto.getNome())) {
					contatto.setNome(contatto.getAliasContatto());
				}

				if (StringUtils.isNullOrEmpty(contatto.getCognome())) {
					contatto.setCognome(contatto.getAliasContatto());
				}

			}

			if (!StringUtils.isNullOrEmpty(contatto.getMailSelected())) {
				if (TipologiaIndirizzoEmailEnum.PEC.equals(contatto.getTipologiaEmail())) {
					contatto.setMailPec(contatto.getMailSelected());
					contatto.setMail(null);
				} else {
					contatto.setMail(contatto.getMailSelected());
					contatto.setMailPec(null);
				}
			}

			final Long idContatto = contattoDAO.insertContatto(contatto, connection);
			contatto.setContattoID(idContatto);
			commitConnection(connection);

			return contatto;

		} catch (final Exception e) {
			rollbackConnection(connection);
			LOGGER.error("Errore durante l'inserimento del contatto on the fly", e);
			throw new RedException("Errore durante l'inserimento del contatto on the fly", e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IRubricaSRV#fetchContattiById(java.lang.String,
	 *      it.ibm.red.business.persistence.model.Aoo).
	 */
	@Override
	public List<Contatto> fetchContattiById(final String objectIdCont, final Aoo aoo) {
		final List<Contatto> listContatti = new ArrayList<>();
		PropertiesProvider pp = PropertiesProvider.getIstance();

		final AooFilenet aooFilenet = aoo.getAooFilenet();
		final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
				aooFilenet.getObjectStore(), aoo.getIdAoo());
		if (!StringUtils.isNullOrEmpty(objectIdCont)) {
			final IndependentObjectSet contattiList = fceh.fetchContatti(objectIdCont);
			final Iterator<?> iter = contattiList.iterator();
			while (iter.hasNext()) {
				final Contatto contattoNew = new Contatto();
				final CustomObject object = (CustomObject) iter.next();
				final String aliasContatto = (String) object.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.ALIAS_CONTATTO_ON_THE_FLY));
				final String nomeContatto = (String) object.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.NOME_CONTATTO_ON_THE_FLY));
				final String cognomeContatto = (String) object.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.COGNOME_CONTATTO_ON_THE_FLY));
				final String mailPeoContatto = (String) object.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.MAILPEO_CONTATTO_ON_THE_FLY));
				final String mailPecContatto = (String) object.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.MAILPEC_CONTATTO_ON_THE_FLY));
				final String idContatto = (String) object.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.ID_CONTATTO_ON_THE_FLY));

				final String tipoContatto = (String) object.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.TIPO_CONTATTO_ON_THE_FLY));
				final String indirizzo = (String) object.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.INDIRIZZO_CONTATTO_ON_THE_FLY));
				final String cap = (String) object.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.CAP_CONTATTO_ON_THE_FLY));
				final String telefono = (String) object.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.TELEFONO_CONTATTO_ON_THE_FLY));
				final String cellulare = (String) object.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.CELLULARE_CONTATTO_ON_THE_FLY));
				final String fax = (String) object.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.FAX_CONTATTO_ON_THE_FLY));
				final Integer idRegione = (Integer) object.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.ID_REGIONE_CONTATTO_ON_THE_FLY));
				final Integer idProvincia = (Integer) object.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.ID_PROVINCIA_CONTATTO_ON_THE_FLY));
				final Integer idComune = (Integer) object.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.ID_COMUNE_CONTATTO_ON_THE_FLY));
				final Integer isOnTheFly = (Integer) object.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.IS_ON_THE_FLY));
				final String titolo = (String) object.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.TITOLO_CONTATTO_ON_THE_FLY));
				final Boolean isPEC = (Boolean) object.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.IS_TIPOLOGIA_PEC_CONTATTO));

				if (!"null".equals(idContatto)) {
					contattoNew.setContattoID(Long.valueOf(idContatto));
					contattoNew.setAliasContatto(aliasContatto);
					contattoNew.setNome(nomeContatto);
					contattoNew.setCognome(cognomeContatto);
					contattoNew.setMail(mailPeoContatto);
					contattoNew.setMailPec(mailPecContatto);
					if (isPEC == null) {
						if (!StringUtils.isNullOrEmpty(mailPeoContatto)) {
							contattoNew.setMailSelected(mailPeoContatto);
							contattoNew.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEO);
						} else {
							contattoNew.setMailSelected(mailPecContatto);
							contattoNew.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEC);
						}
					} else if (Boolean.TRUE.equals(isPEC) && mailPecContatto!=null) {
						contattoNew.setMailSelected(mailPecContatto);
						contattoNew.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEC);
					}else if(mailPeoContatto !=null) {
						contattoNew.setMailSelected(mailPeoContatto);
						contattoNew.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEO);
					}
					contattoNew.setTipoPersona(tipoContatto);
					contattoNew.setIndirizzo(indirizzo);
					contattoNew.setCAP(cap);
					contattoNew.setTelefono(telefono);
					contattoNew.setCellulare(cellulare);
					contattoNew.setFax(fax);
					if (idRegione != null) {
						contattoNew.setIdRegioneIstat(String.valueOf(idRegione));
					}
					if (idProvincia != null) {
						contattoNew.setIdProvinciaIstat(String.valueOf(idProvincia));
					}
					if (idComune != null) {
						contattoNew.setIdComuneIstat(String.valueOf(idComune));
					}
					contattoNew.setOnTheFly(isOnTheFly);
					contattoNew.setTitolo(titolo);
				}
				listContatti.add(contattoNew);
			}
		}
		return listContatti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#notificaCreazioneContatto(it.ibm.red.business.persistence.model.NotificaContatto).
	 */
	@Override
	public void notificaCreazioneContatto(final NotificaContatto notifica) {
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			insertNotifica(notifica, connection);
			commitConnection(connection);
		} catch (final Exception e) {
			rollbackConnection(connection);
			LOGGER.error("Si è verificato un errore durante la creazione della notifica", e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#removeContattoRichiestaCreazione(java.lang.Long).
	 */
	@Override
	public void removeContattoRichiestaCreazione(final Long idContatto) {
		Connection conn = null;

		try {
			conn = setupConnection(getDataSource().getConnection(), true);
			contattoDAO.removeContattoRichiestaCreazione(idContatto, conn);
			commitConnection(conn);
		} catch (final Exception ex) {
			rollbackConnection(conn);
			LOGGER.error("Si è verificato un errore durante l'eliminazione della notifica", ex);
			throw new RedException(ex);
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#contattoGiaInGruppi(java.util.Collection,
	 *      java.lang.Long).
	 */
	@Override
	public void contattoGiaInGruppi(final Collection<Contatto> idgruppoToCheck, final Long idContattoToCheck) {
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			for (final Contatto gruppoToCheck : idgruppoToCheck) {
				final boolean isPresente = contattoDAO.contattoGiaInGruppi(gruppoToCheck.getContattoID(), idContattoToCheck, conn);
				if (isPresente) {
					gruppoToCheck.setSelected(isPresente);
				}
			}

		} catch (final Exception ex) {
			LOGGER.error("Si è verificato un errore durante l'eliminazione della notifica", ex);
			throw new RedException(ex);
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IRubricaSRV#eliminaContattiOnTheFlyRangeTemporale(it.ibm.red.business.persistence.model.Aoo).
	 */
	@Override
	public void eliminaContattiOnTheFlyRangeTemporale(final Aoo aoo) {
		Connection conn = null;
		try {
			LOGGER.info("START - Eliminazione contatto job");
			conn = setupConnection(getDataSource().getConnection(), true);
			contattoDAO.eliminaContattiOnTheFlyRangeTemporale(aoo.getNumGiorniEliminazioneContOnTheFlyJob(), aoo.getIdAoo(), conn);
			commitConnection(conn);
			LOGGER.info("END - Eliminazione contatto job");
		} catch (final Exception ex) {
			rollbackConnection(conn);
			LOGGER.error("Si è verificato un errore durante l'eliminazione del contatto on the fly");
			throw new RedException(ex);
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRubricaFacadeSRV#updateStatoNotificaRubrica(java.lang.Long,
	 *      java.lang.Long, java.lang.Integer).
	 */
	@Override
	public Integer updateStatoNotificaRubrica(final Long idUtente, final Long idAoo, final Integer idNotificaRubrica) {
		Connection conn = null;
		Integer output = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), true);
			output = notificaContattoDAO.updateStatoNotificaRubrica(StatoNotificaUtenteEnum.NOTIFICA_DA_LEGGERE.getIdStato(), idUtente, idAoo, idNotificaRubrica, conn);
			commitConnection(conn);
		} catch (final Exception ex) {
			rollbackConnection(conn);
			LOGGER.error("Errore durante l'update della notiica nella tabella stato notifica rubrica " + ex);
			throw new RedException("Errore durante l'update della notiica nella tabella stato notifica rubrica " + ex);
		} finally {
			closeConnection(conn);
		}

		return output;
	}

}