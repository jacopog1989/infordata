package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.Date;

import com.filenet.api.core.Document;

import it.ibm.red.business.dao.ITitolarioDAO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class FromDocumentoToFascicoloTrasformer.
 *
 * @author CPIERASC
 * 
 *         Trasformer fascicolo.
 */
public class FromDocumentoToFascicoloTrasformer extends TrasformerCE<FascicoloDTO> {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -8028086433965866619L;
	
	/**
	 * DAO.
	 */
	private ITitolarioDAO titolarioDAO;
	

	/**
	 * Costruttore.
	 */
	public FromDocumentoToFascicoloTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_FASCICOLO);
		titolarioDAO = ApplicationContextProvider.getApplicationContext().getBean(ITitolarioDAO.class);
	}
	
	/**
	 * Trasform.
	 *
	 * @param document the document
	 * @param connection the connection
	 * @return the fascicolo DTO
	 */
	@Override
	public final FascicoloDTO trasform(final Document document, final Connection connection) {
		String idFascicolo = (String) getMetadato(document, PropertiesNameEnum.NOME_FASCICOLO_METAKEY);
		String stato = (String) getMetadato(document, PropertiesNameEnum.STATO_FASCICOLO_METAKEY);
		String titolario = (String) getMetadato(document, PropertiesNameEnum.TITOLARIO_METAKEY);
		if (!StringUtils.isNullOrEmpty(titolario)) {
			String[] splitted = titolario.split("_");
			if (splitted.length > 1) {
				titolario = splitted[1];
			}
		}

		Integer idAOO = (Integer) getMetadato(document, PropertiesNameEnum.ID_AOO_METAKEY);
		
		String descrizioneTitolario = null;
		if (idAOO != null) {
			TitolarioDTO t = titolarioDAO.getNodoByIndice(idAOO.longValue(), titolario, connection);
			if (t != null) {
				descrizioneTitolario = t.getDescrizione();
			}
		}
		
		Date dataCreazione = (Date) getMetadato(document, PropertiesNameEnum.DATA_CREAZIONE_METAKEY);
		String oggetto = (String) getMetadato(document, PropertiesNameEnum.OGGETTO_METAKEY);
		String guid = StringUtils.cleanGuidToString(document.get_Id());
		String idFascicoloFEPA =  (String) getMetadato(document, PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA);
		
		String classeDocumentale = document.getClassName();
		String nomeDocumento = (String) getMetadato(document, PropertiesNameEnum.NOME_FASCICOLO_METAKEY);
		
		Boolean faldonato = (Boolean) getMetadato(document, PropertiesNameEnum.FALDONATO_METAKEY);
		Date dataTerminazione = (Date) getMetadato(document, PropertiesNameEnum.DATA_TERMINAZIONE_METAKEY);
		String documentTitle = (String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
		
		String tipoFascicoloFepa = (String) getMetadato(document, PropertiesNameEnum.METADATO_FASCICOLO_TIPO_FASCICOLO_FEPA_METAKEY);
		
		return new FascicoloDTO(idFascicolo, oggetto, titolario, dataCreazione, stato, guid, idFascicoloFEPA, classeDocumentale, 
				nomeDocumento, oggetto, idAOO, faldonato, dataTerminazione, documentTitle, descrizioneTitolario, tipoFascicoloFepa);
	}
}
