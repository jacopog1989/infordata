package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.service.facade.IPMefFacadeSRV;

/**
 * The Interface IPMefSRV.
 *
 * @author mcrescentini
 * 
 *         Interfaccia servizio per la protocollazione MEF.
 */
public interface IPMefSRV extends IPMefFacadeSRV {

	/**
	 * Ottiene la lista dei destinatari.
	 * @param utente
	 * @param assegnazioniCompetenza
	 * @param assegnazioniConoscenza
	 * @param assegnazioniContributo
	 * @param assegnazioniFirma
	 * @param assegnazioniSigla
	 * @param assegnazioniVisto
	 * @param assegnazioniFirmaMultipla
	 * @param controlloIter
	 * @param con
	 * @return lista dei destinatari
	 */
	List<String> getListaDestinatari(UtenteDTO utente, String[] assegnazioniCompetenza, String[] assegnazioniConoscenza, String[] assegnazioniContributo, 
			String[] assegnazioniFirma, String[] assegnazioniSigla,	String[] assegnazioniVisto, String[] assegnazioniFirmaMultipla, boolean controlloIter, Connection con);
}