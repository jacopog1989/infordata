package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.ColonneCodeDTO; 

/**
 * The Class GestioneColonneDAO.
 *
 * @author VINGENITO
 * 
 *         Dao per gestione delle colonne;
 */
public interface IGestioneColonneDAO extends Serializable {
	 
	/**
	 * Salva le colonne scelte.
	 * @param idUtente
	 * @param colonneEnum
	 * @param visibilita
	 * @param conn
	 */
	void salvaColonneScelte(Long idUtente, String colonneEnum, boolean visibilita, Connection conn);
	
	/**
	 * Cancella le colonne scelte.
	 * @param idUtente
	 * @param conn
	 */
	void deleteColonneScelte(Long idUtente, Connection conn);
	
	/**
	 * Ottiene le colonne scelte.
	 * @param idUtente
	 * @param conn
	 * @return lista delle colonne scelte
	 */
	List<ColonneCodeDTO> getColonneScelte(Long idUtente, Connection conn);
}