package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Model della CODATRASFORMAZIONE.
 * @author a.dilegge
 *
 */
public class CodaTrasformazioneDTO implements Serializable {

	private static final long serialVersionUID = 1947962400860503249L;
	
	/**
	 * Identificativo coda.
	 */
	private int idCoda;
	
	/**
	 * Identificativo documento.
	 */
	private String idDocumento;
	
	/**
	 * Processo chiamante.
	 */
	private String processoChiamante;
	
	/**
	 * Tipo trasformazione.
	 */
	private int tipoTrasformazione;
	
	/**
	 * Stato.
	 */
	private int stato;
	
	/**
	 * Flag Prioritaria.
	 */
	private int prioritaria;
	
	/**
	 * Data inserimento.
	 */
	private Date dataInserimento;
	
	/**
	 * Messaggio errore.
	 */
	private String messaggioErrore;
	
	/**
	 * Parametri.
	 */
	private CodaTrasformazioneParametriDTO parametri;

	/**
	 * Costruttore di classe.
	 * @param idCoda
	 * @param idDocumento
	 * @param processoChiamante
	 * @param tipoTrasformazione
	 * @param stato
	 * @param prioritaria
	 * @param dataInserimento
	 * @param messaggioErrore
	 * @param parametri
	 */
	public CodaTrasformazioneDTO(final int idCoda, final String idDocumento, final String processoChiamante, final int tipoTrasformazione,
			final int stato, final int prioritaria, final Date dataInserimento, final String messaggioErrore,
			final CodaTrasformazioneParametriDTO parametri) {
		this();
		this.idCoda = idCoda;
		this.idDocumento = idDocumento;
		this.processoChiamante = processoChiamante;
		this.tipoTrasformazione = tipoTrasformazione;
		this.stato = stato;
		this.prioritaria = prioritaria;
		this.dataInserimento = dataInserimento;
		this.messaggioErrore = messaggioErrore;
		this.parametri = parametri;
	}

	/**
	 * Costruttore di default.
	 */
	public CodaTrasformazioneDTO() {
		super();
	}

	/**
	 * Restituisce l'id della coda.
	 * @return idCoda
	 */
	public int getIdCoda() {
		return idCoda;
	}

	/**
	 * Imposta l'id della coda.
	 * @param idCoda
	 */
	public void setIdCoda(final int idCoda) {
		this.idCoda = idCoda;
	}

	/**
	 * Restituisce l'id del documento.
	 * @return idDocumento
	 */
	public String getIdDocumento() {
		return idDocumento;
	}

	/**
	 * Imposta l'id del documento.
	 * @param idDocumento
	 */
	public void setIdDocumento(final String idDocumento) {
		this.idDocumento = idDocumento;
	}

	/**
	 * Restituisce il processo chiamante.
	 * @return processoChiamante
	 */
	public String getProcessoChiamante() {
		return processoChiamante;
	}

	/**
	 * Imposta il processo chiamante.
	 * @param processoChiamante
	 */
	public void setProcessoChiamante(final String processoChiamante) {
		this.processoChiamante = processoChiamante;
	}

	/**
	 * Restituisce il tipo trasformazione.
	 * @return tipoTrasformazione
	 */
	public int getTipoTrasformazione() {
		return tipoTrasformazione;
	}

	/**
	 * Imposta il tipo trasformazione.
	 * @param tipoTrasformazione
	 */
	public void setTipoTrasformazione(final int tipoTrasformazione) {
		this.tipoTrasformazione = tipoTrasformazione;
	}

	/**
	 * Restituisce lo stato.
	 * @return stato
	 */
	public int getStato() {
		return stato;
	}

	/**
	 * Imposta lo stato.
	 * @param stato
	 */
	public void setStato(final int stato) {
		this.stato = stato;
	}

	/**
	 * Restituisce la data inserimento.
	 * @return dataInserimento
	 */
	public Date getDataInserimento() {
		return dataInserimento;
	}

	/**
	 * Imposta la data di inserimento.
	 * @param dataInserimento
	 */
	public void setDataInserimento(final Date dataInserimento) {
		this.dataInserimento = dataInserimento;
	}

	/**
	 * Restituisce il messaggio di errore.
	 * @return messaggioErrore
	 */
	public String getMessaggioErrore() {
		return messaggioErrore;
	}

	/**
	 * Imposta il messaggio di errore.
	 * @param messaggioErrore
	 */
	public void setMessaggioErrore(final String messaggioErrore) {
		this.messaggioErrore = messaggioErrore;
	}

	/**
	 * Restituisce i parametri. @see CodaTrasformazioneParametriDTO
	 * @return DTO che definisce i parametri i loro valori.
	 */
	public CodaTrasformazioneParametriDTO getParametri() {
		return parametri;
	}

	/**
	 * Imposta i parametri.
	 * @param parametri
	 */
	public void setParametri(final CodaTrasformazioneParametriDTO parametri) {
		this.parametri = parametri;
	}

	/**
	 * Getter prioritaria.
	 * @return prioritaria
	 */
	public int getPrioritaria() {
		return prioritaria;
	}

	/**
	 * Setter prioritaria.
	 * @param prioritaria
	 */
	public void setPrioritaria(final int prioritaria) {
		this.prioritaria = prioritaria;
	}

}
