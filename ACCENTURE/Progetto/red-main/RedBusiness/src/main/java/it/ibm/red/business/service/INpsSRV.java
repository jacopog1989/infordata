package it.ibm.red.business.service;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;

import it.ibm.red.business.dto.DetailDocumentoDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaNPSDTO;
import it.ibm.red.business.dto.RegistroDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.PostaEnum;
import it.ibm.red.business.enums.TipoAllaccioEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.NpsConfiguration;
import it.ibm.red.business.service.facade.INpsFacadeSRV;
import npstypes.v1.it.gov.mef.AllTipoRegistroAusiliarioType;
import npstypes.v1.it.gov.mef.DocTipoOperazioneType;
import npstypes.v1.it.gov.mef.ProtDestinatarioType;

/**
 * The Interface INpsSRV.
 *
 * @author CPIERASC
 * 
 *         Oggetto per la gestione della protocollazione tramite NPS.
 */
public interface INpsSRV extends INpsFacadeSRV {

	/**
	 * Creazione protocollo uscita (in fase di firma).
	 * 
	 * @param documentDetail   dettaglio documento
	 * @param acl              acl
	 * @param utenteFirmatario credenziali utente firmatario
	 * @param statoProtocollo  stato del protocollo
	 * @param connection       connessione al db
	 * @return dati protocollo nps
	 */
	ProtocolloNpsDTO creaProtocolloUscita(DetailDocumentoDTO documentDetail, String acl, UtenteDTO utenteFirmatario, String statoProtocollo, Connection connection,
			IFilenetCEHelper fceh);

	/**
	 * Creazione protocollo uscita (da salva documento).
	 * 
	 * @param documentRedDetail
	 * @param indiceFascicolo
	 * @param acl
	 * @param utenteFirmatario
	 * @param statoProtocollo
	 * @param connection
	 * @return
	 */
	ProtocolloNpsDTO creaProtocolloUscita(SalvaDocumentoRedDTO documentRedDetail, String indiceFascicolo, String acl, UtenteDTO utenteFirmatario, String statoProtocollo,
			Connection connection, IFilenetCEHelper fceh);

	/**
	 * Creazione protocollo in ingresso
	 * 
	 * @param docInput
	 * @param acl
	 * @param utente
	 * @param assegnazioniCompetenza
	 * @param assegnazioniConoscenza
	 * @param assegnazioniContributo
	 * @param assegnazioniFirma
	 * @param assegnazioniSigla
	 * @param assegnazioniVisto
	 * @param controlloIter
	 * @param fceh
	 * @param connection
	 * @return
	 */
	ProtocolloNpsDTO creaProtocolloEntrata(SalvaDocumentoRedDTO docInput, String acl, UtenteDTO utente, String[] assegnazioniCompetenza, String[] assegnazioniConoscenza,
			String[] assegnazioniContributo, String[] assegnazioniFirma, String[] assegnazioniSigla, String[] assegnazioniVisto, String[] assegnazioniFirmaMultipla,
			boolean controlloIter, String idDocumentoMessaggioPostaNps, Connection connection);

	/**
	 * Metodo per upload documento per gestione asincrona NPS.
	 * 
	 * @param contentIS      input stream content
	 * @param guid           guid documento
	 * @param guidProtocollo guid protocollo
	 * @param contentType    tipo di content
	 * @param nomefile       nome del file
	 * @param descrizione    descrizione
	 * @param descrizione    numeroAnnoProtocollo
	 * @param connection     connessione al db
	 * @return
	 */
	int uploadDocToAsyncNps(InputStream contentIS, String guid, String guidProtocollo, String contentType, String nomefile, String descrizione, String numeroAnnoProtocollo,
			UtenteDTO utente, String idDocumento, Connection connection);

	/**
	 * Metodo per upload documento per gestione asincrona NPS.
	 * 
	 * @param contentIS		 input stream content
	 * @param guid			 guid documento
	 * @param guidProtocollo guid protocollo
	 * @param contentType	 tipo di content
	 * @param nomefile		 nome del file
	 * @param descrizione	 descrizione 
	 * @param descrizione	 numeroAnnoProtocollo 
	 * @param firmato		 flag documento firmato
	 * @param connection	 connessione al db
	 * @return 
	 */
	int uploadDocToAsyncNps(InputStream contentIS, String guid, String guidProtocollo, String contentType, String nomefile, String descrizione, 
			String numeroAnnoProtocollo, UtenteDTO utente, String idDocumento, boolean firmato, Connection connection);
	
	/**
	 * 
	 * @param idAoo
	 * @param firmato
	 * @param fileName
	 * @param mimetype
	 * @param descrizione
	 * @param content
	 * @param dataDocumento
	 * @param guidDocumento
	 * @return
	 * @throws IOException
	 */
	String uploadDocToNps(Integer idAoo, boolean firmato, String fileName, String mimetype, String descrizione, InputStream content, Date dataDocumento, String guidDocumento)
			throws IOException;

	/**
	 * Metodo per upload allegati del documento per gestione asincrona NPS.
	 * 
	 * @param guidDocumentoPrincipale
	 * @param infoProtocollo
	 * @param idProtocollo
	 * @param utente
	 * @param isCollegato
	 * @param fceh
	 * @param con
	 */
	void uploadAllegatiDocToAsyncNps(String guidDocumentoPrincipale, String infoProtocollo, String idProtocollo, UtenteDTO utente, boolean isCollegato, IFilenetCEHelper fceh,
			Connection con);

	/**
	 * Associazione documento protocollo alla gestione asincrona di NPS.
	 * 
	 * @param idNar
	 * @param guid
	 * @param protocolloGuid
	 * @param nomeFile
	 * @param descrizione
	 * @param utente
	 * @param isPrincipale
	 * @param daInviare
	 * @param idDocumentoOperazione
	 * @param tipoOperazioneEnum
	 * @param numeroAnnoProtocollo
	 * @param connection
	 */
	void associateDocumentoProtocolloToAsyncNps(int idNar, String guid, String protocolloGuid, String nomeFile, String descrizione, UtenteDTO utente, boolean isPrincipale,
			boolean daInviare, String idDocumentoOperazione, DocTipoOperazioneType tipoOperazioneEnum, String numeroAnnoProtocollo, String idDocumento, Connection connection);
	
	/**
	 * Associazione documento protocollo.
	 * 
	 * @param idNar
	 * @param guid
	 * @param protocolloGuid
	 * @param nomeFile
	 * @param descrizione
	 * @param utente
	 * @param isPrincipale
	 * @param daInviare
	 * @param idDocumentoOperazione
	 * @param tipoOperazioneEnum
	 * @param idDocumento
	 * @param connection
	 */
	void associateDocumentoProtocolloToNps(int idNar, String guid, String protocolloGuid, String nomeFile, String descrizione, UtenteDTO utente, boolean isPrincipale,
			boolean daInviare, String idDocumentoOperazione, DocTipoOperazioneType tipoOperazioneEnum, String idDocumento, Connection connection);

	/**
	 * Spedisci protocollo uscita asincrono.
	 * 
	 * @param utente
	 * @param infoProtocollo
	 * @param idProtocollo
	 * @param dataSpedizione
	 * @param con
	 * @return
	 */
	int spedisciProtocolloUscitaAsync(UtenteDTO utente, String infoProtocollo, String idProtocollo, String dataSpedizione, String oggettoMessaggio, String corpoMessaggio,
			String idDocumento, Connection connection);

	/**
	 * Invia allacci in maniera asincrona ad NPS.
	 *
	 * @param utente               dati utente
	 * @param connection           connessione db
	 * @param idProtocollo         identificativo protocollo
	 * @param allacci              collezione allacci
	 * @param numeroAnnoProtocollo numero anno protocollo
	 * @param isRisposta           flag risposta
	 */
	void aggiungiAllacciAsync(UtenteDTO utente, Connection connection, String idProtocollo, Collection<RispostaAllaccioDTO> allacci, String numeroAnnoProtocollo,
			boolean isRisposta, String idDocumento);

	/**
	 * Metodo di utilità che non si interfaccia con NPS
	 * 
	 * @param securityList
	 * @param fceh
	 * @return
	 */
	String getACLFromSecurity(ListSecurityDTO securityList, IFilenetCEHelper fceh);

	/**
	 * 
	 * @param numeroAnnoProtocollo
	 * @param idNodoNew
	 * @param idUtenteNew
	 * @param idProtocollo
	 * @param securityList
	 * @param dataAssegnazione
	 * @param utente
	 * @param perCompetenza
	 * @param con
	 * @param fceh
	 * @return
	 */
	int aggiungiAssegnatarioPerRiassegnazioneAsync(int idNar, String numeroAnnoProtocollo, Long idNodoNew, Long idUtenteNew, String idProtocollo,
			ListSecurityDTO securityList, Date dataAssegnazione, UtenteDTO utente, String perCompetenza, String idDocumento, Connection con,
			IFilenetCEHelper fceh);

	/**
	 * 
	 * @param infoProtocollo
	 * @param idProtocollo
	 * @param oldEmailDestinatario
	 * @param newEmailDestinatario
	 * @param utente
	 * @param con
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	int modificaDestinatarioUscitaAsync(String infoProtocollo, String idProtocollo, String oldEmailDestinatario, String newEmailDestinatario, UtenteDTO utente,
			String idDocumento, Connection con) throws DatatypeConfigurationException;

	/**
	 * Annullamento parziale di protocollo.
	 * 
	 * @param utente
	 * @param infoProtocollo
	 * @param idProtocollo
	 * @param oggetto
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param descrizioneTipologiaDocumento
	 * @param indiceClassificazione
	 * @param idDocumento
	 * @param metadatiEstesi
	 * @param con
	 * @return
	 */
	boolean aggiornaDatiProtocollo(UtenteDTO utente, String infoProtocollo, String idProtocollo, String oggetto, Integer idTipologiaDocumento, Integer idTipoProcedimento,
			String descrizioneTipologiaDocumento, String indiceClassificazione, String idDocumento, Collection<MetadatoDTO> metadatiEstesi, Connection con);

	/**
	 * Annulamento parziale di protocollo (solo per modifiche al titolario)
	 * 
	 * @param utente
	 * @param infoProtocollo
	 * @param idProtocollo
	 * @param indiceClassificazione
	 * @param idDocumento
	 * @param con
	 * @return
	 */
	boolean aggiornaTitolarioProtocollo(UtenteDTO utente, String infoProtocollo, String idProtocollo, String indiceClassificazione, String idDocumento, Connection con);

	/**
	 * @param guidProtocollo
	 * @param utente
	 * @param stato
	 * @param con
	 * @return
	 */
	boolean cambiaStatoProtEntrata(String guidProtocollo, String numeroAnnoProtocollo, UtenteDTO utente, String stato, String idDocumento, Connection con);

	/**
	 * @param guidProtocollo
	 * @param utente
	 * @param stato
	 * @param con
	 * @return
	 */
	int cambiaStatoProtEntrataAsync(String guidProtocollo, String numeroAnnoProtocollo, UtenteDTO utente, String stato, String idDocumento, Connection con);

	/**
	 * Aggiorna gli item in coda request asincrone associati al protocollo di
	 * emergenza in input, con i dati del protocollo ufficiale
	 * 
	 * @param dto
	 * @param codiceAoo
	 * @param connection
	 */
	void aggiornaItemPostEmergenza(ProtocolloEmergenzaDocDTO dto, String codiceAoo, Connection connection);

	/**
	 * Cambia lo stato di un protocollo in uscita.
	 * 
	 * @param idProtocollo
	 * @param infoProtocollo
	 * @param utente
	 * @param stato
	 * @param con
	 * @return
	 */
	int cambiaStatoProtUscita(String idProtocollo, String infoProtocollo, UtenteDTO utente, String stato, String idDocumento, Connection con);

	/**
	 * Aggiorna acl nps.
	 * 
	 * @param utente
	 * @param infoProtocollo
	 * @param idProtocollo
	 * @param acl
	 */
	boolean aggiornaAcl(UtenteDTO utente, String infoProtocollo, String idProtocollo, String acl, String idDocumento);

	/**
	 * Effettua l'aggiornamento delle acl e del flag riservato su NPS.
	 * 
	 * @param utente
	 * @param infoProtocollo
	 * @param idProtocollo
	 * @param securityList
	 * @param isRiservato
	 * @param idDocumento
	 * @return true se l'aggiornamento ha avuto esito positivo.
	 */
	boolean aggiornaRiservatezza(final UtenteDTO utente, final String infoProtocollo, final String idProtocollo, final ListSecurityDTO securityList, final boolean isRiservato, final String idDocumento);
	
	/**
	 * Creazione registrazione ausiliaria (in fase di firma).
	 * 
	 * 
	 * @return dati registrazione ausiliaria
	 */
	RegistrazioneAusiliariaNPSDTO creaRegistrazioneAusiliaria(DetailDocumentoDTO documentDetail, String acl, UtenteDTO utenteFirmatario, RegistroDTO registro,
			AllTipoRegistroAusiliarioType tipoRegistro, String idProtocolloIngresso, Connection connection);

	/**
	 * Collega registrazione ausiliaria al protocollo in uscita
	 *
	 */
	void collegaRegAuxProtUscita(UtenteDTO utente, Connection connection, String idProtocollo, String idRegAux, String numeroAnnoProtocollo, String idDocumento);

	/**
	 * Protocollazione e spedizione uscita flussi AUT
	 * 
	 * @param documentDetail
	 * @param acl
	 * @param tipoAllaccio
	 * @param identificatoreProcesso
	 * @param utenteFirmatario
	 * @param guidDocPrincipale
	 * @param guidAllegati
	 * @param connection
	 * @param fceh
	 * @return
	 */
	ProtocolloNpsDTO inviaAtto(String codiceFlusso, DetailDocumentoDTO documentDetail, String acl, TipoAllaccioEnum tipoAllaccio, String identificatoreProcesso,
			UtenteDTO utenteFirmatario, String guidDocPrincipale, String[] guidAllegati, Connection connection);

	/**
	 * @param protocolloNps
	 * @param perProtocollazioneAutomatica
	 * @param idAoo
	 * @param ricercaAdminNps
	 * @param connection
	 * @return
	 */
	ProtocolloNpsDTO getDettagliProtocollo(ProtocolloNpsDTO protocolloNps, boolean perProtocollazioneAutomatica, Integer idAoo, UtenteDTO utente, boolean aperturaDaFascicolo, Connection connection);

	/**
	 * Invio messaggio posta
	 * 
	 * @param mailMittente
	 * @param testoMessaggio
	 * @param indirizzoMailDest
	 * @param allegatiGuid
	 * @param idAoo
	 * @param connection
	 * @return
	 */
	String inviaMessaggioPosta(Integer idNotifica, String mailMittente, String testoMessaggio, String corpoMessaggio, List<String> indirizzoMailDest,
			List<String> idDocumentiNps, String isUfficiale, String isPec, int idAoo, Boolean notificaSpedizione, Connection connection);

	/**
	 * Ottiene tutte le configurazioni Nps.
	 * 
	 * @return lista delle configurazioni Nps
	 */
	@Override
	List<NpsConfiguration> getAll();
	
	/**
	 * Cambio lo stato del prot in uscita a spedito 
	 * @param idProtocollo 
	 * @param infoProtocollo 
	 * @param utente
	 * @param stato
	 * @param idDocumento 
	 * @return 
	 */  
	int cambiaStatoProtUscitaToSpedito(Aoo aoo, MessaggioEmailDTO msgNext);
	
	/**
	 * Crea il protocollo in uscita coerente con i dati di input.
	 * 
	 * @param destinatariArray
	 * @param acl
	 * @param idUfficioCreatore
	 * @param idUtenteCreatore
	 * @param utenteFirmatario
	 * @param oggetto
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param tipologiaDocumentoDesc
	 * @param indiceFascicolo
	 * @param descrIndiceFascicolo
	 * @param statoProtocollo
	 * @param descRegistroProtocollo
	 * @param metadatiEstesi
	 * @param isRiservato
	 * @param connection
	 * @return
	 */
	ProtocolloNpsDTO creaProtocolloUscita(ProtDestinatarioType[] destinatariArray, String acl, Long idUfficioCreatore, Long idUtenteCreatore,
			UtenteDTO utenteFirmatario, String oggetto, Integer idTipologiaDocumento, final Integer idTipoProcedimento, final String tipologiaDocumentoDesc, 
			String indiceFascicolo, String descrIndiceFascicolo, String statoProtocollo, String descRegistroProtocollo, Collection<MetadatoDTO> metadatiEstesi, 
			boolean isRiservato, Connection connection);
	
	/**
	 * Restituisce il destinatario NPS creato a partire dai parametri.
	 * 
	 * @param mezzoSpedizione
	 *            Mezzo di spedizione destinatario.
	 * @param tipoPersona
	 *            Tipo persona destinatario.
	 * @param nome
	 *            Nome destinatario.
	 * @param cognome
	 *            Cognome destinatario.
	 * @param codiceFiscalePIva
	 *            Codice fiscale o partita IVA del destinatario.
	 * @param aliasContatto
	 *            Alias del destinatario.
	 * @param indirizzoEmail
	 *            Indirizzo e-mail.
	 * @param isPEC
	 *            Definisce se la mail è PEC o no.
	 * @param idContatto
	 *            Identificativo del destinatario.
	 * @param tipoPosta
	 *            Tipologia della posta.
	 * @param codiceAoo
	 *            Codice dell'area organizzativa.
	 * @param denominazioneAoo
	 *            Denominazione dell'area organizzativa.
	 * @param indirizzoCasellaMittente
	 *            Indirizzo casella mittente.
	 * @param perCompetenza
	 *            Definisce il tipo assegnazione del destinatario.
	 * @return Destinatario NPS.
	 */
	ProtDestinatarioType getDestinatarioNPS(MezzoSpedizioneEnum mezzoSpedizione, String tipoPersona, String nome,  String cognome,
			String codiceFiscalePIva, String aliasContatto, String indirizzoEmail, boolean isPEC, Long idContatto, PostaEnum tipoPosta,
			String codiceAoo, String denominazioneAoo, String indirizzoCasellaMittente, boolean perCompetenza);
}