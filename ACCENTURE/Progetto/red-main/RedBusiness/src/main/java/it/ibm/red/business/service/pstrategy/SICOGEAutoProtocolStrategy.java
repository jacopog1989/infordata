package it.ibm.red.business.service.pstrategy;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.CapitoloSpesaDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MessaggioPostaNpsFlussoDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.RicercaCapitoloSpesaDTO;
import it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO;
import it.ibm.red.business.dto.TipologiaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.SICOGEMessageIDEnum;
import it.ibm.red.business.enums.SalvaDocumentoErroreEnum;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.enums.TipoProtocolloEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.builder.Builder;
import it.ibm.red.business.nps.dto.AllaccioDTO;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.facade.ICapitoloSpesaFacadeSRV;
import it.ibm.red.webservice.model.interfaccianps.npstypesestesi.MetaDatoAssociato;

/**
 * Strategy di protocollazione automatica del flusso SICOGE.
 */
public class SICOGEAutoProtocolStrategy extends AbstractAutoProtocolStrategy<MessaggioPostaNpsFlussoDTO> {

	/**
	 * Processo SICOGE - Label.
	 */
	private static final String PROCESSO_SICOGE = "[Processo SICOGE: ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SICOGEAutoProtocolStrategy.class.getName());

	/**
	 * Servizio.
	 */
	private final IUtenteSRV utenteSRV = ApplicationContextProvider.getApplicationContext().getBean(IUtenteSRV.class);

	/**
	 * Servizio.
	 */
	private final IAooSRV aooSRV = ApplicationContextProvider.getApplicationContext().getBean(IAooSRV.class);

	/**
	 * Servizio.
	 */
	private final IFascicoloSRV fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloSRV.class);

	/**
	 * Servizio.
	 */
	private final ICapitoloSpesaFacadeSRV capitoloSpesaSRV = ApplicationContextProvider.getApplicationContext().getBean(ICapitoloSpesaFacadeSRV.class);

	/**
	 * @see it.ibm.red.business.service.pstrategy.IAutoProtocolStrategy#protocol(it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO,
	 *      java.lang.String).
	 */
	@Override
	public EsitoSalvaDocumentoDTO protocol(final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessagio, final String guidMail) {
		LOGGER.info("Avvio protocollazione automatica flusso SICOGE.");
		EsitoSalvaDocumentoDTO out = null;
		final Integer idMessaggio = richiestaElabMessagio.getMessaggio().getDatiFlusso().getIdentificativo();
		final SICOGEMessageIDEnum msg = SICOGEMessageIDEnum.getEnumById(idMessaggio);

		if (SICOGEMessageIDEnum.SICOGE_PROTOCOLLA_TITOLO.equals(msg)) {
			LOGGER.info("Richiesta protocollazione titolo.");
			out = creaNuovoProcessoSICOGE(richiestaElabMessagio);

		} else if (SICOGEMessageIDEnum.SICOGE_DOCUMENTAZIONE_AGGIUNTIVA.equals(msg)) {
			LOGGER.info("Richiesta integrazione documentazione.");
			out = processaDocumentazioneAggiuntiva(richiestaElabMessagio);

		}

		LOGGER.info("Fine protocollazione automatica flusso SICOGE.");
		return out;
	}

	/**
	 * Creazione di un nuovo documento SICOGE (1001 o 1002 senza protocollo
	 * riferimento presente su RED).
	 * 
	 * @param richiestaElabMessagio
	 * @return
	 */
	private EsitoSalvaDocumentoDTO creaNuovoProcessoSICOGE(final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessagio) {
		EsitoSalvaDocumentoDTO out = new EsitoSalvaDocumentoDTO();
		IFilenetCEHelper fceh = null;

		try {
			final AooFilenet aooFilenet = aooSRV.recuperaAoo(richiestaElabMessagio.getIdAoo().intValue()).getAooFilenet();
			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), richiestaElabMessagio.getIdAoo());

			out = creaNuovoProcessoSICOGE(richiestaElabMessagio, fceh);
		} catch (final Exception e) {
			LOGGER.error("creaNuovoProcessoSICOGE -> " + e.getMessage(), e);
			out.getErrori().add(SalvaDocumentoErroreEnum.ERRORE_GENERICO);
			out.setNote("Errore flusso SICOGE: " + e.getMessage());
		} finally {
			if (fceh != null) {
				fceh.popSubject();
			}
		}

		return out;
	}

	/**
	 * Creazione di un nuovo documento SICOGE (1001 o 1002 senza protocollo
	 * riferimento presente su RED).
	 * 
	 * @param richiestaElabMessagio
	 * @param fceh
	 * @return
	 */
	private EsitoSalvaDocumentoDTO creaNuovoProcessoSICOGE(final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessagio,
			final IFilenetCEHelper fceh) {
		FascicoloDTO fascicoloSelezionato = null;

		// Gestione fascicolo FEPA
		final String idFascicoloFepa = richiestaElabMessagio.getMessaggio().getDatiFlusso().getIdFascicoloFepa();

		if (StringUtils.isNotBlank(idFascicoloFepa)) {
			// Si verifica se esiste un fascicolo RED per il fascicolo FEPA in input:
			// in caso positivo, il documento deve essere inserito in quel fascicolo
			final Document fascicoloFilenet = fceh.getFascicoloByIdFascicoloFepa(idFascicoloFepa);

			if (fascicoloFilenet != null) {
				fascicoloSelezionato = TrasformCE.transform(fascicoloFilenet, TrasformerCEEnum.FROM_DOCUMENTO_TO_FASCICOLO);
			}
		}

		// Creazione automatica capitolo di spesa se non esiste
		final boolean capitoloRiservato = gestioneMetadatoCapitolo(richiestaElabMessagio);
		
		return creaNuovoProcesso(richiestaElabMessagio, null, TipoContestoProceduraleEnum.SICOGE, fascicoloSelezionato, capitoloRiservato);
	}
	
	private boolean gestioneMetadatoCapitolo(final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessagio) {
		final Long idAoo = richiestaElabMessagio.getIdAoo();
		boolean capitoloRiservato = false;
		if (richiestaElabMessagio.getMessaggio().getDatiFlusso() != null && richiestaElabMessagio.getMessaggio().getDatiFlusso().getMetadatiEstesi() != null) {

			final List<MetaDatoAssociato> metadatiForniti = richiestaElabMessagio.getMessaggio().getDatiFlusso().getMetadatiEstesi().getMetadatoAssociato();
			final TipologiaDTO tipologia = getTipologia(idAoo, richiestaElabMessagio.getMessaggio().getDatiFlusso().getMetadatiEstesi().getNOME());
			final List<MetadatoDTO> metadatiConfigurati = caricaMetadatiEstesiPerGUI(idAoo, tipologia);
			
			for (final MetaDatoAssociato metadatiFornito : metadatiForniti) {
				for (final MetadatoDTO metadatiDichiarato : metadatiConfigurati) {
					if (metadatiFornito.getCodice().equalsIgnoreCase(metadatiDichiarato.getName())) {
						if (TipoMetadatoEnum.CAPITOLI_SELECTOR.equals(metadatiDichiarato.getType())) {
							capitoloRiservato = gestisciCapitolo(metadatiFornito.getValore(), idAoo);
						}
						break;
					}
				}

			}

		}
		
		return capitoloRiservato;
	}

	/**
	 * Crea il capitolo in caso non sia presente in base dati. 
	 * Restituisce l'informazione sulla risewrvatezza del capitolo, se già presente in base dati.
	 * 
	 * @param codiceCapitolo
	 * @param idAoo
	 * @return true se il capitolo risulta riservato per l'AOO, false altrimenti
	 */
	private boolean gestisciCapitolo(final String codiceCapitolo, final Long idAoo) {
		final RicercaCapitoloSpesaDTO ricercaDTO = new RicercaCapitoloSpesaDTO(idAoo.longValue());
		ricercaDTO.setCodice(codiceCapitolo);
		ricercaDTO.setDescrizione("");

		final Collection<CapitoloSpesaDTO> capitoli = capitoloSpesaSRV.ricerca(ricercaDTO);
		boolean riservato = false;
		if (capitoli == null || capitoli.isEmpty()) {
			// Inserisci automaticamente il capitolo se non c'è
			final int anno = Calendar.getInstance().get(Calendar.YEAR);
			final CapitoloSpesaDTO capitoloSpesa = new CapitoloSpesaDTO(codiceCapitolo, "Inserito automaticamente tramite flusso SICOGE", anno, false);
			capitoloSpesaSRV.inserisci(capitoloSpesa, idAoo);
		} else {
			riservato = capitoli.iterator().next().isFlagRiservato();
		}
		
		return riservato;
	}

	/**
	 * La documentazione aggiuntiva può essere spontanea o meno, in quest'ultimo
	 * caso il documento va rimesso in lavorazione.
	 * 
	 * @param richiestaElabMessagio
	 * @return
	 */
	private EsitoSalvaDocumentoDTO processaDocumentazioneAggiuntiva(final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessagio) {
		EsitoSalvaDocumentoDTO out = new EsitoSalvaDocumentoDTO();

		final Integer idAoo = richiestaElabMessagio.getIdAoo().intValue();
		final Aoo aoo = aooSRV.recuperaAoo(idAoo);
		final AooFilenet aooFilenet = aoo.getAooFilenet();

		final UtenteDTO utente = utenteSRV
				.getByUsername(PropertiesProvider.getIstance().getParameterByString(aoo.getCodiceAoo() + "." + PropertiesNameEnum.AOO_SISTEMA_AMMINISTRATORE.getKey()));
		final String idProcesso = richiestaElabMessagio.getMessaggio().getDatiFlusso().getIdentificatoreProcesso();

		String documentTitleProtocolloRiferimento = getDocumentTitle(aooFilenet, TipoContestoProceduraleEnum.SICOGE, idProcesso);

		if (StringUtils.isNotBlank(documentTitleProtocolloRiferimento)) {
			LOGGER.info(PROCESSO_SICOGE + idProcesso + "] Protocollo riferimento trovato su FileNet con Document Title: " + documentTitleProtocolloRiferimento);
			out = gestisciIntegrazioneDati(richiestaElabMessagio, documentTitleProtocolloRiferimento, aooFilenet);

		} else { // Il protocollo riferimento potrebbe essere un protocollo migrato senza ID
					// processo
			LOGGER.info(PROCESSO_SICOGE + idProcesso + "] Protocollo riferimento NON trovato su FileNet."
					+ " Potrebbe trattarsi di un protocollo migrato, si tenta di recuperarlo dagli allacci NPS del protocollo.");
			IFilenetCEHelper fceh = null;
			final PropertiesProvider pp = PropertiesProvider.getIstance();

			try {
				fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
						aooFilenet.getObjectStore(), utente.getIdAoo());

				final ProtocolloNpsDTO protocollo = getDettagliProtocollo(utente.getIdAoo().intValue(), richiestaElabMessagio.getMessaggio().getProtocollo());
				// Recupero il protocollo riferimento (1001) dagli allacci del 1002
				if (!CollectionUtils.isEmpty(protocollo.getAllacciList())) {
					LOGGER.info(PROCESSO_SICOGE + idProcesso + "] Protocollo riferimento recuperato dagli allacci NPS del protocollo");
					final AllaccioDTO protocolloRiferimento = protocollo.getAllacciList().get(0);

					final Document protocolloRiferimentoFilenet = fceh.verificaProtocollo(Arrays.asList(aoo.getCodiceAoo() + "_DocumentoGenerico"),
							Integer.parseInt(protocolloRiferimento.getAnnoProtocollo()), protocolloRiferimento.getNumeroProtocollo(), protocolloRiferimento.getIdProtocollo(),
							TipoProtocolloEnum.ENTRATA.getId());

					// Se il protocollo riferimento è presente senza ID processo, lo si aggiorna in
					// modo da renderlo 1001
					if (protocolloRiferimentoFilenet != null) {

						documentTitleProtocolloRiferimento = (String) TrasformerCE.getMetadato(protocolloRiferimentoFilenet,
								pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));

						final Map<String, Object> metadatiDaAggiornare = new HashMap<>();

						// Aggiornamento del protocollo riferimento con l'ID processo (per SICOGE è l'ID
						// del fascicolo FEPA)
						metadatiDaAggiornare.put(pp.getParameterByKey(PropertiesNameEnum.IDENTIFICATORE_PROCESSO_METAKEY), idProcesso);

						fceh.updateMetadati(protocolloRiferimentoFilenet, metadatiDaAggiornare);

						// Aggiornamento del fascicolo procedimentale con l'inserimento di
						// identificativo e tipologia del fascicolo FEPA
						final FascicoloDTO fascicoloProcedimentale = fascicoloSRV.getFascicoloProcedimentale(documentTitleProtocolloRiferimento, idAoo, utente, fceh);
						if (fascicoloProcedimentale != null) {
							metadatiDaAggiornare.clear();
							metadatiDaAggiornare.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA),
									richiestaElabMessagio.getMessaggio().getDatiFlusso().getIdFascicoloFepa()); // ID fascicolo FEPA
							metadatiDaAggiornare.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_TIPO_FASCICOLO_FEPA_METAKEY),
									richiestaElabMessagio.getMessaggio().getDatiFlusso().getTipoFascicoloFepa()); // Tipo fascicolo FEPA

							fceh.updateFascicoloMetadati(fascicoloProcedimentale.getIdFascicolo(), aoo.getIdAoo(),
									pp.getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY), metadatiDaAggiornare);
						}

						// Gestione dell'integrazione dati
						out = gestisciIntegrazioneDati(richiestaElabMessagio, documentTitleProtocolloRiferimento, aooFilenet);

					} else {

						// Altrimenti, se il protocollo riferimento non è presente, il 1002 viene
						// comunque ingressato come un documento SICOGE a sé stante
						out = creaNuovoProcessoSICOGE(richiestaElabMessagio, fceh);

					}

				} else {
					throw new RedException(PROCESSO_SICOGE + idProcesso + "] Nessun allaccio trovato su NPS per il protocollo: "
							+ richiestaElabMessagio.getMessaggio().getProtocollo().getIdProtocollo() + ". Impossibile recuperare il protocollo riferimento.");
				}
			} catch (final Exception e) {
				LOGGER.error("processaDocumentazioneAggiuntiva -> " + e.getMessage(), e);
				out.getErrori().add(SalvaDocumentoErroreEnum.ERRORE_GENERICO);
				out.setNote("Errore flusso SICOGE: " + e.getMessage());
			} finally {
				if (fceh != null) {
					fceh.popSubject();
				}
			}

		}

		return out;
	}

	/**
	 * @param richiestaElabMessagio
	 * @param documentTitleProtocolloRiferimento
	 * @param aooFilenet
	 * @return
	 */
	private EsitoSalvaDocumentoDTO gestisciIntegrazioneDati(final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessagio,
			final String documentTitleProtocolloRiferimento, final AooFilenet aooFilenet) {
		EsitoSalvaDocumentoDTO out = null;
		
		final boolean capitoloRiservato = gestioneMetadatoCapitolo(richiestaElabMessagio);
		
		final String queueName = getWorkflowPrincipale(documentTitleProtocolloRiferimento, aooFilenet);

		if (queueName != null) {
			if (DocumentQueueEnum.SOSPESO.getName().equals(queueName)) {
				LOGGER.info("Integrazione documentazione su richiesta.");
				out = riconciliaIntegrazioneDati(richiestaElabMessagio, null, TipoContestoProceduraleEnum.SICOGE, capitoloRiservato);
			} else {
				LOGGER.info("Integrazione spontanea documentazione.");
				// Il protocollo di riferimento non è chiuso
				out = integraDatiSpontanei(richiestaElabMessagio, null, TipoContestoProceduraleEnum.SICOGE, Builder.CHIUSO, false, capitoloRiservato);
			}
		} else {
			LOGGER.info("Integrazione spontanea documentazione workflow non attivo.");
			// Il protocollo di riferimento è chiuso
			out = integraDatiSpontanei(richiestaElabMessagio, null, TipoContestoProceduraleEnum.SICOGE, Builder.CHIUSO, true, capitoloRiservato);
		}

		return out;
	}

}
