/**
 * 
 */
package it.ibm.red.business.dto;

import it.ibm.red.business.enums.TipoDocumentoModeEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;

/**
 * DTO che definisce il capitolo spesa come metadato.
 */
public class CapitoloSpesaMetadatoDTO extends MetadatoDTO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -3929331553242567122L;

	/**
	 * Capitolo selezionato.
	 */
	private String capitoloSelected;

	/**
	 * Costruttore di default del DTO.
	 */
	public CapitoloSpesaMetadatoDTO() {
		super();
	}

	/**
	 * Costruttore di classe.
	 * 
	 * @param name
	 *            nome del capitolo spesa
	 * @param displayName
	 *            display name del capitolo spesa
	 * @param visibility
	 *            visibilità del capitolo spesa
	 * @param editability
	 *            editabilità del capitolo spesa
	 * @param obligatoriness
	 *            obbligatorietà del capitolo spesa
	 * @param flagOut
	 *            flag out del capitolo spesa
	 */
	public CapitoloSpesaMetadatoDTO(final String name, final String displayName, final TipoDocumentoModeEnum visibility, final TipoDocumentoModeEnum editability, 
			final TipoDocumentoModeEnum obligatoriness, final Boolean flagOut) {
		super(null, name, TipoMetadatoEnum.CAPITOLI_SELECTOR, displayName, null, null, null, visibility, editability, obligatoriness, 
				false, flagOut, null, false);
	}

	/**
	 * Costruttore di classe.
	 * 
	 * @param name
	 *            nome capitolo spesa
	 * @param displayName
	 *            display name del capitolo spesa
	 * @param capitoloSelected
	 *            capitolo spesa selezionato
	 * @param visibility
	 *            visibilità del capitolo spesa
	 * @param editability
	 *            editabilità del capitolo spesa
	 * @param obligatoriness
	 *            obbligatorietà del capitolo spesa
	 */
	public CapitoloSpesaMetadatoDTO(final String name, final String displayName, final String capitoloSelected, final TipoDocumentoModeEnum visibility,	
			final TipoDocumentoModeEnum editability, final TipoDocumentoModeEnum obligatoriness) {
		super(null, name, TipoMetadatoEnum.CAPITOLI_SELECTOR, displayName, null, null, null, visibility, editability, obligatoriness, 
				false, null, null, false);
		setCapitoloSelected(capitoloSelected);
	}
	
	/**
	 * Restituisce il capitolo spesa selezionato.
	 * 
	 * @return capitolo selezionato
	 */
	public String getCapitoloSelected() {
		return capitoloSelected;
	}

	/**
	 * Imposta il capitolo spesa selezionato tra i valori esistenti.
	 * 
	 * @param capitoloSelected
	 *            capitolo selezionato
	 */
	public void setCapitoloSelected(final String capitoloSelected) {
		this.capitoloSelected = capitoloSelected;
	}

	/**
	 * Restituisce il capitolo selezionato che ne rappresenta il valore selezionato
	 * sul metadato.
	 * 
	 * @see it.ibm.red.business.dto.MetadatoDTO#getValue4AttrExt()
	 */
	@Override
	public String getValue4AttrExt() {
		return getCapitoloSelected();
	}
}
