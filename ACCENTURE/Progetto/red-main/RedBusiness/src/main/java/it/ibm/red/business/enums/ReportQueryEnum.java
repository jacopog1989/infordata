package it.ibm.red.business.enums;

/**
 * Enum che definisce tutte le query esistenti per la costruzione dei report.
 */
public enum ReportQueryEnum {

	/**
	 * Valore.
	 */
	SQL_DETTAGLIO_REPORT_GET_ALL("report.dettaglio_report_get_all"),

	/**
	 * Valore.
	 */
	SQL_TIPO_OPERAZIONE_REPORT_GET_ALL_BY_ID_DETTAGLIO_AND_ID_AOO("report.tipo_operazione_report_get_all_by_id_dettaglio_and_id_aoo"),

	/**
	 * Valore.
	 */
	SQL_CODA_DI_LAVORO_REPORT_GET_ALL_BY_ID_DETTAGLIO("report.coda_di_lavoro_report_get_all_by_id_dettaglio"),
	

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_ENTRATA_UFFICIO_GROUPBY("report.getdocentratabyufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_ENTRATA_UFFICIOUTENTE_GROUPBY("report.getdocentratabyufficioutente"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_ENTRATA_UFFICIO("report.d_getdocentratabyufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_ENTRATA_UFFICIOUTENTE("report.d_getdocentratabyufficioutente"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_USCITA_UFFICIO_GROUPBY("report.getdocuscitabyufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_USCITA_UFFICIOUTENTE_GROUPBY("report.getdocuscitabyufficioutente"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_USCITA_UFFICIO("report.d_getdocuscitabyufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_USCITA_UFFICIOUTENTE("report.d_getdocuscitabyufficioutente"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_FIRMATI_UFFICIO_GROUPBY("report.getdocfirmatibyufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_FIRMATI_UFFICIOUTENTE_GROUPBY("report.getdocfirmatibyufficioutente"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_FIRMATI_UFFICIO("report.d_getdocfirmatibyufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_FIRMATI_UFFICIOUTENTE("report.d_getdocfirmatibyufficioutente"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_SIGLATI_UFFICIO_GROUPBY("report.getdocsiglatibyufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_SIGLATI_UFFICIOUTENTE_GROUPBY("report.getdocsiglatibyufficioutente"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_SIGLATI_UFFICIO("report.d_getdocsiglatibyufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_SIGLATI_UFFICIOUTENTE("report.d_getdocsiglatibyufficioutente"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_VISTATI_UFFICIO_GROUPBY("report.getdocvistatibyufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_VISTATI_UFFICIOUTENTE_GROUPBY("report.getdocvistatibyufficioutente"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_VISTATI_UFFICIO("report.d_getdocvistatibyufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_VISTATI_UFFICIOUTENTE("report.d_getdocvistatibyufficioutente"),
	

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_MESSI_AGLI_ATTI_UFFICIO_GROUPBY("report.getdocmessiagliattibyufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_MESSI_AGLI_ATTI_UFFICIOUTENTE_GROUPBY("report.getdocmessiagliattibyufficioutente"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_MESSI_AGLI_ATTI_UFFICIO("report.d_getdocmessiagliattibyufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_MESSI_AGLI_ATTI_UFFICIOUTENTE("report.d_getdocmessiagliattibyufficioutente"),
	

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_CONTRIBUTO_INS_UFFICIO_GROUPBY("report.getdoccontributoinsbyufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_CONTRIBUTO_INS_UFFICIOUTENTE_GROUPBY("report.getdoccontributoinsbyufficioutente"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_CONTRIBUTO_INS_UFFICIO("report.d_getdoccontributoinsbyufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DOC_CONTRIBUTO_INS_UFFICIOUTENTE("report.d_getdoccontributoinsbyufficioutente"),

	/**
	 * Valore.
	 */
	SQL_REPORT_CODA_DA_LAVORARE_MASTER("report.getcodadalavorarebyufficioutente"),

	/**
	 * Valore.
	 */
	SQL_REPORT_CODA_DA_LAVORARE_DETAIL("report.d_getcodadalavorarebyufficioutente"),


	/**
	 * Valore.
	 */
	SQL_REPORT_CODA_LIBRO_FIRMA_MASTER("report.getcodalibrofirmabyufficioutente"),

	/**
	 * Valore.
	 */
	SQL_REPORT_CODA_LIBRO_FIRMA_DETAIL("report.d_getcodalibrofirmabyufficioutente"),
	

	/**
	 * Valore.
	 */
	SQL_REPORT_CODA_IN_SOSPESO_MASTER("report.getcodainsospesobyufficioutente"),

	/**
	 * Valore.
	 */
	SQL_REPORT_CODA_IN_SOSPESO_DETAIL("report.d_getcodainsospesobyufficioutente"),
	

	/**
	 * Valore.
	 */
	SQL_REPORT_CODA_CORRIERE_UFFICIO_GROUPBY("report.getcodacorrierebyufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_CODA_CORRIERE_UFFICIO("report.d_getcodacorrierebyufficio"),
	

	/**
	 * Valore.
	 */
	SQL_REPORT_CODA_SPEDIZIONE_UFFICIO_GROUPBY("report.getcodaspedizionebyufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_CODA_SPEDIZIONE_UFFICIO("report.d_getcodaspedizionebyufficio"),


	/**
	 * Valore.
	 */
	SQL_REPORT_MASTER_UTENTI_ATTIVI_CON_UN_DATO_RUOLO("report.master_utenti_attivi_con_un_dato_ruolo"),

	/**
	 * Valore.
	 */
	SQL_REPORT_DETAIL_UTENTI_ATTIVI_CON_UN_DATO_RUOLO("report.detail_utenti_attivi_con_un_dato_ruolo"),
	

	/**
	 * Valore.
	 */
	SQL_REPORT_CODA_CHIUSI_UFFICIO_GROUPBY("report.getdoccodachiusibyufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_CODA_CHIUSI_UFFICIO_IDDOC("report.d_getiddoccodachiusibyufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_CODA_CHIUSI_UFFICIO("report.d_getdoccodachiusibyufficio"),
	

	/**
	 * Valore.
	 */
	SQL_REPORT_CODA_ATTI_UFFICIOUTENTE_GROUPBY("report.getdoccodaattibyufficioutente"),

	/**
	 * Valore.
	 */
	SQL_REPORT_CODA_ATTI_UFFICIOUTENTE_IDDOC("report.d_getiddoccodaattibyufficioutente"),

	/**
	 * Valore.
	 */
	SQL_REPORT_CODA_ATTI_UFFICIOUTENTE("report.d_getdoccodaattibyufficioutente"),
	

	/**
	 * Valore.
	 */
	SQL_REPORT_STATISTICHE_UFFICI_INGRESSO_APERTI("report.statistiche_uffici.ingresso_aperti"),

	/**
	 * Valore.
	 */
	SQL_REPORT_STATISTICHE_UFFICI_INGRESSO_CHIUSI("report.statistiche_uffici.ingresso_chiusi"),

	/**
	 * Valore.
	 */
	SQL_REPORT_STATISTICHE_UFFICI_USCITA_APERTI("report.statistiche_uffici.uscita_aperti"),

	/**
	 * Valore.
	 */
	SQL_REPORT_STATISTICHE_UFFICI_USCITA_CHIUSI("report.statistiche_uffici.uscita_chiusi"),


	/**
	 * Valore.
	 */
	SQL_REPORT_COUNT_PROT_ENTRATA_GENERATI_UFFICIO("report.getdocentratacreatibyufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_PROT_ENTRATA_GENERATI_UFFICIO("report.d_getdocentratacreatibyufficio"),
	
	/**
	 * Valore.
	 */
	SQL_REPORT_PROT_PERVENUTI_COMPETENZA("report.protocolli_pervenuti_competenza"),
	
	/**
	 * Valore.
	 */
	SQL_REPORT_PROT_INEVASI_COMPETENZA("report.protocolli_inevasi_competenza"),
	
	/**
	 * Valore.
	 */
	SQL_REPORT_PROT_LAVORATI_COMPETENZA("report.protocolli_lavorati_competenza"),
	
	/**
	 * Valore.
	 */
	SQL_REPORT_ELENCO_DIVISIONALE_UFFICIO("report.elenco_divisionale_ufficio"),

	/**
	 * Valore.
	 */
	SQL_REPORT_ELENCO_DIVISIONALE_UFFICIOUTENTE("report.elenco_divisionale_ufficioutente"),
	
	/**
	 * Valore.
	 */
	SQL_REPORT_ELENCO_NOTIFICHE_CONTI_CONSUNTIVI("report.elenco_notifiche_conti_consuntivi");

	/**
	 * Chiave.
	 */
	private String key;
	
	ReportQueryEnum(final String inKey) {
		key = inKey;
	}
	
	/**
	 * Restituisce la chiave dell'enum.
	 * @return key
	 */
	public String getKey() {
		return key;
	}
}