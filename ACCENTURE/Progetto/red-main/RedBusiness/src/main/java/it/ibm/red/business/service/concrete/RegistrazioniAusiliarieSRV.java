package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IRegistrazioniAusiliarieDAO;
import it.ibm.red.business.dao.IRegistroAusiliarioDAO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.metadatiestesi.MetadatiEstesiHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IRegistrazioniAusiliarieSRV;
import it.ibm.red.business.utils.CollectionUtils;

/**
 * Service che gestisce le funzionalità relative alle registrazioni ausiliarie.
 * 
 * @author SimoneLungarella
 */
@Service
public class RegistrazioniAusiliarieSRV extends AbstractService implements IRegistrazioniAusiliarieSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -760431698546716735L;
	
	/**
	 * Logger per la gestione degli errori in console.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RegistrazioniAusiliarieSRV.class.getName());
	
	/**
	 * DAO per il recupero delle informazioni sulla registrazione ausiliaria dallo strato di persistenza.
	 */
	@Autowired
	private IRegistrazioniAusiliarieDAO templateRegistrazioneDAO; 
	
	/**
	 * DAO per il recupero delle informazioni sul registro ausiliario dallo strato di persistenza.
	 */
	@Autowired
	private IRegistroAusiliarioDAO registroAuxDAO;

	/**
	 * Restituisce la registrazione ausiliaria associata al documento identificato
	 * dal <code> documentTitle </code> se esiste, restituisce <code> null </code>
	 * altrimenti.
	 * 
	 * @see it.ibm.red.business.service.IRegistrazioniAusiliarieSRV#getRegistrazioneAusiliaria(java.lang.String,
	 *      int, java.sql.Connection)
	 */
	@Override
	public RegistrazioneAusiliariaDTO getRegistrazioneAusiliaria(final String documentTitle, final int idAoo, final Connection connection) {
		RegistrazioneAusiliariaDTO documentoMetadati = null;
		
		try {
			
			documentoMetadati = templateRegistrazioneDAO.getRegistrazioneAusiliaria(connection, documentTitle, idAoo);
			if (documentoMetadati != null) {
				final List<MetadatoDTO> metadatiRegistro = new ArrayList<>(registroAuxDAO.getMetadatiRegistro(connection, documentoMetadati.getRegistroAusiliario().getId()));
				
				if (!CollectionUtils.isEmptyOrNull(metadatiRegistro) && documentoMetadati.getClobMetadatiEstesi() != null) { 
					documentoMetadati.setMetadatiRegistroAusiliario(
							MetadatiEstesiHelper.deserializeMetadatiEstesi(documentoMetadati.getClobMetadatiEstesi().toString(), metadatiRegistro));
			
				}
			}
			
		} catch (final Exception sqlex) {
			LOGGER.error("Errore durante il recupero dei metadati del registro ausiliario associati al documento: " + documentTitle, sqlex);
			throw new RedException(sqlex);
		} 
		
		return documentoMetadati;
	}
	
	/**
	 * Rende persistenti sulla base dati le informazioni sulla registrazione
	 * ausiliaria associata al documento identificato dal
	 * <code> documentTitle </code>.
	 * 
	 * @see it.ibm.red.business.service.IRegistrazioniAusiliarieSRV#saveRegistrazioneAusiliaria(java.lang.String,
	 *      int, int, java.util.Collection, java.sql.Connection)
	 */
	@Override
	public void saveRegistrazioneAusiliaria(final String documentTitle, final int idAoo, final int idRegistro, final Collection<MetadatoDTO> metadati, final Connection con) {
		try {
			final String xml = MetadatiEstesiHelper.serializeMetadatiEstesi(metadati);
			templateRegistrazioneDAO.saveRegistrazioneAusiliaria(con, documentTitle, idRegistro, xml, idAoo);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il salvataggio dei metadati del registro ausiliario associati al documento: " + documentTitle, e);
			throw new RedException(e);
		}
	}

	/**
	 * Elimina dalla base dati le informazioni sulla registrazione ausiliaria <em>
	 * eventualmente </em> associata al documento identificato dal
	 * <code> documentTitle </code>.
	 * 
	 * @see it.ibm.red.business.service.IRegistrazioniAusiliarieSRV#deleteRegistrazioneAusiliaria(java.lang.String,
	 *      int, java.sql.Connection)
	 */
	@Override
	public void deleteRegistrazioneAusiliaria(final String documentTitle, final int idAoo, final Connection con) {
		try {
			
			templateRegistrazioneDAO.deleteRegistrazioneAusiliaria(documentTitle, idAoo, con);
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'eliminazione dei metadati del registro ausiliario associati al documento: " + documentTitle, e);
			throw new RedException(e);
		}
	}

	/**
	 * Consente un update completo delle informazioni della registrazione ausiliaria
	 * <em> eventualmente </em> associata al documento identificato dal
	 * <code> documentTitle </code>.
	 * 
	 * @see it.ibm.red.business.service.IRegistrazioniAusiliarieSRV#updateDatiRegistrazioneAusiliaria(java.lang.String,
	 *      int, java.util.Collection, boolean, java.sql.Connection)
	 */
	@Override
	public void updateDatiRegistrazioneAusiliaria(final String documentTitle, final int idAoo, final Collection<MetadatoDTO> metadati,
			final boolean documentoDefinitivo, final Connection con) {
		try {
			final String xml = MetadatiEstesiHelper.serializeMetadatiEstesi(metadati);
			templateRegistrazioneDAO.updateDatiRegistrazioneAusiliaria(con, documentTitle, idAoo, xml, documentoDefinitivo);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il salvataggio dei metadati del registro ausiliario associati al documento: " + documentTitle, e);
			throw new RedException(e);
		}
		
	}

	/**
	 * Consente un update parziale delle informazioni della registrazione ausiliaria
	 * <em> eventualmente </em> associata al documento identificato dal
	 * <code> documentTitle </code>.
	 * 
	 * @see it.ibm.red.business.service.IRegistrazioniAusiliarieSRV#updateParzialeRegistrazioneAusiliaria(java.sql.Connection,
	 *      java.lang.String, java.lang.Long, java.lang.Integer, java.lang.String,
	 *      java.util.Date)
	 */
	@Override
	public void updateParzialeRegistrazioneAusiliaria(final Connection connection, final String documentTitle, final Long idAoo, final Integer numeroRegistrazione, final String idRegistrazione, final Date dataRegistrazione) {
		try {
			templateRegistrazioneDAO.updateParzialeRegistrazioneAusiliaria(connection, documentTitle, idAoo, numeroRegistrazione, idRegistrazione, dataRegistrazione);
		} catch (final Exception e) {
			LOGGER.error("Errore riscontrato durante l'aggiornamento parziale della registrazione ausiliaria relativa al documento: " + documentTitle, e);
			throw new RedException(e);
		}
	}
	
	/**
	 * Consente un update completo delle informazioni della registrazione ausiliaria
	 * <em> eventualmente </em> associata al documento identificato dal
	 * <code> documentTitle </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IRegistrazioniAusiliarieFacadeSRV#updateDatiRegistrazioneAusiliaria(java.lang.String,
	 *      int, java.util.Collection, boolean)
	 */
	@Override
	public void updateDatiRegistrazioneAusiliaria(final String documentTitle, final int idAoo, final Collection<MetadatoDTO> metadati, final boolean documentoDefinitivo) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			updateDatiRegistrazioneAusiliaria(documentTitle, idAoo, metadati, documentoDefinitivo, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore riscontrato durante l'aggiornamento dei dati della registrazione ausiliaria relativa al documento: " + documentTitle, e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}
}
