package it.ibm.red.business.service.pstrategy;

import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.MessaggioPostaNpsFlussoDTO;
import it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;

/**
 * Strategy di protocollazione automatica flusso SILICE.
 */
public class SILICEAutoProtocolStrategy extends ORDAutoProtocolStrategy {

	/**
	 * @see it.ibm.red.business.service.pstrategy.ORDAutoProtocolStrategy#protocol(it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO,
	 *      java.lang.String).
	 */
	@Override
	public EsitoSalvaDocumentoDTO protocol(final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessagio, final String guidFilenetMessaggio) {
		return protocol(richiestaElabMessagio, guidFilenetMessaggio, TipoContestoProceduraleEnum.SILICE);
	}
	
}