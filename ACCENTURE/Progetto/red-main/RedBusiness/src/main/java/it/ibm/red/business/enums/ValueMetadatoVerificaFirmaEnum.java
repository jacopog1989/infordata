package it.ibm.red.business.enums;

/**
 * Enum che definisce il value del metadato verifica firma.
 */
public enum ValueMetadatoVerificaFirmaEnum {

	/**
	 * valore Verifica Firma 0.
	 */
	FIRMA_NON_VERIFICATA(0),

	/**
	 * valore Verifica Firma 1.
	 */
	FIRMA_OK(1),

	/**
	 * valore Verifica Firma 2.
	 */
	FIRMA_KO(2),

	/**
	 * valore Verifica Firma 3.
	 */
	FIRMA_NON_PRESENTE(3);

	/**
	 * Valore.
	 */
	private Integer value;

	/**
	 * Costruttore di default.
	 * @param inValue
	 */
	ValueMetadatoVerificaFirmaEnum(final Integer inValue) {
		this.value = inValue;
	}

	/**
	 * @return value - valore metadato Verifica Firma.
	 */
	public Integer getValue() {
		return value;
	}
	
	/**
	 * Restituisce un oggetto dell'enum in base al valore in input.
	 * @param value
	 * @return valueVerificaFirma
	 */
	public static ValueMetadatoVerificaFirmaEnum get(final Integer value) {
		ValueMetadatoVerificaFirmaEnum outValueVerificaFirma = null;
		if (value != null) {
			for (ValueMetadatoVerificaFirmaEnum valueVerificaFirma : ValueMetadatoVerificaFirmaEnum.values()) {
				if (valueVerificaFirma.getValue().equals(value)) {
					outValueVerificaFirma = valueVerificaFirma;
					break;
				}
			}
		}
		return outValueVerificaFirma;
	}
}