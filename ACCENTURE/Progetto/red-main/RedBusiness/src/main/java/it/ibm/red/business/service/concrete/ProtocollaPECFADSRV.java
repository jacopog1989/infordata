package it.ibm.red.business.service.concrete;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;
import com.filenet.api.property.Properties;
import com.google.common.net.MediaType;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.Varie;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.MetadatoDinamicoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.DocumentoRedFnDTO;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.adobe.AdobeLCHelper;
import it.ibm.red.business.helper.adobe.IAdobeLCHelper;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.html.HtmlFileHelper;
import it.ibm.red.business.helper.xml.AbstractXmlHelper;
import it.ibm.red.business.helper.xml.AttoDecretoXmlHelper;
import it.ibm.red.business.helper.xml.DatiProtocollo;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.service.IAllegatoSRV;
import it.ibm.red.business.service.IProtocollaPECFADSRV;
import it.ibm.red.business.service.concrete.FepaSRV.ProvenienzaType;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Service che gestisce la funzionalità di protocollazione PEC FAD.
 */
@Service
@Component
public class ProtocollaPECFADSRV extends ProtocollaPECAbstractSRV implements IProtocollaPECFADSRV {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 5072215933806523575L;

	/**
	 * Servizio.
	 */
	@Autowired
	private IAllegatoSRV allegatoSRV;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ProtocollaPECFADSRV.class.getName());
	
	/**
	 * @see it.ibm.red.business.service.concrete.ProtocollaPECAbstractSRV#initXmlHelper(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      com.filenet.api.core.Document).
	 */
	@Override
	public AbstractXmlHelper initXmlHelper(final IFilenetCEHelper fceh, final Document email) {
		FileItem segnatura = null;
		try {
			segnatura = extractAllegato(fceh, email, Constants.Varie.SEGNATURA_FILENAME);
			final AbstractXmlHelper helper = new AttoDecretoXmlHelper();
			helper.setXmlDocument(segnatura.getInputStream());
			return helper;
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di parsing dell'allegato della mail " + email.get_Id().toString(), e);
			throw new RedException(e);
		} finally {
			if (segnatura != null) {
				segnatura.delete();
			}
		}
	}
	
	/**
	 * @see it.ibm.red.business.service.IProtocollaPECSRV#extractMailAttachments(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      com.filenet.api.core.Document, it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.DetailDocumentRedDTO,
	 *      it.ibm.red.business.helper.xml.AbstractXmlHelper).
	 */
	@Override
	public void extractMailAttachments(final IFilenetCEHelper fceh, final Document email, final UtenteDTO utente, final DetailDocumentRedDTO documento, final AbstractXmlHelper helper) {
	
		try {
			
			populateDocumentoPrincipale(documento, email, fceh, (AttoDecretoXmlHelper) helper);
			populateAllegati(documento, email, fceh, (AttoDecretoXmlHelper) helper);
									
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di estrazione degli allegati della mail " + email.get_Id().toString(), e);
			throw new RedException(e);
		} 
		
	}
	
	private void populateAllegati(final DetailDocumentRedDTO documento, final Document email, final IFilenetCEHelper fceh, final AttoDecretoXmlHelper helper) throws IOException {
		
		final int numeroAllegati = helper.getNumeroAllegati();
		if (numeroAllegati != 0) {
			final List<AllegatoDTO> allegati = new ArrayList<>();
			
			for (int i = 0; i < numeroAllegati; i++) {
				allegati.add(populateAllegato(email, fceh, i, helper));
			}
			
			documento.setAllegati(allegati);
		}
		
	}

	private AllegatoDTO populateAllegato(final Document email, final IFilenetCEHelper fceh, final int i, final AttoDecretoXmlHelper helper) throws IOException {
		
		final AllegatoDTO allegato = new AllegatoDTO();
		
		allegato.setNomeFile(helper.getFileNameAllegato(i));
		
		final String oggetto = helper.getOggettoAllegato(i);
		allegato.setOggetto((StringUtils.isNullOrEmpty(oggetto) ? helper.getOggettoIntestazione() : oggetto));
		
		allegato.setFormato(FormatoAllegatoEnum.FORMATO_ORIGINALE.getId());
		allegato.setFormatoOriginale(true);
		
		FileItem fi = null;
		try {
			fi = extractAllegato(fceh, email, helper.getFileNameAllegato(i));
			allegato.setMimeType(fi.getContentType());
			allegato.setContent(FileUtils.getByteFromInputStream(fi.getInputStream()));
		} finally {
			if (fi != null) {
				fi.delete();
			}
		}
		
		
		return allegato;
		
	}

	private void populateDocumentoPrincipale(final DetailDocumentRedDTO documento, final Document email, final IFilenetCEHelper fceh, final AttoDecretoXmlHelper helper) throws IOException {
		
		final Properties propMail = email.getProperties();
		
		documento.setDocumentClass(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));
		documento.setMittente(propMail.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.FROM_MAIL_METAKEY)));
		documento.setNomeFile(helper.getFileNameDocumentoPrincipale());
		
		final String oggetto = helper.getOggettoDocumentoPrincipale();
		documento.setOggetto(StringUtils.isNullOrEmpty(oggetto) ? helper.getOggettoIntestazione() : oggetto);
		
		final Collection<MetadatoDinamicoDTO> metadatiDinamici = new ArrayList<>();
		metadatiDinamici.add(new MetadatoDinamicoDTO(getPP().getParameterByKey(PropertiesNameEnum.AMMINISTRAZIONE_FAD_METAKEY), helper.getCodiceAmministrazione()));
		metadatiDinamici.add(new MetadatoDinamicoDTO(getPP().getParameterByKey(PropertiesNameEnum.RAGIONERIA_FAD_METAKEY), helper.getCodiceRagioneria()));
		
		DatiProtocollo tipoProtocollo = helper.getProtocolloAmm();
		metadatiDinamici.add(new MetadatoDinamicoDTO(getPP().getParameterByKey(PropertiesNameEnum.AOO_PROT_AMM_METAKEY), tipoProtocollo.getAoo()));
		metadatiDinamici.add(new MetadatoDinamicoDTO(getPP().getParameterByKey(PropertiesNameEnum.NUM_PROT_AMM_METAKEY), tipoProtocollo.getNumeroProtocollo()));
		metadatiDinamici.add(new MetadatoDinamicoDTO(getPP().getParameterByKey(PropertiesNameEnum.DATA_PROT_AMM_METAKEY), DateUtils.parseDate(tipoProtocollo.getDataProtocollo())));
		
		tipoProtocollo = helper.getProtocolloIngressoUCB();
		metadatiDinamici.add(new MetadatoDinamicoDTO(getPP().getParameterByKey(PropertiesNameEnum.AOO_PROT_UCB_INGRESSO_METAKEY), tipoProtocollo.getAoo()));
		metadatiDinamici.add(new MetadatoDinamicoDTO(getPP().getParameterByKey(PropertiesNameEnum.NUM_PROT_UCB_INGRESSO_METAKEY), tipoProtocollo.getNumeroProtocollo()));
		metadatiDinamici.add(new MetadatoDinamicoDTO(getPP().getParameterByKey(PropertiesNameEnum.DATA_PROT_UCB_INGRESSO_METAKEY), DateUtils.parseDate(tipoProtocollo.getDataProtocollo())));
		
		tipoProtocollo = helper.getProtocolloUscitaUCB();
		metadatiDinamici.add(new MetadatoDinamicoDTO(getPP().getParameterByKey(PropertiesNameEnum.AOO_PROT_UCB_USCITA_METAKEY), tipoProtocollo.getAoo()));
		metadatiDinamici.add(new MetadatoDinamicoDTO(getPP().getParameterByKey(PropertiesNameEnum.NUM_PROT_UCB_USCITA_METAKEY), tipoProtocollo.getNumeroProtocollo()));
		metadatiDinamici.add(new MetadatoDinamicoDTO(getPP().getParameterByKey(PropertiesNameEnum.DATA_PROT_UCB_USCITA_METAKEY), DateUtils.parseDate(tipoProtocollo.getDataProtocollo())));
		
		documento.setMetadatiDinamici(metadatiDinamici);
		
		documento.setTipoProtocollo(Constants.Protocollo.TIPO_PROTOCOLLO_ENTRATA);
		
		FileItem fi = null;
		try {
			fi = extractAllegato(fceh, email, helper.getFileNameDocumentoPrincipale());
			documento.setMimeType(fi.getContentType());
			documento.setContent(FileUtils.getByteFromInputStream(fi.getInputStream()));
		} finally {
			if (fi != null) {
				fi.delete();
			}
		}
		
	}

	/**
	 * @see it.ibm.red.business.service.concrete.ProtocollaPECAbstractSRV#eseguiAttivitaPostProtocollazione(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      com.filenet.api.core.Document, java.lang.String,
	 *      it.ibm.red.business.persistence.model.Aoo,
	 *      it.ibm.red.business.helper.xml.AbstractXmlHelper).
	 */
	@Override
	public void eseguiAttivitaPostProtocollazione(final IFilenetCEHelper fceh, final Document email, final String guidDocumento, final Aoo aoo, final AbstractXmlHelper helper) {
		
		final Document doc = fceh.getDocumentByGuid(guidDocumento, 
				new String[] {
						getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY),
						getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY),
						getPP().getParameterByKey(PropertiesNameEnum.AOO_PROT_AMM_METAKEY),
						getPP().getParameterByKey(PropertiesNameEnum.NUM_PROT_AMM_METAKEY),
						getPP().getParameterByKey(PropertiesNameEnum.DATA_PROT_AMM_METAKEY),
						getPP().getParameterByKey(PropertiesNameEnum.AOO_PROT_UCB_INGRESSO_METAKEY),
						getPP().getParameterByKey(PropertiesNameEnum.NUM_PROT_UCB_INGRESSO_METAKEY),
						getPP().getParameterByKey(PropertiesNameEnum.DATA_PROT_UCB_INGRESSO_METAKEY),
						getPP().getParameterByKey(PropertiesNameEnum.AOO_PROT_UCB_USCITA_METAKEY),
						getPP().getParameterByKey(PropertiesNameEnum.NUM_PROT_UCB_USCITA_METAKEY),
						getPP().getParameterByKey(PropertiesNameEnum.DATA_PROT_UCB_USCITA_METAKEY),
				});
		
		final List<DatiProtocollo> protocolli = new ArrayList<>();
		
		DatiProtocollo tipoProtocollo = new DatiProtocollo(
				aoo.getCodiceAoo(),
				Constants.Protocollo.TIPOLOGIA_PROTOCOLLO_INGRESSO,
				doc.getProperties().getInteger32Value(getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY)),
				DateUtils.dateToString(doc.getProperties().getDateTimeValue(getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY)), true), 
				ProvenienzaType.RED
				);
		
		protocolli.add(tipoProtocollo);
		
		tipoProtocollo = new DatiProtocollo(
				doc.getProperties().getStringValue(getPP().getParameterByKey(PropertiesNameEnum.AOO_PROT_AMM_METAKEY)),
				Constants.Protocollo.TIPOLOGIA_PROTOCOLLO_USCITA,
				doc.getProperties().getInteger32Value(getPP().getParameterByKey(PropertiesNameEnum.NUM_PROT_AMM_METAKEY)),
				DateUtils.dateToString(doc.getProperties().getDateTimeValue(getPP().getParameterByKey(PropertiesNameEnum.DATA_PROT_AMM_METAKEY)), true), 
				ProvenienzaType.AMMINISTRAZIONE
				);
		
		protocolli.add(tipoProtocollo);
		
		tipoProtocollo = new DatiProtocollo(
				doc.getProperties().getStringValue(getPP().getParameterByKey(PropertiesNameEnum.AOO_PROT_UCB_INGRESSO_METAKEY)),
				Constants.Protocollo.TIPOLOGIA_PROTOCOLLO_INGRESSO,
				doc.getProperties().getInteger32Value(getPP().getParameterByKey(PropertiesNameEnum.NUM_PROT_UCB_INGRESSO_METAKEY)),
				DateUtils.dateToString(doc.getProperties().getDateTimeValue(getPP().getParameterByKey(PropertiesNameEnum.DATA_PROT_UCB_INGRESSO_METAKEY)), true), 
				ProvenienzaType.UCB
				);
		
		protocolli.add(tipoProtocollo);
		
		tipoProtocollo = new DatiProtocollo(
				doc.getProperties().getStringValue(getPP().getParameterByKey(PropertiesNameEnum.AOO_PROT_UCB_USCITA_METAKEY)),
				Constants.Protocollo.TIPOLOGIA_PROTOCOLLO_USCITA,
				doc.getProperties().getInteger32Value(getPP().getParameterByKey(PropertiesNameEnum.NUM_PROT_UCB_USCITA_METAKEY)),
				DateUtils.dateToString(doc.getProperties().getDateTimeValue(getPP().getParameterByKey(PropertiesNameEnum.DATA_PROT_UCB_USCITA_METAKEY)), true), 
				ProvenienzaType.UCB
				);
		
		protocolli.add(tipoProtocollo);
				
		final ByteArrayInputStream reportHTML = (new HtmlFileHelper()).createReportProtocolliFadHTML(protocolli);
		InputStream reportPDF = null;
		
		try {
			
			final IAdobeLCHelper adobeh = new AdobeLCHelper();
			reportPDF = adobeh.htmlToPdf(IOUtils.toByteArray(reportHTML)); 
		
		} catch (final IOException e) {
			throw new RedException("Errore in fase di lettura del report HTML generato", e);
		}
		
		//costruisci allegato dto
		
		final String documentTitleReportProtocolli = Varie.REPORT_PROTOCOLLI_NAME + "_" + (new SimpleDateFormat("ddMMyyyyHHmmss")).format(new Date());
		
		final AllegatoDTO allegato = new AllegatoDTO();
		allegato.setNomeFile(Varie.REPORT_PROTOCOLLI_PDF_FILENAME);
		allegato.setOggetto(Varie.REPORT_PROTOCOLLI_PDF_DESCR);
		allegato.setDocumentTitle(documentTitleReportProtocolli);
		allegato.setFormato(FormatoAllegatoEnum.ELETTRONICO.getId());
		allegato.setFormatoOriginale(true);
		allegato.setMimeType(MediaType.PDF.toString());
		allegato.setContent(FileUtils.getByteFromInputStream(reportPDF));
		
		//converti in dto filenet
		List<DocumentoRedFnDTO> allegati = null;
		try {
			allegati = allegatoSRV.convertToAllegatiFilenet(Arrays.asList(allegato), aoo.getIdAoo());
		} catch (final IOException e) {
			throw new RedException("Errore in fase di conversione dell'allegato in oggetto Filenet", e);
		}
		
		fceh.inserisciAllegati(doc, allegati);		
		
	}
	
	/**
	 * @see it.ibm.red.business.service.concrete.ProtocollaPECAbstractSRV#sbustaP7M().
	 */
	@Override
	public boolean sbustaP7M() {
		return true;
	}
	
}