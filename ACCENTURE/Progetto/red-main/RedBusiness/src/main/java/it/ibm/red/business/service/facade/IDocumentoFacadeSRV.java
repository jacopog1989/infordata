package it.ibm.red.business.service.facade;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.filenet.api.core.Document;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.ApprovazioneDTO;
import it.ibm.red.business.dto.AttachmentDTO;
import it.ibm.red.business.dto.DetailDocumentoDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.GestioneLockDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.MasterDocumentoDTO;
import it.ibm.red.business.dto.PlaceholderInfoDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoOperazioneLibroFirmaEnum;
import it.ibm.red.business.exception.SearchException;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;


/**
 * The Interface IDocumentoFacadeSRV.
 *
 * @author CPIERASC
 * 
 *         Servizi per la gestione dei documenti.
 */
public interface IDocumentoFacadeSRV extends Serializable {

	/**
	 * Recupero content documento sulla classe documentale base per NSD.
	 * 
	 * @param fcDTO					credenziali FileNet
	 * @param documentTitle			id documento
	 * @param onlyPdf				posto a true impone la ricerca della velina in assenza di un content pdf
	 * @param idAoo					id aoo
     * @param contentLibroFirma		indica se recuperare il content originale del documento o quello dell'annotation CONTENT_LIBRO_FIRMA
	 * @return				content
	 */
	FileDTO getDocumentoContentPreview(FilenetCredentialsDTO fcDTO, String documentTitle, boolean onlyPdf, Long idAoo, boolean contentLibroFirma);
	
	/**
	 * Recupero content documento sulla classe documentale base per NSD.
	 * 
	 * @param fcDTO				credenziali FileNet
	 * @param documentTitle		id documento
	 * @param onlyPdf			posto a true impone la ricerca della velina in assenza di un content pdf
	 * @param idAoo				id aoo
	 * @return
	 */
	FileDTO getDocumentoContentPreview(FilenetCredentialsDTO fcDTO, String documentTitle, boolean onlyPdf, Long idAoo);

	/**
	 * Recupero content documento sulla classe documentale dei contributi.
	 * 
	 * @param fcDTO         credenziali filenet
	 * @param documentTitle id documento
	 * @return content
	 */
	FileDTO getContributoContentPreview(FilenetCredentialsDTO fcDTO, String documentTitle, Long idAoo);

	/**
	 * Recupero content allegato.
	 * 
	 * @param id	identificativo
	 * @param fcDTO	credenziali filenet
	 * @return		content allegato
	 */
	AttachmentDTO getContentAllegato(String id, FilenetCredentialsDTO fcDTO, Long idAoo);
	
	/**
	 * Recupero content allegato.
	 * 
	 * @param id					identificativo
	 * @param fcDTO					credenziali FileNet
	 * @param contentLibroFirma		indica se deve essere recuperato il content originale o quello dell'annotation CONTENT_LIBRO_FIRMA
	 * @return		content allegato
	 */
	AttachmentDTO getContentAllegato4Preview(String id, FilenetCredentialsDTO fcDTO, boolean isDownload, PlaceholderInfoDTO placeholderInfo, Long idAoo, boolean contentLibroFirma);


	/**
	 * Recupero dettaglio documento.
	 * 
	 * @param utente    credenziali utente
	 * @param wobNumber wob numbwe
	 * @param idAoo     identificativo aoo
	 * @return dettaglio documento
	 */
	DetailDocumentoDTO getDocumento(UtenteDTO utente, String wobNumber, Long idAoo);

	/**
	 * Recupero documenti libro firma.
	 * 
	 * @param fcDTO     credenziali filenet
	 * @param idUtente  identificativo utente
	 * @param idUfficio identificativo ufficio
	 * @param idRuolo   identificativo ruolo
	 * @param idAoo     identificativo aoo
	 * @param filter    filtro tipo operazione
	 * @param forCount  TRUE per risultati per il conteggio
	 * @return collezione documenti
	 */
	Collection<MasterDocumentoDTO> getDocumenti(FilenetCredentialsDTO fcDTO, Long idUtente, Long idUfficio, Long idRuolo, Long idAoo, TipoOperazioneLibroFirmaEnum filter,
			boolean forCount);

	/**
	 * Recupero il numero documenti libro firma.
	 * 
	 * @param fcDTO     credenziali filenet
	 * @param idUtente  identificativo utente
	 * @param idUfficio identificativo ufficio
	 * @param idAoo     identificativo aoo
	 * @param idRuolo   identificativo ruolo
	 * @param filter    filtro tipo operazione
	 * @return collezione documenti
	 */
	int getCountDocumenti(FilenetCredentialsDTO fcDTO, Long idUtente, Long idUfficio, Long idAoo, Long idRuolo, TipoOperazioneLibroFirmaEnum filter);

	/**
	 * Metodo per eseguire ricerca generica.
	 * 
	 * @param key    chiave ricerca
	 * @param anno   anno ricerca
	 * @param type   tpologia ricerca
	 * @param utente credenziali utente
	 * @return documenti risultanti
	 * @throws SearchException eccezione generata in fase di ricerca
	 */
	Collection<MasterDocumentoDTO> ricercaGenerica(String key, Integer anno, RicercaGenericaTypeEnum type, UtenteDTO utente) throws SearchException;

	/**
	 * Recupero allegati documento.
	 * 
	 * @param fcDTO credenziali filenet
	 * @param id    identificativo documento
	 * @return allegati
	 */
	Collection<AttachmentDTO> getAttachments(FilenetCredentialsDTO fcDTO, String id, Long idAoo);

	/**
	 * Recupero documento solo da CE.
	 * 
	 * @param fcDTO         credenziali filenet
	 * @param documentTitle document title documento
	 * @param utente        utente che esegue l'operazione
	 * @return dettaglio del documento
	 */
	DetailDocumentoDTO getDocumentoFromCE(FilenetCredentialsDTO fcDTO, String documentTitle, UtenteDTO utente);

	/**
	 * Recupero documento da PE/CE.
	 * 
	 * @param documentTitle document title documento
	 * @param utente        credenziali utente
	 * @return documento
	 * @throws SearchException eccezione generata in fase di estrazione del
	 *                         documento
	 */
	MasterDocumentoDTO getDocumento(String documentTitle, UtenteDTO utente) throws SearchException;

	/**
	 * Metodo per l'aggiornamento dell'icona stato del master.
	 *
	 * @param master - DTO del master
	 * @param idAoo  - Identificativo dell'AOO
	 * @throws SearchException the search exception
	 */
	void updateMasterIconaStato(MasterDocumentoDTO master, Long idAoo) throws SearchException;

	/**
	 * Annulla il documento.
	 * 
	 * @param docAnnullato
	 * @param motivoAnnullamento
	 * @param peh
	 * @param ceh
	 * @param wo
	 * @param idAoo
	 * @throws IOException
	 */
	void annullaDocumento(Document docAnnullato, String motivoAnnullamento, FilenetPEHelper peh, IFilenetCEHelper ceh, VWWorkObject wo, Long idAoo) throws IOException;

	/**
	 * Verifica la presenza di richieste di contributo pendenti per il documento.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @return true o false
	 */
	boolean hasRichiesteContributoPendenti(String documentTitle, Long idAoo);

	/**
	 * Ottiene la preview del content del contributo.
	 * 
	 * @param fcDTO
	 * @param documentTitle
	 * @param idAoo
	 * @return preview del content del contributo
	 */
	FileDTO getRegistroProtocolloContentPreview(FilenetCredentialsDTO fcDTO, String documentTitle, Long idAoo);

	/**
	 * Verifica l'obbligatorietà della faldonatura per il documento.
	 * 
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @return true o false
	 */
	boolean isDocumentoFaldonaturaObbligatoria(Integer idTipologiaDocumento, Integer idTipoProcedimento);

	/**
	 * Ottiene la preview del content del documento SIGI.
	 * 
	 * @param fcDTO
	 * @param documentTitle
	 * @param idAoo
	 * @return preview del content del documento SIGI
	 */
	FileDTO getDocumentoSIGIContentPreview(FilenetCredentialsDTO fcDTO, String documentTitle, Long idAoo);

	/**
	 * Permette di costruire e ritrnare il file delle approvazioni stampigliate.
	 * 
	 * @param utente
	 * @param numeroDocumento
	 * @param descrizioneIspettorato
	 * @param approvazioneList
	 * @return
	 */
	InputStream createPdfApprovazioniStampigliate(UtenteDTO utente, Integer numeroDocumento, String documentTitle, List<ApprovazioneDTO> approvazioneList);

	/**
	 * Ottiene i codici di amministrazione dal wobnumber.
	 * 
	 * @param wobNumber
	 * @param utente
	 * @return lista di codici di amministrazione
	 */
	String[] codiceAmministrazioneFromWob(String wobNumber, UtenteDTO utente);

	/**
	 * Verifica l'obbligatorietà della faldonatura per il documento.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @param fcDTO
	 * @return true o false
	 */
	boolean isDocumentoFaldonaturaObbligatoria(String documentTitle, Long idAoo, FilenetCredentialsDTO fcDTO);

	/**
	 * Ottiene la preview del content del documento cartaceo.
	 * 
	 * @param fcDTO
	 * @param documentTitle
	 * @param idAoo
	 * @return preview del content del documento cartaceo
	 */
	FileDTO getDocumentoCartaceoContentPreview(FilenetCredentialsDTO fcDTO, String documentTitle, Long idAoo);

	/**
	 * Ottiene la preview del content dell'allegato.
	 * 
	 * @param fcDTO
	 * @param guid
	 * @param onlyPdf
	 * @param nomeFile
	 * @param idAoo
	 * @return preview del content dell'allegato
	 */
	FileDTO getContentPreviewAllegato(FilenetCredentialsDTO fcDTO, String guid, Boolean onlyPdf, boolean nomeFile, Long idAoo, boolean contentLibroFirma);
	
	/**
	 * Verifica se si tratta di flussi AUT UCB.
	 * 
	 * @param idDocUscita
	 * @param allaccioPrincipale
	 * @param utente
	 * @return true o false
	 */
	boolean checkIsFlussiAutUCB(String idDocUscita, RispostaAllaccioDTO allaccioPrincipale, UtenteDTO utente);

	/**
	 * Verifica se si tratta di un flusso Atto Decreto UCB.
	 * 
	 * @param utente
	 * @param idTipologiaDocumento
	 * @return true o false
	 */
	boolean checkIsFlussoAttoDecretoUCB(UtenteDTO utente, Long idTipologiaDocumento);

	/**
	 * Gestisce i destinatari interni.
	 * 
	 * @param wobNumber
	 * @param documentTitle
	 * @param idAOO
	 * @param idUtente
	 * @param idUfficio
	 * @param idRuolo
	 * @param numeroProtocollo
	 * @param annoProtocollo
	 * @param isRiservato
	 * @return true o false
	 */
	boolean gestisciDestinatariInterni(String wobNumber, String documentTitle, Integer idAOO, Long idUtente, Long idUfficio, Long idRuolo, Integer numeroProtocollo,
			String annoProtocollo, boolean isRiservato);

	/**
	 * Ottiene il file principale dall'allegato.
	 * 
	 * @param fcDTO
	 * @param guid
	 * @param onlyPdf
	 * @param nomeFile
	 * @param utente
	 * @param tipoFirma
	 * @param siglatarioPrincipaleString
	 * @param placeholderInfo
	 * @return
	 */
	Long getFilePrincipaleFromAllegato(FilenetCredentialsDTO fcDTO, String guid, Boolean onlyPdf, boolean nomeFile, UtenteDTO utente, TipoAssegnazioneEnum tipoFirma,
			String[] siglatarioPrincipaleString, PlaceholderInfoDTO placeholderInfo);

	/**
	 * Ottiene il lock del documento.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @param statoLock
	 * @param idUtente
	 * @return lock del documento
	 */
	GestioneLockDTO getLockDocumento(Integer documentTitle, Long idAoo, Integer statoLock, Long idUtente);

	/**
	 * Elimina il lock del documento.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @param statoLock
	 * @param idUtente
	 * @return true o false
	 */
	boolean deleteLockDocumento(Integer documentTitle, Long idAoo, Integer statoLock, Long idUtente);

	/**
	 * Ottiene il lock massivo dei documenti.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @param statoLock
	 * @param idUtente
	 * @return lock dei documenti
	 */
	GestioneLockDTO getLockDocumentoMassivo(List<Integer> documentTitle, Long idAoo, Integer statoLock, Long idUtente);
	
	/**
	 * Inserisce il lock del documento
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @param statoLock
	 * @param idUtente
	 * @param nome
	 * @param cognome
	 * @param isIncarico
	 * @return true o false
	 */
	boolean insertLockDocumento(Integer documentTitle, Long idAoo, Integer statoLock, Long idUtente, String nome, String cognome, boolean isIncarico);

	/**
	 * Aggiorna il lock del documento.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @param statoLock
	 * @param isIncarico
	 * @param idUtente
	 * @param nome
	 * @param cognome
	 * @return true o false
	 */
	boolean updateLockDocumento(Integer documentTitle, Long idAoo, Integer statoLock, boolean isIncarico, Long idUtente, String nome, String cognome);

	/**
	 * Ottiene il lock dei documenti master.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @return mappa document title lock del documento
	 */
	HashMap<Long, GestioneLockDTO> getLockDocumentiMaster(List<String> documentTitle, Long idAoo);

	/**
	 * Restituisce il @see FileDTO per la creazione della preview dell'allegato P7M.
	 * @param fcDTO
	 * @param documentTitle
	 * @param onlyPdf
	 * @param idAoo
	 * @return informazioni per la creazione della preview
	 */
	FileDTO getAllegatoP7MPreview(FilenetCredentialsDTO fcDTO, String documentTitle, Boolean onlyPdf, Long idAoo);
	
	/**
	 * Recupera il master del documento dal suo guid.
	 * 
	 * @param utente
	 * @param guid
	 * @return
	 */
	MasterDocumentRedDTO getMasterFromGuidPrinc(UtenteDTO utente, String guid);

	AttachmentDTO getContentAllegato(String documentTitle, IFilenetCEHelper fceh, Long idAoo);

	FileDTO getAllegatoP7MPreview(IFilenetCEHelper fcehInput, String documentTitle, Boolean onlyPdf, Long idAoo);

	FileDTO getContentPreviewAllegato(IFilenetCEHelper fcehInput, String guid, Boolean onlyPdf, boolean nomeFile,
			Long idAoo, boolean contentLibroFirma);

	FileDTO getContentPreviewAllegato(IFilenetCEHelper fcehInput, FilenetCredentialsDTO fcDTO, String guid,
			Boolean onlyPdf, boolean nomeFile, Long idAoo, boolean contentLibroFirma);

	
}