/**
 * 
 */
package it.ibm.red.business.enums;

/**
 * Enum che definisce i tipi operazione APPLET.
 */
public enum TipoOperazioneAppletEnum {
	
	/**
	 * Valore.
	 */
	MODIFICA_DOCUMENTO("0"), 

	/**
	 * Valore.
	 */
	MODIFICA_TEMPLATE("1");

	/**
	 * Identificativo.
	 */
	private String id;
	
	/**
	 * Costruttore.
	 * @param inId
	 */
	TipoOperazioneAppletEnum(final String inId) {
		id = inId;
	}

	/**
	 * Restituisce l'id.
	 * @return id
	 */
	public String getId() {
		return id;
	}
}