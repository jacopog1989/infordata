package it.ibm.red.business.dto;

import java.util.List;
import java.util.Map;

/**
 * The Class FascicoloWsDTO.
 *
 * @author m.crescentini
 * 
 * 	Classe utilizzata per modellare un fascicolo utilizzato nell'interfaccia web service.
 */
public class FascicoloWsDTO extends AbstractDTO {

	private static final long serialVersionUID = -1573135429431611056L;

	/**
	 * Identificativo fascicolo.
	 */
	private final String idFascicolo;
	
	/**
	 * Oggetto.
	 */
	private final String oggetto;

	/**
	 * Titolario.
	 */
	private final String titolario;

	/**
	 * Classe documentale.
	 */
	private final String classeDocumentale;

	/**
	 * Metadati.
	 */
	private transient Map<String, Object> metadati;
	
	/**
	 * Sicurezza.
	 */
	private final List<SecurityWsDTO> security;
	

	/**
	 * @param idFascicolo
	 * @param oggetto
	 * @param titolario
	 * @param classeDocumentale
	 * @param metadati
	 * @param security
	 */
	public FascicoloWsDTO(final String idFascicolo, final String oggetto, final String titolario, final String classeDocumentale,
			final Map<String, Object> metadati, final List<SecurityWsDTO> security) {
		super();
		this.idFascicolo = idFascicolo;
		this.oggetto = oggetto;
		this.titolario = titolario;
		this.classeDocumentale = classeDocumentale;
		this.metadati = metadati;
		this.security = security;
	}

	/**
	 * @return the idFascicolo
	 */
	public String getIdFascicolo() {
		return idFascicolo;
	}

	/**
	 * @return the oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}
	
	/**
	 * @return the titolario
	 */
	public String getTitolario() {
		return titolario;
	}

	/**
	 * @return the classeDocumentale
	 */
	public String getClasseDocumentale() {
		return classeDocumentale;
	}
	
	/**
	 * @return the metadati
	 */
	public Map<String, Object> getMetadati() {
		return metadati;
	}

	/**
	 * @return the security
	 */
	public List<SecurityWsDTO> getSecurity() {
		return security;
	}
	
}
