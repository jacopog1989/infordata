package it.ibm.red.business.service;

import  it.ibm.red.business.service.facade.IComuneFacadeSRV;

/**
 * Interface del servizio gestione comuni.
 */
public interface IComuneSRV extends IComuneFacadeSRV {

}
