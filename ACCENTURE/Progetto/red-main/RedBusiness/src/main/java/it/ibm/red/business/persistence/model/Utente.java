package it.ibm.red.business.persistence.model;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class Utente.
 *
 * @author CPIERASC
 * 
 *         Entità che mappa la tabella UTENTE.
 */
public class Utente implements Serializable {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Codice fiscale.
	 */
	private final String codiceFiscale;

	/**
	 * Cognome.
	 */
	private final String cognome;

	/**
	 * Data attivazione.
	 */
	private final Date dataAttivazione;

	/**
	 * Data disattivazione.
	 */
	private final Date dataDisattivazione;

	/**
	 * Email.
	 */
	private final String email;

	/**
	 * Identificativo utente.
	 */
	private final Long idUtente;

	/**
	 * Nome.
	 */
	private final String nome;

	/**
	 * Registro riservato.
	 */
	private final Long registroRiservato;

	/**
	 * Username.
	 */
	private final String username;

	/**
	 * Oggetto contenente le credenziali di firma dell'utente.
	 */
	private final UtenteFirma utenteFirma;

	/**
	 * Oggetto contenente le preferenze dell'utente.
	 */
	private UtentePreferenze utentePreferenze;
	
	/**
	 * Costruttore. 
	 * 
	 * @param inIdUtente		identificativo
	 * @param inNome				nome
	 * @param inCognome				cognome
	 * @param inUsername			username
	 * @param inDataAttivazione		data attivazione
	 * @param inDataDisattivazione	data disattivazione
	 * @param inCodiceFiscale		codice fiscale
	 * @param inEmail				email
	 * @param inRegistroRiservato	registro riservato
	 * @param inUtenteFirma			oggetto contenente le credenziali di firma
	 * @param inUtentePreferenze	oggetto contenente le preferenze dell'utente
	 */
	public Utente(final Long inIdUtente, final String inNome, final String inCognome, final String inUsername, final Date inDataAttivazione, final Date inDataDisattivazione, 
			final String inCodiceFiscale, final String inEmail, final Long inRegistroRiservato, final UtenteFirma inUtenteFirma) {
		idUtente = inIdUtente;
		nome = inNome;
		cognome = inCognome;
		username = inUsername;
		dataAttivazione = inDataAttivazione;
		dataDisattivazione = inDataDisattivazione;
		codiceFiscale = inCodiceFiscale;
		email = inEmail;
		registroRiservato = inRegistroRiservato;
		utenteFirma = inUtenteFirma;
	}

	/**
	 * Getter codice fiscale.
	 * 
	 * @return	codice fiscale
	 */
	public final String getCodiceFiscale() {
		return codiceFiscale;
	}

	/**
	 * Getter cognome.
	 * 
	 * @return	cognome
	 */
	public final String getCognome() {
		return cognome;
	}

	/**
	 * Getter data attivazione.
	 * 
	 * @return	data attivazione
	 */
	public final Date getDataAttivazione() {
		return dataAttivazione;
	}

	/**
	 * Getter data disattivazione.
	 * 
	 * @return	data disattivazione
	 */
	public final Date getDataDisattivazione() {
		return dataDisattivazione;
	}

	/**
	 * Getter email.
	 * 
	 * @return	email
	 */
	public final String getEmail() {
		return email;
	}

	/**
	 * Getter id utente.
	 * 
	 * @return	identificativo
	 */
	public final Long getIdUtente() {
		return idUtente;
	}

	/**
	 * Getter nome.
	 * 
	 * @return	nome
	 */
	public final String getNome() {
		return nome;
	}

	/**
	 * Getter registro riservato.
	 * 
	 * @return	registro riservato
	 */
	public final Long getRegistroRiservato() {
		return registroRiservato;
	}

	/**
	 * Getter username.
	 * 
	 * @return	username
	 */
	public final String getUsername() {
		return username;
	}

	/**
	 * Getter oggetto credenziali firma.
	 * 
	 * @return	oggetto credenziali firma
	 */
	public final UtenteFirma getUtenteFirma() {
		return utenteFirma;
	}

	/**
	 * Getter oggetto preferenze.
	 * 
	 * @return	oggetto preferenze
	 */
	public final UtentePreferenze getUtentePreferenze() {
		return utentePreferenze;
	}

	/**
	 * Imposta le preferenze dell'utente.
	 * @param up
	 */
	public final void setUtentePreferenze(final UtentePreferenze up) {
		utentePreferenze = up;
	}

}