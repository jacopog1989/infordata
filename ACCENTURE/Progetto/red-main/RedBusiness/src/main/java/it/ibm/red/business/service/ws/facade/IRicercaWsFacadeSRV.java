package it.ibm.red.business.service.ws.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.RisultatoRicercaWsDTO;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.webservice.model.documentservice.types.messages.RedRicercaAvanzataType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedRicercaDocumentiType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedRicercaFaldoniType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedRicercaFascicoliType;


/**
 * The Interface IRicercaWsFacadeSRV.
 *
 * @author m.crescentini
 * 
 *         Facade per il servizio per la ricerca (generica e avanzata) da web service.
 */
public interface IRicercaWsFacadeSRV extends Serializable {
	
	/**
	 * @param client
	 * @param ricercaDocumentiInput
	 * @return
	 */
	RisultatoRicercaWsDTO redRicercaDocumenti(RedWsClient client, RedRicercaDocumentiType ricercaDocumentiInput);

	/**
	 * @param client
	 * @param request
	 * @return
	 */
	RisultatoRicercaWsDTO redRicercaAvanzata(RedWsClient client, RedRicercaAvanzataType request);

	/**
	 * @param client
	 * @param request
	 * @return
	 */
	RisultatoRicercaWsDTO redRicercaFascicoli(RedWsClient client, RedRicercaFascicoliType request);

	/**
	 * @param client
	 * @param request
	 * @return
	 */
	RisultatoRicercaWsDTO redRicercaFaldoni(RedWsClient client, RedRicercaFaldoniType request);
	
}