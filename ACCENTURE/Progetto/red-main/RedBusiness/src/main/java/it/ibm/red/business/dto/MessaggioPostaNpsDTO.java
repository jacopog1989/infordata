/**
 * 
 */
package it.ibm.red.business.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import it.ibm.red.business.enums.TipoMessaggioPostaNpsEnum;
import it.ibm.red.business.enums.TipoMessaggioPostaNpsIngressoEnum;
import it.ibm.red.business.utils.EmailUtils;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioPostaType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.EmailIndirizzoEmailType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.EmailMessageType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.ProtIdentificatoreProtocolloType;

/**
 * Rappresenta un messaggio di posta PEO o PEC in ingresso ricevuto tramite NPS.
 * 
 * @author m.crescentini
 *
 */
public class MessaggioPostaNpsDTO extends AbstractDTO {

	private static final long serialVersionUID = 2983640695454694220L;
	
	/**
	 * ID attribuito da NPS al messaggio.
	 */
	private final String idMessaggio;
	
	// ### DATI MESSAGGIO POSTA -> START
	/**
	 * ID del "documento" messaggio tramite cui interrogare NPS per il download del content del messaggio.
	 */
	private String idDocumento;
	
    
	/**
	 * Id.
	 */
	private String messageId;
	
    
	/**
	 * Date.
	 */
	private LocalDateTime data;
	
	/**
	 * Oggetto.
	 */
	private String oggetto;
	
	/**
	 * Corpo.
	 */
	private String corpo;
	
	/**
	 * Mittente.
	 */
	private ContattoPostaNpsDTO mittente;
	
	/**
	 * Destinatari.
	 */
	private List<ContattoPostaNpsDTO> destinatari;
	
	/**
	 * Casella destinataria.
	 */
	private CasellaPostaNpsDTO casellaDestinataria;
	
	/**
	 * Indica la tipologia generica del messaggio, ovvero se si tratta di: PEO/PEC standard, PEO/PEC flusso, notifica PEC, notifica di interoperabilita, etc.
	 */
	private TipoMessaggioPostaNpsEnum tipo;
	
	/**
	 * Segnatura di interoperabilità: rappresenta il file Segnatura.xml di un messaggio interoperabile.
	 */
	private SegnaturaMessaggioInteropDTO segnatura;
	// ### DATI MESSAGGIO POSTA -> END
	
	/** 
	 * Indica la tipologia specifica del messaggio in entrata ricevuto: PEO/PEC da protocollare, PEO/PEC protocollata, PEO/PEC flusso,
	 * notifica PEC, notifica di interoperabilita, notifica di errore di protocollazione, messaggio non associato a un'e-mail, etc.
	 */
	private final TipoMessaggioPostaNpsIngressoEnum tipoIngresso;

	/**
	 * Protocollo NPS del messaggio di posta.
	 */
	private ProtocolloNpsDTO protocollo;
	
	/**
	 * DTO del protocollo associato alla notifica.
	 */
	private ProtocolloNpsDTO protocolloNotifica;
	
	/**
	 * Costruttore per messaggio PEO/PEC standard.
	 * 
	 * @param richiesta
	 */
	public MessaggioPostaNpsDTO(final RichiestaElaboraMessaggioPostaType richiesta) {
		this(richiesta.getIdMessaggio(), richiesta.getIdDocumento(), TipoMessaggioPostaNpsIngressoEnum.PEO_PEC, richiesta.getMessaggio());
		
		// Segnatura
		if (richiesta.getDatiSegnatura() != null) {
			this.segnatura = new SegnaturaMessaggioInteropDTO(richiesta.getDatiSegnatura(), richiesta.getValidazioneMessaggio().getValidazioneSegnatura());
		}
	}
	
	
	/**
	 * Costruttore per messaggio PEO/PEC protocollato.
	 * 
	 * @param idMessaggio
	 * @param idDocumento
	 * @param tipoIngresso
	 * @param messaggioRicevuto
	 * @param protocollo
	 */
	public MessaggioPostaNpsDTO(final String idMessaggio, final String idDocumento, final TipoMessaggioPostaNpsIngressoEnum tipoIngresso, 
			final EmailMessageType messaggioRicevuto, final ProtIdentificatoreProtocolloType protocollo, ProtIdentificatoreProtocolloType protocolloNotifica) {
		this(idMessaggio, idDocumento, tipoIngresso, messaggioRicevuto);
		impostaProtocollo(protocollo);
		impostaProtocolloNotifica(protocolloNotifica);
	}
	
	/**
	 * Costruttore senza messaggio per SICOGE.
	 * 
	 * @param idMessaggio
	 * @param tipoIngresso
	 * @param protocollo
	 */
	public MessaggioPostaNpsDTO(final String idMessaggio, final TipoMessaggioPostaNpsIngressoEnum tipoIngresso, final ProtIdentificatoreProtocolloType protocollo) {
		this.idMessaggio = idMessaggio;
		this.tipoIngresso = tipoIngresso;
		impostaProtocollo(protocollo);
	}
	
	/**
	 * Costruttore comune.
	 * 
	 * @param idMessaggio
	 * @param idDocumento
	 * @param tipoIngresso
	 * @param messaggioRicevuto
	 */
	public MessaggioPostaNpsDTO(final String idMessaggio, final String idDocumento, final TipoMessaggioPostaNpsIngressoEnum tipoIngresso, 
			final EmailMessageType messaggioRicevuto) {
		this.idMessaggio = idMessaggio;
		this.idDocumento = idDocumento;
		this.messageId = EmailUtils.getMessageIdConParentesi(messaggioRicevuto.getCodiMessaggio());
		this.data = messaggioRicevuto.getDataMessaggio().toGregorianCalendar().toZonedDateTime().toLocalDateTime();
		this.oggetto = messaggioRicevuto.getOggettoMessaggio();
		this.corpo = messaggioRicevuto.getCorpoMessaggio();
		this.mittente = new ContattoPostaNpsDTO(messaggioRicevuto.getMittente().getDisplayName(), messaggioRicevuto.getMittente().getEmail());
		
		final String mailCasellaDestinataria = messaggioRicevuto.getCasellaEmail().getIndirizzoEmail().getEmail();
		this.casellaDestinataria = new CasellaPostaNpsDTO(mailCasellaDestinataria, mailCasellaDestinataria, 
				messaggioRicevuto.getCasellaEmail().getAoo().getCodiceAOO());
		
		this.tipo = TipoMessaggioPostaNpsEnum.get(messaggioRicevuto.getTipoMessaggio());
		this.tipoIngresso = tipoIngresso;
		
		// Destinatari -> START
		this.destinatari = new ArrayList<>();
		ContattoPostaNpsDTO destinatarioMessaggio;
		for (final EmailIndirizzoEmailType destinatario : messaggioRicevuto.getDestinatari()) {
			destinatarioMessaggio = new ContattoPostaNpsDTO(destinatario.getDisplayName(), destinatario.getEmail());
			this.destinatari.add(destinatarioMessaggio);
		}
		// Destinatari -> END
	}
	
	/**
	 * @return the idMessaggio
	 */
	public String getIdMessaggio() {
		return idMessaggio;
	}


	/**
	 * @return the messageId
	 */
	public String getMessageId() {
		return messageId;
	}


	/**
	 * @return the idDocumento
	 */
	public String getIdDocumento() {
		return idDocumento;
	}


	/**
	 * @return the data
	 */
	public LocalDateTime getData() {
		return data;
	}


	/**
	 * @param data the data to set
	 */
	public void setData(final LocalDateTime data) {
		this.data = data;
	}


	/**
	 * @return the oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}


	/**
	 * @return the corpo
	 */
	public String getCorpo() {
		return corpo;
	}


	/**
	 * @param corpo the corpo to set
	 */
	public void setCorpo(final String corpo) {
		this.corpo = corpo;
	}


	/**
	 * @return the mittente
	 */
	public ContattoPostaNpsDTO getMittente() {
		return mittente;
	}


	/**
	 * @return the destinatari
	 */
	public List<ContattoPostaNpsDTO> getDestinatari() {
		return destinatari;
	}


	/**
	 * @return the casellaDestinataria
	 */
	public CasellaPostaNpsDTO getCasellaDestinataria() {
		return casellaDestinataria;
	}


	/**
	 * @return the tipoMessaggio
	 */
	public TipoMessaggioPostaNpsEnum getTipoMessaggio() {
		return tipo;
	}
	

	/**
	 * @return the tipoIngresso
	 */
	public TipoMessaggioPostaNpsIngressoEnum getTipoIngresso() {
		return tipoIngresso;
	}


	/**
	 * @return the segnatura
	 */
	public SegnaturaMessaggioInteropDTO getSegnatura() {
		return segnatura;
	}


	/**
	 * @return the protocolloMittente
	 */
	public ProtocolloNpsDTO getProtocollo() {
		return protocollo;
	}

	/**
	 * Restituisce il protocollo notifica se ne esiste uno associato al documento.
	 * @return protocollo notifica
	 */
	public ProtocolloNpsDTO getProtocolloNotifica() {
		return protocolloNotifica;
	}

	/**
	 * @param protocollo
	 */
	private void impostaProtocollo(final ProtIdentificatoreProtocolloType protocollo) {
		if(protocollo != null) {
			this.protocollo = new ProtocolloNpsDTO();
			this.protocollo.setIdProtocollo(protocollo.getIdProtocollo());
			this.protocollo.setNumeroProtocollo(protocollo.getNumeroRegistrazione());
			this.protocollo.setDataProtocollo(protocollo.getDataRegistrazione().toGregorianCalendar().getTime());
			this.protocollo.setTipoProtocollo(protocollo.getTipoProtocollo().toString());
			final Calendar cal = Calendar.getInstance();
			cal.setTime(this.protocollo.getDataProtocollo());
			this.protocollo.setAnnoProtocollo(String.valueOf(cal.get(Calendar.YEAR)));
			this.protocollo.setCodiceAoo(protocollo.getRegistro().getAoo().getCodiceAOO());
		}
	}
	
	/**
	 * Imposta i parametri del protocollo di notifica.
	 * @param protocolloNotificaToSet
	 */
	private void impostaProtocolloNotifica(ProtIdentificatoreProtocolloType protocolloNotificaToSet) {
		if (protocolloNotificaToSet != null) {
			this.protocolloNotifica = new ProtocolloNpsDTO();
			this.protocolloNotifica.setIdProtocollo(protocolloNotificaToSet.getIdProtocollo());
			this.protocolloNotifica.setNumeroProtocollo(protocolloNotificaToSet.getNumeroRegistrazione());
			this.protocolloNotifica.setDataProtocollo(protocolloNotificaToSet.getDataRegistrazione().toGregorianCalendar().getTime());
			this.protocolloNotifica.setTipoProtocollo(protocolloNotificaToSet.getTipoProtocollo().toString());
			final Calendar cal = Calendar.getInstance();
			cal.setTime(this.protocolloNotifica.getDataProtocollo());
			this.protocolloNotifica.setAnnoProtocollo(String.valueOf(cal.get(Calendar.YEAR)));
			this.protocolloNotifica.setCodiceAoo(protocolloNotificaToSet.getRegistro().getAoo().getCodiceAOO());
		}
	}
}
