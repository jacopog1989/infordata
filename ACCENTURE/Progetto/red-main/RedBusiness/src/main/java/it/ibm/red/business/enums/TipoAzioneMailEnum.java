package it.ibm.red.business.enums;

/**
 * The Enum TipoAzioneEnum.
 *
 * @author CPIERASC
 * 
 *         Enum per le azioni rappresentate sul calendario.
 */
public enum TipoAzioneMailEnum {	
	
	/**
	 * Protocolla.
	 */
	MAIL_PROTOCOLLA("Protocolla"),
	
	/**
	 * Protocolla Dsr.
	 */
	MAIL_PROTOCOLLA_DSR("Protocolla Dsr"),
	
	/**
	 * Rifiuta.
	 */
	MAIL_RIFIUTA("Rifiuta"),
	
	/**
	 * Inoltra.
	 */
	MAIL_INOLTRA("Inoltra"),
	
	/**
	 * Elimina.
	 */
	MAIL_ELIMINA("Elimina"),
	
	/**
	 * Ripristina.
	 */
	MAIL_RIPRISTINA("Ripristina"),
	
	/**
	 * InoltraDaCoda.
	 */
	MAIL_INOLTRA_DA_CODA("InoltraDaCoda");

	/**
	 * Valore.
	 */
	private String value;

	/**
	 * Costruttore.
	 * 
	 * @param inValue	value
	 */
	TipoAzioneMailEnum(final String inValue) {
		this.value = inValue;
	}

	/**
	 * Getter valore.
	 * 
	 * @return	valore
	 */
	public String getValue() {
		return value;
	}

}
