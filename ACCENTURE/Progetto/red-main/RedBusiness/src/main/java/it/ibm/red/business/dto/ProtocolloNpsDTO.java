/**
 * 
 */
package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import it.ibm.red.business.nps.dto.AllaccioDTO;
import it.ibm.red.business.nps.dto.AllegatoNpsDTO;
import it.ibm.red.business.nps.dto.AssegnatariDTO;
import it.ibm.red.business.nps.dto.DocumentoNpsDTO;
import it.ibm.red.business.nps.dto.PersonaFisicaDTO;
import it.ibm.red.business.nps.dto.StoricoDTO;
import it.ibm.red.business.persistence.model.Contatto;

/**
 * Classe ProtocolloNpsDTO.
 *
 * @author APerquoti
 */
public class ProtocolloNpsDTO extends AbstractDTO implements Serializable {

	/**
	 * Constant serial UID
	 */
	private static final long serialVersionUID = -3187847906712114552L;
	
	/**
	 * Identificativo documento
	 */
	private String idDocumento;
	
	/**
	 * Oggetto protocollo
	 */
	private String oggetto;
	
	/**
	 * Mittente protocollo
	 */
	private Contatto mittente;
	
	/**
	 * Destinatario protocollo 
	 */
	private String destinatario;
	
	/**
	 * NUMERO PROTOCOLLO
	 */
	private int numeroProtocollo;
	
	/**
	 * Identificativo protocollo
	 */
	private String idProtocollo;
	
	/**
	 * Anno protocollo
	 */
	private String annoProtocollo;
	
	/**
	 * Data protocollo
	 */
	private Date dataProtocollo;
	
	/**
	 * Assegnatari
	 */
	private String[] assegnatari;
	
	/**
	 * Numero prot anno protoollo
	 */
	private String numeroProtocolloAnnoProtocollo;
	
	/**
	 * Codice titolario
	 */
	private String codiceTitolario;
	
	/**
	 * Descrizione titolario
	 */
	private String descrizioneTitolario;
	
	/**
	 * Descrizione tipologia documento
	 */
	private String descrizioneTipologiaDocumento;
	
	/**
	 * Lista allegati
	 */
	private transient List<AllegatoNpsDTO> allegatiList;
	
	/**
	 * Lista allacci
	 */
	private transient List<AllaccioDTO> allacciList;
	
	/**
	 * Lista storico
	 */
	private transient List<StoricoDTO> storicoList;
	
	/**
	 * Lista ssegnatari
	 */
	private transient List<AssegnatariDTO> assegnatariList;
	
	/**
	 * Tipologia protocollo
	 */
	private String tipoProtocollo;
	
	/**
	 * Destinatari 
	 */
	private String destinatariString;
	
	/**
	 * Mittente
	 */
	private String mittenteString;
	
	/**
	 * Flag protocollo entrata
	 */
	private boolean entrataProtocollo;
	
	/**
	 * Costruttore documento nps DTO.
	 */
	private transient DocumentoNpsDTO documentoPrincipale;
	
	/**
	 * Stato protocollo
	 */
	private String statoProtocollo;
	
	/**
	 * Utente annullatore
	 */
	private String utenteAnnullatore;
	
	/**
	 * Motivo annullamento
	 */
	private String motivoAnnullamento;
	
	/**
	 * Provvedimento annullamento
	 */
	private String provvedimentoAnnullamento;
	
	/**
	 * Data annullamento
	 */
	private Date dataAnnullamento;
	
	/**
	 * Sistema produttore protocollo
	 */
	private String sistemaProduttore;
	
	/**
	 * Codice registro
	 */
	private String codiceRegistro;
	
	/**
	 * Codice aoo
	 */
	private String codiceAoo;
	
	/**
	 * Protocollatore
	 */
	private transient PersonaFisicaDTO protocollatore;
	
	/**
	 * Chiave esterna protocollatore
	 */
	private String chiaveEsternaProtocollatore;
	
	/**
	 * Metadati estesi tipologia documento
	 */
	private List<MetadatoDTO> metadatiEstesiTipologiaDoc;
	
	/**
	 * destinatari
	 */
	private List<Contatto> destinatari;
	
	/**
	 * data protocollo mittente.
	 */
	private Date dataProtocolloMittente;
	
	/**
	 * numero protocollo mittente.
	 */
	private String numeroProtocolloMittente;
	
	/**
	 * Costruttore protocollo nps DTO.
	 */
	public ProtocolloNpsDTO() {
		super();
	}
	
	/**
	 * Hash code.
	 *
	 * @return the int
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		
		result = prime * result + ((idProtocollo == null) ? 0 : idProtocollo.hashCode());
		
		return result;
	}
	
	/**
	 * Equals.
	 *
	 * @param obj the obj
	 * @return true, if successful
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		final ProtocolloNpsDTO other = (ProtocolloNpsDTO) obj;
		if (idProtocollo == null) {
			if (other.idProtocollo != null) {
				return false;
			}
		} else if (!idProtocollo.equals(other.idProtocollo)) {
			return false;
		}
		
		return true;
	}

	/** 
	 * @return the idDocumento
	 */
	public final String getIdDocumento() {
		return idDocumento;
	}

	/** 
	 * @param idDocumento the idDocumento to set
	 */
	public final void setIdDocumento(final String idDocumento) {
		this.idDocumento = idDocumento;
	}

	/** 
	 * @return the oggetto
	 */
	public final String getOggetto() {
		return oggetto;
	}

	/** 
	 * @param oggetto the oggetto to set
	 */
	public final void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/** 
	 * @return the mittente
	 */
	public final Contatto getMittente() {
		return mittente;
	}

	/** 
	 * @param mittente the mittente to set
	 */
	public final void setMittente(final Contatto mittente) {
		this.mittente = mittente;
	}

	/** 
	 * @return the destinatario
	 */
	public final String getDestinatario() {
		return destinatario;
	}

	/** 
	 * @param destinatario the destinatario to set
	 */
	public final void setDestinatario(final String destinatario) {
		this.destinatario = destinatario;
	}

	/** 
	 * @return the numeroProtocollo
	 */
	public final int getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/** 
	 * @param numeroProtocollo the numeroProtocollo to set
	 */
	public final void setNumeroProtocollo(final int numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	/** 
	 * @return the idProtocollo
	 */
	public final String getIdProtocollo() {
		return idProtocollo;
	}

	/** 
	 * @param idProtocollo the idProtocollo to set
	 */
	public final void setIdProtocollo(final String idProtocollo) {
		this.idProtocollo = idProtocollo;
	}

	/** 
	 * @return the annoProtocollo
	 */
	public final String getAnnoProtocollo() {
		return annoProtocollo;
	}

	/** 
	 * @param annoProtocollo the annoProtocollo to set
	 */
	public final void setAnnoProtocollo(final String annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/** 
	 * @return the dataProtocollo
	 */
	public final Date getDataProtocollo() {
		return dataProtocollo;
	}

	/** 
	 * @param dataProtocollo the dataProtocollo to set
	 */
	public final void setDataProtocollo(final Date dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}

	/** 
	 * @return the assegnatari
	 */
	public final String[] getAssegnatari() {
		return assegnatari;
	}

	/** 
	 * @param assegnatari the assegnatari to set
	 */
	public final void setAssegnatari(final String[] assegnatari) {
		this.assegnatari = assegnatari;
	}

	/** 
	 * @return the numeroProtocolloAnnoProtocollo
	 */
	public final String getNumeroProtocolloAnnoProtocollo() {
		return numeroProtocolloAnnoProtocollo;
	}

	/** 
	 * @param numeroProtocolloAnnoProtocollo the numeroProtocolloAnnoProtocollo to set
	 */
	public final void setNumeroProtocolloAnnoProtocollo(final String numeroProtocolloAnnoProtocollo) {
		this.numeroProtocolloAnnoProtocollo = numeroProtocolloAnnoProtocollo;
	}
	
	/** 
	 * @return the codice titolario
	 */
	public String getCodiceTitolario() {
		return codiceTitolario;
	}

	/** 
	 * @param codiceTitolario the new codice titolario
	 */
	public void setCodiceTitolario(final String codiceTitolario) {
		this.codiceTitolario = codiceTitolario;
	}

	/** 
	 * @return the descrizioneTitolario
	 */
	public final String getDescrizioneTitolario() {
		return descrizioneTitolario;
	}

	/** 
	 * @param descrizioneTitolario the descrizioneTitolario to set
	 */
	public final void setDescrizioneTitolario(final String descrizioneTitolario) {
		this.descrizioneTitolario = descrizioneTitolario;
	}

	/** 
	 * @return the descrizioneTipologiaDocumento
	 */
	public final String getDescrizioneTipologiaDocumento() {
		return descrizioneTipologiaDocumento;
	}

	/** 
	 * @param descrizioneTipologiaDocumento the descrizioneTipologiaDocumento to set
	 */
	public final void setDescrizioneTipologiaDocumento(final String descrizioneTipologiaDocumento) {
		this.descrizioneTipologiaDocumento = descrizioneTipologiaDocumento;
	}

	/** 
	 * @return the allegatiList
	 */
	public final List<AllegatoNpsDTO> getAllegatiList() {
		return allegatiList;
	}

	/** 
	 * @param allegatiList the allegatiList to set
	 */
	public final void setAllegatiList(final List<AllegatoNpsDTO> allegatiList) {
		this.allegatiList = allegatiList;
	}

	/** 
	 * @return the allacciList
	 */
	public final List<AllaccioDTO> getAllacciList() {
		return allacciList;
	}

	/** 
	 * @param allacciList the allacciList to set
	 */
	public final void setAllacciList(final List<AllaccioDTO> allacciList) {
		this.allacciList = allacciList;
	}

	/** 
	 * @return the storicoList
	 */
	public final List<StoricoDTO> getStoricoList() {
		return storicoList;
	}

	/** 
	 * @param storicoList the storicoList to set
	 */
	public final void setStoricoList(final List<StoricoDTO> storicoList) {
		this.storicoList = storicoList;
	}

	/** 
	 * @return the assegnatariList
	 */
	public final List<AssegnatariDTO> getAssegnatariList() {
		return assegnatariList;
	}

	/** 
	 * @param assegnatariList the assegnatariList to set
	 */
	public final void setAssegnatariList(final List<AssegnatariDTO> assegnatariList) {
		this.assegnatariList = assegnatariList;
	}

	/** 
	 * @return the tipoProtocollo
	 */
	public final String getTipoProtocollo() {
		return tipoProtocollo;
	}

	/** 
	 * @param tipoProtocollo the tipoProtocollo to set
	 */
	public final void setTipoProtocollo(final String tipoProtocollo) {
		this.tipoProtocollo = tipoProtocollo;
	}

	/** 
	 * @return the destinatariString
	 */
	public final String getDestinatariString() {
		return destinatariString;
	}

	/** 
	 * @param destinatariString the destinatariString to set
	 */
	public final void setDestinatariString(final String destinatariString) {
		this.destinatariString = destinatariString;
	}

	/** 
	 * @return the mittenteString
	 */
	public final String getMittenteString() {
		return mittenteString;
	}

	/** 
	 * @param mittenteString the mittenteString to set
	 */
	public final void setMittenteString(final String mittenteString) {
		this.mittenteString = mittenteString;
	}

	/** 
	 * @return the entrataProtocollo
	 */
	public final boolean isEntrataProtocollo() {
		return entrataProtocollo;
	}

	/** 
	 * @param entrataProtocollo the entrataProtocollo to set
	 */
	public final void setEntrataProtocollo(final boolean entrataProtocollo) {
		this.entrataProtocollo = entrataProtocollo;
	}

	/** 
	 * @return the documentoPrincipale
	 */
	public final DocumentoNpsDTO getDocumentoPrincipale() {
		return documentoPrincipale;
	}

	/** 
	 * @param documentoPrincipale the documentoPrincipale to set
	 */
	public final void setDocumentoPrincipale(final DocumentoNpsDTO documentoPrincipale) {
		this.documentoPrincipale = documentoPrincipale;
	}

	/** 
	 * @return the statoProtocollo
	 */
	public final String getStatoProtocollo() {
		return statoProtocollo;
	}

	/** 
	 * @param statoProtocollo the statoProtocollo to set
	 */
	public final void setStatoProtocollo(final String statoProtocollo) {
		this.statoProtocollo = statoProtocollo;
	}

	/** 
	 * @return the utenteAnnullatore
	 */
	public final String getUtenteAnnullatore() {
		return utenteAnnullatore;
	}

	/** 
	 * @param utenteAnnullatore the utenteAnnullatore to set
	 */
	public final void setUtenteAnnullatore(final String utenteAnnullatore) {
		this.utenteAnnullatore = utenteAnnullatore;
	}

	/** 
	 * @return the motivoAnnullamento
	 */
	public final String getMotivoAnnullamento() {
		return motivoAnnullamento;
	}

	/** 
	 * @param motivoAnnullamento the motivoAnnullamento to set
	 */
	public final void setMotivoAnnullamento(final String motivoAnnullamento) {
		this.motivoAnnullamento = motivoAnnullamento;
	}

	/** 
	 * @return the provvedimentoAnnullamento
	 */
	public final String getProvvedimentoAnnullamento() {
		return provvedimentoAnnullamento;
	}

	/** 
	 * @param provvedimentoAnnullamento the provvedimentoAnnullamento to set
	 */
	public final void setProvvedimentoAnnullamento(final String provvedimentoAnnullamento) {
		this.provvedimentoAnnullamento = provvedimentoAnnullamento;
	}

	/** 
	 * @return the dataAnnullamento
	 */
	public final Date getDataAnnullamento() {
		return dataAnnullamento;
	}

	/** 
	 * @param dataAnnullamento the dataAnnullamento to set
	 */
	public final void setDataAnnullamento(final Date dataAnnullamento) {
		this.dataAnnullamento = dataAnnullamento;
	}
	
	/** 
	 * @return the sistemaProduttore
	 */
	public String getSistemaProduttore() {
		return sistemaProduttore;
	}

	/** 
	 * @param sistemaProduttore the sistemaProduttore to set
	 */
	public void setSistemaProduttore(final String sistemaProduttore) {
		this.sistemaProduttore = sistemaProduttore;
	}

	/** 
	 * @return the codiceRegistro
	 */
	public String getCodiceRegistro() {
		return codiceRegistro;
	}

	/** 
	 * @param codiceRegistro the codiceRegistro to set
	 */
	public void setCodiceRegistro(final String codiceRegistro) {
		this.codiceRegistro = codiceRegistro;
	}

	/** 
	 * @return the codiceAoo
	 */
	public String getCodiceAoo() {
		return codiceAoo;
	}

	/** 
	 * @param codiceAoo the codiceAoo to set
	 */
	public void setCodiceAoo(final String codiceAoo) {
		this.codiceAoo = codiceAoo;
	}

	/** 
	 * @return the protocollatore
	 */
	public PersonaFisicaDTO getProtocollatore() {
		return protocollatore;
	}

	/** 
	 * @param protocollatore the protocollatore to set
	 */
	public void setProtocollatore(final PersonaFisicaDTO protocollatore) {
		this.protocollatore = protocollatore;
	}
	
	/** 
	 * @return the chiave esterna protocollatore
	 */
	public String getChiaveEsternaProtocollatore() {
		return chiaveEsternaProtocollatore;
	}

	/** 
	 * @param chiaveEsternaProtocollatore the new chiave esterna protocollatore
	 */
	public void setChiaveEsternaProtocollatore(final String chiaveEsternaProtocollatore) {
		this.chiaveEsternaProtocollatore = chiaveEsternaProtocollatore;
	}
	
	/** 
	 * @return the metadatiEstesiTipologiaDoc
	 */
	public List<MetadatoDTO> getMetadatiEstesiTipologiaDoc() {
		return metadatiEstesiTipologiaDoc;
	}

	/** 
	 * @param metadatiEstesiTipologiaDoc the metadatiEstesiTipologiaDoc to set
	 */
	public void setMetadatiEstesiTipologiaDoc(final List<MetadatoDTO> metadatiEstesiTipologiaDoc) {
		this.metadatiEstesiTipologiaDoc = metadatiEstesiTipologiaDoc;
	}
	
	/** 
	 * @return the destinatari
	 */
	public List<Contatto> getDestinatari() {
		return destinatari;
	}

	/** 
	 * @param destinatari the new destinatari
	 */
	public void setDestinatari(final List<Contatto> destinatari) {
		this.destinatari = destinatari;
	}

	/**
	 * 
	 * @return dataProtocolloMittente
	 */
	public Date getDataProtocolloMittente() {
		return dataProtocolloMittente;
	}

	/**
	 * 
	 * @param dataProtocolloMittente
	 */
	public void setDataProtocolloMittente(Date dataProtocolloMittente) {
		this.dataProtocolloMittente = dataProtocolloMittente;
	}

	/**
	 * 
	 * @return numeroProtocolloMittente
	 */
	public String getNumeroProtocolloMittente() {
		return numeroProtocolloMittente;
	}

	/**
	 * 
	 * @param numeroProtocolloMittente
	 */
	public void setNumeroProtocolloMittente(String numeroProtocolloMittente) {
		this.numeroProtocolloMittente = numeroProtocolloMittente;
	}
	
}