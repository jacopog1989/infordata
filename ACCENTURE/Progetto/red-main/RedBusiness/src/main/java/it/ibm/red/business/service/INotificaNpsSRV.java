package it.ibm.red.business.service;

import com.filenet.api.core.Document;

import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.service.facade.INotificaNpsFacadeSRV;

/**
 * Interfaccia del servizio di notifica NPS.
 */
public interface INotificaNpsSRV extends INotificaNpsFacadeSRV {

	/**
	 * Recupera il documento filenet identificato dall'id protocollo se non null.
	 * @param idProtocollo
	 * @param numeroProtocollo
	 * @param annoProtocollo
	 * @param tipoProtocollo
	 * @param fceh
	 * @param idAoo
	 * @return Document da filenet
	 */
	Document getDocFileNetByProtocollo(final String idProtocollo, final Integer numeroProtocollo, final Integer annoProtocollo, final int tipoProtocollo,
			final IFilenetCEHelper fceh, final int idAoo);

}
