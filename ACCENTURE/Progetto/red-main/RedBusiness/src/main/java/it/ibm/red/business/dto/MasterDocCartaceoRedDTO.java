package it.ibm.red.business.dto;

import java.util.Date;

/**
 * DTO che definisce le informazioni di un master di documento cartaceo.
 */
public class MasterDocCartaceoRedDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 6776030621200544567L;

	/**
	 * Document title.
	 */
	private final String documentTitle;
	
	/**
	 * Guid.
	 */
	private final String guid;
	
	/**
	 * Data creazione.
	 */
	private final Date dataCreazione;
	
	/**
	 * Barcode.
	 */
	private final String barcode;
	
	/**
	 * Barcode principale.
	 */
	private final String barcodePrincipale;
	
	/**
	 * Stoato recogniform.
	 */
	private final String statoRecogniform;
	
	/**
	 * Mime type.
	 */
	private final String mimeType;
	
	/**
	 * Principale guid.
	 */
	private final String guidPrincipale;
	
	/**
	 * Flag selezionato come allegato.
	 */
	private boolean selectedAsAllegato;

	
	/**
	 * @param documentTitle
	 * @param dataCreazione
	 * @param barcode
	 * @param barcodePrincipale
	 * @param statoRecogniform
	 * @param guidPrincipale
	 */
	public MasterDocCartaceoRedDTO(final String documentTitle, final String guid, final Date dataCreazione, final String barcode, final String barcodePrincipale, final String statoRecogniform, 
			final String mimeType, final String guidPrincipale) {
		super();
		this.documentTitle = documentTitle;
		this.guid = guid;
		this.dataCreazione = dataCreazione;
		this.barcode = barcode;
		this.barcodePrincipale = barcodePrincipale;
		this.statoRecogniform = statoRecogniform;
		this.mimeType = mimeType;
		this.guidPrincipale = guidPrincipale;
	}

	/**
	 * @return the documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}
	
	/**
	 * @return the guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * @return the dataCreazione
	 */
	public Date getDataCreazione() {
		return dataCreazione;
	}

	/**
	 * @return the barcode
	 */
	public String getBarcode() {
		return barcode;
	}

	/**
	 * @return the barcodePrincipale
	 */
	public String getBarcodePrincipale() {
		return barcodePrincipale;
	}

	/**
	 * @return the statoRecogniform
	 */
	public String getStatoRecogniform() {
		return statoRecogniform;
	}

	/**
	 * @return the guidPrincipale
	 */
	public String getGuidPrincipale() {
		return guidPrincipale;
	}
	
	/**
	 * @return the mimeType
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * @return the selectedAsAllegato
	 */
	public boolean isSelectedAsAllegato() {
		return selectedAsAllegato;
	}

	/**
	 * @param selectedAsAllegato the selectedAsAllegato to set
	 */
	public void setSelectedAsAllegato(final boolean selectedAsAllegato) {
		this.selectedAsAllegato = selectedAsAllegato;
	}
	
}