package it.ibm.red.business.enums;

/**
 * Enum che definisce le visualizzazioni degli organigrammi.
 */
public enum OrganigrammaVisualizzazioneCompetenzaIngressoEnum {
	
	/**
	 * Visualizzazione organigramma standard.
	 */
	STANDARD,
	
	/**
	 * Visualizzazione con utenti.
	 */
	CON_UTENTI,
	
	/**
	 * Visualizzazione senza utenti.
	 */
	SENZA_UTENTI;
}
