package it.ibm.red.business.enums;

/**
 * The Enum TipoProtocolloEnum.
 *
 * @author CPIERASC
 * 
 *         Enum dei possibili tipi di protocollo per AOO.
 */
public enum TipologiaProtocollazioneEnum {
	
	/**
	 * MEF.
	 */
	MEF(1L),

	/**
	 * NPS.
	 */
	NPS(2L),
	
	/**
	 * EMERGENZANPS.
	 */
	EMERGENZANPS(-2L);

	
	/**
	 * Identificativo azione.
	 */
	private Long id;
	
	
	/**
	 * Costruttore.
	 * 
	 * @param inId			identificativo
	 */
	TipologiaProtocollazioneEnum(final Long inId) {
		this.id = inId;
	}


	/**
	 * Getter identificativo.
	 * 
	 * @return	identificativo
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Verifica che la tipologia in input sia relativa ad NPS o ad NPS in regime di emergenza.
	 * 
	 * @param idTipologiaProtocollazione
	 * @return
	 */
	public static boolean isProtocolloNPS(final Long idTipologiaProtocollazione) {
		return TipologiaProtocollazioneEnum.NPS.getId().equals(idTipologiaProtocollazione) 
				|| TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(idTipologiaProtocollazione);
	}
}