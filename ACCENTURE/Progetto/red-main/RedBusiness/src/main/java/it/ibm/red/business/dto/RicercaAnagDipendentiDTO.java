package it.ibm.red.business.dto;

/**
 * DTO delle informazioni di ricerca anagrafica dipendente.
 */
public class RicercaAnagDipendentiDTO extends AbstractDTO {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = 6147765027397840158L;

	/**
	 * Nome anagrafica ricerca.
	 */
	private String nome;

	/**
	 * Cognome anagrafica ricerca.
	 */
	private String cognome;

	/**
	 * Codice fiscale anagrafica ricerca.
	 */
	private String codiceFiscale;
	
	/**
	 * Costruttore.
	 * 
	 */
	public RicercaAnagDipendentiDTO() {
		super();
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param nome
	 * @param cognome
	 * @param codiceFiscale
	 */
	public RicercaAnagDipendentiDTO(String nome, String cognome, String codiceFiscale) {
		this();
		this.nome = nome;
		this.cognome = cognome;
		this.codiceFiscale = codiceFiscale;
	}

	/**
	 * Restituisce il nome dell'anagrafica da ricercare.
	 * @return nome ricerca
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Imposta il nome dell'anagrafica da ricercare.
	 * @param nome da ricercare
	 */
	public void setNome(final String nome) {
		this.nome = nome;
	}

	/**
	 * Restituisce il cognome dell'anagrafica da ricercare.
	 * @return cognome ricerca
	 */
	public String getCognome() {
		return cognome;
	}

	/**
	 * Imposta il cognome dell'anagrafica da ricercare.
	 * @param cognome da ricercare 
	 */
	public void setCognome(final String cognome) {
		this.cognome = cognome;
	}

	/**
	 * Restituisce il codice fiscale dell'anagrafica da ricercare.
	 * @return codice fiscale ricerca
	 */
	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	/**
	 * Imposta il codice fiscale dell'anagrafica da ricercare.
	 * @param codice fiscale da ricercare
	 */
	public void setCodiceFiscale(final String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

}
