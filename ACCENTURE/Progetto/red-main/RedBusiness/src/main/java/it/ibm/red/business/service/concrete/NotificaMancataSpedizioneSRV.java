package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList; 
import java.util.List;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
   
import it.ibm.red.business.dto.EventoSottoscrizioneDTO;
import it.ibm.red.business.dto.NotificaEmailDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.exception.RedException; 
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo; 
import it.ibm.red.business.service.INotificaMancataSpedizioneSRV;
import it.ibm.red.business.service.ISottoscrizioniSRV;
import it.ibm.red.business.service.IStoricoSRV; 

/**
 * Service notifica mancata spedizione.
 */
@Service
@Component
public class NotificaMancataSpedizioneSRV extends NotificaWriteAbstractSRV implements INotificaMancataSpedizioneSRV{

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = 6633628470610226020L;
	
	/**
	 * LOGGER.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotificaMancataSpedizioneSRV.class.getName());
	
	/**
	 * Service.
	 */
	@Autowired
	private ISottoscrizioniSRV sottoscrizioniSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IStoricoSRV storicoSRV;

	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getSottoscrizioni(int).
	 */
	@Override
	protected List<EventoSottoscrizioneDTO> getSottoscrizioni(int idAoo) {
		Connection con = null;
		List<EventoSottoscrizioneDTO> eventi = null;
		try {
			Integer[] idEventi = new Integer[]{
					Integer.parseInt(EventTypeEnum.NOTIFICA_MANCATA_SPEDIZIONE.getValue())};
		
			con = setupConnection(getFilenetDataSource().getConnection(), false);
			eventi = sottoscrizioniSRV.getEventiSottoscrittiByListaEventi(idAoo, idEventi, con);
			
		} catch (Exception e) {
			LOGGER.error("Errore in fase recupero delle sottoscrizioni per l'evento di mancata spedizione per l'aoo " + idAoo, e);
		} finally {
			closeConnection(con);
		}
		return eventi;
	}

	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getDataDocumenti(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.persistence.model.Aoo, int).
	 */
	@Override
	protected List<InfoDocumenti> getDataDocumenti(UtenteDTO utente, Aoo aoo, int idEvento) {
		throw new RedException("Metodo non previsto per il servizio ");
	}

	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getDataDocumenti(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.persistence.model.Aoo, int, java.lang.Object).
	 */
	@Override
	protected List<InfoDocumenti> getDataDocumenti(UtenteDTO utente, Aoo aoo, int idEvento, Object objectForGetDataDocument) {
		List<InfoDocumenti> documenti = new ArrayList<>();
 		try {
 			
 			NotificaEmailDTO notifica = (NotificaEmailDTO) objectForGetDataDocument;
			
 			if (notifica != null) {
 				boolean inviaNotifica = storicoSRV.isDocLavoratoDaUfficio(utente.getIdAoo(),notifica.getIdDocumento(), utente.getIdUfficio());
 	 			if(inviaNotifica) {
 		 			InfoDocumenti info = new InfoDocumenti();
 		 			info.setIdDocumento(Integer.parseInt(notifica.getIdDocumento()));
 		 			documenti.add(info);
 	 			} 
 			}
 		} catch (Exception e) {
			LOGGER.error("Errore in fase recupero info notifica NPS", e);
		}
		return documenti;
	}

}
