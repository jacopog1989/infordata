package it.ibm.red.business.service;

import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.service.facade.IInvioInFirmaFacadeSRV;

/**
 * Interface del servizio che gestisce la funzionalità di invio in firma.
 */
public interface IInvioInFirmaSRV extends IInvioInFirmaFacadeSRV {
	
	/**
	 * Per ogni workflow nella lista dei wibNumber, modifica la sicerezza dei fascicoli el documenti e dei documenti allacciati e dei contributi inseriti.
	 * 
	 * Avanza il workflow prima di cambiare la sicurezza dei documenti allacciata e inseriti ma dopo aver cambiato la sicurezza dei fascicoli e del documento
	 * 
	 * @param utente Propietario delle credenziali di accesso a FileNet
	 * @param wobNumbers Lista di wobNumber ciascuno legato a un workflow
	 * @param idNodoDestinatarioNew Ufficio del nuovo destinatario del workflow
	 * @param idUtenteDestinatarioNew id dell'utente destinatario
	 * @param motivazione motivazione aggiunta dall'utente per l'invio 
	 * @param tipoResponse 
	 * 	Response invocata dall'utente che esegue questa operazione
	 * @param urgente
	 * 	Si tratta della rappresentazione 1: true 0:false di un check booleano per verificare se il comando è urgente o no
	 * @return
	 */
	List<EsitoOperazioneDTO> invioInFirmaSiglaVisto(UtenteDTO utente, Collection<String> wobNumbers, Long idNodoDestinatarioNew,
			Long idUtenteDestinatarioNew, String motivazione, ResponsesRedEnum tipoResponse, Integer urgente);
}
