package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IReportFacadeSRV;

/**
 * The Interface IReportSRV.
 *
 * @author m.crescentini
 * 
 *         Interfaccia del servizio per la gestione dei report.
 */
public interface IReportSRV extends IReportFacadeSRV {

	
}