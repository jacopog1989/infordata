package it.ibm.red.business.dto;

/**
 * DTO per la gestione degli attributi estesi di un ATTO DECRETO.
 */
public class AttributiEstesiAttoDecretoDTO extends AbstractDTO {

	/**
	 * UID.
	 */
	private static final long serialVersionUID = -7665313589465813914L;
	
	/**
	 * Codice amministrazione.
	 */
	private String codAmministrazione;
	
	/**
	 * Codice ragioneria.
	 */
	private String codRagioneria;
	
	/**
	 * Ufficio segreteria.
	 */
	private String uffSegreteria;

	/**
	 * Restituisce codice amminstrazione.
	 * @return codAmministrazione
	 */
	public String getCodAmministrazione() {
		return codAmministrazione;
	}

	/**
	 * Imposta il codice amministrazione.
	 * @param codAmministrazione
	 */
	public void setCodAmministrazione(final String codAmministrazione) {
		this.codAmministrazione = codAmministrazione;
	}

	/**
	 * Restituisce il codice ragioneria.
	 * @return codRagioneria
	 */
	public String getCodRagioneria() {
		return codRagioneria;
	}

	/**
	 * Imposta il codice ragioneria.
	 * @param codRagioneria
	 */
	public void setCodRagioneria(final String codRagioneria) {
		this.codRagioneria = codRagioneria;
	}

	/**
	 * Restituisce il codice segreteria.
	 * @return uffSegreteria
	 */
	public String getUffSegreteria() {
		return uffSegreteria;
	}

	/**
	 * Imposta il codice dell'ufficio di segreteria.
	 * @param uffSegreteria
	 */
	public void setUffSegreteria(final String uffSegreteria) {
		this.uffSegreteria = uffSegreteria;
	}

}
