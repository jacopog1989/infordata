package it.ibm.red.business.service;

import java.sql.Connection;

import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.service.facade.ILookupFacadeSRV;

/**
 * The Interface ILookupSRV.
 *
 * @author CPIERASC
 * 
 *         Interfaccia servizio lookup
 */
public interface ILookupSRV extends ILookupFacadeSRV {

	/**
	 * Recupero ufficio per id.
	 * 
	 * @param id	identificativo ufficio
	 * @param conn	sql connection
	 * @return		ufficio
	 */
	Nodo getUfficio(Long id, Connection conn);
	
}
