package it.ibm.red.business.dto;

/**
 * The Class DestinatarioDTO.
 *
 * @author CPIERASC
 * 
 * 	Data Transfer Object per i dati sintetici di un destinatario.
 */
public class DestinatarioDTO extends AbstractDTO {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Tipologia.
	 */
	private String tipo;

	/**
	 * Provenienza.
	 */
	private String provenienza;

	/**
	 * Identificativo.
	 */
	private Long id;
	
	/**
	 * OPZIONALE - Nel caso del trasformer da entità Document -> Detail 
	 * questo dato verrà valorizzato con l'id ufficio.
	 * 
	 * Per il momento verrà sfruttato solo nella Protocollazione in Uscita
	 * e per la gestione dei Destinatari.
	 */
	private Long idUfficio;

	/**
	 * Tipologia spedizione.
	 */
	private Integer tipoSpedizione;

	/**
	 * Account.
	 */
	private String account;
	
	/**
	 * Mail.
	 */
	private String mail;
	
	/**
	 * Mail PEC.
	 */
	private String mailPec;
	
	/**
	 * Destinatario in TO o CC.
	 */
	private Boolean isCC;
	

	/**
	 * Costruttore.
	 * 
	 * @param inId				identificativo
	 * @param inDescrizione		descrizione
	 * @param inTipo			tipologia
	 * @param inProvenienza		provenienza
	 * @param inAccount			account
	 * @param inTipoSpedizione	tipologia spedizione
	 */
	public DestinatarioDTO(final Long inId, final String inDescrizione, final String inTipo, final String inProvenienza, 
			final String inAccount, final Integer inTipoSpedizione, final Boolean inIsCC, final Long inIdUfficio) {
		super();
		this.descrizione = inDescrizione;
		this.tipo = inTipo;
		this.provenienza = inProvenienza;
		this.id = inId;
		this.account = inAccount;
		this.tipoSpedizione = inTipoSpedizione;
		this.isCC = inIsCC;
		this.idUfficio = inIdUfficio;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	account
	 */
	public final String getAccount() {
		return account;
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo
	 */
	public final Long getId() {
		return id;
	}

	/**
	 * Getter.
	 * 
	 * @return	descrizione
	 */
	public final String getDescrizione() {
		return descrizione;
	}

	/**
	 * Getter.
	 * 
	 * @return	tipologia
	 */
	public final String getTipo() {
		return tipo;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	provenienza
	 */
	public final String getProvenienza() {
		return provenienza;
	}

	/**
	 * Getter.
	 * 
	 * @return	tipologia spedizione
	 */
	public final Integer getTipoSpedizione() {
		return tipoSpedizione;
	}

	/**
	 * Getter.
	 * 
	 * @return mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * Getter.
	 * 
	 * @return mail PEC
	 */
	public String getMailPec() {
		return mailPec;
	}

	/**
	 * Getter.
	 * 
	 * @return destinatario TO/CC
	 */
	public Boolean getIsCC() {
		return isCC;
	}

	/**
	 * @return the idUfficio
	 */
	public Long getIdUfficio() {
		return idUfficio;
	}
	
}
