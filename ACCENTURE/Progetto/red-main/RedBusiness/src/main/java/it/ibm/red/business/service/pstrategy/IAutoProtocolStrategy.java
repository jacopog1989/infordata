package it.ibm.red.business.service.pstrategy;

import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.MessaggioPostaNpsDTO;
import it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO;

/**
 * Interfaccia della Strategy di protocollazione automatica.
 * @param <T>
 */
public interface IAutoProtocolStrategy<T extends MessaggioPostaNpsDTO> {

	/**
	 * @param richiestaElabMessaggio
	 * @param guidMessaggio
	 * @return
	 */
	EsitoSalvaDocumentoDTO protocol(RichiestaElabMessaggioPostaNpsDTO<T> richiestaElabMessaggio, String guidMessaggio);
	
}
