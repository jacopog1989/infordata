package it.ibm.red.business.service.flusso;

import java.io.Serializable;

/**
 * Interfaccia del servizio di gestione azioni flusso.
 */
public interface IAzioniFlussoSRV extends Serializable {

	/**
	 * Recupera le azioni relative al flusso identificato dal codice <code> codiceFlusso </code>.
	 * 
	 * @param codiceFlusso
	 *            codice flusso del flusso di riferimento
	 * @return IAzioniFlussoBaseSRV service per la gestione delle azioni flusso
	 */
	IAzioniFlussoBaseSRV retrieveAzioniFlussoSRV(String codiceFlusso);
	
}
