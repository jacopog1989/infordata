package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;

import it.ibm.red.business.persistence.model.RedWsClient;

/**
 * 
 * @author m.crescentini
 *
 *	DAO gestione client web service Red EVO.
 */
public interface IRedWsClientDAO extends Serializable {
	
	/**
	 * Recupero RedWsClient da id.
	 * 
	 * @param idClient		identificativo client
	 * @param connection	connessione
	 * @return				RedWsClient
	 */
	RedWsClient getById(String idClient, Connection connection);

}
