package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.AnagraficaDipendenteDTO;
import it.ibm.red.business.dto.AnagraficaDipendentiComponentDTO;
import it.ibm.red.business.dto.AssegnatarioDTO;
import it.ibm.red.business.dto.AssegnazioneAutomaticaCapSpesaDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.AssegnazioneMetadatiDTO;
import it.ibm.red.business.dto.CapitoloSpesaMetadatoDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAssegnazioneAutomaticaCapSpesaSRV;
import it.ibm.red.business.service.IAssegnazioneAutomaticaMetadatiSRV;
import it.ibm.red.business.service.IAssegnazioneAutomaticaSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * Implementazione del service per la gestione delle assegnazioni automatiche
 * esistenti su un documento
 * 
 * @author SimoneLungarella
 */
@Service
@Component
public class AssegnazioneAutomaticaSRV extends AbstractService implements IAssegnazioneAutomaticaSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -6671144507188627926L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AssegnazioneAutomaticaSRV.class.getName());

	/**
	 * Servizio assegnazione automatica per capitoli di spesa.
	 */
	@Autowired
	private IAssegnazioneAutomaticaCapSpesaSRV assAutomaticaCapitoliSpesaSRV;

	/**
	 * Servizio assegnazione automatica per metadati.
	 */
	@Autowired
	private IAssegnazioneAutomaticaMetadatiSRV assAutomaticaMetadatiSRV;

	/**
	 * Servizio utente.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * DAO utente.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * Dao nodo.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Restituisce true se l' <code> assegnatario </code> è un'assegnazione che si
	 * riferisce ad un utente o ufficio.
	 * 
	 * @param assegnatario
	 *            DTO su cui verificare la validità
	 * @return true se valido, false altrimenti
	 */
	private static Boolean assegnatarioFound(final AssegnatarioDTO assegnatario) {
		Boolean output = false;
		if (assegnatario != null && assegnatario.getIdUfficio() != null) {
			output = true;
		}
		return output;
	}

	/**
	 * Restituisce l'assegnatario recuperato se esiste una regola di assegnazione
	 * automatica sul capitolo di spesa identificato dal
	 * <code> codiceCapSpesa </code>, se non esiste alcuna regola verrà restituito
	 * <code> null </code>.
	 * 
	 * @param idAoo
	 *            identificativo dell'area organizzativa per la quale esiste la
	 *            regola eventualmente
	 * @param codiceCapSpesa
	 *            codice del capitolo di spesa per la quale occorre recuperare
	 *            l'assegnatario dell'eventuale regola di assegnazione automatica
	 * @param metadati
	 *            metadati da analizzare per verificare l'esistenza di una regola di
	 *            assegnazione automatica
	 * @param connection
	 *            connessione al database
	 * @return assegnatario se recuperato da una regola di assegnazione automatica
	 *         per capitolo di spesa, <code> null </code> altrimenti
	 */
	private AssegnatarioDTO getAssegnatarioPerCapitoliSpesa(final Long idAoo, final String codiceCapSpesa,
			final Collection<MetadatoDTO> metadati, final Connection connection) {
		AssegnatarioDTO output = null;
		AssegnazioneAutomaticaCapSpesaDTO capSpesaDTO = new AssegnazioneAutomaticaCapSpesaDTO();
		if (!StringUtils.isNullOrEmpty(codiceCapSpesa)) {
			capSpesaDTO = assAutomaticaCapitoliSpesaSRV.get(codiceCapSpesa, idAoo, connection);
		}

		if (capSpesaDTO != null && capSpesaDTO.getAssegnatario() != null) {
			for (final MetadatoDTO metadato : metadati) {
				if (TipoMetadatoEnum.CAPITOLI_SELECTOR.equals(metadato.getType())) {
					final CapitoloSpesaMetadatoDTO metadatoCapitoloSpesa = ((CapitoloSpesaMetadatoDTO) metadato);

					if (metadatoCapitoloSpesa.getValue4AttrExt() != null
							&& metadatoCapitoloSpesa.getCapitoloSelected()
									.equals(capSpesaDTO.getCapitoloSpesa().getCodice())
							&& capSpesaDTO.getAssegnatario().getIdUfficio() != null
							&& !capSpesaDTO.getAssegnatario().getIdUfficio().equals(0L)) {

						output = new AssegnatarioDTO(capSpesaDTO.getAssegnatario());
						break;
					}
				}
			}
		}
		return output;
	}

	/**
	 * Restituisce l'assegnatario stabilito dall'assegnazione automatica per
	 * metadati se esistente. Il metodo consente di ricerca l'assegnazione
	 * automatica per il metadato in quanto metadato o per uno specifico metadato
	 * che ha uno specifico valore, questa possibilità è gestibile attraverso il
	 * parametro <code> onlyExist </code>. Se il parametro è <code> true </code>
	 * viene ricercata l'assegnazione automatica non valutando la valorizzazione
	 * dello stesso, altrimenti l'assegnazione automatica esiste se e solo se esiste
	 * la regola che grava sul metadato e sul valore specifico dello stesso.
	 * 
	 * @param onlyExist
	 *            consente di specificare il tipo di assegnazione automatica
	 *            ricercato, se <code> true </code> indica la necessità di mera
	 *            esistenza del metadato, se <code> false </code> occorre analizzare
	 *            anche il valore del metadato stesso
	 * @param assegnazioni
	 *            le assegnazioni automatiche esistenti per l'area organizzativa
	 *            sulle quali occorre ricerca l'assegnatario associato ad
	 *            un'eventuale assegnazione automatica
	 * @param metadati
	 *            i metadati da analizzare per la ricerca di un riscontro di
	 *            un'assegnazione automatica
	 * @return assegnatario stabilito dall'assegnazione automatica se esistente,
	 *         <code> null </code> altrimenti
	 */
	private AssegnatarioDTO getAssegnatarioPerMetadati(final Boolean onlyExist,
			final List<AssegnazioneMetadatiDTO> assegnazioni, final Collection<MetadatoDTO> metadati) {
		AssegnatarioDTO assegnatario = null;
		for (final AssegnazioneMetadatiDTO ass : assegnazioni) {

			// Scarto l'assegnazione automatica se non compatibile col tipo di controllo
			// (presenza/valore specifico) che sto eseguendo
			if (ass.getMetadato() == null
					|| (Boolean.FALSE.equals(onlyExist)
							&& StringUtils.isNullOrEmpty(ass.getMetadato().getValue4AttrExt()))
					|| (onlyExist && !StringUtils.isNullOrEmpty(ass.getMetadato().getValue4AttrExt()))) {
				continue;
			}

			if (TipoMetadatoEnum.LOOKUP_TABLE.equals(ass.getMetadato().getType())) {
				for (final MetadatoDTO m : metadati) {
					// Scorro tutti i metadati forniti alla ricerca di un match per nome e tipo
					if (TipoMetadatoEnum.LOOKUP_TABLE.equals(m.getType())
							&& ass.getMetadato().getName().equals(m.getName())
							&& ((Boolean.TRUE.equals(onlyExist) && !StringUtils.isNullOrEmpty(m.getValue4AttrExt())) // Mera esistenza
									|| (Boolean.FALSE.equals(onlyExist) && ass.getMetadato().getValue4AttrExt().equals(m.getValue4AttrExt())))) {// Automatismo per valorizzazione metadato
						assegnatario = ass.getAssegnatario();
						break;
					}
				}
			} else if (TipoMetadatoEnum.PERSONE_SELECTOR.equals(ass.getMetadato().getType())) {
				// Scorro tutti i metadati forniti alla ricerca di un match per nome e tipo
				for (final MetadatoDTO m : metadati) {
					if (TipoMetadatoEnum.PERSONE_SELECTOR.equals(m.getType())
							&& ass.getMetadato().getName().equals(m.getName())) {
						if (Boolean.TRUE.equals(onlyExist)) {
							// Controllo di mera esistenza
							if (CollectionUtils.isEmpty(((AnagraficaDipendentiComponentDTO) m).getSelectedValues())) {
								assegnatario = ass.getAssegnatario();
								break;
							}
						} else {
							// Controllo di valore esatto
							// NB: non può essere fatto utilizzando getValue4AttrExt perché il metadato del
							// documento non è stato ancora serializzato quindi contiene più informazioni
							// del necessario per verificare l'uguaglianza
							if (!CollectionUtils.isEmpty(((AnagraficaDipendentiComponentDTO) m).getSelectedValues())
									&& !CollectionUtils.isEmpty(
											((AnagraficaDipendentiComponentDTO) ass.getMetadato()).getSelectedValues())
									&& ((AnagraficaDipendentiComponentDTO) m).getSelectedValues()
											.size() == ((AnagraficaDipendentiComponentDTO) ass.getMetadato())
													.getSelectedValues().size()) {
								int matches = 0;
								for (final AnagraficaDipendenteDTO anagrafica : ((AnagraficaDipendentiComponentDTO) ass
										.getMetadato()).getSelectedValues()) {
									for (final AnagraficaDipendenteDTO anagraficaDoc : ((AnagraficaDipendentiComponentDTO) m)
											.getSelectedValues()) {
										if (anagrafica.getId().equals(anagraficaDoc.getId())) {
											matches++;
											break;
										}
									}
								}
								if (matches == (((AnagraficaDipendentiComponentDTO) ass.getMetadato())
										.getSelectedValues()).size()) {
									assegnatario = new AssegnatarioDTO(ass.getAssegnatario());
									break;
								}
							}
						}
					}
				}
			} else if (TipoMetadatoEnum.CAPITOLI_SELECTOR.equals(ass.getMetadato().getType())) {
				// Scorro tutti i metadati forniti alla ricerca di un match per nome e tipo
				for (final MetadatoDTO m : metadati) {
					if (TipoMetadatoEnum.CAPITOLI_SELECTOR.equals(m.getType())
							&& ass.getMetadato().getName().equals(m.getName())) {
						if (Boolean.TRUE.equals(onlyExist)) {
							// Controllo di mera esistenza
							if (!StringUtils.isNullOrEmpty(m.getValue4AttrExt())) {
								assegnatario = ass.getAssegnatario();
								break;
							}
						} else {
							// Controllo di valore esatto
							if (ass.getMetadato().getValue4AttrExt().equals(m.getValue4AttrExt())) {
								assegnatario = ass.getAssegnatario();
								break;
							}
						}
					}
				}
			} else {
				for (final MetadatoDTO m : metadati) {
					// Scorro tutti i metadati forniti alla ricerca di un match per nome, non
					// controllo il tipo considerando tutti quelli semplici
					if (Boolean.TRUE.equals(onlyExist)) {
						// Controllo di mera esistenza
						if (ass.getMetadato().getName().equals(m.getName())
								&& !StringUtils.isNullOrEmpty(m.getValue4AttrExt())) {
							assegnatario = ass.getAssegnatario();
							break;
						}
					} else {
						// Controllo di valore esatto
						if (ass.getMetadato().getValue4AttrExt().equals(m.getValue4AttrExt())) {
							assegnatario = ass.getAssegnatario();
							break;
						}
					}
				}
			}
			if (Boolean.TRUE.equals(assegnatarioFound(assegnatario))) {
				break;
			}
		}
		return assegnatario;
	}

	/**
	 * Restituisce l'assegnatario associato ad un'eventuale assegnazione automatica
	 * esistente sulla tipologia documentale specificata da
	 * <code> idTipoDocumento </code>. E' possibile specificare l'assegnazione
	 * automatica sulla coppia tipo documento / tipo procedimento valorizzando la
	 * tipologia procedimento <code> idTipoProcedimento </code>.
	 * La differenza delle assegnazioni automatiche può essere specificata attraverso il parametro <code> onlyTipoDoc </code>.
	 * Se il parametro viene impostato a <code> true </code> l'assegnazione
	 * automatica ricerca si ferma sulla valutazione della tipologia del documento,
	 * altrimenti l'assegnazione automatica è valida per la coppia se e solo se
	 * definisce la tipologia del documento e la tipologia del procedimento.
	 * 
	 * @param onlyTipoDoc
	 *            se <code> true </code> consente di ricercare l'assegnazione
	 *            automatica fermandosi solo sull'esistenza di una regola che grava
	 *            su una tipologia del documento, se <code> false </code> la ricerca
	 *            dell'assegnazione automatica viene fatta per la coppia tipologia
	 *            documento / tipologia procedimento
	 * 
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @param idTipoDocumento
	 *            identificativo della tipologia del documento
	 * @param idTipoProcedimento
	 *            identificativo della tipologia del procedimento
	 * @param connection
	 *            connessione alla base dati
	 * @return assegnatario associato all'assegnazione automatica se esistente,
	 *         <code> null </code> se non esiste alcuna assegnazione automatica per
	 *         i parametri specificati
	 */
	private AssegnatarioDTO getAssegnatarioPerTipologia(final Boolean onlyTipoDoc, final Long idAoo,
			final Long idTipoDocumento, final Long idTipoProcedimento, final Connection connection) {
		AssegnatarioDTO output = null;
		final List<AssegnazioneMetadatiDTO> assegnazioni = assAutomaticaMetadatiSRV.getByTipoDocumento(idAoo,
				idTipoDocumento, connection);
		for (final AssegnazioneMetadatiDTO ass : assegnazioni) {
			if (Boolean.FALSE.equals(onlyTipoDoc)) {
				if (ass.getTipoDoc().getIdTipologiaDocumento() == idTipoDocumento.longValue()
						&& ass.getTipoProc() != null
						&& ass.getTipoProc().getTipoProcedimentoId() == idTipoProcedimento) {
					output = ass.getAssegnatario();
					break;
				}
			} else {
				if (ass.getTipoDoc().getIdTipologiaDocumento() == idTipoDocumento.longValue()
						&& ass.getTipoProc() == null) {
					output = ass.getAssegnatario();
					break;
				}
			}
		}
		return output;
	}

	/**
	 * Restituisce l'assegnatario associato all'assegnazione automatica
	 * eventualmente esistente sulla tipologia documento caratterizzata come
	 * definito nei parametri.
	 * 
	 * @see it.ibm.red.business.service.facade.IAssegnazioneAutomaticaFacadeSRV#getAssegnatario(java.lang.Long,
	 *      java.lang.Long, java.lang.Long, java.lang.String, java.util.Collection)
	 */
	@Override
	public AssegnatarioDTO getAssegnatario(final Long idAoo, final Long idTipoDocumento, final Long idTipoProcedimento,
			final String codiceCapSpesa, final Collection<MetadatoDTO> metadati) {
		Connection connection = null;
		AssegnatarioDTO output = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			// 1) Verifica automatismo capitolo di spesa (un tipo documento può avere al
			// massimo un capitolo di spesa quindi non è necessario ordinare le regole)
			output = getAssegnatarioPerCapitoliSpesa(idAoo, codiceCapSpesa, metadati, connection);

			if (Boolean.FALSE.equals(assegnatarioFound(output)) && metadati != null) {

				// 2) Verifica automatismo metadati

				// Recupero tutti gli automatismi, sia quelli per valore esatto che quelli per
				// mera valorizzazione (le assegnazioni per metadato vengono ordinate dalle più
				// vecchie ed al primo match si interrompe)
				final List<AssegnazioneMetadatiDTO> assegnazioniMetadato = assAutomaticaMetadatiSRV.get(idAoo,
						idTipoDocumento, idTipoProcedimento, connection);

				// 2.1) Verifico automatismi per valore esatto
				output = getAssegnatarioPerMetadati(false, assegnazioniMetadato, metadati);

				if (Boolean.FALSE.equals(assegnatarioFound(output))) {
					// 2.2) Verifico automatismi per presenza valore
					output = getAssegnatarioPerMetadati(true, assegnazioniMetadato, metadati);
				}

			}

			if (Boolean.FALSE.equals(assegnatarioFound(output))) {
				// 3) Verifica automatismo tipologia del documento (le assegnazioni per
				// tipologia vengono ordinate dalle più vecchie ed al primo match si interrompe)

				// 3.1) Verifica tipo documento e tipo procedimento
				output = getAssegnatarioPerTipologia(false, idAoo, idTipoDocumento, idTipoProcedimento, connection);
				if (Boolean.FALSE.equals(assegnatarioFound(output))) {
					// 3.2) Verifica solo tipo documento
					output = getAssegnatarioPerTipologia(true, idAoo, idTipoDocumento, idTipoProcedimento, connection);
				}
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero delle assegnazioni automatiche relative al documento: "
					+ idTipoDocumento, e);
			throw new RedException("Errore durante il recupero delle assegnazioni automatiche relative al documento: "
					+ idTipoDocumento, e);
		} finally {
			closeConnection(connection);
		}

		return output;
	}

	/**
	 * Restituisce l'assegnazione per competenza stabilita dalle assegnazioni
	 * automatiche se esiste una regola per il documento identificato dall'
	 * <code> idDocumento </code>, restituisce <code> null </code> altrimenti.
	 * 
	 * @see it.ibm.red.business.service.facade.IAssegnazioneAutomaticaFacadeSRV#getAssegnazioneCompetenza(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public AssegnazioneDTO getAssegnazioneCompetenza(final String idDocumento, final UtenteDTO utente) {
		AssegnazioneDTO output = null;

		try {

			final List<AssegnazioneDTO> assegnazioniList = getAssegnazioni(idDocumento, utente);

			for (final AssegnazioneDTO ass : assegnazioniList) {
				if (TipoAssegnazioneEnum.COMPETENZA.equals(ass.getTipoAssegnazione())) {
					output = ass;
					break;
				}
			}

		} catch (final Exception e) {
			LOGGER.error(
					"Errore durante il recupero dell'assegnazione per COMPETENZA relative al documento: " + idDocumento,
					e);
			throw new RedException(
					"Errore durante il recupero dell'assegnazione per COMPETENZA relative al documento: " + idDocumento,
					e);
		}

		return output;
	}

	/**
	 * Restituisce la lista delle assegnazioni automatiche esistenti sul documento
	 * identificato dall' <code> idDocumento </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAssegnazioneAutomaticaFacadeSRV#getAssegnazioni(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public List<AssegnazioneDTO> getAssegnazioni(final String idDocumento, final UtenteDTO utente) {
		final List<AssegnazioneDTO> output = new ArrayList<>();
		FilenetPEHelper fpeh = null;
		Connection con = null;

		try {

			fpeh = new FilenetPEHelper(utente.getFcDTO());
			con = setupConnection(getDataSource().getConnection(), false);

			final VWRosterQuery query = fpeh.getRosterQueryWorkByIdDocumentoAndCodiceAOO(idDocumento,
					utente.getFcDTO().getIdClientAoo());
			while (query.hasNext()) {
				output.add(getAssegnazione(utente.getCodiceAoo(), (VWWorkObject) query.next(), con));
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero delle assegnazioni relative al documento: " + idDocumento, e);
			throw new RedException(
					"Errore durante il recupero delle assegnazioni relative al documento: " + idDocumento, e);
		} finally {
			logoff(fpeh);
			closeConnection(con);
		}

		return output;
	}

	/**
	 * Popola l'assegnazione automatica.
	 * 
	 * @param codiceAoo
	 *            codice identificativo dell'Area organizzativa
	 * @param wo
	 *            work object del workflow
	 * @param con
	 *            connesione al database
	 * @return DTO dell'assegnazione
	 */
	private AssegnazioneDTO getAssegnazione(final String codiceAoo, final VWWorkObject wo, final Connection con) {
		AssegnazioneDTO output = null;

		try {

			final PropertiesProvider pp = PropertiesProvider.getIstance();

			// Ottieni i metadati
			final Integer idUtenteDestinatario = (Integer) TrasformerPE.getMetadato(wo,
					pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY));
			final Integer idNodoDestinatario = (Integer) TrasformerPE.getMetadato(wo,
					pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY));
			final Integer idTipoAssegnazioneDestinatario = (Integer) TrasformerPE.getMetadato(wo,
					pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY));

			final Date dataAssegnazione = (Date) TrasformerPE.getMetadato(wo,
					pp.getParameterByKey(PropertiesNameEnum.DATA_ASSEGNAZIONE_WF_METAKEY));
			String dataAssegnazioneStr = null;
			if (dataAssegnazione != null) {
				final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				dataAssegnazioneStr = sdf.format(dataAssegnazione);
			}

			final String motivoAssegnazione = (String) TrasformerPE.getMetadato(wo,
					pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY));

			final TipoAssegnazioneEnum ta = TipoAssegnazioneEnum.get(idTipoAssegnazioneDestinatario);

			Long idNodo = 0L;
			Nodo nodoModel = null;
			if (idNodoDestinatario != null) {
				idNodo = idNodoDestinatario.longValue();
				nodoModel = nodoDAO.getNodo(idNodo, con);
			}

			Long idUtente = 0L;
			Utente utenteModel = null;
			if (idUtenteDestinatario != null) {
				idUtente = idUtenteDestinatario.longValue();
				utenteModel = utenteDAO.getUtente(idUtente, con);
			}

			final UtenteDTO utente = new UtenteDTO(idUtente, null, null, idNodo, null, null, null, null, null, null,
					codiceAoo);
			if (utenteModel != null) {
				utente.setNome(utenteModel.getNome());
				utente.setCognome(utenteModel.getCognome());
			}

			UfficioDTO ufficio = null;
			if (nodoModel != null) {
				ufficio = new UfficioDTO(idNodo, nodoModel.getDescrizione());
			}

			output = new AssegnazioneDTO(motivoAssegnazione, ta, dataAssegnazioneStr, dataAssegnazione, null, utente,
					ufficio);
			output.setWobNumber(wo.getWorkObjectNumber());

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell' assegnazione dal workflow", e);
			throw new RedException("Errore durante il recupero dell' assegnazione dal workflow", e);
		}

		return output;
	}

	/**
	 * Consente di recuperare un'assegnazione a partire da un assegnatario.
	 * 
	 * @see it.ibm.red.business.service.facade.IAssegnazioneAutomaticaFacadeSRV#getAssegnazioneFromAssegnatario(it.ibm.red.business.dto.AssegnatarioDTO)
	 */
	@Override
	public AssegnazioneDTO getAssegnazioneFromAssegnatario(final AssegnatarioDTO assegnatario) {

		Connection connection = null;
		AssegnazioneDTO assegnazione = null;
		UtenteDTO utente = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final Nodo ufficio = nodoDAO.getNodo(assegnatario.getIdUfficio(), connection);
			String desc = ufficio.getDescrizione();
			if (assegnatario.getIdUtente() != null && assegnatario.getIdUtente() != 0) {
				utente = utenteSRV.getById(assegnatario.getIdUtente());
				desc += " - " + utente.getDescrizione();
			}

			assegnazione = new AssegnazioneDTO(null, TipoAssegnazioneEnum.COMPETENZA, null, null, desc, utente,
					new UfficioDTO(ufficio.getIdNodo(), ufficio.getDescrizione()));

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'ufficio con id: " + assegnatario.getIdUfficio(), e);
			throw new RedException("Errore durante il recupero dell'ufficio con id: " + assegnatario.getIdUfficio(), e);
		} finally {
			closeConnection(connection);
		}

		return assegnazione;
	}
}
