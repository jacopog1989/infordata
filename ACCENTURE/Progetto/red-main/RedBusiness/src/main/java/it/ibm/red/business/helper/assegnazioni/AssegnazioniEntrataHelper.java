package it.ibm.red.business.helper.assegnazioni;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.AbstractDTO;
import it.ibm.red.business.dto.AssegnatarioOrganigrammaWFAttivoDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;

/**
 * Helper per le assegnazioni entrata.
 */
public class AssegnazioniEntrataHelper extends AbstractDTO {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -1239158049601486527L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AssegnazioniEntrataHelper.class.getName());
	
	/**
	 * Assegnatari competenza.
	 */
	private final Set<AssegnatarioOrganigrammaWFAttivoDTO> assegnatariCompetenza;

	/**
	 * Assegnatari conoscenza.
	 */
	private final Set<AssegnatarioOrganigrammaWFAttivoDTO> assegnatariConoscenza;

	/**
	 * Assegnatari contributo.
	 */
	private final Set<AssegnatarioOrganigrammaWFAttivoDTO> assegnatariContributo;

	/**
	 * Costruttore di default. Inizializza le liste.
	 */
	public AssegnazioniEntrataHelper() {
		
		assegnatariCompetenza = new HashSet<>();
		assegnatariConoscenza = new HashSet<>();
		assegnatariContributo = new HashSet<>();
		
	}

	/**
	 * Esegue il calcolo delle assegnazioni.
	 * @param storico
	 * @param fpeh
	 */
	public void calcolaAssegnazioni(final List<StoricoDTO> storico, final FilenetPEHelper fpeh) {
		
		final Map<String, Set<StoricoDTO>> map = new HashMap<>();
		String workflow = null;
		
		for (final StoricoDTO step: storico) {
			workflow = step.getWorkFlowNumber();
			if (workflow == null || map.get(workflow) == null) {
				map.put(workflow, new HashSet<>());
			}
			map.get(workflow).add(step);
		}
		
		for (final Entry<String, Set<StoricoDTO>> entry : map.entrySet()) {
		    boolean wfAttivo = false;
			AssegnatarioOrganigrammaWFAttivoDTO assegnante = null;
			AssegnatarioOrganigrammaWFAttivoDTO assegnatario = null;
			boolean competenza = false;
			boolean conoscenza = false;
			boolean contributo = false;
			
			final List<StoricoDTO> listaEventi = new ArrayList<>();  
			listaEventi.addAll(entry.getValue());
			Collections.sort(listaEventi);
			
			for (final StoricoDTO step: listaEventi) {
				
				if (!competenza && !conoscenza && !contributo) {
					if (("00000000000000000000000000000000").equals(step.getWorkFlowNumberPadre())) {
						competenza = true;
					}
						
					if (EventTypeEnum.RICHIESTA_CONTRIBUTO.equals(EventTypeEnum.get(step.getIdTipoOperazione()))) {
						contributo = true;
					}
						
					if (EventTypeEnum.ASSEGNAZIONE_CONOSCENZA.equals(EventTypeEnum.get(step.getIdTipoOperazione()))) {
						conoscenza = true;
					}
				}
				
				if (step.getWorkFlowNumber() != null) {
					final VWWorkObject wo = fpeh.getWorkFlowByWob(step.getWorkFlowNumber(), true);
					if (wo != null) {
						wfAttivo = true;
					}
				}
			
				
				assegnante = new AssegnatarioOrganigrammaWFAttivoDTO(step.getIdNodoAssegnante(), step.getIdUtenteAssegnante(), step.getWorkFlowNumber(), wfAttivo);
				try {
					assegnatario = new AssegnatarioOrganigrammaWFAttivoDTO(step.getIdNodoAssegnatario(), step.getIdUtenteAssegnatario(), step.getWorkFlowNumber(), wfAttivo);
				} catch (final IllegalArgumentException e) {
					LOGGER.warn(e);
					assegnatario = null;
				}
				
				if (competenza) {
					assegnatariCompetenza.add(assegnante);
					if (assegnatario != null) {
						assegnatariCompetenza.add(assegnatario);
					}
				}
				
				if (conoscenza) {
					assegnatariConoscenza.add(assegnante);
					if (assegnatario != null) {
						assegnatariConoscenza.add(assegnatario);
					}
				}
				
				if (contributo) {
					assegnatariContributo.add(assegnante);
					if (assegnatario != null) {
						assegnatariContributo.add(assegnatario);
					}
				}
				
			}
		}
		
	}

	/**
	 * Restituisce un set che definisce gli assegnatari per competenza.
	 * @return assegnatariCompetenza
	 */
	public Set<AssegnatarioOrganigrammaWFAttivoDTO> getAssegnatariCompetenza() {
		return assegnatariCompetenza;
	}

	/**
	 * Restitusice ub set che definisce gli assegnatari per conoscenza.
	 * @return assegnatariConoscenza.
	 */
	public Set<AssegnatarioOrganigrammaWFAttivoDTO> getAssegnatariConoscenza() {
		return assegnatariConoscenza;
	}

	/**
	 * Restitusice ub set che definisce gli assegnatari per contributo.
	 * @return assegnatariContributo.
	 */
	public Set<AssegnatarioOrganigrammaWFAttivoDTO> getAssegnatariContributo() {
		return assegnatariContributo;
	}
	
}