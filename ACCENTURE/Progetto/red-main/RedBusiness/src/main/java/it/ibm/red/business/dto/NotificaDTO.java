package it.ibm.red.business.dto;

import java.util.Date;

import it.ibm.red.business.enums.BooleanFlagEnum;

/**
 * The Class NotificaDTO.
 *
 * @author CPIERASC
 * 
 * 	DTO per le notifiche.
 */
public class NotificaDTO extends NotificaUtenteDTO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 4142282957308392793L;
	

	/**
	 * Notifica.
	 */
	private int idNotifica;

	/**
	 * Utente.
	 */
	private String idUtente;

	/**
	 * Nodo.
	 */
	private String idNodo;

	/**
	 * Ruolo.
	 */
	private int idRuolo;

	/**
	 * Documento.
	 */
	private String idDocumento;

	/**
	 * Evento.
	 */
	private int idEvento;

	/**
	 * Stato notifica.
	 */
	private int statoNotifica;

	/**
	 * Data evento.
	 */
	private Date dataEvento;

	/**
	 * Descrizione errore.
	 */
	private String descErrore;

	/**
	 * Data invio.
	 */
	private Date dataInvio;

	/**
	 * Flag visualizzato.
	 */
	private int visualizzato;

	/**
	 * Flag eliminaro.
	 */
	private int eliminato;
	

	/**
	 * Descrizione evento.
	 */
	private String descrizioneEvento;

	/**
	 * Aoo.
	 */
	private Long idAoo;

	/**
	 * Numero documento.
	 */
	private String numeroDocumento;

	/**
	 * Oggetto documento.
	 */
	private String oggettoDocumento;

	/**
	 * Data scadenza documento.
	 */
	private Date dataScadenzaDocumento;

	/**
	 * Categoria documento.
	 */
	private Integer idCategoriaDocumento;

	/**
	 * Classe docuentale.
	 */
	private String classeDocumentale;

	/**
	 * Anno documento.
	 */
	private Integer annoDocumento;

	/**
	 * Numero protocollo.
	 */
	private Integer numeroProtocollo;

	/**
	 * Anno protocollo.
	 */
	private Integer annoProtocollo;

	/**
	 * Icona.
	 */
	private String iconaSottoscrizione;

	/**
	 * Sotto categoria uscita.
	 */
	private Integer sottoCategoriaDocUscita;

	/**
	 * Tipologia documento.
	 */
	private Integer tipologiaDocumentoId;

	/**
	 * Ufficio creatore.
	 */
	private Integer ufficioCreatoreId;

	/**
	 * Tipo firma.
	 */
	private Integer tipoFirma;  
	
	
	/** 
	 * The Constant DISABILITATA. 
	 */
	public static final int DISABILITATA = -2;
	
	/** 
	 The Constant NON_VISUALIZZARE. 
	 */
	public static final int NON_VISUALIZZARE = -1;
	
	/** 
	 * The Constant DA_VISUALIZZARE. 
	 */
	public static final int DA_VISUALIZZARE = 0;
	
	/** 
	 * The Constant NON_INVIARE. 
	 */
	public static final int NON_INVIARE = -1;
	
	/** 
	 * The Constant DA_INVIARE. 
	 */
	public static final int DA_INVIARE = 0;
	
	/** 
	 * The Constant INVIATO. 
	 */
	public static final int INVIATO = 1;
	
	/** 
	 * The Constant ERRORE. 
	 */
	
	public static final int ERRORE = 99;
	
	/** 
	 *
	 * @return the id notifica
	 */
	public int getIdNotifica() {
		return idNotifica;
	}

	/** 
	 *
	 * @param idNotifica the new id notifica
	 */
	public void setIdNotifica(final int idNotifica) {
		this.idNotifica = idNotifica;
	}

	/** 
	 *
	 * @return the id utente
	 */
	public String getIdUtente() {
		return idUtente;
	}

	/** 
	 *
	 * @param idUtente the new id utente
	 */
	public void setIdUtente(final String idUtente) {
		this.idUtente = idUtente;
	}

	/** 
	 *
	 * @return the id nodo
	 */
	public String getIdNodo() {
		return idNodo;
	}

	/** 
	 *
	 * @param idNodo the new id nodo
	 */
	public void setIdNodo(final String idNodo) {
		this.idNodo = idNodo;
	}

	/** 
	 *
	 * @return the id documento
	 */
	public String getIdDocumento() {
		return idDocumento;
	}

	/** 
	 *
	 * @param idDocumento the new id documento
	 */
	public void setIdDocumento(final String idDocumento) {
		this.idDocumento = idDocumento;
	}

	/** 
	 *
	 * @return the id evento
	 */
	public int getIdEvento() {
		return idEvento;
	}

	/** 
	 *
	 * @param idEvento the new id evento
	 */
	public void setIdEvento(final int idEvento) {
		this.idEvento = idEvento;
	}

	/** 
	 *
	 * @return the stato notifica
	 */
	public int getStatoNotifica() {
		return statoNotifica;
	}

	/** 
	 *
	 * @param statoNotifica the new stato notifica
	 */
	public void setStatoNotifica(final int statoNotifica) {
		this.statoNotifica = statoNotifica;
	}

	/** 
	 *
	 * @return the data evento
	 */
	public Date getDataEvento() {
		return dataEvento;
	}

	/** 
	 *
	 * @param dataEvento the new data evento
	 */
	public void setDataEvento(final Date dataEvento) {
		this.dataEvento = dataEvento;
	}

	/** 
	 *
	 * @return the desc errore
	 */
	public String getDescErrore() {
		return descErrore;
	}

	/** 
	 *
	 * @param descErrore the new desc errore
	 */
	public void setDescErrore(final String descErrore) {
		this.descErrore = descErrore;
	}

	/** 
	 *
	 * @return the data invio
	 */
	public Date getDataInvio() {
		return dataInvio;
	}

	/** 
	 *
	 * @param dataInvio the new data invio
	 */
	public void setDataInvio(final Date dataInvio) {
		this.dataInvio = dataInvio;
	}

	/** 
	 *
	 * @return the idRuolo
	 */
	public int getIdRuolo() {
		return idRuolo;
	}

	/** 
	 *
	 * @param idRuolo the idRuolo to set
	 */
	public void setIdRuolo(final int idRuolo) {
		this.idRuolo = idRuolo;
	}
	
	/** 
	 *
	 * @return the visualizzato
	 */
	public int getVisualizzato() {
		return visualizzato;
	}
	
	/** 
	 *
	 * @param visualizzato the new visualizzato
	 */
	public void setVisualizzato(final int visualizzato) {
		this.visualizzato = visualizzato;
	}
	
	/** 
	 *
	 * @return the eliminato
	 */
	public int getEliminato() {
		return eliminato;
	}

	/** 
	 *
	 * @param eliminato the new eliminato
	 */
	public void setEliminato(final int eliminato) {
		this.eliminato = eliminato;
	}

	/** 
	 *
	 * @return the descrizione evento
	 */
	public String getDescrizioneEvento() {
		return descrizioneEvento;
	}

	/** 
	 *
	 * @param descrizioneEvento the new descrizione evento
	 */
	public void setDescrizioneEvento(final String descrizioneEvento) {
		this.descrizioneEvento = descrizioneEvento;
	}

	/** 
	 *
	 * @return the id aoo
	 */
	public Long getIdAoo() {
		return idAoo;
	}

	/** 
	 *
	 * @param idAoo the new id aoo
	 */
	public void setIdAoo(final Long idAoo) {
		this.idAoo = idAoo;
	}

	/** 
	 *
	 * @return the numero documento
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	/** 
	 *
	 * @param numeroDocumento the new numero documento
	 */
	public void setNumeroDocumento(final String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/** 
	 *
	 * @return the oggetto documento
	 */
	public String getOggettoDocumento() {
		return oggettoDocumento;
	}

	/** 
	 *
	 * @param oggettoDocumento the new oggetto documento
	 */
	public void setOggettoDocumento(final String oggettoDocumento) {
		this.oggettoDocumento = oggettoDocumento;
	}
	
	/** 
	 *
	 * @return the data scadenza documento
	 */
	public Date getDataScadenzaDocumento() {
		return dataScadenzaDocumento;
	}

	/** 
	 *
	 * @param dataScadenzaDocumento the new data scadenza documento
	 */
	public void setDataScadenzaDocumento(final Date dataScadenzaDocumento) {
		this.dataScadenzaDocumento = dataScadenzaDocumento;
	}
	
	/** 
	 *
	 * @return the id categoria documento
	 */
	public Integer getIdCategoriaDocumento() {
		return idCategoriaDocumento;
	}

	/** 
	 *
	 * @param idCategoriaDocumento the new id categoria documento
	 */
	public void setIdCategoriaDocumento(final Integer idCategoriaDocumento) {
		this.idCategoriaDocumento = idCategoriaDocumento;
	}

	/** 
	 *
	 * @return the classe documentale
	 */
	public String getClasseDocumentale() {
		return classeDocumentale;
	}

	/** 
	 *
	 * @param classeDocumentale the new classe documentale
	 */
	public void setClasseDocumentale(final String classeDocumentale) {
		this.classeDocumentale = classeDocumentale;
	}
	
	/** 
	 *
	 * @return the anno documento
	 */
	public Integer getAnnoDocumento() {
		return annoDocumento;
	}

	/** 
	 *
	 * @param annoDocumento the new anno documento
	 */
	public void setAnnoDocumento(final Integer annoDocumento) {
		this.annoDocumento = annoDocumento;
	}

	/** 
	 *
	 * @return the numero protocollo
	 */
	public Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/** 
	 *
	 * @param numeroProtocollo the new numero protocollo
	 */
	public void setNumeroProtocollo(final Integer numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	/** 
	 *
	 * @return the anno protocollo
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/** 
	 *
	 * @param annoProtocollo the new anno protocollo
	 */
	public void setAnnoProtocollo(final Integer annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}
	
	/**
	 * Clona notifica.
	 *
	 * @return the notifica DTO
	 */
	public NotificaDTO clonaNotifica() {
		final NotificaDTO notifica = new NotificaDTO();
		notifica.setDataEvento(dataEvento);
		notifica.setDataInvio(dataInvio);
		notifica.setDescErrore(descErrore);
		notifica.setIdDocumento(idDocumento);
		notifica.setIdEvento(idEvento);
		notifica.setIdNodo(idNodo);
		notifica.setIdNotifica(idNotifica);
		notifica.setIdRuolo(idRuolo);
		notifica.setIdUtente(idUtente);
		notifica.setStatoNotifica(statoNotifica);
		notifica.setVisualizzato(visualizzato);
		notifica.setIdCategoriaDocumento(idCategoriaDocumento);
		notifica.setClasseDocumentale(classeDocumentale);
		notifica.setAnnoDocumento(annoDocumento);
		notifica.setNumeroProtocollo(numeroProtocollo);
		notifica.setAnnoProtocollo(annoProtocollo);
		return notifica;
	}
	
	/**
	 * Metodo che calcola le notifiche da leggere.
	 */
	@Override
	public void calcolaDaLeggere() {
		if (BooleanFlagEnum.NO.getIntValue().equals(visualizzato)) {
			setDaLeggere(true);
		}
	}
	 
	/** 
	 *
	 * @return the icona sottoscrizione
	 */
	public String getIconaSottoscrizione() {
		return iconaSottoscrizione;
	}
	
	/** 
	 *
	 * @param iconaSottoscrizione the new icona sottoscrizione
	 */
	public void setIconaSottoscrizione(final String iconaSottoscrizione) {
		this.iconaSottoscrizione = iconaSottoscrizione; 
	}

	/** 
	 *
	 * @return the sotto categoria doc uscita
	 */
	public Integer getSottoCategoriaDocUscita() {
		return sottoCategoriaDocUscita;
	}

	/** 
	 *
	 * @param sottoCategoriaDocUscita the new sotto categoria doc uscita
	 */
	public void setSottoCategoriaDocUscita(final Integer sottoCategoriaDocUscita) {
		this.sottoCategoriaDocUscita = sottoCategoriaDocUscita;
	} 
	
	/** 
	 *
	 * @return the tipologia documento id
	 */
	public Integer getTipologiaDocumentoId() {
		return tipologiaDocumentoId;
	}

	/** 
	 *
	 * @param tipologiaDocumentoId the new tipologia documento id
	 */
	public void setTipologiaDocumentoId(final Integer tipologiaDocumentoId) {
		this.tipologiaDocumentoId = tipologiaDocumentoId;
	}

	/** 
	 *
	 * @return the ufficio creatore id
	 */
	public Integer getUfficioCreatoreId() {
		return ufficioCreatoreId;
	}

	/** 
	 *
	 * @param ufficioCreatoreId the new ufficio creatore id
	 */
	public void setUfficioCreatoreId(final Integer ufficioCreatoreId) {
		this.ufficioCreatoreId = ufficioCreatoreId;
	}

	/** 
	 *
	 * @return the tipo firma
	 */
	public Integer getTipoFirma() {
		return tipoFirma;
	}

	/** 
	 *
	 * @param tipoFirma the new tipo firma
	 */
	public void setTipoFirma(final Integer tipoFirma) {
		this.tipoFirma = tipoFirma;
	}
}