package it.ibm.red.business.utils;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * Utility per la serializzazione.
 */
public final class SerializationUtils implements Serializable {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SerializationUtils.class.getName());
	
	/**
	 * Messaggio.
	 */
	private static final String ERRORE_GENERICO = "Errore generico";

	/**
	 * Costruttore vuoto.
	 */
	private SerializationUtils() {
		// Costruttore vuoto.
	}
	
	/**
	 * Serializza un oggetto in flusso di byte.
	 * @param obj - oggetto da serializzare
	 * @return byte array che rappresenta l'oggetto serializzato
	 */
	public static <T> byte[] serialize(final T obj) {
		try (
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutput out = new ObjectOutputStream(bos);
		) {
			out.writeObject(obj);
			return bos.toByteArray();
		} catch (final Exception e) {
			throw new RedException(e);
		}
	}

	/**
	 * Deserializza un oggetto rappresentato da uno stream.
	 * @param obj - byte array dell'oggetto da deserializzare
	 * @return oggetto deserializzato
	 */
	@SuppressWarnings("unchecked")
	public static <T> T deserialize(final byte[] obj) {
		T output = null;
		ObjectInputStream inputStream = null;
		
		try {
			inputStream = new ObjectInputStream(new ByteArrayInputStream(obj));
		    output = (T) inputStream.readObject();
		} catch (final Exception e) {
			LOGGER.error(ERRORE_GENERICO, e);
		} finally {
			closeInputStream(inputStream);
		}
		
		return output;
	}

	/**
	 * Gestisce la chiusura dell'input stream.
	 * @param inputStream
	 */
	private static void closeInputStream(final ObjectInputStream inputStream) {
		if (inputStream != null) {
			try {
				inputStream.close();
			} catch (final IOException e) {
				LOGGER.error(ERRORE_GENERICO, e);
			}
		}
	}  
	
	/**
	 * Serializza un oggetto come JSON.
	 * @param obj - oggetto da serializzare
	 * @return oggetto serializzato - JSON
	 */
	public static <T> String serializeJSon(final T obj) {
		   String serialized = null;
		   if (obj != null) {
			    try {
			    	final JSONSerializer serializer = new JSONSerializer();
					serialized = serializer.serialize(obj);
				} catch (final Exception e) {
					LOGGER.error(ERRORE_GENERICO, e);
				}		   
		   }	
		   return serialized;

	}
	
	/**
	 * Deserializza un JSON.
	 * @param xml - oggetto JSON da deserializzare
	 * @return oggetto deserializzato
	 */
	public static <T> T deserializeJSon(final String xml) {
		T deserialized = null;
		
		if (xml != null) {	
			try {
				final JSONDeserializer<T> deSerializer = new JSONDeserializer<>();
				 deserialized = deSerializer.deserialize(xml);
			} catch (final Exception e) {
				LOGGER.error(ERRORE_GENERICO, e);
			} 
		}
		
		return deserialized;
	}  
	
	/**
	 * Serializza un oggetto come XML.
	 * @param obj - oggetto da serializzare
	 * @return xml che rappresenta l'oggetto deserializzato
	 */
	public static <T> String serializeToXML(final T obj) {
		String xml = null;
		final ByteArrayOutputStream bos = new ByteArrayOutputStream();
		final XMLEncoder encoder = new XMLEncoder(bos);
		encoder.writeObject(obj);
	    encoder.close();
	    try {
	    	xml = new String(bos.toByteArray());
			bos.close();
		} catch (final IOException e) {
			LOGGER.error(ERRORE_GENERICO, e);
		}
	    return xml;
	}

	/**
	 * Deserializza un oggetto XML.
	 * @param xml - oggetto da deserializzare
	 * @return oggetto deserializzato
	 */
	@SuppressWarnings("unchecked")
	public static <T> T deserializeXML(final String xml) {
		T output = null;
		try {
			final ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());
	        final XMLDecoder decoder = new XMLDecoder(bais);
			output = (T) decoder.readObject();
			decoder.close();
			bais.close();
		} catch (final IOException e) {
			LOGGER.error(ERRORE_GENERICO, e);
		}
		return output;
	}  
	
	/**
	 * Effettua il marshalling dei dati.
	 * @param metadata
	 * @return byte array dei dati dopo il marshalling
	 * @throws Exception 
	 */
	public static byte[] jaxbMarshall(final Object metadata) {
		try {
			final JAXBContext jaxbContext = JAXBContext.newInstance(metadata.getClass());
			final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			final ByteArrayOutputStream xmlStream = new ByteArrayOutputStream();
			jaxbMarshaller.marshal(metadata, xmlStream);
			final byte[] xml = xmlStream.toByteArray();
			xmlStream.close();
			return xml;
		} catch (JAXBException | IOException e) {
			throw new RedException(e);
		}
	}
	
	
}
