package it.ibm.red.business.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.persistence.model.Aoo;

/**
 * The Class DocumentoFascicoloDTO.
 *
 * @author CPIERASC
 * 
 *         DTO per modellare un documento contenuto all'interno di un fascicolo.
 */
public class DocumentoFascicoloDTO implements Serializable {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1647654297052965425L;

	/**
	 * Anno protocollo.
	 */
	private String annoProtocollo;

	/**
	 * Flag documento info fascicolo (ogni fascicolo ha un documento utilizzato per
	 * contenere i suoi metadati).
	 */
	private Boolean bFlagInfoFascicolo;

	/**
	 * Categoria.
	 */
	private String categoriaDocumento;

	/**
	 * Data protocollo.
	 */
	private Date dataProtocollo;

	/**
	 * Document title.
	 */
	private String documentTitle;

	/**
	 * Numero documento.
	 */
	private Integer numeroDocumento;

	/**
	 * Numero protocollo.
	 */
	private Integer numeroProtocollo;

	/**
	 * Oggetto.
	 */
	private String oggetto;

	/**
	 * Tipologia documento.
	 */
	private String tipoDocumento;

	/**
	 * Indetificativo.
	 */
	private String guid;

	/**
	 * Data creazione.
	 */
	private Date dataCreazione;

	/**
	 * Nome del file.
	 */
	private String nomeFile;

	/**
	 * Mime type.
	 */
	private String mimeType;

	/**
	 * Tipologia protocollo.
	 */
	private Integer tipoProtocollo;

	/**
	 * Identificativo categoria.
	 */
	private Integer idCategoria;

	/**
	 * Aoo.
	 */
	private Aoo aoo;

	/**
	 * Indica se è modificabile dall'utente loggato.
	 */
	private boolean modificabile;

	/**
	 * Numero allegati.
	 */
	private Integer numAllegati;

	/**
	 * Identificativo protocollo.
	 */
	private String idProtocollo;

	/**
	 * Sotto categoria uscita.
	 */
	private Integer sottoCategoriaDocUscita;

	/**
	 * Flag integrazione dati.
	 */
	private boolean integrazioneDati;

	/**
	 * Costruttore.
	 */
	public DocumentoFascicoloDTO() {
		super();
	}

	/**
	 * Costruttore.
	 * 
	 * @param inNumeroDocumento    numero documento
	 * @param inTipoDocumento      tipologia documento
	 * @param inCategoriaDocumento categoria documento
	 * @param inNumeroProtocollo   numero protocollo
	 * @param inDataProtocollo     data protocollo
	 * @param inOggetto            oggetto
	 * @param inFlagInfoFascicolo  flag info fascicolo (ogni fascicolo ha un
	 *                             documento all'interno che contiene i suoi
	 *                             metadati)
	 * @param inDocumentTitle      document title
	 */
	public DocumentoFascicoloDTO(final Integer inNumeroDocumento, final String inTipoDocumento, final String inCategoriaDocumento, final Integer inNumeroProtocollo,
			final Date inDataProtocollo, final String inOggetto, final Boolean inFlagInfoFascicolo, final String inDocumentTitle) {
		super();
		this.tipoDocumento = inTipoDocumento;
		this.categoriaDocumento = inCategoriaDocumento;
		this.numeroProtocollo = inNumeroProtocollo;
		this.dataProtocollo = inDataProtocollo;
		if (inDataProtocollo != null) {
			final SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
			annoProtocollo = sdf.format(inDataProtocollo);
		}
		this.oggetto = inOggetto;
		documentTitle = inDocumentTitle;
		bFlagInfoFascicolo = inFlagInfoFascicolo;
		this.numeroDocumento = inNumeroDocumento;
	}

	/**
	 * DTO del documento fascicolo.
	 * 
	 * @param inNumeroDocumento
	 *            numero documento
	 * @param inTipoDocumento
	 *            tipo documento
	 * @param inIdCategoria
	 *            id categoria documento
	 * @param inCategoriaDocumento
	 *            categoria documento
	 * @param inNumeroProtocollo
	 *            numero protocollo
	 * @param inDataProtocollo
	 *            data protocollo
	 * @param inOggetto
	 *            oggetto documento
	 * @param inFlagInfoFascicolo
	 *            flag info fascicolo
	 * @param inDocumentTitle
	 *            document title del documento
	 * @param guid
	 *            guid del documento
	 * @param dataCreazione
	 *            data creazione del documento
	 * @param nomeFile
	 *            nome file del documento
	 * @param mimeType
	 *            mimetype del file
	 * @param tipoProtocollo
	 *            tipo protocollo
	 * @param inAoo
	 *            area organizzativa
	 * @param numAllegati
	 *            numero allegati
	 * @param inIdProtocollo
	 *            id protocollo
	 * @param inSottoCategoriaDocUscita
	 *            sottocategoria documento uscita
	 * @param inIntegrazioneDati
	 *            integrazione dati
	 */
	public DocumentoFascicoloDTO(final Integer inNumeroDocumento, final String inTipoDocumento, final Integer inIdCategoria, final String inCategoriaDocumento,
			final Integer inNumeroProtocollo, final Date inDataProtocollo, final String inOggetto, final Boolean inFlagInfoFascicolo, final String inDocumentTitle,
			final String guid, final Date dataCreazione, final String nomeFile, final String mimeType, final Integer tipoProtocollo, final Aoo inAoo,
			final Integer numAllegati, final String inIdProtocollo, final Integer inSottoCategoriaDocUscita, final boolean inIntegrazioneDati) {
		this(inNumeroDocumento, inTipoDocumento, inCategoriaDocumento, inNumeroProtocollo, inDataProtocollo, inOggetto, inFlagInfoFascicolo, inDocumentTitle);

		this.guid = guid;
		this.dataCreazione = dataCreazione;
		this.nomeFile = nomeFile;
		this.mimeType = mimeType;
		this.tipoProtocollo = tipoProtocollo;
		this.idCategoria = inIdCategoria;
		this.aoo = inAoo;
		this.setNumAllegati(numAllegati);
		this.idProtocollo = inIdProtocollo;
		this.sottoCategoriaDocUscita = inSottoCategoriaDocUscita;
		this.integrazioneDati = inIntegrazioneDati;
	}

	/**
	 * Getter.
	 * 
	 * @return anno protocollo
	 */
	public final String getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * Getter.
	 * 
	 * @return flag info fascicolo
	 */
	public final Boolean getbFlagInfoFascicolo() {
		return bFlagInfoFascicolo;
	}

	/**
	 * Getter.
	 * 
	 * @return categoria documento
	 */
	public final String getCategoriaDocumento() {
		return categoriaDocumento;
	}

	/**
	 * Getter.
	 * 
	 * @return data protocollo
	 */
	public final Date getDataProtocollo() {
		return dataProtocollo;
	}

	/**
	 * Getter.
	 * 
	 * @return document title
	 */
	public final String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Getter.
	 * 
	 * @return numero documento
	 */
	public final Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * Getter.
	 * 
	 * @return numero protocollo
	 */
	public final Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Getter.
	 * 
	 * @return oggetto
	 */
	public final String getOggetto() {
		return oggetto;
	}

	/**
	 * Getter.
	 * 
	 * @return tipo documento
	 */
	public final String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * Setter.
	 * 
	 * @param inDataProtocollo data protocollo
	 */
	public final void setDataProtocollo(final Date inDataProtocollo) {
		this.dataProtocollo = inDataProtocollo;
	}

	/**
	 * Setter.
	 * 
	 * @param inNumeroProtocollo numero protocollo
	 */
	public final void setNumeroProtocollo(final Integer inNumeroProtocollo) {
		this.numeroProtocollo = inNumeroProtocollo;
	}

	/**
	 * Setter.
	 * 
	 * @param inOggetto oggetto
	 */
	public final void setOggetto(final String inOggetto) {
		this.oggetto = inOggetto;
	}

	/**
	 * Setter.
	 * 
	 * @param inTipoDocumento tipo documento
	 */
	public final void setTipoDocumento(final String inTipoDocumento) {
		this.tipoDocumento = inTipoDocumento;
	}

	/**
	 * Restituisce il guid.
	 * 
	 * @return guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * Restituisce la data di creazione.
	 * 
	 * @return data creazione
	 */
	public Date getDataCreazione() {
		return dataCreazione;
	}

	/**
	 * Restituisce il nome del file.
	 * 
	 * @return nome file
	 */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * Restituisce il mime type del file.
	 * 
	 * @return mime type
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * Restituisce il tipo protocollo.
	 * 
	 * @return tipo protocollo
	 */
	public Integer getTipoProtocollo() {
		return tipoProtocollo;
	}

	/**
	 * Restituisce l'id categoria.
	 * 
	 * @return id categoria
	 */
	public Integer getIdCategoria() {
		return idCategoria;
	}

	/**
	 * Restituisce un Aoo che contiene tutte le informazioni sull'Area Omogenea
	 * Organizzativa.
	 * 
	 * @return aoo
	 */
	public Aoo getAoo() {
		return aoo;
	}

	/**
	 * Restituisce true se modificabile, false altrimenti.
	 * 
	 * @return true se modificabile, false altrimenti
	 */
	public boolean isModificabile() {
		return modificabile;
	}

	/**
	 * Imposta la modificabilita.
	 * 
	 * @param modificabile
	 */
	public void setModificabile(final boolean modificabile) {
		this.modificabile = modificabile;
	}

	/**
	 * Restituisce la descrizione del tipo categoria del documento.
	 * 
	 * @see CategoriaDocumentoEnum.
	 * @return descrizione tipo categoria
	 */
	public String getDescTipoCategoriaDoc() {
		String output = null;

		if (CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0].equals(idCategoria)) {
			output = "ENTRATA";
		} else if (CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA.getIds()[0].equals(idCategoria) || CategoriaDocumentoEnum.INTERNO.getIds()[0].equals(idCategoria)) {
			output = "INTERNO";
		} else {
			output = "USCITA";
		}

		return output;
	}

	/**
	 * Restituisce il numero degli allegati.
	 * 
	 * @return numero allegati
	 */
	public Integer getNumAllegati() {
		return numAllegati;
	}

	/**
	 * Imposta il numero degli allegati.
	 * 
	 * @param numAllegati
	 */
	public void setNumAllegati(final Integer numAllegati) {
		this.numAllegati = numAllegati;
	}

	/**
	 * Restituisce l'id del protocollo.
	 * 
	 * @return id protocollo
	 */
	public String getIdProtocollo() {
		return idProtocollo;
	}

	/**
	 * Imposta l'id del protocollo.
	 * 
	 * @param idProtocollo
	 */
	public void setIdProtocollo(final String idProtocollo) {
		this.idProtocollo = idProtocollo;
	}

	/**
	 * Restituisce l'id della sottocategoria documento uscita.
	 * 
	 * @see SottoCategoriaDocumentoUscitaEnum.
	 * @return id sottocategoria documento uscita
	 */
	public Integer getSottoCategoriaDocUscita() {
		return sottoCategoriaDocUscita;
	}

	/**
	 * Imposta l'id della sottocategoria documento uscita.
	 * 
	 * @param sottoCategoriaDocUscita
	 */
	public void setSottoCategoriaDocUscita(final Integer sottoCategoriaDocUscita) {
		this.sottoCategoriaDocUscita = sottoCategoriaDocUscita;
	}

	/**
	 * Restituisce true se integrazione dati, false altrimenti.
	 * 
	 * @return true se integrazione dati, false altrimenti
	 */
	public boolean isIntegrazioneDati() {
		return integrazioneDati;
	}

	/**
	 * @param integrazioneDati
	 */
	public void setIntegrazioneDati(final boolean integrazioneDati) {
		this.integrazioneDati = integrazioneDati;
	}

}
