package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocCartaceoRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.StatoDocCartaceoEnum;

/**
 * Facade del servizio che gestisce il recogniform.
 */
public interface IRecogniformFacadeSRV extends Serializable {

	/**
	 * Ottiene i documenti cartacei.
	 * @param flagRegistroRiservato
	 * @param eliminati
	 * @param utente
	 * @return lista dei documenti cartacei
	 */
	List<MasterDocCartaceoRedDTO> getDocumentiCartacei(boolean flagRegistroRiservato, boolean eliminati, UtenteDTO utente);

	/**
	 * Aggiorna lo stato Recogniform.
	 * @param barcode
	 * @param guid
	 * @param nuovoStato
	 * @param utente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO aggiornaStatoRecogniform(String barcode, String guid, StatoDocCartaceoEnum nuovoStato, UtenteDTO utente);

	/**
	 * Rimuove il documento.
	 * @param barcode
	 * @param guid
	 * @param guidPrincipale
	 * @param utente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO rimuoviDocumento(String barcode, String guid, String guidPrincipale, UtenteDTO utente);
	
	/**
	 * Acquisisce i documenti dalla coda recogniform per l'AOO.
	 * @param idAoo
	 */
	void acquisisciDocumentiInCoda(int idAoo);

	/**
	 * Prepara il dettaglio del documento cartaceo per il censimento in entrata.
	 * @param principaleCartaceo
	 * @param allegatiCartacei
	 * @param utente
	 * @return dettaglio del documento
	 */
	DetailDocumentRedDTO preparaDetailDocumentoInEntrataCartaceo(MasterDocCartaceoRedDTO principaleCartaceo, List<MasterDocCartaceoRedDTO> allegatiCartacei, 
			UtenteDTO utente);

}
