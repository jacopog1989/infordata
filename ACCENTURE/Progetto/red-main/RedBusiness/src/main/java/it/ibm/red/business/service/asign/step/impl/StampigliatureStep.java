package it.ibm.red.business.service.asign.step.impl;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignStepResultDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.asign.step.ASignStepAbstract;
import it.ibm.red.business.service.asign.step.ContextASignEnum;
import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * Step di stampigliatura - firma asincrona.
 */
@Service
public class StampigliatureStep extends ASignStepAbstract {

	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = -7553486970039206304L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(StampigliatureStep.class.getName());
	
	/**
	 * Esegue lo step di stampigliatura per item di firma asincrona.
	 * @param in
	 * @param item
	 * @param context
	 * @return risultato dell'esecuzione dello step
	 */
	@Override
	protected ASignStepResultDTO run(Connection con, ASignItemDTO item, Map<ContextASignEnum, Object> context) {
		Long idItem = item.getId();
		LOGGER.info("run -> START [ID item: " + idItem + "]");
		ASignStepResultDTO out = new ASignStepResultDTO(item.getId(), getStep());
		
		// Si recupera l'utente firmatario
		UtenteDTO utenteFirmatario = getUtenteFirmatario(item, con);
		
		// Si recupera il workflow
		VWWorkObject wob = getWorkflow(item, out, utenteFirmatario);
		
		if (wob != null) {
			// ### STAMPIGLIATURA ###
			EsitoOperazioneDTO esitoStampigliatura = signSRV.stampigliatura(wob, utenteFirmatario, item.getSignType(), context, con);
			
			if (esitoStampigliatura.isEsito()) {
				out.setStatus(true);
				out.appendMessage("[OK] Stampigliatura eseguita");
			} else {
				throw new RedException("[KO] Errore nello step di stampigliatura: " + esitoStampigliatura.getNote() + ". [Codice errore: " + esitoStampigliatura.getCodiceErrore() + "]");
			}
		}
		
		LOGGER.info("run -> END [ID item: " + idItem + "]");
		return out;
	}

	/**
	 * Esegue il recovery in caso di errore durante l'esecuzione di {@link #run(Connection, ASignItemDTO, Map)}.
	 * @param in
	 * @param item
	 * @param context
	 * @return risultato del recovery
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected ASignStepResultDTO recovery(ASignStepResultDTO in, ASignItemDTO item, Map<ContextASignEnum, Object> context) {
		LOGGER.info("recovery -> START [ID item: " + item.getId() + "]");
		ASignStepResultDTO out = in;
		Connection con = null;
		IFilenetCEHelper fceh = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			UtenteDTO utenteFirmatario = getUtenteFirmatario(item, con);
			fceh = FilenetCEHelperProxy.newInstance(utenteFirmatario.getFcDTO(), utenteFirmatario.getIdAoo());
			
			// Il recovery provvede ad eliminare l'ultima versione (cioè quella stampigliata) del documento principale e dei suoi allegati
			if (context != null && context.get(ContextASignEnum.DOC_FN_GUID_LIST) != null) {
				List<String> documentsToRollbackGuid = (List<String>) context.get(ContextASignEnum.DOC_FN_GUID_LIST);
				
				for (String docToRollbackGuid : documentsToRollbackGuid) {
					fceh.deleteVersion(fceh.idFromGuid(docToRollbackGuid));
				}
			}
			
			// Status a false per impostare il retry
			out.setStatus(false);
		} catch (Exception e) {
			LOGGER.error(e);
			out.appendMessage(getErrorMessage() + e.getMessage());
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
		
		LOGGER.info("recovery -> END [ID item: " + item.getId() + "]");
		return out;
	}

	/**
	 * Restituisce lo @see StepEnum associato alla classe.
	 * @return StepEnum
	 */
	@Override
	protected StepEnum getStep() {
		return StepEnum.STAMPIGLIATURE;
	}

	/**
	 * @see it.ibm.red.business.service.asign.step.ASignStepAbstract#getErrorMessage().
	 */
	@Override
	protected String getErrorMessage() {
		return "[KO] Errore durante il recovery dello step di stampigliatura: ";
	}

}
