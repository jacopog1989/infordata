package it.ibm.red.business.enums;

import it.ibm.red.business.utils.StringUtils;

/**
 * The Enum HomepageEnum.
 *
 * @author CPIERASC
 * 
 *         Tipologie di codifica.
 */
public enum HomepageEnum {
	
	/**
	 * Pagina vuota.
	 */
	HOMEPAGE("Homepage", "homepage"), 
	
	/**
	 * Pagina dello scadenzario.
	 */
	SCADENZARIO("Scadenzario", "scadenzario"), 

	/**
	 * Pagina del libro firma.
	 */
	LIBRO("Libro Firma", "libro"),
	/**
	 * Pagina del Corriere.
	 */
	CORRIERE("Corriere", "corriere");

	/**
	 * Descrizione.
	 */
	private String label;
	
	/**
	 * Valore.
	 */
	private String value;
	
	
	/**
	 * Costruttore.
	 * 
	 * @param inLabel	descrizione
	 * @param inValue	valore
	 */
	HomepageEnum(final String inLabel, final String inValue) {
		label = inLabel;
		value = inValue;
	}

	/**
	 * Getter label.
	 * 
	 * @return	label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Getter value.
	 * 
	 * @return	value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Ottiene l'eventuale valore dell'enum.
	 * @param homePageEnumValue
	 * @return valore dell'enum o null
	 */
	public static HomepageEnum get(final String homePageEnumValue) {
		 HomepageEnum output = null;
		 for (HomepageEnum val:HomepageEnum.values()) {
			 if (!StringUtils.isNullOrEmpty(val.getValue()) && val.getValue().equalsIgnoreCase(homePageEnumValue)) {
				 output = val;
				 break;
			 }
		 }
		 return output;
	 }

}