package it.ibm.red.business.service.facade;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.webservice.model.redservice.header.v1.CredenzialiUtenteRed;
import it.ibm.red.webservice.model.redservice.messages.v1.GetDocumentiFascicoloResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.GetVersioneDocumentoResponse;
import it.ibm.red.webservice.model.redservice.types.v1.DocumentoRed;
import it.ibm.red.webservice.model.redservice.types.v1.IdentificativoUnitaDocumentaleFEPATypeRed;
import it.ibm.red.webservice.model.redservice.types.v1.MapRed;
import it.ibm.red.webservice.model.redservice.types.v1.MetadatoRed;
import it.ibm.red.webservice.model.redservice.types.v1.RispostaDocRed;
import it.ibm.red.webservice.model.redservice.types.v1.RispostaElencoVersioniDocRed;

/**
 * Facade del servizio RED.
 */
public interface IREDServiceFacadeSRV extends Serializable {

	/**
	 * Riassegna la fattura FEPA.
	 * @param idFascicoloFepa
	 * @param utenteCedente
	 * @param destinatario
	 * @param peh
	 * @param ceh
	 * @return id documento
	 */
	String riassegnaFatturaFepa(String idFascicoloFepa, String utenteCedente, String destinatario, FilenetPEHelper peh, IFilenetCEHelper ceh);
	
	/**
	 * Ottiene la versione del documento.
	 * @param idUtente
	 * @param idDocumento
	 * @param versione
	 * @return response di recupero della versione del documento
	 */
	GetVersioneDocumentoResponse getVersioneDocumento(String idUtente, String idDocumento, int versione);
	
	/**
	 * Modifica i metadati.
	 * @param idUtente
	 * @param idDocumento
	 * @param metadati
	 */
	void modificaMetadati(String idUtente, String idDocumento, List<MetadatoRed> metadati);
	
	/**
	 * Effettua il check out.
	 * @param idDocumento
	 * @param idUtente
	 */
	void checkOut(String idDocumento, String idUtente);
	
	/**
	 * Ottiene l'elenco delle versioni del documento.
	 * @param idUtente
	 * @param idDocumento
	 * @return risposta del recupero delle versioni del documento
	 */
	RispostaElencoVersioniDocRed elencoVersioniDocumento(String idUtente, String idDocumento);
	
	/**
	 * Ottiene i documenti del fascicolo.
	 * @param idUtente
	 * @param idFascicoloFEPA
	 * @return response di recupero dei documenti del fascicolo
	 */
	GetDocumentiFascicoloResponse getDocumentiFascicolo(String idUtente, String idFascicoloFEPA);
	
	/**
	 * Assegna il fascicolo alla fattura.
	 * @param credenziali
	 * @param idFascicoloFEPA
	 * @param mapRed
	 * @return id documento ritornato
	 */
	String assegnaFascicoloFattura(CredenzialiUtenteRed credenziali, String idFascicoloFEPA, MapRed mapRed);
	
	/**
	 * Inserisce il fascicolo decreto.
	 * @param credenziali
	 * @param metadati
	 * @param documento
	 * @param fascicoli
	 * @return id documento ritornato
	 */
	String inserimentoFascicoloDecreto(CredenzialiUtenteRed credenziali, MapRed metadati, DocumentoRed documento,
			List<IdentificativoUnitaDocumentaleFEPATypeRed> fascicoli);
	
	//Solo per finalità di test.
	/**
	 * Assegna il fascicolo alla fattura senza FEPA.
	 * @param credenziali
	 * @param idFascicoloFEPA
	 * @param mapRed
	 * @param idDocumento
	 * @param descrizione
	 * @param inFileName
	 * @param inContent
	 * @param inMimeType
	 * @return id documento ritornato
	 */
	String assegnaFascicoloFatturaWithoutFepa(CredenzialiUtenteRed credenziali, String idFascicoloFEPA, MapRed mapRed,
			String idDocumento, String descrizione, String inFileName, byte[] inContent, String inMimeType);
	
	/**
	 * Annulla il documento.
	 * @param idUtente
	 * @param idDocumento
	 * @param motivazioneAnnullamento
	 */
	void annullaDocumento(String idUtente, String idDocumento, String motivazioneAnnullamento);
	
	/**
	 * Effettua il check in.
	 * @param documentTitle
	 * @param idUtente
	 * @param metadatiMap
	 * @param allegati
	 * @param content
	 * @return risposta
	 * @throws IOException
	 */
	RispostaDocRed checkIn(String documentTitle, String idUtente, Map<String, Object> metadatiMap,
			List<DocumentoRed> allegati, byte[] content) throws IOException;
	
	/**
	 * Annulla il check out.
	 * @param documentTitle
	 * @param idUtente
	 */
	void undoCheckOut(String documentTitle, String idUtente);
	
	/**
	 * Modifica il decreto.
	 * @param idNodo
	 * @param idUtente
	 * @param idDocumentoDecreto
	 * @param fattureDaAggiungereList
	 * @param fattureDaRimuovereList
	 * @param content
	 * @param contentType
	 */
	void modificaDecreto(String idNodo, String idUtente, String idDocumentoDecreto,
			List<String> fattureDaAggiungereList, List<String> fattureDaRimuovereList, byte[] content,
			String contentType);


}