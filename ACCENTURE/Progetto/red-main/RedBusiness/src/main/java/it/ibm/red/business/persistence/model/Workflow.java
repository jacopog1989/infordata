package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * The Class Workflow.
 *
 * @author m.crescentini
 * 
 *         Entità che mappa la tabella WORKFLOW.
 */
public class Workflow implements Serializable {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 8519525733039157864L;
	
	/**
	 * Identificativo.
	 */
	private Integer id;
	
	/**
	 * Nome.
	 */
	private String name;
	
	/**
	 * Descrizione.
	 */
	private String description;
	
	/**
	 * Flag entrata generica.
	 */
	private int flagGenericoEntrata;
	
	/**
	 * Flag generico uscita.
	 */
	private int flagGenericoUscita;
	
	/**
	 * Identificativo AOO.
	 */
	private Long idAoo;
	
	
	/**
	 * Costruttore.
	 * 
	 * @param id
	 * @param name
	 * @param description
	 * @param flagGenericoEntrata
	 * @param flagGenericoUscita
	 * @param idAoo
	 */
	public Workflow(final Integer id, final String name, final String description, final int flagGenericoEntrata, final int flagGenericoUscita, final Long idAoo) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.flagGenericoEntrata = flagGenericoEntrata;
		this.flagGenericoUscita = flagGenericoUscita;
		this.idAoo = idAoo;
	}


	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * @return the flagGenericoEntrata
	 */
	public int getFlagGenericoEntrata() {
		return flagGenericoEntrata;
	}


	/**
	 * @return the flagGenericoUscita
	 */
	public int getFlagGenericoUscita() {
		return flagGenericoUscita;
	}


	/**
	 * @return the idAoo
	 */
	public Long getIdAoo() {
		return idAoo;
	}
}