package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.Document;

import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dao.ILookupDAO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.PEDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.ContextTrasformerPEEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.TrasformPE;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.builder.Builder;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.provider.ServiceTaskProvider.ServiceTask;
import it.ibm.red.business.service.IAttiSRV;
import it.ibm.red.business.service.IEventoLogSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.IIntegrazioneDatiSRV;
import it.ibm.red.business.service.IOperationWorkFlowSRV;
import it.ibm.red.business.service.IRiassegnazioneSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * Service integrazione dati.
 */
@Service
@Component
public class IntegrazioneDatiSRV extends AbstractService implements IIntegrazioneDatiSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 8447337068295228612L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(IntegrazioneDatiSRV.class.getName());

	/**
	 * Messaggio.
	 */
	private static final String MOTIVAZIONE_ATTI_VALIDATA = "Integrazione dati validata";

	/**
	 * Messaggio.
	 */
	private static final String MOTIVAZIONE_ATTI_ASSOCIATA = "Integrazione dati associata";

	/**
	 * Messaggio.
	 */
	private static final String MOTIVAZIONE_ASSEGNAZIONE_ASSOCIATA = "Integrazione dati associata";

	/**
	 * Servizio.
	 */
	@Autowired
	private IFascicoloSRV fascicoloSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IAttiSRV attiSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IOperationWorkFlowSRV operationWorkflowSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IRiassegnazioneSRV riassegnaSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private ILookupDAO lookupDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private IEventoLogSRV logSRV;

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * Post construct.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IIntegrazioneDatiFacadeSRV#validaIntegrazioneDati(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "validaIntegrazioneDati")
	public EsitoOperazioneDTO validaIntegrazioneDati(final UtenteDTO utente, final String wobNumber) {
		return validaIntegrazioneDati(utente, wobNumber, true, false, Builder.AGLI_ATTI);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IIntegrazioneDatiFacadeSRV#validaIntegrazioneDati(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "validaIntegrazioneDati")
	public Collection<EsitoOperazioneDTO> validaIntegrazioneDati(final UtenteDTO utente, final Collection<String> wobNumber) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();

		for (final String w : wobNumber) {
			esiti.add(validaIntegrazioneDati(utente, w));
		}

		return esiti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IIntegrazioneDatiFacadeSRV#validaIntegrazioneDati(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, boolean, boolean, java.lang.String).
	 */
	@Override
	public EsitoOperazioneDTO validaIntegrazioneDati(final UtenteDTO utente, final String wobNumber, final boolean spostaInLavorazione, final boolean attiAsync,
			final String statoNPS) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		Connection con = null;
		Connection connectionDwh = null;

		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			final VWWorkObject workObject = fpeh.getWorkFlowByWob(wobNumber, true);

			final EnumMap<ContextTrasformerPEEnum, Object> context = new EnumMap<>(ContextTrasformerPEEnum.class);
			context.put(ContextTrasformerPEEnum.UCB, utente.isUcb());
			final PEDocumentoDTO wfDTO = TrasformPE.transform(workObject, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO_CONTEXT, context);

			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final String[] parametriDocumento = new String[] { pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY),
					pp.getParameterByKey(PropertiesNameEnum.PROTOCOLLO_RIFERIMENTO_METAKEY), pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY) };
			final Document doc = fceh.getDocumentByIdGestionale(wfDTO.getIdDocumento(), Arrays.asList(parametriDocumento), null, utente.getIdAoo().intValue(), null,
					pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));

			final Integer numeroDocumento = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			esito.setIdDocumento(numeroDocumento);

			final String idProtocollo = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);

			final String protocolloRiferimento = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.PROTOCOLLO_RIFERIMENTO_METAKEY);

			if (StringUtils.isNullOrEmpty(protocolloRiferimento)) {
				throw new RedException("Documento da validare sprovvisto di protocollo di riferimento");
			}

			// Sposta documento di integrazione dati in fascicolo del protocollo di
			// riferimento
			con = setupConnection(getDataSource().getConnection(), false);
			fascicolaIntegrazioneDati(utente, wfDTO.getIdDocumento(), protocolloRiferimento, idProtocollo, fceh, fpeh, con);

			// ### Messa agli atti del documento di integrazione dati -> START
			boolean procediConMessaAgliAtti = true;
			// N.B. Se il documento si trova nella coda Corriere, si assegna al Dirigente di
			// quell'ufficio prima della messa agli atti
			if (DocumentQueueEnum.CORRIERE.getName().equals(workObject.getCurrentQueueName())) {
				LOGGER.info("Il documento di integrazione dati si trova nella coda CORRIERE, si procede con l'assegnazione al dirigente dell'ufficio"
						+ " propedeutica alla sua messa agli atti. [Document Title: " + wfDTO.getIdDocumento() + "]");

				final String motivoAssegnazione = "Assegnazione automatica a fronte della ricezione dell'integrazione dati";

				// Assegnazione al Dirigente
				final Collection<EsitoOperazioneDTO> esitiAssegnazione = riassegnaSRV.assegnaADirigenteUfficio(wfDTO.getIdDocumento(), workObject, motivoAssegnazione, fceh,
						fpeh, con);

				final Iterator<EsitoOperazioneDTO> it = esitiAssegnazione.iterator();
				while (it.hasNext()) {
					if (!it.next().isEsito()) {
						procediConMessaAgliAtti = false;
						LOGGER.error("Errore nell'assegnazione al dirigente dell'ufficio propedeutica alla messa agli atti del documento di integrazione dati."
								+ " [Document Title: " + wfDTO.getIdDocumento() + "]");
						break;
					}
				}
			}

			if (procediConMessaAgliAtti) {
				final EsitoOperazioneDTO esitoMettiAgliAtti = attiSRV.mettiAgliAtti(wobNumber, utente, MOTIVAZIONE_ATTI_VALIDATA, attiAsync, statoNPS);
				if (!esitoMettiAgliAtti.isEsito()) {
					throw new RedException("Impossibile mettere agli atti il documento di integrazione dati");
				}
			}
			// ### Messa agli atti del documento di integrazione dati -> END

			// Rimetti in lavorazione il protocollo di riferimento, se richiesto
			// (e.g. l'integrazione dati spontanea non rimette il lavorazione il protocollo
			// di riferimento)
			if (spostaInLavorazione) {
				LOGGER.info("validaIntegrazioneDati -> Spostamento in lavorazione del protocollo di riferimento");
				final VWWorkObject workflowPrincipale = fpeh.getWorkflowPrincipale(protocolloRiferimento, utente.getFcDTO().getIdClientAoo());
				final EsitoOperazioneDTO esitoSpostaInLavorazione = operationWorkflowSRV.spostaInLavorazione(utente, workflowPrincipale.getWorkObjectNumber());
				if (!esitoSpostaInLavorazione.isEsito()) {
					throw new RedException("Impossibile spostare in lavorazione il documento in attesa di integrazione dati");
				}
			}

			connectionDwh = setupConnection(getFilenetDataSource().getConnection(), false);
			logSRV.inserisciEventoLog(Integer.parseInt(protocolloRiferimento), utente.getIdUfficio(), utente.getId(), 0L, 0L, new Date(),
					EventTypeEnum.RICONCILIAZIONE_INTEGRAZIONE_DATI, MOTIVAZIONE_ATTI_VALIDATA, 0, utente.getIdAoo(), connectionDwh);

			esito.setEsito(true);
			esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);

		} catch (final Exception e) {
			LOGGER.error("Errore durante la validazione integrazione dati del documento con wobNumber: " + wobNumber, e);
			esito.setEsito(false);
			esito.setNote("Errore durante la validazione integrazione dati del documento.");
		} finally {
			logoff(fpeh);
			popSubject(fceh);
			closeConnection(con);
			closeConnection(connectionDwh);
		}

		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IIntegrazioneDatiFacadeSRV#associaIntegrazioneDati(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String).
	 */
	@Override
	public EsitoOperazioneDTO associaIntegrazioneDati(final UtenteDTO utente, final String wobNumber, final String idDocumentoIntegrazioneDati) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		Connection conn = null;
		Connection connectionDwh = null;
		try {

			fpeh = new FilenetPEHelper(utente.getFcDTO());

			// recupera l'id del documento che insiste sul wobNumber sollecitato
			final VWWorkObject workObject = fpeh.getWorkFlowByWob(wobNumber, true);

			final EnumMap<ContextTrasformerPEEnum, Object> context = new EnumMap<>(ContextTrasformerPEEnum.class);
			context.put(ContextTrasformerPEEnum.UCB, utente.isUcb());
			final PEDocumentoDTO wfDTO = TrasformPE.transform(workObject, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO_CONTEXT, context);

			final String idDocumentoProtocolloRiferimento = wfDTO.getIdDocumento();

			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			String[] parametriDocumento = new String[] { pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY) };
			final Document docProtocolloRiferimento = fceh.getDocumentByIdGestionale(idDocumentoProtocolloRiferimento, Arrays.asList(parametriDocumento), null,
					utente.getIdAoo().intValue(), null, pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));

			final Integer numeroDocumento = (Integer) TrasformerCE.getMetadato(docProtocolloRiferimento, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			esito.setIdDocumento(numeroDocumento);

			// Aggiorna il protocollo di riferimento del documento di integrazione dati
			final Map<String, Object> metadati = new HashMap<>();
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.PROTOCOLLO_RIFERIMENTO_METAKEY), idDocumentoIntegrazioneDati);
			fceh.updateMetadati(docProtocolloRiferimento, metadati);

			// recupera info documento di integrazione dati
			parametriDocumento = new String[] { pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY) };
			final Document doc = fceh.getDocumentByIdGestionale(idDocumentoIntegrazioneDati, Arrays.asList(parametriDocumento), null, utente.getIdAoo().intValue(), null,
					pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));

			final String idProtocollo = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);

			// sposta documento di integrazione dati in fascicolo del protocollo di
			// riferimento
			conn = setupConnection(getDataSource().getConnection(), false);
			fascicolaIntegrazioneDati(utente, idDocumentoIntegrazioneDati, idDocumentoProtocolloRiferimento, idProtocollo, fceh, fpeh, conn);

			// In caso il documento su cui è stata sollecitata la response sia nella coda
			// corriere, assegna il documento all'utente in sessione
			final VWWorkObject workflowPrincipaleIntegrazioneDati = fpeh.getWorkflowPrincipale(idDocumentoIntegrazioneDati, utente.getFcDTO().getIdClientAoo());
			final PEDocumentoDTO wfDTOIntegrazioneDati = TrasformPE.transform(workflowPrincipaleIntegrazioneDati, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO_CONTEXT, context);
			if (DocumentQueueEnum.isOneKindOfCorriere(wfDTOIntegrazioneDati.getQueue())) {
				riassegnaSRV.riassegna(utente, Arrays.asList(wfDTOIntegrazioneDati.getWobNumber()), ResponsesRedEnum.ASSEGNA, utente.getIdUfficio(), utente.getId(),
						MOTIVAZIONE_ASSEGNAZIONE_ASSOCIATA);
			}

			// messa agli atti del documento di integrazione dati
			final EsitoOperazioneDTO esitoMettiAgliAtti = attiSRV.mettiAgliAtti(wfDTOIntegrazioneDati.getWobNumber(), utente, MOTIVAZIONE_ATTI_ASSOCIATA);
			if (!esitoMettiAgliAtti.isEsito()) {
				throw new RedException("Impossibile mettere agli atti il documento di integrazione dati");
			}

			// Messa in Lavorazione sul wf relativo al documento sulla quale è stata
			// sollecitata la response
			final EsitoOperazioneDTO esitoSpostaInLavorazione = operationWorkflowSRV.spostaInLavorazione(utente, wobNumber);
			if (!esitoSpostaInLavorazione.isEsito()) {
				throw new RedException("Impossibile spostare in lavorazione il documento in attesa di integrazione dati");
			}

			connectionDwh = setupConnection(getFilenetDataSource().getConnection(), false);
			logSRV.inserisciEventoLog(Integer.parseInt(idDocumentoProtocolloRiferimento), utente.getIdUfficio(), utente.getId(), 0L, 0L, new Date(),
					EventTypeEnum.RICONCILIAZIONE_INTEGRAZIONE_DATI, MOTIVAZIONE_ATTI_VALIDATA, 0, utente.getIdAoo(), connectionDwh);

			esito.setEsito(true);
			esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);

		} catch (final Exception e) {
			LOGGER.error("Errore durante la validazione integrazione dati del documento con wobNumber: " + wobNumber, e);
			esito.setEsito(false);
			esito.setNote("Errore durante la validazione integrazione dati del documento.");
		} finally {
			logoff(fpeh);
			popSubject(fceh);
			closeConnection(conn);
			closeConnection(connectionDwh);
		}

		return esito;
	}

	private void fascicolaIntegrazioneDati(final UtenteDTO utente, final String idDocumentoIntegrazioneDati, final String idDocumentoProtocolloRiferimento,
			final String idProtocolloIntegrazioneDati, final IFilenetCEHelper fceh, final FilenetPEHelper fpeh, final Connection conn) {

		final FascicoloDTO fascicoloProcedimentale = fascicoloSRV.getFascicoloProcedimentale(idDocumentoIntegrazioneDati, utente.getIdAoo().intValue(), fceh, conn);
		final FascicoloDTO fascicoloProcedimentaleProtocolloRiferimento = fascicoloSRV.getFascicoloProcedimentale(idDocumentoProtocolloRiferimento,
				utente.getIdAoo().intValue(), fceh, conn);

		final String titolario = fascicoloProcedimentaleProtocolloRiferimento.getIndiceClassificazione() + " - "
				+ fascicoloProcedimentaleProtocolloRiferimento.getDescrizioneTitolario();

		final boolean esitoSpostaDocumento = fascicoloSRV.spostaDocumento(utente, idDocumentoIntegrazioneDati, fascicoloProcedimentaleProtocolloRiferimento.getIdFascicolo(),
				fascicoloProcedimentale.getIdFascicolo(), utente.getIdAoo(), idProtocolloIntegrazioneDati, titolario, fceh, fpeh, conn);

		if (!esitoSpostaDocumento) {
			throw new RedException("Errore in fase di spostamento dell'integrazione dati nel fascicolo del protocollo di riferimento");
		}

	}

	/**
	 * @see it.ibm.red.business.service.facade.IIntegrazioneDatiFacadeSRV#getIntegrazioneDati4Associa(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<MasterDocumentRedDTO> getIntegrazioneDati4Associa(final UtenteDTO utente) {
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		Connection conn = null;
		Collection<MasterDocumentRedDTO> documenti = null;
		EnumMap<DocumentQueueEnum, Collection<String>> idsMap = null;
		try {

			documenti = new ArrayList<>();
			idsMap = new EnumMap<>(DocumentQueueEnum.class);

			// recupera documenti dalle code corriere e da lavorare
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			conn = setupConnection(getDataSource().getConnection(), false);

			final VWQueueQuery queryCorriere = fpeh.getWorkFlowsByWobQueueFilter(null, DocumentQueueEnum.CORRIERE, DocumentQueueEnum.CORRIERE.getIndexName(),
					utente.getIdUfficio(), null, utente.getFcDTO().getIdClientAoo(), null, null, null);
			idsMap.put(DocumentQueueEnum.CORRIERE, getIds(queryCorriere));

			final VWQueueQuery queryDaLavorare = fpeh.getWorkFlowsByWobQueueFilter(null, DocumentQueueEnum.DA_LAVORARE, DocumentQueueEnum.DA_LAVORARE.getIndexName(),
					utente.getIdUfficio(), Arrays.asList(utente.getId()), utente.getFcDTO().getIdClientAoo(), null, null, null);
			idsMap.put(DocumentQueueEnum.DA_LAVORARE, getIds(queryDaLavorare));

			// filtra i documenti di tipo integrazione dati, recuperando le informazioni su
			// CE
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			conn = setupConnection(getDataSource().getConnection(), false);
			final Map<Long, String> idsTipoDoc = lookupDAO.getDescTipoDocumento(utente.getIdAoo().intValue(), conn);
			documenti.addAll(getDocumenti4IntegrazioneDati(idsMap, DocumentQueueEnum.CORRIERE, idsTipoDoc, utente.getIdAoo().intValue(), fceh));
			documenti.addAll(getDocumenti4IntegrazioneDati(idsMap, DocumentQueueEnum.DA_LAVORARE, idsTipoDoc, utente.getIdAoo().intValue(), fceh));

			// ordina risultati, per data di creazione
			Collections.sort((List<MasterDocumentRedDTO>) documenti);

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero dei documenti di integrazione dati", e);
			throw new RedException("Errore in fase di recupero dei documenti di integrazione dati", e);
		} finally {
			logoff(fpeh);
			popSubject(fceh);
			closeConnection(conn);
		}

		return documenti;
	}

	private Collection<String> getIds(final VWQueueQuery query) {
		final Collection<String> ids = new HashSet<>();
		while (query.hasNext()) {
			final VWWorkObject wo = (VWWorkObject) query.next();
			ids.add(TrasformerPE.getMetadato(wo, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY).toString());
		}
		return ids;
	}

	private Collection<MasterDocumentRedDTO> getDocumenti4IntegrazioneDati(final Map<DocumentQueueEnum, Collection<String>> idsMap, final DocumentQueueEnum queue,
			final Map<Long, String> idsTipoDoc, final Integer idAoo, final IFilenetCEHelper fceh) {

		final Iterator<String> it = idsMap.get(queue).iterator();

		final StringBuilder whereCondition = new StringBuilder();
		whereCondition.append("d.").append(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)).append(" IN (");
		while (it.hasNext()) {
			whereCondition.append("'").append(it.next()).append("'");
			if (it.hasNext()) {
				whereCondition.append(", ");
			}
		}
		whereCondition.append(")");

		whereCondition.append(" AND d." + pp.getParameterByKey(PropertiesNameEnum.INTEGRAZIONE_DATI_METAKEY) + " = true");

		final DocumentSet dsFileNet = fceh.getDocuments(idAoo, whereCondition.toString(),
				PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), true, false);

		if (dsFileNet != null) {
			final Map<ContextTrasformerCEEnum, Object> context = new EnumMap<>(ContextTrasformerCEEnum.class);
			context.put(ContextTrasformerCEEnum.IDENTIFICATIVI_TIPO_DOC, idsTipoDoc);
			context.put(ContextTrasformerCEEnum.QUEUE, queue);

			return TrasformCE.transform(dsFileNet, TrasformerCEEnum.FROM_DOCUMENTO_TO_ASSOCIA_INTEGRAZIONE_DATI_CONTEXT, context);
		} else {
			return Collections.emptyList();

		}
	}

}