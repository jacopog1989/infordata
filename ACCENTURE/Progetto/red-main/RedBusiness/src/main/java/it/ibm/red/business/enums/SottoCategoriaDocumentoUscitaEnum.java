package it.ibm.red.business.enums;

/**
 * The Enum SottoCategoriaDocumentoUscitaEnum.
 *
 * @author a.dilegge
 * 
 *         Enum sotto categoria documento uscita.
 */
public enum SottoCategoriaDocumentoUscitaEnum {
	
	/**
	 * Valore.
	 */
	VISTO("Visto", 1, "VIS", true),
	
	/**
	 * Valore.
	 */
	OSSERVAZIONE("Osservazione", 2, "OSS", true),
	
	/**
	 * Valore.
	 */
	RISPOSTA("Risposta", 3, "RISP", false),
	
	/**
	 * Valore.
	 */
	INOLTRO("Inoltro", 4, "IN", false),
	
	/**
	 * Valore.
	 */
	RICHIESTA_INTEGRAZIONI("Richiesta integrazioni", 5, "R. INT", true),
	
	/**
	 * Valore.
	 */
	RESTITUZIONE("Restituzione", 6, "RES", true),
	
	/**
	 * Valore.
	 */
	RELAZIONE_POSITIVA("Relazione Positiva", 7, "R. POS", true),
	
	/**
	 * Valore.
	 */
	RELAZIONE_NEGATIVA("Relazione Negativa", 8, "R. NEG", true);

	/**
	 * Identificativo.
	 */
	private Integer id;
	
	/**
	 * Descrizione.
	 */
	private String descrizione;
	
	/**
	 * Codice.
	 */
	private String codice;
	
	/**
	 * Flag registrazione ausiliaria.
	 */
	private boolean registrazioneAusiliaria;
	
	/**
	 * Costruttore.
	 * 
	 * @param inDescrizione	descrizione
	 * @param inIds			identificativi associati
	 */
	SottoCategoriaDocumentoUscitaEnum(final String inDescrizione, final Integer inId, final String inCodice, final boolean inRegistrazioneAusiliaria) {
		this.id = inId;
		this.descrizione = inDescrizione;
		this.codice = inCodice;
		this.registrazioneAusiliaria = inRegistrazioneAusiliaria;
	}

	/**
	 * Getter identificativo.
	 * 
	 * @return	identificativo associato
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Getter descrizione.
	 * 
	 * @return	descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Metodo per il recupero di un enum a partire dal suo valore caratteristico.
	 * 
	 * @param value	valore
	 * @return		enum associata al valore
	 */
	public static SottoCategoriaDocumentoUscitaEnum get(final Integer value) {
		
		if (value == null) {
			return null;
		}
		
		final SottoCategoriaDocumentoUscitaEnum output = null;
		for (final SottoCategoriaDocumentoUscitaEnum cat : SottoCategoriaDocumentoUscitaEnum.values()) {
			if (value.equals(cat.getId())) {
				return cat;
			}
		}
		
		return output;
	}

	/**
	 * Restituisce il codice.
	 * @return codice
	 */
	public String getCodice() {
		return codice;
	}

	/**
	 * Imposta il codice.
	 * @param codice
	 */
	protected void setCodice(final String codice) {
		this.codice = codice;
	}
	
	/**
	 * Restituisce true se la sottocategoria è associata ad una registrazione ausiliaria.
	 * @return true se registrazione ausiliaria, false altrimenti
	 */
	public boolean isRegistrazioneAusiliaria() {
		return registrazioneAusiliaria;
	}
}
