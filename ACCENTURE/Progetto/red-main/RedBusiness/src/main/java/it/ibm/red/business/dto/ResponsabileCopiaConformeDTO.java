package it.ibm.red.business.dto;

import java.util.Date;

/**
 * DTO che definisce un responsabile copia conforme.
 */
public class ResponsabileCopiaConformeDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -4144124240355695923L;

	/**
	 * Flag Predefinito.
	 */
	private Integer predefinito;

	/**
	 * Nodo.
	 */
	private Long idNodo;

	/**
	 * Descriione nodo.
	 */
	private String descrizioneNodo;

	/**
	 * Utente dirigente.
	 */
	private Integer idUtenteDirigente;

	/**
	 * Utente.
	 */
	private Long idUtente;

	/**
	 * Nome.
	 */
	private String nome;

	/**
	 * Cognome.
	 */
	private String cognome;

	/**
	 * Ruolo.
	 */
	private Integer idRuolo;

	/**
	 * Descrizione ruolo.
	 */
	private String descrizioneRuolo;

	/**
	 * Data attivazoine.
	 */
	private Date dataAttivazione;

	/**
	 * Data disattivazione.
	 */
	private Date dataDisattivazione;

	/**
	 * Flag Attivato.
	 */
	private Integer attivato;

	/**
	 * Descrizione nodo padre.
	 */
	private String descrizioneNodoPadre;

	/**
	 * Aoo.
	 */
	private Integer idAOO;

	/**
	 * E' costituito dall'id dell'ufficio + "spazio" + id dell'utente
	 * Mi serve per distinguere questi valori sulla maschera di inserimento/modifica 
	 * mancando un id univoco per il responsabile copia conforme
	 */
	private String id;  
	
	/**
	 * Costruttore vuoto.
	 */
	public ResponsabileCopiaConformeDTO() { }

	/**
	 * Costruttore completo.
	 * @param idNodo
	 * @param descrizioneNodo
	 * @param predefinito
	 * @param idAOO
	 * @param idRuolo
	 * @param descrizioneRuolo
	 * @param idUtente
	 * @param nome
	 * @param cognome
	 * @param dataAttivazione
	 * @param dataDisattivazione
	 * @param idUtenteDirigente
	 * @param descrizioneNodoPadre
	 */
	public ResponsabileCopiaConformeDTO(final Long idNodo, final String descrizioneNodo, final Integer predefinito,
			final Integer idAOO, final Integer idRuolo, final String descrizioneRuolo, final Long idUtente, final String nome, final String cognome,
			final Date dataAttivazione, final Date dataDisattivazione, final Integer idUtenteDirigente,
			final String descrizioneNodoPadre) { 
		this.idNodo = idNodo;
		this.descrizioneNodo = descrizioneNodo;
		this.predefinito = predefinito;
		this.idAOO = idAOO;
		this.idRuolo = idRuolo;
		this.descrizioneRuolo = descrizioneRuolo;
		this.idUtente = idUtente;
		this.nome = nome;
		this.cognome = cognome;
		this.dataAttivazione = dataAttivazione;
		this.dataDisattivazione = dataDisattivazione;
		this.idUtenteDirigente = idUtenteDirigente;
		this.descrizioneNodoPadre = descrizioneNodoPadre;
		this.id = idNodo + " " + idUtente;
	}

	/*get & set*****************************************************************************************/
	/**
	 * Restituisce l'id del nodo.
	 * @return id nodo
	 */
	public Long getIdNodo() {
		return idNodo;
	}

	/**
	 * Restituisce la descrizione del nodo.
	 * @return descrizione nodo
	 */
	public String getDescrizioneNodo() {
		return descrizioneNodo;
	}

	/**
	 * Restituisce il predefinito.
	 * @return predefinito
	 */
	public Integer getPredefinito() {
		return predefinito;
	}

	/**
	 * Restituisce l'id dell'Area Organizzativa Omogenea.
	 * @return id AOO
	 */
	public Integer getIdAOO() {
		return idAOO;
	}

	/**
	 * Restituisce l'id del ruolo.
	 * @return id ruolo
	 */
	public Integer getIdRuolo() {
		return idRuolo;
	}

	/**
	 * Restitusice la descrizione del ruolo.
	 * @return descrizione ruolo
	 */
	public String getDescrizioneRuolo() {
		return descrizioneRuolo;
	}

	/**
	 * Restituisce l'id dell'utente.
	 * @return id utente
	 */
	public Long getIdUtente() {
		return idUtente;
	}

	/**
	 * Restituisce il nome.
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Restitusice il cognome.
	 * @return cognome
	 */
	public String getCognome() {
		return cognome;
	}

	/**
	 * Restituisce la data di attivazione.
	 * @return data attivazione
	 */
	public Date getDataAttivazione() {
		return dataAttivazione;
	}

	/**
	 * Restituisce la data di disattivazione.
	 * @return data disattivazione
	 */
	public Date getDataDisattivazione() {
		return dataDisattivazione;
	}

	/**
	 * Restituisce l'id dell'utente dirigente.
	 * @return id utente dirigente
	 */
	public Integer getIdUtenteDirigente() {
		return idUtenteDirigente;
	}

	/**
	 * @return attivato
	 */
	public Integer getAttivato() {
		return attivato;
	}

	/**
	 * Restituisce la descrizione del nodo padre.
	 * @return nodo padre
	 */
	public String getDescrizioneNodoPadre() {
		return descrizioneNodoPadre;
	}

	/**
	 * Restituisce l'id.
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Imposta l'id.
	 * @param id
	 */
	public void setId(final String id) {
		this.id = id;
	}
}
