package it.ibm.red.business.enums;

/**
 * Enum per la gestione delle response Applicative e Filenet
 * 
 * @author APerquoti
 */
public enum TaskTypeEnum {

	/**
	 * Response che richiedono input dell'utente.
	 */
	HUMAN_TASK,

	/**
	 * Response che richiedono solo un SRV di supporto.
	 */
	SERVICE_TASK,

	/**
	 * Response che necessitano di gestione tramite Bean.
	 */
	BEAN_TASK;
}
