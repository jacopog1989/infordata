package it.ibm.red.business.validator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.util.CollectionUtils;

import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.enums.SalvaDocumentoErroreEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;

/**
 * Classe Validator destinatario RED.
 */
public class DestinatarioRedValidator extends AbstractValidator<DestinatarioRedDTO, SalvaDocumentoErroreEnum> {

	/**
	 * Costruttore di default.
	 */
	public DestinatarioRedValidator() {
		super();
	}

	/**
	 * @see it.ibm.red.business.validator.AbstractValidator#validaTutti(java.util.Collection).
	 */
	@Override
	public List<SalvaDocumentoErroreEnum> validaTutti(final Collection<DestinatarioRedDTO> destinatari) {
		if (!CollectionUtils.isEmpty(destinatari)) {
			final List<DestinatarioRedDTO> destinatariDaConfrontare = new ArrayList<>();
			for (final DestinatarioRedDTO destinatario : destinatari) {
				valida(destinatario);

				// Si verifica se il destinatario è già presente nella lista
				if (getErrori().isEmpty() && destinatari.size() > 1) {
					if (verificaDestinatarioDuplicato(destinatariDaConfrontare, destinatario)) {
						getErrori().add(SalvaDocumentoErroreEnum.DOC_DESTINATARIO_DUPLICATO);
					} else {
						destinatariDaConfrontare.add(destinatario);
					}
				}
			}
			// Lista dei destinatari vuota
		} else {
			getErrori().add(SalvaDocumentoErroreEnum.DOC_DESTINATARI_NON_PRESENTI);
		}

		return getErrori();
	}

	/**
	 * @see it.ibm.red.business.validator.AbstractValidator#valida(java.lang.Object).
	 */
	@Override
	public List<SalvaDocumentoErroreEnum> valida(final DestinatarioRedDTO destinatario) {
		// Si verifica se la tipologia del destinatario è specificata
		if (destinatario.getTipologiaDestinatarioEnum() == null) {
			getErrori().add(SalvaDocumentoErroreEnum.DOC_DESTINATARIO_TIPOLOGIA);
		} else {
			// Se il destinatario è INTERNO, devono essere presenti ID Nodo e ID Utente
			if (TipologiaDestinatarioEnum.INTERNO.equals(destinatario.getTipologiaDestinatarioEnum())
					&& (destinatario.getIdNodo() == null || destinatario.getIdUtente() == null)) {
				getErrori().add(SalvaDocumentoErroreEnum.DOC_DESTINATARIO_INTERNO_NON_VALIDO);
				// Se il destinatario è ESTERNO...
			} else if (TipologiaDestinatarioEnum.ESTERNO.equals(destinatario.getTipologiaDestinatarioEnum())) {
				// ...deve essere presente il Contatto
				if (destinatario.getContatto() == null) {
					getErrori().add(SalvaDocumentoErroreEnum.DOC_DESTINATARIO_ESTERNO_NON_VALIDO);
				}
				// ...deve essere specificato il Mezzo di Spedizione
				if (destinatario.getMezzoSpedizioneEnum() == null) {
					getErrori().add(SalvaDocumentoErroreEnum.DOC_DESTINATARIO_MEZZO_SPEDIZIONE);
				}
			}
		}

		return getErrori();
	}

	private static boolean verificaDestinatarioDuplicato(final List<DestinatarioRedDTO> destinatariDaConfrontare, final DestinatarioRedDTO destinatarioDaVerificare) {
		boolean isDuplicato = false;

		final TipologiaDestinatarioEnum tipologiaDestinatarioDaVerificare = destinatarioDaVerificare.getTipologiaDestinatarioEnum();

		for (final DestinatarioRedDTO destinatario : destinatariDaConfrontare) {
			if ((TipologiaDestinatarioEnum.INTERNO.equals(tipologiaDestinatarioDaVerificare) && destinatarioDaVerificare.getIdNodo().equals(destinatario.getIdNodo())
					&& destinatarioDaVerificare.getIdUtente().equals(destinatario.getIdUtente()))) {
				isDuplicato = true;
				break;
			}
		}

		return isDuplicato;
	}
}