package it.ibm.red.business.helper.notifiche.states;

import java.util.Map;

import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.enums.StatoCodaEmailEnum;
import it.ibm.red.business.enums.TipoNotificaPecEnum;
import it.ibm.red.business.helper.notifiche.IGestioneMailState;
import it.ibm.red.business.helper.notifiche.NotificaHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.CodaEmail;

/**
 * Stato: attesa avvenuta consegna.
 */
public class AttesaAvvenutaConsegnaState2 implements IGestioneMailState {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AttesaAvvenutaConsegnaState2.class.getName());

	/**
	 * @see it.ibm.red.business.helper.notifiche.IGestioneMailState#getNextNotificaEmailToUpdate(it.ibm.red.business.helper.notifiche.NotificaHelper,
	 *      it.ibm.red.business.persistence.model.CodaEmail, java.util.Map,
	 *      boolean).
	 */
	@Override
	public CodaEmail getNextNotificaEmailToUpdate(final NotificaHelper nh, final CodaEmail notificaEmail, final Map<String, EmailDTO> notifiche, final boolean reInvio) {
		CodaEmail result = notificaEmail;
		
		IGestioneMailState nextState = new AttesaAvvenutaConsegnaState2();
		int nextStatoRicevuta =  notificaEmail.getStatoRicevuta();
		String destinatario = notificaEmail.getEmailDestinatario();
		String key = destinatario + "#" + notificaEmail.getIdMessaggio();
		LOGGER.info("recupero notifica nella mappa delle notifiche recuperate su Filenet con chiave " + key);
		
		EmailDTO notifica = notifiche.get(key);
		
		if (notifica != null) {
			String oggetto = notifica.getOggetto();
			LOGGER.info("notifica con chiave " + key + " recuperata da Filenet con oggetto [" + oggetto + "]");
			if (TipoNotificaPecEnum.checkPrefissoNotificaPec(oggetto, TipoNotificaPecEnum.AVVENUTA_CONSEGNA)) {
				nextState = new ChiusuraState3();
				nextStatoRicevuta = StatoCodaEmailEnum.CHIUSURA.getStatus();
			} else if (TipoNotificaPecEnum.checkPrefissoNotifichePec(oggetto, TipoNotificaPecEnum.ERRORE_CONSEGNA, TipoNotificaPecEnum.PREAVVISO_ERRORE_CONSEGNA)) {
				nextState = new ReInvioState5();
				nextStatoRicevuta = StatoCodaEmailEnum.REINVIO.getStatus();
			} else if (TipoNotificaPecEnum.checkPrefissoNotificaPec(oggetto, TipoNotificaPecEnum.ERRORE_CONSEGNA_VIRUS)) {
				nextState = new ErroreState4();
				nextStatoRicevuta = StatoCodaEmailEnum.REINVIO.getStatus();
			}
					
			nh.setGestioneMailState(nextState);
			notificaEmail.setStatoRicevuta(nextStatoRicevuta);
			return result;
		} else {
			LOGGER.warn("notifica con chiave " + key + " non recuperata da Filenet ");
			return null;
		}
	}

}
