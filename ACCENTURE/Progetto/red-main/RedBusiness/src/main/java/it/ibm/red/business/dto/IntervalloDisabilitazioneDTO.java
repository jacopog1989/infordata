package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * DTO che definisce un intervallo disabilitazione.
 */
public class IntervalloDisabilitazioneDTO implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 6101366525273877519L;
	
	/**
	 * Label.
	 */
	public static final int DISABILITA_SEMPRE = 0;

	/**
	 * Label.
	 */
	public static final int DISABILITA_INTERVALLO = 1;

	/**
	 * Identificativo.
	 */
	private int id;

	/**
	 * Nome.
	 */
	private String nome;
	
	/**
	 * Costruttore del DTO.
	 */
	public IntervalloDisabilitazioneDTO() {
	}
	
	/**
	 * Costruttore del DTO.
	 * @param id
	 * @param nome
	 */
	public IntervalloDisabilitazioneDTO(final int id, final String nome) {
		super();
		this.id = id;
		this.nome = nome;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final int id) {
		this.id = id;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(final String nome) {
		this.nome = nome;
	}

	/**
	 * @see java.lang.Object#toString().
	 */
	@Override
	public String toString() {
		return "IntervalloDisabilitazione [id=" + id + ", nome=" + nome + "]";
	}
}