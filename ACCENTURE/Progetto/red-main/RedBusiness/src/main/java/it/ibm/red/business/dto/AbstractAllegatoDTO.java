package it.ibm.red.business.dto;

import java.util.Date;

/**
 * DTO abstract degli allegati.
 */
public class AbstractAllegatoDTO extends AbstractDTO {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = 2506757913120741774L;

	/**
	 * Guid allegato.
	 */
	private String guid;
	
	/**
	 * Nome del file.
	 */
	private String nomeFile;
	
	/**
	 * Oggetto.
	 */
	private String oggetto;
	
	/**
	 * Data creazione.
	 */
	private Date dateCreated;
	
	
	/**
	 * Costruttore di default.
	 * */
	public AbstractAllegatoDTO() {
		super();
	}

	/**
	 * Costruttore.
	 * 
	 * @param guid
	 * @param nomeFile
	 * @param oggetto
	 * @param dateCreated
	 * */
	public AbstractAllegatoDTO(final String guid, final String nomeFile, final String oggetto, final Date dateCreated) {
		super();
		this.guid = guid;
		this.nomeFile = nomeFile;
		this.oggetto = oggetto;
		this.dateCreated = dateCreated;
	}

	/**
	 * Getter del Guid.
	 * 
	 * @return il Guid dell'allegato
	 * */
	public String getGuid() {
		return guid;
	}

	/**
	 * Setter del Guid dell'allegato.
	 * 
	 * @param guid il guid da impostare
	 */
	public void setGuid(final String guid) {
		this.guid = guid;
	}

	/**
	 * Getter del nome del file.
	 * 
	 * @return il nome del file dell'allegato
	 * */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * Setter del nome del file dell'allegato.
	 * 
	 * @param nomeFile il nome del file da impostare
	 * */
	public void setNomeFile(final String nomeFile) {
		this.nomeFile = nomeFile;
	}

	/**
	 * Getter dell'oggetto dell'allegato.
	 * 
	 * @return l'oggetto dell'allegato come String
	 * */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Setter dell'oggetto dell'allegato.
	 * 
	 * @param oggetto la stringa che deve essere impostata come oggetto.
	 * 
	*/
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * Getter della data di creazione dell'allegato.
	 * 
	 * @return la date di creazione
	 * */
	public Date getDateCreated() {
		return dateCreated;
	}

	/**
	 * Setter della data di creazione.
	 * 
	 * @param dateCreated la data di creazione da impostare
	 * */
	public void setDateCreated(final Date dateCreated) {
		this.dateCreated = dateCreated;
	}
}