package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.IGestioneNotificheEmailDAO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.PostaEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.StatoCodaEmailEnum;
import it.ibm.red.business.enums.TipologiaDestinatariEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.CodaEmail;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.ISignSRV;
import it.ibm.red.business.service.ISpeditoSRV;

/**
 * Service che gestisce la funzionalità di spedizione.
 */
@Service
public class SpeditoSRV extends AbstractService implements ISpeditoSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -5812499795976766649L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SpeditoSRV.class.getName());

	/**
	 * Messaggio errore avanzamento workflow del messaggio.
	 */
	private static final String ERROR_AVANZAMENTO_WORKFLOW_MSG = "Errore nell'avanzamento del workflow.";

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * DAO.
	 */
	@Autowired
	private IGestioneNotificheEmailDAO notificheMailDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private INpsSRV npsSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ISignSRV signSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IFascicoloSRV fascicoloSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * Esegue la logica successivamente alla creazione del service.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISpeditoFacadeSRV#spedito(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Date, java.util.Collection).
	 */
	@Override
	public Collection<EsitoOperazioneDTO> spedito(final UtenteDTO utente, final Date dataSped, final Collection<String> wobNumbers) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		for (final String wobNumber : wobNumbers) {
			esiti.add(spedito(utente, dataSped, wobNumber));
		}
		return esiti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISpeditoFacadeSRV#spedito(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Date, java.lang.String).
	 */
	@Override
	public EsitoOperazioneDTO spedito(final UtenteDTO utente, final Date dataSped, final String wobNumber) {
		EsitoOperazioneDTO esito = null;
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			esito = spedito(utente, dataSped, wobNumber, true, connection);

			commitConnection(connection);
		} catch (final Exception e) {
			LOGGER.error(ERROR_AVANZAMENTO_WORKFLOW_MSG, e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}

		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.ISpeditoSRV#spedito(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Date, java.lang.String, boolean, java.sql.Connection).
	 */
	@Override
	public EsitoOperazioneDTO spedito(final UtenteDTO utente, final Date dataSped, final String wobNumber, final boolean aggiornaNps, final Connection connection) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		Integer numeroDoc = 0;

		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;

		try {

			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final Integer idDoc = (Integer) fpeh.getMetadato(wobNumber, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
			final String idAoo = utente.getIdAoo().toString();
			final Document d = fceh.getDocumentByDTandAOO(idDoc.toString(), Long.parseLong(idAoo));
			numeroDoc = (Integer) TrasformerCE.getMetadato(d, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);

			final HashMap<String, Object> map = new HashMap<>();
			final Integer idFascicolo = (Integer) fpeh.getMetadato(wobNumber, pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY));
			map.put(pp.getParameterByKey(PropertiesNameEnum.DATA_ASSEGNAZIONE_WF_METAKEY), dataSped);
			fascicoloSRV.aggiornaStato(String.valueOf(idFascicolo), FilenetStatoFascicoloEnum.CHIUSO, utente.getIdAoo(), fceh);
			fpeh.nextStepMittente(wobNumber, utente.getId(), ResponsesRedEnum.SPEDITO.getResponse(), map);

			boolean hasDestinatariElettronici = false;
			final Collection<CodaEmail> notificheMail = notificheMailDAO.getNotificheEmailByIdDocumento(idDoc.toString(), connection);
			if (!(notificheMail.isEmpty())) {
				for (final CodaEmail notifica : notificheMail) {
					final List<Integer> items = new ArrayList<>();
					if (notifica.getTipoDestinatario() == TipologiaDestinatariEnum.CARTACEO.getId()) {
						items.add(notifica.getIdNotifica());
					} else {
						hasDestinatariElettronici = true;
					}
					notificheMailDAO.updateStatoRicevuta(items, StatoCodaEmailEnum.CHIUSURA, connection);
				}
			} else {
				LOGGER.info("idDocumento: " + idDoc + " Questo documento non ha notifiche associate");
			}

			final Aoo aoo = aooDAO.getAoo(utente.getIdAoo(), connection);

			// se posta interna o solo destinatari cartacei => spedisci protocollo su NPS
			if ((PostaEnum.POSTA_INTERNA.getValue().intValue() == aoo.getPostaInteropEsterna() || !hasDestinatariElettronici)
					&& utente.getIdTipoProtocollo() != null && aggiornaNps && TipologiaProtocollazioneEnum.isProtocolloNPS(utente.getIdTipoProtocollo())
							&& d.getProperties().isPropertyPresent(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY))
							&& d.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY)) != null){

				final String idProtocollo = d.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY));

				boolean spedisci = true;
				final Integer tipoProtocollo = d.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY));
				final ProtocolloNpsDTO dto = npsSRV.getDatiMinimiByIdProtocollo(utente.getIdAoo().intValue(), idProtocollo, tipoProtocollo);
				if (dto.getStatoProtocollo() != null && "SPEDITO".equalsIgnoreCase(dto.getStatoProtocollo())) {
					spedisci = false;
				}

				if (spedisci) {
					final String infoProtocollo = new StringBuilder()
							.append(d.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY))).append("/")
							.append(d.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY))).append("_")
							.append(utente.getCodiceAoo()).toString();

					npsSRV.spedisciProtocolloUscitaAsync(utente, infoProtocollo, idProtocollo, new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()), null, null,
							idDoc.toString(), connection);
				}
			
			}

			signSRV.gestisciAllacci(fceh, fpeh, aoo, d, utente, aggiornaNps, connection);

			esito.setEsito(true);
			esito.setIdDocumento(numeroDoc);
			esito.setNote("Spedizione effettuata con successo.");

		} catch (final Exception e) {
			LOGGER.error(ERROR_AVANZAMENTO_WORKFLOW_MSG, e);

			esito.setIdDocumento(numeroDoc);
			esito.setNote(ERROR_AVANZAMENTO_WORKFLOW_MSG);
		} finally {

			logoff(fpeh);
			popSubject(fceh);
		}

		return esito;
	}

}
