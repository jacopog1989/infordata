/**
 * 
 */
package it.ibm.red.business.service.concrete;
 
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.AssegnatarioOrganigrammaWFAttivoDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.StatoLavorazioneEnum;
import it.ibm.red.business.helper.assegnazioni.AssegnazioniEntrataHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.provider.ServiceTaskProvider.ServiceTask;
import it.ibm.red.business.service.INextStepSRV;
import it.ibm.red.business.service.IStoricoSRV;
import it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV;

/**
 * @author APerquoti
 *
 */
@Service
public class NextStepSRV extends AbstractService implements INextStepSRV {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4172214923176125960L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NextStepSRV.class.getName());
	
	/**
	 * 
	 */
	@Autowired
	private IOperationWorkFlowFacadeSRV owfSRV;
	
	/**
	 * Service.
	 */
	@Autowired
	private IStoricoSRV storicoSRV;
	
	/**
	 * Service.
	 */
	@Autowired
	private CodeDocumentiSRV codeDocSRV;

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#mettiAgliAtti(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "MettiagliAtti")
	public EsitoOperazioneDTO mettiAgliAtti(final UtenteDTO utente, final String wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.METTI_ATTI.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#presaVisione(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "presaVisione")
	public EsitoOperazioneDTO presaVisione(final UtenteDTO utente, final String wobNumber) {
		final FilenetPEHelper fpeh = new FilenetPEHelper(utente.getFcDTO());
		EsitoOperazioneDTO esito = null;
		
		try {
			final VWWorkObject wo = fpeh.getWorkFlowByWob(wobNumber, true);
			final String documentTitle = TrasformerPE.getMetadato(wo, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY)).toString();

			esito = owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.PRESA_VISIONE.getResponse());
			rimuoviDocumentoCodeApplicative(utente, documentTitle, fpeh);
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'avanzamento del WF per l'operazione Rimuovi Documento Code Applicative: ", e);
		} finally {
			logoff(fpeh);
		}
		
		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#presaVisione(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "presaVisione")
	public Collection<EsitoOperazioneDTO> presaVisione(final UtenteDTO utente, final Collection<String> wobNumber) {
		final FilenetPEHelper fpeh = new FilenetPEHelper(utente.getFcDTO());
		final List<String> documentTitleList = new ArrayList<>();
		Collection<EsitoOperazioneDTO> esito = null;
		
		try {
			for (final String wob : wobNumber) {
				final VWWorkObject wo = fpeh.getWorkFlowByWob(wob, true);
				final String documentTitle = TrasformerPE.getMetadato(wo, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY)).toString();
				documentTitleList.add(documentTitle);
			}

			esito = owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.PRESA_VISIONE.getResponse());
			for (final String documentTitle : documentTitleList) {
				rimuoviDocumentoCodeApplicative(utente, documentTitle, fpeh);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'avanzamento del WF per l'operazione Rimuovi Documento Code Applicative: ", e);
		} finally {
			logoff(fpeh);
		}
		
		return esito;
	}
	
	private void rimuoviDocumentoCodeApplicative(final UtenteDTO utente, final String documentTitle, final FilenetPEHelper fpeh) {
		IFilenetCEHelper fceh = null;
		
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			
			final Document documento = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());
			final Integer categoria = (Integer) TrasformerCE.getMetadato(documento, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY);
			final CategoriaDocumentoEnum catEnum = CategoriaDocumentoEnum.get(categoria);
			
			if (CategoriaDocumentoEnum.ENTRATA.equals(catEnum)) {
				final VWWorkObject workflowPrincipale = fpeh.getWorkflowPrincipale(documentTitle, utente.getFcDTO().getIdClientAoo());
				
				// se wf principale NON esiste, cancellazione del documento dalle code applicative degli uffici/utenti assegnatari per conoscenza
				if (workflowPrincipale == null) {
					final AssegnazioniEntrataHelper assegnazioniEntrata = new AssegnazioniEntrataHelper();
					assegnazioniEntrata.calcolaAssegnazioni((List<StoricoDTO>) storicoSRV.getStorico(Integer.parseInt(documentTitle), utente.getIdAoo().intValue()), fpeh);
					boolean isNodo = false;
					boolean isNodoUtente = false;
					for (final AssegnatarioOrganigrammaWFAttivoDTO assCompetenza : assegnazioniEntrata.getAssegnatariCompetenza()) {
						if (assCompetenza.getIdNodo().intValue() == utente.getIdUfficio().intValue()) {
							isNodo = true;
							if (assCompetenza.getIdUtente().intValue() == utente.getId().intValue()) {
								isNodoUtente = true;
								break;
							}
						}
					}
					if (!isNodo) {
						codeDocSRV.rimuoviDocumentoCodeApplicative(documentTitle, utente.getIdUfficio());
					} else if (!isNodoUtente) {
						codeDocSRV.rimuoviDocumentoCodeApplicative(documentTitle, utente.getIdUfficio(), utente.getId());
					} else {
						codeDocSRV.aggiornaStatoDocumentoLavorazione(documentTitle, utente.getIdUfficio(), utente.getId(), StatoLavorazioneEnum.ATTI);
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore riscontrato: ", e);
			throw e;
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#procedi(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "procedi")
	public EsitoOperazioneDTO procedi(final UtenteDTO utente, final String wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.PROCEDI.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#procedi(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "procedi")
	public Collection<EsitoOperazioneDTO> procedi(final UtenteDTO utente, final Collection<String> wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.PROCEDI.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#prosegui(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "prosegui")
	public EsitoOperazioneDTO prosegui(final UtenteDTO utente, final String wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.PROSEGUI.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#prosegui(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "prosegui")
	public Collection<EsitoOperazioneDTO> prosegui(final UtenteDTO utente, final Collection<String> wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.PROSEGUI.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#inviaIspettore(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "inviaIspettore")
	public EsitoOperazioneDTO inviaIspettore(final UtenteDTO utente, final String wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.INVIA_ISPETTORE.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#inviaIspettore(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "inviaIspettore")
	public Collection<EsitoOperazioneDTO> inviaIspettore(final UtenteDTO utente, final Collection<String> wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.INVIA_ISPETTORE.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#stornaAlCorriere(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "stornaAlCorriere")
	public EsitoOperazioneDTO stornaAlCorriere(final UtenteDTO utente, final String wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.STORNA_AL_CORRIERE.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#stornaAlCorriere(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "stornaAlCorriere")
	public Collection<EsitoOperazioneDTO> stornaAlCorriere(final UtenteDTO utente, final Collection<String> wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.STORNA_AL_CORRIERE.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#mettiInSospeso(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "mettiInSospeso")
	public EsitoOperazioneDTO mettiInSospeso(final UtenteDTO utente, final String wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.METTI_IN_SOSPESO.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#mettiInSospeso(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "mettiInSospeso")
	public Collection<EsitoOperazioneDTO> mettiInSospeso(final UtenteDTO utente, final Collection<String> wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.METTI_IN_SOSPESO.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#inviaUffCoordinamento(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "inviaUffCoordinamento")
	public EsitoOperazioneDTO inviaUffCoordinamento(final UtenteDTO utente, final String wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.INVIA_UFFICIO_COORDINAMENTO.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#inviaUffCoordinamento(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "inviaUffCoordinamento")
	public Collection<EsitoOperazioneDTO> inviaUffCoordinamento(final UtenteDTO utente, final Collection<String> wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.INVIA_UFFICIO_COORDINAMENTO.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#inviaUCP(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "inviaUCP")
	public EsitoOperazioneDTO inviaUCP(final UtenteDTO utente, final String wobNumber) {
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		String documentTitle = null;
		Integer numDoc = null;
		
		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			
			final VWWorkObject wo = fpeh.getWorkFlowByWob(wobNumber, true);
			documentTitle = TrasformerPE.getMetadato(wo, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY)).toString();
			numDoc = fceh.getNumDocByDocumentTitle(documentTitle);
			
			fpeh.nextStepMittente(wobNumber, utente.getId(), ResponsesRedEnum.INVIA_UCP.getResponse(), null);
			
			esito = new EsitoOperazioneDTO(null, null, numDoc, wobNumber);
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'avanzamento del WF per l'operazione INVIA UCP: ", e);
			esito.setNote("Errore durante l'avanzamento del WF per l'operazione INVIA UCP");
			esito.setIdDocumento(numDoc);
			esito.setEsito(false);
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}
		
		return esito;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#inviaUCP(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "inviaUCP")
	public Collection<EsitoOperazioneDTO> inviaUCP(final UtenteDTO utente, final Collection<String> wobNumber) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		for (final String w : wobNumber) {
			esiti.add(inviaUCP(utente, w));
		}
		
		return esiti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#inviaFirmaDigitale(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "inviaFirmaDigitale")
	public EsitoOperazioneDTO inviaFirmaDigitale(final UtenteDTO utente, final String wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.INVIA_FIRMA_DIGITALE.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#inviaFirmaDigitale(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "inviaFirmaDigitale")
	public Collection<EsitoOperazioneDTO> inviaFirmaDigitale(final UtenteDTO utente, final Collection<String> wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.INVIA_FIRMA_DIGITALE.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#verificato(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "verificato")
	public EsitoOperazioneDTO verificato(final UtenteDTO utente, final String wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.VERIFICATO.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#verificato(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "verificato")
	public Collection<EsitoOperazioneDTO> verificato(final UtenteDTO utente, final Collection<String> wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.VERIFICATO.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#inviaDirettore(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "inviaDirettore")
	public EsitoOperazioneDTO inviaDirettore(final UtenteDTO utente, final String wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.INVIA_AL_DIRETTORE.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#inviaDirettore(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "inviaDirettore")
	public Collection<EsitoOperazioneDTO> inviaDirettore(final UtenteDTO utente, final Collection<String> wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.INVIA_AL_DIRETTORE.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#spostaNelLibroFirma(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "spostaNelLibroFirma")
	public Collection<EsitoOperazioneDTO> spostaNelLibroFirma(final UtenteDTO utente, final Collection<String> wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.SPOSTA_NEL_LIBRO_FIRMA.getResponse());
	}

	/**
	 * @see it.ibm.red.business.service.facade.INextStepFacadeSRV#spostaNelLibroFirma(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "spostaNelLibroFirma")
	public EsitoOperazioneDTO spostaNelLibroFirma(final UtenteDTO utente, final String wobNumber) {
		return owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.SPOSTA_NEL_LIBRO_FIRMA.getResponse());
	}
	
}