package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;

import it.ibm.red.business.logger.REDLogger;

import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;

import it.ibm.red.business.dto.MailAttachmentDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.utils.FileUtils;

/**
 * The Class FromDocumentoToAllegatoMailTrasformer.
 *
 * @author CPIERASC
 * 
 *         Trasformer allegato mail.
 */
public class FromDocumentoToAllegatoMailTrasformer extends TrasformerCE<MailAttachmentDTO> {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 7115280317344104212L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentoToAllegatoMailTrasformer.class.getName());
	
	
	/**
	 * Costruttore.
	 */
	public FromDocumentoToAllegatoMailTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_ALLEGATO_MAIL);
	}

	/**
	 * Trasform.
	 *
	 * @param document
	 *            the document
	 * @param connection
	 *            the connection
	 * @return the mail attachment DTO
	 */
	@Override
	public final MailAttachmentDTO trasform(final Document document, final Connection connection) {
		try {
			
			String inDocumentTitle = (String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			String inFileName = (String) getMetadato(document, PropertiesNameEnum.NOME_FILE_METAKEY);
			String id = getMetadato(document, PropertyNames.ID).toString();
			String mimeType = getMetadato(document, PropertyNames.MIME_TYPE).toString();
			byte [] inContent = FileUtils.getByteFromInputStream(((ContentTransfer) document.get_ContentElements().get(0)).accessContentStream());
			
			return buildMailAttachment(id, inFileName, inContent, mimeType, id, null, inDocumentTitle);
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero di un metadato del dettaglio del documento: ", e);
			return null;
		}
	}

	/**
	 * Costruisce l'attachment della mail.
	 * 
	 * @param id
	 * @param inFileName
	 * @param inContent
	 * @param mimeType
	 * @param inDescription
	 * @param inNeedSign
	 * @param inDocuemenTitle
	 * @return Mail Attachment creato.
	 */
	protected MailAttachmentDTO buildMailAttachment(final String id, final String inFileName, final byte[] inContent, final String mimeType,
			final String inDescription, final Boolean inNeedSign, final String inDocuemenTitle) {
		
		return new MailAttachmentDTO(id, inFileName, inContent, mimeType, inDescription, inNeedSign, inDocuemenTitle);
	}

}
