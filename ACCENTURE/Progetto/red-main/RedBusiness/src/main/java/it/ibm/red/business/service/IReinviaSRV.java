package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IReinviaFacadeSRV;

/**
 * Interfaccia del servizio che gestisce la funzionalità di reinvio.
 */
public interface IReinviaSRV extends IReinviaFacadeSRV {

}
