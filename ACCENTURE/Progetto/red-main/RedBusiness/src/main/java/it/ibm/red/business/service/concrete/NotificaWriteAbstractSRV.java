package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.INotificaDAO;
import it.ibm.red.business.dao.ISottoscrizioniDAO;
import it.ibm.red.business.dto.CanaleTrasmissioneDTO;
import it.ibm.red.business.dto.DisabilitazioneNotificheUtenteDTO;
import it.ibm.red.business.dto.EventoSottoscrizioneDTO;
import it.ibm.red.business.dto.NotificaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedWSException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.INotificaWriteSRV;
import it.ibm.red.business.service.ITracciaDocumentiSRV;
import it.ibm.red.business.service.IUtenteSRV;

/**
 * Abstract del service di scrittura di notifiche.
 */
@Service
public abstract class NotificaWriteAbstractSRV extends AbstractService implements INotificaWriteSRV {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 5072215933806523575L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotificaWriteAbstractSRV.class.getName());

	/**
	 * Service.
	 */
	@Autowired
	private ITracciaDocumentiSRV tracciaDocumentiSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private INotificaDAO notificaDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ISottoscrizioniDAO sottoscrizioniDAO;

	/**
	 * Fornitore della properties dell'applicazione.
	 */
	private PropertiesProvider pp;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaWriteFacadeSRV#writeNotifiche(int).
	 */
	@Override
	public void writeNotifiche(final int idAoo) {
		
		//RECUPERO LA LISTA DELLE SOTTOSCRIZIONI IN SCADENZA PER L'AOO IN INPUT
		final List<EventoSottoscrizioneDTO> sottoscrizioniList = getSottoscrizioni(idAoo);
		//NOTIFICO I DOCUMENTI IN SCADENZA
		registraNotifiche(idAoo, sottoscrizioniList);
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaWriteFacadeSRV#writeNotifiche(int, java.lang.Object).
	 */
	@Override
	public void writeNotifiche(final int idAoo, final Object objectForGetDataDocument) {
		
		//RECUPERO LA LISTA DELLE SOTTOSCRIZIONI IN SCADENZA PER L'AOO IN INPUT
		final List<EventoSottoscrizioneDTO> sottoscrizioniList = getSottoscrizioni(idAoo);
		
		//NOTIFICO I DOCUMENTI IN SCADENZA
		registraNotifiche(idAoo, sottoscrizioniList, objectForGetDataDocument);
	}

	/**
	 * Recupera i tipi sottoscrizione relativi al job.
	 * 
	 * @param idAoo
	 * @return
	 */
	protected abstract List<EventoSottoscrizioneDTO> getSottoscrizioni(int idAoo);

	private void registraNotifiche(final int idAoo, final List<EventoSottoscrizioneDTO> sottoscrizioni) {
		registraNotifiche(idAoo, sottoscrizioni, null);
	
	}
	
	private void registraNotifiche(final int idAoo, final List<EventoSottoscrizioneDTO> sottoscrizioni, final Object objectForGetDataDocument) {

		if (sottoscrizioni != null && !sottoscrizioni.isEmpty()) {
			//RECUPERA LE SOTTOSCRIZIONI DA REGISTRARE
			final List<NotificaDTO> notificheList = buildNotifiche(idAoo, sottoscrizioni, objectForGetDataDocument);
			//INSERISCI NOTIFICA
			insertNotifiche(notificheList);	
		}
	}
	
	private List<NotificaDTO> buildNotifiche(final int idAoo, final List<EventoSottoscrizioneDTO> sottoscrizioni, final Object objectForGetDataDocument) {
		final Aoo aoo = aooSRV.recuperaAoo(idAoo);
		final List<NotificaDTO> notificheList = new ArrayList<>();
		
		UtenteDTO utente = null;
		for (final EventoSottoscrizioneDTO evento: sottoscrizioni) {

			utente = utenteSRV.getById((long) evento.getIdUtente(), (long) evento.getIdRuolo(), (long) evento.getIdNodo());
			if(utente==null) {
				continue;
			}		
			List<InfoDocumenti> listaDocumentiInScadenza = null;
			if (objectForGetDataDocument != null) {
				listaDocumentiInScadenza = getDataDocumenti(utente, aoo, evento.getIdEvento(), objectForGetDataDocument);
			} else {
				listaDocumentiInScadenza = getDataDocumenti(utente, aoo, evento.getIdEvento());
			}
			Integer idDocumento = null;
			for (final InfoDocumenti documento : listaDocumentiInScadenza) {
				idDocumento = documento.getIdDocumento();
				if (checkEvento(evento, documento.getDataScadenza(), idDocumento.intValue(), utente)) {
					final NotificaDTO notifica = new NotificaDTO();
					notifica.setIdUtente(evento.getIdUtente() + "");
					notifica.setIdNodo(evento.getIdNodo() + "");
					notifica.setIdDocumento(idDocumento + "");
					notifica.setIdEvento(evento.getIdEvento());
					notifica.setIdRuolo(evento.getIdRuolo());
					// Gestione canale trasmissione
					final int idCanaleTrasmissioneEvento = evento.getCanaleTrasmissione().getId();

					final List<Integer> idCanaliTrasmissioneUtente = new ArrayList<>();
					if (idCanaleTrasmissioneEvento == 0) {
						idCanaliTrasmissioneUtente.add(CanaleTrasmissioneDTO.ID_EMAIL);
						idCanaliTrasmissioneUtente.add(CanaleTrasmissioneDTO.ID_RED);
					} else {
						idCanaliTrasmissioneUtente.add(idCanaleTrasmissioneEvento);
					}
					//abilitate di default
					notifica.setStatoNotifica(NotificaDTO.DA_INVIARE);
					notifica.setVisualizzato(NotificaDTO.DA_VISUALIZZARE);
					//check disabilitazione utente
					for (final Integer idCT: idCanaliTrasmissioneUtente) {
						
						final boolean isCanaleTrasmissioneDisabilitato = 
								checkCanaleTrasmissioneDisabilitato(utente.getId(), idCT);

						if (isCanaleTrasmissioneDisabilitato) {
							if (idCT.intValue() == CanaleTrasmissioneDTO.ID_EMAIL) {
								notifica.setStatoNotifica(NotificaDTO.DISABILITATA);
							} else {
								notifica.setVisualizzato(NotificaDTO.DISABILITATA);
							}
						}
					}
					//check inibizione singolo canale evento per quelli non isabilitati in precedenza
					if (idCanaleTrasmissioneEvento == CanaleTrasmissioneDTO.ID_EMAIL 
							&& notifica.getVisualizzato() != NotificaDTO.DISABILITATA) {
						notifica.setVisualizzato(NotificaDTO.NON_VISUALIZZARE);
					}
					
					if (idCanaleTrasmissioneEvento == CanaleTrasmissioneDTO.ID_RED 
							&& notifica.getStatoNotifica() != NotificaDTO.DISABILITATA) {
						notifica.setStatoNotifica(NotificaDTO.NON_INVIARE);
					}
					notificheList.add(notifica);
				}
			}
		}
		return notificheList;
	}

	/**
	 * Recupera i documenti di interesse per la tipologia di notifiche el servizio.
	 * 
	 * @param utente
	 * @param aoo
	 * @param idEvento
	 * @return
	 */
	protected abstract List<InfoDocumenti> getDataDocumenti(UtenteDTO utente, Aoo aoo, int idEvento);
	
	/**
	 * Recupera i documenti di interesse per la tipologia di notifiche el servizio.
	 * 
	 * @param utente
	 * @param aoo
	 * @param idEvento
	 * @return
	 */
	protected abstract List<InfoDocumenti> getDataDocumenti(UtenteDTO utente, Aoo aoo, int idEvento, Object objectForGetDataDocument);
	
	/**
	 * Verifica che la data scadenza sia nel range relativo di date dell'evento e che il documento sia fra quelli da tracciare.
	 * 
	 * @param evento
	 * @param dataScadenza
	 * @param idDocumento
	 * @param utente
	 * @return
	 */
	private boolean checkEvento(final EventoSottoscrizioneDTO evento, final Date dataScadenza, final int idDocumento, final UtenteDTO utente) {
		final int scadenzaDa = evento.getScadenzaDa();
		final int scadenzaA = evento.getScadenzaA();
		
		Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, scadenzaDa);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
		
        final Date dataDa = new Date(cal.getTimeInMillis());
        
        cal = Calendar.getInstance();
        cal.add(Calendar.DATE, scadenzaA);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);

        final Date dataA = new Date(cal.getTimeInMillis());

        boolean idDocIncluso = true;
        if (evento.getTracciamento() != Constants.Sottoscrizioni.TRACCIAMENTO_TUTTI) {
        	idDocIncluso = tracciaDocumentiSRV.isDocumentoTracciato(utente, idDocumento);
        }
        return (dataScadenza == null || (dataScadenza.after(dataDa) && dataScadenza.before(dataA))) 
        		&& idDocIncluso;
	}
	
	/**
	 * Verifica che le notifiche siano disabilitate per l'utente sul canale di trasmissione.
	 * 
	 * @param idUtente
	 * @param idCanaleTrasmissione
	 * @return true se disabilitato, false altrimenti
	 */
	private boolean checkCanaleTrasmissioneDisabilitato(final Long idUtente, final Integer idCanaleTrasmissione) {
		Connection con = null;
		try {
			con = setupConnection(getFilenetDataSource().getConnection(), false);
			final DisabilitazioneNotificheUtenteDTO disabilitazioneNotificheUtente = sottoscrizioniDAO.getDisabilitazioniByUserAndCanaleTrasmissioneToday("" + idUtente, idCanaleTrasmissione, con);
			if (disabilitazioneNotificheUtente != null) {
				return true;
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero delle disabilitazioni per l'utente " + idUtente, e);
			rollbackConnection(con);
			throw new RedWSException(e);
		} finally {
			closeConnection(con);
		}
		return false;
	}

	/**
	 * Registra le notifiche in input.
	 * 
	 * @param notificheList
	 */
	private void insertNotifiche(final List<NotificaDTO> notificheList) {
		Connection con = null;

		try {
			con = setupConnection(getFilenetDataSource().getConnection(), false);
			
			for (final NotificaDTO notifica: notificheList) {
				
				if (!notificaDAO.isPresente(notifica, con)) {
					notificaDAO.insert(notifica, con);
				}
			}

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di inserimento delle notifiche", e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Restituisce il provider delle properties.
	 * @return pp
	 */
	protected PropertiesProvider getPP() {
		return pp;
	}

	/**
	 * Classe che definisce le informazioni sui documenti.
	 */
	public static class InfoDocumenti {

		/**
		 * Identificativo documento.
		 */
		private Integer idDocumento;

		/**
		 * Data scadenza.
		 */
		private Date dataScadenza;

		/**
		 * Costruttore vuoto.
		 */
		public InfoDocumenti() {
			super();
		}

		/**
		 * Costruttore di default.
		 * @param idDocumento
		 * @param dataScadenza
		 */
		public InfoDocumenti(final Integer idDocumento, final Date dataScadenza) {
			this();
			this.idDocumento = idDocumento;
			this.dataScadenza = dataScadenza;
		}

		/**
		 * @return idDocumento
		 */
		public Integer getIdDocumento() {
			return idDocumento;
		}

		/**
		 * Imposta l'id del documento.
		 * @param idDocumento
		 */
		public void setIdDocumento(final Integer idDocumento) {
			this.idDocumento = idDocumento;
		}

		/**
		 * @return dataScadenza
		 */
		public Date getDataScadenza() {
			return dataScadenza;
		}

		/**
		 * Imposta la data di scadenza.
		 * @param dataScadenza
		 */
		public void setDataScadenza(final Date dataScadenza) {
			this.dataScadenza = dataScadenza;
		}

		/**
		 * Restituisce un'istanza di InfoDocumenti.
		 * @return infoDocumenti
		 */
		public static InfoDocumenti getInstance() {
			return new InfoDocumenti();
		}
	}
}