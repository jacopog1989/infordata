package it.ibm.red.business.dto;

import org.apache.commons.lang3.StringUtils;

/**
 * DTO assegnatari.
 */
public class AssegnatarioDTO extends AbstractDTO {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = 2573168672574951155L;

	/**
	 * Identificativo ufficio.
	 */
	private Long idUfficio;
	
	/**
	 * Identificativo utente.
	 */
	private Long idUtente;
	
	/**
	 * Descrizione ufficio.
	 */
	private String descrizioneUfficio;
	
	/**
	 * Descrizione utente.
	 */
	private String descrizioneUtente;


	/**
	 * Costruttore di default.
	 */
	public AssegnatarioDTO() {
		super();
	}

	/**
	 * Costruttore copia, riceve un oggetto AssegnatarioDTO e ne copie le caratteristiche.
	 * @param other
	 */
	public AssegnatarioDTO(final AssegnatarioDTO other) {
		setIdUfficio(other.getIdUfficio());
		setIdUtente(other.getIdUtente());
		setDescrizioneUfficio(other.getDescrizioneUfficio());
		setDescrizioneUtente(other.getDescrizioneUtente());
	}

	/**
	 * @param idUfficio
	 * @param idUtente
	 */
	public AssegnatarioDTO(final Long idUfficio, final Long idUtente) {
		super();
		this.idUfficio = idUfficio;
		this.idUtente = idUtente;
	}
	
	
	/**
	 * @param descrizioneUfficio
	 */
	public AssegnatarioDTO(final String descrizioneUfficio) {
		super();
		this.descrizioneUfficio = descrizioneUfficio;
	}
	
	
	/**
	 * @param descrizioneUfficio
	 * @param descrizioneUtente
	 */
	public AssegnatarioDTO(final String descrizioneUfficio, final String descrizioneUtente) {
		super();
		this.descrizioneUfficio = descrizioneUfficio;
		this.descrizioneUtente = descrizioneUtente;
	}
	
	
	/**
	 * @param idUfficio
	 * @param descrizioneUfficio
	 */
	public AssegnatarioDTO(final Long idUfficio, final String descrizioneUfficio) {
		super();
		this.idUfficio = idUfficio;
		this.descrizioneUfficio = descrizioneUfficio;
	}
	
	
	/**
	 * @param idUfficio
	 * @param idUtente
	 * @param descrizioneUfficio
	 * @param descrizioneUtente
	 */
	public AssegnatarioDTO(final Long idUfficio, final Long idUtente, final String descrizioneUfficio, final String descrizioneUtente) {
		super();
		this.idUfficio = idUfficio;
		this.idUtente = idUtente;
		this.descrizioneUfficio = descrizioneUfficio;
		this.descrizioneUtente = descrizioneUtente;
	}
	
	
	/**
	 * Ridefinizione della <code> toString() </code> per un assegnatario.
	 * 
	 * @return stringa formatta
	 */
	@Override
	public String toString() {
		String assegnatarioInteropToString = null;
		
		if (this.getIdUfficio() != null) {
			assegnatarioInteropToString = this.getIdUfficio().toString();
		} else {
			assegnatarioInteropToString = this.getDescrizioneUfficio();
		}
		
		if (this.getIdUtente() != null) {
			assegnatarioInteropToString += "," + this.getIdUtente().toString();
		} else if (StringUtils.isNotBlank(this.getDescrizioneUtente())) {
			assegnatarioInteropToString += "," + this.getDescrizioneUtente();
		}
		
		return assegnatarioInteropToString;
	}

	
	/**
	 * @return the idUfficio
	 */
	public Long getIdUfficio() {
		return idUfficio;
	}

	
	/**
	 * @return the idUtente
	 */
	public Long getIdUtente() {
		return idUtente;
	}

	
	/**
	 * @return the descrizioneUfficio
	 */
	public String getDescrizioneUfficio() {
		return descrizioneUfficio;
	}

	
	/**
	 * @return the descrizioneUtente
	 */
	public String getDescrizioneUtente() {
		return descrizioneUtente;
	}

	
	/**
	 * @param idUfficio the idUfficio to set
	 */
	public void setIdUfficio(final Long idUfficio) {
		this.idUfficio = idUfficio;
	}

	
	/**
	 * @param idUtente the idUtente to set
	 */
	public void setIdUtente(final Long idUtente) {
		this.idUtente = idUtente;
	}

	
	/**
	 * @param descrizioneUfficio the descrizioneUfficio to set
	 */
	public void setDescrizioneUfficio(final String descrizioneUfficio) {
		this.descrizioneUfficio = descrizioneUfficio;
	}

	
	/**
	 * @param descrizioneUtente the descrizioneUtente to set
	 */
	public void setDescrizioneUtente(final String descrizioneUtente) {
		this.descrizioneUtente = descrizioneUtente;
	}
	
}
