package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio di riattivazione procedimento.
 */
public interface IRiattivaProcedimentoFacadeSRV extends Serializable {
	
	/**
	 * Riattiva il documento.
	 * @param documentTitle
	 * @param utente
	 * @param motivazione
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO riattiva(String documentTitle, UtenteDTO utente, String motivazione);

	/**
	 * Verifica che il documentTitle sia riattivabile.
	 *  
	 * @param documentTitle
	 * @param utente
	 * @return
	 */
	boolean isRiattivabile(String documentTitle, UtenteDTO utente);

	/**
	 * Riattiva il documento.
	 * @param documentTitle
	 * @param utente
	 * @param motivazione
	 * @param fromPredisponi
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO riattiva(String documentTitle, UtenteDTO utente, String motivazione, boolean fromPredisponi);
}
