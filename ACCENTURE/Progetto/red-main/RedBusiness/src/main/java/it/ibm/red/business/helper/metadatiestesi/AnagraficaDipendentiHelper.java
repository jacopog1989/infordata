package it.ibm.red.business.helper.metadatiestesi;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.parser.Parser;

import it.ibm.red.business.dto.AnagraficaDipendenteDTO;
import it.ibm.red.business.logger.REDLogger;

/**
 * La classe consente di serializzare e deserializzare i metadati estesi di tipo anagrafica dipendenti provenienti dall'esterno.
 * 
 * @author a.dilegge
 *
 */
public final class AnagraficaDipendentiHelper {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AnagraficaDipendentiHelper.class.getName());
	
	/**
	 * Costruttore vuoto.
	 */
	private AnagraficaDipendentiHelper() {
		// Costruttore vuoto.
	}
	
	/**
	 * Deserializza la stringa xml generando anagrafiche sotto forma di DTO.
	 * @param xml
	 * @return lista di anagrafiche generate
	 */
	public static List<AnagraficaDipendenteDTO> deserializeAnagraficaDipendente(final String xml) {
		final List<AnagraficaDipendenteDTO> lista = new ArrayList<>();
		AnagraficaDipendenteDTO anag;
				
		final org.jsoup.nodes.Document doc = Jsoup.parse(xml, "", Parser.xmlParser());
		// Per ogni metadato nell' xml
		for (final org.jsoup.nodes.Element e : doc.select("> AnagraficaDipendente")) {
			anag = new AnagraficaDipendenteDTO();
			anag.setCodiceFiscale(e.select("> CodiceFiscaleDipendente").text());
			anag.setNome(e.select("> NomeDipendente").text());
			anag.setCognome(e.select("> CognomeDipendente").text());
			anag.setComuneNascita(e.select("> ComuneNascitaDipendente").text());
			final String sessoString = e.select("> SessoDipendente").text();
			Boolean flagMaschio = null;
			if (sessoString != null) {
				if ("M".equalsIgnoreCase(sessoString)) {
					flagMaschio = Boolean.TRUE;
				} else if ("F".equalsIgnoreCase(sessoString)) {
					flagMaschio = Boolean.FALSE;
				}
			}
			anag.setFlagMaschio(flagMaschio);
			try {
				
				final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				anag.setDataNascita(sdf.parse(e.select("> DataNascitaDipendente").text()));
			
			} catch (Exception ex) {
				LOGGER.warn(ex);
			}
			lista.add(anag);
		}
		
		return lista;
	}
	
}