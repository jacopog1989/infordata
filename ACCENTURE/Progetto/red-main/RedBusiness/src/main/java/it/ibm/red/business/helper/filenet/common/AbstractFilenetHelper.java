package it.ibm.red.business.helper.filenet.common;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import javax.security.auth.Subject;

import com.filenet.api.collection.ContentElementList;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.Connection;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;
import com.filenet.api.core.Domain;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.property.FilterElement;
import com.filenet.api.property.PropertyFilter;
import com.filenet.api.util.UserContext;

import filenet.vw.api.VWFieldDefinition;
import filenet.vw.api.VWMapDefinition;
import filenet.vw.api.VWMapNode;
import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWSession;
import filenet.vw.api.VWWorkObject;
import filenet.vw.api.VWWorkflowDefinition;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * The Class AbstractFilenetHelper.
 *
 * @author CPIERASC
 * 
 *         Classe per la gestione della connessione a Filenet.
 */
public abstract class AbstractFilenetHelper {
	
	/**
	 * Label Descrizione.
	 */
	
	private static final String DESC_LABEL = "> DESC <";
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AbstractFilenetHelper.class.getName());
	
	/**
	 * Metodo per la connessione al PE.
	 * 
	 * @param userId			identificativo utente
	 * @param password			password
	 * @param uri				uri
	 * @param connectionPoint	connection point
	 * @return					sessione
	 */
	protected final VWSession connectToPE(final String userId, final String password, final String uri, final String connectionPoint) {
		try {
			VWSession pesession = new VWSession();
			pesession.setBootstrapCEURI(uri); 
			UserContext.get();
			pesession.logon(userId, password, connectionPoint);
			return pesession;
		} catch (Exception e) {
			LOGGER.error("Errore in fase di connessione al PE", e);
			throw new FilenetException(e);
		}
	}
	
	/**
	 * Metodo per il recupero dell'objectstore.
	 * 
	 * @param objectStore	nome object store
	 * @param con			connessione
	 * @return				object store
	 */
	protected final ObjectStore getObjectStore(final String objectStore, final Connection con) {
		PropertyFilter pf = new PropertyFilter();
		pf.addIncludeProperty(new FilterElement(0, null, Boolean.TRUE, PropertyNames.NAME, null));
		pf.addIncludeProperty(new FilterElement(0, null, Boolean.TRUE, PropertyNames.DOMAIN, null));
		Domain dom = Factory.Domain.fetchInstance(con, null, pf);
		ObjectStore os = Factory.ObjectStore.getInstance(dom, objectStore);
		os.fetchProperties(pf);
		return os;
	}
	
	/**
	 * Metodo per l'inizializzazione del contesto dell'utente.
	 * 
	 * @param userId		identificativo utente
	 * @param password		password utente
	 * @param uri			uri
	 * @param stanzaJAAS	stanza jaas
	 * @return				connessione a Filenet
	 */
	protected static final Connection initUserContext(final String userId, final String password, final String uri, final String stanzaJAAS) {
		Connection con = Factory.Connection.getConnection(uri);
		Subject sub = UserContext.createSubject(con, userId, password, stanzaJAAS);
		UserContext.get().pushSubject(sub);
		
		return con;
	}
	
	
	protected final void closeUserContext() {
		UserContext.get().popSubject();
	}
	

	/**
	 * Metodo per il dump dei risultati di una query.
	 * 
	 * @param query		query
	 * @param noEmpty	flag per indicare se visualizzare o meno i campi vuoti
	 */
	protected final void dumpResult(final VWQueueQuery query, final Boolean noEmpty) {
		try {
			LOGGER.info("Dumping " + query.fetchCount() + " elements.");
			while (query.hasNext()) {
				VWWorkObject wo = (VWWorkObject) query.next();
				dumpObject(wo, noEmpty);
			}
		} catch (Exception e) {
			LOGGER.error("Errore in fase di dump dei result", e);
			throw new FilenetException(e);
		}
	}

	/**
	 * Metodo per il dump dei metadati di un work object.
	 * 
	 * @param wo			work object
	 * @param noEmpty		flag per indicare se sopprimere o meno i metadati nulli
	 */
	public static void dumpObject(final VWWorkObject wo, final Boolean noEmpty) {
		LOGGER.info("===========================================================================");
		LOGGER.info("========== WORK OBJECT NUMBER [" + wo.getWorkObjectNumber() + "] ==========");
		LOGGER.info("===========================================================================");
		for (String name:wo.getFieldNames()) {
			String data = wo.getDataField(name).getStringValue();
			if (Boolean.FALSE.equals(noEmpty) || (data.length() != 0 && !"{}".equals(data))) {
				LOGGER.info("[" + name + "] => " + "[" + data + "]");
			}
		}
	}

	/**
	 * Esegue il dump del {@code wob}.
	 * 
	 * @param wob
	 *            workflow su cui fare il dump
	 */
	protected void dump(final VWWorkObject wob) {
		VWWorkflowDefinition wfd = wob.getProcess().fetchWorkflowDefinition(false);
		for (VWFieldDefinition fd : wfd.getFields()) {
			LOGGER.info("FIELD NAME<" + fd.getName() + DESC_LABEL + fd.getDescription() + ">");
		}
		for (VWMapDefinition md : wfd.getMaps()) {
			LOGGER.info("MAP NAME<" + md.getName() + DESC_LABEL + md.getDescription() + ">");
			for (VWMapNode mn : md.getSteps()) {
				LOGGER.info("NODE NAME<" + mn.getName() + DESC_LABEL + mn.getDescription() + ">");
			}
		}
	}

	/**
	 * Metodo per il recupero del content di un documento.
	 * 
	 * @param d	documento
	 * @return	contenuto
	 */
	public final byte[] getByteArrayFromDocument(final Document d) {
		InputStream is = getInputStreamContent(d);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] output = null;
		try {
			if (is != null) {
				byte[] buf = new byte[is.available()];
				int byteRead;
				while ((byteRead = is.read(buf)) != -1) {
					bos.write(buf, 0, byteRead);
				}
				output = bos.toByteArray();
			}
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero del content del documento: ", e);
			throw new RedException(e);
		} finally {
			try { 
				if (is != null) {
					is.close();
				}
				bos.close();
			} catch (Exception e) {
				LOGGER.warn(e);
			}
		}
		return output;
	}
	
	/**
	 * Metodo per il recupero dell'input stream legato al content di un documento.
	 * 
	 * @param d	documento
	 * @return	input stream
	 */
	protected final InputStream getInputStreamContent(final Document d) {
		ContentTransfer ct = getContentTransfer(d);
		return ct.accessContentStream();
	}
	
	/**
	 * Metodo per il recupero del filename di un documento.
	 * 
	 * @param d	documento
	 * @return	filename
	 */
    public final String getFileName(final Document d) {
    	ContentTransfer ct = getContentTransfer(d);
    	return ct.get_RetrievalName();
    }
    
    /**
     * Metodo per il recupero del content type di un documento.
     * 
     * @param d	documento
     * @return	content type
     */
    public final String getContentType(final Document d) {
    	ContentTransfer ct = getContentTransfer(d);
    	return ct.get_ContentType();
    }
    
    /**
     * Metodo per il recupero del content transfer.
     * 
     * @param d	documento
     * @return	content trasfer
     */
    private static ContentTransfer getContentTransfer(final Document d) {
    	ContentElementList ceList = d.get_ContentElements();
    	return (ContentTransfer) ceList.get(0);
    }

}
