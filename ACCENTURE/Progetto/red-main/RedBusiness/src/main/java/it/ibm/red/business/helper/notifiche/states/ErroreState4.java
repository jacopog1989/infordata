package it.ibm.red.business.helper.notifiche.states;

import java.util.Map;

import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.helper.notifiche.IGestioneMailState;
import it.ibm.red.business.helper.notifiche.NotificaHelper;
import it.ibm.red.business.persistence.model.CodaEmail;

/**
 * Stato 4 - Errore.
 */
public class ErroreState4 implements IGestioneMailState {

	/**
	 * @see it.ibm.red.business.helper.notifiche.IGestioneMailState#getNextNotificaEmailToUpdate(it.ibm.red.business.helper.notifiche.NotificaHelper,
	 *      it.ibm.red.business.persistence.model.CodaEmail, java.util.Map,
	 *      boolean).
	 */
	@Override
	public CodaEmail getNextNotificaEmailToUpdate(final NotificaHelper nh, final CodaEmail notificaEmail, final Map<String, EmailDTO> notifiche, final boolean reInvio) {
		CodaEmail result = notificaEmail;
		
		int nextStatoRicevuta =  notificaEmail.getStatoRicevuta();
		nh.setGestioneMailState(null);
		result.setStatoRicevuta(nextStatoRicevuta);
		
		return result;
	}

}
