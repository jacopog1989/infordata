package it.ibm.red.business.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.io.StringWriter;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.activation.DataHandler;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.dto.NamedStreamDTO;
import it.ibm.red.business.dto.TipologiaInvio;
import it.ibm.red.business.dto.TipologiaMessaggio;
import it.ibm.red.business.exception.AdobeException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.adobe.IAdobeLCHelper;
import it.ibm.red.business.helper.adobe.dto.CountSignsOutputDTO;
import it.ibm.red.business.helper.signing.SignHelper;
import it.ibm.red.business.logger.REDLogger;

/**
 * Utility per la gestione dei file.
 *
 * @author CPIERASC
 */
public final class FileUtils {

	/**
	 * Label.
	 */
	private static final String CONTENT = "content";

	/**
	 * Codifica del BOM.
	 */
	private static final String BOM_HEX_ENCODE = "efbbbf";

	/**
	 * Label.
	 */
	private static final String NOME_FILE = "nomeFile";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FileUtils.class.getName());

	/**
	 * Buffer size.
	 */
	private static final int BUFFER_SIZE = 2048;

	/**
	 * Dimensione massima chunk dati in fase di lettura.
	 */
	private static final int CHUNK_SIZE = 16384;

	private FileUtils() {

	}

	/**
	 * Metodo per il salvataggio di un file sul filesystem (tipicamente usato in
	 * fase di test).
	 * 
	 * @param content  contenuto da salvare
	 * @param fileName path del file
	 */
	public static void saveToFile(final byte[] content, final String fileName) {
		try (
			FileOutputStream fos = new FileOutputStream(new File(fileName));
			BufferedOutputStream bs = new BufferedOutputStream(fos);
		) {
			bs.write(content);
		} catch (final Exception e) {
			LOGGER.error("FILE UTILS saveToFile(): Errore in fase di salvataggio di un file sul filesystem. ", e);
		}
	}

	/**
	 * Metodo per il recupero del contenuto di un file.
	 * 
	 * @param cl       classloader di riferimento (per il recupero da ear come
	 *                 stream)
	 * @param filename nome del file
	 * @return contenuto del file
	 */
	public static byte[] getFile(final ClassLoader cl, final String filename) {
		byte[] b = null;
		try {
			final InputStream is = cl.getResourceAsStream(filename);
			b = getByteFromInputStream(is);
			is.close();
		} catch (final Exception e) {
			LOGGER.error("FILE UTILS getFile(): Errore in fase di recupero del contenuto di un file da classloader. ", e);
		}
		return b;
	}

	/**
	 * Metodo per il recupero del contenuto di un file da file system.
	 * 
	 * @param filename nome del file
	 * @return contenuto del file
	 */
	public static byte[] getFileFromFS(final String filename) {
		byte[] b = null;
		try {
			final File f = new File(filename);
			final InputStream is = new FileInputStream(f);
			b = getByteFromInputStream(is);
			is.close();
		} catch (final Exception e) {
			LOGGER.error("FILE UTILS getFileFromFS(): Errore in fase di recupero del contenuto di un file da file system. ", e);
		}
		return b;
	}

	/**
	 * Metodo per il recupero del contenuto di un file dalla folder interna
	 * "/src/main/resources".
	 * 
	 * @param filename nome del file
	 * @return contenuto del file
	 */
	public static byte[] getFileFromInternalResources(final String filename) {
		byte[] b = null;
		InputStream is = null;
		try {
			is = FileUtils.class.getClassLoader().getResourceAsStream(filename);
			b = getByteFromInputStream(is);
			is.close();
		} catch (final Exception e) {
			LOGGER.error("FILE UTILS getFileFromInternalResources(): Errore in fase di recupero del contenuto di un file dalla folder '/src/main/resources'. ", e);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (final IOException e) {
					LOGGER.error(e);
				}
			}
		}
		return b;
	}

	/**
	 * Recupero contenuto file da input stream.
	 *
	 * @param is input stream
	 * @return contenuto file
	 */
	public static byte[] getByteFromInputStream(final InputStream is) {
		byte[] b;
		try {
			final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			int nRead;
			final byte[] data = new byte[CHUNK_SIZE];

			while ((nRead = is.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, nRead);
			}
			buffer.flush();
			b = buffer.toByteArray();
		} catch (final Exception e) {
			LOGGER.error("Errore durante il trasform da InputStream a byte[]: ", e);
			throw new RedException(e);
		}
		return b;
	}

	/**
	 * Ottieni un array di byte da un oggetto generico serializzabile.
	 * 
	 * @param object
	 * @return
	 * @throws Exception
	 */
	public static byte[] getByteObject(final Object object) {
		byte[] arrBytes = null;

		try {
			final ByteArrayOutputStream bos = new ByteArrayOutputStream();
			final ObjectOutput out = new ObjectOutputStream(bos);
			out.writeObject(object);
			arrBytes = bos.toByteArray();
			out.close();
			bos.close();
		} catch (final Exception e) {
			LOGGER.error("Errore durante il trasform da Serializable a byte[]: ", e);
			throw new RedException(e);
		}

		return arrBytes;
	}

	/**
	 * Serializza un MessaggioEmailDTO.
	 * 
	 * @param messDTO
	 * @return
	 */
	public static byte[] serializeMessaggioEmail(final MessaggioEmailDTO messDTO) {
		byte[] arrBytes = null;

		try {
			arrBytes = new JSONSerializer().include("*.class", "allegatiDocTitle").serialize(messDTO)
					.replace(MessaggioEmailDTO.class.getCanonicalName(), "com.accenture.nsd.email.Messaggio")
					.replace(TipologiaInvio.class.getCanonicalName(), "com.accenture.nsd.controller.ws.FWSServiceStub$TipologiaInvio")
					.replace(TipologiaMessaggio.class.getCanonicalName(), "com.accenture.nsd.controller.ws.FWSServiceStub$TipologiaMessaggio")
					.replace("\"ARCHIVIATA_DEFINITIVAMENTE\"", "0").replace("\"INARRIVO\"", "1").replace("\"ELIMINATA\"", "2").replace("\"IMPORTATA\"", "3")
					.replace("\"NOTIFICATA\"", "4").replace("\"INOLTRATA\"", "5").replace("\"RIFIUTATA\"", "6").replace("\"ARCHIVIATA\"", "7")
					.replace("\"IN_FASE_DI_PROTOCOLLAZIONE_MANUALE\"", "9").replace("\"IN_FASE_DI_PROTOCOLLAZIONE_AUTOMATICA\"", "10")
					.replace("\"ERRORE_DI_PROTOCOLLAZIONE\"", "11").replace("\"PROTOCOLLATA\"", "12").replace("\"INARRIVO_NON_PROTOCOLLABILE_AUTOMATICAMENTE\"", "13")
					.replace("\"ERRORE_DI_POST_PROTOCOLLAZIONE\"", "14").getBytes();
		} catch (final Exception e) {
			LOGGER.error("Errore durante il trasform da Serializable a byte[]: ", e);
			throw new RedException(e);
		}

		return arrBytes;
	}

	/**
	 * Metodo per creare un file zip.
	 *
	 * @param files the files
	 * @return file zippato
	 */
	public static byte[] writeZipFile(final Map<String, byte[]> files) {
		final ByteArrayOutputStream bos = new ByteArrayOutputStream();
		final ZipOutputStream out = new ZipOutputStream(bos);
		try {
			BufferedInputStream origin;

			final byte[] data = new byte[BUFFER_SIZE];

			for (final Entry<String, byte[]> entry : files.entrySet()) {
				final String fileName = entry.getKey();
				final ByteArrayInputStream fi = new ByteArrayInputStream(entry.getValue());
				origin = new BufferedInputStream(fi, BUFFER_SIZE);
				final ZipEntry zip = new ZipEntry(fileName);
				out.putNextEntry(zip);
				int count;
				while ((count = origin.read(data, 0, BUFFER_SIZE)) != -1) {
					out.write(data, 0, count);
				}
				origin.close();
			}
			out.flush();
			out.close();
		} catch (final Exception e) {
			LOGGER.error("FILE UTILS writeZipFile(): Errore in fase di creazione di un file zip. ", e);
		}
		return bos.toByteArray();
	}

	/**
	 * @param mimeType
	 * @return
	 */
	public static String getExtensionFileByMimeType(final String mimeType) {
		String extension = "";
		if (mimeType.equals(ContentType.PDF)) {
			extension = "pdf";
		} else if (mimeType.equals(ContentType.DOC)) {
			extension = "doc";
		} else if (mimeType.equals(ContentType.RTF)) {
			extension = "rtf";
		} else if (mimeType.equals(ContentType.ZIP_MIME)) {
			extension = "zip";
		} else if (mimeType.equals(ContentType.XLS)) {
			extension = "xls";
		} else if (mimeType.equals(ContentType.GIF)) {
			extension = "gif";
		} else if (mimeType.equals(ContentType.JPG)) {
			extension = "jpg";
		} else if (mimeType.equals(ContentType.TIFF)) {
			extension = "tiff";
		} else if (mimeType.equals(ContentType.HTML)) {
			extension = "htm";
		} else if (mimeType.equals(ContentType.XML)) {
			extension = "xml";
		} else if (mimeType.equals(ContentType.P7M_X_MIME) || mimeType.equals(ContentType.P7M_MIME) || mimeType.equals(ContentType.P7M)) {
			extension = "p7m";
		} else if (mimeType.equals(ContentType.DOCX)) {
			extension = "docx";
		} else if (mimeType.equals(ContentType.XLSX)) {
			extension = "xlsx";
		} else if (mimeType.equals(ContentType.TEXT)) {
			extension = "txt";
		}
		return extension;
	}

	/**
	 * Restituisce il mime type in base all'estensione del file.
	 * 
	 * @param estensione
	 * @return mimetype
	 */
	public static String getMimeType(final String estensione) {
		String mimetype = ContentType.MIMETYPE_ENC;

		if ("pdf".equalsIgnoreCase(estensione)) {
			mimetype = ContentType.PDF;
		} else if ("doc".equalsIgnoreCase(estensione)) {
			mimetype = ContentType.DOC;
		} else if ("docx".equalsIgnoreCase(estensione)) {
			mimetype = ContentType.DOCX;
		} else if ("xls".equalsIgnoreCase(estensione)) {
			mimetype = ContentType.XLS;
		} else if ("xlsx".equalsIgnoreCase(estensione)) {
			mimetype = ContentType.XLSX;
		} else if ("rtf".equalsIgnoreCase(estensione)) {
			mimetype = ContentType.RTF;
		} else if ("zip".equalsIgnoreCase(estensione)) {
			mimetype = ContentType.ZIP_MIME;
		} else if ("gif".equalsIgnoreCase(estensione)) {
			mimetype = ContentType.GIF;
		} else if ("jpg".equalsIgnoreCase(estensione) || "jpeg".equalsIgnoreCase(estensione)) {
			mimetype = ContentType.JPG;
		} else if ("tiff".equalsIgnoreCase(estensione) || "tif".equalsIgnoreCase(estensione)) {
			mimetype = ContentType.TIFF;
		} else if ("htm".equalsIgnoreCase(estensione) || "html".equalsIgnoreCase(estensione) || "xhtml".equalsIgnoreCase(estensione)) {
			mimetype = ContentType.HTML;
		} else if ("xml".equalsIgnoreCase(estensione)) {
			mimetype = ContentType.XML;
		} else if ("p7m".equalsIgnoreCase(estensione)) {
			mimetype = ContentType.P7M_X_MIME;
		} else if ("eml".equalsIgnoreCase(estensione)) {
			mimetype = ContentType.EML;
		} else if ("msg".equalsIgnoreCase(estensione)) {
			mimetype = ContentType.MSG;
		} else if ("txt".equalsIgnoreCase(estensione)) {
			mimetype = ContentType.TEXT;
		} else if ("png".equalsIgnoreCase(estensione)) {
			mimetype = ContentType.PNG;
		} else if ("bmp".equalsIgnoreCase(estensione)) {
			mimetype = ContentType.BMP;
		} else if ("7z".equalsIgnoreCase(estensione)) {
			mimetype = ContentType.ZIP7Z_MIME;
		}
		return mimetype;
	}

	/**
	 * Metodo per capire se un file è di tipo html.
	 * 
	 * @param filename nome del file
	 * @return esito test
	 */
	public static boolean isHtmlFile(final String filename) {
		boolean ret = false;
		final String extension = FilenameUtils.getExtension(filename);
		if ("html".equalsIgnoreCase(extension) || "htm".equalsIgnoreCase(extension) || "xhtml".equalsIgnoreCase(extension)) {
			ret = true;
		}

		return ret;
	}

	/**
	 * Ottieni un Object generico da Input stream.
	 * 
	 * @param inputStream
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws Exception
	 */
	private static Object getObjectFromInputstream(final InputStream inputStream) throws IOException, ClassNotFoundException {
		final ObjectInput in = new ObjectInputStream(inputStream);
		final Object o = in.readObject();
		inputStream.close();
		in.close();

		if (o instanceof byte[]) {
			return getObjectFromInputstream(new ByteArrayInputStream((byte[]) o));
		} else {
			return o;
		}
	}

	/**
	 * Ottieni un Object generico da byte[].
	 * 
	 * @param inputStream
	 * @return
	 * @throws Exception
	 */
	public static Object getObjectFromByteArray(final byte[] data) {
		Object output = null;

		try {
			final InputStream in = new ByteArrayInputStream(data);

			output = getObjectFromInputstream(in);

		} catch (final StreamCorruptedException e) {
			
			LOGGER.warn(e);
			final String messaggioJson = new String(data).replace("com.accenture.nsd.email.Messaggio", MessaggioEmailDTO.class.getCanonicalName())
					.replace("com.accenture.nsd.controller.ws.FWSServiceStub$TipologiaInvio", TipologiaInvio.class.getCanonicalName())
					.replace("com.accenture.nsd.controller.ws.FWSServiceStub$TipologiaMessaggio", TipologiaMessaggio.class.getCanonicalName());

			output = new JSONDeserializer<MessaggioEmailDTO>().deserialize(messaggioJson, MessaggioEmailDTO.class);
		
		} catch (IOException | ClassNotFoundException e) {
			throw new RedException(e);
		} 

		return output;
	}

	/**
	 * Estrae il P7M in input.
	 * 
	 * @param fi                  il p7m da sbustare
	 * @param diskFileItemFactory
	 * @param pUrlHandler         url PK
	 * @param pSecurePin          secure PIN PK
	 * @return
	 */
	public static FileItem p7mExtract(final FileItem inFi, final DiskFileItemFactory diskFileItemFactory, final String pUrlHandler, final byte[] pSecurePin,
			final boolean disableUseHostOnly) {

		FileItem fi = inFi;

		if (fi != null && fi.getName().endsWith(".p7m")) {
			final Map<String, Object> map = recursiveExtractP7M(fi.get(), fi.getName(), pUrlHandler, pSecurePin, disableUseHostOnly);
			fi.delete();

			fi = diskFileItemFactory.createItem((String) map.get(NOME_FILE), URLConnection.getFileNameMap().getContentTypeFor((String) map.get(NOME_FILE)), false,
					(String) map.get(NOME_FILE));
			try {
				IOUtils.copy(new ByteArrayInputStream((byte[]) map.get(CONTENT)), fi.getOutputStream());
			} catch (final IOException e) {
				LOGGER.error("Errore in fase di copia del byte[] estratto ", e);
				throw new RedException(e);
			}
		}

		return fi;

	}

	// START VI
	private static HashMap<String, Object> recursiveExtractP7M(final byte[] documento, final String nomeDocumento, final String pUrlHandler, final byte[] pSecurePin,
			final boolean disableUseHostOnly) {
		return recursiveExtractP7M(documento, nomeDocumento, pUrlHandler, pSecurePin, false, disableUseHostOnly);
	}

	/**
	 * Sbusta in modalità ricorsiva il P7M in input.
	 * 
	 * @param documento     il p7m da sbustare
	 * @param nomeDocumento
	 * @param map
	 * @param pUrlHandler
	 * @param pSecurePin
	 * @return
	 */
	// START VI PK HANDLER
	private static HashMap<String, Object> recursiveExtractP7M(final byte[] documento, final String nomeDocumento, final String pUrlHandler, final byte[] pSecurePin,
			final boolean verificaFirma, final boolean disableUseHostOnly) {
		HashMap<String, Object> map = null;

		String nomeFileSenzaUltimaEst = nomeDocumento.substring(0, nomeDocumento.length() - 4);

		final byte[] doc = extractP7mContent(documento, pUrlHandler, pSecurePin, verificaFirma, disableUseHostOnly);

		if (nomeFileSenzaUltimaEst.toLowerCase().endsWith(".p7m")) {
			try {
				return recursiveExtractP7M(doc, nomeFileSenzaUltimaEst, pUrlHandler, pSecurePin, verificaFirma, disableUseHostOnly);
			} catch (final Exception e) {
				
				LOGGER.warn(e);
				// il nome del file contiene dei .p7m di troppo
				do {
					nomeFileSenzaUltimaEst = nomeFileSenzaUltimaEst.substring(0, nomeFileSenzaUltimaEst.length() - 4);
				} while (nomeFileSenzaUltimaEst.toLowerCase().endsWith(".p7m"));
			}
		}

		map = new HashMap<>();
		map.put(NOME_FILE, nomeFileSenzaUltimaEst);
		map.put(CONTENT, doc);

		return map;
	}

	// START VI PK HANDLER
	private static byte[] extractP7mContent(final byte[] documento, final String pUrlHandler, final byte[] pSecurePin, final boolean verifySignature,
			final boolean disableUseHostOnly) {
		final SignHelper sh = new SignHelper(pUrlHandler, pSecurePin, disableUseHostOnly);
		byte[] doc;

		if (verifySignature) {
			doc = sh.extractP7MAndVerifySignature(documento);
		} else {
			doc = sh.extractP7M(documento);
		}
		if (doc == null || doc.length == 0) {
			throw new RedException("Errore in fase di estrazione: null ricevuto da pkBox.extractP7M");
		}

		return doc;
	}

	/**
	 * Restituisce il file p7m estratto.
	 * 
	 * @param documento
	 * @param nomeDocumento
	 * @param pUrlHandler
	 * @param pSecurePin
	 * @param disableUseHostOnly
	 * @return file come NamedStreamDTO
	 */
	public static NamedStreamDTO recursiveExtractP7MFile(final byte[] documento, final String nomeDocumento, final String pUrlHandler, final byte[] pSecurePin,
			final boolean disableUseHostOnly) {
		NamedStreamDTO file;

		final HashMap<String, Object> mappa = recursiveExtractP7M(documento, nomeDocumento, pUrlHandler, pSecurePin, false, disableUseHostOnly);

		final byte[] inContent = (byte[]) mappa.get(CONTENT);
		final String inFileName = (String) mappa.get(NOME_FILE);
		file = new NamedStreamDTO();
		file.setContent(inContent);
		file.setName(inFileName);

		return file;
	}

	/**
	 * Controlla che il documento abbia almeno un campo firma VUOTO.
	 * 
	 * @param doc inputStream di un PDF
	 * @return <code>true</code> se in <code>doc</code> è presente almeno un campo
	 *         firma
	 * @throws AdobeException l'eccezione viene sollevata anche se <code>doc</code>
	 *                        non è un PDF.
	 */
	public static boolean hasEmptySignFields(final InputStream doc, final IAdobeLCHelper adobeh) {
		Integer numEmptySignFields = 0;
		final CountSignsOutputDTO out = adobeh.countsSignedFields(doc);
		numEmptySignFields = out.getNumEmptySignFields();

		return (numEmptySignFields != null && numEmptySignFields > 0);
	}

	/**
	 * Ottieni un oggetto DataHandler da InputStream.
	 * 
	 * @param is
	 * @return
	 * @throws IOException
	 */
	public static DataHandler toDataHandler(final InputStream is) throws IOException {
		return toDataHandler(is, null);
	}

	/**
	 * Ottieni un oggetto DataHandler da InputStream.
	 * 
	 * @param is
	 * @param contentType
	 * @return
	 * @throws IOException
	 */
	public static DataHandler toDataHandler(final InputStream is, final String contentType) throws IOException {
		return toDataHandler(IOUtils.toByteArray(is), contentType);
	}

	/**
	 * {@link #toDataHandler(byte[], String)}
	 * 
	 * @param content
	 * @return DataHandler
	 */
	public static DataHandler toDataHandler(final byte[] content) {
		return toDataHandler(content, null);
	}

	/**
	 * Restituisce il DataHandler per la gestione del content.
	 * 
	 * @param content
	 * @param contentType
	 * @return DataHandler
	 */
	public static DataHandler toDataHandler(final byte[] content, final String contentType) {
		ByteArrayDataSource rawData;
		DataHandler dataHandler = null;

		if (contentType != null) {
			rawData = new ByteArrayDataSource(content, contentType);
		} else {
			// Default content type: "application/octet-stream"
			rawData = new ByteArrayDataSource(content, "application/octet-stream");
		}

		dataHandler = new DataHandler(rawData);

		return dataHandler;
	}

	/**
	 * Converte il contenuto di un InputStream in una stringa.
	 * 
	 * @param is
	 * @param charset
	 * @return
	 * @throws IOException
	 */
	public static String inToString(final InputStream is, final String charset) throws IOException {
		InputStreamReader isr;
		if (charset == null || charset.length() == 0) {
			isr = new InputStreamReader(is);
		} else {
			isr = new InputStreamReader(is, charset);
		}

		final char[] buffer = new char[8192];
		final StringBuilder content = new StringBuilder();
		int read = 0;
		while ((read = isr.read(buffer)) != -1) {
			content.append(buffer, 0, read);
		}
		isr.close();

		return content.toString();
	}

	/**
	 * Gestisce la serializzazione di un oggetto.
	 * 
	 * @param obj
	 * @return restituisce l'oggetto serializzato
	 */
	public static byte[] serializeObj(final Object obj) {
		byte[] output = new byte[0];
		try (
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutput out = new ObjectOutputStream(bos);
		){
			out.writeObject(obj);
			out.flush();
			output = bos.toByteArray();
		} catch (final Exception e) {
			LOGGER.error(e);
		}
		return output;
	}

	/**
	 * Trasforma il contenuto dell'InputStream sostituendo il separator "\r" con
	 * spazi vuoti e "\n" con "<br>
	 * ".
	 * 
	 * @param is
	 * @return byte array del file trasformato
	 */
	public static byte[] toByteArrayHtml(final InputStream is) {
		byte[] output = null;
		try {
			final StringWriter writer = new StringWriter();
			IOUtils.copy(is, writer, null);
			final String strToConvert = writer.toString().replace("\\r", "").replace("\\n", "<br>");
			output = strToConvert.getBytes();
		} catch (final IOException e) {
			LOGGER.error(e);
		}

		return output;
	}

	/**
	 * Restituisce un clone dell'oggetto passatogli. L'oggetto ed i suoi attributi
	 * DEVONO essere serializable
	 */
	public static Object deepClone(final Object object) {
		Object output = null;
		try {
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			final ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(object);
			final ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			final ObjectInputStream ois = new ObjectInputStream(bais);
			output = ois.readObject();
		} catch (final Exception e) {
			LOGGER.error(e);
		}
		return output;
	}

	/**
	 * Estrae il primo livello dello zip e lo converte in NamedStreamDTO
	 * valorizzando solo il nome.
	 * 
	 * @param zipContent
	 * @return
	 * @throws IOException
	 */
	public static List<NamedStreamDTO> extractZipEntryName(final byte[] zipContent) throws IOException {
		final List<NamedStreamDTO> fileList = new ArrayList<>();

		try (InputStream bais = new ByteArrayInputStream(zipContent); ZipInputStream zipStream = new ZipInputStream(bais, Charset.forName("Cp437"));) {
			ZipEntry entry;
			while ((entry = zipStream.getNextEntry()) != null) {
				final String name = entry.getName();
				final NamedStreamDTO entryFile = new NamedStreamDTO();
				final byte[] unzippedFile = IOUtils.toByteArray(zipStream);
				entryFile.setContent(unzippedFile);
				entryFile.setName(name);
				fileList.add(entryFile);
			}
		}

		return fileList;
	}

	/**
	 * Estrae il primo livello dello zip recupera il contenuto in byte del file
	 * unzippato con nome name.
	 * 
	 * @param zipContent
	 * @param name       nome dello zipEntry di interesse
	 * @return
	 * @throws IOException
	 */
	public static byte[] extractZipEntryByName(final byte[] zipContent, final String name) throws IOException {
		byte[] unzippedFile = null;

		try (InputStream bais = new ByteArrayInputStream(zipContent); ZipInputStream zipStream = new ZipInputStream(bais, Charset.forName("Cp437"));) {
			ZipEntry entry;
			while ((entry = zipStream.getNextEntry()) != null) {
				final String nameEntry = entry.getName();
				if (name.equals(nameEntry)) {
					unzippedFile = IOUtils.toByteArray(zipStream);
					break;
				}
			}
		}

		return unzippedFile;
	}

	/**
	 * Decodifica l'InputStream codificato in base 64.
	 * 
	 * @param is
	 * @return byte array del file decodificato
	 */
	public static byte[] decodeBuffer(final InputStream is) {
		byte[] output = null;
		try {
			output = new sun.misc.BASE64Decoder().decodeBuffer(is);
		} catch (final IOException e) {
			LOGGER.error("FILE UTILS decodeBuffer(): Errore in fase di decodifica un input stream in base64. ", e);
		}
		return output;
	}

	/**
	 * @param is
	 * @return .
	 */
	public static String encodeBase64(final InputStream is) {
		String base64 = null;
		try {
			final byte[] data = IOUtils.toByteArray(is);
			base64 = new String(Base64.encodeBase64(data));
		} catch (final IOException e) {
			throw new RedException("Errore nella conversione in base64 del file. ", e);
		}
		return base64;
	}

	/**
	 * Restituisce true se il file è un P7M, false altrimenti.
	 * 
	 * @param mimeType
	 * @return true se P7M, false altrimenti
	 */
	public static boolean isP7MFile(final String mimeType) {
		return ContentType.P7M_X_MIME.equalsIgnoreCase(mimeType) || ContentType.P7M_MIME.equalsIgnoreCase(mimeType) || ContentType.P7M.equalsIgnoreCase(mimeType)
				|| ContentType.P7M_PKNET_MANAGER.equalsIgnoreCase(mimeType);
	}
	
	/**
	 * Esegue il parsing del file rimuovendo i caratteri del BOM se presenti.
	 * @param fileWithBOM
	 * @return byte array del content senza BOM
	 */
	public static byte[] removeBOM(byte[] fileWithBOM) {

		if (isBOMPresent(fileWithBOM)) {
			ByteBuffer bb = ByteBuffer.wrap(fileWithBOM);

			// Se il BOM è presente, è nei primi 3 byte
			byte[] bom = new byte[3];
			bb.get(bom, 0, bom.length);

			byte[] contentAfterFirst3Bytes = new byte[fileWithBOM.length - 3];
			bb.get(contentAfterFirst3Bytes, 0, contentAfterFirst3Bytes.length);

			return contentAfterFirst3Bytes;
		} else {
			return fileWithBOM;
		}

	}
	
	/**
	 * Restituisce true se il content contiene il BOM.
	 * @param content
	 * @return true se BOM è presente, false altrimenti
	 * @throws IOException
	 */
	private static boolean isBOMPresent(byte[] content){
		boolean result = false;

		byte[] bom = new byte[3];
		try (ByteArrayInputStream is = new ByteArrayInputStream(content)) {
			int bytesReaded = is.read(bom);

			if(bytesReaded != -1) {
				String stringContent = new String(Hex.encodeHex(bom));
				if (BOM_HEX_ENCODE.equalsIgnoreCase(stringContent)) {
					LOGGER.info("BOM identificato nel content.");
					result = true;
				}
			}
		} catch (Exception e) {
			LOGGER.error("Errore riscontrato durante il tentativo di verifica di esistenza del BOM.", e);
		}

		return result;
	}
	
}