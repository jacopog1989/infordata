package it.ibm.red.business.service.pstrategy;

import java.io.IOException;
import java.util.List;

import org.apache.pdfbox.io.IOUtils;

import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.MessaggioPostaNpsDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO;
import it.ibm.red.business.dto.TipologiaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;

/**
 * Strategy protcollazione automatica BASIC.
 */
public class BASICAutoProtocolStrategy extends AbstractAutoProtocolStrategy<MessaggioPostaNpsDTO> {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(BASICAutoProtocolStrategy.class.getName());

	/**
	 * @see it.ibm.red.business.service.pstrategy.IAutoProtocolStrategy#protocol(it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO, java.lang.String)
	 **/
	@Override
	public EsitoSalvaDocumentoDTO protocol(final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsDTO> richiestaElabMessagio, final String guidFilenetMessaggio) {
		LOGGER.info("Avvio protocollazione automatica interoperabile.");
		LOGGER.info("ID AOO: " + richiestaElabMessagio.getIdAoo());
		
		final ProtocolloNpsDTO protocollo = getDettagliProtocollo(richiestaElabMessagio.getIdAoo().intValue(), richiestaElabMessagio.getMessaggio().getProtocollo());

		byte[] content = null;
		try {
			content = IOUtils.toByteArray(protocollo.getDocumentoPrincipale().getInputStream());
		} catch (final IOException e) {
			LOGGER.error("Errore in fase di recupero content del documento principale.", e);
			throw new RedException("Errore in fase di recupero content del documento principale.", e);
		}
		final String fileName = protocollo.getDocumentoPrincipale().getNomeFile();
		final String mType = protocollo.getDocumentoPrincipale().getContentType();
		
		final TipologiaDTO tipologia = getTipologiaFromProtocolloNPS(richiestaElabMessagio.getIdAoo(), protocollo);
		final Integer idTipologiaDocumento = tipologia.getIdTipoDocumento();
		final Integer idTipologiaProcedimento = tipologia.getIdTipoProcedimento();
		final String descTipoDoc = getDescTipoDocFromID(idTipologiaDocumento);
		final String oggetto = protocollo.getOggetto();
		final String descrizioneFascicoloProcedimentale = descTipoDoc + " - " + oggetto;
		final String indiceClassificazioneFascicoloProcedimentale = protocollo.getDescrizioneTitolario();
		final List<AssegnazioneDTO> assegnazioni = getAssegnazioni(protocollo);
		final UtenteDTO utenteCreatore = getUtenteProtocollatore(protocollo);
		
		final String mail = (richiestaElabMessagio.getMessaggio() != null && richiestaElabMessagio.getMessaggio().getMittente() != null) 
				? richiestaElabMessagio.getMessaggio().getMittente().getMail() : null;
		final Contatto contattoMittente = getMittente(protocollo.getMittente(), utenteCreatore, mail);
		final Integer idMittenteContatto = contattoMittente.getContattoID().intValue();
		
		final TipoContestoProceduraleEnum tcp = TipoContestoProceduraleEnum.BASIC;
		// N.B. Nella strategia BASIC non è previsto l'utilizzo dell'ID di processo, non appartenendo ad un flusso.
		
		final EsitoSalvaDocumentoDTO esito = creaIngresso(tcp, oggetto, utenteCreatore, idMittenteContatto, idTipologiaDocumento, 
				idTipologiaProcedimento, assegnazioni, null, content, fileName, 
				mType, null, null, guidFilenetMessaggio, indiceClassificazioneFascicoloProcedimentale, 
				descrizioneFascicoloProcedimentale, protocollo, null, null, false, false);
		
		LOGGER.info("Fine protocollazione automatica interoperabile.");
		return esito;
	}
	
}
