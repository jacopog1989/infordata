package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.CodaTrasformazioneDTO;
import it.ibm.red.business.dto.CodaTrasformazioneParametriDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.enums.StatoCodaTrasformazioneEnum;

/**
 * The Interface ITrasformazionePDFFacadeSRV.
 *
 * @author a.dilegge
 * 
 *         Facade del servizio di gestione delle trasformazioni in PDF dei documenti.
 */
public interface ITrasformazionePDFFacadeSRV extends Serializable {
	
	/**
	 * Metodo per il recupero dell'item da trasformare sulla base dati. Lo stato dell'item viene modificato 
	 * in modo da non essere preso in carico da nessun altro.
	 * 
	 * @param idAoo
	 *  
	 * @return	item recuperato, null in caso la coda non contenga item da trasformare
	 */
	CodaTrasformazioneDTO getItem(int idAoo);
	
	/**
	 * Modifica lo stato dell'item identificato dall'idCoda in input.
	 * 
	 * @param idCoda
	 * @param stato
	 * @param messaggioErrore
	 */
	void changeStatusItem(int idCoda, StatoCodaTrasformazioneEnum stato, String messaggioErrore);
	
	/**
	 * Aggiorna l'item identificato dall'idCoda in input in modo da: <br/>
	 * <ul>
	 * <li>settare lo stato ad elaborato</li>
	 * <li>registrare i secondi di lavorazione (in input)</li>
	 * <li>eliminare il blob parametri</li>
	 * <li>aggiornare a "" il messaggio d'errore</li>
	 * </ul>
	 * 
	 * @param idCoda
	 * @param secondElapsed
	 */
	void setItemDone(int idCoda, long secondElapsed);

	/**
	 * Metodo che esegue la trasformazione in PDF del documento registrato all'interno dei parametri in input.
	 * 
	 * @param parametri
	 * @param utente
	 */
	void doTransformation(CodaTrasformazioneParametriDTO parametri);

	/**
	 * Verifica se il content è trasformabile in PDF.
	 * @param mimeType
	 * @param estensione
	 * @return true o false
	 */
	boolean isContentTrasformabile(String mimeType, String estensione);
	
	/**
	 * @param idAoo
	 * @param inputFile
	 * @param inNumeroDocumento
	 * @param inProtocollo
	 * @param idFirmatari
	 * @param entrata
	 * @param principale
	 * @param applicaTimbroProtocollo
	 * @param applicaTimbroNumeroDocumento
	 * @param apponiCampoFirma
	 * @param firmaMultipla
	 * @param applicaAncheSenzaTag
	 * @param mantieniFormatoOriginale
	 * @param stampigliaturaSigla
	 * @param idSiglatario
     * @param idUffCreatore
     * @param idTipoDoc
	 * @return
	 */
	FileDTO convertiInPDFConStampigliature(Long idAoo, FileDTO inputFile, Integer inNumeroDocumento, String inProtocollo, 
			List<Integer> idFirmatari, boolean entrata, boolean principale, boolean applicaTimbroProtocollo, boolean applicaTimbroNumeroDocumento, 
			boolean apponiCampoFirma, boolean firmaMultipla, boolean applicaAncheSenzaTag, boolean mantieniFormatoOriginale, boolean stampigliaturaSigla,
			Long idSiglatario, Long idUffCreatore, Integer idTipoDoc);
	
	/**
	 * Esegue la trasformazione del content di un documento, gestisce l'invio della mail e i possibili errori generati dalla trasformazione.
	 * 
	 * @param idAoo
	 * */
	void cleanTransformation(int idAoo);

	/**
	 * Restituisce il testo da utilizzare per la creazione della postilla per l'area
	 * organizzativa identificata dall' {@code idAoo}.
	 * 
	 * @param idAOO
	 *            identificativo dell'area organizzativa
	 * @return testo postilla associato all'area organizzativa
	 */
	String getTestoPostilla(Long idAOO);
}
