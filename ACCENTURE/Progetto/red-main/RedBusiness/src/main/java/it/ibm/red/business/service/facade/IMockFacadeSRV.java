package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.enums.TipoDocumentoMockEnum;
import it.ibm.red.business.enums.TipoProtocolloEnum;

/**
 * Facade del servizio che gestisce il mock dei servizi.
 */
public interface IMockFacadeSRV extends Serializable {
	
	/**
	 * Stacca il nuovo protocollo mock.
	 * @param codiceAoo
	 * @param tipoProtocollo
	 * @return protocollo mock
	 */
	ProtocolloDTO getNewProtocollo(String codiceAoo, TipoProtocolloEnum tipoProtocollo);
	
	/**
	 * Ottiene il protocollo mock.
	 * @param idProtocollo
	 * @return protocollo mock
	 */
	ProtocolloDTO getProtocollo(String idProtocollo);
	
	/**
	 * Ottiene il documento mock.
	 * @param tipoDocumento
	 * @param serverName
	 * @return file
	 */
	FileDTO getDocumento(TipoDocumentoMockEnum tipoDocumento, String serverName);
	
	/**
	 * Ottiene i protocolli mock.
	 * @param codiceAoo
	 * @param numeroProtocolloDa
	 * @param numeroProtocolloA
	 * @param maxResults
	 * @return protocolli mock
	 */
	List<ProtocolloDTO> getProtocolli(String codiceAoo, Integer numeroProtocolloDa, Integer numeroProtocolloA, int maxResults);
	
}
