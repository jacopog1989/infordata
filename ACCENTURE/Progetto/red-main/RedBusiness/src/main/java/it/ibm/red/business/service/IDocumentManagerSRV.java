package it.ibm.red.business.service;

import java.util.List;

import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV;

/**
 * Interface del servizio che gestisce il document manager.
 */
public interface IDocumentManagerSRV extends IDocumentManagerFacadeSRV {
	
	/**
	 * Ottiene la lista dei formati dei documenti dai permessi dell'utente.
	 * @param permessi
	 * @return lista dei formati dei documenti
	 */
	List<FormatoDocumentoEnum> getComboFormatoDocumentoByPermessiUtente(Long permessi);
	
	/**
	 * Verifica la visibilità della combo per i formati del documento.
	 * @param modalitaMaschera
	 * @param categoria
	 * @return true o false
	 */
	boolean isComboFormatiDocumentoVisible(String modalitaMaschera, CategoriaDocumentoEnum categoria);

	/**
	 * Verifica la visibilità di mittenti.
	 * @param categoria
	 * @return true o false
	 */
	boolean isMittentiVisible(CategoriaDocumentoEnum categoria);
	
	/**
	 * Verifica la visibilità dei destinatari.
	 * @param categoria
	 * @return
	 */
	boolean isDestinatariVisible(CategoriaDocumentoEnum categoria);
	
	/**
	 * Verifica la visibilità di Da non protocollare.
	 * @param isDocInModifica
	 * @param categoria
	 * @return true o false
	 */
	boolean isDaNonProtocollareVisible(boolean isDocInModifica, CategoriaDocumentoEnum categoria);

	/**
	 * Verifica la visibilità del tab di spedizione.
	 * @param categoria
	 * @return
	 */
	boolean isTabSpedizioneVisible(CategoriaDocumentoEnum categoria);

}
