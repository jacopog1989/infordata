package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * The Interface IVistiFacadeSRV.
 *
 * @author a.dilegge
 * 
 *         Facade del servizio di gestione dei visti.
 */
public interface IRichiesteVistoFacadeSRV extends Serializable {

	/**
	 * Assegna la sicurezza ai faldoni e al docuemnto e avanza il Workflow per la
	 * response richiedi visti.
	 * 
	 * Questa operazione viene svolta dalla segreteria di un ufficio affinché il
	 * documento venga smistato tra i propri dipendenti
	 * 
	 * @param utente                Richiedente dell'operazione
	 * @param wobNumbers            lista di workflow che verranno avanzati nello
	 *                              stato richiedi vista
	 * @param motivazione           Descrizione della motivazione per cui è stata
	 *                              eseguita questa operazione
	 * @param assegnazioneVistoList Insieme di utenti a cui Richiedere il visto
	 * 
	 * @return Esito dell'operazione effettuata, qualora l'operazione si sia
	 *         completata prima che venisse recuperato il documentTitle, solo il
	 *         wobNumber è valorizzato.
	 */
	EsitoOperazioneDTO richiediVisti(UtenteDTO utente, String wobNumbers, String motivazione, Collection<AssegnazioneDTO> assegnazioneVistoList);

	/**
	 * Assegna un docuemnto in visto a un ufficio o ad un ispettorato perché venga
	 * preso in carico dalla segreteria, e quindi smistato al responsabile
	 * dell'ufficio selezionato.
	 * 
	 * @param utente                           mittente dall'operazione
	 * @param wobNumbers                       Documento a cui applicare
	 *                                         l'operazione
	 * @param motivazione                      motivazione addotta dall'utente per
	 *                                         giustificare l'operaizone di
	 *                                         richiesta visto
	 * @param assegnazioneVistoList            lista di assegnazioni con cui
	 *                                         calcolare le security
	 * @param assegnazioneUfficioVistoList     lista di uffici a cui assegnare il
	 *                                         visto
	 * @param assegnazioneIspettoratoVistoList lista di ispettorati a cui assegnare
	 *                                         il visto
	 * @return Esito dell'operazione effettuata, qualora l'operazione si sia
	 *         completata prima che venisse recuperato il documentTitle, solo il
	 *         wobNumber è valorizzato.
	 */
	EsitoOperazioneDTO richiestaVisti(UtenteDTO utente, String wobNumber, String motivazione, List<AssegnazioneDTO> assegnazioneVistoList, final boolean isDestDelegato);

	/**
	 * Assegna un docuemnto in visto a un ufficio o ad un ispettorato perché venga
	 * preso in carico dalla segreteria, e quindi smistato al responsabile
	 * dell'ufficio selezionato. Questo serve a implementare quello richiamato da
	 * richiesta nVisti 2
	 * 
	 * @param utente                           mittente dall'operazione
	 * @param wobNumbers                       Documento a cui applicare
	 *                                         l'operazione
	 * @param motivazione                      motivazione addotta dall'utente per
	 *                                         giustificare l'operaizone di
	 *                                         richiesta visto
	 * @param assegnazioneVistoList            lista di assegnazioni con cui
	 *                                         calcolare le security
	 * @param assegnazioneUfficioVistoList     lista di uffici a cui assegnare il
	 *                                         visto
	 * @param assegnazioneIspettoratoVistoList lista di ispettorati a cui assegnare
	 *                                         il visto
	 * @return Esito dell'operazione effettuata, qualora l'operazione si sia
	 *         completata prima che venisse recuperato il documentTitle, solo il
	 *         wobNumber è valorizzato.
	 */
	EsitoOperazioneDTO richiestaVisti2(UtenteDTO utente, String wobNumber, String motivazione, List<AssegnazioneDTO> assegnazioneVistoList, final boolean isDestDelegato);

	/**
	 * Avanza il workflow per richiedi_visto_ispettore.
	 * 
	 * @param utente      mittente
	 * @param wobNumbers  workflow selezionato
	 * @param motivazione motivazione addotta dall'utente
	 * @return Esito dell'operaizone effettuata. Qualora l'operazione si sia
	 *         completata prima che venisse recuperato il documentTitle, solo il
	 *         wobNumber è valorizzato.
	 */
	EsitoOperazioneDTO richiediVistoIspettore(UtenteDTO utente, String wobNumbers, String motivazione);

	/**
	 * La response richiedi visto ufficio assegna un documento in visto a uno degli
	 * uffici interni all'ispettorato.
	 * 
	 * Il segretario dell'ufficio dovrà smistare il visto a uno degli utenti
	 * dell'ufficio.
	 * 
	 * @param utente                  mittente dell'operazione
	 * @param wobNumber               identificativo del workflow per il quale viene
	 *                                richiesta l'operaizone
	 * @param motivazione             descrizione della motivazione dell'operazione
	 *                                eseguita
	 * @param assegnazioneUfficioList Oggetto DTO la cui informaizone principale da
	 *                                conoscere è l'idUffcio,
	 * @return Esito dell'operazione effettuata, qualora l'operazione si sia
	 *         completata prima che venisse recuperato il documentTitle, solo il
	 *         wobNumber è valorizzato.
	 */
	EsitoOperazioneDTO richiediVistoUfficio(UtenteDTO utente, String wobNumber, String motivazione, List<AssegnatarioOrganigrammaDTO> assegnazioneUfficioList);

	/**
	 * La response richiedi visto interno assegna un documento in visto a uno degli
	 * uffici interni all'ispettorato.
	 * 
	 * Attualmente non è chiaro come viene richiamata e se è presente in FILENET, si
	 * comporta esattamente come RICHIEDI_VISTO_UFFICIO L'implemetazione differisce
	 * perché viene passsata a filenet la response INTERNO.
	 * 
	 * @param utente                  mittente dell'operazione
	 * @param wobNumber               identificativo del workflow per il quale viene
	 *                                richiesta l'operaizone
	 * @param motivazione             descrizione della motivazione dell'operazione
	 *                                eseguita
	 * @param assegnazioneUfficioList Oggetto DTO la cui informaizone principale da
	 *                                conoscere è l'idUffcio,
	 * @return Esito dell'operazione effettuata, qualora l'operazione si sia
	 *         completata prima che venisse recuperato il documentTitle, solo il
	 *         wobNumber è valorizzato.
	 */
	EsitoOperazioneDTO richiediVistoInterno(UtenteDTO utente, String wobNumber, String motivoAssegnazione, List<AssegnatarioOrganigrammaDTO> assegnatari);

	/**
	 * Metodo per la richiesta di verifica massiva.
	 * 
	 * @param utente
	 * @param wobNumbers
	 * @param assegnazioneVerifica
	 * @param motivoAssegnazione
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> richiediVerifica(UtenteDTO utente, Collection<String> wobNumbers, AssegnatarioOrganigrammaDTO assegnazioneVerifica,
			String motivoAssegnazione);

	/**
	 * Metodo per la richiesta di verifica.
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param assegnazioneVerifica
	 * @param motivoAssegnazione
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO richiediVerifica(UtenteDTO utente, String wobNumber, AssegnatarioOrganigrammaDTO assegnazioneVerifica, String motivoAssegnazione);

	/**
	 * Metodo per la sigla e la richiesta visti massive.
	 * 
	 * @param utente
	 * @param wobNumbers
	 * @param assegnazioniVisto
	 * @param motivoAssegnazione
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> siglaERichiediVisti(UtenteDTO utente, Collection<String> wobNumbers, List<AssegnazioneDTO> assegnazioniVisto, String motivoAssegnazione);

	/**
	 * Metodo per la sigla e la richiesta visti.
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param assegnazioniVisto
	 * @param motivoAssegnazione
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO siglaERichiediVisti(UtenteDTO utente, String wobNumber, List<AssegnazioneDTO> assegnazioniVisto, String motivoAssegnazione);

	/**
	 * Recupera la lista delle assegnazioni per l'ispettorato corrispondente ai vari
	 * uffici assegnati per contributo al documento ignora invece eventuali
	 * assegnazioni per utente.
	 * 
	 * @param wobNumber id del documento nel pe.
	 * 
	 * @param utente    utente richiedente dell'operazione.
	 * 
	 * @return UNa lista di assegnazioni di ispettorati. Gli organigrammai non sono
	 *         dublicati.
	 */
	Set<NodoOrganigrammaDTO> getUfficioContributi(String wobNumber, String documentTitle, UtenteDTO utente);
}