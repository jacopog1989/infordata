package it.ibm.red.business.persistence.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * The Class RedWsClient.
 *
 * @author m.crescentini
 * 
 *         Entità che mappa la tabella REDWS_CLIENT.
 */
public class RedWsClient implements Serializable {
	
	private static final long serialVersionUID = 2832204543383499489L;

	/**
	 * Id Client.
	 */
	private String idClient;
	
	/**
	 * Password Client.
	 */
	private String pwdClient;
	
	/**
	 * Aoo.
	 */
	private Long idAoo;
	
	/**
	 * Nodo.
	 */
	private Long idNodo;
	
	/**
	 * Utente.
	 */
	private Long idUtente;
	
	/**
	 * Lista nodi.
	 */
	private List<Long> nodiCompetenti;
	
	/**
	 * Lista aoo.
	 */
	private Set<Long> aooCompetenti;
	
	/**
	 * Lista servizi.
	 */
	private List<String> serviziAbilitati;
	
	/**
	 * Flag autorizzato.
	 */
	private boolean authorized;
	
	/**
	 * Errore autorizzazione.
	 */
	private String authorizationErrorMessage;
	
	/**
	 * Costruttore di default.
	 */
	public RedWsClient() {
		super();
	}
	
	/**
	 * @param idClient
	 * @param pwdClient
	 */
	public RedWsClient(final String idClient, final String pwdClient) {
		this();
		this.idClient = idClient;
		this.pwdClient = pwdClient;
	}

	/**
	 * Costruttore.
	 * @param idClient
	 * @param pwdClient
	 * @param idAoo
	 * @param idNodo
	 * @param idUtente
	 */
	public RedWsClient(final String idClient, final String pwdClient, final Long idAoo, final Long idNodo, final Long idUtente) {
		this();
		this.idClient = idClient;
		this.pwdClient = pwdClient;
		this.idAoo = idAoo;
		this.idNodo = idNodo;
		this.idUtente = idUtente;
	}
	
	/**
	 * Restituisce l'id del Client.
	 * @return client ID
	 */
	public String getIdClient() {
		return idClient;
	}

	/**
	 * Restituisce la password del Client.
	 * @return client password
	 */
	public String getPwdClient() {
		return pwdClient;
	}

	/**
	 * Restituisce la lista dei nodi competenti.
	 * @return nodi competenti
	 */
	public List<Long> getNodiCompetenti() {
		return nodiCompetenti;
	}

	/**
	 * Imposta la lista dei nodi competenti.
	 * @param nodiCompetenti
	 */
	public void setNodiCompetenti(final List<Long> nodiCompetenti) {
		this.nodiCompetenti = nodiCompetenti;
	}
	
	/**
	 * Restituisce la lista delle AOO competenti.
	 * @return aoo competenti
	 */
	public Set<Long> getAooCompetenti() {
		return aooCompetenti;
	}

	/**
	 * Imposta la lista delle aoo competenti.
	 * @param aooCompetenti
	 */
	public void setAooCompetenti(final Set<Long> aooCompetenti) {
		this.aooCompetenti = aooCompetenti;
	}

	/**
	 * Restituisce la lista dei servizi abitativi.
	 * @return servizi abitativi
	 */
	public List<String> getServiziAbilitati() {
		return serviziAbilitati;
	}

	/**
	 * Imposta la lista dei servizi abitativi.
	 * @param serviziAbilitati
	 */
	public void setServiziAbilitati(final List<String> serviziAbilitati) {
		this.serviziAbilitati = serviziAbilitati;
	}
	
	/**
	 * Restituisce l'id dell'Area Organizzativa Omogenea.
	 * @return id AOO
	 */
	public Long getIdAoo() {
		return idAoo;
	}

	/**
	 * Imposta l'id dell'Area Organizzativa Omogenea.
	 * @param idAoo
	 */
	public void setIdAoo(final Long idAoo) {
		this.idAoo = idAoo;
	}
	
	/**
	 * Restituisce l'id del nodo.
	 * @return id nodo
	 */
	public Long getIdNodo() {
		return idNodo;
	}

	/**
	 * Imposta l'id del nodo.
	 * @param idNodo
	 */
	public void setIdNodo(final Long idNodo) {
		this.idNodo = idNodo;
	}

	/**
	 * Restituisce l'id dell'utente.
	 * @return id utente
	 */
	public Long getIdUtente() {
		return idUtente;
	}

	/**
	 * Imposta l'id dell'utente.
	 * @param idUtente
	 */
	public void setIdUtente(final Long idUtente) {
		this.idUtente = idUtente;
	}

	/**
	 * Restituisce true se autorizzato, false altrimenti.
	 * @return true se autorizzato, false altrimenti
	 */
	public boolean isAuthorized() {
		return authorized;
	}

	/**
	 * Imposta il flag: authorized.
	 * @param authorized
	 */
	public void setAuthorized(final boolean authorized) {
		this.authorized = authorized;
	}

	/**
	 * Restituisce il messaggio di errore sollevato in fase di authrozation.
	 * @return authorizationErrorMessage
	 */
	public String getAuthorizationErrorMessage() {
		return authorizationErrorMessage;
	}

	/**
	 * Imposta il messaggio di errore sollevato in fase di authrozation.
	 * @param authorizationErrorMessage
	 */
	public void setAuthorizationErrorMessage(final String authorizationErrorMessage) {
		this.authorizationErrorMessage = authorizationErrorMessage;
	}
	
}