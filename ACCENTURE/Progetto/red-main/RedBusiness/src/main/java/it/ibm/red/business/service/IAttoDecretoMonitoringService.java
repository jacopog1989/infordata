package it.ibm.red.business.service;

import it.ibm.red.business.monitoring.attodecreto.AttoDecretoMonitoring;

/**
 * Interface del service che si occupa del monitoring di un Atto Decreto.
 */
public interface IAttoDecretoMonitoringService {

	/**
	 * Inserisce l'operazione.
	 * @param beanInfo
	 * @return true se l'inserimento va a buon fine, false in caso contrario
	 */
	boolean insertOperation(AttoDecretoMonitoring beanInfo);
	
	/**
	 * Aggiorna l'operazione.
	 * @param beanInfo
	 * @return true se l'inserimento va a buon fine, false in caso contrario
	 */
	boolean updateOperation(AttoDecretoMonitoring beanInfo);
	
}
