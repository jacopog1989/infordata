package it.ibm.red.business.service.ws;

import java.sql.Connection;
import java.util.List;
import java.util.Set;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.Document;

import it.ibm.red.business.dto.DocumentoWsDTO;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.service.ws.facade.IDocumentoWsFacadeSRV;


/**
 * Servizio per la gestione di documenti tramite web service.
 * 
 * @author m.crescentini
 *
 */
public interface IDocumentoWsSRV extends IDocumentoWsFacadeSRV {

	/**
	 * @param documentiFilenet
	 * @param withContent
	 * @param idAoo
	 * @param con
	 * @return
	 */
	List<DocumentoWsDTO> transformToDocumentiWsPerRicercaSemplice(DocumentSet documentiFilenet, boolean withContent, Long idAoo, Connection con);

	/**
	 * @param docFilenet
	 * @param withContent
	 * @param withMetadati
	 * @param idAoo
	 * @param fceh
	 * @param con
	 * @return
	 */
	DocumentoWsDTO transformToDocumentoWs(Document docFilenet, boolean withContent, boolean withMetadati, Long idAoo, IFilenetCEHelper fceh, 
			Connection con);

	/**
	 * @param docFilenet
	 * @param withContent
	 * @param withMetadati
	 * @param metadatiDaRecuperare
	 * @param idAoo
	 * @param fceh
	 * @param con
	 * @return
	 */
	DocumentoWsDTO transformToDocumentoWs(Document docFilenet, boolean withContent, boolean withMetadati, Set<String> metadatiDaRecuperare, Long idAoo, 
			IFilenetCEHelper fceh, Connection con);
	
	/**
	 * @param documentiFilenet
	 * @param withContent
	 * @param withMetadati
	 * @param idAoo
	 * @param fceh
	 * @param con
	 * @return
	 */
	List<DocumentoWsDTO> transformToDocumentiWs(DocumentSet documentiFilenet, boolean withContent, boolean withMetadati, Long idAoo, IFilenetCEHelper fceh, 
			Connection con);

	/**
	 * @param emailFilenet
	 * @param withContent
	 * @param withMetadati
	 * @param inMetadatiDaRecuperare, 
	 * @param idAoo
	 * @param fceh
	 * @param con
	 * @return
	 */
	DocumentoWsDTO transformToDocumentoEmailWs(Document emailFilenet, boolean withContent, boolean withMetadati, Set<String> inMetadatiDaRecuperare, 
			Long idAoo, IFilenetCEHelper fceh, Connection con);

}