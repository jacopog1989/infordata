package it.ibm.red.business.persistence.model;

import java.sql.Date;

/**
 * Indica un certo periodo di tempo in cui le notifiche (Qualsiasi notifica) relativa al canale di trasmissione 
 * sono disabilitate per l'utente idUtente
 * 
 * Viene confidurata dall'utente per segnalare periodi di tempo in cui ad esempio l'utente si trova in ferie e non gradisce avere notifiche di alcun tipo.
 *
 */
public class DisabilitazioneNotificheUtente {
	
	/**
	 * Reference a Utente.
	 */
	private Long idUtente;
	
	/**
	 * Reference a CanaleTrasmissioneDTO.
	 */
	private Integer idCanaleTrasmissione;
	
	/**
	 * Data in cui inizia la disabilitazione.
	 */
	private Date dataDa;
	
	/**
	 * Data fino a cui è valida la disabilitazione.
	 */
	private Date dataA;

	/**
	 * Restituisce l'id dell'utente.
	 * @return id utente
	 */
	public Long getIdUtente() {
		return idUtente;
	}

	/**
	 * Imposta l'id dell'utente.
	 * @param idUtente
	 */
	public void setIdUtente(final Long idUtente) {
		this.idUtente = idUtente;
	}

	/**
	 * Restituisce l'id del canale di trasmissione.
	 * @return id canale di trasmissione
	 */
	public Integer getIdCanaleTrasmissione() {
		return idCanaleTrasmissione;
	}

	/**
	 * Imposta l'id del canale di trasmissione.
	 * @param idCanaleTrasmissione
	 */
	public void setIdCanaleTrasmissione(final Integer idCanaleTrasmissione) {
		this.idCanaleTrasmissione = idCanaleTrasmissione;
	}

	/**
	 * Restituisce la data in cui inizia la disabilitazione.
	 * @return data
	 */
	public Date getDataDa() {
		return dataDa;
	}

	/**
	 * Imposta la data in cui inizia la disabilitazione.
	 * @param dataDa
	 */
	public void setDataDa(final Date dataDa) {
		this.dataDa = dataDa;
	}

	/**
	 * Restituisce la data fino a cui è valida la disabilitazione.
	 * @return data
	 */
	public Date getDataA() {
		return dataA;
	}

	/**
	 * Imposta la data fino a cui è valida la disabilitazione.
	 * @param inDataA
	 */
	public void setDataA(final Date inDataA) {
		dataA = inDataA;
	}
}
