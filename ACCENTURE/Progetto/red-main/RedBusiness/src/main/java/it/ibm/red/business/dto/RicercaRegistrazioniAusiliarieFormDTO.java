/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.Collection;

/**
 * @author m.crescentini
 *
 */
public class RicercaRegistrazioniAusiliarieFormDTO extends AbstractDTO {

	private static final long serialVersionUID = -6888063763877829672L;

	/**
	 * Lista tipologie documento.
	 */
	private Collection<TipologiaDocumentoDTO> tipologieDocumento;
	
	/**
	 * Lista registri.
	 */
	private Collection<RegistroDTO> registri;
	

	/**
	 * @return the tipologieDocumento
	 */
	public Collection<TipologiaDocumentoDTO> getTipologieDocumento() {
		return tipologieDocumento;
	}

	/**
	 * @param tipologieDocumento the tipologieDocumento to set
	 */
	public void setTipologieDocumento(final Collection<TipologiaDocumentoDTO> tipologieDocumento) {
		this.tipologieDocumento = tipologieDocumento;
	}

	/**
	 * @return the registri
	 */
	public Collection<RegistroDTO> getRegistri() {
		return registri;
	}

	/**
	 * @param registri the registri to set
	 */
	public void setRegistri(final Collection<RegistroDTO> registri) {
		this.registri = registri;
	}
	
}
