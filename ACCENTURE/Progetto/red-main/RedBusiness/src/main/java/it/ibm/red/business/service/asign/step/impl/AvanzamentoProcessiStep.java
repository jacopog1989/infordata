package it.ibm.red.business.service.asign.step.impl;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignStepResultDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.SignModeEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.asign.step.ASignStepAbstract;
import it.ibm.red.business.service.asign.step.ContextASignEnum;
import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * Esegue lo step di avanzamento processi - firma asincrona.
 */
@Service
public class AvanzamentoProcessiStep extends ASignStepAbstract {

	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = -7553486970039206304L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AvanzamentoProcessiStep.class.getName());

	/**
	 * Esegue l'avanzamento dei processi per un item di firma asincrona.
	 * @param in
	 * @param item
	 * @param context
	 * @return risultato dell'avanzamento
	 */
	@Override
	protected ASignStepResultDTO run(Connection con, ASignItemDTO item, Map<ContextASignEnum, Object> context) {
		Long idItem = item.getId();
		LOGGER.info("run -> START [ID item: " + idItem + "]");
		ASignStepResultDTO out = new ASignStepResultDTO(item.getId(), getStep());
		FilenetPEHelper fpeh = null;
		
		try {
			// Si recupera l'utente firmatario
			UtenteDTO utenteFirmatario = getUtenteFirmatario(item, con);
			fpeh = new FilenetPEHelper(utenteFirmatario.getFcDTO());
			
			// Si recupera il workflow senza chiudere il PE helper, perché l'avanzamento del workflow necessita che la sessione verso il PE rimanga aperta
			VWWorkObject wob = getWorkflow(item, out, utenteFirmatario, fpeh);
			
			if (wob != null) {
				// ### SCRITTURA NEL DB DEI METADATI DEL WORKFLOW NECESSARI PER GLI STEP SUCCESSIVI
				PropertiesProvider pp = PropertiesProvider.getIstance();
				Map<String, String> dataToSave = new HashMap<>();
				dataToSave.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), 
						String.valueOf(TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY))));
				dataToSave.put(pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY), 
						String.valueOf(TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY))));
				String[] elencoLibroFirma = (String[]) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ELENCO_LIBROFIRMA_METAKEY));
				if (elencoLibroFirma != null && elencoLibroFirma.length > 0) {
					dataToSave.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_LIBROFIRMA_METAKEY), String.join("|", elencoLibroFirma));
				}
				Integer count = (Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.COUNT_METAKEY));
				if (count != null) {
					dataToSave.put(pp.getParameterByKey(PropertiesNameEnum.COUNT_METAKEY), String.valueOf(count));
				}
				aSignSRV.saveAndOverrideItemData(con, item.getId(), dataToSave);
				
				// ### AVANZAMENTO DEI PROCESSI ###
				EsitoOperazioneDTO esitoAvanzamentoProcessi = signSRV.avanzamentoProcessi(wob, utenteFirmatario, SignModeEnum.AUTOGRAFA.equals(item.getSignMode()), 
						context, fpeh, con);
				
				if (esitoAvanzamentoProcessi.isEsito()) {
					out.setStatus(true);
					out.appendMessage("[OK] Avanzamento dei processi eseguito");
				} else {
					throw new RedException("[KO] Errore nello step di avanzamento dei processi: " + esitoAvanzamentoProcessi.getNote() 
						+ ". [Codice errore: " + esitoAvanzamentoProcessi.getCodiceErrore() + "]");
				}
			}
		} finally {
			logoff(fpeh);
		}
		
		LOGGER.info("run -> END [ID item: " + idItem + "]");
		return out;
	}

	/**
	 * Esegue le azioni di recovery in caso di errore durante l'esecuzione di: {@link #run(Connection, ASignItemDTO, Map)}.
	 * @param in
	 * @param item
	 * @param context
	 * @return risultato del recovery
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected ASignStepResultDTO recovery(ASignStepResultDTO in, ASignItemDTO item, Map<ContextASignEnum, Object> context) {
		LOGGER.info("recovery -> START [ID item: " + item.getId() + "]");
		ASignStepResultDTO out = in;
		Connection con = null;
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			UtenteDTO utenteFirmatario = getUtenteFirmatario(item, con);
			fceh = FilenetCEHelperProxy.newInstance(utenteFirmatario.getFcDTO(), utenteFirmatario.getIdAoo());
			fpeh = new FilenetPEHelper(utenteFirmatario.getFcDTO());
			
			// Il recovery provvede ad eliminare i documento di categoria "INTERNO" ed i relativi workflow eventualmente creati per i destinatari interni.
			if (context != null) {
				// Eliminazione sul CE (documento)
				if (context.get(ContextASignEnum.DOC_INTERNO_GUID_LIST) != null) {
					List<String> guidToDeleteList = (List<String>) context.get(ContextASignEnum.DOC_INTERNO_GUID_LIST);
					
					for (String guidToDelete : guidToDeleteList) {
						fceh.eliminaDocumento(guidToDelete);
						LOGGER.info("recovery -> Eliminazione del documento interno sul CE eseguita. GUID: " + guidToDelete);
					}
				}
				
				// Eliminazione sul PE (workflow)
				if (context.get(ContextASignEnum.DOC_INTERNO_WOB_NUMBER_LIST) != null) {
					List<String> wobNumberToDeleteList = (List<String>) context.get(ContextASignEnum.DOC_INTERNO_WOB_NUMBER_LIST);
					
					for (String wobNumberToDelete : wobNumberToDeleteList) {
						VWWorkObject workflowDocInterno = fpeh.getWorkFlowByWob(wobNumberToDelete, false);
						
						if (workflowDocInterno != null) {
							fpeh.deleteWF(workflowDocInterno);
							LOGGER.info("recovery -> Eliminazione del workflow afferente al documento interno eseguita. WOB number: " + wobNumberToDelete);
						}
					}
				}
			}
			
			// Status a false per impostare il retry
			out.setStatus(false);
		} catch (Exception e) {
			LOGGER.error(e);
			out.appendMessage(getErrorMessage() + e.getMessage());
		} finally {
			closeConnection(con);
			popSubject(fceh);
			logoff(fpeh);
		}
		
		LOGGER.info("recovery -> END [ID item: " + item.getId() + "]");
		return out;
	}

	/**
	 * Restituisce lo @see StepEnum a cui fa riferimento questa classe.
	 * @return StepEnum
	 */
	@Override
	protected StepEnum getStep() {
		return StepEnum.AVANZAMENTO_PROCESSI;
	}

	/**
	 * @see it.ibm.red.business.service.asign.step.ASignStepAbstract#getErrorMessage().
	 */
	@Override
	protected String getErrorMessage() {
		return "[KO] Errore durante il recovery dello step di avanzamento dei processi: ";
	}


}
