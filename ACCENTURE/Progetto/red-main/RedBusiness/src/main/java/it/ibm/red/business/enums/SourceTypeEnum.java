package it.ibm.red.business.enums;

/**
 * Enum per la gestione delle response Applicative e Filenet
 * 
 * @author APerquoti
 */
public enum SourceTypeEnum {

	/**
	 * Valore.
	 */
	FILENET,

	/**
	 * Valore.
	 */
	APP;
}
