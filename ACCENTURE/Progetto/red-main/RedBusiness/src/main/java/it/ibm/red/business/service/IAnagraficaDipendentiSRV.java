package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IAnagraficaDipendentiFacadeSRV;

/**
 * Interface del service che gestisce le anagrafiche dipendenti.
 */
public interface IAnagraficaDipendentiSRV extends IAnagraficaDipendentiFacadeSRV {
}
