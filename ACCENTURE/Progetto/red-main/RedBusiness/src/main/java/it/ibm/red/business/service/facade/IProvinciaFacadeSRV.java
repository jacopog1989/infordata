package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.ProvinciaDTO;
import it.ibm.red.business.dto.SelectItemDTO;

/**
 * Facade del servizio di gestione province.
 */
public interface IProvinciaFacadeSRV extends Serializable {

	/**
	 * Restituisce l'elenco di tutte le province configurate sulla base dati.
	 * 
	 * @return province recuperate
	 */
	Collection<ProvinciaDTO> getAll();
	
	/**
	 * Ottiene le province associate ad una descrizione uguale a
	 * <code> query </code>.
	 * 
	 * @param query
	 *            descrizione delle province
	 * @return lista di province recuperate
	 */
	List<ProvinciaDTO> getProvincieFromDesc(String query);

	/**
	 * Ottiene la provincia.
	 * @param id
	 * @return provincia
	 */
	ProvinciaDTO get(String id);

	/**
	 * Ottiene le province dalla regione.
	 * @param idRegione
	 * @return lista di province
	 */
	List<ProvinciaDTO> getProvFromRegione(Long idRegione);

	/**
	 * Ottiene le povince dalla regione e dalla descrizione.
	 * @param query
	 * @return lista di province
	 */
	List<ProvinciaDTO> getProvFromRegioneAndDesc(String query);

	/**
	 * Ottiene le province.
	 * @return item selezionati
	 */
	Collection<SelectItemDTO> getProvince();
	
}
