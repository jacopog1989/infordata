package it.ibm.red.business.dao.impl;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IPropertiesDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.persistence.model.Property;

/**
 * The Class PropertiesDAO.
 *
 * @author CPIERASC
 * 
 * 	Dao gestione property.
 */
@Repository
public class PropertiesDAO extends AbstractDAO implements IPropertiesDAO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Gets the all values.
	 *
	 * @param connection the connection
	 * @return the all
	 */
	@Override
	public final Collection<Property> getAllValues(final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection<Property> properties = new ArrayList<>();
		try {
			String querySQL = "SELECT * FROM PROPERTIES WHERE VALUE IS NOT NULL AND BLOB_VALUE IS NULL";
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			while (rs.next()) {
				properties.add(new Property(rs.getString("KEY"), rs.getString("VALUE")));
			}
		} catch (SQLException e) {
			throw new RedException("Errore durante il recupero delle properties ", e);
		} finally {
			closeStatement(ps, rs);
		}
		return properties;
	}
	
	
	/**
	 * Gets the all bob values.
	 *
	 * @param connection the connection
	 * @return the all
	 */
	@Override
	public final Collection<Property> getAllBlobValues(final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection<Property> properties = new ArrayList<>();
		try {
			String querySQL = "SELECT * FROM PROPERTIES WHERE BLOB_VALUE IS NOT NULL AND VALUE IS NULL";
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			while (rs.next()) {
				Blob blobValue = rs.getBlob("BLOB_VALUE");
				if (blobValue != null) {
					byte[] propertyValue = blobValue.getBytes(1, (int) blobValue.length());
					properties.add(new Property(rs.getString("KEY"), new String(propertyValue)));
				}
			}
		} catch (SQLException e) {
			throw new RedException("Errore durante il recupero delle properties ", e);
		} finally {
			closeStatement(ps, rs);
		}
		return properties;
	}
	

	/**
	 * Gets the by enum.
	 *
	 * @param key the key
	 * @param connection the connection
	 * @return the by enum
	 */
	@Override
	public final String getByEnum(final String key, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String querySQL = "SELECT * FROM PROPERTIES WHERE KEY=" + sanitize(key);
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString("VALUE");
			}
		} catch (SQLException e) {
			throw new RedException("Errore durante il recupero della property=" + key, e);
		} finally {
			closeStatement(ps, rs);
		}
		return null;
	}

}
