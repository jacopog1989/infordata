package it.ibm.red.business.dto;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import it.ibm.red.business.dto.filenet.StoricoDTO;

/**
 * Questo DTO e' utilizzato per il front end del dettaglio di un fascicolo.
 * @author SLac
 *
 */
public class DetailFascicoloRedDTO extends AbstractDTO {
	
	private static final long serialVersionUID = 1474062088738005197L;

	/**
	 * Il campo si chiama nome ma in realta' e' l'id del fascicolo.
	 */
	private String nomeFascicolo;
	
	/**
	 * Stato.
	 */
	private String stato;
	
	/**
	 * Titolario.
	 */
	private String titolario;
	
	/**
	 * Info titolario.
	 */
	private TitolarioDTO titolarioDTO;
	
	/**
	 * Data creazione.
	 */
	private Date dataCreazione;
	
	/**
	 * L'oggetto è composto da NUMERO_ANNO_descrizione.
	 */
	private String oggetto;
	
	/**
	 * Oggetto.
	 */
	private String oggettoNoNumeroAnno;
	
	/**
	 * Anno.
	 */
	private String anno;
	
	/**
	 * Guid.
	 */
	private String guid;
	
	/**
	 * Id fascicolo FEPA.
	 */
	private String idFascicoloFEPA;
	
	/**
	 * Id aoo.
	 */
	private Integer idAOO;
	
	/**
	 * Flag faldonato.
	 */
	private Boolean faldonato;
	
	/**
	 * Data terminazione.
	 */
	private Date dataTerminazione;
	
	/**
	 * Document title.
	 */
	private String documentTitle;
	
	/**
	 * Classe documentale.
	 */
	private String classeDocumentale;
	
	/**
	 * Faldoni.
	 */
	private List<FaldoneDTO> faldoni;

	/**
	 * Documenti.
	 */
	private List<DocumentoFascicoloDTO> documenti;
	
	/**
	 * Storico.
	 */
	private Collection<StoricoDTO> storico;
	
	/**
	 * Lista assegnazioni.
	 */
	private List<AssegnazioneDTO> assegnazioni;
	
	/**
	 * Sicurezza.
	 */
	private ListSecurityDTO security;
	
	/**
	 * Id protocollo.
	 */
	private String idProtocollo;
	 
	/**
	 * Storico nsd.
	 */
	private List<AllaccioRiferimentoStoricoDTO> rifStoricoNsd;
	
	/**
	 * Riferimento allaccio NPS.
	 */
	private List<RiferimentoProtNpsDTO> rifAllaccioNps;
 
	
	/** GET & SET **********************************************************************/
 

	/**
	 * Restituisce il nome del fascicolo.
	 * @return nome fascicolo
	 */
	public String getNomeFascicolo() {
		return nomeFascicolo;
	}

	/**
	 * Imposta il nome del fascicolo.
	 * @param nomeFascicolo
	 */
	public void setNomeFascicolo(final String nomeFascicolo) {
		this.nomeFascicolo = nomeFascicolo;
	}

	/**
	 * Restituisce lo stato del fascicolo.
	 * @return stato fascicolo
	 */
	public String getStato() {
		return stato;
	}

	/**
	 * Imposta lo stato del fascicolo.
	 * @param stato
	 */
	public void setStato(final String stato) {
		this.stato = stato;
	}

	/**
	 * Restituisce il titolario del fascicolo.
	 * @return titolario
	 */
	public String getTitolario() {
		return titolario;
	}

	/**
	 * Imposta il titolario.
	 * @param titolario
	 */
	public void setTitolario(final String titolario) {
		this.titolario = titolario;
	}

	/**
	 * Imposta la data di creazione del fascicolo.
	 * @return data creazione fascicolo
	 */
	public Date getDataCreazione() {
		return dataCreazione;
	}

	/**
	 * Imposta la data di creazione.
	 * @param dataCreazione
	 */
	public void setDataCreazione(final Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	/**
	 * Restituisce l'oggetto.
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Imposta l'oggetto.
	 * @param oggetto
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * Restituisce il guid.
	 * @return guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * Imposta il guid.
	 * @param guid
	 */
	public void setGuid(final String guid) {
		this.guid = guid;
	}

	/**
	 * Restituisce l'id del fascicolo FEPA.
	 * @return id fascicolo FEPA
	 */
	public String getIdFascicoloFEPA() {
		return idFascicoloFEPA;
	}

	/**
	 * Imposta l'id del fascicolo FEPA.
	 * @param idFascicoloFEPA
	 */
	public void setIdFascicoloFEPA(final String idFascicoloFEPA) {
		this.idFascicoloFEPA = idFascicoloFEPA;
	}

	/**
	 * Restituisce la classe documentale.
	 * @return classeDocumentale
	 */
	public String getClasseDocumentale() {
		return classeDocumentale;
	}

	/**
	 * Imposta la classe documentale.
	 * @param classeDocumentale
	 */
	public void setClasseDocumentale(final String classeDocumentale) {
		this.classeDocumentale = classeDocumentale;
	}

	/**
	 * Restituisce la lista dei Faldoni.
	 * @return faldoni
	 */
	public List<FaldoneDTO> getFaldoni() {
		return faldoni;
	}

	/**
	 * Imposta la lista dei Faldoni.
	 * @param faldoni
	 */
	public void setFaldoni(final List<FaldoneDTO> faldoni) {
		this.faldoni = faldoni;
	}

	/**
	 * Restituisce la lista dei documenti del fascicolo.
	 * @return List di DocumentoFascicoloDTO
	 */
	public List<DocumentoFascicoloDTO> getDocumenti() {
		return documenti;
	}

	/**
	 * Imposta la lista del documenti associati al fascicolo.
	 * @param documenti
	 */
	public void setDocumenti(final List<DocumentoFascicoloDTO> documenti) {
		this.documenti = documenti;
	}

	/**
	 * Restituisce le security del fascicolo.
	 * 
	 * @return ListSecurityDTO
	 */
	public ListSecurityDTO getSecurity() {
		return security;
	}

	/**
	 * Imposta le Security del fascicolo.
	 * @param security
	 */
	public void setSecurity(final ListSecurityDTO security) {
		this.security = security;
	}

	/**
	 * Restituisce le assegnazioni.
	 * @return assegnazioni
	 */
	public List<AssegnazioneDTO> getAssegnazioni() {
		return assegnazioni;
	}

	/**
	 * Imposta le assegnazioni.
	 * @param assegnazioni
	 */
	public void setAssegnazioni(final List<AssegnazioneDTO> assegnazioni) {
		this.assegnazioni = assegnazioni;
	}

	/**
	 * Restituisce l'id dell'Area Organizzativa Omogenea a cui fa riferimento il fascicolo.
	 * @return id AOO
	 */
	public Integer getIdAOO() {
		return idAOO;
	}

	/**
	 * Imposta l'id dell'Area Organizzativa Omogenea a cui fa riferimento il fascicolo.
	 * @param idAOO
	 */
	public void setIdAOO(final Integer idAOO) {
		this.idAOO = idAOO;
	}

	/**
	 * Restituisce true se il fascicolo risulta faldonato, false altrimenti.
	 * @return true se faldonato, false altrimenti.
	 */
	public Boolean getFaldonato() {
		return faldonato;
	}

	/**
	 * Imposta il flag che fa riferimento alla faldonatura del fascicolo.
	 * @param faldonato
	 */
	public void setFaldonato(final Boolean faldonato) {
		this.faldonato = faldonato;
	}

	/**
	 * Restituisce la data di terminazione.
	 * @return data terminazione
	 */
	public Date getDataTerminazione() {
		return dataTerminazione;
	}

	/**
	 * Imposta la data di terminazione.
	 * @param dataTerminazione
	 */
	public void setDataTerminazione(final Date dataTerminazione) {
		this.dataTerminazione = dataTerminazione;
	}

	/**
	 * Restituisce il document title.
	 * @return documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta il document title.
	 * @param documentTitle
	 */
	public void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * Restitusice oggettoNoNumeroAnno.
	 * @return oggettoNoNumeroAnno
	 */
	public String getOggettoNoNumeroAnno() {
		return oggettoNoNumeroAnno;
	}

	/**
	 * @param oggettoNoNumeroAnno
	 */
	public void setOggettoNoNumeroAnno(final String oggettoNoNumeroAnno) {
		this.oggettoNoNumeroAnno = oggettoNoNumeroAnno;
	}

	/**
	 * Restituisce l'anno di riferimento del fascicolo.
	 * @return anno
	 */
	public String getAnno() {
		return anno;
	}

	/**
	 * Imposta l'anno di riferimento del fascicolo.
	 * @param anno
	 */
	public void setAnno(final String anno) {
		this.anno = anno;
	}

	/**
	 * Restituisce lo storico come lista di StoricoDTO.
	 *
	 * @return storico
	 */
	public Collection<StoricoDTO> getStorico() {
		return storico;
	}

	/**
	 * Imposta lo storico del fascicolo.
	 * @param storico
	 */
	public void setStorico(final Collection<StoricoDTO> storico) {
		this.storico = storico;
	}

	/**
	 * Restituisce le informazioni del titolario come TitolarioDTO.
	 *
	 * @return titolario
	 */
	public TitolarioDTO getTitolarioDTO() {
		return titolarioDTO;
	}

	/**
	 * Imposta il titolario con tutte le sue informazioni.
	 * @param titolarioDTO
	 */
	public void setTitolarioDTO(final TitolarioDTO titolarioDTO) {
		this.titolarioDTO = titolarioDTO;
	}

	/**
	 * Restituisce l'id del protocollo.
	 * @return id protocollo
	 */
	public String getIdProtocollo() {
		return idProtocollo;
	}

	/**
	 * Imposta l'id del protocollo.
	 * @param idProtocollo
	 */
	public void setIdProtocollo(final String idProtocollo) {
		this.idProtocollo = idProtocollo;
	}

	/**
	 * @return rifStoricoNsd
	 */
	public List<AllaccioRiferimentoStoricoDTO> getRifStoricoNsd() {
		return rifStoricoNsd;
	}

	/**
	 * @param rifStoricoNsd
	 */
	public void setRifStoricoNsd(final List<AllaccioRiferimentoStoricoDTO> rifStoricoNsd) {
		this.rifStoricoNsd = rifStoricoNsd;
	}
	
	/**
	 * @return rifAllaccioNps
	 */
	public List<RiferimentoProtNpsDTO> getRifAllaccioNps() {
		return rifAllaccioNps;
	}

	/**
	 * @param rifAllaccioNps
	 */
	public void setRifAllaccioNps(List<RiferimentoProtNpsDTO> rifAllaccioNps) {
		this.rifAllaccioNps = rifAllaccioNps;
	}
}
