package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.MezzoRicezioneDTO;
import it.ibm.red.business.enums.FormatoDocumentoEnum;

/**
 * Interfaccia DAO mezzo ricezione.
 */
public interface IMezzoRicezioneDAO extends Serializable {

	/**
	 * Ottiene tutti i mezzi di ricezione.
	 * @param connection
	 * @return lista dei mezzi di ricezione
	 */
	List<MezzoRicezioneDTO> getAllMezziRicezione(Connection connection);
	
	/**
	 * Ottiene il mezzo di ricezione tramite l'id del mezzo.
	 * @param idMezzoRicezione
	 * @param connection
	 * @return mezzo di ricezione
	 */
	MezzoRicezioneDTO getMezzoRicezioneByIdMezzo(Integer idMezzoRicezione, Connection connection);
	
	/**
	 * Ottiene i mezzi di ricezione tramite l'id del formato.
	 * @param formatoDocumento
	 * @param connection
	 * @return lista dei mezzi di ricezione
	 */
	List<MezzoRicezioneDTO> getMezziRicezioneByIdFormato(FormatoDocumentoEnum formatoDocumento, Connection connection);
	
}
