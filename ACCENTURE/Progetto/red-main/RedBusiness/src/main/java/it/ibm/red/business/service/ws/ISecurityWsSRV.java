package it.ibm.red.business.service.ws;

import java.sql.Connection;

import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.business.service.ws.facade.ISecurityWsFacadeSRV;
import it.ibm.red.webservice.model.documentservice.dto.Servizio;

/**
 * The Interface ISecurityWsSRV.
 *
 * @author a.dilegge
 * 
 *         Interfaccia servizio gestione security web service.
 */
public interface ISecurityWsSRV extends ISecurityWsFacadeSRV {

	/**
	 * Metodo che permette di autorizzare il client.
	 * @param client
	 * @param servizio
	 * @param conn
	 */
	void autorizzaClient(RedWsClient client, Servizio servizio, Connection conn);
	
}