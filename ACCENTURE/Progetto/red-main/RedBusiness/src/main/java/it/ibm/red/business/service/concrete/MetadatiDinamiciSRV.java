package it.ibm.red.business.service.concrete;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.admin.Choice;
import com.filenet.api.admin.PropertyDefinition;
import com.filenet.api.collection.Integer32List;
import com.filenet.api.collection.StringList;
import com.filenet.api.constants.Cardinality;
import com.filenet.api.constants.TypeID;
import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.KeyValueDTO;
import it.ibm.red.business.dto.MetadatoDinamicoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.MetadatoDinamicoDataTypeEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IMetadatiDinamiciSRV;
import it.ibm.red.business.utils.DateUtils;

/**
 * Service metadati dinamici.
 */
@Service
@Component
public class MetadatiDinamiciSRV extends AbstractService implements IMetadatiDinamiciSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 8830931488980977989L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(MetadatiDinamiciSRV.class);

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * Restituisce i metadati dinamici associati alla classe documentale.
	 * {@link #recuperaMetadatiDinamici(UtenteDTO, String)}.
	 * 
	 * @param utente
	 * @param classeDocumentale
	 * @return lista dei metadati dinamici
	 */
	@Override
	public List<MetadatoDinamicoDTO> getMetadatyByClasseDocumentale(final UtenteDTO utente, final String classeDocumentale) {
		return recuperaMetadatiDinamici(utente, classeDocumentale);
	}

	/**
	 * Restituisce i metadati dinamici associati alla classe documentale.
	 * 
	 * @param utente
	 * @param className
	 * @return lista dei metadati dinamici
	 */
	public List<MetadatoDinamicoDTO> recuperaMetadatiDinamici(final UtenteDTO utente, final String className) {
		List<MetadatoDinamicoDTO> items = null;
		IFilenetCEHelper fceh = null;

		try {
			final String classNameDocumento = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY);

			final boolean withSystemProperties = false;

			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final List<PropertyDefinition> listDocumento = fceh.getClassProperties(classNameDocumento, withSystemProperties);

			final Map<String, PropertyDefinition> mapDocumento = listDocumento.stream().collect(Collectors.toMap(PropertyDefinition::get_SymbolicName, p -> p));

			final List<PropertyDefinition> listInput = fceh.getClassProperties(className, withSystemProperties);
			items = new ArrayList<>();
			MetadatoDinamicoDTO meta = null;
			for (final PropertyDefinition pd : listInput) {
				if (mapDocumento.containsKey(pd.get_SymbolicName()) || Boolean.TRUE.equals(pd.get_IsHidden())) {
					continue;
				}

				meta = new MetadatoDinamicoDTO(pd.get_SymbolicName(), null, pd.get_DisplayName());

				meta.setCardinality(pd.get_Cardinality().getValue()); // Cardinality.LIST_AS_INT oppure Cardinality.SINGLE_AS_INT

				if (pd.get_ChoiceList() != null) {
//					SELECT
					meta.setDataTypeEnum(MetadatoDinamicoDataTypeEnum.SELECT);
					meta.setOptionsList(new ArrayList<>());
					for (int i = 0; i < pd.get_ChoiceList().get_ChoiceValues().size(); i++) {
						final Choice c = (Choice) pd.get_ChoiceList().get_ChoiceValues().get(i);

						if (pd.get_DataType().getValue() == TypeID.LONG_AS_INT) {
							meta.getOptionsList().add(new KeyValueDTO(c.get_Name(), c.get_ChoiceIntegerValue()));
							meta.setDataTypeValue(TypeID.LONG_AS_INT);
						} else {
							meta.getOptionsList().add(new KeyValueDTO(c.get_Name(), c.get_ChoiceStringValue()));
							meta.setDataTypeValue(TypeID.STRING_AS_INT);
						}

					}
				} else {
					switch (pd.get_DataType().getValue()) {
					case TypeID.BOOLEAN_AS_INT:
						// CHECKBOX
						meta.setDataTypeEnum(MetadatoDinamicoDataTypeEnum.CHECKBOX);
						break;

					case TypeID.DATE_AS_INT:
						// DATA
						updateMetaInfo(meta, pd);
						break;

					case TypeID.LONG_AS_INT:
						// INPUT TEXT
						meta.setDataTypeEnum(MetadatoDinamicoDataTypeEnum.INPUT_TEXT);
						meta.setMaximumLength(Constants.FilenetConstants.MAXLENGTH_INT_FILENET);
						meta.setDataTypeValue(TypeID.LONG_AS_INT);
						break;

					case TypeID.STRING_AS_INT:
						// INPUT TEXT
						meta.setDataTypeEnum(MetadatoDinamicoDataTypeEnum.INPUT_TEXT);
						meta.setMaximumLength(Constants.FilenetConstants.MAXLENGTH_STRING_FILENET);
						meta.setDataTypeValue(TypeID.STRING_AS_INT);
						break;

					default:
//							INPUT TEXT
						meta.setDataTypeEnum(MetadatoDinamicoDataTypeEnum.INPUT_TEXT);
						break;
					}
				}

				items.add(meta);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}

		return items;
	}

	/**
	 * Aggiorna il metadato {@code meta} con le informazioni associate alla
	 * tipologia Date.
	 * 
	 * @param meta
	 *            Metadato da aggiornare.
	 * @param pd
	 *            Property definition.
	 */
	private void updateMetaInfo(MetadatoDinamicoDTO meta, final PropertyDefinition pd) {
		meta.setDataTypeEnum(MetadatoDinamicoDataTypeEnum.DATE);
		meta.setMaxDate(DateUtils.getLastDate());
		if (pd.get_SymbolicName().equals(pp.getParameterByKey(PropertiesNameEnum.DATA_DOCUMENTO_DYNAMIC_METAKEY))) {
			meta.setMaxDate(new Date());
		}
	}
	
	/**
	 * @see it.ibm.red.business.service.IMetadatiDinamiciSRV#settaValoriMetadatiDinamici(com.filenet.api.core.Document,
	 *      java.util.Collection).
	 */
	@Override
	public Collection<MetadatoDinamicoDTO> settaValoriMetadatiDinamici(final Document docFilenet, final Collection<MetadatoDinamicoDTO> list) {
		final Collection<MetadatoDinamicoDTO> output = new ArrayList<>();

		try {
			Object valoreMeta = null;
			for (final MetadatoDinamicoDTO m : list) {
				valoreMeta = TrasformerCE.getMetadato(docFilenet, m.getChiave());

//				se e' una lista devo costruire una stringa del tipo:val1,val2,val3

				if (m.getCardinality() == Cardinality.LIST_AS_INT) {
					final List<Object> items = new ArrayList<>();
					if (m.getDataTypeValue() == TypeID.STRING_AS_INT) {
						final StringList sl = (StringList) valoreMeta;
						for (final Object object : sl) {
							items.add(object);
						}
					} else {
						final Integer32List il = (Integer32List) valoreMeta;
						for (final Object object : il) {
							items.add(object);
						}
					}
					if (!items.isEmpty()) {
						final StringBuilder valoreStr = new StringBuilder("{");
						for (int i = 0; i < items.size(); i++) {
							final Object object = items.get(i);
							valoreStr.append(String.valueOf(object));
							if (i <= items.size() - 1) {
								valoreStr.append(",");
							}
						}
						valoreStr.append("}");
						m.setValore(valoreStr.toString());
					}

				} else { // altrimenti setto il valore così com'e'
					// Gestione dei valori predefiniti negativi (-1) per i metadati dinamici Numero
					// RDP e Numero Legislatura
					if ((valoreMeta instanceof Integer && ((Integer) valoreMeta) == -1)
							&& (PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NUMERO_LEGISLATURA_METAKEY).equals(m.getChiave())
									|| PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NUMERO_RDP_METAKEY).equals(m.getChiave()))) {
						valoreMeta = null;
					}
					m.setValore(valoreMeta);
				}

				output.add(m);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}

		return output;
	}

}
