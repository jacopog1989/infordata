package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.DocumentQueueQueryObject;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.persistence.model.Nodo;

/**
 * Facade del servizio che gestisce la lista documenti.
 */
public interface IListaDocumentiFacadeSRV extends Serializable {

	/**
	 * Metodo utilizzato per il recupero dei DocumentMaster come risultato di una
	 * ricerca.
	 * 
	 * @param queue
	 * @param utente
	 * @return Collection di DTO utilizzati per valorizzare il DTH
	 */
	Collection<MasterDocumentRedDTO> getDocumentForMaster(DocumentQueueEnum queue, Collection<String> documentTitlesToFilterBy, UtenteDTO utente);

	/**
	 * Restituisce la VWQueueQuery associata alla queue.
	 * 
	 * @param queue
	 * @param documentTitlesToFilterBy
	 * @param utente
	 * @return VWQueueQuery
	 */
	Object getFepaQueryObject(DocumentQueueEnum queue, Collection<String> documentTitlesToFilterBy, UtenteDTO utente);

	/**
	 * Restituisce la VWQueueQuery associata alla queue tramite document title.
	 * 
	 * @param queue
	 * @param utente
	 * @param documentTitle
	 * @return VWQueueQuery
	 */
	Object getFepaQueryObjectForDocumentTitle(DocumentQueueEnum queue, UtenteDTO utente, String documentTitle);

	/**
	 * Ottiene le code Filenet tramite document title.
	 * 
	 * @param utente
	 * @param documentTitle
	 * @param classeDocumentale
	 * @param isDocEntrata
	 * @return code Filenet
	 */
	Collection<DocumentQueueQueryObject> getQueueFNbyDocumentTitleFull(UtenteDTO utente, String documentTitle, String classeDocumentale, Boolean isDocEntrata);

	/**
	 * Verifica che il seguente documento si trova nella cosa.
	 * 
	 * @param utente
	 * @param documentTitle
	 * @param queue         La coda in cui verificare se è presente il documento
	 * @return
	 */
	boolean isDocumentoInCoda(UtenteDTO utente, String documentTitle, DocumentQueueEnum queue);

	/**
	 * Ottiene i documenti master partendo dall'Organigramma Scrivania.
	 * 
	 * @param queue
	 * @param nodoPadre
	 * @param fcDto
	 * @return documenti master
	 */
	Collection<MasterDocumentRedDTO> getMasterFromScrivania(DocumentQueueEnum queue, NodoOrganigrammaDTO nodoPadre, FilenetCredentialsDTO fcDto);

	/**
	 * Verifica se il documento identificato dal Document Title in input si trova
	 * nel libro firma dell'utente (coppia ID Ufficio/ID Utente) in input.
	 * 
	 * @param documentTitle
	 * @param idUfficioDestinatario
	 * @param idUtenteDestinatario
	 * @param utenteRichiedente
	 * @return
	 */
	boolean isDocumentoInLibroFirmaUtente(String documentTitle, Long idUfficioDestinatario, Long idUtenteDestinatario, UtenteDTO utenteRichiedente);

	/**
	 * Restitusice la lista di Document Title dei documenti la cui spedizione è in
	 * stato di errore.
	 * 
	 * @param utente
	 * @return
	 */
	List<String> getDocInErroreCodaSpedizione(UtenteDTO utente);

	// START VI
	/**
	 * Restitusice la lista di Document Title dei documenti la cui spedizione è in
	 * stato di errore,attesariconciliazione,spediti.
	 * 
	 * @param utente
	 * @return
	 */
	List<String> getInfoSpedizioneWidget(UtenteDTO utente);

	/**
	 * Controlla se il documento con lo stato in ingresso è presente nella tabella
	 * documentoutentestato utile per la response recall
	 * 
	 * @param documentTitle,utente,idStatoLavorazione
	 * @return
	 */
	boolean checkDocumentTitleRecall(String documentTitle, UtenteDTO utente, Long idStatoLavorazione);

	/**
	 * Restitusice il workFlowPrincipale dal documentTitle
	 * 
	 * @param documentTitle,utente
	 * @return
	 */
	String getWorkflowPrincipale(String documentTitle, UtenteDTO utente);

	/**
	 * Ottiene la VWQueueQuery associata alla queue.
	 * 
	 * @param queue
	 * @param documentTitlesToFilterBy
	 * @param utente
	 * @param idNodo
	 * @param fpeh
	 * @return VWQueueQuery
	 */
	Object getFepaQueryObject(DocumentQueueEnum queue, Collection<String> documentTitlesToFilterBy, UtenteDTO utente, Nodo idNodo, FilenetPEHelper fpeh);

	/**
	 * Ottiene l'ufficio padre per report.
	 * 
	 * @param idNodo
	 * @return nodo
	 */
	Nodo recuperaUfficioPadrePerReport(Long idNodo);

	/**
	 * Ottiene il nodo destinatario per report di non competenza.
	 * 
	 * @param documentTitle
	 * @param idNodo
	 * @param utente
	 * @return id nodo destinatario
	 */
	Integer recuperaUfficioPadrePerReportNonCompetenza(String documentTitle, Long idNodo, UtenteDTO utente);

	/**
	 * Controlla se il documento è stato già spedito.
	 * 
	 * @param documentTitle
	 * @return true o false
	 */
	boolean giaSpedito(String documentTitle);

	/**
	 * Ottiene l'id dell'utente firmatario.
	 * 
	 * @param documentTitle
	 * @param utente
	 * @return id dell'utente firmatario
	 */
	Long getMetadatoIdUtenteFirmatario(String documentTitle, UtenteDTO utente);

}