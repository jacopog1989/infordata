package it.ibm.red.business.dto;

import java.util.Date;

/**
 * DTO documento fascicolo fepa.
 */
public class DocumentoFascicoloFepaDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 7622543821077780992L;
	
	/**
	 * Identificativo fascicolo.
	 */
	private String idFascicolo;
	
	/**
	 * Descrizione.
	 */
	private String descrizione;
	
	/**
	 * Data creazione.
	 */
	private Date dataCreazione;
	
	/**
	 * Nome del file.
	 */
	private String fileName;
	
	/**
	 * Identificativo.
	 */
	private String guid;
	
	/**
	 * Mime type.
	 */
	private String mimeType;
	
	/**
	 * Numero protocollo.
	 */
	private String numeroProtocollo;
	
	/**
	 * Aoo.
	 */
	private String aooProtocollo;
	
	/**
	 * Data protocollo.
	 */
	private Date dataProtocollo;
	
	/**
	 * Tipo protocollo.
	 */
	private String tipoProtocollo;
	
	/**
	 * Flag firmatario.
	 */
	private boolean firmato;
	
	/**
	 * Content.
	 */
	private byte[] byteContent;
	
	/**
	 * Identificativo categoria documento.
	 */
	private Integer idCategoriaDocumento;
	
	/**
	 * Tipologia documento.
	 */
	private String tipologiaDocumento;
	
	/**
	 * Tipologia fascicolo.
	 */
	private String tipoFascicolo;
	
	/**
	 * Identificativo fascicolo riferimento.
	 */
	private String idFascicoloRiferimento;

	/**
	 * Costruttore vuoto.
	 */
	public DocumentoFascicoloFepaDTO() {
		// Metodo intenzionalmente vuoto.
	}

	/**
	 * Restituisce l'id del fascicolo.
	 * @return id fascicolo
	 */
	public String getIdFascicolo() {
		return idFascicolo;
	}

	/**
	 * Imposta l'id del fascicolo.
	 * @param idFascicolo
	 */
	public void setIdFascicolo(final String idFascicolo) {
		this.idFascicolo = idFascicolo;
	}

	/**
	 * Restituisce la descrizione.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Imposta la descrizione.
	 * @param descrizione
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * Restituisce la data di creazione del fascicolo.
	 * @return data creazione fascicolo
	 */
	public Date getDataCreazione() {
		return dataCreazione;
	}

	/**
	 * Imposta la data di creazione del fascicolo.
	 * @param dataCreazione
	 */
	public void setDataCreazione(final Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	/**
	 * Restituisce il nome del file.
	 * @return nome file
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Imposta il nome del file.
	 * @param fileName
	 */
	public void setFileName(final String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Restituisce il guid.
	 * @return  il guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * Imposta il guid.
	 * @param guid
	 */
	public void setGuid(final String guid) {
		this.guid = guid;
	}

	/**
	 * Restituisce il mime type.
	 * @return mime type
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * Imposta il mime type.
	 * @param mimeType
	 */
	public void setMimeType(final String mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 * Restituisce il numero protocollo.
	 * @return numero protocollo
	 */
	public String getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Imposta il numero protocollo.
	 * @param numeroProtocollo
	 */
	public void setNumeroProtocollo(final String numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	/**
	 * Restituisce l'aoo del protocollo.
	 * @return aoo protocollo
	 */
	public String getAooProtocollo() {
		return aooProtocollo;
	}

	/**
	 * Imposta l'aoo del protocollo.
	 * @param aooProtocollo
	 */
	public void setAooProtocollo(final String aooProtocollo) {
		this.aooProtocollo = aooProtocollo;
	}

	/**
	 * Restituisce la data di protocollo.
	 * @return data protocollo
	 */
	public Date getDataProtocollo() {
		return dataProtocollo;
	}

	/**
	 * Imposta la data di protocollo.
	 * @param dataProtocollo
	 */
	public void setDataProtocollo(final Date dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}

	/**
	 * Restituisce il tipo di protocollo.
	 * @return tipo protocollo
	 */
	public String getTipoProtocollo() {
		return tipoProtocollo;
	}

	/**
	 * Imposta il tipo protocollo.
	 * @param tipoProtocollo
	 */
	public void setTipoProtocollo(final String tipoProtocollo) {
		this.tipoProtocollo = tipoProtocollo;
	}

	/**
	 * Restituisce true se firmato, false altrimenti.
	 * @return true se firmato, false altrimenti
	 */
	public boolean isFirmato() {
		return firmato;
	}

	/**
	 * Imposta il flag: firmato.
	 * @param firmato
	 */
	public void setFirmato(final boolean firmato) {
		this.firmato = firmato;
	}

	/**
	 * Restituisce il byte array del content.
	 * @return content
	 */
	public byte[] getByteContent() {
		return byteContent;
	}

	/**
	 * Imposta il content come byte array.
	 * @param byteContent
	 */
	public void setByteContent(final byte[] byteContent) {
		this.byteContent = byteContent;
	}

	/**
	 * Restituisce l'id della categoria documento.
	 * @return id categoria documento
	 */
	public Integer getIdCategoriaDocumento() {
		return idCategoriaDocumento;
	}

	/**
	 * Imposta l'id della categoria del documento.
	 * @param idCategoriaDocumento
	 */
	public void setIdCategoriaDocumento(final Integer idCategoriaDocumento) {
		this.idCategoriaDocumento = idCategoriaDocumento;
	}

	/**
	 * Restituisce la tipologia documento.
	 * @return tipologia documento
	 */
	public String getTipologiaDocumento() {
		return tipologiaDocumento;
	}

	/**
	 * Imposta la tipologia documento.
	 * @param tipologiaDocumento
	 */
	public void setTipologiaDocumento(final String tipologiaDocumento) {
		this.tipologiaDocumento = tipologiaDocumento;
	}

	/**
	 * Restituisce il tipo fascicolo.
	 * @return tipo fascicolo
	 */
	public String getTipoFascicolo() {
		return tipoFascicolo;
	}

	/**
	 * Imposta il tipo fascicolo.
	 * @param tipoFascicolo
	 */
	public void setTipoFascicolo(final String tipoFascicolo) {
		this.tipoFascicolo = tipoFascicolo;
	}

	/**
	 * Restituisce l'id del fascicolo di riferimento.
	 * @return id fascicolo riferimento
	 */
	public String getIdFascicoloRiferimento() {
		return idFascicoloRiferimento;
	}

	/**
	 * Imposta l'id del fascicolo di riferimento.
	 * @param idFascicoloRiferimento
	 */
	public void setIdFascicoloRiferimento(final String idFascicoloRiferimento) {
		this.idFascicoloRiferimento = idFascicoloRiferimento;
	}
	
}
