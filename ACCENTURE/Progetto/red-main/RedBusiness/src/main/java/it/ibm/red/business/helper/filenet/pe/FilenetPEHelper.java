package it.ibm.red.business.helper.filenet.pe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections4.CollectionUtils;

import com.filenet.api.core.Document;
import com.filenet.api.core.ObjectStore;

import filenet.vw.api.VWAttachment;
import filenet.vw.api.VWAttachmentType;
import filenet.vw.api.VWException;
import filenet.vw.api.VWFetchType;
import filenet.vw.api.VWFieldType;
import filenet.vw.api.VWLibraryType;
import filenet.vw.api.VWMapDefinition;
import filenet.vw.api.VWMapNode;
import filenet.vw.api.VWModeType;
import filenet.vw.api.VWParameter;
import filenet.vw.api.VWProcess;
import filenet.vw.api.VWQueue;
import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWRoster;
import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWRouteDefinition;
import filenet.vw.api.VWSession;
import filenet.vw.api.VWStepElement;
import filenet.vw.api.VWWorkObject;
import filenet.vw.api.VWWorkObjectNumber;
import filenet.vw.api.VWWorkflowDefinition;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.PEProperty;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.PEDocumentoDTO;
import it.ibm.red.business.dto.RouteWsDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.WobNumberWsDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.common.AbstractFilenetHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEQueryBuilder.Proposition;
import it.ibm.red.business.helper.filenet.pe.trasform.TrasformPE;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Classe per la gestione del PE di Filenet.
 *
 * @author CPIERASC
 */
public final class FilenetPEHelper extends AbstractFilenetHelper {

	private static final String ID_DOCUMENTO = "idDocumento";

	private static final String AND = " AND ";

	/**
	 * LITERAL.
	 */
	private static final String NELLA_CODA_LABEL = " nella coda -> ";

	/**
	 * Messaggio errore recupero parametri dal workflow.
	 */
	private static final String ERROR_RECUPERO_PARAMETRI_WORKFLOW_MSG = "Errore in getSettableParameters per il workflow ";

	/**
	 * Messaggio errore chiusura workflow.
	 */
	private static final String ERROR_CHIUSURA_WORKFLOW_MSG = "Errore in fase di chiusura del workflow ";

	/**
	 * Messaggio errore ricerca workflow.
	 */
	private static final String ERRORE_DURANTE_LA_RICERCA_DEI_WORKFLOW_PER_ID_DOCUMENTO = "Errore durante la ricerca dei workflow per id documento: ";

	/**
	 * Messaggio errore ricerca assegnazioni.
	 */
	private static final String ERRORE_DURANTE_LA_RICERCA_DELLE_ASSEGNAZIONI = "Errore durante la ricerca delle assegnazioni per il documento con id: ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FilenetPEHelper.class.getName());

	/**
	 * Sessione PE.
	 */
	private final VWSession session;

	/**
	 * Property provider.
	 */
	private final PropertiesProvider pp;

	/**
	 * Costruttore.
	 * 
	 * @param dto credenziali
	 */
	public FilenetPEHelper(final FilenetCredentialsDTO dto) {
		this(dto.getUserName(), dto.getPassword(), dto.getUri(), dto.getStanzaJAAS(), dto.getConnectionPoint());
	}

	/**
	 * Costruttore.
	 * 
	 * @param userName        username
	 * @param password        password
	 * @param urlPE           url PE
	 * @param stanzaJAAS      stanza JASS
	 * @param connectionPoint connection point PE
	 */
	public FilenetPEHelper(final String userName, final String password, final String urlPE, final String stanzaJAAS, final String connectionPoint) {
		initUserContext(userName, password, urlPE, stanzaJAAS);
		session = connectToPE(userName, password, urlPE, connectionPoint);
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * Metodo per la chiusura della sessione
	 */
	public void logoff() {
		session.logoff();
		closeUserContext();
	}

	/**
	 * Cancellazione workflow.
	 * 
	 * @param inWo workflow
	 */
	public void deleteWF(final VWWorkObject inWo) {
		try {
			inWo.doDelete(true, false);
		} catch (final VWException e) {
			LOGGER.error("Errore in fase di eliminazione workflow", e);
		}
	}

	/**
	 * Esegue un aggiornamento del workflow.
	 * 
	 * @param idDocumento
	 * @param metadati
	 * @return true se l'update è andato a buon fine, false altrimenti
	 */
	public Boolean updateWorkflow(final Integer idDocumento, final Map<String, Object> metadati) {
		try {
			final List<Long> documentiId = new ArrayList<>();
			documentiId.add(Long.valueOf(idDocumento));
			final VWRosterQuery rQueryNSD = getDocumentWorkFlowsByIdDocumenti(documentiId);
			Boolean output = true;
			while (rQueryNSD.hasNext()) {
				final VWWorkObject wf = (VWWorkObject) rQueryNSD.next();
				LOGGER.info("Viene aggiornato il workflow : " + wf.getWorkflowNumber() + " - " + wf.getSubject());
				output = output && updateWorkflow(wf, metadati);
			}
			return output;
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di aggiornamento del workflow relativo all'idDocumento:" + idDocumento, e);
			throw new RedException(e);
		}
	}

	/**
	 * Aggiornamento metadati workflow.
	 * 
	 * @param wo       workflow
	 * @param metadati metadati da aggiornare
	 * @return true se l'aggiornamento va a buon fine
	 */
	public Boolean updateWorkflow(final VWWorkObject wo, final Map<String, Object> metadati) {
		String wob = "";
		final Boolean output = true;
		try {
			wob = wo.getWorkflowNumber();
			if (metadati != null && !metadati.keySet().isEmpty()) {
				wo.doLock(false);
				if (!metadati.isEmpty()) {
					for (final Entry<String, Object> entry : metadati.entrySet()) {
						Object value = entry.getValue();
						if (value instanceof Long) {
							value = ((Long) value).intValue();
						}
						wo.setFieldValue(entry.getKey(), value, false);
					}
				}
				wo.doSave(true);
			}
		} catch (final Exception e) {
			LOGGER.error("updateWorkflow: " + e.getMessage(), e);
			try {
				wo.doAbort();
			} catch (final VWException e1) {
				LOGGER.error("updateWorkflow trying to abort: " + e1.getMessage(), e1);
			}
			throw new FilenetException("Si è verificato un errore durante l'aggiornamento dell'oggetto [" + wob + "] ");
		}
		return output;
	}

	/**
	 * Recupera workflow da MOBILE.
	 * 
	 * @param wobNumber           wob number
	 * @param nomeQueue           nome coda
	 * @param indexName           indexName
	 * @param idNodoDestinatario  identificativo ufficio destinatario
	 * @param idUtenteDestinatari Lista degli identificativi utente destinatario
	 * @param idClientAoo         identificativo client aoo
	 * @param idTipoAssegnazione  identificativo del tipo assegnazione
	 * @param flagRenderizzato    flag renderizzato
	 * @param registroRiservato   registro riservato
	 * @return l'insieme di workflow risultanti
	 */
	public VWQueueQuery getWorkFlowsByWobQueueFilter(final String wobNumber, final DocumentQueueEnum queue, final String indexName, final Long idNodoDestinatario,
			final List<Long> idUtenteDestinatari, final String idClientAoo, final Long idTipoAssegnazione, final Integer flagRenderizzato, final Boolean registroRiservato) {

		final ArrayList<Long> idsTipoAssegnazione = new ArrayList<>();

		if (idTipoAssegnazione != null) {
			idsTipoAssegnazione.add(idTipoAssegnazione);
		}

		return getWorkFlowsByWobQueueFilterRed(wobNumber, queue, indexName, idNodoDestinatario, idUtenteDestinatari, idClientAoo, idsTipoAssegnazione, flagRenderizzato,
				registroRiservato, null, null);
	}

	/**
	 * {@link #getWorkFlowsByWobQueueFilterRed(String, DocumentQueueEnum, String, Long, List, String, List, Integer, Boolean, Collection, String, Date, Date, boolean)}.
	 * 
	 * @param wobNumber
	 * @param queue
	 * @param indexName
	 * @param idNodoDestinatario
	 * @param idUtenteDestinatari
	 * @param idClientAoo
	 * @param idsTipoAssegnazione
	 * @param flagRenderizzato
	 * @param registroRiservato
	 * @param documentTitles
	 * @param dataScadenza
	 * @return
	 */
	public VWQueueQuery getWorkFlowsByWobQueueFilterRed(final String wobNumber, final DocumentQueueEnum queue, final String indexName, final Long idNodoDestinatario,
			final List<Long> idUtenteDestinatari, final String idClientAoo, final List<Long> idsTipoAssegnazione, final Integer flagRenderizzato,
			final Boolean registroRiservato, final Collection<String> documentTitles, final String dataScadenza) {

		return getWorkFlowsByWobQueueFilterRed(wobNumber, queue, indexName, idNodoDestinatario, idUtenteDestinatari, idClientAoo, idsTipoAssegnazione, flagRenderizzato,
				registroRiservato, documentTitles, dataScadenza, null, null, false);
	}
	
	/**
	 * @param wobNumber
	 * @param queue
	 * @param indexName
	 * @param idNodoDestinatario
	 * @param idUtenteDestinatari
	 * @param idClientAoo
	 * @param idsTipoAssegnazione
	 * @param flagRenderizzato
	 * @param registroRiservato
	 * @param documentTitles
	 * @param dataScadenza
	 * @param dataScadenzaFrom
	 * @param dataScadenzaTo
	 * @param flagInScadenza
	 * @return VWQueueQuery
	 */
	public VWQueueQuery getWorkFlowsByWobQueueFilterRed(final String wobNumber, final DocumentQueueEnum queue, final String indexName, final Long idNodoDestinatario,
			final List<Long> idUtenteDestinatari, final String idClientAoo, final List<Long> idsTipoAssegnazione, final Integer flagRenderizzato,
			final Boolean registroRiservato, final Collection<String> documentTitles, final String dataScadenza, final Date dataScadenzaFrom, final Date dataScadenzaTo,
			final boolean flagInScadenza) {

		final String nomeQueue = queue.getName();
		final FilenetPEQueryBuilder fqb = new FilenetPEQueryBuilder(session, nomeQueue);
		if (!StringUtils.isNullOrEmpty(wobNumber)) {
			fqb.and(Constants.PEProperty.F_WOB_NUM, wobNumber.trim());
		}

		if (DocumentQueueEnum.SOSPESO.getName().equals(nomeQueue) || DocumentQueueEnum.IN_ACQUISIZIONE.getName().equals(nomeQueue)) {
			fqb.and(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), idNodoDestinatario.intValue());

			if (idUtenteDestinatari != null && !idUtenteDestinatari.isEmpty()) {
				if (idUtenteDestinatari.size() == 1) {
					fqb.and(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), idUtenteDestinatari.get(0).intValue());
				} else {
					final List<Proposition> propositions = new ArrayList<>();
					for (final Long idUtenteDestinatario : idUtenteDestinatari) {
						propositions.add(fqb.createProposition(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), idUtenteDestinatario.intValue()));
					}
					final Proposition[] longPropositions = propositions.toArray(new Proposition[propositions.size()]);
					fqb.or(longPropositions);
				}
			}
		} else {
			fqb.and(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), idNodoDestinatario.intValue());

			if (idUtenteDestinatari != null && !idUtenteDestinatari.isEmpty()) {
				if (idUtenteDestinatari.size() == 1) {
					fqb.and(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), idUtenteDestinatari.get(0).intValue());
				} else {
					final List<Proposition> propositions = new ArrayList<>();
					for (final Long idUtenteDestinatario : idUtenteDestinatari) {
						propositions.add(fqb.createProposition(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), idUtenteDestinatario.intValue()));
					}
					final Proposition[] longPropositions = propositions.toArray(new Proposition[propositions.size()]);
					fqb.or(longPropositions);
				}
			}
		}

		if (idClientAoo != null) {
			fqb.and(pp.getParameterByKey(PropertiesNameEnum.ID_CLIENT_METAKEY), idClientAoo);
		}
		if (idsTipoAssegnazione != null && !idsTipoAssegnazione.isEmpty()) {
			if (idsTipoAssegnazione.size() > 1) {
				final Long[] idst = idsTipoAssegnazione.toArray(new Long[0]);
				fqb.in(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), idst);
			} else {
				fqb.and(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), idsTipoAssegnazione.get(0).intValue());
			}
		}

		if (flagRenderizzato != null) {
			fqb.and(pp.getParameterByKey(PropertiesNameEnum.FLAG_RENDERIZZATO_METAKEY), flagRenderizzato);
		}

		if (registroRiservato != null) {
			fqb.and(pp.getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY), registroRiservato);
		}

		if (!CollectionUtils.isEmpty(documentTitles)) {
			fqb.in(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY), documentTitles);
		}

		if (dataScadenza != null) {
			fqb.and(pp.getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY), dataScadenza);
		}

		if (flagInScadenza) {
			fqb.andNotNull(pp.getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY));
		}

		if (dataScadenzaFrom != null) {
			// Se la data è uguale a NULL_DATE, la data scadenza deve essere strettamente
			// maggiore
			if (DateUtils.NULL_DATE.equals(LocalDateTime.ofInstant(dataScadenzaFrom.toInstant(), ZoneId.systemDefault()))) {
				fqb.gt(pp.getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY), dataScadenzaFrom);
			} else {
				fqb.ge(pp.getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY), dataScadenzaFrom);
			}
		}

		if (dataScadenzaTo != null) {
			fqb.le(pp.getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY), dataScadenzaTo);
		}
		
		if (DocumentQueueEnum.CORRIERE_DIRETTO.equals(queue)) {
			fqb.and(pp.getParameterByKey(PropertiesNameEnum.ASSEGNAZIONE_INDIRETTA_METAKEY), false);
		}

		if (DocumentQueueEnum.CORRIERE_INDIRETTO.equals(queue)) {
			fqb.and(pp.getParameterByKey(PropertiesNameEnum.ASSEGNAZIONE_INDIRETTA_METAKEY), true);
		}
		
		if (DocumentQueueEnum.DA_LAVORARE_UCB.equals(queue)) {
			fqb.and(pp.getParameterByKey(PropertiesNameEnum.IN_LAVORAZIONE_METAKEY), false);
		}

		if (DocumentQueueEnum.IN_LAVORAZIONE_UCB.equals(queue)) {
			fqb.and(pp.getParameterByKey(PropertiesNameEnum.IN_LAVORAZIONE_METAKEY), true);
		}

		if (DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE.equals(queue)) {
			fqb.and(pp.getParameterByKey(PropertiesNameEnum.FLAG_FIRMA_ASINCRONA_AVVIATA_WF_METAKEY), true);
		}
		
		if (DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE_FEPA.equals(queue)) {
			fqb.and(pp.getParameterByKey(PropertiesNameEnum.FLAG_FIRMA_ASINCRONA_AVVIATA_WF_METAKEY), true);
		}
		
		if (DocumentQueueEnum.NSD.equals(queue)) {
			fqb.and(pp.getParameterByKey(PropertiesNameEnum.FLAG_FIRMA_ASINCRONA_AVVIATA_WF_METAKEY), false);
		}
		
		if (indexName != null) {
			return fqb.createQuery(true, indexName);
		}

		return fqb.createQuery(true);
	}

	/**
	 * Recupero workflow per wob number.
	 * 
	 * @param wobNumber wobnumber
	 * @param eager     flag che indica se recuperare tutte le informazioni presenti
	 *                  sul workflow o meno
	 * @return workflow
	 */
	public VWWorkObject getWorkFlowByWob(final String wobNumber, final Boolean eager) {
		try {
			final String queryFilter = Constants.PEProperty.IW_WORKFLOW_NUMBER + " = :A";
			final Object[] subVars = { new VWWorkObjectNumber(wobNumber) };
			final VWRosterQuery query = getWorkFlowFromRoster(queryFilter, subVars, eager);
			return (VWWorkObject) query.next();
		} catch (final VWException e) {
			LOGGER.error("Errore nel recupero del WF: " + wobNumber + e.getMessage(), e);
			throw new FilenetException(e);
		}
	}

	/**
	 * Recupero workflow query.
	 * 
	 * @param queryFilter filtri da impostare nella query. In caso ci siano
	 *                    variabili, queste devono essere impostate in
	 *                    <code>subVars</code>
	 * @param subVars     valori da sostituire nelle variabili impostate in
	 *                    <code>queryFilter</code>. Da impostare a <code>null</code>
	 *                    in caso <code>queryFilter</code> non abbia variabili
	 * @param eager       flag che indica se recuperare tutte le informazioni
	 *                    presenti sul workflow o meno
	 * @return workflow
	 */
	private VWRosterQuery getWorkFlowFromRoster(final String queryFilter, final Object[] subVars, final Boolean eager) {
		try {
			final VWRoster roster = session.getRoster(pp.getParameterByKey(PropertiesNameEnum.PE_ROSTERNAME_FN_METAKEY));
			Integer queryFlag = VWRoster.QUERY_GET_NO_TRANSLATED_SYSTEM_FIELDS;
			if (Boolean.TRUE.equals(eager)) {
				queryFlag = VWRoster.QUERY_NO_OPTIONS;
			}
			return roster.createQuery(null, null, null, queryFlag, queryFilter, subVars, VWFetchType.FETCH_TYPE_WORKOBJECT);
		} catch (final VWException e) {
			LOGGER.error("Errore nel recupero dei WF: " + queryFilter, e);
			throw new FilenetException(e);
		}
	}

	/**
	 * Recupera le response di un workflow.
	 * 
	 * @param obj workflow
	 * @return insieme delle response
	 */
	public String[] getResponses(final VWWorkObject obj) {
		String[] output = null;

		try {
			if (obj != null) {
				output = obj.getStepResponses();
			}

			return output;
		} catch (final VWException e) {
			LOGGER.error(e);
			throw new FilenetException(e);
		}
	}

	/**
	 * Restituisce la lista delle response.
	 * 
	 * @param query
	 * @return lista di response
	 */
	public String[] getResponses(final VWRosterQuery query) {
		String[] output = null;

		try {
			while (query.hasNext()) {
				output = getResponses((VWWorkObject) query.next());
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero delle response.", e);
			throw new FilenetException("Errore durante il recupero delle response.", e);
		}

		return output;
	}

	/**
	 * Metodo per gestire casi di response duplicate prima di un avanzamnto WF
	 * 
	 * @param obj
	 * @param respEnum
	 * @return
	 */
	public String handleDuplicateResponses(final VWWorkObject obj, final ResponsesRedEnum respEnum) {
		String output = null;

		try {
			final String[] arrayResponses = getResponses(obj);
			if (arrayResponses != null) {
				for (int i = 0; i < arrayResponses.length; i++) {
					if (arrayResponses[i].equalsIgnoreCase(respEnum.getResponse())) {
						output = arrayResponses[i];
					}
				}
			}
		} catch (final VWException e) {
			LOGGER.error(e);
			throw new FilenetException(e);
		}

		return output;
	}

	/**
	 * Recupera tutti i WF al giro visti dato l'Id di un documento.
	 * 
	 * @param idDocumento - Id del documento.
	 * @return Mappa di WobNumber/Workflow ricercati.
	 */
	public Map<String, VWWorkObject> fetchWorkflowsAlGiroVisti(final int idDocumento) {
		Map<String, VWWorkObject> wfRet = null;
		final String queryFilter = pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY) + " = " + idDocumento;
		try {
			final VWQueue alGiroVistiQ = session.getQueue(pp.getParameterByKey(PropertiesNameEnum.AL_GIROVISTI_STEPNAME_WFKEY).split("\\|")[0]);
			final VWQueueQuery query = alGiroVistiQ.createQuery(null, null, null, VWRoster.QUERY_NO_OPTIONS, queryFilter, null, VWFetchType.FETCH_TYPE_WORKOBJECT);
			wfRet = new HashMap<>(query.fetchCount());
			while (query.hasNext()) {
				final VWWorkObject wo = (VWWorkObject) query.next();
				wfRet.put(wo.getWorkflowNumber(), wo);
			}
		} catch (final Exception e) {
			LOGGER.error(ERRORE_DURANTE_LA_RICERCA_DEI_WORKFLOW_PER_ID_DOCUMENTO + idDocumento, e);
			throw new FilenetException(ERRORE_DURANTE_LA_RICERCA_DEI_WORKFLOW_PER_ID_DOCUMENTO + idDocumento, e);
		}
		return wfRet;
	}

	/**
	 * Recupera tutti i WF al giro visti dato l'Id di un documento.
	 * 
	 * @param idDocumento - Id del documento.
	 * @return Workflow ricercati.
	 */
	public Collection<VWWorkObject> fetchWorkflowsAlGiroVistiByDocumentoId(final int idDocumento) {
		Map<String, VWWorkObject> wfRet = null;
		final String queryFilter = pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY) + " = " + idDocumento;
		try {
			final VWQueue alGiroVistiQ = session.getQueue(pp.getParameterByKey(PropertiesNameEnum.AL_GIROVISTI_STEPNAME_WFKEY).split("\\|")[0]);
			final VWQueueQuery query = alGiroVistiQ.createQuery(null, null, null, VWRoster.QUERY_NO_OPTIONS, queryFilter, null, VWFetchType.FETCH_TYPE_WORKOBJECT);

			wfRet = new HashMap<>(query.fetchCount());
			while (query.hasNext()) {
				final VWWorkObject wo = (VWWorkObject) query.next();
				wfRet.put(wo.getWorkflowNumber(), wo);
			}

			final ArrayList<String> parents = new ArrayList<>();
			for (final Entry<String, VWWorkObject> wf : wfRet.entrySet()) {
				final String wfParentNumber = (String) wf.getValue().getFieldValue(pp.getParameterByKey(PropertiesNameEnum.WF_PARENT_NUMBER_METAKEY));
				if (!StringUtils.isNullOrEmpty(wfParentNumber)) {
					parents.add(wfParentNumber);
				}
			}

			final HashMap<String, VWWorkObject> newWfRet = new HashMap<>();

			for (final Entry<String, VWWorkObject> wf : wfRet.entrySet()) {
				if (!parents.contains(wf.getKey())) {
					newWfRet.put(wf.getKey(), wf.getValue());
				}
			}

			final List<Long> documentiId = new ArrayList<>();
			documentiId.add(Long.parseLong(Integer.toString(idDocumento)));
			final VWRosterQuery rQueryNSD = getDocumentWorkFlowsByIdDocumenti(documentiId);

			if (!newWfRet.isEmpty() && rQueryNSD.hasNext()) {
				LOGGER.info("Verifica se i workflow attivi siano figli dei WF al giro visti ");
				while (rQueryNSD.hasNext()) {
					final VWWorkObject wf = (VWWorkObject) rQueryNSD.next();
					LOGGER.info("Viene analizzato il workflow : " + wf.getWorkflowNumber() + " - " + wf.getSubject());
					// vengono esclusi i WF fermi "al giro visto"
					if (!newWfRet.containsKey(wf.getWorkflowNumber())) {
						final Boolean prontoAlloStorno = (Boolean) wf.getFieldValue(pp.getParameterByKey(PropertiesNameEnum.PRONTO_ALLO_STORNO_METAKEY));
						final String parentWFNumber = (String) wf.getFieldValue(pp.getParameterByKey(PropertiesNameEnum.WF_PARENT_NUMBER_METAKEY));
						LOGGER.info("Workflow padre: " + parentWFNumber);
						// il workflow in esame è un 'figlio' di uno fermo al giro visti?
						if (wfRet.containsKey(parentWFNumber) && prontoAlloStorno != null && Boolean.FALSE.equals(prontoAlloStorno)) {
							// pronto allo storno indica che sta per terminare, evita di considerare i wf in
							// step execute db o invoke ws
							newWfRet.remove(parentWFNumber);
						}
					} else {
						LOGGER.info("Workflow al giro visti");

					}
				}
			}
			return newWfRet.values();
		} catch (final Exception e) {
			LOGGER.error(ERRORE_DURANTE_LA_RICERCA_DEI_WORKFLOW_PER_ID_DOCUMENTO + idDocumento, e);
			throw new FilenetException(e);
		}
	}

	/**
	 * Da utilizzare quando il nodo mittente dello step attuale non cambia
	 * 
	 * @param wobNumber
	 * @param idUtente
	 * @param response
	 * @param inMetadati
	 * @return
	 */
	public boolean nextStepMittente(final String wobNumber, final Long idUtente, final String response, final Map<String, Object> inMetadati) {
		Map<String, Object> metadati = inMetadati;
		if (metadati == null) {
			metadati = new HashMap<>();
		}
		metadati.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), idUtente);
		metadati.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), "");
		return nextStep(getWorkFlowByWob(wobNumber, true), metadati, response);
	}

	/**
	 * Avanzamento del WF partendo dal WobNumber.
	 * 
	 * @param wobNumber
	 * @param metadati
	 * @param response
	 * @return
	 */
	public boolean nextStep(final String wobNumber, final Map<String, Object> metadati, final String response) {
		return nextStep(getWorkFlowByWob(wobNumber, true), metadati, response);
	}

	/**
	 * Avanza il WFe aggiorna i metadati.
	 * 
	 * @param wo       - WObject del WF.
	 * @param metadati - Metadati da aggiornare.
	 * @param response - Response per l'avanzamento del WF.
	 * @return esito.
	 */
	public boolean nextStep(final VWWorkObject wo, final Map<String, Object> metadati, final String response) {
		boolean ret = false;
		try {
			wo.doLock(false);
			if (metadati != null && !metadati.isEmpty()) {
				for (final Entry<String, Object> entry : metadati.entrySet()) {
					Object value = entry.getValue();
					if (value instanceof Long) {
						value = ((Long) value).intValue();
					}
					wo.setFieldValue(entry.getKey(), value, false);
				}
			}
			if (wo.getStepResponses() != null && !StringUtils.isNullOrEmpty(response)) {
				wo.setSelectedResponse(response);
			}
			wo.doDispatch();
			ret = true;
		} catch (final Exception e) {
			LOGGER.error("FilenetPEHelper.nextStep " + e.getMessage(), e);
			try {
				wo.doAbort();
			} catch (final VWException e1) {
				LOGGER.error("FilenetPEHelper.nextStep trying to abort " + e1.getMessage(), e1);
			}
			throw new FilenetException(e);
		}
		return ret;
	}

	/**
	 * Esegue lo step.doDispatch.
	 * 
	 * @param idClientAoo
	 * @param idDocumento
	 * @param response
	 * @throws Exception
	 */
	public void nextStepWithOrWithoutResponseIdDocumento(final String idClientAoo, final String idDocumento, final String response) {

		if (idDocumento != null && !"".equals(idDocumento)) {
			final VWWorkObject wob = getWorkflowPrincipale(idDocumento, idClientAoo);

			if (wob != null) {
				final String[] wobResponses = getResponses(wob);
				final List<String> wobResponsesList = Arrays.asList(wobResponses);
				if (response != null && wobResponsesList.contains(response)) {
					LOGGER.info("idDocumento = " + idDocumento);
					LOGGER.info("lo StepElement contiente la response [" + response + "]");
					final VWStepElement step = wob.fetchStepElement();

					step.doLock(true);
					step.setSelectedResponse(response);
					step.doDispatch();

					LOGGER.info("step.doDispatch eseguito");
				} else {
					LOGGER.info("idDocumento = " + idDocumento);
					wob.doLock(true);
					wob.doDispatch();
					LOGGER.info("wob.doDispatch eseguito");
				}
			}
		}

	}

	/**
	 * Esegue lo step.doDispatch.
	 * 
	 * @param idClientAoo
	 * @param wobNumber
	 * @param response
	 * @throws Exception
	 */
	public void nextStepWithOrWithoutResponseWob(final String wobNumber, final String response) {

		if (wobNumber != null && !"".equals(wobNumber)) {
			final VWWorkObject wob = getWorkFlowByWob(wobNumber, true);

			if (wob != null) {
				final String[] wobResponses = getResponses(wob);
				final List<String> wobResponsesList = Arrays.asList(wobResponses);
				if (response != null && wobResponsesList.contains(response)) {
					LOGGER.info("wob = " + wob);
					LOGGER.info("lo StepElement contiente la response [" + response + "]");
					final VWStepElement step = wob.fetchStepElement();

					step.doLock(true);
					step.setSelectedResponse(response);
					step.doDispatch();

					LOGGER.info("step.doDispatch eseguito");
				} else {
					LOGGER.info("wob = " + wob);
					wob.doLock(true);
					wob.doDispatch();
					LOGGER.info("wob.doDispatch eseguito");
				}
			}
		}

	}

	/**
	 * Avanza il wo, sulla response in input. Se la response è null, utilizza la
	 * prima response restituita dallo step corrente.
	 * 
	 * @param wo       - WObject del WF.
	 * @param response - Response per l'avanzamento del WF.
	 * @return esito.
	 */
	public boolean nextStepFromStepElement(final VWWorkObject wo, final String response) {
		VWStepElement step = null;
		boolean ret = false;
		try {
			wo.doLock(true);
			step = wo.fetchStepElement();
			step.doLock(true);

			String responseToDispatch = response;
			if (responseToDispatch == null) {
				final String[] responses = step.getStepResponses();
				responseToDispatch = responses[0];
			}

			if (responseToDispatch != null && !"".equals(responseToDispatch)) {
				step.setSelectedResponse(responseToDispatch);
			}

			step.doDispatch();
			ret = true;
		} catch (final VWException e) {
			LOGGER.error("FilenetPEHelper.nextStepFromStepElement " + e.getMessage(), e);
			final VWStepElement[] steps = new VWStepElement[1];
			steps[0] = step;
			try {
				VWStepElement.doUnlockMany(steps, true, false);
			} catch (final VWException e1) {
				LOGGER.error("FilenetPEHelper.nextStepFromStepElement trying to unlockmany " + e1.getMessage(), e1);
			}
			throw new FilenetException(e);
		} catch (final Exception e) {
			LOGGER.error("FilenetPEHelper.nextStepFromStepElement " + e.getMessage(), e);
			try {
				wo.doAbort();
			} catch (final VWException e1) {
				LOGGER.error("FilenetPEHelper.nextStepFromStepElement trying to abort " + e1.getMessage(), e1);
			}
			throw new FilenetException(e);
		}
		return ret;
	}

	/**
	 * Restituisce i wob numbers associati al workflow.
	 * 
	 * @param workFlowNumber
	 * @return List di WobNumberWsDTO
	 */
	public List<WobNumberWsDTO> getWobNumbers(final String workFlowNumber) {
		final List<WobNumberWsDTO> output = new ArrayList<>();
		VWWorkObject vWWorkObject = null;
		WobNumberWsDTO wobNumber = null;
		final List<RouteWsDTO> route = new ArrayList<>();

		try {

			final String queryFilter = Constants.PEProperty.IW_WORKFLOW_NUMBER + " = :A";
			final Object[] subVars = { new VWWorkObjectNumber(workFlowNumber) };
			final VWRosterQuery query = getWorkFlowFromRoster(queryFilter, subVars, Boolean.TRUE);

			while (query.hasNext()) {
				vWWorkObject = (VWWorkObject) query.next();

				wobNumber = new WobNumberWsDTO();
				wobNumber.setWobNumber(vWWorkObject.getWorkObjectNumber());

				final VWStepElement step = vWWorkObject.fetchStepElement();
				wobNumber.setStepName(step.getStepName());
				wobNumber.setStepDescription(step.getStepDescription());

				final VWProcess process = step.fetchProcess();
				final VWWorkflowDefinition ws = process.fetchWorkflowDefinition(false);
				final VWMapDefinition md = ws.getMainMap();

				for (final VWMapNode node : md.getSteps()) {

					if (step.getStepName() != null && step.getStepName().equals(node.getName())) {

						final List<String> responses = Arrays.asList(step.getStepResponses());
						if (responses != null) {
							wobNumber.setResponses(responses);
						}

						final List<RouteWsDTO> alRoute = new ArrayList<>();
						final VWRouteDefinition[] routes = node.getNextRoutes();
						if (routes != null) {
							for (final VWRouteDefinition routeDef : routes) {
								final RouteWsDTO routeCustom = new RouteWsDTO();
								routeCustom.setRouteName(routeDef.getName());
								routeCustom.setCondition(routeDef.getCondition());
								routeCustom.setDestinationStep(routeDef.getDestinationStep().getName());
								alRoute.add(routeCustom);
							}
						}

						route.addAll(alRoute);
					}
				}

				if ((wobNumber.getResponses() == null || wobNumber.getResponses().isEmpty())
						&& (vWWorkObject.getStepResponses() != null && vWWorkObject.getStepResponses().length > 0)) {

					wobNumber.setResponses(Arrays.asList(vWWorkObject.getStepResponses()));

				}

				wobNumber.setRoute(route);
				output.add(wobNumber);
			}

		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dei WobNumbers: " + e.getMessage(), e);
			throw new FilenetException("Errore nel recupero dei WobNumbers: " + e.getMessage(), e);
		}

		return output;
	}

	/**
	 * Restituisce il workflow principale del documento identificato dall'idDoc.
	 * 
	 * @param idDoc
	 * @param idClientAoo
	 * @return VWWorkObject
	 */
	public VWWorkObject getWorkflowPrincipale(final String idDoc, final String idClientAoo) {
		try {
			String queryFilter = "(" + pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY) + " = " + idDoc + ")";

			if (idClientAoo != null) {
				queryFilter += " AND (" + pp.getParameterByKey(PropertiesNameEnum.ID_CLIENT_METAKEY) + " = '" + idClientAoo + "')";
			}

			queryFilter += " AND F_Originator > 0 AND F_Subject NOT LIKE '%contributo%' ";
			final VWRosterQuery roster = getWorkFlowFromRoster(queryFilter, null, true);
			return (VWWorkObject) roster.next();
		} catch (final VWException e) {
			LOGGER.error("Errore nel recupero del WF: " + e.getMessage(), e);
			throw new FilenetException("Errore nel recupero del WF. ", e);
		}
	}

	/**
	 * Recupera workflow sulla base della lista di id documenti fornita in ingresso.
	 * 
	 * @param idDocIterator iterator di id documenti (in formato stringa o numerico)
	 * @return insieme di workflow
	 */
	public VWRosterQuery getDocumentWorkFlowsByIdDocumenti(final List<Long> idDocs) {
		try {
			final String queryFilter = "(" + StringUtils.createInCondition(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY), idDocs.iterator(), false) + ")";

			return getWorkFlowFromRoster(queryFilter, null, true);
		} catch (final VWException e) {
			LOGGER.error("Errore nel recupero dei workflow: " + e.getMessage(), e);
			throw new FilenetException("Errore nel recupero dei workflow. ", e);
		}
	}

	/**
	 * Recupera workflow sulla base della lista di wobnumber fornita in ingresso.
	 * 
	 * @param wobNumbers lista wobnumber
	 * @return insieme di workflow
	 */
	public VWRosterQuery getDocumentWorkFlowsByWobNumbers(final Collection<String> wobNumbers) {
		try {
			final String queryFilter = "(" + StringUtils.createInCondition(Constants.PEProperty.IW_WORKFLOW_NUMBER, wobNumbers.iterator(), true) + ")";

			return getWorkFlowFromRoster(queryFilter, null, true);
		} catch (final VWException e) {
			LOGGER.error("Errore nel recupero dei workflow: " + e.getMessage(), e);
			throw new FilenetException("Errore nel recupero dei workflow.", e);
		}
	}

	/**
	 * Restituisce il VWRosterQuery.
	 * 
	 * @param idDocumento
	 * @param idClientAoo
	 * @return VWRosterQuery
	 */
	public VWRosterQuery getRosterQueryWorkByIdDocumentoAndCodiceAOO(final String idDocumento, final String idClientAoo) {
		VWRosterQuery query = null;
		try {
			final StringBuilder sb = new StringBuilder(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY)).append(" = ").append(idDocumento);

			if (idClientAoo != null) {
				sb.append(AND).append(pp.getParameterByKey(PropertiesNameEnum.ID_CLIENT_METAKEY)).append(" = '").append(idClientAoo).append("' ");
			}

			query = getWorkFlowFromRoster(sb.toString(), null, true);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new FilenetException("Errore nel recupero del WF. ", e);
		}

		return query;
	}

	/**
	 * Ritorna le assegnaizioni ordinate per data.
	 * 
	 * @param idDocumento
	 * @param codiceAoo
	 * @param query
	 * @return
	 */
	public List<AssegnazioneDTO> getAssegnazioni(final String idDocumento, final String codiceAoo, final VWRosterQuery query) {
		final List<AssegnazioneDTO> assegnazioniList = new ArrayList<>();

		try {
			while (query.hasNext()) {
				assegnazioniList.add(getAssegnazione(codiceAoo, (VWWorkObject) query.next()));
			}
		} catch (final Exception e) {
			LOGGER.error(ERRORE_DURANTE_LA_RICERCA_DELLE_ASSEGNAZIONI + idDocumento, e);
			throw new FilenetException(ERRORE_DURANTE_LA_RICERCA_DELLE_ASSEGNAZIONI + idDocumento, e);
		}

		return assegnazioniList;
	}

	/**
	 * Restituisce l'assegnazione associata al workflow e all'AOO.
	 * 
	 * @param codiceAoo
	 * @param wo
	 * @return AssegnazioneDTO
	 */
	public static AssegnazioneDTO getAssegnazione(final String codiceAoo, final VWWorkObject wo) {
		final PropertiesProvider pp = PropertiesProvider.getIstance();

		// Ottieni i metadati
		final Integer idUtenteDestinatario = (Integer) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY));

		final Integer idNodoDestinatario = (Integer) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY));

		final Integer idTipoAssegnazioneDestinatario = (Integer) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY));

		final Date dataAssegnazione = (Date) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.DATA_ASSEGNAZIONE_WF_METAKEY));
		String dataAssegnazioneStr = null;
		if (dataAssegnazione != null) {
			final SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.DD_MM_YYYY);
			dataAssegnazioneStr = sdf.format(dataAssegnazione);
		}

		final String motivoAssegnazione = (String) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY));

		Long idAoo = null;
		final Integer idAooInt = (Integer) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ID_AOO_WF_METAKEY));
		if (idAooInt != null) {
			idAoo = Long.valueOf(idAooInt);
		}

		final TipoAssegnazioneEnum ta = TipoAssegnazioneEnum.get(idTipoAssegnazioneDestinatario);

		Long idNodo = 0L;
		if (idNodoDestinatario != null) {
			idNodo = idNodoDestinatario.longValue();
		}

		Long idUtente = 0L;
		if (idUtenteDestinatario != null) {
			idUtente = idUtenteDestinatario.longValue();
		}

		final UtenteDTO utente = new UtenteDTO(idUtente, idAoo, null, idNodo, null, null, null, null, null, null, codiceAoo);
		final UfficioDTO ufficio = new UfficioDTO(idNodo, null);

		final AssegnazioneDTO assegnazione = new AssegnazioneDTO(motivoAssegnazione, ta, dataAssegnazioneStr, dataAssegnazione, null, utente, ufficio);
		assegnazione.setWobNumber(wo.getWorkObjectNumber());

		return assegnazione;
	}

	/**
	 * Recupera assegnazioni di uno specifico documento. ho dovuto spezzare questo
	 * metodo perché più metodi hanno bisogno di andare sul roster
	 * 
	 * @param idDocumento identificativo documento
	 * @param codiceAoo   identificativo aoo
	 * @param idClientAoo identificativo client aoo
	 * @return insieme delle assegnazioni
	 */
	public List<AssegnazioneDTO> getAssegnazioni(final String idDocumento, final String codiceAoo, final String idClientAoo) {
		try {
			final VWRosterQuery query = getRosterQueryWorkByIdDocumentoAndCodiceAOO(idDocumento, idClientAoo);

			return getAssegnazioni(idDocumento, codiceAoo, query);
		} catch (final Exception e) {
			LOGGER.error(ERRORE_DURANTE_LA_RICERCA_DELLE_ASSEGNAZIONI + idDocumento, e);
			throw new FilenetException(ERRORE_DURANTE_LA_RICERCA_DELLE_ASSEGNAZIONI + idDocumento, e);
		}
	}

	/**
	 * Recupera le assegnazioni storico per ID documento e AOO.
	 * 
	 * @param idDocumento - Id documento.
	 * @param codiceAoo   - Codice AOO.
	 * @return Array delle assegnazioni.
	 */
	public String[] getAssegnazioniStorico(final String idDocumento, final String idClientAoo) {
		String[] assegnazioni = new String[0];
		final List<String> assegnazioniList = new ArrayList<>();
		try {

			final StringBuilder sb = new StringBuilder();
			sb.append(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY) + " = " + idDocumento);

			if (idClientAoo != null) {
				sb.append(AND + pp.getParameterByKey(PropertiesNameEnum.ID_CLIENT_METAKEY) + " = " + "'" + idClientAoo + "'");
			}

			final VWRosterQuery query = getWorkFlowFromRoster(sb.toString(), null, true);

			String currentAssegnazione;
			String currAssegnazioneCoordinatore;
			// restituisce il risultato della ricerca
			while (query.hasNext()) {
				final VWWorkObject wo = (VWWorkObject) query.next();
				// Ottieni i metadati
				final List<String> fieldNames = Arrays.asList(wo.getFieldNames());
				String idUtenteStr = "";
				if (fieldNames.contains(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY))) {
					final Integer idUtenteDest = (Integer) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY));
					idUtenteStr = "";
					if (idUtenteDest != null) {
						idUtenteStr = idUtenteDest + "";
					}
				}
				String idNodoStr = "";
				if (fieldNames.contains(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY))) {
					final Integer idNodoDest = (Integer) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY));
					idNodoStr = "";
					if (idNodoDest != null) {
						idNodoStr = idNodoDest + "";
					}
				}

				Integer idUtenteCoordinatore = null;
				if (fieldNames.contains(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_COORDINATORE_METAKEY))) {
					idUtenteCoordinatore = (Integer) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_COORDINATORE_METAKEY));
				}
				Integer idNodoCoordinatore = null;
				if (fieldNames.contains(pp.getParameterByKey(PropertiesNameEnum.ID_UFFICIO_COORDINATORE_METAKEY))) {
					idNodoCoordinatore = (Integer) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ID_UFFICIO_COORDINATORE_METAKEY));
				}

				// Se valorizzato inserire la security sul coordinatore
				if (idUtenteCoordinatore != null && idUtenteCoordinatore != 0 && idNodoCoordinatore != null && idNodoCoordinatore != 0) {
					Integer idUtenteCoord = 0;
					if (idUtenteCoordinatore != null) {
						idUtenteCoord = idUtenteCoordinatore;
					}
					currAssegnazioneCoordinatore = idNodoCoordinatore + "," + idUtenteCoord;
					assegnazioniList.add(currAssegnazioneCoordinatore);
				}

				// Destinatario corrente del processo
				if (idNodoStr != null && !"".equals(idNodoStr) && !"0".equals(idNodoStr) && idUtenteStr != null && !"".equals(idUtenteStr)) {
					String idUtenteTmp = "0";
					if (!StringUtils.isNullOrEmpty(idUtenteStr)) {
						idUtenteTmp = idUtenteStr;
					}
					currentAssegnazione = idNodoStr + "," + idUtenteTmp;
					assegnazioniList.add(currentAssegnazione);
				}

				// Firmatari
				final String[] firmatari = (String[]) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ELENCO_LIBROFIRMA_METAKEY));
				if (firmatari != null && firmatari.length > 0 && !"".equals(firmatari[0])) {
					// Se esistono assegnazioni per firma / sigla / visto si
					// tratta di un monoprocesso
					// quindi recupero le assegnazioni dal metadato
					// elencoLibroFirma
					assegnazioniList.addAll(Arrays.asList(firmatari));
				}

				// R.C. Gestione destinatari interni
				final String[] destinatari = (String[]) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ELENCO_DESTINATARI_INTERNI_METAKEY));
				if (destinatari != null && destinatari.length > 0 && !"".equals(destinatari[0])) {
					// Se esistono assegnazioni per destinatari interni
					assegnazioniList.addAll(Arrays.asList(destinatari));
				}

				// R.C. Gestione elenco Visti

				final String[] vistatari = (String[]) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ELENCO_VISTI_METAKEY));
				if (vistatari != null && vistatari.length > 0 && !"".equals(vistatari[0])) {
					// Se esistono assegnazioni per visto
					assegnazioniList.addAll(Arrays.asList(vistatari));
				}

				// R.C. Gestione elenco Conoscenze
				final String[] conoscenze = (String[]) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONOSCENZA_STORICO_METAKEY));
				if (conoscenze != null && conoscenze.length > 0 && !"".equals(conoscenze[0])) {
					// Se esistono assegnazioni per conoscenza
					assegnazioniList.addAll(Arrays.asList(conoscenze));
				}
				gestioneElencoContributi(assegnazioniList, wo);
				gestioneElencoVistiUffici(assegnazioniList, wo);
				gestioneElencoVistiIspettorati(assegnazioniList, wo);
				gestioneRiassegnazioni(assegnazioniList, wo);
			}

			assegnazioni = assegnazioniList.toArray(assegnazioni);

		} catch (final Exception e) {
			LOGGER.error(ERRORE_DURANTE_LA_RICERCA_DELLE_ASSEGNAZIONI + idDocumento, e);
			throw new FilenetException(ERRORE_DURANTE_LA_RICERCA_DELLE_ASSEGNAZIONI + idDocumento, e);
		}
		return assegnazioni;
	}

	/**
	 * Recupera e setta le assegnazioni contributi.
	 *
	 * @param assegnazioniList - Lista delle assegnazioni da settare.
	 * @param wo               - WObject del WF.
	 */
	private void gestioneElencoContributi(final List<String> assegnazioniList, final VWWorkObject wo) {
		// R.C. Gestione elenco Contributi
		final String[] contributi = (String[]) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONTRIBUTI_STORICO_METAKEY));
		if (contributi != null && contributi.length > 0 && !"".equals(contributi[0])) {
			// Se esistono assegnazioni per contributo
			assegnazioniList.addAll(Arrays.asList(contributi));
		}
	}

	/**
	 * Recupera e setta le assegnazioni visti.
	 *
	 * @param assegnazioniList - Lista delle assegnazioni da settare.
	 * @param wo               - WObject del WF.
	 */
	private void gestioneElencoVistiUffici(final List<String> assegnazioniList, final VWWorkObject wo) {
		// R.C. Gestione Elenco Visti Uffici
		final String[] vistiUffici = (String[]) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ELENCO_UFFICI_VISTI_METAKEY));
		if (vistiUffici != null && vistiUffici.length > 0 && !"".equals(vistiUffici[0])) {
			// Se esistono assegnazioni per Elenco Visti Uffici
			for (int i = 0; i < vistiUffici.length; i++) {
				assegnazioniList.add(vistiUffici[i] + "," + "0");
			}
		}
	}

	/**
	 * Recupera e setta le assegnazioni visti ispettore.
	 *
	 * @param assegnazioniList - Lista delle assegnazioni da settare.
	 * @param wo               - WObject del WF.
	 */
	private void gestioneElencoVistiIspettorati(final List<String> assegnazioniList, final VWWorkObject wo) {
		// R.C. Gestione Elenco Visti Ispettorati
		final String[] vistiIspettorati = (String[]) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ELENCO_VISTI_ISPETTORATI_METAKEY));
		if (vistiIspettorati != null && vistiIspettorati.length > 0 && !"".equals(vistiIspettorati[0])) {
			// Se esistono assegnazioni per Elenco Visti Ispettorati
			for (int i = 0; i < vistiIspettorati.length; i++) {
				assegnazioniList.add(vistiIspettorati[i] + "," + "0");
			}
		}
	}

	/**
	 * Recupera e setta le riassegnazioni.
	 *
	 * @param assegnazioniList - Lista delle assegnazioni da settare.
	 * @param wo               - WObject del WF.
	 */
	private void gestioneRiassegnazioni(final List<String> assegnazioniList, final VWWorkObject wo) {
		// Gestione Riassegnazioni
		final String[] elencoitemAssegnatari = (String[]) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ITEM_ASSEGNATARI_METAKEY));
		if (elencoitemAssegnatari != null && elencoitemAssegnatari.length > 0 && !"".equals(elencoitemAssegnatari[0])) {
			// Se esistono assegnazioni per Elenco Visti Ispettorati
			for (int i = 0; i < elencoitemAssegnatari.length; i++) {
				// elenco assegnatari ha il seguente formato:
				// idufficio,idutente,tipoassegnazione, (es. 41,0,3,).
				// Viene trasformato in idufficio,idutente
				if (elencoitemAssegnatari[i] != null && !"".equals(elencoitemAssegnatari[i])) {
					final String assegnatario = elencoitemAssegnatari[i];

					String assegnatariotmp = assegnatario;
					if (assegnatario.endsWith(",")) {
						assegnatariotmp = assegnatario.substring(0, assegnatario.length() - 1);
					}
					assegnatariotmp = assegnatariotmp.substring(0, assegnatariotmp.lastIndexOf(','));
					assegnazioniList.add(assegnatariotmp);
				}
			}
		}
	}

	/**
	 * Metodo per avviare workflow.
	 * 
	 * @param inNameWf      nome workflow
	 * @param inMetadati    metadati workflow
	 * @param inIdDocumento identificativo documento
	 * @param inIdClientAOO identificativo client aoo
	 * @return workflow number
	 */
	public String avviaWF(final String inNameWf, final Map<String, Object> inMetadati, final Integer inIdDocumento, final String inIdClientAOO, final boolean isRiavvio) {
		return avviaWF(inNameWf, inMetadati, inIdDocumento, inIdClientAOO, isRiavvio, null);
	}

	/**
	 * Metodo per avviare workflow.
	 * 
	 * @param inNameWf           nome workflow
	 * @param inMetadati         metadati workflow
	 * @param inIdDocumento      identificativo documento
	 * @param inIdClientAOO      identificativo client aoo
	 * @param inSelectedResponse response selezionata per l'avvio del workflow
	 * @return workflow number
	 */
	@SuppressWarnings("rawtypes")
	public String avviaWF(final String inNameWf, final Map<String, Object> inMetadati, final Integer inIdDocumento, final String inIdClientAOO, final boolean isRiavvio,
			final String selectedResponse) {
		String m = "";

		try {
			final VWStepElement launchStep = session.createWorkflow(inNameWf);
			if (isRiavvio) {
				launchStep.setComment("Riattivazione procedimento per numero: " + inIdDocumento);
			} else {
				launchStep.setComment("Inserimento documento numero: " + inIdDocumento);
			}
			launchStep.setParameterValue(pp.getParameterByKey(PropertiesNameEnum.ID_CLIENT_METAKEY), inIdClientAOO, false);

			if (!StringUtils.isNullOrEmpty(selectedResponse)) {
				launchStep.setSelectedResponse(selectedResponse);
			}

			for (final Entry<String, Object> metadato : inMetadati.entrySet()) {
				if ((metadato.getValue() != null) /* && (alNames.contains(metadato.getKey())) */) {
					Object obj = metadato.getValue();
					if (obj instanceof Collection) {
						obj = ((Collection) obj).toArray();
					} else if (obj instanceof Long) {
						obj = ((Long) obj).intValue();
					}
					m = metadato.getKey();
					if (launchStep.getParameter(m) != null) {
						launchStep.setParameterValue(metadato.getKey(), obj, false);
					}
				}
			}

			launchStep.doDispatch();

			return launchStep.getWorkflowNumber();
		} catch (final Exception e) {
			if (!StringUtils.isNullOrEmpty(m)) {
				LOGGER.error("Errore in fase di avvio di un nuovo workflow: Metadato non gestito :" + m + " " + e.getMessage(), e);
			} else {
				LOGGER.error("Errore in fase di avvio di un nuovo workflow: " + e.getMessage(), e);
			}
			throw new RedException(e);
		}
	}

	/**
	 * Metodo per ottentere un metadato sapendo il wobNumber ed il metadato da
	 * cercare
	 * 
	 * @param wobNumber
	 * @param nomeMetadato
	 * @return il metadato
	 */
	public Object getMetadato(final String wobNumber, final String nomeMetadato) {
		return TrasformerPE.getMetadato(getWorkFlowByWob(wobNumber, false), nomeMetadato);
	}

	/**
	 * Esegue il dispatch dei flussi e restituisce la lista degli id documento per i
	 * quali non è stato possibile portare a termine l'operazione
	 * 
	 * @param idDocumentoList
	 * @param response
	 * @return
	 */
	public List<Long> dispatchManualeFlussi(final List<Long> idDocumentoList, final String response) {

		VWStepElement step = null;
		VWWorkObject vwobj = null;
		String tipologiaFirma = null;
		String wob = null;
		final List<Long> idDocumentToRollback = new ArrayList<>();
		try {

			VWRosterQuery workflows = null;
			try {
				workflows = getDocumentWorkFlowsByIdDocumenti(idDocumentoList);
			} catch (final FilenetException e) {
				LOGGER.warn(e);
				return idDocumentoList;
			}

			Long idDocumento = null;
			while (workflows.hasNext()) {
				try {
					vwobj = (VWWorkObject) workflows.next();
					wob = vwobj.getWorkflowNumber();
					idDocumento = ((Integer) TrasformerPE.getMetadato(vwobj, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY))).longValue();
					tipologiaFirma = (String) TrasformerPE.getMetadato(vwobj, pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_FIRMA_WF_METAKEY));

					if (tipologiaFirma != null && !"".equals(tipologiaFirma) && wob != null && !"".equalsIgnoreCase(wob)) {

						vwobj.doLock(true);
						step = vwobj.fetchStepElement();
						step.doLock(true);
						step.setSelectedResponse(response);
						step.doDispatch();
						LOGGER.info("Invocata response " + response + " sul workflow del documento " + idDocumento);
					}

				} catch (final Exception e) {
					LOGGER.warn("Errore in fase di chiusura del workflow per il documento " + idDocumento, e);
					idDocumentToRollback.add(idDocumento);
				}

			}

		} catch (final VWException e) {
			final VWStepElement[] steps = new VWStepElement[1];
			steps[0] = step;
			try {
				VWStepElement.doUnlockMany(steps, true, false);
			} catch (final VWException e1) {
				LOGGER.error(e1);
			}
			throw new RedException(e);
		}

		return idDocumentToRollback;

	}

	/**
	 * Metodo che indica la presenza di workflow attivi
	 * 
	 * @param idDocumento
	 * @param idUfficioDestinatario
	 * @param idClientAoo
	 * @return
	 */
	public List<VWWorkObject> getWorkflowsByIdDocumentoNodo(final int idDocumento, final Long idUfficioDestinatario, final String idClientAoo) {
		return getWorkflowsByIdDocumentoNodoUtente(idDocumento, idUfficioDestinatario, 0L, idClientAoo);
	}

	/**
	 * Restituisce il workflow associato ad un documento dato l'id e l'utente.
	 * 
	 * @param idDocumento
	 * @param idUfficioDestinatario
	 * @param idUtenteDestinatario
	 * @param idClientAoo
	 * @return workflow
	 */
	public List<VWWorkObject> getWorkflowsByIdDocumentoNodoUtente(final int idDocumento, final Long idUfficioDestinatario, final Long idUtenteDestinatario,
			final String idClientAoo) {
		List<VWWorkObject> wfRet = null;
		try {
			// Imposta i criteri di ricerca
			final StringBuilder sb = new StringBuilder(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY) + " = " + idDocumento);

			if (idClientAoo != null) {
				sb.append(AND).append(pp.getParameterByKey(PropertiesNameEnum.ID_CLIENT_METAKEY)).append(" = '").append(idClientAoo).append("' ");
			} else {
				sb.append(AND).append(pp.getParameterByKey(PropertiesNameEnum.ID_CLIENT_METAKEY)).append(" = '")
						.append(pp.getParameterByKey(PropertiesNameEnum.WS_IDCLIENT)).append("' ");
			}

			if (idUfficioDestinatario > 0) {
				sb.append(" AND idNodoDestinatario = ").append(idUfficioDestinatario);
			}
			if (idUtenteDestinatario > 0) {
				sb.append(" AND idUtenteDestinatario = ").append(idUtenteDestinatario);
			}

			final VWRosterQuery query = getWorkFlowFromRoster(sb.toString(), null, true);

			wfRet = new ArrayList<>();
			// Restituisce il risultato della ricerca
			while (query.hasNext()) {
				final VWWorkObject wo = (VWWorkObject) query.next();
				wfRet.add(wo);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la ricerca dei workflow attivi per il nodo destinatario: " + idUfficioDestinatario + " e l'utente: " + idUtenteDestinatario, e);
			throw new FilenetException(
					"Errore durante la ricerca dei workflow attivi per il nodo destinatario: " + idUfficioDestinatario + " e l'utente: " + idUtenteDestinatario, e);
		}

		return wfRet;
	}

	/**
	 * Restituisce la lista dei documenti associati ai workflow recuperati dal
	 * VWQueueQuery.
	 * 
	 * @param vwqq
	 * @return lista dei documenti
	 */
	public List<String> getIdDocumenti(final VWQueueQuery vwqq) {
		final List<String> output = new ArrayList<>();

		while (vwqq.hasNext()) {
			final VWWorkObject wo = (VWWorkObject) vwqq.next();
			output.add(((Integer) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY))).toString());
		}

		return output;
	}

	/**
	 * Metodo per verificare se un determinato documento, con una specifica
	 * assegnazione, è gia presente in coda
	 * 
	 * @param queue
	 * @param documentTitle
	 * @param idNodoDestinatario
	 * @param idUtenteDestinatario
	 * @param idsTipoAssegnazioni
	 * @param registroRiservato
	 * @return true se la coda selezionata contiene gia questo Documento
	 */
	public boolean hasDocumentInCoda(final DocumentQueueEnum queue, final String documentTitle, final Long idNodoDestinatario, final Long idUtenteDestinatario,
			final List<Long> idsTipoAssegnazioni, final Boolean registroRiservato) {

		boolean hasWorkflowsAttivi = false;
		try {
			final List<Long> idsUtentiDestinatari = new ArrayList<>();
			if (idUtenteDestinatario != null) {
				idsUtentiDestinatari.add(idUtenteDestinatario);
			}
			final VWQueueQuery queryQueue = getWorkFlowsByWobQueueFilterRed(null, queue, queue.getIndexName(), idNodoDestinatario, idsUtentiDestinatari, null,
					idsTipoAssegnazioni, null, registroRiservato, Arrays.asList(documentTitle), null);

			final int numeroElementi = queryQueue.fetchCount();
			if (numeroElementi > 0) {
				hasWorkflowsAttivi = true;
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei WorkFlow attivi per il Documento con DocumentTitle: " + documentTitle + NELLA_CODA_LABEL + queue.getName(), e);
			throw new FilenetException(
					"Errore durante il recupero dei WorkFlow attivi per il Documento con DocumentTitle: " + documentTitle + NELLA_CODA_LABEL + queue.getName());
		}
		return hasWorkflowsAttivi;
	}

	/**
	 * Restituisce true se il documento identificato dal wobnumber è contenuto nella
	 * coda passata come parametro.
	 * 
	 * @param wobNumber
	 * @param queue
	 * @return true se il wobnumber esiste nella coda, false altrimenti.
	 */
	public boolean isWobInCoda(final String wobNumber, final DocumentQueueEnum queue) {
		boolean isWobIncoda = false;
		try {
			final FilenetPEQueryBuilder fqb = new FilenetPEQueryBuilder(session, queue.getName());
			if (wobNumber != null) {
				fqb.and(Constants.PEProperty.F_WOB_NUM, wobNumber.trim());
			}
			VWQueueQuery queryQueue = null;
			if (queue.getIndexName() != null) {
				queryQueue = fqb.createQuery(true, queue.getIndexName());
			} else {
				queryQueue = fqb.createQuery(true);
			}

			final int numeroElementi = queryQueue.fetchCount();
			if (numeroElementi > 0) {
				isWobIncoda = true;
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei WorkFlow attivi per il Documento con wob:" + wobNumber + NELLA_CODA_LABEL + queue.getName(), e);
			throw new FilenetException("Errore durante il recupero dei WorkFlow attivi per il Documento con wob: " + wobNumber + NELLA_CODA_LABEL + queue.getName());
		}

		return isWobIncoda;
	}

	/**
	 * Cerca i workflow relativi alle assegnazioni per visto che insistono sul
	 * documento in input, diversi dal workflow in input
	 * 
	 * @param idDocumento                 documento di riferimento
	 * @param f_WorkflowNumber            workflow da escludere
	 * @param codiceAoo                   aoo di riferimento
	 * @param escludiGiroVistiIspettorato se true, esclude fra i risultati il "Giro
	 *                                    Visti Ispettorato"
	 * @return
	 */
	public List<VWWorkObject> getWorkflowsVisti(final String idDocumento, final String fWorkflowNumber, final String codiceAoo, final boolean escludiGiroVistiIspettorato) {
		return getWorkflowsFromRoster(codiceAoo, idDocumento, fWorkflowNumber, escludiGiroVistiIspettorato,
				Arrays.asList(TipoAssegnazioneEnum.VISTO), null);
	}

	/**
	 * Cerca i workflow escludendo le assegnazioni per visto e contributo che
	 * insistono sul documento in input, diversi dal workflow in input
	 * 
	 * @param idDocumento      documento di riferimento
	 * @param f_WorkflowNumber workflow da escludere
	 * @param codiceAoo        aoo di riferimento
	 * @return
	 */
	public List<VWWorkObject> getWorkflowsNonVistiNonContributi(final String idDocumento, final String fWorkflowNumber, final String codiceAoo) {
		return getWorkflowsFromRoster(codiceAoo, idDocumento, fWorkflowNumber, false, null,
				Arrays.asList(TipoAssegnazioneEnum.VISTO, TipoAssegnazioneEnum.CONTRIBUTO));
	}

	/**
	 * Cerca i workflow relativi agli input
	 * 
	 * @param codiceAoo                   obbligatorio
	 * @param idDocumento                 obbligatorio
	 * @param workflowNumberToExclude     se diverso da null, esclude il workflow
	 *                                    dai risultati
	 * @param escludiGiroVistiIspettorato
	 * @param tipoAssegnazioniIncluse
	 * @param tipoAssegnazioniEscluse
	 * @return
	 */
	private List<VWWorkObject> getWorkflowsFromRoster(final String codiceAoo, final String idDocumento, final String workflowNumberToExclude,
			final boolean escludiGiroVistiIspettorato, final List<TipoAssegnazioneEnum> tipoAssegnazioniIncluse, final List<TipoAssegnazioneEnum> tipoAssegnazioniEscluse) {
		final List<VWWorkObject> wos = new ArrayList<>();

		final StringBuilder queryFilter = new StringBuilder("");
		queryFilter.append(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY) + " = " + idDocumento);
		queryFilter.append(AND + pp.getParameterByKey(PropertiesNameEnum.ID_CLIENT_METAKEY) + " = '"
				+ pp.getParameterByString(codiceAoo + "." + PropertiesNameEnum.WS_IDCLIENT.getKey()) + "'");

		if (workflowNumberToExclude != null) {
			queryFilter.append(AND + PEProperty.IW_WORKFLOW_NUMBER + " <> '" + workflowNumberToExclude + "'");
		}

		if (escludiGiroVistiIspettorato) {
			queryFilter.append(AND + PEProperty.F_SUBJECT + " <> '" + PEProperty.GIRO_VISTI_ISPETTORATO + "'");
		}

		if (tipoAssegnazioniIncluse != null) {
			for (final TipoAssegnazioneEnum ta : tipoAssegnazioniIncluse) {
				queryFilter.append(AND + pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY) + " = " + ta.getId());
			}
		}

		if (tipoAssegnazioniEscluse != null) {
			for (final TipoAssegnazioneEnum ta : tipoAssegnazioniEscluse) {
				queryFilter.append(AND + pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY) + " <> " + ta.getId());
			}
		}

		final VWRosterQuery query = getWorkFlowFromRoster(queryFilter.toString(), null, true);
		while (query.hasNext()) {
			wos.add((VWWorkObject) query.next());
		}

		return wos;
	}

	/**
	 * Gestisce la chiusura del workflow con concorrente aggiornamento dello
	 * storico.
	 * 
	 * @param wob
	 * @param eventTypeExp
	 */
	public void chiudiWFConStorico(final VWWorkObject wob, final String eventTypeExp) {
		try {
			final Map<String, Object> map = getWobFields(wob);

			chiudiWF(wob);

			createStoricoWF(map, eventTypeExp);
		} catch (final FilenetException e) {
			throw e;
		} catch (final Exception e) {
			LOGGER.error(ERROR_CHIUSURA_WORKFLOW_MSG + wob.getWorkflowNumber(), e);
			throw new FilenetException(e);
		}
	}

	/**
	 * Chiude il workflow.
	 * 
	 * @param wob
	 */
	public void chiudiWF(final VWWorkObject wob) {
		try {
			LOGGER.info("Terminating WF " + wob.getWorkflowNumber());
			wob.doLock(true);
			wob.doTerminate();
		} catch (final Exception e) {
			LOGGER.error(ERROR_CHIUSURA_WORKFLOW_MSG + wob.getWorkflowNumber(), e);
			throw new FilenetException(e);
		}
	}

	/**
	 * Chiude il workflow identificato dal wobnumber.
	 * 
	 * @param wobNumber
	 */
	public void chiudiWF(final String wobNumber) {
		try {
			LOGGER.info("Terminating WF " + wobNumber);
			final VWWorkObject wob = getWorkFlowByWob(wobNumber, false);
			wob.doLock(true);
			wob.doTerminate();
		} catch (final Exception e) {
			LOGGER.error(ERROR_CHIUSURA_WORKFLOW_MSG + wobNumber, e);
			throw new FilenetException(e);
		}
	}

	private Map<String, Object> getWobFields(final VWWorkObject workObject) {
		final Map<String, Object> fieldMap = new HashMap<>((int) (workObject.getFieldNames().length / .75));

		for (final String fieldName : workObject.getFieldNames()) {
			Object wobvalue = workObject.getFieldValue(fieldName);
			if (wobvalue instanceof Date) {
				final Date d = (Date) wobvalue;
				if (d.getTime() <= 0) {
					wobvalue = null;
				}
			}
			fieldMap.put(fieldName, wobvalue);
		}
		return fieldMap;
	}

	/**
	 * Crea lo storico del workflow.
	 * 
	 * @param map
	 * @param eventTypeExp
	 */
	public void createStoricoWF(final Map<String, Object> map, final String eventTypeExp) {
		try {
			LOGGER.info("Launching WF " + pp.getParameterByKey(PropertiesNameEnum.WF_PROCESSO_CHIUSURA_STORICO_WFKEY));
			final VWStepElement step = session.createWorkflow(pp.getParameterByKey(PropertiesNameEnum.WF_PROCESSO_CHIUSURA_STORICO_WFKEY));
			final Map<String, Object> definedParams = getSettableParameters(step);
			final Map<String, Object> instanceParameters = new HashMap<>(definedParams.size());
			for (final String key : definedParams.keySet()) {
				if (map.containsKey(key)) {
					instanceParameters.put(key, map.get(key));
				}
			}

			instanceParameters.put(pp.getParameterByKey(PropertiesNameEnum.EVENT_TYPE_EXP_WF_METAKEY), eventTypeExp);

			setStepParameters(step, instanceParameters);
			step.doDispatch();

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di avvio del workflow " + pp.getParameterByKey(PropertiesNameEnum.WF_PROCESSO_CHIUSURA_STORICO_WFKEY), e);
			throw new FilenetException(e);
		}
	}

	private Map<String, Object> getSettableParameters(final VWStepElement step) {
		final Map<String, Object> parameters = new HashMap<>(step.getParameterNames().length);
		try {

			if (step.getStepResponses() != null) {
				final Collection<String> responses = new ArrayList<>(step.getStepResponses().length);
				responses.addAll(Arrays.asList(step.getStepResponses()));
				parameters.put(PEProperty.F_RESPONSES, responses);
			}

			if (step.getSelectedResponse() != null) {
				parameters.put(PEProperty.F_RESPONSE, step.getSelectedResponse());
			} else {
				parameters.put(PEProperty.F_RESPONSE, "");
			}

			final VWParameter[] vwparameters = getVWParameters(step);

			for (final VWParameter param : vwparameters) {
				if (param.getMode() != VWModeType.MODE_TYPE_IN) {

					if (param.getName().equals(PEProperty.F_RESPONSES) || param.getName().equals(PEProperty.F_SUBJECT)) {
						continue;
					}

					if (param.isArray()) {
						parameters.put(param.getName(), Collections.addAll(new ArrayList<Object>(), param.getValue()));
					}

					parameters.put(param.getName(), param.getValue());
				}
			}

		} catch (final VWException e) {
			LOGGER.error(ERROR_RECUPERO_PARAMETRI_WORKFLOW_MSG + step.getWorkflowName(), e);
			throw new FilenetException(e);
		}
		return parameters;
	}

	/**
	 * @param step
	 * @param vwparameters
	 * @return
	 */
	private VWParameter[] getVWParameters(final VWStepElement step) {
		VWParameter[] vwparameters = new VWParameter[0];
		try {
			vwparameters = step.getParameters(VWFieldType.ALL_FIELD_TYPES & (~VWFieldType.FIELD_TYPE_ATTACHMENT), VWStepElement.FIELD_USER_AND_SYSTEM_DEFINED);
		} catch (final VWException e) {
			LOGGER.error(ERROR_RECUPERO_PARAMETRI_WORKFLOW_MSG + step.getWorkflowName(), e);
		}
		return vwparameters;
	}

	private void setStepParameters(final VWStepElement aStep, final Map<String, Object> parameters) {
		try {
			for (final Entry<String, Object> entry : parameters.entrySet()) {
				
				final String key = entry.getKey();
				final Object value = entry.getValue();
				
				final VWParameter parameter = aStep.getParameter(key);

				if (!"".equals(value) && value != null && parameter != null) {
					if (key.equals(PEProperty.F_RESPONSE)) {
						aStep.setSelectedResponse((String) value);
					} else {
						Object vwValue = null;
						vwValue = getParameterObject(value, parameter);
						aStep.setParameterValue(key, vwValue, false);
					}
				}
			}
		} catch (final VWException e) {
			LOGGER.error(ERROR_RECUPERO_PARAMETRI_WORKFLOW_MSG + aStep.getWorkflowName(), e);
			throw new FilenetException(e);
		}
	}

	private Object getParameterObject(final Object value, final VWParameter parameter) {

		switch (parameter.getFieldType()) {
		case VWFieldType.FIELD_TYPE_STRING:
			if (value == null) {
				return "";
			}
			return value;
		case VWFieldType.FIELD_TYPE_TIME:
			return value;
		case VWFieldType.FIELD_TYPE_FLOAT:
			if (value == null) {
				return 0;
			}
			return (value instanceof String) ? Float.valueOf((String) value) : value;
		case VWFieldType.FIELD_TYPE_BOOLEAN:
			if (value == null) {
				return false;
			}
			return (value instanceof String) ? Boolean.valueOf((String) value) : value;
		case VWFieldType.FIELD_TYPE_INT:
			if (value == null) {
				return 0;
			}
			return (value instanceof String) ? Integer.valueOf((String) value) : value;
		default:
			throw new FilenetException("Field type non gestito: " + parameter.getName());
		}
	}

	/**
	 * Converte in allegato workflow il document in ingresso.
	 * 
	 * @param docInputFn
	 * @return VWAttachment
	 */
	public VWAttachment convertiInAllegatoWorkflow(final Document docInputFn) {
		final VWAttachment allegatoWorkflow = new VWAttachment();
		final ObjectStore objectStore = docInputFn.getObjectStore();
		objectStore.refresh();

		allegatoWorkflow.setId(docInputFn.get_Id().toString());
		allegatoWorkflow.setVersion(docInputFn.get_VersionSeries().get_Id().toString());
		allegatoWorkflow.setLibraryName(objectStore.get_SymbolicName());
		allegatoWorkflow.setLibraryType(VWLibraryType.LIBRARY_TYPE_CONTENT_ENGINE);
		allegatoWorkflow.setType(VWAttachmentType.ATTACHMENT_TYPE_DOCUMENT);

		return allegatoWorkflow;
	}

	/**
	 * @param queueName
	 * @param indexName
	 * @param filter
	 * @return workFlowDTO come collection di PEDocumentoDTO
	 */
	public Collection<PEDocumentoDTO> getWorkflowByFilter(final String queueName, final String indexName, final String filter) {
		final int queryFlagsNoOptions = VWRoster.QUERY_NO_OPTIONS;
		final int queryTypeWorkobjects = VWFetchType.FETCH_TYPE_WORKOBJECT;
		Collection<PEDocumentoDTO> workFlowDTO = null;
		try {
			if ("NSD_Roster".equalsIgnoreCase(queueName)) {
				final VWRoster roster = session.getRoster(queueName);
				final VWRosterQuery queryRoster = roster.createQuery(indexName, null, null, queryFlagsNoOptions, filter, null, queryTypeWorkobjects);
				workFlowDTO = TrasformPE.transform(queryRoster, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO);
			} else {
				final VWQueue queue = session.getQueue(queueName);
				final VWQueueQuery query = queue.createQuery(indexName, null, null, queryFlagsNoOptions, filter, null, queryTypeWorkobjects);
				workFlowDTO = TrasformPE.transform(query, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO, null);
			}
			return workFlowDTO;
		} catch (final Exception e) {
			LOGGER.error("Errore durante la ricerca dei workflow attivi per Filter: " + filter, e);
			throw new RedException(" Errore durante la ricerca dei workflow attivi per la coda", e);
		}

	}

	/**
	 * Restituisce la VWQueueQuery associata alla queue.
	 * 
	 * @param queueName
	 * @param indexName
	 * @param filter
	 * @return VWQueueQuery
	 */
	public Object getQueueQuery(final String queueName, final String indexName, final String filter) {
		final int queryFlagsNoOptions = VWRoster.QUERY_NO_OPTIONS;
		final int queryTypeWorkobjects = VWFetchType.FETCH_TYPE_WORKOBJECT;
		if ("NSD_Roster".equalsIgnoreCase(queueName)) {
			final VWRoster roster = session.getRoster(queueName);
			return roster.createQuery(indexName, null, null, queryFlagsNoOptions, filter, null, queryTypeWorkobjects);
		} else {
			final VWQueue queue = session.getQueue(queueName);
			return queue.createQuery(indexName, null, null, queryFlagsNoOptions, filter, null, queryTypeWorkobjects);
		}
	}

	/**
	 * @param idFascicoloRaccoltaProvvisoria
	 */
	public void terminaAttoDecreto(final String idFascicoloRaccoltaProvvisoria) {
		try {
			final VWQueue codaSospeso = session.getQueue(pp.getParameterByKey(PropertiesNameEnum.STEP_IN_SOSPESO_MAPPING_WFKEY).split("\\|")[0]);
			final VWQueue codaDaLavorare = session.getQueue(DocumentQueueEnum.DA_LAVORARE.getName());
			final String queryFilter = "idRaccoltaProvvisoria = '" + idFascicoloRaccoltaProvvisoria + "'";
			VWQueueQuery query = codaSospeso.createQuery("raccolta_prov_idx", null, null, VWRoster.QUERY_NO_OPTIONS, queryFilter, null, VWFetchType.FETCH_TYPE_WORKOBJECT);
			if (!query.hasNext()) {
				throw new RedException(idFascicoloRaccoltaProvvisoria + " non trovato. Impossibile instradare il workflow con la response "
						+ ResponsesRedEnum.SPOSTA_IN_LAVORAZIONE.getResponse());
			}

			VWWorkObject wob = (VWWorkObject) query.next();
			wob.doLock(true);
			wob.setSelectedResponse(ResponsesRedEnum.SPOSTA_IN_LAVORAZIONE.getResponse());
			wob.doDispatch();

			query = codaDaLavorare.createQuery("raccolta_prov_idx", null, null, VWRoster.QUERY_NO_OPTIONS, queryFilter, null, VWFetchType.FETCH_TYPE_WORKOBJECT);
			if (!query.hasNext()) {
				throw new RedException(
						idFascicoloRaccoltaProvvisoria + " non trovato. Impossibile instradare il workflow con la response " + ResponsesRedEnum.ATTI.getResponse());
			}

			wob = (VWWorkObject) query.next();
			wob.doLock(true);
			wob.setSelectedResponse(ResponsesRedEnum.ATTI.getResponse());
			wob.doDispatch();

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}
	}

	/**
	 * @param wobNumber
	 * @param nomeQueue
	 * @param indexName
	 * @param idUtenteDestinatari
	 * @param idClientAoo
	 * @param idsTipoAssegnazione
	 * @param flagRenderizzato
	 * @param registroRiservato
	 * @param documentTitles
	 * @param dataScadenza
	 * @param dataScadenzaFrom
	 * @param dataScadenzaTo
	 * @param flagInScadenza
	 * @param listUfficio
	 * @param inApprovazioneDirigente
	 * @return VWQueueQuery
	 */
	public VWQueueQuery getWorkFlowsByWobQueueFilterRedWidget(final String wobNumber, final String nomeQueue, final String indexName, final List<Long> idUtenteDestinatari,
			final String idClientAoo, final List<Long> idsTipoAssegnazione, final Integer flagRenderizzato, final Boolean registroRiservato,
			final Collection<String> documentTitles, final String dataScadenza, final Date dataScadenzaFrom, final Date dataScadenzaTo, final boolean flagInScadenza,
			final List<Nodo> listUfficio, final boolean inApprovazioneDirigente) {

		final FilenetPEQueryBuilder fqb = new FilenetPEQueryBuilder(session, nomeQueue);
		if (wobNumber != null) {
			fqb.and(Constants.PEProperty.F_WOB_NUM, wobNumber.trim());
		}

		if (DocumentQueueEnum.SOSPESO.getName().equals(nomeQueue) || DocumentQueueEnum.IN_ACQUISIZIONE.getName().equals(nomeQueue)) {
			final List<Long> nodoUfficio = new ArrayList<>();

			fqb.in(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), nodoUfficio);

			if (idUtenteDestinatari != null && !idUtenteDestinatari.isEmpty()) {
				if (idUtenteDestinatari.size() == 1) {
					fqb.and(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), idUtenteDestinatari.get(0).intValue());
				} else {
					final List<Proposition> propositions = new ArrayList<>();
					for (final Long idUtenteDestinatario : idUtenteDestinatari) {
						propositions.add(fqb.createProposition(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), idUtenteDestinatario.intValue()));
					}
					final Proposition[] longPropositions = propositions.toArray(new Proposition[propositions.size()]);
					fqb.or(longPropositions);
				}
			}
		} else {
			final List<Long> nodoUfficio = new ArrayList<>();

			if (listUfficio != null) {
				for (final Nodo nodo : listUfficio) {
					nodoUfficio.add(nodo.getIdNodo());
				}
			}
			fqb.in(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), nodoUfficio);
		}

		if (inApprovazioneDirigente && idUtenteDestinatari != null && !idUtenteDestinatari.isEmpty()) {
			if (idUtenteDestinatari.size() == 1) {
				fqb.and(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), idUtenteDestinatari.get(0).intValue());
			} else {
				final List<Proposition> propositions = new ArrayList<>();
				for (final Long idUtenteDestinatario : idUtenteDestinatari) {
					propositions.add(fqb.createProposition(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), idUtenteDestinatario.intValue()));
				}
				final Proposition[] longPropositions = propositions.toArray(new Proposition[propositions.size()]);
				fqb.or(longPropositions);
			}
		}
		if (idClientAoo != null) {
			fqb.and(pp.getParameterByKey(PropertiesNameEnum.ID_CLIENT_METAKEY), idClientAoo);
		}
		if (idsTipoAssegnazione != null && !idsTipoAssegnazione.isEmpty()) {
			if (idsTipoAssegnazione.size() > 1) {
				final Long[] idst = idsTipoAssegnazione.toArray(new Long[0]);
				fqb.in(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), idst);
			} else {
				fqb.and(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), idsTipoAssegnazione.get(0).intValue());
			}
		}
		if (flagRenderizzato != null) {
			fqb.and(pp.getParameterByKey(PropertiesNameEnum.FLAG_RENDERIZZATO_METAKEY), flagRenderizzato);
		}
		if (registroRiservato != null) {
			fqb.and(pp.getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY), registroRiservato);
		}

		if (!CollectionUtils.isEmpty(documentTitles)) {
			fqb.in(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY), documentTitles);
		}

		if (dataScadenza != null) {
			fqb.and(pp.getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY), dataScadenza);
		}

		if (flagInScadenza) {
			fqb.andNotNull(pp.getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY));
		}

		if (dataScadenzaFrom != null) {
			// Se la data è uguale a NULL_DATE, la data scadenza deve essere strettamente
			// maggiore
			if (DateUtils.NULL_DATE.equals(LocalDateTime.ofInstant(dataScadenzaFrom.toInstant(), ZoneId.systemDefault()))) {
				fqb.gt(pp.getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY), dataScadenzaFrom);
			} else {
				fqb.ge(pp.getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY), dataScadenzaFrom);
			}
		}

		if (dataScadenzaTo != null) {
			fqb.le(pp.getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY), dataScadenzaTo);
		}

		if (indexName != null) {
			return fqb.createQuery(true, indexName);
		}

		return fqb.createQuery(true);
	}

	/**
	 * Recupera e restituisce gli id decreti da lavorare.
	 * 
	 * @return gli id decreti da lavorare
	 */
	public List<String> fetchIdDecretiDaLavorare() {
		final String queryWfDecreti = "idClient = 'NSD' AND ((F_Class = 'SIGI-RED- Decreto_Dirigenziale_FEP' AND (F_StepName = 'Attesa Associazione DSR' OR F_StepName = 'DSR Associato')) OR (F_Class = 'SIGI-RED- Decreto_Dirigenziale_FEP_IVA' AND F_StepName = 'Decreto IVA'))";
		final VWQueue q = session.getQueue(DocumentQueueEnum.DD_DA_LAVORARE.getName());
		final VWQueueQuery vwq = q.createQuery(null, null, null, VWQueue.QUERY_READ_LOCKED | VWQueue.QUERY_READ_BOUND, queryWfDecreti, null,
				VWFetchType.FETCH_TYPE_WORKOBJECT);

		final List<String> ids = new ArrayList<>(vwq.fetchCount());
		while (vwq.hasNext()) {
			final VWWorkObject r = (VWWorkObject) vwq.next();
			ids.add(((Integer) r.getFieldValue(ID_DOCUMENTO)).toString());
		}
		return ids;
	}

	/**
	 * 
	 * 
	 * @param workflowName
	 * @param propertyName
	 * @return valore di default assegnato a questa property al momento dell'avvio
	 *         di un Workflow
	 */
	public String getValueByKey(final String workflowName, final String propertyName) {
		String output = null;

		try {

			final VWStepElement launchStep = session.createWorkflow(workflowName);
			final VWParameter[] parameters = launchStep.getParameters(VWFieldType.ALL_FIELD_TYPES, VWStepElement.FIELD_USER_AND_SYSTEM_DEFINED);

			for (final VWParameter param : parameters) {
				final String name = param.getName();
				final String value = param.getStringValue();

				if (propertyName.equals(name)) {
					output = value;
					break;
				}
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero della property [ " + propertyName + " ] del WORKFLOW con nome[" + workflowName + "]", e);
			throw new FilenetException("Errore durante il recupero della property [ " + propertyName + " ] del WORKFLOW con nome[" + workflowName + "]", e);
		}

		return output;
	}

	/**
	 * Avvia un'istanza di un workflow.
	 * 
	 * @param name
	 * @param subject
	 * @param comment
	 * @param metadati
	 * @param response
	 * @return workflownumber del workflow avviato
	 */
	public String avviaIstanzaWorkflow(final String name, final String subject, final String comment, final Map<String, Object> metadati, final String response) {
		String workflowNumber = null;

		try {

			final VWStepElement launchStep = session.createWorkflow(name);
			launchStep.setParameterValue(PEProperty.F_SUBJECT, subject, false);
			launchStep.setComment(comment);

			// Set della response iniziale.
			final String[] responses = launchStep.getStepResponses();
			if (responses != null && response != null) {
				launchStep.setSelectedResponse(response);
			}

			// Set dei metadati nella nuova istanza.
			for (final Entry<String, Object> metadato : metadati.entrySet()) {
				if ((metadato.getValue() != null) /* && (alNames.contains(metadato.getKey())) */) {
					Object obj = metadato.getValue();

					if (obj instanceof Collection) {
						obj = ((Collection<?>) obj).toArray();
					} else if (obj instanceof Long) {
						obj = ((Long) obj).intValue();
					}

					final String m = metadato.getKey();
					if (launchStep.getParameter(m) != null) {
						launchStep.setParameterValue(metadato.getKey(), obj, false);
					}

				}
			}

			// Salvataggio delle modifiche.
			launchStep.doDispatch();
			workflowNumber = launchStep.getWorkflowNumber();

		} catch (final Exception e) {
			LOGGER.error(
					"Errore durante l'avvio di un workflow con name [ " + name + " ], subject [ " + subject + " ], comment [ " + comment + " ], response [ " + response + " ]",
					e);
			throw new FilenetException(
					"Errore durante l'avvio di un workflow con name [ " + name + " ], subject [ " + subject + " ], comment [ " + comment + " ], response [ " + response + " ]",
					e);
		}

		return workflowNumber;
	}
	
	/**
	 * @param clientAoo
	 * @param dataCreazione
	 * @return List di id
	 */
	public List<String> fetchIdDocSpedizioneSpeditoJob(final String clientAoo,final Date dataCreazione) {
		VWQueueQuery vwq = getWorkFlowsByWobQueueFilterRedJob(DocumentQueueEnum.SPEDIZIONE, DocumentQueueEnum.SPEDIZIONE.getIndexName(), clientAoo, false);
		List<String> ids = new ArrayList<>(vwq.fetchCount());
		while (vwq.hasNext()) {
			VWWorkObject r = (VWWorkObject) vwq.next();
			String dataAssegnazione = r.getFieldValue("dataAssegnazioneStr").toString();
			if(dataAssegnazione!=null && dataCreazione!=null) {
				try {
					Date dataAssegnazioneDate=new SimpleDateFormat("dd/MM/yyyy").parse(dataAssegnazione);
					if(dataAssegnazioneDate.before(dataCreazione)) {
						ids.add(((Integer) r.getFieldValue(ID_DOCUMENTO)).toString());
					}
				} catch (ParseException e) {
					LOGGER.error("Errore durante la conversione delle date per il batch di notifica spedizione cartacea", e);
				} 
				
			}else {
				ids.add(((Integer) r.getFieldValue(ID_DOCUMENTO)).toString());
			}
			
		}
		return ids;
	}
	
	
	private VWQueueQuery getWorkFlowsByWobQueueFilterRedJob(final DocumentQueueEnum queue, final String indexName,  
			final String idClientAoo,final Boolean registroRiservato) {

		String nomeQueue = queue.getName();
		
		FilenetPEQueryBuilder fqb = new FilenetPEQueryBuilder(session, nomeQueue);
  
 
		if (idClientAoo != null) {
			fqb.and(pp.getParameterByKey(PropertiesNameEnum.ID_CLIENT_METAKEY), idClientAoo);
		}
	
		if (registroRiservato != null) {
			fqb.and(pp.getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY), registroRiservato);
		}

		//Filtro per prendere i documenti spediti cartacei/misti
		fqb.and(" idTipoSpedizione", 1);
		
		return fqb.createQuery(true, indexName);

	}
}