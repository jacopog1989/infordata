package it.ibm.red.business.persistence.model;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import it.ibm.red.business.dto.NotificaEmailDTO;
import it.ibm.red.business.utils.LogStyleNotNull;

/**
 * The Class CodaEmail.
 *
 * @author a.dilegge
 * 
 * 	model per la notifica via mail.
 */
public class CodaEmail extends NotificaEmailDTO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -7763735214591856721L;
	
    /**
     * Data richiesta spedizione.
     */
	private Date dataRichiestaSpedizione;
	
    /**
     * Data spedizione.
     */
	private Date dataSpedizione;
	
    /**
     * Flag to.
     */
	private Integer flagTO;
	
    /**
     * Flag cc.
     */
	private Integer flagCC;
	
    /**
     * Dimensione messaggio.
     */
	private Integer sizeMsg;
	
    /**
     * Identificativo spedizione unica.
     */
	private Integer idSpedizioneUnica;
	
    /**
     * Identificativo spedizione nps.
     */
	private Integer idSpedizioneNps;

	/**
	 * Costruttore di classe.
	 * @param notificaEmail
	 * @param messaggioEmailBytes
	 * @param flagToCc
	 */
	public CodaEmail(final NotificaEmailDTO notificaEmail, final byte[] messaggioEmailBytes, final Integer flagToCc) {
		super(notificaEmail.getIdNotifica(), notificaEmail.getIdDocumento(), notificaEmail.getIdMessaggio(), notificaEmail.getEmailMittente(), 
				notificaEmail.getTipoMittente(), notificaEmail.getEmailDestinatario(), notificaEmail.getTipoDestinatario(),
				notificaEmail.getIdDestinatario(), notificaEmail.getStatoRicevuta(), notificaEmail.getIdAoo(), 
				notificaEmail.getCountRetry(), notificaEmail.getMaxRetry(), notificaEmail.getSpedizione(), messaggioEmailBytes, 
				notificaEmail.getTipoEvento());
		
		setDataRichiestaSpedizione(null);
		setDataSpedizione(null);
		setFlagCC(flagToCc == 2 ? 1 : 0);
		setFlagTO(flagToCc == 1 ? 1 : 0);
		setIdSpedizioneUnica(null);
		setSizeMsg(null);
		
	}

	/**
	 * Costruttore completo.
	 * @param idNotifica
	 * @param idDocumento
	 * @param idMessaggio
	 * @param emailMittente
	 * @param tipoMittente
	 * @param emailDestinatario
	 * @param tipoDestinatario
	 * @param idDestinatario
	 * @param statoRicevuta
	 * @param idAoo
	 * @param countRetry
	 * @param maxRetry
	 * @param spedizione
	 * @param messaggio
	 * @param tipoEvento
	 * @param dataRichiestaSpedizione
	 * @param dataSpedizione
	 * @param flagCC
	 * @param flagTo
	 * @param idSpedizioneUnica
	 * @param idSpedizioneNps
	 */
	public CodaEmail(final Integer idNotifica, final String idDocumento, final String idMessaggio, final String emailMittente, final Integer tipoMittente, 
			final String emailDestinatario, final Integer tipoDestinatario, final Long idDestinatario, final Integer statoRicevuta, final Integer idAoo, 
			final Integer countRetry, final Integer maxRetry, final Integer spedizione, final byte[] messaggio, final Integer tipoEvento, 
			final Date dataRichiestaSpedizione, final Date dataSpedizione, final Integer flagCC, final Integer flagTo,  final Integer idSpedizioneUnica,
			final Integer idSpedizioneNps) {
		super(idNotifica, idDocumento, idMessaggio, emailMittente, tipoMittente, emailDestinatario, tipoDestinatario, idDestinatario, 
				statoRicevuta, idAoo, countRetry, maxRetry, spedizione,  messaggio, tipoEvento);
		
		setDataRichiestaSpedizione(dataRichiestaSpedizione);
		setDataSpedizione(dataSpedizione);
		setFlagCC(flagCC);
		setFlagTO(flagTo);
		setIdSpedizioneUnica(idSpedizioneUnica);
		setSizeMsg(messaggio != null ? messaggio.length : 0);
		setIdSpedizioneNps(idSpedizioneNps);
	}

	/**
	 * @see it.ibm.red.business.dto.NotificaEmailDTO#getDataRichiestaSpedizione().
	 */
	@Override
	public Date getDataRichiestaSpedizione() {
		return dataRichiestaSpedizione;
	}

	/**
	 * @see it.ibm.red.business.dto.NotificaEmailDTO#setDataRichiestaSpedizione(java.util.Date).
	 */
	@Override
	public void setDataRichiestaSpedizione(final Date dataRichiestaSpedizione) {
		this.dataRichiestaSpedizione = dataRichiestaSpedizione;
	}

	/**
	 * Restituisce la data di spedizione.
	 * 
	 * @return dataSpedizione
	 */
	public Date getDataSpedizione() {
		return dataSpedizione;
	}

	/**
	 * Imposta la data di spedizione.
	 * 
	 * @param dataSpedizione
	 */
	public void setDataSpedizione(final Date dataSpedizione) {
		this.dataSpedizione = dataSpedizione;
	}

	/**
	 * Restituisce il flag TO che fa riferiferimento al tipo destinatario: TO o CC.
	 * @return flagTO
	 */
	public Integer getFlagTO() {
		return flagTO;
	}

	/**
	 * Imposta il flag TO.
	 * @param flagTO
	 */
	public void setFlagTO(final Integer flagTO) {
		this.flagTO = flagTO;
	}

	/**
	 * Restituisce il flag CC che fa riferiferimento al tipo destinatario: TO o CC.
	 * @return flagTO
	 */
	public Integer getFlagCC() {
		return flagCC;
	}

	/**
	 * Imposta il flag CC.
	 * @param flagCC
	 */
	public void setFlagCC(final Integer flagCC) {
		this.flagCC = flagCC;
	}

	/**
	 * Restituisce la dimensione del messaggio.
	 * @return
	 */
	public Integer getSizeMsg() {
		return sizeMsg;
	}

	/**
	 * Imposta la dimensione del messaggio.
	 * @param sizeMsg
	 */
	public void setSizeMsg(final Integer sizeMsg) {
		this.sizeMsg = sizeMsg;
	}

	/**
	 * Restituisce l'id della spedizione unica.
	 * @return idSpedizioneUnica
	 */
	public Integer getIdSpedizioneUnica() {
		return idSpedizioneUnica;
	}

	/**
	 * Imposta l'id di spedizione unica.
	 * @param idSpedizioneUnica
	 */
	public void setIdSpedizioneUnica(final Integer idSpedizioneUnica) {
		this.idSpedizioneUnica = idSpedizioneUnica;
	}

	/**
	 * Restituisce l'id di spedizione di NPS.
	 * 
	 * @return idSpedizioneNps
	 */
	public Integer getIdSpedizioneNps() {
		return idSpedizioneNps;
	}

	/**
	 * Imposta l'id di spedizione di NPS.
	 * 
	 * @param idSpedizioneNps
	 */
	public void setIdSpedizioneNps(final Integer idSpedizioneNps) {
		this.idSpedizioneNps = idSpedizioneNps;
	}

	/**
	 * @see it.ibm.red.business.dto.NotificaEmailDTO#toString().
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, new LogStyleNotNull()); 
	}

}