package it.ibm.red.business.dto;

import java.util.Calendar;
import java.util.Date;

import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;


/**
 * DTO per la ricerca avanzata dei fascicoli.
 * 
 * @author m.crescentini
 *
 */
public class ParamsRicercaAvanzataFascDTO extends AbstractDTO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6661272052583785032L;

	/**
	 * Anno fascicolo 
	 */
	private Integer annoFascicolo;
	
	/**
	 * Flag anno fascicolo disabilitato 
	 */
	private boolean annoFascicoloDisabled;
	
	/**
	 * Numero fascicolo 
	 */
	private Integer numeroFascicolo;
	
	/**
	 * Descrizione fascicolo 
	 */
	private String descrizioneFascicolo;
	
	/**
	 * Flag Faldonato 
	 */
	private Boolean faldonato;
	
	/**
	 * Tipo assegnazione 
	 */
	private TipoAssegnazioneEnum tipoAssegnazione;
	
	/**
	 * Assegnatario 
	 */
	private AssegnatarioOrganigrammaDTO assegnatario;
	
	/**
	 * Stato fascicolo 
	 */
	private FilenetStatoFascicoloEnum statoFascicolo;
	
	/**
	 * Titolario 
	 */
	private String titolario;
	
	/**
	 * Data creazione da 
	 */
	private Date dataCreazioneDa;
	
	/**
	 * Data creazione a 
	 */
	private Date dataCreazioneA;
	
	/**
	 * Data chiusura Da 
	 */
	private Date dataChiusuraDa;
	
	/**
	 * Data chiusura A 
	 */
	private Date dataChiusuraA;
	
	/**
	 * Descrizione tipologia documento 
	 */
	private String descrizioneTipologiaDocumento;
	
	/**
	 * Descrizione tipo procedimento 
	 */
	private String descrizioneTipoProcedimento;
	
	/**
	 * Costruttore params ricerca avanzata fasc DTO.
	 */
	public ParamsRicercaAvanzataFascDTO() {
		this.annoFascicolo = Calendar.getInstance().get(Calendar.YEAR);
	}
	
	
	/** 
	 *
	 * @return the annoFascicolo
	 */
	public Integer getAnnoFascicolo() {
		return annoFascicolo;
	}
	
	/** 
	 *
	 * @param annoFascicolo the annoFascicolo to set
	 */
	public void setAnnoFascicolo(final Integer annoFascicolo) {
		this.annoFascicolo = annoFascicolo;
	}
	
	/** 
	 *
	 * @return the numeroFascicolo
	 */
	public Integer getNumeroFascicolo() {
		return numeroFascicolo;
	}
	
	/** 
	 *
	 * @param numeroFascicolo the new numero fascicolo
	 */
	public void setNumeroFascicolo(final Integer numeroFascicolo) {
		this.numeroFascicolo = numeroFascicolo;
	}
	
	/** 
	 *
	 * @return the descrizioneFascicolo
	 */
	public String getDescrizioneFascicolo() {
		return descrizioneFascicolo;
	}
	
	/** 
	 *
	 * @param descrizioneFascicolo the new descrizione fascicolo
	 */
	public void setDescrizioneFascicolo(final String descrizioneFascicolo) {
		this.descrizioneFascicolo = descrizioneFascicolo;
	}
	
	/** 
	 *
	 * @return the faldonato
	 */
	public Boolean getFaldonato() {
		return faldonato;
	}
	
	/** 
	 *
	 * @param faldonato the faldonato to set
	 */
	public void setFaldonato(final Boolean faldonato) {
		this.faldonato = faldonato;
	}
	
	/** 
	 *
	 * @return the tipoAssegnazione
	 */
	public TipoAssegnazioneEnum getTipoAssegnazione() {
		return tipoAssegnazione;
	}
	
	/** 
	 *
	 * @param tipoAssegnazione the tipoAssegnazione to set
	 */
	public void setTipoAssegnazione(final TipoAssegnazioneEnum tipoAssegnazione) {
		this.tipoAssegnazione = tipoAssegnazione;
	}
	
	/** 
	 *
	 * @return the assegnatario
	 */
	public AssegnatarioOrganigrammaDTO getAssegnatario() {
		return assegnatario;
	}
	
	/** 
	 *
	 * @param assegnatario the assegnatario to set
	 */
	public void setAssegnatario(final AssegnatarioOrganigrammaDTO assegnatario) {
		this.assegnatario = assegnatario;
	}
	
	/** 
	 *
	 * @return the statoFascicolo
	 */
	public FilenetStatoFascicoloEnum getStatoFascicolo() {
		return statoFascicolo;
	}
	
	/** 
	 *
	 * @param statoFascicolo the statoFascicolo to set
	 */
	public void setStatoFascicolo(final FilenetStatoFascicoloEnum statoFascicolo) {
		this.statoFascicolo = statoFascicolo;
	}
	
	/** 
	 *
	 * @return the titolario
	 */
	public String getTitolario() {
		return titolario;
	}
	
	/** 
	 *
	 * @param titolario the titolario to set
	 */
	public void setTitolario(final String titolario) {
		this.titolario = titolario;
	}
	
	/** 
	 *
	 * @return the dataCreazioneDa
	 */
	public Date getDataCreazioneDa() {
		return dataCreazioneDa;
	}
	
	/** 
	 *
	 * @param dataCreazioneDa the dataCreazioneDa to set
	 */
	public void setDataCreazioneDa(final Date dataCreazioneDa) {
		this.dataCreazioneDa = dataCreazioneDa;
	}
	
	/** 
	 *
	 * @return the dataCreazioneA
	 */
	public Date getDataCreazioneA() {
		return dataCreazioneA;
	}
	
	/** 
	 *
	 * @param dataCreazioneA the dataCreazioneA to set
	 */
	public void setDataCreazioneA(final Date dataCreazioneA) {
		this.dataCreazioneA = dataCreazioneA;
	}
	
	/** 
	 *
	 * @return the dataChiusuraDa
	 */
	public Date getDataChiusuraDa() {
		return dataChiusuraDa;
	}
	
	/** 
	 *
	 * @param dataChiusuraDa the dataChiusuraDa to set
	 */
	public void setDataChiusuraDa(final Date dataChiusuraDa) {
		this.dataChiusuraDa = dataChiusuraDa;
	}
	
	/** 
	 *
	 * @return the dataChiusuraA
	 */
	public Date getDataChiusuraA() {
		return dataChiusuraA;
	}
	
	/** 
	 *
	 * @param dataChiusuraA the dataChiusuraA to set
	 */
	public void setDataChiusuraA(final Date dataChiusuraA) {
		this.dataChiusuraA = dataChiusuraA;
	}
	
	/** 
	 *
	 * @return the descrizioneTipologiaDocumento
	 */
	public String getDescrizioneTipologiaDocumento() {
		return descrizioneTipologiaDocumento;
	}
	
	/** 
	 *
	 * @param descrizioneTipologiaDocumento the descrizioneTipologiaDocumento to set
	 */
	public void setDescrizioneTipologiaDocumento(final String descrizioneTipologiaDocumento) {
		this.descrizioneTipologiaDocumento = descrizioneTipologiaDocumento;
	}
	
	/** 
	 *
	 * @return the descrizioneTipoProcedimento
	 */
	public String getDescrizioneTipoProcedimento() {
		return descrizioneTipoProcedimento;
	}
	
	/** 
	 *
	 * @param descrizioneTipoProcedimento the descrizioneTipoProcedimento to set
	 */
	public void setDescrizioneTipoProcedimento(final String descrizioneTipoProcedimento) {
		this.descrizioneTipoProcedimento = descrizioneTipoProcedimento;
	}


	/** 
	 *
	 * @return true, if is anno fascicolo disabled
	 */
	public boolean isAnnoFascicoloDisabled() {
		return annoFascicoloDisabled;
	}


	/** 
	 *
	 * @param annoFascicoloDisabled the new anno fascicolo disabled
	 */
	public void setAnnoFascicoloDisabled(final boolean annoFascicoloDisabled) {
		this.annoFascicoloDisabled = annoFascicoloDisabled;
	}
}