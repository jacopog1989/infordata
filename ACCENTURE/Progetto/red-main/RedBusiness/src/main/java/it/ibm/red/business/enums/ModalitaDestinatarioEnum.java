package it.ibm.red.business.enums;

/**
 * @author Simone Lacarpia
 *
 *	TO / CC
 */
public enum ModalitaDestinatarioEnum {

	/**
	 * Valore.
	 */
	TO("A"),
	

	/**
	 * Valore.
	 */
	CC("CC");
	

	/**
	 * Label.
	 */
	private String label;
	
	
	ModalitaDestinatarioEnum(final String l) {
		this.label = l;
	}
	
	
	/**
	 * @param s
	 * @return
	 */
	public static ModalitaDestinatarioEnum getModalita(final String s) {
		ModalitaDestinatarioEnum output = null;
		
		if (TO.name().equals(s) || TO.getLabel().equals(s)) {
			output = TO;
		} else if (CC.name().equals(s) || CC.getLabel().equals(s)) {
			output = CC;
		}
		
		return output;
	}

	/**
	 * Restitusice la label associata all'enum.
	 * @return
	 */
	public String getLabel() {
		return label;
	}
}
