package it.ibm.red.business.service.asign.step.impl;

import java.sql.Connection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dao.IASignDAO;
import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignStepResultDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.asign.step.ASignStepAbstract;
import it.ibm.red.business.service.asign.step.ContextASignEnum;
import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * Step che gestisce la fase di protocollazione - firma asincrona.
 */
@Service
public class ProtocollazioneStep extends ASignStepAbstract {

	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = -7553486970039206304L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ProtocollazioneStep.class.getName());
	
	/**
	 * Servizio firma asincrona.
	 */
	@Autowired
	private IASignDAO aSignDAO;

	/**
	 * Esegue la protocollazione per un item di firma asincrona se e solo se non è
	 * stata già fatta.
	 * 
	 * @param in
	 * @param item
	 * @param context
	 * @return risultato della protocollazione se viene effettuata, un risultato
	 *         positivo se già esiste il protocollo
	 */
	@Override
	protected ASignStepResultDTO run(Connection con, ASignItemDTO item, Map<ContextASignEnum, Object> context) {
		Long idItem = item.getId();
		LOGGER.info("run -> START [ID item: " + idItem + "]");
		ASignStepResultDTO out = new ASignStepResultDTO(item.getId(), getStep());
		
		if (!Boolean.TRUE.equals(aSignSRV.isProtocollato(idItem))) {
			// Si recupera l'utente firmatario
			UtenteDTO utenteFirmatario = getUtenteFirmatario(item, con);
			
			// Si recupera il workflow
			VWWorkObject wob = getWorkflow(item, out, utenteFirmatario);
			
			if (wob != null) {
				// ### PROTOCOLLAZIONE ###
				EsitoOperazioneDTO esitoProt = signSRV.protocollazione(aSignSRV.getSignTransactionId(utenteFirmatario, "Batch"), wob, utenteFirmatario, con);
				
				if (esitoProt.isEsito()) {
					out.setStatus(true);
					out.appendMessage("[OK] Step di protocollazione eseguito");
					
					if (esitoProt.getNumeroProtocollo() != null && esitoProt.getNumeroProtocollo() > 0) {
						out.appendMessage(" Protocollo ottenuto: " + esitoProt.getNumeroProtocollo() + "/" + esitoProt.getAnnoProtocollo());
						
						// Aggiornamento dell'item con i dati del protocollo appena ottenuto
						aSignDAO.updateProtocolloItem(con, idItem, esitoProt.getNumeroProtocollo(), esitoProt.getAnnoProtocollo());
					}
				} else {
					throw new RedException("[KO] Errore nello step di protocollazione: " + esitoProt.getNote() + ". [Codice errore: " + esitoProt.getCodiceErrore() + "]");
				}
			} else {
				out.setStatus(false);
				out.appendMessage("[KO] Errore nello step di protocollazione: wob number non valido");
			}
		} else {
			out.setStatus(true);
			out.appendMessage("[OK] Il documento risulta già in possesso di un protocollo. Protocollo: " + item.getNumeroProtocollo() + "/" + item.getAnnoProtocollo());
		}
		
		LOGGER.info("run -> END [ID item: " + idItem + "]");
		return out;
	}

	/**
	 * Restituisce lo @see StatusEnum a cui fa riferimento la classe.
	 * @return StepEnum
	 */
	@Override
	protected StepEnum getStep() {
		return StepEnum.PROTOCOLLAZIONE;
	}

	/**
	 * @see it.ibm.red.business.service.asign.step.ASignStepAbstract#getErrorMessage().
	 */
	@Override
	protected String getErrorMessage() {
		return "[KO] Errore imprevisto nello step di protocollazione";
	}

}
