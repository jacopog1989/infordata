package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IValutaFacadeSRV;

/**
 * Interfaccia del servizio che gestisce i metadati di tipo Valuta.
 */
public interface IValutaSRV extends IValutaFacadeSRV {

}
