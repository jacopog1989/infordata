package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.IdDocumentDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * Trasformer dal Documento ad IdDocumentDTO.
 */
public class FromDocumentoToIdDocumentDTOTrasformer extends TrasformerCE<IdDocumentDTO> {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 167539275426825446L;

	/**
	 * Costruttore.
	 */
	public FromDocumentoToIdDocumentDTOTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_ID_DOCUMENTO);
	}
	
	/**
	 * Esegue la trasformazione del document.
	 * @param document
	 * @param connection
	 * @return IdDocumentDTO
	 */
	@Override
	public IdDocumentDTO trasform(final Document document, final Connection connection) {
		String documentTitle = (String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
		
		Integer numDocumento = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
		
		Integer numeroProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
		Integer annoProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
		Integer idAOO = (Integer) getMetadato(document, PropertiesNameEnum.ID_AOO_METAKEY);
		Date dataCreazione = (Date) getMetadato(document, PropertiesNameEnum.DATA_CREAZIONE_METAKEY);
		
		return new IdDocumentDTO(documentTitle, numDocumento, idAOO, annoProtocollo, numeroProtocollo, dataCreazione);
	}
	
	/**
	 * Restituisce la lista dei metadati.
	 * @return metadati
	 */
	public static final List<String> getMetadati() {
		EnumSet<PropertiesNameEnum> set = EnumSet.of(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY,
				PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY,
				PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY,
				PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY,
				PropertiesNameEnum.ID_AOO_METAKEY,
				PropertiesNameEnum.DATA_CREAZIONE_METAKEY);
		
		PropertiesProvider pp = PropertiesProvider.getIstance();
		List<String> metadatiList = new ArrayList<>(set.size());
		for (PropertiesNameEnum name : set) {
			metadatiList.add(pp.getParameterByKey(name));
		}
		return metadatiList;
	}
}
