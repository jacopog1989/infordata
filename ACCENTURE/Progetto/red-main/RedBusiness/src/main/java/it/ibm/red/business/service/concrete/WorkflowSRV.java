package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.IAllaccioDAO;
import it.ibm.red.business.dao.IWorkflowDAO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Workflow;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IDocumentoRedSRV;
import it.ibm.red.business.service.IWorkflowSRV;

/**
 * The Class WorkflowSRV.
 * 
 * @author m.crescentini
 * 
 *         Servizio gestione workflow su DB.
 *
 */
@Service
public class WorkflowSRV extends AbstractService implements IWorkflowSRV {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2759000299427080675L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(WorkflowSRV.class.getName());

	/**
	 * DAO.
	 */
	@Autowired
	private IWorkflowDAO workflowDAO;
	
	/**
	 * DAO.
	 */
	@Autowired
	private IAllaccioDAO allaccioDAO;
	
	/**
	 * Servizio.
	 */
	@Autowired
	private IDocumentoRedSRV documentoRedSRV;

	/**
	 * Fornitore della properties dell'applicazione.
	 */
	private PropertiesProvider pp;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.IWorkflowSRV#getWorkflowNameProcessoStep(java.sql.Connection).
	 */
	@Override
	public String getWorkflowNameProcessoStep(final Connection connection) {
		String workflowName = Constants.EMPTY_STRING;

		final int idWorkflowStep = Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.WF_PROCESSO_STEP_MAPPING_WFKEY));

		final Collection<Workflow> workflowList = workflowDAO.getAll(connection);

		for (final Workflow workflow : workflowList) {
			if (workflow.getId() == idWorkflowStep) {
				workflowName = workflow.getName();
				break;
			}
		}

		LOGGER.info("getWorkflowNameProcessoStep -> Nome del workflow: " + workflowName);
		return workflowName;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IWorkflowFacadeSRV#isDocumentInQueue(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.enums.DocumentQueueEnum, java.lang.String,
	 *      java.lang.Long, java.lang.Long, java.util.List).
	 */
	@Override
	public boolean isDocumentInQueue(final UtenteDTO utente, final DocumentQueueEnum queue, final String documentTitle, final Long idNodoDestinatario,
			final Long idUtenteDestinatario, final List<Long> idsTipoAssegnazioni) {

		FilenetPEHelper fpeh = null;

		try {

			fpeh = new FilenetPEHelper(utente.getFcDTO());
			return fpeh.hasDocumentInCoda(queue, documentTitle, idNodoDestinatario, idUtenteDestinatario, idsTipoAssegnazioni, false);

		} catch (final Exception e) {
			LOGGER.error("Attenzione, errore durante il recupero del documento in coda", e);
			throw new RedException("Attenzione, errore durante il recupero del documento in coda", e);
		} finally {
			logoff(fpeh);
		}

	}

	/**
	 * @see it.ibm.red.business.service.facade.IWorkflowFacadeSRV#getWorkflowId(it.ibm.red.business.dto.TipoProcedimentoDTO).
	 */
	@Override
	public int getWorkflowId(final TipoProcedimentoDTO procedimento) {
		Connection connection = null;
		int idWorkflow = 0;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			idWorkflow = workflowDAO.getWorkflowId(procedimento, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il setup della connessione", e);
			throw new RedException("Errore durante il setup della connessione", e);
		} finally {
			closeConnection(connection);
		}
		return idWorkflow;
	}

	/**
	 * @see it.ibm.red.business.service.IWorkflowSRV#gestioneCodaInLavorazioneInSospeso(filenet.vw.api.VWWorkObject,
	 *      java.lang.Integer, java.sql.Connection, java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      it.ibm.red.business.provider.PropertiesProvider).
	 */
	@Override
	public void gestioneCodaInLavorazioneInSospeso(final VWWorkObject workflow, final Integer idDocIngresso, final Connection con, final String motivo,
			final UtenteDTO utente, final IFilenetCEHelper fceh, final PropertiesProvider pp) {

		final String codaWorkflow = workflow.getCurrentQueueName();
		RispostaAllaccioDTO allaccio = null;
		if (DocumentQueueEnum.DA_LAVORARE.getName().equals(codaWorkflow)) {

			final boolean inLavorazione = TrasformerPE.getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.IN_LAVORAZIONE_METAKEY)) != null
					&& ((Integer) TrasformerPE.getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.IN_LAVORAZIONE_METAKEY))) == 1;

			if (inLavorazione) {
				// cerca eventuali osservazioni/visti non ancora spediti e annullali
				allaccio = allaccioDAO.getLastRichiestaVistoOsservazione(idDocIngresso, con);
			}

		} else if (DocumentQueueEnum.SOSPESO.getName().equals(codaWorkflow)) {

			// cerca eventuali richieste di integrazione non ancora spedite e annullale
			allaccio = allaccioDAO.getLastRichiestaIntegrazioni(idDocIngresso, con);
			if(allaccio != null) {
				Document doc = fceh.getDocumentByDTandAOO(allaccio.getIdDocumento().toString(), utente.getIdAoo());
				Integer numeroProtocollo = (Integer)TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
				Date dataProtocollo = (Date)TrasformerCE.getMetadato(doc, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);
				if(numeroProtocollo != null && dataProtocollo != null) {
					//il documento di integrazione dati è protocollato e non va annullato
					allaccio = null;
				}
			}

		}

		if (allaccio != null) {
			documentoRedSRV.annullaDocumentoOAnnullaProtocollo(allaccio.getIdDocumento().toString(), motivo, motivo, utente, fceh);
		}

	}

}