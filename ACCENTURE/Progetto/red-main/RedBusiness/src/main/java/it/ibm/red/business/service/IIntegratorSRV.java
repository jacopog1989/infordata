/**
 * 
 */
package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IIntegratorFacadeSRV;

/**
 * Interface del servizio integrator.
 */
public interface IIntegratorSRV extends IIntegratorFacadeSRV {

}