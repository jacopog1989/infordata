package it.ibm.red.business.enums;

import it.ibm.red.business.dto.IntervalloDisabilitazioneDTO;

/**
 * La descrizione dovrebbe essere impostata di default tramite uno switch
 * nell'xhtml Viene usato un moggetto per switchare tra una configurazione
 * specifica delle date
 * 
 */
public enum IntervalloDisabilitazioneEnum {

	
	/**
	 * Valore.
	 */
	SEMPRE("Sempre disabilitato"), 
	
	
	/**
	 * Valore.
	 */
	INTERVALLO("Disabilita per intervallo"), 
	
	
	/**
	 * Valore.
	 */
	MAI("Abilitato");


	/**
	 * Nome.
	 */
	private String nome;

	IntervalloDisabilitazioneEnum(final String nome) {
		this.nome = nome;
	}

	/**
	 * Recupera il nome.
	 * @return nome
	 */
	public String getNome() {
		return this.nome;
	}

	/**
	 * Ottiene l'intervallo di disabilitazione.
	 * @return intervallo di disabilitazione
	 */
	public IntervalloDisabilitazioneDTO getIntervalloDisabilitazioneDTO() {
		IntervalloDisabilitazioneDTO dto = new IntervalloDisabilitazioneDTO();
		dto.setId(this.ordinal());
		dto.setNome(nome);
		return dto;
	}
}
