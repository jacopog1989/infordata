package it.ibm.red.business.enums;

/**
 * Enum stati coda trasformazione.
 * 
 * @author a.dilegge
 *
 */
public enum StatoCodaEmailEnum {
	
	/**
	 * Da elaborare.
	 */
	DA_ELABORARE(0, "Da elaborare"),

	/**
	 * Attesa accettazione.
	 */
	ATTESA_ACCETTAZIONE(1, "Attesa accettazione"),

	/**
	 * Attesa avvenuta consegna.
	 */
	ATTESA_AVVENUTA_CONSEGNA(2, "Attesa avvenuta consegna"),

	/**
	 * Chiusura.
	 */
	CHIUSURA(3, "Chiusura"),

	/**
	 * Errore.
	 */
	ERRORE(4, "Errore"),

	/**
	 * Reinvio.
	 */
	REINVIO(5, "Reinvio"),
	
	/**
	 * Elaborazione.
	 */
	ELABORAZIONE(6, "Elaborazione"),
	
	/**
	 * Spedito.
	 */
	SPEDITO(7, "Spedito"),
	
	/**
	 * Attesa spedizione utente.
	 */
	ATTESA_SPEDIZIONE_UTENTE(8, "Attesa spedizione utente"),
	
	/**
	 * Attesa spedizione NPS.
	 */
	ATTESA_SPEDIZIONE_NPS(9, "Attesa spedizione NPS");

	/**
	 * Stato.
	 */
	private Integer status;
	
	/**
	 * Descrizione.
	 */
	private String description;

	/**
	 * Costruttore.
	 * 
	 * @param inStatus		stato
	 * @param inDescription	descrizione
	 */
	StatoCodaEmailEnum(final Integer inStatus, final String inDescription) {
		status = inStatus;
		description = inDescription;
	}

	/**
	 * Getter.
	 * 
	 * @return	descrizione
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Getter.
	 * 
	 * @return	stato
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Metodo per il recupero di un enum a partire dal valore associato.
	 * 
	 * @param value	valore
	 * @return		enum
	 */
	public static StatoCodaEmailEnum get(final Integer value) {
		StatoCodaEmailEnum output = null;
		for (StatoCodaEmailEnum sme:StatoCodaEmailEnum.values()) {
			if (sme.getStatus().equals(value)) {
				output = sme;
			}
		}
		return output;
	}
	
}
