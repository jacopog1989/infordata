package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import it.ibm.red.business.logger.REDLogger;
import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IRiferimentoDAO;
import it.ibm.red.business.exception.RedException;

/**
 * Dao per la gestione dei riferimenti.
 *
 * @author mcrescentini
 */
@Repository
public class RiferimentoDAO extends AbstractDAO implements IRiferimentoDAO {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 6982140673282987407L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RiferimentoDAO.class.getName());
	
	/**
	 * @see it.ibm.red.business.dao.IRiferimentoDAO#insertRiferimentoFascicolo(int,
	 *      int, int, java.sql.Connection).
	 */
	@Override
	public int insertRiferimentoFascicolo(final int idRisorsa, final int idRiferimento, final int tipoRiferimento, final Connection con) {
		PreparedStatement ps = null;
		int result = 0;
		try {
			int index = 1;
			ps = con.prepareStatement("INSERT INTO RIFERIMENTO (idrisorsa, idriferimento, tipologia) values (?,?,?)");
			ps.setInt(index++, idRisorsa);
			ps.setInt(index++, idRiferimento);
			ps.setInt(index++, tipoRiferimento);
			
			result = ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("Errore durante l'inserimento del Riferimento: " + idRiferimento + " per la risorsa: " + idRisorsa, e);
			throw new RedException("Errore durante l'inserimento del Riferimento: " + idRiferimento + " per la risorsa: " + idRisorsa, e);
		} finally {
			closeStatement(ps);
		}
		
		return result;
	}
}