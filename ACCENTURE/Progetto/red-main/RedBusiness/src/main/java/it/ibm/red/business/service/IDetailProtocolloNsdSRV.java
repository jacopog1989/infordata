/**
 * 
 */
package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IDetailProtocolloNsdFacadeSRV;

/**
 * @author VINGENITO
 *
 */
public interface IDetailProtocolloNsdSRV extends IDetailProtocolloNsdFacadeSRV {

}
