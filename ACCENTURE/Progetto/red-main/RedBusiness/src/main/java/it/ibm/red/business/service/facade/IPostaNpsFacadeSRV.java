package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.MessaggioPostaNpsDTO;
import it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraErroreProtocollazioneAutomaticaType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioPostaType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioProtocollazioneAutomaticaType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioProtocollazioneFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraNotificaInteroperabilitaProtocolloType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraNotificaPECType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.OrgCasellaEmailType;

/**
 * Facade del servizio di gestione posta NPS.
 */
public interface IPostaNpsFacadeSRV extends Serializable {

	/**
     * Accoda una richiesta di elaborazione di un messaggio di posta (PEO o PEC), restituendo la PK dell'item della coda inserito.
     * La request verrà trasformata in un nuovo DTO, e questo sarà serializzato nella base dati.
     * 
     * @param messageIdNps	Message ID della request inviata da NPS e contenuto nell'header SOAP
     * @param richiesta   	Request NPS
     * @return          	PK della coda
     */
	Long accodaMessaggioPosta(String messageIdNps, RichiestaElaboraMessaggioPostaType richiesta, Long idTrack);
	
	
	/**
	 * Accoda una richiesta di elaborazione di una notifica PEC, restituendo la PK dell'item della coda inserito.
     * La request verrà trasformata in un nuovo DTO, e questo sarà serializzato nella base dati.
     * 
     * @param messageIdNps	Message ID della request inviata da NPS e contenuto nell'header SOAP
     * @param richiesta   	Request NPS
	 * @return
	 */
	Long accodaNotificaPEC(String messageIdNps, RichiestaElaboraNotificaPECType richiesta, Long idTrack);
	
	
	/**
	 * Accoda una richiesta di elaborazione di una notifica di interoperabilità.
	 * 
     * @param messageIdNps	Message ID della request inviata da NPS e contenuto nell'header SOAP
     * @param richiesta   	Request NPS
	 * @return
	 */
	Long accodaNotificaInteroperabilita(String messageIdNps, RichiestaElaboraNotificaInteroperabilitaProtocolloType richiesta, Long idTrack);
	
	
	/**
	 * Accoda una richiesta di elaborazione di una protocollazione automatica, restituendo la PK dell'item della coda inserito.
     * La request verrà trasformata in un nuovo DTO, e questo sarà serializzato nella base dati.
	 * 
	 * @param messageId
	 * @param parameters
	 * @param idTrack
	 * @return 
	 */
	Long accodaMessaggioProtocollazioneAutomatica(String messageId,	RichiestaElaboraMessaggioProtocollazioneAutomaticaType richiesta, Long idTrack);
	
	
	/**
	 * 	Accoda una richiesta di elaborazione di un messaggio di posta afferente a un flusso.
	 * 
	 * @param messageId
	 * @param parameters
	 * @param idTrack
	 */
	Long accodaMessaggioProtocollazioneFlusso(String messageIdNps, RichiestaElaboraMessaggioProtocollazioneFlussoType richiesta, Long idTrack);


	/**
	 * Accoda una richiesta di elaborazione di una notifica di errore durante la protocollazione automatica, restituendo la PK dell'item della coda inserito.
     * La request verrà trasformata in un nuovo DTO, e questo sarà serializzato nella base dati.
     * 
	 * @param messageIdNps
	 * @param richiesta
	 * @param idTrack
	 * @return
	 */
	Long accodaNotificaErroreProtocollazioneAutomatica(String messageIdNps, RichiestaElaboraErroreProtocollazioneAutomaticaType richiesta, Long idTrack);

	
	/**
	 * Accoda una richiesta di elaborazione di un messaggio di posta afferente a un flusso.
	 * 
	 * @param messageId
	 * @param parameters
	 * @param idTrack
	 */
	Long accodaMessaggioFlusso(String messageId, RichiestaElaboraMessaggioFlussoType parameters, Long idTrack);
	
	
	/**
     * Persiste sullo strato di persistenza l'item della coda in input, restituendo il GUID della mail salvata su Filenet.
     * 
     * @param <T>
     * @param richiestaElabMessagio   Richiesta di elaborazione di un messaggio
     * @return
     */
	<T extends MessaggioPostaNpsDTO> String elaboraMessaggioPostaNpsInIngresso(RichiestaElabMessaggioPostaNpsDTO<T> richiestaElabMessagio);
	
	
	/**
	 * Recupera (con lock) una richiesta da lavorare dalla coda di elaborazione.
	 * 
	 * @param idAoo
	 * @return
	 */
	<T extends MessaggioPostaNpsDTO> RichiestaElabMessaggioPostaNpsDTO<T> recuperaRichiestaElabDaLavorare(Long idAoo);

	/**
	 * Verifica che la casella di posta inviata da NPS sia configurata per la protocollazione automatica
	 * 
	 * @param casellaPostaNPS
	 * @return
	 */
	boolean isCasellaPostaleProtocollazioneAutomatica(OrgCasellaEmailType casellaPostaNPS);

}
