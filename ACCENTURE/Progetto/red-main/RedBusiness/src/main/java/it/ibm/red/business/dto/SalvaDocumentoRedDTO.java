package it.ibm.red.business.dto;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.persistence.model.Contatto;

/**
 * DTO che definisce l'entità di salvataggio di un documento RED.
 */
public class SalvaDocumentoRedDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -4412208454299894494L;
	
	/**
	 * Document title.
	 */
	private String documentTitle;
	
	/**
	 * Guuid.
	 */
	private String guid;
	
	/**
	 * Identificativo wf.
	 */
	private String wobNumber;
	
	/**
	 * Identificativo aoo.
	 */
	private Long idAOO;
	
	/**
	 * Iter approvativo.
	 */
	private IterApprovativoDTO iterApprovativo;
	
	/**
	 * Identificativo categoria documento.
	 */
	private Integer idCategoriaDocumento;
	
	/**
	 * Formato documento.
	 */
	private FormatoDocumentoEnum formatoDocumentoEnum;
	
	/**
	 * Tipo assegnazione.
	 */
	private TipoAssegnazioneEnum tipoAssegnazioneEnum;
	
	/**
	 * Momento protocollazione.
	 */
	private MomentoProtocollazioneEnum momentoProtocollazioneEnum; 
	
	/**
	 * Tipologia documento.
	 */
	private TipologiaDocumentoDTO tipologiaDocumento;
	
	/**
	 * Tipo procedimento.
	 */
	private TipoProcedimentoDTO tipoProcedimento;
	
	/**
	 * Mezzo ricezione.
	 */
	private MezzoRicezioneDTO mezzoRicezione;
	
	/**
	 * Mail spedizione.
	 */
	private EmailDTO mailSpedizione;
	
	/**
	 * Contatto mittente.
	 */
	private Contatto mittenteContatto;
	
	/**
	 * Motivo assegnazione.
	 */
	private String motivoAssegnazione;
	
	/**
	 * Indice classificazione fascicolo procedimentale.
	 */
	private String indiceClassificazioneFascicoloProcedimentale;
	
	/**
	 * Descrizione indice classificazione fascicolo procedimentale.
	 */
	private String descrizioneIndiceClassificazioneFascicoloProcedimentale;
	
	/**
	 * Descrizione fascicolo procedimentale.
	 */
	private String descrizioneFascicoloProcedimentale;
	
	/**
	 * Data protocollo.
	 */
	private Date dataProtocollo;
	
	/**
	 * Data protocollo risposta.
	 */
	private Date dataProtocolloRisposta;
	
	/**
	 * Data protocollo mittente.
	 */
	private Date dataProtocolloMittente;
	
	/**
	 * Numero contributo mittnte.
	 */
	private Integer numeroMittenteContributo;
	
	/**
	 * Anno contributo mittente.
	 */
	private Integer annoMittenteContributo;
	
	/**
	 * Barcode.
	 */
	private String barcode;
	
	/**
	 * Contenuto.
	 */
	private byte[] content;
	
	/**
	 * Content type.
	 */
	private String contentType;
	
	/**
	 * Identificativo protocollo.
	 */
	private String idProtocollo;
	
	/**
	 * Anno protocollo.
	 */
	private Integer annoProtocollo;

	/**
	 * Anno protocollo mittente.
	 */
	private Integer annoProtocolloMittente;
	
	/**
	 * Anno protocollo risposta.
	 */
	private Integer annoProtocolloRisposta;
	
	/**
	 * Nome file.
	 */
	private String nomeFile;
	
	/**
	 * Oggetto.
	 */
	private String oggetto;
	
	/**
	 * Folder.
	 */
	private String folder;
	
	/**
	 * Destinatario contributo esterno.
	 */
	private String destinatarioContributoEsterno;
	
	/* *********** PRELEX ************ */
	/**
	 * Numero RDP.
	 */
	private Integer numeroRDP;
	
	/**
	 * Numero legislatura.
	 */
	private Integer numeroLegislatura;
	/* ******************************* */
	
	/**
	 * Numero protocollo.
	 */
	private Integer numeroProtocollo;
	
	/**
	 * Protocollo mittente.
	 */
	private String protocolloMittente;
	
	/**
	 * Numero protocollo risposta.
	 */
	private String numeroProtocolloRisposta;
	
	/**
	 * Numero raccomandata.
	 */
	private String numeroRaccomandata;
	
	/**
	 * Note.
	 */
	private String note;

	/**
	 * Tipologia documento NPS.
	 */
	private String tipologiaDocumentoNps;
	
	/**
	 * Data scadenza.
	 */
	private Date dataScadenza;
	
	/**
	 * Identificativo ufficio coordinatore.
	 */
	private Long idUfficioCoordinatore;
	
	/**
	 * Identificativo utente coordinatore.
	 */
	private Long idUtenteCoordinatore;
	
	/**
	 * Identifiativo ufficio creatore.
	 */
	private Long idUfficioCreatore;
	
	/**
	 * Identificativo utente creatore.
	 */
	private Long idUtenteCreatore;
	
	/**
	 * Flag urgente.
	 */
	private Boolean isUrgente;
	
	/**
	 * Flag registro riservato.
	 */
	private Boolean registroRiservato;
	
	/**
	 * Flag riservato.
	 */
	private Boolean isRiservato;
	
	/**
	 * Flag iter manuale.
	 */
	private Boolean isIterManuale;
	
	/**
	 * Flag corte dei conti.
	 */
	private Boolean flagCorteConti;
	
	/**
	 * Flag da protocollare.
	 */
	private Boolean daProtocollare;
	
	/**
	 * Flag check firma digitale RGS.
	 */
	private Boolean checkFirmaDigitaleRGS;
	
	/**
	 * Flag firma copia conforme.
	 */
	private Boolean firmaConCopiaConforme;
	
	/**
	 * Flag tracciamento procedimento.
	 */
	private Boolean tracciaProcedimento;
	
	/**
	 * Flag numero dodcumento destinatrio interno.
	 */
	private Integer numDocDestIntUscita; // Metadato per documento con categoria Assegnazione Interna
	
	/**
	 * Fascicolo procedimentale.
	 */
	private FascicoloDTO fascicoloProcedimentale;
	
	/**
	 * Id raccolta FAD.
	 */
	private String idRaccoltaFAD;
	
	/**
	 * Responsabile copia conforme.
	 */
	private ResponsabileCopiaConformeDTO responsabileCopiaConforme;
	
	/**
	 * Lista fascicoli.
	 */
	private List<FascicoloDTO> fascicoli;
	
	/**
	 * Lista faldoni.
	 */
	private Collection<FaldoneDTO> faldoni;
	
	/**
	 * Lista allacci.
	 */
	private List<RispostaAllaccioDTO> allacci;

	/**
	 * Allacci storici.
	 */
	private List<RiferimentoStoricoNsdDTO> allacciRifStorico;
	
	/**
	 * Lista sssegnazioni.
	 */
	private List<AssegnazioneDTO> assegnazioni;
	
	/**
	 * Lista destinatari.
	 */
	private List<DestinatarioRedDTO> destinatari;
	
	/**
	 * Lista allegati.
	 */
	private List<AllegatoDTO> allegati;
	
	/**
	 * Lista contributi.
	 */
	private List<ContributoDTO> contributi;
	
	/**
	 * Lista approvazioni.
	 */
	private List<ApprovazioneDTO> approvazioni;
	
	/**
	 * Metadati dinamici.
	 */
	private transient Map<String, Object> metadatiDinamici;
	
	/**
	 * Metadati estesi.
	 */
	private Collection<MetadatoDTO> metadatiEstesi;

	/**
	 * Template documento uscita.
	 */
	private Map<TemplateValueDTO, List<TemplateMetadatoValueDTO>> templateDocUscitaMap;
	
	/**
	 * Preassegnatario mail interoperabile.
	 */
	private AssegnatarioInteropDTO preassegnatarioDaMailProtocollaInterop;
	
	/**
	 * Nota mail.
	 */
	private NotaDTO contentNoteDaMailProtocollaDTO;
	
	/**
	 * Account protocollazione.
	 */
	private String protocollazioneMailAccount;
	
	/**
	 * Guid documenti cartacei.
	 */
	private NodoDTO<String> guidDocumentiCartacei;
	
	/**
	 * Anno protocollo emergenza.
	 */
	private Integer annoProtocolloEmergenza;
	
	/**
	 * Numero protocollo emergenza.
	 */
	private Integer numeroProtocolloEmergenza;
	
	/**
	 * Data protocollo emergenza.
	 */
	private Date dataProtocolloEmergenza;
	
	/**
	 * Numero documento.
	 */
	private Integer numeroDocumento;
	
	/**
	 * Codice flusso.
	 */
	private String codiceFlusso;
	
	/**
	 * Decreto liquidazione Sicoge.
	 */
	private String decretoLiquidazioneSicoge;
	
	/**
	 * Mittente mail.
	 */
	private String mittenteMailProt;
	
	/**
	 * Flag protocolla e mantieni.
	 */
	private boolean protocollaEMantieni;
	
	/**
	 * Lista allegati non sbustati.
	 */
	private List<AllegatoDTO> allegatiNonSbustati;
	
	/**
	 * Flag assegnazione indiretta.
	 */
	private Boolean assegnazioneIndiretta;
	
	/**
	 * Sotto categoria documento uscita.
	 */
	private SottoCategoriaDocumentoUscitaEnum sottoCategoriaUscita;
	
	/**
	 * Identificativo registro ausiliario.
	 */
	private Integer idRegistroAusiliario;
	
	/**
	 * Lista metadati registro ausiliario.
	 */
	private Collection<MetadatoDTO> metadatiRegistroAusiliario;
	
	/**
	 * Flag integrazione dati.
	 */
	private boolean integrazioneDati;
	
	/**
	 * Protocollo riferimento.
	 */
	private String protocolloRiferimento;
	
	/**
	 * Identificatore processo.
	 */
	private String identificatoreProcesso;
	
	/**
	 * Identificativo mittente OTF.
	 */
	private String objectIdContattoMittOnTheFly;

	/**
	 * Identificativo destinatario OTF.
	 */
	private String objectIdContattoDestOnTheFly;
	 
	/**
	 * FLag modifica metadati minimi.
	 */
	private boolean modificaMetadatiMinimi;
	
	/**
	 * Data download.
	 */
	private Date dataScarico;
	
	/**
	 * Id protocollo risposta.
	 */
	private String idProtocolloRisposta;
	
	/**
	 * Restituisce il documentTitle.
	 * @return documentTitle.
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta il documentTitle.
	 * @param documentTitle
	 */
	public void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * Restituisce il guid.
	 * @return guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * Imposta il guid.
	 * @param guid
	 */
	public void setGuid(final String guid) {
		this.guid = guid;
	}

	/**
	 * Restituisce il wob number.
	 * @return wob number
	 */
	public String getWobNumber() {
		return wobNumber;
	}

	/**
	 * Imposta il wob number.
	 * @param wobNumber
	 */
	public void setWobNumber(final String wobNumber) {
		this.wobNumber = wobNumber;
	}

	/**
	 * Restituisce l'id dell'Area Organizzativa Omogenea.
	 * @return id AOO
	 */
	public Long getIdAOO() {
		return idAOO;
	}

	/**
	 * Imposta l'id dell'AOO.
	 * @param idAOO
	 */
	public void setIdAOO(final Long idAOO) {
		this.idAOO = idAOO;
	}

	/**
	 * Restituisce l'id della categoria del documento.
	 * @return id categoria documento
	 */
	public Integer getIdCategoriaDocumento() {
		return idCategoriaDocumento;
	}

	/**
	 * Imposta l'id della categoria del documento.
	 * @param idCategoriaDocumento
	 */
	public void setIdCategoriaDocumento(final Integer idCategoriaDocumento) {
		this.idCategoriaDocumento = idCategoriaDocumento;
	}

	/**
	 * Restituisce il formato del documento.
	 * @return formato documento
	 */
	public FormatoDocumentoEnum getFormatoDocumentoEnum() {
		return formatoDocumentoEnum;
	}

	/**
	 * Imposta il formato del documento.
	 * @param formatoDocumentoEnum
	 */
	public void setFormatoDocumentoEnum(final FormatoDocumentoEnum formatoDocumentoEnum) {
		this.formatoDocumentoEnum = formatoDocumentoEnum;
	}

	/**
	 * Restituisce la data del protocollo mittente.
	 * @return data protocollo mittente
	 */
	public Date getDataProtocolloMittente() {
		return dataProtocolloMittente;
	}

	/**
	 * Imposta la data del protocollo mittente.
	 * @param dataProtocolloMittente
	 */
	public void setDataProtocolloMittente(final Date dataProtocolloMittente) {
		this.dataProtocolloMittente = dataProtocolloMittente;
	}

	/**
	 * Restituisce il numero mittente del contributo.
	 * @return numero mittente contributo
	 */
	public Integer getNumeroMittenteContributo() {
		return numeroMittenteContributo;
	}

	/**
	 * Imposta il numero mittente del contributo.
	 * @param numeroMittenteContributo
	 */
	public void setNumeroMittenteContributo(final Integer numeroMittenteContributo) {
		this.numeroMittenteContributo = numeroMittenteContributo;
	}

	/**
	 * Restituisce l'anno mittente contributo.
	 * @return anno mittente
	 */
	public Integer getAnnoMittenteContributo() {
		return annoMittenteContributo;
	}

	/**
	 * Imposta l'anno mittente contributo.
	 * @param annoMittenteContributo
	 */
	public void setAnnoMittenteContributo(final Integer annoMittenteContributo) {
		this.annoMittenteContributo = annoMittenteContributo;
	}

	/**
	 * Restituisce true se iter manuale, false altrimenti.
	 * @return true se iter manuale, false altrimenti
	 */
	public Boolean getIsIterManuale() {
		return isIterManuale;
	}

	/**
	 * Imposta il flag associato all'iter manuale.
	 * @param isIterManuale
	 */
	public void setIsIterManuale(final Boolean isIterManuale) {
		this.isIterManuale = isIterManuale;
	}

	/**
	 * Restituisce la mail spedizione.
	 * @return mail spedizione
	 */
	public EmailDTO getMailSpedizione() {
		return mailSpedizione;
	}

	/**
	 * Imposta la mail spedizione.
	 * @param mailSpedizione
	 */
	public void setMailSpedizione(final EmailDTO mailSpedizione) {
		this.mailSpedizione = mailSpedizione;
	}

	/**
	 * Restituisce il mittente contatto.
	 * @return mittente contatto
	 */
	public Contatto getMittenteContatto() {
		return mittenteContatto;
	}

	/**
	 * Imposta il mittente contatto.
	 * @param mittenteContatto
	 */
	public void setMittenteContatto(final Contatto mittenteContatto) {
		this.mittenteContatto = mittenteContatto;
	}

	/**
	 * Restituisce il motivo assegnazione.
	 * @return motivo assegnazione
	 */
	public String getMotivoAssegnazione() {
		return motivoAssegnazione;
	}

	/**
	 * Imposta il motivo assegnazione.
	 * @param motivoAssegnazione
	 */
	public void setMotivoAssegnazione(final String motivoAssegnazione) {
		this.motivoAssegnazione = motivoAssegnazione;
	}

	/**
	 * Restituisce l'indice di classificazione del fascicolo procedimentale.
	 * @return indice classificiazione fascicolo procedimentale
	 */
	public String getIndiceClassificazioneFascicoloProcedimentale() {
		return indiceClassificazioneFascicoloProcedimentale;
	}

	/**
	 * Imposta l'indice di classificazione del fascicolo procedimentale.
	 * @param indiceClassificazioneFascicoloProcedimentale
	 */
	public void setIndiceClassificazioneFascicoloProcedimentale(final String indiceClassificazioneFascicoloProcedimentale) {
		this.indiceClassificazioneFascicoloProcedimentale = indiceClassificazioneFascicoloProcedimentale;
	}

	/**
	 * Restituisce la descrizione del fascicolo procedimentale.
	 * @return descrizione del fascicolo procedimentale
	 */
	public String getDescrizioneFascicoloProcedimentale() {
		return descrizioneFascicoloProcedimentale;
	}

	/**
	 * Imposta la descrizione del fascicolo procedimentale.
	 * @param descrizioneFascicoloProcedimentale
	 */
	public void setDescrizioneFascicoloProcedimentale(final String descrizioneFascicoloProcedimentale) {
		this.descrizioneFascicoloProcedimentale = descrizioneFascicoloProcedimentale;
	}

	/**
	 * Restituisce la data del protocollo.
	 * @return data protocollo
	 */
	public Date getDataProtocollo() {
		return dataProtocollo;
	}

	/**
	 * Imposta la data del protocollo.
	 * @param dataProtocollo
	 */
	public void setDataProtocollo(final Date dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}

	/**
	 * Restituisce la data protocollo risposta.
	 * @return data protocollo risposta
	 */
	public Date getDataProtocolloRisposta() {
		return dataProtocolloRisposta;
	}

	/**
	 * Imposta la data protocollo risposta.
	 * @param dataProtocolloRisposta
	 */
	public void setDataProtocolloRisposta(final Date dataProtocolloRisposta) {
		this.dataProtocolloRisposta = dataProtocolloRisposta;
	}

	/**
	 * Restituisce il tipo assegnazione.
	 * @return tipo assegnazione
	 */
	public TipoAssegnazioneEnum getTipoAssegnazioneEnum() {
		return tipoAssegnazioneEnum;
	}

	/**
	 * Imposta il tipo assegnazione.
	 * @param tipoAssegnazioneEnum
	 */
	public void setTipoAssegnazioneEnum(final TipoAssegnazioneEnum tipoAssegnazioneEnum) {
		this.tipoAssegnazioneEnum = tipoAssegnazioneEnum;
	}

	/**
	 * Restituisce true se in firma digitale RGS.
	 * @return true se in firma digitale RGS, false altrimenti
	 */
	public Boolean getCheckFirmaDigitaleRGS() {
		return checkFirmaDigitaleRGS;
	}

	/**
	 * Imposta il flag associato alla firma digitale RGS.
	 * @param checkFirmaDigitaleRGS
	 */
	public void setCheckFirmaDigitaleRGS(final Boolean checkFirmaDigitaleRGS) {
		this.checkFirmaDigitaleRGS = checkFirmaDigitaleRGS;
	}

	/**
	 * Restituisce true se in copia conforme, false altrimenti.
	 * @return true se in copia conforme, false altrimenti
	 */
	public Boolean getFirmaConCopiaConforme() {
		return firmaConCopiaConforme;
	}

	/**
	 * Imposta il flag associato alla copia conforme.
	 * @param firmaConCopiaConforme
	 */
	public void setFirmaConCopiaConforme(final Boolean firmaConCopiaConforme) {
		this.firmaConCopiaConforme = firmaConCopiaConforme;
	}

	/**
	 * Restituisce il barcode.
	 * @return barcode
	 */
	public String getBarcode() {
		return barcode;
	}

	/**
	 * Imposta il barcode.
	 * @param barcode
	 */
	public void setBarcode(final String barcode) {
		this.barcode = barcode;
	}

	/**
	 * Restituisce il content come byte array.
	 * @return content
	 */
	public byte[] getContent() {
		return content;
	}

	/**
	 * Imposta il content.
	 * @param content
	 */
	public void setContent(final byte[] content) {
		this.content = content;
	}

	/**
	 * Restituisce il content type.
	 * @return content type
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * Imposta il content type.
	 * @param contentType
	 */
	public void setContentType(final String contentType) {
		this.contentType = contentType;
	}

	/**
	 * Restituisce l'id del protocollo.
	 * @return id protocollo
	 */
	public String getIdProtocollo() {
		return idProtocollo;
	}

	/**
	 * Imposta l'id del protocollo.
	 * @param idProtocollo
	 */
	public void setIdProtocollo(final String idProtocollo) {
		this.idProtocollo = idProtocollo;
	}

	/**
	 * Restitusice l'anno del protocollo.
	 * @return anno protocollo
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * Imposta l'anno del protocollo.
	 * @param annoProtocollo
	 */
	public void setAnnoProtocollo(final Integer annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/**
	 * Restituisce l'anno del protocollo mittente.
	 * @return anno protocollo mittente
	 */
	public Integer getAnnoProtocolloMittente() {
		return annoProtocolloMittente;
	}

	/**
	 * Imposta l'anno del protocollo mittente.
	 * @param annoProtocolloMittente
	 */
	public void setAnnoProtocolloMittente(final Integer annoProtocolloMittente) {
		this.annoProtocolloMittente = annoProtocolloMittente;
	}

	/**
	 * Restituisce l'anno protocollo risposta.
	 * @return anno protocollo risposta
	 */
	public Integer getAnnoProtocolloRisposta() {
		return annoProtocolloRisposta;
	}

	/**
	 * Imposta l'anno protocollo risposta.
	 * @param annoProtocolloRisposta
	 */
	public void setAnnoProtocolloRisposta(final Integer annoProtocolloRisposta) {
		this.annoProtocolloRisposta = annoProtocolloRisposta;
	}
	
	/**
	 * Restituisce il nome del file.
	 * @return nome file
	 */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * Imposta il nome del file.
	 * @param nomeFile
	 */
	public void setNomeFile(final String nomeFile) {
		this.nomeFile = nomeFile;
	}

	/**
	 * Restituisce l'oggetto.
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Imposta l'oggetto.
	 * @param oggetto
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * Restituisce la folder.
	 * @return folder
	 */
	public String getFolder() {
		return folder;
	}

	/**
	 * Imposta la folder.
	 * @param folder
	 */
	public void setFolder(final String folder) {
		this.folder = folder;
	}

	/**
	 * Restituisce il numero RDP.
	 * @return numero RDP
	 */
	public Integer getNumeroRDP() {
		return numeroRDP;
	}

	/**
	 * Imposta il numero RDP.
	 * @param numeroRDP
	 */
	public void setNumeroRDP(final Integer numeroRDP) {
		this.numeroRDP = numeroRDP;
	}

	/**
	 * Restituisce il numero legislatura.
	 * @return numero legislatura
	 */
	public Integer getNumeroLegislatura() {
		return numeroLegislatura;
	}

	/**
	 * Imposta il numero legislatura.
	 * @param numeroLegislatura
	 */
	public void setNumeroLegislatura(final Integer numeroLegislatura) {
		this.numeroLegislatura = numeroLegislatura;
	}
	
	/**
	 * Restituisce l'iter approvativo.
	 * @return iter approvativo
	 */
	public IterApprovativoDTO getIterApprovativo() {
		return iterApprovativo;
	}

	/**
	 * Imposta l'iter approvativo.
	 * @param iterApprovativo
	 */
	public void setIterApprovativo(final IterApprovativoDTO iterApprovativo) {
		this.iterApprovativo = iterApprovativo;
	}

	/**
	 * Restituisce il momento protocollazione enum.
	 * @see MomentoProtocollazioneEnum.
	 * @return momento protocollazione
	 */
	public MomentoProtocollazioneEnum getMomentoProtocollazioneEnum() {
		return momentoProtocollazioneEnum;
	}

	/**
	 * Imposta il momento protocollazione.
	 * @param momentoProtocollazioneEnum
	 */
	public void setMomentoProtocollazioneEnum(final MomentoProtocollazioneEnum momentoProtocollazioneEnum) {
		this.momentoProtocollazioneEnum = momentoProtocollazioneEnum;
	}

	/**
	 * Restituisce la tipologia documento.
	 * @return tipologia documento
	 */
	public TipologiaDocumentoDTO getTipologiaDocumento() {
		return tipologiaDocumento;
	}

	/**
	 * Imposta la tipologia documento.
	 * @param tipologiaDocumento
	 */
	public void setTipologiaDocumento(final TipologiaDocumentoDTO tipologiaDocumento) {
		this.tipologiaDocumento = tipologiaDocumento;
	}

	/**
	 * Restituisce il tipo procedimento.
	 * @return tipo procedimento
	 */
	public TipoProcedimentoDTO getTipoProcedimento() {
		return tipoProcedimento;
	}

	/**
	 * Imposta il tipo procedimento.
	 * @param tipoProcedimento
	 */
	public void setTipoProcedimento(final TipoProcedimentoDTO tipoProcedimento) {
		this.tipoProcedimento = tipoProcedimento;
	}

	/**
	 * Restituisce il numero protocollo.
	 * @return numero protocollo
	 */
	public Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Restituisce il protocollo mittente.
	 * @return protocollo mittente
	 */
	public String getProtocolloMittente() {
		return protocolloMittente;
	}
	
	/**
	 * Imposta il numero protocollo.
	 * @param numeroProtocollo
	 */
	public void setNumeroProtocollo(final Integer numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	/**
	 * Restitusice il numero protocollo risposta.
	 * @return numero protocollo risposta
	 */
	public String getNumeroProtocolloRisposta() {
		return numeroProtocolloRisposta;
	}
	
	/**
	 * Imposta il protocollo mittente.
	 * @param protocolloMittente
	 */
	public void setProtocolloMittente(final String protocolloMittente) {
		this.protocolloMittente = protocolloMittente;
	}

	/**
	 * Restituisce il numero raccomandata.
	 * @return numero raccomandata
	 */
	public String getNumeroRaccomandata() {
		return numeroRaccomandata;
	}
	
	/**
	 * Imposta il numero protocollo risposta.
	 * @param numeroProtocolloRisposta
	 */
	public void setNumeroProtocolloRisposta(final String numeroProtocolloRisposta) {
		this.numeroProtocolloRisposta = numeroProtocolloRisposta;
	}

	/**
	 * Imposta il numero raccomandata.
	 * @param numeroRaccomandata
	 */
	public void setNumeroRaccomandata(final String numeroRaccomandata) {
		this.numeroRaccomandata = numeroRaccomandata;
	}
	
	/**
	 * Restituisce true se registro riservato, false altrimenti.
	 * @return true se registro riservato, false altrimenti
	 */
	public Boolean getRegistroRiservato() {
		return registroRiservato;
	}

	/**
	 * Imposta il flag associato al registro riservato.
	 * @param registroRiservato
	 */
	public void setRegistroRiservato(final Boolean registroRiservato) {
		this.registroRiservato = registroRiservato;
	}

	/**
	 * Restituisce le note.
	 * @return note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * Imposta le note.
	 * @param note
	 */
	public void setNote(final String note) {
		this.note = note;
	}

	/**
	 * Restituisce la tipologia del documento Nps.
	 * @return tipologia documento Nps
	 */
	public String getTipologiaDocumentoNps() {
		return tipologiaDocumentoNps;
	}

	/**
	 * Imposta la tipologia del documento Nps.
	 * @param tipologiaDocumentoNps
	 */
	public void setTipologiaDocumentoNps(final String tipologiaDocumentoNps) {
		this.tipologiaDocumentoNps = tipologiaDocumentoNps;
	}

	/**
	 * Restituisce la data di scadenza del documento.
	 * @return data di scadenza documento
	 */
	public Date getDataScadenza() {
		return dataScadenza;
	}

	/**
	 * Imposta la data di scadenza del documento.
	 * @param dataScadenza
	 */
	public void setDataScadenza(final Date dataScadenza) {
		this.dataScadenza = dataScadenza;
	}

	/**
	 * Restituisce l'id dell'ufficio del coordinatore.
	 * @return id ufficio coordinatore
	 */
	public Long getIdUfficioCoordinatore() {
		return idUfficioCoordinatore;
	}

	/**
	 * Imposta l'id dell ufficio del coordinatore.
	 * @param idUfficioCoordinatore
	 */
	public void setIdUfficioCoordinatore(final Long idUfficioCoordinatore) {
		this.idUfficioCoordinatore = idUfficioCoordinatore;
	}

	/**
	 * Rstituisce l'id dell'utente coordinatore.
	 * @return id utente coordinatore
	 */
	public Long getIdUtenteCoordinatore() {
		return idUtenteCoordinatore;
	}
	
	/**
	 * Imposta l'id dell'utente coordinatore.
	 * @param idUtenteCoordinatore
	 */
	public void setIdUtenteCoordinatore(final Long idUtenteCoordinatore) {
		this.idUtenteCoordinatore = idUtenteCoordinatore;
	}

	/**
	 * Restituisce true se urgente, false altrimenti.
	 * @return true se urgente, false altrimenti
	 */
	public Boolean getIsUrgente() {
		return isUrgente;
	}

	/**
	 * Imposta il flag associato all'urgenza.
	 * @param isUrgente
	 */
	public void setIsUrgente(final Boolean isUrgente) {
		this.isUrgente = isUrgente;
	}

	/**
	 * Restituisce il destinatario del contributo esterno.
	 * @return destinatario contributo esterno
	 */
	public String getDestinatarioContributoEsterno() {
		return destinatarioContributoEsterno;
	}

	/**
	 * Imposta il destinatario del contributo esterno.
	 * @param destinatarioContributoEsterno
	 */
	public void setDestinatarioContributoEsterno(final String destinatarioContributoEsterno) {
		this.destinatarioContributoEsterno = destinatarioContributoEsterno;
	}

	/**
	 * Restituisce il mezzo di ricezione del documento.
	 * @return mezzo ricezione
	 */
	public MezzoRicezioneDTO getMezzoRicezione() {
		return mezzoRicezione;
	}

	/**
	 * Imposta il mezzo di ricezione del documento.
	 * @param mezzoRicezione
	 */
	public void setMezzoRicezione(final MezzoRicezioneDTO mezzoRicezione) {
		this.mezzoRicezione = mezzoRicezione;
	}

	/**
	 * Restituisce true se riservato, false altrimenti.
	 * @return true se riservato, false altrimenti
	 */
	public Boolean getIsRiservato() {
		return isRiservato;
	}

	/**
	 * Imposta il flag associato alla riservatezza del documento.
	 * @param isRiservato
	 */
	public void setIsRiservato(final Boolean isRiservato) {
		this.isRiservato = isRiservato;
	}

	/**
	 * Restituisce l'id dell'ufficio creatore.
	 * @return id ufficio creatore
	 */
	public Long getIdUfficioCreatore() {
		return idUfficioCreatore;
	}

	/**
	 * Imposta l'id dell'ufficio creatore.
	 * @param idUfficioCreatore
	 */
	public void setIdUfficioCreatore(final Long idUfficioCreatore) {
		this.idUfficioCreatore = idUfficioCreatore;
	}

	/**
	 * Restituisce l'id dell'utente creatore.
	 * @return id utente creatore
	 */
	public Long getIdUtenteCreatore() {
		return idUtenteCreatore;
	}

	/**
	 * Imposta l'id dell'utente coordinatore.
	 * @param idUtenteCreatore
	 */
	public void setIdUtenteCreatore(final Long idUtenteCreatore) {
		this.idUtenteCreatore = idUtenteCreatore;
	}

	/**
	 * Restitusice il flag associato alla corte dei conti.
	 * @return flag corte dei conti
	 */
	public Boolean getFlagCorteConti() {
		return flagCorteConti;
	}

	/**
	 * Imposta il flag associato alla corte dei conti.
	 * @param flagCorteConti
	 */
	public void setFlagCorteConti(final Boolean flagCorteConti) {
		this.flagCorteConti = flagCorteConti;
	}

	/**
	 * Restitusice l'id della raccolta FAD.
	 * @return id raccolta FAD
	 */
	public String getIdRaccoltaFAD() {
		return idRaccoltaFAD;
	}

	/**
	 * Imposta l'id della raccolta FAD.
	 * @param idRaccoltaFAD
	 */
	public void setIdRaccoltaFAD(final String idRaccoltaFAD) {
		this.idRaccoltaFAD = idRaccoltaFAD;
	}

	/**
	 * Restituisce il responsabile copia conforme.
	 * @return responsabile copia conforme
	 */
	public ResponsabileCopiaConformeDTO getResponsabileCopiaConforme() {
		return responsabileCopiaConforme;
	}

	/**
	 * Imposta il responsabile copia conforme.
	 * @param responsabileCopiaConforme
	 */
	public void setResponsabileCopiaConforme(final ResponsabileCopiaConformeDTO responsabileCopiaConforme) {
		this.responsabileCopiaConforme = responsabileCopiaConforme;
	}

	/**
	 * Restituisce true se da protocollare, false altrimenti.
	 * @return true se da protocollare, false altrimenti
	 */
	public Boolean getDaProtocollare() {
		return daProtocollare;
	}

	/**
	 * Imposta il flag associato alla necessita di protocollare il documento.
	 * @param daProtocollare
	 */
	public void setDaProtocollare(final Boolean daProtocollare) {
		this.daProtocollare = daProtocollare;
	}

	/**
	 * Restituisce true se il procedimento è tracciato, false altrimenti.
	 * @return true se il procedimento è tracciato, false altrimenti
	 */
	public Boolean getTracciaProcedimento() {
		return tracciaProcedimento;
	}

	/**
	 * Imposta il flag associato alla necessita di tracciamento del procedimento.
	 * @param tracciaProcedimento
	 */
	public void setTracciaProcedimento(final Boolean tracciaProcedimento) {
		this.tracciaProcedimento = tracciaProcedimento;
	}

	/**
	 * @return numDocDestIntUscita
	 */
	public Integer getNumDocDestIntUscita() {
		return numDocDestIntUscita;
	}

	/**
	 * @param numDocDestIntUscita
	 */
	public void setNumDocDestIntUscita(final Integer numDocDestIntUscita) {
		this.numDocDestIntUscita = numDocDestIntUscita;
	}

	/**
	 * Restituisce la lista degli allacci.
	 * @return lista allacci
	 */
	public List<RispostaAllaccioDTO> getAllacci() {
		return allacci;
	}

	/**
	 * Imposta la lista degli allacci.
	 * @param allacci
	 */
	public void setAllacci(final List<RispostaAllaccioDTO> allacci) {
		this.allacci = allacci;
	}

	/**
	 * Restituisce la lista delle assegnazioni.
	 * @return lista assegnazioni
	 */
	public List<AssegnazioneDTO> getAssegnazioni() {
		return assegnazioni;
	}

	/**
	 * Imposta la lista delle assegnazioni.
	 * @param assegnazioni
	 */
	public void setAssegnazioni(final List<AssegnazioneDTO> assegnazioni) {
		this.assegnazioni = assegnazioni;
	}

	/**
	 * Restituisce la lista dei destinatari.
	 * @return lista destinatari
	 */
	public List<DestinatarioRedDTO> getDestinatari() {
		return destinatari;
	}

	/**
	 * Impsta la lista dei destinatari.
	 * @param destinatari
	 */
	public void setDestinatari(final List<DestinatarioRedDTO> destinatari) {
		this.destinatari = destinatari;
	}

	/**
	 * Restituisce gli allegati del documento.
	 * @return allegati
	 */
	public List<AllegatoDTO> getAllegati() {
		return allegati;
	}

	/**
	 * Imposta gli allegati del documento.
	 * @param allegati
	 */
	public void setAllegati(final List<AllegatoDTO> allegati) {
		this.allegati = allegati;
	}

	/**
	 * Restituisce la lista dei contributi.
	 * @return lista contributi
	 */
	public List<ContributoDTO> getContributi() {
		return contributi;
	}

	/**
	 * Imposta la lista dei contributi.
	 * @param contributi
	 */
	public void setContributi(final List<ContributoDTO> contributi) {
		this.contributi = contributi;
	}

	/**
	 * Restituisce la lista dei fascicoli.
	 * @return lista fascicoli
	 */
	public List<FascicoloDTO> getFascicoli() {
		return fascicoli;
	}

	/**
	 * Imposta la lista dei fascicoli.
	 * @param fascicoli
	 */
	public void setFascicoli(final List<FascicoloDTO> fascicoli) {
		this.fascicoli = fascicoli;
	}

	/**
	 * Restituisce la lista dei faldoni.
	 * @return lista faldoni
	 */
	public Collection<FaldoneDTO> getFaldoni() {
		return faldoni;
	}

	/**
	 * Imposta la lista dei faldoni.
	 * @param faldoni
	 */
	public void setFaldoni(final Collection<FaldoneDTO> faldoni) {
		this.faldoni = faldoni;
	}

	/**
	 * Restituisce il fascicolo procedimentale.
	 * @return fascicolo procedimentale
	 */
	public FascicoloDTO getFascicoloProcedimentale() {
		return fascicoloProcedimentale;
	}

	/**
	 * Imposta il fascicolo procedimentale.
	 * @param fascicoloProcedimentale
	 */
	public void setFascicoloProcedimentale(final FascicoloDTO fascicoloProcedimentale) {
		this.fascicoloProcedimentale = fascicoloProcedimentale;
	}

	/**
	 * Restituisce le approvazioni.
	 * @return lista approvazioni
	 */
	public List<ApprovazioneDTO> getApprovazioni() {
		return approvazioni;
	}

	/**
	 * Imposta la lista delle approvazioni.
	 * @param approvazioni
	 */
	public void setApprovazioni(final List<ApprovazioneDTO> approvazioni) {
		this.approvazioni = approvazioni;
	}

	/**
	 * Restituisce i metadati dinamici del documento.
	 * @return metadati dinamici
	 */
	public Map<String, Object> getMetadatiDinamici() {
		return metadatiDinamici;
	}

	/**
	 * Imposta i metadati dinamici del documento.
	 * @param metadatiDinamici
	 */
	public void setMetadatiDinamici(final Map<String, Object> metadatiDinamici) {
		this.metadatiDinamici = metadatiDinamici;
	}
	
	/**
	 * Restituisce i metadati estesi del documento.
	 * @return metadati estesi
	 */
	public Collection<MetadatoDTO> getMetadatiEstesi() {
		return metadatiEstesi;
	}

	/**
	 * Imposta i metadati estesi del documento.
	 * @param metadatiEstesi
	 */
	public void setMetadatiEstesi(final Collection<MetadatoDTO> metadatiEstesi) {
		this.metadatiEstesi = metadatiEstesi;
	}

	/**
	 * Restituisce la map dei template del documento in uscita.
	 * @return la map dei template del documento in uscita
	 */
	public Map<TemplateValueDTO, List<TemplateMetadatoValueDTO>> getTemplateDocUscitaMap() {
		return templateDocUscitaMap;
	}

	/**
	 * Imposta la map dei template del documento in uscita.
	 * @param templateDocUscitaMap
	 */
	public void setTemplateDocUscitaMap(final Map<TemplateValueDTO, List<TemplateMetadatoValueDTO>> templateDocUscitaMap) {
		this.templateDocUscitaMap = templateDocUscitaMap;
	}

	/**
	 * Restituisce i guid dei documenti cartacei.
	 * @return guidDocumentiCartacei
	 */
	public NodoDTO<String> getGuidDocumentiCartacei() {
		return guidDocumentiCartacei;
	}

	/**
	 * @param guidDocumentiCartacei
	 */
	public void setGuidDocumentiCartacei(final NodoDTO<String> guidDocumentiCartacei) {
		this.guidDocumentiCartacei = guidDocumentiCartacei;
	}

	/**
	 * Restituisce l'anno protocollo emergenza.
	 * @return anno protocollo emergenza
	 */
	public Integer getAnnoProtocolloEmergenza() {
		return annoProtocolloEmergenza;
	}

	/**
	 * Restituisce il numero protocollo emergenza.
	 * @return numero protocollo emergenza
	 */
	public Integer getNumeroProtocolloEmergenza() {
		return numeroProtocolloEmergenza;
	}
	
	/**
	 * Imposta l'anno protocollo emergenza.
	 * @param annoProtocolloEmergenza
	 */
	public void setAnnoProtocolloEmergenza(final Integer annoProtocolloEmergenza) {
		this.annoProtocolloEmergenza = annoProtocolloEmergenza;
	}
	
	/**
	 * Restituisce la data protocollo emergenza.
	 * @return data protocollo emergenza
	 */
	public Date getDataProtocolloEmergenza() {
		return dataProtocolloEmergenza;
	}
	
	/**
	 * Imposta il numero protocollo emergenza.
	 * @param numeroProtocolloEmergenza
	 */
	public void setNumeroProtocolloEmergenza(final Integer numeroProtocolloEmergenza) {
		this.numeroProtocolloEmergenza = numeroProtocolloEmergenza;
	}

	/**
	 * Imposta la data protocollo emergenza.
	 * @param dataProtocolloEmergenza
	 */
	public void setDataProtocolloEmergenza(final Date dataProtocolloEmergenza) {
		this.dataProtocolloEmergenza = dataProtocolloEmergenza;
	}

	/**
	 * Restituisce la descrizione dell'indice di classificazione del fascicolo procedimentale.
	 * @return descrizione indice di classificazione fascicolo procedimentale
	 */
	public String getDescrizioneIndiceClassificazioneFascicoloProcedimentale() {
		return descrizioneIndiceClassificazioneFascicoloProcedimentale;
	}

	/**
	 * Imposta la descrizione dell'indice di classificazione del fascicolo procedimentale.
	 * @param descrizioneIndiceClassificazioneFascicoloProcedimentale
	 */
	public void setDescrizioneIndiceClassificazioneFascicoloProcedimentale(final String descrizioneIndiceClassificazioneFascicoloProcedimentale) {
		this.descrizioneIndiceClassificazioneFascicoloProcedimentale = descrizioneIndiceClassificazioneFascicoloProcedimentale;
	}

	/**
	 * Restituisce il content note da mail.
	 * @return contentNoteDaMailProtocollaDTO
	 */
	public NotaDTO getContentNoteDaMailProtocollaDTO() {
		return contentNoteDaMailProtocollaDTO;
	}

	/**
	 * Imposta il content note da mail.
	 * @param contentNoteDaMailProtocollaDTO
	 */
	public void setContentNoteDaMailProtocollaDTO(final NotaDTO contentNoteDaMailProtocollaDTO) {
		this.contentNoteDaMailProtocollaDTO = contentNoteDaMailProtocollaDTO;
	}

	/**
	 * Restitusce il preassegnatario da mail protocolla interoperabile.
	 * @return preassegnatarioDaMailProtocollaInterop
	 */
	public AssegnatarioInteropDTO getPreassegnatarioDaMailProtocollaInterop() {
		return preassegnatarioDaMailProtocollaInterop;
	}

	/**
	 * @param preassegnatarioDaMailProtocollaInterop
	 */
	public void setPreassegnatarioDaMailProtocollaInterop(final AssegnatarioInteropDTO preassegnatarioDaMailProtocollaInterop) {
		this.preassegnatarioDaMailProtocollaInterop = preassegnatarioDaMailProtocollaInterop;
	}

	/**
	 * Restituisce il numero documento.
	 * @return numero documento
	 */
	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * Imposta il numero documento.
	 * @param numeroDocumento
	 */
	public void setNumeroDocumento(final Integer numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * Restituisce il codice flusso.
	 * @return codice flusso
	 */
	public String getCodiceFlusso() {
		return codiceFlusso;
	}

	/**
	 * Imposta il codice flusso.
	 * @param codiceFlusso
	 */
	public void setCodiceFlusso(final String codiceFlusso) {
		this.codiceFlusso = codiceFlusso;
	}
	
	/**
	 * Restituisce il decreto liquidazione Sicoge.
	 * @return decreto liquidazione Sicoge
	 */
	public String getDecretoLiquidazioneSicoge() {
		return decretoLiquidazioneSicoge;
	}

	/**
	 * Imposta il decreto liquidazione Sicoge.
	 * @param decretoLiquidazioneSicoge
	 */
	public void setDecretoLiquidazioneSicoge(final String decretoLiquidazioneSicoge) {
		this.decretoLiquidazioneSicoge = decretoLiquidazioneSicoge;
	}

	/**
	 * Restituisce la lista degli allacci riferimento storico.
	 * @return riferimento storico Nsd
	 */
	public List<RiferimentoStoricoNsdDTO> getAllacciRifStorico() {
		return allacciRifStorico;
	}

	/**
	 * Imposta la lista degli allacci riferimento storico.
	 * @param allacciRifStorico
	 */
	public void setAllacciRifStorico(final List<RiferimentoStoricoNsdDTO> allacciRifStorico) {
		this.allacciRifStorico = allacciRifStorico;
	}

	/**
	 * @return protocollaEMantieni
	 */
	public boolean getProtocollaEMantieni() {
		return protocollaEMantieni;
	}

	/**
	 * @param protocollaEMantieni
	 */
	public void setProtocollaEMantieni(final boolean protocollaEMantieni) {
		this.protocollaEMantieni = protocollaEMantieni;
	}

	/**
	 * Restituisce il mittente mail protocollazione.
	 * @return mittente mail
	 */
	public String getMittenteMailProt() {
		return mittenteMailProt;
	}

	/**
	 * Imposta il mittente mail Prot.
	 * @param mittenteMailProt
	 */
	public void setMittenteMailProt(final String mittenteMailProt) {
		this.mittenteMailProt = mittenteMailProt;
	}

	/**
	 * Restituisce true se assegnaizone indiretta, false altrimenti.
	 * @return true se assegnaizone indiretta, false altrimenti
	 */
	public Boolean isAssegnazioneIndiretta() {
		return assegnazioneIndiretta;
	}

	/**
	 * Imposta il flag associato ad un'assegnazione indiretta.
	 * @param assegnazioneIndiretta
	 */
	public void setAssegnazioneIndiretta(final Boolean assegnazioneIndiretta) {
		this.assegnazioneIndiretta = assegnazioneIndiretta;
	}

	/**
	 * Restituisce gli allegati non sbustati.
	 * @return allegati non sbustati
	 */
	public List<AllegatoDTO> getAllegatiNonSbustati() {
		return allegatiNonSbustati;
	}
	
	/**
	 * Imposta la lista degli allegati non sbustati.
	 * @param allegatiNonSbustati
	 */
	public void setAllegatiNonSbustati(final List<AllegatoDTO> allegatiNonSbustati) {
		this.allegatiNonSbustati = allegatiNonSbustati;
	}
	
	/**
	 * Restituisce la sottocategoria del documento uscita.
	 * @return sottocategoria documento
	 */
	public SottoCategoriaDocumentoUscitaEnum getSottoCategoriaUscita() {
		return sottoCategoriaUscita;
	}

	/**
	 * Imposta la sottocategoria del documento in uscita.
	 * @param sottoCategoriaUscita
	 */
	public void setSottoCategoriaUscita(final SottoCategoriaDocumentoUscitaEnum sottoCategoriaUscita) {
		this.sottoCategoriaUscita = sottoCategoriaUscita;
	}

	/**
	 * Restituisce l'id del registro ausiliario associato al documento.
	 * @return id registro ausiliario
	 */
	public Integer getIdRegistroAusiliario() {
		return idRegistroAusiliario;
	}

	/**
	 * Imposta l'id del registro ausiliario associato al documento.
	 * @param idRegistroAusiliario
	 */
	public void setIdRegistroAusiliario(final Integer idRegistroAusiliario) {
		this.idRegistroAusiliario = idRegistroAusiliario;
	}

	/**
	 * Restituisce la lista dei metadati estesi del registro ausiliario associato al documento.
	 * @return metadati estesi registro ausiliario
	 */
	public Collection<MetadatoDTO> getMetadatiRegistroAusiliario() {
		return metadatiRegistroAusiliario;
	}
	
	/**
	 * Imposta la lista dei metadati estesi del registro ausiliario associato al documento.
	 * @param metadatiRegistroAusiliario
	 */
	public void setMetadatiRegistroAusiliario(final Collection<MetadatoDTO> metadatiRegistroAusiliario) {
		this.metadatiRegistroAusiliario = metadatiRegistroAusiliario;
	}
	
	/**
	 * Restituisce true se integrazione dati, false altrimenti.
	 * @return true se integrazione dati, false altrimenti
	 */
	public boolean isIntegrazioneDati() {
		return integrazioneDati;
	}

	/**
	 * Imposta il flag associato al documento di tipo: integrazione dati.
	 * @param integrazioneDati
	 */
	public void setIntegrazioneDati(final boolean integrazioneDati) {
		this.integrazioneDati = integrazioneDati;
	}

	/**
	 * Restituisce il protocollo di riferimento.
	 * @return protocollo riferimento
	 */
	public String getProtocolloRiferimento() {
		return protocolloRiferimento;
	}

	/**
	 * Imposta il protocollo di riferimento.
	 * @param protocolloRiferimento
	 */
	public void setProtocolloRiferimento(final String protocolloRiferimento) {
		this.protocolloRiferimento = protocolloRiferimento;
	}

	/**
	 * Restituisce l'identificatore di processo.
	 * @return identificatore processo
	 */
	public String getIdentificatoreProcesso() {
		return identificatoreProcesso;
	}

	/**
	 * Imposta l'identificatore di processo.
	 * @param identificatoreProcesso
	 */
	public void setIdentificatoreProcesso(final String identificatoreProcesso) {
		this.identificatoreProcesso = identificatoreProcesso;
	}

	/**
	 * Restituisce l'account mail di protocollazione.
	 * @return account mail protocollazione
	 */
	public String getProtocollazioneMailAccount() {
		return protocollazioneMailAccount;
	}

	/**
	 * Imposta l'account mail di protocollazione.
	 * @param protocollazioneMailAccount
	 */
	public void setProtocollazioneMailAccount(final String protocollazioneMailAccount) {
		this.protocollazioneMailAccount = protocollazioneMailAccount;
	}

	/**
	 * Restituisce objectIdContattoMittOnTheFly.
	 * @return objectIdContattoMittOnTheFly
	 */
	public String getObjectIdContattoMittOnTheFly() {
		return objectIdContattoMittOnTheFly;
	}

	/**
	 * @param objectIdContattoMittOnTheFly
	 */
	public void setObjectIdContattoMittOnTheFly(final String objectIdContattoMittOnTheFly) {
		this.objectIdContattoMittOnTheFly = objectIdContattoMittOnTheFly;
	}

	/**
	 * @return objectIdContattoDestOnTheFly
	 */
	public String getObjectIdContattoDestOnTheFly() {
		return objectIdContattoDestOnTheFly;
	}

	/**
	 * @param objectIdContattoDestOnTheFly
	 */
	public void setObjectIdContattoDestOnTheFly(final String objectIdContattoDestOnTheFly) {
		this.objectIdContattoDestOnTheFly = objectIdContattoDestOnTheFly;
	}

	/**
	 * Restituisce true se in modifica metadati minimi, false altrimenti.
	 * @return true se in modifica metadati minimi, false altrimenti
	 */
	public boolean isModificaMetadatiMinimi() {
		return modificaMetadatiMinimi;
	}
	
	/**
	 * Restituisce la data storico.
	 * @return data storico
	 */
	public Date getDataScarico() {
		return dataScarico;
	}
	
	/**
	 * Imposta il flag associato alla modifica dei metadati minimi.
	 * @param modificaMetadatiMinimi
	 */
	public void setModificaMetadatiMinimi(final boolean modificaMetadatiMinimi) {
		this.modificaMetadatiMinimi = modificaMetadatiMinimi;
	}

	/**
	 * Restitusice l'id del protocollo risposta.
	 * @return id protocollo risposta
	 */
	public String getIdProtocolloRisposta() {
		return idProtocolloRisposta;
	}
	
	/**
	 * Imposta la data storico.
	 * @param dataScarico
	 */
	public void setDataScarico(final Date dataScarico) {
		this.dataScarico = dataScarico;
	}

	/**
	 * Imposta l'id del protocollo risposta.
	 * @param idProtocolloRisposta
	 */
	public void setIdProtocolloRisposta(final String idProtocolloRisposta) {
		this.idProtocolloRisposta = idProtocolloRisposta;
	}


}