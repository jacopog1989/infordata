package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.persistence.model.Widget;

/**
 * Interfaccia DAO gestione dashboard.
 */
public interface IDashboardDAO extends Serializable {

	/**
	 * Utile per salvare le preferenze relative alla scelta dei widget
	 * @param wdgId
	 * @param utenteId
	 * @param connection
	 * @return
	 */
	void salvaPreferenzeWdg(Long wdgId, Long utenteId, Long ruoloUtente, Long posizione, Connection connection);
	
	/**
	 * Utile per rimuovere le preferenze relative alla scelta dei widget
	 * @param wdgId
	 * @param utenteId
	 * @param connection
	 * @return
	 */
	void rimuoviPreferenze(Long wdgId, Long utenteId, Connection connection);
	
	/**
	 * @param utenteId
	 * @param utenteRuolo
	 * @param connection
	 * @return
	 */
	List<Widget> getWidgetPreferenze(Long utenteId, Long utenteRuolo, Connection connection);
	
	/**
	 * @param idRuolo
	 * @param connection
	 * @return
	 */
	List<Widget> getWidgetRuolo(Long idRuolo, Connection connection);

	/**
	 * @param utenteId
	 * @param ruoloId
	 * @param connection
	 * @return
	 */
	void rimuoviPreferenzeWidget(Long utenteId, Long ruoloId, Connection connection); 
}
