package it.ibm.red.business.service.concrete;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.activation.DataHandler;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.Document;
import com.filenet.api.property.FilterElement;
import com.filenet.api.property.PropertyFilter;
import com.google.common.net.MediaType;

import filenet.vw.api.VWException;
import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.constants.Constants.MotivoAssegnazione;
import it.ibm.red.business.constants.Constants.Varie;
import it.ibm.red.business.dao.IAllaccioDAO;
import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.IContributoDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO;
import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.DetailAssegnazioneDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EventoLogDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.DocumentoRedFnDTO;
import it.ibm.red.business.dto.filenet.FascicoloRedFnDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.ContributoEsternoModificabileEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.StatoContributoEnum;
import it.ibm.red.business.enums.StatoContributoEsternoEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contributo;
import it.ibm.red.business.persistence.model.DEventiCustom;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAttiSRV;
import it.ibm.red.business.service.IContributoSRV;
import it.ibm.red.business.service.IDocumentoSRV;
import it.ibm.red.business.service.IEventoLogSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.INotificaPostEliminazioneSRV;
import it.ibm.red.business.service.IOperationDocumentSRV;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.service.IStoricoSRV;
import it.ibm.red.business.service.ITipoProcedimentoSRV;
import it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.OrganigrammaRetrieverUtils;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class ContributoSRV.
 *
 * @author CPIERASC
 * 
 *         Servizio gestione utenti.
 */
@Service
public class ContributoSRV extends AbstractService implements IContributoSRV {

	/**
	 * Label documento, occorre appendere il riferimento al documento dopo il
	 * messaggio.
	 */
	private static final String PER_DOCUMENTO_LITERAL = " per il documento: ";

	/**
	 * Definizione "esterno".
	 */
	private static final String ESTERNO_LITERAL = " esterno";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContributoSRV.class.getName());

	/**
	 * DAO gestione contributi.
	 */
	@Autowired
	private IContributoDAO contributoDAO;

	/**
	 * Dao gestione aoo.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * Dao gestione nodo.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Dao gestione utente.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * Dao per i documenti allacciati.
	 */
	@Autowired
	private IAllaccioDAO allaccioDAO;

	/**
	 * Service gestione documenti (per il recupero del content nel caso non fosse
	 * generato a partire dalla nota).
	 */
	@Autowired
	private IDocumentoSRV documentoSRV;

	/**
	 * Servizio per la gestione delle security.
	 */
	@Autowired
	private ISecuritySRV securitySRV;

	/**
	 * Servizio per la gestione dei tipi di procedimento.
	 */
	@Autowired
	private ITipoProcedimentoSRV tipoProcedimentoSRV;

	/**
	 * Servizio per la gestione di operazioni sul documento.
	 */
	@Autowired
	private IOperationDocumentSRV operationDocumentSRV;

	/**
	 * Servizio per la gestione di operazioni sul workflow.
	 */
	@Autowired
	private IOperationWorkFlowFacadeSRV operationWorkflowSRV;

	/**
	 * Servizio per il recupero dello storico.
	 */
	@Autowired
	private IStoricoSRV storicoSRV;

	/**
	 * Servizio per la gestione dei fascicoli.
	 */
	@Autowired
	private IFascicoloSRV fascicoloSRV;

	/**
	 * Servizio per la gestione della messa agli atti.
	 */
	@Autowired
	private IAttiSRV attiSRV;

	/**
	 * Servizio per la gestione degli eventi.
	 */
	@Autowired
	private IEventoLogSRV eventoLogSRV;

	/**
	 * Servizio per la gestione delle notifiche.
	 */
	@Autowired
	private INotificaPostEliminazioneSRV notificaPostElimContributoSRV;

	/**
	 * Serialization.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @see it.ibm.red.business.service.facade.IContributoFacadeSRV#getContributi(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	public List<ContributoDTO> getContributi(final UtenteDTO utente, final String documentTitle) {
		List<ContributoDTO> contributi = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			contributi = getContributi(fceh, documentTitle, utente.getIdAoo());
		} finally {
			popSubject(fceh);
		}

		return contributi;
	}

	/**
	 * Gets the contributi.
	 *
	 * @param fceh          the fceh
	 * @param documentTitle the document title
	 * @return the contributi
	 */
	@Override
	public final List<ContributoDTO> getContributi(final IFilenetCEHelper fceh, final String documentTitle, final Long idAoo) {
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			return getContributi(fceh, documentTitle, idAoo, connection);
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Gets the contributi.
	 *
	 * @param fceh          the fceh
	 * @param documentTitle the document title
	 * @param connection    the connection
	 * @return the contributi
	 */
	@Override
	public final List<ContributoDTO> getContributi(final IFilenetCEHelper fceh, final String documentTitle, final Long idAoo, final Connection connection) {
		final Collection<Contributo> contributi = contributoDAO.getContributiInterni(documentTitle, idAoo, connection);
		contributi.addAll(contributoDAO.getContributiEsterni(documentTitle, idAoo, connection));
		return loadContributi(fceh, contributi, connection);
	}

	/**
	 * Metodo per il caricamento dei contributi.
	 * 
	 * Attenzione se non è presente il file (guid null e nomeFile null) il content
	 * viene visualizzato con il documento di default
	 * 
	 * @param fce        helper gestione ce
	 * @param contributi model contributi
	 * @param connection connessione al database
	 * @return lista dei contributi
	 */
	private List<ContributoDTO> loadContributi(final IFilenetCEHelper fce, final Collection<Contributo> contributi, final Connection connection) {
		final List<ContributoDTO> output = new ArrayList<>();
		ContributoDTO c = null;

		for (final Contributo contributo : contributi) {
			String filename = ContributoDTO.getNomeNotaPdf(contributo.getIdContributo());
			String mimeType = MediaType.PDF.toString();

			// Contributo Interno
			if (Boolean.FALSE.equals(contributo.getFlagEsterno())) {
				final String documentTitle = contributo.getGuid();
				if (!StringUtils.isNullOrEmpty(documentTitle) && org.apache.commons.lang3.StringUtils.isNotBlank(contributo.getNomeFile())) {
					final Document doc = fce.getDocumentForContributo(documentTitle);
					filename = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NOME_FILE_METAKEY);
					mimeType = doc.get_MimeType();
				}

				final Utente utente = utenteDAO.getUtente(contributo.getIdUtente(), connection);
				final String descUtente = utente.getNome() + " " + utente.getCognome();
				final String descUfficio = nodoDAO.getNodo(contributo.getIdUfficio(), connection).getDescrizione();
				final String descAoo = aooDAO.getAoo(contributo.getIdAoo(), connection).getDescrizione();
				final String dataContributo = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(contributo.getDataContributo());
				c = new ContributoDTO(contributo.getIdContributo(), dataContributo, descUtente, descUfficio, descAoo, contributo.getFlagEsterno(), filename, mimeType,
						documentTitle, contributo.getNota(), contributo.getIdUtente(), contributo.getGuid(), contributo.getIdDocumento().intValue());

				// Contributo Esterno
			} else {
				final String guid = contributo.getGuid();
				String documentTitle = null;
				Document doc = null;
				if (!StringUtils.isNullOrEmpty(guid) && !"-".equals(guid)) {
					doc = fce.getDocumentByGuid(guid);
					documentTitle = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);

				} else {
					documentTitle = contributo.getIdContributo().toString();
					doc = fce.getDocumentByDTandAOO(documentTitle, contributo.getIdAoo());
					contributo.setGuid(doc.get_Id().toString());
				}

				filename = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NOME_FILE_METAKEY);
				mimeType = doc.get_MimeType();

				final String dataContributo = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(contributo.getDataContributo());
				c = new ContributoDTO(contributo.getIdContributo(), dataContributo, contributo.getProtocollo(), contributo.getFlagEsterno(), filename, mimeType, documentTitle,
						contributo.getNota(), contributo.getIdUtente(), contributo.getGuid(), contributo.getMittente(),
						ContributoEsternoModificabileEnum.MODIFICABILE.getId().equals(contributo.getModificabile()), contributo.getIdDocumento().intValue(),
						contributo.getValiditaFirma());

				// Va avviato il controllo di firma solo su file PDF e P7M
				if (MediaType.PDF.toString().equalsIgnoreCase(mimeType) || ContentType.P7M.equalsIgnoreCase(mimeType) || ContentType.P7M_X_MIME.equalsIgnoreCase(mimeType)
						|| ContentType.P7M_MIME.equalsIgnoreCase(mimeType)) {
					c.setVisualizzaInfoVerificaFirma(true);
				}
			}

			output.add(c);
		}

		return output;
	}

	@Override
	public final FileDTO getContent(final UtenteDTO utente, final Boolean flagEsterno, final Long id) {
		FileDTO output;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final ContributoDTO contributo = contributoDAO.get(id, connection, flagEsterno);

			if (StringUtils.isNullOrEmpty(contributo.getDocumentTitle())) {
				LOGGER.info("Documento non presente, procedo alla creazione della nota on the fly");

				output = getPDFNotaOFT(utente, flagEsterno, id, connection);

			} else {
				LOGGER.info("Documento presente, procedo al recupero del content da FileNet");

				if (Boolean.FALSE.equals(flagEsterno)) {
					// In questo caso il document title del contributo è il realmente il document
					// title del documento su FileNet
					LOGGER.info("Recupero file esterno");

					output = documentoSRV.getContributoContentPreview(utente.getFcDTO(), contributo.getDocumentTitle(), utente.getIdAoo());
				} else {
					// In questo caso il document title del contributo è il GUID del documento su
					// FileNet
					final String documentTitle = fromGuidToDocumentTitle(utente.getFcDTO(), contributo.getDocumentTitle(), utente.getIdAoo());
					LOGGER.info("Recupero file interno passando dal guid " + contributo.getDocumentTitle() + "al " + documentTitle);

					output = documentoSRV.getContributoContentPreview(utente.getFcDTO(), documentTitle, utente.getIdAoo());
				}
			}
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return output;
	}

	@Override
	public final FileDTO getPDFNotaOFT(final UtenteDTO utente, final Boolean flagEsterno, final Long id) {
		FileDTO output;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			output = getPDFNotaOFT(utente, flagEsterno, id, connection);
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return output;
	}

	/**
	 * Generazione PDF nota on the fly.
	 * 
	 * @param utente      credenziali utente
	 * @param flagEsterno a true indica che il contributo è esterno
	 * @param id          identificativo contributo
	 * @return file
	 */
	private FileDTO getPDFNotaOFT(final UtenteDTO utente, final Boolean flagEsterno, final Long id, final Connection connection) {
		FileDTO output;

		final ContributoDTO contributo = contributoDAO.get(id, connection, flagEsterno);

		final Integer idDocumento = fromDocumentTitleToIdDocumento(utente, contributo.getIdDocumento().toString());
		if (Boolean.TRUE.equals(flagEsterno)) {
			LOGGER.info("Recupero nota esterna");
			output = contributoDAO.getNotaContributoEsternoContent(id, connection, idDocumento);
		} else {
			LOGGER.info("Recupero nota interna");
			output = contributoDAO.getNotaContributoInternoContent(id, connection, idDocumento);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.IContributoSRV#insertContributoInterno(java.lang.Long,
	 *      java.lang.Long, java.lang.Long, java.lang.Long, java.lang.String,
	 *      java.lang.String, java.util.Date, int, int, int, java.sql.Connection).
	 */
	@Override
	public Long insertContributoInterno(final Long idUtente, final Long idUfficio, final Long idAoo, final Long idDocumento, final String nota, final String nomeFile,
			final Date dataCdC, final int foglio, final int registro, final int stato, final Connection con) {
		final Contributo contributo = new Contributo(null, idUtente, idUfficio, idAoo, idDocumento, nota, nomeFile, null, null, DateUtils.dateToString(dataCdC, true), foglio,
				registro, stato);
		return contributoDAO.insertContributoInterno(contributo, con);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IContributoFacadeSRV#richiediContributoInterno(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.util.List, java.lang.String, java.lang.Boolean).
	 */
	@Override
	public final EsitoOperazioneDTO richiediContributoInterno(final UtenteDTO utente, final String wobNumber,
			final List<AssegnatarioOrganigrammaDTO> inputAssegnazioniContributo, final String motivazioneAssegnazione, final Boolean isRichiestaUrgente) {
		return gestisciRichiediContributo(utente, wobNumber, ResponsesRedEnum.RICHIEDI_CONTRIBUTO_INTERNO, inputAssegnazioniContributo, motivazioneAssegnazione,
				isRichiestaUrgente, true, DocumentQueueEnum.CORRIERE);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IContributoFacadeSRV#richiediContributoUtente(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.util.List, java.lang.String, java.lang.Boolean).
	 */
	@Override
	public final EsitoOperazioneDTO richiediContributoUtente(final UtenteDTO utente, final String wobNumber,
			final List<AssegnatarioOrganigrammaDTO> inputAssegnazioniContributo, final String motivazioneAssegnazione, final Boolean isRichiestaUrgente) {
		return gestisciRichiediContributo(utente, wobNumber, ResponsesRedEnum.RICHIEDI_CONTRIBUTO_UTENTE, inputAssegnazioniContributo, motivazioneAssegnazione,
				isRichiestaUrgente, false, null);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IContributoFacadeSRV#richiediContributo(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.util.List, java.lang.String, java.lang.Boolean,
	 *      java.lang.Integer, java.lang.Integer).
	 */
	@Override
	public final EsitoOperazioneDTO richiediContributo(final UtenteDTO utente, final String wobNumber, final List<AssegnatarioOrganigrammaDTO> inputAssegnazioniContributo,
			final String motivazioneAssegnazione, final Boolean isRichiestaUrgente, final Integer annoProtocollo, final Integer numProtocollo) {
		return gestisciRichiediContributo(utente, wobNumber, ResponsesRedEnum.RICHIEDI_CONTRIBUTO, inputAssegnazioniContributo, motivazioneAssegnazione, isRichiestaUrgente,
				annoProtocollo, numProtocollo, false, null);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IContributoFacadeSRV#richiediContributo(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.util.List, java.lang.String, java.lang.Boolean).
	 */
	@Override
	public final EsitoOperazioneDTO richiediContributo(final UtenteDTO utente, final String wobNumber, final List<AssegnatarioOrganigrammaDTO> inputAssegnazioniContributo,
			final String motivazioneAssegnazione, final Boolean isRichiestaUrgente) {
		return gestisciRichiediContributo(utente, wobNumber, ResponsesRedEnum.RICHIEDI_CONTRIBUTO, inputAssegnazioniContributo, motivazioneAssegnazione, isRichiestaUrgente,
				false, null);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IContributoFacadeSRV#richiestaContributo(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.util.List, java.lang.String, java.lang.Boolean).
	 */
	@Override
	public final EsitoOperazioneDTO richiestaContributo(final UtenteDTO utente, final String wobNumber, final List<AssegnatarioOrganigrammaDTO> inputAssegnazioniContributo,
			final String motivazioneAssegnazione, final Boolean isRichiestaUrgente) {
		return gestisciRichiediContributo(utente, wobNumber, ResponsesRedEnum.RICHIESTA_CONTRIBUTO, inputAssegnazioniContributo, motivazioneAssegnazione, isRichiestaUrgente,
				false, null);
	}

	private EsitoOperazioneDTO gestisciRichiediContributo(final UtenteDTO utente, final String wobNumber, final ResponsesRedEnum response,
			final List<AssegnatarioOrganigrammaDTO> inputAssegnazioniContributo, final String motivazioneAssegnazione, final Boolean isRichiestaUrgente,
			final boolean verificaConcorrenza, final DocumentQueueEnum codaDaVericare) {
		return gestisciRichiediContributo(utente, wobNumber, response, inputAssegnazioniContributo, motivazioneAssegnazione, isRichiestaUrgente, null, null,
				verificaConcorrenza, codaDaVericare);
	}

	private EsitoOperazioneDTO gestisciRichiediContributo(final UtenteDTO utente, final String wobNumber, final ResponsesRedEnum response,
			final List<AssegnatarioOrganigrammaDTO> inputAssegnazioniContributo, final String motivazioneAssegnazione, final Boolean isRichiestaUrgente,
			final Integer annoProtocollo, final Integer numProtocollo, final boolean verificaConcorrenza, final DocumentQueueEnum codaDaVericare) {
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		Connection connection = null;
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;
		final PropertiesProvider pp = PropertiesProvider.getIstance();

		try {
			boolean isStoricoPresente = false;
			connection = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);

			if (verificaConcorrenza && codaDaVericare != null) {
				final boolean isPresenteInCoda = fpeh.isWobInCoda(wobNumber, codaDaVericare);
				if (!isPresenteInCoda) {
					final String errore = "Il documento risulta già assegnato per contributo";
					LOGGER.error(errore);
					throw new RedException(errore);
				}
			}

			final Integer idDocumento = (Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));

			// Si ottengono le attuali assegnazioni per contributo
			final String[] assegnazioniContributoCorrenti = (String[]) TrasformerPE.getMetadato(wob,
					pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONTRIBUTI_STORICO_METAKEY));
			if (assegnazioniContributoCorrenti != null) {
				isStoricoPresente = true;
			}

			if (idDocumento != null && CollectionUtils.isNotEmpty(inputAssegnazioniContributo)) {
				final String idDocumentoString = String.valueOf(idDocumento);

				// Si trasforma la lista delle nuove assegnazioni per contributo in array
				final String[] inputAssegnazioniContributoArray = OrganigrammaRetrieverUtils.initArrayAssegnazioni(inputAssegnazioniContributo);

				// ### Si verifica se gli utenti assegnatari hanno già una richiesta di
				// contributo per questo documento
				final boolean hasRichiestaContributo = hasRichiestaContributo(idDocumentoString, inputAssegnazioniContributo, fpeh, utente);

				if (!hasRichiestaContributo) {
					// ### Si modificano le security dei fascicoli in cui è presente il documento
					final boolean esitoModificaSecurityFascicoli = securitySRV.modificaSecurityFascicoli(idDocumentoString, utente, inputAssegnazioniContributoArray,
							connection);

					if (esitoModificaSecurityFascicoli) {
						// ### Si uniscono le assegnazioni per contributo correnti con le nuove in input
						String[] allAssegnazioniContributo = new String[0];
						if (assegnazioniContributoCorrenti != null && !StringUtils.isNullOrEmpty(assegnazioniContributoCorrenti[0])) {
							allAssegnazioniContributo = ArrayUtils.addAll(assegnazioniContributoCorrenti, inputAssegnazioniContributoArray);
						} else {
							allAssegnazioniContributo = inputAssegnazioniContributoArray;
						}

						final DetailAssegnazioneDTO detailDocument = securitySRV.getDocumentDetailAssegnazione(fceh, utente.getIdAoo(), idDocumentoString, false);
						final boolean isRiservato = Boolean.TRUE.equals(detailDocument.isRiservato());

						// ### Si ottengono le nuove security...
						final ListSecurityDTO securityDoc = securitySRV.getSecurityPerAddAssegnazione(utente, idDocumentoString, utente.getId(),
								utente.getIdUfficio(), inputAssegnazioniContributoArray, true, false, isRiservato, connection);

						// ### ...e si usano per aggiornare le security del documento
						securitySRV.aggiornaEVerificaSecurityDocumento(idDocumentoString, utente.getIdAoo(), securityDoc, false, false, fceh, connection);

						final Map<String, Object> metadatiWorkflow = new HashMap<>();
						metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONTRIBUTI_METAKEY), inputAssegnazioniContributoArray);
						metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivazioneAssegnazione);
						metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId());
						if (isStoricoPresente) {
							metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONTRIBUTI_STORICO_METAKEY), allAssegnazioniContributo);
						}
						if (Boolean.TRUE.equals(isRichiestaUrgente)) {
							metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY), 1);
						}

						// ### Si avanza il workflow su FileNet
						final boolean avanzamentoWorkflowOk = fpeh.nextStep(wobNumber, metadatiWorkflow, response.getResponse());

						if (avanzamentoWorkflowOk) {
							// Si aggiornano le security dei documenti allacciati
							securitySRV.aggiornaSecurityDocumentiAllacciati(utente, idDocumento, securityDoc, inputAssegnazioniContributoArray, fceh, connection);

							// Si aggiornano le security dei contributi (interni)
							securitySRV.aggiornaSecurityContributiInseriti(utente, idDocumentoString, inputAssegnazioniContributoArray, fceh, connection);

							// Si aggiornano di tutti i workflow del documento
							metadatiWorkflow.clear();
							metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONTRIBUTI_STORICO_METAKEY), allAssegnazioniContributo);

							// Si aggiorna il metadato con l'elenco dei contributi richiesti in tutti i
							// workflow coinvolti nel giro contributi
							aggiornaWorkflowsContributi(idDocumentoString, metadatiWorkflow, utente.getIdAoo(), utente.getFcDTO().getIdClientAoo(), fpeh, pp);

							esito = new EsitoOperazioneDTO(null, null, detailDocument.getNumeroDocumento(), wobNumber);
							if (annoProtocollo != null && numProtocollo != null) {
								esito.setAnnoProtocollo(annoProtocollo);
								esito.setNumeroProtocollo(numProtocollo);
							}
							esito.setNote("Richiesta contributo effettuata con successo.");
							LOGGER.info("Inserimento di nuovi uffici/utenti in contributo eseguito correttamente per il documento: " + idDocumentoString);
						} else {
							final String errore = "Errore durante l'avanzamento del workflow: " + wobNumber + " con la response: " + response.name() + PER_DOCUMENTO_LITERAL
									+ idDocumentoString;
							LOGGER.error(errore);
							throw new FilenetException(errore);
						}
					}
				} else {
					final String errore = "Il documento risulta già assegnato all'utente per contributo";
					LOGGER.error(errore);
					throw new RedException(errore);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento di nuovi uffici/utenti in contributo per il workflow: " + wobNumber, e);
			// Si genera l'esito in caso di eccezione
			esito.setNote("Errore durante l'inserimento di nuovi uffici/utenti in contributo per il workflow: " + wobNumber + ".\nErrore: " + e.getMessage() + "\n");
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}

		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.IContributoSRV#getAssegnazioniContributi(java.lang.String,
	 *      it.ibm.red.business.helper.filenet.pe.FilenetPEHelper).
	 */
	@Override
	public List<AssegnatarioOrganigrammaDTO> getAssegnazioniContributi(final String wobNumber, final FilenetPEHelper fpeh) {
		final PropertiesProvider pp = PropertiesProvider.getIstance();
		final List<AssegnatarioOrganigrammaDTO> assContributiList = new ArrayList<>();
		try {
			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);

			// Si ottengono le attuali assegnazioni per contributo dallo storico
			assegnatarioOrganigrammaTrasformerFromContributo(pp, assContributiList, wob);
		} catch (final NumberFormatException ne) {
			final String mess = "Uno delle assegnazioni per contributo recuperate dal wobNumner:" + wobNumber + " è malformata";
			LOGGER.error(mess, ne);
			throw new RedException(mess);
		}

		return assContributiList;
	}

	/**
	 * Scrive nell'array eventuali assegnatari organigramma se esistono.
	 * 
	 * @param pp
	 * @param assContributiList
	 * @param wob
	 */
	private void assegnatarioOrganigrammaTrasformerFromContributo(final PropertiesProvider pp, final List<AssegnatarioOrganigrammaDTO> assContributiList,
			final VWWorkObject wob) {
		final String[] assegnazioniContributoCorrenti = (String[]) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONTRIBUTI_STORICO_METAKEY));
		if (CollectionUtils.isNotEmpty(assContributiList)) {
			for (final String assegnazione : assegnazioniContributoCorrenti) {
				final AssegnatarioOrganigrammaDTO assDTO = OrganigrammaRetrieverUtils.create(assegnazione);
				assContributiList.add(assDTO);
			}
		}
	}

	/**
	 * @see it.ibm.red.business.service.IContributoSRV#getAssegnazioniContributiAllaccio(java.lang.Integer,
	 *      it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.helper.filenet.pe.FilenetPEHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public List<AssegnatarioOrganigrammaDTO> getAssegnazioniContributiAllaccio(final Integer idDocumentoPrincipale, final UtenteDTO utente, final FilenetPEHelper fpeh,
			final Connection con) {
		final PropertiesProvider pp = PropertiesProvider.getIstance();
		final List<AssegnatarioOrganigrammaDTO> assContributiList = new ArrayList<>();
		try {
			LOGGER.info("Recupero le risposte allaccio del documento");
			final List<RispostaAllaccioDTO> documentiDaAllacciati = allaccioDAO.getDocumentiRispostaAllaccio(idDocumentoPrincipale, utente.getIdAoo().intValue(), con);
			final List<Long> iddocumentiAllacciatiList = documentiDaAllacciati.stream().map(a -> Long.parseLong(a.getIdDocumentoAllacciato())).collect(Collectors.toList());

			LOGGER.info("Recupero le assegnazioni per i contributi di tutti i workflow degli allacci");
			if (CollectionUtils.isNotEmpty(iddocumentiAllacciatiList)) {
				final VWRosterQuery rQueryNSD = fpeh.getDocumentWorkFlowsByIdDocumenti(iddocumentiAllacciatiList);
				while (rQueryNSD.hasNext()) {
					final VWWorkObject wf = (VWWorkObject) rQueryNSD.next();
					final String oggetto = wf.getSubject();
					// Commento preso da RED
					// se F_subject è 'Richiedi contributo' o si tratta
					// del workflow padre

					final String oggettoPerIntero = pp.getParameterByKey(PropertiesNameEnum.WF_PROCESSO_CONTRIBUTO_WFKEY);
					final String oggettoDaConfrontare = oggettoPerIntero.split("\\|")[0];
					if (oggettoDaConfrontare.equals(oggetto)) {
						assegnatarioOrganigrammaTrasformerFromContributo(pp, assContributiList, wf);
					}
				}
			}

			// Ottieni anche quelle dello storico
			LOGGER.info("Recupero l'assegnazione per il contributo dallo storico di ciascun documento allacciato");
			for (final Long idDocumentoAllacciato : iddocumentiAllacciatiList) {

				final List<DEventiCustom> docStoricoList = storicoSRV.getStoricoDocumento(idDocumentoAllacciato.intValue(), utente.getIdAoo().intValue());
				for (final DEventiCustom storico : docStoricoList) {
					// se è una richiesta di contributo
					if (EventTypeEnum.RICHIESTA_CONTRIBUTO.getValue().equals(storico.getTipoEvento().toString())) {
						final Long idNodoString = storico.getIdNodoDestinatario();
						final Long idUtenteString = storico.getIdUtenteDestinatario();

						final AssegnatarioOrganigrammaDTO assegnatario = new AssegnatarioOrganigrammaDTO(idNodoString, idUtenteString);
						if (assegnatario != null) {
							assContributiList.add(assegnatario);
						}
					}
				}

			}
		} catch (final Exception e) {
			final String messaggio = "Errore nell'ottenere le assegnazioni per contributo dei documenti allacciati.";
			LOGGER.error(messaggio, e);
			throw new RedException(messaggio);
		}

		return assContributiList;
	}

	private static boolean hasRichiestaContributo(final String documentTitle, final List<AssegnatarioOrganigrammaDTO> assegnazioniContributo, final FilenetPEHelper fpeh,
			final UtenteDTO utente) {

		try {
			final ArrayList<Long> idsTipoAssegnazione = new ArrayList<>();
			idsTipoAssegnazione.add(Long.valueOf(TipoAssegnazioneEnum.CONTRIBUTO.getId()));

			final List<DocumentQueueEnum> queues = new ArrayList<>();
			if (utente.isUcb()) {
				queues.add(DocumentQueueEnum.DA_LAVORARE_UCB);
				queues.add(DocumentQueueEnum.IN_LAVORAZIONE_UCB);
			} else {
				queues.add(DocumentQueueEnum.DA_LAVORARE);
			}

			queues.add(DocumentQueueEnum.SOSPESO);

			for (final AssegnatarioOrganigrammaDTO assegnazioneContributo : assegnazioniContributo) {

				for (final DocumentQueueEnum queue : queues) {
					if (fpeh.hasDocumentInCoda(queue, documentTitle, assegnazioneContributo.getIdNodo(), assegnazioneContributo.getIdUtente(), idsTipoAssegnazione, false)) {
						return true;
					}
				}

			}
		} catch (final FilenetException e) {
			LOGGER.error("Attenzione! Errore nella ricerca di una richiesta di contributo per il documento: " + documentTitle, e);
		}

		return false;
	}

	private void aggiornaWorkflowsContributi(final String idDocumento, final Map<String, Object> metadatiContributo, final Long idAoo, final String idClientAoo,
			final FilenetPEHelper fpeh, final PropertiesProvider pp) {
		final String workflowSubjectProcessoContributo = pp.getParameterByKey(PropertiesNameEnum.WF_PROCESSO_CONTRIBUTO_WFKEY).split("\\|")[0];

		// Si ottengono tutti i workflow associati al documento
		final VWRosterQuery rQueryNSD = fpeh.getRosterQueryWorkByIdDocumentoAndCodiceAOO(idDocumento, idClientAoo);

		while (rQueryNSD.hasNext()) {
			final VWWorkObject workflow = (VWWorkObject) rQueryNSD.next();

			final String[] metadatoElencoContributiStorico = (String[]) TrasformerPE.getMetadato(workflow,
					pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONTRIBUTI_STORICO_METAKEY));
			final String workflowSubject = workflow.getSubject();

			// Si procede all'aggiornamento dei metadati del workflow se si verificano
			// entrambe queste condizioni:
			// 1) È già presente il metadato dello storico contributi nel workflow
			// 2) F_subject del workflow è 'Richiedi contributo' o si tratta del workflow
			// padre.
			if (metadatoElencoContributiStorico != null
					&& (workflowSubject != null && (workflowSubjectProcessoContributo.equals(workflowSubject) || isWorkflowPadrePerContributo(workflow, idAoo)))) {
				fpeh.updateWorkflow(workflow, metadatiContributo);
			}
		}
	}

	/**
	 * Getter flag workflow padre per contributo.
	 * 
	 * @param workFlow workflow
	 * @param idAoo    identificativo aoo
	 * @return flag workflow padre per contributo
	 */
	@Override
	public Boolean isWorkflowPadrePerContributo(final VWWorkObject workFlow, final Long idAoo) {
		Boolean output = false;
		String wfSubject = null;

		final String processoGenericoEntrata = tipoProcedimentoSRV.getWorkflowSubjectProcessoGenericoEntrata(idAoo);
		final String processoGenericoUscita = tipoProcedimentoSRV.getWorkflowSubjectProcessoGenericoUscita(idAoo);

		try {
			if (workFlow != null) {
				wfSubject = workFlow.getSubject();
			}
		} catch (final VWException e) {
			LOGGER.error(e);
		}

		if (wfSubject != null && (wfSubject.equals(processoGenericoEntrata) || wfSubject.equals(processoGenericoUscita))) {
			output = true;
		}

		return output;
	}

	@Override
	public final EsitoOperazioneDTO inserisciContributo(final UtenteDTO utente, final String wobNumber, final ContributoDTO contributo) {
		return gestisciInserisciContributoInterno(utente, wobNumber, ResponsesRedEnum.INSERISCI_CONTRIBUTO, contributo);
	}

	@Override
	public final EsitoOperazioneDTO inserisciContributo2(final UtenteDTO utente, final String wobNumber, final ContributoDTO contributo) {
		return gestisciInserisciContributoInterno(utente, wobNumber, ResponsesRedEnum.INSERISCI_CONTRIBUTO_2, contributo);
	}

	@Override
	public final EsitoOperazioneDTO validaContributi(final UtenteDTO utente, final String wobNumber, final ContributoDTO contributo) {
		return gestisciInserisciContributoInterno(utente, wobNumber, ResponsesRedEnum.VALIDA_CONTRIBUTI, contributo);
	}

	private EsitoOperazioneDTO gestisciInserisciContributoInterno(final UtenteDTO utente, final String wobNumber, final ResponsesRedEnum response,
			final ContributoDTO contributo) {
		LOGGER.info(response.name() + " -> START");
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		Connection connection = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		final PropertiesProvider pp = PropertiesProvider.getIstance();

		try {
			if (response.equals(ResponsesRedEnum.VALIDA_CONTRIBUTI) || !StringUtils.isNullOrEmpty(contributo.getNota())
					|| (contributo.getContent() != null && contributo.getMimeType() != null)) {

				fpeh = new FilenetPEHelper(utente.getFcDTO());
				fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

				final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);
				final Integer idDocumento = (Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));

				if (idDocumento != null) {
					connection = setupConnection(getDataSource().getConnection(), true);

					final String idDocumentoString = String.valueOf(idDocumento);

					final DetailAssegnazioneDTO detailDocument = securitySRV.getDocumentDetailAssegnazione(fceh, utente.getIdAoo(), idDocumentoString, false);
					final boolean isRiservato = Boolean.TRUE.equals(detailDocument.isRiservato());

					int statoContributo = 0;
					if (ResponsesRedEnum.VALIDA_CONTRIBUTI.equals(response)) {
						statoContributo = StatoContributoEnum.VALIDATO.getId();
					}
					if (PermessiUtils.isCorteDeiConti(utente.getPermessi())) {
						contributo.setFoglio(contributo.getFoglio() != null && contributo.getFoglio() > 0 ? contributo.getFoglio() : 0);
						contributo.setRegistro(contributo.getRegistro() != null && contributo.getRegistro() > 0 ? contributo.getRegistro() : 0);
					} else {
						contributo.setFoglio(0);
						contributo.setRegistro(0);
					}

					// ### Si inserisce il nuovo contributo nel DB
					final Long idContributo = insertContributoInterno(utente.getId(), utente.getIdUfficio(), utente.getIdAoo(), Long.valueOf(idDocumento),
							contributo.getNota(), contributo.getFilename(), contributo.getDataCdc(), contributo.getFoglio(), contributo.getRegistro(), statoContributo,
							connection);

					final Map<String, Object> metadatiContributo = new HashMap<>();
					// Si procede al salvataggio del documento di contributo su FileNet
					if (contributo.getContent() != null) {
						final DocumentoRedFnDTO docRedFn = new DocumentoRedFnDTO();
						docRedFn.setDocumentTitle(String.valueOf(idContributo));
						docRedFn.setClasseDocumentale(pp.getParameterByKey(PropertiesNameEnum.CONTRIBUTO_CLASSNAME_FN_METAKEY));
						docRedFn.setFolder(pp.getParameterByKey(PropertiesNameEnum.FOLDER_DOCUMENTI_CE_KEY));
						docRedFn.setDataHandler(FileUtils.toDataHandler(contributo.getContent(), contributo.getMimeType()));
						docRedFn.setContentType(contributo.getMimeType());

						final ListSecurityDTO securityContributo = securitySRV.getSecurityPerContributo(utente, String.valueOf(idDocumento), isRiservato,
								connection);
						docRedFn.setSecurity(securityContributo);

						metadatiContributo.put(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), String.valueOf(idContributo));
						metadatiContributo.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), contributo.getFilename());
						metadatiContributo.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), utente.getIdAoo());
						docRedFn.setMetadati(metadatiContributo);

						// ### Si inserisce il documento di contributo su FileNet
						fceh.creaDocumento(docRedFn, null, null, utente.getIdAoo());
						LOGGER.info(response.name() + " -> Inserimento su FileNet del documento di contributo eseguito correttamente per il documento: " + idDocumentoString);

						// ### Si aggiorna il contributo precedentemente inserito nel DB
						contributoDAO.updateGuidContributo(idContributo, docRedFn.getDocumentTitle(), false, connection);
						metadatiContributo.clear();
					}

					String motivoAssegnazione = null;
					if (ResponsesRedEnum.VALIDA_CONTRIBUTI.equals(response)) {
						motivoAssegnazione = MotivoAssegnazione.MOTIVO_VALIDA_CONTRIBUTI;
					} else {
						motivoAssegnazione = MotivoAssegnazione.MOTIVO_INSERISCI_CONTRIBUTO;
					}

					metadatiContributo.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivoAssegnazione);
					metadatiContributo.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId());

					// ### Si avanza il workflow su FileNet
					final boolean avanzamentoWorkflowOk = fpeh.nextStep(wobNumber, metadatiContributo, response.getResponse());

					if (avanzamentoWorkflowOk) {
						// ### Si storna il workflow per contributo
						operationDocumentSRV.stornaWorkflowPerContributo(wobNumber, idDocumentoString, fpeh, utente.getIdAoo());

						// ### Se la response è "Valida Contributi", si aggiorna lo stato del contributo
						// a "VALIDATO"
						if (ResponsesRedEnum.VALIDA_CONTRIBUTI.equals(response)) {
							contributoDAO.updateStatoContributoByIdUfficio(utente.getIdUfficio(), idDocumento, StatoContributoEnum.VALIDATO.getId(), connection);
						}

						// ### Commit
						commitConnection(connection);

						esito = new EsitoOperazioneDTO(null, null, detailDocument.getNumeroDocumento(), wobNumber);
						if (esito.isEsito()) {
							if (ResponsesRedEnum.VALIDA_CONTRIBUTI.equals(response)) {
								esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
							} else {
								esito.setNote("Inserimento contributo effettuato con successo.");
							}
						}
						LOGGER.info(response.name() + " -> Inserimento del contributo eseguito correttamente per il documento: " + idDocumentoString);
					} else {
						final String errore = response.name() + " -> Errore durante l'avanzamento del workflow: " + wobNumber + " con la response: " + response.name()
								+ PER_DOCUMENTO_LITERAL + idDocumentoString;
						LOGGER.error(errore);
						throw new FilenetException(errore);
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error(response.name() + " -> Errore durante l'inserimento di un nuovo contributo interno: " + e.getMessage(), e);
			rollbackConnection(connection);
			// Si genera l'esito in caso di eccezione
			esito.setNote("Si è verificato un errore durante l'inserimento del contributo interno.");
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}

		LOGGER.info(response.name() + " -> END");
		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IContributoFacadeSRV#validazioneContributo(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String).
	 */
	@Override
	public EsitoOperazioneDTO validazioneContributo(final UtenteDTO utente, final String wobNumbers, final String motivoAssegnazione) {
		final EsitoOperazioneDTO esito = operationWorkflowSRV.avanzaWorkflowEStornaWfContributi(utente, wobNumbers, motivoAssegnazione,
				ResponsesRedEnum.VALIDAZIONE_CONTRIBUTO);
		if (esito.isEsito()) {
			esito.setNote("La validazione del documento è stata effettuata con successo.");
		}

		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IContributoFacadeSRV#inserisciContributoEsternoManuale(java.lang.String,
	 *      it.ibm.red.business.dto.ContributoDTO, java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public final EsitoOperazioneDTO inserisciContributoEsternoManuale(final String documentTitle, final ContributoDTO contributo, final String idFascicolo,
			final UtenteDTO utente) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(Integer.parseInt(documentTitle));

		try {
			final Contributo contributoEsterno = new Contributo(null, utente.getIdAoo(), Long.valueOf(documentTitle), contributo.getMittente(), contributo.getNota(),
					contributo.getFilename(), documentTitle + "_" + Instant.now().toEpochMilli(), "-", StatoContributoEsternoEnum.ATTIVO.getId(),
					ContributoEsternoModificabileEnum.MODIFICABILE.getId(), null);

			inserisciContributoEsterno(documentTitle, documentTitle, contributoEsterno, FileUtils.toDataHandler(contributo.getContent()), contributo.getMimeType(),
					idFascicolo, utente);

			esito.setNote("Contributo esterno inserito correttamente");
			esito.setEsito(true);
		} catch (final Exception e) {
			LOGGER.error(e);
			esito.setNote(e.getMessage());
		}

		return esito;
	}

	private void inserisciContributoEsterno(final String documentTitleDocContributoRichiesto, final String documentTitleDocRispostaContributo,
			final Contributo contributoEsterno, final DataHandler content, final String contentType, final String idFascicolo, final UtenteDTO utente) {
		IFilenetCEHelper fceh = null;
		Connection con = null;
		Document contributoFilenet = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			con = setupConnection(getDataSource().getConnection(), true);
			final PropertiesProvider pp = PropertiesProvider.getIstance();

			// ### Si inserisce il contributo esterno nel DB
			final long idContributo = contributoDAO.insertContributoEsterno(contributoEsterno, con);

			// ### Se l'inserimento è andato a buon fine, si procede con l'inserimento del
			// content su FileNet
			if (idContributo > 0) {
				final DocumentoRedFnDTO contributoRedFn = new DocumentoRedFnDTO();
				contributoRedFn.setDocumentTitle(String.valueOf(idContributo));
				contributoRedFn.setClasseDocumentale(pp.getParameterByKey(PropertiesNameEnum.CONTRIBUTO_CLASSNAME_FN_METAKEY));
				contributoRedFn.setFolder(pp.getParameterByKey(PropertiesNameEnum.FOLDER_DOCUMENTI_CE_KEY));
				contributoRedFn.setDataHandler(content);
				contributoRedFn.setContentType(contentType);

				final ListSecurityDTO securityContributo = securitySRV.getSecurityPerContributo(utente, documentTitleDocRispostaContributo, false, con);
				contributoRedFn.setSecurity(securityContributo);

				final Map<String, Object> metadatiContributo = new HashMap<>();
				metadatiContributo.put(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), String.valueOf(idContributo));
				metadatiContributo.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), contributoEsterno.getNomeFile());
				metadatiContributo.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), utente.getIdAoo());

				contributoRedFn.setMetadati(metadatiContributo);

				final FascicoloRedFnDTO fascicoloContributo = new FascicoloRedFnDTO();
				fascicoloContributo.setIdFascicolo(idFascicolo);

				// ### Si inserisce il documento di contributo esterno su FileNet, nel fascicolo
				// indicato
				contributoFilenet = fceh.creaDocumento(contributoRedFn, fascicoloContributo, null, utente.getIdAoo());
				LOGGER.info("Inserimento su FileNet del documento di Contributo Esterno eseguito correttamente per il documento: " + documentTitleDocContributoRichiesto);

				// ### Si aggiorna il contributo esterno precedentemente inserito nel DB con il
				// GUID del documento appena creato su FileNet
				contributoDAO.updateGuidContributo(idContributo, contributoFilenet.get_Id().toString(), true, con);

			}

			commitConnection(con);
		} catch (final Exception e) {
			final String msg = "Si è verificato un errore durante l'inserimento del contributo esterno";
			LOGGER.error(msg + PER_DOCUMENTO_LITERAL + documentTitleDocContributoRichiesto, e);
			// Rollback delle operazioni eseguite sul DB
			rollbackConnection(con);
			// Se il documento di contributo esterno era stato creato su FileNet, lo si
			// rimuove
			if (contributoFilenet != null) {
				fceh.deleteVersion(contributoFilenet.get_Id());
			}
			throw new RedException(msg, e);
		} finally {
			popSubject(fceh);
			closeConnection(con);
		}
	}

	/**
	 * Dal guid di un documento al suo document title.
	 * 
	 * @param fcDTO credenziali filenet
	 * @param guid  guid documento
	 * @return document title
	 */
	private String fromGuidToDocumentTitle(final FilenetCredentialsDTO fcDTO, final String guid, final Long idAoo) {
		String documentTitle = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);
			final Document doc = fceh.getDocumentByGuid(guid);

			documentTitle = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
		} finally {
			popSubject(fceh);
		}

		return documentTitle;
	}

	/**
	 * Dal document title all'identificativo del documento.
	 * 
	 * @param utente        utente che esegue l'operazione
	 * @param documentTitle document title
	 * @return identificativo documento
	 */
	private Integer fromDocumentTitleToIdDocumento(final UtenteDTO utente, final String documentTitle) {
		Integer numDoc = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final Document doc = fceh.getDocumentForDetail(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY),
					documentTitle);

			numDoc = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
		} finally {
			popSubject(fceh);
		}

		return numDoc;
	}

	@Override
	public final EsitoOperazioneDTO eliminaContributoInterno(final Long idContributo, final Long idUtenteContributo, final Integer idDocumento, final UtenteDTO utente) {
		return eliminaContributo(idContributo, idUtenteContributo, idDocumento, false, null, utente);
	}

	@Override
	public final EsitoOperazioneDTO eliminaContributoEsterno(final Long idContributo, final String guidContributo, final Integer idDocumento, final UtenteDTO utente) {
		return eliminaContributo(idContributo, null, idDocumento, true, guidContributo, utente);
	}

	private EsitoOperazioneDTO eliminaContributo(final Long idContributo, final Long idUtenteContributo, final Integer idDocumento, final boolean flagEsterno,
			final String guidContributoEsterno, final UtenteDTO utente) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(String.valueOf(idContributo));

		// Un contributo interno può essere eliminato solo dall'utente che l'ha inserito
		if ((!flagEsterno && utente.getId().equals(idUtenteContributo)) || flagEsterno) {
			final PropertiesProvider pp = PropertiesProvider.getIstance();
			Connection con = null;
			FilenetPEHelper fpeh = null;
			IFilenetCEHelper fceh = null;

			try {
				con = setupConnection(getDataSource().getConnection(), true);

				// Gestione eliminazione contributo interno
				if (!flagEsterno) {
					// Si aggiorna lo stato del contributo interno a "ELIMINATO" (eliminazione
					// logica)
					final int contributoAggiornato = contributoDAO.updateStatoContributo(idContributo, StatoContributoEnum.ELIMINATO.getId(), false, con);

					// Si crea il workflow per lo storico
					if (contributoAggiornato > 0) {
						fpeh = new FilenetPEHelper(utente.getFcDTO());

						final Map<String, Object> metadatiWorkflow = new HashMap<>();
						metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY), idDocumento);
						metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_CLIENT_METAKEY),
								pp.getParameterByString(utente.getCodiceAoo() + "." + PropertiesNameEnum.WS_IDCLIENT.getKey()));
						metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), utente.getIdUfficio().intValue());
						metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId().intValue());

						notificaPostElimContributoSRV.writeNotifiche(utente.getIdAoo().intValue(), idContributo);

						fpeh.createStoricoWF(metadatiWorkflow, "83");
					}
					// Gestione eliminazione contributo esterno
				} else {
					// Si rimuove il contributo esterno dal DB (eliminazione fisica)
					final int contributoEliminato = contributoDAO.deleteContributoEsterno(idContributo, con);

					// Si rimuove il documento del contributo esterno da FileNet
					if (contributoEliminato > 0) {
						fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

						fceh.eliminaDocumento(guidContributoEsterno);
					}
				}

				commitConnection(con);
				esito.setEsito(true);
				esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
			} catch (final Exception e) {
				LOGGER.error("eliminaContributo -> Si è verificato un errore durante l'eliminazione del contributo" + (flagEsterno ? ESTERNO_LITERAL : "") + " con ID: "
						+ idContributo, e);
				rollbackConnection(con);
				esito.setNote("Errore durante l'eliminazione del contributo.");
				esito.setEsito(false);
			} finally {
				closeConnection(con);
				logoff(fpeh);
				popSubject(fceh);
			}
		}

		return esito;
	}

	@Override
	public final EsitoOperazioneDTO aggiornaContributo(final ContributoDTO contributo, final UtenteDTO utente) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(String.valueOf(contributo.getIdContributo()));

		final boolean isEsterno = Boolean.TRUE.equals(contributo.getFlagEsterno());

		// La modifica di un contributo è possibile solo se:
		// - È interno e l'utente che esegue la modifica corrisponde all'utente che l'ha inserito
		// - È esterno ed è modificabile.
		if ((!isEsterno && utente.getId().equals(contributo.getIdUtente())) || (isEsterno && Boolean.TRUE.equals(contributo.getModificabile()))) {
			Document contributoFilenet = null;
			Connection con = null;
			FilenetPEHelper fpeh = null;
			IFilenetCEHelper fceh = null;
			final PropertiesProvider pp = PropertiesProvider.getIstance();

			try {
				con = setupConnection(getDataSource().getConnection(), true);

				// Gestione aggiornamento contributo interno
				if (!isEsterno) {
					fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

					// Se nel contributo in input è presente il content...
					if (contributo.getContent() != null && contributo.getMimeType() != null) {
						final Map<String, Object> metadatiDocumento = new HashMap<>();
						metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), contributo.getFilename());

						// ...va aggiornato su FileNet, se già presente
						if (!StringUtils.isNullOrEmpty(contributo.getGuidContributo())) {
							// Si recupera il contributo da FileNet
							final Document contributoFilenetDaAggiornare = fceh.getDocumentForContributo(contributo.getGuidContributo());

							final InputStream isContributo = new ByteArrayInputStream(contributo.getContent());

							// Si aggiunge una nuova versione del contributo su FileNet
							contributoFilenet = fceh.addVersion(contributoFilenetDaAggiornare, isContributo, metadatiDocumento, contributo.getFilename(),
									contributo.getMimeType(), utente.getIdAoo());
							// ...altrimenti, va inserito ex-novo
						} else {
							final String docTitleContributo = String.valueOf(contributo.getIdContributo());

							metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), docTitleContributo);
							metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), utente.getIdAoo());
							final PropertyFilter pfRiservato = new PropertyFilter();
							pfRiservato.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY), null));
							final List<String> selectRiservato = new ArrayList<>(1);
							selectRiservato.add(pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY));

							final Document documentoOriginale = fceh.getDocumentByIdGestionale(String.valueOf(contributo.getIdDocumento()), selectRiservato, pfRiservato,
									utente.getIdAoo().intValue(), null, null);
							final Integer isRiservato = (Integer) TrasformerCE.getMetadato(documentoOriginale, PropertiesNameEnum.RISERVATO_METAKEY);

							final ListSecurityDTO securityContributo = securitySRV.getSecurityPerContributo(utente, String.valueOf(contributo.getIdDocumento()),
									BooleanFlagEnum.SI.getIntValue().equals(isRiservato), con);

							final DocumentoRedFnDTO contributoRedFn = new DocumentoRedFnDTO();
							contributoRedFn.setDocumentTitle(docTitleContributo);
							contributoRedFn.setClasseDocumentale(pp.getParameterByKey(PropertiesNameEnum.CONTRIBUTO_CLASSNAME_FN_METAKEY));
							contributoRedFn.setDataHandler(FileUtils.toDataHandler(contributo.getContent()));
							contributoRedFn.setContentType(contributo.getMimeType());
							contributoRedFn.setMetadati(metadatiDocumento);
							contributoRedFn.setSecurity(securityContributo);

							// Si crea il documento del contributo su FileNet
							contributoFilenet = fceh.creaDocumento(contributoRedFn, null, null, utente.getIdAoo());

							contributo.setGuidContributo(docTitleContributo);
						}
						// Altrimenti, se il content del contributo è stato rimosso (cioè il GUID è
						// ancora presente, ma il campo relativo al nome del file è vuoto)
					} else if (StringUtils.isNullOrEmpty(contributo.getFilename()) && !StringUtils.isNullOrEmpty(contributo.getGuidContributo())) {
						// ...lo si rimuove da FileNet
						fceh.eliminaDocumentoByDocumentTitle(contributo.getGuidContributo(), utente.getIdAoo());

						// Si rimuove l'associazione nel database
						contributo.setGuidContributo(null);
					}

					final int contributoAggiornato = contributoDAO.updateContributoInterno(contributo, utente.getIdUfficio(), utente.getId(), con);

					// Si crea il workflow per lo storico
					if (contributoAggiornato > 0) {
						fpeh = new FilenetPEHelper(utente.getFcDTO());

						final Map<String, Object> metadatiWorkflow = new HashMap<>();
						metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY), contributo.getIdDocumento());
						metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_CLIENT_METAKEY),
								pp.getParameterByString(utente.getCodiceAoo() + "." + PropertiesNameEnum.WS_IDCLIENT.getKey()));
						metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), utente.getIdUfficio().intValue());
						metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId().intValue());

						fpeh.createStoricoWF(metadatiWorkflow, "82");
					}
					// Gestione aggiornamento contributo esterno
				} else {
					contributoDAO.updateContributoEsternoByProtocollo(contributo.getProtocollo(), utente.getIdAoo(), contributo.getMittente(), contributo.getNota(), con);
				}

				commitConnection(con);
				esito.setEsito(true);
				if (isEsterno) {
					esito.setNote("Contributo esterno aggiornato correttamente");
				} else {
					esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
				}
			} catch (final Exception e) {
				LOGGER.error("aggiornaContributo -> Si è verificato un errore durante l'aggiornamento del contributo" + (isEsterno ? ESTERNO_LITERAL : "") + " con ID: "
						+ contributo.getIdContributo(), e);
				rollbackConnection(con);
				if (contributoFilenet != null) {
					fceh.eliminaUltimaVersione(contributoFilenet);
				}
				esito.setNote("Si è verificato un errore durante l'aggiornamento del contributo" + (isEsterno ? ESTERNO_LITERAL : ""));
				esito.setEsito(false);
			} finally {
				closeConnection(con);
				logoff(fpeh);
				popSubject(fceh);
			}
		} else {
			esito.setNote("Non è possibile procedere con la modifica del contributo.");
		}

		return esito;
	}

	@Override
	public final Collection<DetailDocumentRedDTO> getContributiAttivi(final Integer numeroDocumento, final Integer annoDocumento, final UtenteDTO utente) {
		Collection<DetailDocumentRedDTO> contributiAttivi = new ArrayList<>();
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;
		
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			
			final DocumentSet contributiDoc = fceh.getContributiAttivi(numeroDocumento, annoDocumento, utente.getIdAoo());

			if (contributiDoc != null && !contributiDoc.isEmpty()) {
				Collection<DetailDocumentRedDTO> contributi = TrasformCE.transform(contributiDoc, TrasformerCEEnum.FROM_DOCUMENTO_TO_DETAIL_RED);
				
				//considera attivi i soli contributi non messi agli atti
				Iterator<DetailDocumentRedDTO> it = contributi.iterator();
				while(it.hasNext()) {
					DetailDocumentRedDTO contributo = it.next();
					
					VWWorkObject workflowPrincipale = fpeh.getWorkflowPrincipale(contributo.getDocumentTitle(), utente.getFcDTO().getIdClientAoo());
					boolean add = false;
					if(workflowPrincipale != null) {
						contributo.setWobNumberPrincipale(workflowPrincipale.getWorkObjectNumber());
						add = true;
					} else {
						EventoLogDTO lastChiusuraIngressoEvent = eventoLogSRV.getLastChiusuraIngressoEvent(Integer.parseInt(contributo.getDocumentTitle()), utente.getIdAoo().intValue());
						if(!EventTypeEnum.ATTI.equals(EventTypeEnum.get(Integer.parseInt(lastChiusuraIngressoEvent.getEventType())))){
							add = true;
						}
					}
					
					if(add) {
						contributiAttivi.add(contributo);
					}
				}
				
			}
		} catch (final Exception e) {
			final String msg = "Si è verificato un errore durante il recupero dei contributi relativi al documento numero: " + numeroDocumento + " dell'anno: "
					+ annoDocumento;
			LOGGER.error(msg, e);
			throw new RedException(msg);
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}

		return contributiAttivi;
	}

	/**
	 * @see it.ibm.red.business.service.IContributoSRV#checkContributoEsternoNonConcluso(java.lang.String,
	 *      java.lang.Integer, java.lang.Integer, it.ibm.red.business.dto.UtenteDTO,
	 *      java.sql.Connection, boolean).
	 */
	@Override
	public boolean checkContributoEsternoNonConcluso(final String documentTitle, final Integer numeroDocumento, final Integer annoDocumento, final UtenteDTO utente,
			final Connection con, final boolean isSollecito) {
		boolean contributoEsternoNonConcluso = false;
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			
			// Si recuperano le richieste di Contributo Esterno (-> documenti con tipologia
			// documentale "CONTRIBUTO ESTERNO" in USCITA)
			final DocumentSet richiesteContributo = fceh.getContributiAttivi(numeroDocumento, annoDocumento, utente.getIdAoo());

			if (richiesteContributo != null) {
				// Si recuperano i documenti di Contributo Esterno associati nel DB
				final Collection<Contributo> contributi = contributoDAO.getContributiEsterni(documentTitle, utente.getIdAoo(), con);

				if (contributi != null) {
					final Set<String> protocolliContributiEsterniEntrata = new HashSet<>();

					for (final Contributo contributo : contributi) {
						protocolliContributiEsterniEntrata.add(contributo.getProtocollo());
					}

					int numeroRichiesteContributo = 0;
					final Iterator<?> itRichiesteContributo = richiesteContributo.iterator();
					while (itRichiesteContributo.hasNext()) {
						
						Document contributo = (Document)itRichiesteContributo.next();
						String documentTitleContributo = (String)TrasformerCE.getMetadato(contributo, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
						
						VWWorkObject workflowPrincipale = fpeh.getWorkflowPrincipale(documentTitleContributo, utente.getFcDTO().getIdClientAoo());
						boolean add = false;
						if(workflowPrincipale != null) {
							if(!isSollecito) {
								add = true;
							}
						} else {
							EventoLogDTO lastChiusuraIngressoEvent = eventoLogSRV.getLastChiusuraIngressoEvent(Integer.parseInt(documentTitleContributo), utente.getIdAoo().intValue());
							if(!EventTypeEnum.ATTI.equals(EventTypeEnum.get(Integer.parseInt(lastChiusuraIngressoEvent.getEventType())))){
								add = true;
							}
						}
						
						if(add) {
							numeroRichiesteContributo++;
						}
						
					}

					if (numeroRichiesteContributo != protocolliContributiEsterniEntrata.size()) {
						contributoEsternoNonConcluso = true;
					}
				}
			}
		} catch (final Exception e) {
			final StringBuilder msg = new StringBuilder("Si è verificato un errore durante il controllo sulla presenza di Richieste di Contributo Esterno non concluse")
					.append(PER_DOCUMENTO_LITERAL).append(numeroDocumento).append("/").append(annoDocumento);
			LOGGER.error(msg.append(" (DocumentTitle: ").append(documentTitle).append(")").toString(), e);
			throw new RedException(msg.toString(), e);
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}

		return contributoEsternoNonConcluso;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.ibm.red.business.service.facade.IContributoFacadeSRV#collegaContributo(
	 * java.lang.String, java.lang.String, it.ibm.red.business.dto.FascicoloDTO,
	 * it.ibm.red.business.dto.TitolarioDTO, it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public final EsitoOperazioneDTO collegaContributo(final String wobNumber, final String documentTitle, final FascicoloDTO fascicoloSelezionato,
			final TitolarioDTO titolarioSelezionato, final UtenteDTO utente) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;
		Connection con = null;
		Connection dwhCon = null;

		try {
			final PropertiesProvider pp = PropertiesProvider.getIstance();
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);

			final Integer idFascicoloDaChiudere = (Integer) TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY);

			// Si recuperano i documenti presenti nel fascicolo selezionato
			final DocumentSet documentiFascicolo = fceh.getDocumentiFascicolo(Integer.parseInt(fascicoloSelezionato.getIdFascicolo()),
					fascicoloSelezionato.getIdAOO().longValue());

			if (documentiFascicolo != null && !documentiFascicolo.isEmpty()) {
				// Si verifica la presenza di almeno un documento con tipologia documentale
				// "CONTRIBUTO ESTERNO" in USCITA (-> RICHIESTA CONTRIBUTO ESTERNO)
				final Iterator<?> itDocumentiFascicolo = documentiFascicolo.iterator();
				Document docRichiestaContributoEsterno = null;
				while (itDocumentiFascicolo.hasNext()) {
					final Document docFascicolo = (Document) itDocumentiFascicolo.next();

					// Si skippa il documento FileNet del fascicolo, contenuto in quest'ultimo
					if (!pp.getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY).equalsIgnoreCase(docFascicolo.getClassName())) {
						final Document docTmp = fceh.getDocument(docFascicolo.get_Id());

						final Integer idTipologiaDocumento = (Integer) TrasformerCE.getMetadato(docTmp, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
						if (idTipologiaDocumento != null && Integer
								.parseInt(pp.getParameterByString(utente.getCodiceAoo() + "." + Varie.CONTRIBUTO_ESTERNO_USCITA_ID_KEY_SUFFIX)) == idTipologiaDocumento) {
							docRichiestaContributoEsterno = docTmp;
							break;
						}
					}
				}

				if (docRichiestaContributoEsterno != null) {
					Integer numeroDocMittenteContributo = (Integer) TrasformerCE.getMetadato(docRichiestaContributoEsterno,
							PropertiesNameEnum.METADATO_NUMERO_MITTENTE_CONTRIBUTO);
					if (numeroDocMittenteContributo == null) {
						numeroDocMittenteContributo = -1;
					}
					Integer annoDocMittenteContributo = (Integer) TrasformerCE.getMetadato(docRichiestaContributoEsterno,
							PropertiesNameEnum.METADATO_ANNO_MITTENTE_CONTRIBUTO);
					if (annoDocMittenteContributo == null) {
						annoDocMittenteContributo = -1;
					}

					// Si recupera il documento su cui è stata fatta la richiesta di contributo
					// esterno
					final StringBuilder whereCondition = new StringBuilder("d.").append(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY)).append(" = ")
							.append(numeroDocMittenteContributo.toString()).append(" AND d.").append(pp.getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY))
							.append(" = ").append(annoDocMittenteContributo.toString());

					final DocumentSet docsContributoRichiesto = fceh.getDocuments(utente.getIdAoo().intValue(), whereCondition.toString(),
							pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), true, false);

					if (docsContributoRichiesto != null && !docsContributoRichiesto.isEmpty()) {
						final Document docContributoRichiesto = (Document) docsContributoRichiesto.iterator().next();
						final String documentTitleDocContributoRichiesto = (String) TrasformerCE.getMetadato(docContributoRichiesto,
								PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);

						// ### Si procede all'aggiornamento dell'indice di classificazione
						// del fascicolo del documento di risposta alla richiesta di contributo esterno
						con = setupConnection(getDataSource().getConnection(), false);
						fascicoloSRV.associaATitolario(String.valueOf(idFascicoloDaChiudere), titolarioSelezionato.getIndiceClassificazione(),
								fascicoloSelezionato.getIdAOO().longValue(), fceh, con);

						// Si recupera il documento di risposta alla richiesta di contributo esterno
						// (documento con tipologia documentale "CONTRIBUTO ESTERNO" in ENTRATA)
						final PropertyFilter pfDocClass = new PropertyFilter();
						pfDocClass.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY), null));
						pfDocClass.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY), null));
						pfDocClass.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.MITTENTE_METAKEY), null));
						pfDocClass.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), null));
						pfDocClass.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY), null));
						pfDocClass.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.CONTENT_ELEMENTS, null));
						pfDocClass.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.MIME_TYPE, null));
						pfDocClass.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.ID, null));

						final Document docRispostaContributoEsterno = fceh.getDocumentByIdGestionale(documentTitle,
								Arrays.asList(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY),
										pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY), pp.getParameterByKey(PropertiesNameEnum.MITTENTE_METAKEY),
										pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY),
										PropertyNames.CONTENT_ELEMENTS, PropertyNames.MIME_TYPE, PropertyNames.ID),
								pfDocClass, utente.getIdAoo().intValue(), null, null);

						// Protocollo
						final String protocollo = TrasformerCE.getMetadato(docRispostaContributoEsterno, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY) + "/"
								+ TrasformerCE.getMetadato(docRispostaContributoEsterno, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
						// Mittente
						final String mittente = ((String) TrasformerCE.getMetadato(docRispostaContributoEsterno, PropertiesNameEnum.MITTENTE_METAKEY)).split(",")[1];

						// ### Si inserisce il documento principale del documento di risposta come
						// contributo esterno
						if (FilenetCEHelper.hasDocumentContentTransfer(docRispostaContributoEsterno)) {
							// Nome file
							final String nomeFile = (String) TrasformerCE.getMetadato(docRispostaContributoEsterno, PropertiesNameEnum.NOME_FILE_METAKEY);

							final Contributo contributoEsterno = new Contributo(null, utente.getIdAoo(), Long.valueOf(documentTitleDocContributoRichiesto), mittente, "-",
									nomeFile, protocollo, "-", StatoContributoEsternoEnum.ATTIVO.getId(), ContributoEsternoModificabileEnum.IMMODIFICABILE_PRINCIPALE.getId(),
									null);

							// ### Inserimento del contributo esterno (content del documento principale)
							inserisciContributoEsterno(documentTitleDocContributoRichiesto, documentTitle, contributoEsterno,
									FilenetCEHelper.getDocumentContentAsDataHandler(docRispostaContributoEsterno), docRispostaContributoEsterno.get_MimeType(),
									fascicoloSelezionato.getIdFascicolo(), utente);

							LOGGER.info("Inserimento del contributo esterno afferente al documento: " + protocollo + " effettuato con successo.");
						}

						// ### Si inseriscono gli allegati del documento di risposta come contributi
						// esterni
						// Si recuperano gli allegati
						final DocumentSet allegati = fceh.getAllegatiConContent(docRispostaContributoEsterno.get_Id().toString());
						if (allegati != null && !allegati.isEmpty()) {
							final Iterator<?> itAllegati = allegati.iterator();

							while (itAllegati.hasNext()) {
								final Document allegato = (Document) itAllegati.next();
								// Nome file
								final String nomeFile = (String) TrasformerCE.getMetadato(allegato, PropertiesNameEnum.NOME_FILE_METAKEY);

								final Contributo contributoEsterno = new Contributo(null, utente.getIdAoo(), Long.valueOf(documentTitleDocContributoRichiesto), mittente, "-",
										nomeFile, protocollo, "-", StatoContributoEsternoEnum.ATTIVO.getId(),
										ContributoEsternoModificabileEnum.IMMODIFICABILE_ALLEGATO.getId(), null);

								// ### Inserimento del contributo esterno (content dell'allegato)
								inserisciContributoEsterno(documentTitleDocContributoRichiesto, documentTitle, contributoEsterno,
										FilenetCEHelper.getDocumentContentAsDataHandler(allegato), allegato.get_MimeType(), fascicoloSelezionato.getIdFascicolo(), utente);
							}

							LOGGER.info("Inserimento dei contributi esterni afferenti agli allegati del documento: " + protocollo + " effettuato con successo.");
						}

						// ### Messa agli atti del documento di risposta alla richiesta di contributo
						// esterno
						final EsitoOperazioneDTO esitoMessaAtti = attiSRV.mettiAgliAtti(wobNumber, utente, "Messa agli atti per collegamento contributo");

						if (esitoMessaAtti.isEsito()) {
							dwhCon = setupConnection(getFilenetDataSource().getConnection(), false);

							final Integer numeroDocRichiestaContributo = (Integer) TrasformerCE.getMetadato(docRichiestaContributoEsterno,
									PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
							final Integer annoDocRichiestaContributo = (Integer) TrasformerCE.getMetadato(docRichiestaContributoEsterno,
									PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY);

							final StringBuilder motivazioneAssegnazione = new StringBuilder("Collegato contributo nel fascicolo ")
									.append(fascicoloSelezionato.getDescrizione()).append(" ed avente documento di tipologia CONTRIBUTO ESTERNO ")
									.append(numeroDocRichiestaContributo).append("-").append(annoDocRichiestaContributo);

							// ### Se la messa agli atti è avvenuta correttamente, si traccia l'evento di
							// collegamento del contributo
							eventoLogSRV.inserisciEventoLog(Integer.parseInt(documentTitle), utente.getIdUfficio(), utente.getId(), 0L, 0L, Calendar.getInstance().getTime(),
									EventTypeEnum.COLLEGA_CONTRIBUTO, motivazioneAssegnazione.toString(), 0, utente.getIdAoo(), dwhCon);

							// ### Esito positivo dell'operazione di collegamento del contributo
							esito.setEsito(true);
							esito.setNote("Collegamento contributo effettuato con successo.");
							LOGGER.info(
									"Collegamento del contributo effettuato con successo per il documento: " + numeroDocMittenteContributo + "/" + annoDocMittenteContributo);
						} else {
							LOGGER.error("Errore durante la messa agli atti del documento a seguito del collegamento del contributo esterno: " + protocollo);
							esito.setNote(
									"Si è verificato un errore durante la messa agli atti del documento a seguito del collegamento del contributo esterno: " + protocollo);
						}
					} else {
						esito.setNote("Non è stato trovato il documento dal quale è stato richiesto il contributo originario: " + numeroDocMittenteContributo + "-"
								+ annoDocMittenteContributo);
					}
				} else {
					esito.setNote("Il fascicolo selezionato non ha nessun documento con tipologia documentale 'CONTRIBUTO ESTERNO';"
							+ " non è possibile procedere con il collegamento del contributo esterno.");
				}
			} else {
				esito.setNote("Il fascicolo selezionato non ha nessun documento.");
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			esito.setNote("Si è verificato un errore durante il collegamento del contributo.");
		} finally {
			popSubject(fceh);
			logoff(fpeh);
			closeConnection(con);
			closeTransaction(dwhCon);
		}

		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IContributoFacadeSRV#eliminaContributiEsterniByProtocollo(java.lang.String,
	 *      java.lang.String, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public EsitoOperazioneDTO eliminaContributiEsterniByProtocollo(final String documentTitle, final String protocollo, final UtenteDTO utente) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(documentTitle);
		Connection con = null;
		IFilenetCEHelper fceh = null;

		try {
			con = setupConnection(getDataSource().getConnection(), true);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final Collection<Contributo> contributiDaEliminare = contributoDAO.getContributiEsterniByProtocollo(protocollo, utente.getIdAoo(), con);

			if (!CollectionUtils.isEmpty(contributiDaEliminare)) {
				// Si eliminano i contributi esterni dal DB
				final int eliminazioneOk = contributoDAO.deleteContributiEsterniByProtocollo(protocollo, utente.getIdAoo(), con);

				if (eliminazioneOk > 0) {
					// Si eliminano i contributi esterni da FileNet
					for (final Contributo contributo : contributiDaEliminare) {
						fceh.deleteVersion(fceh.idFromGuid(contributo.getGuid()));
					}

					esito.setEsito(true);
					esito.setNote("Cancellazione contributo esterno avvenuta con successo");
				}
			}

			// Commit
			commitConnection(con);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'eliminazione dei contributi agganciati al documento: " + documentTitle + " e afferenti al protocollo: "
					+ protocollo + " dell'AOO: " + utente.getIdAoo(), e);
			rollbackConnection(con);
			esito.setNote("Si è verificato un errore durante l'eliminazione dei contributi");
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}

		return esito;
	}

}