
package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IMockDAO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaNPSDTO;
import it.ibm.red.business.dto.RegistroDTO;
import it.ibm.red.business.enums.TipoDocumentoMockEnum;
import it.ibm.red.business.enums.TipoProtocolloEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IMockSRV;

/**
 * Service mock.
 */
@Service
@Component
public class MockSRV extends AbstractService implements IMockSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 9109661265689686742L;

	/**
	 * Messaggio errore stacco protocollo.
	 */
	private static final String ERROR_STACCO_PROTOCOLLO_MOCK_MSG = "Errore durante lo stacco del protocollo mock";
	
	/**
	 * Messaggio errore recupero protocollo.
	 */
	private static final String ERROR_RECUPERO_PROTOCOLLO_MOCK_MSG = "Errore durante il recupero del protocollo mock";
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(MockSRV.class.getName());

	/**
	 * DAO.
	 */
	@Autowired
	private IMockDAO mockDAO;

	/**
	 * @see it.ibm.red.business.service.IMockSRV#getNewProtocollo(java.lang.String, it.ibm.red.business.enums.TipoProtocolloEnum, java.sql.Connection).
	 */
	@Override
	public ProtocolloDTO getNewProtocollo(final String codiceAoo, final TipoProtocolloEnum tipoProtocollo, final Connection conn) {
		ProtocolloDTO protocollo = null;

		try {
			
			final Integer nextProt = mockDAO.getNextProt(codiceAoo, tipoProtocollo, conn);
			final Date today = new Date();
			final SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
			protocollo = new ProtocolloDTO(UUID.randomUUID().toString(), nextProt, sdf.format(today), 
					today, null, null);
			mockDAO.insertProt(protocollo, codiceAoo, tipoProtocollo, conn);
			
		} catch (final Exception e) {
			LOGGER.error(ERROR_STACCO_PROTOCOLLO_MOCK_MSG, e);
		}
		
		return protocollo;

	}

	/**
	 * @see it.ibm.red.business.service.facade.IMockFacadeSRV#getProtocollo(java.lang.String).
	 */
	@Override
	public ProtocolloDTO getProtocollo(final String idProtocollo) {
		ProtocolloDTO protocollo = null;
		Connection connection =  null;
				
		try {
			
			connection = setupConnection(getDataSource().getConnection(), false);
			protocollo = getProtocollo(idProtocollo, connection);
			
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_PROTOCOLLO_MOCK_MSG, e);
		} finally {
			closeConnection(connection);
		}
		
		return protocollo;
	}

	/**
	 * @see it.ibm.red.business.service.IMockSRV#getProtocollo(java.lang.String, java.sql.Connection).
	 */
	@Override
	public ProtocolloDTO getProtocollo(final String idProtocollo, final Connection conn) {
		ProtocolloDTO protocollo = null;
						
		try {
			
			protocollo = mockDAO.getProt(idProtocollo, conn);
			
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_PROTOCOLLO_MOCK_MSG, e);
		} 
		
		return protocollo;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IMockFacadeSRV#getNewProtocollo(java.lang.String, it.ibm.red.business.enums.TipoProtocolloEnum).
	 */
	@Override
	public ProtocolloDTO getNewProtocollo(final String codiceAoo, final TipoProtocolloEnum tipoProtocollo) {
		ProtocolloDTO protocollo = null;
		Connection connection =  null;
				
		try {
			
			connection = setupConnection(getDataSource().getConnection(), false);
			protocollo = getNewProtocollo(codiceAoo, tipoProtocollo, connection);
			
		} catch (final Exception e) {
			LOGGER.error(ERROR_STACCO_PROTOCOLLO_MOCK_MSG, e);
		} finally {
			closeConnection(connection);
		}
		
		return protocollo;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IMockFacadeSRV#getDocumento(it.ibm.red.business.enums.TipoDocumentoMockEnum, java.lang.String).
	 */
	@Override
	public FileDTO getDocumento(final TipoDocumentoMockEnum tipoDocumento, final String serverName) {
		FileDTO documento = null;
		Connection connection =  null;
				
		try {
			
			connection = setupConnection(getDataSource().getConnection(), false);
			documento = mockDAO.getDocumento(tipoDocumento.toString(), serverName, connection);
			
		} catch (final Exception e) {
			LOGGER.error(ERROR_STACCO_PROTOCOLLO_MOCK_MSG, e);
		} finally {
			closeConnection(connection);
		}
		
		return documento;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IMockFacadeSRV#getProtocolli(java.lang.String, java.lang.Integer, java.lang.Integer, int).
	 */
	@Override
	public List<ProtocolloDTO> getProtocolli(final String codiceAoo, final Integer numeroProtocolloDa, final Integer numeroProtocolloA, final int maxResults) {
		List<ProtocolloDTO> protocolli = null;
		Connection connection =  null;
				
		try {
			
			connection = setupConnection(getDataSource().getConnection(), false);
			protocolli = mockDAO.getProtocolli(codiceAoo, numeroProtocolloDa, numeroProtocolloA, maxResults, connection);
			
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_PROTOCOLLO_MOCK_MSG, e);
		} finally {
			closeConnection(connection);
		}
		
		return protocolli;
	}

	/**
	 * @see it.ibm.red.business.service.IMockSRV#getRegistrazioniAusiliarie().
	 */
	@Override
	public List<RegistrazioneAusiliariaNPSDTO> getRegistrazioniAusiliarie() {
		final List<RegistrazioneAusiliariaNPSDTO> registrazioniAusiliarieList = new ArrayList<>();
		
		RegistrazioneAusiliariaNPSDTO registrazioneAusiliaria = new RegistrazioneAusiliariaNPSDTO();
		registrazioneAusiliaria.setCodiceRegistro("REGISTRO 1");
		registrazioneAusiliaria.setNumeroRegistrazione(1);
		registrazioneAusiliaria.setDataRegistrazione(new Date());
		registrazioneAusiliaria.setDescrizioneTipologiaDocumento("TIPOLOGIA GENERICA");
		registrazioneAusiliaria.setOggetto("OGGETTO MOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOLTO LUNGOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
		
		registrazioniAusiliarieList.add(registrazioneAusiliaria);
		
		registrazioneAusiliaria = new RegistrazioneAusiliariaNPSDTO();
		registrazioneAusiliaria.setCodiceRegistro("REGISTRO 2");
		registrazioneAusiliaria.setNumeroRegistrazione(2);
		registrazioneAusiliaria.setDataRegistrazione(new Date());
		registrazioneAusiliaria.setAnnullato(true);
		registrazioneAusiliaria.setDataAnnullamento(new Date());
		registrazioneAusiliaria.setMotivoAnnullamento("ANNULLATO MOTIV.");
		registrazioneAusiliaria.setDescrizioneTipologiaDocumento("ACCCCORDO DI PROGRAMMA");
		registrazioneAusiliaria.setOggetto("OGGETTO DELLA REGISTRAZIONE AUSILIARIA");

		
		registrazioniAusiliarieList.add(registrazioneAusiliaria);
		
		return registrazioniAusiliarieList;
	}

	/**
	 * @see it.ibm.red.business.service.IMockSRV#getRegistrazioneAusiliaria(java.lang.String, it.ibm.red.business.dto.RegistroDTO).
	 */
	@Override
	public RegistrazioneAusiliariaNPSDTO getRegistrazioneAusiliaria(final String codiceAoo, final RegistroDTO registro) {
		RegistrazioneAusiliariaNPSDTO registrazione = null;
		Connection connection =  null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			
			final Integer nextProt = mockDAO.getNextRegistrazione(codiceAoo, connection);
			final Date today = new Date();
			
			registrazione = new RegistrazioneAusiliariaNPSDTO();
			registrazione.setDataRegistrazione(today);
			registrazione.setId(UUID.randomUUID().toString());
			registrazione.setNumeroRegistrazione(nextProt);
			
			mockDAO.insertRegistrazione(registrazione, codiceAoo, registro.getId(), connection);
			
		} catch (final Exception e) {
			LOGGER.error(ERROR_STACCO_PROTOCOLLO_MOCK_MSG, e);
		} finally {
			closeConnection(connection);
		}
		
		return registrazione;
	}

}
