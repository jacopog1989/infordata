package it.ibm.red.business.service.concrete;

import it.ibm.red.business.service.INotificaWriteSRV;

/**
 * Interfaccia servizio notifica post mail.
 */
public interface INotificaPostMailErroreSRV extends INotificaWriteSRV {

}
