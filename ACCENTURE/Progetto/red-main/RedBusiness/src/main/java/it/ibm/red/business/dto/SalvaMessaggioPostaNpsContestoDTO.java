package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.List;

import javax.mail.internet.MimeMessage;

/**
 * 
 * @author CristianoPierascenzi
 *
 * @param <T>	Tipo generico contenuto messaggio posta.
 */
public class SalvaMessaggioPostaNpsContestoDTO<T extends MessaggioPostaNpsDTO> implements Serializable {

	private static final long serialVersionUID = -7447136377457135772L;

	/**
	 * Richiesta elaborazione messaggio.
	 */
	private final RichiestaElabMessaggioPostaNpsDTO<T> richiesta;
	
	/**
	 * Mime type messaggio.
	 */
	private transient MimeMessage messaggioMime;
	
	/**
	 * Content messaggio.
	 */
	private final List<NamedStreamDTO> contentMessaggio;

	
	/**
	 * @param richiesta
	 * @param messaggioMime
	 * @param contentMessaggio
	 */
	public SalvaMessaggioPostaNpsContestoDTO(final RichiestaElabMessaggioPostaNpsDTO<T> richiesta, final MimeMessage messaggioMime,
			final List<NamedStreamDTO> contentMessaggio) {
		super();
		this.richiesta = richiesta;
		this.messaggioMime = messaggioMime;
		this.contentMessaggio = contentMessaggio;
	}


	/**
	 * @return the richiesta
	 */
	public RichiestaElabMessaggioPostaNpsDTO<T> getRichiesta() {
		return richiesta;
	}


	/**
	 * @return the messaggioMime
	 */
	public MimeMessage getMessaggioMime() {
		return messaggioMime;
	}


	/**
	 * @return the contentMessaggio
	 */
	public List<NamedStreamDTO> getContentMessaggio() {
		return contentMessaggio;
	}
	
}
