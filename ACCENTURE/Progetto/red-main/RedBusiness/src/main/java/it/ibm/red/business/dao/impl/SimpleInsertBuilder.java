package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.StringUtils;

/**
 * Classe builder per la costruzione delle query tipo INSERT.
 */
public class SimpleInsertBuilder extends SimpleQueryBuilder {

	/**
	 * SerialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SimpleInsertBuilder.class.getName());
	
	/**
	 * Sequence.
	 */
	private final Map<String, String> sequences;
	
	/**
	 * Valori Sequence.
	 */
	private final Map<String, Integer> sequencesValue;
	
	/**
	 * Costruttore, inizializza la lista delle sequence: {@link #sequences} e
	 * {@link #sequencesValue}.
	 * 
	 * @param inTable
	 */
	public SimpleInsertBuilder(final String inTable) {
		super(inTable);
		sequences = new HashMap<>();
		sequencesValue = new HashMap<>();
	}

	/**
	 * Restituisce i valori delle sequence.
	 * 
	 * @return valori sequence
	 */
	public Map<String, Integer> getSequencesValue() {
		return sequencesValue;
	}

	/**
	 * Aggiunge una sequence alla lista {@link #sequences}.
	 * 
	 * @param key
	 * @param value
	 */
	public void addSequence(final String key, final String value) {
		sequences.put(key, value);
	}

	private static Integer getNextValue(final Connection con, final String seq) throws SQLException {
		Integer output = null;
		final String sql = "select " + seq + ".nextval from DUAL";
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				output = rs.getInt(1);
			}
			
		} catch (final Exception e) {
			LOGGER.error("Errore nel calcolo del next value.", e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}
	
	/**
	 * Chiude statement e result set.
	 * 
	 * @param ps
	 * @param rs
	 */
	public static void closeStatement(final PreparedStatement ps, final ResultSet rs) {
		try {
			if (ps != null) {
				ps.close();
			}
		} catch (final SQLException sqle) {
			LOGGER.error("Errore riscontrato durante la chiusura delo statement.", sqle);
		}
		
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (final SQLException sqle) {
			LOGGER.error("Errore riscontrato durante la chiusura del result set.", sqle);
		}
	}
	
	/**
	 * Esegue la insert utilizzando i parametri definiti sulla classe e restituisce lo statement.
	 * @param con
	 * @return numero di record inseriti
	 * @throws SQLException
	 */
	public Integer insert(final Connection con) {
		PreparedStatement psInsert = null;
		Integer out = 0;
		try {
			StringBuilder fields = new StringBuilder("");
			StringBuilder values = new StringBuilder("");

			//Gestiamo sequence
			if (!sequences.keySet().isEmpty()) {
				for (final Entry<String, String> sequence:sequences.entrySet()) {
					fields.append(sequence.getKey()).append(",");
					final Integer seqValue = getNextValue(con, sequence.getValue());
					values.append(seqValue).append(",");
					sequencesValue.put(sequence.getKey(), seqValue);
				}
			}


			//Gestiamo parametri
			final List<String> paramKeys = new ArrayList<>(getParams().keySet());
	
			if (!paramKeys.isEmpty()) {
					
				for (final String paramKey:paramKeys) {
					fields.append(paramKey).append(",");
					values.append("?,");
				}
				fields = new StringBuilder(StringUtils.cutLast(fields.toString()));
				values = new StringBuilder(StringUtils.cutLast(values.toString()));
			}
	
				
			final String sqlInsert = "INSERT INTO " + getTable() + " (" + fields + ") VALUES (" + values + ")";
			psInsert = con.prepareStatement(sqlInsert);
			Integer index = 1;
			for (final String paramKey:paramKeys) {
				handleParams(psInsert, index, getParams().get(paramKey));
				index++;
			}
			
			out = psInsert.executeUpdate();
			
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeStatement(psInsert);
		}
		return out;
	}
	
	/**
	 * Consente di aggiungere in AND la verifica dell'uguaglianza key - value nella query di riferimento aggiungendo il parametro alla lista.
	 * Il metodo gestisce valori null o stringhe vuote.
	 * @param key
	 * @param value
	 */
	public void addParams(final String key, final Object value) {
		addParams(true, key, value);
	}

}
