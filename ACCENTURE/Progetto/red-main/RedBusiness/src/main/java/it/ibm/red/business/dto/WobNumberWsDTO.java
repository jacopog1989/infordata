/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.List;

/**
 * @author APerquoti
 *
 */
public class WobNumberWsDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 4567198782192736001L;
	
	/**
	 * Lista response.
	 */
	private List<String> responses;
	
	/**
	 * Lista rotte.
	 */
	private List<RouteWsDTO> route;
	
	/**
	 * Descrizione step.
	 */
	private String stepDescription;
	
	/**
	 * Nome step.
	 */
	private String stepName;
	
	/**
	 * WF Number.
	 */
	private String wobNumber;
	
	/**
	 * Costruttore.
	 */
	public WobNumberWsDTO() {
		super();
	}


	/**
	 * @return the responses
	 */
	public List<String> getResponses() {
		return responses;
	}


	/**
	 * @param responses the responses to set
	 */
	public void setResponses(final List<String> responses) {
		this.responses = responses;
	}

	/**
	 * @return the route
	 */
	public List<RouteWsDTO> getRoute() {
		return route;
	}

	/**
	 * @param route the route to set
	 */
	public void setRoute(final List<RouteWsDTO> route) {
		this.route = route;
	}


	/**
	 * @return the stepDescription
	 */
	public String getStepDescription() {
		return stepDescription;
	}


	/**
	 * @param stepDescription the stepDescription to set
	 */
	public void setStepDescription(final String stepDescription) {
		this.stepDescription = stepDescription;
	}


	/**
	 * @return the stepName
	 */
	public String getStepName() {
		return stepName;
	}


	/**
	 * @param stepName the stepName to set
	 */
	public void setStepName(final String stepName) {
		this.stepName = stepName;
	}


	/**
	 * @return the wobNumber
	 */
	public String getWobNumber() {
		return wobNumber;
	}


	/**
	 * @param wobNumber the wobNumber to set
	 */
	public void setWobNumber(final String wobNumber) {
		this.wobNumber = wobNumber;
	}
	
	

}
