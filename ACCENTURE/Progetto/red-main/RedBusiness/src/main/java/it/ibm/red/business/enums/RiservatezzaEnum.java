package it.ibm.red.business.enums;

/**
 * @author SLac
 *
 */
public enum RiservatezzaEnum {
	
	/**
	 * 
	 */
	PUBBLICO(0, "Pubblico"),
	/**
	 * 
	 */
	RISERVATO(1, "Riservato");
	
	/**
	 * 
	 */
	private int id;
	/**
	 * 
	 */
	private String descrizione;
	
	/**
	 * @param id
	 * @param descrizione
	 */
	RiservatezzaEnum(final int id, final String descrizione) {
		this.id = id;
		this.descrizione = descrizione;
	}

	/**
	 * @return
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * @return
	 */
	public int getId() {
		return id;
	}

}
