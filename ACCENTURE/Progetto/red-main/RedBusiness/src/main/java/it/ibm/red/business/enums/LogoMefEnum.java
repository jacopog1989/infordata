package it.ibm.red.business.enums;

/**
 * The Enum LogoMEFEnum.
 *
 * @author m.crescentini
 * 
 *	Enum dei loghi MEF personalizzati per gli AOO. 
 */
public enum LogoMefEnum {

	/**
	 * Default.
	 */
	DEFAULT(0, LogoMefEnum.PATH_LOGO_RED_EVO, LogoMefEnum.HEADER_IMAGE_LEFT_RGS, "Immagine AOO Generica"),
//	DEFAULT(0, "/resources/images/Logo_mef_new.png", "headerImageLeft", "Immagine AOO Generica"),

	/**
	 * RGS.
	 */
	RGS(1, LogoMefEnum.PATH_LOGO_RED_EVO, LogoMefEnum.HEADER_IMAGE_LEFT_RGS, "Immagine AOO RGS"),
	
	/**
	 * RGS - RL.
	 */
	RGS_RL(2, LogoMefEnum.PATH_LOGO_RED_EVO, LogoMefEnum.HEADER_IMAGE_LEFT_RGS, "Immagine AOO RGS RL"),

	/**
	 * DSII.
	 */
	DSII(3, LogoMefEnum.PATH_LOGO_RED_EVO, LogoMefEnum.HEADER_IMAGE_LEFT_RGS, "Immagine AOO DAG");
	
	private static final String PATH_LOGO_RED_EVO= "/resources/images/logoredevo.png"; 
	private static final String HEADER_IMAGE_LEFT_RGS = "headerImageLeftRGS";
	

	/**
	 * ID.
	 */
	private Integer id;

	/**
	 * Path relativo del logo.
	 */
	private String relativePath;

	/**
	 * Classe CSS associata al logo.
	 */
	private String cssClass;

	/**
	 * Title associato al logo.
	 */
	private String title;
	
	/**
	 * Costruttore di default.
	 * @param id
	 * @param relativePath
	 * @param cssClass
	 * @param title
	 */
	LogoMefEnum(final Integer id, final String relativePath, final String cssClass, final String title) {
		this.id = id;
		this.relativePath = relativePath;
		this.cssClass = cssClass;
		this.title = title;
	}

	/**
	 * Restituisce il valore dell'enum
	 * in base all'id in input.
	 * @param id
	 * @return
	 */
	public static LogoMefEnum getById(final Integer id) {
		LogoMefEnum selectedLogo = DEFAULT;

		for (LogoMefEnum logo : LogoMefEnum.values()) {
			if (logo.getId().equals(id)) {
				selectedLogo = logo;
				break;
			}
		}
		return selectedLogo;
	}

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Restituisce il path del logo.
	 * @return relativePath
	 */
	public String getRelativePath() {
		return relativePath;
	}

	/**
	 * Restituisce lo styleClass.
	 * @return cssClass
	 */
	public String getCssClass() {
		return cssClass;
	}

	/**
	 * Restituisce il title del logo.
	 * @return title
	 */
	public String getTitle() {
		return title;
	}
}