/**
 * 
 */
package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.enums.TipoAssegnazioneEnum;

/**
 * @author APerquoti
 *
 */
public interface ITipoAssegnazioneFacadeSRV extends Serializable {
	
	/**
	 * recupero dei tipi assegnazione utilizzati per l'assegnazione iter manuale.
	 * 
	 * @param idAoo
	 * @return
	 */
	List<TipoAssegnazioneEnum> getAllByIdAooIterManuale(Integer idAoo);

}
