package it.ibm.red.business.pcollector;

/**
 * ClassLoader per il caricamento dei collector dal DB.
 */
public class ByteClassLoader extends ClassLoader {

	/**
	 * Nome.
	 */
	private final String name;

	/**
	 * Contenuto.
	 */
	private final byte[] data;

	/**
	 * Costruttore del Loader.
	 * 
	 * @param parent
	 * @param inName
	 * @param inData
	 */
	public ByteClassLoader(final ClassLoader parent, final String inName, final byte[] inData) {
		super(parent);
		name = inName;
		data = inData;
	}

	/**
	 * Restituisce la classe che ha il nome in ingresso. Restituisce null se la
	 * classe non viene trovata.
	 * 
	 * @param inName
	 * @return classe trovata, o null se non esistente
	 */
	@Override
	public Class findClass(final String inName) {
		Class out = null;
		if (inName.equalsIgnoreCase(name) && data != null) {
			out = defineClass(name, data, 0, data.length);
		}
		return out;
	}

}