package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.ComunicazioneProtocolloArgoDTO;


/**
 * The Interface IProtocollazioneArgoFacadeSRV.
 *
 * @author m.crescentini
 * 
 *         Facade per la comunicazione verso il sistema ARGO del protocollo staccato in fase di firma.
 */
public interface IComunicazioneProtocolliArgoFacadeSRV extends Serializable {
	
	
	/**
	 * Elabora una comunicazione di protocollo verso il sistema ARGO, eseguendo una chiamata REST.
	 * 
	 * @param comunicazioneProtocollo Informazioni necessarie per comunicare il protocoll al sistema ARGO
	 * @return TRUE se la comunicazione è andata a buon fine (HTTP status 200 della chiamata REST), FALSE altrimenti
	 */
	boolean elaboraComunicazioneProtocollo(ComunicazioneProtocolloArgoDTO comunicazioneProtocollo);
	
	
	/**
	 * Recupera la prossima richiesta di comunicazione del protocollo verso il sistema ARGO ancora da lavorare.
	 * 
	 * @param idAoo
	 * @return Informazioni necessarie per comunicare il protocoll al sistema ARGO
	 */
	ComunicazioneProtocolloArgoDTO recuperaComunicazioneProtocolloDaLavorare(long idAoo);

}
