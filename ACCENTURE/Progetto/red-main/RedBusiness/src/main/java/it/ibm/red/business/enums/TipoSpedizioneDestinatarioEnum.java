package it.ibm.red.business.enums;

import java.util.Collection;

import it.ibm.red.business.constants.Constants;

/**
 * The Enum TipoSpedizioneEnum.
 *
 * @author CPIERASC
 * 
 *         Enum dei possibili tipi di spedizione di un documento.
 */
public enum TipoSpedizioneDestinatarioEnum {
	
	/**
	 * Non definito.
	 */
	IGNOTO(-1, "Ignoto"),
	
	/**
	 * Interno.
	 */
	INTERNO(0, "Interno"),

	/**
	 * Cartaceo. 
	 * Sottoinsieme del tipoDestinatario esterno
	 */
	CARTACEO(1, "Cartaceo"),

	/**
	 * Elettronico.
	 * Sottoinsieme del tipoDestinatario esterno
	 */
	ELETTRONICO(2, "Elettronico");

	/**
	 * Identificativo azione.
	 */
	private Integer id;
	/**
	 * Descrizione azione.
	 */
	private String descrizione;
	
	/**
	 * Costruttore.
	 * 
	 * @param inId			identificativo
	 * @param inDescrizione	descrizione
	 */
	TipoSpedizioneDestinatarioEnum(final Integer inId, final String inDescrizione) {
		this.id = inId;
		this.descrizione = inDescrizione;
	}
	
	/**
	 * @see ISignSRV -> getTipoSpedizione
	 * @param destinatari
	 * @return output
	 * @deprecated
	 */
	@Deprecated
	public static int getIdTipoSpedizione(final Collection<String> destinatari) {
		int output;
		boolean bElettronico = false;
		boolean bCartaceo = false;
		boolean bInterni = false;
		
		if (destinatari != null) {
			
			//ciclo i destinatari di un documento per determinare l'id spedizione 
			for (final String dest : destinatari) {
				Integer idTipoSpedizione;
				//splitto ogni singolo destinatario perche' composto da diverse informazioni
				final String[] destSplit = dest.split("\\,");
				
				final String tipoDestinatario = destSplit[3];
				
				//prima determino che tipo di destinatario è (Esterno/Interno)
				if (Constants.TipoDestinatario.ESTERNO.equals(tipoDestinatario)) {
					
					//se esterno determino se è (Elettronico/Cartaceo)
					idTipoSpedizione = Integer.parseInt(destSplit[2]);
					if (Constants.TipoSpedizione.MEZZO_ELETTRONICO.equals(idTipoSpedizione)) {
						bElettronico = true;
					} else {
						bCartaceo = true;
					}
				} else {
					bInterni = true;
				}
				
				//se risulta sia elettronico che cartaceo interrompo
				if (bElettronico && bCartaceo) {
					break;
				}
			}
		}
		
		//una volta ciclato la totalita' dei destinatari determino se e' (Interno/Elettronico/Cartaceo/Ignoto)
		if ((bElettronico && bCartaceo) || (bInterni && (bElettronico || bCartaceo))) {
			output = TipoSpedizioneDestinatarioEnum.CARTACEO.getId();
		} else if (bElettronico) {
			output = TipoSpedizioneDestinatarioEnum.ELETTRONICO.getId();
		} else if (bInterni) {
			output = TipoSpedizioneDestinatarioEnum.INTERNO.getId();
		} else {
			output = TipoSpedizioneDestinatarioEnum.CARTACEO.getId();
		}
		return output;
	}

	/**
	 * Getter descrizione.
	 * 
	 * @return	descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Getter identificativo.
	 * 
	 * @return	identificativo
	 */
	public Integer getId() {
		return id;
	}
	
	
	/**
	 * Verifica che sia ignoto verificando una incoerenza tra i metadati del documento.
	 * 
	 * @param assegnatariDocumento corrisponde al metadato del documento elencoLibroFirma
	 * @param metadatiCount corrisponde al metadati del docuemnto count
	 * @return il risultato della verifica
	 */
	public boolean isInvalid(final Collection<String> assegnatariDocumento, final Integer metadatiCount) {
		return !metadatiCount.equals(assegnatariDocumento.size());
	}

}
