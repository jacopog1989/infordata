package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IAnagraficaDipendenteDAO;
import it.ibm.red.business.dto.AnagraficaDipendenteDTO;
import it.ibm.red.business.dto.IndirizzoDTO;
import it.ibm.red.business.dto.RicercaAnagDipendentiDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IAnagraficaDipendentiSRV;

/**
 * Service per la gestione delle anagrafiche.
 */
@Service
@Component
public class AnagraficaDipendentiSRV extends AbstractService implements IAnagraficaDipendentiSRV {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 5881030494507530259L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AnagraficaDipendentiSRV.class.getName());

	/**
	 * Dao anagrafica dipendenti.
	 */
	@Autowired
	private IAnagraficaDipendenteDAO anagraficaDipendenteDAO;

	/**
	 * Effettua la modifica dell'anagrafica.
	 * 
	 * @see it.ibm.red.business.service.facade.IAnagraficaDipendentiFacadeSRV#modifica(it.ibm.red.business.dto.AnagraficaDipendenteDTO)
	 */
	@Override
	public void modifica(final AnagraficaDipendenteDTO newAnagrafica) {
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			anagraficaDipendenteDAO.modifica(con, newAnagrafica);
		} catch (final Exception e) {
			LOGGER.error("Errore nella modifica dell'anagrafica" + e.getMessage(), e);
			rollbackConnection(con);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Rende persistente sulla base dati l'anagrafica <code> newAnagrafica </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAnagraficaDipendentiFacadeSRV#salva(long,
	 *      it.ibm.red.business.dto.AnagraficaDipendenteDTO)
	 */
	@Override
	public AnagraficaDipendenteDTO salva(final long idAoo, final AnagraficaDipendenteDTO newAnagrafica) {
		AnagraficaDipendenteDTO out = null;
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			out = anagraficaDipendenteDAO.salva(con, idAoo, newAnagrafica);
		} catch (final Exception e) {
			LOGGER.error("Errore nel salvataggio dell'anagrafica" + e.getMessage(), e);
			rollbackConnection(con);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		return out;
	}

	/**
	 * Esegue la ricerca sulla base dati dell'anagrafica restituendone tutti i
	 * risultati.
	 * 
	 * @see it.ibm.red.business.service.facade.IAnagraficaDipendentiFacadeSRV#ricerca(long,
	 *      it.ibm.red.business.dto.RicercaAnagDipendentiDTO)
	 */
	@Override
	public Collection<AnagraficaDipendenteDTO> ricerca(final long idAoo, final RicercaAnagDipendentiDTO ricercaDTO) {
		Collection<AnagraficaDipendenteDTO> out = null;
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			
			out = anagraficaDipendenteDAO.ricerca(con, idAoo, ricercaDTO);
			
		} catch (final Exception e) {
			LOGGER.error("Errore nella ricerca delle anagrafiche" + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		return out;
	}

	/**
	 * Memorizza un indirizzo sulla base dati associandolo all'anagrafica
	 * identificata dall' <code> idAnagraficaDipendente </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAnagraficaDipendentiFacadeSRV#salvaIndirizzo(java.lang.Integer,
	 *      it.ibm.red.business.dto.IndirizzoDTO)
	 */
	@Override
	public IndirizzoDTO salvaIndirizzo(final Integer idAnagraficaDipendente, final IndirizzoDTO newIndirizzo) {
		IndirizzoDTO out = null;
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			out = anagraficaDipendenteDAO.salvaIndirizzo(con, idAnagraficaDipendente, newIndirizzo);
		} catch (final Exception e) {
			LOGGER.error("Errore nel salvataggio dell'indirizzo" + e.getMessage(), e);
			rollbackConnection(con);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		return out;
	}

	/**
	 * Effettua la modifica dell'indirizzo associato all'anagrafica identificata
	 * dall' <code> idAnagraficaDipendente </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAnagraficaDipendentiFacadeSRV#modificaIndirizzo(java.lang.Integer,
	 *      it.ibm.red.business.dto.IndirizzoDTO)
	 */
	@Override
	public void modificaIndirizzo(final Integer idAnagraficaDipendente, final IndirizzoDTO newIndirizzo) {
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			anagraficaDipendenteDAO.modificaIndirizzo(con, idAnagraficaDipendente, newIndirizzo);
		} catch (final Exception e) {
			LOGGER.error("Errore nella modifica dell'indirizzo" + e.getMessage(), e);
			rollbackConnection(con);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Restituisce i DUG.
	 * 
	 * @see it.ibm.red.business.service.facade.IAnagraficaDipendentiFacadeSRV#getDug()
	 */
	@Override
	public Collection<SelectItemDTO> getDug() {
		Collection<SelectItemDTO> out = null;
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			out = anagraficaDipendenteDAO.getDug(con);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dei dug." + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		return out;
	}

	/**
	 * Restituisce gli indirizzi validi per l'anagrafica identificata dall'
	 * <code> idAnagrafica </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAnagraficaDipendentiFacadeSRV#getIndirizzi(java.lang.Integer)
	 */
	@Override
	public Collection<IndirizzoDTO> getIndirizzi(final Integer idAnagrafica) {
		Collection<IndirizzoDTO> out = null;
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			out = anagraficaDipendenteDAO.getIndirizzi(con, idAnagrafica);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero degli indirizzi." + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		return out;
	}

	/**
	 * Elimina dalla base dati l'indirizzo identificato dall'
	 * <code> idIndirizzo </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAnagraficaDipendentiFacadeSRV#removeIndirizzo(java.lang.Integer)
	 */
	@Override
	public void removeIndirizzo(final Integer idIndirizzo) {
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			anagraficaDipendenteDAO.removeIndirizzo(con, idIndirizzo);
		} catch (final Exception e) {
			LOGGER.error("Errore nella rimozione dell'indirizzo." + e.getMessage(), e);
			rollbackConnection(con);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Rimuove dalla base dati l'anagrafica dipendenti identificata dall'
	 * <code> id </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAnagraficaDipendentiFacadeSRV#remove(java.lang.Integer)
	 */
	@Override
	public void remove(final Integer id) {
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			final Collection<IndirizzoDTO> indirizzi = anagraficaDipendenteDAO.getIndirizzi(con, id);
			for (final IndirizzoDTO indirizzo:indirizzi) {
				anagraficaDipendenteDAO.removeIndirizzo(con, indirizzo.getId());
			}
			anagraficaDipendenteDAO.remove(con, id);
		} catch (final Exception e) {
			LOGGER.error("Errore nella rimozione dell'anagrafica." + e.getMessage(), e);
			rollbackConnection(con);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}

}