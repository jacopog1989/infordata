package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * The Class RedWsFlusso.
 *
 * @author m.crescentini
 * 
 *         Entità che mappa la tabella REDWS_FLUSSO.
 */
public class RedWsFlusso implements Serializable {
	
	private static final long serialVersionUID = 4273511730344755010L;
	
	/**
	 * Flusso.
	 */
	private final int idFlusso;
	
	/**
	 * Codice.
	 */
	private final String codice;
	
	/**
	 * Nome interfaccia.
	 */
	private final String interfaceName;
	
	/**
	 * Flag uscita.
	 */
	private final Integer flagUscita;

	
	/**
	 * @param idFlusso
	 * @param codice
	 * @param flagUscita
	 */
	public RedWsFlusso(final int idFlusso, final String codice, final String interfaceName, final Integer flagUscita) {
		super();
		this.idFlusso = idFlusso;
		this.codice = codice;
		this.interfaceName = interfaceName;
		this.flagUscita = flagUscita;
	}


	/**
	 * @return
	 */
	public int getIdFlusso() {
		return idFlusso;
	}


	/**
	 * @return
	 */
	public String getCodice() {
		return codice;
	}


	/**
	 * @return the interfaceName
	 */
	public String getInterfaceName() {
		return interfaceName;
	}


	/**
	 * @return
	 */
	public Integer getFlagUscita() {
		return flagUscita;
	}
	
}
