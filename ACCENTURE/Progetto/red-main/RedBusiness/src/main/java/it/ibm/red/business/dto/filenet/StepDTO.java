package it.ibm.red.business.dto.filenet;


import java.util.ArrayList;
import java.util.Collection;

import it.ibm.red.business.dto.AbstractDTO;
import it.ibm.red.business.enums.StepTypeEnum;

/**
 * The Class StepDTO.
 *
 * @author CPIERASC
 * 
 *         DTO step storico visuale.
 */
public class StepDTO extends AbstractDTO {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Label.
	 */
	private String label;

	/**
	 * Header.
	 */
	private EventLogDTO header;

	/**
	 * Body.
	 */
	private Collection<EventLogDTO> body = new ArrayList<>();

	/**
	 * Tipo step.
	 */
	private StepTypeEnum stepType;

	/**
	 * Costruttore.
	 * 
	 * @param inStepType	tipo step
	 * @param inEventLog	event log
	 */
	public StepDTO(final StepTypeEnum inStepType, final EventLogDTO inEventLog) {
		stepType = inStepType;
		label = " ";
		header = inEventLog;
	}
	
	
	/**
	 * Costruttore
	 * 
	 * @param inStepType
	 * @param inEventLog
	 * @param label
	 */
	public StepDTO(final StepTypeEnum inStepType, final EventLogDTO inEventLog, final String label) {
		this.stepType = inStepType;
		this.label = label;
		this.header = inEventLog;
	}

	/**
	 * Costruttore
	 * 
	 * @param inStepType
	 * @param inEventLog
	 * @param label
	 */
	public StepDTO(final StepTypeEnum inStepType, final EventLogDTO inEventLog, final String inLabel, final Collection<EventLogDTO> inBody) {
		this.stepType = inStepType;
		this.label = inLabel;
		this.header = inEventLog;
		this.body = inBody;
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param eventLog	event log
	 */
	public StepDTO(final EventLogDTO eventLog) {
		if ((eventLog.getTipo() != null && "R".equalsIgnoreCase(eventLog.getTipo())) && ("".equals(eventLog.getDataChiusura()))) {
			stepType = StepTypeEnum.RIFIUTATO;
		} else if ((eventLog.getTipo() != null && "I".equalsIgnoreCase(eventLog.getTipo())) && ("".equals(eventLog.getDataChiusura()))) {
			stepType = StepTypeEnum.IN_CORSO;
		} else {
			stepType = StepTypeEnum.COMPLETATO;
		}
		header = eventLog;
		label = "<div class='ui-widget-content myLabel'>" + eventLog.getDataAssegnazione() + "</div>";
	}

	/**
	 * Getter.
	 * 
	 * @return	header
	 */
	public final EventLogDTO getHeader() {
		return header;
	}

	/**
	 * Getter.
	 * 
	 * @return	body
	 */
	public final Collection<EventLogDTO> getBody() {
		return body;
	}

	/**
	 * Getter.
	 * 
	 * @return	tipo step
	 */
	public final StepTypeEnum getStepType() {
		return stepType;
	}

	/**
	 * Getter.
	 * 
	 * @return	label
	 */
	public final String getLabel() {
		return label;
	}

	/**
	 * Setter.
	 * 
	 * @param bodyEventList	lista eventi body
	 */
	public final void setBody(final Collection<EventLogDTO> bodyEventList) {
		body = bodyEventList;
}
	
}