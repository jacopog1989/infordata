package it.ibm.red.business.persistence.model;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class Calendario.
 *
 * @author CPIERASC
 * 
 *         Entità che mappa la tabella CALENDARIO.
 */
public class Calendario implements Serializable {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -1931455455796868398L;
	
	/**
	 * Contenuto.
	 */
	private final String contenuto;

	/**
	 * Data inizio.
	 */
	private final Date dataInizio;

	/**
	 * Data scadenza.
	 */
	private final Date dataScadenza;

	/**
	 * Data creazione.
	 */
	private final Date dataTime;

	/**
	 * Identificativo evento.
	 */
	private final Integer idEvento;

	/**
	 * Titolo.
	 */
	private final String titolo;

	/**
	 * Urgente.
	 */
	private final Integer urgente;

	/**
	 * Costruttore.
	 * 
	 * @param inIdEvento		id evento
	 * @param inDataInizio		data inizio
	 * @param inDataScadenza	data scadenza
	 * @param inTitolo			titolo
	 * @param inContenuto		contenuto
	 * @param inDataTime		data creazione
	 * @param inUrgente			urgenza
	 */
	public Calendario(final Integer inIdEvento, final Date inDataInizio, 
			final Date inDataScadenza, final String inTitolo, final String inContenuto, final Date inDataTime, final Integer inUrgente) {
		super();
		this.idEvento = inIdEvento;
		this.dataInizio = inDataInizio;
		this.dataScadenza = inDataScadenza;
		this.titolo = inTitolo;
		this.contenuto = inContenuto;
		this.dataTime = inDataTime;
		this.urgente = inUrgente;
	}

	/**
	 * Getter contenuto.
	 * 
	 * @return	contenuto
	 */
	public final String getContenuto() {
		return contenuto;
	}

	/**
	 * Getter data inizio.
	 * 
	 * @return	data inizio
	 */
	public final Date getDataInizio() {
		return dataInizio;
	}

	/**
	 * Getter data scadenza.
	 * 
	 * @return	data scadenza
	 */
	public final Date getDataScadenza() {
		return dataScadenza;
	}

	/**
	 * Getter data creazione.
	 * 
	 * @return	data creazione
	 */
	public final Date getDataTime() {
		return dataTime;
	}

	/**
	 * Getter id evento.
	 * 
	 * @return	id evento
	 */
	public final Integer getIdEvento() {
		return idEvento;
	}

	/**
	 * Getter titolo.
	 * 
	 * @return	titolo
	 */
	public final String getTitolo() {
		return titolo;
	}

	/**
	 * Getter urgenza.
	 * 
	 * @return	urgenza
	 */
	public final Integer getUrgente() {
		return urgente;
	}

}
