package it.ibm.red.business.dto;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import it.ibm.red.business.utils.LogStyleNotNull;

/**
 * Classe ProtocollazionePECConfigDTO.
 */
public class ProtocollazionePECConfigDTO {

	/** 
	 * Identificativo aoo
	 */
	private Integer idAoo;
	
	/** 
	 * Casella postale
	 */
	private String casellaPostale;
	
	/** 
	 * Identificativo nodo destinatario
	 */
	private Integer idNodoDestinatario;
	
	/** 
	 * Identificativo ufficio protocollatore
	 */
	private Integer idUfficioProtocollatore;
	
	/** 
	 * Identificativo utente protocollatore
	 */
	private Integer idUtenteProtocollatore;
	
	/** 
	 * Identificativo tipologia documento
	 */
	private Integer idTipologiaDocumento;
	
	/** 
	 * Identificativo tipologia procedimento
	 */
	private Integer idTipologiaProcedimento;
	
	/** 
	 * Indice classificazione
	 */
	private String indiceClassificazione;
	
	/** 
	 * Identificativo faldone
	 */
	private Integer idFaldone;
	
	/** 
	 * Estensioni permesse
	 */
	private List<String> allowedFileExtensions;
	
	/**
	 * Costruttore protocollazione PEC config DTO.
	 *
	 * @param idAoo the id aoo
	 * @param casellaPostale the casella postale
	 * @param idNodoDestinatario the id nodo destinatario
	 * @param idUfficioProtocollatore the id ufficio protocollatore
	 * @param idUtenteProtocollatore the id utente protocollatore
	 * @param idTipologiaDocumento the id tipologia documento
	 * @param idTipologiaProcedimento the id tipologia procedimento
	 * @param indiceClassificazione the indice classificazione
	 * @param idFaldone the id faldone
	 * @param allowedFileExtensions the allowed file extensions
	 */
	public ProtocollazionePECConfigDTO(final Integer idAoo, final String casellaPostale, final Integer idNodoDestinatario, final Integer idUfficioProtocollatore, 
			final Integer idUtenteProtocollatore, final Integer idTipologiaDocumento, final Integer idTipologiaProcedimento, final String indiceClassificazione,
			final Integer idFaldone, final List<String> allowedFileExtensions) {
		super();
		this.idAoo = idAoo;
		this.casellaPostale = casellaPostale;
		this.idNodoDestinatario = idNodoDestinatario;
		this.idUfficioProtocollatore = idUfficioProtocollatore;
		this.idUtenteProtocollatore = idUtenteProtocollatore;
		this.idTipologiaDocumento = idTipologiaDocumento;
		this.idTipologiaProcedimento = idTipologiaProcedimento;
		this.indiceClassificazione = indiceClassificazione;
		this.idFaldone = idFaldone;
		this.allowedFileExtensions = allowedFileExtensions;
	}

	/** 
	 * @return the id aoo
	 */
	public Integer getIdAoo() {
		return idAoo;
	}

	/** 
	 * @param idAoo the new id aoo
	 */
	public void setIdAoo(final Integer idAoo) {
		this.idAoo = idAoo;
	}

	/** 
	 * @return the casella postale
	 */
	public String getCasellaPostale() {
		return casellaPostale;
	}

	/** 
	 * @param casellaPostale the new casella postale
	 */
	public void setCasellaPostale(final String casellaPostale) {
		this.casellaPostale = casellaPostale;
	}

	/** 
	 * @return the id nodo destinatario
	 */
	public Integer getIdNodoDestinatario() {
		return idNodoDestinatario;
	}

	/** 
	 * @param idNodoDestinatario the new id nodo destinatario
	 */
	public void setIdNodoDestinatario(final Integer idNodoDestinatario) {
		this.idNodoDestinatario = idNodoDestinatario;
	}

	/** 
	 * @return the id ufficio protocollatore
	 */
	public Integer getIdUfficioProtocollatore() {
		return idUfficioProtocollatore;
	}

	/** 
	 * @param idUfficioProtocollatore the new id ufficio protocollatore
	 */
	public void setIdUfficioProtocollatore(final Integer idUfficioProtocollatore) {
		this.idUfficioProtocollatore = idUfficioProtocollatore;
	}

	/** 
	 * @return the id utente protocollatore
	 */
	public Integer getIdUtenteProtocollatore() {
		return idUtenteProtocollatore;
	}

	/** 
	 * @param idUtenteProtocollatore the new id utente protocollatore
	 */
	public void setIdUtenteProtocollatore(final Integer idUtenteProtocollatore) {
		this.idUtenteProtocollatore = idUtenteProtocollatore;
	}

	/** 
	 * @return the id tipologia documento
	 */
	public Integer getIdTipologiaDocumento() {
		return idTipologiaDocumento;
	}

	/** 
	 * @param idTipologiaDocumento the new id tipologia documento
	 */
	public void setIdTipologiaDocumento(final Integer idTipologiaDocumento) {
		this.idTipologiaDocumento = idTipologiaDocumento;
	}

	/** 
	 * @return the id tipologia procedimento
	 */
	public Integer getIdTipologiaProcedimento() {
		return idTipologiaProcedimento;
	}

	/** 
	 * @param idTipologiaProcedimento the new id tipologia procedimento
	 */
	public void setIdTipologiaProcedimento(final Integer idTipologiaProcedimento) {
		this.idTipologiaProcedimento = idTipologiaProcedimento;
	}

	/** 
	 * @return the indice classificazione
	 */
	public String getIndiceClassificazione() {
		return indiceClassificazione;
	}

	/** 
	 * @param indiceClassificazione the new indice classificazione
	 */
	public void setIndiceClassificazione(final String indiceClassificazione) {
		this.indiceClassificazione = indiceClassificazione;
	}

	/** 
	 * @return the id faldone
	 */
	public Integer getIdFaldone() {
		return idFaldone;
	}

	/** 
	 * @param idFaldone the new id faldone
	 */
	public void setIdFaldone(final Integer idFaldone) {
		this.idFaldone = idFaldone;
	}

	/** 
	 * @return the allowed file extensions
	 */
	public List<String> getAllowedFileExtensions() {
		return allowedFileExtensions;
	}

	/** 
	 * @param allowedFileExtensions the new allowed file extensions
	 */
	public void setAllowedFileExtensions(final List<String> allowedFileExtensions) {
		this.allowedFileExtensions = allowedFileExtensions;
	}
	
	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, new LogStyleNotNull()); 
	}
	
}
