package it.ibm.red.business.enums;

/**
 * @author SLac
 *
 */
public enum FunzionalitaEnum {
	

	/**
	 * Valore.
	 */
	MAIL_LEAF(1, "Foglia Posta Elettronica"),

	/**
	 * Valore.
	 */
	RUBRICA_MAIL_LEAF(2, "Gestione Rubrica Mail"),

	/**
	 * Valore.
	 */
	INOLTRA_MAIL_DA_CODA(3, "Inoltra Mail da Coda"),

	/**
	 * Valore.
	 */
	TAB_SPEDIZIONE(4, "Tab Spedizione");
	
	/**
	 * Identificativo.
	 */
	private Integer idFunzionalita;
	
	/**
	 * Descrizione.
	 */
	private String descrFunzionalita;
	
	/**
	 * Costruttore.
	 * @param id
	 * @param descr
	 */
	FunzionalitaEnum(final Integer id, final String descr) {
		this.idFunzionalita = id;
		this.descrFunzionalita = descr;
	}
	
	/**
	 * Restituisce l'id della funzionalita.
	 * @return id funzionalita
	 */
	public Integer getIdFunzionalita() {
		return idFunzionalita;
	}

	/**
	 * Restituisce la descrizione della funzionalita.
	 * @return descrizione funzionalita
	 */
	public String getDescrFunzionalita() {
		return descrFunzionalita;
	}
}
