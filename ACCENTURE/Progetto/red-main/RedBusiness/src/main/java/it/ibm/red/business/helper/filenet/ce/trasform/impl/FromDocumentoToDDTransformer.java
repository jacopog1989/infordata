/**
 * 
 */
package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.Date;

import com.filenet.api.core.Document;

import it.ibm.red.business.dao.ILookupDAO;
import it.ibm.red.business.dto.DecretoDirigenzialeDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.utils.StringUtils;

/**
 * @author APerquoti
 * 
 * 			Trasformer documento generic Doc
 *
 */
public class FromDocumentoToDDTransformer extends TrasformerCE<DecretoDirigenzialeDTO> {

	/**
	 * Serializzable
	 */
	private static final long serialVersionUID = -376051212611046568L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentoToDDTransformer.class.getName());
	
	/**
	 * Dimensione massima soggetto.
	 */
	private static final int MAX_ABSTRACT_SIZE = 100;
	
	/**
	 * DAO.
	 */
	private ILookupDAO lookupDAO;
	
	/**
	 * 
	 */
	public FromDocumentoToDDTransformer() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_DD);
		lookupDAO = ApplicationContextProvider.getApplicationContext().getBean(ILookupDAO.class);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE#trasform(com.filenet.api.core.Document, java.sql.Connection).
	 */
	@Override
	public DecretoDirigenzialeDTO trasform(final Document document, final Connection connection) {
		try {
			Integer numDoc = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			String ogg = StringUtils.getTextAbstract((String) getMetadato(document, PropertiesNameEnum.OGGETTO_METAKEY), MAX_ABSTRACT_SIZE);
			Integer annoDoc = (Integer) getMetadato(document, PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY);
			
			Date dataCreazione = (Date) getMetadato(document, PropertiesNameEnum.DATA_CREAZIONE_METAKEY);
			
			//Pacchetto informazioni riguardanti il protocollo
			Integer numeroProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
			Integer annoProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
			Date dataProtocollazione = (Date) getMetadato(document, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);
			
//			====================ATTRIBUTI CALCOLATI========================
			//Recupero della tipologia del documento
			Integer idTipologiaDocumento = (Integer) getMetadato(document, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
			String tipologiaDocumento = null;
			if (idTipologiaDocumento != null) {
				tipologiaDocumento = lookupDAO.getDescTipoDocumento(idTipologiaDocumento.longValue(), connection);
			}
			
			return new DecretoDirigenzialeDTO(numDoc, ogg, tipologiaDocumento, numeroProtocollo, annoProtocollo,
					dataCreazione, annoDoc, dataProtocollazione);

		} catch (Exception e) {
			LOGGER.error("Errore nel recupero di un metadato durante la trasformazione da Document a MasterDocumentRedDTO: ", e);
			return null;
		}
	}

}
