package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;

import it.ibm.red.business.dto.CanaleTrasmissioneDTO;

/**
 * Interfaccia DAO canale trasmissione.
 */
public interface ICanaleTrasmissioneDAO extends Serializable {

	/**
	 * Ottiene tutti i canali di trasmissione.
	 * @param con
	 * @return canali di trasmissione
	 */
	Collection<CanaleTrasmissioneDTO> getAll(Connection con);

	/**
	 * Ottiene il canale di trasmissione dall'id.
	 * @param id
	 * @param connection
	 * @return canale di trasmissione
	 */
	CanaleTrasmissioneDTO getById(Integer id, Connection connection);

}
