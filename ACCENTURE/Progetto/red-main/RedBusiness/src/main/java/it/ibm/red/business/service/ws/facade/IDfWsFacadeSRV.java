package it.ibm.red.business.service.ws.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.MappingOrgNsdDTO;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.webservice.model.dfservice.types.messages.RedMappingOrganigrammaType;


/**
 * The Interface IDfWsFacadeSRV.
 *
 * @author a.dilegge
 * 
 *         Facade servizio web service dedicato al DF.
 */
public interface IDfWsFacadeSRV extends Serializable {
	
	/**
	 * Ottiene il mapping dell'organigramma.
	 * @param client
	 * @param request
	 * @return lista di mapping Nsd
	 */
	List<MappingOrgNsdDTO> mappingOrganigramma(RedWsClient client, RedMappingOrganigrammaType request);
	
}
