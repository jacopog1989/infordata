package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.provider.ServiceTaskProvider.ServiceTask;
import it.ibm.red.business.service.IChiudiSRV;
import it.ibm.red.business.service.INpsSRV;

/**
 * Service chiusura.
 */
@Service
@Component
public class ChiudiSRV extends AbstractService implements IChiudiSRV {
	
	/**
	 * UID.
	 */
	private static final long serialVersionUID = 7906415908849718015L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ChiudiSRV.class.getName());

	/**
	 * Gestore properties.
	 */
	private PropertiesProvider pp;
	
	/**
	 * Servizio nps.
	 */
	@Autowired
	private INpsSRV npsSRV;

	/**
	 * Esegue le azioni nel post construct.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IChiudiFacadeSRV#chiudi(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "chiudi")
	public EsitoOperazioneDTO chiudi(final UtenteDTO utente, final String wobNumber) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber, true, null);
		Connection connection = null;
		FilenetPEHelper peh = null;
		IFilenetCEHelper ceh = null;
		
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			peh = new FilenetPEHelper(utente.getFcDTO());
			ceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			
			final Map<String, Object> map = new HashMap<>();
			map.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId().intValue());
			final Document d = ceh.getDocumentByDTandAOO(("" + peh.getMetadato(wobNumber, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY))), (utente.getIdAoo()));
			final String guidDoc = d.get_Id().toString();
						
			// Se Riservato e destinatari cartacei effettua la spedizione automatica
			if (d.getProperties().isPropertyPresent(pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY)) 
					&& d.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY)) != null) {
				final boolean isRiservato = d.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY)).intValue() == 1;
				if (isRiservato) {
					map.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), "Spedizione Riservata");
				} else {
					map.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), "");
				}
			} else {
				map.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), "");
			}
			
			if (utente.getIdTipoProtocollo() != null && TipologiaProtocollazioneEnum.isProtocolloNPS(utente.getIdTipoProtocollo())
					&& d.getProperties().isPropertyPresent(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY)) 
					&& d.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY))  != null) {
				final String idProtocollo = d.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY));
									
				// infoProtocollo = NUMPROT/ANNOPROT_CODICEAOO
				final String infoProtocollo = new StringBuilder()
						.append(d.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY))).append("/")
						.append(d.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY)))
						.append("_").append(utente.getCodiceAoo()).toString();
				
				npsSRV.uploadAllegatiDocToAsyncNps(guidDoc, infoProtocollo, idProtocollo, utente, false, ceh, connection);
				
				final String idDocumento = d.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
				npsSRV.spedisciProtocolloUscitaAsync(utente, infoProtocollo, idProtocollo, new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()), null, null, idDocumento, connection);
			}
			
			// Si avanza il workflow
			if (!peh.nextStep(peh.getWorkFlowByWob(wobNumber, true), map, ResponsesRedEnum.CHIUDI.getResponse())) {
				esito.setNote("Si è verificato un errore nell'avanzamento del workflow.");
				
				rollbackConnection(connection);
			} else {
				final Integer numeroDoc = (Integer) TrasformerCE.getMetadato(d, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
				esito.setIdDocumento(numeroDoc);
				esito.setEsito(true);
				esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
				
				commitConnection(connection);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore nell'avanzamento del workflow.", e);
			rollbackConnection(connection);
			esito.setNote("Errore nell'avanzamento del workflow.");
		} finally {
			closeConnection(connection);
			logoff(peh);
			popSubject(ceh);
		}
		
		return esito;
	}
}
