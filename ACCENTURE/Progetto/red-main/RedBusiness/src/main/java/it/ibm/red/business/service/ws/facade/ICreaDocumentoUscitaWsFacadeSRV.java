package it.ibm.red.business.service.ws.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCreazioneDocumentoUscitaType;


/**
 * The Interface ICreaDocumentoUscitaWsFacadeSRV.
 *
 * @author m.crescentini
 * 
 *         Facade per il servizio di creazione di documenti in uscita tramite web service.
 */
public interface ICreaDocumentoUscitaWsFacadeSRV extends Serializable {
	
	/**
	 * Servizio per la creazione di un documento in uscita per RED.
	 * 
	 * @param docUscitaInput
	 * @return
	 */
	EsitoSalvaDocumentoDTO creaDocumentoUscitaRed(RedWsClient client, RedCreazioneDocumentoUscitaType docUscitaInput);

}