package it.ibm.red.business.dto;

import java.util.Date;

/**
 * DTO che definisce i dati di un documento conto consuntivo.
 */
public class DatiDocumentoContiConsuntiviDTO extends AbstractDTO {
	
	/**
	 * serial version UID.
	 */
	private static final long serialVersionUID = 5882280745370624877L;
	
	/**
	 * Data protocollo documento.
	 */
	private Date dataProtocollo;
	
	/**
	 * Numero Protocollo documento.
	 */
	private Integer numeroProtocollo;
	
	/**
	 * Utente assegnatario.
	 */
	private String utenteAssegnatario;
	
	/**
	 * Ufficio assegnatario.
	 */
	private String ufficioAssegnatario;
	
	/**
	 * Stato.
	 */
	private String stato;

	/**
	 * Restituisce la data protocollo documento.
	 * @return data protocollo documento
	 */
	public Date getDataProtocollo() {
		return dataProtocollo;
	}

	/**
	 * Imposta la data protocollo documento.
	 * 
	 * @param dataProtocollo
	 */
	public void setDataProtocollo(Date dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}

	/**
	 * Restituisce il numero protocollo documento.
	 * 
	 * @return numero protocollo documento
	 */
	public Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Imposta il numero protocollo documento.
	 * 
	 * @param numeroProtocollo
	 */
	public void setNumeroProtocollo(Integer numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	/**
	 * Restituisce l'utente assegnatario.
	 * 
	 * @return utente assegnatario
	 */
	public String getUtenteAssegnatario() {
		return utenteAssegnatario;
	}

	/**
	 * Imposta l'utente assegnatario.
	 * 
	 * @param utenteAssegnatario
	 */
	public void setUtenteAssegnatario(String utenteAssegnatario) {
		this.utenteAssegnatario = utenteAssegnatario;
	}

	/**
	 * Restituisce l'ufficio assegnatario.
	 * 
	 * @return ufficio assegnatario
	 */
	public String getUfficioAssegnatario() {
		return ufficioAssegnatario;
	}

	/**
	 * Imposta l'ufficio assegnatario.
	 * @param ufficioAssegnatario
	 */
	public void setUfficioAssegnatario(String ufficioAssegnatario) {
		this.ufficioAssegnatario = ufficioAssegnatario;
	}

	/**
	 * Restituisce lo stato.
	 * @return stato
	 */
	public String getStato() {
		return stato;
	}

	/**
	 * Imposta lo stato.
	 * @param stato
	 */
	public void setStato(String stato) {
		this.stato = stato;
	}

}
