package it.ibm.red.business.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.filenet.StepDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.enums.TipoOperazioneLibroFirmaEnum;
import it.ibm.red.business.nps.dto.ProtocolloNpsDTO;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class DetailDocumentoDTO.
 *
 * @author CPIERASC
 * 
 *         Data Transfer Object per i dati di un documento.
 */
public class DetailDocumentoDTO extends AbstractDTO {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 4694070483887083132L;

	/**
	 * Contributi.
	 */
	private Collection<ContributoDTO> contributi;

	/**
	 * Descrizione Tipo documento NPS.
	 */
	private String descrizioneTipoDocumentoNps;

	/**
	 * Oggetto protocollo NPS.
	 */
	private String oggettoProtocolloNps;

	/**
	 * Step creazione documento.
	 */
	private StepDTO stepCreazione;

	/**
	 * Flag riservato.
	 */
	private Boolean flagRiservato;

	/**
	 * Anno protocollo.
	 */
	private Integer annoProtocollo;

	/**
	 * Flag firma PDF.
	 */
	private Boolean flagFirmaPDF;

	/**
	 * Anno protocollo fascicolo.
	 */
	private Integer annoProtocolloFascicolo;

	/**
	 * Approvazioni.
	 */
	private Collection<String> approvazioni;

	/**
	 * Flag allegati presenti.
	 */
	private Boolean bAttachmentPresent;

	/**
	 * Flag trasformazione pdf in errore.
	 */
	private Boolean bTrasfPdfErrore;

	/**
	 * Flag trasformazione pdf in warning (errore che permette un nuovo tentativo
	 * senza passare dalla GA).
	 */
	private Boolean bTrasfPdfWarning;

	/**
	 * Data creazione fascicolo.
	 */
	private Date dataCreazioneFascicolo;

	/**
	 * Data protocollo.
	 */
	private Date dataProtocollo;

	/**
	 * Destinatari.
	 */
	private Collection<String> destinatari;

	/**
	 * Destinatari documento.
	 */
	private List<DestinatarioDTO> destinatariDocumento;

	/**
	 * Documenti fascicolo.
	 */
	private Collection<DocumentoFascicoloDTO> documentiFascicolo;

	/**
	 * Document title.
	 */
	private String documentTitle;

	/**
	 * Flag firma pades.
	 */
	private Boolean firmaPades;

	/**
	 * Flag cambio iter.
	 */
	private Boolean flagCambioIter;

	/**
	 * Flag fascicolo disabilitato.
	 */
	private Boolean flagDisableFascicolo;

	/**
	 * Flag abilita firma.
	 */
	private Boolean flagEnableFirma;

	/**
	 * Flag abilita firma autografa.
	 */
	private Boolean flagEnableFirmaAutografa;

	/**
	 * Flag abilita rifiuto.
	 */
	private Boolean flagEnableRifiuta;

	/**
	 * Flag abilita sigla.
	 */
	private Boolean flagEnableSigla;

	/**
	 * Flag abilita vista.
	 */
	private Boolean flagEnableVista;

	/**
	 * Flag firma autografa red (utilizzato per nascondere il documento dal libro
	 * firma).
	 */
	private Boolean flagFirmaAutografaRM;

	/**
	 * Flag firma digitale.
	 */
	private Boolean flagFirmaDig;

	/**
	 * Flag Procedi da Corriere.
	 */
	private Boolean flagEnableProcediCorriere;
	
	/**
	 * Identificativo formato allegato.
	 */
	private Integer formatoAllegatoId;

	/**
	 * Guid documento.
	 */
	private String guid;

	/**
	 * Flag documento ha destinatari esterni.
	 */
	private boolean hasDestinatariEsterni;

	/**
	 * Flag documento ha destinatari interni.
	 */
	private boolean hasDestinatariInterni;

	/**
	 * Identificativo categoria documento.
	 */
	private Integer idCategoriaDocumento;

	/**
	 * Identificativo momento procollazinoe.
	 */
	private Integer idMomentoProtocollazione;

	/**
	 * Identificativo protocollo.
	 */
	private String idProtocollo;

	/**
	 * Identificativo ufficio creatore.
	 */
	private Integer idUfficioCreatore;

	/**
	 * Identiicativo utente creatore.
	 */
	private Integer idUtenteCreatore;

	/**
	 * Identificativo utente destinatario.
	 */
	private Integer idUtenteDestinatario;

	/**
	 * Indice classificazione fascicolo.
	 */
	private String indiceFascicolo;

	/**
	 * Iter approvativo.
	 */
	private String iterApprovativo;

	/**
	 * Motivo assegnazione.
	 */
	private String motivazioneAssegnazione;

	/**
	 * Nomefile.
	 */
	private String nomeFile;

	/**
	 * Numero di allegati.
	 */
	private Integer numAttachment;

	/**
	 * Numero documenti.
	 */
	private Integer numeroDocumento;

	/**
	 * Numero documento fascicolo.
	 */
	private String numeroDocumentoFascicolo;

	/**
	 * Numero protocollo.
	 */
	private Integer numeroProtocollo;

	/**
	 * Numero protocollo fascicolo.
	 */
	private Integer numeroProtocolloFascicolo;

	/**
	 * Oggetto.
	 */
	private String oggetto;

	/**
	 * Oggetto fascicolo.
	 */
	private String oggettoFascicolo;

	/**
	 * Operazioni.
	 */
	private TipoOperazioneLibroFirmaEnum operazione;

	/**
	 * Coda.
	 */
	private DocumentQueueEnum queue;

	/**
	 * Stato fascicolo.
	 */
	private String statoFascicolo;

	/**
	 * Storico.
	 */
	private Collection<StoricoDTO> storico;

	/**
	 * Tipologia documento.
	 */
	private String tipologiaDocumento;

	/**
	 * Ufficio mittente.
	 */
	private String ufficioMittente;

	/**
	 * Wob number.
	 */
	private String wobNumber;

	/**
	 * Content.
	 */
	private byte[] content;

	/**
	 * mimeType.
	 */
	private String mimeType;

	/**
	 * Data creazione.
	 */
	private Date dataCreazione;

	/**
	 * Data scadenza.
	 */
	private Date dataScadenza;

	/**
	 * Urgenza.
	 */
	private Boolean urgenza;

	/**
	 * Modello dati per timeline visuale.
	 */
	private List<StepDTO> steps;

	/**
	 * Step approvazioni.
	 */
	private List<ApprovazioneDTO> stepsApprovazioni;

	/**
	 * ID Tipologia Documento.
	 */
	private Long idTipologiaDocumento;

	/**
	 * ID Tipologia Procedimento.
	 */
	private Long idTipoProcedimento;

	/**
	 * Descrizione Registro Repertorio.
	 */
	private String descRegistroRepertorio;

	/**
	 * Descrizione Registro Repertorio.
	 */
	private Boolean isRegistroRepertorio;

	/**
	 * Codice dell'eventuale flusso a cui afferisce il documento.
	 */
	private String codiceFlusso;

	/**
	 * Descrizione indice classificazione fascicolo.
	 */
	private String descrizioneIndiceFascicolo;

	/**
	 * Sotto categoria uscita.
	 */
	private SottoCategoriaDocumentoUscitaEnum sottoCategoriaUscita;

	/**
	 * Metadati estesi.
	 */
	private Collection<MetadatoDTO> metadatiEstesi;

	/**
	 * Registrazione ausiliaria.
	 */
	private RegistrazioneAusiliariaDTO registrazioneAusiliaria;

	/**
	 * Label stampigliatura.
	 */
	private String labelStampigliaturaRR;

	/**
	 * Costruttore.
	 */
	public DetailDocumentoDTO() {
		super();
	}

	/**
	 * Costruttore.
	 *
	 * @param inGuid
	 *            guid
	 * @param inNumeroProtocollo
	 *            numero protocollo
	 * @param inAnnoProtocollo
	 *            anno protocollo
	 * @param inIdProtocollo
	 *            identificativo protocollo
	 * @param inDataProtocollo
	 *            data protocollo
	 * @param inDescrizioneTipoDocumentoNps
	 *            descrizione del tipo documento NPS
	 * @param inOggettoProtocolloNps
	 *            oggetto protocollo NPS
	 * @param inDocumentTitle
	 *            document title
	 * @param inNumeroDocumento
	 *            numero documento
	 * @param inTipologiaDocumento
	 *            tipologia documento
	 * @param inOggetto
	 *            oggetto
	 * @param inQueue
	 *            coda
	 * @param inUfficioMittente
	 *            ufficio mittente
	 * @param inIdUfficioCreatore
	 *            identificativo ufficio creatore
	 * @param inIdUtenteCreatore
	 *            identificativo utente creatore
	 * @param inIterApprovativo
	 *            iter approvativo
	 * @param inDestinatari
	 *            destinatari
	 * @param inDestinatariDocumento
	 *            destinatari documento
	 * @param inApprovazioni
	 *            approvazioni
	 * @param inOperazione
	 *            operazione
	 * @param inNumAttachment
	 *            numero di allegati
	 * @param inTrasfPdfErrore
	 *            flag trasformazione pdf in errore
	 * @param inIdCategoriaDocumento
	 *            identificativo categoria documento
	 * @param inNomefile
	 *            nomefile
	 * @param inHasDestinatariInterni
	 *            flag destinatari interni presenti
	 * @param inHasDestinatariEsterni
	 *            flag destinatari esterni presenti
	 * @param inFormatoAllegatoId
	 *            identificativo formato allegato
	 * @param inFlagFirmaAutografaRM
	 *            flag firma autografa RED
	 * @param inStorico
	 *            storico
	 * @param inIdMomentoProtocollazione
	 *            identificativo momento protocollazione
	 * @param inDataCreazione
	 *            data creazionew
	 * @param inDataScadenza
	 *            data scadenza
	 * @param inUrgenza
	 *            urgenza
	 * @param inFlagRiservato
	 *            flag riservato
	 * @param inFlagFirmaPDF
	 *            the in flag firma PDF
	 * @param inIdTipologiaDocumento
	 * @param inMimeType
	 * @param inIdTipoProcedimento
	 * @param codiceFlusso
	 *            codice del flusso a cui afferisce il documento
	 */
	public DetailDocumentoDTO(final String inGuid, final Integer inNumeroProtocollo, final Integer inAnnoProtocollo, final String inIdProtocollo, final Date inDataProtocollo, 
			final String inDescrizioneTipoDocumentoNps, final String inOggettoProtocolloNps, final String inDocumentTitle,
			final Integer inNumeroDocumento, final String inTipologiaDocumento, final String inOggetto, final DocumentQueueEnum inQueue, final String inUfficioMittente, 
			final Integer inIdUfficioCreatore, final Integer inIdUtenteCreatore,
			final String inIterApprovativo, final Collection<String> inDestinatari, final List<DestinatarioDTO> inDestinatariDocumento, 
			final Collection<String> inApprovazioni, final TipoOperazioneLibroFirmaEnum inOperazione, final Integer inNumAttachment, 
			final Boolean inTrasfPdfErrore, final Boolean inTrasfPdfWarning, final Integer inIdCategoriaDocumento, final String inNomefile, final boolean inHasDestinatariInterni, 
			final boolean inHasDestinatariEsterni, final Integer inFormatoAllegatoId, final Boolean inFlagFirmaAutografaRM, final Collection<StoricoDTO> inStorico, 
			final Integer inIdMomentoProtocollazione, final Date inDataCreazione,	final Date inDataScadenza, final Boolean inUrgenza, final Boolean inFlagRiservato,
			final Boolean inFlagFirmaPDF, final Long inIdTipologiaDocumento, final Long inIdTipoProcedimento, final String inMimeType, 
			final String inCodiceFlusso, final SottoCategoriaDocumentoUscitaEnum inSottoCategoriaUscita, final Collection<MetadatoDTO> inMetadatiEstesi,
			final RegistrazioneAusiliariaDTO inRegistrazioneAusiliaria) {
		super();
		this.guid = inGuid;
		this.numeroProtocollo = inNumeroProtocollo;
		this.annoProtocollo = inAnnoProtocollo;
		this.idProtocollo = inIdProtocollo;
		this.documentTitle = inDocumentTitle;
		this.numeroDocumento = inNumeroDocumento;
		this.tipologiaDocumento = inTipologiaDocumento;
		this.oggetto = inOggetto;
		this.queue = inQueue;
		this.ufficioMittente = inUfficioMittente;
		this.iterApprovativo = inIterApprovativo;
		this.destinatari = inDestinatari;
		this.destinatariDocumento = inDestinatariDocumento;
		this.approvazioni = inApprovazioni;
		this.operazione = inOperazione;
		this.idUfficioCreatore = inIdUfficioCreatore;
		this.idUtenteCreatore = inIdUtenteCreatore;
		this.numAttachment = inNumAttachment;
		this.bTrasfPdfErrore = inTrasfPdfErrore;
		this.bTrasfPdfWarning = inTrasfPdfWarning;
		this.idCategoriaDocumento = inIdCategoriaDocumento;
		this.nomeFile = inNomefile;
		this.dataProtocollo = inDataProtocollo;
		this.hasDestinatariInterni = inHasDestinatariInterni;
		this.hasDestinatariEsterni = inHasDestinatariEsterni;
		this.flagDisableFascicolo = false;
		this.flagFirmaAutografaRM = inFlagFirmaAutografaRM;
		this.storico = inStorico;
		this.idMomentoProtocollazione = inIdMomentoProtocollazione;
		this.dataCreazione = inDataCreazione;
		this.dataScadenza = inDataScadenza;
		this.urgenza = inUrgenza;
		this.flagRiservato = inFlagRiservato;
		this.descrizioneTipoDocumentoNps = inDescrizioneTipoDocumentoNps;
		this.oggettoProtocolloNps = inOggettoProtocolloNps;
		this.flagFirmaPDF = inFlagFirmaPDF;
		this.idTipologiaDocumento = inIdTipologiaDocumento;
		this.mimeType = inMimeType;
		this.idTipoProcedimento = inIdTipoProcedimento;
		this.codiceFlusso = inCodiceFlusso;
		this.sottoCategoriaUscita = inSottoCategoriaUscita;
		this.metadatiEstesi = inMetadatiEstesi;
		this.registrazioneAusiliaria = inRegistrazioneAusiliaria;
		updateFlagCambioIter();
	}

	/**
	 * Getter.
	 * 
	 * @return lista approvazioni.
	 */
	public List<ApprovazioneDTO> getStepsApprovazioni() {
		return stepsApprovazioni;
	}

	/**
	 * Gets the flag firma PDF.
	 *
	 * @return the flag firma PDF
	 */
	public Boolean getFlagFirmaPDF() {
		return flagFirmaPDF;
	}

	/**
	 * Getter.
	 * 
	 * @return steps
	 */
	public final List<StepDTO> getSteps() {
		return steps;
	}

	/**
	 * Get step di creazione documento.
	 * 
	 * @return step di creazione documento
	 */
	public final StepDTO getStepCreazione() {
		return stepCreazione;
	}

	/**
	 * Getter flag riservato.
	 * 
	 * @return flag riservato
	 */
	public final Boolean getFlagRiservato() {
		return flagRiservato;
	}

	/**
	 * Setter.
	 * 
	 * @param inSteps steps
	 */
	public final void setSteps(final List<StepDTO> inSteps) {
		this.steps = inSteps;
		for (final StepDTO step : steps) {
			if (step.getHeader().getDescrizione().toLowerCase().contains("creazione documento")) {
				stepCreazione = step;
				break;
			}
		}
	}

	/**
	 * Getter.
	 * 
	 * @return flag cambio iter
	 */
	public Boolean getFlagCambioIter() {
		return flagCambioIter;
	}

	/**
	 * Getter.
	 * 
	 * @return contributi
	 */
	public final Collection<ContributoDTO> getContributi() {
		return contributi;
	}

	/**
	 * Setter.
	 * 
	 * @param inContributi contributi
	 */
	public final void setContributi(final Collection<ContributoDTO> inContributi) {
		this.contributi = inContributi;
	}

	/**
	 * Aggiorna flag cambio iter.
	 */
	private void updateFlagCambioIter() {
		flagCambioIter = TipoOperazioneLibroFirmaEnum.SIGLA.equals(operazione) || TipoOperazioneLibroFirmaEnum.FIRMA.equals(operazione)
				|| TipoOperazioneLibroFirmaEnum.COMPETENZA.equals(operazione);
	}

	/**
	 * Getter.
	 * 
	 * @return data creazione
	 */
	public final Date getDataCreazione() {
		return dataCreazione;
	}

	/**
	 * Getter.
	 * 
	 * @return data scadenza
	 */
	public final Date getDataScadenza() {
		return dataScadenza;
	}

	/**
	 * Getter.
	 * 
	 * @return urgenza
	 */
	public final Boolean getUrgenza() {
		return urgenza;
	}

	/**
	 * Setter.
	 * 
	 * @param inUrgenza flag urgenza
	 */
	public void setUrgenza(final Boolean inUrgenza) {
		this.urgenza = inUrgenza;
	}

	/**
	 * Getter.
	 * 
	 * @return anno protocollo
	 */
	public final Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * Getter.
	 * 
	 * @return anno protocollo fascicolo
	 */
	public final Integer getAnnoProtocolloFascicolo() {
		return annoProtocolloFascicolo;
	}

	/**
	 * Getter.
	 * 
	 * @return approvazioni
	 */
	public final String getApprovazioni() {
		return StringUtils.fromStringListToString(approvazioni);
	}

	/**
	 * Getter.
	 * 
	 * @return flag allegati presenti
	 */
	public final Boolean getbAttachmentPresent() {
		return bAttachmentPresent;
	}

	/**
	 * Getter.
	 * 
	 * @return flag trasformazione in errore
	 */
	public final Boolean getbTrasfPdfErrore() {
		return bTrasfPdfErrore;
	}

	/**
	 * Getter.
	 * 
	 * @return flag trasformazione in warning
	 */
	public final Boolean getbTrasfPdfWarning() {
		return bTrasfPdfWarning;
	}

	/**
	 * Getter.
	 * 
	 * @return data creazione fascicolo
	 */
	public final Date getDataCreazioneFascicolo() {
		return dataCreazioneFascicolo;
	}

	/**
	 * Getter.
	 * 
	 * @return data protocollazione
	 */
	public final Date getDataProtocollo() {
		return dataProtocollo;
	}

	/**
	 * Getter.
	 * 
	 * @return destinatari
	 */
	public final String getDestinatari() {
		return StringUtils.fromStringListToString(destinatari);
	}

	/**
	 * Getter.
	 * 
	 * @return destinatari documento
	 */
	public final List<DestinatarioDTO> getDestinatariDocumento() {
		return destinatariDocumento;
	}

	/**
	 * Metodo per testare se il documento ha destinatari elettronici.
	 * 
	 * @return risultato test
	 */
	public final boolean isDestinatariElettronici() {
		if (!hasDestinatariEsterni) {
			return false;
		}
		if (destinatariDocumento != null) {
			for (final DestinatarioDTO destinatario : destinatariDocumento) {
				if (Constants.TipoDestinatario.ESTERNO.equalsIgnoreCase(destinatario.getTipo())
						&& !Constants.TipoSpedizione.MEZZO_ELETTRONICO.equals(destinatario.getTipoSpedizione())) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Getter.
	 * 
	 * @return documenti fascicolo
	 */
	public final Collection<DocumentoFascicoloDTO> getDocumentiFascicolo() {
		final Collection<DocumentoFascicoloDTO> output = new ArrayList<>();
		if (documentiFascicolo != null) {
			for (final DocumentoFascicoloDTO d : documentiFascicolo) {
				if (d.getbFlagInfoFascicolo() != null && !d.getbFlagInfoFascicolo() && d.getNumeroDocumento() != null) {
					output.add(d);
				}
			}
		}
		return output;
	}

	/**
	 * Getter.
	 * 
	 * @return document title
	 */
	public final String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Getter.
	 * 
	 * @return flag firma pades
	 */
	public final Boolean getFirmaPades() {
		return firmaPades;
	}

	/**
	 * Getter.
	 * 
	 * @return flag disable fascicolo
	 */
	public final Boolean getFlagDisableFascicolo() {
		return flagDisableFascicolo;
	}

	/**
	 * Getter.
	 * 
	 * @return flag enable firma
	 */
	public final Boolean getFlagEnableFirma() {
		return flagEnableFirma;
	}

	/**
	 * Getter.
	 * 
	 * @return flag abilita rifiuto
	 */
	public final Boolean getFlagEnableRifiuta() {
		return flagEnableRifiuta;
	}
	
	/**
	 * Getter.
	 * 
	 * @return flag abilita sigla
	 */
	public final Boolean getFlagEnableSigla() {
		return flagEnableSigla;
	}
	
	/**
	 * Getter.
	 * 
	 * @return flag enable firma autografa
	 */
	public final Boolean getFlagEnableFirmaAutografa() {
		return flagEnableFirmaAutografa;
	}

	/**
	 * Getter.
	 * 
	 * @return flag firma autografa Red
	 */
	public final Boolean getFlagFirmaAutografaRM() {
		return flagFirmaAutografaRM;
	}
	
	/**
	 * Getter.
	 * 
	 * @return flag abilita visto
	 */
	public final Boolean getFlagEnableVista() {
		return flagEnableVista;
	}

	/**
	 * Setter flag firma autografa RM.
	 * 
	 * @param inFlagFirmaAutografaRM flag firma autografa RM
	 */
	public final void setFlagFirmaAutografaRM(final Boolean inFlagFirmaAutografaRM) {
		this.flagFirmaAutografaRM = inFlagFirmaAutografaRM;
	}

	/**
	 * Getter.
	 * 
	 * @return flag firma digitale
	 */
	public final Boolean getFlagFirmaDig() {
		return flagFirmaDig;
	}

	/**
	 * Getter.
	 * 
	 * @return identificativo formato allegato
	 */
	public final Integer getFormatoAllegatoId() {
		return formatoAllegatoId;
	}

	/**
	 * Getter.
	 * 
	 * @return guid
	 */
	public final String getGuid() {
		return guid;
	}

	/**
	 * Getter.
	 * 
	 * @return identificativo categoria documento
	 */
	public final Integer getIdCategoriaDocumento() {
		return idCategoriaDocumento;
	}

	/**
	 * Getter.
	 * 
	 * @return identificativo momento protocollazione
	 */
	public final Integer getIdMomentoProtocollazione() {
		return idMomentoProtocollazione;
	}

	/**
	 * Getter.
	 * 
	 * @return identificativo protocollo
	 */
	public final String getIdProtocollo() {
		return idProtocollo;
	}

	/**
	 * Getter.
	 * 
	 * @return identificativo ufficio creatore
	 */
	public final Integer getIdUfficioCreatore() {
		return idUfficioCreatore;
	}

	/**
	 * Setter idUfficioCreatore.
	 * 
	 * @param idUfficioCreatore the idUfficioCreatore to set
	 */
	public void setIdUfficioCreatore(final Integer idUfficioCreatore) {
		this.idUfficioCreatore = idUfficioCreatore;
	}

	/**
	 * Setter idUtenteCreatore.
	 * 
	 * @param idUtenteCreatore the idUtenteCreatore to set
	 */
	public void setIdUtenteCreatore(final Integer idUtenteCreatore) {
		this.idUtenteCreatore = idUtenteCreatore;
	}

	/**
	 * Getter.
	 * 
	 * @return identificativo utente creatore
	 */
	public final Integer getIdUtenteCreatore() {
		return idUtenteCreatore;
	}

	/**
	 * Getter.
	 * 
	 * @return identificativo utente destinatario
	 */
	public final Integer getIdUtenteDestinatario() {
		return idUtenteDestinatario;
	}

	/**
	 * Getter.
	 * 
	 * @return indice fascicolo
	 */
	public final String getIndiceFascicolo() {
		return indiceFascicolo;
	}

	/**
	 * Getter.
	 * 
	 * @return iter approvativo
	 */
	public final String getIterApprovativo() {
		return iterApprovativo;
	}

	/**
	 * Getter.
	 * 
	 * @return motivo assegnazione
	 */
	public final String getMotivazioneAssegnazione() {
		return motivazioneAssegnazione;
	}

	/**
	 * Getter.
	 * 
	 * @return nomefile
	 */
	public final String getNomeFile() {
		return nomeFile;
	}

	/**
	 * Imposta il nome del file.
	 * 
	 * @param nomeFile
	 */
	public void setNomeFile(final String nomeFile) {
		this.nomeFile = nomeFile;
	}

	/**
	 * Getter.
	 * 
	 * @return numero allegati
	 */
	public final Integer getNumAttachment() {
		return numAttachment;
	}

	/**
	 * Setter.
	 * 
	 * tags:
	 * 
	 * @param numAttachment
	 */
	public void setNumAttachment(final Integer numAttachment) {
		this.numAttachment = numAttachment;
	}

	/**
	 * Getter.
	 * 
	 * @return numero documento
	 */
	public final Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * Getter.
	 * 
	 * @return numero documento fascicolo
	 */
	public final String getNumeroDocumentoFascicolo() {
		return numeroDocumentoFascicolo;
	}

	/**
	 * Getter.
	 * 
	 * @return numero protocollo
	 */
	public final Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Getter.
	 * 
	 * @return numero protocollo fascicolo
	 */
	public final Integer getNumeroProtocolloFascicolo() {
		return numeroProtocolloFascicolo;
	}

	/**
	 * Getter.
	 * 
	 * @return oggetto
	 */
	public final String getOggetto() {
		return oggetto;
	}

	/**
	 * Setter oggetto.
	 * 
	 * @param oggetto the oggetto to set
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * Getter.
	 * 
	 * @return oggetto fascicolo
	 */
	public final String getOggettoFascicolo() {
		return oggettoFascicolo;
	}

	/**
	 * Getter.
	 * 
	 * @return operazione
	 */
	public final TipoOperazioneLibroFirmaEnum getOperazione() {
		return operazione;
	}

	/**
	 * Getter.
	 * 
	 * @return oggetto propotocollo NPS
	 */
	public final ProtocolloNpsDTO getProtocolloNps() {
		ProtocolloNpsDTO protocollo = null;
		if (idProtocollo != null && idProtocollo.trim().length() > 0) {
			protocollo = new ProtocolloNpsDTO();
			protocollo.setIdDocumento(this.idProtocollo);
			protocollo.setNumeroProtocollo(this.numeroProtocollo);
			protocollo.setDataProtocollo(this.dataProtocollo);
			protocollo.setAnnoProtocollo(this.annoProtocollo + "");
		}
		return protocollo;
	}

	/**
	 * Getter.
	 * 
	 * @return coda
	 */
	public final DocumentQueueEnum getQueue() {
		return queue;
	}

	/**
	 * Getter.
	 * 
	 * @return stato fascicolo
	 */
	public final String getStatoFascicolo() {
		return statoFascicolo;
	}

	/**
	 * Getter.
	 * 
	 * @return storico
	 */
	public final Collection<StoricoDTO> getStorico() {
		return storico;
	}

	/**
	 * Getter.
	 * 
	 * @return tipologia documento
	 */
	public final String getTipologiaDocumento() {
		return tipologiaDocumento;
	}

	/**
	 * Setter tipologiaDocumento.
	 * 
	 * @param tipologiaDocumento the tipologiaDocumento to set
	 */
	public void setTipologiaDocumento(final String tipologiaDocumento) {
		this.tipologiaDocumento = tipologiaDocumento;
	}

	/**
	 * Getter.
	 * 
	 * @return ufficio mittente
	 */
	public final String getUfficioMittente() {
		return ufficioMittente;
	}

	/**
	 * Getter.
	 * 
	 * @return numero wob
	 */
	public final String getWobNumber() {
		return wobNumber;
	}

	/**
	 * Getter.
	 * 
	 * @return flag destiniatari esterni
	 */
	public final boolean isHasDestinatariEsterni() {
		return hasDestinatariEsterni;
	}

	/**
	 * Getter.
	 * 
	 * @return flag destinatari interni
	 */
	public final boolean isHasDestinatariInterni() {
		return hasDestinatariInterni;
	}

	/**
	 * Getter.
	 * 
	 * @return content
	 */
	public final byte[] getContent() {
		return content;
	}

	/**
	 * Getter.
	 * 
	 * @return mimeType
	 */
	public final String getMimeType() {
		return mimeType;
	}

	/**
	 * Setter.
	 * 
	 * @param inAnnoProtocolloFascicolo anno protocollo fascicolo
	 */
	public final void setAnnoProtocolloFascicolo(final Integer inAnnoProtocolloFascicolo) {
		this.annoProtocolloFascicolo = inAnnoProtocolloFascicolo;
	}

	/**
	 * Setter.
	 * 
	 * @param inApprovazioni approvazioni
	 */
	public final void setApprovazioni(final Collection<String> inApprovazioni) {
		this.approvazioni = inApprovazioni;
	}

	/**
	 * Setter.
	 * 
	 * @param inBAttachmentPresent flag allegati presenti
	 */
	public final void setbAttachmentPresent(final Boolean inBAttachmentPresent) {
		this.bAttachmentPresent = inBAttachmentPresent;
	}

	/**
	 * Setter.
	 * 
	 * @param inBTrasfPdfErrore flag trasformazione pdf in erorre
	 */
	public final void setbTrasfPdfErrore(final Boolean inBTrasfPdfErrore) {
		this.bTrasfPdfErrore = inBTrasfPdfErrore;
	}

	/**
	 * Setter.
	 * 
	 * @param inBTrasfPdfWarning flag trasformazione pdf in warning
	 */
	public final void setbTrasfPdfWarning(final Boolean inBTrasfPdfWarning) {
		this.bTrasfPdfWarning = inBTrasfPdfWarning;
	}

	/**
	 * Setter.
	 * 
	 * @param inDataCreazioneFascicolo data creazione fascicolo
	 */
	public final void setDataCreazioneFascicolo(final Date inDataCreazioneFascicolo) {
		this.dataCreazioneFascicolo = inDataCreazioneFascicolo;
	}

	/**
	 * Setter.
	 * 
	 * @param inDocumentiFascicolo documenti fascicolo
	 */
	public final void setDocumentiFascicolo(final Collection<DocumentoFascicoloDTO> inDocumentiFascicolo) {
		documentiFascicolo = inDocumentiFascicolo;
	}

	/**
	 * Setter.
	 * 
	 * @param canPades flag possibile firmare in pades
	 */
	public final void setFirmaPades(final Boolean canPades) {
		firmaPades = canPades;
	}

	/**
	 * Setter.
	 * 
	 * @param inFlagDisableFascicolo flag disabilita sezione fascicolo
	 */
	public final void setFlagDisableFascicolo(final Boolean inFlagDisableFascicolo) {
		this.flagDisableFascicolo = inFlagDisableFascicolo;
	}

	/**
	 * Setter.
	 * 
	 * @param inFlagEnableFirmaAutografa flag abilita firma autografa
	 */
	public final void setFlagEnableFirmaAutografa(final Boolean inFlagEnableFirmaAutografa) {
		this.flagEnableFirmaAutografa = inFlagEnableFirmaAutografa;
	}
	
	/**
	 * Setter.
	 * 
	 * @param inFlagEnableFirma flag abilita firma
	 */
	public final void setFlagEnableFirma(final Boolean inFlagEnableFirma) {
		this.flagEnableFirma = inFlagEnableFirma;
	}

	/**
	 * Setter.
	 * 
	 * @param inFlagEnableSigla flag abilita sigla
	 */
	public final void setFlagEnableSigla(final Boolean inFlagEnableSigla) {
		this.flagEnableSigla = inFlagEnableSigla;
	}
	
	/**
	 * Setter.
	 * 
	 * @param inFlagEnableRifiuta flag abilita rifiuto
	 */
	public final void setFlagEnableRifiuta(final Boolean inFlagEnableRifiuta) {
		this.flagEnableRifiuta = inFlagEnableRifiuta;
	}

	/**
	 * Setter.
	 * 
	 * @param inFlagFirmaDig flag abilita firma digitale
	 */
	public final void setFlagFirmaDig(final Boolean inFlagFirmaDig) {
		this.flagFirmaDig = inFlagFirmaDig;
	}
	
	/**
	 * Setter.
	 * 
	 * @param inFlagEnableVista flag abilita vista
	 */
	public final void setFlagEnableVista(final Boolean inFlagEnableVista) {
		this.flagEnableVista = inFlagEnableVista;
	}

	/**
	 * Setter.
	 * 
	 * @param inGuid guid
	 */
	public final void setGuid(final String inGuid) {
		this.guid = inGuid;
	}

	/**
	 * Setter.
	 * 
	 * @param inIdUtenteDestinatario identificativo utente destinatario
	 */
	public final void setIdUtenteDestinatario(final Integer inIdUtenteDestinatario) {
		this.idUtenteDestinatario = inIdUtenteDestinatario;
	}

	/**
	 * Setter.
	 * 
	 * @param inIndiceFascicolo indice fascicolo
	 */
	public final void setIndiceFascicolo(final String inIndiceFascicolo) {
		this.indiceFascicolo = inIndiceFascicolo;
	}

	/**
	 * Setter.
	 * 
	 * @param inIterApprovativo iter approvativo
	 */
	public final void setIterApprovativo(final String inIterApprovativo) {
		this.iterApprovativo = inIterApprovativo;
	}

	/**
	 * Setter.
	 * 
	 * @param inMotivazioneAssegnazione motivazione assegnazione
	 */
	public final void setMotivazioneAssegnazione(final String inMotivazioneAssegnazione) {
		this.motivazioneAssegnazione = inMotivazioneAssegnazione;
	}

	/**
	 * Setter.
	 * 
	 * @param inNumeroDocumentoFascicolo numero documento fascicolo
	 */
	public final void setNumeroDocumentoFascicolo(final String inNumeroDocumentoFascicolo) {
		this.numeroDocumentoFascicolo = inNumeroDocumentoFascicolo;
	}

	/**
	 * Setter.
	 * 
	 * @param inNumeroProtocolloFascicolo numero protocollo fascicolo
	 */
	public final void setNumeroProtocolloFascicolo(final Integer inNumeroProtocolloFascicolo) {
		this.numeroProtocolloFascicolo = inNumeroProtocolloFascicolo;
	}

	/**
	 * Setter.
	 * 
	 * @param inOggettoFascicolo oggetto fasicolo
	 */
	public final void setOggettoFascicolo(final String inOggettoFascicolo) {
		this.oggettoFascicolo = inOggettoFascicolo;
	}

	/**
	 * Setter.
	 * 
	 * @param inOperazione operazione
	 */
	public final void setOperazione(final TipoOperazioneLibroFirmaEnum inOperazione) {
		this.operazione = inOperazione;
		updateFlagCambioIter();
	}

	/**
	 * Setter.
	 * 
	 * @param inQueue coda
	 */
	public final void setQueue(final DocumentQueueEnum inQueue) {
		this.queue = inQueue;
	}

	/**
	 * Setter.
	 * 
	 * @param inStatoFascicolo stato fascicolo
	 */
	public final void setStatoFascicolo(final String inStatoFascicolo) {
		this.statoFascicolo = inStatoFascicolo;
	}

	/**
	 * Setter.
	 * 
	 * @param inWobNumber wob number
	 */
	public final void setWobNumber(final String inWobNumber) {
		wobNumber = inWobNumber;
	}

	/**
	 * Setter.
	 * 
	 * @param inContent content
	 */
	public final void setContent(final byte[] inContent) {
		this.content = inContent;
	}

	/**
	 * Getter Descrizione Tipo Documento Nps.
	 * 
	 * @return Descrizione Tipo Documento Nps
	 */
	public final String getDescrizioneTipoDocumentoNps() {
		return descrizioneTipoDocumentoNps;
	}

	/**
	 * Getter Oggetto Protocollo NPS.
	 * 
	 * @return Oggetto Protocollo NPS
	 */
	public final String getOggettoProtocolloNps() {
		return oggettoProtocolloNps;
	}

	/**
	 * Getter dei dati del protocollo.
	 * 
	 * @return DTO dei dati del protocollo
	 */
	public final ProtocolloDTO getProtocollo() {
		String annoProtocolloString = null;
		if (this.getAnnoProtocollo() != null) {
			annoProtocolloString = this.getAnnoProtocollo().toString();
		}
		return new ProtocolloDTO(this.getIdProtocollo(), this.getNumeroProtocollo(), annoProtocolloString, this.getDataProtocollo(), this.getDescrizioneTipoDocumentoNps(),
				this.getOggettoProtocolloNps());
	}

	/**
	 * 
	 * To string.
	 * 
	 * @return string
	 */
	@Override
	public final String toString() {
		return "DetailDocumentoDTO [guid=" + guid + ", numeroDocumento=" + numeroDocumento + ", oggetto=" + oggetto + ", wobNumber=" + wobNumber + ", dataScadenza="
				+ dataScadenza + "]";
	}

	/**
	 * Setter.
	 * 
	 * @param inStepsApprovazioni lista step approvazioni
	 */
	public void setStepsApprovazioni(final List<ApprovazioneDTO> inStepsApprovazioni) {
		stepsApprovazioni = inStepsApprovazioni;
	}

	/**
	 * Gets the flag enable procedi corriere.
	 *
	 * @return the flagEnableProcediCorriere
	 */
	public Boolean getFlagEnableProcediCorriere() {
		return flagEnableProcediCorriere;
	}

	/**
	 * Setter.
	 * 
	 * @param inFlagEnableProcediCorriere flag abilita procedi corriere
	 */
	public void setFlagEnableProcediCorriere(final Boolean inFlagEnableProcediCorriere) {
		this.flagEnableProcediCorriere = inFlagEnableProcediCorriere;
	}

	/**
	 * Gets the ID tipologia documento.
	 *
	 * @return the idTipologiaDocumento
	 */
	public Long getIdTipologiaDocumento() {
		return idTipologiaDocumento;
	}

	/**
	 * @return the idTipoProcedimento
	 */
	public Long getIdTipoProcedimento() {
		return idTipoProcedimento;
	}

	/**
	 * @return the descRegistroRepertorio
	 */
	public String getDescRegistroRepertorio() {
		return descRegistroRepertorio;
	}

	/**
	 * @param descRegistroRepertorio the descRegistroRepertorio to set
	 */
	public void setDescRegistroRepertorio(final String descRegistroRepertorio) {
		this.descRegistroRepertorio = descRegistroRepertorio;
	}

	/**
	 * @return the isRegistroRepertorio
	 */
	public Boolean getIsRegistroRepertorio() {
		return isRegistroRepertorio;
	}

	/**
	 * @param isRegistroRepertorio the isRegistroRepertorio to set
	 */
	public void setIsRegistroRepertorio(final Boolean isRegistroRepertorio) {
		this.isRegistroRepertorio = isRegistroRepertorio;
	}

	/**
	 * @return the codiceFlusso
	 */
	public String getCodiceFlusso() {
		return codiceFlusso;
	}

	/**
	 * @return the descrizioneIndiceFascicolo
	 */
	public String getDescrizioneIndiceFascicolo() {
		return descrizioneIndiceFascicolo;
	}

	/**
	 * @param descrizioneIndiceFascicolo the descrizioneIndiceFascicolo to set
	 */
	public void setDescrizioneIndiceFascicolo(final String descrizioneIndiceFascicolo) {
		this.descrizioneIndiceFascicolo = descrizioneIndiceFascicolo;
	}

	/**
	 * Restitusce la sotto categoria uscita associata al documento.
	 * 
	 * @return sottoCategoriaUscita
	 */
	public SottoCategoriaDocumentoUscitaEnum getSottoCategoriaUscita() {
		return sottoCategoriaUscita;
	}

	/**
	 * Imposta la sotto categoria uscita relativa al documento.
	 * 
	 * @param sottoCategoriaUscita
	 */
	public void setSottoCategoriaUscita(final SottoCategoriaDocumentoUscitaEnum sottoCategoriaUscita) {
		this.sottoCategoriaUscita = sottoCategoriaUscita;
	}

	/**
	 * Restituisce i metadati estesi del documento.
	 * 
	 * @return metadatiEstesi
	 */
	public Collection<MetadatoDTO> getMetadatiEstesi() {
		return metadatiEstesi;
	}

	/**
	 * Imposta i metadati estesi del documento.
	 * 
	 * @param metadatiEstesi
	 */
	public void setMetadatiEstesi(final Collection<MetadatoDTO> metadatiEstesi) {
		this.metadatiEstesi = metadatiEstesi;
	}

	/**
	 * Restituisce la registrazione ausiliaria associata al documento.
	 * 
	 * @return registrazioneAusiliaria
	 */
	public RegistrazioneAusiliariaDTO getRegistrazioneAusiliaria() {
		return registrazioneAusiliaria;
	}

	/**
	 * Imposta il DTO che definisce la registrazione ausiliaria associata al
	 * documento.
	 * 
	 * @param registrazioneAusiliaria
	 */
	public void setRegistrazioneAusiliaria(final RegistrazioneAusiliariaDTO registrazioneAusiliaria) {
		this.registrazioneAusiliaria = registrazioneAusiliaria;
	}

	/**
	 * Restituisce la label per la stampigliatura RR.
	 * 
	 * @return labelStampigliaturaRR
	 */
	public String getLabelStampigliaturaRR() {
		return labelStampigliaturaRR;
	}

	/**
	 * Imposta la label per la stampigliatura RR.
	 * 
	 * @param labelStampigliaturaRR
	 */
	public void setLabelStampigliaturaRR(final String labelStampigliaturaRR) {
		this.labelStampigliaturaRR = labelStampigliaturaRR;
	}
}
