package it.ibm.red.business.dto;

import java.util.List;

import it.ibm.red.business.enums.TipoStrutturaNodoEnum;
import it.ibm.red.business.enums.TipologiaNodoEnum;

/**
 * The Class NodoOrganigrammaDTO.
 *
 * @author SLac
 */
public class NodoOrganigrammaDTO extends AbstractDTO {

	/** 
	 * The Constant serialVersionUID. 
	 */
	private static final long serialVersionUID = -3939928495608571364L;
	
	/**
	 * Identificativo nodo
	 */
	private Long idNodo;
	
	/**
	 * Identificativo aoo
	 */
	private Long idAOO;
	
	/**
	 * Identificativo nodo padre
	 */
	private Long idNodoPadre;
	
	/**
	 * Codice nodo
	 */
	private String codiceNodo;
	
	/**
	 * Descrizione nodo organigramma
	 */
	private String descrizioneNodo;
	
	/**
	 * Enum che identifica la tipologia del nodo
	 */
	private TipologiaNodoEnum tipoNodo;
	
	/**
	 * Enum che identifica la tipologia della struttura
	 */
	private TipoStrutturaNodoEnum tipoStruttura;
	
	/**
	 * Flag segreteria
	 */
	private boolean flagSegreteria;
	
	/**
	 * Ordinamento
	 */
	private int ordinamento;
	
	/**
	 * Figli
	 */
	private int figli;
	
	/**
	 * Identificativo nodo organigramma
	 */
	private Long idNodoOrg;
	
	/**
	 * Identificativo nodo padre organigramma
	 */
	private Long idNodoPadreOrg;
	
	/**
	 * Identificativo aoo organigramma
	 */
	private Long idAooOrg;
	
	/**
	 * Tipologia visualizzazione 0
	 */
	private Long visualizzazione0;
	
	/**
	 * Tipologia visualizzazione 1
	 */
	private Long visualizzazione1;
	
	/**
	 * Tipologia visualizzazione 2
	 */
	private Long visualizzazione2;

	/**
	 * Tipologia visualizzazione 3
	 */
	private Long visualizzazione3;
	
	/**
	 * Lista di permessi dell'utente
	 */
	private List<Long> permessi;
	
	/**
	 * Variabile conteggio oganigramma
	 */
	private Integer countOrg;
	
	/**
	 * Puo' essere true solo se e' un nodo ufficio, 
	 * serve per tenere traccia dei nodi che devono visualizzare gli utenti
	 * se il valore e' TRUE significa che e' il nodo dell'utente 
	 * oppure che uno dei suoi padri aveva questo valore a TRUE
	 */
	private boolean utentiVisible;
	
	/**
	 * Mi serve per visualizzare l'albero RGS (VISUALIZZAZIONE1) 
	 */
	private String codiceAOO;
	
	/**NODO UTENTE START**********************/
	
	/**
	 * Identificativo utente organigramma
	 */
	private Long idUtente;
	
	/**
	 * Nome utente orgranigramma
	 */
	private String nomeUtente;
	
	/**
	 * Cognome utente organigramma
	 */
	private String cognomeUtente;
	
	/**
	 * Username utente
	 */
	private String usernameUtente;
	
	/**
	 * Email utente
	 */
	private String emailUtente;
	
	/**
	 * Codice fiscale utente
	 */
	private String codfisUtente;
	
	/**
	 * registroRiservatoUtente
	 */
	private boolean registroRiservatoUtente;
	
	/**
	 * NODO UTENTE END*********************.
	 */

	
	/**
	 * 
	 */
	public NodoOrganigrammaDTO() { }

	/**
	 * Costruttore per il nodo ufficio.
	 *
	 * @param idNodo
	 *            the id nodo
	 * @param idAOO
	 *            the id AOO
	 * @param idNodoPadre
	 *            the id nodo padre
	 * @param codiceNodo
	 *            the codice nodo
	 * @param descrizione
	 *            the descrizione
	 * @param tipoNodo
	 *            the tipo nodo
	 * @param tipoStruttura
	 *            the tipo struttura
	 * @param flagSegreteria
	 *            the flag segreteria
	 * @param ordinamento
	 *            the ordinamento
	 * @param figli
	 *            the figli
	 * @param idNodoOrg
	 *            the id nodo org
	 * @param idNodoPadreOrg
	 *            the id nodo padre org
	 * @param idAooOrg
	 *            the id aoo org
	 * @param visualizzazione0
	 *            the visualizzazione 0
	 * @param visualizzazione1
	 *            the visualizzazione 1
	 * @param visualizzazione2
	 *            the visualizzazione 2
	 * @param visualizzazione3
	 *            the visualizzazione 3
	 */
	public NodoOrganigrammaDTO(final Long idNodo, final Long idAOO, final Long idNodoPadre, final String codiceNodo, final String descrizione,
			final TipologiaNodoEnum tipoNodo, final TipoStrutturaNodoEnum tipoStruttura, final boolean flagSegreteria,
			final int ordinamento, final int figli,	final Long idNodoOrg, final Long idNodoPadreOrg, final Long idAooOrg,
			final Long visualizzazione0, final Long visualizzazione1, final Long visualizzazione2, final Long visualizzazione3) { 
		this.idNodo = idNodo; 
		this.idAOO = idAOO; 
		this.idNodoPadre = idNodoPadre; 
		this.codiceNodo = codiceNodo;
		this.descrizioneNodo = descrizione;
		this.tipoNodo = tipoNodo; 
		this.tipoStruttura = tipoStruttura; 
		this.flagSegreteria = flagSegreteria;
		this.ordinamento = ordinamento; 
		this.figli = figli;	
		this.idNodoOrg = idNodoOrg;	
		this.idNodoPadreOrg = idNodoPadreOrg; 
		this.idAooOrg = idAooOrg;
		this.visualizzazione0 = visualizzazione0; 
		this.visualizzazione1 = visualizzazione1; 
		this.visualizzazione2 = visualizzazione2; 
		this.visualizzazione3 = visualizzazione3;
	}
	
	/**
	 * Costruttore per il nodo utente.
	 *
	 * @param idNodo the id nodo
	 * @param idAOO the id AOO
	 * @param codiceNodo the codice nodo
	 * @param descrizione the descrizione
	 * @param idUtente the id utente
	 * @param nome the nome
	 * @param cognome the cognome
	 * @param username the username
	 * @param email the email
	 * @param codfis the codfis
	 * @param tipologiaNodo the tipologia nodo
	 */
	public NodoOrganigrammaDTO(final Long idNodo, final Long idAOO, final String codiceNodo, final String descrizione, final Long idUtente, final String nome, final String cognome, final String username, final String email, final String codfis, final TipologiaNodoEnum tipologiaNodo) { 
		this.idNodo = idNodo; 
		this.idAOO = idAOO; 
		this.codiceNodo = codiceNodo;
		this.descrizioneNodo = descrizione;
		this.idUtente = idUtente;
		this.nomeUtente = nome;
		this.cognomeUtente = cognome;
		this.usernameUtente = username;
		this.codfisUtente = codfis;
		this.emailUtente = email;
		this.tipoNodo = tipologiaNodo;
	}
	
	/**
	 * Costruttore nodo organigramma DTO.
	 *
	 * @param descrizioneNodo the descrizione nodo
	 * @param nodoId the nodo id
	 * @param codiceNodo the codice nodo
	 */
	public NodoOrganigrammaDTO(final String descrizioneNodo, final Integer nodoId, final String codiceNodo) {
		this.descrizioneNodo = descrizioneNodo;
		final Integer tmp = nodoId;
		this.idNodo = tmp.longValue();
		this.codiceNodo = codiceNodo;
	}
	
	/**
	 *  GET & SET ***********************************************************************************.
	 *
	 * @return the id nodo
	 */

	public Long getIdNodo() {
		return idNodo;
	}

	/** 
	 *
	 * @return the id AOO
	 */
	public Long getIdAOO() {
		return idAOO;
	}

	/** 
	 *
	 * @return the id nodo padre
	 */
	public Long getIdNodoPadre() {
		return idNodoPadre;
	}

	/** 
	 *
	 * @return the codice nodo
	 */
	public String getCodiceNodo() {
		return codiceNodo;
	}
	
	/** 
	 *
	 * @return the descrizione nodo
	 */
	public String getDescrizioneNodo() {
		return descrizioneNodo;
	}
	
	/** 
	 *
	 * @param descrizioneNodo the new descrizione nodo
	 */
	public final void setDescrizioneNodo(final String descrizioneNodo) {
		this.descrizioneNodo = descrizioneNodo;
	}

	/** 
	 *
	 * @return the tipo nodo
	 */
	public TipologiaNodoEnum getTipoNodo() {
		return tipoNodo;
	}

	/** 
	 *
	 * @return the tipo struttura
	 */
	public TipoStrutturaNodoEnum getTipoStruttura() {
		return tipoStruttura;
	}

	/** 
	 *
	 * @return true, if is flag segreteria
	 */
	public boolean isFlagSegreteria() {
		return flagSegreteria;
	}

	/** 
	 *
	 * @return the ordinamento
	 */
	public int getOrdinamento() {
		return ordinamento;
	}

	/** 
	 *
	 * @return the figli
	 */
	public int getFigli() {
		return figli;
	}

	/** 
	 *
	 * @return the id nodo org
	 */
	public Long getIdNodoOrg() {
		return idNodoOrg;
	}

	/** 
	 *
	 * @return the id nodo padre org
	 */
	public Long getIdNodoPadreOrg() {
		return idNodoPadreOrg;
	}

	/** 
	 *
	 * @return the id aoo org
	 */
	public Long getIdAooOrg() {
		return idAooOrg;
	}

	/** 
	 *
	 * @return the visualizzazione 0
	 */
	public Long getVisualizzazione0() {
		return visualizzazione0;
	}

	/** 
	 *
	 * @return the visualizzazione 1
	 */
	public Long getVisualizzazione1() {
		return visualizzazione1;
	}

	/** 
	 *
	 * @return the visualizzazione 2
	 */
	public Long getVisualizzazione2() {
		return visualizzazione2;
	}

	/** 
	 *
	 * @return the visualizzazione 3
	 */
	public Long getVisualizzazione3() {
		return visualizzazione3;
	}

	/** 
	 *
	 * @param figli the new figli
	 */
	public void setFigli(final int figli) {
		this.figli = figli;
	}

	/** 
	 *
	 * @return the id utente
	 */
	public Long getIdUtente() {
		return idUtente;
	}

	/** 
	 *
	 * @param idUtente the new id utente
	 */
	public void setIdUtente(final Long idUtente) {
		this.idUtente = idUtente;
	}

	/** 
	 *
	 * @return the nomeUtente
	 */
	public String getNomeUtente() {
		return nomeUtente;
	}

	/** 
	 *
	 * @param nomeUtente the nomeUtente to set
	 */
	public void setNomeUtente(final String nomeUtente) {
		this.nomeUtente = nomeUtente;
	}

	/** 
	 *
	 * @return the cognomeUtente
	 */
	public String getCognomeUtente() {
		return cognomeUtente;
	}

	/** 
	 *
	 * @param cognomeUtente the cognomeUtente to set
	 */
	public void setCognomeUtente(final String cognomeUtente) {
		this.cognomeUtente = cognomeUtente;
	}

	/** 
	 *
	 * @return the username utente
	 */
	public String getUsernameUtente() {
		return usernameUtente;
	}

	/** 
	 *
	 * @return the email utente
	 */
	public String getEmailUtente() {
		return emailUtente;
	}

	/** 
	 *
	 * @return the codfis utente
	 */
	public String getCodfisUtente() {
		return codfisUtente;
	}

	/**
	 * Checks if is registro riservato utente.
	 *
	 * @return true, if is registro riservato utente
	 */
	public boolean isRegistroRiservatoUtente() {
		return registroRiservatoUtente;
	}

	/**
	 * Checks if is utenti visible.
	 *
	 * @return true, if is utenti visible
	 */
	public boolean isUtentiVisible() {
		return utentiVisible;
	}

	/** 
	 *
	 * @param utentiVisible the new utenti visible
	 */
	public void setUtentiVisible(final boolean utentiVisible) {
		this.utentiVisible = utentiVisible;
	}

	/** 
	 *
	 * @param idNodo the new id nodo
	 */
	public void setIdNodo(final Long idNodo) {
		this.idNodo = idNodo;
	}

	/** 
	 *
	 * @param idAOO the new id AOO
	 */
	public void setIdAOO(final Long idAOO) {
		this.idAOO = idAOO;
	}

	/** 
	 *
	 * @return the codice AOO
	 */
	public String getCodiceAOO() {
		return codiceAOO;
	}

	/** 
	 *
	 * @param codiceAOO the new codice AOO
	 */
	public void setCodiceAOO(final String codiceAOO) {
		this.codiceAOO = codiceAOO;
	}

	/** 
	 *
	 * @return the permessi
	 */
	public List<Long> getPermessi() {
		return permessi;
	}

	/** 
	 *
	 * @param permessi the new permessi
	 */
	public void setPermessi(final List<Long> permessi) {
		this.permessi = permessi;
	}

	/** 
	 *
	 * @return the countOrg
	 */
	public final Integer getCountOrg() {
		return countOrg;
	}

	/** 
	 *
	 * @param countOrg the countOrg to set
	 */
	public final void setCountOrg(final Integer countOrg) {
		this.countOrg = countOrg;
	}

	/**
	 * Hash code.
	 *
	 * @return the int
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idAOO == null) ? 0 : idAOO.hashCode());
		result = prime * result + ((idNodo == null) ? 0 : idNodo.hashCode());
		result = prime * result + ((idUtente == null) ? 0 : idUtente.hashCode());
		return result;
	}

	/**
	 * Equals.
	 *
	 * @param obj the obj
	 * @return true, if successful
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final NodoOrganigrammaDTO other = (NodoOrganigrammaDTO) obj;
		if (idAOO == null) {
			if (other.idAOO != null)
				return false;
		} else if (!idAOO.equals(other.idAOO))
			return false;
		if (idNodo == null) {
			if (other.idNodo != null)
				return false;
		} else if (!idNodo.equals(other.idNodo))
			return false;
		if (idUtente == null) {
			if (other.idUtente != null)
				return false;
		} else if (!idUtente.equals(other.idUtente))
			return false;
		return true;
	}

}

