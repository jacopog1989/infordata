package it.ibm.red.business.dao.impl;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.INotificaNpsDAO;
import it.ibm.red.business.dto.NotificaAzioneNpsDTO;
import it.ibm.red.business.dto.NotificaNpsDTO;
import it.ibm.red.business.dto.NotificaOperazioneNpsDTO;
import it.ibm.red.business.enums.StatoElabMessaggioPostaNpsEnum;
import it.ibm.red.business.enums.StatoElabNotificaNpsEnum;
import it.ibm.red.business.enums.TipoNotificaAzioneNPSEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.FileUtils;

/**
 * Interfaccia del DAO per la gestione delle notifiche delle operazioni eseguite
 * da NPS.
 * 
 * @author a.dilegge
 */
@Repository
public class NotificaNpsDAO extends AbstractDAO implements INotificaNpsDAO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -2276156038968479436L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotificaNpsDAO.class);
	
	/**
	 * Colonna - Data protocollo.
	 */
	private static final String DATAPROTOCOLLO = "DATAPROTOCOLLO";

	/**
	 * Label.
	 */
	private static final String ERROR_AGGIORNAMENTO_ITEM_MSG = "Errore durante l'aggiornamento dell'item nella coda di elaborazione delle notifiche operazioni di Nps ";

	/**
	 * Label.
	 */
	private static final String SUCCESS_AGGIORNAMENTO_NOTIFICA_MSG = "Aggiornata notifica operazione ";

	/**
	 * Label antecedente id richiesta, occorre appendere l'id della richiesta al
	 * messaggio.
	 */
	private static final String ID_RICHIESTA_PREFIX = " con idRichiesta: ";

	/**
	 * @see it.ibm.red.business.dao.INotificaNpsDAO#inserisciNotificaInCoda(it.ibm.red.business.dto.NotificaNpsDTO, java.sql.Connection).
	 */
	@Override
	public long inserisciNotificaInCoda(final NotificaNpsDTO notifica, final Connection con) {
		long idRichiesta = 0;
		PreparedStatement ps = null;

		try {
			idRichiesta = getNextId(con, "SEQ_CODA_NPS_NOTIFICHE_OP");

			ps = con.prepareStatement("INSERT INTO REDBATCH_CODA_NPS_NOTIFICHE_OP (IDCODA, MESSAGEIDNPS, IDMESSAGGIONPS, MESSAGEIDMAIL, CODICEAOONPS, "
					+ "IDAOO, IDPROTOCOLLO, NUMEROPROTOCOLLO, ANNOPROTOCOLLO, TIPOPROTOCOLLO, DATAPROTOCOLLO, TIPONOTIFICA, ESITONOTIFICA, EMAILDESTINATARIO, "
					+ "DATANOTIFICA, STATO, DATIAZIONE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

			int index = 1;
			ps.setLong(index++, idRichiesta);
			ps.setString(index++, notifica.getMessageIdNPS());
			ps.setString(index++, notifica.getIdMessaggioNPS());
			if (notifica instanceof NotificaOperazioneNpsDTO) {
				ps.setString(index++, ((NotificaOperazioneNpsDTO) notifica).getMessageIdMail());
			} else {
				ps.setNull(index++, java.sql.Types.VARCHAR);
			}
			ps.setString(index++, notifica.getCodiceAooNPS());
			ps.setLong(index++, notifica.getIdAoo());
			if (notifica.getIdProtocollo() != null) {
				ps.setString(index++, notifica.getIdProtocollo());
			} else {
				ps.setNull(index++, java.sql.Types.VARCHAR);
			}
			if (notifica.getNumeroProtocollo() != null) {
				ps.setInt(index++, notifica.getNumeroProtocollo());
			} else {
				ps.setNull(index++, java.sql.Types.INTEGER);
			}
			if (notifica.getAnnoProtocollo() != null) {
				ps.setInt(index++, notifica.getAnnoProtocollo());
			} else {
				ps.setNull(index++, java.sql.Types.INTEGER);
			}
			if (notifica.getTipoProtocollo() != null) {
				ps.setInt(index++, notifica.getTipoProtocollo());
			} else {
				ps.setNull(index++, java.sql.Types.INTEGER);
			}
			if (notifica.getDataProtocollo() != null) {
				ps.setTimestamp(index++, new Timestamp(notifica.getDataProtocollo().getTime()));
			} else {
				ps.setNull(index++, java.sql.Types.TIMESTAMP);
			}
			ps.setString(index++, notifica.getTipoNotifica());
			if (notifica instanceof NotificaOperazioneNpsDTO) {
				ps.setString(index++, ((NotificaOperazioneNpsDTO) notifica).getEsitoNotifica());
			} else {
				ps.setNull(index++, java.sql.Types.VARCHAR);
			}
			if (notifica instanceof NotificaOperazioneNpsDTO) {
				ps.setString(index++, ((NotificaOperazioneNpsDTO) notifica).getEmailDestinatario());
			} else {
				ps.setNull(index++, java.sql.Types.VARCHAR);
			}
			ps.setTimestamp(index++, new Timestamp(notifica.getDataNotifica().getTime()));
			ps.setInt(index++, StatoElabNotificaNpsEnum.DA_ELABORARE.getId());
			if (notifica instanceof NotificaAzioneNpsDTO) {
				final Blob blobDatiAzione = con.createBlob();
				blobDatiAzione.setBytes(1, FileUtils.getByteObject(((NotificaAzioneNpsDTO) notifica).getDatiAzione()));
				ps.setBlob(index++, blobDatiAzione);
			} else {
				ps.setNull(index++, java.sql.Types.BLOB);
			}

			ps.executeUpdate();

		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Errore durante l'inserimento di un item nella coda di elaborazione delle notifiche operazioni di Nps.", e);
		} finally {
			closeStatement(ps);
		}

		return idRichiesta;
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaNpsDAO#getAndLockNextNotifica(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public NotificaNpsDTO getAndLockNextNotifica(final Long idAoo, final Connection con) {
		NotificaNpsDTO richiestaElabNotifica = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement("SELECT * FROM REDBATCH_CODA_NPS_NOTIFICHE_OP WHERE IDAOO = ? AND STATO = ? ORDER BY DATARICEZIONENOTIFICA FOR UPDATE SKIP LOCKED");
			ps.setLong(1, idAoo);
			ps.setLong(2, StatoElabMessaggioPostaNpsEnum.DA_ELABORARE.getId());
			ps.setMaxRows(1);

			rs = ps.executeQuery();
			
			if (rs.next()) {
				richiestaElabNotifica = populateItem(rs);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del primo item della coda di elaborazione delle notifiche operazioni di Nps.", e);
			throw new RedException("Errore durante il recupero del primo item della coda di elaborazione delle notifiche operazioni di Nps.", e);
		} finally {
			closeStatement(ps, rs);
		}

		return richiestaElabNotifica;
	}
	
	private static NotificaNpsDTO populateItem(final ResultSet rs) throws SQLException {
		NotificaNpsDTO notifica = null;

		final Blob blob = rs.getBlob("DATIAZIONE");
		if (blob != null) {

			final Object datiAzione = FileUtils.getObjectFromByteArray(blob.getBytes(1, (int) blob.length()));
			Date dataProtocollo = null;
			if (rs.getTimestamp(DATAPROTOCOLLO) != null) {
				dataProtocollo = new Date(rs.getTimestamp(DATAPROTOCOLLO).getTime());
			}
			notifica = new NotificaAzioneNpsDTO(rs.getLong("IDCODA"), rs.getString("MESSAGEIDNPS"), rs.getString("CODICEAOONPS"), dataProtocollo,
					rs.getInt("NUMEROPROTOCOLLO"), rs.getInt("ANNOPROTOCOLLO"), rs.getString("IDPROTOCOLLO"), rs.getInt("TIPOPROTOCOLLO"),
					new Date(rs.getTimestamp("DATANOTIFICA").getTime()), rs.getString("TIPONOTIFICA"), rs.getLong("IDAOO"),
					new Date(rs.getTimestamp("DATARICEZIONENOTIFICA").getTime()), StatoElabNotificaNpsEnum.get(rs.getInt("STATO")), rs.getString("ERRORE"), datiAzione);

		} else {

			notifica = new NotificaOperazioneNpsDTO(rs.getLong("IDCODA"), rs.getString("MESSAGEIDNPS"), rs.getString("IDMESSAGGIONPS"), rs.getString("MESSAGEIDMAIL"),
					rs.getString("CODICEAOONPS"), new Date(rs.getTimestamp(DATAPROTOCOLLO).getTime()), rs.getInt("NUMEROPROTOCOLLO"), rs.getInt("ANNOPROTOCOLLO"),
					rs.getInt("TIPOPROTOCOLLO"), rs.getString("IDPROTOCOLLO"), new Date(rs.getTimestamp("DATANOTIFICA").getTime()), rs.getString("TIPONOTIFICA"),
					rs.getString("ESITONOTIFICA"), rs.getString("EMAILDESTINATARIO"), rs.getLong("IDAOO"), new Date(rs.getTimestamp("DATARICEZIONENOTIFICA").getTime()),
					StatoElabNotificaNpsEnum.get(rs.getInt("STATO")), rs.getString("ERRORE"));

		}

		return notifica;
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaNpsDAO#aggiornaStatoNotifica(java.lang.Long,
	 *      java.lang.Integer, java.lang.String, java.sql.Connection).
	 */
	@Override
	public int aggiornaStatoNotifica(final Long idRichiesta, final Integer stato, final String errore, final Connection con) {
		int result = 0;
		PreparedStatement ps = null;
		final ResultSet rs = null;

		try {
			ps = con.prepareStatement("UPDATE REDBATCH_CODA_NPS_NOTIFICHE_OP SET STATO = ?, ERRORE = ? WHERE IDCODA = ?");

			ps.setInt(1, stato);
			if (errore != null) {
				ps.setString(2, errore);
			} else {
				ps.setNull(2, Types.VARCHAR);
			}
			ps.setLong(3, idRichiesta);

			result = ps.executeUpdate();

			LOGGER.info(SUCCESS_AGGIORNAMENTO_NOTIFICA_MSG + idRichiesta + " con stato " + stato);

		} catch (final Exception e) {
			LOGGER.error(ERROR_AGGIORNAMENTO_ITEM_MSG + ID_RICHIESTA_PREFIX + idRichiesta, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaNpsDAO#aggiornaStatoNotifica(java.lang.Long,
	 *      java.lang.Integer, java.lang.String, java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public int aggiornaStatoNotifica(final Long idRichiesta, final Integer stato, final String errore, final String idDocumento, final Connection con) {
		int result = 0;
		PreparedStatement ps = null;
		final ResultSet rs = null;

		try {
			ps = con.prepareStatement("UPDATE REDBATCH_CODA_NPS_NOTIFICHE_OP SET STATO = ?, ERRORE = ?, IDDOCUMENTO = ? WHERE IDCODA = ?");

			ps.setInt(1, stato);
			if (errore != null) {
				ps.setString(2, errore);
			} else {
				ps.setNull(2, Types.VARCHAR);
			}
			ps.setString(3, idDocumento);
			ps.setLong(4, idRichiesta);

			result = ps.executeUpdate();

			LOGGER.info(SUCCESS_AGGIORNAMENTO_NOTIFICA_MSG + idRichiesta + " con stato " + stato + " per il documento " + idDocumento);

		} catch (final Exception e) {
			LOGGER.error(ERROR_AGGIORNAMENTO_ITEM_MSG + ID_RICHIESTA_PREFIX + idRichiesta, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaNpsDAO#getNotificaAzione(java.lang.Long,
	 *      java.lang.Integer, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public NotificaNpsDTO getNotificaAzione(final Long idAoo, final Integer numeroProtocollo, final Integer annoProtocollo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		NotificaNpsDTO notifica = null;

		try {
			final StringBuilder sb = new StringBuilder("SELECT * FROM REDBATCH_CODA_NPS_NOTIFICHE_OP WHERE IDAOO = ? AND NUMEROPROTOCOLLO = ? AND ANNOPROTOCOLLO = ? AND TIPONOTIFICA IN (");
			
			//filtra per le notifiche azione, evitando di estrarre le notifiche operazione
			final TipoNotificaAzioneNPSEnum[] notificheAzione = TipoNotificaAzioneNPSEnum.values();
			TipoNotificaAzioneNPSEnum n = null;
			for(int i=0; i<notificheAzione.length; i++) {
				n = notificheAzione[i];
				if(i == 0) {
					sb.append("'" + n.toString() + "'");
				} else {
					sb.append(", '" + n.toString() + "'");
				}
			}
			sb.append(")");
			
			ps = connection.prepareStatement(sb.toString());

			int index = 1;
			ps.setLong(index++, idAoo);
			ps.setInt(index++, numeroProtocollo);
			ps.setInt(index++, annoProtocollo);

			rs = ps.executeQuery();
			if (rs.next()) {
				notifica = populateItem(rs);
			}

		} catch (final Exception e) {
			LOGGER.error("Errore riscontrato durante il recupero del tipo evento associato al protocollo: [" + numeroProtocollo + "/" + annoProtocollo + "]", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return notifica;
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaNpsDAO#getAndLockNotifica(java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public NotificaNpsDTO getAndLockNotifica(final Integer idCoda, final Connection con) {
		NotificaNpsDTO richiestaElabNotifica = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = con.prepareStatement("SELECT * FROM REDBATCH_CODA_NPS_NOTIFICHE_OP WHERE IDCODA = ? FOR UPDATE SKIP LOCKED");
			ps.setInt(1, idCoda);

			rs = ps.executeQuery();

			if (rs.next()) {
				richiestaElabNotifica = populateItem(rs);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Errore durante il recupero dell'item della coda di elaborazione delle notifiche operazioni di Nps con id = " + idCoda, e);
		} finally {
			closeStatement(ps, rs);
		}

		return richiestaElabNotifica;
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaNpsDAO#aggiornaDatiProtocollo(java.lang.Long,
	 *      java.lang.String, java.lang.Integer, java.lang.Integer, java.util.Date,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public int aggiornaDatiProtocollo(final Long idRichiesta, final String idProtocollo, final Integer numeroProtocollo, final Integer annoProtocollo,
			final java.util.Date dataProtocollo, final String documentTitle, final Connection con) {
		int result = 0;
		PreparedStatement ps = null;
		final ResultSet rs = null;

		try {
			ps = con.prepareStatement("UPDATE REDBATCH_CODA_NPS_NOTIFICHE_OP SET IDPROTOCOLLO = ?, NUMEROPROTOCOLLO = ?, " + "ANNOPROTOCOLLO = ?, " + "DATAPROTOCOLLO = ?, "
					+ "IDDOCUMENTO = ? WHERE IDCODA = ?");

			ps.setString(1, idProtocollo);
			ps.setInt(2, numeroProtocollo);
			ps.setInt(3, annoProtocollo);
			ps.setTimestamp(4, new Timestamp(dataProtocollo.getTime()));
			ps.setString(5, documentTitle);
			ps.setLong(6, idRichiesta);

			result = ps.executeUpdate();

			LOGGER.info(SUCCESS_AGGIORNAMENTO_NOTIFICA_MSG + idRichiesta + " con dati di protocollo");

		} catch (final Exception e) {
			LOGGER.error(ERROR_AGGIORNAMENTO_ITEM_MSG + ID_RICHIESTA_PREFIX + idRichiesta, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return result;
	}

}
