package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.Document;

import filenet.vw.api.VWRosterQuery;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.AllaccioRiferimentoStoricoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DetailFascicoloRedDTO;
import it.ibm.red.business.dto.DetailFatturaFepaDTO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.RiferimentoProtNpsDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.service.IDetailFascicoloSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.IRiferimentoStoricoSRV;
import it.ibm.red.business.service.IStoricoSRV;
import it.ibm.red.business.service.ITitolarioSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * Service gestione dettagli fascicolo.
 */
@Service
public class DetailFascicoloSRV extends AbstractService implements IDetailFascicoloSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -9211873902892260451L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DetailFascicoloSRV.class);

	/************************************************************************/
	/**
	 * Dao per il caricamento dello storico.
	 */
	@Autowired
	private IStoricoSRV storicoSRV;

	/**
	 * Nodo dao.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Utente dao.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * Service.
	 */
	@Autowired
	private ITitolarioSRV titFacSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IFascicoloSRV fascicoloSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IRiferimentoStoricoSRV rifStoricoSRV;

	/**
	 * @see it.ibm.red.business.service.facade.IDetailFascicoloFacadeSRV#getFascicolo(it.ibm.red.business.dto.FascicoloDTO,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public DetailFascicoloRedDTO getFascicolo(final FascicoloDTO fascicolo, final UtenteDTO utente) {
		DetailFascicoloRedDTO detailFascicolo = null;

		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		Connection connection = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			connection = setupConnection(getDataSource().getConnection(), false);

			detailFascicolo = this.fromFascicoloDtoToDetailFascicoloRedDTO(fascicolo);

			final Integer idfascicolo = Integer.valueOf(detailFascicolo.getNomeFascicolo());

			final Long idAOO = Long.valueOf(detailFascicolo.getIdAOO());
			final List<DocumentoFascicoloDTO> documenti = getDocumentiByIdFascicolo(idfascicolo, idAOO, fceh);
			// DOCUMENTI
			detailFascicolo.setDocumenti(documenti);

			fillAssegnazioniStoricoFaldoneTitolarioByDocumenti(utente, detailFascicolo, fpeh, fceh, connection);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}

		return detailFascicolo;
	}

	/**
	 * Per ogni documento presente nel dettaglio del fascicolo si calcola: le
	 * assegnazioni lo storico il faldone e i titolari
	 * 
	 * Questo metodo è pensato per evitare la duplicazione del codice.
	 * 
	 * Dovrebbe essere eventualmente separato per motivi di chiarezza
	 * 
	 * @param utente
	 * @param detailFascicolo
	 * @param fpeh
	 * @param fceh
	 * @param connection
	 */
	private void fillAssegnazioniStoricoFaldoneTitolarioByDocumenti(final UtenteDTO utente, final DetailFascicoloRedDTO detailFascicolo, final FilenetPEHelper fpeh,
			final IFilenetCEHelper fceh, final Connection connection) {
		// ASSEGNAZIONI
		detailFascicolo.setAssegnazioni(new ArrayList<>());
		// STORICO
		detailFascicolo.setStorico(new ArrayList<>());
		// RIF STORICO NSD
		detailFascicolo.setRifStoricoNsd(new ArrayList<>());
		 
		//RIF ALLACCIO NPS
		detailFascicolo.setRifAllaccioNps(new ArrayList<>());

		DetailDocumentRedDTO detailDocumento = null;
		for (final DocumentoFascicoloDTO d : detailFascicolo.getDocumenti()) {
			if (Boolean.FALSE.equals(d.getbFlagInfoFascicolo()) || d.getAoo() != null) {
				// mi serve per il metodo di DocumentoRedSRV
				detailDocumento = new DetailDocumentRedDTO();
				detailDocumento.setDocumentTitle(d.getDocumentTitle());
				detailDocumento.setIdCategoriaDocumento(d.getIdCategoria());

				detailDocumento.setIdAOO(d.getAoo().getIdAoo().intValue());

				final VWRosterQuery query = fpeh.getRosterQueryWorkByIdDocumentoAndCodiceAOO(d.getDocumentTitle(), utente.getFcDTO().getIdClientAoo());
				List<AssegnazioneDTO> assegnazioniList = fpeh.getAssegnazioni(d.getDocumentTitle(), d.getAoo().getCodiceAoo(), query);

				Utente ute = null;
				UtenteDTO inUtente = null;
				Nodo n = null;
				UfficioDTO inUfficio = null;
				// per ogni assegnazione recupero i dati del nodo ufficio e se ci sono i dati
				// dell'utente
				for (final AssegnazioneDTO a : assegnazioniList) {

					n = nodoDAO.getNodo(a.getUfficio().getId(), connection);
					if (n == null) {
						continue; // l'ufficio e' importante che ci sia
					}
					inUfficio = new UfficioDTO(n.getIdNodo(), n.getDescrizione());

					if (a.getUtente().getId() != null && a.getUtente().getId() > 0) {
						ute = utenteDAO.getUtente(a.getUtente().getId(), connection);
						inUtente = new UtenteDTO(ute);
						inUtente.setIdUfficio(n.getIdNodo());
						inUtente.setCodiceUfficio(n.getCodiceNodo());
						inUtente.setUfficioDesc(n.getDescrizione());
						a.setDescrizioneAssegnatario(inUtente.getNome() + " " + inUtente.getCognome());
					} else {
						inUtente = null;
						a.setDescrizioneAssegnatario(inUfficio.getDescrizione());
					}

					a.setUfficio(inUfficio);
					a.setUtente(inUtente);
				}
				assegnazioniList = assegnazioniList == null ? new ArrayList<>() : assegnazioniList;
				detailFascicolo.getAssegnazioni().addAll(assegnazioniList);
				// devo settarli nel documento perché servono nel metodo isDocumentoModificabile
				detailDocumento.setAssegnazioni(assegnazioniList);

				final int idDocumento = Integer.parseInt(d.getDocumentTitle());
				Collection<StoricoDTO> storico = storicoSRV.getStorico(idDocumento, d.getAoo().getIdAoo().intValue());
				String id = "";
				for (final StoricoDTO storicoDTO : storico) {
					id = "" + d.getNumeroDocumento();
					storicoDTO.setIdDocumento(id);
				}
				storico = storico == null ? new ArrayList<>() : storico;
				detailFascicolo.getStorico().addAll(storico);

				if (detailFascicolo.getRifStoricoNsd() != null) {
					final List<AllaccioRiferimentoStoricoDTO> allaccioRifStoricoList = rifStoricoSRV.getRiferimentoStoricoByAoo(d.getDocumentTitle(), utente.getIdAoo(),
							detailFascicolo.getNomeFascicolo());
					if (allaccioRifStoricoList != null && !allaccioRifStoricoList.isEmpty()) {
						for (final AllaccioRiferimentoStoricoDTO allaccioRifStorico : allaccioRifStoricoList) {
							allaccioRifStorico.setIdDocUscita(d.getNumeroDocumento());
						}
						detailFascicolo.setRifStoricoNsd(allaccioRifStoricoList);
					}
				}
				
				if(detailFascicolo.getRifAllaccioNps()!=null) { 
					List<RiferimentoProtNpsDTO> allaccioRifNpsList = rifStoricoSRV.getRiferimentoNpsByAoo(d.getDocumentTitle(), utente.getIdAoo(),detailFascicolo.getNomeFascicolo());
					if(allaccioRifNpsList!=null && !allaccioRifNpsList.isEmpty()) { 
						for(RiferimentoProtNpsDTO allaccioRifNps : allaccioRifNpsList) {
							allaccioRifNps.setIdDocumentoUscita(d.getNumeroDocumento());
						}
						detailFascicolo.setRifAllaccioNps(allaccioRifNpsList);
					}
				}
			}
		}

		final Long idAOOFascicolo = Long.valueOf(detailFascicolo.getIdAOO());
		/** RECUPERO FALDONI start ***********************************/
		detailFascicolo.setFaldoni(getFaldoniByIdFascicolo(detailFascicolo.getNomeFascicolo(), idAOOFascicolo, fceh));
		/** RECUPERO FALDONI end *************************************/

		/** TITOLARIO START *********************************************/
		detailFascicolo.setTitolarioDTO(titFacSRV.getNodoByIndice(idAOOFascicolo, detailFascicolo.getTitolario()));
		if (detailFascicolo.getTitolarioDTO() != null) {
			detailFascicolo.setTitolario(detailFascicolo.getTitolarioDTO().getIndiceClassificazione() + " - " + detailFascicolo.getTitolarioDTO().getDescrizione());
		}
		/** TITOLARIO END *********************************************/

	}

	/**
	 * @see it.ibm.red.business.service.IDetailFascicoloSRV#fromFascicoloDtoToDetailFascicoloRedDTO(it.ibm.red.business.dto.FascicoloDTO).
	 */
	@Override
	public DetailFascicoloRedDTO fromFascicoloDtoToDetailFascicoloRedDTO(final FascicoloDTO fascicoloDTO) {
		DetailFascicoloRedDTO f = null;

		if (fascicoloDTO == null) {
			return f;
		}

		f = new DetailFascicoloRedDTO();
		f.setClasseDocumentale(fascicoloDTO.getClasseDocumentale());
		f.setDataCreazione(fascicoloDTO.getDataCreazione());
		f.setDataTerminazione(fascicoloDTO.getDataTerminazione());
		f.setDocumentTitle(fascicoloDTO.getDocumentTitle());
		f.setFaldonato(fascicoloDTO.getFaldonato());
		f.setGuid(fascicoloDTO.getGuid());
		f.setIdAOO(fascicoloDTO.getIdAOO());
		f.setIdFascicoloFEPA(fascicoloDTO.getIdFascicoloFEPA());
		f.setNomeFascicolo(fascicoloDTO.getNomeDocumento());
		f.setOggetto(fascicoloDTO.getOggetto());
		f.setStato(fascicoloDTO.getStato());
		f.setTitolario(fascicoloDTO.getIndiceClassificazione());

		final String[] oggettoSplit = f.getOggetto().split("_");
		final String anno = oggettoSplit[1];
		final String descr = f.getOggetto().replace((oggettoSplit[0] + "_" + oggettoSplit[1] + "_"), "");

		f.setAnno(anno);
		f.setOggettoNoNumeroAnno(descr);

		return f;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDetailFascicoloFacadeSRV#convertDettaglioToFascicolo(it.ibm.red.business.dto.DetailFascicoloRedDTO,
	 *      it.ibm.red.business.dto.FascicoloDTO).
	 */
	@Override
	public void convertDettaglioToFascicolo(final DetailFascicoloRedDTO detail, final FascicoloDTO fascicolo) {
		
		/*
		 * Nell'implementazione ho inserito dei controlli per evitare che una differenza
		 * nella creazione dell'oggetto mi cancelli un dato non modificato E.G. il
		 * documentTitle del fascicolo viene sovrascritto dal detail che però sicocme
		 * non viene recuperato è nullo
		 * 
		 */
		
		if (detail == null || fascicolo == null) {
			throw new RedException("Sono stati passati argomenti nulli");
		}

		if (detail.getClasseDocumentale() != null) {
			fascicolo.setClasseDocumentale(detail.getClasseDocumentale());
		}
		if (detail.getDataCreazione() != null) {
			fascicolo.setDataCreazione(detail.getDataCreazione());
		}
		if (detail.getDataTerminazione() != null) {
			fascicolo.setDataTerminazione(detail.getDataTerminazione());
		}

		if (detail.getDocumentTitle() != null) {
			fascicolo.setDocumentTitle(detail.getDocumentTitle());
		}
		if (detail.getFaldonato() != null) {
			fascicolo.setFaldonato(detail.getFaldonato());
		}
		if (detail.getGuid() != null) {
			fascicolo.setGuid(detail.getGuid());
		}
		if (detail.getIdAOO() != null) {
			fascicolo.setIdAOO(detail.getIdAOO());
		}
		if (detail.getIdFascicoloFEPA() != null) {
			fascicolo.setIdFascicoloFEPA(detail.getIdFascicoloFEPA());
		}
		if (detail.getNomeFascicolo() != null) {
			fascicolo.setNomeDocumento(detail.getNomeFascicolo());
		}
		if (detail.getOggetto() != null) {
			fascicolo.setOggetto(detail.getOggetto());
		}
		if (detail.getStato() != null) {
			fascicolo.setStato(detail.getStato());
		}
		if (detail.getTitolario() != null) {
			fascicolo.setIndiceClassificazione(detail.getTitolario());
		}
		if (detail.getAnno() != null) {
			fascicolo.setAnno(detail.getAnno());
		}
	}

	/**
	 * @see it.ibm.red.business.service.IDetailFascicoloSRV#getDocumentiByIdFascicolo(java.lang.Integer,
	 *      java.lang.Long, it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public List<DocumentoFascicoloDTO> getDocumentiByIdFascicolo(final Integer idFascicolo, final Long idAOO, final IFilenetCEHelper fceh) {
		return new ArrayList<>(fceh.getOnlyDocumentiRedFascicolo(idFascicolo, idAOO));
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDetailFascicoloFacadeSRV#getDocumentiByIdFascicolo(java.lang.Integer,
	 *      java.lang.Long, it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public List<DocumentoFascicoloDTO> getDocumentiByIdFascicolo(final Integer idFascicolo, final Long idAOO, final UtenteDTO utente) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			return new ArrayList<>(fceh.getOnlyDocumentiRedFascicolo(idFascicolo, idAOO));
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDetailFascicoloFacadeSRV#getFattureFepaByIdFascicolo(java.lang.Integer,
	 *      java.lang.Long, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<DetailFatturaFepaDTO> getFattureFepaByIdFascicolo(final Integer idFascicolo, final Long idAOO, final UtenteDTO utente) {
		IFilenetCEHelper fceh = null;
		List<DetailFatturaFepaDTO> fatture = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fatture = new ArrayList<>(fceh.getOnlyFattureFromRedFascicolo(idFascicolo, idAOO));

			for (final DetailFatturaFepaDTO fattura : fatture) {
				FascicoloDTO fascicoloProcedimentale = null;

				fascicoloProcedimentale = fascicoloSRV.getFascicoloProcedimentale(fattura.getDocumentTitle(), Integer.parseInt("" + idAOO), utente);

				fattura.setDescrizioneFascicoloProcedimentale(fascicoloProcedimentale.getDescrizione());
				fattura.setIdFascicoloProcedimentale(fascicoloProcedimentale.getIdFascicolo());
				if (!StringUtils.isNullOrEmpty(fascicoloProcedimentale.getOggetto())) {
					final String[] arr = fascicoloProcedimentale.getOggetto().split("_");
					if (arr.length >= 3) {
						fattura.setPrefixFascicoloProcedimentale(arr[0] + "_" + arr[1] + "_");
					}
				}
				fattura.setIndiceClassificazioneFascicoloProcedimentale(fascicoloProcedimentale.getIndiceClassificazione());
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}

		return fatture;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDetailFascicoloFacadeSRV#
	 *      getFaldoniByIdFascicolo(java.lang.String, java.lang.Long,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<FaldoneDTO> getFaldoniByIdFascicolo(final String idFascicolo, final Long idAOO, final UtenteDTO utente) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			return getFaldoniByIdFascicolo(idFascicolo, idAOO, fceh);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IDetailFascicoloSRV#getFaldoniByIdFascicolo(java.
	 *      lang.String, java.lang.Long,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetHelper).
	 */
	@Override
	public List<FaldoneDTO> getFaldoniByIdFascicolo(final String idFascicolo, final Long idAOO, final IFilenetCEHelper fceh) {
		List<FaldoneDTO> list = null;

		try {
			final DocumentSet faldoniFilenet = fceh.getFaldoniFascicolo(idFascicolo, idAOO);

			if (faldoniFilenet != null && !faldoniFilenet.isEmpty()) {
				list = new ArrayList<>();
				final Iterator<?> it = faldoniFilenet.iterator();
				Document faldoneFilenet = null;
				FaldoneDTO f = null;
				while (it.hasNext()) {
					faldoneFilenet = (Document) it.next();
					f = TrasformCE.transform(faldoneFilenet, TrasformerCEEnum.FROM_DOCUMENT_TO_FALDONE);
					if (!StringUtils.isNullOrEmpty(f.getParentFaldone())) {
						// se il padre esiste lo valorizzo
						final Document padreD = fceh.getFaldoneByDocumentTitle(f.getParentFaldone(), f.getIdAOO());
						if (padreD != null) {
							final String nomePadre = (String) TrasformerCE.getMetadato(padreD, PropertiesNameEnum.METADATO_FALDONE_NOMEFALDONE);
							f.setNomePadre(nomePadre);
							final String oggettoPadre = (String) TrasformerCE.getMetadato(padreD, PropertiesNameEnum.METADATO_FALDONE_OGGETTO);
							f.setOggettoPadre(oggettoPadre);
						}
					}
					list.add(f);
				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}

		return list;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDetailFascicoloFacadeSRV#getFascicoloDTOperTabFascicoloeFaldone(it.ibm.red.business.dto.FascicoloDTO,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public DetailFascicoloRedDTO getFascicoloDTOperTabFascicoloeFaldone(final FascicoloDTO fascicolo, final UtenteDTO utente) {
		DetailFascicoloRedDTO detailFascicolo = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			detailFascicolo = fromFascicoloDtoToDetailFascicoloRedDTO(fascicolo);

			final Integer idfascicolo = Integer.valueOf(detailFascicolo.getNomeFascicolo());
			final Long idAOO = Long.valueOf(detailFascicolo.getIdAOO());

			// DOCUMENTI
			detailFascicolo.setDocumenti(getDocumentiByIdFascicolo(idfascicolo, idAOO, fceh));

			/** RECUPERO FALDONI start ***********************************/
			detailFascicolo.setFaldoni(getFaldoniByIdFascicolo(detailFascicolo.getNomeFascicolo(), idAOO, fceh));
			/** RECUPERO FALDONI end *************************************/

			/** TITOLARIO START *********************************************/
			detailFascicolo.setTitolarioDTO(titFacSRV.getNodoByIndice(idAOO, detailFascicolo.getTitolario()));
			if (detailFascicolo.getTitolarioDTO() != null) {
				detailFascicolo.setTitolario(detailFascicolo.getTitolarioDTO().getIndiceClassificazione() + " - " + detailFascicolo.getTitolarioDTO().getDescrizione());
			}
			/** TITOLARIO END *********************************************/

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}

		return detailFascicolo;
	}

	/**
	 * Recupera il dettaglio. Il amster fascicolo contiene già il documento mentre
	 * non contiene il fascicolo, ma solo il nome fascicolo.
	 * 
	 * @param idFascicolo
	 * @param documenti
	 * @param utente
	 * @return DetailFascicoloRedDTO
	 */
	@Override
	public DetailFascicoloRedDTO getDetailFascicolo(final Integer idFascicolo, final List<DocumentoFascicoloDTO> documenti, final UtenteDTO utente) {
		DetailFascicoloRedDTO detailFascicolo = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		Connection connection = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			final FascicoloDTO fascicolo = fascicoloSRV.getFascicoloByNomeFascicolo(idFascicolo.toString(), utente, fceh);

			connection = setupConnection(getDataSource().getConnection(), false);
			detailFascicolo = fromFascicoloDtoToDetailFascicoloRedDTO(fascicolo);

			List<DocumentoFascicoloDTO> documentiRecuperati = documenti;
			// DOCUMENTI
			if (documentiRecuperati == null || documentiRecuperati.isEmpty()) {
				final Long idAOO = Long.valueOf(detailFascicolo.getIdAOO());
				documentiRecuperati = getDocumentiByIdFascicolo(idFascicolo, idAOO, fceh);
			}

			detailFascicolo.setDocumenti(documentiRecuperati);

			fillAssegnazioniStoricoFaldoneTitolarioByDocumenti(utente, detailFascicolo, fpeh, fceh, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del dettaglio del fascicolo.", e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}

		return detailFascicolo;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDetailFascicoloFacadeSRV#getDetailFascicolo(java.lang.Integer,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public DetailFascicoloRedDTO getDetailFascicolo(final Integer idFascicolo, final UtenteDTO utente) {
		return getDetailFascicolo(idFascicolo, null, utente);
	}

	/**
	 * Restituisce l'id del fascicolo procedimentale eseguendo una query di ricerca.
	 * 
	 * @param docTitle
	 *            documentTitle del documento per cui si vuole conoscere l'id del
	 *            fascicolo associato.
	 * @param idAoo
	 * @param fcDTO
	 * @return id del fascicolo
	 */
	@Override
	public String getFascicoloProcedimentale(final String docTitle, final Integer idAoo, final FilenetCredentialsDTO fcDTO) {
		IFilenetCEHelper fceh = null;
		Connection conn = null;
		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo.longValue());
			conn = setupConnection(getDataSource().getConnection(), false);
			final FascicoloDTO fascicoloProc = fascicoloSRV.getFascicoloProcedimentale(docTitle, idAoo, fceh, conn);
			return fascicoloProc.getIdFascicolo();
		} catch (final Exception ex) {
			LOGGER.error("Errore durante il recupero del fascicolo procedimentale", ex);
			throw new RedException(ex);
		} finally {
			closeConnection(conn);
			popSubject(fceh);
		}

	}
}
