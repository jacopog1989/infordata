package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.EsitoCreazioneRdsDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.RdsSiebelDTO;
import it.ibm.red.business.dto.RdsSiebelDescrizioneDTO;
import it.ibm.red.business.dto.RdsSiebelGruppoDTO;
import it.ibm.red.business.dto.RdsSiebelTipologiaDTO;
import it.ibm.red.business.dto.TestoPredefinitoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.SiebelException;
import it.ibm.red.webservice.model.redsiebel.types.AllegatoType;

/**
 * Facade del servizio Siebel.
 */
public interface ISiebelFacadeSRV extends Serializable {

	/**
	 * Viene restituito l'idContattoSiebel associato all'ufficio in input se
	 * l'ufficio è abilitato su Siebel.
	 * 
	 * @param idNodo
	 * @return
	 */
	String retrieveIdContattoSiebel(Long idNodo);

	/**
	 * Controlla se il documento in input è associato a RdS aperte.
	 * 
	 * @param idDocumento
	 * @return
	 */
	boolean hasRdsAperte(String idDocumento, Long idAOO);

	/**
	 * Controlla se i documenti in input sono associati a RdS.
	 * 
	 * @param ids
	 * @return
	 */
	Map<String, Boolean> hasRds(Collection<String> ids, Long idAOO);

	/**
	 * Restituisce le RdS Siebel associate al documento in input.
	 * 
	 * @param idDocumento
	 * @return
	 */
	List<RdsSiebelDTO> getRds(String idDocumento, Long idAOO);

	/**
	 * Recupera l'email originale rlativa al documento che insiste sul workflow in
	 * input.
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	DocumentoAllegabileDTO getMailDocumentoByWobNumber(UtenteDTO utente, String wobNumber);

	/**
	 * Apre una RdS su Siebel e, in caso l'operazione sia andata a buon fine,
	 * registra mette in sospeso il documento.
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param emailAlternativa
	 * @param oggetto
	 * @param testo
	 * @param allegati
	 * @param idTipologia
	 * @param codiceGruppo
	 * @return
	 */
	EsitoCreazioneRdsDTO apriRdsSiebel(UtenteDTO utente, String wobNumber, String emailAlternativa, String oggetto, String testo, List<DocumentoAllegabileDTO> allegati,
			Integer idTipologia, String codiceGruppo);

	/**
	 * Verifica che idRdsSiebel sia una Rds presente in Red e che il documentTitle
	 * relativo sia associato al protocollo. numeroProtocolloRed/annoProtocolloRed
	 * 
	 * @param idRdsSiebel
	 * @param numeroProtocolloRed
	 * @param annoProtocolloRed
	 * @return
	 */
	String checkInfoAggiornaRds(String idRdsSiebel, Integer numeroProtocolloRed, Integer annoProtocolloRed);

	/**
	 * Aggiorna la RdS.
	 * 
	 * @param idRdsSiebel
	 * @param numeroProtocolloRed
	 * @param annoProtocolloRed
	 * @param dataChiusuraRdsSiebel
	 * @param dataEvento
	 * @param soluzione
	 * @param tipoEvento
	 * @param allegati
	 * @throws SiebelException
	 */
	void aggiornaRds(String idRdsSiebel, Integer numeroProtocolloRed, Integer annoProtocolloRed, Date dataChiusuraRdsSiebel, Date dataEvento, String soluzione,
			String tipoEvento, List<AllegatoType> allegati);

	/**
	 * Testo predefinito per le email da inviare a Siebel.
	 * 
	 * @param utente Richiedente dell'operazione.
	 * 
	 * @return Una lita di oggetti contenenti i testi dell'email e gli oggetti
	 *         utilizzabili per default.
	 */
	List<TestoPredefinitoDTO> getTestiPredefiniti(UtenteDTO utente);

	/**
	 * Ritorna l'oggetto di default per l'invio a siebel.
	 * 
	 * @param master
	 * @param fascicolo
	 * @param utente
	 * 
	 * @return
	 */
	String createDefaultOggetto(MasterDocumentRedDTO master, FascicoloDTO fascicolo, UtenteDTO utente);

	/**
	 * Property di sistema che regola la dimensione massima degli allegati per
	 * l'inoltro a Siebel.
	 */
	BigDecimal getDimensioneMassimaTotaleAllegatiMailBigDecimalKB();

	/**
	 * Ritorna una stringa contenente gli id dello storico Rds.
	 * 
	 * @param master
	 * @return
	 */
	String getPrefissoTesto(MasterDocumentRedDTO master, Long idAOO);

	/**
	 * Qualora il documento di riferimento sia protocollato, ritorna il protocollo
	 * originale del documento in formato numeroProtocollo/annoProtocollo.
	 * 
	 * @param master Documento di riferimento.
	 * @return Una stringa corrispondente al numeroProtocollo/annoProtocollo.
	 *         Qualora il documento non sia protocollato ritorna una stringa vuota.
	 */
	String getProtocolloOriginale(MasterDocumentRedDTO master);

	/**
	 * Recupera le rds di un documento, le rds recuperate in questo modo sono
	 * comprensive di informazioni accessorie sull'utente che ha aperto la rds e sul
	 * nodo dell'utente.
	 * 
	 * @param idDocumento identificativo del documeento del quale vengono recuperate
	 *                    le rds.
	 * @return Una lista di rds.
	 */
	List<RdsSiebelDescrizioneDTO> getRdsDescrizione(String idDocumento, Long idAOO);

	/**
	 * Recupera l'email originale rlativa al documento identificato dal
	 * documentTitle in input
	 * 
	 * @param utente
	 * @param documentTitle
	 * @return
	 */
	DocumentoAllegabileDTO getMailDocumentoByDocumentTitle(UtenteDTO utente, String documentTitle);

	/**
	 * Ottiene la mappa tipi gruppi siebel.
	 * 
	 * @return mappa tipi gruppi siebel
	 */
	Map<RdsSiebelTipologiaDTO, List<RdsSiebelGruppoDTO>> getMappaTipiGruppiSiebel();

}
