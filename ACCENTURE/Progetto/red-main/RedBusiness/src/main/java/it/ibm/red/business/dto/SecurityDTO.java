package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * DTO rappresentante le security Filenet ereditate da RED.
 * @author CPAOLUZI
 *
 */
public class SecurityDTO extends AbstractDTO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 7490669429342713251L;
	
	/**
	 * Oggetto gruppo.
	 */
	private GruppoS gruppo;

	/**
	 * Oggetto utente.
	 */
	private UtenteS utente;

	/**
	 * Costruttore.
	 */
	public SecurityDTO() {
		this.utente = new UtenteS();
		this.gruppo = new GruppoS();
	}

	/**
	 * Getter.
	 * 
	 * @return	gruppo
	 */
	public final GruppoS getGruppo() {
		return gruppo;
	}

	/**
	 * Setter.
	 * 
	 * @param inGruppo	gruppo
	 */
	public final void setGruppo(final GruppoS inGruppo) {
		this.gruppo = inGruppo;
	}

	/**
	 * Getter utente.
	 * 	
	 * @return	utente
	 */
	public final UtenteS getUtente() {
		return utente;
	}

	/**
	 * Setter.
	 * 
	 * @param inUtente	utente
	 */
	public final void setUtente(final UtenteS inUtente) {
		this.utente = inUtente;
	}
	
	/**
	 * The Class GruppoS.
	 *
	 * @author CPIERASC
	 * 
	 * 	DTO gruppo.
	 */
	public class GruppoS implements Serializable {

		/**
		 * The Constant serialVersionUID.
		 */
		private static final long serialVersionUID = 1L;
		
		/**
		 * Identificativo ufficio.
		 */
		private int idUfficio;

		/**
		 * Identificativo siap.
		 */
		private int idSiap;
		
		/**
		 * Costruttore.
		 */
		public GruppoS() {
			//Metodo intenzionalmente lasciato vuoto.
		}

		/**
		 * Getter.
		 * 
		 * @return	identificativo ufficio
		 */
		public final int getIdUfficio() {
			return idUfficio;
		}

		/**
		 * Setter.
		 * 
		 * @param inIdUfficio	identificativo ufficio
		 */
		public final void setIdUfficio(final int inIdUfficio) {
			this.idUfficio = inIdUfficio;
		}

		/**
		 * Getter.
		 * 
		 * @return	identificativo siap
		 */
		public final int getIdSiap() {
			return idSiap;
		}

		/**
		 * Setter.
		 * 
		 * @param inIdSiap	identificativo siap
		 */
		public final void setIdSiap(final int inIdSiap) {
			this.idSiap = inIdSiap;
		}
	}
	
	/**
	 * The Class UtenteS.
	 *
	 * @author CPIERASC
	 * 
	 * 	DTO utente.
	 */
	public class UtenteS implements Serializable {

		/**
		 * The Constant serialVersionUID.
		 */
		private static final long serialVersionUID = 1L;
		
		/**
		 * Identificativo utente.
		 */
		private int idUtente;
		
		/**
		 * Username.
		 */
		private String username;

		/**
		 * Costruttore.
		 */
		public UtenteS() {
			//Metodo intenzionalmente lasciato vuoto.
		}

		/**
		 * Getter.
		 * 
		 * @return	username
		 */
		public final String getUsername() {
			return username;
		}

		/**
		 * Setter.
		 * 
		 * @param inUsername	username
		 */
		public final void setUsername(final String inUsername) {
			this.username = inUsername;
		}

		/**
		 * Getter identificativo utente.
		 * 
		 * @return	identificativo utente
		 */
		public final int getIdUtente() {
			return idUtente;
		}
		
		/**
		 * Setter.
		 * 
		 * @param inIdUtente	identificativo utente
		 */
		public final void setIdUtente(final int inIdUtente) {
			this.idUtente = inIdUtente;
		}
	}

}
