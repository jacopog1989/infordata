package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;

import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.Document;

import it.ibm.red.business.dto.MailAttachmentDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Trasforma il Documento in un MailAttachment la cui informazione essenziale è
 * l'id di filenet per recuperare il file e il nome dell'allegato.
 * 
 * Al contrario di altre trasformatori scarica solo i metadati dell'allegato e
 * non il suo contenuto in byte[].
 */
public class FromDocumentoToAllegatoDetailTrasformer extends TrasformerCE<MailAttachmentDTO> {

	/*
	 * Implementations of serializable.
	 */
	private static final long serialVersionUID = -597615463270888191L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentoToAllegatoDetailTrasformer.class.getName());
	
	/**
	 * Costruttore.
	 */
	public FromDocumentoToAllegatoDetailTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_ALLEGATO_DETAIL_MAIL);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE#trasform(com.filenet.api.core.Document,
	 *      java.sql.Connection).
	 */
	@Override
	public MailAttachmentDTO trasform(final Document document, final Connection connection) {
		try {
			String inDocumentTitle = (String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			String inFileName = (String) getMetadato(document, PropertiesNameEnum.NOME_FILE_METAKEY);
			String inDescription = (String) getMetadato(document, PropertiesNameEnum.DESCRIZIONE_TIPOLOGIA_DOCUMENTO_NPS_METAKEY);
			String id = getMetadato(document, PropertyNames.ID).toString();
			String inMimeType = getMetadato(document, PropertyNames.MIME_TYPE).toString();
			
			return new MailAttachmentDTO(id, inFileName, inMimeType, inDescription, null, inDocumentTitle);
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero di un metadato del dettaglio del documento: ", e);
			return null;
		}
	}
	
}
