package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IOrganigrammaDAO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.OrganigrammaVisualizzazioneCompetenzaIngressoEnum;
import it.ibm.red.business.enums.OrganigrammaVisualizzazioneEnum;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipoStrutturaNodoEnum;
import it.ibm.red.business.enums.TipologiaNodoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IOrganigrammaSRV;
import it.ibm.red.business.utils.PermessiUtils;

/**
 * Service di gestione organigramma.
 */
@Service
@Component
public class OrganigrammaSRV extends AbstractService implements IOrganigrammaSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -858066436003733633L;

	/**
	 * Messaggio errore raggiungimento connessione.
	 */
	private static final String ERROR_RAGGIUNGIMENTO_CONNECTION_MSG = "Errore nel raggiungere la connessione";

	/**
	 * Messaggio errore caricamento Tree selezione DELEGANTE.
	 */
	private static final String ERROR_CARICAMENTO_TREE_DELEGANTE_MSG = "Errore durante il caricamento del tree per la selezione del DELEGANTE";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(OrganigrammaSRV.class);

	/**
	 * DAO.
	 */
	@Autowired
	private IOrganigrammaDAO organigrammaDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	private static String getVisualizzazione(final boolean def, final boolean fogliaOrganigramma) {
		if (!def) {
			if (fogliaOrganigramma) {
				return OrganigrammaVisualizzazioneEnum.VISUALIZZAZIONE1.toString();
			} else {
				return null;
			}
		} else {
			return OrganigrammaVisualizzazioneEnum.VISUALIZZAZIONE0.toString();
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getFigliAlberoAssegnatarioPerCompetenzaUscita(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerCompetenzaUscita(final UtenteDTO utente, final NodoOrganigrammaDTO nodoSelected) {
		Connection connection = null;
		List<NodoOrganigrammaDTO> figli = new ArrayList<>();
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			figli = getFigliAlberoAssegnatarioPerCompetenzaUscita(utente, nodoSelected, connection);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
		return figli;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getFigliAlberoAssegnatarioPerCompetenzaUscita(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO, boolean).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerCompetenzaUscita(UtenteDTO utente,
			NodoOrganigrammaDTO nodoSelected, boolean visualizzaUtenti) {
		List<NodoOrganigrammaDTO> figli = new ArrayList<>();
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			figli = getFigliAlberoAssegnatarioPerCompetenzaUscita(utente, nodoSelected,visualizzaUtenti, connection);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
		return figli;
	}

	/**
	 * @see it.ibm.red.business.service.IOrganigrammaSRV#getFigliAlberoAssegnatarioPerCompetenzaUscita(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO, java.sql.Connection).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerCompetenzaUscita(final UtenteDTO utente, final NodoOrganigrammaDTO nodeToOpen, final Connection connection) {
		final String visualizzazione = getVisualizzazione(false, false);

		final boolean visualizzaUtenti = true;
		boolean onlyIspettorato = true;
		boolean visualizzaUtentiSoloIspettoratoCorrente = true;
		if (utente.getIdAoo().toString().equals(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IDAOO_RL))) {
			onlyIspettorato = false;
			visualizzaUtentiSoloIspettoratoCorrente = false;
		}
		return filtroGenerico(utente.getIdUfficio(), nodeToOpen, visualizzaUtenti, null, visualizzazione, connection, onlyIspettorato, visualizzaUtentiSoloIspettoratoCorrente,
				false);

	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getFigliAlberoAssegnatarioPerCompetenzaUscita(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO, boolean,
	 *      java.sql.Connection).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerCompetenzaUscita(UtenteDTO utente,
			NodoOrganigrammaDTO nodeToOpen,boolean visualizzaUtenti,  Connection connection) {
		String visualizzazione = getVisualizzazione(false, false);

		boolean onlyIspettorato= true;
		boolean visualizzaUtentiSoloIspettoratoCorrente = true;
		if(utente.getIdAoo().toString().equals(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IDAOO_RL))) {
			onlyIspettorato = false;
			visualizzaUtentiSoloIspettoratoCorrente = false;
		}
		return filtroGenerico(utente.getIdUfficio(), nodeToOpen, visualizzaUtenti, null, visualizzazione, connection, onlyIspettorato, visualizzaUtentiSoloIspettoratoCorrente, false);

	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getFigliAlberoAssegnatarioPerCompetenzaIngresso(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerCompetenzaIngresso(final UtenteDTO utente, final NodoOrganigrammaDTO nodeToOpen) {
		Connection connection = null;
		List<NodoOrganigrammaDTO> figli = new ArrayList<>();
		
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			figli = getFigliAlberoAssegnatarioPerCompetenzaIngresso(utente, nodeToOpen, connection);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
		return figli;
	}

	/**
	 * @see it.ibm.red.business.service.IOrganigrammaSRV#getFigliAlberoAssegnatarioPerCompetenzaIngresso(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO, java.sql.Connection).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerCompetenzaIngresso(final UtenteDTO utente, final NodoOrganigrammaDTO nodeToOpen,
			final Connection connection) {
		final String visualizzazione = getVisualizzazione(true, false);

		boolean visualizzaUtenti = false;
		boolean onlyIspettorato = true;
		boolean visualizzaUtentiSoloIspettoratoCorrente = true;
		if (utente.getIdAoo().toString().equals(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IDAOO_RL))) {
			visualizzaUtenti = true;
			onlyIspettorato = false;
			visualizzaUtentiSoloIspettoratoCorrente = false;
		}
		return filtroGenerico(utente.getIdUfficio(), nodeToOpen, visualizzaUtenti, null, visualizzazione, connection, onlyIspettorato, visualizzaUtentiSoloIspettoratoCorrente,
				false);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovo(java.lang.Boolean,
	 *      java.lang.Long, it.ibm.red.business.dto.NodoOrganigrammaDTO,
	 *      java.lang.String).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovo(final Boolean isEntrata, final Long idUfficioUtente,
			final NodoOrganigrammaDTO nodeToOpen, final String indirizzoEmail, final boolean consideraDisattivi) {
		return getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovo(isEntrata, idUfficioUtente, nodeToOpen, indirizzoEmail,
				OrganigrammaVisualizzazioneCompetenzaIngressoEnum.STANDARD, consideraDisattivi);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovoConUtenti(java.lang.Boolean,
	 *      java.lang.Long, it.ibm.red.business.dto.NodoOrganigrammaDTO,
	 *      java.lang.String).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovoConUtenti(final Boolean isEntrata, final Long idUfficioUtente,
			final NodoOrganigrammaDTO nodeToOpen, final String indirizzoEmail, final boolean consideraDisattivi) {
		return getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovo(isEntrata, idUfficioUtente, nodeToOpen, indirizzoEmail,
				OrganigrammaVisualizzazioneCompetenzaIngressoEnum.CON_UTENTI, consideraDisattivi);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovoSenzaUtenti(java.lang.Boolean,
	 *      java.lang.Long, it.ibm.red.business.dto.NodoOrganigrammaDTO).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovoSenzaUtenti(final Boolean isEntrata, final Long idUfficioUtente,
			final NodoOrganigrammaDTO nodeToOpen, final boolean consideraDisattivi) {
		return getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovo(isEntrata, idUfficioUtente, nodeToOpen, null,
				OrganigrammaVisualizzazioneCompetenzaIngressoEnum.SENZA_UTENTI, consideraDisattivi);
	}

	private List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovo(final Boolean isEntrata, final Long idUfficioUtente,
			final NodoOrganigrammaDTO nodeToOpen, final String indirizzoEmail, final OrganigrammaVisualizzazioneCompetenzaIngressoEnum tipoVisualizzazione,final boolean consideraDisattivi) {
		List<NodoOrganigrammaDTO> output = null;
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			final String visualizzazione = getVisualizzazione(false, true);

			boolean onlyIspettorato = true;
			if (nodeToOpen.getIdAOO().toString().equals(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IDAOO_RL))) {
				onlyIspettorato = false;
			}
			boolean forzaVisualizzazioneUtenti = false;
			switch (tipoVisualizzazione) {
			case SENZA_UTENTI:
				forzaVisualizzazioneUtenti = false;
				break;
			case CON_UTENTI:
				forzaVisualizzazioneUtenti = true;
				break;
			case STANDARD:
			default:
				forzaVisualizzazioneUtenti = isEntrata && nodoDAO.getFlagAssegnazioneDiretta(nodeToOpen.getIdNodo(), indirizzoEmail, connection);
				break;
			}

			final boolean visualizzaUtenti = forzaVisualizzazioneUtenti;
			output = filtroGenerico(forzaVisualizzazioneUtenti, idUfficioUtente, nodeToOpen, visualizzaUtenti, null, visualizzazione, connection, onlyIspettorato, isEntrata,
					indirizzoEmail, false, consideraDisattivi);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getFigliAlberoCompletoNoUtentiFromRoot(java.lang.Long,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoCompletoNoUtentiFromRoot(final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final String visualizzazione = getVisualizzazione(true, true);
			final boolean visualizzaUtenti = false;
			return filtroGenerico(idUfficioUtente, nodeToOpen, visualizzaUtenti, null, visualizzazione, connection, false, false);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RAGGIUNGIMENTO_CONNECTION_MSG + e.getMessage(), e);
			rollbackConnection(connection);
			throw new RedException(ERROR_RAGGIUNGIMENTO_CONNECTION_MSG + e.getMessage());
		} finally {
			closeConnection(connection);
		}

	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getFigliAlberoCompletoNoUtenti(java.lang.Long,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoCompletoNoUtenti(final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final String visualizzazione = getVisualizzazione(false, false);
			final boolean visualizzaUtenti = false;
			return filtroGenerico(idUfficioUtente, nodeToOpen, visualizzaUtenti, null, visualizzazione, connection, false, false);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RAGGIUNGIMENTO_CONNECTION_MSG + e.getMessage(), e);
			rollbackConnection(connection);
			throw new RedException(ERROR_RAGGIUNGIMENTO_CONNECTION_MSG + e.getMessage());
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getFigliAlberoCompletoUtenti(java.lang.Long,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoCompletoUtenti(final Long idUfficioUtente, final boolean consideraDisattivi, final NodoOrganigrammaDTO nodeToOpen) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final String visualizzazione = getVisualizzazione(false, false);
			final boolean visualizzaUtenti = true;
			return filtroGenerico(idUfficioUtente, nodeToOpen, visualizzaUtenti, null, visualizzazione, connection, false, consideraDisattivi);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RAGGIUNGIMENTO_CONNECTION_MSG + e.getMessage(), e);
			rollbackConnection(connection);
			throw new RedException(ERROR_RAGGIUNGIMENTO_CONNECTION_MSG + e.getMessage());
		} finally {
			closeConnection(connection);
		}

	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getFigliAlberoAssegnatarioPerFirma(java.lang.Long,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerFirma(final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen) {
		Connection connection = null;
		List<NodoOrganigrammaDTO> figli = new ArrayList<>();
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			figli = getFigliAlberoAssegnatarioPerFirma(idUfficioUtente, nodeToOpen, connection);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
		return figli;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getFigliAlberoAssegnatarioPerSigla(java.lang.Long,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerSigla(final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen) {
		Connection connection = null;
		List<NodoOrganigrammaDTO> figli = new ArrayList<>();
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			figli = getFigliAlberoAssegnatarioPerSigla(idUfficioUtente, nodeToOpen, connection);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
		return figli;
	}

	/**
	 * @see it.ibm.red.business.service.IOrganigrammaSRV#getFigliAlberoAssegnatarioPerFirma(java.lang.Long,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO, java.sql.Connection).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerFirma(final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen, final Connection connection) {

		final String visualizzazione = getVisualizzazione(true, false);

		final boolean visualizzaUtenti = true;
		return filtroGenerico(idUfficioUtente, nodeToOpen, visualizzaUtenti, PermessiEnum.FIRMA, visualizzazione, connection, true, false);

	}

	/**
	 * @see it.ibm.red.business.service.IOrganigrammaSRV#getFigliAlberoAssegnatarioPerSigla(java.lang.Long,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO, java.sql.Connection).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerSigla(final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen, final Connection connection) {

		final String visualizzazione = getVisualizzazione(true, false);

		final boolean visualizzaUtenti = true;
		return filtroGenerico(idUfficioUtente, nodeToOpen, visualizzaUtenti, PermessiEnum.SIGLA, visualizzazione, connection, true, false);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getFigliAlberoAssegnatarioPerStornaAUtente(java.lang.Long,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerStornaAUtente(final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen) {
		Connection connection = null;
		List<NodoOrganigrammaDTO> figli = new ArrayList<>();
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			figli = organigrammaDAO.getUtentiByNodo(false, nodeToOpen.getIdNodo(), connection);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
		return figli;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getFigliAlberoAssegnatarioPerAssegna(java.lang.Long,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerAssegna(final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen) {
		Connection connection = null;
		List<NodoOrganigrammaDTO> figliAlberoAssegnatario = new ArrayList<>();
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final String visualizzazione = getVisualizzazione(true, true);
			final boolean visualizzaUtenti = true;
			figliAlberoAssegnatario = filtroGenerico(idUfficioUtente, nodeToOpen, visualizzaUtenti, null, visualizzazione, connection, true, false);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
		return figliAlberoAssegnatario;
	}

	/* FILTRI START *******************************************************/

	/**
	 * Restituisce solo gli ispettorati e quello corrente.
	 *
	 * @param idUfficioUtente
	 * @param list
	 * @param connection
	 * @return filtered
	 */
	private List<NodoOrganigrammaDTO> filtraPerIspettoratiEapriSoloIspettoratoCorrente(final Long idUfficioUtente, final List<NodoOrganigrammaDTO> list,
			final Connection connection) {
		final List<NodoOrganigrammaDTO> filtered = new ArrayList<>();
		final Integer idNodoIspettorato = nodoDAO.getSegreteriaDirezione(idUfficioUtente.intValue(), connection);

		for (final NodoOrganigrammaDTO n : list) {
			if (n.getTipoStruttura() == TipoStrutturaNodoEnum.ISPETTORATO) {
				if (n.getIdNodo().intValue() != idNodoIspettorato.intValue()) {
					n.setFigli(0);
				}
				filtered.add(n);
			}
		}

		return filtered;
	}

	/**
	 * @param idNodoCorrente
	 * @param visualizzaUtenti
	 * @param idNodoIspettorato
	 * @param list
	 * @return result
	 */
	private static List<NodoOrganigrammaDTO> rendiEspandibileSoloIspettoratoCorrenteEdIsuoiFigli(final Integer idNodoCorrente, final boolean visualizzaUtenti,
			final Integer idNodoIspettorato, final List<NodoOrganigrammaDTO> list) {
		final List<NodoOrganigrammaDTO> result = new ArrayList<>(list);

		for (final NodoOrganigrammaDTO n : result) {
			if (n.getTipoStruttura() == TipoStrutturaNodoEnum.ISPETTORATO) {
				if (n.getIdNodo().intValue() != idNodoIspettorato.intValue() && idNodoCorrente.intValue() != idNodoIspettorato.intValue()) {
					n.setFigli(0);
				} else if (visualizzaUtenti && idNodoCorrente.intValue() == idNodoIspettorato.intValue()) {
					n.setUtentiVisible(true);
				}
			}
		}
		return result;
	}

	private List<NodoOrganigrammaDTO> filtroGenerico(final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen, final boolean visualizzaUtenti,
			final PermessiEnum permesso, final String visualizzazione, final Connection connection, final Boolean onlyIspettorato, final boolean consideraDisattivi) {
		return filtroGenerico(false, idUfficioUtente, nodeToOpen, visualizzaUtenti, permesso, visualizzazione, connection, onlyIspettorato, false, null, false, consideraDisattivi);
	}

	/**
	 * Trova i figli del nodo passato come radice.
	 * 
	 * @param idUfficioUtente
	 *            id dell'ufficio del nodo radice
	 * @param nodeToOpen
	 *            nodo radice
	 * @param visualizzaUtenti
	 *            opzione di filtro per gli utenti
	 * @param permesso
	 *            Filtri per le credenziali degli utenti
	 * @param visualizzazione
	 *            Discerne tra le varie entity pescate dal db
	 * @param connection
	 *            connessione
	 * @param onlyIspettorato
	 *            filtra tra uffici e utenti che appartengono allo stesso ufficio
	 * @return
	 */
	private List<NodoOrganigrammaDTO> filtroGenerico(final boolean forzaVisualizzazioneUtenti, final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen,
			final boolean visualizzaUtenti, final PermessiEnum permesso, final String visualizzazione, final Connection connection, final boolean onlyIspettorato,
			final boolean isCompetenzaEntrata, final String indirizzoEmail, final boolean primoLivello, boolean consideraDisattivi) {

		try {

			final List<NodoOrganigrammaDTO> listResult = new ArrayList<>();
			final Long idNodeToOpen = nodeToOpen.getIdNodo(); // nodeToOpen non e' mai null
			final Long idAOO = nodeToOpen.getIdAOO();

			// preondo la lista dei nodi figli (tutti)
			final List<NodoOrganigrammaDTO> list = organigrammaDAO.getFigliByNodo(idNodeToOpen, idAOO, visualizzazione, idUfficioUtente, connection);

			List<NodoOrganigrammaDTO> listEspandibile = null;

			if (onlyIspettorato) {
				// calcolo il nodo dell'ispettorato a partire dall'ufficio dell'utente
				final Integer idNodoIspettorato = nodoDAO.getSegreteriaDirezione(idUfficioUtente.intValue(), connection);

				// resetto tutti i figli a meno ché non sia il nodo ispettorato dell'utente o
				// non sia il nodo dell'utente o non sia un suo figlio
				listEspandibile = rendiEspandibileSoloIspettoratoCorrenteEdIsuoiFigli(idNodeToOpen.intValue(), visualizzaUtenti, idNodoIspettorato, list);
			} else {
				listEspandibile = list;
			}

			if (visualizzaUtenti) { // mostro gli utenti se il nodo puo' mostrare gli utenti
				if ((!forzaVisualizzazioneUtenti && (nodeToOpen.getIdNodo().longValue() == idUfficioUtente.longValue() || nodeToOpen.isUtentiVisible()))
						|| (Boolean.TRUE.equals(forzaVisualizzazioneUtenti))) {

					if (forzaVisualizzazioneUtenti) {
						nodeToOpen.setUtentiVisible(true);
					}

					for (final NodoOrganigrammaDTO nodoOrganigrammaDTO : listEspandibile) {
						nodoOrganigrammaDTO.setUtentiVisible(true);
					}
				}

				// aggiungo gli utenti per il nodo selezionato solo se e' il nodo dell'utente o
				// uno dei suoi figli
				if (nodeToOpen.isUtentiVisible() && !primoLivello) {
					final List<NodoOrganigrammaDTO> utentiDaFiltrare = organigrammaDAO.getUtentiByNodo(consideraDisattivi,idNodeToOpen, connection);
					List<NodoOrganigrammaDTO> utentiDaAggiungere = null;
					if (permesso != null) { // se il permesso e' valorizzato filtro gli utenti che hanno il permesso in
											// input (es: FIRMA, SIGLA...)
						utentiDaAggiungere = new ArrayList<>();
						for (final NodoOrganigrammaDTO nodoDaVerificare : utentiDaFiltrare) {
							if (PermessiUtils.hasPermesso(nodoDaVerificare.getPermessi(), permesso)) {
								utentiDaAggiungere.add(nodoDaVerificare);
							}
						}
					} else { // altrimenti li voglio tutti (es: per competenza)
						utentiDaAggiungere = utentiDaFiltrare;
					}

					listResult.addAll(utentiDaAggiungere);
					nodeToOpen.setFigli(nodeToOpen.getFigli() + listResult.size());
				}

				// se è una competenza in ingresso, rendi espandibile i nodi relativi all'utente
				// ed eventualmente quelli che hanno assegnazione diretta
				for (final NodoOrganigrammaDTO nodoOrganigrammaDTO : listEspandibile) {
					if (forzaVisualizzazioneUtenti || (nodoOrganigrammaDTO.getIdNodo().intValue() == idUfficioUtente.intValue())
							|| (isCompetenzaEntrata && nodoDAO.getFlagAssegnazioneDiretta(nodoOrganigrammaDTO.getIdNodo(), indirizzoEmail, connection))
							|| nodoDAO.getAlberaturaUfficio(nodoOrganigrammaDTO.getIdNodo(), connection).contains(idUfficioUtente)) {
						nodoOrganigrammaDTO.setUtentiVisible(true);
					}
				}
			}

			listResult.addAll(listEspandibile);

			return listResult;
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}
	}

	/**
	 * Trova i figli del nodo passato come radice.
	 * 
	 * @param idUfficioUtente
	 *            id dell'ufficio del nodo radice
	 * @param nodeToOpen
	 *            nodo radice
	 * @param visualizzaUtenti
	 *            opzione di filtro per gli utenti
	 * @param permesso
	 *            Filtri per le credenziali degli utenti
	 * @param visualizzazione
	 *            Discerne tra le varie entity pescate dal db
	 * @param connection
	 *            connessione
	 * @param onlyIspettorato
	 *            filtra tra uffici e utenti che appartengono allo stesso ufficio
	 * @return
	 */
	private List<NodoOrganigrammaDTO> filtroGenerico(final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen, final boolean visualizzaUtenti,
			final PermessiEnum permesso, final String visualizzazione, final Connection connection, final Boolean onlyIspettorato,
			final Boolean visualizzaUtentiSoloIspettoratoCorrente, final Boolean mostraFigliRoot) {

		try {

			final List<NodoOrganigrammaDTO> listResult = new ArrayList<>();
			final Long idNodeToOpen = nodeToOpen.getIdNodo(); // nodeToOpen non e' mai null
			final Long idAOO = nodeToOpen.getIdAOO();

			// preondo la lista dei nodi figli (tutti)
			final List<NodoOrganigrammaDTO> list = organigrammaDAO.getFigliByNodo(idNodeToOpen, idAOO, visualizzazione, idUfficioUtente, connection);

			List<NodoOrganigrammaDTO> listEspandibile = null;

			if (Boolean.TRUE.equals(onlyIspettorato)) {
				// calcolo il nodo dell'ispettorato a partire dall'ufficio dell'utente
				final Integer idNodoIspettorato = nodoDAO.getSegreteriaDirezione(idUfficioUtente.intValue(), connection);

				// resetto tutti i figli a meno ché non sia il nodo ispettorato dell'utente o
				// non sia il nodo dell'utente o non sia un suo figlio
				listEspandibile = rendiEspandibileSoloIspettoratoCorrenteEdIsuoiFigli(idNodeToOpen.intValue(), visualizzaUtenti, idNodoIspettorato, list);
			} else {
				listEspandibile = list;
			}

			if (visualizzaUtenti) { // mostro gli utenti se il nodo puo' mostrare gli utenti
				if ((!visualizzaUtentiSoloIspettoratoCorrente && (mostraFigliRoot || nodeToOpen.getIdNodoPadre() != null))
						|| (nodeToOpen.getIdNodo().longValue() == idUfficioUtente.longValue() || nodeToOpen.isUtentiVisible())) {
					nodeToOpen.setUtentiVisible(true);
				}
				if (!Boolean.TRUE.equals(visualizzaUtentiSoloIspettoratoCorrente) || (nodeToOpen.getIdNodo().longValue() == idUfficioUtente.longValue() || nodeToOpen.isUtentiVisible())) {
					for (final NodoOrganigrammaDTO nodoOrganigrammaDTO : listEspandibile) {
						nodoOrganigrammaDTO.setUtentiVisible(true);
					}
				}

				// aggiungo gli utenti per il nodo selezionato solo se e' il nodo dell'utente o
				// uno dei suoi figli
				if (nodeToOpen.isUtentiVisible()) {
					final List<NodoOrganigrammaDTO> utentiDaFiltrare = organigrammaDAO.getUtentiByNodo(false,idNodeToOpen, connection);
					List<NodoOrganigrammaDTO> utentiDaAggiungere = null;
					if (permesso != null) { // se il permesso e' valorizzato filtro gli utenti che hanno il permesso in
											// input (es: FIRMA, SIGLA...)
						utentiDaAggiungere = new ArrayList<>();
						for (final NodoOrganigrammaDTO nodoDaVerificare : utentiDaFiltrare) {
							if (PermessiUtils.hasPermesso(nodoDaVerificare.getPermessi(), permesso)) {
								utentiDaAggiungere.add(nodoDaVerificare);
							}
						}
					} else { // altrimenti li voglio tutti (es: per competenza)
						utentiDaAggiungere = utentiDaFiltrare;
					}

					listResult.addAll(utentiDaAggiungere);
					nodeToOpen.setFigli(nodeToOpen.getFigli() + listResult.size());
				}

				for (final NodoOrganigrammaDTO nodoOrganigrammaDTO : listEspandibile) {
					if (nodoOrganigrammaDTO.getIdNodo().intValue() == idUfficioUtente.intValue()) {
						nodoOrganigrammaDTO.setUtentiVisible(true);
						break;
					}
				}
			}

			listResult.addAll(listEspandibile);

			return listResult;
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}
	}

	/* FILTRI END *********************************************************/

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getSottoNodi(java.lang.Long,
	 *      java.lang.Long).
	 */
	@Override
	public List<UfficioDTO> getSottoNodi(final Long idUfficio, final Long idAOO) {
		List<UfficioDTO> output = new ArrayList<>();
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			output = organigrammaDAO.getSottoNodi(idUfficio, idAOO, con);
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getFigliAlberoAssegnatarioPerConoscenza(java.lang.Long,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO, boolean).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerConoscenza(final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen, final boolean primoLivello) {
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			return getFigliAlberoAssegnatarioPerConoscenza(idUfficioUtente, nodeToOpen, primoLivello, con);
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}

	private List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerConoscenza(final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen, final boolean primoLivello,
			final Connection connection) {

		final String visualizzazione = getVisualizzazione(false, true);

		boolean forzaVisualizzaUtenti = false;
		if (nodeToOpen.getIdAOO().toString().equals(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IDAOO_RL))) {
			forzaVisualizzaUtenti = true;
		}
		return filtroGenerico(forzaVisualizzaUtenti, idUfficioUtente, nodeToOpen, true, null, visualizzazione, connection, true, false, null, primoLivello, false);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getFigliAlberoUtentiAssegnatariFilteredByPermesso(java.lang.Long,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO,
	 *      it.ibm.red.business.enums.ResponsesRedEnum).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoUtentiAssegnatariFilteredByPermesso(final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen,
			final ResponsesRedEnum responseEnum) {
		switch (responseEnum) {
		case INVIO_IN_FIRMA:
			return getFigliAlberoAssegnatarioPerFirma(idUfficioUtente, nodeToOpen);
		case INVIO_IN_SIGLA:
			return getFigliAlberoAssegnatarioPerSigla(idUfficioUtente, nodeToOpen);
		case INVIO_IN_VISTO:

			final boolean visualizzaUtentiTrue = true;
			final boolean onlyIspettoratoFalse = false;
			return getFigliAlberoAssegnatarioPerVisto(idUfficioUtente, nodeToOpen, visualizzaUtentiTrue, onlyIspettoratoFalse);
		default:
			return new ArrayList<>();
		}
	}

	private List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerVisto(final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen, final boolean visualizzaUtenti,
			final boolean onlyIspettorato) {
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			final PermessiEnum permesso = PermessiEnum.VISTO;
			final String visualizzazione = getVisualizzazione(true, false);
			return filtroGenerico(idUfficioUtente, nodeToOpen, visualizzaUtenti, permesso, visualizzazione, con, onlyIspettorato, false);
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Genera un albero che mostra solo gli uffici ma non gli utente e nel quale
	 * solo l'ispettorato dell'utente può essere espanso.
	 * 
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @return
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerRichiestaVisto(final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen) {
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			final PermessiEnum permesso = PermessiEnum.VISTO;
			final String visualizzazione = getVisualizzazione(true, false);
			final boolean visualizzaUtenti = false;
			final boolean onlyIspettorato = true;
			return filtroGenerico(idUfficioUtente, nodeToOpen, visualizzaUtenti, permesso, visualizzazione, con, onlyIspettorato, false);
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Genera un albero che mostra solo gli uffici ma non gli utente e nel quale
	 * solo l'ispettorato dell'utente può essere espanso.
	 * 
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @param filtraIspettorati
	 * @return
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerRichiestaVisto(final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen,
			final boolean filtraIspettorati) {
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			final PermessiEnum permesso = null;
			final String visualizzazione = getVisualizzazione(true, false);
			final boolean visualizzaUtenti = false;
			final boolean onlyIspettorato = true;

			final List<NodoOrganigrammaDTO> rawList = filtroGenerico(idUfficioUtente, nodeToOpen, visualizzaUtenti, permesso, visualizzazione, con, onlyIspettorato, false);

			if (filtraIspettorati) {
				return filtraPerIspettoratiEapriSoloIspettoratoCorrente(idUfficioUtente, rawList, con);
			}
			return rawList;
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getNodoUCP(java.lang.Long).
	 */
	@Override
	public NodoOrganigrammaDTO getNodoUCP(final Long idUfficioUtente) {
		Connection con = null;
		NodoOrganigrammaDTO ucp = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			final Long idUCP = nodoDAO.getNodoUCP(idUfficioUtente, con);
			final Nodo n = nodoDAO.getNodo(idUCP, con);
			ucp = new NodoOrganigrammaDTO(n.getIdNodo(), n.getAoo().getIdAoo(), n.getCodiceNodo(), n.getDescrizione(), null, null, null, null, null, null,
					TipologiaNodoEnum.UFFICIO);
			return ucp;
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getFigliUfficioAssegnatarioPerVisto(java.lang.Long,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliUfficioAssegnatarioPerVisto(final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen) {
		Connection con = null;
		final List<NodoOrganigrammaDTO> output = new ArrayList<>();
		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final List<NodoOrganigrammaDTO> utentiDaFiltrare = organigrammaDAO.getUtentiByNodo(false,idUfficioUtente, con);
			for (final NodoOrganigrammaDTO nodoDaVerificare : utentiDaFiltrare) {
				if (PermessiUtils.hasPermesso(nodoDaVerificare.getPermessi(), PermessiEnum.VISTO)) {
					output.add(nodoDaVerificare);
				}
			}

			return output;
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IOrganigrammaSRV#filterByTipoStruttura(java.util.List,
	 *      java.lang.Boolean, it.ibm.red.business.enums.TipoStrutturaNodoEnum[]).
	 */
	@Override
	public List<NodoOrganigrammaDTO> filterByTipoStruttura(final List<NodoOrganigrammaDTO> allNodi, final Boolean inclusivo, final TipoStrutturaNodoEnum... enumArgs) {
		if (enumArgs.length == 0) {
			return Boolean.TRUE.equals(inclusivo) ? new ArrayList<>(allNodi) : new ArrayList<>(0);
		}

		final EnumSet<TipoStrutturaNodoEnum> tipiStruttura = EnumSet.of(enumArgs[0], enumArgs);
		final Predicate<NodoOrganigrammaDTO> predicate = Boolean.TRUE.equals(inclusivo) ? c -> tipiStruttura.contains(c.getTipoStruttura())
				: c -> !tipiStruttura.contains(c.getTipoStruttura());
		return allNodi.stream().filter(predicate).collect(Collectors.toList());
	}

	/**
	 * @param nodoDiPartenza
	 *            - id nodo di partenza per calcolare la struttura
	 * @param idUfficioUtente
	 * @return
	 */
	@Override
	public List<NodoOrganigrammaDTO> getOrgForScrivania(final NodoOrganigrammaDTO nodoDiPartenza, final Long idUfficioUtente) {
		List<NodoOrganigrammaDTO> output = new ArrayList<>();
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			output = organigrammaDAO.getFigliByNodo(nodoDiPartenza.getIdNodo(), nodoDiPartenza.getIdAOO(), null, idUfficioUtente, connection);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getTreeSelezioneDelegante(it.ibm.red.business.dto.NodoOrganigrammaDTO).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getTreeSelezioneDelegante(final NodoOrganigrammaDTO nodoDiPartenza) {
		List<NodoOrganigrammaDTO> output = new ArrayList<>();
		Connection connection = null;

		try {

			connection = setupConnection(getDataSource().getConnection(), false);
			output = organigrammaDAO.getNodiConDirigente(nodoDiPartenza.getIdNodo(), nodoDiPartenza.getIdAOO(), null, connection);

		} catch (final Exception e) {
			LOGGER.error(ERROR_CARICAMENTO_TREE_DELEGANTE_MSG, e);
			throw new RedException(ERROR_CARICAMENTO_TREE_DELEGANTE_MSG, e);
		} finally {
			closeConnection(connection);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getDirigenteFromNodo(it.ibm.red.business.dto.NodoOrganigrammaDTO).
	 */
	@Override
	public NodoOrganigrammaDTO getDirigenteFromNodo(final NodoOrganigrammaDTO nodoDiPartenza) {
		NodoOrganigrammaDTO output = null;
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			output = organigrammaDAO.getUtenteDirigenteByNodo(nodoDiPartenza.getIdNodo(), connection);

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del dirigente per l'ufficio selezionato", e);
			throw new RedException("Errore durante il recupero del dirigente per l'ufficio selezionato", e);
		} finally {
			closeConnection(connection);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getUtentiFromNodo(it.ibm.red.business.dto.NodoOrganigrammaDTO,
	 *      boolean).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getUtentiFromNodo(final NodoOrganigrammaDTO nodoDiPartenza, final boolean withDirigente) {
		List<NodoOrganigrammaDTO> output = new ArrayList<>();
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			output = organigrammaDAO.getUtentiByNodo(nodoDiPartenza.getIdNodo(), withDirigente, connection);

		} catch (final Exception e) {
			LOGGER.error(ERROR_CARICAMENTO_TREE_DELEGANTE_MSG, e);
			throw new RedException(ERROR_CARICAMENTO_TREE_DELEGANTE_MSG, e);
		} finally {
			closeConnection(connection);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getFigliAlberoAssegnatarioOperazione(java.lang.Long,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO,
	 *      it.ibm.red.business.enums.PermessiEnum).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioOperazione(final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen, final PermessiEnum permesso) {

		return getFigliAlberoAssegnatarioOperazione(idUfficioUtente, nodeToOpen, permesso, true);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getFigliAlberoAssegnatarioOperazione(java.lang.Long,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO,
	 *      it.ibm.red.business.enums.PermessiEnum, boolean).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioOperazione(final Long idUfficioUtente, final NodoOrganigrammaDTO nodeToOpen, final PermessiEnum permesso,
			final boolean conUtenti) {
		Connection connection = null;
		List<NodoOrganigrammaDTO> figliAlberoAssegnatarioOperazione = new ArrayList<>();
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final String visualizzazione = getVisualizzazione(true, false);
			figliAlberoAssegnatarioOperazione = filtroGenerico(idUfficioUtente, nodeToOpen, conUtenti, permesso, visualizzazione, connection, true, false);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
		return figliAlberoAssegnatarioOperazione;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV#getAlberaturaBottomUp(java.lang.Long,
	 *      java.lang.Long).
	 */
	@Override
	public List<Nodo> getAlberaturaBottomUp(final Long idAoo, final Long idUfficio) {
		Connection connection = null;
		List<Nodo> alberatura = new ArrayList<>();
		
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			alberatura = nodoDAO.getAlberaturaBottomUp(idAoo, idUfficio, connection);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
		return alberatura;
	}
}