package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ILookupDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class LookupDAO.
 *
 * @author CPIERASC
 * 
 * 	Dao per la gestione delle lookup.
 */
@Repository
public class LookupDAO extends AbstractDAO implements ILookupDAO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 4515810066803534710L;
	
	/**
	 * Messaggio errore recupero descrizioni.
	 */
	private static final String ERROR_RECUPERO_DESCRIZIONI_MSG = "Errore durante il recupero delle descrizioni dei tipi documento";

	/**
	 * Gets the desc tipo documento.
	 *
	 * @param id the id
	 * @param connection the connection
	 * @return the desc tipo documento
	 */
	@Override
	public final String getDescTipoDocumento(final Long id, final Connection connection) {
		String querySQL = "SELECT t.descrizione FROM TipologiaDocumento t WHERE t.idtipologiadocumento = " + id;
		return getDescription(id, connection, querySQL);
	}

	@Override
	public final Map<Long, String> getDescTipoDocumento(final Integer idAoo, final Connection connection) {
		Map<Long, String> output = new HashMap<>();
		String querySQL = "SELECT t.idtipologiadocumento as id, t.descrizione as description FROM TipologiaDocumento t WHERE IDAOO = " + sanitize(idAoo);
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			while (rs.next()) {
				output.put(rs.getLong("id"), rs.getString("description"));
			}
		} catch (SQLException e) {
			throw new RedException(ERROR_RECUPERO_DESCRIZIONI_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	/**
	 * Metodo per il recupero della descrizione da id.
	 * 
	 * @param id			identificativo
	 * @param connection	connessione
	 * @param querySQL		query da eseguire
	 * @return				descrizione
	 */
	private static String getDescription(final Long id, final Connection connection, final String querySQL) {
		String output = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			if (rs.next()) {
				output = rs.getString("descrizione");
			}
		} catch (SQLException e) {
			throw new RedException("Errore durante il recupero della descrizione con id=" + id, e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	/**
	 * Gets the iter approvativo.
	 *
	 * @param id the id
	 * @param connection the connection
	 * @return the iter approvativo
	 */
	@Override
	public final String getIterApprovativo(final String id, final Connection connection) {
		String output = null;
		if (!StringUtils.isNullOrEmpty(id)) {
			Long idLong = Long.parseLong(id);
			String querySQL = "SELECT i.* FROM Iterapprovativo i WHERE i.IDITERAPPROVATIVO = " + sanitize(idLong);
			output = getDescription(idLong, connection, querySQL);
		}
		return output;
	}

	/**
	 * Gets the approvazioni.
	 *
	 * @param documentTitle the document title
	 * @param idAoo the id aoo
	 * @param connection the connection
	 * @return the approvazioni
	 */
	@Override
	public final Collection<String> getApprovazioni(final String documentTitle, final Long idAoo, final Connection connection) {
		Collection<String> output = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String querySQL = "SELECT ta.descrizione||' ' || n.descrizione FROM approvazione a, tipoapprovazione ta, "
					+ "utente u, nodo n WHERE a.iddocumento = ? AND a.idaoo = ? AND n.idnodo = a.idnodo AND u.idutente = a.idutente "
					+ "AND ta.idtipoapprovazione = a.idtipoapprovazione ORDER BY a.dataapprovazione ASC";
			ps = connection.prepareStatement(querySQL);
			ps.setString(1, documentTitle);
			ps.setLong(2, idAoo);
			rs = ps.executeQuery();
			while (rs.next()) {
				output.add(rs.getString(1));
			}
		} catch (SQLException e) {
			throw new RedException("Errore durante il recupero dele approvazioni ", e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}
	
	/**
	 * Gets the desc categoria documento.
	 *
	 * @param idCatDoc the id cat doc
	 * @param connection the connection
	 * @return the desc categoria documento
	 */
	@Override
	public final String getDescCategoriaDocumento(final Long idCatDoc, final Connection connection) {
		if (idCatDoc != null) {
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				String querySQL = "SELECT cd.* FROM CATEGORIADOCUMENTO cd WHERE cd.IDCATEGORIADOCUMENTO = " + sanitize(idCatDoc);
				ps = connection.prepareStatement(querySQL);
				rs = ps.executeQuery();
				if (rs.next()) {
					return rs.getString("DESCRIZIONE");
				}
			} catch (SQLException e) {
				throw new RedException("Errore durante il recupero della CategoriaDocumento con id=" + idCatDoc, e);
			} finally {
				closeStatement(ps, rs);
			}
		}
		return null;
	}

	/**
	 * Gets the tipo assegnazione desc by id.
	 *
	 * @param assegnazioneId the assegnazione id
	 * @param iterManuale the iter manuale
	 * @param connection the connection
	 * @return the tipo assegnazione desc by id
	 */
	@Override
	public final String getTipoAssegnazioneDescById(final Integer assegnazioneId, final Boolean iterManuale, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String appendIterManuale = "";
		if (iterManuale != null) {
			String tmp = "0";
			if (iterManuale) {
				tmp = "1";
			}
			appendIterManuale = " AND t.ITERMANUALE = " + tmp;
		}
		StringBuilder querySQL = new StringBuilder("SELECT t.descrizione FROM TIPOASSEGNAZIONE t WHERE t.IDTIPOASSEGNAZIONE = " + sanitize(assegnazioneId));
		if (!StringUtils.isNullOrEmpty(appendIterManuale)) {
			querySQL.append(appendIterManuale);
		}
		try {
			ps = connection.prepareStatement(querySQL.toString());
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString("descrizione");
			}
		} catch (SQLException e) {
			throw new RedException("Errore durante il recupero della descrizione del tipo di assegnazione con id=" + assegnazioneId, e);
		} finally {
			closeStatement(ps, rs);
		}
		return null;
	}
	
	
	@Override
	public final Map<Long, String> getDescUffici(final Collection<Long> ids, final Connection connection) {
		Map<Long, String> output = new HashMap<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String querySQL = "SELECT n.idnodo as id, n.descrizione as description FROM nodo n";
			
			if (!CollectionUtils.isEmpty(ids)) {
				querySQL += " WHERE (" + StringUtils.createInCondition("n.idnodo", ids.iterator(), false) + ")";
			}
			
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				output.put(rs.getLong("id"), rs.getString("description"));
			}
		} catch (SQLException e) {
			throw new RedException(ERROR_RECUPERO_DESCRIZIONI_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return output;
	}
	
	
	@Override
	public final Map<Long, String> getDescUtenti(final Collection<Long> ids, final Connection connection) {
		Map<Long, String> output = new HashMap<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String querySQL = "SELECT u.idutente as id, u.nome as nome, u.cognome as cognome FROM utente u";
			
			if (!CollectionUtils.isEmpty(ids)) {
				querySQL += " WHERE (" + StringUtils.createInCondition("u.idutente", ids.iterator(), false) + ")";
			}
			
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				output.put(rs.getLong("id"), rs.getString("nome") + " " + rs.getString("cognome"));
			}
		} catch (SQLException e) {
			throw new RedException(ERROR_RECUPERO_DESCRIZIONI_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return output;
	}
	
	
	@Override
	public final List<Long> getIdsTipologiaDocumentoByDescrizione(final Collection<String> descrizioni, final Long idAoo, final Connection connection) {
		List<Long> output = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		String querySQL = "SELECT t.idtipologiadocumento as id FROM tipologiadocumento t WHERE t.idaoo = ?";
		
		if (!CollectionUtils.isEmpty(descrizioni)) {
			querySQL += " AND (" + StringUtils.createLikeLowerCaseCondition("t.descrizione", descrizioni.iterator()) + ")";
		}
		
		try {
			ps = connection.prepareStatement(querySQL);
			ps.setLong(1, idAoo);
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				output.add(rs.getLong("id"));
			}
		} catch (SQLException e) {
			throw new RedException(ERROR_RECUPERO_DESCRIZIONI_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return output;
	}
	
}
