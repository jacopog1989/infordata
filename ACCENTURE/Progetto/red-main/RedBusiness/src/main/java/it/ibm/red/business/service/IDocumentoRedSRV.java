package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV;

/**
 * Interface del servizio di gestione documenti RED.
 */
public interface IDocumentoRedSRV extends IDocumentoRedFacadeSRV {
	
	/**
	 * @param annoProtocollo
	 * @param numeroProtocollo
	 * @param tipoCategoria
	 * @param idAoo
	 * @param fceh
	 * @param connection
	 * @return
	 */
	Document verificaProtocollo(int annoProtocollo, int numeroProtocollo, TipoCategoriaEnum tipoCategoria, Long idAoo,	IFilenetCEHelper fceh, Connection connection);

	/**
	 * Il metodo restituisce il wobnumber del workflow principale (rilevandone la presenza) .
	 * 
	 * @param idAOO
	 * @param documentTitle
	 * @param idClientAoo
	 * @param fpeh
	 * @param connection
	 * @return
	 */
	String getWobNumberWorkflowPrincipale(Integer idAOO, String documentTitle, String idClientAoo, FilenetPEHelper fpeh, Connection connection);
	
	
	/**
	 * @param documentTitle
	 * @param idAoo
	 * @param fceh
	 * @return
	 */
	List<Document> getElencoVersioniDocumento(String documentTitle, Long idAoo, IFilenetCEHelper fceh);

	/**
	 * @param documentTitle
	 * @param numeroVersione
	 * @param idAoo
	 * @param fceh
	 * @return
	 */
	Document getVersioneDocumento(String documentTitle, Integer numeroVersione, Long idAoo, IFilenetCEHelper fceh);

	/**
	 * @param documentTitle
	 * @param utente
	 * @param wobNumberIN
	 * @param documentClassName
	 * @param isRicercaRiservato
	 * @param connection
	 * @return
	 */
	DetailDocumentRedDTO getDocumentDetail(String documentTitle, UtenteDTO utente, String wobNumberIN, String documentClassName, boolean isRicercaRiservato, Connection connection);

	/**
	 * @param documentTitle
	 * @param utente
	 * @param wobNumberIN
	 * @param documentClassName
	 * @param con
	 * @return
	 */
	DetailDocumentRedDTO getDocumentDetail(String documentTitle, UtenteDTO utente, String wobNumberIN, String documentClassName, Connection con);

}