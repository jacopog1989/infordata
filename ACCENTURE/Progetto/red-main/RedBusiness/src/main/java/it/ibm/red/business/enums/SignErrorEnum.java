package it.ibm.red.business.enums;

/**
 * The Enum SignErrorEnum.
 *
 * @author CPIERASC
 * 
 *         Enum codici di errore in fase di firma.
 */
/**
 * @author m.crescentini
 *
 */
public enum SignErrorEnum {
	
	/**
	 * Otp non valido.
	 */
	INVALID_OTP_FIRMA_COD_ERROR(1, "OTP inserito non valido. "),
	
	/**
	 * Allegati in formato non pdf.
	 */
	DOCUMENTO_ALLEGATI_NO_PDF_FIRMA_COD_ERROR(2, "Sono presenti allegati non in formato PDF. "),
	
	/**
	 * Aggiornamento della versione dell'allegato firmato.
	 */
	UPDATE_VERSION_ALLEGATI_COD_ERROR(3, "Si è verificato un errore durante l'aggiornamento della versione di uno degli allegati firmati. "),
	
	/**
	 * Errore durante la preparazione del documento per la firma.
	 */
	PREPARE_SIGN_DOCUMENT_COD_ERROR(4, "Si è verificato un errore durante la procedura di preparazione del documento per la firma. "),
	
	/**
	 * Errore durante la connessione a PKBOX.
	 */
	PKBOX_COD_ERROR(5, "Si è verificato un errore durante la connessione con l'infrastruttura PkBOX. "),
	
	/**
	 * Errore in fase di recupero del certificato dell'utente.
	 */
	PKBOX_CERTIFICATE_COD_ERROR(6, "Si è verificato un errore con il recupero del certificato dell'utente sull'infrastruttura PkBOX. "),
	
	/**
	 * Errore durante la firma degli allegati.
	 */
	PKBOX_SIGN_COD_ERROR(7, "Si è verificato un errore durante la firma del documento o di uno degli allegati tramite l'infrastruttura PkBOX. "),
	
	/**
	 * Timeout sessione di firma.
	 */
	PKBOX_AUTH_TOKEN_EXPIRED_COD_ERROR(8, "La sessione di firma remota tramite l'infrastruttura PkBOX ha superato il limite massimo. "),
	
	/**
	 * Certificato non trovato sull'infrastruttura PkBOX.
	 */
	PKBOX_NO_CERTIFICATE_FOUND_COD_ERROR(9, "Non è stato trovato nessun certificato remoto con il quale firmare i documenti sull'infrastruttura PkBOX. Contattare l'assistenza."),
	
	/**
	 * Documento/allegato non in formato PDF.
	 */
	DOCUMENTO_NO_PDF_FIRMA_COD_ERROR(10, "Impossibile firmare in formato PaDES un documento non in formato PDF. "),
	
	/**
	 * PIN inserito non valido.
	 */
	INVALID_PIN_FIRMA_COD_ERROR(11, "PIN inserito non valido."),
	
	/**
	 * OTP o PIN inseriti non validi.
	 */
	INVALID_PIN_OTP_FIRMA_COD_ERROR(12, "PIN o OTP inseriti non validi."),
	
	/**
	 * PIN locked.
	 */
	INVALID_PIN_LOCKED_FIRMA_COD_ERROR(13, "PIN risulta in stato 'LOCKED'."),
	
	/**
	 * Campi firma vuoti mancanti.
	 */
	DOCUMENTO_PRINCIPALE_NO_PADES_VISIBILE_SIGN_FIELDS_COD_ERROR(14, "Impossibile firmare in formato PaDES visibile un documento senza campi firma vuoti."),
	
	/**
	 * Glifo non configurato.
	 */
	DOCUMENTO_PRINCIPALE_NO_PADES_VISIBILE_GLIFO_COD_ERROR(15, "Impossibile firmare in formato PaDES visibile: Il glifo non è stato configurato per l'utente."),
	
	/**
	 * Errore durante l'esecuzione delle operazioni da eseguire successivamente alla firma dei content.
	 */
	POST_FIRMA_CONTENTS_ASIGN_COD_ERROR(16, "Si è verificato un errore durante l'esecuzione delle operazioni successive alla firma."),
	
	/**
	 * Errore durante lo step di gestione della registrazione ausiliaria.
	 */
	PROTOCOLLAZIONE_STEP_ASIGN_COD_ERROR(17, "Si è verificato un errore durante lo step di protocollazione."),
	
	/**
	 * Errore durante lo step di comunicazione con NPS per la protocollazione della registrazione ausiliaria.
	 */
	REG_AUX_PROTOCOLLAZIONE_STEP_ASIGN_COD_ERROR(18, "Si è verificato un errore durante lo step di gestione della registrazione ausiliaria, in fase di protocollazione."),
	
	/**
	 * Errore durante lo step di rigenerazione content della registrazione ausiliaria.
	 */
	REG_AUX_RIGENERAZIONE_CONTENT_STEP_ASIGN_COD_ERROR(19, "Si è verificato un errore durante lo step di gestione della registrazione ausiliaria, in fase di rigenerazione content."),
	
	/**
	 * Errore durante lo step di stampigliatura.
	 */
	STAMPIGLIATURA_STEP_ASIGN_COD_ERROR(20, "Si è verificato un errore durante lo step di stampigliatura."),
	
	/**
	 * Errore durante lo step di aggiornamento dei metadati.
	 */
	AGGIORNAMENTO_METADATI_STEP_ASIGN_COD_ERROR(21, "Si è verificato un errore durante lo step di aggiornamento dei metadati."),
	
	/**
	 * Errore durante lo step di gestione degli allacci.
	 */
	GESTIONE_ALLACCI_STEP_ASIGN_COD_ERROR(22, "Si è verificato un errore durante lo step di gestione degli allacci."),
	
	/**
	 * Errore durante lo step di avanzamento dei processi.
	 */
	AVANZAMENTO_PROCESSI_STEP_ASIGN_COD_ERROR(23, "Si è verificato un errore durante lo step di avanzamento dei processi."),
	
	/**
	 * Errore durante lo step di invio mail.
	 */
	INVIO_EMAIL_STEP_ASIGN_COD_ERROR(24, "Sono presenti errori durante l'invio dell'email della notifica a seguito della firma."),
	
	/**
	 * Errore durante lo step di allineamento di NPS.
	 */
	ALLINEAMENTO_NPS_STEP_ASIGN_COD_ERROR(25, "Si è verificato un errore durante lo step di allineamento di NPS."),
	
	/**
	 * Documento già firmato (e.g. delegato al libro firma che firma un documento prima che il dirigente provi a firmarlo senza ricaricare la coda)
	 */
	DOCUMENTO_GIA_FIRMATO_ASIGN_COD_ERROR(26, "Il documento risulta già firmato."),
	
	/**
	 * Operazione non consentita.
	 */
	FORBIDDEN_OPERATION_COD_ERROR(403, "Operazione non consentita."),
	
	/**
	 * Errore generico.
	 */
	GENERIC_COD_ERROR(500, "Si è verificato un errore generico durante l'esecuzione dell'operazione richiesta. Contattare l'assistenza."); 
	
	

	/**
	 * Codice errore.
	 */
	private int codError;
	
	/**
	 * Messaggio errore.
	 */
	private String message;
	

	/**
	 * Costruttore.
	 * 
	 * @param inCodError	codice errore
	 * @param inMessage		messaggio errore
	 */
	SignErrorEnum(final int inCodError, final String inMessage) {
		codError = inCodError;
		message = inMessage;
	}
	
	/**
	 * Getter codice errore.
	 * 
	 * @return	codice errore
	 */
	public int getCodError() {
		return codError;
	}
	
	/**
	 * Getter messaggio errore.
	 * 
	 * @return	messaggio errore
	 */
	public String getMessage() {
		return message;
	}
}
