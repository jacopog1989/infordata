package it.ibm.red.business.dto;

/**
 * DTO che definisce le informazioni ottenute dal form di ricerca capitolo spesa.
 */
public class RicercaCapitoloSpesaDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -3965601704923167981L;

	/**
	 * Codice.
	 */
	private String codice;
	

	/**
	 * Descrizione.
	 */
	private String descrizione;
	

	/**
	 * Aoo.
	 */
	private final long idAoo;

	/**
	 * Costruttore.
	 * @param idAoo
	 */
	public RicercaCapitoloSpesaDTO(final long idAoo) {
		super();
		this.idAoo = idAoo;
	}

	/**
	 * @return the codice
	 */
	public String getCodice() {
		return codice;
	}

	/**
	 * @param codice the codice to set
	 */
	public void setCodice(final String codice) {
		this.codice = codice;
	}

	/**
	 * @return the descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * @param descrizione the descrizione to set
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * Restituisce l'id dell'Area Organizzativa Omogenea.
	 * @return id AOO
	 */
	public long getIdAoo() {
		return idAoo;
	}
}
