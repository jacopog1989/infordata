package it.ibm.red.business.dto;

import java.util.ArrayList;
import java.util.List;

import it.ibm.red.business.enums.SalvaDocumentoErroreEnum;

/**
 * The Class EsitoSalvaDocumentoDTO.
 *
 * @author m.crescentini
 * 
 * 	DTO per modellare l'esito dell'operazione di salvataggio di un documento.
 */
public class EsitoSalvaDocumentoDTO extends AbstractDTO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 6658184222285421418L;

	/**
	 * Flag operazione riuscita.
	 */
	private boolean esitoOk;

	/**
	 * Document title del documento salvato.
	 */
	private String documentTitle;

	/**
	 * GUID del documento salvato.
	 */
	private String guid;

	/**
	 * Wob Number del processo avviato.
	 */
	private String wobNumber;

	/**
	 * Numero documento del documento salvato.
	 */
	private Integer numeroDocumento;

	/**
	 * Lista di errori.
	 */
	private final List<SalvaDocumentoErroreEnum> errori;

	/**
	 * Note.
	 */
	private String note;
	
    /**
	 * Flag assegnazione competenza automatica.
	 */
	private boolean assegnazioneCompetenzaAutomatica;
	
    /**
	 * Flag assegnazione competenza indiretta.
	 */
	private boolean assegnazioneCompetenzaIndiretta;


	/**
	 * Costruttore.
	 */
	public EsitoSalvaDocumentoDTO() {
		super();
		esitoOk = false;
		errori = new ArrayList<>();
	}
	
	/**
	 * Getter.
	 * 
	 * @return	esito
	 */
	public final boolean isEsitoOk() {
		return esitoOk;
	}
	
	/**
	 * Setter.
	 * 
	 * @param inEsitoOk	esitoOk
	 */
	public final void setEsitoOk(final boolean inEsitoOk) {
		this.esitoOk = inEsitoOk;
	}

	/**
	 * @return the errori
	 */
	public List<SalvaDocumentoErroreEnum> getErrori() {
		return errori;
	}
	
	/**
	 * @return the documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * @return the guid
	 */
	public String getGuid() {
		return guid;
	}
	
	/**
	 * @param documentTitle the documentTitle to set
	 */
	public void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}
	
	/**
	 * @param guid the guid to set
	 */
	public void setGuid(final String guid) {
		this.guid = guid;
	}
	
	/**
	 * @return
	 */
	public String getWobNumber() {
		return wobNumber;
	}

	/**
	 * @return
	 */
	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}
	
	/**
	 * @param wobNumber
	 */
	public void setWobNumber(final String wobNumber) {
		this.wobNumber = wobNumber;
	}

	/**
	 * @param numeroDocumento
	 */
	public void setNumeroDocumento(final Integer numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * Getter.
	 * 
	 * @return	note
	 */
	public final String getNote() {
		return note;
	}
	
	/**
	 * Setter.
	 * 
	 * @param inNote	note
	 */
	public final void setNote(final String inNote) {
		this.note = inNote;
	}

	/**
	 * @return
	 */
	public boolean isAssegnazioneCompetenzaAutomatica() {
		return assegnazioneCompetenzaAutomatica;
	}

	/**
	 * @param assegnazioneCompetenzaAutomatica
	 */
	public void setAssegnazioneCompetenzaAutomatica(final boolean assegnazioneCompetenzaAutomatica) {
		this.assegnazioneCompetenzaAutomatica = assegnazioneCompetenzaAutomatica;
	}

	/**
	 * @return
	 */
	public boolean isAssegnazioneCompetenzaIndiretta() {
		return assegnazioneCompetenzaIndiretta;
	}

	/**
	 * @param assegnazioneCompetenzaIndiretta
	 */
	public void setAssegnazioneCompetenzaIndiretta(final boolean assegnazioneCompetenzaIndiretta) {
		this.assegnazioneCompetenzaIndiretta = assegnazioneCompetenzaIndiretta;
	}
	
}