package it.ibm.red.business.service.concrete;

import java.sql.Connection; 
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.service.INodoSRV;

/**
 * Service che si occupa della gestione dei Nodi.
 */
@Service
public class NodoSRV extends AbstractService implements INodoSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -8035242534710297886L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NodoSRV.class.getName());

	/**
	 * DAO.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * @see it.ibm.red.business.service.facade.INodoFacadeSRV#getNodo(java.lang.Long).
	 */
	@Override
	public Nodo getNodo(final Long idUfficio) {
		Connection conn = null;
		Nodo ufficio = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			
			ufficio = nodoDAO.getNodo(idUfficio, conn);
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'ufficio " + idUfficio, e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
		return ufficio;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.INodoFacadeSRV#getDescrizioneByIdNodo(java.lang.Integer).
	 */
	@Override
	public HashMap<Integer, String> getDescrizioneByIdNodo(final Integer idAoo) {
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(),false); 
			return nodoDAO.getDescrizioneByIdNodo(idAoo, conn); 
		} catch(Exception ex) {
			LOGGER.error("Errore nel recupero della mappa di id nodo descrizione nodo : "+ex);
			throw new RedException("Errore nel recupero della mappa di id nodo descrizione nodo : "+ex);
		} finally {
			closeConnection(conn);
		} 
	}
}
