package it.ibm.red.business.dto;

/**
 * @author APerquoti
 */
public class TandemDTO extends AbstractDTO {

	private static final long serialVersionUID = -3921488760681636997L;

    /**
	 * Dati Process Engine.
	 */
	private PEDocumentoDTO datiPE;

    /**
	 * Dati Content Engine.
	 */
	private MasterDocumentRedDTO datiCE;

	/**
	 * Costruttore vuoto.
	 */
	public TandemDTO() {
		super();
	}

	/**
	 * Costruttore PE.
	 * @param tpe
	 */
	public TandemDTO(final PEDocumentoDTO tpe) {
		setDatiPE(tpe);
	}

	/**
	 * Costruttore CE.
	 * @param tce
	 */
	public TandemDTO(final MasterDocumentRedDTO tce) {
		setDatiCE(tce);
	}

	/**
	 * @return tandemPE
	 */
	public PEDocumentoDTO getDatiPE() {
		return datiPE;
	}

	/**
	 * Restituisce il documento recuperto dal PE.
	 * @param tandemPE
	 */
	public void setDatiPE(final PEDocumentoDTO inDatiPE) {
		this.datiPE = inDatiPE;
	}

	/**
	 * Restituisce il documento recuperto dal CE.
	 * @return datiCE
	 */
	public MasterDocumentRedDTO getDatiCE() {
		return datiCE;
	}

	/**
	 * Imposta il documento recuperto dal CE.
	 * @param datiCE
	 */
	public void setDatiCE(final MasterDocumentRedDTO inDatiCE) {
		this.datiCE = inDatiCE;
	}
}
