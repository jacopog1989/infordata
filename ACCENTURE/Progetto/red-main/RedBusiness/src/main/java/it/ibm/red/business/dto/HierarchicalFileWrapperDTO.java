package it.ibm.red.business.dto;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * Rappresenta un nodo di un albero di NamedStreamDTO.
 */
public class HierarchicalFileWrapperDTO extends AbstractDTO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -9119216426479473526L;

	/**
	 * separatore nome.
	 */
	public static final String BRANCH_NAME_SEPARATOR = ":";
	/**
	 * Separatore padre-figlio.
	 */
	public static final String FATHER_SON_SEPARATOR = "|";

	/**
	 * Id incrementale dei nodi sullo stesso ramo. In caso di radice il valore e'
	 * null.
	 * 
	 */
	private Integer identificativoRamo;

	/**
	 * Concatenazione separata da File.pathSeparator di identifivativoRamo
	 * Path.separator file.getName dei parent di questo oggetto. Null se l'oggetto è
	 * radice.
	 * 
	 * il path inizia con il guid del documento principale senza identificativoRamo.
	 */
	private String path;

	/**
	 * L'attuale contenuto
	 */
	private NamedStreamDTO file;

	// Verificare se serve il parent

	/**
	 * Gli allegati all'attuale contenuto
	 */
	private List<HierarchicalFileWrapperDTO> sons;

	/**
	 * Allegato che può essere selezionato dall'utente Un allegato selezionabile non
	 * deve avere figli Questo flag indica quindi se il HFWDTO è una foglia
	 * dell'albero.
	 */
	private boolean attachmentSelectable;

	/**
	 * Identifica qualora l'allegato selezionato sia il principale.
	 * 
	 */
	private boolean principalSelectable;

	/**
	 * Flag protetto.
	 */
	private Boolean isProtected;

	/**
	 * Relative node id.
	 * 
	 * @deprecated
	 */
	@Deprecated
	private String relativeNodeId;

	/**
	 * Flag archivio.
	 */
	private boolean archivio;

	/**
	 * Flag non sbustato.
	 */
	private boolean fileNonSbustato;

	/**
	 * Flag firmato.
	 */
	private boolean firmato;

	/**
	 * Flag firma valida.
	 */
	private Boolean firmaValida;

	/**
	 * Flag firma valida.
	 */
	private int contentSize;

	/**
	 * Recupera il file.
	 * 
	 * @return file
	 */
	public NamedStreamDTO getFile() {
		return file;
	}

	/**
	 * Imposta il file.
	 * 
	 * @param file
	 */
	public void setFile(final NamedStreamDTO file) {
		this.file = file;
	}

	/**
	 * Recupera i file gerarchicamente.
	 * 
	 * @return lista gerarchica dei file
	 */
	public List<HierarchicalFileWrapperDTO> getSons() {
		return sons;
	}

	/**
	 * Imposta i file gerarchicamente.
	 * 
	 * @param sons
	 *            lista gerarchica dei file
	 */
	public void setSons(final List<HierarchicalFileWrapperDTO> sons) {
		this.sons = sons;
	}

	/**
	 * Verifica che questo allegato può essere selezionato dall'utente Un allegato
	 * selezionabile non deve avere figli Un allegato selezionabile è una foglia
	 * dell'albero.
	 */
	public boolean isAttachmentSelectable() {
		return attachmentSelectable;
	}

	/**
	 * Imposta l'attributo attachmentSelectable.
	 * 
	 * @param attachmentSelectable
	 */
	public void setAttachmentSelectable(final boolean attachmentSelectable) {
		this.attachmentSelectable = attachmentSelectable;
	}

	/**
	 * Recupera l'attributo principalSelectable.
	 * 
	 * @return principalSelectable
	 */
	public boolean isPrincipalSelectable() {
		return principalSelectable;
	}

	/**
	 * Imposta l'attributo principalSelectable.
	 * 
	 * @param principalSelectable
	 */
	public void setPrincipalSelectable(final boolean principalSelectable) {
		this.principalSelectable = principalSelectable;
	}

	private boolean hasSons() {
		return this.sons != null && !this.sons.isEmpty();
	}

	/**
	 * Un hierarchicalWrapper è ben formato quando viene flaggato come foglia e non
	 * ha figli oppure viceversa.
	 * 
	 */
	public boolean isValid() {
		// Si tratta dello xor
		return this.isAttachmentSelectable() && !this.hasSons() || !this.isAttachmentSelectable() && this.hasSons();
	}

	/**
	 * Ritorna una stringa che permette di individuare un allegato a partire dalla
	 * mail che lo contiene.
	 * 
	 * mailGuid/indiceRamo_allegatoGuid/indiceRamo_nomeAllegato/
	 * 
	 * @return
	 */
	public String getNodeId() {
		final String pathTranche = StringUtils.isNotBlank(path) ? StringUtils.join(path, FATHER_SON_SEPARATOR) : "";
		final String nodo = identificativoRamo == null ? file.getName()
				: StringUtils.join(identificativoRamo, BRANCH_NAME_SEPARATOR, file.getName());

		return StringUtils.join(pathTranche, nodo);
	}

	/**
	 * Imposta l'id del nodo per mezzo del path e dell'indice del ramo.
	 * 
	 * @param inPath
	 *            path
	 * @param indiceRamo
	 *            indice del ramo
	 */
	public void setNodeId(final String inPath, final Integer indiceRamo) {
		this.path = inPath;
		this.identificativoRamo = indiceRamo;
	}

	/**
	 * Recupera l'id del nodo relativo.
	 * 
	 * @return id del nodo relativo
	 */
	public String getRelativeNodeId() {
		return relativeNodeId;
	}

	/**
	 * Imposta l'id del nodo relativo.
	 * 
	 * @param relativeNodeId
	 *            id del nodo relativo
	 */
	public void setRelativeNodeId(final String relativeNodeId) {
		this.relativeNodeId = relativeNodeId;
	}

	/**
	 * Recupera l'identificativo del ramo.
	 * 
	 * @return identificativo del ramo
	 */
	public Integer getIdentificativoRamo() {
		return identificativoRamo;
	}

	/**
	 * @return the isProtected
	 */
	public Boolean getIsProtected() {
		return isProtected;
	}

	/**
	 * @param isProtected
	 *            the isProtected to set
	 */
	public void setIsProtected(final Boolean isProtected) {
		this.isProtected = isProtected;
	}

	/**
	 * Recupera l'archivio.
	 * 
	 * @return archivio
	 */
	public boolean getArchivio() {
		return archivio;
	}

	/**
	 * Imposta l'archivio.
	 * 
	 * @param archivio
	 */
	public void setArchivio(final boolean archivio) {
		this.archivio = archivio;
	}

	/**
	 * Recupera il file non sbustato.
	 * 
	 * @return file non sbustato
	 */
	public boolean getFileNonSbustato() {
		return fileNonSbustato;
	}

	/**
	 * Imposta il file non sbustato.
	 * 
	 * @param fileNonSbustato
	 *            file non sbustato
	 */
	public void setFileNonSbustato(final boolean fileNonSbustato) {
		this.fileNonSbustato = fileNonSbustato;
	}

	/**
	 * Recupera l'attributo firmato.
	 * 
	 * @return firmato
	 */
	public boolean isFirmato() {
		return firmato;
	}

	/**
	 * Imposta l'attributo firmato.
	 * 
	 * @param firmato
	 */
	public void setFirmato(final boolean firmato) {
		this.firmato = firmato;
	}

	/**
	 * Recupera l'attributo firmaValida.
	 * 
	 * @return firmaValida
	 */
	public Boolean getFirmaValida() {
		return firmaValida;
	}

	/**
	 * Imposta l'attributo firmaValida.
	 * 
	 * @param firmaValida
	 */
	public void setFirmaValida(final Boolean firmaValida) {
		this.firmaValida = firmaValida;
	}

	/**
	 * Restituisce la dimensione del content.
	 * 
	 * @return dimensione content.
	 */
	public int getContentSize() {
		return contentSize;
	}

	/**
	 * Imposta la dimensione del content.
	 * 
	 * @param contentSize
	 *            Dimensione content da impostare.
	 */
	public void setContentSize(int contentSize) {
		this.contentSize = contentSize;
	}

}
