package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.FascicoloRedFnDTO;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.service.facade.IFascicoloFacadeSRV;

/**
 * The Interface IFascicoloSRV.
 *
 * @author CPIERASC
 * 
 *         Interfaccia servizio fascicolo
 */
public interface IFascicoloSRV extends IFascicoloFacadeSRV {
	
	/**
	 * Inserisce il documento nel fascicolo.
	 * @param idFascicolo
	 * @param idDocumento
	 * @param tipoFascicolo
	 * @param idAoo
	 * @param fceh
	 * @param con
	 */
	void fascicola(String idFascicolo, int idDocumento, int tipoFascicolo, Long idAoo, IFilenetCEHelper fceh, Connection con);

	/**
	 * Inserisce il fascicolo su filenet.
	 * @param fascicolo
	 * @param utente
	 * @param fceh
	 * @param con
	 */
	void inserisciSuFilenet(FascicoloRedFnDTO fascicolo, UtenteDTO utente, IFilenetCEHelper fceh, Connection con);

	/**
	 * Costruisce il fascicolo filenet.
	 * @param idFascicolo
	 * @param anno
	 * @param numero
	 * @param descrizione
	 * @param indiceClassificazione
	 * @param idAoo
	 * @return fascicolo filenet
	 */
	FascicoloRedFnDTO costruisciFascicoloRedFn(String idFascicolo, int anno, String numero, String descrizione, String indiceClassificazione, Long idAoo);
	
	/**
	 * Costruisce il fascicolo filenet.
	 * @param idFascicolo
	 * @param metadatiFascicolo
	 * @param anno
	 * @param numero
	 * @param descrizione
	 * @param indiceClassificazione
	 * @param idAoo
	 * @return fascicolo filenet
	 */
	FascicoloRedFnDTO costruisciFascicoloRedFn(String idFascicolo, Map<String, Object> metadatiFascicolo, int anno, String numero, String descrizione, 
			String indiceClassificazione, Long idAoo);

	/**
	 * Aggiunge il fascicolo automatico.
	 * @param idDocumento
	 * @param utente
	 * @param indiceClassificazione
	 * @param descrizioneFascicolo
	 * @param metadatiFascicolo
	 * @param securityFascicolo
	 * @param fascicoliRedFn
	 * @param fceh
	 * @param con
	 * @return fascicolo filenet
	 */
	FascicoloRedFnDTO aggiungiFascicoloAutomatico(int idDocumento, UtenteDTO utente, String indiceClassificazione, String descrizioneFascicolo, 
			Map<String, Object> metadatiFascicolo, ListSecurityDTO securityFascicolo,	List<FascicoloRedFnDTO> fascicoliRedFn, IFilenetCEHelper fceh, 
			Connection con);
	
	/**
	 * Crea il fascicolo.
	 * @param utente
	 * @param indiceClassificazione
	 * @param descrizioneFascicolo
	 * @param metadatiFascicolo
	 * @param securityFascicolo
	 * @param fascicoliRedFn
	 * @param fceh
	 * @param con
	 * @return fascicolo filenet
	 */
	FascicoloRedFnDTO creaFascicolo(UtenteDTO utente, String indiceClassificazione, String descrizioneFascicolo, Map<String, Object> metadatiFascicolo, 
			ListSecurityDTO securityFascicolo,	List<FascicoloRedFnDTO> fascicoliRedFn, IFilenetCEHelper fceh, Connection con);

	/**
	 * Aggiorna lo stato.
	 * @param idFascicolo
	 * @param statoFascicolo
	 * @param idAoo
	 * @param fceh
	 */
	void aggiornaStato(String idFascicolo, FilenetStatoFascicoloEnum statoFascicolo, Long idAoo, IFilenetCEHelper fceh);
	
	/**
	 * Aggiorna i metadati.
	 * @param idFascicolo
	 * @param statoFascicolo
	 * @param oggetto
	 * @param idAoo
	 * @param fceh
	 */
	void aggiornaMetadati(String idFascicolo, FilenetStatoFascicoloEnum statoFascicolo, String oggetto, Long idAoo, IFilenetCEHelper fceh);
	
	/**
	 * Aggiorna il fascicolo procedimentale.
	 * @param docInput
	 * @param newFascicoloProcedimentale
	 * @param currentFascicoloProcedimentale
	 * @param utente
	 * @param idAoo
	 * @param fceh
	 * @param fpeh
	 * @param con
	 */
	void aggiornaFascicoloProcedimentale(SalvaDocumentoRedDTO docInput, FascicoloDTO newFascicoloProcedimentale, FascicoloDTO currentFascicoloProcedimentale, 
			UtenteDTO utente, Long idAoo, IFilenetCEHelper fceh, FilenetPEHelper fpeh, Connection con);

	/**
	 * Sposta il documento di fascicolo.
	 * @param utente
	 * @param idDocumento
	 * @param newFascicoloId
	 * @param currentFascicoloId
	 * @param idAoo
	 * @param idProtocollo
	 * @param indiceClassificazioneFascicoloProcedimentale
	 * @param fceh
	 * @param fpeh
	 * @param con
	 * @return true o false
	 */
	boolean spostaDocumento(UtenteDTO utente, String idDocumento, String newFascicoloId, String currentFascicoloId, Long idAoo, String idProtocollo, 
			String indiceClassificazioneFascicoloProcedimentale, IFilenetCEHelper fceh, FilenetPEHelper fpeh, Connection con);
	
	/**
	 * Copia il documento in un nuovo fascicolo.
	 * @param utente
	 * @param idDocumento
	 * @param newFascicoloId
	 * @param currentFascicoloId
	 * @param idAoo
	 * @param fceh
	 * @param con
	 * @return true o false
	 */
	boolean copiaDocumento(UtenteDTO utente, String idDocumento, String newFascicoloId, String currentFascicoloId, Long idAoo, IFilenetCEHelper fceh, Connection con);

	/**
	 * Inserisce il documento nel fascicolo su filenet.
	 * @param idFascicolo
	 * @param idDocumento
	 * @param idAoo
	 * @param fceh
	 * @return true o false
	 */
	boolean fascicolaSuFilenet(String idFascicolo, int idDocumento, Long idAoo, IFilenetCEHelper fceh);

	/**
	 * Recupero il fascicolo documentale data una lista di fascicoli in input (i fascicoli del documento)
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @param fascicoliDTO
	 * @param con
	 * @return
	 */
	FascicoloDTO getFascicoloProcedimentale(String documentTitle, Integer idAoo, List<FascicoloDTO> fascicoliDTO, Connection con);

	/**
	 * ATTENZIONE: utilizzare solo per le DSR.
	 * 
	 * Il metodo normalmente utilizzato per recuperare il fascicolo procedimentale non funziona per le dsr.
	 * 
	 * Secondo una analisi svolta su nsd questo è vero anche per altre code applicative.
	 * Il fascicolo procedimentale  viene recuperato in un atributo del PE delle dsr che si chiama idFascicolo
	 * 
	 * Per ragioni di prestazione noi recuperiamo il fascicolo procedimentale di una dsr desumendonolo dall'indice di classificazione.
	 * Infatti il fascicolo procedimentale di una dsr è l'unico fascicolo che non corrispondendo a un decreto dichiarazione è catalogato
	 * 
	 * @param fascicoliList
	 *  Lista dei fascicoli di una dsr, tra i quali è presente il procedimentale
	 * @return
	 * 	Il fascicolo procedimentale
	 */
	FascicoloDTO getFascicoloProcedimentaleDSR(List<FascicoloDTO> fascicoliList);
	
	/**
	 * Elimina su fileNet l'associazione (denominata fascicola) tra idFascicolo e documento
	 * 
	 * @param idFascicolo id del fascicolo in cui si trova il documento da disassociare
	 * @param documentTitle id del docuemento che si trova nel fascicolo
	 * @param idAoo idAoo dell'utente che esegue l'operazione
	 * @param fceh 
	 * @throws FileNetException nel caso si verifichino problemi con fileNet
	 */
	void disassociaDocumentoFascicolo(String idFascicolo, String documentTitle, Long idAoo, IFilenetCEHelper fceh);

	/**
	 * Recupera il fascicolo facendo uso di un fceh precedentemente creato.
	 * 
	 * @param nomeFascicolo
	 * @param utente
	 * @param fceh
	 * @return
	 */
	FascicoloDTO getFascicoloByNomeFascicolo(String nomeFascicolo, UtenteDTO utente, IFilenetCEHelper fceh);
	
	/**
	 * Recupera i fascicoli che contengono il documento indicato in ingresso 
	 * 
	 * @param doc - il documento del fascicolo da cercare
	 * @param utente 
	 * 
	 * @return la lista dei fascicoli che contengono il documento
	 */
	List<FascicoloDTO> recuperaFascicoliDelDocumento(MasterDocumentRedDTO doc, UtenteDTO utente);

	/**
	 * Aggiorna l'indice di classificazione su NPS. 
	 * 
	 * @return 
	 */
	void aggiornaIndiceClassFascicoloNps(String infoProtocollo, String idFascicolo, IFilenetCEHelper fceh, Connection con, UtenteDTO utente,
			String indiceClassificazioneDescrizione);

	/**
	 * Elimina il fascicolo in input
	 * 
	 * @param idFascicolo
	 * @param idAoo
	 * @param fceh
	 */
	boolean eliminaFascicolo(String idFascicolo, Long idAoo, IFilenetCEHelper fceh);

	/**
	 * Prima recupero una lista di fascicoli che poi seleziono tramite una query del fascicolo automatico.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @param fceh
	 * @param con
	 * @return
	 */
	FascicoloDTO getFascicoloProcedimentale(String documentTitle, Integer idAoo, IFilenetCEHelper fceh, Connection con);

	/**
	 * @param docFilenet
	 * @param idAoo
	 * @param fceh
	 * @param con
	 * @return
	 */
	FascicoloDTO getFascicoloProcedimentale(Document docFilenet, Integer idAoo, IFilenetCEHelper fceh, Connection con);

	/**
	 * @param documentTitle
	 * @param idAoo
	 * @param utente
	 * @param con
	 * @return
	 */
	FascicoloDTO getFascicoloProcedimentale(String documentTitle, Integer idAoo, UtenteDTO utente, Connection con);

	/**
	 * @param documentTitle
	 * @param idAoo
	 * @param utente
	 * @param fceh
	 * @return
	 */
	FascicoloDTO getFascicoloProcedimentale(String documentTitle, Integer idAoo, UtenteDTO utente, IFilenetCEHelper fceh);
	
	/**
	 * @param documentTitle
	 * @param idAoo
	 * @param utente
	 * @param aooFilenet
	 * @return
	 */
	FascicoloDTO getFascicoloProcedimentale(String documentTitle, Integer idAoo, UtenteDTO utente, AooFilenet aooFilenet);

}