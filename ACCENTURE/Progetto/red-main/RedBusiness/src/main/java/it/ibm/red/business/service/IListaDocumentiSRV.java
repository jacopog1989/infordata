package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;

import filenet.vw.api.VWQueueQuery;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.PEDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV;

/**
 * Interface del servizio di gestione lista documenti.
 */
public interface IListaDocumentiSRV extends IListaDocumentiFacadeSRV {
	
	/**
	 * Metodo per cercare un DocuemntTitle su tutte le code FileNet disponibili.
	 * 
	 * @param utente
	 * @param documentTitle
	 * @return
	 */
	Collection<VWQueueQuery> getQueueFNbyDocumentTitle(UtenteDTO utente, String documentTitle, String classeDocumentale, Boolean isDocEntrata);
	
	/**
	 * Restituisce una data coda cercando per DocumentTitle.
	 * I documenti restituiti sono solo quelli con la data scadenza valorizzata.
	 * 
	 * @param queue
	 * @param utente
	 * @param documentTitlesToFilterBy
	 * @param docConScadenza
	 * @param connection
	 * @return
	 */
	VWQueueQuery getQueueFilenetConScadenza(DocumentQueueEnum queue, UtenteDTO utente,	Collection<String> documentTitlesToFilterBy, Connection connection);
	
	/**
	 * Restituisce una data coda cercando per DocumentTitle.
	 * 
	 * @param queue
	 * @param utente
	 * @param documentTitlesToFilterBy
	 * @param docConScadenza
	 * @param connection
	 * @return
	 */
	VWQueueQuery getQueueFilenet(DocumentQueueEnum queue, UtenteDTO utente, Collection<String> documentTitlesToFilterBy, Connection connection);

	/**
	 * Ottiene i workflow FEPA.
	 * @param queue
	 * @param utente
	 * @return documenti PE
	 */
	Collection<PEDocumentoDTO> getFepaWorflows(DocumentQueueEnum queue, UtenteDTO utente);

	/**
	 * Ottiene una coda Filenet.
	 * @param queue
	 * @param utente
	 * @param documentTitlesToFilterBy
	 * @param fpeh
	 * @param connection
	 * @return VWQueueQuery
	 */
	VWQueueQuery getQueueFilenet(DocumentQueueEnum queue, UtenteDTO utente, Collection<String> documentTitlesToFilterBy, FilenetPEHelper fpeh, Connection connection);

	/**
	 * Ottiene una coda Filenet con scadenza.
	 * @param queue
	 * @param utente
	 * @param documentTitlesToFilterBy
	 * @param fpeh
	 * @param connection
	 * @return VWQueueQuery
	 */
	VWQueueQuery getQueueFilenetConScadenza(DocumentQueueEnum queue, UtenteDTO utente, Collection<String> documentTitlesToFilterBy, FilenetPEHelper fpeh, 
			Connection connection);

	/**
	 * Ottiene una coda Filenet per paginazione.
	 * @param queue
	 * @param utente
	 * @param documentTitlesToFilterBy
	 * @param connection
	 * @return VWQueueQuery
	 */
	VWQueueQuery getQueueFilenetPerPaginazione(DocumentQueueEnum queue, UtenteDTO utente, Collection<String> documentTitlesToFilterBy, Connection connection);

	/**
	 * Ottiene la lista di masters dal CE tramite i document titles.
	 * @param documentTitlesToFilterBy
	 * @param utente
	 * @param connection
	 * @return lista di documenti masters
	 */
	Collection<MasterDocumentRedDTO> getMastersFromCEByDocumentTitles(Collection<String> documentTitlesToFilterBy, UtenteDTO utente, Connection connection);

	/**
	 * Ottiene la lista di masters filtrati dal CE.
	 * @param documentTitlesToFilterBy
	 * @param filterString
	 * @param utente
	 * @param connection
	 * @return lista di documenti masters filtrati
	 */
	Collection<MasterDocumentRedDTO> getFilteredMastersFromCE(Collection<String> documentTitlesToFilterBy, String filterString, UtenteDTO utente, Connection connection);

	/**
	 * Verifica se il documento è in coda.
	 * @param utente
	 * @param documentTitle
	 * @param queue
	 * @param con
	 * @return true o false
	 */
	boolean isDocumentoInCoda(UtenteDTO utente, String documentTitle, DocumentQueueEnum queue, Connection con);
	 
	/**
	 * Ottiene una coda Filenet widget.
	 * @param queue
	 * @param utente
	 * @param documentTitlesToFilterBy
	 * @param docConScadenza
	 * @param listUfficio
	 * @param inApprovazioneDirigente
	 * @param fpeh
	 * @param connection
	 * @return VWQueueQuery
	 */
	VWQueueQuery getQueueFilenetWidget(DocumentQueueEnum queue, UtenteDTO utente, Collection<String> documentTitlesToFilterBy, Boolean docConScadenza, List<Nodo> listUfficio, 
			boolean inApprovazioneDirigente, FilenetPEHelper fpeh, Connection connection);

	/**
	 * Metodo utilizzato per l'assegnazione delle icone di STATO e FLUSSO al DocumentoMaster.
	 * 
	 * @param master
	 * @param idAoo
	 * @param connection
	 */
	void updateIcone(MasterDocumentRedDTO master, UtenteDTO utente, Connection connection);
	
}