package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Collection;

import it.ibm.red.business.dto.TimerRichiesteIntegrazioniDTO;

/**
 * @author d.ventimiglia
 * 
 * 	Interfaccia del DAO del Timer per la gestione dei documenti non lavorati
 *
 */
public interface ITimerRichiesteIntegrazioniDAO extends Serializable {
	
	/**
	 * @param idDocumento	id documento in ingresso come allaccio principale
	 * @param utente		id utente destinatario	
	 * @param timestamp		data invio documento
	 * @param flagLavorato	flag stato lavorazione: 0 non lavorato, 1 lavorato, -1 documento senza wf
	 * @param conn			
	 */
	void registraTimestamp(long idDocumento, long idNodo, long idUtente, Timestamp timestamp, int flagLavorato, Connection conn);
	
	/**
	 * @param idAoo
	 * @param con
	 * @return	prossimo item da lavorare
	 */
	Collection<TimerRichiesteIntegrazioniDTO> getAndLockNextDaLavorare(Connection conn);
	
	/**
	 * @param flagLavorato	flag stato lavorazione: 0 da lavorare, 1 in elaborazione, 2 lavorato, 3 obsoleto , 4 errore
	 * @param conn
	 */
	void aggiornaFlagLavorato(long idTimer, int flagLavorato, Connection conn);
	
	/**
	 * @param idTD	id tipo documento
	 * @param idTP  id tipo procedimento
	 * @param conn
	 * @return tempo configurato per la coppia td/tp in ingresso
	 */
	long getTimerConfigurazione(long idTD, long idTP, Connection conn);
}
