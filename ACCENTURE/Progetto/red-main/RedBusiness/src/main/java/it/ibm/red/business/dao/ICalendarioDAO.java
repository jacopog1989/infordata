package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dao.impl.TipoCalendario;
import it.ibm.red.business.dto.RuoloDTO;
import it.ibm.red.business.dto.SearchEventiDTO;
import it.ibm.red.business.persistence.model.Calendario;
import it.ibm.red.business.persistence.model.Evento;
import it.ibm.red.business.persistence.model.TestiDefault;

/**
 * 
 * @author CPIERASC
 *
 *	Dao per la gestione del calendario.
 */
public interface ICalendarioDAO extends Serializable {

	/**
	 * Ottiene l'allegato dell'evento.
	 * @param idEvento - id dell'evento
	 * @param con
	 * @return allegato
	 */
	byte[] getAllegatoById(Integer idEvento, Connection con);
	
	/**
	 * Recupero eventi in calendario.
	 * 
	 * @param id			identificativo calendario
	 * @param connection	connessione
	 * @return				evento del calendario
	 */
	Calendario findById(Integer id, Connection connection);

	/**
	 * 
	 * 
	 * @param inIdUtente	identificativo utente
	 * @param inIdNodo		identificativo nodo
	 * @param inIdRuolo		identificativo ruolo
	 * @param connection	connessione
	 * @return				lista record calendario
	 */
	Collection<Calendario> findBy(Long inIdUtente, Long inIdNodo, Long inIdRuolo, Connection connection);

	/**
	 * 
	 * @param evento		Evento da creare
	 * @param con			Connessione
	 * @return index	
	 */
	int insertEvento(Evento evento, Connection con);

	/**
	 * 
	 * @param evento	Evento da aggiornare
	 * @param con		Connessione
	 * @return index
	 */
	boolean aggiornaEvento(Evento evento, Connection con);
	
	/**
	 * 
	 * @param idEvento	id evento da cancellare
	 * @param connection connessione
	 * @return index
	 */
	
	boolean cancellaEvento(int idEvento, Connection connection);
	
	/**
	 * Ottiene gli eventi del calendario.
	 * @param searchBean
	 * @param idUtente - id dell'utente
	 * @param idUfficio - id dell'ufficio
	 * @param idRuolo - id del ruolo
	 * @param idAoo - id dell'Aoo
	 * @param idEnte - id dell'ente
	 * @param withContent
	 * @param con
	 * @return lista degli eventi
	 */
	List<Evento> getEventi(SearchEventiDTO searchBean, Long idUtente, Long idUfficio, Long idRuolo, Long idAoo, Integer idEnte, boolean withContent, Connection con);
	
	/**
	 * Ottiene i testi di default.
	 * @param connection
	 * @return lista di testi di default
	 */
	List<TestiDefault> getAllTestiDefault(Connection connection);
	
	/**
	 * Ottiene i ruoli.
	 * @param con
	 * @param idAoo - id dell'Aoo
	 * @return lista di ruoli
	 */
	List<RuoloDTO> getCategorieRuoli(Connection con, Long idAoo);
	
	/**
	 * Ottiene le tipologie di eventi del calendario
	 * @param con
	 * @return lista di tipologie di eventi del calendario
	 */
	List<TipoCalendario> getAllTipologie(Connection con);

	/**
	 * Inserisce i ruoli target per l'evento.
	 * @param idEvento - id dell'evento
	 * @param idRuoliCategoriaTarget - lista degli id dei ruoli target
	 * @param con
	 */
	void insertRuoliTarget(int idEvento, List<Long> idRuoliCategoriaTarget, Connection con);

	/**
	 * Inserisce i nodi target per l'evento.
	 * @param idEvento - id dell'evento
	 * @param idNodiTarget - lista degli id dei nodi target
	 * @param con
	 */
	void insertNodiTarget(int idEvento, List<Integer> idNodiTarget, Connection con);

	/**
	 * Cancella i nodi target per l'evento.
	 * @param idEvento - id dell'evento
	 * @param con
	 */
	void deleteNodiTarget(int idEvento, Connection con);

	/**
	 * Cancella i ruoli target per l'evento.
	 * @param idEvento - id dell'evento
	 * @param con
	 */
	void deleteRuoliTarget(int idEvento, Connection con);
 
	/**
	 * Ottiene gli eventi del calendario.
	 * @param searchBean
	 * @param idUtente - id dell'utente
	 * @param idUfficio - id dell'ufficio
	 * @param idRuolo - id del ruolo
	 * @param idAoo - id dell'Aoo
	 * @param idEnte - id dell'ente
	 * @param withContent
	 * @param con
	 * @return lista degli eventi
	 */
	List<Evento> getEventiHomepage(SearchEventiDTO searchBean, Long idUtente, Long idUfficio, Long idRuolo, Long idAoo,
			Integer idEnte, boolean withContent, Connection con);

	/**
	 * Ottiene il conteggio degli eventi del calendario.
	 * @param searchBean
	 * @param idUtente - id dell'utente
	 * @param idUfficio - id dell'ufficio
	 * @param idRuolo - id del ruolo
	 * @param idAoo - id dell'Aoo
	 * @param idEnte - id dell'ente
	 * @param con
	 * @return contatore
	 */
	Integer getCountEventiHomepage(SearchEventiDTO searchBean, Long idUtente, Long idUfficio, Long idRuolo, Long idAoo, Integer idEnte, Connection con);
 
}
