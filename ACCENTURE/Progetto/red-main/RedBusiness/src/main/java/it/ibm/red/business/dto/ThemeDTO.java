package it.ibm.red.business.dto;

import it.ibm.red.business.enums.ThemeEnum;

/**
 * The Class ThemeDTO.
 *
 * @author CPIERASC
 * 
 * 	DTO dei temi.
 */
public final class ThemeDTO extends AbstractDTO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificativo.
	 */
	private int id;

	/**
	 * Display name.
	 */
	private String displayName;

	/**
	 * Name.
	 */
	private String name;
    
	/**
	 * Costruttore.
	 * 
	 * @param t	enum
	 */
    public ThemeDTO(final ThemeEnum t) {
        this.id = t.getId();
        this.displayName = t.getDisplayName();
        this.name = t.getName();
    }
 
	/**
	 * Getter.
	 * 
	 * @return	identificativo
	 */
    public int getId() {
        return id;
    }
 
	/**
	 * Getter.
	 * 
	 * @return	display name
	 */
    public String getDisplayName() {
        return displayName;
    }
 
	/**
	 * Getter.
	 * 
	 * @return	name
	 */
    public String getName() {
        return name;
    }
 
    /**
     * 
     * tags:
     * @return
     */
    @Override
    public String toString() {
        return name;
    }
}