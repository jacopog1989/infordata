package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.AssegnazioneAutomaticaCapSpesaDTO;


/**
 * The Interface IAssegnazioneAutomaticaFacadeSRV.
 *
 * @author mcrescentini
 * 
 *         Facade assegnazioni automatiche capitoli di spesa service.
 */
public interface IAssegnazioneAutomaticaCapSpesaFacadeSRV extends Serializable {

	/**
	 * @param idAoo
	 * @return
	 */
	List<AssegnazioneAutomaticaCapSpesaDTO> getByIdAoo(Long idAoo);
	
	/**
	 * @param assegnazioneAutomatica
	 * @param idAoo
	 */
	void elimina(AssegnazioneAutomaticaCapSpesaDTO assegnazioneAutomatica, Long idAoo);
	
	/**
	 * @param idAoo
	 */
	void eliminaTutte(Long idAoo);
	
	/**
	 * @param codiceCapitoloSpesa
	 * @param idAoo
	 * @param idUfficio
	 * @param idUtente
	 */
	void inserisci(String codiceCapitoloSpesa, Long idAoo, Long idUfficio, Long idUtente);
	
	/**
	 * @param assegnazioniAutomatiche
	 * @param idAoo
	 */
	void inserisciMultiple(List<AssegnazioneAutomaticaCapSpesaDTO> assegnazioniAutomatiche, Long idAoo);

	/**
	 * @param file
	 * @param primaCella
	 * @param idAoo
	 * @return
	 */
	List<AssegnazioneAutomaticaCapSpesaDTO> estraiEValidaAssegnazioneAutomaticheDaExcel(byte[] file, String primaCella, Long idAoo);
	
}