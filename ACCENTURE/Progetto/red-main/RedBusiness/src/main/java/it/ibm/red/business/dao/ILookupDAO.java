package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author CPIERASC
 *
 *	Dao per la lookup delle tabelle.
 */
public interface ILookupDAO extends Serializable {

	/**
	 * Recupero descrizione tipo documento.
	 * 
	 * @param id			identificativo tipo documento
	 * @param connection	connessione
	 * @return				tipo documento
	 */
	String getDescTipoDocumento(Long id, Connection connection);
	
	/**
	 * Recupero iter approvativo.
	 * 	
	 * @param id			identificativo iter approvativo
	 * @param connection	connessione
	 * @return				iter approvativo
	 */
	String getIterApprovativo(String id, Connection connection);
	
	/**
	 * Recupero approvazioni.
	 * 
	 * @param documentTitle	document title documento
	 * @param idAoo			identificativo aoo
	 * @param connection	connessione
	 * @return				lista approvazioni
	 */
	Collection<String> getApprovazioni(String documentTitle, Long idAoo, Connection connection);
	
	/**
	 * Recupero descrizione categoria documento.
	 * 
	 * @param idCatDoc		identificativo categoria documento
	 * @param connection	connessione
	 * @return				descrizione
	 */
	String getDescCategoriaDocumento(Long idCatDoc, Connection connection);
	
	/**
	 * Recupero descrizione tipo assegnazione.
	 * 
	 * @param assegnazioneId	id assegnazione
	 * @param iterManuale		flag iter manuale
	 * @param connection		connessione
	 * @return					descrizione tipo assegnazione
	 */
	String getTipoAssegnazioneDescById(Integer assegnazioneId, Boolean iterManuale, Connection connection);

	/**
	 * Ottiene le descrizioni dei tipi documento.
	 * @param idAoo
	 * @param connection
	 * @return mappa id descrizione
	 */
	Map<Long, String> getDescTipoDocumento(Integer idAoo, Connection connection);

	/**
	 * Ottiene le descrizioni degli uffici.
	 * @param ids
	 * @param connection
	 * @return mappa id descrizione
	 */
	Map<Long, String> getDescUffici(Collection<Long> ids, Connection connection);
	
	/**
	 * Ottiene le descrizioni degli utenti.
	 * @param ids
	 * @param connection
	 * @return mappa id descrizione
	 */
	Map<Long, String> getDescUtenti(Collection<Long> ids, Connection connection);

	/**
	 * Ottiene gli ids delle tipologie documento tramite descrizione.
	 * @param descrizioni
	 * @param idAoo
	 * @param connection
	 * @return lista di ids delle tipologie documento
	 */
	List<Long> getIdsTipologiaDocumentoByDescrizione(Collection<String> descrizioni, Long idAoo, Connection connection);
	 
}
