/**
 * 
 */
package it.ibm.red.business.dto;

/**
 * @author APerquoti
 * 
 * tags:
 */
public class DataDTO extends AbstractDTO {

	/**
	 * 
	 * Comment.
	 */
	private static final long serialVersionUID = -5854114535337473513L;
	

	/**
	 * Identificativo file.
	 */
	private String idFile;
	

	/**
	 * Flag is post firma.
	 */
	private boolean isPostFirma;
	
	/**
	 *.
	 * tags:
	 */
	public DataDTO() {
		super();
	}

	/**
	 *.
	 * tags:
	 * @return the idFile
	 */
	public String getIdFile() {
		return idFile;
	}

	/**
	 *.
	 * tags:
	 * @param idFile the idFile to set
	 */
	public void setIdFile(final String idFile) {
		this.idFile = idFile;
	}

	/**
	 *.
	 * tags:
	 * @return the isPostFirma
	 */
	public boolean isPostFirma() {
		return isPostFirma;
	}

	/**
	 *.
	 * tags:
	 * @param isPostFirma the isPostFirma to set
	 */
	public void setPostFirma(final boolean isPostFirma) {
		this.isPostFirma = isPostFirma;
	}

}
