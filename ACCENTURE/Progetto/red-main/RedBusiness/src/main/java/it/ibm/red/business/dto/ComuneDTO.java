package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * DTO che definisce un comune.
 */
public class ComuneDTO implements Serializable {
	
	/**
	 * UID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Id comune.
	 */
	private String idComune;
	
	/**
	 * Denominazione comune.
	 */
	private String denominazione;
	
	/**
	 * Flag soppresso.
	 */
	private Integer soppresso; 
	
	/**
	 * Identificativo provincia.
	 */
	private String idProvincia;

	/**
	 * Restituisce l'id del comune.
	 * @return idComune
	 */
	public String getIdComune() {
		return idComune;
	}

	/**
	 * Imposta l'id del comune.
	 * @param idComune
	 */
	public void setIdComune(final String idComune) {
		this.idComune = idComune;
	}

	/**
	 * Restituisce la denominazione del comune.
	 * @return denominazione
	 */
	public String getDenominazione() {
		return denominazione;
	}

	/**
	 * Imposta la denominazione del comune.
	 * @param denominazione
	 */
	public void setDenominazione(final String denominazione) {
		this.denominazione = denominazione;
	}

	/**
	 * @return soppresso
	 */
	public Integer getSoppresso() {
		return soppresso;
	}

	/**
	 * @param soppresso
	 */
	public void setSoppresso(final Integer soppresso) {
		this.soppresso = soppresso;
	}

	/**
	 * Restituisce l'id della provincia.
	 * @return idProvincia
	 */
	public String getIdProvincia() {
		return idProvincia;
	}

	/**
	 * Imposta l'id del provincia.
	 * @param idProvincia
	 */
	public void setIdProvincia(final String idProvincia) {
		this.idProvincia = idProvincia;
	}
	
	/**
	 * @see java.lang.Object#toString().
	 */
	@Override
	public String toString() {
		return "ComuneETY [idComune=" + idComune + ", denominazione=" + denominazione + ", soppresso=" + soppresso
				+ ", idProvincia=" + idProvincia + "]";
	}

}
