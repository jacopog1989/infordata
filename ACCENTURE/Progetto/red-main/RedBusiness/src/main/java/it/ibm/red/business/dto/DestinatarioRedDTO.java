package it.ibm.red.business.dto;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.persistence.model.Contatto;

/**
 * @author Simone Lacarpia
 *
 */
public class DestinatarioRedDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 71528918732298234L;
	
	/**
	 * Tabella utente idUtente.
	 */
	private Long idUtente;
	
	/**
	 * Tabella nodo idNodo.
	 */
	private Long idNodo;
	
	/**
	 * Cartaceo(1) o Elettronico(2).
	 */
	private MezzoSpedizioneEnum mezzoSpedizioneEnum;
	
	/**
	 * Se il contatto dell'utente ha una mail PEC o PEO allora il mezzo puo' essere anche elettronico
	 * altrimenti deve essere cartaceo e deve essere disabilitata la combo.
	 */
	private boolean mezzoSpedizioneDisabled;

	/**
	 * Il mezzo di spedizione è visibile solo per i destinatari esterni .
	 */
	private boolean mezzoSpedizioneVisible;
	
	/**
	 * Interno(I) / Esterno(E).
	 */
	private TipologiaDestinatarioEnum tipologiaDestinatarioEnum;
	
	/**
	 * TO / CC  .
	 */
	private ModalitaDestinatarioEnum modalitaDestinatarioEnum;
	
	/**
	 * Contatto associato al destinatario.
	 */
	private Contatto contatto;
  	
	/**
	 * Lista contatti.
	 */
	private List<Contatto> contattiGruppo;
	
	/**
	 * Flag non modificabile.
	 */
	private boolean nonModificabile; 
	
	/**
	 * Costruttore vuoto.
	 */
	public DestinatarioRedDTO() {
		super();
	}
	
	
	/**
	 * DESTINATARIO ESTERNO (ha il mezzo di spedizione, il tipo e la modalità, non ha l'ID Utente e l'ID Nodo).
	 * 
	 * @param idMezzo
	 * @param tipo
	 * @param modalita
	 */
	public DestinatarioRedDTO(final Integer idMezzo, final String tipo, final String modalita) {
		this.mezzoSpedizioneEnum = MezzoSpedizioneEnum.getById(idMezzo);
		this.tipologiaDestinatarioEnum = TipologiaDestinatarioEnum.getByTipologia(tipo);
		this.modalitaDestinatarioEnum = ModalitaDestinatarioEnum.getModalita(modalita);
	}
	
	
	/**
	 * DESTINATARIO ESTERNO PRESENTE IN RUBRICA (ha l'ID contatto, il mezzo di spedizione, il tipo e la modalità, NON ha l'ID Utente e l'ID Nodo).
	 * 
	 * @param idContatto
	 * @param mezzoSpedizione
	 * @param modalita
	 */
	public DestinatarioRedDTO(final Long idContatto, final MezzoSpedizioneEnum mezzoSpedizione, final ModalitaDestinatarioEnum modalita) {
		this.contatto = new Contatto();
		this.contatto.setContattoID(idContatto);
		this.mezzoSpedizioneEnum = mezzoSpedizione;
		this.modalitaDestinatarioEnum = modalita;
		this.tipologiaDestinatarioEnum = TipologiaDestinatarioEnum.ESTERNO;
	}
	
	
	/**
	 * DESTINATARIO ESTERNO DA INSERIRE IN RUBRICA (ha le info del nuovo contatto, il mezzo di spedizione, il tipo e la modalità, NON ha l'ID Utente e l'ID Nodo).
	 * 
	 * @param nome
	 * @param cognome
	 * @param alias
	 * @param mail
	 * @param mailPec
	 * @param tipoPersona
	 * @param idAoo
	 * @param mezzoSpedizione
	 * @param modalita
	 */
	public DestinatarioRedDTO(final String nome, final String cognome, final String alias, final String mail, final String mailPec, final String tipoPersona, 
			final Long idAoo, final MezzoSpedizioneEnum mezzoSpedizione, final ModalitaDestinatarioEnum modalita) {
		this.contatto = new Contatto(nome, cognome, tipoPersona, null, mail, mailPec, getAliasContatto(alias, cognome, nome), 1, idAoo);
		this.mezzoSpedizioneEnum = mezzoSpedizione;
		this.modalitaDestinatarioEnum = modalita;
		this.tipologiaDestinatarioEnum = TipologiaDestinatarioEnum.ESTERNO;
	}
	

	/**
	 * DESTINATARIO INTERNO (ha l'UD Utente e l'ID Nodo, non ha il mezzo di spedizione).
	 * 
	 * @param idNodo
	 * @param idUtente
	 * @param tipo
	 * @param modalita
	 */
	public DestinatarioRedDTO(final Long idNodo, final Long idUtente, final String tipologia, final String modalita) {
		this.idUtente = (idUtente == null) ? 0L : idUtente;
		this.idNodo = idNodo;
		this.tipologiaDestinatarioEnum = TipologiaDestinatarioEnum.getByTipologia(tipologia);
		this.modalitaDestinatarioEnum = ModalitaDestinatarioEnum.getModalita(modalita);
	}
	
	
	/**
	 * DESTINATARIO INTERNO (ha l'idUtente e l'idNodo, non ha il mezzo di spedizione).
	 * 
	 * @param idNodo
	 * @param idUtente
	 * @param tipo
	 * @param modalita
	 */
	public DestinatarioRedDTO(final Long idNodo, final Long idUtente, final TipologiaDestinatarioEnum tipo, final ModalitaDestinatarioEnum modalita) {
		this.idUtente = (idUtente == null) ? 0L : idUtente;
		this.idNodo = idNodo;
		this.tipologiaDestinatarioEnum = tipo;
		this.modalitaDestinatarioEnum = modalita;
	}
	
	/**
	 * Il destinatario può essere: 
	 * Interno -> ufficio o utente
	 * Esterno -> contatto della rubrica
	 * Questo identificativo generale permette di distinguere tra le varie possibiità di cui sopra.
	 * 
	 * @return
	 */
	public String getIdentificativo() {
		String id = null;
		
		if (TipologiaDestinatarioEnum.INTERNO == tipologiaDestinatarioEnum) {
			id = TipologiaDestinatarioEnum.INTERNO.getTipologia() + "---" + idNodo;
			id += (idUtente != null && idUtente > 0) ? ("---" + idUtente) : "";
		} else if (TipologiaDestinatarioEnum.ESTERNO == tipologiaDestinatarioEnum && (contatto != null && contatto.getContattoID() != null)) {
			id = TipologiaDestinatarioEnum.ESTERNO.getTipologia() + "---" + contatto.getContattoID();
		} else if (contatto != null) {
			id = TipologiaDestinatarioEnum.ESTERNO.getTipologia() + "---" + contatto.getContattoID() + "---" + System.identityHashCode(contatto);
		}
		
		return id;
	}
	
	/**
	 * @param alias
	 * @param cognome
	 * @param nome
	 * @return
	 */
	private String getAliasContatto(final String alias, final String cognome, final String nome) {
		String lAlias = alias;
		
		if (StringUtils.isBlank(lAlias)) {
			if (StringUtils.isNotBlank(cognome)) {
				lAlias = cognome;
				if (StringUtils.isNotBlank(nome)) {
					lAlias += " " + nome;
				}
			} else if (StringUtils.isNotBlank(nome)) {
				lAlias = nome;
			}
		}
		
		return lAlias;
	}
	
	/** GET & SET. ********************************************************************************************************/
	public Contatto getContatto() {
		return contatto;
	}

	/**
	 * Imposta il contatto.
	 * @param contatto
	 */
	public void setContatto(final Contatto contatto) {
		this.contatto = contatto;
	}

	/**
	 * Restituisce il serialVersionUID.
	 * @return serialVersionUID della classe
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * Restituisce il mezzo di spedizione: CARTACEO, ELETTRONICO, etc.
	 * @see MezzoSpedizioneEnum
	 * @return mezzoSpedizioneEnum
	 */
	public MezzoSpedizioneEnum getMezzoSpedizioneEnum() {
		return mezzoSpedizioneEnum;
	}

	/**
	 * Restituisce la tipologia del destinatario.
	 * @see TipologiaDestinatarioEnum
	 * @return tipologiaDestinatarioEnum
	 */
	public TipologiaDestinatarioEnum getTipologiaDestinatarioEnum() {
		return tipologiaDestinatarioEnum;
	}

	/**
	 * Restituisce la modalità del destinatario: TO, CC, etc.
	 * @see ModalitaDestinatarioEnum
	 * @return modalitaDestinatarioEnum
	 */
	public ModalitaDestinatarioEnum getModalitaDestinatarioEnum() {
		return modalitaDestinatarioEnum;
	}

	/**
	 * Restituisce l'id dell'utente.
	 * @return idUtente
	 */
	public Long getIdUtente() {
		return idUtente;
	}

	/**
	 * Restituisce l'id del nodo associato al destinatario.
	 * @return idNodo
	 */
	public Long getIdNodo() {
		return idNodo;
	}

	/**
	 * Imposta l'id dell'utente.
	 * @param idUtente
	 */
	public void setIdUtente(final Long idUtente) {
		this.idUtente = idUtente;
	}

	/**
	 * Imposta l'id del nodo.
	 * @param idNodo
	 */
	public void setIdNodo(final Long idNodo) {
		this.idNodo = idNodo;
	}

	/**
	 * Imposta il messo di spedizione.
	 * @param mezzoSpedizioneEnum
	 */
	public void setMezzoSpedizioneEnum(final MezzoSpedizioneEnum mezzoSpedizioneEnum) {
		this.mezzoSpedizioneEnum = mezzoSpedizioneEnum;
	}

	/**
	 * Imposta la tiplogia del destinatario.
	 * @param tipologiaDestinatarioEnum
	 */
	public void setTipologiaDestinatarioEnum(final TipologiaDestinatarioEnum tipologiaDestinatarioEnum) {
		this.tipologiaDestinatarioEnum = tipologiaDestinatarioEnum;
	}

	/**
	 * Imposta la modalità del destinatario.
	 * @param modalitaDestinatarioEnum
	 */
	public void setModalitaDestinatarioEnum(final ModalitaDestinatarioEnum modalitaDestinatarioEnum) {
		this.modalitaDestinatarioEnum = modalitaDestinatarioEnum;
	}

	/**
	 * Restituisce true se il mezzo di spezione è disabilitato, false altrimenti.
	 * @return mezzoSpedizioneDisabled
	 */
	public boolean isMezzoSpedizioneDisabled() {
		return mezzoSpedizioneDisabled;
	}

	/**
	 * Imposta il flag: mezzoSpedizioneDisabled.
	 * @param mezzoSpedizioneDisabled
	 */
	public void setMezzoSpedizioneDisabled(final boolean mezzoSpedizioneDisabled) {
		this.mezzoSpedizioneDisabled = mezzoSpedizioneDisabled;
	}

	/**
	 * Restituisce true se il mezzo di spezione è visibile, false altrimenti.
	 * @return
	 */
	public boolean isMezzoSpedizioneVisible() {
		return mezzoSpedizioneVisible;
	}

	/**
	 * Imposta il flag: mezzoSpedizioneVisible.
	 * @param mezzoSpedizioneVisible
	 */
	public void setMezzoSpedizioneVisible(final boolean mezzoSpedizioneVisible) {
		this.mezzoSpedizioneVisible = mezzoSpedizioneVisible;
	}

	/**
	 * Restituisce la lista dei contatti del gruppo.
	 * @return
	 */
	public List<Contatto> getContattiGruppo() {
		return contattiGruppo;
	}

	/**
	 * Imposta la lista dei contatti del gruppo.
	 * @param contattiGruppo
	 */
	public void setContattiGruppo(final List<Contatto> contattiGruppo) {
		this.contattiGruppo = contattiGruppo;
	}

	/**
	 * @return the modificabile
	 */
	public boolean isNonModificabile() {
		return nonModificabile;
	}

	/**
	 * @param modificabile the modificabile to set
	 */
	public void setNonModificabile(final boolean nonModificabile) {
		this.nonModificabile = nonModificabile;
	}

}