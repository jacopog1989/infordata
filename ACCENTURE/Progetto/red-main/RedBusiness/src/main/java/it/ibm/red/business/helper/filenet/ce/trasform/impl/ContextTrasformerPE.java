package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.enums.ContextTrasformerPEEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;

/**
 * Context Trasformer PE.
 * @param <T>
 */
public abstract class ContextTrasformerPE<T> extends TrasformerPE<T> {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -6383805075105624935L;
	
    /**
     * Logger.
     */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContextTrasformerPE.class.getName());

	/**
	 * @param inEnumKey
	 */
	protected ContextTrasformerPE(final TrasformerPEEnum inEnumKey) {
		super(inEnumKey);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE#trasform(filenet.vw.api.VWWorkObject).
	 */
	@Override
	public T trasform(final VWWorkObject workflow) {
		throw new RedException();
	}

	/**
	 * @param workflow
	 * @param context
	 * @return T
	 */
	public abstract T trasform(VWWorkObject workflow, Map<ContextTrasformerPEEnum, Object> context);

	/**
	 * @param workflows
	 * @param context
	 * @return Collection<T>
	 */
	public final Collection<T> trasform(final Collection<VWWorkObject> workflows, final Map<ContextTrasformerPEEnum, Object> context) {
		try {
			Collection<T> output = new ArrayList<>();
			Iterator<?> it = workflows.iterator();
			
			while (it.hasNext()) {
				VWWorkObject workflow = (VWWorkObject) it.next();
				output.add(trasform(workflow, context));
			}
			
			return output;
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero delle informazioni del workflow da FileNet (PE). ", e);
			throw new FilenetException(e);
		}
	}
}