package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * Classe di gestione tipo operazione di un Report.
 */
public class TipoOperazioneReport implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Tipo.
	 */
	private int idTipo;
	
	/**
	 * Codice.
	 */
	private String codice;
	
	/**
	 * Descrizione.
	 */
	private String descrizione;
	
	/**
	 * Identificativo dettaglio report.
	 */
	private int idDettaglioReport;
	
	/**
	 * Restituisce l'id del tipo.
	 * @return id tipo operazione
	 */
	public int getIdTipo() {
		return idTipo;
	}
	
	/**
	 * Imposta l'id del tipo operazione.
	 * @param idTipo
	 */
	public void setIdTipo(final int idTipo) {
		this.idTipo = idTipo;
	}
	
	/**
	 * Restituisce il codice.
	 * @return codice
	 */
	public String getCodice() {
		return codice;
	}
	
	/**
	 * Imposta il codice.
	 * @param codice
	 */
	public void setCodice(final String codice) {
		this.codice = codice;
	}
	
	/**
	 * Restituisce la descrizione.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}
	
	/**
	 * Imposta la descrizione.
	 * @param descrizione
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}
	
	/**
	 * Restituisce l'id del dettaglio report.
	 * @return id dettaglio report
	 */
	public int getIdDettaglioReport() {
		return idDettaglioReport;
	}
	
	/**
	 * Imposta l'id del dettaglio report.
	 * @param idDettaglioReport
	 */
	public void setIdDettaglioReport(final int idDettaglioReport) {
		this.idDettaglioReport = idDettaglioReport;
	}
}