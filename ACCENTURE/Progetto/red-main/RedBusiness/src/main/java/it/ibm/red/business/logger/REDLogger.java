package it.ibm.red.business.logger;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.ibm.red.business.tlocal.CurrentUserTL;
 
/**
 * Logger RED EVO.
 */
public final class REDLogger implements Serializable {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -5742066710392337338L;
	
	/**
     * Logger.
     */
	private transient Logger logger;
	
	/**
	 * Costruttore.
	 * @param inClsName
	 */
	private REDLogger(final String inClsName) {
		logger = LoggerFactory.getLogger(inClsName);
	}
	
	/**
	 * Restituisce un Logger per la gestione dei messaggi di errori associato alla class in ingresso.
	 * @param inCls
	 * @return Logger
	 */
	public static REDLogger getLogger(final Class<?> inCls) {
		return new REDLogger(inCls.getName());
	}

	/**
	 * Restituisce un Logger per la gestione dei messaggi di errori associato alla classe identificata dal nome in ingresso.
	 * @param inClsName
	 * @return Logger
	 */
	public static REDLogger getLogger(final String inClsName) {
		return new REDLogger(inClsName);
	}

	/**
	 * Aggiunge al log un messaggio di tipo informativo.
	 * @param msg
	 */
	public void info(final String msg) {  
		logger.info(CurrentUserTL.getValue() + " " + msg);
	}
	
	/**
	 * Aggiunge al log un messaggio di errore.
	 * @param msg
	 */
	public void error(final String msg) {
		logger.error(CurrentUserTL.getValue() + " " + msg);
	}
	
	/**
	 * Aggiunge al log un messaggio di errore, mostrandone anche l'eccezione riscontrata.
	 * @param msg
	 * @param e
	 */
	public void error(final String msg, final Exception e) {
		logger.error(msg, e);
	}
	
	/**
	 * Aggiunge al log l'eccezione riscontrata.
	 * @param e
	 */
	public void error(final Exception e) {
		logger.error(e.getMessage(), e);
	}

	/**
	 * Aggiunge al log un messaggio di warning.
	 * @param msg
	 */
	public void warn(final String msg) {
		logger.warn(msg);
	}

	/**
	 * Aggiunge al log un messaggio di warning, mostrandone anche l'eccezione riscontrata.
	 * @param msg
	 * @param e
	 */
	public void warn(final String msg, final Exception e) {
		logger.warn(msg, e);
	}

	/**
	 * Aggiunge al log l'eccezione riscontrata.
	 * @param e
	 */
	public void warn(final Exception e) {
		warn(null, e);
	}

}