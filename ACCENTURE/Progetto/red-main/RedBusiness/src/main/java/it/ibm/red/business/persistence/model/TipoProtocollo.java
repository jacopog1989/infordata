package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * The Class TipoProtocollo.
 *
 * @author CPIERASC
 * 
 *         Entità che mappa la tabella TIPOPROTOCOLLO.
 */
public class TipoProtocollo implements Serializable {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Identificativo.
	 */
	private Long idTipoProtocollo;

	/**
	 * Costruttore.
	 * 
	 * @param inIdTipoProtocollo	identificativo
	 * @param inDescrizione			descrizione
	 */
	public TipoProtocollo(final Long inIdTipoProtocollo, final String inDescrizione) {
		this.descrizione = inDescrizione;
		this.idTipoProtocollo = inIdTipoProtocollo;
	}

	/**
	 * Getter descrizione.
	 * 
	 * @return	descrizione
	 */
	public final String getDescrizione() {
		return descrizione;
	}

	/**
	 * Getter id.
	 * 
	 * @return	identificativo
	 */
	public final Long getIdTipoProtocollo() {
		return idTipoProtocollo;
	}

}