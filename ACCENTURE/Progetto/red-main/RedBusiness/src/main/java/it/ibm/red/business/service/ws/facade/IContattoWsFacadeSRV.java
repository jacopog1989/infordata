/**
 * 
 */
package it.ibm.red.business.service.ws.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.RicercaRubricaResultDTO;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.webservice.model.documentservice.types.messages.RedGetContattiType;

/**
 * @author APerquti
 *
 */
public interface IContattoWsFacadeSRV extends Serializable {
	
	/**
	 * @param client
	 * @param request
	 * @return
	 */
	RicercaRubricaResultDTO getContatti(RedWsClient client, RedGetContattiType request);

}
