package it.ibm.red.business.service.concrete;

import java.util.Comparator;

import it.ibm.red.business.dto.MasterDocumentoDTO;

/**
 * The Class DocumentoComparator.
 *
 * @author CPIERASC
 * 
 *         Comparatore per documenti basato sulla data creazione.
 */
public class DocumentoComparator implements Comparator<MasterDocumentoDTO> {

	/**
	 * Compare.
	 *
	 * @param master1
	 *            the master 1
	 * @param master2
	 *            the master 2
	 * @return the int
	 */
	@Override
	public final int compare(final MasterDocumentoDTO master1, final MasterDocumentoDTO master2) {
		int output;
		if (master1.equals(master2)) {
			output = 0;
		} else {
			output = master2.getDataCreazione().compareTo(master1.getDataCreazione());
			if (output == 0) {
				output = master2.getWobNumber().compareTo(master1.getWobNumber());
			}
		}
		return output;
	}
	
}
