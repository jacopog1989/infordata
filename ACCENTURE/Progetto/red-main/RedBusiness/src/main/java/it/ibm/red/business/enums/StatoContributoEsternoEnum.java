/**
 * 
 */
package it.ibm.red.business.enums;

/**
 * @author m.crescentini
 *
 */
public enum StatoContributoEsternoEnum {

	/**
	 * Valore.
	 */
	DISATTIVO(0),
	
	/**
	 * Valore.
	 */
	ATTIVO(1),
	
	/**
	 * Valore.
	 */
	ELIMINATO(2);
	
	/**
	 * Identificativo.
	 */
	private Integer id;
	
	/**
	 * Costruttore.
	 * @param inId
	 */
	StatoContributoEsternoEnum(final Integer inId) {
		id = inId;
	}
	
	/**
	 * Restituisce l'id dell'enum.
	 * @return id
	 */
	public Integer getId() {
		return id;
	}
}