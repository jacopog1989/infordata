package it.ibm.red.business.enums;

/**
 * The Enum TipoAssegnazioneEnum.
 *
 * @author CPIERASC
 * 
 *         Enum per la tipologia di assegnazione.
 */
public enum TipoAssegnazioneEnum {

	/**
	 * Storico.
	 */
	STORICO(0, "Storico"),

	/**
	 * Competenza.
	 */
	COMPETENZA(1, "Competenza"),

	/**
	 * Conoscenza.
	 */
	CONOSCENZA(2, "Conoscenza"),

	/**
	 * Contributo.
	 */
	CONTRIBUTO(3, "Contributo"),

	/**
	 * Firma.
	 */
	FIRMA(4, "Firma"),

	/**
	 * Sigla.
	 */
	SIGLA(5, "Sigla"),

	/**
	 * Visto.
	 */
	VISTO(6, "Visto"),

	/**
	 * Rifiuto assegnazione.
	 */
	RIFIUTO_ASSEGNAZIONE(7, "Rifiuto Assegnazione"),

	/**
	 * Rifiuto firma.
	 */
	RIFIUTO_FIRMA(8, "Rifiuto Firma"),

	/**
	 * Rifiuto sigla.
	 */
	RIFIUTO_SIGLA(9, "Rifiuto Sigla"),

	/**
	 * Rifiuto visto.
	 */
	RIFIUTO_VISTO(10, "Rifiuto Visto"),

	/**
	 * Spedizione.
	 */
	SPEDIZIONE(11, "Spedizione"),

	/**
	 * Visto manuale.
	 */
	VISTO_MANUALE(12, "Visto Manuale"),

	/**
	 * Firmato.
	 */
	FIRMATO(13, "Firmato"),

	/**
	 * Firmato e spedito.
	 */
	FIRMATO_E_SPEDITO(14, "Firmato e Spedito"),
	
	/**
	 * Copia conforme.
	 */
	COPIA_CONFORME(15, "Copia Conforme"),
	
	/**
	 * Firma multipla.
	 */
	FIRMA_MULTIPLA(16, "Firma Multipla"),
	
	/**
	 * Firma multipla.
	 */
	RIFIUTO_FIRMA_MULTIPLA(17, "Rifiuto Firma Multipla"),
	
	/**
	 * Destinatario interno.
	 */
	DESTINATARIO_INTERNO(55, "Destinatario Interno");
	

	/**
	 * Identificativo associato.
	 */
	private int tipoAssegnazioneId;

	/**
	 * Descrizione.
	 */
	private String descrizione;


	/**
	 * Costruttore.
	 * 
	 * @param id	identificativo associato
	 */
	TipoAssegnazioneEnum(final int inId, final String inDescrizione) {
		tipoAssegnazioneId = inId;
		descrizione = inDescrizione;
	}

	/**
	 * Getter identificativo associato.
	 * 
	 * @return	identificativo associato
	 */
	public int getId() {
		return tipoAssegnazioneId;
	}
	
	/**
	 * @return the descrizione
	 */
	public final String getDescrizione() {
		return descrizione;
	}

	/**
	 * Metodo per il recupero dell'enum a partire dall'identificativo associato.
	 * 
	 * @param idTipoAssegnazione	identificativo associato
	 * @return						enum associato all'identificativo
	 */
	public static TipoAssegnazioneEnum get(final Integer idTipoAssegnazione) {
		TipoAssegnazioneEnum output = null;
		if (idTipoAssegnazione != null) {
			for (TipoAssegnazioneEnum t : TipoAssegnazioneEnum.values()) {
				if (t.getId() == idTipoAssegnazione) {
					output = t;
				}
			}
		}
		return output;
	}

	/**
	 * Metodo per testare se un tipo di assegnazione è presente all'interno di una sequenza di tipi assegnazione.
	 * 
	 * @param valore	tipo di cui verificare la presenza
	 * @param tipi		sequenza
	 * @return			esito del test
	 */
	public static boolean isIn(final TipoAssegnazioneEnum valore, final TipoAssegnazioneEnum... tipi) {
		boolean output = false;
		for (TipoAssegnazioneEnum tipo : tipi) {
			if (tipo.equals(valore)) {
				output = true;
				break;
			}
		}
		return output;
	}

}
