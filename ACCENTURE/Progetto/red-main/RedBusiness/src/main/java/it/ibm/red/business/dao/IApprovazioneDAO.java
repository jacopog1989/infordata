package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;

import it.ibm.red.business.dto.ApprovazioneDTO;

/**
 * 
 * @author m.crescentini
 *
 *	Dao gestione approvazioni.
 */
public interface IApprovazioneDAO extends Serializable {

	/**
	 * Aggiorna la stampigliatura di un'approvazione.
	 * @param stampigliatura
	 * @param idApprovazione - id dell'approvazione
	 * @param con
	 * @return esisto della query di Update
	 */
	int aggiornaStampigliatura(int stampigliaturaFirma, int idApprovazione, Connection con);
	
	/**
	 * Ottiene le approvazioni per il documento.
	 * @param idDocumento - id documento
	 * @param idAoo - id dell'Aoo
	 * @param con
	 * @return approvazioni
	 */
	Collection<ApprovazioneDTO> getApprovazioniByIdDocumento(int idDocumento, int idAoo, Connection con);
}
