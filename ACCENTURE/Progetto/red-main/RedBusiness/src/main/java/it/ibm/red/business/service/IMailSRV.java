package it.ibm.red.business.service;
 

import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.enums.StatoMailEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.service.facade.IMailFacadeSRV;

/**
 * Servizio per la gestione delle mail.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public interface IMailSRV extends IMailFacadeSRV {

	/**
	 * Elimina da FileNet la bozza della mail.
	 * @param documentTitle
	 * @param fcUtente
	 * @param idAoo
	 */
	void eliminaBozzaEmailDocumento(String documentTitle, FilenetCredentialsDTO fcUtente, Long idAoo);

	/**
	 * Elimina da FileNet la bozza della mail.
	 * @param documentTitle
	 * @param emailGuid
	 * @param idAoo
	 * @param fcehAdmin
	 */
	void eliminaBozzaEmailDocumento(String documentTitle, String emailGuid, Long idAoo,	IFilenetCEHelper fcehAdmin);

	/**
	 * Verifica che lo stato mail sia protocollabile o meno, quando una email ha lo stato protocollabile <br/>
	 * l'email è protocollabile.
	 * 
	 * @param Stato dell'email
	 * 
	 * @return
	 *  ritorna true quando l'email è protocollabile
	 */
	boolean isProtocollabile(StatoMailEnum st);

	
}