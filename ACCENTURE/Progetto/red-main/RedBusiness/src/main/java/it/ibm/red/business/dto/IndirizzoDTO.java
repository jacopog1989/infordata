package it.ibm.red.business.dto;

/**
 * DTO per la definizione di un indirizzo.
 */
public class IndirizzoDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -5344563822676890438L;

    /**
     * Identificativo.
     */
	private Integer id;

    /**
     * Codice dug.
     */
	private String codiceDug;

    /**
     * Descrizione dug.
     */
	private String descrizioneDug;

    /**
     * Toponimo.
     */
	private String toponimo;

    /**
     * Civico.
     */
	private String civico;

    /**
     * Cap.
     */
	private String cap;

    /**
     * Codice provincia.
     */
	private String codiceProvincia;

    /**
     * Comune.
     */
	private String comune;

    /**
     * Telefono.
     */
	private String telefono;

    /**
     * Fax.
     */
	private String fax;

    /**
     * Note.
     */
	private String note;
	
	/**
	 * Recupera la descrizione dei bug.
	 * @return descrizione dei bug
	 */
	public String getDescrizioneDug() {
		return descrizioneDug;
	}
	
	/**
	 * Imposta la descrizione dei bug.
	 * @param descrizioneDug descrizione dei bug
	 */
	public void setDescrizioneDug(final String descrizioneDug) {
		this.descrizioneDug = descrizioneDug;
	}
	
	/**
	 * Recupera le note.
	 * @return note
	 */
	public String getNote() {
		return note;
	}
	
	/**
	 * Imposta le note.
	 * @param note
	 */
	public void setNote(final String note) {
		this.note = note;
	}
	
	/**
	 * Recupera l'id.
	 * @return id
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Imposta l'id.
	 * @param id
	 */
	public void setId(final Integer id) {
		this.id = id;
	}
	
	/**
	 * Recupera il codice del bug.
	 * @return codice del bug
	 */
	public String getCodiceDug() {
		return codiceDug;
	}
	
	/**
	 * Imposta il codice del bug.
	 * @param codiceDug codice del bug
	 */
	public void setCodiceDug(final String codiceDug) {
		this.codiceDug = codiceDug;
	}
	
	/**
	 * Recupera il toponimo.
	 * @return toponimo
	 */
	public String getToponimo() {
		return toponimo;
	}
	
	/**
	 * Imposta il toponimo.
	 * @param toponimo
	 */
	public void setToponimo(final String toponimo) {
		this.toponimo = toponimo;
	}
	
	/**
	 * Recupera il civico.
	 * @return civico
	 */
	public String getCivico() {
		return civico;
	}
	
	/**
	 * Imposta il civico.
	 * @param civico
	 */
	public void setCivico(final String civico) {
		this.civico = civico;
	}
	
	/**
	 * Recupera il CAP.
	 * @return cap
	 */
	public String getCap() {
		return cap;
	}
	
	/**
	 * Imposta il CAP.
	 * @param cap
	 */
	public void setCap(final String cap) {
		this.cap = cap;
	}
	
	/**
	 * Recupera il codice della provincia.
	 * @return codice della provincia
	 */
	public String getCodiceProvincia() {
		return codiceProvincia;
	}
	
	/**
	 * Imposta il codice della provincia.
	 * @param codiceProvincia codice della provincia
	 */
	public void setCodiceProvincia(final String codiceProvincia) {
		this.codiceProvincia = codiceProvincia;
	}
	
	/**
	 * Recupera il comune.
	 * @return comune
	 */
	public String getComune() {
		return comune;
	}
	
	/**
	 * Imposta il comune.
	 * @param comune
	 */
	public void setComune(final String comune) {
		this.comune = comune;
	}
	
	/**
	 * Recupera il telefono.
	 * @return telefono
	 */
	public String getTelefono() {
		return telefono;
	}
	
	/**
	 * Imposta il telefono.
	 * @param telefono
	 */
	public void setTelefono(final String telefono) {
		this.telefono = telefono;
	}
	
	/**
	 * Recupera il fax.
	 * @return fax
	 */
	public String getFax() {
		return fax;
	}
	
	/**
	 * Imposta il fax.
	 * @param fax
	 */
	public void setFax(final String fax) {
		this.fax = fax;
	}

}
