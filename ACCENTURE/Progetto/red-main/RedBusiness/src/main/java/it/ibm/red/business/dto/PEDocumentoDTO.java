package it.ibm.red.business.dto;

import java.util.Date;

import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.LibroFirmaPerFirmaResponseEnum;
import it.ibm.red.business.enums.TipoOperazioneLibroFirmaEnum;

/**
 * The Class PEDocumentoDTO.
 *
 * @author CPIERASC
 * 
 * 	DTO per le informazioni su un documento contenute nel PE.
 */
public class PEDocumentoDTO extends PEDocumentoAbsrtractDTO {

	/**
	 * Serializzable.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Contatore.
	 */
	private int count;

	/**
	 * Data creazione work flow.
	 */
	private final String dataCreazioneWF;

	/**
	 * Elenco libro firma.
	 */
	private final String[] elencoLibroFirma;

	/**
	 * Flag firma abilitata.
	 */
	private Boolean flagEnableFirma;
	
	/**
	 * Flag firma copia conforme.
	 */
	private final Boolean firmaCopiaConforme;

	/**
	 * Flag firma autografa abilitata.
	 */
	private Boolean flagEnableFirmaAutografa;

	/**
	 * Flag rifiuto abilitato.
	 */
	private Boolean flagEnableRifiuta;

	/**
	 * Flag sigla abilitata.
	 */
	private Boolean flagEnableSigla;

	/**
	 * Flag visto abilitato.
	 */
	private Boolean flagEnableVista;

	/**
	 * Flag firma digitale.
	 */
	private final Boolean flagFirmaDig;

	/**
	 * Flag iter manuale.
	 */
	private final Integer flagIterManuale;

	/**
	 * Identificativo fascicolo.
	 */
	private final Integer idFascicolo;

	/**
	 * Motivo assegnazione.
	 */
	private final String motivazioneAssegnazione;

	/**
	 * Operazione da eseguire sul documento.
	 */
	private final TipoOperazioneLibroFirmaEnum operazione;

	/**
	 * Soggetto.
	 */
	private final String subject;
	
	/**
	 * Flag renderizzato.
	 */
	private final Boolean flagRenderizzato;

	/**
	 * Flag urgenti.
	 */
	private final Boolean urgente;

	/**
	 * Flag procedi da corriere.
	 */
	private Boolean flagProcediDaCorriere;
	
	/**
	 * Lista di response grezza restituita dal wf
	 */
	private String[] responses;
	
	/**
	 * Step name
	 */
	private String stepName;
	
	/**
	 *  Nota dto
	 */
	private NotaDTO notaDTO;
	
	/**
	 * Contributi richiesti
	 */
	private Integer contributiRichiesti;
	
	/**
	 * Contributi pervenuti
	 */
	private Integer contributiPervenuti;
	
	/**
	 *  Assegnatario
	 */
	private final String assegnatario;
	
	/**
	 * Data scadenza
	 */
	private Date dataScadenza;
	
	/**
	 * 
	 */
	private final Date dataAss;
	
	/**
	 * Tipo firma
	 */
	private final int tipoFirma;
	
	/**
	 * Annulla template
	 */
	private final boolean annullaTemplate;
	
	/**
	 * 
	 */
	private Boolean firmaAsincronaAvviata;

	/** 
	 *
	 * @return true, if is annulla template
	 */

	/**
	 * Costruttore.
	 *
	 * @param inIdDocumento 				identificativo documento
	 * @param inOperazione 				operazione
	 * @param inWobNumber 				wob number
	 * @param inQueue 					coda
	 * @param inIdUtenteDestinatario 	identificativo utente destinatario
	 * @param inIdNodoDestinatario 		identificativo nodo destinatario
	 * @param inFlagFirmaDig 			flag firma digitale
	 * @param inMotivazioneAssegnazione 	motivazione assegnazione
	 * @param inResponses 				insieme di response associate
	 * @param inIdFascicolo 				identigicativo fascicolo
	 * @param inCodaCorrente 			coda corrente
	 * @param inTipoAssegnazioneId 		identificativo tipo assegnazione
	 * @param inFlagIterManuale 			flag iter manuale
	 * @param inSubject 					subject
	 * @param inElencoLibroFirma 		elenco libro firma
	 * @param inCount 					contatore
	 * @param inDataCreazioneWF 			data creazione work flow
	 * @param inFlagRenderizzato 		Flag renderizzazione work flow
	 * @param inFirmaCopiaConforme 		flag copia conforme
	 * @param inUrgente 					flag urgente
	 * @param inStepName the in step name
	 * @param inNotaDTO the in nota DTO
	 * @param inContributiRichiesti the in contributi richiesti
	 * @param inContributiPervenuti the in contributi pervenuti
	 * @param tipoFirma the tipo firma
	 * @param annullaTemplate the annulla template
	 */
	public PEDocumentoDTO(final String inIdDocumento, final TipoOperazioneLibroFirmaEnum inOperazione, final String inWobNumber, 
			final DocumentQueueEnum inQueue, final Integer inIdUtenteDestinatario, final Integer inIdNodoDestinatario, final Boolean inFlagFirmaDig, 
			final String inMotivazioneAssegnazione, final String[] inResponses, final Integer inIdFascicolo, final String inCodaCorrente, 
			final Integer inTipoAssegnazioneId, final Integer inFlagIterManuale, final String inSubject, final String[] inElencoLibroFirma,
			final Integer inCount, final String inDataCreazioneWF, final Boolean inFlagRenderizzato, final Boolean inFirmaCopiaConforme, final Boolean inUrgente, 
			final String inStepName, final NotaDTO inNotaDTO, final Integer inContributiRichiesti, final Integer inContributiPervenuti, final int tipoFirma, 
			final boolean annullaTemplate, final Boolean firmaAsincronaAvviata) {
		this(null, inIdDocumento, inOperazione, inWobNumber, inQueue, inIdUtenteDestinatario, inIdNodoDestinatario, inFlagFirmaDig, inMotivazioneAssegnazione, inResponses,
				inIdFascicolo, inCodaCorrente, inTipoAssegnazioneId, inFlagIterManuale, inSubject, inElencoLibroFirma, inCount, inDataCreazioneWF, inFlagRenderizzato,
				inFirmaCopiaConforme, inUrgente, inStepName, inNotaDTO, inContributiRichiesti, inContributiPervenuti, null, null, tipoFirma, annullaTemplate,
				firmaAsincronaAvviata);	
	}
	
	
	
	/**
	 * Costruttore.
	 *
	 * @param inDataAss
	 *            data assegnazione
	 * @param inIdDocumento
	 *            id documento
	 * @param inOperazione
	 *            operazione
	 * @param inWobNumber
	 *            wob number
	 * @param inQueue
	 *            coda lavorativa
	 * @param inIdUtenteDestinatario
	 *            id utente destinatario
	 * @param inIdNodoDestinatario
	 *            id nodo destinatario
	 * @param inFlagFirmaDig
	 *            flag firma digitale
	 * @param inMotivazioneAssegnazione
	 *            motivazione assegnazione
	 * @param inResponses
	 *            response
	 * @param inIdFascicolo
	 *           indice fascicolo
	 * @param inCodaCorrente
	 *            coda corrente
	 * @param inTipoAssegnazioneId
	 *            id tipo assegnazione
	 * @param inFlagIterManuale
	 *            flag iter manuale
	 * @param inSubject
	 *            subject
	 * @param inElencoLibroFirma
	 *            elenco libro firma
	 * @param inCount
	 *            count dei documenti
	 * @param inDataCreazioneWF
	 *            data creazione WF
	 * @param inFlagRenderizzato
	 *            flag renderizzato
	 * @param inFirmaCopiaConforme
	 *            firma copia conforme
	 * @param inUrgente
	 *            flag urgente
	 * @param inStepName
	 *            step name
	 * @param inNotaDTO
	 *            nota DTO
	 * @param inContributiRichiesti
	 *            contributi richiesti
	 * @param inContributiPervenuti
	 *            contributi pervenuti
	 * @param inAssegnatario
	 *            assegnatario
	 * @param inDataScadenza
	 *            data scadenza
	 * @param tipoFirma
	 *            tipo firma
	 * @param annullaTemplate
	 *            flag annulla template
	 */
	public PEDocumentoDTO(final Date inDataAss, final String inIdDocumento, final TipoOperazioneLibroFirmaEnum inOperazione, final String inWobNumber, 
			final DocumentQueueEnum inQueue, final Integer inIdUtenteDestinatario, final Integer inIdNodoDestinatario, final Boolean inFlagFirmaDig, 
			final String inMotivazioneAssegnazione, final String[] inResponses, final Integer inIdFascicolo, final String inCodaCorrente, 
			final Integer inTipoAssegnazioneId, final Integer inFlagIterManuale, final String inSubject, final String[] inElencoLibroFirma,
			final Integer inCount, final String inDataCreazioneWF, final Boolean inFlagRenderizzato, final Boolean inFirmaCopiaConforme, final Boolean inUrgente, 
			final String inStepName, final NotaDTO inNotaDTO, final Integer inContributiRichiesti, final Integer inContributiPervenuti, final String inAssegnatario, 
			final Date inDataScadenza, final int tipoFirma, final boolean annullaTemplate, final Boolean firmaAsincronaAvviata) {
		super(inIdDocumento,
				inWobNumber,
				inTipoAssegnazioneId,
				inCodaCorrente,
				inQueue,
				inIdNodoDestinatario,
				inIdUtenteDestinatario);
		dataAss = inDataAss;
		urgente = inUrgente;
		operazione = inOperazione;
		flagFirmaDig = inFlagFirmaDig;
		motivazioneAssegnazione = inMotivazioneAssegnazione;
		idFascicolo = inIdFascicolo;
		flagIterManuale = inFlagIterManuale;
		subject = inSubject;

		flagEnableFirma = false;
		flagEnableFirmaAutografa = false;
		flagEnableRifiuta = false;
		flagEnableVista = false;
		flagEnableSigla = false;
		flagProcediDaCorriere = false;

		elencoLibroFirma = inElencoLibroFirma;
		if (inCount != null) {
			count = inCount;
		}
		dataCreazioneWF = inDataCreazioneWF;
		flagRenderizzato = inFlagRenderizzato;
		firmaCopiaConforme = inFirmaCopiaConforme;
		
		responses = inResponses;
		stepName = inStepName;
		notaDTO = inNotaDTO;
		
		contributiPervenuti = inContributiPervenuti;
		contributiRichiesti = inContributiRichiesti;
		assegnatario = inAssegnatario;
		
		dataScadenza = inDataScadenza;
		
		this.tipoFirma = tipoFirma;
		
		final boolean bIsInSignQueue = (inResponses != null && getQueue() != null && DocumentQueueEnum.isInSignQueue(getQueue().getName()));
		if (inResponses != null) {
			for (final String response:inResponses) {
				if (bIsInSignQueue) {
					if (LibroFirmaPerFirmaResponseEnum.FIRMA_DIGITALE.firstIn(response) != null) {
						flagEnableFirma = true;
					} else if (LibroFirmaPerFirmaResponseEnum.FIRMA_AUTOGRAFA.firstIn(response) != null) {
						flagEnableFirmaAutografa = true;
					} else if (LibroFirmaPerFirmaResponseEnum.RIFIUTA.firstIn(response) != null) {
						flagEnableRifiuta = true;
					} else if (LibroFirmaPerFirmaResponseEnum.VISTA.firstIn(response) != null) {
						flagEnableVista = true;
					} else if (LibroFirmaPerFirmaResponseEnum.SIGLA.firstIn(response) != null) {
						flagEnableSigla = true;
					}
				} else {
					if (LibroFirmaPerFirmaResponseEnum.PROCEDI_DA_CORRIERE.firstIn(response) != null) {
						 flagProcediDaCorriere = true;
					}
				}
			}
		}
		
		this.annullaTemplate = annullaTemplate;
		this.firmaAsincronaAvviata = firmaAsincronaAvviata;
	}

	/**
	 * Getter.
	 * 
	 * @return	urgenti
	 */
	public Boolean getUrgente() {
		return urgente;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	flag firma copia conforme
	 */
	public final Boolean getFirmaCopiaConforme() {
		return firmaCopiaConforme;
	}

	/**
	 * Getter flag renderizzato.
	 * 
	 * @return	Flag renderizzato.
	 */
	public final Boolean getFlagRenderizzato() {
		if (flagRenderizzato == null) {
			return Boolean.FALSE;
		}
		return flagRenderizzato;
	}

	/**
	 * Getter.
	 * 
	 * @return	contatore
	 */
	public final int getCount() {
		return count;
	}

	/**
	 * Getter.
	 * 
	 * @return	data creazione workflow
	 */
	public final String getDataCreazioneWF() {
		return dataCreazioneWF;
	}

	/**
	 * Getter.
	 * 
	 * @return	elenco libro firma
	 */
	public final String[] getElencoLibroFirma() {
		return elencoLibroFirma;
	}

	/**
	 * Getter.
	 * 
	 * @return	flag enable firma
	 */
	public final Boolean getFlagEnableFirma() {
		return flagEnableFirma;
	}

	/**
	 * Getter.
	 * 
	 * @return	flag enable firma autografa
	 */
	public final Boolean getFlagEnableFirmaAutografa() {
		return flagEnableFirmaAutografa;
	}

	/**
	 * Getter.
	 * 
	 * @return	flag enable rifiuta
	 */
	public final Boolean getFlagEnableRifiuta() {
		return flagEnableRifiuta;
	}

	/**
	 * Getter.
	 * 
	 * @return	flag enable sigla
	 */
	public final Boolean getFlagEnableSigla() {
		return flagEnableSigla;
	}

	/**
	 * Getter.
	 * 
	 * @return	flag enable vista
	 */
	public final Boolean getFlagEnableVista() {
		return flagEnableVista;
	}

	/**
	 * Getter.
	 * 
	 * @return	flag enable firma digitale
	 */
	public final Boolean getFlagFirmaDig() {
		return flagFirmaDig;
	}

	/**
	 * Getter.
	 * 
	 * @return	flag iter manuale
	 */
	public final Integer getFlagIterManuale() {
		return flagIterManuale;
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo fascicolo
	 */
	public final Integer getIdFascicolo() {
		return idFascicolo;
	}

	/**
	 * Getter.
	 * 
	 * @return	motivazione assegnazione
	 */
	public final String getMotivazioneAssegnazione() {
		return motivazioneAssegnazione;
	}

	/**
	 * Getter.
	 * 
	 * @return	tipo operazione
	 */
	public final TipoOperazioneLibroFirmaEnum getOperazione() {
		return operazione;
	}

	/**
	 * Getter.
	 * 
	 * @return	subject
	 */
	public final String getSubject() {
		return subject;
	}

	/**
	 * Gets the flag procedi da corriere.
	 *
	 * @return the flagProcediDaCorriere
	 */
	public Boolean getFlagProcediDaCorriere() {
		return flagProcediDaCorriere;
	}

	/**
	 * Sets the flag procedi da corriere.
	 *
	 * @param inFlagProcediDaCorriere the flagProcediDaCorriere to set
	 */
	public void setFlagProcediDaCorriere(final Boolean inFlagProcediDaCorriere) {
		this.flagProcediDaCorriere = inFlagProcediDaCorriere;
	}

	/** 
	 *
	 * @return the responses
	 */
	public final String[] getResponses() {
		return responses;
	}

	/** 
	 *
	 * @param responses the responses to set
	 */
	public final void setResponses(final String[] responses) {
		this.responses = responses;
	}

	/** 
	 *
	 * @return the stepName
	 */
	public String getStepName() {
		return stepName;
	}

	/** 
	 *
	 * @param stepName the stepName to set
	 */
	public void setStepName(final String stepName) {
		this.stepName = stepName;
	}

	/** 
	 *
	 * @return the notaDTO
	 */
	public NotaDTO getNotaDTO() {
		return notaDTO;
	}

	/** 
	 *
	 * @param notaDTO the notaDTO to set
	 */
	public void setNotaDTO(final NotaDTO notaDTO) {
		this.notaDTO = notaDTO;
	}

	/** 
	 *
	 * @return the contributiRichiesti
	 */
	public final Integer getContributiRichiesti() {
		return contributiRichiesti;
	}

	/** 
	 *
	 * @param contributiRichiesti the contributiRichiesti to set
	 */
	public final void setContributiRichiesti(final Integer contributiRichiesti) {
		this.contributiRichiesti = contributiRichiesti;
	}

	/** 
	 *
	 * @return the contributiPervenuti
	 */
	public final Integer getContributiPervenuti() {
		return contributiPervenuti;
	}

	/** 
	 *
	 * @param contributiPervenuti the contributiPervenuti to set
	 */
	public final void setContributiPervenuti(final Integer contributiPervenuti) {
		this.contributiPervenuti = contributiPervenuti;
	}

	/** 
	 *
	 * @return the assegnatario
	 */
	public String getAssegnatario() {
		return assegnatario;
	}

	/** 
	 *
	 * @return the data ass
	 */
	public Date getDataAss() {
		return dataAss;
	}
 
	
	/** 
	 *
	 * @return the tipo firma
	 */
	public int getTipoFirma() {
		return tipoFirma;
	}

	

	/** 
	 *
	 * @return true, if is annulla template
	 */
	public boolean isAnnullaTemplate() {
		return annullaTemplate;
	}

	/** 
	 *
	 * @return the data scadenza
	 */
	public Date getDataScadenza() {
		return dataScadenza;
	}

	/** 
	 *
	 * @param dataScadenza the new data scadenza
	 */
	public void setDataScadenza(final Date dataScadenza) {
		this.dataScadenza = dataScadenza;
	}
	
	/**
	 * @return the firmaAsincronaAvviata
	 */
	public Boolean getFirmaAsincronaAvviata() {
		return firmaAsincronaAvviata;
	}
	
}
