package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio che gestisce l'invio in firma di un documento.
 */
public interface IInvioInFirmaFacadeSRV extends Serializable {
	
	/**
	 * Invia uno o più workflow nello stato "Invia in firma".
	 * 
	 * @param utente
	 *            utente che esegue l'operazione
	 * @param wobNumbers
	 *            lista di identificativi dei workflow
	 * @param idNodoDestinatarioNew
	 *            nodo/ufficio del destinatario
	 * @param idUtenteDestinatarioNew
	 *            identificativo del destinatario
	 * @param motivazione
	 *            motivazione per l'attivazione di questa procedura
	 * @param urgente
	 *            se questa procedura è urgente
	 * @return Un DTO che contiene l'esito dell'operazione
	 */
	List<EsitoOperazioneDTO> invioInFirma(UtenteDTO utente, Collection<String> wobNumbers, Long idNodoDestinatarioNew,
			Long idUtenteDestinatarioNew, String motivazione, Boolean urgente);

	
	/**
	 * Invia uno o più workflow nello stato "Invia in sigla".
	 * 
	 * @param utente
	 *            utente che esegue l'operazione
	 * @param wobNumbers
	 *            lista di identificativi dei workflow
	 * @param idNodoDestinatarioNew
	 *            nodo/ufficio del destinatario
	 * @param idUtenteDestinatarioNew
	 *            identificativo del destinatario
	 * @param motivazione
	 *            motivazione per l'attivazione di questa procedura
	 * @param urgente
	 *            se questa procedura è urgente
	 * @return Un DTO che contiene l'esito dell'operazione
	 */
	List<EsitoOperazioneDTO> invioInSigla(UtenteDTO utente, Collection<String> wobNumbers, Long idNodoDestinatarioNew,
			Long idUtenteDestinatarioNew, String motivazione, Boolean urgente);
	

	/**
	 * Invia uno o più workflow nello stato "Invia in firma multipla".
	 * 
	 * @param utente
	 *            utente che esegue l'operazione
	 * @param wobNumbers
	 *            lista di identificativi dei workflow
	 * @param idNodoDestinatarioNew
	 *            nodo/ufficio del destinatario
	 * @param idUtenteDestinatarioNew
	 *            identificativo del destinatario
	 * @param motivazione
	 *            motivazione per l'attivazione di questa procedura
	 * @param urgente
	 *            se questa procedura è urgente
	 * @return Un DTO che contiene l'esito dell'operazione
	 */
	List<EsitoOperazioneDTO> invioInFirmaMultipla(UtenteDTO utente, Collection<String> wobNumbers, Long idNodoDestinatarioNew,
			Long idUtenteDestinatarioNew, String motivazione, Boolean urgente);
}
