package it.ibm.red.business.service;

import java.util.List;
import java.util.Map;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.xml.AbstractXmlHelper;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.service.facade.IProtocollaPECFacadeSRV;

/**
 * Interfaccia del servizio di protocollazione PEC.
 */
public interface IProtocollaPECSRV extends IProtocollaPECFacadeSRV {

	/**
	 * Inizializza l'helper che consente il parsing degli allegati xml previsti per
	 * il flusso.
	 * 
	 * @param email
	 */
	AbstractXmlHelper initXmlHelper(IFilenetCEHelper fceh, Document email);

	/**
	 * Recupera la prima email in arrivo
	 * 
	 * @param fceh
	 * @param casellaPostale
	 * @return
	 */
	Document fetchFirstMail(IFilenetCEHelper fceh, String casellaPostale);

	/**
	 * Recupera l'ufficio destinatario dall'email.
	 * 
	 * @param email
	 * @return null, in caso per il flusso specifico, l'ufficio destinatario non
	 *         dipenda dalla singola mail
	 */
	Integer getUfficioDestinatarioFromMail(Document email, AbstractXmlHelper helper);

	/**
	 * Il metodo controlla che la mail in input abbia allegati conentiti in termini
	 * di estensioni
	 * 
	 * @param fceh
	 * @param email
	 * @param allowedFileExtensions
	 * @return
	 */
	boolean hasSupportedFileExtensionsAttachment(IFilenetCEHelper fceh, Document email, List<String> allowedFileExtensions);

	/**
	 * Prende in carico l'email cambiandone il metadato relativo allo stato su
	 * Filenet
	 * 
	 * @param fceh
	 * @param email
	 * @return
	 */
	boolean markInElaborazione(IFilenetCEHelper fceh, Document email);

	/**
	 * Scarta l'email cambiandone il metadato relativo allo stato su Filenet
	 * 
	 * @param fceh
	 * @param email
	 */
	void markInArrivoScartata(IFilenetCEHelper fceh, Document email);

	/**
	 * Registra la mail come protocollata
	 * 
	 * @param fceh
	 * @param email
	 * @param guidDocumentoProtocollato
	 * @return
	 */
	boolean markProtocollata(IFilenetCEHelper fceh, Document email, String guidDocumentoProtocollato);

	/**
	 * Registra la mail in errore post protocollazione
	 * 
	 * @param fceh
	 * @param email
	 * @return
	 */
	boolean markInErrorePostProtocollazione(IFilenetCEHelper fceh, Document email);

	/**
	 * Registra la mail in errore di protocollazione
	 * 
	 * @param fceh
	 * @param email
	 * @return
	 */
	boolean markInErrore(IFilenetCEHelper fceh, Document email);

	/**
	 * Estrae gli allegati dalla mail in input, popolando il DocumentoRedFnDTO
	 * 
	 * @param fceh
	 * @param email
	 * @param utente
	 * @param documento
	 */
	void extractMailAttachments(IFilenetCEHelper fceh, Document email, UtenteDTO utente, DetailDocumentRedDTO documento, AbstractXmlHelper helper);

	/**
	 * Esegue, se necessario, attività a valle della fase di creazione e
	 * protocollazione in ingresso del documento (il cui guid è in input)
	 * 
	 * @param fceh
	 * @param email         l'oggetto Filenet relativo alla mail protocollata
	 * @param guidDocumento del documento registrato e protocollato a partire dalla
	 *                      mail
	 */
	void eseguiAttivitaPostProtocollazione(IFilenetCEHelper fceh, Document email, String guidDocumento, Aoo aoo, AbstractXmlHelper helper);

	/**
	 * Recupera, in caso il flusso lo richieda, l'identificativo del fascicolo da
	 * associare al documento che si sta registrando su Filenet a partire dalla mail
	 * protocollata.
	 * 
	 * @param mail
	 * @param fceh
	 * @return
	 */
	String getIdFascicoloDaAssociare(Document mail, IFilenetCEHelper fceh, AbstractXmlHelper helper);

	/**
	 * Recupera, in caso il flusso lo richieda, alcuni metadati del fascicolo da
	 * associare al documento che si sta registrando su Filenet a partire dalla mail
	 * protocollata.
	 * 
	 * @param mail
	 * @return
	 */
	Map<String, Object> getMetadatiFascicoloDaAssociare(Document mail, AbstractXmlHelper helper);
}