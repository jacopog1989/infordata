package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.NotaDTO;
import it.ibm.red.business.dto.NotaDocumentoDTO;
import it.ibm.red.business.dto.NotaDocumentoDescrizioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ColoreNotaEnum;

/**
 * The Interface INotaFacadeSRV.
 *
 * @author FMANTUANO
 * 
 *         Servizi gestione della nota sul workflow.
 */
public interface INotaFacadeSRV extends Serializable {

	/**
	 * Recupero della nota.
	 * 
	 * @param fcDTO     credenziali filenet
	 * @param wobNumber Wob Number
	 * @return content
	 */
	NotaDTO getNota(FilenetCredentialsDTO fcDTO, String wobNumber);

	/**
	 * Salvataggio della nota.
	 * 
	 * @param utente    utente
	 * @param wobNumber Wob Number
	 * @param nota      content da salvare
	 * @return
	 */
	EsitoOperazioneDTO registraNotaFromWorkflow(UtenteDTO utente, String wobNumber, NotaDTO nota, Integer numDoc, boolean writeOnDb);

	/**
	 * Salvataggio della nota.
	 * 
	 * @param utente     utente
	 * @param wobNumbers WobNumbers
	 * @param nota       content da salvare
	 * @return
	 */
	Collection<EsitoOperazioneDTO> registraNotaFromWorkflow(UtenteDTO utente, List<String> wobNumbers, NotaDTO nota, Integer numDoc, boolean writeOnDb);

	/**
	 * Salvataggio della nota inserita dallo storico note.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param colore
	 * @param testoNota
	 * @return
	 */
	void registraNotaFromDocumento(UtenteDTO utente, Integer idDocumento, ColoreNotaEnum colore, String testoNota);

	/**
	 * Cancella logicamente la nota in input
	 * 
	 * @param nota
	 * @param idAoo
	 * @return
	 */
	void deleteNotaDocumento(NotaDocumentoDTO nota, Integer idAoo);

	/**
	 * Restituisce tutte le note del documento
	 * 
	 * @param idDocumento
	 * @param alsoClosed
	 * @param utente
	 * @return
	 */
	List<NotaDocumentoDescrizioneDTO> getAllNotes(String idDocumento, boolean alsoClosed, UtenteDTO utente);

}
