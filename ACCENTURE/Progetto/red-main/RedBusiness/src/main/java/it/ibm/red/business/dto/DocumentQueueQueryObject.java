package it.ibm.red.business.dto;

import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWRosterQuery;
import it.ibm.red.business.enums.DocumentQueueEnum;

/**
 * Model Document Queue Query Object.
 */
public class DocumentQueueQueryObject {
	
	/**
	 * Queue query.
	 */
	private VWQueueQuery queueQueryObj;

	/**
	 * Roster query.
	 */
	private VWRosterQuery rosterQueryObj;

	/**
	 * Coda.
	 */
	private DocumentQueueEnum queueEnum;

	/**
	 * Restituisce il queueObj per la ricerca.
	 * @return queueQueryObj
	 */
	public VWQueueQuery getQueueQueryObj() {
		return queueQueryObj;
	}

	/**
	 * Imposta il queueObj per la ricerca.
	 * @param queueQueryObj
	 */
	public void setQueueQueryObj(final VWQueueQuery queueQueryObj) {
		this.queueQueryObj = queueQueryObj;
	}

	/**
	 * Restituisce il VWRosterQuery.
	 * @return rosterQueryObj
	 */
	public VWRosterQuery getRosterQueryObj() {
		return rosterQueryObj;
	}

	/**
	 * @param rosterQueryObj
	 */
	public void setRosterQueryObj(final VWRosterQuery rosterQueryObj) {
		this.rosterQueryObj = rosterQueryObj;
	}

	/**
	 * @return queueEnum
	 */
	public DocumentQueueEnum getQueueEnum() {
		return queueEnum;
	}

	/**
	 * 
	 * @param queueEnum
	 */
	public void setQueueEnum(final DocumentQueueEnum queueEnum) {
		this.queueEnum = queueEnum;
	}

}
