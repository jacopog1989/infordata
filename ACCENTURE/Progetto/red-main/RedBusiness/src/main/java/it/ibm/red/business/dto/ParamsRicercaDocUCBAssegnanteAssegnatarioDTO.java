package it.ibm.red.business.dto;

import java.util.Date;

import it.ibm.red.business.enums.RicercaAssegnanteAssegnatarioEnum;


/**
 * DTO per la ricerca dei documenti UCB per assegnante/assegnatario.
 * 
 * @author m.crescentini
 *
 */
public class ParamsRicercaDocUCBAssegnanteAssegnatarioDTO extends AbstractDTO {

	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = -7536538789913565390L;
	
	/**
	 * Tipo ricerca 
	 */
	private RicercaAssegnanteAssegnatarioEnum tipoRicerca;

	/**
	 * Data creazione da 
	 */
	private Date dataCreazioneDa;
	
	/**
	 * Data creazione a 
	 */
	private Date dataCreazioneA;
	
	/**
	 * Data scadenza da 
	 */
	private Date dataScadenzaDa;
	
	/**
	 * Data scadenza a 
	 */
	private Date dataScadenzaA;
	
	/**
	 * Numero protocollo da 
	 */
	private Integer numeroProtocolloDa;
	
	/**
	 * Numero protocollo a 
	 */
	private Integer numeroProtocolloA;

	/**
	 * Anno protocollo 
	 */
	private Integer annoProtocollo;
	
	/**
	 * Descrizione assegnatario 
	 */
	private String descrizioneAssegnatario;
	
	/**
	 * Assegnatario 
	 */
	private AssegnatarioOrganigrammaDTO assegnatario;
	
	
	/**
	 * Costruttore params ricerca doc UCB assegnante assegnatario DTO.
	 */
	public ParamsRicercaDocUCBAssegnanteAssegnatarioDTO() {
		super();
	}

	/** 
	 *
	 * @return the tipo ricerca
	 */
	public RicercaAssegnanteAssegnatarioEnum getTipoRicerca() {
		return tipoRicerca;
	}

	/** 
	 *
	 * @param tipoRicerca the new tipo ricerca
	 */
	public void setTipoRicerca(final RicercaAssegnanteAssegnatarioEnum tipoRicerca) {
		this.tipoRicerca = tipoRicerca;
	}

	/** 
	 *
	 * @return the data creazione da
	 */
	public Date getDataCreazioneDa() {
		return dataCreazioneDa;
	}

	/** 
	 *
	 * @param dataCreazioneDa the new data creazione da
	 */
	public void setDataCreazioneDa(final Date dataCreazioneDa) {
		this.dataCreazioneDa = dataCreazioneDa;
	}

	/** 
	 *
	 * @return the data creazione A
	 */
	public Date getDataCreazioneA() {
		return dataCreazioneA;
	}

	/** 
	 *
	 * @param dataCreazioneA the new data creazione A
	 */
	public void setDataCreazioneA(final Date dataCreazioneA) {
		this.dataCreazioneA = dataCreazioneA;
	}

	/** 
	 *
	 * @return the data scadenza da
	 */
	public Date getDataScadenzaDa() {
		return dataScadenzaDa;
	}

	/** 
	 *
	 * @param dataScadenzaDa the new data scadenza da
	 */
	public void setDataScadenzaDa(final Date dataScadenzaDa) {
		this.dataScadenzaDa = dataScadenzaDa;
	}

	/** 
	 *
	 * @return the data scadenza A
	 */
	public Date getDataScadenzaA() {
		return dataScadenzaA;
	}

	/** 
	 *
	 * @param dataScadenzaA the new data scadenza A
	 */
	public void setDataScadenzaA(final Date dataScadenzaA) {
		this.dataScadenzaA = dataScadenzaA;
	}

	/** 
	 *
	 * @return the numero protocollo da
	 */
	public Integer getNumeroProtocolloDa() {
		return numeroProtocolloDa;
	}

	/** 
	 *
	 * @param numeroProtocolloDa the new numero protocollo da
	 */
	public void setNumeroProtocolloDa(final Integer numeroProtocolloDa) {
		this.numeroProtocolloDa = numeroProtocolloDa;
	}

	/** 
	 *
	 * @return the numero protocollo A
	 */
	public Integer getNumeroProtocolloA() {
		return numeroProtocolloA;
	}

	/** 
	 *
	 * @param numeroProtocolloA the new numero protocollo A
	 */
	public void setNumeroProtocolloA(final Integer numeroProtocolloA) {
		this.numeroProtocolloA = numeroProtocolloA;
	}

	/** 
	 *
	 * @return the anno protocollo
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/** 
	 *
	 * @param annoProtocollo the new anno protocollo
	 */
	public void setAnnoProtocollo(final Integer annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/** 
	 *
	 * @return the assegnatario
	 */
	public AssegnatarioOrganigrammaDTO getAssegnatario() {
		return assegnatario;
	}

	/** 
	 *
	 * @param assegnatario the new assegnatario
	 */
	public void setAssegnatario(final AssegnatarioOrganigrammaDTO assegnatario) {
		this.assegnatario = assegnatario;
	}

	/** 
	 *
	 * @return the descrizione assegnatario
	 */
	public String getDescrizioneAssegnatario() {
		return descrizioneAssegnatario;
	}

	/** 
	 *
	 * @param descrizioneAssegnatario the new descrizione assegnatario
	 */
	public void setDescrizioneAssegnatario(final String descrizioneAssegnatario) {
		this.descrizioneAssegnatario = descrizioneAssegnatario;
	}
	
}