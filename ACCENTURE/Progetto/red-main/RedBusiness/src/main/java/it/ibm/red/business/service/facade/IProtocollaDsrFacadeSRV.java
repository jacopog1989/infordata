package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;

import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.HierarchicalFileWrapperDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.persistence.model.Contatto;

/**
 * Facade del servizio di protocollazione Dsr.
 */
public interface IProtocollaDsrFacadeSRV extends Serializable {

	/**
	 * Verifica che il pdf identificato come documento principale di una DSR abbia
	 * le seguenti condizioni.
	 * 
	 * Almeno due campi firma Il primo campo firma valorizzato
	 * 
	 * @param utente
	 * @param guidMailAllegatoPath Path che identifica in maniera univoca l'allegato
	 *                             dell'email
	 * @return L'esito della verifica
	 */
	boolean validateProtocollazioneDSR(UtenteDTO utente, String guidMailAllegatoPath);

	/**
	 * Protocolla Dsr.
	 * 
	 * @param utente
	 * @param detail Fact totum del salvataggio del documento
	 * @return
	 */
	EsitoSalvaDocumentoDTO protocollaMailDsr(UtenteDTO utente, DetailDocumentRedDTO detail);

	/**
	 * Inizializza DetailDocument valorizzandolo con tutti i valori di default
	 * necessari alla protocollazione FEPA.
	 * 
	 * @param utente             utente che ha richiesto l'operazione
	 * @param contact
	 * @param selectedAllegati   Allegati selezionati dall'utente per essere
	 *                           allegati anche al documento (Per il dsr non ci
	 *                           dovrebbero esere allegati olre al docuemnto
	 *                           principale)
	 * @param principale         Il documento principale, deve trattarsi di un dsr,
	 *                           è possibile verificare che il documento rispetti le
	 *                           condizioni per essere un dsr eseguendo
	 *                           validateProtocollazioneDsr
	 * @param notificaProtocolla Richiede se l'operazione deve essere notificata o
	 *                           meno
	 * @param mailGUID           guid dell'email presa in oggetto
	 * @param mailOggetto        Identifica la mail di protocollazione
	 * @return Il detailDocuemntRedDTO utilizzabile per visualizzare l'interfaccia e
	 *         per protocollare l'email
	 */
	DetailDocumentRedDTO createDetailFromMail(UtenteDTO utente, Contatto contact, Collection<HierarchicalFileWrapperDTO> selectedAllegati,
			HierarchicalFileWrapperDTO principale, Boolean notificaProtocolla, String mailGUID, String mailOggetto);

	/**
	 * Ritorna una assegnazione in competenza per l'ufficio competente FEPA. L' id
	 * ufficio è recuperato dalla properties
	 * PropertiesNameEnum.FEPA_UFFICIO_COMPETENTE. Quindi viene recuperata il nodo
	 * con le informazioni necessarie.
	 * 
	 * @param conn Connessione per recuperare la properties dal nodo.
	 * @return l'assegnazione per competenza all'ufficio competente FEPA.
	 */
	AssegnazioneDTO getAssegnazioneDefaultUfficioFepa(Connection conn);

}
