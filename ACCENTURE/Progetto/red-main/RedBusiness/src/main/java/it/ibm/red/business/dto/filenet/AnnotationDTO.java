package it.ibm.red.business.dto.filenet;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;

/**
 * DTO che definisce un'annotation.
 */
public class AnnotationDTO implements Serializable, Comparable<AnnotationDTO> {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -8086905215425911554L;
	
	/**
	 * Descrizione.
	 */
	private String descriptiveText;

	/**
	 * Content.
	 */
	private transient InputStream content;

	/**
	 * Data creazione.
	 */
	private Date dateCreated;

	/**
	 * Costruttore di default.
	 */
	public AnnotationDTO() {
		super();
	}

	/**
	 * @return descriptiveText
	 */
	public String getDescriptiveText() {
		return descriptiveText;
	}

	/**
	 * @param descriptiveText
	 */
	public void setDescriptiveText(final String descriptiveText) {
		this.descriptiveText = descriptiveText;
	}

	/**
	 * @return content
	 */
	public InputStream getContent() {
		return content;
	}

	/**
	 * @param inContent
	 */
	public void setContent(final InputStream inContent) {
		this.content = inContent;
	}

	/**
	 * @return dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}

	/**
	 * @param inDateCreated
	 */
	public void setDateCreated(final Date inDateCreated) {
		this.dateCreated = inDateCreated;
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object).
	 */
	@Override
	public int compareTo(final AnnotationDTO o) {
		int result = 0;
		if (o != null && o.getDateCreated() != null) {
			result = this.getDateCreated().compareTo(o.getDateCreated());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateCreated == null) ? 0 : dateCreated.hashCode());
		result = prime * result + ((descriptiveText == null) ? 0 : descriptiveText.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnnotationDTO other = (AnnotationDTO) obj;
		if (dateCreated == null) {
			if (other.dateCreated != null)
				return false;
		} else if (!dateCreated.equals(other.dateCreated))
			return false;
		if (descriptiveText == null) {
			if (other.descriptiveText != null)
				return false;
		} else if (!descriptiveText.equals(other.descriptiveText))
			return false;
		return true;
	}

}
