package it.ibm.red.business.service.asign.step;

import it.ibm.red.business.service.asign.step.impl.AggiornamentoMetadatiStep;
import it.ibm.red.business.service.asign.step.impl.AllineamentoNPSStep;
import it.ibm.red.business.service.asign.step.impl.AvanzamentoProcessiStep;
import it.ibm.red.business.service.asign.step.impl.GestioneAllacciStep;
import it.ibm.red.business.service.asign.step.impl.InvioMailStep;
import it.ibm.red.business.service.asign.step.impl.ProtocollazioneStep;
import it.ibm.red.business.service.asign.step.impl.RegAuxProtocollazioneStep;
import it.ibm.red.business.service.asign.step.impl.RegAuxRigenerazioneContentStep;
import it.ibm.red.business.service.asign.step.impl.StampigliatureStep;

/**
 * Enum che definisce tutti gli step di firma asincrona.
 * Ogni step si occupa di gestire una parte del processo di firma e gestisce eventuali errori e rollback.
 */
public enum StepEnum {

	/**
	 * Step iniziale, all'insert di un item questo step viene aperto ma non chiuso.
	 */
	START("Avvio", 0, null),
	
	/**
	 * Step di protocollazione, in questo step vengono svolte tutte le attività associate alla protocollazione.
	 */
	PROTOCOLLAZIONE("Protocollazione", 1, ProtocollazioneStep.class),
	
	/**
	 * In questo step viene staccata il numero della registrazione ausiliaria solo se non ne esiste
	 * già uno per il documento.
	 */
	REG_AUX_PROTOCOLLAZIONE("Registrazione Ausiliaria - Protocollazione", 2, RegAuxProtocollazioneStep.class),
	
	/**
	 * In questo step, se è stato staccato un numero per la registrazione ausiliaria e il documento non risulta "Definitivo" allora viene
	 * rigenerato il content del template associato alla registrazione ausiliaria utilizzando i corretti valori.
	 */
	REG_AUX_RIGENERAZIONE_CONTENT("Registrazione Ausiliaria - Rigenerazione Content", 3, RegAuxRigenerazioneContentStep.class),
	
	/**
	 * In questo step vengono svolte tutte le operazioni associate alla stampigliatura.
	 */
	STAMPIGLIATURE("Stampigliature", 4, StampigliatureStep.class),
	
	/**
	 * In questo step vengono aggiornati i metadati del documento.
	 */
	AGGIORNAMENTO_METADATI("Aggiornamento Metadati", 5, AggiornamentoMetadatiStep.class),
	
	/**
	 * In questo step vengono gestiti gli allacci del documento.
	 */
	GESTIONE_ALLACCI("Gestione Allacci", 6, GestioneAllacciStep.class),
	
	/**
	 * In questo step viene eseguito l'avanzamento dei processo eventualemente sia necessario.
	 */
	AVANZAMENTO_PROCESSI("Avanzamento Processi", 7, AvanzamentoProcessiStep.class),
	
	/**
	 * In questo step viene eseguito l'allineamento con NPS.
	 */
	ALLINEAMENTO_NPS("Upload NPS", 8, AllineamentoNPSStep.class),
	
	/**
	 * In questo step viene eseguito l'invio della mail.
	 */
	INVIO_MAIL("Invio Mail", 9, InvioMailStep.class),
	/**
	 * Questo step viene inserito quando tutti gli altri step sono andati a buon fine.
	 * Questo step viene automaticamente chiuso in quanto non deve svolgere alcuna azione.
	 */
	STOP("Stop", 10, null);

	/**
	 * Executor dello step.
	 */
	private Class<? extends IASignStep> executor;

	/**
	 * Id sequenziale degli step.
	 */
	private Integer sequence;
	
	/**
	 * Display name dello step.
	 */
	private String displayName;

	/**
	 * Costruttore completo.
	 * @param inDisplayName
	 * @param s
	 * @param inClazz
	 */
	private StepEnum(String inDisplayName, Integer s, Class<? extends IASignStep> inClazz) {
		sequence = s;
		executor = inClazz;
		displayName = inDisplayName;
	}

	/**
	 * Restituisce il display name dello Step.
	 * @return display name step
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Restituisce l'id dello Step.
	 * @return id step
	 */
	public Integer getSequence() {
		return sequence;
	}

	/**
	 * Restituisce lo step associato all'Id.
	 * @param seq
	 * @return step identificato dall'id
	 */
	public static StepEnum get(Integer seq) {
		StepEnum out = null;
		for (StepEnum s: StepEnum.values()) {
			if (s.getSequence().equals(seq)) {
				out = s;
				break;
			}
		}
		return out;
	}

	/**
	 * Restituisce lo step successivo al currentStep in ingresso.
	 * @param currentStep
	 * @return step successivo
	 */
	public static StepEnum nextStep(StepEnum currentStep) {
		StepEnum out = StepEnum.START;
		if (currentStep != null) {
			out = get(currentStep.getSequence() + 1);
			if (out == null) {
				out = StepEnum.STOP;
			}
		}
		return out;
	}

	/**
	 * Restituisce l'executor.
	 * @return executor
	 */
	public Class<? extends IASignStep> getExecutor() {
		return executor;
	}

}
