package it.ibm.red.business.service.ws.facade;

import java.io.Serializable;

/**
 * 	The Interface IRedWsDocGenFacadeSRV.
 * 
 * 	@author m.crescentini
 *
 * 	Facade servizio per il tracciamento dei documenti creati tramite i web service di Red EVO.
 */
public interface IRedWsDocGenFacadeSRV extends Serializable {

	/**
	 * @param idDocumento
	 * @param codiceFlusso
	 * @param con
	 */
	void insertDocGen(int idDocumento, String codiceFlusso);
	
}
