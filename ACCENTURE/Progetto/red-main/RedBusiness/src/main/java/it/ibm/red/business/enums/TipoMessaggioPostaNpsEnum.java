package it.ibm.red.business.enums;

/**
 * @author m.crescentini
 *
 */
public enum TipoMessaggioPostaNpsEnum {

	/**
	 * Valore.
	 */
	GENERICO_EMAIL,
	
	/**
	 * Valore.
	 */
	GENERICO_FLUSSO,
	
	/**
	 * Valore.
	 */
	SEGNATURA,
	
	/**
	 * Valore.
	 */
	SEGNATURA_CONFERMA_RICEZIONE,
	
	/**
	 * Valore.
	 */
	SEGNATURA_NOTIFICA_ECCEZIONE,
	
	/**
	 * Valore.
	 */
	SEGNATURA_AGGIORNAMENTO_CONFERMA,
	
	/**
	 * Valore.
	 */
	SEGNATURA_ANNULLAMENTO_PROTOCOLLAZIONE,
	
	/**
	 * Valore.
	 */
	PEC_RICEVUTA,
	
	/**
	 * Valore.
	 */
	PEC_ACCETTAZIONE,
	
	/**
	 * Valore.
	 */
	PEC_NON_ACCETTAZIONE,
	
	/**
	 * Valore.
	 */
	PEC_PRESA_IN_CARICO,
	
	/**
	 * Valore.
	 */
	PEC_AVVENUTA_CONSEGNA,
	
	/**
	 * Valore.
	 */
	PEC_POSTA_CERTIFICATA,
	
	/**
	 * Valore.
	 */
	PEC_ERRORE_CONSEGNA,
	
	/**
	 * Valore.
	 */
	PEC_PREAVVISO_ERRORE_CONSEGNA,
	
	/**
	 * Valore.
	 */
	PEC_ANOMALIA_MESSAGGIO,
	
	/**
	 * Valore.
	 */
	PEC_RILEVAZIONE_VIRUS;
	
	/**
	 * Restituisce l'enum associata al nome.
	 * @param nome
	 * @return enum associata a nome
	 */
	public static TipoMessaggioPostaNpsEnum get(final String nome) {
		TipoMessaggioPostaNpsEnum output = null;
		
		for (final TipoMessaggioPostaNpsEnum tipoMessaggioInterop : TipoMessaggioPostaNpsEnum.values()) {
			if (tipoMessaggioInterop.name().equalsIgnoreCase(nome)) {
				output = tipoMessaggioInterop;
				break;
			}
		}
		
		return output;
	}
	
}
