package it.ibm.red.business.service.concrete;

import it.ibm.red.business.dto.AbstractDTO;

/**
 * DTO che definisce una notifica di ricezione integrazione dati.
 */
public class NotificaRicezioneIntegrazioneDatiDTO extends AbstractDTO {
	
	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = -2417119681956937887L;

	/**
	 * Identificarivo documento.
	 */
	private String idDocumento;
	
	/**
	 * Identificativo nodo.
	 */
	private Long idNodo;
	
	/**
	 * Idnetificativo utente.
	 */
	private Long idUtente;
	
	/**
	 * Costruttore.
	 * @param inIdDocumento
	 * @param inIdNodo
	 * @param inIdUtente
	 */
	public NotificaRicezioneIntegrazioneDatiDTO(final String inIdDocumento, final long inIdNodo, final long inIdUtente) {
		this.idDocumento = inIdDocumento;
		this.idNodo = inIdNodo;
		this.idUtente = inIdUtente;
	}

	/**
	 * Costruttore vuoto.
	 */
	public NotificaRicezioneIntegrazioneDatiDTO() {
		super();
	}

	/**
	 * @return the idDocumento
	 */
	public String getIdDocumento() {
		return idDocumento;
	}

	/**
	 * @return the idNodo
	 */
	public Long getIdNodo() {
		return idNodo;
	}

	/**
	 * @return the idUtente
	 */
	public Long getIdUtente() {
		return idUtente;
	}
}
