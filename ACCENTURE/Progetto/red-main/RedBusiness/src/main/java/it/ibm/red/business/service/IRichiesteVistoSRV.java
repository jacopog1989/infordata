package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IRichiesteVistoFacadeSRV;

/**
 * Interfaccia del servizio di gestione richieste visto.
 */
public interface IRichiesteVistoSRV extends IRichiesteVistoFacadeSRV {
	
}
