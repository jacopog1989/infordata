package it.ibm.red.business.service.asign.step;

import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignStepResultDTO;

/**
 * Interfaccia di uno step di firma asincrona.
 */
public interface IASignStep {

	/**
	 * Metodo per simulare un crash sullo step in esecuzione.
	 * 
	 * @param item		item su cui eseguire lo step
	 * @return			info risultanti dall'esecuzione
	 */
	ASignStepResultDTO simulateCrash(ASignItemDTO item);

	/**
	 * Metodo per eseguire uno step su di un item.
	 * 
	 * @param item		item su cui eseguire lo step
	 * @return			info risultanti dall'esecuzione
	 */
	ASignStepResultDTO execute(ASignItemDTO item);
	
}
