package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.SignTypeEnum;

/**
 * Facade per l'implementazione dei servizi di firma.
 * 
 * @author APerquoti
 *
 */
public interface IFirmaRapidaFacadeSRV extends Serializable {
	
	/**
	 * Metodo per pades.
	 * 
	 * @param fcDTO		credenziali filenet
	 * @param idDoc		identificativo documento
	 * @param utente	credenziali utente
	 * @param otp		otp firma
	 * @return			esito operazione
	 * @throws Exception 
	 */
	Map<String, byte[]> pades(FileDTO file, UtenteDTO utente, String otp, SignTypeEnum signType);

	/**
	 * Pades multipla.
	 * 
	 * @param utente	credenziali utente
	 * @param otp		otp
	 * @return			esito operazione
	 * @throws Exception 
	 */
	Map<String, byte[]> pades(Collection<FileDTO> fileDTOs, UtenteDTO utente, String otp, SignTypeEnum signType);

	/**
	 * Metodo per cades.
	 * 
	 * @param fcDTO		credenziali filenet
	 * @param idDoc		identificativo documento
	 * @param utente	credenziali utente
	 * @param otp		otp firma
	 * @return			esito operazione
	 */
	Map<String, byte[]> cades(FileDTO file, UtenteDTO utente, String otp);

	/**
	 * Metodo cades multipla.
	 * 
	 * @param fcDTO		credenziali filenet
	 * @param wobs		identificativi documenti
	 * @param utente	credenziali utente
	 * @param otp		otp
	 * @return			esito operazione
	 * @throws Exception 
	 */
	Map<String, byte[]> cades(Collection<FileDTO> fileDTOs, UtenteDTO utente, String otp);

}
