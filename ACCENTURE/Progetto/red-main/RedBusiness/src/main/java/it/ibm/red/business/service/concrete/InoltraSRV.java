package it.ibm.red.business.service.concrete;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.service.IInoltraSRV;
import it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV;

/**
 * Service inoltra.
 */
@Service
public class InoltraSRV extends AbstractService implements IInoltraSRV {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 1575542012423781112L;
	
	/**
	 * Servizio per la gestione di operazioni sul workflow.
	 */
	@Autowired
	private IOperationWorkFlowFacadeSRV operationWorkflowSRV;
	
	/**
	 * @see it.ibm.red.business.service.facade.IInoltraFacadeSRV#inoltra(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection, java.lang.String).
	 */
	@Override
	public Collection<EsitoOperazioneDTO> inoltra(final UtenteDTO utente, final Collection<String> wobNumbers, final String motivoAssegnazione) {
		Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		
		for (String wobNumber : wobNumbers) {
			esiti.add(inoltra(utente, wobNumber, motivoAssegnazione));
		}
		
		return esiti;
	}
	
	
	/**
	 * Per testare questa response usare P.trimarchi come ufficio creare un
	 * documento per firma al nostro ufficio e inviarlo per visto a un ispettorato
	 * diverso dal proprio Come segreteria dell'ispettorato possiamo eseguire
	 * INOLTRA del documento richiesto per visto.
	 */
	@Override
	public EsitoOperazioneDTO inoltra(final UtenteDTO utente, final String wobNumbers, final String motivoAssegnazione) {
		EsitoOperazioneDTO esito = operationWorkflowSRV.avanzaWorkflowEStornaWfContributi(utente, wobNumbers, motivoAssegnazione, ResponsesRedEnum.INOLTRA);
		if (esito.isEsito()) {
			esito.setNote("Inoltro del documento effettuato con successo.");
		}
		
		return esito;
	}
}
