package it.ibm.red.business.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

import it.ibm.red.business.utils.LogStyleNotNull;

/**
 * JobConfigurationDTO
 * 
 * @author a.dilegge
 *
 */
public class JobConfigurationDTO extends AbstractDTO {
	
	
	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = -959602141381621290L;

	/**
     * Jabo configuration.
     */
	private int idJobConfiguration;

	/**
	 * Nome classe.
	 */
	private String className;

	/**
	 * Nome metodo.
	 */
	private String methodName;

	/**
	 * Aoo.
	 */
	private int idAoo;

	/**
	 * Operazoine.
	 */
	private String operation;

	/**
	 * Stato.
	 */
	private boolean jobState;

	/**
	 * Schedule.
	 */
	private String chronSchedule;

	/**
	 * Trigger.
	 */
	private int triggerNumber;

	/**
	 * Descrizoine job.
	 */
	private String jobDescription;

	/**
	 * Erorre job.
	 */
	private String jobError;

	/**
	 * Server name.
	 */
	private String serverName;
	
	/**
	 * Costruttore del DTO.
	 */
	public JobConfigurationDTO() {
	}

	/**
	 * Costruttore del DTO.
	 * @param idJobConfiguration - id della configurazione del job
	 * @param className - nome della classe
	 * @param methodName - nome del metodo
	 * @param idAoo - id dell'Aoo
	 * @param operation - operazione
	 * @param jobState - stato del job
	 * @param chronSchedule - cronologia di schedulazione
	 * @param triggerNumber - numero del trigger
	 * @param jobDescription - descrizione del job
	 * @param jobError - errore del job
	 * @param serverName - nome del server
	 */
	public JobConfigurationDTO(final int idJobConfiguration, final String className, final String methodName,
			final int idAoo, final String operation, final boolean jobState,
			final String chronSchedule, final int triggerNumber, final String jobDescription,
			final String jobError, final String serverName) {
		super();
		this.idJobConfiguration = idJobConfiguration;
		this.className = className;
		this.methodName = methodName;
		this.idAoo = idAoo;
		this.operation = operation;
		this.jobState = jobState;
		this.chronSchedule = chronSchedule;
		this.triggerNumber = triggerNumber;
		this.jobDescription = jobDescription;
		this.jobError = jobError;
		this.serverName = serverName;
	}
	
	/**
	 * Recupera l'id della configurazione del job.
	 * @return id della configurazione del job
	 */
	public int getIdJobConfiguration() {
		return idJobConfiguration;
	}

	/**
	 * Imposta l'id della configurazione del job.
	 * @param idJobConfiguration id della configurazione del job
	 */
	public void setIdJobConfiguration(final int idJobConfiguration) {
		this.idJobConfiguration = idJobConfiguration;
	}

	/**
	 * Recupera il nome della classe.
	 * @return nome della classe
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * Imposta il nome della classe.
	 * @param className nome della classe
	 */
	public void setClassName(final String className) {
		this.className = className;
	}

	/**
	 * Recupera il nome del metodo.
	 * @return nome del metodo
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * Imposta il nome del metodo.
	 * @param methodName nome del metodo
	 */
	public void setMethodName(final String methodName) {
		this.methodName = methodName;
	}

	/**
	 * @return the idAoo
	 */
	public int getIdAoo() {
		return idAoo;
	}

	/**
	 * @param idAoo the idAoo to set
	 */
	public void setIdAoo(final int idAoo) {
		this.idAoo = idAoo;
	}

	/**
	 * Recupera l'operazione.
	 * @return operazione
	 */
	public String getOperation() {
		return operation;
	}

	/**
	 * Imposta l'operazione.
	 * @param operation operazione
	 */
	public void setOperation(final String operation) {
		this.operation = operation;
	}

	/**
	 * Recupera lo stato del job.
	 * @return stato del job.
	 */
	public boolean isJobState() {
		return jobState;
	}

	/**
	 * Imposta lo stato del job.
	 * @param jobState stato del job
	 */
	public void setJobState(final boolean jobState) {
		this.jobState = jobState;
	}

	/**
	 * Recupera la cronologia di schedulazione.
	 * @return cronologia di schedulazione
	 */
	public String getChronSchedule() {
		return chronSchedule;
	}

	/**
	 * Imposta la cronologia di schedulazione.
	 * @param chronSchedule cronologia di schedulazione
	 */
	public void setChronSchedule(final String chronSchedule) {
		this.chronSchedule = chronSchedule;
	}

	/**
	 * Recupera il numero del trigger.
	 * @return numero del trigger
	 */
	public int getTriggerNumber() {
		return triggerNumber;
	}

	/**
	 * Imposta il numero del trigger.
	 * @param triggerNumber numero del trigger
	 */
	public void setTriggerNumber(final int triggerNumber) {
		this.triggerNumber = triggerNumber;
	}

	/**
	 * Recupera la descrizione del job.
	 * @return descrizione del job
	 */
	public String getJobDescription() {
		return jobDescription;
	}

	/**
	 * imposta la descrizione del job.
	 * @param jobDescription descrizione del job
	 */
	public void setJobDescription(final String jobDescription) {
		this.jobDescription = jobDescription;
	}

	/**
	 * Recupera l'errore del job.
	 * @return errore del job
	 */
	public String getJobError() {
		return jobError;
	}

	/**
	 * Imposta l'errore del job.
	 * @param jobError errore del job
	 */
	public void setJobError(final String jobError) {
		this.jobError = jobError;
	}

	/**
	 * Recupera il nome del server.
	 * @return nome del server
	 */
	public String getServerName() {
		return serverName;
	}

	/**
	 * Imposta il nome del server.
	 * @param serverName nome del server
	 */
	public void setServerName(final String serverName) {
		this.serverName = serverName;
	}

	/**
	 * @see it.ibm.red.business.dto.AbstractDTO#toString().
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, new LogStyleNotNull()); 
	}
}
