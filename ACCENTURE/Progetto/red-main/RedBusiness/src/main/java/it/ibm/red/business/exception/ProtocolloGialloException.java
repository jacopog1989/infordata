package it.ibm.red.business.exception;

/**
 * The Class ProtocolloGialloException.
 *
 * @author VINGENITO
 * 
 *         Eccezione generica dell'applicativo.
 */
public class ProtocolloGialloException extends RuntimeException {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Costruttore.
	 * 
	 * @param e	eccezione
	 */
	public ProtocolloGialloException(final Exception e) {
		super(e);
	}

	/**
	 * Costruttore.
	 * 
	 * @param msg	messaggio
	 */
	public ProtocolloGialloException(final String msg) {
		super(msg);
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param msg	messaggio
	 * @param e		eccezione root
	 */
	public ProtocolloGialloException(final String msg, final Exception e) {
		super(msg, e);
	}
	
}