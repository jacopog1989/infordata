package it.ibm.red.business.dto;

/**
 * Parametri di ricerca estratti conto trimestrali CCVT.
 */
public class ParamsRicercaEstrattiContoDTO extends AbstractDTO {

	/**
	 * serial version UID.
	 */
	private static final long serialVersionUID = 5882280745370624877L;

	/**
	 * Numero esercizio.
	 */
	private Integer esercizio;
	
	/**
	 * Trimestre.
	 */
	private Integer trimestre;
	
	/**
	 * Sede estera selezionata.
	 */
	private String sedeEstera;
	
	/**
	 * @return esercizio
	 */
	public Integer getEsercizio() {
		return esercizio;
	}

	/**
	 * @param esercizio
	 */
	public void setEsercizio(Integer esercizio) {
		this.esercizio = esercizio;
	}

	/**
	 * @return trimestre
	 */
	public Integer getTrimestre() {
		return trimestre;
	}

	/**
	 * @param trimestre
	 */
	public void setTrimestre(Integer trimestre) {
		this.trimestre = trimestre;
	}

	/**
	 * @return sedeEstera
	 */
	public String getSedeEstera() {
		return sedeEstera;
	}

	/**
	 * @param sedeEstera
	 */
	public void setSedeEstera(String sedeEstera) {
		this.sedeEstera = sedeEstera;
	}
}
