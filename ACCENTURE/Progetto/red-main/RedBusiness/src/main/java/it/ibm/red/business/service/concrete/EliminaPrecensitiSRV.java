package it.ibm.red.business.service.concrete;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.provider.ServiceTaskProvider.ServiceTask;
import it.ibm.red.business.service.IEliminaPrecensitiSRV;

/**
 * Service gestione eliminazione precensiti.
 */
@Service
@Component
public class EliminaPrecensitiSRV extends AbstractService implements IEliminaPrecensitiSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 5060739983730350133L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(EliminaPrecensitiSRV.class.getName());

	/**
	 * @see it.ibm.red.business.service.facade.IEliminaPrecensitiFacadeSRV#eliminaPrecensiti(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "eliminaPrecensiti")
	public final Collection<EsitoOperazioneDTO> eliminaPrecensiti(final UtenteDTO utente, final Collection<String> wobNumbers) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		for (final String wobNumber : wobNumbers) {
			esiti.add(eliminaPrecensito(utente, wobNumber));
		}
		return esiti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IEliminaPrecensitiFacadeSRV#eliminaPrecensito(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "eliminaPrecensiti")
	public EsitoOperazioneDTO eliminaPrecensito(final UtenteDTO utente, final String wobNumber) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		Integer numDoc = 0;

		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);
			final Integer idDocumento = (Integer) TrasformerPE.getMetadato(wob, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));

			final Document doc = fceh.getDocumentByIdGestionale(idDocumento.toString(), null, null, utente.getIdAoo().intValue(), null, null);
			numDoc = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);

			fceh.updateMetadataAndDeleteLastVersion(doc, null);
			if (!fpeh.nextStep(wob, null, ResponsesRedEnum.ELIMINA.getResponse())) {
				throw new RedException("Workflow non avanzato");
			}

			esito.setEsito(true);
			esito.setIdDocumento(numDoc);
			esito.setNote("Eliminazione del documento precensito effettuata con successo.");
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'avanzamento del WF per l'operazione EliminaPrecensito: ", e);
			esito.setIdDocumento(numDoc);
			esito.setNote("Errore durante l'avanzamento del WF per l'operazione EliminaPrecensito.");
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}

		return esito;
	}
}
