package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.persistence.model.PkHandler;

/**
 * 
 * @author m.crescentini
 *
 *	DAO per gli handler PK.
 */
public interface IPkHandlerDAO extends Serializable {

	/**
	 * @param id
	 * @param con
	 * @return
	 */
	PkHandler getById(int id, Connection con);
	
	/**
	 * @param url
	 * @param con
	 * @return
	 */
	PkHandler getByHandler(String url, Connection con);
	
	/**
	 * @param con
	 * @return
	 */
	List<String> getAllHandlers(Connection con);
}