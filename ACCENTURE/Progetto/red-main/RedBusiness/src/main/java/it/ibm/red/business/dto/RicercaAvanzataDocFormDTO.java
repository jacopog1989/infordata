package it.ibm.red.business.dto;

import java.util.List;

import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.persistence.model.Legislatura;
import it.ibm.red.business.persistence.model.TipoEvento;

/**
 * DTO che raccoglie le informazioni del form di ricerca avanzata.
 */
public class RicercaAvanzataDocFormDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 6961707405888925540L;

	/**
	 * Tipi documento.
	 */
	private List<SelectItemDTO> comboTipiDocumento;

	/**
	 * Tipo documento selezoinato.
	 */
	private String tipoDocumentoSelected;

	/**
	 * Lista legislature.
	 */
	private List<Legislatura> comboLegislatura;

	/**
	 * Lista tipologie documento.
	 */
	private List<TipologiaDocumentoDTO> comboTipologieDocumento;

	/**
	 * Liste tipologie procedimento.
	 */
	private List<TipoProcedimentoDTO> comboTipiProcedimento;

	/**
	 * Liste mezzi spedizione.
	 */
	private List<MezzoRicezioneDTO> comboMezziRicezione;

	/**
	 * Lista tipo assegnazione.
	 */
	private List<TipoAssegnazioneEnum> comboTipoAssegnazione;

	/**
	 * Flag assegnazione disabilitata.
	 */
	private boolean assegnazioneBtnDisabled;

	/**
	 * Flag assegnatario organigramma visibile.
	 */
	private boolean assegnatarioOrganigrammaVisible;

	/**
	 * Flag responsabile copia conforme visibile.
	 */
	private boolean comboResponsabileCopiaConformeVisible;

	/**
	 * Lista responsabili copia conforme.
	 */
	private List<ResponsabileCopiaConformeDTO> comboResponsabileCopiaConforme;

	/**
	 * Flag ricerca testo.
	 */
	private boolean paroleRicercaTestoDisabled;

	/**
	 * Lista tipi operazione.
	 */
	private List<TipoEvento> comboTipiOperazione;

	/**
	 * Flag assegnazione operazione.
	 */
	private boolean assegnazioneOperazioneBtnDisabled;

	/**
	 * Lista registro repertorio.
	 */
	private List<RegistroRepertorioDTO> comboRegistroRepertorio;

	/*
	 * GET & SET
	 ******************************************************************************************/
	/**
	 * Restituisce la lista degli item associati alle tipologie documenti per la
	 * combobox.
	 * 
	 * @return tipi documento
	 */
	public List<SelectItemDTO> getComboTipiDocumento() {
		return comboTipiDocumento;
	}

	/**
	 * Imposta la lista degli item associati ai tipi documento per la combobox.
	 * 
	 * @param comboTipiDocumento
	 */
	public void setComboTipiDocumento(final List<SelectItemDTO> comboTipiDocumento) {
		this.comboTipiDocumento = comboTipiDocumento;
	}

	/**
	 * Restituisce il tipo documento selezionato.
	 * 
	 * @return tipo documento selezionato
	 */
	public String getTipoDocumentoSelected() {
		return tipoDocumentoSelected;
	}

	/**
	 * Imposta il tipo documento selezionato.
	 * 
	 * @param tipoDocumentoSelected
	 */
	public void setTipoDocumentoSelected(final String tipoDocumentoSelected) {
		this.tipoDocumentoSelected = tipoDocumentoSelected;
	}

	/**
	 * Restituisce la lista delle legislatura per il popolamento della combobox.
	 * 
	 * @return lista legislature
	 */
	public List<Legislatura> getComboLegislatura() {
		return comboLegislatura;
	}

	/**
	 * Imposta la lista delle legislatura per il popolamento della combobox.
	 * 
	 * @param comboLegislatura
	 */
	public void setComboLegislatura(final List<Legislatura> comboLegislatura) {
		this.comboLegislatura = comboLegislatura;
	}

	/**
	 * Restituisce la lista dei tipi documento per la combobox.
	 * 
	 * @return lista delle tipologie dei documenti
	 */
	public List<TipologiaDocumentoDTO> getComboTipologieDocumento() {
		return comboTipologieDocumento;
	}

	/**
	 * Imposta la lista dei tipi documento per la combobox.
	 * 
	 * @param tipologieDocumento
	 */
	public void setComboTipologieDocumento(final List<TipologiaDocumentoDTO> tipologieDocumento) {
		this.comboTipologieDocumento = tipologieDocumento;
	}

	/**
	 * Restituisce il tipo procedimento per la combobox.
	 * 
	 * @return lista dei tipi procedimento
	 */
	public List<TipoProcedimentoDTO> getComboTipiProcedimento() {
		return comboTipiProcedimento;
	}

	/**
	 * Imposta la lista dei tipi procedimento per popolare la combobox.
	 * 
	 * @param comboTipiProcedimento
	 */
	public void setComboTipiProcedimento(final List<TipoProcedimentoDTO> comboTipiProcedimento) {
		this.comboTipiProcedimento = comboTipiProcedimento;
	}

	/**
	 * Restituisce la lista dei mezzi di ricezione per il popolamento della
	 * combobox.
	 * 
	 * @return lista mezzi di ricezione
	 */
	public List<MezzoRicezioneDTO> getComboMezziRicezione() {
		return comboMezziRicezione;
	}

	/**
	 * Imposta la lista dei mezzi di ricezione per il popolamento della combobox.
	 * 
	 * @param comboMezziRicezione
	 */
	public void setComboMezziRicezione(final List<MezzoRicezioneDTO> comboMezziRicezione) {
		this.comboMezziRicezione = comboMezziRicezione;
	}

	/**
	 * Restituisce la lista delle tipologie assegnazione per il popolamento della
	 * combobox.
	 * 
	 * @return lista tipo assegnazioni
	 */
	public List<TipoAssegnazioneEnum> getComboTipoAssegnazione() {
		return comboTipoAssegnazione;
	}

	/**
	 * Imposta la lista delle tipologie assegnazione per il popolamento della
	 * combobox.
	 * 
	 * @param comboTipoAssegnazione
	 */
	public void setComboTipoAssegnazione(final List<TipoAssegnazioneEnum> comboTipoAssegnazione) {
		this.comboTipoAssegnazione = comboTipoAssegnazione;
	}

	/**
	 * Restituisce true se il button per l'assegnazione è disabilitata, false
	 * altrimenti.
	 * 
	 * @return true se il button per l'assegnazione è disabilitata, false altrimenti
	 */
	public boolean isAssegnazioneBtnDisabled() {
		return assegnazioneBtnDisabled;
	}

	/**
	 * Abilita o disabilita il button delle assegnazioni.
	 * 
	 * @param assegnazioneBtnDisabled
	 */
	public void setAssegnazioneBtnDisabled(final boolean assegnazioneBtnDisabled) {
		this.assegnazioneBtnDisabled = assegnazioneBtnDisabled;
	}

	/**
	 * Restituisce true se l'organigramma degli assegnatari è visibile, false
	 * altrimenti.
	 * 
	 * @return true se l'organigramma degli assegnatari è visibile, false altrimenti
	 */
	public boolean isAssegnatarioOrganigrammaVisible() {
		return assegnatarioOrganigrammaVisible;
	}

	/**
	 * Imposta la visibilita dell'organigramma degli assegnatari.
	 * 
	 * @param assegnatarioOrganigrammaVisible
	 */
	public void setAssegnatarioOrganigrammaVisible(final boolean assegnatarioOrganigrammaVisible) {
		this.assegnatarioOrganigrammaVisible = assegnatarioOrganigrammaVisible;
	}

	/**
	 * Restituisce true se la combo dei responsabili copia conforme è visibile,
	 * false altrimenti.
	 * 
	 * @return true se la combo dei responsabili copia conforme è visibile, false
	 *         altrimenti
	 */
	public boolean isComboResponsabileCopiaConformeVisible() {
		return comboResponsabileCopiaConformeVisible;
	}

	/**
	 * Imposta la visibilita della combo dei responsabili copia conforme.
	 * 
	 * @param comboResponsabileCopiaConformeVisible
	 */
	public void setComboResponsabileCopiaConformeVisible(final boolean comboResponsabileCopiaConformeVisible) {
		this.comboResponsabileCopiaConformeVisible = comboResponsabileCopiaConformeVisible;
	}

	/**
	 * Restituisce la lista dei responsabili della copia conforme per la combobox.
	 * 
	 * @return lista responsabili copia conforme
	 */
	public List<ResponsabileCopiaConformeDTO> getComboResponsabileCopiaConforme() {
		return comboResponsabileCopiaConforme;
	}

	/**
	 * Imposta la lista dei responsabili di copia conforme per la combobox.
	 * 
	 * @param comboResponsabileCopiaConforme
	 */
	public void setComboResponsabileCopiaConforme(final List<ResponsabileCopiaConformeDTO> comboResponsabileCopiaConforme) {
		this.comboResponsabileCopiaConforme = comboResponsabileCopiaConforme;
	}

	/**
	 * Restitusce la lista dei tipi di evento per il popolamento della combobox.
	 * 
	 * @return lista tipi di evento
	 */
	public List<TipoEvento> getComboTipiOperazione() {
		return comboTipiOperazione;
	}

	/**
	 * Imposta la lista dei tipi di evento.
	 * 
	 * @param comboTipiOperazione
	 */
	public void setComboTipiOperazione(final List<TipoEvento> comboTipiOperazione) {
		this.comboTipiOperazione = comboTipiOperazione;
	}

	/**
	 * Restituisce true se il button per assegnazione operazione è disabilitato,
	 * false altrimenti.
	 * 
	 * @return true se il button per assegnazione operazione è disabilitato, false
	 *         altrimenti
	 */
	public boolean isAssegnazioneOperazioneBtnDisabled() {
		return assegnazioneOperazioneBtnDisabled;
	}

	/**
	 * @param assegnazioneOperazioneBtnDisabled
	 */
	public void setAssegnazioneOperazioneBtnDisabled(final boolean assegnazioneOperazioneBtnDisabled) {
		this.assegnazioneOperazioneBtnDisabled = assegnazioneOperazioneBtnDisabled;
	}

	/**
	 * @return paroleRicercaTestoDisabled
	 */
	public boolean isParoleRicercaTestoDisabled() {
		return paroleRicercaTestoDisabled;
	}

	/**
	 * @param paroleRicercaTestoDisabled
	 */
	public void setParoleRicercaTestoDisabled(final boolean paroleRicercaTestoDisabled) {
		this.paroleRicercaTestoDisabled = paroleRicercaTestoDisabled;
	}

	/**
	 * Restituisce la lista dei registri di repertorio per il popolamento della
	 * combobox.
	 * 
	 * @return lista registri di repertorio
	 */
	public List<RegistroRepertorioDTO> getComboRegistroRepertorio() {
		return comboRegistroRepertorio;
	}

	/**
	 * Imposta la lista dei registri di repertorio per il popolamento della
	 * combobox.
	 * 
	 * @param comboRegistroRepertorio
	 */
	public void setComboRegistroRepertorio(final List<RegistroRepertorioDTO> comboRegistroRepertorio) {
		this.comboRegistroRepertorio = comboRegistroRepertorio;
	}
}
