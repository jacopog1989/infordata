package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.Date;

import com.filenet.api.core.Document;

import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * Trasformer dal Documento al UpdateDetailRed.
 */
public class FromDocumentoToUpdateDetailRedTrasformer extends TrasformerCE<DetailDocumentRedDTO> {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 8212037904935061830L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentoToUpdateDetailRedTrasformer.class.getName());
	
	/**
	 * Dao per recuperare la tipologia di documento.
	 */
	private ITipologiaDocumentoDAO tipologiaDocumentoDAO;

	/**
	 * Dao per recuperare il tipo procedimento.
	 */
	private ITipoProcedimentoDAO tipoProcedimentoDAO;
	
	/**
	 * Properties.
	 */
	private PropertiesProvider pp;
	
	/**
	 * Costruttore del trasformer.
	 */
	public FromDocumentoToUpdateDetailRedTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_UPDATE_DETAIL_RED);
		tipologiaDocumentoDAO = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoDAO.class);
		tipoProcedimentoDAO = ApplicationContextProvider.getApplicationContext().getBean(ITipoProcedimentoDAO.class);
		pp = PropertiesProvider.getIstance();
	}
	
	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE#trasform(com.filenet.api.core.Document, java.sql.Connection).
	 */
	@Override
	public DetailDocumentRedDTO trasform(final Document documentoFilenet, final Connection connection) {
		DetailDocumentRedDTO outputDetailDocRed = null;
		
		try {
			if (documentoFilenet != null) {
				outputDetailDocRed = new DetailDocumentRedDTO();
				
				// Document title
				outputDetailDocRed.setDocumentTitle((String) getMetadato(documentoFilenet, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
				
				// Tipologia documento -> START
				outputDetailDocRed.setIdTipologiaDocumento((Integer) getMetadato(documentoFilenet, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY));
				if (outputDetailDocRed.getIdTipologiaDocumento() != null) {
					TipologiaDocumentoDTO tipologiaDocumento = tipologiaDocumentoDAO.getById(outputDetailDocRed.getIdTipologiaDocumento(), connection);
					outputDetailDocRed.setDescTipologiaDocumento(tipologiaDocumento.getDescrizione());
					outputDetailDocRed.setDocumentClass(tipologiaDocumento.getDocumentClass());
				}
				// Tipologia documento -> END
				
				// Tipo procedimento -> START
				outputDetailDocRed.setIdTipologiaProcedimento((Integer) getMetadato(documentoFilenet, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY));
				if (outputDetailDocRed.getIdTipologiaProcedimento() != null) {
					TipoProcedimento t = tipoProcedimentoDAO.getTPbyId(connection, outputDetailDocRed.getIdTipologiaProcedimento()); 
					outputDetailDocRed.setDescTipoProcedimento(t.getDescrizione());
				}
				// Tipo procedimento -> END
				
				// Dati protocollo -> START
				outputDetailDocRed.setIdProtocollo((String) getMetadato(documentoFilenet, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY));
				outputDetailDocRed.setNumeroProtocollo((Integer) getMetadato(documentoFilenet, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY));
				outputDetailDocRed.setAnnoProtocollo((Integer) getMetadato(documentoFilenet, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY));
				outputDetailDocRed.setDataProtocollo((Date) getMetadato(documentoFilenet, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY));
				// Dati protocollo -> END
				
				// Data scadenza
				outputDetailDocRed.setDataScadenza((Date) getMetadato(documentoFilenet, PropertiesNameEnum.DATA_SCADENZA_METAKEY));
				
				// Urgente
				Integer urgente = (Integer) getMetadato(documentoFilenet, PropertiesNameEnum.URGENTE_METAKEY);
				outputDetailDocRed.setUrgente(BooleanFlagEnum.SI.getIntValue().equals(urgente));
				
				// Riservato
				outputDetailDocRed.setRiservato((Integer) getMetadato(documentoFilenet, PropertiesNameEnum.RISERVATO_METAKEY));
				
				// ID raccolta FAD
				Integer tipologiaDocAttoDecreto = Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_TIPO_DOC_VALUE));
				Long tipoProcedimentoDocAttoDecretoManuale = Long.parseLong(pp.getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_MANUALE_VALUE));
				if (tipologiaDocAttoDecreto.equals(outputDetailDocRed.getIdTipologiaDocumento())
						&& tipoProcedimentoDocAttoDecretoManuale.equals(Long.valueOf(outputDetailDocRed.getIdTipologiaProcedimento()))) {
					outputDetailDocRed.setIdraccoltaFAD((String) getMetadato(documentoFilenet, PropertiesNameEnum.ID_RACCOLTA_FAD_METAKEY));
				}
				
				//Mittente new filenet
				outputDetailDocRed.setObjectIdContattoMittOnTheFly((String) getMetadato(documentoFilenet, PropertiesNameEnum.OBJECTID_CONTATTO_MITT_METADATO));
				//Destinatario new filenet
				outputDetailDocRed.setObjectIdContattoDestOnTheFly((String) getMetadato(documentoFilenet, PropertiesNameEnum.OBJECTID_CONTATTO_DEST_METADATO));
				//Oggetto documento
				outputDetailDocRed.setOggetto((String) getMetadato(documentoFilenet, PropertiesNameEnum.OGGETTO_METAKEY));
				//Note documento
				outputDetailDocRed.setNote((String) getMetadato(documentoFilenet, PropertiesNameEnum.NOTE_METAKEY)); 
				
			}
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero di un metadato del dettaglio del documento: " + documentoFilenet.get_Id(), e);
		}
		
		return outputDetailDocRed;
	}

}
