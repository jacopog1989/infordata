package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Ruolo;

/**
 * Facade del servizio che gestisce le assegnazioni ufficio.
 */
public interface IAssegnaUfficioFacadeSRV extends Serializable {

	/**
	 * Assegna UCP.
	 * 
	 * @param utente
	 *            utente in sessione
	 * @param wobNumbers
	 *            identificativi documenti
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> assegnaUCP(UtenteDTO utente, Collection<String> wobNumbers);

	/**
	 * Assegna l'ufficio proponente.
	 * 
	 * @param utente
	 *            utente in sessione
	 * @param wobNumbers
	 *            identificativi documenti
	 * @param motivazione
	 *            motivazione dell'assegnamento all'ufficio proponente
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> assegnaUfficioProponente(UtenteDTO utente, Collection<String> wobNumbers,
			String motivazione);

	/**
	 * Assegna UCP.
	 * 
	 * @param utente
	 *            utente in sessione
	 * @param wobNumber
	 *            identificativo del documento
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO assegnaUCP(UtenteDTO utente, String wobNumber);

	/**
	 * Assegna l'ufficio proponente.
	 * 
	 * @param utente
	 *            utente in sessione
	 * @param wobNumber
	 *            identificativo del documento
	 * @param motivazione
	 *            motivazione dell'assegnamento ad ufficio proponente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO assegnaUfficioProponente(UtenteDTO utente, String wobNumber, String motivazione);

	/**
	 * Ottiene l'id nodo Corriere per l'ufficio.
	 * 
	 * @param idUfficio
	 *            - id dell'ufficio
	 * @return nodo Corriere
	 */
	Long getIdNodoCorriere(Long idUfficio);

	/**
	 * Ottiene i nodi dall'id dell'utente e dall'id dell'Aoo.
	 * 
	 * @param idUtente
	 *            - id dell'utente
	 * @param idAOO
	 *            - id dell'Aoo
	 * @return lista di nodi
	 */
	List<Nodo> getNodiFromIdUtenteandIdAOO(Long idUtente, Long idAOO);

	/**
	 * Ottiene i ruoli dall'id dell'utente, dall'id del nodo e dall'id dell'Aoo.
	 * 
	 * @param idUtente
	 *            - id dell'utente
	 * @param idNodo
	 *            - id del nodo
	 * @param idAOO
	 *            - id dell'Aoo
	 * @return lista di ruoli
	 */
	List<Ruolo> getRuoliFromIdNodoAndIdAOO(Long idUtente, Long idNodo, Long idAOO);

	/**
	 * Ottiene la lista di Aoo dall'id dell'utente.
	 * 
	 * @param idUtente
	 *            - id dell'utente
	 * @return lista di Aoo
	 */
	List<Aoo> getAOOFromIdUtente(Long idUtente);

	/**
	 * Ottiene il nodo utente ruolo.
	 * 
	 * @param utente
	 *            utente in sessione
	 * @param ufficioUtente
	 *            - ufficio dell'utente
	 * @param ruoloUtente
	 *            - ruolo dell'utente
	 */
	void popolaUtente(UtenteDTO utente, Nodo ufficioUtente, Ruolo ruoloUtente);

	/**
	 * Imposta l'utente.
	 * 
	 * @param utente
	 *            utente in sessione
	 * @param ufficioUtente
	 *            - ufficio dell'utente
	 * @param ruoloUtente
	 *            - ruolo dell'utente
	 * @param gestioneApplicativa
	 *            true se l'utente è di gestione applicativa, false altrimenti
	 */
	void popolaUtente(UtenteDTO utente, Nodo ufficioUtente, Ruolo ruoloUtente, boolean gestioneApplicativa);

	/**
	 * Ottiene il nodo dall'id.
	 * 
	 * @param idNodo
	 *            identificativo ufficio
	 * @return nodo informazioni su ufficio
	 */
	Nodo getNodoById(Long idNodo);
}