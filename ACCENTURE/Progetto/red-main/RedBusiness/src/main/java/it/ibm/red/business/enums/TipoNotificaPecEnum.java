package it.ibm.red.business.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * Enum che definisce i tipi notifica PEC.
 */
public enum TipoNotificaPecEnum {
	
	/**
	 * Valore.
	 */
	POSTA_CERTIFICATA("posta-certificata", null, "POSTA CERTIFICATA", "NON PREVISTA"), // Busta di trasporto
	
	/**
	 * Valore.
	 */
	ACCETTAZIONE("accettazione", "AccettazionePEC", "ACCETTAZIONE", "ACCETTAZIONE"),
	
	/**
	 * Valore.
	 */
	NON_ACCETTAZIONE("non-accettazione", "NONAccettazionePEC", "AVVISO DI NON ACCETTAZIONE", "MANCATA ACCETTAZIONE"),
	
	/**
	 * Valore.
	 */
	NON_ACCETTAZIONE_VIRUS("non-accettazione", null, "AVVISO DI NON ACCETTAZIONE PER VIRUS", "MANCATA ACCETTAZIONE"),
	
	/**
	 * Valore.
	 */
	PRESA_IN_CARICO("presa-in-carico", null, "PRESA IN CARICO", "PRESA IN CARICO"),
	
	/**
	 * Valore.
	 */
	AVVENUTA_CONSEGNA("avvenuta-consegna", "ConsegnaPEC", "CONSEGNA", "CONSEGNA"),
	
	/**
	 * Valore.
	 */
	ERRORE_CONSEGNA("errore-consegna", "NONConsegnaPEC", "AVVISO DI MANCATA CONSEGNA", TipoNotificaPecEnum.MANCATA_CONSEGNA),
	
	/**
	 * Valore.
	 */
	ERRORE_CONSEGNA_VIRUS("errore-consegna", null, "AVVISO DI MANCATA CONSEGNA PER VIRUS", TipoNotificaPecEnum.MANCATA_CONSEGNA),
	
	/**
	 * Valore.
	 */
	PREAVVISO_ERRORE_CONSEGNA("preavviso-errore-consegna", "NONConsegnaPEC", "AVVISO DI MANCATA CONSEGNA PER SUP. TEMPO MASSIMO", TipoNotificaPecEnum.MANCATA_CONSEGNA),
		
	/**
	 * Valore.
	 */
	RILEVAZIONE_VIRUS("rilevazione-virus", null, "PROBLEMA DI SICUREZZA", "NON PREVISTA");
	
	private static final String MANCATA_CONSEGNA = "MANCATA CONSEGNA";
	
	
	/**
	 * Nome ricevuta.
	 */
	private String nomeRicevuta;
	

	/**
	 * Nome FNet.
	 */
	private String nomeFilenet;
	

	/**
	 * Prefisso oggetto.
	 */
	private String prefissoOggetto;
	

	/**
	 * Stato notifica email.
	 */
	private String statoNotificaEmail;
	

	/**
	 * @param nome
	 */
	TipoNotificaPecEnum(final String nomeRicevuta, final String nomeFilenet, final String prefissoOggetto, final String statoNotificaEmail) {
		this.nomeRicevuta = nomeRicevuta;
		this.nomeFilenet = nomeFilenet;
		this.prefissoOggetto = prefissoOggetto;
		this.statoNotificaEmail = statoNotificaEmail;
	}


	/**
	 * @return the nome
	 */
	public String getNomeRicevuta() {
		return nomeRicevuta;
	}
	
	
	/**
	 * @return the prefisso
	 */
	public String getNomeFilenet() {
		return nomeFilenet;
	}


	/**
	 * @return the prefissiOggetto
	 */
	public String getPrefissoOggetto() {
		return prefissoOggetto;
	}
	
	
	/**
	 * @return the statoNotificaEmail
	 */
	public String getStatoNotificaEmail() {
		return statoNotificaEmail;
	}


	/**
	 * @param nome
	 * @return
	 */
	public static TipoNotificaPecEnum get(final String nome) {
		TipoNotificaPecEnum output = null;
		
		for (final TipoNotificaPecEnum tipoNotificaPec : TipoNotificaPecEnum.values()) {
			if (tipoNotificaPec.name().equalsIgnoreCase(nome)) {
				output = tipoNotificaPec;
				break;
			}
		}
		
		return output;
	}
	
	
	/**
	 * @param tipoNotificaPec
	 * @param stringaDaControllare
	 * @return
	 */
	public static boolean checkPrefissoNotificaPec(final String stringaDaControllare, final TipoNotificaPecEnum tipoNotificaPec) {
		boolean isNotificaPec = false;
		
		if (StringUtils.isNotBlank(stringaDaControllare) 
				&& stringaDaControllare.startsWith(tipoNotificaPec.getPrefissoOggetto())) {
			isNotificaPec = true;
		}
		
		return isNotificaPec;
	}
	
	
	/**
	 * @param stringaDaControllare
	 * @param tipoNotifichePec
	 * @return
	 */
	public static boolean checkPrefissoNotifichePec(final String stringaDaControllare, final TipoNotificaPecEnum... tipoNotifichePec) {
		boolean isNotificaPec = false;
		
		for (final TipoNotificaPecEnum tipoNotificaPec : tipoNotifichePec) {
			isNotificaPec = checkPrefissoNotificaPec(stringaDaControllare, tipoNotificaPec);
			
			if (isNotificaPec) {
				break;
			}
		}
		
		return isNotificaPec;
	}
	
	/**
	 * Restituisce l'enum associata alla stringa in ingresso.
	 * @param stringaDaControllare
	 * @return enum associata alla stringa
	 */
	public static String getStatoNotificaEmailByPrefisso(final String stringaDaControllare) {
		String statoNotificaEmail = null;
		
		for (final TipoNotificaPecEnum tipoNotificaPec : TipoNotificaPecEnum.values()) {
			if (checkPrefissoNotificaPec(stringaDaControllare, tipoNotificaPec)) {
				statoNotificaEmail = tipoNotificaPec.getStatoNotificaEmail();
				break;
			}
		}
		
		return statoNotificaEmail;
	}
}
