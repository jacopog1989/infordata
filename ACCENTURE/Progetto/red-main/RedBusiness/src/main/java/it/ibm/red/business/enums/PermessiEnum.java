package it.ibm.red.business.enums;

/**
 * The Enum PermessiEnum.
 *
 * @author CPIERASC
 * 
 *         Enum dei permessi associati ai vari ruoli.
 */
public enum PermessiEnum {
	/**
	 *  Protocollo in entrata
	 */
	PROTOCOLLO_IN_ENTRATA(1L),
	/**
	 *  PERMESSO_PRECENSIMENTO (SUL DB: 2,Precensimento)
	 */
	PRECENSIMENTO(2L),
	/**
	 * Permesso AOO protocollazione multipla
	 */
	AOO_PROTOCOLLAZIONE_MULTIPLA_IN(2L),
	/**
	 * Permesso Coda Corriere.
	 */
	CODA_CORRIERE(4L), 
	/**
	 * Visibilita  scheda organigramma
	 */
	VISIBILITA_SCHEDA_ORGANIGRAMMA(8L),
	/**
	 * Permesso di sigla.
	 */
	SIGLA(16L), 
	/**
	 * Permesso di visto.
	 */
	VISTO(32L),
	/**
	 * Permesso di firma.
	 */
	FIRMA(64L), 
	/**
	 * Gestione dei registri riservati di AOO.
	 */
	REPORT(128L),
	/**
	 * Mozione
	 */
	MOZIONE(256L),
	/**
	 * Spedizione
	 */
	SPEDIZIONE(512L),
	/**
	 * Annullamento protocollo
	 */
	ANNULLAMENTO_PROTOCOLLO(1024L),
		
	/**
	 * Amministratore/Gestione di AOO.
	 */
	GESTIONE_AOO(2048L),
	
	/**
	 * permesso firma copia conforme 
	 */
	AOO_FIRMA_COPIA_CONFORME(4096L),
	
	/**
	 * Gestione Applicazione
	 */
	GESTIONE_APPLICAZIONE(8192L),
	/**
	 * Gestione Processi
	 */
	GESTIONE_PROCESSI(16384L),
	/**
	 * Calendario
	 */
	CALENDARIO(32768L),
		
	/**
	 * Permesso AOO corte dei conti 
	 */
	AOO_CORTE_DEI_CONTI(65536L),
	
	
	/**
	 * Gestione Elenchi Trasmissione
	 */
	GESTIONE_ELENCHI_TRASMISSIONE(131072L),

	/**
	 * Permesso di delega.
	 */
	DELEGA(262144L),
	
	/**
	 * Accesso coda in Spedizione.
	 */
	ACCESSO_CODA_IN_SPEDIZIONE(524288L),
	
	/**
	 * permesso RIATTIVA PROCEDIMENTO
	 */
	RIATTIVA_PROCEDIMENTO(1048576L),
	
	/**
	 * Associazione Decreto Dirigenziale
	 */
	ASSOCIAZIONE_DECRETO_DIRIGENZIALE(2097152L),
	
	
	/**
	 * Permesso Dirigente FEPA
	 */
	PERMESSO_DIRIGENTE_FEPA(4194304L),
	
	/**
	 * Permesso Fattura Sogei
	 */
	PERMESSO_FATTURA_SOGEI(8388608L),
	
	/**
	 * TODO:TROVARE VALORE E INSERIRE PARAMETRO
	 */
	PERMESSO_FATTURA_NON_SOGEI(0L),
	/**
	 * TODO:TROVARE VALORE E INSERIRE PARAMETRO
	 */
	PERMESSO_FATTURA_MISTO(0L),
	/**
	 * Ricerca Autorizzazione Alla Spesa
	 */
	RICERCA_AUTORIZZAZIONE_ALLA_SPESA(16777216L),
	
	/**
	 * Registro Protocollo
	 */
	REGISTRO_PROTOCOLLO(33554432L),
	
	/**
	 * Permesso DSR
	 */
	DSR(67108864L),
	
	/**
	 * Gestione Rubrica
	 */
	GESTIONE_RUBRICA(134217728L),
	
	/**
	 * Visibilità tab scadenzario.
	 */
	SCADENZARIO_TAB(536870912L),
	
	/**
	 * Ricerca_Nps
	 */
	RICERCA_NPS(1073741824L),
	
	/**
	 * Annulla Protocollo Nps.
	 */
	ANNULLA_PROTOCOLLO_NPS(2147483648L),
	
	/**
	 * Responsabile Copia Conforme.
	 */
	RESPONSABILE_COPIA_CONFORME(4294967296L),
	
	/**
	 * Contributo esterno
	 */
	CONTRIBUTO_ESTERNO(8589934592L),
	
	/**
	 * LibroFirmaMobile
	 */
	LIBRO_FIRMA_MOBILE(17179869184L),
	
	/**
	 * Cancellazione Note
	 */
	CANCELLAZIONE_NOTE(68719476736L),
	
	/**
	 * Dashboard
	 * **/
	DASHBOARD(137438953472L),
	
	/**
	 * Assegnazione Deleghe.
	 */
	ASSEGNAZIONE_DELEGHE(274877906944L),
	
	/**
	 * Ricerca riservato.
	 */
	RICERCA_RISERVATO(549755813888L),
	
	/**
	 * Ricerca registrazioni ausiliarie.
	 */
	RICERCA_REGISTRAZIONI_AUSILIARIE(1099511627776L),
	
	/**
	 * Wizard ucb
	 */
	WIZARD_UCB(2199023255552L),
	
	/**
	 * Ricerca NPS senza ACL
	 */
	RICERCANPSSENZAACL(4398046511104L),
	
	/**
	 * Modifica metadati minimi
	 */
	MODIFICA_METADATI_MINIMI(8796093022208L);
	
	
	/**
	 * Identificativo.
	 */
	private Long id;

	/**
	 * Costruttore.
	 * 
	 * @param inId	identificativo
	 */
	PermessiEnum(final Long inId) {
		this.id = inId;
	}
	
	/**
	 * Getter identificativo.
	 * 
	 * @return	identificativo
	 */
	public Long getId() {
		return id;
	}
	 
}
