package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import it.ibm.red.business.logger.REDLogger;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.collection.PageIterator;
import com.filenet.api.core.Document;
import com.filenet.api.exception.EngineRuntimeException;
import com.filenet.api.property.Property;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * The Class TrasformerCE.
 *
 * @author CPIERASC
 * 
 *         Oggetto per la trasformazione di un documento presente nel CE.
 * @param <T>
 *            the generic type
 */
public abstract class TrasformerCE<T> implements Serializable {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TrasformerCE.class.getName());
	
	/**
	 * Enum che rappresenta la trasformazione CE.
	 */
	private TrasformerCEEnum enumKey;
	
	/**
	 * Costruttore.
	 * 
	 * @param inEnumKey	enum
	 */
	protected TrasformerCE(final TrasformerCEEnum inEnumKey) {
		enumKey = inEnumKey;
	}

	/**
	 * Metodo per il recupero di un metadato sottoforma di oggetto.
	 * 
	 * @param document		documento sorgente
	 * @param propertyEnum	enum della proprietà da estrapolare
	 * @return				prorpietà sottoforma di oggetto
	 */
	public static final Object getMetadato(final Document document, final PropertiesNameEnum propertyEnum) {
		Object metadato = getMetadato(document, PropertiesProvider.getIstance().getParameterByKey(propertyEnum));
		
		if (PropertiesNameEnum.URGENTE_METAKEY.equals(propertyEnum) && metadato instanceof Boolean) {
			if (!Boolean.TRUE.equals(metadato)) {
				metadato = Integer.valueOf(0);
			} else {
				metadato = Integer.valueOf(1);
			}
		}
		
		return metadato;
	}
	
	/**
	 * Metodo per il recupero di un metadato sottoforma di oggetto.
	 * 
	 * @param document		documento sorgente
	 * @param prop			nome della proprietà da estrapolare
	 * @return				proprietà sottoforma di oggetto
	 */
	public static final Object getMetadato(final Document document, final String prop) {
		Object output = null;
		try {
			Property metadato = document.getProperties().get(prop);
			output = metadato.getObjectValue();
		} catch (EngineRuntimeException e) {
			LOGGER.info("[CE]" + e);
			LOGGER.info("[CE] PROPRIETA' '" + prop + "' NON TROVATA");
		}
		return output;
	}
	
	
	/**
	 * Metodo per il recupero di una proprietà di un oggetto sotto forma di stringa.
	 * 
	 * @param object		oggetto sorgente
	 * @param fieldName		nome del campo
	 * @return				stringa che rappresenta il campo
	 * @throws VWException	eccezione generata in fase di estrazione del valore
	 */
	protected final String getDataFieldValue(final VWWorkObject object, final String fieldName) {
		return object.getDataField(fieldName).getStringValue();
	}
	
	/**
	 * Metodo per il recupero dell'enum caratteristica della trasformazione.
	 * 	
	 * @return	enum
	 */
	public final TrasformerCEEnum getEnumKey() {
		return enumKey;
	}
	
	/**
	 * Metodo per la trasformazione di un documento sorgente in un tipo destinazione.
	 * 
	 * @param document	documento sorgente
	 * @param connection - Connessione al DB
	 * @return			oggetto di destinazione
	 */
	public abstract T trasform(Document document, Connection connection);
	
	/**
	 * Metodo per la trasformazione di un insieme di documenti in una collezione di oggetti.
	 * 
	 * @param documentSet	insieme di documenti da trasformare
	 * @param connection - Connessione al DB
	 * @return				insieme di oggetti trasformati
	 */
	public final Collection<T> trasform(final DocumentSet documentSet, final Connection connection) {
		try {
			Collection<T> output = new ArrayList<>();
			Iterator<?> it = documentSet.iterator();
			while (it.hasNext()) {
				Document document = (Document) it.next();
				T obj = trasform(document, connection);
				if (obj != null) {
					output.add(obj);
				}
			}
			return output;
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero delle informazioni del documento da FileNet (CE). ", e);
			throw new FilenetException(e);
		}
	}
	
	/**
	 * Esegue la logica di trasvormazione dei documenti nella pagina.
	 * @param pi
	 * @param connection
	 * @return collection
	 */
	public final Collection<T> trasform(final PageIterator pi, final Connection connection) {
		try {
			Collection<T> output = new ArrayList<>();
			if (pi != null && pi.nextPage()) {
				for (Object document : pi.getCurrentPage()) {
					Document doc = (Document) document;
					T obj = trasform(doc, connection);
					if (obj != null) {
						output.add(obj);
					}
				}
			}
			return output;
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero delle informazioni del documento da FileNet (CE). ", e);
			throw new FilenetException(e);
		}
	}
	
	/**
	 * Imposta la key dell'enum.
	 * @param inEnumKey
	 */
	protected void setEnumKey(final TrasformerCEEnum inEnumKey) {
		this.enumKey = inEnumKey;
	}
}
