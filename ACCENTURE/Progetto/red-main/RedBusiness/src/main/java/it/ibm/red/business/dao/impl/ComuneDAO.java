package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IComuneDAO;
import it.ibm.red.business.dto.ComuneDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * DAO gestione comuni.
 */
@Repository
public class ComuneDAO extends AbstractDAO implements IComuneDAO {

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(ComuneDAO.class);

	/**
	 * UID.
	 */
	private static final long serialVersionUID = -2140833616716097017L;

	/**
	 * @see it.ibm.red.business.dao.IComuneDAO#getAll(java.sql.Connection).
	 */
	@Override
	public Collection<ComuneDTO> getAll(final Connection conn) {
		final PreparedStatement ps = null;
		final ResultSet rs = null;
		final String sql = "SELECT * FROM comune";
		return populateComuni(conn, ps, rs, sql);

	}

	/**
	 * @see it.ibm.red.business.dao.IComuneDAO#get(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public ComuneDTO get(final String idComune, final Connection conn) {
		final PreparedStatement ps = null;
		final ResultSet rs = null;
		final String sql = "SELECT * FROM comune WHERE IDCOMUNEISTAT = " + sanitize(idComune) + " ";
		logger.info(" ComuneDAO - get : " + idComune);
		logger.info(sql);
		return populateComune(conn, ps, rs, sql);
	}

	/**
	 * @see it.ibm.red.business.dao.IComuneDAO#getComuni(java.lang.Long,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public List<ComuneDTO> getComuni(final Long idProvincia, final String query, final Connection conn) {
		final PreparedStatement ps = null;
		final ResultSet rs = null;
		final String sql = "SELECT * FROM comune WHERE IDPROVINCIAISTAT = " + sanitize(idProvincia) + " AND lower (DENOMINAZIONE) like "
				+ sanitize("%" + query.toLowerCase() + "%");
		return (List<ComuneDTO>) populateComuni(conn, ps, rs, sql);
	}

	/**
	 * @see it.ibm.red.business.dao.IComuneDAO#getComuneByNome(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public ComuneDTO getComuneByNome(final String nomeComune, final Connection conn) {
		final PreparedStatement ps = null;
		final ResultSet rs = null;
		final String sql = "SELECT * FROM comune WHERE DENOMINAZIONE = " + sanitize(nomeComune) + " ";
		logger.info(" ComuneDAO - getComuneByNome : " + nomeComune);
		logger.info(sql);
		return populateComune(conn, ps, rs, sql);
	}

	private ComuneDTO populateComune(final Connection conn, PreparedStatement ps, ResultSet rs, final String sql) {
		ComuneDTO comune = null;
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				comune = new ComuneDTO();
				comune.setIdComune(rs.getString("IDCOMUNEISTAT"));
				comune.setDenominazione(rs.getString("DENOMINAZIONE"));
				comune.setSoppresso(rs.getInt("SOPPRESSO"));
				comune.setIdProvincia(rs.getString("IDPROVINCIAISTAT"));
			}

		} catch (final SQLException e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		if (comune != null) {
			logger.info(comune.toString());
		}
		return comune;
	}

	private static Collection<ComuneDTO> populateComuni(final Connection conn, PreparedStatement ps, ResultSet rs, final String sql) {
		final Collection<ComuneDTO> comuni = new ArrayList<>();
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				final ComuneDTO cTemp = new ComuneDTO();
				cTemp.setIdComune(rs.getString("IDCOMUNEISTAT"));
				cTemp.setDenominazione(rs.getString("DENOMINAZIONE"));
				cTemp.setSoppresso(rs.getInt("SOPPRESSO"));
				cTemp.setIdProvincia(rs.getString("IDPROVINCIAISTAT"));
				comuni.add(cTemp);

			}

		} catch (final SQLException e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return comuni;
	}
}
