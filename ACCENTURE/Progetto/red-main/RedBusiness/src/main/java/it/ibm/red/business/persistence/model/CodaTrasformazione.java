package it.ibm.red.business.persistence.model;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import it.ibm.red.business.utils.LogStyleNotNull;

/**
 * Model della CODATRASFORMAZIONE.
 * 
 * @author a.dilegge
 *
 */
public class CodaTrasformazione implements Serializable {

	private static final long serialVersionUID = 1947962400860503249L;

	/**
	 * Identificativo coda.
	 */
	private int idCoda;
	
	/**
	 * Identificativo aoo.
	 */
	private int idAoo;
	
	/**
	 * Identificativo documento.
	 */
	private String idDocumento;

	/**
	 * Processo chiamante.
	 */
	private String processoChiamante;

	/**
	 * Identificativo tipo trasformazione.
	 */
	private int tipoTrasformazione;

	/**
	 * Identificativo stato.
	 */
	private int stato;

	/**
	 * Priorità.
	 */
	private int prioritaria;

	/**
	 * Data inserimento.
	 */
	private Date dataInserimento;

	/**
	 * Messaggio errore.
	 */
	private String messaggioErrore;

	/**
	 * Parametri.
	 */
	private byte[] parametri;

	/**
	 * Costruttore di default.
	 */
	public CodaTrasformazione() {
		super();
	}

	/**
	 * Costruttore completo della classe.
	 * @param idCoda
* 	 * @param idAoo
	 * @param idDocumento
	 * @param processoChiamante
	 * @param tipoTrasformazione
	 * @param stato
	 * @param prioritaria
	 * @param dataInserimento
	 * @param messaggioErrore
	 * @param parametri
	 */
	public CodaTrasformazione(final int idCoda, final int idAoo, final String idDocumento, final String processoChiamante, final int tipoTrasformazione,
			final int stato, final int prioritaria, final Date dataInserimento, final String messaggioErrore, final byte[] parametri) {
		this();
		this.idCoda = idCoda;
		this.idAoo = idAoo;
		this.idDocumento = idDocumento;
		this.processoChiamante = processoChiamante;
		this.tipoTrasformazione = tipoTrasformazione;
		this.stato = stato;
		this.prioritaria = prioritaria;
		this.dataInserimento = dataInserimento;
		this.messaggioErrore = messaggioErrore;
		this.parametri = parametri;
	}

	/**
	 * Restituisce l'id della coda.
	 * @return idCoda
	 */
	public int getIdCoda() {
		return idCoda;
	}

	/**
	 * Imposta l'id della coda.
	 * @param idCoda
	 */
	public void setIdCoda(final int idCoda) {
		this.idCoda = idCoda;
	}
	
	/**
	 * Restituisce l'id dell'Aoo.
	 * @return idAoo
	 */
	public int getIdAoo() {
		return idAoo;
	}
	
	/**
	 * Imposta l'id dell'Aoo.
	 * @param idAoo
	 */
	public void setIdAoo(final int idAoo) {
		this.idAoo = idAoo;
	}
	
	/**
	 * Restituisce l'id del documento.
	 * @return idDocumento
	 */
	public String getIdDocumento() {
		return idDocumento;
	}

	/**
	 * Imposta l'id del documento.
	 * @param idDocumento
	 */
	public void setIdDocumento(final String idDocumento) {
		this.idDocumento = idDocumento;
	}

	/**
	 * Restituisce il processo chimante come String.
	 * @return processoChiamante
	 */
	public String getProcessoChiamante() {
		return processoChiamante;
	}

	/**
	 * Imposta il processo chiamante.
	 * @param processoChiamante
	 */
	public void setProcessoChiamante(final String processoChiamante) {
		this.processoChiamante = processoChiamante;
	}

	/**
	 * Restituisce il tipo trasformazione.
	 * @return tipoTrasformazione
	 */
	public int getTipoTrasformazione() {
		return tipoTrasformazione;
	}

	/**
	 * Imposta il tipo trasformazione.
	 * @param tipoTrasformazione
	 */
	public void setTipoTrasformazione(final int tipoTrasformazione) {
		this.tipoTrasformazione = tipoTrasformazione;
	}

	/**
	 * Restituisce lo stato definito da un intero.
	 * @return stato
	 */
	public int getStato() {
		return stato;
	}

	/**
	 * Imposta lo stato.
	 * @param stato
	 */
	public void setStato(final int stato) {
		this.stato = stato;
	}

	/**
	 * Restituisce la data di avvenuto inserimento.
	 * @return dataInserimento
	 */
	public Date getDataInserimento() {
		return dataInserimento;
	}

	/**
	 * Imposta la data di avvenuto inserimento.
	 * @param dataInserimento
	 */
	public void setDataInserimento(final Date dataInserimento) {
		this.dataInserimento = dataInserimento;
	}

	/**
	 * Restituisce il messaggio di errore.
	 * @return messaggioErrore
	 */
	public String getMessaggioErrore() {
		return messaggioErrore;
	}

	/**
	 * Imposta il messaggio di errore.
	 * @param messaggioErrore
	 */
	public void setMessaggioErrore(final String messaggioErrore) {
		this.messaggioErrore = messaggioErrore;
	}

	/**
	 * Restituisce i parametri come array di byte.
	 * @return parametri
	 */
	public byte[] getParametri() {
		return parametri;
	}

	/**
	 * Imposta i parametri.
	 * @param parametri
	 */
	public void setParametri(final byte[] parametri) {
		this.parametri = parametri;
	}

	/**
	 * Getter prioritaria.
	 * @return prioritaria
	 */
	public int getPrioritaria() {
		return prioritaria;
	}

	/**
	 * Setter prioritaria.
	 * @param prioritaria
	 */
	public void setPrioritaria(final int prioritaria) {
		this.prioritaria = prioritaria;
	}

	/**
	 * @see java.lang.Object#toString().
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, new LogStyleNotNull()); 
	}

}
