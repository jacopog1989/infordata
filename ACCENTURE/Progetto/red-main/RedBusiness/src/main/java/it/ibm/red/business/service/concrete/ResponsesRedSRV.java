/**
 * 
 */
package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.Document;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.TipoDestinatario;
import it.ibm.red.business.dao.IAllaccioDAO;
import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.IGestioneNotificheEmailDAO;
import it.ibm.red.business.dao.IRegistrazioniAusiliarieDAO;
import it.ibm.red.business.dao.IRegistroAusiliarioDAO;
import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.PEDocumentoDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaDTO;
import it.ibm.red.business.dto.RegistroDTO;
import it.ibm.red.business.dto.ResponsesDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ConfigurazioneFirmaAooEnum;
import it.ibm.red.business.enums.ContextTrasformerPEEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.ExecutionTypeEnum;
import it.ibm.red.business.enums.PermessiAOOEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.enums.TipoRegistroAusiliarioEnum;
import it.ibm.red.business.enums.TipoSpedizioneDocumentoEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.TrasformPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.CodaEmail;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IASignSRV;
import it.ibm.red.business.service.IContributoSRV;
import it.ibm.red.business.service.IListaDocumentiSRV;
import it.ibm.red.business.service.IResponseRedSRV;
import it.ibm.red.business.service.ISiebelSRV;
import it.ibm.red.business.service.IStoricoSRV;
import it.ibm.red.business.service.ITracciaDocumentiSRV;
import it.ibm.red.business.service.asign.step.StatusEnum;
import it.ibm.red.business.service.flusso.IAzioniFlussoBaseSRV;
import it.ibm.red.business.service.flusso.IAzioniFlussoSRV;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * @author APerquoti
 *
 */
@Service
public class ResponsesRedSRV extends AbstractService implements IResponseRedSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -3891338031630080762L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ResponsesRedSRV.class.getName());

	/**
	 * Messaggio errore pulizia response per documento. Occorre appendere il
	 * document title del documento di riferimento.
	 */
	private static final String ERROR_PULIZIA_RESPONSE_DOCUMENTO_MSG = "Errore durante la pulizia delle response per il documento con DocumentTitle :( ";

	/**
	 * Messaggio errore puliza response per i master. Occorre appendere gl id dei
	 * master al messaggio.
	 */
	private static final String ERROR_PULIZIA_RESPONSE_MASTER_MSG = "Errore durante la pulizia delle response per i Master selezionati : ";

	/**
	 * Messaggio.
	 */
	private static final String RICHIEDI_ULTERIORI_CONTRIBUTI = "Richiedi ulteriori contributi";

	/**
	 * Recall.
	 */
	private static final String RECALL = "recall";

	/**
	 * Service per la gestione dello storico di un documento.
	 */
	@Autowired
	private IStoricoSRV storicoSRV;

	/**
	 * Service per la gestione delle sottoscrizioni.
	 */
	@Autowired
	private ITracciaDocumentiSRV tracciaDocumentiSRV;

	/**
	 * Service per la gestione dei contributi.
	 */
	@Autowired
	private IContributoSRV contributoSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ISiebelSRV siebelSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IAzioniFlussoSRV azioniFlussoSRV;

	/**
	 * DAO per la gestione delle notifiche email.
	 */
	@Autowired
	private IGestioneNotificheEmailDAO gneDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IAllaccioDAO allaccioDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IRegistrazioniAusiliarieDAO registrazioneAusiliariaDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IRegistroAusiliarioDAO registroAusiliarioDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipologiaDocumentoDAO tipologiaDocumentoDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IListaDocumentiSRV listaDocumentiSRV;	
	
	/**
	 * Servizio firma asincrona.
	 */
	@Autowired
	private IASignSRV asyncSignSRV;

	/**
	 * @see it.ibm.red.business.service.facade.IResponseRedFacadeSRV#refineReponsesSingle(it.ibm.red.business.dto.MasterDocumentRedDTO,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.Boolean).
	 */
	@Override
	public ResponsesDTO refineReponsesSingle(final MasterDocumentRedDTO m, final UtenteDTO u, final Boolean onlyMulti) {
		ResponsesDTO r = new ResponsesDTO();
		final List<MasterDocumentRedDTO> masters = new ArrayList<>();
		masters.add(m);

		final Collection<ResponsesDTO> responses = refineResponses(masters, u, onlyMulti);

		if (!responses.isEmpty()) {
			r = responses.iterator().next();
		}

		return r;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IResponseRedFacadeSRV#refineResponses(java.util.List,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.Boolean).
	 */
	@Override
	public Collection<ResponsesDTO> refineResponses(final List<MasterDocumentRedDTO> masters, final UtenteDTO u, final Boolean onlyMulti) {
		final Collection<ResponsesDTO> output = new ArrayList<>();
		Collection<String> rawResponse = new ArrayList<>();
		Connection connection = null;
		IFilenetCEHelper fceh = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(u.getFcDTO(), u.getIdAoo());
			final Aoo aoo = aooDAO.getAoo(u.getIdAoo(), connection);

			final boolean hasPkHandlerTimbri = (aoo.getPkHandlerTimbro() != null);
			boolean hasRdsAperte = false;
			boolean isInLavorazioneUCBQueue = false;

			for (final MasterDocumentRedDTO m : masters) {
				ResponsesDTO respDto = new ResponsesDTO();

				isInLavorazioneUCBQueue = DocumentQueueEnum.IN_LAVORAZIONE_UCB.equals(m.getQueue());
				// Se siamo nella coda in lavorazione, skippo tutte le response di FileNet
				if (!isInLavorazioneUCBQueue && m.getResponsesRaw() != null && m.getResponsesRaw().length > 0) {
	
					hasRdsAperte = documentHasRdsAperte(aoo.getIdAoo(), m.getDocumentTitle());
					
					// Converto l'array in un formato umanamente gestibile
					rawResponse = new ArrayList<>(Arrays.asList(m.getResponsesRaw()));
					final Collection<String> iterateResponse = new ArrayList<>(Arrays.asList(m.getResponsesRaw()));

					final Iterator<String> ite = iterateResponse.iterator();
					
					boolean isInterno = false;
					for (String[] destSplit : m.getDestinatariRaw()) {
						String tipoDestinatario = destSplit[3];
						if (TipoDestinatario.INTERNO.equalsIgnoreCase(tipoDestinatario)) {
							isInterno = true;
							break;
						}
					}

					while (ite.hasNext()) {
						final String response = ite.next();

						boolean bRemove = false;
						if (hasRdsAperte) {
							// Rimozione dalla lista di response filenet in caso il documento abbia RdS
							// aperte
							bRemove = true;
						} else if (response.startsWith("SYS_")) {
							// Rimozione dalla lista di response quelle di Sistema 'SYS_'
							bRemove = true;
						} else if (ResponsesRedEnum.SPEDISCI_USCITA.getResponse().equals(response)) {
							// Rimozione dalla lista di response SPEDISCI USCITA
							bRemove = true;
						} else if (Boolean.TRUE.equals(ufficioUCPeSpedito(m.getDocumentTitle(), u, response, connection))) {
							// Regola Business 1 *** Rimuovo le response se il Documento risulta Elettronico
							// ***
							bRemove = true;
						} else if (Boolean.TRUE.equals(inviaDecretoinFirma(m.getDocumentTitle(), u, response, fceh))) {
							// Regola Business 2 *** Rimuovo la response se il Documento(Decreto) non ha
							// ordini di pagamento associati in stato INVIATO o essere in un fascicolo OP
							// disponibile ***
							bRemove = true;
						} else if (Boolean.TRUE.equals(inviaUfficioCoordinamento(m.getTipoSpedizione(), response)
								|| (ResponsesRedEnum.INVIA_UFFICIO_COORDINAMENTO.getResponse().equalsIgnoreCase(response)
										&& u.getIdAoo().equals(Long.parseLong(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IDAOO_DF)))))) {
							// Regola Business 3 *** Rimuovo la response se il doc ha almeno un destinatario
							// elettronico ***
							bRemove = true;
						} else if(isInterno && ResponsesRedEnum.INVIA_UFFICIO_COORDINAMENTO.getResponse().equalsIgnoreCase(response) ) {
							bRemove = true;
						} else if (Boolean.TRUE.equals(inviaFirmaeFirmaDigitale(m.getIsModalitaRegistroRepertorio(), m.getTipoSpedizione(), response))
								&& !(ResponsesRedEnum.INVIA_FIRMA_DIGITALE.getResponse().equalsIgnoreCase(response)
										&& u.getIdAoo().equals(Long.parseLong(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IDAOO_DF))))) {
							// Regola Business 4 *** Rimuovo le response se il doc non ha nessun
							// destinatario elettronico ***
							if(!isInterno) {
								bRemove = true;
							}
						} else if ((ResponsesRedEnum.ASSEGNA_UFFICIO_UCP.getResponse().equalsIgnoreCase(response)
								|| ResponsesRedEnum.ASSEGNA_UFFICIO_UCP_2.getResponse().equalsIgnoreCase(response))
								&& u.getIdAoo().equals(Long.parseLong(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IDAOO_DF)))) {
							// Regola Business 4 *** Rimuovo le response se il doc non ha nessun
							// destinatario elettronico ***
							bRemove = true;
						} else if (ResponsesRedEnum.RICHIEDI_FIRMA_COPIA_CONFORME.getResponse().equals(response)) {
							// Regola Business 5 *** Rimuovo la response se per nessun allegato è prevista
							// la Copia conforme ***
							if (Boolean.TRUE.equals(richiediFirmaCopiaConforme(m.getGuuid(), m.getDocumentTitle(), fceh))) {
								bRemove = true;
							}
						} else if (Boolean.TRUE.equals(richiediSollecito(m.getNumeroDocumento(), m.getAnnoDocumento(), m.getDocumentTitle(), u, response, connection))) {
							// Regola Business 6 *** Rimuovo la response se il documento non ha contributi
							// attivi collegati ***
							bRemove = true;
						} else if (Boolean.TRUE.equals(predisponiDichiarazione(u, response))) {
							// Regola Business 7 *** Rimuovo la response se l'utente non possiede il
							// permesso DSR ***
							bRemove = true;
						} else if (Boolean.TRUE.equals(validaContributi(m, response))) {
							// Regola Business 8 *** Rimuovo la response se tutti i contributi richiesti non
							// sono stati evasi ***
							bRemove = true;
						} else if (Boolean.TRUE.equals(configurazioneFirmaDigitaleAoo(u, response))) {
							// Regola Business 9 *** Rimuovo la response se la configurazione dell'AOO
							// permette solo alcune tipologie di firma ***
							bRemove = true;
						} else if (firmaMultipla(m.getQueue(), m.getTipoAssegnazione(), response)) {
							// Regola Business 10 *** Rimuovo le response non consentite durante il processo
							// di Firma Multipla ***
							bRemove = true;
						} else if (invioInFirmaMultipla(hasPkHandlerTimbri, response)||(ResponsesRedEnum.INVIO_IN_FIRMA_MULTIPLA.getResponse().equals(response) && m.getSottoCategoriaDocumentoUscita()!=null && m.getSottoCategoriaDocumentoUscita().isRegistrazioneAusiliaria())) {
							// Regola Business 11 *** Rimuovo la response se l'AOO non ha configurato il PK
							// Handler per i Timbri ***
							bRemove = true;
						} else if (predisponiRestituzione(m.getIdTipologiaDocumento(), m.getProtocolloRiferimento(), response, u.getCodiceAoo(), u.isUcb(), u.getFcDTO(),
								connection)) {
							// Regola Business 12 *** Rimuovo la response se il documento NON è di tipo
							// "RITIRO PROVVEDIMENTO" oppure se l'AOO non è un UCB
							// oppure se il documento a cui si riferisce il metadato "protocollo
							// riferimento"
							// NON è nella coda Da Lavorare e NON è nella coda In Sospeso con allaccio
							// principale di tipo Richiesta Integrazioni ***
							bRemove = true;
						} else if (disableResponseRegAus(response, m.getNumeroProtocollo(), m.getAnnoProtocollo(), u.isUcb(), m.getIdTipologiaDocumento(),
								m.getIdTipoProcedimento(), u.getIdAoo(), m.getCodiceFlusso(), connection)) {
							// Regola Business 13 *** Rimuovo le response che attivano una Registrazione
							// Ausiliaria se il documento selezionato è privo di protocollo
							// e il tipo documento non ha nessun registro ausiliario configurato ***
							bRemove = true;
						} else if (Boolean.TRUE.equals(isResponseDaRimuovere(response))) {
							// Regola 'X' *** Rimuovo le responses che per un motivo o per un altro sono
							// presenti nel WorkFlow ma non sono più gestite dall'applicazione ***
							bRemove = true;
						} else if (ResponsesRedEnum.SPEDITO.getResponse().equalsIgnoreCase(response)) {
							final boolean isSpedito = listaDocumentiSRV.giaSpedito(m.getDocumentTitle());
							// rimuovo la response se risulta già spedito
							bRemove = isSpedito;

						}

						if (bRemove) {
							rawResponse.remove(response);
						}
						
					}
				}

				respDto = ResponsesRedEnum.getStructuredDTO(rawResponse, onlyMulti);

				// ### Aggiungo le response applicative
				getResponseApplicative(m, u, onlyMulti, connection, hasRdsAperte, hasPkHandlerTimbri, isInLavorazioneUCBQueue, respDto.getResponsesEnum());
				
				// ### Gestione delle response per i documenti generati da flussi
				if (!StringUtils.isNullOrEmpty(m.getCodiceFlusso())) {
					final IAzioniFlussoBaseSRV azioniFlussoBaseSRV = azioniFlussoSRV.retrieveAzioniFlussoSRV(m.getCodiceFlusso());
					if (azioniFlussoBaseSRV != null) {
						respDto = azioniFlussoBaseSRV.refineResponses(m.getQueue(), respDto);
					}
				}

				if ((m.getQueue().equals(DocumentQueueEnum.CORRIERE) && respDto.getResponsesEnum().contains(ResponsesRedEnum.ASSEGNA)
						&& respDto.getResponsesEnum().contains(ResponsesRedEnum.RICHIEDI_CONTRIBUTO)
						&& respDto.getResponsesEnum().contains(ResponsesRedEnum.METTI_IN_CONOSCENZA))) {
					respDto.getResponsesEnum().remove(ResponsesRedEnum.ASSEGNA);
					respDto.getResponsesEnum().remove(ResponsesRedEnum.RICHIEDI_CONTRIBUTO);
					respDto.getResponsesEnum().remove(ResponsesRedEnum.METTI_IN_CONOSCENZA);
					respDto.getResponsesEnum().add(ResponsesRedEnum.ASSEGNA_NUOVA);
				}

				output.add(respDto);
				
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_PULIZIA_RESPONSE_MASTER_MSG + e.getMessage(), e);
			throw new RedException(ERROR_PULIZIA_RESPONSE_MASTER_MSG + e.getMessage(), e);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}

		return output;
	}

	/**
	 * @param idAoo
	 * @param documentTitle
	 * @return true se il documento identificato dal documentTitle è associato ad
	 *         RDS aperte
	 */
	private boolean documentHasRdsAperte(final Long idAoo, final String documentTitle) {
		boolean hasRdsAperte = false;

		try {
			hasRdsAperte = siebelSRV.hasRdsAperte(documentTitle, idAoo);
		} catch (final Exception e) {
			LOGGER.error(e);
		}
		return hasRdsAperte;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IResponseRedFacadeSRV#hasLibroFirmaMultipla(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public boolean hasLibroFirmaMultipla(final UtenteDTO u) {
		boolean hasFirmaMultipla = false;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			hasFirmaMultipla = (aooDAO.getAoo(u.getIdAoo(), connection).getPkHandlerTimbro() != null);

		} catch (final Exception e) {
			LOGGER.error(ERROR_PULIZIA_RESPONSE_MASTER_MSG + e.getMessage(), e);
			throw new RedException(ERROR_PULIZIA_RESPONSE_MASTER_MSG + e.getMessage(), e);
		} finally {
			closeConnection(connection);
		}

		return hasFirmaMultipla;
	}

	/**
	 * @param guid
	 * @param documentTitle
	 * @param fceh
	 * @return
	 */
	private Boolean richiediFirmaCopiaConforme(final String guid, final String documentTitle, final IFilenetCEHelper fceh) {
		Boolean output = false;

		try {
			// Recupero informazioni necessarie per applicare la 5° regola di business
			Boolean flagCopiaConforme = false;
			final DocumentSet documents = fceh.getAllegati(guid, false, false, null, null);

			final Iterator<?> it = documents.iterator();
			while (it.hasNext()) {
				final Document d = (Document) it.next();
				flagCopiaConforme = (Boolean) TrasformerCE.getMetadato(d, PropertiesNameEnum.IS_COPIA_CONFORME_METAKEY);
				if (Boolean.TRUE.equals(flagCopiaConforme)) {
					break;
				}
			}

			if (Boolean.FALSE.equals(flagCopiaConforme)) {
				output = true;
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la pulizia delle response per il documento con DocumentTitle [" + documentTitle + "]: " + e.getMessage(), e);
		}

		return output;
	}

	/**
	 * @param m
	 * @param u
	 * @param rawResponse
	 * @param s
	 */
	private Boolean richiediSollecito(final Integer numDoc, final Integer annoDoc, final String documentTitle, final UtenteDTO u, final String response,
			final Connection connection) {
		Boolean output = false;

		if (response.equalsIgnoreCase(ResponsesRedEnum.RICHIEDI_SOLLECITO.getResponse())) {
			try {
				output = !contributoSRV.checkContributoEsternoNonConcluso(documentTitle, numDoc, annoDoc, u, connection, true);
			} catch (final Exception e) {
				LOGGER.error(ERROR_PULIZIA_RESPONSE_DOCUMENTO_MSG + documentTitle + " ): " + e.getMessage(), e);
			}
		}

		return output;
	}

	/**
	 * @param m
	 * @param u
	 * @param rawResponse
	 * @param s
	 */
	private static Boolean inviaDecretoinFirma(final String documentTitle, final UtenteDTO u, final String s, final IFilenetCEHelper fceh) {
		Boolean output = false;
		if (s.equalsIgnoreCase(ResponsesRedEnum.INVIO_DECRETO_FIRMA.getResponse())) {
			try {
				// recupero informazioni necessari per applicare la 2° regola di business
				final Boolean enableInvioDecretoInFirma = fceh.verifyFascicoloFepaDisponibilePerOPCollegati(documentTitle, u.getIdAoo());

				// Regola Business 2 *** Rimuovo la response se il Documento(Decreto) non ha
				// ordini di pagamento associati in stato INVIATO o essere in un fascicolo OP
				// disponibile ***
				output = !enableInvioDecretoInFirma;
			} catch (final Exception e) {
				LOGGER.error(ERROR_PULIZIA_RESPONSE_DOCUMENTO_MSG + documentTitle + " ): " + e.getMessage(), e);
			}
		}
		return output;
	}

	/**
	 * @param m
	 * @param u
	 * @param rawResponse
	 */
	private Boolean ufficioUCPeSpedito(final String documentTitle, final UtenteDTO u, final String s, final Connection con) {
		Boolean output = false;
		if (s.equalsIgnoreCase(ResponsesRedEnum.SPEDITO.getResponse()) || s.equalsIgnoreCase(ResponsesRedEnum.ASSEGNA_UFFICIO_UCP.getResponse())) {
			try {
				// recupero informazioni necessari per applicare la 1° regola di business
				boolean removeUfficioUCP = false;
				final Collection<CodaEmail> notificheMail = gneDAO.getNotificheEmailByIdDocumento(documentTitle, con);
				for (final CodaEmail n : notificheMail) {
					if ((n.getStatoRicevuta() == Constants.Notifiche.STATO_RICEVUTA_NOTIFICA_EMAIL_CHIUSURA)
							&& (n.getTipoDestinatario() == Constants.Notifiche.TIPOLOGIA_DESTINATARI_MITTENTI_CARTACEI)) {
						removeUfficioUCP = true;
					} else if (n.getTipoDestinatario() == Constants.Notifiche.TIPOLOGIA_DESTINATARI_MITTENTI_MISTI) {
						// se destinatari misti e ha nello storico la response spedito, nascondi la
						// response
						removeUfficioUCP = hasEventoStorico(documentTitle, EventTypeEnum.SPEDITO.getValue(), u.getIdAoo().intValue());
					}

					if (removeUfficioUCP) {
						break;
					}
				}

				// Regola Business 1 *** Rimuovo le response se il Documento risulta Elettronico
				// ***
				output = removeUfficioUCP;
			} catch (final Exception e) {
				LOGGER.error(ERROR_PULIZIA_RESPONSE_DOCUMENTO_MSG + documentTitle + " ): " + e.getMessage(), e);
			}
		}

		return output;
	}

	/**
	 * @param m
	 * @param rawResponse
	 * @param s
	 */
	private static Boolean inviaUfficioCoordinamento(final TipoSpedizioneDocumentoEnum tipo, final String s) {
		Boolean output = false;

		if (s.equalsIgnoreCase(ResponsesRedEnum.INVIA_UFFICIO_COORDINAMENTO.getResponse()) && tipo.equals(TipoSpedizioneDocumentoEnum.ELETTRONICO)) {
			output = true;
		}

		return output;
	}

	/**
	 * @param m
	 * @param rawResponse
	 * @param s
	 */
	private static Boolean inviaFirmaeFirmaDigitale(final Boolean isRepertorio, final TipoSpedizioneDocumentoEnum tipo, final String s) {
		Boolean output = false;
		if (s.equalsIgnoreCase(ResponsesRedEnum.INVIA_FIRMA_DIGITALE.getResponse()) && !tipo.equals(TipoSpedizioneDocumentoEnum.ELETTRONICO)
				&& Boolean.FALSE.equals(isRepertorio)) {
			output = true;
		}
		return output;
	}

	/**
	 * @param u
	 * @param rawResponse
	 * @param s
	 */
	private static Boolean predisponiDichiarazione(final UtenteDTO u, final String s) {
		Boolean output = false;

		if (s.equalsIgnoreCase(ResponsesRedEnum.PREDISPONI_DICHIARAZIONE.getResponse()) && !u.isPermessoDSR()) {
			output = true;
		}

		return output;
	}

	private static Boolean validaContributi(final MasterDocumentRedDTO m, final String s) {
		Boolean output = Boolean.FALSE;

		if (ResponsesRedEnum.VALIDA_CONTRIBUTI.getResponse().equalsIgnoreCase(s) && (m.getContributiRichiesti() != null && m.getContributiPervenuti() != null)) {
			// Regola Business 8 *** Rimuovo la response se tutti i contributi non sono
			// stati evasi (Richiesti != Pervenuti) ***
			output = Boolean.TRUE;
			if ((m.getContributiRichiesti() > 0 && m.getContributiRichiesti().equals(m.getContributiPervenuti()))) {
				output = Boolean.FALSE;
			}
		}

		return output;
	}

	/**
	 * Gestisce la visualizzazione delle response di Firma Digitale.
	 * 
	 * @param utente
	 * @param response
	 * @return
	 */
	private static Boolean configurazioneFirmaDigitaleAoo(final UtenteDTO utente, final String response) {
		Boolean output = Boolean.FALSE;

		ConfigurazioneFirmaAooEnum configurazioneFirma = null;
		if (utente.getSignerInfo() != null) {
			configurazioneFirma = utente.getSignerInfo().getConfigurazioneFirma();
		}

		// Si rimuovono le response FileNet 'Firma Digitale' / 'Firma Digitale Multipla'
		// se:
		// - L'AOO è abilitato alla sola firma remota, oppure
		// - L'AOO non è abilitato alla firma, oppure
		// - Siamo in regime di emergenza.
		// N.B. La gestione delle response applicative 'Firma Remota' / 'Firma Remota
		// Multipla' avviene successivamente, cfr. metodo "getResponseApplicative"
		if ((ResponsesRedEnum.FIRMA_DIGITALE.getResponse().equalsIgnoreCase(response) || ResponsesRedEnum.FIRMA_DIGITALE_MULTIPLA.getResponse().equalsIgnoreCase(response))
				&& (ConfigurazioneFirmaAooEnum.SOLO_REMOTA.equals(configurazioneFirma) || ConfigurazioneFirmaAooEnum.DISABILITATA.equals(configurazioneFirma)
						|| TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo()))) {
			output = Boolean.TRUE;
		}

		return output;
	}

	/**
	 * Gestisce la visualizzazione delle response di Firma Multipla.
	 * 
	 * @param tipoAssegnazione
	 * @param response
	 * @return
	 */
	private static boolean firmaMultipla(final DocumentQueueEnum coda, final TipoAssegnazioneEnum tipoAssegnazione, final String response) {
		boolean output = false;

		// Se l'assegnazione è per Firma Multipla (rifiuto compreso), devono essere
		// visibili solo le seguenti response FileNet:
		// - Nella coda 'DA LAVORARE':
		// --- Invio in Firma Multipla
		// --- Invio in Firma
		// --- Modifica Iter
		// - Nella coda 'LIBRO FIRMA':
		// --- Firma Digitale
		// --- Firma Digitale Multipla
		// --- Rifiuta
		if ((TipoAssegnazioneEnum.FIRMA_MULTIPLA.equals(tipoAssegnazione) || TipoAssegnazioneEnum.RIFIUTO_FIRMA_MULTIPLA.equals(tipoAssegnazione))
				&& ((DocumentQueueEnum.isOneKindOfDaLavorare(coda) && !(ResponsesRedEnum.INVIO_IN_FIRMA_MULTIPLA.getResponse().equals(response)
						|| ResponsesRedEnum.INVIO_IN_FIRMA.getResponse().equals(response) || ResponsesRedEnum.MODIFICA_ITER.getResponse().equals(response)))
						|| (DocumentQueueEnum.NSD.equals(coda) && !(ResponsesRedEnum.FIRMA_DIGITALE.getResponse().equals(response)
								|| ResponsesRedEnum.FIRMA_DIGITALE_MULTIPLA.getResponse().equals(response) || ResponsesRedEnum.RIFIUTA.getResponse().equals(response))))) {
			output = true;
		}

		return output;
	}

	/**
	 * Gestisce la visualizzazione della response 'Invio in Firma Multipla'.
	 * 
	 * @param hasPkHandlerTimbri
	 * @param response
	 * @return
	 */
	private static boolean invioInFirmaMultipla(final boolean hasPkHandlerTimbri, final String response) {
		boolean output = false;

		// Se l'AOO non ha configurato il PK Handler per i Timbri, si nasconde la
		// response 'Invio In Firma Multipla'
		if (ResponsesRedEnum.INVIO_IN_FIRMA_MULTIPLA.getResponse().equals(response) && !hasPkHandlerTimbri) {
			output = true;
		}

		return output;
	}

	/**
	 * Gestisce la visualizzazione della response 'Predisponi Restituzione'. N.B.
	 * Per facilitare la comprensione delle condizioni, viene usata una logica
	 * inversa negando il booleano alla fine.
	 * 
	 * @param idTipologiaDocumento
	 * @param documentTitleProtRif
	 * @param response
	 * @param codiceAoo
	 * @param isUCB
	 * @param fcUtente
	 * @param con
	 * @return true se la response deve essere rimossa dalla lista, false altrimenti
	 */
	private boolean predisponiRestituzione(final Integer idTipologiaDocumento, final String documentTitleProtRif, final String response, final String codiceAoo,
			final boolean isUCB, final FilenetCredentialsDTO fcUtente, final Connection con) {
		boolean output = false;
		if (ResponsesRedEnum.PREDISPONI_RESTITUZIONE.getResponse().equals(response)) {
			if (isUCB) {

				final Integer idTipologiaRitiroInAutotutelaEntrata = Integer.parseInt(PropertiesProvider.getIstance()
						.getParameterByString(codiceAoo + "." + PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_ENTRATA_RITIRO_IN_AUTOTUTELA.getKey()));

				// La response "Predisponi Restituzione" deve innanzitutto essere disponibile
				// SOLO sui documenti di tipologia "RITIRO IN AUTOTUTELA" (a.k.a. "RITIRO
				// PROVVEDIMENTO")
				if (!idTipologiaDocumento.equals(idTipologiaRitiroInAutotutelaEntrata) || StringUtils.isNullOrEmpty(documentTitleProtRif)) {
					output = true;
				} else {
					FilenetPEHelper fpeh = null;

					try {
						fpeh = new FilenetPEHelper(fcUtente);

						// Si recupera il workflow principale del protocollo riferimento
						final VWWorkObject workflowPrincipaleProtRif = fpeh.getWorkflowPrincipale(documentTitleProtRif, fcUtente.getIdClientAoo());

						if (workflowPrincipaleProtRif != null) {
							final EnumMap<ContextTrasformerPEEnum, Object> context = new EnumMap<>(ContextTrasformerPEEnum.class);
							context.put(ContextTrasformerPEEnum.UCB, isUCB);

							final PEDocumentoDTO wfDTO = TrasformPE.transform(workflowPrincipaleProtRif, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO_CONTEXT, context);

							// Si cerca l'allaccio principale di tipo Richiesta Integrazioni
							final RispostaAllaccioDTO lastRichiestaIntegrazioni = allaccioDAO.getLastRichiestaIntegrazioni(Integer.parseInt(documentTitleProtRif), con);

							// La response "Predisponi Restituzione" è disponibile se il documento a cui si
							// riferisce "protocollo riferimento" è nelle code:
							// a) DA_LAVORARE_UCB o IN_LAVORAZIONE_UCB, oppure
							// b) SOSPESO, con allaccio principale di tipo Richiesta Integrazioni
							if (!DocumentQueueEnum.DA_LAVORARE_UCB.equals(wfDTO.getQueue()) && !DocumentQueueEnum.IN_LAVORAZIONE_UCB.equals(wfDTO.getQueue())
									&& !(DocumentQueueEnum.SOSPESO.equals(wfDTO.getQueue()) && lastRichiestaIntegrazioni != null)) {
								output = true;
							}
						} else {
							LOGGER.error("Nessun workflow principale trovato per il protocollo riferimento: " + documentTitleProtRif);
						}
					} finally {
						logoff(fpeh);
					}
				}

			} else {

				output = true;

			}
		}
		

		return output;
	}

	/**
	 * 
	 * @param response
	 * @param numeroProt
	 * @param annoProt
	 * @param isUCB
	 * @return
	 */
	private boolean disableResponseRegAus(final String response, final Integer numeroProt, final Integer annoProt, final boolean isUCB, final Integer idTipologiaDocumento,
			final Integer idTipoProcedimento, final Long idAoo, String codiceFlusso, final Connection con) {
		boolean output = false;
		Collection<RegistroDTO> registri = null;
		if (isUCB) {
			if (ResponsesRedEnum.PREDISPONI_VISTO.getResponse().equals(response)) {

				if (!TipoContestoProceduraleEnum.FLUSSO_CG2.name().equals(codiceFlusso)) {
					registri = registroAusiliarioDAO.getRegistri(con, TipoRegistroAusiliarioEnum.VISTO, idTipologiaDocumento, idTipoProcedimento);
					output = registri.isEmpty() || numeroProt == null || annoProt == null;
					
					if (!output) {
						output = checkTipoDocUscita(idTipologiaDocumento, idAoo, con);
					}
				} else {
					output = true;
				}
				
			} else if (ResponsesRedEnum.PREDISPONI_RELAZIONE_POSITIVA.getResponse().equals(response)) {
				
				if (TipoContestoProceduraleEnum.FLUSSO_CG2.name().equals(codiceFlusso)) {
					registri = registroAusiliarioDAO.getRegistri(con, TipoRegistroAusiliarioEnum.VISTO, idTipologiaDocumento, idTipoProcedimento);
					output = registri.isEmpty() || numeroProt == null || annoProt == null;
					
					if (!output) {
						output = checkTipoDocUscita(idTipologiaDocumento, idAoo, con);
					}
				} else {
					output = true;
				}

			} else if (ResponsesRedEnum.PREDISPONI_OSSERVAZIONE.getResponse().equals(response)) {
				
				if (!TipoContestoProceduraleEnum.FLUSSO_CG2.name().equals(codiceFlusso)) {
					registri = registroAusiliarioDAO.getRegistri(con, TipoRegistroAusiliarioEnum.OSSERVAZIONE_RILIEVO, idTipologiaDocumento, idTipoProcedimento);
					output = registri.isEmpty() || numeroProt == null || annoProt == null;
				} else {
					output = true;
				}
				
			} else if (ResponsesRedEnum.PREDISPONI_RELAZIONE_NEGATIVA.getResponse().equals(response)) {
				
				if (TipoContestoProceduraleEnum.FLUSSO_CG2.name().equals(codiceFlusso)) {
					registri = registroAusiliarioDAO.getRegistri(con, TipoRegistroAusiliarioEnum.OSSERVAZIONE_RILIEVO, idTipologiaDocumento, idTipoProcedimento);
					output = registri.isEmpty() || numeroProt == null || annoProt == null;
				} else {
					output = true;
				}
				
			} else if (ResponsesRedEnum.RICHIESTA_INTEGRAZIONI.getResponse().equals(response)) {
				registri = registroAusiliarioDAO.getRegistri(con, TipoRegistroAusiliarioEnum.RICHIESTA_ULTERIORI_INFORMAZIONI, idTipologiaDocumento, idTipoProcedimento);
				output = registri.isEmpty() || numeroProt == null || annoProt == null;
			} else if (ResponsesRedEnum.PREDISPONI_RESTITUZIONE.getResponse().equals(response)) {
				registri = registroAusiliarioDAO.getRegistri(con, TipoRegistroAusiliarioEnum.RESTITUZIONI, idTipologiaDocumento, idTipoProcedimento);
				output = registri.isEmpty() || numeroProt == null || annoProt == null;
			} else if (ResponsesRedEnum.RITIRO_AUTOTUTELA.getResponse().equals(response)) {
				output = checkTipoDocUscita(idTipologiaDocumento, idAoo, con);
			}
		}
		
		return output;
	}

	/**
	 * @param idTipologiaDocumento
	 * @param idAoo
	 * @param con
	 * @return
	 */
	private boolean checkTipoDocUscita(final Integer idTipologiaDocumento, final Long idAoo, final Connection con) {
		boolean output;
		final TipologiaDocumentoDTO tipologiaDocumentoEntrata = tipologiaDocumentoDAO.getById(idTipologiaDocumento, con);
		// Si recupera la tipologia documento per l'uscita corrispondente a quella del
		// documento in Entrata.
		final Integer idTipologiaDocumentoUscita = tipologiaDocumentoDAO.getIdTipologiaDocumentoByDescrizioneAndTipoCategoria(tipologiaDocumentoEntrata.getDescrizione(),
				TipoCategoriaEnum.USCITA.getIdTipoCategoria(), idAoo, con);
		output = idTipologiaDocumentoUscita == null;
		return output;
	}

	/**
	 * Metodo per verificare se la response fa parte di un gruppo di response non
	 * disponibili nell'applicazione e quindi da rimuovere.
	 * 
	 * @param s
	 * @return true se la response è da rimuovere.
	 */
	private static Boolean isResponseDaRimuovere(final String s) {
		Boolean output = false;

		if (RICHIEDI_ULTERIORI_CONTRIBUTI.equalsIgnoreCase(s) || RECALL.equalsIgnoreCase(s)) {
			output = true;
		}

		return output;
	}

	private void getResponseApplicative(final MasterDocumentRedDTO m, final UtenteDTO u, final Boolean onlyMulti, final Connection connection, final boolean hasRdsAperte,
			final boolean hasPkHandlerTimbri, final boolean isInLavorazioneUCBQueue, final Collection<ResponsesRedEnum> allResponses) {
		final Collection<ResponsesRedEnum> output = new ArrayList<>();

		try {
			// Gestione conferma annullamento visto, osservazione e richiesta chiarimenti
			// Se sono in lavorazione o in sospeso
			if (DocumentQueueEnum.isOneKindOfDaLavorare(m.getQueue()) && m.isAnnullaTemplate()) {
				// se il metadato del wf annullaTemplate è valorizzato ad 1

				// elimina tutte le response precedenti
				allResponses.clear();

				// aggiungi la response applicativa
				addAppResponse(output, ResponsesRedEnum.CONFERMA_ANNULLAMENTO, onlyMulti);

				// aggiungi response
				allResponses.addAll(output);

				return;

			}
			
			// Aggiunta della response Applicativa 'inserisci Nota' per ogni Coda tranne che per la coda ATTI
			// Aggiunta la condizione in OR per far in modo che inserisci nota non venga inserita anche in approvazione
			if (!(m.getQueue().equals(DocumentQueueEnum.ATTI) 
					|| m.getQueue().equals(DocumentQueueEnum.RICHIAMA_DOCUMENTI)
					|| m.getQueue().equals(DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE_FEPA)
					|| m.getQueue().equals(DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE))) {
				addAppResponse(output, ResponsesRedEnum.INSERISCI_NOTA, onlyMulti);
			}

			// Aggiungi le altre response solo in caso non ci siano RdS aperte sul documento
			// e la coda non sia InLavorazioneUCB
			if (!hasRdsAperte && !isInLavorazioneUCBQueue) {
				// precondizioni per l'inserimento della response 'Spedisci Mail'
				boolean notificaIsPresent = false;
				final Collection<CodaEmail> notificheMail = gneDAO.getNotificheEmailByIdDocumento(m.getDocumentTitle(), connection);
				for (final CodaEmail n : notificheMail) {
					if ((n.getStatoRicevuta() == Constants.Notifiche.STATO_RICEVUTA_NOTIFICA_EMAIL_ATTESA_SPEDIZIONE_UTENTE)) {
						notificaIsPresent = true;
						break;
					}
				}

				final Integer idAooRL = Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IDAOO_RL));
				if ((u.getIdAoo().intValue() == idAooRL) && (m.getQueue().equals(DocumentQueueEnum.SPEDIZIONE)) && notificaIsPresent) {
					addAppResponse(output, ResponsesRedEnum.SPEDISCI_MAIL, onlyMulti);
				}

				// Precondizioni per l'inserimento delle responses 'Firma Remota' / 'Firma
				// Remota Multipla'
				// N.B. Si verifica anche se l'AOO è abilitato alla firma, sia locale sia remota
				// che solamente remota,
				// e se non siamo in regime di emergenza
				ConfigurazioneFirmaAooEnum configurazioneFirmaAoo = null;
				if (u.getSignerInfo() != null) {
					configurazioneFirmaAoo = u.getSignerInfo().getConfigurazioneFirma();

					if (u.getSignerInfo().getSigner() != null && m.getStepName() != null
							&& (m.getStepName().endsWith("Libro Firma per Firma") || m.getStepName().endsWith("Libro Firma per firmaMultipla"))
							&& !TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(u.getIdTipoProtocollo())
							&& (ConfigurazioneFirmaAooEnum.SOLO_REMOTA.equals(configurazioneFirmaAoo)
									|| ConfigurazioneFirmaAooEnum.LOCALE_REMOTA.equals(configurazioneFirmaAoo))) {

						// Se il documento si trova nel libro firma con assegnazione per Firma Multipla
						// e l'AOO ha configurato il PK Handler per i Timbri, allora si aggiunge la
						// response di Firma Remota Multipla
						if (TipoAssegnazioneEnum.FIRMA_MULTIPLA.equals(m.getTipoAssegnazione()) && hasPkHandlerTimbri) {
							addAppResponse(output, ResponsesRedEnum.FIRMA_REMOTA_MULTIPLA, onlyMulti);
						} else {
							addAppResponse(output, ResponsesRedEnum.FIRMA_REMOTA, onlyMulti);
						}
					}
				}

				// Precondizioni per l'inserimento delle responses 'Traccia Procedimento', 'Non
				// Tracciare Procedimento' & 'Inoltra Mail'
				if ((DocumentQueueEnum.isOneKindOfDaLavorare(m.getQueue())) || (DocumentQueueEnum.NSD.equals(m.getQueue()))
						|| (DocumentQueueEnum.isOneKindOfCorriere(m.getQueue())) || (DocumentQueueEnum.SPEDIZIONE.equals(m.getQueue()))) {

					if (u.getFlagInoltraMail() == 1 && !(TipoAssegnazioneEnum.FIRMA_MULTIPLA.equals(m.getTipoAssegnazione()) // Si esclude la Firma Multipla
							|| TipoAssegnazioneEnum.RIFIUTO_FIRMA_MULTIPLA.equals(m.getTipoAssegnazione()))) {
						addAppResponse(output, ResponsesRedEnum.INOLTRA_DA_CODA_ATTIVA, onlyMulti);
					}

					if (tracciaDocumentiSRV.hasTracciamentiSingoli(u)) {
						if (tracciaDocumentiSRV.isDocumentoTracciato(u, Integer.parseInt(m.getDocumentTitle()))) {
							addAppResponse(output, ResponsesRedEnum.NO_TRACCIA_PROCEDIMENTO, onlyMulti);
						} else {
							addAppResponse(output, ResponsesRedEnum.TRACCIA_PROCEDIMENTO, onlyMulti);
						}
					}

					if (DocumentQueueEnum.isOneKindOfDaLavorare(m.getQueue())) {
						// Apri RDS Siebel se ho il documento in competenza, l'ufficio destinatario è
						// abilitato, il documento è protocollato
						final boolean isCompetente = m.getTipoAssegnazione().equals(TipoAssegnazioneEnum.COMPETENZA);
						final boolean isUfficioAbilitato = !StringUtils.isNullOrEmpty(siebelSRV.retrieveIdContattoSiebel(m.getIdUfficioDestinatario().longValue())); // ufficio																																// destinatario
						final boolean isProtocollato = m.getNumeroProtocollo() != null && m.getNumeroProtocollo() != 0;

						if (isCompetente && isUfficioAbilitato && isProtocollato) {
							addAppResponse(output, ResponsesRedEnum.APRI_RDS_SIEBEL, onlyMulti);
						}
					}
				}

				if (m.getQueue().equals(DocumentQueueEnum.ATTI)) {
					addAppResponse(output, ResponsesRedEnum.RIATTIVA_PROCEDIMENTO, onlyMulti);
				}

				// ### GESTIONE RESPONSES CONTRIBUTO ESTERNO -> START
				// - Coda "Da Lavorare" o "Libro Firma"
				if ((DocumentQueueEnum.isOneKindOfDaLavorare(m.getQueue()) || m.getQueue().equals(DocumentQueueEnum.NSD))) {
					final boolean altroContributoEsternoNonConcluso = contributoSRV.checkContributoEsternoNonConcluso(m.getDocumentTitle(), m.getNumeroDocumento(),
							m.getAnnoDocumento(), u, connection, false);

					// Response "Richiedi Contributo Esterno":
					// - Non è presente, per lo stesso documento, un altro Contributo Esterno non concluso (senza risposta)
					// - Assegnazione per competenza, sigla o firma
					// - Permesso AOO "Contributo Esterno".
					if (!altroContributoEsternoNonConcluso
							&& (TipoAssegnazioneEnum.COMPETENZA.equals(m.getTipoAssegnazione()) || TipoAssegnazioneEnum.SIGLA.equals(m.getTipoAssegnazione())
									|| TipoAssegnazioneEnum.FIRMA.equals(m.getTipoAssegnazione()))
							&& PermessiUtils.hasPermesso(u.getPermessiAOO(), PermessiAOOEnum.CONTRIBUTO_ESTERNO)) {
						addAppResponse(output, ResponsesRedEnum.RICHIEDI_CONTRIBUTO_ESTERNO, onlyMulti);
					}

					// Response "Collega Contributo":
					// - Documento in entrata e di tipologia "Contributo Esterno".
					if (CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0].equals(m.getIdCategoriaDocumento())
							&& "CONTRIBUTO ESTERNO".equalsIgnoreCase(m.getTipologiaDocumento())) {
						addAppResponse(output, ResponsesRedEnum.COLLEGA_CONTRIBUTO_ESTERNO, onlyMulti);
					}
				}
				// ### GESTIONE RESPONSES CONTRIBUTO ESTERNO -> END

				// Gestione response recall per la coda in approvazione
				if (m.getQueue().equals(DocumentQueueEnum.RICHIAMA_DOCUMENTI)) {
					addAppResponse(output, ResponsesRedEnum.RECALL, onlyMulti);
				}

				// Gestione annullamento visto, osservazione e richiesta chiarimenti.
				// Se:
				// - sono sul libro firma, e
				// - l'assegnazione è per firma, e
				// - non e' stata gia' staccata una registrazione ausiliaria per il documento
				// (tentativo di firma K.O. dopo lo stacco della registrazione ausiliaria)
				if ((m.getQueue().equals(DocumentQueueEnum.NSD) || m.getQueue().equals(DocumentQueueEnum.NSD_LIBRO_FIRMA_DELEGATO))
						&& (TipoAssegnazioneEnum.FIRMA.equals(m.getTipoAssegnazione()) || TipoAssegnazioneEnum.FIRMA_MULTIPLA.equals(m.getTipoAssegnazione()))) {

					final RegistrazioneAusiliariaDTO regAusiliaria = registrazioneAusiliariaDAO.getRegistrazioneAusiliaria(connection, m.getDocumentTitle(),
							u.getIdAoo().intValue());
					// Se l'ID della registrazione ausiliaria non è presente, allora la
					// registrazione è parziale e non ancora staccata su NPS,
					// quindi l'utente può procedere con l'eventuale annullamento tramite l'apposita
					// response
					if (regAusiliaria == null || regAusiliaria.getIdRegistrazione() == null) {

						// Aggiungi la response applicativa a seconda della sotto categoria del
						// documento
						if (SottoCategoriaDocumentoUscitaEnum.OSSERVAZIONE.equals(m.getSottoCategoriaDocumentoUscita())) {
							addAppResponse(output, ResponsesRedEnum.ANNULLA_OSSERVAZIONE, onlyMulti);
						} else if (SottoCategoriaDocumentoUscitaEnum.VISTO.equals(m.getSottoCategoriaDocumentoUscita())) {
							addAppResponse(output, ResponsesRedEnum.ANNULLA_VISTO, onlyMulti);
						} else if (SottoCategoriaDocumentoUscitaEnum.RICHIESTA_INTEGRAZIONI.equals(m.getSottoCategoriaDocumentoUscita())) {
							addAppResponse(output, ResponsesRedEnum.ANNULLA_RICHIESTA_INTEGRAZIONI, onlyMulti);
						} else if (SottoCategoriaDocumentoUscitaEnum.RELAZIONE_POSITIVA.equals(m.getSottoCategoriaDocumentoUscita())) {
							addAppResponse(output, ResponsesRedEnum.ANNULLA_RELAZIONE_POSITIVA, onlyMulti);
						} else if (SottoCategoriaDocumentoUscitaEnum.RELAZIONE_NEGATIVA.equals(m.getSottoCategoriaDocumentoUscita())) {
							addAppResponse(output, ResponsesRedEnum.ANNULLA_RELAZIONE_NEGATIVA, onlyMulti);
						}

					}
					
				}

				// Gestione response valida integrazione dati
				if (m.getQueue().equals(DocumentQueueEnum.DA_LAVORARE_UCB) && m.getIntegrazioneDati() && !StringUtils.isNullOrEmpty(m.getProtocolloRiferimento())) {
					addAppResponse(output, ResponsesRedEnum.VALIDA_INTEGRAZIONE_DATI, onlyMulti);
				}

				// Gestione response associa integrazione dati
				if (m.getQueue().equals(DocumentQueueEnum.SOSPESO) && CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0].equals(m.getIdCategoriaDocumento())) {
					// cerca allaccio principale
					final RispostaAllaccioDTO lastRichiestaIntegrazioni = allaccioDAO.getLastRichiestaIntegrazioni(Integer.parseInt(m.getDocumentTitle()), connection);
					if (lastRichiestaIntegrazioni != null) {
						addAppResponse(output, ResponsesRedEnum.ASSOCIA_INTEGRAZIONE_DATI, onlyMulti);
					}

				}

				if((DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE.equals(m.getQueue()) || DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE_FEPA.equals(m.getQueue())) 
						&& StatusEnum.ERRORE.equals(asyncSignSRV.checkDocStatus(m.getDocumentTitle()))) {
					addAppResponse(output, ResponsesRedEnum.RETRY, onlyMulti);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la gestione delle Response Applicative per il doc : ( " + m.getDocumentTitle() + " ): " + e.getMessage(), e);
		}

		// IMPORTANTE: non inserire il 'finally' con la chiusura della connection
		// altrimenti al prossimo giro non la trova disponibile
		allResponses.addAll(output);
	}

	private static void addAppResponse(final Collection<ResponsesRedEnum> output, final ResponsesRedEnum response, final Boolean onlyMulti) {
		if ((onlyMulti && ExecutionTypeEnum.MULTI.equals(response.getEt())) || Boolean.FALSE.equals(onlyMulti)) {
			output.add(response);
		}
	}

	/**
	 * 
	 * @param documentTitle
	 * @param evento
	 * @param idAoo
	 * @return
	 */
	private boolean hasEventoStorico(final String documentTitle, final String evento, final int idAoo) {
		final boolean hasStorico = false;
		Collection<StoricoDTO> storico = null;
		try {
			if (evento != null && documentTitle != null) {
				storico = storicoSRV.getStorico(Integer.parseInt(documentTitle), idAoo);
				if (storico != null && !storico.isEmpty()) {
					for (final StoricoDTO stepStorico : storico) {
						if (evento.equals(stepStorico.getTipoOperazione())) {
							return true;
						}
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dello storico", e);
		}
		return hasStorico;
	}

}
