package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.dto.SequKDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.persistence.model.Contatto;

/**
 * The Interface IPMefFacadeSRV.
 *
 * @author mcrescentini
 * 
 *         Facade PMef service.
 */
public interface IPMefFacadeSRV extends Serializable {
	
	/**
	 * Crea il protocollo in uscita tramite servizi PMEF.
	 * @param oggettoDocumento
	 * @param parametriProtocolloMEF
	 * @param destinatarioConoscenza
	 * @param utente
	 * @return protocollo PMEF
	 */
	ProtocolloDTO creaProtocolloMEFUscita(String oggettoDocumento, SequKDTO parametriProtocolloMEF, String destinatarioConoscenza, UtenteDTO utente);

	/**
	 * Crea il protocollo in entrata tramite servizi PMEF.
	 * @param inMittente
	 * @param oggettoDocumento
	 * @param parametriProtocolloMEF
	 * @param utente
	 * @return protocollo PMEF
	 */
	ProtocolloDTO creaProtocolloMEFEntrata(Contatto inMittente, String oggettoDocumento, SequKDTO parametriProtocolloMEF, UtenteDTO utente);
}