package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;

import it.ibm.red.business.persistence.model.CodaTrasformazione;


/**
 * Dao per la gestione della coda di trasformazione.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public interface ICodaTrasformazioneDAO extends Serializable {

	/**
	 * Metodo per il recupero dello stato di un documento attraverso l'ID.
	 * 
	 * @param idDocumento	identificativo del documento da trasformare
	 * @param con			connessione
	 * @return				stato del documento
	 * @throws DAOException
	 */
	int getStatoDocumentoFromCodaTrasformazione(int idDocumento, int idAoo, Connection con);
	
	/**
	 * Metodo che restituisce il primo elemento nella coda da trasformare.
	 * 
	 * @param idAoo
	 * @param con
	 * @return
	 */
	CodaTrasformazione getAndLockFirstItem(int idAoo, Connection con);
	
	/**
	 * Metodo che aggiorna lo stato della coda e l'ecentuale messaggio di errore.
	 * 
	 * @param idCoda
	 * @param stato
	 * @param messaggioErrore
	 * @param con
	 */
	void changeStatusItem(int idCoda, int stato, String messaggioErrore, Connection con);

	/**
	 * Aggiorna l'item identificato dall'idCoda in input in modo da: <br/>
	 * <ul>
	 * <li>settare lo stato ad elaborato</li>
	 * <li>registrare i secondi di lavorazione (in input)</li>
	 * <li>eliminare il blob parametri</li>
	 * <li>aggiornare a "" il messaggio d'errore</li>
	 * </ul>
	 * 
	 * @param idCoda
	 * @param secondElapsed
	 * @param connection
	 */
	void closeItem(int idCoda, long secondElapsed, Connection connection);
	
	/**
	 * Inserisce il blob nel DB.
	 * @param codaTrasformazione - coda di trasformazione
	 * @param con
	 * @return
	 */
	int insert(CodaTrasformazione codaTrasformazione, Connection con);
}
