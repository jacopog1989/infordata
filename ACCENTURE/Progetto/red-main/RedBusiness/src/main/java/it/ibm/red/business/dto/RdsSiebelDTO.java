package it.ibm.red.business.dto;

import java.sql.Date;

/**
 * DTO Red Siebel.
 */
public class RdsSiebelDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -1851563040291197467L;

    /**
     * Documento.
     */
	private Integer idDocumento;

	/**
     * Aoo.
     */
	private Integer idAoo;

	/**
	 * Rds Siebel.
	 */
	private String idRdsSiebel;

	/**
	 * Stato rds Siebel.
	 */
	private Integer idStatoRdsSiebel;

	/**
	 * Data apertura.
	 */
	private Date dataAperturaRds;

	/**
	 * Nodo apertura.
	 */
	private Integer idNodoAperturaRds;

	/**
	 * Utente apertura.
	 */
	private Integer idUtenteAperturaRds;

	/**
	 * Titolo RDS.
	 */
	private String titoloRds;

	/**
	 * Descrizione RDS.
	 */
	private String descrizioneRds;

	/**
	 * Email alternativa.
	 */
	private String emailAlternativa;

	/**
	 * Elenco file invalidi.
	 */
	private String elencoFileInviati;
	
	/**
	 * Tipologia.
	 */
	private Integer idTipologia;

	/**
	 * Codice gruppo.
	 */
	private String codiceGruppo;

	/**
	 * Data chiusura.
	 */
	private Date dataChiusuraRds;

	/**
	 * Data invio.
	 */
	private Date dataInvioSiebel;

	/**
	 * Elenco file ricevuti.
	 */
	private String elencoFileRicevuti;

	/**
	 * Soluzione.
	 */
	private String soluzione;
	
	/**
	 * Costruttore di default.
	 */
	public RdsSiebelDTO() {
		super();
	}
	
	/**
	 * Costruttore.
	 * @param idDocumento
	 * @param idAoo
	 * @param idRdsSiebel
	 * @param idStatoRdsSiebel
	 * @param dataAperturaRds
	 * @param idNodoAperturaRds
	 * @param idUtenteAperturaRds
	 * @param titoloRds
	 * @param descrizioneRds
	 * @param emailAlternativa
	 * @param elencoFileInviati
	 * @param idTipologia
	 * @param codiceGruppo
	 */
	public RdsSiebelDTO(final Integer idDocumento, final Integer idAoo, final String idRdsSiebel, final Integer idStatoRdsSiebel, final Date dataAperturaRds,
			final Integer idNodoAperturaRds, final Integer idUtenteAperturaRds, final String titoloRds, final String descrizioneRds,
			final String emailAlternativa, final String elencoFileInviati, final Integer idTipologia, final String codiceGruppo) {
		super();
		this.idDocumento = idDocumento;
		this.idAoo = idAoo;
		this.idRdsSiebel = idRdsSiebel;
		this.idStatoRdsSiebel = idStatoRdsSiebel;
		this.dataAperturaRds = dataAperturaRds;
		this.idNodoAperturaRds = idNodoAperturaRds;
		this.idUtenteAperturaRds = idUtenteAperturaRds;
		this.titoloRds = titoloRds;
		this.descrizioneRds = descrizioneRds;
		this.emailAlternativa = emailAlternativa;
		this.elencoFileInviati = elencoFileInviati;
		this.idTipologia = idTipologia;
		this.codiceGruppo = codiceGruppo;
	}
	
	/**
	 * Restituisce l'id del documento.
	 * @return id documento
	 */
	public Integer getIdDocumento() {
		return idDocumento;
	}
	
	/**
	 * Imposta l'id del documento.
	 * @param idDocumento
	 */
	public void setIdDocumento(final Integer idDocumento) {
		this.idDocumento = idDocumento;
	}
	
	/**
	 * Restituisce l'id dell'AOO.
	 * @return id AOO
	 */
	public Integer getIdAoo() {
		return idAoo;
	}
	
	/**
	 * Imposta l'id dell'AOO.
	 * @param idAoo
	 */
	public void setIdAoo(final Integer idAoo) {
		this.idAoo = idAoo;
	}
	
	/**
	 * Restituisce l'id Rds Siebel.
	 * @return id Rds Siebel
	 */
	public String getIdRdsSiebel() {
		return idRdsSiebel;
	}
	
	/**
	 * Imposta l'id Rds Siebel.
	 * @param idRdsSiebel
	 */
	public void setIdRdsSiebel(final String idRdsSiebel) {
		this.idRdsSiebel = idRdsSiebel;
	}
	
	/**
	 * Restituisce l'id dello stato Rds Siebel.
	 * @return id stato Rds Siebel
	 */
	public Integer getIdStatoRdsSiebel() {
		return idStatoRdsSiebel;
	}
	
	/**
	 * Imposta l'id dello stato Rds Siebel.
	 * @param idStatoRdsSiebel
	 */
	public void setIdStatoRdsSiebel(final Integer idStatoRdsSiebel) {
		this.idStatoRdsSiebel = idStatoRdsSiebel;
	}
	
	/**
	 * Restituisce la data di apertura Rds.
	 * @return data apertura
	 */
 	public Date getDataAperturaRds() {
		return dataAperturaRds;
	}
 	
 	/**
 	 * Imposta la data apertura Rds.
 	 * @param dataAperturaRds
 	 */
	public void setDataAperturaRds(final Date dataAperturaRds) {
		this.dataAperturaRds = dataAperturaRds;
	}
	
	/**
	 * Restiuisce l'id del nodo apertura Rds.
	 * @return id nodo apertura Rds
	 */
	public Integer getIdNodoAperturaRds() {
		return idNodoAperturaRds;
	}
	
	/**
	 * Imposta l'id del nodo di apertura Rds.
	 * @param idNodoAperturaRds
	 */
	public void setIdNodoAperturaRds(final Integer idNodoAperturaRds) {
		this.idNodoAperturaRds = idNodoAperturaRds;
	}
	
	/**
	 * Restituisce l'id dell'utente che apre l'Rds.
	 * @return id utente
	 */
	public Integer getIdUtenteAperturaRds() {
		return idUtenteAperturaRds;
	}
	
	/**
	 * Imposta l'id dell'utente che apre l'Rds.
	 * @param idUtenteAperturaRds
	 */
	public void setIdUtenteAperturaRds(final Integer idUtenteAperturaRds) {
		this.idUtenteAperturaRds = idUtenteAperturaRds;
	}
	
	/**
	 * Restituisce il titolo dell'Rds.
	 * @return titolo
	 */
	public String getTitoloRds() {
		return titoloRds;
	}
	
	/**
	 * Imposta i titolo dell'Rds.
	 * @param titoloRds
	 */
	public void setTitoloRds(final String titoloRds) {
		this.titoloRds = titoloRds;
	}
	
	/**
	 * Restituisce la descrizione Rds.
	 * @return descrizione
	 */
	public String getDescrizioneRds() {
		return descrizioneRds;
	}
	
	/**
	 * Imposta la descrizione Rds.
	 * @param descrizioneRds
	 */
	public void setDescrizioneRds(final String descrizioneRds) {
		this.descrizioneRds = descrizioneRds;
	}
	
	/**
	 * Restituisce la mail alternativa.
	 * @return Email alternativa
	 */
	public String getEmailAlternativa() {
		return emailAlternativa;
	}
	
	/**
	 * Imposta la mail alternativa.
	 * @param emailAlternativa
	 */
	public void setEmailAlternativa(final String emailAlternativa) {
		this.emailAlternativa = emailAlternativa;
	}
	
	/**
	 * Restituisce la data di chiusura dell'Rds.
	 * @return data chiusura Rds
	 */
	public Date getDataChiusuraRds() {
		return dataChiusuraRds;
	}
	
	/**
	 * Imposta la data chiusura dell'Rds.
	 * @param dataChiusuraRds
	 */
	public void setDataChiusuraRds(final Date dataChiusuraRds) {
		this.dataChiusuraRds = dataChiusuraRds;
	}
	
	/**
	 * Restituisce la data di invio Siebel.
	 * @return data invio Siebel
	 */
	public Date getDataInvioSiebel() {
		return dataInvioSiebel;
	}
	
	/**
	 * Imposta la data di invio Siebel.
	 * @param dataInvioSiebel
	 */
	public void setDataInvioSiebel(final Date dataInvioSiebel) {
		this.dataInvioSiebel = dataInvioSiebel;
	}
	
	/**
	 * Restituisce l'elenco dei file inviati, sotto forma di String.
	 * @return elenco file inviati
	 */
	public String getElencoFileInviati() {
		return elencoFileInviati;
	}
	
	/**
	 * Imposta l'elenco dei file inviati.
	 * @param elencoFileInviati
	 */
	public void setElencoFileInviati(final String elencoFileInviati) {
		this.elencoFileInviati = elencoFileInviati;
	}
	
	/**
	 * Restituisce l'elenco dei file ricevuti, sotto forma di String.
	 * @return elenco file ricevuti
	 */
	public String getElencoFileRicevuti() {
		return elencoFileRicevuti;
	}
	
	/**
	 * Imposta l'elenco dei file ricevuti.
	 * @param elencoFileRicevuti
	 */
	public void setElencoFileRicevuti(final String elencoFileRicevuti) {
		this.elencoFileRicevuti = elencoFileRicevuti;
	}
	
	/**
	 * Restituisce la soluzione.
	 * @return soluzione
	 */
	public String getSoluzione() {
		return soluzione;
	}
	
	/**
	 * Imposta la soluzione.
	 * @param soluzione
	 */
	public void setSoluzione(final String soluzione) {
		this.soluzione = soluzione;
	}

	/**
	 * Restituisce l'id della tipologia.
	 * @return id tipologia
	 */
	public Integer getIdTipologia() {
		return idTipologia;
	}

	/**
	 * Imposta l'id della tipologia.
	 * @param idTipologia
	 */
	public void setIdTipologia(final Integer idTipologia) {
		this.idTipologia = idTipologia;
	}

	/**
	 * Restituisce il codice gruppo.
	 * @return codice gruppo
	 */
	public String getCodiceGruppo() {
		return codiceGruppo;
	}

	/**
	 * Imposta il codice gruppo.
	 * @param codiceGruppo
	 */
	public void setCodiceGruppo(final String codiceGruppo) {
		this.codiceGruppo = codiceGruppo;
	}
}