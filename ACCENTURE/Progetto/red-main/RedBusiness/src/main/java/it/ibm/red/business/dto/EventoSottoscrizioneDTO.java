package it.ibm.red.business.dto;

/**
 * DTO per la definizione di un evento di sottoscrizione.
 */
public class EventoSottoscrizioneDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -5423043670240510373L;
	
	/**
	 * Id sottoscrizione.
	 */
	private int idSottoscrizione;
	
	/**
	 * Id evento.
	 */
	private int idEvento;
	
	/**
	 * Tipologia evento.
	 */
	private String tipologiaEvento = "";
	
	/**
	 * Nome evento.
	 */
	private String nomeEvento = "";
	
	/**
	 * Dettaglio evento.
	 */
	private String dettaglioEvento = "";
	
	/**
	 * Descrizione categoria.
	 */
	private String descrizioneCategoria = "";
	
	/**
	 * Identificativo aoo.
	 */
	private int idAoo;
	
	/**
	 * Identificativo utente.
	 */
	private int idUtente;
	
	/**
	 * Identificativo nodo.
	 */
	private int idNodo;
	
	/**
	 * Identificativo ruolo.
	 */
	private int idRuolo;
	
	/**
	 * Scadenza da.
	 */
	private int scadenzaDa;
	
	/**
	 * Scadenza a.
	 */
	private int scadenzaA;
	
	/**
	 * Id tracciamento.
	 */
	private int tracciamento;
	
	/**
	 * Canale trasmissione.
	 */
	private CanaleTrasmissioneDTO canaleTrasmissione;
	
	/**
	 * Costruttore.
	 */
	public EventoSottoscrizioneDTO() {
	}
	
	/**
	 * Costruttore.
	 * @param idEvento - id dell'evento
	 * @param nomeEvento - nome dell'evento
	 * @param dettaglioEvento - dettaglio dell'evento
	 * @param descrizioneCategoria - descrizione della categoria
	 * @param idAoo - id dell'Aoo
	 * @param scadenzaDa
	 * @param scadenzaA
	 * @param canaleTrasmissione - canale di trasmissione
	 */
	public EventoSottoscrizioneDTO(final int idEvento, final String nomeEvento, final String dettaglioEvento, final String descrizioneCategoria,
			final int idAoo, final int scadenzaDa, final int scadenzaA, final CanaleTrasmissioneDTO canaleTrasmissione) {
		super();
		this.idEvento = idEvento;
		this.nomeEvento = nomeEvento;
		this.dettaglioEvento = dettaglioEvento;
		this.descrizioneCategoria = descrizioneCategoria;
		this.idAoo = idAoo;
		this.scadenzaDa = scadenzaDa;
		this.scadenzaA = scadenzaA;
		this.canaleTrasmissione = canaleTrasmissione;
	}
	
	/**
	 * Costruttore.
	 * @param idEvento - id dell'evento
	 * @param tipo
	 * @param nome
	 * @param dettaglioEvento - dettaglio dell'evento
	 */
	public EventoSottoscrizioneDTO(final int idEvento, final String tipo, final String nome, final String dettaglioEvento) {
		this.idEvento = idEvento;
		this.tipologiaEvento = tipo;
		this.nomeEvento = nome;
		this.dettaglioEvento = dettaglioEvento;
	}
	
	/**
	 * Recupera l'id della sottoscrizione.
	 * @return id della sottoscrizione
	 */
	public int getIdSottoscrizione() {
		return idSottoscrizione;
	}

	/**
	 * Imposta l'id della sottoscrizione.
	 * @param idSottoscrizione id della sottoscrizione.
	 */
	public void setIdSottoscrizione(final int idSottoscrizione) {
		this.idSottoscrizione = idSottoscrizione;
	}

	/**
	 * Recupera l'id dell'evento.
	 * @return id dell'evento
	 */
	public int getIdEvento() {
		return idEvento;
	}

	/**
	 * Imposta l'id dell'evento.
	 * 
	 * @param idEvento id dell'evento
	 */
	public void setIdEvento(final int idEvento) {
		this.idEvento = idEvento;
	}

	/**
	 * Recupera la tipologia dell'evento.
	 * @return tipologia dell'evento
	 */
	public String getTipologiaEvento() {
		return tipologiaEvento;
	}

	/**
	 * Imposta la tipologia dell'evento.
	 * @param tipologiaEvento tipologia dell'evento.
	 */
	public void setTipologiaEvento(final String tipologiaEvento) {
		this.tipologiaEvento = tipologiaEvento;
	}

	/**
	 * Recupera il nome dell'evento.
	 * @return nome dell'evento
	 */
	public String getNomeEvento() {
		return nomeEvento;
	}

	/**
	 * Imposta il nome dell'evento.
	 * @param nomeEvento nome dell'evento
	 */
	public void setNomeEvento(final String nomeEvento) {
		this.nomeEvento = nomeEvento;
	}

	/**
	 * Recupera il dettaglio dell'evento.
	 * @return dettaglio dell'evento
	 */
	public String getDettaglioEvento() {
		return dettaglioEvento;
	}

	/**
	 * Imposta il dettaglio dell'evento.
	 * @param dettaglioEvento dettaglio dell'evento.
	 */
	public void setDettaglioEvento(final String dettaglioEvento) {
		this.dettaglioEvento = dettaglioEvento;
	}
	
	/**
	 * Recupera la descrizione della categoria.
	 * @return descrizione della categoria
	 */
	public String getDescrizioneCategoria() {
		return descrizioneCategoria;
	}

	/**
	 * Imposta la descrizione della categoria.
	 * 
	 * @param descrizioneCategoria descrizione della categoria
	 */
	public void setDescrizioneCategoria(final String descrizioneCategoria) {
		this.descrizioneCategoria = descrizioneCategoria;
	}

	/**
	 * Recupera l'id dell'Aoo.
	 * @return id dell'Aoo
	 */
	public int getIdAoo() {
		return idAoo;
	}

	/**
	 * Imposta l'id dell'Aoo.
	 * @param idAoo id dell'Aoo
	 */
	public void setIdAoo(final int idAoo) {
		this.idAoo = idAoo;
	}

	/**
	 * Recupera l'id dell'utente.
	 * @return id dell'utente
	 */
	public int getIdUtente() {
		return idUtente;
	}

	/**
	 * Imposta l'id dell'utente.
	 * @param idUtente id dell'utente
	 */
	public void setIdUtente(final int idUtente) {
		this.idUtente = idUtente;
	}

	/**
	 * Recupera l'id del nodo.
	 * @return id del nodo
	 */
	public int getIdNodo() {
		return idNodo;
	}

	/**
	 * Imposta l'id del nodo.
	 * @param idNodo id del nodo
	 */
	public void setIdNodo(final int idNodo) {
		this.idNodo = idNodo;
	}
	
	/**
	 * Recupera l'attributo scadenzaDa.
	 * @return scadenzaDa
	 */
	public int getScadenzaDa() {
		return scadenzaDa;
	}

	/**
	 * Imposta l'attributo scadenzaDa.
	 * @param scadenzaDa
	 */
	public void setScadenzaDa(final int scadenzaDa) {
		this.scadenzaDa = scadenzaDa;
	}

	/**
	 * Recupera l'attributo scadenzaA.
	 * @return scadenzaA
	 */
	public int getScadenzaA() {
		return scadenzaA;
	}

	/**
	 * Imposta l'attributo scadenzaA.
	 * @param scadenzaA
	 */
	public void setScadenzaA(final int scadenzaA) {
		this.scadenzaA = scadenzaA;
	}
	
	/**
	 * Recupera il tracciamento.
	 * @return tracciamento
	 */
	public int getTracciamento() {
		return tracciamento;
	}

	/**
	 * Imposta il tracciamento.
	 * @param tracciamento
	 */
	public void setTracciamento(final int tracciamento) {
		this.tracciamento = tracciamento;
	}

	/**
	 * @return the idRuolo
	 */
	public int getIdRuolo() {
		return idRuolo;
	}

	/**
	 * @param idRuolo the idRuolo to set
	 */
	public void setIdRuolo(final int idRuolo) {
		this.idRuolo = idRuolo;
	}

	/**
	 * @return
	 */
	public CanaleTrasmissioneDTO getCanaleTrasmissione() {
		return canaleTrasmissione;
	}

	/**
	 * @param canaleTrasmissione
	 */
	public void setCanaleTrasmissione(final CanaleTrasmissioneDTO canaleTrasmissione) {
		this.canaleTrasmissione = canaleTrasmissione;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object other) {
		boolean result = false;
		
		if ((other instanceof EventoSottoscrizioneDTO) && (this.getIdEvento() == ((EventoSottoscrizioneDTO) other).getIdEvento())) {
			result = true;
		}
		
        return result;
	}

	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		return super.hashCode();
	}
}