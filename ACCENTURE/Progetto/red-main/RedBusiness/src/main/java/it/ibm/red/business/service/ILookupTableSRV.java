/**
 * 
 */
package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.persistence.model.LookupTable;
import it.ibm.red.business.service.facade.ILookupTableFacadeSRV;

/**
 * Interfaccia del servizio di gestione liste utenti.
 * 
 * @author APerquoti
 * @author SimoneLungarella
 */
public interface ILookupTableSRV extends ILookupTableFacadeSRV {

	/**
	 * Metodo per recuperare i valori definiti per un selettore e storicizzati con
	 * una data target.
	 * 
	 * @param selector
	 *            identificativo selettore definito in fase di creazione del
	 *            Metadato
	 * @param dateTarget
	 *            data target dei selettori, se occorre recuperare i selettori
	 *            disponibili per una creazione, la data deve essere concorrente
	 *            alla creazione, se occorre recuperarli per una modifica (ad es.
	 *            modifica di un documento) occorre recuperarli impostando la
	 *            <code> dateTarget </code> alla data di creazione, se occorre
	 *            recuperarli per una ricerca la storicizzazione non deve essere
	 *            considerata, quindi occorre impostare quesot parametro a
	 *            <code> null </code>
	 * @param forCreazione
	 *            flag che specifica per quale ambito vengo recuperati i valori
	 *            della lookup
	 * @return selettori valorizzati
	 */
	List<SelectItemDTO> getValues(String selector, Date dateTarget, Long idAoo, Connection connection);

	/**
	 * Metodo per recuperare i valori definiti per un selettore di un'area
	 * organizzativa identificata dall' <code> idAoo </code> e storicizzati con una
	 * <code> dataTarget </code>.
	 * 
	 * @param selector
	 *            identificativo selettore definito in fase di creazione del
	 *            Metadato
	 * @param dateTarget
	 *            data target dei selettori, se occorre recuperare i selettori
	 *            disponibili per una creazione, la data deve essere concorrente
	 *            alla creazione, se occorre recuperarli per una modifica (ad es.
	 *            modifica di un documento) occorre recuperarli impostando la
	 *            <code> dateTarget </code> alla data di creazione, se occorre
	 *            recuperarli per una ricerca la storicizzazione non deve essere
	 *            considerata, quindi occorre impostare quesot parametro a
	 *            <code> null </code>
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @param connection
	 *            connessione al database
	 * @return selettori valorizzati
	 */
	List<SelectItemDTO> getValuesBySelector(String selector, Date dateTarget, Long idAoo, Connection connection);

	/**
	 * Rende persistenti sulla base dati le liste utenti definite dalle
	 * <code> lookups </code>.
	 * 
	 * @param lookups
	 *            liste utenti da memorizzare sul database
	 * @param connection
	 *            connessione al database
	 */
	void saveLookupTables(List<LookupTable> lookups, Connection connection);

	/**
	 * Restituisce true se il metadato <code> lookupTable </code> deve essere
	 * validato con criterio "LIKE".
	 * 
	 * @param idMetadato
	 *            identificativo del metadato da validare
	 * @param codiceAOO
	 *            codice dell'AOO dell'utente in sessione
	 * @return <code> true </code> se l'id del metadato è configurato tra i metadati
	 *         da validare in "Like", <code> false </code> altrimenti
	 */
	boolean hasToCompareWithLike(final Integer idMetadato, final String codiceAOO);
}
