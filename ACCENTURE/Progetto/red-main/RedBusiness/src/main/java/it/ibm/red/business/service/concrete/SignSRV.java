package it.ibm.red.business.service.concrete;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.pdfbox.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.collection.ContentElementList;
import com.filenet.api.collection.DocumentSet;
import com.filenet.api.constants.CompoundDocumentState;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;
import com.google.common.net.MediaType;

import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.BooleanFlag;
import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.constants.Constants.Protocollo;
import it.ibm.red.business.dao.IASignDAO;
import it.ibm.red.business.dao.IAllaccioDAO;
import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.IApprovazioneDAO;
import it.ibm.red.business.dao.IContattoDAO;
import it.ibm.red.business.dao.IContributoDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IParametriProtocolloDAO;
import it.ibm.red.business.dao.IStampigliaturaSegnoGraficoDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dao.IVerificaFirmaDAO;
import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.ApprovazioneDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.AttachmentDTO;
import it.ibm.red.business.dto.CollectedParametersDTO;
import it.ibm.red.business.dto.DestinatarioDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DetailDocumentoDTO;
import it.ibm.red.business.dto.DocumentoDTO;
import it.ibm.red.business.dto.EsitoDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.LocalSignContentDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.PEDocumentoDTO;
import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaNPSDTO;
import it.ibm.red.business.dto.RegistroRepertorioDTO;
import it.ibm.red.business.dto.RicercaRubricaDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.SecurityDTO;
import it.ibm.red.business.dto.SequKDTO;
import it.ibm.red.business.dto.SignerInfoDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.VerificaFirmaPrincipaleAllegatoDTO;
import it.ibm.red.business.dto.VerifyInfoDTO;
import it.ibm.red.business.dto.filenet.AnnotationDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.FilenetAnnotationEnum;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.LibroFirmaPerFirmaResponseEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.SignErrorEnum;
import it.ibm.red.business.enums.SignModeEnum;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.enums.SignTypeEnum.SignTypeGroupEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.enums.StatoVerificaFirmaEnum;
import it.ibm.red.business.enums.TipoAllaccioEnum;
import it.ibm.red.business.enums.TipoApprovazioneEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.enums.TipoOperazioneLibroFirmaEnum;
import it.ibm.red.business.enums.TipoRubricaEnum;
import it.ibm.red.business.enums.TipoSpedizioneEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.enums.TrasformazionePDFInErroreEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.ProtocolloException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.exception.SignException;
import it.ibm.red.business.helper.adobe.AdobeLCHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.TrasformPE;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.helper.signing.SignHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.builder.Builder;
import it.ibm.red.business.nps.dto.DocumentoNpsDTO;
import it.ibm.red.business.nps.exceptions.ProtocolloGialloException;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.PkHandler;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.persistence.model.UtenteFirma;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.ICodaMailSRV;
import it.ibm.red.business.service.IDocumentoSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.IListaDocumentiSRV;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.IPMefSRV;
import it.ibm.red.business.service.IProtocolliEmergenzaSRV;
import it.ibm.red.business.service.IRegistrazioniAusiliarieSRV;
import it.ibm.red.business.service.IRegistroAusiliarioSRV;
import it.ibm.red.business.service.IRegistroRepertorioSRV;
import it.ibm.red.business.service.ISalvaDocumentoSRV;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.service.ISignSRV;
import it.ibm.red.business.service.ITimerRichiesteIntegrazioniSRV;
import it.ibm.red.business.service.ITipologiaDocumentoSRV;
import it.ibm.red.business.service.IUpdateContentSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.IWorkflowSRV;
import it.ibm.red.business.service.asign.step.ContextASignEnum;
import it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV;
import it.ibm.red.business.service.facade.IEventoLogFacadeSRV;
import it.ibm.red.business.service.facade.IFepaFacadeSRV;
import it.ibm.red.business.service.flusso.IAzioniFlussoBaseSRV;
import it.ibm.red.business.service.flusso.IAzioniFlussoSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.StringUtils;
import it.pkbox.client.PKBoxException;
import npstypes.v1.it.gov.mef.DocTipoOperazioneType;

/**
 * Servizi per eseguire le operazioni di firme sui documenti.
 * 
 * @author CPAOLUZI
 * 
 */
@Service
public class SignSRV extends AbstractService implements ISignSRV {

	private static final String ERRORE_RISCONTRATO_DURANTE_LA_RIGENERAZIONE_DEL_CONTENT_DELLA_REGISTRAZIONE_AUSILIARIA_ASSOCIATA_AL_DOCUMENTO = "Errore riscontrato durante la rigenerazione del content della registrazione ausiliaria associata al documento: ";

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -496989538613847500L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SignSRV.class.getName());

	/**
	 * Messaggio di errore recupero content da verificare.
	 */
	private static final String GENERIC_ERROR_RECUPERO_CONTENT_DA_VERIFICARE_MSG = "VerificaFirmaJob.recuperaContentDaVerificare: errore generico.";

	/**
	 * Messaggio di errore generico processo di firma.
	 */
	private static final String GENERIC_ERROR_FIRMA_MSG = "Si è verificato un errore durante il processo di verifica della firma.";

	/**
	 * Label Firma documento.
	 */
	private static final String FIRMA_DOCUMENTO_LABEL = "Firma documento ";
	
	/**
	 * Messaggio errore recupero del content principale. Occorre appendere il
	 * document title al messaggio.
	 */
	private static final String ERROR_RECUPERO_CONTENT_MSG = "Errore durante il recupero del content del documento principale con documentTitle: ";

	/**
	 * Messaggio errore completamento del processo di firma. Occorre appendere il
	 * wobnumber associato al documento al messaggio.
	 */
	private static final String ERROR_COMPLETAMENTO_PROCESSO_FIRMA_MSG = "Errore durante il completamento del processo di firma del documento con WobNumber=";

	/**
	 * Label documento.
	 */
	private static final String DOCUMENTO_LABEL = "Documento ";
	
	/**
	 * Label allaccio principale.
	 */
	private static final String ALLACCIO_PRINCIPALE_LABEL = " allaccio principale ";

	/**
	 * Label codice flusso.
	 */
	private static final String CODICE_FLUSSO_LABEL = "codiceFlusso ";

	/**
	 * Dao gestione aoo.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * Verifica Firma.
	 */
	@Autowired
	private IVerificaFirmaDAO verificaDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IContributoDAO contributoDAO;

	/**
	 * Oggetto per il recupero delle informazioni sui nodi.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Oggetto per il recupero/update del protocollo NPS.
	 */
	@Autowired
	private INpsSRV npsSRV;

	/**
	 * Oggetto per il recupero dei parametri del protocollo.
	 */
	@Autowired
	private IParametriProtocolloDAO parametriProtocolloDAO;

	/**
	 * Oggetto per il recupero/update delle security FN.
	 */
	@Autowired
	private ISecuritySRV securitySRV;

	/**
	 * Dao gestione fascicolo.
	 */
	@Autowired
	private IAllaccioDAO allaccioDAO;

	/**
	 * Oggetto per il recupero/update delle informazioni sugli utenti.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * Oggetto che espone servizi per la protocollazione MEF.
	 */
	@Autowired
	private IPMefSRV pMefSRV;

	/**
	 * Oggetto per la gestione della coda mail.
	 */
	@Autowired
	private ICodaMailSRV codaMailSRV;

	/**
	 * Oggetto per la gestione dei fascicoli.
	 */
	@Autowired
	private IFascicoloSRV fascicoloSRV;

	/**
	 * DAO per la gestione delle approvazioni.
	 */
	@Autowired
	private IApprovazioneDAO approvazioneDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IContattoDAO contattoDAO;

	/**
	 * DAO per la gestione delle tipologie di procedimento.
	 */
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;

	/**
	 * Servizi di interfaccia al sistema Bilancio Enti.
	 */
	@Autowired
	private IFepaFacadeSRV fepaSRV;

	/**
	 * Servizi per la memorizzazione di un documento.
	 */
	@Autowired
	private ISalvaDocumentoSRV salvaDocumentoSRV;

	/**
	 * Servizi per la memorizzazione di un documento.
	 */
	@Autowired
	private IDocumentoSRV documentoSRV;

	/**
	 * Servizi per la gestione di un utente.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IListaDocumentiSRV listaDocumentiSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ITipologiaDocumentoSRV tipologiaDocumentoSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IProtocolliEmergenzaSRV protocolliEmergenzaSRV;

	/**
	 * Servizio per la gestione dei Registri di Repertorio.
	 */
	@Autowired
	private IRegistroRepertorioSRV registroRepertorioSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IStampigliaturaSegnoGraficoDAO stampigliaturaSegnoGraficoDAO;

	/**
	 * Servizio per la gestione delle azioni collegate ai flussi.
	 */
	@Autowired
	private IAzioniFlussoSRV azioniFlussoSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IRegistrazioniAusiliarieSRV registrazioniAusiliarieSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IRegistroAusiliarioSRV registroAusiliarioSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IUpdateContentSRV updateContentSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ITimerRichiesteIntegrazioniSRV timerSRV;
	
	/**
	 * DAO gestione firma asincrona.
	 */
	@Autowired
	private IASignDAO aSignDAO;
	
	
	/**
	 * Fornitore della properties dell'applicazione.
	 */
	private PropertiesProvider pp;
	
	/**
	 * Service.
	 */
	@Autowired
	private IDocumentManagerFacadeSRV documentManagerSRV;
 
	/**
	 * Servizio.
	 */
	@Autowired
	private IWorkflowSRV workflowSRV;
	
	/**
	 * Servizio.
	 */
	@Autowired
	private IEventoLogFacadeSRV eventLogSRV;
	
	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * Metodo che recuperera il numero dei documenti/allegati totali da firmare per
	 * una serie di WF.
	 * 
	 * @param wobs  - Collection dei WobNumbers dei WF sul quale effettuare il
	 *              onteggio dei documenti da firmare.
	 * @param fcDTO - Credenziali FileNet con le quale connettersi.
	 * @return Numero di documenti da firmare.
	 */
	private int getDocumentToSignSizeByWobNumbers(final Collection<String> wobs, final FilenetCredentialsDTO fcDTO, final Long idAoo) {
		int count = 0;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;

		try {
			fpeh = new FilenetPEHelper(fcDTO);
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);

			for (final String wobNumber : wobs) {
				count += getDocumentToSignSizeByWobNumber(wobNumber, fpeh, fceh);
			}
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}

		return count;
	}

	/**
	 * Metodo che recuperera il numero dei documenti/allegati totali da firmare dato
	 * un WobNumber di un WF.
	 * 
	 * @param wobNumber - WobNumber del WF sul quale effettuare il conteggio dei
	 *                  documenti da firmare.
	 * @param fpeh      - Helper con il quale connettersi per recuperare le
	 *                  informazioni sul PE. Se non definito, verrà instanziato con
	 *                  le credenziali 'fcDTO'.
	 * @param fceh      - Helper con il quale connettersi per recuperare le
	 *                  informazioni sul CE. Se non definito, verrà instanziato con
	 *                  le credenziali 'fcDTO'.
	 * @return Numero di documenti da firmare.
	 */
	private int getDocumentToSignSizeByWobNumber(final String wobNumber, final FilenetPEHelper fpeh, final IFilenetCEHelper fceh) {
		int count = 1;

		// Recupero le info dal documento principale e dai suoi allegati dal ce
		final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);
		final PEDocumentoDTO wobDTO = TrasformPE.transform(wob, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO);
		// ### Recupero dettagli del documento principale
		// #####################################################
		count += fceh.getCountAllegatiToSign(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), wobDTO.getIdDocumento());
		// ####################################################################################################

		return count;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#checkDocumentsPreSign(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.List).
	 */
	@Override
	public String checkDocumentsPreSign(final UtenteDTO utente, final List<MasterDocumentRedDTO> documentiSelezionati) {
		String errorMessage = Constants.EMPTY_STRING;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final StringBuilder numDoc4ErrorContentList = new StringBuilder(Constants.EMPTY_STRING);
			final StringBuilder numDoc4ErrorSignProcessList = new StringBuilder("");

			for (final MasterDocumentRedDTO doc : documentiSelezionati) {
				final boolean hasContent = fceh.hasDocumentContentTransfer(doc.getDocumentTitle(), utente.getIdAoo().intValue());
				if (!hasContent) {
					if (Constants.EMPTY_STRING.equals(numDoc4ErrorContentList.toString())) {
						numDoc4ErrorContentList.append(doc.getNumeroDocumento());
					} else {
						numDoc4ErrorContentList.append(", ").append(doc.getNumeroDocumento());
					}
				}

				if (Boolean.TRUE.equals(doc.getbTrasfPdfErrore())) {
					if (Constants.EMPTY_STRING.equals(numDoc4ErrorSignProcessList.toString())) {
						numDoc4ErrorSignProcessList.append(doc.getNumeroDocumento());
					} else {
						numDoc4ErrorSignProcessList.append(", ").append(doc.getNumeroDocumento());
					}
				}
			}

			if (!Constants.EMPTY_STRING.equals(numDoc4ErrorContentList.toString())) {
				errorMessage = "Attenzione, i seguenti documenti non contengono content: " + numDoc4ErrorContentList
						+ ". Su di essi non è possibile effettuare l'operazione di firma remota o locale ma esclusivamente di firma autografa.";
			}

			if (!Constants.EMPTY_STRING.equals(numDoc4ErrorSignProcessList.toString())) {
				if (!StringUtils.isNullOrEmpty(errorMessage)) {
					errorMessage = errorMessage + "\n";
				}

				errorMessage = errorMessage + "Attenzione, sui seguenti documenti non è possibile effettuare la firma: " + numDoc4ErrorSignProcessList + ".";
			}

			if (!StringUtils.isNullOrEmpty(errorMessage)) {
				errorMessage = errorMessage + " Deselezionarli e riprovare.";
			}
		} finally {
			popSubject(fceh);
		}

		return errorMessage;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#checkDocumentsPreSignAutografa(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.List).
	 */
	@Override
	public String checkDocumentsPreSignAutografa(final UtenteDTO utente, final List<MasterDocumentRedDTO> documentiSelezionati) {
		String confirmMessage = Constants.EMPTY_STRING;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final StringBuilder numDoc4ConfirmDigList = new StringBuilder("");
			final StringBuilder numDoc4ConfirmDestList = new StringBuilder("");
			String firmaVisibileMessage = Constants.EMPTY_STRING;

			for (final MasterDocumentRedDTO doc : documentiSelezionati) {
				// check firmadigrgs
				if (Boolean.TRUE.equals(doc.getFlagFirmaDig())) {
					if (Constants.EMPTY_STRING.equals(numDoc4ConfirmDigList.toString())) {
						numDoc4ConfirmDigList.append(doc.getNumeroDocumento());
					} else {
						numDoc4ConfirmDigList.append(", ").append(doc.getNumeroDocumento());
					}
				}

				// check destinatari elettronici
				final Document document = fceh.getDocumentByIdGestionale("" + doc.getDocumentTitle(), null, null, utente.getIdAoo().intValue(), null, null);
				final DetailDocumentoDTO documentDetailDTO = TrasformCE.transform(document, TrasformerCEEnum.FROM_DOCUMENTO_TO_DETAIL);
				if (isOneElettronico(documentDetailDTO.getDestinatariDocumento())) {
					if (Constants.EMPTY_STRING.equals(numDoc4ConfirmDestList.toString())) {
						numDoc4ConfirmDestList.append(doc.getNumeroDocumento());
					} else {
						numDoc4ConfirmDestList.append(", ").append(doc.getNumeroDocumento());
					}
				}

				// check firma visibile allegati
				final DocumentSet allegati = fceh.getAllegatiConContentDaFirmare(StringUtils.cleanGuidToString(document.get_Id()));
				final Iterator<Document> iter = allegati.iterator();
				Document allegato = null;
				while (iter.hasNext()) {
					allegato = iter.next();
					final Boolean isFirmaVisibile = allegato.getProperties().getBooleanValue((pp.getParameterByKey(PropertiesNameEnum.FIRMA_VISIBILE_METAKEY)));
					if (isFirmaVisibile != null && Boolean.TRUE.equals(isFirmaVisibile)) {
						firmaVisibileMessage = "Il documento non sarà firmato elettronicamente neanche per gli allegati indicati con firma visibile";
						break;
					}
				}
			}
			// restituisci warning
			if (!Constants.EMPTY_STRING.equals(numDoc4ConfirmDigList.toString())) {
				confirmMessage = "\nAvete scelto di firmare autografamente i seguenti documenti predisposti per la firma digitale: " + numDoc4ConfirmDigList + ".";
			}

			if (!Constants.EMPTY_STRING.equals(numDoc4ConfirmDestList.toString())) {

				if (!StringUtils.isNullOrEmpty(confirmMessage)) {
					confirmMessage = confirmMessage + "\n";
				}

				confirmMessage = confirmMessage + "I seguenti documenti prevedono almeno un destinatario elettronico: " + numDoc4ConfirmDestList
						+ ". Procedendo con la Firma Autografa la modalità di spedizione sarà modificata in CARTACEA.";
			}

			if (!Constants.EMPTY_STRING.equals(firmaVisibileMessage)) {

				if (!StringUtils.isNullOrEmpty(confirmMessage)) {
					confirmMessage = confirmMessage + "\n";
				}

				confirmMessage = confirmMessage + firmaVisibileMessage;
			}

			confirmMessage = checkProtocolsPreSign(documentiSelezionati, confirmMessage);

			if (!StringUtils.isNullOrEmpty(confirmMessage)) {
				confirmMessage = "Attenzione\n" + confirmMessage + "\nPremere il tasto OK per procedere o CHIUDI per annullare.";
			}
		} finally {
			popSubject(fceh);
		}

		return confirmMessage;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#checkProtocolsPreSign(java.util.List).
	 */
	@Override
	public String checkProtocolsPreSign(final List<MasterDocumentRedDTO> documentiSelezionati) {
		return checkProtocolsPreSign(documentiSelezionati, null);
	}

	private String checkProtocolsPreSign(final List<MasterDocumentRedDTO> documentiSelezionati, final String inConfirmMessage) {

		final StringBuilder numDoc4ExistentProtocolList = new StringBuilder("");
		for (final MasterDocumentRedDTO doc : documentiSelezionati) {

			if (doc.getNumeroProtocollo() != null
					&& !doc.getClasseDocumentale().equals(pp.getParameterByKey(PropertiesNameEnum.DECRETO_DIRIGENZIALE_FEPA_CLASSNAME_FN_METAKEY))) {

				if ("".equals(numDoc4ExistentProtocolList.toString())) {
					numDoc4ExistentProtocolList.append(doc.getNumeroProtocollo());
				} else {
					numDoc4ExistentProtocolList.append(", ").append(doc.getNumeroProtocollo());
				}

			}

		}

		String confirmMessage = "";

		if (inConfirmMessage != null) {
			confirmMessage = inConfirmMessage;
		}

		if (!"".equals(numDoc4ExistentProtocolList.toString())) {
			if (inConfirmMessage != null) {
				confirmMessage = confirmMessage + "\nSi stanno per firmare i seguenti documenti per i quali è già stato staccato un numero di protocollo in uscita: "
						+ numDoc4ExistentProtocolList + ". Tale numero sarà riutilizzato per la firma a meno che non se ne richieda l'annullamento all'assistenza.";
			} else {
				confirmMessage = "Attenzione si stanno per firmare i seguenti documenti per i quali è già stato staccato un numero di protocollo in uscita: "
						+ numDoc4ExistentProtocolList
						+ ". Tale numero sarà riutilizzato per la firma. Se si desidera continuare cliccare su \"Ok\", altrimenti su \"Chiudi\" e richiedere l'annullamento del protocollo all'assistenza.";
			}
		}

		return confirmMessage;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#convertiDestinatariElettroniciInCartacei(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.List).
	 */
	@Override
	public void convertiDestinatariElettroniciInCartacei(final UtenteDTO utente, final List<MasterDocumentRedDTO> documentiSelezionati) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			for (final MasterDocumentRedDTO doc : documentiSelezionati) {
				final Document document = fceh.getDocumentByIdGestionale(Constants.EMPTY_STRING + doc.getDocumentTitle(), null, null, utente.getIdAoo().intValue(), null,
						null);

				final DetailDocumentRedDTO documentDetailDTO = TrasformCE.transform(document, TrasformerCEEnum.FROM_DOCUMENTO_TO_DETAIL_RED);

				final List<DestinatarioRedDTO> destinatari = documentDetailDTO.getDestinatari();
				if (DestinatariRedUtils.checkOneDestinatarioElettronico(destinatari)) {
					final Collection<?> destinatariArray = (Collection<?>) TrasformerCE.getMetadato(document, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
					// Si normalizza il metadato per evitare errori comuni durante lo split delle
					// singole stringhe dei destinatari
					final List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(destinatariArray);

					final String[] destinatariArrayNew = new String[destinatari.size()];
					StringBuilder destinatarioNew = null;

					for (int i = 0; i < destinatari.size(); i++) {

						final String[] destinatarioSplit = destinatariDocumento.get(i);
						destinatarioNew = new StringBuilder(org.apache.commons.lang3.StringUtils.join(destinatarioSplit, ","));

						if (TipologiaDestinatarioEnum.ESTERNO.equals(destinatari.get(i).getTipologiaDestinatarioEnum())
								&& MezzoSpedizioneEnum.ELETTRONICO.equals(destinatari.get(i).getMezzoSpedizioneEnum()) && destinatarioSplit.length >= 5) {

							destinatarioNew = new StringBuilder(destinatarioSplit[0]);
							for (int s = 1; s < destinatarioSplit.length; s++) {
								if (s == 2) {
									destinatarioNew.append(",").append(MezzoSpedizioneEnum.CARTACEO.getId());
								} else {
									destinatarioNew.append(",").append(destinatarioSplit[s]);
								}
							}
						}

						destinatariArrayNew[i] = destinatarioNew.toString();
					}

					final Map<String, Object> metadati = new HashMap<>();
					metadati.put(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY), destinatariArrayNew);
					fceh.updateMetadati(document, metadati);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#checkErrorDocumentsPreSignAutografa(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.List).
	 */
	@Override
	public String checkErrorDocumentsPreSignAutografa(final UtenteDTO utente, final List<MasterDocumentRedDTO> documentiSelezionati) {
		String errorMessage = Constants.EMPTY_STRING;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final StringBuilder numDocInErrorList = new StringBuilder("");

//			<-- Controllo Protocollo d'Emergenza -->
			if (TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo()) && documentiSelezionati.size() > 1) {
				errorMessage = "Attenzione! Il Protocollo NPS è in stato di Emergenza, per poter continuare a protocollare è necessario selezionare un documento alla volta.";
				return errorMessage;
			}

			for (final MasterDocumentRedDTO doc : documentiSelezionati) {
				final Document document = fceh.getDocumentByIdGestionale(doc.getDocumentTitle(), null, null, utente.getIdAoo().intValue(), null, null);
				final DocumentSet allegati = fceh.getAllegati(document.get_Id().toString(), true, true);

				final Iterator<?> it = allegati.iterator();
				while (it.hasNext()) {
					final Document docAllegato = (Document) it.next();
					final Integer idFormatoAllegato = (Integer) TrasformerCE.getMetadato(docAllegato, PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY);
					if (FormatoAllegatoEnum.FIRMATO_DIGITALMENTE.getId().equals(idFormatoAllegato)
							|| FormatoAllegatoEnum.FORMATO_ORIGINALE.getId().equals(idFormatoAllegato)) {
						if ("".equals(numDocInErrorList.toString())) {
							numDocInErrorList.append(doc.getNumeroDocumento());
						} else {
							numDocInErrorList.append(", ").append(doc.getNumeroDocumento());
						}
					}
				}
			}

			if (!Constants.EMPTY_STRING.equals(numDocInErrorList.toString())) {
				errorMessage = "Attenzione si è scelto di firmare autografamente i seguenti documenti che contengono allegati firmati digitalmente o in formato originale: "
						+ numDocInErrorList + ". Deselezionarli e riprovare.";
			}
		} finally {
			popSubject(fceh);
		}

		return errorMessage;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#firmaRemota(it.ibm.red.business.enums.SignTypeEnum,
	 *      java.util.Collection, it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, boolean).
	 */
	@Override
	public Collection<EsitoOperazioneDTO> firmaRemota(final String signTransaction, final SignTypeEnum signType, final Collection<String> wobs,
			final UtenteDTO utente, final String otp, final String pin, final boolean principaleOnlyPAdESVisible) {
		LOGGER.info("Esecuzione della Firma Remota in modalità asincrona");
		return firmaRemota(signTransaction, signType, wobs, utente, otp, pin, principaleOnlyPAdESVisible, false);
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#firmaRemotaMultipla(it.ibm.red.business.enums.SignTypeEnum,
	 *      java.util.Collection, it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, boolean).
	 */
	@Override
	public Collection<EsitoOperazioneDTO> firmaRemotaMultipla(final String signTransaction, final SignTypeEnum signType, final Collection<String> wobs, final UtenteDTO utente, 
			final String otp, final String pin,	final boolean principaleOnlyPAdESVisible) {
		LOGGER.info("Esecuzione della Firma Remota Multipla in modalità asincrona");
		return firmaRemota(signTransaction, signType, wobs, utente, otp, pin, principaleOnlyPAdESVisible, true);
	}

	
	/**
	 * @param signType
	 * @param wobs
	 * @param utente
	 * @param otp
	 * @param pin
	 * @param principaleOnlyPAdESVisible
	 * @param firmaMultipla
	 * @return
	 */
	private Collection<EsitoOperazioneDTO> firmaRemota(final String signTransactionId, final SignTypeEnum signType, final Collection<String> wobs, final UtenteDTO utente, final String otp, final String pin,
			final boolean principaleOnlyPAdESVisible, final boolean firmaMultipla) {
		final Collection<EsitoOperazioneDTO> output = new ArrayList<>();
		
		if (wobs != null) {
			final SignerInfoDTO siUtente = utente.getSignerInfo();
			final int documentToSignSize = getDocumentToSignSizeByWobNumbers(wobs, utente.getFcDTO(), utente.getIdAoo());
			
			LOGGER.info(signTransactionId + " Utente [" + utente.getUsername() + "] con alias [" + siUtente.getSigner() + "] sta per firmare " + documentToSignSize + " documenti");
			 
			final SignHelper sh = new SignHelper(utente.getSignerInfo().getPkHandlerFirma().getHandler(), utente.getSignerInfo().getPkHandlerFirma().getSecurePin(), 
					utente.getDisableUseHostOnly());
			final EsitoDTO esitoST = sh.startTransaction(siUtente.getCustomerinfo(), siUtente.getSigner(), pin, otp, documentToSignSize);
			if (!esitoST.isEsito()) {
				for (final String wobNumber : wobs) {
					output.add(new EsitoOperazioneDTO(wobNumber, esitoST));
				}
				return output;
			}
			
			IFilenetCEHelper fceh = null;
			FilenetPEHelper fpeh = null;
			String currentWobNumber = null;
			String currentDocumentTitle = null;
			VWWorkObject wob;
			Document documentForDetails;
			boolean tokenExpired = false;
			Date startTransaction = new Date();
			try {
				fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
				fpeh = new FilenetPEHelper(utente.getFcDTO());
				
				for (String wobNumber : wobs) {
					currentWobNumber = wobNumber;
					
					// Recupero il workflow dal PE
					wob = fpeh.getWorkFlowByWob(wobNumber, true);
					currentDocumentTitle = TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY).toString();

					tokenExpired = tokenExpired || !checkTokenExpired(startTransaction);
					if (!tokenExpired) {
						startTransaction = new Date();
						
						// Recupero il dettaglio del documento dal CE
						documentForDetails = fceh.getDocumentRedForDetail(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), currentDocumentTitle);
						
						LOGGER.info(signTransactionId + "Utente [" + utente.getUsername() + "] con alias [" + siUtente.getSigner() + "] sta per eseguire una firma remota " 
								+ (firmaMultipla ? "multipla " : "") + "per il workflow " + wobNumber);
						
						// ### FIRMA DEI CONTENT
						EsitoOperazioneDTO esito = firmaContentsDocumento(signTransactionId, wobNumber, documentForDetails, currentDocumentTitle, utente, sh, signType, false, 
								principaleOnlyPAdESVisible, fceh, fpeh);

						if (esito.isEsito()) {
							esito.setNote("Firma remota " + (firmaMultipla ? "multipla " : "") + "del documento effettuata con successo.");
						}
						output.add(esito);
						
						if (!esito.isEsito() && esito.getCodiceErrore() != null) {
							if (esito.getCodiceErrore().equals(SignErrorEnum.PKBOX_AUTH_TOKEN_EXPIRED_COD_ERROR)) {
								tokenExpired = true;
							}
							
							if (esito.getCodiceErrore().equals(SignErrorEnum.INVALID_OTP_FIRMA_COD_ERROR) || 
									esito.getCodiceErrore().equals(SignErrorEnum.INVALID_PIN_FIRMA_COD_ERROR) || 
									esito.getCodiceErrore().equals(SignErrorEnum.INVALID_PIN_LOCKED_FIRMA_COD_ERROR)) {
								// È inutile che ciclo, se ho uno di questi errori, torno direttamente al chiamante che gestirà questo tipo di errori
								break;
							}
						}
					} else {
						EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber, currentDocumentTitle);
						esito.setCodiceErrore(SignErrorEnum.PKBOX_AUTH_TOKEN_EXPIRED_COD_ERROR);
						esito.setNote(SignErrorEnum.PKBOX_AUTH_TOKEN_EXPIRED_COD_ERROR.getMessage());
						output.add(esito);
					}
				}
			} catch (Exception e) {
				LOGGER.error("Errore generico in fase di firma remota: " + e.getMessage(), e);
				EsitoOperazioneDTO esitoKo = new EsitoOperazioneDTO(currentWobNumber, currentDocumentTitle);
				esitoKo.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
				esitoKo.setNote(SignErrorEnum.GENERIC_COD_ERROR.getMessage());
				output.add(esitoKo);
			} finally {
				popSubject(fceh);
				logoff(fpeh);
			}
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#recuperaGlifoDelegato(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public HashMap<String, byte[]> recuperaGlifoDelegato(UtenteDTO utenteDelegato) {
		HashMap<String, byte[]> mapGlifoUtente = new HashMap<>();
		 
		UtenteFirma uf = utenteSRV.getUtenteFirmaById(utenteDelegato.getId());
		if(uf != null && uf.getImageFirma() != null) {     
			mapGlifoUtente.put("Glifo personale", uf.getImageFirma());
		}
		
		UtenteFirma ufDelegata = utenteSRV.getGlifoDelegato(utenteDelegato.getId(), utenteDelegato.getIdUtenteDelegante());
		if(ufDelegata != null && ufDelegata.getImageFirma() != null) {  
			mapGlifoUtente.put("Glifo delegato", ufDelegata.getImageFirma());
		}

		return mapGlifoUtente;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#recuperaGlifoDelegante(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public void recuperaGlifoDelegante(UtenteDTO utenteDelegato) {  
		UtenteFirma uf = utenteSRV.getUtenteFirmaById(utenteDelegato.getIdUtenteDelegante());
		if(uf != null && uf.getImageFirma() != null) {
			SignerInfoDTO signerInfo = utenteDelegato.getSignerInfo();
			signerInfo.setImageFirma(uf.getImageFirma());
			signerInfo.setReason(uf.getReason()); 
		}  
	}

	
	/**
	 * @param wobs
	 * @param utente
	 * @param protocolloEmergenza
	 * @return
	 */
	@Override
	public Collection<EsitoOperazioneDTO> firmaAutografa(final String signTransactionId, final Collection<String> wobs, final UtenteDTO utente, final ProtocolloEmergenzaDocDTO protocolloEmergenza) {
		final Collection<EsitoOperazioneDTO> output = new ArrayList<>();
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;
		String currentWob = null;
		String documentTitle = null;
		VWWorkObject wob = null;
		Document documentForDetails = null;
		String logId = signTransactionId;
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			
			for (final String wobNumber : wobs) {
				currentWob = wobNumber;
				
				// Recupero il workflow dal PE
				wob = fpeh.getWorkFlowByWob(wobNumber, true);
				LOGGER.info(signTransactionId + " recuperato workflow su Filenet " + wobNumber);
								
				documentTitle = TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY).toString();
				
				// Recupero il dettaglio del documento dal CE
				documentForDetails = fceh.getDocumentRedForDetail(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle);
				
				LOGGER.info(signTransactionId + " recuperato documento su Filenet " + documentTitle);
				
				logId = logId + documentTitle;
				
				// ### FIRMA DEI CONTENT
				final EsitoOperazioneDTO esitoFirmaDoc = firmaContentsDocumento(logId, wobNumber, documentForDetails, documentTitle, utente, null, null, true, false, fceh, fpeh);
			
				if (esitoFirmaDoc.isEsito()) {
					// ### GESTIONE DEL PROTOCOLLO EMERGENZA
					final EsitoOperazioneDTO esitoProtocolloEmergenza = gestisciProtocolloEmergenza(wobNumber, documentForDetails, documentTitle, protocolloEmergenza, utente, fceh);
					LOGGER.info(logId + " gestito protocollo di emergenza");
					
					if (esitoProtocolloEmergenza.isEsito()) {
						esitoFirmaDoc.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
					}
					output.add(esitoFirmaDoc);
				}
				
			}
		} catch (final Exception e) {
			LOGGER.error("Errore generico in fase di firma autografa: " + e.getMessage(), e);
			EsitoOperazioneDTO esitoKo = new EsitoOperazioneDTO(currentWob);
			esitoKo.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
			esitoKo.setNote(SignErrorEnum.GENERIC_COD_ERROR.getMessage());
			output.add(esitoKo);
		} finally {
			popSubject(fceh);
			logoff(fpeh);
		}

		return output;
	}

	/**
	 * @param wobNumber
	 * @param documentForDetails
	 * @param documentTitle
	 * @param protocolloEmergenza
	 * @param utente
	 * @param fceh
	 * @return
	 */
	private EsitoOperazioneDTO gestisciProtocolloEmergenza(final String wobNumber, final Document documentForDetails, final String documentTitle, final ProtocolloEmergenzaDocDTO protocolloEmergenza, 
			final UtenteDTO utente, final IFilenetCEHelper fceh) {
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber, documentTitle);
		Connection conEmergenza = null;
		
		try {
			if (protocolloEmergenza != null && TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo())) {
				conEmergenza = setupConnection(getDataSource().getConnection(), true);
				
				String oggetto = (String) TrasformerCE.getMetadato(documentForDetails, PropertiesNameEnum.OGGETTO_METAKEY);
				String tipologiaDocumento = tipologiaDocumentoSRV.getDescTipologiaDocumentoById(
						(Integer) TrasformerCE.getMetadato(documentForDetails, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY));
				
				// Si verifica se è già presente un protocollo di emergenza per il documento
				ProtocolloNpsDTO protocolloNps = protocolliEmergenzaSRV.getInfoEmergenzaPerProtocolloNps(documentTitle, oggetto, tipologiaDocumento, utente.getIdAoo(), conEmergenza);
				
				if (protocolloNps == null) {
					// Si registrano le informazioni di emergenza sulla base dati
					protocolloNps = protocolliEmergenzaSRV.registraInfoEmergenza(documentTitle, utente.getIdAoo(), utente.getCodiceAoo(), 
							protocolloEmergenza.getNumeroProtocolloEmergenza(), protocolloEmergenza.getAnnoProtocolloEmergenza(), protocolloEmergenza.getDataProtocolloEmergenza(), 
							Protocollo.TIPO_PROTOCOLLO_USCITA, oggetto,tipologiaDocumento, conEmergenza);
					
					if (protocolloNps != null && protocolloNps.getNumeroProtocollo() > 0 && !StringUtils.isNullOrEmpty(protocolloNps.getIdProtocollo())) {
						// Si inseriscono i metadati relativi al protocollo emergenza nel CE
						ProtocolloDTO protocollo = new ProtocolloDTO(protocolloNps.getIdProtocollo(), protocolloNps.getNumeroProtocollo(), protocolloNps.getAnnoProtocollo(), 
								protocolloNps.getDataProtocollo(), protocolloNps.getDescrizioneTipologiaDocumento(), protocolloNps.getOggetto(), protocolloNps.getCodiceRegistro());
						
						fceh.setPropertiesAndSaveDocument(documentForDetails, getMetadatiProtocolloToUpdate(protocollo, utente, true), true);
					} else {
						throw new RedException("Errore durante la scrittura nella base dati");
					}
					
				} else {
					LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": è già presente un protocollo di emergenza per il documento");
				}
				
				commitConnection(conEmergenza);
			}
			
			impostaEsitoOk(esito);
			
		} catch (Exception e) {
			rollbackConnection(conEmergenza);
			String errore = "Errore nella registrazione delle informazioni del protocollo di emergenza";
			LOGGER.error(errore + ": " + e.getMessage(), e);
			esito.setEsito(false);
			esito.setNote(errore);
		} finally {
			closeConnection(conEmergenza);
		}
		
		return esito;
	}

	/**
	 * Cades.
	 *
	 * @param fcDTO  the fc DTO
	 * @param wobs   the wobs
	 * @param utente the utente
	 * @param otp    the otp
	 * @return the collection
	 */
	@Override
	public final Collection<EsitoOperazioneDTO> cades(final Collection<String> wobs, final UtenteDTO utente, final String otp) {
		final SignerInfoDTO siUtente = utente.getSignerInfo();
		return firmaRemota("cades" + UUID.randomUUID().toString(), SignTypeEnum.CADES, wobs, utente, otp, siUtente.getPin(), true, false);
	}

	/**
	 * Pades non visibile.
	 *
	 * @param fcDTO  the fc DTO
	 * @param wobs   the wobs
	 * @param utente the utente
	 * @param otp    the otp
	 * @return the collection
	 */
	@Override
	public final Collection<EsitoOperazioneDTO> padesNonVisibile(final Collection<String> wobs, final UtenteDTO utente, final String otp) {
		final SignerInfoDTO siUtente = utente.getSignerInfo();
		return firmaRemota("padesNonVisibile" + UUID.randomUUID().toString(), SignTypeEnum.PADES_INVISIBLE, wobs, utente, otp, siUtente.getPin(), true, false);
	}

	/**
	 * Pades visibile.
	 *
	 * @param fcDTO  the fc DTO
	 * @param wobs   the wobs
	 * @param utente the utente
	 * @param otp    the otp
	 * @return the collection
	 */
	@Override
	public final Collection<EsitoOperazioneDTO> padesVisibile(final Collection<String> wobs, final UtenteDTO utente, final String otp) {
		final SignerInfoDTO siUtente = utente.getSignerInfo();
		return firmaRemota("padesVisibile" + UUID.randomUUID().toString(), SignTypeEnum.PADES_VISIBLE, wobs, utente, otp, siUtente.getPin(), true, false);
	}

	/**
	 * Pades posizionale.
	 *
	 * @param fcDTO     the fc DTO
	 * @param wobNumber the wob number
	 * @param utente    the utente
	 * @param page      the page
	 * @param x         the x
	 * @param y         the y
	 * @param otp       the otp
	 * @return the esito operazione DTO
	 */
	@Override
	public final EsitoOperazioneDTO padesPosizionale(final String wobNumber, final UtenteDTO utente, final Integer page, final Integer x, final Integer y, final String otp) {
		final SignerInfoDTO siUtente = utente.getSignerInfo();
		final Collection<EsitoOperazioneDTO> esiti = firmaRemota("padesPosizionale" + UUID.randomUUID().toString(), SignTypeEnum.PADES_POSITONAL, Arrays.asList(wobNumber), utente, otp, siUtente.getPin(), true, false);
		if (esiti != null && !esiti.isEmpty()) {
			return esiti.iterator().next();
		} else {
			return null;
		}
	}

	/**
	 * Check sull'abilitazione dell'utente alla firma del documento relativo al WF
	 * specificato.
	 * 
	 * @param wobNumber  - WobNumber del WF su cui eseguire il check
	 * @param utente     - Utente che esegue l'operazione
	 * @param connection - Connessione allo strato di peristenza
	 * @param fpeh       - Helper per la connessione al PE
	 * @return esito dell'operazione di check
	 */
	private EsitoOperazioneDTO checkSignAuthorization(final String wobNumber, final UtenteDTO utente, final Connection connection, final FilenetPEHelper fpeh,
			final String documentTitle) {
		Long idUtenteDelegante = null;
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		if (utenteDAO.isDelegato(utente.getIdRuolo(), connection)) {
			final Utente dirigente = nodoDAO.getNodo(utente.getIdUfficio(), connection).getDirigente();
			if (dirigente != null) {
				idUtenteDelegante = dirigente.getIdUtente();
			}
		}
		if (Boolean.FALSE.equals(isWorkFlowInLibroFirmaUtente(wobNumber, utente, idUtenteDelegante, fpeh, documentTitle))) {
			esito.setCodiceErrore(SignErrorEnum.FORBIDDEN_OPERATION_COD_ERROR);
			esito.setNote(SignErrorEnum.FORBIDDEN_OPERATION_COD_ERROR.getMessage() + " Il documento non risulta sul Libro firma dell'utente.");
			return esito;
		}
		impostaEsitoOk(esito);
		return esito;
	}
	
	/**
	 * @param esito
	 */
	private static void impostaEsitoOk(final EsitoOperazioneDTO esito) {
		esito.setEsito(true);
		esito.setCodiceErrore(null);
		esito.setNote(null);
	}

	/**
	 * Metodo che dato un WobNumber di un WF, controlla che il WF sia sul libro
	 * firma dell'utente desiderato.
	 * 
	 * @param wobNumber         - WobNumber del WF su cui eseguire il check.
	 * @param idUtente          - Identificativo dell'utente per il quale si esegue
	 *                          l'operazione di controllo.
	 * @param idUfficio         - Identificativo dell'ufficio per il quale si
	 *                          eseguel'operazione di controllo.
	 * @param idUtenteDelegante - Identificativo dell'utente delegante per il quale
	 *                          si esegue l'operazione di controllo.
	 * @param idClientAoo       - Client AOO con il quale eseguire l'operazione.
	 * @param fpeh              - Helper con il quale connettersi per recuperare le
	 *                          informazioni sul PE.
	 * @return Esito del controllo.
	 */

	private Boolean isWorkFlowInLibroFirmaUtente(final String wobNumber, final UtenteDTO utente, final Long idUtenteDelegante, final FilenetPEHelper fpeh,
			final String documentTitle) {
		final Long idUtente = utente.getId();
		final Long idUfficio = utente.getIdUfficio();
		final String idClientAoo = utente.getFcDTO().getIdClientAoo();

		try {
			Boolean output = false;
			final List<Long> idUtenteDestinatari = new ArrayList<>();
			idUtenteDestinatari.add(idUtente);
			if (idUtenteDelegante != null) {
				idUtenteDestinatari.add(idUtenteDelegante);
			}

			final VWQueueQuery query = fpeh.getWorkFlowsByWobQueueFilterRed(null, DocumentQueueEnum.NSD, null, idUfficio, idUtenteDestinatari, idClientAoo, null,
					BooleanFlag.TRUE, false, Arrays.asList(documentTitle), null);
			if (query.hasNext()) {
				output = true;
			} else {
				final VWQueueQuery queueQuery = (VWQueueQuery) listaDocumentiSRV.getFepaQueryObject(DocumentQueueEnum.DD_DA_FIRMARE, null, utente);
				while (queueQuery.hasNext()) {
					final VWWorkObject obj = (VWWorkObject) queueQuery.next();
					if (obj.getWorkflowNumber().equalsIgnoreCase(wobNumber)) {
						output = true;
						break;
					}
				}
			}
			return output;
		} catch (final Exception e) {
			throw new FilenetException(e);
		}
	}

	/**
	 * Check sull'abilitazione dell'utente alla firma degli allegati relativo al WF specificato.
	 * 
	 * @param wobNumber - WobNumber del WF su cui eseguire il check.
	 * @param allegati  - DocumentSet degli allegati sul quale effettuare in check
	 * @param ste       - Tipo di firma richiesta
	 * @return Esito del Check.
	 */
	private EsitoOperazioneDTO checkAuthAttachmentSign(final String wobNumber, final DocumentSet allegati, final SignTypeEnum ste) {
		final EsitoOperazioneDTO esitoCheck = new EsitoOperazioneDTO(wobNumber);
		
		if (!ste.equals(SignTypeEnum.CADES) && allegati != null) {
			
			final Iterator<?> it = allegati.iterator();
			while (it.hasNext()) {
				final Document docAllegato = (Document) it.next();
				String mimeTypeAllegato = null;
				if (FilenetCEHelper.hasDocumentContentTransfer(docAllegato)) {
					mimeTypeAllegato = FilenetCEHelper.getDocumentContentTransfer(docAllegato).get_ContentType();
				}
				if (!MediaType.PDF.toString().equalsIgnoreCase(mimeTypeAllegato)) {
					esitoCheck.setCodiceErrore(SignErrorEnum.DOCUMENTO_ALLEGATI_NO_PDF_FIRMA_COD_ERROR);
					esitoCheck.setNote(SignErrorEnum.DOCUMENTO_ALLEGATI_NO_PDF_FIRMA_COD_ERROR.getMessage());
					return esitoCheck;
				}
			}
			
		}
		
		impostaEsitoOk(esitoCheck);
		return esitoCheck;
	}

	/**
	 * Esegue le operazioni di firma remota su un content.
	 * 
	 * @param signTransactionId.
	 * @param siDTO    - Informazione dell'utente firmatario.
	 * @param shTrans  - Helper per la firma remota.
	 * @param ste      - Tipo di firma digitale.
	 * @param source   - Content da firmare
	 * @param tipoMime - MimeType del content da firmare.
	 * @return Content firmato.
	 */
	private byte[] remoteSignContent(final String signTransactionId, final SignerInfoDTO siDTO, final SignHelper shTrans, final SignTypeEnum ste, final byte[] source, final String tipoMime, final String stampigliatura, final Aoo aoo) {
		byte[] output = null;
		final byte[] imageFirma = siDTO.getImageFirma();
		if (SignTypeEnum.CADES.equals(ste) && tipoMime.equalsIgnoreCase(MediaType.PDF.toString()) && !StringUtils.isNullOrEmpty(stampigliatura)) {
			SignHelper sh = new SignHelper(aoo.getPkHandlerTimbro().getHandler(), aoo.getPkHandlerTimbro().getSecurePin(), aoo.getDisableUseHostOnly());
			byte[] contentStampigliato = sh.applicaTimbroProtocolloPrincipaleUscita(
					aoo.getSignerTimbro(), aoo.getPinTimbro(), source, stampigliatura, aoo.isConfPDFAPerHandler());
			
			LOGGER.info(signTransactionId + " firma CADES - eseguito applicaTimbroProtocolloPrincipaleUscita");
			
			if (contentStampigliato != null) {
				output = shTrans.cadesSign(contentStampigliato);
			} else {
				output = shTrans.cadesSign(source);
			}

		} else if (SignTypeEnum.CADES.equals(ste) || (!tipoMime.equalsIgnoreCase(MediaType.PDF.toString()))) {
			output = shTrans.cadesSign(source);
		} else if (SignTypeEnum.PADES_VISIBLE.equals(ste)) {
			output = shTrans.padesQuickVisibleSign(source, imageFirma, siDTO.getReason(), siDTO.getSigLayout());
		} else if (SignTypeEnum.PADES_INVISIBLE.equals(ste)) {
			output = shTrans.padesInvisibleSign(source, siDTO.getReason());
		}
		return output;
	}

	/**
	 * Firma un documento con la gestione dell'errore.
	 * 
	 * @param signTransactionId.
	 * @param wobNumber       - WobNumber del WF del documento da firmare.
	 * @param numeroDocumento - IDentificativo del documento da firmare.
	 * @param siDTO           - Dto delle informazioni di firma dell'utente che
	 *                        effettua l'operazione di firma.
	 * @param mimeType        - MimeType del documento da firmare.
	 * @param ste             - Tipo di firma.
	 * @param detailDTO       - DTO delle informazioni del documento da firmare.
	 * @param isPrincipale    - Documento principale o allegato.
	 * @return Esito della firma.
	 */
	private EsitoOperazioneDTO signDocument(final String signTransactionId, final String wobNumber, final Integer numeroDocumento, final SignerInfoDTO siDTO, final String mimeType, final SignTypeEnum ste,
			final DetailDocumentoDTO detailDTO, final boolean isPrincipale, final SignHelper sh, final Document documentForDetails, final UtenteDTO utente, final Aoo aoo) {
		final EsitoOperazioneDTO esitoOperazioneFirma = new EsitoOperazioneDTO(wobNumber);
		esitoOperazioneFirma.setNomeDocumento(detailDTO.getNomeFile());
		try {

			String tipoDocumento = "pricipale";
			if (!isPrincipale) {
				tipoDocumento = "allegato";
			}

			LOGGER.info(signTransactionId + " Firma digitale del documento " + tipoDocumento + " [" + wobNumber + "][" + numeroDocumento + "] con alias di firma [" + sh.getSigner() + "]");
				
			boolean protocolloPresente = detailDTO.getProtocollo().getNumeroProtocollo() != 0;
			boolean onlyDestinatariInterni = detailDTO.isHasDestinatariInterni() && !detailDTO.isHasDestinatariEsterni();
			String stampigliatura = null;
			final boolean isProtocollazioneAnticipata = detailDTO.getIdMomentoProtocollazione() != null && MomentoProtocollazioneEnum.PROTOCOLLAZIONE_NON_STANDARD.getId() == detailDTO.getIdMomentoProtocollazione();
			if (isPrincipale && SignTypeEnum.CADES.equals(ste) && (protocolloPresente || onlyDestinatariInterni) && !isProtocollazioneAnticipata) {
				stampigliatura = getStampigliaturaProtocollo(detailDTO, documentForDetails, detailDTO.getProtocollo(), utente, protocolloPresente, true);
				LOGGER.info(signTransactionId + " stampigliatura di protocollo " + stampigliatura);
			}
			
			detailDTO.setContent(remoteSignContent(signTransactionId, siDTO, sh, ste, detailDTO.getContent(), mimeType, stampigliatura, aoo));
			
			LOGGER.info(signTransactionId + " Firma digitale applicata");

		} catch (final SignException e) {
			
			return gestisciSignException(esitoOperazioneFirma, e, true, isPrincipale, numeroDocumento);
			
		} catch (final Exception e) {
			
			return gestisciSignException(esitoOperazioneFirma, e, false, isPrincipale, numeroDocumento);
			
		}
		
		impostaEsitoOk(esitoOperazioneFirma);
		return esitoOperazioneFirma;
	}
	
	/**
	 * Costruzione dell'esito a fronte di un errore in fase di firma.
	 * 
	 * @param esitoOperazioneFirma
	 * @param exc
	 * @param isSignException
	 * @param isPrincipale
	 * @param numeroDocumento
	 * @return
	 */
	private EsitoOperazioneDTO gestisciSignException(final EsitoOperazioneDTO esitoOperazioneFirma, final Exception exc, final boolean isSignException, final boolean isPrincipale, final Integer numeroDocumento) {
		
		String tipoDocumento = "pricipale";
		if (!isPrincipale) {
			tipoDocumento = "allegato";
		}
		LOGGER.error("Errore durante la firma remota del documento " + tipoDocumento + " [ID=" + numeroDocumento + "]", exc);
		
		if(isSignException) {

			SignException e = (SignException)exc;
			
			if ((e instanceof SignException) && (e.getCause() instanceof PKBoxException)) {
				final PKBoxException ePK = (PKBoxException) e.getCause();
				if ((ePK.GetErrorCode() == PKBoxException.PKBOX_PARAM_INVALID_OTP) || (ePK.getMessage() != null && ePK.getMessage().endsWith("Invalid OTP")
						|| ePK.getMessage().contains("it.pkbox.client.PKBoxException: Invalid response status code: 500"))) {
					esitoOperazioneFirma.setCodiceErrore(SignErrorEnum.INVALID_OTP_FIRMA_COD_ERROR);
					esitoOperazioneFirma.setNote(SignErrorEnum.INVALID_OTP_FIRMA_COD_ERROR.getMessage());
					return esitoOperazioneFirma;
				} else if ((ePK.GetErrorCode() == PKBoxException.PKBOX_PARAM_INVALID_PIN) || (ePK.GetErrorCode() == PKBoxException.PKBOX_PARAM_CERTIFICATE_CHAIN_NOT_FOUND)) {
					esitoOperazioneFirma.setCodiceErrore(SignErrorEnum.INVALID_PIN_FIRMA_COD_ERROR);
					esitoOperazioneFirma.setNote(SignErrorEnum.INVALID_PIN_FIRMA_COD_ERROR.getMessage());
					return esitoOperazioneFirma;
				} else if ((ePK.GetErrorCode() == PKBoxException.PKBOX_PARAM_PIN_LOCKED)) {
					esitoOperazioneFirma.setCodiceErrore(SignErrorEnum.INVALID_PIN_LOCKED_FIRMA_COD_ERROR);
					esitoOperazioneFirma.setNote(SignErrorEnum.INVALID_PIN_LOCKED_FIRMA_COD_ERROR.getMessage());
					return esitoOperazioneFirma;
				}
			}
		
		}

		if (exc.getMessage() != null && exc.getMessage().endsWith("the authentication token is expired")) {
			esitoOperazioneFirma.setCodiceErrore(SignErrorEnum.PKBOX_NO_CERTIFICATE_FOUND_COD_ERROR);
			esitoOperazioneFirma.setNote(SignErrorEnum.PKBOX_NO_CERTIFICATE_FOUND_COD_ERROR.getMessage());
			return esitoOperazioneFirma;
		}
	
		esitoOperazioneFirma.setCodiceErrore(SignErrorEnum.PKBOX_SIGN_COD_ERROR);
		esitoOperazioneFirma.setNote(SignErrorEnum.PKBOX_SIGN_COD_ERROR.getMessage());
		return esitoOperazioneFirma;
		
	}

	
	/**
	 * @param datiProtocolloIdDoc
	 * @param documentDetailDTO
	 * @param documentForDetails
	 * @param esitoOperazioneFirma
	 * @param utente
	 * @param fceh
	 * @param connection
	 * @return
	 */
	private ProtocolloDTO salvaDatiProtocollo(final String signTransactionId, final ProtocolloNpsDTO datiProtocolloIdDoc, final DetailDocumentoDTO documentDetailDTO, final Document documentForDetails,
			final EsitoOperazioneDTO esitoOperazioneFirma, final UtenteDTO utente, final IFilenetCEHelper fceh, final Connection connection) {
		
		if (datiProtocolloIdDoc == null || datiProtocolloIdDoc.getIdProtocollo() == null || datiProtocolloIdDoc.getIdProtocollo().trim().length() <= 0) {
			throw new ProtocolloException("Non è stato possibile recuperare l'ID del protocollo NPS. "
					+ "Per il processo di firma tramite inviaAtto NPS si necessita dell'ID del protocollo. Contattare l'assistenza. " + "\nDati del documento: "
					+ documentDetailDTO);
		}

		final ProtocolloDTO protocollo = new ProtocolloDTO(datiProtocolloIdDoc.getIdProtocollo(), datiProtocolloIdDoc.getNumeroProtocollo(),
				datiProtocolloIdDoc.getAnnoProtocollo(), datiProtocolloIdDoc.getDataProtocollo(), datiProtocolloIdDoc.getDescrizioneTipologiaDocumento(),
				datiProtocolloIdDoc.getOggetto(), datiProtocolloIdDoc.getCodiceRegistro());

		if (protocollo.getNumeroProtocollo() <= 0) {
			throw new ProtocolloException("Non è stato possibile recuperare il protocollo. Contattare l'assistenza.");
		}

		LOGGER.info(signTransactionId + ": [Flusso] protocollo " + protocollo);

		doActionsProtocolloBooked(signTransactionId, documentDetailDTO, documentForDetails, protocollo, utente, fceh, connection);

		esitoOperazioneFirma.setAnnoProtocollo(protocollo.getAnnoProtocolloInt());
		esitoOperazioneFirma.setNumeroProtocollo(protocollo.getNumeroProtocollo());
		esitoOperazioneFirma.setRegistroRepertorio(documentDetailDTO.getIsRegistroRepertorio());

		LOGGER.info(signTransactionId + ": [Flusso] savati dati protocollo ");

		return protocollo;
	}
	
	/**
	 * @param signTransactionId
	 * @param codiceFlusso
	 * @param documentDetail
	 * @param acl
	 * @param utente
	 * @param fceh
	 * @param connection
	 * @param idDocumentoPrincipaleNPS
	 * @param idAllegatiNPS
	 * @return
	 */
	private ProtocolloNpsDTO inviaAtto(final String signTransactionId, final String codiceFlusso, final DetailDocumentoDTO documentDetail, final String acl, final UtenteDTO utente, final IFilenetCEHelper fceh, final Connection connection, 
			final String idDocumentoPrincipaleNPS, final List<String> idAllegatiNPS) {
		String identificatoreProcesso = null;
		TipoAllaccioEnum tipoAllaccio = null;
		if (codiceFlusso.equals(Constants.Varie.CODICE_FLUSSO_ATTO_DECRETO)) {
			LOGGER.info(signTransactionId + ": [Flusso " + codiceFlusso + "]");
		} else {
			final RispostaAllaccioDTO allaccioPrincipale = allaccioDAO.getAllaccioPrincipale(Integer.parseInt(documentDetail.getDocumentTitle()), connection);
			
			LOGGER.info(signTransactionId + ": recuperato allaccio principale su database " + allaccioPrincipale.getIdDocumentoAllacciato());
			
			tipoAllaccio = allaccioPrincipale.getTipoAllaccioEnum();
			
			final List<String> props = Arrays.asList(pp.getParameterByKey(PropertiesNameEnum.IDENTIFICATORE_PROCESSO_METAKEY));
			final Document document = fceh.getDocumentByIdGestionale(allaccioPrincipale.getIdDocumentoAllacciato(), props, null, utente.getIdAoo().intValue(), null, null);
			LOGGER.info(signTransactionId + ": recuperato allaccio principale su filenet");
			identificatoreProcesso = TrasformerCE.getMetadato(document, 
					PropertiesNameEnum.IDENTIFICATORE_PROCESSO_METAKEY) != null ? TrasformerCE.getMetadato(document, PropertiesNameEnum.IDENTIFICATORE_PROCESSO_METAKEY).toString() : Constants.EMPTY_STRING;
			LOGGER.info(signTransactionId + ": [Flusso " + codiceFlusso + "] identificatore di processo " + identificatoreProcesso + " associato all'allaccio principale " 
					+ allaccioPrincipale.getIdDocumentoAllacciato() + " di tipo " + allaccioPrincipale.getTipoAllaccioEnum().toString());
		}

		final String[] idAllegatiNPSArray = new String[idAllegatiNPS.size()];
		idAllegatiNPS.toArray(idAllegatiNPSArray);
		return npsSRV.inviaAtto(codiceFlusso, documentDetail, acl, tipoAllaccio, identificatoreProcesso, utente, idDocumentoPrincipaleNPS, idAllegatiNPSArray, connection);
	}
	
	/**
	 * @param signTransactionId
	 * @param allegatiToSend
	 * @param documentTitleDocPrincipale
	 * @param cades
	 * @param firmaAutografa
	 * @param fceh
	 * @param idDocumentiNPS
	 * @param guidDocumentiSentToNPS
	 * @throws IOException
	 */
	private void uploadAttachSyncNPS(final String signTransactionId, final Integer idAoo, final Map<String, byte[]> allegatiToSend, final String documentTitleDocPrincipale, final boolean cades, final boolean firmaAutografa, final IFilenetCEHelper fceh, 
			final List<String> idDocumentiNPS, final List<String> guidDocumentiSentToNPS) throws IOException {
						
		for (final Entry<String, byte[]> allegatoToSend : allegatiToSend.entrySet()) {
			final Document allegatoFilenet = fceh.getDocumentByGuid(allegatoToSend.getKey(), ArrayUtils.toArray(pp.getParameterByKey(PropertiesNameEnum.DA_FIRMARE_METAKEY),
					pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), 
					pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), PropertyNames.CONTENT_TYPE));
			
			final String documentTitle = (String) TrasformerCE.getMetadato(allegatoFilenet, pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
			final String nomeFile = (String) TrasformerCE.getMetadato(allegatoFilenet, pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));
			final String oggetto = (String) TrasformerCE.getMetadato(allegatoFilenet, pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY));
			final boolean daFirmare = FilenetCEHelper.isDaFirmare(allegatoFilenet);
			final String mimeType = allegatoFilenet.get_MimeType();
			
			final String idAllegatoNPS = uploadSyncNPS(idAoo, cades, !firmaAutografa && daFirmare, nomeFile, mimeType, new ByteArrayInputStream(allegatoToSend.getValue()), 
					StringUtils.cleanGuidToString(allegatoToSend.getKey()), oggetto);
			
			LOGGER.info(signTransactionId + ": inviato allegato da firmare (" + documentTitle + ") ad NPS " + idAllegatoNPS);

			idDocumentiNPS.add(idAllegatoNPS);
			guidDocumentiSentToNPS.add(allegatoToSend.getKey());
		}
	}

	/**
	 * Esegue l'upload sincrono degli allegati al documento principale il cui guid
	 * non è presenti in guidDocumentiSentToNPS
	 * 
	 * @param guidDocPrincipale
	 * @param documentTitleDocPrincipale
	 * @param cades
	 * @param fceh
	 * @param idDocumentiNPS
	 * @param guidDocumentiSentToNPS
	 * @throws IOException
	 */
	private void uploadNotSentDocSyncNPS(final String signTransactionId, final Integer idAoo, final String guidDocPrincipale, final String documentTitleDocPrincipale, final boolean cades,
			final IFilenetCEHelper fceh, final List<String> idDocumentiNPS, final List<String> guidAttachSentToNPS) throws IOException {

		final DocumentSet allegati = fceh.getAllegatiConContent(StringUtils.cleanGuidToString(guidDocPrincipale));
		if (allegati != null && !allegati.isEmpty()) {
			final Iterator<?> it = allegati.iterator();
			// se non è da firmare, invia ad nps
			while (it.hasNext()) {
				final Document allegato = (Document) it.next();
				if (!guidAttachSentToNPS.contains(allegato.get_Id().toString())) {
					final String idAllegatoNPS = uploadSyncNPS(idAoo, cades, false,
							allegato.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY)), allegato.get_MimeType(),
							allegato.accessContentStream(0), StringUtils.cleanGuidToString(allegato.get_Id()),
							allegato.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY)));
					
					LOGGER.info(signTransactionId + ": [Flusso] inviato allegato da non firmare ("
							+ allegato.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)) + ") ad NPS " + idAllegatoNPS);

					idDocumentiNPS.add(idAllegatoNPS);
					guidAttachSentToNPS.add(allegato.get_Id().toString());
				}

			}
		}

	}
	
	private String uploadSyncNPS(final int idAoo, final boolean cades, final boolean firmato, final String fileName, final String mimeType, final InputStream content, final String guid,
			final String oggetto) throws IOException {
		// esegui upload sincrono content verso NPS
		String fileNameNPS = fileName;
		String mimeTypeNPS = mimeType;
		if (cades) {
			fileNameNPS = fileName + ".p7m";
			mimeTypeNPS = ContentType.P7M;
		}
		final String descrizione = "upload_" + oggetto;

		return npsSRV.uploadDocToNps(idAoo, firmato, fileNameNPS, mimeTypeNPS, descrizione, content, new Date(), guid);
	}
	
	private void associaRegAuxProtUscitaNPS(final RegistrazioneAusiliariaDTO registrazioneAusiliaria, final String idProtocollo, final UtenteDTO utente,
			final String numeroAnnoProtocollo, final String idDocumento, final Connection connection) {

		npsSRV.collegaRegAuxProtUscita(utente, connection, idProtocollo, registrazioneAusiliaria.getIdRegistrazione(), numeroAnnoProtocollo, idDocumento);
	}
	

	private Document rigeneraContentRegistrazioneAusiliaria(final String signTransactionId, final VWWorkObject wob, final UtenteDTO utente, final IFilenetCEHelper fceh, final Document documentForDetails, final DetailDocumentoDTO documentDetailDTO, final Connection connection) {
		Document documentForDetailsReturn = null;
		String documentTitle = TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY).toString();

		try {
			
			// Recupero informazioni registrazione ausiliaria
			final RegistrazioneAusiliariaDTO registrazioneAusiliaria = registrazioniAusiliarieSRV.getRegistrazioneAusiliaria(documentTitle, utente.getIdAoo().intValue(), connection);
			
			LOGGER.info(signTransactionId + " recuperate dalla base dati le informazioni relative alla registrazione ausiliaria");

			if (registrazioneAusiliaria != null && !registrazioneAusiliaria.isDocumentoDefinitivo()) {
				RispostaAllaccioDTO allaccioPrincipale = allaccioDAO.getAllaccioPrincipale(Integer.parseInt(documentTitle), connection);
				
				LOGGER.info(signTransactionId + " recuperato dalla base dati l'allaccio principale");

				// Aggiornamento metadati documento
				Collection<MetadatoDTO> metadatiEstesi = documentDetailDTO.getMetadatiEstesi();
				if (metadatiEstesi != null && !metadatiEstesi.isEmpty()) {
					for (MetadatoDTO m : metadatiEstesi) {
						if (pp.getParameterByKey(PropertiesNameEnum.DATA_REGISTRAZIONE_AUX_METALKEY)
								.equals(m.getName())) {
							m.setSelectedValue(registrazioneAusiliaria.getDataRegistrazione());
						}

						if (pp.getParameterByKey(PropertiesNameEnum.NUMERO_REGISTRAZIONE_AUX_METALKEY)
								.equals(m.getName())) {
							m.setSelectedValue(registrazioneAusiliaria.getNumeroRegistrazione());
						}

						if (pp.getParameterByKey(PropertiesNameEnum.NOME_REGISTRAZIONE_AUX_METALKEY)
								.equals(m.getName())) {
							m.setSelectedValue(registrazioneAusiliaria.getRegistroAusiliario().getNome());
						}
					}
				}
				
				LOGGER.info(signTransactionId + " aggiornati in memoria i metadati estesi del documento");
				
				// Aggiornamento DetailDocumentoDTO con la registrazione ausiliaria aggiornata
				documentDetailDTO.setRegistrazioneAusiliaria(registrazioneAusiliaria);
				
				// Registrazione timer integrazione dati
				if (TipoAllaccioEnum.RICHIESTA_INTEGRAZIONE.equals(allaccioPrincipale.getTipoAllaccioEnum())) {
					timerSRV.registraDataInvio(documentTitle, utente.getIdAoo(), fceh, connection);
					LOGGER.info(signTransactionId + " registrati i dati di invio della richiesta integrazioni sulla base dati");
				}
				
				// Salvataggio metadati estesi documento
				salvaDocumentoSRV.salvaMetadatiEstesi(Integer.parseInt(documentTitle), metadatiEstesi, utente.getIdAoo(), documentDetailDTO.getIdTipologiaDocumento(),
						documentDetailDTO.getIdTipoProcedimento(), null, false, connection, fceh);
				
				LOGGER.info(signTransactionId + ": " + "metadati estesi aggiornati su Filenet");
				
				// Rigenerazione content
				Collection<MetadatoDTO> metadatiRegistroAusiliario = registrazioneAusiliaria.getMetadatiRegistroAusiliario();
				for (MetadatoDTO m : metadatiRegistroAusiliario) {
					if (pp.getParameterByKey(PropertiesNameEnum.DATA_REGISTRAZIONE_AUX_TEMPLATE).equals(m.getName())) {
						m.setSelectedValue(registrazioneAusiliaria.getDataRegistrazione());
					}

					if (pp.getParameterByKey(PropertiesNameEnum.NUMERO_REGISTRAZIONE_AUX_TEMPLATE)
							.equals(m.getName())) {
						m.setSelectedValue(registrazioneAusiliaria.getNumeroRegistrazione());
					}

					if (pp.getParameterByKey(PropertiesNameEnum.DATA_FIRMA_TEMPLATE).equals(m.getName())) {
						m.setSelectedValue(new Date());
					}
				}
				
				LOGGER.info(signTransactionId + " aggiornati in memoria i metadati estesi del template");
				
				CollectedParametersDTO result = registroAusiliarioSRV.collectParameters(utente, registrazioneAusiliaria.getRegistroAusiliario().getId(), metadatiRegistroAusiliario, false);
				LOGGER.info(signTransactionId + " recuperato il template dalla base dati");
				byte[] contentNew = registroAusiliarioSRV.createPdf(registrazioneAusiliaria.getRegistroAusiliario().getId(), result.getStringParams());
				LOGGER.info(signTransactionId + " rigenerato il doucmento principale");
				
				// Salvataggio del content sul CE
				fceh.addVersion(documentForDetails, new ByteArrayInputStream(contentNew), null, null, documentDetailDTO.getNomeFile(), documentDetailDTO.getMimeType(), utente.getIdAoo());
				LOGGER.info(signTransactionId + " aggiornata la versione del documento principale su Filenet con la versione firmata");
				
				// Trasformazione sincrona per stampigliatura ID documento
				updateContentSRV.addPDFVersion(utente, wob, connection);
				LOGGER.info(signTransactionId + " aggiornata la versione del documento principale su Filenet con la versione stampigliata");
				
				// Refresh documentDetailDTO con nuovo content
				documentForDetailsReturn = fceh.getDocumentRedForDetail(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle);
				LOGGER.info(signTransactionId + " recuperata ultima versione del documento da Filenet");
				setContentOnDetail(signTransactionId, documentDetailDTO, documentTitle, fceh);
				
				LOGGER.info(signTransactionId + ": " + "rigenerato content dopo registrazione e salvato su FileNet con GUID: " + documentForDetailsReturn.get_Id().toString());
				
				// Salvataggio dei dati su database (xml metadati registrazione ausiliaria e informazioni registrazione)
				registrazioniAusiliarieSRV.updateDatiRegistrazioneAusiliaria(documentTitle, utente.getIdAoo().intValue(), registrazioneAusiliaria.getMetadatiRegistroAusiliario(), true, connection);
				LOGGER.info(signTransactionId + ": " + "dati registrazione ausiliaria aggiornati su db ");
			}
		} catch (Exception e) {
			LOGGER.error(ERRORE_RISCONTRATO_DURANTE_LA_RIGENERAZIONE_DEL_CONTENT_DELLA_REGISTRAZIONE_AUSILIARIA_ASSOCIATA_AL_DOCUMENTO + documentTitle, e);
			throw new RedException(ERRORE_RISCONTRATO_DURANTE_LA_RIGENERAZIONE_DEL_CONTENT_DELLA_REGISTRAZIONE_AUSILIARIA_ASSOCIATA_AL_DOCUMENTO + documentTitle, e);
		} 

		return documentForDetailsReturn;
	}

	private void protocollaRegistrazioneAusiliaria(String signTransactionId, VWWorkObject wob, UtenteDTO utente, IFilenetCEHelper fceh, DetailDocumentoDTO documentDetailDTO, Connection connection) {
		
		String documentTitle = TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY).toString();
		try {
			
			// Recupero dettagli registrazione ausiliaria
			boolean firmaMultipla = Integer.valueOf(TipoAssegnazioneEnum.FIRMA_MULTIPLA.getId()).equals(TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY));

			if (!firmaMultipla && documentDetailDTO.getSottoCategoriaUscita() != null && documentDetailDTO.getSottoCategoriaUscita().isRegistrazioneAusiliaria()) {
				RegistrazioneAusiliariaDTO registrazioneAusiliaria = registrazioniAusiliarieSRV.getRegistrazioneAusiliaria(documentTitle, utente.getIdAoo().intValue(), connection);
				
				LOGGER.info(signTransactionId + " recuperate dalla base dati info registrazione ausiliaria");
				
				if(registrazioneAusiliaria.getIdRegistrazione() == null) {
					RispostaAllaccioDTO allaccioPrincipale = allaccioDAO.getAllaccioPrincipale(Integer.parseInt(documentTitle), connection);
					LOGGER.info(signTransactionId + ": " + " recuperato allaccio principale dalla base dati " + (allaccioPrincipale != null ? allaccioPrincipale.getTipoAllaccioEnum().toString() : ""));

					if(allaccioPrincipale == null) {
						throw new RedException("Allaccio principale del documento: " + documentTitle + " non recuperato correttamente.");
					}
					
					// Recupero id protocollo dell'ingresso
					List<String> props = Arrays.asList(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY));
					Document document = fceh.getDocumentByIdGestionale(allaccioPrincipale.getIdDocumentoAllacciato(), props, null, utente.getIdAoo().intValue(), null, null);
					LOGGER.info(signTransactionId + " recuperato allaccio principale da Filenet");
					
					String idProtocolloIngresso = TrasformerCE.getMetadato(document, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY) != null ? TrasformerCE.getMetadato(document, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY).toString() : "";
					
					// Recupero ACL
					String acl = getACL4NPS(wob, documentDetailDTO, utente, fceh, documentDetailDTO.getFlagRiservato());
					LOGGER.info(signTransactionId + " costruita catena di acl da inviare ad NPS " + acl);
					// ### Stacca la registrazione ausiliaria ###
					RegistrazioneAusiliariaNPSDTO registrazioneAusiliariaNPS = npsSRV.creaRegistrazioneAusiliaria(documentDetailDTO, acl, utente, registrazioneAusiliaria.getRegistroAusiliario(),
							allaccioPrincipale.getTipoAllaccioEnum().getTipoRegistro(), idProtocolloIngresso, connection);
					
					LOGGER.info(signTransactionId + ": registrazioneAusiliariaNPS " + registrazioneAusiliariaNPS);
					
					// Persistenza delle informazioni ottenute da NPS
					registrazioniAusiliarieSRV.updateParzialeRegistrazioneAusiliaria(connection, documentTitle, utente.getIdAoo(), registrazioneAusiliariaNPS.getNumeroRegistrazione(), registrazioneAusiliariaNPS.getId(), registrazioneAusiliariaNPS.getDataRegistrazione());
					LOGGER.info(signTransactionId + " aggiornate sulla base dati info registrazione ausiliaria");
				}
			} 

		} catch (Exception e) {
			LOGGER.error(signTransactionId + " Errore riscontrato durante la protocollazione della registrazione ausiliaria associata al documento: " + documentTitle, e);
			throw new RedException(ERRORE_RISCONTRATO_DURANTE_LA_RIGENERAZIONE_DEL_CONTENT_DELLA_REGISTRAZIONE_AUSILIARIA_ASSOCIATA_AL_DOCUMENTO + documentTitle, e);
		}
	}

	/**
	 * Imposta il content del documento identificato dal documentTitle sul
	 * dettaglio: documentDetailDTO. Non si occupa della chiusura del
	 * FilenetCEHelper.
	 * 
	 * @param documentDetailDTO
	 * @param documentTitle
	 * @param fceh
	 */
	private void setContentOnDetail(final String signTransactionId, final DetailDocumentoDTO documentDetailDTO, final String documentTitle, final IFilenetCEHelper fceh) {
		try {
			final Document contentDocument = fceh.getDocumentForDownload(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle);
			LOGGER.info(signTransactionId + " recuperato il documento su Filenet");
			documentDetailDTO.setContent(FilenetCEHelper.getDocumentContentAsByte(contentDocument));
		} catch (final Exception e) {
			LOGGER.error(signTransactionId + " " + ERROR_RECUPERO_CONTENT_MSG + documentTitle, e);
			throw new RedException("Errore durante il recupero del content del documento principale.", e);
		}
	}

	/**
	 * @param utente
	 * @param connection
	 * @param documentDetailDTO
	 * @return
	 */
	@Override
	public void checkIsRegistroRepertorio(final UtenteDTO utente, final DetailDocumentoDTO documentDetailDTO, final Connection connection) {
		final Map<Long, List<RegistroRepertorioDTO>> registriRepertorioConfigured = registroRepertorioSRV.getRegistriRepertorioConfigurati(utente.getIdAoo(), connection);
		final Boolean isRegistroRepertorio = registroRepertorioSRV.checkIsRegistroRepertorio(utente.getIdAoo(), documentDetailDTO.getIdTipologiaDocumento(),
				documentDetailDTO.getIdTipoProcedimento(), registriRepertorioConfigured, connection);

		if (Boolean.TRUE.equals(isRegistroRepertorio)) {
			final List<RegistroRepertorioDTO> registriRepertorioList = registriRepertorioConfigured.get(documentDetailDTO.getIdTipoProcedimento());

			if (registriRepertorioList != null && !registriRepertorioList.isEmpty()) {
				// Si cicla la lista di Registri Repertorio configurati perchè c'è la
				// possibilità
				// che un tipoProcedimento possa essere configurato per più tipiDocumento.
				for (final RegistroRepertorioDTO rr : registriRepertorioList) {
					if (rr.getIdTipologiaDocumento().equals(documentDetailDTO.getIdTipologiaDocumento())) {  
						
						// Una volta verificata che la coppia TipologiaDocumento/TipoProcedimento selezionata 
						// sia presente all'interno della configurazione su DB viene recuperata la descrizione.
						documentDetailDTO.setDescRegistroRepertorio(rr.getDescrizioneRegistroRepertorio());
						documentDetailDTO.setLabelStampigliaturaRR(rr.getLabelStampigliatura());
						break;
					}
				}
			}
		}

		documentDetailDTO.setIsRegistroRepertorio(isRegistroRepertorio);
	}

	
	/**
	 * @param documentForDetails
	 * @param fceh
	 * @param wob
	 */
	private void aggiornaVersioneCopiaConforme(final Document documentForDetails, final IFilenetCEHelper fceh, final VWWorkObject wob) {
		final Boolean flagCopiaConforme = (Boolean) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.FIRMA_COPIA_CONFORME_METAKEY));
		if (flagCopiaConforme != null && flagCopiaConforme) {
			// Se copia conforme, aggiorna metadato versione copia conforme
			final Integer versionNumber = documentForDetails.get_MajorVersionNumber();
			final Map<String, Object> metadati = new HashMap<>();
			metadati.put(PropertiesNameEnum.VERSIONE_COPIA_CONFORME_METAKEY.getKey(), Constants.EMPTY_STRING + versionNumber);
			fceh.updateMetadati(documentForDetails, metadati);
		}
	}

	/**
	 * @param documentDetailDTO
	 * @param protocollo
	 * @param idAoo
	 * @param fceh
	 */
	private void aggiornaFascicoloBilancioEnti(final DetailDocumentoDTO documentDetailDTO, final ProtocolloDTO protocollo, final Integer idAoo, final IFilenetCEHelper fceh) {
		if (pp.getParameterByKey(PropertiesNameEnum.TIPO_DOC_BILANCIO_ENTI_USCITA).equals(Constants.EMPTY_STRING + documentDetailDTO.getIdTipologiaDocumento())) {
			String idFascicoloFepa = null;

			final List<FascicoloDTO> fascicoli = fceh.getFascicoliDocumento(documentDetailDTO.getDocumentTitle(), idAoo);
			if (fascicoli != null && !fascicoli.isEmpty()) {
				final FascicoloDTO fascicolo = fascicoli.get(0);
				idFascicoloFepa = fascicolo.getIdFascicoloFEPA();
				if (idFascicoloFepa != null) {
					fepaSRV.addDocumentoToFascicoloBilancioEnti(idFascicoloFepa, documentDetailDTO, protocollo, Constants.Protocollo.TIPOLOGIA_PROTOCOLLO_USCITA, true, idAoo);
				}
			}
		}
	}

	private List<String> getApprovazioni(final Integer idDoc, final Long idAoo, final Connection con) {
		final List<String> siglaVistoList = new ArrayList<>();
		final Collection<ApprovazioneDTO> approvazioni = approvazioneDAO.getApprovazioniByIdDocumento(idDoc, idAoo.intValue(), con);
		ApprovazioneDTO approvazione = null;
		final Iterator<ApprovazioneDTO> it = approvazioni.iterator();
		while (it.hasNext()) {

			approvazione = it.next();
			final Integer stampigliatura = approvazione.getStampigliaturaFirma();

			if (stampigliatura > 0) {

				// prepara alias utente
				if (stampigliatura == 1) {
					siglaVistoList.add(approvazione.getDescUtente());
				} else if (stampigliatura == 2) {
					siglaVistoList.add(approvazione.getNome().substring(0, 1) + "." + approvazione.getCognome());
				} else if (stampigliatura == 3) {
					siglaVistoList.add(approvazione.getNomeRuolo());
				}
			}
		}

		return siglaVistoList;
	}
	
	
	@Override
	public final void updateDocumentsVersionFromSignedDocsBuffer(final String signTransactionId, final String wobNumber, final UtenteDTO utente, final SignTypeEnum signType, final IFilenetCEHelper fceh, 
			final Connection connection) {
		Collection<String> documentsToRollback = new HashSet<>();

		try {
			// Si recuperano i content firmati dalla tabella di bufferizzazione
			Collection<DocumentoDTO> bufferedSignedDocs = aSignDAO.getFromSignedDocsBuffer(connection, wobNumber);
			LOGGER.info(signTransactionId + " recuperati content sulla tabella di bufferizzazione per il wobNumber " + wobNumber);
			
			if (!CollectionUtils.isEmpty(bufferedSignedDocs)) {
				Document signedDocFilenet;
				
				for (DocumentoDTO signedDoc : bufferedSignedDocs) {
					// ###### Upload della versione firmata del documento sul CE
					signedDocFilenet = uploadSignedDocument(signedDoc.getGuid(), signedDoc.getContent(), signedDoc.isFlagAllegato(), utente, signType, fceh);
					LOGGER.info(signTransactionId + " aggiunta versione su Filenet per " + ((signedDoc.isFlagAllegato()) ? "l'allegato" : "il documento principale") + " " + signedDocFilenet.get_Id().toString());
					documentsToRollback.add(signedDocFilenet.get_Id().toString());
				}
			}
		} catch (Exception e) {
			LOGGER.error(signTransactionId + " Errore in fase di caricamento sul CE delle versioni firmate dei documenti afferenti al workflow: " + wobNumber, e);
			// Si rimuovono da FileNet le eventuali versioni firmate già inserite
			if (!CollectionUtils.isEmpty(documentsToRollback)) {
				for (String guidDocToRollback : documentsToRollback) {
					fceh.deleteVersion(fceh.idFromGuid(guidDocToRollback));
					LOGGER.info(signTransactionId + " rimossa da Filenet la versione " + guidDocToRollback);
				}
			}
			// Si rilancia l'eccezione per la gestione nel chiamante
			throw new RedException(e);
		} finally {
			// Si svuota il buffer in ogni caso
			aSignDAO.removeFromSignedDocsBufferByWobNumber(connection, wobNumber);
			LOGGER.info(signTransactionId + " rimossi content sulla tabella di bufferizzazione per il wobNumber " + wobNumber);
		}
	}
	
	
	/**
	 * @param guid
	 * @param content
	 * @param allegato
	 * @param utente
	 * @param ste
	 * @param fceh
	 * @return
	 */
	private Document uploadSignedDocument(final String guid, final byte[] content, final boolean allegato, final UtenteDTO utente, final SignTypeEnum signType, 
			final IFilenetCEHelper fceh) {
		Document documentForDetails = fceh.getDocumentByGuid(guid);
		
		Map<String, Object> metadatiDocFirmato = getMetadatiToVersionUpdate(documentForDetails, allegato, signType);
		String mimeType = MediaType.PDF.toString();
		if (SignTypeEnum.CADES.equals(signType)) {
			mimeType = ContentType.P7M;
		}
		String nomeFile = (String) metadatiDocFirmato.get(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));

		// ### Aggiorno la versione del documento principale su CE a seguito della firma #####################
		return fceh.addVersion(documentForDetails, new ByteArrayInputStream(content), null, metadatiDocFirmato, nomeFile, mimeType, utente.getIdAoo());
	}

	
	/**
	 * Effettua l'aggiornamento della versione del documento principale firmati, con
	 * annesso upload del documento su protocollo NPS (in caso di protocollazione
	 * con NPS).
	 * 
	 * @param ste
	 * @param documentDetailDTO
	 * @param utente
	 * @param documentTitle
	 * @param protocollo
	 * @param protocolloToBook
	 * @param documentForDetails
	 * @param mimeTypeDocPrincipale
	 * @param connection
	 * @param fceh
	 * @return
	 */
	private Document updateSignedDocument(final SignTypeEnum ste, final DetailDocumentoDTO documentDetailDTO, final UtenteDTO utente, final String documentTitle, 
			final ProtocolloDTO protocollo, final boolean protocolloToBook, final Document inDocumentForDetails, final Connection connection, final IFilenetCEHelper fceh, 
			final boolean sendToNPS, final boolean isProtocollazioneAnticipata) {
		// ### Aggiorno la versione del documento principale su CE a seguito della firma #####################
		String nomeFile = documentDetailDTO.getNomeFile();
		final Map<String, Object> metadatiDocPrincipaleFirmato = getMetadatiToVersionUpdate(utente, nomeFile, documentTitle, ste, protocollo, protocolloToBook);
		String mimeType = MediaType.PDF.toString();
		if (ste.equals(SignTypeEnum.CADES)) {
			mimeType = ContentType.P7M;
		}
		Document documentForDetails = inDocumentForDetails;
		nomeFile = (String) metadatiDocPrincipaleFirmato.get(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));
		InputStream isDoc = new ByteArrayInputStream(documentDetailDTO.getContent());
		documentForDetails = fceh.addVersion(documentForDetails, isDoc, null, metadatiDocPrincipaleFirmato, nomeFile, mimeType,utente.getIdAoo());
		documentDetailDTO.setGuid(StringUtils.cleanGuidToString(documentForDetails.get_Id()));
		
		String guidFirstVersionDocPrincipale = null;
		DocTipoOperazioneType tipoOperazioneEnum = null;
		if(isProtocollazioneAnticipata) {
			guidFirstVersionDocPrincipale = fceh.getFirstVersionById(documentDetailDTO.getDocumentTitle(), utente.getIdAoo().intValue()).get_Id().toString(); 
			tipoOperazioneEnum = DocTipoOperazioneType.COLLEGA_DOCUMENTO;
		}
		
		// ### UPLOAD/ASSOCIAZIONE PROTOCOLLO TO NPS DOCUMENTO PRINCIPALE e ALLEGATI (in più aggiorno la versione)
		if (TipologiaProtocollazioneEnum.isProtocolloNPS(utente.getIdTipoProtocollo()) && sendToNPS) {
			uploadAssociazioneProtocolloToAsyncNps(protocollo.getIdProtocollo(), new ByteArrayInputStream(documentDetailDTO.getContent()), documentDetailDTO.getGuid(),
					mimeType, nomeFile, documentDetailDTO.getOggetto(), utente, true, protocollo.getNumeroAnnoProtocolloNPS(utente.getCodiceAoo()), documentTitle, connection,
					guidFirstVersionDocPrincipale, tipoOperazioneEnum);
		}

		return documentForDetails;
	}

	/**
	 * Effettua l'aggiornamento della versione dell'allegato firmati, con annesso
	 * upload del documento su protocollo NPS (in caso di protocollazione con NPS).
	 * 
	 * @param allegatoFirmato
	 * @param content
	 * @param ste
	 * @param utente
	 * @param hasDestinatariEsterni
	 * @param protocollo
	 * @param fceh
	 * @param connection
	 * @return
	 */
	private Document updateSignedAttachment(final Document allegatoFirmato, final byte[] content, final SignTypeEnum ste, 
			final UtenteDTO utente, final ProtocolloDTO protocollo, final IFilenetCEHelper fceh, final Connection connection, 
			final boolean sendToNPS, final boolean isProtocollazioneAnticipata) {
		
		String nomeFileAllegato = (String) allegatoFirmato.getProperties().get(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY)).getObjectValue();
		String mimeTypeAllegato = MediaType.PDF.toString();
		if (ste.equals(SignTypeEnum.CADES)) {
			mimeTypeAllegato = ContentType.P7M_MIME;
			final String idDocumentoAllegato = TrasformerCE.getMetadato(allegatoFirmato, pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY)) != null
					? TrasformerCE.getMetadato(allegatoFirmato, pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY)).toString()
					: "";
			if (nomeFileAllegato != null) {
				nomeFileAllegato = nomeFileAllegato + ".p7m";
			} else if (idDocumentoAllegato != null) {
				nomeFileAllegato = idDocumentoAllegato + ".p7m";
			} else {
				nomeFileAllegato = StringUtils.cleanGuidToString(allegatoFirmato.get_Id()) + ".p7m";
			}
		}

		final Map<String, Object> metadatiAllegatoFirmato = new HashMap<>();
		metadatiAllegatoFirmato.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), nomeFileAllegato);

		final Document allegatoNew = fceh.addVersion(allegatoFirmato, new ByteArrayInputStream(content), metadatiAllegatoFirmato, nomeFileAllegato, mimeTypeAllegato,
				utente.getIdAoo());

		final String oggettoDocAllegato = allegatoFirmato.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY));
		final String idDocumento = allegatoFirmato.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
		String guidFirstVersionAllegato = null;
		DocTipoOperazioneType tipoOperazioneEnum = null;
		if(isProtocollazioneAnticipata) {
			guidFirstVersionAllegato = fceh.getFirstVersionByGuid(allegatoFirmato.get_Id().toString()).get_Id().toString();
			tipoOperazioneEnum = DocTipoOperazioneType.COLLEGA_DOCUMENTO;
		}
		if (TipologiaProtocollazioneEnum.isProtocolloNPS(utente.getIdTipoProtocollo()) && sendToNPS) {
			uploadAssociazioneProtocolloToAsyncNps(protocollo.getIdProtocollo(), new ByteArrayInputStream(content), StringUtils.cleanGuidToString(allegatoNew.get_Id()),
					mimeTypeAllegato, nomeFileAllegato, oggettoDocAllegato, utente, false, protocollo.getNumeroAnnoProtocolloNPS(utente.getCodiceAoo()), idDocumento,
					connection, guidFirstVersionAllegato, tipoOperazioneEnum);
		}

		return allegatoNew;
	}
	
	/**
	 * @param documentoPrincipale
	 * @param utente
	 * @param protocollo
	 * @param onlyUnsigned
	 * @param fceh
	 * @param connection
	 */
	private void sendAttachmentToNPS(final Document documentoPrincipale, final UtenteDTO utente, final ProtocolloDTO protocollo, final boolean onlyUnsigned, 
			final IFilenetCEHelper fceh, final Connection connection, final boolean isProtocollazioneAnticipata) {
		if (TipologiaProtocollazioneEnum.isProtocolloNPS(utente.getIdTipoProtocollo())) {
			// Recupera allegati del documento principale
			final DocumentSet allegati = fceh.getAllegatiConContent(StringUtils.cleanGuidToString(documentoPrincipale.get_Id()));
			if (allegati != null && !allegati.isEmpty()) {
				final Iterator<?> it = allegati.iterator();
				// Se non è da firmare, invia ad NPS
				while (it.hasNext()) {
					final Document allegato = (Document) it.next();
					
					if (!onlyUnsigned || (!FilenetCEHelper.isDaFirmare(allegato) && !FilenetCEHelper.hasStampigliaturaSigla(allegato))) {

						final byte[] content = FilenetCEHelper.getDocumentContentAsByte(allegato);
						final String nomeFileAllegato = (String) allegato.getProperties().get(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY)).getObjectValue();
						final String oggettoDocAllegato = allegato.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY));
						final String idDocumentoAllegato = allegato.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));

						String guidFirstVersionAllegato = null;
						DocTipoOperazioneType tipoOperazioneEnum = null;
						if(isProtocollazioneAnticipata) {
							guidFirstVersionAllegato = fceh.getFirstVersionByGuid(allegato.get_Id().toString()).get_Id().toString();
							tipoOperazioneEnum = DocTipoOperazioneType.COLLEGA_DOCUMENTO;
						}
						
						uploadAssociazioneProtocolloToAsyncNps(protocollo.getIdProtocollo(), new ByteArrayInputStream(content),
								StringUtils.cleanGuidToString(allegato.get_Id()), allegato.get_MimeType(), nomeFileAllegato, oggettoDocAllegato, utente, false,
								protocollo.getNumeroAnnoProtocolloNPS(utente.getCodiceAoo()), idDocumentoAllegato, connection, guidFirstVersionAllegato, tipoOperazioneEnum);
						
					}
				}
			}
		}
	}

	/**
	 * Restituisce una Map dei metadati del protocollo da aggiornare a seguito della
	 * protocollazione.
	 * 
	 * @param protocollo - Dati del protocollo.
	 * @param utente     - Utente protocollatore
	 * @return metadati del protocollo.
	 */
	@Override
	public Map<String, Object> getMetadatiProtocolloToUpdate(final ProtocolloDTO protocollo, final UtenteDTO utente, final boolean aggiornaDataProtocollo) {

		final Long idTipoProtocollo = utente.getIdTipoProtocollo();

		final Map<String, Object> output = new HashMap<>();
		final String idProtocolloNps = protocollo.getIdProtocollo();
		int annoProtocollo = -1;
		if (protocollo.getAnnoProtocolloInt() != null) {
			annoProtocollo = protocollo.getAnnoProtocolloInt().intValue();
		}
		final int numeroProtocollo = protocollo.getNumeroProtocollo();
		final Date dataProtocollo = protocollo.getDataProtocollo();
		final String descrizioneTipologiaDocumento = protocollo.getDescrizioneTipologiaDocumentoNps();
		final String oggettoProtocolloNps = protocollo.getOggettoProtocolloNps();
		final String registroProtocollo = protocollo.getRegistroProtocollo();

		if (idProtocolloNps != null && idProtocolloNps.trim().length() > 0) {
			output.put(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY), idProtocolloNps);
		}
		if (annoProtocollo > 0) {
			output.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY), annoProtocollo);
			if (TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(idTipoProtocollo)) {
				output.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_EMERGENZA_METAKEY), annoProtocollo);
			}
		}
		if (numeroProtocollo > 0) {
			output.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY), numeroProtocollo);
			if (TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(idTipoProtocollo)) {
				output.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_EMERGENZA_METAKEY), numeroProtocollo);
			}
		}
		if (dataProtocollo != null && aggiornaDataProtocollo) {
			output.put(pp.getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY), new Date());
			if (TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(idTipoProtocollo)) {
				output.put(pp.getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_EMERGENZA_METAKEY), dataProtocollo);
			}
		}
		if (descrizioneTipologiaDocumento != null) {
			output.put(pp.getParameterByKey(PropertiesNameEnum.DESCRIZIONE_TIPOLOGIA_DOCUMENTO_NPS_METAKEY), descrizioneTipologiaDocumento);
		}
		if (oggettoProtocolloNps != null) {
			// Si applica la substring() dato che il metadato sul CE è limitato a max 64
			// caratteri
			output.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_PROTOCOLLO_NPS_METAKEY), org.apache.commons.lang3.StringUtils.substring(oggettoProtocolloNps, 0, 64));
		}

		if (registroProtocollo != null) {
			output.put(pp.getParameterByKey(PropertiesNameEnum.REGISTRO_PROTOCOLLO_FN_METAKEY), registroProtocollo);
		}

		output.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_PROTOCOLLATORE_METAKEY), utente.getId().intValue());
		output.put(pp.getParameterByKey(PropertiesNameEnum.ID_GRUPPO_PROTOCOLLATORE_METAKEY), utente.getIdUfficio().intValue());
		output.put(pp.getParameterByKey(PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY), Constants.Protocollo.TIPO_PROTOCOLLO_USCITA);
		
		return output;
	}
	
	
	/**
	 * Recupera i metadati del documento, per effettuare l'aggiornamento della versione firmata.
	 * 
	 * @param documentForDetails - Documento FileNet da aggiornare con la versione firmata.
	 * @param allegato - Indica se il documento è il principale o un allegato.
	 * @param signType - Tipo di firma.
	 * @return Map metadato/value dei metadati del documento, per effettuare l'aggiornamento della versione firmata.
	 */
	private Map<String, Object> getMetadatiToVersionUpdate(final Document documentForDetails, final boolean allegato, final SignTypeEnum signType) {
		Map<String, Object> output = new HashMap<>();
		
		String documentTitle = (String) TrasformerCE.getMetadato(documentForDetails, pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
		Integer numeroDocumento = (Integer) TrasformerCE.getMetadato(documentForDetails, pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY));
		String nomeFile = (String) TrasformerCE.getMetadato(documentForDetails, pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));
		
		if (SignTypeEnum.CADES.equals(signType)) {
			String nomeFileProp = null;
			if (!allegato) { // Principale
				nomeFileProp = documentTitle + ".p7m";
				if (!StringUtils.isNullOrEmpty(nomeFile)) {
					nomeFileProp = nomeFile + ".p7m";
				}
			} else { // Allegato
				if (!StringUtils.isNullOrEmpty(nomeFile)) {
					nomeFileProp = nomeFile + ".p7m";
				} else if (numeroDocumento != null) {
					nomeFileProp = numeroDocumento + ".p7m";
				} else {
					nomeFileProp = StringUtils.cleanGuidToString(documentForDetails.get_Id()) + ".p7m";
				}
			}

			output.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), nomeFileProp);
		} else {
			output.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), nomeFile);
		}
		
		return output;
	}
	

	/**
	 * Recupera i metadati del documento principale, per effettuare l'aggiornamento
	 * della versione firmata.
	 * 
	 * @param utente           Utente protocollatore.
	 * @param nomeFile         - Nome del file.
	 * @param documentTitle    - DocumentTitle del documento principale firmato.
	 * @param ste              - Tipo di firma digitale.
	 * @param protocollo       - Protocollo del documento principale(se presente).
	 * @param protocolloToBook - Condizione se è stato recuperato un protocollo per
	 *                         il documento principale firmato.
	 * @return Map metadato/value dei metadati del documento principale, per
	 *         effettuare l'aggiornamento della versione firmata.
	 */
	private Map<String, Object> getMetadatiToVersionUpdate(final UtenteDTO utente, final String nomeFile, final String documentTitle, final SignTypeEnum ste,
			final ProtocolloDTO protocollo, final boolean protocolloToBook) {
		Map<String, Object> output = new HashMap<>();
		if (protocolloToBook) {
			output = getMetadatiProtocolloToUpdate(protocollo, utente, false);
		}

		output.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), nomeFile);
		if (ste.equals(SignTypeEnum.CADES)) {
			String nomeFileProp = documentTitle + ".p7m";
			if (nomeFile != null) {
				nomeFileProp = nomeFile + ".p7m";
			}
			output.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), nomeFileProp);
		}
		return output;
	}

	/**
	 * Restituisce se fra la lista dei destinatari ce ne è almeno uno di tipo
	 * Elettronico.
	 * 
	 * @param destinatari - Lista di DTO dei destinatari.
	 * @return Esito del controllo sui destinatari.
	 */
	public static final boolean isOneElettronico(final List<DestinatarioDTO> destinatari) {
		for (final DestinatarioDTO dest : destinatari) {
			if (Constants.TipoDestinatario.ESTERNO.equalsIgnoreCase(dest.getTipo()) && Constants.TipoSpedizione.MEZZO_ELETTRONICO.equals(dest.getTipoSpedizione())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gestisce l'invio della mail a seguito della firma di un documento principale.
	 * 
	 * @param utente
	 *            - Utente che sta eseguente l'operazione di firma.
	 * @param wobDTO
	 *            - DTO relativo al WF su cui si sta eseguendo la firma.
	 * @param documentDetail
	 *            - DTO relativo al documento su cui sta eseguendo la firma.
	 * @param protocollo
	 *            - Protocollo relativo al documento principale su cui sta eseguendo
	 *            la firma.
	 * @param destinatariCollection
	 *            - Destinatari del documento principale.
	 * @param fpeh
	 *            - Helper per la connessione al PE.
	 * @param connection
	 *            - Connessione al DB.
	 * 
	 * @return Esito della procedura di invio della mail.
	 */
	@Override
	public EsitoOperazioneDTO invioMail(final UtenteDTO utente, final PEDocumentoDTO wobDTO,
			final DetailDocumentoDTO documentDetail, final ProtocolloDTO protocollo,
			final Collection<?> destinatariCollection, final Connection connection, final boolean firmaDigitale,
			final boolean simulaInvioNPS, final boolean chiudiFascicolo) {
		String documentTitle = documentDetail.getDocumentTitle();
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobDTO.getWobNumber(), documentTitle);
		esito.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
		esito.setEsito(false);
		esito.setIdDocumento(documentDetail.getNumeroDocumento());
		IFilenetCEHelper fcehAdmin = null;

		try {
			fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()),
					utente.getIdAoo());

			String[] elencoLibroFirma = wobDTO.getElencoLibroFirma();
			int count = wobDTO.getCount();

			int idTipoAssegnazione = wobDTO.getTipoAssegnazioneId();
			int idFascicolo = wobDTO.getIdFascicolo();

			boolean isWorkflowParziale = false;
			if (elencoLibroFirma[0].split("\\,").length == Constants.Mail.WORK_PARZIALE_MAIL_POSITION) {
				isWorkflowParziale = true;
			}
			if (isWorkflowParziale) {
				idTipoAssegnazione = Integer.parseInt(elencoLibroFirma[count - 1].split("\\,")[2]);
			}
			if (count == elencoLibroFirma.length && isOneElettronico(documentDetail.getDestinatariDocumento())
					&& (isWorkflowParziale || idTipoAssegnazione == TipoAssegnazioneEnum.FIRMA.getId())) {
				// Invia la mail
				Document mail = fcehAdmin.fetchMailSpedizione(documentTitle, utente.getIdAoo().intValue());

				final String from = mail.getProperties()
						.getStringValue(pp.getParameterByKey(PropertiesNameEnum.FROM_MAIL_METAKEY));
				final String to = mail.getProperties()
						.getStringValue(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY));
				final String cc = mail.getProperties()
						.getStringValue(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_CC_EMAIL_METAKEY));
				String oggetto = mail.getProperties()
						.getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_EMAIL_METAKEY));

				ContentTransfer ct;
				if (FilenetCEHelper.hasDocumentContentTransfer(mail)) {
					ct = FilenetCEHelper.getDocumentContentTransfer(mail);
				} else {
					throw new RedException(
							"Errore durante il recupero del content della mail con idDocumento: " + documentTitle);
				}

				String testoMail = null;
				if (ct != null) {
					testoMail = new String(FileUtils.toByteArrayHtml(mail.accessContentStream(0)),
							StandardCharsets.UTF_8.name());
				}

				Integer tipologiaCasellaId = fcehAdmin.getTipoCasellaPostaleFromNome(from);
				if (tipologiaCasellaId == null) {
					// Attenzione, in questo caso vuol dire che la mail non è configurata su FileNet
					LOGGER.warn("Attenzione il documento con id " + documentTitle
							+ " ha la mail del mittente non impostata su FileNet.");
					LOGGER.warn(
							"In questo caso non riesce a recuperare la tipologia del destinatario, di default sarà PEO");
					tipologiaCasellaId = 1;
				}

				oggetto = getPrefissoOggettoMail(documentTitle, utente.getCodiceAoo(), protocollo) + " "
						+ oggetto;

				codaMailSRV.creaMailEInserisciInCoda(documentTitle, documentDetail.getGuid(), from,
						destinatariCollection, to, cc, oggetto, testoMail, null, null, 1, tipologiaCasellaId, null,
						false, true, null, null, utente, connection, firmaDigitale, simulaInvioNPS);

				if (chiudiFascicolo) {
					chiudiFascicolo(utente, fcehAdmin, idFascicolo);
				}
			}

			impostaEsitoOk(esito);
		} catch (final Exception e) {
			gestisciEccezioneStepFirma(e, esito, SignErrorEnum.INVIO_EMAIL_STEP_ASIGN_COD_ERROR, null);
		} finally {
			popSubject(fcehAdmin);
		}

		return esito;
	}
	
	/**
	 * @see it.ibm.red.business.service.ISignSRV#invioMail(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.ASignItemDTO, java.sql.Connection).
	 */
	@Override
	public EsitoOperazioneDTO invioMail(UtenteDTO utente, ASignItemDTO aSignItem, Connection connection) {
		String documentTitle = aSignItem.getDocumentTitle();
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(aSignItem.getWobNumber(), documentTitle);
		esito.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
		esito.setEsito(false);
		IFilenetCEHelper fcehAdmin = null;
		
		try {
			fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());			
			
			Document documentForDetails = fcehAdmin.getDocumentRedForDetail(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle);
			DetailDocumentoDTO documentDetailDTO = getDocumentDetailDTO("invioEmaildocumentTitle", documentForDetails, utente, connection);
			
			esito.setIdDocumento(documentDetailDTO.getNumeroDocumento());
			
			Collection<?> destinatariCollection = (Collection<?>) documentForDetails.getProperties().get(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY)).getObjectValue();
			boolean simulaInvioNps = hasToSimulateInvioNps(documentForDetails, utente, documentDetailDTO.getIdTipologiaDocumento(), connection, fcehAdmin);
			
			// Si recuperano dal DB i dati del workflow memorizzati nel DB
			Map<String, String> datiWorkflow = aSignDAO.getItemData(connection, aSignItem.getId());

			int idTipoAssegnazione = Integer.parseInt(datiWorkflow.get(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY)));
			int idFascicolo = Integer.parseInt(datiWorkflow.get(pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY)));
			String elencoLibroFirmaDB = datiWorkflow.get(pp.getParameterByKey(PropertiesNameEnum.ELENCO_LIBROFIRMA_METAKEY));
			String[] elencoLibroFirma = null;
			if (!StringUtils.isNullOrEmpty(elencoLibroFirmaDB)) {
				elencoLibroFirma = elencoLibroFirmaDB.split("\\|");
			} else {
				elencoLibroFirma = new String[1];
				elencoLibroFirma[0] = Constants.EMPTY_STRING;
			}
			int count = 0;
			String countDB = datiWorkflow.get(pp.getParameterByKey(PropertiesNameEnum.COUNT_METAKEY));
			if (!StringUtils.isNullOrEmpty(countDB)) {
				count = Integer.parseInt(countDB);
			}

			boolean isWorkflowParziale = false;
			if (elencoLibroFirma[0].split("\\,").length == Constants.Mail.WORK_PARZIALE_MAIL_POSITION) {
				isWorkflowParziale = true;
				idTipoAssegnazione = Integer.parseInt(elencoLibroFirma[count - 1].split("\\,")[2]);
			}
			
			if (count == elencoLibroFirma.length && isOneElettronico(documentDetailDTO.getDestinatariDocumento())
					&& (isWorkflowParziale || (!isWorkflowParziale && idTipoAssegnazione == TipoAssegnazioneEnum.FIRMA.getId()))) {
				// Invia la mail
				Document mail = fcehAdmin.fetchMailSpedizione(documentTitle, utente.getIdAoo().intValue());

				String from = mail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.FROM_MAIL_METAKEY));
				String to = mail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY));
				String cc = mail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_CC_EMAIL_METAKEY));
				String oggetto = mail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_EMAIL_METAKEY));

				ContentTransfer ct;
				if (FilenetCEHelper.hasDocumentContentTransfer(mail)) {
					ct = FilenetCEHelper.getDocumentContentTransfer(mail);
				} else {
					throw new RedException("Errore durante il recupero del content della mail con idDocumento: " + documentTitle);
				}
				
				String testoMail = null;
				if (ct != null) {
					testoMail = new String(FileUtils.toByteArrayHtml(mail.accessContentStream(0)), StandardCharsets.UTF_8.name());
				}
				
				Integer tipologiaCasellaId = fcehAdmin.getTipoCasellaPostaleFromNome(from);
				if (tipologiaCasellaId == null) {
					// Attenzione, in questo caso vuol dire che la mail non è configurata su FileNet
					LOGGER.warn("Attenzione il documento con id " + documentTitle + " ha la mail del mittente non impostata su FileNet.");
					LOGGER.warn("In questo caso non riesce a recuperare la tipologia del destinatario, di default sarà PEO");
					tipologiaCasellaId = 1;
				}
				
				oggetto = getPrefissoOggettoMail(documentTitle, utente.getCodiceAoo(), documentDetailDTO.getProtocollo()) + " " + oggetto;
				
				codaMailSRV.creaMailEInserisciInCoda(documentTitle, documentDetailDTO.getGuid(), from, destinatariCollection, to, cc, 
						oggetto, testoMail, null, null, 1, tipologiaCasellaId, null, false, true, null, null, utente, connection, !SignModeEnum.AUTOGRAFA.equals(aSignItem.getSignMode()),
						simulaInvioNps);
				
				chiudiFascicolo(utente, fcehAdmin, idFascicolo);
			}
			
			impostaEsitoOk(esito);
		} catch (Exception e) {
			gestisciEccezioneStepFirma(e, esito, SignErrorEnum.INVIO_EMAIL_STEP_ASIGN_COD_ERROR, null);
		} finally {
			popSubject(fcehAdmin);
		}
		
		return esito;
	}
	
	
	/**
	 * Metodo per la chiusura del fascicolo.
	 * 
	 * @param utente      dati utente
	 * @param fceh        helper gestione ce
	 * @param idFascicolo identificativo fascicolo
	 */
	private void chiudiFascicolo(final UtenteDTO utente, final IFilenetCEHelper fceh, final int idFascicolo) {
		// Chiusura del fascicolo
		fascicoloSRV.aggiornaStato(String.valueOf(idFascicolo), FilenetStatoFascicoloEnum.CHIUSO, utente.getIdAoo(), fceh);
		
		// Eliminato invio Notifica della chiusura del fascicolo
	}

	/**
	 * Recupero tipo spedizione.
	 * 
	 * N.B. questo metodo al contrario del metodo presente su NSD non si preoccupa
	 * di inviare una mail.
	 * 
	 * @param wobDTO - DTO workflow
	 * @param documentDetail - info documento
	 * @return identificativo tipo spedizione
	 */
	@Override
	public int getTipoSpedizione(final PEDocumentoDTO wobDTO, final DetailDocumentoDTO documentDetail) {
		int tipoSpedizione = -1;
		final String[] elencoLibroFirma = wobDTO.getElencoLibroFirma();
		final int count = wobDTO.getCount();

		int idTipoAssegnazione = wobDTO.getTipoAssegnazioneId();
		boolean isWorkflowParziale = false;
		if (elencoLibroFirma[0].split("\\,").length == Constants.Mail.WORK_PARZIALE_MAIL_POSITION) {
			isWorkflowParziale = true;
		}
		if (isWorkflowParziale) {
			idTipoAssegnazione = Integer.parseInt(elencoLibroFirma[count - 1].split("\\,")[2]);
		}
		if (count == elencoLibroFirma.length) {
			if (isOneElettronico(documentDetail.getDestinatariDocumento())) {
				if (isWorkflowParziale || (idTipoAssegnazione == TipoAssegnazioneEnum.FIRMA.getId())) {
					if (documentDetail.isDestinatariElettronici()) {
						tipoSpedizione = TipoSpedizioneEnum.ELETTRONICO.getId().intValue();
					} else {
						tipoSpedizione = TipoSpedizioneEnum.CARTACEO.getId().intValue();
					}
				}
			} else if (documentDetail.isHasDestinatariInterni() && !documentDetail.isHasDestinatariEsterni()) {
				tipoSpedizione = TipoSpedizioneEnum.INTERNO.getId().intValue();
			} else {
				tipoSpedizione = TipoSpedizioneEnum.CARTACEO.getId().intValue();
			}
		}
		return tipoSpedizione;
	}

	/**
	 * Restituisce l'oggetto della mail da inviare.
	 * 
	 * @param documentoID     - Identificativo del documento.
	 * @param descrizioneEnte - Descrizione dell'ente.
	 * @param protocollo      - Protocollo del documento principale.
	 * @return Oggetto della mail.
	 */
	private static String getPrefissoOggettoMail(final String documentoID, final String codiceAOO, final ProtocolloDTO protocollo) {
		int annoProtocollo = -1;
		if (protocollo.getAnnoProtocollo() != null) {
			annoProtocollo = protocollo.getAnnoProtocolloInt();
		}
		final int numeroProtocollo = protocollo.getNumeroProtocollo();
		String dataProtocollo = null;
		if (protocollo.getDataProtocollo() != null) {
			dataProtocollo = DateUtils.formatDataByUSLocale(protocollo.getDataProtocollo().toString(), null);
		}

		String title;
		String completeTitle;
		String prefixOggettoMail;
		if (annoProtocollo > -1 && numeroProtocollo > -1) {
			title = "Prot. " + numeroProtocollo + "/" + annoProtocollo;
			completeTitle = title + " del " + dataProtocollo;
		} else {
			title = DOCUMENTO_LABEL + documentoID;
			completeTitle = title;
		}
		prefixOggettoMail = "MEF - " + codiceAOO + " - " + completeTitle;

		return prefixOggettoMail;
	}
	
	/**
	 * Esegue il deposito sul DB dell'upload e dell'associazione del protocollo NPS.
	 * 
	 * @param protocolloNpsId
	 *            - Identificativo del protocollo NPS.
	 * @param documentContentInputStream
	 *            - Content del documento.
	 * @param guid
	 *            - Identificativo Filenet del documento.
	 * @param mimeType
	 *            - MimeType del documento.
	 * @param fileName
	 *            - Nome del file del documento.
	 * @param descrizione
	 *            - Descrizione del documento.
	 * @param utente
	 *            - Utente che esegue l'operazione.
	 * @param isPrincipale
	 *            - Documento principale o allegato
	 * @param numeroAnnoProtocollo
	 *            - NumeroProtocollo/AnnoProtocollo_CodiceAOO.
	 * @param idDocumento
	 *            - Document title.
	 * @param connection
	 *            - Connessione al DB.
	 */
	private void uploadAssociazioneProtocolloToAsyncNps(final String protocolloNpsId,
			final InputStream documentContentInputStream, final String guid, final String mimeType,
			final String fileName, final String descrizione, final UtenteDTO utente, final boolean isPrincipale,
			final String numeroAnnoProtocollo, final String idDocumento, final Connection connection, 
			final String idDocumentoOperazione, final DocTipoOperazioneType tipoOperazioneEnum) {

		try {

			final int idNar = npsSRV.uploadDocToAsyncNps(documentContentInputStream, guid, protocolloNpsId, mimeType,
					fileName, descrizione, numeroAnnoProtocollo, utente, idDocumento, connection);

			npsSRV.associateDocumentoProtocolloToAsyncNps(idNar, guid, protocolloNpsId, fileName, descrizione, utente,
					isPrincipale, true, null, null, numeroAnnoProtocollo, idDocumento, connection);

		} catch (final Exception e) {
			LOGGER.error("Errore nell'inserimento della request di upload/aggiungi su NPS in modo asincrono "
					+ e.getMessage(), e);
		}
	}

	/**
	 * @param documentDetailDTO
	 * @param documentForDetails
	 * @param protocollo
	 * @param utente
	 * @param fceh
	 * @param connection
	 */
	private void doActionsProtocolloBooked(String signTransactionId, DetailDocumentoDTO documentDetailDTO, Document documentForDetails, ProtocolloDTO protocollo, 
			UtenteDTO utente, IFilenetCEHelper fceh, Connection connection) {
				
		// ### Si aggiorna il documento su FileNet con le informazioni relative al protocollo appena staccato
		fceh.setPropertiesAndSaveDocument(documentForDetails, getMetadatiProtocolloToUpdate(protocollo, utente, true), true);
		
		LOGGER.info(signTransactionId + " salvati metadati di protocollo su Filenet");

		// ### Gestione dei documenti generati da flussi: esecuzione delle eventuali
		// operazioni aggiuntive da effettuare a valle della protocollazione in uscita
		doActionsFlussoProtocolloBooked(signTransactionId, documentDetailDTO, protocollo, utente, connection);
	}
	
	
	/**
	 * @param signTransactionId
	 * @param documentDetailDTO
	 * @param protocollo
	 * @param utente
	 * @param connection
	 */
	private void doActionsFlussoProtocolloBooked(String signTransactionId, DetailDocumentoDTO documentDetailDTO, ProtocolloDTO protocollo, UtenteDTO utente, Connection connection) {
		if (!StringUtils.isNullOrEmpty(documentDetailDTO.getCodiceFlusso())) {
			// Si recupera il service appropriato per il flusso a cui afferisce il documento
			final IAzioniFlussoBaseSRV azioniFlussoBaseSRV = azioniFlussoSRV.retrieveAzioniFlussoSRV(documentDetailDTO.getCodiceFlusso());
			LOGGER.info(signTransactionId + " recuperata IAzioniFlussoBaseSRV per " + documentDetailDTO.getCodiceFlusso());
			// Si eseguono le operazioni
			azioniFlussoBaseSRV.doAfterProtocollazioneUscita(Integer.parseInt(documentDetailDTO.getDocumentTitle()), protocollo, utente.getIdAoo(), connection);
			LOGGER.info(signTransactionId + " eseguita IAzioniFlussoBaseSRV.doAfterProtocollazioneUscita per " + documentDetailDTO.getCodiceFlusso());
		}
	}

	/**
	 * Restituisce se presente, 'idNodoCoordinatore' + ',' + 'idUtenteCoordinatore'.
	 * 
	 * @param wob - Oggetto FileNet del WF.
	 * @return Security del coordinatore.
	 */
	public String getSecurityCoordinatore(final VWWorkObject wob) {
		final Integer coordinatoreFlag = (Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.COORDINATORE_FLAG_METAKEY));
		final Integer idUtenteCoordinatore = (Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_COORDINATORE_METAKEY));
		final Integer idNodoCoordinatore = (Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_UFFICIO_COORDINATORE_METAKEY));
		String securityCoordinatore = null;
		if (coordinatoreFlag != null && coordinatoreFlag == 1 && idUtenteCoordinatore != null && idNodoCoordinatore != null) {
			securityCoordinatore = idNodoCoordinatore + "," + idUtenteCoordinatore;
		}
		return securityCoordinatore;
	}

	/**
	 * Recupera il protocollo da NPS.
	 * 
	 * @param wob               - Oggetto Filenet relativo al WF del documento da
	 *                          firmare.
	 * @param documentDetailDTO - DTO rappresentante il documento CE da firmare.
	 * @param utente            - Utente che effetta l'operazione di firma.
	 * @param fceh              - Helper per la connessione al CE.
	 * @param connection        - Connessione al DB.
	 * @param isRiservato       - flag riservato
	 * @return Protocollo NPS.
	 */
	@Override
	public ProtocolloNpsDTO getNewProtocolloNps(final VWWorkObject wob, final DetailDocumentoDTO documentDetailDTO, final UtenteDTO utente, final IFilenetCEHelper fceh,
			final Connection connection, final Boolean isRiservato) {
		final String acl = getACL4NPS(wob, documentDetailDTO, utente, fceh, isRiservato);
		return npsSRV.creaProtocolloUscita(documentDetailDTO, acl, utente, Builder.DA_SPEDIRE, connection, fceh);
	}

	/**
	 * @param wob
	 * @param documentDetailDTO
	 * @param utente
	 * @param fceh
	 * @param isRiservato
	 * @return
	 */
	private String getACL4NPS(final VWWorkObject wob, final DetailDocumentoDTO documentDetailDTO, final UtenteDTO utente, final IFilenetCEHelper fceh,
			final Boolean isRiservato) {
		final String[] destinatariInterniArray = (String[]) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ELENCO_DESTINATARI_INTERNI_METAKEY));

		final String securityCoordinatore = getSecurityCoordinatore(wob);
		// Recupero le ACL
		final List<SecurityDTO> securities = securitySRV.getSecurityUtentePerUfficio(utente, documentDetailDTO.getDocumentTitle(), utente.getIdUfficio() + "",
				Arrays.asList(destinatariInterniArray), securityCoordinatore, isRiservato);
		StringBuilder acl = new StringBuilder(Constants.EMPTY_STRING);
		if (securities != null) {
			try {
				for (final SecurityDTO securityObj : securities) {
					acl.append(fceh.getADGroupNameFormId(String.valueOf(securityObj.getGruppo().getIdUfficio())) + ",");
				}
				if (!securities.isEmpty()) {
					acl.append(acl.substring(acl.length() - 1));
				}
			} catch (final Exception e) {
				LOGGER.error("Errore durante il recupero delle ACL ", e);
			}
		}
		
		return acl.toString();
	}

	/**
	 * Avanza il WF a seguito della firma.
	 *
	 * @param utente
	 *            dati utente
	 * @param fpeh
	 *            helper pe
	 * @param wob
	 *            wob
	 * @param documentTitle
	 *            the document title
	 * @param tipoSpedizione
	 *            the tipo spedizione
	 * @param isRiservato
	 *            the is riservato
	 * @param fceh
	 *            the fceh
	 * @param connection
	 *            the connection
	 * @param idFascicolo
	 *            the id fascicolo
	 * @param flagDestinariNonElettronici
	 *            the flag destinari non elettronici
	 * @param firmaMultipla
	 *            booleano per la firma multipla
	 * @param firmaAutografa
	 *            booleano per la firma autografa
	 * @throws InterruptedException
	 * 
	 */
	private void avanzaWFFirma(final UtenteDTO utente, final FilenetPEHelper fpeh, final VWWorkObject wob,
			final String documentTitle, final int tipoSpedizione, final Boolean isRiservato, final IFilenetCEHelper fceh, final Connection connection, 
			final Integer idFascicolo, final boolean flagDestinariNonElettronici, final boolean firmaMultipla, final boolean firmaAutografa, 
			final boolean chiudiFascicolo) throws InterruptedException {
		// ### Avanzo workflow dopo la firma #################################################################
		HashMap<String, Object> metadati = new HashMap<>();
		if (tipoSpedizione > 0) {
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_SPEDIZIONE_METAKEY), tipoSpedizione);
		}
		metadati.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId().intValue());
		metadati.put("motivazioneAssegnazione", "Firma digitale" + (firmaMultipla ? " multipla" : Constants.EMPTY_STRING));

		// ### Avanzamento del workflow
		final boolean esitoAvanzamentoWorkflow = avanzaWF(wob, metadati, firmaAutografa, fpeh);

		if (esitoAvanzamentoWorkflow && Boolean.TRUE.equals(isRiservato) && flagDestinariNonElettronici && !firmaMultipla) {
			// Recupero il nuovo WF
			// Aspetto 5 secondi altrimenti rischio di non trovare il wf, se non lo trova non si rompe solo non chiude il wf in automatico
			Thread.sleep(5000);
			final Integer idDoc = (Integer) TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY);
			final VWWorkObject wobNewWF = fpeh.getWorkflowPrincipale(Constants.EMPTY_STRING + idDoc, utente.getFcDTO().getIdClientAoo());
			avanzaWfSpedizioneAutomatica(fpeh, fceh, connection, utente, idFascicolo, documentTitle, wobNewWF, chiudiFascicolo);
		}
	}

	
	/**
	 * Aggiorna le security a seguito della firma.
	 *
	 * @param utente        dati utente
	 * @param wob           wob
	 * @param documentTitle the document title
	 * @param isRiservato   the is riservato
	 */
	private void aggiornaSecurity(final UtenteDTO utente, final VWWorkObject wob, final String documentTitle, final Boolean isRiservato) {
		// ### AGGIORNAMENTO DELLE SECURITY doc/fascicolo ####################################################
		String[] destinatariInterniArray = (String[]) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ELENCO_DESTINATARI_INTERNI_METAKEY));
		final String securityCoordinatore = getSecurityCoordinatore(wob);
		ListSecurityDTO securities = securitySRV.getSecurityUtentePerUfficioFascicolo(utente, documentTitle, utente.getIdUfficio() + Constants.EMPTY_STRING,
				Arrays.asList(destinatariInterniArray), securityCoordinatore, isRiservato);
		
		securitySRV.modificaSecurityFascicoli(utente, securities, documentTitle);
		
		securities = securitySRV.getSecurityUtentePerUfficio(utente, documentTitle, utente.getIdUfficio() + "", Arrays.asList(destinatariInterniArray), securityCoordinatore,
				isRiservato);
		securitySRV.updateSecurity(utente, documentTitle, pp.getParameterByKey(PropertiesNameEnum.FOLDER_DOCUMENTI_CE_KEY), securities, true);
		// ###################################################################################################
	}

	/**
	 * Esegue l'avanzamento del workflow.
	 * 
	 * @param wob - WobNumber del workflow da avanzare.
	 * @param metadati - Metadati da aggiornare.
	 * @param firmaAutografa - Indica se la firma è autografa.
	 * @param fpeh         - Helper per la connessione al PE.
	 * @return Esito dell'operazione di avanzamento.
	 */
	private boolean avanzaWF(final VWWorkObject wob, final HashMap<String, Object> metadati, final boolean firmaAutografa, 
			final FilenetPEHelper fpeh) {
		boolean esito = false;
		
		String response = getResponse(wob, firmaAutografa, fpeh);
		if (!StringUtils.isNullOrEmpty(response)) {
			esito = fpeh.nextStep(wob, metadati, response);
		}
		
		return esito;
	}

	/**
	 * Restituisce l'input per l'avanzameto del workflow dato l'Enum.
	 * 
	 * @param wob - WobNumber del workflow.
	 * @param firmaAutografa - Indica se la firma è autografa.
	 * @return L'input per l'avanzameto del workflow.
	 */
	private final String getResponse(final VWWorkObject wob, final boolean firmaAutografa, final FilenetPEHelper fpeh) {
		LibroFirmaPerFirmaResponseEnum e = LibroFirmaPerFirmaResponseEnum.FIRMA_DIGITALE;
		if (firmaAutografa) {
			e = LibroFirmaPerFirmaResponseEnum.FIRMA_AUTOGRAFA;
		}
		
		final String[] responses = fpeh.getResponses(wob);
		
		return e.firstIn(responses);
	}

	/**
	 * Restituisce la validità della durata della transazione con PK, dato l'inizio
	 * della transazione.
	 * 
	 * @param startTransaction - Data dell'inizio della transazione.
	 * @return Esito del controllo sulla validità.
	 */
	private static boolean checkTokenExpired(final Date startTransaction) {
		boolean esitoCheck = true;
		if ((new Date().getTime() - startTransaction.getTime()) > Constants.Firma.TOKEN_EXP_WARN_TIME) {
			esitoCheck = false;
		}
		return esitoCheck;
	}

	/**
	 * Firma autografa di Red Mobile.
	 *
	 * @param fcDTO     the fc DTO
	 * @param wobNumber the wob number
	 * @param utente    the utente
	 * @return the esito operazione DTO
	 */
	@Override
	public final EsitoOperazioneDTO firmaAutografaMobile(final String wobNumber, final UtenteDTO utente) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		Integer idDocumento = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;

		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);
			final PEDocumentoDTO doc = TrasformPE.transform(wob, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO);

			if (!doc.getOperazione().equals(TipoOperazioneLibroFirmaEnum.FIRMA)) {
				esito.setCodiceErrore(SignErrorEnum.FORBIDDEN_OPERATION_COD_ERROR);
				esito.setNote(SignErrorEnum.FORBIDDEN_OPERATION_COD_ERROR.getMessage());
				return esito;
			}

			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final Document dFirma = fceh.getDocumentForDetail(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), doc.getIdDocumento());
			idDocumento = (Integer) TrasformerCE.getMetadato(dFirma, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			esito.setIdDocumento(idDocumento);
			Boolean oldValue = (Boolean) TrasformerCE.getMetadato(dFirma, PropertiesNameEnum.FLAG_FIRMA_AUTOGRAFA_RM_METAKEY);
			if (oldValue == null) {
				oldValue = false;
			}
			dFirma.getProperties().putValue(pp.getParameterByKey(PropertiesNameEnum.FLAG_FIRMA_AUTOGRAFA_RM_METAKEY), !oldValue);
			dFirma.save(RefreshMode.REFRESH);

			return new EsitoOperazioneDTO(null, null, idDocumento, wobNumber);
		} catch (final Exception e) {
			LOGGER.error("Errore durante la firma autografa del documento con WOB_NUMBER: " + wobNumber + " ", e);
			esito.setNote("Errore durante la firma autografa del documento con WOB_NUMBER: " + wobNumber + " \n");
			esito.setIdDocumento(idDocumento);
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}

		return esito;
	}

	/**
	 * Multi firma autografa di Red Mobile.
	 *
	 * @param fcDTO  the fc DTO
	 * @param wobs   the wobs
	 * @param utente the utente
	 * @return the collection
	 */
	@Override
	public final Collection<EsitoOperazioneDTO> multiFirmaAutografaMobile(final Collection<String> wobs, final UtenteDTO utente) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		for (final String wobNumber : wobs) {
			esiti.add(firmaAutografaMobile(wobNumber, utente));
		}
		return esiti;
	}

	/**
	 * Avanzamento workflow per spedizione automatica.
	 * 
	 * @param fpeh        helper gestione pe
	 * @param fceh        helper gestione ce
	 * @param connection  connessione al db
	 * @param utente      dati utente
	 * @param idFascicolo identificativo fascicolo
	 * @param idDocumento identificativo documento
	 * @param wo          workflow
	 */
	private void avanzaWfSpedizioneAutomatica(final FilenetPEHelper fpeh, final IFilenetCEHelper fceh, final Connection connection, final UtenteDTO utente,
			final Integer idFascicolo, final String idDocumento, final VWWorkObject wo, final boolean chiudiFascicolo) {
		final HashMap<String, Object> metadati = new HashMap<>();
		metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY), "1");
		metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), "0");
		metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), "Spedizione Riservata");
		
		final Calendar cal = new GregorianCalendar();
		cal.add(Calendar.MINUTE, 1);
		final String date = new SimpleDateFormat(DateUtils.DD_MM_YYYY_HH_MM_SS).format(cal.getTime());
		metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DATA_ASSEGNAZIONE_WF_METAKEY), date);
		
		if(chiudiFascicolo) {
			chiudiFascicolo(utente, fceh, idFascicolo);
		}
//		// Verrà eseguita solo se è presente almento un destinatario cartaceo
//		if (wo != null && ArrayUtils.contains(wo.getStepResponses(), ResponsesRedEnum.SPEDITO.getResponse())) {
//			fpeh.nextStep(wo, metadati, ResponsesRedEnum.SPEDITO.getResponse());
//		}

		final List<String> props = Arrays.asList(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY),
				pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY), pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY),
				pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
		final Document document = fceh.getDocumentByIdGestionale(idDocumento, props, null, utente.getIdAoo().intValue(), null, null);
		final String idProtocollo = TrasformerCE.getMetadato(document, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY) != null
				? TrasformerCE.getMetadato(document, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY).toString()
				: "";
		final Aoo aoo = aooDAO.getAoo(utente.getIdAoo(), connection);
		if (TipologiaProtocollazioneEnum.isProtocolloNPS(aoo.getTipoProtocollo().getIdTipoProtocollo()) && !StringUtils.isNullOrEmpty(idProtocollo)) {
			final String dataSpedizione = new SimpleDateFormat(DateUtils.DD_MM_YYYY_HH_MM_SS).format(new Date());

			final String infoProtocollo = new StringBuilder()
					.append(document.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY))).append("/")
					.append(document.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY))).append("_")
					.append(aoo.getCodiceAoo()).toString();

			npsSRV.spedisciProtocolloUscitaAsync(utente, infoProtocollo, idProtocollo, dataSpedizione, null, null, idDocumento, connection);
		}
		
		gestisciAllacci(fceh, fpeh, aoo, document, utente, true, connection); // Non serve associare la registrazione ausiliaria, è stato fatto in precedenza
	}

	/**
	 * Gestione allacci.
	 * 
	 * @param fceh			helper gestione ce
	 * @param fpeh			helper gestione pe
	 * @param aoo			oggetto aoo
	 * @param documento		documento di cui gestire gli allacci
	 * @param utente		dati utente
	 * @param allineaNPS	stabilisce se comunicare (in modalità asincrona) gli allacci a NPS
	 * @param connection	connessione db
	 */
	@Override
	public void gestisciAllacci(final IFilenetCEHelper fceh, final FilenetPEHelper fpeh, final Aoo aoo, final Document documento,
			final UtenteDTO utente, final boolean allineaNPS, final Connection connection) {
		
		try {
			final String documentTitle = TrasformerCE.getMetadato(documento, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY).toString();

			if (documentTitle != null) {
				Integer idDocumento = Integer.parseInt(documentTitle);
				Collection<RispostaAllaccioDTO> documentiDaAllacciare = allaccioDAO.getDocumentiRispostaAllaccio(idDocumento, aoo.getIdAoo().intValue(), connection);
				
				for (RispostaAllaccioDTO rispostaAllaccio : documentiDaAllacciare) {
					
					// Recupera l'allaccio principale se siamo nel caso di un visto/osservazione
					if (rispostaAllaccio.isPrincipale() && (TipoAllaccioEnum.VISTO.equals(rispostaAllaccio.getTipoAllaccioEnum())
							|| TipoAllaccioEnum.OSSERVAZIONE.equals(rispostaAllaccio.getTipoAllaccioEnum())
									|| TipoAllaccioEnum.RELAZIONE_POSITIVA.equals(rispostaAllaccio.getTipoAllaccioEnum())
											|| TipoAllaccioEnum.RELAZIONE_NEGATIVA.equals(rispostaAllaccio.getTipoAllaccioEnum()))) {
						// recupera il workflow principale
						VWWorkObject workflowPrincipale = fpeh.getWorkflowPrincipale(rispostaAllaccio.getIdDocumentoAllacciato(), utente.getFcDTO().getIdClientAoo());
						// Si sollecita la response solo se è presente tra quelle disponibili nello step attuale
						if (workflowPrincipale != null && ArrayUtils.contains(workflowPrincipale.getStepResponses(), ResponsesRedEnum.SPEDISCI_USCITA.getResponse())) {
							// Sollecita la response SPEDISCI USCITA sul workflow principale del documento in ingresso
							fpeh.nextStep(workflowPrincipale, null, ResponsesRedEnum.SPEDISCI_USCITA.getResponse());
						}
						
						// Aggiorna allaccio principale da chiudere
						if (!Boolean.TRUE.equals(rispostaAllaccio.getDocumentoDaChiudere())) {
							allaccioDAO.chiudiAllaccioPrincipale(idDocumento, utente.getIdAoo(), connection);
						}
					}
					
					// Se il documento è stato selezionato come "da chiudere"
					if (Boolean.TRUE.equals(rispostaAllaccio.getDocumentoDaChiudere())) {
						// Ottieni i fascicoli del documento
						final List<FascicoloDTO> fascicoli = fceh.getFascicoliDocumentoPerMetodoAllacci(rispostaAllaccio.getIdDocumentoAllacciato(),
								aoo.getIdAoo().intValue());
						if (!CollectionUtils.isEmpty(fascicoli)) {
							for (final FascicoloDTO fascicolo : fascicoli) {
								// Chiusura del fascicolo del documento principale senza inviare notifica
								// Eliminato invio notifica chiusura del fascicolo sul documento rispostaAllaccio.getIdDocumentoAllacciato()
								if (!FilenetStatoFascicoloEnum.CHIUSO.getNome().equals(fascicolo.getStato())) {
									chiudiFascicolo(utente, fceh, Integer.parseInt(fascicolo.getIdFascicolo()));
								}
							}
						}
					}

				}

				
				if (allineaNPS) {
					inviaAllacciToNPS(documento, documentTitle, documentiDaAllacciare, utente, aoo, connection);
				}
			} else {
				throw new RedException("Non è stato possibile recuperare il Document Title del documento durante la gestione degli allacci!");
			}
			
		} catch (final Exception e) {
			LOGGER.error("Errore riscontrato: ", e);
			throw e;
		}
	}

	/**
	 * @param documentFilenet  documento FileNet che è stato firmato
	 * @param documentDetail   documento che è stato firmato
	 * @param utenteFirmatario utente che ha firmato il documento
	 */
	@Override
	public void gestisciDestinatariInterni(final Document documentFilenet, final DetailDocumentoDTO documentDetail, final ProtocolloDTO protocollo,
			final UtenteDTO utenteFirmatario, final IFilenetCEHelper fceh, final Connection con) {
		final String idDocumento = documentDetail.getDocumentTitle();
		LOGGER.info("gestisciDestinatariInterni -> START. Documento: " + idDocumento);

		final String classeDocumentale = documentFilenet.getClassName();
		// Se il documento ha destinatari interni e non è una DSR o una FATTURA FEPA
		// si procede con la creazione di un nuovo documento per assegnazione interna a
		// ciascun destinatario (interno)
		if (documentDetail.isHasDestinatariInterni() && !pp.getParameterByKey(PropertiesNameEnum.DSR_CLASSNAME_FN_METAKEY).equals(classeDocumentale)
				&& !pp.getParameterByKey(PropertiesNameEnum.FATTURA_FEPA_CLASSNAME_FN_METAKEY).equals(classeDocumentale)) {
			final DetailDocumentRedDTO nuovoDoc = new DetailDocumentRedDTO(); // Nuovo documento per l'assegnazione interna

			// ### Impostazione degli attributi/metadati nel nuovo documento per
			// assegnazione interna da creare -> START
			final int idTipologiaDocumento = tipologiaDocumentoSRV.getTipologiaDocumentalePredefinita(utenteFirmatario.getIdAoo(), utenteFirmatario.getCodiceAoo(),
					TipoCategoriaEnum.ENTRATA, con);
			nuovoDoc.setIdTipologiaDocumento(idTipologiaDocumento); // Tipologia Documento
			final long lIdTipoProcedimento = tipoProcedimentoDAO.getTipiProcedimentoByTipologiaDocumento(con, idTipologiaDocumento).get(0).getTipoProcedimentoId();
			nuovoDoc.setIdTipologiaProcedimento((int) lIdTipoProcedimento); // Tipo Procedimento

			Contatto contattoMittente = null;

			final RicercaRubricaDTO ricercaContattoObj = new RicercaRubricaDTO();
			ricercaContattoObj.setNome(utenteFirmatario.getNome());
			ricercaContattoObj.setCognome(utenteFirmatario.getCognome());
			ricercaContattoObj.setRicercaRED(true);
			ricercaContattoObj.setRicercaMEF(true);
			ricercaContattoObj.setIdAoo(utenteFirmatario.getIdAoo().intValue());
			final List<Contatto> contatti = contattoDAO.ricerca(null, ricercaContattoObj, con);

			if (!CollectionUtils.isEmpty(contatti)) {
				contattoMittente = contatti.get(0);
				// Verifico se ho un contatto sulla rubrica MEF
				for (final Contatto contRubrica : contatti) {
					if (contRubrica.getTipoRubricaEnum().equals(TipoRubricaEnum.MEF)) {
						contattoMittente = contRubrica;
						break;
					}
				}
			} else {
				contattoMittente = new Contatto();
				contattoMittente.setNome(utenteFirmatario.getNome());
				contattoMittente.setCognome(utenteFirmatario.getCognome());
				contattoMittente.setAliasContatto(utenteFirmatario.getCognome() + " " + utenteFirmatario.getNome());
				contattoMittente.setTipoRubrica("RED");
				contattoMittente.setIdAOO(utenteFirmatario.getIdAoo());
				contattoMittente.setTipoPersona("F");
				contattoMittente.setPubblico(1);
				contattoMittente.setDatacreazione(new Date());
				final Long contattoId = contattoDAO.inserisci(null, contattoMittente, con);
				contattoMittente.setContattoID(contattoId);
			}

			nuovoDoc.setMittenteContatto(contattoMittente); // Mittente

			nuovoDoc.setIdCategoriaDocumento(CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA.getIds()[0]); // Categoria: Assegnazione Interna
			nuovoDoc.setIdUtenteCreatore(utenteFirmatario.getId().intValue()); // Utente Creatore
			nuovoDoc.setIdUfficioCreatore(utenteFirmatario.getIdUfficio().intValue()); // Ufficio Creatore
			nuovoDoc.setFormatoDocumentoEnum(FormatoDocumentoEnum.ELETTRONICO); // Formato Documento
			nuovoDoc.setIdAOO((Integer) TrasformerCE.getMetadato(documentFilenet, pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY))); // ID AOO
			nuovoDoc.setOggetto(documentDetail.getOggetto()); // Oggetto
			final Integer registroRiservato = (Integer) TrasformerCE.getMetadato(documentFilenet, pp.getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY));
			nuovoDoc.setRegistroRiservato(registroRiservato != null && registroRiservato == 1); // Registro Riservato
			nuovoDoc.setNote((String) TrasformerCE.getMetadato(documentFilenet, pp.getParameterByKey(PropertiesNameEnum.NOTE_METAKEY))); // Note
			nuovoDoc.setRiservato(Boolean.TRUE.equals(documentDetail.getFlagRiservato()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue()); // Riservato
			nuovoDoc.setNomeFile((String) TrasformerCE.getMetadato(documentFilenet, pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY))); // Nome File
			nuovoDoc.setContent(documentDetail.getContent()); // Content FIRMATO
			nuovoDoc.setMimeType(documentFilenet.get_MimeType()); // MIME Type
			nuovoDoc.setGuid(documentDetail.getGuid()); // GUID

			if (protocollo != null) {
				nuovoDoc.setProtocolloMittente(String.valueOf(protocollo.getNumeroProtocollo())); // Numero Protocollo Mittente
				nuovoDoc.setAnnoProtocolloMittente(protocollo.getAnnoProtocolloInt()); // Anno Protocollo Mittente
			}

			nuovoDoc.setNumDocDestIntUscita(documentDetail.getNumeroDocumento()); // Num Doc Dest Int Uscita

			// Si clonano gli allegati del documento originale e li si impostano nel nuovo
			// documento per assegnazione interna
			final List<AllegatoDTO> allegatiNuovoDoc = getAllegatiDocumentoAssegnazioneInterna(documentDetail.getGuid(), fceh);
			nuovoDoc.setAllegati(allegatiNuovoDoc);
			// ### Impostazione degli attributi/metadati nel nuovo documento per
			// assegnazione interna da creare -> END

			final Collection<?> destinatari = (Collection<?>) TrasformerCE.getMetadato(documentFilenet, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
			// Si normalizza il metadato per evitare errori comuni durante lo split delle
			// singole stringhe dei destinatari
			final List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(destinatari);

			if (!CollectionUtils.isEmpty(destinatariDocumento)) {
				// Si cicla sui destinatari
				for (final String[] destinatarioSplit : destinatariDocumento) {

					final String tipoDestinatario = destinatarioSplit[3];
					// Si crea un nuovo documento per assegnazione interna per ciascun destinatario
					// interno
					if (TipologiaDestinatarioEnum.INTERNO.getTipologia().equals(tipoDestinatario)) {
						final Long idUfficio = Long.parseLong(destinatarioSplit[0]);
						final Long idUtente = Long.parseLong(destinatarioSplit[1]);

						// ### Impostazione dell'assegnazione per competenza nel nuovo documento per
						// assegnazione interna
						final UtenteDTO utenteDestinatario = utenteSRV.getById(idUtente, con);
						final Nodo nodoDestinatario = nodoDAO.getNodo(idUfficio, con);
						final UfficioDTO ufficioDestinatario = new UfficioDTO(idUfficio, nodoDestinatario.getDescrizione());

						final List<AssegnazioneDTO> assegnazioniNuovoDoc = new ArrayList<>();
						final Date now = new Date();
						final AssegnazioneDTO assegnazioneCompetenza = new AssegnazioneDTO(null, TipoAssegnazioneEnum.COMPETENZA, DateUtils.dateToString(now, null), now, null,
								utenteDestinatario, ufficioDestinatario);
						assegnazioniNuovoDoc.add(assegnazioneCompetenza);
						nuovoDoc.setAssegnazioni(assegnazioniNuovoDoc);

						// ### Impostazione dei parametri per il salvataggio del documento -> START
						final SalvaDocumentoRedParametriDTO parametri = new SalvaDocumentoRedParametriDTO();

						parametri.setIdUfficioMittenteWorkflow(utenteFirmatario.getIdUfficio());
						parametri.setIdUtenteMittenteWorkflow(0L);
						parametri.setIdUfficioDestinatarioWorkflow(idUfficio);
						parametri.setIdUtenteDestinatarioWorkflow(idUtente);
						parametri.setIsAssegnazioneInternaWorkflow(TipoAssegnazioneEnum.DESTINATARIO_INTERNO);
						parametri.setIdDocumentoOldWorkflow(documentDetail.getDocumentTitle());
						parametri.setMotivazioneAssegnazioneWorkflow("ID Documento mittente " + documentDetail.getNumeroDocumento());
						// ### Impostazione dei parametri per il salvataggio del documento -> END

						LOGGER.info("gestisciDestinatariInterni -> Creazione di un nuovo documento per assegnazione interna dal documento firmato: " + idDocumento);
						// ### Chiamata al processo automatico di creazione e memorizzazione di un nuovo
						// documento in ingresso
						salvaDocumentoSRV.salvaDocumento(nuovoDoc, parametri, utenteFirmatario, ProvenienzaSalvaDocumentoEnum.PROCESSO_AUTOMATICO_INTERNO);
					}

				}
			}
		}
		LOGGER.info("gestisciDestinatariInterni -> END. Documento: " + idDocumento);
	}

	private List<AllegatoDTO> getAllegatiDocumentoAssegnazioneInterna(final String guidDocFirmato, final IFilenetCEHelper fceh) {
		final List<AllegatoDTO> allegati = new ArrayList<>();

		final DocumentSet allegatiFilenet = fceh.getAllegatiConContent(guidDocFirmato);
		final Iterator<?> itAllegatiFilenet = allegatiFilenet.iterator();
		while (itAllegatiFilenet.hasNext()) {
			final Document allegatoFilenet = (Document) itAllegatiFilenet.next();

			final AllegatoDTO allegato = new AllegatoDTO();
			allegato.setNomeFile((String) TrasformerCE.getMetadato(allegatoFilenet, pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY)));
			allegato.setFormato((Integer) TrasformerCE.getMetadato(allegatoFilenet, pp.getParameterByKey(PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY)));
			allegato.setIdTipologiaDocumento((Integer) TrasformerCE.getMetadato(allegatoFilenet, pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY)));
			allegato.setOggetto((String) TrasformerCE.getMetadato(allegatoFilenet, pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY)));
			allegato.setContent(FilenetCEHelper.getDocumentContentAsByte(allegatoFilenet));
			allegato.setMimeType(allegatoFilenet.get_MimeType());
			allegato.setDaFirmare(0);

			allegati.add(allegato);
		}

		return allegati;
	}

	/**
	 * Item verifica firma.
	 */
	public static class VerificaFirmaItem {

		/**
		 * Valore successivo.
		 */
		private Integer nextVal;

		/**
		 * Classe documentale.
		 */
		private String classeDocumentale;

		/**
		 * Documet title.
		 */
		private String documentTitle;

		/**
		 * Versione documento.
		 */
		private Integer versioneDocumento;

		/**
		 * Costruttore item verifica firma.
		 * 
		 * @param classeDocumentale
		 * @param documentTitle
		 */
		public VerificaFirmaItem(final String classeDocumentale, final String documentTitle) {
			super();
			this.classeDocumentale = classeDocumentale;
			this.documentTitle = documentTitle;
		}

		/**
		 * Costruttore item verifica firma.
		 * 
		 * @param nextVal
		 * @param classeDocumentale
		 * @param documentTitle
		 * @param versioneDocumento
		 */
		public VerificaFirmaItem(final Integer nextVal, final String classeDocumentale, final String documentTitle, final Integer versioneDocumento) {
			super();
			this.nextVal = nextVal;
			this.classeDocumentale = classeDocumentale;
			this.documentTitle = documentTitle;
			this.versioneDocumento = versioneDocumento;
		}

		/**
		 * Restituisce la classe documentale.
		 * 
		 * @return classe documentale
		 */
		public String getClasseDocumentale() {
			return classeDocumentale;
		}

		/**
		 * Imposta la classe documentale.
		 * 
		 * @param classeDocumentale
		 */
		public void setClasseDocumentale(final String classeDocumentale) {
			this.classeDocumentale = classeDocumentale;
		}

		/**
		 * Restituisce il documentTitle.
		 * 
		 * @return documentTitle
		 */
		public String getDocumentTitle() {
			return documentTitle;
		}

		/**
		 * Imposta il documentTitle.
		 * 
		 * @param documentTitle
		 */
		public void setDocumentTitle(final String documentTitle) {
			this.documentTitle = documentTitle;
		}

		/**
		 * Restituisce la versione del documento.
		 * 
		 * @return versione documento
		 */
		public Integer getVersioneDocumento() {
			return versioneDocumento;
		}

		/**
		 * Imposta la versione del documento.
		 * 
		 * @param versioneDocumento
		 */
		public void setVersioneDocumento(final Integer versioneDocumento) {
			this.versioneDocumento = versioneDocumento;
		}

		/**
		 * Restituisce il nextVal.
		 * 
		 * @return nextVal
		 */
		public Integer getNextVal() {
			return nextVal;
		}

		/**
		 * Imposta il nextVal.
		 * 
		 * @param nextVal
		 */
		public void setNextVal(final Integer nextVal) {
			this.nextVal = nextVal;
		}

	}

	/**
	 * Notifiche rubrica, recupero di secondi di polling dalle properties .
	 * 
	 * @return
	 */
	private int getVerificaFirmaMaxItems() {
		int n = 1;
		
		try {
			n = Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.VERIFICA_FIRMA_MAX_ITEM));
		} catch (final Exception e) {
			LOGGER.warn(e.getMessage(), e);
			n = 1;
		}
		
		return n;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#recuperaContentDaVerificare(int).
	 */
	@Override
	public List<VerificaFirmaItem> recuperaContentDaVerificare(final int idAoo) {
		Connection connection = null;
		
		try {
			connection = setupConnection(getFilenetDataSource().getConnection(), false);

			return verificaDAO.getContentsToVerify(connection, idAoo, getVerificaFirmaMaxItems());

		} catch (final SQLException e) {
			LOGGER.error("Errore in fase di recupero dei documenti da validare " + e.getMessage(), e);
			throw new RedException("Errore in fase di recupero dei documenti da validare " + e.getMessage(), e);
		} catch (final Exception e) {
			LOGGER.error(GENERIC_ERROR_RECUPERO_CONTENT_DA_VERIFICARE_MSG, e);
			throw new RedException(GENERIC_ERROR_RECUPERO_CONTENT_DA_VERIFICARE_MSG, e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#aggiornaVerificaFirma(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public final VerifyInfoDTO aggiornaVerificaFirma(final String guid, final UtenteDTO utente) {
		VerifyInfoDTO verifyInfo = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final Document doc = fceh.getDocumentByGuid(guid);
			final InputStream docContent = FilenetCEHelper.getDocumentContentAsInputStream(doc);

			verifyInfo = aggiornaVerificaFirma(doc, docContent, fceh, utente);
		} finally {
			popSubject(fceh);
		}

		return verifyInfo;
	}

	/**
	 * @see it.ibm.red.business.service.ISignSRV#aggiornaVerificaFirma(com.filenet.api.core.Document,
	 *      java.io.InputStream,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public final VerifyInfoDTO aggiornaVerificaFirma(final Document doc, final InputStream docContent, final IFilenetCEHelper fceh, final UtenteDTO utente) {
		VerifyInfoDTO verifyInfo = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			verifyInfo = obtainVerifyInfoFirma(docContent, doc.get_MimeType(), utente);
			final Map<String, Object> mapToUpdate = new HashMap<>();

			// Si crea il metadato da aggiornare su filenet
			final Integer metadatoValue = verifyInfo.getMetadatoValiditaFirmaValue();
			if (metadatoValue != null) {
				final String guid = doc.get_Id().toString();
				mapToUpdate.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA), metadatoValue);

				// Si aggiorna su FileNet il valore del metadato ValiditaFirma associato al
				// documento
				fceh.updateMetadati(doc, mapToUpdate);

				LOGGER.info("Documento con GUID: " + guid + " aggiornato con validitaFirma: " + metadatoValue);

				// Si aggiorna il valore di validita firma sul database
				// Vale solo per il contributo esterno, ma si fa sempre in quanto si entra su
				// FileNet per GUID univoco
				contributoDAO.updateValiditaFirmaByGuid(guid, metadatoValue, connection);
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore in fase di aggiornamento della validita firma.", e);
		} finally {
			closeConnection(connection);
		}

		return verifyInfo;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#obtainVerifyInfoFirma(java.io.InputStream,
	 *      java.lang.String, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public VerifyInfoDTO obtainVerifyInfoFirma(final InputStream docContent, String mimeType, final UtenteDTO utente) {
		VerifyInfoDTO verifyInfo;
		final PkHandler pkHandler = utente.getSignerInfo().getPkHandlerVerifica();
		// START VI PK HANDLER
		final SignHelper lsh = new SignHelper(pkHandler.getHandler(), pkHandler.getSecurePin(), utente.getDisableUseHostOnly());

		verifyInfo = lsh.infoFirmaDocumento(docContent, Constants.ContentType.TEXT_XML.equals(mimeType));
		return verifyInfo;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#checkApprovazioniDaNonStampigliare(java.util.List,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public final String checkApprovazioniDaNonStampigliare(final List<MasterDocumentRedDTO> documentiSelezionati, final UtenteDTO utente) {
		String confirmMessage = null;
		final StringBuilder docsReturn = new StringBuilder("");
		Connection connection = null;
		IFilenetCEHelper fceh = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final PkHandler pkHandler = utente.getSignerInfo().getPkHandlerVerifica();
			final SignHelper sh = new SignHelper(pkHandler.getHandler(), pkHandler.getSecurePin(), utente.getDisableUseHostOnly());

			final Integer idAoo = utente.getIdAoo().intValue();
			Integer idDoc = null;
			Document contentDocument = null;
			InputStream isDoc = null;
			Integer idTipoApprovazione = null;
			Integer stampigliatura = null;
			Integer numeroDocumento = null;
			String documentTitle = null;
			for (final MasterDocumentRedDTO documento : documentiSelezionati) {

				documentTitle = documento.getDocumentTitle();
				contentDocument = fceh.getDocumentForDownload(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle);
				isDoc = FilenetCEHelper.getDocumentContentAsInputStream(contentDocument);
				numeroDocumento = documento.getNumeroDocumento();

				if (!sh.hasSignatures(isDoc, false)) {
					// il controllo deve essere effettuato solo se il documento non è stato ancora
					// firmato

					idDoc = Integer.parseInt(documentTitle);
					final Collection<ApprovazioneDTO> approvazioni = approvazioneDAO.getApprovazioniByIdDocumento(idDoc, idAoo, connection);
					ApprovazioneDTO approvazione = null;
					final Iterator<ApprovazioneDTO> it = approvazioni.iterator();
					while (it.hasNext()) {

						approvazione = it.next();
						idTipoApprovazione = approvazione.getIdTipoApprovazione();
						if (idTipoApprovazione == TipoApprovazioneEnum.SIGLA.getId().intValue() || idTipoApprovazione == TipoApprovazioneEnum.VISTO.getId().intValue()
								|| idTipoApprovazione == TipoApprovazioneEnum.VISTO_POSITIVO.getId().intValue()
								|| idTipoApprovazione == TipoApprovazioneEnum.VISTO_CONDIZIONATO.getId().intValue()
								|| idTipoApprovazione == TipoApprovazioneEnum.VISTO_NEGATIVO.getId().intValue()) {
							stampigliatura = approvazione.getStampigliaturaFirma();

							if (stampigliatura <= 0) {
								if ("".equals(docsReturn.toString())) {
									docsReturn.append(numeroDocumento);
								} else {
									docsReturn.append(", ").append(numeroDocumento);
								}
								break; // passa al prossimo documento
							}
						}
					}
				}

			}

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero delle approvazioni", e);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}

		if (!StringUtils.isNullOrEmpty(docsReturn.toString())) {
			confirmMessage = "Esistono approvazioni per sigla o visto sui seguenti documenti: " + docsReturn
					+ ". Selezionare REGISTRA per procedere alla firma, CHIUDI qualora si voglia procedere alla stampigliatura accedendo all'apposita scheda approvazioni.";
		}

		return confirmMessage;
	}

	@Override
	public final List<AllegatoDTO> getAllegatiToSignByDocPrincipale(final String documentTitle, final String guid, final UtenteDTO utente, final SignTypeEnum ste) {
		List<AllegatoDTO> allegatiDTO = new ArrayList<>();
		Connection connection = null;
		IFilenetCEHelper fceh = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(),utente.getIdAoo());
		
			final DocumentSet allegati = fceh.getAllegatiConContentDaFirmare(StringUtils.cleanGuidToString(guid));

			// Controllo che l'utente possa firmare gli allegati
			final EsitoOperazioneDTO checkAttach = checkAuthAttachmentSign(documentTitle, allegati, ste);
			if (checkAttach.isEsito()) {
				if (allegati != null) {
					final Iterator<?> itAllegati = allegati.iterator();
					while (itAllegati.hasNext()) {
						final Document allegato = (Document) itAllegati.next();
						final AllegatoDTO allegatoDTO = new AllegatoDTO();
						allegatoDTO.setDocumentTitle(TrasformerCE.getMetadato(allegato, pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)).toString());
						allegatoDTO.setNomeFile(TrasformerCE.getMetadato(allegato, pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY)).toString());
						allegatoDTO.setGuid(allegato.get_Id().toString());
						allegatiDTO.add(allegatoDTO);
					}
				}
			} else {
				LOGGER.error("L'utente " + utente.getUsername() + " non è autorizzato a firmare gli allegati");
				throw new RedException(checkAttach.getNote());
			}
		} catch (final RedException e) {
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero degli allegati da firmare per il documento: " + documentTitle, e);
			throw new RedException("Errore durante il recupero degli allegati da firmare");
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}

		return allegatiDTO;
	}

	/**
	 * Recupera il content del documento principale, staccando il protocollo e
	 * stampigliandolo sul PDF da firmare.
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param documentTitle
	 * @param ste
	 * @return
	 */
	@Override
	public LocalSignContentDTO getDocumentoPrincipale4FirmaLocale(final UtenteDTO utente, final String wobNumber, final String documentTitle, final SignTypeEnum ste, final SottoCategoriaDocumentoUscitaEnum sottoCategoriaDocumentoUscita) {
		
		final LocalSignContentDTO file = new LocalSignContentDTO();
		Connection connection = null;
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			byte[] content = null;
			byte[] digest = null;
			LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": " + ste.toString() + " - locale");

			// Controllo che l'utente possa firmare il documento
			final EsitoOperazioneDTO isSignAuthorized = checkSignAuthorization(wobNumber, utente, connection, fpeh, documentTitle);
			if (!isSignAuthorized.isEsito()) {
				LOGGER.error("L'utente " + utente.getUsername() + " non è autorizzato a firmare il documento principale");
				throw new RedException(isSignAuthorized.getNote());
			}
			AnnotationDTO annotationContentLibroFirma = null;
			// Se il content è associato a una registrazione ausiliaria l'annotation CONTENT_LIBRO_FIRMA deve essere ignorata nel recupero del content da firmare,
						// poiché contiene i placeholder della registrazione ausiliaria e quindi non può essere firmata.
			if (sottoCategoriaDocumentoUscita == null || !sottoCategoriaDocumentoUscita.isRegistrazioneAusiliaria()) {
				annotationContentLibroFirma = fceh.getAnnotationDocumentContent(documentTitle, false, FilenetAnnotationEnum.CONTENT_LIBRO_FIRMA, utente.getIdAoo());
			}
			
			if (annotationContentLibroFirma != null) { // Si firma l'annotation
				content = IOUtils.toByteArray(annotationContentLibroFirma.getContent());
			}else {
				content = recuperaContentPrincipale(documentTitle, fceh);
			}
			
			Date dataFirma = new Date();
			
			if (!SignTypeEnum.CADES.equals(ste)) {
				SignHelper sh = new SignHelper(utente.getSignerInfo().getPkHandlerFirma().getHandler(), utente.getSignerInfo().getPkHandlerFirma().getSecurePin(), 
						utente.getDisableUseHostOnly());
				digest = sh.createPdfDigest(content, null, utente.getSignerInfo().getImageFirma(), utente.getUsername(), 0, dataFirma, null);
			}

			file.setContent(content);
			file.setDigest(digest);
			file.setDataFirma(dataFirma);
		} catch (RedException e) {
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Errore durante la procedura di recupero del documento principale con WOB number: " + wobNumber, e);
			throw new RedException("Errore durante la procedura di recupero del documento principale", e);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}

		return file;
	}

	/**
	 * @param documentTitle
	 * @param fceh
	 * @param content
	 * @return byte array content del documento principale
	 */
	private byte[] recuperaContentPrincipale(final String documentTitle, IFilenetCEHelper fceh) {
		Document documentForDetails = null;
		// ### Recupero content del documento principale
		documentForDetails = fceh.getDocumentForDownload(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle, false);
		byte[] content;
		
		try {
			 content = FilenetCEHelper.getDocumentContentAsByte(documentForDetails);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_CONTENT_MSG + documentTitle, e);
			throw new RedException("Errore durante il recupero del content del documento principale");
		}
		return content;
	}

	/**
	 * Recupera il content dell'allegato preparandolo per la firma locale.
	 * 
	 * @param utente
	 * @param documentTitle
	 * @param ste
	 * @return
	 */
	@Override
	public LocalSignContentDTO getAllegato4FirmaLocale(final UtenteDTO utente, final String documentTitle, final SignTypeEnum ste, final SottoCategoriaDocumentoUscitaEnum sottoCategoriaDocumentoUscita ) {
		final LocalSignContentDTO file = new LocalSignContentDTO();
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			AttachmentDTO allegato = documentoSRV.getContentAllegato(documentTitle, fceh, utente.getIdAoo());
			
			AnnotationDTO annotationContentLibroFirma = null;
			byte[] content = null;
			if (sottoCategoriaDocumentoUscita == null || !sottoCategoriaDocumentoUscita.isRegistrazioneAusiliaria()) {
				annotationContentLibroFirma = fceh.getAnnotationDocumentContent(documentTitle, true, FilenetAnnotationEnum.CONTENT_LIBRO_FIRMA, utente.getIdAoo());
			}
			
			if (annotationContentLibroFirma != null) { // Si firma l'annotation
				content = IOUtils.toByteArray(annotationContentLibroFirma.getContent());
			}else {
				content = allegato.getContent();
			}
			byte[] digest = null;
			final Date dataFirma = new Date();

			if (!SignTypeEnum.CADES.equals(ste)) {
				byte[] glifo = null;
				
				if (!FormatoAllegatoEnum.FIRMATO_DIGITALMENTE.getId().equals(allegato.getFormatoAllegato()) &&
						FileUtils.hasEmptySignFields(new ByteArrayInputStream(allegato.getContent()), new AdobeLCHelper())) {
					glifo = utente.getSignerInfo().getImageFirma();
				}

				SignHelper sh = new SignHelper(utente.getSignerInfo().getPkHandlerFirma().getHandler(), utente.getSignerInfo().getPkHandlerFirma().getSecurePin(), 
						utente.getDisableUseHostOnly());
				digest = sh.createPdfDigest(content, null, glifo, utente.getUsername(), 0, dataFirma, null);
			}

			file.setContent(content);
			file.setDigest(digest);
			file.setDataFirma(dataFirma);
		} catch (Exception e) {
			LOGGER.error("Errore durante la procedura di recupero dell'allegato: " + documentTitle, e);
			throw new RedException("Errore durante la procedura di recupero di un allegato", e);
		}finally {
			popSubject(fceh);
		}

		return file;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#manageSignedContent(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.enums.SignTypeEnum, byte[], byte[], java.util.Date,
	 *      java.lang.String, boolean, boolean).
	 */
	@Override
	public byte[] manageSignedContent(final UtenteDTO utente, final SignTypeEnum ste, final byte[] content, final byte[] docSigned, final Date dataFirma, final String documentTitle, 
			final boolean principale, final boolean copiaConforme) {
		byte[] contentFirmato = null;
		IFilenetCEHelper fceh = null;

		try (
			ByteArrayInputStream bis = new ByteArrayInputStream(content);
		){
			if (SignTypeEnum.CADES.equals(ste)) {
				// Firma CAdES
				contentFirmato = docSigned;
			} else {
				// Firma PAdES
				SignHelper sh = new SignHelper(utente.getSignerInfo().getPkHandlerFirma().getHandler(), utente.getSignerInfo().getPkHandlerFirma().getSecurePin(), utente.getDisableUseHostOnly());
				byte[] glifo = null;
				String reason = null;
				
				if (!copiaConforme) {
					boolean hasEmptySignFields = FileUtils.hasEmptySignFields(bis, new AdobeLCHelper());
					
					boolean allegatoFirmatoDigitalmente = false;
					if (!principale) {
						fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
						Document allegato = fceh.getAllegatoForDownload(pp.getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY), documentTitle);
						
						allegatoFirmatoDigitalmente = FormatoAllegatoEnum.FIRMATO_DIGITALMENTE.getId()
								.equals(TrasformerCE.getMetadato(allegato, pp.getParameterByKey(PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY)));
					}
					
					// Firma INVISIBILE (senza glifo) se:
					// - Il documento (principale o allegato) non ha campi firma vuoti
					// - Si tratta di un allegato firmato digitalmente.
					if (hasEmptySignFields && !allegatoFirmatoDigitalmente) {
						glifo = utente.getSignerInfo().getImageFirma();
					}
				} else {
					reason = getPostillaCopiaConforme(utente);
				}

				// ### Merge di content stampigliato + content firmato + eventuale glifo di firma
				contentFirmato = sh.pdfmerge(content, docSigned, glifo, utente.getUsername(), null, dataFirma, reason);
			}
			
			if (contentFirmato == null || contentFirmato.length == 0) {
				throw new RedException("Errore in fase di recupero del content firmato");
			}
		} catch (Exception e) {
			LOGGER.error("Errore durante la procedura di aggiornamento del documento: " + documentTitle, e);
			throw new RedException("Errore durante la procedura di aggiornamento del documento");
		} finally {
			popSubject(fceh);
		}

		return contentFirmato;
	}
	
	private String saveSignedContent(final boolean isPrincipale, final SignTypeEnum ste, final byte[] contentFirmato, final String documentTitle, Document documentForDetails,
			Document allegato, final DetailDocumentoDTO documentDetailDTO, final UtenteDTO utente, final IFilenetCEHelper fceh, final boolean sendToNPS,
			final List<String> guidAttachSentToNPS, final boolean isProtocollazioneAnticipata) {
		Connection connection = null;
		String errorMsg = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			// aggiorna versione su CE Filenet
			if (isPrincipale) {
				documentDetailDTO.setContent(contentFirmato);
				documentForDetails = updateSignedDocument(ste, documentDetailDTO, utente, documentTitle, documentDetailDTO.getProtocollo(), 
						true, documentForDetails, connection, fceh, sendToNPS, isProtocollazioneAnticipata);
				
				//invia ad NPS allegati non da firmare
				if (sendToNPS) {
					sendAttachmentToNPS(documentForDetails, utente, documentDetailDTO.getProtocollo(), true, fceh, connection, isProtocollazioneAnticipata);
				}

			} else {
				allegato = updateSignedAttachment(allegato, contentFirmato, ste, utente, documentDetailDTO.getProtocollo(), fceh, connection, sendToNPS, isProtocollazioneAnticipata);
				if (guidAttachSentToNPS != null) {
					guidAttachSentToNPS.add(StringUtils.cleanGuidToString(allegato.get_Id()));
				}
			}

			commitConnection(connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante la procedura di aggiornamento del documento " + documentTitle, e);
			rollbackConnection(connection);
			errorMsg = "Errore durante la procedura di aggiornamento del documento";
		} finally {
			closeConnection(connection);
		}

		return errorMsg;
	}

	/**
	 * Completa il processo di firma nel momento in cui sono stati firmati
	 * correttamente documento principale e tutti gli allegati ad esso associati.
	 * 
	 * @param utente
	 * @param ste
	 * @param wobNumber
	 * @return
	 */
	@Override
	public final EsitoOperazioneDTO completeLocalSignProcess(final UtenteDTO utente, final SignTypeEnum ste, final String wobNumber, final boolean firmaMultipla,
			final List<String> idDocumentiNPS, final List<String> guidAttachSentToNPS) {
		final EsitoOperazioneDTO esitoOperazioneFirma = new EsitoOperazioneDTO(wobNumber);
		esitoOperazioneFirma.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
		esitoOperazioneFirma.setEsito(false);
		Integer numeroDocumento = null;
		String documentTitle = null;
		Document documentForDetails = null;
		PEDocumentoDTO wobDTO = null;
		Connection connection = null;
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;
		
		String logId = "completeLocalSign" + UUID.randomUUID().toString();

		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			// ### Recupero le info dal documento principale e dai suoi allegati dal ce
			// ##########################
			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);
			wobDTO = TrasformPE.transform(wob, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO);

			documentTitle = wobDTO.getIdDocumento();

			// ### Recupero dettagli del documento principale
			// #####################################################
			documentForDetails = fceh.getDocumentRedForDetail(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle);
			final DetailDocumentoDTO documentDetailDTO = TrasformCE.transform(documentForDetails, TrasformerCEEnum.FROM_DOCUMENTO_TO_DETAIL);
			documentDetailDTO.setContent(FilenetCEHelper.getDocumentContentAsByte(documentForDetails));

			numeroDocumento = documentDetailDTO.getNumeroDocumento();
			esitoOperazioneFirma.setIdDocumento(numeroDocumento);

			final RispostaAllaccioDTO allaccioPrincipale = allaccioDAO.getAllaccioPrincipale(Integer.parseInt(documentDetailDTO.getDocumentTitle()), connection);
			boolean isFlussoAUT = false;
			String codiceFlusso = null;
			if (allaccioPrincipale != null) {
				final IFilenetCEHelper fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()),utente.getIdAoo());
				final List<String> props = Arrays.asList(pp.getParameterByKey(PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY));
				final Document document = fcehAdmin.getDocumentByIdGestionale(allaccioPrincipale.getIdDocumentoAllacciato(), props, null, utente.getIdAoo().intValue(), null, null);
				codiceFlusso = TrasformerCE.getMetadato(document, 
						PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY) != null ? TrasformerCE.getMetadato(document, PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY).toString() : "";
			
				LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": " + CODICE_FLUSSO_LABEL + codiceFlusso + ALLACCIO_PRINCIPALE_LABEL
						+ allaccioPrincipale.getIdDocumentoAllacciato());
				
				isFlussoAUT = isFamigliaFlussoAUT(codiceFlusso);
				popSubject(fcehAdmin);
			}

			final boolean isProtocollazioneAnticipata = MomentoProtocollazioneEnum.PROTOCOLLAZIONE_NON_STANDARD.getId() == documentDetailDTO.getIdMomentoProtocollazione();
			final boolean isInoltroAttoDecretoUCB = checkIsFlussoAttoDecretoUCB(utente, documentDetailDTO.getIdTipologiaDocumento());
			if (isInoltroAttoDecretoUCB) {
				codiceFlusso = Constants.Varie.CODICE_FLUSSO_ATTO_DECRETO;
			}

			ProtocolloDTO protocolloUscita = null;

			if (isFlussoAUT || isInoltroAttoDecretoUCB) {

				// upload allegati da non firmare a NPS (quelli non ancora inviati, presenti in
				// guidAttachSentToNPS)
				uploadNotSentDocSyncNPS(logId, utente.getIdAoo().intValue(), documentForDetails.get_Id().toString(), documentTitle, SignTypeEnum.CADES.equals(ste), fceh,
						idDocumentiNPS, guidAttachSentToNPS);

				// inviaAtto
				List<String> idAllegatiNPS = null;
				if (idDocumentiNPS.size() > 1) {
					idAllegatiNPS = idDocumentiNPS.subList(1, idDocumentiNPS.size());
				} else {
					idAllegatiNPS = new ArrayList<>();
				}

				final String acl = getACL4NPS(wob, documentDetailDTO, utente, fceh, documentDetailDTO.getFlagRiservato());

				LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": " + "acl " + acl);
				
				final ProtocolloNpsDTO datiProtocolloIdDoc = inviaAtto(logId, codiceFlusso, documentDetailDTO, acl, utente, fceh, connection, idDocumentiNPS.get(0), idAllegatiNPS);
				
				//salva dati protocollo
				protocolloUscita = salvaDatiProtocollo(logId, datiProtocolloIdDoc, documentDetailDTO, documentForDetails, esitoOperazioneFirma, utente, fceh, connection);
								
				//recupera documento stampigliato da NPS
				if(!ste.equals(SignTypeEnum.CADES)) {
					final DocumentoNpsDTO downloadDocumento = npsSRV.downloadDocumento(utente.getIdAoo().intValue(), datiProtocolloIdDoc.getIdDocumento(), false);
					documentDetailDTO.setContent(IOUtils.toByteArray(downloadDocumento.getInputStream()));
					
					LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentDetailDTO.getDocumentTitle() + ": [Flusso] recuperato content stampigliato da NPS " + datiProtocolloIdDoc.getIdDocumento());
					
					//aggiorna content su CE documento principale recuperato da nps
					final String errorMsg = saveSignedContent(true, ste, documentDetailDTO.getContent(), documentDetailDTO.getDocumentTitle(), 
							documentForDetails, null, documentDetailDTO, utente, fceh, false, null, isProtocollazioneAnticipata);
					
					if (!StringUtils.isNullOrEmpty(errorMsg)) {
						throw new RedException(errorMsg);
					}
				}

			} else {
				protocolloUscita = documentDetailDTO.getProtocollo();
				esitoOperazioneFirma.setAnnoProtocollo(protocolloUscita.getAnnoProtocolloInt());
				esitoOperazioneFirma.setNumeroProtocollo(protocolloUscita.getNumeroProtocollo());
				esitoOperazioneFirma.setNomeDocumento(documentDetailDTO.getNomeFile());
			}
			
			//chiudi il fascicolo se è una mozione oppure se l'allaccio principale non è una richiesta integrazioni
			final boolean chiudiFascicolo = allaccioPrincipale == null || !TipoAllaccioEnum.RICHIESTA_INTEGRAZIONE.equals(allaccioPrincipale.getTipoAllaccioEnum());

			if (!firmaMultipla) {

				// se ho staccato la registrazione ausiliaria, collega il protocollo in uscita
				// alla registrazione su NPS (asincrono)
				if (TipologiaProtocollazioneEnum.isProtocolloNPS(utente.getIdTipoProtocollo()) && documentDetailDTO.getRegistrazioneAusiliaria() != null) {

					associaRegAuxProtUscitaNPS(documentDetailDTO.getRegistrazioneAusiliaria(), protocolloUscita.getIdProtocollo(), utente,
							protocolloUscita.getNumeroAnnoProtocolloNPS(utente.getCodiceAoo()), documentDetailDTO.getDocumentTitle(), connection);

				}

				// Aggiorna copia conforme
				aggiornaVersioneCopiaConforme(documentForDetails, fceh, wob);

				// Aggiorna le security
				aggiornaSecurity(utente, wob, documentTitle, documentDetailDTO.getFlagRiservato());

				// Aggiorna fascicolo Bilancio Enti
				aggiornaFascicoloBilancioEnti(documentDetailDTO, protocolloUscita, utente.getIdAoo().intValue(), fceh);

				// Gestione allacci
				final Aoo aoo = aooDAO.getAoo(utente.getIdAoo(), connection);
				gestisciAllacci(fceh, fpeh, aoo, documentForDetails, utente, true, connection);

				// Avanza il WF
				avanzaWFFirma(utente, fpeh, wob, documentTitle, getTipoSpedizione(wobDTO, documentDetailDTO), documentDetailDTO.getFlagRiservato(), fceh, connection,
						wobDTO.getIdFascicolo(), !documentDetailDTO.isDestinatariElettronici(), firmaMultipla, false, chiudiFascicolo);

				// Se è un flusso AUT si chiude l'eventuale notifica associata al documento in uscita
				if (isFlussoAUT && Boolean.TRUE.equals(allaccioPrincipale.getDocumentoDaChiudere())) {
					mettiAgliAttiNotifica(protocolloUscita, allaccioPrincipale, aoo, utente, fpeh, fceh, connection);
				}
				
				// ### INVIO MAIL DI NOTIFICA
				// ########################################################################
				final Collection<?> destinatariCollection = (Collection<?>) documentForDetails.getProperties()
						.get(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY)).getObjectValue();
				
				final EsitoOperazioneDTO esitoInvioMail = invioMail(utente, wobDTO, documentDetailDTO, protocolloUscita, destinatariCollection, connection, true,
						(isFlussoAUT || isInoltroAttoDecretoUCB), chiudiFascicolo);
				if (!esitoInvioMail.isEsito()) {
					throw new SignException(esitoInvioMail.getCodiceErrore(), esitoInvioMail.getNote());
				}

				gestisciDestinatariInterni(documentForDetails, documentDetailDTO, protocolloUscita, utente, fceh, connection);
				// Se si tratta di Firma Multipla, ci si limita ad avanzare il workflow
			} else {
				avanzaWFFirma(utente, fpeh, wob, documentTitle, getTipoSpedizione(wobDTO, documentDetailDTO), documentDetailDTO.getFlagRiservato(), fceh, connection,
						wobDTO.getIdFascicolo(), !documentDetailDTO.isDestinatariElettronici(), firmaMultipla, false, chiudiFascicolo);
			}

			commitConnection(connection);

			impostaEsitoOk(esitoOperazioneFirma);

		} catch (final ProtocolloGialloException e) {
			LOGGER.error(ERROR_COMPLETAMENTO_PROCESSO_FIRMA_MSG + wobNumber, e);
			esitoOperazioneFirma.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
			esitoOperazioneFirma.setNote(e.getMessage());
			rollbackConnection(connection);
			throw new ProtocolloGialloException(e);
		} catch (final SignException e) {
			LOGGER.error(ERROR_COMPLETAMENTO_PROCESSO_FIRMA_MSG + wobNumber, e);
			esitoOperazioneFirma.setCodiceErrore(e.getCodiceErrore());
			esitoOperazioneFirma.setNote(e.getNote());
			rollbackConnection(connection);
		} catch (final Exception e) {
			LOGGER.error(ERROR_COMPLETAMENTO_PROCESSO_FIRMA_MSG + wobNumber, e);
			esitoOperazioneFirma.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
			esitoOperazioneFirma.setNote(e.getMessage());
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}

		return esitoOperazioneFirma;
	}

	/**
	 * Chiude il worklfow associato alla notifica eventualemente allacciata all'allaccio principale del documento, comunicandone il nuovo stato ad NPS.
	 * @param protocolloUscita
	 * @param allaccioPrincipale allaccio principale del documento
	 * @param aoo aoo dell'utente in sessione
	 * @param utente utente in sessione
	 * @param fpeh helper Filenet
	 * @param fceh helper per il content engine di Filenet
	 * @param connection connessione al db
	 */
	private void mettiAgliAttiNotifica(final ProtocolloDTO protocolloUscita, final RispostaAllaccioDTO allaccioPrincipale, final Aoo aoo,
			final UtenteDTO utente, final FilenetPEHelper fpeh, final IFilenetCEHelper fceh, final Connection connection) {
		
		VWWorkObject workflowPrincipale = null;
		IFilenetCEHelper fcehAdmin = null;
		// Recupero Notifica
		RispostaAllaccioDTO allaccioNotifica = null;
		List<RispostaAllaccioDTO> allacci = allaccioDAO.getAllacciOfType(allaccioPrincipale.getIdDocumentoAllacciato(), TipoAllaccioEnum.NOTIFICA, connection);
		
		// L'allaccio notifica viene identificato come il primo tra gli allacci di tipo notifica
		if (!CollectionUtils.isEmpty(allacci)) {
			allaccioNotifica = allacci.get(0);
		}
		
		Integer numeroProtocolloNotifica = null;
		Integer annoProtocolloNotifica = null;
		String idProtocolloNotifica = null;
		
		if (allaccioNotifica != null) {
			try {
				fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());
				Document docNotifica = fcehAdmin.getDocumentByDTandAOO(allaccioNotifica.getIdDocumentoAllacciato(), utente.getIdAoo());
				
				if (TrasformerCE.getMetadato(docNotifica, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY) != null) {
					numeroProtocolloNotifica = (Integer) TrasformerCE.getMetadato(docNotifica, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
				}
				if (TrasformerCE.getMetadato(docNotifica, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY) != null) {
					annoProtocolloNotifica = (Integer) TrasformerCE.getMetadato(docNotifica, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
				}
				idProtocolloNotifica = (String) TrasformerCE.getMetadato(docNotifica, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);
			} catch (Exception e) {
				LOGGER.error("Errore riscontrato durante il recupero della notifica allacciata.");
				throw new RedException("Errore riscontrato durante il recupero della notifica allacciata.", e);
			} finally {
				if(fcehAdmin != null) {
					fcehAdmin.popSubject();
				}
			}

			workflowPrincipale = fpeh.getWorkflowPrincipale(allaccioNotifica.getIdDocumentoAllacciato(), aoo.getAooFilenet().getIdClientAoo());
		}
		if (workflowPrincipale != null) {
			
			String numeroAnnoProtocolloUscita = protocolloUscita.getNumeroProtocollo() + "/" + protocolloUscita.getAnnoProtocollo();
			String numeroAnnoNotifica = numeroProtocolloNotifica + "/" + annoProtocolloNotifica;
			final String motivazioneAnnullamento = new StringBuilder()
					.append("Il documento è stato annullato automaticamente essendo allacciato alla notifica con protocollo ")
					.append(numeroAnnoNotifica)
					.append(" chiuso con risposta").append(" a fronte del protocollo ").append(numeroAnnoProtocolloUscita).toString();
			
			workflowSRV.gestioneCodaInLavorazioneInSospeso(workflowPrincipale, Integer.parseInt(allaccioNotifica.getIdDocumentoAllacciato()), 
					connection, motivazioneAnnullamento, utente, fceh, pp);
			
			// Viene chiuso il workflow principale
			fpeh.chiudiWF(workflowPrincipale);
			LOGGER.info("Chiuso wf principale {" + workflowPrincipale.getWorkflowNumber() + "}");
			
			final String motivazioneChiusuraNotifica = "Il documento è stato chiuso automaticamente con risposta a fronte del protocollo " + numeroAnnoProtocolloUscita;
			eventLogSRV.writePredisposizioneRispostaAutomaticaEventLog(utente.getIdUfficio(), utente.getId(), Integer.parseInt(allaccioNotifica.getIdDocumentoAllacciato()), motivazioneChiusuraNotifica, utente.getIdAoo());
			
			// Vengono chiusi tutti gli altri workflow eventualmente esistenti
			final VWRosterQuery workflowAggiuntivi = fpeh.getRosterQueryWorkByIdDocumentoAndCodiceAOO(allaccioNotifica.getIdDocumentoAllacciato(), aoo.getAooFilenet().getIdClientAoo());
			while (workflowAggiuntivi.hasNext()) {
				final VWWorkObject workflow = (VWWorkObject) workflowAggiuntivi.next();
				if (workflow != null) {
					fpeh.chiudiWF(workflow);
					LOGGER.info("Chiuso wf {" + workflow.getWorkflowNumber() + "}");
				}
			}

			// Aggiornamento dello stato della notifica
			npsSRV.cambiaStatoProtEntrata(idProtocolloNotifica, numeroAnnoNotifica, utente, Builder.CHIUSO, allaccioNotifica.getIdDocumentoAllacciato(), connection);
		} else {
			if (allaccioNotifica != null) {
				LOGGER.info(allaccioNotifica.getIdDocumentoAllacciato() + " risulta già precedentemente chiuso");
			} else {
				LOGGER.info("Nessuna notifica associata al flusso AUT.");
			}
		}
		
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#aggiornaMetadatoTrasformazione(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.Integer).
	 */
	@Override
	public void aggiornaMetadatoTrasformazione(final String signTransactionId, final String documentTitle, final UtenteDTO utente, final Integer metadatoPdfErrorValue) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			// Prendi l'ultima versione del documento
			final Document documentForDetails = fceh.getDocumentForDetail(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle);
			
			LOGGER.info(signTransactionId + " recuperato il documento su filenet");

			// Aggiorna il metadato trasformazionePDFInErrore
			final Map<String, Object> metadati = new HashMap<>();
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.TRASFORMAZIONE_PDF_ERRORE_METAKEY), metadatoPdfErrorValue);

			fceh.updateMetadati(documentForDetails, metadati);
			
			LOGGER.info(signTransactionId + " aggiornato il metadato su filenet");
		} catch (final Exception e) {
			LOGGER.error(signTransactionId + "Errore durante l'aggiornamento del metadato TRASFORMAZIONE_PDF_ERRORE per il documento: " + documentTitle, e);
			throw e;
		} finally {
			popSubject(fceh);
		}
	}

	@Override
	public final boolean isValidSignature(final InputStream contentFile, final UtenteDTO utente, boolean isXades) {
		try {
			final PkHandler pkHandler = utente.getSignerInfo().getPkHandlerVerifica();
			final SignHelper shVerifica = new SignHelper(pkHandler.getHandler(), pkHandler.getSecurePin(), utente.getDisableUseHostOnly());

			return shVerifica.isValidSignature(contentFile, isXades);
		} catch (final Exception e) {
			LOGGER.error(GENERIC_ERROR_FIRMA_MSG, e);
			throw new RedException(e);
		}
	}

	@Override
	public final boolean hasSignatures(final InputStream contentFile, final UtenteDTO utente) {
		try {
			final PkHandler pkHandler = utente.getSignerInfo().getPkHandlerVerifica();
			final SignHelper shVerifica = new SignHelper(pkHandler.getHandler(), pkHandler.getSecurePin(), utente.getDisableUseHostOnly());

			return shVerifica.hasSignatures(contentFile, false);
		} catch (final Exception e) {
			LOGGER.error(GENERIC_ERROR_FIRMA_MSG, e);
			throw new RedException(e);
		}
	}

	@Override
	public final boolean hasSignaturesForSpedizione(final InputStream contentFile, final UtenteDTO utente, final String signerAoo) {
		try {
			boolean esito = false;
			final PkHandler pkHandler = utente.getSignerInfo().getPkHandlerVerifica();
			final SignHelper shVerifica = new SignHelper(pkHandler.getHandler(), pkHandler.getSecurePin(), utente.getDisableUseHostOnly());

			final String utenteSigner = utenteSRV.getByUsernameForSpedizione(signerAoo);
			if (!StringUtils.isNullOrEmpty(utenteSigner)) {
				esito = shVerifica.hasSignaturesForSpedizione(contentFile, utenteSigner);
			} else {
				LOGGER.error("Configurare Username timbro IMPORTANTE");
			}
			return esito;
		} catch (final Exception e) {
			LOGGER.error(GENERIC_ERROR_FIRMA_MSG, e);
			throw new RedException(e);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#checkAllegatiCopiaConforme(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.List).
	 */
	@Override
	public String checkAllegatiCopiaConforme(final UtenteDTO utente, final List<MasterDocumentRedDTO> documentiSelezionati) {
		IFilenetCEHelper fceh = null;
		StringBuilder errorMessage = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			DocumentSet allegatiFN = null;
			for (final MasterDocumentRedDTO doc : documentiSelezionati) {

				allegatiFN = fceh.getAllegati(doc.getGuuid(), false, false);
				Document allegatoFN = null;
				Boolean copiaConforme = null;
				@SuppressWarnings("unchecked")
				final Iterator<Document> i = allegatiFN.iterator();
				boolean esito = false;
				while (i.hasNext()) {
					allegatoFN = i.next();
					copiaConforme = (Boolean) TrasformerCE.getMetadato(allegatoFN, PropertiesNameEnum.IS_COPIA_CONFORME_METAKEY);
					if (copiaConforme != null && copiaConforme) {
						esito = true;
						break;
					}
				}

				if (!esito) {
					if (errorMessage == null) {
						errorMessage = new StringBuilder(
								"Attenzione i seguenti documenti non contengono allegati sui quali è possibile procedere con la Firma per copia conforme: ")
										.append(doc.getNumeroDocumento());
					} else {
						errorMessage.append(", ").append(doc.getNumeroDocumento());
					}
				}

			}

			return errorMessage != null ? errorMessage.toString() : null;

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero degli allegati da attestare per copia conforme ", e);
			throw e;
		} finally {
			popSubject(fceh);
		}
	}

	@Override
	public final List<AllegatoDTO> getAllegatiToSignCopiaConformeByDocPrincipale(final String documentTitle, final String guid, final UtenteDTO utente) {
		List<AllegatoDTO> allegatiDTO = new ArrayList<>();
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(),utente.getIdAoo());
		
			final DocumentSet allegati = fceh.getAllegati(StringUtils.cleanGuidToString(guid), false, false);
			
			if (allegati != null) {
				final Iterator<?> itAllegati = allegati.iterator();
				while (itAllegati.hasNext()) {
					final Document allegato = (Document) itAllegati.next();

					if (Boolean.TRUE.equals(TrasformerCE.getMetadato(allegato, PropertiesNameEnum.COPIA_CONFORME_METAKEY))) {
						final AllegatoDTO allegatoDTO = new AllegatoDTO();
						allegatoDTO.setDocumentTitle(TrasformerCE.getMetadato(allegato, pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)).toString());
						allegatoDTO.setNomeFile(TrasformerCE.getMetadato(allegato, pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY)).toString());
						allegatoDTO.setGuid(StringUtils.restoreGuidFromString(allegato.get_Id().toString()));
						allegatiDTO.add(allegatoDTO);
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero degli allegati da attestare per copia conforme per il documento: " + documentTitle, e);
			throw new RedException("Errore durante il recupero degli allegati da attestare per copia conforme");
		} finally {
			popSubject(fceh);
		}

		return allegatiDTO;
	}

	@Override
	public final LocalSignContentDTO getAllegato4CopiaConforme(final UtenteDTO utente, final String documentTitle) {
		LocalSignContentDTO file = new LocalSignContentDTO();
		
		try {
			final AttachmentDTO allegato = documentoSRV.getContentAllegato(documentTitle, utente.getFcDTO(), utente.getIdAoo());
			
			final String reason = getPostillaCopiaConforme(utente);
			
			final byte[] content = new AdobeLCHelper().insertPostillaCopiaConforme(reason, allegato.getContent());
			final Date dataFirma = new Date();

			final SignHelper sh = new SignHelper(utente.getSignerInfo().getPkHandlerFirma().getHandler(), utente.getSignerInfo().getPkHandlerFirma().getSecurePin(), 
					utente.getDisableUseHostOnly());
			final byte[] digest = sh.createPdfDigest(content, null, null, utente.getUsername(), 0, dataFirma, reason);

			file.setContent(content);
			file.setDigest(digest);
			file.setDataFirma(dataFirma);
		} catch (final Exception e) {
			LOGGER.error("Errore durante la procedura di recupero dell'allegato: " + documentTitle, e);
			throw new RedException("Errore durante la procedura di recupero dell'allegato");
		}

		return file;
	}

	private static String getPostillaCopiaConforme(final UtenteDTO utente) {
		return "Firmato digitalmente per copia conforme all'originale da " + utente.getNome() + " " + utente.getCognome();
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#saveAllegatoCopiaConforme(it.ibm.red.business.dto.UtenteDTO,
	 *      byte[], byte[], java.util.Date, java.lang.String, java.lang.String).
	 */
	@Override
	public String saveAllegatoCopiaConforme(final UtenteDTO utente, final byte[] content, final byte[] docSigned, final Date dataFirma, final String documentTitle,
			final String nomeAllegato) {
		String errorMsg = null;
		Document allegato = null;
		byte[] contentFirmato = null;
		IFilenetCEHelper fceh = null;

		try {
			// Recupera l'allegato
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(),utente.getIdAoo());
			allegato = fceh.getAllegatoForDownload(pp.getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY), documentTitle);
						
			// Merge  
			final SignHelper sh = new SignHelper(utente.getSignerInfo().getPkHandlerFirma().getHandler(), utente.getSignerInfo().getPkHandlerFirma().getSecurePin(), utente.getDisableUseHostOnly());
			final String reason = getPostillaCopiaConforme(utente);
			contentFirmato = sh.pdfmerge(content, docSigned, null, utente.getUsername(), null, dataFirma, reason);

			if (contentFirmato != null && contentFirmato.length != 0) {
				fceh.addVersion(allegato, new ByteArrayInputStream(contentFirmato), null, nomeAllegato, Constants.ContentType.PDF, utente.getIdAoo());
			} else {
				errorMsg = "Errore in fase di recupero del content firmato";
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la procedura di aggiornamento dell'allegato " + documentTitle, e);
			errorMsg = "Errore durante la procedura di aggiornamento dell'allegato";
		} finally {
			popSubject(fceh);
		}

		return errorMsg;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#completeCopiaConformeProcess(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	public EsitoOperazioneDTO completeCopiaConformeProcess(final UtenteDTO utente, final String wobNumber) {
		final EsitoOperazioneDTO esitoOperazioneFirma = new EsitoOperazioneDTO(wobNumber);
		esitoOperazioneFirma.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
		esitoOperazioneFirma.setEsito(false);

		FilenetPEHelper fpeh = null;

		try {

			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fpeh.nextStep(wobNumber, null, ResponsesRedEnum.ATTESTA_COPIA_CONFORME.getResponse());
			impostaEsitoOk(esitoOperazioneFirma);

		} catch (final SignException e) {
			LOGGER.error(ERROR_COMPLETAMENTO_PROCESSO_FIRMA_MSG + wobNumber, e);
			esitoOperazioneFirma.setCodiceErrore(e.getCodiceErrore());
			esitoOperazioneFirma.setNote(e.getNote());
		} catch (final Exception e) {
			LOGGER.error(ERROR_COMPLETAMENTO_PROCESSO_FIRMA_MSG + wobNumber, e);
			esitoOperazioneFirma.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
			esitoOperazioneFirma.setNote(e.getMessage());
		} finally {
			logoff(fpeh);
		}

		return esitoOperazioneFirma;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#checkDocumentsHasSoloDestinatariInterni(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.List).
	 */
	@Override
	public Boolean checkDocumentsHasSoloDestinatariInterni(final UtenteDTO utente, final List<MasterDocumentRedDTO> docList) {
		Boolean output = Boolean.TRUE;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(),utente.getIdAoo());
			
			for (MasterDocumentRedDTO doc : docList) {
				
				Document document = fceh.getDocumentByIdGestionale(doc.getDocumentTitle(), null, null, utente.getIdAoo().intValue(), null, null);
				DetailDocumentRedDTO documentDetailDTO = TrasformCE.transform(document, TrasformerCEEnum.FROM_DOCUMENTO_TO_DETAIL_RED);
				
				List<DestinatarioRedDTO> destinatari = documentDetailDTO.getDestinatari();
				if (!DestinatariRedUtils.checkDestinatariSoloInterni(destinatari)) {
					output = Boolean.FALSE;
					break;
				}
			}

			return output;
		} catch (Exception e) {
			LOGGER.error("Errore durante la verifica che i documenti selezionati contengano solo destinatari Interni.", e);
			throw new RedException("Errore durante la verifica che i documenti selezionati contengano solo destinatari Interni.", e);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @param idTipologiaDocumentoDocToSign
	 * @param isUtenteUCB
	 * @param codiceAoo
	 * @return
	 */
	private boolean checkIsInoltroAttoDecretoUCB(final Long idTipologiaDocumentoDocToSign, final boolean isUtenteUCB, final String codiceAoo) {
		boolean isInoltroAttoDecretoUCB = false;
		
		final String tipologiaAttoDecretoUscitaUCB = pp.getParameterByString(codiceAoo + "." + PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_USCITA_ATTO_DECRETO.getKey());
		
		if (isUtenteUCB && !StringUtils.isNullOrEmpty(tipologiaAttoDecretoUscitaUCB)
				&& Long.valueOf(tipologiaAttoDecretoUscitaUCB).equals(idTipologiaDocumentoDocToSign)) {
			
			isInoltroAttoDecretoUCB = true;

		}

		return isInoltroAttoDecretoUCB;
	}

	/**
	 * @see it.ibm.red.business.service.ISignSRV#checkIsFlussoAttoDecretoUCB(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.Long).
	 */
	@Override
	public boolean checkIsFlussoAttoDecretoUCB(final UtenteDTO utente, final Long idTipologiaDocumento) {
		return checkIsInoltroAttoDecretoUCB(idTipologiaDocumento, utente.isUcb(), utente.getCodiceAoo());
	}

	/**
	 * @see it.ibm.red.business.service.ISignSRV#checkIsFlussiAutUCB(java.lang.String,
	 *      it.ibm.red.business.dto.RispostaAllaccioDTO,
	 *      it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean checkIsFlussiAutUCB(final String idDocUscita, RispostaAllaccioDTO allaccioPrincipale, final UtenteDTO utente, final IFilenetCEHelper fceh,
			final Connection connection) {
		boolean isFlussoAUT = false;

		try {

			if (!StringUtils.isNullOrEmpty(idDocUscita) && allaccioPrincipale == null) {
				allaccioPrincipale = allaccioDAO.getAllaccioPrincipale(Integer.parseInt(idDocUscita), connection);
			}
			
			if (allaccioPrincipale != null) {
				final IFilenetCEHelper fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()),utente.getIdAoo());
				List<String> props = Arrays.asList(pp.getParameterByKey(PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY));
				Document document = fcehAdmin.getDocumentByIdGestionale(allaccioPrincipale.getIdDocumentoAllacciato(), props, null, utente.getIdAoo().intValue(), null, null);
				String codiceFlusso = TrasformerCE.getMetadato(document, PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY) != null ? TrasformerCE.getMetadato(document, PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY).toString() : "";
				
				if (!StringUtils.isNullOrEmpty(codiceFlusso)) {
					LOGGER.info(FIRMA_DOCUMENTO_LABEL + idDocUscita + ": codiceFlusso " + codiceFlusso + ", allaccio principale " + allaccioPrincipale.getIdDocumentoAllacciato());
					isFlussoAUT = isFamigliaFlussoAUT(codiceFlusso);
				}
				popSubject(fcehAdmin);
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante la verifica che il documento appartenga ai flussi AUT", e);
			throw new RedException("Errore durante la verifica che il documento appartenga ai flussi AUT", e);
		}

		return isFlussoAUT;
	}
	
	
	/**
	 * @param wobNumber
	 * @param documentForDetails
	 * @param documentTitle
	 * @param utente
	 * @param sh
	 * @param ste
	 * @param firmaAutografa
	 * @param principaleOnlyPAdESVisible
	 * @param fceh
	 * @param fpeh
	 * @return
	 */
	private EsitoOperazioneDTO firmaContentsDocumento(String signTransactionId, String wobNumber, Document documentForDetails, String documentTitle, UtenteDTO utente, SignHelper sh, 
			SignTypeEnum ste, boolean firmaAutografa, boolean principaleOnlyPAdESVisible, IFilenetCEHelper fceh, FilenetPEHelper fpeh) {
		EsitoOperazioneDTO esitoOperazioneFirma = new EsitoOperazioneDTO(wobNumber, documentTitle);
		esitoOperazioneFirma.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
		esitoOperazioneFirma.setEsito(false);
		List<EsitoOperazioneDTO> esitiOperazioneFirmaAllegati = new ArrayList<>();
		Integer metadatoPdfErrorValue = Constants.Varie.TRASFORMAZIONE_PDF_OK;
		Connection connection = null;
		
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			
			LOGGER.info(signTransactionId + ": " + (ste != null ? ste.toString() : "") + " - " + ((firmaAutografa) ? "autografa" : "remota"));
			
			DetailDocumentoDTO documentDetailDTO = getDocumentDetailDTO(signTransactionId, documentForDetails, utente, connection);
			
			Aoo aoo = null;
			if(SignTypeEnum.CADES.equals(ste)) {
				aoo = aooDAO.getAoo(utente.getIdAoo(), connection);
			}
			Integer numeroDocumento = documentDetailDTO.getNumeroDocumento();
			esitoOperazioneFirma.setIdDocumento(numeroDocumento);
			esitoOperazioneFirma.setNomeDocumento(documentDetailDTO.getNomeFile());
			ProtocolloDTO protocollo = documentDetailDTO.getProtocollo();
			
			// Elimino eventuali dati sporchi presenti nella tabella di bufferizzazione dei content firmati, derivanti da possibili precedenti tentativi di firma non andati a buon fine
			aSignDAO.removeFromSignedDocsBufferByWobNumber(connection, wobNumber);
			
			LOGGER.info(signTransactionId + ": ripulisco la tabella di bufferizzazione per il wobNumber " + wobNumber);
			
			// Controllo che l'utente possa firmare il documento
			EsitoOperazioneDTO isSignAuthorized = checkSignAuthorization(wobNumber, utente, connection, fpeh, documentTitle);
			if (!isSignAuthorized.isEsito()) {
				metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.WARN_GENERICO.getValue();
				return isSignAuthorized;
			}
			
			// ##################################################### Recupero content del documento principale #####################################################
			String mimeTypeDocPrincipale = null;
			try {
				FileDTO contentPrincipale = getContentToSign(signTransactionId, documentForDetails, documentTitle, documentDetailDTO.getSottoCategoriaUscita(), true, firmaAutografa, utente.getIdAoo(), fceh);
				LOGGER.info(signTransactionId + ": recuperato content principale da firmare su Filenet");
				documentDetailDTO.setContent(contentPrincipale.getContent());
				if (documentDetailDTO.getContent() != null && documentDetailDTO.getContent().length > 0) {
					mimeTypeDocPrincipale = contentPrincipale.getMimeType();
				}
			} catch (Exception e) {
				metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.KO_GENERICO.getValue();
				LOGGER.error(ERROR_RECUPERO_CONTENT_MSG + documentTitle, e);
				throw new RedException("Errore durante il recupero del content del documento principale.", e);
			}
			
			// ##################################################### Recupero allegati del documento principale ####################################################
			DocumentSet allegatiDaFirmare = fceh.getAllegatiConContentDaFirmare(StringUtils.cleanGuidToString(documentForDetails.get_Id()));
			LOGGER.info(signTransactionId + ": recuperati allegati da firmare su Filenet");
			
			// Controllo che l'utente possa firmare gli allegati
			if (!firmaAutografa) {
				EsitoOperazioneDTO checkAttach = checkAuthAttachmentSign(wobNumber, allegatiDaFirmare, ste);
				if (!checkAttach.isEsito()) {
					metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.KO_GENERICO.getValue();
					throw new SignException(checkAttach.getCodiceErrore(), checkAttach.getNote());
				}
			}
			
			SignTypeEnum stTemp;
			if (!firmaAutografa) { // Firma remota
				EsitoOperazioneDTO esitoSign = null;
				boolean firmaAllegatiOk = true;
				SignerInfoDTO siDTO = utente.getSignerInfo();
				// #################################################### Firmo allegati documento principale ########################################################
				if (allegatiDaFirmare != null) {
					Document docAllegato;
					String documentTitleAllegato;
					Integer formatoAllegatoId;
					Integer idAttach;
					Integer daFirmare;
					FileDTO contentAllegatoToSign;
					String tipoMimeAllegato;
					DetailDocumentoDTO allegatoDetailDTO;
					
					Iterator<?> it = allegatiDaFirmare.iterator();
					while (it.hasNext()) {
						stTemp = ste;
						docAllegato = (Document) it.next();
						documentTitleAllegato = (String) TrasformerCE.getMetadato(docAllegato, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
						
						LOGGER.info(signTransactionId + " firma allegato " + documentTitleAllegato);
						
						formatoAllegatoId = (Integer) TrasformerCE.getMetadato(docAllegato, PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY);
						idAttach = (Integer) TrasformerCE.getMetadato(docAllegato, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
						daFirmare = (Integer) TrasformerCE.getMetadato(docAllegato, PropertiesNameEnum.DA_FIRMARE_METAKEY);
						contentAllegatoToSign = getContentToSign(signTransactionId, docAllegato, documentTitleAllegato, null, false, firmaAutografa, utente.getIdAoo(), fceh);
						tipoMimeAllegato = contentAllegatoToSign.getMimeType();
						
						if (daFirmare != null && daFirmare == 1) {
							if (!ste.equals(SignTypeEnum.CADES)) {
								if (!MediaType.PDF.toString().equals(tipoMimeAllegato)) {
									metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.KO_GENERICO.getValue();
									throw new SignException(SignErrorEnum.DOCUMENTO_ALLEGATI_NO_PDF_FIRMA_COD_ERROR, SignErrorEnum.DOCUMENTO_ALLEGATI_NO_PDF_FIRMA_COD_ERROR.getMessage());
								}
								if (FormatoAllegatoEnum.FIRMATO_DIGITALMENTE.getId().equals(formatoAllegatoId) 
										|| (PdfHelper.getBlankSignatureNumber(contentAllegatoToSign.getContent()) <= 0)) {
									stTemp = SignTypeEnum.PADES_INVISIBLE;
								}
							}
							
							allegatoDetailDTO = new DetailDocumentoDTO();
							allegatoDetailDTO.setContent(contentAllegatoToSign.getContent());
							allegatoDetailDTO.setNomeFile((String) TrasformerCE.getMetadato(docAllegato, PropertiesNameEnum.NOME_FILE_METAKEY));
							// ### Firma del singolo allegato
							esitoSign = signDocument(signTransactionId, wobNumber, idAttach, siDTO, tipoMimeAllegato, stTemp, allegatoDetailDTO, false, sh, documentForDetails, utente, aoo);
							esitiOperazioneFirmaAllegati.add(esitoSign);
							if (!esitoSign.isEsito()) {
								firmaAllegatiOk = false;
							}
							
							// Inserimento dell'allegato nel buffer dei documenti firmati
							aSignDAO.insertIntoSignedDocsBuffer(connection, wobNumber, docAllegato.get_Id().toString(), allegatoDetailDTO.getContent(), true);
							LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentDetailDTO.getDocumentTitle() + ": firmato allegato: " + allegatoDetailDTO.getNomeFile());
						}
					} 
				} 
				
				esitoOperazioneFirma.setEsitiAllegati(esitiOperazioneFirmaAllegati);
				// ####################################################################################################
				
				stTemp = ste;
				if (!ste.equals(SignTypeEnum.CADES)) {
					if (!MediaType.PDF.toString().equals(mimeTypeDocPrincipale)) {
						metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.KO_GENERICO.getValue();
						throw new SignException(SignErrorEnum.DOCUMENTO_NO_PDF_FIRMA_COD_ERROR, SignErrorEnum.DOCUMENTO_NO_PDF_FIRMA_COD_ERROR.getMessage());
					}
					
					boolean noBlankSign = PdfHelper.getBlankSignatureNumber(documentDetailDTO.getContent()) <= 0;
					if (principaleOnlyPAdESVisible) {
						if (noBlankSign) {
							metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.WARN_GENERICO.getValue();
							throw new SignException(SignErrorEnum.DOCUMENTO_PRINCIPALE_NO_PADES_VISIBILE_SIGN_FIELDS_COD_ERROR, SignErrorEnum.DOCUMENTO_PRINCIPALE_NO_PADES_VISIBILE_SIGN_FIELDS_COD_ERROR.getMessage());
						}
						if (siDTO.getImageFirma() == null || siDTO.getImageFirma().length <= 0) {
							metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.WARN_GENERICO.getValue();
							throw new SignException(SignErrorEnum.DOCUMENTO_PRINCIPALE_NO_PADES_VISIBILE_GLIFO_COD_ERROR, SignErrorEnum.DOCUMENTO_PRINCIPALE_NO_PADES_VISIBILE_GLIFO_COD_ERROR.getMessage());
						}
					} else {
						if (noBlankSign) {
							// FIRMA INVISIBILE SE "FIRMATO_DIGITALMENTE" O NON CONTIENE CAMPI FIRMA
							stTemp = SignTypeEnum.PADES_INVISIBLE;
						}
					}
				}
				
				// ############################################################ Firmo documento principale ############################################################
				esitoSign = signDocument(signTransactionId, wobNumber, numeroDocumento, siDTO, mimeTypeDocPrincipale, stTemp, documentDetailDTO, true, sh, documentForDetails, utente, aoo);
				if (!esitoSign.isEsito() || !firmaAllegatiOk) {
					metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.WARN_PKBOX.getValue();
					throw new SignException(esitoSign.getCodiceErrore(), esitoSign.getNote());
				}
				
				LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentDetailDTO.getDocumentTitle() + ": firmato documento principale");
			}
			
			// Inserimento dell'allegato nel buffer dei documenti firmati
			aSignDAO.insertIntoSignedDocsBuffer(connection, wobNumber, documentDetailDTO.getGuid(), documentDetailDTO.getContent(), false);
			
			commitConnection(connection);
			
			impostaEsitoOk(esitoOperazioneFirma, protocollo, documentDetailDTO.getIsRegistroRepertorio());
			
		} catch (SignException e) {
			metadatoPdfErrorValue = gestisciEccezioneFirmaContents(wobNumber, esitoOperazioneFirma, e, e.getCodiceErrore(), e.getNote(), metadatoPdfErrorValue, connection);
		} catch (ProtocolloGialloException e) {
			metadatoPdfErrorValue = gestisciEccezioneFirmaContents(wobNumber, esitoOperazioneFirma, e, SignErrorEnum.GENERIC_COD_ERROR, e.getMessage(), metadatoPdfErrorValue, connection);
		} catch (Exception e) {
			metadatoPdfErrorValue = gestisciEccezioneFirmaContents(wobNumber, esitoOperazioneFirma, e, SignErrorEnum.GENERIC_COD_ERROR, SignErrorEnum.GENERIC_COD_ERROR.getMessage(), 
					metadatoPdfErrorValue, connection);
		} finally {
			closeConnection(connection);
			
			if (documentTitle != null && !firmaAutografa) {
				aggiornaMetadatoTrasformazione(signTransactionId, documentTitle, utente, metadatoPdfErrorValue);
			}
		}
		
		return esitoOperazioneFirma;
	}

	
	/**
	 * Restituisce il content da firmare.
	 * 
	 * @param signTransactionId
	 * @param docFilenet
	 * @param documentTitlePrincipale
	 * @param sottoCategoriaUscitaDoc
	 * @param principale
	 * @param firmaAutografa
	 * @param fceh
	 * @return
	 * @throws IOException 
	 */
	private FileDTO getContentToSign(String signTransactionId, Document docFilenet, String documentTitle, SottoCategoriaDocumentoUscitaEnum sottoCategoriaUscitaDoc, boolean principale, 
			boolean firmaAutografa, Long idAoo, IFilenetCEHelper fceh) throws IOException {
		// FIRMA AUTOGRAFA	
		//	- PRINCIPALE	Il content da firmare è quello originale.
		//	- ALLEGATI		Il content da firmare è quello presente nell’annotation, se presente e non esplicitamente ignorato, altrimenti sarà quello originale.
		// FIRMA DIGITALE
		//	- PRINCIPALE	Il content da firmare è quello presente nell’annotation, se presente e non esplicitamente ignorato, altrimenti sarà quello originale.
		//					Se firmi l’originale dovrai aggiungere la postilla come timbro solo nella prima pagina tramite processo asincrono.
		//					Nel caso siano richieste le approvazioni queste verranno inserite come timbro di protocollo solo nell’ultima pagina (a piè di pagina),
		//					attraverso il processo asincrono di firma.
		//	- ALLEGATI		Il content da firmare è quello presente nell’annotation, se presente e non esplicitamente ignorato, altrimenti sarà quello originale.
		FileDTO contentToSignFile = null;
		
		if (firmaAutografa && principale) {
		
			Document contentDocument = fceh.getDocumentForDownload(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle, false);
			contentToSignFile = new FileDTO(null, FilenetCEHelper.getDocumentContentAsByte(contentDocument), contentDocument.get_MimeType());
		
		} else { // Firma digitale (principale/allegato) o firma autografa (allegato)
			
			String nomeFile = (String) TrasformerCE.getMetadato(docFilenet, PropertiesNameEnum.NOME_FILE_METAKEY);
			
			// Il content da firmare è quello presente nell’annotation, se è presente e non deve essere esplicitamente ignorato.
			AnnotationDTO annotationContentLibroFirma = null;
			
			// Se il content è associato a una registrazione ausiliaria l'annotation CONTENT_LIBRO_FIRMA deve essere ignorata nel recupero del content da firmare,
			// poiché contiene i placeholder della registrazione ausiliaria e quindi non può essere firmata.
			if (sottoCategoriaUscitaDoc == null || !sottoCategoriaUscitaDoc.isRegistrazioneAusiliaria()) {
				annotationContentLibroFirma = fceh.getAnnotationDocumentContent(documentTitle, !principale, FilenetAnnotationEnum.CONTENT_LIBRO_FIRMA, idAoo);
				LOGGER.info(signTransactionId + " recuperata annotationContentLibroFirma su Filenet");
			}
			
			if (annotationContentLibroFirma != null) { // Si firma l'annotation
				
				contentToSignFile = new FileDTO(nomeFile, IOUtils.toByteArray(annotationContentLibroFirma.getContent()), ContentType.PDF, true);
				
			} else { // Si firma il content originale
				
				Document contentDocument;
				if (principale) {
					contentDocument = fceh.getDocumentForDownload(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle, false);
					LOGGER.info(signTransactionId + " recuperato content su Filenet");
				} else {
					contentDocument = docFilenet; // Se si tratta di un allegato, il documento Filenet in input ha il content
				}
				
				contentToSignFile = new FileDTO(nomeFile, FilenetCEHelper.getDocumentContentAsByte(contentDocument), contentDocument.get_MimeType());
				
			}
			
		}
		
		return contentToSignFile;
	}
	
	/**
	 * @see it.ibm.red.business.service.ISignSRV#gestisciFlussiPerNPS(it.ibm.red.business.dto.EsitoOperazioneDTO,
	 *      filenet.vw.api.VWWorkObject, java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.enums.SignTypeEnum, boolean,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public void gestisciFlussiPerNPS(final String signTransactionId, final EsitoOperazioneDTO esito, final VWWorkObject workflow, final String documentTitle, final UtenteDTO utente, 
			final SignTypeEnum signType, final boolean firmaAutografa, final IFilenetCEHelper fceh, final Connection connection, final Connection conBuffer) {
		String wobNumber = workflow.getWorkflowNumber();
		
		try {
			// Recupero il dettaglio del documento dal CE
			Document documentForDetails = fceh.getDocumentRedForDetail(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle);
			
			LOGGER.info(signTransactionId + " recuperato documento su Filenet");
			
			DetailDocumentoDTO documentDetailDTO = getDocumentDetailDTO(signTransactionId, documentForDetails, utente, connection);
			
			LOGGER.info(signTransactionId + " attività trasformer completate");
			
			String codiceFlusso = getCodiceFlussoDocumentoFirma(signTransactionId, documentTitle, utente, connection);
			boolean flussoAUT = isFamigliaFlussoAUT(codiceFlusso);
			boolean inoltroAttoDecretoUCB = checkIsInoltroAttoDecretoUCB(documentDetailDTO.getIdTipologiaDocumento(), utente.isUcb(), utente.getCodiceAoo());
			if (inoltroAttoDecretoUCB) {
				codiceFlusso = Constants.Varie.CODICE_FLUSSO_ATTO_DECRETO;
			}
			
			// Se flusso AUT o inoltro Atto Decreto UCB => upload sincrono verso NPS, invioAtti, salva dati protocollo
			if (flussoAUT || inoltroAttoDecretoUCB) {
				boolean cades = !firmaAutografa && SignTypeEnum.CADES.equals(signType);
				byte[] contentPrincipaleFirmato = null;
				Map<String, byte[]> allegatiFirmatiToPersist = new HashMap<>();
				
				// Si recuperano i content firmati dalla tabella di bufferizzazione
				Collection<DocumentoDTO> signedDocs = aSignDAO.getFromSignedDocsBuffer(conBuffer, workflow.getWorkflowNumber());
				
				LOGGER.info(signTransactionId + " recuperati i content da inviare ad NPS sulla tabella di bufferizzazione");
				
				if (!CollectionUtils.isEmpty(signedDocs)) {
					for (DocumentoDTO signedDoc : signedDocs) {
						if (signedDoc.isFlagAllegato()) {
							allegatiFirmatiToPersist.put(signedDoc.getGuid(), signedDoc.getContent());
						} else {
							contentPrincipaleFirmato = signedDoc.getContent();
						}
					}
				}
				
				// Esegui upload sincrono content principale verso NPS
				String idDocumentoPrincipaleNPS = uploadSyncNPS(utente.getIdAoo().intValue(), cades, !firmaAutografa, documentDetailDTO.getNomeFile(), 
						documentDetailDTO.getMimeType(), new ByteArrayInputStream(contentPrincipaleFirmato), documentDetailDTO.getGuid(), 
						documentDetailDTO.getOggetto());
				
				LOGGER.info(signTransactionId + " => inviato content principale ad NPS: " + idDocumentoPrincipaleNPS);
				
				// Esegui upload sincrono allegati firmati verso NPS
				List<String> idAllegatiNPS = new ArrayList<>();
				List<String> guidAllegatiInviati = new ArrayList<>();
				uploadAttachSyncNPS(signTransactionId, utente.getIdAoo().intValue(), allegatiFirmatiToPersist, documentTitle, cades, firmaAutografa, fceh, idAllegatiNPS, guidAllegatiInviati);
				
				// Esegui upload sincrono restanti allegati verso NPS
				uploadNotSentDocSyncNPS(signTransactionId, utente.getIdAoo().intValue(), documentDetailDTO.getGuid(), documentDetailDTO.getDocumentTitle(), cades, fceh, idAllegatiNPS, guidAllegatiInviati);
				
				String acl = getACL4NPS(workflow, documentDetailDTO, utente, fceh, documentDetailDTO.getFlagRiservato());
				LOGGER.info(signTransactionId + " calcolata la catena di ACL da inviare ad NPS: " + acl);
				
				// Esegui inviaAtto
				ProtocolloNpsDTO datiProtocolloIdDoc = inviaAtto(signTransactionId, codiceFlusso, documentDetailDTO, acl, utente, fceh, connection, idDocumentoPrincipaleNPS, idAllegatiNPS);
				LOGGER.info(signTransactionId + " invocato inviaAtto di NPS");
				
				// Salva dati protocollo
				salvaDatiProtocollo(signTransactionId, datiProtocolloIdDoc, documentDetailDTO, documentForDetails, esito, utente, fceh, connection);
				
				LOGGER.info(signTransactionId + " salvati i dati di protocollo su Filenet");
				
				// Recupera documento stampigliato da NPS
				if (!cades) {
					DocumentoNpsDTO downloadDocumento = npsSRV.downloadDocumento(utente.getIdAoo().intValue(), datiProtocolloIdDoc.getIdDocumento(), false);
					LOGGER.info(signTransactionId + " recuperato content stampigliato da NPS: " + datiProtocolloIdDoc.getIdDocumento());
					
					// Si aggiorna il content del documento principale nella tabella di bufferizzazione
					aSignDAO.updateContentPrincipaleForSignedDocsBuffer(conBuffer, IOUtils.toByteArray(downloadDocumento.getInputStream()), workflow.getWorkflowNumber());
					
					LOGGER.info(signTransactionId + " salvato content principale sulla base dati (tabella di bufferizzazione)");
				}
			}
		} catch (Exception e) {
			LOGGER.error(signTransactionId + " => errore durante l'esecuzione delle operazioni successive alla firma di documenti afferenti a flussi"
					+ " che coinvolgono NPS. WOB number: " + wobNumber, e);
			throw new RedException(e);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#protocollazione(java.util.Collection,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<EsitoOperazioneDTO> protocollazione(String signTransactionId, Collection<String> wobNumbers, UtenteDTO utente) {
		Collection<EsitoOperazioneDTO> esitiProtocollazione = new ArrayList<>();
		String currentWobNumber = null;
		Connection connection = null;
		FilenetPEHelper fpeh = null;
		
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			VWWorkObject wob = null;
			EsitoOperazioneDTO esitoProt;
			
			LOGGER.info(signTransactionId + ": protocollazione di (" + wobNumbers.size() + ") item ");
			
			for (String wobNumber : wobNumbers) {
				currentWobNumber = wobNumber;
				wob = fpeh.getWorkFlowByWob(wobNumber, true);
				
				esitoProt = protocollazione(signTransactionId, wob, utente, connection);
				
				if (esitoProt.isEsito()) {
					connection.commit();
				} else {
					rollbackConnection(connection);
				}
				
				esitiProtocollazione.add(esitoProt);
			}
			
			if(wobNumbers.size() == 0) {
				commitConnection(connection);
			}
			
		} catch (Exception e) {
			rollbackConnection(connection);
			LOGGER.error("Errore generico in fase di protocollazione: " + e.getMessage(), e);
			EsitoOperazioneDTO esitoKo = new EsitoOperazioneDTO(currentWobNumber);
			esitoKo.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
			esitoKo.setNote(SignErrorEnum.GENERIC_COD_ERROR.getMessage());
			esitiProtocollazione.add(esitoKo);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
		}
		
		return esitiProtocollazione;
	}
	
	/**
	 * @see it.ibm.red.business.service.ISignSRV#protocollazione(filenet.vw.api.VWWorkObject,
	 *      it.ibm.red.business.dto.UtenteDTO, java.sql.Connection).
	 */
	@Override
	public EsitoOperazioneDTO protocollazione(String signTransactionId, VWWorkObject wob, UtenteDTO utente, Connection connection) {
		String documentTitle = TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY).toString();
		EsitoOperazioneDTO esitoProtocollazionePerFirma = new EsitoOperazioneDTO(wob.getWorkflowNumber(), documentTitle);
		esitoProtocollazionePerFirma.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
		esitoProtocollazionePerFirma.setEsito(false);
		IFilenetCEHelper fceh = null;
		ProtocolloDTO protocollo = null;
		Integer metadatoPdfErrorValue = 0;
		final String logId = signTransactionId + documentTitle;
		try {
			LOGGER.info(logId + ": protocollazione");
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			
			Document documentForDetails = fceh.getDocumentRedForDetail(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle);
			DetailDocumentoDTO documentDetailDTO = getDocumentDetailDTO(logId, documentForDetails, utente, connection);
			
			esitoProtocollazionePerFirma.setIdDocumento(documentDetailDTO.getNumeroDocumento());
			esitoProtocollazionePerFirma.setNomeDocumento(documentDetailDTO.getNomeFile());
			protocollo = documentDetailDTO.getProtocollo();
			
			boolean flussoAUT = isFamigliaFlussoAUT(getCodiceFlussoDocumentoFirma(logId, documentTitle, utente, connection));
			boolean inoltroAttoDecretoUCB = checkIsInoltroAttoDecretoUCB(documentDetailDTO.getIdTipologiaDocumento(), utente.isUcb(), utente.getCodiceAoo());
			
			boolean protocolloToBook = !Integer.valueOf(TipoAssegnazioneEnum.FIRMA_MULTIPLA.getId()).equals(TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY))
					&& !(flussoAUT || inoltroAttoDecretoUCB) && (documentDetailDTO.getIsRegistroRepertorio() 
							|| (documentDetailDTO.isHasDestinatariEsterni() && !((Integer) MomentoProtocollazioneEnum.PROTOCOLLAZIONE_NON_STANDARD.getId()).equals(documentDetailDTO.getIdMomentoProtocollazione())));
			
			if (protocolloToBook) {
				Long idTipoProtocollo = utente.getIdTipoProtocollo();
				boolean protocolloBooked = false;
				// ### Stacco del protocollo -> START ###
				if (protocollo.getNumeroProtocollo() <= 0) {
					
					if (TipologiaProtocollazioneEnum.isProtocolloNPS(idTipoProtocollo)) {
						ProtocolloNpsDTO protocolloNps = null;
						
						if (TipologiaProtocollazioneEnum.NPS.getId().equals(idTipoProtocollo)) {
						
							Integer idFascicolo = (Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY));
							if (idFascicolo != null) {
								Document dFascicolo = fceh.getFascicolo(idFascicolo + Constants.EMPTY_STRING, utente.getIdAoo().intValue());
								LOGGER.info(logId + ": recuperato fascicolo " + idFascicolo + " da Filenet");
								FascicoloDTO fascicoloDTO = TrasformCE.transform(dFascicolo, TrasformerCEEnum.FROM_DOCUMENTO_TO_FASCICOLO);
								LOGGER.info(logId + ": costuito DTO fascicolo " + idFascicolo);
								documentDetailDTO.setOggettoFascicolo(fascicoloDTO.getDescrizione());
								documentDetailDTO.setStatoFascicolo(fascicoloDTO.getStato());
								documentDetailDTO.setIndiceFascicolo(fascicoloDTO.getIndiceClassificazione());
								documentDetailDTO.setDataCreazioneFascicolo(fascicoloDTO.getDataCreazione());
								documentDetailDTO.setNumeroDocumentoFascicolo(fascicoloDTO.getIdFascicolo());
								documentDetailDTO.setDescrizioneIndiceFascicolo(fascicoloDTO.getDescrizioneTitolario());
							}
							
							String acl = getACL4NPS(wob, documentDetailDTO, utente, fceh, documentDetailDTO.getFlagRiservato());
							LOGGER.info(logId + ": costruita catena di acl da inviare a NPS " + acl);
							
							protocolloNps = creaProtocolloUscita(utente, connection, fceh, documentDetailDTO, acl);
							
							LOGGER.info(logId + ": staccato protocollo da NPS");
						
						} else {
							// Recupera le informazioni di emergenza dalla base dati
							protocolloNps = protocolliEmergenzaSRV.getInfoEmergenzaPerProtocolloNps(documentTitle, documentDetailDTO.getOggetto(), 
									documentDetailDTO.getTipologiaDocumento(), utente.getIdAoo(), connection);
							
							LOGGER.info(logId + ": recuperate informazioni protocollo di emergenza sulla base dati");
							
							if (protocolloNps == null || protocolloNps.getNumeroProtocollo() == 0 || StringUtils.isNullOrEmpty(protocolloNps.getIdProtocollo()) 
									|| StringUtils.isNullOrEmpty(protocollo.getAnnoProtocollo()) || "null".equalsIgnoreCase(protocollo.getAnnoProtocollo())) {
								throw new ProtocolloException("Errore durante il recupero dal DB delle informazioni del protocollo di emergenza");
							}
						}
						
						if (protocolloNps == null || protocolloNps.getIdProtocollo() == null || protocolloNps.getIdProtocollo().trim().length() <= 0) {
							throw new ProtocolloException("Non è stato possibile recuperare l'ID del protocollo NPS. "
									+ "Per il processo di firma tramite protocollazione NPS si necessita dell'ID del protocollo. Contattare l'assistenza. "
									+ "\nDati del documento: " + documentDetailDTO);
						}
						
						protocollo = new ProtocolloDTO(protocolloNps.getIdProtocollo(), protocolloNps.getNumeroProtocollo(), protocolloNps.getAnnoProtocollo(), 
								protocolloNps.getDataProtocollo(), protocolloNps.getDescrizioneTipologiaDocumento(), protocolloNps.getOggetto(), protocolloNps.getCodiceRegistro());
						protocolloBooked = true;
						
					} else if (TipologiaProtocollazioneEnum.MEF.getId().equals(idTipoProtocollo)) {
						SequKDTO sequK = parametriProtocolloDAO.getParametriProtocolloByIdAoo(utente.getIdAoo(), connection);
						
						protocollo = creaProtocolloMefUscita(utente, documentDetailDTO, sequK);
						
						LOGGER.info(logId + ": staccato protocollo da PMEF");
						
						protocolloBooked = true;
					} else {
						throw new RedException("Il servizio di protocollazione da utilizzare per la protocollazione del documento NON è stato riconosciuto. Contattare l'assistenza.");
					}
					
					if (protocolloBooked) {
						doActionsProtocolloBooked(logId, documentDetailDTO, documentForDetails, protocollo, utente, fceh, connection);				
					}
					// ### Stacco del protocollo -> END ###
				// Protocollo già staccato e salvato su FileNet, si ri-eseguono le azioni successive alla protocollazione
				} else {
					// Azioni relative a flussi successive alla protocollazione
					doActionsFlussoProtocolloBooked(logId, documentDetailDTO, protocollo, utente, connection);
				}
				
				if (protocollo == null || protocollo.getNumeroProtocollo() <= 0) {
					throw new ProtocolloException("Non è stato possibile recuperare il protocollo. Contattare l'assistenza.");
				}
				
				LOGGER.info(logId + ": protocollo " + protocollo);
			} else {
				LOGGER.info(logId + ": NON è necessario staccare il protocollo.");
			}
						
			impostaEsitoOk(esitoProtocollazionePerFirma, protocollo, documentDetailDTO.getIsRegistroRepertorio());
			
		} catch (ProtocolloException e) {
			gestisciEccezioneStepFirma(e, esitoProtocollazionePerFirma, SignErrorEnum.PROTOCOLLAZIONE_STEP_ASIGN_COD_ERROR, null);
			metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.KO_GENERICO.getValue();
		} catch (ProtocolloGialloException e) {
			gestisciEccezioneStepFirma(e, esitoProtocollazionePerFirma, SignErrorEnum.PROTOCOLLAZIONE_STEP_ASIGN_COD_ERROR, null);
			metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.WARN_GENERICO.getValue();
		} catch (Exception e) {
			gestisciEccezioneStepFirma(e, esitoProtocollazionePerFirma, SignErrorEnum.PROTOCOLLAZIONE_STEP_ASIGN_COD_ERROR, null);
			metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.WARN_GENERICO.getValue();
		} finally {
			popSubject(fceh);
			
			if (documentTitle != null/* && !firmaAutografa*/) {
				aggiornaMetadatoTrasformazione(logId, documentTitle, utente, metadatoPdfErrorValue);
			}
		}
		
		return esitoProtocollazionePerFirma;
	}

	/**
	 * @param utente
	 * @param documentDetailDTO
	 * @param sequK
	 * @return ProtocolloDTO se creato correttamente
	 */
	private ProtocolloDTO creaProtocolloMefUscita(UtenteDTO utente, DetailDocumentoDTO documentDetailDTO, SequKDTO sequK) {
		ProtocolloDTO protocollo = null;
		try {
			protocollo = pMefSRV.creaProtocolloMEFUscita(documentDetailDTO.getOggetto(), sequK, utente.getNome() + " " + utente.getCognome(), utente);
		} catch (RedException e) {
			throw new ProtocolloException(e);
		}
		return protocollo;
	}

	/**
	 * @param utente
	 * @param connection
	 * @param fceh
	 * @param documentDetailDTO
	 * @param acl
	 * @return ProtocolloNpsDTO se creato correttamente
	 */
	private ProtocolloNpsDTO creaProtocolloUscita(UtenteDTO utente, Connection connection, IFilenetCEHelper fceh,
			DetailDocumentoDTO documentDetailDTO, String acl) {
		ProtocolloNpsDTO protocolloNps = null;
		try {
			// Si stacca il protocollo da NPS
			protocolloNps = npsSRV.creaProtocolloUscita(documentDetailDTO, acl, utente, Builder.DA_SPEDIRE, connection, fceh);
		} catch(ProtocolloGialloException e) {
			throw new ProtocolloGialloException(e);
		} catch (RedException e) {
			throw new ProtocolloException(e);
		}
		return protocolloNps;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#registrazioneAusiliaria(java.util.Collection,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<EsitoOperazioneDTO> registrazioneAusiliaria(String signTransactionId, Collection<String> wobNumbers, UtenteDTO utente) {
		Collection<EsitoOperazioneDTO> esitiRegistrazioneAusiliaria = new ArrayList<>();
		String currentWobNumber = null;
		Connection connection = null;
		Connection connectionRigenerazioneContent = null;
		FilenetPEHelper fpeh = null;
		VWWorkObject wob = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			connectionRigenerazioneContent = setupConnection(getDataSource().getConnection(), true);
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			EsitoOperazioneDTO esitoRegAus;
			EsitoOperazioneDTO esitoProtocollazioneRegAus;
			EsitoOperazioneDTO esitoRigenerazioneContentRegAus;
			
			LOGGER.info(signTransactionId + ": registrazioneAusiliaria di (" + wobNumbers.size() + ") item ");
			
			for (String wobNumber : wobNumbers) {
				currentWobNumber = wobNumber;
				wob = fpeh.getWorkFlowByWob(wobNumber, true);
				LOGGER.info(signTransactionId + ": recuperato workflow " + wobNumber);
				// Inizializzazione esito
				esitoRegAus = new EsitoOperazioneDTO(currentWobNumber);
				esitoRegAus.setEsito(false);
				esitoRegAus.setCodiceErrore(SignErrorEnum.REG_AUX_PROTOCOLLAZIONE_STEP_ASIGN_COD_ERROR);
				
				// Esecuzione fase 1 registrazione ausiliaria
				esitoProtocollazioneRegAus = protocollaRegAux(signTransactionId, wob, utente, connection);

				// Se la prima fase ha avuto esito positivo, occorre eseguire la seconda fase prima di considerare la registrazione completata con successo
				if (esitoProtocollazioneRegAus.isEsito()) {
					connection.commit();
					// Se la prima fase ha avuto esito positivo, viene modificato il codice errore
					esitoRegAus.setCodiceErrore(SignErrorEnum.REG_AUX_RIGENERAZIONE_CONTENT_STEP_ASIGN_COD_ERROR);

					esitoRigenerazioneContentRegAus = rigeneraContentRegAux(signTransactionId, wob, utente, connectionRigenerazioneContent);
					
					// Se anche la seconda fase ha avuto esito positivo, la registrazione ausiliaria è stata conclusa correttamente
					if (esitoRigenerazioneContentRegAus.isEsito()) {
						connectionRigenerazioneContent.commit();
						esitoRegAus = esitoRigenerazioneContentRegAus;
					} else {
						rollbackConnection(connectionRigenerazioneContent);
					}
				} else {
					rollbackConnection(connection);
				}
				
				esitiRegistrazioneAusiliaria.add(esitoRegAus);
			}
			
			if(wobNumbers.size() == 0) {
				commitConnection(connection);
				commitConnection(connectionRigenerazioneContent);
			}
			
		} catch (Exception e) {
			rollbackConnection(connection);
			rollbackConnection(connectionRigenerazioneContent);
			LOGGER.error("Errore generico in fase di gestione della registrazione ausiliaria: " + e.getMessage(), e);
			EsitoOperazioneDTO esitoKo = new EsitoOperazioneDTO(currentWobNumber);
			esitoKo.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
			esitoKo.setNote(SignErrorEnum.GENERIC_COD_ERROR.getMessage());
			esitiRegistrazioneAusiliaria.add(esitoKo);
			try {
				String documentTitle = TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY).toString();
				String logId = signTransactionId + documentTitle;
				aggiornaMetadatoTrasformazione(logId, documentTitle, utente, TrasformazionePDFInErroreEnum.WARN_PROTO.getValue());
			}catch(Exception ex) {
				LOGGER.error("Errore nell'aggiornamento del SEMAFORO Registrazione ausiliaria", ex);
			}
		} finally {
			closeConnection(connection);
			closeConnection(connectionRigenerazioneContent);
			logoff(fpeh);
		}
		
		return esitiRegistrazioneAusiliaria;
	}

	/**
	 * Restituisce l'immagine del glifo della specifica aoo.
	 * 
	 * @param idAOO
	 *            id area organizzativa
	 * @param confPDFA
	 *            flag associato alla transparency della postilla
	 * @return byte array dell'immagine
	 */
	@Override
	public byte[] getImageFirmaAOO(Long idAOO, boolean confPDFA) {

		Connection connection = null;
		byte [] contentReturn = null;
		
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			contentReturn = utenteDAO.getImageFirmaAOO(idAOO, confPDFA, connection);
		
		} catch (Exception e) {
			rollbackConnection(connection);
			LOGGER.error("Errore generico in fase di gestione di retrieve della postilla",e );
		} finally {
			closeConnection(connection);
		}
			return contentReturn;
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public final EsitoOperazioneDTO stampigliatura(final VWWorkObject wob, final UtenteDTO utente, final SignTypeGroupEnum ste, Map<ContextASignEnum, Object> context, 
			final Connection connection) {
		String documentTitle = TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY).toString();
		EsitoOperazioneDTO esitoStampigliatura = new EsitoOperazioneDTO(wob.getWorkflowNumber(), documentTitle);
		esitoStampigliatura.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
		esitoStampigliatura.setEsito(false);
		IFilenetCEHelper fceh = null;
		String dtAllegato = "";
		try {
			LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": stampigliatura");
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			
			Document documentForDetails = fceh.getDocumentRedForDetail(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle);
			
			// Esclusione DSR e DD dalla stampigliatura
			if (!pp.getParameterByKey(PropertiesNameEnum.DSR_CLASSNAME_FN_METAKEY).equals(documentForDetails.getClassName()) &&
					!pp.getParameterByKey(PropertiesNameEnum.DECRETO_DIRIGENZIALE_FEPA_CLASSNAME_FN_METAKEY).equals(documentForDetails.getClassName())) {
				Aoo aoo = aooDAO.getAoo(utente.getIdAoo(), connection);
				DetailDocumentoDTO documentDetailDTO = getDocumentDetailDTO(FIRMA_DOCUMENTO_LABEL + documentTitle, documentForDetails, utente, connection);
				
				ProtocolloDTO protocollo = documentDetailDTO.getProtocollo();
				LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": protocollo " + protocollo);
				
				boolean firmaMultipla = Integer.valueOf(TipoAssegnazioneEnum.FIRMA_MULTIPLA.getId()).equals(TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY));
				
				boolean protocolloPresente = protocollo != null && protocollo.getNumeroProtocollo() > 0;
				boolean flussoAUT = isFamigliaFlussoAUT(getCodiceFlussoDocumentoFirma(FIRMA_DOCUMENTO_LABEL + documentTitle, documentTitle, utente, connection));
				boolean inoltroAttoDecretoUCB = checkIsInoltroAttoDecretoUCB(documentDetailDTO.getIdTipologiaDocumento(), utente.isUcb(), utente.getCodiceAoo());
				// 1) Se l'iter è di Firma Multipla, il protocollo non deve essere stampigliato.
				// 2) Se il documento è associato a una registrazione ausiliaria il protocollo è già stato stampigliato dal processo di Trasformazione PDF richiamato
				// al termine dello step di registrazione ausiliaria per apporre nuovamente le stampigliature sul nuovo content contenente le info della registrazione ausiliaria.
				// 3) Se si tratta di flusso AUT/CG2 o Atto Decreto UCB, il protocollo è già stato stampigliato sul documento principale da NPS con la chiamata a "inviaAtto".
				// 4) Se la protocollazione è anticipata (non standard), il protocollo è già stato stampigliato dal processo di Trasformazione PDF.
				boolean protocolloDaStampigliare = !firmaMultipla && (documentDetailDTO.getSottoCategoriaUscita() == null || !documentDetailDTO.getSottoCategoriaUscita().isRegistrazioneAusiliaria()) 
						&& !(flussoAUT || inoltroAttoDecretoUCB) && (documentDetailDTO.getIsRegistroRepertorio() 
								|| (documentDetailDTO.isHasDestinatariEsterni() && MomentoProtocollazioneEnum.PROTOCOLLAZIONE_NON_STANDARD.getId() != documentDetailDTO.getIdMomentoProtocollazione()
								|| (!documentDetailDTO.isHasDestinatariEsterni() && documentDetailDTO.isHasDestinatariInterni())));
				
				if (aoo.getPkHandlerTimbro() != null) {
					SignHelper shTimbro = new SignHelper(aoo.getPkHandlerTimbro().getHandler(), aoo.getPkHandlerTimbro().getSecurePin(), aoo.getDisableUseHostOnly());
					context.put(ContextASignEnum.DOC_FN_GUID_LIST, new ArrayList<>());
					String stampigliaturaProtocollo = Constants.EMPTY_STRING;
					
					// ###### Si stampigliano il protocollo, le approvazioni e la postilla sul documento PRINCIPALE -> START
					// Esclusione dei P7M dal processo di stampigliatura della postilla e del protocollo
					if (!FileUtils.isP7MFile(documentDetailDTO.getMimeType())) {
						LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": stampigliatura documento principale");
						Boolean flagCopiaConforme = (Boolean) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.FIRMA_COPIA_CONFORME_METAKEY));
						byte[] contentPrincipaleStampigliato = FilenetCEHelper.getDocumentContentAsByte(documentForDetails);
							
						if (!Boolean.TRUE.equals(flagCopiaConforme)) {
							// Protocollo
							if (protocolloDaStampigliare) {
								stampigliaturaProtocollo = getStampigliaturaProtocollo(documentDetailDTO, documentForDetails, protocollo, utente, protocolloPresente, true);
								
								if (StringUtils.isNullOrEmpty(stampigliaturaProtocollo)) {
									throw new RedException("Impossibile generare la stampigliatura del protocollo in quanto non è stato possibile recuperare il protocollo.");
								} else {
									LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": stampigliatura di protocollo " + stampigliaturaProtocollo);
								}
								
								contentPrincipaleStampigliato = shTimbro.applicaTimbroProtocolloPrincipaleUscita(aoo.getSignerTimbro(), aoo.getPinTimbro(), contentPrincipaleStampigliato, 
										stampigliaturaProtocollo, aoo.isConfPDFAPerHandler());
								LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": stampigliatura protocollo eseguita");
							}
							
							// Postilla
							// La postilla standard:
							// 1) è prevista solo per la firma digitale (NON multipla)
							// 2) viene apposta solo se il content NON è proveniente dall'annotation CONTENT_LIBRO_FIRMA, in caso contrario è già stata inserita
							// dal processo di Trasformazione PDF.
							// 3) viene apposta solo se l'aoo è configurato perché sia attiva
							if (!firmaMultipla && !SignTypeGroupEnum.NON_DIGITALE.equals(ste)
									&& !isContentFromAnnotationContentLibroFirma(documentTitle, documentDetailDTO.getSottoCategoriaUscita(), utente.getIdAoo(), fceh)) {
								// Chiamata al CE volutamente inserita all'interno dell'if qui sopra	
								if(utente.isPostillaAttiva()) {
									byte[] imageFirma = getImageFirmaAOO (utente.getIdAoo(), aoo.isConfPDFAPerHandler());

									contentPrincipaleStampigliato = shTimbro.applicaTimbroPostilla(aoo.getSignerTimbro(), aoo.getPinTimbro(), contentPrincipaleStampigliato, 
											aoo.isConfPDFAPerHandler(), utente.isPostillaSoloPag1(), imageFirma);
								}
								LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": stampigliatura postilla eseguita");
							}
							
							// Approvazioni
							// Si recuperano le approvazioni
							List<String> approvazioni = getApprovazioni(Integer.parseInt(documentDetailDTO.getDocumentTitle()), utente.getIdAoo(), connection);
							String approvazione1 = (approvazioni != null && !approvazioni.isEmpty()) ? approvazioni.get(0) : null;
							String approvazione2 = (approvazioni != null && approvazioni.size() > 1) ? approvazioni.get(1) : null;
							
							if (!StringUtils.isNullOrEmpty(approvazione1)) {
								contentPrincipaleStampigliato = shTimbro.applicaTimbroApprovazione(aoo.getSignerTimbro(), aoo.getPinTimbro(), contentPrincipaleStampigliato, 
										approvazione1, false, aoo.isConfPDFAPerHandler(), true);
								LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": stampigliatura approvazione 1 eseguita");
							}
							
							if (!StringUtils.isNullOrEmpty(approvazione2)) {
								contentPrincipaleStampigliato = shTimbro.applicaTimbroApprovazione(aoo.getSignerTimbro(), aoo.getPinTimbro(), contentPrincipaleStampigliato, 
										approvazione2, true, aoo.isConfPDFAPerHandler(), true);
								LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": stampigliatura approvazione 2 eseguita");
							}
						// Copia Conforme
						} else {
							// Postilla Copia Conforme
							
							if(utente.isPostillaAttiva()) {
								byte[] imageFirma = getImageFirmaAOO (utente.getIdAoo(), aoo.isConfPDFAPerHandler());
								
								contentPrincipaleStampigliato = shTimbro.applicaTimbroPostillaCopiaConforme(aoo.getSignerTimbro(), aoo.getPinTimbro(), contentPrincipaleStampigliato, 
										aoo.isConfPDFAPerHandler(),imageFirma);
							}
							LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": stampigliatura postilla Copia Conforme eseguita");
						}
						
						// ### Upload del content principale stampigliato su FileNet
						documentForDetails = fceh.addVersion(documentForDetails, new ByteArrayInputStream(contentPrincipaleStampigliato), null, null, 
								documentDetailDTO.getNomeFile(), documentDetailDTO.getMimeType(), utente.getIdAoo());
						
						// Inserimento del documento principale tra quelli da rollbackare nell'eventuale recovery dello step di stampigliatura
						((List<String>) context.get(ContextASignEnum.DOC_FN_GUID_LIST)).add(documentForDetails.get_Id().toString());
					}
					// ###### Si stampigliano il protocollo, le approvazioni e la postilla sul documento PRINCIPALE -> END
						
					// ###### Si stampiglia il protocollo sugli ALLEGATI -> START
					// N.B. I segni grafici sono già stati eventualmente apposti dal processo di Trasformazione PDF.
					if (CompoundDocumentState.COMPOUND_DOCUMENT_AS_INT == documentForDetails.get_CompoundDocumentState().getValue()) {
						LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": stampigliatura allegati");
						boolean stampigliato = false;
						boolean formatoOriginale;
						boolean firmatoDigitalmente;
						boolean isTimbroUscita;
						byte[] contentAllegatoStampigliato;
						ContentTransfer ctAllegato;
						Document allegatoStampigliato = null;
						
						stampigliaturaProtocollo = Constants.EMPTY_STRING;
						if (protocolloDaStampigliare) {
							stampigliaturaProtocollo = getStampigliaturaProtocollo(documentDetailDTO, documentForDetails, protocollo, utente, protocolloPresente, false);
						}
						
						Iterator<?> itAllegati = documentForDetails.get_ChildDocuments().iterator();
						while (itAllegati.hasNext()) {
							stampigliato = false;
							Document allegato = (Document) itAllegati.next();
							ctAllegato = FilenetCEHelper.getDocumentContentTransfer(allegato);
							dtAllegato = (String) TrasformerCE.getMetadato(allegato, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);

							contentAllegatoStampigliato = FileUtils.getByteFromInputStream(ctAllegato.accessContentStream());
							firmatoDigitalmente = FormatoAllegatoEnum.FIRMATO_DIGITALMENTE.getId().equals(TrasformerCE.getMetadato(allegato, PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY));
							formatoOriginale = Boolean.TRUE.equals(TrasformerCE.getMetadato(allegato, PropertiesNameEnum.IS_FORMATO_ORIGINALE_METAKEY));
							isTimbroUscita = Boolean.TRUE.equals(TrasformerCE.getMetadato(allegato, PropertiesNameEnum.IS_TIMBRO_USCITA_METAKEY));
							// ## Protocollo
							// Apponi protocollo se:
							// 1) Protocollo da stampigliare
							// 2) Timbro uscita configurato per l'AOO
							// 3) Protocollo presente
							// 4) È in formato PDF
							// 5) Firma autografa oppure PAdES (se digitale/remota)
							// 6) NO flag mantieni formato originale OPPURE è firmato digitalmente
							// 7) L'allegato è marcato con timbro uscita
							// N.B. 1), 2) e 3) implicano stampigliatura di protocollo NON vuota per gli allegati di un documento in uscita
							if (protocolloDaStampigliare && utente.getTimbroUscitaAoo() && Boolean.TRUE.equals(isTimbroUscita) && protocolloPresente && MediaType.PDF.toString().equals(allegato.get_MimeType()) 
									&& !SignTypeGroupEnum.CADES.equals(ste) && (!Boolean.TRUE.equals(formatoOriginale) || firmatoDigitalmente)) {
								contentAllegatoStampigliato = shTimbro.applicaTimbroProtocolloAllegatoUscita(aoo.getSignerTimbro(), aoo.getPinTimbro(), contentAllegatoStampigliato, 
										stampigliaturaProtocollo, aoo.isConfPDFAPerHandler());
								stampigliato = true;
								LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": stampigliatura protocollo su allegato " + allegato.get_Name() + " eseguita");
							}
							
							// ### Upload del content dell'allegato stampigliato su FileNet
							if (stampigliato) {
								allegatoStampigliato = fceh.addVersion(allegato, new ByteArrayInputStream(contentAllegatoStampigliato), null, 
										allegato.get_Name(), allegato.get_MimeType(), utente.getIdAoo());
								
								// Inserimento dell'allegato tra i documenti da rollbackare nell'eventuale recovery dello step di stampigliatura
								((List<String>) context.get(ContextASignEnum.DOC_FN_GUID_LIST)).add(allegatoStampigliato.get_Id().toString());
							}
						}
						// ###### Si stampigliano il protocollo e il campo sigla sugli ALLEGATI -> END
					}
				} else {
					throw new RedException("Il PK Handler Timbro dell'AOO NON è configurato");
				}
			}
			
			impostaEsitoOk(esitoStampigliatura);
			
		} catch (Exception e) {
			gestisciEccezioneStepFirma(e, esitoStampigliatura, SignErrorEnum.STAMPIGLIATURA_STEP_ASIGN_COD_ERROR, dtAllegato);
		} finally {
			popSubject(fceh);
		}
		
		return esitoStampigliatura;
	}
	
	/**
	 * @see it.ibm.red.business.service.ISignSRV#aggiornamentoMetadati(filenet.vw.api.VWWorkObject,
	 *      it.ibm.red.business.dto.UtenteDTO, java.sql.Connection).
	 */
	@Override
	public EsitoOperazioneDTO aggiornamentoMetadati(final VWWorkObject wob, final UtenteDTO utente, final Connection connection) {
		String documentTitle = TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY).toString();
		EsitoOperazioneDTO esitoAggiornamentoMetadati = new EsitoOperazioneDTO(wob.getWorkflowNumber(), documentTitle);
		esitoAggiornamentoMetadati.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
		esitoAggiornamentoMetadati.setEsito(false);
		IFilenetCEHelper fceh = null;

		try {
			LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": aggiornamento dei metadati del documento, delle security per documento e fascicoli e dei fascicoli");
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			
			Document documentForDetails = fceh.getDocumentRedForDetail(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle);
			DetailDocumentoDTO documentDetailDTO = getDocumentDetailDTO(FIRMA_DOCUMENTO_LABEL + documentTitle, documentForDetails, utente, connection);
			
			boolean firmaMultipla = Integer.valueOf(TipoAssegnazioneEnum.FIRMA_MULTIPLA.getId()).equals(TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY));
			
			if (!firmaMultipla) {
				LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": NO firma multipla => si procede con l'aggiornamento dei metadati");

				// Aggiorna metadato versione copia conforme
				aggiornaVersioneCopiaConforme(documentForDetails, fceh, wob);
				
				// Aggiorna security documento e fascicoli
				aggiornaSecurity(utente, wob, documentTitle, documentDetailDTO.getFlagRiservato());
				
				// Aggiorna fascicolo Bilancio Enti
				aggiornaFascicoloBilancioEnti(documentDetailDTO, documentDetailDTO.getProtocollo(), utente.getIdAoo().intValue(), fceh);
			}
			
			impostaEsitoOk(esitoAggiornamentoMetadati);
			
		} catch (Exception e) {
			gestisciEccezioneStepFirma(e, esitoAggiornamentoMetadati, SignErrorEnum.AGGIORNAMENTO_METADATI_STEP_ASIGN_COD_ERROR, null);
		} finally {
			popSubject(fceh);
		}
		
		return esitoAggiornamentoMetadati;
	}
	
	/**
	 * @see it.ibm.red.business.service.ISignSRV#gestioneAllacci(filenet.vw.api.VWWorkObject,
	 *      it.ibm.red.business.dto.UtenteDTO, java.sql.Connection).
	 */
	@Override
	public EsitoOperazioneDTO gestioneAllacci(final VWWorkObject wob, final UtenteDTO utente, final Connection connection) {
		String documentTitle = TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY).toString();
		EsitoOperazioneDTO esitoGestioneAllacci = new EsitoOperazioneDTO(wob.getWorkflowNumber(), documentTitle);
		esitoGestioneAllacci.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
		esitoGestioneAllacci.setEsito(false);
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;

		try {
			LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": gestione degli allacci");
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			
			Document documentForDetails = fceh.getDocumentRedForDetail(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle);
			
			boolean firmaMultipla = Integer.valueOf(TipoAssegnazioneEnum.FIRMA_MULTIPLA.getId()).equals(TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY));
			
			if (!firmaMultipla) {
				LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": NO firma multipla => si procede con la gestione degli allacci");

				fpeh = new FilenetPEHelper(utente.getFcDTO());
				Aoo aoo = aooDAO.getAoo(utente.getIdAoo(), connection);
				
				gestisciAllacci(fceh, fpeh, aoo, documentForDetails, utente, true, connection);
			}
			
			impostaEsitoOk(esitoGestioneAllacci);
			
		} catch (Exception e) {
			gestisciEccezioneStepFirma(e, esitoGestioneAllacci, SignErrorEnum.GESTIONE_ALLACCI_STEP_ASIGN_COD_ERROR, null);
		} finally {
			popSubject(fceh);
			logoff(fpeh);
		}
		
		return esitoGestioneAllacci;
	}
	
	/**
	 * @see it.ibm.red.business.service.ISignSRV#avanzamentoProcessi(filenet.vw.api.VWWorkObject,
	 *      it.ibm.red.business.dto.UtenteDTO, boolean, java.util.Map,
	 *      it.ibm.red.business.helper.filenet.pe.FilenetPEHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public EsitoOperazioneDTO avanzamentoProcessi(final VWWorkObject wob, final UtenteDTO utente, final boolean firmaAutografa, final Map<ContextASignEnum, Object> context, 
			final FilenetPEHelper fpeh, final Connection connection) {
		String documentTitle = TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY).toString();
		EsitoOperazioneDTO esitoAvanzamentoProcessi = new EsitoOperazioneDTO(wob.getWorkflowNumber(), documentTitle);
		esitoAvanzamentoProcessi.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
		esitoAvanzamentoProcessi.setEsito(false);
		IFilenetCEHelper fceh = null;

		try {
			LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": avanzamento dei processi");
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			Document documentForDetails = fceh.getDocumentRedForDetail(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle);
			DetailDocumentoDTO documentDetailDTO = getDocumentDetailDTO(FIRMA_DOCUMENTO_LABEL + documentTitle, documentForDetails, utente, connection);
			PEDocumentoDTO wobDTO = TrasformPE.transform(wob, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO);
			
			final boolean firmaMultipla = Integer.valueOf(TipoAssegnazioneEnum.FIRMA_MULTIPLA.getId()).equals(TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY));
			
			final RispostaAllaccioDTO allaccioPrincipale = allaccioDAO.getAllaccioPrincipale(Integer.parseInt(documentDetailDTO.getDocumentTitle()), connection);
			
			//chiudi il fascicolo se è una mozione oppure se l'allaccio principale non è una richiesta integrazioni
			final boolean chiudiFascicolo = allaccioPrincipale == null || !TipoAllaccioEnum.RICHIESTA_INTEGRAZIONE.equals(allaccioPrincipale.getTipoAllaccioEnum());
			
			if (!firmaMultipla) {
				LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": NO firma multipla => si procede con la gestione completa dei processi");

				// 1) Avanzamento del workflow
				avanzaWFFirma(utente, fpeh, wob, documentTitle, getTipoSpedizione(wobDTO, documentDetailDTO), documentDetailDTO.getFlagRiservato(), 
						fceh, connection, wobDTO.getIdFascicolo(), !documentDetailDTO.isDestinatariElettronici(), firmaMultipla, firmaAutografa,
						chiudiFascicolo);
				
				// 2) Gestione dei destinatari interni
				gestisciDestinatariInterni(documentForDetails, documentDetailDTO, documentDetailDTO.getProtocollo(), utente, context, fceh, connection);
				
				// Se è un flusso AUT si chiude l'eventuale notifica associata al documento in uscita
				final Aoo aoo = aooDAO.getAoo(utente.getIdAoo(), connection);
				boolean isFlussoAUT = false;
				String codiceFlusso = null;
				if (allaccioPrincipale != null) {
					final IFilenetCEHelper fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()),utente.getIdAoo());
					final List<String> props = Arrays.asList(pp.getParameterByKey(PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY));
					final Document document = fcehAdmin.getDocumentByIdGestionale(allaccioPrincipale.getIdDocumentoAllacciato(), props, null, utente.getIdAoo().intValue(), null, null);
					codiceFlusso = TrasformerCE.getMetadato(document, 
							PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY) != null ? TrasformerCE.getMetadato(document, PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY).toString() : "";
				
					LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": " + CODICE_FLUSSO_LABEL + codiceFlusso + ALLACCIO_PRINCIPALE_LABEL
							+ allaccioPrincipale.getIdDocumentoAllacciato());
					
					isFlussoAUT = TipoContestoProceduraleEnum.FLUSSO_AUT.getId().equals(codiceFlusso);
					popSubject(fcehAdmin);
				}
				
				if (isFlussoAUT && Boolean.TRUE.equals(allaccioPrincipale.getDocumentoDaChiudere())) {
					mettiAgliAttiNotifica(documentDetailDTO.getProtocollo(), allaccioPrincipale, aoo, utente, fpeh, fceh, connection);
				}
				
			
			// Altrimenti, ci si limita a gestire il workflow
			} else {
				
				// Avanzamento del workflow
				avanzaWFFirma(utente, fpeh, wob, documentTitle, getTipoSpedizione(wobDTO, documentDetailDTO), documentDetailDTO.getFlagRiservato(), 
						fceh, connection, wobDTO.getIdFascicolo(), !documentDetailDTO.isDestinatariElettronici(), firmaMultipla, firmaAutografa, chiudiFascicolo);
			
			}
			
			impostaEsitoOk(esitoAvanzamentoProcessi);
			
		} catch (Exception e) {
			gestisciEccezioneStepFirma(e, esitoAvanzamentoProcessi, SignErrorEnum.AVANZAMENTO_PROCESSI_STEP_ASIGN_COD_ERROR, null);
		} finally {
			popSubject(fceh);
		}

		return esitoAvanzamentoProcessi;
	}
	
	/**
	 * @see it.ibm.red.business.service.ISignSRV#allineamentoNPS(it.ibm.red.business.dto.ASignItemDTO,
	 *      it.ibm.red.business.dto.UtenteDTO, java.sql.Connection).
	 */
	@Override
	public EsitoOperazioneDTO allineamentoNPS(final ASignItemDTO aSignItem, final UtenteDTO utente, final Connection connection) {
		String documentTitle = aSignItem.getDocumentTitle();
		EsitoOperazioneDTO esitoAllineamentoNPS = new EsitoOperazioneDTO(aSignItem.getWobNumber(), documentTitle);
		esitoAllineamentoNPS.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
		esitoAllineamentoNPS.setEsito(false);
		IFilenetCEHelper fceh = null;

		try {
			LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": allineamento di NPS");
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			
			boolean firmaAutografa = SignModeEnum.AUTOGRAFA.equals(aSignItem.getSignMode());
			SignTypeGroupEnum ste = aSignItem.getSignType();
			
			// Si recuperano i dati del workflow memorizzati nel DB
			Map<String, String> datiWorkflow = aSignDAO.getItemData(connection, aSignItem.getId());
			boolean firmaMultipla = String.valueOf(TipoAssegnazioneEnum.FIRMA_MULTIPLA.getId()).equals(
					datiWorkflow.get(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY)));
			
			// N.B. Tutte le operazioni di allineamento di NPS sono effettuate in modalità asincrona,
			// tramite molteplici scrittura sulla coda NPS_ASYCREQUEST_EVO e il loro successivo processamento da parte di un apposito job
			if (TipologiaProtocollazioneEnum.isProtocolloNPS(utente.getIdTipoProtocollo()) && !firmaMultipla) {
				LOGGER.info(FIRMA_DOCUMENTO_LABEL + documentTitle + ": allineamento di NPS => si procede con l'allineamento");
				Document documentForDetails = fceh.getDocumentRedForDetail(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle);
				DetailDocumentoDTO documentDetailDTO = getDocumentDetailDTO(FIRMA_DOCUMENTO_LABEL + documentTitle, documentForDetails, utente, connection);
				
				ProtocolloDTO protocollo = documentDetailDTO.getProtocollo();				
				boolean flussoAUT = isFamigliaFlussoAUT(getCodiceFlussoDocumentoFirma(FIRMA_DOCUMENTO_LABEL + documentTitle, documentTitle, utente, connection));
				boolean inoltroAttoDecretoUCB = checkIsInoltroAttoDecretoUCB(documentDetailDTO.getIdTipologiaDocumento(), utente.isUcb(), utente.getCodiceAoo());
				
				// Se flusso AUT/CG2 o Atto Decreto UCB, l'upload dei content su NPS avviene in modalità sincrona all'atto della firma dei content
				if (!(flussoAUT || inoltroAttoDecretoUCB)) {
					
					final boolean protocollazioneAnticipata = MomentoProtocollazioneEnum.PROTOCOLLAZIONE_NON_STANDARD.getIdAsInteger().equals(documentDetailDTO.getIdMomentoProtocollazione());
					String guidFirstVersionDocPrincipale = null;
					DocTipoOperazioneType tipoOperazioneEnum = null;
					if(protocollazioneAnticipata) {
						guidFirstVersionDocPrincipale = fceh.getFirstVersionById(documentDetailDTO.getDocumentTitle(), utente.getIdAoo().intValue()).get_Id().toString(); 
						tipoOperazioneEnum = DocTipoOperazioneType.COLLEGA_DOCUMENTO;
					}
					
					// ### Si invia il content firmato e stampigliato del documento principale e lo si associa al protocollo su NPS
					uploadAssociazioneProtocolloToAsyncNps(protocollo.getIdProtocollo(), FilenetCEHelper.getDocumentContentAsInputStream(documentForDetails), documentDetailDTO.getGuid(),
							documentDetailDTO.getMimeType(), documentDetailDTO.getNomeFile(), documentDetailDTO.getOggetto(), utente, true, 
							protocollo.getNumeroAnnoProtocolloNPS(utente.getCodiceAoo()), documentDetailDTO.getDocumentTitle(), connection,
							guidFirstVersionDocPrincipale, tipoOperazioneEnum);
					
					// ### Si inviano gli allegati e li si associa al protocollo su NPS
					inviaAllegatiToNPS(documentForDetails.get_Id().toString(), documentDetailDTO, ste, firmaAutografa, false, false, false, utente, fceh, connection);
					
				}
				
				// Se ho staccato la registrazione ausiliaria, si collega il protocollo alla registrazione su NPS
				if (documentDetailDTO.getRegistrazioneAusiliaria() != null) {
					associaRegAuxProtUscitaNPS(documentDetailDTO.getRegistrazioneAusiliaria(), protocollo.getIdProtocollo(), utente, 
							protocollo.getNumeroAnnoProtocolloNPS(utente.getCodiceAoo()), documentTitle, connection);
				}
				
				// Si inviano gli allacci associandoli al protocollo su NPS
				inviaAllacciToNPS(documentForDetails, documentTitle, utente, connection);
			}
			
			impostaEsitoOk(esitoAllineamentoNPS);
			
		} catch (Exception e) {
			gestisciEccezioneStepFirma(e, esitoAllineamentoNPS, SignErrorEnum.ALLINEAMENTO_NPS_STEP_ASIGN_COD_ERROR, null);
		} finally {
			popSubject(fceh);
		}

		return esitoAllineamentoNPS;
	}
	
	/**
	 * @see it.ibm.red.business.service.ISignSRV#protocollaRegAux(filenet.vw.api.VWWorkObject,
	 *      it.ibm.red.business.dto.UtenteDTO, java.sql.Connection).
	 */
	@Override
	public EsitoOperazioneDTO protocollaRegAux(String signTransactionId, VWWorkObject wob, UtenteDTO utente, Connection connection) {
		String documentTitle = TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY).toString();
		EsitoOperazioneDTO esitoRegistrazioneAusiliaria = new EsitoOperazioneDTO(wob.getWorkflowNumber(), documentTitle);
		
		// Impostazione esito registrazione ausiliaria iniziale
		esitoRegistrazioneAusiliaria.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
		esitoRegistrazioneAusiliaria.setEsito(false);
		IFilenetCEHelper fceh = null;
		Document documentForDetails = null;
		DetailDocumentoDTO documentDetailDTO = null;
		String logId = signTransactionId + documentTitle;
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			documentForDetails = fceh.getDocumentRedForDetail(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle);
			LOGGER.info(logId + " recuperato documento su Filenet");
			documentDetailDTO = getDocumentDetailDTO(logId, documentForDetails, utente, connection);
			
			// Impostazione dettagli esito
			esitoRegistrazioneAusiliaria.setIdDocumento(documentDetailDTO.getNumeroDocumento());
			esitoRegistrazioneAusiliaria.setNomeDocumento(documentDetailDTO.getNomeFile());

			// Comunicazione con NPS e memorizzazione informazioni registrazione ausiliaria su database
			protocollaRegistrazioneAusiliaria(logId, wob, utente, fceh, documentDetailDTO, connection);
			
			impostaEsitoOk(esitoRegistrazioneAusiliaria, documentDetailDTO.getProtocollo(), documentDetailDTO.getIsRegistroRepertorio());
		} catch (Exception e) {
			gestisciEccezioneStepFirma(e, esitoRegistrazioneAusiliaria, SignErrorEnum.REG_AUX_PROTOCOLLAZIONE_STEP_ASIGN_COD_ERROR, null);
			try {
				aggiornaMetadatoTrasformazione(logId, documentTitle, utente, TrasformazionePDFInErroreEnum.WARN_PROTO.getValue());
			}catch(Exception ex) {
				LOGGER.error("Errore nell'aggiornamento del SEMAFORO Registrazione ausiliaria", ex);
			}
		} finally {
			popSubject(fceh);
		}

		return esitoRegistrazioneAusiliaria;
	}

	/**
	 * @see it.ibm.red.business.service.ISignSRV#recuperaContentDaVerificareNew(it.ibm.red.business.persistence.model.Aoo).
	 */
	@Override
	public List<VerificaFirmaItem> recuperaContentDaVerificareNew(final Aoo aoo) {
		List<VerificaFirmaItem> verificaFirmaItem = null;
		Connection connection = null;
		try {

			connection = setupConnection(getDataSource().getConnection(), true);
			verificaFirmaItem = verificaDAO.getDocumentToVerify(connection, aoo.getIdAoo().intValue(), getVerificaFirmaMaxItems(), aoo.getVerificaFirmaMinutiDelay());

			final List<Integer> idDaLockare = new ArrayList<>();

			for (final VerificaFirmaItem item : verificaFirmaItem) {
				idDaLockare.add(item.getNextVal());
			}

			verificaDAO.updateStatoDocumentTitleVerificato(idDaLockare, StatoVerificaFirmaEnum.IN_LAVORAZIONE.getStatoVerificaFirma(), connection);
			commitConnection(connection);
		} catch (final Exception e) {
			rollbackConnection(connection);
			LOGGER.error(GENERIC_ERROR_RECUPERO_CONTENT_DA_VERIFICARE_MSG, e);
			throw new RedException(GENERIC_ERROR_RECUPERO_CONTENT_DA_VERIFICARE_MSG, e);
		} finally {
			closeConnection(connection);
		}

		return verificaFirmaItem;

	}

	/**
	 * @see it.ibm.red.business.service.ISignSRV#updateStatoDocumentTitleVerificato(java.util.List,
	 *      java.lang.Integer).
	 */
	@Override
	public boolean updateStatoDocumentTitleVerificato(final List<Integer> idDocElaborati, final Integer stato) {
		boolean output = false;
		Connection conn = null;
		try {

			conn = setupConnection(getDataSource().getConnection(), true);
			output = verificaDAO.updateStatoDocumentTitleVerificato(idDocElaborati, stato, conn);
			commitConnection(conn);
		} catch (final Exception ex) {
			rollbackConnection(conn);
			LOGGER.error("Errore nell'aggiornamento dello stato del doc title verificato : "+ex);
			throw new RedException("Errore nell'aggiornamento dello stato del doc title verificato : "+ex);
		} finally {
			closeConnection(conn);
		}
		return output;

	}
	
	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#sonoPresentiAllegatiFirmatiNonVerificati(java.lang.Long,
	 *      java.lang.String, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public boolean sonoPresentiAllegatiFirmatiNonVerificati (final Long idAoo, final String guid, final UtenteDTO utente) {
		boolean sonoPresentiAllegatiFirmatiNonVerificati = false; 
		IFilenetCEHelper fceh = null;   
		try { 
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), idAoo); 
			sonoPresentiAllegatiFirmatiNonVerificati = fceh.sonoPresentiAllegatiFirmatiNonVerificati(guid);
		}catch (Exception ex) {
			LOGGER.error("Errore durante la verifica firma del doc principale : "+ex);
			throw new RedException("Errore durante la verifica firma del doc principale , nessun content : "+ex);
		} finally {
			popSubject(fceh);
		}
		
		return sonoPresentiAllegatiFirmatiNonVerificati;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#getMetadatoVerificaFirmaPrincAllegato(java.lang.Long,
	 *      java.lang.String, it.ibm.red.business.dto.UtenteDTO, boolean,
	 *      it.ibm.red.business.dto.MasterDocumentRedDTO).
	 */
	@Override
	public List<VerificaFirmaPrincipaleAllegatoDTO> getMetadatoVerificaFirmaPrincAllegato(final Long idAoo,final String documentTitle,final UtenteDTO utente, final boolean hasAllegati,MasterDocumentRedDTO dto) {
		IFilenetCEHelper fceh = null;
		List<VerificaFirmaPrincipaleAllegatoDTO> listVerFirmaPrincAllegato;

		try {
			listVerFirmaPrincAllegato = new ArrayList<>();
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), idAoo); 
			
			//Doc Principale
			Document docPrincipale = fceh.getDocumentByDTandAOOForVerificaFirma(documentTitle, idAoo,dto.getVersion());
			String nomeFile = docPrincipale.getProperties().getStringValue((pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY)));
			String mymeType = docPrincipale.getProperties().getStringValue((pp.getParameterByKey(PropertiesNameEnum.MIMETYPE_FN_METAKEY)));
			String codiceFlusso = docPrincipale.getProperties().getStringValue((pp.getParameterByKey(PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY)));
			Integer metadatoDocumentoValiditaFirma = docPrincipale.getProperties().getInteger32Value((pp.getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA)));
			ValueMetadatoVerificaFirmaEnum valueMetadatoEnum = null;
			if(metadatoDocumentoValiditaFirma!=null) {
				valueMetadatoEnum = ValueMetadatoVerificaFirmaEnum.get(metadatoDocumentoValiditaFirma); 
			}
			
			if(!MediaType.PDF.toString().equalsIgnoreCase(mymeType)&& !documentManagerSRV.isVerificaFirmaDocPrincipaleVisible(mymeType)) {
				valueMetadatoEnum = ValueMetadatoVerificaFirmaEnum.FIRMA_NON_PRESENTE;
				Map<String, Object> mapToUpdate = new HashMap<>();
				mapToUpdate.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA), ValueMetadatoVerificaFirmaEnum.FIRMA_NON_PRESENTE.getValue());
				fceh.updateMetadati(docPrincipale, mapToUpdate); 
			}
			
			if(valueMetadatoEnum==null  || ValueMetadatoVerificaFirmaEnum.FIRMA_NON_VERIFICATA.equals(valueMetadatoEnum)) {
				//Se è null verifichi solo il principale.
				valueMetadatoEnum = verificaFirma(utente, fceh, docPrincipale);  
			}
			final VerificaFirmaPrincipaleAllegatoDTO verFirmaPrincDTO = new VerificaFirmaPrincipaleAllegatoDTO(nomeFile, documentTitle, true, valueMetadatoEnum);
			verFirmaPrincDTO.setFirmeValidePrincAllegato(ValueMetadatoVerificaFirmaEnum.FIRMA_OK.equals(valueMetadatoEnum));
			listVerFirmaPrincAllegato.add(verFirmaPrincDTO);
			if(valueMetadatoEnum != null) {
				dto.setMetadatoValiditaFirmaPrincipale(valueMetadatoEnum.getValue());
			} else {
				dto.setMetadatoValiditaFirmaPrincipale(null);
			}
			dto.setFirmaValidaPrincipale(null);
			if(valueMetadatoEnum!=null) {
				if(ValueMetadatoVerificaFirmaEnum.FIRMA_OK.equals(valueMetadatoEnum)) {
					dto.setFirmaValidaPrincipale(true);
				}else if(ValueMetadatoVerificaFirmaEnum.FIRMA_KO.equals(valueMetadatoEnum)){
					dto.setFirmaValidaPrincipale(false);
				}
				
			}
				
			if(hasAllegati) {	 
				//Allegati
				Boolean firmeValidaAll = null;
				final DocumentSet allegati = fceh.getAllegati(docPrincipale.get_Id().toString(), false, false);
				if (!allegati.isEmpty()) {
					metadatoDocumentoValiditaFirma = null;
					final Iterator<?> itAllegati = allegati.iterator();
					while (itAllegati.hasNext()) {
						Document allegato = (Document) itAllegati.next();
						String nomeFileAll = (String) TrasformerCE.getMetadato(allegato, PropertiesNameEnum.NOME_FILE_METAKEY);
						String mymeTypeAll = allegato.getProperties().getStringValue((pp.getParameterByKey(PropertiesNameEnum.MIMETYPE_FN_METAKEY)));
						String docTitleAll = (String) TrasformerCE.getMetadato(allegato, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
						metadatoDocumentoValiditaFirma = (Integer) TrasformerCE.getMetadato(allegato, PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA);
						valueMetadatoEnum = null;

						if (metadatoDocumentoValiditaFirma != null) {
							valueMetadatoEnum = ValueMetadatoVerificaFirmaEnum.get(metadatoDocumentoValiditaFirma);
						}
						
						if((!MediaType.PDF.toString().equalsIgnoreCase(mymeTypeAll) && !documentManagerSRV.isVerificaFirmaDocPrincipaleVisible(mymeTypeAll))
								&& !(Constants.ContentType.TEXT_XML.equals(mymeTypeAll) && nomeFileAll.endsWith(".xml") && Constants.Varie.CODICE_FLUSSO_SIPAD.equals(codiceFlusso) 
										&& !Constants.Varie.SEGNATURA_FILENAME.equals(nomeFileAll))) {
							valueMetadatoEnum = ValueMetadatoVerificaFirmaEnum.FIRMA_NON_PRESENTE;
						}
						
						if(valueMetadatoEnum==null  || ValueMetadatoVerificaFirmaEnum.FIRMA_NON_VERIFICATA.equals(valueMetadatoEnum)) {  
							valueMetadatoEnum = verificaFirma(utente, fceh, allegato);  
						} 
						VerificaFirmaPrincipaleAllegatoDTO verFirmaAllDTO = new VerificaFirmaPrincipaleAllegatoDTO(nomeFileAll,docTitleAll, false, valueMetadatoEnum);
						verFirmaAllDTO.setFirmeValidePrincAllegato(ValueMetadatoVerificaFirmaEnum.FIRMA_OK.equals(valueMetadatoEnum));
						listVerFirmaPrincAllegato.add(verFirmaAllDTO);
						
						if(ValueMetadatoVerificaFirmaEnum.FIRMA_KO.equals(valueMetadatoEnum)) {
							firmeValidaAll = false;
						}
						// se è false non va piu modificato 
						if(firmeValidaAll ==null && ValueMetadatoVerificaFirmaEnum.FIRMA_OK.equals(valueMetadatoEnum)) {
							firmeValidaAll = ValueMetadatoVerificaFirmaEnum.FIRMA_OK.equals(valueMetadatoEnum);
						}
					}
					// Aggiorna metadato valido su filenet
					final Map<String, Object> metadati = new HashMap<>();
					metadati.put(pp.getParameterByKey(PropertiesNameEnum.VERIFICA_FIRMA_DOC_ALLEGATO), firmeValidaAll);
					fceh.updateMetadati(docPrincipale, metadati);	
					dto.setFirmaValidaAllegati(firmeValidaAll);
					
				}   else {
					//docPrincipale
					//Aggiorna metadato  valido su filenet
					Map<String, Object> metadati = new HashMap<>();
					metadati.put(pp.getParameterByKey(PropertiesNameEnum.VERIFICA_FIRMA_DOC_ALLEGATO), null);
					fceh.updateMetadati(docPrincipale, metadati);		
					dto.setFirmaValidaAllegati(null);
				}

			} else {
				// Se non ho allegati la verifica degli allegati è true
				final Map<String, Object> metadati = new HashMap<>();
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.VERIFICA_FIRMA_DOC_ALLEGATO), null);
				fceh.updateMetadati(docPrincipale, metadati);
				dto.setFirmaValidaAllegati(null);
			}
		} catch (final Exception ex) {
			LOGGER.error("Errore durante la verifica firma del doc principale : " + ex);
			throw new RedException("Errore durante la verifica firma del doc principale , nessun content : " + ex);
		} finally {
			popSubject(fceh);
		}

		return listVerFirmaPrincAllegato;
	}

	/**
	 * @param utente
	 * @param fceh
	 * @param allegato
	 * @return stato della firma
	 */
	private ValueMetadatoVerificaFirmaEnum verificaFirma(final UtenteDTO utente, IFilenetCEHelper fceh, Document allegato) {
		ValueMetadatoVerificaFirmaEnum valueMetadatoEnum = ValueMetadatoVerificaFirmaEnum.FIRMA_NON_VERIFICATA;
		InputStream docContentAll = null;
		try {
			ContentElementList ceList = allegato.get_ContentElements();
			ContentTransfer ct = (ContentTransfer) ceList.get(0);
			docContentAll = ct.accessContentStream();
			VerifyInfoDTO verificaDTO = aggiornaVerificaFirma(allegato, docContentAll, fceh, utente);

			if (verificaDTO == null) {
				throw new RedException("Informazioni sulla verifica della firma non correttamente recuperate.");
			}

			valueMetadatoEnum = ValueMetadatoVerificaFirmaEnum.get(verificaDTO.getMetadatoValiditaFirmaValue());
		} catch (Exception e) {
			LOGGER.warn(DOCUMENTO_LABEL + allegato.get_Id().toString() + " senza content", e);
			valueMetadatoEnum = ValueMetadatoVerificaFirmaEnum.FIRMA_NON_VERIFICATA;
		}
		return valueMetadatoEnum;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#aggiornaVerificaFirmaDocAllegato(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      com.filenet.api.core.Document).
	 */
	@Override
	public void aggiornaVerificaFirmaDocAllegato(final IFilenetCEHelper fceh, final Document docPrincipale) {
		if (docPrincipale != null) {
			final Integer attachment = docPrincipale.get_CompoundDocumentState().getValue();
			final boolean bAttachmentPresent = attachment > 0;
			if (bAttachmentPresent) {
				final DocumentSet allegati = fceh.getAllegati(docPrincipale.get_Id().toString(), false, false);
				if (!allegati.isEmpty()) {
					final Iterator<?> it = allegati.iterator();
					Boolean firmeValidaAll = null;
					while (it.hasNext()) {
						final Document allegato = (Document) it.next();
						final Integer metadatoDocumentoValiditaFirma = (Integer) TrasformerCE.getMetadato(allegato, PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA);
						ValueMetadatoVerificaFirmaEnum valueMetadatoEnum = null;
							//questo metodo è pensato per il batch quindi non verifica la firma degli allegati, 
						//perchè gli allegati saranno aggiornati dal batch quando sarà il loro turno.
						
						if (metadatoDocumentoValiditaFirma != null) {
							valueMetadatoEnum = ValueMetadatoVerificaFirmaEnum.get(metadatoDocumentoValiditaFirma);
						}
						if (ValueMetadatoVerificaFirmaEnum.FIRMA_KO.equals(valueMetadatoEnum)) {
							firmeValidaAll = false;
						}
						// se è false non va piu modificato
						if (firmeValidaAll == null && ValueMetadatoVerificaFirmaEnum.FIRMA_OK.equals(valueMetadatoEnum)) {
							firmeValidaAll = ValueMetadatoVerificaFirmaEnum.FIRMA_OK.equals(valueMetadatoEnum);
						}
					}
					// Aggiorna metadato non valido su filenet
					final Map<String, Object> metadati = new HashMap<>();
					metadati.put(pp.getParameterByKey(PropertiesNameEnum.VERIFICA_FIRMA_DOC_ALLEGATO), firmeValidaAll);
					fceh.updateMetadati(docPrincipale, metadati);
				} else {
					// docPrincipale
					// Aggiorna metadato valido su filenet
					final Map<String, Object> metadati = new HashMap<>();
					metadati.put(pp.getParameterByKey(PropertiesNameEnum.VERIFICA_FIRMA_DOC_ALLEGATO), null);
					fceh.updateMetadati(docPrincipale, metadati);
				}
			} else {
				// Se non ho allegati la verifica degli allegati è true
				final Map<String, Object> metadati = new HashMap<>();
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.VERIFICA_FIRMA_DOC_ALLEGATO), null);
				fceh.updateMetadati(docPrincipale, metadati);
			}
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISignFacadeSRV#verificaFirmaAllegatoVisualizzaComponent(java.lang.Long,
	 *      java.lang.String, it.ibm.red.business.dto.FilenetCredentialsDTO).
	 */
	@Override
	public void verificaFirmaAllegatoVisualizzaComponent(final Long idAoo, final String guid, final FilenetCredentialsDTO fcDTO) {
		IFilenetCEHelper fceh = null;
		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);
			final Document docPrincipale = fceh.getDocumentForAllegato(guid, false);
			aggiornaVerificaFirmaDocAllegato(fceh, docPrincipale);
		} catch (final Exception ex) {
			LOGGER.error("Errore durante la verifica firma dell'allegato : " + ex);
			throw new RedException("Errore durante la verifica firma dell'allegato : " + ex);
		} finally {
			popSubject(fceh);
		}
	}
	
	private static boolean isFamigliaFlussoAUT(final String codiceFlusso) {
		TipoContestoProceduraleEnum tcp = TipoContestoProceduraleEnum.getEnumById(codiceFlusso);
		return tcp != null && tcp.isFamigliaFlussiAut();
	}
	
	/**
	 * @see it.ibm.red.business.service.ISignSRV#rigeneraContentRegAux(filenet.vw.api.VWWorkObject,
	 *      it.ibm.red.business.dto.UtenteDTO, java.sql.Connection).
	 */
	@Override
	public EsitoOperazioneDTO rigeneraContentRegAux(final String signTransactionId, final VWWorkObject wob, final UtenteDTO utente, final Connection connection) {
		final String documentTitle = TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY).toString();
		final EsitoOperazioneDTO esitoRegistrazioneAusiliaria = new EsitoOperazioneDTO(wob.getWorkflowNumber(), documentTitle);
		
		// Impostazione esito registrazione ausiliaria iniziale
		esitoRegistrazioneAusiliaria.setCodiceErrore(SignErrorEnum.GENERIC_COD_ERROR);
		esitoRegistrazioneAusiliaria.setEsito(false);
		IFilenetCEHelper fceh = null;
		Document documentForDetails = null;
		DetailDocumentoDTO documentDetailDTO = null;
		String logId = signTransactionId + documentTitle;
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			documentForDetails = fceh.getDocumentRedForDetail(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle);
			LOGGER.info(logId + " recuperato documento su Filenet");
			documentDetailDTO = getDocumentDetailDTO(logId, documentForDetails, utente, connection);
			
			// Impostazione dettagli esito
			esitoRegistrazioneAusiliaria.setIdDocumento(documentDetailDTO.getNumeroDocumento());
			esitoRegistrazioneAusiliaria.setNomeDocumento(documentDetailDTO.getNomeFile());
			
			// Rigenerazione content in seguito alla registrazione ausiliaria, viene eseguito solo se il numero protocollo è già stato staccato e il content non è mai stato rigenerato
			rigeneraContentRegistrazioneAusiliaria(logId, wob, utente, fceh, documentForDetails, documentDetailDTO, connection);
			impostaEsitoOk(esitoRegistrazioneAusiliaria, documentDetailDTO.getProtocollo(), documentDetailDTO.getIsRegistroRepertorio());
		} catch (Exception e) {
			gestisciEccezioneStepFirma(e, esitoRegistrazioneAusiliaria, SignErrorEnum.REG_AUX_RIGENERAZIONE_CONTENT_STEP_ASIGN_COD_ERROR, null);
		}
		
		return esitoRegistrazioneAusiliaria;
	}
	
	
	/**
	 * @param documentDetailDTO
	 * @param documentForDetails
	 * @param protocollo
	 * @param utente
	 * @param protocolloPresente
	 * @param isPrincipale
	 * @return
	 */
	private String getStampigliaturaProtocollo(final DetailDocumentoDTO documentDetailDTO, final Document documentForDetails, final ProtocolloDTO protocollo, final UtenteDTO utente, 
			final boolean protocolloPresente, final boolean isPrincipale) {
		String dataProtocolloStr = protocollo.getDataProtocollo() != null ? DateUtils.dateToString(protocollo.getDataProtocollo(), true) : Constants.EMPTY_STRING;
		Long idTipoProtocolloPerStampigliatura = utente.getIdTipoProtocollo();
		int numeroDocDaStampigliare = 0;
		
		if (!protocolloPresente) {
			if (isPrincipale) {
				Integer numeroProtocolloEmergenza = (Integer) TrasformerCE.getMetadato(documentForDetails, PropertiesNameEnum.NUMERO_PROTOCOLLO_EMERGENZA_METAKEY);
				if (numeroProtocolloEmergenza != null && numeroProtocolloEmergenza > 0) {
					idTipoProtocolloPerStampigliatura = TipologiaProtocollazioneEnum.EMERGENZANPS.getId();
				}
				
				// Se il protocollo non è stato staccato ma il documento ha destinatari TUTTI interni, 
				// si deve comunque stampigliare il numero del documento al posto del numero di protocollo
				if (documentDetailDTO.isHasDestinatariInterni() && !documentDetailDTO.isHasDestinatariEsterni() && !Boolean.TRUE.equals(documentDetailDTO.getIsRegistroRepertorio())) {
					numeroDocDaStampigliare = documentDetailDTO.getNumeroDocumento();
				}
			}
		} else {
			numeroDocDaStampigliare = protocollo.getNumeroProtocollo();
		}
		
		return PdfHelper.getStampigliaturaProtocollo(isPrincipale, documentDetailDTO.getIdCategoriaDocumento(), protocollo.getAnnoProtocolloInt(), 
				numeroDocDaStampigliare, dataProtocolloStr, false, utente.getCodiceAoo(), utente.getAooDesc(), idTipoProtocolloPerStampigliatura, 
				documentDetailDTO.getIsRegistroRepertorio(), utente.getTimbroUscitaAoo(), documentDetailDTO.getLabelStampigliaturaRR());
	}
	
	
	/**
	 * @param documentForDetails
	 * @param utente
	 * @param connection
	 * @return
	 */
	private DetailDocumentoDTO getDocumentDetailDTO(String signTransactionId, Document documentForDetails, UtenteDTO utente, Connection connection) {
		DetailDocumentoDTO documentDetailDTO = TrasformCE.transform(documentForDetails,	TrasformerCEEnum.FROM_DOCUMENTO_TO_DETAIL);
		
		LOGGER.info(signTransactionId + " attività trasformer completate");
		
		documentDetailDTO.setContent(FilenetCEHelper.getDocumentContentAsByte(documentForDetails));
		
		LOGGER.info(signTransactionId + " recuperato content su Filenet");

		// ### Gestione e controllo della configurazione dei Registri Repertorio
		// Verifica se è un registro di repertorio e nel caso esegue il set della descrizione trovata.
		checkIsRegistroRepertorio(utente, documentDetailDTO, connection);
		
		LOGGER.info(signTransactionId + " verificata l'appartenenza ad un registro di repertorio");
		
		return documentDetailDTO;
	}
	
	
	/**
	 * @param signTransactionId
	 * @param documentTitle
	 * @param idAoo
	 * @param fceh
	 * @param connection
	 * @return
	 */
	private String getCodiceFlussoDocumentoFirma(String signTransactionId, String documentTitle, UtenteDTO utente, Connection connection) {
		String codiceFlusso = null;

		RispostaAllaccioDTO allaccioPrincipale = allaccioDAO.getAllaccioPrincipale(Integer.parseInt(documentTitle), connection);
		
		LOGGER.info(signTransactionId + ": recuperato l'allaccio principale dalla base dati");
		
		if (allaccioPrincipale != null) {
			IFilenetCEHelper fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());
			List<String> props = Arrays.asList(pp.getParameterByKey(PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY));
			Document document = fcehAdmin.getDocumentByIdGestionale(allaccioPrincipale.getIdDocumentoAllacciato(), props, null, utente.getIdAoo().intValue(), null, null);
			LOGGER.info(signTransactionId + ": recuperato l'allaccio principale su Filenet");
			codiceFlusso = TrasformerCE.getMetadato(document, PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY) != null ? TrasformerCE.getMetadato(document, PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY).toString() : "";
			if (!StringUtils.isNullOrEmpty(codiceFlusso)) {
				LOGGER.info(signTransactionId + " codiceFlusso " + codiceFlusso + ", allaccio principale " + allaccioPrincipale.getIdDocumentoAllacciato());
			}
			popSubject(fcehAdmin);
		}
		
		return codiceFlusso;
	}
	
	
	/**
	 * @param guidDocumentoPrincipale
	 * @param documentDetailDTO
	 * @param ste
	 * @param firmaAutografa
	 * @param firmaMultipla
	 * @param flussoAUT
	 * @param inoltroAttoDecretoUCB
	 * @param utente
	 * @param fceh
	 * @param connection
	 */
	private void inviaAllegatiToNPS(String guidDocumentoPrincipale, DetailDocumentoDTO documentDetailDTO, SignTypeGroupEnum ste, boolean firmaAutografa, boolean firmaMultipla, 
			boolean flussoAUT, boolean inoltroAttoDecretoUCB, UtenteDTO utente, IFilenetCEHelper fceh, Connection connection) {
		// Recupera allegati del documento principale
		DocumentSet allegati = fceh.getAllegatiConContent(StringUtils.cleanGuidToString(fceh.idFromGuid(guidDocumentoPrincipale)));
		
		if (allegati != null && !allegati.isEmpty()) {
			boolean firmatoDigitalmente;
			boolean formatoOriginale;
			boolean stampigliaSigla;
			boolean siglaStampigliata;
			boolean protocolloStampigliato;
			boolean firmato;
			
			ProtocolloDTO protocollo = documentDetailDTO.getProtocollo();
			boolean protocolloPresente = protocollo != null && protocollo.getNumeroProtocollo() > 0;
			boolean protocollazioneAnticipata = MomentoProtocollazioneEnum.PROTOCOLLAZIONE_NON_STANDARD.getIdAsInteger().equals(documentDetailDTO.getIdMomentoProtocollazione());
			boolean protocolloDaStampigliare = !(flussoAUT || inoltroAttoDecretoUCB) && (documentDetailDTO.getIsRegistroRepertorio() 
					|| (documentDetailDTO.isHasDestinatariEsterni() && !protocollazioneAnticipata));
			
			Map<String, byte[]>  glifoSiglaMap = null;			
			if (SignTypeGroupEnum.PADES.equals(ste) && !firmaMultipla) {
				glifoSiglaMap = stampigliaturaSegnoGraficoDAO.getPlaceholderSegnoGrafico(utente.getId(), 
						documentDetailDTO.getIdUfficioCreatore().longValue(), documentDetailDTO.getIdTipologiaDocumento().intValue(), connection);
			}
			
			Iterator<?> it = allegati.iterator();
			while (it.hasNext()) {
				Document allegato = (Document) it.next();
				String idDocumentoAllegato = allegato.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
				try {

					firmatoDigitalmente = FormatoAllegatoEnum.FIRMATO_DIGITALMENTE.getId().equals(TrasformerCE.getMetadato(allegato, PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY));
					formatoOriginale = Boolean.TRUE.equals(TrasformerCE.getMetadato(allegato, PropertiesNameEnum.IS_FORMATO_ORIGINALE_METAKEY));
					stampigliaSigla = Boolean.TRUE.equals(TrasformerCE.getMetadato(allegato, PropertiesNameEnum.STAMPIGLIATURA_SIGLA_ALLEGATO));
					
					firmato = !firmaAutografa && FilenetCEHelper.isDaFirmare(allegato);
					siglaStampigliata = stampigliaSigla && MediaType.PDF.toString().equals(allegato.get_MimeType()) && !formatoOriginale && !firmatoDigitalmente 
							&& SignTypeGroupEnum.PADES.equals(ste) && !firmaMultipla && !MapUtils.isEmpty(glifoSiglaMap);
					protocolloStampigliato = protocolloDaStampigliare && utente.getTimbroUscitaAoo() && protocolloPresente && MediaType.PDF.toString().equals(allegato.get_MimeType()) 
							&& !SignTypeGroupEnum.CADES.equals(ste) && (!Boolean.TRUE.equals(formatoOriginale) || firmatoDigitalmente);
					
					// L'allegato è da inviare a NPS solo se:
					// a) La protocollazione NON è stata anticipata e quindi non è ancora mai stato inviato l'allegato a NPS, oppure
					// b) L'allegato è stato firmato, oppure
					// c) L'allegato è stato stampigliato (protocollo/sigla)
					if (!protocollazioneAnticipata || firmato || siglaStampigliata || protocolloStampigliato) {
						byte[] content = FilenetCEHelper.getDocumentContentAsByte(allegato);
						String nomeFileAllegato = (String) allegato.getProperties().get(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY)).getObjectValue();
						String oggettoDocAllegato = allegato.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY));
						
						String guidFirstVersionAllegato = null;
						DocTipoOperazioneType tipoOperazioneEnum = null;
						if(protocollazioneAnticipata) {
							guidFirstVersionAllegato = fceh.getFirstVersionByGuid(allegato.get_Id().toString()).get_Id().toString();
							tipoOperazioneEnum = DocTipoOperazioneType.COLLEGA_DOCUMENTO;
						}
						
						uploadAssociazioneProtocolloToAsyncNps(protocollo.getIdProtocollo(), new ByteArrayInputStream(content),
								StringUtils.cleanGuidToString(allegato.get_Id()), allegato.get_MimeType(), nomeFileAllegato, oggettoDocAllegato, utente, 
								false, protocollo.getNumeroAnnoProtocolloNPS(utente.getCodiceAoo()), idDocumentoAllegato, connection, guidFirstVersionAllegato, tipoOperazioneEnum);
						
					}
				} catch (Exception e) {
					throw new RedException("Allegato [" + idDocumentoAllegato + "] - " + e.getMessage(), e);
				}
			}
		}
	}
	
	
	/**
	 * @param documentForDetails
	 * @param documentTitle
	 * @param utente
	 * @param connection
	 */
	private void inviaAllacciToNPS(Document documentForDetails, String documentTitle, UtenteDTO utente, Connection connection) {
		Aoo aoo = aooDAO.getAoo(utente.getIdAoo(), connection);
		inviaAllacciToNPS(documentForDetails, documentTitle, utente, aoo, connection);
	}
	
	
	/**
	 * @param documentForDetails
	 * @param documentTitle
	 * @param utente
	 * @param aoo
	 * @param connection
	 */
	private void inviaAllacciToNPS(Document documentForDetails, String documentTitle, UtenteDTO utente, Aoo aoo, Connection connection) {
		Integer idDocumento = Integer.parseInt(documentTitle);
		Collection<RispostaAllaccioDTO> documentiDaAllacciare = allaccioDAO.getDocumentiRispostaAllaccio(idDocumento, aoo.getIdAoo().intValue(), connection);
		inviaAllacciToNPS(documentForDetails, documentTitle, documentiDaAllacciare, utente, aoo, connection);
	}
	
	
	/**
	 * @param documentForDetails
	 * @param documentTitle
	 * @param documentiDaAllacciare
	 * @param utente
	 * @param aoo
	 * @param connection
	 */
	private void inviaAllacciToNPS(Document documentForDetails, String documentTitle, Collection<RispostaAllaccioDTO> documentiDaAllacciare, UtenteDTO utente, 
			Aoo aoo, Connection connection) {
		IFilenetCEHelper fcehAdmin = null;
		
		try {
			fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()),utente.getIdAoo());
			
			for (RispostaAllaccioDTO rispostaAllaccio : documentiDaAllacciare) {
				if (TipologiaProtocollazioneEnum.isProtocolloNPS(aoo.getTipoProtocollo().getIdTipoProtocollo())) {
					// Recupero l'ID protocollo del documento allacciato
					List<String> props = Arrays.asList(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY));
					Document document = fcehAdmin.getDocumentByIdGestionale(rispostaAllaccio.getIdDocumentoAllacciato(), props, null, aoo.getIdAoo().intValue(), null, null);
					
					String idProtocollo = TrasformerCE.getMetadato(document, 
							PropertiesNameEnum.ID_PROTOCOLLO_METAKEY) != null ? TrasformerCE.getMetadato(document, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY).toString() : Constants.EMPTY_STRING;
					rispostaAllaccio.setIdProtocollo(idProtocollo);
				}
			}
			
			// Allacci to NPS
			if (TipologiaProtocollazioneEnum.isProtocolloNPS(aoo.getTipoProtocollo().getIdTipoProtocollo()) && !documentiDaAllacciare.isEmpty()) {
				String annoProtocollo = TrasformerCE.getMetadato(documentForDetails, 
						PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY) != null ? TrasformerCE.getMetadato(documentForDetails, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY).toString() : Constants.EMPTY_STRING;
				String numeroProtocollo = TrasformerCE.getMetadato(documentForDetails, 
						PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY) != null ? TrasformerCE.getMetadato(documentForDetails, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY).toString() : Constants.EMPTY_STRING;
				String numeroAnnoProtocollo = numeroProtocollo + "/" + annoProtocollo + "_" + aoo.getCodiceAoo();
				String idProtocollo = TrasformerCE.getMetadato(documentForDetails,
						PropertiesNameEnum.ID_PROTOCOLLO_METAKEY) != null ? TrasformerCE.getMetadato(documentForDetails, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY).toString() : Constants.EMPTY_STRING;
				
				npsSRV.aggiungiAllacciAsync(utente, connection, idProtocollo, documentiDaAllacciare, numeroAnnoProtocollo, true, documentTitle);
			}
		} finally {
			popSubject(fcehAdmin);
		}
	}

	
	/**
	 * @param documentFilenet
	 * @param documentDetail
	 * @param protocollo
	 * @param utenteFirmatario
	 * @param context
	 * @param fceh
	 * @param con
	 */
	@SuppressWarnings("unchecked")
	private void gestisciDestinatariInterni(final Document documentFilenet, final DetailDocumentoDTO documentDetail, final ProtocolloDTO protocollo, final UtenteDTO utenteFirmatario, 
			final Map<ContextASignEnum, Object> context, final IFilenetCEHelper fceh, final Connection con) {
		String idDocumento = documentDetail.getDocumentTitle();
		LOGGER.info("gestisciDestinatariInterni -> START. Documento: " + idDocumento);
		
		String classeDocumentale = documentFilenet.getClassName();
		// Se il documento ha destinatari interni e non è una DSR o una FATTURA FEPA
		// si procede con la creazione di un nuovo documento per assegnazione interna a ciascun destinatario (interno)
		if (documentDetail.isHasDestinatariInterni() 
				&& !pp.getParameterByKey(PropertiesNameEnum.DSR_CLASSNAME_FN_METAKEY).equals(classeDocumentale)
				&& !pp.getParameterByKey(PropertiesNameEnum.FATTURA_FEPA_CLASSNAME_FN_METAKEY).equals(classeDocumentale)) {
			context.put(ContextASignEnum.DOC_INTERNO_GUID_LIST, new ArrayList<>());
			context.put(ContextASignEnum.DOC_INTERNO_WOB_NUMBER_LIST, new ArrayList<>());
			DetailDocumentRedDTO nuovoDoc = new DetailDocumentRedDTO(); // Nuovo documento per l'assegnazione interna
			
			// ### Impostazione degli attributi/metadati nel nuovo documento per assegnazione interna da creare -> START
			int idTipologiaDocumento = tipologiaDocumentoSRV.getTipologiaDocumentalePredefinita(utenteFirmatario.getIdAoo(), utenteFirmatario.getCodiceAoo(), TipoCategoriaEnum.ENTRATA, con);
			nuovoDoc.setIdTipologiaDocumento(idTipologiaDocumento); // Tipologia Documento
			long lIdTipoProcedimento = tipoProcedimentoDAO.getTipiProcedimentoByTipologiaDocumento(con, idTipologiaDocumento).get(0).getTipoProcedimentoId();
			nuovoDoc.setIdTipologiaProcedimento((int) lIdTipoProcedimento); // Tipo Procedimento
			
			Contatto contattoMittente = null;
			
			RicercaRubricaDTO ricercaContattoObj = new RicercaRubricaDTO();
			ricercaContattoObj.setNome(utenteFirmatario.getNome());
			ricercaContattoObj.setCognome(utenteFirmatario.getCognome());
			ricercaContattoObj.setRicercaRED(true);
			ricercaContattoObj.setRicercaMEF(true);
			ricercaContattoObj.setIdAoo(utenteFirmatario.getIdAoo().intValue());
			List<Contatto> contatti = contattoDAO.ricerca(null, ricercaContattoObj, con);
			
			if (!CollectionUtils.isEmpty(contatti)) {
				contattoMittente = contatti.get(0);
				// Verifico se ho un contatto sulla rubrica MEF 
				for (Contatto contRubrica: contatti) {
					if (contRubrica.getTipoRubricaEnum().equals(TipoRubricaEnum.MEF)) {
						contattoMittente = contRubrica;
						break;
					}
				}
			} else {
				contattoMittente = new Contatto();
				contattoMittente.setNome(utenteFirmatario.getNome());
				contattoMittente.setCognome(utenteFirmatario.getCognome());
				contattoMittente.setAliasContatto(utenteFirmatario.getCognome() + " " + utenteFirmatario.getNome());
				contattoMittente.setTipoRubrica("RED");
				contattoMittente.setIdAOO(utenteFirmatario.getIdAoo());
				contattoMittente.setTipoPersona("F");
				contattoMittente.setPubblico(1);
				contattoMittente.setDatacreazione(new Date());
				Long contattoId = contattoDAO.inserisci(null,contattoMittente, con);
				contattoMittente.setContattoID(contattoId);
			}
			
			nuovoDoc.setMittenteContatto(contattoMittente); // Mittente
			
			nuovoDoc.setIdCategoriaDocumento(CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA.getIds()[0]); // Categoria: Assegnazione Interna
			nuovoDoc.setIdUtenteCreatore(utenteFirmatario.getId().intValue()); // Utente Creatore
			nuovoDoc.setIdUfficioCreatore(utenteFirmatario.getIdUfficio().intValue()); // Ufficio Creatore
			nuovoDoc.setFormatoDocumentoEnum(FormatoDocumentoEnum.ELETTRONICO); // Formato Documento
			nuovoDoc.setIdAOO((Integer) TrasformerCE.getMetadato(documentFilenet, pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY))); // ID AOO
			nuovoDoc.setOggetto(documentDetail.getOggetto()); // Oggetto
			Integer registroRiservato = (Integer) TrasformerCE.getMetadato(documentFilenet, pp.getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY));
			nuovoDoc.setRegistroRiservato(registroRiservato != null && registroRiservato == 1); // Registro Riservato
			nuovoDoc.setNote((String) TrasformerCE.getMetadato(documentFilenet, pp.getParameterByKey(PropertiesNameEnum.NOTE_METAKEY))); // Note
			nuovoDoc.setRiservato(
					Boolean.TRUE.equals(documentDetail.getFlagRiservato()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue()); // Riservato
			nuovoDoc.setNomeFile((String) TrasformerCE.getMetadato(documentFilenet, pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY))); // Nome File
			nuovoDoc.setContent(documentDetail.getContent()); //  Content FIRMATO
			nuovoDoc.setMimeType(documentFilenet.get_MimeType()); // MIME Type
			nuovoDoc.setGuid(documentDetail.getGuid()); // GUID
			
			if (protocollo != null) {
				nuovoDoc.setProtocolloMittente(String.valueOf(protocollo.getNumeroProtocollo())); // Numero Protocollo Mittente
				nuovoDoc.setAnnoProtocolloMittente(protocollo.getAnnoProtocolloInt()); // Anno Protocollo Mittente
			}
			
			nuovoDoc.setNumDocDestIntUscita(documentDetail.getNumeroDocumento()); // Num Doc Dest Int Uscita
			
			// Si clonano gli allegati del documento originale e li si impostano nel nuovo documento per assegnazione interna
			List<AllegatoDTO> allegatiNuovoDoc = getAllegatiDocumentoAssegnazioneInterna(documentDetail.getGuid(), fceh);
			nuovoDoc.setAllegati(allegatiNuovoDoc);
			// ### Impostazione degli attributi/metadati nel nuovo documento per assegnazione interna da creare -> END

			Collection<?> destinatari = (Collection<?>) TrasformerCE.getMetadato(documentFilenet, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
			// Si normalizza il metadato per evitare errori comuni durante lo split delle singole stringhe dei destinatari
			List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(destinatari);
			
			if (!CollectionUtils.isEmpty(destinatariDocumento)) {
				// Si cicla sui destinatari
				for (String[] destinatarioSplit : destinatariDocumento) {
					
					String tipoDestinatario = destinatarioSplit[3];
					// Si crea un nuovo documento per assegnazione interna per ciascun destinatario interno
					if (TipologiaDestinatarioEnum.INTERNO.getTipologia().equals(tipoDestinatario)) {
						Long idUfficio = Long.parseLong(destinatarioSplit[0]);
						Long idUtente = Long.parseLong(destinatarioSplit[1]);
						
						// ### Impostazione dell'assegnazione per competenza nel nuovo documento per assegnazione interna
						UtenteDTO utenteDestinatario = utenteSRV.getById(idUtente, con);
						Nodo nodoDestinatario = nodoDAO.getNodo(idUfficio, con);
						UfficioDTO ufficioDestinatario = new UfficioDTO(idUfficio, nodoDestinatario.getDescrizione());
						
						List<AssegnazioneDTO> assegnazioniNuovoDoc = new ArrayList<>();
						Date now = new Date();
						AssegnazioneDTO assegnazioneCompetenza = new AssegnazioneDTO(null, TipoAssegnazioneEnum.COMPETENZA, DateUtils.dateToString(now, null), now, null, 
								utenteDestinatario, ufficioDestinatario);
						assegnazioniNuovoDoc.add(assegnazioneCompetenza);
						nuovoDoc.setAssegnazioni(assegnazioniNuovoDoc);
						
						// ### Impostazione dei parametri per il salvataggio del documento -> START
						SalvaDocumentoRedParametriDTO parametri = new SalvaDocumentoRedParametriDTO();
						
						parametri.setIdUfficioMittenteWorkflow(utenteFirmatario.getIdUfficio());
						parametri.setIdUtenteMittenteWorkflow(0L);
						parametri.setIdUfficioDestinatarioWorkflow(idUfficio);
						parametri.setIdUtenteDestinatarioWorkflow(idUtente);
						parametri.setIsAssegnazioneInternaWorkflow(TipoAssegnazioneEnum.DESTINATARIO_INTERNO);
						parametri.setIdDocumentoOldWorkflow(documentDetail.getDocumentTitle());
						parametri.setMotivazioneAssegnazioneWorkflow("ID Documento mittente " + documentDetail.getNumeroDocumento());
						// ### Impostazione dei parametri per il salvataggio del documento -> END
												
						LOGGER.info("gestisciDestinatariInterni -> Creazione di un nuovo documento per assegnazione interna dal documento firmato: " + idDocumento);
						// ### Chiamata al processo automatico di creazione e memorizzazione di un nuovo documento in ingresso
						EsitoSalvaDocumentoDTO esitoCreazioneDocInterno = 
								salvaDocumentoSRV.salvaDocumento(nuovoDoc, parametri, utenteFirmatario, ProvenienzaSalvaDocumentoEnum.PROCESSO_AUTOMATICO_INTERNO);
						
						// ### Memorizzazione di GUID e WOB number del documento interno nel contesto, per l'eventuale successivo recovery
						if (esitoCreazioneDocInterno != null && esitoCreazioneDocInterno.isEsitoOk()) {
							((List<String>) context.get(ContextASignEnum.DOC_INTERNO_GUID_LIST)).add(esitoCreazioneDocInterno.getGuid());
							((List<String>) context.get(ContextASignEnum.DOC_INTERNO_WOB_NUMBER_LIST)).add(esitoCreazioneDocInterno.getWobNumber());
						}
					}
					
				}
			}
		}
		LOGGER.info("gestisciDestinatariInterni -> END. Documento: " + idDocumento);
	}
	
	
	/**
	 * @param document
	 * @param utente
	 * @param idTipologiaDocumento
	 * @return
	 */
	private boolean hasToSimulateInvioNps(Document document, UtenteDTO utente, Long idTipologiaDocumento, Connection connection, IFilenetCEHelper fcehAdmin ) {
		boolean isFlussoAUT = false;
		boolean isInoltroAttoDecretoUCB = false;
		
		String documentTitle = (String)TrasformerCE.getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
		//TODO recupera allaccio principale, se esiste get da CE e recupero codiceFlusso
		//se non esiste return false
		RispostaAllaccioDTO allaccioPrincipale = allaccioDAO.getAllaccioPrincipale(Integer.parseInt(documentTitle), connection);
		if (allaccioPrincipale != null) {
			
			List<String> props = Arrays.asList(pp.getParameterByKey(PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY));
			Document documentAllaccioPrincipale = fcehAdmin.getDocumentByIdGestionale(allaccioPrincipale.getIdDocumentoAllacciato(), props, null, utente.getIdAoo().intValue(), null, null);
			
			String codiceFlusso = TrasformerCE.getMetadato(documentAllaccioPrincipale, PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY) != null ? 
					TrasformerCE.getMetadato(documentAllaccioPrincipale, PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY).toString() : "";
			
			// Verifica flusso AUT
			isFlussoAUT = isFamigliaFlussoAUT(codiceFlusso);
			// Verifica flusso AttoDecretoUCB
			isInoltroAttoDecretoUCB = checkIsFlussoAttoDecretoUCB(utente, idTipologiaDocumento);
			// Se si tratta di uno dei due flussi occorre simulare l'invio ad NPS
			return isFlussoAUT || isInoltroAttoDecretoUCB;
					
		
		} else {
			return false;
		}
		
	}
	
	
	/**
	 * @param esito
	 * @param protocollo
	 * @param isRegistroRepertorio
	 */
	private static void impostaEsitoOk(EsitoOperazioneDTO esito, ProtocolloDTO protocollo, Boolean isRegistroRepertorio) {
		esito.setEsito(true);
		esito.setCodiceErrore(null);
		esito.setNote(null);
		
		if (protocollo != null) {
			esito.setNumeroProtocollo(protocollo.getNumeroProtocollo());
			esito.setAnnoProtocollo(protocollo.getAnnoProtocolloInt());
			esito.setRegistroRepertorio(isRegistroRepertorio);
		}
	}
	
	
	/**
	 * @param wobNumber
	 * @param esitoOperazioneFirma
	 * @param e
	 * @param codiceErrore
	 * @param messaggio
	 * @param metadatoPdfErrorValue
	 * @param connection
	 * @return
	 */
	private static Integer gestisciEccezioneFirmaContents(String wobNumber, EsitoOperazioneDTO esitoOperazioneFirma, Exception e, Enum<?> codiceErrore, String messaggio,
			Integer metadatoPdfErrorValue, Connection connection) {
		LOGGER.error("Errore durante la firma dei content del documento afferente al workflow: " + wobNumber, e);
		esitoOperazioneFirma.setCodiceErrore(codiceErrore);
		esitoOperazioneFirma.setNote(messaggio);
		rollbackConnection(connection);
		
		if (metadatoPdfErrorValue == Constants.Varie.TRASFORMAZIONE_PDF_OK) {
			metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.WARN_GENERICO.getValue();
		}
		
		return metadatoPdfErrorValue;
	}
	
	
	/**
	 * Gestisce l'eccezione lanciata: e, memorizzando l'errore nel log associato allo step in lavorazione.
	 * Se l'errore viene riscontrato su un allegato, verrà riportato il documentTitle dell'allegato nel log.
	 * @param e
	 * @param esitoStepFirma
	 * @param error
	 */
	private void gestisciEccezioneStepFirma(Exception e, EsitoOperazioneDTO esitoStepFirma, SignErrorEnum error, String dTAllegato) {
		LOGGER.error("ERRORE - Firma documento " + esitoStepFirma.getDocumentTitle() + ": "+ e.getMessage(), e);		
		esitoStepFirma.setCodiceErrore(error);
		esitoStepFirma.setNote(!StringUtils.isNullOrEmpty(dTAllegato) ? ("Allegato [" + dTAllegato + "] - ") : ("") + error.getMessage() + " \n" + ExceptionUtils.getStackTrace(e));
	}

	
	/**
	 * @param documentTitle
	 * @param sottoCategoriaUscitaDoc
	 * @param idAoo
	 * @param fceh
	 * @return
	 */
	private boolean isContentFromAnnotationContentLibroFirma(String documentTitle, SottoCategoriaDocumentoUscitaEnum sottoCategoriaUscitaDoc, Long idAoo, IFilenetCEHelper fceh) {
		return !CollectionUtils.isEmpty(fceh.getAnnotationsDocument(documentTitle, false, FilenetAnnotationEnum.CONTENT_LIBRO_FIRMA, idAoo, true))
				&& (sottoCategoriaUscitaDoc == null || !sottoCategoriaUscitaDoc.isRegistrazioneAusiliaria());
	}
	
}