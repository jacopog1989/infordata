package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IDisabilitazioneNotificheUtenteDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.DisabilitazioneNotificheUtente;

/**
 * DAO disabilitazioen notifiche utente.
 */
@Repository
public class DisabilitazioneNotificheUtenteDAO extends AbstractDAO implements IDisabilitazioneNotificheUtenteDAO {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DisabilitazioneNotificheUtenteDAO.class);

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -8398468874401116361L;

	/**
	 * @see it.ibm.red.business.dao.IDisabilitazioneNotificheUtenteDAO#getbyUser(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public List<DisabilitazioneNotificheUtente> getbyUser(final Long idUtente, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<DisabilitazioneNotificheUtente> notificheList = new ArrayList<>();
		try {
			String sql = "select * " 
					+ " from SE_DISAB_NOTIFICA_UTENTE"
					+ " where IDUTENTE = ?";
			ps = connection.prepareStatement(sql);
			int index = 0;
			ps.setLong(++index, idUtente);

			rs = ps.executeQuery();

			while (rs.next()) {
				DisabilitazioneNotificheUtente dnu = populateObject(rs);
				notificheList.add(dnu);
			}

		} catch (SQLException sql) {
			throw new RedException(sql);
		} finally {
			closeStatement(ps, rs);
		}
		return notificheList;
	}

	private static DisabilitazioneNotificheUtente populateObject(final ResultSet rs) throws SQLException {
		Long idUtente = rs.getLong("idutente");
		int canaleTrasmissione = rs.getInt("idcanaletrasmissione");
		Date dataDa = rs.getDate("datada");
		Date dataA = rs.getDate("dataa");
		DisabilitazioneNotificheUtente dnu = new DisabilitazioneNotificheUtente();
		dnu.setIdUtente(idUtente);
		dnu.setIdCanaleTrasmissione(canaleTrasmissione);
		dnu.setDataDa(dataDa);
		dnu.setDataA(dataA);
		return dnu;
	}

	/**
	 * @see it.ibm.red.business.dao.IDisabilitazioneNotificheUtenteDAO#delete(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public void delete(final Long idUtente, final Connection connection) {
		PreparedStatement ps = null;
		try {

			int index = 1;
			ps = connection.prepareStatement("delete " + " from SE_DISAB_NOTIFICA_UTENTE" + " where IDUTENTE = ?");

			ps.setString(index++, idUtente.toString());
			ps.executeUpdate();

		} catch (SQLException e) {
			LOGGER.error("Errore durante l'eliminazione nella tabella DisabilitazioneNotificheUtente per l'utente -> "
					+ idUtente, e);
			throw new RedException(
					"Errore durante l'eliminazione nella tabella DisabilitazioneNotificheUtente per l'utente -> "
							+ idUtente,
					e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IDisabilitazioneNotificheUtenteDAO#insert(it.ibm.red.business.persistence.model.DisabilitazioneNotificheUtente,
	 *      java.sql.Connection).
	 */
	@Override
	public void insert(final DisabilitazioneNotificheUtente disabilitazioneNotificheUtente,
			final Connection connection) {

		PreparedStatement ps = null;

		try {

			int index = 1;
			ps = connection.prepareStatement(
					"insert into SE_DISAB_NOTIFICA_UTENTE (IDUTENTE, IDCANALETRASMISSIONE, DATADA, DATAA) values (?,?,?,?) ");
			ps.setString(index++, disabilitazioneNotificheUtente.getIdUtente().toString());
			ps.setInt(index++, disabilitazioneNotificheUtente.getIdCanaleTrasmissione());
			if (disabilitazioneNotificheUtente.getDataDa() != null) {
				ps.setDate(index++, disabilitazioneNotificheUtente.getDataDa());
			} else {
				ps.setNull(index++, Types.DATE);
			}
			if (disabilitazioneNotificheUtente.getDataDa() != null) {
				ps.setDate(index++, disabilitazioneNotificheUtente.getDataA());
			} else {
				ps.setNull(index++, Types.DATE);
			}

			ps.executeUpdate();

		} catch (SQLException e) {
			LOGGER.error("Errore durante l'inserimento nella tabella DisabilitazioneNotificheUtente per l'utente -> "
					+ disabilitazioneNotificheUtente.getIdUtente(), e);
			throw new RedException(
					"Errore durante l'inserimento nella tabella DisabilitazioneNotificheUtente per l'utente -> "
							+ disabilitazioneNotificheUtente.getIdUtente(),
					e);
		} finally {
			closeStatement(ps);
		}

	}
}
