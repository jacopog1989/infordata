package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;

/**
 * 
 * @author CPIERASC
 *
 *	Dao gestione fascicolo.
 */
public interface IFaldoneDAO extends Serializable {

	/**
	 * Ottiene la sequence del faldone.
	 * @param idAoo
	 * @param con
	 * @return id successivo del faldone
	 */
	Integer getNextIdFaldone(Integer idAoo, Connection con);

}