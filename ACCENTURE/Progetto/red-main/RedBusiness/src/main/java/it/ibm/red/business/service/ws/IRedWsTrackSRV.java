package it.ibm.red.business.service.ws;

import it.ibm.red.business.service.ws.facade.IRedWsTrackFacadeSRV;

/**
 * The Interface IRedWsTrackSRV.
 *
 * @author a.dilegge
 * 
 *         Interfaccia servizio per il tracciamento delle invocazioni a RedEvo WS.
 */
public interface IRedWsTrackSRV extends IRedWsTrackFacadeSRV {
	

}