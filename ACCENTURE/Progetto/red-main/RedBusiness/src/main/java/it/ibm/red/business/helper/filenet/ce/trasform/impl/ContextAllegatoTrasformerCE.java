package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.Map;

import com.filenet.api.core.Document;

import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;

/**
 * Context Allegato.
 * @param <T>
 */
public abstract class ContextAllegatoTrasformerCE<T> extends ContextTrasformerCE<T> {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -4674628855410638846L;

	/**
	 * Costruttore.
	 * @param inEnumKey
	 */
	protected ContextAllegatoTrasformerCE(final TrasformerCEEnum inEnumKey) {
		super(inEnumKey);
	}
	
	/**
	 * @param allegato
	 * @param principale
	 * @param context
	 * @return T
	 */
	public abstract T trasform(Document allegato, Document principale, Map<ContextTrasformerCEEnum, Object> context);

	/**
	 * @param document
	 * @param connection
	 * @param context
	 * @return T
	 */
	public abstract T trasform(Document document, Connection connection, Map<ContextTrasformerCEEnum, Object> context);	 
}