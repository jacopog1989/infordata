/**
 * 
 */
package it.ibm.red.business.service.concrete;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWRosterQuery;
import it.ibm.red.business.dao.ICambiaUfficioDAO;
import it.ibm.red.business.dao.ICodeApplicativeDAO;
import it.ibm.red.business.dao.IGestioneNotificheEmailDAO;
import it.ibm.red.business.dao.INpsConfigurationDAO;
import it.ibm.red.business.dao.IOrganigrammaDAO;
import it.ibm.red.business.dao.IRuoloDAO;
import it.ibm.red.business.dao.ISottoscrizioniDAO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataFascDTO;
import it.ibm.red.business.dto.PerformanceDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.AccessoFunzionalitaEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.FunzionalitaEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.QueueGroupEnum;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.RicercaPerBusinessEntityEnum;
import it.ibm.red.business.enums.SourceTypeEnum;
import it.ibm.red.business.enums.StatoMailEnum;
import it.ibm.red.business.enums.StatoMailGUIEnum;
import it.ibm.red.business.helper.adobe.AdobeLCHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.helper.signing.SignHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.BusinessDelegate;
import it.ibm.red.business.nps.builder.Builder;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.NpsConfiguration;
import it.ibm.red.business.persistence.model.PkHandler;
import it.ibm.red.business.persistence.model.Ruolo;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IASignSRV;
import it.ibm.red.business.service.ICasellePostaliSRV;
import it.ibm.red.business.service.ICounterSRV;
import it.ibm.red.business.service.IListaDocumentiSRV;
import it.ibm.red.business.service.IRecogniformSRV;
import it.ibm.red.business.service.IRicercaAvanzataFascSRV;
import it.ibm.red.business.service.IRicercaSRV;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.PermessiUtils;
import npsmessages.v1.it.gov.mef.RispostaReportRegistroGiornalieroType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType;

/**
 * @author APerquoti
 *
 */
@Service
public class CounterSRV extends AbstractService implements ICounterSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 321217309598188473L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CounterSRV.class.getName());
	
	/**
	 * Service.
	 */
	@Autowired
	private IUtenteFacadeSRV utenteSRV;
	
	/**
	 * Service.
	 */
	@Autowired
	private IListaDocumentiSRV listaDocSRV;
	
	/**
	 * Service.
	 */
	@Autowired
	private IRecogniformSRV recogniformSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ICasellePostaliSRV casellePostaliSRV;

	/**
	 * Dao.
	 */
	@Autowired
	private ISottoscrizioniDAO sottoscrizioniDAO;
	
	/**
	 * Dao.
	 */
	@Autowired
	private ICodeApplicativeDAO codeAppDAO;
	
	/**
	 * Dao.
	 */
	@Autowired
	private IOrganigrammaDAO organigrammaDAO;
	
	/**
	 * Dao.
	 */
	@Autowired
	private IGestioneNotificheEmailDAO gestioneNotificheMailDAO;
	
	/**
	 * Dao.
	 */
	@Autowired
	private ICambiaUfficioDAO cambiaUfficioDAO;
	
	/**
	 * Dao.
	 */
	@Autowired
	private IRuoloDAO ruoloDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private INpsConfigurationDAO npsConfigurationDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IRicercaSRV ricercaSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IRicercaAvanzataFascSRV ricercaAvanzataSRV;

	/**
	 * Servizio firma asincrona.
	 */
	@Autowired
	private IASignSRV aSignSRV;

	/**
	 * @see it.ibm.red.business.service.facade.ICounterFacadeSRV#getCounterMenuMail(java.lang.String,
	 *      it.ibm.red.business.dto.FilenetCredentialsDTO, java.lang.Long).
	 */
	@Override
	public Map<String, Integer> getCounterMenuMail(final String nomeCasella, final FilenetCredentialsDTO fcDTO, final Long idAoo) {
		final Map<String, Integer> output = new HashMap<>();
		IFilenetCEHelper fceh = null;
		
		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);

			final String casellePostaliFolder = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FOLDER_ELETTRONICI_FN_METAKEY).split("\\|")[1];
			final String folder = "/" + casellePostaliFolder + "/" + nomeCasella + "/" + PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_INBOX_FN_METAKEY);
			final String classeDocumentale = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MAIL_CLASSNAME_FN_METAKEY);	
			
			// definizione stati e query per il count della cartella IN_ARRIVO
			List<String> stati = new ArrayList<>();
			stati.add(StatoMailEnum.INARRIVO.getStatusString());
			stati.add(StatoMailEnum.INARRIVO_NON_PROTOCOLLABILE_AUTOMATICAMENTE.getStatusString());
			stati.add(StatoMailEnum.IN_FASE_DI_PROTOCOLLAZIONE_MANUALE.getStatusString());
			LOGGER.info("####INFO#### Inizio Count In Arrivo");
			output.put(StatoMailGUIEnum.INARRIVO.getLabel(), fceh.countMailFolder(folder, classeDocumentale, stati));
			LOGGER.info("####INFO#### Fine Count In Arrivo");
			// definizione stati e query per il count della cartella ELIMINATA
			stati = new ArrayList<>();
			stati.add(StatoMailEnum.ELIMINATA.getStatusString());
			
			LOGGER.info("####INFO#### Inizio Count Eliminata");
			output.put(StatoMailGUIEnum.ELIMINATA.getLabel(), fceh.countMailFolder(folder, classeDocumentale, stati));
			LOGGER.info("####INFO#### Fine Count Eliminata");
			// definizione stati e query per il count della cartella RIFIUTATA/INOLTRATA
			// imposto la key come 'inoltrata' anche se il count comprende anche quelle rifiutate
			stati = new ArrayList<>();
			stati.add(StatoMailEnum.INOLTRATA.getStatusString());
			stati.add(StatoMailEnum.RIFIUTATA.getStatusString());
			LOGGER.info("####INFO#### Inizio Count Inoltrata");
			output.put(StatoMailGUIEnum.INOLTRATA.getLabel(), fceh.countMailFolder(folder, classeDocumentale, stati));
			LOGGER.info("####INFO#### Fine Count Inoltrata");
			stati = new ArrayList<>();
			stati.add(StatoMailEnum.INUSCITA.getStatusString());
			LOGGER.info("####INFO#### Inizio Count In Uscita");
			output.put(StatoMailGUIEnum.INUSCITA.getLabel(), fceh.countMailFolder(folder, classeDocumentale, stati));
			LOGGER.info("####INFO#### Fine Count In Uscita");
			//RIFAUTO START
			stati = new ArrayList<>();
			stati.add(StatoMailEnum.RIFIUTATA_AUTOMATICAMENTE.getStatusString());
			LOGGER.info("####INFO#### Inizio Count Rifiutate Automaticamente");
			output.put(StatoMailGUIEnum.RIFAUTO.getLabel(), fceh.countMailFolder(folder, classeDocumentale, stati));
			LOGGER.info("####INFO#### Fine Count Rifiutate Automaticamente");
			//RIFAUTO END
			
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante il counter delle mail per la casella postale: " + nomeCasella.toUpperCase(), e);
		} finally {
			popSubject(fceh);
		}
		
		return output;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.ICounterFacadeSRV#getCounterMenuAttivita(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Map<Integer, Integer> getCounterMenuAttivita(final UtenteDTO utente) {
		final Map<Integer, Integer> output = new HashMap<>();
		Connection connection = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		Integer count = 0;
		final List<String> documentTitleList = new ArrayList<>();
		
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			
			for (final DocumentQueueEnum queue : DocumentQueueEnum.values()) {
				// Prendo in considerazione solo le code che si trovano all'interno del menu attivita ignorando quelle FEPA (si fanno a parte)
				if (SourceTypeEnum.FILENET.equals(queue.getType()) && !"NSD_Roster".equals(queue.getName()) && !"FEPA_LibroFirma".equals(queue.getName()) 
						&& !"FEPA_DaLavorare".equals(queue.getName()) && !"FEPA_Attesa".equals(queue.getName()) && !QueueGroupEnum.RECOGNIFORM.equals(queue.getGroup())
						&& !DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE_FEPA.equals(queue)) {

					if (hasToSkipQueue(utente, queue)) {
						continue;
					}
					
					final VWQueueQuery workFlow = listaDocSRV.getQueueFilenet(queue, utente, null, fpeh, connection);
					count = workFlow.fetchCount();
					output.put(queue.getId(), count);
					
					LOGGER.info("###NumErroriSped### - getConterMenuAttività Coda " + queue.getDisplayName());
					
					LOGGER.info("###NumErroriSped### - getConterMenuAttività countSpedizioni " + count);
					
					if (DocumentQueueEnum.SPEDIZIONE.equals(queue)) {
						Integer countErrori = 0;
						LOGGER.info("###NumErroriSped### - Start getNumErroriSpedizioneCoda counterSRV");
						// Si recuperano i document title da workflow e si controllano quelli andati in errore
						documentTitleList.addAll(fpeh.getIdDocumenti(workFlow));
						LOGGER.info("SPEDIZIONE ERRORE - START - DocumentTitleList Size : " + documentTitleList.size());
						countErrori = gestioneNotificheMailDAO.getNumErroriSpedizioneCoda(documentTitleList, connection);
						LOGGER.info("SPEDIZIONE ERRORE - END - CountErrori : " + countErrori);
						output.put(DocumentQueueEnum.SPEDIZIONE_ERRORE.getId(), countErrori);
						
						LOGGER.info("###NumErroriSped### - End - getConterMenuAttività countSpedizioni counterSRV " + countErrori);
					}
					
				// Documenti cartacei (acquisiti / eliminati)
				} else if (SourceTypeEnum.FILENET.equals(queue.getType()) 
						&& QueueGroupEnum.RECOGNIFORM.equals(queue.getGroup()) 
						&& PermessiUtils.haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.DOCUMENTI_CARTACEI_ACQUISITI_ELIMINATI, utente)) {
					
					final boolean eliminati = DocumentQueueEnum.ELIMINATI.equals(queue);
					count = recogniformSRV.getCountDocumentiCartacei(fceh, false, eliminati);
					output.put(queue.getId(), count);
				} else if (DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE.equals(queue) && utente.isGestioneApplicativa()) {
					Set<String> dts = aSignSRV.getIncompletedDTs(utente, queue);
					count = 0;
					if (dts != null) {
						count = dts.size();
					}
					output.put(queue.getId(), count);
				} else if (SourceTypeEnum.APP.equals(queue.getType()) && !QueueGroupEnum.SERVIZIO.equals(queue.getGroup())) {
					
					Long idUtente = utente.getId();
					if (QueueGroupEnum.UFFICIO.equals(queue.getGroup())) {
						idUtente = 0L;
					}
					count = codeAppDAO.countCodeAppl(utente.getIdUfficio(), idUtente, queue.getIdStatiLavorazione(), connection);
					output.put(queue.getId(), count);
					
				} 
			}
		} catch (final Exception e) {
			LOGGER.error("Erorre durante il counter del Menù Attività.", e);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}
		
		return output;
	}

	/**
	 * Restituisce true se la coda {@code queue} analizzata deve essere ignorata
	 * nell'aggiornamento dei contatori.
	 * 
	 * @param utente
	 *            Utente in sessione.
	 * @param queue
	 *            Coda da analizzare.
	 * @return {@code True} se occorre saltare la coda, {@code false} altrimenti.
	 */
	private static boolean hasToSkipQueue(UtenteDTO utente, DocumentQueueEnum queue) {
		
		boolean hasToSkip = false;
		
		if (utente.getIdUfficio().equals(utente.getIdNodoAssegnazioneIndiretta()) 
				&& DocumentQueueEnum.CORRIERE.equals(queue)) {
			hasToSkip = true;
		}
		if (!utente.getIdUfficio().equals(utente.getIdNodoAssegnazioneIndiretta())
				&& (DocumentQueueEnum.CORRIERE_DIRETTO.equals(queue) || DocumentQueueEnum.CORRIERE_INDIRETTO.equals(queue))) {
			hasToSkip = true;
		}
		if (utente.isUcb() && DocumentQueueEnum.DA_LAVORARE.equals(queue)) {
			hasToSkip = true;
		}
		if (!utente.isUcb() && (DocumentQueueEnum.DA_LAVORARE_UCB.equals(queue) || DocumentQueueEnum.IN_LAVORAZIONE_UCB.equals(queue))) {
			hasToSkip = true;
		}
		return hasToSkip;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.ICounterFacadeSRV#getCounterMenuFepa(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Map<DocumentQueueEnum, Integer> getCounterMenuFepa(final UtenteDTO utente) {
		final EnumMap<DocumentQueueEnum, Integer> output = new EnumMap<>(DocumentQueueEnum.class);
		
		try {
			for (final DocumentQueueEnum queue : DocumentQueueEnum.values()) {
				if (SourceTypeEnum.FILENET.equals(queue.getType()) && "NSD_Roster".equals(queue.getName())) {
					
					final VWRosterQuery queryRoster =	(VWRosterQuery) listaDocSRV.getFepaQueryObject(queue, null, utente);
					final int count = queryRoster.fetchCount();
					output.put(queue, count);
					
				} else if (SourceTypeEnum.FILENET.equals(queue.getType()) && ("FEPA_LibroFirma".equals(queue.getName()) || "FEPA_DaLavorare".equals(queue.getName()) || "FEPA_Attesa".equals(queue.getName()))) {
					
					final VWQueueQuery query = (VWQueueQuery) listaDocSRV.getFepaQueryObject(queue, null, utente);
					final int count = query.fetchCount();
					output.put(queue, count);
					
				} else if (DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE_FEPA.equals(queue) && utente.isGestioneApplicativa()) {
					
					Set<String> dts = aSignSRV.getIncompletedDTs(utente, queue);
					output.put(queue, dts == null ? 0 : dts.size());
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Erorre durante il counter del Menù Attività ", e);
		}
		
		return output;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.ICounterFacadeSRV#countCodeUfficioOrg(java.lang.Long,
	 *      it.ibm.red.business.dto.FilenetCredentialsDTO, boolean).
	 */
	@Override
	public Map<String, Integer> countCodeUfficioOrg(final Long idNodo, final FilenetCredentialsDTO fcDto, final boolean isUCB) {
		final Map<String, Integer> output = new HashMap<>();
		Connection connection = null;
		FilenetPEHelper fpeh = null; 
		
		try {
			fpeh = new FilenetPEHelper(fcDto);
			connection = setupConnection(getDataSource().getConnection(), false);
			for (final DocumentQueueEnum queue : DocumentQueueEnum.getQueueByGroup(QueueGroupEnum.UFFICIO)) {
				if (SourceTypeEnum.FILENET.equals(queue.getType())) {
					
					if ((!isUCB && (!DocumentQueueEnum.CORRIERE_DIRETTO.equals(queue) && !DocumentQueueEnum.CORRIERE_INDIRETTO.equals(queue))) || isUCB) {
						final VWQueueQuery query = fpeh.getWorkFlowsByWobQueueFilter(null, queue, queue.getIndexName(), idNodo, null, fcDto.getIdClientAoo(), null, null,
								false);
						if (DocumentQueueEnum.CORRIERE_DIRETTO.equals(queue) || DocumentQueueEnum.CORRIERE_INDIRETTO.equals(queue)) {
							output.put(queue.getDisplayName(), query.fetchCount());
						} else {
							output.put(queue.getName(), query.fetchCount());
						}
					}
					
				} else {
					
					final Integer count = codeAppDAO.countCodeAppl(idNodo, 0L, queue.getIdStatiLavorazione(), connection);
					output.put(queue.getName(), count);
				}
				
			}
			
		} catch (final Exception e) {
			LOGGER.error("Erorre durante il counter delle code ufficio nell'organigramma ", e);
		} finally {
			closeConnection(connection);
		}
		
		return output;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.ICounterFacadeSRV#countCodeUtentiOrg(java.lang.Long,
	 *      java.lang.Long, java.util.List,
	 *      it.ibm.red.business.dto.FilenetCredentialsDTO, boolean).
	 */
	@Override
	public Map<Long, Integer> countCodeUtentiOrg(final Long idNodo, final Long idUtenteSessione, final List<NodoOrganigrammaDTO> listaUtenti, final FilenetCredentialsDTO fcDto, final boolean isUtenteSessioneGestioneApplicativa) {
		final Map<Long, Integer> output = new HashMap<>();
		FilenetPEHelper fpeh = null;
		
		try {
			fpeh = new FilenetPEHelper(fcDto); 
			for (final NodoOrganigrammaDTO n : listaUtenti) {
				Integer count = 0;
				// Modifica del 12/06/2012 per ADT4320
				// Non considerare i documenti riservati di altri utenti (LOGICA PRESA DA NSD)
				Boolean registroRiservato = Boolean.FALSE;
//				if (!idUtenteSessione.equals(n.getIdUtente())) {
//					registroRiservato = Boolean.FALSE;
//				}
				
				final ArrayList<Long> idsUtenteDestinatario = new ArrayList<>();
				idsUtenteDestinatario.add(n.getIdUtente());
				//non serve differenziare per UCB in quanto la count prende in considerazione tutti i documenti nella coda Filenet Da Lavorare, anche quelli nella coda logica In Lavorazione
				final VWQueueQuery query = fpeh.getWorkFlowsByWobQueueFilter(null, DocumentQueueEnum.DA_LAVORARE, DocumentQueueEnum.DA_LAVORARE.getIndexName(), idNodo, idsUtenteDestinatario, fcDto.getIdClientAoo(), null, null, registroRiservato);
				count = query.fetchCount();
				
				final VWQueueQuery queryDue = fpeh.getWorkFlowsByWobQueueFilter(null, DocumentQueueEnum.SOSPESO, DocumentQueueEnum.SOSPESO.getIndexName(), idNodo, idsUtenteDestinatario, fcDto.getIdClientAoo(), null, null, registroRiservato);
				count += queryDue.fetchCount();
				
				if (isUtenteSessioneGestioneApplicativa) {
					final VWQueueQuery queryTre = fpeh.getWorkFlowsByWobQueueFilter(null, DocumentQueueEnum.NSD, DocumentQueueEnum.NSD.getIndexName(), idNodo, idsUtenteDestinatario, fcDto.getIdClientAoo(), null, null, registroRiservato);
					count += queryTre.fetchCount();
				}
				
				//associo l'id utente con il count totale dei doc 
				output.put(n.getIdUtente(), count);
			}
			
		} catch (final Exception e) {
			LOGGER.error("Erorre durante il counter delle code ufficio nell'organigramma ", e);
		}
		
		return output;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.ICounterFacadeSRV#countTotSottoUffici(java.util.List,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Map<Long, Integer> countTotSottoUffici(final List<NodoOrganigrammaDTO> sottoUffici, final UtenteDTO utente) {
		final Map<Long, Integer> output = new HashMap<>();
		FilenetPEHelper fpeh = null;
		Connection connection = null;
		
		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			connection = setupConnection(getDataSource().getConnection(), false);
			for (final NodoOrganigrammaDTO su : sottoUffici) {
				Integer countTot = 0;
				
				// viene calcolato il totale per ogni nodo
				countTot = countSottoUffici(su.getIdNodo(), fpeh, utente.getFcDTO().getIdClientAoo());
				
				// e contestualmente viene calcolato il totale anche de figli
				final List<NodoOrganigrammaDTO> figli = organigrammaDAO.getFigliByNodo(su.getIdNodo(), su.getIdAOO(), null, utente.getIdUfficio(), connection);
				if (!figli.isEmpty()) {
					Integer countSottoUffici = 0;
					for (final NodoOrganigrammaDTO n : figli) {
						
						// se c'è riscontro viene sommato
						countSottoUffici = countSottoUffici(n.getIdNodo(), fpeh, utente.getFcDTO().getIdClientAoo());
						countTot += countSottoUffici;
					}
				}
				
				output.put(su.getIdNodo(), countTot);
			}
			
			// In Sostanza per recuperare il Count totale di un ufficio si va a sommare il totale dell coda CORRIERE e il totale dei sotto nodi di tipo utente(DA LAVORARE, SOSPESO).
			// In più con la stessa logica se sono presenti si vanno a sommare i totali di eventuali sotto nodi(CORRIERE, DA LAVORARE, SOSPESO).
			// quindi data una lista di uffici si va in profondità con la count di massimo un livello.
			
		} catch (final Exception e) {
			LOGGER.error("Erorre durante il counter dei sottouffici nell'organigramma ", e);
		} finally {
			closeConnection(connection);
		}
		
		return output;
	}
	
	
	private Integer countSottoUffici(final Long idNodo, final FilenetPEHelper fpeh, final String idClientAoo) {
		Integer count = 0;
		//imposto il registro riservato sempre a false essendo l'id utente a '0' e quindi una count solo a livello ufficio
		final Boolean registroRiservato = Boolean.FALSE;
		
		// logica condivisa per il calcolo del contatore di un determinato Nodo
		try {
			final ArrayList<Long> idsUtenteDestinatario = null;

			// Per Totale si intende la somma delle count delle rispettive code FileNet: Corriere, Da Lavorare, Sospeso.
			final VWQueueQuery query = fpeh.getWorkFlowsByWobQueueFilter(null, DocumentQueueEnum.CORRIERE, DocumentQueueEnum.CORRIERE.getIndexName(), idNodo, idsUtenteDestinatario, idClientAoo, null, null, registroRiservato);
			count = query.fetchCount();
			
			// Con le code Da Lavoraree in Sospeso si identificano i nodi utente che contengono documenti provenienti da queste due tipologie di Code
			//non serve differenziare per UCB in quanto la count prende in considerazione tutti i documenti nella coda Filenet Da Lavorare, anche quelli nella coda logica In Lavorazione
			final VWQueueQuery queryDue = fpeh.getWorkFlowsByWobQueueFilter(null, DocumentQueueEnum.DA_LAVORARE, DocumentQueueEnum.DA_LAVORARE.getIndexName(), idNodo, idsUtenteDestinatario, idClientAoo, null, null, registroRiservato);
			count += queryDue.fetchCount();
			
			final VWQueueQuery queryTre = fpeh.getWorkFlowsByWobQueueFilter(null, DocumentQueueEnum.SOSPESO, DocumentQueueEnum.SOSPESO.getIndexName(), idNodo, idsUtenteDestinatario, idClientAoo, null, null, registroRiservato);
			count += queryTre.fetchCount();
			
		} catch (final Exception e) {
			LOGGER.error("Erorre durante il counter dei sottouffici nell'organigramma ", e);
		}
		
		return count;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICounterFacadeSRV#countTotSottoUfficiWidget(java.util.List,
	 *      it.ibm.red.business.dto.UtenteDTO, boolean).
	 */
	@Override
	public Map<String, Integer> countTotSottoUfficiWidget(final List<Nodo> listUffici, final UtenteDTO utente, final boolean inApprovazioneDirigente) {
		final Map<String, Integer> outputCount = new HashMap<>(); 
		Connection connection = null;
		FilenetPEHelper fpeh = null; 
		
		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO()); 
			connection = setupConnection(getDataSource().getConnection(), false);
		 
			if (listUffici != null) {
				for (final Nodo nodo : listUffici) {
					final List<Nodo> nodoList = new ArrayList<>();
					nodoList.add(nodo);
					final List<Long> idRuoli = cambiaUfficioDAO.getIdRuoloFromIdUtenteAndIdNodoAndIdAOO(utente.getId(), nodo.getIdNodo().intValue(), utente.getIdAoo(), connection);
					final boolean haRuoloDelegato = idRuoli.contains(nodo.getAoo().getIdRuoloDelegatoLibroFirma());
					
					if (haRuoloDelegato) {
						final VWQueueQuery workFlow = listaDocSRV.getQueueFilenetWidget(DocumentQueueEnum.NSD, utente, null, false, nodoList, inApprovazioneDirigente, fpeh, connection);
						outputCount.put(nodo.getIdNodo().intValue() + "", workFlow.fetchCount());
						
						final VWQueueQuery workFlowDelegato = listaDocSRV.getQueueFilenetWidget(DocumentQueueEnum.NSD_LIBRO_FIRMA_DELEGATO, utente, null, false, nodoList, inApprovazioneDirigente, fpeh, connection);
						outputCount.put(nodo.getIdNodo().intValue() + "|DELEGATO", workFlowDelegato.fetchCount());
					} else {
						boolean haLibroFirma = false;
						boolean haLibroFirmaFEPA = false;
						for (final Long ruoloId : idRuoli) {
							final Ruolo ruolo = ruoloDAO.getRuolo(ruoloId, connection);
							if (ruolo.getDataDisattivazione() == null && PermessiUtils.haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.LIBROFIRMA, ruolo.getPermessi().longValue(), nodo.getAoo().getParametriAOO(), nodo.getIdTipoNodo())) {
								haLibroFirma = true;
							}
							if (ruolo.getDataDisattivazione() == null && PermessiUtils.haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.DECRETI_DIRIGENTE, ruolo.getPermessi().longValue(), nodo.getAoo().getParametriAOO(), nodo.getIdTipoNodo())) {
								haLibroFirmaFEPA = true;		
							}
						}
						
						if (haLibroFirma) {
							final VWQueueQuery workFlow = listaDocSRV.getQueueFilenetWidget(DocumentQueueEnum.NSD, utente, null, false, nodoList, inApprovazioneDirigente, fpeh, connection);
							outputCount.put(nodo.getIdNodo().intValue() + "", workFlow.fetchCount());
						}
						
						if (haLibroFirmaFEPA) {
							final VWQueueQuery workFlow = (VWQueueQuery) listaDocSRV.getFepaQueryObject(DocumentQueueEnum.DD_DA_FIRMARE, null, utente, nodo, fpeh);
							outputCount.put(nodo.getIdNodo().intValue() + "|FEPA", workFlow.fetchCount());
						}
						
					}		
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il counter dei sottouffici nell'organigramma ", e);
			rollbackConnection(connection);
		} finally {
			logoff(fpeh);
			closeConnection(connection);
		}
		return outputCount;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICounterFacadeSRV#countTotSottoUfficiWidgetStruttura(java.util.List,
	 *      it.ibm.red.business.dto.UtenteDTO, boolean).
	 */
	@Override
	public Map<Integer, Integer> countTotSottoUfficiWidgetStruttura(final List<Nodo> listUffici, final UtenteDTO utente, final boolean inApprovazioneDirigente) {
		final Map<Integer, Integer> outputCount = new HashMap<>(); 
		Connection connection = null;
		FilenetPEHelper fpeh = null; 
		
		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO()); 
			connection = setupConnection(getDataSource().getConnection(), false);
		 
			if (listUffici != null) {
				for (final Nodo nodo : listUffici) {
					final List<Nodo> nodoList = new ArrayList<>();
					nodoList.add(nodo);
					final VWQueueQuery workFlow = listaDocSRV.getQueueFilenetWidget(DocumentQueueEnum.NSD, utente, null, false, nodoList, inApprovazioneDirigente, fpeh, connection);
					outputCount.put(nodo.getIdNodo().intValue(), workFlow.fetchCount());
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il counter dei sottouffici nell'organigramma ", e);
			rollbackConnection(connection);
		} finally {
			logoff(fpeh);
			closeConnection(connection);
		}
		return outputCount;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICounterFacadeSRV#measure(java.lang.String,
	 *      java.lang.Boolean).
	 */
	@Override
	public PerformanceDTO measure(final String username, final Boolean isFull) {
		
		final Long startTimeTotal = new Date().getTime();

		Long startTime = new Date().getTime();

		Long stopTime = new Date().getTime();
		final Long getConnectionMS = stopTime - startTime;

		startTime = new Date().getTime();
		final UtenteDTO utente = utenteSRV.getByUsername(username);
		stopTime = new Date().getTime();
		final Long getUserMS = stopTime - startTime;
		LOGGER.info("### RECUPERO DATI UTENTE ### [" + getUserMS + "ms]");

		Connection connectionApp = null;
		Connection connectionDwh = null;
		PerformanceDTO output = null;
		try {
			connectionApp = setupConnection(getDataSource().getConnection(), false);
			connectionDwh = setupConnection(getFilenetDataSource().getConnection(), false);

			startTime = new Date().getTime();
			codeAppDAO.countCodeAppl(utente.getIdUfficio(), utente.getId(), DocumentQueueEnum.MOZIONE.getIdStatiLavorazione(), connectionApp);
			stopTime = new Date().getTime();
			final Long dbApplicativoMS = stopTime - startTime;
			LOGGER.info("### DATABASE APPLICATIVO ### [" + dbApplicativoMS + "ms]");
			
			startTime = new Date().getTime();
			sottoscrizioniDAO.getCountSottoscrizioni(connectionDwh, utente.getId(), 30, utente.getIdAoo());
			stopTime = new Date().getTime();
			final Long dbDWHMS = stopTime - startTime;
			LOGGER.info("### DATABASE DWH ### [" + dbDWHMS + "ms]");

			startTime = new Date().getTime();
			listaDocSRV.getQueueFilenet(DocumentQueueEnum.CORRIERE, utente, null, connectionApp);
			stopTime = new Date().getTime();
			final Long fnetPEMS = stopTime - startTime;
			LOGGER.info("### PE ### [" + fnetPEMS + "ms]");

			startTime = new Date().getTime();
			casellePostaliSRV.getCasellePostali(utente, FunzionalitaEnum.MAIL_LEAF);
			stopTime = new Date().getTime();
			final Long fnetCEMS = stopTime - startTime;
			LOGGER.info("### CE ### [" + fnetCEMS + "ms]");

			startTime = new Date().getTime();
			ricercaSRV.ricercaRapidaUtente("irene", 2019, RicercaGenericaTypeEnum.TUTTE, RicercaPerBusinessEntityEnum.FASCICOLI, false, utente, false);
			stopTime = new Date().getTime();
			final Long ricercaGenericaFascicoliMS = stopTime - startTime;
			LOGGER.info("### RICERCA GENERICA FASCICOLI ### [" + ricercaGenericaFascicoliMS + "ms]");
			
			final Calendar calStart = Calendar.getInstance();
			calStart.set(Calendar.YEAR, 2019);
			calStart.set(Calendar.MONTH, 1);
			calStart.set(Calendar.DAY_OF_MONTH, 1);
			final Calendar calStop = Calendar.getInstance();
			calStop.set(Calendar.YEAR, 2019);
			calStop.set(Calendar.MONTH, 1);
			calStop.set(Calendar.DAY_OF_MONTH, 6);
			final ParamsRicercaAvanzataFascDTO paramsRicercaAvanzataFascicoli = new ParamsRicercaAvanzataFascDTO();
			paramsRicercaAvanzataFascicoli.setDataCreazioneDa(calStart.getTime());
			paramsRicercaAvanzataFascicoli.setDataCreazioneA(calStop.getTime());
			paramsRicercaAvanzataFascicoli.setStatoFascicolo(FilenetStatoFascicoloEnum.APERTO);
			startTime = new Date().getTime();
			ricercaAvanzataSRV.eseguiRicercaFascicoli(paramsRicercaAvanzataFascicoli, utente, false);
			stopTime = new Date().getTime();
			final Long ricercaFascicoliMS = stopTime - startTime;
			LOGGER.info("### RICERCA FASCICOLI ### [" + ricercaFascicoliMS + "ms]");
			
			
			Long npsMS = null;
			Long iTextMS = null;
			Long adobeMS = null;
			Long pkMS = null;
			
			if (Boolean.TRUE.equals(isFull)) {
				startTime = new Date().getTime();
				final Date yesterday = DateUtils.addDay(DateUtils.dropTimeInfo(new Date()), -1);
				final NpsConfiguration npsConfigurationAoo = npsConfigurationDAO.getByIdAoo(utente.getIdAoo().intValue(), connectionApp);
				
				final OrgOrganigrammaType operatore = Builder.buildOrganigrammaPf(String.valueOf(utente.getId()),
						utente.getCognome(), utente.getNome(), utente.getCodFiscale(), npsConfigurationAoo.getCodiceAmministrazione(),
						npsConfigurationAoo.getDenominazioneAmministrazione(), npsConfigurationAoo.getCodiceAoo(),
						npsConfigurationAoo.getDenominazioneAmministrazione(), utente.getCodiceUfficio(),
						utente.getNodoDesc(), String.valueOf(utente.getIdUfficio()));

				final RispostaReportRegistroGiornalieroType resp = BusinessDelegate.Report.registroGiornaliero(utente.getIdAoo().intValue(), yesterday, yesterday, operatore, npsConfigurationAoo.getRegistroUfficiale(), npsConfigurationAoo.getDenominazioneRegistro());
				stopTime = new Date().getTime();
				npsMS = stopTime - startTime;
				LOGGER.info("### NPS ### [" + npsMS + "ms]");

				startTime = new Date().getTime();
				byte[] pdf = null;
				if (resp.getReport() != null && !resp.getReport().isEmpty()) {
					pdf = resp.getReport().iterator().next().getContent();
					PdfHelper.getNumberOfPages(pdf);
				}
				stopTime = new Date().getTime();
				iTextMS = stopTime - startTime;
				LOGGER.info("### ITEXT ### [" + iTextMS + "ms]");

				startTime = new Date().getTime();
				new AdobeLCHelper().insertPostillaCopiaConforme("Misurazione performance", pdf);
				stopTime = new Date().getTime();
				adobeMS = stopTime - startTime;
				LOGGER.info("### ADOBE ### [" + adobeMS + "ms]");

				startTime = new Date().getTime();
				final PkHandler pkHandler = utente.getSignerInfo().getPkHandlerVerifica();
				//START VI PK HANDLER
				final SignHelper lsh = new SignHelper(pkHandler.getHandler(), pkHandler.getSecurePin(), utente.getDisableUseHostOnly());
				lsh.infoFirmaDocumento(new ByteArrayInputStream(pdf), false);
				stopTime = new Date().getTime();
				pkMS = stopTime - startTime;
				LOGGER.info("### PK ### [" + pkMS + "ms]");

			}

			final Long stopTimeTotal = new Date().getTime();
			final Long timeTotal = stopTimeTotal - startTimeTotal;
			LOGGER.info("### TOTAL TIME ### [" + timeTotal + "ms]");
			output = new PerformanceDTO(dbApplicativoMS, dbDWHMS, fnetPEMS, fnetCEMS, getUserMS, getConnectionMS, npsMS, adobeMS, pkMS, iTextMS, stopTimeTotal - startTimeTotal, ricercaGenericaFascicoliMS, ricercaFascicoliMS);
		} catch (final Exception e) {
			LOGGER.error("Errore durante la misurazione", e);
		} finally {
			closeConnection(connectionApp);
			closeConnection(connectionDwh);
		}
		return output;
	} 
}