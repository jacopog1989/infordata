package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.dto.NotificaInteroperabilitaEmailDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.CodaEmail; 

/**
 * Facade del servizio di gestione coda mail.
 */
public interface ICodaMailFacadeSRV extends Serializable {

	/**
	 * Il metodo recupera i primi numItemToSend item della BATCHRED_CODAMAIL da inviare.
	 * Prende in carico gli item modificandone opportunamente lo stato.
	 * 
	 * @param idAoo ID dell'AOO
	 * @param isPostaInterna Indica se la posta dell'AOO è gestita dall'applicativo (Posta Interna) o no (Posta Esterna)
	 * @param numItemToSend
	 * @return
	 */
	List<MessaggioEmailDTO> recuperaMailDaInviare(int idAoo, boolean isPostaInterna, Integer numItemToSend);
	
	/**
	 * Gestisce l'invio della mail in input e le attività a contorno (recupero allegati su Filenet, aggiornamento elenco trasmissioni, etc...).
	 * 
	 * @param msgNext
	 * @param aoo
	 */
	void invioMailDaCoda(MessaggioEmailDTO msgNext, Aoo aoo);
	
	/**
	 * Spedisci il protocollo e aggiorna l'item in coda IN ATTESA DI SPEDIZIONE DA PROTOCOLLO
	 * @param msgNext
	 * @param idAoo
	 */
	void spedisciProtocolloNPSDaCoda(MessaggioEmailDTO msgNext, int idAoo);

	/**
	 * Recupera gli item della BATCHRED_CODAMAIL per i quali si è in attesa di notifiche, 
	 * le verifica ed eventualmente ne aggiorna lo stato sulla base dati.
	 * 
	 * @param fceh
	 * @param notificheEmailToCheck
	 */
	void checkAndUpdateNotificaMailStatus(IFilenetCEHelper fceh, Aoo aoo);

	/**
	 * Aggiorna lo stato delle notifiche relative ai documenti "riconciliati" con tutte le notifiche.
	 * 
	 * @param fpeh
	 * @param aoo
	 * @param fceh
	 */
	void chiudiDocumentiRiconciliati(FilenetPEHelper fpeh, IFilenetCEHelper fceh, Aoo aoo);

	/**
	 * Aggiorna lo stato delle notifiche relative ai documenti con destinatari non solo elettronici.
	 * 
	 * @param fceh
	 * @param fpeh
	 * @param aoo
	 */
	void chiudiDocumentiDestinatariMisti(IFilenetCEHelper fceh, FilenetPEHelper fpeh, Aoo aoo);

	/**
	 * Controlla che la mail sia stata inviata.
	 * @param idDocumento - id documento
	 * @param destinatario
	 * @return true o false
	 */
	boolean checkEmailInviata(String idDocumento, String[] destinatario);

	/**
	 * Ottiene le notifiche PEC.
	 * @param utente
	 * @param documentTitle - document title
	 * @param mittente
	 * @param path
	 * @return lista di notifiche
	 */
	List<EmailDTO> getNotifichePECList(UtenteDTO utente, String documentTitle, String mittente, String path);

	/**
	 * Esegue il reinvio della mail.
	 * @param utente
	 * @param idDocumento id documento
	 * @param mailGuid
	 * @param indirizzoEmailMittente - indirizzo email del mittente
	 * @param indirizzoEmailDestinatarioMittente - indirizzo email del destinatario mittente
	 * @param oldEmailDestinatarioReinvia - indirizzo email del destinatario
	 * @param numeroProtocollo - numero di protocollo
	 * @param annoProtocollo - anno di protocollo
	 * @param idProtocollo - id del protocollo
	 * @param clickMailTO - clickMailTO
	 */
	void reinviaMail(UtenteDTO utente, String idDocumento, String mailGuid, String indirizzoEmailMittente,
			String indirizzoEmailDestinatarioMittente, String oldEmailDestinatarioReinvia, String numeroProtocollo, String annoProtocollo, String idProtocollo,
			boolean clickMailTO);
	
	/**
	 * Ottiene le notifiche PEC.
	 * @param utente
	 * @param documentTitle - document title
	 * @param mittente
	 * @param path
	 * @return mappa mittente notifica
	 */
	Map<String, EmailDTO> getNotifichePEC(UtenteDTO utente, String documentTitle, String mittente, String path);

	/**
	 * Ottiene le notifiche PEC.
	 * @param utente
	 * @param documentTitle - document title
	 * @param mittente
	 * @param path
	 * @param isTabSpedizione
	 * @return mappa mittente notifica
	 */
	Map<String, EmailDTO> getNotifichePEC(UtenteDTO utente, String documentTitle, String mittente, String path, boolean isTabSpedizione);

	/**
	 * Ottiene le notifiche di interoperabilità per il protocollo.
	 * @param idProtocollo - id del protocollo
	 * @param numeroProtocollo - numero di protocollo
	 * @param annoProtocollo - anno di protocollo
	 * @param utente
	 * @return lista di notifiche di interoperabilità
	 */
	List<NotificaInteroperabilitaEmailDTO> getNotificheInteroperabilita(String idProtocollo, Integer numeroProtocollo, Integer annoProtocollo, UtenteDTO utente);
    
	/**
	 * Invia il messaggio di posta su Nps.
	 * @param msgNext - messaggio della mail
	 * @param aoo
	 */
	void inviaMessaggioPostaNps(MessaggioEmailDTO msgNext, Aoo aoo);

	/**
	 * Restituisce la mappa delle notifiche per l'icon stato.
	 * @param documentTitle
	 * @param idAoo
	 * @return mappa notifiche
	 */
	public Map<String, List<CodaEmail>> getNotificheForIconaStato(final String documentTitle, final Long idAoo);

	Map<String, List<CodaEmail>> getNotificheForIconaStatoReinvia(String documentTitle, Long idAoo);

}