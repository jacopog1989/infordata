package it.ibm.red.business.template;

import java.io.Serializable;

import it.ibm.red.business.dto.DetailEmailDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedDTO;

/**
 * Abstract Template.
 */
public abstract class Template implements Serializable {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = -1739553917851257631L;

	/**
	 * Genera l'headerTable HTML per il documento in input.
	 * @param documento
	 * @return sb
	 */
	public abstract StringBuilder getHeaderTable(SalvaDocumentoRedDTO documento);

	/**
	 * Crea la sezione dettaglio documento in HTML.
	 * @param documento
	 * @return sb
	 */
	public abstract StringBuilder getDettaglioDocumentoTable(SalvaDocumentoRedDTO documento);

	/**
	 * Crea il footerTable per il documento in input.
	 * @param documento
	 * @return sb
	 */
	public abstract StringBuilder getFooterTable(SalvaDocumentoRedDTO documento);

	/**
	 * Genera l'headerTable HTML per l'email in input.
	 * @param email
	 * @return sb
	 */
	public abstract StringBuilder getHeaderTableEmail(DetailEmailDTO email);

	/**
	 * Crea la sezione dettaglio dell'email in HTML.
	 * @param email
	 * @return sb
	 */
	public abstract StringBuilder getDettaglioDocumentoTableEmail(DetailEmailDTO email);

	/**
	 * Crea la sezione dettaglio dell'email in plain text.
	 * @param email
	 * @return null
	 */
	public abstract StringBuilder getDettaglioDocumentoTableEmailPlainText(DetailEmailDTO email);
}
