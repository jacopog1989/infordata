package it.ibm.red.business.service.sign.strategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.SignErrorEnum;
import it.ibm.red.business.enums.SignModeEnum;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IASignSRV;
import it.ibm.red.business.service.ISignSRV;
import it.ibm.red.business.service.concrete.AbstractService;

/**
 * Abstract della strategy di firma.
 */
@Service
public abstract class AbstractSignStrategy extends AbstractService implements ISignStrategy {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 7315809137760147577L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AbstractSignStrategy.class.getName());

	/**
	 * Il service che gestisce la firma sincrona.
	 */
	@Autowired
	protected ISignSRV signSRV;

	/**
	 * Il service che gestisce la firma asincrona.
	 */
	@Autowired
	private IASignSRV aSignSRV;
	
	/**
	 * Esegue le azioni successive alla firma locale.
	 * 
	 * @param signTransactionId
	 *            transactionId dell'operazione di firma
	 * @param signOutcome
	 *            esito della firma locale
	 * @param utenteFirmatario
	 *            utente responsabile della firma locale
	 * @param signType
	 *            tipologia della firma applicata, @see SignTypeEnum
	 * @param copiaConforme
	 *            flag associato alla copia conforme
	 */
	@Override
	public final void postFirmaLocale(final String signTransactionId, final EsitoOperazioneDTO signOutcome, final UtenteDTO utenteFirmatario, final SignTypeEnum signType, final boolean copiaConforme) {
		aSignSRV.gestisciContentsFirmati(signTransactionId, Arrays.asList(signOutcome), utenteFirmatario, SignModeEnum.LOCALE, signType, copiaConforme);
	}
	
	/**
	 * Esegue le operazioni precedenti alla firma dei contents identificati dai
	 * wobnumbers: <code> wobNumbersToProcess </code>.
	 * @param signTransactionId
	 *            transactionId dell'operazione di firma
	 * @param wobNumbersToProcess
	 *            identificativi documenti da firmare
	 * @param utenteFirmatario
	 *            utente responsabile della firma
	 * @return collection degli esiti associati alle firme
	 */
	protected abstract Collection<EsitoOperazioneDTO> operazioniPreFirmaContents(String signTransactionId, Collection<String> wobNumbersToProcess, UtenteDTO utenteFirmatario);
	
	
	/**
	 * @param wobNumbers
	 * @param utenteFirmatario
	 * @param signMode
	 * @param firmaMultipla
	 * @return
	 */
	protected final Collection<EsitoOperazioneDTO> firma(String signTransactionId, Collection<String> wobNumbers, UtenteDTO utenteFirmatario, SignModeEnum signMode, Boolean firmaMultipla) {
		return firma(signTransactionId, wobNumbers, utenteFirmatario, signMode, null, null, null, null, firmaMultipla, null);
	}


	/**
	 * @param signTransactionId
	 * @param wobNumbers
	 * @param utenteFirmatario
	 * @param signMode
	 * @param pin
	 * @param otp
	 * @param signType
	 * @param principaleOnlyPAdESVisible
	 * @param firmaMultipla
	 * @param protocolloEmergenza
	 * @return
	 */
	protected final Collection<EsitoOperazioneDTO> firma(String signTransactionId, Collection<String> wobNumbers, UtenteDTO utenteFirmatario, SignModeEnum signMode, String pin, String otp, 
			SignTypeEnum signType, Boolean principaleOnlyPAdESVisible, Boolean firmaMultipla, ProtocolloEmergenzaDocDTO protocolloEmergenza) {
		
		Collection<EsitoOperazioneDTO> signOutcomes = new ArrayList<>();
		Collection<EsitoOperazioneDTO> stepOutcomes;
		Collection<String> wobNumbersToProcess = wobNumbers;
		
		// ### ESCLUSIONE DOCUMENTI GIÀ FIRMATI (PRESENTI NELLA CODA DI FIRMA ASINCRONA)
		wobNumbersToProcess = getWobNumbersNotAlreadyInQueue(signTransactionId, signOutcomes, wobNumbersToProcess, utenteFirmatario);
		
		LOGGER.info(signTransactionId + " " + wobNumbersToProcess.size() + " wob number da elaborare");
		
		// ### ESECUZIONE DELLE OPERAZIONI PROPEDEUTICHE ALLA FIRMA DEI CONTENT
		stepOutcomes = operazioniPreFirmaContents(signTransactionId, wobNumbersToProcess, utenteFirmatario);
		
		LOGGER.info(signTransactionId + " operazioni pre firma completate");
		
		// Se la firma è LOCALE, la firma dei content viene eseguita dall'applet e la successiva gestione è poi richiamata direttamente da quest'ultima (cfr. postFirmaLocale)
		if (!SignModeEnum.LOCALE.equals(signMode)) {
			
			// ### FIRMA DEI CONTENT
			wobNumbersToProcess = getWobNumbersForNextStep(signOutcomes, stepOutcomes);
			
			LOGGER.info(signTransactionId + " " + wobNumbersToProcess.size() + " wob number da continuare a lavorare");
			
			stepOutcomes = firmaContents(signTransactionId, wobNumbersToProcess, utenteFirmatario, signMode, pin, otp, signType, principaleOnlyPAdESVisible, firmaMultipla, protocolloEmergenza);
			
			LOGGER.info(signTransactionId + " documenti firmati");
			
			// ### GESTIONE DEI CONTENT FIRMATI
			signOutcomes.addAll(stepOutcomes);
			aSignSRV.gestisciContentsFirmati(signTransactionId, signOutcomes, utenteFirmatario, signMode, signType, false);
			
			LOGGER.info(signTransactionId + " operazioni su documenti firmati completate");
		
		} else {
			signOutcomes.addAll(stepOutcomes);
		}
		
		return signOutcomes;
	}
	
	
	/**
	 * @param signTransactionId
	 * @param wobNumbersToProcess
	 * @param utenteFirmatario
	 * @param signMode
	 * @param pin
	 * @param otp
	 * @param signType
	 * @param principaleOnlyPAdESVisible
	 * @param firmaMultipla
	 * @param protocolloEmergenza
	 * @return
	 */
	protected final Collection<EsitoOperazioneDTO> firmaContents(String signTransactionId, Collection<String> wobNumbersToProcess, final UtenteDTO utenteFirmatario,
			final SignModeEnum signMode, final String pin, final String otp, final SignTypeEnum signType, final Boolean principaleOnlyPAdESVisible,
			final Boolean firmaMultipla, final ProtocolloEmergenzaDocDTO protocolloEmergenza) {
		Collection<EsitoOperazioneDTO> stepOutcomes;
		
		if (!SignModeEnum.AUTOGRAFA.equals(signMode)) {
			if (!Boolean.TRUE.equals(firmaMultipla)) {
				stepOutcomes = signSRV.firmaRemota(signTransactionId, signType, wobNumbersToProcess, utenteFirmatario, otp, pin, Boolean.TRUE.equals(principaleOnlyPAdESVisible));	// REMOTA
			} else {
				stepOutcomes = signSRV.firmaRemotaMultipla(signTransactionId, signType, wobNumbersToProcess, utenteFirmatario, otp, pin, Boolean.TRUE.equals(principaleOnlyPAdESVisible)); // REMOTA MULTIPLA
			}
		} else {
			stepOutcomes = signSRV.firmaAutografa(signTransactionId, wobNumbersToProcess, utenteFirmatario, protocolloEmergenza); // AUTOGRAFA
		}
		
		return stepOutcomes;
	}
	
	
	/**
	 * Restituisce la collection di wob number con esito positivo, che quindi possono essere lavorati da un eventuale step successivo.
	 * Inserisce i wob numbers K.O. nella collection degli esiti da restituire al termine del processo sincrono di firma.
	 * 
	 * @param signOutcomes Esiti da restituire in output al termine del processo sincrono di firma.
	 * @param stepOutcomes Esiti dello step appena concluso
	 * @return
	 */
	protected static final Collection<String> getWobNumbersForNextStep(final Collection<EsitoOperazioneDTO> signOutcomes, final Collection<EsitoOperazioneDTO> stepOutcomes) {
		Collection<String> wobNumbersToProcess = new ArrayList<>();
		
		Iterator<EsitoOperazioneDTO> itStepOutcomes = stepOutcomes.iterator();
		while (itStepOutcomes.hasNext()) {
			EsitoOperazioneDTO stepOutcome = itStepOutcomes.next();
			
			if (stepOutcome.isEsito()) {
				wobNumbersToProcess.add(stepOutcome.getWobNumber());
			} else {
				signOutcomes.add(stepOutcome);
			}
		}
		
		return wobNumbersToProcess;
	}
	
	
	/**
	 * @param signOutcomes
	 * @param wobNumbersToSign
	 * @param utente
	 * @return
	 */
	private final Collection<String> getWobNumbersNotAlreadyInQueue(final String signTransactionId, final Collection<EsitoOperazioneDTO> signOutcomes, final Collection<String> wobNumbersToSign,
			final UtenteDTO utenteFirmatario) {
		Collection<String> wobNumbersToProcess = new ArrayList<>();
		
		if (!CollectionUtils.isEmpty(wobNumbersToSign)) {
			Map<String, ASignItemDTO> wobNumbersToSkip = aSignSRV.filterWorkflowForAsyncSignAlreadyStarted(signTransactionId, wobNumbersToSign, utenteFirmatario);
			
			if (!MapUtils.isEmpty(wobNumbersToSkip)) {
				EsitoOperazioneDTO esitoFirma;
				String wobNumberToSign;
				ASignItemDTO aSignItem;
				
				Iterator<String> itWobNumbersToSign = wobNumbersToSign.iterator();
				while (itWobNumbersToSign.hasNext()) {
					wobNumberToSign = itWobNumbersToSign.next();
					
					// Se uno dei documenti da firmare è già presente nella coda di firma asincrona (PREPARAZIONE ALLA SPEDIZIONE),
					// il documento non viene processato e si restituisce esito negativo per quel documento.
					aSignItem = wobNumbersToSkip.get(wobNumberToSign);
					if (aSignItem != null) {
						esitoFirma = new EsitoOperazioneDTO(wobNumberToSign, aSignItem.getDocumentTitle());
						esitoFirma.setIdDocumento(aSignItem.getNumeroDocumento());
						esitoFirma.setNumeroProtocollo(aSignItem.getNumeroProtocollo());
						esitoFirma.setAnnoProtocollo(aSignItem.getAnnoProtocollo());
						esitoFirma.setCodiceErrore(SignErrorEnum.DOCUMENTO_GIA_FIRMATO_ASIGN_COD_ERROR);
						esitoFirma.setNote(SignErrorEnum.DOCUMENTO_GIA_FIRMATO_ASIGN_COD_ERROR.getMessage());
						
						signOutcomes.add(esitoFirma);
					} else {
						wobNumbersToProcess.add(wobNumberToSign);
					}
				}
			} else {
				wobNumbersToProcess = wobNumbersToSign;
			}
		}
		
		return wobNumbersToProcess;
	}
	
	/**
	 * Esegue il processo di firma remota sui documenti identificati dai wobnumber:
	 * <code> wobNumbers </code>.
	 * 
	 * @param signTransactionId
	 *            transactionId dell'operazione di firma
	 * @param wobNumbers
	 *            wobnumbers dei documenti da firmare
	 * @param pin
	 *            pin per la firma digitale
	 * @param otp
	 *            one-time-password per la firma digitale
	 * @param utenteFirmatario
	 *            utente firmatario dei documenti identificati dai
	 *            <code> wobNumbers </code>
	 * @param signType
	 *            tipologia della firma, @see SignTypeEnum
	 * @param principaleOnlyPAdESVisible
	 *            flag associato alla visibilità PADES
	 * @param firmaMultipla
	 *            true se firma multipla, false altrimenti
	 * @return gli esiti delle firme per ogni documenti
	 */
	@Override
	public final Collection<EsitoOperazioneDTO> firmaRemota(final String signTransactionId, final Collection<String> wobNumbers, final String pin, final String otp, 
			final UtenteDTO utenteFirmatario, final SignTypeEnum signType, final boolean principaleOnlyPAdESVisible, final boolean firmaMultipla) {
		LOGGER.info(signTransactionId + " -> START");
		Collection<EsitoOperazioneDTO> signOutcomes = firma(signTransactionId, wobNumbers, utenteFirmatario, SignModeEnum.REMOTA, pin, otp, signType, principaleOnlyPAdESVisible, firmaMultipla, null);
		LOGGER.info(signTransactionId + " -> END");
		return signOutcomes;
	}


	/**
	 * Esegue il processo di firma autografa sui documenti identificati dai
	 * <code> wobNumbers </code>.
	 * 
	 * @param signTransactionId
	 *            transactionId dell'operazione di firma
	 * @param wobNumbers
	 *            identificativi univoci dei documenti
	 * @param utenteFirmatario
	 *            utente responsabile per la firma dei documenti identificati dai
	 *            <code> wobNumbers </code>
	 * @param protocolloEmergenza
	 *            protocollo emergenza specificato
	 * @return lista degli esiti associati ad ogni documento mandato in firma
	 */
	@Override
	public final Collection<EsitoOperazioneDTO> firmaAutografa(final String signTransactionId, final Collection<String> wobNumbers, final UtenteDTO utenteFirmatario, 
			final ProtocolloEmergenzaDocDTO protocolloEmergenza) {
		LOGGER.info(signTransactionId + " -> START");
		Collection<EsitoOperazioneDTO> signOutcomes = firma(signTransactionId, wobNumbers, utenteFirmatario, SignModeEnum.AUTOGRAFA, null, null, null, null, null, protocolloEmergenza);
		LOGGER.info(signTransactionId + " -> END");
		return signOutcomes;
	}

	/**
	 * Esegue le azioni precedenti alla firma locale del documento identificato dal:
	 * <code> wobNumber </code>.
	 * 
	 * @param signTransactionId
	 *            transactionId dell'operazione di firma
	 * @param wobNumber
	 *            identificativo univoco del documento in firma
	 * @param utenteFirmatario
	 *            utente responsabile per la firma locale del documento identificato
	 *            dal: <code> wobNumber </code>
	 * @param signType
	 *            tipologia di firma, @see SignTypeEnum
	 * @param firmaMultipla
	 *            flag che indica se la firma locale è una firma multipla
	 * @return esito della firma
	 */
	@Override
	public final EsitoOperazioneDTO preFirmaLocale(final String signTransactionId, final String wobNumber, final UtenteDTO utenteFirmatario, final SignTypeEnum signType, final boolean firmaMultipla) {
		String logId = signTransactionId + " [preFirmaLocale]";
		LOGGER.info(logId + " -> START");
		Collection<EsitoOperazioneDTO> signOutcomes = firma(logId, Arrays.asList(wobNumber), utenteFirmatario, SignModeEnum.LOCALE, firmaMultipla);
		LOGGER.info(logId + " -> END");
		return signOutcomes.iterator().next();
	}
	
}
