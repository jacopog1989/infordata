package it.ibm.red.business.monitoring.attodecreto;

import it.ibm.red.business.dto.RaccoltaFadDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Classe che gestisce il monitoring dell'Atto Decreto.
 */
public class AttoDecretoMonitoring extends AttoDecretoParsedResponse {

	/**
	 * Identificativo monitorning.
	 */
	private Integer idMonitoring;

	/**
	 * Identificativo documento.
	 */
	private Integer idDocumento;

	/**
	 * Numero protocollo.
	 */
	private Integer numeroProtocollo;

	/**
	 * Anno protocollo.
	 */
	private Integer annoProtocollo;

	/**
	 * Identificativo aoo.
	 */
	private Integer idAoo;

	/**
	 * Identificativo ufficio.
	 */
	private Integer idUfficio;

	/**
	 * Identificativo utente.
	 */
	private Integer idUtente;

	/**
	 * Operazione.
	 */
	private String operazione;

	/**
	 * Nomefile.
	 */
	private String nomeFile;

	/**
	 * Request.
	 */
	private String request;

	/**
	 * Response.
	 */
	private String response;

	/**
	 * Stacktrace.
	 */
	private String stackTrace;

	/**
	 * Costruttore di classe.
	 * @param idDocumento
	 * @param idAoo
	 * @param numeroProtocollo
	 * @param annoProtocollo
	 * @param idUfficio
	 * @param idUtente
	 */
	public AttoDecretoMonitoring(final Integer idDocumento, final Integer idAoo, 
			final Integer numeroProtocollo, final Integer annoProtocollo,
			final Integer idUfficio, final Integer idUtente) {
		super();
		this.idDocumento = idDocumento;
		this.numeroProtocollo = numeroProtocollo;
		this.annoProtocollo = annoProtocollo;
		this.idAoo = idAoo;
		this.idUfficio = idUfficio;
		this.idUtente = idUtente;
	}

	/**
	 * Costruttore di classe.
	 * @param fdaDTO
	 * @param utente
	 * @param idRaccoltaProvvisoria
	 */
	public AttoDecretoMonitoring(final RaccoltaFadDTO fdaDTO, final UtenteDTO utente,
			final String idRaccoltaProvvisoria) {
		this(fdaDTO, utente);
		setIdRaccoltaProvvisoria(idRaccoltaProvvisoria);
	}

	/**
	 * Costruttore di classe.
	 * @param fdaDTO
	 * @param utente
	 * @param idRaccoltaProvvisoria
	 * @param nomeFile
	 */
	public AttoDecretoMonitoring(final RaccoltaFadDTO fdaDTO, final UtenteDTO utente,
			final String idRaccoltaProvvisoria, final String nomeFile) {
		this(fdaDTO, utente, idRaccoltaProvvisoria);
		this.nomeFile = nomeFile;
	}

	/**
	 * Costruttore di classe.
	 * @param fdaDTO
	 * @param utente
	 */
	public AttoDecretoMonitoring(final RaccoltaFadDTO fdaDTO, final UtenteDTO utente) {
		this(Integer.parseInt(fdaDTO.getDocumentTitle()), utente.getIdAoo().intValue(), Integer.parseInt(fdaDTO.getNumeroProtocollo()), 
				Integer.parseInt(fdaDTO.getAnnoProtocollo()), utente.getIdUfficio().intValue(), utente.getId().intValue());
	}

	/**
	 * Restituisce l'id del monitoring.
	 * @return
	 */
	public Integer getIdMonitoring() {
		return idMonitoring;
	}

	/**
	 * Imposta l'id del monitoring.
	 * @param idMonitoring
	 */
	public void setIdMonitoring(final Integer idMonitoring) {
		this.idMonitoring = idMonitoring;
	}

	/**
	 * Restituisce l'id del documento.
	 * @return
	 */
	public Integer getIdDocumento() {
		return idDocumento;
	}

	/**
	 * Imposta l'id del documento.
	 * @param idDocumento
	 */
	public void setIdDocumento(final Integer idDocumento) {
		this.idDocumento = idDocumento;
	}

	/**
	 * Restituisce il numero del protocollo.
	 * @return numeroProtocollo
	 */
	public Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Imposta il numero del protocollo.
	 * @param numeroProtocollo
	 */
	public void setNumeroProtocollo(final Integer numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	/**
	 * Restituisce l'anno del protocollo.
	 * @return annoProtocollo
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * Imposta l'anno del protocollo.
	 * @param annoProtocollo
	 */
	public void setAnnoProtocollo(final Integer annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/**
	 * Restituisce l'id dell' AOO.
	 * @return
	 */
	public Integer getIdAoo() {
		return idAoo;
	}

	/**
	 * Imposta l'id dell'aoo.
	 * @param idAoo
	 */
	public void setIdAoo(final Integer idAoo) {
		this.idAoo = idAoo;
	}

	/**
	 * Restituisce l'id dell'ufficio.
	 * @return idUfficio
	 */
	public Integer getIdUfficio() {
		return idUfficio;
	}

	/**
	 * Imposta l'id dell'ufficio.
	 * @param idUfficio
	 */
	public void setIdUfficio(final Integer idUfficio) {
		this.idUfficio = idUfficio;
	}

	/**
	 * Restituisce l'id dell'utente.
	 * @return
	 */
	public Integer getIdUtente() {
		return idUtente;
	}

	/**
	 * Imposta l'id dell'utente.
	 * @param idUtente
	 */
	public void setIdUtente(final Integer idUtente) {
		this.idUtente = idUtente;
	}

	/**
	 * Restituisce l'operazione definita da un testo.
	 * @return operazione come String
	 */
	public String getOperazione() {
		return operazione;
	}

	/**
	 * Imposta l'operazione come descrizione.
	 * @param operazione
	 */
	public void setOperazione(final String operazione) {
		this.operazione = operazione;
	}

	/**
	 * Restituisce il nome del file.
	 * @return nomeFile
	 */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * Imposta il nome del file.
	 * @param nomeFile
	 */
	public void setNomeFile(final String nomeFile) {
		this.nomeFile = nomeFile;
	}

	/**
	 * Restituice la request definita da un testo.
	 * @return request come String
	 */
	public String getRequest() {
		return request;
	}

	/**
	 * Imposta la request come testo.
	 * @param request
	 */
	public void setRequest(final String request) {
		this.request = request;
	}

	/**
	 * Restituisce la response.
	 * @return response
	 */
	public String getResponse() {
		return response;
	}

	/**
	 * Imposta la response come testo.
	 * @param response
	 */
	public void setResponse(final String response) {
		this.response = response;
	}

	/**
	 * Restituisce lo stack trace come testo.
	 * @return stackTrace come String
	 */
	public String getStackTrace() {
		return stackTrace;
	}

	/**
	 * Imposta lo stack trace.
	 * @param stackTrace
	 */
	public void setStackTrace(final String stackTrace) {
		this.stackTrace = stackTrace;
	}

	/**
	 * Restituisce la stringa per il log, viene definita. 
	 * @return combinazione dell'id documento, del numero protocollo e dell'anno del protocollo
	 */
	public String getLogString() {
		return "[" + getIdDocumento() + "][" + getNumeroProtocollo() + "/" + getAnnoProtocollo() + "]";
	}
	
}