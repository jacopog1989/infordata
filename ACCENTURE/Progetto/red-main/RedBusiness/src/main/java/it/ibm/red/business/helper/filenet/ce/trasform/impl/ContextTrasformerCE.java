package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.collection.PageIterator;
import com.filenet.api.core.Document;

import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * Context Trasformer CE.
 * @param <T>
 */
public abstract class ContextTrasformerCE<T> extends TrasformerCE<T> {
	
	/**
	 * UID.
	 */
	private static final long serialVersionUID = -6383805075105624935L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContextTrasformerCE.class.getName());

	/**
	 * @param inEnumKey
	 */
	protected ContextTrasformerCE(final TrasformerCEEnum inEnumKey) {
		super(inEnumKey);
	}

	/**
	 * @param document
	 * @param connection
	 * @throws RedException
	 */
	public T trasform(final Document document, final Connection connection) {
		throw new RedException();
	}

	/**
	 * @param document
	 * @param context
	 * @return T
	 */
	public abstract T trasform(Document document, Map<ContextTrasformerCEEnum, Object> context);

	/**
	 * @param document
	 * @param connection
	 * @param context
	 * @return T
	 */
	public abstract T trasform(Document document, Connection connection, Map<ContextTrasformerCEEnum, Object> context);

	/**
	 * @param documentSet
	 * @param context
	 * @return T
	 */
	public final Collection<T> trasform(final DocumentSet documentSet, final Map<ContextTrasformerCEEnum, Object> context) {
		try {
			Collection<T> output = new ArrayList<>();
			Iterator<?> it = documentSet.iterator();
			while (it.hasNext()) {
				Document document = (Document) it.next();
				output.add(trasform(document, context));
			}
			return output;
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero delle informazioni del documento da FileNet (CE). ", e);
			throw new FilenetException(e);
		}
	}

	/**
	 * @param pi
	 * @param context
	 * @return Collection<T>
	 */
	public final Collection<T> trasform(final PageIterator pi, final Map<ContextTrasformerCEEnum, Object> context) {
		try {
			Collection<T> output = new ArrayList<>();
			
			if (pi != null && pi.nextPage()) {
				for (Object document : pi.getCurrentPage()) {
					Document doc = (Document) document;
					output.add(trasform(doc, context));
				}
			}
			
			return output;
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero delle informazioni del documento da FileNet (CE). ", e);
			throw new FilenetException(e);
		}
	}
}