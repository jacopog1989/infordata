package it.ibm.red.business.dto;

import java.util.Date;
import java.util.List;


/**
 * @author SimoneLungarella
 * DTO per la gestione dei parametri di ricerca per l'elenco divisionale.
 */
public class ParamsRicercaElencoDivisionaleDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 3493596459964244660L;

	/**
	 * Data assegnazione.
	 */
	private Date dataAssegnazione;
	
	/**
	 * Descrizione tipologia documento.
	 */
	private String tipologiaDocumento;
	
	/**
	 * Lista di utenti assegnatari.
	 */
	private List<UtenteDTO> utentiAssegnatari;
	
	/**
	 * Ufficio assegnatario.
	 */
	private UfficioDTO ufficioAssegnatario;
	
	/**
	 * Ufficio assegnante.
	 */
	private UfficioDTO ufficioAssegnante;
	
	
	/**
	 * Costruttore vuoto, inizializza i parametri non obbligatori.
	 */
	public ParamsRicercaElencoDivisionaleDTO() {
		initParams();
	}
	
	/**
	 * Costruttore che inizializza i parametri obbligatori.
	 */
	public ParamsRicercaElencoDivisionaleDTO (Date inDataAssegnazione, UfficioDTO inUfficioAssegnante, UfficioDTO inUfficioAssegnatario) {
		
		setDataAssegnazione(inDataAssegnazione);
		setUfficioAssegnante(inUfficioAssegnante);
		setUfficioAssegnatario(inUfficioAssegnatario);
		initParams();
	}

	/**
	 * Restituisce la data dell'assegnazione.
	 * @return data assegnazione
	 */
	public Date getDataAssegnazione() {
		return dataAssegnazione;
	}
	
	/**
	 * Imposta la data dell'assegnazione.
	 * @param dataAssegnazione
	 */
	public void setDataAssegnazione(Date dataAssegnazione) {
		this.dataAssegnazione = dataAssegnazione;
	}
	
	/**
	 * Restituisce la tipologia documento.
	 * @return tipologia documento
	 */
	public String getTipologiaDocumento() {
		return tipologiaDocumento;
	}
	
	/**
	 * Imposta la tipologia documento.
	 * @param tipologiaDocumento
	 */
	public void setTipologiaDocumento(String tipologiaDocumento) {
		this.tipologiaDocumento = tipologiaDocumento;
	}
	
	/**
	 * Restituisce la lista di utenti assegnatari.
	 * 
	 * @return lista utenti assegnatari
	 */
	public List<UtenteDTO> getUtentiAssegnatari() {
		return utentiAssegnatari;
	}

	/**
	 * Imposta la lista di utenti assegnatari.
	 * 
	 * @param utentiAssegnatari
	 */
	public void setUtentiAssegnatari(List<UtenteDTO> utentiAssegnatari) {
		this.utentiAssegnatari = utentiAssegnatari;
	}

	/**
	 * Restituisce l'ufficio assegnatario.
	 * 
	 * @return ufficio assegnatario
	 */
	public UfficioDTO getUfficioAssegnatario() {
		return ufficioAssegnatario;
	}

	/**
	 * Imposta l'ufficio assegnatario.
	 * 
	 * @param ufficioAssegnatario
	 */
	public void setUfficioAssegnatario(UfficioDTO ufficioAssegnatario) {
		this.ufficioAssegnatario = ufficioAssegnatario;
	}

	/**
	 * Restituisce l'ufficio assegnante.
	 * 
	 * @return ufficio assegnante
	 */
	public UfficioDTO getUfficioAssegnante() {
		return ufficioAssegnante;
	}

	/**
	 * Imposta l'ufficio assegnante.
	 * 
	 * @param ufficioAssegnante
	 */
	public void setUfficioAssegnante(UfficioDTO ufficioAssegnante) {
		this.ufficioAssegnante = ufficioAssegnante;
	}

	/**
	 * Inizializzazione a stringa vuota dei parametri String non obbligatori.
	 */
	private void initParams() {
		setTipologiaDocumento("");
	}
	
}
