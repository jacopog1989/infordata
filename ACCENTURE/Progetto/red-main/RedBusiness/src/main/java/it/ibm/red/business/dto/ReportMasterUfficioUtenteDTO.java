package it.ibm.red.business.dto;

import java.util.Date;

/**
 * DTO per il master dei report.
 * 
 * @author m.crescentini
 *
 */
public class ReportMasterUfficioUtenteDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 8394113223248958386L;

	/**
	 * Ufficio.
	 */
	private String ufficio;
	
	/**
	 * Anno.
	 */
	private String anno;
	
	/**
	 * Username.
	 */
	private String username;
	
	/**
	 * Numero documenti.
	 */
	private Integer numeroDocumenti;
	
	/**
	 * Nodo.
	 */
	private Long idNodo;
	
	/**
	 * Utente.
	 */
	private Long idUtente;
	
	/**
	 * Data da.
	 */
	private Date dataDa;
	
	/**
	 * Data a.
	 */
	private Date dataA;
	
	/**
	 * Restituisce l'ufficio.
	 * @return ufficio
	 */
	public String getUfficio() {
		return ufficio;
	}
	
	/**
	 * Imposta l'ufficio.
	 * @param ufficio
	 */
	public void setUfficio(final String ufficio) {
		this.ufficio = ufficio;
	}
	
	/**
	 * Restituisce l'anno.
	 * @return anno
	 */
	public String getAnno() {
		return anno;
	}
	
	/**
	 * Imposta l'anno.
	 * @param anno
	 */
	public void setAnno(final String anno) {
		this.anno = anno;
	}
	
	/**
	 * Restituisce il numero dei documenti.
	 * @return numero documenti
	 */
	public Integer getNumeroDocumenti() {
		return numeroDocumenti;
	}
	
	/**
	 * Imposta il numero dei documenti.
	 * @param numeroDocumenti
	 */
	public void setNumeroDocumenti(final Integer numeroDocumenti) {
		this.numeroDocumenti = numeroDocumenti;
	}
	
	/**
	 * Restituisce lo username.
	 * @return username
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * Imposta lo username.
	 * @param username
	 */
	public void setUsername(final String username) {
		this.username = username;
	}
	
	/**
	 * Restituisce l'id del nodo.
	 * @return id nodo
	 */
	public Long getIdNodo() {
		return idNodo;
	}
	
	/**
	 * Imposta l'id del nodo.
	 * @param idnodo
	 */
	public void setIdNodo(final Long idnodo) {
		this.idNodo = idnodo;
	}
	
	/**
	 * Restituisce l'id dell'utente.
	 * @return id utente
	 */
	public Long getIdUtente() {
		return idUtente;
	}
	
	/**
	 * Imposta l'id dell'utente.
	 * @param idutente
	 */
	public void setIdUtente(final Long idutente) {
		this.idUtente = idutente;
	}
	
	/**
	 * Restituisce la data iniziale del range di date.
	 * @return data inziale range di date
	 */
	public Date getDataDa() {
		return dataDa;
	}
	
	/**
	 * Imposta al data iniziale del range.
	 * @param dataDa
	 */
	public void setDataDa(final Date dataDa) {
		this.dataDa = dataDa;
	}
	
	/**
	 * Restituisce la data finale del range.
	 * @return data finale
	 */
	public Date getDataA() {
		return dataA;
	}
	
	/**
	 * Imposta la data finale del range.
	 * @param dataA
	 */
	public void setDataA(final Date dataA) {
		this.dataA = dataA;
	}
	
}
