package it.ibm.red.business.helper.notifiche.states;

import java.util.Map;

import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.helper.notifiche.IGestioneMailState;
import it.ibm.red.business.helper.notifiche.NotificaHelper;
import it.ibm.red.business.persistence.model.CodaEmail;

/**
 * Stato 3 - Chiusura.
 */
public class ChiusuraState3 implements IGestioneMailState {

	/**
	 * @see it.ibm.red.business.helper.notifiche.IGestioneMailState#getNextNotificaEmailToUpdate(it.ibm.red.business.helper.notifiche.NotificaHelper,
	 *      it.ibm.red.business.persistence.model.CodaEmail, java.util.Map,
	 *      boolean).
	 */
	@Override
	public CodaEmail getNextNotificaEmailToUpdate(final NotificaHelper nh, final CodaEmail notificaEmail, final Map<String, EmailDTO> notifiche, final boolean reInvio) {
		CodaEmail result = notificaEmail;
		nh.setGestioneMailState(null);
		return result;
	}

}
