package it.ibm.red.business.service.ws.facade;

import java.io.Serializable;

/**
 * The Interface IRedWsTrackFacadeSRV.
 * 
 * @author a.dilegge
 *
 * Facade servizio per il tracciamento delle invocazioni a RedEvo WS.
 */
public interface IRedWsTrackFacadeSRV extends Serializable {

	/**
	 * Log request.
	 * @param request
	 * @param serviceName
	 * @param remoteAddress
	 * @return idTrack
	 */
	Long logRequest(String request, String serviceName, String remoteAddress);

	/**
	 * Log response.
	 * @param idTrack
	 * @param esitoOperazione
	 * @param response
	 */
	void logResponse(Long idTrack, Integer esitoOperazione, String response); 
	
}
