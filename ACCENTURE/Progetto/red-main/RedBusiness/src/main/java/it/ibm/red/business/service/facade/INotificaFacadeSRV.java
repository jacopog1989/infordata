package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.NotificaDTO;

/**
 * Facade del servizio che gestisce le notifiche.
 */
public interface INotificaFacadeSRV extends Serializable {

	/**
	 * Recupera la prossima notifica da inviare e la invia a seconda del canale
	 * scelto dall'utente.
	 * 
	 * @param idAoo
	 */
	void inviaNextNotifica(int idAoo);

	/**
	 * Ritorna le notifiche dell'utente non più vecchie di aPartireDaNgiorni giorni.
	 * 
	 * @param idUtente
	 * @param idAoo
	 * @param fcUtente
	 * @param aPartireDaNgiorni se null, il servizio imposta di default 7300 giorni
	 *                          (20 anni)
	 * @return
	 */
	List<NotificaDTO> recuperaNotifiche(Long idUtente, Long idAoo, FilenetCredentialsDTO fcUtente, Integer aPartireDaNgiorni);

	/**
	 * Elimina logicamente le notifiche in input.
	 * 
	 * @param notificheSelezionate
	 * @return
	 */
	boolean eliminaNotifiche(Collection<NotificaDTO> notificheSelezionate);

	/**
	 * Contrassegna come lette le notifiche in input.
	 * 
	 * @param notificheSelezionate
	 * @return
	 */
	boolean contrassegnaNotifiche(Collection<NotificaDTO> notificheSelezionate);

	/**
	 * Recupera singola notifica dato l'id - utile per il widget delle
	 * sottoscrizioni
	 * 
	 * @param idNotifica,idUtente,idAoo,fcUtente
	 * @return NotificaDTO
	 */
	List<NotificaDTO> recuperaNotifiche(Integer idNotifica, Long idUtente, Long idAoo, FilenetCredentialsDTO fcUtente);

	/**
	 * Contrassegna le notifiche.
	 * 
	 * @param notificaSelezionata
	 * @return true o false
	 */
	boolean contrassegnaNotifiche(NotificaDTO notificaSelezionata);

	/**
	 * Elimina le notifiche.
	 * 
	 * @param notificaSelezionata
	 * @return true o false
	 */
	boolean eliminaNotifiche(NotificaDTO notificaSelezionata);

	/**
	 * Recupera le notifiche.
	 * 
	 * @param idUtente
	 * @param idAoo
	 * @param fcUtente
	 * @param inAPartireDaNgiorni
	 * @param maxRowNum
	 * @param fromeHomepage
	 * @param maxNotificheNonLette
	 * @param dwhCon
	 * @return lista delle notifiche
	 */
	List<NotificaDTO> recuperaNotifiche(Long idUtente, Long idAoo, FilenetCredentialsDTO fcUtente, Integer inAPartireDaNgiorni, Integer maxRowNum, boolean fromeHomepage,
			int maxNotificheNonLette, Connection dwhCon);
}