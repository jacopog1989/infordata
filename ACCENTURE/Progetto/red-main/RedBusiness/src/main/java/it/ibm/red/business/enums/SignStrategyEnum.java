package it.ibm.red.business.enums;

/**
 * Enum che definisce le diverse strategie di firma che possono essere adottate da un'AOO.
 */
public enum SignStrategyEnum {
	
	/**
	 * In questa modalità sono eseguiti all'atto della firma da parte dell'utente (sincroni) i seguenti step:<br>
	 * - Protocollazione<br>
	 * - Registrazione ausiliaria<br>
	 * - Firma
	 */
	ASYNC_STRATEGY_A(1),
	
	/**
	 * In questa modalità sono eseguiti all'atto della firma da parte dell'utente (sincroni) i seguenti step:<br>
	 * - Firma
	 */
	ASYNC_STRATEGY_B(0);
	
	/**
	 * Modalità della firma.
	 */
	private Integer mode;

	/**
	 * Costruttore.
	 * @param mode
	 */
	private SignStrategyEnum(Integer mode) {
		this.mode = mode;
	}

	/**
	 * Restituisce la modalita di firma. 1: Strategia A, 0: Strategia B.
	 * @return modalita firma
	 */
	public Integer getMode() {
		return mode;
	}

	/**
	 * Restituisce l'enum associata alla modalita, se esiste una modalita definita per l'Integer,
	 * restituisce null altrimenti.
	 * @param mode
	 * @return enum associata alla modalita
	 */
	public static SignStrategyEnum get(Integer mode) {
		SignStrategyEnum out = null;
		
		for (SignStrategyEnum m : SignStrategyEnum.values()) {
			if (m.getMode().equals(mode)) {
				out = m;
				break;
			}
		}
		
		return out;
	}

}
