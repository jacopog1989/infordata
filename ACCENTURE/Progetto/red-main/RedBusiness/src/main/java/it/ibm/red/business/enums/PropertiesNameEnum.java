package it.ibm.red.business.enums;

/**
 * The Enum PropertiesNameEnum.
 *
 * @author CPAOLUZI
 * 
 *         Enum delle properties salvate su db.
 */
public enum PropertiesNameEnum {

	/**
	 * Numero massimo di tentativi di firma asincrona per lo stesso item.
	 * Viene azzerato in caso di reset manuale dalla GA.
	 */
	ASIGN_MAX_RETRY("asign.max_retry"),

	/**
	 * Tempo minimo di attesa tra un retry e l'altro.
	 */
	ASIGN_DELTA_RETRY("asign.delta_retry_minutes"),
	
	/**
	 * Numero degli elementi che il job prenderà in lavorazione.
	 * Da concatenare con la stringa CODICEAOO + ".".
	 */
	ASIGN_JOB_PARTITION("asign.job.partition"),
	
	/**
	 * Contatto destinatario utente GA per la ricezione delle comunicazioni di errori sulla firma asincrona
	 * Da concatenare con la stringa CODICEAOO + ".".
	 */
	ASIGN_AOO_DESTINATARIO_COMUNICAZIONI("asign.destinatario.comunicazioni"),
	
	/**
	 * Valore.
	 */
	TESTO_MAIL_ATTO_DECRETO("attodecreto.testomail"),

	/**
	 * Valore.
	 */
	OGGETTO_MAIL_ATTO_DECRETO("attodecreto.oggettomail"),

	/**
	 * Id.
	 */
	ID_FN_METAKEY("metadato.id"),
	
	/**
	 * MimeType.
	 */
	MIMETYPE_FN_METAKEY("metadato.mimetype"),
	/**
	 * Destinatari mail cc.
	 */
	DESTINATARI_CC_LONG("metadato.destinatariCCLong"),
	/**
	 * Mail message id.
	 */
	MESSAGE_ID("metadato.messageId"),
	
	/**
	 * Tipo Evento.
	 */
	TIPO_EVENTO("metadato.tipoEvento"),
	
	/**
	 * FLAG RIATTIVA.
	 */
	FLAG_RIATTIVA("metadato.flagRiattiva"),
	
	/**
	 * Destinatario contributo esterno.
	 */
	DESTINATARIO_CONTRIBUTO_ESTERNO("metadato.destinatarioContributoEsterno"),
	
	/**
	 * Identificativo nodo mittente.
	 */
	ID_NODO_MITTENTE_WF_METAKEY("metadato.idNodoMittente"),

	/**
	 * Contributo esterno.
	 */
	CONTRIBUTO_ESTERNO("metadato.contributoEsterno"),
	
	/**
	 * Mail coordinamento.
	 */
	MAIL_COORDINAMENTO("metadato.mailCoordinamento"),
	
	/**
	 * Metadato firma copia conforme.
	 */
	ID_RUOLO_AMMINISTRATORE_AOO("ruolo.amministratoreAOO.id"),

	
	/**
	 * Metadato key per l'encrypt dell'url libro firma.
	 */
	MAIL_LINK_KEY_ENCRYPTION("metadato.mailLinkKeyEncryption"),
	
	/**
	 * Metadato firma copia conforme.
	 */
	FIRMA_COPIA_CONFORME_METAKEY("metadato.firmaCopiaConforme"),
	
	/**
	 * Metadato versione copia conforme.
	 */
	VERSIONE_COPIA_CONFORME_METAKEY("metadato.versionecopiaconforme"),
	
	/**
	 * Metadato copia conforme. DA RIMUOVERE, USARE IS_COPIA_CONFORME_METAKEY
	 */
	COPIA_CONFORME_METAKEY("metadato.copiaConforme"),
	
	/**
	 * Metadato pe data assegnazione.
	 */
	DATA_ASSEGNAZIONE_WF_METAKEY("metadato.dataAssegnazione"),
	
	/**
	 * Metadato per decreto liquidazione.
	 */
	DECRETO_LIQUIDAZIONE_METAKEY("metadato.decretoLiquidazione"),

	/**
	 * Metadato pe identificativo utente mittente.
	 */
	ID_UTENTE_MITTENTE_WF_METAKEY("metadato.idUtenteMittente"),
	/**
	 * Iter approvativo semaforo.
	 */
	ITER_APPROVATIVO_SEMAFORO_METAKEY("metadato.iterApprovativoSemaforo"),
	
	/**
	 * Livello di security per l'accesso al sistema:
	 *  - high: la pagina di login è bloccata a tutti gli utenti
	 *  - medium: la pagina di login è bloccata a tutti gli utenti che non hanno come ruolo predefinito: Amministratore dell’AOO
	 *  - low: la pagina di login è visibile a tutti gli utenti (utilizzare solo in ambiente di collaudo e manutenzione.
	 */
	ACCESS_SECURITY_LEVEL("access.security.level"),
	
	/**
	 * Intervallo di tempo di refresh delle properties.
	 */
	SUFFISSO_GRUPPOSECURITY_KEY("suffisso.gruppisecurity"),
	
	/**
	 * Data archiviazione.
	 */
	DATA_ARCHIVIAZIONE_METAKEY("metadato.dataArchiviazione"),
	
	/**
	 * Metadato id formato documento.
	 */
	ID_FORMATO_DOCUMENTO("metadato.idFormatoDocumento"),
	
	/**
	 * Metadato allegato da firmare.
	 */
	DA_FIRMARE_METAKEY("metadato.daFirmare"),
	
	/**
	 * Metadato firma allegato visibile.
	 */
	FIRMA_VISIBILE_METAKEY("metadato.firmaVisibile"),
	
	/**
	 * Chiave della properties 'DocumentTitle'.
	 */
	DOCUMENT_TITLE_METAKEY("metadato.documenttitle"),
	/**
	 * Chiave della properties 'MajorVersionNumber'.
	 */
	MAJOR_VERSION_NUMBER_METAKEY("metadato.majorversionnumber"),
	/**
	 * Chiave della properties 'IDAOO'.
	 */
	ID_AOO_METAKEY("metadato.idaoo"),
	/**
	 * Chiave della properties 'IDAOO' del PE.
	 */
	ID_AOO_WF_METAKEY("pe.metadato.idaoo"),
	/**
	 * Chiave della properties 'Flag Firma autografa RedMobile'.
	 */
	FLAG_FIRMA_AUTOGRAFA_RM_METAKEY("metadato.flagFirmaAutografaRM"),
	/**
	 * Chiave della properties 'Data scadenza'.
	 */
	DATA_SCADENZA_METAKEY("metadato.datascadenza"),
	/**
	 * Chiave della properties 'Urgente'.
	 */
	URGENTE_METAKEY("metadato.urgente"),
	/**
	 * Chiave della properties 'Data creazione'.
	 */
	DATA_CREAZIONE_METAKEY("metadato.dataCreazione"),
	/**
	 * Chiave della properties 'Numero documento'.
	 */
	NUMERO_DOCUMENTO_METAKEY("metadato.numeroDocumento"),
	/**
	 * Chiave della properties 'Oggetto'.
	 */
	OGGETTO_METAKEY("metadato.oggetto"),
	/**
	 * Chiave della properties 'ID PROTOCOLLO'.
	 */
	ID_PROTOCOLLO_METAKEY("metadato.idprotocollo"),
	/**
	 * Chiave della properties 'Tipologia documento ID'.
	 */
	TIPOLOGIA_DOCUMENTO_ID_METAKEY("metadato.idtipologiadocumento"),
	/**
	 * Chiave della properties 'Tipologia documento ID'.
	 */
	ATTO_DECRETO_TIPO_DOC_VALUE("metadato.attodecretotipodoc"),
	/**
	 * Chiave della properties 'Tipologia documento ID'.
	 */
	PARERE_TIPO_DOC_METAKEY("metadato.pareretipodoc"),
	/**
	 * Chiave della properties 'Tipologia documento ID'.
	 */
	ATTO_DECRETO_MANUALE_VALUE("metadato.attodecretomanuale"),
	/**
	 * Chiave della properties 'Tipologia documento ID'.
	 */
	ATTO_DECRETO_AUTOMATICO_VALUE("metadato.attodecretoautomatico"),
	/**
	 * Chiave della properties 'Tipologia spedizione ID'.
	 */
	ID_TIPO_SPEDIZIONE_METAKEY("metadato.idtipospedizione"),
	/**
	 * Chiave della properties 'Tipologia spedizione ID'.
	 */
	IS_DEST_CARTACEI_METAKEY("metadato.isDestinatariCartacei"), 
	/**
	 * Chiave della properties 'Tipologia spedizione ID'.
	 */
	SPED_CART_METAKEY("metadato.sped_cart"), 
	/**
	 * Chiave della properties 'Tipologia spedizione ID'.
	 */
	SPED_ELE_METAKEY("metadato.sped_ele"), 
	/**
	 * Chiave della properties 'Tipologia spedizione ID'.
	 */
	SPED_FORZATA_METAKEY("metadato.flag_spedizioneForzata"),  
	/**
	 * Chiave della properties 'Numero protocollo'.
	 */
	NUMERO_PROTOCOLLO_METAKEY("metadato.numeroprotocollo"),
	/**
	 * Chiave della properties 'Tipologia documento NPS'.
	 */
	DESCRIZIONE_TIPOLOGIA_DOCUMENTO_NPS_METAKEY("metadato.tipologiadocumentonps"),
	/**
	 * Chiave della properties 'Oggetto protocolllo NPS'.
	 */
	OGGETTO_PROTOCOLLO_NPS_METAKEY("metadato.oggettoprotocollonps"),
	/**
	 * Chiave della properties 'Elenco libro firma'.
	 */
	ELENCO_LIBROFIRMA_METAKEY("metadato.elencolibrofirma"),
	/**
	 * Chiave della properties 'Elenco visti'.
	 */
	ELENCO_VISTI_METAKEY("metadato.elencovisti"),
	/**
	 * Chiave della properties 'Elenco conoscenza'.
	 */
	ELENCO_CONOSCENZA_METAKEY("metadato.elencoconoscenza"),
	/**
	 * Chiave della properties 'Elenco conoscenza storico'.
	 */
	ELENCO_CONOSCENZA_STORICO_METAKEY("metadato.elencoconoscenzastorico"),
	/**
	 * Chiave della properties 'Elenco contributi'.
	 */
	ELENCO_CONTRIBUTI_METAKEY("metadato.elencocontributi"),
	/**
	 * Chiave della properties 'Elenco contributi storico'.
	 */
	ELENCO_CONTRIBUTI_STORICO_METAKEY("metadato.elencocontributistorico"),
	/**
	 * Chiave della properties 'Contributi pervenuti'.
	 */
	CONTRIBUTI_PERVENUTI_METAKEY("metadato.contributipervenuti"),
	/**
	/**
	 * Chiave della properties 'Contributi richiesti'.
	 */
	CONTRIBUTI_RICHIESTI_METAKEY("metadato.contributirichiesti"),
	/**
	 * Chiave della properties 'Elenco visti uffici'.
	 */
	ELENCO_UFFICI_VISTI_METAKEY("metadato.elencovistiuffici"),
	/**
	 * Chiave della properties 'Elenco visti ispettorati'.
	 */
	ELENCO_VISTI_ISPETTORATI_METAKEY("metadato.elencovistiispettorati"),
	/**
	 * Chiave della properties 'Item assegnatari'.
	 */
	ITEM_ASSEGNATARI_METAKEY("metadato.itemAssegnatari"),
	/**
	 * Chiave della properties 'Item assegnatari'.
	 */
	ELENCO_TRASMISSIONE_LIST_METAKEY("metadato.elencoTrasmissioneList"),
	/**
	 * Chiave della properties 'Count'.
	 */
	COUNT_METAKEY("metadato.count"),
	/**
	 * Chiave della properties 'Anno protocollo'.
	 */
	ANNO_PROTOCOLLO_METAKEY("metadato.annoprotocollo"),
	/**
	 * Chiave della properties 'Data protocollo'.
	 */
	DATA_PROTOCOLLO_METAKEY("metadato.dataprotocollo"),
	/**
	 * Chiave della properties 'Stato fascicolo'.
	 */
	STATO_FASCICOLO_METAKEY("metadato.statofascicolo"),
	/**
	 * Chiave della properties 'Faldonato'.
	 */
	FALDONATO_METAKEY("metadato.faldonato"),
	/**
	 * Chiave della properties 'idFascicoloFEPA'.
	 */
	METADATO_FASCICOLO_IDFASCICOLOFEPA("metadato.fascicolo.idfascicolofepa"),
	/**
	 * Chiave della properties 'idFascicoloFEPA'.
	 */
	METADATO_FASCICOLO_IDFASCICOLO("metadato.fascicolo.idfascicolo"),
	/**
	 * Chiave della properties 'Trasformazione PDF errore'.
	 */
	TRASFORMAZIONE_PDF_ERRORE_METAKEY("metadato.trasformazionepdfinerrore"),
	/**
	 * Chiave della properties 'Firma PDF'.
	 */
	FIRMA_PDF_METAKEY("metadato.firmapdf"),
	/**
	 * Chiave della properties 'Destinatari documento'.
	 */
	DESTINATARI_DOCUMENTO_METAKEY("metadato.destinatari"),
	/**
	 * Chiave della properties 'Ufficio creatore ID'.
	 */
	UFFICIO_CREATORE_ID_METAKEY("metadato.idnodocreatore"),
	/**
	 * Chiave della properties 'Utente creatore ID'.
	 */
	UTENTE_CREATORE_ID_METAKEY("metadato.idutentecreatore"),
	/**
	 * Chiave della properties 'ID utente protocollatore'.
	 */
	ID_UTENTE_PROTOCOLLATORE_METAKEY("metadato.idutenteprotocollatore"),
	/**
	 * Chiave della properties 'ID gruppo protocollatore'.
	 */
	ID_GRUPPO_PROTOCOLLATORE_METAKEY("metadato.idgruppoprotocollatore"),
	/**
	 * Chiave della properties 'Iter approvativo'.
	 */
	ITER_APPROVATIVO_METAKEY("metadato.iterapprovativo"),
	/**
	 * Chiave della properties 'Content element'.
	 */
	CONTENT_ELEMENTS_METAKEY("metadato.contentelements"),
	/**
	 * Chiave della properties 'formato Originale'.
	 */
	IS_FORMATO_ORIGINALE_METAKEY("metadato.isformatooriginale"),

	/**
	 * Chiave della properties 'formato Originale'.
	 */
	IS_TIMBRO_USCITA_METAKEY("metadato.istimbrouscita"),

	/**
	 * Chiave della properties 'Nome fascicolo'.
	 */
	NOME_FASCICOLO_METAKEY("metadato.nomefascicolo"),
	/**
	 * Chiave della properties 'Titolario/Indice di classificazione'.
	 */
	TITOLARIO_METAKEY("metadato.indiceclassificazione"),
	/**
	 * Chiave della properties 'Tipologia assegnazione ID'.
	 */
	ID_TIPO_ASSEGNAZIONE_METAKEY("metadato.idtipoassegnazione"),
	/**
	 * Chiave della properties 'Flag renderizzato'.
	 */
	FLAG_RENDERIZZATO_METAKEY("metadato.flagrenderizzato"),
	/**
	 * Chiave della properties 'Stato richiesta ID'.
	 */
	STATO_RICHIESTA_ID_METAKEY("metadato.assetinformatici.idstatorichiesta"),
	/**
	 * Chiave della properties 'Nome coda'.
	 */
	NOME_CODA_METAKEY("metadato.nomecoda"),
	/**
	 * Chiave della properties 'Parent number del WF'.
	 */
	WF_PARENT_NUMBER_METAKEY("metadato.parentWFNumber"),
	/**
	 * Chiave della properties 'Pronto allo storno'.
	 */
	PRONTO_ALLO_STORNO_METAKEY("metadato.prontoallostorno"),
	/**
	 * Chiave della properties IDCATEGORIADOCUMENTO.
	 */
	IDCATEGORIADOCUMENTO_METAKEY("metadato.idcategoriadocumento"),
	/**
	 * Chiave della properties 'Nome file'.
	 */
	NOME_FILE_METAKEY("metadato.nomefile"),
	/**
	 * Chiave della properties 'Elenco destinatari interni'.
	 */
	ELENCO_DESTINATARI_INTERNI_METAKEY("metadato.elencoDestinatariInterni"),
	/**
	 * Chiave della properties 'Coordinatore flag'.
	 */
	COORDINATORE_FLAG_METAKEY("metadato.coordinatore"),
	/**
	 * Chiave della properties 'Id utente Coordinatore'.
	 */
	ID_UTENTE_COORDINATORE_METAKEY("metadato.idutentecoordinatore"),
	/**
	 * Chiave della properties 'Id ufficio Coordinatore'.
	 */
	ID_UFFICIO_COORDINATORE_METAKEY("metadato.idnodocoordinatore"),
	/**
	 * Chiave della properties 'Id client'.
	 */
	ID_CLIENT_METAKEY("metadato.idclient"),
	/**
	 * Chiave della properties 'Data terminazione'.
	 */
	DATA_TERMINAZIONE_METAKEY("metadato.dataterminazione"),
	/**
	 * Chiave della properties 'Momento protocollazione'.
	 */
	MOMENTO_PROTOCOLLAZIONE_ID_METAKEY("metadato.idmomentoprotocollazione"),
	/**
	 * Chiave della properties 'Formato allegato'.
	 */
	FORMATO_ALLEGATO_ID_METAKEY("metadato.idformatoallegato"),
	/**
	 * Chiave della properties 'Posizione'.
	 */
	POSIZIONE_METAKEY("metadato.posizione"),
	/**
	 * Chiave della properties 'Is copia conforme'.
	 */
	IS_COPIA_CONFORME_METAKEY("metadato.iscopiaconforme"),
	/**
	 * Chiave della properties 'Id ufficio copia conforme'.
	 */
	ID_UFFICIO_COPIA_CONFORME_METAKEY("metadato.idnodocopiaconforme"),
	/**
	 * Chiave della properties 'Id utente copia conforme'.
	 */
	ID_UTENTE_COPIA_CONFORME_METAKEY("metadato.idutentecopiaconforme"),
	/**
	 * Chiave della properties 'Id applicazione key'.
	 */
	ID_APPLICAZIONE_KEY_METAKEY("metadato.idapplicazione.key"),
	/**
	 * Chiave della properties 'Id applicazione value'.
	 */
	ID_APPLICAZIONE_VALUE_METAKEY("metadato.idapplicazione.value"),
	/**
	 * Chiave della properties 'Mittente'.
	 */
	MITTENTE_METAKEY("metadato.mittente"),
	/**
	 * Chiave della properties 'Numero RDP'.
	 */
	NUMERO_RDP_METAKEY("metadato.numerordp"),
	/**
	 * Chiave della properties 'Numero legislatura'.
	 */
	NUMERO_LEGISLATURA_METAKEY("metadato.numerolegislatura"),
	/**
	 * Chiave della properties 'Numero protocollo mittente'.
	 */
	NUMERO_PROTOCOLLO_MITTENTE_METAKEY("metadato.numeroprotocollomittente"),
	/**
	 * Chiave della properties 'Numero protocollo mittente'.
	 */
	ANNO_PROTOCOLLO_MITTENTE_METAKEY("metadato.annoprotocollomittente"),
	/**
	 * Chiave della properties 'Data protocollo mittente'.
	 */
	DATA_PROTOCOLLO_MITTENTE_METAKEY("metadato.dataprotocollomittente"),
	/**
	 * Chiave della properties 'Assegnatari Competenza'.
	 */
	ASSEGNATARI_COMPETENZA_METAKEY("metadato.assegnataricompetenza"),
	/**
	 * Chiave della properties 'Assegnatari Conoscenza'.
	 */
	ASSEGNATARI_CONOSCENZA_METAKEY("metadato.assegnatariconoscenza"),
	/**
	 * Chiave della properties 'Assegnatari Contributo'.
	 */
	ASSEGNATARI_CONTRIBUTO_METAKEY("metadato.assegnataricontributo"),
	/**
	 * Chiave della properties 'Numero Protocollo Risposta'.
	 */
	NUMERO_PROTOCOLLO_RISPOSTA_STRING_METAKEY("metadato.numeroprotocollorispostastring"),
	/**
	 * Chiave della properties 'Anno Protocollo Risposta'.
	 */
	ANNO_PROTOCOLLO_RISPOSTA_METAKEY("metadato.annoprotocollorisposta"),
	/**
	 * Chiave della properties 'Numero Raccomandata'.
	 */
	NUMERO_RACCOMANDATA_METAKEY("metadato.numeroraccomandata"),
	/**
	 * Chiave della properties 'Note'.
	 */
	NOTE_METAKEY("metadato.note"),
	/**
	 * Chiave della properties 'Flag Corte Conti'.
	 */
	FLAG_CORTE_CONTI_METAKEY("metadato.flagcorteconti"),
	/**
	 * Chiave della properties 'Num Doc Dest Int Uscita'.
	 */
	NUM_DOC_DEST_INT_USCITA_METAKEY("metadato.numDocDestIntUscita"),
	/**
	 * Chiave della properties 'Data Elenco Trasmissione'.
	 */
	DATA_ELENCO_TRASMISSIONE_METAKEY("metadato.documento.dataElencoTrasmissione"),
	//#################################################################
	
	//### MAIL METAKEY ###############################################
	/**
	 * Chiave della properties 'Stato Mail'.
	 */
	STATO_MAIL_METAKEY("metadato.statomail"),
	/**
	 * Chiave della properties 'From Mail'.
	 */
	FROM_MAIL_METAKEY("metadato.from"),
	/**
	 * Chiave della properties 'Destinatari eMail'.
	 */
	DESTINATARI_EMAIL_METAKEY("metadato.destinatarimail"),
	/**
	 * Chiave della properties "Data ricezione Mail".
	 */
	DATA_RICEZIONE_MAIL_METAKEY("metadato.dataricezionemail"),
	/**
	 * Chiave della properties 'Destinatari CC eMail'.
	 */
	DESTINATARI_CC_EMAIL_METAKEY("metadato.destinataricc"),
	/**
	 * Chiave della properties 'Oggetto eMail'.
	 */
	OGGETTO_EMAIL_METAKEY("metadato.oggettomail"),
	/**
	 * Chiave della properties 'Casella postale'.
	 */
	CASELLA_POSTALE_METAKEY("metadato.casellapostale"),
	/**
	 * Chiave della properties 'Mittente mail'.
	 */
	MITTENTE_MAIL_METAKEY("metadato.mittentemail"),
	/**
	 * Chiave della properties 'Tipologia invio'.
	 */
	TIPOLOGIA_INVIO_METAKEY("metadato.tipologiainvio"),
	/**
	 * Chiave della properties 'SentOn'.
	 */
	DATA_INVIO_MAIL_METAKEY("metadato.datainviomail"),
	/**
	 * Chiave della properties 'ID utente destinatario'.
	 */
	ID_UTENTE_DESTINATARIO_WF_METAKEY("metadato.idutentedestinatario"),
	/**
	 * Chiave della properties 'ID utente destinatario'.
	 */
	ID_UTENTE_FIRMATARIO_WF_METAKEY("metadato.idUtenteFirmatario"),
	/**
	 * Chiave della properties 'ID nodo destinatario'.
	 */
	ID_NODO_DESTINATARIO_WF_METAKEY("metadato.idnododestinatario"),
	/**
	 * Chiave della properties 'Id documento WF'.
	 */
	ID_DOCUMENTO_WF_METAKEY("metadato.iddocumento"),
	
	/**
	 * Chiave della property 'Id utente temporaneo WF'.
	 */
	ID_UTENTE_TMP_WF_METAKEY("metadato.idutentetmp"),
	/**
	 * Chiave della properties 'Firma DIG RGS'.
	 */
	FIRMA_DIG_RGS_WF_METAKEY("metadato.firmadigrgs"),
	/**
	 * Chiave della properties 'Flag Iter Manaule'.
	 */
	FLAG_ITER_MANUALE_WF_METAKEY("metadato.flagIterManuale"),
	/**
	 * Chiave della properties 'ID fascicolo'.
	 */
	ID_FASCICOLO_WF_METAKEY("metadato.idfascicolo"),
	/**
	 * Chiave della properties 'Motivazione assegnazione'.
	 */
	MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY("metadato.motivazioneassegnazione"),
	/**
	 * Chiave della properties Workflow processo step'.
	 */
	ID_NODO_START_WF_METAKEY("metadato.idNodoStart"),
	/**
	 * Chiave della properties 'Is Assegnazione Interna'.
	 */
	IS_ASSEGNAZIONE_INTERNA_WF_METAKEY("metadato.isassegnazioneinterna"),
	/**
	 * Chiave della properties 'ID Documento Old'.
	 */
	ID_DOCUMENTO_OLD_WF_METAKEY("metadato.iddocumentoold"),
	/**
	 * Chiave della properties 'Nota email'.
	 */
	NOTA_EMAIL_METAKEY("metadato.notaemail"),
	/**
	 * Chiave della properties 'Motivo eliminazione email'.
	 */
	MOTIVO_ELIMINAZIONE_EMAIL_METAKEY("metadato.motivazioneEliminazioneMail"),
	/**
	 * Chiave della properties 'Data Cambio Stato'.
	 */
	DATA_CAMBIO_STATO_EMAIL_METAKEY("metadato.datacambiostato"),
	/**
	 * 
	 */
	ID_RACCOLTA_FAD_METAKEY("metadato.idraccoltafad"),
	/**
	 * 
	 */
	AMMINISTRAZIONE_FAD_METAKEY("metadato.amministrazioneFad"),
	/**
	 * 
	 */
	RAGIONERIA_FAD_METAKEY("metadato.ragioneriaFad"),
	/**
	 * 
	 */
	ID_RACCOLTA_PROVVISORIA_METAKEY("metadato.idraccoltaprovvisoria"),
	
	/**
	 * 
	 */
	AOO_PROT_AMM_METAKEY("metadato.aooprotamm"),
	/**
	 * 
	 */
	NUM_PROT_AMM_METAKEY("metadato.numprotamm"),
	/**
	 * 
	 */
	DATA_PROT_AMM_METAKEY("metadato.dataprotamm"),
	/**
	 * 
	 */
	AOO_PROT_UCB_INGRESSO_METAKEY("metadato.aooprotiucb"),
	/**
	 * 
	 */
	NUM_PROT_UCB_INGRESSO_METAKEY("metadato.numprotiucb"),
	/**
	 * 
	 */
	DATA_PROT_UCB_INGRESSO_METAKEY("metadato.dataprotiucb"),
	/**
	 * 
	 */
	AOO_PROT_UCB_USCITA_METAKEY("metadato.aooprotuucb"),
	/**
	 * 
	 */
	NUM_PROT_UCB_USCITA_METAKEY("metadato.numprotuucb"),
	/**
	 * 
	 */
	DATA_PROT_UCB_USCITA_METAKEY("metadato.dataprotuucb"),
	/**
	 * 
	 */
	DOC_OLD_ALLEGATO_WF_METAKEY("metadato.allegatoWorkflow.documentoOld"),
	/**
	 * 
	 */
	DOC_NEW_ALLEGATO_WF_METAKEY("metadato.allegatoWorkflow.documentoNew"),
	/**
	 * 
	 */
	EVENT_TYPE_EXP_WF_METAKEY("metadato.eventTypeExp"),
	
	//### CASELLA POSTA METAKEY #######################################
	/**
	 * key all'atto dell'inserimento:
	 * mantieniAllegatiMailOriginali.
	 * 
	 * Booleano che gestisce se scompattare o semplicemente mostrare gli allegati dell'email.
	 */
	MAILBOX_MANTIENI_ALLEGATI_MAIL_ORIGINALI_METAKEY("metadato.mantieniallegatimailoriginali"),
	/**
	 * Chiave della properties 'tipologia casella postale'.
	 */
	TIPOLOGIA_CASELLA_POSTA_METAKEY("metadato.tipologia"),
	//#################################################################
	
	//### FILENET #####################################################

	/**
	 * Metadato indirizzo mailbox.
	 */
	MAILBOX_INDIRIZZO_FN_METAKEY("metadato.mailboxName"),
	/**
	 * Chiave della properties 'hostinvio'.
	 */
	MAILBOX_HOSTINVIO_MAIL_METAKEY("metadato.hostinvio"),
	/**
	 * Chiave della properties 'hostricezione'.
	 */
	MAILBOX_HOSTRICEZIONE_METAKEY("metadato.hostricezione"),
	/**
	 * Chiave della properties 'passwordinvio'.
	 */
	MAILBOX_PASSWORDINVIO_METAKEY("metadato.passwordinvio"),
	/**
	 * Chiave della properties 'passwordricezione'.
	 */
	MAILBOX_PASSWORDRICEZIONE_METAKEY("metadato.passwordricezione"),
	/**
	 * Chiave della properties 'portinvio'.
	 */
	MAILBOX_PORTINVIO_METAKEY("metadato.portinvio"),
	/**
	 * Chiave della properties 'portricezione'.
	 */
	MAILBOX_PORTRICEZIONE_METAKEY("metadato.portricezione"),
	/**
	 * Chiave della properties 'proxyhost'.
	 */
	MAILBOX_PROXYHOST_METAKEY("metadato.proxyhost"),
	/**
	 * Chiave della properties 'proxyport'.
	 */
	MAILBOX_PROXYPORT_METAKEY("metadato.proxyport"),
	/**
	 * Chiave della properties 'usernameinvio'.
	 */
	MAILBOX_USERNAMEINVIO_METAKEY("metadato.usernameinvio"),
	/**
	 * Chiave della properties 'usernamericezione'.
	 */
	MAILBOX_USERNAMERICEZIONE_METAKEY("metadato.usernamericezione"),
	/**
	 * Chiave della properties 'serverintype'.
	 */
	MAILBOX_SERVERINTYPE_METAKEY("metadato.serverintype"),
	/**
	 * Chiave della properties 'serveroutautenticazione'.
	 */
	MAILBOX_SERVEROUTAUTENTICAZIONE_METAKEY("metadato.serveroutautenticazione"),
	/**
	 * Chiave della properties 'gestionewhitelist'.
	 */
	MAILBOX_GESTIONEWHITELIST_METAKEY("metadato.gestionewhitelist"),
	/**
	 * Chiave della properties 'tls12'.
	 */
	MAILBOX_TLS12_METAKEY("metadato.tls12"),
	/**
	 * Chiave della properties 'tls12'.
	 */
	MAILBOX_FLAG_PROT_AUTO_METAKEY("metadato.flagProtAutomatica"),
	/**
	 * Chiave della properties 'Mailbox className'.
	 */
	MAILBOX_CLASSNAME_FN_METAKEY("filenet.classemailbox"),
	/**
	 * Chiave della properties 'DOCUMENTO_NSD className'.
	 */
	DOCUMENTO_CLASSNAME_FN_METAKEY("filenet.classedocumento"),
	/**
	 * Chiave della properties 'DOCUMENTO className'.
	 */
	DOCUMENTO_PADRE_CLASSNAME_FN_METAKEY("filenet.classedocumentopadre"),
	/**
	 * Chiave della properties 'CONTRIBUTO className'.
	 */
	CONTRIBUTO_CLASSNAME_FN_METAKEY("filenet.classecontributo"),
	/**
	 * Chiave della properties 'Fascicolo className'.
	 */
	FASCICOLO_CLASSNAME_FN_METAKEY("filenet.classefascicolo"),
	/**
	 * Chiave della properties 'Faldone className'.
	 */
	FALDONE_CLASSNAME_FN_METAKEY("filenet.classefaldone"),
	/**
	 * Chiave della properties 'Allegato className'.
	 */
	ALLEGATO_CLASSNAME_FN_METAKEY("filenet.classeallegato"),
	/**
	 * Chiave della properties 'Allegato className'.
	 */
	ALLEGATO_NSD_OP_FN_METAKEY("filenet.allegato.NSD_OP"),
	/**
	 * Chiave della properties 'DSR className'.
	 */
	DSR_CLASSNAME_FN_METAKEY("filenet.classedocumentodichiarazioneserviziresi"),
	/**
	 * Chiave della properties 'Fattura FEPA className'.
	 */
	FATTURA_FEPA_CLASSNAME_FN_METAKEY("filenet.classedocumentofatturafepa"),
	/**
	 * Chiave della properties 'Mailbox className'.
	 */
	DECRETO_DIRIGENZIALE_FEPA_CLASSNAME_FN_METAKEY("filenet.classedocumentodescretodirigenzialefepa"),
	/**
	 * Chiave della properties 'Atto decreto className'.
	 */
	ATTO_DECRETO_CLASSNAME_FN_METAKEY("filenet.classedocumentoattodecreto"),
	/**
	 * Chiave della properties 'Classe documento generico'.
	 */
	DOCUMENTO_GENERICO_CLASSNAME_FN_METAKEY("filenet.classedocumentogenerico"),
	/**
	 * Chiave della properties 'Titolario className'.
	 */
	TITOLARIO_CLASSNAME_FN_METAKEY("filenet.classetitolario"),
	/**
	 * Chiave della properties 'Mail className'.
	 */
	MAIL_CLASSNAME_FN_METAKEY("filenet.classemail"),
	/**
	 * Chiave della properties 'Documento SIGI className'.
	 */
	DOCUMENTO_SIGI_CLASSNAME_FN_METAKEY("filenet.classedocumentosigi"),
	/**
	 * Chiave della properties 'Registro Protocollo className'.
	 */
	REGISTRO_PROTOCOLLO_CLASSNAME_FN_METAKEY("filenet.classeregistroprotocollo"),
	/**
	 * Chiave della properties 'Documento Cartaceo'.
	 */
	DOCUMENTO_CARTACEO_CLASSNAME_FN_METAKEY("filenet.classedocumentocartaceo"),
	/**
	 * Chiave della properties 'Classe documentale Spedizione'.
	 */
	DOCUMENTO_SPEDIZIONE_CLASSNAME_FN_METAKEY("filenet.classedocumentospedizione"),
	/**
	 * Chiave della properties 'Classe documentale Notifica Interoperabilità'.
	 */
	NOTIFICA_INTEROP_CLASSNAME_FN_METAKEY("filenet.classemailnotificainteroperabilita"),
	/**
	 * Chiave della properties 'BARCODE'.
	 */
	BARCODE_METAKEY("metadato.barcode"),
	/**
	 * Chiave della properties 'Tipo protocollo'.
	 */
	TIPO_PROTOCOLLO_METAKEY("metadato.tipoprotocollo"),
	/**
	 * Chiave della properties 'Riservato'.
	 */
	RISERVATO_METAKEY("metadato.riservato"),
	/**
	 * Chiave della properties 'Registro riservato'.
	 */
	REGISTRO_RISERVATO_METAKEY("metadato.registroriservato"),
	/**
	 * Chiave della properties 'Anno documento'.
	 */
	ANNO_DOCUMENTO_METAKEY("metadato.annoDocumento"), //annoDocumento
	/**
	 * Chiave della properties 'Folder elettronici'.
	 */
	FOLDER_ELETTRONICI_FN_METAKEY("folder.elettronici"),
	/**
	 * Chiave della properties 'Folder fascicolo'.
	 */
	FOLDER_FASCICOLI_FN_METAKEY("folder.fascicoli"),
	/**
	 * Chiave della properties 'Folder fascicolo'.
	 */
	FOLDER_CASELLA_POSTALE_FN_METAKEY("folder.casellapostale"),
	/**
	 * Chiave della properties 'Folder inviata'.
	 */
	FOLDER_MAIL_INVIATA_FN_METAKEY("folder.mail.fws.inviata"),
	/**
	 * Chiave della properties 'Folder inbox'.
	 */
	FOLDER_MAIL_INBOX_FN_METAKEY("folder.mail.fws.inbox"),
	/**
	 * Chiave della properties 'Folder notifiche'.
	 */
	FOLDER_MAIL_NOTIFICHE_FN_METAKEY("folder.mail.fws.notifiche"),
	/**
	 * Chiave della properties 'Folder acquisiti'.
	 */
	FOLDER_ACQUISITI_FN_METAKEY("folder.acquisiti"),
	/**
	 * Chiave della properties 'Folder spedizioni'.
	 */
	FOLDER_SPEDIZIONI_FN_METAKEY("folder.spedizioni"),
	/**
	 * Chiave della properties 'Folder notifiche interoperabilità'.
	 */
	FOLDER_MAIL_NOTIFICHE_INTEROP_FN_METAKEY("folder.mail.notificheinterop"),
	/**
	 * Chiave della properties 'PE ROSTER NAME'.
	 */
	PE_ROSTERNAME_FN_METAKEY("pe.rostername"),
	/**
	 * Chiave della properties 'Tipo approvazione ID' del PE.
	 */
	ID_TIPO_APPROVAZIONE_FN_METAKEY("metadato.idtipoapprovazione"),
	/**
	 * Chiave della properties 'Tipo approvazione ID' del CE.
	 */
	TIPO_PROCEDIMENTO_ID_FN_METAKEY("metadato.idTipologiaProcedimento"),
	/**
	 * Chiave della properties 'Note visto'.
	 */
	NOTE_VISTO_FN_METAKEY("metadato.notevisto"),
	/**
	 * Chiave della properties 'ID coda lavorazione'.
	 */
	ID_CODA_LAVORAZIONE_FN_METAKEY("metadato.idcodalavorazione"),
	/**
	 * Chiave della properties 'ID tipo firma'.
	 */
	ID_TIPO_FIRMA_FN_METAKEY("metadato.idtipofirma"),
	/**
	 * Chiave della properties 'WS client'.
	 */
	WS_IDCLIENT("ws.idclient"),
	//#################################################################
	
	//### CE KEY ######################################################
	/**
	 * Chiave della properties 'Folder documenti'.
	 */
	FOLDER_DOCUMENTI_CE_KEY("folder.documenti"),
	/**
	 * Chiave della properties 'Folder bozze'.
	 */
	FOLDER_BOZZE_CE_KEY("folder.bozze"),
	/**
	 * Chiave della properties 'Folder bozze mail'.
	 */
	FOLDER_BOZZE_MAIL_CE_KEY("folder.bozzemail"),
	//#################################################################
	
	//### WF KEY ######################################################
	/**
	 * Chiave della properties 'Workflow processo giro visto ufficio'.
	 */
	WF_PROCESSO_GIROVISTOUFFICIO_MAPPING_WFKEY("workflow.processogirovistoufficio.mapping"),
	/**
	 * Chiave della properties 'Workflow processo giro visto ispettorato'.
	 */
	WF_PROCESSO_GIROVISTOISPETTORATO_MAPPING_WFKEY("workflow.processogirovistoispettorato.mapping"),
	/**
	 * Chiave della properties 'Workflow processo giro visto ispettore competente'.
	 */
	WF_PROCESSO_GIROVISTOISPCOMPETENTE_MAPPING_WFKEY("workflow.processogirovistoispcompetente.mapping"),
	/**
	 * Chiave della properties 'Mapping Step In Sospeso'.
	 */
	STEP_IN_SOSPESO_MAPPING_WFKEY("stepname.insospeso.mapping"),
	/**
	 * Chiave della properties 'Workflow processso contributo'.
	 */
	WF_PROCESSO_CONTRIBUTO_WFKEY("workflow.processocontributo.mapping"),
	/**
	 * Chiave della properties 'Workflow processo chiusura storico'.
	 */
	WF_PROCESSO_CHIUSURA_STORICO_WFKEY("pe.chiusurastorico"),
	/**
	 * Chiave della properties 'Al giro visti Step Name'.
	 */
	AL_GIROVISTI_STEPNAME_WFKEY("stepname.algirovisti.mapping"),
	/**
	 * Chiave della properties Workflow processo step'.
	 */
	WF_PROCESSO_STEP_MAPPING_WFKEY("workflow.processostep.mapping"),
	/**
	 * Chiave della properties Workflow processo conoscenza'.
	 */
	WF_PROCESSO_CONOSCENZA_MAPPING_WFKEY("workflow.processoconoscenza.mapping"),
	/**
	 * Chiave della properties Workflow processo conoscenza'.
	 */
	WF_PROCESSO_GESTIONE_PROCESSI_WFKEY("pe.gestioneprocessi"),
	//######################################################################
	
	//### NPS KEY ##########################################################
	/**
	 * Chiave della properties 'Endpoint servizi documentale WS NPS'.
	 */
	NPS_WS_DOCUMENTALE_ENDPOINT_NPSKEY("nps.ws.documentale.endpoint"),
	/**
	 * Chiave della properties 'Endpoint servizi protocollo WS NPS'.
	 */
	NPS_WS_PROTOCOLLO_ENDPOINT_NPSKEY("nps.ws.protocollo.endpoint"),
	/**
	 * Chiave della properties 'Endpoint servizi registrazione ausiliaria WS NPS'.
	 */
	NPS_WS_REGISTRAZIONE_AUSILIARIA_ENDPOINT_NPSKEY("nps.ws.registrazioneausiliaria.endpoint"),
	/**
	 * Chiave della properties 'Endpoint servizi registrazione ausiliaria WS NPS'.
	 */
	NPS_WS_FLUSSO_AUT_ENDPOINT_NPSKEY("nps.ws.flussoaut.endpoint"),
	/**
	 * Chiave della properties 'Endpoint servizi registrazione ausiliaria WS NPS'.
	 */
	NPS_WS_POSTA_ENDPOINT_NPSKEY("nps.ws.posta.endpoint"),

	/**
	 * Nome della variabile d'ambiente che permette di abilitare gli endpoint di NPS sviluppi.
	 */
	NPS_ENVIRONMENT_VAR_ENDPOINT_SVIL("nps.environment.var.endpoint.svil"),
	/**
	 * Chiave della properties 'Endpoint servizi documentale WS NPS'.
	 */
	NPS_WS_DOCUMENTALE_ENDPOINT_NPSKEY_SVIL("nps.ws.documentale.endpoint.svil"),
	/**
	 * Chiave della properties 'Endpoint servizi protocollo WS NPS'.
	 */
	NPS_WS_PROTOCOLLO_ENDPOINT_NPSKEY_SVIL("nps.ws.protocollo.endpoint.svil"),
	/**
	 * Chiave della properties 'Endpoint servizi registrazione ausiliaria WS NPS'.
	 */
	NPS_WS_REGISTRAZIONE_AUSILIARIA_ENDPOINT_NPSKEY_SVIL("nps.ws.registrazioneausiliaria.endpoint.svil"),
	/**
	 * Chiave della properties 'Endpoint servizi registrazione ausiliaria WS NPS'.
	 */
	NPS_WS_FLUSSO_AUT_ENDPOINT_NPSKEY_SVIL("nps.ws.flussoaut.endpoint.svil"),
	/**
	 * Chiave della properties 'Endpoint servizi registrazione ausiliaria WS NPS'.
	 */
	NPS_WS_POSTA_ENDPOINT_NPSKEY_SVIL("nps.ws.posta.endpoint.svil"),

	/**
	 * Chiave della properties 'Endpoint servizi WS NPS'.
	 */
	NPS_CLIENT_NPSKEY("nps.ws.client"),
	/**
	 * Chiave della properties 'Endpoint servizi WS NPS'.
	 */
	NPS_PWD_NPSKEY("nps.ws.pwd"),
	/**
	 * Chiave della properties 'Endpoint servizi WS NPS'.
	 */
	NPS_SERVICE_NPSKEY("nps.ws.service"),
	/**
	 * Chiave della properties 'Endpoint servizi WS NPS'.
	 */
	NPS_USER_NPSKEY("nps.ws.user"),
	/**
	 * Chiave della properties 'Endpoint servizi WS NPS'.
	 */
	NPS_APPLICATION_NPSKEY("nps.ws.application"),
	/**
	 * Chiave della properties 'Endpoint servizi WS NPS'.
	 */
	NPS_ACTOR_NPSKEY("nps.ws.actor"),
	/**
	 * Chiave della properties 'Numero massimo risultati ricerca protocolli'.
	 */
	NPS_MAX_RISULTATI_RICERCA_PROTOCOLLI("nps.max.risultati.ricerca.protocolli"),
	//######################################################################
	//### PMEF KEY ##########################################################
	
	/**
	 * Protocollo MEF endpoint.
	 */
	TARGET_ENDPOINT_PMEFKEY("protocollomef.targetendpoint"),
	
	/**
	 * Protocollo MEF username.
	 */
	WSUSERNAME_PMEFKEY("protocollomef.wsusername"),
	
	/**
	 * Protocollo MEF password.
	 */
	WSPASSWORD_PMEFKEY("protocollomef.wspassword"),
	
	/**
	 * Protocollo MEF azione.
	 */
	AZIONE_PMEFKEY("protocollomef.azione"),
	
	/**
	 * Protocollo MEF servizio.
	 */
	SERVIZIO_PMEFKEY("protocollomef.servizio"),
	//######################################################################
	
	
	//### ADOBE KEY ########################################################
	/**
	 * Chiave della properties 'Endpoint server Adobe'.
	 */
	DSC_DEFAULT_SOAP_ENDPOINT_ADOBEKEY("DSC_DEFAULT_SOAP_ENDPOINT"),
	/**
	 * Chiave della properties 'MAX_CONVERT_RETRY Adobe'.
	 */
	MAX_CONVERT_RETRY_ADOBEKEY("MAX_CONVERT_RETRY"),
	/**
	 * Chiave della properties 'DSC_SERVER_TYPE Adobe'.
	 */
	DSC_SERVER_TYPE_ADOBEKEY("DSC_SERVER_TYPE"),
	/**
	 * Chiave della properties 'DSC_CREDENTIAL_USERNAME Adobe'.
	 */
	DSC_CREDENTIAL_USERNAME_ADOBEKEY("DSC_CREDENTIAL_USERNAME"),
	/**
	 * Chiave della properties 'DSC_CREDENTIAL_USERNAME Adobe'.
	 */
	DSC_CREDENTIAL_PASSWORD_ADOBEKEY("DSC_CREDENTIAL_PASSWORD"),
	/**
	 * Chiave della properties 'system.admin.username'.
	 */
	SYSTEM_ADMIN_USERNAME_KEY("system.admin.username"),
	/**
	 * Chiave della properties 'sistema.amministratore', da concatenare con la stringa CODICEAOO + ".".
	 */
	AOO_SISTEMA_AMMINISTRATORE("sistema.amministratore"),
	
	/**
	 * parte della chiave della properties usata per il processo di approvazioni pdf
	 * chiave completa codiceAoo + Chiave.
	 */
	STAMPA_APPROVAZIONI_ADOBE_PROCESS_NAME(".stampa.approvazioni.adobe.process.name"),
	
	/**
	 * Valore.
	 */
	FILE_TYPE_SETTINGS_ADOBE_KEY("adobe.fileTypeSettings"),
	
	/**
	 * Valore.
	 */
	ADOBE_PDF_SETTINGS_ADOBE_KEY("adobe.adobePDFSettings"),

	/**
	 * Valore.
	 */
	SECURITY_SETTINGS_ADOBE_KEY("adobe.securitySettings"),

	/**
	 * Valore.
	 */
	PARAM_APPLICAZIONE_PROCESS_PDF_A_ADOBE_KEY("adobe.processParamApplicazionePDFA"),
	
	//### DEMBIL_FEPA KEY ##########################################################
	/**
	 * 
	 */
	FAD_DEMBIL_TARGET_ENDPOINT("fad.ws.targetendpoint"),
	/**
	 * 
	 */
	FAD_DEMBIL_APPLICATION("dembil_fad.ws.applicazione"),
	/**
	 * 
	 */
	FAD_DEMBIL_ORG_ID("dembil_fad.ws.orgid"),
	/**
	 * 
	 */
	FAD_DEMBIL_USER_ID("dembil_fad.ws.userid"),
	/**
	 * 
	 */
	FAD_DEMBIL_USER_TYPE("dembil_fad.ws.usertype"),
	/**
	 * 
	 */
	FAD_DEMBIL_CLIENT_ID("dembil_fad.ws.clientid"),
	/**
	 * 
	 */
	FAD_DEMBIL_PASSWORD("dembil_fad.ws.password"),
	/**
	 * 
	 */
	FAD_DEMBIL_SERVICE("dembil_fad.ws.service"),

	//### INTEGRATOR KEY ##########################################################
	/**
	 * Valore.
	 */
	SIGI_ENDPOINT("sigi.ws.targetendpoint"),

	/**
	 * Valore.
	 */
	SICOGE_ENDPOINT("sicoge.ws.targetendpoint"),

	/**
	 * Valore.
	 */
	SICOGE_HEADER_APPLICAZIONE("sicoge.ws.header.applicazione"),

	/**
	 * Valore.
	 */
	SICOGE_HEADER_UTENTE("sicoge.ws.header.utente"),

	/**
	 * Valore.
	 */
	SICOGE_HEADER_PASSWORD("sicoge.ws.header.password"),
	
	//### FEPA3 KEY ##########################################################
	/**
	 * 
	 */
	FEPA3_ENDPOINT("fepa3.ws.targetendpoint"),

	//### BILENTI KEY ##########################################################
	/**
	 * 
	 */
	BILENTI_ENDPOINT("bilenti.ws.targetendpoint"),
	/**
	 * 
	 */
	BILENTI_APPLICAZIONE_WS("bilenti.fepa.credenziali.ws.applicazione"),
	/**
	 * 
	 */
	BILENTI_CLIENTID("bilenti.fepa.credenziali.ws.clientid"),
	/**
	 * 
	 */
	BILENTI_PASSWORD("bilenti.fepa.credenziali.ws.password"),
	/**
	 * 
	 */
	BILENTI_SERVICE("bilenti.fepa.credenziali.ws.service"),
	/**
	 * 
	 */
	BILENTI_APPLICAZIONE_UTENTE("bilenti.fepa.credenziali.utente.applicazione"),
	/**
	 * 
	 */
	BILENTI_ORGID("bilenti.fepa.credenziali.utente.orgid"),
	/**
	 * 
	 */
	BILENTI_USERID("bilenti.fepa.credenziali.utente.userid"),
	/**
	 * 
	 */
	BILENTI_USERTYPE("bilenti.fepa.credenziali.utente.usertype"),

	//### SICOGE KEY ##########################################################
	/**
	 * 
	 */
	SICOGE_NOME_FLUSSO("sicoge.fepa.nome.flusso"),
	/**
	 * 
	 */
	SICOGE_APPLICAZIONE_WS("sicoge.fepa.credenziali.ws.applicazione"),
	/**
	 * 
	 */
	SICOGE_CLIENTID("sicoge.fepa.credenziali.ws.clientid"),
	/**
	 * 
	 */
	SICOGE_PASSWORD("sicoge.fepa.credenziali.ws.password"),
	/**
	 * 
	 */
	SICOGE_SERVICE("sicoge.fepa.credenziali.ws.service"),
	/**
	 * 
	 */
	SICOGE_ORGID("sicoge.fepa.credenziali.utente.orgid"),
	/**
	 * 
	 */
	SICOGE_USERID("sicoge.fepa.credenziali.utente.userid"),
	/**
	 * 
	 */
	SICOGE_USERTYPE("sicoge.fepa.credenziali.utente.usertype"),
	
	//### REVISORI IGF KEY ##########################################################
	/**
	 * 
	 */
	REVISORI_IGF_ENDPOINT("verbalerevisori.ws.targetendpoint"),
	/**
	 * 
	 */
	REVISORI_IGF_APPLICAZIONE_WS("verbalerevisori.fepa.credenziali.ws.applicazione"),
	/**
	 * 
	 */
	REVISORI_IGF_CLIENTID("verbalerevisori.fepa.credenziali.ws.clientid"),
	/**
	 * 
	 */
	REVISORI_IGF_PASSWORD("verbalerevisori.fepa.credenziali.ws.password"),
	/**
	 * 
	 */
	REVISORI_IGF_SERVICE("verbalerevisori.fepa.credenziali.ws.service"),
	/**
	 * 
	 */
	REVISORI_IGF_APPLICAZIONE_UTENTE("verbalerevisori.fepa.credenziali.utente.applicazione"),
	/**
	 * 
	 */
	REVISORI_IGF_ORGID("verbalerevisori.fepa.credenziali.utente.orgid"),
	/**
	 * 
	 */
	REVISORI_IGF_USERID("verbalerevisori.fepa.credenziali.utente.userid"),
	/**
	 * 
	 */
	REVISORI_IGF_USERTYPE("verbalerevisori.fepa.credenziali.utente.usertype"),
	
	//### FEPA KEY ##########################################################

	/**
	 * Valore.
	 */
	FEPA_APPLICAZIONE("red.fepa.credenziali.applicazione"),
	
	/**
	 * Valore.
	 */
	FEPA_USER("red.fepa.credenziali.userID"),
	
	/**
	 * Valore.
	 */
	FEPA_CLIENT("red.fepa.credenziali.clientID"),
	
	/**
	 * Valore.
	 */
	FEPA_SERVICE("red.fepa.credenziali.service"),
	
	/**
	 * Valore.
	 */
	FEPA_PASSWORD("red.fepa.credenziali.password"),
	
	/**
	 * Valore.
	 */
	FEPA_ENDPOINT("red.fepa.credenziali.endpoint"),
	
	/**
	 * Valore.
	 */
	FEPA_OP_ENDPOINT("red.fepa_OP.credenziali.endpoint"),
	
	/**
	 * Valore.
	 */
	FEPA_TIPOUTENTE("red.fepa.credenziali.tipoutente"),
	
	/**
	 * Valore.
	 */
	FEPA_ORGID("red.fepa.credenziali.orgID"),
	
	/**
	 * Valore.
	 */
	FEPA_METADATO_NUMERO_OP("fepa.metadato.numeroOP"),

	/**
	 * Valore.
	 */
	FEPA_METADATO_AMMINISTRAZ("fepa.metadato.amministraz"),

	/**
	 * Valore.
	 */
	FEPA_METADATO_CODICE_RAGIONERIA("fepa.metadato.codiceRagioneria"),

	/**
	 * Valore.
	 */
	FEPA_METADATO_CAPITOLO_NUMERI_SIRGS("fepa.metadato.CAPITOLO_NUMERI_SIRGS"),

	/**
	 * Valore.
	 */
	FEPA_METADATO_TIPO_OP("fepa.metadato.tipoOp"),

	/**
	 * Valore.
	 */
	FEPA_METADATO_PIANO_GESTIONE("fepa.metadato.pianoGestione"),

	/**
	 * Valore.
	 */
	FEPA_METADATO_BENEFICIARIO("fepa.metadato.beneficiario"),

	/**
	 * Valore.
	 */
	FEPA_METADATO_ESERCIZIO("fepa.metadato.ESERCIZIO"),

	/**
	 * Valore.
	 */
	FEPA_METADATO_DATA_EMISSIONE_OP("fepa.metadato.dataEmissioneOP"),

	/**
	 * Valore.
	 */
	FEPA_METADATO_STATO_OP("fepa.metadato.statoOP"),

	/**
	 * Valore.
	 */
	FEPA_METADATO_OP_PROXY("fepa.metadato.opProxy"),

	/**
	 * Valore.
	 */
	FEPA_METADATO_ELENCO_OP("fepa.metadato.elencoOP"),
	
	/**
	 * Valore.
	 */
	FEPA_METADATO_LISTA_OP("fepa.metadato.listaOP"),
	
	/**
	 * Valore.
	 */
	FEPA_METADATO_CLASSNAME_ORDINE_DI_PAGAMENTO_DD("fepa.classname.odp"),

	/**
	 * Valore.
	 */
	FEPA_METADATO_CLASSNAME_ELENCO_ORDINE_DI_PAGAMENTO_DD("fepa.classname.elencoodp"),
	
	/**
	 * Valore.
	 */
	FEPA_METADATO_PROXY_DECRETO("fepa.proxy.decreto"),
	
	/**
	 * Valore.
	 */
	FEPA_METADATO_TRASMESSO("fepa.metadato.trasmesso"),

	/**
	 * Valore.
	 */
	FEPA_METADATO_DATA_TRASMISSIONE_DD("fepa.metadato.datatrasmissionedd"),
	
	/**
	 * Valore.
	 */
	FEPA_METADATO_ELENCO_DICHIARAZIONI("fepa.metadato.elencodichiarazioni"),
	
	/**
	 * Valore.
	 */
	TIPOLOGIA_FEPA_FATTURA_ID("fepa.idTipologiaFEPAFattura"),
	
	/**
	 * mezzoRicezione : contiene l'id del mezzo.
	 */
	METADATO_DOCUMENTO_MEZZORICEZIONE("metadato.documento.mezzoricezione"),
	
	/**
	 * dataAnnullamentoDocumento.
	 */
	METADATO_DOCUMENTO_DATA_ANNULLAMENTO("metadato.documento.dataAnnullamento"),
	
	/**
	 * motivazioneAnnullamento Documento.
	 */
	METADATO_DOCUMENTO_MOTIVAZIONE_ANNULLAMENTO("metadato.documento.motivazioneAnnullamento"),
	
	/**
	 * mittente : String.
	 */
	METADATO_DOCUMENTO_MITTENTE("metadato.documento.mittente"),
	
	/**CAMPI FALDONE START***********************************************************/
	/**
	 * DocumentTitle.
	 */
	METADATO_FALDONE_DOCUMENTTITLE("metadato.faldone.documentTitle"),
	/**
	 * nomeFaldone.
	 */
	METADATO_FALDONE_NOMEFALDONE("metadato.faldone.nomeFaldone"),
	/**
	 * parentFaldone.
	 */
	METADATO_FALDONE_PARENTFALDONE("metadato.faldone.parentFaldone"),
	/**
	 * descrizioneFaldone.
	 */
	METADATO_FALDONE_DESCRIZIONEFALDONE("metadato.faldone.descrizioneFaldone"),
	/**
	 * idAOO.
	 */
	METADATO_FALDONE_IDAOO("metadato.faldone.idAOO"),
	/**
	 * Oggetto.
	 */
	METADATO_FALDONE_OGGETTO("metadato.faldone.oggetto"),
	/**
	 * IsReserved.
	 */
	METADATO_FALDONE_RESERVED("metadato.faldone.reserved"),
	/**CAMPI FALDONE END*************************************************************/
	

	/**
	 * Valore.
	 */
	METADATO_ANNO_MITTENTE_CONTRIBUTO("metadato.annomittentecontributo"),
	
	/**
	 * Valore.
	 */
	METADATO_NUMERO_MITTENTE_CONTRIBUTO("metadato.numeromittentecontributo"),
	
	/**
	 * Valore.
	 */
	MODIFICA_MONTANARO_METAKEY("metadato.modifica_montanaro"),
	
	/**
	 * Valore.
	 */
	METADATO_STATO_PRE_ARCHIVIAZIONE("metadato.statomailprearchiviazione"),
	
	/**
	 * Valore.
	 */
	DELTA_GIORNI_ELIMINAZIONE_MAIL("delta.numero.gg"),
	
	/**
	 * Valore.
	 */
	DELTA_MESI_SPOSTA_MAIL("delta.numero.mesi"),
	
	/**
	 * Valore.
	 */
	VERIFICA_FIRMA_MAX_ITEM("verifica.firma.max.item"),
	
	/**
	 * Valore.
	 */
	METADATO_DOCUMENTO_VALIDITA_FIRMA("metadato.validitaFirma"), 
	
	/**
	 * Valore.
	 */
	GROUP_SECURITY_PREFIX("group.security.prefix"),
	
	/**
	 * Valore.
	 */
	FALDONE_PARENT_FOLDER_PREFIX("faldone.parent.folder.prefix"),
	
	/**
	 * Valore.
	 */
	MAX_FASCICOLI_NON_CLASSIFICATI("max.fascicoli.non_classificati"),
	
	/**
	 * Chiave della properties 'tipologiaFirma'.
	 */
	TIPOLOGIA_FIRMA_WF_METAKEY("metadato.tipologiafirma"),
	
	/**
	 * Valore.
	 */
	TIPO_DOC_BILANCIO_ENTI_USCITA("tipologia.documento.bilancio.enti.uscita"),
	
	/**
	 * Valore.
	 */
	RED_WEB_LOGGING_PROPERTIES("RED.web.loggingproperties"),
	
	/**
	 * Valore.
	 */
	RED_WS_LOGGING_PROPERTIES("RED.ws.loggingproperties"),

	/**
	 * Valore.
	 */
	RED_BATCH_LOGGING_PROPERTIES("RED.batch.loggingproperties"),
	
	/**
	 * Valore.
	 */
	COPPIA_TIPOLOGIA_DOC_PROC_FALDONATURA_OBBLIGATORIA("coppia.tipologia.documento_procedimento.faldonatura.obbligatoria"),
	
	/**
	 * Valore.
	 */
	FEPA_ID_NODO_FIRMA_METAKEY("fepa.metadato.idnodofirma"),
	
	/**
	 * Valore.
	 */
	FEPA_ID_UTENTE_FIRMA_METAKEY("fepa.metadato.idutentefirma"), 
	
	/**
	 * Valore.
	 */
	FEPA_DICHIARAZIONE_SERVIZI_RESI_WFKEY("fepa.metadato.dichiarazioneServiziResi"),
	
	/**
	 * Valore.
	 */
	FEPA_ELENCO_DECRETI_WFKEY("fepa.metadato.elencodecretifepa"),
	
	/**
	 * Valore.
	 */
	FEPA_DECRETI_DA_AGGIUNGERE_WF_METAKEY("fepa.metadato.elencodecretidaaggiungere"),
	
	/**
	 * Valore.
	 */
	FEPA_MITTENTE("fepa.metadato.mittente"),
	
	/**
	 * Valore.
	 */
	ID_DOCUMENTO_FEPA("fepa.metadato.idDocumento"),
	
	/**
	 * Valore.
	 */
	FEPA_CODICE_TIPO_DOCUMENTO_METAKEY("metadato.codiceTipoDocumentoFepa"),
	
	
	/**
	 * Valore.
	 */
	TIPO_DECRETO_NETTO_ID("fepa.metadato.tipologiaDecretoNetto"),
	
	/**
	 * Valore.
	 */
	TIPO_DECRETO_IVA_ID("fepa.metadato.tipologiaDecretoIva"),
	
	/**
	 * Valore.
	 */
	ELENCO_FATTURE_FEPA("fepa.metadato.elencoFattureFepa"),
	
	/**
	 * Valore.
	 */
	FEPA_DECRETI_DA_ELIMINARE_WF_METAKEY("fepa.metadato.elencodecretidaeliminare"), 
	
	/**
	 * Valore.
	 */
	WF_WOBPARENT("metadato.wobparent"),
	
	/**
	 * Valore.
	 */
	FEPA_UFFICIO_DESTINATARI("fepa.ufficio.destinatari"),

	/**
	 * Valore.
	 */
	FEPA_PERMESSI_DESTINATARI("fepa.permessi.destinatari"),

	/**
	 * Valore.
	 */
	FEPA_INDICE_CLASSIFICAZIONE_DEFAULT("fepa.indiceclassificazione.protocollazione"),


	/**
	 * Valore.
	 */
	//Corrisponde a fepa.ufficio.competenza su NSD
	FEPA_UFFICIO_COMPETENTE("fepa.ufficio.competente"),
	
	/**
	 * Valore.
	 */
	FEPA_NODO_UFFICIO_TERZO("fepa.ufficio_terzo.ufficio"),
	

	/**
	 * Valore.
	 */
	FEPA_UTENTE_UFFICIO_TERZO("fepa.ufficio_terzo.utente"),
	
	/**
	 * Valore.
	 */
	EVENTLOG_ID_TIPO_EVENTO("metadato.idtipoevento"),
	

	/**
	 * Valore.
	 */
	// Costanti per la predisposizione della dichiarazione
	FEPA_NODO_FIRMA("fepa.ufficio.firma"),
	/**
	 * Valore.
	 */
	FEPA_UTENTE_FIRMA("fepa.utente.firma"),
	
	// Sottoscrizioni
	/**
	 * Valore.
	 */
	SOTTOSCRIZIONI_URL_LIBROFIRMA("se.url.librofirma"),
	/**
	 * Valore.
	 */
	SOTTOSCRIZIONI_URL_LIBROFIRMA_EVO("se.url.librofirmaevo"),
	/**
	 * Valore.
	 */
	SOTTOSCRIZIONI_CONTEXTPATH_MOBILEAPP("se.contextpath.mobile.app"),
	/**
	 * Valore.
	 */
	SOTTOSCRIZIONI_CONTEXTPATH_EVOAPP("se.contextpath.evo.app"),
	/**
	 * Valore.
	 */
	SOTTOSCRIZIONI_MAIL_MITTENTE("se.mail.mittente"),
	/**
	 * Valore.
	 */
	SOTTOSCRIZIONI_MAIL_USERNAME_INVIO("se.mail.usernameinvio"),
	/**
	 * Valore.
	 */
	SOTTOSCRIZIONI_MAIL_PASSWORD_INVIO("se.mail.passwordinvio"),
	/**
	 * Valore.
	 */
	SOTTOSCRIZIONI_MAIL_PROXY_HOST("se.mail.proxyhost"),
	/**
	 * Valore.
	 */
	SOTTOSCRIZIONI_MAIL_PROXY_PORT("se.mail.proxyport"),
	/**
	 * Valore.
	 */
	SOTTOSCRIZIONI_MAIL_SERVER_OUT_AUTH("se.mail.serverOutAutenticazione"),
	/**
	 * Valore.
	 */
	SOTTOSCRIZIONI_MAIL_HOST_INVIO("se.mail.hostinvio"),
	/**
	 * Valore.
	 */
	SOTTOSCRIZIONI_MAIL_PORT_INVIO("se.mail.portinvio"),
	
	//Coda
	/**
	 * Valore.
	 */
	CODA_MAX_RESULTS("coda.max"),
	
	//inserire su db
	/**
	 * Valore.
	 */
	CODA_LIBRO_MAX_RESULTS("codaLibro.max"),
	/**
	 * Valore.
	 */
	CODA_CORRIERE_MAX_RESULTS("codaCorriere.max"),
	/**
	 * Valore.
	 */
	CODA_DA_LAVORARE_MAX_RESULTS("codaDaLavorare.max"),
	/**
	 * Valore.
	 */
	CODA_SOSPESO_MAX_RESULTS("codaSospeso.max"),
	/**
	 * Valore.
	 */
	CODA_SPEDIZIONE_MAX_RESULTS("codaSpedizione.max"),
	/**
	 * Valore.
	 */
	CODA_GIRO_VISTI_MAX_RESULTS("codaGiroVisti.max"),
	/**
	 * Valore.
	 */
	CODA_DSR_MAX_RESULTS("codaDsr.max"),
	/**
	 * Valore.
	 */
	CODA_FATTURE_IN_LAVORAZIONE_MAX_RESULTS("codaFattureLavorazione.max"),
	/**
	 * Valore.
	 */
	CODA_DD_DA_LAVORARE_MAX_RESULTS("codaDDLavorare.max"),
	/**
	 * Valore.
	 */
	CODA_DD_IN_FIRMA_MAX_RESULTS("codaDDinFirma.max"),
	/**
	 * Valore.
	 */
	CODA_DD_FIRMATI_MAX_RESULTS("codaDDFirmati.max"),
	/**
	 * Valore.
	 */
	CODA_DD_DA_FIRMARE_MAX_RESULTS("codaDDdaFirmare.max"),

	/**
	 * Valore.
	 */
	CODA_FATTURE_DA_LAVORARE_MAX_RESULTS("codaFattureDaLavorare.max"),

	/**
	 * Valore.
	 */
	// Ricerca
	RICERCA_MAX_RESULTS("ricerca.max"),

	/**
	 * Valore.
	 */
	RICERCA_FASCICOLI_MAX_RESULTS("ricercafascicoli.max"),

	/**
	 * Valore.
	 */
	RICERCA_FULLTEXT_MAX_RESULTS("ricercafulltext.max"), 

	/**
	 * Valore.
	 */
	IDAOO_RGS("idaoorgs"),

	/**
	 * Valore.
	 */
	IDAOO_RL("idaoorl"),

	/**
	 * Valore.
	 */
	IDAOO_DSII("idaoodag"),

	/**
	 * Valore.
	 */
	IDAOO_DF("idaooDF"),
	
	/**
	 * IGEPA
	 */
	IGEPA_ANNO_METAKEY("metadato.anno"),

	/**
	 * Valore.
	 */
	IGEPA_CODICE_ENTE_IGF_METAKEY("metadato.codiceEnteIGF"),

	/**
	 * Valore.
	 */
	IGEPA_TIPOLOGIA_ENTE_METAKEY("metadato.tipologia_ente"), 

	/**
	 * Valore.
	 */
	IGEPA_DESCRIZIONE_ENTE_METAKEY("metadato.descrizioneEnte"),

	/**
	 * Valore.
	 */
	IGEPA_FALDONE("igepa.faldone"),

	/**
	 * Valore.
	 */
	IGEPA_OPF_CLASSNAME_FN_METAKEY("filenet.classedocumentoopfigepa"),

	/**
	 * Valore.
	 */
	IGEPA_OPF_SEZIONE_METAKEY("metadato.sezioneopf"),

	/**
	 * Valore.
	 */
	IGEPA_OPF_ANNO_RICHIESTA_METAKEY("metadato.annorichiestaopf"),

	/**
	 * Valore.
	 */
	IGEPA_OPF_NUMERO_RICHIESTA_METAKEY("metadato.numerorichiestaopf"),

	/**
	 * Valore.
	 */
	PROVVEDIMENTO_NUMERO_CONTO_METAKEY("metadato.provvedimento.numeroconto"), 

	/**
	 * Valore.
	 */
	IGEPA_ID_CONTATTO("igepa.idcontatto"),

	/**
	 * Valore.
	 */
	IGEPA_UTENTE_CEDENTE("igepa.utentecedente"),

	/**
	 * Valore.
	 */
	ID_TIPOLOGIA_DECRETO_IVA("idTipologiaDecretoIVA"),

	/**
	 * Valore.
	 */
	ID_TIPOLOGIA_REVISORI_IGF_ENTRATA("idTipologiaRevisoriIGF"),

	/**
	 * Valore.
	 */
	ID_TIPOLOGIA_BILANCIO_ENTI_ENTRATA("idTipologiaBilancioEnti"),
	
	//NOTIFICHE HEADER

	/**
	 * Valore.
	 */
	NOTIFICHE_POLLING_SECONDI("notifiche.polling.secondi"),

	/**
	 * Valore.
	 */
	NOTIFICHE_SOTTOSCRIZIONI_ULTIMI_N_GIORNI("notifiche.sottoscrizioni.ultimi.n.giorni"),

	/**
	 * Valore.
	 */
	NOTIFICHE_RUBRICA_ULTIMI_N_GIORNI("notifiche.rubrica.ultimi.n.giorni"),

	// STATUS OAM

	/**
	 * Valore.
	 */
	URL_LOGOUT("url.logout"),

	// SESSION LISTENER STATUS

	/**
	 * Valore.
	 */
	SESSION_LISTENER_STATUS("session.listener.status"),
	

	/**
	 * Valore.
	 */
	LOCALSIGN_SERVERPORT_STATUS("localsign.serverport.status"), //on in fase di firma ribalti la porta del server

	/**
	 * Valore.
	 */
	X_UA_COMPATIBLE_VALUE("x.ua.compatible.value"), //valore da fornire per il compatibility filter, lasciare vuoto per disattivare
		

	/**
	 * Valore.
	 */
	// UCRA
	DESC_UFF_UCRA("descrnodoucra"),
	
	// COPIA CONFORME

	/**
	 * Valore.
	 */
	UFFICIO_COPIA_CONFORME_WF_METAKEY("metadato.nodoCopiaConforme"),

	/**
	 * Valore.
	 */
	UTENTE_COPIA_CONFORME_WF_METAKEY("metadato.utenteCopiaConforme"),
	
	// INFO ALLEGATI DA ALLACCI

	/**
	 * Valore.
	 */
	ALLEGATO_IDFASCICOLOPROVENIENZA_METAKEY("metadato.idfascicoloprovenienza"),

	/**
	 * Valore.
	 */
	ALLEGATO_DESCRIZIONEFASCICOLOPROVENIENZA_METAKEY("metadato.descrizionefascicoloprovenienza"),

	/**
	 * Valore.
	 */
	ALLEGATO_DOCUMENTTITLEDOCUMENTOPROVENIENZA_METAKEY("metadato.documenttitledocumentoprovenienza"),

	/**
	 * Valore.
	 */
	ALLEGATO_DESCRIZIONEDOCUMENTOPROVENIENZA_METAKEY("metadato.descrizionedocumentoprovenienza"),

	/**
	 * Valore.
	 */
	ALLEGATO_DOCUMENTTITLEALLEGATOPROVENIENZA_METAKEY("metadato.documenttitleallegatoprovenienza"),

	/**
	 * Valore.
	 */
	ALLEGATO_NOMEFILEALLEGATOPROVENIENZA_METAKEY("metadato.nomefileallegatoprovenienza"),
	

	/**
	 * Valore.
	 */
	INOLTRA_MAIL_CODA_ALLEGATI_MAX_DIM("inoltromailcoda.allegati.max.dim.tot"),
	
	// SIEBEL

	/**
	 * Valore.
	 */
	TARGET_ENDPOINT_SIEBELKEY("siebel.rds.ws.targetendpoint"),

	/**
	 * Valore.
	 */
	SIEBEL_RDS_WS_SISTEMA_ESTERNO("siebel.rds.ws.sistema.esterno"),

	/**
	 * Valore.
	 */
	SIEBEL_APRI_RDS_ALLEGATI_MAX_DIM_TOT("siebel.rds.allegati.max.dim.tot"), // value 31457280
	
	// RECOGNIFORM

	/**
	 * Valore.
	 */
	STATO_RECOGNIFORM_METAKEY("metadato.statorecogniform"),

	/**
	 * Valore.
	 */
	BARCODE_PRINCIPALE_METAKEY("metadato.barcodeprincipale"),

	/**
	 * Valore.
	 */
	STATO_DOC_CARTACEO_METAKEY("metadato.statodocumentocartaceo"),

	/**
	 * Valore.
	 */
	MAX_BARCODE_LENGTH("barcode.length"),
	
	// PROTOCOLLO DI EMERGENZA

	/**
	 * Valore.
	 */
	NUMERO_PROTOCOLLO_EMERGENZA_METAKEY("metadato.numeroprotocollo.emergenza"),

	/**
	 * Valore.
	 */
	ANNO_PROTOCOLLO_EMERGENZA_METAKEY("metadato.annoprotocollo.emergenza"),

	/**
	 * Valore.
	 */
	DATA_PROTOCOLLO_EMERGENZA_METAKEY("metadato.dataprotocollo.emergenza"),
	

	/**
	 * Valore.
	 */
	DATA_DOCUMENTO_DYNAMIC_METAKEY("metadato.dynamic.datadocumento"),

	/**
	 * Valore.
	 */
	DATA_ARRIVO_DYNAMIC_METAKEY("metadato.dynamic.dataarrivo"),
	

	/**
	 * Valore.
	 */
	NOTIFICA_DESTINATARIO_SPEDIZIONE_FN_METAKEY("metadato.spedizione.notificaDestinatario"),
	

	/**
	 * Valore.
	 */
	RUOLI_PREASSEGNATARIO("ruoli.preassegnatario"),
	
	// INTEROPERABILITA -> START

	/**
	 * Valore.
	 */
	PREASSEGNATARIO_SEGN_INTEROP_MAIL_METAKEY("metadato.mail.segnaturainterop.preassegnatario"),

	/**
	 * Valore.
	 */
	MAIL_MITTENTE_SEGN_INTEROP_MAIL_METAKEY("metadato.mail.segnaturainterop.mailmittente"),

	/**
	 * Valore.
	 */
	WARNINGS_VALIDAZIONE_SEGN_INTEROP_MAIL_METAKEY("metadato.mail.segnaturainterop.validazione.warnings"),

	/**
	 * Valore.
	 */
	ERRORI_VALIDAZIONE_SEGN_INTEROP_MAIL_METAKEY("metadato.mail.segnaturainterop.validazione.errori"),

	/**
	 * Valore.
	 */
	ID_DOC_PRINCIPALE_SEGN_INTEROP_MAIL_METAKEY("metadato.mail.segnaturainterop.docprincipale"),

	/**
	 * Valore.
	 */
	IDS_ALLEGATI_SEGN_INTEROP_MAIL_METAKEY("metadato.mail.segnaturainterop.allegati"),

	/**
	 * Valore.
	 */
	VALIDAZIONE_SEGN_INTEROP_MAIL_METAKEY("metadato.mail.segnaturainterop.validazione"),
	

	/**
	 * Valore.
	 */
	ID_MESSAGGIO_INTEROP_POSTA_NPS_MAIL_METAKEY("metadato.mail.postanps.idmessaggio"),
	

	/**
	 * Valore.
	 */
	RETRIEVE_EML_BOOL_INTEROP_POSTA_NPS("interop.postanps.retrieveeml"),
	// INTEROPERABILITA -> END


	/**
	 * Valore.
	 */
	PROTOCOLLI_GENERATI_MAIL_METAKEY("metadato.mail.protocolligeneratiemail"),
	
	/**
	 * Valore.
	 */
	PROTOCOLLA_E_MANTIENI_METAKEY("metadato.mail.protocollaemantieni"),
	

	/**
	 * Valore.
	 */
	REGISTRO_PROTOCOLLO_FN_METAKEY("metadato.registroprotocollo"),
	

	/**
	 * Valore.
	 */
	CODICE_FLUSSO_FN_METAKEY("metadato.codiceflusso"),
	

	/**
	 * Valore.
	 */
	INTEGRAZIONE_PROTOCOLLO_ARGO_ENDPOINT("integrazioneprotocollo.argo.endpoint"),

	/**
	 * Valore.
	 */
	INTEGRAZIONE_PROTOCOLLO_ARGO_CONNECTION_TIMEOUT_MS("integrazioneprotocollo.argo.connection.timeout.ms"),
	

	/**
	 * Valore.
	 */
	EXCEPTION_HANDLER_VALUE("exception.handler.value"),
	

	/**
	 * Valore.
	 */
	HTTP_CLIENT_POOL_MAX_CONN_TOT("http.client.pool.max.conn.tot"),

	/**
	 * Valore.
	 */
	HTTP_CLIENT_POOL_MAX_CONN_PER_ROUTE("http.client.pool.max.conn.per.route"),
	

	/**
	 * Valore.
	 */
	HTTP_CACHE_SECURE_CONTROL("http.cache.secure.control.flag"),
	
	// Limiti ricerca Contatti da WS

	/**
	 * Valore.
	 */
	REDWS_CAP_RUBRICA_RED("redws.cap.rubrica.red"),

	/**
	 * Valore.
	 */
	REDWS_CAP_RUBRICA_IPA("redws.cap.rubrica.ipa"),

	/**
	 * Valore.
	 */
	REDWS_CAP_RUBRICA_MEF("redws.cap.rubrica.mef"),
	

	/**
	 * Valore.
	 */
	INDIRIZZO_MAIL_MITTENTE_PROT("metadato.doc.indirizzomailprot"),
	

	/**
	 * Valore.
	 */
	PREASSEGNATARIO_CONOSCENZA_INTEROP_MAIL_METAKEY("metadato.mail.segnaturainterop.preassegnatariConoscenza"),
	

	/**
	 * Valore.
	 */
	AUTORE_ULTIMA_MODIFICA_NOTA_METAKEY("metadato.mail.autoreUltimaNota"),
	 

	/**
	 * Valore.
	 */
	FOLDER_MAIL_ARCHIVIATE_FN_METAKEY("folder.mail.fws.archiviate"),
	

	/**
	 * Valore.
	 */
	OPERAZIONE_POSTA_ELETTRONICA_FN_METAKEY("metadato.mail.operazionePostaElettronica"),
	

	/**
	 * Valore.
	 */
	//Cartella di destinazione delle mail obsolete da spostare
	FOLDER_DESTINAZIONE_MAIL_FN_METAKEY("folder.destinazione.mail"),
	

	/**
	 * Valore.
	 */
	FLAG_NORMALIZZAZIONE_NOME_FILE("flag.normalizzazione.nomefile"),
	

	/**
	 * Valore.
	 */
	FLAG_VERIFICA_CONTENT_FILE("flag.verifica.content"),
	 

	/**
	 * Valore.
	 */
	STAMPIGLIATURA_SIGLA_ALLEGATO("metadato.allegato.stampigliaturaSiglaAllegato"),

	/**
	 * Valore.
	 */
	PLACEHOLDER_SIGLA_ALLEGATO("metadato.allegato.placeholderSigla"),
 

	/**
	 * Valore.
	 */
	MOCK_ENABLED("mock.enabled"),
	
	// ENDPOINT SERVIZI NSD

	/**
	 * Valore.
	 */
	NSD_RICERCA_FASCICOLI_ENDPOINT("nsd.ws.fascicolo.endpoint"), 

	/**
	 * Valore.
	 */
	NSD_RICERCA_PROTOCOLLI_ENDPOINT("nsd.ws.protocollo.endpoint"),

	/**
	 * Valore.
	 */
	NSD_RICERCA_DOCUMENTI_ENDPOINT("nsd.ws.documento.endpoint"), 

	/**
	 * Valore.
	 */
	NSD_MAX_ROW_RICERCA_PROTOCOLLI("nsd.max.row.ricerca.protocolli"),

	/**
	 * Valore.
	 */
	NSD_MAX_ROW_RICERCA_FASCICOLI("nsd.max.row.ricerca.fascicoli"),
	

	/**
	 * Valore.
	 */
	ALLEGATO_NON_SBUSTATO("metadato.mail.fileNonSbustato"),
	

	/**
	 * Valore.
	 */
	DISABLE_LOG_DOWNLOAD_CONTENT_SERVLET("disable.log.download.content.servlet"),

	/**
	 * Valore.
	 */
	DISABLE_LOG_PROXY_FILENET("disable.log.proxy.filenet"),


	/**
	 * Valore.
	 */
	SOTTOCATEGORIA_DOCUMENTO_USCITA("metadato.sottocategoria.documento.uscita"),
		
	/**
	 * Chiave della properties 'assegnazioneIndiretta'.
	 */
	ASSEGNAZIONE_INDIRETTA_METAKEY("metadato.assegnazioneindiretta"),
	

	/**
	 * Valore.
	 */
	ASSEGNAZIONI_AUTOMATICHE_XLS_CELLA_INIZIO("assegnazioni.automatiche.xls.cellainizio"),
	
	/**
	 * Chiave della properties 'inLavorazione'.
	 */
	IN_LAVORAZIONE_METAKEY("metadato.inlavorazione"),
	
	/**
	 * Chiave della properties 'redisponiosservazione.uscita.tipologiadocumento.id', da concatenare con la stringa CODICEAOO + ".".
	 */
	ID_TIPOLOGIA_DOCUMENTO_USCITA_PREDISPONI_OSSERVAZIONE("predisponiosservazione.uscita.tipologiadocumento.id"),
	
	/**
	 * Chiave della properties 'richiestaintegrazioni.uscita.tipologiadocumento.id', da concatenare con la stringa CODICEAOO + ".".
	 */
	ID_TIPOLOGIA_DOCUMENTO_USCITA_RICHIESTA_INTEGRAZIONI("richiestaintegrazioni.uscita.tipologiadocumento.id"),
	
	/**
	 * Chiave della properties 'ritiroinautotutela.entrata.tipologiadocumento.id', da concatenare con la stringa CODICEAOO + ".".
	 */
	ID_TIPOLOGIA_DOCUMENTO_ENTRATA_RITIRO_IN_AUTOTUTELA("ritiroinautotutela.entrata.tipologiadocumento.id"),

	/**
	 * Chiave della properties 'attodecreto.uscita.tipologiadocumento.id', da concatenare con la stringa CODICEAOO + ".".
	 */
	ID_TIPOLOGIA_DOCUMENTO_USCITA_ATTO_DECRETO("attodecreto.uscita.tipologiadocumento.id"),
	
	/**
	 * Chiave della properties 'attodecreto.entrata.tipologiadocumento.id', da concatenare con la stringa CODICEAOO + ".".
	 */
	ID_TIPOLOGIA_DOCUMENTO_ENTRATA_ATTO_DECRETO("attodecreto.entrata.tipologiadocumento.id"),
	
	/**
	 * Chiave della property 'atti.soggetti.visto.entrata.tipologiadocumento.id', da concatenare con la stringa CODICEAOO + ".".
	 * */
	ID_TIPOLOGIA_DOCUMENTO_ENTRATA_ATTI_SOGGETTI_VISTO("atti.soggetti.visto.entrata.tipologiadocumento.id"),
	
	/**
	 * Chiave della property 'decreto.impegno.entrata.tipologiadocumento.id', da concatenare con la stringa CODICEAOO + ".".
	 * */
	ID_TIPOLOGIA_DOCUMENTO_ENTRATA_DECRETO_IMPEGNO("decreto.impegno.entrata.tipologiadocumento.id"),
	
	/**
	 * Chiave della property 'decreto.impegno.da.sicoge.entrata.tipologiadocumento.id', da concatenare con la stringa CODICEAOO + ".".
	 * */
	ID_TIPOLOGIA_DOCUMENTO_ENTRATA_DECRETO_IMPEGNO_DA_SICOGE("decreto.impegno.da.sicoge.entrata.tipologiadocumento.id"),
	
	/**
	 * Chiave della property 'gestione.bilancio.finanziario.entrata.tipologiadocumento.id', da concatenare con la stringa CODICEAOO + ".".
	 * */
	ID_TIPOLOGIA_DOCUMENTO_ENTRATA_GESTIONE_BILANCIO_FINANZIARIO("gestione.bilancio.finanziario.entrata.tipologiadocumento.id"),
	
	/**
	 * Chiave della property 'provvedimenti.pers.in.servizio.entrata.tipologiadocumento.id', da concatenare con la stringa CODICEAOO + ".".
	 * */
	ID_TIPOLOGIA_DOCUMENTO_ENTRATA_PROVVEDIMENTI_PERS_IN_SERVIZIO("provvedimenti.pers.in.servizio.entrata.tipologiadocumento.id"),
	
	/**
	 * Chiave della property 'decreto.accertamento.entrate.entrata.tipologiadocumento.id', da concatenare con la stringa CODICEAOO + ".".
	 * */
	ID_TIPOLOGIA_DOCUMENTO_ENTRATA_DECRETO_ACCERTAMENTO_ENTRATE("decreto.accertamento.entrate.entrata.tipologiadocumento.id"),
	
	/**
	 * Chiave della properties 'AMMINISTRAZIONE_ATTO_DECRETO'.
	 */
	AMMINISTRAZIONE_ATTO_DECRETO_METADATO_ESTESO_NAME("amministrazione.attodecreto.metadatoesteso.name"),
	
	/**
	 * Chiave della properties 'AMMINISTRAZIONE_ATTO_DECRETO'.
	 */
	ID_CONTATTO_MAIL_ATTO_DECRETO_USCITA_UCB("id.contatto.mail.attodecreto.uscita.ucb"),
	
	/**
	 * Chiave della properties 'RAGIONERIA_ATTO_DECRETO'.
	 */
	RAGIONERIA_ATTO_DECRETO_METADATO_ESTESO_NAME("ragioneria.attodecreto.metadatoesteso.name"),
	
	/**
	 * Chiave della properties 'UFFICIO_SEGRETERIA_ATTO_DECRETO'.
	 */
	UFFICIO_SEGRETERIA_ATTO_DECRETO_METADATO_ESTESO_NAME("uffsegreteria.attodecreto.metadatoesteso.name"),
	
	
	/**
	 * Chiave della properties 'UFFICIO_SEGRETERIA_ATTO_DECRETO'.
	 */
	NUM_PROT_ATTO_DECRETO_METADATO_ESTESO_NAME("numeroprot.attodecreto.metadatoesteso.name"),
	
	/**
	 * Chiave della properties 'UFFICIO_SEGRETERIA_ATTO_DECRETO'.
	 */
	DATA_PROT_ATTO_DECRETO_METADATO_ESTESO_NAME("dataprot.attodecreto.metadatoesteso.name"),
	
	
	/**
	 * Chiave della properties 'UFFICIO_SEGRETERIA_ATTO_DECRETO'.
	 */
	AOO_PROT_ATTO_DECRETO_METADATO_ESTESO_NAME("aooprot.attodecreto.metadatoesteso.name"),
	
	/**
	 * Chiave della properties 'NOME_TIPO_DOC_RIFERIMENTO'.
	 */
	NOME_TIPO_DOC_RIFERIMENTO_METADATO_ESTESO_NAME("nometipodocriferimento.metadatoesteso.name"),
	
	/**
	 * Chiave della properties 'ANNO_PROTOCOLLO'.
	 */
	ANNO_PROTOCOLLO_METADATO_ESTESO_NAME("annoprotocollo.metadatoesteso.name"),
	
	/**
	 * Chiave della properties 'NUMERO_PROTOCOLLO'.
	 */
	NUMERO_PROTOCOLLO_METADATO_ESTESO_NAME("numeroprotocollo.metadatoesteso.name"),
	
	/**
	 * Chiave della properties 'TIPO_DOCUMENTO'.
	 */
	TIPO_DOCUMENTO_METADATO_ESTESO_NAME("tipodocumento.metadatoesteso.name"),
	
	/**
	 * Chiave della properties 'TIPO_DOCUM'.
	 */
	LOOKUP_TABLE_SELETTORE_TIPO_DOCUMENTO("lookuptable.selettore.tipodocumento"),
	
	/**
	 * Chiave della properties 'annullaTemplate'.
	 */
	ANNULLA_TEMPLATE_METAKEY("metadato.annullatemplate"),
	
	/**
	 * Chiave della properties 'protocolloRiferimento'.
	 */
	PROTOCOLLO_RIFERIMENTO_METAKEY("metadato.protocolloriferimento"),
	
	/**
	 * Chiave della properties 'metadato.integrazione.dati'.
	 */
	INTEGRAZIONE_DATI_METAKEY("metadato.integrazione.dati"),
	
	/**
	 * Chiave della properties 'metadato.numero.reg.aux'.
	 */
	NUMERO_REGISTRAZIONE_AUX_METALKEY("metadato.numero.reg.aux"),
	
	/**
	 * Chiave della properties 'metadato.data.reg.aux'.
	 */
	DATA_REGISTRAZIONE_AUX_METALKEY("metadato.data.reg.aux"),
	
	/**
	 * Chiave della properties 'metadato.nome.reg.aux'.
	 */
	NOME_REGISTRAZIONE_AUX_METALKEY("metadato.nome.reg.aux"),
	
	/**
	 * Chiave della properties 'template.numero.reg.aux'.
	 */
	NUMERO_REGISTRAZIONE_AUX_TEMPLATE("template.numero.reg.aux"),
	
	/**
	 * Chiave della properties 'template.data.reg.aux'.
	 */
	DATA_REGISTRAZIONE_AUX_TEMPLATE("template.data.reg.aux"),
	
	/**
	 * Chiave della properties 'template.data.firma'.
	 */
	DATA_FIRMA_TEMPLATE("template.data.firma"),

	/**
	 * Chiave della properties 'tipo.ritiro.autotutela', da concatenare con la stringa CODICEAOO + ".".
	 */
	TIPO_RITIRO_AUTOTUTELA("tipo.ritiro.autotutela"),
	
	/**
	 * Chiave della properties 'identificatoreProcesso'.
	 */
	IDENTIFICATORE_PROCESSO_METAKEY("metadato.identificatoreprocesso"),

	/**
	 * Chiave della property 'descrizione.corte.conti' da concatenare col codice AOO + ".".
	 * */
	
	DESCRIZIONE_CORTE_DEI_CONTI("descrizione.corte.conti"),
	
	/**
	 * Chiave della property 'indirizzo.corte.conti' da concatenare col codice AOO + "."
	 * E' l'indirizzo email della corte dei conti.
	 * */
	
	ID_CONTATTO_CORTE_DEI_CONTI("id.contatto.corte.conti"),
	
	/**
	 * Chiave della property 'ids.registri.ausiliari.dest.corte.dei.conti' da concatenare con codice AOO + "." 
	 * Gli id dei registri devono essere separati da ';'.
	 * */
	IDS_REGISTRI_DEST_CORTE_DEI_CONTI("ids.registri.ausiliari.dest.corte.dei.conti"),
	
	/**
	 * Chiave della property 'descrizione.amministrazione.regaux' da concatenare con codice AOO + "." 
	 * Consente di popolare il metadato "amministrazione-protocollo-mittente" in fase di creazione template registri ausiliari.
	 * */
	DESCRIZIONE_AMMINISTRAZIONE_REGAUX("descrizione.amministrazione.regaux"),
	
	/**
	 * Chiave della property 'username.protocollatore.nps' da concatenare con codice AOO + "." .
	 * */
	USERNAME_PROTOCOLLATORE_NPS("username.protocollatore.nps"),
	
	/**
	 * Chiave della property 'nodo.protocollatore.nps' da concatenare con codice AOO + "." .
	 * */
	NODO_PROTOCOLLATORE_NPS("nodo.protocollatore.nps"),
	
	/**
	 * Chiave della property 'metadato.fascicolo.tipofascicolofepa' per il metadato FileNet 'tipoFascicoloFepa' del fascicolo.
	 */
	METADATO_FASCICOLO_TIPO_FASCICOLO_FEPA_METAKEY("metadato.fascicolo.tipofascicolofepa"),
	
	
	/**
	 * Chiave della property 'nps.warning.codici': sulla base dati saranno registrati n differenti chiavi del tipo
	 * nps.warning.codici.n, dove n va da 1 al numero di codici da gestire.
	 */
	NPS_WARNING_CODICI("nps.warning.codici"),
	
	/**
	 * Chiave della property 'nps.warning.descrizioni': sulla base dati saranno registrati n differenti chiavi del tipo
	 * nps.warning.descrizioni.n, dove n va da 1 al numero di codici da gestire.
	 */
	NPS_WARNING_DESCRIZIONI("nps.warning.descrizioni"),
	
	/*METADATI RUBRICA START*/
	
	/**
	 * Valore.
	 */
	CONTATTO_METADATO_CLASSNAME("contatto.classname"),
	
	/**
	 * Valore.
	 */
	LISTA_CONTATTI_METADATO_CLASSNAME("listaContatti.classname"),
	
	/**
	 * Valore.
	 */
	CONTATTO_METADATO_PROXY("metadato.contatto.contattoProxy"),
	
	/**
	 * Valore.
	 */
	OBJECTID_CONTATTO_MITT_METADATO("metadato.documento.objectIdContMittente"),
	
	/**
	 * Valore.
	 */
	OBJECTID_CONTATTO_DEST_METADATO("metadato.documento.objectIdContDestinatario"),
	
	/**
	 * Valore.
	 */
	ALIAS_CONTATTO_ON_THE_FLY("metadato.contatto.alias"),
	
	/**
	 * Valore.
	 */
	NOME_CONTATTO_ON_THE_FLY("metadato.contatto.nome"),
	
	/**
	 * Valore.
	 */
	COGNOME_CONTATTO_ON_THE_FLY("metadato.contatto.cognome"),
	
	/**
	 * Valore.
	 */
	MAILPEC_CONTATTO_ON_THE_FLY("metadato.contatto.mailpec"),
	
	/**
	 * Valore.
	 */
	MAILPEO_CONTATTO_ON_THE_FLY("metadato.contatto.mailpeo"),
	
	/**
	 * Valore.
	 */
	ID_CONTATTO_ON_THE_FLY("metadato.contatto.idContatto"), 
	
	/**
	 * Valore.
	 */
	IS_ON_THE_FLY("metadato.contatto.isOnTheFly"), 
	
	
	/**
	 * Valore.
	 */
	TIPO_CONTATTO_ON_THE_FLY("metadato.contatto.tipoContatto"),
	
	/**
	 * Valore.
	 */
	INDIRIZZO_CONTATTO_ON_THE_FLY("metadato.contatto.indirizzoContatto"),
	
	/**
	 * Valore.
	 */
	CAP_CONTATTO_ON_THE_FLY("metadato.contatto.capContatto"),
	
	/**
	 * Valore.
	 */
	TELEFONO_CONTATTO_ON_THE_FLY("metadato.contatto.telefonoContatto"),
	
	/**
	 * Valore.
	 */
	CELLULARE_CONTATTO_ON_THE_FLY("metadato.contatto.cellulareContatto"),
	
	/**
	 * Valore.
	 */
	FAX_CONTATTO_ON_THE_FLY("metadato.contatto.faxContatto"),
	
	/**
	 * Valore.
	 */
	ID_REGIONE_CONTATTO_ON_THE_FLY("metadato.contatto.idRegioneContatto"),
	
	/**
	 * Valore.
	 */
	ID_PROVINCIA_CONTATTO_ON_THE_FLY("metadato.contatto.idProvinciaContatto"),
	
	/**
	 * Valore.
	 */
	ID_COMUNE_CONTATTO_ON_THE_FLY("metadato.contatto.idComuneContatto"),
	
	/**
	 * Valore.
	 */
	TITOLO_CONTATTO_ON_THE_FLY("metadato.contatto.titoloContatto"),
	
	/**
	 * Valore.
	 */
	IS_TIPOLOGIA_PEC_CONTATTO("metadato.contatto.isTipologiaPEC"),
	/*METADATI RUBRICA END*/
	
	/**
	 * Chiave della property 'rispondia.codice.titolario.default' da concatenare col codice AOO + "."
	 * E' l'indice di titolario di default per documenti creati con la notifica azione RispondiA di NPS.
	 * */
	RISPONDIA_CODICE_TITOLARIO_DEFAULT("rispondia.codice.titolario.default"),
	
	/**
	 * Valore.
	 */
	DATA_SCARICO_METAKEY("metadato.dataScarico"),
	
	/**
	 * Valore.
	 */
	ID_UTENTE_DELEGATO_VISTO_WF_METAKEY("metadato.idUtenteDestinatarioDelegato"), 
	
	/**
	 * Valore.
	 */
	VERIFICA_FIRMA_DOC_ALLEGATO("metadato.documento.verificaFirmaDocAll"),
	
	/**
	 * Chiave della properties 'SICOGE_DECRETO_LIQUIDAZIONE_METADATO_ESTESO_NAME'.
	 */
	SICOGE_DECRETO_LIQUIDAZIONE_METADATO_ESTESO_NAME("sicoge.decreto.liquidazione.metadatoesteso.name"),
	
	/**
	 * Chiave della properties 'SICOGE_DECRETO_LIQUIDAZIONE_METADATO_ESTESO_NAME'.
	 */
	DATA_ATTIVAZIONE_GIRO_VISTI_DELEGATO("parametro.dataAttivazioneGiroVistiDelegato"),
	
	/**
	 * Flag associato all'avvio dell'iter di firma asincrono.
	 */
	FLAG_FIRMA_ASINCRONA_AVVIATA_WF_METAKEY("metadato.firmaasincrona.avviata"),
	
	/**
	 * Tempo di timout per la gestione degli item di firma asincrona.
	 */
	TIMOUT_FIRMA_ASINCRONA("firmaasincrona.timout.firma"),
	
	/**
	 * Chiave della property 'fepa.warning.codici': sulla base dati saranno registrati n differenti chiavi del tipo
	 * fepa.warning.codici.n, dove n va da 1 al numero di codici da gestire
	 */
	FEPA_WARNING_CODICI("fepa.warning.codici"),
	
	/**
	 * Chiave della property 'fepa.warning.descrizioni': sulla base dati saranno registrati n differenti chiavi del tipo
	 * fepa.warning.descrizioni.n, dove n va da 1 al numero di codici da gestire
	 */
	FEPA_WARNING_DESCRIZIONI("fepa.warning.descrizioni"),
	
	/**
	 * Massima dimensione in MB spacchettamento allegati
	 */
	MAX_SIZE_SPACCHETTAMENTO_MAIL_MB("parametro.maxSizeSpacchettamentoMailMB"),
	
	/**
	 * Chiave della properties 'timestamp da'.
	 */
	TIMESTAMP_OPERAZIONE_DA("metadato.timestampoperazioneda"),
	
	/**
	 * Chiave della properties 'timestamp a'.
	 */
	TIMESTAMP_OPERAZIONE_A("metadato.timestampoperazionea"),
	
	/**
	 * Chiave della properties 'creazione da'.
	 */
	DATA_CREAZIONE_DA("metadato.datacreazioneda"),
	
	/**
	 * Chiave della properties 'creazione a'.
	 */
	DATA_CREAZIONE_A("metadato.datacreazionea"),
	
	/**
	 * Chiave della property 'CODICE_AOO'_ESTRATTO_CONTO_TRIMESTRALE_CCVT.
	 * Il valore restituisto sarà una stringa contenente
	 * tipo doc. e tipo e proc. separati da una virgola.
	 */
	ESTRATTO_CONTO_TRIMESTRALE_CCVT("estratto.conto.trimestrale.ccvt"),
	
	/**
	 * Chiave della property 'CODICE_AOO'_CONTO_CONSUNTIVO_SEDE_ESTERA.
	 * Il valore restituisto sarà una stringa contenente
	 * tipo doc. e tipo proc. separati da una virgola.
	 */
	CONTO_CONSUNTIVO_SEDE_ESTERA("conto.consuntivo.sede.estera"),
	
	/**
	 * Chiave della property 'CODICE_AOO'_CONTO_CONSUNTIVO_SEDE_ESTERA_FLUSSO.
	 * Il valore restituisto sarà una stringa contenente
	 * tipo doc. e tipo proc. separati da una virgola.
	 */
	CONTO_CONSUNTIVO_SEDE_ESTERA_FLUSSO("conto.consuntivo.sede.estera.flusso"),
	
	/**
	 * Chiave della property 'CODICE_AOO'_CONTO_GIUDIZIALE.
	 * Il valore restituisto sarà una stringa contenente
	 * tipo doc. e tipo proc. separati da una virgola.
	 */
	CONTO_GIUDIZIALE("conto.giudiziale"),
	
	/**
	 * Chiave della property 'CODICE_AOO'_CONTO_GIUDIZIALE_FLUSSO.
	 * Il valore restituisto sarà una stringa contenente
	 * tipo doc. e tipo proc. separati da una virgola.
	 */
	CONTO_GIUDIZIALE_FLUSSO("conto.giudiziale.flusso"),
	
	/**
	 * Property generica che fa riferimento ad una generica stringa custom da rimpiazzare per la correzione del file html del flusso SIPAD.
	 * Occcorre appendere alla property il numero a cui fa riferimento la property richiesta. Ad es. per recuperare la prima property occorre
	 * recuperare <code> sipad.replace.custom.text.1 </code>.
	 */
	FLUSSO_SIPAD_REPLACE_CUSTOM_TEXT("sipad.replace.custom.text"),
	
	/**
	 * Property che identifica tutti i mittenti associati al flusso SIPAD, i mittenti sono separati da ";".
	 */
	FLUSSO_SIPAD_MITTENTI("sipad.email.mittenti"),
	
	/**
	 * Nome property che identifica l'elenco degli id dei metadati che devono essere validati con criterio "like".
	 * Il value della property elenca tutti i metadati per i quali vale questa regola separandoli da ";".
	 * La property deve essere concatenata con la stringa CODICEAOO + "." in quanto parametrica per AOO.
	 */
	METADATO_LOOKUPTABLE_LIKE("metadatoesteso.lookuptable.like"),
	
	/**
	 * Chiave della property 'CODICE_AOO'_TIPODOCUMENTO_NOTIFICA.
	 * Il valore restituisto sarà una stringa contenente
	 * la lista delle coppie tipo documento/tipo procedimento separate da ';'.
	 */
	TIPODOCUMENTO_NOTIFICA("tipodocumento.notifica"),
	
	RICERCA_UTENTI_DISATTIVI("ricercaUtentiDisattivi"),
	
	SELETTORE_SEDI_ESTERE("selettoreSediEstere"),
	
	MAX_SIZE_EMAIL_BCK("maxSizeEmailBCK"),
	
	
	;
	/**
	 * Chiave del parametro.
	 */
	private String key;
	
	/**
	 * Costruttore.
	 * @param inKey	chiave
	 */
	PropertiesNameEnum(final String inKey) {
		key = inKey;
	}
	
	/**
	 * Getter chiave.
	 * @return	chiave del parametro
	 */
	public String getKey() {
		return key;
	}
}
