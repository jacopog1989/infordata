package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IVerificaFirmaFacadeSRV;

/**
 * Interfaccia del servizio che gestisce la funzionalità di verifica firma.
 */
public interface IVerificaFirmaSRV extends IVerificaFirmaFacadeSRV {
   
	/**
	 * Metodo per l'inserimento dopo la creazione su filenet.
	 * @param idAoo
	 * @param documentTitle
	 * @param stato
	 * @param objectStore
	 * @param classeDocumentale
	 * @param versDocumento
	 * @return true o false
	 */
	boolean insertDopoCreazioneSuFilenet(Long idAoo, String documentTitle, Integer stato, String objectStore,
			String classeDocumentale, Integer versDocumento);

	/**
	 * Metodo per la delete prima della creazione su filenet.
	 * @param idAoo
	 * @param documentTitle 
	 * @param objectStore
	 * @param classeDocumentale
	 * @param versDocumento
	 * @return true o false
	 */
	boolean deleteSingoloElemento(Long idAoo, String documentTitle, String objectStore,
			String classeDocumentale, Integer versDocumento);
}
