package it.ibm.red.business.dto;

import java.io.Serializable;

import it.ibm.red.business.utils.StringUtils;

/**
 * DTO che definisce il template metadato valore.
 */
public class TemplateMetadatoValueDTO extends TemplateMetadatoDTO implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 8884824499272657064L;

	/**
	 * Valore.
	 */
	private String value;

	/**
	 * Costruttore vuoto.
	 */
	public TemplateMetadatoValueDTO() {
		super();
	}

	/**
	 * @return value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Restituisce il value codificato in HTML.
	 * @return value
	 */
	public String getEncodedHTMLValue() {
		if (StringUtils.isNullOrEmpty(value)) {
			return value;
		} else {
			return value.replace("&", "&amp;");
		}
	}

	/**
	 * Imposta il value.
	 * @param value
	 */
	public void setValue(final String value) {
		this.value = value;
	}

	/**
	 * @see java.lang.Object#toString().
	 */
	@Override
	public String toString() {
		return super.toString() + " | TemplateValue [value=" + value + "]";
	}
}