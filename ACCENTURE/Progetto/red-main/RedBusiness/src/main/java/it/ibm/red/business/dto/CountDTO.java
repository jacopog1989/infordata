/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.Map;

import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.logger.REDLogger; 

/**
 * Raccoglie i contatori delle liste.
 * 
 * Offre dei metodi di utilità che permettono data la coda di ritornare il giusto contatore
 * 
 * @author APerquoti
 *
 */
public class CountDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1549600579166374046L;
	
	/**
     * Logger.
     */
	private static final REDLogger LOGGER = REDLogger.getLogger(CountDTO.class);
	
    /**
     * Numero documenti da lavorare.
     */
	private Integer daLavorare;
	
    /**
     * Numero documenti in sospeso.
     */
	private Integer sospeso;
	
    /**
     * Numero documenti stornati.
     */
	private Integer stornati;
	
    /**
     * Numero documenti libro firma.
     */
	private Integer libroFirma;
	
    /**
     * Numero documenti libro firma delegato.
     */
	private Integer libroFirmaDelegato;
	
    /**
     * Numero procedimenti.
     */
	private Integer procedimenti;
	
    /**
     * Numero documenti agli atti.
     */
	private Integer atti;
	
    /**
     * Numero documenti in risposta.
     */
	private Integer risposta;
	
    /**
     * Numero documenti in mozione.
     */
	private Integer mozione;
	
    /**
     * Numero documenti nel corriere.
     */
	private Integer corriere;
	
    /**
     * Bipartizione corriere: assegnazione diretta.
     */
	private Integer corrierediretto;

	/**
     * Bipartizione corriere: assegnazione indiretta.
     */
	private Integer corriereindiretto;

	/**
	 * Documenti in coda: assegnati.
	 */
	private Integer assegnate;

	/**
	 * Documenti in coda: chiusi.
	 */
	private Integer chiuse;

	/**
	 * Documenti in coda: girovisti.
	 */
	private Integer giroVisti;

	/**
	 * Documenti in coda: spedizione.
	 */
	private Integer spedizione;

	/**
	 * Documenti in coda: acquisizione.
	 */
	private Integer inAcquisizione;

	/**
	 * Documenti in coda: acquisiti.
	 */
	private Integer acquisiti;

	/**
	 * Documenti in coda: eliminati.
	 */
	private Integer eliminati;

	/**
	 * Documenti in coda: spedizione in errore.
	 */
	private Integer spedizioneErrore; 

	/**
	 * Documenti in coda: richiama documenti.
	 */
	private Integer richiamaDocumenti;

	/**
	 * Documenti in coda: da lavorare (UCB).
	 */
	private Integer daLavorareUCB;

	/**
	 * Documenti in coda: in lavorazione (UCB).
	 */
	private Integer inLavorazioneUCB;
	
	/**
	 * Contatore Preparazione alla spedizione.
	 */
	private Integer preparazioneSpedizione;
	
	/**
	 * Count dei documenti provenienti dall'organigramma scrivania.
	 * 
	 * Questo count viene settato dinamicamente in base al count selezionato dell'organigramma scrivania.
	 * 
	 */
	private Integer orgScrivania;

	/**
	 * Costruttore di default.
	 */
	public CountDTO() {
		super();
	}
	
	/**
	 * init del DTO con i valori recuperati dal Service per il menu Attivita.
	 * 
	 * @param map
	 */
	public CountDTO(final Map<Integer, Integer> map) {
		super();
		
		daLavorare = map.get(DocumentQueueEnum.DA_LAVORARE.getId());
		sospeso = map.get(DocumentQueueEnum.SOSPESO.getId());
		stornati = map.get(DocumentQueueEnum.STORNATI.getId());
		libroFirma = map.get(DocumentQueueEnum.NSD.getId());
		procedimenti = map.get(DocumentQueueEnum.PROCEDIMENTI.getId());
		atti = map.get(DocumentQueueEnum.ATTI.getId());
		risposta = map.get(DocumentQueueEnum.RISPOSTA.getId());
		mozione = map.get(DocumentQueueEnum.MOZIONE.getId());
		corriere = map.get(DocumentQueueEnum.CORRIERE.getId());
		corrierediretto = map.get(DocumentQueueEnum.CORRIERE_DIRETTO.getId());
		corriereindiretto = map.get(DocumentQueueEnum.CORRIERE_INDIRETTO.getId());
		assegnate = map.get(DocumentQueueEnum.ASSEGNATE.getId());
		chiuse = map.get(DocumentQueueEnum.CHIUSE.getId());
		giroVisti = map.get(DocumentQueueEnum.GIRO_VISTI.getId());
		spedizione = map.get(DocumentQueueEnum.SPEDIZIONE.getId());
		libroFirmaDelegato = map.get(DocumentQueueEnum.NSD_LIBRO_FIRMA_DELEGATO.getId());	
		inAcquisizione = map.get(DocumentQueueEnum.IN_ACQUISIZIONE.getId());	
		acquisiti = map.get(DocumentQueueEnum.ACQUISITI.getId());	
		eliminati = map.get(DocumentQueueEnum.ELIMINATI.getId());	
		LOGGER.info("SPEDIZIONE ERRORE - START " + map.get(DocumentQueueEnum.SPEDIZIONE_ERRORE.getId()));
		spedizioneErrore = map.get(DocumentQueueEnum.SPEDIZIONE_ERRORE.getId()); 
		LOGGER.info("SPEDIZIONE ERRORE - END " + spedizioneErrore);
		richiamaDocumenti = map.get(DocumentQueueEnum.RICHIAMA_DOCUMENTI.getId());
		daLavorareUCB = map.get(DocumentQueueEnum.DA_LAVORARE_UCB.getId());
		inLavorazioneUCB = map.get(DocumentQueueEnum.IN_LAVORAZIONE_UCB.getId());
		preparazioneSpedizione = map.get(DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE.getId());
	}

	/**
	 * Restituisce il numero di documenti presenti nella coda queue.
	 * @param queue per cui occorre conoscere il numero di documenti
	 * @return totale dei documenti presenti nella coda
	 */
	public Integer getQueueCount(final DocumentQueueEnum queue) {
		Integer total = null;
		
		if (DocumentQueueEnum.DA_LAVORARE.equals(queue)) {
			total = daLavorare;
		} else if (DocumentQueueEnum.SOSPESO.equals(queue)) {
			total = sospeso;
		} else if (DocumentQueueEnum.STORNATI.equals(queue)) {
			total = stornati;
		} else if (DocumentQueueEnum.NSD.equals(queue)) {
			total = libroFirma;
		} else if (DocumentQueueEnum.PROCEDIMENTI.equals(queue)) {
			total = procedimenti;
		} else if (DocumentQueueEnum.ATTI.equals(queue)) {
			total = atti;
		} else if (DocumentQueueEnum.RISPOSTA.equals(queue)) {
			total = risposta;
		} else if (DocumentQueueEnum.MOZIONE.equals(queue)) {
			total = mozione;
		} else if (DocumentQueueEnum.CORRIERE.equals(queue)) {
			total = corriere;
		} else if (DocumentQueueEnum.CORRIERE_DIRETTO.equals(queue)) {
			total = corrierediretto;
		} else if (DocumentQueueEnum.CORRIERE_INDIRETTO.equals(queue)) {
			total = corriereindiretto;
		} else if (DocumentQueueEnum.ASSEGNATE.equals(queue)) {
			total = assegnate;
		} else if (DocumentQueueEnum.CHIUSE.equals(queue)) {
			total = chiuse;
		} else if (DocumentQueueEnum.GIRO_VISTI.equals(queue)) {
			total = giroVisti;
		} else if (DocumentQueueEnum.SPEDIZIONE.equals(queue)) {
			total = spedizione;
		} else if (DocumentQueueEnum.ORG_SCRIVANIA.equals(queue)) {
			total = orgScrivania;
		} else if (DocumentQueueEnum.NSD_LIBRO_FIRMA_DELEGATO.equals(queue)) {
			total = libroFirmaDelegato;
		} else if (DocumentQueueEnum.IN_ACQUISIZIONE.equals(queue)) {
			total = inAcquisizione;
		} else if (DocumentQueueEnum.ACQUISITI.equals(queue)) {
			total = acquisiti;
		} else if (DocumentQueueEnum.ELIMINATI.equals(queue)) {
			total = eliminati;
		} else if (DocumentQueueEnum.RICHIAMA_DOCUMENTI.equals(queue)) {
			total = richiamaDocumenti;
		} else if (DocumentQueueEnum.DA_LAVORARE_UCB.equals(queue)) {
			total = daLavorareUCB;
		} else if (DocumentQueueEnum.IN_LAVORAZIONE_UCB.equals(queue)) {
			total = inLavorazioneUCB;
		} else if (DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE.equals(queue)) {
			total = preparazioneSpedizione;
		}
		
		return total;
	}

	/**
	 * @return the daLavorare
	 */
	public final Integer getDaLavorare() {
		return daLavorare;
	}

	/**
	 * @param daLavorare the daLavorare to set
	 */
	public final void setDaLavorare(final Integer daLavorare) {
		this.daLavorare = daLavorare;
	}

	/**
	 * @return the sospeso
	 */
	public final Integer getSospeso() {
		return sospeso;
	}

	/**
	 * @param sospeso the sospeso to set
	 */
	public final void setSospeso(final Integer sospeso) {
		this.sospeso = sospeso;
	}

	/**
	 * @return the stornati
	 */
	public final Integer getStornati() {
		return stornati;
	}

	/**
	 * @param stornati the stornati to set
	 */
	public final void setStornati(final Integer stornati) {
		this.stornati = stornati;
	}

	/**
	 * @return the libroFirma
	 */
	public final Integer getLibroFirma() {
		return libroFirma;
	}

	/**
	 * @param libroFirma the libroFirma to set
	 */
	public final void setLibroFirma(final Integer libroFirma) {
		this.libroFirma = libroFirma;
	}

	/**
	 * @return the procedimenti
	 */
	public final Integer getProcedimenti() {
		return procedimenti;
	}

	/**
	 * @param procedimenti the procedimenti to set
	 */
	public final void setProcedimenti(final Integer procedimenti) {
		this.procedimenti = procedimenti;
	}

	/**
	 * @return the atti
	 */
	public final Integer getAtti() {
		return atti;
	}

	/**
	 * @param atti the atti to set
	 */
	public final void setAtti(final Integer atti) {
		this.atti = atti;
	}

	/**
	 * @return the risposta
	 */
	public final Integer getRisposta() {
		return risposta;
	}

	/**
	 * @param risposta the risposta to set
	 */
	public final void setRisposta(final Integer risposta) {
		this.risposta = risposta;
	}

	/**
	 * @return the mozione
	 */
	public final Integer getMozione() {
		return mozione;
	}

	/**
	 * @param mozione the mozione to set
	 */
	public final void setMozione(final Integer mozione) {
		this.mozione = mozione;
	}

	/**
	 * @return the corriere
	 */
	public final Integer getCorriere() {
		return corriere;
	}

	/**
	 * @param corriere the corriere to set
	 */
	public final void setCorriere(final Integer corriere) {
		this.corriere = corriere;
	}

	/**
	 * Restituisce il numero di documenti presenti nel corriere diretto.
	 * @return numero doc corriere diretto
	 */
	public Integer getCorrierediretto() {
		return corrierediretto;
	}

	/**
	 * Imposta il numero di documenti presenti nel corriere diretto.
	 * @param corrierediretto
	 */
	public void setCorrierediretto(final Integer corrierediretto) {
		this.corrierediretto = corrierediretto;
	}

	/**
	 * Restituisce il numero di documenti presenti nel corriere indiretto.
	 * @return numero doc corriere indiretto
	 */
	public Integer getCorriereindiretto() {
		return corriereindiretto;
	}

	/**
	 * Imposta il numero di documenti presenti nel corriere indiretto.
	 * @param corriereindiretto
	 */
	public void setCorriereindiretto(final Integer corriereindiretto) {
		this.corriereindiretto = corriereindiretto;
	}

	/**
	 * @return the assegnate
	 */
	public final Integer getAssegnate() {
		return assegnate;
	}

	/**
	 * @param assegnate the assegnate to set
	 */
	public final void setAssegnate(final Integer assegnate) {
		this.assegnate = assegnate;
	}

	/**
	 * @return the chiuse
	 */
	public final Integer getChiuse() {
		return chiuse;
	}

	/**
	 * @param chiuse the chiuse to set
	 */
	public final void setChiuse(final Integer chiuse) {
		this.chiuse = chiuse;
	}

	/**
	 * @return the giroVisti
	 */
	public final Integer getGiroVisti() {
		return giroVisti;
	}

	/**
	 * @param giroVisti the giroVisti to set
	 */
	public final void setGiroVisti(final Integer giroVisti) {
		this.giroVisti = giroVisti;
	}

	/**
	 * @return the spedizione
	 */
	public final Integer getSpedizione() {
		return spedizione;
	}

	/**
	 * @param spedizione the spedizione to set
	 */
	public final void setSpedizione(final Integer spedizione) {
		this.spedizione = spedizione;
	}

	/**
	 * @return the libroFirmaDelegato
	 */
	public final Integer getLibroFirmaDelegato() {
		return libroFirmaDelegato;
	}

	/**
	 * @param libroFirmaDelegato the libroFirmaDelegato to set
	 */
	public final void setLibroFirmaDelegato(final Integer libroFirmaDelegato) {
		this.libroFirmaDelegato = libroFirmaDelegato;
	}

	/**
	 * Restituisce il contatore associato ad orgScrivania.
	 * @return contatore orgScrivania
	 */
	public Integer getOrgScrivania() {
		return orgScrivania;
	}

	/**
	 * Imposta il contatore associato ad orgScrivania.
	 * @param orgScrivania
	 */
	public void setOrgScrivania(final Integer orgScrivania) {
		this.orgScrivania = orgScrivania;
	}

	/**
	 * Restituisce il contatore di InAcquisizione.
	 * @return contatore InAcquisizione
	 */
	public Integer getInAcquisizione() {
		return inAcquisizione;
	}

	/**
	 * Imposta il contatore di InAcquisizione.
	 * @param inAcquisizione
	 */
	public void setInAcquisizione(final Integer inAcquisizione) {
		this.inAcquisizione = inAcquisizione;
	}

	/**
	 * Restituisci il contatore di Acqusiti.
	 * @return contatore acquisiti
	 */
	public Integer getAcquisiti() {
		return acquisiti;
	}

	/**
	 * Imposta il contatore di Acquisiti.
	 * @param acquisiti
	 */
	public void setAcquisiti(final Integer acquisiti) {
		this.acquisiti = acquisiti;
	}

	/**
	 * Restituisce il contatore associato ad Eliminati.
	 * @return contatore eliminati
	 */
	public Integer getEliminati() {
		return eliminati;
	}

	/**
	 * Imposta il contatore associato ad Eliminati.
	 * @param contatore eliminati
	 */
	public void setEliminati(final Integer eliminati) {
		this.eliminati = eliminati;
	}

	/**
	 * Restituisce il contatore associato alla spedizione in errore.
	 * @return contatore spedizioneErrore
	 */
	public Integer getSpedizioneErrore() {
		return spedizioneErrore;
	}

	/**
	 * Imposta il contatore associato alla spedizione in errore.
	 * @param spedizioneErrore
	 */
	public void setSpedizioneErrore(final Integer spedizioneErrore) {
		this.spedizioneErrore = spedizioneErrore;
	}

	/**
	 * Restituisce il contatore associato alla spedizione in Richiama documenti.
	 * @return contatore richiama documenti
	 */
	public Integer getRichiamaDocumenti() {
		return richiamaDocumenti;
	}

	/**
	 * Imposta il contatore associato alla spedizione in Richiama documenti.
	 * @param richiamaDocumenti
	 */
	public void setRichiamaDocumenti(final Integer richiamaDocumenti) {
		this.richiamaDocumenti = richiamaDocumenti; 
	}

	/**
	 * Restituisce il contatore associato alla coda "Da lavorare UCB".
	 * @return contatore Da Lavorare UCB
	 */
	public Integer getDaLavorareUCB() {
		return daLavorareUCB;
	}

	/**
	 * Imposta il contatore associato alla coda "Da Lavorare UCB".
	 * @param daLavorareUCB
	 */
	public void setDaLavorareUCB(final Integer daLavorareUCB) {
		this.daLavorareUCB = daLavorareUCB;
	}

	/**
	 * Restituisce il contatore associato alla coda "In Lavorazione UCB".
	 * @return contatore coda In Lavorazione UCB
	 */
	public Integer getInLavorazioneUCB() {
		return inLavorazioneUCB;
	}

	/**
	 * Imposta il contatore associato alla coda "In Lavorazione UCB".
	 * @param inLavorazioneUCB
	 */
	public void setInLavorazioneUCB(final Integer inLavorazioneUCB) {
		this.inLavorazioneUCB = inLavorazioneUCB;
	}

	/**
	 * Restituisce il numero di documenti nella coda preparazione alla spedizione.
	 * @return numero documenti coda preparazione alla spedizione
	 */
	public Integer getPreparazioneSpedizione() {
		return preparazioneSpedizione;
	}

	/**
	 * Restituisce il numero di documenti nella coda preparazione alla spedizione di FEPA.
	 * @param preparazioneSpedizione numero documenti coda preparazione alla spedizione
	 */
	public void setPreparazioneSpedizione(Integer preparazioneSpedizione) {
		this.preparazioneSpedizione = preparazioneSpedizione;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CountDTO [daLavorare=" + daLavorare + ", sospeso=" + sospeso + ", stornati=" + stornati
				+ ", libroFirma=" + libroFirma + ", libroFirmaDelegato=" + libroFirmaDelegato + ", procedimenti="
				+ procedimenti + ", atti=" + atti + ", risposta=" + risposta + ", mozione=" + mozione + ", corriere="
				+ corriere + ", corrierediretto=" + corrierediretto + ", corriereindiretto=" + corriereindiretto
				+ ", assegnate=" + assegnate + ", chiuse=" + chiuse + ", giroVisti=" + giroVisti + ", spedizione="
				+ spedizione + ", inAcquisizione=" + inAcquisizione + ", acquisiti=" + acquisiti + ", eliminati="
				+ eliminati + ", spedizioneErrore=" + spedizioneErrore + ", richiamaDocumenti=" + richiamaDocumenti
				+ ", daLavorareUCB=" + daLavorareUCB + ", inLavorazioneUCB=" + inLavorazioneUCB
				+ ", preparazioneSpedizione=" + preparazioneSpedizione + ", orgScrivania=" + orgScrivania + "]";
	}
	
}