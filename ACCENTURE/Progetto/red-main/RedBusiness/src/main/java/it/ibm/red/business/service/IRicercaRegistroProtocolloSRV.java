package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IRicercaRegistroProtocolloFacadeSRV;

/**
 * The Interface IRicercaRegistroProtocolloSRV.
 *
 * @author m.crescentini
 * 
 *         Interfaccia del servizio per la gestione della ricerca nel Registro Protocollo.
 */
public interface IRicercaRegistroProtocolloSRV extends IRicercaRegistroProtocolloFacadeSRV {

}