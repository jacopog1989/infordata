package it.ibm.red.business.enums;

/**
 * Enum contenente i nomi dei metadati
 * relativi ai report di monitoraggio
 * estratti conto trimestrali CCVT UCB.
 * 
 * @author DarioVentimiglia
 */
public enum EstrattiContoCCVTEnum {

	/**
	 * Esercizio.
	 */
	ESERCIZIO("ESERCIZIO"),
	
	/**
	 * Trimestre.
	 */
	TRIMESTRE("TRIMESTRE"),
	
	/**
	 * Codice sede estera.
	 */
	CODICE_SEDE_ESTERA("COD_SED_EST"),
	
	/**
	 * Descrizione sede estera identificata dal {@link #CODICE_SEDE_ESTERA}.
	 */
	DESCRIZIONE_SEDE_ESTERA("SEDE_ESTERA"),
	
	/**
	 * Valuta.
	 */
	VALUTA("DIVISA1"),
	
	/**
	 * Primo metadato saldo.
	 */
	SALDO_CCVT_1T("IMPO_SAL1"),
	
	/**
	 * Secondo metadato saldo.
	 */
	SALDO_CCVT_2T("IMPO_SAL2"),
	
	/**
	 * Terzo metadato saldo.
	 */
	SALDO_CCVT_3T("IMPO_SAL3"),
	
	/**
	 * Quarto metadato saldo.
	 */
	SALDO_CCVT_4T("IMPO_SAL4");
	
	/**
	 * Nome metadato.
	 */
	private String metadatoName;
	
	/**
	 * Costruttore.
	 * @param metadatoName
	 */
	private EstrattiContoCCVTEnum(String metadatoName) {
		this.metadatoName = metadatoName;
	}

	/**
	 * Restituisce il nome del metadato.
	 * @return nome metadato
	 */
	public String getMetadatoName() {
		return metadatoName;
	}
}
