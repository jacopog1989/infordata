package it.ibm.red.business.dto;

import java.util.Date;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class ContributoDTO.
 *
 * @author CPIERASC
 * 
 * 	DTO per gestire i contributi interni ed esterni.
 */
public class ContributoDTO extends VerificaFirmaDTO {
	
	/**
	 * Serializzable.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Data contributo.
	 */
	private String dataContributo;

	/**
	 * Descrizione utente.
	 */
	private String descUtente;

	/**
	 * Descrizione ufficio.
	 */
	private String descUfficio;

	/**
	 * Descrizione aoo.
	 */
	private String descAoo;

	/**
	 * Flag esterno.
	 */
	private Boolean flagEsterno;
	
	/**
	 * Identificativo contributo.
	 */
	private Long idContributo;

	/**
	 * Protocollo.
	 */
	private String protocollo;
	
	/**
	 * Filename.
	 */
	private String filename;

	/**
	 * Mime type.
	 */
	private String mimeType;

	/**
	 * Document title.
	 */
	private String documentTitle;

	/**
	 * Identificativo del documento originale a cui è associato il contributo.
	 */
	private Integer idDocumento;
		
	/**
	 * Campo che si trova sul db sia per quelli esterni che interni.
	 */
	private String nota;
	
    /**
     * Content.
     */
	private byte[] content;
	
    /**
     * Data CdC.
     */
	private Date dataCdc;
	
    /**
     * Foglio.
     */
	private Integer foglio;
	
    /**
     * Registro.
     */
	private Integer registro;
	
    /**
     * Identificativo utente.
     */
	private Long idUtente;
	
    /**
     * Guid contributo.
     */
	private String guidContributo;
	
    /**
     * Mittente.
     */
	private String mittente;
	
    /**
     * Flag modificabile.
     */
	private Boolean modificabile;
		 
    /**
     * Indice contribuente.
     */
	private String indiceContr;
	
	/**
	 * Costruttore.
	 * 
	 * @param inIdContributo
	 * @param inDataContributo
	 * @param inDescUtente
	 * @param inDescUfficio
	 * @param inDescAoo
	 * @param inFlagEsterno
	 * @param inFilename
	 * @param inMimeType
	 * @param inDocumentTitle
	 * @param nota
	 */
	public ContributoDTO(final Long inIdContributo, final String inDataContributo, final String inDescUtente, final String inDescUfficio, 
			final String inDescAoo, final Boolean inFlagEsterno, final String inFilename, final String inMimeType, 
			final String inDocumentTitle, final String inNota, final Long inIdUtente, final String inGuidContributo, final Integer inIdDocumento) {
		super();
		this.idContributo = inIdContributo;
		this.dataContributo = inDataContributo;
		this.descUtente = inDescUtente;
		this.descUfficio = inDescUfficio;
		this.descAoo = inDescAoo;
		this.flagEsterno = inFlagEsterno;
		this.filename = inFilename;
		this.mimeType = inMimeType;
		this.documentTitle = inDocumentTitle;
		this.nota = inNota;
		this.idUtente = inIdUtente;
		this.guidContributo = inGuidContributo;
		this.idDocumento = inIdDocumento;
	}	

	/**
	 * Costruttore.
	 * 
	 * @param inIdContributo
	 * @param inDataContributo
	 * @param inProtocollo
	 * @param inFlagEsterno
	 * @param inFilename
	 * @param inMimeType
	 * @param inDocumentTitle
	 * @param nota
	 */
	public ContributoDTO(final Long inIdContributo, final String inDataContributo, final String inProtocollo, final Boolean inFlagEsterno, 
			final String inFilename, final String inMimeType, final String inDocumentTitle, final String inNota, final Long inIdUtente,
			final String inGuidContributo, final String inMittente, final Boolean inModificabile, final Integer inIdDocumento, final Integer validitaFirma) {
		super();
		this.idContributo = inIdContributo;
		this.dataContributo = inDataContributo;
		this.protocollo = inProtocollo;
		this.flagEsterno = inFlagEsterno;
		this.filename = inFilename;
		this.mimeType = inMimeType;
		this.documentTitle = inDocumentTitle;
		this.nota = inNota;
		this.idUtente = inIdUtente;
		this.guidContributo = inGuidContributo;
		this.mittente = inMittente;
		this.modificabile = inModificabile;
		this.idDocumento = inIdDocumento;
		this.valoreVerificaFirma = ValueMetadatoVerificaFirmaEnum.get(validitaFirma);
	}	
	
	/**
	 * Costruttore.
	 * 
	 * @param inIdContributo
	 * @param inIdDocumento
	 * @param inDocumentTitle
	 * @param inFlagEsterno
	 * @param nota
	 */
	public ContributoDTO(final Long inIdContributo, final Integer inIdDocumento, final String inDocumentTitle, final Boolean inFlagEsterno, final String nota) {
		this.idContributo = inIdContributo;
		this.flagEsterno = inFlagEsterno;
		this.documentTitle = inDocumentTitle;
		this.idDocumento = inIdDocumento;
		this.nota = nota;
	}

	/**
	 * Costruttore che imposta solo la nota del contributo.
	 * @param nota
	 */
	public ContributoDTO(final String nota) {
		this.nota = nota;
	}

	/**
	 * Costruttore che imposta filename, mimetype e content.
	 * @param filename
	 * @param mimeType
	 * @param content
	 */
	public ContributoDTO(final String filename, final String mimeType, final byte[] content) {
		this.content = content;
		this.mimeType = mimeType;
		this.filename = filename;
	}

	/**
	 * Costruttore di default.
	 */
	public ContributoDTO() {
		super();
	}

	/**
	 * Restituisce il nome del file associato al contributo includendo l'estensione PDF.
	 * @param idContributo
	 * @return filename che rispetta lo standard PDF
	 */
	public static final String getNomeNotaPdf(final Long idContributo) {
		return "nota" + idContributo + ".pdf";
	}
	
	/**
	 * Getter.
	 * 
	 * @return	identificativo documento
	 */
	public final Integer getIdDocumento() {
		return idDocumento;
	}
	
	/**
	 * @param idDocumento the idDocumento to set
	 */
	public void setIdDocumento(final Integer idDocumento) {
		this.idDocumento = idDocumento;
	}

	/**
	 * Getter.
	 * 
	 * @return	mime type
	 */
	public final String getMimeType() {
		return mimeType;
	}

	/**
	 * Getter.
	 * 
	 * @return	document title
	 */
	public final String getDocumentTitle() {
		return documentTitle;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	filename
	 */
	public final String getFilename() {
		return filename;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	identificativo contributo
	 */
	public final Long getIdContributo() {
		return idContributo;
	}

	/**
	 * Getter protocollo.
	 * 
	 * @return	protocollo
	 */
	public final String getProtocollo() {
		return protocollo;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	data contributo in forma testuale
	 */
	public final String getDataContributo() {
		return dataContributo;
	}

	/**
	 * Getter.
	 * 
	 * @return	descrizione utente
	 */
	public final String getDescUtente() {
		return descUtente;
	}

	/**
	 * Getter.
	 * 
	 * @return	descrizione ufficio
	 */
	public final String getDescUfficio() {
		return descUfficio;
	}

	/**
	 * Getter.
	 * 
	 * @return	descrizione aoo
	 */
	public final String getDescAoo() {
		return descAoo;
	}

	/**
	 * Getter.
	 * 
	 * @return	flag esterno
	 */
	public final Boolean getFlagEsterno() {
		return flagEsterno;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	nota
	 */
	public String getNota() {
		return nota;
	}

	/**
	 * Imposta la nota del contributo.
	 * @param nota
	 */
	public void setNota(final String nota) {
		this.nota = nota;
	}

	/**
	 * Getter.
	 * 
	 * @return	content
	 */
	public byte[] getContent() {
		return content;
	}

	/**
	 * Getter.
	 * 
	 * @return	foglio
	 */
	public Integer getFoglio() {
		return foglio;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	registro
	 */
	public Integer getRegistro() {
		return registro;
	}
	
	/**
	 * Setter.
	 * 
	 * @param idContributo
	 */
	public void setIdContributo(final Long idContributo) {
		this.idContributo = idContributo;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	dataCdc
	 */
	public Date getDataCdc() {
		return dataCdc;
	}

	/**
	 * Setter.
	 * 
	 * @param dataCdc the dataCdc to set
	 */
	public void setDataCdc(final Date dataCdc) {
		this.dataCdc = dataCdc;
	}

	/**
	 * Setter.
	 * 
	 * @param foglio the foglio to set
	 */
	public void setFoglio(final Integer foglio) {
		this.foglio = foglio;
	}

	/**
	 * Setter.
	 * 
	 * @param registro the registro to set
	 */
	public void setRegistro(final Integer registro) {
		this.registro = registro;
	}
	
	/**
	 * @return the idUtente
	 */
	public Long getIdUtente() {
		return idUtente;
	}

	/**
	 * @return the guidContributo
	 */
	public String getGuidContributo() {
		return guidContributo;
	}
	
	/**
	 * @param guidContributo the guidContributo to set
	 */
	public void setGuidContributo(final String guidContributo) {
		this.guidContributo = guidContributo;
	}

	/**
	 * @return the mittente
	 */
	public String getMittente() {
		return mittente;
	}
	
	/**
	 * @param mittente the mittente to set
	 */
	public void setMittente(final String mittente) {
		this.mittente = mittente;
	}

	/**
	 * @return the modificabile
	 */
	public Boolean getModificabile() {
		return modificabile;
	}

	/**
	 * @param flagEsterno the flagEsterno to set
	 */
	public void setFlagEsterno(final Boolean flagEsterno) {
		this.flagEsterno = flagEsterno;
	}

	/**
	 * Imposta i parametri del file di un contributo.
	 * @param fileName
	 * @param mimeType
	 * @param contents
	 */
	public void setFile(final String fileName, final String mimeType, final byte[] contents) {
		this.filename = fileName;
		this.mimeType = mimeType;
		this.content = contents;
	}

	/**
	 * Restituisce l'abstract text di una nota.
	 * @return String
	 */
	public String getAbstractNota() {
		String output = Constants.EMPTY_STRING;
		
		if (!StringUtils.isNullOrEmpty(nota)) {
			output = StringUtils.getTextAbstract(nota, 54);
		}
		
		return output;
	}

	/**
	 * Restituisce l'indice di contributo.
	 * @return indiceContr
	 */
	public String getIndiceContr() {
		return indiceContr;
	}

	/**
	 * Imposta l'indice di contributo.
	 * @param indiceContr
	 */
	public void setIndiceContr(final String indiceContr) {
		this.indiceContr = indiceContr;
	}
}