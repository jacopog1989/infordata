package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;

import com.filenet.api.collection.AccessPermissionList;
import com.filenet.api.collection.FolderSet;
import com.filenet.api.collection.Integer32List;
import com.filenet.api.collection.StringList;
import com.filenet.api.constants.CompoundDocumentState;
import com.filenet.api.constants.SecurityPrincipalType;
import com.filenet.api.core.Document;
import com.filenet.api.core.Folder;
import com.filenet.api.security.AccessPermission;
import com.filenet.apiimpl.property.PropertyImpl;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.DocumentoWsDTO;
import it.ibm.red.business.dto.SecurityWsDTO;
import it.ibm.red.business.dto.SecurityWsDTO.GruppoWs;
import it.ibm.red.business.dto.SecurityWsDTO.UtenteWs;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class FromDocumentoToDocumentoWsTrasformer.
 *
 * @author m.crescentini
 * 
 *         Trasformer documento WS.
 */
public class ContextFromDocumentoToDocumentoWsTrasformer extends ContextTrasformerCE<DocumentoWsDTO> {

	private static final long serialVersionUID = -5550530174350382835L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContextFromDocumentoToDocumentoWsTrasformer.class.getName());

	/**
	 * Dao nodo.
	 */
	private final INodoDAO nodoDAO;

	/**
	 * Utente nodo.
	 */
	private final IUtenteDAO utenteDAO;

	/**
	 * Costruttore.
	 */
	public ContextFromDocumentoToDocumentoWsTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENT_TO_DOC_WS);
		nodoDAO = ApplicationContextProvider.getApplicationContext().getBean(INodoDAO.class);
		utenteDAO = ApplicationContextProvider.getApplicationContext().getBean(IUtenteDAO.class);
	}

	/**
	 * Trasform.
	 *
	 * @param document   the document
	 * @param connection the connection
	 * @return the DocumentoWs DTO
	 */
	@SuppressWarnings("unchecked")
	@Override
	public final DocumentoWsDTO trasform(final Document document, final Connection connection, final Map<ContextTrasformerCEEnum, Object> context) {
		DocumentoWsDTO docWs = null;

		try {
			final String guid = StringUtils.cleanGuidToString(document.get_Id());
			final String classeDocumentale = document.getClassName();
			final String documentTitle = (String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			final String oggetto = (String) getMetadato(document, PropertiesNameEnum.OGGETTO_METAKEY);
			final String nomeFile = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.NOME_FILE_METAKEY);
			final Date dataModifica = document.get_DateCheckedIn(); // cfr. FWS
			final Integer numeroVersione = document.get_MajorVersionNumber();

			byte[] content = null;
			String contentType = null;
			Integer numeroDocumento = null;
			Integer annoDocumento = null;
			Integer numeroProtocollo = null;
			Integer annoProtocollo = null;
			String folder = null;
			Map<String, Object> metadati = null;
			List<DocumentoWsDTO> allegati = null;
			List<SecurityWsDTO> securityList = null;

			// Content (se richiesto)
			final boolean withContent = Boolean.TRUE.equals(context.get(ContextTrasformerCEEnum.RECUPERA_CONTENT));

			if (withContent) {
				if (FilenetCEHelper.hasDocumentContentTransfer(document)) {
					content = FilenetCEHelper.getDocumentContentAsByte(document);
					contentType = document.get_MimeType();
				} else {
					LOGGER.warn("Non è stato trovato il content del documento con ID: " + documentTitle);
				}
			}

			if (!PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY).equals(classeDocumentale)) {
				numeroDocumento = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
				annoDocumento = (Integer) getMetadato(document, PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY);
				numeroProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
				annoProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);

				// Folder
				folder = Constants.EMPTY_STRING;
				final FolderSet folders = document.get_FoldersFiledIn();

				if (folders != null && !folders.isEmpty()) {
					final Iterator<?> itFolders = folders.iterator();

					if (itFolders.hasNext()) {
						folder = ((Folder) itFolders.next()).get_FolderName();
					}
				}

				// Metadati (se richiesti)
				final boolean withMetadati = Boolean.TRUE.equals(context.get(ContextTrasformerCEEnum.RECUPERA_METADATI));

				if (withMetadati) {
					metadati = new HashMap<>();
					final Set<String> classDefinitionPropertyList = (Set<String>) context.get(ContextTrasformerCEEnum.CLASS_DEFINITION_PROPERTIES);

					final Iterator<?> itMetadati = document.getProperties().iterator();
					while (itMetadati.hasNext()) {
						final PropertyImpl property = (PropertyImpl) itMetadati.next();

						if (classDefinitionPropertyList.contains(property.getKey())) {
							final Object valoreMetadato = property.getObjectValue();

							if (valoreMetadato != null) {
								if (valoreMetadato instanceof StringList || valoreMetadato instanceof Integer32List) {
									final List<String> valore = new ArrayList<>();

									for (final Object valoreSingolo : ((StringList) valoreMetadato)) {
										valore.add(String.valueOf(valoreSingolo));
									}

									if (!CollectionUtils.isEmpty(valore)) {
										metadati.put((String) property.getKey(), valore);
									}
								} else {
									final String valore = String.valueOf(valoreMetadato);

									if (!StringUtils.isNullOrEmpty(valore)) {
										metadati.put((String) property.getKey(), valore);
									}
								}
							}
						}
					}
				}

				// Allegati
				if (CompoundDocumentState.COMPOUND_DOCUMENT_AS_INT == document.get_CompoundDocumentState().getValue()) {
					allegati = new ArrayList<>();

					final Iterator<?> itAllegati = document.get_ChildDocuments().iterator();
					while (itAllegati.hasNext()) {
						final DocumentoWsDTO allegato = TrasformCE.transform((Document) itAllegati.next(), TrasformerCEEnum.FROM_DOCUMENT_TO_DOC_WS, context, connection);

						if (allegato != null) {
							allegati.add(allegato);
						}
					}
				}
			}

			// Security
			final AccessPermissionList acl = document.get_Permissions();
			if (!CollectionUtils.isEmpty(acl)) {
				securityList = new ArrayList<>();

				final Iterator<?> itAcl = acl.iterator();
				while (itAcl.hasNext()) {
					final SecurityWsDTO security = new SecurityWsDTO();

					final AccessPermission perm = (AccessPermission) itAcl.next();
					final String accessPermissionName = perm.get_GranteeName();

					if (!"#AUTHENTICATED-USERS".equals(accessPermissionName) && !"#CREATOR-OWNER".equals(accessPermissionName)
							&& !SecurityPrincipalType.UNKNOWN.equals(perm.get_GranteeType())) {

						final String ufficioOUtente = perm.get_GranteeName().split("@")[0];

						// Gruppo (Ufficio)
						if (SecurityPrincipalType.GROUP.equals(perm.get_GranteeType())) {

							final String[] groupPrefixes = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.GROUP_SECURITY_PREFIX).split("\\|");

							// groupPrefixes[0] = "red" (ufficio)
							// groupPrefixes[1] = "VIRred" (corriere)
							final String idGruppo = org.apache.commons.lang3.StringUtils.replaceIgnoreCase(
									org.apache.commons.lang3.StringUtils.replaceIgnoreCase(ufficioOUtente, groupPrefixes[1], Constants.EMPTY_STRING), groupPrefixes[0],
									Constants.EMPTY_STRING);

							final Nodo nodo = nodoDAO.getNodo(Long.valueOf(idGruppo), connection);

							if (nodo != null) {
								final GruppoWs gruppoSecurity = security.getGruppo();
								gruppoSecurity.setIdUfficio(nodo.getIdNodo().intValue());
								gruppoSecurity.setIdSiap(-1);
								gruppoSecurity.setDescrizione(nodo.getDescrizione());
								if (nodo.getIdNodoPadre() != null) {
									gruppoSecurity.setIdUfficioPadre(nodo.getIdNodoPadre().intValue());
								}

								security.setGruppo(gruppoSecurity);
							}
							// Utente
						} else {
							final Utente utente = utenteDAO.getByUsername(ufficioOUtente, connection);

							if (utente != null) {
								final UtenteWs utenteSecurity = security.getUtente();
								utenteSecurity.setIdUtente(utente.getIdUtente().intValue());
								utenteSecurity.setIdSiap(-1);
								utenteSecurity.setUsername(utente.getUsername());
								utenteSecurity.setNome(utente.getNome());
								utenteSecurity.setCognome(utente.getCognome());
								utenteSecurity.setEmail(utente.getEmail());
								utenteSecurity.setDataDisattivazione(utente.getDataDisattivazione());

								security.setUtente(utenteSecurity);
							}
						}

						securityList.add(security);
					}
				}
			}

			docWs = new DocumentoWsDTO(guid, documentTitle, nomeFile, content, contentType, numeroDocumento, annoDocumento, numeroProtocollo, annoProtocollo, oggetto,
					dataModifica, allegati, numeroVersione, classeDocumentale, folder, metadati, securityList);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero di un metadato durante la trasformazione da documento FileNet a DocumentoWsDTO", e);
		}

		return docWs;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextTrasformerCE#
	 *      trasform(com.filenet.api.core.Document, java.util.Map).
	 */
	@Override
	public DocumentoWsDTO trasform(final Document document, final Map<ContextTrasformerCEEnum, Object> context) {
		throw new RedException();
	}

}
