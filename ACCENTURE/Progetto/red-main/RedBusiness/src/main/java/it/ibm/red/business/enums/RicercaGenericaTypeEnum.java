package it.ibm.red.business.enums;

/**
 * The Enum RicercaGenericaTypeEnum.
 *
 * @author CPIERASC
 * 
 *         Enum per la tipologia di ricerca.
 */
public enum RicercaGenericaTypeEnum {

	/**
	 * Tutte le parole.
	 */
	TUTTE(1, "Tutte le parole"),
	
	/**
	 * Qualsiasi parola.
	 */
	QUALSIASI(2, "Qualsiasi parola"),
	
	/**
	 * Frase esatta.
	 */
	ESATTA(3, "Frase esatta");
	
	/**
	 * Chiave.
	 */
	private int key;

	/**
	 * Label.
	 */
	private String label;

	/**
	 * Costruttore.
	 * 
	 * @param inKey		chiave
	 * @param inLabel	label
	 */
	RicercaGenericaTypeEnum(final int inKey, final String inLabel) {
		this.key = inKey;
		this.label = inLabel;
	}


	/**
	 * Getter chiave.
	 * 
	 * @return chiave
	 */
	public int getKey() {
		return key;
	}

	/**
	 * Getter label.
	 * 
	 * @return label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Restituisce l'enum associato al type in ingresso.
	 * @param typeValue
	 * @return enum associata
	 */
	public static RicercaGenericaTypeEnum get(final String typeValue) {
		RicercaGenericaTypeEnum output = null;
		for (RicercaGenericaTypeEnum e:RicercaGenericaTypeEnum.values()) {
			if (String.valueOf(e.getKey()).equals(typeValue)) {
				 output = e;
			}
		}
		return output;
	}
	
}
