package it.ibm.red.business.dto;

import java.util.Map;

/**
 * DTO per la gestione delle stampigliature dei segni grafici.
 */
public class StampigliaturaSegnoGraficoDTO extends AbstractDTO {
 
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1476902505416479418L;
 
	/**
	 * Mappa placeholder.
	 */
	private Map<String, byte[]> placeholderUtente;
	
	/**
	 * Colore.
	 */
	private Integer red;
	
	/**
	 * Colore.
	 */
	private Integer green;
	
	/**
	 * Colore.
	 */
	private Integer blue;

	/**
	 * Costruttore.
	 * @param inPlaceholderUtente
	 * @param inRed
	 * @param inGreen
	 * @param inBlue
	 */
	public StampigliaturaSegnoGraficoDTO(final Map<String, byte[]> inPlaceholderUtente, final Integer inRed, final Integer inGreen, final Integer inBlue) {
		placeholderUtente = inPlaceholderUtente; 
		red = inRed;
		green = inGreen;
		blue = inBlue;		
	}

	/**
	 * Restituisce la lista dei placeholder utente.
	 * @return placeholder utente
	 */
	public Map<String, byte[]> getPlaceholderUtente() {
		return placeholderUtente;
	}

	/**
	 * Imposta la lista dei placeholder utente.
	 * @param placeholderUtente
	 */
	public void setPlaceholderUtente(final Map<String, byte[]> placeholderUtente) {
		this.placeholderUtente = placeholderUtente;
	}

	/**
	 * Restituisce il valore acquisito dal colore rosso.
	 * @return red
	 */
	public Integer getRed() {
		return red;
	}
	
	/**
	 * Imposta il valore acquisito dal colore rosso.
	 * @param red
	 */
	public void setRed(final Integer red) {
		this.red = red;
	}
	
	/**
	 * Restituisce il valore acquisito dal colore verde.
	 * @return green
	 */
	public Integer getGreen() {
		return green;
	}
	
	/**
	 * Imposta il valore acquisito dal colore verde.
	 * @param green
	 */
	public void setGreen(final Integer green) {
		this.green = green;
	}
	
	/**
	 * Restituisce il valore acquisito dal colore blue.
	 * @return
	 */
	public Integer getBlue() {
		return blue;
	}
	
	/**
	 * Imposta il valore acquisito dal colore blu.
	 * @param blue
	 */
	public void setBlue(final Integer blue) {
		this.blue = blue;
	}

}
