package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;

import it.ibm.red.business.dto.ClientDTO;

/**
 * 
 * @author CPIERASC
 *
 *	Interfaccia del dao per la gestione di un utente.
 */
public interface ILogDAO extends Serializable {
	
	/**
	 * Metodo per il tracciamento dell'esecuzione del download di un contenuto.
	 * 
	 * @param idUtente				identificativo utente
	 * @param idRuolo				identificativo ruolo
	 * @param idUfficio				identificativo ufficio
	 * @param documentTitle			document title
	 * @param contentDescription	content description
	 * @param client				dati chiamante
	 * @param connection			connessione al database
	 */
	void insertLogDownload(Long idUtente, Long idRuolo, Long idUfficio, String documentTitle, String contentDescription,
			ClientDTO client, String source, Connection connection);
	
}
