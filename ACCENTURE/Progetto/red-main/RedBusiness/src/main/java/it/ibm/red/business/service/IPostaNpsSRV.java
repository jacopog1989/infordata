package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IPostaNpsFacadeSRV;

/**
 * Interfaccia del servizio di posta NPS.
 */
public interface IPostaNpsSRV extends IPostaNpsFacadeSRV {

}
