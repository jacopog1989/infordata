package it.ibm.red.business.enums;

/**
 * Enum stato Rds Siebel.
 */
public enum StatoRdSSiebelEnum {

	/**
	 * Valore.
	 */
	APERTA(0, "APERTURA"), 
	
	/**
	 * Valore.
	 */
	CHIUSA(1, "CHIUSURA"), 
	
	/**
	 * Valore.
	 */
	ANNULLATA(2, "ANNULLAMENTO");
	
	
	/**
	 * codice.
	 */
	private int statoCode;
	
	/**
	 * Evento.
	 */
	private String evento;
	
	/**
	 * Costruttore.
	 * @param statoCode
	 * @param evento
	 */
	StatoRdSSiebelEnum(final int statoCode, final String evento) {
		this.statoCode = statoCode;
		this.evento = evento;
	}
	
	/**
	 * Restituisce il codice dello stato.
	 * @return codice stato
	 */
	public int getStatoCode() {
		return this.statoCode;
	}
	
	/**
	 * Restituisce l'evento.
	 * @return evento
	 */
	public String getEvento() {
		return this.evento;
	}
	
	/**
	 * Restituisce l'enum associata al codice in ingresso.
	 * @param id
	 * @return enum associata al codice
	 */
	public static StatoRdSSiebelEnum getByCode(final int id) {
		for (final StatoRdSSiebelEnum s: StatoRdSSiebelEnum.values()) {
			if (s.getStatoCode() == id) {
				return s;
			}
		}
		return null;
	}
	
	/**
	 * Restituisce l'enum associata all'evento in ingresso.
	 * @param evento
	 * @return enum associata all'evento
	 */
	public static StatoRdSSiebelEnum getByEvento(final String evento) {
		for (final StatoRdSSiebelEnum s: StatoRdSSiebelEnum.values()) {
			if (s.getEvento().equals(evento)) {
				return s;
			}
		}
		return null;
	}
}