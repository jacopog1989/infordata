package it.ibm.red.business.dto;

import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * @author SLac
 *
 */
public class IterApprovativoDTO extends AbstractDTO {
	

	/**
	 * Fima manuale.
	 */
	public static final Integer ITER_FIRMA_MANUALE = 0;
	

	/**
	 * Livello ispettore.
	 */
	public static final Integer LIVELLO_ISPETTORE = 2;

	/**
	 * Livello Ragioniere.
	 */
	public static final Integer LIVELLO_RAGIONIERE = 3;
	
	// RGS

	/**
	 * Iter ragioniere.
	 */
	public static final Integer ITER_FIRMA_RAGIONIERE = 3;

	/**
	 * Iter ministro.
	 */
	public static final Integer ITER_FIRMA_MINISTRO = 8;

	/**
	 * 
	 */
	private static final long serialVersionUID = -6283875416263887172L;
	

	/**
	 * Iter approvativo.
	 */
	private Integer idIterApprovativo;

	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Info.
	 */
	private String info;

	/**
	 * Tipo firma.
	 */
	private Integer tipoFirma;

	/**
	 * Parziale.
	 */
	private Integer parziale;

	/**
	 * Identificativo wf.
	 */
	private Integer idWorkflow;

	/**
	 * Identificativo AOO.
	 */
	private Integer idAOO;

	/**
	 * WF name.
	 */
	private String workflowName;

	/**
	 * WF description.
	 */
	private String workflowDescription;

	/**
	 * Flag generico entrata.
	 */
	private Boolean flagGenericoEntrata;

	/**
	 * Flag generico uscita.
	 */
	private Boolean flagGenericoUscita;
	
	/**
	 * Costruttore.
	 */
	public IterApprovativoDTO() { }
	
	/**
	 * Costruttore.
	 * @param idIterApprovativo
	 * @param descrizione
	 * @param info
	 * @param tipoFirma
	 * @param parziale
	 * @param idWorkflow
	 * @param idAOO
	 * @param workflowName
	 * @param workflowDescription
	 * @param flagGenericoEntrata
	 * @param flagGenericoUscita
	 */
	public IterApprovativoDTO(final Integer idIterApprovativo, final String descrizione, final String info, final Integer tipoFirma, final Integer parziale,
			final Integer idWorkflow, final Integer idAOO, final String workflowName, final String workflowDescription, final Boolean flagGenericoEntrata, 
			final Boolean flagGenericoUscita) { 
		this.idIterApprovativo = idIterApprovativo;
		this.descrizione = descrizione;
		this.info = info;
		this.tipoFirma = tipoFirma;
		this.parziale = parziale;
		this.idWorkflow = idWorkflow;
		this.idAOO = idAOO;
		this.workflowName = workflowName;
		this.workflowDescription = workflowDescription;
		this.flagGenericoEntrata = flagGenericoEntrata;
		this.flagGenericoUscita = flagGenericoUscita;
	}
	
	/** GET & SET *****************************************************************************/

	public Integer getIdIterApprovativo() {
		return idIterApprovativo;
	}

	/**
	 * Recupera la descrizione.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Recupera le info.
	 * @return info
	 */
	public String getInfo() {
		return info;
	}

	/**
	 * Recupera il tipo di firma.
	 * @return tipo di firma
	 */
	public Integer getTipoFirma() {
		return tipoFirma;
	}

	/**
	 * Recupera il parziale.
	 * @return parziale
	 */
	public Integer getParziale() {
		return parziale;
	}

	/**
	 * Recupera l'id del workflow.
	 * @return id del workflow
	 */
	public Integer getIdWorkflow() {
		return idWorkflow;
	}

	/**
	 * Recupera l'id dell'Aoo.
	 * @return id dell'Aoo
	 */
	public Integer getIdAOO() {
		return idAOO;
	}

	/**
	 * Recupera il nome del workflow.
	 * @return nome del workflow
	 */
	public String getWorkflowName() {
		return workflowName;
	}

	/**
	 * Recupera la descrizione del workflow.
	 * @return descrizione del workflow
	 */
	public String getWorkflowDescription() {
		return workflowDescription;
	}

	/**
	 * Recupera il flag generico entrata.
	 * @return flag generico entrata
	 */
	public Boolean getFlagGenericoEntrata() {
		return flagGenericoEntrata;
	}

	/**
	 * Recupera il flag generico uscita.
	 * @return flag generico uscita
	 */
	public Boolean getFlagGenericoUscita() {
		return flagGenericoUscita;
	}
	
	/**
	 * Verifica se è iter automatico oltre firma dirigente.
	 * @return true o false
	 */
	public boolean isIterAutomaticoMoreThanDirigente() {
		return tipoFirma > 1;
	}
	
	/**
	 * Verifica se è iter automatico di firma dirigenti.
	 * @return true o false
	 */
	public boolean isIterAutomaticoFirmaDirigenti() {
		return tipoFirma == 1;
	}
	
	/**
	 * Verifica se è iter automatico di firma direttori ispettori.
	 * @return true o false
	 */
	public boolean isIterAutomaticoFirmaDirettoriIspettori() {
		return tipoFirma == 2;
	}
	
	/**
	 * Verifica se è iter automatico di firma ragioniere.
	 * @return true o false
	 */
	public boolean isIterAutomaticoFirmaRagioniere() {
		return tipoFirma == 3;
	}
	
	/**
	 * Verifica se è iter automatico di firma capo dipartimento.
	 * @return true o false
	 */
	public boolean isIterAutomaticoFirmaCapoDipartimento() {
		return tipoFirma == 4;
	}
	
	/**
	 * Verifica se è iter automatico RGS.
	 * 
	 * @param pp
	 * @return true o false
	 */
	public boolean isIterAutomaticoRGS(final PropertiesProvider pp) {
		return pp.getParameterByKey(PropertiesNameEnum.IDAOO_RGS) != null 
				&&  getIdAOO() == Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.IDAOO_RGS));
	}
	
	/**
	 * Verifica se è iter automatico.
	 * 
	 * @return true o false
	 */
	public boolean isIterAutomatico() {
		return idIterApprovativo != ITER_FIRMA_MANUALE.intValue();
	}

	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idIterApprovativo == null) ? 0 : idIterApprovativo.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final IterApprovativoDTO other = (IterApprovativoDTO) obj;
		if (idIterApprovativo == null) { 
			if (other.idIterApprovativo != null) {
				return false;
			}
		} else if (!idIterApprovativo.equals(other.idIterApprovativo)) {
			return false;
		}
		return true;
	}
	
}