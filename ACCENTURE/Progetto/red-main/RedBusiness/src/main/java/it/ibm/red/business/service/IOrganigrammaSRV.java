package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoStrutturaNodoEnum;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;

/**
 * Interfaccia del servizio di gestione organigramma.
 */
public interface IOrganigrammaSRV extends IOrganigrammaFacadeSRV {

	/**
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @param connection
	 * @return
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerCompetenzaUscita(UtenteDTO utente, NodoOrganigrammaDTO nodeToOpen, Connection connection);

	/**
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @param connection
	 * @return
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerCompetenzaIngresso(UtenteDTO utente, NodoOrganigrammaDTO nodeToOpen, Connection connection);
	
	/**
	 * 
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @param connection
	 * @return
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerFirma(Long idUfficioUtente, NodoOrganigrammaDTO nodeToOpen, Connection connection);
	
	/**
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @param connection
	 * @return
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerSigla(Long idUfficioUtente, NodoOrganigrammaDTO nodeToOpen, Connection connection);
	
	/**
	 * Ricerca nella lista allNodi tutti i nodi che hanno il tipoStruttura contenuto in enumArgs
	 * 
	 * Ritorna la lista ricercata oppure la sua differenza rispetto al totale in base al valore booleano inclusivo o meno
	 * 
	 * @param allNodi lista tra cui ricercare, la lista non viene modificata.
	 * @param inclusivo considera se restituire l'insieme inclusivo ricercato o l'insieme esclusivo 
	 * @param enumArgs lista dei tipiStruttura da ricercare
	 * @return Una lista di nodiOrganigramma filtrata secondo i criteri selezionati
	 */
	List<NodoOrganigrammaDTO> filterByTipoStruttura(List<NodoOrganigrammaDTO> allNodi, Boolean inclusivo, TipoStrutturaNodoEnum... enumArgs);




}
