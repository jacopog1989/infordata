package it.ibm.red.business.service.ws;

import it.ibm.red.business.service.ws.facade.ICreaDocumentoEntrataWsFacadeSRV;


/**
 * Servizio per la creazione di documenti in entrata tramite web service.
 * 
 * @author m.crescentini
 *
 */
public interface ICreaDocumentoEntrataWsSRV extends ICreaDocumentoEntrataWsFacadeSRV {

}