package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ISiebelDAO;
import it.ibm.red.business.dto.RdsSiebelDTO;
import it.ibm.red.business.dto.RdsSiebelDescrizioneDTO;
import it.ibm.red.business.dto.RdsSiebelGruppoDTO;
import it.ibm.red.business.dto.RdsSiebelTipologiaDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * The Class SiebelDAO.
 *
 * @author adilegge
 * 
 *         Dao per la gestione delle RdS Siebel
 */
@Repository
public class SiebelDAO extends AbstractDAO implements ISiebelDAO {

	private static final String CODICEGRUPPO = "CODICEGRUPPO";
	private static final String IDTIPOLOGIA = "IDTIPOLOGIA";
	/**
	 * The Constant serialVersionUID.
	 */

	private static final long serialVersionUID = 1L;
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SiebelDAO.class.getName());

	/**
	 * Messaggio errore recupero RDS per il documento.
	 */
	private static final String ERROR_RECUPERO_RDS_MSG = "Errore durante il recupero delle RDS per il documento ";

	/**
	 * @see it.ibm.red.business.dao.ISiebelDAO#getIdContattoSiebel(java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public String getIdContattoSiebel(final Integer idNodo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String idContattoSiebel = null;
		try {
			ps = con.prepareStatement("SELECT * FROM nodosiebel WHERE idnodo = ? AND abilitato = 1");
			ps.setInt(1, idNodo);
			rs = ps.executeQuery();
			while (rs.next()) {
				idContattoSiebel = rs.getString("IDCONTATTOSIEBEL");
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero dell'id contatto Siebel per l'ufficio " + idNodo, e);
			throw new RedException("Errore durante il recupero dell'id contatto Siebel per l'ufficio " + idNodo, e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
		return idContattoSiebel;
	}

	/**
	 * @see it.ibm.red.business.dao.ISiebelDAO#hasRdsAperte(java.lang.String,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public boolean hasRdsAperte(final String idDocumento, final Long idAOO, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			int count = 0;

			ps = con.prepareStatement("SELECT count(*) FROM rdssiebel WHERE iddocumento = ? AND idstatordssiebel = 0 AND idAOO = ? ");
			ps.setString(1, idDocumento);
			ps.setLong(2, idAOO);
			rs = ps.executeQuery();
			if (rs.next()) {
				count = rs.getInt(1);
			}

			return count != 0;

		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero del numero di RDS aperte per il documento " + idDocumento, e);
			throw new RedException("Errore durante il recupero del numero di RDS aperte per il documento " + idDocumento, e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISiebelDAO#getAllRdsDescrizione(java.lang.String,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<RdsSiebelDescrizioneDTO> getAllRdsDescrizione(final String idDocumento, final Long idAOO, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<RdsSiebelDescrizioneDTO> rdsSiebelList = null;

		try {
			final String sql = StringUtils.join("SELECT t.*, ",
					"u.COGNOME as COGNOMEUTENTE, u.NOME as NOMEUTENTE, n.DESCRIZIONE as DESCRIZIONENODO, tt.DESCRIZIONETIPOLOGIA as DESCRIZIONETIPOLOGIA, tg.DESCRIZIONEGRUPPO as DESCRIZIONEGRUPPO ",
					"FROM rdssiebel t ", "left join utente u on t.idUtenteAperturaRds = u.idUtente ", "left join nodo n on t.idNodoAperturaRds = n.idNodo ",
					"left join rdssiebel_tipologia tt on t.idTipologia = tt.idTipologia ", "left join rdssiebel_gruppo tg on t.codiceGruppo = tg.codiceGruppo ",
					"where t.iddocumento  =  ? AND t.idAOO = ? ");

			ps = con.prepareStatement(sql);
			ps.setString(1, idDocumento);
			ps.setLong(2, idAOO);
			rs = ps.executeQuery();

			rdsSiebelList = new ArrayList<>();
			while (rs.next()) {
				final RdsSiebelDescrizioneDTO rds = new RdsSiebelDescrizioneDTO();

				populateVO(rds, rs);

				final String cognome = rs.getString("COGNOMEUTENTE");
				rds.setCognomeUtenteAperturaRds(cognome);

				final String nome = rs.getString("NOMEUTENTE");
				rds.setNomeUtenteAperturaRds(nome);

				final String descrizoneNodo = rs.getString("DESCRIZIONENODO");
				rds.setNodoAperturaRds(descrizoneNodo);

				final String descrizoneTipologia = rs.getString("DESCRIZIONETIPOLOGIA");
				rds.setDescrizioneTipologia(descrizoneTipologia);

				final String descrizioneGruppo = rs.getString("DESCRIZIONEGRUPPO");
				rds.setDescrizioneGruppo(descrizioneGruppo);

				rdsSiebelList.add(rds);
			}

			return rdsSiebelList;
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_RDS_MSG + idDocumento, e);
			throw new RedException(ERROR_RECUPERO_RDS_MSG + idDocumento, e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISiebelDAO#getAllRds(java.lang.String,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<RdsSiebelDTO> getAllRds(final String idDocumento, final Long idAOO, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<RdsSiebelDTO> rdsSiebelList = null;

		try {

			ps = con.prepareStatement("SELECT * FROM rdssiebel WHERE iddocumento = ? AND idAOO = ? ");
			ps.setString(1, idDocumento);
			ps.setLong(2, idAOO);
			rs = ps.executeQuery();

			rdsSiebelList = fetchMultiResult(rs);

			return rdsSiebelList;

		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_RDS_MSG + idDocumento, e);
			throw new RedException(ERROR_RECUPERO_RDS_MSG + idDocumento, e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	private static ArrayList<RdsSiebelDTO> fetchMultiResult(final ResultSet rs) throws SQLException {
		final ArrayList<RdsSiebelDTO> resultList = new ArrayList<>();
		while (rs.next()) {
			final RdsSiebelDTO rdsSiebel = new RdsSiebelDTO();
			populateVO(rdsSiebel, rs);
			resultList.add(rdsSiebel);
		}
		return resultList;
	}

	private static void populateVO(final RdsSiebelDTO rdsSiebel, final ResultSet rs) throws SQLException {

		rdsSiebel.setDataAperturaRds(new Date(rs.getTimestamp("DATAAPERTURARDS").getTime()));

		if (rs.getTimestamp("DATACHIUSURARDS") != null) {
			rdsSiebel.setDataChiusuraRds(new Date(rs.getTimestamp("DATACHIUSURARDS").getTime()));
		}

		if (rs.getTimestamp("DATAINVIOSIEBEL") != null) {
			rdsSiebel.setDataInvioSiebel(new Date(rs.getTimestamp("DATAINVIOSIEBEL").getTime()));
		}

		rdsSiebel.setDescrizioneRds(rs.getString("DESCRIZIONERDS"));
		rdsSiebel.setElencoFileInviati(rs.getString("ELENCOFILEINVIATI"));
		rdsSiebel.setElencoFileRicevuti(rs.getString("ELENCOFILERICEVUTI"));
		rdsSiebel.setEmailAlternativa(rs.getString("EMAILALTERNATIVA"));
		rdsSiebel.setIdDocumento(rs.getInt("IDDOCUMENTO"));
		rdsSiebel.setIdAoo(rs.getInt("IDAOO"));
		rdsSiebel.setIdNodoAperturaRds(rs.getInt("IDNODOAPERTURARDS"));
		rdsSiebel.setIdRdsSiebel(rs.getString("IDRDSSIEBEL"));
		rdsSiebel.setIdStatoRdsSiebel(rs.getInt("IDSTATORDSSIEBEL"));
		rdsSiebel.setIdUtenteAperturaRds(rs.getInt("IDUTENTEAPERTURARDS"));
		rdsSiebel.setTitoloRds(rs.getString("TITOLORDS"));
		rdsSiebel.setSoluzione(rs.getString("SOLUZIONE"));
		rdsSiebel.setIdTipologia(rs.getInt(IDTIPOLOGIA));
		rdsSiebel.setCodiceGruppo(rs.getString(CODICEGRUPPO));
	}

	/**
	 * @see it.ibm.red.business.dao.ISiebelDAO#inserisciRdS(it.ibm.red.business.dto.RdsSiebelDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public void inserisciRdS(final RdsSiebelDTO rds, final Connection con) {
		int index = 1;
		PreparedStatement ps = null;
		try {
			final StringBuilder query = new StringBuilder();
			query.append("INSERT INTO rdssiebel ( ");
			query.append(" iddocumento, ");
			query.append(" idaoo, ");
			query.append(" idrdssiebel, ");
			query.append(" idstatordssiebel, ");
			query.append(" dataaperturards, ");
			query.append(" idutenteaperturards, ");
			query.append(" idnodoaperturards, ");
			query.append(" titolords, ");
			query.append(" descrizionerds, ");
			query.append(" emailalternativa, ");
			query.append(" elencofileinviati, ");
			query.append(" idtipologia, ");
			query.append(" codicegruppo ");
			query.append(" ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");

			ps = con.prepareStatement(query.toString());
			ps.setInt(index++, rds.getIdDocumento());
			ps.setInt(index++, rds.getIdAoo());
			ps.setString(index++, rds.getIdRdsSiebel());
			ps.setInt(index++, rds.getIdStatoRdsSiebel());
			ps.setTimestamp(index++, new Timestamp(rds.getDataAperturaRds().getTime()));
			ps.setInt(index++, rds.getIdUtenteAperturaRds());
			ps.setInt(index++, rds.getIdNodoAperturaRds());
			ps.setString(index++, rds.getTitoloRds());
			ps.setString(index++, rds.getDescrizioneRds());

			if (rds.getEmailAlternativa() != null) {
				ps.setString(index++, rds.getEmailAlternativa());
			} else {
				ps.setNull(index++, Types.CHAR);
			}

			if (rds.getElencoFileInviati() != null) {
				ps.setString(index++, rds.getElencoFileInviati());
			} else {
				ps.setNull(index++, Types.CHAR);
			}
			ps.setInt(index++, rds.getIdTipologia());
			ps.setString(index++, rds.getCodiceGruppo());

			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la registrazione della RdS", e);
			throw new RedException("Errore durante la registrazione della RdS", e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISiebelDAO#getRds(java.lang.String, java.sql.Connection).
	 */
	@Override
	public RdsSiebelDTO getRds(final String idRdsSiebel, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		RdsSiebelDTO rdsSiebel = null;

		try {

			ps = con.prepareStatement("SELECT * FROM rdssiebel WHERE idrdssiebel = ?");
			ps.setString(1, idRdsSiebel);
			rs = ps.executeQuery();

			if (rs.next()) {
				rdsSiebel = new RdsSiebelDTO();
				populateVO(rdsSiebel, rs);
			}

			return rdsSiebel;

		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero della RDS " + idRdsSiebel, e);
			throw new RedException("Errore durante il recupero della RDS " + idRdsSiebel, e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISiebelDAO#aggiornaRds(it.ibm.red.business.dto.RdsSiebelDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public void aggiornaRds(final RdsSiebelDTO rds, final Connection connection) {
		int index = 1;
		PreparedStatement ps = null;
		try {
			final StringBuilder query = new StringBuilder();
			query.append("UPDATE rdssiebel ");
			query.append("SET idstatordssiebel = ?, datachiusurards = ?, datainviosiebel = ?, elencofilericevuti = ?, soluzione = ? ");
			query.append("WHERE idrdssiebel = ? ");

			ps = connection.prepareStatement(query.toString());
			ps.setInt(index++, rds.getIdStatoRdsSiebel());
			ps.setTimestamp(index++, new Timestamp(rds.getDataChiusuraRds().getTime()));
			ps.setTimestamp(index++, new Timestamp(rds.getDataInvioSiebel().getTime()));

			if (rds.getElencoFileRicevuti() != null) {
				ps.setString(index++, rds.getElencoFileRicevuti());
			} else {
				ps.setNull(index++, Types.CHAR);
			}

			if (rds.getSoluzione() != null) {
				ps.setString(index++, rds.getSoluzione());
			} else {
				ps.setNull(index++, Types.CHAR);
			}

			ps.setString(index++, rds.getIdRdsSiebel());
			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento della RdS", e);
			throw new RedException("Errore durante l'aggiornamento della RdS", e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISiebelDAO#hasRds(java.util.Collection,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public Map<String, Boolean> hasRds(final Collection<String> ids, final Long idAOO, final Connection con) {
		Statement st = null;
		ResultSet rs = null;
		final Map<String, Boolean> retValMap = new HashMap<>();

		try {
			final List<String> idList = ids.stream().collect(Collectors.toList());
			idList.forEach(id -> retValMap.put(id, false));

			final String inclause = it.ibm.red.business.utils.StringUtils.createInCondition("idDocumento", idList.iterator(), false);

			String sql = StringUtils.join("select distinct iddocumento from rdssiebel where ", inclause);
			sql += " AND IDAOO = " + idAOO;

			st = con.createStatement();
			rs = st.executeQuery(sql);

			while (rs.next()) {
				retValMap.put("" + rs.getInt("iddocumento"), Boolean.TRUE);
			}

			return retValMap;

		} catch (final SQLException e) {
			LOGGER.error("Errore durante la verifica della presenza di RDS ", e);
			throw new RedException("Errore durante la verifica della presenza di RDS ", e);
		} finally {
			closeStatement(st);
			closeResultset(rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISiebelDAO#getMappaTipiGruppiSiebel(java.sql.Connection).
	 */
	@Override
	public Map<RdsSiebelTipologiaDTO, List<RdsSiebelGruppoDTO>> getMappaTipiGruppiSiebel(final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final Map<RdsSiebelTipologiaDTO, List<RdsSiebelGruppoDTO>> mappaTipiGruppiSiebel = new HashMap<>();
		try {
			final String sql = StringUtils.join("SELECT * ", "FROM rdssiebel_tipologia t, rdssiebel_gruppo g ",
					"WHERE t.idtipologia = g.idtipologia(+) AND g.flagattivo(+) = 1 ", "ORDER BY t.descrizionetipologia, g.descrizionegruppo ");
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			RdsSiebelTipologiaDTO tipologia = null;
			List<RdsSiebelGruppoDTO> gruppi = null;
			RdsSiebelGruppoDTO gruppo = null;
			while (rs.next()) {
				gruppo = null;
				if (rs.getString(CODICEGRUPPO) != null) {
					gruppo = new RdsSiebelGruppoDTO();
					gruppo.setCodiceGruppo(rs.getString(CODICEGRUPPO));
					gruppo.setDescrizioneGruppo(rs.getString("DESCRIZIONEGRUPPO"));
				}
				if (tipologia == null || rs.getInt(IDTIPOLOGIA) != tipologia.getIdTipologia().intValue()) {
					tipologia = new RdsSiebelTipologiaDTO();
					tipologia.setIdTipologia(rs.getInt(IDTIPOLOGIA));
					tipologia.setDescrizioneTipologia(rs.getString("DESCRIZIONETIPOLOGIA"));
					gruppi = new ArrayList<>();
					mappaTipiGruppiSiebel.put(tipologia, gruppi);
				}
				if (gruppo != null) {
					gruppi.add(gruppo);
				}
			}
			return mappaTipiGruppiSiebel;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero delle RDS", e);
			throw new RedException("Errore durante il recupero delle RDS", e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

}