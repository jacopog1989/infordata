package it.ibm.red.business.dto;

/**
 * Classe NotificaRitiroAutotutelaDTO.
 *
 * @author APerquoti
 */
public class NotificaRitiroAutotutelaDTO extends AbstractDTO {

	/** 
	 * The Constant serialVersionUID. 
	 */
	private static final long serialVersionUID = -2679439706272444765L;
	
	/**
	 * Identificativo documento
	 */
	private long idDocumento;
	
	/**
	 * Identificativo nodo
	 */
	private long idNodo;
	
	/**
	 * Identificativo utente
	 */
	private long idUtente;
	
	/**
	 * Costruttore notifica ritiro autotutela DTO.
	 *
	 * @param inIdDocumento the in id documento
	 * @param inIdNodo the in id nodo
	 * @param inIdUtente the in id utente
	 */
	public NotificaRitiroAutotutelaDTO(final long inIdDocumento, final long inIdNodo, final long inIdUtente) {
		this.idDocumento = inIdDocumento;
		this.idNodo = inIdNodo;
		this.idUtente = inIdUtente;
	}

	/**
	 * Costruttore notifica ritiro autotutela DTO.
	 */
	public NotificaRitiroAutotutelaDTO() {
		super();
	}

	/** 
	 *
	 * @return the idDocumento
	 */
	public long getIdDocumento() {
		return idDocumento;
	}

	/** 
	 *
	 * @return the idNodo
	 */
	public long getIdNodo() {
		return idNodo;
	}

	/** 
	 *
	 * @return the idUtente
	 */
	public long getIdUtente() {
		return idUtente;
	}

}
