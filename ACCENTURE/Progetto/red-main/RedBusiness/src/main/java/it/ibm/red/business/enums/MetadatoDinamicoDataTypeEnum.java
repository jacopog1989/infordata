package it.ibm.red.business.enums;

/**
 * Enum metadati dinamici.
 */
public enum MetadatoDinamicoDataTypeEnum {
	
	/**
	 * Valore.
	 */
	SELECT,
	
	/**
	 * Valore.
	 */
	CHECKBOX,
	
	/**
	 * Valore.
	 */
	
	/**
	 * Valore.
	 */
	DATE,
	
	/**
	 * Valore.
	 */
	INPUT_TEXT;
}