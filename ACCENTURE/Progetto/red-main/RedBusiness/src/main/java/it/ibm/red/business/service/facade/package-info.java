/**
 * @author CPIERASC
 *
 *	Questo package contiene le facade dei servizi: tramite queste interfacce il client può accedere esclusivamente ai
 *	servizi a grana grossa e quindi vede una versione semplificata delle api.
 *	
 */
package it.ibm.red.business.service.facade;
