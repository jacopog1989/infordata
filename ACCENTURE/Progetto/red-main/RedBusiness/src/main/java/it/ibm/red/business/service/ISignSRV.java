package it.ibm.red.business.service;

import java.io.InputStream;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.filenet.api.core.Document;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.DetailDocumentoDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.PEDocumentoDTO;
import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.VerifyInfoDTO;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.enums.SignTypeEnum.SignTypeGroupEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.service.asign.step.ContextASignEnum;
import it.ibm.red.business.service.concrete.SignSRV.VerificaFirmaItem;
import it.ibm.red.business.service.facade.ISignFacadeSRV;

/**
 * Interfaccia per l'implementazione dei servizi di firma.
 * 
 * @author CPAOLUZI
 *
 */
public interface ISignSRV extends ISignFacadeSRV {
	
	/**
	 * Ritorna il tipoSpedizione a partire dai destinatari e dal workflow.
	 * 
	 * @param wobDTO
	 * @param documentDetail
	 * @return
	 */
	int getTipoSpedizione(PEDocumentoDTO wobDTO, DetailDocumentoDTO documentDetail);
	
	/**
	 * Verifica firma e aggiorna il metadato verificaFirma su CE.
	 * 
	 * @param doc			Documento FileNet da verificare
	 * @param docContent 	Content da verificare
	 * @param fceh			Helper per le operazioni sul CE di FileNet
	 * @param utente		utente
	 * @return 				Il DTO contenente le informazioni di firma
	 */
	VerifyInfoDTO aggiornaVerificaFirma(Document doc, InputStream docContent, IFilenetCEHelper fceh, UtenteDTO utente);

	/**
	 * Gestisce gli allacci.
	 * @param fceh
	 * @param fpeh
	 * @param aoo
	 * @param documento
	 * @param utente
	 * @param aggiornaNps
	 * @param connection
	 */
	void gestisciAllacci(IFilenetCEHelper fceh, FilenetPEHelper fpeh, Aoo aoo, Document documento, UtenteDTO utente, boolean allineaNPS, Connection connection);
	
	/**
	 * Controlla che il documento appartenga ai flussi autorizzativi UCB.
	 * 
	 * @param idDocUscita
	 * @param allaccioPrincipale
	 * @param utente
	 * @param fceh
	 * @param connection
	 * @return true o false
	 */
	boolean checkIsFlussiAutUCB(String idDocUscita, RispostaAllaccioDTO allaccioPrincipale, UtenteDTO utente, IFilenetCEHelper fceh, Connection connection);
	
	/**
	 * Controlla che il documento appartenga al flusso atto decreto UCB.
	 * @param utente
	 * @param idTipologiaDocumento
	 * @return true o false
	 */
	boolean checkIsFlussoAttoDecretoUCB(UtenteDTO utente, Long idTipologiaDocumento);

	/**
	 * Recupera il protocollo da NPS.
	 * @param wob
	 * @param documentDetailDTO
	 * @param utente
	 * @param fceh
	 * @param connection
	 * @param isRiservato
	 * @return protocollo NPS
	 */
	ProtocolloNpsDTO getNewProtocolloNps(VWWorkObject wob, DetailDocumentoDTO documentDetailDTO, UtenteDTO utente,
			IFilenetCEHelper fceh, Connection connection, Boolean isRiservato);

	/**
	 * Controlla se è un registro di repertorio.
	 * @param utente
	 * @param documentDetailDTO
	 * @param connection
	 */
	void checkIsRegistroRepertorio(UtenteDTO utente, DetailDocumentoDTO documentDetailDTO, Connection connection);
	
	/**
	 * Ottiene il content da verificare.
	 * @param aoo
	 * @return lista delle verifiche della firma
	 */
	List<VerificaFirmaItem> recuperaContentDaVerificareNew(Aoo aoo);
  	
  	/**
	 * Aggiorna lo stato del dococument title verificato.
	 * @param idDocElaborati
	 * @param stato
	 * @return true o false
	 */
	boolean updateStatoDocumentTitleVerificato(List<Integer> idDocElaborati, Integer stato);
	
	/**
	 * Effettua la protocollazione del documento.
	 * 
	 * @param signTransactionId
	 * @param wob
	 * @param utente
	 * @param connection
	 * @return
	 */
	EsitoOperazioneDTO protocollazione(String signTransactionId, VWWorkObject wob, UtenteDTO utente, Connection connection);

	/**
	 * Gestisce la prima transazione per la registrazione ausiliaria: comunicazione con NPS e ottenimento numero protocollo reg. aux.
	 * 
	 * @param signTransactionId
	 * @param wob
	 * @param utente
	 * @param connection
	 * */
	EsitoOperazioneDTO protocollaRegAux(String signTransactionId, VWWorkObject wob, UtenteDTO utente, Connection connection);
	
	/**
	 * Gestisce la seconda transazione per la registrazione ausiliaria: rigenerazione content
	 *
	 * @param signTransactionId
	 * @param wob
	 * @param utente
	 * @param connection
	 * */
	EsitoOperazioneDTO rigeneraContentRegAux(String signTransactionId, VWWorkObject wob, UtenteDTO utente, Connection connection);
	
	/**
	 * Stampiglia sul documento protocollo, approvazioni e postilla e inserisce il content stampigliato su FileNet a valle della firma.
	 * 
	 * @param wob - Oggetto Filenet relativo al WF del documento da firmare.
	 * @param utente - Utente che esegue l'operazione di firma.
	 * @param ste - Tipo di firma (CAdES/PAdES/Non digitale)
	 * @param context - Contesto del processo di firma asincrona.
	 * @param connection - Connessione al DB.
	 *
	 * @return Esito della stampigliatura.
	 */
	EsitoOperazioneDTO stampigliatura(VWWorkObject wob, UtenteDTO utente, SignTypeGroupEnum ste, Map<ContextASignEnum, Object> context, Connection connection);

	/**
	 * Aggiorna i metadati del documento, le security per documento e fascicoli e dei fascicoli a valle della firma.
	 * 
	 * @param wob
	 * @param utente
	 * @param connection
	 * @return
	 */
	EsitoOperazioneDTO aggiornamentoMetadati(VWWorkObject wob, UtenteDTO utente, Connection connection);

	/**
	 * Gestisce gli allacci a valle della firma.
	 * 
	 * @param wob
	 * @param utente
	 * @param connection
	 * @return
	 */
	EsitoOperazioneDTO gestioneAllacci(VWWorkObject wob, UtenteDTO utente, Connection connection);

	/**
	 * Gestisce l'invio della mail a seguito della firma di un documento principale.
	 * 
	 * @param utente - Utente che sta eseguente l'operazione di firma.
	 * @param wobDTO - DTO relativo al WF su cui si sta eseguendo la firma.
	 * @param documentDetail - DTO relativo al documento su cui sta eseguendo la firma.
	 * @param protocollo - Protocollo relativo al documento principale su cui sta eseguendo la firma.
	 * @param destinatariCollection - Destinatari del documento principale.
	 * @param fpeh - Helper per la connessione al PE.
	 * @param connection - Connessione al DB.
	 * 
	 * @return Esito della procedura di invio della mail.
	 */
	EsitoOperazioneDTO invioMail(final UtenteDTO utente, final PEDocumentoDTO wobDTO, final DetailDocumentoDTO documentDetail, final ProtocolloDTO protocollo, 
			final Collection<?> destinatariCollection, final Connection connection, final boolean firmaDigitale, final boolean simulaInvioNPS, final boolean chiudiFascicolo);

	/**
	 * @param utente
	 * @param aSignItemDTO
	 * @param connection
	 * @return
	 */
	EsitoOperazioneDTO invioMail(UtenteDTO utente, ASignItemDTO aSignItemDTO, Connection connection);
	
	/**
	 * Esegue l'avanzamento del workflow del documento firmato e l'eventale creazione del documento di categoria "INTERNO" per i destinatari interni.
	 * 
	 * @param wob
	 * @param utente
	 * @param firmaAutografa
	 * @param context
	 * @param fpeh
	 * @param connection
	 * @return
	 */
	EsitoOperazioneDTO avanzamentoProcessi(VWWorkObject wob, UtenteDTO utente, boolean firmaAutografa, Map<ContextASignEnum, Object> context, FilenetPEHelper fpeh, 
			Connection connection);

	/**
	 * Esegue l'allineamento di NPS in relazione alla firma appena effettuata.
	 * Ciò avviene in modalità asincrona, scrivendo su un'apposita coda tutte le chiamate da effettuare successivamente verso NPS.
	 * 
	 * @param aSignItem
	 * @param utente
	 * @param connection
	 * @return
	 */
	EsitoOperazioneDTO allineamentoNPS(ASignItemDTO aSignItem, UtenteDTO utente, Connection connection);

	/**
	 * @param signTransactionId
	 * @param wobNumber
	 * @param utente
	 * @param signType
	 * @param fceh
	 * @param connection
	 */
	void updateDocumentsVersionFromSignedDocsBuffer(String signTransactionId, String wobNumber, UtenteDTO utente, SignTypeEnum signType, IFilenetCEHelper fceh, Connection connection);

	/**
	 * @param signTransactionId
	 * @param esito
	 * @param workflow
	 * @param documentForDetails
	 * @param documentTitle
	 * @param utente
	 * @param signType
	 * @param firmaAutografa
	 * @param fceh
	 * @param connection
	 * @param conBuffer
	 */
	void gestisciFlussiPerNPS(String signTransactionId, EsitoOperazioneDTO esito, VWWorkObject workflow, String documentTitle, UtenteDTO utente, SignTypeEnum signType, boolean firmaAutografa, 
			IFilenetCEHelper fceh, Connection connection, Connection conBuffer);
	
}
