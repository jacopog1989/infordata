package it.ibm.red.business.dao.impl;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IRedWsTrackDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.ServerUtils;

/**
 * The Class RedWsTrackDAO.
 *
 * @author a.dilegge
 * 
 * 	DAO per il log delle invocazioni a RedEvoWS.
 */
@Repository
public class RedWsTrackDAO extends AbstractDAO implements IRedWsTrackDAO {
	
	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = -4098659861630393783L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RedWsTrackDAO.class.getName());
	
	/**
	 * Query.
	 */
	private static final String SQL_INSERT = "INSERT INTO REDWS_TRACK (IDTRACK, IPREQUESTTRACK, SERVERNAMETRACK, SERVICENAMETRACK, REQUESTTRACK, DATAREQUESTTRACK) VALUES (?, ?, ?, ?, ?, sysdate)";
	
	/**
	 * Query.
	 */
	private static final String SQL_UPDATE = "UPDATE REDWS_TRACK SET RESPONSETRACK = ?, ESITOTRACK = ?, DATARESPONSETRACK = sysdate WHERE IDTRACK = ?";
	
	/**
	 * @see it.ibm.red.business.dao.IRedWsTrackDAO#insert(java.lang.String,
	 *      java.lang.String, java.lang.String, java.sql.Connection).
	 */
	@Override
	public long insert(final String request, final String serviceName, final String remoteAddress, final Connection con) {
		LOGGER.info("Execute query: " + SQL_INSERT);
		int index = 1;
		PreparedStatement ps = null;
		long idTrack = 0L;
		
		try {
			idTrack = getNextId(con, "SEQ_REDWS_TRACK");
			
			ps = con.prepareStatement(SQL_INSERT);
			ps.setLong(index++, idTrack);
			ps.setString(index++, remoteAddress);
			ps.setString(index++, ServerUtils.getServerFullName());
			ps.setString(index++, serviceName);
			final Blob blob = con.createBlob();
			blob.setBytes(1, request.getBytes(StandardCharsets.UTF_8.name()));
			ps.setBlob(index++, blob);
			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la registrazione ", e);
			throw new RedException("Errore durante la registrazione ", e);
		} catch (final UnsupportedEncodingException e) {
			LOGGER.error("Errore durante la creazione del blob nel corso della registrazione ", e);
			throw new RedException("Errore durante la creazione del blob nel corso della registrazione ", e);
		} finally {
			closeStatement(ps);
		}
		
		return idTrack;
	}

	/**
	 * @see it.ibm.red.business.dao.IRedWsTrackDAO#update(java.lang.Long,
	 *      java.lang.Integer, java.lang.String, java.sql.Connection).
	 */
	@Override
	public void update(final Long idTrack, final Integer esitoOperazione, final String response, final Connection con) {
		LOGGER.info("Execute query: " + SQL_UPDATE);
		int index = 1;
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(SQL_UPDATE);
			final Blob blob = con.createBlob();
			blob.setBytes(1, response.getBytes(StandardCharsets.UTF_8));
			ps.setBlob(index++, blob);
			ps.setInt(index++, esitoOperazione);
			ps.setLong(index++, idTrack);
			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento ", e);
			throw new RedException("Errore durante l'aggiornamento ", e);
		} finally {
			closeStatement(ps);
		}
	}
	
}