package it.ibm.red.business.enums;

/**
 * Tipologia dei destintari Dati Cert.
 */
public enum TipoDestinatarioDatiCertEnum {
	
	/**
	 * Valore.
	 */
	ESTERNO,
	
	/**
	 * Valore.
	 */
	CERTIFICATO;
	
	/**
	 * Restituisce il valore dell'enum corrispettivo al nome in input.
	 * @param nomeTipo
	 * @return tipoDestinatarioDatiCertEnum
	 */
	public static TipoDestinatarioDatiCertEnum get(final String nomeTipo) {
		TipoDestinatarioDatiCertEnum output = null;
		
		for (TipoDestinatarioDatiCertEnum tipo : TipoDestinatarioDatiCertEnum.values()) {
			if (tipo.name().equalsIgnoreCase(nomeTipo)) {
				output = tipo;
				break;
			}
		}
		return output;
	}
}
