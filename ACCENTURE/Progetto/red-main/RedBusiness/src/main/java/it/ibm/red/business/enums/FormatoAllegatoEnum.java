package it.ibm.red.business.enums;

/**
 * The Enum FormatoAllegatoEnum.
 *
 * @author CPIERASC
 * 
 *         Enumerato utilizzato per definire il formato di un allegato.
 */
public enum FormatoAllegatoEnum {
	
	/**
	 * Cartaceo.
	 */
	CARTACEO(1, "Cartaceo"),
	/**
	 * Elettronico.
	 */
	ELETTRONICO(2, "Elettronico"),
	/**
	 * Non specificato.
	 */
	NON_SPECIFICATO(3, "Non specificato"),
	/**
	 * Firmato digitalmente.
	 */
	FIRMATO_DIGITALMENTE(4, "Firmato digitalmente"),
	/**
	 * Formato originale.
	 */
	FORMATO_ORIGINALE(5, "Formato originale");

	/**
	 * Identificativo formato.
	 */
	private Integer id;
	/**
	 * Descrizione formato.
	 */
	private String descrizione;
	
	/**
	 * Costruttore.
	 * 
	 * @param inId			identificativo
	 * @param inDescrizione	descrizione
	 */
	FormatoAllegatoEnum(final Integer inId, final String inDescrizione) {
		this.id = inId;
		this.descrizione = inDescrizione;
	}
	
	/**
	 * @param id
	 * @return
	 */
	public static FormatoAllegatoEnum getById(final Integer id) {
		FormatoAllegatoEnum f = null;
		for (FormatoAllegatoEnum item : FormatoAllegatoEnum.values()) {
			if (item.getId().equals(id)) {
				f = item;
				break;
			}
		}
		return f;
	}

	/**
	 * Getter descrizione.
	 * 
	 * @return	descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Getter identificativo.
	 * 
	 * @return	identificativo
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Getter identificativo in formato String.
	 * 
	 * @return	identificativo
	 */
	public String getIdString() {
		return "" + id;
	}

}
