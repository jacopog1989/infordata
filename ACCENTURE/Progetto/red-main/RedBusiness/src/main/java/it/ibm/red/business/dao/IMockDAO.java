package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaNPSDTO;
import it.ibm.red.business.enums.TipoProtocolloEnum;

/**
 * 
 * @author a.dilegge
 *
 *	Dao mock.
 */
public interface IMockDAO extends Serializable {
	
	/**
	 * Ottiene la sequence la protocollazione mock.
	 * @param codiceAoo
	 * @param tipoProtocollo
	 * @param con
	 * @return result
	 */
	Integer getNextProt(String codiceAoo, TipoProtocolloEnum tipoProtocollo, Connection con);
	
	/**
	 * Inserisce il protocollo mock.
	 * @param protocollo
	 * @param codiceAoo
	 * @param tipoProtocollo
	 * @param con
	 */
	void insertProt(ProtocolloDTO protocollo, String codiceAoo, TipoProtocolloEnum tipoProtocollo, Connection con);

	/**
	 * Ottiene il protocollo mock.
	 * @param idProtocollo
	 * @param connection
	 * @return protocollo mock
	 */
	ProtocolloDTO getProt(String idProtocollo, Connection connection);

	/**
	 * Ottiene il documento mock.
	 * @param tipoDocumento
	 * @param serverName
	 * @param connection
	 * @return file
	 */
	FileDTO getDocumento(String tipoDocumento, String serverName, Connection connection);

	/**
	 * Ottiene i protocolli mock.
	 * @param codiceAoo
	 * @param numeroProtocolloDa
	 * @param numeroProtocolloA
	 * @param maxResults
	 * @param connection
	 * @return lista di protocolli mock
	 */
	List<ProtocolloDTO> getProtocolli(String codiceAoo, Integer numeroProtocolloDa, Integer numeroProtocolloA, int maxResults, Connection connection);

	/**
	 * Ottiene la sequence la registrazione mock.
	 * @param codiceAoo
	 * @param connection
	 * @return result
	 */
	Integer getNextRegistrazione(String codiceAoo, Connection connection);

	/**
	 * Inserisce il protocollo mock.
	 * @param registrazione
	 * @param codiceAoo
	 * @param idRegistro
	 * @param connection
	 */
	void insertRegistrazione(RegistrazioneAusiliariaNPSDTO registrazione, String codiceAoo, Integer idRegistro, Connection connection);
	
}
