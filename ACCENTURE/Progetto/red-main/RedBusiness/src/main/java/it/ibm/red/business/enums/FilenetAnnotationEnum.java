package it.ibm.red.business.enums;

/**
 * The Enum FilenetAnnotationEnum.
 *
 * @author mcrescentini
 * 
 *         Enum delle annotation filenet.
 */
public enum FilenetAnnotationEnum {
	

	/**
	 * Valore.
	 */
	CURRENT_VERSION("CURVERSION"),
	

	/**
	 * Valore.
	 */
	TEXT_MAIL("MAIL"),
	

	/**
	 * Valore.
	 */
	TEXT_MAIL_MESSAGE_ID("MAILMESSAGEID"),
	

	/**
	 * Valore.
	 */
	TEXT_PREVIEW("PREVIEW"),
	
	/**
	 * Content libro firma.
	 */
	CONTENT_LIBRO_FIRMA("CONTENT_LIBRO_FIRMA")
	
	;
	
	

	/**
	 * Nome.
	 */
	private final String nome;
	
	/**
	 * Costruttore dell'enum.
	 * @param nome
	 */
	FilenetAnnotationEnum(final String nome) {
		this.nome = nome;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
}