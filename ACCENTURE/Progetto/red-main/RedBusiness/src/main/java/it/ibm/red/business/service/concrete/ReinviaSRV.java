package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dao.impl.IterApprovativoDAO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.provider.ServiceTaskProvider.ServiceTask;
import it.ibm.red.business.service.IReinviaSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * Service che gestisce la funzionalità di reinvio.
 */
@Service
public class ReinviaSRV extends AbstractService implements IReinviaSRV {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ReinviaSRV.class.getName());

	/**
	 * Servizio.
	 */
	@Autowired
	private UpdateContentSRV updateContentSRV;

	/**
	 * Dao.
	 */
	@Autowired
	private IterApprovativoDAO iaDao;

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1764077735858752481L;

	/**
	 * @see it.ibm.red.business.service.facade.IReinviaFacadeSRV#reinvia(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "reinvia")
	public final Collection<EsitoOperazioneDTO> reinvia(final UtenteDTO utente, final Collection<String> wobNumbers) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		for (final String wobNumber : wobNumbers) {
			esiti.add(reinvia(utente, wobNumber));
		}
		return esiti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IReinviaFacadeSRV#reinvia(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "reinvia")
	public EsitoOperazioneDTO reinvia(final UtenteDTO utente, final String wobNumber) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		Integer idDocumento = null;
		boolean trasformato = false;
		Integer numeroDoc = 0;
		String[] firmatari;
		Connection connection = null;
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);

			idDocumento = (Integer) TrasformerPE.getMetadato(wob, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
			firmatari = (String[]) TrasformerPE.getMetadato(wob, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ELENCO_LIBROFIRMA_METAKEY));
			if (firmatari == null || firmatari.length == 0 || StringUtils.isNullOrEmpty(firmatari[0])) {
				firmatari = new String[1];
				firmatari[0] = TrasformerPE.getMetadato(wob, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY)) + ","
						+ TrasformerPE.getMetadato(wob, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY));
			}

			Boolean flagModificaMontanaro = false;
			final Object flagModificaMontanaroObj = TrasformerPE.getMetadato(wob,
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MODIFICA_MONTANARO_METAKEY));
			if (flagModificaMontanaroObj instanceof String) {
				flagModificaMontanaro = Boolean.parseBoolean((String) flagModificaMontanaroObj);
			} else if (flagModificaMontanaroObj != null) {
				flagModificaMontanaro = (Boolean) flagModificaMontanaroObj;
			}
			final Integer idTipoAssegnazione = (Integer) TrasformerPE.getMetadato(wob,
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY));

			final List<String> selectList = new ArrayList<>();
			selectList.add(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY));
			final Document doc = fceh.getDocumentByIdGestionale(idDocumento.toString(), selectList, null, utente.getIdAoo().intValue(), null, null);
			numeroDoc = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);

			final Integer tipoFirma = iaDao.getTipoFirma(utente.getIdAoo().intValue(), wob.getWorkflowName(), connection);

			trasformato = updateVersione(utente, idDocumento, firmatari, flagModificaMontanaro, idTipoAssegnazione, tipoFirma);

			if (!fpeh.nextStep(wob, null, ResponsesRedEnum.REINVIA.getResponse())) {
				throw new RedException("Errore nell'avanzamento del workflow.");
			}

			esito.setEsito(true);
			esito.setIdDocumento(numeroDoc);
			esito.setNote("Reinvio effettuato con successo.");
		} catch (final Exception e) {
			LOGGER.error("Errore nel reinvio del documento.", e);
			if (trasformato) {
				fceh.updateMetadataAndDeleteLastVersion(fceh.getDocumentByIdGestionale(idDocumento.toString(), null, null, utente.getIdAoo().intValue(), null, null), null);
			}
			esito.setIdDocumento(numeroDoc);
			esito.setNote("Si è verificato un errore durante reinvio del documento.");
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}

		return esito;
	}

	/**
	 * @param utente
	 * @param idDocumento
	 * @param firmatari
	 * @param flagModificaMontanaro
	 * @param idTipoAssegnazione
	 * @param tipoFirma
	 * @return true se trasfomrato
	 */
	private boolean updateVersione(final UtenteDTO utente, final Integer idDocumento, final String[] firmatari, final Boolean flagModificaMontanaro,
			final Integer idTipoAssegnazione, final Integer tipoFirma) {
		boolean trasformato = false;

		try {
			updateContentSRV.addPDFVersion(utente, idDocumento.toString(), firmatari, flagModificaMontanaro, tipoFirma, idTipoAssegnazione);
			trasformato = true;
		} catch (final Exception e) {
			throw new RedException("Errore nella trasformazione PDF", e);
		}
		return trasformato;
	}
}
