/**
 * 
 */
package it.ibm.red.business.dto;

/**
 * The Class PropertyTemplateDTO.
 *
 * @author APerquoti
 */
public class PropertyTemplateDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3643327889223794155L;
	
	/**
	 * Tipo String.
	 */
	public static final int TIPOSTRING = 1;

	/**
	 * Tipo int.
	 */
	public static final int TIPOINT = 2;

	/**
	 * Tipo Date.
	 */
	public static final int TIPODATE = 3;

	/**
	 * Tipo Boolean.
	 */
	public static final int TIPOBOOLEAN = 4;

	/**
	 * Tipo Binary.
	 */
	public static final int TIPOBINARY = 5;

	/**
	 * Nome della Property.
	 */
	private String nomeProperty;
	
	/**
	 * Tipo della Property.
	 */
	private int tipo;
	
	/**
	 * Costruttore property template DTO.
	 */
	public PropertyTemplateDTO() {
		super();
	}

	/** 
	 * @return the nomeProperty
	 */
	public String getNomeProperty() {
		return nomeProperty;
	}

	/** 
	 * @param nomeProperty the nomeProperty to set
	 */
	public void setNomeProperty(final String nomeProperty) {
		this.nomeProperty = nomeProperty;
	}

	/** 
	 * @return the tipo
	 */
	public int getTipo() {
		return tipo;
	}

	/** 
	 * @param tipo the tipo to set
	 */
	public void setTipo(final int tipo) {
		this.tipo = tipo;
	}
	
}
