/**
 * 
 */
package it.ibm.red.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IConservazioneDAO;
import it.ibm.red.business.dto.ConservazioneWsDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * @author APerquoti
 *
 */
@Repository
public class ConservazioneDAO extends AbstractDAO implements IConservazioneDAO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -8009753447536763342L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ConservazioneDAO.class.getName());

	/**
	 * @see it.ibm.red.business.dao.IConservazioneDAO#checkDocumentoConservato(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean checkDocumentoConservato(final String uuid, final Connection con) {
		Boolean output = Boolean.TRUE;
		final StringBuilder sb = new StringBuilder();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {

			sb.append("SELECT COUNT(0) ")
			  .append("FROM conservazione ")
			  .append("WHERE uuiddocumento = ? ");
			
			ps = con.prepareStatement(sb.toString());
			
			final int index = 1;
			ps.setString(index, uuid);
			
			rs = ps.executeQuery();
			
			rs.next();
			output = (rs.getInt(1) > 0) ? Boolean.TRUE : Boolean.FALSE;
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante la verifica se il documento è già Conservato. ", e);
			throw new RedException("Errore durante la verifica se il documento è già Conservato. ", e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IConservazioneDAO#setConservazione(it.ibm.red.business.dto.ConservazioneWsDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public int setConservazione(final ConservazioneWsDTO daConservare, final Connection con) {
		int output = 0;
		CallableStatement cs = null;
		
		try {
			
			cs = con.prepareCall("{? = call f_setconservazione(?,?,?,?,?,?,?,?,?)}");
			
			cs.registerOutParameter(1, Types.INTEGER);
            cs.setInt(2, daConservare.getIdConservazione());
            cs.setLong(3, daConservare.getIdTicket());
            cs.setInt(4, daConservare.getCodiceErrore());
            
            if (daConservare.getDescrizioneErrore() != null && !daConservare.getDescrizioneErrore().isEmpty()) {
            	cs.setString(5, daConservare.getDescrizioneErrore());
			} else {
				cs.setNull(5, Types.VARCHAR);
			}
            
            cs.setString(6, daConservare.getIdApplicativo());
            cs.setString(7, daConservare.getNomeDocumento());
            cs.setString(8, daConservare.getUuidDocumento());
            
            if (daConservare.getDataConservazione() != null) {
            	cs.setDate(9, new java.sql.Date(daConservare.getDataConservazione().getTime()));
			} else {
				cs.setNull(9, Types.DATE);
			}
            
            cs.setInt(10, daConservare.getStato());
            cs.execute();
            
            output = cs.getInt(1);
            
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'update/insert di un record sulla tabella CONSERVAZIONE. ", e);
			throw new RedException("Errore durante l'update/insert di un record sulla tabella CONSERVAZIONE. ", e);
		} finally {
			close(cs);
		}
		
		return output;
	}

}
