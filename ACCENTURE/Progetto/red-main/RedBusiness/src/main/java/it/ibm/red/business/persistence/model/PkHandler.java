package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * Classe PkHandler.
 */
public class PkHandler implements Serializable {
	
	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = -2194900340919556979L;

	/**
	 * Identificativo PK Handler
	 */
	private int id;
	
	/**
	 * Nome handler
	 */
	private String handler;
	
	/**
	 * Pin
	 */
	private byte[] securePin;
	
	
	/**
	 * Costruttore pk handler.
	 */
	public PkHandler() {
	}
	
	
	/**
	 * Costruttore pk handler.
	 *
	 * @param id the id
	 * @param handler the handler
	 * @param securePin the secure pin
	 */
	public PkHandler(final int id, final String handler, final byte[] securePin) {
		super();
		this.id = id;
		this.handler = handler;
		this.securePin = securePin;
	}


	/** 
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	

	/** 
	 * @return the handler
	 */
	public String getHandler() {
		return handler;
	}
	

	/** 
	 * @return the secure pin
	 */
	public byte[] getSecurePin() {
		return securePin;
	}


	/** 
	 * @param id the id to set
	 */
	public void setId(final int id) {
		this.id = id;
	}


	/** 
	 * @param handler the handler to set
	 */
	public void setHandler(final String handler) {
		this.handler = handler;
	}


	/** 
	 * @param securePin the securePin to set
	 */
	public void setSecurePin(final byte[] securePin) {
		this.securePin = securePin;
	}
}