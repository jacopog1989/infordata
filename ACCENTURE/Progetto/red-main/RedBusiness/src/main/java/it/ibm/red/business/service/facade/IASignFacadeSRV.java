package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignMasterDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.service.asign.step.StatusEnum;
import it.ibm.red.business.service.asign.step.StepEnum; 

/**
 * Facade del servizio di firma asincrona.
 */
public interface IASignFacadeSRV extends Serializable {
	
	/**
	 * Imposta la priorità di lavorazione dell'item identificato dal
	 * <code> idItem </code>.
	 * 
	 * @param idItem
	 *            identificativo item per il quale impostare la priorità
	 * @param priority
	 *            priorità di lavorazione item
	 */
	void setPriority(Long idItem, Integer priority);
	
	/**
	 * Imposta la priorità come massima.
	 * 
	 * @param numeroProtocollo
	 *            numero protocollo associato all'item
	 * @param annoProtocollo
	 *            anno protocollo associato all'item
	 */
	void setMaxPriority(Integer numeroProtocollo, Integer annoProtocollo);
	
	/**
	 * Restituisce l'identificativo dell'item con l'id maggiore tra tutti quelli
	 * presenti. Più è alto l'identificativo più è recente l'inserimento dell'item,
	 * quindi questo metodo consente di recuperare l'item inserito pù di recente.
	 * 
	 * @param numeroProtocollo
	 *            numero protocollo associato all'item
	 * @param annoProtocollo
	 *            anno protocollo associato all'item
	 * @return idetntificativo item
	 */
	Long getMaxIdItem(Integer numeroProtocollo, Integer annoProtocollo);

	/**
	 * Restituisce set di document title, recuperando gli item afferenti alla coda
	 * specificata dal DB, se l'utente è della gestione applicativa. Se l'utente non
	 * è della gestione applicativa, i document title vengono recuperati da Filenet.
	 * 
	 * @param utente
	 *            utente in sessione
	 * @param queue
	 *            coda di lavorazione NSD (Libro Firma) o DD_DA_FIRMARE (Libro Firma
	 *            FEPA)
	 * @return document title dei documenti presenti nella coda <code> queue </code>
	 */
	Set<String> getItemsDT(UtenteDTO utente, DocumentQueueEnum queue);
	
	/**
	 * Restituisce un set di document title che fanno riferimento ad item che non
	 * hanno ancora raggiunto lo stato <code> StatusEnum.ELABORATO </code>.
	 * 
	 * @param utente
	 *            utente in sessione
	 * @param queue
	 *            coda di lavorazione
	 * @return lista di document title recuperati e presenti nella coda
	 *         <code> queue </code>
	 */
	Set<String> getIncompletedDTs(UtenteDTO utente, DocumentQueueEnum queue);
	
	/**
	 * Esegue il processo di firma remota per i documenti identificati dai
	 * <code> wobNumbers </code>.
	 * 
	 * @param wobNumbers
	 *            identificativi dei documenti da firmare
	 * @param pin
	 *            pin per la firma digitale
	 * @param otp
	 *            one-time-password per la firma digitale
	 * @param utenteFirmatario
	 *            utente responsabile per la firma dei documenti identificati dai
	 *            <code> wobNumbers </code>
	 * @param signType
	 *            tipologia della firma, @see SignTypeEnum
	 * @param firmaMultipla
	 *            specifica se l'iter è di firma multipla o di firma singola
	 * @return esiti della firma
	 */
	Collection<EsitoOperazioneDTO> firmaRemota(Collection<String> wobNumbers, String pin, String otp, UtenteDTO utenteFirmatario, SignTypeEnum signType, boolean firmaMultipla);
	
	/**
	 * Esegue il procedimento di firma autografa per i documenti identificati dai
	 * <code> wobNumbers </code>.
	 * 
	 * @param wobNumbers
	 *            identificativi dei documenti
	 * @param utenteFirmatario
	 *            utente responsabile della firma dei documenti identificati dai
	 *            <code> wobNumbers </code>
	 * @param protocolloEmergenza
	 *            protocollo di emergenza
	 * @return esiti della firma autografa
	 */
	Collection<EsitoOperazioneDTO> firmaAutografa(Collection<String> wobNumbers, UtenteDTO utenteFirmatario, ProtocolloEmergenzaDocDTO protocolloEmergenza);
	
	/**
	 * Restituisce il sign transaction id per legare le varie attività del processo di firma sul log.
	 * 
	 * @param utenteFirmatario
	 * @param tipologiaFirma
	 * @return
	 */
	String getSignTransactionId(UtenteDTO utenteFirmatario, String tipologiaFirma);

	/**
	 * Esegue le azioni precedentemente alla firma locale del documento identificato
	 * dal <code> wobNumber </code>.
	 * 
	 * @param signTransactionId
	 *            transactionId dell'operazione di firma
	 * @param wobNumber
	 *            identificativo documento in firma locale
	 * @param utenteFirmatario
	 *            utente responsabile della firma del documenti identificato dal
	 *            <code> wobNumber </code>
	 * @param signType
	 *            tipologia della firma, @see SignTypeEnum
	 * @param firmaMultipla
	 *            specifica la tipologia di firma intesa come multipla o singola
	 * @return esito della firma locale del documento identificato dal
	 *         <code> wobNumber </code>
	 */
	EsitoOperazioneDTO preFirmaLocale(String signTransactionId, String wobNumber, UtenteDTO utenteFirmatario, SignTypeEnum signType, boolean firmaMultipla);

	/**
	 * Esegue operazioni a valle della firma del documento a partire dalle
	 * informazioni sull'esito ottenute da <code> signOutCome </code>.
	 *
	 * @param signTransactionId
	 *            transactionId dell'operazione di firma
	 * @param signOutcome
	 *            esito firma locale documento
	 * @param utenteFirmatario
	 *            utente responsabile della firma
	 * @param signType
	 *            tipologia della firma, @see SignTypeEnum
	 * @param copiaConforme
	 *            definisce il flag della copia conforme
	 */
	void postFirmaLocale(String signTransactionId, EsitoOperazioneDTO signOutcome, UtenteDTO utenteFirmatario, SignTypeEnum signType, boolean copiaConforme);
	
	/**
	 * Restituisce i dettagli dell'item associato al documento identificato dal
	 * <code> dt </code>.
	 * 
	 * @param dt
	 *            document title del documento associato all'item
	 * @return informazioni sull'item
	 */
	ASignItemDTO getDetailInfo(String dt);
	
	/**
	 * Restituisce i dettagli degli item associati ai documenti identificati dai
	 * <code> dts </code>.
	 * 
	 * @param dts
	 *            lista di document title che consentono l'identificazione dei
	 *            documenti
	 * @return mappa che associa ad ogni document title un item con le informazioni
	 *         per il popolamento di un master
	 */
	Map<String, ASignMasterDTO> getMasterInfo(Collection<String> dts);

	/**
	 * Restituisce informazioni sull' item identificato dall' <code> idItem </code>.
	 * 
	 * @param idItem
	 *            identificativo dell'item da recuperare
	 * @param bLog
	 *            se true consente il recupero del log associato agli step
	 * @return informazioni dell'item identificato da <code> idItem </code>
	 */
	ASignItemDTO getItem(Long idItem, Boolean bLog);

	/**
	 * Restituisce l'item identificato dall' <code> idItem </code>.
	 * 
	 * @param idItem
	 *            identificativo dell'item da recuperare
	 * @return tutte le informazioni dell'item identificato dal suo id
	 */
	ASignItemDTO getItem(Long idItem);

	/**
	 * Effettua il reset del numero dei retry associato all'item identificato dall'
	 * <code> idItem </code>. Il reset consente di rielaborare l'item con il
	 * <code> retry </code> dell'Orchestrator di firma asincrona anche se l'item è
	 * stato già sottoposto a lavorazione per il numero massimo di volte consentito
	 * in quanto questo metodo ne azzera i tentativi.
	 * 
	 * @param idItem
	 *            identificativo dell'item per il quale occorre azzerare il numero
	 *            dei retry
	 * @return esito del reset
	 */
	EsitoOperazioneDTO resetRetry(Long idItem);

	/**
	 * Recupera stato raggiunto da un documento. Lo stato raggiunto viene
	 * identificato dallo stato dell'item di riferimento al document title per il
	 * quale non sia stata definita una data di fine lavorazione.
	 * 
	 * @param documentTitle
	 *            identificativo del documento per il quale occorre conoscere lo
	 *            stato corrente
	 * @return stato corrent del documento, @see StatusEnum
	 */
	StatusEnum checkDocStatus(String documentTitle);

	/**
	 * Recupera id dell'item associato al documento identificato dal
	 * <code> documentTitle </code>.
	 * 
	 * @param documentTitle
	 *            identificativo del documento
	 * @return identificativo item associato al documento
	 */
	Long getIdItem(String documentTitle);

	/**
	 * Imposta la priorità massima per l'item associato al documento identificato
	 * dal <code> dt </code>.
	 * 
	 * @param dt
	 *            document title del documento
	 */
	void setMaxPriority(String dt);
	
	/**
	 * Restituisce il log associato allo step definito da <code> step </code> per
	 * l'item identificato da <code> idItem </code>.
	 * 
	 * @param idItem
	 *            identificativo item
	 * @param step
	 *            step specifico per il quale occorre conoscere il log
	 * @return log associato allo step se esistente, null altrimenti
	 */
	String getLog(Long idItem, StepEnum step);
	
	/**
	 * Restituisce true se esiste almeno un item associato ad un documento
	 * identificato con il <code> documentTitle </code>.
	 * 
	 * @param documentTitle
	 *            identificativo documento per il quale occorre conoscere se esiste
	 *            almeno un item
	 * @return true se esiste l'item, false altrimenti
	 */
	Boolean isPresent(String documentTitle);
	
	/**
	 * Effettua l'insert dello step di <code> StepEnum.STOP </code>.
	 * 
	 * @param idItem
	 *            identificativo dell'item per il quale occorre inserire lo step:
	 *            STOP
	 */
	void insertStopStep(Long idItem);

	/**
	 * Effettua la insert del content nella tabella Buffer per la gestione dei
	 * content firmati con una delle stategie asincrone.
	 * 
	 * @param wobNumber
	 *            wob number documento
	 * @param guid
	 *            guid documento
	 * @param contentFirmato
	 *            bytearray del content firmato
	 * @param allegato
	 *            true se il documento è un allegato, false altrimenti
	 */
	void insertIntoSignedDocsBuffer(String wobNumber, String guid, byte[] contentFirmato, boolean allegato);
	Document findDocAggiornato(String documentTitle, UtenteDTO utente, boolean isPrincipale);

}
