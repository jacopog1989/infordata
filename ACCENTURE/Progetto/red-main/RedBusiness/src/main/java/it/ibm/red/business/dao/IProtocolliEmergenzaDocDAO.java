package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;

import it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO;

/**
 * 
 * @author a.dilegge
 *
 *         Interfaccia del DAO per la gestione delle informazioni relative ai protocolli di emergenza e le sue riconciliazioni con il registro ufficiale.
 */

public interface IProtocolliEmergenzaDocDAO extends Serializable {
	
	/**
	 * Verifica se il protocollo d'emergenza in input è già presente in coda
	 * 
	 * @param messageIdNPS
	 * @param connection
	 * @return true se esiste un item elaborato con il messageId in input, false altrimenti
	 */
	boolean isProtocolloEmergenzaPresent(Integer numeroProtocolloEmergenza, Integer annoProtocolloEmergenza, Long idAoo, Connection connection);

	/**
	 * Inserisce il protocollo di emergenza sulla base dati
	 * 
	 * @param dto
	 * @param connection
	 * @return
	 */
	int insert(ProtocolloEmergenzaDocDTO dto, Connection connection);

	/**
	 * Recupera le informazioni relative al protocollo di emergenza per il documento in input
	 * 
	 * @param idDocumento
	 * @param idAoo
	 * @param connection
	 * @return
	 */
	ProtocolloEmergenzaDocDTO getByIdDocumento(String idDocumento, Long idAoo, Connection connection);
	
	/**
	 * Verifica se il protocollo in input è un emergenza non ancora riconciliata con l'ufficiale
	 * 
	 * @param idProtocolloEmergenza
	 * @param connection
	 * @return true se esiste un protocollo di emergenza per l'id in input non riconciliato con l'ufficiale, false altrimenti
	 */
	boolean isEmergenzaNotUpdated(String idProtocolloEmergenza, Connection connection);
	
	/**
	 * Verifica se il messageIdNPS in input è già presente in coda
	 * 
	 * @param messageIdNPS
	 * @param connection
	 * @return true se esiste un item elaborato con il messageId in input, false altrimenti
	 */
	boolean isMessageIdNPSPresent(String messageIdNPS, Connection connection);

	/**
	 * Aggiorna il protocollo di emergenza con le informazioni relative al protocollo ufficiale
	 * 
	 * @param dto
	 * @param connection
	 * @return
	 */
	int riconcilia(ProtocolloEmergenzaDocDTO dto, Connection connection);
	
	/**
	 * Recupera le informazioni relative al protocollo di emergenza per l'identificativo di protocollo in input
	 * 
	 * @param idProtocollo
	 * @param connection
	 * @return
	 */
	ProtocolloEmergenzaDocDTO getByIdProtocollo(String idProtocollo, Connection connection);
	
}