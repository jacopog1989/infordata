package it.ibm.red.business.enums;

import java.util.Calendar;
import java.util.Date;
 
/**
 * Enum che definisce i possibili ambiti temporali.
 */
public enum AmbitoTemporaleEnum {
	
	/**
	 * Periodo ultima settimana.
	 */
	ULTIMA_SETTIMANA("S", "Ultima Settimana"), 

	/**
	 * Periodo ultimo mese.
	 */
	ULTIMO_MESE("M", "Ultimo Mese"),

	/**
	 * Periodo ultimo trimestre.
	 */
	ULTIMO_TRIMESTRE("T", "Ultimo Trimestre");

	/**
	 * Identificativo.
	 */
	private final String id;

	/**
	 * Nome.
	 */
	private final String nome;
	
	
	AmbitoTemporaleEnum(final String id, final String nome) {
		this.id = id;
		this.nome = nome;
	}

	/**
	 * Restituisce l'id associato all'ambito temporale.
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Restituisce l'enum dato l'id.
	 * @param id
	 * @return Enum associata all'id
	 */
	public static AmbitoTemporaleEnum getEnumById(final String id) {
		AmbitoTemporaleEnum output = null;
		for (AmbitoTemporaleEnum item : AmbitoTemporaleEnum.values()) {
			if (item.getId().equals(id)) {
				output = item;
				break;
			}
		}

		return output;
	}

	/**
	 * Restituisce la data a partire dall'ambito temporale di riferimento.
	 * @return
	 */
	public Date calcolaData() {
		Date data = null;
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		
		switch (this) {
		//sottraggo giorni in base all'ambito temporale
		case ULTIMA_SETTIMANA:
			cal.add(Calendar.DAY_OF_YEAR, -7);
			break;
		case ULTIMO_MESE:
			cal.add(Calendar.DAY_OF_YEAR, -30);
			break;
		case ULTIMO_TRIMESTRE:
			cal.add(Calendar.DAY_OF_YEAR, -90);
			break;
		default:
			break;
		}
		data = cal.getTime();
		
		return data;
	}
}