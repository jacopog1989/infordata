package it.ibm.red.business.enums;

/**
 * The Enum FilenetTipoOperazioneEnum.
 *
 * @author mcrescentini
 * 
 *         Enum delle operazioni Filenet.
 */
public enum FilenetTipoOperazioneEnum {
	
	/**
	 * Aggiungi.
	 */
	ADD(1),
	
	/**
	 * Rimuovi.
	 */
	REMOVE(2),
	
	/**
	 * Copia.
	 */
	COPY(3),
	
	/**
	 * Sposta.
	 */
	MOVE(4),
	
	/**
	 * Linka.
	 */
	LINK(5),
	
	/**
	 * Elimina legame.
	 */
	UNFILE(6),
	
	/**
	 * Modifica.
	 */
	MODIFY(7);
	
	/**
	 * Tipo operazione.
	 */
	private final int tipoOperazione;
	
	/**
	 * Costruttore dell'Enum, imposta il tipo operazione.
	 * @param tipoOperazione
	 */
	FilenetTipoOperazioneEnum(final int tipoOperazione) {
		this.tipoOperazione = tipoOperazione;
	}
	
	/**
	 * Restituisce il tipo operazione associato all'Enum.
	 * @return tipo operazione
	 */
	public int getValue() {
		return tipoOperazione;
	}
}