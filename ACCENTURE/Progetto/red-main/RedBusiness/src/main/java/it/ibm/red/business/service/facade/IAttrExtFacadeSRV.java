package it.ibm.red.business.service.facade;

import java.io.Serializable; 

import it.ibm.red.business.dto.UtenteDTO; 

/**
 * Facade del servizio di gestione attributi estesi (AttrExt).
 */
public interface IAttrExtFacadeSRV extends Serializable {

	/**
	 * Metodo per l'export in formato XML.
	 * @param utente
	 * @param idTipoDocumento - id del tipo documento
	 * @param idTipoProcedimento - id del tipo procedimento
	 * @return export
	 */
	String exportSample(UtenteDTO utente, Integer idTipoDocumento, Integer idTipoProcedimento);

}