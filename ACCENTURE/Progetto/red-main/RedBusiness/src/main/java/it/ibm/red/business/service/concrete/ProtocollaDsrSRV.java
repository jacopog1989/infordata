package it.ibm.red.business.service.concrete;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.Modalita;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.HierarchicalFileWrapperDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.exception.AdobeException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.adobe.AdobeLCHelper;
import it.ibm.red.business.helper.adobe.IAdobeLCHelper;
import it.ibm.red.business.helper.adobe.dto.CountSignsOutputDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IProtocollaDsrSRV;
import it.ibm.red.business.service.ISalvaDocumentoSRV;
import it.ibm.red.business.service.IScompattaMailSRV;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;

/**
 * Service che gestisce la protocollazione di una DSR.
 */
@Service
public class ProtocollaDsrSRV extends AbstractService implements IProtocollaDsrSRV {
	
	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 1818525549079939254L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ProtocollaDsrSRV.class.getName());

	/**
	 * DAO.
	 */
	@Autowired
	private ITipologiaDocumentoDAO tipologiaDocumentoDAO;

	/**
	 * DAO.
	 */	
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;

	/**
	 * DAO.
	 */	
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Service.
	 */	
	@Autowired
	private IScompattaMailSRV mailSRV;
	
	/**
	 * Service.
	 */	
	@Autowired
	private ISalvaDocumentoSRV salvaDocumentoSRV;
	
	/**
	 * Service.
	 */	
	@Autowired
	private ITipologiaDocumentoFacadeSRV tipologiaDocumentoSRV;
	
	/**
	 * Properties.
	 */	
	private PropertiesProvider pp;
	
	/**
	 * Post construct del Service.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}
	
	/**
	 * Permette di inizializzare il detailDocument per la protocollazioneDsr Siccome
	 * sono presenti alcune "logiche di business" ho creato questo mmetodo.
	 */
	@Override
	public DetailDocumentRedDTO initializeDetailDocumentForProtocollazioneDsr(final DetailDocumentRedDTO detailDocument, final UtenteDTO utente) {
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);

			detailDocument.setFormatoDocumentoEnum(FormatoDocumentoEnum.ELETTRONICO);
			detailDocument.setDescrizioneFascicoloProcedimentale(Constants.Fepa.OGGETTO_PROTOCOLLAZIONE_DSR_PREFIX + detailDocument.getOggetto());
			detailDocument.setIndiceClassificazioneFascicoloProcedimentale(pp.getParameterByKey(PropertiesNameEnum.FEPA_INDICE_CLASSIFICAZIONE_DEFAULT));
			
			inizializzaTipoProcedimentoAndDocumento(detailDocument, CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0], utente, conn);
			
			List<AssegnazioneDTO> assegnazioni = new ArrayList<>(1);			
			AssegnazioneDTO perCompetenza = getAssegnazioneDefaultUfficioFepa(conn);
		
			assegnazioni.add(perCompetenza);
			detailDocument.setAssegnazioni(assegnazioni);
		} catch (SQLException sql) {
			LOGGER.error(sql);
			throw new RedException("Impossibile recuperare l'ufficio per competenza");
		} catch (RedException red) {
			LOGGER.error(red);
			throw red;
		} catch (Exception e) {
			LOGGER.error(e);
			throw new RedException("Errore generico durante l'inizializzazione della protocolalzione");
		} finally {
			closeConnection(conn);
		}
		
		return detailDocument;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IProtocollaDsrFacadeSRV#getAssegnazioneDefaultUfficioFepa(java.sql.Connection).
	 */
	@Override
	public AssegnazioneDTO getAssegnazioneDefaultUfficioFepa(final Connection conn) {
		Nodo nodo = nodoDAO.getNodo(Long.valueOf(pp.getParameterByKey(PropertiesNameEnum.FEPA_UFFICIO_COMPETENTE)), conn);
		UfficioDTO ufficio =  new UfficioDTO(nodo.getIdNodo(), nodo.getDescrizione());
		return new AssegnazioneDTO("Assegnazione di default", TipoAssegnazioneEnum.COMPETENZA, null, null, ufficio.getDescrizione(), null, ufficio);
		 
	}

	/**
	 * @see it.ibm.red.business.service.IProtocollaDsrSRV#inizializzaTipoProcedimentoAndDocumento(it.ibm.red.business.dto.DetailDocumentRedDTO,
	 *      java.lang.Integer, it.ibm.red.business.dto.UtenteDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public void inizializzaTipoProcedimentoAndDocumento(final DetailDocumentRedDTO detailDocument, final Integer idCategoriaDocumento, final UtenteDTO utente, final Connection conn) {
		detailDocument.setIdCategoriaDocumento(idCategoriaDocumento);
		List<TipologiaDocumentoDTO> tipologiaDichiarazioneServiziResi = tipologiaDocumentoDAO.getTipologiaByClasseDocumentaleAndAooAndidTipologiaDocumento(
				pp.getParameterByKey(PropertiesNameEnum.DSR_CLASSNAME_FN_METAKEY),
				utente.getIdAoo(),	detailDocument.getIdCategoriaDocumento(), conn);
		if (CollectionUtils.isEmpty(tipologiaDichiarazioneServiziResi) || tipologiaDichiarazioneServiziResi.size() > 1) {
			throw new RedException("Esistono più record di tipologiaDocumento per dichiarazione servizi resi con la medesima categoriaDocumento");
		}
		TipologiaDocumentoDTO tipoDichiarazione = tipologiaDichiarazioneServiziResi.get(0);
		detailDocument.setIdTipologiaDocumento(tipoDichiarazione.getIdTipologiaDocumento());
		
		
		List<TipoProcedimento> tipoProcedimentoList = tipoProcedimentoDAO.getTipiProcedimentoByTipologiaDocumento(conn, tipoDichiarazione.getIdTipologiaDocumento());
		if (CollectionUtils.isEmpty(tipoProcedimentoList)) {
			throw new RedException("Non esistono tipiProcedimento asssociati al tipologiaDocumento id: " + tipoDichiarazione.getIdTipologiaDocumento());
		}
		TipoProcedimento tipoProcedimento = tipoProcedimentoList.get(0);
		Long idTipoProcedimento = tipoProcedimento.getTipoProcedimentoId();
		detailDocument.setIdTipologiaProcedimento(idTipoProcedimento.intValue());
	}

	/**
	 * @see it.ibm.red.business.service.facade.IProtocollaDsrFacadeSRV#validateProtocollazioneDSR(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	public boolean validateProtocollazioneDSR(final UtenteDTO utente, final String guidMailAllegatoPath) {
		boolean res = false;
		if (!"pdf".equals(FilenameUtils.getExtension(guidMailAllegatoPath))) {
			LOGGER.error("Il documento selezionato come documento principale per la protocollazione DSR non è un pdf");
		} else {
			byte[] byteArray = mailSRV.getContentutoDiscompattataMail(utente, guidMailAllegatoPath);
			res = validateAdobeProtocollazioneDsr(byteArray);
		}
		
		if (!res) {
			LOGGER.error("Impossibile generare l'inputstream a partire dal contenuto con il seguente identificativo: " + guidMailAllegatoPath);
		}
		
		return res;
	}

	/**
	 * @see it.ibm.red.business.service.IProtocollaDsrSRV#validateAdobeProtocollazioneDsr(byte[]).
	 */
	@Override
	public boolean validateAdobeProtocollazioneDsr(final byte[] byteArray) {
		boolean res = false;
		IAdobeLCHelper adobe = new AdobeLCHelper();
		try (
			InputStream io = new ByteArrayInputStream(byteArray);
		) {			
			CountSignsOutputDTO countSigns = adobe.countsSignedFields(io);
			
			res = !(countSigns.getNumSignFields() < 2 || countSigns.getNumSigns() < 1 || countSigns.getNumEmptySignFields() < 1);
		} catch (IOException ioExe) {
			LOGGER.error("Impossibile generare l'inputstream a partire dal contenuto con il seguente identificativo", ioExe);
		} catch (AdobeException adobeEcxeption) {
			LOGGER.error("Errore durante la validazione di un documento per la protocollazione Dsr", adobeEcxeption);
		}
		return res;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IProtocollaDsrFacadeSRV#protocollaMailDsr(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.DetailDocumentRedDTO).
	 */
	@Override
	public EsitoSalvaDocumentoDTO protocollaMailDsr(final UtenteDTO utente, final DetailDocumentRedDTO inDetail) {
		DetailDocumentRedDTO detail = inDetail;
		EsitoSalvaDocumentoDTO esito = new EsitoSalvaDocumentoDTO();
		//MI assicuro che le costanti non siano state modificate dal framework
		detail = initializeDetailDocumentForProtocollazioneDsr(detail, utente);
		
		// Importo il contenuto dall'email, oppure lo prendo dalla interfaccia se l'utente l'ha modificato
		if (detail.getContent() == null) {
			byte[] allegatoContent = mailSRV.getContentutoDiscompattataMail(utente, detail.getName());
			detail.setContent(allegatoContent);
		} else {
			if (!validateAdobeProtocollazioneDsr(detail.getContent())) {
				esito.setNote("Il documento deve essere formito di 2 campi firma e solo uno dei due campi deve essere firmato");
				esito.setEsitoOk(false);
				return esito;
			}
		}
		
		if (CollectionUtils.isNotEmpty(detail.getAllegati())) {
			detail.getAllegati().stream()
				.filter(allegato -> (allegato.getContent()  == null || allegato.getContent().length == 0) && org.apache.commons.lang3.StringUtils.isNotBlank(allegato.getDocumentTitle()))
				.forEach(allegato ->  allegato.setContent(mailSRV.getContentutoDiscompattataMail(utente, allegato.getDocumentTitle())));
			
			// Qua annulliano il contenuto degli allegati con documentitle nullo. Sarebbe più corretto descrivere la situazione nell'esitoOperazione o almeno loggarlo
			List<AllegatoDTO> errorList = detail.getAllegati().stream()
			.filter(allegato -> org.apache.commons.lang3.StringUtils.isBlank(allegato.getDocumentTitle()))
			.collect(Collectors.toList());
			errorList.stream().forEach(allegato ->  allegato.setContent(new byte[0]));
			
			if (CollectionUtils.isNotEmpty(errorList)) {
				LOGGER.error("Attenzione uno degli allegati da protocollare risulta avere il path di scompattaMail nullo quindi non è possibile recupere il suo content");
			}
			
		}
		
		SalvaDocumentoRedParametriDTO parametri = new SalvaDocumentoRedParametriDTO();
		parametri.setInputMailGuid(detail.getProtocollazioneMailGuid());
		parametri.setModalita(Modalita.MODALITA_INS_MAIL);
		parametri.setContentVariato(true);
		
		return salvaDocumentoSRV.salvaDocumento(detail, parametri, utente, ProvenienzaSalvaDocumentoEnum.FEPA_DSR);
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IProtocollaDsrFacadeSRV#createDetailFromMail(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.persistence.model.Contatto, java.util.Collection,
	 *      it.ibm.red.business.dto.HierarchicalFileWrapperDTO, java.lang.Boolean,
	 *      java.lang.String, java.lang.String).
	 */
	@Override
	public DetailDocumentRedDTO createDetailFromMail(final UtenteDTO utente, final Contatto contact, final Collection<HierarchicalFileWrapperDTO> selectedAllegati, 
			final HierarchicalFileWrapperDTO principale, final Boolean notificaProtocolla, final String mailGUID, final String mailOggetto) {
		
		
		DetailDocumentRedDTO detailsProt = new DetailDocumentRedDTO();
		List<AllegatoDTO> allegatiSelezionatiDati = new ArrayList<>();
		
		List<TipologiaDocumentoDTO> tipiDocAttive = tipologiaDocumentoSRV.getComboTipologieByAooAndTipoCategoriaAttivi(TipoCategoriaEnum.ENTRATA, utente.getIdAoo());
		
		// Dettagli degli allegati selezionati
		for (HierarchicalFileWrapperDTO selected : selectedAllegati) {
			String nomeFile = selected.getFile().getName();
			String mimeType = selected.getFile().getTipoFile().getMimeType();
			//2 -> formatoElettronico
			boolean fileNonSbustato = selected.getFileNonSbustato();
			AllegatoDTO alleg = new AllegatoDTO(selected.getNodeId(), FormatoAllegatoEnum.ELETTRONICO.getId(), null, null, null, null, null, false, null, null, nomeFile, mimeType,
					null, null, null, null, null, null, true, tipiDocAttive, fileNonSbustato, false);		
			alleg.setNodeIdFromHierarchicalFile(selected.getNodeId());
			allegatiSelezionatiDati.add(alleg);
		}
		detailsProt.setAllegati(allegatiSelezionatiDati);
		
		detailsProt.setName(principale.getNodeId());
		detailsProt.setNomeFile(principale.getFile().getName());
		detailsProt.setMimeType(principale.getFile().getTipoFile().getMimeType());
			
		detailsProt.setProtocollazioneMailGuid(mailGUID);

		detailsProt.setMittenteContatto(contact);
		detailsProt.setOggetto(mailOggetto);
		detailsProt.setIsNotifica(notificaProtocolla);
		detailsProt.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0]); //1
		detailsProt.setIdFilePrincipaleDaProtocolla(principale.getNodeId());
		
		detailsProt = initializeDetailDocumentForProtocollazioneDsr(detailsProt, utente);
		
		// Proveniendo da mail il dettaglio è modificabile
		detailsProt.setModificabile(true);
		
		return detailsProt;
	}

}
