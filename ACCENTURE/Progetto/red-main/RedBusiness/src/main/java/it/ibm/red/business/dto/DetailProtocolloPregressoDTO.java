package it.ibm.red.business.dto;
 
import java.util.Date;
import java.util.List;
 
/**
 * 
 * @author VINGENITO
 *
 */
public class DetailProtocolloPregressoDTO extends AbstractDTO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -7779722029355159611L;
	
	/**
	 * Codice aoo.
	 */
	private String codiceAoo;
	
	/**
	 * Descrizione aoo.
	 */
	private String descrizioneAoo;
	
	/**
	 * Codice registro.
	 */
	private String codRegistro;
	
	/**
	 * Descrizione registro.
	 */
	private String descRegistro;
	
	/**
	 * Ident.
	 */
	private Integer idProtocollo;
	
	/**
	 * PRotocollatore.
	 */
	private String protocollatore;
	
	/**
	 * Descrizione protocollatore.
	 */
	private String descProtocollatore;
	
	/**
	 * Oggetto.
	 */
	private String descOggetto;
	
	/**
	 * Identificativo ufficio.
	 */
	private String idUfficio;
	
	/**
	 * Descrizione ufficio.
	 */
	private String descUfficio;
	
	/**
	 * Codice ente.
	 */
	private String codiceEnte;
	
	/**
	 * Titolario.
	 */
	private String campoTitolario;
	
	/**
	 * Data acquisizione documento.
	 */
	private Date dataAcquisizioneDoc;
	
	/**
	 * Data protocollo.
	 */
	private Date dataProtocollo;
	
	/**
	 * Data ricezione.
	 */
	private Date dataRicezione;
	
	/**
	 * Tipo documento.
	 */
	private String tipoDocumento;
	
	/**
	 * Id documento originario.
	 */
	private String originalDocId;
	
	/**
	 * Registro.
	 */
	private Long registro;
	
	/**
	 * Allegati.
	 */
	private List<AllegatoNsdDTO> listaAllegati;
	
	/**
	 * Filename.
	 */
	private String descFileName;
	
	/**
	 * Mittente.
	 */
	private String mittente;
	
	/**
	 * Numero allegati.
	 */
	private String numAllegati; 
	
	/**
	 * Flag annullato.
	 */
	private boolean flagAnnullato;
	
	/**
	 * Destinatario.
	 */
	private String destinatario;
	
	/**
	 * Modaliutà.
	 */
	private String modalita;
	
	/**
	 * Flag chiuso.
	 */
	private boolean flagChiuso;
	  
	/**
	 * Costruttore che riversa il master nel detail.
	 * 
	 * @param prot
	 */
	public DetailProtocolloPregressoDTO(final ProtocolliPregressiDTO prot) {
		codiceAoo = prot.getCodiceAoo(); 
		codRegistro = prot.getCodRegistro(); 
		idProtocollo = prot.getIdProtocollo(); 
		descProtocollatore = prot.getDescProtocollatore();
		descOggetto = prot.getDescOggetto(); 
		descUfficio = prot.getDescUfficio(); 
		campoTitolario = prot.getCampoTitolario(); 
		dataProtocollo = prot.getDataProtocollo(); 
		registro = prot.getRegistro();
		tipoDocumento = prot.getTipoDocumento();
		originalDocId = prot.getOriginalDocId(); 
		numAllegati = prot.getNumAllegati();
		descFileName = prot.getDescFileName();
		mittente = prot.getMittente();
		destinatario = prot.getDestinatario();
		flagAnnullato = prot.isFlagAnnullato();
		flagChiuso = prot.isFlagChiuso(); 
		modalita = prot.getModalita(); 
		listaAllegati = prot.getListaAllegati();
		
	}
	/**
	 * @return the codiceAoo
	 */
	public String getCodiceAoo() {
		return codiceAoo;
	}

	/**
	 * @param codiceAoo the codiceAoo to set
	 */
	public void setCodiceAoo(final String codiceAoo) {
		this.codiceAoo = codiceAoo;
	}

	/**
	 * @return the descrizioneAoo
	 */
	public String getDescrizioneAoo() {
		return descrizioneAoo;
	}

	/**
	 * @param descrizioneAoo the descrizioneAoo to set
	 */
	public void setDescrizioneAoo(final String descrizioneAoo) {
		this.descrizioneAoo = descrizioneAoo;
	}

	/**
	 * @return the codRegistro
	 */
	public String getCodRegistro() {
		return codRegistro;
	}

	/**
	 * @param codRegistro the codRegistro to set
	 */
	public void setCodRegistro(final String codRegistro) {
		this.codRegistro = codRegistro;
	}

	/**
	 * @return the descRegistro
	 */
	public String getDescRegistro() {
		return descRegistro;
	}

	/**
	 * @param descRegistro the descRegistro to set
	 */
	public void setDescRegistro(final String descRegistro) {
		this.descRegistro = descRegistro;
	}

	/**
	 * @return the idProtocollo
	 */
	public Integer getIdProtocollo() {
		return idProtocollo;
	}

	/**
	 * @param idProtocollo the idProtocollo to set
	 */
	public void setIdProtocollo(final Integer idProtocollo) {
		this.idProtocollo = idProtocollo;
	}

	/**
	 * @return the protocollatore
	 */
	public String getProtocollatore() {
		return protocollatore;
	}

	/**
	 * @param protocollatore the protocollatore to set
	 */
	public void setProtocollatore(final String protocollatore) {
		this.protocollatore = protocollatore;
	}

	/**
	 * @return the descProtocollatore
	 */
	public String getDescProtocollatore() {
		return descProtocollatore;
	}

	/**
	 * @param descProtocollatore the descProtocollatore to set
	 */
	public void setDescProtocollatore(final String descProtocollatore) {
		this.descProtocollatore = descProtocollatore;
	}

	/**
	 * @return the descOggetto
	 */
	public String getDescOggetto() {
		return descOggetto;
	}

	/**
	 * @param descOggetto the descOggetto to set
	 */
	public void setDescOggetto(final String descOggetto) {
		this.descOggetto = descOggetto;
	}

	/**
	 * @return the idUfficio
	 */
	public String getIdUfficio() {
		return idUfficio;
	}

	/**
	 * @param idUfficio the idUfficio to set
	 */
	public void setIdUfficio(final String idUfficio) {
		this.idUfficio = idUfficio;
	}

	/**
	 * @return the descUfficio
	 */
	public String getDescUfficio() {
		return descUfficio;
	}

	/**
	 * @param descUfficio the descUfficio to set
	 */
	public void setDescUfficio(final String descUfficio) {
		this.descUfficio = descUfficio;
	}

	/**
	 * @return the codiceEnte
	 */
	public String getCodiceEnte() {
		return codiceEnte;
	}

	/**
	 * @param codiceEnte the codiceEnte to set
	 */
	public void setCodiceEnte(final String codiceEnte) {
		this.codiceEnte = codiceEnte;
	}

	/**
	 * @return the campoTitolario
	 */
	public String getCampoTitolario() {
		return campoTitolario;
	}

	/**
	 * @param campoTitolario the campoTitolario to set
	 */
	public void setCampoTitolario(final String campoTitolario) {
		this.campoTitolario = campoTitolario;
	}

	/**
	 * @return the dataAcquisizioneDoc
	 */
	public Date getDataAcquisizioneDoc() {
		return dataAcquisizioneDoc;
	}

	/**
	 * @param dataAcquisizioneDoc the dataAcquisizioneDoc to set
	 */
	public void setDataAcquisizioneDoc(final Date dataAcquisizioneDoc) {
		this.dataAcquisizioneDoc = dataAcquisizioneDoc;
	}

	/**
	 * @return the dataProtocollo
	 */
	public Date getDataProtocollo() {
		return dataProtocollo;
	}

	/**
	 * @param dataProtocollo the dataProtocollo to set
	 */
	public void setDataProtocollo(final Date dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}

	/**
	 * @return the dataRicezione
	 */
	public Date getDataRicezione() {
		return dataRicezione;
	}

	/**
	 * @param dataRicezione the dataRicezione to set
	 */
	public void setDataRicezione(final Date dataRicezione) {
		this.dataRicezione = dataRicezione;
	}

	/**
	 * @return the registro
	 */
	public Long getRegistro() {
		return registro;
	}

	/**
	 * @param registro the registro to set
	 */
	public void setRegistro(final Long registro) {
		this.registro = registro;
	}

	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(final String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * @return the originalDocId
	 */
	public String getOriginalDocId() {
		return originalDocId;
	}

	/**
	 * @param originalDocId the originalDocId to set
	 */
	public void setOriginalDocId(final String originalDocId) {
		this.originalDocId = originalDocId;
	}
 

	/**
	 * @return the listaAllegati
	 */
	public List<AllegatoNsdDTO> getListaAllegati() {
		return listaAllegati;
	}
	
	/**
	 * @param listaAllegati the listaAllegati to set
	 */
	public void setListaAllegati(final List<AllegatoNsdDTO> listaAllegati) {
		this.listaAllegati = listaAllegati;
	}
	/**
	 * @return the numAllegati
	 */
	public String getNumAllegati() {
		return numAllegati;
	}

	/**
	 * @param numAllegati the numAllegati to set
	 */
	public void setNumAllegati(final String numAllegati) {
		this.numAllegati = numAllegati;
	}

	/**
	 * @return the descFileName
	 */
	public String getDescFileName() {
		return descFileName;
	}

	/**
	 * @param descFileName the descFileName to set
	 */
	public void setDescFileName(final String descFileName) {
		this.descFileName = descFileName;
	}

	/**
	 * @return the mittente
	 */
	public String getMittente() {
		return mittente;
	}

	/**
	 * @param mittente the mittente to set
	 */
	public void setMittente(final String mittente) {
		this.mittente = mittente;
	}

	/**
	 * @return the destinatario
	 */
	public String getDestinatario() {
		return destinatario;
	}

	/**
	 * @param destinatario the destinatario to set
	 */
	public void setDestinatario(final String destinatario) {
		this.destinatario = destinatario;
	}

	/**
	 * Restituisce true se annullato, false altrimenti.
	 * 
	 * @return annullato
	 */
	public boolean isFlagAnnullato() {
		return flagAnnullato;
	}

	/**
	 * Imposta il flag: flagAnnullato.
	 * 
	 * @param flagAnnullato
	 */
	public void setFlagAnnullato(final boolean flagAnnullato) {
		this.flagAnnullato = flagAnnullato;
	}

	/**
	 * Restituisce true se chiuso, false altrimenti.
	 * @return flagChiuso
	 */
	public boolean isFlagChiuso() {
		return flagChiuso;
	}

	/**
	 * Imposta il valore del flag.
	 * @param flagChiuso
	 */
	public void setFlagChiuso(final boolean flagChiuso) {
		this.flagChiuso = flagChiuso;
	}

	/**
	 * Restituisce la modalita.
	 * @return modalita
	 */
	public String getModalita() {
		return modalita;
	}

	/**
	 * Imposta la modalita.
	 * @param modalita
	 */
	public void setModalita(final String modalita) {
		this.modalita = modalita;
	}

}
