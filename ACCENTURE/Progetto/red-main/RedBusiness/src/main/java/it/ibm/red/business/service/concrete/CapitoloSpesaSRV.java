package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.ICapitoloSpesaDAO;
import it.ibm.red.business.dto.CapitoloSpesaDTO;
import it.ibm.red.business.dto.RicercaCapitoloSpesaDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.ICapitoloSpesaSRV;

/**
 * The Class CapitoloSpesaSRV.
 *
 * @author mcrescentini
 * 
 *         Servizio gestione capitoli di spesa.
 */
@Service
@Component
public class CapitoloSpesaSRV extends AbstractService implements ICapitoloSpesaSRV {

	private static final long serialVersionUID = -5049519354032869746L;
	

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CapitoloSpesaSRV.class.getName());
	
	/**
	 * Dao capitolo spesa.
	 */
	@Autowired
	private ICapitoloSpesaDAO capitoloSpesaDAO;
	

	/**
	 * Inserisce l'associazione con l'aoo. Se il capitolo non esiste, inserisce anche il capitolo in base dati.
	 */
	@Override
	public void inserisci(final CapitoloSpesaDTO capitoloSpesa, final Long idAoo) {
		Connection con = null;
		
		if (StringUtils.isNotBlank(capitoloSpesa.getCodice()) && StringUtils.isNotBlank(capitoloSpesa.getDescrizione()) 
				&& capitoloSpesa.getAnno() != null && capitoloSpesa.getFlagSentenza() != null) {
			try {
				con = setupConnection(getDataSource().getConnection(), false);
				
				//se non esiste capitolo, fai insert
				final CapitoloSpesaDTO capitoloDB = capitoloSpesaDAO.getByCodice(capitoloSpesa.getCodice(), con);
				if (capitoloDB == null) {
					// Si inserisce il nuovo capitolo di spesa
					capitoloSpesaDAO.inserisci(capitoloSpesa.getCodice(), capitoloSpesa.getDescrizione(), capitoloSpesa.getAnno(), 
							capitoloSpesa.getFlagSentenza(), con);
				}
				//fai insert su associazione con aoo
				capitoloSpesaDAO.inserisciAssociazioniAoo(Arrays.asList(capitoloSpesa.getCodice()), idAoo, con);
				
			} catch (final Exception e) {
				LOGGER.error("Si è verificato un errore durante l'inserimento di un nuovo capitolo di spesa con codice [" 
						+ capitoloSpesa.getCodice() + "]", e);
				throw new RedException("Errore durante l'inserimento del capitolo di spesa");
			} finally {
				closeConnection(con);
			}
		} else {
			throw new RedException("Dati del capitolo di spesa con corretti");
		}
	}
	

	/**
	 * Esegue l'inserimento multiplo dei capitoli spesa definiti nella lista:
	 * <code> capitoliSpesa </code>. I capitoli spesa inseriti saranno validi solo
	 * per l'AOO identificata da: <code> idAoo </code>.
	 * 
	 * @see it.ibm.red.business.service.ICapitoloSpesaSRV#inserisciMultipli(java.util.List,
	 *      java.lang.Long, java.sql.Connection).
	 * @param capitoliSpesa
	 *            capitoli spesa da inserire
	 * @param idAoo
	 *            identificativo dell'Area organizzativa omogenea
	 * @param con
	 *            connession al database
	 */
	@Override
	public void inserisciMultipli(final List<CapitoloSpesaDTO> capitoliSpesa, final Long idAoo, final Connection con) {
		if (!CollectionUtils.isEmpty(capitoliSpesa)) {
			// Si estraggono i codici dei capitoli di spesa, effettuando contestualmente una validazione su ciascun capitolo
			final List<String> codiciCapitoliSpesa = capitoliSpesa.stream()
					.filter(cap -> 
						(StringUtils.isNotBlank(cap.getCodice()) && StringUtils.isNotBlank(cap.getDescrizione()) 
								&& cap.getAnno() != null && cap.getFlagSentenza() != null))
					.map(CapitoloSpesaDTO::getCodice)
					.collect(Collectors.toList());
			
			// Se il numero di codici validi da gestire (codiciCapitoliSpesa) non è uguale al numero di capitoli in input,
			// almeno uno di questi non è valido per mancanza di dati e quindi si blocca l'inserimento massivo
			if (!CollectionUtils.isEmpty(codiciCapitoliSpesa) && codiciCapitoliSpesa.size() == capitoliSpesa.size()) {
				try {
					
					// Si eliminano le associazioni attuali tra capitoli di spesa in input e AOO
					capitoloSpesaDAO.eliminaAssociazioniAoo(codiciCapitoliSpesa, idAoo, con);
					
					for (final CapitoloSpesaDTO capitoloSpesa: capitoliSpesa) {
						final CapitoloSpesaDTO capitoloDB = capitoloSpesaDAO.getByCodice(capitoloSpesa.getCodice(), con);
						if (capitoloDB == null) {
							// Si inserisce il nuovo capitolo di spesa
							capitoloSpesaDAO.inserisci(capitoloSpesa.getCodice(), capitoloSpesa.getDescrizione(), capitoloSpesa.getAnno(), 
									capitoloSpesa.getFlagSentenza(), con);
						} else {
							//Si aggiorna il capitolo di spesa
							capitoloSpesaDAO.aggiorna(capitoloSpesa.getCodice(), capitoloSpesa.getDescrizione(), capitoloSpesa.getAnno(), 
									capitoloSpesa.getFlagSentenza(), con);
						}
					}
					
					// Si inseriscono le nuove associazioni tra capitoli di spesa e AOO
					capitoloSpesaDAO.inserisciAssociazioniAoo(codiciCapitoliSpesa, idAoo, con);
					
				} catch (final Exception e) {
					LOGGER.error("Si è verificato un errore durante l'inserimento massivo dei nuovi capitoli di spesa", e);
					throw new RedException("Errore durante l'inserimento massivo dei nuovi capitoli di spesa");
				}
			} else {
				throw new RedException("Uno o più capitoli di spesa hanno informazioni mancanti");
			}
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICapitoloSpesaFacadeSRV#ricerca(it.ibm.red.business.dto.RicercaCapitoloSpesaDTO).
	 */
	@Override
	public Collection<CapitoloSpesaDTO> ricerca(final RicercaCapitoloSpesaDTO ricercaDTO) {
		Collection<CapitoloSpesaDTO> out = null;
		Connection con = null;
		try {
			
			con = setupConnection(getDataSource().getConnection(), false);
			out = capitoloSpesaDAO.ricerca(con, ricercaDTO);
			
		} catch (final Exception e) {
			LOGGER.error("Errore nella ricerca dei capitoli di spesa" + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		return out;
	}
	
}