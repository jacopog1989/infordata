package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IInApprovazioneWidgetDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * DAO in approvazione widget.
 */
@Repository
public class InApprovazioneWidgetDAO  extends AbstractDAO implements IInApprovazioneWidgetDAO  {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = 6363972869315059763L;

    /**
     * Logger.
     */
	private static final REDLogger LOGGER = REDLogger.getLogger(InApprovazioneWidgetDAO.class.getName());

	/**
	 * @see it.ibm.red.business.dao.IInApprovazioneWidgetDAO#getIdNodiFromIdUtenteandIdAOO(java.lang.Long,
	 *      java.lang.Long, boolean, java.sql.Connection).
	 */
	@Override
	public List<Long> getIdNodiFromIdUtenteandIdAOO(final Long idUtente, final Long idAOO, final boolean flag, final Connection connection) {
		final List<Long> idNodi = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String andCondition = null; 
		try {
			String querySQL = "SELECT DISTINCT n.idnodo," + "n.descrizione," + "norg.ordinamento " 
				+ "FROM nodoutenteruolo nur INNER JOIN nodo n ON n.idnodo = nur.idnodo " 
				+ "INNER JOIN ruolo ruo ON ruo.idruolo = nur.idruolo " 
				+ "INNER JOIN utente u ON u.idutente = nur.idutente " 
				+ "INNER JOIN aoo aoo ON aoo.idaoo = n.idaoo " 
				+ "INNER JOIN nodo_organigramma norg ON n.idnodo = norg.idnodo_org " 
				+ "AND nur.datadisattivazione IS NULL AND n.datadisattivazione IS NULL " 
				+ "AND u.datadisattivazione IS NULL AND ruo.datadisattivazione IS NULL " 
				+ "AND aoo.datadisattivazione IS NULL ";
			
					if (flag) {
						andCondition = "WHERE u.idutente = ? AND aoo.idaoo = ? " 
								+ "ORDER BY norg.ordinamento ";
					} else {
						andCondition = "WHERE n.idnodopadre = ? AND aoo.idaoo = ? " 
								+ "ORDER BY norg.ordinamento ";
					}
					
			querySQL += andCondition;
			ps = connection.prepareStatement(querySQL);
			ps.setLong(1, idUtente);
			ps.setLong(2, idAOO);
			rs = ps.executeQuery();
				while (rs.next()) {
					idNodi.add(rs.getLong("IDNODO"));
				}
			return idNodi;
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Errore durante l'estrazione", e);
		} finally {
			closeStatement(ps, rs);
		} 
	}
}
