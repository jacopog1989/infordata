package it.ibm.red.business.enums;

/**
 * Enum che definisce i tipi di controllo.
 */
public enum TipoControlloEnum {
	
	/**
	 * Valore P.
	 */
	PREVENTIVO("P", "PREVENTIVO"),
	
	/**
	 * Valore S.
	 */
	SUCCESSIVO("S", "SUCCESSIVO"),
	
	/**
	 * Valore C.
	 */
	CONCOMITANTE("C", "CONCOMITANTE");

	/**
	 * Codice.
	 */
	private String code;
	
	/**
	 * Descrizione.
	 */
	private String description;

	/**
	 * Costruttore di default.
	 * @param code
	 * @param description
	 */
	TipoControlloEnum(final String code, final String description) {
		this.code = code;
		this.description = description;
	}
	
	/**
	 * @return code
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Restituisce il valore dell'enum corrispondente al codic in input.
	 * @param inCode
	 * @return tipoControlloEnum
	 */
	public static TipoControlloEnum get(final String inCode) {
		TipoControlloEnum output = null;
		
		for (final TipoControlloEnum controllo: TipoControlloEnum.values()) {
			if (controllo.getCode().equalsIgnoreCase(inCode)) {
				output = controllo;
				break;
			}
		}
		return output;
	}
}
