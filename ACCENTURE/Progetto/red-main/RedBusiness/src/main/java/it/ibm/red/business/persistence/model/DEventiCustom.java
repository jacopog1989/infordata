package it.ibm.red.business.persistence.model;

import java.util.Date;

/**
 * Corrisponde alla tabella D_EVENTI_CUSTOM del database FILENET.
 */
public class DEventiCustom {
	
	/**
	 * Identificativo.
	 */
	private Integer id;
	
	/**
	 * Identificativo aoo.
	 */
	private Integer idAoo;
	
	/**
	 * Identificativo documento.
	 */
	private Integer idDocumento;
	/**
	 * UfficioAssegnante.
	 */
	private Long idNodoMittente;
	/**
	 * utenteAssegnante.
	 */
	private Long idUtenteMittente;
	/**
	 * ufficioAssegnatario.
	 */
	private Long idNodoDestinatario;
	/**
	 * utenteAssegnatario.
	 */
	private Long idUtenteDestinatario;
	
	/**
	 * Data assegnazione.
	 */
	private Date dataAssegnazione;
	
	/**
	 * Tipo evento.
	 */
	private Integer tipoEvento;
	
	/**
	 * Motivazione assegnazione.
	 */
	private String motivazioneAssegnazione;
	
	/**
	 * Tipologia assegnazione.
	 */
	private Integer idTipoAssegnazione;
	
	/**
	 * Orario operazione.
	 */
	private Date timestampOperazione;
	
	/**
	 * Anno.
	 */
	private Integer anno;
	
	//Missing 2 raw column to evade sqlException or unefficency
	/**
	 * Costruttore vuoto.
	 */
	public DEventiCustom() {
	}

	/**
	 * Costruttore completo.
	 * @param id
	 * @param idAoo
	 * @param idDocumento
	 * @param idNodoMittente
	 * @param idUtenteMittente
	 * @param idNodoDestinatario
	 * @param idUtenteDestinatario
	 * @param dataAssegnazione
	 * @param tipoEvento
	 * @param motivazioneAssegnazione
	 * @param idTipoAssegnazione
	 * @param timestampOperazione
	 * @param anno
	 */
	public DEventiCustom(final Integer id, final Integer idAoo, final Integer idDocumento, final Long idNodoMittente, final Long idUtenteMittente,
			final Long idNodoDestinatario, final Long idUtenteDestinatario, final Date dataAssegnazione, final Integer tipoEvento,
			final String motivazioneAssegnazione, final Integer idTipoAssegnazione, final Date timestampOperazione, final Integer anno) {
		super();
		this.id = id;
		this.idAoo = idAoo;
		this.idDocumento = idDocumento;
		this.idNodoMittente = idNodoMittente;
		this.idUtenteMittente = idUtenteMittente;
		this.idNodoDestinatario = idNodoDestinatario;
		this.idUtenteDestinatario = idUtenteDestinatario;
		this.dataAssegnazione = dataAssegnazione;
		this.tipoEvento = tipoEvento;
		this.motivazioneAssegnazione = motivazioneAssegnazione;
		this.idTipoAssegnazione = idTipoAssegnazione;
		this.timestampOperazione = timestampOperazione;
		this.anno = anno;
	}

	/**
	 * Restituisce l'id.
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Imposta l'id.
	 * @param id
	 */
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * Restituisce l'id dell'Area Organizzativa omogenea.
	 * @return id AOO
	 */
	public Integer getIdAoo() {
		return idAoo;
	}

	/**
	 * Imposta l'id dell'Area Organizzativa Omogenea.
	 * @param idAoo
	 */
	public void setIdAoo(final Integer idAoo) {
		this.idAoo = idAoo;
	}

	/**
	 * Restituisce l'id del documento.
	 * @return id documento
	 */
	public Integer getIdDocumento() {
		return idDocumento;
	}

	/**
	 * Imposta l'id del documento.
	 * @param idDocumento
	 */
	public void setIdDocumento(final Integer idDocumento) {
		this.idDocumento = idDocumento;
	}

	/**
	 * Restituisce l'id del nodo mittente.
	 * @return id nodo mittente
	 */
	public Long getIdNodoMittente() {
		return idNodoMittente;
	}

	/**
	 * Imposta l'id del nodo mittente.
	 * @param idNodoMittente
	 */
	public void setIdNodoMittente(final Long idNodoMittente) {
		this.idNodoMittente = idNodoMittente;
	}

	/**
	 * Restituisce l'id dell'utente mittente.
	 * @return id utente mittente
	 */
	public Long getIdUtenteMittente() {
		return idUtenteMittente;
	}

	/**
	 * Imposta l'id dell'utente mittente.
	 * @param idUtenteMittente
	 */
	public void setIdUtenteMittente(final Long idUtenteMittente) {
		this.idUtenteMittente = idUtenteMittente;
	}

	/**
	 * Restituisce l'id del nodo destinatario.
	 * @return id nodo destinatario
	 */
	public Long getIdNodoDestinatario() {
		return idNodoDestinatario;
	}

	/**
	 * Imposta l'id del nodo destinatario.
	 * @param idNodoDestinatario
	 */
	public void setIdNodoDestinatario(final Long idNodoDestinatario) {
		this.idNodoDestinatario = idNodoDestinatario;
	}

	/**
	 * Restituisce l'id dell'utente destinatario.
	 * @return id utente destinatario
	 */
	public Long getIdUtenteDestinatario() {
		return idUtenteDestinatario;
	}

	/**
	 * Imposta l'id dell'utente destinatario.
	 * @param idUtenteDestinatario
	 */
	public void setIdUtenteDestinatario(final Long idUtenteDestinatario) {
		this.idUtenteDestinatario = idUtenteDestinatario;
	}

	/**
	 * Restituisce la data di assegnazione.
	 * @return data assegnazione
	 */
	public Date getDataAssegnazione() {
		return dataAssegnazione;
	}

	/**
	 * Imposta la data di assegnazione.
	 * @param dataAssegnazione
	 */
	public void setDataAssegnazione(final Date dataAssegnazione) {
		this.dataAssegnazione = dataAssegnazione;
	}

	/**
	 * Restituisce il tipo evento identificato dall'intero.
	 * @return tipo evento come Integer
	 */
	public Integer getTipoEvento() {
		return tipoEvento;
	}

	/**
	 * Imposta il tipo evento.
	 * @param tipoEvento
	 */
	public void setTipoEvento(final Integer tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	/**
	 * Restituisce la motivazione dell'assegnazione.
	 * @return motivazione assegnazione
	 */
	public String getMotivazioneAssegnazione() {
		return motivazioneAssegnazione;
	}

	/**
	 * Imposta la motivazione assegnazione.
	 * @param motivazioneAssegnazione
	 */
	public void setMotivazioneAssegnazione(final String motivazioneAssegnazione) {
		this.motivazioneAssegnazione = motivazioneAssegnazione;
	}

	/**
	 * Restituisce l'id del tipo assegnazione.
	 * @return id tipo assegnazione
	 */
	public Integer getIdTipoAssegnazione() {
		return idTipoAssegnazione;
	}

	/**
	 * Imposta l'id del tipo assegnazione.
	 * @param idTipoAssegnazione
	 */
	public void setIdTipoAssegnazione(final Integer idTipoAssegnazione) {
		this.idTipoAssegnazione = idTipoAssegnazione;
	}

	/**
	 * Restituisce il timestamp dell'operazione.
	 * @return timestamp operazione
	 */
	public Date getTimestampOperazione() {
		return timestampOperazione;
	}

	/**
	 * Imposta il timestamp dell'operazione.
	 * @param timestampOperazione
	 */
	public void setTimestampOperazione(final Date timestampOperazione) {
		this.timestampOperazione = timestampOperazione;
	}

	/**
	 * Restituisce l'anno.
	 * @return anno
	 */
	public Integer getAnno() {
		return anno;
	}

	/**
	 * Imposta l'anno.
	 * @param anno
	 */
	public void setAnno(final Integer anno) {
		this.anno = anno;
	}
	
}
