package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IProtocollaFlussoFacadeSRV;

/**
 * Interfaccia del servizio di protocollazione flusso.
 */
public interface IProtocollaFlussoSRV extends IProtocollaFlussoFacadeSRV {

}
