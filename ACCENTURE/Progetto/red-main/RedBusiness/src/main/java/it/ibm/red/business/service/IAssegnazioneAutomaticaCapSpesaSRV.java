/**
 * 
 */
package it.ibm.red.business.service;

import java.sql.Connection;

import it.ibm.red.business.dto.AssegnazioneAutomaticaCapSpesaDTO;
import it.ibm.red.business.service.facade.IAssegnazioneAutomaticaCapSpesaFacadeSRV;

/**
 * The Interface IAssegnazioneAutomaticaSRV.
 *
 * @author mcrescentini
 * 
 *         Interfaccia servizio per la gestione delle assegnazioni automatiche dei capitoli di spesa.
 */
public interface IAssegnazioneAutomaticaCapSpesaSRV extends IAssegnazioneAutomaticaCapSpesaFacadeSRV {

	/**
	 * 
	 * @param codiceCapitoloSpesa
	 * @param idAoo
	 * @param connection
	 * @return
	 */
	AssegnazioneAutomaticaCapSpesaDTO get(String codiceCapitoloSpesa, Long idAoo, Connection connection);
	
}