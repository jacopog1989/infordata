package it.ibm.red.business.dto;

import java.util.List;

/**
 * The Class EsitoOperazioneDTO.
 *
 * @author CPIERASC
 * 
 * 	DTO esito operazione.
 */
public class EsitoOperazioneDTO extends EsitoDTO {

    /**
	 * Messaggio.
	 */
	public static final String MESSAGGIO_ESITO_OK = "Operazione effettuata con successo.";
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 5899907999995977843L;
	
	/**
	 * Anno protocollo.
	 */
	private Integer annoProtocollo;

	/**
	 * Identificativo documento.
	 */
	private Integer idDocumento;
	
	/**
	 * Document title.
	 */
	private String documentTitle;

	/**
	 * Numero protocollo.
	 */
	private Integer numeroProtocollo;

	/**
	 * Wob number.
	 */
	private String wobNumber;
	
	/**
	 * Nome del documento.
	 */
	private String nomeDocumento;
	
	/**
	 * Collezione di esiti degli allegati.
	 */
	private List<EsitoOperazioneDTO> esitiAllegati;

	/**
	 * Flag allegato.
	 */
	private boolean isAllegato;
	
	/**
	 * Flag registro repertorio.
	 */
	private boolean isRegistroRepertorio;
	
	/**
	 * Costruttore.
	 * 
	 * @param inWobNumber	wob number
	 */
	public EsitoOperazioneDTO(final String inWobNumber) {
		super();
		this.wobNumber = inWobNumber;
	}
	
	/**
	 * Costruttore.
	 * @param docTitle document title
	 */
	
	/**
	 * Costruttore.
	 * 
	 * @param inWobNumber		wob number
	 * @param inDocumentTitle	document title
	 */
	public EsitoOperazioneDTO(final String inWobNumber, final String documentTitle) {
		super();
		this.wobNumber = inWobNumber;
		this.documentTitle = documentTitle;
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param idDocumento	ID documento
	 */
	public EsitoOperazioneDTO(final Integer idDocumento) {
		super();
		this.idDocumento = idDocumento;
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param inWobNumber	numero wob
	 * @param esito			oggetto esito
	 */
	public EsitoOperazioneDTO(final String inWobNumber, final EsitoDTO esito) {
		this(inWobNumber);
		setEsito(esito.isEsito());
		setCodiceErrore(esito.getCodiceErrore());
		setNote(esito.getNote());
	}


	/**
	 * Costruttore.
	 * 
	 * @param inNumeroProtocollo	numero protocollo
	 * @param inAnnoProtocollo		anno protocollo
	 * @param inIdDocumento			identificativo documento
	 * @param inWobNumber			wob number
	 */
	public EsitoOperazioneDTO(final Integer inNumeroProtocollo, final Integer inAnnoProtocollo, final Integer inIdDocumento, final String inWobNumber) {
		this(inNumeroProtocollo, inAnnoProtocollo, inIdDocumento, inWobNumber, true, null);
	}
	
	/**
	 * Costruttore con gestione manuale dell'esito.
	 * 
	 * @param inNumeroProtocollo	numero protocollo
	 * @param inAnnoProtocollo		anno protocollo
	 * @param inIdDocumento			identificativo documento
	 * @param inWobNumber			wob number
	 */
	public EsitoOperazioneDTO(final String inWobNumber, final Boolean esito, final String descError) {
		this(null, null, null, inWobNumber, esito, descError);
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param inNumeroProtocollo	numero protocollo
	 * @param inAnnoProtocollo		anno protocollo
	 * @param inIdDocumento			identificativo documento
	 * @param inWobNumber			wob number
	 * @param inFlagEsito			flag esito
	 * @param inDescErrore			descrizione errore
	 */
	private EsitoOperazioneDTO(final Integer inNumeroProtocollo, final Integer inAnnoProtocollo, final Integer inIdDocumento, final String inWobNumber, 
			final Boolean inFlagEsito, final String inDescErrore) {
		super();
		setEsito(inFlagEsito);
		setNote(inDescErrore);
		this.numeroProtocollo = inNumeroProtocollo;
		this.annoProtocollo = inAnnoProtocollo;
		this.idDocumento = inIdDocumento;
		this.wobNumber = inWobNumber;
	}

	/**
	 * Getter.
	 * 
	 * @return	anno protocollo
	 */
	public final Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * Getter.
	 * 
	 * @return	flag esito
	 */
	public final Boolean getFlagEsito() {
		return isEsito();
	}
	
	/**
	 * Getter.
	 * 
	 * @return	identificativo documento
	 */
	public final Integer getIdDocumento() {
		return idDocumento;
	}

	/**
	 * Getter.
	 * 
	 * @return	numero protocollo
	 */
	public final Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Getter.
	 * 
	 * @return	wob number
	 */
	public final String getWobNumber() {
		return wobNumber;
	}
	
	/**
	 * Setter.
	 * 
	 * @param inAnnoProtocollo	anno protocollo
	 */
	public final void setAnnoProtocollo(final Integer inAnnoProtocollo) {
		this.annoProtocollo = inAnnoProtocollo;
	}

	/**
	 * Setter.
	 * 
	 * @param inIdDocumento	identificativo documento
	 */
	public final void setIdDocumento(final Integer inIdDocumento) {
		this.idDocumento = inIdDocumento;
	}

	/**
	 * @return the documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * @param documentTitle the documentTitle to set
	 */
	public void setDocumentTitle(String documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * Setter.
	 * 
	 * @param inNumeroProtocollo	numero protocollo
	 */
	public final void setNumeroProtocollo(final Integer inNumeroProtocollo) {
		this.numeroProtocollo = inNumeroProtocollo;
	}

	/**
	 * Recupera il nome del documento.
	 * @return nome del documento
	 */
	public String getNomeDocumento() {
		return nomeDocumento;
	}

	/**
	 * Imposta il nome del documento.
	 * @param nomeDocumento nome del documento
	 */
	public void setNomeDocumento(final String nomeDocumento) {
		this.nomeDocumento = nomeDocumento;
	}
	
	/**
	 * Recupera gli esiti degli allegati.
	 * @return lista degli esiti degli allegati
	 */
	public List<EsitoOperazioneDTO> getEsitiAllegati() {
		return esitiAllegati;
	}
	
	/**
	 * Imposta gli esiti degli allegati.
	 * @param esitiAllegati lista degli esiti degli allegati
	 */
	public void setEsitiAllegati(final List<EsitoOperazioneDTO> esitiAllegati) {
		this.esitiAllegati = esitiAllegati;
	}

	/**
	 * Imposta l'attributo isAllegato.
	 * @return isAllegato
	 */
	public boolean isAllegato() {
		return isAllegato;
	}

	/**
	 * Recupera l'attributo isAllegato
	 * @param isAllegato
	 */
	public void setAllegato(final boolean isAllegato) {
		this.isAllegato = isAllegato;
	}

	/**
	 * @return the isRegistroRepertorio
	 */
	public boolean isRegistroRepertorio() {
		return isRegistroRepertorio;
	}

	/**
	 * @param isRegistroRepertorio the isRegistroRepertorio to set
	 */
	public void setRegistroRepertorio(final boolean isRegistroRepertorio) {
		this.isRegistroRepertorio = isRegistroRepertorio;
	}
	
}