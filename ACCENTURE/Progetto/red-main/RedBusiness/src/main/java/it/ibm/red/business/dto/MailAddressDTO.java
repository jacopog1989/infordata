package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * DTO che definisce un indrizzo mail.
 */
public class MailAddressDTO implements Serializable {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1604457227191473746L;

	/**
	 * Utente.
	 */
	private int idUtente;

	/**
	 * Mail
	 */
	private int idMail;

	/**
	 * Indirizzo.
	 */
	private String address;

	/**
	 * Costruttore vuoto.
	 */
	public MailAddressDTO() {

	}

	/**
	 * Costruttore completo.
	 * @param idUtente
	 * @param idMail
	 * @param address
	 */
	public MailAddressDTO(final int idUtente, final int idMail, final String address) {
		this.idUtente = idUtente;
		this.idMail = idMail;
		this.address = address;
	}

	/**
	 * Restituisce l'id dell'utente.
	 * @return id utente
	 */
	public int getIdUtente() {
		return idUtente;
	}

	/**
	 * Imposta l'id utente.
	 * @param idUtente
	 */
	public void setIdUtente(final int idUtente) {
		this.idUtente = idUtente;
	}

	/**
	 * Restitusce l'id della mail.
	 * @return id mail
	 */
	public int getIdMail() {
		return idMail;
	}

	/**
	 * Imposta l'id della mail.
	 * @param idMail
	 */
	public void setIdMail(final int idMail) {
		this.idMail = idMail;
	}

	/**
	 * Restituisce l'indirizzo mail.
	 * @return address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Imposta l'indirizzo.
	 * @param address
	 */
	public void setAddress(final String address) {
		this.address = address;
	}

	/**
	 * @see java.lang.Object#toString().
	 */
	@Override
	public String toString() {
		return "[" + address + "]";
	}
	
}
