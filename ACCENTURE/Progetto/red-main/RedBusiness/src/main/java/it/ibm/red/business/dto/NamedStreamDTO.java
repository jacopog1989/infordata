package it.ibm.red.business.dto;

import org.apache.commons.io.FilenameUtils;

import it.ibm.red.business.persistence.model.TipoFile;

/**
 * Classe NamedStreamDTO.
 */
public class NamedStreamDTO extends AbstractDTO {
	
	/** 
	 * The Constant serialVersionUID. 
	 */
	private static final long serialVersionUID = 6247477389024046830L;

	/**
	 * The file name from which the stream has been collected
	 */
	private String name;
	
	/**
	 * The stream of the file
	 */
	private byte[] content;
	
	/**
	 * MymeType del namedStream viene individuato tramite il nome del file
	 */
	private TipoFile tipoFile;
	
	/**
	 * The validity of file sing(s)
	 */
	private Boolean signValidity;
	
	/** 
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/** 
	 *
	 * @param name the new name
	 */
	public void setName(final String name) {
		this.name = name;
	}
	
	/** 
	 *
	 * @return the content
	 */
	public byte[] getContent() {
		return content;
	}
	
	/** 
	 *
	 * @param content the new content
	 */
	public void setContent(final byte[] content) {
		this.content = content;
	}

	/** 
	 *
	 * @return the tipo file
	 */
	public TipoFile getTipoFile() {
		return tipoFile;
	}

	/** 
	 *
	 * @param mymeType the new tipo file
	 */
	public void setTipoFile(final TipoFile mymeType) {
		this.tipoFile = mymeType;
	}
	
	/**
	 * Ritorna solo il nome del file.
	 * Se il nome del file è preceduto da un percorso taglia il percorso per ritornare solo il nome del file.
	 * 
	 * @return
	 * il nome del file.
	 */
	public String getFileName() {
		return FilenameUtils.getName(name);
	}

	/** 
	 *
	 * @return the sign validity
	 */
	public Boolean getSignValidity() {
		return signValidity;
	}

	/**  
	 *
	 * @param signValidity the new sign validity
	 */
	public void setSignValidity(final Boolean signValidity) {
		this.signValidity = signValidity;
	}
}
