package it.ibm.red.business.dao.impl;

import java.io.Reader;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IAssegnazioneAutomaticaMetadatiDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dto.AssegnatarioDTO;
import it.ibm.red.business.dto.AssegnazioneMetadatiDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.metadatiestesi.MetadatiEstesiHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.TipoProcedimento;

/**
 * DAO che gestisce le assegnazioni automatiche per tipologia documento,
 * tipologia procedimento e metadati.
 * 
 * @author SimoneLungarella
 */
@Repository
public class AssegnazioneAutomaticaMetadatiDAO extends AbstractDAO implements IAssegnazioneAutomaticaMetadatiDAO {

	private static final String IDUTENTEASS = "IDUTENTEASS";

	private static final String IDNODOASS = "IDNODOASS";

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 7117748354728045018L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AssegnazioneAutomaticaMetadatiDAO.class.getName());
	
	/**
	 * Tipo procedimento DAO.
	 */
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;

	/**
	 * @see it.ibm.red.business.dao.IAssegnazioneAutomaticaMetadatiDAO#getByIdAoo(java.sql.Connection,
	 *      java.lang.Long).
	 */
	@Override
	public List<AssegnazioneMetadatiDTO> getByIdAoo(final Connection connection, final Long idAoo) {
		final List<AssegnazioneMetadatiDTO> output = new ArrayList<>();
		AssegnazioneMetadatiDTO assegnazione;
		PreparedStatement ps = null;
		ResultSet rs = null;
		TipologiaDocumentoDTO tipoDocumento;
		AssegnatarioDTO assegnatario;
		TipoProcedimentoDTO tipoProcedimento;
		MetadatoDTO metadato;
		
		try {
			final String query = "SELECT ass.idassmeta, ass.idnodoass, ass.idutenteass, ass.idtipodoc, ass.idtipoproc, ass.metadato, doc.descrizione as TIPODOC, ufficio.descrizione as UFFICIO, utente.nome as NOMEUTENTE, utente.cognome as COGNOMEUTENTE " 
					+ "FROM ASSEGNAZIONE_METADATI ass LEFT JOIN UTENTE utente ON (utente.IDUTENTE = ass.IDUTENTEASS), TIPOLOGIADOCUMENTO doc, NODO ufficio " 
					+ "WHERE doc.IDTIPOLOGIADOCUMENTO = ass.IDTIPODOC AND ufficio.IDNODO = ass.IDNODOASS AND ass.IDAOO = ?";
			ps = connection.prepareStatement(query);
			ps.setLong(1, idAoo);
			rs = ps.executeQuery();
			StringBuilder metadatoXml = new StringBuilder();
			while (rs.next()) {
				assegnatario = new AssegnatarioDTO(rs.getLong(IDNODOASS), rs.getLong(IDUTENTEASS));
				tipoDocumento = new TipologiaDocumentoDTO();
				tipoProcedimento = new TipoProcedimentoDTO();
				metadato = new MetadatoDTO();
				
				assegnazione = new AssegnazioneMetadatiDTO();
				
				Clob clob = null;
				clob = rs.getClob("METADATO");
				metadatoXml = new StringBuilder();
				if (clob != null) {
					final Reader reader = clob.getCharacterStream();
					final char[] buffer = new char[(int) clob.length()];
					while (reader.read(buffer) != -1) {
						metadatoXml.append(buffer);
					}
				}
				tipoDocumento.setIdTipologiaDocumento(rs.getInt("IDTIPODOC"));
				tipoDocumento.setDescrizione(rs.getString("TIPODOC"));
				
				final Integer idTipoProc = rs.getInt("IDTIPOPROC");
				if (idTipoProc != 0) {
					final TipoProcedimento tipo = tipoProcedimentoDAO.getTPbyId(connection, idTipoProc);
					tipoProcedimento.setTipoProcedimentoId(idTipoProc);
					tipoProcedimento.setDescrizione(tipo.getDescrizione());
				}
				
				metadato = MetadatiEstesiHelper.deserializeMetadatoLight(metadatoXml.toString());
				
				// Definizione descrizione utente
				StringBuilder descrizioneUtente = new StringBuilder("");
				if (rs.getString("NOMEUTENTE") != null) {
					descrizioneUtente = new StringBuilder(rs.getString("NOMEUTENTE")).append(" ");
					
					if (rs.getString("COGNOMEUTENTE") != null) {
						descrizioneUtente.append(rs.getString("COGNOMEUTENTE"));
					}
				}
				
				assegnatario.setDescrizioneUtente(descrizioneUtente.toString());
				assegnatario.setDescrizioneUfficio(rs.getString("UFFICIO"));
				assegnazione.setAssegnatario(assegnatario);
				
				assegnazione.setMetadato(metadato);
				assegnazione.setTipoDoc(tipoDocumento);
				assegnazione.setTipoProc(tipoProcedimento);
				assegnazione.setId(rs.getInt("IDASSMETA"));
				
				output.add(assegnazione);
			}
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero delle assegnazioni automatiche dei metadati - idAoo: " + idAoo, e);
			throw new RedException("Errore durante il recupero delle assegnazioni automatiche dei metadati - idAoo: " + idAoo, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IAssegnazioneAutomaticaMetadatiDAO#deleteByIdAoo(java.sql.Connection,
	 *      java.lang.Long).
	 */
	@Override
	public void deleteByIdAoo(final Connection connection, final Long idAoo) {
		PreparedStatement ps = null;
		final ResultSet rs = null;
		
		try {
			ps = connection.prepareStatement("DELETE FROM ASSEGNAZIONE_METADATI WHERE IDAOO = ?");
			ps.setLong(1, idAoo);
			
			ps.executeUpdate();
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante la cancellazione delle assegnazioni automatiche dei metadati dell'AOO con id: " + idAoo, e);
			throw new RedException("Errore durante la cancellazione delle assegnazioni automatiche dei metadati dell'AOO con id: " + idAoo, e);
		} finally {
			closeStatement(ps, rs);
		}
		
	}

	/**
	 * @see it.ibm.red.business.dao.IAssegnazioneAutomaticaMetadatiDAO#registraAssegnazioneMetadato(java.sql.Connection,
	 *      it.ibm.red.business.dto.AssegnazioneMetadatiDTO, java.lang.Long).
	 */
	@Override
	public void registraAssegnazioneMetadato(final Connection connection, final AssegnazioneMetadatiDTO assegnazione, final Long idAoo) {
		PreparedStatement ps = null;
		final ResultSet rs = null;
		int index = 1;
		
		try {
			ps = connection.prepareStatement("INSERT INTO ASSEGNAZIONE_METADATI(IDASSMETA, IDTIPODOC, IDTIPOPROC, METADATO, IDAOO, IDNODOASS, IDUTENTEASS) "
					+ "VALUES (SEQ_ASS_META.NEXTVAL, ?, ?, ?, ?, ?, ?)");
			
			ps.setInt(index++, assegnazione.getTipoDoc().getIdTipologiaDocumento());
			
			if (assegnazione.getTipoProc().getTipoProcedimentoId() != -1) {
				ps.setLong(index++, assegnazione.getTipoProc().getTipoProcedimentoId());
			} else {
				ps.setNull(index++, java.sql.Types.NUMERIC);
			}
			
			if (assegnazione.getMetadato() != null) {
				final Clob clob = connection.createClob();
				clob.setString(1L, MetadatiEstesiHelper.serializeMetadato(assegnazione.getMetadato()));
				ps.setClob(index++, clob);
			} else {
				ps.setNull(index++, java.sql.Types.CLOB);
			}
			
			ps.setLong(index++, idAoo);
			ps.setLong(index++, assegnazione.getAssegnatario().getIdUfficio());
			if (assegnazione.getAssegnatario().getIdUtente() != null) {
				ps.setLong(index++, assegnazione.getAssegnatario().getIdUtente());
			} else {
				ps.setNull(index++, Types.NUMERIC);
			}
			
			ps.executeUpdate();
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento dell'assegnazione automatica dei metadati", e);
			throw new RedException("Errore durante l'inserimento dell'assegnazione automatica dei metadati", e);
		} finally {
			closeStatement(ps, rs);
		}
		
	}
	
	/**
	 * @see it.ibm.red.business.dao.IAssegnazioneAutomaticaMetadatiDAO#deleteByIdAssegnazione(java.sql.Connection,
	 *      java.lang.Integer).
	 */
	@Override
	public void deleteByIdAssegnazione(final Connection connection, final Integer idAssegnazione) {
		PreparedStatement ps = null;
		
		try {
			ps = connection.prepareStatement("DELETE FROM ASSEGNAZIONE_METADATI WHERE IDASSMETA = ?");
			ps.setLong(1, idAssegnazione);
			
			ps.executeUpdate();
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante la cancellazione dell'assegnazione automatica dei metadati con id: " + idAssegnazione, e);
			throw new RedException("Errore durante la cancellazione dell'assegnazione automatica dei metadati con id: " + idAssegnazione, e);
		} finally {
			closeStatement(ps);
		}
		
	}

	/**
	 * @see it.ibm.red.business.dao.IAssegnazioneAutomaticaMetadatiDAO#getAssegnazioniMetadati(java.sql.Connection,
	 *      java.lang.Long, java.lang.Long, java.lang.Long).
	 */
	@Override
	public List<AssegnazioneMetadatiDTO> getAssegnazioniMetadati(final Connection connection, final Long idAoo, final Long idTipoDocumento, final Long idTipoProcedimento) {
		final List<AssegnazioneMetadatiDTO> assegnazioni = new ArrayList<>();
		AssegnazioneMetadatiDTO assegnazione = new AssegnazioneMetadatiDTO();
		PreparedStatement ps = null;
		ResultSet rs = null;
		MetadatoDTO metadato = new MetadatoDTO();
		StringBuilder sb = new StringBuilder();
		int index = 1;
		try {
			ps = connection.prepareStatement(
					"SELECT IDNODOASS, IDUTENTEASS, METADATO FROM ASSEGNAZIONE_METADATI WHERE IDTIPODOC = ? AND IDTIPOPROC = ? AND IDAOO = ? ORDER BY IDASSMETA");
			
			ps.setLong(index++, idTipoDocumento);
			ps.setLong(index++, idTipoProcedimento);
			ps.setLong(index++, idAoo);
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				assegnazione = new AssegnazioneMetadatiDTO();
				// Recupero metadati serializzati in CLOB
				Clob clob = null;
				clob = rs.getClob("METADATO");
				if (clob != null) {
					sb = new StringBuilder();
					final Reader reader = clob.getCharacterStream();
					final char[] buffer = new char[(int) clob.length()];
					while (reader.read(buffer) != -1) {
						sb.append(buffer);
					}
				}
				metadato = MetadatiEstesiHelper.deserializeMetadatoLight(sb.toString());
				assegnazione.setMetadato(metadato);
				assegnazione.getAssegnatario().setIdUfficio(rs.getLong(IDNODOASS));
				assegnazione.getAssegnatario().setIdUtente(rs.getLong(IDUTENTEASS));
				
				assegnazioni.add(assegnazione);
			}
		
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'assegnazione automatica dei metadati del documento con id: " + idTipoDocumento, e);
			throw new RedException("Errore durante il recupero dell'assegnazione automatica dei metadati del documento con id: " + idTipoDocumento, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return assegnazioni;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IAssegnazioneAutomaticaMetadatiDAO#getAssegnazioniByTipoDoc(java.lang.Long,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<AssegnazioneMetadatiDTO> getAssegnazioniByTipoDoc(final Long idAoo, final Long idTipoDocumento, final Connection connection) {
		final List<AssegnazioneMetadatiDTO> assegnazioni = new ArrayList<>();
		AssegnazioneMetadatiDTO assegnazione = new AssegnazioneMetadatiDTO();
		TipologiaDocumentoDTO tipoDocumento;
		TipoProcedimentoDTO tipoProcedimento;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		
		try {
			ps = connection.prepareStatement("SELECT IDNODOASS, IDUTENTEASS, IDTIPODOC, IDTIPOPROC FROM ASSEGNAZIONE_METADATI WHERE IDTIPODOC = ? AND IDAOO = ? AND METADATO is null ORDER BY IDASSMETA");
			
			ps.setLong(index++, idTipoDocumento);
			ps.setLong(index++, idAoo);
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				tipoDocumento = new TipologiaDocumentoDTO();
				tipoProcedimento = new TipoProcedimentoDTO();
				assegnazione = new AssegnazioneMetadatiDTO();
				
				assegnazione.getAssegnatario().setIdUfficio(rs.getLong(IDNODOASS));
				assegnazione.getAssegnatario().setIdUtente(rs.getLong(IDUTENTEASS));
				
				tipoDocumento.setIdTipologiaDocumento(rs.getInt("IDTIPODOC"));
				tipoProcedimento.setTipoProcedimentoId(rs.getInt("IDTIPOPROC"));
				
				assegnazione.setTipoDoc(tipoDocumento);
				assegnazione.setTipoProc(tipoProcedimento);
				assegnazioni.add(assegnazione);
			}
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'assegnazione automatica del documento con id: " + idTipoDocumento, e);
			throw new RedException("Errore durante il recupero dell'assegnazione automatica del documento con id: " + idTipoDocumento, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return assegnazioni;
	}

}
