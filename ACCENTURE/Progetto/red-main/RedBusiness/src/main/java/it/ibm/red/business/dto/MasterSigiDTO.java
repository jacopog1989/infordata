/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.Date;

/**
 * @author APerquoti
 *
 */
public class MasterSigiDTO extends AbstractDTO implements Comparable<MasterSigiDTO> {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 591747364919790381L;
	

	/**
	 * Id documento.
	 */
	private String idDocumento;

	/**
	 * OGgetto.
	 */
	private String oggetto;

	/**
	 * Numero protocollo.
	 */
	private Integer numeroProtocollo;

	/**
	 * Anno protocollo.
	 */
	private Integer annoProtocollo;

	/**
	 * Flag annullato.
	 */
	private boolean flagAnnullato;

	/**
	 * Motivo annullamento.
	 */
	private String motivoAnnullato;

	/**
	 * Data annullamento.
	 */
	private Date dataAnnullato;
	
	/**
	 * Costruttore di default.
	 */
	public MasterSigiDTO() {
		super();
	}
	
	/**
	 * Costruttore del DTO.
	 * @param idDocumento
	 * @param oggetto
	 * @param numeroProtocollo
	 * @param annoProtocollo
	 * @param motivoAnnullato
	 * @param dataAnnullato
	 */
	public MasterSigiDTO(final String idDocumento, final String oggetto, final Integer numeroProtocollo, final Integer annoProtocollo, final String motivoAnnullato, final Date dataAnnullato) {
		super();
		
		this.idDocumento = idDocumento;
		this.oggetto = oggetto;
		this.numeroProtocollo = numeroProtocollo;
		this.annoProtocollo = annoProtocollo;
		this.motivoAnnullato = motivoAnnullato;
		this.dataAnnullato = dataAnnullato;
		if (dataAnnullato != null) {
			flagAnnullato = true;
		}
		
	}

	/**
	 * @return the idDocumento
	 */
	public final String getIdDocumento() {
		return idDocumento;
	}

	/**
	 * @param idDocumento the idDocumento to set
	 */
	public final void setIdDocumento(final String idDocumento) {
		this.idDocumento = idDocumento;
	}

	/**
	 * @return the oggetto
	 */
	public final String getOggetto() {
		return oggetto;
	}

	/**
	 * @param oggetto the oggetto to set
	 */
	public final void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * @return the numeroProtocollo
	 */
	public final Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * @param numeroProtocollo the numeroProtocollo to set
	 */
	public final void setNumeroProtocollo(final Integer numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	/**
	 * @return the annoProtocollo
	 */
	public final Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * @param annoProtocollo the annoProtocollo to set
	 */
	public final void setAnnoProtocollo(final Integer annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/**
	 * @return the flagAnnullato
	 */
	public final boolean isFlagAnnullato() {
		return flagAnnullato;
	}

	/**
	 * @param flagAnnullato the flagAnnullato to set
	 */
	public final void setFlagAnnullato(final boolean flagAnnullato) {
		this.flagAnnullato = flagAnnullato;
	}

	/**
	 * @return the motivoAnnullato
	 */
	public final String getMotivoAnnullato() {
		return motivoAnnullato;
	}

	/**
	 * @param motivoAnnullato the motivoAnnullato to set
	 */
	public final void setMotivoAnnullato(final String motivoAnnullato) {
		this.motivoAnnullato = motivoAnnullato;
	}

	/**
	 * @return the dataAnnullato
	 */
	public final Date getDataAnnullato() {
		return dataAnnullato;
	}

	/**
	 * @param dataAnnullato the dataAnnullato to set
	 */
	public final void setDataAnnullato(final Date dataAnnullato) {
		this.dataAnnullato = dataAnnullato;
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object).
	 */
	@Override
	public int compareTo(final MasterSigiDTO o) {
		int comparison = 0;
		
		if (this.dataAnnullato != null) {
			if (o.getDataAnnullato() != null) {
				comparison = this.dataAnnullato.compareTo(o.getDataAnnullato());
			} else {
				comparison = -1;
			}
		}
			
		
		if (this.annoProtocollo != null) {
			if (o.getAnnoProtocollo() != null) {
				comparison = this.annoProtocollo.compareTo(o.getAnnoProtocollo());
			} else {
				comparison = 1;
			}
		}
		
		if (comparison == 0 && this.numeroProtocollo != null) {
			if (o.getNumeroProtocollo() != null) {
				comparison = this.numeroProtocollo.compareTo(o.getNumeroProtocollo());
			} else {
				comparison = 1;
			}
		}

		return comparison;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idDocumento == null) ? 0 : idDocumento.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MasterSigiDTO other = (MasterSigiDTO) obj;
		if (idDocumento == null) {
			if (other.idDocumento != null)
				return false;
		} else if (!idDocumento.equals(other.idDocumento))
			return false;
		return true;
	}
	
	

}
