package it.ibm.red.business.dto;

import java.util.Date;
import java.util.List;

import it.ibm.red.business.enums.EventoCalendarioEnum;
import it.ibm.red.business.enums.TipoAzioneEnum;
import it.ibm.red.business.persistence.model.Calendario;
import it.ibm.red.business.utils.StringUtils;

/**
 * DTO per la definizione di un evento.
 */
public class EventoDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -7780235124252730535L;
	
	/**
	 * Data inizio.
	 */
	private Date dataInizio;

	/**
	 * Data scadenza.
	 */
	private Date dataScadenza;

	/**
	 * Titolo.
	 */
	private String titolo;

	/**
	 * Tipologia evento.
	 */
	private EventoCalendarioEnum tipologia;

	/**
	 * Tipo azione.
	 */
	private TipoAzioneEnum tipoCal;

	/**
	 * Id evento.
	 */
	private int idEvento;

	/**
	 * Id ente.
	 */
	private int idEnte;

	/**
	 * Id Aoo.
	 */
	private Long idAoo;

	/**
	 * Id utente.
	 */
	private Long idUtente;

	/**
	 * Id Nodo.
	 */
	private Long idNodo;

	/**
	 * Id ruolo.
	 */
	private long idRuolo;

	/**
	 * Contenuto.
	 */
	private String contenuto;

	/**
	 * Link esterno.
	 */
	private String linkEsterno;

	/**
	 * Colore contenuto.
	 */
	private String coloreContenuto;

	/**
	 * Flag locked.
	 */
	private boolean isLocked;

	/**
	 * Flag pubblico.
	 */
	private boolean isPubblico;

	/**
	 * Flag urgente.
	 */
	private int urgente;

	/**
	 * Data.
	 */
	private Date dataTime;

	/**
	 * Document title.
	 */
	private String documentTitle;

	/**
	 * Numero documento.
	 */
	private Integer numeroDocumento;

	/**
	 * Id ruoli.
	 */
	private List<Long> idRuoliCategoriaTarget;

	/**
	 * Nodi target.
	 */
	private List<Integer> idNodiTarget;

	/**
	 * Descrizione nodi.
	 */
	private List<String> descrizioneNodi;
	

	/**
	 * Allegato.
	 */
	private byte[] allegato;

	/**
	 * Nome allegato.
	 */
	private String nomeAllegato;

	/**
	 * Estensione allegato.
	 */
	private String estensioneAllegato;

	/**
	 * Mime type allegato.
	 */
	private String mimeTypeAllegato;
	
	/**
	 * Id categoria documento.
	 */
	private Integer idCategoriaDocumento;

	/**
	 * flag modificabile.
	 */
	private Boolean isModificabileWdg;
	
	/**
	 * Costruttore.
	 */
	public EventoDTO() {
		super();
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param id
	 * @param dataInizio
	 *            - data di inizio
	 * @param dataScadenza
	 *            - data di scadenza
	 * @param titolo
	 * @param tipologia
	 * @param contenuto
	 * @param linkEsterno
	 *            - link esterno
	 * @param documentTitle
	 *            - document title
	 * @param nomeAllegato
	 *            - nome dell'allegato
	 * @param estensioneAllegato
	 *            - estensione dell'allegato
	 * @param mimeTypeAllegato
	 * @param idUtente
	 *            - id dell'utente
	 * @param idRuoliCategoriaTarget
	 *            - id dei ruoli categoria target
	 * @param idNodiTarget
	 *            - id dei nodi target
	 * @param descrizioneNodi
	 *            - descrizione dei nodi
	 */
	public EventoDTO(final int id, final Date dataInizio, final Date dataScadenza, final String titolo, final EventoCalendarioEnum tipologia, final String contenuto, final String linkEsterno, final String documentTitle, 
			final String nomeAllegato, final String estensioneAllegato, final String mimeTypeAllegato, final Long idUtente, final List<Long> idRuoliCategoriaTarget, final List<Integer> idNodiTarget, final List<String> descrizioneNodi) {
		super();
		this.idEvento = id;
		this.dataInizio = dataInizio;
		this.dataScadenza = dataScadenza;
		this.titolo = titolo;
		this.tipologia = tipologia;
		this.contenuto = contenuto;
		this.linkEsterno = linkEsterno;
		this.documentTitle = documentTitle;
		this.nomeAllegato = nomeAllegato;
		this.estensioneAllegato = estensioneAllegato;
		this.mimeTypeAllegato = mimeTypeAllegato;
		this.idUtente = idUtente;
		this.idRuoliCategoriaTarget = idRuoliCategoriaTarget;
		this.idNodiTarget = idNodiTarget;
		this.descrizioneNodi = descrizioneNodi;
	}
	
	/**
	 * Costruttore.
	 * @param e evento
	 */
	public EventoDTO(final EventoDTO e) {
        super();
        this.idEvento = e.idEvento;
        this.dataInizio = e.dataInizio;
        this.dataScadenza = e.dataScadenza;
        this.titolo = e.titolo;
        this.tipologia = e.tipologia;
        this.contenuto = e.contenuto;
        this.linkEsterno = e.linkEsterno;
        this.documentTitle = e.documentTitle;
    }
	
	/**
	 * Costruttore.
	 * @param calendario
	 */
	public EventoDTO(final Calendario calendario) {
		tipoCal = TipoAzioneEnum.EVENTO;
		idEvento = calendario.getIdEvento();
		dataTime = calendario.getDataTime();
		dataInizio = calendario.getDataInizio();
		dataScadenza = calendario.getDataScadenza();
		urgente = 0;
		if (calendario.getUrgente() == 1) {
			urgente = 1;
		}
		titolo = calendario.getTitolo();
		contenuto = calendario.getContenuto();
	}

	/**
	 * Costruttore.
	 * @param dataInizio - data di inizio
	 * @param dataScadenza - data di scadenza
	 * @param titolo
	 */
	public EventoDTO(final Date dataInizio, final Date dataScadenza, final String titolo) {
		super();
		this.dataInizio = dataInizio;
		this.dataScadenza = dataScadenza;
		this.titolo = titolo;
	}


	/**
	 * Recupera la data di inizio.
	 * @return data di inizio
	 */
	public Date getDataInizio() {
		return dataInizio;
	}
	
	/**
	 * Recupera la data di scadenza.
	 * @return data di scadenza
	 */
	public Date getDataScadenza() {
		return dataScadenza;
	}
	
	/**
	 * Recupera il titolo.
	 * @return titolo
	 */
	public String getTitolo() {
		return titolo;
	}
	
	/**
	 * Recupera la tipologia di evento del calendario.
	 * @return tipologia di evento del calendario
	 */
	public EventoCalendarioEnum getTipologia() {
		return tipologia;
	}
	
	/**
	 * Recupera l'id dell'evento.
	 * @return id dell'evento
	 */
	public int getIdEvento() {
		return idEvento;
	}
	
	/**
	 * Recupera l'id dell'ente.
	 * @return id dell'ente
	 */
	public int getIdEnte() {
		return idEnte;
	}
	
	/**
	 * Imposta l'id dell'evento.
	 * @param idEvento id dell'evento
	 */
	public void setIdEvento(final int idEvento) {
		this.idEvento = idEvento;
	}
	
	/**
	 * Recupera l'id dell'Aoo.
	 * @return id dell'Aoo
	 */
	public Long getIdAoo() {
		return idAoo;
	}
	
	/**
	 * Imposta l'id dell'ente.
	 * @param idEnte id dell'ente
	 */
	public void setIdEnte(final int idEnte) {
		this.idEnte = idEnte;
	}
	
	/**
	 * Recupera l'id dell'utente.
	 * @return id dell'utente
	 */
	public Long getIdUtente() {
		return idUtente;
	}
	
	/**
	 * Imposta l'id dell'Aoo.
	 * @param idAoo id dell'Aoo
	 */
	public void setIdAoo(final Long idAoo) {
		this.idAoo = idAoo;
	}
	
	/**
	 * Imposta l'id dell'utente.
	 * @param idUtente id dell'utente
	 */
	public void setIdUtente(final Long idUtente) {
		this.idUtente = idUtente;
	}
	
	/**
	 * Recupera l'id del nodo.
	 * @return id del nodo
	 */
	public Long getIdNodo() {
		return idNodo;
	}
	
	/**
	 * Recupera l'id del ruolo.
	 * @return id del ruolo
	 */
	public Long getIdRuolo() {
		return idRuolo;
	}
	
	/**
	 * Imposta l'id del nodo.
	 * @param idNodo id del nodo
	 */
	public void setIdNodo(final Long idNodo) {
		this.idNodo = idNodo;
	}
	
	/**
	 * Imposta l'id del ruolo.
	 * @param idRuolo id del ruolo
	 */
	public void setIdRuolo(final Long idRuolo) {
		this.idRuolo = idRuolo;
	}
	
	/**
	 * Recupera il contenuto.
	 * @return contenuto
	 */
	public String getContenuto() {
		return contenuto;
	}
	
	/**
	 * Recupera il link esterno.
	 * @return link esterno
	 */
	public String getLinkEsterno() {
		return linkEsterno;
	}
	
	/**
	 * Imposta il contenuto.
	 * @param contenuto contenuto
	 */
	public void setContenuto(final String contenuto) {
		this.contenuto = contenuto;
	}
	
	/**
	 * Recupera il colore del contenuto.
	 * @return colore del contenuto
	 */
	public String getColoreContenuto() {
		return coloreContenuto;
	}
	
	/**
	 * Imposta il link esterno.
	 * @param linkEsterno link esterno
	 */
	public void setLinkEsterno(final String linkEsterno) {
		this.linkEsterno = linkEsterno;
	}
	
	/**
	 * Recupera l'attributo isLocked.
	 * @return isLocked
	 */
	public boolean isLocked() {
		return isLocked;
	}
	
	/**
	 * Imposta il colore del contenuto.
	 * @param coloreContenuto colore del contenuto
	 */
	public void setColoreContenuto(final String coloreContenuto) {
		this.coloreContenuto = coloreContenuto;
	}
	
	/**
	 * Imposta l'attributo isLocked.
	 * @param isLocked
	 */
	public void setLocked(final boolean isLocked) {
		this.isLocked = isLocked;
	}
	
	/**
	 * Recupera l'attributo isPubblico.
	 * @return isPubblico
	 */
	public boolean isPubblico() {
		return isPubblico;
	}
	
	/**
	 * Recupera l'attributo urgente.
	 * @return urgente
	 */
	public int getUrgente() {
		return urgente;
	}
	
	/**
	 * Imposta l'attributo isPubblico.
	 * @param isPubblico
	 */
	public void setPubblico(final boolean isPubblico) {
		this.isPubblico = isPubblico;
	}
	
	/**
	 * Imposta l'attributo urgente.
	 * @param urgente
	 */
	public void setUrgente(final int urgente) {
		this.urgente = urgente;
	}
	
	/**
	 * Recupera l'attributo dataTime.
	 * @return dataTime
	 */
	public Date getDataTime() {
		return dataTime;
	}
	
	/**
	 * Recupera l'allegato.
	 * @return allegato
	 */
	public byte[] getAllegato() {
		return allegato;
	}
	
	/**
	 * Imposta l'attributo dataTime.
	 * @param dataTime
	 */
	public void setDataTime(final Date dataTime) {
		this.dataTime = dataTime;
	}

	/**
	 * Recupera il nome dell'allegato.
	 * @return nome dell'allegato
	 */
	public String getNomeAllegato() {
		return nomeAllegato;
	}
	
	/**
	 * Imposta l'allegato.
	 * @param allegato
	 */
	public void setAllegato(final byte[] allegato) {
		this.allegato = allegato;
	}
	
	/**
	 * Imposta il nome dell'allegato.
	 * @param nomeAllegato nome dell'allegato
	 */
	public void setNomeAllegato(final String nomeAllegato) {
		this.nomeAllegato = nomeAllegato;
	}
	
	/**
	 * Recupera l'estensione dell'allegato.
	 * @return estensione dell'allegato
	 */
	public String getEstensioneAllegato() {
		return estensioneAllegato;
	}
	
	/**
	 * Recupera la data di inizio.
	 */
	public void setDataInizio(final Date dataInizio) {
		this.dataInizio = dataInizio;
	}
	
	/**
	 * Imposta l'estensione dell'allegato.
	 * @param estensioneAllegato estensione dell'allegato
	 */
	public void setEstensioneAllegato(final String estensioneAllegato) {
		this.estensioneAllegato = estensioneAllegato;
	}

	/**
	 * Imposta la data di inizio.
	 * 
	 * @param dataInizio data di inizio
	 */
	public void setDataScadenza(final Date dataScadenza) {
		this.dataScadenza = dataScadenza;
	}
	
	/**
	 * Imposta il titolo.
	 * 
	 * @param titolo titolo
	 */
	public void setTitolo(final String titolo) {
		this.titolo = titolo;
	}
	
	/**
	 * Imposta la tipologia di evento del calendario.
	 * @param tipologia tipologia di evento del calendario
	 */
	public void setTipologia(final EventoCalendarioEnum tipologia) {
		this.tipologia = tipologia;
	}

	/**
	 * Recupera il titolo breve.
	 * @return titolo breve
	 */
	public String getTitoloBreve() {
		return StringUtils.getTextAbstract(titolo, 20);
	}

	/**
	 * Recupera il tipo calendario.
	 * @return tipo calendario
	 */
	public TipoAzioneEnum getTipoCal() {
		return tipoCal;
	}

	/**
	 * Imposta il tipo calendario.
	 * @param tipoCal tipo calendario
	 */
	public void setTipoCal(final TipoAzioneEnum tipoCal) {
		this.tipoCal = tipoCal;
	}
	
	/**
	 * recupera il mimeType allegato.
	 * @return mimeType allegato
	 */
	public String getMimeTypeAllegato() {
		return mimeTypeAllegato;
	}

	/**
	 * Recupera il document title.
	 * @return document title
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}
	
	/**
	 * Imposta il mimeType allegato.
	 * 
	 * @param mimeTypeAllegato mimeType allegato
	 */
	public void setMimeTypeAllegato(final String mimeTypeAllegato) {
		this.mimeTypeAllegato = mimeTypeAllegato;
	}
	
	/**
	 * Recupera il numero documento.
	 * @return numero documento
	 */
	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}
	
	/**
	 * Imposta il document title.
	 * @param documentTitle document title
	 */
	public void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * Imposta il numero documento.
	 * @param numeroDocumento numero documento
	 */
	public void setNumeroDocumento(final Integer numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * Recupera gli id dei ruoli categoria target.
	 * @return lista degli id dei ruoli categoria target
	 */
	public List<Long> getIdRuoliCategoriaTarget() {
		return idRuoliCategoriaTarget;
	}

	/**
	 * Recupera gli id dei nodi target.
	 * @return lista degli id dei nodi target
	 */
	public List<Integer> getIdNodiTarget() {
		return idNodiTarget;
	}
	
	/**
	 * Imposta gli id dei ruoli categoria target.
	 * @param idRuoliCategoriaTarget lista degli id dei ruoli categoria target
	 */
	public void setIdRuoliCategoriaTarget(final List<Long> idRuoliCategoriaTarget) {
		this.idRuoliCategoriaTarget = idRuoliCategoriaTarget;
	}

	/**
	 * Imposta gli id dei nodi target.
	 * @param idNodiTarget lista degli id dei nodi target
	 */
	public void setIdNodiTarget(final List<Integer> idNodiTarget) {
		this.idNodiTarget = idNodiTarget;
	}

	/**
	 * Recupera la descrizione dei nodi.
	 * @return lista delle descrizioni dei nodi
	 */
	public List<String> getDescrizioneNodi() {
		return descrizioneNodi;
	}

	/**
	 * Recupera l'id della categoria documento.
	 * @return id della categoria documento
	 */
	public Integer getIdCategoriaDocumento() {
		return idCategoriaDocumento;
	}
	
	/**
	 * Imposta la descrizione dei nodi.
	 * @param descrizioneNodi lista delle descrizioni dei nodi
	 */
	public void setDescrizioneNodi(final List<String> descrizioneNodi) {
		this.descrizioneNodi = descrizioneNodi;
	}

	/**
	 * Imposta l'id della categoria documento.
	 * @param idCategoriaDocumento id della categoria documento
	 */
	public void setIdCategoriaDocumento(final Integer idCategoriaDocumento) {
		this.idCategoriaDocumento = idCategoriaDocumento;
	} 
	
	/**
	 * Recupera l'attributo isModificabileWdg.
	 * @return isModificabileWdg
	 */
	public Boolean getIsModificabileWdg() {
		return isModificabileWdg;
	}
	
	/**
	 * Imposta l'attributo isModificabileWdg.
	 * @param isModificabileWdg
	 */
	public void setIsModificabileWdg(final Boolean isModificabileWdg) {
		this.isModificabileWdg = isModificabileWdg;
	} 
}
