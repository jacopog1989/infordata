package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IEliminaPrecensitiFacadeSRV;

/**
 * Interface del servizio eliminazione precensiti.
 */
public interface IEliminaPrecensitiSRV extends IEliminaPrecensitiFacadeSRV {

}
