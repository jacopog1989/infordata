package it.ibm.red.business.service.concrete;

import java.io.StringReader;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXB;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.apache.axis2.Constants;
import org.apache.axis2.jaxws.handler.SOAPHeadersAdapter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.dto.SequKDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoProtocolloEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.provider.WebServiceClientProvider;
import it.ibm.red.business.service.IMockSRV;
import it.ibm.red.business.service.IPMefSRV;
import net.protmef.data.common.v1.CredenzialiSicurezzaType;
import net.protmef.data.common.v1.CredenzialiUtenteProtocolloType;
import net.protmef.data.common.v1.MezzoSpedizioneType;
import net.protmef.data.common.v1.RegistroType;
import net.protmef.data.common.v1.UfficioType;
import net.protmef.data.common.v1.UfficioUtenteType;
import net.protmef.data.common.v1.UtenteType;
import net.protmef.data.protocollo.v1.AssegnatarioType;
import net.protmef.data.protocollo.v1.DestinatarioType;
import net.protmef.data.protocollo.v1.MittenteEntrataType;
import net.protmef.data.protocollo.v1.MittenteEntrataType.PERSONAFISICA;
import net.protmef.data.protocollo.v1.MittenteEntrataType.PERSONAGIURIDICA;
import net.protmef.data.protocollo.v1.ProtocolloEntrataType;
import net.protmef.data.protocollo.v1.ProtocolloEntrataType.ASSEGNATARI;
import net.protmef.data.protocollo.v1.ProtocolloUscitaType;
import net.protmef.data.protocollo.v1.ProtocolloUscitaType.DESTINATARI;
import net.protmef.data.protocollo.v1.RegistrazioneProtocolloType;
import net.protmef.data.protocollo.v1.SPEDIZIONETypeIPA;
import net.protmef.data.protocollo.v1.StatoProtocolloType;
import net.protmef.data.protocollo.v1.TipoDocumentoType;
import net.protmef.headerdata.v1.ArrayOfEccezione;
import net.protmef.headerdata.v1.ArrayOfEccezione.Eccezione;
import net.protmef.headerdata.v1.HeaderProtMef;
import net.protmef.headerdata.v1.HeaderProtMef.IntestazioneMessaggio;
import net.protmef.messages.v1.ProtocollazioneEntrataRequest;
import net.protmef.messages.v1.ProtocollazioneEntrataResponse;
import net.protmef.messages.v1.ProtocollazioneUscitaRequest;
import net.protmef.messages.v1.ProtocollazioneUscitaResponse;
import net.protmef.ws.v1.IProtocolloMEFWS;


/**
 * Servizio per la protocollazione MEF.
 *
 * @author mcrescentini
 */
@Service
@Component
public class PMefSRV extends AbstractService implements IPMefSRV {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 5861630704467114794L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PMefSRV.class.getName());
	
	/**
	 * DAO.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;
	
	/**
	 * DAO.
	 */
	@Autowired
	private INodoDAO nodoDAO;
	
	/**
	 * Service.
	 */
	@Autowired
	private IMockSRV mockSRV;
	
	/**
	 * Fornitore delle properties dell'applicazione.
	 */
	private PropertiesProvider pp;
	
	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}
	
	/**
	 * @see it.ibm.red.business.service.IPMefSRV#getListaDestinatari(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String[], java.lang.String[], java.lang.String[],
	 *      java.lang.String[], java.lang.String[], java.lang.String[],
	 *      java.lang.String[], boolean, java.sql.Connection).
	 */
	@Override
	public List<String> getListaDestinatari(final UtenteDTO utente, final String[] assegnazioniCompetenza, final String[] assegnazioniConoscenza,
			final String[] assegnazioniContributo, final String[] assegnazioniFirma, final String[] assegnazioniSigla, final String[] assegnazioniVisto, 
			final String[] assegnazioniFirmaMultipla, final boolean controlloIter, final Connection con) {
		final List<String> destinatari = new ArrayList<>();
		
		Integer assegnazioniCompetenzaSize = 0;
		if (assegnazioniCompetenza != null) {
			assegnazioniCompetenzaSize = assegnazioniCompetenza.length;
		}
		Integer assegnazioniConoscenzaSize = 0;
		if (assegnazioniConoscenza != null) {
			assegnazioniConoscenzaSize = assegnazioniConoscenza.length;
		}
		Integer assegnazioniContributoSize = 0;
		if (assegnazioniContributo != null) {
			assegnazioniContributoSize = assegnazioniContributo.length;
		}
		Integer assegnazioniFirmaSize = 0;
		if (assegnazioniFirma != null) {
			assegnazioniFirmaSize = assegnazioniFirma.length;
		}
		Integer assegnazioniSiglaSize = 0;
		if (assegnazioniSigla != null) {
			assegnazioniSiglaSize = assegnazioniSigla.length;
		}
		Integer assegnazioniVistoSize = 0;
		if (assegnazioniVisto != null) {
			assegnazioniVistoSize = assegnazioniVisto.length;
		}
		Integer assegnazioniFirmaMultiplaSize = 0;
		if (assegnazioniFirmaMultipla != null) {
			assegnazioniFirmaMultiplaSize = assegnazioniFirmaMultipla.length;
		}
		
		if (assegnazioniCompetenzaSize > 0) {
			getDestinatari(destinatari, assegnazioniCompetenza, con);
		}
		
		if (assegnazioniConoscenzaSize > 0) {
			getDestinatari(destinatari, assegnazioniConoscenza, con);
		}
		
		if (assegnazioniContributoSize > 0) {
			getDestinatari(destinatari, assegnazioniContributo, con);
		}
		
		if (assegnazioniFirmaSize > 0) {
			getDestinatari(destinatari, assegnazioniFirma, con);
		}
		
		if (assegnazioniSiglaSize > 0) {
			getDestinatari(destinatari, assegnazioniSigla, con);
		}

		if (assegnazioniVistoSize > 0) {
			getDestinatari(destinatari, assegnazioniVisto, con);
		}
		
		if (assegnazioniFirmaMultiplaSize > 0) {
			getDestinatari(destinatari, assegnazioniFirmaMultipla, con);
		}
		
		if (controlloIter && utente != null 
				&& (assegnazioniCompetenzaSize == 0 && assegnazioniConoscenzaSize == 0 && assegnazioniContributoSize == 0 && assegnazioniFirmaSize == 0 
					&& assegnazioniSiglaSize == 0 && assegnazioniVistoSize == 0 && assegnazioniFirmaMultiplaSize == 0)) {
			// Si recupera l'utente dirigente
			final Nodo nodoUtente = nodoDAO.getNodo(utente.getIdUfficio(), con);	
			final UtenteDTO dirigenteUfficioUtente = new UtenteDTO(utenteDAO.getUtente(nodoUtente.getDirigente().getIdUtente(), con));
			// Si inserisce tra i destinatari
			if (dirigenteUfficioUtente != null) {
				final String destinatarioDirigente = dirigenteUfficioUtente.getNome() + " " + dirigenteUfficioUtente.getCognome();
				destinatari.add(destinatarioDirigente);
			}
		}
		
		return destinatari;
	}
	
	
	private void getDestinatari(final List<String> destinatari, final String[] assegnazioni, final Connection con) {
		Long idUfficio;
		Long idUtente;
		for (final String assegnazione : assegnazioni) {
			final String[] assegnazioneSplit = assegnazione.split(",");
			idUfficio = Long.parseLong(assegnazioneSplit[0]);
			idUtente = Long.parseLong(assegnazioneSplit[1]);
			
			if (idUtente > 0) { // Utente
				final Utente utenteAssegnazione = utenteDAO.getUtente(idUtente, con);
				destinatari.add(utenteAssegnazione.getNome() + " " + utenteAssegnazione.getCognome());
			} else { // Ufficio
				final Nodo nodo = nodoDAO.getNodo(idUfficio, con);
				destinatari.add(nodo.getDescrizione());
			}
		}
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IPMefFacadeSRV#creaProtocolloMEFEntrata(it.ibm.red.business.persistence.model.Contatto,
	 *      java.lang.String, it.ibm.red.business.dto.SequKDTO,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public ProtocolloDTO creaProtocolloMEFEntrata(final Contatto inMittente, final String oggettoDocumento, 
			final SequKDTO parametriProtocolloMEF, final UtenteDTO utenteDTO) {
		ProtocolloDTO protocolloMef = null;
		
		try {
			
			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED)) 
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[creaProtocolloMEFEntrata] Non è possibile comunicare con PMEF in regime mock");
				
				return mockSRV.getNewProtocollo(utenteDTO.getCodiceAoo(), TipoProtocolloEnum.ENTRATA);
				
			}
			
			// Header
			final HeaderProtMef header = getHeaderProtocolloMEF(parametriProtocolloMEF);

			// Creazione Registro - id_registro e id_aoo
			final RegistroType registro = new RegistroType();
			registro.setSEQUKREGISTRO(new BigDecimal(parametriProtocolloMEF.getSequKRegistro()));
			registro.setFKAOO(new BigDecimal(parametriProtocolloMEF.getSequKAoo())); // Facoltativo!!!

			// Ufficio
			final UfficioType ufficio = new UfficioType();
			ufficio.setSEQUKUFFICIO(new BigDecimal(parametriProtocolloMEF.getSequKUfficio()));
			ufficio.setFKAOO(new BigDecimal(parametriProtocolloMEF.getSequKAoo()));
			// Utente
			final UtenteType utente = new UtenteType();
			utente.setSEQUKUTENTE(new BigDecimal(parametriProtocolloMEF.getSequKUtente()));

			final UfficioUtenteType protocollatore = new UfficioUtenteType();
			protocollatore.setUFFICIO(ufficio);
			protocollatore.setUTENTE(utente);

			// Creazione Mittente (persona giuridica/fisica)
			final MittenteEntrataType mittente = new MittenteEntrataType();

			if ("F".equals(inMittente.getTipoPersona())) {
				final PERSONAFISICA personaFisicaDettagli = new PERSONAFISICA();
				personaFisicaDettagli.setCOGNOME(inMittente.getCognome());
				personaFisicaDettagli.setNOME(inMittente.getNome());
				mittente.setPERSONAFISICA(personaFisicaDettagli);
			} else {
				final PERSONAGIURIDICA personaGiuridicaDettagli = new PERSONAGIURIDICA();
				personaGiuridicaDettagli.setDENOMINAZIONE(StringUtils.isNotBlank(inMittente.getNome()) ? inMittente.getNome() : inMittente.getAliasContatto());
				mittente.setPERSONAGIURIDICA(personaGiuridicaDettagli);
			}

			// Creazione Tipo Documento - id_documento
			final TipoDocumentoType tipoDoc = new TipoDocumentoType();
			tipoDoc.setSEQUKTIPODOCUMENTO(new BigDecimal(parametriProtocolloMEF.getSequKTipodocumento()));

			// Creazione Assegnatario - competenza/conoscenza
			final ASSEGNATARI assegnatari = new ASSEGNATARI();
			boolean checkCompetenza = false; // Check se è stato impostato un assegnatario per competenza
			final String[] splitPV = parametriProtocolloMEF.getAssegnatari().split(";");

			for (int z = 0; z < splitPV.length; z++) {
				// Si ricavano i campi idUfficio, idUtente, tipoAssegnazione
				final String[] splitVirg = splitPV[z].split(",");

				final Integer idUfficioInteger = new Integer(splitVirg[0]);
				final Integer idTipoAssegnInteger = new Integer(splitVirg[1]);

				final UfficioType uffassegnato = new UfficioType();
				uffassegnato.setSEQUKUFFICIO(new BigDecimal(idUfficioInteger));
				uffassegnato.setFKAOO(new BigDecimal(parametriProtocolloMEF.getSequKAoo()));

				final UfficioUtenteType ufficioassegnato = new UfficioUtenteType();
				ufficioassegnato.setUFFICIO(uffassegnato);

				final AssegnatarioType assegnatario = new AssegnatarioType();
				assegnatario.setASSEGNATARIO(ufficioassegnato);
				assegnatario.setUFFICIOASSEGNANTE(ufficio);
				assegnatario.setIDASSEGNAZIONE(new BigDecimal(1)); // FACOLTATIVO!!!

				// Si entra se l'assegnazione è per competenza e se non è stato ancora impostato un assegnatario per competenza
				if (idTipoAssegnInteger == 1 && !checkCompetenza) {
					assegnatari.setASSEGNATARIOCOMPETENZA(assegnatario);
					checkCompetenza = true; // L'assegnatario per competenza in SIGED2 è unico.
				}
			}

			final ProtocolloEntrataType datiProtocolloEntrata = new ProtocolloEntrataType();
			datiProtocolloEntrata.setTIPODOCUMENTO(tipoDoc);
			datiProtocolloEntrata.setREGISTRO(registro);
			datiProtocolloEntrata.setOGGETTO(oggettoDocumento);
			datiProtocolloEntrata.setPROTOCOLLATORE(protocollatore);
			datiProtocolloEntrata.setMITTENTE(mittente);
			datiProtocolloEntrata.setASSEGNATARI(assegnatari);
			datiProtocolloEntrata.setSTATOPROTOCOLLO(StatoProtocolloType.DA_ASSEGNARE);
			
			final IProtocolloMEFWS proxy = WebServiceClientProvider.getIstance().getPmefClient();

			final ProtocollazioneEntrataRequest request = new ProtocollazioneEntrataRequest();
			request.setDatiProtocollo(datiProtocolloEntrata);
			
			final ProtocollazioneEntrataResponse response = proxy.protocollazioneEntrata(header, request);
			
			if (response != null) {
				LOGGER.info("Chiamata al Protocollo MEF per la protocollazione in entrata effettuata con successo.");
				final RegistrazioneProtocolloType protocollo = response.getEstremiProtocollo();
				
				if (protocollo != null) {
					final Date dataRegistrazione = protocollo.getDATAREGISTRAZIONE() != null ? protocollo.getDATAREGISTRAZIONE().toGregorianCalendar().getTime() : null;
					// Creazione oggetto Protocollo MEF
					protocolloMef = new ProtocolloDTO(null, protocollo.getNUMEPROTOCOLLO(),	String.valueOf(protocollo.getANNO()), dataRegistrazione, null, null);
					LOGGER.info("Protocollazione in entrata EFFETTUATA. Protocollo generato: "	+ protocolloMef.getNumeroProtocollo()
						+ ", anno: " + protocolloMef.getAnnoProtocollo());
				} else {
					gestisciErroreProtocollazione(proxy);
				}
			} else {
				throw new RedException("Servizio di protocollazione non disponibile, utilizzare il protocollo di emergenza o contattare l'assistenza");
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la creazione del protocollo in entrata tramite servizi PMEF.", e);
			throw new RedException(e);
		} 
		
		return protocolloMef;
	}
	
	
	/**
	 * Recupera il protocollo da PMEF.
	 * 
	 * @param oggettoDocumento - Oggetto del documento.
	 * @param parametriProtocolloMEF - Parametri del SequK utilizzato per il recupero del protocollo da PMEF.
	 * @param destinatarioConoscenza - Destinatario di conoscenza.
	 * @return Protocollo PMEF.
	 */
	@Override
	public ProtocolloDTO creaProtocolloMEFUscita(final String oggettoDocumento, final SequKDTO parametriProtocolloMEF, 
			final String destinatarioConoscenza, final UtenteDTO utenteDTO) {
		ProtocolloDTO protocolloMef = null;
		
		try {
			
			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED)) 
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[creaProtocolloMEFUscita] Non è possibile comunicare con PMEF in regime mock");
				
				return mockSRV.getNewProtocollo(utenteDTO.getCodiceAoo(), TipoProtocolloEnum.USCITA);
				
			}
			
			// Header
			final HeaderProtMef header = getHeaderProtocolloMEF(parametriProtocolloMEF);
			
			// Genero la request
			final RegistroType registro = new RegistroType();
			registro.setSEQUKREGISTRO(new BigDecimal(parametriProtocolloMEF.getSequKRegistro()));
			registro.setFKAOO(new BigDecimal(parametriProtocolloMEF.getSequKAoo()));

			// Creazione Mittente
			final UfficioType uffMittente = new UfficioType();
			uffMittente.setSEQUKUFFICIO(new BigDecimal(parametriProtocolloMEF.getSequKUfficioMittente()));
			uffMittente.setFKAOO(new BigDecimal(parametriProtocolloMEF.getSequKAoo()));
			uffMittente.setFKSTORICOUFFICIO(new BigDecimal(parametriProtocolloMEF.getSequKStoricoUff()));

			final UfficioUtenteType uffUtMittente = new UfficioUtenteType();
			uffUtMittente.setUFFICIO(uffMittente);

			// Creazione Oggetto direttamente nella creazione dell'oggetto protocollo uscita.

			// Creazione Tipo Documento
			final TipoDocumentoType tipoDocumento = new TipoDocumentoType();
			tipoDocumento.setSEQUKTIPODOCUMENTO(new BigDecimal(parametriProtocolloMEF.getSequKTipodocumento()));

			// Creazione Protocollatore
			final UfficioType uffProtocollatore = new UfficioType();
			uffProtocollatore.setSEQUKUFFICIO(new BigDecimal(parametriProtocolloMEF.getSequKUfficioProtocollatore()));
			uffProtocollatore.setFKAOO(new BigDecimal(parametriProtocolloMEF.getSequKAoo()));

			final UtenteType utenteProtocollatore = new UtenteType();
			utenteProtocollatore.setSEQUKUTENTE(new BigDecimal(parametriProtocolloMEF.getSequKUtenteProtocollatore()));

			final UfficioUtenteType uffUtProtocollatore = new UfficioUtenteType();
			uffUtProtocollatore.setUFFICIO(uffProtocollatore);
			uffUtProtocollatore.setUTENTE(utenteProtocollatore);

			final DESTINATARI destinatari = new DESTINATARI();
			// Ricavo i campi Destinatario e Tipo Spedizione
			final DestinatarioType destinatarioOgg = new DestinatarioType();
			final SPEDIZIONETypeIPA ipa = new SPEDIZIONETypeIPA();
			destinatarioOgg.setSPEDIZIONEIPA(ipa);
			destinatarioOgg.setDESTINATARIO(destinatarioConoscenza);
			destinatarioOgg.setSEQUKDESTINATARIO(new BigDecimal(-1));
			final MezzoSpedizioneType mezzoSpedizioneType = new MezzoSpedizioneType();
			mezzoSpedizioneType.setSEQUKMEZZOSPED(new BigDecimal(parametriProtocolloMEF.getSequKMezzoSpedizione()));
			destinatarioOgg.setMEZZOSPEDIZIONE(mezzoSpedizioneType);
			// Per default il primo destinatario è per competenza...
			destinatari.setDESTINATARIOCOMPETENZA(destinatarioOgg);

			// Creazione oggetto Protocollo Uscita
			final ProtocolloUscitaType protUscitaType = new ProtocolloUscitaType();
			// END DATI OPZIONALI
			protUscitaType.setREGISTRO(registro);
			protUscitaType.setMITTENTE(uffUtMittente);
			protUscitaType.setOGGETTO(oggettoDocumento);
			protUscitaType.setTIPODOCUMENTO(tipoDocumento);
			protUscitaType.setPROTOCOLLATORE(uffUtProtocollatore);
			protUscitaType.setDESTINATARI(destinatari);
			protUscitaType.setSTATOPROTOCOLLO(StatoProtocolloType.DA_ASSEGNARE);

			final ProtocollazioneUscitaRequest request = new ProtocollazioneUscitaRequest();

			// Creazione Richiesta da sottoporre al Web Service.
			request.setDatiProtocollo(protUscitaType);

			final StringBuilder parametriInviati = new StringBuilder("Parametri da inviare al servizio di protocollo uscita ->");
			parametriInviati.append(parametriProtocolloMEF.toString());
			LOGGER.info(parametriInviati.toString());
			
			final IProtocolloMEFWS proxy = WebServiceClientProvider.getIstance().getPmefClient();
			
			final ProtocollazioneUscitaResponse response = proxy.protocollazioneUscita(header, request);
			
			if (response != null) {
				LOGGER.info("Chiamata al Protocollo MEF per la protocollazione in uscita effettuata con successo.");
				final RegistrazioneProtocolloType protocollo = response.getEstremiProtocollo();
				
				if (protocollo != null) {
					final Date dataRegistrazione = protocollo.getDATAREGISTRAZIONE() != null ? protocollo.getDATAREGISTRAZIONE().toGregorianCalendar().getTime() : null;
					protocolloMef = new ProtocolloDTO(null, protocollo.getNUMEPROTOCOLLO(),
							Integer.toString(protocollo.getANNO()), dataRegistrazione, null, null);
					final int numprotocollo = protocollo.getNUMEPROTOCOLLO();
					final int anno = protocollo.getANNO();
					LOGGER.info("Protocollazione in uscita EFFETTUATA. Protocollo generato: " + numprotocollo	+ ", anno: " + anno);
				} else {
					gestisciErroreProtocollazione(proxy);
				}
			} else {
				throw new RedException("Servizio di protocollazione non disponibile, utilizzare il protocollo di emergenza o contattare l'assistenza.");
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la creazione del protocollo in uscita tramite servizi PMEF.", e);
			throw new RedException(e);
		}

		return protocolloMef;
	}
	

	/**
	 * Header per chiamata con protocollo PMEF.
	 * 
	 * @param sequKUtente - Parametri del SequK utilizzato per il recupero del protocollo da PMEF.
	 * @return Header per chiamata con protocollo PMEF.
	 */
	private HeaderProtMef getHeaderProtocolloMEF(final SequKDTO sequKUtente) {
		final HeaderProtMef header = new HeaderProtMef();
		try {
			// HEADER
			final String azione = pp.getParameterByKey(PropertiesNameEnum.AZIONE_PMEFKEY);
			final String servizio = pp.getParameterByKey(PropertiesNameEnum.SERVIZIO_PMEFKEY);
			final String wspassword = pp.getParameterByKey(PropertiesNameEnum.WSPASSWORD_PMEFKEY);
			final String wsusername = pp.getParameterByKey(PropertiesNameEnum.WSUSERNAME_PMEFKEY);

			LOGGER.info("Parametri recuperati dal file configurazione : ");

			LOGGER.info("azione : " + azione);
			LOGGER.info("servizio : " + servizio);
			LOGGER.info("wspassword : " + wspassword);
			LOGGER.info("wsusername : " + wsusername);

			final UtenteType utente = new UtenteType();
			utente.setSEQUKUTENTE(new BigDecimal(sequKUtente.getSequKUtente()));

			final UfficioType ufficio = new UfficioType();
			ufficio.setSEQUKUFFICIO(new BigDecimal(sequKUtente.getSequKUfficio()));
			ufficio.setFKAOO(new BigDecimal(sequKUtente.getSequKAoo()));

			final CredenzialiUtenteProtocolloType credProt = new CredenzialiUtenteProtocolloType();
			credProt.setUTENTE(utente);
			credProt.setPASSWORD(wspassword); // password FACOLTATIVA!!!
			credProt.setUFFICIO(ufficio);

			final CredenzialiSicurezzaType credSicurezza = new CredenzialiSicurezzaType();
			credSicurezza.setUSERID(wsusername);
			credSicurezza.setPASSWORD(wspassword); // OBBLIGATORIA... property

			final IntestazioneMessaggio intestMessaggio = new IntestazioneMessaggio();
			intestMessaggio.setCredenzialiUtente(credProt);
			intestMessaggio.setCredenzialiSicurezza(credSicurezza);
			intestMessaggio.setAzione(azione);
			intestMessaggio.setServizio(servizio);
			header.setIntestazioneMessaggio(intestMessaggio);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore nella generazione dell'header per la richiesta della protocollazione MEF.", e);
			throw new RedException(e);
		}
		
		return header;
	}
	
	
	private void gestisciErroreProtocollazione(final IProtocolloMEFWS proxy) {
		HeaderProtMef header = null;
		try {
			final BindingProvider bindingProvider = (BindingProvider) proxy;
			final SOAPHeadersAdapter list = (SOAPHeadersAdapter) bindingProvider.getResponseContext().get(Constants.JAXWS_INBOUND_SOAP_HEADERS);
			final QName headerQName = new QName("urn:protmef.net:headerdata:v1", "_HeaderProtMef");
			String headerAsString = null;
			if (list != null) {
				headerAsString = list.get(headerQName).get(0);
				header = JAXB.unmarshal(new StringReader(headerAsString), HeaderProtMef.class);	
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'unmarshal dell'eccezione restituita dal servizio di protocollazione.", e);
			throw new RedException(e);
		}
		
		if (header != null) {
			final ArrayOfEccezione eccezioniList = header.getListaEccezioni();
			if (eccezioniList != null && !CollectionUtils.isEmpty(eccezioniList.getEccezione())) {
				final Eccezione primaEccezione = eccezioniList.getEccezione().get(0);
				final String errore = "Si è verificato il seguente errore nella chiamata al servizio di protocollazione.\nTipologia Errore: " 
						+ primaEccezione.getCodiceEccezione() + ", Messaggio Errore: " + primaEccezione.getMessaggioEccezione()	
						+ ", Rilevanza: " + primaEccezione.getRilevanza();
				LOGGER.error(errore);
				throw new RedException(errore);
			}
		}
	}
}