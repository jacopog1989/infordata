package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.enums.TrasformerCEEnum;

/**
 * The Class FromDocumentoToMailRicercaTrasformer.
 *
 * @author VINGENITO
 * 
 *         Trasformer ricerca mail.
 */
public class FromDocumentoToMailRicercaTrasformer extends FromDocumentoToMailAbstractTrasformer {
	
	private static final long serialVersionUID = 7115280317344104212L;
	
	/**
	 * Costruttore.
	 */
	public FromDocumentoToMailRicercaTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_MAIL_RICERCA);
	}
	

	/**
	 * Trasform.
	 *
	 * @param document
	 *            the document
	 * @param connection
	 *            the connection
	 * @return the email DTO
	 */
	@Override
	public final EmailDTO trasform(final Document document, final Connection connection) {
		return trasform(document, connection, TrasformerCEEnum.FROM_DOCUMENTO_TO_MAIL_RICERCA);
	}
	
}