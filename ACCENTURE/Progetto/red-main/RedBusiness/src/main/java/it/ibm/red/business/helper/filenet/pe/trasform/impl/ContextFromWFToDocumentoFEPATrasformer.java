package it.ibm.red.business.helper.filenet.pe.trasform.impl;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.NotaDTO;
import it.ibm.red.business.dto.PEDocumentoFEPADTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoOperazioneLibroFirmaEnum; 
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * Trasformer da WF a DocumentoFEPA.
 */
public class ContextFromWFToDocumentoFEPATrasformer extends FromWFToGenericDocumentTrasformer<PEDocumentoFEPADTO> {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContextFromWFToDocumentoFEPATrasformer.class.getName());

	/*
	 * UID.
	 */
	private static final long serialVersionUID = 2006486176907747048L;

	/**
	 * Trasformer context - FROM_WF_TO_DOCUMENTO_FEPA.
	 * @see TrasformerPEEnum
	 */
	public ContextFromWFToDocumentoFEPATrasformer() {
		super(TrasformerPEEnum.FROM_WF_TO_DOCUMENTO_FEPA);
	}

	/**
	 * @param object
	 * @param idDocumento
	 * @param tolf
	 * @param wobNumber
	 * @param documentQueueEnum
	 * @param idUtenteDestinatario
	 * @param idNodoDestinatario
	 * @param bFirmaFig
	 * @param motivazioneAssegnazione
	 * @param responses
	 * @param idFascicolo
	 * @param codaCorrente
	 * @param tipoAssegnazioneId
	 * @param flagIterManuale
	 * @param subject
	 * @param elencoLibroFirma
	 * @param count
	 * @param dataCreazioneWF
	 * @param flagRenderizzatoBool
	 * @param firmaCopiaConforme
	 * @param urgente
	 * @param stepName
	 * @param notaDTO
	 * @param inContributiRichiesti
	 * @param inContributiPervenuti
	 * @param tipoFirma
	 * @param annullaTemplate
	 * @return PEDocumentoFEPADTO
	 */
	@Override
	PEDocumentoFEPADTO createGeneric(final VWWorkObject object, final String idDocumento, final TipoOperazioneLibroFirmaEnum tolf, final String wobNumber,
			final DocumentQueueEnum documentQueueEnum, final Integer idUtenteDestinatario, final Integer idNodoDestinatario,
			final Boolean bFirmaFig, final String motivazioneAssegnazione, final String[] responses, final Integer idFascicolo,
			final String codaCorrente, final Integer tipoAssegnazioneId, final Integer flagIterManuale, final String subject,
			final String[] elencoLibroFirma, final Integer count, final String dataCreazioneWF, final Boolean flagRenderizzatoBool,
			final Boolean firmaCopiaConforme, final Boolean urgente, final String stepName, final NotaDTO notaDTO, final Integer inContributiRichiesti, 
			final Integer inContributiPervenuti, final int tipoFirma, final boolean annullaTemplate, final Boolean firmaAsincronaAvviata) {

		PropertiesProvider pp = PropertiesProvider.getIstance();
		PEDocumentoFEPADTO doc = new PEDocumentoFEPADTO(idDocumento, tolf, wobNumber, documentQueueEnum, idUtenteDestinatario, 
				idNodoDestinatario, bFirmaFig, motivazioneAssegnazione, responses, idFascicolo, codaCorrente, tipoAssegnazioneId, 
				flagIterManuale, subject, elencoLibroFirma, count, dataCreazioneWF, flagRenderizzatoBool, firmaCopiaConforme, urgente, stepName, notaDTO, 
				null, null, tipoFirma, annullaTemplate, firmaAsincronaAvviata);
		boolean hasDSR = object.hasFieldName(pp.getParameterByKey(PropertiesNameEnum.FEPA_DICHIARAZIONE_SERVIZI_RESI_WFKEY));
		if (hasDSR) {
			String[] wobDichiarazioneServiziResi = (String[]) getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.FEPA_DICHIARAZIONE_SERVIZI_RESI_WFKEY));
			doc.setWobDichiarazioneServiziResi(wobDichiarazioneServiziResi);
			doc.sethasWobDichiarazioneServiziResi(true);
		}
		String[] elencoDecretiFepa = (String[]) getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.FEPA_ELENCO_DECRETI_WFKEY));
		doc.setElencoDecretiFepa(elencoDecretiFepa);
		
		return doc;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.pe.trasform.impl.FromWFToGenericDocumentTrasformer#getLogger().
	 */
	@Override
	protected REDLogger getLogger() {
		return LOGGER;
	}
}
