package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.Date;
import java.util.Map;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * @author a.dilegge
 *
 */
public class ContextFromDocumentoToAssociaIntegrazioneDati extends ContextTrasformerCE<MasterDocumentRedDTO> {

	/**
	 * UID.
	 */
	private static final long serialVersionUID = 8212037904935061830L;
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContextFromDocumentoToAssociaIntegrazioneDati.class.getName());

	/**
	 * Trasformer context - FROM_DOCUMENTO_TO_ASSOCIA_INTEGRAZIONE_DATI_CONTEXT.
	 * 
	 * @see TrasformerCEEnum
	 */
	public ContextFromDocumentoToAssociaIntegrazioneDati() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_ASSOCIA_INTEGRAZIONE_DATI_CONTEXT);
	}

	/**
	 * Questa implementazione non può essere usata, lancia un'eccezione.
	 * 
	 * @param document
	 * @param connection
	 * @param context
	 * @throw RedException
	 */
	@Override
	public MasterDocumentRedDTO trasform(final Document document, final Connection connection, final Map<ContextTrasformerCEEnum, Object> context) {
		throw new RedException();
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextTrasformerCE#trasform(com.filenet.api.core.Document,
	 *      java.util.Map).
	 */
	@SuppressWarnings("unchecked")
	@Override
	public MasterDocumentRedDTO trasform(final Document document, final Map<ContextTrasformerCEEnum, Object> context) {
		if (document == null) {
			return null;
		}

		try {

			final String documentTitle = (String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);

			final Integer annoDocumento = (Integer) getMetadato(document, PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY);
			final Integer numeroDocumento = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);

			final Integer idTipologiaDocumento = (Integer) getMetadato(document, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
			String tipologiaDocumento = null;
			if (idTipologiaDocumento != null) {
				tipologiaDocumento = ((Map<Long, String>) context.get(ContextTrasformerCEEnum.IDENTIFICATIVI_TIPO_DOC)).get(idTipologiaDocumento.longValue());
			}

			final String oggetto = (String) getMetadato(document, PropertiesNameEnum.OGGETTO_METAKEY);
			final Integer annoProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
			final Integer numeroProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
			final String idProtocollo = (String) getMetadato(document, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);
			final Date dataProtocollo = (Date) getMetadato(document, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);
			final Integer annoProtocolloEmergenza = (Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_EMERGENZA_METAKEY);
			final Integer numeroProtocolloEmergenza = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_EMERGENZA_METAKEY);

			final Date dataCreazione = (Date) getMetadato(document, PropertiesNameEnum.DATA_CREAZIONE_METAKEY);

			final Integer idTipologiaProcedimento = (Integer) getMetadato(document, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);

			final String decretoLiquidazione = (String) getMetadato(document, PropertiesNameEnum.DECRETO_LIQUIDAZIONE_METAKEY);

			final DocumentQueueEnum queue = (DocumentQueueEnum) context.get(ContextTrasformerCEEnum.QUEUE);

			final Boolean firmeValideAll = (Boolean) getMetadato(document, PropertiesNameEnum.VERIFICA_FIRMA_DOC_ALLEGATO); // Vale solo per gli allegati
			final Integer metadatoValiditaFirma = (Integer) getMetadato(document, PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA);
			Boolean firmaValidaPrinc = null;
			if(metadatoValiditaFirma!=null) {
				if(ValueMetadatoVerificaFirmaEnum.FIRMA_OK.equals(ValueMetadatoVerificaFirmaEnum.get(metadatoValiditaFirma))) {
					firmaValidaPrinc = true;
				}else if(ValueMetadatoVerificaFirmaEnum.FIRMA_KO.equals(ValueMetadatoVerificaFirmaEnum.get(metadatoValiditaFirma))){
					firmaValidaPrinc = false;
				}
				
			}
			
			
			MasterDocumentRedDTO m = new MasterDocumentRedDTO(documentTitle, numeroDocumento, oggetto, tipologiaDocumento, numeroProtocollo, annoProtocollo, null, null, null, null, null, 
															  dataCreazione, null, null, null, null, null, null, null, null, annoDocumento, null, null, dataProtocollo, null, null, null, 
															  null, null, null, true, idProtocollo, numeroProtocolloEmergenza, annoProtocolloEmergenza, null, null, null, null, idTipologiaDocumento, 
															  idTipologiaProcedimento, null, null, false, null, firmaValidaPrinc,firmeValideAll, decretoLiquidazione, metadatoValiditaFirma, null,
															  null, null, null);
			
			m.setQueue(queue);

			return m;

		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero di un metadato del dettaglio del documento: ", e);
			return null;
		}
	}

}
