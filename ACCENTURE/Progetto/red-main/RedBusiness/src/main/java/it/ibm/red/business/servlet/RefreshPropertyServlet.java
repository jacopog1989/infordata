package it.ibm.red.business.servlet;

import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * The Class RefreshPropertyServlet.
 *
 * @author CPIERASC
 * 
 *         Servlet per il caricamento delle property.
 */
public class RefreshPropertyServlet extends HttpServlet {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RefreshPropertyServlet.class.getName());
	
    /**
	 * Instantiates a new refresh property servlet.
	 *
	 * @see HttpServlet#HttpServlet()
	 */
    public RefreshPropertyServlet() {
        super();
    }

    /**
	 * Do get.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 */
    @Override
    protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException {
    	try {
    		PropertiesProvider pp = PropertiesProvider.getIstance();
    		pp.refreshProperties();
    		PrintWriter writer = response.getWriter();
    		writer.print(pp);
    		writer.close();
    	} catch (Exception e) {
    		LOGGER.error("Errore nel refresh delle properties di sistema: ", e);
    	}
    }
}
