package it.ibm.red.business.enums;

import java.util.EnumSet;
import java.util.Optional;

/**
 * Identifica le colonne della tabella NODO_ORGANIGRAMMA
 *
 */
public enum OrganigrammaVisualizzazioneEnum {
	
	/**
	 * Visualizzazione 0
	 */
	VISUALIZZAZIONE0,
	
	/**
	 * Visualizzazione 1
	 */
	VISUALIZZAZIONE1,
	
	/**
	 * Visualizzazione 2
	 */
	VISUALIZZAZIONE2,
	
	/**
	 * Visualizzazione 3
	 */
	VISUALIZZAZIONE3;

	/**
	 * Ritorna l'enum passandogli il nome della colonna
	 * @param nomeColonna
	 * @return
	 */
	public static OrganigrammaVisualizzazioneEnum get(final String nomeColonna) {
		EnumSet<OrganigrammaVisualizzazioneEnum> set = EnumSet.allOf(OrganigrammaVisualizzazioneEnum.class);
		Optional<OrganigrammaVisualizzazioneEnum> organigramma = set.stream()
			.filter(a->a.toString().equals(nomeColonna))
			.findFirst();
		return organigramma.isPresent() ? organigramma.get() : null;
	}
}
