package it.ibm.red.business.service.facade;


import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio che gestisce l'inoltro.
 */
public interface IInoltraFacadeSRV extends Serializable {
	
	/**
	 * Esegue la response "Inoltra".
	 * 
	 * Consiste nell'avanzare ciascun workflow contenuto nella lista di wobNumber
	 * 
	 * @param utente mittente dell'operazione
	 * @param wobNumbers wobnumber del workflow da avanzare
	 * @param motivoAssegnazione descrizione data dall'utente delle motivazioni di esecuzione dell'operazione
	 * @return Esito dell'operazione, qualora non sia stato possibile recuperare il numeroDocumento ritorna il wobvNumber
	 */
	EsitoOperazioneDTO inoltra(UtenteDTO utente, String wobNumbers, String motivoAssegnazione);
	
	/**
	 * Esegue la response "Inoltra" avanzando ciascun workflow contenuto nella lista di wobNumber.
	 * @param utente
	 * @param wobNumbers
	 * @param motivoAssegnazione
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> inoltra(UtenteDTO utente, Collection<String> wobNumbers, String motivoAssegnazione);
}