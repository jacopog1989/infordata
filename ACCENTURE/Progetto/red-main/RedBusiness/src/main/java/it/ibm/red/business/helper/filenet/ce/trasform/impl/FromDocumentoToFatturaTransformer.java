package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.filenet.api.collection.FolderSet;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.Document;
import com.filenet.api.core.Folder;

import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.IContattoDAO;
import it.ibm.red.business.dao.IIterApprovativoDAO;
import it.ibm.red.business.dao.IMezzoRicezioneDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailFatturaFepaDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.MezzoRicezioneDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoRubricaEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Trasformer dal Documento alla Fattura.
 */
public class FromDocumentoToFatturaTransformer extends TrasformerCE<DetailFatturaFepaDTO> {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 8212037904935061830L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentoToFatturaTransformer.class.getName());

	/**
	 * Dao per recuperare la tipologia di documento.
	 */
	private ITipologiaDocumentoDAO tipologiaDocumentoDAO;

	/**
	 * Dao nodo.
	 */
	private INodoDAO nodoDAO;
	
	/**
	 * DAO per recuperare l'aoo e l'ente
	 */
	private IAooDAO aooDAO;

	/**
	 * Dao per la gestione dei mezzi di ricezione.
	 */
	private IMezzoRicezioneDAO mezzoRicezioneDAO;
	
	/**
	 * Dao per recuperare il tipo procedimento
	 */
	private ITipoProcedimentoDAO tipoProcedimentoDAO;
	
	/**
	 *  Dao per recuperare le info di contatto dell'utente
	 */
	private IContattoDAO contattoDAO;
	/**
	 * Dao per il recupero dell'iter approvativo.
	 */
	private IIterApprovativoDAO iterApprovativoDAO;

	/**
	 * DAO.
	 */
	private IUtenteDAO utenteDAO;
	
	/**
	 * Costruttore del trasformer, inizializza i dao che utilizza il trasformer.
	 */
	public FromDocumentoToFatturaTransformer() {
		super(TrasformerCEEnum.FROM_DOCUMENT_TO_FATTURA);
		tipologiaDocumentoDAO = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoDAO.class);
		nodoDAO = ApplicationContextProvider.getApplicationContext().getBean(INodoDAO.class);
		aooDAO = ApplicationContextProvider.getApplicationContext().getBean(IAooDAO.class);
		mezzoRicezioneDAO = ApplicationContextProvider.getApplicationContext().getBean(IMezzoRicezioneDAO.class);
		tipoProcedimentoDAO = ApplicationContextProvider.getApplicationContext().getBean(ITipoProcedimentoDAO.class);
		contattoDAO = ApplicationContextProvider.getApplicationContext().getBean(IContattoDAO.class);
		iterApprovativoDAO = ApplicationContextProvider.getApplicationContext().getBean(IIterApprovativoDAO.class);

		utenteDAO = ApplicationContextProvider.getApplicationContext().getBean(IUtenteDAO.class);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE#trasform(com.filenet.api.core.Document, java.sql.Connection).
	 */
	@Override
	public DetailFatturaFepaDTO trasform(final Document document, final Connection connection) {
		if (document == null) {
			return null;
		}
		
		try {
			DetailFatturaFepaDTO d = new DetailFatturaFepaDTO();
			d.setGuid(StringUtils.cleanGuidToString(document.get_Id()));
			d.setDocumentTitle((String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
			
			d.setIdFascicoloFepa((String) getMetadato(document, PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA));
	
			d.setCreator(document.get_Creator());
			d.setAnnoDocumento((Integer) getMetadato(document, PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY));
			d.setNumeroDocumento((Integer) getMetadato(document, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY));
			d.setIdFormatoDocumento((Integer) getMetadato(document, PropertiesNameEnum.ID_FORMATO_DOCUMENTO));
			if (d.getIdFormatoDocumento() != null) {
				d.setFormatoDocumentoEnum(FormatoDocumentoEnum.getEnumById(d.getIdFormatoDocumento().longValue()));
			}
			
			d.setIdUtenteCreatore((Integer) getMetadato(document, PropertiesNameEnum.UTENTE_CREATORE_ID_METAKEY));
			d.setIdUfficioCreatore((Integer) getMetadato(document, PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY));
			
			if (d.getIdUfficioCreatore() > 0) {
				d.setUfficioMittente(nodoDAO.getNodo(Long.valueOf(d.getIdUfficioCreatore()), connection));
			}
			
			// tipologia documento start
			d.setIdTipologiaDocumento((Integer) getMetadato(document, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY));
			if (d.getIdTipologiaDocumento() != null) {
				TipologiaDocumentoDTO tipologiaDocumento = tipologiaDocumentoDAO.getById(d.getIdTipologiaDocumento(), connection);
				d.setDescTipologiaDocumento(tipologiaDocumento.getDescrizione());
				d.setDocumentClass(tipologiaDocumento.getDocumentClass());
			}
			// tipologia documento end
			
			d.setOggetto((String) getMetadato(document, PropertiesNameEnum.OGGETTO_METAKEY));
			d.setAnnoProtocollo((Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY));
			d.setNumeroProtocollo((Integer) getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY));
			d.setTipoProtocollo((Integer) getMetadato(document, PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY));
			d.setIdProtocollo((String) getMetadato(document, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY));
			d.setDataProtocollo((Date) getMetadato(document, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY));
			
	
			d.setIdCategoriaDocumento((Integer) getMetadato(document, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY));
			d.setNomeFile((String) getMetadato(document, PropertiesNameEnum.NOME_FILE_METAKEY));
			d.setMimeType((String) getMetadato(document, PropertyNames.MIME_TYPE));
			d.setDateCreated((Date) getMetadato(document, PropertiesNameEnum.DATA_CREAZIONE_METAKEY));
			d.setDataScadenza((Date) getMetadato(document, PropertiesNameEnum.DATA_SCADENZA_METAKEY));
			
			d.setIdAOO((Integer) getMetadato(document, PropertiesNameEnum.ID_AOO_METAKEY));
			Aoo aoo = aooDAO.getAoo(d.getIdAOO().longValue(), connection);
			d.setAoo(aoo);
			
			/**ITER APPROVATIVO START******************************************/
			String iterApprovativoSemaforo = (String) getMetadato(document, PropertiesNameEnum.ITER_APPROVATIVO_METAKEY);
			if (!StringUtils.isNullOrEmpty(iterApprovativoSemaforo)) {
				IterApprovativoDTO iterApprovativoDTO = iterApprovativoDAO.getIterApprovativoById(Integer.valueOf(iterApprovativoSemaforo), connection);
				if (iterApprovativoDTO != null) {
					d.setIdIterApprovativo(iterApprovativoDTO.getIdIterApprovativo());
					d.setIterApprovativoSemaforo(iterApprovativoDTO.getDescrizione());
				}
			}
			/**ITER APPROVATIVO END********************************************/
		
			//mezzo di ricezione start
			d.setMezzoRicezione((Integer) getMetadato(document, PropertiesNameEnum.METADATO_DOCUMENTO_MEZZORICEZIONE));
			if (d.getMezzoRicezione() != null) {
				MezzoRicezioneDTO m = mezzoRicezioneDAO.getMezzoRicezioneByIdMezzo(d.getMezzoRicezione(), connection);
				d.setMezzoRicezioneDTO(m);
			}
			//mezzo di ricezione end
			
			//tipologia procedimento start
			d.setIdTipologiaProcedimento((Integer) getMetadato(document, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY));
			if (d.getIdTipologiaProcedimento() != null) {
				TipoProcedimento t = tipoProcedimentoDAO.getTPbyId(connection, d.getIdTipologiaProcedimento()); 
				d.setDescTipoProcedimento(t.getDescrizione());
			}
			//tipologia procedimento end
			
			d.setNote((String) getMetadato(document, PropertiesNameEnum.NOTE_METAKEY));

			/**FOLDER START***********************/
			FolderSet fs = document.get_FoldersFiledIn();
			if (fs != null) {
				String folder = null;
				Iterator<?> itf = fs.iterator();
				if (itf.hasNext()) {
					folder = ((Folder) itf.next()).get_FolderName();
				}
				d.setFolder(folder);
			}
			/**FOLDER END*************************/
			
			
			String mittente = (String) getMetadato(document, PropertiesNameEnum.METADATO_DOCUMENTO_MITTENTE);
			if (!StringUtils.isNullOrEmpty(mittente)) {
				d.setMittente(mittente);
				String[] mittSplit = mittente.split(","); 
				Long idContatto = Long.valueOf(mittSplit[0]); 
				Contatto c = contattoDAO.getContattoByID(idContatto, connection);
				//Questa cosa e' profondamente sbagliata ma RED fa così quindi semplicemente copio il comportamente
				//si assume quindi che il l'id estratto dal campo mittente di filenet dovrebbe essere l'id del contatto
				//se però non trova nulla nei contatti allora l'id corrisponde all'id sulla tabella utente
				if (c == null) {
					c = new Contatto();
					Utente u = utenteDAO.getUtente(idContatto, connection);
					c.setTipoRubricaEnum(TipoRubricaEnum.INTERNO);
					c.setIdUtente(u.getIdUtente());
					c.setNome(u.getNome());
					c.setCognome(u.getCognome());
					c.setAliasContatto(u.getUsername());
					c.setIdNodo(d.getUfficioMittente().getIdNodo());
				}
				d.setMittenteContatto(c);
			} else {
				//### DESTINATARI #########################################################################################
				Collection<?> inDestinatari = (Collection<?>) getMetadato(document, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
				// Si normalizza il metadato per evitare errori comuni durante lo split delle singole stringhe dei destinatari
				List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(inDestinatari);
				
				if (!CollectionUtils.isEmpty(destinatariDocumento)) {
					List<DestinatarioRedDTO> list = new ArrayList<>();
					
					for (String[] destSplit : destinatariDocumento) {
						
						if (destSplit.length >= 5) {
							DestinatarioRedDTO desDTO = null;
							
							TipologiaDestinatarioEnum tde = TipologiaDestinatarioEnum.getByTipologia(destSplit[3]);
							if (TipologiaDestinatarioEnum.INTERNO == tde) {
								Long idNodo = Long.valueOf(destSplit[0]);
								Long idUtente = Long.valueOf(destSplit[1]);
								String  aliasContatto = destSplit[2];
								desDTO = new DestinatarioRedDTO(idNodo, idUtente, destSplit[3], destSplit[4]);
								Contatto c = new Contatto(null, null, null, null, null, null, null, aliasContatto);
								c.setIdNodo(idNodo);
								c.setIdUtente(idUtente);
								desDTO.setContatto(c);
								desDTO.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.INTERNO);
							} else if (TipologiaDestinatarioEnum.ESTERNO == tde) {
								Long idContatto = Long.valueOf(destSplit[0]);
								Integer idMezzo = Integer.valueOf(destSplit[2]);
								desDTO = new DestinatarioRedDTO(idMezzo, destSplit[3], destSplit[4]);
								Contatto c = contattoDAO.getContattoByID(idContatto, connection);
								desDTO.setContatto(c);
								desDTO.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
							}
							
							list.add(desDTO);
						}
						
					}
					d.setDestinatari(list);
				}
				//###################################################################################################################
			}
			
			
			d.setTipoProtocollo((Integer) getMetadato(document, PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY));
			
			return d;
			
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero di un metadato del dettaglio del documento: ", e);
			return null;
		}
	}

}
