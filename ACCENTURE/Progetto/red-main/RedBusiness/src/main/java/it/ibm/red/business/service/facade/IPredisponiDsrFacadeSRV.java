package it.ibm.red.business.service.facade;

import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio di predisposizione Dsr.
 */
public interface IPredisponiDsrFacadeSRV extends Serializable {

	/**
	 * I destinatari per la predisposizione del docuemnto sono costanti.
	 * 
	 * @return
	 */
	List<UtenteDTO> retrieveDestinatariDsr();

	/**
	 * 
	 * @param detail in realtà serve solo il documentDetail
	 * @param utente
	 * @return
	 */
	InputStream retrieveContentDsr(String documentTitle, UtenteDTO utente);

	/**
	 * Permette la predisposizione della dichiarazione.
	 * 
	 * @param utente
	 * @param documentTitle
	 * @param wobNumber
	 * @param oggetto
	 * @param allegati
	 * @param content
	 * @return
	 */

	DetailDocumentRedDTO getDetailDSRPredisposta(String documentTitle, String wobNumber, UtenteDTO utente);

	/**
	 * Aggiorna la dichiarazione.
	 * @param utente
	 * @param detail
	 * @param contentVariato
	 * @return esito del salvataggio del documento
	 */
	EsitoSalvaDocumentoDTO aggiornaDichiarazione(UtenteDTO utente, DetailDocumentRedDTO detail, boolean contentVariato);


	/**
	 * Aggiorna la dichiarazione.
	 * @param utente
	 * @param documentTitle
	 * @param wobNumber
	 * @param nomeFile
	 * @param oggetto
	 * @param allegati
	 * @param content
	 * @param contentVariato
	 * @return esito del salvataggio del documento
	 */
	EsitoSalvaDocumentoDTO aggiornaDichiarazione(UtenteDTO utente, String documentTitle, String wobNumber,
			String nomeFile, String oggetto, List<AllegatoDTO> allegati, byte[] content, boolean contentVariato);

	/**
	 * Predispone la dichiarazione.
	 * @param utente
	 * @param documentTitle
	 * @param wobNumber
	 * @param nomeFile
	 * @param oggetto
	 * @param allegati
	 * @param content
	 * @return esito del salvataggio del documento
	 */
	EsitoSalvaDocumentoDTO predisponiDichiarazione(UtenteDTO utente, String documentTitle, String wobNumber,
			String nomeFile, String oggetto, List<AllegatoDTO> allegati, byte[] content);

	/**
	 * Costruisce il detail perché permette la predisposizione della dichiarazione.
	 * 
	 * Non ha gli allegati del vecchio documento ma solo il documento principale.
	 * Il documento restituito avrà gli stessi dati posti in input.
	 * 
	 * @param documentTitle
	 *  Id del documento recuperato dal master.
	 * @param wobNumber
	 *  Id del PE per il documento.
	 * @param numeroDocumento
	 *  Id del wobNumber per l'utente.
	 * @param utente
	 *  utente che ha richiesto l'operazione.
	 * @return
	 *  Una dchiarazione basata sul documento da predisporre. 
	 */
	DetailDocumentRedDTO getDocumentDetail(String documentTitle, String wobNumber, Integer numeroDocumento,	UtenteDTO utente);

	/**
	 * Permette di verificare che l'utente abbia i privilegi necessari a modificare il docuemnto.
	 * 
	 * @param detail
	 * @param utente
	 * @return
	 */
	boolean isModificabile(DetailDocumentRedDTO detail, UtenteDTO utente);

}
