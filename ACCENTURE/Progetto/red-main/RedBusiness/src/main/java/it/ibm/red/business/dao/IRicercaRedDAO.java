package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.persistence.model.RicercaAvanzataSalvata;

/**
 * 
 * @author mcrescentini
 *	
 *	DAO per la gestione della ricerca RED.
 */
public interface IRicercaRedDAO extends Serializable {
	
	/**
	 * Ottiene tutte le ricerche avanzate salvate per utente.
	 * @param idUtente
	 * @param idUfficio
	 * @param idRuolo
	 * @param con
	 * @return lista delle ricerche avanzate salvate
	 */
	List<RicercaAvanzataSalvata> getAllRicercheSalvateByUtente(long idUtente, long idUfficio, long idRuolo, Connection con);
	
	/**
	 * Inserisce la ricerca avanzata salvata sulla base dati.
	 * @param ricercaAvanzataSalvata
	 * @param con
	 * @return id della ricerca
	 */
	Integer insertRicercaAvanzataSalvata(RicercaAvanzataSalvata ricercaAvanzataSalvata, Connection con);
	
	/**
	 * Elimina la ricerca avanzata salvata sulla base dati.
	 * @param idRicerca
	 * @param con
	 */
	void eliminaRicercaAvanzataSalvata(int idRicerca, Connection con);
	
	/**
	 * Inserisce record nella tabella ricerca_campo_valore.
	 * @param idRicerca
	 * @param campo
	 * @param valore
	 * @param con
	 */
	void insertRicercaCampoValore(int idRicerca, String campo, String valore, Connection con);
	
	/**
	 * Ottiene il campo e il valore tramite l'id della ricerca.
	 * @param idRicerca
	 * @param con
	 * @return mappa campo valore
	 */
	Map<String, String> getCampiValoriByIdRicerca(int idRicerca, Connection con);
}