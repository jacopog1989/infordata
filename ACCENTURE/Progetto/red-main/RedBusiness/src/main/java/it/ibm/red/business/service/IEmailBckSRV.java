package it.ibm.red.business.service;

import java.sql.Connection;

import javax.mail.Message;

import it.ibm.red.business.service.facade.IEmailBckFacadeSRV;

/**
 * The Interface IEmailBckSRV.
 *
 * @author mcrescentini
 * 
 *  Interfaccia del servizio per il backup delle mail.
 */
public interface IEmailBckSRV extends IEmailBckFacadeSRV {

	/**
	 * Salva il backup della mail.
	 * @param message
	 * @param messageId
	 * @param indirizzoEmailCasella
	 * @param mittente
	 * @param isPec
	 * @param isNotifica
	 * @param con
	 * @return
	 */
	int salvaEmailBck(Message message, String messageId, String indirizzoEmailCasella, String mittente, boolean isPec,
			boolean isNotifica, Connection con);

}