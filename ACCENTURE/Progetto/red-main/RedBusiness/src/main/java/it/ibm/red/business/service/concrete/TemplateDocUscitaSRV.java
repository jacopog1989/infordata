package it.ibm.red.business.service.concrete;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.dao.ITemplateDAO;
import it.ibm.red.business.dao.ITestoPredefinitoDAO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.TemplateDTO;
import it.ibm.red.business.dto.TemplateMetadatoDTO;
import it.ibm.red.business.dto.TemplateMetadatoValueDTO;
import it.ibm.red.business.dto.TemplateMetadatoVersion;
import it.ibm.red.business.dto.TemplateValueDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.adobe.AdobeLCHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.html.HtmlFileHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.ITemplateDocUscitaSRV;
import it.ibm.red.business.utils.DateUtils;

/**
 * The Class TemplateDocUscitaSRV.
 *
 * @author adilegge
 */
@Service
@Component
public class TemplateDocUscitaSRV extends AbstractService implements ITemplateDocUscitaSRV {

	/**
	 * Tag chiusura br.
	 */
	private static final String BR = "<br/>";

	/**
	 * Messaggio errore recupero template documento in uscita.
	 */
	private static final String ERROR_RECUPERO_TEMPLATE_DOC_USCITA_MSG = "Errore durante il recupero dei template doc uscita ";

	/**
	 * Enum che elenca i valori di un dettaglio documento.
	 */
	public enum ValueFromDettaglioDocumento {

		/**
		 * Valore.
		 */
		AOO,

		/**
		 * Valore.
		 */
		UFFICIO,

		/**
		 * Valore.
		 */
		OGGETTO,

		/**
		 * Valore.
		 */
		DESTINATARI,

		/**
		 * Valore.
		 */
		SYSDATE,

		/**
		 * Valore.
		 */
		PROTOCOLLO,
		
		/**
		 * Valore.
		 */
		DESTINATARITO,
		
		/**
		 * Valore.
		 */
		DESTINATARICC;
	}

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TemplateDocUscitaSRV.class.getName());

	/**
	 * DAO.
	 */
	@Autowired
	private ITemplateDAO templateDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ITestoPredefinitoDAO testoPredefinitoDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * @see it.ibm.red.business.service.facade.ITemplateDocUscitaFacadeSRV#getTemplateDocUscita(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<TemplateDTO> getTemplateDocUscita(final UtenteDTO utente) {
		Connection con = null;
		List<TemplateDTO> all = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			all = templateDAO.getAll(con, utente.getIdAoo(), false);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_TEMPLATE_DOC_USCITA_MSG, e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		return all;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITemplateDocUscitaFacadeSRV#getMetadati(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.TemplateDTO).
	 */
	@Override
	public List<TemplateMetadatoDTO> getMetadati(final UtenteDTO utente, final TemplateDTO template) {
		Connection con = null;
		List<TemplateMetadatoDTO> metadati = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);

			metadati = templateDAO.getMetadati(con, template.getIdTemplate());
			for (final TemplateMetadatoDTO metadato : metadati) {
				if (metadato.getTestoPredefinito() != null) {
					metadato.setTestiPredefiniti(testoPredefinitoDAO.getTestiByTipoAndAOO(metadato.getTestoPredefinito(), utente.getIdAoo(), con));
				}
			}

		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_TEMPLATE_DOC_USCITA_MSG, e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		return metadati;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITemplateDocUscitaFacadeSRV#getValues(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	public Map<TemplateValueDTO, List<TemplateMetadatoValueDTO>> getValues(final UtenteDTO utente, final String idDocumento) {
		Connection con = null;
		final Map<TemplateValueDTO, List<TemplateMetadatoValueDTO>> values = new HashMap<>();
		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final TemplateValueDTO templateValue = templateDAO.getTemplateValue(con, idDocumento);

			if (templateValue != null) {
				final List<TemplateMetadatoValueDTO> metadatiValues = templateDAO.getMetadatiValues(con, idDocumento, templateValue.getIdTemplate());
				for (final TemplateMetadatoValueDTO metadato : metadatiValues) {
					if (metadato.getTestoPredefinito() != null) {
						metadato.setTestiPredefiniti(testoPredefinitoDAO.getTestiByTipoAndAOO(metadato.getTestoPredefinito(), utente.getIdAoo(), con));
					}
				}

				values.put(templateValue, metadatiValues);
			}

			return values;

		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_TEMPLATE_DOC_USCITA_MSG, e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITemplateDocUscitaFacadeSRV#generaDocumento(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.util.List).
	 */
	@Override
	public FileDTO generaDocumento(final UtenteDTO utente, final String guidTemplate, final String nomeFile, final List<TemplateMetadatoValueDTO> metadati) {
		FileDTO file = null;
		StringBuilder content = null;

		IFilenetCEHelper fceh = null;

		try {
			final Aoo aoo = aooSRV.recuperaAoo(utente.getIdAoo().intValue());
			final String usernameKey = aoo.getCodiceAoo() + "." + PropertiesNameEnum.AOO_SISTEMA_AMMINISTRATORE.getKey();
			final String username = PropertiesProvider.getIstance().getParameterByString(usernameKey);
			final AooFilenet aooFilenet = aoo.getAooFilenet();

			fceh = FilenetCEHelperProxy.newInstance(username, aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(), aooFilenet.getObjectStore(),
					aoo.getIdAoo());

			final Document templateFn = fceh.getDocumentByGuid(guidTemplate);
			final byte[] contentByteArray = FilenetCEHelper.getDocumentContentAsByte(templateFn);
			content = new StringBuilder(new String(contentByteArray, StandardCharsets.UTF_8.name()));

			LOGGER.info("Template " + guidTemplate + " recuperato dal CE");
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del template " + guidTemplate + " su filenet ", e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}
		try {

			final InputStream isHtml = new HtmlFileHelper().createTemplateDocUscita(content, metadati);

			final AdobeLCHelper adobeHelper = new AdobeLCHelper();
			InputStream reportPDF = null;

			reportPDF = adobeHelper.htmlToPdf(IOUtils.toByteArray(isHtml));
			file = new FileDTO(nomeFile, IOUtils.toByteArray(reportPDF), ContentType.PDF);
			LOGGER.info("Template " + guidTemplate + " trasformato in PDF con successo");

		}catch (final RedException e) {
			LOGGER.error("Errore durante la trasformazione in pdf del template: " + e.getMessage() , e);
			throw new RedException("Errore durante la trasformazione in pdf del template: " + e.getMessage(), e);
		}
		catch (final Exception e) {
			LOGGER.error("Errore durante la trasformazione in pdf del template " + guidTemplate, e);
			throw new RedException("Errore durante la trasformazione in pdf del template", e);
		}

		return file;
	}

	/**
	 * @see it.ibm.red.business.service.ITemplateDocUscitaSRV#aggiornaDatiDocumento(int,
	 *      java.util.Map, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public void aggiornaDatiDocumento(final int idDocumento, final Map<TemplateValueDTO, List<TemplateMetadatoValueDTO>> templateDocUscitaMap, final Long idUtenteCreatore,
			final Connection conn) {
		try {
			final Integer versione = templateDAO.getVersionTemplate(conn, Constants.EMPTY_STRING + idDocumento);
			if (templateDocUscitaMap != null && !templateDocUscitaMap.isEmpty()) {

				final TemplateValueDTO template = templateDocUscitaMap.keySet().iterator().next();
				template.setIdDocumento(Constants.EMPTY_STRING + idDocumento);
				final List<TemplateMetadatoValueDTO> metadati = templateDocUscitaMap.get(template);
				if (versione != null) {
					templateDAO.insertTemplateValueNew(conn, template, metadati, versione + 1, idUtenteCreatore);
				} else {
					templateDAO.insertTemplateValueNew(conn, template, metadati, 1, idUtenteCreatore); // In creazione uscita inserisco la prima versione
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'aggiornamento dei metadati del documento " + idDocumento, e);
			throw new RedException("Errore durante l'aggiornamento dei metadati del documento", e);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ITemplateDocUscitaSRV#getMetadati(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public List<TemplateMetadatoDTO> getMetadati(final String guidTemplate, final Connection conn) {

		try {
			return templateDAO.getMetadati(conn, guidTemplate);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei metadati del template " + guidTemplate, e);
			throw new RedException("Errore durante il recupero dei metadati del template", e);
		}

	}

	/**
	 * @see it.ibm.red.business.service.facade.ITemplateDocUscitaFacadeSRV#getMetadatoValue(java.lang.String,
	 *      java.util.List, java.lang.String, java.lang.Integer, java.lang.Integer,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.Integer).
	 */
	@Override
	public String getMetadatoValue(final String dbDefaultValue, final List<DestinatarioRedDTO> destinatariDoc, final String oggetto, final Integer numeroProtocollo,
			final Integer annoProtocollo, final UtenteDTO utente, final Integer idMetadatoIntestazione) {
		StringBuilder valore = null;

		if (StringUtils.isNotBlank(dbDefaultValue) && dbDefaultValue.startsWith("{") && dbDefaultValue.endsWith("}")) {

			final String defaultValue = dbDefaultValue.substring(1, dbDefaultValue.length() - 1);
			final ValueFromDettaglioDocumento defaultValueEnum = ValueFromDettaglioDocumento.valueOf(defaultValue);

			switch (defaultValueEnum) {
			case SYSDATE:
				valore = new StringBuilder(LocalDate.now().format(DateTimeFormatter.ofPattern(DateUtils.DD_MM_YYYY)));
				break;

			case DESTINATARI:
				valore = getValueFromDestinatari(destinatariDoc);
				break;

			case OGGETTO:
				valore = new StringBuilder(oggetto);
				break;

			case PROTOCOLLO:
				valore = new StringBuilder("");
				if (numeroProtocollo != null) {
					valore = new StringBuilder(numeroProtocollo).append("/").append(annoProtocollo);
				}
				break;

			case AOO:
				valore = new StringBuilder(utente.getAooDesc());
				break;

			case UFFICIO:
				valore = new StringBuilder(utente.getNodoDesc());
				break;
			case DESTINATARITO:
				valore = getValueFromSpecificDestinatario(destinatariDoc, idMetadatoIntestazione, ModalitaDestinatarioEnum.TO);
				break;
				
			case DESTINATARICC:
				valore = getValueFromSpecificDestinatario(destinatariDoc, idMetadatoIntestazione, ModalitaDestinatarioEnum.CC);
				break;

			default:
				break;
			}
		} else if(StringUtils.isNotBlank(dbDefaultValue)){
			if(dbDefaultValue!=null) {
				valore = new StringBuilder(dbDefaultValue);
			}else {
				valore = null;
			}
		}

		return valore != null ? valore.toString() : null;
	}

	/**
	 * Restituisce il value sotto forme di stringa a partire dai destinatari del
	 * documento.
	 * 
	 * @param destinatariDoc
	 *            Destinatari del documento.
	 * @param idMetadatoIntestazione
	 *            Identificativo del metadato intestazione.
	 * @param modalita
	 *            Modalita del destinatario.
	 * @return Value come stringa del documento.
	 */
	private StringBuilder getValueFromSpecificDestinatario(final List<DestinatarioRedDTO> destinatariDoc, final Integer idMetadatoIntestazione, final ModalitaDestinatarioEnum modalita) {
		StringBuilder valore = new StringBuilder("");
		
		if (!CollectionUtils.isEmpty(destinatariDoc)) {
			for (final DestinatarioRedDTO dest : destinatariDoc) {
				if (dest.getContatto() != null && modalita.equals(dest.getModalitaDestinatarioEnum())) {

					valore.append((valore.length() != 0 ? BR : "")).append(idMetadatoIntestazione != null ? "[" + idMetadatoIntestazione + "]<br/>" : "")
							.append(dest.getContatto().getAliasContatto()).append(dest.getContatto().getMailSelected() != null ? BR + dest.getContatto().getMailSelected() : "");
				}
			}
		}
		return valore;
	}

	/**
	 * Restituisce il value sotto forme di stringa a partire dai destinatari del
	 * documento.
	 * 
	 * @param destinatariDoc
	 *            Destinatari del documento da recuperare come stringa.
	 * @return String builder del valore recuperato, rappresenta una stringa vuota
	 *         se non esistono destinatari.
	 */
	private StringBuilder getValueFromDestinatari(final List<DestinatarioRedDTO> destinatariDoc) {
		StringBuilder valore = new StringBuilder("");
		
		if (!CollectionUtils.isEmpty(destinatariDoc)) {
			for (final DestinatarioRedDTO dest : destinatariDoc) {
				if (dest.getContatto() != null) {
					valore.append((valore.length() != 0 ? "\n" : "")).append(dest.getContatto().getAliasContatto());
				}
			}
		}
		return valore;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITemplateDocUscitaFacadeSRV#getVersioniDocumento(java.lang.Integer).
	 */
	@Override
	public List<TemplateMetadatoVersion> getVersioniDocumento(final Integer idDocumento) {
		Connection conn = null;
		List<TemplateMetadatoVersion> versionList = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			versionList = templateDAO.getVersioniDocumento(idDocumento, conn);
		} catch (final Exception ex) {
			LOGGER.error("Errore durante il recupero delle versione del template" + ex);
			throw new RedException("Errore durante il recupero delle versione del template" + ex);
		} finally {
			closeConnection(conn);
		}
		return versionList;

	}

	/**
	 * @see it.ibm.red.business.service.facade.ITemplateDocUscitaFacadeSRV#getMetadatiNew(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.TemplateDTO).
	 */
	@Override
	public List<TemplateMetadatoDTO> getMetadatiNew(final UtenteDTO utente, final TemplateDTO template) {
		
		return getMetadati(utente, template);
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITemplateDocUscitaFacadeSRV#getValueMetadato(java.lang.String,
	 *      java.lang.Integer, java.lang.Integer).
	 */
	@Override
	public List<TemplateMetadatoValueDTO> getValueMetadato(final String idTemplate, final Integer idDocumento, final Integer versione) {
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			return templateDAO.getValueMetadato(idTemplate, idDocumento, versione, conn);
		} catch (final Exception ex) {
			LOGGER.error("Errore nel recupero del valore del metadato " + ex);
			throw new RedException("Errore nel recupero del valore del metadato " + ex);
		} finally {
			closeConnection(conn);
		}
	}
}
