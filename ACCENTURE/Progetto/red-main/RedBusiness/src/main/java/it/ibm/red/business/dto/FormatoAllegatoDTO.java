package it.ibm.red.business.dto;

import it.ibm.red.business.enums.FormatoAllegatoEnum;

/**
 * Dto dei formati degli allegati 
 *
 */
public class FormatoAllegatoDTO extends AbstractDTO {
	
	private static final long serialVersionUID = -4824138583126078799L;

	/**
	 * Rapprtesenta nell'applicazione il formato degli allegati
	 */
	private FormatoAllegatoEnum formatoAllegato;
	
	/**
	 * label da mostrare all'utente che descrive il formato dell'allegato
	 */
	private String descrizione;

	/**
	 * Costruttore del DTO.
	 * @param formatoAllegato
	 * @param descrizione
	 */
	public FormatoAllegatoDTO(final FormatoAllegatoEnum formatoAllegato, final String descrizione) {
		this.formatoAllegato = formatoAllegato;
		this.descrizione = descrizione;
	}
	
	/**
	 * Restituisce il formato dell'allegato.
	 * @return formato allegato
	 */
	public FormatoAllegatoEnum getFormatoAllegato() {
		return formatoAllegato;
	}

	/**
	 * Restituisce la descrizione.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}
	
}
