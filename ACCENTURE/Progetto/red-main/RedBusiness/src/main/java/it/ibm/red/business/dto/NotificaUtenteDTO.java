package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * Classe NotificaUtenteDTO.
 */
public abstract class NotificaUtenteDTO extends AbstractDTO implements Serializable {

	/** 
	 * The Constant serialVersionUID. 
	 */
	private static final long serialVersionUID = 8717824336534718934L;
	
	/**
	 * Flag da leggere
	 */
	private boolean daLeggere;
	
	
	/** 
	 *
	 * @return the daLeggere
	 */
	public boolean isDaLeggere() {
		return daLeggere;
	}
	
	
	/** 
	 *
	 * @param daLeggere the daLeggere to set
	 */
	public void setDaLeggere(final boolean daLeggere) {
		this.daLeggere = daLeggere;
	}

	
	/**
	 * Ciascun oggetto che estende questa classe astratta deve implementare una sua logica per stabilire
	 * se la notifica deve essere visualizzata come "da leggere", quindi con uno stile proprio
	 * che la metta in evidenza rispetto alle altre notifiche.
	 */
	public abstract void calcolaDaLeggere();
}