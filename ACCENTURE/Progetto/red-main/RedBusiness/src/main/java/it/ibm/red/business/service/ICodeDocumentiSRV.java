package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.AssegnatarioDTO;
import it.ibm.red.business.enums.StatoLavorazioneEnum;
import it.ibm.red.business.service.facade.ICodeDocumentiFacadeSRV;

/**
 * Interface del servizio gestione code documenti.
 */
public interface ICodeDocumentiSRV extends ICodeDocumentiFacadeSRV {
	
	/**
	 * Elimina gli item recall nella documentoutentestato per il documento in input
	 * 
	 * @param idDocumento
	 * @param connection
	 */
	void deleteRecallItem(Integer idDocumento, Connection connection); 
	
	/**
	 * Elimina tutti i record relativi al documento in input e, per ogni assegnatario, inserisce lo stato CHIUSO per
	 * l'ufficio e, se presente, anche per l'utente.
	 * 
	 * @param idDocumento
	 * @param assegnatari
	 * @param connection
	 */
	void checkChiusuraAllacci(Integer idDocumento, List<AssegnatarioDTO> assegnatari, Connection connection);

	/**
	 * Rimuove gli item relativi al nodo-utente per il documento.
	 * @param documentTitle - document title
	 * @param idUfficio - id dell'ufficio
	 * @param idUtente - id dell'utente
	 */
	void rimuoviDocumentoCodeApplicative(String documentTitle, Long idUfficio, Long idUtente);

	/**
	 * Rimuove gli item relativi al nodo per il documento.
	 * @param documentTitle - document title
	 * @param idUfficio - id dell'ufficio
	 */
	void rimuoviDocumentoCodeApplicative(String documentTitle, Long idUfficio);

	/**
	 * Aggiorna lo stato della lavorazione del documento.
	 * @param documentTitle - document title
	 * @param idUfficio - id dell'ufficio
	 * @param idUtente - id dell'utente
	 * @param statoLavorazione - stato lavorazione
	 */
	void aggiornaStatoDocumentoLavorazione(String documentTitle, Long idUfficio, Long idUtente, StatoLavorazioneEnum statoLavorazione);

	/**
	 * Annulla l'utente per il documento.
	 * @param documentTitle - document title
	 * @param idUfficio - id dell'ufficio
	 * @param idUtente - id dell'utente
	 */
	void annullaUtenteDocumento(String documentTitle, Long idUfficio, Long idUtente);

	/**
	 * Verifica se ci sono item relativi al nodo per il documento.
	 * @param documentTitle - document title
	 * @param idUfficio - id dell'ufficio
	 * @return true se ci sono item, false in caso contrario
	 */
	boolean hasItemNodo(String documentTitle, Long idUfficio);
	
}