package it.ibm.red.business.dto;

import it.ibm.red.business.dao.AbstractDAO;

/**
 * @author SLac
 * 
 *	Mappa la tabella TEMPLATE
 */
public class CategoriaEventoDTO extends AbstractDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7307294604860930799L;

	/**
	 * REPLACE_IDDOCUMENTO.
	 */
	public static final String REPLACE_IDDOCUMENTO = "#IDDOCUMENTO";

	/**
	 * REPLACE_OGGETTODOCUMENTO.
	 */
	public static final String REPLACE_OGGETTODOCUMENTO = "#OGGETTODOCUMENTO";

	/**
	 * REPLACE_DESCEVENTO.
	 */
	public static final String REPLACE_DESCEVENTO = "#DESCEVENTO";

	/**
	 * REPLACE_DATASCADENZA.
	 */
	public static final String REPLACE_DATASCADENZA = "#DATASCADENZA";
	
	/**
	 * REPLACE_URLLIBROFIRMAEVO.
	 */
	public static final String REPLACE_URLLIBROFIRMAEVO = "#URLLIBROFIRMAEVO";

	/**
	 * REPLACE_URLLIBROFIRMA.
	 */
	public static final String REPLACE_URLLIBROFIRMA = "#URLLIBROFIRMA";
	/**
	 * REPLACE_URLLIBROFIRMAEVOALT.
	 */
	public static final String REPLACE_URLLFEVO = "#URLLFEVO";

	/**
	 * REPLACE_URLLIBROFIRMAALT.
	 */
	public static final String REPLACE_URLLFMOB = "#URLLFMOB";

	/**
	 * REPLACE_UTENTEMITTENTE.
	 */
	public static final String REPLACE_UTENTEMITTENTE = "#UTENTEMITTENTE";

	/**
	 * REPLACE_UFFICIOMITTENTE.
	 */
	public static final String REPLACE_UFFICIOMITTENTE = "#UFFICIOMITTENTE";

	/**
	 * REPLACE_INFORMAZIONIFLUSSO.
	 */
	public static final String REPLACE_INFORMAZIONIFLUSSO = "#INFORMAZIONIFLUSSO";
	
	/**
	 * REPLACE_DESTINATARINOTIFICA.
	 */
	public static final String REPLACE_DESTINATARINOTIFICA = "#DESTINATARINOTIFICA";
	
	/**
	 * Oggetto mail.
	 */
	private String oggettoMail = "";
	
	
	/**
	 * Corpo mail.
	 */
	private String corpoMail = "";
	
	/**
	 * Descrizione evento.
	 */
	private String descrizioneEvento = "";
	
	/**
	 * Identificativo categoria.
	 */
	private int idCategoria;

	/**
	 * Restituisce l'oggetto della mail.
	 * @return oggettoMail
	 */
	public String getOggettoMail() {
		return oggettoMail;
	}

	/**
	 * Imposta l'oggetto della mail.
	 * @param oggettoMail
	 */
	public void setOggettoMail(final String oggettoMail) {
		this.oggettoMail = oggettoMail;
	}

	/**
	 * Restituisce il corpo della mail.
	 * @return corpoMail
	 */
	public String getCorpoMail() {
		return corpoMail;
	}

	/**
	 * Imposta il corpo della mail.
	 * @param corpoMail
	 */
	public void setCorpoMail(final String corpoMail) {
		this.corpoMail = corpoMail;
	}

	/**
	 * Restituisce la descrizione dell'evento.
	 * @return descrizioneEvento
	 */
	public String getDescrizioneEvento() {
		return descrizioneEvento;
	}

	/**
	 * Imposta la descrizione dell'evento.
	 * @param descrizioneEvento
	 */
	public void setDescrizioneEvento(final String descrizioneEvento) {
		this.descrizioneEvento = descrizioneEvento;
	}

	/**
	 * @return the idCategoria
	 */
	public int getIdCategoria() {
		return idCategoria;
	}

	/**
	 * @param idCategoria the idCategoria to set
	 */
	public void setIdCategoria(final int idCategoria) {
		this.idCategoria = idCategoria;
	}

	/**
	 * Sostituisce le stringhe definite da oldChar con le stringhe definite da newChar.
	 * Esegue l'azione sia sull'oggetto che sul corpo della mail.
	 * @param oldChar
	 * @param newChar
	 */
	public void replace(final String oldChar, final String newChar) {
		if (!newChar.isEmpty() && !oggettoMail.isEmpty() && !corpoMail.isEmpty()) {
			oggettoMail = oggettoMail.replace(oldChar, newChar);
			corpoMail = corpoMail.replace(oldChar, newChar);
		}
	}

	
}
