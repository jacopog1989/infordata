package it.ibm.red.business.persistence.model;

import java.io.Serializable;
import java.util.List;

/**
 * Model di un gruppo.
 */
public class Gruppo implements Serializable {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -4521280033276133441L;

	/**
     * Gruppo.
     */
	private int idGruppo;

    /**
     * Nodo.
     */
	private int idNodo;

    /**
     * Flag cancellato.
     */
	private boolean isCancellato;

    /**
     * Descrizione.
     */
	private String descrizione;

    /**
     * Nome del gruppo.
     */
	private String nomeGruppo;

    /**
     * Lista contatti.
     */
	private List<Contatto> listContatto;

	/**
	 * Resituisce l'id del gruppo.
	 * @return idGruppo
	 */
	public int getIdGruppo() {
		return idGruppo;
	}

	/**
	 * Imposta l'id del gruppo.
	 * @param idGruppo
	 */
	public void setIdGruppo(final int idGruppo) {
		this.idGruppo = idGruppo;
	}

	/**
	 * Restituisce l'id dell'ufficio.
	 * @return idNodo
	 */
	public int getIdNodo() {
		return idNodo;
	}

	/**
	 * Imposta l'id dell'ufficio.
	 * @param idNodo
	 */
	public void setIdNodo(final int idNodo) {
		this.idNodo = idNodo;
	}

	/**
	 * Restituisce true se il gruppo è stato cancellato.
	 * @return isCancellato
	 */
	public boolean isCancellato() {
		return isCancellato;
	}

	/**
	 * Imposta un booleano che definisce se il gruppo è stato cancellato.
	 * @param isCancellato
	 */
	public void setCancellato(final boolean isCancellato) {
		this.isCancellato = isCancellato;
	}

	/**
	 * Restituisce la descrizione del gruppo.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Imposta la descrizione del gruppo.
	 * @param descrizione
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * Restituisce il nome del gruppo.
	 * @return numeroGruppo
	 */
	public String getNomeGruppo() {
		return nomeGruppo;
	}

	/**
	 * Imposta il nome del gruppo.
	 * @param nomeGruppo
	 */
	public void setNomeGruppo(final String nomeGruppo) {
		this.nomeGruppo = nomeGruppo;
	}

	/**
	 * Restituisce la lista di contatti presenti nel gruppo.
	 * @return listContatto
	 */
	public List<Contatto> getListContatto() {
		return listContatto;
	}

	/**
	 * Imposta la lista di contatti presenti nel gruppo.
	 * @param listContatto
	 */
	public void setListContatto(final List<Contatto> listContatto) {
		this.listContatto = listContatto;
	}
}
