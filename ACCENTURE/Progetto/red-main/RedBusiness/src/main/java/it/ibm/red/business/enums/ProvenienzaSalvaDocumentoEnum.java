/**
 * 
 */
package it.ibm.red.business.enums;

/**
 * Enum ProvenienzaSalvaDocumentoEnum.
 *
 * @author APerquoti
 */
public enum ProvenienzaSalvaDocumentoEnum {

	/** Documento proveniente da RED */
	GUI_RED(true),
	
	/** Documento proveniente da PROCESSO_AUTOMATICO_INTERNO */
	PROCESSO_AUTOMATICO_INTERNO(false),
	
	/** Documento proveniente da Dai fascicoli */
	GUI_RED_FASCICOLO(false),
	
	/** Documento proveniente da Fepa*/
	FEPA_DSR(false),
	
	/** Documento proveniente da FEPA_FATTURA_DECRETO */
	FEPA_FATTURA_DECRETO(false),
	
	/** IGEPA */
	IGEPA(false),
	
	/** IGEPA_OPF */
	IGEPA_OPF(false),
	
	/** INOLTRO_RIFIUTO_AUTOMATICO. */
	INOLTRO_RIFIUTO_AUTOMATICO(true),
	
	/** WEB_SERVICE */
	WEB_SERVICE(true),
	
	/** GUI_RED_FASCICOLO_SENZA_TRASFORMAZIONE. */
	GUI_RED_FASCICOLO_SENZA_TRASFORMAZIONE(false),
	
	/** POSTA_NPS */
	POSTA_NPS(true);
	
	/** 
	 * Flag che permette di eseguire la validazione
	 */
	private boolean eseguiValidazione;
	
	
	/**
	 * Costruttore provenienza salva documento enum.
	 *
	 * @param eseguiValidazione the esegui validazione
	 */
	ProvenienzaSalvaDocumentoEnum(final boolean eseguiValidazione) {
		this.eseguiValidazione = eseguiValidazione;
	}
	
	/** 
	 * @return the eseguiValidazione
	 */
	public boolean isEseguiValidazione() {
		return eseguiValidazione;
	}
}