/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.Date;

/**
 * @author APerquoti
 *
 */
public class ConservazioneWsDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3359673547834817624L;
	
	/**
	 * Identificativo Conservazione.
	 */
	private int idConservazione;

	/**
	 * Ticket conservazione.
	 */
	private long idTicket;
	
	/**
	 * Codice errore (se presente).
	 */
	private int codiceErrore;
	
	/**
	 * Descrizione errore (se applicabile).
	 */
	private String descrizioneErrore;
	
	/**
	 * Identificativo applicativo.
	 */
	private String idApplicativo;
	
	/**
	 * Nome documento.
	 */
	private String nomeDocumento;
	
	/**
	 * Identificativo univoco documento.
	 */
	private String uuidDocumento;
	
	/**
	 * Data acquisizione documento.
	 */
	private Date dataAcquisizione;
	
	/**
	 * Data conservazione documento.
	 */
	private Date dataConservazione;
	
	/**
	 * Stato conservaizone.
	 */
	private int stato;
	
	/**
	 * Descrizione stato conservazione.
	 */
	private String descrizioneStato;

	/**
	 * Costruttore di default.
	 */
	public ConservazioneWsDTO() {
		super();
	}

	/**
	 * @return the idConservazione
	 */
	public int getIdConservazione() {
		return idConservazione;
	}

	/**
	 * @param idConservazione the idConservazione to set
	 */
	public void setIdConservazione(final int idConservazione) {
		this.idConservazione = idConservazione;
	}

	/**
	 * @return the idTicket
	 */
	public long getIdTicket() {
		return idTicket;
	}

	/**
	 * @param idTicket the idTicket to set
	 */
	public void setIdTicket(final long idTicket) {
		this.idTicket = idTicket;
	}

	/**
	 * @return the codiceErrore
	 */
	public int getCodiceErrore() {
		return codiceErrore;
	}

	/**
	 * @param codiceErrore the codiceErrore to set
	 */
	public void setCodiceErrore(final int codiceErrore) {
		this.codiceErrore = codiceErrore;
	}

	/**
	 * @return the descrizioneErrore
	 */
	public String getDescrizioneErrore() {
		return descrizioneErrore;
	}

	/**
	 * @param descrizioneErrore the descrizioneErrore to set
	 */
	public void setDescrizioneErrore(final String descrizioneErrore) {
		this.descrizioneErrore = descrizioneErrore;
	}

	/**
	 * @return the idApplicativo
	 */
	public String getIdApplicativo() {
		return idApplicativo;
	}

	/**
	 * @param idApplicativo the idApplicativo to set
	 */
	public void setIdApplicativo(final String idApplicativo) {
		this.idApplicativo = idApplicativo;
	}

	/**
	 * @return the nomeDocumento
	 */
	public String getNomeDocumento() {
		return nomeDocumento;
	}

	/**
	 * @param nomeDocumento the nomeDocumento to set
	 */
	public void setNomeDocumento(final String nomeDocumento) {
		this.nomeDocumento = nomeDocumento;
	}

	/**
	 * @return the uuidDocumento
	 */
	public String getUuidDocumento() {
		return uuidDocumento;
	}

	/**
	 * @param uuidDocumento the uuidDocumento to set
	 */
	public void setUuidDocumento(final String uuidDocumento) {
		this.uuidDocumento = uuidDocumento;
	}

	/**
	 * @return the dataAcquisizione
	 */
	public Date getDataAcquisizione() {
		return dataAcquisizione;
	}

	/**
	 * @param dataAcquisizione the dataAcquisizione to set
	 */
	public void setDataAcquisizione(final Date dataAcquisizione) {
		this.dataAcquisizione = dataAcquisizione;
	}

	/**
	 * @return the dataConservazione
	 */
	public Date getDataConservazione() {
		return dataConservazione;
	}

	/**
	 * @param dataConservazione the dataConservazione to set
	 */
	public void setDataConservazione(final Date dataConservazione) {
		this.dataConservazione = dataConservazione;
	}

	/**
	 * @return the stato
	 */
	public int getStato() {
		return stato;
	}

	/**
	 * @param stato the stato to set
	 */
	public void setStato(final int stato) {
		this.stato = stato;
	}

	/**
	 * @return the descrizioneStato
	 */
	public String getDescrizioneStato() {
		return descrizioneStato;
	}

	/**
	 * @param descrizioneStato the descrizioneStato to set
	 */
	public void setDescrizioneStato(final String descrizioneStato) {
		this.descrizioneStato = descrizioneStato;
	}
	
	
}
