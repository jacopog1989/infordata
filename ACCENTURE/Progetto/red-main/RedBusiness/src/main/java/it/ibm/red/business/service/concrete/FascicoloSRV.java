package it.ibm.red.business.service.concrete;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.collection.RepositoryRowSet;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.Document;
import com.filenet.api.query.RepositoryRow;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.TipoFascicolo;
import it.ibm.red.business.dao.IFascicoloDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IRiferimentoStoricoDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO.DocumentoAllegabileType;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.MasterFascicoloDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedDTO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.DocumentoFascicolazioneRedFnDTO;
import it.ibm.red.business.dto.filenet.FascicoloRedFnDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.FilenetTipoOperazioneEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IEventoLogSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.service.facade.IRicercaAvanzataDocFacadeSRV;
import it.ibm.red.business.utils.DocumentoUtils;

/**
 * The Class FascicoloSRV.
 *
 * @author CPIERASC
 * 
 *         Servizio gestione fascicolo.
 */
@Service
@Component
public class FascicoloSRV extends AbstractService implements IFascicoloSRV {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Messaggio di errore reucpero documenti
	 */
	private static final String ERROR_RECUPERO_DOCUMENTI = "Errore in fase di recupero dei documenti del fascicolo";

	/**
	 * Messaggio.
	 */
	private static final String NEL_FASCICOLO = " nel fascicolo: ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FascicoloSRV.class.getName());

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * Service.
	 */
	@Autowired
	private ISecuritySRV securitySRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IFascicoloDAO fascicoloDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IEventoLogSRV eventLogSRV;

	/**
	 * Service.
	 */
	@Autowired
	private INpsSRV npsSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IRiferimentoStoricoDAO rifStoricoDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IRicercaAvanzataDocFacadeSRV ricercaAvanzataDocumentiSRV;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#fascicola(java.lang.String,
	 *      int, int, java.lang.Long,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public void fascicola(final String idFascicolo, final int idDocumento, final int tipoFascicolo, final Long idAoo, final IFilenetCEHelper fceh, final Connection con) {
		LOGGER.info("fascicola -> START. Documento: " + idDocumento + ". Fascicolo: " + idFascicolo);

		final boolean esitoFascicolazioneFilenet = fascicolaSuFilenet(idFascicolo, idDocumento, idAoo, fceh);
		if (!esitoFascicolazioneFilenet) {
			throw new RedException(
					"fascicola -> Si è verificato un errore durante l'inserimento del documento: " + idDocumento + NEL_FASCICOLO + idFascicolo + " su FileNet.");
		} else {
			// Si inserisce l'associazione tra documento e fascicolo sul DB
			fascicoloDAO.insertDocumentoFascicolo(idDocumento, Integer.parseInt(idFascicolo), tipoFascicolo, idAoo, con);
		}

		LOGGER.info("fascicola -> END. Documento: " + idDocumento + " inserito con successo nel fascicolo: " + idFascicolo);
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#fascicolaSuFilenet(java.lang.String,
	 *      int, java.lang.Long,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public boolean fascicolaSuFilenet(final String idFascicolo, final int idDocumento, final Long idAoo, final IFilenetCEHelper fceh) {
		LOGGER.info("fascicolaSuFilenet -> START. Documento: " + idDocumento + ". Fascicolo: " + idFascicolo);
		Date dataInizio;
		Date dataFine;
		boolean esito = false;

		final DocumentoFascicolazioneRedFnDTO docFascicolazione = new DocumentoFascicolazioneRedFnDTO();
		docFascicolazione.setDocumentTitle(String.valueOf(idDocumento));
		docFascicolazione.setTipoOperazione(FilenetTipoOperazioneEnum.LINK);
		docFascicolazione.setToValidate(false);

		dataInizio = new Date();
		// Si associa il documento al fascicolo su FileNet
		esito = gestisciFascicolazioneDocumento(idFascicolo, docFascicolazione, idAoo, fceh);
		dataFine = new Date();
		if (esito) {
			LOGGER.info("fascicolaSuFilenet -> Documento: " + idDocumento + " inserito nel fascicolo: " + idFascicolo + " su FileNet. Timing ===> "
					+ (dataFine.getTime() - dataInizio.getTime()) + "ms");
		} else {
			LOGGER.error("fascicolaSuFilenet -> Si è verificato un errore durante l'inserimento del documento: " + idDocumento + NEL_FASCICOLO + idFascicolo + " su FileNet.");
		}

		LOGGER.info("fascicolaSuFilenet -> END.");
		return esito;
	}

	private static boolean gestisciFascicolazioneDocumento(final String idFascicolo, final DocumentoFascicolazioneRedFnDTO docFascicolazione, final Long idAoo,
			final IFilenetCEHelper fceh) {
		LOGGER.info("gestisciFascicolazioneDocumento -> START");
		boolean esito = true;
		final FilenetTipoOperazioneEnum tipoOperazione = docFascicolazione.getTipoOperazione();

		LOGGER.info("Documento: " + docFascicolazione.getDocumentTitle() + ", operazione: " + tipoOperazione.name());
		if (FilenetTipoOperazioneEnum.LINK.equals(tipoOperazione)) {
			fceh.fascicolaDocumento(idFascicolo, docFascicolazione, idAoo);
		} else if (FilenetTipoOperazioneEnum.UNFILE.equals(tipoOperazione)) {
			fceh.rimuoviDocumentoFascicolo(idFascicolo, docFascicolazione, idAoo);
		} else if (FilenetTipoOperazioneEnum.COPY.equals(tipoOperazione)) {
			fceh.copiaDocumentoFascicolo(idFascicolo, docFascicolazione, idAoo);
		} else {
			esito = false;
		}

		LOGGER.info("gestisciFascicolazioneDocumento -> END, esito: " + esito);
		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#costruisciFascicoloRedFn(java.lang.String,
	 *      int, java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.Long).
	 */
	@Override
	public FascicoloRedFnDTO costruisciFascicoloRedFn(final String idFascicolo, final int anno, final String numero, final String descrizione,
			final String indiceClassificazione, final Long idAoo) {
		final Map<String, Object> metadatiFascicolo = new HashMap<>();
		return costruisciFascicoloRedFn(idFascicolo, metadatiFascicolo, anno, numero, descrizione, indiceClassificazione, idAoo);
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#costruisciFascicoloRedFn(java.lang.String,
	 *      java.util.Map, int, java.lang.String, java.lang.String,
	 *      java.lang.String, java.lang.Long).
	 */
	@Override
	public FascicoloRedFnDTO costruisciFascicoloRedFn(final String idFascicolo, final Map<String, Object> metadatiFascicolo, final int anno, final String numero,
			final String descrizione, final String indiceClassificazione, final Long idAoo) {
		LOGGER.info("costruisciFascicoloRedFn -> START");
		final FascicoloRedFnDTO fascicolo = new FascicoloRedFnDTO();

		// N.B. SU NSD NON IMPOSTA IL NOME DELLA CLASSE DOCUMENTALE, MA L'ATTRIBUTO
		// CLASSEDOCUMENTALE NON E' NULL PERCHE' VIENE USATO PER I METADATI
		final String classeDocumentale = pp.getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY);
		fascicolo.setClasseDocumentale(classeDocumentale); // Classe Documentale
		fascicolo.setIdFascicolo(idFascicolo); // ID

		// ### METADATI -> START
		metadatiFascicolo.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), numero + "_" + anno + "_" + descrizione); // Oggetto
		metadatiFascicolo.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), idAoo); // ID AOO
		metadatiFascicolo.put(pp.getParameterByKey(PropertiesNameEnum.STATO_FASCICOLO_METAKEY), FilenetStatoFascicoloEnum.APERTO.getNome()); // Stato
		metadatiFascicolo.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY), fascicolo.getIdFascicolo()); // Nome Fascicolo
		metadatiFascicolo.put(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), fascicolo.getIdFascicolo()); // Document Title

		String titolario = FilenetStatoFascicoloEnum.NON_CATALOGATO.getNome();
		if (StringUtils.isNotBlank(indiceClassificazione)) {
			titolario = DocumentoUtils.getTitolario(idAoo, indiceClassificazione).trim();
		}
		metadatiFascicolo.put(pp.getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY), titolario); // Titolario

		fascicolo.setMetadati(metadatiFascicolo);
		// ### METADATI -> END

		LOGGER.info("costruisciFascicoloRedFn -> END");
		return fascicolo;
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#inserisciSuFilenet(it.ibm.red.business.dto.filenet.FascicoloRedFnDTO,
	 *      it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public void inserisciSuFilenet(final FascicoloRedFnDTO fascicolo, final UtenteDTO utente, final IFilenetCEHelper fceh, final Connection con) {
		LOGGER.info("inserisciFascicoloSuFilenet -> START");
		if (!CollectionUtils.isEmpty(fascicolo.getSecurity())) {
			securitySRV.verificaSecurities(fascicolo.getSecurity(), con);
		} else {
			final List<Nodo> organigramma = nodoDAO.getAlberaturaBottomUp(utente.getIdAoo(), utente.getIdUfficio(), con);
			final ListSecurityDTO securityFascicolo = securitySRV.getSecurityByGruppo(organigramma);
			fascicolo.setSecurity(securityFascicolo);
		}

		// Inserimento del fascicolo su FileNet
		fceh.creaFascicolo(fascicolo, null, null, null);
		LOGGER.info("inserisciFascicoloSuFilenet -> END");
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#getIndiceClassificazione(java.lang.String,
	 *      long).
	 */
	@Override
	public String getIndiceClassificazione(final String idFascicolo, final long idAoo) {
		Connection connection = null;
		String indiceClassificazione = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			indiceClassificazione = fascicoloDAO.getIndiceClassificazione(idFascicolo, idAoo, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dell'indice di classificazione: " + e.getMessage(), e);
		} finally {
			closeConnection(connection);
		}

		return indiceClassificazione;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#getIndiceClassificazioneByIdFascicolo(java.lang.String,
	 *      java.lang.Long).
	 */
	@Override
	public TitolarioDTO getIndiceClassificazioneByIdFascicolo(final String idFascicolo, final Long idAoo) {
		Connection connection = null;
		TitolarioDTO t = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			t = fascicoloDAO.getIndiceClassificazioneByIdFascicolo(idFascicolo, idAoo, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dell'indice di classificazione: " + e.getMessage(), e);
		} finally {
			closeConnection(connection);
		}

		return t;
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#aggiungiFascicoloAutomatico(int,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String, java.lang.String,
	 *      java.util.Map, it.ibm.red.business.dto.ListSecurityDTO, java.util.List,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public FascicoloRedFnDTO aggiungiFascicoloAutomatico(final int idDocumento, final UtenteDTO utente, final String indiceClassificazione, final String descrizioneFascicolo,
			final Map<String, Object> metadatiFascicolo, final ListSecurityDTO securityFascicolo, final List<FascicoloRedFnDTO> fascicoliRedFn,
			final IFilenetCEHelper fceh, final Connection con) {
		LOGGER.info("aggiungiFascicoloAutomatico -> START");
		final FascicoloRedFnDTO fascicolo = creaFascicolo(utente, indiceClassificazione, descrizioneFascicolo, metadatiFascicolo, securityFascicolo, fascicoliRedFn, fceh,
				con);

		// Inserimento dell'associazione tra documento e fascicolo sul DB
		fascicoloDAO.insertDocumentoFascicolo(idDocumento, Integer.parseInt(fascicolo.getIdFascicolo()), TipoFascicolo.AUTOMATICO, utente.getIdAoo(), con);

		LOGGER.info("aggiungiFascicoloAutomatico -> END");
		return fascicolo;
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#creaFascicolo(it.ibm.red.business.
	 *      dto.UtenteDTO, java.lang.String, java.lang.String, java.util.Map,
	 *      it.ibm.red.business.dto.ListSecurityDTO, java.util.List,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public FascicoloRedFnDTO creaFascicolo(final UtenteDTO utente, final String indiceClassificazione, final String descrizioneFascicolo,
			final Map<String, Object> metadatiFascicolo, final ListSecurityDTO securityFascicolo, final List<FascicoloRedFnDTO> fascicoliRedFn,
			final IFilenetCEHelper fceh, final Connection con) {
		LOGGER.info("creaFascicolo -> START");
		FascicoloRedFnDTO fascicolo = null;

		// Inserimento del fascicolo automatico sul DB
		final int idFascicolo = fascicoloDAO.insert(indiceClassificazione, utente.getIdAoo(), con);
		LOGGER.info("creaFascicolo -> È stato inserito un fascicolo su DB identificato dal numero: " + idFascicolo);

		// Costruzione dell'oggetto Fascicolo per FileNet
		if (metadatiFascicolo == null) {
			fascicolo = costruisciFascicoloRedFn(String.valueOf(idFascicolo), Calendar.getInstance().get(Calendar.YEAR), String.valueOf(idFascicolo), descrizioneFascicolo,
					indiceClassificazione, utente.getIdAoo());
		} else {
			fascicolo = costruisciFascicoloRedFn(String.valueOf(idFascicolo), metadatiFascicolo, Calendar.getInstance().get(Calendar.YEAR), String.valueOf(idFascicolo),
					descrizioneFascicolo, indiceClassificazione, utente.getIdAoo());
		}
		fascicolo.setSecurity(securityFascicolo);

		// Si aggiunge il nuovo fascicolo alla lista dei DTO RED -> FileNet dei
		// fascicoli
		if (fascicoliRedFn != null) {
			fascicoliRedFn.add(fascicolo);
		}

		// Inserimento del fascicolo automatico su FileNet
		final Date dataInizio = new Date();
		inserisciSuFilenet(fascicolo, utente, fceh, con);
		final Date dataFine = new Date();
		LOGGER.info("creaFascicolo -> Timing per l'inserimento del fascicolo su FileNet ===> " + (dataFine.getTime() - dataInizio.getTime()));

		LOGGER.info("creaFascicolo -> END");
		return fascicolo;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#
	 *      associaATitolarioAggiornaNPS(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.lang.String).
	 */
	@Override
	public void associaATitolarioAggiornaNPS(final UtenteDTO utente, final String idFascicolo, final String indiceClassificazione,
			final String indiceClassificazioneDescrizione) {
		Connection con = null;
		IFilenetCEHelper fceh = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			associaATitolarioAggiornaNPS(idFascicolo, indiceClassificazione, fceh, con, utente, indiceClassificazioneDescrizione);

		} catch (final Exception e) {
			LOGGER.error("associaATitolario -> Errore durante l'associazione del fascicolo: " + idFascicolo + " al titolario: " + indiceClassificazione, e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
	}

	/**
	 * @param idFascicolo
	 * @param indiceClassificazione
	 * @param fceh
	 * @param con
	 * @param utente
	 * @param indiceClassificazioneDescrizione
	 */
	private void associaATitolarioAggiornaNPS(final String idFascicolo, final String indiceClassificazione, final IFilenetCEHelper fceh, final Connection con,
			final UtenteDTO utente, final String indiceClassificazioneDescrizione) {
		LOGGER.info("associaATitolario -> START");
		String titolarioDestinazione = Constants.EMPTY_STRING;
		if (StringUtils.isNotBlank(indiceClassificazione)) {
			titolarioDestinazione = DocumentoUtils.getTitolario(utente.getIdAoo(), indiceClassificazione);
		}

		// Si associa il fascicolo al titolario su FileNet
		fceh.associaFascicoloATitolario(idFascicolo, titolarioDestinazione, utente.getIdAoo());

		// Si aggiorna l'associazione tra fascicolo e indice di classificazione sul DB
		fascicoloDAO.aggiornaIndiceClassificazione(Integer.parseInt(idFascicolo), indiceClassificazione, utente.getIdAoo(), con);

		if (TipologiaProtocollazioneEnum.isProtocolloNPS(utente.getIdTipoProtocollo())) {
			aggiornaIndiceClassFascicoloNps(null, idFascicolo, fceh, con, utente, indiceClassificazioneDescrizione);
		}

		LOGGER.info("associaATitolario -> Titolario del fascicolo: " + idFascicolo + " aggiornato. Nuovo titolario: " + indiceClassificazione);
		LOGGER.info("associaATitolario -> END");
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#aggiornaIndiceClassFascicoloNps(
	 *      java.lang.String, java.lang.String,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 *      java.sql.Connection, it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	public void aggiornaIndiceClassFascicoloNps(final String infoProtocollo, final String idFascicolo, final IFilenetCEHelper fceh, final Connection con,
			final UtenteDTO utente, final String indiceClassificazioneDescrizione) {
		final Collection<DocumentoFascicoloDTO> listDocFascicolo = fceh.getOnlyDocumentiRedFascicolo(Integer.parseInt(idFascicolo), utente.getIdAoo());
		final Iterator<DocumentoFascicoloDTO> it = listDocFascicolo.iterator();

		while (it.hasNext()) {
			final DocumentoFascicoloDTO doc = it.next();
			if (StringUtils.isNotBlank(doc.getIdProtocollo())) {

				final FascicoloDTO fascicoloProcedimentale = getFascicoloProcedimentale(doc.getDocumentTitle(), utente.getIdAoo().intValue(), utente);

				if (idFascicolo.equals(fascicoloProcedimentale.getIdFascicolo())) {
					final boolean esitoOk = npsSRV.aggiornaTitolarioProtocollo(utente, infoProtocollo, doc.getIdProtocollo(), indiceClassificazioneDescrizione,
							doc.getDocumentTitle(), con);

					if (!esitoOk) {
						throw new RedException("aggiornaDocumento -> Si è verificato un errore durante l'aggiornamento dei dati di protocollo Oggetto e/o "
								+ "Tipologia Documento e/o indice di classificazione per il documento: " + doc.getDocumentTitle());
					}
				}
			}
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#associaATitolario(it.
	 *      ibm.red.business.dto.UtenteDTO, java.lang.String, java.lang.String).
	 */
	@Override
	public void associaATitolario(final UtenteDTO utente, final String idFascicolo, final String indiceClassificazione) {
		Connection con = null;
		IFilenetCEHelper fceh = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			associaATitolario(idFascicolo, indiceClassificazione, utente.getIdAoo(), fceh, con);
		} catch (final Exception e) {
			LOGGER.error("associaATitolario -> Errore durante l'associazione del fascicolo: " + idFascicolo + " al titolario: " + indiceClassificazione, e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#associaATitolario(java
	 *      .lang.String, java.lang.String, java.lang.Long,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public void associaATitolario(final String idFascicolo, final String indiceClassificazione, final Long idAoo, final IFilenetCEHelper fceh, final Connection con) {
		LOGGER.info("associaATitolario -> START");
		String titolarioDestinazione = Constants.EMPTY_STRING;
		if (StringUtils.isNotBlank(indiceClassificazione)) {
			titolarioDestinazione = DocumentoUtils.getTitolario(idAoo, indiceClassificazione);
		}

		// Si associa il fascicolo al titolario su FileNet
		fceh.associaFascicoloATitolario(idFascicolo, titolarioDestinazione, idAoo);

		// Si aggiorna l'associazione tra fascicolo e indice di classificazione sul DB
		fascicoloDAO.aggiornaIndiceClassificazione(Integer.parseInt(idFascicolo), indiceClassificazione, idAoo, con);

		LOGGER.info("associaATitolario -> Titolario del fascicolo: " + idFascicolo + " aggiornato. Nuovo titolario: " + indiceClassificazione);
		LOGGER.info("associaATitolario -> END");
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#aggiornaStato(java.lang.String,
	 *      it.ibm.red.business.enums.FilenetStatoFascicoloEnum, java.lang.Long,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetHelper).
	 */
	@Override
	public void aggiornaStato(final String idFascicolo, final FilenetStatoFascicoloEnum statoFascicolo, final Long idAoo, final IFilenetCEHelper fceh) {
		LOGGER.info("aggiornaStato -> START");
		final HashMap<String, Object> metadatiFascicolo = new HashMap<>();
		metadatiFascicolo.put(pp.getParameterByKey(PropertiesNameEnum.STATO_FASCICOLO_METAKEY), statoFascicolo.getNome());
		// Se si sta chiudendo il fascicolo, si aggiorna il metadato Data Terminazione
		if (FilenetStatoFascicoloEnum.CHIUSO.equals(statoFascicolo)) {
			metadatiFascicolo.put(pp.getParameterByKey(PropertiesNameEnum.DATA_TERMINAZIONE_METAKEY), new Date());
		}

		fceh.updateFascicoloMetadati(idFascicolo, idAoo, pp.getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY), metadatiFascicolo);
		LOGGER.info("aggiornaStato -> END");
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#aggiornaMetadati(java.lang.String,
	 *      it.ibm.red.business.enums.FilenetStatoFascicoloEnum, java.lang.String,
	 *      java.lang.Long, it.ibm.red.business.helper.filenet.ce.IFilenetHelper).
	 */
	@Override
	public void aggiornaMetadati(final String idFascicolo, final FilenetStatoFascicoloEnum statoFascicolo, final String oggetto, final Long idAoo,
			final IFilenetCEHelper fceh) {
		LOGGER.info("aggiornaStato -> START");
		final HashMap<String, Object> metadatiFascicolo = new HashMap<>();

		if (statoFascicolo != null) {
			metadatiFascicolo.put(pp.getParameterByKey(PropertiesNameEnum.STATO_FASCICOLO_METAKEY), statoFascicolo.getNome());
			// Se si sta chiudendo il fascicolo, si aggiorna il metadato Data Terminazione
			if (FilenetStatoFascicoloEnum.CHIUSO.equals(statoFascicolo)) {
				metadatiFascicolo.put(pp.getParameterByKey(PropertiesNameEnum.DATA_TERMINAZIONE_METAKEY), new Date());
			}
		}

		if (StringUtils.isNotEmpty(oggetto)) {
			final Document docFascicolo = fceh.getFascicolo(idFascicolo, idAoo.intValue());
			final String oggettoOld = (String) TrasformerCE.getMetadato(docFascicolo, PropertiesNameEnum.OGGETTO_METAKEY);
			final String[] arrOggettoOld = oggettoOld.split("_");
			if (arrOggettoOld.length != 3) {
				throw new RedException("L'attuale oggetto del fascicolo risulta mal formato");
			}

			final String oggettoNew = arrOggettoOld[0] + "_" + arrOggettoOld[1] + "_" + oggetto;
			metadatiFascicolo.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), oggettoNew);
		}

		if (metadatiFascicolo.isEmpty()) {
			throw new RedException("Non è presente alcun metadato da aggiornare.");
		}

		fceh.updateFascicoloMetadati(idFascicolo, idAoo, pp.getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY), metadatiFascicolo);
		LOGGER.info("aggiornaStato -> END");
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#apriFascicolo(java.
	 *      lang.String, java.lang.Long, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public void apriFascicolo(final String idFascicolo, final Long idAoo, final UtenteDTO utente) {
		setStatoFascicolo(idFascicolo, idAoo, utente, FilenetStatoFascicoloEnum.APERTO);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#chiudiFascicolo(java.lang.String,
	 *      java.lang.Long, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public void chiudiFascicolo(final String idFascicolo, final Long idAoo, final UtenteDTO utente) {
		setStatoFascicolo(idFascicolo, idAoo, utente, FilenetStatoFascicoloEnum.CHIUSO);
	}

	private void setStatoFascicolo(final String idFascicolo, final Long idAoo, final UtenteDTO utente, final FilenetStatoFascicoloEnum stato) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			aggiornaStato(idFascicolo, stato, idAoo, fceh);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#aggiornaFascicoloProcedimentale(it.
	 *      ibm.red.business.dto.SalvaDocumentoRedDTO,
	 *      it.ibm.red.business.dto.FascicoloDTO,
	 *      it.ibm.red.business.dto.FascicoloDTO, it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.Long, it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 *      it.ibm.red.business.helper.filenet.pe.FilenetPEHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public void aggiornaFascicoloProcedimentale(final SalvaDocumentoRedDTO docInput, final FascicoloDTO newFascicoloProcedimentale,
			final FascicoloDTO currentFascicoloProcedimentale, final UtenteDTO utente, final Long idAoo, final IFilenetCEHelper fceh, final FilenetPEHelper fpeh,
			final Connection con) {
		final String idDocumento = docInput.getDocumentTitle();
		LOGGER.info("aggiornaFascicoloProcedimentale -> START. Documento: " + idDocumento);
		final String currentFascicoloProcedimentaleId = currentFascicoloProcedimentale.getIdFascicolo();
		final String newFascicoloProcedimentaleId = newFascicoloProcedimentale.getIdFascicolo();
		FascicoloDTO fascicoloCorrente = currentFascicoloProcedimentale;

		// Si confronta l'ID del nuovo fascicolo con quello del precedente
		if (!newFascicoloProcedimentaleId.equals(currentFascicoloProcedimentaleId)) {
			// Se il fascicolo è completamente cambiato, si effettua lo spostamento del
			// documento
			LOGGER.info("aggiornaFascicoloProcedimentale -> Il fascicolo procedimentale per il documento: " + idDocumento + " è stato cambiato da: "
					+ currentFascicoloProcedimentaleId + " a: " + newFascicoloProcedimentaleId);
			// Si controlla se è già esistente
			if (!fascicoloDAO.isDocumentoAssociato(Integer.parseInt(idDocumento), Integer.parseInt(newFascicoloProcedimentaleId), idAoo, con)) {
				// In caso negativo, si esegue lo spostamento del documento
				// N.B. Se il fascicolo precedente diventa vuoto, il metodo provvede anche alla
				// sua eliminazione
				String titolario = Constants.EMPTY_STRING;
				if (StringUtils.isNotBlank(docInput.getIndiceClassificazioneFascicoloProcedimentale())) {
					titolario = docInput.getIndiceClassificazioneFascicoloProcedimentale() + " - " + docInput.getDescrizioneIndiceClassificazioneFascicoloProcedimentale();
				}
				final boolean spostaDocumento = spostaDocumento(utente, idDocumento, newFascicoloProcedimentaleId, currentFascicoloProcedimentaleId, idAoo,
						docInput.getIdProtocollo(), titolario, fceh, fpeh, con);

				if (spostaDocumento) {
					fascicoloCorrente = newFascicoloProcedimentale;
				} else {
					throw new RedException("aggiornaFascicoloProcedimentale -> Si è verificato un errore durante lo spostamento del documento: " + idDocumento
							+ " nel nuovo fascicolo procedimentale: " + newFascicoloProcedimentaleId);
				}
			}
		}
		// L'ID è lo stesso (currentFascicoloProcedimentale ==
		// newFascicoloProcedimentale).
		// Si controlla se sono cambiati la descrizione o l'indice di classificazione
		// del fascicolo principale, in caso positivo li si aggiorna

		// Gestione della modifica del nome del fascicolo
		final String inputDescrizioneFascicolo = docInput.getDescrizioneFascicoloProcedimentale();
		if (!fascicoloCorrente.getDescrizione().equals(inputDescrizioneFascicolo)) {
			final Calendar calFascicolo = Calendar.getInstance();
			calFascicolo.setTime(fascicoloCorrente.getDataCreazione());
			final String oggettoFascicolo = fascicoloCorrente.getIdFascicolo() + "_" + calFascicolo.get(Calendar.YEAR) + "_"
					+ docInput.getDescrizioneFascicoloProcedimentale();

			final HashMap<String, Object> metadatiFascicolo = new HashMap<>(1);
			metadatiFascicolo.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), oggettoFascicolo);

			fceh.updateFascicoloMetadati(fascicoloCorrente.getIdFascicolo(), idAoo, pp.getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY),
					metadatiFascicolo);

			newFascicoloProcedimentale.setDescrizione(inputDescrizioneFascicolo);
		}

		// Gestione della modifica dell'indice di classificazione
		final String inputIndiceClassificazione = docInput.getIndiceClassificazioneFascicoloProcedimentale();
		if (inputIndiceClassificazione != null
				&& ((fascicoloCorrente.getIndiceClassificazione() != null && !fascicoloCorrente.getIndiceClassificazione().equals(inputIndiceClassificazione))
						|| fascicoloCorrente.getIndiceClassificazione() == null)) {
			// Aggiornamento dell'associazione tra fascicolo e titolario/indice di
			// classificazione rispettivamente su FileNet e sul DB
			associaATitolario(fascicoloCorrente.getIdFascicolo(), inputIndiceClassificazione, idAoo, fceh, con);

			newFascicoloProcedimentale.setIndiceClassificazione(inputIndiceClassificazione);
		}

		LOGGER.info("aggiornaFascicoloProcedimentale -> END. Documento: " + idDocumento);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#
	 *      documentoGiaPresenteInFascicolo(java.lang.String, java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public boolean documentoGiaPresenteInFascicolo(final String idDocumento, final String idFascicolo, final UtenteDTO utente) {
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			final int idDoc = Integer.parseInt(idDocumento);
			final int idFasc = Integer.parseInt(idFascicolo);

			return fascicoloDAO.isDocumentoAssociato(idDoc, idFasc, utente.getIdAoo(), con);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#spostaDocumento(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.Long,
	 *      java.lang.String, java.lang.String,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      it.ibm.red.business.helper.filenet.pe.FilenetPEHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean spostaDocumento(final UtenteDTO utente, final String idDocumento, final String newFascicoloId, final String oldFascicoloId, final Long idAoo,
			final String idProtocollo, final String indiceClassificazioneFascicoloProcedimentale, final IFilenetCEHelper fceh, final FilenetPEHelper fpeh,
			final Connection con) {
		LOGGER.info("spostaDocumento -> START. Documento: " + idDocumento + ". Nuovo fascicolo: " + newFascicoloId);

		final DocumentoFascicolazioneRedFnDTO docFascicolazione = new DocumentoFascicolazioneRedFnDTO();
		docFascicolazione.setDocumentTitle(idDocumento);
		docFascicolazione.setTipoOperazione(FilenetTipoOperazioneEnum.UNFILE);

		// Si rimuove il documento dal fascicolo corrente su FileNet
		boolean esito = gestisciFascicolazioneDocumento(oldFascicoloId, docFascicolazione, idAoo, fceh);

		// Se l'operazione è avvenuta correttamente...
		if (esito) {
			// Si inserisce il documento nel nuovo fascicolo su FileNet
			docFascicolazione.setTipoOperazione(FilenetTipoOperazioneEnum.LINK);
			esito = gestisciFascicolazioneDocumento(newFascicoloId, docFascicolazione, idAoo, fceh);

			// Se l'esito dell'operazione è positivo...
			if (esito) {
				// ...si aggiorna l'associazione sul DB...
				final int updateResult = fascicoloDAO.updateDocumentoFascicolo(Integer.parseInt(idDocumento), Integer.parseInt(newFascicoloId),
						Integer.parseInt(oldFascicoloId), TipoFascicolo.AUTOMATICO, idAoo, con);
				if (BooleanFlagEnum.NO.getIntValue().equals(updateResult)) {
					esito = false;
				} else {
					// ...si traccia lo spostamento nello storico del documento
					eventLogSRV.writeSpostaCopiaEventLog(utente, Integer.parseInt(idDocumento), EventTypeEnum.SPOSTA_IN_ALTRO_FASCICOLO, Integer.parseInt(oldFascicoloId),
							Integer.parseInt(newFascicoloId));

					// ...e, se il documento spostato era l'ultimo documento rimasto nel fascicolo
					// corrente, si elimina quest'ultimo
					fceh.eliminaFascicoloSeVuoto(oldFascicoloId, idAoo);

					// ...si procede con l'aggiornamento dei workflow del documento
					final Map<String, Object> metadatiWorkflow = new HashMap<>(1);
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY), newFascicoloId);

					fpeh.updateWorkflow(Integer.parseInt(idDocumento), metadatiWorkflow);

					// ...infine, si aggiornano i dati di protocollo su NPS
					if (StringUtils.isNotBlank(idProtocollo)) {

						final String[] parametriDocumento = new String[] { pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY),
								pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY), PropertyNames.ID };
						final Document doc = fceh.getDocumentByIdGestionale(idDocumento, Arrays.asList(parametriDocumento), null, utente.getIdAoo().intValue(), null,
								pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));
						final Integer numeroProtocollo = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
						final Integer annoProtocollo = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);

						final String infoProtocollo = new StringBuilder().append(numeroProtocollo).append("/").append(annoProtocollo).append("_").append(utente.getCodiceAoo())
								.toString();

						npsSRV.aggiornaTitolarioProtocollo(utente, infoProtocollo, idProtocollo, indiceClassificazioneFascicoloProcedimentale, idDocumento, con);

					}
					rifStoricoDAO.updateIdFascicoloProcedimentaleSposta(newFascicoloId, idDocumento, idAoo, con);

				}
				// ...altrimenti, si ripristina l'associazione con il fascicolo corrente
			} else {
				esito = gestisciFascicolazioneDocumento(oldFascicoloId, docFascicolazione, idAoo, fceh);
			}
		}

		if (!esito) {
			LOGGER.error("Si è verificato un errore durante lo spostamento del documento: " + idDocumento + " dal fascicolo: " + oldFascicoloId + " al fascicolo: "
					+ newFascicoloId);
		}

		LOGGER.info("spostaDocumento -> END. Documento: " + idDocumento + ". Esito: " + esito);
		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#copiaDocumento(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.Long,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean copiaDocumento(final UtenteDTO utente, final String idDocumento, final String newFascicoloId, final String oldFascicoloId, final Long idAoo,
			final IFilenetCEHelper fceh, final Connection con) {
		LOGGER.info("copiaDocumento -> START. Inizio copia del documento: " + idDocumento + NEL_FASCICOLO + newFascicoloId);

		final DocumentoFascicolazioneRedFnDTO docFascicolazione = new DocumentoFascicolazioneRedFnDTO();
		docFascicolazione.setDocumentTitle(idDocumento);
		docFascicolazione.setTipoOperazione(FilenetTipoOperazioneEnum.COPY);

		// Copia il documento nel nuovo fascicolo su FileNet, mantenendolo in quello
		// corrente
		boolean esito = gestisciFascicolazioneDocumento(newFascicoloId, docFascicolazione, idAoo, fceh);

		// Inserimento dell'associazione sul DB
		if (esito) {
			final int insertResult = fascicoloDAO.insertDocumentoFascicolo(Integer.parseInt(idDocumento), Integer.parseInt(newFascicoloId), TipoFascicolo.NON_AUTOMATICO,
					idAoo, con);
			if (BooleanFlagEnum.NO.getIntValue().equals(insertResult)) {
				esito = false;
			}

			// Traccia lo spostamento nello storico del documento
			eventLogSRV.writeSpostaCopiaEventLog(utente, Integer.parseInt(idDocumento), EventTypeEnum.COPIA_IN_ALTRO_FASCICOLO, Integer.parseInt(oldFascicoloId),
					Integer.parseInt(newFascicoloId));
		}

		if (!esito) {
			LOGGER.error("Si è verificato un errore durante la copia del documento: " + idDocumento + NEL_FASCICOLO + newFascicoloId);
		}

		LOGGER.info("copiaDocumento -> END. Fine copia del documento: " + idDocumento + NEL_FASCICOLO + newFascicoloId);
		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#getDocumentiFascicolo(java.lang.Integer,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<DocumentoFascicoloDTO> getDocumentiFascicolo(final Integer idfascicolo, final UtenteDTO utente) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			return TrasformCE.transform(fceh.getDocumentiFascicolo(idfascicolo, utente.getIdAoo()), TrasformerCEEnum.FROM_DOCUMENTO_TO_DOCUMENTO_FASCICOLO);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#getOnlyDocumentiRedFascicolo(java.lang.Integer,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<DocumentoFascicoloDTO> getOnlyDocumentiRedFascicolo(final Integer idfascicolo, final UtenteDTO utente) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			return fceh.getOnlyDocumentiRedFascicolo(idfascicolo, utente.getIdAoo());
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#getFascicoliNonClassificati(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<MasterFascicoloDTO> getFascicoliNonClassificati(final UtenteDTO utente) {
		final Collection<MasterFascicoloDTO> output = new ArrayList<>();
		IFilenetCEHelper fcehAdmin = null;

		try {
			fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());

			final Boolean aclCorriere = true;
			final Boolean aclNodo = true;
			final Boolean aclUtente = false;
			final Long idUfficioCorriereUtente = utente.getIdNodoCorriere();
			final DocumentSet ds = fcehAdmin.getFascicoliNonClassificati(utente, idUfficioCorriereUtente.intValue(), utente.getGruppiAD(), aclCorriere, aclNodo, aclUtente);

			final Iterator<?> it = ds.iterator();
			while (it.hasNext()) {
				final Document document = (Document) it.next();
				final String id = document.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY));
				final String oggetto = document.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY));
				final Date data = document.getProperties().getDateTimeValue(pp.getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY));
				final Long idAOO = Long.valueOf(((Integer) document.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY))));

				final MasterFascicoloDTO m = new MasterFascicoloDTO(Integer.valueOf(id), oggetto, data, idAOO);
				output.add(m);
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero dei fascicoli non classificati", e);
			throw new RedException("Errore durante il recupero dei fascicoli non classificati", e);
		} finally {
			popSubject(fcehAdmin);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#getCountFascicoliNonClassificati(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Integer getCountFascicoliNonClassificati(final UtenteDTO utente) {
		IFilenetCEHelper fcehAdmin = null;
		Integer output = null;
		try {
			fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());
			final Boolean aclCorriere = true;
			final Boolean aclNodo = true;
			final Boolean aclUtente = false;
			output = fcehAdmin.getCountFascicoliNonClassificati(utente, utente.getIdNodoCorriere().intValue(), utente.getGruppiAD(), aclCorriere, aclNodo, aclUtente);

		} catch (final Exception e) {
			LOGGER.error("Errore durante la count dei fascicoli non classificati", e);
			throw new RedException("Errore durante la count dei fascicoli non classificati", e);
		} finally {
			popSubject(fcehAdmin);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#getFascicoliByIndiceDiClassificazione(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	public Collection<MasterFascicoloDTO> getFascicoliByIndiceDiClassificazione(final UtenteDTO utente, final String indiceDiClassificazione) {
		final Collection<MasterFascicoloDTO> output = new ArrayList<>();
		IFilenetCEHelper fcehAdmin = null;

		try {
			fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());

			final Boolean aclCorriere = true;
			final Boolean aclNodo = true;
			final Boolean aclUtente = false;
			final Long idUfficioCorriereUtente = utente.getIdNodoCorriere();
			final DocumentSet ds = fcehAdmin.getFascicoliByIndice(utente, idUfficioCorriereUtente.intValue(), utente.getGruppiAD(), aclCorriere, aclNodo, aclUtente,
					indiceDiClassificazione);

			final Iterator<?> it = ds.iterator();
			while (it.hasNext()) {
				final Document document = (Document) it.next();
				final String id = document.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY));
				final String oggetto = document.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY));
				final Date data = document.getProperties().getDateTimeValue(pp.getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY));
				final Long idAOO = Long.valueOf(((Integer) document.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY))));
				final String statoFascicolo = document.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.STATO_FASCICOLO_METAKEY));
				final MasterFascicoloDTO m = new MasterFascicoloDTO(Integer.valueOf(id), oggetto, data, idAOO, statoFascicolo);

				output.add(m);
			}
		} finally {
			popSubject(fcehAdmin);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#getDocumentiByIdFascicoloPadre(java.lang.Integer,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<DetailDocumentRedDTO> getDocumentiByIdFascicoloPadre(final Integer idfascicolo, final UtenteDTO utente) {
		final List<DetailDocumentRedDTO> list = new ArrayList<>();
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final DocumentSet docs = fceh.getDocumentiFascicolo(idfascicolo, utente.getIdAoo());
			if (docs == null || docs.isEmpty()) {
				return list;
			}
			Document document = null;
			Document documentLight = null;
			final String fascicoloClassName = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY);
			final String contributoClassName = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.CONTRIBUTO_CLASSNAME_FN_METAKEY);
			final String allegatoClassName = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY);

			final Iterator<?> it = docs.iterator();
			while (it.hasNext()) {
				document = (Document) it.next();
				if (fascicoloClassName.equalsIgnoreCase(document.getClassName()) || contributoClassName.equalsIgnoreCase(document.getClassName())
						|| allegatoClassName.equalsIgnoreCase(document.getClassName())) {
					continue;
				}
				final String documentTitle = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
				final String documentClass = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY);
				documentLight = fceh.getDocumentRedForDetailLIGHT(documentClass, documentTitle);
				if (documentLight != null) {
					list.add(TrasformCE.transform(documentLight, TrasformerCEEnum.FROM_DOCUMENTO_TO_LIGHT_DETAIL_RED));
				}
			}
		} finally {
			popSubject(fceh);
		}

		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.IFascicoloSRV#getFascicoloProcedimentale
	 * (java.lang.String, java.lang.Integer, java.util.List, java.sql.Connection)
	 */
	@Override
	public final FascicoloDTO getFascicoloProcedimentale(final String documentTitle, final Integer idAoo, final List<FascicoloDTO> fascicoli, final Connection con) {
		FascicoloDTO procedimentale = null;

		try {
			if (fascicoli == null || fascicoli.isEmpty()) {
				return procedimentale;
			}
			final Long idAooL = Long.valueOf(idAoo);
			final Integer idFascicoloProcedimentale = fascicoloDAO.getIdFascicoloProcedimentale(documentTitle, idAooL, con);

			if (idFascicoloProcedimentale != null && idFascicoloProcedimentale.intValue() > 0) {
				for (final FascicoloDTO f : fascicoli) {
					if (f.getIdFascicolo().equals(idFascicoloProcedimentale.toString())) {
						procedimentale = f;
						break;
					}
				}
			}
			if (procedimentale == null) {
				procedimentale = getFascicoloConIndiceMinimo(fascicoli);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}

		return procedimentale;
	}

	private FascicoloDTO getFascicoloConIndiceMinimo(final List<FascicoloDTO> inputFascicoli) {
		FascicoloDTO fascicolo = null;

		if (!CollectionUtils.isEmpty(inputFascicoli)) {
			fascicolo = inputFascicoli.get(0);

			if (inputFascicoli.size() > 1) {
				final int idFascicoloMinimo = Integer.parseInt(fascicolo.getIdFascicolo());
				for (int index = 1; index < inputFascicoli.size(); index++) {
					if (Integer.parseInt(inputFascicoli.get(index).getIdFascicolo()) < idFascicoloMinimo) {
						fascicolo = inputFascicoli.get(index);
					}
				}
			}
		}

		return fascicolo;
	}

	@Override
	public final FascicoloDTO getFascicoloProcedimentale(final String documentTitle, final Integer idAoo, final UtenteDTO utente, final IFilenetCEHelper fceh) {
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			return getFascicoloProcedimentale(documentTitle, idAoo, fceh, con);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.ibm.red.business.service.IFascicoloSRV#getFascicoloProcedimentale(java.
	 * lang.String, java.lang.Integer, it.ibm.red.business.dto.UtenteDTO,
	 * java.sql.Connection)
	 */
	@Override
	public final FascicoloDTO getFascicoloProcedimentale(final String documentTitle, final Integer idAoo, final UtenteDTO utente, final Connection con) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			return getFascicoloProcedimentale(documentTitle, idAoo, fceh, con);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#
	 * getFascicoloProcedimentale(java.lang.String, java.lang.Integer,
	 * it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public final FascicoloDTO getFascicoloProcedimentale(final String documentTitle, final Integer idAoo, final UtenteDTO utente) {
		Connection con = null;
		IFilenetCEHelper fceh = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			return getFascicoloProcedimentale(documentTitle, idAoo, fceh, con);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#getFascicoloProcedimentale
	 *      (java.lang.String, java.lang.Integer,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public FascicoloDTO getFascicoloProcedimentale(final String documentTitle, final Integer idAoo, final IFilenetCEHelper fceh, final Connection con) {
		try {
			final List<FascicoloDTO> fascicoliDTO = fceh.getFascicoliDocumento(documentTitle, idAoo);

			return getFascicoloProcedimentale(documentTitle, idAoo, fascicoliDTO, con);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#getFascicoloProcedimentale
	 *      (com.filenet.api.core.Document, java.lang.Integer,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public final FascicoloDTO getFascicoloProcedimentale(final Document docFilenet, final Integer idAoo, final IFilenetCEHelper fceh, final Connection con) {
		// Si recuperano i fascicoli del documento
		final List<FascicoloDTO> fascicoli = fceh.getFascicoliDocumento(docFilenet, idAoo);

		final String documentTitle = (String) TrasformerCE.getMetadato(docFilenet, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);

		return getFascicoloProcedimentale(documentTitle, idAoo, fascicoli, con);
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#getFascicoloProcedimentaleDSR(java.
	 *      util.List).
	 */
	@Override
	public FascicoloDTO getFascicoloProcedimentaleDSR(final List<FascicoloDTO> fascicoliList) {
		if (CollectionUtils.isEmpty(fascicoliList)) {
			LOGGER.error("I fascicoli di una DSR non possono essere nulli");
		}

		final List<FascicoloDTO> fascicoliCatalogati = fascicoliList.stream()
				.filter(f -> !FilenetStatoFascicoloEnum.NON_CATALOGATO.getNome().equalsIgnoreCase(f.getIndiceClassificazione())).collect(Collectors.toList());

		if (fascicoliCatalogati.isEmpty()) {
			LOGGER.error("Non è stato possibile trovare fascicoli non catalogati per una dsr");
		}

		if (fascicoliCatalogati.size() > 1) {
			final List<String> idFascicoliList = fascicoliCatalogati.stream().map(FascicoloDTO::getIdFascicolo).collect(Collectors.toList());
			LOGGER.error("Sono stati individuati molteplici fascicoli catalogati per una Dsr, di seguito gli idFascicoli: " + idFascicoliList.toString());
		}

		FascicoloDTO output = null;
		if (!CollectionUtils.isEmpty(fascicoliCatalogati)) {
			output = fascicoliCatalogati.get(0);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#disassociaDocumentoFascicolo
	 *      (java.lang.String, java.lang.String, java.lang.Long,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetHelper).
	 */
	@Override
	public void disassociaDocumentoFascicolo(final String idFascicolo, final String documentTitle, final Long idAoo, final IFilenetCEHelper fceh) {
		final DocumentoFascicolazioneRedFnDTO docFascicolazione = new DocumentoFascicolazioneRedFnDTO();
		docFascicolazione.setDocumentTitle(documentTitle);
		docFascicolazione.setTipoOperazione(FilenetTipoOperazioneEnum.UNFILE);

		// Si rimuove il documento dal fascicolo corrente su FileNet
		gestisciFascicolazioneDocumento(idFascicolo, docFascicolazione, idAoo, fceh);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#spostaDocumento
	 *      (java.lang.String, java.lang.String, java.lang.String, java.lang.Long,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String, java.lang.String,
	 *      java.lang.String, java.lang.String).
	 */
	@Override
	public boolean spostaDocumento(final String idDocumento, final String newFascicoloId, final String currentFascicoloId, final Long idAoo, final UtenteDTO utente,
			final String idProtocollo, final String indiceClassificazioneFascicoloProcedimentale) {
		Connection con = null;
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			return spostaDocumento(utente, idDocumento, newFascicoloId, currentFascicoloId, idAoo, idProtocollo, indiceClassificazioneFascicoloProcedimentale, fceh, fpeh,
					con);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
			logoff(fpeh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#copiaDocumento
	 *      (java.lang.String, java.lang.String, java.lang.String, java.lang.Long,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public boolean copiaDocumento(final String idDocumento, final String newFascicoloId, final String currentFascicoloId, final Long idAoo, final UtenteDTO utente) {
		Connection con = null;
		IFilenetCEHelper fceh = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			return copiaDocumento(utente, idDocumento, newFascicoloId, currentFascicoloId, idAoo, fceh, con);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#
	 *      getFascicoloByNomeFascicolo(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public FascicoloDTO getFascicoloByNomeFascicolo(final String nomeFascicolo, final UtenteDTO utente) {
		FascicoloDTO f = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			f = getFascicoloByNomeFascicolo(nomeFascicolo, utente, fceh);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}

		return f;
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#getFascicoloByNomeFascicolo
	 *      (java.lang.String, it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetHelper).
	 */
	@Override
	public FascicoloDTO getFascicoloByNomeFascicolo(final String nomeFascicolo, final UtenteDTO utente, final IFilenetCEHelper fceh) {
		FascicoloDTO f;
		final Document docFascicolo = fceh.getFascicolo(nomeFascicolo, utente.getIdAoo().intValue());
		f = TrasformCE.transform(docFascicolo, TrasformerCEEnum.FROM_DOCUMENTO_TO_FASCICOLO);
		return f;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#
	 *      getFascicoliByFaldoneAbsolutePath(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<FascicoloDTO> getFascicoliByFaldoneAbsolutePath(final String falodneAbsolutePath, final UtenteDTO utente) {
		/**
		 * IMPORTANTE: la connessione viene fatta con l'utente passatogli e non con
		 * l'utente admin (a differenza di FaldoneSRV.getFascicoliFaldoneDS(...))
		 */
		List<FascicoloDTO> list = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final RepositoryRowSet rrs = fceh.getFascicoliByFaldone(falodneAbsolutePath, utente.getGruppiAD());

			final Iterator<?> it = rrs.iterator();
			RepositoryRow childLink = null;
			Document docFascicolo = null;
			String nomeFascicolo = null;

			list = new ArrayList<>();
			while (it.hasNext()) {
				childLink = (RepositoryRow) it.next();
				nomeFascicolo = childLink.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY));
				docFascicolo = fceh.getFascicolo(nomeFascicolo, utente.getIdAoo().intValue());
				if (docFascicolo != null) {
					list.add(TrasformCE.transform(docFascicolo, TrasformerCEEnum.FROM_DOCUMENTO_TO_FASCICOLO));
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}

		return list;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#getZipFascicoloContents(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public InputStream getZipFascicoloContents(final String idFascicolo, final UtenteDTO utente) {
		IFilenetCEHelper fceh = null;
		ZipOutputStream zos = null;
		ByteArrayOutputStream bos = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final FascicoloDTO fascicolo = getFascicoloByNomeFascicolo(idFascicolo, utente, fceh);

			final Collection<DocumentoAllegabileDTO> listaDocFascicolo = fceh.getOnlyDocumentiRedFascicoloAllegabili(fascicolo, utente.getIdAoo(), true, true);

			if (!listaDocFascicolo.isEmpty()) {
				bos = new ByteArrayOutputStream();
				zos = new ZipOutputStream(bos);
			}

			boolean allegatiOriginali = false;
			String documentTitlePrincipale = null;
			final Map<String, Integer> countFileNameInFolder = new HashMap<>();
			Integer count = 0;

			for (final DocumentoAllegabileDTO docPrincipale : listaDocFascicolo) {

				LOGGER.info("Documento " + docPrincipale.getDocumentTitle());

				String folder = "ID documento " + docPrincipale.getDocumentTitle();
				if (docPrincipale.getNumeroProtocollo() != null && docPrincipale.getNumeroProtocollo() != 0) {
					folder = "Protocollo numero " + docPrincipale.getNumeroProtocollo() + " del " + docPrincipale.getAnnoProtocollo();
				} else if (docPrincipale.getNumeroDocumento() != null && docPrincipale.getNumeroDocumento() != 0) {
					folder = "Documento numero " + docPrincipale.getNumeroDocumento() + " del " + docPrincipale.getAnnoDocumento();
				}

				if (docPrincipale.getContenuto() != null) {
					createZipEntry(zos, docPrincipale.getContenuto().getInputStream(), folder + "/" + docPrincipale.getNomeFile());
				}

				if (docPrincipale.getDocumentTitle().equals(documentTitlePrincipale)) {
					continue;
				}

				documentTitlePrincipale = docPrincipale.getDocumentTitle();

				allegatiOriginali = docPrincipale.getIdCategoria().equals(CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0])
						|| docPrincipale.getIdCategoria().equals(CategoriaDocumentoEnum.INTERNO.getIds()[0])
						|| docPrincipale.getIdCategoria().equals(CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA.getIds()[0]);

				countFileNameInFolder.clear();

				final Collection<DocumentoAllegabileDTO> allegatiDocFascicolo = fceh.getAllegatiAllegabili(docPrincipale.getFascicolo(), docPrincipale.getDocumentTitle(),
						false, utente.getIdAoo(), allegatiOriginali, true);
				for (final DocumentoAllegabileDTO allegato : allegatiDocFascicolo) {

					LOGGER.info("Allegato " + allegato.getDocumentTitle());
					String nomeAllegato = allegato.getNomeFile();
					count = countFileNameInFolder.get(nomeAllegato);
					if (count != null) {

						// se il file esiste già nel folder, aggiungi _(count) prima dell'estensione
						final String extension = FilenameUtils.getExtension(nomeAllegato);
						String nomeAllegatoSenzaEstensione = nomeAllegato;
						if (nomeAllegato.lastIndexOf(".") != -1) {
							nomeAllegatoSenzaEstensione = nomeAllegato.substring(0, nomeAllegato.lastIndexOf("."));
						}

						nomeAllegato = nomeAllegatoSenzaEstensione + "_(" + count + ")." + extension;

					} else {
						count = 0;
					}

					if (allegato.getContenuto() != null) {
						createZipEntry(zos, allegato.getContenuto().getInputStream(), folder + "/allegati/" + nomeAllegato);
					}

					countFileNameInFolder.put(allegato.getNomeFile(), ++count);

				}
			}

			if (bos != null) {
				zos.finish();
				final byte[] zipByteArray = bos.toByteArray();
				return new ByteArrayInputStream(zipByteArray);
			}

			return null;

		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_DOCUMENTI, e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}

	}

	private static void createZipEntry(final ZipOutputStream zos, final InputStream is, final String nomeFile) throws IOException {
		final byte[] buffer = new byte[1024];
		zos.putNextEntry(new ZipEntry(nomeFile));
		int length;
		while ((length = is.read(buffer)) > 0) {
			zos.write(buffer, 0, length);
		}
		zos.closeEntry();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#
	 *      getFascicoloProcedimentale (java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public FascicoloDTO getFascicoloProcedimentale(final String wobNumber, final UtenteDTO utente) {
		FilenetPEHelper fpeh = null;

		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			final String documentTitle = fpeh.getMetadato(wobNumber, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY.getKey()).toString();

			return getFascicoloProcedimentale(documentTitle, utente.getIdAoo().intValue(), utente);

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero del fascicolo procedimanetale", e);
			throw new RedException(e);
		} finally {
			logoff(fpeh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#getDocumentiAllegabili
	 *      (java.lang.String, it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.FascicoloDTO, boolean).
	 */
	@Override
	public List<DocumentoAllegabileDTO> getDocumentiAllegabili(final String wobNumber, final UtenteDTO utente, final FascicoloDTO fascicolo, final boolean withContent) {
		FilenetPEHelper fpeh = null;

		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			final String documentTitle = fpeh.getMetadato(wobNumber, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY)).toString();

			return getDocumentiAllegabili(Integer.parseInt(documentTitle), utente, fascicolo, null, null, withContent);

		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_DOCUMENTI, e);
			throw new RedException(e);
		} finally {
			logoff(fpeh);
		}

	}

	private static List<DocumentoAllegabileDTO> getDocumentiAllegabili(final Integer idDocumento, final UtenteDTO utente, final FascicoloDTO fascicolo,
			final Integer idDocumentoToExclude, final List<AllegatoDTO> allegatiDaAllacciToSelect, final boolean withContent) {
		final List<DocumentoAllegabileDTO> listaDocumentiReturn = new ArrayList<>();
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final String documentTitle = idDocumento.toString();

			final Collection<DocumentoAllegabileDTO> listaDocumenti = fceh.getOnlyDocumentiRedFascicoloAllegabili(fascicolo, utente.getIdAoo(), false, withContent);
			for (final DocumentoAllegabileDTO docAllegabile : listaDocumenti) {

				if (idDocumentoToExclude != null && docAllegabile.getDocumentTitle().equals(idDocumentoToExclude.toString()) && docAllegabile.getContenuto() == null) {
					continue;
				}

				if (allegatiDaAllacciToSelect != null) {
					for (final AllegatoDTO allegatoToSelect : allegatiDaAllacciToSelect) {

						if (allegatoToSelect.equalsFromAllacci(fascicolo.getIdFascicolo(), docAllegabile.getDocumentTitle(), null)) {
							docAllegabile.setSelected(true);
							docAllegabile.setDisabled(true);
							break;
						}
					}
				}

				if (!docAllegabile.getDocumentTitle().equals(documentTitle)) {
					listaDocumentiReturn.add(docAllegabile);
				} else {
					listaDocumentiReturn.add(0, docAllegabile);
				}

			}

			for (final DocumentoAllegabileDTO docAllegabile : listaDocumenti) {

				if (idDocumentoToExclude != null && docAllegabile.getDocumentTitle().equals(idDocumentoToExclude.toString())) {
					continue;
				}

				final Collection<DocumentoAllegabileDTO> allegatiDocFascicolo = fceh.getAllegatiAllegabili(docAllegabile.getFascicolo(), docAllegabile.getDocumentTitle(),
						false, utente.getIdAoo(), false, withContent);

				if (allegatiDaAllacciToSelect != null) {
					for (final DocumentoAllegabileDTO allegato : allegatiDocFascicolo) {
						for (final AllegatoDTO allegatoToSelect : allegatiDaAllacciToSelect) {

							if (allegatoToSelect.equalsFromAllacci(fascicolo.getIdFascicolo(), allegato.getDocumentTitleDocumentoPrincipale(), allegato.getDocumentTitle())) {
								allegato.setSelected(true);
								allegato.setDisabled(true);
								break;
							}
						}
					}
				}

				listaDocumentiReturn.addAll(allegatiDocFascicolo);
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_DOCUMENTI, e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}

		return listaDocumentiReturn;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#
	 *      getDocumentiAllegabiliMap (it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.List, java.lang.Integer, java.util.List, boolean).
	 */
	@Override
	public Map<FascicoloDTO, List<DocumentoAllegabileDTO>> getDocumentiAllegabiliMap(final UtenteDTO utente, final List<Integer> idDocs, final Integer idDocumentoToExclude,
			final List<AllegatoDTO> allegatiDaAllacciToSelect, final boolean withContent) {
		final Map<FascicoloDTO, List<DocumentoAllegabileDTO>> returnMap = new HashMap<>();

		FascicoloDTO fascicolo = null;
		List<DocumentoAllegabileDTO> documenti = null;
		for (final Integer idDocumento : idDocs) {
			fascicolo = getFascicoloProcedimentale(Constants.EMPTY_STRING + idDocumento, utente.getIdAoo().intValue(), utente);
			if (!returnMap.containsKey(fascicolo)) {
				documenti = getDocumentiAllegabili(idDocumento, utente, fascicolo, idDocumentoToExclude, allegatiDaAllacciToSelect, withContent);
				if (documenti != null && !documenti.isEmpty()) {
					fascicolo.setDescrizioneDocumentoProvenienza("(allaccio del documento " + documenti.get(0).getNumProtId() + ")");
					returnMap.put(fascicolo, documenti);
				}

			}
		}

		return returnMap;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#getDocsProtRiferimento(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.Integer, java.lang.Integer, boolean).
	 */
	@Override
	public Map<FascicoloDTO, List<DocumentoAllegabileDTO>> getDocsProtRiferimento(final UtenteDTO utente, final Integer numProt, final Integer annoProt,
			final boolean isForIntegrazioneDati) {
		final ParamsRicercaAvanzataDocDTO paramsRicerca = new ParamsRicercaAvanzataDocDTO();
		final Map<FascicoloDTO, List<DocumentoAllegabileDTO>> output = new HashMap<>();
		List<DocumentoAllegabileDTO> documenti = new ArrayList<>();
		List<DocumentoAllegabileDTO> documentiDaEliminare = null;
		List<FascicoloDTO> fascicoliDTO = new ArrayList<>();
		Integer documentTitle = null;
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;

		try {

			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			/**
			 * Recupero Documento partendo dal protocollo.
			 */
			paramsRicerca.setNumeroProtocolloDa(numProt);
			paramsRicerca.setNumeroProtocolloA(numProt);
			paramsRicerca.setAnnoProtocolloDa(annoProt);
			paramsRicerca.setAnnoProtocolloA(annoProt);
			final Collection<MasterDocumentRedDTO> result = ricercaAvanzataDocumentiSRV.eseguiRicercaDocumenti(paramsRicerca, utente, false, true);

			if (result.iterator().hasNext()) {
				documentTitle = Integer.parseInt(result.iterator().next().getDocumentTitle());
			}

			/**
			 * Recupero dei Fascicoli che contengono il protocollo
			 * 
			 * List<FascicoloDTO> fascicoliDTO = fceh.getFascicoliDocumento(documentTitle,
			 * idAoo);
			 */
			if (documentTitle != null) {
				fascicoliDTO = fceh.getFascicoliDocumento(documentTitle.toString(), utente.getIdAoo().intValue());
			}

			if (!fascicoliDTO.isEmpty()) {

				for (final FascicoloDTO f : fascicoliDTO) {

					if (!output.containsKey(f)) {
						documenti = getDocumentiAllegabili(documentTitle, utente, f, null, null, false);
						if (documenti != null && !documenti.isEmpty()) {

							documentiDaEliminare = new ArrayList<>();

							for (final DocumentoAllegabileDTO d : documenti) {

								if (!DocumentoAllegabileType.DOCUMENTO.equals(d.getTipo())) {
									documentiDaEliminare.add(d);
									continue;
								}

								// Rendere non selezionabile il documento.
								d.setDisabled(true);

								// Renderlo selezionabile solo se è un'ENTRATA.
								final CategoriaDocumentoEnum catDoc = CategoriaDocumentoEnum.get(d.getIdCategoria());
								if (CategoriaDocumentoEnum.ENTRATA.equals(catDoc)) {

									// Calcolo della coda del Documento.
									final VWWorkObject wo = fpeh.getWorkflowPrincipale(d.getDocumentTitleDocumentoPrincipale(), null);
									DocumentQueueEnum dqe = null;
									if (wo != null) {
										dqe = DocumentQueueEnum.getByDisplayName(wo.getCurrentQueueName());
									}

									// E se il wf principale e in SOSPESO e l'allaccio principale è di tipo
									// INTEGRAZIONE_DATI.
									if (isForIntegrazioneDati && DocumentQueueEnum.SOSPESO.equals(dqe) /* && tipoIntegrazioneDati.equals(idTipoDocAllaccio.toString()) */) {
										d.setDisabled(false);
									}

									if (!isForIntegrazioneDati && (DocumentQueueEnum.SOSPESO.equals(dqe) || DocumentQueueEnum.DA_LAVORARE.equals(dqe))) {
										// Oppure se non è per INTEGRAZIONE_DATI (e quindi è per RITIRO_AUTOTUTELA) e il
										// wf principale si trovi in SOSPESO o DA_LAVORARE.
										d.setDisabled(false);
									}

								}

							}

							documenti.removeAll(documentiDaEliminare);
							output.put(f, documenti);

						}
					}

				}

			}
			/**
			 * Recupero di tutti i documenti all'interno di ogni singolo fascicolo
			 * 
			 * documenti = getDocumentiAllegabili(idDocumento, utente, fascicolo,
			 * idDocumentoToExclude, allegatiDaAllacciToSelect, withContent);
			 */

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero dei content dei documenti", e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#
	 *      loadContent4DocumentiAllegabili(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.List).
	 */
	@Override
	public void loadContent4DocumentiAllegabili(final UtenteDTO utente, final List<DocumentoAllegabileDTO> allegatiSelezionati) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			Document doc = null;
			for (final DocumentoAllegabileDTO allegatoSelezionato : allegatiSelezionati) {
				if (allegatoSelezionato.getTipo().equals(DocumentoAllegabileType.EMAIL)) {
					continue;
				} else if (allegatoSelezionato.getTipo().equals(DocumentoAllegabileType.ALLEGATO)) {
					doc = fceh.getAllegatoForDownload(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY),
							allegatoSelezionato.getDocumentTitle());
				} else {
					doc = fceh.getDocumentByDTandAOO(allegatoSelezionato.getDocumentTitle(), utente.getIdAoo());
				}
				allegatoSelezionato.setContenuto(FilenetCEHelper.getDocumentContentAsDataHandler(doc));
			}

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero dei content dei documenti", e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#recuperaFascicoliDelDocumento
	 *      (it.ibm.red.business.dto.MasterDocumentRedDTO,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<FascicoloDTO> recuperaFascicoliDelDocumento(final MasterDocumentRedDTO doc, final UtenteDTO utente) {
		IFilenetCEHelper fceh = null;
		Connection connection = null;
		final List<FascicoloDTO> fascicoliDelDocumento = new ArrayList<>();

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			connection = setupConnection(getDataSource().getConnection(), false);

			final Document docFilenet = fceh.getDocumentRedForDetail(doc.getClasseDocumentale(), doc.getDocumentTitle());
			final List<Document> fascicoliFilenet = fceh.getFascicoliByFoldersFiledIn(docFilenet.get_FoldersFiledIn(), utente.getIdAoo().intValue());

			for (final Document docFascicolo : fascicoliFilenet) {
				final FascicoloDTO fascicoloDTO = TrasformCE.transform(docFascicolo, TrasformerCEEnum.FROM_DOCUMENTO_TO_FASCICOLO, connection);
				fascicoliDelDocumento.add(fascicoloDTO);
			}

		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dei fascicoli: " + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}
		return fascicoliDelDocumento;
	}

	/**
	 * @see it.ibm.red.business.service.IFascicoloSRV#eliminaFascicolo
	 *      (java.lang.String, java.lang.Long,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetHelper).
	 */
	@Override
	public boolean eliminaFascicolo(final String idFascicolo, final Long idAoo, final IFilenetCEHelper fceh) {
		return fceh.eliminaFascicoloSeVuoto(idFascicolo, idAoo);
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IFascicoloFacadeSRV#verificaFascicoloFlusso(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public boolean verificaFascicoloFlusso(String idfascicolo, UtenteDTO utente) {
		boolean isFascicoloFlusso = false;
		List<DetailDocumentRedDTO> documentiFascicolo = getDocumentiByIdFascicoloPadre(Integer.parseInt(idfascicolo), utente);
		
		for (DetailDocumentRedDTO documento : documentiFascicolo) {
			if (StringUtils.isNotBlank(documento.getCodiceFlusso())) {
				
				TipoContestoProceduraleEnum flusso = TipoContestoProceduraleEnum.getEnumById(documento.getCodiceFlusso());
				
				if(flusso != null && flusso.isBlindaFascicolo()) {
					isFascicoloFlusso = true;
					break;
				}
			}
		}
		
		return isFascicoloFlusso;
	}
	
	@Override
	public final FascicoloDTO getFascicoloProcedimentale(final String documentTitle, final Integer idAoo, final UtenteDTO utente, final AooFilenet aooFilenet) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), aooFilenet.getAoo().getIdAoo());

			return getFascicoloProcedimentale(documentTitle, idAoo, utente, fceh);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}
	}

}