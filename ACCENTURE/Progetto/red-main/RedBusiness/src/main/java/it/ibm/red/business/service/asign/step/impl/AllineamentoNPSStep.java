package it.ibm.red.business.service.asign.step.impl;

import java.sql.Connection;
import java.util.Map;

import org.springframework.stereotype.Service;

import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignStepResultDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.asign.step.ASignStepAbstract;
import it.ibm.red.business.service.asign.step.ContextASignEnum;
import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * Step di allineamento NPS - firma asincrona.
 */
@Service
public class AllineamentoNPSStep extends ASignStepAbstract {

	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = -7553486970039206304L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AllineamentoNPSStep.class.getName());

	/**
	 * Esegue l'allineamento NPS per un item che ha raggiunto lo step @see StepEnum.ALLINEAMENTO_NPS.
	 * @param con
	 * @param item
	 * @param context
	 * @return risultato dell'esecuzione dello step
	 */
	@Override
	protected ASignStepResultDTO run(Connection con, ASignItemDTO item, Map<ContextASignEnum, Object> context) {
		Long idItem = item.getId();
		LOGGER.info("run -> START [ID item: " + idItem + "]");
		ASignStepResultDTO out = new ASignStepResultDTO(item.getId(), getStep());
		
		// Si recupera l'utente firmatario
		UtenteDTO utenteFirmatario = getUtenteFirmatario(item, con);
		
		// ### ALLINEAMENTO DI NPS ###
		EsitoOperazioneDTO esitoAllineamentoNPS = signSRV.allineamentoNPS(item, utenteFirmatario, con);
		
		if (esitoAllineamentoNPS.isEsito()) {
			out.setStatus(true);
			out.appendMessage("[OK] Allineamento di NPS eseguito");
		} else {
			throw new RedException("[KO] Errore nello step di allineamento di NPS: " + esitoAllineamentoNPS.getNote() 
				+ ". [Codice errore: " + esitoAllineamentoNPS.getCodiceErrore() + "]");
		}
		
		LOGGER.info("run -> END [ID item: " + idItem + "]");
		return out;
	}

	/**
	 * Restituice lo @see StepEnum a cui è associata questa classe.
	 * @return StepEnum
	 */
	@Override
	protected StepEnum getStep() {
		return StepEnum.ALLINEAMENTO_NPS;
	}

	/**
	 * @see it.ibm.red.business.service.asign.step.ASignStepAbstract#getErrorMessage().
	 */
	@Override
	protected String getErrorMessage() {
		return "[KO] Errore imprevisto nello step di allineamento di NPS";
	}

}
