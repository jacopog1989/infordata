package it.ibm.red.business.helper.pdf;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.pdfbox.io.RandomAccessBufferedFileInputStream;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.google.common.net.MediaType;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields.FieldPosition;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfAnnotation;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfException;
import com.itextpdf.text.pdf.PdfFileSpecification;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.ApprovazioneDTO;
import it.ibm.red.business.dto.AttachmentDTO;
import it.ibm.red.business.dto.PrintDocumentDTO;
import it.ibm.red.business.dto.StampigliaturaSegnoGraficoDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.IconEnum;
import it.ibm.red.business.enums.PriorityAlertEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.exception.PdfHelperException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.DocumentoUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class PdfHelper.
 *
 * @author CPIERASC
 * 
 *         Helper gestione pdf.
 */
public final class PdfHelper {

	private static final String DEL = " del ";

	/**
	 * Label MEF.
	 */
	private static final String MEF_LABEL = "MEF - ";

	/**
	 * Messaggio errore conversione documento. Occorre appendere l'identificativo
	 * del documento al messaggio.
	 */
	private static final String ERROR_CONVERSIONE_DOCUMENTO_MSG = "Errore durante la conversione del documento : ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PdfHelper.class.getName());

	/**
	 * Percentuale di scala immagine mef.
	 */
	private static final int SCALE_PERCENTAGE_IMG_MEF = 15;

	/**
	 * Padding top cella tabella.
	 */
	private static final int PADDING_TOP_CELL = 10;

	/**
	 * Padding bottom tabella.
	 */
	private static final int PADDING_BOTTOM_TABLE = 10;

	/**
	 * Size header approvazioni.
	 */
	private static final int APPROVAZIONI_HEADER_SIZE = 16;

	/**
	 * Spaziatura approvazioni.
	 */
	private static final int NEW_LINES_APPROVAZIONI = 3;

	/**
	 * Dimensione label approvazioni.
	 */
	private static final int APPROVAZIONI_LABEL_SIZE = 12;

	/**
	 * Dimensione testo approvazioni.
	 */
	private static final int APPROVAZIONI_TEXT_SIZE = 12;

	/**
	 * Numero di nuove righe tra titolo e corpo di un contributo.
	 */
	private static final int NEW_LINES_CONTRIBUTO = 3;

	/**
	 * Dimensione testo contributo.
	 */
	private static final int CONTRIBUTO_TEXT_SIZE = 16;

	/**
	 * Dimensione header contributo.
	 */
	private static final int CONTRIBUTO_HEADER_SIZE = 20;

	/**
	 * Messaggio d'errore.
	 */
	private static final String ERRORE_DURANTE_IL_PROCESSAMENTO_DEL_PDF = "Errore durante il processamento del PDF: ";

	/**
	 * Enum ChiaviCampoFirma.
	 */
	public enum ChiaviCampoFirma {

		/** Firmatario1 */
		FIRMATARIO1("Firmatario1", true, false),

		/** Firmatario2. */
		FIRMATARIO2("Firmatario2", true, false),

		/** Firmatario3. */
		FIRMATARIO3("Firmatario3", true, false),

		/** Il Dirigente. */
		DIRIGENTE("Il Dirigente", true, true),

		/** Il direttore. */
		DIRETTORE("Il Direttore", true, true),

		/** L’Ispettore Generale Capo */
		ISPETTORE("L’Ispettore Generale Capo", true, true),

		/** Il Ragioniere Generale dello Stato. */
		RGS("Il Ragioniere Generale dello Stato", false, true),

		/** L'Ispettore Generale Capo. */
		ISPETTORE2("L'Ispettore Generale Capo", true, true);

		/**
		 * Key
		 */
		private String key;

		/**
		 * Cover
		 */
		private boolean cover;

		/**
		 * Only Bottom
		 */
		private boolean onlyBottom;

		/**
		 * Costruttore private ChiaviCampoFirma.
		 */
		ChiaviCampoFirma(final String key, final boolean cover, final boolean onlyBottom) {
			this.key = key;
			this.cover = cover;
			this.onlyBottom = onlyBottom;
		}

		/**
		 * @return the key
		 */
		public String getKey() {
			return key;
		}

		/**
		 * @return true, if is cover
		 */
		public boolean isCover() {
			return cover;
		}

		/**
		 * @return true, if is only bottom
		 */
		public boolean isOnlyBottom() {
			return onlyBottom;
		}

	}

	/**
	 * Campo costante LARGHEZZA_BASE_CAMPO_FIRMA
	 */
	private static final int LARGHEZZA_BASE_CAMPO_FIRMA = 142;

	/**
	 * Campo costante ALTEZZA_BASE_CAMPO_FIRMA
	 */
	private static final int ALTEZZA_BASE_CAMPO_FIRMA = 57;

	/**
	 * Costruttore.
	 */
	private PdfHelper() {

	}

	/**
	 * Aggiungi allegati.
	 * 
	 * @param source      pdf
	 * @param attachments lista allegati
	 * @return pdf modificato
	 */
	public static byte[] addAttachment(final byte[] source, final Collection<AttachmentDTO> attachments) {
		try {
			final DataManager dm = new DataManager(source, true);
			final PdfWriter writer = dm.getWriter();
			for (final AttachmentDTO attachment : attachments) {
				String name = attachment.getFileName();
				if (attachment.getNeedSign() != null && attachment.getNeedSign()) {
					name = "[F] " + attachment.getFileName();
				}
				PdfFileSpecification fs;
				if (attachment.getContent() != null) {
					fs = PdfFileSpecification.fileEmbedded(writer, null, name, attachment.getContent());
				} else {
					fs = PdfFileSpecification.fileEmbedded(writer, null, name, FileUtils.getFile(PdfHelper.class.getClassLoader(), "resources/anteprimaNonDisponibile.pdf"));
				}
				writer.addFileAttachment(attachment.getDescription(), fs);
			}
			return dm.close();
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento degli allegati del PDF: ", e);
			throw new PdfHelperException(e);
		}
	}

	/**
	 * Ritorna il numero di pagine.
	 * 
	 * @param source pdf
	 * @return numero di pagine
	 */
	public static Integer getNumberOfPages(final byte[] source) {
		try {
			final PdfReader reader = new PdfReader(source);
			return reader.getNumberOfPages();
		} catch (final Exception e) {
			LOGGER.error(ERRORE_DURANTE_IL_PROCESSAMENTO_DEL_PDF, e);
			throw new PdfHelperException(e);
		}
	}

	/**
	 * Imposta il messaggio in apertura del pdf.
	 * 
	 * @param source   pdf
	 * @param priority priorità del messaggio
	 * @param msg      messaggio
	 * @return pdf risultante
	 */
	public static byte[] openDocMessage(final byte[] source, final PriorityAlertEnum priority, final String msg) {
		final DataManager dm = new DataManager(source);
		PdfAction action = null;
		if (msg != null) {
			action = PdfAction.javaScript("app.alert('" + msg + "', " + priority.getValue() + ");", dm.getWriter());
		}
		try {
			dm.getStamper().setPageAction(PdfWriter.PAGE_OPEN, action, 1);
		} catch (final PdfException e) {
			LOGGER.error(ERRORE_DURANTE_IL_PROCESSAMENTO_DEL_PDF, e);
			throw new PdfHelperException(e);
		}
		return dm.close();
	}

	/**
	 * Rimuove messaggio in apertura del pdf.
	 * 
	 * @param source pdf
	 * @return pdf modificato
	 */
	public static byte[] removeOpenDocMessage(final byte[] source) {
		return openDocMessage(source, null, null);
	}

	/**
	 * Metodo per scalare una immagine.
	 * 
	 * @param source immagine sorgente
	 * @param width  nuova dimensione
	 * @return immagine scalata
	 */
	public static byte[] scaleImg(final byte[] source, final Integer width) {
		try {
			final BufferedImage sourceImage = ImageIO.read(new ByteArrayInputStream(source));
			final java.awt.Image thumbnail = sourceImage.getScaledInstance(width, -1, java.awt.Image.SCALE_SMOOTH);
			final BufferedImage bufferedThumbnail = new BufferedImage(thumbnail.getWidth(null), thumbnail.getHeight(null), BufferedImage.TYPE_INT_RGB);
			bufferedThumbnail.getGraphics().drawImage(thumbnail, 0, 0, null);
			final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			ImageIO.write(bufferedThumbnail, "jpeg", outputStream);
			final byte[] output = outputStream.toByteArray();
			outputStream.close();
			return output;
		} catch (final Exception e) {
			LOGGER.error(ERRORE_DURANTE_IL_PROCESSAMENTO_DEL_PDF, e);
			throw new PdfHelperException(e);
		}
	}

	/**
	 * Converte una pagina del pdf in immagine.
	 * 
	 * @return Byte array vuoto.
	 */
	public static byte[] convertPageToJpg() {
		return new byte[0];
	}

	/**
	 * Aggiunta nota.
	 * 
	 * @param visibleWidth
	 *            larghezza visibile pdf (utile se ridimensionato)
	 * @param source
	 *            pdf
	 * @param page
	 *            pagina su apporre la nota
	 * @param xPosition
	 *            posizione nota
	 * @param yPosition
	 *            posizione nota
	 * @param icon
	 *            icona nota
	 * @param color
	 *            colore nota
	 * @param title
	 *            titolo nota
	 * @param text
	 *            testo nota
	 * @param open
	 *            flag aperto/chiuso
	 * @return pdf
	 */
	public static byte[] addStickyNote(final Integer visibleWidth, final byte[] source, final Integer page, final Integer xPosition, final Integer yPosition,
			final IconEnum icon, final BaseColor color, final String title, final String text, final Boolean open) {
		final DataManager dm = new DataManager(source);
		final Rectangle pagesize = dm.getReader().getPageSizeWithRotation(page);
		final float k = pagesize.getWidth() / visibleWidth;
		final float xPositionNew = xPosition * k;
		final float yPositionNew = yPosition * k;
		final Rectangle position = new Rectangle(pagesize.getLeft() + xPositionNew, pagesize.getBottom(), pagesize.getRight(), pagesize.getTop() - yPositionNew);
		final PdfAnnotation comment = PdfAnnotation.createText(dm.getStamper().getWriter(), position, title, text, open, icon.getIconName());
		comment.setColor(color);
		dm.getStamper().addAnnotation(comment, page);
		return dm.close();
	}

	/**
	 * Aggiunge un allegato visibile al pdf.
	 * 
	 * @param visibleWidth larghezza visibile del pdf (utile se ridimensionato)
	 * @param source       pdf
	 * @param page         pagina su cui mettere il pin
	 * @param att          allegato
	 * @param filename     nome file allegato
	 * @param text         testo pin
	 * @param xPosition    posizione pin
	 * @param yPosition    posizione pin
	 * @param pinDimension dimensione pin che rappresenta l'allegato
	 * @return pdf aggiornato
	 */
	public static byte[] addAttachment(final Integer visibleWidth, final byte[] source, final Integer page, final byte[] att, final String filename, final String text,
			final Integer xPosition, final Integer yPosition, final Integer pinDimension) {
		try {
			final DataManager dm = new DataManager(source);
			final Rectangle pagesize = dm.getReader().getPageSizeWithRotation(page);
			final float k = pagesize.getWidth() / visibleWidth;
			final float xPositionNew = xPosition * k;
			final float yPositionNew = yPosition * k;
			final Rectangle position = new Rectangle(pagesize.getLeft() + xPositionNew, pagesize.getTop() - yPositionNew - pinDimension,
					pagesize.getLeft() + xPositionNew + pinDimension, pagesize.getTop() - yPositionNew);
			final PdfFileSpecification fs = PdfFileSpecification.fileEmbedded(dm.getStamper().getWriter(), null, filename, att);
			final PdfAnnotation attachment = PdfAnnotation.createFileAttachment(dm.getWriter(), position, text, fs);
			dm.getStamper().addAnnotation(attachment, page);
			return dm.close();
		} catch (final IOException e) {
			LOGGER.error(ERRORE_DURANTE_IL_PROCESSAMENTO_DEL_PDF, e);
			throw new PdfHelperException(e);
		}
	}

	/**
	 * Aggiunge una immagine al pdf.
	 * 
	 * @param visibleWidth larghezza visibile dell'immagine (utile se il pdf è stato
	 *                     ridimensionato)
	 * @param source       pdf sorgente
	 * @param page         pagina su cui apporre l'immagine
	 * @param imgBuf       immagine
	 * @param x            ascissa immagine
	 * @param y            ordinata immagine
	 * @param w            larghezza immagine
	 * @return pdf aggiornato
	 */
	public static byte[] addImage(final Integer visibleWidth, final byte[] source, final Integer page, final byte[] imgBuf, final int x, final int y, final int w) {
		try {
			final DataManager dm = new DataManager(source);
			final Image img = Image.getInstance(imgBuf);
			img.setAbsolutePosition(0, 0);
			final float h = w * img.getScaledHeight() / img.getScaledWidth();
			img.scaleAbsolute(w, h);
			final PdfContentByte cb = dm.getStamper().getOverContent(page);
			final Rectangle pagesize = dm.getReader().getPageSizeWithRotation(page);
			final float k = pagesize.getWidth() / visibleWidth;
			final float xNew = x * k;
			final float yNew = y * k;
			cb.addImage(img, w, 0, 0, h, xNew, pagesize.getTop() - yNew - h);
			return dm.close();
		} catch (final Exception e) {
			LOGGER.error(ERRORE_DURANTE_IL_PROCESSAMENTO_DEL_PDF, e);
			throw new PdfHelperException(e);
		}
	}

	/**
	 * Ritorna il numero di campi firma non firmati.
	 * 
	 * @param source pdf
	 * @return numero di campi firma non firmati
	 */
	public static int getBlankSignatureNumber(final byte[] source) {
		Integer out = 0;
		try {
			final PdfReader pr = new PdfReader(source);
			final List<?> signatures = pr.getAcroFields().getBlankSignatureNames();
			if (signatures != null) {
				out = signatures.size();
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei campi firma vuoti del PDF: ", e);
		}
		return out;
	}

	/**
	 * Permette di disegnare il rettangolo su una specifica posizione
	 *
	 * @param source   the source
	 * @param position the position
	 * @param bColor   the b color
	 * @param font     the font
	 * @param text     the text
	 * @return the byte[]
	 */
	public static byte[] drawRectOnPosition(final byte[] source, final FieldPosition position, final BaseColor bColor, final Font font, final String text) {
		byte[] out = null;
		if (position != null) {
			final DataManager dm = new DataManager(source);
			final PdfContentByte canvas = dm.getStamper().getOverContent(position.page);
			final Rectangle rect = new Rectangle(position.position);
			rect.setBorder(Rectangle.BOX);
			rect.setBorderWidth(2);
			rect.setBackgroundColor(bColor);

			canvas.rectangle(rect);

			canvas.fillStroke();

			final ColumnText ct = new ColumnText(canvas);
			ct.setSimpleColumn(rect);

			final Paragraph p = new Paragraph(text);
			p.setAlignment(Element.ALIGN_CENTER);
			p.setFont(font);

			ct.addElement(p);

			try {
				ct.go();
			} catch (final DocumentException e) {
				LOGGER.error(e);
				throw new RedException(e);
			}
			out = dm.close();
		}
		return out;
	}

	/**
	 * Permette di disegnare un'immagine in una specifica posizione
	 *
	 * @param source     the source
	 * @param image      the image
	 * @param position   the position
	 * @param imageWidth the image width
	 */
	public static void drawImageOnPosition(final byte[] source, final byte[] image, final FieldPosition position, final Integer imageWidth) {
		addImage(imageWidth, source, position.page, image, (int) position.position.getLeft(), (int) position.position.getTop(), imageWidth);
	}

	/**
	 * Ritorna il primo campo vuoto da firmare
	 *
	 * @param source the source
	 * @return the first blank signature position
	 */
	public static FieldPosition getFirstBlankSignaturePosition(final byte[] source) {
		FieldPosition out = null;

		try {
			final PdfReader pr = new PdfReader(source);
			final List<String> signatures = pr.getAcroFields().getBlankSignatureNames();
			if (signatures != null && !signatures.isEmpty()) {
				final List<FieldPosition> positions = pr.getAcroFields().getFieldPositions(signatures.get(0));
				out = positions.get(0);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero della posizione del primo campo firma vuoto.", e);
		}

		return out;
	}

	/**
	 * Ritorna il numero dei campi firma FIRMATI.
	 * 
	 * @param source file
	 * @return numero di firme
	 */
	public static int getSignatureNumber(final byte[] source) {
		Integer out = 0;
		try {
			final PdfReader pr = new PdfReader(source);
			final List<?> signatures = pr.getAcroFields().getSignatureNames();
			if (signatures != null) {
				out = signatures.size();
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei campi firma pieni del PDF: ", e);
		}
		return out;
	}

	/**
	 * Metodo per aggiungere una frase ad un paragrafo preesistente.
	 * 
	 * @param paragraph paragrafo preesistente
	 * @param alignment allineamento frase
	 * @param text      testo frase
	 * @param font      font frase
	 * @return frase creata
	 */
	private static Phrase addPhraseToParagraph(final Paragraph paragraph, final int alignment, final String text, final Font font) {
		return addPhraseToParagraph(paragraph, alignment, text, font, 0);
	}

	/**
	 * Metodo per aggiungere una frase ad un paragrafo preesistente aggiungendo in
	 * coda alla frase delle nuove righe.
	 * 
	 * @param paragraph      paragrafo preesistente
	 * @param alignment      allineamento frase
	 * @param text           testo frase
	 * @param font           font frase
	 * @param trailerNewLine numero di nuove righe da aggiungere a fine testo
	 * @return frase creata
	 */
	private static Phrase addPhraseToParagraph(final Paragraph paragraph, final int alignment, final String text, final Font font, final Integer trailerNewLine) {
		final Chunk timesRomanChunk = new Chunk(text, font);

		final Phrase phrase = new Phrase();
		phrase.add(timesRomanChunk);
		paragraph.setAlignment(alignment);

		if (trailerNewLine != null) {
			for (int i = 0; i < trailerNewLine; i++) {
				phrase.add(Chunk.NEWLINE);
			}
		}
		paragraph.add(phrase);

		return phrase;
	}

	/**
	 * Metodo per la creazione di un nuovo pdf che rappresenta un contributo sotto
	 * forma di nota testuale.
	 * 
	 * @param idDoc              identificativo documento
	 * @param inNota             nota che costituisce il contributo
	 * @param idContributo       identificativo del contributo
	 * @param dataContributo     data creazione contributo
	 * @param descContribuente   descrizione utente che ha generato il contributo
	 * @param nomeFileContributo nome dell'eventuale file inserito come contributo
	 *                           insieme alla nota
	 * @return pdf generato
	 */
	public static byte[] createContributo(final Integer idDoc, final String inNota, final Long idContributo, final Date dataContributo, final String descContribuente,
			final String nomeFileContributo) {
		try {
			LOGGER.info("Creazione contributo");
			final Font titlefont = new Font(Font.FontFamily.TIMES_ROMAN, CONTRIBUTO_HEADER_SIZE, Font.BOLDITALIC);
			final Font textfont = new Font(Font.FontFamily.TIMES_ROMAN, CONTRIBUTO_TEXT_SIZE, Font.NORMAL);
			final Font citfont = new Font(Font.FontFamily.TIMES_ROMAN, CONTRIBUTO_TEXT_SIZE, Font.ITALIC);

			final Document document = new Document();
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PdfWriter.getInstance(document, baos);
			document.open();

			final Paragraph paragraphTitle = new Paragraph();
			paragraphTitle.setMultipliedLeading(2);
			addPhraseToParagraph(paragraphTitle, Element.ALIGN_CENTER, "Contributo testuale associato al documento con identificativo " + idDoc, titlefont,
					NEW_LINES_CONTRIBUTO);
			document.add(paragraphTitle);

			final Paragraph paragraphContributo = new Paragraph();
			paragraphContributo.setMultipliedLeading(2);
			String txt = "In data " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(dataContributo);
			if (!StringUtils.isNullOrEmpty(descContribuente)) {
				txt += " l'utente " + descContribuente + " ha fornito";
			} else {
				txt += " è stato fornito ";
			}
			txt += " il seguente contributo (identificativo " + idContributo + "): ";
			LOGGER.info("Testo generato: " + txt);

			// RED consente la validazione dei contributi (response "Valida Contributi")
			// senza l'inserimento di testo e file.
			// Nel caso di PDF generato per la validazione, si sostituisce il testo vuoto
			// ("null") con la stringa "Validato".
			String nota = inNota;
			if (StringUtils.isNullOrEmpty(nota) && StringUtils.isNullOrEmpty(nomeFileContributo)) {
				nota = "Validato";
			}

			addPhraseToParagraph(paragraphContributo, Element.ALIGN_LEFT, txt, textfont, 1);
			addPhraseToParagraph(paragraphContributo, Element.ALIGN_LEFT, "- Testuale: ", textfont);
			addPhraseToParagraph(paragraphContributo, Element.ALIGN_LEFT, "\"" + nota + "\"", citfont, 1);

			if (!StringUtils.isNullOrEmpty(nomeFileContributo)) {
				addPhraseToParagraph(paragraphContributo, Element.ALIGN_LEFT, "- File: ", textfont);
				addPhraseToParagraph(paragraphContributo, Element.ALIGN_LEFT, nomeFileContributo, citfont, 1);
			}

			document.add(paragraphContributo);
			document.close();

			final byte[] output = baos.toByteArray();

			int size = 0;
			if (output != null) {
				size = output.length;
			}
			LOGGER.info("Dimensione content: " + size);
			baos.close();

			return output;
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		}
	}

	/**
	 * Metodo per il dump di un oggetto in una riga di una tabella tramite
	 * reflection.
	 * 
	 * @param rowFont  font da usare nella scrittura della riga della tabella
	 * @param table    tabella su cui inserire la riga
	 * @param mappings lista della mappatura tra colonne della tabella e campi
	 *                 dell'oggetto
	 * @param obj      oggetto contenente i valori
	 */
	private static void dumpValueOnRow(final Font rowFont, final PdfPTable table, final List<TableCellValue> mappings, final Object obj) {
		try {
			for (final TableCellValue tcv : mappings) {
				final String getterName = "get" + StringUtils.firstUppercase(tcv.getProperty());
				final Method mtdGetter = obj.getClass().getMethod(getterName);
				final Object value = mtdGetter.invoke(obj);
				final PdfPCell cell = new PdfPCell();
				cell.addElement(new Phrase(tcv.getFormatter().format(value), rowFont));
				cell.setPaddingBottom(PADDING_BOTTOM_TABLE);
				table.addCell(cell);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di creazione tabella", e);
			throw new PdfHelperException(e);
		}
	}

	/**
	 * Metodo per disegnare una tabella in un pdf.
	 * 
	 * @param titleFont font del titolo
	 * @param rowFont   font della riga
	 * @param mappings  mapping oggetto-riga
	 * @param values    valori
	 * @return tabella
	 */
	private static PdfPTable dumpCollectionOnTable(final Font titleFont, final Font rowFont, final List<TableCellValue> mappings, final Collection<? extends Object> values) {
		final PdfPTable table = new PdfPTable(mappings.size());
		for (final TableCellValue tcv : mappings) {

			final Paragraph paragraphCell = new Paragraph();
			addPhraseToParagraph(paragraphCell, Element.ALIGN_CENTER, tcv.getLabel(), titleFont, 2);

			final PdfPCell cell = new PdfPCell();
			cell.addElement(paragraphCell);
			cell.setPaddingTop(PADDING_TOP_CELL);
			table.addCell(cell);
		}
		for (final Object value : values) {
			dumpValueOnRow(rowFont, table, mappings, value);
		}
		return table;
	}

	/**
	 * Metodo per la creazione del pdf delle approvazioni.
	 * 
	 * @param ispettoratoPreponente ispettorato proponenete
	 * @param idDoc                 identificativo documento
	 * @param values                approvazioni
	 * @return pdf creato
	 */
	public static byte[] createApprovazioniDocument(final String ispettoratoPreponente, final Integer idDoc, final Collection<ApprovazioneDTO> values) {
		try {
			final List<TableCellValue> mappings = new ArrayList<>();
			mappings.add(new TableCellValue("UFFICIO", "descUfficio"));
			mappings.add(new TableCellValue("UTENTE", "descUtente"));
			mappings.add(new TableCellValue("TIPOLOGIA", "descApprovazione"));
			mappings.add(new TableCellValue("DATA", "dataApprovazione", obj -> new SimpleDateFormat("dd/MM/yyyy").format(obj)));

			final Document document = new Document();

			final ByteArrayOutputStream stream = new ByteArrayOutputStream();
			PdfWriter.getInstance(document, stream);

			document.open();

			final Font titlefont = new Font(Font.FontFamily.TIMES_ROMAN, APPROVAZIONI_HEADER_SIZE, Font.BOLDITALIC);
			final Font labelfont = new Font(Font.FontFamily.TIMES_ROMAN, APPROVAZIONI_LABEL_SIZE, Font.BOLD);
			final Font textfont = new Font(Font.FontFamily.TIMES_ROMAN, APPROVAZIONI_TEXT_SIZE, Font.NORMAL);

			final byte[] byteImgMEF = FileUtils.getByteFromInputStream(PdfHelper.class.getResourceAsStream("/logo_mef_documenti.png"));
			final Image imgMEF = Image.getInstance(byteImgMEF);
			imgMEF.scalePercent(SCALE_PERCENTAGE_IMG_MEF);
			imgMEF.setAlignment(Element.ALIGN_CENTER);
			document.add(imgMEF);

			document.add(new Paragraph());
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);

			final Paragraph paragraphIspettorato = new Paragraph();
			addPhraseToParagraph(paragraphIspettorato, Element.ALIGN_RIGHT, "Ispettorato - Ufficio proponente: " + ispettoratoPreponente, textfont, NEW_LINES_APPROVAZIONI);
			document.add(paragraphIspettorato);

			final Paragraph paragraphTitle = new Paragraph();
			addPhraseToParagraph(paragraphTitle, Element.ALIGN_CENTER, "Approvazioni associate al documento con identificativo " + idDoc, titlefont, NEW_LINES_APPROVAZIONI);
			document.add(paragraphTitle);

			final PdfPTable pdfPTable = dumpCollectionOnTable(labelfont, textfont, mappings, values);
			document.add(pdfPTable);

			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);

			final Paragraph paragraphEnd = new Paragraph();
			addPhraseToParagraph(paragraphEnd, Element.ALIGN_LEFT, "Data generazione documento", textfont, 1);
			addPhraseToParagraph(paragraphEnd, Element.ALIGN_LEFT, new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()), textfont, 1);
			document.add(paragraphEnd);

			document.close();

			return stream.toByteArray();
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di creazione del documento contenente le approvazioni", e);
			throw new RedException(e);
		}
	}

	/**
	 * Ritorna true se nel contenuto fornito in ingresso esistono dei campi firma
	 * non firmati.
	 * 
	 * @param accessContentStream contenuto da analizzare
	 * @return true se il contenuto possiede dei campi firma non firmati, false
	 *         altrimenti
	 */
	public static boolean checkCampoFirma(final InputStream accessContentStream) {
		final byte[] bytes = FileUtils.getByteFromInputStream(accessContentStream);
		final Integer nBlankSignFields = getBlankSignatureNumber(bytes);
		return nBlankSignFields > 0;
	}

	/**
	 * Verifica se un PDF è protetto o meno.
	 *
	 * @param content the content
	 * @return true se il PDF è valido (NON protetto), false altrimenti
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static boolean checkValidaPDF(final byte[] content) throws IOException {
		boolean isValid = true;
		boolean pdfBoxError = false;
		PDDocument originalPdfDoc = null;
		DataManager dm = null;

		try {
			final InputStream isContent = new ByteArrayInputStream(content);
			final RandomAccessBufferedFileInputStream rabfis = new RandomAccessBufferedFileInputStream(isContent);
			final PDFParser parser = new PDFParser(rabfis);
			// Si verifica con PDFBox
			parser.parse();
			originalPdfDoc = parser.getPDDocument();

			if (originalPdfDoc.isEncrypted()) {
				isValid = false;
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il check del PDF con PDFBox, si prova ad effettuare il check con iText.", e);
			pdfBoxError = true;
		} finally {
			if (originalPdfDoc != null) {
				originalPdfDoc.close();
			}
		}

		if (pdfBoxError) {
			try {
				// Si verifica con iText
				dm = new DataManager(content);
				final PdfReader reader = dm.getReader();

				if (reader.isEncrypted()) {
					isValid = false;
				}
			} catch (final Exception e) {
				LOGGER.error("Errore durante il check del PDF con iText, non è stato possibile determinare se il content è protetto", e);
				throw new RedException(e);
			} finally {
				if (dm != null) {
					dm.close();
				}
			}
		}

		return isValid;
	}

	/**
	 * Converte pagina in immagine.
	 * 
	 * @param source pdf
	 * @param page   pagina 1-based
	 * @return immagine
	 */
	private static byte[] convertImgToPDF(final byte[] image, final Boolean fitA4) {
		byte[] out;
		try {
			final Document document = new Document();
			final ByteArrayOutputStream stream = new ByteArrayOutputStream();
			PdfWriter.getInstance(document, stream);
			document.open();
			final Image img = Image.getInstance(image);
			if (fitA4 != null && fitA4) {
				img.scaleToFit(PageSize.A4.getWidth(), PageSize.A4.getHeight());
			}
			document.add(img);
			document.close();
			out = stream.toByteArray();
			stream.close();
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		}
		return out;
	}

	/**
	 * Converte pagina html in pdf.
	 * 
	 * @param source contentHTML
	 * @return pdf
	 */
	private static byte[] convertHtmlToPDF(final String str) {
		byte[] out = null;

		try {
			out = createPDFFromHtml(str);
		} catch (final Exception e) {
			LOGGER.error(e);
		}
		if (out == null) {
			out = convertTextToPDF(str);
		}

		return out;
	}

	/**
	 * Crea un pdf a partire da una pagine html
	 * 
	 * @param source contentHTML
	 * @return pdf
	 */
	private static byte[] createPDFFromHtml(final String str) {
		byte[] out = null;
		final byte[] htmlContent = str.getBytes();

		try {
			final String sContent = new String(htmlContent, StandardCharsets.UTF_8);

			final ITextRenderer itext = new ITextRenderer();
			itext.setDocumentFromString(sContent);
			itext.layout();

			final ByteArrayOutputStream bos = new ByteArrayOutputStream();
			itext.createPDF(bos, true);
			out = bos.toByteArray();
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		}

		return out;
	}

	private static byte[] convertTextToPDF(final String str) {
		byte[] out;

		try {
			final Document document = new Document();
			final ByteArrayOutputStream stream = new ByteArrayOutputStream();
			PdfWriter.getInstance(document, stream);
			document.open();
			document.add(new Paragraph(str));
			document.close();
			out = stream.toByteArray();
			stream.close();
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		}

		return out;
	}

	/**
	 * Converte pagina xml in pdf.
	 * 
	 * @param source        contentHTML
	 * @param formattazione true
	 * @return pdf
	 */
	private static byte[] convertXMLToPDF(final String str, final Boolean prettyPrint) {
		String in = str;
		if (prettyPrint != null && prettyPrint) {
			in = prettyFormat(str, 4);
		}
		return convertTextToPDF(in);
	}

	/**
	 * Permette di iddentare un xml
	 * 
	 * @param source        contentXML
	 * @param formattazione tipoIddent
	 * @return pdf
	 */
	private static String prettyFormat(final String input, final int indent) {
		try {
			final Source xmlInput = new StreamSource(new StringReader(input));
			final StringWriter stringWriter = new StringWriter();
			final StreamResult xmlOutput = new StreamResult(stringWriter);
			final org.apache.xalan.xsltc.trax.TransformerFactoryImpl transformerFactory = new org.apache.xalan.xsltc.trax.TransformerFactoryImpl();
			transformerFactory.setAttribute("indent-number", indent);
			final Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(xmlInput, xmlOutput);
			return xmlOutput.getWriter().toString();
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		}
	}

	/**
	 * Convert.
	 *
	 * @param mimeType the mime type
	 * @param source   the source
	 * @return the byte[]
	 */
	public static byte[] convert(final String mimeType, final byte[] source) {
		byte[] out = null;
		if (MediaType.JPEG.toString().equalsIgnoreCase(mimeType) || MediaType.BMP.toString().equalsIgnoreCase(mimeType) || MediaType.GIF.toString().equalsIgnoreCase(mimeType)
				|| MediaType.PNG.toString().equalsIgnoreCase(mimeType) || MediaType.TIFF.toString().equalsIgnoreCase(mimeType)) {
			out = convertImgToPDF(source, true);
		} else if (MediaType.HTML_UTF_8.toString().equalsIgnoreCase(mimeType) || MediaType.XHTML_UTF_8.toString().equalsIgnoreCase(mimeType)) {
			out = convertHtmlToPDF(new String(source));
		} else if (MediaType.PLAIN_TEXT_UTF_8.toString().equalsIgnoreCase(mimeType)) {
			out = convertTextToPDF(new String(source));
		} else if (MediaType.XML_UTF_8.toString().equalsIgnoreCase(mimeType)) {
			out = convertXMLToPDF(new String(source), true);
		}
		return out;
	}

	/**
	 * Restituisce il contenuto delle prime 3 pagine del documento PDF in input.
	 *
	 * @param contentDocumento Byte array del documento PDF da cui generare la
	 *                         preview
	 * @return the PDF preview
	 */
	public static InputStream getPDFPreview(final byte[] contentDocumento) {
		ByteArrayInputStream content = null;
		final int maxNumberPage = 3;
		final ByteArrayOutputStream bos = new ByteArrayOutputStream();

		try {
			final Document document = new Document();
			final PdfCopy copy = new PdfCopy(document, bos);
			document.open();
			final PdfReader reader = new PdfReader(contentDocumento);
			int n;

			n = reader.getNumberOfPages();
			if (n > maxNumberPage) {
				n = maxNumberPage;
			}

			for (int page = 1; page <= n; page++) {
				copy.addPage(copy.getImportedPage(reader, page));
			}

			copy.freeReader(reader);
			copy.close();
			document.close();
			bos.flush();

			final byte[] out = bos.toByteArray();
			bos.close();
			reader.close();

			content = new ByteArrayInputStream(out);
		} catch (final Exception e) {
			LOGGER.error("getPDFPreview -> Si è verificato un errore durante la generazione della preview del documento PDF.", e);
			throw new RedException(e);
		}

		return content;
	}

	/**
	 * Elimina campi firma.
	 *
	 * @param source the source
	 * @return the byte[]
	 */
	public static byte[] eliminaCampiFirma(final byte[] source) {
		try {
			final PdfReader reader = new PdfReader(source);
			final List<String> signatures = reader.getAcroFields().getBlankSignatureNames();
			for (final String name : signatures) {
				reader.getAcroFields().removeField(name);
			}

			final ByteArrayOutputStream dest = new ByteArrayOutputStream();
			final PdfStamper stamper = new PdfStamper(reader, dest);
			stamper.close();
			reader.close();
			dest.close();
			return dest.toByteArray();
		} catch (final Exception e) {
			throw new RedException(e);
		}
	}

	/**
	 * Inserisci campi firma.
	 *
	 * @param pdf                  the pdf
	 * @param firmatari            the firmatari
	 * @param altezzaFooterAoo     the altezza footer aoo
	 * @param spaziaturaFirmaAoo   the spaziatura firma aoo
	 * @param firmaMultipla        the firma multipla
	 * @param applicaAncheSenzaTag the applica anche senza tag
	 * @return the byte[]
	 */
	public static byte[] inserisciCampiFirma(final byte[] pdf, final List<Integer> firmatari, final Integer altezzaFooterAoo, final Integer spaziaturaFirmaAoo,
			final boolean firmaMultipla, final boolean applicaAncheSenzaTag) {
		byte[] pdfConCampiFirma = pdf;

		if (CollectionUtils.isNotEmpty(firmatari)) {
			for (int numFirmatario = 1; numFirmatario <= firmatari.size(); numFirmatario++) {
				pdfConCampiFirma = inserisciCampoFirma(pdfConCampiFirma, numFirmatario, altezzaFooterAoo, spaziaturaFirmaAoo, firmaMultipla, applicaAncheSenzaTag);
			}
		}

		return pdfConCampiFirma;
	}

	/**
	 * Inserisci campi firma.
	 *
	 * @param pdf                  the pdf
	 * @param firmatari            the firmatari
	 * @param altezzaFooterAoo     the altezza footer aoo
	 * @param spaziaturaFirmaAoo   the spaziatura firma aoo
	 * @param firmaMultipla        the firma multipla
	 * @param applicaAncheSenzaTag the applica anche senza tag
	 * @return the byte[]
	 */
	private static byte[] inserisciCampoFirma(final byte[] inPdf, final int inNumFirmatario, final Integer inAltezzaFooterAoo, final Integer inSpaziaturaFirmaAoo,
			final boolean firmaMultipla, final boolean applicaAncheSenzaTag) {
		byte[] output = inPdf;

		try {
			if (inPdf != null && inPdf.length > 0 && !firmaMultipla) { // Se è Firma Multipla, NON si inserisce mai il campo firma
				final DataManager dm = new DataManager(inPdf, true);
				final PdfReader reader = dm.getReader();
				final boolean firmato = !CollectionUtils.isEmpty(reader.getAcroFields().getSignatureNames());
				final PdfReaderContentParser parser = new PdfReaderContentParser(reader);
				final int pagineTot = reader.getNumberOfPages();

				Rectangle posizioneBaseCampoFirma = null; // Posizione "base" da cui partire per calcolare il posizionamento del campo
															// firma
				ChiaviCampoFirma chiaveCampoFirmaTrovata = null;
				int paginaStringaPosizioneCampoFirma = -1;
				// Ricerca per chiave su tutte le pagine del documento
				for (final ChiaviCampoFirma chiaveCampoFirma : ChiaviCampoFirma.values()) {

					for (int paginaCorrente = pagineTot; paginaCorrente >= 1; paginaCorrente--) {

						final float pageHeight = reader.getPageSizeWithRotation(paginaCorrente).getHeight();

						posizioneBaseCampoFirma = parser
								.processContent(paginaCorrente,
										new SearchTextExtractionStrategy(chiaveCampoFirma.getKey(), pageHeight, chiaveCampoFirma.isOnlyBottom(), paginaCorrente == pagineTot))
								.getTextPosition();

						if (posizioneBaseCampoFirma != null) {
							chiaveCampoFirmaTrovata = chiaveCampoFirma;
							paginaStringaPosizioneCampoFirma = paginaCorrente;
							break;
						}

					}

					if (posizioneBaseCampoFirma != null) {
						break;
					}

				}

				// ### LOGICA INSERIMENTO CAMPO FIRMA -> START
				// CASO 1) Nel testo sono presenti le stringhe indicatrici per la posizione del
				// campo firma ("Firmatario1", "Firmatario2",
				// "Il Dirigente", "Il Ragioniere Generale dello Stato", etc.)
				// ==> si inserisce un campo firma in quel punto.
				if (posizioneBaseCampoFirma != null) {
					// Indica se il campo firma deve coprire la scritta di posizionamento, oppure
					// posizionarsi al di sotto di essa.
					final boolean stringaPosizioneCampoFirmaDaCoprire = chiaveCampoFirmaTrovata.isCover();

					float offestXDaStringa = 0;
					float offsetYDaStringa = 0;

					// N.B. Dimensione campo firma: 142 x 57
					// In questo caso il campo firma deve coprire la stringa di posizionamento...
					if (stringaPosizioneCampoFirmaDaCoprire) {
						offsetYDaStringa = posizioneBaseCampoFirma.getHeight();
						// ...altrimenti, deve essere posizionato al di sotto di essa.
					} else {
						// Si centra rispetto alla stringa di posizionamento soprastante
						offestXDaStringa = ((posizioneBaseCampoFirma.getRight() - posizioneBaseCampoFirma.getLeft()) - LARGHEZZA_BASE_CAMPO_FIRMA) / 2;
						offsetYDaStringa = -1;
					}

					inserisciCampoFirma(dm, inNumFirmatario, paginaStringaPosizioneCampoFirma, posizioneBaseCampoFirma.getLeft(), posizioneBaseCampoFirma.getBottom(),
							offestXDaStringa, offsetYDaStringa);

					// CASO 2) C'è almeno un campo firma FIRMATO:
					// ==> NON si inserisce il campo firma e al momento della firma ne sarà eseguita
					// una INVISIBILE
					// CASO 3) Altrimenti:
					// ==> Si crea un campo firma posizionato: in basso a destra sotto il testo
					// nell'ultima pagina se c'è spazio sufficiente, altrimenti in una nuova pagina
				} else if (!firmato && applicaAncheSenzaTag) {
					int pagCorr = pagineTot;
					final Rectangle dimensioniPagina = reader.getPageSizeWithRotation(pagineTot);
					final float pageHeight = reader.getPageSizeWithRotation(pagineTot).getHeight();
					int spaziaturaFirmaAoo = 0;
					if (inSpaziaturaFirmaAoo != null) {
						spaziaturaFirmaAoo = inSpaziaturaFirmaAoo;
					}

					// Nel caso avessi già una pagina bianca salgo su
					while (posizioneBaseCampoFirma == null && pagCorr > 0) {
						// Si estrae la posizione dell'ultimo blocco di testo del PDF
						posizioneBaseCampoFirma = parser.processContent(pagCorr, new SearchTextExtractionStrategy(null, pageHeight, false, false)).getTextPosition();
						if (posizioneBaseCampoFirma == null) {
							pagCorr--;
						}
					}

					if (posizioneBaseCampoFirma != null) {
						float baseX = 0;
						float baseY = 0;
						float offsetX = 0;
						float offsetY = 0;
						int altezzaFooterAoo = 0;
						if (inAltezzaFooterAoo != null) {
							altezzaFooterAoo = inAltezzaFooterAoo;
						}

						// ALTEZZA TESTO = ALTEZZA PAGINA - COORDINATA Y DELL'ULTIMO BLOCCO TESTO
						// SPAZIATURA FIRMA = distanza dal testo soprastante inclusa l'altezza del campo
						// firma
						// Se (ALTEZZA TESTO + SPAZIATURA FIRMA + ALTEZZA FOOTER) > ALTEZZA PAGINA,
						// allora il campo firma deve essere inserito in una nuova pagina.
						if (((dimensioniPagina.getHeight() - posizioneBaseCampoFirma.getBottom()) + spaziaturaFirmaAoo + altezzaFooterAoo) > dimensioniPagina.getHeight()) {

							if (pagCorr == pagineTot) {
								// Si inserisce una nuova pagina
								dm.getStamper().insertPage(pagineTot + 1, dimensioniPagina);
								// Si inserisce il campo firma nella nuova pagina
								pagCorr = reader.getNumberOfPages();
							} else {
								// Ho già una pagina bianca e la sfrutto
								pagCorr = pagineTot;
							}

							baseX = dimensioniPagina.getRight();
							baseY = dimensioniPagina.getTop();
							offsetX = -200;
							offsetY = -15;
							// Altrimenti, si inserisce in basso a destra sotto il testo
						} else {
							baseX = dimensioniPagina.getRight() - LARGHEZZA_BASE_CAMPO_FIRMA;
							baseY = posizioneBaseCampoFirma.getBottom();
							offsetX = -56;
							offsetY = (float) ALTEZZA_BASE_CAMPO_FIRMA - spaziaturaFirmaAoo; // N.B. Spaziatura Firma: distanza dal testo soprastante inclusa l'altezza del
																								// campo firma
						}

						inserisciCampoFirma(dm, inNumFirmatario, pagCorr, baseX, baseY, offsetX, offsetY);
					}
				}
				// ### LOGICA INSERIMENTO CAMPO FIRMA -> END

				output = dm.close();
			}
		} catch (final Exception e) {
			LOGGER.error(e);
		}

		return output;
	}

	/**
	 * Ritorna i placeholder degli allegati dato un pdf in input e i placeholder dei
	 * rispettivi utenti
	 *
	 * @param inPdf                the in pdf
	 * @param placeholderDaCercare the placeholder da cercare
	 * @return the placeholder from allegati
	 */
	public static List<String> getPlaceholderFromAllegati(final byte[] inPdf, final Map<String, byte[]> placeholderDaCercare) {
		DataManager dm = null;
		final List<String> placeholderTrovatiList = new ArrayList<>();
		try {
			if (inPdf != null && inPdf.length > 0) {
				dm = new DataManager(inPdf, true);
				final PdfReader reader = dm.getReader();
				final int pagineTot = reader.getNumberOfPages();
				for (int paginaCorrente = 1; paginaCorrente <= pagineTot; paginaCorrente++) {
					final String textFromPage = PdfTextExtractor.getTextFromPage(reader, paginaCorrente);
					for (final String placeholder : placeholderDaCercare.keySet()) {
						if (placeholderTrovatiList.contains(placeholder)) {
							continue;
						}
						final Pattern p = Pattern.compile(placeholder.replace("[", "\\[").replace("]", "\\]"));
						final Matcher m = p.matcher(textFromPage);
						if (m.find()) {
							placeholderTrovatiList.add(placeholder); // Aggiungo l'elemento nella nuova lista da salvare su filenet
						}
					}
				}
			}
		} catch (final Exception ex) {
			LOGGER.error(ex);
		}
		return placeholderTrovatiList;
	}

	/**
	 * Inserisci campo sigla.
	 *
	 * @param inPdf            the in pdf
	 * @param placeholderGlifo the placeholder glifo
	 * @param mostraGlifo      the mostra glifo
	 * @return the byte[]
	 */
	public static byte[] inserisciCampoSigla(final byte[] inPdf, final Map<String, byte[]> placeholderGlifo, final boolean mostraGlifo) {
		return inserisciCampoSigla(inPdf, placeholderGlifo, mostraGlifo, null);
	}

	/**
	 * Inserisci campo sigla.
	 *
	 * @param inPdf            the in pdf
	 * @param placeholderGlifo the placeholder glifo
	 * @param mostraGlifo      the mostra glifo
	 * @param listSegnoGrafico the list segno grafico
	 * @return the byte[]
	 */
	public static byte[] inserisciCampoSigla(byte[] inPdf, final Map<String, byte[]> placeholderGlifo, final boolean mostraGlifo,
			final List<StampigliaturaSegnoGraficoDTO> listSegnoGrafico) {
		byte[] output = inPdf;
		DataManager dm = null;
		boolean apponiSiglaContinue = false;
		try {
			if (inPdf != null && inPdf.length > 0) {
				dm = new DataManager(inPdf, true);
				PdfReader reader = dm.getReader();
				PdfReaderContentParser parser = new PdfReaderContentParser(reader);
				final int pagineTot = reader.getNumberOfPages();

				for (int paginaCorrente = 1; paginaCorrente <= pagineTot; paginaCorrente++) {
					if (apponiSiglaContinue) {
						dm = new DataManager(output,true);
						reader = dm.getReader();
						parser = new PdfReaderContentParser(reader);
						apponiSiglaContinue = false;
					}
					Rectangle posizioneBaseCampoSigla = null;  
					final HashMap<Rectangle,String> positionPlaceholder = new HashMap<>();
					
					final float pageHeight = reader.getPageSizeWithRotation(paginaCorrente).getHeight();
					
					// Conto le occorrenze dei placeholder
					final String textFromPage = PdfTextExtractor.getTextFromPage(reader, paginaCorrente);
					for (final String stringaPosizioneCampoSigla : placeholderGlifo.keySet()) {
						int contPlaceholder = 0; 
						int z = 0;
						final Pattern p = Pattern.compile(stringaPosizioneCampoSigla.replace("[", "\\[").replace("]", "\\]"));
						final Matcher m = p.matcher(textFromPage);
						while (m.find()) {
						    z++;
						} 
						for (int i=0; i<z; i++) { 
							posizioneBaseCampoSigla = parser.processContent(paginaCorrente, 
									new SearchTextExtractionStrategy(stringaPosizioneCampoSigla, pageHeight, contPlaceholder, false, false)).getTextPosition();
							if (posizioneBaseCampoSigla != null) {
								positionPlaceholder.put(posizioneBaseCampoSigla, stringaPosizioneCampoSigla);
							} else {
								break;
							}
							contPlaceholder++;
						}
					}

					for (final Rectangle posizioneBaseCampo : positionPlaceholder.keySet()) {
						if (apponiSiglaContinue) {
							dm = new DataManager(output, true);
						}

						if (posizioneBaseCampo != null) {
							BaseColor color = BaseColor.YELLOW;
							if (listSegnoGrafico != null) {
								color = getColorRectangleForPreview(listSegnoGrafico, positionPlaceholder, posizioneBaseCampo, color);
							}
							inserisciSegnoGrafico(dm, paginaCorrente, posizioneBaseCampo, placeholderGlifo.get(positionPlaceholder.get(posizioneBaseCampo)), mostraGlifo, color);
							inPdf = dm.close();
							apponiSiglaContinue = true;
						}
						output = inPdf;
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
		}

		return output;
	}
	
	/**
	 * Inserisci postilla.
	 *
	 * @param inPdf            the in pdf
	 * @param postilla the placeholder glifo
	 * @param soloPrimaPagina      the mostra glifo
	 * @return the byte[]
	 */
	public static byte[] inserisciPostilla(final byte[] inPdf, final String postilla, final boolean confPDFA, final boolean soloPrimaPagina) {
		byte[] output = inPdf;
		final Integer dimensioneCarattere = 9;
		DataManager dm = null;
		try {
			if (inPdf != null && inPdf.length > 0) {
				dm = new DataManager(inPdf, true);
				final PdfReader reader = dm.getReader();
				final int pagineTot = reader.getNumberOfPages();
				
				for (int paginaCorrente = 1; ((paginaCorrente <= pagineTot) && ((soloPrimaPagina && paginaCorrente==1)||(!soloPrimaPagina))); paginaCorrente++) {
				
					final PdfContentByte canvas = dm.getStamper().getOverContent(paginaCorrente);
					final float pageHeight = reader.getPageSizeWithRotation(paginaCorrente).getHeight();

					Font f = null;
					if(confPDFA) {
						f  = new Font(Font.FontFamily.HELVETICA, dimensioneCarattere, Font.BOLD, BaseColor.BLACK);
					} else {
						f  = new Font(Font.FontFamily.TIMES_ROMAN, dimensioneCarattere, Font.BOLD, BaseColor.BLACK);
					}
			
					ColumnText.showTextAligned(canvas, Element.ALIGN_CENTER, new Phrase(postilla,f) , 15, pageHeight/2, 90);
			
				}
				
				output = dm.close();
			}
			
		} catch (final Exception e) {
			
			LOGGER.error("Errore in fase di inserimento della postilla.", e);
		
		} finally {
			if(dm != null){
				dm.close();
			}
		}

		return output;
	}


	/**
	 * Otteniamo i colori dei rettangoli da visualizzare in preview in
	 * corrispondenza di quelli configurati a db
	 *
	 * @param listSegnoGrafico
	 * @param positionPlaceholder
	 * @param posizioneBaseCampo
	 * @param color
	 * @return color
	 */
	private static BaseColor getColorRectangleForPreview(final List<StampigliaturaSegnoGraficoDTO> listSegnoGrafico, final HashMap<Rectangle, String> positionPlaceholder,
			final Rectangle posizioneBaseCampo, BaseColor color) {
		for (final StampigliaturaSegnoGraficoDTO segnoGrafico : listSegnoGrafico) {
			if (segnoGrafico.getRed() != null && segnoGrafico.getGreen() != null && segnoGrafico.getBlue() != null && segnoGrafico.getPlaceholderUtente() != null) {

				for (final String placeHolderKey : segnoGrafico.getPlaceholderUtente().keySet()) {
					if (placeHolderKey.equals(positionPlaceholder.get(posizioneBaseCampo))) {
						color = new BaseColor(segnoGrafico.getRed(), segnoGrafico.getGreen(), segnoGrafico.getBlue());
					}
				}
			}
		}
		return color;
	}

	/**
	 * @param dm
	 * @param nomeCampo
	 * @param lowerLeftX
	 * @param lowerLeftY
	 * @param upperRightX
	 * @param upperRightY
	 * @return
	 */
	private static PdfFormField creaCampoFirma(final DataManager dm, final int idCampoFirma, final float lowerLeftX, final float lowerLeftY, final float upperRightX,
			final float upperRightY) {
		final PdfFormField campoFirma = PdfFormField.createSignature(dm.getWriter());

		campoFirma.setWidget(new Rectangle(lowerLeftX, // Lower Left X
				lowerLeftY, // Lower Left Y
				upperRightX, // Upper Right X
				upperRightY), // Upper Right Y
				null);
		campoFirma.setFlags(PdfAnnotation.FLAGS_PRINT);
		campoFirma.put(PdfName.DA, new PdfString("/Helv 0 Tf 0 g"));
		campoFirma.setFieldName("campofirma_" + idCampoFirma);

		return campoFirma;
	}

	private static void inserisciSegnoGrafico(final DataManager dm, final int pagina, final Rectangle rect, final byte[] placeholdeGlifo, final boolean mostraGlifo,
			final BaseColor color) throws IOException, DocumentException {

		final float lowerLeftX = rect.getLeft() + rect.getWidth() / 2 - 25;
		final float lowerLeftY = rect.getTop() - 50;
		final float upperRightX = rect.getLeft() + rect.getWidth() / 2 + 25;
		final float upperRightY = rect.getTop();

		final Rectangle rectSegnoGrafico = new Rectangle(lowerLeftX, lowerLeftY, upperRightX, upperRightY);

		if (mostraGlifo) {
			creaSegnoGrafico(dm, placeholdeGlifo, rectSegnoGrafico, pagina);
		} else {
			creaRettangoloSegnoGrafico(dm, rectSegnoGrafico, pagina, color);
		}
	}

	private static void creaRettangoloSegnoGrafico(final DataManager dm, final Rectangle rect, final int pagina, final BaseColor color) {
		final PdfContentByte canvas = dm.getStamper().getOverContent(pagina);
		rect.setBorder(Rectangle.BOX);
		rect.setBorderWidth(2);
		rect.setBackgroundColor(color);
		canvas.rectangle(rect);
		canvas.fillStroke();

	}

	private static void creaSegnoGrafico(final DataManager dm, final byte[] placeholdeGlifo, final Rectangle rect, final int pagina) throws IOException, DocumentException {
		final PdfContentByte canvas = dm.getStamper().getOverContent(pagina);
		final Image image = Image.getInstance(placeholdeGlifo);
		image.scaleAbsolute(50, 50);
		image.setAbsolutePosition((rect.getLeft() + rect.getRight() - 30) / 2, (rect.getBottom() + rect.getTop() - 25) / 2);
		canvas.addImage(image);
		canvas.fillStroke();
	}

	private static void inserisciCampoFirma(final DataManager dm, final int idCampoFirma, final int pagina, final float baseX, final float baseY, final float offsetX,
			final float offsetY) {
		final float lowerLeftX = baseX + offsetX;
		final float lowerLeftY = baseY + offsetY - ALTEZZA_BASE_CAMPO_FIRMA;
		final float upperRightX = baseX + offsetX + LARGHEZZA_BASE_CAMPO_FIRMA;
		final float upperRightY = baseY + offsetY;

		final PdfFormField campoFirma = creaCampoFirma(dm, idCampoFirma, lowerLeftX, lowerLeftY, upperRightX, upperRightY);

		dm.getStamper().addAnnotation(campoFirma, pagina);
	}

	/**
	 * Restituisce la stringa identificativa del documento contenente anche i dati
	 * di protocollo.
	 *
	 * @param isPrincipale           the is principale
	 * @param idCategoriaDocumento   Categoria del documento (entrata/uscita)
	 * @param annoProtocollo         Anno di protocollo
	 * @param numeroProtocollo       Numero del protocollo o, in assenza di
	 *                               protocollo, ID del documento
	 * @param dataProtocollo         Data del protocollo
	 * @param flagFirmaCopiaConforme the flag firma copia conforme
	 * @param codiceAoo              the codice aoo
	 * @param descrizioneAoo         the descrizione aoo
	 * @param idTipoProtocollo       the id tipo protocollo
	 * @param isRegistroRepertorio   the is registro repertorio
	 * @param timbroUscitaAoo        the timbro uscita aoo
	 * @param labelStampigliaturaRR  the label stampigliatura RR
	 * @return the stampigliatura protocollo
	 */
	public static String getStampigliaturaProtocollo(final boolean isPrincipale, final Integer idCategoriaDocumento, final Integer annoProtocollo,
			final Integer numeroProtocollo, final String dataProtocollo, final boolean flagFirmaCopiaConforme, final String codiceAoo,
			final String descrizioneAoo, final Long idTipoProtocollo, Boolean isRegistroRepertorio, final boolean timbroUscitaAoo, 
			final String labelStampigliaturaRR) {
		
		if (numeroProtocollo != null && numeroProtocollo > 0 && !flagFirmaCopiaConforme) {

			if (isPrincipale) {

				if (annoProtocollo != null && annoProtocollo > 0 && dataProtocollo != null) {

					String descTipoDocumentoPrincipale = Constants.EMPTY_STRING;
					final String tipoDocumento = getTipoDocumento(idCategoriaDocumento);
					if (!StringUtils.isNullOrEmpty(tipoDocumento)) {
						descTipoDocumentoPrincipale = " - " + tipoDocumento;
					}
					String prot = "Prot.";
					if (TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(idTipoProtocollo)) {
						prot = prot + " " + Constants.Protocollo.PROTOCOLLO_EMERGENZA_ACRONIMO;
					} else if (Boolean.TRUE.equals(isRegistroRepertorio)) {
						prot = Constants.Protocollo.PROTOCOLLO_REPERTORIO_ACRONIMO;
						descTipoDocumentoPrincipale = Constants.EMPTY_STRING;
					}
					if (labelStampigliaturaRR == null || labelStampigliaturaRR.isEmpty()) {
						return MEF_LABEL + codiceAoo + " - " + prot + " " + numeroProtocollo + DEL + dataProtocollo + descTipoDocumentoPrincipale;
					} else {
						return MEF_LABEL + codiceAoo + " - " + prot + "_" + labelStampigliaturaRR + " " + numeroProtocollo + DEL + dataProtocollo
								+ descTipoDocumentoPrincipale;
					}

				} else {

					return MEF_LABEL + codiceAoo + " - Id: " + numeroProtocollo + DEL + DateUtils.dateToString(new Date(), true);

				}
			} else {

				if (DocumentoUtils.isCategoriaDocumentoEntrata(idCategoriaDocumento) || (DocumentoUtils.isCategoriaDocumentoUscita(idCategoriaDocumento) && timbroUscitaAoo)) {

					return "Documento allegato al protocollo " + numeroProtocollo + DEL + annoProtocollo + " " + descrizioneAoo;

				} else {
					return Constants.EMPTY_STRING;
				}

			}

		}

		return Constants.EMPTY_STRING;
	}

	/**
	 * Restituisce il simbolo del tipo documento dato la categoria.
	 * 
	 * @param idCategoria - Identificativo della categoria.
	 * @return Simbolo del tipo documento.
	 */
	private static String getTipoDocumento(final Integer idCategoria) {
		String output = "";

		if (CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0].equals(idCategoria)) {
			output = "E";
		}
		if (CategoriaDocumentoEnum.DOCUMENTO_USCITA.getIds()[0].equals(idCategoria) || CategoriaDocumentoEnum.MOZIONE.getIds()[0].equals(idCategoria)) {
			output = "U";
		}

		return output;
	}

	/**
	 * Convert to png PDF and PD fto png.
	 *
	 * @param pdfContent the pdf content
	 * @return the byte[]
	 */
	public static byte[] convertToPngPDFAndPDFtoPng(final byte[] pdfContent) {
		byte[] output = null;
		try {
			final HashMap<Integer, PrintDocumentDTO> pngContentList = pdfToImg(pdfContent);
			output = pngToPDF(pngContentList);

		} catch (final Exception ex) {
			LOGGER.error(ERROR_CONVERSIONE_DOCUMENTO_MSG + ex);
			throw new RedException(ERROR_CONVERSIONE_DOCUMENTO_MSG + ex);
		}
		return output;
	}

	private static HashMap<Integer, PrintDocumentDTO> pdfToImg(final byte[] pdfContent) {
		
		HashMap<Integer, PrintDocumentDTO> pngConvertedContentList = null;
		try (
			InputStream isContent = new ByteArrayInputStream(pdfContent);
			PDDocument document = PDDocument.load(isContent);
		) {
			pngConvertedContentList = new HashMap<>();
			final int page = document.getNumberOfPages();
			final PDFRenderer pdfRender = new PDFRenderer(document);
			for (int i = 0; i < page; i++) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				final PrintDocumentDTO doc = new PrintDocumentDTO();
				final float llx = document.getPage(i).getCropBox().getLowerLeftX();
				final float lly = document.getPage(i).getCropBox().getLowerLeftY();
				final float urx = document.getPage(i).getCropBox().getUpperRightX();
				final float ury = document.getPage(i).getCropBox().getUpperRightY();

				doc.setPosition(new Rectangle(llx, lly, urx, ury));
				final BufferedImage image = pdfRender.renderImageWithDPI(i, 300);
				ImageIO.write(image, "PNG", baos);

				doc.setContent(baos.toByteArray());
				pngConvertedContentList.put(i, doc);
			}

		} catch (final IOException e) {
			LOGGER.error(ERROR_CONVERSIONE_DOCUMENTO_MSG + e);
			throw new RedException(ERROR_CONVERSIONE_DOCUMENTO_MSG + e);
		}
		return pngConvertedContentList;
	}

	private static byte[] pngToPDF(final HashMap<Integer, PrintDocumentDTO> pngConvertedContentList) {
		byte[] output = null;
		try {
			final Document document = new Document();
			final ByteArrayOutputStream stream = new ByteArrayOutputStream();
			PdfWriter.getInstance(document, stream);
			document.open();
			for (int i = 0; i < pngConvertedContentList.size(); i++) {
				final Image img = Image.getInstance(pngConvertedContentList.get(i).getContent());
				document.setPageSize(pngConvertedContentList.get(i).getPosition());
				img.scaleToFit(pngConvertedContentList.get(i).getPosition());
				img.setAbsolutePosition(0, 0);
				document.newPage();
				document.add(img);

			}
			document.close();
			output = stream.toByteArray();
			stream.close();
		} catch (final Exception ex) {
			LOGGER.error("Errore durante la conversione del documento da png a pdf :" + ex);
			throw new RedException("Errore durante la conversione del documento da png a pdf :" + ex);
		}
		return output;
	}
	
	/**
	 * Aggiunge footer al pdf.
	 * @param content
	 * @param footerString
	 * @return byte array del pdf modificato
	 */
	public static byte[] addFooterToPDF(final byte[] content, final String footerString) {
		
		final Phrase phrase = new Phrase(footerString, FontFactory.getFont(FontFactory.COURIER, 8.0f));
		
		final PdfPTable footer = new PdfPTable(1);
        footer.setTotalWidth(500);
		footer.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		footer.getDefaultCell().disableBorderSide(Rectangle.BOTTOM);
		footer.getDefaultCell().disableBorderSide(Rectangle.TOP);
		footer.getDefaultCell().disableBorderSide(Rectangle.LEFT);
		footer.getDefaultCell().disableBorderSide(Rectangle.RIGHT);
		footer.addCell(phrase);
		
		final DataManager dm = new DataManager(content, true);
		
		int numOfPages = dm.getReader().getNumberOfPages();
		
		for(int i=1; i<=numOfPages; i++) {
			final PdfContentByte underContent = dm.getStamper().getUnderContent(i);
			com.itextpdf.text.Document document = underContent.getPdfDocument();
			footer.writeSelectedRows(0, -1, document.left(), document.bottom() - 10, underContent);
		}
		
		return dm.close();
	}

}
