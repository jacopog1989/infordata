package it.ibm.red.business.dto;

import java.util.Date;

import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoStrutturaNodoEnum;

/**
 * The Class AssegnazioneDTO.
 *
 * @author CPIERASC
 * 
 *         DTO assegnazione.
 */
public class AssegnazioneDTO extends AbstractDTO implements Comparable<AssegnazioneDTO> {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 5899907999995977843L;

	/**
	 * Motivo assegnazione.
	 */
	private final String motivoAssegnazione;

	/**
	 * The tipo assegnazione.
	 */
	private TipoAssegnazioneEnum tipoAssegnazione;

	/**
	 * Identifica se l'assegnazione si riferisce a un ufficio, ispettorato o utente.
	 * Non sempre è utilizzato
	 */
	private final TipoStrutturaNodoEnum tipoStrutturaNodo;

	/**
	 * Rappresentazione grafica della data di assegnazione.
	 */
	private final String dataAssegnazione;
	/**
	 * La data di assegnazione.
	 */
	private final Date dataAssegnazioneDate;

	/**
	 * The descrizione assegnatario.
	 */
	private String descrizioneAssegnatario;

	/**
	 * The utente.
	 */
	private UtenteDTO utente;

	/**
	 * 
	 */
	private UfficioDTO ufficio;

	/**
	 * WobNumber del workflow a cui si riferisce l'assegnazione.
	 */
	private String wobNumber;

	/**
	 * Costruttore.
	 * 
	 * @param inMotivoAssegnazione
	 *            motivo assegnazione
	 * @param inTipoAssegnazione
	 *            tipo assegnazione
	 * @param inDataAssegnazione
	 *            data assegnazione
	 * @param inDescrizioneAssegnatario
	 *            descrizione assegnatario
	 * @param inUtente
	 *            utente
	 */
	public AssegnazioneDTO(final String inMotivoAssegnazione, final TipoAssegnazioneEnum inTipoAssegnazione, final String inDataAssegnazione, final Date date,
			final String inDescrizioneAssegnatario, final UtenteDTO inUtente, final UfficioDTO ufficio) {
		this(inMotivoAssegnazione, inTipoAssegnazione, null, inDataAssegnazione, date, inDescrizioneAssegnatario, inUtente, ufficio);
	}

	/**
	 * Costruttore con il tipoStrutturaNodo. L'oggetto assegnazione serve a
	 * disaccoppiare dall'interfaccia l'oggetto NodoOrganigrammaDTO
	 * 
	 * @see it.ibm.red.web.mbean.humantask.MultiAssegnazioneBean
	 * 
	 * @param inMotivoAssegnazione
	 * @param inTipoAssegnazione
	 * @param inTipoStrutturaNodo
	 * @param inDataAssegnazione
	 * @param inDescrizioneAssegnatario
	 * @param inUtente
	 * @param ufficio
	 */
	public AssegnazioneDTO(final String inMotivoAssegnazione, final TipoAssegnazioneEnum inTipoAssegnazione, final TipoStrutturaNodoEnum inTipoStrutturaNodo,
			final String inDataAssegnazione, final Date date, final String inDescrizioneAssegnatario, final UtenteDTO inUtente, final UfficioDTO ufficio) {
		super();
		this.motivoAssegnazione = inMotivoAssegnazione;
		this.tipoAssegnazione = inTipoAssegnazione;
		this.tipoStrutturaNodo = inTipoStrutturaNodo;
		this.dataAssegnazione = inDataAssegnazione;
		this.dataAssegnazioneDate = date;
		this.descrizioneAssegnatario = inDescrizioneAssegnatario;
		this.utente = inUtente;
		this.ufficio = ufficio;
	}

	/**
	 * Getter motivo assegnazione.
	 * 
	 * @return motivo assegnaizone
	 */
	public final String getMotivoAssegnazione() {
		return motivoAssegnazione;
	}

	/**
	 * Getter tipo assegnazione.
	 * 
	 * @return tipo assegnaizone
	 */
	public final TipoAssegnazioneEnum getTipoAssegnazione() {
		return tipoAssegnazione;
	}

	/**
	 * Setter tipo assegnazione.
	 * 
	 * @param tipoAssegnazione
	 */
	public void setTipoAssegnazione(TipoAssegnazioneEnum tipoAssegnazione) {
		this.tipoAssegnazione = tipoAssegnazione;
	}
	
	/**
	 * Restituisce il tipo della struttura del nodo.
	 * 
	 * @return tipoStrutturaNodo
	 */
	public TipoStrutturaNodoEnum getTipoStrutturaNodo() {
		return tipoStrutturaNodo;
	}

	/**
	 * Getter data assegnazione.
	 * 
	 * @return data assegnazione
	 */
	public final String getDataAssegnazione() {
		return dataAssegnazione;
	}

	/**
	 * Getter descrizione assegnatario.
	 * 
	 * @return descrizione assegnatario
	 */
	public final String getDescrizioneAssegnatario() {
		return descrizioneAssegnatario;
	}

	/**
	 * Getter utente.
	 * 
	 * @return utente
	 */
	public final UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'utente con l'oggetto UtenteDTO.
	 * 
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Restituisce l'ufficio associato all'assegnazione.
	 * 
	 * @return UfficioDTO relativo all'ufficio
	 */
	public UfficioDTO getUfficio() {
		return ufficio;
	}

	/**
	 * Imposta l'ufficio come UfficioDTO.
	 * 
	 * @param ufficio
	 */
	public void setUfficio(final UfficioDTO ufficio) {
		this.ufficio = ufficio;
	}

	/**
	 * Imposta la descrizione dell'assegnatario.
	 * 
	 * @param descrizioneAssegnatario
	 */
	public void setDescrizioneAssegnatario(final String descrizioneAssegnatario) {
		this.descrizioneAssegnatario = descrizioneAssegnatario;
	}

	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descrizioneAssegnatario == null) ? 0 : descrizioneAssegnatario.hashCode());
		result = prime * result + ((ufficio == null || ufficio.getId() == null) ? 0 : ufficio.getId().hashCode());
		result = prime * result + ((utente == null || utente.getId() == null) ? 0 : utente.getId().hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final AssegnazioneDTO other = (AssegnazioneDTO) obj;
		if (descrizioneAssegnatario == null) {
			if (other.descrizioneAssegnatario != null) {
				return false;
			}
		} else if (!descrizioneAssegnatario.equals(other.descrizioneAssegnatario)) {
			return false;
		}
		if (ufficio == null) {
			if (other.ufficio != null) {
				return false;
			}
		} else if (ufficio.getId() == null) {
			if (other.ufficio.getId() != null) {
				return false;
			}
		} else if (!ufficio.getId().equals(other.ufficio.getId())) {
			return false;
		}
		if (utente == null) {
			if (other.utente != null) {
				return false;
			}
		} else if (utente.getId() == null) {
			if (other.utente.getId() != null) {
				return false;
			}
		} else if (!utente.getId().equals(other.utente.getId())) {
			return false;
		}
		return true;
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object).
	 */
	@Override
	public int compareTo(final AssegnazioneDTO o) {
		if (equals(o)) {
			return 0;
		} else if (dataAssegnazioneDate == null || o.dataAssegnazioneDate == null) {
			return -1;
		} else {
			return o.dataAssegnazioneDate.compareTo(dataAssegnazioneDate);
		}
	}

	/**
	 * Restituisce il wob number.
	 * 
	 * @return wobNumber
	 */
	public String getWobNumber() {
		return wobNumber;
	}

	/**
	 * Imposta il wob number.
	 * 
	 * @param wobNumber
	 */
	public void setWobNumber(final String wobNumber) {
		this.wobNumber = wobNumber;
	}

}
