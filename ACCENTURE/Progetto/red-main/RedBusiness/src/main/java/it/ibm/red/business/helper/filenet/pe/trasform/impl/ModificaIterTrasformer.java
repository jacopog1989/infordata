package it.ibm.red.business.helper.filenet.pe.trasform.impl;

import java.util.Arrays;
import java.util.List;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.ModificaIterPEDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * The Class ModificaIterTrasformer.
 *
 * @author CPIERASC
 * 
 *         Oggetto per trasformare un workflow in un documento.
 */
public class ModificaIterTrasformer extends TrasformerPE<ModificaIterPEDTO> {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1705454340138423161L;

	/**
	 * Costruttore.
	 */
	public ModificaIterTrasformer() {
		super(TrasformerPEEnum.MODIFICA_ITER);
	}

	/**
	 * Trasform.
	 *
	 * @param object the object
	 * @return the modifica iter PEDTO
	 */
	@Override
	public final ModificaIterPEDTO trasform(final VWWorkObject object) {
		final Integer idDocumento = (Integer) getMetadato(object, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
		final Integer idFascicolo = (Integer) getMetadato(object, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY));
		final Integer idTipoAssegnazione = (Integer) getMetadato(object, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY));
		final String[] destinatariInterni = (String[]) TrasformerPE.getMetadato(object,
				PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ELENCO_DESTINATARI_INTERNI_METAKEY));
		final String[] contributi = (String[]) TrasformerPE.getMetadato(object,
				PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ELENCO_CONTRIBUTI_STORICO_METAKEY));
		final String[] conoscenze = (String[]) TrasformerPE.getMetadato(object,
				PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ELENCO_CONOSCENZA_STORICO_METAKEY));

		List<String> destinatariInterniList = null;
		if (destinatariInterni != null) {
			destinatariInterniList = Arrays.asList(destinatariInterni);
		}
		List<String> conoscenzeList = null;
		if (conoscenze != null) {
			conoscenzeList = Arrays.asList(conoscenze);
		}
		List<String> contributiList = null;
		if (contributi != null) {
			contributiList = Arrays.asList(contributi);
		}

		return new ModificaIterPEDTO(idDocumento, idFascicolo, destinatariInterniList, conoscenzeList, contributiList, idTipoAssegnazione);
	}
}
