package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.sql.Connection;

import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Servizio cambio iter.
 */
public interface ISecurityCambioIterFacadeSRV extends Serializable {

	/**
	 * Recupero assegnazioni.
	 * 
	 * @param assegnazioneCoordinatore assegnazione coordinatore
	 * @return lista assegnazioni
	 */
	String[] getAssegnazioneUtenteCoordinatore(String assegnazioneCoordinatore);

	/**
	 * Assegnazioni per riassegnazione.
	 * 
	 * @param utente            utente
	 * @param idDocumento       identificativo documento
	 * @param assegnazioniArray array assegnazioni
	 * @param isRiservato       flag riservato
	 * @param con               connessione
	 * @param coordinatore      coordinatore
	 * @return lista security
	 */
	ListSecurityDTO getSecurityPerRiassegnazione(UtenteDTO utente, String idDocumento, String[] assegnazioniArray, boolean isRiservato, Connection con,
			String coordinatore);

	/**
	 * Aggiornamento security contributi.
	 * 
	 * @param idDocumentoPrincipale identificativo documento principale
	 * @param con                   connessione
	 * @param assegnazioni          assegnazioni
	 * @param utente                utente
	 */
	void aggiornaSecurityContributiInseriti(String idDocumentoPrincipale, Connection con, String[] assegnazioni, UtenteDTO utente);

	/**
	 * Aggiornamento security documenti allacciati.
	 * 
	 * @param idDocumentoPrincipale identificativo documento principale
	 * @param securities            securities
	 * @param con                   connessione
	 * @param assegnazioni          assegnazioni
	 * @param utente                utente
	 */
	void aggiornaSecurityDocumentiAllacciati(String idDocumentoPrincipale, ListSecurityDTO securities, Connection con, String[] assegnazioni, UtenteDTO utente);
}