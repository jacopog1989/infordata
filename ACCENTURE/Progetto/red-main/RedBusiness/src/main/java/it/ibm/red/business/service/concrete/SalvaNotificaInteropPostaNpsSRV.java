package it.ibm.red.business.service.concrete;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.constants.Constants.TipologiaInvio;
import it.ibm.red.business.constants.Constants.TipologiaMessaggio;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.dto.NotificaInteropPostaNpsDTO;
import it.ibm.red.business.dto.NotificaInteroperabilitaEmailDTO;
import it.ibm.red.business.dto.SalvaMessaggioPostaNpsContestoDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoMessaggioPostaNpsIngressoEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;

/**
 * Service che gestisce il salvataggio notifica interoperabile.
 */
@Service
@Component
public class SalvaNotificaInteropPostaNpsSRV extends SalvaMessaggioPostaNpsAbstractSRV<NotificaInteropPostaNpsDTO> {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = -4354157535902293591L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SalvaNotificaInteropPostaNpsSRV.class.getName());

	/**
	 * @see it.ibm.red.business.service.concrete.SalvaMessaggioInteropAbstractSRV#
	 *      getMessageId(it.ibm.red.business.dto.SalvaMessaggioInteropContestoDTO).
	 * 
	 */
	@Override
	protected String getMessageId(final SalvaMessaggioPostaNpsContestoDTO<NotificaInteropPostaNpsDTO> contesto) {
		String outMessageId = null;

		if (StringUtils.isNotBlank(contesto.getRichiesta().getMessaggio().getMessageId())) {
			outMessageId = contesto.getRichiesta().getMessaggio().getMessageId();
		} else {
			LOGGER.info("Message-ID non trovato per la notifica di interoperabilità associata alla richiesta di elaborazione: " + contesto.getRichiesta().getIdCoda());
		}

		return outMessageId;
	}

	/**
	 * @see it.ibm.red.business.service.concrete.SalvaMessaggioInteropAbstractSRV#
	 *      getMittente(it.ibm.red.business.dto.SalvaMessaggioInteropContestoDTO).
	 */
	@Override
	protected String getMittente(final SalvaMessaggioPostaNpsContestoDTO<NotificaInteropPostaNpsDTO> contesto) {
		String mittente = null;

		if (contesto.getRichiesta().getMessaggio().getMittente() != null) {
			mittente = contesto.getRichiesta().getMessaggio().getMittente().getMail();
		} else {
			LOGGER.info("Mittente non trovato per la notifica di interoperabilità associata alla richiesta di elaborazione: " + contesto.getRichiesta().getIdCoda());
		}

		return mittente;
	}

	/**
	 * @see it.ibm.red.business.service.concrete.SalvaMessaggioPostaNpsAbstractSRV#
	 *      costruisciMessaggioPostaNpsRedFilenet(java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.String, java.util.List, java.util.Date,
	 *      it.ibm.red.business.dto.MessaggioPostaNpsDTO, java.lang.boolean).
	 */
	@Override
	protected final MessaggioEmailDTO costruisciMessaggioPostaNpsRedFilenet(final String mailDestinatariStringTo, final String mailDestinatariStringCc, final String mittente,
			final String oggetto, final String messageId, final List<AllegatoDTO> allegatiMessaggio, final Date dataRicezione,
			final NotificaInteropPostaNpsDTO messaggioPostaNps) {
		// In questo caso si crea il DTO per la notifica di Interoperabilità
		return new NotificaInteroperabilitaEmailDTO(mailDestinatariStringTo, mailDestinatariStringCc, mittente, oggetto, TipologiaInvio.NUOVOMESSAGGIO,
				TipologiaMessaggio.TIPO_MSG_EMAIL, messageId, !CollectionUtils.isEmpty(allegatiMessaggio), allegatiMessaggio, dataRicezione,
				getFolderDestinazioneMessaggio(messaggioPostaNps.getTipoIngresso()), messaggioPostaNps.getIdMessaggio(), messaggioPostaNps.getProtocolloMittente());
	}

	/**
	 * @see it.ibm.red.business.service.concrete.SalvaMessaggioPostaNpsAbstractSRV#
	 *      getFolderDestinazioneMessaggio(
	 *      it.ibm.red.business.enums.TipoMessaggioPostaNpsIngressoEnum).
	 */
	@Override
	protected String getFolderDestinazioneMessaggio(final TipoMessaggioPostaNpsIngressoEnum tipoIngresso) {
		return getPp().getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_NOTIFICHE_INTEROP_FN_METAKEY);
	}

	/**
	 * @see it.ibm.red.business.service.concrete.SalvaMessaggioInteropAbstractSRV#
	 *      isMessaggioDuplicato(it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 *      java.lang.String, java.lang.String, java.lang.String).
	 */
	@Override
	protected boolean isMessaggioDuplicato(final IFilenetCEHelper fceh, final String messageId, final String mailCasellaPostale) {
		return false;
	}

}