package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IAttoDecretoUCBDAO;
import it.ibm.red.business.dto.AttributiEstesiAttoDecretoDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * DAO atto decreto - UCB.
 */
@Repository
public class AttoDecretoUCBDAO extends AbstractDAO implements IAttoDecretoUCBDAO {

	/**
	 * UID.
	 */
	private static final long serialVersionUID = -4999123047567186478L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AttoDecretoUCBDAO.class.getName());

	/**
	 * @see it.ibm.red.business.dao.IAttoDecretoUCBDAO#getExtension(java.sql.Connection).
	 */
	@Override
	public Collection<String> getExtension(final Connection connection) {
		final Collection<String> out = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement("SELECT ESTENSIONE FROM TIPOFILE WHERE ATTODECRETO <> 0");
			
			// Esecuzione della SELECT
			rs = ps.executeQuery();
			
			while (rs.next()) {
				out.add(rs.getString("ESTENSIONE"));
			}

		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return out;
	}

	/**
	 * @see it.ibm.red.business.dao.IAttoDecretoUCBDAO#getAttributiEstesi(java.sql.Connection,
	 *      java.lang.Integer).
	 */
	@Override
	public AttributiEstesiAttoDecretoDTO getAttributiEstesi(final Connection connection, final Integer idAOO) {
		AttributiEstesiAttoDecretoDTO out = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			
			ps = connection.prepareStatement("SELECT COD_AMMINISTRAZIONE, COD_RAGIONERIA, UFF_SEGRETERIA FROM ATTRIBUTI_ESTESI_ATTO_DECRETO WHERE ID_AOO = ?");
			ps.setInt(1, idAOO);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				out = new AttributiEstesiAttoDecretoDTO();
				out.setCodAmministrazione(rs.getString("COD_AMMINISTRAZIONE"));
				out.setCodRagioneria(rs.getString("COD_RAGIONERIA"));
				out.setUffSegreteria(rs.getString("UFF_SEGRETERIA"));
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei metadati estesi aoo:" + idAOO, e);
			throw new RedException("Errore durante il recupero dei metadati estesi aoo:" + idAOO, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return out;
	}

}
