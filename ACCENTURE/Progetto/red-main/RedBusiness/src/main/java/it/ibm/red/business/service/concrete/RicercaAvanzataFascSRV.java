package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;

import it.ibm.red.business.constants.Constants.Ricerca;
import it.ibm.red.business.dao.IFascicoloDAO;
import it.ibm.red.business.dao.ITipoAssegnazioneDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataFascDTO;
import it.ibm.red.business.dto.RicercaAvanzataFascicoliFormDTO;
import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IRicercaAvanzataDocSRV;
import it.ibm.red.business.service.IRicercaAvanzataFascSRV;
import it.ibm.red.business.service.IRicercaSRV;
import it.ibm.red.business.utils.DateUtils;

/**
 * The Class RicercaAvanzataFascSRV.
 *
 * @author m.crescentini
 * 
 *         Servizio per la gestione della ricerca avanzata dei fascicoli.
 */
@Service
@Component
public class RicercaAvanzataFascSRV extends AbstractService implements IRicercaAvanzataFascSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -7975005494736680746L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaAvanzataFascSRV.class.getName());

	/**
	 * Service.
	 */
	@Autowired
	private IRicercaAvanzataDocSRV ricercaAvanzataDocSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IRicercaSRV ricercaSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IFascicoloDAO fascicoloDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipologiaDocumentoDAO tipologiaDocumentoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipoAssegnazioneDAO tipoAssegnazioneDAO;

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaAvanzataFascFacadeSRV#eseguiRicercaFascicoli(it.ibm.red.business.dto.ParamsRicercaAvanzataFascDTO,
	 *      it.ibm.red.business.dto.UtenteDTO, boolean).
	 */
	@Override
	public Collection<FascicoloDTO> eseguiRicercaFascicoli(final ParamsRicercaAvanzataFascDTO paramsRicercaAvanzataFascicoli, final UtenteDTO utente,
			final boolean orderbyInQuery) {
		LOGGER.info("eseguiRicercaFascicoli -> START");
		Collection<FascicoloDTO> fascicoliRicerca = new ArrayList<>();
		final HashMap<String, Object> mappaValoriRicercaFilenet = new HashMap<>();
		final Set<String> idFascicoli = new HashSet<>();
		Connection con = null;
		IFilenetCEHelper fceh = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			// Se tra i parametri di ricerca dei fascicoli sono presenti parametri relativi
			// ai documenti in essi contenuti, si esegue innanzitutto una ricerca
			// sui documenti per estrarre tutti i relativi fascicoli. I fascicoli ottenuti
			// restringeranno la successivi ricerca dei fascicoli nel CE.
			if (paramsRicercaAvanzataFascicoli.getTipoAssegnazione() != null || isComboSelected(paramsRicercaAvanzataFascicoli.getDescrizioneTipologiaDocumento())
					|| isComboSelected(paramsRicercaAvanzataFascicoli.getDescrizioneTipoProcedimento())) {
				// Si costruiscono i parametri di ricerca relativi ai documenti a partire da
				// quelli di ricerca dei fascicoli inseriti
				final ParamsRicercaAvanzataDocDTO paramsRicercaAvanzataDocumenti = costruisciParametriRicercaDocumento(paramsRicercaAvanzataFascicoli);

				final Collection<MasterDocumentRedDTO> documentiFascicoli = ricercaAvanzataDocSRV.eseguiRicercaDocumenti(paramsRicercaAvanzataDocumenti, utente, con,
						orderbyInQuery, true, false);

				if (!CollectionUtils.isEmpty(documentiFascicoli)) {
					for (final MasterDocumentRedDTO documentoFascicolo : documentiFascicoli) {
						
						if (documentoFascicolo != null && !StringUtils.isBlank(documentoFascicolo.getDocumentTitle())) {
							final Set<String> ss = fascicoloDAO.getFascicoliDocumento(Integer.parseInt(documentoFascicolo.getDocumentTitle()), utente.getIdAoo(), con);
							
							if (ss != null) {
								idFascicoli.addAll(ss);
							} else {
								LOGGER.warn("fascicoloDAO.getFascicoliDocumento(" + documentoFascicolo.getDocumentTitle() + ")");
							}
						} else {
							LOGGER.error("documentoFascicolo == null");
						}
					}
				}
			}

			// Si convertono i parametri di ricerca relativi al fascicolo in una mappa
			// chiave -> valore
			convertiInParametriFascicoloRicercaFilenet(mappaValoriRicercaFilenet, paramsRicercaAvanzataFascicoli);

			// ### Si esegue la ricerca nel CE
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final DocumentSet fascicoliRicercaFilenet = fceh.ricercaAvanzataFascicoli(mappaValoriRicercaFilenet, idFascicoli, utente);
			if (fascicoliRicercaFilenet != null && !fascicoliRicercaFilenet.isEmpty()) {
				fascicoliRicerca = TrasformCE.transform(fascicoliRicercaFilenet, TrasformerCEEnum.FROM_DOCUMENTO_TO_FASCICOLO);
				ricercaSRV.setDescTitolario(utente, con, fascicoliRicerca);

				if (!orderbyInQuery) {
					final List<FascicoloDTO> fasList = new ArrayList<>(fascicoliRicerca);
					// implementa Comparable
					Collections.sort(fasList, Collections.reverseOrder());
					fascicoliRicerca = fasList;
				}
			}

		} catch (final Exception e) {
			LOGGER.error("eseguiRicercaFascicoli -> Si è verificato un errore durante l'esecuzione della ricerca.", e);
			throw new RedException("eseguiRicercaFascicoli -> Si è verificato un errore durante l'esecuzione della ricerca.", e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}

		LOGGER.info("eseguiRicercaFascicoli -> END. Documenti trovati: " + fascicoliRicerca.size());
		return fascicoliRicerca;
	}

	private void convertiInParametriFascicoloRicercaFilenet(final HashMap<String, Object> paramsRicercaFilenet, final ParamsRicercaAvanzataFascDTO paramsRicerca) {
		LOGGER.info("convertiInParametriFascicoloRicercaFilenet -> START");

		// Numero Fascicolo
		if (paramsRicerca.getNumeroFascicolo() != null) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), paramsRicerca.getNumeroFascicolo());
		}

		// Descrizione Fascicolo
		if (StringUtils.isNotBlank(paramsRicerca.getDescrizioneFascicolo())) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), paramsRicerca.getDescrizioneFascicolo());
		}

		// Stato Fascicolo
		if (paramsRicerca.getStatoFascicolo() != null && !FilenetStatoFascicoloEnum.NON_CATALOGATO.equals(paramsRicerca.getStatoFascicolo())) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.STATO_FASCICOLO_METAKEY), paramsRicerca.getStatoFascicolo().getNome());
		}

		// Titolario
		if (StringUtils.isNotBlank(paramsRicerca.getTitolario())) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY), paramsRicerca.getTitolario());
		}

		// Data Creazione Da
		if (paramsRicerca.getDataCreazioneDa() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_CREAZIONE_DA, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataCreazioneDa(), true));
		}

		// Data Creazione A
		if (paramsRicerca.getDataCreazioneA() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_CREAZIONE_A, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataCreazioneA(), false));
		}

		// Anno Fascicolo
		if (paramsRicerca.getDataCreazioneDa() == null && paramsRicerca.getDataCreazioneA() == null) {
			final Calendar dataCreazioneDa = Calendar.getInstance();
			dataCreazioneDa.set(paramsRicerca.getAnnoFascicolo(), 0, 1);
			paramsRicercaFilenet.put(Ricerca.DATA_CREAZIONE_DA, DateUtils.buildFilenetDataForSearch(dataCreazioneDa.getTime(), true));

			final Calendar dataCreazioneA = Calendar.getInstance();
			dataCreazioneA.set(paramsRicerca.getAnnoFascicolo(), 11, 31);
			paramsRicercaFilenet.put(Ricerca.DATA_CREAZIONE_A, DateUtils.buildFilenetDataForSearch(dataCreazioneA.getTime(), false));
		}

		// Data Chiusura Da
		if (paramsRicerca.getDataChiusuraDa() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_CHIUSURA_DA, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataChiusuraDa(), true));
		}

		// Data Chiusura A
		if (paramsRicerca.getDataChiusuraA() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_CHIUSURA_A, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataChiusuraA(), false));
		}

		// Faldonato
		if (paramsRicerca.getFaldonato() != null) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.FALDONATO_METAKEY), paramsRicerca.getFaldonato());
		}

		LOGGER.info("convertiInParametriFascicoloRicercaFilenet -> END");
	}

	private ParamsRicercaAvanzataDocDTO costruisciParametriRicercaDocumento(final ParamsRicercaAvanzataFascDTO paramsRicercaFascicoli) {
		LOGGER.info("costruisciParametriRicercaDocumento -> START");
		final ParamsRicercaAvanzataDocDTO paramsRicercaAvanzataDocumenti = new ParamsRicercaAvanzataDocDTO();

		// Anno Documento
		paramsRicercaAvanzataDocumenti.setAnnoDocumento(paramsRicercaFascicoli.getAnnoFascicolo());

		// Tipologia Documento
		if (isComboSelected(paramsRicercaFascicoli.getDescrizioneTipologiaDocumento())) {
			paramsRicercaAvanzataDocumenti.setDescrizioneTipologiaDocumento(paramsRicercaFascicoli.getDescrizioneTipologiaDocumento());
		}

		// Tipo Procedimento
		if (isComboSelected(paramsRicercaFascicoli.getDescrizioneTipoProcedimento())) {
			paramsRicercaAvanzataDocumenti.setDescrizioneTipoProcedimento(paramsRicercaFascicoli.getDescrizioneTipoProcedimento());
		}

		// Assegnatario
		if (paramsRicercaFascicoli.getTipoAssegnazione() != null && paramsRicercaFascicoli.getAssegnatario() != null) {
			paramsRicercaAvanzataDocumenti.setTipoAssegnazione(paramsRicercaFascicoli.getTipoAssegnazione());
			paramsRicercaAvanzataDocumenti.setAssegnatario(paramsRicercaFascicoli.getAssegnatario());
		}

		LOGGER.info("costruisciParametriRicercaDocumento -> END");
		return paramsRicercaAvanzataDocumenti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaAvanzataFascFacadeSRV#initComboFormRicerca(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.RicercaAvanzataFascicoliFormDTO).
	 */
	@Override
	public void initComboFormRicerca(final UtenteDTO utente, final RicercaAvanzataFascicoliFormDTO ricercaAvanzataForm) {

		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			/**
			 * TIPOLOGIA DOCUMENTO START
			 *****************************************************/
			ricercaAvanzataForm.setComboTipologieDocumento(getListaTipologieDocumento(con, utente.getIdAoo()));
			ricercaAvanzataForm.setComboTipiProcedimento(getTipiProcedimentoByTipologiaDocumento(null, utente.getIdAoo()));
			/**
			 * TIPOLOGIA DOCUMENTO END
			 *******************************************************/

			/**
			 * COMBO TIPO ASSEGNAZIONE START
			 *************************************************/
			ricercaAvanzataForm.setComboTipoAssegnazione(new ArrayList<>());
			for (final TipoAssegnazioneEnum t : tipoAssegnazioneDAO.getTipiAssegnazioneManualeByIdAoo(utente.getIdAoo().intValue(), con)) {
				if (t == TipoAssegnazioneEnum.COPIA_CONFORME) { // lo levo perché non viene utilizzato nella ricerca dei fascicoli
					continue;
				}
				ricercaAvanzataForm.getComboTipoAssegnazione().add(t);
			}
			/**
			 * COMBO TIPO ASSEGNAZIONE END
			 ***************************************************/

		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

	}

	private List<TipologiaDocumentoDTO> getListaTipologieDocumento(final Connection inCon, final Long idAoo) {
		Connection con = inCon;
		final List<TipologiaDocumentoDTO> list = new ArrayList<>();
		final boolean closeConnection = (con == null);

		try {
			if (closeConnection) {
				con = setupConnection(getDataSource().getConnection(), false);
			}

			list.add(new TipologiaDocumentoDTO(null, null, null, "-", null, null, null, null, null, null, null, null, false, null, null));

			final List<TipologiaDocumentoDTO> tmplist = tipologiaDocumentoDAO.getComboTipologieByAoo(idAoo, con);
			final List<String> descrizioni = new ArrayList<>();

			for (final TipologiaDocumentoDTO t : tmplist) {
				if (descrizioni.contains(t.getDescrizione())) {
					continue;
				}
				descrizioni.add(t.getDescrizione());
				list.add(t);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			if (closeConnection) {
				closeConnection(con);
			}
		}

		return list;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaAvanzataFascFacadeSRV#getTipiProcedimentoByTipologiaDocumento(java.lang.String,
	 *      java.lang.Long).
	 */
	@Override
	public List<TipoProcedimentoDTO> getTipiProcedimentoByTipologiaDocumento(final String descrTipologiaDocumento, final Long idAOO) {
		final List<TipoProcedimentoDTO> list = new ArrayList<>();
		list.add(new TipoProcedimentoDTO(new TipoProcedimento(0L, "-"))); // mi serve purtroppo la voce vuota

		if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(descrTipologiaDocumento) || "-".equals(descrTipologiaDocumento.trim())) {
			return list;
		}

		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final List<Integer> ids = tipologiaDocumentoDAO.getIdsByDescrizione(descrTipologiaDocumento, idAOO, connection);
			boolean toInsert = true;
			for (final Integer id : ids) {
				for (final TipoProcedimento t : tipoProcedimentoDAO.getTipiProcedimentoByTipologiaDocumento(connection, id)) {
					for (final TipoProcedimentoDTO tAlreadyIn : list) {
						if (tAlreadyIn.getDescrizione().equals(t.getDescrizione())) {
							toInsert = false;
							break;
						}
					}
					if (toInsert) {
						list.add(new TipoProcedimentoDTO(t));
					}
				}
			}
		} catch (final SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return list;
	}

	private boolean isComboSelected(final String comboValue) {
		boolean isComboSelected = false;

		if (StringUtils.isNotBlank(comboValue) && !"-".equals(comboValue)) {
			isComboSelected = true;
		}

		return isComboSelected;
	}
}