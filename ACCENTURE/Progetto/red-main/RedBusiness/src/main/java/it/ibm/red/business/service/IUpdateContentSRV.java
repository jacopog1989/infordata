package it.ibm.red.business.service;

import java.sql.Connection;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.service.facade.IUpdateContentFacadeSRV;

/**
 * Interfaccia del servizio che gestisce l'aggiornamento del content.
 */
public interface IUpdateContentSRV extends IUpdateContentFacadeSRV {

	/**
	 * Aggiunge la versione PDF del documento.
	 * @param utente
	 * @param idDocumento
	 * @param firmatari
	 * @param flagModificaMontanaro
	 * @param tipoFirma
	 * @param idTipoAssegnazione
	 */
	void addPDFVersion(UtenteDTO utente, String idDocumento, String[] firmatari, Boolean flagModificaMontanaro,	Integer tipoFirma, Integer idTipoAssegnazione);
	
	/**
	 * Aggiunge la versione PDF del documento.
	 * @param utente
	 * @param wob
	 * @param connection
	 */
	void addPDFVersion(UtenteDTO utente, VWWorkObject wob, Connection connection);
	
}
