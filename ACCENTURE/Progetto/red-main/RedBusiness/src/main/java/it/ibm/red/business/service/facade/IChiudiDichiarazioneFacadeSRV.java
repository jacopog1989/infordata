package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.ChiudiDichiarazioneDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio che gestisce la chiusura di una dichiarazione.
 */
public interface IChiudiDichiarazioneFacadeSRV extends Serializable {
	
	/**
	 * Esegue la chiusura della dichiarazione.
	 * @param wobNumber
	 * @param utente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO chiudiDichiarazione(String wobNumber, UtenteDTO utente);

	/**
	 * Ottiene i metadati.
	 * @param fnDTO - credenziali filenet
	 * @param documentTitle - document title
	 * @param idAOO - id dell'Aoo
	 * @return dati di chiusura dichiarazione
	 */
	ChiudiDichiarazioneDTO getMetadati(FilenetCredentialsDTO fnDTO, String documentTitle, long idAOO);

}
