/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.List;

/**
 * @author APerquoti
 *
 */
public class WorkFlowWsDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -7908179352437266221L;
	
	/**
	 * Numero wf.
	 */
	private String workflowNumber;
	
	/**
	 * WFs.
	 */
	private List<WobNumberWsDTO> wobNumbers;
	
	/**
	 * Subject wf.
	 */
	private String subject;
	
	/**
	 * Costruttore di default.
	 */
	public WorkFlowWsDTO() {
		super();
	}

	/**
	 * @return the workflowNumber
	 */
	public String getWorkflowNumber() {
		return workflowNumber;
	}

	/**
	 * @param workflowNumber the workflowNumber to set
	 */
	public void setWorkflowNumber(final String workflowNumber) {
		this.workflowNumber = workflowNumber;
	}

	/**
	 * @return the wobNumbers
	 */
	public List<WobNumberWsDTO> getWobNumbers() {
		return wobNumbers;
	}

	/**
	 * @param wobNumbers the wobNumbers to set
	 */
	public void setWobNumbers(final List<WobNumberWsDTO> wobNumbers) {
		this.wobNumbers = wobNumbers;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(final String subject) {
		this.subject = subject;
	}
	

}
