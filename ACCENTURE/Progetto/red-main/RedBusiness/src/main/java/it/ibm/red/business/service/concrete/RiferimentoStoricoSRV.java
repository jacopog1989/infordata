package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IRiferimentoStoricoDAO;
import it.ibm.red.business.dto.AllaccioRiferimentoStoricoDTO;
import it.ibm.red.business.dto.RiferimentoProtNpsDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IRiferimentoStoricoSRV;

/**
 * @author VINGENITO
 *
 */
@Service
public class RiferimentoStoricoSRV extends AbstractService implements IRiferimentoStoricoSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -7706324636237559883L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RiferimentoStoricoSRV.class.getName());

	/**
	 * Dao.
	 */
	@Autowired
	private IRiferimentoStoricoDAO rifStoricoDAO;

	/**
	 * @see it.ibm.red.business.service.IRiferimentoStoricoSRV#getRiferimentoStoricoByAoo(java.lang.String,
	 *      java.lang.Long, java.lang.String).
	 */
	@Override
	public List<AllaccioRiferimentoStoricoDTO> getRiferimentoStoricoByAoo(final String docTitle, final Long idAoo, final String idFascicoloProcedimentale) {
		List<AllaccioRiferimentoStoricoDTO> allaccioRifStoricoDTO = new ArrayList<>();
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), true);

			allaccioRifStoricoDTO = rifStoricoDAO.getRiferimentoStoricoByAoo(docTitle, idAoo, idFascicoloProcedimentale, conn);

			commitConnection(conn);
		} catch (final Exception e) {
			LOGGER.error("Errore nella ricerca dei documenti dei riferimenti storici :" + e);
			rollbackConnection(conn);
		} finally {
			closeConnection(conn);
		}
		return allaccioRifStoricoDTO;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRiferimentoStoricoFacadeSRV#inserisciRiferimentoStorico(java.util.List).
	 */
	@Override
	public void inserisciRiferimentoStorico(final List<AllaccioRiferimentoStoricoDTO> allaccioRifStoricoDTOList) {
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			if (allaccioRifStoricoDTOList != null) {
				for (final AllaccioRiferimentoStoricoDTO allaccioRifStoricoDTO : allaccioRifStoricoDTOList) {
					rifStoricoDAO.insertRiferimentoStorico(allaccioRifStoricoDTO.getDocTitleUscita(), Integer.parseInt(allaccioRifStoricoDTO.getnProtocolloNsd()),
							allaccioRifStoricoDTO.getIdFilePrincipale(), new Date(), new Date(), allaccioRifStoricoDTO.getIdFascProcedimentale(),
							allaccioRifStoricoDTO.getDescFascProcedimentale(), allaccioRifStoricoDTO.getIdAoo(), allaccioRifStoricoDTO.getOggetto(),
							allaccioRifStoricoDTO.getAnnoProtocollo(), conn);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore nell'inserimento dei documenti nella tabella di allaccio riferimento storico:" + e);
			rollbackConnection(conn);
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRiferimentoStoricoFacadeSRV#deleteRiferimentoStoricoModifica(java.lang.String,
	 *      java.lang.Long).
	 */
	@Override
	public void deleteRiferimentoStoricoModifica(final String documentTitle, final Long idAoo) {
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			rifStoricoDAO.deleteRiferimentoStoricoModifica(documentTitle, idAoo, conn);
		} catch (final Exception e) {
			LOGGER.error("Errore nella cancellazione dei documenti nella tabella di allaccio riferimento storico:" + e);
			rollbackConnection(conn);
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRiferimentoStoricoFacadeSRV#insertRifAllaccioNps(java.lang.Long,
	 *      java.lang.String, java.util.List, java.lang.String).
	 */
	@Override
	public void insertRifAllaccioNps(final Long idAoo,final String docTitleUscita,final List<RiferimentoProtNpsDTO> allacciRiferimentoNps,final String idFascicoloProc) { 
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), true);
			if(allacciRiferimentoNps!=null) {
				for(RiferimentoProtNpsDTO allaccioRiferimentoNps : allacciRiferimentoNps) {  
					rifStoricoDAO.insertRifAllaccioNps(idAoo,allaccioRiferimentoNps.getAnnoProtocolloNps(), allaccioRiferimentoNps.getNumeroProtocolloNps(), new Date(),
							docTitleUscita,idFascicoloProc,allaccioRiferimentoNps.getOggettoProtocolloNps(),allaccioRiferimentoNps.getIdDocumentoDownload(), conn);
				 }
			} 
			commitConnection(conn);
		} catch (Exception e) {
			LOGGER.error("Errore nell'inserimento dei documenti nella tabella di allaccio riferimento storico:"+e);
			rollbackConnection(conn);
		} finally {
			closeConnection(conn);
		} 
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRiferimentoStoricoFacadeSRV#deleteRiferimentoAllaccioNpsModifica(java.lang.String,
	 *      java.lang.Long).
	 */
	@Override
	public void deleteRiferimentoAllaccioNpsModifica(final String documentTitle,final Long idAoo) { 
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			rifStoricoDAO.deleteRiferimentoAllaccioNpsModifica(documentTitle, idAoo, conn);
		} catch (Exception e) {
			LOGGER.error("Errore nella cancellazione dei documenti nella tabella di allaccio riferimento storico:"+e);
			rollbackConnection(conn);
		} finally {
			closeConnection(conn);
		} 
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRiferimentoStoricoFacadeSRV#getRiferimentoNpsByAoo(java.lang.String,
	 *      java.lang.Long, java.lang.String).
	 */
	@Override
	public List<RiferimentoProtNpsDTO> getRiferimentoNpsByAoo(final String docTitle,final Long idAoo,final String idFascicoloProcedimentale) {
		List<RiferimentoProtNpsDTO> allaccioRifNpsDTO = new ArrayList<>();
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection() ,true);
			allaccioRifNpsDTO = rifStoricoDAO.getRiferimentoNpsByAoo(docTitle,idAoo,idFascicoloProcedimentale, conn);
			commitConnection(conn);
		} catch (Exception e) {
			LOGGER.error("Errore nella ricerca dei documenti dei riferimenti storici :"+e);
			rollbackConnection(conn);
		} finally {
			closeConnection(conn);
		} 
		return allaccioRifNpsDTO;
	}
}
