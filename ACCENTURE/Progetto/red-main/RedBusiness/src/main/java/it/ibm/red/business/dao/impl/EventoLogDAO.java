package it.ibm.red.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.constants.Constants.Ricerca;
import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IEventoLogDAO;
import it.ibm.red.business.dto.EventoLogDTO;
import it.ibm.red.business.dto.FascicoloFlussoDTO;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Dao per la gestione degli eventi.
 * 
 * @author m.crescentini
 */
@Repository
public class EventoLogDAO extends AbstractDAO implements IEventoLogDAO {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1087447120707389680L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(EventoLogDAO.class.getName());
	
	/**
	 * Colonna - id documento.
	 */
	private static final String IDDOCUMENTO = "IDDOCUMENTO";

	/**
	 * Keyword oracle per la trasformazione in Date di una String.
	 */
	private static final String TO_DATE = "to_date('";

	/**
	 * Label - AND.
	 */
	private static final String AND = " AND ";

	/**
	 * Messaggio di errore recupero record.
	 */
	private static final String ERROR_RECUPERO_RECORD_MSG = "Errore nel reperire il record agli atti del documento: ";

	@Override
	public final void inserisciEvento(final EventoLogDTO evento, final Long idAoo, final Connection con) {
		LOGGER.info("inserisciEvento -> START");
		CallableStatement cs = null;
		try {
			int index = 0;
			cs = con.prepareCall("call p_inseventicustom(?,?,?,?,?,?,?,?,?,?)");

			final Calendar calendarDataAssegnazione = Calendar.getInstance(Locale.ITALY);
			final SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.DD_MM_YYYY_HH_MM_SS);
			calendarDataAssegnazione.setTime(evento.getDataAssegnazione());

			cs.setLong(++index, idAoo); // in_IDAOO
			cs.setInt(++index, evento.getIdDocumento()); // in_IDDOCUMENTO
			cs.setLong(++index, evento.getIdUfficioMittente()); // in_IDNODOMITTENTE
			cs.setLong(++index, evento.getIdUtenteMittente()); // in_IDUTENTEMITTENTE
			cs.setLong(++index, evento.getIdUfficioDestinatario()); // in_IDNODODESTINATARIO
			cs.setLong(++index, evento.getIdUtenteDestinatario()); // in_IDUTENTEDESTINATARIO
			cs.setString(++index, sdf.format(calendarDataAssegnazione.getTime())); // in_DATAASSEGNAZIONE
			cs.setInt(++index, Integer.parseInt(evento.getEventType())); // in_TIPOEVENTO
			cs.setString(++index, evento.getMotivazioneAssegnazione()); // in_MOTIVAZIONEASSEGNAZIONE
			cs.setInt(++index, evento.getIdTipoAssegnazione()); // in_IDTIPOASSEGNAZIONE

			LOGGER.info("inserisciEvento -> Chiamata alla procedura p_inseventicustom");
			cs.execute();
		} catch (final SQLException e) {
			LOGGER.error("Errore nell'inserimento dell'evento: " + evento.getEventType() + " per il documento: " + evento.getIdDocumento(), e);
			throw new RedException("Errore nell'inserimento dell'evento: " + evento.getEventType() + " per il documento: " + evento.getIdDocumento(), e);
		} finally {
			closeStatement(cs);
		}
		LOGGER.info("inserisciEvento -> END");
	}

	@Override
	public final EventoLogDTO getAttiEvent(final Connection con, final String documentTitle, final Long idAoo) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		EventoLogDTO eventLog = null;

		try {
			final String querySQL = "SELECT * FROM D_EVENTI_CUSTOM WHERE IDDOCUMENTO = ? AND IDAOO = ? AND TIPOEVENTO = " + EventTypeEnum.ATTI.getValue();
			ps = con.prepareStatement(querySQL);
			ps.setInt(1, Integer.parseInt(documentTitle));
			ps.setInt(2, idAoo.intValue());
			rs = ps.executeQuery();
			if (rs.next()) {
				eventLog = populateEventoLog(rs);
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_RECORD_MSG + documentTitle, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return eventLog;
	}

	@Override
	public final boolean giaSpedito(final Connection con, final String documentTitle) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean giaSpedito = false;
		int index = 1;
		try {
			final String querySQL = "SELECT * FROM D_EVENTI_CUSTOM WHERE IDDOCUMENTO = ?  AND TIPOEVENTO = ?";
			ps = con.prepareStatement(querySQL);
			ps.setInt(index++, Integer.parseInt(documentTitle));
			ps.setInt(index++, Integer.valueOf(EventTypeEnum.SPEDITO.getValue()));

			rs = ps.executeQuery();
			if (rs.next()) {
				giaSpedito = true;
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_RECORD_MSG + documentTitle, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return giaSpedito;
	}

	/**
	 * @see it.ibm.red.business.dao.IEventoLogDAO#cercaEventiPerRicercaDocumenti(java.
	 *      util.Map, java.sql.Connection).
	 */
	@Override
	public final List<EventoLogDTO> cercaEventiPerRicercaDocumenti(final Map<String, ?> mappaValoriRicerca, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<EventoLogDTO> eventoLogList = new ArrayList<>();
		final StringBuilder stringBuilderQueryTemp = new StringBuilder();
		Boolean tipoRicercaListaIn = true; // IN operatore

		if (mappaValoriRicerca.containsKey(Ricerca.TIPO_RICERCA_LISTA)) {
			tipoRicercaListaIn = (Boolean) mappaValoriRicerca.get(Ricerca.TIPO_RICERCA_LISTA); // false; // OR operatore
			mappaValoriRicerca.remove(Ricerca.TIPO_RICERCA_LISTA);
		}

		String annoCondition = null;
		final String annoDocumentoKey = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY);
		if (mappaValoriRicerca.containsKey(annoDocumentoKey)) {
			final Integer annoDocumento = (Integer) mappaValoriRicerca.get(annoDocumentoKey);
			mappaValoriRicerca.remove(annoDocumentoKey);

			try {
				final String daAnno = TO_DATE + "01-01-" + annoDocumento + " 00:00:00', 'dd-mm-yyyy hh24:mi:ss')";
				final String aAnno = TO_DATE + "31-12-" + annoDocumento + " 23:59:59', 'dd-mm-yyyy hh24:mi:ss')";
				annoCondition = " AND timestampoperazione >= " + daAnno + " AND timestampoperazione <= " + aAnno;
			} catch (final Exception e) {
				LOGGER.error("cercaEventiPerRicerca -> Si è verificato un errore durante il recupero dell'anno di ricerca.", e);
			}
		}

		String daTimestampoperazioneCondition = null;
		final String timestampoperazioneda = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TIMESTAMP_OPERAZIONE_DA);
		if (mappaValoriRicerca.containsKey(timestampoperazioneda)) {
			final Date dataDa = (Date) mappaValoriRicerca.get(timestampoperazioneda);
			mappaValoriRicerca.remove(timestampoperazioneda);

			try {
				final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				final String daTimestampoperazione = TO_DATE + sdf.format(dataDa) + "', 'dd-mm-yyyy')";
				daTimestampoperazioneCondition = " AND trunc(timestampoperazione) >= " + daTimestampoperazione;
			} catch (final Exception e) {
				LOGGER.error("cercaEventiPerRicerca -> Si è verificato un errore durante il recupero del timestampoperazioneda.", e);
			}
		}

		
		String timestampoperazionea =  PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TIMESTAMP_OPERAZIONE_A);
		if (mappaValoriRicerca.containsKey(timestampoperazionea)) {
			Date dataDa = (Date) mappaValoriRicerca.get(timestampoperazionea);
			mappaValoriRicerca.remove(timestampoperazionea);
			
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				String daTimestampoperazione = TO_DATE + sdf.format(dataDa) + "', 'dd-mm-yyyy')";
				daTimestampoperazioneCondition += " AND trunc(timestampoperazione) <= " + daTimestampoperazione;
			} catch (Exception e) {
				LOGGER.error("cercaEventiPerRicerca -> Si è verificato un errore durante il recupero del timestampoperazioneda.", e);
			}
		}
		
		try {
			final Set<String> chiaviRicerca = mappaValoriRicerca.keySet();
			final Iterator<String> itChiaviRicerca = chiaviRicerca.iterator();
			while (itChiaviRicerca.hasNext()) {
				final String chiaveRicerca = itChiaviRicerca.next();

				if (mappaValoriRicerca.get(chiaveRicerca) instanceof List) {
					final List<?> valoriListaRicerca = (List<?>) mappaValoriRicerca.get(chiaveRicerca);
					final StringBuilder sbIn = new StringBuilder();

					if (valoriListaRicerca != null && !valoriListaRicerca.isEmpty()) {
						for (int i = 0; i < valoriListaRicerca.size(); i++) {
							final Integer id = (Integer) valoriListaRicerca.get(i);
							if (Boolean.TRUE.equals(tipoRicercaListaIn)) {
								sbIn.append(id);
							} else {
								sbIn.append(chiaveRicerca + " = " + id);
							}
							if (i < valoriListaRicerca.size() - 1) {
								if (Boolean.TRUE.equals(tipoRicercaListaIn)) {
									sbIn.append(", ");
								} else {
									sbIn.append(" OR ");
								}
							}
						}
					}

					if (sbIn.length() > 0) {
						if (Boolean.TRUE.equals(tipoRicercaListaIn)) {
							stringBuilderQueryTemp.append(AND + chiaveRicerca + " IN (" + sbIn.toString() + ") ");
						} else {
							stringBuilderQueryTemp.append(" AND (" + sbIn.toString() + ") ");
						}
					}
				} else {
					final Object value = mappaValoriRicerca.get(chiaveRicerca);
					if (stringBuilderQueryTemp.length() > 0) {
						stringBuilderQueryTemp.append(AND + chiaveRicerca + " = " + value);
					} else {
						stringBuilderQueryTemp.append(" " + chiaveRicerca + " = " + value);
					}
				}
			}

			if (annoCondition != null) {
				stringBuilderQueryTemp.append(annoCondition);
			}

			if (daTimestampoperazioneCondition != null) {
				stringBuilderQueryTemp.append(daTimestampoperazioneCondition);
			}

			final StringBuilder stringBuilderQuery = new StringBuilder();
			stringBuilderQuery.append("SELECT iddocumento FROM d_eventi_custom WHERE ");
			stringBuilderQuery.append(stringBuilderQueryTemp);

			ps = con.prepareStatement(stringBuilderQuery.toString());
			rs = ps.executeQuery();
			eventoLogList = fetchMultiResultSetRicerca(rs);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la ricerca degli eventi per la ricerca dei documenti.", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return eventoLogList;
	}

	/**
	 * @see it.ibm.red.business.dao.IEventoLogDAO#getEventiStoricoByIdDocumento(int,
	 *      java.lang.Long, java.sql.Connection).
	 * @param idDocumento
	 *            id del documento dal quale recuperare gli eventi storico
	 * @param idAoo
	 *            id aoo
	 * @param con
	 *            connessione al database
	 * @return lista degli event log recuperati
	 */
	@Override
	public final List<EventoLogDTO> getEventiStoricoByIdDocumento(final int idDocumento, final Long idAoo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<EventoLogDTO> eventiDocumento = new ArrayList<>();

		try {
			int index = 1;
			final String query = "SELECT * FROM d_eventi_custom WHERE iddocumento = ? AND idaoo = ? ORDER BY DATAASSEGNAZIONE ASC, ID ASC";

			ps = con.prepareStatement(query);
			ps.setInt(index++, idDocumento);
			ps.setLong(index++, idAoo);

			rs = ps.executeQuery();
			eventiDocumento = fetchMultiResultSet(rs);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la ricerca degli eventi per il documento: " + idDocumento, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return eventiDocumento;
	}

	/**
	 * @see it.ibm.red.business.dao.IEventoLogDAO#getLastChiusuraIngressoEvent(java.sql.
	 *      Connection, int, int).
	 * @param filenetConnection
	 *            connessione dwh
	 * @param idDocumento
	 *            id documento dal quale recuperare l'ultimo evento di chiusura
	 *            ingresso
	 * @param idAoo
	 *            id aoo
	 * @return event log se esistente, null altrimenti
	 */
	@Override
	public final EventoLogDTO getLastChiusuraIngressoEvent(final Connection filenetConnection, final int idDocumento, final int idAoo) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		EventoLogDTO eventLog = null;

		try {
			// cerco nello storico l'ultima chiusura di ingresso (messa agli atti o
			// preparazione alla risposta)
			final String filenetQuerySQL = "SELECT * FROM ( SELECT * FROM D_EVENTI_CUSTOM WHERE IDDOCUMENTO = ? AND IDAOO = ? AND TIPOEVENTO IN (1009, 1010, 1108, 1109, 1111, 1112, 1113, 1114) ORDER BY DATAASSEGNAZIONE DESC, ID DESC )";

			ps = filenetConnection.prepareStatement(filenetQuerySQL);
			ps.setInt(1, idDocumento);
			ps.setInt(2, idAoo);
			rs = ps.executeQuery();

			if (rs.next()) {
				eventLog = fetchResultSet(rs);
			}

			return eventLog;
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IEventoLogDAO#getFirstUserEvent(int,
	 *      java.lang.Long, java.sql.Connection).
	 * @param idDocumento
	 *            id documento dal quale recuperare il primo evento utente
	 * @param idAoo
	 *            id dell'AOO
	 * @param con
	 *            connession al database
	 * @return event log dell'evento se esistente, null altrimenti
	 */
	@Override
	public final EventoLogDTO getFirstUserEvent(final int idDocumento, final Long idAoo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		EventoLogDTO eventLog = null;

		try {
			final String filenetQuerySQL = "SELECT * FROM (SELECT * FROM D_EVENTI_CUSTOM WHERE IDDOCUMENTO = ? AND IDAOO = ? and IDNODODESTINATARIO <> 0"
					+ " ORDER BY DATAASSEGNAZIONE asc, ID ASC ) WHERE ROWNUM = 1";

			ps = con.prepareStatement(filenetQuerySQL);
			ps.setInt(1, idDocumento);
			ps.setLong(2, idAoo);
			rs = ps.executeQuery();

			if (rs.next()) {
				eventLog = fetchResultSet(rs);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return eventLog;
	}

	@Override
	public final boolean isEventoPresentePerDocumento(final int idDocumento, final Long idAoo, final EventTypeEnum tipoEvento, final Connection dwhCon) {
		boolean eventoPresente = false;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final StringBuilder filenetQuerySQL = new StringBuilder("SELECT * FROM D_EVENTI_CUSTOM WHERE IDDOCUMENTO = ? AND IDAOO = ? AND TIPOEVENTO = ?");

			ps = dwhCon.prepareStatement(filenetQuerySQL.toString());
			ps.setInt(1, idDocumento);
			ps.setLong(2, idAoo);
			ps.setInt(3, tipoEvento.getIntValue());

			rs = ps.executeQuery();

			if (rs.next()) {
				eventoPresente = true;
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return eventoPresente;
	}

	private static List<EventoLogDTO> fetchMultiResultSet(final ResultSet rs) throws SQLException {
		final List<EventoLogDTO> resultList = new ArrayList<>();

		while (rs.next()) {
			final EventoLogDTO eventoLog = fetchResultSet(rs);
			resultList.add(eventoLog);
		}

		return resultList;
	}

	private static EventoLogDTO fetchResultSet(final ResultSet rs) throws SQLException {
		final EventoLogDTO eventoLog = populateEventoLog(rs);
		eventoLog.setWorkFlowNumber(rs.getString("idworkflow"));
		eventoLog.setWorkFlowNumberPadre(rs.getString("idworkflow_padre"));

		return eventoLog;
	}

	/**
	 * @param rs
	 * @param pp
	 * @return
	 * @throws SQLException
	 */
	private static EventoLogDTO populateEventoLog(final ResultSet rs) throws SQLException {
		
		final PropertiesProvider pp = PropertiesProvider.getIstance();
		final EventoLogDTO eventoLog = new EventoLogDTO();
		eventoLog.setIdDocumento(rs.getInt(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY)));
		eventoLog.setEventType(String.valueOf(rs.getInt(pp.getParameterByKey(PropertiesNameEnum.TIPO_EVENTO))));
		eventoLog.setIdUfficioMittente(rs.getLong(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY)));
		eventoLog.setIdUtenteMittente(rs.getLong(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY)));
		eventoLog.setIdUfficioDestinatario(rs.getLong(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY)));
		eventoLog.setIdUtenteDestinatario(rs.getLong(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY)));
		eventoLog.setIdTipoAssegnazione(rs.getInt(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY)));
		eventoLog.setMotivazioneAssegnazione(rs.getString(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY)));
		if (rs.getDate(pp.getParameterByKey(PropertiesNameEnum.DATA_ASSEGNAZIONE_WF_METAKEY)) != null) {
			eventoLog.setDataAssegnazione(rs.getTimestamp(pp.getParameterByKey(PropertiesNameEnum.DATA_ASSEGNAZIONE_WF_METAKEY)));
		}
		return eventoLog;
	}
	
	private static List<EventoLogDTO> fetchMultiResultSetRicerca(final ResultSet rs) throws SQLException {
		final List<EventoLogDTO> resultList = new ArrayList<>();

		while (rs.next()) {
			final EventoLogDTO eventLog = new EventoLogDTO();
			eventLog.setIdDocumento(rs.getInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY)));
			resultList.add(eventLog);
		}

		return resultList;
	}

	/**
	 * @see it.ibm.red.business.dao.IEventoLogDAO#filtraDocumentiByDataEvento(java.util.Map,
	 *      java.util.Date, java.util.Date, java.sql.Connection).
	 */
	@Override
	public void filtraDocumentiByDataEvento(final Map<String, FascicoloFlussoDTO> docs, final Date dataDa, Date dataA, final Connection dwhCon) {

		// se dataA non presente, filtro fino alla data odierna
		if (dataA == null) {
			dataA = new Date();
		}

		PreparedStatement ps = null;
		ResultSet rs = null;

		final StringBuilder query = new StringBuilder("SELECT ID_DOC, TIMESTAMPOPERAZIONE, DESCRIZIONE FROM ( ");
		query.append("SELECT e.IDDOCUMENTO AS ID_DOC, max(e.ID) AS ID_EVENTO FROM d_eventi_custom e GROUP BY e.IDDOCUMENTO) tmp "
				+ "INNER JOIN d_eventi_custom ON d_eventi_custom.ID = ID_EVENTO INNER JOIN TIPOEVENTO ON TIPOEVENTO = IDTIPOEVENTO ");
		// condizione temporale
		query.append("WHERE TIMESTAMPOPERAZIONE >= ? AND TIMESTAMPOPERAZIONE <= ?");
		// condizione IN
		query.append(AND).append(StringUtils.createInCondition(IDDOCUMENTO, docs.keySet().iterator(), true));
		try {

			ps = dwhCon.prepareStatement(query.toString());

			ps.setTimestamp(1, new Timestamp(dataDa.getTime()));
			ps.setTimestamp(2, new Timestamp(dataA.getTime()));

			rs = ps.executeQuery();

			while (rs.next()) {
				final String idDoc = rs.getString("ID_DOC");

				final FascicoloFlussoDTO f = docs.get(idDoc);
				f.setDataUltimoEvento(rs.getDate("TIMESTAMPOPERAZIONE"));
				f.setDescrizioneUltimoEvento(rs.getString("DESCRIZIONE"));
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante la ricerca eventi", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	@Override
	public final EventoLogDTO getPredisponiEvent(final String documentTitle, final Long idAoo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		EventoLogDTO eventLog = null;

		try {
			final StringBuilder querySQL = new StringBuilder("SELECT * FROM D_EVENTI_CUSTOM WHERE IDDOCUMENTO = ? AND IDAOO = ? AND TIPOEVENTO IN (") 
															.append(EventTypeEnum.RISPOSTA.getValue()).append(",")
															.append(EventTypeEnum.PREDISPOSIZIONE_RISPOSTA.getValue()).append(",")
															.append(EventTypeEnum.PREDISPOSIZIONE_INOLTRO.getValue()).append(",")
															.append(EventTypeEnum.PREDISPOSIZIONE_RESTITUZIONE.getValue())
															.append(")");
			ps = con.prepareStatement(querySQL.toString());
			ps.setInt(1, Integer.parseInt(documentTitle));
			ps.setInt(2, idAoo.intValue());
			rs = ps.executeQuery();
			if (rs.next()) {
				eventLog = populateEventoLog(rs);
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_RECORD_MSG + documentTitle, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return eventLog;
	}

	/**
	 * @see it.ibm.red.business.dao.IEventoLogDAO#getPredisponiEventWfAperto(java.lang.String,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public EventoLogDTO getPredisponiEventWfAperto(String documentTitle, Long idAoo, Connection fnCon) {
		PreparedStatement ps = null;
		final PropertiesProvider pp = PropertiesProvider.getIstance();
		ResultSet rs = null;
		EventoLogDTO eventLog = null;

		try {
			final StringBuilder querySQL = new StringBuilder("SELECT * FROM D_EVENTI_CUSTOM WHERE IDDOCUMENTO = ? AND IDAOO = ? AND TIPOEVENTO IN (") 
															.append(EventTypeEnum.PREDISPOSIZIONE_VISTO.getValue()).append(",")
															.append(EventTypeEnum.PREDISPOSIZIONE_OSSERVAZIONE.getValue()).append(",")
															.append(EventTypeEnum.PREDISPOSIZIONE_RELAZIONE_POSITIVA.getValue()).append(",")
															.append(EventTypeEnum.PREDISPOSIZIONE_RELAZIONE_NEGATIVA.getValue()).append(",")
															.append(EventTypeEnum.RICHIESTA_INTEGRAZIONI.getValue())
															.append(")");
			ps = fnCon.prepareStatement(querySQL.toString());
			ps.setInt(1, Integer.parseInt(documentTitle));
			ps.setInt(2, idAoo.intValue());
			rs = ps.executeQuery();
			if (rs.next()) {
				eventLog = new EventoLogDTO();
				eventLog.setIdDocumento(rs.getInt(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY)));
				eventLog.setEventType(String.valueOf(rs.getInt(pp.getParameterByKey(PropertiesNameEnum.TIPO_EVENTO))));
				eventLog.setIdUfficioMittente(rs.getLong(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY)));
				eventLog.setIdUtenteMittente(rs.getLong(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY)));
				eventLog.setIdUfficioDestinatario(rs.getLong(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY)));
				eventLog.setIdUtenteDestinatario(rs.getLong(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY)));
				eventLog.setIdTipoAssegnazione(rs.getInt(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY)));
				eventLog.setMotivazioneAssegnazione(rs.getString(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY)));
				if (rs.getDate(pp.getParameterByKey(PropertiesNameEnum.DATA_ASSEGNAZIONE_WF_METAKEY)) != null) {
					eventLog.setDataAssegnazione(rs.getTimestamp(pp.getParameterByKey(PropertiesNameEnum.DATA_ASSEGNAZIONE_WF_METAKEY)));
				}
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_RECORD_MSG + documentTitle, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return eventLog;
	}
	
	@Override
	public final HashMap<String, Integer> getIdUfficioCreatore(final List<String> documentTitle, final Long idAoo,final Connection conFilenet) {
		PreparedStatement ps = null; 
		ResultSet rs = null;
		HashMap<String,Integer> mapDocTitleUffCreatore = null;
		
		try {	
			int index = 1;
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT DISTINCT IDDOCUMENTO,IDNODOMITTENTE ");
			sb.append("FROM (SELECT d.IDDOCUMENTO,d.IDNODOMITTENTE, ROW_NUMBER() OVER(PARTITION BY d.iddocumento ORDER BY d.id ASC) as colonnacount ");
			sb.append("FROM D_EVENTI_CUSTOM d ");
			sb.append("WHERE IDAOO = ? AND TIPOEVENTO = ? AND " );
			sb.append(StringUtils.createInCondition(IDDOCUMENTO, documentTitle.iterator(), true));
			sb.append(" ORDER BY d.id ASC) where COLONNACOUNT=1");
			
			ps = conFilenet.prepareStatement(sb.toString()); 
			ps.setInt(index++, idAoo.intValue());
			ps.setString(index++, EventTypeEnum.DOCUMENTO.getValue());
			rs = ps.executeQuery();
			mapDocTitleUffCreatore = new HashMap<>();
			while (rs.next()) {
				mapDocTitleUffCreatore.put(rs.getString(IDDOCUMENTO), rs.getInt("IDNODOMITTENTE")); 
			}
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero dell'ufficio creatore" + documentTitle, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return mapDocTitleUffCreatore;
	}
	
}