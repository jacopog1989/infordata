package it.ibm.red.business.helper.html;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.w3c.tidy.Configuration;
import org.w3c.tidy.Tidy;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.DetailEmailDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedDTO;
import it.ibm.red.business.dto.TemplateMetadatoValueDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoTemplateMetadatoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.xml.DatiProtocollo;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.concrete.FepaSRV.ProvenienzaType;
import it.ibm.red.business.template.TemplateDocumento;
import it.ibm.red.business.template.TemplateDocumentoEntrata;
import it.ibm.red.business.template.TemplateDocumentoUscita;
import it.ibm.red.business.utils.FileUtils;

/**
 * Helper file HTML.
 */
public class HtmlFileHelper implements IHtmlFileHelper, Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -3393689032084667020L;
	
	/**
	 * Tag di chiusura table.
	 */
	private static final String TABLE_CLOSE_TAG = "</table>";

	/**
	 * Tag di apertura table con style specifico.
	 */
	private static final String TABLE_BORDER_0_STYLE_WIDTH_650PX = "<table border=\"0\" style=\"width: 650px;\">";

	/**
	 * Nome file Template - Parte 3.
	 */
	private static final String TEMPLATE_PART_3 = "template_part_3.txt";
	
	/**
	 * Nome file Template Documento - Parte 1.
	 */
	private static final String TEMPLATE_DOCUMENTO_PART_1 = "template_documento_part_1.txt";
	
	/**
	 * Messaggio errore creazone del file HTML. Occorre appendere l'identificativo del documento a questo messaggio.
	 */
	private static final String ERROR_CREAZIONE_HTML_MSG = "Errore nella creazione del file HTML per il documento: ";
	
	/**
	 * Messaggio di avvenuta creazione HTML per il documento. Occorre appendere l'identificativo del documento a questo messaggio.
	 */
	private static final String SUCCESS_HTML_GENERATO_MSG = "HTML generato con successo per il documento: ";
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(HtmlFileHelper.class.getName());

	/**
	 * @see it.ibm.red.business.helper.html.IHtmlFileHelper#creaDocumentContentMailHTML(java.lang.String).
	 */
	@Override
	public ByteArrayInputStream creaDocumentContentMailHTML(final String contentMail) {
		ByteArrayInputStream resultHtml = null;
		final StringBuilder fileHtml = new StringBuilder();

		try {
			// Parte comune di header
			fileHtml.append("<html>").append(System.lineSeparator()).append("<body>").append(System.lineSeparator());
			
			// Contenuto
			fileHtml.append(contentMail);
			
			// Parte comune di footer
			fileHtml.append(System.lineSeparator()).append("</body>").append(System.lineSeparator()).append("</html>");

			resultHtml = new ByteArrayInputStream(fileHtml.toString().getBytes(StandardCharsets.UTF_8.name()));
			LOGGER.info("HTML generato con successo");
		} catch (final Exception e) {
			LOGGER.error("Errore nella creazione del file HTML", e);
		}
		
		return resultHtml;
	}
	
	/**
	 * @see it.ibm.red.business.helper.html.IHtmlFileHelper#creaDocumentoHtml(it.ibm.red.business.template.TemplateDocumento,
	 *      it.ibm.red.business.dto.SalvaDocumentoRedDTO).
	 */
	@Override
	public ByteArrayInputStream creaDocumentoHtml(final TemplateDocumento templateDocumento, final SalvaDocumentoRedDTO documento) {
		ByteArrayInputStream resultHtml = null;
		final StringBuilder fileHtml = new StringBuilder();
		final String idDocumento = documento.getDocumentTitle();
		
		try {
			final String htmlDocumentoPart1 = new String(FileUtils.getFileFromInternalResources(TEMPLATE_DOCUMENTO_PART_1), StandardCharsets.UTF_8.name());
			final String htmlPart3 = new String(FileUtils.getFileFromInternalResources(TEMPLATE_PART_3), StandardCharsets.UTF_8.name());
			
			String part2FileName = "";
			String htmlDocumentoPart2 = "";
			if (templateDocumento instanceof TemplateDocumentoEntrata) {
				part2FileName = "template_documento_entrata_part_2.txt";
			} else if (templateDocumento instanceof TemplateDocumentoUscita) {
				part2FileName = "template_documento_uscita_part_2.txt";
			}
			if (StringUtils.isNotBlank(part2FileName)) {
				htmlDocumentoPart2 = new String(FileUtils.getFileFromInternalResources(part2FileName), StandardCharsets.UTF_8.name());
			}
			
			// Si codificano i campi di testo libero del documento in HTML 
			codificaCampiDocumentoInHtml(documento);
		
			// Parte comune di header
			fileHtml.append(htmlDocumentoPart1);
			// Tabella di header
			fileHtml.append(templateDocumento.getHeaderTable(documento));
			// Seconda parte comune
			fileHtml.append(htmlDocumentoPart2);
			// Tabella corpo Richiesta
			fileHtml.append(TABLE_BORDER_0_STYLE_WIDTH_650PX);
			// Dettaglio del documento
			fileHtml.append(templateDocumento.getDettaglioDocumentoTable(documento));
			// Footer (componenti dinamici)
			fileHtml.append(templateDocumento.getFooterTable(documento));
			// Chiusura tabella
			fileHtml.append(TABLE_CLOSE_TAG);
			// Parte comune di footer
			fileHtml.append(htmlPart3);
			
			resultHtml = new ByteArrayInputStream(fileHtml.toString().getBytes(StandardCharsets.UTF_8.name()));
			
			LOGGER.info(SUCCESS_HTML_GENERATO_MSG + idDocumento);
		} catch (final Exception e) {
			LOGGER.error(ERROR_CREAZIONE_HTML_MSG + idDocumento, e);
		}
		
		return resultHtml;
	}
	
	
	/**
	 * Metodo che codifica i campi di testo libero del documento in HTML, 
	 * sostistuendo i caratteri speciali con le rispettive entità HTML.
	 * 
	 * @param documento
	 * @throws UnsupportedEncodingException 
	 */
	private void codificaCampiDocumentoInHtml(final SalvaDocumentoRedDTO documento) {
		if (StringUtils.isNotBlank(documento.getOggetto())) {
			final byte[] oggettoUTF8 = documento.getOggetto().getBytes();
			final String oggettoCodificato = new String(oggettoUTF8, StandardCharsets.UTF_8);
			documento.setOggetto(oggettoCodificato);
		}
		if (StringUtils.isNotBlank(documento.getNote())) {
			final byte[] noteUTF8 = documento.getNote().getBytes();
			final String noteCodificato = new String(noteUTF8, StandardCharsets.UTF_8);
			documento.setNote(noteCodificato);
		}
	}
	
	/**
	 * Metodo che genera un HTML che elenca i protocolli del flusso FAD in maschera
	 * 
	 * @param classTemplateHtml
	 * @param documento
	 * @return
	 */
	@Override
	public ByteArrayInputStream createReportProtocolliFadHTML(final List<DatiProtocollo> protocolli) {
		ByteArrayInputStream resultHtml = null;
		StringBuilder fileHtml = new StringBuilder();

		try {
			
			fileHtml.append(new String(FileUtils.getFileFromInternalResources("report_protocolli_fad.txt"), StandardCharsets.UTF_8.name()));
			ProvenienzaType pp = null;
			String tp = null;
			for (final DatiProtocollo infoProt: protocolli) {
				
				pp = infoProt.getProvenienza();
				tp = infoProt.getTipoProtocollo();
				switch (pp) {
				case AMMINISTRAZIONE:
					fileHtml = replacePlaceHolder(fileHtml, "{AOO_PROT_AMM}", infoProt.getAoo());
					fileHtml = replacePlaceHolder(fileHtml, "{NUM_PROT_AMM}", "" + infoProt.getNumeroProtocollo());
					fileHtml = replacePlaceHolder(fileHtml, "{DATA_PROT_AMM}", infoProt.getDataProtocollo());
					break;
				case UCB:
					fileHtml = handleUcbPlaceholders(fileHtml, tp, infoProt);
					break;
				case RED:
					fileHtml = replacePlaceHolder(fileHtml, "{AOO_PROT_RED}", infoProt.getAoo());
					fileHtml = replacePlaceHolder(fileHtml, "{NUM_PROT_RED}", "" + infoProt.getNumeroProtocollo());
					fileHtml = replacePlaceHolder(fileHtml, "{DATA_PROT_RED}", infoProt.getDataProtocollo());
					break;

				default:
					break;
				}
				
			}
			
			resultHtml = new ByteArrayInputStream(fileHtml.toString().getBytes(StandardCharsets.UTF_8.name()));
			
		
		} catch (final Exception e) {
			LOGGER.error("Errore nella creazione del file HTML per il report protocolli", e);
		}
		
		return resultHtml;
	}

	/**
	 * Aggiorna il file html sostituendone i placeholder con i valori giusti.
	 * 
	 * @param fileHtml
	 *            StringBuilder dal quale sostituire i placeholders.
	 * @param tp
	 *            Tipologia protocollo.
	 * @param infoProt
	 *            Informazioni per il popolamento dei placeholders.
	 * @return StringBuilder aggiornato.
	 */
	private static StringBuilder handleUcbPlaceholders(StringBuilder fileHtml, String tp, final DatiProtocollo infoProt) {
		
		if (tp.equals(Constants.Protocollo.TIPOLOGIA_PROTOCOLLO_INGRESSO)) {
			fileHtml = replacePlaceHolder(fileHtml, "{AOO_PROT_UCB_INGRESSO}", infoProt.getAoo());
			fileHtml = replacePlaceHolder(fileHtml, "{NUM_PROT_UCB_INGRESSO}", "" + infoProt.getNumeroProtocollo());
			fileHtml = replacePlaceHolder(fileHtml, "{DATA_PROT_UCB_INGRESSO}", infoProt.getDataProtocollo());
		} else if (tp.equals(Constants.Protocollo.TIPOLOGIA_PROTOCOLLO_USCITA)) {
			fileHtml = replacePlaceHolder(fileHtml, "{AOO_PROT_UCB_USCITA}", infoProt.getAoo());
			fileHtml = replacePlaceHolder(fileHtml, "{NUM_PROT_UCB_USCITA}", "" + infoProt.getNumeroProtocollo());
			fileHtml = replacePlaceHolder(fileHtml, "{DATA_PROT_UCB_USCITA}", infoProt.getDataProtocollo());
		}
		return fileHtml;
	}
	
	
	private static StringBuilder replacePlaceHolder(final StringBuilder sb, final String placeHolder, final String newValue) {
		final int startIndexStartString = sb.indexOf(placeHolder);
		if (startIndexStartString < 0) {
			return sb;
		}
		
		final String startString = sb.substring(0, startIndexStartString);
		
		final int startIndexEndString = startIndexStartString + placeHolder.length();
		final String endString = sb.substring(startIndexEndString);
		
		final StringBuilder output = new StringBuilder();
		output.append(startString);
		output.append(newValue);
		output.append(endString);
		
		return output;
	}

	/**
	 * @see it.ibm.red.business.helper.html.IHtmlFileHelper#createTemplateDocUscita(java.lang.StringBuilder,
	 *      java.util.List).
	 */
	@Override
	public ByteArrayInputStream createTemplateDocUscita(final StringBuilder fileHtml, final List<TemplateMetadatoValueDTO> metadati) {
		ByteArrayInputStream resultHtml = null;
		
		try {
			for (final TemplateMetadatoValueDTO metadato : metadati) {
				if(metadato.getValue()!=null && metadato.getValue().length()>metadato.getMaxLength().intValue()) {
					throw new RedException ("Dimensione massima superata per il campo "+metadato.getLabel());
				}
				final int start = fileHtml.indexOf(metadato.getTemplateValue());
				if (start != -1) {
					final int end = start + metadato.getTemplateValue().length();
					String stringToReplace = "";
					if (StringUtils.isNotBlank(metadato.getValue())) {
						if (metadato.isTemplateLabel()) {
							stringToReplace = metadato.getLabel() + ": ";
						}
				
						if (metadato.getTipoMetadato().equals(TipoTemplateMetadatoEnum.TEXTAREA)) {
							if (metadato.isTemplateTesto()) {
								stringToReplace = stringToReplace + (metadato.getEncodedHTMLValue().replace("\n", "</td></tr><tr><td class='testo'>"));
							} else {
								stringToReplace = stringToReplace + (metadato.getEncodedHTMLValue().replace("\n", "<br></br>"));
							}
						} else {
							stringToReplace = stringToReplace + metadato.getEncodedHTMLValue();
						}
					}
					
					if(metadato.getIdMetadatoIntestazione()!=null && metadato.getValue().contains("["+metadato.getIdMetadatoIntestazione()+"]")) {
						for(final TemplateMetadatoValueDTO metadatoInt : metadati){
							if(metadatoInt.getIdMetadato().equals(metadato.getIdMetadatoIntestazione())) {
								stringToReplace = stringToReplace.replace("["+metadato.getIdMetadatoIntestazione()+"]", metadatoInt.getValue());
								break;
							}
						}
						
						
					}
					
					fileHtml.replace(start, end, stringToReplace);
				}
			}
			
			resultHtml = new ByteArrayInputStream(fileHtml.toString().getBytes(StandardCharsets.UTF_8.name()));
		}catch (final RedException e) {
			throw e;
		} 
		catch (final Exception e) {
			LOGGER.error("Errore nella creazione del file HTML per il documento in uscita da template", e);
		}
		
		return resultHtml;
	}
	
	/**
	 * @see it.ibm.red.business.helper.html.IHtmlFileHelper#creaEmailHtml(it.ibm.red.business.template.TemplateDocumento,
	 *      it.ibm.red.business.dto.DetailEmailDTO).
	 */
	@Override
	public ByteArrayInputStream creaEmailHtml(final TemplateDocumento templateDocumento, final DetailEmailDTO email) {
		ByteArrayInputStream resultHtml = null;
		final StringBuilder fileHtml = new StringBuilder();
		final String idDocumento = email.getGuid();
		
		try {
			final String htmlDocumentoPart1 = new String(FileUtils.getFileFromInternalResources(TEMPLATE_DOCUMENTO_PART_1), StandardCharsets.UTF_8.name());
			final String htmlPart3 = new String(FileUtils.getFileFromInternalResources(TEMPLATE_PART_3), StandardCharsets.UTF_8.name());
			
			final String part2FileName = "<br/> " 
					+ " <div id=\"pJHP56-chdex\" class=\"z-vlayout-inner\" style=\"padding-bottom:5px\"> "
					+ "  <div id=\"pJHP76\" class=\"z-tabs z-tabs-scroll\" style=\"width: 660px;\"> "
					+ "   <ul id=\"pJHP76-cave\" class=\"z-tabs-cnt\"> "
					+ "    <li id=\"pJHP86\" class=\"z-tab z-tab-seld\"> "
					+ "     <div id=\"pJHP86-hl\" class=\"z-tab-hl\"> "
					+ "      <div id=\"pJHP86-hr\" class=\"z-tab-hr\"> "
					+ "       <div id=\"pJHP86-hm\" class=\"z-tab-hm \"> "
					+ "        <span class=\"scriptaSection\">Dettaglio Email</span> "
					+ "       </div> "
					+ "      </div> "
					+ "     </div> "
					+ "    </li> "
					+ "   </ul> "
					+ "  </div> "
					+ " </div> " 
					+ " <div id=\"sCJQzd\" class=\"z-tabpanel\"> " 
					+ " <div id=\"sCJQzd-cave\" class=\"z-tabpanel-cnt\">"; 
			
			final String htmlDocumentoPart2 = part2FileName;
			  
			// Si codificano i campi di testo libero del documento in HTML 
			codificaCampiEmailInHtml(email);
		
			// Header
			fileHtml.append(htmlDocumentoPart1);
			// Tabella di header
			fileHtml.append(templateDocumento.getHeaderTableEmail(email));
			// Seconda parte 
			fileHtml.append(htmlDocumentoPart2);
			// Tabella corpo Richiesta
			fileHtml.append(TABLE_BORDER_0_STYLE_WIDTH_650PX);
			// Dettaglio del documento
			fileHtml.append(templateDocumento.getDettaglioDocumentoTableEmail(email));

			// Chiusura tabella
			fileHtml.append(TABLE_CLOSE_TAG);
			// Parte comune di footer
			fileHtml.append(htmlPart3);
			
			resultHtml = new ByteArrayInputStream(fileHtml.toString().getBytes(StandardCharsets.UTF_8.name()));
			
			LOGGER.info(SUCCESS_HTML_GENERATO_MSG + idDocumento);
		} catch (final Exception e) {
			LOGGER.error(ERROR_CREAZIONE_HTML_MSG + idDocumento, e);
		}
		
		return resultHtml;
	}
	
	/**
	 * @see it.ibm.red.business.helper.html.IHtmlFileHelper#creaEmailHtmlPlainText(it.ibm.red.business.template.TemplateDocumento,
	 *      it.ibm.red.business.dto.DetailEmailDTO).
	 */
	@Override
	public ByteArrayInputStream creaEmailHtmlPlainText(final TemplateDocumento templateDocumento, final DetailEmailDTO email) {
		ByteArrayInputStream resultHtml = null;
		final StringBuilder fileHtml = new StringBuilder();
		final String idDocumento = email.getGuid();
		
		try {
			final String htmlDocumentoPart1 = new String(FileUtils.getFileFromInternalResources(TEMPLATE_DOCUMENTO_PART_1), StandardCharsets.UTF_8.name());
			final String htmlPart3 = new String(FileUtils.getFileFromInternalResources(TEMPLATE_PART_3), StandardCharsets.UTF_8.name());
			
			final String part2FileName = "<br/> " 
					+ " <div id=\"pJHP56-chdex\" class=\"z-vlayout-inner\" style=\"padding-bottom:5px\"> " 
					+ "  <div id=\"pJHP76\" class=\"z-tabs z-tabs-scroll\" style=\"width: 660px;\"> " 
					+ "   <ul id=\"pJHP76-cave\" class=\"z-tabs-cnt\"> " 
					+ "    <li id=\"pJHP86\" class=\"z-tab z-tab-seld\"> " 
					+ "     <div id=\"pJHP86-hl\" class=\"z-tab-hl\"> " 
					+ "      <div id=\"pJHP86-hr\" class=\"z-tab-hr\"> " 
					+ "       <div id=\"pJHP86-hm\" class=\"z-tab-hm \"> " 
					+ "        <span class=\"scriptaSection\">Dettaglio Email</span> " 
					+ "       </div> " 
					+ "      </div> " 
					+ "     </div> " 
					+ "    </li> " 
					+ "   </ul> " 
					+ "  </div> " 
					+  " </div> "
					+ " <div id=\"sCJQzd\" class=\"z-tabpanel\"> " 
					+ " <div id=\"sCJQzd-cave\" class=\"z-tabpanel-cnt\">"; 
			
			final String htmlDocumentoPart2 = part2FileName;
			  
			// Si codificano i campi di testo libero del documento in HTML 
			codificaCampiEmailInHtml(email);
		
			// Header
			fileHtml.append(htmlDocumentoPart1);
			// Tabella di header
			fileHtml.append(templateDocumento.getHeaderTableEmail(email));
			// Seconda parte 
			fileHtml.append(htmlDocumentoPart2);
			// Tabella corpo Richiesta
			fileHtml.append(TABLE_BORDER_0_STYLE_WIDTH_650PX);
			// Dettaglio del documento
			fileHtml.append(templateDocumento.getDettaglioDocumentoTableEmailPlainText(email));

			// Chiusura tabella
			fileHtml.append(TABLE_CLOSE_TAG);
			// Parte comune di footer
			fileHtml.append(htmlPart3);
			
			resultHtml = new ByteArrayInputStream(fileHtml.toString().getBytes(StandardCharsets.UTF_8.name()));
			
			LOGGER.info(SUCCESS_HTML_GENERATO_MSG + idDocumento);
		} catch (final Exception e) {
			LOGGER.error(ERROR_CREAZIONE_HTML_MSG + idDocumento, e);
		}
		
		return resultHtml;
	}
	
	private void codificaCampiEmailInHtml(final DetailEmailDTO email) {
		try {
			if (StringUtils.isNotBlank(email.getOggetto())) { 
				final byte[] oggettoUTF8 = email.getOggetto().getBytes();
				final String oggettoCodificato = new String(oggettoUTF8, StandardCharsets.UTF_8);
				email.setOggetto(oggettoCodificato);
			}

			if (StringUtils.isNotBlank(email.getDataArrivo())) {
				final byte[] dataArrivoUTF8 = email.getDataArrivo().getBytes();
				final String dataArrivoCodificato = new String(dataArrivoUTF8, StandardCharsets.UTF_8);
				email.setDataArrivo(dataArrivoCodificato);
			}

			if (StringUtils.isNotBlank(email.getMittente())) {
				final byte[] mittenteUTF8 = email.getMittente().getBytes();
				final String mittenteCodificato = new String(mittenteUTF8, StandardCharsets.UTF_8);
				email.setMittente(mittenteCodificato);
			}

			if (StringUtils.isNotBlank(email.getTesto())) {
				final byte[] testoUTF8 = email.getTesto().getBytes();
				final String testoCodificato = new String(testoUTF8, StandardCharsets.UTF_8);
				email.setTesto(testoCodificato);
			}
		} catch (final Exception ex) {
			LOGGER.error("Errore durante la codifica dei campi html : " + ex);
			throw new RedException("Errore durante la codifica dei campi html : " + ex);
		}
	}
	
	/**
     * Effettua il parsing del file html eseguendo diverse fix.
     * @param dirtyHtml html su cui fare le fix
     * @param showWarnings se true, verranno mostrati i messaggi di warning di Tidy in console
     * @return byte array del file html pulito
	 * @throws UnsupportedEncodingException 
     */
    public static byte[] quickFix(byte[] dirtyHtml, boolean showWarnings) throws UnsupportedEncodingException {
    	InputStream stream = new ByteArrayInputStream(dirtyHtml);
		Tidy tidyParser = new Tidy();
		tidyParser.setFixBackslash(true);
		tidyParser.setXHTML(true);
		tidyParser.setDropFontTags(true);
		tidyParser.setEncloseBlockText(true);
		tidyParser.setIndentContent(true);
		tidyParser.setQuoteNbsp(true);
		tidyParser.setMakeClean(true);
		tidyParser.setCharEncoding(Configuration.UTF8);
		
		// Modifica preferenze
		tidyParser.setQuiet(!showWarnings);
		tidyParser.setShowWarnings(showWarnings);
		tidyParser.setTidyMark(false);
		
		ByteArrayOutputStream cleanBOS = new ByteArrayOutputStream();
		tidyParser.parse(stream, cleanBOS);
		return cleanBOS.toString(StandardCharsets.UTF_8.name()).getBytes(StandardCharsets.UTF_8.name());
    }
    
    /**
	 * Modifica il flusso html in ingresso andando a rimpiazzare le stringhe definite dalle properties.
	 * Consente di modificare il file html per la trasformazione in pdf nell'ambito del flusso SIPAD.
	 * @param html il file da modificare
	 * @return byte array modificato
	 */
	public static byte[] replaceTextFromProperties (byte [] html)  {
		String temp = new String(html, StandardCharsets.UTF_8);
		boolean hasProperties = true;
		PropertiesProvider pp = PropertiesProvider.getIstance();
		int i = 1;
		while (hasProperties) {
			try {
				String currentProperty = pp.getParameterByString(PropertiesNameEnum.FLUSSO_SIPAD_REPLACE_CUSTOM_TEXT.getKey() + "." + i);
				
				String[] replacements = StringUtils.split(currentProperty, "|");
				String oldText = replacements[0];
				String newText = replacements[1];
				temp = temp.replace(oldText, newText);

				i++;
			} catch (Exception e) {
				LOGGER.error("Property non esistente.", e);
				hasProperties = false;
			}
		}
		
		return temp.getBytes(StandardCharsets.UTF_8);
	}
    
}