package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.collection.RepositoryRowSet;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.Document;
import com.filenet.api.query.RepositoryRow;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.IFaldoneDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.MasterFascicoloDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.FascicoloRedFnDTO;
import it.ibm.red.business.enums.EsitoFaldoneEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.VisibilitaFaldoniEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.exception.SearchException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IDocumentoSRV;
import it.ibm.red.business.service.IFaldoneSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class FaldoneSRV.
 *
 * @author mcrescentini
 * 
 *         Servizio gestione faldone.
 */
@Service
public class FaldoneSRV extends AbstractService implements IFaldoneSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 2281054311008918487L;

	/**
	 * LITERAL riferimento faldone.
	 */
	private static final String DAL_FALDONE_LITERAL = " dal faldone: ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FaldoneSRV.class.getName());

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * Service.
	 */
	@Autowired
	private IDocumentoSRV documentoSRV;

	/**
	 * Dao.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private IFaldoneDAO faldoneDAO;

	/**
	 * Padre fittizio albero.
	 */
	private final FaldoneDTO padreFittizio = new FaldoneDTO("0", null, "fittizio", null, "Faldoni Inconsistenti", "Faldoni Inconsistenti");

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFaldoneFacadeSRV#faldona(java.lang.String,
	 *      java.util.List, it.ibm.red.business.dto.UtenteDTO, java.lang.String).
	 */
	@Override
	public List<EsitoOperazioneDTO> faldona(final String nomeFaldone, final List<String> wobNumbers, final UtenteDTO utente, final String idFascicolo) {
		final List<EsitoOperazioneDTO> esiti = new ArrayList<>();
		if (!CollectionUtils.isEmpty(wobNumbers)) {
			for (final String wobNumber : wobNumbers) {
				esiti.add(faldona(utente, nomeFaldone, wobNumber, idFascicolo));
			}
		}
		return esiti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFaldoneFacadeSRV#faldona(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.lang.String).
	 */
	@Override
	public EsitoOperazioneDTO faldona(final UtenteDTO utente, final String nomeFaldone, final String wobNumber, final String idFascicolo) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;

		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final VWWorkObject wo = fpeh.getWorkFlowByWob(wobNumber, false);
			final Integer idDoc = (Integer) wo.getFieldValue(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));

			final Integer numeroDoc = fceh.getNumDocByDocumentTitle(idDoc.toString());
			faldonaFascicolo(nomeFaldone, idFascicolo, utente.getIdAoo(), fceh);

			esito.setIdDocumento(numeroDoc);
			esito.setEsito(true);
			esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
		} catch (final Exception e) {
			LOGGER.error(e);
			esito.setNote("Si è verificato un errore durante la faldonatura.");
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}

		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.IFaldoneSRV#faldonaFascicoli(java.lang.String,
	 *      java.util.List, java.lang.Long,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public void faldonaFascicoli(final String nomeFaldone, final List<FascicoloRedFnDTO> fascicoli, final Long idAoo, final IFilenetCEHelper fceh) {
		LOGGER.info("faldonaFascicoli -> START. Faldone: " + nomeFaldone);
		if (!CollectionUtils.isEmpty(fascicoli)) {
			for (final FascicoloRedFnDTO fascicolo : fascicoli) {
				faldonaFascicolo(nomeFaldone, fascicolo.getIdFascicolo(), idAoo, fceh);
			}
		}
		LOGGER.info("faldonaFascicoli -> END. Faldone: " + nomeFaldone);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFaldoneFacadeSRV#faldonaFascicolo(java.lang.String,
	 *      java.lang.String, java.lang.Long, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public boolean faldonaFascicolo(final String nomeFaldone, final String idFascicolo, final Long idAoo, final UtenteDTO utente) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			return faldonaFascicolo(nomeFaldone, idFascicolo, idAoo, fceh);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IFaldoneSRV#faldonaFascicolo(java.lang.String,
	 *      java.lang.String, java.lang.Long,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public boolean faldonaFascicolo(final String nomeFaldone, final String idFascicolo, final Long idAoo, final IFilenetCEHelper fceh) {
		LOGGER.info("faldonaFascicolo -> START. Inserimento del fascicolo: " + idFascicolo + " nel faldone: " + nomeFaldone);
		// Si controlla se il fascicolo è già faldonato nel faldone selezionato
		boolean checkFascicoloFaldonato = false;

		final DocumentSet faldoniFascicoloFilenet = fceh.getFaldoniFascicolo(idFascicolo, idAoo);
		if (faldoniFascicoloFilenet != null && !faldoniFascicoloFilenet.isEmpty()) {
			final Iterator<?> it = faldoniFascicoloFilenet.iterator();
			while (it.hasNext()) {
				final FaldoneDTO faldoneFascicolo = TrasformCE.transform((Document) it.next(), TrasformerCEEnum.FROM_DOCUMENT_TO_FALDONE);
				if (faldoneFascicolo.getNomeFaldone().equals(nomeFaldone)) {
					checkFascicoloFaldonato = true;
					break;
				}
			}
		}

		if (!checkFascicoloFaldonato) {
			// Si associa il fascicolo al faldone su FileNet
			LOGGER.info("faldonaFascicolo -> Inserimento del fascicolo: " + idFascicolo + " nel faldone: " + nomeFaldone + " in corso...");
			fceh.associaFascicoloAFaldone(idFascicolo, nomeFaldone, idAoo);
			LOGGER.info("faldonaFascicolo -> Il fascicolo: " + idFascicolo + " è stato inserito nel faldone: " + nomeFaldone);
		} else {
			LOGGER.info("faldonaFascicolo -> Il fascicolo: " + idFascicolo + " risulta già faldonato nel faldone: " + nomeFaldone);
		}

		LOGGER.info("faldonaFascicolo -> END");
		return !checkFascicoloFaldonato;
	}

	private FaldoneDTO getFaldoneByDT(final UtenteDTO utente, final String documentTitle, final Boolean buildGerarchia, final IFilenetCEHelper fcehAdmin) {
		final Document f = fcehAdmin.getFaldoneByDocumentTitle(documentTitle, utente.getIdAoo().intValue());

		if (f != null || !buildGerarchia) {
			return TrasformCE.transform(f, TrasformerCEEnum.FROM_DOCUMENT_TO_FALDONE_TREE);
		} else {
			return padreFittizio;
		}
	}

	@Override
	public final FaldoneDTO getFaldone(final String documentTitle, final UtenteDTO utente) {
		final FaldoneDTO faldone = null;
		IFilenetCEHelper fcehAdmin = null;

		try {
			fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());

			getFaldone(documentTitle, utente.getIdAoo(), fcehAdmin);
		} finally {
			popSubject(fcehAdmin);
		}

		return faldone;
	}

	@Override
	public final FaldoneDTO getFaldone(final String documentTitle, final Long idAoo, final IFilenetCEHelper fcehAdmin) {
		FaldoneDTO faldone = null;

		final Document faldoneFilenet = fcehAdmin.getFaldoneByDocumentTitle(documentTitle, idAoo.intValue());
		if (faldoneFilenet != null) {
			faldone = TrasformCE.transform(faldoneFilenet, TrasformerCEEnum.FROM_DOCUMENT_TO_FALDONE_TREE);
		}

		return faldone;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFaldoneFacadeSRV#getFaldoni(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.Integer, java.lang.String).
	 */
	@Override
	public Collection<FaldoneDTO> getFaldoni(final UtenteDTO utente, final Integer idUfficioSelezionato, final String parentName) {
		IFilenetCEHelper fcehAdmin = null;
		try {
			fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());
			return TrasformCE.transform(getFaldoniDS(utente, idUfficioSelezionato, parentName, fcehAdmin), TrasformerCEEnum.FROM_DOCUMENT_TO_FALDONE_TREE);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fcehAdmin);
		}
	}

	private DocumentSet getFaldoniDS(final UtenteDTO utente, final Integer idUfficioSelezionato, final String documentTitle, final IFilenetCEHelper inFcehAdmin) {
		Connection con = null;
		IFilenetCEHelper fcehAdmin = inFcehAdmin;
		final boolean needsToPopSubject = fcehAdmin == null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final Long idUfficioCorriereUtente = utente.getIdNodoCorriere();
			final VisibilitaFaldoniEnum vfAoo = VisibilitaFaldoniEnum.get(aooDAO.getVisFaldoni(utente.getIdAoo(), con));
			final Boolean bAooVisTotale = VisibilitaFaldoniEnum.TOTALE.equals(vfAoo);
			final Long idUfficioCorriereSelezionato = nodoDAO.getNodoCorriereId(Long.valueOf(idUfficioSelezionato), con);
			if (fcehAdmin == null) {
				fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());
			}
			final Boolean aclCorriere = true;
			final Boolean aclNodo = true;
			final Boolean aclUtente = false;

			return fcehAdmin.getFaldoneChildren(utente, documentTitle, bAooVisTotale, idUfficioCorriereUtente.intValue(), idUfficioSelezionato,
					idUfficioCorriereSelezionato.intValue(), utente.getGruppiAD(), aclCorriere, aclNodo, aclUtente);
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
			if (needsToPopSubject) {
				popSubject(fcehAdmin);
			}
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFaldoneFacadeSRV#getFascicoliFaldone(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	public Collection<MasterFascicoloDTO> getFascicoliFaldone(final UtenteDTO utente, final String pathFaldone) {
		final RepositoryRowSet rrs = getFascicoliFaldoneDS(utente, pathFaldone);

		final Collection<MasterFascicoloDTO> output = new ArrayList<>();
		final Iterator<?> it = rrs.iterator();
		while (it.hasNext()) {
			final RepositoryRow childLink = (RepositoryRow) it.next();
			final String id = childLink.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY));
			final String oggetto = childLink.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY));
			final Date dataCreazione = childLink.getProperties().getDateTimeValue(PropertyNames.DATE_CREATED);
			final Long idAOO = Long.valueOf(((Integer) childLink.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY))));
			final MasterFascicoloDTO m = new MasterFascicoloDTO(Integer.valueOf(id), oggetto, dataCreazione, idAOO);
			output.add(m);
		}

		LOGGER.info("getFascicoliByFaldone 3 " + output.size());
		return output;
	}

	private static RepositoryRowSet getFascicoliFaldoneDS(final UtenteDTO utente, final String pathFaldone) {
		IFilenetCEHelper fcehAdmin = null;

		try {
			fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());

			return fcehAdmin.getFascicoliByFaldone(pathFaldone, utente.getGruppiAD());
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			popSubject(fcehAdmin);
		}
	}

	private boolean canDelete(final UtenteDTO utente, final Integer idUfficioSelezionato, final String documentTitle, final String pathFaldone) {
		boolean output = true;
		final DocumentSet dsFaldoni = getFaldoniDS(utente, idUfficioSelezionato, documentTitle, null);
		final Iterator<?> itFaldoni = dsFaldoni.iterator();
		if (itFaldoni.hasNext()) {
			output = false;
		} else {
			final RepositoryRowSet rrsFascicoliFaldone = getFascicoliFaldoneDS(utente, pathFaldone);
			final Iterator<?> itFascicoliFaldone = rrsFascicoliFaldone.iterator();
			if (itFascicoliFaldone.hasNext()) {
				output = false;
			}
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFaldoneFacadeSRV#delete(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.Integer, java.lang.String, java.lang.String).
	 */
	@Override
	public void delete(final UtenteDTO utente, final Integer idUfficioSelezionato, final String pathFaldone, final String documentTitle) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			delete(utente, idUfficioSelezionato, pathFaldone, documentTitle, fceh);

		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IFaldoneSRV#delete(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.Integer, java.lang.String, java.lang.String,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public void delete(final UtenteDTO utente, final Integer idUfficioSelezionato, final String pathFaldone, final String documentTitle, final IFilenetCEHelper fceh) {
		if (canDelete(utente, idUfficioSelezionato, documentTitle, pathFaldone)) {
			try {
				fceh.deleteFaldone(utente.getIdAoo(), documentTitle);
			} catch (final Exception e) {
				LOGGER.error(e);
				throw e;
			}
		} else {
			final String msg = "Impossibile procedere con la cancellazione: il faldone non è vuoto";
			LOGGER.error(msg);
			throw new RedException(msg);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFaldoneFacadeSRV#createFaldone(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.Boolean).
	 */
	@Override
	public String createFaldone(final UtenteDTO utente, final String oggetto, final String descrizione, final String faldonePadre, final Boolean canBeDuplicate) {
		Connection con = null;
		IFilenetCEHelper fceh = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			return createFaldone(utente, oggetto, descrizione, faldonePadre, canBeDuplicate, con, fceh);
		} catch (final RedException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Si è verificato un errore durante la creazione del faldone.");
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IFaldoneSRV#createFaldone(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.Boolean,
	 *      java.sql.Connection,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public String createFaldone(final UtenteDTO utente, final String oggetto, final String descrizione, final String faldonePadre, final Boolean canBeDuplicate,
			final Connection con, final IFilenetCEHelper fceh) {

		if (Boolean.FALSE.equals(canBeDuplicate) && checkOggettoFaldoneEsistente(oggetto, utente, fceh, null)) {
			throw new RedException("Esiste già un faldone con lo stesso oggetto");
		}

		final String nome = Constants.EMPTY_STRING + faldoneDAO.getNextIdFaldone(utente.getIdAoo().intValue(), con);
		fceh.createFaldone(utente.getIdAoo().intValue(), utente.getIdUfficio().intValue(), nome, faldonePadre, oggetto, descrizione);

		return nome;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFaldoneFacadeSRV#updateFaldone(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.Boolean).
	 */
	@Override
	public void updateFaldone(final UtenteDTO utente, final String documentTitle, final String nuovoOggetto, final String nuovaDescrizione, final Boolean canBeDuplicate) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final Map<String, Object> metadati = new HashMap<>();

			if (Boolean.FALSE.equals(canBeDuplicate) && checkOggettoFaldoneEsistente(nuovoOggetto, utente, fceh, documentTitle)) {
				throw new RedException("Esiste già un faldone con lo stesso oggetto");
			}
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_OGGETTO), nuovoOggetto);
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_DESCRIZIONEFALDONE), nuovaDescrizione);
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_IDAOO), utente.getIdAoo());

			fceh.updateMetadatiFaldone(utente.getIdAoo(), documentTitle, metadati);

		} catch (final Exception e) {
			LOGGER.error("Errore durante l'aggiornamento di un faldone: " + e.getMessage(), e);
			throw new RedException("Errore durante l'aggiornamento di un faldone", e);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @param oggettoNuovoFaldone
	 * @param utente
	 * @param fceh
	 * @param documentTitleFaldoneInModifica
	 *            è null se sono in creazione altrimenti si riferisce al
	 *            documentTitle del faldone che si sta modificando.
	 * @return
	 * @throws Exception
	 */
	private boolean checkOggettoFaldoneEsistente(final String oggettoNuovoFaldone, final UtenteDTO utente, final IFilenetCEHelper fceh,
			final String documentTitleFaldoneInModifica) {
		boolean isEsistente = false;

		try {
			final Long idUtente = utente.getId();
			final Long idUfficioUtente = utente.getIdUfficio();
			final String utenteUsername = utente.getUsername();
			final Long idUfficioCorriereUtente = utente.getIdNodoCorriere();
			final DocumentSet documents = fceh.ricercaGenericaFaldoni(oggettoNuovoFaldone, RicercaGenericaTypeEnum.ESATTA, utente.getIdAoo(), null, idUfficioUtente,
					idUfficioCorriereUtente.intValue(), idUtente, utenteUsername, utente.getVisFaldoni(), true);

			final Collection<FaldoneDTO> listaFaldoni = TrasformCE.transform(documents, TrasformerCEEnum.FROM_DOCUMENT_TO_FALDONE);

			if (listaFaldoni != null && !listaFaldoni.isEmpty()) {
				for (final FaldoneDTO fald : listaFaldoni) {
					if (!StringUtils.isNullOrEmpty(documentTitleFaldoneInModifica) && documentTitleFaldoneInModifica.equals(fald.getDocumentTitle())) {
						// se sto confrontando il faldone con se stesso devo continuare perché potrei
						// cambiare la descrizione, l'oggetto rimane quindi lo stesso
						continue;
					}

					// altrimenti confronto l'oggetto
					if (fald.getOggetto().equals(oggettoNuovoFaldone)) {
						isEsistente = true;
						break;
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la ricerca di faldoni con lo stesso oggetto " + e.getMessage(), e);
			throw new RedException("Errore durante la ricerca di faldoni con lo stesso oggetto ");
		}

		return isEsistente;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFaldoneFacadeSRV#ricercaFaldoni
	 *      (java.lang.String, it.ibm.red.business.enums.RicercaGenericaTypeEnum,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<FaldoneDTO> ricercaFaldoni(final String key, final RicercaGenericaTypeEnum type, final UtenteDTO utente) throws SearchException {
		DocumentSet documents = null;
		Collection<FaldoneDTO> documentiCE = null;
		// Considero solo i campi che sono stati valorizzati per la ricerca
		if (type == null) {
			throw new SearchException("Il tipo di ricerca non è stato valorizzato correttamente.");
		}

		IFilenetCEHelper fceh = null;
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			documents = fceh.ricercaGenericaFaldoni(key, type, utente.getIdAoo(), null, utente.getIdUfficio(), utente.getIdNodoCorriere().intValue(), utente.getId(),
					utente.getUsername(), utente.getVisFaldoni(), false);

			documentiCE = TrasformCE.transform(documents, TrasformerCEEnum.FROM_DOCUMENT_TO_FALDONE);
		} catch (final Exception e) {
			LOGGER.error("Errore durante la ricerca generica dei faldoni: " + e.getMessage(), e);
			throw new SearchException("Errore durante la ricerca generica dei faldoni: " + e.getMessage());
		} finally {
			popSubject(fceh);
		}

		return documentiCE;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFaldoneFacadeSRV#getGerarchia
	 *      (java.util.Collection, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<FaldoneDTO> getGerarchia(final Collection<FaldoneDTO> listaFaldoni, final UtenteDTO utente) {
		final Collection<FaldoneDTO> gerarchia = new ArrayList<>();
		IFilenetCEHelper fcehAdmin = null;

		try {
			// va creata la connection a Filenet prima di recuperare la gerarchia dei
			// risultati dei faldoni
			fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());

			for (final FaldoneDTO faldone : listaFaldoni) {
				addGerarchiaFaldone(faldone, gerarchia, utente, fcehAdmin);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			popSubject(fcehAdmin);
		}

		return gerarchia;
	}

	private void addGerarchiaFaldone(final FaldoneDTO inFaldone, final Collection<FaldoneDTO> gerarchiaEsistente, final UtenteDTO utente, final IFilenetCEHelper fcehAdmin) {
		FaldoneDTO faldone = inFaldone;
		String parentFaldone = faldone.getParentFaldone();
		FaldoneDTO padre;
		// add faldone alla gerarchia
		gerarchiaEsistente.add(faldone);
		// fino a quando c'è un padre, aggiungilo alla gerarchia (a meno che non sia già
		// presente)
		while (parentFaldone != null) {
			padre = getFaldoneByDT(utente, parentFaldone, true, fcehAdmin);
			if (padre.getDescrizioneFaldone() == null || !padre.getDescrizioneFaldone().equalsIgnoreCase(padreFittizio.getDescrizioneFaldone())) {
				faldone = padre;
			} else {
				// il padre è il pozzo
				faldone = padreFittizio;
			}
			parentFaldone = faldone.getParentFaldone();
			if (!gerarchiaEsistente.contains(faldone)) {
				gerarchiaEsistente.add(faldone);
			}
		}
	}

	/**
	 * @see it.ibm.red.business.service.IFaldoneSRV#getGerarchiaFaldone(it.ibm.red.business.dto.FaldoneDTO,
	 *      it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public String getGerarchiaFaldone(final FaldoneDTO faldone, final UtenteDTO utente, final IFilenetCEHelper fcehAdmin) {
		String parentFaldone = faldone.getParentFaldone();
		FaldoneDTO padre = null;
		// viene impostato come path di partenza l'oggetto del faldone in ingresso
		final StringBuilder sb = new StringBuilder(faldone.getOggetto());

		// fino a quando c'è un padre viene aggiunto alla radice della gerarchia
		while (parentFaldone != null) {
			padre = getFaldoneByDT(utente, parentFaldone, true, fcehAdmin);
			if (!StringUtils.isNullOrEmpty(padre.getOggetto()) && !padreFittizio.getDescrizioneFaldone().equalsIgnoreCase(padre.getDescrizioneFaldone())) {
				sb.insert(0, padre.getOggetto() + " / ");
			}
			parentFaldone = padre.getParentFaldone();
		}

		return sb.toString();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFaldoneFacadeSRV#disassociaFascicoloDaFaldone(java.lang.String,
	 *      java.lang.String, java.lang.Long, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public final EsitoFaldoneEnum disassociaFascicoloDaFaldone(final String nomeFaldone, final String idFascicolo, final Long idAOO, final UtenteDTO utente) {
		LOGGER.info("disassociaFascicoloDaFaldone -> START. Disassociazione del fascicolo: " + idFascicolo + DAL_FALDONE_LITERAL + nomeFaldone);
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			return disassociaFascicoloDaFaldone(nomeFaldone, idFascicolo, idAOO, fceh);
		} catch (final Exception e) {
			LOGGER.error(
					"disassociaFascicoloDaFaldone -> Si è verificato un errore durante la disassociazione del fascicolo: " + idFascicolo + DAL_FALDONE_LITERAL + nomeFaldone,
					e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IFaldoneSRV#disassociaFascicoloDaFaldone(java.lang.String,
	 *      java.lang.String, java.lang.Long,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public final EsitoFaldoneEnum disassociaFascicoloDaFaldone(final String nomeFaldone, final String idFascicolo, final Long idAOO, final IFilenetCEHelper fceh) {
		return disassociaFascicoloDaFaldone(nomeFaldone, idFascicolo, idAOO, false, fceh);
	}

	/**
	 * @see it.ibm.red.business.service.IFaldoneSRV#disassociaFascicoloDaFaldone(java.lang.String,
	 *      java.lang.String, java.lang.Long, boolean,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public final EsitoFaldoneEnum disassociaFascicoloDaFaldone(final String nomeFaldone, final String idFascicolo, final Long idAOO, final boolean checkDisassociazione,
			final IFilenetCEHelper fceh) {
		LOGGER.info("disassociaFascicoloDaFaldone -> START. Disassociazione del fascicolo: " + idFascicolo + DAL_FALDONE_LITERAL + nomeFaldone);
		EsitoFaldoneEnum esito = null;
		try {

			if (checkDisassociazione && !isFaldoneDisassociabile(nomeFaldone, idFascicolo, idAOO, fceh)) {
				return EsitoFaldoneEnum.FALDONE_NON_DISASSOCIABILE;
			}

			fceh.disassociaFascicoloDaFaldone(idFascicolo, nomeFaldone, idAOO);
			esito = EsitoFaldoneEnum.FALDONE_DISASSOCIATO_DA_FASCICOLO_CON_SUCCESSO;
		} catch (final Exception e) {
			LOGGER.error(
					"disassociaFascicoloDaFaldone -> Si è verificato un errore durante la disassociazione del fascicolo: " + idFascicolo + DAL_FALDONE_LITERAL + nomeFaldone,
					e);
			throw new RedException(e);
		}
		LOGGER.info("disassociaFascicoloDaFaldone -> END.");
		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFaldoneFacadeSRV#isFaldoneDisassociabile(java.lang.String,
	 *      java.lang.String, java.lang.Long, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public boolean isFaldoneDisassociabile(final String nomeFaldone, final String idFascicolo, final Long idAOO, final UtenteDTO utente) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			return isFaldoneDisassociabile(nomeFaldone, idFascicolo, idAOO, fceh);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IFaldoneSRV#isFaldoneDisassociabile(java.lang.String,
	 *      java.lang.String, java.lang.Long,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public boolean isFaldoneDisassociabile(final String nomeFaldone, final String idFascicolo, final Long idAOO, final IFilenetCEHelper fceh) {
		boolean isFaldoneDisassociabile = true;

		try {
			// Si recuperano i documenti contenuti nel fascicolo
			final DocumentSet documentiFascicolo = fceh.getDocumentiFascicolo(Integer.parseInt(idFascicolo), idAOO);

			final Iterator<?> itDocumentiFascicolo = documentiFascicolo.iterator();
			while (itDocumentiFascicolo.hasNext()) {
				final Document documentoFascicolo = (Document) itDocumentiFascicolo.next();
				final Integer idTipologiaDocumento = (Integer) TrasformerCE.getMetadato(documentoFascicolo, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
				final Integer idTipoProcedimento = (Integer) TrasformerCE.getMetadato(documentoFascicolo, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);

				// Si verifica se il documento deve essere obbligatoriamente faldonato
				// (faldonatura obbligatoria)
				final boolean isDocumentoFaldonaturaObbligatoria = documentoSRV.isDocumentoFaldonaturaObbligatoria(idTipologiaDocumento, idTipoProcedimento);

				if (isDocumentoFaldonaturaObbligatoria) {
					// Si recupera il numero di faldoni a cui è associato il fascicolo
					Integer numeroFaldoniFascicolo = 0;
					final Iterator<?> itFaldoniFascicolo = fceh.getFaldoniFascicolo(idFascicolo, idAOO).iterator();
					while (itFaldoniFascicolo.hasNext()) {
						numeroFaldoniFascicolo++;
					}

					if (numeroFaldoniFascicolo < 2) {
						isFaldoneDisassociabile = false;
						break;
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("isFaldoneDisassociabile -> Si è verificato un errore durante la verifica di fattibilità per la disassociazione del fascicolo: " + idFascicolo
					+ DAL_FALDONE_LITERAL + nomeFaldone, e);
			throw new RedException(e);
		}

		return isFaldoneDisassociabile;
	}

	/**
	 * @see it.ibm.red.business.service.IFaldoneSRV#getFaldoneOPFIgepa(java.lang.String,
	 *      java.lang.String, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public String getFaldoneOPFIgepa(final String oggettoFaldone, final String annoFaldone, final UtenteDTO utente) {
		String nomeFaldone = Constants.EMPTY_STRING;
		String nomeFaldonePadre = Constants.EMPTY_STRING;
		IFilenetCEHelper fcehAdmin = null;

		try {
			final FilenetCredentialsDTO fcUtente = utente.getFcDTO();
			final FilenetCredentialsDTO fcAdmin = new FilenetCredentialsDTO(fcUtente);
			fcehAdmin = FilenetCEHelperProxy.newInstance(fcAdmin, utente.getIdAoo());

			// Si recupera il faldone padre
			final Document faldonePadre = fcehAdmin.getFaldoneByMetadato(pp.getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_OGGETTO), oggettoFaldone,
					utente.getGruppiAD());
			// Se il faldone padre esiste...
			if (faldonePadre != null) {
				nomeFaldonePadre = (String) TrasformerCE.getMetadato(faldonePadre, pp.getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_NOMEFALDONE));
				LOGGER.info("getFaldoneOPFIgepa -> Faldone padre recuperato: " + nomeFaldonePadre);

				// Si recuperano i figli del faldone
				final DocumentSet faldoniFigli = fcehAdmin.getFaldoniFigli(nomeFaldonePadre, utente.getGruppiAD(), utente.getIdAoo(), null);

				// Si cerca il faldone figlio corretto (es. 2014)
				if (faldoniFigli != null && !faldoniFigli.isEmpty()) {
					final Iterator<?> itFaldoniFigli = faldoniFigli.iterator();
					while (itFaldoniFigli.hasNext()) {
						final Document faldoneFiglio = (Document) itFaldoniFigli.next();
						final String oggettoFaldoneFiglio = (String) TrasformerCE.getMetadato(faldoneFiglio,
								pp.getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_OGGETTO));
						if (annoFaldone.equals(oggettoFaldoneFiglio)) {
							nomeFaldone = (String) TrasformerCE.getMetadato(faldoneFiglio, pp.getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_NOMEFALDONE));
							break;
						}
					}
				}

				// Se il faldone figlio corretto non esiste, lo si crea
				if (StringUtils.isNullOrEmpty(nomeFaldone)) {
					nomeFaldone = createFaldone(utente, annoFaldone, annoFaldone, nomeFaldonePadre, true);
				}
				// Altrimenti, si creano sia il faldone padre sia il faldone figlio
			} else {
				nomeFaldonePadre = createFaldone(utente, oggettoFaldone, oggettoFaldone, null, false); // Faldone padre
				nomeFaldone = createFaldone(utente, annoFaldone, annoFaldone, nomeFaldonePadre, true); // Faldone figlio
			}
		} catch (final Exception e) {
			LOGGER.error("getFaldoneOPFIgepa -> Si è verificato un errore durante il recupero del faldone OPF IGEPA. Oggetto: " + oggettoFaldone + ", anno: " + annoFaldone,
					e);
			throw new RedException(e);
		} finally {
			popSubject(fcehAdmin);
		}

		LOGGER.info("getFaldoneOPFIgepa -> Nome faldone: " + nomeFaldone);
		return nomeFaldone;
	}
}