package it.ibm.red.business.dto;

/**
 * The Class ContatoreScadenzaDocumentiDTO.
 *
 * @author CPIERASC
 * 
 * 	DTO per modellare il contatore dei documenti in scadenza.
 */
public class ContatoreScadenzaDocumentiDTO extends AbstractDTO {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Contatore.
	 */
	private Integer counter;
	
	/**
	 * Flag urgente (i documenti in scadenza possono essere urgenti o meno, se un documento nel gruppo è urgente, lo è tutto il gruppo).
	 */
	private Boolean urgente;

	/**
	 * Costruttore.
	 */
	public ContatoreScadenzaDocumentiDTO() {
		counter = 0;
	}

	/**
	 * Metodo per aggiungere un documento al contatore (urgente o meno).
	 * 
	 * @param flagUrgente	urgenza del documento (se un documento è urgente tutto l'insieme dei documenti lo è considerato)
	 */
	public final void add(final Boolean flagUrgente) {
		counter++;
		if (urgente == null) {
			urgente = flagUrgente;
		} else {
			urgente = urgente || flagUrgente;
		}
	}

	/**
	 * Getter.
	 * 
	 * @return	contatore
	 */
	public final Integer getCounter() {
		return counter;
	}

	/**
	 * Getter.
	 * 
	 * @return	urgenza gruppo
	 */
	public final Boolean getUrgente() {
		return urgente;
	}
	
}