package it.ibm.red.business.service.ws.concrete;

import java.sql.Connection;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.IRedWsDocGenDAO;
import it.ibm.red.business.dao.IRedWsFlussoDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.RedWsFlusso;
import it.ibm.red.business.service.concrete.AbstractService;
import it.ibm.red.business.service.ws.IRedWsDocGenSRV;

/**
 * 
 * Servizio per il tracciamento dei documenti creati tramite i web service di
 * Red EVO.
 * 
 * @author m.crescentini
 */
@Service
public class RedWsDocGenSRV extends AbstractService implements IRedWsDocGenSRV {

	private static final String PER_IL_FLUSSO = " per il flusso: ";

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 6088787481287153939L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RedWsDocGenSRV.class.getName());

	/**
	 * DAO.
	 */
	@Autowired
	private IRedWsDocGenDAO redWsDocGenDAO;
	
	/**
	 * DAO.
	 */
	@Autowired
	private IRedWsFlussoDAO redWsFlussoDAO;


	/**
	 * @see it.ibm.red.business.service.ws.IRedWsDocGenSRV#insertDocGen(int,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public void insertDocGen(final int idDocumento, final String codiceFlusso) {
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), true);
			
			insertDocGen(idDocumento, codiceFlusso, con);
			
			commitConnection(con);
		} catch (final Exception e) {
			LOGGER.error("[RedWsDocGenSRV][insertDocGen] Errore durante il tracciamento del documento creato con ID: " + idDocumento 
					+ PER_IL_FLUSSO + codiceFlusso, e);
			rollbackConnection(con);
			throw new RedException("Errore durante il tracciamento del documento creato con ID: " + idDocumento + PER_IL_FLUSSO + codiceFlusso, e);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * @see it.ibm.red.business.service.ws.IRedWsDocGenSRV#insertDocGen(int,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public void insertDocGen(final int idDocumento, final String codiceFlusso, final Connection con) {
		Integer idFlusso = null;
			
		if (StringUtils.isNotBlank(codiceFlusso)) {
			final RedWsFlusso flusso = redWsFlussoDAO.getByCodice(codiceFlusso, con);
			
			if (flusso != null) {
				idFlusso = flusso.getIdFlusso();
			} else {
				LOGGER.error("[RedWsDocGenSRV][insertDocGen] Nessun flusso configurato per il codice flusso: " + codiceFlusso);
				throw new RedException("Nessun flusso configurato per il codice flusso: " + codiceFlusso);
			}
		}
		
		// Si traccia la creazione del documento, eventualmente associata al flusso indicato
		final int insert = redWsDocGenDAO.insertDocGen(idDocumento, idFlusso, con);
		
		if (insert > 0) {
			LOGGER.info("[RedWsDocGenSRV][insertDocGen] Tracciata la creazione tramite web service del documento con ID: " + idDocumento 
					+ (StringUtils.isNotBlank(codiceFlusso) ? PER_IL_FLUSSO + codiceFlusso : Constants.EMPTY_STRING));
		}
	}
	
	/**
	 * @see it.ibm.red.business.service.ws.IRedWsDocGenSRV#isDocPresente(int,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public boolean isDocPresente(final int idDocumento, final String codiceFlusso, final Connection con) {
		Integer idFlusso = null;
		
		if (StringUtils.isNotBlank(codiceFlusso)) {
			final RedWsFlusso flusso = redWsFlussoDAO.getByCodice(codiceFlusso, con);
			
			if (flusso != null) {
				idFlusso = flusso.getIdFlusso();
			}
		}
		
		return redWsDocGenDAO.isDocPresente(idDocumento, idFlusso, con);
	}
}