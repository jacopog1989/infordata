package it.ibm.red.business.service;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.ParamsRicercaContiConsuntiviDTO;
import it.ibm.red.business.dto.RisultatoRicercaContiConsuntiviDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio di gestione conti consuntivi.
 * 
 * @author SimoneLungarella
 */
public interface IContiConsuntiviFacadeSRV extends Serializable {

	/**
	 * Genera il report pdf a partire dai risultati di ricerca dell'elenco notifiche
	 * e conti consuntivi.
	 * 
	 * @param paramsRicerca
	 *            parametri della ricerca
	 * @param results
	 *            risultati della ricerca
	 * @param utente
	 *            utente in sessione
	 * @return byte array del pdf generato
	 */
	byte[] generaPdfReport(ParamsRicercaContiConsuntiviDTO paramsRicerca, List<RisultatoRicercaContiConsuntiviDTO> results, UtenteDTO utente);
}
