package it.ibm.red.business.dto;

import java.util.Date;


/**
 * DTO per la ricerca avanzata dei faldoni.
 * 
 * @author m.crescentini
 *
 */
public class ParamsRicercaAvanzataFaldDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1804739458005354090L;
	
	/**
	 * Descrizione faldone 
	 */
	private String descrizioneFaldone;
	
	/**
	 * Data creazione da 
	 */
	private Date dataCreazioneDa;
	
	/**
	 * Data creazione a 
	 */
	private Date dataCreazioneA;
	

	/** 
	 *
	 * @return the descrizioneFaldone
	 */
	public String getDescrizioneFaldone() {
		return descrizioneFaldone;
	}

	/** 
	 *
	 * @param descrizioneFaldone the new descrizione faldone
	 */
	public void setDescrizioneFaldone(final String descrizioneFaldone) {
		this.descrizioneFaldone = descrizioneFaldone;
	}

	/** 
	 *
	 * @return the dataCreazioneDa
	 */
	public Date getDataCreazioneDa() {
		return dataCreazioneDa;
	}

	/** 
	 *
	 * @param dataCreazioneDa the dataCreazioneDa to set
	 */
	public void setDataCreazioneDa(final Date dataCreazioneDa) {
		this.dataCreazioneDa = dataCreazioneDa;
	}

	/** 
	 *
	 * @return the dataCreazioneA
	 */
	public Date getDataCreazioneA() {
		return dataCreazioneA;
	}

	/** 
	 *
	 * @param dataCreazioneA the dataCreazioneA to set
	 */
	public void setDataCreazioneA(final Date dataCreazioneA) {
		this.dataCreazioneA = dataCreazioneA;
	}
}