package it.ibm.red.business.enums;

/**
 * @author m.crescentini
 *
 */
public enum ContributoEsternoModificabileEnum {

	/**
	 * Modificabile.
	 */
	MODIFICABILE(1),
	
	/**
	 * Principale non modificabile.
	 */
	IMMODIFICABILE_PRINCIPALE(2),
	
	/**
	 * Allegato non modificabile.
	 */
	IMMODIFICABILE_ALLEGATO(3);
	
	
	/**
	 * Identificativo.
	 */
	private Integer id;

	/**
	 * Costruttore con id.
	 * @param inId
	 */
	ContributoEsternoModificabileEnum(final Integer inId) {
		id = inId;
	}

	/**
	 * Restituisce l'id del Contributo Esterno.
	 * @return id
	 */
	public Integer getId() {
		return id;
	}
}