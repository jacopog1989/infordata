package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;

import it.ibm.red.business.logger.REDLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IStatoRichiestaDAO;
import it.ibm.red.business.dto.StatoRichiestaDTO;
import it.ibm.red.business.persistence.model.StatoRichiesta;
import it.ibm.red.business.service.IStatoRichiestaSRV;

/**
 * The Class StatoRichiestaSRV.
 *
 * @author CPIERASC
 * 
 *         Servizio per la gestione dello stato della richiesta.
 */
@Service
@Component
public class StatoRichiestaSRV extends AbstractService implements IStatoRichiestaSRV {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1722164217727238856L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(StatoRichiestaSRV.class.getName());
	
	/**
	 * Dao per lo stato della richiesta.
	 */
	@Autowired
	private IStatoRichiestaDAO statoRichiestaDAO;

	/**
	 * 
	 * tags.
	 * @return
	 */
	@Override
	public final Collection<StatoRichiestaDTO> getAll() {
		Collection<StatoRichiestaDTO> statiDTO = new ArrayList<>();
		Connection connection =  null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			Collection<StatoRichiesta> stati = statoRichiestaDAO.getAll(connection);
			if (stati != null) {
				for (StatoRichiesta sr : stati) {
					statiDTO.add(new StatoRichiestaDTO(sr));
				}
			}
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero dei stati richiesta ", e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
		return statiDTO;
	}

}
