/**
 * 
 */
package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.ILookupDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.enums.TipoSpedizioneDocumentoEnum;
import it.ibm.red.business.enums.TrasformazionePDFInErroreEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * @author APerquoti
 * 
 * 			Trasformer documento generic Doc
 *
 */
public class FromDocumentoToGenericDoc extends TrasformerCE<MasterDocumentRedDTO> {

	/**
	 * Serializzable
	 */
	private static final long serialVersionUID = -376051212611046568L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentoToGenericDoc.class.getName());
	
	/**
	 * Posizione tipo destinatari.
	 */
	private static final int TIPO_DEST_POSITION = 3;

	/**
	 * DAO.
	 */
	private ILookupDAO lookupDAO;
	
	/**
	 * DAO.
	 */
	private ITipoProcedimentoDAO tipoPDAO;
	
	/**
	 * Costruttore.
	 */
	public FromDocumentoToGenericDoc() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_GENERIC_DOC);
		lookupDAO = ApplicationContextProvider.getApplicationContext().getBean(ILookupDAO.class);
		tipoPDAO = ApplicationContextProvider.getApplicationContext().getBean(ITipoProcedimentoDAO.class);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE#trasform(com.filenet.api.core.Document,
	 *      java.sql.Connection).
	 */
	@Override
	public MasterDocumentRedDTO trasform(final Document document, final Connection connection) {
		try {
			String guuid = StringUtils.cleanGuidToString(document.get_Id());
			String classeDocumentale = document.getClassName();
			String dt = (String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			Integer numDoc = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			String ogg = (String) getMetadato(document, PropertiesNameEnum.OGGETTO_METAKEY);
			Integer annoDoc = (Integer) getMetadato(document, PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY);
			String nomeFile = (String) getMetadato(document, PropertiesNameEnum.NOME_FILE_METAKEY);
			
			Date dataCreazione = (Date) getMetadato(document, PropertiesNameEnum.DATA_CREAZIONE_METAKEY);
			Date dataScadenza = (Date) getMetadato(document, PropertiesNameEnum.DATA_SCADENZA_METAKEY);
			Integer urgenza = (Integer) getMetadato(document, PropertiesNameEnum.URGENTE_METAKEY);
			Integer idCategoriaDocumento = (Integer) getMetadato(document, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY); 
			Integer idFormatoDocumento = (Integer) getMetadato(document, PropertiesNameEnum.ID_FORMATO_DOCUMENTO); 
			Boolean flagFirmaAutografaRM = (Boolean) getMetadato(document, PropertiesNameEnum.FLAG_FIRMA_AUTOGRAFA_RM_METAKEY);
			
			//Pacchetto informazioni riguardanti il protocollo
			Integer numeroProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
			Integer annoProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
			Integer tipoProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY);
			Date dataProtocollazione = (Date) getMetadato(document, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);
			
//			====================ATTRIBUTI CALCOLATI========================
			//Recupero della tipologia del documento
			Integer idTipologiaDocumento = (Integer) getMetadato(document, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
			String tipologiaDocumento = null;
			if (idTipologiaDocumento != null) {
				tipologiaDocumento = lookupDAO.getDescTipoDocumento(idTipologiaDocumento.longValue(), connection);
			}
			
			//Calcolo presenza di Allegati
			Integer attachment = (Integer) document.get_CompoundDocumentState().getValue();
			Boolean bAttachmentPresent = attachment > 0;
			
			//Gestione del flag PDF in errore
			Integer trasfPdfErrore = (Integer) getMetadato(document, PropertiesNameEnum.TRASFORMAZIONE_PDF_ERRORE_METAKEY);
			Boolean bTrasfPdfErrore = false;
			Boolean bTrasfPdfWarning = false;
			String errorMessage = "";
			String warningMessage = "";
			if (trasfPdfErrore != null && trasfPdfErrore != 0) {
				bTrasfPdfErrore = TrasformazionePDFInErroreEnum.getErrorCodes().contains(trasfPdfErrore);
				errorMessage = Boolean.TRUE.equals(bTrasfPdfErrore) ? TrasformazionePDFInErroreEnum.get(trasfPdfErrore).getDescription() : "";

				bTrasfPdfWarning = TrasformazionePDFInErroreEnum.getWarnCodes().contains(trasfPdfErrore);
				warningMessage = Boolean.TRUE.equals(bTrasfPdfWarning) ? TrasformazionePDFInErroreEnum.get(trasfPdfErrore).getDescription() : "";
			}
			
			
			//Gestione del Flag riservato
			Integer riservato = (Integer) getMetadato(document, PropertiesNameEnum.RISERVATO_METAKEY);
			Boolean flagRiservato = false;
			if (riservato != null && riservato > 0) {
				flagRiservato = true;
			}
			
			
			//Recupero del tipo procedimento
			Integer idTipoP = (Integer) getMetadato(document, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);
			String tipoProcedimento = null;
			if (idTipoP != null) {
				tipoProcedimento = tipoPDAO.getTPbyId(connection, idTipoP).getDescrizione();
			}
			
			//Recupero del numero dei contributi
			Long numContributi = 0L;
			
			//recupero del Tipo Spedizione
			Collection<?> inDestinatari = (Collection<?>) getMetadato(document, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
			// Si normalizza il metadato per evitare errori comuni durante lo split delle singole stringhe dei destinatari
			List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(inDestinatari);
			TipoSpedizioneDocumentoEnum tipoSpedizione = getTipoSpedizione(destinatariDocumento);
			
			SottoCategoriaDocumentoUscitaEnum sottoCategoriaDocumentoUscita = SottoCategoriaDocumentoUscitaEnum.get((Integer) getMetadato(document, PropertiesNameEnum.SOTTOCATEGORIA_DOCUMENTO_USCITA));
			
			String protocolloRiferimento = (String) getMetadato(document, PropertiesNameEnum.PROTOCOLLO_RIFERIMENTO_METAKEY);
			
			boolean integrazioneDati = false;
			Boolean flagIntegrazioneDati = (Boolean) getMetadato(document, PropertiesNameEnum.INTEGRAZIONE_DATI_METAKEY);
			if (flagIntegrazioneDati != null) {
				integrazioneDati = flagIntegrazioneDati.booleanValue();
			}
			
			Integer ufficioCreatore = (Integer) getMetadato(document, PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY);
			
			String decretoLiquidazione =  (String) getMetadato(document, PropertiesNameEnum.DECRETO_LIQUIDAZIONE_METAKEY);
			  
//			====================FINE========================
			
			
			return new MasterDocumentRedDTO(dt, numDoc, ogg, tipologiaDocumento, numeroProtocollo, annoProtocollo, bAttachmentPresent, bTrasfPdfErrore, bTrasfPdfWarning, 
											dataScadenza, urgenza, dataCreazione, idCategoriaDocumento, flagFirmaAutografaRM, tipoProtocollo, flagRiservato, 
											numContributi, null, tipoProcedimento, tipoSpedizione, annoDoc, guuid, idFormatoDocumento, dataProtocollazione, 
											classeDocumentale, errorMessage, warningMessage, nomeFile, sottoCategoriaDocumentoUscita, protocolloRiferimento, 
											integrazioneDati, ufficioCreatore, decretoLiquidazione);
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero di un metadato durante la trasformazione da Document a MasterDocumentRedDTO : ", e);
			return null;
		}
	}
	
	private static TipoSpedizioneDocumentoEnum getTipoSpedizione(final List<String[]> inDestinatari) {
		TipoSpedizioneDocumentoEnum output;
		boolean bElettronico = false;
		boolean bCartaceo = false;
		
		if (inDestinatari != null) {
			
			for (String[] destSplit : inDestinatari) {
				Integer idTipoSpedizione;
				String tipoDestinatario = destSplit[TIPO_DEST_POSITION];
				if (Constants.TipoDestinatario.ESTERNO.equalsIgnoreCase(tipoDestinatario)) {
					idTipoSpedizione = Integer.parseInt(destSplit[2]);
					if (Constants.TipoSpedizione.MEZZO_ELETTRONICO.equals(idTipoSpedizione)) {
						bElettronico = true;
					} else {
						bCartaceo = true;
					}
				}
				
				if (bElettronico && bCartaceo) {
					break;
				}
				
			}
		}
		
		if (bElettronico) {
			output = TipoSpedizioneDocumentoEnum.ELETTRONICO;
		} else {
			output = TipoSpedizioneDocumentoEnum.CARTACEO;
		}
		
		return output;
	}

}
