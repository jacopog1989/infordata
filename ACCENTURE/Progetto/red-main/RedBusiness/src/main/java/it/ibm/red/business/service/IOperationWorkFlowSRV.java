/**
 * 
 */
package it.ibm.red.business.service;

import java.util.Map;

import it.ibm.red.business.dto.WorkFlowWsDTO;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV;

/**
 * @author APerquoti
 *
 */
public interface IOperationWorkFlowSRV extends IOperationWorkFlowFacadeSRV {

	/**
	 * Avanza il workflow.
	 * 
	 * @param wobNumber
	 * @param metadati
	 * @param response
	 * @param isWithRouting
	 * @param fpeh
	 * @return WorkFlowWsDTO
	 */
	WorkFlowWsDTO avanzaWorkFlowDaWs(String wobNumber, Map<String, Object> metadati, String response, boolean isWithRouting, FilenetPEHelper fpeh);

	/**
	 * Avvia una nuova istanza del workflow con nome dato.
	 * 
	 * @param name
	 * @param subject
	 * @param comment
	 * @param metadati
	 * @param response
	 * @param isWithRouting
	 * @param fpeh
	 * @return WorkFlowWsDTO
	 */
	WorkFlowWsDTO avviaIstanzaWorkFlowDaWs(String name, String subject, String comment, Map<String, Object> metadati, String response, boolean isWithRouting,
			FilenetPEHelper fpeh);
}
