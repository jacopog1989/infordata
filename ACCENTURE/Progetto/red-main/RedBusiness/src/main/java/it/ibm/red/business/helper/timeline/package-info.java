/**
 * @author CPIERASC
 *
 *	In questo package è riportato l'helper per la gestione della timeline grafica: in pratica l'oggetto javascript che
 *	utilizzeremo nel front-end per rappresentare una timeline necessità di un model esprimibile attraverso JSON; compito
 *	di tale classe di helper è aiutare il programmatore nella costruzione del model e nella sua traduzione in formato
 *	JSON, in maniera tale da poter alimentare il componente di front-end.
 *
 */
package it.ibm.red.business.helper.timeline;
