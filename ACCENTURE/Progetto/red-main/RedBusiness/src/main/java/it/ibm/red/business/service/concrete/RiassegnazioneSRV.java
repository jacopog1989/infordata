/**
 * 
 */
package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.PEDocumentoDTO;
import it.ibm.red.business.dto.RaccoltaFadDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.TrasformPE;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IFepaSRV;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.IRiassegnazioneSRV;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * @author APerquoti
 *
 */
@Service
public class RiassegnazioneSRV extends AbstractService implements IRiassegnazioneSRV {

	/**
	 * Serializable.
	 */
	private static final long serialVersionUID = 701493781134954694L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RiassegnazioneSRV.class.getName());

	/**
	 * Messaggio errore aggiunta assegnatario a protocollo NPS.
	 */
	private static final String ERROR_AGGIUNTA_ASSEGNATARIO_MSG = "Attenzione, errore nell'aggiungere l'assegnatario a protocollo NPS";

	/**
	 * 
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * 
	 */
	@Autowired
	private IFepaSRV fepaSRV;

	/**
	 * 
	 */
	@Autowired
	private INpsSRV npsSRV;

	/**
	 * 
	 */
	@Autowired
	private ISecuritySRV securitySRV;

	/**
	 * 
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * @see it.ibm.red.business.service.IRiassegnazioneSRV#riassegna(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection, it.ibm.red.business.enums.ResponsesRedEnum,
	 *      java.lang.Long, java.lang.Long, java.lang.String,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      it.ibm.red.business.helper.filenet.pe.FilenetPEHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public Collection<EsitoOperazioneDTO> riassegna(final UtenteDTO utente, final Collection<String> wobNumbers, final ResponsesRedEnum response,
			final Long idNodoDestinatarioNew, final Long idUtenteDestinatarioNew, final String motivoAssegnazione, final IFilenetCEHelper fceh, final FilenetPEHelper fpeh,
			final Connection con) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		Integer numeroDocumento = 0;
		Nodo nodoDirigente = null;
		RaccoltaFadDTO fadDto = null;
		Long idNodoDestinatarioActual = null;
		Long idUtenteDestinatarioActual = null;

		String nomeOperazione;
		boolean isAssegnazione = false;
		if (ResponsesRedEnum.RIASSEGNA.equals(response)) {
			nomeOperazione = "riassegnazione";
		} else if (ResponsesRedEnum.STORNA.equals(response) || ResponsesRedEnum.STORNA_UTENTE.equals(response)) {
			nomeOperazione = "storno";
		} else {
			nomeOperazione = "assegnazione";
			isAssegnazione = true;
		}
		final VWRosterQuery vwrq = fpeh.getDocumentWorkFlowsByWobNumbers(wobNumbers);
		final Collection<PEDocumentoDTO> workFlows = TrasformPE.transform(vwrq, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO);

		final boolean aooHasAssegnazioneIndiretta = utente.getIdNodoAssegnazioneIndiretta() != null && utente.getIdNodoAssegnazioneIndiretta() != 0;

		for (final PEDocumentoDTO wf : workFlows) {
			final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wf.getWobNumber());
			try {
				final Document doc = fceh.getDocumentByIdGestionale(wf.getIdDocumento(), null, null, utente.getIdAoo().intValue(), null, null);
				numeroDocumento = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);

				final Integer annoProtocollo = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
				final Integer numProtocollo = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);

				// Se la response è STORNA_UTENTE verifico che questo documento specifico non si
				// trovi già
				// nelle code 'NSD_DaLavorare' e 'NSD_InSospeso' del nuovo assegnatario
				if (ResponsesRedEnum.STORNA_UTENTE.equals(response)) {
					final boolean hasDocumentiInCoda = isInCoda(idNodoDestinatarioNew, idUtenteDestinatarioNew, fpeh, wf);

					// se la verifica ha esito positivo aggiungo un esito negativo alla lista
					// complessiva
					if (hasDocumentiInCoda) {
						throw new RedException("Attenzione non è possibile inoltrare la richiesta. L’utente selezionato ha già il documento in lavorazione.");
					}
				}
				final HashMap<String, Object> metadati = new HashMap<>();
				// inizio la raccolta daei metadati da modificare durante l'avanzamneto del
				// WorkFlow
				metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivoAssegnazione);
				metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), wf.getTipoAssegnazioneId());
				metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId());

				if (aooHasAssegnazioneIndiretta && (ResponsesRedEnum.STORNA.equals(response) || ResponsesRedEnum.ASSEGNA.equals(response))) {
					metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ASSEGNAZIONE_INDIRETTA_METAKEY), BooleanFlagEnum.NO.getIntValue());
				}
//					<-- RACCOLTA FAD -->
				final boolean isRaccoltaFAD = isRaccoltaFAD((Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY),
						(Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY),
						(String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.RAGIONERIA_FAD_METAKEY),
						(String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.AMMINISTRAZIONE_FAD_METAKEY));
				idNodoDestinatarioActual = idNodoDestinatarioNew;
				idUtenteDestinatarioActual = idUtenteDestinatarioNew;

				if (isAssegnazione && isRaccoltaFAD) {
					nodoDirigente = nodoDAO.getNodo(idNodoDestinatarioNew, con);

					idNodoDestinatarioActual = nodoDirigente.getIdNodo();
					idUtenteDestinatarioActual = nodoDirigente.getDirigente().getIdUtente();
				}
				if (idNodoDestinatarioActual != null) {
					metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), idNodoDestinatarioActual);
				}
				if (idUtenteDestinatarioActual != null) {
					metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), idUtenteDestinatarioActual);
				}

				if (isAssegnazione && isRaccoltaFAD) {
					fadDto = getMetadatiForRaccoltaFad(doc, nodoDirigente, wf.getIdFascicolo().toString());
					fadDto.setIdFascicolo(wf.getIdFascicolo().toString());
					fadDto.setWobNumber(wf.getWobNumber());
					fadDto.setNewMetadati(metadati);

					final String idFascicoloRaccoltaProvvisoria = creaRaccoltaFAD(fadDto, utente, con);

					if (idFascicoloRaccoltaProvvisoria == null) {
						throw new RedException("Attenzione, non è possibile inoltrare la richiesta. Si è verificato un errore durante la creazione della raccolta.");
					}
				}
//					<-- FINE RACCOLTA FAD -->
//					<-- MODIFICA SECURITY FASCICOLO -->
				// ottengo il Coordinatore del mittente
				final Integer idUtenteCoordinatore = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.ID_UTENTE_COORDINATORE_METAKEY);
				final Integer idNodoCoordinatore = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.ID_UFFICIO_COORDINATORE_METAKEY);
				String securityCoordinatore = null;
				if (idUtenteCoordinatore != null && idNodoCoordinatore != null) {
					securityCoordinatore = idNodoCoordinatore + "," + idUtenteCoordinatore;
				}
				// Modifica delle security del FASCICOLO per la RIASSEGNAZIONE
				final ListSecurityDTO securitiesModificaFascicolo = securitySRV.getSecurityPerRiassegnazioneFascicolo(utente, wf.getIdDocumento(),
						idUtenteDestinatarioActual, idNodoDestinatarioActual, securityCoordinatore, con);
				final Boolean modificaSecFascicolo = securitySRV.modificaSecurityFascicoli(utente, securitiesModificaFascicolo, wf.getIdDocumento());
//					<-- FINE MODIFICA SECURITY FASCICOLO -->					
				if (Boolean.TRUE.equals(modificaSecFascicolo)) {
					Boolean delegato = true;
					final Integer isRiservatoInt = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.RISERVATO_METAKEY);
					final boolean isRiservato = isRiservatoInt != null && isRiservatoInt == 1;
					if (isRiservato) {
						delegato = false;
					}
//						<-- MODIFICA SECURITY DOCUMENTO -->
					// Nuove securities per la riassegnazione
					final ListSecurityDTO securitiesPerRiassegnazione = securitySRV.getSecurityPerRiassegnazione(utente, wf.getIdDocumento(),
							idUtenteDestinatarioActual, idNodoDestinatarioActual, securityCoordinatore, delegato, true, isRiservato, false, con);
					// Aggiorno le securities del documento con quelle nuove
					securitySRV.aggiornaEVerificaSecurityDocumento(wf.getIdDocumento(), utente.getIdAoo(), securitiesPerRiassegnazione, true, false, fceh, con);
//						<-- FINE MODIFICA SECURITY DOCUMENTO -->						
					// Verifico che l'Aoo abbia impostato il protocollo su NPS
					// se il documento è protocollato aggiungo l'assegnatario al protocollo (NPS)
					if (TipologiaProtocollazioneEnum.isProtocolloNPS(utente.getIdTipoProtocollo())) {
						aggiungiAssegnatarioAProtocollo(wf.getIdDocumento(), utente, idUtenteDestinatarioActual, idNodoDestinatarioActual, securitiesPerRiassegnazione, fceh,
								con);
					}
					// Avanzamento del workflow
					final Boolean isAvanzato = fpeh.nextStep(wf.getWobNumber(), (fadDto != null) ? fadDto.getNewMetadati() : metadati, response.getResponse());
					if (Boolean.TRUE.equals(isAvanzato)) {
						// Aggiorno le Security per tutti i documenti allacciati...
						final String[] assegnazioni = new String[1];
						assegnazioni[0] = idNodoDestinatarioActual + "," + idUtenteDestinatarioActual;
						securitySRV.aggiornaSecurityDocumentiAllacciati(utente, Integer.parseInt(wf.getIdDocumento()), securitiesPerRiassegnazione, assegnazioni, fceh, con);
						// ...e di conseguenza anche di tutti i contributi interni
						securitySRV.aggiornaSecurityContributiInseriti(utente, wf.getIdDocumento(), idUtenteDestinatarioActual.toString(), idNodoDestinatarioActual.toString(),
								fceh, con);

						if (isAssegnazione && isRaccoltaFAD) {
							fepaSRV.inviaDocumentoInSospeso(fadDto, utente, false);
						}
					} else {
						LOGGER.error("Attenzione, avanzamento del workFlow ( wobNumber: " + wf.getWobNumber() + " ) non riuscito ");
						throw new RedException("Attenzione, avanzamento del workFlow ( wobNumber: " + wf.getWobNumber() + " ) non riuscito ");
					}
				}
				if (annoProtocollo != null && numProtocollo != null) {
					esito.setNumeroProtocollo(numProtocollo);
					esito.setAnnoProtocollo(annoProtocollo);
				}
				esito.setEsito(true);
				esito.setIdDocumento(numeroDocumento);
				esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
			} catch (final Exception e) {
				LOGGER.error("Attenzione, errore durante la riassegnazione di uno dei documenti selezionato.", e);
				esito.setNote("Errore durante l'operazione di " + nomeOperazione + " del documento selezionato.");
				esito.setIdDocumento(numeroDocumento);
			}

			esiti.add(esito);
		}

		return esiti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRiassegnazioneFacadeSRV#riassegna(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection, it.ibm.red.business.enums.ResponsesRedEnum,
	 *      java.lang.Long, java.lang.Long, java.lang.String).
	 */
	@Override
	public Collection<EsitoOperazioneDTO> riassegna(final UtenteDTO utente, final Collection<String> wobNumbers, final ResponsesRedEnum response,
			final Long idNodoDestinatarioNew, final Long idUtenteDestinatarioNew, final String motivoAssegnazione) {
		Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		Connection con = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;

		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			con = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			esiti = riassegna(utente, wobNumbers, response, idNodoDestinatarioNew, idUtenteDestinatarioNew, motivoAssegnazione, fceh, fpeh, con);

		} catch (final Exception e) {
			LOGGER.error("Attenzione, errore durante la creazione delle connessioni necessarie per la riassegnazione dei documenti selezionati", e);
			throw new RedException("Attenzione, errore durante la creazione delle connessioni necessarie per la riassegnazione dei documenti selezionati", e);
		} finally {
			closeConnection(con);
			logoff(fpeh);
			popSubject(fceh);
		}

		return esiti;
	}

	private static boolean isInCoda(final Long idNodoDestinatarioNew, final Long idUtenteDestinatarioNew, final FilenetPEHelper fpeh, final PEDocumentoDTO wf) {
		boolean hasDocumentiInCoda = false;

		try {
			final ArrayList<Long> idsTipoAssegnazione = new ArrayList<>();
			idsTipoAssegnazione.add(Long.valueOf(TipoAssegnazioneEnum.CONTRIBUTO.getId()));
			// Interrogo FileNet per verificare che l'utente e il nodo di destinazione non
			// contengano già questo documento nelle proprie code di lavoro
			// non serve dividere logicamente la coda da lavorare per ucb
			hasDocumentiInCoda = (fpeh.hasDocumentInCoda(DocumentQueueEnum.DA_LAVORARE, wf.getIdDocumento(), idNodoDestinatarioNew, idUtenteDestinatarioNew,
					idsTipoAssegnazione, false)
					|| fpeh.hasDocumentInCoda(DocumentQueueEnum.SOSPESO, wf.getIdDocumento(), idNodoDestinatarioNew, idUtenteDestinatarioNew, idsTipoAssegnazione, false));

		} catch (final Exception e) {
			LOGGER.error("Errore durante L'esecuzione della Response 'Storna a Utente'. Accesso alla coda del destinatario Non Riuscita", e);
		}

		return hasDocumentiInCoda;
	}

	/**
	 * Metodo per verificare che il documento sia una raccolta FAD.
	 * 
	 * @param idTipologiaDocumento
	 * @param idTipologiaProcedimento
	 * @param ragioneriaFAD
	 * @param amministrazioneFAD
	 * @return
	 */
	private static boolean isRaccoltaFAD(final Integer idTipologiaDocumento, final Integer idTipologiaProcedimento, final String ragioneriaFAD, final String amministrazioneFAD) {
		final Integer idAttoDecreto = Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_TIPO_DOC_VALUE));
		final Integer idAttoDecretoAuto = Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_AUTOMATICO_VALUE));

		return (idTipologiaDocumento.equals(idAttoDecreto) && idTipologiaProcedimento.equals(idAttoDecretoAuto) && (ragioneriaFAD != null) && (amministrazioneFAD != null));
	}

	/**
	 * 
	 * @param fadDto
	 * @param connection
	 * @return
	 */
	private String creaRaccoltaFAD(final RaccoltaFadDTO fadDto, final UtenteDTO utente, final Connection connection) {
		String idFascicoloRaccoltaProvvisoria = null;

		try {
			idFascicoloRaccoltaProvvisoria = fadDto.getIdFascicoloRaccoltaProvvisoria();
			if (idFascicoloRaccoltaProvvisoria == null) {
				// creo la raccolta FAD e recupero id Fascicolo
				idFascicoloRaccoltaProvvisoria = fepaSRV.creaRaccoltaFad(fadDto, false, utente, connection);
			} else {
				// aggiorno i metadati di creazione del documento
				fepaSRV.updateCreatoreRaccoltaProvvisoria(fadDto, idFascicoloRaccoltaProvvisoria, false, utente, fadDto.getIdNodoDirigente(), fadDto.getDescrizioneNodo());
			}

			if (idFascicoloRaccoltaProvvisoria != null) {
				fadDto.setIdFascicoloRaccoltaProvvisoria(idFascicoloRaccoltaProvvisoria);
				// aggiungo il documento al fascicoloRaccoltaProvvisoria
				fepaSRV.addDocumentoFascicoloRaccoltaProvvisoria(fadDto, utente, false, connection);
			}

		} catch (final Exception e) {
			LOGGER.error("Attenzione, errore nella creazione raccolta FAD", e);
			throw new RedException("Attenzione, errore nella creazione raccolta FAD", e);
		}

		return idFascicoloRaccoltaProvvisoria;

	}

	/**
	 * Raccolta dati necessaria per la creazione Raccolta FAD.
	 * 
	 * @param doc
	 * @param nodoDirigente
	 * @return
	 */
	private RaccoltaFadDTO getMetadatiForRaccoltaFad(final Document doc, final Nodo nodoDirigente, final String idFascicoloWF) {
		final RaccoltaFadDTO datiFad = new RaccoltaFadDTO();

		datiFad.setClasseDocumentale((String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));
		datiFad.setDocumentTitle((String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
		datiFad.setIdFascicoloRaccoltaProvvisoria((String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.ID_RACCOLTA_PROVVISORIA_METAKEY));
		datiFad.setAnnoDocumento((Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY));
		datiFad.setOggetto((String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.OGGETTO_METAKEY));
		datiFad.setNumeroProtocollo(((Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY)).toString());
		datiFad.setAnnoProtocollo(((Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY)).toString());
		datiFad.setDataProtocollo(DateUtils.dateToString((Date) TrasformerCE.getMetadato(doc, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY), true));
		datiFad.setRagioneriaFad((String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.RAGIONERIA_FAD_METAKEY));
		datiFad.setAmministrazioneFad((String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.AMMINISTRAZIONE_FAD_METAKEY));

		datiFad.setIdNodoDirigente(nodoDirigente.getIdNodo().toString());
		datiFad.setDescrizioneNodo(nodoDirigente.getDescrizione());
		datiFad.setIdUtenteDirigente(nodoDirigente.getDirigente().getIdUtente().toString());

		final Integer annoFascicolo = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY);
		final String prefixFascicoloLabel = idFascicoloWF + "_" + annoFascicolo + "_";
		datiFad.setPrefixFascicoloLabel(prefixFascicoloLabel);

		datiFad.setAooProtocolloAmmUscita((String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.AOO_PROT_AMM_METAKEY));
		datiFad.setNumeroProtocolloAmmUscita(((Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUM_PROT_AMM_METAKEY)).toString());
		datiFad.setDataProtocolloAmmUscita((Date) TrasformerCE.getMetadato(doc, PropertiesNameEnum.DATA_PROT_AMM_METAKEY));

		datiFad.setAooProtocolloUcbIngresso((String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.AOO_PROT_UCB_INGRESSO_METAKEY));
		datiFad.setNumeroProtocolloUcbIngresso(((Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUM_PROT_UCB_INGRESSO_METAKEY)).toString());
		datiFad.setDataProtocolloUcbIngresso((Date) TrasformerCE.getMetadato(doc, PropertiesNameEnum.DATA_PROT_UCB_INGRESSO_METAKEY));

		datiFad.setAooProtocolloUcbUscita((String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.AOO_PROT_UCB_USCITA_METAKEY));
		datiFad.setNumeroProtocolloUcbUscita(((Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUM_PROT_UCB_USCITA_METAKEY)).toString());
		datiFad.setDataProtocolloUcbUscita((Date) TrasformerCE.getMetadato(doc, PropertiesNameEnum.DATA_PROT_UCB_USCITA_METAKEY));

		return datiFad;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.IRiassegnazioneSRV#assegnaADirigenteUfficio(
	 * java.lang.String, filenet.vw.api.VWWorkObject, java.lang.String,
	 * it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 * it.ibm.red.business.helper.filenet.pe.FilenetPEHelper, java.sql.Connection)
	 */
	@Override
	public final Collection<EsitoOperazioneDTO> assegnaADirigenteUfficio(final String documentTitle, final VWWorkObject workflowPrincipale, final String motivoAssegnazione,
			final IFilenetCEHelper fceh, final FilenetPEHelper fpeh, final Connection con) {

		final Long idNodoDestinatario = Long.valueOf(
				(Integer) TrasformerPE.getMetadato(workflowPrincipale, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY)));

		final Nodo nodo = nodoDAO.getNodo(idNodoDestinatario, con);
		final Utente dirigente = nodo.getDirigente();
		if (dirigente == null) {
			return Arrays.asList(new EsitoOperazioneDTO(Integer.parseInt(documentTitle)));
		}

		// Si recupera il dirigente dell'ufficio nella cui coda Corriere si trova il
		// documento,
		// in qualità di utente competente per il documento
		
		final UtenteDTO utenteCompetente = utenteSRV.getById(dirigente.getIdUtente(), null, idNodoDestinatario, con);

		LOGGER.info("assegnaADirigenteUfficio -> Assegnazione all'utente dirigente: " + utenteCompetente.getNome() + " " + utenteCompetente.getCognome()
				+ " in qualità di utente competente per il documento con document title: " + documentTitle);

		// Assegnazione tramite sollecitazione della response "Assegna"
		return riassegna(utenteCompetente, Arrays.asList(workflowPrincipale.getWorkflowNumber()), ResponsesRedEnum.ASSEGNA, utenteCompetente.getIdUfficio(),
				utenteCompetente.getId(), motivoAssegnazione, fceh, fpeh, con);
	}

	/**
	 * @param idDocumento
	 * @param utente
	 * @param idUtenteDestinatarioNew
	 * @param idNodoDestinatarioNew
	 * @param security
	 * @param fceh
	 * @param connection
	 */
	private void aggiungiAssegnatarioAProtocollo(final String idDocumento, final UtenteDTO utente, final Long idUtenteDestinatarioNew, final Long idNodoDestinatarioNew,
			final ListSecurityDTO security, final IFilenetCEHelper fceh, final Connection connection) {
		final List<String> selectList = new ArrayList<>();
		selectList.add(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY));
		selectList.add(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY));
		selectList.add(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY));
		selectList.add(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY));
		try {
			final Document doc = fceh.getDocumentByIdGestionale(idDocumento, selectList, null, utente.getIdAoo().intValue(), null, null);

			final String idProtocollo = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);
			final Integer idCategoriaDocumento = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY);

			if (!StringUtils.isNullOrEmpty(idProtocollo) && idCategoriaDocumento.equals(CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0])) {

				final String numeroAnnoProtocollo = (TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY) + "/"
						+ TrasformerCE.getMetadato(doc, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY));
				final String perCompetenza = "si";
				if (npsSRV.aggiungiAssegnatarioPerRiassegnazioneAsync(0, numeroAnnoProtocollo, idNodoDestinatarioNew, idUtenteDestinatarioNew, idProtocollo, security,
						new Date(), utente, perCompetenza, idDocumento, connection, fceh) == 0) {
					LOGGER.error(ERROR_AGGIUNTA_ASSEGNATARIO_MSG);
					throw new RedException(ERROR_AGGIUNTA_ASSEGNATARIO_MSG);
				}
			}

		} catch (final Exception e) {
			LOGGER.error(ERROR_AGGIUNTA_ASSEGNATARIO_MSG, e);
			throw new RedException(ERROR_AGGIUNTA_ASSEGNATARIO_MSG);
		}
	}

}
