/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.Date;
import java.util.List;

import com.filenet.api.constants.Cardinality;

import it.ibm.red.business.enums.MetadatoDinamicoDataTypeEnum;

/**
 * The class MetadatoDinamicoDTO.
 * 
 * @author m.crescentini
 * 
 * Classe utilizzata per modellare un metadato dinamico.
 * 
 */
public class MetadatoDinamicoDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 8387874187790838861L;
	

	/**
	 * Chiave.
	 */
	private final String chiave;
	
	/**
	 * Valore.
	 */
	private transient Object valore;
	
	/**
	 * Display name.
	 */
	private String displayName;
	
	/**
	 *  SELECT, CHECKBOX, DATE, INPUT_TEXT.
	 */
	private MetadatoDinamicoDataTypeEnum dataTypeEnum;
	
	/**
	 * Opzioni quando il metadato e' una select.
	 */
	private List<KeyValueDTO> optionsList;
	
	/**
	 * Lunghezza massima per stringhe o numeri.
	 */
	private int maximumLength;
	
	/**
	 * Si riferisce all'oggetto filenet Cardinality e può avere due valori.
	 * Cardinality.LIST_AS_INT 
	 * oppure 
	 * Cardinality.SINGLE_AS_INT
	 */
	private int cardinality;
	
	/**
	 * Preso dal valore filenet PropertyDefinition.get_DataType().getValue().
	 * Indica se il dataTypeEnum INPUT_TEXT e' una stringa o un intero
	 * TypeID.STRING_AS_INT
	 * oppure 
	 * TypeID.LONG_AS_INT
	 */
	private int dataTypeValue;
	
	/**
	 * Data massima.
	 */
	private Date maxDate;	

	/**
	 * Costruttore.
	 * @param chiave
	 * @param valore
	 */
	public MetadatoDinamicoDTO(final String chiave, final Object valore) {
		super();
		this.chiave = chiave;
		this.valore = valore;
		this.cardinality = Cardinality.SINGLE_AS_INT;
	}

	/**
	 * Costruttore.
	 * @param chiave
	 * @param valore
	 * @param displayName
	 */
	public MetadatoDinamicoDTO(final String chiave, final Object valore, final String displayName) {
		super();
		this.chiave = chiave;
		this.valore = valore;
		this.cardinality = Cardinality.SINGLE_AS_INT;
		this.displayName = displayName;
	}

	/**
	 * Costruttore.
	 * @param chiave
	 * @param valore
	 * @param optionsList
	 * @param maximumLength
	 */
	public MetadatoDinamicoDTO(final String chiave, final Object valore, final List<KeyValueDTO> optionsList, final int maximumLength) {
		super();
		this.chiave = chiave;
		this.valore = valore;
		this.optionsList = optionsList;
		this.maximumLength = maximumLength;
	}

	/**
	 * Costruttore.
	 * @param chiave
	 * @param valore
	 * @param optionsList
	 * @param maximumLength
	 * @param displayName
	 */
	public MetadatoDinamicoDTO(final String chiave, final Object valore, final List<KeyValueDTO> optionsList, final int maximumLength, final String displayName) {
		super();
		this.chiave = chiave;
		this.valore = valore;
		this.optionsList = optionsList;
		this.maximumLength = maximumLength;
		this.displayName = displayName;
	}

	/**
	 * Restituisce la chiave del metadato dinamico.
	 * @return chiave metadato dinamico
	 */
	public String getChiave() {
		return chiave;
	}

	/**
	 * Restituisce il valore del metadato dinamico.
	 * @return valore metadato dinamico
	 */
	public Object getValore() {
		return valore;
	}

	/**
	 * Restituisce il tipo del metadato dinamico.
	 * @return tipo metadato dinamico
	 */
	public MetadatoDinamicoDataTypeEnum getDataTypeEnum() {
		return dataTypeEnum;
	}

	/**
	 * Imposta il tipo del metadato dinamico.
	 * @param dataTypeEnum
	 */
	public void setDataTypeEnum(final MetadatoDinamicoDataTypeEnum dataTypeEnum) {
		this.dataTypeEnum = dataTypeEnum;
	}

	/**
	 * Restituisce la lista di opzioni.
	 * @return optionsList
	 */
	public List<KeyValueDTO> getOptionsList() {
		return optionsList;
	}

	/**
	 * Imposta la lista delle opzioni.
	 * @param optionsList
	 */
	public void setOptionsList(final List<KeyValueDTO> optionsList) {
		this.optionsList = optionsList;
	}

	/**
	 * Restituisce la lunghezza massima.
	 * @return lunghezza massima
	 */
	public int getMaximumLength() {
		return maximumLength;
	}

	/**
	 * Imposta la lunghezza massima.
	 * @param maximumLength
	 */
	public void setMaximumLength(final int maximumLength) {
		this.maximumLength = maximumLength;
	}

	/**
	 * Imposta il valore del metadato dinamico.
	 * @param valore
	 */
	public void setValore(final Object valore) {
		this.valore = valore;
	}

	/**
	 * Restituisce la cardinalita.
	 * @return cardinalita
	 */
	public int getCardinality() {
		return cardinality;
	}

	/**
	 * Imposta la cardinalita.
	 * @param cardinality
	 */
	public void setCardinality(final int cardinality) {
		this.cardinality = cardinality;
	}

	/**
	 * Restituisce il tipo dei dati.
	 * @return tipo dati
	 */
	public int getDataTypeValue() {
		return dataTypeValue;
	}

	/**
	 * Imposta il tipo dei dati.
	 * @param dataTypeValue
	 */
	public void setDataTypeValue(final int dataTypeValue) {
		this.dataTypeValue = dataTypeValue;
	}

	/**
	 * Restituisce il display name del metadato dinamico.
	 * @return display name
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Restituisce la data massima.
	 * @return data massima
	 */
	public Date getMaxDate() {
		return maxDate;
	}

	/**
	 * Imposta la data massima.
	 * @param maxDate
	 */
	public void setMaxDate(final Date maxDate) {
		this.maxDate = maxDate;
	}
	
}