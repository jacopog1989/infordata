/**
 * 
 */
package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IRegistroRepertorioDAO;
import it.ibm.red.business.dto.RegistroRepertorioDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IRegistroRepertorioSRV;

/**
 * @author APerquoti
 *
 */
@Service
public class RegistroRepertorioSRV extends AbstractService implements IRegistroRepertorioSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -1625875237077243676L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RegistroRepertorioSRV.class);

	/**
	 * DAO.
	 */
	@Autowired
	private IRegistroRepertorioDAO regRepertorioDAO;

	/**
	 * @see it.ibm.red.business.service.IRegistroRepertorioSRV#getRegistriRepertorioConfigurati(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public HashMap<Long, List<RegistroRepertorioDTO>> getRegistriRepertorioConfigurati(final Long idAoo, Connection connection) {
		final HashMap<Long, List<RegistroRepertorioDTO>> output = new HashMap<>();
		final boolean manageConn = connection == null;
		try {

			if (manageConn) {
				connection = setupConnection(getDataSource().getConnection(), false);
			}

			// Vengono recuperate le occorrenze configurate per l'idAoo preso in input
			final List<RegistroRepertorioDTO> listCoppie = regRepertorioDAO.getRegistriRepertorioByAoo(idAoo, connection);

			for (final RegistroRepertorioDTO rr : listCoppie) {

				// Viene verificato se Per ogni elemento recuperato
				// esiste già un mapping all'interno della struttura.
				if (output.containsKey(rr.getIdTipoProcedimento())) {
					output.get(rr.getIdTipoProcedimento()).add(rr);
				} else {

					final List<RegistroRepertorioDTO> list = new ArrayList<>();
					list.add(rr);

					output.put(rr.getIdTipoProcedimento(), list);
				}

			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante la creazione della struttura che rappresenta le coppie TipoProcedimento/TipologiaDocumento per i Registri di Repertorio", e);
			throw new RedException("Errore durante la creazione della struttura che rappresenta le coppie TipoProcedimento/TipologiaDocumento per i Registri di Repertorio",
					e);
		} finally {
			if (manageConn) {
				closeConnection(connection);
			}
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.IRegistroRepertorioSRV#checkIsRegistroRepertorio(
	 *      java.lang.Long, java.lang.Long, java.lang.Long, java.util.HashMap,
	 *      java.sql.Connection).
	 */
	@Override
	public Boolean checkIsRegistroRepertorio(final Long idAoo, final Long idTipologiaDocumento, final Long idTipoProcedimento,
			Map<Long, List<RegistroRepertorioDTO>> registriRepertorioConfigured, Connection connection) {
		Boolean output = Boolean.FALSE;
		boolean oneShot = Boolean.FALSE;

		if (idTipologiaDocumento == null || idTipoProcedimento == null) {
			return output;
		}

		try {

			if (connection == null) {
				connection = setupConnection(getDataSource().getConnection(), false);
				oneShot = Boolean.TRUE;
			}

			if (registriRepertorioConfigured == null) {
				registriRepertorioConfigured = getRegistriRepertorioConfigurati(idAoo, connection);
			}

			final List<RegistroRepertorioDTO> registriRepertorioList = registriRepertorioConfigured.get(idTipoProcedimento);

			if (registriRepertorioList != null && !registriRepertorioList.isEmpty()) {

				// Si cicla la lista di Registri Repertorio configurati perchè c'è la
				// possibilità
				// che un tipoProcedimento possa essere configurato per più tipiDocumento.
				for (final RegistroRepertorioDTO rr : registriRepertorioList) {
					if (rr.getIdTipologiaDocumento().equals(idTipologiaDocumento)) {
						// Una volta verificata che la coppia TipologiaDocumento/TipoProcedimento
						// selezionata
						// sia presente all'interno della configurazione su DB restitusco un risultato
						// positivo
						output = Boolean.TRUE;
						break;
					}

				}

			}

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la verifica della modalità Registro Repertorio: ", e);
			throw new RedException("Si è verificato un errore durante la verifica della modalità Registro Repertorio: ", e);
		} finally {
			if (oneShot) {
				closeConnection(connection);
			}
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRegistroRepertorioFacadeSRV#
	 *      checkIsRegistroRepertorio(java.lang.Long, java.lang.Long,
	 *      java.lang.Long, java.util.HashMap).
	 */
	@Override
	public Boolean checkIsRegistroRepertorio(final Long idAoo, final Long idTipologiaDocumento, final Long idTipoProcedimento,
			final Map<Long, List<RegistroRepertorioDTO>> registriRepertorioConfigured) {
		Boolean output = Boolean.FALSE;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			output = checkIsRegistroRepertorio(idAoo, idTipologiaDocumento, idTipoProcedimento, registriRepertorioConfigured, connection);

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore nella connessione al database per la verifica della modalità Registro Repertorio: ", e);
			throw new RedException("Si è verificato un errore nella connessione al dataabse per la verifica della modalità Registro Repertorio: ", e);
		} finally {
			closeConnection(connection);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRegistroRepertorioFacadeSRV#getDescrForShowEtichetteByAoo(java.lang.Long).
	 */
	@Override
	public List<String> getDescrForShowEtichetteByAoo(final Long idAoo) {
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return regRepertorioDAO.getDescrForShowEtichetteByAoo(idAoo, connection);

		} catch (final SQLException e) {
			LOGGER.error("Si è verificato un errore nel recupero delle descrizioni per la stampa etichette", e);
			throw new RedException("Si è verificato un errore nel recupero delle descrizioni per la stampa etichette: ", e);
		} finally {
			closeConnection(connection);
		}
	}

}
