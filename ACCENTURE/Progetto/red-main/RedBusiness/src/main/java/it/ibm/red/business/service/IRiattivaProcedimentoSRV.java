package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IRiattivaProcedimentoFacadeSRV;

/**
 * Interfaccia del servizio che gestisce la funzionalità di riattivamento procedimento.
 */
public interface IRiattivaProcedimentoSRV extends IRiattivaProcedimentoFacadeSRV {

}