/**
 * 
 */
package it.ibm.red.business.service.ws;

import it.ibm.red.business.service.ws.facade.ICheckDocumentWsFacadeSRV;

/**
 * @author APerquoti
 *
 * Interfaccia servizio gestione check dei Documenti web service.
 */
public interface ICheckDocumentWsSRV extends ICheckDocumentWsFacadeSRV {

}
