package it.ibm.red.business.persistence.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The Class Ruolo.
 *
 * @author CPIERASC
 * 
 *         Entità che mappa la tabella RUOLO.
 */
public class Ruolo implements Serializable {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Aoo.
	 */
	private Aoo aoo;

	/**
	 * Data attivazione.
	 */
	private Date dataAttivazione;

	/**
	 * Data disattivazione.
	 */
	private Date dataDisattivazione;

	/**
	 * Identificativo ruolo.
	 */
	private long idRuolo;

	/**
	 * Nome ruolo.
	 */
	private String nomeRuolo;

	/**
	 * Permessi.
	 */
	private BigDecimal permessi;

	/**
	 * Costruttore.
	 * 
	 * @param inIdRuolo				identificativo
	 * @param inAoo					aoo
	 * @param inDataAttivazione		data attivazione
	 * @param inDataDisattivazione	data disattivazione
	 * @param inNomeRuolo			nome ruolo
	 * @param inPermessi			permessi
	 */
	public Ruolo(final long inIdRuolo, final Aoo inAoo, final Date inDataAttivazione,  
			final Date inDataDisattivazione,  final String inNomeRuolo,  final BigDecimal inPermessi) {
		this.aoo = inAoo;
		this.dataAttivazione = inDataAttivazione;
		this.dataDisattivazione = inDataDisattivazione;
		this.idRuolo = inIdRuolo;
		this.nomeRuolo = inNomeRuolo;
		this.permessi = inPermessi;
	}
	
	/**
	 * Getter aoo.
	 * 
	 * @return	aoo
	 */
	public final Aoo getAoo() {
		return aoo;
	}

	/**
	 * Getter data attivazione.
	 * 
	 * @return	data attivazione
	 */
	public final Date getDataAttivazione() {
		return dataAttivazione;
	}

	/**
	 * Getter data disattivazione.
	 * 
	 * @return	data disattivazione
	 */
	public final Date getDataDisattivazione() {
		return dataDisattivazione;
	}

	/**
	 * Getter identificativo ruolo.
	 * 
	 * @return	identificativo ruolo
	 */
	public final long getIdRuolo() {
		return idRuolo;
	}

	/**
	 * Getter nome ruolo.
	 * 
	 * @return	nome ruolo
	 */
	public final String getNomeRuolo() {
		return nomeRuolo;
	}

	/**
	 * Getter permessi.
	 * 
	 * @return	permessi
	 */
	public final BigDecimal getPermessi() {
		return permessi;
	}

	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (idRuolo ^ (idRuolo >>> 32));
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Ruolo other = (Ruolo) obj;
		return (idRuolo == other.idRuolo);
	}
	
}