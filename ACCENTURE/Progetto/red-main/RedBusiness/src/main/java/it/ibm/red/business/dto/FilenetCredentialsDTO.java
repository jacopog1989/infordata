package it.ibm.red.business.dto;

/**
 * The Class FilenetCredentialsDTO.
 *
 * @author CPIERASC
 * 
 * 	Data Transfer Object per i dati di connessione a Filenet.
 */
public class FilenetCredentialsDTO extends AbstractDTO {

	/**
	 * Serializzable.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Messaggio errore ricerca assegnazioni.
	 */
	public static final String P8_ADMIN = "p8admin";


	/**
	 * Connection point.
	 */
	private final String connectionPoint;

	/**
	 * Identificativo client aoo.
	 */
	private final String idClientAoo;

	/**
	 * Object store.
	 */
	private final String objectStore;

	/**
	 * Password.
	 */
	private final String password;

	/**
	 * Stanza jaas.
	 */
	private final String stanzaJAAS;

	/**
	 * Uri.
	 */
	private final String uri;

	/**
	 * Username.
	 */
	private final String userName;

	/**
	 * IdAoo.
	 */
	private Long idAoo;
	
	/**
	 * Costruttore.
	 * 
	 * @param inUserName		username
	 * @param inPassword		password
	 * @param inUri				uri
	 * @param inStanzaJAAS		stanza jass
	 * @param inConnectionPoint	connection point
	 * @param inObjectStore		object store
	 * @param inIdClientAoo		identificativo client aoo
	 */
	public FilenetCredentialsDTO(final String inUserName, final String inPassword, final String inUri, final String inStanzaJAAS, 
								 final String inConnectionPoint, final String inObjectStore, final String inIdClientAoo) {
		super();
		userName = inUserName;
		password = inPassword;
		uri = inUri;
		stanzaJAAS = inStanzaJAAS;
		connectionPoint = inConnectionPoint;
		objectStore = inObjectStore;
		idClientAoo = inIdClientAoo;
	}
	
	/**
	 * Costruttore per accesso come P8ADMIN
	 * 
	 * @param inFcDto
	 */
	public FilenetCredentialsDTO(final FilenetCredentialsDTO inFcDto) {
		super();
		userName = P8_ADMIN;
		password = inFcDto.getPassword();
		uri = inFcDto.getUri();
		stanzaJAAS = inFcDto.getStanzaJAAS();
		connectionPoint = inFcDto.getConnectionPoint();
		objectStore = inFcDto.getObjectStore();
		idClientAoo = inFcDto.idClientAoo;
	}
	
	/**
	 * Costruttore per accesso come P8ADMIN.
	 * 
	 * @param inPassword		password
	 * @param inUri				uri
	 * @param inStanzaJAAS		stanza jass
	 * @param inConnectionPoint	connection point
	 * @param inObjectStore		object store
	 * @param inIdClientAoo		identificativo client aoo
	 */
	public FilenetCredentialsDTO(final String inPassword, final String inUri, final String inStanzaJAAS, 
								 final String inConnectionPoint, final String inObjectStore, final String inIdClientAoo) {
		super();
		userName = P8_ADMIN;
		password = inPassword;
		uri = inUri;
		stanzaJAAS = inStanzaJAAS;
		connectionPoint = inConnectionPoint;
		objectStore = inObjectStore;
		idClientAoo = inIdClientAoo;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	connection point
	 */
	public final String getConnectionPoint() {
		return connectionPoint;
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo client aoo
	 */
	public final String getIdClientAoo() {
		return idClientAoo;
	}

	/**
	 * Getter.
	 * 
	 * @return	object store
	 */
	public final String getObjectStore() {
		return objectStore;
	}

	/**
	 * Getter.
	 * 
	 * @return	password
	 */
	public final String getPassword() {
		return password;
	}

	/**
	 * Getter.
	 * 
	 * @return	stanza jaas
	 */
	public final String getStanzaJAAS() {
		return stanzaJAAS;
	}

	/**
	 * Getter.
	 * 
	 * @return	uri
	 */
	public final String getUri() {
		return uri;
	}

	/**
	 * Getter.
	 * 
	 * @return	username
	 */
	public final String getUserName() {
		return userName;
	}

	/**
	 * Restituisce dell'Area Organizzativa Omogenea.
	 * @return id AOO
	 */
	public Long getIdAoo() {
		return idAoo;
	}

	/**
	 * Imposta l'id dell'AOO.
	 * @param idAoo
	 */
	public void setIdAoo(final Long idAoo) {
		this.idAoo = idAoo;
	}
	
	
}
