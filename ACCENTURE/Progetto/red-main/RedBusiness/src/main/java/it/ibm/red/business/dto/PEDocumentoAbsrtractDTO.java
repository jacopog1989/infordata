package it.ibm.red.business.dto;

import it.ibm.red.business.enums.DocumentQueueEnum;

/**
 * Versione molto snella del documento che si trova nel PE Qualora esista la
 * necessità di avere addirittura meno attributi di questi forse potrebbe essere
 * opportuno evitare di usare un DTO o un trasformer per recuperare questi dati.
 */
public class PEDocumentoAbsrtractDTO extends AbstractDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8966760210675774322L;

	/**
	 * Identificativo documento.
	 */
	private String idDocumento;
	
	/**
	 * WobNumber
	 */
	private String wobNumber;
	
	/**
	 * Identificativo tipo assegnazione.
	 */
	private Integer tipoAssegnazioneId;
	
	/**
	 * Coda corrente.
	 */
	private String codaCorrente;
	
	/**
	 * Coda.
	 */
	private DocumentQueueEnum queue;
		
	/**
	 * Identificativo nodo destinatario.
	 */
	private Integer idNodoDestinatario;

	/**
	 * Identificativo utente destinatario.
	 */
	private Integer idUtenteDestinatario;

	/**
	 * Costruttore PE documento absrtract DTO.
	 *
	 * @param idDocumento
	 *            the id documento
	 * @param inWobNmber
	 *            the in wob nmber
	 * @param tipoAssegnazioneId
	 *            the tipo assegnazione id
	 * @param codaCorrente
	 *            the coda corrente
	 * @param queue
	 *            the queue
	 * @param idNodoDestinatario
	 *            the id nodo destinatario
	 * @param idUtenteDestinatario
	 *            the id utente destinatario
	 */
	public PEDocumentoAbsrtractDTO(final String idDocumento, final String inWobNmber, final Integer tipoAssegnazioneId, final String codaCorrente,
			final DocumentQueueEnum queue, final Integer idNodoDestinatario, final Integer idUtenteDestinatario) {
		this.wobNumber = inWobNmber;
		this.idDocumento = idDocumento;
		this.tipoAssegnazioneId = tipoAssegnazioneId;
		this.codaCorrente = codaCorrente;
		this.queue = queue;
		this.idNodoDestinatario = idNodoDestinatario;
		this.idUtenteDestinatario = idUtenteDestinatario;
	}

	/** 
	 *
	 * @return the id documento
	 */
	public String getIdDocumento() {
		return idDocumento;
	}

	/**  
	 * @return the wob number
	 */
	public String getWobNumber() {
		return wobNumber;
	}

	/**  
	 * @return the tipo assegnazione id
	 */
	public Integer getTipoAssegnazioneId() {
		return tipoAssegnazioneId;
	}

	/**  
	 * @return the coda corrente
	 */
	public String getCodaCorrente() {
		return codaCorrente;
	}

	/**  
	 * @return the queue
	 */
	public DocumentQueueEnum getQueue() {
		return queue;
	}

	/**  
	 * @return the id nodo destinatario
	 */
	public Integer getIdNodoDestinatario() {
		return idNodoDestinatario;
	}

	/** 
	 * @return the id utente destinatario
	 */
	public Integer getIdUtenteDestinatario() {
		return idUtenteDestinatario;
	}

	/**
	 * Imposta l'identificativo del documento.
	 * 
	 * @param idDocumento
	 *            Identificativo documento da impostare.
	 */
	protected void setIdDocumento(final String idDocumento) {
		this.idDocumento = idDocumento;
	}

	/**
	 * Imposta il wob number del documento.
	 * 
	 * @param wobNumber
	 *            Wob number da impostare.
	 */
	protected void setWobNumber(final String wobNumber) {
		this.wobNumber = wobNumber;
	}

	/**
	 * Imposta l'identificativo del tipo assegnazione.
	 * 
	 * @param tipoAssegnazioneId
	 *            ID tipo assegnazione da impostare.
	 */
	protected void setTipoAssegnazioneId(final Integer tipoAssegnazioneId) {
		this.tipoAssegnazioneId = tipoAssegnazioneId;
	}

	/**
	 * Imposta la coda corrente.
	 * 
	 * @param codaCorrente
	 *            Coda corrente da impostare.
	 */
	protected void setCodaCorrente(final String codaCorrente) {
		this.codaCorrente = codaCorrente;
	}

	/**
	 * Imposta la coda.
	 * 
	 * @param queue
	 *            La coda da impostare.
	 */
	public void setQueue(final DocumentQueueEnum queue) {
		this.queue = queue;
	}

	/**
	 * Imposta l'identificativo del nodo destinatario.
	 * 
	 * @param idNodoDestinatario
	 *            ID nodo destinatario da impostare.
	 */
	protected void setIdNodoDestinatario(final Integer idNodoDestinatario) {
		this.idNodoDestinatario = idNodoDestinatario;
	}

	/**
	 * Imposta l'identificativo dell'utente destinatario.
	 * 
	 * @param idUtenteDestinatario
	 *            Identificativo utente destinatario da impostare.
	 */
	protected void setIdUtenteDestinatario(final Integer idUtenteDestinatario) {
		this.idUtenteDestinatario = idUtenteDestinatario;
	}

}
