package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.Calendar;

import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.RicercaPerBusinessEntityEnum;

/**
 * The Class RicercaVeloceRequestDTO.
 *
 * @author CPIERASC
 * 
 * 	Dto per la ricerca di documenti.
 */
public class RicercaVeloceRequestDTO implements Serializable {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -3099779900367535323L;

	/**
	 * Anno di creazione.
	 */
	private Integer anno;
	
	/**
	 * Chiave di ricerca.
	 */
	private String key;
	
	/**
	 * Tipologia di ricerca.
	 */
	private String typeValue;

	/**
	 * Tipo.
	 */
	private RicercaGenericaTypeEnum type;
	
	/**
	 * Parametro ricerca.
	 */
	private String ricercaBEntityEnumValue;
	
	/**
	 * Parametro ricerca.
	 */
	private RicercaPerBusinessEntityEnum ricercaBEntityEnum;
	
	/**
	 * Funzionalità ricerca full-text.
	 */
	private boolean flagFullText;
	
	
	/**
	 * Costruttore.
	 */
	public RicercaVeloceRequestDTO() {
		super();
		this.anno = Calendar.getInstance().get(Calendar.YEAR);
	}

	/**
	 * Costruttore.
	 * 
	 * @param inAnno	anno creazione
	 * @param inKey		chiave ricerca
	 * @param inType	tipologia ricerca
	 */
	public RicercaVeloceRequestDTO(final Integer inAnno, final String inKey, final RicercaGenericaTypeEnum inType, final boolean inFlagFullText) {
		this.anno = inAnno;
		this.key = inKey;
		this.type = inType;
		this.flagFullText = inFlagFullText;
		if (inType != null) {
			typeValue = String.valueOf(inType.getKey());
		}
	}

	/**
	 * Getter anno creazione.
	 * 
	 * @return	anno creazione
	 */
	public final Integer getAnno() {
		return anno;
	}

	/**
	 * Setter anno ricerca.
	 * 
	 * @param inAnno	anno ricerca
	 */
	public final void setAnno(final Integer inAnno) {
		this.anno = inAnno;
	}

	/**
	 * Getter chiave ricerca.
	 * 
	 * @return	chiave ricerca
	 */
	public final String getKey() {
		return key;
	}

	/**
	 * Setter chiave ricerca.
	 * 
	 * @param inKey	chiave ricerca
	 */
	public final void setKey(final String inKey) {
		this.key = inKey;
	}

	/**
	 * @return the typeValue
	 */
	public final String getTypeValue() {
		return typeValue;
	}
	
	/**
	 * @param typeValue the typeValue to set
	 */
	public final void setTypeValue(final String typeValue) {
		this.typeValue = typeValue;
		type = RicercaGenericaTypeEnum.get(typeValue);
	}

	/**
	 * Getter tipo ricerca.
	 * 
	 * @return	tipo ricerca
	 */
	public final RicercaGenericaTypeEnum getType() {
		return type;
	}

	/**
	 * Setter tipo ricerca.
	 * 
	 * @param inType	tipo ricerca
	 */
	public final void setType(final RicercaGenericaTypeEnum inType) {
		this.type = inType;
		if (type != null) {
			typeValue = String.valueOf(type.getKey());
		}
	}

	/**
	 * Restituisce l'enum che definisce l'entita di ricerca.
	 * @return enum ricerca
	 */
	public RicercaPerBusinessEntityEnum getRicercaBEntityEnum() {
		return ricercaBEntityEnum;
	}

	/**
	 * Imposta l'enum che definisce l'entita di ricerca.
	 * @param ricercaBEntityEnum
	 */
	public void setRicercaBEntityEnum(final RicercaPerBusinessEntityEnum ricercaBEntityEnum) {
		this.ricercaBEntityEnum = ricercaBEntityEnum;
		if (ricercaBEntityEnum != null) {
			ricercaBEntityEnumValue = String.valueOf(ricercaBEntityEnum.getKey());
		}
	}

	/**
	 * Restituisce il valore dell'enum che definisce l'entita di ricerca.
	 * @return valore enum ricerca
	 */
	public String getRicercaBEntityEnumValue() {
		return ricercaBEntityEnumValue;
	}

	/**
	 * Imposta il valore dell'enum che definisce l'entita di ricerca.
	 * @param ricercaBEntityEnumValue
	 */
	public void setRicercaBEntityEnumValue(final String ricercaBEntityEnumValue) {
		this.ricercaBEntityEnumValue = ricercaBEntityEnumValue;
		ricercaBEntityEnum = RicercaPerBusinessEntityEnum.get(ricercaBEntityEnumValue);
	}

	/**
	 * @return the flagFullText
	 */
	public final boolean isFlagFullText() {
		return flagFullText;
	}

	/**
	 * @param flagFullText the flagFullText to set
	 */
	public final void setFlagFullText(final boolean flagFullText) {
		this.flagFullText = flagFullText;
	}
	
	
}
