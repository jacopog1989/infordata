package it.ibm.red.business.enums;

/**
 * The Enum TipoSpedizioneDocumentoEnum.
 *
 * @author CPIERASC
 * 
 *         Enum per le possibili tipologie di spedizione di un documento
 */
public enum TipologiaDestinatariEnum {

	/**
	 * Spedizione cartacea.
	 */
	CARTACEO(0),
	
	/**
	 * Spedizione PEO.
	 */
	PEO(1),
	
	/**
	 * Spedizione PEC.
	 */
	PEC(2),
	
	/**
	 * Spedizione mista.
	 */
	MISTO(2),
	
	/**
	 * Spedizione fax.
	 */
	FAX(3);
	
	/**
	 * Identificativo azione.
	 */
	private int id;
	
	
	/**
	 * Costruttore.
	 * 
	 * @param inId			identificativo
	 */
	TipologiaDestinatariEnum(final int inId) {
		this.id = inId;
	}


	/**
	 * Getter identificativo.
	 * 
	 * @return	identificativo
	 */
	public int getId() {
		return id;
	}

	
	/**
	 * @param idTipologiaDestinatario
	 * @return
	 */
	public static TipologiaDestinatariEnum getById(final int idTipologiaDestinatario) {
		TipologiaDestinatariEnum output = null;
		
		for (TipologiaDestinatariEnum tipologia : TipologiaDestinatariEnum.values()) {
			if (tipologia.getId() == idTipologiaDestinatario) {
				output = tipologia;
				break;
			}
		}
		
		return output;
	}
	
	/**
	 * @param nomeTipologia
	 * @return
	 */
	public static TipologiaDestinatariEnum getByNome(final String nomeTipologia) {
		TipologiaDestinatariEnum output = null;
		
		for (TipologiaDestinatariEnum tipologia : TipologiaDestinatariEnum.values()) {
			if (tipologia.name().equalsIgnoreCase(nomeTipologia)) {
				output = tipologia;
				break;
			}
		}
		
		return output;
	}
}
