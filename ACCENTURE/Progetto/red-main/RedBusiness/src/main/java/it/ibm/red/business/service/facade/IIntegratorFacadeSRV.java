package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.SicogeResultDTO;
import it.ibm.red.business.dto.SigiResultDTO;

/**
 * Facade del servizio integrator.
 */
public interface IIntegratorFacadeSRV extends Serializable {

	/**
	 * SICOGE - Salva su EVO gli OP dei decreti da lavorare recuperati da SICOGE.
	 * @param idAoo
	 * @return result
	 */
	SicogeResultDTO syncSICOGE(Integer idAoo);
	
	/**
	 * SIGI - Salva su EVO gli OP dei decreti da lavorare recuperati da SIGI.
	 * @param idAoo
	 * @return result
	 */
	SigiResultDTO syncSIGI(Integer idAoo);
	
}