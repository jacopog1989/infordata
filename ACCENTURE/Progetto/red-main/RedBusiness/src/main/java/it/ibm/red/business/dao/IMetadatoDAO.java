package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.MetadatoDTO;

/**
 * @author SLungarella
 * 
 * DAO per la gestione dei metadati.
 *
 */
public interface IMetadatoDAO extends Serializable {
	
	/**
	 * @param idTipoDocumento
	 * @param metadato
	 * @param connection
	 */
	int save(MetadatoDTO metadato, Connection connection);
	
	/**
	 * Carica tutti i metadati in riferimento ad una tipologia documento di riferimento.
	 * 
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param connection
	 * @return
	 */
	List<MetadatoDTO> caricaMetadati(int idTipologiaDocumento, int idTipoProcedimento, Connection connection);

	/**
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param connection
	 * @return
	 */
	List<MetadatoDTO> caricaMetadatiOut(int idTipologiaDocumento, int idTipoProcedimento, Connection connection);

	/**
	 * Recupera il metadato che rappresenta la tipologia documento del documento in entrata di riferimento (allaccio principale) per l'uscita.
	 * 
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param nomeMetadato
	 * @param connection
	 * @return
	 */
	MetadatoDTO getMetadatoTipologiaDocRiferimento(int idTipologiaDocumento, int idTipoProcedimento, String nomeMetadato, Connection connection);
	
	/**
	 * Salva la relazione tra tipologia documento, metadato e tipologia procedimento.
	 * @param connection
	 * @param idDocumento
	 * @param idProcedimento
	 * @param idMetadato
	 */
	void saveCrossDocumentoMetadatoProcedimento(Connection connection, int idDocumento, Long idProcedimento, int idMetadato);
	
}