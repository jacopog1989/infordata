package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

import it.ibm.red.business.dto.IconaStampigliaturaSegnoGraficoDTO;
import it.ibm.red.business.dto.StampigliaturaSegnoGraficoDTO;

/**
 * 
 * @author Vingenito
 *
 */
public interface IStampigliaturaSegnoGraficoDAO extends Serializable {
	
	/**
	 * Inserisce il placeholder da parte del job.
	 * @param idDocumento
	 * @param idUffIstruttore
	 * @param idUtente
	 * @param idTipoProc
	 * @param placeholder
	 * @param conn
	 * @return
	 */
	int insertOrUpdatePlaceholderJob(String idDocumento, Long idUffIstruttore, Long idUtente, Integer idTipoProc, List<String> placeholder, Connection conn);

	/**
	 * Ottiene il placeholder tramite l'id documento.
	 * @param idDocumento
	 * @param conn
	 * @return placeholder
	 */
	String getPlaceholderByIdDoc(String idDocumento, Connection conn);
	
	/**
	 * Ottiene l'icona tramite il placeholder.
	 * @param placeholder
	 * @param numDocumento
	 * @param conn
	 * @return icona
	 */
	IconaStampigliaturaSegnoGraficoDTO getIconByPlaceholder(String placeholder, String numDocumento, Connection conn);
	 
	/**
	 * Ottiene il tipo di firma.
	 * @param idIterApprovativo
	 * @param con
	 * @return tipo di firma
	 */
	Integer getTipoFirma(Integer idIterApprovativo, Connection con);
	
	/**
	 * Elimina il record dalla tabella documento_placeholder.
	 * @param idDocumento
	 * @param idUffIstruttore
	 * @param idUtente
	 * @param idTipoProc
	 * @param conn
	 * @return
	 */
	int deleteRowPlaceholderEmpty(String idDocumento, Long idUffIstruttore, Long idUtente, Integer idTipoProc, Connection conn); 
	
	/**
	 * Ottiene il placeholder di sigla.
	 * @param idUtente
	 * @param idUffIstruttore
	 * @param idTipoDoc
	 * @param conn
	 * @return mappa placeholder glifo
	 */
	HashMap<String, byte[]> getPlaceholderSegnoGrafico(Long idUtente,Long idUffIstruttore,Integer idTipoDoc,Connection conn);
	
	/**
	 * Ottiene il placeholder di sigla.
	 * @param idUtente
	 * @param idUffIstruttore
	 * @param idTipoDoc
	 * @param listStampigliaturaSegnoGrafico
	 * @param conn
	 * @return mappa placeholder glifo
	 */
	HashMap<String, byte[]> getPlaceholderSegnoGrafico(Long idUtente, Long idUffIstruttore, Integer idTipoDoc, List<StampigliaturaSegnoGraficoDTO> listStampigliaturaSegnoGrafico, Connection conn);

	/**
	 * Aggiorna il placeholder.
	 * @param idDocumento
	 * @param idUffIstruttore
	 * @param idTipoProc
	 * @param placeholder
	 * @param conn
	 * @return
	 */
	int updatePlaceholder(String idDocumento, Long idUffIstruttore, Integer idTipoProc, String placeholder,
			Connection conn);
}
