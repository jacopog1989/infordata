package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Date;

import it.ibm.red.business.dto.NotificaNpsDTO;

/**
 * The Class INotificaNpsDAO.
 * 
 * @author a.dilegge
 *
 *         Interfaccia del DAO per la gestione delle notifiche delle operazioni
 *         eseguite da NPS
 */
public interface INotificaNpsDAO extends Serializable {

	/**
	 * @param notifica
	 * @param con
	 * @return
	 */
	long inserisciNotificaInCoda(NotificaNpsDTO notifica, Connection conn);

	/**
	 * @param idAoo
	 * @param con
	 * @return
	 */
	NotificaNpsDTO getAndLockNextNotifica(Long idAoo, Connection con);

	/**
	 * @param idRichiesta
	 * @param stato
	 * @param errore
	 * @param con
	 * @return
	 */
	int aggiornaStatoNotifica(Long idRichiesta, Integer stato, String errore, Connection con);

	/**
	 * @param idRichiesta
	 * @param stato
	 * @param errore
	 * @param idDocumento
	 * @param con
	 * @return
	 */
	int aggiornaStatoNotifica(Long idRichiesta, Integer stato, String errore, String idDocumento, Connection con);

	/**
	 * Recupera la notifica notifica
	 * 
	 * @param idAoo
	 * @param numeroProtocollo
	 * @param annoProtocollo
	 * @param connection
	 * 
	 * @return notifica NPS
	 */
	NotificaNpsDTO getNotificaAzione(Long idAoo, Integer numeroProtocollo, Integer annoProtocollo, Connection connection);

	/**
	 * @param idCoda
	 * @param con
	 * @return
	 */
	NotificaNpsDTO getAndLockNotifica(Integer idCoda, Connection con);

	/**
	 * 
	 * @param idRichiesta
	 * @param numeroProtocollo
	 * @param annoProtocollo
	 * @param dataProtocollo
	 * @param documentTitle
	 * @param con
	 * @return
	 */
	int aggiornaDatiProtocollo(Long idRichiesta, String idProtocollo, Integer numeroProtocollo, Integer annoProtocollo, Date dataProtocollo, String documentTitle,
			Connection con);
}