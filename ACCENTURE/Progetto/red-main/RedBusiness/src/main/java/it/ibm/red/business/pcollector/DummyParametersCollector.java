package it.ibm.red.business.pcollector;

import java.util.Collection;

import it.ibm.red.business.dto.CollectedParametersDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Parameter collector dummy - necessario per la scrittura della logica di
 * recupero parameters collectors.
 * I collectors per le registrazioni ausiliarie vengono generati al runtime,
 * questo collector è l'unica implementazione esistente a compile time.
 */
public class DummyParametersCollector extends AbstractParametersCollector {

	/**
	 * @see it.ibm.red.business.pcollector.AbstractParametersCollector#collectParameters(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection, boolean).
	 */
	@Override
	public CollectedParametersDTO collectParameters(final UtenteDTO utente, final Collection<MetadatoDTO> listaMetadatiRegistro, final boolean checkConfiguration) {

		final CollectedParametersDTO out = new CollectedParametersDTO();

		for (final MetadatoDTO m : listaMetadatiRegistro) {
			out.addStringParam(m.getName(), getValue(m));
		}

		return out;
	}

	/**
	 * @see it.ibm.red.business.pcollector.AbstractParametersCollector#initParameters(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.util.Collection, boolean).
	 */
	@Override
	protected void initParameters(final UtenteDTO utente, final String documentTitle, final Collection<MetadatoDTO> out, final boolean setDefaultValue) {
		// Questo metodo deve essere implementato da un collector concreto.
	}

	/**
	 * @see it.ibm.red.business.pcollector.ParametersCollector#prepareMetadatiForDocUscita(java.util.Collection,
	 *      java.util.Collection, java.lang.String).
	 */
	@Override
	public Collection<MetadatoDTO> prepareMetadatiForDocUscita(final Collection<MetadatoDTO> metadatiToPrepare, final Collection<MetadatoDTO> listaMetadatiRegistro,
			final String nomeRegistro) {
		// Il dummyParametere non può gestire differenti tipologie di decumenti, quindi
		// si limita a restituire i metadati del doc in uscita così come ricevuti
		return metadatiToPrepare;
	}

}
