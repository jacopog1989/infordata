package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio di gestione conferma o annullamento.
 */
public interface IConfermaAnnullamentoFacadeSRV extends Serializable {

	/**
	 * Conferma l'annullamento del documento associato al wobNumber.
	 * @param utente
	 * @param wobNumber
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO confermaAnnullamento(UtenteDTO utente, String wobNumber);

	/**
	 * Conferma massiva dell'annullamento dei documenti associati ai wobNumber.
	 * @param utente
	 * @param wobNumber
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> confermaAnnullamento(UtenteDTO utente, Collection<String> wobNumber);

}