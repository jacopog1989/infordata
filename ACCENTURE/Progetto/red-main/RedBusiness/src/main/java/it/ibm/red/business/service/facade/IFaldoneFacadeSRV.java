package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.MasterFascicoloDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.EsitoFaldoneEnum;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.exception.SearchException;

/**
 * The Interface IFaldoneFacadeSRV.
 *
 * @author mcrescentini
 * 
 *         Facade faldone service.
 */
public interface IFaldoneFacadeSRV extends Serializable {

	/**
	 * Ottiene i faldoni.
	 * @param utente
	 * @param idUfficioSelezionato
	 * @param parentName
	 * @return faldoni
	 */
	Collection<FaldoneDTO> getFaldoni(UtenteDTO utente, Integer idUfficioSelezionato, String parentName);

	/**
	 * Ottiene i fascicoli del faldone.
	 * @param utente
	 * @param pathFaldone
	 * @return fascicoli master
	 */
	Collection<MasterFascicoloDTO> getFascicoliFaldone(UtenteDTO utente, String pathFaldone);

	/**
	 * Crea il faldone.
	 * @param utente
	 * @param oggetto
	 * @param descrizione
	 * @param faldonePadre
	 * @param canBeDuplicated
	 * @return nome del faldone
	 */
	String createFaldone(UtenteDTO utente, String oggetto, String descrizione, String faldonePadre, Boolean canBeDuplicated);

	/**
	 * Aggiorna il faldone.
	 * @param utente
	 * @param nameFaldone
	 * @param nuovoOggetto
	 * @param nuovaDescrizione
	 * @param canBeDuplicated
	 */
	void updateFaldone(UtenteDTO utente, String nameFaldone, String nuovoOggetto, String nuovaDescrizione, Boolean canBeDuplicated);

	/**
	 * Esegue la faldonatura.
	 * @param utente
	 * @param nomeFaldone
	 * @param wobNumber
	 * @param idFascicolo
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO faldona(UtenteDTO utente, String nomeFaldone, String wobNumber, String idFascicolo);
	
	/**
	 * Esegue la faldonatura massiva.
	 * @param nomeFaldone
	 * @param wobNumbers
	 * @param utente
	 * @param idFascicolo
	 * @return esiti dell'operazione
	 */
	List<EsitoOperazioneDTO> faldona(String nomeFaldone, List<String> wobNumbers, UtenteDTO utente, String idFascicolo);
	
	/**
	 * Ricerca i faldoni.
	 * @param key
	 * @param type
	 * @param utente
	 * @return faldoni
	 * @throws SearchException
	 */
	Collection<FaldoneDTO> ricercaFaldoni(String key, RicercaGenericaTypeEnum type, UtenteDTO utente) throws SearchException;
	
	/**
	 * Ottiene la gerarchia.
	 * @param listaFaldoni
	 * @param utente
	 * @return lista di faldoni
	 */
	Collection<FaldoneDTO> getGerarchia(Collection<FaldoneDTO> listaFaldoni, UtenteDTO utente);

	/**
	 * Cancella il faldone.
	 * @param utente
	 * @param idUfficioSelezionato
	 * @param pathFaldone
	 * @param documentTitle
	 */
	void delete(UtenteDTO utente, Integer idUfficioSelezionato, String pathFaldone, String documentTitle);

	/**
	 * Rimuove l'associazione del fascicolo al faldone.
	 * @param nomeFaldone
	 * @param idFascicolo
	 * @param idAOO
	 * @param utente
	 * @return esito
	 */
	EsitoFaldoneEnum disassociaFascicoloDaFaldone(String nomeFaldone, String idFascicolo, Long idAOO, UtenteDTO utente);
	
	/**
	 * Verifica se l'associazione al faldone può essere rimossa.
	 * @param nomeFaldone
	 * @param idFascicolo
	 * @param idAOO
	 * @param utente
	 * @return true o false
	 */
	boolean isFaldoneDisassociabile(String nomeFaldone, String idFascicolo, Long idAOO, UtenteDTO utente);
	
	/**
	 * Faldona il fascicolo.
	 * @param nomeFaldone
	 * @param idFascicolo
	 * @param idAoo
	 * @param utente
	 * @return true o false
	 */
	boolean faldonaFascicolo(String nomeFaldone, String idFascicolo, Long idAoo, UtenteDTO utente);

	/**
	 * Ottiene il faldone.
	 * @param documentTitle
	 * @param utente
	 * @return faldone
	 */
	FaldoneDTO getFaldone(String documentTitle, UtenteDTO utente);
}
