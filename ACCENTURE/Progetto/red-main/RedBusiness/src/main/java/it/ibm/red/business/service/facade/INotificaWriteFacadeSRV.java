package it.ibm.red.business.service.facade;

import java.io.Serializable;

/**
 * Facade del servizio di scrittura notifiche.
 */
public interface INotificaWriteFacadeSRV extends Serializable {
	
	/**
	 * Il metodo si occupa di registrare le notifiche degli eventi gestiti direttamente dall'applicativo (non dal PE).
	 * 
	 * @param idAoo
	 */
	void writeNotifiche(int idAoo);
	
	/**
	 * Il metodo si occupa di registrare le notifiche degli eventi gestiti direttamente dall'applicativo (non dal PE).
	 * 
	 * @param idAoo
	 * @param oggetto utilizzato dal getDataDocument
	 */
	void writeNotifiche(int idAoo, Object objectForGetDataDocument);
}