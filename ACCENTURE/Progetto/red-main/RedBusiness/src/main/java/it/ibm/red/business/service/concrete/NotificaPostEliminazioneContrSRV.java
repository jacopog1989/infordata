package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IContributoDAO;
import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.EventoSottoscrizioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.service.INotificaPostEliminazioneSRV;
import it.ibm.red.business.service.ISottoscrizioniSRV; 

/**
 * Service notifica post eliminazione contr.
 */
@Service
public class NotificaPostEliminazioneContrSRV extends NotificaWriteAbstractSRV implements INotificaPostEliminazioneSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 3278773255133187499L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotificaPostEliminazioneContrSRV.class.getName());

	/**
	 * DAO.
	 */
	@Autowired
	private IContributoDAO contributoDAO;

	/**
	 * Service.
	 */
	@Autowired
	private ISottoscrizioniSRV sottoscrizioniSRV;
	
	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getSottoscrizioni(int).
	 */
	@Override
	protected List<EventoSottoscrizioneDTO> getSottoscrizioni(final int idAoo) {
		Connection con = null;
		List<EventoSottoscrizioneDTO> eventi = null;
		
		try {
			final Integer[] idEventi = new Integer[]{ Integer.parseInt(EventTypeEnum.ELIMINAZIONE_CONTRIBUTO.getValue()) };
		
			con = setupConnection(getFilenetDataSource().getConnection(), false);
			eventi = sottoscrizioniSRV.getEventiSottoscrittiByListaEventi(idAoo, idEventi, con);			
		
		} catch (final Exception e) {
			LOGGER.error("Errore in fase recupero delle sottoscrizioni per l'evento per l'aoo " + idAoo, e);
		} finally {
			closeConnection(con);
		}
		
		return eventi;
	}
	
	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getDataDocumenti(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.persistence.model.Aoo, int, java.lang.Object).
	 */
 	@Override
 	protected List<InfoDocumenti> getDataDocumenti(final UtenteDTO utente, final Aoo aoo, final int idEvento, final Object objectForGetDataDocument) {
 		ContributoDTO contributo = null;
 		final List<InfoDocumenti> documenti = new ArrayList<>();
 		
 		Connection con = null;
  		
 		try {
 			
 			final Long idContributo = (Long) objectForGetDataDocument;
 			
			con = setupConnection(getDataSource().getConnection(), false);
			
			contributo = contributoDAO.getContributoSottoscrizioniByIDandAOO(idContributo, aoo.getIdAoo(), con);
			
 			if (contributo != null) {
	 			final InfoDocumenti info = new InfoDocumenti();
	 			info.setIdDocumento(contributo.getIdDocumento());
	 			documenti.add(info);
 			}
 		} catch (final Exception e) {
			LOGGER.error("Errore in fase recupero dei contributi [" + utente.getId() + "," + utente.getIdUfficio() + "] l'aoo " + aoo.getIdAoo(), e);
		} finally {
			closeConnection(con);
		}
		
		return documenti;
 	}
 	
 	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getDataDocumenti(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.persistence.model.Aoo, int).
	 */
 	@Override
	protected List<InfoDocumenti> getDataDocumenti(final UtenteDTO utente, final Aoo aoo, final int idEvento) {
		throw new RedException("Metodo non previsto per il servizio ");
	}
	
}