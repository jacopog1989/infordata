package it.ibm.red.business.helper.pdf;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import it.ibm.red.business.logger.REDLogger;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;

import it.ibm.red.business.exception.PdfHelperException;

/**
 * The Class DataManager.
 *
 * @author CPIERASC
 * 
 *         Oggetto per la gestione dei dati del pdf.
 */
public class DataManager {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DataManager.class.getName());

	/**
	 * Stream di output del pdf.
	 */
	private ByteArrayOutputStream baos;

	/**
	 * Reader del pdf.
	 */
	private PdfReader reader;

	/**
	 * Stamper del pdf.
	 */
	private PdfStamper stamper;

	/**
	 * Writer del pdf.
	 */
	private PdfWriter writer;

	/**
	 * Costruttore.
	 * 
	 * @param source		content file
	 * @param appendMode	flag che richiede la creazione di una nuova versione del documento se impostato a true
	 */
	public DataManager(final byte[] source, final Boolean appendMode) {
    	try {
	    	baos = new ByteArrayOutputStream();
			reader = new PdfReader(source);
	    	//True per non invalidare la firma
		    stamper = new PdfStamper(reader, baos, '\0', appendMode);
		    writer = stamper.getWriter();
		} catch (Exception e) {
			LOGGER.error(e);
			throw new PdfHelperException(e);
		}
	}

	/**
	 * Costruttore.
	 * 
	 * @param source	content del pdf
	 */
	public DataManager(final byte[] source) {
		this(source, true);
	}

	/**
	 * Metodo per la scrittura di dati su di un nuovo pdf di cui dare il path.
	 * 
	 * @param data	dati da scrivere nel pdf
	 * @param path	path nuovo pdf
	 */
    public static void write(final byte [] data, final String path) {
    	
    	try (
    		FileOutputStream fileOuputStream = new FileOutputStream(path);
    	) {
		    fileOuputStream.write(data);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new PdfHelperException(e);
		}
    }

    /**
     * Getter reader.
     * 
     * @return	reader
     */
    public final PdfReader getReader() {
		return reader;
	}
    
    /**
     * Getter stamper.
     * 
     * @return	stamper
     */
    public final PdfStamper getStamper() {
		return stamper;
	}
    
    /**
     * Getter writer.
     * 
     * @return	writer
     */
    public final PdfWriter getWriter() {
		return writer;
	}
    
    /**
     * Metodo per la chiusura dello stamper e del reader.
     * 
     * @return	contenuto
     */
    public final byte[] close() {
    	try {
		    stamper.close();
	        reader.close();
	    	return baos.toByteArray();
		} catch (Exception e) {
			LOGGER.error(e);
			throw new PdfHelperException(e);
		} finally {
			try {
				baos.flush();
				baos.close();
			} catch (IOException ioe) {
				LOGGER.error(ioe);
			}
		}
	}

}
