/**
 * 
 */
package it.ibm.red.business.service.ws;

import it.ibm.red.business.service.ws.facade.IContattoWsFacadeSRV;

/**
 * @author APerquti
 *
 */
public interface IContattoWsSRV extends IContattoWsFacadeSRV {

}
