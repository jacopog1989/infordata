package it.ibm.red.business.enums;

/**
 * Tipi di accesso FEPA.
 */
public enum TipoAccessoFepa3 {

	/**
	 * Bilancio enti.
	 */
	BILENTI(PropertiesNameEnum.BILENTI_APPLICAZIONE_WS, PropertiesNameEnum.BILENTI_ORGID, PropertiesNameEnum.BILENTI_USERID,
			PropertiesNameEnum.BILENTI_USERTYPE, PropertiesNameEnum.BILENTI_CLIENTID, PropertiesNameEnum.BILENTI_PASSWORD,
			PropertiesNameEnum.BILENTI_SERVICE),

	/**
	 * Revisori IGF.
	 */
	REVISORI_IGF(PropertiesNameEnum.REVISORI_IGF_APPLICAZIONE_WS, PropertiesNameEnum.REVISORI_IGF_ORGID, PropertiesNameEnum.REVISORI_IGF_USERID,
			PropertiesNameEnum.REVISORI_IGF_USERTYPE, PropertiesNameEnum.REVISORI_IGF_CLIENTID, PropertiesNameEnum.REVISORI_IGF_PASSWORD,
			PropertiesNameEnum.REVISORI_IGF_SERVICE),

	/**
	 * FEPA.
	 */
	FEPA(PropertiesNameEnum.FEPA_APPLICAZIONE, PropertiesNameEnum.FEPA_ORGID, PropertiesNameEnum.FEPA_USER,
			PropertiesNameEnum.FEPA_TIPOUTENTE, PropertiesNameEnum.FEPA_CLIENT, PropertiesNameEnum.FEPA_PASSWORD,
			PropertiesNameEnum.FEPA_SERVICE),

	/**
	 * SICOGE.
	 */
	SICOGE(PropertiesNameEnum.SICOGE_APPLICAZIONE_WS, PropertiesNameEnum.SICOGE_ORGID, PropertiesNameEnum.SICOGE_USERID,
			PropertiesNameEnum.SICOGE_USERTYPE, PropertiesNameEnum.SICOGE_CLIENTID, PropertiesNameEnum.SICOGE_PASSWORD,
			PropertiesNameEnum.SICOGE_SERVICE);
	
	/**
	 * Applicazione.
	 */
	private PropertiesNameEnum applicazione;

	/**
	 * Organigramma.
	 */
	private PropertiesNameEnum orgID;

	/**
	 * Utente.
	 */
	private PropertiesNameEnum utente;

	/**
	 * Tipo utente.
	 */
	private PropertiesNameEnum tipoUtente;

	/**
	 * Client.
	 */
	private PropertiesNameEnum client;

	/**
	 * Password.
	 */
	private PropertiesNameEnum password;

	/**
	 * Servizio.
	 */
	private PropertiesNameEnum servizio;

	/**
	 * Costruttore di default.
	 * @param applicazione
	 * @param orgID
	 * @param utente
	 * @param tipoUtente
	 * @param client
	 * @param password
	 * @param servizio
	 */
	TipoAccessoFepa3(final PropertiesNameEnum applicazione, final PropertiesNameEnum orgID, final PropertiesNameEnum utente,
			final PropertiesNameEnum tipoUtente, final PropertiesNameEnum client, final PropertiesNameEnum password,
			final PropertiesNameEnum servizio) {
		this.applicazione = applicazione;
		this.orgID = orgID;
		this.utente = utente;
		this.tipoUtente = tipoUtente;
		this.client = client;
		this.password = password;
		this.servizio = servizio;
	}

	/**
	 * @return applicazione
	 */
	public PropertiesNameEnum getApplicazione() {
		return applicazione;
	}

	/**
	 * @return orgID
	 */
	public PropertiesNameEnum getOrgID() {
		return orgID;
	}

	/**
	 * @return utente
	 */
	public PropertiesNameEnum getUtente() {
		return utente;
	}

	/**
	 * @return tipoUtente
	 */
	public PropertiesNameEnum getTipoUtente() {
		return tipoUtente;
	}

	/**
	 * @return client
	 */
	public PropertiesNameEnum getClient() {
		return client;
	}

	/**
	 * @return password
	 */
	public PropertiesNameEnum getPassword() {
		return password;
	}

	/**
	 * @return servizio
	 */
	public PropertiesNameEnum getServizio() {
		return servizio;
	}
	
}
