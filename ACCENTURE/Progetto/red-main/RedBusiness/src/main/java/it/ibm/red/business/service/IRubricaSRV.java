package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.RicercaRubricaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.service.facade.IRubricaFacadeSRV;

/**
 * Interfaccia del servizio di gestione rubrica.
 */
public interface IRubricaSRV extends IRubricaFacadeSRV {

	/**
	 * Ottiene i contatti preferiti.
	 * @param idUfficio
	 * @param idAoo
	 * @returnn lista di contatti
	 */
	@Override
	List<Contatto> getPreferiti(Long idUfficio, Long idAOO);

	/**
	 * Recupera l'id contatto associato alla email in input per l'aoo in input.
	 * Se il contatto non c'è, si occupa di crearne uno con dati fittizi.
	 * 
	 * @param email
	 * @param idAoo
	 * @return
	 */
	Contatto retrieveContattoFromMail(String email, Integer idAoo);

	/**
	 * @param idContatto
	 * @param connection
	 * @return
	 */
	Contatto getContattoByID(Long idContatto, Connection connection);

	/**
	 * @param idUfficio
	 * @param inserisciContattoItem
	 * @param connection
	 * @return
	 */
	Long inserisci(Long idUfficio, Contatto inserisciContattoItem, Connection connection, boolean checkUnivocitaMail);

	/**
	 * @param contatto
	 * @param idContatto
	 * @param tipoRubrica
	 * @param utenteCreatore
	 * @param connection
	 */
	void inserisciNotificaCreazioneAutomaticaContatto(Contatto contatto, Long idContatto, String tipoRubrica, UtenteDTO utenteCreatore, Connection connection);

	/**
	 * @param idUfficio
	 * @param ricercaContattoObj
	 * @param connection
	 * @return
	 */
	List<Contatto> ricerca(Long idUfficio, RicercaRubricaDTO ricercaContattoObj, Connection connection);

	/**
	 * Inserisce il contatto on the fly.
	 * @param contatto
	 * @param idAoo
	 * @return contatto
	 */
	Contatto insertContattoOnTheFly(Contatto contatto, Long idAoo);

	/**
	 * Fetch contatti tramite id.
	 * @param objectIdCont
	 * @param aoo
	 * @return lista di contatti
	 */
	List<Contatto> fetchContattiById(String objectIdCont, Aoo aoo);
	 
	/**
	 * Elimina i contatti on the fly in un range temporale.
	 * @param aoo
	 */
	void eliminaContattiOnTheFlyRangeTemporale(Aoo aoo);

	/**
	 * Inserisce il contatto a flusso automatico.
	 * @param inContatto
	 * @param utenteCreatore
	 * @param conn
	 * @return contatto
	 */
	Contatto insertContattoFlussoAutomatico(Contatto inContatto, UtenteDTO utenteCreatore, Connection conn);
	
}
