/**
 * 
 */
package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.Date;

import it.ibm.red.business.persistence.model.LookupTable;

/**
 * @author APerquoti
 *
 */
public interface ILookupTableDAO extends Serializable {

	/**
	 * Metodo per recuperare i selettori attivi per una specifica AOO.
	 * 
	 * @param idAoo
	 * @return
	 */
	Collection<String> getSelectorByIdAoo(long idAoo, Connection con);
	
	/**
	 * Metodo per recuperare i valori definiti attraverso uno specifico selettore.
	 * 
	 * @param idAoo
	 * @return
	 */
	Collection<LookupTable> getValuesBySelector(String selector, Date dateTarget, Long idAoo, Connection con);

	/**
	 * @param key
	 * @param con
	 * @return
	 */
	String getDescByPk(Integer key, Connection con);

	/**
	 * @param selettore
	 * @param valore
	 * @param idAoo
	 * @param con
	 * @return
	 */
	LookupTable getIdBySelectorAndValueAndAoo(String selettore, String valore, Long idAoo, Connection con);
	
	/**
	 * Metodo per il salvataggio di una lookuptable nella tabella LOOKUPTABLE a partire dal suo model
	 * 
	 * @param lookupTable la lookupTable da salvare in DB
	 * @param connection
	 * */
	void saveLookupTable(LookupTable lookupTable, Connection connection);
	
	/**
	 * Metodo per il sollecitamento della sequenza associata alla tabella LOOKUPTABLE: SEQ_LOOKUPTABLE
	 * 
	 * @param connection
	 * */
	long getNextValFromSequence(Connection connection);
	
	/**
	 * Restituisce le lookup tables presenti sul database per uno specifico AOO
	 * @param idAoo 	Se NULL vengono recuperate le liste utenti in comune per tutti gli AOO
	 * @param connection
	 * @return 			Liste utenti
	 * */
	Collection<LookupTable> getLookupTables(Long idAoo, Connection connection);
}
