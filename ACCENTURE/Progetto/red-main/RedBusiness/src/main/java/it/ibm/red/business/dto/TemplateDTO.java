package it.ibm.red.business.dto;

/**
 * Mappa la tabella TEMPLATE.
 * 
 * @author SLac
 */
public class TemplateDTO extends AbstractDTO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 7307294604860930799L;

	/**
	 * Identificativo del template.
	 */
	private String idTemplate;

	/**
	 * Descrizione del template.
	 */
	private String descrizione;

	/**
	 * Costruttore vuoto.
	 */
	public TemplateDTO() { }

	/**
	 * Costruttore di default.
	 * @param idTemplate
	 * @param descrizione
	 */
	public TemplateDTO(final String idTemplate, final String descrizione) {
		this.idTemplate = idTemplate;
		this.descrizione = descrizione;
	}

	/**
	 * Restituisce l'id del template.
	 * @return idTemplate
	 */
	public String getIdTemplate() {
		return idTemplate;
	}

	/**
	 * Imposta l'id del template.
	 * @param idTemplate
	 */
	public void setIdTemplate(final String idTemplate) {
		this.idTemplate = idTemplate;
	}

	/**
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Imposta la descrizione del template.
	 * @param descrizione
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idTemplate == null) ? 0 : idTemplate.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final TemplateDTO other = (TemplateDTO) obj;
		if (idTemplate == null) {
			if (other.idTemplate != null) {
				return false;
			}
		} else if (!idTemplate.equals(other.idTemplate)) {
			return false;
		}
		return true;
	}
}