package it.ibm.red.business.service;

import java.util.List;

import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.FunzionalitaEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.service.facade.ICasellePostaliFacadeSRV;

/**
 * Interface del service per la gestione delle caselle postali.
 */
public interface ICasellePostaliSRV extends ICasellePostaliFacadeSRV {

	/**
	 * Ottiene le caselle postali.
	 * @param utente
	 * @param fceh
	 * @param f - funzionalità
	 * @return lista di caselle postali
	 */
	List<CasellaPostaDTO> getCasellePostali(UtenteDTO utente, IFilenetCEHelper fceh, FunzionalitaEnum f);
	
	/**
	 * Ottiene la casella postale.
	 * @param fceh
	 * @param nomeCasella - nome della casella postale
	 * @return casella postale
	 */
	CasellaPostaDTO getCasellaPostale(IFilenetCEHelper fceh, String nomeCasella);
	
}