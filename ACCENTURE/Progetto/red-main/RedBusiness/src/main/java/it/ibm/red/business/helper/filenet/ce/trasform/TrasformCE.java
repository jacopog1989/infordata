package it.ibm.red.business.helper.filenet.ce.trasform;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import javax.sql.DataSource;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.collection.PageIterator;
import com.filenet.api.core.Document;

import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextAllegatoTrasformerCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextTrasformerCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;

/**
 * The Class TrasformCE.
 *
 * @author CPIERASC
 * 
 *         Classe per la trasformazione degli oggetti CE.
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public abstract class TrasformCE {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TrasformCE.class.getName());
	
	/**
	 * Costruttore.
	 */
	private TrasformCE() {
	}
	
	/**
	 * Metodo per la chiusura della connessione.
	 * 
	 * @param connection - Connessione al DB
	 */
	private static void closeConnection(final Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (final SQLException e) {
				LOGGER.error("Errore durante la chiusura della connessione: ",  e);
			}
		}
	}
	
	/**
	 * Getter della connessione.
	 * 
	 * @return	connessione
	 */
	private static Connection getConnection() {
		Connection connection = null;
		try {
			connection = ((DataSource) ApplicationContextProvider.getApplicationContext().getBean("DataSource")).getConnection();
			connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			connection.setAutoCommit(true);
		} catch (final Exception e) {
			closeConnection(connection);
			LOGGER.error("Errore nel recupero della connessione al DB:", e);
			throw new RedException("Errore nel recupero della connessione al DB.", e);
		}
	    return connection;
	}

	/**
	 * Metodo per la trasformazione di un document set in un insieme di oggetti.
	 * 
	 * @param <T>			tipologia degli oggetti restituiti
	 * @param documentSet	insieme dei documenti
	 * @param te			tipologia di trasformazione
	 * @return				collezione degli oggetti trasformati
	 */
	public static <T> Collection<T> transform(final DocumentSet documentSet, final TrasformerCEEnum te) {
		Connection connection =  null;
		try {
			final TrasformerCE trasformer = TrasformerCEFactory.getInstance().getTrasformer(te);
			connection = getConnection();
			return trasformer.trasform(documentSet, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il trasform della lista di documenti: ", e);
			throw new RedException("Errore durante il trasform della lista di documenti. ", e);
		} finally {
			closeConnection(connection);
		}
	}
	

	/**
	 * Metodo per la trasformazione di un document set in un insieme di oggetti.
	 * Sfrutta la connesione precedentemente aperta.
	 * 
	 * @param <T>			tipologia degli oggetti restituiti
	 * @param documentSet	insieme dei documenti
	 * @param te			tipologia di trasformazione
	 * @param connection	connesione al db.
	 * @return				collezione degli oggetti trasformati
	 */
	public static <T> Collection<T> transform(final DocumentSet documentSet, final TrasformerCEEnum te, final Connection connection) {
		try {
			final TrasformerCE trasformer = TrasformerCEFactory.getInstance().getTrasformer(te);
			return trasformer.trasform(documentSet, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il trasform della lista di documenti: ", e);
			throw new RedException("Errore durante il trasform della lista di documenti. ", e);
		}
	}
	
	/**
	 * Gestisce la logica di trasformazione.
	 * @param documentSet
	 * @param te
	 * @param context
	 * @return oggetti trasformati
	 */
	public static <T> Collection<T> transform(final DocumentSet documentSet, final TrasformerCEEnum te, final Map<ContextTrasformerCEEnum, Object> context) {
		final ContextTrasformerCE trasformer = (ContextTrasformerCE) TrasformerCEFactory.getInstance().getTrasformer(te);
		return trasformer.trasform(documentSet, context);
	}
	
	/**
	 * Gestisce la logica di trasformazione.
	 * @param document
	 * @param te
	 * @param context
	 * @return oggetti trasformati
	 */
	public static <T> T transform(final Document document, final TrasformerCEEnum te, final Map<ContextTrasformerCEEnum, Object> context) {
		final ContextTrasformerCE trasformer = (ContextTrasformerCE) TrasformerCEFactory.getInstance().getTrasformer(te);
		return (T) trasformer.trasform(document, context);
	}

	/**
	 * Gestisce la logica di trasformazione.
	 * @param document
	 * @param te
	 * @param context
	 * @param connection
	 * @return oggetti trsformati
	 */
	public static <T> T transform(final Document document, final TrasformerCEEnum te, final Map<ContextTrasformerCEEnum, Object> context, final Connection connection) {
		final ContextTrasformerCE trasformer = (ContextTrasformerCE) TrasformerCEFactory.getInstance().getTrasformer(te);
		return (T) trasformer.trasform(document, connection, context);
	}
	
	/**
	 * Gestisce la logica di trasformazione.
	 * @param allegato
	 * @param principale
	 * @param te
	 * @param context
	 * @return oggetti trasformati
	 */
	public static <T> T transform(final Document allegato, final Document principale, final TrasformerCEEnum te, final Map<ContextTrasformerCEEnum, Object> context) {
		final ContextAllegatoTrasformerCE trasformer = (ContextAllegatoTrasformerCE) TrasformerCEFactory.getInstance().getTrasformer(te);
		return (T) trasformer.trasform(allegato, principale, context);
	}

	
	/**
	 * Metodo per la trasformazione di un documento in un oggetto.
	 * 
	 * @param <T>		tipologia degli oggetti restituiti
	 * @param document	documento da trasformare
	 * @param te		trasformazione
	 * @return			oggetto trasformato
	 */
	public static <T> T transform(final Document document, final TrasformerCEEnum te) {
		final TrasformerCE trasformer = TrasformerCEFactory.getInstance().getTrasformer(te);
		Connection connection = null;
		try {
			connection = getConnection();
			return (T) trasformer.trasform(document, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero delle informazioni del documento.", e);
			throw new FilenetException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Fuziona esattamente come il metodo transform per documento singolo 
	 * ma gli viene passata la connessione.
	 * 
	 * @param document
	 * @param te
	 * @param connection
	 * @return
	 */
	public static <T> T transform(final Document document, final TrasformerCEEnum te, final Connection connection) {
		final TrasformerCE trasformer = TrasformerCEFactory.getInstance().getTrasformer(te);
		return (T) trasformer.trasform(document, connection);
	}

	/**
	 * Gestisce la logica di trasformazione.
	 * @param pi
	 * @param te
	 * @param connection
	 * @return oggetti trasformati
	 */
	public static <T> Collection<T> transform(final PageIterator pi, final TrasformerCEEnum te, final Connection connection) {
		final TrasformerCE trasformer = TrasformerCEFactory.getInstance().getTrasformer(te);
		return trasformer.trasform(pi, connection);
	}

	/**
	 * Gestisce la logica di trasformazione.
	 * @param pi
	 * @param te
	 * @param context
	 * @return oggetti trasformati
	 */
	public static <T> Collection<T> transform(final PageIterator pi, final TrasformerCEEnum te, final Map<ContextTrasformerCEEnum, Object> context) {
		final ContextTrasformerCE trasformer = (ContextTrasformerCE) TrasformerCEFactory.getInstance().getTrasformer(te);
		return trasformer.trasform(pi, context);
	}

}