package it.ibm.red.business.dto;

import it.ibm.red.business.persistence.model.NpsConfiguration;

/**
 * Classe ParamNpsDfWSDTO.
 */
public class ParamNpsDfWSDTO extends AbstractDTO {

	private static final long serialVersionUID = -2138863763980906742L;

	/**
	 * Utente Protocollatore 
	 */
	private UtenteProtocollatoreNpsDfWSDTO utenteProtocollatoreDTO;
	
	/**
	 * Assegnatario Competenza Destinatario NpsDfWSDTO.
	 */
	private AssCompetenzaDestNpsDfWSDTO assCompetenzaDestNpsDfWSDTO;
	
	/**
	 * Assegnatario Per Competenza.
	 */
	private String assegnatarioPerCompetenza;
	
	/**
	 * acl.
	 */
	private String acl;
	
	/**
	 * Tipo Protocollo.
	 */
	private String tipoProtocollo;
	
	/**
	 * Configurazione Nps.
	 */
	private NpsConfiguration configurazioneNps;
	
	/**
	 * Mezzo di Ricezione.
	 */
	private String mezzoRicezione; 
	
	/**
	 * Tipo Mittente.
	 */
	private String tipoMittente;
	
	/**
	 * Nome Mittente.
	 */
	private String nomeMittente;
	
	/**
	 * Cognome Mittente.
	 */
	private String cognomeMittente;
	
	/**
	 * Codice Fiscale Mittente.
	 */
	private String cfMittente;
	
	/** 
	 *
	 * @return the utente protocollatore DTO
	 */
	public UtenteProtocollatoreNpsDfWSDTO getUtenteProtocollatoreDTO() {
		return utenteProtocollatoreDTO;
	}
	
	/** 
	 *
	 * @param utenteProtocollatoreDTO the new utente protocollatore DTO
	 */
	public void setUtenteProtocollatoreDTO(final UtenteProtocollatoreNpsDfWSDTO utenteProtocollatoreDTO) {
		this.utenteProtocollatoreDTO = utenteProtocollatoreDTO;
	}
	
	/** 
	 *
	 * @return the assegnatario per competenza
	 */
	public String getAssegnatarioPerCompetenza() {
		return assegnatarioPerCompetenza;
	}
	
	/** 
	 *
	 * @param assegnatarioPerCompetenza the new assegnatario per competenza
	 */
	public void setAssegnatarioPerCompetenza(final String assegnatarioPerCompetenza) {
		this.assegnatarioPerCompetenza = assegnatarioPerCompetenza;
	}
	
	/** 
	 *
	 * @return the acl
	 */
	public String getAcl() {
		return acl;
	}
	
	/** 
	 *
	 * @param acl the new acl
	 */
	public void setAcl(final String acl) {
		this.acl = acl;
	}
	
	/** 
	 *
	 * @return the tipo protocollo
	 */
	public String getTipoProtocollo() {
		return tipoProtocollo;
	}
	
	/** 
	 *
	 * @param tipoProtocollo the new tipo protocollo
	 */
	public void setTipoProtocollo(final String tipoProtocollo) {
		this.tipoProtocollo = tipoProtocollo;
	}
	
	/** 
	 *
	 * @return the configurazione nps
	 */
	public NpsConfiguration getConfigurazioneNps() {
		return configurazioneNps;
	}
	
	/** 
	 *
	 * @param configurazioneNps the new configurazione nps
	 */
	public void setConfigurazioneNps(final NpsConfiguration configurazioneNps) {
		this.configurazioneNps = configurazioneNps;
	}
	
	/** 
	 *
	 * @return the ass competenza dest nps df WSDTO
	 */
	public AssCompetenzaDestNpsDfWSDTO getAssCompetenzaDestNpsDfWSDTO() {
		return assCompetenzaDestNpsDfWSDTO;
	}
	
	/** 
	 *
	 * @param assCompetenzaDestNpsDfWSDTO the new ass competenza dest nps df WSDTO
	 */
	public void setAssCompetenzaDestNpsDfWSDTO(final AssCompetenzaDestNpsDfWSDTO assCompetenzaDestNpsDfWSDTO) {
		this.assCompetenzaDestNpsDfWSDTO = assCompetenzaDestNpsDfWSDTO;
	}
	
	/** 
	 *
	 * @return the mezzo ricezione
	 */
	public String getMezzoRicezione() {
		return mezzoRicezione;
	}
	
	/** 
	 *
	 * @param mezzoRicezione the new mezzo ricezione
	 */
	public void setMezzoRicezione(final String mezzoRicezione) {
		this.mezzoRicezione = mezzoRicezione;
	}
	
	/** 
	 *
	 * @return the tipo mittente
	 */
	public String getTipoMittente() {
		return tipoMittente;
	}
	
	/** 
	 *
	 * @param tipoMittente the new tipo mittente
	 */
	public void setTipoMittente(final String tipoMittente) {
		this.tipoMittente = tipoMittente;
	}
	
	/** 
	 *
	 * @return the nome mittente
	 */
	public String getNomeMittente() {
		return nomeMittente;
	}
	
	/** 
	 *
	 * @param nomeMittente the new nome mittente
	 */
	public void setNomeMittente(final String nomeMittente) {
		this.nomeMittente = nomeMittente;
	}
	
	/** 
	 *
	 * @return the cognome mittente
	 */
	public String getCognomeMittente() {
		return cognomeMittente;
	}
	
	/** 
	 *
	 * @param cognomeMittente the new cognome mittente
	 */
	public void setCognomeMittente(final String cognomeMittente) {
		this.cognomeMittente = cognomeMittente;
	}
	
	/** 
	 *
	 * @return the cf mittente
	 */
	public String getCfMittente() {
		return cfMittente;
	}
	
	/** 
	 *
	 * @param cfMittente the new cf mittente
	 */
	public void setCfMittente(final String cfMittente) {
		this.cfMittente = cfMittente;
	}
}
