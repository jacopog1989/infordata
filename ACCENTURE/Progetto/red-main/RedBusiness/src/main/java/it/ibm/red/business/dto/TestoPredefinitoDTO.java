package it.ibm.red.business.dto;

import it.ibm.red.business.enums.TestoPredefinitoEnum;

/**
 * @author SLac
 *
 * Tabella NSD.TESTOPREDEFINITO
 */
public class TestoPredefinitoDTO extends AbstractDTO {

	private static final long serialVersionUID = 2560908732414378358L;


	/**
	 * Id testo predefinito.
	 */
	private Long idTestoPredefinito;
	
	/**
	 * Oggetto.
	 */
	private String oggetto;
	
	/**
	 * Corpo testo.
	 */
	private String corpotesto;
	
	/**
	 * Descrizione.
	 */
	private String descrizione;
	
	/**
	 * Aoo.
	 */
	private Long idAOO;
	
	/**
	 * Tipo testo.
	 */
	private TestoPredefinitoEnum tipoTesto;
	
	/**
	 * Costruttore vuoto.
	 */
	public TestoPredefinitoDTO() { }

	/**
	 * Costruttore di default.
	 * @param idTestoPredefinito
	 * @param oggetto
	 * @param corpotesto
	 * @param descrizione
	 * @param idAOO
	 * @param tipoTesto
	 */
	public TestoPredefinitoDTO(final Long idTestoPredefinito, final String oggetto, final String corpotesto, final String descrizione, 
			final Long idAOO, final int tipoTesto) {
		this.idTestoPredefinito = idTestoPredefinito;
		this.oggetto = oggetto;
		this.corpotesto = corpotesto;
		this.descrizione = descrizione;
		this.idAOO = idAOO;
		this.tipoTesto = TestoPredefinitoEnum.getById(tipoTesto);
	}

	/**
	 * @return idTestoPredefinito
	 */
	public Long getIdTestoPredefinito() {
		return idTestoPredefinito;
	}

	/**
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * @return corpotesto
	 */
	public String getCorpotesto() {
		return corpotesto;
	}

	/**
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Restituisce l'id dell'Area Organizzativa Omog.
	 * @return idAOO
	 */
	public Long getIdAOO() {
		return idAOO;
	}

	/**
	 * Restituisce il tipo di testo predefinito.
	 * @return tipoTesto
	 */
	public TestoPredefinitoEnum getTipoTesto() {
		return tipoTesto;
	}
}
