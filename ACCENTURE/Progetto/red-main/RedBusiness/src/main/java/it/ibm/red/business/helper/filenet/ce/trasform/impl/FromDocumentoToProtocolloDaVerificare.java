package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.filenet.api.core.Document;

import it.ibm.red.business.dao.IContattoDAO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Trasformer dal Documento al Protocollo Da Verificare.
 */
public class FromDocumentoToProtocolloDaVerificare extends TrasformerCE<DetailDocumentRedDTO> {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1791750445239258281L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentoToProtocolloDaVerificare.class);
	
	/**
	 *  Dao per recuperare le info di contatto dell'utente
	 */
	private IContattoDAO contattoDAO;

	/**
	 * Costruttore.
	 */
	public FromDocumentoToProtocolloDaVerificare() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_PROTOCOLLO_DA_VERIFICARE);
		contattoDAO = ApplicationContextProvider.getApplicationContext().getBean(IContattoDAO.class);
	}

	/**
	 * Esegue la trasformazione del documento.
	 * @param docFilenet
	 * @param connection
	 * @return DetailDocumentRedDTO
	 */
	@Override
	public DetailDocumentRedDTO trasform(final Document docFilenet, final Connection connection) {
		if (docFilenet == null) {
			return null;
		}
		
		try {
			DetailDocumentRedDTO d = new DetailDocumentRedDTO();
			d.setGuid(StringUtils.cleanGuidToString(docFilenet.get_Id()));
			d.setDocumentTitle((String) TrasformerCE.getMetadato(docFilenet, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
			d.setNumeroProtocollo((Integer) TrasformerCE.getMetadato(docFilenet, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY));
			d.setAnnoProtocollo((Integer) TrasformerCE.getMetadato(docFilenet, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY));
			d.setTipoProtocollo((Integer) TrasformerCE.getMetadato(docFilenet, PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY));
			d.setDataProtocollo((Date) TrasformerCE.getMetadato(docFilenet, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY));
			d.setDataAnnullamentoDocumento((Date) TrasformerCE.getMetadato(docFilenet, PropertiesNameEnum.METADATO_DOCUMENTO_DATA_ANNULLAMENTO));
			d.setDateCreated(docFilenet.get_DateCreated());
			d.setIdTipologiaDocumento((Integer) TrasformerCE.getMetadato(docFilenet, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY));
			d.setIdTipologiaProcedimento((Integer) TrasformerCE.getMetadato(docFilenet, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY));
			
			Collection<?> inDestinatari = (Collection<?>) getMetadato(docFilenet, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
			// Si normalizza il metadato per evitare errori comuni durante lo split delle singole stringhe dei destinatari
			List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(inDestinatari);
			
			if (!CollectionUtils.isEmpty(destinatariDocumento)) {
				List<DestinatarioRedDTO> list = new ArrayList<>();
				
				for (String[] destSplit : destinatariDocumento) {
					if (destSplit.length >= 5) {
						DestinatarioRedDTO desDTO = null;
						
						TipologiaDestinatarioEnum tde = TipologiaDestinatarioEnum.getByTipologia(destSplit[3]);
						if (TipologiaDestinatarioEnum.INTERNO == tde) {
							Long idNodo = Long.valueOf(destSplit[0]);
							Long idUtente = Long.valueOf(destSplit[1]);
							String  aliasContatto = destSplit[2];
							desDTO = new DestinatarioRedDTO(idNodo, idUtente, destSplit[3], destSplit[4]);
							Contatto c = new Contatto(null, null, null, null, null, null, null, aliasContatto);
							c.setIdNodo(idNodo);
							c.setIdUtente(idUtente);
							desDTO.setContatto(c);
							desDTO.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.INTERNO);
						} else if (TipologiaDestinatarioEnum.ESTERNO == tde) {
							Long idContatto = Long.valueOf(destSplit[0]);
							Integer idMezzo = Integer.valueOf(destSplit[2]);
							desDTO = new DestinatarioRedDTO(idMezzo, destSplit[3], destSplit[4]);
							Contatto c = contattoDAO.getContattoByID(idContatto, connection);
							desDTO.setContatto(c);
							desDTO.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
						}
						
						list.add(desDTO);
					}
				}
				d.setDestinatari(list);
			}
			
			return d;
			
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero di un metadato del dettaglio del documento: ", e);
			return null;
		}
	}
}
