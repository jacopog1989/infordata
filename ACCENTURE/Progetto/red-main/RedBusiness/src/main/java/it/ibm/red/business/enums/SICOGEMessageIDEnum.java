package it.ibm.red.business.enums;

/**
 * Enum messaggi SICOGE.
 */
public enum SICOGEMessageIDEnum {
	
	/**
	 * Valore.
	 */
	SICOGE_PROTOCOLLA_TITOLO(1001),
	
	/**
	 * Valore.
	 */
	SICOGE_DOCUMENTAZIONE_AGGIUNTIVA(1002);

	/**
	 * Valore.
	 */
	private final Integer id;

	/**
	 * Costruttore dell'enum.
	 * @param id
	 */
	SICOGEMessageIDEnum(final Integer id) {
		this.id = id;
	}

	/**
	 * Restituisce l'id dell'enum.
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Restituisce l'enum associata all'id in ingresso.
	 * @param id
	 * @return enum associata all'id
	 */
	public static SICOGEMessageIDEnum getEnumById(final Integer id) {
		SICOGEMessageIDEnum output = null;
		
		for (SICOGEMessageIDEnum item : SICOGEMessageIDEnum.values()) {
			if (item.getId().equals(id)) {
				output = item;
				break;
			}
		}

		return output;
	}
	
}