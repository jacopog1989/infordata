package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;

/**
 * 
 * @author m.crescentini
 *
 *	DAO per il tracciamento dei documenti creati tramite i web service esposti da Red EVO.
 */
public interface IRedWsDocGenDAO extends Serializable {
	
	
	/**
	 * @param idDocumento
	 * @param idFlusso
	 * @param con
	 * @return
	 */
	int insertDocGen(int idDocumento, Integer idFlusso, Connection con);
	
	
	/**
	 * @param idDocumento
	 * @pararm idFlusso
	 * @param con
	 * @return
	 */
	boolean isDocPresente(int idDocumento, Integer idFlusso, Connection con);
	
}
