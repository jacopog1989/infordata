package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.RegistroProtocolloDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * The Interface IRicercaRegistroProtocolloFacadeSRV.
 *
 * @author m.crescentini
 * 
 *         Facade del servizio per la gestione della ricerca nel Registro Protocollo.
 */
public interface IRicercaRegistroProtocolloFacadeSRV extends Serializable {
	
	/**
	 * Esegue la ricerca dei documenti per la stampa del registro di protocollo.
	 * @param paramsRicercaStampaProtocollo
	 * @param utente
	 * @return documenti
	 */
	Collection<RegistroProtocolloDTO> eseguiRicercaPerStampaRegistroProtocollo(ParamsRicercaAvanzataDocDTO paramsRicercaStampaProtocollo, UtenteDTO utente);
}