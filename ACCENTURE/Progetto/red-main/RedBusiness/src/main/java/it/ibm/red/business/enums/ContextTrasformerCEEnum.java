package it.ibm.red.business.enums;

/**
 * Enum che definisce i Context Trasformer CE.
 */
public enum ContextTrasformerCEEnum {

	/**
	 * Chiave identificativi tipo procedimento.
	 */
	IDENTIFICATIVI_TIPO_PROC,

	/**
	 * Chiave identificativi tipo documento.
	 */
	IDENTIFICATIVI_TIPO_DOC,

	/**
	 * Chiave check contributi.
	 */
	CHECK_CONTRIBUTI,

	/**
	 * Chiave gestisci response.
	 */
	GESTISCI_RESPONSE,

	/**
	 * Chiave numero versione.
	 */
	NUM_VERSIONE_DOC,

	/**
	 * Chiave id categoria del documento principale.
	 */
	ID_CATEGORIA_DOC_PRINCIPALE,

	/**
	 * Chiave contatti display name.
	 */
	CONTATTI_DISPLAY_NAME,

	/**
	 * Chiave protocollatori.
	 */
	PROTOCOLLATORI,

	/**
	 * Chiave Siebel.
	 */
	SIEBEL,

	/**
	 * Chiave tipologie documenti attive.
	 */
	TIPOLOGIE_DOCUMENTO_ATTIVE,

	/**
	 * Chiave tipologie documento non attive.
	 */
	TIPOLOGIE_DOCUMENTO_NON_ATTIVE,

	/**
	 * Chiave registro protocollo.
	 */
	REGISTRO_PROTOCOLLO,

	/**
	 * Chiave recupera content.
	 */
	RECUPERA_CONTENT,

	/**
	 * Chiave recupera metadati.
	 */
	RECUPERA_METADATI,

	/**
	 * Chiave recupera security.
	 */
	RECUPERA_SECURITY,

	/**
	 * Chiave class definition properties.
	 */
	CLASS_DEFINITION_PROPERTIES,

	/**
	 * Chiave libro firma placeholder.
	 */
	LIBRO_FIRMA_PLACEHOLDER,

	/**
	 * Chiave sottocategoria documento in uscita.
	 */
	SOTTOCATEGORIA_DOCUMENTO_USCITA,

	/**
	 * Chiave Coda.
	 */
	QUEUE,
	
	/**
	 * IS RICERCA.
	 */
	IS_RICERCA,
	
	/**
	 * MAP LOCK.
	 */
	MAPLOCK,
	
	/**
	 * UFF_CREATORE_MAP.
	 */
	UFF_CREATORE_MAP,
	
	/**
	 * CODICE FLUSSO DOC PRINCIPALE.
	 */
	CODICE_FLUSSO_PRINCIPALE_METAKEY,
	
	/**
	 * ASIGN INFO.
	 */
	ASIGN_INFO
	;
}
