package it.ibm.red.business.service.facade;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentAppletContentDTO;
import it.ibm.red.business.dto.DocumentManagerDTO;
import it.ibm.red.business.dto.EsitoDTO;
import it.ibm.red.business.dto.EsitoVerificaProtocolloDTO;
import it.ibm.red.business.dto.FadAmministrazioneDTO;
import it.ibm.red.business.dto.FadRagioneriaDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.MezzoRicezioneDTO;
import it.ibm.red.business.dto.ResponsabileCopiaConformeDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.NodoUtenteCoordinatore;
import it.ibm.red.business.persistence.model.TipoProcedimento;

/**
 * Facade del servizio che gestisce il document manager.
 */
public interface IDocumentManagerFacadeSRV extends Serializable {

	/**
	 * Predispone la maschera di creazione o di modifica.
	 * 
	 * @param utente
	 * @param detail
	 * @param fromProtocollaMail
	 * @return documento
	 */
	DocumentManagerDTO prepareMascheraCreazioneOModifica(UtenteDTO utente, DetailDocumentRedDTO detail, boolean fromProtocollaMail);

	/**
	 * Ottiene i tipi procedimento dalla tipologia documento.
	 * 
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @return lista di tipi procedimento
	 */
	List<TipoProcedimento> getTipiProcedimentoByTipologiaDocumento(Integer idTipologiaDocumento, Integer idTipoProcedimento);

	/**
	 * Ottiene i mezzi di ricezione dal formato del documento.
	 * 
	 * @param listMezziRicezione
	 * @param f
	 * @return lista dei mezzi di ricezione
	 */
	List<MezzoRicezioneDTO> filtraMezziRicezioneByFormatoDocumento(List<MezzoRicezioneDTO> listMezziRicezione, FormatoDocumentoEnum f);

	/**
	 * Verifica se il documento è allacciabile.
	 * 
	 * @param utente
	 * @param r
	 * @param t
	 * @return esito della verifica
	 */
	EsitoVerificaProtocolloDTO verificaAllaccioProtocollo(UtenteDTO utente, RispostaAllaccioDTO r, TipoCategoriaEnum t);

	/**
	 * L'RDP e' visibile solo se il processo e' di tipo PRELEX.
	 * 
	 * @param categoria
	 * @param descTipoProcedimento
	 * @return
	 */
	boolean isRdpPanelVisible(CategoriaDocumentoEnum categoria, String descTipoProcedimento);

	/**
	 * @param modalitaMaschera
	 * @param f
	 * @param categoria
	 * @param storicoSteps
	 * @return
	 */
	boolean isDocPrincipalePanelVisible(String modalitaMaschera, FormatoDocumentoEnum f, CategoriaDocumentoEnum categoria, Collection<StoricoDTO> storico);

	/**
	 * Verifica la visibilità del template button.
	 * 
	 * @param modalitaMaschera
	 * @param categoria
	 * @return true o false
	 */
	boolean isTemplateButtonVisible(String modalitaMaschera, CategoriaDocumentoEnum categoria);

	/**
	 * Verifica la visibilità del pannello barcode.
	 * 
	 * @param modalitaMaschera
	 * @param categoria
	 * @param f
	 * @return true o false
	 */
	boolean isBarcodePanelVisible(String modalitaMaschera, CategoriaDocumentoEnum categoria, FormatoDocumentoEnum f);

	/**
	 * Verifica la visibilità della firma del documento principale.
	 * 
	 * @param mimeType
	 * @return true o false
	 */
	boolean isVerificaFirmaDocPrincipaleVisible(String mimeType);

	/**
	 * Verifica la visibilità del flag per la firma di copia conforme.
	 * 
	 * @param utente
	 * @param t
	 * @return true o false
	 */
	boolean isFlagFirmaCopiaConformeVisible(UtenteDTO utente, TipoAssegnazioneEnum t);

	/**
	 * Verifica la visibilità dell'indice di classificazione.
	 * 
	 * @param f
	 * @return true o false
	 */
	boolean isIndiceDiClassificazioneVisible(FormatoDocumentoEnum f);

	/**
	 * ompone la descrizione del fascicolo.
	 * 
	 * @param detail
	 * @return descrizione
	 */
	String componiDescrizioneFascicolo(DetailDocumentRedDTO detail);

	/**
	 * Verifica la visibilità del protocollo mittente.
	 * 
	 * @param categoria
	 * @param f
	 * @return true o false
	 */
	boolean isProtocolloMittenteRispostaVisible(CategoriaDocumentoEnum categoria, FormatoDocumentoEnum f);

	/**
	 * Verifica la visibilità della raccolta Fad.
	 * 
	 * @param detail
	 * @return true o false
	 */
	boolean isRaccoltaFadVisible(DetailDocumentRedDTO detail);

	/**
	 * Restituisce l'id della raccolta provvisoria.
	 * 
	 * @param detail
	 * @param utente
	 * @return
	 */
	String creaRaccoltaFadManuale(DetailDocumentRedDTO detail, FadAmministrazioneDTO f, FadRagioneriaDTO r, UtenteDTO utente);

	/**
	 * Verifica la visibilità dell'assegnatario per competenza.
	 * 
	 * @param categoria
	 * @return true o false
	 */
	boolean isAssegnatarioPerCompetenzaVisible(CategoriaDocumentoEnum categoria);

	/**
	 * Ottiene l'ufficio UCR dall'id dell'Aoo.
	 * 
	 * @param idAOO
	 * @return ufficio UCR
	 */
	UfficioDTO getUfficioUCR(Long idAOO);

	/**
	 * Restituisce il responsabile dalla lista (la combo responsabili copia
	 * conforme.
	 * 
	 * sia dell tab dettaglio della creazione che nel tab allegati) NB: l'id e'
	 * costituito dall'idUfficio + " " + idUtente
	 * 
	 * @param id
	 * @param list
	 * @return
	 */
	ResponsabileCopiaConformeDTO selectResponsabileById(String id, List<ResponsabileCopiaConformeDTO> list);

	/** ALLEGATI START ************************/
	boolean isResponsabiliCopiaConformeForAllegatoVisible(CategoriaDocumentoEnum categoria, boolean isComboResponsabileCopiaConformeEmpty);

	/**
	 * Verifica la visibilità del check sugli allegati da firmare.
	 * 
	 * @param categoria
	 * @return true o false
	 */
	boolean isAllegatiCheckDaFirmareVisible(CategoriaDocumentoEnum categoria);

	/** ALLEGATI END ************************/

	boolean isIterDocUscitaVisible(CategoriaDocumentoEnum categoria);

	/**
	 * Restituisce i documenti contenuti nel fascicolo procedimentale a meno del
	 * documento principale stesso.
	 * 
	 * @param idFascicoloProcedimentale
	 * @param documentTitle
	 * @param utente
	 * @return
	 */
	List<DetailDocumentRedDTO> getAllegatiMail(String idFascicoloProcedimentale, String documentTitle, UtenteDTO utente);

	/**
	 * Recupero lo stream del documenti dal guid.
	 * 
	 * mi serve per il download delle versioni dei documenti
	 * 
	 * @param guid
	 * @param utente
	 * @return
	 */
	InputStream getStreamDocumentoByGuid(String guid, UtenteDTO utente);

	/**
	 * @param guid
	 * @param utente
	 * @return
	 */
	Document getDocumentByGuid(String guid, UtenteDTO utente);

	/**
	 * Visibilità combo coordinatori.
	 * 
	 * @param i
	 * @param comboCoordinatori
	 * @return
	 */
	boolean comboCoordinatoriVisible(Integer i, List<NodoUtenteCoordinatore> comboCoordinatori);

	/**
	 * recupera l'utente protocollatore dalla mail nella procedura di
	 * protocollazione da mail.
	 * 
	 * @param mailGuid
	 * @return
	 */
	UtenteDTO getUtenteProtocollatoreMailByGuidMail(String mailGuid, UtenteDTO utente);

	/**
	 * Il tab assegnazioni deve essere abilitato sse l'iter approvativo è MANUALE.
	 * 
	 * @param idIterApprovativo
	 * @param categoria
	 * @return
	 */
	boolean isTabAssegnazioniDisabled(Integer idIterApprovativo, CategoriaDocumentoEnum categoria);

	/**
	 * Restituisce true se posso modificare il content del documento.
	 * 
	 * @param utente
	 * @param detail
	 * @param documentManagerDTO
	 * @return
	 */
	boolean isDocPrincipaleRimuovibile(UtenteDTO utente, DetailDocumentRedDTO detail, DocumentManagerDTO documentManagerDTO);

	/**
	 * Restituisce true se nello storico del documento esiste un evento firmato o
	 * firmato e spedito.
	 * 
	 * @param detail
	 * @param documentManagerDTO
	 * @return
	 */
	boolean isDocFirmato(DetailDocumentRedDTO detail, DocumentManagerDTO documentManagerDTO);

	/**
	 * Tutti i controlli lato front end del file principale.
	 * 
	 * @param f
	 * @param utente
	 * @param dm
	 * @param classDocMainDoc
	 * @return
	 */
	EsitoDTO checkUploadFilePrincipale(FileDTO f, UtenteDTO utente, DocumentManagerDTO dm, TipoAssegnazioneEnum tipoAssegnazione, String classDocMainDoc,
			boolean maxSizeRispettate);

	/**
	 * @param utente
	 * @param detail
	 * @return
	 */
	List<Contatto> getContattiPreferitiPerComboDestinatario(UtenteDTO utente, DetailDocumentRedDTO detail);

	/**
	 * Se esiste almeno un destinatario interno la protocollazione deve essere
	 * STANDARD e disabilitata perché non può essere cambiata.
	 * 
	 * @param detail
	 * @param destinatariInModifica
	 * @param documentManagerDTO
	 */
	void checkDestinatariInterni(DetailDocumentRedDTO detail, List<DestinatarioRedDTO> destinatariInModifica, DocumentManagerDTO documentManagerDTO);

	/**
	 * Restituisce il DTO per aprire l'applet
	 * 
	 * @param guid
	 * @param utente
	 * @return
	 */
	DocumentAppletContentDTO getDocumentForAppletByGuid(String guid, UtenteDTO utente);

	/**
	 * Restituisce true se bisogna impostare la nel tab spedizioni la casella
	 * configurata del ragioniere
	 * 
	 * @param utente
	 * @param item                 è il documentManagerDTO
	 * @param idIterApprovativo    del documento, quello selezionato nella maschera
	 * @param tipoAssegnazioneEnum del documento, quello selezionato nella maschera
	 * @param assegnazioni         del documento, quelle selezionate nella maschera
	 * @return
	 */
	boolean isCasellaTabSpedizioniConfigurata(UtenteDTO utente, DocumentManagerDTO item, Integer idIterApprovativo, TipoAssegnazioneEnum tipoAssegnazioneEnum,
			List<AssegnazioneDTO> assegnazioni);

	/**
	 * Serve per sapere se bisogna cekkare di default il flag RGS al momento della
	 * creazione del documento.
	 * 
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @return
	 */
	boolean gestioneCheckFirmaDigitaleRGS(int idTipologiaDocumento, long idTipoProcedimento);

	/**
	 * Ottiene la firma digitale RGS dal workflow.
	 * 
	 * @param documentTitle
	 * @param utente
	 * @return true o false
	 */
	boolean getFirmaDigitaleRGSFromWF(String documentTitle, UtenteDTO utente);

	/**
	 * Un documento non possiede il dettaglio se il tipo documento termina con
	 * '_LIGHT'.
	 * 
	 * @param tipologiaDocumento La tipologia documento da analizzare.
	 * @return True se il doucmento non termina con LIGHT oppure null.
	 */
	boolean hasValidDetail(String tipologiaDocumento);

	/**
	 * Recupera tutte le possibili modalita di destinatario.
	 */
	List<ModalitaDestinatarioEnum> retrieveComboModalitaDestinatario();

	/**
	 * Ottiene il content del documento.
	 * 
	 * @param d documento
	 * @return content del documento
	 */
	byte[] getDocContent(Document d);

	/**
	 * Verifica la visibilità del button per il template del documento in uscita.
	 * 
	 * @param isTemplateDocUscitaAttivo
	 * @param isDocumentoModificabile
	 * @param isGeneratedByTemplateDocUscita
	 * @param isInCreazioneUscita
	 * @param isInModificaUscita
	 * @param filePresente
	 * @return true o false
	 */
	boolean isTemplateDocUscitaButtonVisible(Boolean isTemplateDocUscitaAttivo, Boolean isDocumentoModificabile, boolean isGeneratedByTemplateDocUscita,
			boolean isInCreazioneUscita, boolean isInModificaUscita, boolean filePresente);

	/**
	 * Preseleziona il ribalta titolario.
	 * 
	 * @param idUfficio
	 * @param idAoo
	 * @return true o false
	 */
	boolean preselezionaRibaltaTitolario(Long idUfficio, Long idAoo);

	/**
	 * Invia la mail di spedizione.
	 * 
	 * @param utente
	 * @param documentTitle
	 * @param mittente
	 * @param destinatari
	 * @param oggettoMail
	 * @param testoMail
	 * @return
	 */
	Long inviaMailSpedizione(UtenteDTO utente, String documentTitle, String mittente, List<DestinatarioRedDTO> destinatari, String oggettoMail, String testoMail);

	/**
	 * Tutti i controlli lato front end del file allegato.
	 * 
	 * @param f
	 * @param utente
	 * @param dm
	 * @param formato
	 * @return
	 */
	EsitoDTO checkUploadFileAllegato(FileDTO f, AllegatoDTO allegato, UtenteDTO utente, DocumentManagerDTO dm, FormatoAllegatoEnum formato);

	/**
	 * Controlla la stampigliatura di sigla.
	 * 
	 * @param idUffIstruttore id dell'ufficio istruttore
	 * @param idTipologiaDoc  id della tipologia di documento
	 * @return true se la stampigliatura di sigla è visibile, false in caso
	 *         contrario
	 */
	boolean checkStampigliaturaSiglaVisible(Long idUffIstruttore, Integer idTipologiaDoc);

	/**
	 * @param detail
	 * @param inModifica
	 * @param onlyFirma  se true, restituisce la combo popolata con la sola
	 *                   assegnazione per Firma.
	 * @param idAoo
	 * @return
	 */
	List<TipoAssegnazioneEnum> getComboTipoAssegnazione(DetailDocumentRedDTO detail, boolean inModifica, boolean onlyFirma, Long idAoo);
}