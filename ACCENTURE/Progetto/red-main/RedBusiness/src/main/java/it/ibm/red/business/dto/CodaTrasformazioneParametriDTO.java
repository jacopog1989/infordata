package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import it.ibm.red.business.utils.LogStyleNotNull;

/**
 * Struttura dati di servizio per l'eleborazione del servizio di trasformazione in PDF.
 * 
 * @author a.dilegge
 *
 */
public class CodaTrasformazioneParametriDTO implements Serializable {

	private static final long serialVersionUID = 1947962400860503249L;
	
	/**
	 * Identificativo coda.
	 */
	private Integer idCoda;
	
	/**
	 * Identificativo documento.
	 */
	private String idDocumento;
	
	/**
	 * Identificativo utente.
	 */
	private Long idUtente;
	
	/**
	 * Identificativo nodo.
	 */
	private Long idNodo;
	
	/**
	 * Lista firmatari.
	 */
	private String[] firmatariString;
	
	/**
	 * Tipologia firma.
	 */
	private Integer tipoFirma;
	
	/**
	 * Numero revision file.
	 */
	private Integer fileRevisionNumber;
	
	/**
	 * Flag principale.
	 */
	private boolean principale;
	
	/**
	 * è l'and logico dei vecchi GD_NO_ATTACHMENT e MODIFICA_MONTANARO.
	 */
	private boolean allegati;
	
	/**
	 * Lista id allegati.
	 */
	private List<String> idAllegati;
	
	/**
	 * Lista id allegati da firmare.
	 */
	private List<String> idAllegatiDaFirmare;
	
	/**
	 * Lista id allegati con firma visibile.
	 */
	private List<String> idAllegatiConFirmaVisibile;
	
	/**
	 * Flag protocollazione P7M.
	 */
	private boolean protocollazioneP7M;
	
	/**
	 * Flag firma copia conforme.
	 */
	private boolean flagFirmaCopiaConforme;
	
	/**
	 * Flag aggiorna firma pdf.
	 */
	private boolean aggiornaFirmaPDF;
	
	/**
	 * Tipologia assegnaizone.
	 */
	private Integer idTipoAssegnazione;
	
	/**
	 * Identificativo notifica spedizione.
	 */
	private Long idNotificaSped;
	
	/**
	 * Indica se il guid della prima versione del content deve essere recuperata su NPS,
	 * non coincidendo con quello di filenet (protocollazione automatica).
	 */
	private boolean firstVersionOnNPS;

	/**
	 * Costruttore di default.
	 */
	public CodaTrasformazioneParametriDTO() {
		super();
	}

	/**
	 * @return idCoda
	 */
	public Integer getIdCoda() {
		return idCoda;
	}

	/**
	 * @param idCoda
	 */
	public void setIdCoda(final Integer idCoda) {
		this.idCoda = idCoda;
	}

	/**
	 * @return idDocumento
	 */
	public String getIdDocumento() {
		return idDocumento;
	}

	/**
	 * @param idDocumento
	 */
	public void setIdDocumento(final String idDocumento) {
		this.idDocumento = idDocumento;
	}

	/**
	 * @return idUtente
	 */
	public Long getIdUtente() {
		return idUtente;
	}

	/**
	 * @param idUtente
	 */
	public void setIdUtente(final Long idUtente) {
		this.idUtente = idUtente;
	}

	/**
	 * @return idNodo
	 */
	public Long getIdNodo() {
		return idNodo;
	}

	/**
	 * @param idNodo
	 */
	public void setIdNodo(final Long idNodo) {
		this.idNodo = idNodo;
	}

	/**
	 * Restituisce la lista dei firmatari in un array di String.
	 * @return firmatariString
	 */
	public String[] getFirmatariString() {
		return firmatariString;
	}

	/**
	 * Imposta i firmatari come array di String.
	 * @param firmatariString
	 */
	public void setFirmatariString(final String[] firmatariString) {
		this.firmatariString = firmatariString;
	}

	/**
	 * @return tipoFirma
	 */
	public Integer getTipoFirma() {
		return tipoFirma;
	}

	/**
	 * @param tipoFirma
	 */
	public void setTipoFirma(final Integer tipoFirma) {
		this.tipoFirma = tipoFirma;
	}

	/**
	 * @return fileRevisionNumber
	 */
	public Integer getFileRevisionNumber() {
		return fileRevisionNumber;
	}

	/**
	 * @param fileRevisionNumber
	 */
	public void setFileRevisionNumber(final Integer fileRevisionNumber) {
		this.fileRevisionNumber = fileRevisionNumber;
	}

	/**
	 * @return principale
	 */
	public boolean isPrincipale() {
		return principale;
	}

	/**
	 * @param principale
	 */
	public void setPrincipale(final boolean principale) {
		this.principale = principale;
	}

	/**
	 * @return allegati
	 */
	public boolean isAllegati() {
		return allegati;
	}

	/**
	 * @param allegati
	 */
	public void setAllegati(final boolean allegati) {
		this.allegati = allegati;
	}

	/**
	 * @return idAllegati
	 */
	public List<String> getIdAllegati() {
		return idAllegati;
	}

	/**
	 * @param idAllegati
	 */
	public void setIdAllegati(final List<String> idAllegati) {
		this.idAllegati = idAllegati;
	}

	/**
	 * Restituisce gli id degli allegati per cui occorre la firma.
	 * @return idAllegatiDaFirmare
	 */
	public List<String> getIdAllegatiDaFirmare() {
		return idAllegatiDaFirmare;
	}

	/**
	 * Imposta la lista degli id degli allegati da firmare.
	 * @param idAllegatiDaFirmare
	 */
	public void setIdAllegatiDaFirmare(final List<String> idAllegatiDaFirmare) {
		this.idAllegatiDaFirmare = idAllegatiDaFirmare;
	}

	/**
	 * Restituisce true se si tratta di una protocollazione P7M, false altrimenti.
	 * @return protocollazioneP7M
	 */
	public boolean isProtocollazioneP7M() {
		return protocollazioneP7M;
	}

	/**
	 * Imposta il parametro protocollazione P7M.
	 * @param protocollazioneP7M
	 */
	public void setProtocollazioneP7M(final boolean protocollazioneP7M) {
		this.protocollazioneP7M = protocollazioneP7M;
	}

	/**
	 * @return true se si tratta di copia conforme, false altrimenti
	 */
	public boolean isFlagFirmaCopiaConforme() {
		return flagFirmaCopiaConforme;
	}

	/**
	 * Imposta il flag copia conforme.
	 * @param flagFirmaCopiaConforme
	 */
	public void setFlagFirmaCopiaConforme(final boolean flagFirmaCopiaConforme) {
		this.flagFirmaCopiaConforme = flagFirmaCopiaConforme;
	}

	/**
	 * Restituisce il flag aggiornaFirmaPdf.
	 * @return aggiornaFirmaPDF
	 */
	public boolean isAggiornaFirmaPDF() {
		return aggiornaFirmaPDF;
	}

	/**
	 * Imposta il flag aggiornaFirmaPDF.
	 * @param aggiornaFirmaPDF
	 */
	public void setAggiornaFirmaPDF(final boolean aggiornaFirmaPDF) {
		this.aggiornaFirmaPDF = aggiornaFirmaPDF;
	}

	/**
	 * Restituisce l'id del tipo assegnazione.
	 * @return idTipoAssegnazione
	 */
	public Integer getIdTipoAssegnazione() {
		return idTipoAssegnazione;
	}

	/**
	 * Imposta l'id del tipo assegnazione.
	 * @param idTipoAssegnazione
	 */
	public void setIdTipoAssegnazione(final Integer idTipoAssegnazione) {
		this.idTipoAssegnazione = idTipoAssegnazione;
	}

	/**
	 * @see java.lang.Object#toString().
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, new LogStyleNotNull()); 
	}

	/**
	 * Restituisce l'id della notifica di spedizione.
	 * @return idNotificaSped
	 */
	public Long getIdNotificaSped() {
		return idNotificaSped;
	}

	/**
	 * Imposta l'id della notifica di spedizione.
	 * @param idNotificaSped
	 */
	public void setIdNotificaSped(final Long idNotificaSped) {
		this.idNotificaSped = idNotificaSped;
	}

	/**
	 * Restituisce la Lista di id degli allegati per il quale occorre poter vedere la firma.
	 * @return idAllegatiConFirmaVisibile
	 */
	public List<String> getIdAllegatiConFirmaVisibile() {
		return idAllegatiConFirmaVisibile;
	}

	/**
	 * Imposta la lista degli id di riferimento agli allegati per il quale la firma deve essere visibile.
	 * @param idAllegatiConFirmaVisibile
	 */
	public void setIdAllegatiConFirmaVisibile(final List<String> idAllegatiConFirmaVisibile) {
		this.idAllegatiConFirmaVisibile = idAllegatiConFirmaVisibile;
	}

	/**
	 * Restituisce firstVersionOnNPS.
	 * 
	 * @return the firstVersionOnNPS
	 */
	public boolean isFirstVersionOnNPS() {
		return firstVersionOnNPS;
	}

	/**
	 * Imposta firstVersionOnNPS.
	 * 
	 * @param firstVersionOnNPS the firstVersionOnNPS to set
	 */
	public void setFirstVersionOnNPS(boolean firstVersionOnNPS) {
		this.firstVersionOnNPS = firstVersionOnNPS;
	}
	
	

}