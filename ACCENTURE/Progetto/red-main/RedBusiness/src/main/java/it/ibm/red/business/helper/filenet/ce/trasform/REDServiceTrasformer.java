package it.ibm.red.business.helper.filenet.ce.trasform;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBElement;

import com.filenet.api.collection.ContentElementList;
import com.filenet.api.collection.StringList;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;
import com.filenet.api.property.Properties;
import com.filenet.api.property.Property;

import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.webservice.model.redservice.messages.v1.GetVersioneDocumentoResponse;
import it.ibm.red.webservice.model.redservice.types.v1.ClasseDocumentaleRed;
import it.ibm.red.webservice.model.redservice.types.v1.DocumentoRed;
import it.ibm.red.webservice.model.redservice.types.v1.MetadatoRed;
import it.ibm.red.webservice.model.redservice.types.v1.ObjectFactory;
import it.ibm.red.webservice.model.redservice.types.v1.RispostaVersioneDocRed;

/**
 * Trasformer Red Service.
 */
@SuppressWarnings("rawtypes")
public final class REDServiceTrasformer {

	/**
	 * Factory.
	 */
	private static ObjectFactory of;

	/**
	 * Properties.
	 */
	private static PropertiesProvider pp;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(REDServiceTrasformer.class.getName());

	static {
		of = new ObjectFactory();
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * Costruttore vuoto.
	 */
	private REDServiceTrasformer() {
		// Costruttore vuoto.
	}

	/**
	 * Esegue la trasformazione restituendo un DocumentoRed a partire da un
	 * Document.
	 * 
	 * @param doc
	 * @param ceh
	 * @return JAXBElement di DocumentoRed
	 */
	public static JAXBElement<DocumentoRed> getDocumentoRedFromDocument(final Document doc, final IFilenetCEHelper ceh) {
		final DocumentoRed documentoRed = of.createDocumentoRed();

		// costruisco la classeDocumentale
		final ClasseDocumentaleRed cs = new ClasseDocumentaleRed();
		cs.setNomeClasse(of.createClasseDocumentaleRedNomeClasse(doc.getClassName()));
		cs.getMetadato().addAll(getMetadatiDoc(doc));

		documentoRed.setIdFascicolo(of.createDocumentoRedIdFascicolo(""));
		documentoRed.setClasseDocumentale(of.createDocumentoRedClasseDocumentale(cs));
		documentoRed.setNumVersione(doc.get_MajorVersionNumber());
		documentoRed.setGuid(of.createDocumentoRedGuid(doc.get_Id().toString()));
		documentoRed.setDataModifica(of.createDocumentoRedDataModifica(DateUtils.dateToString(doc.get_DateCheckedIn(), true)));
		final ContentElementList ceList = doc.get_ContentElements();
		final ContentTransfer ct = (ContentTransfer) ceList.get(0);
		if (ct != null) {
			documentoRed.setContentType(of.createDocumentoRedContentType(ct.get_ContentType()));
			documentoRed.setDataHandler(of.createDocumentoRedDataHandler(FilenetCEHelper.getDocumentContentAsByte(doc)));

			if (doc.getProperties().isPropertyPresent(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY))) {
				documentoRed.getAllegati().addAll(getDocumentoRedFromAllegato(doc, ceh));
				documentoRed.setIdDocumento(
						of.createDocumentoRedIdDocumento((doc.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY)) + "-"
								+ doc.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY)))));
			} else {
				documentoRed.setIdDocumento(of.createDocumentoRedIdDocumento(ct.get_RetrievalName()));
			}
		} else {
			if (doc.getProperties().isPropertyPresent(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY))) {
				documentoRed.getAllegati().addAll(getDocumentoRedFromAllegato(doc, ceh));
				documentoRed.setIdDocumento(
						of.createDocumentoRedIdDocumento((doc.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY)) + "-"
								+ doc.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY)))));
			} else {
				documentoRed.setIdDocumento(of.createDocumentoRedIdDocumento(doc.get_Name()));
			}
		}

		return of.createRispostaVersioneDocRedVersioneDocumento(documentoRed);
	}

	private static List<DocumentoRed> getDocumentoRedFromAllegato(final Document doc, final IFilenetCEHelper ceh) {
		final List<DocumentoRed> allegati = new ArrayList<>();
		final Iterator allegatiNonFirma = ceh.getAllegatiConContent(doc.get_Id().toString()).iterator();
		final Iterator allegatiFirma = ceh.getAllegatiConContentDaFirmare(doc.get_Id().toString()).iterator();
		while (allegatiNonFirma.hasNext()) {
			final Document d = (Document) allegatiNonFirma.next();
			allegati.add(getDocumentoRedFromDocument(d, ceh).getValue());
		}
		while (allegatiFirma.hasNext()) {
			final Document d = (Document) allegatiFirma.next();
			allegati.add(getDocumentoRedFromDocument(d, ceh).getValue());
		}
		return allegati;
	}

	/**
	 * Restituisce la lista dei metadati: MetadatoRed, recuperati dal documento.
	 * 
	 * @param doc
	 * @return lista di metadati
	 */
	public static List<MetadatoRed> getMetadatiDoc(final Document doc) {

		final List<MetadatoRed> output = new ArrayList<>();
		try {
			Properties props;
			Object value = null;
			props = doc.getProperties();
			final Property[] prop = props.toArray();
			for (final Property property : prop) {
				
				value = getValueFromProperty(property);
				
				if (value == null) {
					continue;
				}
				
				ArrayList<String> stringList = null;
				if (value instanceof StringList) {
					stringList = new ArrayList<>();
					final StringList sl = (StringList) value;
					for (int ii = 0; ii < sl.size(); ii++) {
						stringList.add((String) sl.get(ii));
					}
				}
				
				final MetadatoRed metadato = new MetadatoRed();
				if (stringList != null) {
					final String[] multiValues = new String[stringList.size()];
					stringList.toArray(multiValues);
					metadato.setKey(of.createMetadatoRedKey(property.getPropertyName()));
					metadato.getMultiValues().addAll(Arrays.asList(multiValues));
				} else {
					metadato.setKey(of.createMetadatoRedKey(property.getPropertyName()));
					metadato.setValue(of.createMetadatoRedValue(String.valueOf(value)));
				}
				output.add(metadato);
			}
		} catch (final Exception e) {
			LOGGER.error(" Errore durante il recupero dei metadati del documento.", e);
			throw new RedException(" Errore durante il recupero dei metadati del documento.", e);
		}

		return output;
	}

	/**
	 * Recupera il value a partire dalla {@code property}, restituisce {@code null}
	 * se il tentativo di recupero non va a buon fine.
	 * 
	 * @param property
	 *            Property dalla quale recuperare il value.
	 * @return Value recuperato dalla property o {@code null} se il tentativo di
	 *         recupero fallisce per qualsiasi motivo.
	 */
	private static Object getValueFromProperty(final Property property) {
		Object value = null;
		try {
			value = property.getObjectValue();
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero del valore associato alla property " + property.getPropertyName(), e);
		}
		return value;
	}

	/**
	 * @param doc
	 * @param ceh
	 * @return GetVersioneDocumentoResponse
	 */
	public static GetVersioneDocumentoResponse getGetVersioneDocumentoResponseDocumentFromDocument(final Document doc, final IFilenetCEHelper ceh) {
		GetVersioneDocumentoResponse getVersioneDocumentoResponse = null;

		if (doc != null) {
			getVersioneDocumentoResponse = new GetVersioneDocumentoResponse();
			final RispostaVersioneDocRed rispostaVersioneDocRed = new RispostaVersioneDocRed();
			rispostaVersioneDocRed.setVersioneDocumento(getDocumentoRedFromDocument(doc, ceh));
			rispostaVersioneDocRed.setEsito(true);
			final it.ibm.red.webservice.model.redservice.messages.v1.ObjectFactory factory2 = new it.ibm.red.webservice.model.redservice.messages.v1.ObjectFactory();
			final JAXBElement<RispostaVersioneDocRed> response = factory2.createGetVersioneDocumentoResponseReturn(rispostaVersioneDocRed);
			response.getValue().setVersioneDocumento(getDocumentoRedFromDocument(doc, ceh));
			getVersioneDocumentoResponse.setReturn(response);
			rispostaVersioneDocRed.setEsito(true);
		}

		return getVersioneDocumentoResponse;
	}
}