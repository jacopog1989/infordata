package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ResponsesRedEnum;

/**
 * Facade del servizio di gestione funzionalità di riassegnazione.
 */
public interface IRiassegnazioneFacadeSRV extends Serializable {

	/**
	 * Metodo per la riassegnazione di un documento. Utilizzato dalle responses
	 * STORNA, STORNA_UTENTE, ASSEGNA.
	 * 
	 * @param utente
	 * @param wobNumbers
	 * @param response
	 * @param idNodoDestinatarioNews
	 * @param idUtenteDestinatarioNews
	 * @param motivoAssegnazione
	 * @return
	 */
	Collection<EsitoOperazioneDTO> riassegna(UtenteDTO utente, Collection<String> wobNumbers, ResponsesRedEnum response, Long idNodoDestinatarioNews,
			Long idUtenteDestinatarioNews, String motivoAssegnazione);
}