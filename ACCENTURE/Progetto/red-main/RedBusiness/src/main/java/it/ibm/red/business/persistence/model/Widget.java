package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * Model di un Widget.
 */
public class Widget implements Serializable {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 1L;

    /**
     * Id widget.
     */
	private Integer idWdg;

    /**
     * Id utente.
     */
	private Integer idUtente;
	
    /**
     * Id ruolo.
     */
	private Integer idRuolo;
	
    /**
     * Flag eliminabile.
     */
	private boolean eliminabile; 
	
    /**
     * Posizione.
     */
	private Integer posizione;
	
	/**
	 * Costruttore.
	 * @param inIdWdg
	 * @param inPosizione
	 */
	public Widget(final Integer inIdWdg, final Integer inPosizione) {
		idWdg = inIdWdg;
		posizione = inPosizione;
	}
	
	/**
	 * Costruttore.
	 * @param inIdWdg
	 * @param inPosizione
	 * @param inEliminabile
	 */
	public Widget(final Integer inIdWdg, final Integer inPosizione, final boolean inEliminabile) {
		idWdg = inIdWdg;
		eliminabile = inEliminabile;
		posizione = inPosizione;
	}
	
	/**
	 * Costruttore.
	 * @param inIdWdg
	 * @param inEliminabile
	 * @param inIdUtente
	 * @param inIdRuolo
	 * @param inPosizione
	 */
	public Widget(final Integer inIdWdg, final boolean inEliminabile, final Integer inIdUtente, final Integer inIdRuolo, final Integer inPosizione) {
		idWdg = inIdWdg;
		eliminabile = inEliminabile;
		idUtente = inIdUtente;
		idRuolo = inIdRuolo;
		posizione = inPosizione;
	}

	/**
	 * Restituisce l'id del widget.
	 * @return id widget
	 */
	public final Integer getIdWdg() {
		return idWdg;
	}
	
	/**
	 * Restituisce true se il widget è eliminabile, false altrimenti.
	 * @return true se il widget è eliminabile, false altrimenti
	 */
	public final boolean getEliminabile() {
		return eliminabile;
	}
	
	/**
	 * Restituisce l'id utente.
	 * @return id utente
	 */
	public final Integer getIdUtente() {
		return idUtente;
	}
	
	/**
	 * Restituisce l'id del ruolo.
	 * @return id ruolo
	 */
	public final Integer getIdRuolo() {
		return idRuolo;
	}
	
	/**
	 * Restituisce la posizione.
	 * @return posizione
	 */
	public final Integer getPosizione() {
		return posizione;
	}
}


