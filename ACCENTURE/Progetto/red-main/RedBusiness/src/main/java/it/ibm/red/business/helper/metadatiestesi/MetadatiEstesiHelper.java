package it.ibm.red.business.helper.metadatiestesi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.AnagraficaDipendenteDTO;
import it.ibm.red.business.dto.AnagraficaDipendentiComponentDTO;
import it.ibm.red.business.dto.CapitoloSpesaMetadatoDTO;
import it.ibm.red.business.dto.LookupTableDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.utils.DateUtils;

/**
 * La classe consente di serializzare e deserializzare i metadati estesi dei
 * tipi documento
 * 
 * @author c.pierascenzi
 *
 */
public final class MetadatiEstesiHelper {

	private static final String COGNOME = "> cognome";
	private static final String NOME = "> nome";
	private static final String TYPE = "\" type=\"";
	private static final String METADATO_CLOSE_TAG = "</metadato>";
	private static final String METADATO_NAME = "<metadato name=\"";

	private MetadatiEstesiHelper() {

	}

	/**
	 * Esegue {@link #serializeMetadato(MetadatoDTO)} su tutti i metadati estesi in
	 * ingresso.
	 * 
	 * @param metadatiEstesi
	 * @return flusso xml che definisce i metadati estesi serializzati
	 */
	public static String serializeMetadatiEstesi(final Collection<MetadatoDTO> metadatiEstesi) {
		final StringBuilder out = new StringBuilder("<metadati>");

		if (metadatiEstesi != null) {
			for (final MetadatoDTO metadato : metadatiEstesi) {
				out.append(serializeMetadato(metadato));
			}
		}

		out.append("</metadati>");

		return out.toString();
	}

	/**
	 * Serializza il metadato come flusso xml.
	 * 
	 * @param metadato da serializzare
	 * @return rappresentazione xml del metadato
	 */
	public static String serializeMetadato(final MetadatoDTO metadato) {
		String out = "";

		final String name = metadato.getName();
		final String type = metadato.getType().getCode();

		if (metadato instanceof AnagraficaDipendentiComponentDTO) {

			out = serializeAnagrafica(metadato, name, type);
		} else {

			out = serializeDefault(metadato, name, type);
		}
		return out;
	}

	/**
	 * Per le LookupTable il value è la descrizione (colonna DISPLAYNAME della
	 * tabella LOOKUPTABLE) della selezione, mentre la chiave è l'identificativo
	 * (colonna ID della tabella LOOKUPTABLE) e viene serializzata in un apposito
	 * attributo del metadato. In questo modo, in fase di deserializazione abbiamo
	 * già a disposizione la descrizione del valore selezionato per il metadato,
	 * senza dover fare ulteriori query.
	 * 
	 * @param metadato
	 * @param name
	 * @param type
	 */
	private static String serializeDefault(final MetadatoDTO metadato, final String name, final String type) {

		final StringBuilder serializedMetadato = new StringBuilder().append(METADATO_NAME).append(name).append(TYPE).append(type).append("\"");

		String value = "";
		if (metadato instanceof LookupTableDTO) {

			final SelectItemDTO lookupSelect = ((LookupTableDTO) metadato).getLookupValueSelected();
			if (lookupSelect != null && lookupSelect.getValue() != null && !"null".equals(lookupSelect.getValue())) {
				serializedMetadato.append(" key=\"").append(lookupSelect.getValue().toString()).append("\"");
				value = lookupSelect.getDescription();
			}
		} else if (metadato instanceof CapitoloSpesaMetadatoDTO) {
			value = ((CapitoloSpesaMetadatoDTO) metadato).getCapitoloSelected();
		} else if (metadato.getSelectedValue() != null) {
			value = metadato.getValue4AttrExt();
		}

		if (value == null) {
			value = Constants.EMPTY_STRING;
		}
		return serializedMetadato.append(">").append(value).append(METADATO_CLOSE_TAG).toString();
	}

	/**
	 * @param metadato
	 * @param name
	 * @param type
	 */
	private static String serializeAnagrafica(final MetadatoDTO metadato, final String name, final String type) {
		final StringBuilder value = new StringBuilder();
		StringBuilder anagrafica = new StringBuilder();
		if (((AnagraficaDipendentiComponentDTO) metadato).getSelectedValues() != null) {
			for (final AnagraficaDipendenteDTO anag : ((AnagraficaDipendentiComponentDTO) metadato).getSelectedValues()) {
				value.setLength(0);
				value.append("<codiceFiscale>").append(anag.getCodiceFiscale()).append("</codiceFiscale>").append("<nome>").append(anag.getNome()).append("</nome>")
						.append("<cognome>").append(anag.getCognome()).append("</cognome>");

				anagrafica = new StringBuilder().append(METADATO_NAME).append(name).append(TYPE).append(type).append("\" key=\"").append(anag.getId())
						.append("\">").append(value).append(METADATO_CLOSE_TAG);
			}
		} else {
			anagrafica = new StringBuilder().append(METADATO_NAME).append(name).append(TYPE).append(type).append("\">").append(value).append(METADATO_CLOSE_TAG);
		}
		return anagrafica.toString();
	}

	/**
	 * Deserilizza i metadati estesi recuperati dal flusso xml restituendone una
	 * lista di MetadatoDTO.
	 * 
	 * @param xml                        definisce i metadati e il loro valore
	 * @param metadatiTipologiaDocumento metadati specifici della tipologia
	 *                                   documento, non valorizzati
	 * @return lista dei metadati generata dal flusso xml
	 */
	public static Collection<MetadatoDTO> deserializeMetadatiEstesi(final String xml, final List<MetadatoDTO> metadatiTipologiaDocumento) {
		final Collection<MetadatoDTO> out = new ArrayList<>();
		final Map<String, Collection<AnagraficaDipendenteDTO>> anagrafiche = new HashMap<>();

		// Mappa nome metadato -> metadato per recuperare le proprietà di ogni metadato
		// dato il suo nome
		final Map<String, MetadatoDTO> mapMetadatiTipologiaDocumento = metadatiTipologiaDocumento.stream().collect(Collectors.toMap(MetadatoDTO::getName, m -> m));

		MetadatoDTO metadato;
		MetadatoDTO metadatoDB;

		final org.jsoup.nodes.Document doc = Jsoup.parse(xml, "", Parser.xmlParser());
		// Per ogni metadato nell' xml
		for (final org.jsoup.nodes.Element e : doc.select("metadati > metadato")) {
			final String name = e.attr("name");
			final TipoMetadatoEnum type = TipoMetadatoEnum.get(e.attr("type"));

			metadato = null;
			metadatoDB = mapMetadatiTipologiaDocumento.get(name);
			if (TipoMetadatoEnum.PERSONE_SELECTOR.equals(type)) {

				Collection<AnagraficaDipendenteDTO> anags = anagrafiche.get(name);
				if (anags == null) {
					anags = new ArrayList<>();
				}

				final AnagraficaDipendenteDTO anag = new AnagraficaDipendenteDTO();

				if (!e.attr("key").isEmpty()) {
					anag.setId(Integer.valueOf(e.attr("key")));
					anag.setCodiceFiscale(e.select("> codiceFiscale").text());
					anag.setNome(e.select(NOME).text());
					anag.setCognome(e.select(COGNOME).text());

					// Si aggiunge alle anagrafiche del metadato
					anags.add(anag);
				}

				anagrafiche.put(name, anags);

			} else {
				final String value = e.wholeText();

				if (TipoMetadatoEnum.DATE.equals(type)) {
					final SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.DD_MM_YYYY);
					Date d = null;

					try {
						if (value != null) {
							d = sdf.parse(value);
						}
					} catch (final ParseException e1) {
						// Non deve gestire l'eccezione.
					}

					metadato = new MetadatoDTO(name, type, metadatoDB.getDisplayName(), d, metadatoDB.getVisibility(), metadatoDB.getEditability(),
							metadatoDB.getObligatoriness());

				} else if (TipoMetadatoEnum.DOUBLE.equals(type)) {

					metadato = new MetadatoDTO(name, type, metadatoDB.getDisplayName(), !"".equals(value) ? Double.parseDouble(value) : null, metadatoDB.getVisibility(),
							metadatoDB.getEditability(), metadatoDB.getObligatoriness());

				} else if (TipoMetadatoEnum.INTEGER.equals(type)) {

					metadato = new MetadatoDTO(name, type, metadatoDB.getDisplayName(), !"".equals(value) ? Integer.parseInt(value) : null, metadatoDB.getVisibility(),
							metadatoDB.getEditability(), metadatoDB.getObligatoriness());

				} else if (TipoMetadatoEnum.STRING.equals(type) || TipoMetadatoEnum.TEXT_AREA.equals(type)) {

					metadato = new MetadatoDTO(name, type, metadatoDB.getDisplayName(), value, metadatoDB.getVisibility(), metadatoDB.getEditability(),
							metadatoDB.getObligatoriness());

				} else if (TipoMetadatoEnum.CAPITOLI_SELECTOR.equals(type)) {

					metadato = new CapitoloSpesaMetadatoDTO(name, metadatoDB.getDisplayName(), value, metadatoDB.getVisibility(), metadatoDB.getEditability(),
							metadatoDB.getObligatoriness());

				} else if (TipoMetadatoEnum.LOOKUP_TABLE.equals(type)) {

					metadato = new LookupTableDTO(name, metadatoDB.getDisplayName(), new SelectItemDTO(e.attr("key"), value), metadatoDB.getLookupTableMode(),
							metadatoDB.getVisibility(), metadatoDB.getEditability(), metadatoDB.getObligatoriness());

				}
			}

			if (metadato != null) {
				out.add(metadato);
			}
		}

		for (final Entry<String, Collection<AnagraficaDipendenteDTO>> e : anagrafiche.entrySet()) {
			metadatoDB = mapMetadatiTipologiaDocumento.get(e.getKey());

			out.add(new AnagraficaDipendentiComponentDTO(e.getKey(), e.getValue(), metadatoDB.getDisplayName(), metadatoDB.getVisibility(), metadatoDB.getEditability(),
					metadatoDB.getObligatoriness()));
		}

		return out;
	}

	/**
	 * Metodo per la deserializzazione dei metadati salvati sul database.
	 * Restituisce una struttura chiave valore contenete nome e valore (concatenato
	 * nel caso delle anagrafiche) del metadato salvato.
	 * 
	 * @param xml
	 * @param metadatiTipologiaDocumento
	 * @return output - struttura chiave valore che rappresenta nome e valore del
	 *         metadato
	 */
	public static Map<String, String> deserializeMetadatiEstesiPerRisultatiRicerca(final String xml) {
		final Map<String, String> output = new HashMap<>();

		if (StringUtils.isNotBlank(xml)) {
			String value = null;

			final Document doc = Jsoup.parse(xml, "", Parser.xmlParser());
			for (final Element e : doc.select("metadati > metadato")) {
				final String name = e.attr("name");
				final TipoMetadatoEnum type = TipoMetadatoEnum.get(e.attr("type"));

				if (TipoMetadatoEnum.PERSONE_SELECTOR.equals(type)) {

					final String nome = e.select(NOME).text();
					final String cognome = e.select(COGNOME).text();
					final StringBuilder sb = new StringBuilder();

					final String valueAnagrafica = output.get(name);
					if (valueAnagrafica == null || valueAnagrafica.isEmpty()) {
						value = sb.append(nome).append(" ").append(cognome).toString();
					} else {
						value = sb.append(valueAnagrafica).append(", ").append(nome).append(" ").append(cognome).toString();
					}

				} else {
					value = e.wholeText();
				}

				output.put(name, value);
			}
		}

		return output;
	}

	/**
	 * Metodo che consente una deserializzazione "light" dei metadati, recuperando
	 * solo nome, tipo e value del metadato.
	 * 
	 * @param xml String rappresentante il metadato in formato xml
	 * 
	 * @return MetadatoDTO
	 */
	public static MetadatoDTO deserializeMetadatoLight(final String xml) {
		MetadatoDTO metadato = null;
		Collection<AnagraficaDipendenteDTO> anags = null;

		final org.jsoup.nodes.Document doc = Jsoup.parse(xml, "", Parser.xmlParser());
		for (final org.jsoup.nodes.Element e : doc.select("metadato")) {
			final String name = e.attr("name");
			final TipoMetadatoEnum type = TipoMetadatoEnum.get(e.attr("type"));

			// Gestione metadato tipo ANAGRAFICHE
			if (TipoMetadatoEnum.PERSONE_SELECTOR.equals(type)) {
				if (anags == null) {
					anags = new ArrayList<>();
				}

				metadato = new AnagraficaDipendentiComponentDTO();
				metadato.setName(name);
				metadato.setType(type);

				final AnagraficaDipendenteDTO anag = new AnagraficaDipendenteDTO();
				if (!StringUtils.isEmpty(e.attr("key"))) {
					anag.setId(Integer.valueOf(e.attr("key")));
					anag.setCodiceFiscale(e.select("> codiceFiscale").text());
					anag.setNome(e.select(NOME).text());
					anag.setCognome(e.select(COGNOME).text());
					anags.add(anag);
				}

			} else if (TipoMetadatoEnum.LOOKUP_TABLE.equals(type)) {
				metadato = new LookupTableDTO();
				metadato.setName(name);
				metadato.setType(type);
				final String value = e.ownText();
				final String key = e.attr("key");

				if (!StringUtils.isEmpty(value) && !StringUtils.isEmpty(key)) {
					((LookupTableDTO) metadato).setLookupValueSelected(new SelectItemDTO(key, value));
				} else {
					((LookupTableDTO) metadato).setLookupValueSelected(null);
				}

			} else if (TipoMetadatoEnum.CAPITOLI_SELECTOR.equals(type)) {
				metadato = new CapitoloSpesaMetadatoDTO();
				metadato.setName(name);
				metadato.setType(type);
				final String value = e.ownText();

				if (value != null) {
					((CapitoloSpesaMetadatoDTO) metadato).setCapitoloSelected(value);
				} else {
					((CapitoloSpesaMetadatoDTO) metadato).setCapitoloSelected(null);
				}

			} else {
				metadato = new MetadatoDTO();
				metadato.setName(name);
				metadato.setType(type);

				final String value = e.wholeText();
				// Gestione metadato tipo DATE
				if (TipoMetadatoEnum.DATE.equals(type)) {
					final SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.DD_MM_YYYY);
					Date data = null;

					if (value != null) {
						try {
							data = sdf.parse(value);
							metadato.setSelectedValue(data);
						} catch (final ParseException e1) {
							// Non deve gestire l'eccezione.
						}
					}
				} else {
					metadato.setSelectedValue(value);
				}
			}
		}

		if (metadato instanceof AnagraficaDipendentiComponentDTO) {
			if (!CollectionUtils.isEmpty(anags)) {
				((AnagraficaDipendentiComponentDTO) metadato).setSelectedValues(anags);
			} else {
				((AnagraficaDipendentiComponentDTO) metadato).setSelectedValues(null);
			}
		}

		return metadato;
	}

}
