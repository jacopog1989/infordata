package it.ibm.red.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IDocumentoDAO;
import it.ibm.red.business.dto.GestioneLockDTO;
import it.ibm.red.business.enums.StatoLavorazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.StringUtils;

/**
 * Dao per la gestione dei documenti.
 *
 * @author mcrescentini
 */
@Repository
public class DocumentoDAO extends AbstractDAO implements IDocumentoDAO {

	private static final String IDUTENTE = "IDUTENTE";

	private static final String ISUTENTEINCARICO = "ISUTENTEINCARICO";

	private static final String COGNOME = "COGNOME";

	private static final String DOCUMENTTITLE = "DOCUMENTTITLE";

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -8765283686481290919L;

	/**
	 * Messaggio di errore inserimento lock.
	 */
	private static final String ERROR_INSERIMENTO_LOCK_MSG = "Errore durante l'inserimento del lock del documento :";

	/**
	 * Messaggio di errore recupero lock.
	 */
	private static final String ERROR_RECUPERO_LOCK_MSG = "Errore durante il recupero del lock del documento :";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DocumentoDAO.class.getName());

	/**
	 * @see it.ibm.red.business.dao.IDocumentoDAO#insert(java.lang.Long,
	 *      java.lang.Integer, int, java.sql.Connection).
	 */
	@Override
	public int insert(final Long idAoo, final Integer idTipologiaDocumento, final int registroRiservato, final Connection connection) {
		int idDocumento = 0;
		CallableStatement cs = null;
		try {
			int index = 1;
			cs = connection.prepareCall("{call p_insdocumento_maoo(?,?,?,?)}");

			cs.setLong(index++, idAoo);
			if (idTipologiaDocumento != null) {
				cs.setInt(index++, idTipologiaDocumento);
			} else {
				cs.setNull(index++, Types.INTEGER);
			}
			cs.setInt(index++, registroRiservato);
			cs.registerOutParameter(index++, Types.INTEGER);

			cs.execute();
			idDocumento = cs.getInt(index - 1);
			return idDocumento;
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeStatement(cs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IDocumentoDAO#getNextId(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public int getNextId(final Long idAoo, final Connection connection) {
		CallableStatement cs = null;
		try {
			cs = connection.prepareCall("{? = call call_seq_documento_by_idaoo(?)}");
			cs.registerOutParameter(1, Types.INTEGER);
			cs.setLong(2, idAoo);
			cs.execute();
			return cs.getInt(1);
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeStatement(cs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IDocumentoDAO#hasItemNodo(int, java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean hasItemNodo(final int idDocumento, final Long idUfficio, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean output = false;

		try {
			ps = con.prepareStatement("SELECT count(*) as counter " + "FROM documentoutentestato " + "WHERE iddocumento = ? AND idnodo = ? AND idutente = 0");
			ps.setInt(1, idDocumento);
			ps.setLong(2, idUfficio);

			rs = ps.executeQuery();

			if (rs.next()) {
				final Integer counter = rs.getInt("counter");
				output = counter != 0;
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il conteggio degli item relativi al nodo per il documento: " + idDocumento, e);
			throw new RedException("Errore durante il conteggio degli item relativi al nodo per il documento: " + idDocumento, e);
		} finally {
			closeStatement(ps, rs);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IDocumentoDAO#aggiornaTipologiaDocumento(int,
	 *      int, java.sql.Connection).
	 */
	@Override
	public void aggiornaTipologiaDocumento(final int idDocumento, final int idTipologiaDocumento, final Connection con) {
		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement("UPDATE documento SET idtipologiadocumento = ? WHERE iddocumento = ?");
			ps.setInt(1, idTipologiaDocumento);
			ps.setInt(2, idDocumento);

			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento della Tipologia Documento per il documento: " + idDocumento, e);
			throw new RedException("Errore durante l'aggiornamento della Tipologia Documento per il documento: " + idDocumento, e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IDocumentoDAO#aggiornaStatoDocumentoLavorazione(int,
	 *      java.lang.Long, java.lang.Long,
	 *      it.ibm.red.business.enums.StatoLavorazioneEnum, java.sql.Connection).
	 */
	@Override
	public void aggiornaStatoDocumentoLavorazione(final int idDocumento, final Long idUfficio, final Long idUtente, final StatoLavorazioneEnum statoLavorazione,
			final Connection con) {
		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement("UPDATE documentoutentestato SET idstatolavorazione = ? WHERE iddocumento = ? AND idnodo = ? AND idutente = ?");
			ps.setLong(1, statoLavorazione.getId());
			ps.setInt(2, idDocumento);
			ps.setLong(3, idUfficio);
			ps.setLong(4, idUtente);

			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento dello stato lavorazione per il documento: " + idDocumento, e);
			throw new RedException("Errore durante l'aggiornamento dello stato lavorazione per il documento: " + idDocumento, e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IDocumentoDAO#annullaUtenteDocumento(int,
	 *      java.lang.Long, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public void annullaUtenteDocumento(final int idDocumento, final Long idUfficio, final Long idUtente, final Connection con) {
		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement("UPDATE documentoutentestato SET idutente = 0 WHERE iddocumento = ? AND idnodo = ? AND idutente = ?");
			ps.setInt(1, idDocumento);
			ps.setLong(2, idUfficio);
			ps.setLong(3, idUtente);

			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento dell'utente per il documento: " + idDocumento, e);
			throw new RedException("Errore durante l'aggiornamento dell'utente per il documento: " + idDocumento, e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IDocumentoDAO#inserisciStatoDocumentoLavorazione(int,
	 *      java.lang.Long, java.lang.Long,
	 *      it.ibm.red.business.enums.StatoLavorazioneEnum, java.sql.Connection).
	 */
	@Override
	public void inserisciStatoDocumentoLavorazione(final int idDocumento, final Long idUfficio, final Long idUtente, final StatoLavorazioneEnum statoLavorazione,
			final Connection con) {
		CallableStatement cs = null;

		try {
			int index = 1;

			cs = con.prepareCall("INSERT INTO documentoutentestato(iddocumento, idnodo, idutente, idstatolavorazione) VALUES (?,?,?,?)");
			cs.setInt(index++, idDocumento);
			cs.setLong(index++, idUfficio);
			cs.setLong(index++, idUtente);
			cs.setLong(index++, statoLavorazione.getId());

			cs.execute();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'inserimento dello stato lavorazione per il documento: " + idDocumento, e);
			throw new RedException(e);
		} finally {
			closeStatement(cs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IDocumentoDAO#rimuoviItemNodoUtente(int,
	 *      java.lang.Long, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public void rimuoviItemNodoUtente(final int idDocumento, final Long idUfficio, final Long idUtente, final Connection con) {
		CallableStatement cs = null;

		try {
			int index = 1;

			cs = con.prepareCall("DELETE FROM documentoutentestato WHERE iddocumento = ? AND idnodo = ? AND idutente = ?");
			cs.setInt(index++, idDocumento);
			cs.setLong(index++, idUfficio);
			cs.setLong(index++, idUtente);

			cs.execute();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la cancellazione degli item relativi al nodo-utente per il documento: " + idDocumento, e);
			throw new RedException(e);
		} finally {
			closeStatement(cs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IDocumentoDAO#rimuoviItemNodo(int,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public void rimuoviItemNodo(final int idDocumento, final Long idUfficio, final Connection con) {
		CallableStatement cs = null;

		try {
			int index = 1;

			cs = con.prepareCall("DELETE FROM documentoutentestato WHERE iddocumento = ? AND idnodo = ?");
			cs.setInt(index++, idDocumento);
			cs.setLong(index++, idUfficio);

			cs.execute();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la cancellazione degli item relativi al nodo per il documento: " + idDocumento, e);
			throw new RedException(e);
		} finally {
			closeStatement(cs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IDocumentoDAO#getLockDocumento(java.lang.Integer,
	 *      java.lang.Long, java.lang.Integer, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public GestioneLockDTO getLockDocumento(final Integer documentTitle, final Long idAoo, final Integer statoLock, final Long idUtente, final Connection conn) {
		GestioneLockDTO gestioneLockDTO = null;
		Boolean nessunLock = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM LOCK_DOCUMENTO_UTENTE WHERE DOCUMENTTITLE = ? AND IDAOO = ? AND STATO_LOCK = ?");

			ps = conn.prepareStatement(sb.toString());
			ps.setInt(index++, documentTitle);
			ps.setLong(index++, idAoo);
			ps.setInt(index++, statoLock);
			rs = ps.executeQuery();

			gestioneLockDTO = new GestioneLockDTO(idUtente, nessunLock);
			if (rs.next()) {
				if (!idUtente.equals(rs.getLong(IDUTENTE))) {
					nessunLock = false;
					gestioneLockDTO.setIdUtenteCheHaLock(rs.getLong(IDUTENTE));
					gestioneLockDTO.setNessunLock(nessunLock);
					final boolean inCarico = (rs.getString(ISUTENTEINCARICO) != null && rs.getInt(ISUTENTEINCARICO) == 1);
					gestioneLockDTO.setInCarico(inCarico);
					gestioneLockDTO.setNome(rs.getString("NOME"));
					gestioneLockDTO.setCognome(rs.getString(COGNOME));
					if (!StringUtils.isNullOrEmpty(gestioneLockDTO.getNome()) && !StringUtils.isNullOrEmpty(gestioneLockDTO.getCognome())) {
						gestioneLockDTO.setUsernameUtenteLock(gestioneLockDTO.getNome() + " " + gestioneLockDTO.getCognome());
					}
				} else {
					nessunLock = true;
					gestioneLockDTO.setIdUtenteCheHaLock(rs.getLong(IDUTENTE));
					gestioneLockDTO.setNessunLock(nessunLock);
					final boolean inCarico = (rs.getString(ISUTENTEINCARICO) != null && rs.getInt(ISUTENTEINCARICO) == 1);
					gestioneLockDTO.setInCarico(inCarico);
					gestioneLockDTO.setNome(rs.getString("NOME"));
					gestioneLockDTO.setCognome(rs.getString(COGNOME));
					if (!StringUtils.isNullOrEmpty(gestioneLockDTO.getNome()) && !StringUtils.isNullOrEmpty(gestioneLockDTO.getCognome())) {
						gestioneLockDTO.setUsernameUtenteLock(gestioneLockDTO.getNome() + " " + gestioneLockDTO.getCognome());
					}
				}
			}
		} catch (final Exception ex) {
			LOGGER.error(ERROR_RECUPERO_LOCK_MSG + ex);
			throw new RedException(ERROR_RECUPERO_LOCK_MSG + ex);
		} finally {
			closeStatement(ps, rs);
		}
		return gestioneLockDTO;
	}

	/**
	 * @see it.ibm.red.business.dao.IDocumentoDAO#insertLockDocumento(java.lang.Integer,
	 *      java.lang.Long, java.lang.Long, java.lang.Integer, boolean,
	 *      java.lang.String, java.lang.String, java.sql.Connection).
	 */
	@Override
	public boolean insertLockDocumento(final Integer documentTitle, final Long idAoo, final Long idUtente, final Integer statoLock, final boolean isIncarico,
			final String nome, final String cognome, final Connection conn) {
		PreparedStatement ps = null;
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("INSERT INTO LOCK_DOCUMENTO_UTENTE(DOCUMENTTITLE, IDAOO, IDUTENTE, STATO_LOCK, ISUTENTEINCARICO, NOME, COGNOME) VALUES (?,?,?,?,?,?,?)");

			ps = conn.prepareStatement(sb.toString());
			ps.setInt(index++, documentTitle);
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idUtente);
			ps.setInt(index++, statoLock);
			ps.setInt(index++, isIncarico ? 1 : 0);
			ps.setString(index++, nome);
			ps.setString(index++, cognome);

			return ps.executeUpdate() > 0;

		} catch (final Exception ex) {
			LOGGER.error(ERROR_INSERIMENTO_LOCK_MSG + ex);
			throw new RedException(ERROR_INSERIMENTO_LOCK_MSG + ex);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IDocumentoDAO#updateLockDocumento(java.lang.Integer,
	 *      java.lang.Long, java.lang.Integer, java.lang.Long, boolean,
	 *      java.lang.String, java.lang.String, java.sql.Connection).
	 */
	@Override
	public boolean updateLockDocumento(final Integer documentTitle, final Long idAoo, final Integer statoLock, final Long idUtente, final boolean isIncarico,
			final String nome, final String cognome, final Connection conn) {
		PreparedStatement ps = null;
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("UPDATE LOCK_DOCUMENTO_UTENTE SET IDUTENTE = ?, NOME=? , COGNOME=? , ISUTENTEINCARICO = ? WHERE DOCUMENTTITLE = ? AND IDAOO = ? AND STATO_LOCK = ?");

			ps = conn.prepareStatement(sb.toString());
			ps.setLong(index++, idUtente);
			ps.setString(index++, nome);
			ps.setString(index++, cognome);
			ps.setInt(index++, isIncarico ? 1 : 0);
			ps.setInt(index++, documentTitle);
			ps.setLong(index++, idAoo);
			ps.setLong(index++, statoLock);

			return ps.executeUpdate() > 0;

		} catch (final Exception ex) {
			LOGGER.error(ERROR_INSERIMENTO_LOCK_MSG + ex);
			throw new RedException(ERROR_INSERIMENTO_LOCK_MSG + ex);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IDocumentoDAO#deleteLockDocumento(java.lang.Integer,
	 *      java.lang.Long, java.lang.Integer, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public boolean deleteLockDocumento(final Integer documentTitle, final Long idAoo, final Integer statoLock, final Long idUtente, final Connection conn) {
		PreparedStatement ps = null;
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("DELETE FROM LOCK_DOCUMENTO_UTENTE WHERE DOCUMENTTITLE = ? AND IDAOO = ? AND STATO_LOCK = ? AND IDUTENTE = ?");

			ps = conn.prepareStatement(sb.toString());
			ps.setInt(index++, documentTitle);
			ps.setLong(index++, idAoo);
			ps.setLong(index++, statoLock);
			ps.setLong(index++, idUtente);

			return ps.executeUpdate() > 0;

		} catch (final Exception ex) {
			LOGGER.error("Errore durante l'eliminazione del lock del documento :" + ex);
			throw new RedException("Errore durante l'eliminazione del lock del documento :" + ex);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IDocumentoDAO#getLockDocumentoMassivo(java.util.List,
	 *      java.lang.Long, java.lang.Integer, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public GestioneLockDTO getLockDocumentoMassivo(final List<Integer> documentTitle, final Long idAoo, final Integer statoLock, final Long idUtente, final Connection conn) {
		GestioneLockDTO gestioneLockDTO = null;
		Boolean nessunLock = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM LOCK_DOCUMENTO_UTENTE WHERE IDAOO = ? AND STATO_LOCK = ? AND ISUTENTEINCARICO <> 1");

			if (documentTitle != null) {
				sb.append(" AND " + StringUtils.createInCondition(DOCUMENTTITLE, documentTitle.iterator(), false));

			}
			ps = conn.prepareStatement(sb.toString());
			ps.setLong(index++, idAoo);
			ps.setInt(index++, statoLock);
			rs = ps.executeQuery();

			gestioneLockDTO = new GestioneLockDTO(idUtente, nessunLock);
			while (rs.next()) {
				if (!idUtente.equals(rs.getLong(IDUTENTE))) {
					nessunLock = false;
					gestioneLockDTO.setIdUtenteCheHaLock(rs.getLong(IDUTENTE));
					gestioneLockDTO.setNessunLock(nessunLock);
					final boolean inCarico = (rs.getString(ISUTENTEINCARICO) != null && rs.getInt(ISUTENTEINCARICO) == 1);
					gestioneLockDTO.setInCarico(inCarico);
					gestioneLockDTO.setNome(rs.getString("NOME"));
					gestioneLockDTO.setCognome(rs.getString(COGNOME));
				} else {
					nessunLock = true;
					gestioneLockDTO.setIdUtenteCheHaLock(rs.getLong(IDUTENTE));
					gestioneLockDTO.setNessunLock(nessunLock);
					final boolean inCarico = (rs.getString(ISUTENTEINCARICO) != null && rs.getInt(ISUTENTEINCARICO) == 1);
					gestioneLockDTO.setInCarico(inCarico);
					gestioneLockDTO.setNome(rs.getString("NOME"));
					gestioneLockDTO.setCognome(rs.getString(COGNOME));
				}
			}
		} catch (final Exception ex) {
			LOGGER.error(ERROR_RECUPERO_LOCK_MSG + ex);
			throw new RedException(ERROR_RECUPERO_LOCK_MSG + ex);
		} finally {
			closeStatement(ps, rs);
		}
		return gestioneLockDTO;
	}

	/**
	 * @see it.ibm.red.business.dao.IDocumentoDAO#getLockDocumentiMaster(java.util.List,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public HashMap<Long, GestioneLockDTO> getLockDocumentiMaster(final List<String> documentTitle, final Long idAoo, final Connection conn) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<Long> docTitlelongs = new ArrayList<>();
		if (documentTitle != null) {
			for (final String docTit : documentTitle) {
				docTitlelongs.add(Long.parseLong(docTit));
			}
		}
		final HashMap<Long, GestioneLockDTO> listaLock = new HashMap<>();
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM LOCK_DOCUMENTO_UTENTE WHERE IDAOO = ? AND STATO_LOCK=1 AND ISUTENTEINCARICO  <> 1");

			if (docTitlelongs != null) {
				sb.append(" AND " + StringUtils.createInCondition(DOCUMENTTITLE, docTitlelongs.iterator(), false));

			}
			ps = conn.prepareStatement(sb.toString());
			ps.setLong(index++, idAoo);
			rs = ps.executeQuery();

			while (rs.next()) {
				final GestioneLockDTO gestioneLockDTO = new GestioneLockDTO();
				gestioneLockDTO.setIdUtenteCheHaLock(rs.getLong(IDUTENTE));
				gestioneLockDTO.setDocumentTitle(rs.getLong(DOCUMENTTITLE));
				final boolean inCarico = (rs.getString(ISUTENTEINCARICO) != null && rs.getInt(ISUTENTEINCARICO) == 1);
				gestioneLockDTO.setInCarico(inCarico);
				gestioneLockDTO.setNome(rs.getString("NOME"));
				gestioneLockDTO.setCognome(rs.getString(COGNOME));
				if (!StringUtils.isNullOrEmpty(gestioneLockDTO.getNome()) && !StringUtils.isNullOrEmpty(gestioneLockDTO.getCognome())) {
					gestioneLockDTO.setUsernameUtenteLock(gestioneLockDTO.getNome() + " " + gestioneLockDTO.getCognome());
				}
				listaLock.put(gestioneLockDTO.getDocumentTitle(), gestioneLockDTO);
			}
		} catch (final Exception ex) {
			LOGGER.error(ERROR_RECUPERO_LOCK_MSG + ex);
			throw new RedException(ERROR_RECUPERO_LOCK_MSG + ex);
		} finally {
			closeStatement(ps, rs);
		}
		return listaLock;
	}

}