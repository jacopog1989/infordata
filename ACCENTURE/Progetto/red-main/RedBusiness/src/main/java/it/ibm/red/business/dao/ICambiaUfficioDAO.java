package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.persistence.model.Aoo;

/**
 * Interfaccia dao cambio ufficio.
 */
public interface ICambiaUfficioDAO extends Serializable {
	
	/**
	 * Ottiene le Aoo dall'id dell'utente.
	 * @param idUtente - id dell'utente
	 * @param connection
	 * @return lista di aoo
	 */
	List<Aoo> getAOOfromIdUtente(Long idUtente, Connection connection);

	/**
	 * Ottiene gli id dei nodi dall'id dell'utente e dall'id dell'Aoo.
	 * @param idUtente - id dell'utente
	 * @param idAOO - id dell'Aoo
	 * @param connection
	 * @return lista di id
	 */
	List<Long> getIdNodiFromIdUtenteandIdAOO(Long idUtente, Long idAOO, Connection connection);

	/**
	 * Ottiene gli id dei ruoli dall'id dell'utente, dall'id del nodo e dall'id dell'Aoo.
	 * @param idUtente
	 * @param idNodo
	 * @param idAOO
	 * @param connection
	 * @return lista di id
	 */
	List<Long> getIdRuoloFromIdUtenteAndIdNodoAndIdAOO(Long idUtente, Integer idNodo, Long idAOO,
			Connection connection);

}
