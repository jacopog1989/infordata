package it.ibm.red.business.enums;

import npstypes.v1.it.gov.mef.AllTipoRegistroAusiliarioType;

/**
 * Enum che definisce i tipi di registri ausiliari.
 */
public enum TipoRegistroAusiliarioEnum {

	/**
	 * Valore.
	 */
	VISTO(1, "VISTO", AllTipoRegistroAusiliarioType.VISTO),
	
	/**
	 * Valore.
	 */
	OSSERVAZIONE_RILIEVO(2, "OSSERVAZIONE/RILIEVO", AllTipoRegistroAusiliarioType.OSSERVAZIONE),
	
	/**
	 * Valore.
	 */
	RICHIESTA_ULTERIORI_INFORMAZIONI(3, "RICHIESTA ULTERIORI INFORMAZIONI", AllTipoRegistroAusiliarioType.RICHIESTA_INTEGRAZIONE),
	
	/**
	 * Valore.
	 */
	RESTITUZIONI(4, "RESTITUZIONI", AllTipoRegistroAusiliarioType.RESTITUZIONE);

	/**
	 * Codice.
	 */
	private Integer code;

	/**
	 * Descrizione.
	 */
	private String description;

	/**
	 * Tipo registro ausiliario.
	 */
	private AllTipoRegistroAusiliarioType tipoRegistroNPS;

	TipoRegistroAusiliarioEnum(final Integer code, final String description, final AllTipoRegistroAusiliarioType tipoRegistroNPS) {
		this.code = code;
		this.description = description;
		this.tipoRegistroNPS = tipoRegistroNPS;
	}
	
	/**
	 * Restituisce il codice.
	 * @return codice
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Restituisce la descrizione.
	 * @return descrizione
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Restituisce il tipo registro NPS.
	 * @return tipo registro NPS
	 */
	public AllTipoRegistroAusiliarioType getTipoRegistroNPS() {
		return tipoRegistroNPS;
	}

	/**
	 * Restituisce l'enum associata al codice in ingresso.
	 * @param inCode
	 * @return enum associata al codice in ingresso
	 */
	public static TipoRegistroAusiliarioEnum get(final Integer inCode) {
		TipoRegistroAusiliarioEnum output = null;
		
		for (final TipoRegistroAusiliarioEnum e: TipoRegistroAusiliarioEnum.values()) {
			if (e.getCode().equals(inCode)) {
				output = e;
				break;
			}
		}
		
		return output;
	}

}
