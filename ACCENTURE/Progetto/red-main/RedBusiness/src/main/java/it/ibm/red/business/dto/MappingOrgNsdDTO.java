package it.ibm.red.business.dto;

/**
 * DTO che definisce il mapping Org Nsd.
 */
public class MappingOrgNsdDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1198139554675796039L;

	/**
	 * Id aoo.
	 */
	private Long idAooRed;

	/**
	 * Ufficio.
	 */
	private Long idUfficioRed;

	/**
	 * Utente.
	 */
	private Long idUtenteRed;

	/**
	 * Codice aoo.
	 */
	private String codAooNsd;

	/**
	 * Id aoo nsd.
	 */
	private String idAooNsd;

	/**
	 * Codice ufficio nsd.
	 */
	private String codUffNsd;

	/**
	 * Descrizione ufficio nsd.
	 */
	private String descUffNsd;

	/**
	 * Codice fiscale.
	 */
	private String codFiscaleNsd;

	/**
	 * Nome utente.
	 */
	private String nomeUteNsd;

	/**
	 * Cognome utente.
	 */
	private String cognomeUteNsd;

	/**
	 * Id padre.
	 */
	private String idParentNsd;

	/**
	 * Funzionalità.
	 */
	private String funzionalitaNsd;

	/**
	 * Ente.
	 */
	private String enteNsd;

	/**
	 * Parametri NPS.
	 */
	private ParamNpsDfWSDTO paramNpsDTO;

	/**
	 * Costruttore del DTO.
	 * 
	 * @param codAooNsd
	 * @param idAooNsd
	 * @param codUffNsd
	 * @param descUffNsd
	 * @param codFiscaleNsd
	 * @param nomeUteNsd
	 * @param cognomeUteNsd
	 * @param idParentNsd
	 * @param funzionalitaNsd
	 * @param enteNsd
	 */
	public MappingOrgNsdDTO(final String codAooNsd, final String idAooNsd, final String codUffNsd, final String descUffNsd, final String codFiscaleNsd,
			final String nomeUteNsd, final String cognomeUteNsd, final String idParentNsd, final String funzionalitaNsd, final String enteNsd) {
		this.codAooNsd = codAooNsd;
		this.idAooNsd = idAooNsd;
		this.codUffNsd = codUffNsd;
		this.descUffNsd = descUffNsd;
		this.codFiscaleNsd = codFiscaleNsd;
		this.nomeUteNsd = nomeUteNsd;
		this.cognomeUteNsd = cognomeUteNsd;
		this.idParentNsd = idParentNsd;
		this.funzionalitaNsd = funzionalitaNsd;
		this.enteNsd = enteNsd;
	}

	/**
	 * Costruttore.
	 * 
	 * @param codAooNsd
	 * @param idAooNsd
	 * @param codUffNsd
	 * @param descUffNsd
	 * @param codFiscaleNsd
	 * @param nomeUteNsd
	 * @param cognomeUteNsd
	 * @param idParentNsd
	 * @param funzionalitaNsd
	 * @param enteNsd
	 * @param idUfficioRed
	 * @param idUtenteRed
	 * @param idAooRed
	 */
	public MappingOrgNsdDTO(final String codAooNsd, final String idAooNsd, final String codUffNsd, final String descUffNsd, final String codFiscaleNsd,
			final String nomeUteNsd, final String cognomeUteNsd, final String idParentNsd, final String funzionalitaNsd, final String enteNsd, final Long idUfficioRed,
			final Long idUtenteRed, final Long idAooRed) {
		this(codAooNsd, idAooNsd, codUffNsd, descUffNsd, codFiscaleNsd, nomeUteNsd, cognomeUteNsd, idParentNsd, funzionalitaNsd, enteNsd);
		this.idAooRed = idAooRed;
		this.idUfficioRed = idUfficioRed;
		this.idUtenteRed = idUtenteRed;

	}

	/**
	 * Restituisce l'id dell'Area Omogenea Organizzativa di RED.
	 * 
	 * @return id AOO
	 */
	public Long getIdAooRed() {
		return idAooRed;
	}

	/**
	 * Imposta l'id dell'Area Omogenea Organizzativa di RED.
	 * 
	 * @param idAooRed
	 */
	public void setIdAooRed(final Long idAooRed) {
		this.idAooRed = idAooRed;
	}

	/**
	 * Restituisce l'id dell'ufficio RED.
	 * 
	 * @return id ufficio
	 */
	public Long getIdUfficioRed() {
		return idUfficioRed;
	}

	/**
	 * Imposta l'id dell'ufficio RED.
	 * 
	 * @param idUfficioRed
	 */
	public void setIdUfficioRed(final Long idUfficioRed) {
		this.idUfficioRed = idUfficioRed;
	}

	/**
	 * Restituisce l'id dell'utente RED.
	 * 
	 * @return id utente
	 */
	public Long getIdUtenteRed() {
		return idUtenteRed;
	}

	/**
	 * Imposta l'id dell'utente RED.
	 * 
	 * @param idUtenteRed
	 */
	public void setIdUtenteRed(final Long idUtenteRed) {
		this.idUtenteRed = idUtenteRed;
	}

	/**
	 * Restituisce il codice AOO Nsd.
	 * 
	 * @return codAooNsd
	 */
	public String getCodAooNsd() {
		return codAooNsd;
	}

	/**
	 * Imposta il codice AOO Nsd.
	 * 
	 * @param codAooNsd
	 */
	public void setCodAooNsd(final String codAooNsd) {
		this.codAooNsd = codAooNsd;
	}

	/**
	 * Restituisce l'id AOO Nsd.
	 * 
	 * @return id AOO Nsd
	 */
	public String getIdAooNsd() {
		return idAooNsd;
	}

	/**
	 * Imposta l'id AOO Nsd.
	 * 
	 * @param idAooNsd
	 */
	public void setIdAooNsd(final String idAooNsd) {
		this.idAooNsd = idAooNsd;
	}

	/**
	 * Restituisce il codice ufficio Nsd.
	 * 
	 * @return codice ufficio Nsd
	 */
	public String getCodUffNsd() {
		return codUffNsd;
	}

	/**
	 * Imposta il codice ufficio Nsd.
	 * 
	 * @param codUffNsd
	 */
	public void setCodUffNsd(final String codUffNsd) {
		this.codUffNsd = codUffNsd;
	}

	/**
	 * Restituisce la descrizione dell'ufficio Nsd.
	 * 
	 * @return descrizione ufficio Nsd
	 */
	public String getDescUffNsd() {
		return descUffNsd;
	}

	/**
	 * Imposta la descrizione dell'ufficio Nsd.
	 * 
	 * @param descUffNsd
	 */
	public void setDescUffNsd(final String descUffNsd) {
		this.descUffNsd = descUffNsd;
	}

	/**
	 * Restituisce il codice fiscale Nsd.
	 * 
	 * @return codice fiscale Nsd
	 */
	public String getCodFiscaleNsd() {
		return codFiscaleNsd;
	}

	/**
	 * Imposta il codice fiscale Nsd.
	 * 
	 * @param codFiscaleNsd
	 */
	public void setCodFiscaleNsd(final String codFiscaleNsd) {
		this.codFiscaleNsd = codFiscaleNsd;
	}

	/**
	 * Restituisce il nome utente Nsd.
	 * 
	 * @return nome utente Nsd
	 */
	public String getNomeUteNsd() {
		return nomeUteNsd;
	}

	/**
	 * Imposta il nome utente Nsd.
	 * 
	 * @param nomeUteNsd
	 */
	public void setNomeUteNsd(final String nomeUteNsd) {
		this.nomeUteNsd = nomeUteNsd;
	}

	/**
	 * Restituisce il cognome dell'utente Nsd.
	 * 
	 * @return cognome utente Nsd
	 */
	public String getCognomeUteNsd() {
		return cognomeUteNsd;
	}

	/**
	 * Imposta il cognome dell'utente Nsd.
	 * 
	 * @param cognomeUteNsd
	 */
	public void setCognomeUteNsd(final String cognomeUteNsd) {
		this.cognomeUteNsd = cognomeUteNsd;
	}

	/**
	 * Restituisce l'id del Parent di Nsd.+
	 * 
	 * @return id parent Nsd
	 */
	public String getIdParentNsd() {
		return idParentNsd;
	}

	/**
	 * Imposta l'id del parent di Nsd.
	 * 
	 * @param idParentNsd
	 */
	public void setIdParentNsd(final String idParentNsd) {
		this.idParentNsd = idParentNsd;
	}

	/**
	 * Restituisce la funzionalita Nsd.
	 * 
	 * @return funzionalita Nsd
	 */
	public String getFunzionalitaNsd() {
		return funzionalitaNsd;
	}

	/**
	 * Imposta la funzionalita Nsd.
	 * 
	 * @param funzionalitaNsd
	 */
	public void setFunzionalitaNsd(final String funzionalitaNsd) {
		this.funzionalitaNsd = funzionalitaNsd;
	}

	/**
	 * Restituisce l'ente Nsd.
	 * 
	 * @return ente Nsd
	 */
	public String getEnteNsd() {
		return enteNsd;
	}

	/**
	 * Imposta l'ente Nsd.
	 * 
	 * @param enteNsd
	 */
	public void setEnteNsd(final String enteNsd) {
		this.enteNsd = enteNsd;
	}

	/**
	 * Restituisce il DTO dei parametri di Nps.
	 * 
	 * @return parametri Nps
	 */
	public ParamNpsDfWSDTO getParamNpsDTO() {
		return paramNpsDTO;
	}

	/**
	 * Imposta il DTO dei parametri di Nps.
	 * 
	 * @param paramNpsDTO
	 */
	public void setParamNpsDTO(final ParamNpsDfWSDTO paramNpsDTO) {
		this.paramNpsDTO = paramNpsDTO;
	}
}
