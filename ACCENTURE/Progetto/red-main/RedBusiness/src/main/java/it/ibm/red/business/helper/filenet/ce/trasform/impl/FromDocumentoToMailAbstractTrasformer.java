package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.filenet.api.collection.FolderSet;
import com.filenet.api.core.Document;
import com.filenet.api.core.Folder;

import it.ibm.red.business.dao.IPostaNpsDAO;
import it.ibm.red.business.dto.AssegnatarioInteropDTO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.NotaDTO;
import it.ibm.red.business.enums.ColoreInteroperabilitaEnum;
import it.ibm.red.business.enums.ColoreNotaEnum;
import it.ibm.red.business.enums.EsitoValidazioneSegnaturaInteropEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.StatoMailEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.StringUtils;

/**
 * Trasformer ricerca mail.
 *
 * @author VINGENITO
 */
public abstract class FromDocumentoToMailAbstractTrasformer extends TrasformerCE<EmailDTO> {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 7115280317344104212L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentoToMailAbstractTrasformer.class.getName());
	
	/**
	 * Label.
	 */
	private static final String PREASSEGNATARIO_NON_VALIDO = "Non Valido";
	
	/**
	 * DAO.
	 */
	private final IPostaNpsDAO postaNpsDAO;
	
	/**
	 * Costruttore.
	 * 
	 * @param inEnumKey
	 */
	public FromDocumentoToMailAbstractTrasformer(final TrasformerCEEnum inEnumKey) {
		super(inEnumKey);
		postaNpsDAO = ApplicationContextProvider.getApplicationContext().getBean(IPostaNpsDAO.class);
	}
	
	/**
	 * Trasform.
	 *
	 * @param document
	 *            the document
	 * @param connection
	 *            the connection
	 * @param setFolder
	 *            set folder
	 * @return the email DTO
	 */
	@SuppressWarnings("unchecked")
	protected final EmailDTO trasform(final Document document, final Connection connection, final TrasformerCEEnum trasformer) {
		EmailDTO m = null;
		
		try {
			m = new EmailDTO();
			final NotaDTO nota = new NotaDTO();
			String contentNote = (String) getMetadato(document, PropertiesNameEnum.NOTA_EMAIL_METAKEY);
			
			m.setDestinatari((String) getMetadato(document, PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY));
			m.setMittente((String) getMetadato(document, PropertiesNameEnum.FROM_MAIL_METAKEY));
			m.setOggetto((String) getMetadato(document, PropertiesNameEnum.OGGETTO_EMAIL_METAKEY));
			m.setHasAllegati(!document.get_ChildDocuments().isEmpty());
			m.setGuid(document.get_Id().toString());
			
			if (contentNote != null) {
				final String[] note = StringUtils.splittaNota(contentNote);
				contentNote = note[1];
				final int i = getIndex(note);
				nota.setColore(ColoreNotaEnum.get(i));
				nota.setContentNota(contentNote);
			}
			
			m.setNota(nota);
			final Date sentON = (Date) getMetadato(document, PropertiesNameEnum.DATA_INVIO_MAIL_METAKEY);
			final Date receivedON = (Date) getMetadato(document, PropertiesNameEnum.DATA_RICEZIONE_MAIL_METAKEY);
			final Date cambioStatoON = (Date) getMetadato(document, PropertiesNameEnum.DATA_CAMBIO_STATO_EMAIL_METAKEY);
			
			if (sentON != null) {
				m.setDataInvio(sentON);
			}
			if (receivedON != null) {
				m.setDataRicezione(receivedON);
			}
			if (cambioStatoON != null) {
				m.setDataCambioStato(cambioStatoON);
			}
			
			m.setDestinatariCC((String) getMetadato(document, PropertiesNameEnum.DESTINATARI_CC_LONG));
			m.setMsgID((String) getMetadato(document, PropertiesNameEnum.MESSAGE_ID));
			m.setMotivoEliminazione((String) getMetadato(document, PropertiesNameEnum.MOTIVO_ELIMINAZIONE_EMAIL_METAKEY));
			
			Date dataScarico = (Date) getMetadato(document, PropertiesNameEnum.DATA_SCARICO_METAKEY);
			
			if (dataScarico == null) {
				dataScarico = (Date) getMetadato(document, PropertiesNameEnum.DATA_CREAZIONE_METAKEY);
			}
			
			if (dataScarico != null) {
				m.setDataScarico(dataScarico);
			}
			
			final Integer nStatoMail = (Integer) getMetadato(document, PropertiesNameEnum.STATO_MAIL_METAKEY);
			final StatoMailEnum statoMail = StatoMailEnum.get(nStatoMail);
			m.setStato(statoMail);
			final Integer idUtenteProtocollatore = (Integer) getMetadato(document, PropertiesNameEnum.ID_UTENTE_PROTOCOLLATORE_METAKEY);
			m.setIdUtenteProtocollatore(idUtenteProtocollatore);
			
			if(!TrasformerCEEnum.FROM_DOCUMENTO_TO_MAIL_MASTER_WITH_CONTENT.equals(trasformer)) {
				final Integer nStatoMailPreArch = (Integer) getMetadato(document, PropertiesNameEnum.METADATO_STATO_PRE_ARCHIVIAZIONE);
				final StatoMailEnum statoMailPreArch = StatoMailEnum.get(nStatoMailPreArch);
				m.setStatoPreArchiviazione(statoMailPreArch);
				
				final String esitoValidazione = (String) getMetadato(document, PropertiesNameEnum.VALIDAZIONE_SEGN_INTEROP_MAIL_METAKEY);
				if (esitoValidazione != null) {
					final EsitoValidazioneSegnaturaInteropEnum esitoValidazioneEnum = EsitoValidazioneSegnaturaInteropEnum.get(esitoValidazione);
					m.setEsitoValidazioneEnum(esitoValidazioneEnum);
					m.setColoreInteroperabilita(ColoreInteroperabilitaEnum.get(esitoValidazione));
				} 
				 
				final String preassegnatario = (String) getMetadato(document, PropertiesNameEnum.PREASSEGNATARIO_SEGN_INTEROP_MAIL_METAKEY);
				if (preassegnatario != null) { 
				   AssegnatarioInteropDTO assegnatarioInterop = new AssegnatarioInteropDTO(preassegnatario);
					if (((assegnatarioInterop.getIdUfficio() == null || assegnatarioInterop.getIdUfficio() == 0L)  
						&& (assegnatarioInterop.getIdUtente() == null || assegnatarioInterop.getIdUtente() == 0L)) 
						&& (assegnatarioInterop.getDescrizioneUfficio() == null && assegnatarioInterop.getDescrizioneUtente() == null)) {
							m.setColorePreassegnatario(ColoreInteroperabilitaEnum.ROSSO);
							m.setTooltipPreassegnatario(PREASSEGNATARIO_NON_VALIDO); 
					 } else {
						 if (assegnatarioInterop.getIdUfficio() != null || assegnatarioInterop.getDescrizioneUfficio() != null) {
							 m.setColorePreassegnatario(ColoreInteroperabilitaEnum.VERDE);
							 assegnatarioInterop = postaNpsDAO.getDescrizioneById(assegnatarioInterop, connection); 
							 m.setTooltipPreassegnatario(assegnatarioInterop.getDescrizioneUfficio());
							 if (assegnatarioInterop.getDescrizioneUtente() != null) {
							 		m.setTooltipPreassegnatario(assegnatarioInterop.getDescrizioneUfficio() + " - " + assegnatarioInterop.getDescrizioneUtente());
							 	}
							 m.setPreassegnatarioInterop(assegnatarioInterop);
						 }
					 }
				} else { 
					m.setColorePreassegnatario(ColoreInteroperabilitaEnum.GIALLO);
				}      
				
				// Struttura metadato: "NUMERO_PROTOCOLLO/ANNO_PROTOCOLLO"
				final List<String> protocolliGenerati = (List<String>) getMetadato(document, PropertiesNameEnum.PROTOCOLLI_GENERATI_MAIL_METAKEY);
				if (!CollectionUtils.isEmpty(protocolliGenerati)) {
					m.setProtocolliGenerati(protocolliGenerati);
				} 
				 
				final String operazionePostaElettronica = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.OPERAZIONE_POSTA_ELETTRONICA_FN_METAKEY);
				if (operazionePostaElettronica != null) {
					final String[] operazionePostaElettronicaSplit = operazionePostaElettronica.split("#");
					m.setOperazionePostaElettronica(operazionePostaElettronicaSplit[0]);
					final List<String> listOperazioniPosta = new ArrayList<>();
						listOperazioniPosta.add(operazionePostaElettronicaSplit[0]);
					if (operazionePostaElettronicaSplit.length > 1) {
						listOperazioniPosta.addAll(1, Arrays.asList(operazionePostaElettronicaSplit));
					}
					m.setListOperazionePostaElettronica(listOperazioniPosta);
				} 
				
				if(TrasformerCEEnum.FROM_DOCUMENTO_TO_MAIL_RICERCA.equals(trasformer)) {
					final FolderSet fs = document.get_FoldersFiledIn();
					if (fs != null) {
						String folder = null;
						final Iterator<?> itf = fs.iterator();
						if (itf.hasNext()) {
							folder = ((Folder) itf.next()).get_FolderName();
							if (PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_INBOX_FN_METAKEY).equals(folder)) {
								m.setFolderMailInbox(folder);
							} else {
								m.setFolderMailArchiviate(folder);
							}
						}
						 
					}
				}
				
				if(TrasformerCEEnum.FROM_DOCUMENTO_TO_MAIL_MASTER.equals(trasformer)) {
					final Boolean isProtocollaEMantieni = (Boolean) TrasformerCE.getMetadato(document, PropertiesNameEnum.PROTOCOLLA_E_MANTIENI_METAKEY);
					if(Boolean.TRUE.equals(isProtocollaEMantieni)) {
						m.setProtocollaEMantieni(isProtocollaEMantieni);
					}
				}
				
				final String documentTitle = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
				m.setDocumentTitle(documentTitle);
			}
			
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero di un metadato del master della mail", e);
		}
		
		return m;
	}
	
	/**
	 * @param note
	 * @return indice
	 */
	private static int getIndex(final String[] note) {
		int index = 0;
		try {
			index = Integer.parseInt(note[0].substring(1));
		} catch (final Exception e) {
			LOGGER.warn(e);
		}
		return index;
	}
}

