package it.ibm.red.business.dao.impl;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IMockDAO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaNPSDTO;
import it.ibm.red.business.enums.TipoProtocolloEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * Dao per la gestione dei mock.
 *
 * @author a.dilegge
 */
@Repository
public class MockDAO extends AbstractDAO implements IMockDAO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 825657946267375095L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(MockDAO.class.getName());

	/**
	 * @see it.ibm.red.business.dao.IMockDAO#getNextProt(java.lang.String,
	 *      it.ibm.red.business.enums.TipoProtocolloEnum, java.sql.Connection).
	 */
	@Override
	public Integer getNextProt(final String codiceAoo, final TipoProtocolloEnum tipoProtocollo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer result = 0;
		try {

			final String sequenceName = "MOCK_" + codiceAoo + "_SEQ_PROT_" + tipoProtocollo.getCodice();

			ps = con.prepareStatement("SELECT " + sequenceName + ".NEXTVAL FROM DUAL");
			rs = ps.executeQuery();
			if (rs.next()) {
				result = rs.getInt(1);
			}
			return result;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero della sequence la protocollazione mock.", e);
			throw new RedException("Errore durante il recupero della sequence per la protocollazione mock.", e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IMockDAO#insertProt(it.ibm.red.business.dto.ProtocolloDTO,
	 *      java.lang.String, it.ibm.red.business.enums.TipoProtocolloEnum,
	 *      java.sql.Connection).
	 */
	@Override
	public void insertProt(final ProtocolloDTO protocollo, final String codiceAoo, final TipoProtocolloEnum tipoProtocollo, final Connection con) {
		int index = 1;
		PreparedStatement ps = null;
		final StringBuilder query = new StringBuilder();
		try {

			query.append("INSERT INTO mock_protocollo ( ");
			query.append(" codice_aoo, ");
			query.append(" tipo_protocollo, ");
			query.append(" id_protocollo, ");
			query.append(" numero_protocollo, ");
			query.append(" data_protocollo ");
			query.append(" ) VALUES (?, ?, ?, ?, ?) ");

			ps = con.prepareStatement(query.toString());
			ps.setString(index++, codiceAoo);
			ps.setString(index++, tipoProtocollo.getCodice());
			ps.setString(index++, protocollo.getIdProtocollo());
			ps.setInt(index++, protocollo.getNumeroProtocollo());
			ps.setTimestamp(index++, new Timestamp(protocollo.getDataProtocollo().getTime()));
			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'inserimento del protocollo mock", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IMockDAO#getProt(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public ProtocolloDTO getProt(final String idProtocollo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ProtocolloDTO result = null;
		SimpleDateFormat sdf = null;
		try {

			sdf = new SimpleDateFormat("yyyy");
			ps = connection.prepareStatement("SELECT * FROM mock_prototollo where id_protocollo = ?");
			ps.setString(1, idProtocollo);
			rs = ps.executeQuery();
			if (rs.next()) {
				final Date dataProtocollo = rs.getDate("data_protocollo");
				result = new ProtocolloDTO(rs.getString("id_protocollo"), rs.getInt("numero_protocollo"), sdf.format(dataProtocollo), dataProtocollo, null, null);
			}
			return result;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero del protocollo mock.", e);
			throw new RedException("Errore durante il recupero del protocollo mock.", e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IMockDAO#getDocumento(java.lang.String, java.lang.String, java.sql.Connection).
	 */
	@Override
	public FileDTO getDocumento(final String tipoDocumento, final String serverName, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		FileDTO result = null;
		try {

			ps = connection.prepareStatement("SELECT * FROM mock_documento where tipo_documento = ? and (servername = ? or servername is null) order by servername");
			ps.setString(1, tipoDocumento);
			ps.setString(2, serverName);
			rs = ps.executeQuery();
			if (rs.next()) {
				final Blob blob = rs.getBlob("content");
				byte[] blobAsBytes = null;

				if (blob != null) {
					final int blobLength = (int) blob.length();
					blobAsBytes = blob.getBytes(1, blobLength);
					blob.free();
				}
				result = new FileDTO(rs.getString("filename"), blobAsBytes, rs.getString("mimetype"));
			}
			return result;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero del documento mock.", e);
			throw new RedException("Errore durante il recupero del documento mock.", e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IMockDAO#getProtocolli(java.lang.String,
	 *      java.lang.Integer, java.lang.Integer, int, java.sql.Connection).
	 */
	@Override
	public List<ProtocolloDTO> getProtocolli(final String codiceAoo, final Integer numeroProtocolloDa, final Integer numeroProtocolloA, final int maxResults,
			final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<ProtocolloDTO> result = new ArrayList<>();
		SimpleDateFormat sdf = null;
		try {

			sdf = new SimpleDateFormat("yyyy");

			final StringBuilder sql = new StringBuilder("select * from (select * FROM mock_prototollo where codice_aoo = ?");
			if (numeroProtocolloDa != null && numeroProtocolloDa != 0) {
				sql.append(" and numero_protocollo >= ? ");
			}
			if (numeroProtocolloA != null && numeroProtocolloA != 0) {
				sql.append(" and numero_protocollo <= ? ");
			}
			sql.append(" order by data_protocollo desc) where rownum < ? ");

			ps = connection.prepareStatement(sql.toString());

			int index = 1;
			ps.setString(index++, codiceAoo);
			if (numeroProtocolloDa != null && numeroProtocolloDa != 0) {
				ps.setInt(index++, numeroProtocolloDa);
			}
			if (numeroProtocolloA != null && numeroProtocolloA != 0) {
				ps.setInt(index++, numeroProtocolloA);
			}
			ps.setInt(index++, maxResults);
			rs = ps.executeQuery();
			ProtocolloDTO prot = null;
			if (rs.next()) {
				final Date dataProtocollo = rs.getDate("data_protocollo");
				prot = new ProtocolloDTO(rs.getString("id_protocollo"), rs.getInt("numero_protocollo"), sdf.format(dataProtocollo), dataProtocollo, null, null);
				result.add(prot);
			}
			return result;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero dei protocollo mock.", e);
			throw new RedException("Errore durante il recupero dei protocollo mock.", e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IMockDAO#getNextRegistrazione(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public Integer getNextRegistrazione(final String codiceAoo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer result = 0;
		try {

			final String sequenceName = "MOCK_" + codiceAoo + "_SEQ_REG";

			ps = connection.prepareStatement("SELECT " + sequenceName + ".NEXTVAL FROM DUAL");
			rs = ps.executeQuery();
			if (rs.next()) {
				result = rs.getInt(1);
			}
			return result;

		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero della sequence la registrazione mock.", e);
			throw new RedException("Errore durante il recupero della sequence per la registrazione mock.", e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IMockDAO#insertRegistrazione(it.ibm.red.business.dto.RegistrazioneAusiliariaNPSDTO,
	 *      java.lang.String, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public void insertRegistrazione(final RegistrazioneAusiliariaNPSDTO registrazione, final String codiceAoo, final Integer idRegistro, final Connection connection) {

		int index = 1;
		PreparedStatement ps = null;
		final StringBuilder query = new StringBuilder();
		try {

			query.append("INSERT INTO mock_registrazione ( ");
			query.append(" codice_aoo, ");
			query.append(" id_registro, ");
			query.append(" id_registrazione, ");
			query.append(" numero_registrazione, ");
			query.append(" data_registrazione ");
			query.append(" ) VALUES (?, ?, ?, ?, ?) ");

			ps = connection.prepareStatement(query.toString());
			ps.setString(index++, codiceAoo);
			ps.setInt(index++, idRegistro);
			ps.setString(index++, registrazione.getId());
			ps.setInt(index++, registrazione.getNumeroRegistrazione());
			ps.setTimestamp(index++, new Timestamp(registrazione.getDataRegistrazione().getTime()));
			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'inserimento del protocollo mock", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}

	}

}