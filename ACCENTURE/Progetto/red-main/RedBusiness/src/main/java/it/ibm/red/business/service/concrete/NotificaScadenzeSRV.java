package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.EventoSottoscrizioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.service.INotificaScadenzeSRV;
import it.ibm.red.business.service.ISottoscrizioniSRV;
import it.ibm.red.business.utils.DateUtils;

/**
 * Service gestione notifica scadenze.
 */
@Service
@Component
public class NotificaScadenzeSRV extends NotificaWriteAbstractSRV implements INotificaScadenzeSRV {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 5072215933806523575L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotificaScadenzeSRV.class.getName());

	/**
	 * Service.
	 */
	@Autowired
	private ISottoscrizioniSRV sottoscrizioniSRV;

	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getSottoscrizioni(int).
	 */
	@Override
	protected List<EventoSottoscrizioneDTO> getSottoscrizioni(final int idAoo) {
	
		Connection con = null;
		List<EventoSottoscrizioneDTO> eventi = null;
		try {
		
			con = setupConnection(getFilenetDataSource().getConnection(), false);
			eventi = sottoscrizioniSRV.getEventiSottoscrittiByCategoria(idAoo, Constants.Sottoscrizioni.PROCEDIMENTI_IN_SCADENZA, con);			
		
		} catch (final Exception e) {
		
			LOGGER.error("Errore in fase recupero delle sottoscrizioni per gli eventi di categoria PROCEDIMENTI_IN_SCADENZA per l'aoo " + idAoo, e);
		
		} finally {
			closeConnection(con);
		}
		
		return eventi;
	}
	
	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getDataDocumenti(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.persistence.model.Aoo, int, java.lang.Object).
	 */
	@Override
	protected List<InfoDocumenti> getDataDocumenti(final UtenteDTO utente, final Aoo aoo, final int idEvento, final Object objectForGetDataDocument) {
		throw new RedException("Metodo non previsto per il servizio ");
	}

	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getDataDocumenti(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.persistence.model.Aoo, int).
	 */
	@Override
	protected List<InfoDocumenti> getDataDocumenti(final UtenteDTO utente, final Aoo aoo, final int idEvento) {
		InfoDocumenti documentoInScadenza = null;
		final List<InfoDocumenti> documentiInScadenza = new ArrayList<>();

		final AooFilenet aooFilenet = aoo.getAooFilenet();
		final FilenetPEHelper fpeh = new FilenetPEHelper(utente.getUsername(), aooFilenet.getPassword(), 
				aooFilenet.getUri(), aooFilenet.getStanzaJaas(), aooFilenet.getConnectionPoint());
		
		//non serve differenziare la coda Da Lavorare per gli UCB in quanto la coda non è un dato di output
		final DocumentQueueEnum[] queues = new DocumentQueueEnum[] {
				DocumentQueueEnum.DA_LAVORARE, DocumentQueueEnum.SOSPESO, DocumentQueueEnum.NSD
		};
		
		for (final DocumentQueueEnum queue: queues) {
			
			final VWQueueQuery query = fpeh.getWorkFlowsByWobQueueFilterRed(null, queue, queue.getIndexName(), 
					utente.getIdUfficio(), Arrays.asList(utente.getId()), getPP().getParameterByString(aoo.getCodiceAoo() + "." + PropertiesNameEnum.WS_IDCLIENT.getKey()), 
					null, queue.equals(DocumentQueueEnum.NSD) ? 1 : null, null, null, null, 
					Date.from(DateUtils.NULL_DATE.atZone(ZoneId.systemDefault()).toInstant()), null, false);
			
			while (query.hasNext()) {
				
				documentoInScadenza = InfoDocumenti.getInstance();
				
				final VWWorkObject wo = (VWWorkObject) query.next();
				
				final Integer idDocumento = (Integer) TrasformerPE.getMetadato(wo, getPP().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
				documentoInScadenza.setIdDocumento(idDocumento);
				
				final Date dataScadenza = (Date) TrasformerPE.getMetadato(wo, getPP().getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY));
				final Date nullDate = Date.from(DateUtils.NULL_DATE.atZone(ZoneId.systemDefault()).toInstant());	
				if (dataScadenza != null && !dataScadenza.equals(nullDate)) {
					documentoInScadenza.setDataScadenza(dataScadenza);
					documentiInScadenza.add(documentoInScadenza);
				}
				
			}
			
			
		}
		
		return documentiInScadenza;
		
	}
	
}