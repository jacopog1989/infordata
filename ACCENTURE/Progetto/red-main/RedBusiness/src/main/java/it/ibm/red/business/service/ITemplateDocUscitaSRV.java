package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.TemplateMetadatoDTO;
import it.ibm.red.business.dto.TemplateMetadatoValueDTO;
import it.ibm.red.business.dto.TemplateValueDTO;
import it.ibm.red.business.service.facade.ITemplateDocUscitaFacadeSRV;

/**
 * The Interface ITemplateDocUscitaSRV.
 *
 * @author adilegge
 * 
 *         
 */
public interface ITemplateDocUscitaSRV extends ITemplateDocUscitaFacadeSRV {
	
	/**
	 * Elimina i metadati eventualmente associati al documento e, nel caso la mappa sia valorizzata, inserisci i nuovi valori.
	 * 
	 * @param idDocumento
	 * @param templateDocUscitaMap
	 * @param conn
	 */
	void aggiornaDatiDocumento(int idDocumento, Map<TemplateValueDTO, List<TemplateMetadatoValueDTO>> templateDocUscitaMap, Long idUtenteCreatore, Connection conn);
	
	/**
	 * Recupera la lista dei metadati del template
	 * 
	 * @param guidTemplate
	 * @param conn
	 * @return
	 */
	List<TemplateMetadatoDTO> getMetadati(String guidTemplate, Connection conn);

	
	
}
