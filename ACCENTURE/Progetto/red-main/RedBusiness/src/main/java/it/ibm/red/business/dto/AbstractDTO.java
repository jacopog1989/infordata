package it.ibm.red.business.dto;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import it.ibm.red.business.utils.LogStyleNotNull;

/**
 * The Class AbstractDTO.
 *
 * Data Transfer Object base.
 * 
 * @author CPIERASC
 * 
 */
public abstract class AbstractDTO implements Serializable {

	/**
	 * Serializable.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Booleano per indicare la selezione o meno dell'oggetto quando usato per la
	 * presentation.
	 */
	private transient Boolean selected;
	
	/**
	 * Costruttore.
	 */
	protected AbstractDTO() {
	}

	/**
	 * Getter booleano selezione.
	 * 
	 * @return booleano selezione
	 */
	public final Boolean getSelected() {
		return selected;
	}

	/**
	 * Setter booleano selezione.
	 * 
	 * @param inSelected
	 *            booleano selezione
	 */
	public final void setSelected(final Boolean inSelected) {
		this.selected = inSelected;
	}

	/**
	 * @see java.lang.Object#toString().
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, new LogStyleNotNull()); 
	}

}
