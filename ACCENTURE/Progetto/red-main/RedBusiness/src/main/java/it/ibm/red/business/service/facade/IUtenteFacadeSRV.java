package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import it.ibm.red.business.dto.CoordinatoreUfficioDTO;
import it.ibm.red.business.dto.DelegatoDTO;
import it.ibm.red.business.dto.PreferenzeDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.LogDownloadContentEnum;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.persistence.model.UtenteFirma;

/**
 * Servizio gestione utente.
 *
 * @author CPIERASC
 */
public interface IUtenteFacadeSRV extends Serializable {

	/**
	 * Recupera l'utente identificato dal {@code username}.
	 * 
	 * @param username
	 *            Username dell'utente.
	 * @param idRuolo
	 *            Identificativo del ruolo dell'utente.
	 * @param idNodo
	 *            Identificativo del nodo dell'utente.
	 * @return Utente recuperato.
	 */
	UtenteDTO getByUsername(String username, Long idRuolo, Long idNodo);

	/**
	 * Recupera l'utente identificato dall' {@code idUtente}.
	 * 
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @param idRuolo
	 *            Identificativo del ruolo dell'utente.
	 * @param idNodo
	 *            Identificativo del nodo dell'utente.
	 * @return Utente recuperato.
	 */
	UtenteDTO getById(Long idUtente, Long idRuolo, Long idNodo);

	/**
	 * Recupero utente identificato dallo {@code username}.
	 * 
	 * @param username
	 *            username utente
	 * @return dati utente
	 */
	UtenteDTO getByUsername(String username);

	/**
	 * Metodo per il tracciamento di un download.
	 * 
	 * @param idUtente
	 *            identificativo utente
	 * @param idRuolo
	 *            identificativo ruolo
	 * @param idUfficio
	 *            identificativo ufficio
	 * @param documentTitle
	 *            document title contenuto scaricato
	 * @param logDownload
	 *            descrizione contenuto scaricato
	 * @param ip
	 *            ip del chiamante
	 * @param os
	 *            sistema operativo usato dal chiamante
	 * @param browser
	 *            browser usato dal chiamante
	 * @param userAgent
	 *            user agent della request del chiamante
	 */
	void logDownload(Long idUtente, Long idRuolo, Long idUfficio, String documentTitle,
			LogDownloadContentEnum logDownload, String ip, String os, String browser, String userAgent);

	/**
	 * Metodo per il tracciamento di un download.
	 * 
	 * @param idUtente
	 *            identificativo utente
	 * @param idRuolo
	 *            identificativo ruolo
	 * @param idUfficio
	 *            identificativo ufficio
	 * @param documentTitle
	 *            document title contenuto scaricato
	 * @param logDownload
	 *            descrizione contenuto scaricato
	 * @param ip
	 *            ip del chiamante
	 * @param os
	 *            sistema operativo usato dal chiamante
	 * @param browser
	 *            browser usato dal chiamante
	 * @param userAgent
	 *            user agent della request del chiamante
	 * @param source
	 *            sorgente
	 */
	void logDownload(Long idUtente, Long idRuolo, Long idUfficio, String documentTitle,
			LogDownloadContentEnum logDownload, String ip, String os, String browser, String userAgent, String source);

	/**
	 * Service per l'update del pin verificato.
	 * 
	 * @param idUtente
	 *            - Identificato dell'utente
	 * @param pin
	 *            - PIN
	 * @param pinVerificato
	 *            - bool del pin verificato
	 * 
	 * @return esito dell'operazione
	 */
	boolean setPinVerificato(Long idUtente, String pin, boolean pinVerificato);

	/**
	 * Recupero coordinatori.
	 * 
	 * @param idUfficio
	 *            identificativo ufficio
	 * @return lista coordinatori
	 */
	Collection<CoordinatoreUfficioDTO> getCoordinatoriUfficio(Integer idUfficio);

	/**
	 * Il metodo controlla se il ruolo in input ha permessi da amministratore di
	 * AOO.
	 * 
	 * @param idRuolo
	 *            Identificativo del ruolo dell'utente.
	 * @return {@code true} se il ruolo identificato dall' {@code idRuolo} consente
	 *         l'amministrazione dell'area organizzativa.
	 */
	Boolean hasPermessoAmministratore(Long idRuolo);

	/**
	 * Metodo per assegnare il ruolo Delegato.
	 * 
	 * @param idNodo
	 *            Identificativo del nodo.
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @param dataDisattivazione
	 *            Data disattivazione.
	 * @return {@code true} se l'utente identificato dall' {@code idUtente} ha il
	 *         ruolo di delegato al libro firma.
	 */
	Boolean addRuoloDelegato(Long idNodo, Long idUtente, Long idAoo, Date dataAttivazione, Date dataDisattivazione,
			String motivoDelega);

	/**
	 * Metodo per rimuovere i ruoli di Delegato.
	 * 
	 * @param masters
	 *            I ruoli delegati da rimuovere.
	 * @return Esito della rimozione.
	 */
	Boolean removeRuoliDelegato(Collection<DelegatoDTO> masters, Long idAoo);

	/**
	 * Metodo per il recupero degli utenti al quale è stato assegnato il Ruolo da
	 * Delegato Libro Firma.
	 * 
	 * @param idNodo
	 *            Identificativo del nodo utente.
	 * @param idAOO
	 *            Identificativo dell'area organizzativa.
	 * @return Delegati al libro firma per l'area organizzativa identificata dall'
	 *         {@code idAOO}.
	 */
	Collection<DelegatoDTO> getDelegatiLibroFirma(Long idNodo, Long idAOO, boolean allNodes);

	/**
	 * Metodo per verificare che dato un utente e un nodo i ruoli ad esso collegati
	 * abbiano il permesso passato come parametro.
	 * 
	 * @param idNodo
	 *            Identificativo del nodo.
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @param idAOO
	 *            Identificativo dell'area organizzativa.
	 * @param permessoCompare
	 *            Permesso da verificare.
	 * @return {@code true} se l'utente detiene il permesso indicato dal
	 *         {@code permessoCompare}, {@code false} altrimenti.
	 */
	boolean checkRuoliHasPermesso(Long idNodo, Long idUtente, Long idAOO, PermessiEnum permessoCompare);

	/**
	 * Ottiene l'utente tramite l'id.
	 * 
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @return utente Utente recuperato se esistente, {@code null} altrimenti.
	 */
	Utente getUtenteById(Long idUtente);

	/**
	 * Aggiorna l'utente di firma.
	 * 
	 * @param idUtente
	 *            Identificativo utente da aggiornare.
	 * @param key
	 *            Chiave da aggiornare.
	 * @param value
	 *            Valore da aggiornare.
	 * @return Esito dell'aggiornamento.
	 */
	int aggiornaUtenteFirma(Long idUtente, String key, String value);

	/**
	 * Ottiene l'utente di firma tramite l'id.
	 * 
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @return utente di firma recuperato.
	 */
	UtenteFirma getUtenteFirmaById(Long idUtente);

	/**
	 * Verifica se l'utente ha più uffici attivi.
	 * 
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @return {@code true} se l'utente ha più di un ufficio, {@code false}
	 *         altrimenti.
	 */
	boolean haPiuUfficiAttivi(Long idUtente);

	/**
	 * Ottiene l'utente tramite l'id.
	 * 
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @return Utente recuperato se esistente, {@code null} altrimenti.
	 */
	UtenteDTO getById(Long idUtente);

	/**
	 * Modifica le preferenze mobile dell'utente.
	 * 
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @param pref
	 *            Preferenza mobile.
	 * @return true o false
	 */
	boolean changeMobilePreference(long idUtente, PreferenzeDTO pref);

	/**
	 * Ottiene l'utente tramite lo username e i parametri di firma.
	 * 
	 * @param username
	 *            Nome utente.
	 * @param urlHandlerFirma
	 *            URL dell'handler di firma.
	 * @param codiceTipoFirma
	 *            Codice che identifica la tipologia della firma.
	 * @return Utente recuperato.
	 */
	UtenteDTO getByUsernameAndSignParams(String username, String urlHandlerFirma, String codiceTipoFirma);

	/**
	 * Registra la chiusura della sessione.
	 * 
	 * @param id
	 *            Identificativo sessione.
	 * @param user
	 *            Utente in sessione.
	 */
	void registerCloseSession(String id, String user);

	/**
	 * Registra l'apertura della sessione.
	 * 
	 * @param id
	 * @param ip
	 * @param user
	 */
	void registerOpenSession(String id, String ip, String user);

	/**
	 * Ottiene il numero di sessioni aperte.
	 * 
	 * @return numero di sessioni aperte
	 */
	Integer getSessioniAperte();

	/**
	 * Ottiene gli utenti nel nodo.
	 * 
	 * @param ip
	 * @return username degli utenti
	 */
	Collection<String> getUtentiInNodo(String ip);

	/**
	 * Ottiene le sessioni aperte per utente.
	 * 
	 * @return mappa username numero di sessioni
	 */
	Map<String, Integer> getSessioniApertePerUtente();

	/**
	 * Ottiene le sessioni aperte per nodo.
	 * 
	 * @return mappa ip numero di sessioni
	 */
	Map<String, Integer> getSessioniApertePerNodo();

	/**
	 * Ottiene l'amministratore dell'aoo.
	 * 
	 * @param codeAoo
	 * @return utente amministratore
	 */
	UtenteDTO getAmministratoreAoo(String codeAoo);

	/**
	 * Ottiene l'utente tramite username per la spedizione.
	 * 
	 * @param username
	 * @return nome dell'utente
	 */
	String getByUsernameForSpedizione(String username);

	/**
	 * Ottiene l'utente tramite il codice fiscale.
	 * 
	 * @param codiceFiscale
	 * @return utente
	 */
	UtenteDTO getByCodiceFiscale(String codiceFiscale);

	/**
	 * Ottiene l'id dell'utente tramite il codice fiscale.
	 * 
	 * @param codiceFiscale
	 * @return id dell'utente
	 */
	Long getIdByCodiceFiscale(String codiceFiscale);

}
