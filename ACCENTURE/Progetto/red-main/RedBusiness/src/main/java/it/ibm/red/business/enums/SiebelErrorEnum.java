package it.ibm.red.business.enums;

public enum SiebelErrorEnum {
	
	/**
	 * Value.
	 */
	VALIDATION_ERROR(-1),

	/**
	 * Value.
	 */
	INFO_UNKNOWN_ERROR(-2),

	/**
	 * Value.
	 */
	DB_ACCESS_ERROR(-3),

	/**
	 * Value.
	 */
	CE_ERROR(-4),

	/**
	 * Value.
	 */
	PE_ERROR(-5),

	/**
	 * Value.
	 */
	GENERIC_ERROR(-99);
	
	int errorCode;
	
	private SiebelErrorEnum(int errorCode){
		this.errorCode = errorCode;
	}
	
	/**
	 * Restituisce il codice errore.
	 * @return errorCode
	 */
	public int getErrorCode() {
		return errorCode;
	}
}
