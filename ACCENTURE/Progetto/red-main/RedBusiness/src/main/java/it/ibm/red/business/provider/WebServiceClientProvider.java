package it.ibm.red.business.provider;

import java.io.Serializable;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import com.siebel.customui.MEFSBLRDSCREATEFROMWSWF;
import com.siebel.customui.MEFSBLRDSCREATEFROMWSWF_Service;

import fepa.services.v3.InterfacciaFascicoli;
import fepa.services.v3.InterfacciaGENERIC;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembil.InterfacciaRaccoltaProvvisoriaDEMBIL;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembil.InterfacciaRaccoltaProvvisoriaDEMBIL_Service;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoAccessoFepa3;
import it.ibm.red.business.exception.WSClientException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.monitoring.attodecreto.AttoDecretoWSClientHandler;
import it.ibm.red.business.nps.BusinessDelegate;
import it.ibm.red.business.nps.NPSHeaderFactory;
import it.ibm.red.business.persistence.model.NpsConfiguration;
import it.ibm.red.business.service.facade.INpsFacadeSRV;
import it.ibm.red.business.utils.CollectionUtils;
import it.ibm.red.business.utils.StringUtils;
import net.protmef.ws.v1.IProtocolloMEFWS;
import net.protmef.ws.v1.ProtocolloMEF;

/**
 * The Class WebServiceClientProvider.
 *
 * @author a.dilegge
 * 
 *         Fornisce gli stub dei servizi web che RedEvo invoca e gli oggetti
 *         statici ad essi associati (ad esempio gli accessi).
 */
public final class WebServiceClientProvider implements Serializable {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(WebServiceClientProvider.class.getName());

	/**
	 * Messaggio errore costruzione accesso a Fepa3 Client.
	 */
	private static final String ERROR_COSTRUZIONE_ACCESSO_MSG = "Errore in fase di costruzione dell'accesso a Fepa3 Client";

	/**
	 * Istanza (singleton).
	 */
	private static WebServiceClientProvider provider;

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * Fepa client.
	 */
	private transient InterfacciaFascicoli fepa3Client;

	/**
	 * Fad client.
	 */
	private transient InterfacciaRaccoltaProvvisoriaDEMBIL fadClient;

	/**
	 * Accesso applicativo fad.
	 */
	private transient internal.demdec.v1.it.gov.mef.servizi.common.headeraccessoapplicativo.AccessoApplicativo accessoFad;

	/**
	 * Flag nps inizializzato.
	 */
	private boolean bdNPSInitialized;

	/**
	 * Client pmef.
	 */
	private transient IProtocolloMEFWS pmefClient;

	/**
	 * client Siebel.
	 */
	private transient MEFSBLRDSCREATEFROMWSWF siebelClient;

	/**
	 * Client http.
	 */
	private transient CloseableHttpClient closeableHttpClient;

	/**
	 * Flag DF init.
	 */
	private boolean bdDFInitialized;

	/**
	 * Service.
	 */
	private final INpsFacadeSRV npsSRV;

	/**
	 * Costruttore.
	 */
	private WebServiceClientProvider() {

		npsSRV = ApplicationContextProvider.getApplicationContext().getBean(INpsFacadeSRV.class);

		if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(getPP().getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
				&& "true".equals(getPP().getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
			LOGGER.warn("[WebServiceClientProvider] in regime mock non vengono creati gli stub verso i servizi esterni");
		} else {

			try {
				createInterfacciaFepa3();
			} catch (final WSClientException e) {
				LOGGER.error(e);
			}

			try {
				createInterfacciaRaccoltaProvvisoriaDEMBIL();
			} catch (final WSClientException e) {
				LOGGER.error(e);
			}

			try {
				createAccessoInterfacciaRaccoltaProvvisoriaDEMBIL();
			} catch (final WSClientException e) {
				LOGGER.error(e);
			}

			try {
				initializeBDNPS();
			} catch (final WSClientException e) {
				LOGGER.error(e);
			}

			try {
				createPmef();
			} catch (final WSClientException e) {
				LOGGER.error(e);
			}

			try {
				createSiebelClient();
			} catch (final WSClientException e) {
				LOGGER.error(e);
			}

			try {
				initializeBDDF();
			} catch (final WSClientException e) {
				LOGGER.error(e);
			}

		}

	}

	private PropertiesProvider getPP() {

		if (pp == null) {
			pp = PropertiesProvider.getIstance();
		}

		return pp;
	}

	private void createInterfacciaFepa3() {
		InterfacciaGENERIC serviceFepa3;
		try {

			serviceFepa3 = new InterfacciaGENERIC();
			fepa3Client = serviceFepa3.getInterfacciaGENERICSOAPPort();
			final BindingProvider bp = (BindingProvider) fepa3Client;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, getPP().getParameterByKey(PropertiesNameEnum.FEPA3_ENDPOINT));

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di costruzione del Fepa3 Client", e);
			throw new WSClientException("Errore in fase di costruzione del Fepa3 Client", e);
		}
	}

	private fepa.header.v3.AccessoApplicativo createAccessoInterfacciaFepa3(final TipoAccessoFepa3 tipo) {
		try {

			final fepa.header.v3.AccessoApplicativo accessoFepa3 = new fepa.header.v3.AccessoApplicativo();

			accessoFepa3.setApplicazione(getPP().getParameterByKey(tipo.getApplicazione()));
			accessoFepa3.setOrgID(getPP().getParameterByKey(tipo.getOrgID()));
			accessoFepa3.setUtente(getPP().getParameterByKey(tipo.getUtente()));
			accessoFepa3.setTipoUtente(getPP().getParameterByKey(tipo.getTipoUtente()));
			accessoFepa3.setClient(getPP().getParameterByKey(tipo.getClient()));
			accessoFepa3.setPassword(getPP().getParameterByKey(tipo.getPassword()));
			accessoFepa3.setServizio(getPP().getParameterByKey(tipo.getServizio()));

			return accessoFepa3;
		} catch (final Exception e) {
			LOGGER.error(ERROR_COSTRUZIONE_ACCESSO_MSG, e);
			throw new WSClientException(ERROR_COSTRUZIONE_ACCESSO_MSG, e);
		}
	}

	private fepa.header.v3.AccessoApplicativo createAccessoInterfacciaFepa3(final TipoAccessoFepa3 tipo, final String codiceAoo) {
		try {
			final fepa.header.v3.AccessoApplicativo accessoFepa3 = new fepa.header.v3.AccessoApplicativo();

			accessoFepa3.setApplicazione(getPP().getParameterByString(codiceAoo + "." + tipo.getApplicazione().getKey()));
			accessoFepa3.setOrgID(getPP().getParameterByString(codiceAoo + "." + tipo.getOrgID().getKey()));
			accessoFepa3.setUtente(getPP().getParameterByString(codiceAoo + "." + tipo.getUtente().getKey()));
			accessoFepa3.setTipoUtente(getPP().getParameterByString(codiceAoo + "." + tipo.getTipoUtente().getKey()));
			accessoFepa3.setClient(getPP().getParameterByString(codiceAoo + "." + tipo.getClient().getKey()));
			accessoFepa3.setPassword(getPP().getParameterByString(codiceAoo + "." + tipo.getPassword().getKey()));
			accessoFepa3.setServizio(getPP().getParameterByString(codiceAoo + "." + tipo.getServizio().getKey()));

			return accessoFepa3;
		} catch (final Exception e) {
			LOGGER.error(ERROR_COSTRUZIONE_ACCESSO_MSG, e);
			throw new WSClientException(ERROR_COSTRUZIONE_ACCESSO_MSG, e);
		}
	}

	@SuppressWarnings("rawtypes")
	private void createInterfacciaRaccoltaProvvisoriaDEMBIL() {

		InterfacciaRaccoltaProvvisoriaDEMBIL_Service service = null;

		try {

			service = new InterfacciaRaccoltaProvvisoriaDEMBIL_Service();
			fadClient = service.getInterfacciaRaccoltaProvvisoriaDEMBILSOAPPort();
			final BindingProvider bp = (BindingProvider) fadClient;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, getPP().getParameterByKey(PropertiesNameEnum.FAD_DEMBIL_TARGET_ENDPOINT));
			final List<Handler> handlerList = bp.getBinding().getHandlerChain();
			handlerList.add(new AttoDecretoWSClientHandler());
			bp.getBinding().setHandlerChain(handlerList);

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di costruzione del Raccolta Provvisoria DEMBIL Client", e);
			throw new WSClientException("Errore in fase di costruzione del Raccolta Provvisoria DEMBIL Client", e);
		}
	}

	private void createAccessoInterfacciaRaccoltaProvvisoriaDEMBIL() {
		try {

			accessoFad = new internal.demdec.v1.it.gov.mef.servizi.common.headeraccessoapplicativo.AccessoApplicativo();
			accessoFad.setApplicazione(getPP().getParameterByKey(PropertiesNameEnum.FAD_DEMBIL_APPLICATION));
			accessoFad.setOrgID(getPP().getParameterByKey(PropertiesNameEnum.FAD_DEMBIL_ORG_ID));
			accessoFad.setUtente(getPP().getParameterByKey(PropertiesNameEnum.FAD_DEMBIL_USER_ID));
			accessoFad.setTipoUtente(getPP().getParameterByKey(PropertiesNameEnum.FAD_DEMBIL_USER_TYPE));
			accessoFad.setClient(getPP().getParameterByKey(PropertiesNameEnum.FAD_DEMBIL_CLIENT_ID));
			accessoFad.setPassword(getPP().getParameterByKey(PropertiesNameEnum.FAD_DEMBIL_PASSWORD));
			accessoFad.setServizio(getPP().getParameterByKey(PropertiesNameEnum.FAD_DEMBIL_SERVICE));

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di costruzione dell'accesso a Raccolta Provvisoria DEMBIL Client", e);
			throw new WSClientException("Errore in fase di costruzione dell'accesso a Raccolta Provvisoria DEMBIL Client", e);
		}
	}

	private void initializeBDNPS() {

		try {
			this.bdNPSInitialized = false;

			String endPointProtocollo = getPP().getParameterByKey(PropertiesNameEnum.NPS_WS_PROTOCOLLO_ENDPOINT_NPSKEY);
			String endPointDocumentale = getPP().getParameterByKey(PropertiesNameEnum.NPS_WS_DOCUMENTALE_ENDPOINT_NPSKEY);
			String endPointRegistrazioneAusiliaria = getPP().getParameterByKey(PropertiesNameEnum.NPS_WS_REGISTRAZIONE_AUSILIARIA_ENDPOINT_NPSKEY);
			String endPointFlussoAUT = getPP().getParameterByKey(PropertiesNameEnum.NPS_WS_FLUSSO_AUT_ENDPOINT_NPSKEY);
			String endPointPosta = getPP().getParameterByKey(PropertiesNameEnum.NPS_WS_POSTA_ENDPOINT_NPSKEY);

			final String nameVarAmbienteNPS = getPP().getParameterByKey(PropertiesNameEnum.NPS_ENVIRONMENT_VAR_ENDPOINT_SVIL);
			final String valueVarAmbienteNPS = System.getenv(nameVarAmbienteNPS);
			if (!StringUtils.isNullOrEmpty(valueVarAmbienteNPS)) {
				if ("true".equals(valueVarAmbienteNPS)) {
					endPointProtocollo = getPP().getParameterByKey(PropertiesNameEnum.NPS_WS_PROTOCOLLO_ENDPOINT_NPSKEY_SVIL);
					endPointDocumentale = getPP().getParameterByKey(PropertiesNameEnum.NPS_WS_DOCUMENTALE_ENDPOINT_NPSKEY_SVIL);
					endPointRegistrazioneAusiliaria = getPP().getParameterByKey(PropertiesNameEnum.NPS_WS_REGISTRAZIONE_AUSILIARIA_ENDPOINT_NPSKEY_SVIL);
					endPointFlussoAUT = getPP().getParameterByKey(PropertiesNameEnum.NPS_WS_FLUSSO_AUT_ENDPOINT_NPSKEY_SVIL);
					endPointPosta = getPP().getParameterByKey(PropertiesNameEnum.NPS_WS_POSTA_ENDPOINT_NPSKEY_SVIL);
				} else {
					endPointProtocollo = getPP().getParameterByString(valueVarAmbienteNPS + "." 
							+ PropertiesNameEnum.NPS_WS_PROTOCOLLO_ENDPOINT_NPSKEY_SVIL.getKey());
					endPointDocumentale = getPP().getParameterByString(valueVarAmbienteNPS + "." 
							+ PropertiesNameEnum.NPS_WS_DOCUMENTALE_ENDPOINT_NPSKEY_SVIL.getKey());
					endPointRegistrazioneAusiliaria = getPP()
							.getParameterByString(valueVarAmbienteNPS + "." 
									+ PropertiesNameEnum.NPS_WS_REGISTRAZIONE_AUSILIARIA_ENDPOINT_NPSKEY_SVIL.getKey());
					endPointFlussoAUT = getPP().getParameterByString(valueVarAmbienteNPS + "." 
							+ PropertiesNameEnum.NPS_WS_FLUSSO_AUT_ENDPOINT_NPSKEY_SVIL.getKey());
					endPointPosta = getPP().getParameterByString(valueVarAmbienteNPS + "." 
							+ PropertiesNameEnum.NPS_WS_POSTA_ENDPOINT_NPSKEY_SVIL.getKey());
				}
			}

			final Map<String, NPSHeaderFactory> headersAooMap = retrieveHeaderFactoryMap();

			BusinessDelegate.init(endPointProtocollo, endPointDocumentale, endPointRegistrazioneAusiliaria, endPointFlussoAUT, endPointPosta, headersAooMap);
			this.bdNPSInitialized = true;
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di inizializzazione del Business Delegate NPS", e);
			throw new WSClientException("Errore in fase di inizializzazione del Business Delegate NPS", e);
		}
	}

	private Map<String, NPSHeaderFactory> retrieveHeaderFactoryMap() {
		final Map<String, NPSHeaderFactory> headersAooMap = new HashMap<>();

		try {
			final List<NpsConfiguration> confList = npsSRV.getAll();

			if (!CollectionUtils.isEmptyOrNull(confList)) {
				for (final NpsConfiguration conf : confList) {
					headersAooMap.put("" + conf.getIdAoo(),
							new NPSHeaderFactory(conf.getUtente(), conf.getClient(), conf.getPassword(), 
									conf.getServizio(), conf.getActor(), conf.getApplicazione()));
					
					headersAooMap.put(conf.getIdAoo()+"_Admin", new NPSHeaderFactory(conf.getUtenteAdmin(), conf.getClient(), conf.getPasswordAdmin(),
							conf.getServizioAdmin(), conf.getActor(), conf.getApplicazione()));
				}
			}

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di lettura configurazioni NPS", e);
			throw new WSClientException("Errore in fase di lettura configurazioni NPS", e);
		}

		return headersAooMap;
	}

	private void createPmef() {
		try {

			final ProtocolloMEF protocolloMefWS = new ProtocolloMEF(new URL(getPP().getParameterByKey(PropertiesNameEnum.TARGET_ENDPOINT_PMEFKEY)));
			pmefClient = protocolloMefWS.getBasicHttpBindingIProtocolloMEFWS();

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di costruzione del Protocollo MEF Client", e);
			throw new WSClientException("Errore in fase di costruzione del Protocollo MEF Client", e);
		}
	}

	private void createSiebelClient() {
		try {

			final MEFSBLRDSCREATEFROMWSWF_Service siebelClientService = new MEFSBLRDSCREATEFROMWSWF_Service();
			siebelClient = siebelClientService.getMEFSBLRDSCREATEFROMWSWF();
			final BindingProvider bp = (BindingProvider) siebelClient;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, getPP().getParameterByKey(PropertiesNameEnum.TARGET_ENDPOINT_SIEBELKEY));

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di costruzione del Siebel Client", e);
			throw new WSClientException("Errore in fase di costruzione del Siebel Client", e);
		}
	}

	private void createCloseableHttpClient() {
		int maxTot = 50;
		int maxPerRoute = 5;
		try {

			maxTot = Integer.parseInt(getPP().getParameterByKey(PropertiesNameEnum.HTTP_CLIENT_POOL_MAX_CONN_TOT));
			maxPerRoute = Integer.parseInt(getPP().getParameterByKey(PropertiesNameEnum.HTTP_CLIENT_POOL_MAX_CONN_PER_ROUTE));

		} catch (final Exception e) {
			LOGGER.error("Configurare le properties per HTTP_CLIENT_POOL", e);
		}

		try {

			final PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
			cm.setMaxTotal(maxTot);
			cm.setDefaultMaxPerRoute(maxPerRoute);

			closeableHttpClient = HttpClients.custom().setConnectionManager(cm).build();

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di costruzione del Client HTTP", e);
			throw new WSClientException("Errore in fase di costruzione del Client HTTP", e);
		}
	}

	/**
	 * Getter istanza WebServiceProvider.
	 * 
	 * @return istanza
	 */
	public static WebServiceClientProvider getIstance() {
		if (provider == null) {
			provider = new WebServiceClientProvider();
		}
		return provider;
	}

	/**
	 * Permette di resettare l'istanza del WebServiceClientProvider.
	 */
	public static void resetInstance() {
		provider = new WebServiceClientProvider();
	}

	/**
	 * Restituisce l'interfaccia Fepa 3 Client.
	 * 
	 * @return InterfacciaFascicoli
	 */
	public InterfacciaFascicoli getInterfacciaFepa3Client() {
		if (fepa3Client == null) {
			createInterfacciaFepa3();
		}
		return fepa3Client;
	}

	/**
	 * Crea l'accesso applicativo del tipo passato come parametro e lo restituisce.
	 * 
	 * @param tipo
	 * @return accesso interfaccia
	 */
	public fepa.header.v3.AccessoApplicativo getAccessoInterfacciaFepa3Client(final TipoAccessoFepa3 tipo) {
		return createAccessoInterfacciaFepa3(tipo);
	}

	/**
	 * Crea l'accesso applicativo del tipo passato come parametro e lo restituisce.
	 * 
	 * @param tipo      tipo accesso
	 * @param codiceAoo
	 * @return accesso interfaccia
	 */
	public fepa.header.v3.AccessoApplicativo getAccessoInterfacciaFepa3Client(final TipoAccessoFepa3 tipo, final String codiceAoo) {
		return createAccessoInterfacciaFepa3(tipo, codiceAoo);
	}

	/**
	 * Crea interfaccia raccolta provvisoria DEMBIL e restituisce
	 * {@link #fadClient}.
	 * 
	 * @return fadClient
	 */
	public InterfacciaRaccoltaProvvisoriaDEMBIL getInterfacciaRaccoltaProvvisoriaDEMBILClient() {
		if (fadClient == null) {
			createInterfacciaRaccoltaProvvisoriaDEMBIL();
		}
		return fadClient;
	}

	/**
	 * Crea l'accesso interfaccia raccolta provvisoria DEMBIL e restituisce
	 * {@link #fadClient}.
	 * 
	 * @return fadClient
	 */
	public internal.demdec.v1.it.gov.mef.servizi.common.headeraccessoapplicativo.AccessoApplicativo getAccessoInterfacciaRaccoltaProvvisoriaDEMBILClient() {
		if (accessoFad == null) {
			createAccessoInterfacciaRaccoltaProvvisoriaDEMBIL();
		}
		return accessoFad;
	}

	/**
	 * Inizializza BDNPS se non è gia inizializzato valutando
	 * {@link #bdNPSInitialized}.
	 */
	public void checkInitializationBdNPS() {
		if (!bdNPSInitialized) {
			initializeBDNPS();
		}
	}

	/**
	 * Crea Pmef se non è gia esistente valutando {@link #pmefClient}.
	 * 
	 * @return pmefClient
	 */
	public IProtocolloMEFWS getPmefClient() {
		if (pmefClient == null) {
			createPmef();
		}
		return pmefClient;
	}

	/**
	 * Crea SiebelClient se non è gia esistente valutando {@link #siebelClient}.
	 * 
	 * @return siebelClient
	 */
	public MEFSBLRDSCREATEFROMWSWF getSiebelClient() {
		if (siebelClient == null) {
			createSiebelClient();
		}
		return siebelClient;
	}

	/**
	 * Crea Closeable Http Client se non è gia esistente valutando
	 * {@link #closeableHttpClient}.
	 * 
	 * @return closeableHttpClient
	 */
	public CloseableHttpClient getCloseableHttpClient() {
		if (closeableHttpClient == null) {
			createCloseableHttpClient();
		}
		return closeableHttpClient;
	}

	private void initializeBDDF() {
		try {
			this.bdDFInitialized = false;
			final String endPointProtocollo = getPP().getParameterByKey(PropertiesNameEnum.NSD_RICERCA_PROTOCOLLI_ENDPOINT);
			final String endPointFascicolo = getPP().getParameterByKey(PropertiesNameEnum.NSD_RICERCA_FASCICOLI_ENDPOINT);
			final String endPointDocumento = getPP().getParameterByKey(PropertiesNameEnum.NSD_RICERCA_DOCUMENTI_ENDPOINT);
			it.ibm.red.business.df.BusinessDelegate.init(endPointProtocollo, endPointFascicolo, endPointDocumento);
			this.bdDFInitialized = true;
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di inizializzazione del Business Delegate Del DF", e);
			throw new WSClientException("Errore in fase di inizializzazione del Business Delegate DF", e);
		}
	}

	/**
	 * Inizializza BDDF se non è gia inzializzato valutando
	 * {@link #bdDFInitialized}.
	 */
	public void checkInitializationBdDF() {
		if (!bdDFInitialized) {
			initializeBDDF();
		}
	}

}