package it.ibm.red.business.enums;

/**
 * The Enum TipoAzioneEnum.
 *
 * @author CPIERASC
 * 
 *         Enum per le azioni rappresentate sul calendario.
 */
public enum ErroriMailEnum {	
	
	/**
	 * E' stato omesso il mittente nella mail.
	 */
	NOMITTENTE(1, "E' stato omesso il mittente nella mail."),
	
	/**
	 * L'Email del mittente non e' formattata correttamente.
	 */
	EMAILMITTENTEFORMAT(2, "L'Email del mittente non e' formattata correttamente."),
	
	/**
	 * Sono stati omessi i destinatari nella mail.
	 */
	NODESTINATARI(3, "Sono stati omessi i destinatari nella mail."),
	
	/**
	 * Una/delle Email dei destinatari non e'/sono formattata/e correttamente.
	 */
	EMAILDESTINATARIFORMAT(4, "Una/delle Email dei destinatari non e'/sono formattata/e correttamente."),
	
	/**
	 * Una/delle Email dei destinatariCC non e'/sono formattata/e correttamente.
	 */
	EMAILDESTINATARICCFORMAT(5, "Una/delle Email dei destinatariCC non e'/sono formattata/e correttamente."),
	
	/**
	 * E' stato omesso l'oggetto della mail.
	 */
	NOOGGETTOEMAIL(6, "E' stato omesso l'oggetto della mail."),
	
	/**
	 * Non e' stato definito l'oggetto tipologia messaggio.
	 */
	TIPOLOGIARANGE(7, "Non e' stato definito l'oggetto tipologia messaggio."),
	
	/**
	 * Non e' stato definito l'oggetto tipologia invio.
	 */
	NOMESSAGEID(8, "Non e' stato definito l'oggetto tipologia invio."),
	
	/**
	 * Il valore e' fuori dal Range previsto.
	 */
	NOOBJECTTIPOLOGIAINVIO(9, "Il valore e' fuori dal Range previsto."),
	
	/**
	 * Valorizzare il MessageID della email da eliminare.
	 */
	NOOBJECTTIPOLOGIAMESSAGGIO(10, "Valorizzare il MessageID della email da eliminare."),
	
	/**
	 * Non esiste alcuna mail associata al msgid specificato.
	 */
	NODOCUMENTPERMSGID(11, "Non esiste alcuna mail associata al msgid specificato."),
	
	/**
	 * Non esiste una casella di posta associata al mittente.
	 */
	NOCASELLAPOSTALEONFILENET(12, "Non esiste una casella di posta associata al mittente.");

	/**
	 * Valore.
	 */
	private Integer value;

	/**
	 * Descrizione.
	 */
	private String description;

	/**
	 * Costruttore.
	 * 
	 * @param inValue		valore
	 * @param inDescription	descrizione
	 */
	ErroriMailEnum(final int inValue, final String inDescription) {
		this.value = inValue;
		this.description = inDescription;
	}

	/**
	 * Getter descrizione.
	 * 
	 * @return	descrizione
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Getter valore.
	 * 
	 * @return	valore
	 */
	public Integer getValue() {
		return value;
	}

}
