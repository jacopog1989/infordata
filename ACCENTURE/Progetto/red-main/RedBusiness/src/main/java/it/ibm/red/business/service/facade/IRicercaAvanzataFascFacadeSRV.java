package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataFascDTO;
import it.ibm.red.business.dto.RicercaAvanzataFascicoliFormDTO;
import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * The Interface IRicercaAvanzataFascFacadeSRV.
 *
 * @author m.crescentini
 * 
 *         Facade del servizio per la gestione della ricerca avanzata dei fascicoli.
 */
public interface IRicercaAvanzataFascFacadeSRV extends Serializable {
	
	/**
	 * Inizializza la combo dalla ricerca.
	 * @param utente
	 * @param ricercaAvanzataForm
	 */
	void initComboFormRicerca(UtenteDTO utente, RicercaAvanzataFascicoliFormDTO ricercaAvanzataForm);
	
	/**
	 * Ottiene i tipi procedimento dal tipo documento.
	 * @param descrTipologiaDocumento
	 * @param idAOO
	 * @return lista dei tipi procedimento
	 */
	List<TipoProcedimentoDTO> getTipiProcedimentoByTipologiaDocumento(String descrTipologiaDocumento, Long idAOO);

	/**
	 * Esegue la ricerca dei fascicoli.
	 * @param paramsRicercaAvanzataFascicoli
	 * @param utente
	 * @param orderbyInQuery
	 * @return fascicoli
	 */
	Collection<FascicoloDTO> eseguiRicercaFascicoli(ParamsRicercaAvanzataFascDTO paramsRicercaAvanzataFascicoli,
			UtenteDTO utente, boolean orderbyInQuery);
	
	
}