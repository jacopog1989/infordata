package it.ibm.red.business.service.concrete;

import java.sql.Connection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IRuoloDAO;
import it.ibm.red.business.dto.AssegnatarioDTO;
import it.ibm.red.business.dto.EsitoDTO;
import it.ibm.red.business.dto.MappingOrgNsdDTO;
import it.ibm.red.business.enums.SignStrategyEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Ruolo;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.utils.PermessiUtils;

/**
 * Service per la gestione di un'AOO.
 */
@Service
@Component
public class AooSRV extends AbstractService implements IAooSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 5072215933806523575L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AooSRV.class.getName());

	/**
	 * DAO aoo.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * Ruolo dao.
	 */
	@Autowired
	private IRuoloDAO ruoloDAO;

	/**
	 * Nodo dao.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Consente il recupero dell'AOO identificata dall' <code> idAoo </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAooFacadeSRV#recuperaAoo(int)
	 */
	@Override
	public Aoo recuperaAoo(final int idAoo) {
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return recuperaAoo(idAoo, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dal DB dell'AOO: " + idAoo, e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}

		return null;
	}

	/**
	 * Consente il recupero dell'AOO identificata dall' <code> idAoo </code>.
	 * 
	 * @see it.ibm.red.business.service.IAooSRV#recuperaAoo(int,
	 *      java.sql.Connection)
	 */
	@Override
	public Aoo recuperaAoo(final int idAoo, final Connection connection) {
		return aooDAO.getAoo((long) idAoo, connection);
	}

	/**
	 * Recupera le informazioni sul ruolo delegato al libro firma per l'Area
	 * organizzativa identificata dall' <code> idAoo </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAooFacadeSRV#checkRuoloDelegatoLiboFirma(java.lang.Long)
	 */
	@Override
	public EsitoDTO checkRuoloDelegatoLiboFirma(final Long idAoo) {
		final EsitoDTO output = new EsitoDTO();
		Connection conn = null;
		boolean isRuoloConfigured = Boolean.FALSE;
		boolean hasRuoloPermesso = Boolean.FALSE;

		try {

			conn = setupConnection(getDataSource().getConnection(), false);

			final Aoo aoo = recuperaAoo(idAoo.intValue(), conn);

			if (aoo != null && aoo.getIdRuoloDelegatoLibroFirma() != 0) {
				isRuoloConfigured = Boolean.TRUE;

				final Ruolo ruolo = ruoloDAO.getRuolo(aoo.getIdRuoloDelegatoLibroFirma(), conn);

				if (PermessiUtils.hasLibroFirmaDelegato(ruolo.getPermessi().longValue())) {
					hasRuoloPermesso = Boolean.TRUE;
				}
			}

			if (isRuoloConfigured && hasRuoloPermesso) {
				output.setEsito(true);
				output.setNote(
						"Il Ruolo Configurato per Aoo con id: " + idAoo + " risulta correttamente configurata. ");
			} else if (!isRuoloConfigured) {
				output.setEsito(false);
				output.setNote("L AOO con id " + idAoo
						+ " non risulta correttamente configurata con il ruolo Delegato al Libro Firma. ");
			} else {
				output.setEsito(false);
				output.setNote(
						"Il Ruolo configurato per Aoo attualmente in uso non possiede i permessi di Delegato al Libro Firma. ");
			}

		} catch (final Exception e) {
			output.setEsito(false);
			output.setNote(
					"Errore durante la verfica della corretta configurazione del Ruolo di Delegato al Libro Firma. ");
			LOGGER.error(
					"Errore durante la verfica della corretta configurazione del Ruolo di Delegato al Libro Firma. ",
					e);
		} finally {
			closeConnection(conn);
		}

		return output;
	}

	/**
	 * Restituisce l'Area organizzativa identificata dal codice
	 * <code> codiceAoo </code>.
	 * 
	 * @see it.ibm.red.business.service.IAooSRV#recuperaAoo(java.lang.String,
	 *      java.sql.Connection)
	 */
	@Override
	public Aoo recuperaAoo(final String codiceAoo, final Connection conn) {
		return aooDAO.getAoo(codiceAoo, conn);
	}

	/**
	 * Restituisce l'Area organizzativa identificata dal codice
	 * <code> codiceAoo </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAooFacadeSRV#recuperaAoo(java.lang.String)
	 */
	@Override
	public Aoo recuperaAoo(final String codiceAoo) {
		Aoo aoo = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			aoo = aooDAO.getAoo(codiceAoo, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dal DB dell'AOO: " + codiceAoo, e);
		} finally {
			closeConnection(connection);
		}

		return aoo;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IAooFacadeSRV#checkSkipMotivazioneSigla(java.lang.Long).
	 */
	@Override
	public boolean checkSkipMotivazioneSigla(final Long idAoo) {
		boolean skipMotivazioneSigla = false;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			skipMotivazioneSigla = aooDAO.getFlagSkipMotivazioneSigla(idAoo, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero del flag di abilitazione per la motivazione dell'operazione di sigla.",
					e);
		} finally {
			closeConnection(connection);
		}

		return skipMotivazioneSigla;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IAooFacadeSRV#getMappingOrgNsd(java.lang.Long,
	 *      java.lang.Long, java.lang.Long).
	 */
	@Override
	public MappingOrgNsdDTO getMappingOrgNsd(final Long idUfficio, final Long idUtente, final Long idAoo) {
		final MappingOrgNsdDTO output = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			return aooDAO.getMappingOrgNsd(idUfficio, idUtente, idAoo, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero del mapping nsd.", e);
		} finally {
			closeConnection(connection);
		}
		return output;
	}

	/**
	 * Restituisce l'assegnatario di default associato all'Area Organizzativa
	 * Omogenea identificata dall' <code> idAoo </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAooFacadeSRV#recuperaAssegnatarioDiDefault(java.lang.Long)
	 */
	@Override
	public AssegnatarioDTO recuperaAssegnatarioDiDefault(final Long idAoo) {
		Connection connection = null;
		final AssegnatarioDTO assegnatario = new AssegnatarioDTO();
		Long idNodoUfficio;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			idNodoUfficio = aooDAO.getIdNodoUfficioDiDefault(idAoo, connection);
			final Nodo ufficio = nodoDAO.getNodo(idNodoUfficio, connection);

			if (ufficio != null) {
				assegnatario.setIdUfficio(ufficio.getIdNodo());
				assegnatario.setDescrizioneUfficio(ufficio.getDescrizione());
			}

		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dell'assegnatario di default", e);
			throw new RedException("Errore nel recupero dell'assegnatario di default", e);
		} finally {
			closeConnection(connection);
		}

		return assegnatario;
	}

	/**
	 * Restituisce la strategia di firma selezionata dall'Area Organizzativa
	 * identificata dall' <code> idAOO </code>.
	 * 
	 * @see it.ibm.red.business.service.IAooSRV#getSignStrategy(java.lang.Long)
	 */
	@Override
	public SignStrategyEnum getSignStrategy(Long idAOO) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return aooDAO.getSignStrategy(connection, idAOO);

		} catch (Exception e) {
			LOGGER.error("Errore nel recupero del tipo strategia dell'AOO: " + idAOO, e);
			throw new RedException("Errore nel recupero del tipo strategia dell'AOO: " + idAOO, e);
		} finally {
			closeConnection(connection);
		}
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IAooFacadeSRV#recuperaAooFromNPS(java.lang.String).
	 */
	@Override
	public Aoo recuperaAooFromNPS(final String codiceAooNPS) {
		Aoo aoo = null;
		Connection connection =  null;
		
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			
			aoo = aooDAO.getAooFromNPS(codiceAooNPS, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dal DB dell'AOO NPS: " + codiceAooNPS, e);
		} finally {
			closeConnection(connection);
		}
		
		return aoo;
	}
}