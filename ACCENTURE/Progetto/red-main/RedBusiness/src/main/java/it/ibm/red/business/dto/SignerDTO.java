package it.ibm.red.business.dto;

import java.util.Date;

import it.pkbox.client.DNParser;
import it.pkbox.client.Envelope;
import it.pkbox.client.Signer;
import it.pkbox.client.SignerEx;
import it.pkbox.client.XMLEnvelope;

/**
 * The Class SignerDTO.
 *
 * @author CPIERASC
 * 
 * 	Classe utilizzata per descrivere il firmatario di una firma.
 */
public class SignerDTO extends AbstractDTO {
	
	/**
	 * Definisce la tipologia della firma.
	 */
	public enum TipoFirma {

		/**
		 * Valore.
		 */
		PKCS7_FORMAT(Envelope.Pkcs7_Format),

		/**
		 * Valore.
		 */
		CADES_BES_FORMAT(Envelope.CAdES_BES_Format),

		/**
		 * Valore.
		 */
		CADES_T_FORMAT(Envelope.CAdES_T_Format),

		/**
		 * Valore.
		 */
		PDF_FORMAT(Envelope.PAdES_Basic_Format),

		/**
		 * Valore.
		 */
		PADES_BES_FORMAT(Envelope.PAdES_BES_Format),

		/**
		 * Valore.
		 */
		PADES_T_FORMAT(Envelope.PAdES_T_Format),

		/**
		 * Valore.
		 */
		XMLDSIG_FORMAT(XMLEnvelope.XMLDSIG_Format),

		/**
		 * Valore.
		 */
		XADES_BES_FORMAT(XMLEnvelope.XAdES_BES_Format),

		/**
		 * Valore.
		 */
		XADES_T_FORMAT(XMLEnvelope.XAdES_T_Format);
		

		/**
		 * Valore.
		 */
		private Integer value;
		
		/**
		 * Costruttore dell'enum Tipo Firma.
		 * @param value
		 */
		TipoFirma(final Integer value) {
			this.value = value;
		}
	
		/**
		 * Restituisce il value dell'enum associata al tipo firma.
		 * @return value enum tipo firma
		 */
		public Integer getValue() {
			return value;
		}

		/**
		 * Imposta il value dell'enum associata alla tipologia della firma.
		 * @param value
		 */
		protected void setValue(final Integer value) {
			this.value = value;
		}

		/**
		 * Restituisce il tipo firma come enum associato al codice.
		 * @param signatureFormat
		 * @return enum associato al codice
		 */
		public static TipoFirma getTipoFirmaByCode(final Integer signatureFormat) {
			for (TipoFirma tf: TipoFirma.values()) {
				if (tf.getValue().equals(signatureFormat)) {
					return tf;
				}
			}
			return null;
		}
	}

	/**
	 * Definisce lo stato del certificato.
	 */
	public enum StatoCertificato {

		/**
		 * Valore.
		 */
		CERT_EXPIRED(Signer.CERT_EXPIRED),

		/**
		 * Valore.
		 */
		CERT_INVALID(Signer.CERT_INVALID),

		/**
		 * Valore.
		 */
		CERT_NOT_YET_VALID(Signer.CERT_NOT_YET_VALID),

		/**
		 * Valore.
		 */
		CERT_REVOKED(Signer.CERT_REVOKED),

		/**
		 * Valore.
		 */
		CERT_SELF_SIGNED(Signer.CERT_SELF_SIGNED),

		/**
		 * Valore.
		 */
		CERT_SIGN_HASH_ALG_INVALID(Signer.CERT_SIGN_HASH_ALG_INVALID),

		/**
		 * Valore.
		 */
		CERT_SUSPENDED(Signer.CERT_SUSPENDED),

		/**
		 * Valore.
		 */
		CERT_UNKNOWN(Signer.CERT_UNKNOWN),

		/**
		 * Valore.
		 */
		CERT_UNTRUSTED(Signer.CERT_UNTRUSTED),

		/**
		 * Valore.
		 */
		CERT_VALID(Signer.CERT_VALID);
		

		/**
		 * Valore.
		 */
		private Integer value;
		
		/**
		 * Costruttore.
		 * @param value
		 */
		StatoCertificato(final Integer value) {
			this.value = value;
		}
	
		/**
		 * Restituisce il value dell'enum.
		 * @return value
		 */
		public Integer getValue() {
			return value;
		}

		/**
		 * Imposta il value dell'enum.
		 * @param value
		 */
		protected void setValue(final Integer value) {
			this.value = value;
		}

		/**
		 * Restituisce lo stato del certificato associato al codice.
		 * @param certificateStatus
		 * @return enum associato al codice
		 */
		public static StatoCertificato getStatoCertificatoByCode(final Integer certificateStatus) {
			for (StatoCertificato sc: StatoCertificato.values()) {
				if (sc.getValue().equals(certificateStatus)) {
					return sc;
				}
			}
			return null;
		}
	}

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Firmatario.
	 */
	private String firmatario;
	/**
	 * Certificate Authority.
	 */
	private String informazioniCA;
	/**
	 * Unità organizzativa.
	 */
	private String unitaOrganizzativa;

	/**
	 * Stato del certificato.
	 */
	private StatoCertificato statoCertificato;

	/**
	 * Data firma.
	 */
	private Date dataFirma;
	/**
	 * Data inizio validità firmatario.
	 */
	private Date dataInizioValidita;
	/**
	 * Data fine validità firmatario.
	 */
	private Date dataFineValidita;
	/**
	 * Motivo firma.
	 */
	private String motivo;
	
	/**
	 * Validità della firma.
	 */
	private boolean valid;
	
	/**
	 * Codice della tipologia di firma.
	 */
	private TipoFirma tipoFirma;
	
	/**
	 * Dettaglio completo del firmatario.
	 */
	private String dettaglioFirmatario;
	

	/**
	 * Costruttore.
	 * 
	 * @param signer	signer PK
	 */
	public SignerDTO(final SignerEx signer) {
		DNParser dnParser = new DNParser(signer.getSubjectDN());
		DNParser dnParserCA = new DNParser(signer.getIssuerDN());
		
		dettaglioFirmatario = signer.getSubjectDN();
		firmatario = dnParser.get("CN");
		informazioniCA = dnParserCA.get("CN");
		unitaOrganizzativa = dnParser.get("O");
		statoCertificato = StatoCertificato.getStatoCertificatoByCode(signer.getCertificateStatus());
		dataFirma = signer.getSigningTime();
		dataInizioValidita = signer.getStartDate();
		dataFineValidita = signer.getEndDate();
		motivo = signer.getReason();
		tipoFirma = TipoFirma.getTipoFirmaByCode(signer.getSignatureFormat());
		
		// Al momento si è scelto di indicare come non valido tutto quello che differisce da COMPLIANCE_CODE_GOOD
		valid = signer.getSignatureCompliance() == SignerEx.COMPLIANCE_CODE_GOOD;
	}
	
	/**
	 * Getter firmatario.
	 * 
	 * @return	firmatario
	 */
	public final String getFirmatario() {
		return firmatario;
	}

	/**
	 * Getter info CA.
	 * 
	 * @return	informazioni ca
	 */
	public final String getInformazioniCA() {
		return informazioniCA;
	}

	/**
	 * Getter unità organizzativa.
	 * 
	 * @return	unità organizzativa
	 */
	public final String getUnitaOrganizzativa() {
		return unitaOrganizzativa;
	}

	/**
	 * Getter codice stato certificato.
	 * 
	 * @return	codice stato certificato
	 */
	public final StatoCertificato getStatoCertificato() {
		return statoCertificato;
	}

	/**
	 * Getter data firma.
	 * 
	 * @return	data firma
	 */
	public final Date getDataFirma() {
		return dataFirma;
	}

	/**
	 * Getter data inizio validità.
	 * 
	 * @return	data inizio validità
	 */
	public final Date getDataInizioValidita() {
		return dataInizioValidita;
	}

	/**
	 * Getter data fine validità.
	 * 
	 * @return	fine validità
	 */
	public final Date getDataFineValidita() {
		return dataFineValidita;
	}

	/**
	 * Getter motivo.
	 * 
	 * @return	motivo
	 */
	public final String getMotivo() {
		return motivo;
	}
	
	/**
	 * Getter valid.
	 * 
	 * @return valid
	 */
	public boolean isValid() {
		return valid;
	}
	
	/**
	 * Getter tipoFirma.
	 * 
	 * @return tipoFirma
	 */
	public TipoFirma getTipoFirma() {
		
		return tipoFirma;
		
	}

	/**
	 * @return the dettaglioFirmatario
	 */
	public String getDettaglioFirmatario() {
		return dettaglioFirmatario;
	}
}