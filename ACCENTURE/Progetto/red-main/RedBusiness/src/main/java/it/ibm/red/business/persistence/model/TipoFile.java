package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * The Class TipoFile.
 *
 * @author adilegge
 * 
 * DTO tipo procedimento.
 */
public class TipoFile implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * idTipoFile.
	 */
	private Integer idTipoFile;
	
	/**
	 * estensione.
	 */
	private String estensione;
	
	/**
	 * mimeType.
	 */
	private String mimeType;
	
	/**
	 * Flag copiaConforme.
	 */
	private Integer flagCopiaConforme;
	
	/**
	 * Flag generico uscita.
	 */
	private Integer flagEventoAllegato;
	
	/**
	 * Flag convertibile.
	 */
	private Integer flagConvertibile;
	
	/**
	 * Costruttore.
	 * @param idTipoFile
	 * @param estensione
	 * @param mimeType
	 * @param flagCopiaConforme
	 * @param flagEventoAllegato
	 */
	public TipoFile(final Integer idTipoFile, final String estensione, final String mimeType, final Integer flagCopiaConforme,
			final Integer flagEventoAllegato) {
		super();
		this.idTipoFile = idTipoFile;
		this.estensione = estensione;
		this.mimeType = mimeType;
		this.flagCopiaConforme = flagCopiaConforme;
		this.flagEventoAllegato = flagEventoAllegato;
	}

	/**
	 * Costruttore di default.
	 * @param idTipoFile
	 * @param estensione
	 * @param mimeType
	 * @param flagCopiaConforme
	 * @param flagEventoAllegato
	 * @param flagConvertibile
	 */
	public TipoFile(final Integer idTipoFile, final String estensione, final String mimeType, final Integer flagCopiaConforme,
			final Integer flagEventoAllegato, final Integer flagConvertibile) {
		super();
		this.idTipoFile = idTipoFile;
		this.estensione = estensione;
		this.mimeType = mimeType;
		this.flagCopiaConforme = flagCopiaConforme;
		this.flagEventoAllegato = flagEventoAllegato;
		this.flagConvertibile = flagConvertibile;
	}
	
	/**
	 * Copy constructor.
	 * 
	 * @param aTipoFile
	 */
	public TipoFile(final TipoFile aTipoFile) {
		super();
		this.idTipoFile = aTipoFile.getIdTipoFile();
		this.estensione = aTipoFile.getEstensione();
		this.mimeType = aTipoFile.getMimeType();
		this.flagCopiaConforme = aTipoFile.getFlagCopiaConforme();
		this.flagEventoAllegato = aTipoFile.getFlagEventoAllegato();
		this.flagConvertibile = aTipoFile.getFlagConvertibile();
	}
	
	/**
	 * @return idTipoFile
	 */
	public Integer getIdTipoFile() {
		return idTipoFile;
	}

	/**
	 * Imposta l'id del tipo di file.
	 * @param idTipoFile
	 */
	public void setIdTipoFile(final Integer idTipoFile) {
		this.idTipoFile = idTipoFile;
	}

	/**
	 * @return estensione
	 */
	public String getEstensione() {
		return estensione;
	}

	/**
	 * Imposta l'estensione.
	 * @param estensione
	 */
	public void setEstensione(final String estensione) {
		this.estensione = estensione;
	}

	/**
	 * @return mimeType
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * Imposta il mimeType.
	 * @param mimeType
	 */
	public void setMimeType(final String mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 * Restituisce il flag {@link #flagCopiaConforme}.
	 * @return flagCopiaConforme
	 */
	public Integer getFlagCopiaConforme() {
		return flagCopiaConforme;
	}

	/**
	 * Imposta il flag {@link #flagCopiaConforme}.
	 * @param flagCopiaConforme
	 */
	public void setFlagCopiaConforme(final Integer flagCopiaConforme) {
		this.flagCopiaConforme = flagCopiaConforme;
	}

	/**
	 * Restituisce il flag {@link #flagEventoAllegato}.
	 * @return flagEventoAllegato
	 */
	public Integer getFlagEventoAllegato() {
		return flagEventoAllegato;
	}

	/**
	 * Imposta il flag {@link #flagEventoAllegato}.
	 * @param flagEventoAllegato
	 */
	public void setFlagEventoAllegato(final Integer flagEventoAllegato) {
		this.flagEventoAllegato = flagEventoAllegato;
	}
	
	/**
	 * Restituisce il flag {@link #flagConvertibile}.
	 * @return flagConvertibile
	 */
	public Integer getFlagConvertibile() {
		return flagConvertibile;
	}
	
	/**
	 * Imposta il flag {@link #flagConvertibile}.
	 * @param flagConvertibile
	 */
	public void setFlagConvertibile(final Integer flagConvertibile) {
		this.flagConvertibile = flagConvertibile;
	}

	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		if (idTipoFile != null) {
			return idTipoFile.hashCode();
		} else {
			result = prime * result + ((estensione == null) ? 0 : estensione.hashCode());
			result = prime * result + ((mimeType == null) ? 0 : mimeType.hashCode());
		}
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final TipoFile other = (TipoFile) obj;
		if (idTipoFile != null && other.idTipoFile != null) {
			return idTipoFile.equals(other.idTipoFile);
		}
		if (estensione == null) {
			if (other.estensione != null) {
				return false;
			}
		} else if (!estensione.equals(other.estensione)) {
			return false;
		}
		if (mimeType == null) {
			if (other.mimeType != null) {
				return false;
			}
		} else if (!mimeType.equals(other.mimeType)) {
			return false;
		}
		return true;
	}
}
