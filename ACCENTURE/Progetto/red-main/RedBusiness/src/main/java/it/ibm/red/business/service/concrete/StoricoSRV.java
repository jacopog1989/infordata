package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dao.IApprovazioneDAO;
import it.ibm.red.business.dao.IIterApprovativoDAO;
import it.ibm.red.business.dao.IStoricoDAO;
import it.ibm.red.business.dto.ApprovazioneDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.filenet.EventLogDTO;
import it.ibm.red.business.dto.filenet.StepDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.StepTypeEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.DEventiCustom;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IDocumentoSRV;
import it.ibm.red.business.service.IStoricoSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class StoricoSRV.
 *
 * @author CPIERASC
 * 
 *         Servizio gestione storico.
 */
@Service
@Component
public class StoricoSRV extends AbstractService implements IStoricoSRV {

	/**
	 * Step da siglare ispettore.
	 */
	private static final String DA_SIGLARE_ISPETTORE = "DA SIGLARE ISPETTORE";

	/**
	 * Step da firmare.
	 */
	private static final String DA_FIRMARE = "DA FIRMARE";

	/**
	 * Step da siglare dirigente.
	 */
	private static final String DA_SIGLARE_DIRIGENTE = "DA SIGLARE DIRIGENTE";

	/**
	 * Step da siglare dirigente.
	 */
	private static final String DA_SIGLARE_DIRETTORE = "DA SIGLARE DIRETTORE";

	/**
	 * Step creazione documento.
	 */
	private static final String CREAZIONE_DOCUMENTO = "CREAZIONE DOCUMENTO";

	/**
	 * Step errore recupero storico.
	 */
	private static final String ERRORE_RECUPERO_STORICO = "Errore nel recupero dello storico: ";

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Storico.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(StoricoSRV.class.getName());

	/**
	 * Dao per la gestione dello storico.
	 */
	@Autowired
	private IStoricoDAO storicoDAO;

	/**
	 * Dao per la gestione degli iter approvativi.
	 */
	@Autowired
	private IIterApprovativoDAO iterApprovativoDAO;

	/**
	 * Dao per la gestione delle approvazioni.
	 */
	@Autowired
	private IApprovazioneDAO approvazioniDAO;

	/**
	 * Servizio gestione documenti.
	 */
	@Autowired
	private IDocumentoSRV documentoSRV;

	/**
	 * Storico testuale.
	 *
	 * @param idDocumento the id documento
	 * @param idAoo       the id aoo
	 * @return the storico
	 */
	@Override
	public final Collection<StoricoDTO> getStorico(final int idDocumento, final Integer idAoo) {
		Connection filenetConnection = null;
		Connection connection = null;
		try {
			filenetConnection = setupConnection(getFilenetDataSource().getConnection(), false);
			connection = setupConnection(getDataSource().getConnection(), false);
			return storicoDAO.getStorico(idDocumento, idAoo, connection, filenetConnection);
		} catch (final Exception e) {
			LOGGER.error(ERRORE_RECUPERO_STORICO, e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
			closeConnection(filenetConnection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IStoricoSRV#getStoricoDocumento(java.lang.Integer,
	 *      java.lang.Integer).
	 */
	@Override
	public List<DEventiCustom> getStoricoDocumento(final Integer idDocumento, final Integer idAoo) {
		Connection filenetConnection = null;
		try {
			filenetConnection = setupConnection(getFilenetDataSource().getConnection(), false);
			return storicoDAO.getStoricoDocumento(idDocumento, idAoo, filenetConnection);
		} catch (final Exception e) {
			LOGGER.error(ERRORE_RECUPERO_STORICO, e);
			throw new RedException(e);
		} finally {
			closeConnection(filenetConnection);
		}
	}


	/**
	 * Recupera tutti i master ed i dettagli in maniera EAGER.
	 * 
	 * @param documentTitle
	 *            identificativo documento
	 * @param idAoo
	 *            identificativo aoo
	 * @return lista step
	 */
	private List<StepDTO> getStepsStorico(final String documentTitle, final Long idAoo) {
		Connection filenetConnection = null;
		try {
			final List<StepDTO> listaSteps = new ArrayList<>();

			filenetConnection = setupConnection(getFilenetDataSource().getConnection(), false);

			final Collection<EventLogDTO> eventLogList = storicoDAO.getStoricoEventAcc(filenetConnection, documentTitle, idAoo);

			for (final EventLogDTO eventLog : eventLogList) {
				final StepDTO step = new StepDTO(eventLog);
				if (eventLog.getTipo() != null && !"R".equalsIgnoreCase(eventLog.getTipo())) {
					// EventLog FIGLI
					final Collection<EventLogDTO> bodyEventList = storicoDAO.getStoricoEventAccDett(filenetConnection, documentTitle, idAoo, eventLog.getWorkFlowNumber());
					step.setBody(bodyEventList);
				}
				listaSteps.add(step);
			}
			return listaSteps;
		} catch (final Exception e) {
			LOGGER.error(ERRORE_RECUPERO_STORICO, e);
			throw new RedException(e);
		} finally {
			closeConnection(filenetConnection);
		}
	}

	/**
	 * Metodo per aggiungere alla lista di step fornita in input un elemento in più.
	 * 
	 * @param stepsList
	 *            lista degli step
	 * @param stepType
	 *            tipo step
	 * @param descEventLog
	 *            descrizione evento
	 * @param eventTypes
	 *            lista tipi eventi
	 */
	private static void createStepDTO(final List<StepDTO> stepsList, final StepTypeEnum stepType, final String descEventLog, final EventTypeEnum... eventTypes) {
		final StepDTO step = new StepDTO(stepType, new EventLogDTO(descEventLog, eventTypes));
		stepsList.add(step);
	}

	/**
	 * Recupera la lista di EventLog dell'iter automatico.
	 * 
	 * @param idIter
	 *            identificativo iter
	 * @return lista event log
	 */
	private static List<StepDTO> getIterAutomatico(final IterApprovativoDTO iter) {
		final List<StepDTO> output = new ArrayList<>();

		if (iter.isIterAutomaticoFirmaDirigenti()) {
			createStepDTO(output, StepTypeEnum.DA_COMPLETARE, CREAZIONE_DOCUMENTO, EventTypeEnum.DOCUMENTO);
			createStepDTO(output, StepTypeEnum.DA_COMPLETARE, DA_FIRMARE, EventTypeEnum.FIRMATO, EventTypeEnum.ASSEGNAZIONE_SPEDIZIONE);
		} else if (iter.isIterAutomaticoFirmaDirettoriIspettori()) {
			createStepDTO(output, StepTypeEnum.DA_COMPLETARE, CREAZIONE_DOCUMENTO, EventTypeEnum.DOCUMENTO);
			createStepDTO(output, StepTypeEnum.DA_COMPLETARE, DA_SIGLARE_DIRIGENTE, EventTypeEnum.SIGLATO);
			createStepDTO(output, StepTypeEnum.DA_COMPLETARE, DA_FIRMARE, EventTypeEnum.FIRMATO, EventTypeEnum.ASSEGNAZIONE_SPEDIZIONE);
		} else if (iter.isIterAutomaticoFirmaRagioniere()) {
			createStepDTO(output, StepTypeEnum.DA_COMPLETARE, CREAZIONE_DOCUMENTO, EventTypeEnum.DOCUMENTO);
			createStepDTO(output, StepTypeEnum.DA_COMPLETARE, DA_SIGLARE_DIRIGENTE, EventTypeEnum.SIGLATO);
			createStepDTO(output, StepTypeEnum.DA_COMPLETARE, DA_SIGLARE_ISPETTORE, EventTypeEnum.SIGLATO);
			createStepDTO(output, StepTypeEnum.DA_COMPLETARE, DA_FIRMARE, EventTypeEnum.FIRMATO, EventTypeEnum.FIRMATO_SPEDITO, EventTypeEnum.FIRMA_AUTOGRAFA,
					EventTypeEnum.ASSEGNAZIONE_SPEDIZIONE);
		} else if (iter.isIterAutomaticoFirmaCapoDipartimento()) {
			createStepDTO(output, StepTypeEnum.DA_COMPLETARE, CREAZIONE_DOCUMENTO, EventTypeEnum.DOCUMENTO);
			createStepDTO(output, StepTypeEnum.DA_COMPLETARE, DA_SIGLARE_DIRIGENTE, EventTypeEnum.SIGLATO);
			createStepDTO(output, StepTypeEnum.DA_COMPLETARE, DA_SIGLARE_DIRETTORE, EventTypeEnum.SIGLATO);
			createStepDTO(output, StepTypeEnum.DA_COMPLETARE, DA_FIRMARE, EventTypeEnum.FIRMATO, EventTypeEnum.FIRMATO_SPEDITO, EventTypeEnum.FIRMA_AUTOGRAFA,
					EventTypeEnum.ASSEGNAZIONE_SPEDIZIONE);
		}
		return output;
	}

	/**
	 * Arricchisce listaSteps con gli eventuali Step mancanti.
	 * 
	 * @param idIter
	 *            identificativo iter
	 * @param listaSteps
	 *            lista degli step
	 */
	private static void completaIterAutomatico(final IterApprovativoDTO tipoFirma, final List<StepDTO> listaSteps) {
		final List<StepDTO> iterAutomatico = getIterAutomatico(tipoFirma);
		final List<StepDTO> iterAutomaticoDaAggiungere = new ArrayList<>();

		for (final StepDTO stepAutomatico : iterAutomatico) {
			final Collection<String> eventTypeLisAutomatico = stepAutomatico.getHeader().getEventTypeList();
			boolean find = false;
			for (final StepDTO step : listaSteps) {
				if (!step.getHeader().isEventTypeCheched()) {
					for (final String eventTypeStringAutomatico : eventTypeLisAutomatico) {
						if (eventTypeStringAutomatico.equalsIgnoreCase(step.getHeader().getEventType())) {
							find = true;
							step.getHeader().setEventTypeCheched(find);
							break;
						}
					}
				}
				if (find) {
					break;
				}
			}
			if (!find) {
				iterAutomaticoDaAggiungere.addAll(iterAutomatico.subList(iterAutomatico.indexOf(stepAutomatico), iterAutomatico.size()));
				break;
			}
		}
		listaSteps.addAll(iterAutomaticoDaAggiungere);
	}

	@Override
	public final List<StepDTO> getModelloStoricoVisuale(FilenetCredentialsDTO dto, final String wobNumber, final String documentTitle, final Long idAoo,
			final Date dataCreazione, final boolean isRicercaRiservato) {
		if (isRicercaRiservato) {
			dto = new FilenetCredentialsDTO(dto);
		}
		return getModelloStoricoVisuale(dto, wobNumber, documentTitle, idAoo, dataCreazione);
	}

	/**
	 * Gets the modello storico visuale.
	 *
	 * @param dto
	 *            the dto
	 * @param wobNumber
	 *            the wob number
	 * @param documentTitle
	 *            the document title
	 * @param idAoo
	 *            the id aoo
	 * @return the modello storico visuale
	 */
	@Override
	public final List<StepDTO> getModelloStoricoVisuale(final FilenetCredentialsDTO dto, final String wobNumber, final String documentTitle, final Long idAoo,
			final Date dataCreazione) {
		List<StepDTO> stepStorico = null;

		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			stepStorico = getStepsStorico(documentTitle, idAoo);

			final String iterApp = getIterApp(dto, wobNumber, documentTitle, idAoo);

			if (!StringUtils.isNullOrEmpty(iterApp) && !"0".equals(iterApp)) {

				final IterApprovativoDTO iterDTO = iterApprovativoDAO.getIterApprovativoById(Integer.parseInt(iterApp), connection);

				if (iterDTO != null) {
					completaIterAutomatico(iterDTO, stepStorico);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(ERRORE_RECUPERO_STORICO, e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return stepStorico;
	}

	/**
	 * Metodo per il recupero dell'identificativo dell'iter approvativo di un
	 * documento.
	 * 
	 * @param dto
	 *            credenziali filenet
	 * @param wobNumber
	 *            wob number workflow
	 * @param documentTitle
	 *            document title documento
	 * @return identificativo iter approvativo
	 */
	private String getIterApp(final FilenetCredentialsDTO dto, final String wobNumber, final String documentTitle, final Long idAoo) {
		String output = "0";
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(dto, idAoo);

			final Document document = fceh.getDocumentForStoricoVisuale(documentTitle);
			// recupero e trasformazione dei metadati per il test di un documento in uscita
			final Integer tipoProtocollo = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY);
			final Integer momentoProtocollazione = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.MOMENTO_PROTOCOLLAZIONE_ID_METAKEY);
			final String iterApprovativoSemaforo = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.ITER_APPROVATIVO_SEMAFORO_METAKEY);

			if (!StringUtils.isNullOrEmpty(wobNumber) && Boolean.TRUE.equals(documentoSRV.isUscita(tipoProtocollo, momentoProtocollazione))) {
				fpeh = new FilenetPEHelper(dto);

				final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, false);
				final Integer flagManuale = (Integer) TrasformerPE.getMetadato(wob,
						PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FLAG_ITER_MANUALE_WF_METAKEY));
				if ((flagManuale != null && flagManuale != 1) || (flagManuale == null && !"0".equals(iterApprovativoSemaforo))) {
					// AUTOMATICO
					output = iterApprovativoSemaforo;
				}
			}
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}

		return output;
	}

	/**
	 * get modello approvazione visuale.
	 *
	 * @param idDocumento
	 *            the id documento
	 * @param idAoo
	 *            the id aoo
	 * @return the modello approvazioni visuale
	 */
	@Override
	public final List<ApprovazioneDTO> getModelloApprovazioniVisuale(final Integer idDocumento, final Integer idAoo) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			// Recupero delle approvazioni ordinate per data
			return new ArrayList<>(approvazioniDAO.getApprovazioniByIdDocumento(idDocumento, idAoo, connection));
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero delle approvazioni: ", e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IStoricoSRV#isDocLavoratoDaUfficio(java.lang.Long,
	 *      java.lang.String, java.lang.Long).
	 */
	@Override
	public boolean isDocLavoratoDaUfficio(final Long idAoo, final String documentTitle, final Long idUfficio) {
		Connection filenetConnection = null;
		try {
			filenetConnection = setupConnection(getFilenetDataSource().getConnection(), false);
			return storicoDAO.isDocLavoratoDaUfficio(idAoo, documentTitle, idUfficio, filenetConnection);
		} catch (final Exception ex) {
			LOGGER.error("Errore count notifica post rifiuto firma " + ex);
			throw new RedException(ex);
		} finally {
			closeConnection(filenetConnection);
		}
	}
}
