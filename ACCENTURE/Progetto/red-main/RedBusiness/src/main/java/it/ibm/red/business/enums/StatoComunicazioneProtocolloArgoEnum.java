package it.ibm.red.business.enums;

/**
 * @author m.crescentini
 *
 */
public enum StatoComunicazioneProtocolloArgoEnum {

	/**
	 * Valore.
	 */
	DA_ELABORARE(1),
	
	/**
	 * Valore.
	 */
	IN_ELABORAZIONE(2),
	
	/**
	 * Valore.
	 */
	ELABORATA(3),
	
	/**
	 * Valore.
	 */
	IN_ERRORE(4);
	
	/**
	 * Identificativo.
	 */
	private Integer id;
	
	/**
	 * Costruttore.
	 * @param id
	 */
	StatoComunicazioneProtocolloArgoEnum(final Integer id) {
		this.id = id;
	}

	/**
	 * Restituisce l'id.
	 * @return id
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Restituisce l'enum associata al codice in ingresso.
	 * @param codice
	 * @return enum associata al codice
	 */
	public static StatoComunicazioneProtocolloArgoEnum get(final Integer codice) {
		StatoComunicazioneProtocolloArgoEnum output = null;
		
		for (final StatoComunicazioneProtocolloArgoEnum stato : StatoComunicazioneProtocolloArgoEnum.values()) {
			if (stato.getId().equals(codice)) {
				output = stato;
				break;
			}
		}
		
		return output;
	}
}
