package it.ibm.red.business.service.flusso.facade;

import it.ibm.red.business.service.flusso.IAzioniFlussoBaseSRV;

/**
 * Interfaccia del servizio azioni flusso ARGO - Legge Pinto.
 */
public interface IAzioniFlussoArgoLeggePintoFacadeSRV extends IAzioniFlussoBaseSRV {

}
