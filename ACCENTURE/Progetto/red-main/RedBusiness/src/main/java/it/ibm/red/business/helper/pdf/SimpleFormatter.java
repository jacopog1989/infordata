package it.ibm.red.business.helper.pdf;

/**
 * Formatter che si limita ad eseguire il to string dell'oggetto fornito in input.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public class SimpleFormatter implements Formatter {

	
	/**
	 * Format.
	 *
	 * @param obj
	 *            the obj
	 * @return the string
	 */
	@Override
	public final String format(final Object obj) {
		return obj.toString();
	}
	
}