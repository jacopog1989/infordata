package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoApprovazioneEnum;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;

/**
 * The Interface IVistiFacadeSRV.
 *
 * @author a.dilegge
 * 
 *         Facade del servizio di gestione dei visti.
 */
public interface IVistiFacadeSRV extends Serializable {
	
	/**
	 * Metodo che chiude tutti i workflow che insistono su <code>idDocumento</code>, diversi da <code>f_WorkflowNumber</code>
	 * il cui ufficio di segreteria coincide con <code>idUfficio</code>.<br/>
	 * Lo scenario è quello della chiusura delle richieste di visto attive
	 * per uffici di un ispettorato che ha vistato prima della loro risposta.
	 * 
	 * @param idDocumento
	 * @param f_WorkflowNumber
	 * @param idUfficio
	 * 
	 * @return 1 se è tutto ok, -1 se ko
	 */
	int terminaWfVisti(String idDocumento, String fWorkflowNumber, Long idUfficio);

	/**
	 * Metodo che avanza tutti i workflow, non visti e non contributi, che insistono su <code>idDocumento</code>, diversi da <code>f_WorkflowNumber</code>.
	 * 
	 * @param idDocumento
	 * @param f_WorkflowNumber
	 * @param idUfficio
	 * 
	 * @return 1 se è tutto ok, -1 se ko
	 */
	int avanzaWfNonVistiNonContributi(String idDocumento, String fWorkflowNumber, Long idUfficio);
	
	/**
	 * Esegue il visto massivo dei documenti.
	 * @param utente
	 * @param wobNumbers
	 * @param tipoVisto
	 * @param motivoAssegnazione
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> visto(UtenteDTO utente, Collection<String> wobNumbers, TipoApprovazioneEnum tipoVisto, String motivoAssegnazione);

	/**
	 * Esegue il visto del documento.
	 * @param utente
	 * @param wobNumbers
	 * @param tipoVisto
	 * @param motivoAssegnazione
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO visto(UtenteDTO utente, String wobNumbers, TipoApprovazioneEnum tipoVisto, String motivoAssegnazione);

	/**
	 * Esegue la verifica massiva dei documenti.
	 * @param utente
	 * @param wobNumbers
	 * @param tipoVisto
	 * @param motivoAssegnazione
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> verificato(UtenteDTO utente, Collection<String> wobNumbers, TipoApprovazioneEnum tipoVisto, String motivoAssegnazione);
	
	/**
	 * Esegue la verifica del documento.
	 * @param utente
	 * @param wobNumbers
	 * @param tipoVisto
	 * @param motivoAssegnazione
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO verificato(UtenteDTO utente, String wobNumbers, TipoApprovazioneEnum tipoVisto, String motivoAssegnazione);
	
	/**
	 * Esegue la verifica massiva del documento.
	 * @param utente
	 * @param wobNumbers
	 * @param motivoAssegnazione
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> nonVerificato(UtenteDTO utente, Collection<String> wobNumbers, String motivoAssegnazione);
	
	/**
	 * Esegue il visto del documento.
	 * @param utente
	 * @param wobNumbers
	 * @param motivoAssegnazione
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> vistoPositivo(UtenteDTO utente, Collection<String> wobNumbers, String motivoAssegnazione);
	
	/**
	 * Esegue il visto del documento.
	 * @param utente
	 * @param wobNumber
	 * @param motivoAssegnazione
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO vistoPositivo(UtenteDTO utente, String wobNumber, String motivoAssegnazione);

	/**
	 * Esegue la verifica del documento.
	 * @param utente
	 * @param wobNumber
	 * @param motivoAssegnazione
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO nonVerificato(UtenteDTO utente, String wobNumber, String motivoAssegnazione);

	/**
	 * Effettua l'update del workflow padre.
	 * @param wobGiroVisti
	 * @param wobNumberPadre
	 * @param idDocumento
	 * @param idAOO
	 * @param fpeh
	 */
	void updateWorkflowPadre(String wobGiroVisti, String wobNumberPadre, Integer idDocumento, Long idAOO,
			FilenetPEHelper fpeh);
}