/**
 * 
 */
package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.enums.TipoNotificaPecEnum;
import it.ibm.red.webservice.model.interfaccianps.postacerttypes.Consegna;
import it.ibm.red.webservice.model.interfaccianps.postacerttypes.Identificativo;
import it.ibm.red.webservice.model.interfaccianps.postacerttypes.Mittente;
import it.ibm.red.webservice.model.interfaccianps.postacerttypes.Msgid;
import it.ibm.red.webservice.model.interfaccianps.postacerttypes.Oggetto;

/**
 * @author m.crescentini
 *
 */
public class DatiCertDTO implements Serializable {

	private static final long serialVersionUID = 2500655320983361339L;
	
	/**
	 * Tipo notifiche PEC.
	 */
	private final TipoNotificaPecEnum tipoNotificaPec;

	/**
	 * Message id.
	 */
	private String messageId;
	
	/**
	 * Identificativo messaggio.
	 */
	private String identificativoMessaggio;
	
	/**
	 * Oggetto.
	 */
	private String oggetto;
	
	/**
	 * Consegna.
	 */
	private String consegna;
	
	/**
	 * Mittente.
	 */
	private String mittente;
	
	/**
	 * Lista destinatari.
	 */
	private final List<DestinatarioDatiCertDTO> destinatari;
	
	/**
	 * Errore.
	 */
	private final String errore;
	
	// N.B. Alcuni campi del daticert non vengono presi in considerazione in quanto (attualmente) di non interesse
	
	/**
	 * @param tipoNotificaPec
	 * @param msgId
	 * @param identificativo
	 * @param oggetto
	 * @param consegna
	 * @param mittente
	 * @param destinatari
	 * @param errore
	 */
	public DatiCertDTO(final TipoNotificaPecEnum tipoNotificaPec, final Msgid msgId, final Identificativo identificativo, final Oggetto oggetto, final Consegna consegna, 
			final Mittente mittente, final List<DestinatarioDatiCertDTO> destinatari, final String errore) {
		super();
		this.tipoNotificaPec = tipoNotificaPec;
		if (msgId != null) {
			this.messageId = msgId.getContent();
		}
		if (identificativo != null) {
			this.identificativoMessaggio = identificativo.getContent();
		}
		if (oggetto != null) {
			this.oggetto = oggetto.getContent();
		}
		if (consegna != null) {
			this.consegna = consegna.getContent();
		}
		if (mittente != null) {
			this.mittente = mittente.getContent();
		}
		this.destinatari = destinatari;
		this.errore = errore;
	}

	/**
	 * @return the oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * @return the mittente
	 */
	public String getMittente() {
		return mittente;
	}

	/**
	 * @return the messageId
	 */
	public String getMessageId() {
		return messageId;
	}

	/**
	 * @return the identificativoMessaggio
	 */
	public String getIdentificativoMessaggio() {
		return identificativoMessaggio;
	}
	
	/**
	 * @return the destinatari
	 */
	public List<DestinatarioDatiCertDTO> getDestinatari() {
		return destinatari;
	}

	/**
	 * @return the tipoNotificaPec
	 */
	public TipoNotificaPecEnum getTipoNotificaPec() {
		return tipoNotificaPec;
	}
	
	/**
	 * @return the consegna
	 */
	public String getConsegna() {
		return consegna;
	}

	/**
	 * @return the errore
	 */
	public String getErrore() {
		return errore;
	}

}
