/**
 * 
 */
package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.CollectionUtils;

import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.TipoDestinatario;
import it.ibm.red.business.constants.Constants.TipoSpedizione;
import it.ibm.red.business.dto.ASignMasterDTO;
import it.ibm.red.business.dto.GestioneLockDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.RegistroRepertorioDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.enums.TipoSpedizioneDocumentoEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.enums.TrasformazionePDFInErroreEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Trasformer documento generic Doc
 * 
 * @author APerquoti
 */
public class ContextFromDocumentoToGenericDoc extends ContextTrasformerCE<MasterDocumentRedDTO> {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 6198986337394115883L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContextFromDocumentoToGenericDoc.class.getName());

	/**
	 * Posizione tipo destinatari.
	 */
	private static final int TIPO_DEST_POSITION = 3;

	/**
	 * Costruttore.
	 */
	public ContextFromDocumentoToGenericDoc() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_GENERIC_DOC_CONTEXT);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextTrasformerCE#trasform(com.filenet.api.core.Document,
	 *      java.util.Map).
	 */
	@SuppressWarnings("unchecked")
	@Override
	public MasterDocumentRedDTO trasform(final Document document, final Map<ContextTrasformerCEEnum, Object> context) {
		try {
			final String guuid = StringUtils.cleanGuidToString(document.get_Id());
			final String classeDocumentale = document.getClassName();
			final String dt = (String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			final Integer numDoc = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			final String ogg = (String) getMetadato(document, PropertiesNameEnum.OGGETTO_METAKEY);
			final String nomeFile = (String) getMetadato(document, PropertiesNameEnum.NOME_FILE_METAKEY);
			Integer annoDoc = null;

			final Boolean manageResponse = (Boolean) context.get(ContextTrasformerCEEnum.GESTISCI_RESPONSE);
			if (Boolean.TRUE.equals(manageResponse)) {
				annoDoc = (Integer) getMetadato(document, PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY);
			}

			final Date dataCreazione = (Date) getMetadato(document, PropertiesNameEnum.DATA_CREAZIONE_METAKEY);
			final Date dataScadenza = (Date) getMetadato(document, PropertiesNameEnum.DATA_SCADENZA_METAKEY);
			final Integer idCategoriaDocumento = (Integer) getMetadato(document, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY);

			// Pacchetto informazioni riguardanti il protocollo
			final Integer numeroProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
			final Integer annoProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
			final Integer tipoProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY);
			final Date dataProtocollazione = (Date) getMetadato(document, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);
			final String inIdProtocollo = (String) getMetadato(document, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);

			final Integer numeroProtEmergenza = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_EMERGENZA_METAKEY);
			final Integer annoProtEmergenza = (Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_EMERGENZA_METAKEY);

//			==================== ATTRIBUTI CALCOLATI ========================
			// Recupero della tipologia del documento
			final Integer idTipologiaDocumento = (Integer) getMetadato(document, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
			String tipologiaDocumento = null;
			if (idTipologiaDocumento != null) {
				// CONTEXT
				tipologiaDocumento = ((Map<Long, String>) context.get(ContextTrasformerCEEnum.IDENTIFICATIVI_TIPO_DOC)).get(idTipologiaDocumento.longValue());
			}

			// Recupero del tipo procedimento
			final Integer idTipoProcedimento = (Integer) getMetadato(document, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);
			String tipoProcedimento = null;
			if (idTipoProcedimento != null) {
				// CONTEXT
				tipoProcedimento = ((Map<Long, String>) context.get(ContextTrasformerCEEnum.IDENTIFICATIVI_TIPO_PROC)).get(idTipoProcedimento.longValue());
			}

			// Calcolo presenza di Allegati
			final Integer attachment = document.get_CompoundDocumentState().getValue();
			final Boolean bAttachmentPresent = attachment > 0;

			Integer urgente = null;
			Integer idFormatoDocumento = null;
			Boolean flagFirmaAutografaRM = null;
			Boolean bTrasfPdfErrore = false;
			Boolean bTrasfPdfWarning = false;
			String errorMessage = Constants.EMPTY_STRING;
			String warningMessage = Constants.EMPTY_STRING;
			Boolean flagFirmaPDF = null;
			String protocolloRiferimento = null;
			boolean integrazioneDati = false;

			// Codice flusso per i documenti generati da flussi
			final String codiceFlusso = (String) getMetadato(document, PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY);

			// Gestione dei metadati non recuperati dalla query che esegue la ricerca dei
			// documenti
			final boolean isRicerca = Boolean.TRUE.equals(context.get(ContextTrasformerCEEnum.IS_RICERCA));
			if (!isRicerca) {
				// Gestione del flag PDF in errore
				final Integer trasfPdfErrore = (Integer) getMetadato(document, PropertiesNameEnum.TRASFORMAZIONE_PDF_ERRORE_METAKEY);
				if (trasfPdfErrore != null && trasfPdfErrore != 0) {
					bTrasfPdfErrore = TrasformazionePDFInErroreEnum.getErrorCodes().contains(trasfPdfErrore);
					errorMessage = Boolean.TRUE.equals(bTrasfPdfErrore) ? TrasformazionePDFInErroreEnum.get(trasfPdfErrore).getDescription() : "";

					bTrasfPdfWarning = TrasformazionePDFInErroreEnum.getWarnCodes().contains(trasfPdfErrore);
					warningMessage = Boolean.TRUE.equals(bTrasfPdfWarning) ? TrasformazionePDFInErroreEnum.get(trasfPdfErrore).getDescription() : "";
				}

				protocolloRiferimento = (String) getMetadato(document, PropertiesNameEnum.PROTOCOLLO_RIFERIMENTO_METAKEY);

				urgente = (Integer) getMetadato(document, PropertiesNameEnum.URGENTE_METAKEY);

				idFormatoDocumento = (Integer) getMetadato(document, PropertiesNameEnum.ID_FORMATO_DOCUMENTO);

				flagFirmaAutografaRM = (Boolean) getMetadato(document, PropertiesNameEnum.FLAG_FIRMA_AUTOGRAFA_RM_METAKEY);

				// Gestione del metadato firmaPDF
				flagFirmaPDF = (Boolean) getMetadato(document, PropertiesNameEnum.FIRMA_PDF_METAKEY);
				if (flagFirmaPDF == null) {
					flagFirmaPDF = true;
				}
			}

			// Gestione del Flag riservato
			final Integer riservato = (Integer) getMetadato(document, PropertiesNameEnum.RISERVATO_METAKEY);
			Boolean flagRiservato = false;
			if (riservato != null && riservato > 0) {
				flagRiservato = true;
			}

			final Boolean flagIntegrazioneDati = (Boolean) getMetadato(document, PropertiesNameEnum.INTEGRAZIONE_DATI_METAKEY);
			if (flagIntegrazioneDati != null) {
				integrazioneDati = flagIntegrazioneDati.booleanValue();
			}

			// Data da visualizzare
			Date dataDaVisualizzare = dataCreazione;
			// Assumiamo che se l'informazione oraria della data protocollazione è uguale a
			// "00:00:00", allora tale informazione è assente
			if (dataProtocollazione != null && !"00:00:00".equals(new SimpleDateFormat(DateUtils.HH24_MM_SS).format(dataProtocollazione))) {
				dataDaVisualizzare = dataProtocollazione;
			}

			// Recupero del numero dei contributi
			final Boolean hasContributi = false;

			// Destinatari
			final Collection<?> inDestinatari = (Collection<?>) getMetadato(document, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
			// Si normalizza il metadato per evitare errori comuni durante lo split delle
			// singole stringhe dei destinatari
			final List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(inDestinatari);

			// Recupero del Tipo Spedizione
			final TipoSpedizioneDocumentoEnum tipoSpedizione = getTipoSpedizione(destinatariDocumento);

			// Gestione Mittente / Destinatari
			String mittente = null;
			StringBuilder destinatari = null;

			if (context.get(ContextTrasformerCEEnum.CONTATTI_DISPLAY_NAME) != null) {
				// Mittente
				final String mittenteDoc = (String) getMetadato(document, PropertiesNameEnum.MITTENTE_METAKEY);
				if (!StringUtils.isNullOrEmpty(mittenteDoc)) {
					final String[] mittenteSplit = mittenteDoc.split("\\,");

					mittente = ((Map<String, String>) context.get(ContextTrasformerCEEnum.CONTATTI_DISPLAY_NAME)).get(mittenteSplit[0]);
					// Destinatari
				} else if (!CollectionUtils.isEmpty(inDestinatari)) {
					destinatari = new StringBuilder("");

					for (final Object destinatario : inDestinatari) {
						final String[] destSplit = ((String) destinatario).split("\\,");

						if (destSplit.length >= 5) {
							final TipologiaDestinatarioEnum tde = TipologiaDestinatarioEnum.getByTipologia(destSplit[3]);

							String alias = null;
							if (TipologiaDestinatarioEnum.INTERNO.equals(tde) && !"0".equals(destSplit[1])) {
								alias = ((Map<String, String>) context.get(ContextTrasformerCEEnum.CONTATTI_DISPLAY_NAME)).get(destSplit[1]);
							} else if (TipologiaDestinatarioEnum.INTERNO.equals(tde) && "0".equals(destSplit[1])) {
								alias = ((Map<String, String>) context.get(ContextTrasformerCEEnum.CONTATTI_DISPLAY_NAME)).get(destSplit[0]);
							} else if (TipologiaDestinatarioEnum.ESTERNO.equals(tde)) {
								alias = ((Map<String, String>) context.get(ContextTrasformerCEEnum.CONTATTI_DISPLAY_NAME)).get(destSplit[0]);
							}

							if (!StringUtils.isNullOrEmpty(alias)) {
								destinatari.append(alias).append(", ");
							}
						}
					}

					if (destinatari != null && destinatari.length() >= 2) {
						destinatari = new StringBuilder(destinatari.substring(0, destinatari.length() - 2));
					}
				}
			}

			boolean flagNoContent = false;
			if (StringUtils.isNullOrEmpty(nomeFile)) {
				flagNoContent = true;
			}

			RegistroRepertorioDTO registroRepertorio = null;
			Boolean isRegistroRepertorio = null;
			String descrizioneRegistroRepertorio = Constants.EMPTY_STRING;
			if (context.get(ContextTrasformerCEEnum.REGISTRO_PROTOCOLLO) != null) {
				registroRepertorio = ((Map<String, RegistroRepertorioDTO>) context.get(ContextTrasformerCEEnum.REGISTRO_PROTOCOLLO)).get(dt);
				if (registroRepertorio != null) {
					isRegistroRepertorio = (registroRepertorio.getIsRegistroRepertorio() && (CategoriaDocumentoEnum.USCITA.getIds()[0].equals(idCategoriaDocumento)
							|| CategoriaDocumentoEnum.USCITA.getIds()[1].equals(idCategoriaDocumento)
							|| CategoriaDocumentoEnum.USCITA.getIds()[2].equals(idCategoriaDocumento)));
					descrizioneRegistroRepertorio = registroRepertorio.getDescrizioneRegistroRepertorio();
				}
			}

			String placeholderSigla = Constants.EMPTY_STRING;
			if (context.get(ContextTrasformerCEEnum.LIBRO_FIRMA_PLACEHOLDER) != null) {
				placeholderSigla = ((Map<String, String>) context.get(ContextTrasformerCEEnum.LIBRO_FIRMA_PLACEHOLDER)).get(dt);
			}

			final Integer idUffCreat = (Integer) getMetadato(document, PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY);

			final String tipoFirmaStr = (String) getMetadato(document, PropertiesNameEnum.ITER_APPROVATIVO_SEMAFORO_METAKEY);
			Integer tipoFirma = null;
			if (tipoFirmaStr != null) {
				tipoFirma = Integer.parseInt(tipoFirmaStr);
			}
			final SottoCategoriaDocumentoUscitaEnum sottoCategoriaDocumentoUscita = SottoCategoriaDocumentoUscitaEnum
					.get((Integer) getMetadato(document, PropertiesNameEnum.SOTTOCATEGORIA_DOCUMENTO_USCITA));

			final Boolean firmeValideAll = (Boolean) getMetadato(document, PropertiesNameEnum.VERIFICA_FIRMA_DOC_ALLEGATO); // Vale solo per gli allegati
			final Integer metadatoValiditaFirma = (Integer) getMetadato(document, PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA);
			Boolean firmaValidaPrinc = null;
			if(metadatoValiditaFirma!=null) {
				if(ValueMetadatoVerificaFirmaEnum.FIRMA_OK.equals(ValueMetadatoVerificaFirmaEnum.get(metadatoValiditaFirma))) {
					firmaValidaPrinc = true;
				}else if(ValueMetadatoVerificaFirmaEnum.FIRMA_KO.equals(ValueMetadatoVerificaFirmaEnum.get(metadatoValiditaFirma))){
					firmaValidaPrinc = false;
				}
				
			}
			
			final String decretoLiquidazione =  (String) getMetadato(document, PropertiesNameEnum.DECRETO_LIQUIDAZIONE_METAKEY);
				
			Integer versionNumber =  (Integer) getMetadato(document, PropertiesNameEnum.MAJOR_VERSION_NUMBER_METAKEY);
			
			Integer uffCreatore = null;
			if (context.get(ContextTrasformerCEEnum.UFF_CREATORE_MAP) != null) {
				uffCreatore = ((Map<String, Integer>) context.get(ContextTrasformerCEEnum.UFF_CREATORE_MAP)).get(dt);
			}
			 
			ASignMasterDTO info = null;
			if (context.get(ContextTrasformerCEEnum.ASIGN_INFO) != null) {
				info = ((Map<String, ASignMasterDTO>) context.get(ContextTrasformerCEEnum.ASIGN_INFO)).get(dt);
			}
			
			
//			====================FINE========================
			
			MasterDocumentRedDTO master = new MasterDocumentRedDTO(dt, numDoc, ogg, tipologiaDocumento, numeroProtocollo, annoProtocollo, bAttachmentPresent, bTrasfPdfErrore, bTrasfPdfWarning, 
											dataScadenza, urgente, dataDaVisualizzare, idCategoriaDocumento, flagFirmaAutografaRM, tipoProtocollo, flagRiservato, 
											hasContributi, flagFirmaPDF, tipoProcedimento, tipoSpedizione, annoDoc, guuid, idFormatoDocumento, dataProtocollazione, 
											classeDocumentale, errorMessage, warningMessage, nomeFile, mittente, destinatari != null ? destinatari.toString() : null, 
											flagNoContent, inIdProtocollo, numeroProtEmergenza, annoProtEmergenza, isRegistroRepertorio, descrizioneRegistroRepertorio, codiceFlusso,
											placeholderSigla, idTipologiaDocumento, idTipoProcedimento, sottoCategoriaDocumentoUscita, protocolloRiferimento, integrazioneDati,
											idUffCreat,firmaValidaPrinc,firmeValideAll,  decretoLiquidazione, metadatoValiditaFirma, versionNumber, uffCreatore, destinatariDocumento,
											info);
			
			
			if(tipoFirma!=null) {
				master.setTipoFirma(tipoFirma);	
			}

			final HashMap<Long, GestioneLockDTO> listaLock = (HashMap<Long, GestioneLockDTO>) context.get(ContextTrasformerCEEnum.MAPLOCK);

			if (listaLock != null && !listaLock.isEmpty() && master.getDocumentTitle() != null) {
				master.setDocLocked(listaLock.containsKey(Long.parseLong(master.getDocumentTitle())));
				if (master.isDocLocked()) {
					master.setUsernameLocked(
							listaLock.get(Long.parseLong(master.getDocumentTitle())).getNome() + " " + listaLock.get(Long.parseLong(master.getDocumentTitle())).getCognome());
				}

			}

			return master;
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero di un metadato durante la trasformazione da Document a MasterDocumentRedDTO : ", e);
			return null;
		}
	}

	private static TipoSpedizioneDocumentoEnum getTipoSpedizione(final List<String[]> inDestinatari) {
		TipoSpedizioneDocumentoEnum output;
		boolean bElettronico = false;
		boolean bCartaceo = false;
		if (inDestinatari != null) {

			for (final String[] destSplit : inDestinatari) {
				Integer idTipoSpedizione;

				final String tipoDestinatario = destSplit[TIPO_DEST_POSITION];
				if (TipoDestinatario.ESTERNO.equalsIgnoreCase(tipoDestinatario)) {
					idTipoSpedizione = Integer.parseInt(destSplit[2]);
					if (TipoSpedizione.MEZZO_ELETTRONICO.equals(idTipoSpedizione)) {
						bElettronico = true;
					} else {
						bCartaceo = true;
					}
				}
				if (bElettronico && bCartaceo) {
					break;
				}
			}
		}
		if (bElettronico) {
			output = TipoSpedizioneDocumentoEnum.ELETTRONICO;
		} else {
			output = TipoSpedizioneDocumentoEnum.CARTACEO;
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextTrasformerCE#trasform(com.filenet.api.core.Document,
	 *      java.sql.Connection, java.util.Map).
	 */
	@Override
	public MasterDocumentRedDTO trasform(final Document document, final Connection connection, final Map<ContextTrasformerCEEnum, Object> context) {
		throw new RedException();
	}

}
