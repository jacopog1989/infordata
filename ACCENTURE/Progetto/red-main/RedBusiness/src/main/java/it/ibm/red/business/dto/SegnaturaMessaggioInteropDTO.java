/**
 * 
 */
package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.enums.EsitoValidazioneSegnaturaInteropEnum;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.Allegati;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.Documento;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.Segnatura;
import it.ibm.red.webservice.model.interfaccianps.npstypes.ProtErroreValidazioneSegnaturaType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.ProtValidazioneSegnaturaType;

/**
 * @author m.crescentini
 *
 */
public class SegnaturaMessaggioInteropDTO implements Serializable {

	private static final long serialVersionUID = -3990155829469303381L;

	/**
	 * Indirizzo mail mittente.
	 */
	private String indirizzoMailMittente;
	
	/**
	 * Pre-assegnatario.
	 */
	private AssegnatarioInteropDTO preAssegnatario;
	
	/**
	 * Id docunento principale.
	 */
	private String idDocumentoPrincipale;
	
	/**
	 * Lista allegati.
	 */
	private final List<String> allegati;
	
	/**
	 * Esito validazione.
	 */
	private EsitoValidazioneSegnaturaInteropEnum esitoValidazione;
	
	/**
	 * Lista errori validazione.
	 */
	private List<ErroreValidazioneDTO> erroriValidazione;
	
	/**
	 * Costruttore.
	 * @param segnatura
	 * @param validazioneSegnatura
	 */
	public SegnaturaMessaggioInteropDTO(final Segnatura segnatura, final ProtValidazioneSegnaturaType validazioneSegnatura) {
		final Allegati inAllegati = segnatura.getDescrizione().getAllegati();
		
		if (segnatura.getIntestazione() != null 
				&& segnatura.getIntestazione().getOrigine() != null 
				&& segnatura.getIntestazione().getOrigine().getIndirizzoTelematico() != null 
				&& segnatura.getIntestazione().getOrigine().getIndirizzoTelematico().getContent() != null) {
			this.indirizzoMailMittente = segnatura.getIntestazione().getOrigine().getIndirizzoTelematico().getContent();
		}
		
		if (segnatura.getDescrizione().getDocumento() != null) {
			this.idDocumentoPrincipale = segnatura.getDescrizione().getDocumento().getNome();
		}
		
		
		this.allegati = new ArrayList<>();
		
		if (inAllegati != null && !CollectionUtils.isEmpty(inAllegati.getDocumentoOrFascicolo())) {
			String id = null;
			
			for (final Object allegato : inAllegati.getDocumentoOrFascicolo()) {
				if (allegato instanceof Documento) {
					id = ((Documento) allegato).getNome();
					
					if (StringUtils.isNotBlank(id)) {
						this.allegati.add(id);
					}
				}
			}
			
		}
		
		// Validazione -> START
		this.esitoValidazione = EsitoValidazioneSegnaturaInteropEnum.get(validazioneSegnatura.getEsitoValidazione());
		
		this.erroriValidazione = new ArrayList<>();
		ErroreValidazioneDTO erroreValidazioneMessaggio;
		for (final ProtErroreValidazioneSegnaturaType erroreValidazione : validazioneSegnatura.getErroriRiscontrati()) {
			erroreValidazioneMessaggio = new ErroreValidazioneDTO(erroreValidazione.getCode(), erroreValidazione.getDescription(), 
					erroreValidazione.getSeverity());
			this.erroriValidazione.add(erroreValidazioneMessaggio);
		}
		// Validazione -> END
	}
	
	/**
	 * Costruttore.
	 * @param indirizzoMailMittente
	 * @param idDocumentoPrincipale
	 * @param preAssegnatarioString
	 * @param allegati
	 */
	public SegnaturaMessaggioInteropDTO(final String indirizzoMailMittente, final String idDocumentoPrincipale, final String preAssegnatarioString, final List<String> allegati) {	
		this.indirizzoMailMittente = indirizzoMailMittente;
		this.idDocumentoPrincipale = idDocumentoPrincipale;
		this.preAssegnatario = new AssegnatarioInteropDTO(preAssegnatarioString);
		this.allegati = allegati;	
	}

	
	/**
	 * @return the indirizzoMailMittente
	 */
	public String getIndirizzoMailMittente() {
		return indirizzoMailMittente;
	}


	/**
	 * @return the idDocumentoPrincipale
	 */
	public String getIdDocumentoPrincipale() {
		return idDocumentoPrincipale;
	}


	/**
	 * @return the preAssegnatario
	 */
	public AssegnatarioInteropDTO getPreAssegnatario() {
		return preAssegnatario;
	}

	
	/**
	 * @param preAssegnatario the preAssegnatario to set
	 */
	public void setPreAssegnatario(final AssegnatarioInteropDTO preAssegnatario) {
		this.preAssegnatario = preAssegnatario;
	}


	/**
	 * @return the allegati
	 */
	public List<String> getAllegati() {
		return allegati;
	}
	
	
	/**
	 * @return the esitoValidazione
	 */
	public EsitoValidazioneSegnaturaInteropEnum getEsitoValidazione() {
		return esitoValidazione;
	}


	/**
	 * @return the erroriValidazione
	 */
	public List<ErroreValidazioneDTO> getErroriValidazione() {
		return erroriValidazione;
	}
}
