package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.dto.DatiCertDTO;
import it.ibm.red.business.dto.NotificaPECPostaNpsDTO;
import it.ibm.red.business.dto.SalvaMessaggioPostaNpsContestoDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoMessaggioPostaNpsIngressoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.ICodaMailSRV;
import it.ibm.red.business.utils.EmailUtils;

/**
 * Service che gestisce il salvataggio di una notifica PEC.
 */
@Service
@Component
public class SalvaNotificaPECPostaNpsSRV extends SalvaMessaggioPostaNpsAbstractSRV<NotificaPECPostaNpsDTO> {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = -4354157535902293591L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SalvaNotificaPECPostaNpsSRV.class.getName());
	
	/**
	 * Service.
	 */
	@Autowired
	private ICodaMailSRV codaMailSRV;
	
	/**
	 * @see it.ibm.red.business.service.concrete.SalvaMessaggioInteropAbstractSRV#getMessageId(it.ibm.red.business.dto.SalvaMessaggioInteropContestoDTO).
	 * N.B. In questo caso, il Message-ID di interesse è quello del messaggio originale, non quello del messaggio di notifica PEC.
	 */
	@Override
	protected String getMessageId(final SalvaMessaggioPostaNpsContestoDTO<NotificaPECPostaNpsDTO> contesto) {
		String outMessageId = null;
		
		final DatiCertDTO datiCert = contesto.getRichiesta().getMessaggio().getDatiCert();
		if (datiCert != null) {
			outMessageId = datiCert.getMessageId().replace("&lt;", "<").replace("&gt;", ">");
		} else {
			LOGGER.error("Notifica PEC senza file \"daticert.xml\"");
			throw new RedException("Notifica PEC senza file \"daticert.xml\"");
		}
		
		return outMessageId;
	}

	/**
	 * @see it.ibm.red.business.service.concrete.SalvaMessaggioInteropAbstractSRV#getMittente(it.ibm.red.business.dto.SalvaMessaggioInteropContestoDTO).
	 */
	@Override
	protected String getMittente(final SalvaMessaggioPostaNpsContestoDTO<NotificaPECPostaNpsDTO> contesto) {
		String mittente = null;
		
		try {
			List<String> indirizziMittente = null;
			
			final DatiCertDTO datiCert = contesto.getRichiesta().getMessaggio().getDatiCert();
			// Elemento "consegna" del daticert.xml (se presente) 
			if (datiCert != null && StringUtils.isNotBlank(datiCert.getConsegna())) {
				
				mittente = datiCert.getConsegna();
				
			// Altrimenti, attributo "from" del messaggio MIME
			} else if (contesto.getMessaggioMime().getFrom() != null) {
				indirizziMittente = EmailUtils.getIndirizziMailDaAddresses(contesto.getMessaggioMime().getFrom());
				
				if (!CollectionUtils.isEmpty(indirizziMittente)) {
					mittente = indirizziMittente.get(0);
				}
				
			// Altrimenti, attributo "sender" del messaggio MIME
			} else if (contesto.getMessaggioMime().getSender() != null) {
				
				mittente = EmailUtils.getIndirizzoMailDaAddress(contesto.getMessaggioMime().getSender());
			
			} else {
				mittente = "NoSender@NoSender.com";
			}
			
		} catch (final MessagingException me) {
			LOGGER.error("Si è verificato un errore durante il recupero del mittente per il messaggio con Message-ID: "
					+ contesto.getRichiesta().getMessaggio().getMessageId(), me);
		}
		
		return mittente;
	}

	/**
	 * @see it.ibm.red.business.service.concrete.SalvaMessaggioInteropAbstractSRV#aggiornaTipologiaDestinatariMail
	 *      (it.ibm.red.business.dto.DatiCertDTO, java.lang.String,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	protected void aggiornaTipologiaDestinatariMail(final String messageId, final NotificaPECPostaNpsDTO messaggio, final String mittente, final Integer idAdminAoo, final Connection con) {
		try {
			codaMailSRV.updateTipoDestinatario(messaggio.getDatiCert().getDestinatari(), messageId, mittente, idAdminAoo, con);
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'aggiornamento delle tipologie di destinatari per il messaggio con Message-ID: " + messageId, e);
			throw e;
		}
	}


	/**
	 * @see it.ibm.red.business.service.concrete.SalvaMessaggioPostaNpsAbstractSRV#getFolderDestinazioneMessaggio(
	 *      it.ibm.red.business.enums.TipoMessaggioPostaNpsIngressoEnum).
	 */
	@Override
	protected String getFolderDestinazioneMessaggio(final TipoMessaggioPostaNpsIngressoEnum tipoIngresso) {
		return getPp().getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_NOTIFICHE_FN_METAKEY);
	}

	/**
	 * @see it.ibm.red.business.service.concrete.SalvaMessaggioInteropAbstractSRV#isMessaggioDuplicato
	 *      (it.ibm.red.business.helper.filenet.ce.IFilenetHelper, java.lang.String,
	 *      java.lang.String, java.lang.String).
	 */
	@Override
	protected boolean isMessaggioDuplicato(final IFilenetCEHelper fceh, final String messageId, final String mailCasellaPostale) {
		return false;
	}
	
	/**
	 * @see it.ibm.red.business.service.concrete.SalvaMessaggioInteropAbstractSRV#gestisciSpedizione
	 *      (it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 *      it.ibm.red.business.dto.DatiCertDTO).
	 */
	@Override
	protected void gestisciSpedizione(final String messageId, final NotificaPECPostaNpsDTO messaggio, final IFilenetCEHelper fceh) {
		fceh.gestisciSpedizioneDestinatario(messageId, messaggio.getDatiCert());
	}

	/**
	 * @see it.ibm.red.business.service.concrete.SalvaMessaggioInteropAbstractSRV#isNotificaPec().
	 */
	@Override
	protected boolean isNotificaPec() {
		return true;
	}
}