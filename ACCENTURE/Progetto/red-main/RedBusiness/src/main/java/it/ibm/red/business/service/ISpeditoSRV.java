package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.Date;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.service.facade.ISpeditoFacadeSRV;

/**
 * Interfaccia del servizio di gestione spediti.
 */
public interface ISpeditoSRV extends ISpeditoFacadeSRV {

	/**
	 * Metodo di spedizione.
	 * @param utente
	 * @param dataSped
	 * @param wobNumber
	 * @param aggiornaNps
	 * @param conn
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO spedito(UtenteDTO utente, Date dataSped, String wobNumber, boolean aggiornaNps, Connection conn);
	
}
