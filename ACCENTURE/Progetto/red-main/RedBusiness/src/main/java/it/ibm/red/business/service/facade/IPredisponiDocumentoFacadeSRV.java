package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.EsitoPredisponiDocumentoDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ResponsesRedEnum;

/**
 * Facade del servizio di predisposizione documento.
 */
public interface IPredisponiDocumentoFacadeSRV extends Serializable {

	/**
	 * @param utente
	 * @param responseSollecitata
	 * @param docsDaAllacciareAllaccioList
	 * @param docDaRibaltareAllaccio
	 * @param contentUscita
	 * @param idRegistroAusiliario
	 * @param metadatiRegistroAusiliarioList
	 * @return
	 */
	EsitoPredisponiDocumentoDTO predisponiUscita(UtenteDTO utente, ResponsesRedEnum responseSollecitata,
			List<RispostaAllaccioDTO> docsDaAllacciareAllaccioList, RispostaAllaccioDTO docDaRibaltareAllaccio,
			FileDTO contentUscita, Integer idRegistroAusiliario, Collection<MetadatoDTO> metadatiRegistroAusiliarioList, Collection<MetadatoDTO> metadatiDocUscita);

	/**
	 * @param m
	 * @return
	 */
	RispostaAllaccioDTO trasformaInRispostaAllaccio(MasterDocumentRedDTO m);

	/**
	 * @param masters
	 * @return
	 */
	List<RispostaAllaccioDTO> trasformaInRisposteAllaccio(List<MasterDocumentRedDTO> masters);

}
