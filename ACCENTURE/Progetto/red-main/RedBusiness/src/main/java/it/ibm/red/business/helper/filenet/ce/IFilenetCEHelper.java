package it.ibm.red.business.helper.filenet.ce;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.activation.DataHandler;

import com.filenet.api.admin.PropertyDefinition;
import com.filenet.api.collection.AccessPermissionList;
import com.filenet.api.collection.DocumentSet;
import com.filenet.api.collection.FolderSet;
import com.filenet.api.collection.IndependentObjectSet;
import com.filenet.api.collection.PageIterator;
import com.filenet.api.collection.RepositoryRowSet;
import com.filenet.api.core.CustomObject;
import com.filenet.api.core.Document;
import com.filenet.api.core.Folder;
import com.filenet.api.core.IndependentlyPersistableObject;
import com.filenet.api.core.UpdatingBatch;
import com.filenet.api.property.Properties;
import com.filenet.api.property.PropertyFilter;
import com.filenet.api.security.AccessPermission;
import com.filenet.api.util.Id;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.DatiCertDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DetailFatturaFepaDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.dto.PropertyTemplateDTO;
import it.ibm.red.business.dto.SecurityDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.AnnotationDTO;
import it.ibm.red.business.dto.filenet.DocumentoFascicolazioneRedFnDTO;
import it.ibm.red.business.dto.filenet.DocumentoRedFnDTO;
import it.ibm.red.business.dto.filenet.FascicoloRedFnDTO;
import it.ibm.red.business.enums.CampoRicercaEnum;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.FilenetAnnotationEnum;
import it.ibm.red.business.enums.ModalitaRicercaAvanzataTestoEnum;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.StatoMailEnum;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper.Metadato;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.TipoFile;

/**
 * Interfaccia helper Filenet CE.
 */
public interface IFilenetCEHelper {

	/**
	 * Inserisce una nuova versione ad un documento.
	 * 
	 * @param doc
	 * @param stream
	 * @param metadati
	 * @param fileName
	 * @param mimeType
	 * @param idAoo
	 * @return documento memorizzato su cui è stato fatto il refresh
	 */
	Document addVersion(Document doc, InputStream stream, Map<String, Object> metadati, String fileName,
			String mimeType, Long idAoo);

	/**
	 * Inserisce una nuova versione ad un documento.
	 * 
	 * @param doc
	 * @param stream
	 * @param preview
	 * @param metadati
	 * @param fileName
	 * @param mimeType
	 * @param idAoo
	 * @return documento memorizzato su cui è stato fatto il refresh
	 */
	Document addVersion(Document doc, InputStream stream, InputStream preview, Map<String, Object> metadati,
			String fileName, String mimeType, Long idAoo);

	/**
	 * Inserisce una nuova versione ad un documento.
	 * 
	 * @param doc
	 * @param stream
	 * @param preview
	 * @param contentLibroFirma
	 * @param metadati
	 * @param fileName
	 * @param mimeType
	 * @param idAoo
	 * @return
	 */
	Document addVersion(final Document doc, final InputStream stream, final InputStream preview,
			final InputStream contentLibroFirma, final Map<String, Object> metadati, final String fileName,
			final String mimeType, final Long idAoo);

	/**
	 * Chiude la sessione utente del CE FileNet.
	 */
	void popSubject();

	/**
	 * Dump di tutte le 'Class Definition'.
	 */
	void dumpAllClassDefinitions();

	/**
	 * Dump di tutte le 'Class Definition' by nome della classe.
	 * 
	 * @param className
	 */
	void dumpClassDefinitionProperties(String className);

	/**
	 * Aggiorna l'elenco delle DSR per la fattura.
	 * 
	 * @param docFattura
	 * @param docDichiarazioneServiziResi
	 * @throws IOException
	 */
	void aggiornaFatturaConDSR(Document docFattura, List<Document> docDichiarazioneServiziResi) throws IOException;

	/**
	 * Ottiene il valore.
	 * 
	 * @param key
	 * @param value
	 * @param multiValues
	 * @param classeDocumentale
	 * @return valore
	 */
	Object getValueObjectClass(String key, String value, String[] multiValues, String classeDocumentale);

	/**
	 * Aggiornamento la casella mail.
	 * 
	 * @param folder
	 * @param mailBoxs
	 * @param oldMetadato
	 * @param newMetadato
	 * @param milestoneDate
	 */
	void updateStateMailByState(String folder, FolderSet mailBoxs, Metadato oldMetadato, Metadato newMetadato,
			Date milestoneDate);

	/**
	 * Dump di tutte le 'Class Definition' by nome della classe.
	 * 
	 * @param className
	 */
	void dumpClassDefinitionPropertiesWithoutSystemProps(String className);

	/**
	 * Get di tutte le 'Class Definition' by nome della classe.
	 * 
	 * @param className
	 * @return properties
	 */
	Set<String> getClassDefinitionPropertiesNameWithoutSystemProps(String className);

	/**
	 * Ottiene il documento tramite document title e id dell'aoo.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @return documento
	 */
	Document getDocumentByDTandAOO(String documentTitle, Long idAoo);

	/**
	 * Ottiene la versione del documento.
	 * 
	 * @param d
	 * @param majorVersionNumber
	 * @return documento
	 */
	Document getDocumentVersion(Document d, Integer majorVersionNumber);

	/**
	 * Ottiene le versioni del documento.
	 * 
	 * @param idDocumento
	 * @param idAoo
	 * @return lista delle versioni del documento
	 */
	List<?> getDocumentVersionList(String idDocumento, Integer idAoo);

	/**
	 * Ottiene le versioni del documento tramite guid.
	 * 
	 * @param guid
	 * @param idAoo
	 * @return lista delle versioni del documento
	 */
	List<?> getDocumentVersionListByGuid(String guid, Integer idAoo);

	/**
	 * Ottiene la prima versione del documento tramite guid.
	 * 
	 * @param guid
	 * @return documento
	 */
	Document getFirstVersionByGuid(String guid);

	/**
	 * Ottiene la prima versione del documento tramite id.
	 * 
	 * @param idDocumento
	 * @param idAoo
	 * @return documento
	 */
	Document getFirstVersionById(String idDocumento, Integer idAoo);

	/**
	 * Fetch delle email di un documento.
	 * 
	 * @param idDocumento
	 * @param idAOO
	 * @return documento
	 */
	Document fetchMailSpedizione(String idDocumento, Integer idAOO);

	/**
	 * Ottiene il tipo di casella postale dal nome.
	 * 
	 * @param nomeCasella
	 * @return tipo di casella postale
	 */
	Integer getTipoCasellaPostaleFromNome(String nomeCasella);

	/**
	 * Ottiene la folder.
	 * 
	 * @param id
	 * @return folder
	 */
	Folder getFolder(Id id);

	/**
	 * 
	 * @param folder
	 * @return
	 */
	Folder getFolder(String folder);

	/**
	 * Ottiene il documento.
	 * 
	 * @param id
	 * @return documento
	 */
	Document getDocument(Id id);

	/**
	 * Ottiene le caselle postali.
	 * 
	 * @return lista delle caselle postali
	 */
	List<CasellaPostaDTO> getCasellePostaliDTO();

	/**
	 * Ottiene la casella postale.
	 * 
	 * @param nomeCasella
	 * @return casella postale
	 */
	CasellaPostaDTO getCasellaPostaleDTO(String nomeCasella);

	/**
	 * Ottiene le mails come documenti.
	 * 
	 * @param path
	 * @param documentClass
	 * @param filter
	 * @param selectedStatList
	 * @param orderByClause
	 * @return DocumentSet delle mails
	 */
	DocumentSet getMailsAsDocument(String path, String documentClass, String filter,
			List<StatoMailEnum> selectedStatList, String orderByClause);

	/**
	 * Ottiene le mails come documenti.
	 * 
	 * @param path
	 * @param documentClass
	 * @param filter
	 * @param selectedStatList
	 * @return DocumentSet delle mails
	 */
	DocumentSet getMailsAsDocument(String path, String documentClass, String filter,
			List<StatoMailEnum> selectedStatList);

	/**
	 * Ottiene le mails come documenti.
	 * 
	 * @param path
	 * @param documentClass
	 * @param filter
	 * @param searchFields
	 * @param onlyFirst
	 * @param selectedStatList
	 * @return DocumentSet delle mails
	 */
	DocumentSet getMailsAsDocument(String path, String documentClass, String filter, String searchFields,
			boolean onlyFirst, List<StatoMailEnum> selectedStatList);

	/**
	 * Conta il numero di mails.
	 * 
	 * @param path
	 * @param documentClass
	 * @param statoMail
	 * @return numero di mails
	 */
	Integer countMailFolder(String path, String documentClass, List<String> statoMail);

	/**
	 * Conta il numero di allegati da firmare.
	 * 
	 * @param documentClass
	 * @param documentTitle
	 * @return numero di allegati da firmare
	 */
	int getCountAllegatiToSign(String documentClass, String documentTitle);

	/**
	 * Ottiene il documento per contributo.
	 * 
	 * @param documentTitle
	 * @return contributo
	 */
	Document getDocumentForContributo(String documentTitle);

	/**
	 * Ottiene il file di info e il content tramite document title.
	 * 
	 * @param documentTitle
	 * @param idAOO
	 * @return documento
	 */
	Document getFileInfoAndContentByDocumentTitle(String documentTitle, Long idAOO);

	/**
	 * Ottiene il documento per lo storico visuale.
	 * 
	 * @param documentTitle
	 * @return documento
	 */
	Document getDocumentForStoricoVisuale(String documentTitle);

	/**
	 * Ottiene il documento per il dettaglio.
	 * 
	 * @param documentClass
	 * @param documentTitle
	 * @return documento
	 */
	Document getDocumentForDetail(String documentClass, String documentTitle);

	/**
	 * Ottiene il documento per il dettaglio.
	 * 
	 * @param documentClass
	 * @param documentTitle
	 * @return documento
	 */
	Document getDocumentRedForDetail(String documentClass, String documentTitle);

	/**
	 * Ottiene il documento per il dettaglio light.
	 * 
	 * @param documentClass
	 * @param documentTitle
	 * @return documento
	 */
	Document getDocumentRedForDetailLIGHT(String documentClass, String documentTitle);

	/**
	 * Ottiene l'allegato per il download.
	 * 
	 * @param documentClass
	 * @param documentTitle
	 * @return allegato
	 */
	Document getAllegatoForDownload(String documentClass, String documentTitle);

	/**
	 * Ottiene il documento per il download.
	 * 
	 * @param documentClass
	 * @param documentTitle
	 * @return documento
	 */
	Document getDocumentForDownload(String documentClass, String documentTitle);

	/**
	 * Ottiene il documento per il download.
	 * 
	 * @param documentClass
	 * @param documentTitle
	 * @param nomeFile
	 * @return documento
	 */
	Document getDocumentForDownload(String documentClass, String documentTitle, boolean nomeFile);

	/**
	 * Ottiene il documento per il download dell'allegato.
	 * 
	 * @param guid
	 * @param nomeFile
	 * @return documento
	 */
	Document getDocumentForDownloadAllegato(String guid, boolean nomeFile);

	/**
	 * Ottiene il documento per l'allegato.
	 * 
	 * @param guid
	 * @param nomeFile
	 * @return documento
	 */
	Document getDocumentForAllegato(String guid, boolean nomeFile);

	/**
	 * Ottiene il documento per le operazione post rifiuto.
	 * 
	 * @param documentClass
	 * @param documentTitle
	 * @return documento
	 */
	Document getDocumentForRifiuto(String documentClass, String documentTitle);

	/**
	 * Ottiene i documenti per i master filtrati.
	 * 
	 * @param documentTitles
	 * @param allTokensFiltro
	 * @param numericTokens
	 * @param numeroProtocolloTokens
	 * @param annoProtocolloTokens
	 * @param idsTipologiaDocumento
	 * @param idAoo
	 * @param documentClass
	 * @return DocumentSet dei documenti
	 */
	DocumentSet getDocumentForFilteredMasters(Collection<String> documentTitles, List<String> allTokensFiltro,
			List<String> numericTokens, List<String> numeroProtocolloTokens, List<String> annoProtocolloTokens,
			List<Long> idsTipologiaDocumento, Long idAoo, String documentClass);

	/**
	 * Ottiene i documenti per la visualizzazione del master.
	 * 
	 * @param documentClass
	 * @param documentTitles
	 * @return DocumentSet dei documenti
	 */
	DocumentSet getDocumentsForMasters(String documentClass, Collection<String> documentTitles);

	/**
	 * Ottiene il paginatore per la visualizzazione del master.
	 * 
	 * @param documentClass
	 * @param documentTitles
	 * @param pageSize
	 * @return paginatore
	 */
	PageIterator getPageIteratorForMasters(String documentClass, Collection<String> documentTitles, Integer pageSize);

	/**
	 * Ottiene il paginatore per la visualizzazione delle mails.
	 * 
	 * @param path
	 * @param documentClass
	 * @param filter
	 * @param selectedStatList
	 * @param pageSize
	 * @param orderbyClause
	 * @return paginatore
	 */
	PageIterator getPageIteratorForMail(String path, String documentClass, String filter,
			List<StatoMailEnum> selectedStatList, Integer pageSize, String orderbyClause);

	/**
	 * Ottiene il numero documento tramite document title.
	 * 
	 * @param documentTitle
	 * @return numero documento
	 */
	Integer getNumDocByDocumentTitle(String documentTitle);

	/**
	 * Ottiene il tipo documento tramite document title.
	 * 
	 * @param documentTitle
	 * @return id del tipo documento
	 */
	Integer getTipoDocumentoByDocumentTitle(String documentTitle);

	/**
	 * Ottiene il documento tramite guid.
	 * 
	 * @param guid
	 * @return documento
	 */
	Document getDocumentByGuid(String guid);

	/**
	 * Ottiene il documento tramite guid.
	 * 
	 * @param guid
	 * @param selectList
	 * @return documento
	 */
	Document getDocumentByGuid(String guid, String[] selectList);

	/**
	 * Ottiene il numero di versioni del documento.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @return numero di versioni del documento
	 */
	int getNumVersions(Integer documentTitle, Long idAoo);

	/**
	 * Ottiene il fascicolo.
	 * 
	 * @param idFascicolo
	 * @param idAoo
	 * @return fascicolo
	 */
	Document getFascicolo(String idFascicolo, Integer idAoo);

	/**
	 * Ottiene il fascicolo tarmite l'id del fascicolo FEPA.
	 * 
	 * @param idFascicoloFepa
	 * @return fascicolo
	 */
	Document getFascicoloByIdFascicoloFepa(String idFascicoloFepa);

	/**
	 * Ottiene i documenti contenuti nel fascicolo.
	 * 
	 * @param idfascicolo
	 * @param idAoo
	 * @return DocumentSet dei documenti
	 */
	DocumentSet getDocumentiFascicolo(Integer idfascicolo, Long idAoo);

	/**
	 * Ottiene i documenti red contenuti nel fascicolo.
	 * 
	 * @param idfascicolo
	 * @param idAoo
	 * @return DocumentSet dei documenti red
	 */
	Collection<DocumentoFascicoloDTO> getOnlyDocumentiRedFascicolo(Integer idfascicolo, Long idAoo);

	/**
	 * Ottiene i documenti red contenuti nel fascicolo allegabili.
	 * 
	 * @param fascicolo
	 * @param idAoo
	 * @param originale4DocEntrata
	 * @param withContent
	 * @return DocumentSet dei documenti red allegabili
	 */
	Collection<DocumentoAllegabileDTO> getOnlyDocumentiRedFascicoloAllegabili(FascicoloDTO fascicolo, Long idAoo,
			boolean originale4DocEntrata, boolean withContent);

	/**
	 * Ottiene le fatture contenute nel fascicolo.
	 * 
	 * @param idfascicolo
	 * @param idAoo
	 * @return fatture contenute nel fascicolo
	 */
	Collection<DetailFatturaFepaDTO> getOnlyFattureFromRedFascicolo(Integer idfascicolo, Long idAoo);

	/**
	 * Esegue la ricerca generica dei documenti.
	 * 
	 * @param key
	 * @param anno
	 * @param type
	 * @param idAoo
	 * @return DocumentSet dei documenti
	 */
	DocumentSet ricercaGenerica(String key, Integer anno, RicercaGenericaTypeEnum type, Long idAoo);

	/**
	 * Esegue la ricerca generica dei documenti.
	 * 
	 * @param valoreDaRicercare
	 * @param anno
	 * @param type
	 * @param utente
	 * @return DocumentSet dei documenti
	 */
	DocumentSet ricercaGenericaDocumenti(String valoreDaRicercare, Integer anno, RicercaGenericaTypeEnum type,
			UtenteDTO utente);

	/**
	 * Esegue la ricerca generica dei documenti.
	 * 
	 * @param key
	 * @param anno
	 * @param type
	 * @param idAoo
	 * @param onlyDocumentTitle
	 * @return DocumentSet dei documenti
	 */
	DocumentSet ricercaGenerica(String key, Integer anno, RicercaGenericaTypeEnum type, Long idAoo,
			Boolean onlyDocumentTitle);

	/**
	 * Ottiene la lista dei gruppi AD.
	 * 
	 * @return
	 */
	List<String> getGruppiAD();

	/**
	 * Effettua il check in del documento.
	 * 
	 * @param doc
	 * @param is
	 * @param isPreview
	 * @param isLibroFirma
	 * @param metadati
	 * @param fileName
	 * @param mimeType
	 * @param idAoo
	 */
	void checkin(Document doc, InputStream is, InputStream isPreview, InputStream isLibroFirma,
			Map<String, Object> metadati, String fileName, String mimeType, Long idAoo);

	/**
	 * Esegue il setting delle properties di un document.
	 * 
	 * @param document
	 * @param metadati
	 * @param saveDoc
	 */
	void setPropertiesAndSaveDocument(Document document, Map<String, Object> metadati, boolean saveDoc);

	/**
	 * Ottiene le versioni del documento per l'eliminazione dell'ultima versione.
	 * 
	 * @param document
	 * @param metadati
	 */
	void updateMetadataAndDeleteLastVersion(Document document, Map<String, Object> metadati);

	/**
	 * Controlla se è possibile eseguire una firma PADES su un documento FN.
	 * 
	 * @param guid
	 * @return esito del controllo
	 */
	Boolean canPades(String guid);

	/**
	 * Ottiene il DocumentSet FN degli allegati di un documento con content da
	 * firmare.
	 * 
	 * @param guid
	 * @return DocumentSet degli allegati
	 */
	DocumentSet getAllegatiConContentDaFirmare(String guid);

	/**
	 * Ottiene il DocumentSet FN degli allegati di un documento con content da
	 * firmare o siglare.
	 * 
	 * @param guid
	 * @return DocumentSet degli allegati
	 */
	DocumentSet getAllegatiConContentDaFirmareOSiglare(String guid);

	/**
	 * Ottiene il DocumentSet FN degli allegati che non rientrano nei firmati
	 * stampigliati.
	 * 
	 * @param guid
	 * @return DocumentSet degli allegati
	 */
	DocumentSet allegatiTimbroProtocolloCheNonRientranoNeiFirmatiStampigliati(String guid);

	/**
	 * Ottiene il DocumentSet FN degli allegati di un documento con content.
	 * 
	 * @param guid
	 * @return DocumentSet degli allegati
	 */
	DocumentSet getAllegatiConContent(String guid);

	/**
	 * Ottiene il DocumentSet FN degli allegati di un documento con content.
	 * 
	 * @param guid
	 * @param idAllegati
	 * @return DocumentSet degli allegati
	 */
	DocumentSet getAllegatiConContent(String guid, List<String> idAllegati);

	/**
	 * Ottiene il DocumentSet FN degli allegati di un documento.
	 * 
	 * @param guid
	 * @param bDaFirmare
	 * @param bWithContent
	 * @return DocumentSet degli allegati
	 */
	DocumentSet getAllegati(String guid, Boolean bDaFirmare, Boolean bWithContent);

	/**
	 * Ottiene il DocumentSet FN degli allegati di un documento.
	 * 
	 * @param guid
	 * @param bDaFirmare
	 * @param bWithContent
	 * @param unsupportedMimeTypes
	 * @param idAllegati
	 * @return DocumentSet degli allegati
	 */
	DocumentSet getAllegati(String guid, Boolean bDaFirmare, Boolean bWithContent, List<String> unsupportedMimeTypes,
			List<String> idAllegati);

	/**
	 * Ottiene il DocumentSet FN degli allegati di un documento.
	 * 
	 * @param guid
	 * @param bDaFirmare
	 * @param bWithContent
	 * @param unsupportedMimeTypes
	 * @param idAllegati
	 * @param classNameExt
	 * @return DocumentSet degli allegati
	 */
	DocumentSet getAllegati(String guid, Boolean bDaFirmare, Boolean bWithContent, List<String> unsupportedMimeTypes,
			List<String> idAllegati, String classNameExt);

	/**
	 * Ottiene il DocumentSet FN degli allegati di un documento.
	 * 
	 * @param guid
	 * @param bDaFirmare
	 * @param bWithContent
	 * @param unsupportedMimeTypes
	 * @param idAllegati
	 * @param classNameExt
	 * @param stampigliaturaSigla
	 * @return DocumentSet degli allegati
	 */
	DocumentSet getAllegati(String guid, Boolean bDaFirmare, Boolean bWithContent, List<String> unsupportedMimeTypes,
			List<String> idAllegati, String classNameExt, Boolean stampigliaturaSigla);

	/**
	 * Ottiene il DocumentSet FN degli allegati che non rientrano nei firmati
	 * stampigliati.
	 * 
	 * @param guid
	 * @param bWithContent
	 * @param unsupportedMimeTypes
	 * @param idAllegati
	 * @param classNameExt
	 * @return DocumentSet degli allegati
	 */
	DocumentSet allegatiTimbroProtocolloCheNonRientranoNeiFirmatiStampigliati(String guid, Boolean bWithContent,
			List<String> unsupportedMimeTypes, List<String> idAllegati, String classNameExt);

	/**
	 * Ottiene gli allegati allegabili.
	 * 
	 * @param fascicolo
	 * @param idDocumento
	 * @param noContent
	 * @param idAoo
	 * @param originale4DocEntrata
	 * @param withContent
	 * @return documenti allegabili
	 */
	Collection<DocumentoAllegabileDTO> getAllegatiAllegabili(FascicoloDTO fascicolo, String idDocumento,
			boolean noContent, Long idAoo, boolean originale4DocEntrata, boolean withContent);

	/**
	 * Verifica se il documento relativo al documentTitle in input presenta il
	 * content.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @return true o false
	 */
	boolean hasDocumentContentTransfer(String documentTitle, int idAoo);

	/**
	 * Recupera lo short name dell'AD Group.
	 * 
	 * @param idNodo
	 * @return short name dell'AD Group
	 */
	String getADGroupNameFormId(String idNodo);

	/**
	 * Ottiene il documento tramite l'id.
	 * 
	 * @param id
	 * @param selectList
	 * @param pf
	 * @param idAoo
	 * @param searchCondition
	 * @param inClassName
	 * @return documento
	 */
	Document getDocumentByIdGestionale(String id, List<String> selectList, PropertyFilter pf, Integer idAoo,
			String searchCondition, String inClassName);

	/**
	 * Ottiene i fascicoli del documento.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @return lista di fascicoli
	 */
	List<FascicoloDTO> getFascicoliDocumento(String documentTitle, Integer idAoo);

	/**
	 * Ottiene i fascicoli del documento per metodo allacci.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @return lista di fascicoli
	 */
	List<FascicoloDTO> getFascicoliDocumentoPerMetodoAllacci(String documentTitle, Integer idAoo);

	/**
	 * Ottiene i fascicoli del documento.
	 * 
	 * @param docFilenet
	 * @param idAoo
	 * @return lista di fascicoli
	 */
	List<FascicoloDTO> getFascicoliDocumento(Document docFilenet, Integer idAoo);

	/**
	 * Ottiene una lista di documenti grezzi che contengono i fascicoli.
	 * 
	 * @param foldersFiledIn
	 * @param idAoo
	 * @return lista di documenti
	 */
	List<Document> getFascicoliByFoldersFiledIn(FolderSet foldersFiledIn, Integer idAoo);

	/**
	 * Aggiorna le security del fascicolo.
	 * 
	 * @param idFascicolo
	 * @param idAoo
	 * @param securities
	 */
	void updateFascicoloSecurity(String idFascicolo, Long idAoo, List<SecurityDTO> securities);

	/**
	 * Aggiorna le security del fascicolo.
	 * 
	 * @param idFascicolo
	 * @param idAoo
	 * @param acl
	 */
	void updateFascicoloSecurity(String idFascicolo, Integer idAoo, AccessPermissionList acl);

	/**
	 * Aggiorna i metadati del fascicolo.
	 * 
	 * @param idFascicolo
	 * @param idAoo
	 * @param classeDocumentale
	 * @param metadati
	 */
	void updateFascicoloMetadati(String idFascicolo, Long idAoo, String classeDocumentale,
			Map<String, Object> metadati);

	/**
	 * Aggiorna il fascicolo.
	 * 
	 * @param idFascicolo
	 * @param idAoo
	 * @param securities
	 * @param classeDocumentale
	 * @param metadati
	 */
	void updateFascicolo(String idFascicolo, Long idAoo, List<SecurityDTO> securities, String classeDocumentale,
			Map<String, Object> metadati);

	/**
	 * Recupera la Folder FN dato l'id del fascicolo.
	 * 
	 * @param idfascicolo
	 * @param idAoo
	 * @return folder
	 */
	Folder getFolderFascicolo(String idfascicolo, Integer idAoo);

	/**
	 * Aggiunge delle security alle ACL.
	 * 
	 * @param acl
	 * @param securities
	 * @param inheritableDepth
	 */
	void addSecurity(AccessPermissionList acl, List<SecurityDTO> securities, int inheritableDepth);

	/**
	 * Ottiene i permessi di amministratore.
	 * 
	 * @return permessi di accesso
	 */
	AccessPermission getAdminPermission();

	/**
	 * Metodo per il passaggio da guid ad id.
	 * 
	 * @param guid
	 * @return id del documento
	 */
	Id idFromGuid(String guid);

	/**
	 * Ottiene gli allegati di un documento tramite id FN.
	 * 
	 * @param id
	 * @param selectList
	 * @return DocumentSet degli allegati
	 */
	DocumentSet getAllegatiByGuid(Id id, List<String> selectList);

	/**
	 * Ottiene i figli da guid.
	 * 
	 * @param id
	 * @param selectList
	 * @param inClassName
	 * @param pageSize
	 * @param inFilter
	 * @return DocumentSet
	 */
	DocumentSet getChildsByGuid(Id id, List<String> selectList, String inClassName, Integer pageSize, String inFilter);

	/**
	 * Esegue l'esecuzione di un batch FN.
	 * 
	 * @param objects
	 */
	void batchSave(List<IndependentlyPersistableObject> objects);

	/**
	 * Ottiene l'AD tramite username.
	 * 
	 * @param inUsername
	 * @return AD
	 */
	String getADUsernameName(String inUsername);

	/**
	 * Ottiene il documento per la modifica dell'iter tramite id.
	 * 
	 * @param idDocumento
	 * @return documento contenente i metadati necessari all'esecuzione della
	 *         modifica dell'iter
	 */
	Document getDocumentForModificaIter(Integer idDocumento);

	/**
	 * Aggiorna i metadati.
	 * 
	 * @param guid
	 * @param metadati
	 */
	void updateMetadati(String guid, Map<String, Object> metadati);

	/**
	 * Aggiorna i metadati.
	 * 
	 * @param doc
	 * @param metadati
	 */
	void updateMetadati(Document doc, Map<String, Object> metadati);

	/**
	 * Aggiorna i metadati del documento senza creare una nuova versione del
	 * documento.
	 * 
	 * @param idAoo
	 * @param idDocumento
	 * @param metadati
	 */
	void updateMetadatiOnCurrentVersion(Long idAoo, Integer idDocumento, Map<String, Object> metadati);

	/**
	 * Controlla il barcode.
	 * 
	 * @param barcode
	 * @return id del documento
	 */
	String controllaBarcode(String barcode);

	/**
	 * Controlla il barcode.
	 * 
	 * @param barcode
	 * @param className
	 * @return id del documento
	 */
	String controllaBarcode(String barcode, String className);

	/**
	 * Aggiorna il metadato firmaPDF che guida la tipologia di firma Pades/Cades.
	 * 
	 * @param guid
	 */
	void updateFirmaPDF(String guid);

	/**
	 * Ottiene il faldone tramite guid del fascicolo.
	 * 
	 * @param fascicoloGuid
	 * @param idAoo
	 * @return primo documento del faldone
	 */
	Document getFaldoneByFascicoloGuid(String fascicoloGuid, Integer idAoo);

	/**
	 * Crea il documento.
	 * 
	 * @param docRedFn
	 * @param fascicoloRedFn
	 * @param guidMail
	 * @param idAoo
	 * @return documento
	 */
	Document creaDocumento(DocumentoRedFnDTO docRedFn, FascicoloRedFnDTO fascicoloRedFn, String guidMail, Long idAoo);

	/**
	 * Crea il fascicolo.
	 * 
	 * @param fascicolo
	 * @param docFascicolazione
	 * @param docFilenet
	 * @param inUb
	 */
	void creaFascicolo(FascicoloRedFnDTO fascicolo, DocumentoFascicolazioneRedFnDTO docFascicolazione,
			Document docFilenet, UpdatingBatch inUb);

	/**
	 * Elimina il fascicolo se vuoto.
	 * 
	 * @param idFascicolo
	 * @param idAoo
	 * @return true o false
	 */
	boolean eliminaFascicoloSeVuoto(String idFascicolo, Long idAoo);

	/**
	 * Crea annotation del tipo dato per il documento.
	 * 
	 * @param idDocumento
	 * @param text
	 * @param annotationType
	 * @param idAoo
	 */
	void creaAnnotation(String idDocumento, String text, FilenetAnnotationEnum annotationType, Long idAoo);

	/**
	 * Crea annotation del tipo dato per il documento.
	 * 
	 * @param inDoc
	 * @param text
	 * @param annotationType
	 */
	void creaAnnotation(Document inDoc, String text, FilenetAnnotationEnum annotationType);

	/**
	 * Fascicola il documento.
	 * 
	 * @param idFascicolo
	 * @param docFascicolazione
	 * @param idAoo
	 */
	void fascicolaDocumento(String idFascicolo, DocumentoFascicolazioneRedFnDTO docFascicolazione, Long idAoo);

	/**
	 * Rimuove il documento dal fascicolo.
	 * 
	 * @param idFascicolo
	 * @param docFascicolazione
	 * @param idAoo
	 */
	void rimuoviDocumentoFascicolo(String idFascicolo, DocumentoFascicolazioneRedFnDTO docFascicolazione, Long idAoo);

	/**
	 * Copia il documento nel fascicolo.
	 * 
	 * @param idFascicolo
	 * @param docFascicolazione
	 * @param idAoo
	 */
	void copiaDocumentoFascicolo(String idFascicolo, DocumentoFascicolazioneRedFnDTO docFascicolazione, Long idAoo);

	/**
	 * Ottiene le annotations per il documento.
	 * 
	 * @param idDocumento
	 * @param allegato
	 * @param tipoAnnotazione
	 * @param idAoo
	 * @param onlyFirst
	 * @return lista di annotations
	 */
	List<AnnotationDTO> getAnnotationsDocument(String idDocumento, boolean allegato,
			FilenetAnnotationEnum tipoAnnotazione, Long idAoo, boolean onlyFirst);

	/**
	 * Ottiene le annotations per il content del documento.
	 * 
	 * @param idDocumento
	 * @param allegato
	 * @param tipoAnnotazione
	 * @param idAoo
	 * @return
	 */
	AnnotationDTO getAnnotationDocumentContent(String idDocumento, boolean allegato,
			FilenetAnnotationEnum tipoAnnotazione, Long idAoo);

	/**
	 * Ottiene le annotations per il content del documento.
	 * 
	 * @param idDocumento
	 * @param tipoAnnotazione
	 * @param idAoo
	 * @return
	 */
	AnnotationDTO getAnnotationDocumentContent(String idDocumento, FilenetAnnotationEnum tipoAnnotazione, Long idAoo);

	/**
	 * Ripristina la versione precedente del documento.
	 * 
	 * @param inDocument
	 */
	void eliminaUltimaVersione(Document inDocument);

	/**
	 * Elimina la versione del documento tramite guid.
	 * 
	 * @param guid
	 */
	void eliminaVersioneDocumentoByGuid(String guid);

	/**
	 * Elimina il documento tramite document title.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 */
	void eliminaDocumentoByDocumentTitle(String documentTitle, Long idAoo);

	/**
	 * Elimina il documento.
	 * 
	 * @param guid
	 */
	void eliminaDocumento(String guid);

	/**
	 * Elimina il documento.
	 * 
	 * @param document
	 */
	void eliminaDocumento(Document document);

	/**
	 * Elimina la mail.
	 * 
	 * @param guid
	 */
	void eliminaMail(String guid);

	/**
	 * Elimina gli allegati tramite guid.
	 * 
	 * @param guid
	 * @param allegatiDaEliminareGuid
	 * @return
	 */
	List<Document> deleteAllegatiByGuid(String guid, List<String> allegatiDaEliminareGuid);

	/**
	 * Elimina le annotations per il documento.
	 * 
	 * @param idDocumento
	 * @param tipoAnnotazione
	 * @param idAoo
	 */
	void eliminaAnnotationDocumento(String idDocumento, FilenetAnnotationEnum tipoAnnotazione, Long idAoo);

	/**
	 * Verifica che il folder [folderName] sia nel folder [rootFolder].
	 * 
	 * @param rootFolder
	 * @param folderName
	 * @return true o false
	 */
	boolean checkFolder(String rootFolder, String folderName);

	/**
	 * Crea un sub folder.
	 * 
	 * @param folderRoot
	 * @param folderName
	 * @return sub folder
	 */
	Folder createSubFolder(String folderRoot, String folderName);

	/**
	 * Crea folders da root.
	 * 
	 * @param folder
	 * @return folders
	 */
	FolderSet getFoldersByRoot(String folder);

	/**
	 * Popola i metadati di inoltra/rifiuta.
	 * 
	 * @param messaggio
	 * @param indirizzoEmail
	 * @return mappa property oggetto
	 */
	Map<String, Object> populateMetadatiEmailInoltraRifiuta(MessaggioEmailDTO messaggio, String indirizzoEmail);

	/**
	 * Crea i figli del compound document.
	 * 
	 * @param doc
	 * @param allegati
	 * @param tipiFile
	 */
	void setChildCompoundDocument(Document doc, List<AllegatoDTO> allegati, Collection<TipoFile> tipiFile);

	/**
	 * Crea un documento di classe "Email" su FileNet.
	 * 
	 * @param <T>
	 * @param messaggio
	 * @param indirizzoCasellaPostale
	 * @param documentFile
	 * @param tipiFile
	 * @return documento
	 */
	<T extends MessaggioEmailDTO> Document createMailAsDocument(T messaggio, String indirizzoCasellaPostale,
			FileDTO documentFile, Collection<TipoFile> tipiFile);

	/**
	 * Ottiene la mail come documento.
	 * 
	 * @param messaggio
	 * @return mail
	 */
	Document getMailAsDocument(MessaggioEmailDTO messaggio);

	/**
	 * Aggiorna i dati del protocollo per il documento.
	 * 
	 * @param idUtente
	 * @param idUfficio
	 * @param idAoo
	 * @param tipoProtocollo
	 * @param numeroProtocollo
	 * @param idProtocollo
	 * @param oggettoProtocollo
	 * @param descrizioneTipologiaDocumento
	 * @param annoProtocollo
	 * @param dataProtocollo
	 * @param documentTitle
	 * @param tipologiaProtocollazione
	 */
	void aggiornaDocumentoProtocollo(Long idUtente, Long idUfficio, Long idAoo, int tipoProtocollo,
			int numeroProtocollo, String idProtocollo, String oggettoProtocollo, String descrizioneTipologiaDocumento,
			String annoProtocollo, String dataProtocollo, String documentTitle, Long tipologiaProtocollazione);

	/**
	 * Aggiorna l'elenco di trasmissione.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @param destinatari
	 */
	void aggiornaElencoTrasmisione(String documentTitle, int idAoo, String destinatari);

	/**
	 * Verifica gli ordini di pagamento per il documento.
	 * 
	 * @param docuementTitle
	 * @param idAoo
	 * @return true o false
	 */
	Boolean verifyFascicoloFepaDisponibilePerOPCollegati(String docuementTitle, Long idAoo);

	/**
	 * Ottiene tutti gli ids degli ordini di pagamento.
	 * 
	 * @param decreto
	 * @return lista di ids degli ordini di pagamento
	 */
	List<String> getAllOPId(Document decreto);

	/**
	 * Ottiene gli ids degli ordini di pagamento tramite allegato.
	 * 
	 * @param allegato
	 * @return lista di ids degli ordini di pagamento
	 */
	List<String> getOPIdsAllegato(Document allegato);

	/**
	 * Ottiene i documenti.
	 * 
	 * @param idAoo
	 * @param searchCondition
	 * @param className
	 * @param currentVersion
	 * @param setCap
	 * @return DocumentSet dei documenti
	 */
	DocumentSet getDocuments(Integer idAoo, String searchCondition, String className, boolean currentVersion, boolean setCap);

	/**
	 * Ottiene i contributi attivi per il documento.
	 * 
	 * @param numeroDocumento
	 * @param annoDocumento
	 * @param idAoo
	 * @return DocumentSet dei contributi attivi
	 */
	DocumentSet getContributiAttivi(Integer numeroDocumento, Integer annoDocumento, Long idAoo);

	/**
	 * Associa il fascicolo al titolario.
	 * 
	 * @param idFascicolo
	 * @param titolarioDestinazione
	 * @param idAoo
	 */
	void associaFascicoloATitolario(String idFascicolo, String titolarioDestinazione, Long idAoo);

	/**
	 * Ottiene i faldoni in cui si trova il fascicolo.
	 * 
	 * @param idFascicolo
	 * @param idAoo
	 * @return DocumentSet dei faldoni del fascicolo
	 */
	DocumentSet getFaldoniFascicolo(String idFascicolo, Long idAoo);

	/**
	 * Associa il fascicolo al faldone.
	 * 
	 * @param idFascicolo
	 * @param nomeFaldone
	 * @param idAoo
	 */
	void associaFascicoloAFaldone(String idFascicolo, String nomeFaldone, Long idAoo);

	/**
	 * Rimuove l'associazione del fascicolo al faldone.
	 * 
	 * @param idFascicolo
	 * @param nomeFaldone
	 * @param idAoo
	 */
	void disassociaFascicoloDaFaldone(String idFascicolo, String nomeFaldone, Long idAoo);

	/**
	 * Ottiene il folder del faldone.
	 * 
	 * @param nomeFaldone
	 * @param idAoo
	 * @return folder del faldone
	 */
	Folder getFolderFaldone(String nomeFaldone, Long idAoo);

	/**
	 * Ottiene l'ultima versione editabile del documento.
	 * 
	 * @param idDocumento
	 * @param idAoo
	 * @return documento
	 */
	Document getLastEditableVersion(String idDocumento, Integer idAoo);

	/**
	 * Cancella la versione tramite id.
	 * 
	 * @param id
	 */
	void deleteVersion(Id id);

	/**
	 * Controlla se la mail ha file allegati con estensione supportata.
	 * 
	 * @param email
	 * @param allowedFileExtensions
	 * @return true o false
	 */
	boolean hasSupportedFileExtensionsAttachment(Document email, List<String> allowedFileExtensions);

	/**
	 * Sovrascrive la security.
	 * 
	 * @param inputAcl
	 * @param newAcl
	 */
	void sovrascriviSecurity(AccessPermissionList inputAcl, AccessPermissionList newAcl);

	/**
	 * Controlla se il documento possiede un'annotation Preview.
	 * 
	 * @param idDocumento
	 * @param idAoo
	 * @return true o false
	 */
	boolean isDocumentoConPreview(String idDocumento, Long idAoo);

	/**
	 * Crea un'annotation a partire da un file che non è il content del documento.
	 * 
	 * @param idDocumento
	 * @param guid
	 * @param annotationType
	 * @param contentPreview
	 * @param creazione
	 * @param idAoo
	 */
	void creaAnnotation(String idDocumento, String guid, FilenetAnnotationEnum annotationType,
			InputStream contentPreview, boolean creazione, Long idAoo);

	/**
	 * Verifica il protocollo.
	 * 
	 * @param classiDocumentali
	 * @param annoProtocollo
	 * @param numProtocollo
	 * @param idProtocollo
	 * @param tipoProtocollo
	 * @return primo documento
	 */
	Document verificaProtocollo(List<String> classiDocumentali, int annoProtocollo, int numProtocollo,
			String idProtocollo, int tipoProtocollo);

	/**
	 * Cambia la classe documentale.
	 * 
	 * @param idDocumento
	 * @param newClasseDocumentale
	 * @param newMetadatiDocumento
	 * @param newDataHandler
	 * @param idAoo
	 * @return documento
	 */
	Document cambiaClasseDocumentale(String idDocumento, String newClasseDocumentale,
			Map<String, Object> newMetadatiDocumento, DataHandler newDataHandler, Long idAoo);

	/**
	 * Esegue la ricerca generica nei fascicoli.
	 * 
	 * @param inValoreDaRicercare
	 * @param type
	 * @param idAoo
	 * @param anno
	 * @return DocumentSet dei documenti ricercati
	 */
	DocumentSet ricercaGenericaFascicoli(String inValoreDaRicercare, RicercaGenericaTypeEnum type, Long idAoo,
			String anno);

	/**
	 * Ottiene i documenti tramite lista di ids.
	 * 
	 * @param idDocumenti
	 * @return mappa document title dettaglio del documento
	 */
	Map<String, DetailDocumentRedDTO> getDocumentiByIdList(List<String> idDocumenti);

	/**
	 * Ottiene la prima versione degli allegati.
	 * 
	 * @param documentTitle
	 * @return prima versione degli allegati
	 */
	Collection<Document> getAllegatiConContentFirstVersion(String documentTitle);

	/**
	 * Ottiene la prima versione degli allegati.
	 * 
	 * @param d
	 * @return prima versione degli allegati
	 */
	Collection<Document> getAllegatiConContentFirstVersion(Document d);

	/**
	 * Ottiene il documento dal CE tramite id applicativo.
	 * 
	 * @param iddocument
	 * @param idAoo
	 * @return dettaglio del documento
	 */
	DetailDocumentRedDTO getDocumentFromCEByIdApplicativo(String iddocument, int idAoo);

	/**
	 * Esegue la ricerca generica nei faldoni.
	 * 
	 * @param inValoreDaRicercare
	 * @param type
	 * @param idAoo
	 * @param anno
	 * @param idUfficioUtente
	 * @param idUfficioCorriereUtente
	 * @param idUtente
	 * @param utenteUsername
	 * @param visFaldoni
	 * @param onlyOggetto
	 * @return DocumentSet dei documenti ricercati
	 */
	DocumentSet ricercaGenericaFaldoni(String inValoreDaRicercare, RicercaGenericaTypeEnum type, Long idAoo,
			String anno, Long idUfficioUtente, Integer idUfficioCorriereUtente, Long idUtente, String utenteUsername,
			String visFaldoni, boolean onlyOggetto);

	/**
	 * Ottiene le notifiche PEC per il documento.
	 * 
	 * @param idDocumento
	 * @param mittente
	 * @param path
	 * @param aoo
	 * @param isTabSpedizione
	 * @return mappa mittente mail
	 */
	Map<String, EmailDTO> getNotifichePEC(String idDocumento, String mittente, String path, Aoo aoo,
			boolean isTabSpedizione);

	/**
	 * Ottiene le notifiche di interoperabilità per il protocollo.
	 * 
	 * @param idProtocollo
	 * @param numeroProtocollo
	 * @param annoProtocollo
	 * @return DocumentSet delle notifiche di interoperabilità
	 */
	DocumentSet getNotificheInteroperabilitaByProtocollo(String idProtocollo, int numeroProtocollo, int annoProtocollo);

	/**
	 * Conta il numero di fascicoli non classificati.
	 * 
	 * @param utente
	 * @param idUfficioCorriereUtente
	 * @param gruppiAD
	 * @param aclCorriere
	 * @param aclNodo
	 * @param aclUtente
	 * @return numero di fascicoli non classificati
	 */
	Integer getCountFascicoliNonClassificati(UtenteDTO utente, Integer idUfficioCorriereUtente, List<String> gruppiAD,
			Boolean aclCorriere, Boolean aclNodo, Boolean aclUtente);

	/**
	 * Ottiene i fascicoli non classificati.
	 * 
	 * @param utente
	 * @param idUfficioCorriereUtente
	 * @param gruppiAD
	 * @param aclCorriere
	 * @param aclNodo
	 * @param aclUtente
	 * @return DocumentSet dei fascicoli non classificati
	 */
	DocumentSet getFascicoliNonClassificati(UtenteDTO utente, Integer idUfficioCorriereUtente, List<String> gruppiAD,
			Boolean aclCorriere, Boolean aclNodo, Boolean aclUtente);

	/**
	 * Ottiene i fascicoli tramite indice.
	 * 
	 * @param utente
	 * @param idUfficioCorriereUtente
	 * @param gruppiAD
	 * @param aclCorriere
	 * @param aclNodo
	 * @param aclUtente
	 * @param indiceDiClassificazione
	 * @return DocumentSet dei fascicoli
	 */
	DocumentSet getFascicoliByIndice(UtenteDTO utente, Integer idUfficioCorriereUtente, List<String> gruppiAD,
			Boolean aclCorriere, Boolean aclNodo, Boolean aclUtente, String indiceDiClassificazione);

	/**
	 * Restringe la visibilità.
	 * 
	 * @param idUtente
	 * @param idUfficioCorriereUtente
	 * @param idUfficioUtente
	 * @param idUfficioSelezionato
	 * @param idUfficioCorriereSelezionato
	 * @param username
	 * @param gruppiAD
	 * @param aclCorriere
	 * @param aclNodo
	 * @param aclUtente
	 * @param visFaldoni
	 * @return cono di visibilità
	 */
	List<String> restrictVisibility(Long idUtente, Integer idUfficioCorriereUtente, Long idUfficioUtente,
			Integer idUfficioSelezionato, Integer idUfficioCorriereSelezionato, String username, List<String> gruppiAD,
			boolean aclCorriere, boolean aclNodo, boolean aclUtente, String visFaldoni);

	/**
	 * Ottiene i fascicoli tramite il faldone.
	 * 
	 * @param pathFaldone
	 * @param gruppiAD
	 * @return RepositoryRowSet
	 */
	RepositoryRowSet getFascicoliByFaldone(String pathFaldone, List<String> gruppiAD);

	/**
	 * Elimina il faldone.
	 * 
	 * @param idAoo
	 * @param documentTitle
	 */
	void deleteFaldone(Long idAoo, String documentTitle);

	/**
	 * Ottiene il faldone tramite document title.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @return documento
	 */
	Document getFaldoneByDocumentTitle(String documentTitle, Integer idAoo);

	/**
	 * Ottiene i faldoni figli.
	 * 
	 * @param utente
	 * @param documentTitle
	 * @param bAooVisTotale
	 * @param idUfficioCorriereUtente
	 * @param idUfficioSelezionato
	 * @param idUfficioCorriereSelezionato
	 * @param gruppiAD
	 * @param aclCorriere
	 * @param aclNodo
	 * @param aclUtente
	 * @return DocumentSet dei faldoni figli
	 */
	DocumentSet getFaldoneChildren(UtenteDTO utente, String documentTitle, Boolean bAooVisTotale,
			Integer idUfficioCorriereUtente, Integer idUfficioSelezionato, Integer idUfficioCorriereSelezionato,
			List<String> gruppiAD, Boolean aclCorriere, Boolean aclNodo, Boolean aclUtente);

	/**
	 * Crea il faldone.
	 * 
	 * @param idAoo
	 * @param idUfficio
	 * @param nomeFaldone
	 * @param parentFaldone
	 * @param oggetto
	 * @param descrizione
	 */
	void createFaldone(Integer idAoo, Integer idUfficio, String nomeFaldone, String parentFaldone, String oggetto,
			String descrizione);

	/**
	 * Aggiorna i metadati del faldone.
	 * 
	 * @param idAoo
	 * @param documentTitle
	 * @param metadati
	 */
	void updateMetadatiFaldone(Long idAoo, String documentTitle, Map<String, Object> metadati);

	/**
	 * Inserisce gli allegati.
	 * 
	 * @param idDocumento
	 * @param allegati
	 * @param idAoo
	 */
	void inserisciAllegati(String idDocumento, List<DocumentoRedFnDTO> allegati, Long idAoo);

	/**
	 * Inserisce gli allegati.
	 * 
	 * @param documentoPrincipale
	 * @param allegati
	 */
	void inserisciAllegati(Document documentoPrincipale, List<DocumentoRedFnDTO> allegati);

	/**
	 * Inserisce gli allegati all'ordine di pagamento.
	 * 
	 * @param documentoPrincipale
	 * @param allegato
	 */
	void inserisciAllegatoOP(Document documentoPrincipale, DocumentoRedFnDTO allegato);

	/**
	 * Aggiorna l'allegato.
	 * 
	 * @param docPrincipale
	 * @param allegato
	 * @param contentAllegato
	 * @param impostaMetadati
	 * @param idAoo
	 */
	void aggiornaAllegato(DocumentoRedFnDTO docPrincipale, DocumentoRedFnDTO allegato, InputStream contentAllegato,
			boolean impostaMetadati, Long idAoo);

	/**
	 * Aggiorna l'allegato.
	 * 
	 * @param docPrincipaleFilenet
	 * @param allegato
	 * @param contentAllegato
	 * @param impostaMetadati
	 * @param idAoo
	 */
	void aggiornaAllegato(Document docPrincipaleFilenet, DocumentoRedFnDTO allegato, InputStream contentAllegato,
			boolean impostaMetadati, Long idAoo);

	/**
	 * Ottiene l'allegato dal documento tramite document title
	 * 
	 * @param guidDocumentoPrincipale
	 * @param docTitleAllegato
	 * @return allegato
	 */
	Document getAllegatoFromDocumentByDocumentTitle(String guidDocumentoPrincipale, String docTitleAllegato);

	/**
	 * Versiona l'allegato.
	 * 
	 * @param document
	 * @param content
	 * @param mimeType
	 * @param metadati
	 * @param impostaMetadati
	 * @param idAoo
	 */
	void addVersionCompoundDocument(Document document, InputStream content, String mimeType,
			Map<String, Object> metadati, boolean impostaMetadati, Long idAoo);

	/**
	 * Cancella il check out del documento.
	 * 
	 * @param doc
	 */
	void cancelCheckout(Document doc);

	/**
	 * Verifica il check out del documento.
	 * 
	 * @param doc
	 * @return true o false
	 */
	boolean isCheckOut(Document doc);

	/**
	 * Elimina gli allegati tramite guid.
	 * 
	 * @param guid
	 * @param allegatiGuid
	 * @return lista di allegati
	 */
	List<Document> eliminaAllegatiByGuid(String guid, List<String> allegatiGuid);

	/**
	 * Elimina gli allegati tramite guid.
	 * 
	 * @param documentoPrincipale
	 * @param allegatiDaEliminareGuid
	 * @return lista di allegati
	 */
	List<Document> eliminaAllegatiByGuid(Document documentoPrincipale, List<String> allegatiDaEliminareGuid);

	/**
	 * Ottiene il nome della versione precedente del file.
	 * 
	 * @param guid
	 * @return nome del file
	 */
	String getPreviousVersionWordFileName(String guid);

	/**
	 * Ottiene il numero di revisione della versione precedente.
	 * 
	 * @param guid
	 * @return numero di revisione
	 */
	Integer getPreviousVersionWordRevisionNumber(String guid);

	/**
	 * Ottiene l'ultima versione editabile.
	 * 
	 * @param guid
	 * @return documento
	 */
	Document getLastEditableVersion(String guid);

	/**
	 * Recupera tutte le proprietà della classe dato il nome della classe
	 * documentale.
	 * 
	 * @param className
	 * @param withSystemProperties
	 * @return lista delle proprietà della classe
	 */
	List<PropertyDefinition> getClassProperties(String className, boolean withSystemProperties);

	/**
	 * Associa l'allegato all'ordine di pagamento.
	 * 
	 * @param decreto
	 * @param allegatoDecreto
	 */
	void associaAllegatoAOrdineDiPagamento(Document decreto, Document allegatoDecreto);

	/**
	 * Ottiene l'id documento dal matadato FEPA.
	 * 
	 * @param key
	 * @param value
	 * @return id documento
	 */
	String getIdDocumentoByMetadatoFepa(String key, String value);

	/**
	 * Ottiene i permessi di default.
	 * 
	 * @param className
	 * @return lista di permessi di accesso
	 */
	AccessPermissionList getDefaultInstancePermissions(String className);

	/**
	 * Ottiene i fascicoli per la ricerca dei documenti.
	 * 
	 * @param nomeFascicolo
	 * @param titolario
	 * @param idAoo
	 * @return DocumentSet dei fascicoli
	 */
	DocumentSet getFascicoliPerRicercaDocumenti(String nomeFascicolo, String titolario, Long idAoo);

	/**
	 * Metodo per la ricerca avanzata dei documenti.
	 * 
	 * @param mappaValoriRicerca
	 * @param idDocumenti
	 * @param utente
	 * @param orderbyInQuery
	 * @param acl
	 * @param setCap
	 * @return DocumentSet dei documenti ricercati
	 */
	DocumentSet ricercaAvanzataDocumenti(Map<String, Object> mappaValoriRicerca, Set<String> idDocumenti,
			UtenteDTO utente, boolean orderbyInQuery, String acl, boolean setCap);

	/**
	 * Esegue la ricerca del testo degli allegati.
	 * 
	 * @param mappaValoriRicerca
	 * @param modalitaRicercaTestuale
	 * @return DocumentSet degli allegati
	 */
	DocumentSet ricercaAllegatiFullText(Map<String, Object> mappaValoriRicerca,
			ModalitaRicercaAvanzataTestoEnum modalitaRicercaTestuale);

	/**
	 * Metodo per la ricerca avanzata dei fascicoli.
	 * 
	 * @param mappaValoriRicerca
	 * @param idFascicoli
	 * @param utente
	 * @return DocumentSet dei fascicoli ricercati
	 */
	DocumentSet ricercaAvanzataFascicoli(Map<String, Object> mappaValoriRicerca, Set<String> idFascicoli,
			UtenteDTO utente);

	/**
	 * Metodo per la ricerca avanzata dei faldoni.
	 * 
	 * @param mappaValoriRicerca
	 * @param utente
	 * @param orderbyInQuery
	 * @return DocumentSet dei faldoni ricercati.
	 */
	DocumentSet ricercaAvanzataFaldoni(Map<String, Object> mappaValoriRicerca, UtenteDTO utente,
			boolean orderbyInQuery);

	/**
	 * Ottiene il fascicolo tramite l'oggetto.
	 * 
	 * @param oggettoFascicolo
	 * @param gruppiAD
	 * @return fascicolo
	 */
	Document getFascicoloByOggetto(String oggettoFascicolo, List<String> gruppiAD);

	/**
	 * Ottiene il faldone tramite il metadato.
	 * 
	 * @param chiaveMetadato
	 * @param valoreMetadato
	 * @param gruppiAD
	 * @return faldone
	 */
	Document getFaldoneByMetadato(String chiaveMetadato, String valoreMetadato, List<String> gruppiAD);

	/**
	 * Ottiene i faldoni figli tramite il nome del faldone padre.
	 * 
	 * @param nomeFaldonePadre
	 * @param gruppiAD
	 * @param idAoo
	 * @param idFaldonePartenza
	 * @return DocumentSet dei faldoni figli
	 */
	DocumentSet getFaldoniFigli(String nomeFaldonePadre, List<String> gruppiAD, Long idAoo, String idFaldonePartenza);

	/**
	 * Metodo per la ricerca SIGI.
	 * 
	 * @param mappaValoriRicerca
	 * @param utente
	 * @param orderbyInQuery
	 * @return DocumentSet dei documenti ricercati
	 */
	DocumentSet ricercaSigi(Map<String, Object> mappaValoriRicerca, UtenteDTO utente, boolean orderbyInQuery);

	/**
	 * Metodo per la ricerca del registro di protocollo.
	 * 
	 * @param mappaValoriRicerca
	 * @param utente
	 * @return DocumentSet dei documenti ricercati
	 */
	DocumentSet ricercaRegistroProtocollo(Map<String, Object> mappaValoriRicerca, UtenteDTO utente);

	/**
	 * Associa l'allegato all'ordine di pagamento.
	 * 
	 * @param decreto
	 * @param allegatoDecreto
	 */
	void associaAllegatoToOP(Document decreto, Document allegatoDecreto);

	/**
	 * Salva l'allegato all'ordine di pagamento.
	 * 
	 * @param op
	 * @param allegatoDecreto
	 */
	void salvaOPAllegato(CustomObject op, Document allegatoDecreto);

	/**
	 * Ottiene gli ordini di pagamento dall'allegato all'ordine di pagamento.
	 * 
	 * @param idAllegatoOP
	 * @return
	 */
	IndependentObjectSet getOrdiniPagamentoAllegatoOp(String idAllegatoOP);

	/**
	 * Inserisce l'allegato come documento.
	 * 
	 * @param allegatoToDoc
	 * @param permessi
	 * @return allegato
	 */
	Document inserisciAllegatoComeDocumento(DocumentoRedFnDTO allegatoToDoc, AccessPermissionList permessi);

	/**
	 * Ottiene la richiesta OPF.
	 * 
	 * @param numeroRichiesta
	 * @param annoRichiesta
	 * @param numeroProtocollo
	 * @return richiesta
	 */
	Document getRichiestaOPF(int numeroRichiesta, String annoRichiesta, int numeroProtocollo);

	/**
	 * Ottiene la mail associata al documento.
	 * 
	 * @param guid
	 * @return mail
	 */
	Document getMailAllegata(String guid);

	/**
	 * Esegue la ricerca generica per utente.
	 * 
	 * @param key
	 * @param anno
	 * @param type
	 * @param utente
	 * @param fullText
	 * @param orderbyInQuery
	 * @return DocumentSet dei documenti trovati
	 */
	DocumentSet ricercaGenericaUtente(String key, Integer anno, RicercaGenericaTypeEnum type, UtenteDTO utente,
			boolean fullText, boolean orderbyInQuery);

	/**
	 * Esegue la ricerca generica per utente.
	 * 
	 * @param key
	 * @param anno
	 * @param type
	 * @param utente
	 * @param campoRicerca
	 * @param fullText
	 * @param orderbyInQuery
	 * @param limitMaxResults
	 * @param withContent
	 * @return DocumentSet dei documenti trovati
	 */
	DocumentSet ricercaGenericaUtente(String key, Integer anno, RicercaGenericaTypeEnum type, UtenteDTO utente,
			CampoRicercaEnum campoRicerca, boolean fullText, boolean orderbyInQuery, boolean limitMaxResults,
			boolean withContent);

	/**
	 * Esegue la ricerca generica nei fascicoli.
	 * 
	 * @param key
	 * @param type
	 * @param anno
	 * @param idAoo
	 * @param orderbyInQuery
	 * @return DocumentSet dei documenti trovati
	 */
	DocumentSet ricercaGenericaFascicoliUtente(String key, RicercaGenericaTypeEnum type, Integer anno, Long idAoo,
			boolean orderbyInQuery);

	/**
	 * Copia il documento.
	 * 
	 * @param doc
	 * @param rootFolder
	 * @param ancheAllegati
	 * @param docTitle
	 * @return nuovo documento
	 */
	Document copyDocument(Document doc, Folder rootFolder, Boolean ancheAllegati, String docTitle);

	/**
	 * Copia la inbox del documento.
	 * 
	 * @param doc
	 * @param casellaPostale
	 * @param ancheAllegati
	 * @param docTitle
	 * @return nuovo documento
	 */
	Document copyDocumentInbox(Document doc, String casellaPostale, Boolean ancheAllegati, String docTitle);

	/**
	 * Ottiene gli allegati di un documento tramite id filenet.
	 * 
	 * @param guid
	 * @param selectList
	 * @return DocumentSet degli allegati.
	 */
	DocumentSet getAllegatiByGuid(String guid, List<String> selectList);

	/**
	 * Ottiene le mail come documenti.
	 * 
	 * @param path
	 * @param documentClass
	 * @param dataDa
	 * @param dataA
	 * @param searchFields
	 * @param selectedStatList
	 * @param oggettoRicerca
	 * @param mittenteRicerca
	 * @return DocumentSet di mail
	 */
	DocumentSet getMailsAsDocument(String path, String documentClass, Date dataDa, Date dataA,
			List<String> searchFields, List<StatoMailEnum> selectedStatList, String oggettoRicerca,
			String mittenteRicerca);

	/**
	 * Ottiene il testo delle mail come documenti.
	 * 
	 * @param pathRecenti
	 * @param pathArchiviate
	 * @param documentClass
	 * @param oggettoRicerca
	 * @param mittenteRicerca
	 * @param dataDa
	 * @param dataA
	 * @param searchFields
	 * @param selectedStatList
	 * @param numeroProtocolloRicerca
	 * @return DocumentSet di mail
	 */
	DocumentSet getMailAsDocumentFullText(String pathRecenti, String pathArchiviate, String documentClass,
			String oggettoRicerca, String mittenteRicerca, Date dataDa, Date dataA, List<String> searchFields,
			List<StatoMailEnum> selectedStatList, String numeroProtocolloRicerca);

	/**
	 * Conta il numero di documenti cartacei.
	 * 
	 * @param eliminati
	 * @param registroRiservato
	 * @return numero di documenti cartacei
	 */
	Integer getCountDocumentiCartacei(boolean eliminati, boolean registroRiservato);

	/**
	 * Ottiene i documenti cartacei.
	 * 
	 * @param eliminati
	 * @param registroRiservato
	 * @return lista di documenti
	 */
	List<Document> getDocumentiCartacei(boolean eliminati, boolean registroRiservato);

	/**
	 * Verifica se l'email è duplicata.
	 * 
	 * @param messageId
	 * @param indirizzoEmail
	 * @param from
	 * @return true o false
	 */
	boolean checkEmailDuplicata(String messageId, String indirizzoEmail, String from);

	/**
	 * Verifica se l'email è duplicata.
	 * 
	 * @param messageId
	 * @param indirizzoEmail
	 * @param from
	 * @param folder
	 * @param classeDocumentaleEmail
	 * @return true o false
	 */
	boolean checkEmailDuplicata(String messageId, String indirizzoEmail, String from, String folder,
			String classeDocumentaleEmail);

	/**
	 * Restituisce il documento di (classe documentale) spedizione associato alla
	 * mail identificata dall'id del messaggio.
	 * 
	 * @param messageId
	 * @return documento FileNet di spedizione
	 */
	Document getSpedizioneByMessageId(String messageId);

	/**
	 * Gestisce la spedizione per il destinatario.
	 * 
	 * @param messageId
	 * @param datiCert
	 */
	void gestisciSpedizioneDestinatario(String messageId, DatiCertDTO datiCert);

	/**
	 * Ottiene le properties relative all'ordine di pagamento.
	 * 
	 * @param idDocumento
	 * @return lista di properties
	 */
	List<Properties> fetchOPProperties(String idDocumento);

	/**
	 * Salva l'ordine di pagamento per il fascicolo FEPA.
	 * 
	 * @param numeroOP
	 * @param esercizio
	 * @param amministrazione
	 * @param ragioneria
	 * @param capitolo
	 * @param pianoGestione
	 * @param beneficiario
	 * @param tipoTitolo
	 * @param oggettoSpese
	 * @param dataEmissione
	 * @param idFascicolo
	 */
	void saveOPPerFascicoloFepa(String numeroOP, Integer esercizio, String amministrazione, String ragioneria,
			String capitolo, String pianoGestione, String beneficiario, String tipoTitolo, String oggettoSpese,
			Date dataEmissione, String idFascicolo);

	/**
	 * Ottiene l'elenco degli ordini di pagamento tramite il numero di decreto.
	 * 
	 * @param numeroDecreto
	 * @return elenco
	 */
	CustomObject fetchElencoOPByNumDecreto(String numeroDecreto);

	/**
	 * Ottiene il numero di decreto.
	 * 
	 * @param idDocumento
	 * @return numero di decreto
	 */
	String fetchNumeroDecreto(String idDocumento);

	/**
	 * Crea l'elenco degli ordini di pagamento.
	 * 
	 * @param numeroDecreto
	 * @return elenco
	 */
	CustomObject createElencoOP(String numeroDecreto);

	/**
	 * Salva il contatto.
	 * 
	 * @param objectIdCont
	 * @param listContatti
	 * @return contatto
	 */
	CustomObject salvaContatto(String objectIdCont, List<Contatto> listContatti);

	/**
	 * Ottiene i contatti.
	 * 
	 * @param objectIdCont
	 * @return contatti
	 */
	IndependentObjectSet fetchContatti(String objectIdCont);

	/**
	 * Ottiene gli ordini di pagamento.
	 * 
	 * @param elencoOP
	 * @param numeroOp
	 * @return ordini di pagamento
	 */
	CustomObject fetchOP(CustomObject elencoOP, Integer numeroOp);

	/**
	 * Aggiorna l'ordine di pagamento.
	 * 
	 * @param opFilenet
	 * @param annoEseOP
	 * @param codiceAmministrazione
	 * @param codiceRagioneria
	 * @param numeroCapitolo
	 * @param numeroOP
	 * @param pianoGestione
	 * @param statoOP
	 * @param tipoOP
	 */
	void updateOP(CustomObject opFilenet, Integer annoEseOP, String codiceAmministrazione, String codiceRagioneria,
			Integer numeroCapitolo, Integer numeroOP, Integer pianoGestione, String statoOP, Integer tipoOP);

	/**
	 * Crea l'ordine di pagamento.
	 * 
	 * @param elencoOP
	 * @param annoEseOP
	 * @param codiceAmministrazione
	 * @param codiceRagioneria
	 * @param numeroCapitolo
	 * @param numeroOP
	 * @param pianoGestione
	 * @param statoOP
	 * @param tipoOP
	 */
	void createOP(CustomObject elencoOP, Integer annoEseOP, String codiceAmministrazione, String codiceRagioneria,
			Integer numeroCapitolo, Integer numeroOP, Integer pianoGestione, String statoOP, Integer tipoOP);

	/**
	 * Esegue la ricerca avanzata sollecitata tramite web service.
	 * 
	 * @param classeDocumentale
	 * @param fullTextCondition
	 * @param whereCondition
	 * @param inLimitMaxResults
	 * @return DocumentSet dei documenti ricercati
	 */
	DocumentSet ricercaAvanzataDaWs(String classeDocumentale, String fullTextCondition, String whereCondition,
			Integer inLimitMaxResults);

	/**
	 * Codifica le Properties Filenet in DTO.
	 * 
	 * @return lista di properties in DTO
	 */
	List<PropertyTemplateDTO> elencoProperties();

	/**
	 * Esegue la ricerca generica degli allegati.
	 * 
	 * @param stringaRicerca
	 * @param anno
	 * @param modalitaRicercaTestuale
	 * @return DocumentSet degli allegati
	 */
	DocumentSet ricercaAllegatiGenerica(String stringaRicerca, Integer anno,
			RicercaGenericaTypeEnum modalitaRicercaTestuale);

	/**
	 * Ottiene le mail come documenti.
	 * 
	 * @param pathRecenti
	 * @param pathArchiviate
	 * @param documentClass
	 * @param dataDa
	 * @param dataA
	 * @param searchFields
	 * @param selectedStatList
	 * @param oggettoRicerca
	 * @param mittenteRicerca
	 * @param numeroProtocolloRicerca
	 * @return DocumentSet di mail
	 */
	DocumentSet getMailsAsDocumentRicerca(String pathRecenti, String pathArchiviate, String documentClass, Date dataDa,
			Date dataA, List<String> searchFields, List<StatoMailEnum> selectedStatList, String oggettoRicerca,
			String mittenteRicerca, String numeroProtocolloRicerca);

	/**
	 * Esegue la ricerca dei documenti/esiti.
	 * 
	 * @param mappaValoriRicerca
	 * @return DocumentSet di documenti/esiti.
	 */
	DocumentSet ricercaEsitiUscita(Map<String, Object> mappaValoriRicerca);

	/**
	 * Sposta le mail.
	 * 
	 * @param folderDestinazione
	 * @param folder
	 * @param mailBoxs
	 * @param milestoneDate
	 */
	void spostaMail(String folderDestinazione, String folder, FolderSet mailBoxs, Date milestoneDate);

	/**
	 * Ottiene i document title dai documenti.
	 * 
	 * @param docs
	 * @return lista di document title
	 */
	List<String> getDocumentTitlesFromDocumentSet(DocumentSet docs);

	/**
	 * Ottiene il document title tramite l'id del processo.
	 * 
	 * @param flusso
	 * @param idProcesso
	 * @return document title
	 */
	String getDocumentTitleByIdProcesso(TipoContestoProceduraleEnum flusso, String idProcesso);

	/**
	 * Esegue la ricerca generica.
	 * 
	 * @param key
	 * @param anno
	 * @param type
	 * @param categoria
	 * @param idAoo
	 * @param onlyDocumentTitle
	 * @param onlyProtocollati
	 * @return DocumentSet dei documenti ricercati
	 */
	DocumentSet ricercaGenerica(String key, Integer anno, RicercaGenericaTypeEnum type,
			CategoriaDocumentoEnum categoria, Long idAoo, Boolean onlyDocumentTitle, Boolean onlyProtocollati);

	/**
	 * Ottiene i documenti per la verifica della firma.
	 * 
	 * @param idAoo
	 * @param searchCondition
	 * @param className
	 * @param currentVersion
	 * @return DocumentSet dei documenti
	 */
	DocumentSet getDocumentsForVerificaFirma(Integer idAoo, String searchCondition, String className,
			Integer currentVersion);

	/**
	 * Ottiene gli allegati firmati non verificati o non validi.
	 * 
	 * @param guid
	 * @return DocumentSet degli allegati
	 */
	DocumentSet getAllegatiFirmatiNonVerificatiNonValidi(String guid);

	/**
	 * Ottiene la mail come documento.
	 * 
	 * @param messageId
	 * @return mail
	 */
	Document getMailAsDocument(String messageId);

	/**
	 * Restituisce l'info se sono presenti allegati non verificati.
	 * 
	 * @param guid
	 * @return boolean
	 */
	boolean sonoPresentiAllegatiFirmatiNonVerificati(String guid);

	/**
	 * Restituisce il documento per aoo in base alla versione.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @param versione
	 * @return Document
	 */
	Document getDocumentByDTandAOOForVerificaFirma(String documentTitle, Long idAoo, Integer versione);

	/**
	 * Restituisce il Document per l'allegato.
	 * 
	 * @param guid
	 * @param nomeFile
	 * @param selectAll
	 * @return Document
	 */
	Document getDocumentForAllegato(String guid, boolean nomeFile, boolean selectAll);

}
