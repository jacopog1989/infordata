package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * The Class ClientDTO.
 *
 * @author CPIERASC
 * 
 * 	DTO per modellare le informazioni del client.
 */
public class ClientDTO implements Serializable {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Ip.
	 */
	private String ip;

	/**
	 * Sistema operativo.
	 */
	private String os;

	/**
	 * Broswer.
	 */
	private String browser;

	/**
	 * User agent.
	 */
	private String userAgent;

	/**
	 * Costruttore.
	 * 
	 * @param inIp			ip client
	 * @param inOs			sistema operativo host client
	 * @param inBrowser		browser in uso
	 * @param inUserAgent	user agent
	 */
	public ClientDTO(final String inIp, final String inOs, final String inBrowser, final String inUserAgent) {
		super();
		this.ip = inIp;
		this.os = inOs;
		this.browser = inBrowser;
		this.userAgent = inUserAgent;
	}

	/**
	 * Getter.
	 * 
	 * @return	ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * Getter sistema operativo.
	 * 
	 * @return	sistema operativo
	 */
	public String getOs() {
		return os;
	}

	/**
	 * Getter.
	 * 
	 * @return	browser
	 */
	public String getBrowser() {
		return browser;
	}

	/**
	 * Getter.
	 * 
	 * @return	user agent
	 */
	public String getUserAgent() {
		return userAgent;
	}

}
