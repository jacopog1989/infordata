package it.ibm.red.business.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.util.CollectionUtils;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.Varie;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;

/**
 * Utility destintari RED.
 */
public final class DestinatariRedUtils {

	/**
	 * Costruttore vuoto.
	 */
	private DestinatariRedUtils() {
		// Costruttore vuoto.
	}
	
	/**
	 * Verifica se nella lista di destinatari in input c'è almeno un destinatario ESTERNO e con mezzo di spedizione ELETTRONICO.
	 * 
	 * @param destinatari
	 * @return true se è presente, false altrimenti
	 */
	public static boolean checkOneDestinatarioElettronico(final List<DestinatarioRedDTO> destinatari) {
		boolean destinatarioElettronico = false;
		
		if (!CollectionUtils.isEmpty(destinatari)) {
			for (final DestinatarioRedDTO destinatario : destinatari) {
				if (TipologiaDestinatarioEnum.ESTERNO.equals(destinatario.getTipologiaDestinatarioEnum()) 
						&& MezzoSpedizioneEnum.ELETTRONICO.equals(destinatario.getMezzoSpedizioneEnum())) {
					destinatarioElettronico = true;
					break;
				}
			}
		}
		
		return destinatarioElettronico;
	}
	
	
	/**
	 * Verifica se nella lista di destiantari in input c'è almeno un destinatario INTERNO.
	 * 
	 * @param destinatari
	 * @return true se è presente, false altrimenti
	 */
	public static boolean checkOneDestinatarioInterno(final List<DestinatarioRedDTO> destinatari) {
		boolean destinatarioInterno = false;
		
		if (!CollectionUtils.isEmpty(destinatari)) {
			for (final DestinatarioRedDTO destinatario : destinatari) {
				if (TipologiaDestinatarioEnum.INTERNO.equals(destinatario.getTipologiaDestinatarioEnum())) {
					destinatarioInterno = true;
					break;
				}
			}
		}
		
		return destinatarioInterno;
	}
	
	
	/**
	 * Verifica se gli eventuali destinatari ESTERNI presenti nella lista in input hanno tutti mezzo di spedizione ELETTRONICO.
	 * 
	 * @param destinatari
	 * @return true se tutti i destinatari ESTERNI hanno mezzo di spedizione ELETTRONICO, false altrimenti. Se non sono presenti destinatari ESTERNI, restituisce TRUE.
	 */
	public static boolean checkAllMezzoSpedizioneDestinatariElettronico(final List<DestinatarioRedDTO> destinatari) {
		boolean allEsterniElettronici = true;
		
		if (!CollectionUtils.isEmpty(destinatari)) {
			for (final DestinatarioRedDTO destinatario : destinatari) {
				if (TipologiaDestinatarioEnum.ESTERNO.equals(destinatario.getTipologiaDestinatarioEnum()) 
						&& MezzoSpedizioneEnum.CARTACEO.equals(destinatario.getMezzoSpedizioneEnum())) {
					allEsterniElettronici = false;
					break;
				}
			}
		}
		
		return allEsterniElettronici;
	}
	
	
	/**
	 * Verifica se gli eventuali destinatari ESTERNI presente nella lista in input hanno tutti lo STESSO mezzo di spedizione (CARTACEO o ELETTRONICO).
	 * 
	 * @param destinatari
	 * @return true se tutti i destinatari ESTERNI hanno lo STESSO mezzo di spedizione, false altrimenti. Se non sono presenti destinatari ESTERNI, restituisce true.
	 */
	public static boolean checkMezziSpedOmogeneiDestinatariEsterni(final List<DestinatarioRedDTO> destinatari) {
		boolean mezziSpedEsterniOmogenei = true;
		MezzoSpedizioneEnum mezzoSpedPrimoDestinatarioEsterno = null;
		
		if (!CollectionUtils.isEmpty(destinatari)) {
			for (final DestinatarioRedDTO destinatario : destinatari) {
				if (TipologiaDestinatarioEnum.ESTERNO.equals(destinatario.getTipologiaDestinatarioEnum())) {
					final MezzoSpedizioneEnum mezzoSpedEsterno = destinatario.getMezzoSpedizioneEnum();
					// Se l'attributo per il confronto non è valorizzato, si tratta del primo destinatario esterno e si aggiorna l'attributo
			    	if (mezzoSpedPrimoDestinatarioEsterno == null) {
			    		mezzoSpedPrimoDestinatarioEsterno = mezzoSpedEsterno;
					// Altrimenti, si esegue il confronto tra il destinatario esterno attuale e il primo
			    	} else if (!mezzoSpedEsterno.equals(mezzoSpedPrimoDestinatarioEsterno)) {
				    	mezziSpedEsterniOmogenei = false;
				    	break;
				    }
				}
			}
		}
	    
	    return mezziSpedEsterniOmogenei;
	}
	
	
	/**
	 * @param destinatari
	 * @return
	 */
	public static int countDestinatariCartacei(final List<DestinatarioRedDTO> destinatari) {
		int n = 0;
		
		if (!CollectionUtils.isEmpty(destinatari)) {
			for (final DestinatarioRedDTO destinatario : destinatari) {
				if (MezzoSpedizioneEnum.CARTACEO.equals(destinatario.getMezzoSpedizioneEnum())) {
					n++;
				}
			}
		}
		
		return n;
	}
	

	/**
	 * @param destinatari
	 * @return
	 */
	public static int countDestinatariElettronici(final List<DestinatarioRedDTO> destinatari) {
		int n = 0;
		
		if (!CollectionUtils.isEmpty(destinatari)) {
			for (final DestinatarioRedDTO destinatario : destinatari) {
				if (MezzoSpedizioneEnum.ELETTRONICO.equals(destinatario.getMezzoSpedizioneEnum())) {
					n++;
				}
			}
		}
		
		return n;
	}
	

	/**
	 * @param destinatari
	 * @return
	 */
	public static int countDestinatariInterni(final List<DestinatarioRedDTO> destinatari) {
		int n = 0;
		
		if (!CollectionUtils.isEmpty(destinatari)) {
			for (final DestinatarioRedDTO destinatario : destinatari) {
				if (TipologiaDestinatarioEnum.INTERNO.equals(destinatario.getTipologiaDestinatarioEnum())) {
					n++;
				}
			}
		}
		
		return n;
	}
	
	
	/**
	 * @param destinatari
	 * @return
	 */
	public static boolean checkDestinatariSoloInterni(final List<DestinatarioRedDTO> destinatari) {
		if (!CollectionUtils.isEmpty(destinatari)) {
			for (final DestinatarioRedDTO destinatario : destinatari) {
				if (TipologiaDestinatarioEnum.ESTERNO.equals(destinatario.getTipologiaDestinatarioEnum())) {
					return false;
				}
			}
		}
		
		return true;
	}
	

	/**
	 * @param destinatari
	 * @return
	 */
	public static boolean checkDestinatariSoloEsterni(final List<DestinatarioRedDTO> destinatari) {
		if (!CollectionUtils.isEmpty(destinatari)) {
			for (final DestinatarioRedDTO destinatario : destinatari) {
				if (TipologiaDestinatarioEnum.INTERNO.equals(destinatario.getTipologiaDestinatarioEnum())) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	
	/**
	 * Restituisce true se il valore è 0 oppure null.
	 * 
	 * @param value
	 * @return
	 */
	public static boolean is0orNull(final Long value) {
		return (value == null ||  value.intValue() == 0);
	}
	
	
	/**
	 * Restituisce true se il valore è 0 oppure null.
	 * 
	 * @param value
	 * @return
	 */
	public static boolean is0orNull(final Integer value) {
		return (value == null ||  value.intValue() == 0);
	}
	
	
	/**
	 * @param metadatoDestinatariDocumento
	 * @return
	 */
	public static List<String[]> normalizzaMetadatoDestinatariDocumento(final Collection<?> metadatoDestinatariDocumento) {
		final List<String[]> destinatari = new ArrayList<>();
		
		if (!CollectionUtils.isEmpty(metadatoDestinatariDocumento)) {
			
			for (final Object metadatoDestinatarioDocumento : metadatoDestinatariDocumento) {
				String[] destSplit = ((String) metadatoDestinatarioDocumento).split("\\,");
				
				// Ho qualche virgola nell'alias del contatto
				if (destSplit.length > 5) {
					final String [] newDestSplit = new String[5];
					newDestSplit[0] = destSplit[0];
					newDestSplit[1] = Constants.EMPTY_STRING;
					
					// Salto il primo e gli ultimi due elementi che sono quelli che mi interessano
					for (int x = 1; x < destSplit.length - 3; x++) {
						newDestSplit[1] += (x == destSplit.length - 4) ? destSplit[x] : destSplit[x] + ",";
					}
					newDestSplit[2] = destSplit[destSplit.length - 3];
					newDestSplit[3] = destSplit[destSplit.length - 2];
					newDestSplit[4] = destSplit[destSplit.length - 1];
					
					destSplit = newDestSplit;
					
				// Prima della "MEV Cavallo" non era presente l'informazione relativa alla modalita del destinatario (TO/CC),
				// quindi si specifica direttamente il "TO"
				} else if (destSplit.length == Varie.INDEX_BEFORE_MEV_CAVALLO) {
					
					final String[] destSplitNew = new String[5];
					destSplitNew[0] = destSplit[0];
					destSplitNew[1] = destSplit[1];
					destSplitNew[2] = destSplit[2];
					destSplitNew[3] = destSplit[3];
					destSplitNew[4] = ModalitaDestinatarioEnum.TO.name();
					destSplit = destSplitNew;
					
				}
				
				destinatari.add(destSplit);
			}
			
		}
		
		return destinatari;
	}
	
}