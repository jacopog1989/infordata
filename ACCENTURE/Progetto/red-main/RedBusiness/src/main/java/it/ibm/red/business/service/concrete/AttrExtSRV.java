package it.ibm.red.business.service.concrete;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.LookupTableDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoDocumentoModeEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IAttrExtSRV;
import it.ibm.red.business.service.facade.ITipoProcedimentoFacadeSRV;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * Service per la gestione degli attributi estesi.
 */
@Service
public class AttrExtSRV extends AbstractService implements IAttrExtSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -40390450759235713L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AttrExtSRV.class.getName());
	
	/**
	 * Servizio tipo procedimento.
	 */
	@Autowired
	private ITipoProcedimentoFacadeSRV tipoProcSRV;

	/**
	 * Servizio tipo documento.
	 */
	@Autowired
	private ITipologiaDocumentoFacadeSRV tipoDocSRV;

	/*************************************************************************************** SAMPLE. *****************************************************************************************/

	@Override
	public String exportSample(final UtenteDTO utente, final Integer idTipoDocumento, final Integer idTipoProcedimento) {
		final List<MetadatoDTO> listaMetadati = tipoDocSRV.caricaMetadati(idTipoDocumento, idTipoProcedimento, null, utente.getIdAoo());
		final String descrizioneTipoProc = tipoProcSRV.getDescTipoProcedimentoById(idTipoProcedimento);
		if (listaMetadati == null || listaMetadati.isEmpty()) {
			throw new RedException("Nessun metadato presente.");
		}
		if (StringUtils.isNullOrEmpty(descrizioneTipoProc)) {
			throw new RedException("Impossibile recuperare il tipo procedimento.");
		}
		final TipologiaDocumentoDTO tipoDoc = tipoDocSRV.getById(idTipoDocumento);
		return getXML(tipoDoc.getDescrizione(), descrizioneTipoProc, listaMetadati);
	}

	private static String getXML(final String nomeTipoDocumento, final String nomeTipoProcedimento, final List<MetadatoDTO> metadati) {
		final StringBuilder sb = new StringBuilder("");
		sb.append("<TIPOLOGIA xmlns=\"http://www.w3.org/2001/XMLSchema-instance\">");
		sb.append("<NOME>");
		sb.append(nomeTipoDocumento);
		sb.append("</NOME>");
		sb.append("<PROCEDIMENTO>");
		sb.append(nomeTipoProcedimento);
		sb.append("</PROCEDIMENTO>");

		final List<MetadatoDTO> multiValues = new ArrayList<>();
		boolean anagraficaDipendentiPresent = false;
		for (final MetadatoDTO m:metadati) {
			sb.append("<MetadatoAssociato>");
			sb.append("<Codice>");
			sb.append(m.getName());
			sb.append("</Codice>");
			sb.append("<Valore></Valore>");
			sb.append("</MetadatoAssociato>");
			
			if (isMandatatoryOnEntry(m)) {
				sb.append("<!-- Obbligatorio -->");
			} else {
				sb.append("<!-- NON Obbligatorio -->");
			}
			if (isOfType(m, TipoMetadatoEnum.STRING, TipoMetadatoEnum.TEXT_AREA)) {
				if (m.getDimension() != null && m.getDimension() > 0) {
					sb.append("<!-- Tipo: Stringa; Lunghezza max. " + m.getDimension() + " caratteri -->");
				}
			} else if (isOfType(m, TipoMetadatoEnum.INTEGER)) {
				if (m.getDimension() != null && m.getDimension() > 0) {
					sb.append("<!-- Tipo: Numerico (intero); Lunghezza max. " + m.getDimension() + " cifre -->");
				}
			} else if (isOfType(m, TipoMetadatoEnum.DOUBLE)) {
				if (m.getDimension()!=null && m.getDimension()>0) {
					sb.append("<!-- Tipo: Numerico (decimale); Lunghezza max. " + m.getDimension() + " cifre; Separatore decimale '.' -->");
				}
			} else if (isOfType(m, TipoMetadatoEnum.DATE)) {
				sb.append("<!-- Tipo: Data; Formato dd/mm/yyyy -->");
			} else if (isOfType(m, TipoMetadatoEnum.LOOKUP_TABLE)) {
				multiValues.add(m);
				sb.append("<!-- Tipo: Stringa; I valori ammessi sono indicati nella lista in fondo al documento -->");
			} else if (isOfType(m, TipoMetadatoEnum.CAPITOLI_SELECTOR)) {
				sb.append("<!-- Tipo: Stringa; Il valore deve essere del tipo 'CAPITOLO/PG' -->");
			} else if (isOfType(m, TipoMetadatoEnum.PERSONE_SELECTOR)) {
				anagraficaDipendentiPresent = true;
				sb.append("<!-- Tipo: XML; La struttura dei metadati di tipo Anagrafica Dipendente viene descritta in fondo al documento -->");
			} else {
				throw new RedException("Tipo non consentito.");
			}
			sb.append("\n");
		}
		sb.append("</TIPOLOGIA>\n");
		
		for (final MetadatoDTO m:multiValues) {
			sb.append("<!-- ===== Valori ammessi per il metadato associato "); 
			sb.append(m.getName()); 
			sb.append(", da inserire senza le parentesi quadre. Attenzione alla codifica che deve essere UTF-8 ===== -->\n"); 
			final LookupTableDTO ml = (LookupTableDTO) m;
			for (final SelectItemDTO siDTO:ml.getLookupValues()) {
				sb.append("<!-- [");
				sb.append(siDTO.getDescription());
				sb.append("] -->\n");
			}
			sb.append("<!-- ====================================== -->\n\n"); 
		}
		
		if(anagraficaDipendentiPresent) {
			sb.append("<!-- ===== Struttura del metadati di tipo Anagrafica Dipendente. ===== -->\n");
			sb.append("<!-- ===== Valorizzare uno o più oggetti XML cosi strutturati. ===== -->\n");
			sb.append("<!-- <AnagraficaDipendente> -->\n");
			sb.append("<!--     <NomeDipendente></NomeDipendente> -->\n");
			sb.append("<!--     <CognomeDipendente></CognomeDipendente> -->\n");
			sb.append("<!--     <DataNascitaDipendente>{formato data 'dd/MM/yyyy'}</DataNascitaDipendente> -->\n");
			sb.append("<!--     <ComuneNascitaDipendente></ComuneNascitaDipendente> -->\n");
			sb.append("<!--     <SessoDipendente>{valori ammessi 'M' o 'F'}</SessoDipendente> -->\n");
			sb.append("<!-- </AnagraficaDipendente> -->\n");
			sb.append("<!-- ====================================== -->\n\n"); 
		}
		return prettyFormat(sb.toString());
	}

	private static boolean isOfType(final MetadatoDTO m, final TipoMetadatoEnum... tipi) {
		boolean out = false;
		for (final TipoMetadatoEnum tipo:tipi) {
			if (tipo.equals(m.getType())) {
				out = true;
				break;
			}
		}
		return out;
	}

	private static String prettyFormat(final String input) {
		try {
			final Source xmlInput = new StreamSource(new StringReader(input));
			final StringWriter stringWriter = new StringWriter();
			final StreamResult xmlOutput = new StreamResult(stringWriter);
			final TransformerFactory transformerFactory = TransformerFactory.newInstance();
			
			try {
				transformerFactory.setAttribute(Constants.ACCESS_EXTERNAL_DTD, "");
				transformerFactory.setAttribute(Constants.ACCESS_EXTERNAL_STYLESHEET, "");
			} catch (final IllegalArgumentException illegalArgumentEx) {
				LOGGER.warn(illegalArgumentEx);
			}
			
			final Transformer transformer = transformerFactory.newTransformer(); 
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			transformer.transform(xmlInput, xmlOutput);
			return xmlOutput.getWriter().toString();
		} catch (final Exception e) {
			LOGGER.warn(e);
			return input;
		}
	}
	
	private static boolean isMandatatoryOnEntry(final MetadatoDTO m) {
		return m.getObligatoriness() != null
				&& (m.getObligatoriness().equals(TipoDocumentoModeEnum.SOLO_ENTRATA) || m.getObligatoriness().equals(TipoDocumentoModeEnum.SEMPRE));
	}
}
