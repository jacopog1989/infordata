package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IDashboardFacadeSRV;

/**
 * Interface del servizio gestione dashboard.
 */
public interface IDashboardSRV extends IDashboardFacadeSRV {
	
}
