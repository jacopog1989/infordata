package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IPkHandlerFacadeSRV;

/**
 * 
 * @author m.crescentini
 *
 */
public interface IPkHandlerSRV extends IPkHandlerFacadeSRV {
	
}
