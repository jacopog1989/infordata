package it.ibm.red.business.helper.email;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.dto.NotificaDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.LogStyleNotNull;

/**
 * Helper per la gestione delle notifiche email
 */
public class NotificheEmailHelper {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotificheEmailHelper.class.getName());

	/**
	 * Variabile per il recupero delle properties
	 */
	private static PropertiesProvider pp;
	
	/**
	 * Classe utils per il server delle mail
	 */
	private static ServerMailNotifiche serverMail;

	static {
		pp = PropertiesProvider.getIstance();
		serverMail = new ServerMailNotifiche();

		LOGGER.info("Server Mail per invio notifiche: " + serverMail);
	}

	/**
	 * Classe statica che gestisce il server delle mail per le notifiche
	 */
	protected static class ServerMailNotifiche {

		/**
		 * Tipologia messaggio
		 */
		private int tipologiaMessaggio;
		
		/**
		 * Mittente
		 */
		private String mittente;
		
		/**
		 * Username invio
		 */
		private String usernameInvio;
		
		/**
		 * Password invio
		 */
		private String passwordInvio;
		
		/**
		 * Proxy host
		 */
		private String proxyHost;
		
		/**
		 * Proxy port
		 */
		private String proxyPort;
		
		/**
		 * Server out autenticazione
		 */
		private int serverOutAutenticazione;
		
		/**
		 * Host di invio
		 */
		private String hostInvio;
		
		/**
		 * Porta invio
		 */
		private String portInvio;

		/** 
		 * Costruttore
		 */
		public ServerMailNotifiche() { 
			tipologiaMessaggio = Constants.TipologiaMessaggio.TIPO_MSG_EMAIL;
			mittente = pp.getParameterByKey(PropertiesNameEnum.SOTTOSCRIZIONI_MAIL_MITTENTE);
			usernameInvio = pp.getParameterByKey(PropertiesNameEnum.SOTTOSCRIZIONI_MAIL_USERNAME_INVIO);
			passwordInvio = pp.getParameterByKey(PropertiesNameEnum.SOTTOSCRIZIONI_MAIL_PASSWORD_INVIO);
			proxyHost = "null".equals(pp.getParameterByKey(PropertiesNameEnum.SOTTOSCRIZIONI_MAIL_PROXY_HOST)) ? null
					: pp.getParameterByKey(PropertiesNameEnum.SOTTOSCRIZIONI_MAIL_PROXY_HOST);
			proxyPort = "null".equals(pp.getParameterByKey(PropertiesNameEnum.SOTTOSCRIZIONI_MAIL_PROXY_PORT)) ? null
					: pp.getParameterByKey(PropertiesNameEnum.SOTTOSCRIZIONI_MAIL_PROXY_PORT);
			if ("0".equals(pp.getParameterByKey(PropertiesNameEnum.SOTTOSCRIZIONI_MAIL_SERVER_OUT_AUTH))) {
				serverOutAutenticazione = 0;
			} else {
				serverOutAutenticazione = 1;
			}
			hostInvio = pp.getParameterByKey(PropertiesNameEnum.SOTTOSCRIZIONI_MAIL_HOST_INVIO);
			portInvio = pp.getParameterByKey(PropertiesNameEnum.SOTTOSCRIZIONI_MAIL_PORT_INVIO);

		}
		
		/** 
		 * @return the tipologia Messaggio
		 */
		public int getTipologiaMessaggio() {
			return tipologiaMessaggio;
		}

		/**  
		 * @param tipologiaMessaggio the tipologiaMessaggio
		 */
		public void setTipologiaMessaggio(final int tipologiaMessaggio) {
			this.tipologiaMessaggio = tipologiaMessaggio;
		}

		/** 
		 * @return the mittente
		 */
		public String getMittente() {
			return mittente;
		}

		/**  
		 * @param mittente the mittente
		 */
		public void setMittente(final String mittente) {
			this.mittente = mittente;
		}

		/** 
		 * @return the username Invio
		 */
		public String getUsernameInvio() {
			return usernameInvio;
		}

		/**  
		 * @param usernameInvio the usernameInvio
		 */
		public void setUsernameInvio(final String usernameInvio) {
			this.usernameInvio = usernameInvio;
		}

		/** 
		 * @return the password Invio
		 */
		public String getPasswordInvio() {
			return passwordInvio;
		}

		/**  
		 * @param passwordInvio the passwordInvio
		 */
		public void setPasswordInvio(final String passwordInvio) {
			this.passwordInvio = passwordInvio;
		}

		/** 
		 * @return the proxy host
		 */
		public String getProxyHost() {
			return proxyHost;
		}

		/**  
		 * @param proxyHost the proxyHost
		 */
		public void setProxyHost(final String proxyHost) {
			this.proxyHost = proxyHost;
		}

		/** 
		 * @return the proxy Port
		 */
		public String getProxyPort() {
			return proxyPort;
		}

		/**  
		 * @param proxyPort the proxyPort
		 */
		public void setProxyPort(final String proxyPort) {
			this.proxyPort = proxyPort;
		}

		/** 
		 * @return the serverOutAutenticazione
		 */
		public int getServerOutAutenticazione() {
			return serverOutAutenticazione;
		}

		/**  
		 * @param serverOutAutenticazione the serverOutAutenticazione
		 */
		public void setServerOutAutenticazione(final int serverOutAutenticazione) {
			this.serverOutAutenticazione = serverOutAutenticazione;
		}

		/** 
		 * @return the hostInvio
		 */
		public String getHostInvio() {
			return hostInvio;
		}

		/**  
		 * @param hostInvio the hostInvio
		 */
		public void setHostInvio(final String hostInvio) {
			this.hostInvio = hostInvio;
		}

		/** 
		 * @return the portInvio
		 */
		public String getPortInvio() {
			return portInvio;
		}
		

		/**  
		 * @param portInvio the portInvio
		 */
		public void setPortInvio(final String portInvio) {
			this.portInvio = portInvio;
		}

		/**  
		 * toString
		 */
		@Override
		public String toString() {
			return ToStringBuilder.reflectionToString(this, new LogStyleNotNull());
		}
	}

	/**
	 *  Metodo che permette l'invio di una notifica.
	 *
	 * @param notifica the notifica
	 * @param listaAddress the lista address
	 * @param oggettoMail the oggetto mail
	 * @param corpoMail the corpo mail
	 * @return the string
	 */
	public String inviaNotifica(final NotificaDTO notifica, final List<String> listaAddress, final String oggettoMail, final String corpoMail, final String corpoMailHTML) {
		String result = "";
		final List<String> mailInErrore = new ArrayList<>();
		try {
			// Invio una mail per ogni indirizzo presente
			for (final String address : listaAddress) {
				handleInvioMail(notifica, oggettoMail, corpoMail, corpoMailHTML, mailInErrore, address);
			}
			// Recupero le mail andate in errore
			if (!mailInErrore.isEmpty()) {
				result = mailInErrore.size() + "/" + listaAddress.size() + " mail in errore: "
						+ listaAddress.toString();
			}

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore nel tentativo di notificare via mail un evento sottoscritto.", e);
			throw new RedException(e);
		}
		return result;
	}


	/**
	 * @param notifica
	 * @param oggettoMail
	 * @param corpoMail
	 * @param mailInErrore
	 * @param address
	 */
	private static void handleInvioMail(final NotificaDTO notifica, final String oggettoMail, final String corpoMail, final String corpoMailHTML, final List<String> mailInErrore, final String address) {
		
		try {
			inviaMail(address, oggettoMail, corpoMail, corpoMailHTML);
		} catch (final Exception e) {
			LOGGER.warn("Errore invio mail: ", e);
			mailInErrore.add(address);
			LOGGER.info("Invio notifica id: " + notifica.getIdNotifica() + " all'indirizzo: " + address + "NON riuscito");
		}
	}

	private static void inviaMail(final String destMail, final String oggetto, final String testo, final String testoHTML) {
		final MessaggioEmailDTO messaggio = new MessaggioEmailDTO();
		messaggio.setOggetto(oggetto);
		if(testoHTML==null) {
			messaggio.setTesto(testo);
		}else {
			messaggio.setTestoHTML(testoHTML);
		}
		messaggio.setDestinatario(destMail);
		messaggio.setTipologiaMessaggioId(serverMail.getTipologiaMessaggio());
		messaggio.setMittente(serverMail.getMittente());

		final CasellaPostaDTO cp = new CasellaPostaDTO();
		cp.setUsernameInvio(serverMail.getUsernameInvio());
		cp.setPasswordInvio(serverMail.getPasswordInvio());
		cp.setProxyHost(serverMail.getProxyHost());
		cp.setProxyPort(serverMail.getProxyPort());

		cp.setServerOutAutenticazione(serverMail.getServerOutAutenticazione());
		cp.setHostInvio(serverMail.getHostInvio());
		cp.setPortInvio(serverMail.getPortInvio());

		final EmailHelper eh = new EmailHelper("GestoreNotificheMailService", cp, messaggio);
		eh.sendEmail();
	}

}
