package it.ibm.red.business.dto;

import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.TipoOperazioneLibroFirmaEnum;

/**
 * The Class PEDocumentoFEPADTO.
 */
public class PEDocumentoFEPADTO extends PEDocumentoDTO {

	/*
	 * 
	 */
	private static final long serialVersionUID = -3570501156341579027L;
	
	/**
	 * Lista di wobNumber delle dichiarazione a cui è associato questo workflow.
	 */
	private String[] wobDichiarazioneServiziResi;
	
	/**
	 * Identifica se il workflow ha il campo wobDichiarazioneServiziResi 
	 */
	private boolean hasWobDichiarazioneServiziResi;
	
	/**
	 * Elenco decreti fepa
	 */
	private String[] elencoDecretiFepa;
	
	/**
	 * Costruttore PE documento FEPADTO.
	 *
	 * @param inIdDocumento the in id documento
	 * @param inOperazione the in operazione
	 * @param inWobNumber the in wob number
	 * @param inQueue the in queue
	 * @param inIdUtenteDestinatario the in id utente destinatario
	 * @param inIdNodoDestinatario the in id nodo destinatario
	 * @param inFlagFirmaDig the in flag firma dig
	 * @param inMotivazioneAssegnazione the in motivazione assegnazione
	 * @param inResponses the in responses
	 * @param inIdFascicolo the in id fascicolo
	 * @param inCodaCorrente the in coda corrente
	 * @param inTipoAssegnazioneId the in tipo assegnazione id
	 * @param inFlagIterManuale the in flag iter manuale
	 * @param inSubject the in subject
	 * @param inElencoLibroFirma the in elenco libro firma
	 * @param inCount the in count
	 * @param inDataCreazioneWF the in data creazione WF
	 * @param inFlagRenderizzato the in flag renderizzato
	 * @param inFirmaCopiaConforme the in firma copia conforme
	 * @param inUrgente the in urgente
	 * @param inStepName the in step name
	 * @param inNotaDTO the in nota DTO
	 * @param wobDichiarazioneServiziResi the wob dichiarazione servizi resi
	 * @param elencoDecretiFepa the elenco decreti fepa
	 * @param tipoFirma the tipo firma
	 * @param annullaTemplate the annulla template
	 */
	public PEDocumentoFEPADTO(final String inIdDocumento, final TipoOperazioneLibroFirmaEnum inOperazione, final String inWobNumber,
			final DocumentQueueEnum inQueue, final Integer inIdUtenteDestinatario, final Integer inIdNodoDestinatario,
			final Boolean inFlagFirmaDig, final String inMotivazioneAssegnazione, final String[] inResponses, final Integer inIdFascicolo,
			final String inCodaCorrente, final Integer inTipoAssegnazioneId, final Integer inFlagIterManuale, final String inSubject,
			final String[] inElencoLibroFirma, final Integer inCount, final String inDataCreazioneWF, final Boolean inFlagRenderizzato,
			final Boolean inFirmaCopiaConforme, final Boolean inUrgente, final String inStepName, final NotaDTO inNotaDTO, final String[] wobDichiarazioneServiziResi, 
			final String[] elencoDecretiFepa, final int tipoFirma, final boolean annullaTemplate, final Boolean firmaAsincronaAvviata) {
		super(inIdDocumento, inOperazione, inWobNumber, inQueue, inIdUtenteDestinatario, inIdNodoDestinatario, inFlagFirmaDig,
				inMotivazioneAssegnazione, inResponses, inIdFascicolo, inCodaCorrente, inTipoAssegnazioneId, inFlagIterManuale,
				inSubject, inElencoLibroFirma, inCount, inDataCreazioneWF, inFlagRenderizzato, inFirmaCopiaConforme, inUrgente,
				inStepName, inNotaDTO, null, null,tipoFirma, annullaTemplate, firmaAsincronaAvviata);
		this.wobDichiarazioneServiziResi = wobDichiarazioneServiziResi;
		this.sethasWobDichiarazioneServiziResi(false);
		this.setElencoDecretiFepa(elencoDecretiFepa);
	}

	/** 
	 *
	 * @return the wob dichiarazione servizi resi
	 */
	public String[] getWobDichiarazioneServiziResi() {
		return wobDichiarazioneServiziResi;
	}

	/** 
	 *
	 * @param wobDichiarazioneServiziResi the new wob dichiarazione servizi resi
	 */
	public void setWobDichiarazioneServiziResi(final String[] wobDichiarazioneServiziResi) {
		this.wobDichiarazioneServiziResi = wobDichiarazioneServiziResi;
	}

	/** 
	 *
	 * @return true, if successful
	 */
	public boolean hasWobDichiarazioneServiziResi() {
		return hasWobDichiarazioneServiziResi;
	}

	/** 
	 *
	 * @param hasWobDichiarazioneServiziResi the new checks for wob dichiarazione servizi resi
	 */
	public void sethasWobDichiarazioneServiziResi(final boolean hasWobDichiarazioneServiziResi) {
		this.hasWobDichiarazioneServiziResi = hasWobDichiarazioneServiziResi;
	}

	/** 
	 *
	 * @return the elenco decreti fepa
	 */
	public String[] getElencoDecretiFepa() {
		return elencoDecretiFepa;
	}

	/** 
	 *
	 * @param elencoDecretiFepa the new elenco decreti fepa
	 */
	public void setElencoDecretiFepa(final String[] elencoDecretiFepa) {
		this.elencoDecretiFepa = elencoDecretiFepa;
	}
	
	
}
