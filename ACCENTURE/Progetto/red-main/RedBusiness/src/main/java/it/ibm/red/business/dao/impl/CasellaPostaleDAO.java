/**
 * 
 */
package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ICasellaPostaleDAO;
import it.ibm.red.business.enums.FunzionalitaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * @author SLac
 *
 */
@Repository
public class CasellaPostaleDAO extends AbstractDAO implements ICasellaPostaleDAO {

	private static final String AND_CP_IDNODO = "   and cp.idnodo = ? ";

	private static final String WHERE_CP_CASELLAPOSTALE = " WHERE cp.casellapostale = ? ";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1291073775321793045L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CasellaPostaleDAO.class);
	
	
	/**
	 * @see it.ibm.red.business.dao.ICasellaPostaleDAO#getAbilitazioniCasellaPostale(java.lang.String,
	 *      int, it.ibm.red.business.enums.AccessoFunzionalitaEnum,
	 *      java.sql.Connection).
	 * @param casellaPostale
	 *            casella postale da cui occorrono le abilitazioni
	 * @param idNodo
	 *            identificatvo ufficio
	 * @param f
	 *            funzionalita specificata
	 * @param con
	 *            connession al database
	 * @return true se abilitato, false altrimenti
	 */
	@Override
	public boolean getAbilitazioniCasellaPostale(final String casellaPostale, final Long idNodo, final FunzionalitaEnum f, final Connection con) {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean abilitato = false;
		
		try {
			int i = 1;
			final String query = "SELECT cp.* " 
				+ "  FROM CASELLAPOSTALENODOFUNZ cp " 
				+ " INNER JOIN FUNZIONALITA f " 
				+ "    ON cp.idfunzionalita = f.idfunzionalita " 
				+ WHERE_CP_CASELLAPOSTALE 
				+ AND_CP_IDNODO 
				+ "   and f.codfunzionalita = ? ";
			
			ps = con.prepareStatement(query);
			ps.setString(i++, casellaPostale);
			ps.setLong(i++, idNodo);
			ps.setString(i++, f.name());
			rs = ps.executeQuery();
			if (rs.next()) {
				abilitato = true;
			}
			
		} catch (final SQLException e) {
			LOGGER.error(e.getMessage());
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return abilitato;
	}
	

	/**
	 * @see it.ibm.red.business.dao.ICasellaPostaleDAO#getAbilitazioniCasellaPostale(java.lang.String,
	 *      int, it.ibm.red.business.enums.AccessoFunzionalitaEnum,
	 *      java.sql.Connection).
	 * @param casellaPostale
	 *            casella postale sul quale eseguire la query
	 * @param idNodo
	 *            identificativo ufficio
	 * @param f
	 *            funzionalità specificata
	 * @param con
	 *            connessione al database
	 */
	@Override
	public int getOrdinamentoCPAbilitata(final String casellaPostale, final Long idNodo, final FunzionalitaEnum f, final Connection con) {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		int ordinamento = -1;
		
		try {
			int i = 1;
			final String query = "SELECT cp.* " 
				+ "  FROM CASELLAPOSTALENODOFUNZ cp " 
				+ " INNER JOIN FUNZIONALITA f " 
				+ "    ON cp.idfunzionalita = f.idfunzionalita " 
				+ WHERE_CP_CASELLAPOSTALE 
				+ AND_CP_IDNODO 
				+ "   and f.codfunzionalita = ? ";
			
			ps = con.prepareStatement(query);
			ps.setString(i++, casellaPostale);
			ps.setLong(i++, idNodo);
			ps.setString(i++, f.name());
			rs = ps.executeQuery();
			if (rs.next()) {
				ordinamento = rs.getInt("ordinamento");
			}
			
		} catch (final SQLException e) {
			LOGGER.error(e.getMessage());
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return ordinamento;
	}

	/**
	 * @see it.ibm.red.business.dao.ICasellaPostaleDAO#getCasellaPostaleNodoResponse(java.lang.String,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<Integer> getCasellaPostaleNodoResponse(final String casellaPostale, final Long idNodo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<Integer> responseIds = new ArrayList<>();	
		try {
			int i = 1;
			final String query = "SELECT cp.idoperazione " 
				+ "  FROM CASELLAPOSTALENODORESPONSE cp " 
				+ WHERE_CP_CASELLAPOSTALE 
				+ AND_CP_IDNODO;
			
			ps = connection.prepareStatement(query);
			ps.setString(i++, casellaPostale);
			ps.setLong(i++, idNodo);
			rs = ps.executeQuery();
			while (rs.next()) {
				responseIds.add(rs.getInt("idoperazione"));
			}
			return responseIds;
			
		} catch (final SQLException e) {
			LOGGER.error(e.getMessage());
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}	
	}

	/**
	 * @see it.ibm.red.business.dao.ICasellaPostaleDAO#getMaxSizeContentCasellaPostale(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public Integer getMaxSizeContentCasellaPostale(final String casellaPostale, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer maxSize = null;	
		try {
			final String query = "SELECT cp.MAXSIZECONTENTUSCITA "
						+ "FROM CASELLAPOSTALE cp "
						+ "WHERE cp.INDIRIZZOEMAIL = ? ";
			
			ps = connection.prepareStatement(query);
			ps.setString(1, casellaPostale);
			rs = ps.executeQuery();
			if (rs.next()) {
				final String maxSizeNullable = rs.getString("MAXSIZECONTENTUSCITA");
				if (maxSizeNullable != null) {
					maxSize = Integer.valueOf(maxSizeNullable);
				}
			}
			return maxSize;
			
		} catch (final SQLException e) {
			LOGGER.error(e.getMessage());
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}
}
