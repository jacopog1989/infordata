package it.ibm.red.business.enums;

/**
 * @author m.crescentini
 *
 */
public enum StatoNotificaContattoEnum {
	
	
	/**
	 * Valore.
	 */
	ESEGUITA("ESEGUITA"),
	
	/**
	 * Valore.
	 */
	RIFIUTATA("RIFIUTATA"),
	
	/**
	 * Valore.
	 */
	IN_ATTESA("IN_ATTESA"),
	
	/**
	 * Valore.
	 */
	PRESA_VISIONE("PRESA_VISIONE"),
	
	/**
	 * Valore.
	 */
	APPROVATO_CON_MODIFICHE("MODIFICATA");
	
	
	/**
	 * Nome.
	 */
	private String nome;
	
	/**
	 * Costruttore.
	 * @param nome
	 */
	StatoNotificaContattoEnum(final String nome) {
		this.nome = nome;
	}
	
	/**
	 * Restituisce il nome.
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Restituisce l'enum associata al nome in ingresso.
	 * @param nome
	 * @return enum associata al nome
	 */
	public static StatoNotificaContattoEnum get(final String nome) {
		StatoNotificaContattoEnum output = null;
 
		for (StatoNotificaContattoEnum stato : StatoNotificaContattoEnum.values()) {
			if (stato.getNome().equalsIgnoreCase(nome)) {
				output = stato;
				break;
			}
		}
		 
		return output;
	}

}
