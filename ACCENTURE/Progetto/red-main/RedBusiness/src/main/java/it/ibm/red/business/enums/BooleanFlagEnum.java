package it.ibm.red.business.enums;

import java.util.HashSet;
import java.util.Set;

import it.ibm.red.business.dto.SelectItemDTO;

/**
 * Enum che mappa i valori della lingua parlata con boolean.
 */
public enum BooleanFlagEnum {

	/** 
	 * Flag si.
	 */
	SI(1, "S", "Y", "Si"), 
	
	/**
	 * Flag no.
	 */
	NO(0, "N", "N", "No");

	/**
	 * Value intero.
	 */
	private Integer intValue;

	/**
	 * Value string.
	 */
	private String value;

	/**
	 * Value string inglese.
	 */
	private String englishValue;

	/**
	 * Descrizione.
	 */
	private String description;

	/**
	 * Costruttore Enum.
	 * @param inIntValue
	 * @param inValue
	 * @param inEnglishValue
	 * @param inDescription
	 */
	BooleanFlagEnum(final Integer inIntValue, final String inValue, final String inEnglishValue, final String inDescription) {
		intValue = inIntValue; 
		value = inValue; 
		englishValue = inEnglishValue; 
		description = inDescription;
	}

	/**
	 * Restituisce l'intero associato all'enum.
	 * @return intValue come String
	 */
	public String getIntValueInString() {
		return "" + intValue;
	}

	/**
	 * Restituisce l'intero associato all'enum.
	 * @return intValue come Integer
	 */
	public Integer getIntValue() {
		return intValue;
	}

	/**
	 * Imposta l'intero associato all'Enum.
	 * @param intValue
	 */
	protected void setIntValue(final Integer intValue) {
		this.intValue = intValue;
	}

	/**
	 * Restituisce il value dell'Enum.
	 * @return value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Restituisce il value inglese associato all'Enum.
	 * @return englishValue
	 */
	public String getEnglishValue() {
		return englishValue;
	}

	/**
	 * Restituisce la descrizione dell'Enum.
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Restituisce la lista dei valori delle enum come lista di SelectItemDTO. Utile
	 * per il popolamento di combobox.
	 * 
	 * @return Set di SelectItemDTO
	 */
	public static Set<SelectItemDTO> getSelectItems() {
		Set<SelectItemDTO> output = new HashSet<>();
		for (BooleanFlagEnum bfe:BooleanFlagEnum.values()) {
			output.add(new SelectItemDTO(bfe.getValue(), bfe.getDescription()));
		}
		return output;
	}

}
