package it.ibm.red.business.helper.pdf;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.parser.LocationTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextRenderInfo;
import com.itextpdf.text.pdf.parser.Vector;

/**
 * The Class SearchTextExtractionStrategy.
 *
 * @author mcrescentini
 * 
 *         Strategy ricerca iText PDF.
 */
public final class SearchTextExtractionStrategy extends LocationTextExtractionStrategy {

	
	/**
	 * Posizione testo.
	 */
	private Rectangle textPosition;

	
	/**
	 * Testo da ricercare.
	 */
	private final String textToSearchFor;

	/**
	 * Lista blocchi di testo.
	 */
	private final List<TextRenderInfo> textBlockInfoList;

	/**
	 * Lista blocchi caratteri.
	 */
	private List<TextRenderInfo> chunkChars;

	/**
	 * Posizione in fondo.
	 */
	private float mostBottom;

	/**
	 * Indice usato.
	 */
	private int indexAlreadyUsed;
	
	/**
	 * Flag ricerca solo in fondo.
	 */
	private final boolean searchOnlyBottom;
	
	/**
	 * Flag ultima pagina.
	 */
	private final boolean isLastPage;

	/**
	 * Costruttore.
	 * @param textToSearchFor
	 * @param pageHeight
	 * @param searchOnlyBottom
	 * @param isLastPage
	 */
	public SearchTextExtractionStrategy(final String textToSearchFor, final float pageHeight, final boolean searchOnlyBottom, final boolean isLastPage) {
		super();
		this.textToSearchFor = textToSearchFor;
		this.textBlockInfoList = new ArrayList<>();
		this.chunkChars = new ArrayList<>();
		this.mostBottom = pageHeight;
		this.searchOnlyBottom = searchOnlyBottom;
		this.isLastPage = isLastPage;
	}

	/**
	 * Costruttore.
	 * @param textToSearchFor
	 * @param pageHeight
	 * @param indexAlreadyUsed
	 * @param searchOnlyBottom
	 * @param isLastPage
	 */
	public SearchTextExtractionStrategy(final String textToSearchFor, final float pageHeight, final int indexAlreadyUsed, final boolean searchOnlyBottom, final boolean isLastPage) {
		super();
		this.textToSearchFor = textToSearchFor;
		this.textBlockInfoList = new ArrayList<>();
		this.chunkChars = new ArrayList<>();
		this.mostBottom = pageHeight;
		this.indexAlreadyUsed = indexAlreadyUsed;
		this.searchOnlyBottom = searchOnlyBottom;
		this.isLastPage = isLastPage;
	}
    
	@Override
	public void beginTextBlock() {
		textBlockInfoList.clear();
		chunkChars.clear();
	}

	@Override
	public void endTextBlock() {
		final StringBuilder textBlock = new StringBuilder();

		for (final TextRenderInfo info : textBlockInfoList) {
			textBlock.append(info.getText());
		}

		// Stringa da cercare presente
		if (textToSearchFor != null) {
			int indexOfTextToSearchFor = -1;

			if (indexAlreadyUsed != 0) {
				String previousString = textBlock.toString();
				for (int x = 0; x < indexAlreadyUsed; x++) {
					final StringBuilder stringaDaSostituire = new StringBuilder("");

					for (int i = 0; i < textToSearchFor.length(); i++) {
						stringaDaSostituire.append(" ");
					}
					previousString = previousString.replaceFirst(textToSearchFor.replace("[", "\\[").replace("]", "\\]"), stringaDaSostituire.toString());
				}

				indexOfTextToSearchFor = it.ibm.red.business.utils.StringUtils.indexOfIgnoreCase(previousString, textToSearchFor);
			} else {
				indexOfTextToSearchFor = it.ibm.red.business.utils.StringUtils.indexOfIgnoreCase(textBlock.toString(), textToSearchFor);
			}

			//Se c'è altro testo dopo la stringa target non posso considerare il match
        	if (searchOnlyBottom && (indexOfTextToSearchFor != -1) 
        			&& (!isLastPage || (!textBlock.toString().trim().toLowerCase().endsWith(textToSearchFor.toLowerCase())))) {
        	    indexOfTextToSearchFor = -1;
        	} 
			
			if (indexOfTextToSearchFor > -1) {
				chunkChars = getChunkInfo(indexOfTextToSearchFor).getCharacterRenderInfos();
				final Vector bottomLeft = chunkChars.get(0).getDescentLine().getStartPoint(); // Top Left

				chunkChars = getChunkInfo(indexOfTextToSearchFor + textToSearchFor.length()).getCharacterRenderInfos();
				final Vector topRight = chunkChars.get(chunkChars.size() - 1).getAscentLine().getEndPoint(); // Bottom Right

				textPosition = new Rectangle(bottomLeft.get(Vector.I1), bottomLeft.get(Vector.I2),
						topRight.get(Vector.I1), topRight.get(Vector.I2));
			}
		// Nessuna stringa da cercare: si restituisce la posizione all'interno della pagina dell'ultimo blocco di testo
		} else if (!textBlockInfoList.isEmpty()) {
			// Si cerca l'ultimo chunk con del testo
			int lastTextChunkIndex = textBlockInfoList.size() - 1;
			while (lastTextChunkIndex >= 0 && StringUtils.isBlank(textBlockInfoList.get(lastTextChunkIndex).getText())) {
				lastTextChunkIndex--;
			}
			if (lastTextChunkIndex == -1) {
				lastTextChunkIndex = textBlockInfoList.size() - 1;
			}
			
			chunkChars = textBlockInfoList.get(lastTextChunkIndex).getCharacterRenderInfos();
			if (!CollectionUtils.isEmpty(chunkChars)) {
				final Vector bottomRight = chunkChars.get(chunkChars.size() - 1).getDescentLine().getEndPoint(); // Bottom Right

				if (bottomRight.get(Vector.I2) < mostBottom) {
					mostBottom = bottomRight.get(Vector.I2);

					textPosition = new Rectangle(bottomRight.get(Vector.I1), bottomRight.get(Vector.I2),
							bottomRight.get(Vector.I1), bottomRight.get(Vector.I2));
				}
			}
		}
	}

	// Automatically called for each chunk of text in the PDF
	@Override
	public void renderText(final TextRenderInfo renderInfo) {
		super.renderText(renderInfo);

		textBlockInfoList.add(renderInfo);
	}

	/**
	 * Dato l'indice di inizio della stringa trovata, restituisce il chunk di inizio della stringa.
	 * 
	 * @param index
	 * @return chunkInfo
	 */
	private TextRenderInfo getChunkInfo(final int index) {
		TextRenderInfo chunkInfo = null;

		int currentLength = 0;
		for (final TextRenderInfo info : textBlockInfoList) {
			currentLength += info.getText().length();

			if (currentLength >= (index + 1)) {
				chunkInfo = info;
				break;
			}

		}

		if (chunkInfo == null) {
			chunkInfo = textBlockInfoList.get(textBlockInfoList.size() - 1);
		}
		
		return chunkInfo;
	}

	/**
	 * Restituisce la posizione del testo.
	 * @return posizione testo
	 */
	public Rectangle getTextPosition() {
		return textPosition;
	}

}
