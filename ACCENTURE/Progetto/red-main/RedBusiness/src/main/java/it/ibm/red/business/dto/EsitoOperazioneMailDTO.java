package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * The Class EsitoOperazioneMailDTO.
 *
 * @author CPIERASC
 * 
 * 	DTO esito operazione mail.
 */
public class EsitoOperazioneMailDTO implements Serializable {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Guid.
	 */
	private String guid;
	/**
	 * Esito.
	 */
	private Boolean esito;

	/**
	 * Costruttore.
	 * 
	 * @param inGuid	guid
	 */
	public EsitoOperazioneMailDTO(final String inGuid) {
		super();
		this.guid = inGuid;
	}

	/**
	 * Setter.
	 * 
	 * @param inEsito	esito
	 */
	public void setEsito(final Boolean inEsito) {
		this.esito = inEsito;
	}

	/**
	 * Getter.
	 * 
	 * @return	guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * Getter.
	 * 
	 * @return	esito
	 */
	public Boolean getEsito() {
		return esito;
	}
	
	
}
