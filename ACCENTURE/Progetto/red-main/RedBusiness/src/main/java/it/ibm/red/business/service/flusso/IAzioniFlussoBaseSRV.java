package it.ibm.red.business.service.flusso;

import java.io.Serializable;
import java.sql.Connection;

import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.dto.ResponsesDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.IconaFlussoEnum;

/**
 * Interfaccia del servizio di gestione azioni flusso.
 */
public interface IAzioniFlussoBaseSRV extends Serializable {
	
	/**
	 * Implementa le azioni da eseguire a valle della creazione del documento identificato da <code> idDocumento </code> per
	 * il flusso specifico e definito dal <code> codiceFlusso </code>.
	 * 
	 * @param idDocumento
	 *            identificativo documento creato
	 * @param codiceFlusso
	 *            codice flusso di riferimento
	 * @param con
	 *            connessione al database
	 */
	void doAfterCreazioneDocumento(int idDocumento, String codiceFlusso, Connection con);

	/**
	 * Implementa le azioni da eseguire a valle della protocollazione in uscita per il flusso specifico.
	 * N.B. Nel metodo va sempre verificato che l'azione non sia già stata eseguita a causa di una precedente protocollazione non terminata correttamente.
	 * 
	 * @param idDocumento
	 * @param protocollo
	 * @param idAoo
	 * @param con
	 */
	void doAfterProtocollazioneUscita(int idDocumento, ProtocolloDTO protocollo, Long idAoo, Connection con);
	
	/**
	 * Restituisce le response valide tra le response presenti nel parametro:
	 * <code> responses </code> rimuovendone quelle non valide per il flusso di riferimento.
	 * 
	 * @see it.ibm.red.business.service.flusso.IAzioniFlussoBaseSRV#refineResponses
	 *      (it.ibm.red.business.enums.DocumentQueueEnum,
	 *      it.ibm.red.business.dto.ResponsesDTO).
	 * @param coda
	 *            nella quale sono valide le responses
	 * @param responses
	 *            le responses da raffinare
	 * @return responses raffinate
	 */
	ResponsesDTO refineResponses(DocumentQueueEnum coda, ResponsesDTO responses);
	
	
	/**
	 * Restituisce l'icona da mostrare nelle code documenti per l'identificazione del flusso specifico a cui appartiene il documento.
	 * 
	 * @see it.ibm.red.business.service.flusso.IAzioniFlussoBaseSRV#getIconaFlusso().
	 * @return icona flusso, @see IconaFlussoEnum
	 */
	IconaFlussoEnum getIconaFlusso();

}
