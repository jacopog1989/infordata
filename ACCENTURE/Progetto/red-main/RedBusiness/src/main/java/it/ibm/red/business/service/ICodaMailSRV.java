package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.DestinatarioCodaMailDTO;
import it.ibm.red.business.dto.DestinatarioDatiCertDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.dto.NotificaEmailDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.StatoCodaEmailEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.service.concrete.CodaMailSRV.ResponseSendMail;
import it.ibm.red.business.service.facade.ICodaMailFacadeSRV;

/**
 * Interface del servizio gestione code mail.
 */
public interface ICodaMailSRV extends ICodaMailFacadeSRV {

	/**
	 * Il metodo recupera i documenti da allegare alla mail e li setta in msgNext.allegatiMail
	 * @param msgNext
	 * @param idAoo 
	 * @param fceh
	 */
	void setAllegati(MessaggioEmailDTO msgNext, int idAoo, IFilenetCEHelper fceh);

	/**
	 * Il metodo invia il messaggio di posta elettronica in input. 
	 * 
	 * @param msgNext
	 * @param idAoo
	 * @param fceh
	 * @param conn se null, viene creata al suo interno. serve solo a leggere
	 * @return
	 */
	ResponseSendMail sendMail(MessaggioEmailDTO msgNext, int idAoo, IFilenetCEHelper fceh);

	/**
	 * @param msgNext
	 * @param idAoo
	 * @param fcDTO
	 * */
	ResponseSendMail sendMail(MessaggioEmailDTO msgNext, int idAoo, FilenetCredentialsDTO fcDTO);

	/**
	 * Il metodo registra l'errore in fase di invio 
	 * 
	 * @param msgNext
	 * @param erroreInvio
	 * @param spedisciNPS true, in caso si stia registrando la spedizione su NPS
	 */
	void registraErroreInvio(MessaggioEmailDTO msgNext, String erroreInvio, boolean spedisciNPS);

	/**
	 * Il metodo crea l'annotation sul CE per il msgId in input
	 * 
	 * @param msgNext
	 * @param idAoo
	 * @param ceh
	 */
	void creaAnnotation(MessaggioEmailDTO msgNext, int idAoo, IFilenetCEHelper ceh);

	/**
	 * Il metodo registra l'invio o la spedizione della notifica
	 * 
	 * @param msgNext
	 * @param spedisciNPS true, in caso si stia registrando la spedizione su NPS
	 * 
	 */
	void registraInvio(MessaggioEmailDTO msgNext, boolean spedisciNPS);

	/**
	 * Il metodo aggiorna gli elenchi di trasmissione
	 * 
	 * @param msgNext
	 * @param idAoo
	 * @param fceh
	 */
	void aggiornaElencoTrasmissione(MessaggioEmailDTO msgNext, int idAoo, IFilenetCEHelper fceh);

	/**
	 * Il metodo registra un evento su DWH_RED in caso il messaggio sia associato sulla base dati ad un codice evento
	 * 
	 * @param msgNext
	 * @param idAoo
	 */
	void gestisciEvento(MessaggioEmailDTO msgNext, int idAoo);
	
	/**
	 * @param idDocumento
	 * @param guid
	 * @param mittente
	 * @param destinatari
	 * @param destinatariTo
	 * @param destinatariCc
	 * @param oggettoMail
	 * @param testoMail
	 * @param azioneMail
	 * @param motivazioneMail
	 * @param tipologiaInvioId
	 * @param tipologiaMessaggioId
	 * @param messageId
	 * @param isNotifica
	 * @param hasAllegati
	 * @param modalitaSpedizione
	 * @param tipoEvento
	 * @param utente
	 * @param con
	 * @param firmaDigitale
	 * @param simulaInvioNPS
	 */
	void creaMailEInserisciInCoda(String idDocumento, String guid, String mittente, Collection<?> destinatari, String destinatariTo, String destinatariCc, String oggettoMail, 
			String testoMail, String azioneMail, String motivazioneMail, Integer tipologiaInvioId, Integer tipologiaMessaggioId, String messageId, Boolean isNotifica, 
			Boolean hasAllegati, Integer modalitaSpedizione, Integer tipoEvento, UtenteDTO utente, Connection con, Boolean firmaDigitale, boolean simulaInvioNPS);
	
	/**
	 * @param idDocumento
	 * @param guid
	 * @param mittente
	 * @param destinatari
	 * @param destinatariTo
	 * @param destinatariCc
	 * @param oggettoMail
	 * @param testoMail
	 * @param azioneMail
	 * @param motivazioneMail
	 * @param tipologiaInvioId
	 * @param tipologiaMessaggioId
	 * @param messageId
	 * @param isNotifica
	 * @param hasAllegati
	 * @param modalitaSpedizione
	 * @param tipoEvento
	 * @param utente
	 * @param con
	 * @param firmaDigitale
	 * @param allegatiDocTitle
	 */
	void creaMailEInserisciInCoda(String idDocumento, String guid, String mittente, Collection<?> destinatari, String destinatariTo, String destinatariCc, String oggettoMail, 
			String testoMail, String azioneMail, String motivazioneMail, Integer tipologiaInvioId, Integer tipologiaMessaggioId, String messageId, Boolean isNotifica, 
			Boolean hasAllegati, Integer modalitaSpedizione, Integer tipoEvento, UtenteDTO utente, Connection con, Boolean firmaDigitale, List<String> allegatiDocTitle,
			boolean simulaInvioNPS);

	/**
	 * @param guid
	 * @param casellaPostale
	 * @param mittente
	 * @param destinatariToList
	 * @param destinatariCcList
	 * @param oggettoMail
	 * @param testoMail
	 * @param azioneMail
	 * @param motivazioneMail
	 * @param tipologiaInvioId
	 * @param tipologiaMessaggioId
	 * @param isNotifica
	 * @param hasAllegati
	 * @param modalitaSpedizione
	 * @param tipoEvento
	 * @param utente
	 * @param connection
	 */
	void creaMailEInserisciInCoda(String guid, String casellaPostale, String mittente, List<DestinatarioCodaMailDTO> destinatariToList, 
			List<DestinatarioCodaMailDTO> destinatariCcList, String oggettoMail, String testoMail, String azioneMail, String motivazioneMail, Integer tipologiaInvioId,
			Integer tipologiaMessaggioId, Boolean isNotifica, Boolean hasAllegati, Integer modalitaSpedizione, Integer tipoEvento, UtenteDTO utente, Connection connection,
			boolean simulaInvioNPS);


	/**
	 * @param guid
	 * @param casellaPostale
	 * @param mittente
	 * @param destinatariToList
	 * @param destinatariCcList
	 * @param oggettoMail
	 * @param testoMail
	 * @param azioneMail
	 * @param motivazioneMail
	 * @param tipologiaInvioId
	 * @param tipologiaMessaggioId
	 * @param isNotifica
	 * @param hasAllegati
	 * @param modalitaSpedizione
	 * @param tipoEvento
	 * @param utente
	 * @param connection
	 * @param firmaDigitale
	 * @param allegatiDocTitle
	 * @param iterSpedizione
	 */
	Long creaMailEInserisciInCoda(String idDocumento, String guid, String mittente,	Collection<?> inDestinatari, String destinatariTo, 
			String destinatariCc, String oggettoMail, String inTestoMail, String azioneMail, String motivazioneMail,
			Integer tipologiaInvioId, Integer tipologiaMessaggioId, String messageId, Boolean isNotifica, Boolean hasAllegati, Integer modalitaSpedizione,
			Integer tipoEvento, UtenteDTO utente, Connection connection, Boolean firmaDigitale, List<String> allegatiDocTitle, boolean iterManualeSpedizione,
			boolean simulaInvioNPS);
	
	/**
	 * @param destinatariDatiCert
	 * @param messageId
	 * @param mittente
	 * @param idAdminAoo
	 * @param con
	 */
	void updateTipoDestinatario(List<DestinatarioDatiCertDTO> destinatariDatiCert, String messageId, String mittente,Integer idAdminAoo, Connection con);
	
	/**
	 * @param idDocumento
	 * @param destinatario
	 * @param stato
	 * @param conn
	 * @return
	 */
	MessaggioEmailDTO getItemByDocumentTitleAndEmailDestAndStato(String idDocumento, String destinatario, Integer stato, Connection conn);

	/**
	 * Aggiornamento informazioni a fronte di un invio da parte di NPS
	 * @param codaEmailItem
	 * @param idAoo
	 * @param con
	 * @param fceh
	 */
	void gestisciInvioNPS(MessaggioEmailDTO codaEmailItem, int idAoo, Connection con, IFilenetCEHelper fceh);
	
	/**
	 * Esegue l'update dello stato della notifica.
	 * @param idNotifica - id della notifica
	 * @return
	 */
	int updateNotifica(Long idNotifica);

	/**
	 * Gestisce l'item post invio della mail.
	 * @param response
	 * @param msgNext
	 * @param idAoo
	 * @param fceh
	 * @param con
	 * @param gestisciEvento
	 */
	void managePostInvioMail(ResponseSendMail response, MessaggioEmailDTO msgNext, int idAoo, IFilenetCEHelper fceh,
			Connection con, boolean gestisciEvento);

	/**
	 * Aggiorna il documento E-mail creato su FileNet con il Message-ID dell mail inviata.
	 * @param response
	 * @param messaggio
	 * @param message
	 * @param fceh
	 */
	void aggiornaMessaggioPostInvioWS(ResponseSendMail response, MessaggioEmailDTO messaggio, Document message,
			IFilenetCEHelper fceh);

	/**
	 * Aggiorna lo stato della notifica di spedizione.
	 * @param msgNext
	 * @param msgId
	 * @param conn
	 */
	void updateStatoAndManageEmailByCodiMessage(MessaggioEmailDTO msgNext, String msgId, Connection conn);

	/**
	 * Restituisce le notifiche che si trovano nello stato identificato da <code> statoRicevuta </code>
	 * e fanno riferimento all'aoo identificata da <code> idAoo </code>.
	 * @param idAoo
	 * @param statoRicevuta
	 * @param giorniNotificaMancataSpedizione
	 * @return lista notifiche
	 */
	List<NotificaEmailDTO> getNotificheByStatoRicevuta(Long idAoo, StatoCodaEmailEnum statoRicevuta, Integer giorniNotificaMancataSpedizione);

	/**
	 * Effettua l'update della data processamento del documento identificato da <code> documentTitle </code>
	 * e ne restituisce l'esito.
	 * @param idAoo
	 * @param documentTitle
	 * @return true se update effettuato, false altrimenti
	 */
	boolean upateDataProcessamentoByDT(Long idAoo, String documentTitle);
}
