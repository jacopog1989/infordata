package it.ibm.red.business.dto;

import java.util.List;

import it.ibm.red.business.enums.TipoTemplateMetadatoEnum;

/**
 * DTO che definisce il template metadato.
 */
public class TemplateMetadatoDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 8884824499272657064L;

	/**
	 * Identificativo template.
	 */
	private String idTemplate;

	/**
	 * Identificativo metadato.
	 */
	private Integer idMetadato;

	/**
	 * Label.
	 */
	private String label;

	/**
	 * Dimensione massima.
	 */
	private Integer maxLength;

	/**
	 * Ordine.
	 */
	private Integer ordine;

	/**
	 * Valore di default.
	 */
	private String defaultValue;

	/**
	 * Testo predefinito.
	 */
	private Integer testoPredefinito;

	/**
	 * Tipo metadato.
	 */
	private TipoTemplateMetadatoEnum tipoMetadato;

	/**
	 * Lista testi predefiniti.
	 */
	private List<TestoPredefinitoDTO> testiPredefiniti;

	/**
	 * Valore template.
	 */
	private String templateValue;

	/**
	 * Label template.
	 */
	private boolean templateLabel;

	/**
	 * Testo template.
	 */
	private boolean templateTesto;

	/**
	 * Flag case sensitive.
	 */
	private boolean caseSensitive;
	
	/**
	 * Flag confronto.
	 */
	private boolean sonoDiversi;
	
	
	/**
	 * Flag confronto.
	 */
	private Integer idMetadatoIntestazione;

	/**
	 * Flag confronto.
	 */
	private String info;



	/**
	 * Costruttore vuoto.
	 */
	public TemplateMetadatoDTO() {
		// Metodo intenzionalmente vuoto.
	}

	/**
	 * @return idTemplate
	 */
	public String getIdTemplate() {
		return idTemplate;
	}

	/**
	 * Imposta l'id del template.
	 * @param idTemplate
	 */
	public void setIdTemplate(final String idTemplate) {
		this.idTemplate = idTemplate;
	}

	/**
	 * @return idMetadato
	 */
	public Integer getIdMetadato() {
		return idMetadato;
	}

	/**
	 * Imposta l'id del metadato.
	 * @param idMetadato
	 */
	public void setIdMetadato(final Integer idMetadato) {
		this.idMetadato = idMetadato;
	}

	/**
	 * Restituisce il valore della label.
	 * @return label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Imposta il valore della label.
	 * @param label
	 */
	public void setLabel(final String label) {
		this.label = label;
	}

	/**
	 * Restituisce la lunghezza massima.
	 * @return maxLength
	 */
	public Integer getMaxLength() {
		return maxLength;
	}

	/**
	 * Imposta la lunghezza massima.
	 * @param maxLength
	 */
	public void setMaxLength(final Integer maxLength) {
		this.maxLength = maxLength;
	}

	/**
	 * @return ordine
	 */
	public Integer getOrdine() {
		return ordine;
	}
	/**
	 * Imposta l'ordine.
	 * @param ordine
	 */
	public void setOrdine(final Integer ordine) {
		this.ordine = ordine;
	}

	/**
	 * Restituisce il valore di default.
	 * @return defaultValue
	 */
	public String getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Imposta il valore di default.
	 * @param defaultValue
	 */
	public void setDefaultValue(final String defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * @return testoPredefinito
	 */
	public Integer getTestoPredefinito() {
		return testoPredefinito;
	}

	/**
	 * Imposta il testo predefinito.
	 * @param testoPredefinito
	 */
	public void setTestoPredefinito(final Integer testoPredefinito) {
		this.testoPredefinito = testoPredefinito;
	}

	/**
	 * @return tipoMetadato
	 */
	public TipoTemplateMetadatoEnum getTipoMetadato() {
		return tipoMetadato;
	}

	/**
	 * Imposta il tipo di metadato.
	 * @param tipoMetadato
	 */
	public void setTipoMetadato(final TipoTemplateMetadatoEnum tipoMetadato) {
		this.tipoMetadato = tipoMetadato;
	}

	/**
	 * Restituisce la lista di testi predefiniti.
	 * @return testiPredefiniti
	 */
	public List<TestoPredefinitoDTO> getTestiPredefiniti() {
		return testiPredefiniti;
	}

	/**
	 * Imposta la lista di testi predefiniti.
	 * @param testiPredefiniti
	 */
	public void setTestiPredefiniti(final List<TestoPredefinitoDTO> testiPredefiniti) {
		this.testiPredefiniti = testiPredefiniti;
	}

	/**
	 * @return templateValue
	 */
	public String getTemplateValue() {
		return templateValue;
	}

	/**
	 * Imposta il templateValue.
	 * @param templateValue
	 */
	public void setTemplateValue(final String templateValue) {
		this.templateValue = templateValue;
	}

	/**
	 * Restituisce il flag {@link #templateLabel}.
	 * @return templateLabel
	 */
	public boolean isTemplateLabel() {
		return templateLabel;
	}

	/**
	 * Imposta il flag {@link #templateLabel}.
	 * @param templateLabel
	 */
	public void setTemplateLabel(final boolean templateLabel) {
		this.templateLabel = templateLabel;
	}

	/**
	 * Restituisce il flag {@link #templateTesto}.
	 * @return
	 */
	public boolean isTemplateTesto() {
		return templateTesto;
	}

	/**
	 * Imposta il flag {@link #templateTesto}.
	 * @param templateTesto
	 */
	public void setTemplateTesto(final boolean templateTesto) {
		this.templateTesto = templateTesto;
	}

	/**
	 * Restituisce il flag {@link #caseSensitive}.
	 * @return caseSensitive
	 */
	public boolean getCaseSensitive() {
		return caseSensitive;
	}

	/**
	 * Imposta il flag {@link #caseSensitive}.
	 * @param caseSensitive
	 */
	public void setCaseSensitive(final boolean caseSensitive) {
		this.caseSensitive = caseSensitive;
	}

	/**
	 * Restituisce il flag {@link #sonoDiversi}.
	 * @return sonoDiversi
	 */
	public boolean getSonoDiversi() {
		return sonoDiversi;
	}

	/**
	 * Imposta il flag {@link #sonoDiversi}.
	 * @param sonoDiversi
	 */
	public void setSonoDiversi(final boolean sonoDiversi) {
		this.sonoDiversi = sonoDiversi;
	}

	/**
	 * @see java.lang.Object#toString().
	 */
	@Override
	public String toString() {
		return "TemplateMetadato [idTemplate=" + idTemplate + ", idMetadato=" + idMetadato + ", label=" + label
				+ ", maxLength=" + maxLength + ", ordine=" + ordine + ", defaultValue=" + defaultValue
				+ ", tipoMetadato=" + tipoMetadato + ", testoPredefinito=" + testoPredefinito +  ", templateValue=" + templateValue
				+  ", templateLabel=" + templateLabel + ", templateTesto=" + templateTesto + "]";
	}

	/**
	 * Restituisce l'identificativo del metadato intestazione.
	 * 
	 * @return Identificativo metadato intestazione.
	 */
	public Integer getIdMetadatoIntestazione() {
		return idMetadatoIntestazione;
	}

	/**
	 * Imposta l'identificativo del metadato intestazione.
	 * 
	 * @param idMetadatoIntestazione
	 *            Identificativo metadato intestazione da impostare.
	 */
	public void setIdMetadatoIntestazione(Integer idMetadatoIntestazione) {
		this.idMetadatoIntestazione = idMetadatoIntestazione;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
}