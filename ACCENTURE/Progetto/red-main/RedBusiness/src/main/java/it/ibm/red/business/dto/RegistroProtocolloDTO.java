package it.ibm.red.business.dto;

import java.util.Date;
import java.util.UUID;

import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;

/**
 * DTO che definisce un registro di protocollo.
 */
public class RegistroProtocolloDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -1421920241595791019L;

	/**
	 * Guid.
	 */
	private String guid;
	
	/**
	 * Document title.
	 */
	private String documentTitle;
	
	/**
	 * Data protocollo.
	 */
	private Date dataProtocollo;
	
	/**
	 * Anno protocollo.
	 */
	private Integer annoProtocollo;
	
	/**
	 * Data elenco trasmissione.
	 */
	private Date dataElencoTrasmissione;
	
	/**
	 * Nome file.
	 */
	private String nomeFile;
	
	/**
	 * Tipologia protocollo.
	 */
	private TipologiaProtocollazioneEnum tipologiaProtocollo;
	
	/**
	 * Content type.
	 */
	private String contentType;
	
	/**
	 * Content.
	 */
	private byte[] content;
	
	
	/**
	 * Registro Protocollo PMEF (recuperato da FileNet).
	 * 
	 * @param guid
	 * @param documentTitle
	 * @param dataProtocollo
	 * @param annoProtocollo
	 * @param dataElencoTrasmissione
	 * @param nomeFile
	 */
	public RegistroProtocolloDTO(final String guid, final String documentTitle, final Date dataProtocollo, final Integer annoProtocollo, final Date dataElencoTrasmissione, final String nomeFile) {
		super();
		this.guid = guid;
		this.documentTitle = documentTitle;
		this.dataProtocollo = dataProtocollo;
		this.annoProtocollo = annoProtocollo;
		this.dataElencoTrasmissione = dataElencoTrasmissione;
		this.nomeFile = nomeFile;
		this.tipologiaProtocollo = TipologiaProtocollazioneEnum.MEF;
	}
	
	
	/**
	 * Registro Protocollo NPS.
	 * 
	 * @param content
	 * @param contentType
	 * @param dataProtocollo
	 * @param annoProtocollo
	 * @param dataElencoTrasmissione
	 * @param nomeFile
	 */
	public RegistroProtocolloDTO(final byte[] content, final String contentType, final Date dataProtocollo, final Integer annoProtocollo, final Date dataElencoTrasmissione, final String nomeFile) {
		super();
		this.guid = UUID.randomUUID().toString(); // Auto-generato per gestire nel front-end le righe della tabella tramite la rowKey
		this.content = content;
		this.contentType = contentType;
		this.dataProtocollo = dataProtocollo;
		this.annoProtocollo = annoProtocollo;
		this.dataElencoTrasmissione = dataElencoTrasmissione;
		this.nomeFile = nomeFile;
		this.tipologiaProtocollo = TipologiaProtocollazioneEnum.NPS;
	}
	
	
	/**
	 * @return the guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * @return the documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * @return the dataProtocollo
	 */
	public Date getDataProtocollo() {
		return dataProtocollo;
	}

	/**
	 * @return the annoProtocollo
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * @return the dataElencoTrasmissione
	 */
	public Date getDataElencoTrasmissione() {
		return dataElencoTrasmissione;
	}

	/**
	 * @return the nomeFile
	 */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * @return the tipologiaProtocollo
	 */
	public final TipologiaProtocollazioneEnum getTipologiaProtocollo() {
		return tipologiaProtocollo;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @return the content
	 */
	public byte[] getContent() {
		return content;
	}
}