package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.ColonneCodeDTO;

/**
 * The Class IGestioneColonneFacadeSRV.
 *
 * @author VINGENITO
 * 
 *         SRV per gestione delle colonne;
 */
public interface IGestioneColonneFacadeSRV extends Serializable {

	/**
	 * Salva le colonne scelte.
	 * 
	 * @param idUtente
	 * @param listColCodeDTO
	 */
	void salvaColonneScelte(Long idUtente, List<ColonneCodeDTO> listColCodeDTO);

	/**
	 * Ottiene le colonne scelte.
	 * 
	 * @param idUtente
	 * @return lista delle colonne scelte
	 */
	List<ColonneCodeDTO> getColonneScelte(Long idUtente);

}