package it.ibm.red.business.enums;

/**
 * Enum contenente i nomi dei metadati
 * relativi ai report elenco notifiche
 * e conti consuntivi UCB.
 */
public enum ContiConsuntiviEnum {
	
	/**
	 * Sede estere.
	 */
	ANAGSEDEST("ANAGSEDEST"),
	
	/**
	 * Codici sedi estere.
	 */
	CODICE_SEDE_ESTERA("COD_SEDE_EST"),
	
	/**
	 * Descrizione sedi estere.
	 */
	DESCRIZIONE_SEDE_ESTERA("DESCR_SEDE_EST"),
	
	/**
	 * Esercizio.
	 */
	ESERCIZIO("ESERCIZIO"),
	
	/**
	 * Programma controllo UCB.
	 */
	PROGRAMMA_CONTROLLO_UCB("PROG_CONT_UCB"),
	
	/**
	 * Data chiusura.
	 */
	DATA_CHIUSURA("DATA_CHIUSURA"),
	
	/**
	 * Sedi estere.
	 */
	ANAG_SEDI("ANAG_SEDI");
	
	/**
	 * Nome metadato.
	 */
	private String metadatoName;
	
	/**
	 * Costruttore.
	 * 
	 * @param metadatoName
	 */
	private ContiConsuntiviEnum(String metadatoName) {
		this.metadatoName = metadatoName;
	}

	/**
	 * Restituisce il nome del metadato.
	 * 
	 * @return nome metadato
	 */
	public String getMetadatoName() {
		return metadatoName;
	}

}
