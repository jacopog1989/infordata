/**
 * 
 */
package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import it.ibm.red.business.dto.AllaccioRiferimentoStoricoDTO;
import it.ibm.red.business.dto.RiferimentoProtNpsDTO;
import it.ibm.red.business.dto.RiferimentoStoricoNsdDTO;

/**
 * @author VINGENTIO
 *
 */
public interface IRiferimentoStoricoDAO extends Serializable {

	/**
	 * Inserisce il riferimento storico.
	 * 
	 * @param docTitleUscita
	 * @param nProtocolloNsd
	 * @param idFilePrincipale
	 * @param dataCreazione
	 * @param dataAgg
	 * @param idFascicoloProc
	 * @param descrFascicoloProc
	 * @param idAoo
	 * @param oggettoProtNsd
	 * @param annoProtNsd
	 * @param connection
	 * @return
	 */
	int insertRiferimentoStorico(String docTitleUscita, Integer nProtocolloNsd, String idFilePrincipale, Date dataCreazione, Date dataAgg, String idFascicoloProc,
			String descrFascicoloProc, Long idAoo, String oggettoProtNsd, Integer annoProtNsd, Connection connection);

	/**
	 * Ottiene il riferimento storico tramite aoo.
	 * 
	 * @param docTitle
	 * @param idAoo
	 * @param idFascProcedimentale
	 * @param conn
	 * @return lista di allacci
	 */
	List<AllaccioRiferimentoStoricoDTO> getRiferimentoStoricoByAoo(String docTitle, Long idAoo, String idFascProcedimentale, Connection conn);

	/**
	 * Ottiene il riferimento storico.
	 * 
	 * @param docTitle
	 * @param idAoo
	 * @param conn
	 * @return lista di riferimenti storici Nsd
	 */
	List<RiferimentoStoricoNsdDTO> getRiferimentoStorico(String docTitle, Long idAoo, Connection conn);

	/**
	 * Cancella il riferimento storico.
	 * 
	 * @param docTitleUscita
	 * @param idAoo
	 * @param connection
	 * @return
	 */
	int deleteRiferimentoStoricoModifica(String docTitleUscita, Long idAoo, Connection connection);

	/**
	 * Esegue l'update dell'id del fascicolo procedimentale.
	 * 
	 * @param idFascicoloProcedimentale
	 * @param docTitleUscita
	 * @param idAoo
	 * @param conn
	 * @return
	 */
	int updateIdFascicoloProcedimentaleSposta(String idFascicoloProcedimentale, String docTitleUscita, Long idAoo, Connection conn);

	/**
	 * Elimina il riferimento all'allaccio NPS dal documento identificato dal document title: <code> docTitleUscita </code>.
	 * @param docTitleUscita
	 * @param idAoo
	 * @param connection
	 * @return true se viene eliminato l'allaccio, false altrimenti
	 */
	boolean deleteRiferimentoAllaccioNpsModifica(String docTitleUscita, Long idAoo, Connection connection);

	/**
	 * Restituisce i riferimento allaccio NPS associati al documento identificato dal document title: <code> docTitle </code>.
	 * @see RiferimentoProtNpsDTO.
	 * @param docTitle
	 * @param idAoo
	 * @param conn
	 * @return lista dei riferimenti allaccio NPS esistenti sul documento
	 */
	List<RiferimentoProtNpsDTO> getRiferimentoAllaccioNps(String docTitle, Long idAoo, Connection conn);

	/**
	 * Restituisce i riferimenti allacci NPS come {@link #getRiferimentoAllaccioNps(String, Long, Connection)} specificando il fascicolo
	 * attraverso l'id dello stesso: <code> idFascProcedimentale </code>.
	 * @param docTitle
	 * @param idAoo
	 * @param idFascProcedimentale
	 * @param conn
	 * @return lista riferimenti allaccio NPS
	 */
	List<RiferimentoProtNpsDTO> getRiferimentoNpsByAoo(String docTitle, Long idAoo, String idFascProcedimentale, Connection conn);

	/**
	 * Effettua la insert del riferimento allaccio NPS.
	 * @param idAoo
	 * @param annoProt
	 * @param nProtocollo
	 * @param dataCreazione
	 * @param docTitleUscita
	 * @param idFascicoloProcedimentale
	 * @param oggetto
	 * @param idDocumentoDownload
	 * @param connection
	 * @return true se l'inserimento è stato eseguito senza errori, false altrimenti
	 */
	boolean insertRifAllaccioNps(Long idAoo, Integer annoProt, Integer nProtocollo, Date dataCreazione,String docTitleUscita, String idFascicoloProcedimentale, String oggetto, String idDocumentoDownload,Connection connection);
}
