package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import it.ibm.red.business.logger.REDLogger;
import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IMezzoRicezioneDAO;
import it.ibm.red.business.dto.MezzoRicezioneDTO;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.exception.RedException;

/**
 * @author Simone Lacarpia
 *
 */
@Repository
public class MezzoRicezioneDAO extends AbstractDAO implements IMezzoRicezioneDAO {

	private static final String IDFORMATODOCUMENTO = "IDFORMATODOCUMENTO";

	private static final String DESCRIZIONE = "DESCRIZIONE";

	private static final String IDMEZZORICEZIONE = "IDMEZZORICEZIONE";

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Messaggio errore recupero mezzi.
	 */
	private static final String ERROR_RECUPERO_MEZZI_MSG = "Errore durante il recupero dei mezzi";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(MezzoRicezioneDAO.class);

	/**
	 * @see it.ibm.red.business.dao.IMezzoRicezioneDAO#getAllMezziRicezione(java.sql.Connection).
	 */
	@Override
	public List<MezzoRicezioneDTO> getAllMezziRicezione(final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<MezzoRicezioneDTO> list = null; 
		
		try {
			String querySQL = "select * from MEZZORICEZIONE m";
			
			ps = connection.prepareStatement(querySQL);
			
			rs = ps.executeQuery();
			
			list = new ArrayList<>();
			
			MezzoRicezioneDTO item = null;
			while (rs.next()) {
				item = new MezzoRicezioneDTO(
						rs.getInt(IDMEZZORICEZIONE), 
						rs.getString(DESCRIZIONE), 
						FormatoDocumentoEnum.getEnumById(rs.getLong(IDFORMATODOCUMENTO)));
				list.add(item);
			}
		} catch (Exception e) {
			LOGGER.error(ERROR_RECUPERO_MEZZI_MSG, e);
			throw new RedException(ERROR_RECUPERO_MEZZI_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return list;
	}

	/**
	 * @see it.ibm.red.business.dao.IMezzoRicezioneDAO#getMezzoRicezioneByIdMezzo(java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public MezzoRicezioneDTO getMezzoRicezioneByIdMezzo(final Integer idMezzoRicezione, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		MezzoRicezioneDTO item = null;
		try {
			int i = 1;
			String querySQL = "select * from MEZZORICEZIONE m where m.idmezzoricezione = ?";
			
			ps = connection.prepareStatement(querySQL);
			ps.setInt(i, idMezzoRicezione);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				item = new MezzoRicezioneDTO(
						rs.getInt(IDMEZZORICEZIONE), 
						rs.getString(DESCRIZIONE), 
						FormatoDocumentoEnum.getEnumById(rs.getLong(IDFORMATODOCUMENTO)));
			}
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero del mezzo : " + idMezzoRicezione.intValue(), e);
			throw new RedException(ERROR_RECUPERO_MEZZI_MSG + idMezzoRicezione.intValue(), e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return item;
	}

	/**
	 * @see it.ibm.red.business.dao.IMezzoRicezioneDAO#getMezziRicezioneByIdFormato(it.ibm.red.business.enums.FormatoDocumentoEnum,
	 *      java.sql.Connection).
	 */
	@Override
	public List<MezzoRicezioneDTO> getMezziRicezioneByIdFormato(final FormatoDocumentoEnum formatoDocumento,
			final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<MezzoRicezioneDTO> list = null; 
		
		try {
			int i = 1;
			String querySQL = "select * from MEZZORICEZIONE m where m.idformatodocumento = ?";
			
			ps = connection.prepareStatement(querySQL);
			ps.setLong(i, formatoDocumento.getId());
			
			rs = ps.executeQuery();
			
			list = new ArrayList<>();
			
			MezzoRicezioneDTO item = null;
			while (rs.next()) {
				item = new MezzoRicezioneDTO(
						rs.getInt(IDMEZZORICEZIONE), 
						rs.getString(DESCRIZIONE), 
						FormatoDocumentoEnum.getEnumById(rs.getLong(IDFORMATODOCUMENTO)));
				list.add(item);
			}
		} catch (Exception e) {
			LOGGER.error(ERROR_RECUPERO_MEZZI_MSG, e);
			throw new RedException(ERROR_RECUPERO_MEZZI_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return list;
	}


}
