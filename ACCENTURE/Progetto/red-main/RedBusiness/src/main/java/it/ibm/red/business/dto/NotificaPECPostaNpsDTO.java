/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.ArrayList;
import java.util.List;

import it.ibm.red.business.enums.TipoDestinatarioDatiCertEnum;
import it.ibm.red.business.enums.TipoMessaggioPostaNpsIngressoEnum;
import it.ibm.red.business.enums.TipoNotificaPecEnum;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraNotificaPECType;
import it.ibm.red.webservice.model.interfaccianps.postacerttypes.Destinatari;
import it.ibm.red.webservice.model.interfaccianps.postacerttypes.PostacertType;

/**
 * Rappresenta un messaggio di notifica PEC in ingresso ricevuto tramite NPS.
 * 
 * @author m.crescentini
 *
 */
public class NotificaPECPostaNpsDTO extends MessaggioPostaNpsDTO {

	/** 
	 * The Constant serialVersionUID. 
	 */
	private static final long serialVersionUID = 2983640695454694220L;
		
	/** 
	 * Enum tipo notifica Pec 
	 */
	private TipoNotificaPecEnum tipoNotificaPec;
	
	/** 
	 * Dto per i dati cert della pec 
	 */
	private DatiCertDTO datiCert;


	/**
	 * Costruttore per notifica PEC.
	 *
	 * @param richiesta the richiesta
	 */
	public NotificaPECPostaNpsDTO(final RichiestaElaboraNotificaPECType richiesta) {
		super(richiesta.getIdMessaggio(), richiesta.getIdDocumento(), TipoMessaggioPostaNpsIngressoEnum.NOTIFICA_PEC, richiesta.getMessaggio());
		
		// Tipo Notifica PEC
		if (richiesta.getTipoNotificaPEC() != null) {
			tipoNotificaPec = TipoNotificaPecEnum.get(richiesta.getTipoNotificaPEC().getTipoNotifica());
		}
		
		// Daticert
		final PostacertType inDatiCert = richiesta.getDatiCert();
		if (inDatiCert != null) {
			final List<DestinatarioDatiCertDTO> destinatariDatiCert = new ArrayList<>();
			DestinatarioDatiCertDTO destinatarioDatiCert = null;
			
			for (final Destinatari dest : inDatiCert.getIntestazione().getDestinatari()) {
				destinatarioDatiCert = new DestinatarioDatiCertDTO(TipoDestinatarioDatiCertEnum.get(dest.getTipo()), dest.getContent());
				
				destinatariDatiCert.add(destinatarioDatiCert);
			}
			
			this.datiCert = new DatiCertDTO(TipoNotificaPecEnum.get(inDatiCert.getTipo()), inDatiCert.getDati().getMsgid(), 
					inDatiCert.getDati().getIdentificativo(), inDatiCert.getIntestazione().getOggetto(), inDatiCert.getDati().getConsegna(), 
					inDatiCert.getIntestazione().getMittente(), destinatariDatiCert, inDatiCert.getErrore());
		}
	}

	/** 
	 *
	 * @return the tipoNotificaPec
	 */
	public TipoNotificaPecEnum getTipoNotificaPec() {
		return tipoNotificaPec;
	}


	/** 
	 *
	 * @return the datiCert
	 */
	public DatiCertDTO getDatiCert() {
		return datiCert;
	}

}
