package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;

import it.ibm.red.business.persistence.model.Property;

/**
 * 
 * @author CPIERASC
 *
 *	Interfaccia del DAO per la gestione delle properties.
 */
public interface IPropertiesDAO extends Serializable {

	/**
	 * Metodo per il recupero di tutte properties NON blob.
	 * 
	 * @param connection	connessione al db
	 * @return				collezione di properties recuperate
	 */
	Collection<Property> getAllValues(Connection connection);
	
	
	/**
	 * Metodo per il recupero di tutte properties BLOB.
	 * 
	 * @param connection	connessione al db
	 * @return				collezione di properties recuperate
	 */
	Collection<Property> getAllBlobValues(Connection connection);

	/**
	 * Recupero di una property da db.
	 * 
	 * @param key			chiave della property
	 * @param connection	connessione al db
	 * @return				valore della property
	 */
	String getByEnum(String key, Connection connection);
}
