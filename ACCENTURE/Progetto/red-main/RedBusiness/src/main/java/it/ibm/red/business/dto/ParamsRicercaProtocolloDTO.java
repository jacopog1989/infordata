package it.ibm.red.business.dto;

import java.util.Calendar;
import java.util.Date;

import it.ibm.red.business.enums.TipoProtocolloEnum;


/**
 * DTO per la ricerca dei protocolli.
 * 
 * @author m.crescentini
 *
 */
public class ParamsRicercaProtocolloDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7194030277197156111L;
	
	/**
	 * Anno protocollo
	 */
	private Integer annoProtocollo;
	
	/**
	 * Flag Anno protocollo
	 */
	private boolean annoProtocolloDisabled;

	/**
	 * Numero protocollo da
	 */
	private Integer numeroProtocolloDa;
	
	/**
	 * Numero protocollo a
	 */
	private Integer numeroProtocolloA;
	
	/**
	 * Data protocollo da
	 */
	private Date dataProtocolloDa;
	
	/**
	 * Data protocollo a
	 */
	private Date dataProtocolloA;
	
	/**
	 * Mittente
	 */
	private String mittente;
	
	/**
	 * Destinatario
	 */
	private String destinatario;
	
	/**
	 * Stato
	 */
	private String stato;
	
	/**
	 * Tipo protocollo
	 */
	private TipoProtocolloEnum tipoProtocollo;
	
	/**
	 * tipologia documento NPS
	 */
	private String tipologiaDocumento;
	
	/**
	 * Oggetto
	 */
	private String oggetto;
	
	/**
	 * Flag annullato
	 */
	private boolean flagAnnullato;
	
	/**
	 * Costruttore params ricerca protocollo DTO.
	 */
	public ParamsRicercaProtocolloDTO() {
		this.annoProtocollo = Calendar.getInstance().get(Calendar.YEAR);
	}
	
	/** 
	 *
	 * @return the annoProtocollo
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/** 
	 *
	 * @param annoProtocollo the annoProtocollo to set
	 */
	public void setAnnoProtocollo(final Integer annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/** 
	 *
	 * @return the numeroProtocolloDa
	 */
	public Integer getNumeroProtocolloDa() {
		return numeroProtocolloDa;
	}

	/** 
	 *
	 * @param numeroProtocolloDa the numeroProtocolloDa to set
	 */
	public void setNumeroProtocolloDa(final Integer numeroProtocolloDa) {
		this.numeroProtocolloDa = numeroProtocolloDa;
	}

	/** 
	 *
	 * @return the numeroProtocolloA
	 */
	public Integer getNumeroProtocolloA() {
		return numeroProtocolloA;
	}

	/** 
	 *
	 * @param numeroProtocolloA the numeroProtocolloA to set
	 */
	public void setNumeroProtocolloA(final Integer numeroProtocolloA) {
		this.numeroProtocolloA = numeroProtocolloA;
	}

	/** 
	 *
	 * @return the dataProtocolloDa
	 */
	public Date getDataProtocolloDa() {
		return dataProtocolloDa;
	}

	/** 
	 *
	 * @param dataProtocolloDa the dataProtocolloDa to set
	 */
	public void setDataProtocolloDa(final Date dataProtocolloDa) {
		this.dataProtocolloDa = dataProtocolloDa;
	}

	/** 
	 *
	 * @return the dataProtocolloA
	 */
	public Date getDataProtocolloA() {
		return dataProtocolloA;
	}

	/** 
	 *
	 * @param dataProtocolloA the dataProtocolloA to set
	 */
	public void setDataProtocolloA(final Date dataProtocolloA) {
		this.dataProtocolloA = dataProtocolloA;
	}

	/** 
	 *
	 * @return the mittente
	 */
	public String getMittente() {
		return mittente;
	}

	/** 
	 *
	 * @param mittente the mittente to set
	 */
	public void setMittente(final String mittente) {
		this.mittente = mittente;
	}

	/** 
	 *
	 * @return the destinatario
	 */
	public String getDestinatario() {
		return destinatario;
	}

	/** 
	 *
	 * @param destinatario the destinatario to set
	 */
	public void setDestinatario(final String destinatario) {
		this.destinatario = destinatario;
	}
	
	/** 
	 *
	 * @return the stato
	 */
	public String getStato() {
		return stato;
	}

	/** 
	 *
	 * @param stato the stato to set
	 */
	public void setStato(final String stato) {
		this.stato = stato;
	}

	/** 
	 *
	 * @return the tipoProtocollo
	 */
	public TipoProtocolloEnum getTipoProtocollo() {
		return tipoProtocollo;
	}

	/** 
	 *
	 * @param tipoProtocollo the tipoProtocollo to set
	 */
	public void setTipoProtocollo(final TipoProtocolloEnum tipoProtocollo) {
		this.tipoProtocollo = tipoProtocollo;
	}

	/** 
	 *	Get tipologiaDocumento.
	 * @return the tipologiaDocumento
	 */
	public final String getTipologiaDocumento() {
		return tipologiaDocumento;
	}

	/** 
	 *	Set tipologiaDocumento
	 * @param tipologiaDocumento the tipologiaDocumento to set
	 */
	public final void setTipologiaDocumento(final String tipologiaDocumento) {
		this.tipologiaDocumento = tipologiaDocumento;
	}

	/** 
	 *
	 * @return the oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/** 
	 *
	 * @param oggetto the oggetto to set
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/** 
	 *
	 * @return true, if is anno protocollo disabled
	 */
	public boolean isAnnoProtocolloDisabled() {
		return annoProtocolloDisabled;
	}

	/** 
	 *
	 * @param annoProtocolloDisabled the new anno protocollo disabled
	 */
	public void setAnnoProtocolloDisabled(final boolean annoProtocolloDisabled) {
		this.annoProtocolloDisabled = annoProtocolloDisabled;
	}
	
	/** 
	 *
	 * @return the flag annullato
	 */
	public boolean getFlagAnnullato() {
		return flagAnnullato;
	}
	
	/**  
	 *
	 * @param flagAnnullato the new flag annullato
	 */
	public void setFlagAnnullato(final boolean flagAnnullato) {
		this.flagAnnullato = flagAnnullato;
	}
	
}