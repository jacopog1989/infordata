package it.ibm.red.business.dao.impl;
 
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.utils.StringUtils;

/**
 * Classe builder per la costruzione di query.
 */
public abstract class SimpleQueryBuilder extends AbstractDAO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Tabella.
	 */
	private final String table;


	/**
	 * Parametri.
	 */
	private transient Map<String, Object> params;
	
	/**
	 * Costruttore.
	 * @param inTable
	 */
	protected SimpleQueryBuilder(final String inTable) {
		params = new HashMap<>();
		table = inTable;
	}

	/**
	 * Restituisce la lista dei parametri.
	 * @return parametri query
	 */
	public Map<String, Object> getParams() {
		return params;
	}

	/**
	 * Restituisce la tabella su cui eseguire la query.
	 * @return tabella query
	 */
	public String getTable() {
		return table;
	}
	
	/**
	 * Valorizza il parametro nella posizione i con il valore: value gestendone il tipo.
	 * Valido per tipi: Date, Integer, Long, String.
	 * @param psInsert
	 * @param index
	 * @param value
	 * @throws SQLException
	 */
	public void handleParams(final PreparedStatement psInsert, final Integer index, final Object value) throws SQLException {
		if (value instanceof java.sql.Date) {
			final java.sql.Date d = (java.sql.Date) value;
			psInsert.setDate(index, d);
		} else if (value instanceof Integer) {
			final Integer i = (Integer) value;
			psInsert.setInt(index, i);
		} else if (value instanceof Long) {
			final Long l = (Long) value;
			psInsert.setLong(index, l);
		} else {
			final String s = (String) value;
			psInsert.setString(index, s);
		}
	}

	/**
	 * Gestisce l'aggiunta del parametro gestendo eventualmente null value.
	 * @param handleNull
	 * @param key
	 * @param value
	 */
	protected void addParams(final Boolean handleNull, final String key, final Object value) {
		addParams(handleNull, true, key, value);
	}

	/**
	 * Gestisce l'aggiunta del parametro.
	 * @param handleNull se true gestisce i null, false non gestisce assenza valorizzazione
	 * @param handleEmptyString
	 * @param key
	 * @param value
	 */
	protected void addParams(final Boolean handleNull, final Boolean handleEmptyString, final String key, final Object value) {
		Object tmpValue = value;
		if (Boolean.FALSE.equals(handleEmptyString) && tmpValue instanceof String) {
			final String str = (String) tmpValue;
			if (StringUtils.isNullOrEmpty(str)) {
				tmpValue = null;
			}
		}
		
		if (Boolean.TRUE.equals(handleNull) || tmpValue != null) {
			if (tmpValue instanceof Date) {
				final Date d = (Date) tmpValue;
				params.put(key, new java.sql.Date(d.getTime()));
			} else if (tmpValue instanceof Boolean) {
				final Boolean b = (Boolean) tmpValue;
				String v = "N";
				if (Boolean.TRUE.equals(b)) {
					v = "S";
				}
				params.put(key, v);
			} else {
				params.put(key, tmpValue);
			}
		}
	}

}
