package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ICapitoloSpesaDAO;
import it.ibm.red.business.dto.CapitoloSpesaDTO;
import it.ibm.red.business.dto.RicercaCapitoloSpesaDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class CapitoloSpesaDAO.
 *
 * @author mcrescentini
 * 
 * 	DAO per la gestione dei capitoli di spesa.
 */
@Repository
public class CapitoloSpesaDAO extends AbstractDAO implements ICapitoloSpesaDAO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -6156937938043769522L;

	/**
	 * Effettuato.
	 */
	private static final String EFFETTUATO_LITERAL = "effettuato";

	/**
	 * TAG di inizio operazione - LOGGER.
	 */
	private static final String START_LOG = "] -> START";

	/**
	 * TAG di fine operazione - LOGGER.
	 */
	private static final String END_LOG = "] -> END";

	/**
	 * Negazione forte.
	 */
	private static final String NOT_LITERAL = " NON ";

	/**
	 * Messaggio inserimento di un capitolo di spesa, occorre appendere il codice del capitolo spesa.
	 */
	private static final String INSERIMENTO_CAPITOLO_SPESA_MSG = "Inserimento di un capitolo di spesa con codice [";

	/**
	 * Messaggio aggiornamento capitolo spesa, occorre appendere il codice del capitolo spesa.
	 */
	private static final String AGGIORNAMENTO_CAPITOLO_SPESA_MSG = "Aggiornamento di un capitolo di spesa con codice [";
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CapitoloSpesaDAO.class.getName());
	
	/**
	 * Select get capitolo per codice.
	 */
	private static final String SELECT_BY_CODICE = String.join(" ", "SELECT CODICE, DESCRIZIONE, ANNO, FLAGSENTENZA",
			"FROM CAPITOLO_SPESA WHERE CODICE = ?");
	
	/**
	 * Insert capitolo.
	 */
	private static final String INSERT_CAPITOLO = String.join("", "INSERT INTO CAPITOLO_SPESA",
			"(CODICE, DESCRIZIONE, ANNO, FLAGSENTENZA) VALUES (?, ?, ?, ?)");
	
	/**
	 * Aggiorna capitolo.
	 */
	private static final String UPDATE_CAPITOLO = String.join("", "UPDATE CAPITOLO_SPESA",
			" SET DESCRIZIONE = ?, ANNO = ?, FLAGSENTENZA = ? WHERE CODICE = ?");
	
	/**
	 * Inser associazione.
	 */
	private static final String INSERT_ASSOCIAZIONE_AOO = String.join("", "INSERT INTO CAPITOLO_SPESA_AOO",
			"(CODICECAPITOLO, IDAOO) VALUES (?, ?)");

	
	/**
	 * Restituisce il capitolo identificato dal codice univoco.
	 * 
	 * @see it.ibm.red.business.dao.ICapitoloSpesaDAO#getByCodice(java.lang.String,
	 *      java.sql.Connection).
	 * @param codice
	 *            id del capitolo spesa
	 * @param con
	 *            connession al database
	 * @return informazioni sul capitolo spesa nel CapitoloSpesaDTO
	 */
	@Override
	public final CapitoloSpesaDTO getByCodice(final String codice, final Connection con) {
		LOGGER.info("Recupero del capitolo [" + codice + START_LOG);
		CapitoloSpesaDTO capitoloSpesa = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = con.prepareStatement(SELECT_BY_CODICE);
			ps.setString(1, codice);
			
			// Esecuzione della SELECT
			rs = ps.executeQuery();
			
			if (rs.next()) {
				capitoloSpesa = populate(rs);
			}
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		LOGGER.info("Recupero del capitolo [" + codice + END_LOG);
		return capitoloSpesa;
	}
	

	/**
	 * @see it.ibm.red.business.dao.ICapitoloSpesaDAO#inserisci( java.lang.String,
	 *      java.lang.String, java.lang.Integer, java.lang.Boolean,
	 *      java.sql.Connection).
	 * @param codice
	 *            codice del capitolo spesa da aggiornare
	 * @param descrizione
	 *            descrizione del capitolo da impostare
	 * @param annoFinanziario
	 *            anno finanziario da impostare
	 * @param flagSentenza
	 *            flag sentenza da impostare
	 * @param con
	 *            connession al database
	 */
	@Override
	public final void aggiorna(final String codice, final String descrizione, final Integer annoFinanziario, 
			final Boolean flagSentenza, final Connection con) {
		LOGGER.info(AGGIORNAMENTO_CAPITOLO_SPESA_MSG + codice + START_LOG);
		PreparedStatement ps = null;
		
		try {
			ps = con.prepareStatement(UPDATE_CAPITOLO);
			ps.setString(1, descrizione);
			ps.setInt(2, annoFinanziario);
			ps.setInt(3, Boolean.TRUE.equals(flagSentenza) 
					? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());
			ps.setString(4, codice);
			final int output = ps.executeUpdate();
			
			LOGGER.info(AGGIORNAMENTO_CAPITOLO_SPESA_MSG + codice + "]" 
					+ (output > 0 ? " " : NOT_LITERAL) + EFFETTUATO_LITERAL);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		
		LOGGER.info(AGGIORNAMENTO_CAPITOLO_SPESA_MSG + codice + END_LOG);
	}
	
	@Override
	public final void inserisci(final String codice, final String descrizione, final Integer annoFinanziario, 
			final Boolean flagSentenza, final Connection con) {
		LOGGER.info(INSERIMENTO_CAPITOLO_SPESA_MSG + codice + START_LOG);
		PreparedStatement ps = null;
		
		try {
			ps = con.prepareStatement(INSERT_CAPITOLO);
			ps.setString(1, codice);
			ps.setString(2, descrizione);
			ps.setInt(3, annoFinanziario);
			ps.setInt(4, Boolean.TRUE.equals(flagSentenza) 
					? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());
			
			final int output = ps.executeUpdate();
			
			LOGGER.info(INSERIMENTO_CAPITOLO_SPESA_MSG + codice + "]" 
					+ (output > 0 ? " " : NOT_LITERAL) + EFFETTUATO_LITERAL);
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		
		LOGGER.info(INSERIMENTO_CAPITOLO_SPESA_MSG + codice + END_LOG);
	}

	
	/**
	 * @see it.ibm.red.business.dao.ICapitoloSpesaDAO#inserisciMultipli(java.util.List,
	 *      java.sql.Connection).
	 * @param capitoliSpesa
	 *            lista codici capitolo spesa da inserire
	 * @param con
	 *            connession al database
	 */
	@Override
	public final void inserisciMultipli(final List<CapitoloSpesaDTO> capitoliSpesa, final Connection con) {
		LOGGER.info("Inserimento massivo di capitoli di spesa -> START");
		PreparedStatement ps = null;
		
		try {
			ps = con.prepareStatement(INSERT_CAPITOLO);
			
			for (final CapitoloSpesaDTO capitoloSpesa : capitoliSpesa) {
				ps.setString(1, capitoloSpesa.getCodice());
				ps.setString(2, capitoloSpesa.getDescrizione());
				ps.setInt(3, capitoloSpesa.getAnno());
				ps.setInt(4, Boolean.TRUE.equals(capitoloSpesa.getFlagSentenza()) 
						? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());

				ps.addBatch();
			}
			
			// Esecuzione della INSERT MASSIVA
			final int[] result = ps.executeBatch();
			
			LOGGER.info("Inserimento massivo di capitoli di spesa" + (result.length > 0 ? " " : NOT_LITERAL) + EFFETTUATO_LITERAL);
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		
		LOGGER.info("Inserimento massivo di capitoli di spesa -> END");
	}

	/**
	 * @see it.ibm.red.business.dao.ICapitoloSpesaDAO#inserisciAssociazioniAoo(java.util.List,
	 *      java.lang.Long, java.sql.Connection).
	 * @param codiciCapitoliSpesa
	 *            lista di codici dei capitoli di spesa da inserire
	 * @param idAoo
	 *            identificativo dell'AOO
	 * @param con
	 *            connession al database
	 */
	@Override
	public final void inserisciAssociazioniAoo(final List<String> codiciCapitoliSpesa, final Long idAoo, final Connection con) {
		LOGGER.info("Inserimento massivo di associazioni tra capitolo di spesa e AOO -> START");
		PreparedStatement ps = null;
		
		try {
			ps = con.prepareStatement(INSERT_ASSOCIAZIONE_AOO);
			
			for (final String codiceCapitoloSpesa : codiciCapitoliSpesa) {
				ps.setString(1, codiceCapitoloSpesa);
				ps.setLong(2, idAoo);

				ps.addBatch();
			}
			
			// Esecuzione della INSERT MASSIVA
			final int[] result = ps.executeBatch();
			
			LOGGER.info("Inserimento massivo di associazioni tra capitolo di spesa e AOO" 
					+ (result.length > 0 ? " " : NOT_LITERAL) + EFFETTUATO_LITERAL);
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		
		LOGGER.info("Inserimento massivo di associazioni tra capitolo di spesa e AOO -> END");
	}
	
	
	/**
	 * @see it.ibm.red.business.dao.ICapitoloSpesaDAO#eliminaAssociazioniAoo(java.util.List,
	 *      java.lang.Long, java.sql.Connection).
	 * @param codiciCapitoliSpesa
	 *            lista dei codici dei capitoli spesa da cui rimuovere
	 *            l'associazione con l'area organizzativa
	 * @param idAoo
	 *            identificativo dell'AOO a cui sono associati i capitoli
	 * @param con
	 *            connessione al database
	 */
	@Override
	public final void eliminaAssociazioniAoo(final List<String> codiciCapitoliSpesa, final Long idAoo, final Connection con) {
		LOGGER.info("Eliminazione massiva di associazioni tra capitolo di spesa e AOO -> START");
		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement(String.join(" ", 
					"DELETE FROM CAPITOLO_SPESA_AOO WHERE IDAOO =", idAoo.toString(), "AND (",
					StringUtils.createInCondition("CODICECAPITOLO", codiciCapitoliSpesa.iterator(), true),
					")"));
			
			// Esecuzione della DELETE MASSIVA
			final int output = ps.executeUpdate();
			
			LOGGER.info("Eliminazione massiva di associazioni tra capitolo di spesa e AOO" 
					+ (output == codiciCapitoliSpesa.size() ? " " : NOT_LITERAL) + "effettuata");
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		
		LOGGER.info("Eliminazione massiva di associazioni tra capitolo di spesa e AOO -> END");
	}
	
	
	/**
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private static CapitoloSpesaDTO populate(final ResultSet rs) throws SQLException {
		return new CapitoloSpesaDTO(rs.getString("CODICE"), rs.getString("DESCRIZIONE"), rs.getInt("ANNO"), 
				BooleanFlagEnum.SI.getIntValue().equals(rs.getInt("FLAGSENTENZA")));
	}

	/**
	 * @see it.ibm.red.business.dao.ICapitoloSpesaDAO#ricerca(java.sql.Connection,
	 *      it.ibm.red.business.dto.RicercaCapitoloSpesaDTO).
	 */
	@Override
	public Collection<CapitoloSpesaDTO> ricerca(final Connection con, final RicercaCapitoloSpesaDTO ricercaDTO) {
		final Collection<CapitoloSpesaDTO> output = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			final StringBuilder query = new StringBuilder()
					.append("SELECT c.CODICE, c.DESCRIZIONE, c.ANNO, c.FLAGSENTENZA, ")
					.append("       (select n.DESCRIZIONE from NODO n where n.IDNODO = csa.IDNODOASS) as UFFICIO, ")
					.append("       (select count(*) from CAPITOLO_SPESA_AOO_RISERVATI r where r.CODICECAPITOLO = c.CODICE and r.IDAOO = ca.IDAOO) as RISERVATO ")
					.append("  FROM CAPITOLO_SPESA c, CAPITOLO_SPESA_AOO ca, CAPITOLI_SPESA_ASSEGNAZIONI csa ")
					.append(" WHERE c.CODICE = ca.CODICECAPITOLO ")
					.append("   AND ca.IDAOO = ? ")
					.append("   AND UPPER(c.CODICE) LIKE UPPER(?) ")
					.append("   AND UPPER(c.DESCRIZIONE) LIKE UPPER(?) ")
					.append("   AND c.CODICE = csa.CODICECAPITOLO(+) ")
					.append("   AND csa.IDAOO(+) = ? ")
					.append(" ORDER BY C.CODICE ASC ");
			
			ps = con.prepareStatement(query.toString());
			ps.setLong(1, ricercaDTO.getIdAoo());
			ps.setString(2, "%" + ricercaDTO.getCodice() + "%");
			ps.setString(3, "%" + ricercaDTO.getDescrizione() + "%");
			ps.setLong(4, ricercaDTO.getIdAoo());
			
			rs = ps.executeQuery();
			while (rs.next()) {
				final CapitoloSpesaDTO capitolo = new CapitoloSpesaDTO();
				capitolo.setCodice(rs.getString("CODICE"));
				capitolo.setDescrizione(rs.getString("DESCRIZIONE"));
				capitolo.setAnno(rs.getInt("ANNO"));
				capitolo.setUfficioCompetente(rs.getString("UFFICIO"));
				capitolo.setFlagRiservato(rs.getInt("RISERVATO") > 0);
				output.add(capitolo);
			}			
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return output;
	}

}
