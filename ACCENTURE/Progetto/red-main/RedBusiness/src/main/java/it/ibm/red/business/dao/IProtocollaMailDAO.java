package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;

import it.ibm.red.business.dto.ProtocollaMailAooConfDTO;
import it.ibm.red.business.dto.ProtocollaMailCodaDTO;
import it.ibm.red.business.enums.MailOperazioniEnum;

/**
 * 
 * @author adilegge
 *
 *	Interfaccia del dao per la gestione della coda inoltro rifiuto
 */
public interface IProtocollaMailDAO extends Serializable {
	
	/**
	 * Recupera le configurazioni relative all'operazione per l'AOO in input
	 * @param idAoo
	 * @param idAoo
	 * @param conn
	 * @return
	 */
	ProtocollaMailAooConfDTO getConfigurazione(Long idAoo, MailOperazioniEnum operazione, Connection conn);
	
	/**
	 * Recupera il primo item in coda, lockandone l'occorrenza
	 * 
	 * @param idAoo
	 * @param conn
	 * @return
	 */
	ProtocollaMailCodaDTO getFirstInCodaLocked(Long idAoo, Connection conn);
	
	/**
	 * Aggiorna l'item con lo stato in input. Aggiorna gli ulteriori valori se diversi da null
	 * 
	 * @param idCoda id coda da aggiornare
	 * @param idStato
	 * @param descrizioneErrore
	 * @param idDocumentoEntrata
	 * @param wobNumberEntrata
	 * @param idDocumentoUscita
	 * @param wobNumberUscita
	 * @param conn
	 */
	void updateCoda(Long idCoda, Integer idStato, String descrizioneErrore, String idDocumentoEntrata, String wobNumberEntrata, String idDocumentoUscita, String wobNumberUscita, Connection conn);
	
	/**
	 * Inserisci in coda l'item in input
	 * 
	 * @param item
	 * @param conn
	 */
	void inserisciInCoda(ProtocollaMailCodaDTO item, Connection conn);
	
	/**
	 * Verifica l'esistenza di eventuali item da elaborare o presi in carico per la mail in input
	 * 
	 * @param guidMail
	 * @param idAoo
	 * @param conn
	 * @return
	 */
	boolean existItemsToProcess(String guidMail, Long idAoo, Connection conn);
	
}
