package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DetailEmailDTO;
import it.ibm.red.business.dto.HierarchicalFileWrapperDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio di scompattamento mail.
 */
public interface IScompattaMailFacadeSRV extends Serializable {

	/**
	 * Data l'id di filenet di una mail scompatta ricorsivamente ciascuno dei suoi
	 * allegati e costruisce una struttura ad albero. La struttura ad albero
	 * contiene infomrazioni relative ai metadati degli allegati che permettono
	 * eventualmente di scaricare l'allegato.
	 * 
	 * @param utente
	 * @param guid
	 * @param mantieniAllegatiOriginali Consente di evitare lo scompattamento e
	 *                                  ritornare esclusivamente gli allegati del
	 *                                  documento salvati su filenet.
	 * 
	 * @return
	 */
	List<HierarchicalFileWrapperDTO> scompattaMail(UtenteDTO utente, String guid, boolean mantieniAllegatiOriginali);


	/**
	 * Estrae il contenuto di un allegato dell'email leggendone il suo nodeId
	 * 
	 * Il nodeId è un identificativo che permette di individuare l'allegato e
	 * corrisponde al path dell'allegato, come se ogni allegato fosse una directory.
	 * Fin dove è possibile gli allegati sono individuati dall'id di FileNet, poi
	 * successivamente vengono usati i loro documentTitle/fileName.
	 * 
	 * Non è possibile utilizzare questo metodo per scaricare il nodo radice, per
	 * scaricare il nodo radice sfruttare dei metodi che interagiscono direttamente
	 * con l'email, se il path ha meno di due nodi di profondita il metodo ritorna
	 * una RedException
	 * 
	 * @param utente utente che esegue l'operazione e che possiede i privilegi di
	 *               accesso a fileNet
	 * @param path   il nome dell'allegato da recuperare compresivo dei nodi
	 *               percorsi per estrarlo separati fa File.pathSeparator E.G.
	 *               <fileNetId>/<filenetId>/<nomeFile></fileNetId>
	 * @return Il contenuto in byte dell'allegato individuato dal path
	 */
	byte[] getContentutoDiscompattataMail(UtenteDTO utente, String path);

	/**
	 * Trasforma una lista di allegati mail scompattati in una lista di allegati
	 * DTO. </br>
	 * 
	 * Valorizza:
	 * <ul>
	 * <li>il content</li>
	 * <li>il mimetype</li>
	 * <li>il nome del file</li>
	 * <li>l'id dell'allegato scompattato</li>
	 * </ul>
	 * </br>
	 * Pulisce il nome dell'allegato da eventuali percorsi lasciando solo il nome
	 * del file base. </br>
	 * Gestisce il caso fileList null restituendo una lista vuota.
	 * 
	 * @param fileList lista di file scompattati da una o più mail.
	 * @param utente   utente che esegue l'operazione.
	 * @return Una lista di allegati corrispondenti agli allegati dell'email
	 *         scompattata.
	 */
	List<AllegatoDTO> transformToAllegatoDTO(UtenteDTO utente, List<HierarchicalFileWrapperDTO> fileList);

	/**
	 * Trasforma una lista di allegati mail scompattati in una lista di allegati
	 * DTO. </br>
	 * 
	 * Valorizza:
	 * <ul>
	 * <li>il content</li>
	 * <li>il mimetype</li>
	 * <li>il nome del file</li>
	 * <li>l'id dell'allegato scompattato</li>
	 * <li>flag mantieni formato originale</li>
	 * <li>l'id della tipologia documentale associata</li>
	 * </ul>
	 * </br>
	 * Pulisce il nome dell'allegato da eventuali percorsi lasciando solo il nome
	 * del file base. </br>
	 * Gestisce il caso fileList null restituendo una lista vuota.
	 * 
	 * @param utente
	 * @param fileList
	 * @param mantieniFormatoOriginale
	 * @param idTipologiaDocumentale
	 * @return
	 */
	List<AllegatoDTO> transformToAllegatoDTO(UtenteDTO utente, List<HierarchicalFileWrapperDTO> fileList, boolean mantieniFormatoOriginale, Integer idTipologiaDocumentale);

	/**
	 * Trasforma da hierarchicalfilewrapper a AllegatoDTO.
	 * 
	 * @param utente
	 * @param allegatoDTO
	 * @param mailSelected
	 * @param mantieniFormatoOriginale
	 * @param disabilitaMantieniFormatoOriginale
	 * @param idTipologiaDocumentale
	 * @param tipiDocAttive
	 * @return allegato
	 */
	AllegatoDTO populateAllegatoDTOInterop(UtenteDTO utente, AllegatoDTO allegatoDTO, DetailEmailDTO mailSelected, boolean mantieniFormatoOriginale,
			boolean disabilitaMantieniFormatoOriginale, Integer idTipologiaDocumentale, List<TipologiaDocumentoDTO> tipiDocAttive);

}