package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IGestioneColonneDAO;
import it.ibm.red.business.dto.ColonneCodeDTO;
import it.ibm.red.business.enums.ColonneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 *
 * Dao per gestione delle colonne;
 * 
 * @author VINGENITO
 */
@Repository
public class GestioneColonneDAO extends AbstractDAO implements IGestioneColonneDAO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 5044162181810892820L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(GestioneColonneDAO.class.getName());

	/**
	 * @see it.ibm.red.business.dao.IGestioneColonneDAO#salvaColonneScelte(java.lang.Long, java.lang.String, boolean, java.sql.Connection).
	 */
	@Override
	public void salvaColonneScelte(final Long idUtente, final String colonneEnum, final boolean visibilita, final Connection conn) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int index = 1;
			StringBuilder sb = new StringBuilder();
			sb.append("INSERT INTO COLONNE_ATTIVE_UTENTE (IDUTENTE,LABEL_COLONNA,VISIBILITA)");
			sb.append("VALUES(?,?,?)");

			ps = conn.prepareStatement(sb.toString());
			ps.setLong(index++, idUtente);
			ps.setString(index++, colonneEnum);
			ps.setInt(index++, visibilita ? 1 : 0);
			rs = ps.executeQuery();
		} catch (Exception ex) {
			LOGGER.error("Errore durante il salvataggio delle colonne scelte " + ex);
			throw new RedException("Errore durante il salvataggio delle colonne scelte " + ex);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneColonneDAO#deleteColonneScelte(java.lang.Long, java.sql.Connection).
	 */
	@Override
	public void deleteColonneScelte(final Long idUtente, final Connection conn) {
		PreparedStatement ps = null;
		try {
			int index = 1;
			StringBuilder sb = new StringBuilder();
			sb.append("DELETE FROM COLONNE_ATTIVE_UTENTE WHERE IDUTENTE = ?");
			ps = conn.prepareStatement(sb.toString());
			ps.setLong(index, idUtente);
			ps.executeQuery();
		} catch (Exception ex) {
			LOGGER.error("Errore durante la cancellazione delle colonne scelte " + ex);
			throw new RedException("Errore durante la cancellazione delle colonne scelte " + ex);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneColonneDAO#getColonneScelte(java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ColonneCodeDTO> getColonneScelte(final Long idUtente, final Connection conn) {
		List<ColonneCodeDTO> output = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int index = 1;
			output = new ArrayList<>();
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM COLONNE_ATTIVE_UTENTE WHERE IDUTENTE = ?");

			ps = conn.prepareStatement(sb.toString());
			ps.setInt(index, idUtente.intValue());

			rs = ps.executeQuery();

			while (rs.next()) {
				ColonneEnum enumCol = ColonneEnum.get(rs.getString("LABEL_COLONNA"));
				if(enumCol !=null) {
					ColonneCodeDTO col = new ColonneCodeDTO(enumCol, rs.getInt("VISIBILITA") == 1);
					col.setRenderColonna(true);	
					output.add(col);
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Errore durante il recupero delle colonne scelte " + ex);
			throw new RedException("Errore durante il recupero delle colonne scelte " + ex);
		} finally {
			closeStatement(ps, rs);
		}
		return output;

	}
}