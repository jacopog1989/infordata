package it.ibm.red.business.dto;

/**
 * Classe PerformanceDTO.
 */
public class PerformanceDTO extends AbstractDTO {

	/**
	 * Session ID
	 */
	private String sessionID;
	
	/**
	 * Totale
	 */
	private final Long totale;
	
	/**
	 * Parametro connessione
	 */
	private final Long connectionMS;
	
	/**
	 * Utente connessione
	 */
	private final Long userMS;
	
	/**
	 * DB Applicativo
	 */
	private final Long dbApplicativoMS;
	
	/**
	 * DB Filenet
	 */
	private final Long dbDWHMS;
	
	/**
	 * Credenziali filenet
	 */
	private final Long fnetPEMS;
	
	/**
	 * Credenziali filenet
	 */
	private final Long fnetCEMS;
	
	/**
	 * Host
	 */
	private String host;
	
	/**
	 * Nps MS
	 */
	private final Long npsMS;
	
	/**
	 * Adobe MS
	 */
	private final Long adobeMS;
	
	/**
	 * PK MS
	 */
	private final Long pkMS;
	
	/**
	 * IText MS
	 */
	private final Long iTextMS;
	
	/**
	 * Ricerca generica fascicolo
	 */
	private Long ricercaGenericaFascicoliMS;
	
	/**
	 * Ricerca avanzata fascicoli
	 */
	private Long ricercaAvanzataFascicoliMS;

	/**
	 * Costruttore performance DTO.
	 *
	 * @param dbApplicativoMS
	 *            the db applicativo MS
	 * @param dbDWHMS
	 *            the db DWHMS
	 * @param fnetPEMS
	 *            the fnet PEMS
	 * @param fnetCEMS
	 *            the fnet CEMS
	 * @param userMS
	 *            the user MS
	 * @param connectionMS
	 *            the connection MS
	 * @param npsMS
	 *            the nps MS
	 * @param adobeMS
	 *            the adobe MS
	 * @param pkMS
	 *            the pk MS
	 * @param iTextMS
	 *            the i text MS
	 * @param totale
	 *            the totale
	 * @param ricercaGenericaFascicoliMS
	 *            the ricerca generica fascicoli MS
	 * @param ricercaAvanzataFascicoliMS
	 *            the ricerca avanzata fascicoli MS
	 */
	public PerformanceDTO(final Long dbApplicativoMS, final Long dbDWHMS, final Long fnetPEMS, final Long fnetCEMS, final Long userMS, final Long connectionMS, final Long npsMS, final Long adobeMS, final Long pkMS, final Long iTextMS, final Long totale, final Long ricercaGenericaFascicoliMS, final Long ricercaAvanzataFascicoliMS) {
		super();
		this.dbApplicativoMS = dbApplicativoMS;
		this.dbDWHMS = dbDWHMS;
		this.fnetPEMS = fnetPEMS;
		this.fnetCEMS = fnetCEMS;
		this.totale = totale;
		this.userMS = userMS;
		this.connectionMS = connectionMS;
		this.npsMS = npsMS;
		this.adobeMS = adobeMS;
		this.pkMS = pkMS;
		this.iTextMS = iTextMS;
		this.ricercaGenericaFascicoliMS = ricercaGenericaFascicoliMS;
		this.ricercaAvanzataFascicoliMS = ricercaAvanzataFascicoliMS;
	}

	/** 
	 *
	 * @return the connection MS
	 */
	public Long getConnectionMS() {
		return connectionMS;
	}
	
	/** 
	 *
	 * @return the user MS
	 */
	public Long getUserMS() {
		return userMS;
	}
	
	/** 
	 *
	 * @return the db applicativo MS
	 */
	public Long getDbApplicativoMS() {
		return dbApplicativoMS;
	}
	
	/** 
	 *
	 * @return the db DWHMS
	 */
	public Long getDbDWHMS() {
		return dbDWHMS;
	}
	
	/** 
	 * @return the fnet PEMS
	 */
	public Long getFnetPEMS() {
		return fnetPEMS;
	}
	
	/** 
	 * @return the fnet CEMS
	 */
	public Long getFnetCEMS() {
		return fnetCEMS;
	}
	
	/** 
	 * @return the host
	 */
	public String getHost() {
		return host;
	}
	
	/** 
	 * @return the totale
	 */
	public Long getTotale() {
		return totale;
	}
	
	/** 
	 * @param host the new ip
	 */
	public void setIp(final String host) {
		this.host = host;
	}
	
	/** 
	 * @return the nps MS
	 */
	public Long getNpsMS() {
		return npsMS;
	}

	/** 
	 * @return the adobe MS
	 */
	public Long getAdobeMS() {
		return adobeMS;
	}

	/** 
	 * @return the pk MS
	 */
	public Long getPkMS() {
		return pkMS;
	}

	/** 
	 * @return the i text MS
	 */
	public Long getiTextMS() {
		return iTextMS;
	}

	/** 
	 * @return the session ID
	 */
	public String getSessionID() {
		return sessionID;
	}
	
	/** 
	 * @param sessionID the new session ID
	 */
	public void setSessionID(final String sessionID) {
		this.sessionID = sessionID;
	}

	/** 
	 * @return the ricercaGenericaFascicoliMS
	 */
	public Long getRicercaGenericaFascicoliMS() {
		return ricercaGenericaFascicoliMS;
	}

	/** 
	 * @param ricercaGenericaFascicoliMS the ricercaGenericaFascicoliMS to set
	 */
	public void setRicercaGenericaFascicoliMS(final Long ricercaGenericaFascicoliMS) {
		this.ricercaGenericaFascicoliMS = ricercaGenericaFascicoliMS;
	}

	/** 
	 * @return the ricercaAvanzataFascicoliMS
	 */
	public Long getRicercaAvanzataFascicoliMS() {
		return ricercaAvanzataFascicoliMS;
	}

	/** 
	 * @param ricercaAvanzataFascicoliMS the ricercaAvanzataFascicoliMS to set
	 */
	public void setRicercaAvanzataFascicoliMS(final Long ricercaAvanzataFascicoliMS) {
		this.ricercaAvanzataFascicoliMS = ricercaAvanzataFascicoliMS;
	}

	
}