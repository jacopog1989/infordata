package it.ibm.red.business.dto;

import java.util.Collection;
import java.util.Date;

import it.ibm.red.business.enums.TipoAzioneEnum;
import it.ibm.red.business.persistence.model.Calendario;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class ImpegnoDTO.
 *
 * @author CPIERASC
 * 
 * 	DTO per modellare un impegno.
 */
public class ImpegnoDTO extends AbstractDTO {
	
	/**
	 * Serializzable.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Tipologia.
	 */
	private TipoAzioneEnum tipologia;

	/**
	 * Identificativo calendario.
	 */
	private Integer idCalendario;

	/**
	 * Data creazione.
	 */
	private Date dataCreazione;

	/**
	 * Data inizio.
	 */
	private Date dataInizio;

	/**
	 * Data fine.
	 */
	private Date dataFine;

	/**
	 * Flag urgente.
	 */
	private Boolean flagUrgente;

	/**
	 * Titolo.
	 */
	private String titolo;

	/**
	 * Contenuto.
	 */
	private StringBuilder contenuto;

	/**
	 * Numero protocollo.
	 */
	private Integer numeroProtocollo;

	/**
	 * Anno protocollo.
	 */
	private Integer annoProtocollo;

	/**
	 * Numero documento.
	 */
	private Integer numeroDocumento;

	/**
	 * Tipologia documento.
	 */
	private String tipologiaDocumento;

	/**
	 * Oggetto.
	 */
	private String oggetto;

	/**
	 * Identificativo.
	 */
	private Integer id;

	/**
	 * Costruttore.
	 * 
	 * @param calendario	model
	 */
	public ImpegnoDTO(final Calendario calendario) {
		tipologia = TipoAzioneEnum.EVENTO;
		idCalendario = calendario.getIdEvento();
		dataCreazione = calendario.getDataTime();
		dataInizio = calendario.getDataInizio();
		dataFine = calendario.getDataScadenza();
		flagUrgente = false;
		if (calendario.getUrgente() == 1) {
			flagUrgente = true;
		}
		titolo = calendario.getTitolo();
		contenuto = new StringBuilder(calendario.getContenuto());
		id = idCalendario;
	}

	/**
	 * Costruttore.
	 * 
	 * @param documento	master documento dto.
	 */
	public ImpegnoDTO(final MasterDocumentoDTO documento) {
		tipologia = TipoAzioneEnum.SCADENZA_DOCUMENTO;
		dataCreazione = documento.getDataCreazione();
		dataInizio = documento.getDataScadenza();
		dataFine = documento.getDataScadenza();
		flagUrgente = false;
		if (documento.getUrgenza() != null && documento.getUrgenza() == 1) {
			flagUrgente = true;
		}
		numeroProtocollo = documento.getNumeroProtocollo();
		annoProtocollo = documento.getAnnoProtocollo();
		numeroDocumento = documento.getNumeroDocumento();
		tipologiaDocumento = documento.getTipologiaDocumento();
		oggetto = documento.getOggetto();
		id = numeroDocumento;
	}

	/**
	 * Costruttore.
	 * 
	 * @param results	collezione di documenti
	 */
	public ImpegnoDTO(final Collection<MasterDocumentoDTO> results) {
		this(results.iterator().next());
		dataCreazione = null;
		numeroProtocollo = null;
		annoProtocollo = null;
		numeroDocumento = null;
		tipologiaDocumento = null;
		titolo = "Documenti in scadenza: " + results.size();
		contenuto = new StringBuilder("Identificativi documenti in scadenza [");
		for (MasterDocumentoDTO dto:results) {
			contenuto.append(dto.getNumeroDocumento() + ",");
		}
		contenuto = new StringBuilder(StringUtils.cutLast(contenuto.toString()));
		contenuto.append("]");
	}
	
	/**
	 * Getter.
	 * 
	 * @return	identificativo.
	 */
	public final Integer getId() {
		return id;
	}

	/**
	 * Getter.
	 * 
	 * @return	tippologia
	 */
	public final TipoAzioneEnum getTipologia() {
		return tipologia;
	}

	/**
	 * Getter.
	 * 
	 * @return	identificatore calendario
	 */
	public final Integer getIdCalendario() {
		return idCalendario;
	}

	/**
	 * Getter.
	 * 
	 * @return	data creazione
	 */
	public final Date getDataCreazione() {
		return dataCreazione;
	}

	/**
	 * Getter.
	 * 
	 * @return	data inizio
	 */
	public final Date getDataInizio() {
		return dataInizio;
	}

	/**
	 * Getter.
	 * 
	 * @return	data fine
	 */
	public final Date getDataFine() {
		return dataFine;
	}

	/**
	 * Getter.
	 * 
	 * @return	flag urgente
	 */
	public final Boolean getFlagUrgente() {
		return flagUrgente;
	}

	/**
	 * Getter.
	 * 
	 * @return	titolo
	 */
	public final String getTitolo() {
		return titolo;
	}

	/**
	 * Getter.
	 * 
	 * @return	content
	 */
	public final String getContenuto() {
		return contenuto.toString();
	}

	/**
	 * Getter.
	 * 
	 * @return	numero protocollo
	 */
	public final Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Getter.
	 * 
	 * @return	anno protocollo
	 */
	public final Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * Getter.
	 * 
	 * @return	numero documento
	 */
	public final Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * Getter.
	 * 
	 * @return	tipologia documento
	 */
	public final String getTipologiaDocumento() {
		return tipologiaDocumento;
	}

	/**
	 * Getter.
	 * 
	 * @return	oggetto
	 */
	public final String getOggetto() {
		return oggetto;
	}

}
