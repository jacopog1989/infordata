package it.ibm.red.business.dto;

import it.ibm.red.business.enums.IconaExtensionEnum;
import it.ibm.red.business.helper.pdf.PdfHelper;

/**
 * DTO allegato mail.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public class MailAttachmentDTO extends AttachmentDTO {

	/**
	 * Soglia dimensione mega.
	 */
	private static final int M_DIM = 1000000;

	/**
	 * Identificativo mail.
	 */
	private String id;

	/**
	 * Soglia dimensione k.
	 */
	private static final int K_DIM = 1000;

	/**
	 * Soglia dimensione byte.
	 */
	private static final int BYTE_DIM = 100;

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Numero campi firma vuoti.
	 */
	private Integer blankSign;

	/**
	 * Numero campi firma usati.
	 */
	private Integer fullSign;

	/**
	 * Icona.
	 */
	private IconaExtensionEnum iconaExt;

	/**
	 * Dimensione.
	 */
	private String dimension;
	
	/**
	 * Costruttore che pone tutto al default Impostato per utilizzare MailAttachment
	 * per parsare gli allegati dell'email.
	 * 
	 * @param inId
	 * @param inFileName
	 * @param inMimeType
	 * @param inDescription
	 * @param inNeedSign
	 * @param inDocumentTitle
	 */
	public MailAttachmentDTO(final String inId, final String inFileName,  final String inMimeType, final String inDescription, 
			final Boolean inNeedSign, final String inDocumentTitle) {
		super(inFileName, null, inMimeType, inDescription, inNeedSign, inDocumentTitle);
		id = inId;
		iconaExt = IconaExtensionEnum.get(inDocumentTitle);
		dimension = "0B";
		blankSign = 0;
		fullSign = 0;
	}

	/**
	 * Costruttore.
	 * 
	 * @param inId
	 *            identificativo
	 * @param inFileName
	 *            nome file
	 * @param inContent
	 *            content file
	 * @param inMimeType
	 *            mime type
	 * @param inDescription
	 *            descrizione
	 * @param inNeedSign
	 *            flag firma richiesta
	 * @param inDocumentTitle
	 *            docuiment title
	 */
	public MailAttachmentDTO(final String inId, final String inFileName, final byte[] inContent, final String inMimeType, final String inDescription, 
			final Boolean inNeedSign, final String inDocumentTitle) {
		this(inId, inFileName, inMimeType, inDescription, inNeedSign, inDocumentTitle);
		
		if (inContent != null) {
			Integer nDim = inContent.length;
			if (nDim < BYTE_DIM) {
				dimension = nDim + "B";
			} else if (nDim < M_DIM) {
				dimension = (nDim / K_DIM) + "KB";
			} else {
				dimension = (nDim / M_DIM) + "MB";
			}
		}
		
		if (IconaExtensionEnum.P7M.equals(iconaExt)) {
			fullSign = 1;
		} else if (IconaExtensionEnum.PDF.equals(iconaExt)) {
			blankSign = PdfHelper.getBlankSignatureNumber(inContent);
			fullSign = PdfHelper.getSignatureNumber(inContent);
		}
		
	}

	/**
	 * Getter.
	 * 
	 * @return	numero campi firma non usati
	 */
	public Integer getBlankSign() {
		return blankSign;
	}

	/**
	 * Getter.
	 * 
	 * @return	numero campi firma usati
	 */
	public Integer getFullSign() {
		return fullSign;
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo mail
	 */
	public String getId() {
		return id;
	}

	/**
	 * Getter.
	 * 
	 * @return	dimensione
	 */
	public String getDimension() {
		return dimension;
	}

	/**
	 * Getter.
	 * 
	 * @return	icona
	 */
	public IconaExtensionEnum getIconaExt() {
		return iconaExt;
	}
}
