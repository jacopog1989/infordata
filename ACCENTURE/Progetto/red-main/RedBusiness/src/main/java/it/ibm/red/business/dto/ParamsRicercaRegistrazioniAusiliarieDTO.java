package it.ibm.red.business.dto;

import java.util.Calendar;
import java.util.Date;


/**
 * DTO per la ricerca di registrazioni ausiliarie.
 * 
 * @author m.crescentini
 *
 */
public class ParamsRicercaRegistrazioniAusiliarieDTO extends AbstractDTO {

	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = -427880138987454804L;

	/**
	 * Codice registro
	 */
	private String codiceRegistro;
	
	/**
	 * Anno
	 */
	private Short anno;
	
	/**
	 * Data registrazione da
	 */
	private Date dataRegistrazioneDa;
	
	/**
	 * Data registrazione a
	 */
	private Date dataRegistrazioneA;
	
	/**
	 * Data annullamento da
	 */
	private Date dataAnnullamentoDa;
	
	/**
	 * Data annullamento a
	 */
	private Date dataAnnullamentoA;
	
	/**
	 * Numero registrazione da
	 */
	private Integer numeroRegistrazioneDa;
	
	/**
	 * Numero registrazione a
	 */
	private Integer numeroRegistrazioneA;
	
	/**
	 * Oggetto
	 */
	private String oggetto;
	
	/**
	 * Codice tipologia documento
	 */
	private String codiceTipologiaDocumento;
	
	/**
	 * Flag annullato
	 */
	private boolean annullato;
	

	/**
	 * Costruttore vuoto.
	 */
	public ParamsRicercaRegistrazioniAusiliarieDTO() {
		super();
		
		anno = (short) Calendar.getInstance().get(Calendar.YEAR);
	}

	/** 
	 *
	 * @return the codiceRegistro
	 */
	public String getCodiceRegistro() {
		return codiceRegistro;
	}

	/** 
	 *
	 * @return the anno
	 */
	public Short getAnno() {
		return anno;
	}

	/** 
	 *
	 * @param anno the anno to set
	 */
	public void setAnno(final Short anno) {
		this.anno = anno;
	}

	/** 
	 *
	 * @param codiceRegistro the codiceRegistro to set
	 */
	public void setCodiceRegistro(final String codiceRegistro) {
		this.codiceRegistro = codiceRegistro;
	}

	/** 
	 *
	 * @return the dataRegistrazioneDa
	 */
	public Date getDataRegistrazioneDa() {
		return dataRegistrazioneDa;
	}

	/** 
	 *
	 * @param dataRegistrazioneDa the dataRegistrazioneDa to set
	 */
	public void setDataRegistrazioneDa(final Date dataRegistrazioneDa) {
		this.dataRegistrazioneDa = dataRegistrazioneDa;
	}

	/** 
	 *
	 * @return the dataRegistrazioneA
	 */
	public Date getDataRegistrazioneA() {
		return dataRegistrazioneA;
	}

	/** 
	 *
	 * @param dataRegistrazioneA the dataRegistrazioneA to set
	 */
	public void setDataRegistrazioneA(final Date dataRegistrazioneA) {
		this.dataRegistrazioneA = dataRegistrazioneA;
	}

	/** 
	 *
	 * @return the dataAnnullamentoDa
	 */
	public Date getDataAnnullamentoDa() {
		return dataAnnullamentoDa;
	}

	/** 
	 *
	 * @param dataAnnullamentoDa the dataAnnullamentoDa to set
	 */
	public void setDataAnnullamentoDa(final Date dataAnnullamentoDa) {
		this.dataAnnullamentoDa = dataAnnullamentoDa;
	}

	/** 
	 *
	 * @return the dataAnnullamentoA
	 */
	public Date getDataAnnullamentoA() {
		return dataAnnullamentoA;
	}

	/** 
	 *
	 * @param dataAnnullamentoA the dataAnnullamentoA to set
	 */
	public void setDataAnnullamentoA(final Date dataAnnullamentoA) {
		this.dataAnnullamentoA = dataAnnullamentoA;
	}

	/** 
	 *
	 * @return the numeroRegistrazioneDa
	 */
	public Integer getNumeroRegistrazioneDa() {
		return numeroRegistrazioneDa;
	}

	/** 
	 *
	 * @param numeroRegistrazioneDa the numeroRegistrazioneDa to set
	 */
	public void setNumeroRegistrazioneDa(final Integer numeroRegistrazioneDa) {
		this.numeroRegistrazioneDa = numeroRegistrazioneDa;
	}

	/** 
	 *
	 * @return the numeroRegistrazioneA
	 */
	public Integer getNumeroRegistrazioneA() {
		return numeroRegistrazioneA;
	}

	/** 
	 *
	 * @param numeroRegistrazioneA the numeroRegistrazioneA to set
	 */
	public void setNumeroRegistrazioneA(final Integer numeroRegistrazioneA) {
		this.numeroRegistrazioneA = numeroRegistrazioneA;
	}

	/** 
	 *
	 * @return the oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/** 
	 *
	 * @param oggetto the oggetto to set
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/** 
	 *
	 * @return the codiceTipologiaDocumento
	 */
	public String getCodiceTipologiaDocumento() {
		return codiceTipologiaDocumento;
	}

	/** 
	 *
	 * @param codiceTipologiaDocumento the codiceTipologiaDocumento to set
	 */
	public void setCodiceTipologiaDocumento(final String codiceTipologiaDocumento) {
		this.codiceTipologiaDocumento = codiceTipologiaDocumento;
	}

	/** 
	 *
	 * @return the annullato
	 */
	public boolean isAnnullato() {
		return annullato;
	}

	/** 
	 *
	 * @param annullato the annullato to set
	 */
	public void setAnnullato(final boolean annullato) {
		this.annullato = annullato;
	}
	
}