package it.ibm.red.business.enums;

import it.pkbox.client.Envelope;

/**
 * The Enum EncodingEnum.
 *
 * @author CPIERASC
 * 
 *         Tipologie di codifica.
 */
public enum EncodingEnum {
	/**
	 * DER.
	 */
	DER_ENCODE(Envelope.derEncoding), 

	/**
	 * BASE64.
	 */
	BASE64_ENCODE(Envelope.base64Encoding), 

	/**
	 * RAW BINARY.
	 */
	RAW_BINARY_ENCODE(Envelope.rawBinaryEncoding), 

	/**
	 * RAW BASE64 (non utilizzabile per la firma cades).
	 */
	RAW_BASE64_ENCODE(Envelope.rawBase64Encoding); 

	/**
	 * Codifica.
	 */
	private Integer value;
	
	/**
	 * Costruttore.
	 * 
	 * @param v	codifica
	 */
	EncodingEnum(final Integer v) {
		value = v;
	}
	
	/**
	 * Getter codifica.
	 * 
	 * @return	codifica
	 */
	public Integer getValue() {
		return value;
	}
}