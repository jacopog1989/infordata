package it.ibm.red.business.enums;

/**
 * Enum che definisce il tipo di stampigliatura.
 */
public enum StampigliaturaFirmaEnum {
	
	/**
	 * Valore.
	 */
	NESSUNA(0),
	
	/**
	 * Valore.
	 */
	NOME_COGNOME(1),
	
	/**
	 * Valore.
	 */
	N_COGNOME(2),
	
	/**
	 * Valore.
	 */
	RUOLO(3),
	
	/**
	 * Valore.
	 */
	VISTO(4);
	
	/**
	 * Valore.
	 */
	private int value;

	/**
	 * Costruttore.
	 * @param dbValue
	 */
	StampigliaturaFirmaEnum(final int dbValue) {
		this.value = dbValue;
	}

	/**
	 * Restituisce il value dell'enum
	 * @return value
	 */
	public int getValue() {
		return value;
	}
	
}
