package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.ISottoscrizioniDAO;
import it.ibm.red.business.dto.EventoSottoscrizioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.service.INotificaPostSiglaVistoSRV;
import it.ibm.red.business.service.ISottoscrizioniSRV;

/**
 * Service notifica post sigla visto.
 */
@Service
@Component
public class NotificaPostSiglaVistoSRV extends NotificaWriteAbstractSRV implements INotificaPostSiglaVistoSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 5072215933806523575L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotificaPostSiglaVistoSRV.class.getName());

	/**
	 * DAO.
	 */
	@Autowired
	private ISottoscrizioniDAO sottoscrizioniDAO;

	/**
	 * Service.
	 */
	@Autowired
	private ISottoscrizioniSRV sottoscrizioniSRV;

	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getSottoscrizioni(int).
	 */
	@Override
	protected List<EventoSottoscrizioneDTO> getSottoscrizioni(final int idAoo) {
	
		Connection con = null;
		List<EventoSottoscrizioneDTO> eventi = null;
		try {
			
			final Integer[] idEventi = new Integer[]{
					Integer.parseInt(EventTypeEnum.MODIFICATO_DOPO_VISTO.getValue()), 
					Integer.parseInt(EventTypeEnum.MODIFICATO_DOPO_SIGLA.getValue())};
		
			con = setupConnection(getFilenetDataSource().getConnection(), false);
			eventi = sottoscrizioniSRV.getEventiSottoscrittiByListaEventi(idAoo, idEventi, con);			
		
		} catch (final Exception e) {
		
			LOGGER.error("Errore in fase recupero delle sottoscrizioni per gli eventi EVENTTYPE_MODIFICATO_DOPO_VISTO-EVENTTYPE_MODIFICATO_DOPO_SIGLA per l'aoo " + idAoo, e);
		
		} finally {
			closeConnection(con);
		}
		
		return eventi;
	}

	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getDataDocumenti(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.persistence.model.Aoo, int, java.lang.Object).
	 */
	@Override
	protected List<InfoDocumenti> getDataDocumenti(final UtenteDTO utente, final Aoo aoo, final int idEvento, final Object objectForGetDataDocument) {
		throw new RedException("Metodo non previsto per il servizio ");
	}

	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getDataDocumenti(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.persistence.model.Aoo, int).
	 */
	@Override
	protected List<InfoDocumenti> getDataDocumenti(final UtenteDTO utente, final Aoo aoo, final int idEvento) {
		
		final List<InfoDocumenti> documenti = new ArrayList<>();
		
		Connection con = null;
		
		try {
			
			con = setupConnection(getFilenetDataSource().getConnection(), false);
			
			return sottoscrizioniDAO.getDocumentiPostSiglaVisto(con, aoo.getIdAoo().intValue(), idEvento, 
					utente.getId().intValue(), utente.getIdUfficio().intValue());
						
		} catch (final Exception e) {
		
			LOGGER.error("Errore in fase recupero dei documenti siglati e vistati dall'utente/ufficio (" + utente.getId() + "," + utente.getIdUfficio() + ") l'aoo " + aoo.getIdAoo(), e);
		
		} finally {
			closeConnection(con);
		}
		
		return documenti;
		
		
	}
	
}