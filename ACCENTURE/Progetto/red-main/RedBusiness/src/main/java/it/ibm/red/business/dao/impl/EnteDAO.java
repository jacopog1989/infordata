package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IEnteDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Ente;

/**
 * The Class EnteDAO.
 *
 * @author CPIERASC
 * 
 * 	Dao per la gestione di un ente.
 */
@Repository
public class EnteDAO extends AbstractDAO implements IEnteDAO {
	
	/**
	 * SERIAL VERSION UID.
	 */
	private static final long serialVersionUID = -7104950624259449041L;
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(EnteDAO.class.getName());

	/**
	 * Gets the ente.
	 *
	 * @param idEnte the id ente
	 * @param connection the connection
	 * @return the ente
	 */
	@Override
	public final Ente getEnte(final Integer idEnte, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String querySQL = "SELECT e.* FROM Ente e WHERE idEnte = " + sanitize(idEnte);
			ps = connection.prepareStatement(querySQL);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				return new Ente(
					rs.getInt("IDENTE"),
					rs.getString("DESCRIZIONE"));
			}
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero dell'ente tramite id=" + idEnte, e);
			throw new RedException("Errore durante il recupero dell'ente tramite id=" + idEnte, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return null;
	}

}
