package it.ibm.red.business.service.flusso.concrete;

import java.sql.Connection;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IRedWsFlussoDAO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.RedWsFlusso;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.concrete.AbstractService;
import it.ibm.red.business.service.flusso.IAzioniFlussoBaseSRV;
import it.ibm.red.business.service.flusso.IAzioniFlussoSRV;

/**
 * La classe implementa i metodi di servizio del motore dei flussi automatici di Red.
 * 
 * @author a.dilegge
 *
 */
@Service
public class AzioniFlussoSRV extends AbstractService implements IAzioniFlussoSRV {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AzioniFlussoSRV.class.getName());

	/**
	 * Dao flusso.
	 */
	@Autowired
	private IRedWsFlussoDAO redWsFlussoDAO;
	
	/**
	 * Recupera le azioni relative al flusso identificato dal codice <code> codiceFlusso </code>.
	 * 
	 * @param codiceFlusso
	 *            codice flusso del flusso di riferimento
	 * @return IAzioniFlussoBaseSRV service per la gestione delle azioni flusso
	 */
	@Override
	public IAzioniFlussoBaseSRV retrieveAzioniFlussoSRV(final String codiceFlusso) {
		IAzioniFlussoBaseSRV azioniFlussoBaseSRV = null;
		Connection connection =  null;
		
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			
			final RedWsFlusso flusso = redWsFlussoDAO.getByCodice(codiceFlusso, connection);
			
			if (flusso != null && StringUtils.isNotBlank(flusso.getInterfaceName())) {
				azioniFlussoBaseSRV = (IAzioniFlussoBaseSRV) ApplicationContextProvider.getApplicationContext().getBean(flusso.getInterfaceName());
			}
			
		} catch (final Exception e) {
			LOGGER.error(e);
		} finally {
			closeConnection(connection);
		}
		
		return azioniFlussoBaseSRV;
	}
	

}
