package it.ibm.red.business.dto;

import java.util.Date;

/**
 * DTO che definisce le informazioni sulla notifca conti consuntivi.
 */
public class DatiNotificaContiConsuntiviDTO extends AbstractDTO {
	
	/**
	 * serial version UID.
	 */
	private static final long serialVersionUID = 5882280745370624877L;
	
	/**
	 * Data protocollo notifica.
	 */
	private Date dataProtocollo;
	
	/**
	 * Numero Protocollo notifica.
	 */
	private Integer numeroProtocollo;

	/**
	 * Restituisce la data protocollo notifica.
	 * 
	 * @return data protocollo notifica
	 */
	public Date getDataProtocollo() {
		return dataProtocollo;
	}

	/**
	 * Imposta la data protocollo notifica.
	 * 
	 * @param dataProtocollo
	 */
	public void setDataProtocollo(Date dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}

	/**
	 * Restituisce il numero protocollo notifica.
	 * 
	 * @return numero protocollo notifica
	 */
	public Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Imposta il numero protocollo notifica.
	 * 
	 * @param numeroProtocollo
	 */
	public void setNumeroProtocollo(Integer numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

}
