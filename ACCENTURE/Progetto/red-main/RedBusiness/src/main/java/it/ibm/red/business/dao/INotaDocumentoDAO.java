/**
 * 
 */
package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.NotaDocumentoDTO;
import it.ibm.red.business.dto.NotaDocumentoDescrizioneDTO;

/**
 * @author a.dilegge
 *
 */
public interface INotaDocumentoDAO extends Serializable {
	
	
	/**
	 * Recupera le note associate al documento in input
	 * 
	 * @param idDocumento
	 * @param idAoo => match con idnodoowner
	 * @param alsoClosed
	 * @param con
	 * @return
	 * @throws DAOException
	 */
	List<NotaDocumentoDescrizioneDTO> getAllNotes(String idDocumento, Integer idAoo, boolean alsoClosed, Connection con);

	/**
	 * Inserisce una nota sul database
	 * 
	 * @param nota
	 * @param con
	 * @throws DAOException
	 */ 
	void insertNote(NotaDocumentoDTO nota, String descrPreassegnatario, Connection con);
	
	/**
	 * Elimina (logicamente) la nota
	 * 
	 * @param idDocumento
	 * @param idAoo => match con idnodoowner
	 * @param numeroNota
	 * @param con
	 */
	void deleteNote(Integer idDocumento, Integer idAoo, Integer numeroNota, Connection con);
	
}
