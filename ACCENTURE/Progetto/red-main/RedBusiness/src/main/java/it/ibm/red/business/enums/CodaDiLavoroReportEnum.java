package it.ibm.red.business.enums;

/**
 * Enum che definisce le code di lavoro.
 */
public enum CodaDiLavoroReportEnum {
	
	/**
	 * Valore.
	 */
	UFF_CORRIERE,
	
	/**
	 * Valore.
	 */
	UFF_IN_SPEDIZIONE,
	
	/**
	 * Valore.
	 */
	UFF_CHIUSI,
	
	/**
	 * Valore.
	 */
	UTE_DA_LAVORARE,
	
	/**
	 * Valore.
	 */
	UTE_LIBRO_FIRMA,
	
	/**
	 * Valore.
	 */
	UTE_IN_SOSPESO,
	
	/**
	 * Valore.
	 */
	UTE_ATTI;

	/**
	 * Restituisce l'enum associata al value in ingresso.
	 * @param codiceCodaDiLavoroReport
	 * @return l'Enum associata al codice di lavoro report.
	 */
	public static CodaDiLavoroReportEnum get(final String codiceCodaDiLavoroReport) {
		CodaDiLavoroReportEnum output = null;
		
		 for (CodaDiLavoroReportEnum codaDiLavoroReport : CodaDiLavoroReportEnum.values()) {
			 if (codaDiLavoroReport.name().equals(codiceCodaDiLavoroReport)) {
				 output = codaDiLavoroReport;
				 break;
			 }
		 }
		 
		 return output;
	}
}