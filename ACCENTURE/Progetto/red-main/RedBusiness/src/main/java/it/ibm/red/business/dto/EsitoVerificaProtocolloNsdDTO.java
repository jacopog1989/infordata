/**
 * 
 */
package it.ibm.red.business.dto;

/**
 * @author VINGENITO
 *
 */
public class EsitoVerificaProtocolloNsdDTO extends EsitoDTO {

	private static final long serialVersionUID = -8332465417530265999L;

	/**
	 * Documento.
	 */
	private DetailDocumentRedDTO document;

	/**
	 * Costruttore del DTO.
	 */
	public EsitoVerificaProtocolloNsdDTO() {
		super();
	}

	/**
	 * @return the document
	 */
	public DetailDocumentRedDTO getDocument() {
		return document;
	}

	/**
	 * @param document the document to set
	 */
	public void setDocument(final DetailDocumentRedDTO document) {
		this.document = document;
	}

}
