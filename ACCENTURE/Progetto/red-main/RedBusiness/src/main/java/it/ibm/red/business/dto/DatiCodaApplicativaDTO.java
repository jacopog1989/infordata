/**
 * 
 */
package it.ibm.red.business.dto;

/**
 * @author APerquoti
 *
 */
public class DatiCodaApplicativaDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8983257623169731151L;
	
	/**
	 * Stato lavorazione.
	 */
	private Long idStatoLav;
	
	/**
	 * Identificativo utente.
	 */
	private Long idUtente;

	/**
	 * Costruttore del DTO.
	 * @param inIdStatoLav
	 * @param inIdUtente
	 */
	public DatiCodaApplicativaDTO(final Long inIdStatoLav, final Long inIdUtente) {
		super();
		this.idStatoLav = inIdStatoLav;
		this.idUtente = inIdUtente;
	}

	/**
	 * @return the idStatoLav
	 */
	public final Long getIdStatoLav() {
		return idStatoLav;
	}

	/**
	 * @param idStatoLav the idStatoLav to set
	 */
	public final void setIdStatoLav(final Long idStatoLav) {
		this.idStatoLav = idStatoLav;
	}

	/**
	 * @return the idUtente
	 */
	public final Long getIdUtente() {
		return idUtente;
	}

	/**
	 * @param idUtente the idUtente to set
	 */
	public final void setIdUtente(final Long idUtente) {
		this.idUtente = idUtente;
	}
	
	

}
