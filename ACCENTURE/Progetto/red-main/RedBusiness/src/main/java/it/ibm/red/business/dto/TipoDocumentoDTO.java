package it.ibm.red.business.dto;

import java.util.ArrayList;
import java.util.Collection;

import it.ibm.red.business.enums.TipoGestioneEnum;

/**
 * DTO che definisce un tipo documento.
 */
public class TipoDocumentoDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -1068891716906241459L;

	/**
	 * Nome.
	 */
	private String name;

	/**
	 * Metadati.
	 */
	private Collection<MetadatoDTO> metadati;

	/**
	 * Tipologia gestione.
	 */
	private TipoGestioneEnum tipoGestione; // Visibilità del metadato

	/**
	 * Costruttore di default.
	 */
	public TipoDocumentoDTO() {
		name = null;
		metadati = new ArrayList<>();
		tipoGestione = TipoGestioneEnum.IBRIDA;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Imposta il nome del tipo documento.
	 * @param name
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @return metadati
	 */
	public Collection<MetadatoDTO> getMetadati() {
		return metadati;
	}

	/**
	 * Imposta l'insieme di metadati.
	 * @param metadati
	 */
	public void setMetadati(final Collection<MetadatoDTO> metadati) {
		this.metadati = metadati;
	}

	/**
	 * @return tipoGestione
	 */
	public TipoGestioneEnum getTipoGestione() {
		return tipoGestione;
	}

	/**
	 * Imposta il tipo di gestione.
	 * @param tipoGestione
	 */
	public void setTipoGestione(final TipoGestioneEnum tipoGestione) {
		this.tipoGestione = tipoGestione;
	}
}
