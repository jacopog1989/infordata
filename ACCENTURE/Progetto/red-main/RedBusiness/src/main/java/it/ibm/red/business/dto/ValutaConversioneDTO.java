package it.ibm.red.business.dto;

import java.util.Date;

/**
 * DTO che definisce la valuta conversione.
 */
public class ValutaConversioneDTO {
	
	/**
	 * Codice valuta.
	 */
	private String valuta;
	
	/**
	 * Tasso di cambio valuta.
	 */
	private Double cambio;
	
	/**
	 * Data inizio validità tasso di cambio.
	 */
	private Date dataInizio;

	/**
	 * Data fine validità tasso di cambio.
	 */
	private Date dataFine;

	/**
	 * Costruttore di default.
	 * @param valuta
	 * @param cambio
	 * @param dataInizio
	 * @param dataFine
	 */
	public ValutaConversioneDTO(String valuta, Double cambio, Date dataInizio, Date dataFine) {
		this.valuta = valuta;
		this.cambio = cambio;
		this.dataInizio = dataInizio;
		this.dataFine = dataFine;
	}

	/**
	 * Restituisce il codice valuta.
	 * @return valuta
	 */
	public String getValuta() {
		return valuta;
	}

	/**
	 * Imposta il codice valuta.
	 * @param valuta
	 */
	public void setValuta(String valuta) {
		this.valuta = valuta;
	}

	/**
	 * Restituisce il tasso di cambio.
	 * @return cambio
	 */
	public Double getCambio() {
		return cambio;
	}

	/**
	 * Imposta il tasso di cambio.
	 * @param cambio
	 */
	public void setCambio(Double cambio) {
		this.cambio = cambio;
	}

	/**
	 * Restituisce la data d'inizio validità del tasso di cambio.
	 * @return dataInizio
	 */
	public Date getDataInizio() {
		return dataInizio;
	}

	/**
	 * Imposta la data d'inizio validità del tasso di cambio.
	 * @param dataInizio
	 */
	public void setDataInizio(Date dataInizio) {
		this.dataInizio = dataInizio;
	}

	/**
	 * Restituisce la data di fine validità del tasso di cambio.
	 * @return dataFine
	 */
	public Date getDataFine() {
		return dataFine;
	}

	/**
	 * Imposta la data di fine validità del tasso di cambio.
	 * @param dataFine
	 */
	public void setDataFine(Date dataFine) {
		this.dataFine = dataFine;
	}
}
