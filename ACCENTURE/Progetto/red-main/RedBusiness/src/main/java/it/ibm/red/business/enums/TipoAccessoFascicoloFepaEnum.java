package it.ibm.red.business.enums;

/**
 * Enum che definisce i tipi di accesso dei fascicoli Fepa.
 */
public enum TipoAccessoFascicoloFepaEnum {

	/**
	 * Valore.
	 */
	DOCUMENTO_CONTABILE("FF", TipoAccessoFepa3.FEPA),

	/**
	 * Valore.
	 */
	OP("FO", TipoAccessoFepa3.FEPA),

	/**
	 * Valore.
	 */
	VERBALE_REVISIONE("FV", TipoAccessoFepa3.REVISORI_IGF),

	/**
	 * Valore.
	 */
	BILANCIO_ENTI("FB", TipoAccessoFepa3.BILENTI),

	/**
	 * Valore.
	 */
	SICOGE(null, TipoAccessoFepa3.SICOGE); // Per SICOGE il tipo fascicolo non è univoco, vedi TipoFascicoloFepaEnum

	/**
	 * Tipo fascicolo.
	 */
	private String tipoFascicolo;

	/**
	 * Tipo accesso.
	 */
	private TipoAccessoFepa3 tipoAccesso;

	/**
	 * Costruttore di default.
	 * @param tipoFascicolo
	 * @param tipoAccesso
	 */
	TipoAccessoFascicoloFepaEnum(final String tipoFascicolo, final TipoAccessoFepa3 tipoAccesso) {
		this.tipoFascicolo = tipoFascicolo;
		this.tipoAccesso = tipoAccesso;
	}

	/**
	 * @return tipoFascicolo
	 */
	public String getTipoFascicolo() {
		return tipoFascicolo;
	}

	/**
	 * @return tipoAccesso
	 */
	public TipoAccessoFepa3 getTipoAccesso() {
		return tipoAccesso;
	}
}
