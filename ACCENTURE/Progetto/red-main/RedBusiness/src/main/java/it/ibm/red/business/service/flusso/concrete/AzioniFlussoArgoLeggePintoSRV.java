package it.ibm.red.business.service.flusso.concrete;

import java.sql.Connection;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.dao.IComunicazioneProtocolliArgoDAO;
import it.ibm.red.business.dto.ComunicazioneProtocolloArgoDTO;
import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.dto.ResponsesDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.IconaFlussoEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.service.flusso.IAzioniFlussoArgoLeggePintoSRV;

/**
 * Servizio di gestione azioni flusso ARGO - Legge Pinto.
 */
@Service
public class AzioniFlussoArgoLeggePintoSRV extends AzioniFlussoBaseSRV implements IAzioniFlussoArgoLeggePintoSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 7302991792354282773L;

	/**
	 * DAO protocollazione sistema ARGO.
	 */
	@Autowired
	private IComunicazioneProtocolliArgoDAO protocollazioneArgoDAO;

	/**
	 * Inserimento in coda per eseguire una chiamata REST verso ARGO per comunicare i dati del protocollo staccato in fase di firma.
	 */
	@Override
	public void doAfterProtocollazioneUscita(final int idDocumento, final ProtocolloDTO protocollo, final Long idAoo, final Connection con) {
		if (!protocollazioneArgoDAO.isComunicazionePresente(idDocumento, idAoo, con)) {
			// Si costruisce il DTO contenente le informazioni necessarie per la chiamata REST verso ARGO
			ComunicazioneProtocolloArgoDTO comunicazioneProtocolloArgo = new ComunicazioneProtocolloArgoDTO(idDocumento, idAoo, 
					protocollo.getNumeroProtocollo(), protocollo.getAnnoProtocolloInt(), protocollo.getDataProtocollo(), protocollo.getIdProtocollo(), 
					protocollo.getRegistroProtocollo());
			
			// Si inserisce la comunicazione nella coda
			protocollazioneArgoDAO.inserisciComunicazioneProtocolloInCoda(comunicazioneProtocolloArgo, con);
		}
	}


	/**
	 * Eliminazione response diverse da Procedi, Rifiuta, Firma.
	 */
	@Override
	public final ResponsesDTO refineResponses(final DocumentQueueEnum coda, final ResponsesDTO inResponses) {
		if (inResponses != null && !CollectionUtils.isEmpty(inResponses.getResponsesEnum())) {
			ResponsesRedEnum responseEnum = null;

			Iterator<ResponsesRedEnum> itAllResponsesEnum = inResponses.getResponsesEnum().iterator();
			while (itAllResponsesEnum.hasNext()) {
				responseEnum = itAllResponsesEnum.next();
				
				// Se il documento si trova:
				// a) nel Libro Firma, si rimuovono tutte le response diverse da Procedi, Rifiuta e Firma (Digitale, Remota e Autografa)
				// b) nel Corriere, si rimuovono tutte le response diverse da Procedi e Rifiuta
				// c) in Da Lavorare, si rimuovono tutte le response diverse da Atti (NO)
				if ((DocumentQueueEnum.NSD.equals(coda)
						&& !ResponsesRedEnum.PROCEDI.getResponse().equals(responseEnum.getResponse()) 
						&& !ResponsesRedEnum.RIFIUTA.getResponse().equals(responseEnum.getResponse()) 
						&& !ResponsesRedEnum.FIRMA_DIGITALE.getResponse().equals(responseEnum.getResponse()) 
						&& !ResponsesRedEnum.FIRMA_REMOTA.getResponse().equals(responseEnum.getResponse())
						&& !ResponsesRedEnum.FIRMA_AUTOGRAFA.getResponse().equals(responseEnum.getResponse()))
					|| (DocumentQueueEnum.isOneKindOfCorriere(coda) 
							&& !ResponsesRedEnum.PROCEDI.getResponse().equals(responseEnum.getResponse()) 
							&& !ResponsesRedEnum.RIFIUTA.getResponse().equals(responseEnum.getResponse()))
					/*|| (DocumentQueueEnum.DA_LAVORARE.equals(coda) 
							&& !ResponsesRedEnum.ATTI.getResponse().equals(responseEnum.getResponse()))*/) {
					
					itAllResponsesEnum.remove();
				}
			}
		}
		
		return inResponses;
	}
	
	
	/**
	 * Restituisce l'icona del Fluso ARGO.
	 */
	@Override
	public final IconaFlussoEnum getIconaFlusso() {
		return IconaFlussoEnum.ARGO;
	}
}
