package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import filenet.vw.api.VWQueueQuery;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dao.impl.OrganigrammaDAO;
import it.ibm.red.business.dto.CountPer;
import it.ibm.red.business.dto.DocumentoScadenzatoDTO;
import it.ibm.red.business.dto.IdDocumentDTO;
import it.ibm.red.business.dto.PEDocumentoScadenzatoDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PeriodoScadenzaEnum;
import it.ibm.red.business.enums.ScopeScadenziarioStrutturaEnum;
import it.ibm.red.business.enums.TipoStrutturaNodoEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToIdDocumentDTOTrasformer;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.TrasformPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.service.IScadenziaroStrutturaSRV;

/**
 * Service che gestisce lo scadenziario.
 */
@Service
public class ScadenziarioStrutturaSRV extends AbstractService implements IScadenziaroStrutturaSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -7790861858580612035L;

	/**
	 * LOGGER per la visualizzazione dei messaggi in console.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ScadenziarioStrutturaSRV.class);

	/**
	 * Data partenza storico.
	 */
	private static final Date STORICO_START_DATE = new Date(0L);

	/**
	 * DAO.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private OrganigrammaDAO organigrammaDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * @see it.ibm.red.business.service.facade.IScadenziaroStrutturaFacadeSRV#retrieveCountDocumentoInScadenzaPerUfficiDaIspettorato(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<CountPer<UfficioDTO>> retrieveCountDocumentoInScadenzaPerUfficiDaIspettorato(final UtenteDTO utente) {
		FilenetPEHelper fpeh = null;
		Connection connection = null;
		List<CountPer<UfficioDTO>> countSons = new ArrayList<>();

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			final List<UfficioDTO> sons = organigrammaDAO.getSottoNodi(utente.getIdUfficio(), utente.getIdAoo(), connection);

			// Vogliamo che venga considerato anche il nodo IGB
			final UfficioDTO ufficioUtente = new UfficioDTO(utente.getIdUfficio(), utente.getNodoDesc());
			sons.add(ufficioUtente);

			// Per ogni nodo trovato
			// usare la funzionalità che conta i documenti per le 4 code basate sul nodo
			countSons = countDocumentiInScadenzaPerUffici(sons, fpeh, utente);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il count dei documenti scadenzati per ispettorato", e);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
		}

		return countSons;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IScadenziaroStrutturaFacadeSRV#retrieveCountDocumentoInScadenzaPerUfficiDaSegreteria(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Map<CountPer<UfficioDTO>, List<CountPer<UfficioDTO>>> retrieveCountDocumentoInScadenzaPerUfficiDaSegreteria(final UtenteDTO utente) {
		FilenetPEHelper fpeh = null;
		Connection connection = null;
		final Map<CountPer<UfficioDTO>, List<CountPer<UfficioDTO>>> ispettorato2ufficio = new HashMap<>();
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			final Map<UfficioDTO, List<UfficioDTO>> padre2sons = nodoDAO.getSottoNodiIspettorato(utente.getIdAoo(), connection);

			for (final Entry<UfficioDTO, List<UfficioDTO>> entry : padre2sons.entrySet()) {
				final UfficioDTO ispettoratoPadre = entry.getKey();
				final List<UfficioDTO> uffici = entry.getValue();

				// Vogliamo che venga considerato anche il nodo IGB
				uffici.add(ispettoratoPadre);

				final List<CountPer<UfficioDTO>> countDocumentiPerUfficioList = countDocumentiInScadenzaPerUffici(uffici, fpeh, utente);
				// Il numero di documento del padre è la somma del numero dei documenti dei suoi
				// figli
				int counterNumeroDocumentiPadre = 0;
				for (final CountPer<UfficioDTO> part : countDocumentiPerUfficioList) {
					counterNumeroDocumentiPadre += part.getCount();
				}

				final CountPer<UfficioDTO> countPadre = new CountPer<>(ispettoratoPadre, counterNumeroDocumentiPadre);
				ispettorato2ufficio.put(countPadre, countDocumentiPerUfficioList);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il count dei documenti scadenzati da segreteria", e);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
		}

		return ispettorato2ufficio;
	}

	private List<CountPer<UfficioDTO>> countDocumentiInScadenzaPerUffici(final List<UfficioDTO> ufficioList, final FilenetPEHelper fpeh, final UtenteDTO utente) {
		final List<CountPer<UfficioDTO>> countList = new ArrayList<>(ufficioList.size());
		LOGGER.info("START QUERY COUNT SCADENZIARIO CODA");

		for (final UfficioDTO ufficio : ufficioList) {
			int numeroDiDocumenti = 0;
			DocumentQueueEnum[] enumArray = { DocumentQueueEnum.DA_LAVORARE, DocumentQueueEnum.SOSPESO, DocumentQueueEnum.NSD, DocumentQueueEnum.CORRIERE };
			if (utente.getIdUfficio().equals(utente.getIdNodoAssegnazioneIndiretta())) {
				enumArray = new DocumentQueueEnum[] { DocumentQueueEnum.DA_LAVORARE, DocumentQueueEnum.SOSPESO, DocumentQueueEnum.NSD, DocumentQueueEnum.CORRIERE_DIRETTO,
						DocumentQueueEnum.CORRIERE_INDIRETTO };
			}
			for (final DocumentQueueEnum coda : enumArray) {
				final VWQueueQuery query = getCodaQuery(coda, ufficio.getId(), fpeh, utente);
				numeroDiDocumenti += query.fetchCount();
			}
			countList.add(new CountPer<>(ufficio, numeroDiDocumenti));
		}
		LOGGER.info("END QUERY COUNT SCADENZIARIO CODA");
		return countList;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IScadenziaroStrutturaFacadeSRV#retrieveDocumentoInScadenzaPerPeriodoScadenza(java.lang.Long,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Map<CountPer<PeriodoScadenzaEnum>, List<PEDocumentoScadenzatoDTO>> retrieveDocumentoInScadenzaPerPeriodoScadenza(final Long idUfficio, final UtenteDTO utente) {
		final Map<CountPer<PeriodoScadenzaEnum>, List<PEDocumentoScadenzatoDTO>> count2doc = new HashMap<>();

		try {

			final Map<PeriodoScadenzaEnum, List<PEDocumentoScadenzatoDTO>> periodo2doc = retrieveDocumentiPerPeriodoScadenza(idUfficio, utente);
			final EnumSet<PeriodoScadenzaEnum> periodoSet = EnumSet.allOf(PeriodoScadenzaEnum.class);

			for (final PeriodoScadenzaEnum oneOf : periodoSet) {
				final List<PEDocumentoScadenzatoDTO> peDocPeriodoList = periodo2doc.get(oneOf);
				final int size = CollectionUtils.size(peDocPeriodoList);
				final CountPer<PeriodoScadenzaEnum> count = new CountPer<>(oneOf, size);
				count2doc.put(count, size > 0 ? peDocPeriodoList : null);
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei documenti per ufficio", e);
		}
		return count2doc;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IScadenziaroStrutturaFacadeSRV#retrieveDocumentiPerPeriodoScadenza(java.lang.Long,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Map<PeriodoScadenzaEnum, List<PEDocumentoScadenzatoDTO>> retrieveDocumentiPerPeriodoScadenza(final Long idUfficio, final UtenteDTO utente) {
		FilenetPEHelper fpeh = null;
		final EnumMap<PeriodoScadenzaEnum, List<PEDocumentoScadenzatoDTO>> periodo2doc = new EnumMap<>(PeriodoScadenzaEnum.class);
		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			final List<PEDocumentoScadenzatoDTO> peDocList = retrieveDocumentiScadenzatiPerUfficio(idUfficio, fpeh, utente);

			// creo la lista
			for (final PEDocumentoScadenzatoDTO peDoc : peDocList) {
				final PeriodoScadenzaEnum periodo = PeriodoScadenzaEnum.get(peDoc.getDataScadenza());

				List<PEDocumentoScadenzatoDTO> scadenzatiList = periodo2doc.get(periodo);
				if (CollectionUtils.isEmpty(scadenzatiList)) {
					scadenzatiList = new ArrayList<>();
					periodo2doc.put(periodo, scadenzatiList);
				}
				scadenzatiList.add(peDoc);
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei documenti per ufficio", e);
		} finally {
			logoff(fpeh);
		}

		return periodo2doc;
	}

	/*
	 * Conta i documenti scadenzati per ciascuna coda di interesse
	 */
	private List<PEDocumentoScadenzatoDTO> retrieveDocumentiScadenzatiPerUfficio(final Long idUfficio, final FilenetPEHelper fpeh, final UtenteDTO utente) {
		final List<PEDocumentoScadenzatoDTO> pedScadenzati = new ArrayList<>();

		final List<DocumentQueueEnum> queues = new ArrayList<>();
		if (utente.isUcb()) {
			queues.add(DocumentQueueEnum.DA_LAVORARE_UCB);
			queues.add(DocumentQueueEnum.IN_LAVORAZIONE_UCB);
		} else {
			queues.add(DocumentQueueEnum.DA_LAVORARE);
		}

		queues.add(DocumentQueueEnum.SOSPESO);
		queues.add(DocumentQueueEnum.NSD);

		if (utente.getIdUfficio().equals(utente.getIdNodoAssegnazioneIndiretta())) {
			queues.add(DocumentQueueEnum.CORRIERE_DIRETTO);
			queues.add(DocumentQueueEnum.CORRIERE_INDIRETTO);
		} else {
			queues.add(DocumentQueueEnum.CORRIERE);
		}

		for (final DocumentQueueEnum coda : queues) {
			final VWQueueQuery query = getCodaQuery(coda, idUfficio, fpeh, utente);
			final Collection<PEDocumentoScadenzatoDTO> queryPEResults = TrasformPE.transform(query, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO_SCADENZATO, null);
			if (CollectionUtils.isNotEmpty(queryPEResults)) {
				pedScadenzati.addAll(queryPEResults);
			}
		}
		return pedScadenzati;
	}

	/**
	 * Query per i documenti cadenzati. Ritorna un oggetto che può essere
	 * interrogato per avere i risultati oppure il count dei risultati.
	 * 
	 * @param coda
	 *            coda su cui eseguire la ricerca, purtroppo non è possibile
	 *            eseguire una query che coinvolga più query
	 * @param idUfficio
	 *            filtro per ufficio
	 * @param fpeh
	 * @param utente
	 * @return Un oggetto che può essere interrogato per ricevere il risultato della
	 *         query
	 */
	private static VWQueueQuery getCodaQuery(final DocumentQueueEnum coda, final Long idUfficio, final FilenetPEHelper fpeh, final UtenteDTO utente) {
		return fpeh.getWorkFlowsByWobQueueFilterRed(null, coda, null, idUfficio, null, // Arrays.asList(new Long[] {utente.getId()}),
				utente.getFcDTO().getIdClientAoo(), null, null, null, null, null, STORICO_START_DATE, null, false);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IScadenziaroStrutturaFacadeSRV#getScopeScadenziarioStruttura(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public ScopeScadenziarioStrutturaEnum getScopeScadenziarioStruttura(final UtenteDTO utente) {
		ScopeScadenziarioStrutturaEnum scope = null;
		try (Connection con = setupConnection(getDataSource().getConnection(), false)) {
			final Nodo nodo = nodoDAO.getNodo(utente.getIdUfficio(), con);
			if (nodo.getIdTipoStruttura() == null) {
				throw new RedException("L'idTipoStruttura null recuperato per il nodo:" + utente.getIdUfficio());
			}
			final TipoStrutturaNodoEnum tipoStruttura = TipoStrutturaNodoEnum.get(nodo.getIdTipoStruttura());
			if (nodo.getFlagSegreteria() == 1 && TipoStrutturaNodoEnum.SEGRETERIA == tipoStruttura) {
				scope = ScopeScadenziarioStrutturaEnum.SEGRETERIA;
			} else if (nodo.getFlagSegreteria() == 1 && TipoStrutturaNodoEnum.ISPETTORATO == tipoStruttura) {
				scope = ScopeScadenziarioStrutturaEnum.ISPETTORATO;
			} else {
				scope = ScopeScadenziarioStrutturaEnum.UFFICIO;
			}
		} catch (final Exception e) {
			LOGGER.error("Impossibile recuperare lo scope scadenziario", e);
		}
		return scope;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IScadenziaroStrutturaFacadeSRV#retrieveIdDocumentoByPeIdDocument(java.util.List,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<DocumentoScadenzatoDTO> retrieveIdDocumentoByPeIdDocument(final List<PEDocumentoScadenzatoDTO> peList, final UtenteDTO utente) {
		final List<DocumentoScadenzatoDTO> dsList = new ArrayList<>(peList.size());
		Connection con = null;
		IFilenetCEHelper fceh = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			for (final PEDocumentoScadenzatoDTO peDoc : peList) {
				final Document doc = fceh.getDocumentByIdGestionale(peDoc.getIdDocumento(), FromDocumentoToIdDocumentDTOTrasformer.getMetadati(), null, utente.getIdAoo().intValue(),
						null, null);

				// Alcuni documenti ritornati dal PE non possono essere visualizzati nel CE
				// perché non si hanno le credenziali per visualizzarli
				if (doc != null) {
					final IdDocumentDTO idDoc = TrasformCE.transform(doc, TrasformerCEEnum.FROM_DOCUMENTO_TO_ID_DOCUMENTO);

					UfficioDTO ufficioDestinatario = null;
					if (peDoc.getIdNodoDestinatario() != null && peDoc.getIdNodoDestinatario() >= 0) {
						final Nodo nodo = nodoDAO.getNodo(Long.valueOf(peDoc.getIdNodoDestinatario()), con);
						ufficioDestinatario = new UfficioDTO(nodo.getIdNodo(), nodo.getDescrizione());
					}

					Utente utenteDestinatario = null;
					if (peDoc.getIdUtenteDestinatario() != null && peDoc.getIdUtenteDestinatario() >= 0) {
						utenteDestinatario = utenteDAO.getUtente(Long.valueOf(peDoc.getIdUtenteDestinatario()), con);
					}

					final DocumentoScadenzatoDTO ds = new DocumentoScadenzatoDTO(idDoc, peDoc, ufficioDestinatario, utenteDestinatario);
					dsList.add(ds);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Impossibile recuperare i documenti scadenzati dal CE", e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}

		return dsList;
	}

}
