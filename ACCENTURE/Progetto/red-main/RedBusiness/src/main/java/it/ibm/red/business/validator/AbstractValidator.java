package it.ibm.red.business.validator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Abstract Validator.
 * @param <T>
 * @param <E>
 */
public abstract class AbstractValidator<T, E extends Enum<E>> {

	/**
	 * Lista errori.
	 */
	private List<E> errori;

	/**
	 * Costruttore di default che inizializza la lista degli errori.
	 */
	protected AbstractValidator() {
		errori = new ArrayList<>();
	}
	
	/**
	 * Restituisce la lista degli errori.
	 * @return errori
	 */
	public List<E> getErrori() {
		return errori;
	}
	
	/**
	 * Gestisce la logica di validazione di una lista di oggetti.
	 * @param oggettiDaValidare
	 * @return
	 */
	public abstract List<E> validaTutti(Collection<T> oggettiDaValidare);
	
	/**
	 * Gestisce la logica di validazione per un oggetto.
	 * @param oggettoDaValidare
	 * @return
	 */
	public abstract List<E> valida(T oggettoDaValidare);
}