package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IComuneDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.dto.ComuneDTO;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IComuneSRV;

/**
 * Service per la gestione dei comuni.
 */
@Service
@Component
public class ComuneSRV extends AbstractService implements IComuneSRV {

	/**
	 * UID.
	 */
	private static final long serialVersionUID = 638309974199202805L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ComuneSRV.class);
	
	/**
	 * Dao.
	 */
	@Autowired
	private IComuneDAO comuneDAO;
	
	/**
	 * Datasource.
	 */
	private static final String DATA_SOURCE = "DataSource";
	
	/**
	 * Messaggio.
	 */
	private static final String INIZIO_RICERCA_DEI_COMUNI = "Inizio Ricerca dei Comuni ";

	/**
	 * Costruttore di default.
	 */
	public ComuneSRV() {
		super();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IComuneFacadeSRV#getAll().
	 */
	@Override
	public Collection<ComuneDTO> getAll() {
		Collection<ComuneDTO> comuni = null;
		Connection conn = null;
		try {
			LOGGER.info(INIZIO_RICERCA_DEI_COMUNI);
			conn = ((DataSource) ApplicationContextProvider.getApplicationContext().getBean(DATA_SOURCE))
					.getConnection();
			comuni = comuneDAO.getAll(conn);

		} catch (BeansException | SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
		return comuni;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IComuneFacadeSRV#get(java.lang.String).
	 */
	@Override
	public ComuneDTO get(final String id) {
		ComuneDTO comune = null;
		Connection conn = null;
		try {
			LOGGER.info(INIZIO_RICERCA_DEI_COMUNI);
			conn = ((DataSource) ApplicationContextProvider.getApplicationContext().getBean(DATA_SOURCE))
					.getConnection();
			comune = comuneDAO.get(id, conn);

		} catch (BeansException | SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
		return comune;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IComuneFacadeSRV#getComuni(java.lang.Long,
	 *      java.lang.String).
	 */
	@Override
	public List<ComuneDTO> getComuni(final Long idProvincia, final String query) {
		List<ComuneDTO> comuni = null;
		Connection conn = null;
		try {
			LOGGER.info(INIZIO_RICERCA_DEI_COMUNI);
			conn = ((DataSource) ApplicationContextProvider.getApplicationContext().getBean(DATA_SOURCE))
					.getConnection();
			comuni = comuneDAO.getComuni(idProvincia, query, conn);

		} catch (BeansException | SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
		return comuni;
	}

}
