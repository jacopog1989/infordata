package it.ibm.red.business.dao.impl;

import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IRuoloDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.CoordinatoreUfficioDTO;
import it.ibm.red.business.dto.PreferenzeDTO;
import it.ibm.red.business.dto.ResponsabileCopiaConformeDTO;
import it.ibm.red.business.dto.TracciaDocUscitaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.NodoUtenteCoordinatore;
import it.ibm.red.business.persistence.model.NodoUtenteRuolo;
import it.ibm.red.business.persistence.model.Ruolo;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.persistence.model.UtenteFirma;
import it.ibm.red.business.persistence.model.UtentePreferenze;
import it.ibm.red.business.utils.PermessiUtils;

/**
 * The Class UtenteDAO.
 *
 * @author CPIERASC
 * 
 *         Dao per la gestione dell'utente.
 */
@Repository
public class UtenteDAO extends AbstractDAO implements IUtenteDAO {

	private static final String IDUTENTEDELEGANTE = "IDUTENTEDELEGANTE";

	private static final String CONTENTFILE = "CONTENTFILE";

	private static final String PREDEFINITO = "PREDEFINITO";

	private static final String QUERY = "Query: ";

	private static final String IDRUOLO = "IDRUOLO";

	/**
	 * Messaggio errore recupero utente. Occorre concatenare l'id dell'utente a
	 * questo messaggio.
	 */
	private static final String ERROR_RECUPERO_UTENTE_MSG = "Errore durante il recupero dell'utente tramite id=";

	/**
	 * Messaggio errore recupero dati utenti per l'ufficio. Occorre concatenare l'id
	 * ufficio a questo messaggio.
	 */
	private static final String ERROR_RECUPERO_DATI_UTENTE_MSG = "Errore durante il recupero dal DB dei dati utente per l'ufficio: ";

	/**
	 * Stirnga errore durante recupero utente ed ufficio.
	 */
	private static final String ERRORE_DURANTE_IL_RECUPERO_DELL_UTENTE_UFFICIO = "Errore durante il recupero dell'utente/ufficio ";

	/**R
	 * Campo nome tabella utente.
	 */
	private static final String NOME = "NOME";

	/**
	 * Campo cognome tabella utente.
	 */
	private static final String COGNOME = "COGNOME";

	/**
	 * Campo username tabella utente.
	 */
	private static final String USERNAME_C = "USERNAME";

	/**
	 * Campo registroRiservato tabella utente.
	 */
	private static final String REGISTRORISERVATO = "REGISTRORISERVATO";

	/**
	 * Campo email tabella utente.
	 */
	private static final String EMAIL = "EMAIL";

	/**
	 * Campo codFisc tabella utente.
	 */
	private static final String CODFIS = "CODFIS";

	/**
	 * Campo idUtente tabella utente.
	 */
	private static final String IDUTENTE_C = "IDUTENTE";

	/**
	 * Campo dataDisattivazione tabella utente.
	 */
	private static final String DATADISATTIVAZIONE = "DATADISATTIVAZIONE";

	/**
	 * Campo dataDisattivazione tabella utente.
	 */
	private static final String DATAATTIVAZIONE = "DATAATTIVAZIONE";

	/**
	 * Campo idNodo tabella utente.
	 */
	private static final String IDNODO = "IDNODO";

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 4759154120616893062L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(UtenteDAO.class.getName());

	/**
	 * Dao per la gestione dei ruoli.
	 */
	@Autowired
	private IRuoloDAO ruoloDAO;

	/**
	 * Dao per la gestione dei nodi.
	 */
	@Autowired
	private INodoDAO nodoAOO;

	/**
	 * Gets the nodo utente ruolo.
	 *
	 * @param idUtente
	 *            the id utente
	 * @param idRuolo
	 *            the id ruolo
	 * @param connection
	 *            the connection
	 * @return the nodo utente ruolo
	 */
	@Override
	public final Collection<NodoUtenteRuolo> getNodoUtenteRuolo(final Long idUtente, final Long idRuolo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final Collection<NodoUtenteRuolo> nodoUtenteRuolo = new ArrayList<>();
		try {

			// Costruzione della query in base ad utente e ruolo.

			String strIdUtenteSelect = "";
			if (idUtente != null) {
				strIdUtenteSelect = ", UTENTE u ";
			}

			String strIdUtenteWhere = "";
			if (idUtente != null) {
				strIdUtenteWhere = "u.idutente = nur.idutente AND u.idutente = " + sanitize(idUtente);
			}

			String strIdRuoloSelect = "";
			if (idRuolo != null) {
				strIdRuoloSelect = ", RUOLO r ";
			}

			String strIdRuoloWhere = "";
			if (idRuolo != null) {
				if (StringUtils.isNotBlank(strIdUtenteWhere)) {
					strIdRuoloWhere = " AND ";
				}
				strIdRuoloWhere += "r.idruolo = nur.idruolo AND r.idruolo = " + sanitize(idRuolo) + " AND r.datadisattivazione IS NULL ";
			}

			// Composizione della query con la parte statica.

			final String selectSQL = "SELECT nur.* FROM NODOUTENTERUOLO nur " + strIdUtenteSelect + strIdRuoloSelect;
			final String whereCondition = " WHERE (nur.dataDisattivazione IS NULL OR nur.dataDisattivazione > sysdate) AND " + strIdUtenteWhere + strIdRuoloWhere;
			final String querySQL = selectSQL + whereCondition;

			// Esecuzione della query
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();

			// Gestione dei risultati

			while (rs.next()) {
				final Long idNodo = rs.getLong(IDNODO);
				final BigDecimal activeUSer = rs.getBigDecimal("ACTIVEUSER");
				final Date dataAttivazione = rs.getDate(DATAATTIVAZIONE);
				final Date dataDisattivazione = rs.getDate(DATADISATTIVAZIONE);
				final Long idRuoloDB = rs.getLong(IDRUOLO);
				final Long idUtenteDB = rs.getLong(IDUTENTE_C);
				final Long predefinito = rs.getLong(PREDEFINITO);
				final Integer gestioneApplicativa = rs.getInt("GESTIONEAPPLICATIVA");

				// Recupero del nodo a partire dal suo id.
				final Nodo nodo = nodoAOO.getNodo(idNodo, connection);

				// Recupero del ruolo a partire dal suo id.
				final Ruolo ruolo = ruoloDAO.getRuolo(idRuoloDB, connection);

				// Recupero dell'utente a partire dal suo id.
				final Utente utente = getUtente(idUtente, connection);
				if (nodo != null && nodo.getDataDisattivazione() == null) {
					// Accesso alla cross nodo-utente-ruolo utilizzando le entità recuperate in
					// precedenza.
					final NodoUtenteRuolo nur = new NodoUtenteRuolo(activeUSer, dataAttivazione, dataDisattivazione, idNodo, idRuoloDB, idUtenteDB, nodo, predefinito, ruolo,
							utente, gestioneApplicativa);
					nodoUtenteRuolo.add(nur);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero degli utenti attivi ", e);
			throw new RedException("Errore durante il recupero degli utenti attivi ", e);
		} finally {
			closeStatement(ps, rs);
		}
		return nodoUtenteRuolo;
	}

	/**
	 * Gets the by username.
	 *
	 * @param username
	 *            the username
	 * @param connection
	 *            the connection
	 * @return the by username
	 */
	@Override
	public final Utente getByUsername(final String username, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		String usernameUpper = username;
		Utente ut = null;
		try {
			// Rendiamo lo username in uppercase per confrontarlo poi con l'uppercase di un
			// campo del database
			if (usernameUpper != null) {
				usernameUpper = usernameUpper.toUpperCase();
			}
			LOGGER.info("Recupero dell'utente tramite username = " + usernameUpper);
			// Recupero del utente accedendo alle sue principali cross.
			final String querySQL = "SELECT u.*, uf.CONTENTFILE, uf.IDUTENTEDELEGANTE, uf.SIGNER, uf.PIN, uf.SIGNER_PIN, uf.REASON, uf.CUSTOMER_INFO, pk.HANDLER, "
					+ " uf.PIN_VERIFICATO, pk.SECURE_PIN " + " FROM Utente u, NodoUtenteRuolo nur, UtenteFirma uf, Pk_Handler pk "
					+ " WHERE u.idutente = nur.IDUTENTE AND u.dataDisattivazione IS NULL "
					+ " AND u.idutente = uf.idutente(+) AND uf.PK_HANDLER_ID = pk.id(+) and UPPER(u.username) = ? ";
			LOGGER.info(QUERY + querySQL);
			ps = connection.prepareStatement(querySQL);
			ps.setString(1, usernameUpper);
			rs = ps.executeQuery();
			// Gestione dei risultati.
			if (rs.next()) {
				byte[] contentByte = null;
				final Blob sqlBlob = rs.getBlob(CONTENTFILE);
				if (sqlBlob != null) {
					contentByte = sqlBlob.getBytes(1, (int) sqlBlob.length());
					sqlBlob.free();
				}
				Long idUtenteDelegante = null;
				if (rs.getLong(IDUTENTEDELEGANTE) > 0) {
					idUtenteDelegante = rs.getLong(IDUTENTEDELEGANTE);
				}

				byte[] securPINCert = null;
				final Blob securPINCertBlob = rs.getBlob("SECURE_PIN");
				if (securPINCertBlob != null) {
					securPINCert = securPINCertBlob.getBytes(1, (int) securPINCertBlob.length());
					securPINCertBlob.free();
				}
				boolean pinVerificato = false;
				if (rs.getInt("PIN_VERIFICATO") == 1) {
					pinVerificato = true;
				}
				final UtenteFirma uf = new UtenteFirma(rs.getLong(IDUTENTE_C), contentByte, idUtenteDelegante, rs.getString("HANDLER"), rs.getString("SIGNER"),
						rs.getString("PIN"), rs.getString("SIGNER_PIN"), rs.getString("REASON"), rs.getString("CUSTOMER_INFO"), securPINCert, pinVerificato);
				// Accesso alle preferenze dell'utente

				ut = new Utente(rs.getLong(IDUTENTE_C), rs.getString(NOME), rs.getString(COGNOME), rs.getString(USERNAME_C), rs.getDate(DATAATTIVAZIONE),
						rs.getDate(DATADISATTIVAZIONE), rs.getString(CODFIS), rs.getString(EMAIL), rs.getLong(REGISTRORISERVATO), uf);

				ps2 = connection.prepareStatement("select * from UTENTEPREFERENZE u where u.IDUTENTE = ?");
				ps2.setLong(1, ut.getIdUtente());
				rs2 = ps2.executeQuery();

				if (rs2.next()) {
					final UtentePreferenze up = new UtentePreferenze(rs2.getLong(IDUTENTE_C), rs2.getInt("ID_TEMA_MOBILE"), rs2.getString("PAGINA_INIZIALE"),
							rs2.getInt("AUTOCOMPLETE_DELAY"), rs2.getInt("NUMBER_ELEMENTS"), rs2.getString("PAGINA_PREFERITA"), rs2.getInt("SIZE_CARATTERE"),
							rs2.getBoolean("GROUP_SCRIVANIA"), rs2.getBoolean("GROUP_ARCHIVIO"), rs2.getBoolean("GROUP_UFFICIO"), rs2.getBoolean("GROUP_MAIL"),
							rs2.getBoolean("GROUP_FATT_ELET"), rs2.getBoolean("GROUP_ORGANIGRAMMA"), rs2.getBoolean("NATIVE_READER_PDF_ENABLE"), rs2.getBoolean("IS_BASE64"),
							rs2.getBoolean("URL_ENCODING"), rs2.getBoolean("NO_CRYPT"), rs2.getBoolean("FIRMA_REMOTA_FIRST"));
					ut.setUtentePreferenze(up);
				}
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero dell'utente tramite username " + usernameUpper, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
			closeStatement(ps2, rs2);
		}
		return ut;
	}

	/**
	 * Restituisce l'immagine del glifo della specifica aoo.
	 * 
	 * @param idAOO
	 *            id area organizzativa
	 * @param confPDFA
	 *            flag associato alla transparency della postilla
	 * @param connection
	 *            connessione al db per il recupero dell'immagine
	 * @return byte array dell'immagine
	 */
	@Override
	public final byte[] getImageFirmaAOO(final Long idAOO, final boolean confPDFA, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		byte[] contentReturn = null;
		try {
			
			// Recupero del utente accedendo alle sue principali cross.
			final String querySQL = "SELECT * from IDAOO_GLIFO_POSTILLA WHERE IDAOO = ? ";
			LOGGER.info(QUERY + querySQL);
			ps = connection.prepareStatement(querySQL);
			ps.setLong(1, idAOO);
			rs = ps.executeQuery();
			// Gestione dei risultati.
			if (rs.next()) {
				Blob contentReturnBlob = null;
				if(confPDFA) {
					contentReturnBlob = rs.getBlob("GLIFO_POSTILLA_CONFPDFA");
					
				}else {
					contentReturnBlob = rs.getBlob("GLIFO_POSTILLA");
				}
				
				if (contentReturnBlob != null) {
					contentReturn = contentReturnBlob.getBytes(1, (int) contentReturnBlob.length());
					contentReturnBlob.free();
				}
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero del glifo tramite l'idAOO " +idAOO, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return contentReturn;
	}
	
	/**
	 * Restituisce il testo della postilla che definisce la postilla.
	 * 
	 * @param idAOO
	 *            identificativo dell'AOO.
	 * @param connection
	 *            connessione al database
	 * @return testo della postilla
	 */
	@Override
	public final String getTestoPostilla(final Long idAOO, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String returnString= null;
		try {
			// Recupero del utente accedendo alle sue principali cross.
			final String querySQL = "SELECT * from IDAOO_GLIFO_POSTILLA WHERE IDAOO = ? ";
			LOGGER.info(QUERY + querySQL);
			ps = connection.prepareStatement(querySQL);
			ps.setLong(1, idAOO);
			rs = ps.executeQuery();
			// Gestione dei risultati.
			if (rs.next()) {
				returnString = rs.getString("TESTO_POSTILLA_ADOBE");	
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero del glifo tramite l'idAOO " +idAOO, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return returnString;
	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#getByUsernameForSpedizione(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public final String getByUsernameForSpedizione(final String username, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String usernameUpper = username;
		String nomeUtente = "";
		try {
			// Rendiamo lo username in uppercase per confrontarlo poi con l'uppercase di un
			// campo del database
			if (usernameUpper != null) {
				usernameUpper = usernameUpper.toUpperCase();
			}
			LOGGER.info("Recupero dell'utente tramite username = " + usernameUpper);
			// Recupero del utente accedendo alle sue principali cross.
			final String querySQL = "SELECT u.NOME, u.COGNOME " + " FROM Utente u " + " WHERE UPPER(u.username) = ? ";
			LOGGER.info(QUERY + querySQL);
			ps = connection.prepareStatement(querySQL);
			ps.setString(1, usernameUpper);
			rs = ps.executeQuery();
			// Gestione dei risultati.
			if (rs.next()) {
				nomeUtente = rs.getString("NOME") + " " + rs.getString(COGNOME);
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero dell'utente tramite username " + usernameUpper, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return nomeUtente;
	}

	/**
	 * Gets the utente.
	 *
	 * @param idUtente
	 *            the id utente
	 * @param connection
	 *            the connection
	 * @return the utente
	 */
	@Override
	public final Utente getUtente(final Long idUtente, final Connection connection) {
		final UtenteFirma uf = getUtenteFirma(idUtente, connection);

		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			final String querySQL = "SELECT u.* FROM Utente u WHERE u.idutente = ?";
			ps = connection.prepareStatement(querySQL);
			ps.setLong(index, idUtente);
			
			rs = ps.executeQuery();
			if (rs.next()) {

				return new Utente(rs.getLong(IDUTENTE_C), rs.getString(NOME), rs.getString(COGNOME), rs.getString(USERNAME_C), rs.getDate(DATAATTIVAZIONE),
						rs.getDate(DATADISATTIVAZIONE), rs.getString(CODFIS), rs.getString(EMAIL), rs.getLong(REGISTRORISERVATO), uf);
			}

		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_UTENTE_MSG + idUtente, e);
			throw new RedException(ERROR_RECUPERO_UTENTE_MSG + idUtente, e);
		} finally {
			closeStatement(ps, rs);
		}

		return null;
	}

	/**
	 * Gets the utente.
	 *
	 * @param idUtente
	 *            the id utente
	 * @param connection
	 *            the connection
	 * @return the utente
	 */
	@Override
	public final UtenteFirma getUtenteFirma(final Long idUtente, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			final String querySQL = "SELECT u.* FROM UtenteFirma u WHERE u.idutente = ?";
			ps = connection.prepareStatement(querySQL);
			ps.setLong(index, idUtente);
			
			rs = ps.executeQuery();
			if (rs.next()) {

				return new UtenteFirma(rs.getLong(IDUTENTE_C), rs.getBytes(CONTENTFILE), rs.getLong(IDUTENTEDELEGANTE), null, null, rs.getString("PIN"),
						rs.getString("SIGNER_PIN"), rs.getString("REASON"), rs.getString("CUSTOMER_INFO"), null, rs.getBoolean("PIN_VERIFICATO"));
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_UTENTE_MSG + idUtente, e);
			throw new RedException(ERROR_RECUPERO_UTENTE_MSG + idUtente, e);
		} finally {
			closeStatement(ps, rs);
		}
		return null;
	}

	/**
	 * Checks if is delegato.
	 *
	 * @param idRuolo
	 *            the id ruolo
	 * @param connection
	 *            the connection
	 * @return the boolean
	 */
	@Override
	public final boolean isDelegato(final Long idRuolo, final Connection connection) {
		final Ruolo ruolo = ruoloDAO.getRuolo(idRuolo, connection);
		return PermessiUtils.isDelegato(ruolo.getPermessi().longValue());
	}

	/**
	 * Checks if is corriere.
	 *
	 * @param idRuolo
	 *            the id ruolo
	 * @param connection
	 *            the connection
	 * @return the boolean
	 */
	@Override
	public final boolean isCorriere(final Long idRuolo, final Connection connection) {
		final Ruolo ruolo = ruoloDAO.getRuolo(idRuolo, connection);
		return PermessiUtils.isCorriere(ruolo.getPermessi().longValue());
	}

	/**
	 * Checks if is tab scadenzario.
	 *
	 * @param idRuolo
	 *            the id ruolo
	 * @param connection
	 *            the connection
	 * @return the boolean
	 */
	@Override
	public final Boolean isTabScadenzario(final Long idRuolo, final Connection connection) {
		final Ruolo ruolo = ruoloDAO.getRuolo(idRuolo, connection);
		return PermessiUtils.isTabScadenzario(ruolo.getPermessi().longValue());
	}

	/**
	 * Checks for libro firma.
	 *
	 * @param idRuolo
	 *            the id ruolo
	 * @param connection
	 *            the connection
	 * @return the boolean
	 */
	@Override
	public final Boolean hasLibroFirma(final Long idRuolo, final Connection connection) {
		final Ruolo ruolo = ruoloDAO.getRuolo(idRuolo, connection);
		return PermessiUtils.hasLibroFirma(ruolo.getPermessi().longValue());
	}

	/**
	 * Checks for libro firma.
	 *
	 * @param idRuolo
	 *            the id ruolo
	 * @param connectf
	 */
	@Override
	public final boolean hasLibroFirmaMobile(final Long idRuolo, final Aoo aoo, final Connection connection) {
		final Ruolo ruolo = ruoloDAO.getRuolo(idRuolo, aoo, connection);
		return PermessiUtils.hasLibroFirmaMobile(ruolo.getPermessi().longValue());
	}

	/**
	 * Gets the utente ufficio by id utente and id nodo.
	 *
	 * @param idUtente
	 *            the id utente
	 * @param idUfficio
	 *            the id ufficio
	 * @param connection
	 *            the connection
	 * @return the utente ufficio by id utente and id nodo
	 */
	@Override
	public final NodoUtenteRuolo getUtenteUfficioByIdUtenteAndIdNodo(final Long idUtente, final Long idUfficio, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		NodoUtenteRuolo nur = null;
		try {
			final String querySQL = "SELECT DISTINCT ute.idutente, ute.nome, ute.cognome, ute.username, ute.dataattivazione, "
					+ " ute.datadisattivazione, ute.email, ute.codfis, ute.registroriservato, nod.idaoo, nod.idnodo, nod.descrizione, "
					+ " nur.dataattivazione as dataattivazionenur, nur.datadisattivazione as datadisattivazionenur "
					+ " FROM utente ute RIGHT OUTER JOIN nodoutenteruolo nur ON nur.idutente = ute.idutente RIGHT OUTER JOIN nodo nod ON nur.idnodo = nod.idnodo "
					+ " WHERE ute.idutente = ? AND nod.idnodo = ? " + " ORDER BY dataattivazionenur desc, datadisattivazionenur desc ";
			ps = connection.prepareStatement(querySQL);
			ps.setLong(1, idUtente);
			ps.setLong(2, idUfficio);
			rs = ps.executeQuery();
			if (rs.next()) {
				nur = new NodoUtenteRuolo();
				final Utente utente = new Utente(rs.getLong(IDUTENTE_C), rs.getString(NOME), rs.getString(COGNOME), rs.getString(USERNAME_C), rs.getDate(DATAATTIVAZIONE),
						rs.getDate(DATADISATTIVAZIONE), rs.getString(CODFIS), rs.getString(EMAIL), rs.getLong(REGISTRORISERVATO), null);
				nur.setUtente(utente);
				nur.setNodo(nodoAOO.getNodo(rs.getLong(IDNODO), connection));
			}
		} catch (final SQLException e) {
			throw new RedException(ERRORE_DURANTE_IL_RECUPERO_DELL_UTENTE_UFFICIO, e);
		} finally {
			closeStatement(ps, rs);
		}
		return nur;
	}

	/**
	 * Change mobile preference.
	 *
	 * @param idUtente
	 *            the id utente
	 * @param pref
	 * @param connection
	 *            the connection
	 */
	@Override
	public final void changeMobilePreference(final long idUtente, final PreferenzeDTO pref, final Connection connection) {
		PreparedStatement ps = null;
		try {
			String updateQuery = "UPDATE UTENTEPREFERENZE SET PAGINA_INIZIALE = " + sanitize(pref.getHomepage()) + ",  ID_TEMA_MOBILE = " + sanitize(pref.getIdTheme())
					+ ",  AUTOCOMPLETE_DELAY = " + sanitize(pref.getAutocompleteDelay()) + ",  PAGINA_PREFERITA = " + sanitize(pref.getPaginaPreferita())
					+ ",  SIZE_CARATTERE = " + sanitize(pref.getTextSize()) + ",  GROUP_SCRIVANIA = " + fromBooleanToChar(pref.isGroupScrivania()) + ",  GROUP_ARCHIVIO = "
					+ fromBooleanToChar(pref.isGroupArchivio()) + ",  GROUP_UFFICIO = " + fromBooleanToChar(pref.isGroupUfficio()) + ",  GROUP_MAIL = "
					+ fromBooleanToChar(pref.isGroupMail()) + ",  GROUP_FATT_ELET = " + fromBooleanToChar(pref.isGroupFatElettronica()) + ",  GROUP_ORGANIGRAMMA = "
					+ fromBooleanToChar(pref.isGroupOrganigrama()) + ",  NATIVE_READER_PDF_ENABLE = " + fromBooleanToChar(pref.isNativeReaderPDFEnable()) + ",  IS_BASE64 = "
					+ fromBooleanToChar(pref.getBase64()) + ",  URL_ENCODING = " + fromBooleanToChar(pref.getUrlEncoding()) + ",  NO_CRYPT = "
					+ fromBooleanToChar(pref.getNoCrypt());

			if (pref.isFirmaRemotaFirst()) {
				updateQuery += ",  FIRMA_REMOTA_FIRST = 1";
			} else {
				updateQuery += ",  FIRMA_REMOTA_FIRST = 0";
			}
			updateQuery += " WHERE IDUTENTE = " + sanitize(idUtente);

			ps = connection.prepareStatement(updateQuery);
			final Integer output = ps.executeUpdate();
			if (output == null || output == 0) {
				closeStatement(ps, null);
				final String insert = "INSERT INTO UTENTEPREFERENZE(PAGINA_INIZIALE, " + "ID_TEMA_MOBILE, " + "IDUTENTE, " + "AUTOCOMPLETE_DELAY, "
				// + "NUMBER_ELEMENTS, "
						+ "PAGINA_PREFERITA, " + "SIZE_CARATTERE, " + "GROUP_SCRIVANIA, " + "GROUP_ARCHIVIO, " + "GROUP_UFFICIO, " + "GROUP_MAIL, " + "GROUP_FATT_ELET, "
						+ "GROUP_ORGANIGRAMMA, NATIVE_READER_PDF_ENABLE, IS_BASE64, URL_ENCODING, NO_CRYPT) " + "VALUES (" + sanitize(pref.getHomepage()) + ","
						+ sanitize(pref.getIdTheme()) + "," + sanitize(idUtente) + "," + sanitize(pref.getAutocompleteDelay())
						// + "," + sanitize(pref.getNumberOfElements())
						+ "," + sanitize(pref.getPaginaPreferita()) + "," + sanitize(pref.getTextSize()) + "," + fromBooleanToChar(pref.isGroupScrivania()) + ","
						+ fromBooleanToChar(pref.isGroupArchivio()) + "," + fromBooleanToChar(pref.isGroupUfficio()) + "," + fromBooleanToChar(pref.isGroupMail()) + ","
						+ fromBooleanToChar(pref.isGroupFatElettronica()) + "," + fromBooleanToChar(pref.isGroupOrganigrama()) + ","
						+ fromBooleanToChar(pref.isNativeReaderPDFEnable()) + "," + fromBooleanToChar(pref.getBase64()) + "," + fromBooleanToChar(pref.getUrlEncoding()) + ","
						+ fromBooleanToChar(pref.getNoCrypt()) + ")";
				ps = connection.prepareStatement(insert);
				ps.execute();
			}
		} catch (final SQLException e) {
			throw new RedException(ERRORE_DURANTE_IL_RECUPERO_DELL_UTENTE_UFFICIO, e);
		} finally {
			closeStatement(ps, null);
		}
	}

	/**
	 * Sets the pin verificato.
	 *
	 * @param idUtente
	 *            the id utente
	 * @param pin
	 *            the pin
	 * @param pinVerificato
	 *            the pin verificato
	 * @param connection
	 *            the connection
	 */
	@Override
	public final void setPinVerificato(final Long idUtente, final String pin, final boolean pinVerificato, final Connection connection) {
		PreparedStatement ps = null;
		try {
			final String updateQuery = "UPDATE UTENTEFIRMA SET PIN_VERIFICATO = ? , PIN = ? WHERE IDUTENTE = ?";
			ps = connection.prepareStatement(updateQuery);
			final int index = 1;
			ps.setInt(index, 0);
			ps.setNull(index + 1, Types.VARCHAR);
			if (pinVerificato) {
				ps.setInt(index, 1);
				ps.setString(index + 1, pin);
			}
			ps.setInt(index + 2, idUtente.intValue());

			final Integer output = ps.executeUpdate();
			if (output == null || output == 0) {
				throw new RedException("Errore durante l'update del PIN VERIFICATO=" + pinVerificato + " per l'utente con ID=" + idUtente);
			}
		} catch (final SQLException e) {
			throw new RedException("SQLException durante l'update del PIN VERIFICATO=" + pinVerificato + " per l'utente con ID=" + idUtente, e);
		} finally {
			closeStatement(ps, null);
		}
	}

	/**
	 * Gets the utenti delegati.
	 *
	 * @param idNodoDestinatario
	 *            the id nodo destinatario
	 * @param con
	 *            the con
	 * @return the utenti delegati
	 */
	@Override
	public final List<Utente> getUtentiDelegati(final int idNodoDestinatario, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<Utente> utenti = new ArrayList<>();
		try {
			final String query = "SELECT ut.*" + "FROM nodoutenteruolo uffutru, utente ut, nodo uff, ruolo ru, aoo ao" + " WHERE uffutru.idnodo = uff.idnodo"
					+ " AND uffutru.idruolo = ru.idruolo" + " AND uffutru.idutente = ut.idutente" + " AND ao.idaoo = uff.idaoo" + " AND uffutru.idnodo = ?"
					+ " AND bitand(ru.permessi, ?) > 0" + " AND ao.datadisattivazione IS NULL" + " AND uff.datadisattivazione IS NULL" + " AND ru.datadisattivazione IS NULL"
					+ " AND uffutru.datadisattivazione IS NULL" + " AND ut.datadisattivazione IS NULL";
			ps = con.prepareStatement(query);
			ps.setInt(1, idNodoDestinatario);
			ps.setLong(2, PermessiEnum.DELEGA.getId());
			rs = ps.executeQuery();

			Utente utente = null;
			while (rs.next()) {
				utente = new Utente(rs.getLong(IDUTENTE_C), rs.getString(NOME), rs.getString(COGNOME), rs.getString(USERNAME_C), rs.getDate(DATAATTIVAZIONE),
						rs.getDate(DATADISATTIVAZIONE), rs.getString(CODFIS), rs.getString(EMAIL), rs.getLong(REGISTRORISERVATO), null);
				utenti.add(utente);
			}

		} catch (final SQLException e) {
			throw new RedException(ERRORE_DURANTE_IL_RECUPERO_DELL_UTENTE_UFFICIO, e);
		} finally {
			closeStatement(ps, rs);
		}
		return utenti;
	}

	/**
	 * Gets the coordinatori ufficio.
	 *
	 * @param idUfficio
	 *            the id ufficio
	 * @param con
	 *            the con
	 * @return the coordinatori ufficio
	 */
	@Override
	public final Collection<CoordinatoreUfficioDTO> getCoordinatoriUfficio(final Integer idUfficio, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final Collection<CoordinatoreUfficioDTO> output = new ArrayList<>();
		try {
			final String query = "SELECT ute.*, nuc.idnodocoordinatore " + "FROM NODO nod, UTENTE ute, NODOUTENTECOORDINATORE nuc " + "WHERE nuc.idnodomittente = nod.idnodo "
					+ "AND nuc.idutentecoordinatore = ute.idutente " + "AND nod.datadisattivazione IS NULL " + "AND ute.datadisattivazione IS NULL "
					+ "AND nuc.idnodomittente = ? ";
			ps = con.prepareStatement(query);
			ps.setInt(1, idUfficio);
			rs = ps.executeQuery();
			while (rs.next()) {
				final CoordinatoreUfficioDTO cu = new CoordinatoreUfficioDTO(rs.getInt(IDUTENTE_C), rs.getString("NOME"), rs.getString(COGNOME));
				output.add(cu);
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero dei coordinatori ", e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#getUtentiRegistroRiservato(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public final Collection<Utente> getUtentiRegistroRiservato(final Long idAoo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final Collection<Utente> output = new ArrayList<>();

		try {
			final String query = "SELECT ute.* " + "FROM utente ute, nodo nod, nodoutenteruolo nur " + "WHERE ute.idutente = nur.idutente " + "AND nod.idnodo = nur.idnodo "
					+ "AND nod.idaoo = ? " + "AND ute.registroriservato = 1 ";
			ps = con.prepareStatement(query);
			ps.setLong(1, idAoo);

			rs = ps.executeQuery();

			while (rs.next()) {
				final Utente utenteRegistroRiservato = new Utente(rs.getLong(IDUTENTE_C), rs.getString(NOME), rs.getString(COGNOME), rs.getString(USERNAME_C),
						rs.getDate(DATAATTIVAZIONE), rs.getDate(DATADISATTIVAZIONE), rs.getString(CODFIS), rs.getString(EMAIL), rs.getLong(REGISTRORISERVATO), null);
				output.add(utenteRegistroRiservato);
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero degli utenti con registro riservato dell'AOO: " + idAoo, e);
			throw new RedException("Errore durante il recupero degli utenti con registro riservato dell'AOO: " + idAoo, e);
		} finally {
			closeStatement(ps, rs);
		}

		return output;
	}

	/**
	 * Checks if is corriere.
	 *
	 * @param idRuolo
	 *            the id ruolo
	 * @param connection
	 *            the connection
	 * @return the boolean
	 */
	@Override
	public final Boolean isAmministratore(final Long idRuolo, final Connection connection) {
		final Ruolo ruolo = ruoloDAO.getRuolo(idRuolo, connection);
		return PermessiUtils.isAmministratore(ruolo.getPermessi().longValue());
	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#getNodoUtenteCoordinatore(java.lang.Long,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public NodoUtenteCoordinatore getNodoUtenteCoordinatore(final Long idUfficioMittente, final Long idUtenteCoordinatore, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		NodoUtenteCoordinatore utenteCoordinatore = null;
		try {
			int index = 1;
			final String query = "SELECT ute.*, nuc.idnodocoordinatore FROM NODO nod, UTENTE ute, NODOUTENTECOORDINATORE nuc "
					+ " WHERE nuc.idnodomittente = nod.idnodo AND nuc.idutentecoordinatore = ute.idutente AND nuc.idnodomittente = ? AND nuc.idutentecoordinatore = ?";
			ps = con.prepareStatement(query);
			ps.setLong(index++, idUfficioMittente);
			ps.setLong(index++, idUtenteCoordinatore);

			rs = ps.executeQuery();
			if (rs.next()) {
				utenteCoordinatore = new NodoUtenteCoordinatore();
				utenteCoordinatore.setIdUfficioCoordinatore(rs.getLong("IDNODOCOORDINATORE"));
				utenteCoordinatore.setIdUtenteCoordinatore(rs.getLong(IDUTENTE_C));
				utenteCoordinatore.setNome(rs.getString(COGNOME));
				utenteCoordinatore.setCognome(rs.getString("NOME"));
			}
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_DATI_UTENTE_MSG + idUfficioMittente, e);
			throw new RedException(ERROR_RECUPERO_DATI_UTENTE_MSG + idUfficioMittente, e);
		} finally {
			closeStatement(ps, rs);
		}

		return utenteCoordinatore;
	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#getIdRagioniereGeneraleDelloStato(java.sql.Connection).
	 */
	@Override
	public Long getIdRagioniereGeneraleDelloStato(final Connection con) {
		CallableStatement cs = null;
		Long result = -1L;

		try {
			int index = 1;
			cs = con.prepareCall("{call PKG_NSD_PE_RGS.getRagGenDelloStato(?,?)}");
			cs.registerOutParameter(index++, Types.INTEGER);
			cs.registerOutParameter(index++, Types.INTEGER);
			cs.execute();

			result = cs.getLong(2);
		} catch (final SQLException e) {
			LOGGER.error("Errore nel recupero dell'ID del Ragioniere dello stato", e);
			throw new RedException("Errore nel recupero dell'ID del Ragioniere dello stato", e);
		} finally {
			closeStatement(cs);
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#getComboNodiUtenteCoordinatore(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public List<NodoUtenteCoordinatore> getComboNodiUtenteCoordinatore(final Long idUfficioMittente, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<NodoUtenteCoordinatore> list = null;
		try {
			int index = 1;
			final String query = " SELECT ute.*, nuc.idnodocoordinatore " + "  FROM NODO nod, UTENTE ute, NODOUTENTECOORDINATORE nuc "
					+ " WHERE nuc.idnodomittente = nod.idnodo " + "   AND nuc.idutentecoordinatore = ute.idutente " + "   AND nod.datadisattivazione IS NULL "
					+ "   AND ute.datadisattivazione IS NULL " + "   AND nuc.idnodomittente = ?";
			ps = con.prepareStatement(query);
			ps.setLong(index++, idUfficioMittente);

			rs = ps.executeQuery();
			list = new ArrayList<>();
			NodoUtenteCoordinatore utenteCoordinatore = null;
			while (rs.next()) {
				utenteCoordinatore = new NodoUtenteCoordinatore();
				utenteCoordinatore.setIdUfficioCoordinatore(rs.getLong("IDNODOCOORDINATORE"));
				utenteCoordinatore.setIdUtenteCoordinatore(rs.getLong(IDUTENTE_C));
				utenteCoordinatore.setNome(rs.getString(COGNOME));
				utenteCoordinatore.setCognome(rs.getString("NOME"));
				list.add(utenteCoordinatore);
			}
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_DATI_UTENTE_MSG + idUfficioMittente, e);
			throw new RedException(ERROR_RECUPERO_DATI_UTENTE_MSG + idUfficioMittente, e);
		} finally {
			closeStatement(ps, rs);
		}

		return list;
	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#getComboResponsabiliCopiaConforme(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public List<ResponsabileCopiaConformeDTO> getComboResponsabiliCopiaConforme(final Long idAOO, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ResponsabileCopiaConformeDTO> list = null;
		try {
			int index = 1;
			final String query = " SELECT nur.predefinito, " + "       nod.idnodo, " + "       nod.descrizione, " + "       nod.idutentedirigente, " + "       ute.idutente, "
					+ "       ute.nome, " + "       ute.cognome, " + "       ruo.idruolo, " + "       ruo.nomeruolo, " + "       nur.dataattivazione, "
					+ "       nur.datadisattivazione, " + "       nod.idaoo, "
					+ "       (SELECT descrizione FROM nodo WHERE idnodo = nod.idnodopadre) AS descrizionenodopadre "
					+ "  FROM NODOUTENTERUOLO nur, UTENTE ute, RUOLO ruo, NODO nod " + " WHERE nur.IDRUOLO in (select IDRUOLO " + "                         from ruolo "
					+ "                        where SHOWRUOLO(IDRUOLO) like '%Responsabile Copia Conforme%' " + "                          and IDAOO = ? "
					+ "                          and DATADISATTIVAZIONE is null) " + "   AND nur.DATADISATTIVAZIONE is null " + "   AND nur.IDUTENTE = ute.IDUTENTE "
					+ "   AND nur.IDRUOLO = ruo.IDRUOLO " + "   AND nur.IDNODO = nod.IDNODO " + "   ORDER BY nod.descrizione ";
			ps = con.prepareStatement(query);
			ps.setLong(index++, idAOO);

			rs = ps.executeQuery();
			list = new ArrayList<>();
			while (rs.next()) {
				list.add(new ResponsabileCopiaConformeDTO(rs.getLong(IDNODO), rs.getString("DESCRIZIONE"), rs.getInt(PREDEFINITO), rs.getInt("IDAOO"), rs.getInt(IDRUOLO),
						rs.getString("NOMERUOLO"), rs.getLong(IDUTENTE_C), rs.getString("NOME"), rs.getString(COGNOME), rs.getDate(DATAATTIVAZIONE),
						rs.getDate(DATAATTIVAZIONE), rs.getInt("IDUTENTEDIRIGENTE"), rs.getString("DESCRIZIONENODOPADRE")));
			}
		} catch (final SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeStatement(ps, rs);
		}

		return list;
	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#aggiornaUtenteFirma(java.lang.Long,
	 *      java.lang.String, java.lang.String, java.sql.Connection).
	 */
	@Override
	public int aggiornaUtenteFirma(final Long idUtente, final String key, final String value, final Connection con) {
		PreparedStatement ps = null;
		final ResultSet rs = null;
		int updateUtente = 0;
		try {
			int index = 1;
			ps = con.prepareStatement("UPDATE utentefirma SET key = ? WHERE idutente = ?".replace("key", key));
			ps.setString(index++, value);
			ps.setLong(index++, idUtente);

			updateUtente = ps.executeUpdate();

		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento dell'utente firma (idUtente " + idUtente + " - key " + key + " + value " + value + ") al DB. " + e.getMessage(), e);
			throw new RedException("Errore durante l'aggiornamento dell'utente");
		} finally {
			closeStatement(ps, rs);
		}

		return updateUtente;
	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#getUtentiByDescNodoByDescPermesso(java.lang.String,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public final List<UtenteDTO> getUtentiByDescNodoByDescPermesso(final String descrizioneNodo, final String descrizionePermesso, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<UtenteDTO> utenti = new ArrayList<>();

		try {
			int index = 1;
			ps = con.prepareStatement("SELECT DISTINCT nur.idutente, nur.idnodo, u.nome, u.cognome FROM nodoutenteruolo nur, utente u WHERE idnodo ="
					+ " (SELECT idnodo FROM nodo WHERE descrizione = ?) AND idruolo IN"
					+ " (SELECT idruolo FROM RUOLO WHERE bitand(permessi, (SELECT idpermessoapp FROM permessoapp WHERE descrizione = ?)) > 0)"
					+ " AND u.idutente = nur.idutente");
			ps.setString(index++, descrizioneNodo);
			ps.setString(index++, descrizionePermesso);

			rs = ps.executeQuery();
			while (rs.next()) {
				final UtenteDTO utente = new UtenteDTO();
				utente.setId(rs.getLong(IDUTENTE_C));
				utente.setIdUfficio(rs.getLong(IDNODO));
				utente.setNome(rs.getString("NOME"));
				utente.setCognome(rs.getString(COGNOME));
				utente.setNodoDesc(descrizioneNodo);

				utenti.add(utente);
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero degli utenti dell'ufficio: " + descrizioneNodo + " con permesso: " + descrizionePermesso, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return utenti;
	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#getUtenteByUsernameByDescNodo(java.lang.String,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public final UtenteDTO getUtenteByUsernameByDescNodo(final String username, final String descNodo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		UtenteDTO utente = null;

		try {
			int index = 1;
			ps = con.prepareStatement("select distinct nur.idutente, nur.idnodo, u.nome, u.cognome from nodoutenteruolo nur, utente u"
					+ " where nur.idnodo = (select idnodo from nodo where descrizione =?)" + " and username = ?" + " AND u.idutente = nur.idutente");
			ps.setString(index++, descNodo);
			ps.setString(index++, username);

			rs = ps.executeQuery();
			if (rs.next()) {
				utente = new UtenteDTO();
				utente.setId(rs.getLong(IDUTENTE_C));
				utente.setIdUfficio(rs.getLong(IDNODO));
				utente.setNome(rs.getString("NOME"));
				utente.setCognome(rs.getString(COGNOME));
				utente.setNodoDesc(descNodo);
			} else {
				final RedException redEmpty = new RedException("getUtenteByUsernameByDescNodo deve ritornare almeno un valore");
				LOGGER.error(redEmpty);
				throw redEmpty;
			}

			if (rs.next()) { // gestiscve il caso in cui è impostato il farward only
				final RedException redNonUnico = new RedException("getUtenteByUsernameByDescNodo deve ritornare un unico valore");
				LOGGER.error(redNonUnico);
				throw redNonUnico;
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero dell'utente con descrizioneNodo: " + descNodo + " con username: " + username, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return utente;
	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#getDescUtenti(java.util.List,
	 *      java.sql.Connection).
	 */
	@Override
	public Map<Integer, String> getDescUtenti(final List<Integer> idsProtocollatori, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final Map<Integer, String> output = new HashMap<>();

		try {
			if (idsProtocollatori != null && !idsProtocollatori.isEmpty()) {
				final String strIN = it.ibm.red.business.utils.StringUtils.createInCondition("u.idutente", idsProtocollatori.iterator(), false);
				final String sql = "select u.idutente AS ID, u.nome || ' ' || u.cognome AS DESCR from utente u where " + strIN;
				ps = con.prepareStatement(sql);

				rs = ps.executeQuery();
				while (rs.next()) {
					output.put(rs.getInt("ID"), rs.getString("DESCR"));
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero degli utenti : " + idsProtocollatori, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#registerCloseSession(java.lang.String,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public void registerCloseSession(final String id, final String user, final Connection connection) {
		PreparedStatement ps = null;
		try {
			final Date now = new Date();

			final String sql = "UPDATE SESSION_LOG SET SES_USER = ?, SES_TIME_STOP = ?  WHERE SES_JID = ?";

			ps = connection.prepareStatement(sql);
			Integer n = 1;
			ps.setString(n++, user);
			ps.setTimestamp(n++, new Timestamp(now.getTime()));
			ps.setString(n++, id);
			ps.execute();
		} catch (final SQLException e) {
			final String msg = "Errore durante l'inserimento dell'audit delle sessioni (chiusura).";
			LOGGER.error(msg, e);
			throw new RedException(msg, e);
		} finally {
			closeStatement(ps, null);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#registerOpenSession(java.lang.String,
	 *      java.lang.String, java.lang.String, java.sql.Connection).
	 */
	@Override
	public void registerOpenSession(final String id, final String ip, final String user, final Connection connection) {
		PreparedStatement ps = null;
		try {
			final Date now = new Date();
			final String sql = "INSERT INTO SESSION_LOG(SES_ID, SES_JID, SES_IP, SES_TIME_START, SES_USER) VALUES (SEQ_SESSION_LOG.NEXTVAL, ?, ?, ?, ?)";
			ps = connection.prepareStatement(sql);
			Integer n = 1;
			ps.setString(n++, id);
			ps.setString(n++, ip);
			ps.setTimestamp(n++, new Timestamp(now.getTime()));
			ps.setString(n++, user);
			ps.execute();
		} catch (final SQLException e) {
			final String msg = "Errore durante l'inserimento dell'audit delle sessioni (apertura).";
			LOGGER.error(msg, e);
			throw new RedException(msg, e);
		} finally {
			closeStatement(ps, null);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#getSessioniApertePerNodo(java.sql.Connection).
	 */
	@Override
	public Map<String, Integer> getSessioniApertePerNodo(final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final Map<String, Integer> output = new HashMap<>();

		try {
			final String sql = "select SES_IP AS IP, COUNT(SES_ID) AS NUM from SESSION_LOG where SES_TIME_STOP is null and EXTRACT(HOUR FROM (sysdate-SES_TIME_START)) = 0 GROUP BY SES_IP";
			ps = con.prepareStatement(sql);

			rs = ps.executeQuery();
			while (rs.next()) {
				output.put(rs.getString("IP"), rs.getInt("NUM"));
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero delle sessioni aperte per nodo", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#getSessioniApertePerUtente(java.sql.Connection).
	 */
	@Override
	public Map<String, Integer> getSessioniApertePerUtente(final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final Map<String, Integer> output = new HashMap<>();

		try {
			final String sql = "select SES_USER AS USR, COUNT(SES_ID) AS NUM from SESSION_LOG where SES_TIME_STOP is null and EXTRACT(HOUR FROM (sysdate-SES_TIME_START)) = 0 GROUP BY SES_USER";
			ps = con.prepareStatement(sql);

			rs = ps.executeQuery();
			while (rs.next()) {
				output.put(rs.getString("USR"), rs.getInt("NUM"));
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero delle sessioni aperte per utente", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#getSessioniAperte(java.sql.Connection).
	 */
	@Override
	public Integer getSessioniAperte(final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer output = null;

		try {
			final String sql = "select count(SES_ID) AS NUM from SESSION_LOG where SES_TIME_STOP is null and EXTRACT(HOUR FROM (sysdate-SES_TIME_START)) = 0";
			ps = con.prepareStatement(sql);

			rs = ps.executeQuery();
			if (rs.next()) {
				output = rs.getInt("NUM");
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero delle sessioni aperte", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#getUtentiInNodo(java.sql.Connection,
	 *      java.lang.String).
	 */
	@Override
	public Collection<String> getUtentiInNodo(final Connection con, final String ip) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final Collection<String> output = new ArrayList<>();

		try {
			final String sql = "select SES_USER AS USR from SESSION_LOG where SES_TIME_STOP is null and EXTRACT(HOUR FROM (sysdate-SES_TIME_START)) = 0 and SES_IP = ?";
			ps = con.prepareStatement(sql);
			ps.setString(1, ip);

			rs = ps.executeQuery();
			while (rs.next()) {
				output.add(rs.getString("USR"));
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero degli utenti in nodo", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#getNodoUtenteRuolo(java.lang.Long,
	 *      java.lang.Long, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public NodoUtenteRuolo getNodoUtenteRuolo(final Long idUtente, final Long idNodo, final Long idRuolo, final Connection connection) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		NodoUtenteRuolo nur = null;
		try {
			final String querySQL = "SELECT DISTINCT ute.idutente, ute.nome, ute.cognome, ute.username, ute.dataattivazione, "
					+ " ute.datadisattivazione, ute.email, ute.codfis, ute.registroriservato, nod.idaoo, nod.idnodo, nod.descrizione, "
					+ " nur.dataattivazione as dataattivazionenur, nur.datadisattivazione as datadisattivazionenur ,nur.activeuser,nur.predefinito,nur.gestioneapplicativa"
					+ " FROM utente ute RIGHT OUTER JOIN nodoutenteruolo nur ON nur.idutente = ute.idutente RIGHT OUTER JOIN nodo nod ON nur.idnodo = nod.idnodo "
					+ " WHERE ute.idutente = ? AND nod.idnodo = ? AND nur.idruolo = ? " + " ORDER BY dataattivazionenur desc, datadisattivazionenur desc ";
			ps = connection.prepareStatement(querySQL);
			ps.setLong(1, idUtente);
			ps.setLong(2, idNodo);
			ps.setLong(3, idRuolo);
			rs = ps.executeQuery();
			if (rs.next()) {

				final BigDecimal activeUSer = rs.getBigDecimal("ACTIVEUSER");
				final Date dataAttivazione = rs.getDate(DATAATTIVAZIONE);
				final Date dataDisattivazione = rs.getDate(DATADISATTIVAZIONE);

				final Long predefinito = rs.getLong(PREDEFINITO);
				final Integer gestioneApplicativa = rs.getInt("GESTIONEAPPLICATIVA");

				// Recupero del nodo a partire dal suo id.
				final Nodo nodo = nodoAOO.getNodo(idNodo, connection);

				// Recupero del ruolo a partire dal suo id.
				final Ruolo ruolo = ruoloDAO.getRuolo(idRuolo, connection);

				// Recupero del utente a partire dal suo id.
				final Utente utente = getUtente(idUtente, connection);
				if (nodo != null && nodo.getDataDisattivazione() == null) {
					// Accesso alla cross nodo-utente-ruolo utilizzando le entità recuperate in
					// precedenza.
					nur = new NodoUtenteRuolo(activeUSer, dataAttivazione, dataDisattivazione, idNodo, idRuolo, idUtente, nodo, predefinito, ruolo, utente,
							gestioneApplicativa);
				}
			}
		} catch (final SQLException e) {
			throw new RedException(ERRORE_DURANTE_IL_RECUPERO_DELL_UTENTE_UFFICIO, e);
		} finally {
			closeStatement(ps, rs);
		}
		return nur;

	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#getPredefinitoById(java.lang.Long,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public TracciaDocUscitaDTO getPredefinitoById(final Long idAoo, final Long idUtente, final Connection conn) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		TracciaDocUscitaDTO ruoloPredefinito = null;
		try {
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * ");
			sb.append("FROM NODOUTENTERUOLO NUR JOIN NODO N ON NUR.IDNODO = N.IDNODO ");
			sb.append("WHERE NUR.IDUTENTE = " + idUtente + "AND NUR.PREDEFINITO = 1 AND N.IDAOO = " + idAoo);

			ps = conn.prepareStatement(sb.toString());
			rs = ps.executeQuery();
			if (rs.next()) {
				ruoloPredefinito = new TracciaDocUscitaDTO(rs.getLong(IDNODO), rs.getLong(IDUTENTE_C), rs.getLong(IDRUOLO), rs.getLong("IDAOO"));
			}
			return ruoloPredefinito;

		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero del predefinito", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#getIdByCodiceFiscale(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public Long getIdByCodiceFiscale(final String codiceFiscale, final Connection con) {
		Long idUtente = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = con.prepareStatement("SELECT u." + IDUTENTE_C + " FROM UTENTE u WHERE u." + CODFIS + " = ?");
			ps.setString(1, codiceFiscale);

			rs = ps.executeQuery();
			if (rs.next()) {
				idUtente = rs.getLong(IDUTENTE_C);
			}

		} catch (final SQLException e) {
			LOGGER.error("Errore nel recupero dell'ID utente a partire dal codice fiscale: " + codiceFiscale, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return idUtente;
	}

	/**
	 * @see it.ibm.red.business.dao.IUtenteDAO#getGlifoDelegato(java.lang.Long,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public UtenteFirma getGlifoDelegato(Long idUtente, Long idUtenteDelegante, Connection con) {
		UtenteFirma utenteFirma = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {			
			ps = con.prepareStatement("SELECT ufd.CONTENTFILE, ufd.DESCRIZIONE "
									+ "FROM UTENTEFIRMADELEGATA ufd "
									+ "WHERE ufd.IDDELEGATO = ? AND ufd.IDDELEGANTE = ?");
			ps.setString(1, idUtente.toString());
			ps.setString(2, idUtenteDelegante.toString());
			
			rs = ps.executeQuery();
			if (rs.next()) {
				byte[] contentByte = null;
				Blob sqlBlob = rs.getBlob(CONTENTFILE);
				if (sqlBlob != null) {
					contentByte = sqlBlob.getBytes(1, (int) sqlBlob.length());
					sqlBlob.free();
				}
				String reason = rs.getString("DESCRIZIONE");
				
				utenteFirma = new UtenteFirma(idUtente, contentByte, idUtenteDelegante, 
					null, null, null, null, reason, null, null, false);
			}
		} catch (SQLException e) {
			LOGGER.error("Errore nel recupero del glifo dell'utente delegato: " + idUtente, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return utenteFirma;
	}

}