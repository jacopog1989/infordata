package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * DTO che definisce un tipo visto.
 */
public class TipoVistoDTO implements Serializable {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 559111745702970385L;
	
	/**
	 * Tipo contabile.
	 */
	public static final int CONTABILE = 1;

	/**
	 * Tipo non contabile.
	 */
	public static final int NON_CONTABILE = 0;

	/**
	 * Tipo neutro.
	 */
	public static final int NEUTRO = 3;

	/**
	 * Tipo visto.
	 */
	private int idTipoVisto;
	
	/**
	 * Descrizione.
	 */
	private String descrizione;
	
	/**
	 * Tipologia.
	 */
	private int tipo;
	
	/**
	 * Restituisce l'id del tipo visto.
	 * @return id tipo visto
	 */
	public int getIdTipoVisto() {
		return idTipoVisto;
	}

	/**
	 * @param idTipoVisto idTipoVisto da impostare
	 */
	public void setIdTipoVisto(final int idTipoVisto) {
		this.idTipoVisto = idTipoVisto;
	}

	/**
	 * Restituisce la descrizione.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * @param descrizione descrizione da impostare
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}
	
	/**
	 * Restituisce il tipo.
	 * @return tipo
	 */
	public int getTipo() {
		return tipo;
	}

	/**
	 * @param tipo tipo da impostare
	 */
	public void setTipo(final int tipo) {
		this.tipo = tipo;
	}

	/**
	 * @see java.lang.Object#toString().
	 */
	@Override
	public String toString() {
		return "TipoVisto [idTipoVisto=" + idTipoVisto + ", descrizione=" + descrizione + ", tipo=" + tipo + "]";
	}
	
}
