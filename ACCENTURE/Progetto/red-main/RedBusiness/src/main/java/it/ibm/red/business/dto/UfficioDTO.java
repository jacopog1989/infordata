package it.ibm.red.business.dto;

/**
 * The Class UfficioDTO.
 *
 * @author CPIERASC
 * 
 * 	Data Transfer Object per un ufficio.
 */
public class UfficioDTO extends AbstractDTO {
	
	/**
	 * Serializzable.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Identificatore.
	 */
	private Long id;

	/**
	 * CountUfficio.
	 */
 
	/**
	 * Costruttore.
	 * 
	 * @param inId			identificativo
	 * @param inDescrizione	descrizione
	 */
	public UfficioDTO(final Long inId, final String inDescrizione) {
		super();
		this.descrizione = inDescrizione;
		this.id = inId;
	}
	

	/**
	 * Getter identificativo.
	 * 
	 * @return	identificativo
	 */
	public final Long getId() {
		return id;
	}


	/**
	 * Getter descrizione.
	 * 
	 * @return	descrizione
	 */
	public final String getDescrizione() {
		return descrizione;
	}

	/**
	 * .
	 * tags:
	 * @return
	 */
	@Override
	public final int hashCode() {
		int output = 1;
		if (id != null) {
			output = id.intValue();
		}
		return output;
	}

	/**
	 * Equals.
	 *
	 * @param obj the obj
	 * @return true, if successful
	 */
	@Override
	public final boolean equals(final Object obj) {
		Boolean output = false;
		if (obj instanceof UfficioDTO) {
			UfficioDTO other = (UfficioDTO) obj;
			output = id.equals(other.getId());
		}
		return output;
	}
}
