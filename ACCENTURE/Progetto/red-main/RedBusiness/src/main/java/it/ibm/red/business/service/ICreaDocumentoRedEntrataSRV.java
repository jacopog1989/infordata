package it.ibm.red.business.service;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.persistence.model.Aoo;

/**
 * Servizio per la creazione di documenti RED in entrata.
 * 
 * @author m.crescentini
 *
 */
public interface ICreaDocumentoRedEntrataSRV extends Serializable {
	
	/**
	 * @param guidMail
	 * @param oggetto
	 * @param mantieniAllegatiOriginali
	 * @param indiceClassificazione
	 * @param idContattoMittente
	 * @param notaMail
	 * @param utenteAssegnatario
	 * @param aoo
	 * @param con
	 * @return
	 */
	EsitoSalvaDocumentoDTO creaDocumentoRedEntrataDaMail(String guidMail, String oggetto, boolean mantieniAllegatiOriginali, 
			String indiceClassificazione, Long idContattoMittente, String notaMail, UtenteDTO utenteAssegnatario, Aoo aoo, Connection con);
	
	
	/**
	 * @param content
	 * @param contentType
	 * @param nomeFile
	 * @param oggetto
	 * @param utenteCreatore
	 * @param indiceClassificazione
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param idContattoMittente
	 * @param allegati
	 * @param idUtenteAssegnatario
	 * @param idUfficioAssegnatario
	 * @param numeroProtocollo
	 * @param annoProtocollo
	 * @param dataProtocollo
	 * @param codiceFlusso
	 * @param aoo
	 * @param con
	 * @return
	 */
	EsitoSalvaDocumentoDTO creaDocumentoRedEntrataDaContent(byte[] content, String contentType, String nomeFile, String oggetto, UtenteDTO utenteCreatore, 
			String indiceClassificazione, Integer idTipologiaDocumento, Integer idTipoProcedimento, Long idContattoMittente, 
			List<AllegatoDTO> allegati, Long idUtenteAssegnatario, Long idUfficioAssegnatario, Integer numeroProtocollo, Integer annoProtocollo, 
			Date dataProtocollo, String codiceFlusso, Aoo aoo, Connection con);

}
