package it.ibm.red.business.service.concrete;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.signing.SignHelper;
import it.ibm.red.business.persistence.model.PkHandler;
import it.ibm.red.business.service.IFirmaRapidaSRV;

/**
 * Servizi per eseguire le operazioni di firme sui documenti.
 * 
 * @author APerquoti
 * 
 */
@Service
public class FirmaRapidaSRV extends AbstractService implements IFirmaRapidaSRV {

	/**
	 * Messaggio errore URL Handler o Secure PIN.
	 */
	private static final String ERROR_HANDLER_O_SECUREPIN_NON_VALIDO_MSG = "URL Handler o SecurePin firma NON valido. Contattare l'assistenza.";
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1505102140229693498L;

	/**
	 * 
	 * tags.
	 * 
	 * @param fcDTO
	 * @param idDoc
	 * @param utente
	 * @param otp
	 * @return
	 */
	@Override
	public Map<String, byte[]>  cades(final FileDTO file, final UtenteDTO utente, final String otp) {
		Map<String, byte[]> contents = new HashMap<>();
		
		contents.put(file.getUuid(), file.getContent());
		
		final PkHandler pkHandlerFirma = utente.getSignerInfo().getPkHandlerFirma();
		if (pkHandlerFirma != null && pkHandlerFirma.getHandler() != null && pkHandlerFirma.getSecurePin() != null) {
			//START VI PK HANDLER
			contents = new SignHelper(pkHandlerFirma.getHandler(), pkHandlerFirma.getSecurePin(), utente.getDisableUseHostOnly())
																								.remoteSign(utente.getSignerInfo(), 
																											SignTypeEnum.CADES, 
																											otp, 
																											utente.getSignerInfo().getPin(), 
																											contents);
		} else {
			throw new RedException(ERROR_HANDLER_O_SECUREPIN_NON_VALIDO_MSG);
		}
		
		return contents;
	}

	/**
	 * 
	 * tags.
	 * 
	 * @param fileDTOs
	 * @param alias
	 * @param pin
	 * @param otp
	 * @return
	 */
	@Override
	public Map<String, byte[]> cades(final Collection<FileDTO> fileDTOs, final UtenteDTO utente, final String otp) {
		Map<String, byte[]> contents = new HashMap<>();
			
		for (final FileDTO file : fileDTOs) {
			contents.put(file.getUuid(), file.getContent());
		}
		
		final PkHandler pkHandlerFirma = utente.getSignerInfo().getPkHandlerFirma();
		if (pkHandlerFirma != null && pkHandlerFirma.getHandler() != null && pkHandlerFirma.getSecurePin() != null) {
			//START VI PK HANDLER
			contents = new SignHelper(pkHandlerFirma.getHandler(), pkHandlerFirma.getSecurePin(), utente.getDisableUseHostOnly())
																								.remoteSign(utente.getSignerInfo(), 
																											SignTypeEnum.CADES, 
																											otp, 
																											utente.getSignerInfo().getPin(), 
																											contents);
		} else {
			throw new RedException(ERROR_HANDLER_O_SECUREPIN_NON_VALIDO_MSG);
		}
		
		return contents;
	}

	/**
	 * 
	 * tags.
	 * 
	 * @param file
	 * @param utente
	 * @param otp
	 * @return
	 */
	@Override
	public Map<String, byte[]> pades(final FileDTO file, final UtenteDTO utente, final String otp, final SignTypeEnum signType) {
		Map<String, byte[]> contents = new HashMap<>();
		contents.put(file.getUuid(), file.getContent());
		
		final PkHandler pkHandlerFirma = utente.getSignerInfo().getPkHandlerFirma();
		if (pkHandlerFirma != null && pkHandlerFirma.getHandler() != null && pkHandlerFirma.getSecurePin() != null) {
			contents = new SignHelper(pkHandlerFirma.getHandler(), pkHandlerFirma.getSecurePin(), utente.getDisableUseHostOnly())
																								.remoteSign(utente.getSignerInfo(), 
																											signType, 
																											otp, 
																											utente.getSignerInfo().getPin(), 
																											contents);
		} else {
			throw new RedException(ERROR_HANDLER_O_SECUREPIN_NON_VALIDO_MSG);
		}
		
		return contents;
	}

	/**
	 * 
	 * tags.
	 * 
	 * @param fileDTOs
	 * @param utente
	 * @param otp
	 * @return
	 */
	@Override
	public Map<String, byte[]> pades(final Collection<FileDTO> fileDTOs, final UtenteDTO utente, final String otp, final SignTypeEnum signType) {
		Map<String, byte[]> contents = new HashMap<>();
		
		for (final FileDTO file : fileDTOs) {
			contents.put(file.getUuid(), file.getContent());
		}
		
		final PkHandler pkHandlerFirma = utente.getSignerInfo().getPkHandlerFirma();
		if (pkHandlerFirma != null && pkHandlerFirma.getHandler() != null && pkHandlerFirma.getSecurePin() != null) {
			//START VI PK HANDLER
			contents = new SignHelper(pkHandlerFirma.getHandler(), pkHandlerFirma.getSecurePin(), utente.getDisableUseHostOnly())
																								.remoteSign(utente.getSignerInfo(),
																											signType,
																											otp,
																											utente.getSignerInfo().getPin(), 
																											contents);
		} else {
			throw new RedException(ERROR_HANDLER_O_SECUREPIN_NON_VALIDO_MSG);
		}
		
		
		return contents;
	}
}	