package it.ibm.red.business.pcollector;

import java.util.Collection;

import it.ibm.red.business.dto.CollectedParametersDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Classe che permette il recupero dei metadati per il popolamento di uno
 * specifico template, ne permette inoltre un'inizializzazione da mostrare in
 * maschera.
 * 
 * @author SimoneLungarella
 */
public interface ParametersCollector {

	/**
	 * Metodo che consente il recupero del <code> CollectedParametersDTO </code> per
	 * la gestione dei placeholders per la costruzione del template e per il
	 * popolamento della lista di metadati da mostrare sul documento in uscita.
	 * 
	 * @param utente
	 *            utente in sessione
	 * @param listaMetadatiRegistro
	 *            lista dei metadati validi per uno specifico registro, questa lista
	 *            andrà modificata e memorizzata in
	 *            {@link it.ibm.red.business.dto.CollectedParametersDTO#getStringParams()}
	 * @param checkConfiguration
	 *            se <code> true </code> effettua una validazione sui metadati, se
	 *            <code> false </code> non effettua validazione
	 * @return collector per la gestione della registrazione ausiliaria
	 */
	CollectedParametersDTO collectParameters(UtenteDTO utente, Collection<MetadatoDTO> listaMetadatiRegistro, boolean checkConfiguration);

	/**
	 * Questo metodo consente di inizializzare tutti i metadati di un registro in
	 * base alla tipologia di documento per cui viene creata la registrazione.
	 * 
	 * @param utente
	 *            utente in sessione
	 * @param documentTitle
	 *            identificativo del documento su cui viene effettuata la
	 *            registrazione ausiliaria
	 * @param out
	 *            lista dei metadati che devono essere modificati dal metodo
	 */
	void initParameters(UtenteDTO utente, String documentTitle, Collection<MetadatoDTO> out);
	
	/**
	 * Configura i metadati senza impostare i valori di default dal documento in
	 * ingresso.
	 * 
	 * @param utente
	 *            utente in sessione
	 * @param documentTitle
	 *            identificativo del documento su cui viene creata la registrazione
	 *            ausiliaria
	 * @param out
	 *            metadati da configurare
	 */
	void configureMetadati(UtenteDTO utente, String documentTitle, Collection<MetadatoDTO> out);
	
	/**
	 * Metodo che consente di memorizzare le caratteristiche e il valore dei
	 * metadati del documento in uscita in fase di predisposizione della
	 * registrazione.
	 * 
	 * @param metadatiToPrepare
	 *            i metadati del documento in uscita
	 * @param listaMetadatiRegistro
	 *            i metadati utilizzati per il popolamento del template, forniscono
	 *            le caratteristiche che occorrono nel documento in uscita
	 * @param nomeRegisto
	 *            il nome del registro ausiliario associato alla registrazione
	 *            ausiliaria
	 * @return collezione dei metadati preparati per il documento in uscita
	 */
	Collection<MetadatoDTO> prepareMetadatiForDocUscita(Collection<MetadatoDTO> metadatiToPrepare, Collection<MetadatoDTO> listaMetadatiRegistro, String nomeRegisto);
}
