package it.ibm.red.business.service.pstrategy;

import java.util.EnumMap;

import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.MessaggioPostaNpsDTO;
import it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * Factory della strategy di protcollazione automatica.
 */
public final class AutoProtocolStrategyFactory {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AutoProtocolStrategyFactory.class.getName());
	
	/**
	 * Strategie.
	 */
	static EnumMap<TipoContestoProceduraleEnum, Class<? extends IAutoProtocolStrategy<? extends MessaggioPostaNpsDTO>>> strategies;
	
	static {
		strategies = new EnumMap<>(TipoContestoProceduraleEnum.class);
		strategies.put(TipoContestoProceduraleEnum.BASIC, BASICAutoProtocolStrategy.class);
		strategies.put(TipoContestoProceduraleEnum.ORD, ORDAutoProtocolStrategy.class);
		strategies.put(TipoContestoProceduraleEnum.SILICE, SILICEAutoProtocolStrategy.class);
		strategies.put(TipoContestoProceduraleEnum.FLUSSO_AUT, AUTAutoProtocolStrategy.class);
		strategies.put(TipoContestoProceduraleEnum.FLUSSO_CG2, CG2AutoProtocolStrategy.class);
		strategies.put(TipoContestoProceduraleEnum.SICOGE, SICOGEAutoProtocolStrategy.class);
	}

	/**
	 * Costruttore vuoto.
	 */
	private AutoProtocolStrategyFactory() {
		// Costruttore vuoto.
	}

	private static IAutoProtocolStrategy<? extends MessaggioPostaNpsDTO> create(final TipoContestoProceduraleEnum tcp) {
		IAutoProtocolStrategy<? extends MessaggioPostaNpsDTO> out = null;
		if (tcp != null) {
			try {
				final Class<? extends IAutoProtocolStrategy<? extends MessaggioPostaNpsDTO>> cls = strategies.get(tcp);
				out = cls.newInstance();
			} catch (final Exception e) {
				LOGGER.error("Impossibile recuperare la strategia di protocollazione automatica.", e);
				throw new RedException(e);
			}
		}
		return out;
	}

	/**
	 * @param tcp
	 * @param richiestaElabMessaggio
	 * @param guidFilenetMessaggio
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static EsitoSalvaDocumentoDTO protocol(final TipoContestoProceduraleEnum tcp, final RichiestaElabMessaggioPostaNpsDTO richiestaElabMessaggio,
			final String guidFilenetMessaggio) {
		final IAutoProtocolStrategy<? extends MessaggioPostaNpsDTO> strategy = AutoProtocolStrategyFactory.create(tcp);
		
		if (strategy != null) {
			return strategy.protocol(richiestaElabMessaggio, guidFilenetMessaggio);
		} else {
			return null;
		}
	}

	/**
	 * @param idTcp
	 * @param richiestaElabMessaggio
	 * @param guidFilenetMessaggio
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static EsitoSalvaDocumentoDTO protocol(final String idTcp, final RichiestaElabMessaggioPostaNpsDTO richiestaElabMessaggio, final String guidFilenetMessaggio) {
		final IAutoProtocolStrategy<? extends MessaggioPostaNpsDTO> strategy = AutoProtocolStrategyFactory.create(TipoContestoProceduraleEnum.getEnumById(idTcp));
		
		if (strategy != null) {
			return strategy.protocol(richiestaElabMessaggio, guidFilenetMessaggio);
		} else {
			return null;
		}
	}
	
}
