package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IFormatoAllegatoDAO;
import it.ibm.red.business.dto.FormatoAllegatoDTO;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * @author SLac
 *
 */
@Repository
public class FormatoAllegatoDAO extends AbstractDAO implements IFormatoAllegatoDAO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -44526123408232460L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FormatoAllegatoDAO.class);

	/**
	 * @see it.ibm.red.business.dao.IFormatoAllegatoDAO#getAll(java.sql.Connection).
	 */
	@Override
	public List<FormatoAllegatoDTO> getAll(final Connection connection) {
		List<FormatoAllegatoDTO> list = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = connection.prepareStatement("SELECT * FROM formatoallegato ORDER BY idformatoallegato");
			rs = ps.executeQuery();
			
			list = new ArrayList<>();
			
			while (rs.next()) {
				FormatoAllegatoEnum formato = FormatoAllegatoEnum.getById(rs.getInt("IDFORMATOALLEGATO"));
				list.add(new FormatoAllegatoDTO(formato, rs.getString("DESCRIZIONE")));
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return list;
	}

}
