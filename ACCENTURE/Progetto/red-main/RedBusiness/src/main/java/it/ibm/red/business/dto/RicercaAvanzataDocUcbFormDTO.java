/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.List;

/**
 * @author APerquoti.
 *
 */
public class RicercaAvanzataDocUcbFormDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -8010021336470705225L;
	
	/**
	 * Lista tipologie documento.
	 */
	private List<TipologiaDocumentoDTO> comboTipologieDocumento;
	
	/**
	 * Lista tipologie procedimento.
	 */
	private List<TipoProcedimentoDTO> comboTipiProcedimento;

	/**
	 * Lista metadati estesi.
	 */
	private List<MetadatoDTO> listaMetadatiEstesi;

	/**
	 * @return the comboTipologieDocumento
	 */
	public List<TipologiaDocumentoDTO> getComboTipologieDocumento() {
		return comboTipologieDocumento;
	}

	/**
	 * @param comboTipologieDocumento the comboTipologieDocumento to set
	 */
	public void setComboTipologieDocumento(final List<TipologiaDocumentoDTO> comboTipologieDocumento) {
		this.comboTipologieDocumento = comboTipologieDocumento;
	}

	/**
	 * @return the listaMetadatiEstesi
	 */
	public List<MetadatoDTO> getListaMetadatiEstesi() {
		return listaMetadatiEstesi;
	}

	/**
	 * @param listaMetadatiEstesi the listaMetadatiEstesi to set
	 */
	public void setListaMetadatiEstesi(final List<MetadatoDTO> listaMetadatiEstesi) {
		this.listaMetadatiEstesi = listaMetadatiEstesi;
	}

	/**
	 * @return the comboTipiProcedimento
	 */
	public List<TipoProcedimentoDTO> getComboTipiProcedimento() {
		return comboTipiProcedimento;
	}

	/**
	 * @param comboTipiProcedimento the comboTipiProcedimento to set
	 */
	public void setComboTipiProcedimento(final List<TipoProcedimentoDTO> comboTipiProcedimento) {
		this.comboTipiProcedimento = comboTipiProcedimento;
	}
	
}
