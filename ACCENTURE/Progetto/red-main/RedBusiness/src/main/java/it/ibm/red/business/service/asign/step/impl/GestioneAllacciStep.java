package it.ibm.red.business.service.asign.step.impl;

import java.sql.Connection;
import java.util.Map;

import org.springframework.stereotype.Service;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignStepResultDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.asign.step.ASignStepAbstract;
import it.ibm.red.business.service.asign.step.ContextASignEnum;
import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * Step che si occupa della gestione degli allacci - firma asincrona.
 */
@Service
public class GestioneAllacciStep extends ASignStepAbstract {

	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = -7553486970039206304L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(GestioneAllacciStep.class.getName());

	/**
	 * Esegue la gestione degli allacci per un item di firma asincrona.
	 * @param in
	 * @param item
	 * @param context
	 * @return risultato della gestione
	 */
	@Override
	protected ASignStepResultDTO run(Connection con, ASignItemDTO item, Map<ContextASignEnum, Object> context) {
		Long idItem = item.getId();
		LOGGER.info("run -> START [ID item: " + idItem + "]");
		ASignStepResultDTO out = new ASignStepResultDTO(item.getId(), getStep());
		
		// Si recupera l'utente firmatario
		UtenteDTO utenteFirmatario = getUtenteFirmatario(item, con);
		
		// Si recupera il workflow
		VWWorkObject wob = getWorkflow(item, out, utenteFirmatario);
		
		if (wob != null) {
			// ### GESTIONE DEGLI ALLACCI ###
			EsitoOperazioneDTO esitoGestioneAllacci = signSRV.gestioneAllacci(wob, utenteFirmatario, con);
			
			if (esitoGestioneAllacci.isEsito()) {
				out.setStatus(true);
				out.appendMessage("[OK] Gestione degli allacci eseguita");
			} else {
				throw new RedException("[KO] Errore nello step di gestione degli allacci: " + esitoGestioneAllacci.getNote() 
					+ ". [Codice errore: " + esitoGestioneAllacci.getCodiceErrore() + "]");
			}
		}
		
		LOGGER.info("run -> END [ID item: " + idItem + "]");
		return out;
	}
	
	/**
	 * Restituisce lo @see StepEnum a cui è associata questa classe.
	 * 
	 * @return StepEnum
	 */
	@Override
	protected StepEnum getStep() {
		return StepEnum.GESTIONE_ALLACCI;
	}

	/**
	 * @see it.ibm.red.business.service.asign.step.ASignStepAbstract#getErrorMessage().
	 */
	@Override
	protected String getErrorMessage() {
		return "[KO] Errore imprevisto nello step di aggiornamento dei metadati";
	}
}
