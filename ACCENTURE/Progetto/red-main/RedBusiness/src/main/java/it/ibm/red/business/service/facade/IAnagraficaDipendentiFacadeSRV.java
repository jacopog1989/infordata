package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.AnagraficaDipendenteDTO;
import it.ibm.red.business.dto.IndirizzoDTO;
import it.ibm.red.business.dto.RicercaAnagDipendentiDTO;
import it.ibm.red.business.dto.SelectItemDTO;

/**
 * Facade del servizio di gestione anagrafiche dipendenti.
 */
public interface IAnagraficaDipendentiFacadeSRV extends Serializable {

	/**
	 * Salva l'anagrafica del dipendente.
	 * 
	 * @param idAoo
	 *            identificativo dell'Aoo
	 * @param newAnagrafica
	 *            nuova anagrafica da memorizzare
	 * @return anagrafica memorizzata
	 */
	AnagraficaDipendenteDTO salva(long idAoo, AnagraficaDipendenteDTO newAnagrafica);

	/**
	 * Ricerca le anagrafiche dei dipendenti restituendone quelle trovate.
	 * 
	 * @param idAoo
	 *            identificativo dell'Aoo
	 * @param ricercaDTO
	 *            anagrafica dei dipendenti da ricercare
	 * @return anagrafiche recuperate
	 */
	Collection<AnagraficaDipendenteDTO> ricerca(long idAoo, RicercaAnagDipendentiDTO ricercaDTO);

	/**
	 * Restituisce le informazioni sui DUG.
	 * 
	 * @return collezione dei dug
	 */
	Collection<SelectItemDTO> getDug();

	/**
	 * Salva l'indirizzo del dipendente.
	 * 
	 * @param idAnagraficaDipendente
	 *            id dell'anagrafica del dipendente
	 * @param newIndirizzo
	 *            nuovo indirizzo
	 * @return indirizzo
	 */
	IndirizzoDTO salvaIndirizzo(Integer idAnagraficaDipendente, IndirizzoDTO newIndirizzo);

	/**
	 * Ottiene gli indirizzi.
	 * 
	 * @param idAnagrafica
	 *            id dell'anagrafica
	 * @return indirizzi
	 */
	Collection<IndirizzoDTO> getIndirizzi(Integer idAnagrafica);

	/**
	 * Elimina l'indirizzo.
	 * 
	 * @param idIndirizzo
	 *            id dell'indirizzo
	 */
	void removeIndirizzo(Integer idIndirizzo);

	/**
	 * Cancella l'item della tabella anagrafica_dipendenti identificata dall'
	 * <code> id </code>.
	 * 
	 * @param id
	 *            identificativo dell'anagrafica da rimuovere
	 */
	void remove(Integer id);

	/**
	 * Aggiorna la tabella anagrafica_dipendenti.
	 * 
	 * @param newAnagrafica
	 *            nuova anagrafica
	 */
	void modifica(AnagraficaDipendenteDTO newAnagrafica);

	/**
	 * Modifica l'indirizzo del dipendente.
	 * 
	 * @param id
	 *            identificativo dell'anagrafica per la quale modificare l'indirizzo
	 * @param newIndirizzo
	 *            - nuovo indirizzo
	 */
	void modificaIndirizzo(Integer id, IndirizzoDTO newIndirizzo);

}
