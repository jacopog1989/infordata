package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;

import it.ibm.red.business.persistence.model.Ente;

/**
 * 
 * @author CPIERASC
 *
 *	Dao gestione ente.
 */
public interface IEnteDAO extends Serializable {
	
	/**
	 * Recupero ente da id.
	 * 
	 * @param idEnte		identificativo ente
	 * @param connection	connessione
	 * @return				ente
	 */
	Ente getEnte(Integer idEnte, Connection connection);

}
