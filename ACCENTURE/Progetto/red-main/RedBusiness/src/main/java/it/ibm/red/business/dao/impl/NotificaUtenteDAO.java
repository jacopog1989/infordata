package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.INotificaUtenteDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.StringUtils;

/**
 * @author Vingenito
 */
@Repository
public class NotificaUtenteDAO extends AbstractDAO implements INotificaUtenteDAO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -8278774897107293586L;

	/**
	 * Messaggio errore contrassegno notifica in scadenza.
	 */
	private static final String ERROR_CONTRASSEGNO_NOTIFICA_MSG = "Errore durante il contrassegno della notifica in scadenza.";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotificaUtenteDAO.class);

	/**
	 * @see it.ibm.red.business.dao.INotificaUtenteDAO#contrassegnaOEliminaNotificaCalendario(java.lang.Long,
	 *      java.lang.Long, java.lang.Integer, java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean contrassegnaOEliminaNotificaCalendario(final Long idUtente, final Long idAoo, final Integer idEvento, final Integer statoNotifica, final Connection conn) {
		int executionResult = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;	
		boolean trovato = false;
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM STATO_NOTIFICA_CALENDARIO WHERE IDUTENTE = ? AND IDAOO = ? AND IDEVENTO = ? ");
			ps = conn.prepareStatement(sb.toString());
			ps.setLong(index++, idUtente);
			ps.setLong(index++, idAoo);
			ps.setInt(index++, idEvento);
			rs = ps.executeQuery();

			if (rs.next()) {
				trovato = true;
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il contrassegno della notifica.", e);
		} finally {
			closeStatement(ps, rs);
		}

		if (!trovato) {
			executionResult = insertNotificaCalendario(idUtente, idAoo, idEvento, statoNotifica, conn);
		} else {
			executionResult = updateNotificaCalendario(idUtente, idAoo, idEvento, statoNotifica, conn);
		}

		return executionResult > 0;
	}

	private static int insertNotificaCalendario(final Long idUtente, final Long idAoo, final Integer idEvento, final Integer stato, final Connection conn) {
		PreparedStatement ps = null;
		try {
			int index = 1;
			final String insertQuery = "INSERT INTO STATO_NOTIFICA_CALENDARIO (IDUTENTE,IDEVENTO,STATO,IDAOO) VALUES(?,?,?,?)";
			ps = conn.prepareStatement(insertQuery);
			ps.setLong(index++, idUtente);
			ps.setLong(index++, idEvento);
			ps.setInt(index++, stato);
			ps.setLong(index++, idAoo);
			return ps.executeUpdate();
		} catch (final Exception ex) {
			LOGGER.error("Errore durante l'insert della notifica calendario in tabella : " + ex);
			throw new RedException("Errore durante l'insert della notifica calendario in tabella : " + ex);
		} finally {
			closeStatement(ps);
		}
	}

	private static int updateNotificaCalendario(final Long idUtente, final Long idAoo, final Integer idEvento, final Integer stato, final Connection conn) {
		PreparedStatement ps = null;
		try {
			int index = 1;
			final String updateQuery = "UPDATE STATO_NOTIFICA_CALENDARIO SET STATO = ? WHERE IDUTENTE = ? AND IDEVENTO = ? AND IDAOO = ?";
			ps = conn.prepareStatement(updateQuery);
			ps.setInt(index++, stato);
			ps.setLong(index++, idUtente);
			ps.setLong(index++, idEvento);
			ps.setLong(index++, idAoo);

			return ps.executeUpdate();
		} catch (final Exception ex) {
			LOGGER.error("Errore durante l'update della notifica calendario in tabella : " + ex);
			throw new RedException("Errore durante l'update della notifica calendario in tabella : " + ex);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaUtenteDAO#checkNotificaCalendarioScadenza(java.lang.Long,
	 *      java.util.List, java.sql.Connection).
	 */
	@Override
	public HashMap<String, Integer> checkNotificaCalendarioScadenza(final Long idUtente, final List<String> docTitleList, final Connection conn) {
		final HashMap<String, Integer> statoNotificaPerDocTitle = new HashMap<>();
		PreparedStatement ps = null;
		ResultSet rs = null;	 
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM STATO_NOTIFICA_DOC_SCADENZA WHERE IDUTENTE = ? AND " + StringUtils.createInCondition("DOCTITLE", docTitleList.iterator(), true));
			ps = conn.prepareStatement(sb.toString());
			ps.setLong(index++, idUtente); 
			rs = ps.executeQuery();

			while (rs.next()) { 
				statoNotificaPerDocTitle.put(rs.getString("DOCTITLE"), rs.getInt("STATO"));
			}

		} catch (final SQLException e) {
			LOGGER.error("Errore durante il contrassegno della notifica.", e);
		} finally {
			closeStatement(ps, rs);
		}

		return statoNotificaPerDocTitle;
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaUtenteDAO#contrassegnaOEliminaNotificaCalendarioScadenza(java.lang.Long,
	 *      java.lang.Long, java.lang.String, java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean contrassegnaOEliminaNotificaCalendarioScadenza(final Long idUtente, final Long idAoo, final String docTitle, final Integer statoNotifica, final Connection conn) {
		int executionResult = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;	
		boolean trovato = false;
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM STATO_NOTIFICA_DOC_SCADENZA WHERE IDUTENTE = ? AND DOCTITLE= ? AND IDAOO = ?");
			ps = conn.prepareStatement(sb.toString());
			ps.setLong(index++, idUtente);
			ps.setString(index++, docTitle);
			ps.setLong(index++, idAoo);
			rs = ps.executeQuery();

			if (rs.next()) {
				trovato = true;
			}
		} catch (final SQLException e) {
			LOGGER.error(ERROR_CONTRASSEGNO_NOTIFICA_MSG, e);
			throw new RedException(ERROR_CONTRASSEGNO_NOTIFICA_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}	

		if (!trovato) {
			executionResult = insertNotificaCalendarioScadenza(idUtente, idAoo, docTitle, statoNotifica, conn);
		} else {
			executionResult = updateNotificaCalendarioScadenza(idUtente, idAoo, docTitle, statoNotifica, conn);
		} 

		return executionResult > 0;
	}

	private static int insertNotificaCalendarioScadenza(final Long idUtente, final Long idAoo, final String docTitle, final Integer stato, final Connection conn) {
		PreparedStatement ps = null;
		try {
			int index = 1;
			final String insertQuery = "INSERT INTO STATO_NOTIFICA_DOC_SCADENZA (DOCTITLE,IDUTENTE,STATO,IDAOO) VALUES(?,?,?,?)";
			ps = conn.prepareStatement(insertQuery);
			ps.setString(index++, docTitle);
			ps.setLong(index++, idUtente);
			ps.setInt(index++, stato);
			ps.setLong(index++, idAoo);
			return ps.executeUpdate();
		} catch (final Exception ex) {
			LOGGER.error("Errore durante l'insert della notifica in scadenza calendario in tabella : " + ex);
			throw new RedException("Errore durante l'insert della notifica in scadenza calendario in tabella : " + ex);
		} finally {
			closeStatement(ps);
		}
	}

	private static int updateNotificaCalendarioScadenza(final Long idUtente, final Long idAoo, final String docTitle, final Integer stato, final Connection conn) {
		PreparedStatement ps = null;
		try {
			int index = 1;
			final String updateQuery = "UPDATE STATO_NOTIFICA_DOC_SCADENZA SET STATO = ? WHERE IDUTENTE = ? AND DOCTITLE = ? AND IDAOO = ?";
			ps = conn.prepareStatement(updateQuery);
			ps.setInt(index++, stato);
			ps.setLong(index++, idUtente);
			ps.setString(index++, docTitle);
			ps.setLong(index++, idAoo);

			return ps.executeUpdate();
		} catch (final Exception ex) {
			LOGGER.error("Errore durante l'update della notifica in scadenza calendario in tabella : " + ex);
			throw new RedException("Errore durante l'update della notifica in scadenza calendario in tabella : " + ex);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaUtenteDAO#contrassegnaOEliminaNotificaRubrica(java.lang.Long,
	 *      java.lang.Long, java.lang.Integer, java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean contrassegnaOEliminaNotificaRubrica(final Long idUtente, final Long idAoo, final Integer idNotificaRubrica, final Integer statoNotifica, final Connection conn) {
		int executionResult = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;	
		boolean trovato = false;
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM STATO_NOTIFICA_RUBRICA WHERE IDUTENTE = ? AND IDNOTIFICARUBRICA = ? AND IDAOO = ?");
			ps = conn.prepareStatement(sb.toString());
			ps.setInt(index++, idUtente.intValue());
			ps.setInt(index++, idNotificaRubrica);
			ps.setInt(index++, idAoo.intValue());

			rs = ps.executeQuery();

			if (rs.next()) {
				trovato = true;
			}

		} catch (final SQLException e) {
			LOGGER.error(ERROR_CONTRASSEGNO_NOTIFICA_MSG, e);
			throw new RedException(ERROR_CONTRASSEGNO_NOTIFICA_MSG, e);
		} finally {
			closeStatement(ps, rs); 
		}

		if (!trovato) {
			executionResult = insertNotificaRubrica(idUtente, idAoo, idNotificaRubrica, statoNotifica, conn);
		} else {
			executionResult = updateNotificaRubrica(idUtente, idAoo, idNotificaRubrica, statoNotifica, conn);
		}

		return executionResult > 0;
	}

	private static int insertNotificaRubrica(final Long idUtente, final Long idAoo, final Integer idNotificaRubrica, final Integer stato, final Connection conn) {
		PreparedStatement ps = null;
		try {
			int index = 1;
			final String insertQuery = "INSERT INTO STATO_NOTIFICA_RUBRICA (IDNOTIFICARUBRICA,IDUTENTE,STATO_NOTIFICA,IDAOO) VALUES(?,?,?,?)";
			ps = conn.prepareStatement(insertQuery);
			ps.setInt(index++, idNotificaRubrica);
			ps.setLong(index++, idUtente);
			ps.setInt(index++, stato);
			ps.setLong(index++, idAoo);
			return ps.executeUpdate();
		} catch (final Exception ex) {
			LOGGER.error("Errore durante l'insert della notifica rubrica in tabella : " + ex);
			throw new RedException("Errore durante l'insert della notifica rubrica in tabella : " + ex);
		} finally {
			closeStatement(ps);
		}
	}

	private static int updateNotificaRubrica(final Long idUtente, final Long idAoo, final Integer idNotificaRubrica, final Integer stato, final Connection conn) {
		PreparedStatement ps = null;
		try {
			int index = 1;
			final String updateQuery = "UPDATE STATO_NOTIFICA_RUBRICA SET STATO_NOTIFICA = ? WHERE IDUTENTE = ? AND IDAOO = ? AND IDNOTIFICARUBRICA = ?";
			ps = conn.prepareStatement(updateQuery);
			ps.setInt(index++, stato);
			ps.setInt(index++, idUtente.intValue());
			ps.setInt(index++, idAoo.intValue());
			ps.setInt(index, idNotificaRubrica);

			return ps.executeUpdate();
		} catch (final Exception ex) {
			LOGGER.error("Errore durante l'update della notifica rubrica in tabella : " + ex);
			throw new RedException("Errore durante l'update della notifica rubrica  in tabella : " + ex);
		} finally {
			closeStatement(ps);
		}
	}

}
