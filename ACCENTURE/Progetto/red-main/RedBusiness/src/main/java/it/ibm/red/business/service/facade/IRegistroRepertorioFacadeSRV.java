package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.RegistroRepertorioDTO;

/**
 * 
 * @author APerquoti
 *
 */
public interface IRegistroRepertorioFacadeSRV extends Serializable {

	/**
	 * Metodo per verificare che un data coppia TipologiaDocumento/TipoProcedimento faccia parte dei Registri Repertorio configurati.
	 * 
	 * @param idAoo
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param registriRepertorioConfigured
	 * @return
	 */
	Boolean checkIsRegistroRepertorio(Long idAoo, Long idTipologiaDocumento, Long idTipoProcedimento, 
			Map<Long, List<RegistroRepertorioDTO>> registriRepertorioConfigured);
	
	/**
	 * Metodo per recuperare le descr dei registri per la stampa etichette.
	 * 
	 * @param idAoo
	 * @return
	 */
	List<String> getDescrForShowEtichetteByAoo(Long idAoo);
}
