/**
 * 
 */
package it.ibm.red.business.dao.impl;

import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IAttoDecretoMonitoringDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.monitoring.attodecreto.AttoDecretoMonitoring;

/**
 * @author a.dilegge
 *
 */
@Repository
public class AttoDecretoMonitoringDAO extends AbstractDAO implements IAttoDecretoMonitoringDAO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Messaggio di errore aggiornamento dell'operazione del documento, occorre appendere l'id documento al messaggio.
	 */
	private static final String ERROR_AGGIORNAMENTO_DOCUMENTO_MSG = "Errore durante l'aggiornamento dell'operazione per il documento ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AttoDecretoMonitoringDAO.class);

	/**
	 * Next sequence value.
	 */
	private static final String SQL_NEXTID = "SELECT seq_attodecretomonitoring.NEXTVAL FROM DUAL";
	
	/**
	 * SQL insert.
	 */
	private static final String SQL_INSERT = "INSERT INTO attodecretomonitoring (idmonitoring, iddocumento, numeroprotocollo, annoprotocollo, idaoo, " 
			+ "idnodo, idutente, operazione, idraccolta, nomefile, esito, inizio_operazione ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0, sysdate)";
	
	/**
	 * SQL update.
	 */
	private static final String SQL_UPDATE = "UPDATE attodecretomonitoring SET idraccolta = ?, esito = ?, descr_esito = ?, request = ?, response = ?, " 
			+ "stacktrace = ?, fine_operazione = sysdate WHERE idmonitoring = ?";

	/**
	 * @see it.ibm.red.business.dao.IAttoDecretoMonitoringDAO#getNextId(java.sql.Connection).
	 */
	@Override
	public int getNextId(final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int nextId = 0;
		try {
			ps = con.prepareStatement(SQL_NEXTID);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				nextId = rs.getInt(1);
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero progressivo. " + e.getMessage());
			throw new RedException("Errore durante il recupero progressivo. ", e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
		return nextId;
	}

	/**
	 * @see it.ibm.red.business.dao.IAttoDecretoMonitoringDAO#inserisciOperation(it.ibm.red.business.monitoring.attodecreto.AttoDecretoMonitoring,
	 *      java.sql.Connection).
	 */
	@Override
	public void inserisciOperation(final AttoDecretoMonitoring monitoringInfo, final Connection con) {
		
		LOGGER.info("Execute query: " + SQL_INSERT);
		int index = 1;
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(SQL_INSERT);
			ps.setInt(index++, monitoringInfo.getIdMonitoring());
			ps.setInt(index++, monitoringInfo.getIdDocumento());
			ps.setInt(index++, monitoringInfo.getNumeroProtocollo()); 
			ps.setInt(index++, monitoringInfo.getAnnoProtocollo()); 
			ps.setInt(index++, monitoringInfo.getIdAoo()); 
			ps.setInt(index++, monitoringInfo.getIdUfficio());
			ps.setInt(index++, monitoringInfo.getIdUtente());
			ps.setString(index++, monitoringInfo.getOperazione());
			if (monitoringInfo.getIdRaccoltaProvvisoria() != null) {
				ps.setString(index++, monitoringInfo.getIdRaccoltaProvvisoria());
			} else {
				ps.setNull(index++, Types.CHAR);
			}
			if (monitoringInfo.getNomeFile() != null) {
				ps.setString(index++, monitoringInfo.getNomeFile());
			} else {
				ps.setNull(index++, Types.CHAR);
			}
			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la registrazione dell'operazione per il documento " + monitoringInfo.getIdDocumento(), e);
			throw new RedException("Errore durante la registrazione dell'operazione per il documento " + monitoringInfo.getIdDocumento(), e);
		} finally {
			closeStatement(ps);
		}
		
	}

	/**
	 * @see it.ibm.red.business.dao.IAttoDecretoMonitoringDAO#aggiornaOperation(it.ibm.red.business.monitoring.attodecreto.AttoDecretoMonitoring,
	 *      java.sql.Connection).
	 */
	@Override
	public void aggiornaOperation(final AttoDecretoMonitoring monitoringInfo, final Connection connection)  {
		
		LOGGER.info("Execute query: " + SQL_UPDATE);
		int index = 1;
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(SQL_UPDATE);
			
			ps.setString(index++, monitoringInfo.getIdRaccoltaProvvisoria());
			ps.setInt(index++, monitoringInfo.getEsito());
			if (monitoringInfo.getDescrizioneEsito() != null) {
				ps.setString(index++, monitoringInfo.getDescrizioneEsito());
			} else {
				ps.setNull(index++, Types.CHAR);
			}
			if (monitoringInfo.getRequest() != null) {
				final Blob blob = connection.createBlob();
				blob.setBytes(1, monitoringInfo.getRequest().getBytes(StandardCharsets.UTF_8));
				ps.setBlob(index++, blob);
			} else {
				ps.setNull(index++, Types.BLOB);
			}
			if (monitoringInfo.getResponse() != null) {
				final Blob blob = connection.createBlob();
				blob.setBytes(1, monitoringInfo.getResponse().getBytes(StandardCharsets.UTF_8));
				ps.setBlob(index++, blob);
			} else {
				ps.setNull(index++, Types.BLOB);
			}
			if (monitoringInfo.getStackTrace() != null) {
				final Blob blob = connection.createBlob();
				blob.setBytes(1, monitoringInfo.getStackTrace().getBytes(StandardCharsets.UTF_8));
				ps.setBlob(index++, blob);
			} else {
				ps.setNull(index++, Types.BLOB);
			}
			ps.setInt(index++, monitoringInfo.getIdMonitoring());
			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error(ERROR_AGGIORNAMENTO_DOCUMENTO_MSG + monitoringInfo.getIdDocumento(), e);
			throw new RedException(ERROR_AGGIORNAMENTO_DOCUMENTO_MSG + monitoringInfo.getIdDocumento(), e);
		} finally {
			closeStatement(ps);
		}
		
	}

}