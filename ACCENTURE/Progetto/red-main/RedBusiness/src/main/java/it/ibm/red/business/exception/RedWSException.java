package it.ibm.red.business.exception;

/**
 * Exception generica Red Evo.
 */
public class RedWSException extends RedException {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Codice.
	 */
	private final int code;
	
	/**
	 * Costruttore.
	 * @param message
	 * @param code
	 */
	public RedWSException(final String message, final int code) {
		super(message);
		this.code = code;
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param e	eccezione
	 */
	public RedWSException(final Exception e) {
		super(e);
		this.code = -1;
	}

	/**
	 * Costruttore.
	 * 
	 * @param msg	messaggio
	 */
	public RedWSException(final String msg) {
		super(msg);
		this.code = -1;
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param msg	messaggio
	 * @param e		eccezione root
	 */
	public RedWSException(final String msg, final Exception e) {
		super(msg, e);
		this.code = -1;
	}

	/**
	 * Restituisce il codice di errore.
	 * @return codice errore
	 */
	public int getCode() {
		return code;
	}
}
