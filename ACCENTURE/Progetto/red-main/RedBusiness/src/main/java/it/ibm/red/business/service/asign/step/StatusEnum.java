package it.ibm.red.business.service.asign.step;

/**
 * Stati di lavorazione degli item di firma asincrona.
 */
public enum StatusEnum {

	/**
	 * Stato di presa in carico, quando un item viene inserito in tabella, viene impostato questo stato.
	 */
	PRESO_IN_CARICO("PRESO IN CARICO", 1, StatusEnum.LIGHT_GREEN),
	
	/**
	 * Stato in fase di elaborazione, vuol dire che l'orchestrator sta eseguendo gli step di firma.
	 */
	IN_ELABORAZIONE("IN ELABORAZIONE", 2, StatusEnum.LIGHT_GREEN),
	
	/**
	 * Item firmato correttamente e processo completato.
	 */
	ELABORATO("ELABORATO", 3, StatusEnum.LIGHT_GREEN),
	
	/**
	 * Stato di retry, quando un item è in questo stato vuol dire che si è verificato un errore durante il processo di firma
	 * ma l'item può essere recuperato per un nuovo tentativo.
	 */
	RETRY("RETRY", 4, "yellow"),
	
	/**
	 * Stato di errore, in questo stato l'item non verrà più recuperato per un successivo tentativo.
	 */
	ERRORE("ERRORE", 5, "red");
	
	private static final String LIGHT_GREEN = "lightgreen";
	
	/**
	 * Valore dello stato.
	 */
	private Integer value;
	
	/**
	 * Display name dello stato.
	 */
	private String displayName;
	
	/**
	 * Styleclass dell'icona associata allo stato.
	 */
	private String sclass;

	/**
	 * Costruttore completo.
	 * @param inDisplayName
	 * @param v
	 * @param sclass
	 */
	private StatusEnum(String inDisplayName, Integer v, String sclass) {
		value = v;
		displayName = inDisplayName;
		this.sclass = sclass;
	}
	
	/**
	 * Restituisce l'enum identificata dal value.
	 * @param value
	 * @return enum
	 */
	public static StatusEnum get(Integer value) {
		StatusEnum out = null;
		for (StatusEnum s: StatusEnum.values()) {
			if (s.getValue().equals(value)) {
				out = s;
				break;
			}
		}
		return out;
	}

	/**
	 * Restituisce il display name dello stato.
	 * @return display name
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Restituisce il value dello stato.
	 * @return value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Restituisce lo styleclass associato all'icona dello stato.
	 * @return styleclass icona
	 */
	public String getSclass() {
		return sclass;
	}
}
