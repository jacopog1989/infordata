package it.ibm.red.business.dto;

/**
 * DTO per il salvataggio dei parametri della ricerca avanzata dei documenti.
 * 
 * @author m.crescentini
 *
 */
public class ParamsRicercaAvanzataDocSalvataDTO extends AbstractDTO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -911590451558827089L;
	
	/**
	 * Parametri ricerca 
	 */
	private ParamsRicercaAvanzataDocDTO parametri;
	
	/**
	 * Tipo documento 
	 */
	private String tipoDocumento;
	
	/**
	 * Descrizione assegnatario 
	 */
	private String descrAssegnatario;
	
	/**
	 * Assegnatario copia conforme 
	 */
	private String assegnatarioCopiaConforme;
	
	/**
	 * Descrizione assegnatario operazione 
	 */
	private String descrAssegnatarioOperazione;
	
	
	/**
	 * Costruttore params ricerca avanzata doc salvata DTO.
	 *
	 * @param parametri the parametri
	 * @param tipoDocumento the tipo documento
	 * @param descrAssegnatario the descr assegnatario
	 * @param assegnatarioCopiaConforme the assegnatario copia conforme
	 * @param descrAssegnatarioOperazione the descr assegnatario operazione
	 */
	public ParamsRicercaAvanzataDocSalvataDTO(final ParamsRicercaAvanzataDocDTO parametri, final String tipoDocumento, final String descrAssegnatario, final String assegnatarioCopiaConforme, 
			final String descrAssegnatarioOperazione) {
		super();
		this.parametri = parametri;
		this.tipoDocumento = tipoDocumento;
		this.descrAssegnatario = descrAssegnatario;
		this.assegnatarioCopiaConforme = assegnatarioCopiaConforme;
		this.descrAssegnatarioOperazione = descrAssegnatarioOperazione;
	}

	/** 
	 *
	 * @return the parametri
	 */
	public ParamsRicercaAvanzataDocDTO getParametri() {
		return parametri;
	}

	/** 
	 *
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/** 
	 *
	 * @return the descrAssegnatario
	 */
	public String getDescrAssegnatario() {
		return descrAssegnatario;
	}

	/** 
	 *
	 * @return the assegnatarioCopiaConforme
	 */
	public String getAssegnatarioCopiaConforme() {
		return assegnatarioCopiaConforme;
	}

	/** 
	 *
	 * @return the descrAssegnatarioOperazione
	 */
	public String getDescrAssegnatarioOperazione() {
		return descrAssegnatarioOperazione;
	}
}