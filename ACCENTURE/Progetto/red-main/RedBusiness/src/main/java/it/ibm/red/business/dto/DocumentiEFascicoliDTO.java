package it.ibm.red.business.dto;
/**
 * DTO utile alla sez ricerca dei fascicoli per documenti.
 * 
 * @author 
 *
 */
public class DocumentiEFascicoliDTO extends AbstractDTO {

	/**
	 * Serializable.
	 */
	private static final long serialVersionUID = -279118821863998901L;
	
	// Documenti
	
	/**
	 * Il GUUID.
	 */
	private String guuid;

	/**
	 * Numero documento.
	 */
	private Integer numeroDocumento;
	
	/**
	 * Tipologia documento.
	 */
	private String tipologiaDocumento;
	
	/**
	 * Anno protocollo.
	 */
	private Integer annoProtocollo;
	
	/**
	 * Numero protocollo.
	 */
	private Integer numeroProtocollo;
	
	/**
	 * Document title.
	 */
	private String documentTitle;
	
	/**
	 * Classe Documentale.
	 */
	private String classeDocumentale;
	
	//Fascicoli
	
	/**
	 * Id Fascicolo.
	 */
	private String id;
	
	// Fascicoli e Documenti
	
	/**
	 * Oggetto.
	 */
	private String oggetto;
	
	/**
	 * Indica se è un documento o un fascicolo.
	 */
	private String tipo;
	
	/**
	 * Variabili per il recupero degli oggetti originali.
	 */
	private MasterDocumentRedDTO documentoOriginale;
	
	/**
	 * Fascicolo originale.
	 */
	private FascicoloDTO fascicoloOriginale;

	/**
	 * Costruttore di default.
	 */
	public DocumentiEFascicoliDTO() {
		super();
	}
	
	/**
	 * Costruttore per il setting del documento.
	 * 
	 * @param documento
	 */
	public DocumentiEFascicoliDTO(final MasterDocumentRedDTO documento) {
		super();
		//documenti
		if (documento != null) {
			this.tipo = "documento";
			this.guuid = documento.getGuuid();
			this.numeroDocumento = documento.getNumeroDocumento();
			this.tipologiaDocumento = documento.getTipologiaDocumento();
			this.annoProtocollo = documento.getAnnoProtocollo();
			this.numeroProtocollo = documento.getNumeroProtocollo();
			this.oggetto = documento.getOggetto();
			this.documentTitle = documento.getDocumentTitle();
			this.classeDocumentale = documento.getClasseDocumentale();
			this.documentoOriginale = documento;
		}
	}

	/**
	 * Costruttore per il setting del documento.
	 * 
	 * @param documento
	 */
	public DocumentiEFascicoliDTO(final DocumentoAllegabileDTO documento) {
		super();
		//documenti
		if (documento != null) {
			this.tipo = "documento";
			this.guuid = documento.getGuid();
			this.numeroDocumento = documento.getNumeroDocumento();
			this.tipologiaDocumento = documento.getIdTipologiaDocumento().toString();
			this.annoProtocollo = documento.getAnnoProtocollo();
			this.numeroProtocollo = documento.getNumeroProtocollo();
			this.oggetto = documento.getOggetto();
			this.documentTitle = documento.getDocumentTitle();
		}
	}
	
	/**
	 * Costruttore per il setting del fascicolo.
	 * 
	 * @param fascicolo
	 */
	public DocumentiEFascicoliDTO(final FascicoloDTO fascicolo) {
		super();
		//fascicoli
		if (fascicolo != null) {
			this.tipo = "fascicolo";
			this.id = fascicolo.getIdFascicolo();
			this.oggetto = fascicolo.getOggetto();
			this.documentTitle = fascicolo.getDocumentTitle();
			this.classeDocumentale = fascicolo.getClasseDocumentale();
			this.fascicoloOriginale = fascicolo;
		}
	}

	/**
	 * Restituisce il guuid.
	 * @return guuid
	 */
	public String getGuuid() {
		return guuid;
	}

	/**
	 * Imposta il guuid.
	 * @param guuid
	 */
	public void setGuuid(final String guuid) {
		this.guuid = guuid;
	}

	/**
	 * Restituisce il numero documento.
	 * @return numero documento
	 */
	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * Imposta il numero documento.
	 * @param numeroDocumento
	 */
	public void setNumeroDocumento(final Integer numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * Restituisce la tipologia documento.
	 * @return tipologia documento
	 */
	public String getTipologiaDocumento() {
		return tipologiaDocumento;
	}

	/**
	 * Imposta la tipologia documento.
	 * @param tipologiaDocumento
	 */
	public void setTipologiaDocumento(final String tipologiaDocumento) {
		this.tipologiaDocumento = tipologiaDocumento;
	}

	/**
	 * Restituisce l'anno del protocollo.
	 * @return anno protocollo
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * Imposta l'anno del protocollo.
	 * @param annoProtocollo
	 */
	public void setAnnoProtocollo(final Integer annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/**
	 * Restituisce il numero del protocollo.
	 * @return numero protocollo
	 */
	public Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Imposta il numero del protocollo.
	 * 
	 * @param numeroProtocollo
	 */
	public void setNumeroProtocollo(final Integer numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	/**
	 * Restitusce l'id.
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Imposta l'id.
	 * @param id
	 */
	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * Restituisce l'oggetto.
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Imposta l'oggetto.
	 * @param oggetto
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * Restituisce il documentTitle.
	 * @return documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta il documentTitle.
	 * @param documentTitle
	 */
	public void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * Restituisce la classe documentale.
	 * @return classe documentale
	 */
	public String getClasseDocumentale() {
		return classeDocumentale;
	}

	/**
	 * Imposta la classe documentale.
	 * @param classeDocumentale
	 */
	public void setClasseDocumentale(final String classeDocumentale) {
		this.classeDocumentale = classeDocumentale;
	}

	/**
	 * Restituisce il tipo.
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Imposta il tipo.
	 * @param tipo
	 */
	public void setTipo(final String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Restituisce il documento originale come MasterDocumentRedDTO.
	 * @return documento originale
	 */
	public MasterDocumentRedDTO getDocumentoOriginale() {
		return documentoOriginale;
	}

	/**
	 * Restituisce il fascicolo originale.
	 * @return fascicolo originale
	 */
	public FascicoloDTO getFascicoloOriginale() {
		return fascicoloOriginale;
	}

	/**
	 * Imposta il documento originale.
	 * @param documentoOriginale
	 */
	public void setDocumentoOriginale(final MasterDocumentRedDTO documentoOriginale) {
		this.documentoOriginale = documentoOriginale;
	}

	/**
	 * Imposta il fascicolo originale.
	 * @param fascicoloOriginale
	 */
	public void setFascicoloOriginale(final FascicoloDTO fascicoloOriginale) {
		this.fascicoloOriginale = fascicoloOriginale;
	}

}
