package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import it.ibm.red.business.dto.DelegatoDTO;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Ruolo;

/**
 * 
 * @author CPIERASC
 *
 *         Interfaccia dao gestione ruolo.
 */
public interface IRuoloDAO extends Serializable {

	/**
	 * Metodo per il recupero di un ruolo a partire dal suo identificativo.
	 * 
	 * @param idRuolo    identificativo del ruolo
	 * @param connection connessione al db
	 * @return ruolo
	 */
	Ruolo getRuolo(Long idRuolo, Connection connection);

	/**
	 * Metodo per il recupero di un ruolo a partire dal suo identificativo.
	 * 
	 * @param idRuolo    identificativo del ruolo
	 * @param aoo        aoo
	 * @param connection connessione al db
	 * @return ruolo
	 */
	Ruolo getRuolo(Long idRuolo, Aoo aoo, Connection connection);

	/**
	 * Metodo per l'associazione di un Ruolo ad un determinato utente e Nodo.
	 * 
	 * @param idNodo
	 * @param idUtente
	 * @param idRuolo
	 * @param connection
	 */
	boolean addRuoloToUtente(Long idNodo, Long idUtente, Long idRuolo, int predefinito, Date dataAttivazione, Date dataDisattivazione, String motivoDelega,
			Connection connection);

	/**
	 * Metodo per la rimozione di un Ruolo ad un determinato utente e Nodo.
	 * 
	 * @param idNodo
	 * @param idUtente
	 * @param idRuolo
	 * @param connection
	 */
	Boolean removeRuoloFromUtente(Long idNodo, Long idUtente, Long idRuolo, Connection connection);

	/**
	 * Metodo per il recupero delle assegnazioni del Ruolo di Delegato a Libro Firma
	 * 
	 * @param idRuolo
	 * @param idsUffici
	 * @return
	 */
	Collection<DelegatoDTO> getAssegnazioniDelegato(Long idRuolo, List<Long> idsUffici, Long idAoo, Connection con);

	/**
	 * Metodo per il recupero della categoria a partire dall'identificativo del
	 * ruolo.
	 * 
	 * @param idRuolo
	 * @param connection
	 * @return
	 */
	int getCategoriaByRuolo(Long idRuolo, Connection connection);

	/**
	 * Ottiene la lista dei ruoli attivati per l'aoo.
	 * 
	 * @param idAoo
	 * @param con
	 * @return lista di ruoli
	 */
	List<Ruolo> getListaRuoliAttivati(Long idAoo, Connection con);

	/**
	 * Ottiene la lista dei ruoli per utente ufficio.
	 * 
	 * @param idUtente
	 * @param idNodo
	 * @param con
	 * @return lista degli ids dei ruoli
	 */
	List<Long> getListaRuoliUtenteUfficio(Long idUtente, Long idNodo, Connection con);

	/**
	 * Ottiene le deleghe assegnate.
	 * 
	 * @param idRuolo
	 * @param idNodo
	 * @param idAOO
	 * @param con
	 * @return delegato
	 */
	DelegatoDTO getDelegatoLibroFirma(Long idRuolo, Long idNodo, Long idAOO, Connection con);
}
