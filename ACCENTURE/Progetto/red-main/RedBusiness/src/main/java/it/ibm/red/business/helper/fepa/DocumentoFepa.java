/**
 * 
 */
package it.ibm.red.business.helper.fepa;

import java.io.Serializable;
import java.util.Date;

/**
 * Model documento fepa.
 */
public class DocumentoFepa implements Serializable {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -5103222285051078635L;
	
	/**
	 * id del documento FEPA.
	 */
	private String idDocumentoFepa;

	/**
	 * Identificativo documento.
	 */
	private int idDocumento;

	/**
	 * Tipologia documento.
	 */
	private String tipoDocumento;

	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Content.
	 */
	private byte[] content;

	/**
	 * Content type.
	 */
	private String contentType;

	/**
	 * Nome del file.
	 */
	private String filename;

	/**
	 * Document title.
	 */
	private String documentTitle;

	/**
	 * Data creazione.
	 */
	private Date dataCreazione;
	
	//DECRETO DIRIGENZIALE & DICHIARAZIONE SERVIZI RESI

	/**
	 * Data decreto.
	 */
	private Date datadecreto;

	/**
	 * Firmatario decreto.
	 */
	private String firmatario;

	/**
	 * Numero decreto.
	 */
	private int numDecreto;
	//
	
	/**
	 * Costruttore.
	 */
	public DocumentoFepa() {
	}

	/**
	 * Costruttore di classe.
	 * @param idDocumento
	 * @param tipoDocumento
	 * @param descrizione
	 * @param content
	 * @param contentType
	 * @param filename
	 */
	public DocumentoFepa(final int idDocumento,
			final String tipoDocumento, final String descrizione, final byte[] content,
			final String contentType, final String filename) {
		super();
		this.idDocumento = idDocumento;
	 
		this.tipoDocumento = tipoDocumento;
		this.descrizione = descrizione;
		this.content = content;
		this.contentType = contentType;
		this.filename = filename;
	}

	/**
	 * Restituisce l'id del documento.
	 * @return id documento
	 */
	public int getIdDocumento() {
		return idDocumento;
	}

	/**
	 * Imposta l'id del documento.
	 * @param idDocumento
	 */
	public void setIdDocumento(final int idDocumento) {
		this.idDocumento = idDocumento;
	}

	/**
	 * Restituisce il tipo documento.
	 * @return tipo documento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * Imposta il tipo documento.
	 * @param tipoDocumento
	 */
	public void setTipoDocumento(final String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * Restituisce la descrizione.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Imposta la descrizione.
	 * @param descrizione
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * Restituisce il content come byte array.
	 * @return content
	 */
	public byte[] getContent() {
		return content;
	}

	/**
	 * Imposta il content come byte array.
	 * @param bs
	 */
	public void setContent(final byte[] bs) {
		this.content = bs;
	}

	/**
	 * Restituisce il content type.
	 * @return content type
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * Imposta il content type.
	 * @param contentType
	 */
	public void setContentType(final String contentType) {
		this.contentType = contentType;
	}

	/**
	 * Restituisce il nome del file.
	 * @return filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * Imposta il nome del file.
	 * @param filename
	 */
	public void setFilename(final String filename) {
		this.filename = filename;
	}

	/**
	 * Restituisce la data del decreto.
	 * @return data decreto
	 */
	public Date getDatadecreto() {
		return datadecreto;
	}

	/**
	 * Imposta la data decreto.
	 * @param datadecreto
	 */
	public void setDatadecreto(final Date datadecreto) {
		this.datadecreto = datadecreto;
	}

	/**
	 * Restituisce il firmatario.
	 * @return firmatario
	 */
	public String getFirmatario() {
		return firmatario;
	}

	/**
	 * Imposta il firmatario.
	 * @param firmatario
	 */
	public void setFirmatario(final String firmatario) {
		this.firmatario = firmatario;
	}

	/**
	 * Restituisce il numero decreto.
	 * @return numero decreto
	 */
	public int getNumDecreto() {
		return numDecreto;
	}

	/**
	 * Imposta il numero decreto.
	 * @param numDecreto
	 */
	public void setNumDecreto(final int numDecreto) {
		this.numDecreto = numDecreto;
	}

	/**
	 * Restituisce l'id del documento FEPA.
	 * @return id documento
	 */
	public String getIdDocumentoFepa() {
		return idDocumentoFepa;
	}

	/**
	 * Imposta l'id del documento.
	 * @param idFileNet
	 */
	public void setIdDocumentoFepa(final String idFileNet) {
		this.idDocumentoFepa = idFileNet;
	}

	/**
	 * Restituisce il documentTitle.
	 * @return documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta il documentTitle.
	 * @param documentTitle
	 */
	public void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * Restituisce la data di creazione del documento FEPA.
	 * @return data creazione
	 */
	public Date getDataCreazione() {
		return dataCreazione;
	}

	/**
	 * Imposta la data di creazione del documento FEPA.
	 * @param dataCreazione
	 */
	public void setDataCreazione(final Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	/**
	 * @see java.lang.Object#toString().
	 */
	@Override
	public String toString() {
		return "DocumentoFepa [" + (idDocumentoFepa != null ? "idDocumentoFepa=" + idDocumentoFepa + ", " : "")
				+ "idDocumento=" + idDocumento + ", "
				+ (tipoDocumento != null ? "tipoDocumento=" + tipoDocumento + ", " : "")
				+ (descrizione != null ? "descrizione=" + descrizione + ", " : "")
				+ (contentType != null ? "contentType=" + contentType + ", " : "")
				+ (filename != null ? "filename=" + filename + ", " : "")
				+ (datadecreto != null ? "datadecreto=" + datadecreto + ", " : "")
				+ (firmatario != null ? "firmatario=" + firmatario + ", " : "") + "numDecreto=" + numDecreto + "]";
	}

	
}
