package it.ibm.red.business.helper.notifiche.states;

import java.util.Map;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.enums.StatoCodaEmailEnum;
import it.ibm.red.business.enums.TipoNotificaPecEnum;
import it.ibm.red.business.helper.notifiche.IGestioneMailState;
import it.ibm.red.business.helper.notifiche.NotificaHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.CodaEmail;

/**
 * Classe accettazione State 1.
 */
public class AttesaAccettazioneState1 implements IGestioneMailState {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AttesaAccettazioneState1.class.getName());

	/**
	 * @see it.ibm.red.business.helper.notifiche.IGestioneMailState#getNextNotificaEmailToUpdate(it.ibm.red.business.helper.notifiche.NotificaHelper,
	 *      it.ibm.red.business.persistence.model.CodaEmail, java.util.Map,
	 *      boolean).
	 */
	@Override
	public CodaEmail getNextNotificaEmailToUpdate(final NotificaHelper nh, final CodaEmail notificaEmail, final Map<String, EmailDTO> notifiche, final boolean reInvio) {
		CodaEmail result = notificaEmail;
		
		IGestioneMailState nextState =  new AttesaAccettazioneState1();
		int nextStatoRicevuta =  notificaEmail.getStatoRicevuta();
		int tipoDestinatario = notificaEmail.getTipoDestinatario();
		String mittente = nh.getMittente();
		String key = mittente + "#" + notificaEmail.getIdMessaggio() + "_MIT";
		LOGGER.info("recupero notifica nella mappa delle notifiche recuperate su Filenet con chiave " + key);
		EmailDTO notifica = notifiche.get(key);
		
		if (notifica != null) {
		
			String oggetto = notifica.getOggetto();		
			LOGGER.info("notifica con chiave " + key + " recuperata da Filenet con oggetto [" + oggetto + "]");
			if (TipoNotificaPecEnum.checkPrefissoNotificaPec(oggetto, TipoNotificaPecEnum.ACCETTAZIONE)) {
				if (tipoDestinatario == Constants.TipologiaDestinatariMittenti.TIPOLOGIA_DESTINATARI_MITTENTI_PEO) {
					nextState = new ChiusuraState3();
					nextStatoRicevuta = StatoCodaEmailEnum.CHIUSURA.getStatus();
				} else if (tipoDestinatario == Constants.TipologiaDestinatariMittenti.TIPOLOGIA_DESTINATARI_MITTENTI_PEC) {
					nextState = new AttesaAvvenutaConsegnaState2();
					nextStatoRicevuta = StatoCodaEmailEnum.ATTESA_AVVENUTA_CONSEGNA.getStatus();
				}
			} else if (TipoNotificaPecEnum.checkPrefissoNotificaPec(oggetto, TipoNotificaPecEnum.NON_ACCETTAZIONE)) {
				nextState = new ErroreState4();
				nextStatoRicevuta = StatoCodaEmailEnum.REINVIO.getStatus();
			}
			
			nh.setGestioneMailState(nextState);
			result.setStatoRicevuta(nextStatoRicevuta);
			
			return result;
		} else {
			LOGGER.warn("notifica con chiave " + key + " non recuperata da Filenet ");
			return null;
		}
	}

}
