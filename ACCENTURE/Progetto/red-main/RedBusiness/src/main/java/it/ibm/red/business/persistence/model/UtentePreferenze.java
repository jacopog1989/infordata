package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * The Class UtentePreferenze.
 *
 * @author CPIERASC
 * 
 *         Entità che mappa la tabella UTENTEPREFERENZE.
 */
public class UtentePreferenze implements Serializable {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -4291072586253911526L;

	/**
	 * Identificativo utente.
	 */
	private final long idUtente;

	/**
	 * Identificativo tema prescelto dall'utente.
	 */
	private final Integer idTema;

	/**
	 * Dealy autocomplete.
	 */
	private final Integer autocompleteDelay;

	/**
	 * Pagina iniziale prescelta dall'utente.
	 */
	private final String startPage;

	/**
	 * Numero di elementi.
	 */
	private final Integer numberOfElements;

	/**
	 * Dimensione testo.
	 */
	private final Integer textSize;

	/**
	 * Pagina preferita dall'utente.
	 */
	private final String paginaPreferita;

	/**
	 * Flag gruppo scrivania.
	 */
	private boolean groupScrivania;

	/**
	 * Flag gruppo archivio.
	 */
	private boolean groupArchivio;

	/**
	 * Flag gruppo ufficio.
	 */
	private boolean groupUfficio;

	/**
	 * Flag gruppo mail.
	 */
	private boolean groupMail;

	/**
	 * Flag gruppo fatturazione elettronica.
	 */
	private boolean groupFatEletronica;

	/**
	 * Flag gruppo organigramma.
	 */
	private boolean groupOrganigramma;

	/**
	 * Flag gruppo reader nativo/js.
	 */
	private final boolean nativeReaderPDFenable;

	/**
	 * Flag base64.
	 */
	private boolean base64;

	/**
	 * Flag url encoding.
	 */
	private boolean urlEncoding;

	/**
	 * Flag encrypting.
	 */
	private boolean noCrypt;

	/**
	 * Flag firma remota.
	 */
	private boolean firmaRemotaFirst;

	/**
	 * Costruttore.
	 * 
	 * @param inIdUtente              identificativo utente
	 * @param inIdTema                identificativo tema
	 * @param inStartPage             pagina iniziale
	 * @param inAutocompleteDelay
	 * @param inNumberOfElements
	 * @param inPaginaPreferita
	 * @param inTextSize
	 * @param inGroupScrivania
	 * @param inGroupArchivio
	 * @param inGroupUfficio
	 * @param inGroupMail
	 * @param inGroupFatEletronica
	 * @param inGroupOrganigramma
	 * @param inNativeReaderPDFenable
	 */
	public UtentePreferenze(final long inIdUtente, final Integer inIdTema, final String inStartPage, final Integer inAutocompleteDelay, final Integer inNumberOfElements,
			final String inPaginaPreferita, final Integer inTextSize, final boolean inGroupScrivania, final boolean inGroupArchivio, final boolean inGroupUfficio,
			final boolean inGroupMail, final boolean inGroupFatEletronica, final boolean inGroupOrganigramma, 
			final boolean inNativeReaderPDFenable, final boolean inBase64,
			final boolean inUrlEncoding, final boolean noCrypt, final boolean firmaRemotaFirst) {
		super();
		this.idUtente = inIdUtente;
		this.idTema = inIdTema;
		this.startPage = inStartPage;
		this.autocompleteDelay = inAutocompleteDelay;
		this.numberOfElements = inNumberOfElements;
		this.textSize = inTextSize;
		this.paginaPreferita = inPaginaPreferita;
		this.nativeReaderPDFenable = inNativeReaderPDFenable;

		if (inStartPage != null) {
			this.groupScrivania = inGroupScrivania;
			this.groupArchivio = inGroupArchivio;
			this.groupUfficio = inGroupUfficio;
			this.groupMail = inGroupMail;
			this.groupFatEletronica = inGroupFatEletronica;
			this.groupOrganigramma = inGroupOrganigramma;
		} else {
			this.groupScrivania = true;
			this.groupArchivio = true;
			this.groupUfficio = true;
			this.groupMail = true;
			this.groupFatEletronica = true;
			this.groupOrganigramma = true;
		}

		this.base64 = inBase64;
		this.urlEncoding = inUrlEncoding;
		this.noCrypt = noCrypt;
		this.firmaRemotaFirst = firmaRemotaFirst;
	}

	/**
	 * Getter identificativo utente.
	 * 
	 * @return identificativo utente
	 */
	public final long getIdUtente() {
		return idUtente;
	}

	/**
	 * Getter identificativo tema.
	 * 
	 * @return identificativo tema
	 */
	public final Integer getIdTema() {
		return idTema;
	}

	/**
	 * Getter pagina iniziale.
	 * 
	 * @return pagina iniziale
	 */
	public final String getStartPage() {
		return startPage;
	}

	/**
	 * Restituisce il delay dell'autocompletamento.
	 * 
	 * @return delay autocompletamento
	 */
	public Integer getAutocompleteDelay() {
		return autocompleteDelay;
	}

	/**
	 * @return the numberOfElements
	 */
	public final Integer getNumberOfElements() {
		return numberOfElements;
	}

	/**
	 * @return the textSize
	 */
	public final Integer getTextSize() {
		return textSize;
	}

	/**
	 * @return the paginaPreferita
	 */
	public final String getPaginaPreferita() {
		return paginaPreferita;
	}

	/**
	 * @return the groupScrivania
	 */
	public final boolean isGroupScrivania() {
		return groupScrivania;
	}

	/**
	 * @return the groupArchivio
	 */
	public final boolean isGroupArchivio() {
		return groupArchivio;
	}

	/**
	 * @return the groupUfficio
	 */
	public final boolean isGroupUfficio() {
		return groupUfficio;
	}

	/**
	 * @return the groupMail
	 */
	public final boolean isGroupMail() {
		return groupMail;
	}

	/**
	 * @return the groupFatEletronica
	 */
	public final boolean isGroupFatEletronica() {
		return groupFatEletronica;
	}

	/**
	 * @return the groupOrganigramma
	 */
	public final boolean isGroupOrganigramma() {
		return groupOrganigramma;
	}

	/**
	 * Restituisce true se Reader PDF nativo abilitato, false altrimenti.
	 * 
	 * @return true se Reader PDF nativo abilitato, false altrimenti
	 */
	public boolean isNativeReaderPDFenable() {
		return nativeReaderPDFenable;
	}

	/**
	 * Restituisce il flag associato al base 64.
	 * 
	 * @return flag associato al base 64
	 */
	public boolean getBase64() {
		return base64;
	}

	/**
	 * Imposta il flag associato al base 64.
	 * 
	 * @param base64
	 */
	public void setBase64(final boolean base64) {
		this.base64 = base64;
	}

	/**
	 * Restituisce il flag associato all'encoding dell'Url.
	 * 
	 * @return flag associato all'encoding dell'Url
	 */
	public boolean getUrlEncoding() {
		return urlEncoding;
	}

	/**
	 * Imposta il flag associato all'encoding dell'Url.
	 * 
	 * @param urlEncoding
	 */
	public void setUrlEncoding(final boolean urlEncoding) {
		this.urlEncoding = urlEncoding;
	}

	/**
	 * Restituisce il flag associato al no_crypt.
	 * 
	 * @return flag associato al no_crypt
	 */
	public boolean getNoCrypt() {
		return noCrypt;
	}

	/**
	 * Imposta il flag associato al no_crypt.
	 * 
	 * @param noCrypt
	 */
	public void setNoCrypt(final boolean noCrypt) {
		this.noCrypt = noCrypt;
	}

	/**
	 * Restituisce il flag associato alla firma remota FIRST.
	 * 
	 * @return flag associato alla firma remota FIRST
	 */
	public boolean isFirmaRemotaFirst() {
		return firmaRemotaFirst;
	}

	/**
	 * Imposta il flag associato alla firma remota FIRST.
	 * 
	 * @param firmaRemotaFirst
	 */
	public void setFirmaRemotaFirst(final boolean firmaRemotaFirst) {
		this.firmaRemotaFirst = firmaRemotaFirst;
	}
}
