package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.constants.Constants.Modalita;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.HierarchicalFileWrapperDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.SignerInfoDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.service.ICreaDocumentoRedEntrataSRV;
import it.ibm.red.business.service.IRubricaSRV;
import it.ibm.red.business.service.IScompattaMailSRV;
import it.ibm.red.business.service.ITipologiaDocumentoSRV;

/**
 * @author m.crescentini
 * 
 * Servizio per la creazione di un documento in entrata.
 *
 */
@Service
public abstract class CreaDocumentoRedEntrataAbstractSRV extends CreaDocumentoRedAbstractSRV implements ICreaDocumentoRedEntrataSRV {
	
	private static final long serialVersionUID = -676023783214741104L;
	
	/**
	 * Servizio.
	 */
	@Autowired
	private ITipologiaDocumentoSRV tipologiaDocumentoSRV;
	
	/**
	 * Servizio.
	 */
	@Autowired
	private IScompattaMailSRV scompattaMailSRV;
	
	/**
	 * Servizio.
	 */
	@Autowired
	private IRubricaSRV rubricaSRV;
	
	/**
	 * Dao.
	 */
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;
	
	
	/* (non-Javadoc)
	 * @see it.ibm.red.business.service.ICreaDocumentoEntrataSRV#creaDocumentoRedEntrataDaMail(java.lang.String, java.lang.String, boolean, 
	 * java.lang.String, java.lang.Long, java.lang.String, it.ibm.red.business.dto.UtenteDTO, it.ibm.red.business.persistence.model.Aoo, 
	 * java.sql.Connection)
	 */
	@Override
	public final EsitoSalvaDocumentoDTO creaDocumentoRedEntrataDaMail(final String guidMail, final String oggetto, final boolean mantieniAllegatiOriginali, 
			final String indiceClassificazione, final Long idContattoMittente, final String notaMail, final UtenteDTO utenteAssegnatario, final Aoo aoo, final Connection con) {
		// Aggancia PK handler di verifica dall'AOO
		if (utenteAssegnatario.getSignerInfo() == null) {
			utenteAssegnatario.setSignerInfo(new SignerInfoDTO(aoo.getPkHandlerFirma(), aoo.getPkHandlerVerifica()));
		} else {
			utenteAssegnatario.getSignerInfo().setPkHandlerFirma(aoo.getPkHandlerFirma());
			utenteAssegnatario.getSignerInfo().setPkHandlerVerifica(aoo.getPkHandlerVerifica());
		}
		
		// Recupero del documento da creare
		final DetailDocumentRedDTO documento = getDocumentoRedInEntrataDaMail(guidMail, oggetto, mantieniAllegatiOriginali, idContattoMittente, indiceClassificazione,  
				notaMail, utenteAssegnatario, con);
		
		// Recupero dei parametri di creazione
		final SalvaDocumentoRedParametriDTO parametri = getParametriDocumentoRedEntrataDaMail(guidMail);
		
		return creaDocumentoRed(documento, parametri, utenteAssegnatario);
	}
	
	
	/**
	 * @see it.ibm.red.business.service.ICreaDocumentoRedEntrataSRV#creaDocumentoRedEntrataDaContent(byte[],
	 *      java.lang.String, java.lang.String, java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String, java.lang.Integer,
	 *      java.lang.Integer, java.lang.Long, java.util.List, java.lang.Long,
	 *      java.lang.Long, java.lang.Integer, java.lang.Integer, java.util.Date,
	 *      java.lang.String, it.ibm.red.business.persistence.model.Aoo,
	 *      java.sql.Connection).
	 */
	@Override
	public final EsitoSalvaDocumentoDTO creaDocumentoRedEntrataDaContent(final byte[] content, final String contentType, final String nomeFile, final String oggetto, 
			final UtenteDTO utenteCreatore, final String indiceClassificazione, final Integer idTipologiaDocumento, final Integer idTipoProcedimento, 
			final Long idContattoMittente, final List<AllegatoDTO> allegati, final Long idUtenteAssegnatario, final Long idUfficioAssegnatario, 
			final Integer numeroProtocollo, final Integer annoProtocollo, final Date dataProtocollo, final String codiceFlusso, final Aoo aoo, final Connection con) {
		// Recupero del documento da creare
		final DetailDocumentRedDTO documento = getDocumentoRedInEntrataDaContent(content, contentType, nomeFile, oggetto, indiceClassificazione, idTipologiaDocumento, 
				idTipoProcedimento, idContattoMittente, allegati, idUtenteAssegnatario, idUfficioAssegnatario, numeroProtocollo, annoProtocollo, 
				dataProtocollo, codiceFlusso, con);
		
		// Recupero dei parametri di creazione
		final SalvaDocumentoRedParametriDTO parametri = getParametriDocumentoRedEntrataDaContent();
		
		return creaDocumentoRed(documento, parametri, utenteCreatore);
	}


	/**
	 * @param guidMail
	 * @param oggetto
	 * @param mantieniAllegatiOriginali
	 * @param idContattoMittente
	 * @param indiceClassificazione
	 * @param notaMail
	 * @param utente
	 * @param con
	 * @return
	 */
	private DetailDocumentRedDTO getDocumentoRedInEntrataDaMail(final String guidMail, final String oggetto, final boolean mantieniAllegatiOriginali, final Long idContattoMittente, 
			final String indiceClassificazione, final String notaMail, final UtenteDTO utente, final Connection con) {		
		final DetailDocumentRedDTO document = new DetailDocumentRedDTO();
		
		// Formato e categoria
		document.setFormatoDocumentoEnum(FormatoDocumentoEnum.ELETTRONICO);
		document.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0]);
		
		// Oggetto
		document.setOggetto(oggetto);
		
		// Scompatta la mail
		final List<HierarchicalFileWrapperDTO> fileListComplete = scompattaMailSRV.scompattaMail(utente, guidMail, mantieniAllegatiOriginali);
		// Seleziona tutte le foglie
		final List<HierarchicalFileWrapperDTO> fileListToAttach = new ArrayList<>();
		for (final HierarchicalFileWrapperDTO file : fileListComplete) {
			if (file.getSons() != null && !file.getSons().isEmpty()) {
				continue;
			}
			
			fileListToAttach.add(file);
		}
		// Trasforma in allegati mantenendo tutti in formato originale
		final Integer idTipologiaDocumentale = tipologiaDocumentoSRV.getTipologiaDocumentalePredefinita(utente.getIdAoo(), utente.getCodiceAoo(), 
				TipoCategoriaEnum.ENTRATA, con);
		final List<AllegatoDTO> allegati = scompattaMailSRV.transformToAllegatoDTO(utente, fileListToAttach, true, idTipologiaDocumentale);
		
		// Allegati
		final ArrayList<AllegatoDTO> allegatiList = new ArrayList<>();
		if (allegati.size() > 1) {
			for (int i = 1; i < allegati.size(); i++) {
				allegatiList.add(allegati.get(i));
			}
		} 
		document.setAllegati(allegatiList);
		
		// Documento principale
		document.setContent(allegati.get(0).getContent());
		document.setNomeFile(allegati.get(0).getNomeFile());
		document.setMimeType(allegati.get(0).getMimeType());
		
		// Assegnazione per competenza
		document.setAssegnazioni(new ArrayList<>());
		document.getAssegnazioni().add(
				new AssegnazioneDTO(null, TipoAssegnazioneEnum.COMPETENZA, null, null, null, utente, new UfficioDTO(utente.getIdUfficio(), utente.getNodoDesc())));
		
		// Tipologia documento/procedimento di default
		document.setIdTipologiaDocumento(idTipologiaDocumentale);
		final List<TipoProcedimento> tipiProcedimentoByTipologiaDocumento = tipoProcedimentoDAO.getTipiProcedimentoByTipologiaDocumento(con,
				document.getIdTipologiaDocumento());
		document.setIdTipologiaProcedimento((int) tipiProcedimentoByTipologiaDocumento.get(0).getTipoProcedimentoId());
		
		// Indice di classificazione e fascicolo
		final String descrizioneFascicoloProcedimentale = tipologiaDocumentoSRV.getById(idTipologiaDocumentale, con).getDescrizione() + " - " + oggetto;
		document.setDescrizioneFascicoloProcedimentale(descrizioneFascicoloProcedimentale);
		document.setIndiceClassificazioneFascicoloProcedimentale(indiceClassificazione);
		
		// Mittente
		gestisciContattoMittente(document, idContattoMittente, con);
		
		// Nota mail
		document.setContentNoteDaMailProtocolla(notaMail);
		
		return document;
	}

	/**
	 * Restituisce il documento Red in entrata dal content.
	 * 
	 * @param content
	 *            content documento
	 * @param contentType
	 *            tipo content del documento
	 * @param nomeFile
	 *            nome file del content
	 * @param oggetto
	 *            oggetto documento
	 * @param indiceClassificazione
	 *            indice calssificazione documento
	 * @param idTipologiaDocumento
	 *            id tipologia documento
	 * @param idTipoProcedimento
	 *            id tipologia procedimento
	 * @param idContattoMittente
	 *            id del contatto mittente
	 * @param allegati
	 *            allegati del documento
	 * @param idUtenteAssegnatario
	 *            id utente assegnatario
	 * @param idUfficioAssegnatario
	 *            id ufficio assegnatario
	 * @param numeroProtocollo
	 *            numero protocollo
	 * @param annoProtocollo
	 *            anno protocollo
	 * @param dataProtocollo
	 *            data protocollo
	 * @param codiceFlusso
	 *            codice del flusso del documento
	 * @param con
	 *            connessione al database
	 * @return dettaglio documento RED
	 */
	private DetailDocumentRedDTO getDocumentoRedInEntrataDaContent(final byte[] content, final String contentType, final String nomeFile, final String oggetto, final String indiceClassificazione, 
			final Integer idTipologiaDocumento, final Integer idTipoProcedimento, final Long idContattoMittente, final List<AllegatoDTO> allegati, 
			final Long idUtenteAssegnatario, final Long idUfficioAssegnatario, final Integer numeroProtocollo, final Integer annoProtocollo, final Date dataProtocollo, final String codiceFlusso, 
			final Connection con) {		
		final DetailDocumentRedDTO document = new DetailDocumentRedDTO();
		
		// Formato e categoria
		document.setFormatoDocumentoEnum(FormatoDocumentoEnum.ELETTRONICO);
		document.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0]);
		
		// Oggetto
		document.setOggetto(oggetto);
		
		// Documento principale
		document.setContent(content);
		document.setNomeFile(nomeFile);
		document.setMimeType(contentType);
				
		// Allegati
		if (!CollectionUtils.isEmpty(allegati)) {
			document.setAllegati(allegati);
		}
		
		// Assegnazione per competenza
		UtenteDTO utenteAssegnatario = null;
		if (idUtenteAssegnatario != null) {
			utenteAssegnatario = new UtenteDTO();
			utenteAssegnatario.setId(idUtenteAssegnatario);
			utenteAssegnatario.setIdUfficio(idUfficioAssegnatario);
		}
		final UfficioDTO ufficioAssegnatario = new UfficioDTO(idUfficioAssegnatario, null);
		
		document.setAssegnazioni(new ArrayList<>());
		document.getAssegnazioni().add(new AssegnazioneDTO(null, TipoAssegnazioneEnum.COMPETENZA, null, null, null, utenteAssegnatario, ufficioAssegnatario));
		
		// Tipologia documento
		document.setIdTipologiaDocumento(idTipologiaDocumento);
		
		// Tipo procedimento
		document.setIdTipologiaProcedimento(idTipoProcedimento);
		
		// Indice di classificazione e fascicolo
		final String descrizioneFascicoloProcedimentale = tipologiaDocumentoSRV.getById(idTipologiaDocumento, con).getDescrizione() + " - " + oggetto;
		document.setDescrizioneFascicoloProcedimentale(descrizioneFascicoloProcedimentale);
		document.setIndiceClassificazioneFascicoloProcedimentale(indiceClassificazione);
		
		// Mittente
		gestisciContattoMittente(document, idContattoMittente, con);
		
		// Protocollo e flag "Da non protocollare"
		// Il documento in entrata NON HA info di protocollo: 
		// in fase di creazione si contatterà il servizio di protocollazione configurato per l'AOO per staccare il protocollo
		// (cfr. SalvaDocumentoSRV.protocollaDocumento)
		if (numeroProtocollo == null || annoProtocollo == null || dataProtocollo == null) {
			// Da non protocollare -> false
			document.setDaNonProtocollare(false);
		// Il documento in entrata HA già info di protocollo:
		// in fase di creazione non si contatterà alcun servizio di protocollazione, ma ci si limiterà ad inserire nel nuovo documento del CE FileNet
		// anche i metadati relativi al protocollo
		} else {
			document.setNumeroProtocollo(numeroProtocollo);
			document.setAnnoProtocollo(annoProtocollo);
			document.setDataProtocollo(dataProtocollo);
		}
		
		// Codice flusso
		document.setCodiceFlusso(codiceFlusso);
		
		return document;
	}

	
	/**
	 * @param document
	 * @param idContattoMittente
	 * @param con
	 */
	private void gestisciContattoMittente(final DetailDocumentRedDTO document, final Long idContattoMittente, final Connection con) {
		// Si recupera il contatto dalla rubrica
		final Contatto contattoMittente = rubricaSRV.getContattoByID(idContattoMittente, con);
		
		if (contattoMittente != null) {
			document.setMittenteContatto(contattoMittente);
		} else {
			throw new RedException("Nessun contatto non presente con ID: " + idContattoMittente);
		}
	}
	
	
	/**
	 * @param guidMail
	 * @return
	 */
	private static SalvaDocumentoRedParametriDTO getParametriDocumentoRedEntrataDaMail(final String guidMail) {
		final SalvaDocumentoRedParametriDTO parametri = new SalvaDocumentoRedParametriDTO();
		
		parametri.setInputMailGuid(guidMail);
		parametri.setModalita(Modalita.MODALITA_INS_MAIL);
		parametri.setContentVariato(true);
		
		return parametri;
	}

	
	/**
	 * @return
	 */
	private static SalvaDocumentoRedParametriDTO getParametriDocumentoRedEntrataDaContent() {
		final SalvaDocumentoRedParametriDTO parametri = new SalvaDocumentoRedParametriDTO();
		
		parametri.setModalita(Modalita.MODALITA_INS);
		parametri.setContentVariato(true);
		
		return parametri;
	}
}