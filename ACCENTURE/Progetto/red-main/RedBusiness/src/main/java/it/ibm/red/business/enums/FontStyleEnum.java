package it.ibm.red.business.enums;

/**
 * The Enum FontStyleEnum.
 *
 * @author CPIERASC
 * 
 *         Enum per descrivere gli stili dei font nella definizione della firma
 *         pades.
 */
public enum FontStyleEnum {
	
	/**
	 * Normale.
	 */
	NORMAL(0),
	
	/**
	 * Grassetto.
	 */
	BOLD(1),
	
	/**
	 * Italico.
	 */
	ITALIC(2),
	
	/**
	 * Grassetto italico.
	 */
	BOLD_ITALIC(3),
	
	/**
	 * Sottolineato.
	 */
	UNDERLINE(4),
	
	/**
	 * Barrato.
	 */
	STRIKETHRU(8);
	
	/**
	 * Valore.
	 */
	private Integer value;

	/**
	 * Costruttore.
	 * 
	 * @param inValue	valore
	 */
	FontStyleEnum(final Integer inValue) {
		this.value = inValue;
	}

	/**
	 * Getter valore.
	 * 
	 * @return	valore
	 */
	public Integer getValue() {
		return value;
	}

}
