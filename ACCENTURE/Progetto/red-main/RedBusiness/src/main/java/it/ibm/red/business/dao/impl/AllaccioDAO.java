package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IAllaccioDAO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.enums.TipoAllaccioEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.StringUtils;

/**
 * Dao per la gestione degli allacci.
 * 
 * @author mcrescentini
 */
@Repository
public class AllaccioDAO extends AbstractDAO implements IAllaccioDAO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 825657946267375095L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AllaccioDAO.class.getName());
	
	/**
	 * Colonna - principale.
	 */
	private static final String PRINCIPALE = "principale";

	/**
	 * Colonna - documento da chiudere.
	 */
	private static final String DOCUMENTODACHIUDERE = "documentodachiudere";

	/**
	 * Colonna - id tipo allaccio.
	 */
	private static final String IDTIPOALLACCIO = "idtipoallaccio";

	/**
	 * Colonna - id documento allacciato.
	 */
	private static final String IDDOCUMENTOALLACCIATO = "iddocumentoallacciato";

	/**
	 * Colonna - id documento.
	 */
	private static final String IDDOCUMENTO = "iddocumento";

	/**
	 * Clausula FROM allaccio.
	 */
	private static final String FROM_ALLACCIO = " FROM ALLACCIO ";

	/**
	 * SELECT ALL.
	 */
	private static final String SELECT_STAR = "SELECT * ";

	/**
	 * @see it.ibm.red.business.dao.IAllaccioDAO#insert(int, java.lang.Long,
	 *      java.lang.Integer, java.lang.Integer, java.lang.Boolean, boolean,
	 *      java.sql.Connection).
	 */
	@Override
	public int insert(final int idDocumento, final Long idAoo, final Integer idDocumentoAllacciato, final Integer idTipoAllaccio, final Boolean documentoDaChiudere,
			final boolean principale, final Connection connection) {
		PreparedStatement ps = null;
		try {
			int index = 1;
			ps = connection.prepareStatement("INSERT INTO allaccio (iddocumento, iddocumentoallacciato, idtipoallaccio, documentodachiudere, principale, idaoo, dataallaccio)"
					+ " VALUES (?, ?, ?, ?, ?, ?, SYSDATE)");
			ps.setInt(index++, idDocumento);
			ps.setInt(index++, idDocumentoAllacciato);
			ps.setInt(index++, idTipoAllaccio);
			ps.setInt(index++, Boolean.TRUE.equals(documentoDaChiudere) ? 1 : 0);
			ps.setInt(index++, principale ? 1 : 0);
			ps.setLong(index++, idAoo);
			return ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'inserimento dell'allaccio: " + idDocumentoAllacciato + " per il documento: " + idDocumento, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IAllaccioDAO#getDocumentiRispostaAllaccio(int,
	 *      int, java.sql.Connection).
	 */
	@Override
	public List<RispostaAllaccioDTO> getDocumentiRispostaAllaccio(final int idDocumento, final int idAoo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			int index = 1;
			ps = con.prepareStatement(SELECT_STAR + FROM_ALLACCIO + " WHERE IDDOCUMENTO = ? " + "   and IDAOO = ?");

			ps.setInt(index++, idDocumento);
			ps.setLong(index++, idAoo);

			rs = ps.executeQuery();

			return getAllacciFromResultset(rs);
		} catch (final SQLException e) {
			LOGGER.error("Errore nel recupero delle Risposte Allaccio", e);
			throw new RedException("Errore nel recupero delle Risposte Allaccio", e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Restituisce la lista degli allacci recuperati dal ResultSet.
	 * 
	 * @param rs
	 *            ResultSte dal quale recuperare gli allacci.
	 * @return Lista degli allacci recuperati.
	 * @throws SQLException
	 */
	private static List<RispostaAllaccioDTO> getAllacciFromResultset(ResultSet rs) throws SQLException {
		List<RispostaAllaccioDTO> list = new ArrayList<>();
		
		while (rs.next()) {
			list.add(new RispostaAllaccioDTO(rs.getInt(IDDOCUMENTO), rs.getString(IDDOCUMENTOALLACCIATO), rs.getInt(IDTIPOALLACCIO),
					rs.getInt(DOCUMENTODACHIUDERE), rs.getInt(PRINCIPALE) == 1));
		}

		return list;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IAllaccioDAO#getDocumentiRispostaAllaccioUscita(int,
	 *      java.sql.Connection).
	 */
	@Override
	public Collection<RispostaAllaccioDTO> getDocumentiRispostaAllaccioUscita(final int idDocumentoAllacciato, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			int index = 1;
			ps = con.prepareStatement(SELECT_STAR + FROM_ALLACCIO + " WHERE iddocumentoallacciato = ?");

			ps.setInt(index++, idDocumentoAllacciato);
			rs = ps.executeQuery();

			return getAllacciFromResultset(rs);
		} catch (final SQLException e) {
			LOGGER.error("Errore nel recupero dei documenti uscita di Risposta Allaccio.", e);
			throw new RedException("Errore nel recupero dei documenti uscita di Risposta Allaccio.", e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Checks for allacci.
	 *
	 * @param documentTitle the document title
	 * @param idAoo         the id aoo
	 * @param connection    the connection
	 * @return the boolean
	 */
	@Override
	public final Boolean hasAllacci(final String documentTitle, final Long idAoo, final Connection connection) {
		Boolean output = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final Long idDoc = Long.parseLong(documentTitle);
			final String querySQL = "SELECT count(*) as counter FROM allaccio a WHERE (a.IDDOCUMENTO = " + sanitize(idDoc) + " OR a.IDDOCUMENTOALLACCIATO = " + sanitize(idDoc)
					+ ") AND a.IDAOO = " + sanitize(idAoo);
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			if (rs.next()) {
				final Integer counter = rs.getInt("counter");
				output = counter != 0;
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il conteggio degli allacci per il documento con id=" + documentTitle, e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IAllaccioDAO#chiudiAllaccioPrincipale(int,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public int chiudiAllaccioPrincipale(final int idDocumento, final Long idAoo, final Connection connection) {
		PreparedStatement ps = null;
		try {
			int index = 1;
			ps = connection.prepareStatement("UPDATE allaccio SET documentodachiudere = 1 WHERE iddocumento = ? AND idaoo = ? AND principale = 1");
			ps.setInt(index++, idDocumento);
			ps.setLong(index++, idAoo);
			return ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la chiusura dell'allaccio principale per il documento: " + idDocumento, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IAllaccioDAO#getAllacciPrincipaliByDocumentTitle(java.util.Collection,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public Collection<String> getAllacciPrincipaliByDocumentTitle(final Collection<String> docsUscita, final Long idAoo, final Connection connection) {
		final Collection<String> output = new ArrayList<>();
		final StringBuilder query = new StringBuilder();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			query.append("SELECT a.iddocumentoallacciato ").append("FROM ALLACCIO a ").append("WHERE ").append("a.principale = ? ").append("AND ").append("a.idaoo = ? ")
					.append("AND ").append(StringUtils.createInCondition("a.iddocumento", docsUscita.iterator(), true));

			ps = connection.prepareStatement(query.toString());

			int index = 1;
			ps.setInt(index++, 1);
			ps.setLong(index++, idAoo);

			rs = ps.executeQuery();

			while (rs.next()) {
				output.add(rs.getString(IDDOCUMENTOALLACCIATO));
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero degli Allacci partendo da un set di DocumentTitle", e);
			throw new RedException("Errore durante il recupero degli Allacci partendo da un set di DocumentTitle", e);
		} finally {
			closeStatement(ps, rs);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IAllaccioDAO#getLastRichiestaIntegrazioni(int,
	 *      java.sql.Connection).
	 */
	@Override
	public RispostaAllaccioDTO getLastRichiestaIntegrazioni(final int idDocIngresso, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		RispostaAllaccioDTO retVal = null;
		try {
			int index = 1;
			ps = con.prepareStatement("SELECT * FROM ALLACCIO WHERE iddocumentoallacciato = ? AND principale = ? AND idtipoallaccio = ? ORDER BY dataallaccio DESC");

			ps.setInt(index++, idDocIngresso);
			ps.setInt(index++, 1); // Principale
			ps.setInt(index++, TipoAllaccioEnum.RICHIESTA_INTEGRAZIONE.getTipoAllaccioId());
			
			rs = ps.executeQuery();

			if (rs.next()) {
				// Viene popolata la risposta allaccio eventualmente recuperata
				retVal = new RispostaAllaccioDTO(rs.getInt(IDDOCUMENTO), rs.getString(IDDOCUMENTOALLACCIATO),
						rs.getInt(IDTIPOALLACCIO), rs.getInt(DOCUMENTODACHIUDERE), rs.getInt(PRINCIPALE) == 1);
			}

			return retVal;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero dell'ultima richiesta integrazione per il documento identificato da: " + idDocIngresso, e);
			throw new RedException("Errore durante il recupero dell'ultima richiesta integrazione per il documento identificato da: " + idDocIngresso, e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IAllaccioDAO#getLastRichiestaVistoOsservazione(int,
	 *      java.sql.Connection).
	 */
	@Override
	public RispostaAllaccioDTO getLastRichiestaVistoOsservazione(final int idDocIngresso, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		RispostaAllaccioDTO retVal = null;
		try {
			int index = 1;
			ps = con.prepareStatement("SELECT * FROM ALLACCIO WHERE iddocumentoallacciato = ? AND principale = ? AND idtipoallaccio in (?, ?) ORDER BY dataallaccio DESC");

			ps.setInt(index++, idDocIngresso);
			ps.setInt(index++, 1); // Principale
			ps.setInt(index++, TipoAllaccioEnum.OSSERVAZIONE.getTipoAllaccioId());
			ps.setInt(index++, TipoAllaccioEnum.VISTO.getTipoAllaccioId());
			
			rs = ps.executeQuery();
			if (rs.next()) {
				retVal = new RispostaAllaccioDTO(rs.getInt(IDDOCUMENTO), rs.getString(IDDOCUMENTOALLACCIATO), rs.getInt(IDTIPOALLACCIO),
						rs.getInt(DOCUMENTODACHIUDERE), rs.getInt(PRINCIPALE) == 1);
			}

			return retVal;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero dell'ultima richiesta di visto o osservazione per il documento identificato da: " + idDocIngresso, e);
			throw new RedException("Errore durante il recupero dell'ultima richiesta di visto o osservazione per il documento identificato da: " + idDocIngresso, e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IAllaccioDAO#getAllaccioPrincipale(int,
	 *      java.sql.Connection).
	 */
	@Override
	public RispostaAllaccioDTO getAllaccioPrincipale(final int idDocUscita, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		RispostaAllaccioDTO retVal = null;
		try {
			int index = 1;
			ps = con.prepareStatement(SELECT_STAR + FROM_ALLACCIO + " WHERE iddocumento = ? " + "   AND principale = 1 " + "  ORDER BY dataallaccio desc ");

			ps.setInt(index++, idDocUscita);
			rs = ps.executeQuery();

			if (rs.next()) {
				retVal = new RispostaAllaccioDTO(rs.getInt(IDDOCUMENTO), rs.getString(IDDOCUMENTOALLACCIATO), rs.getInt(IDTIPOALLACCIO),
						rs.getInt(DOCUMENTODACHIUDERE), rs.getInt(PRINCIPALE) == 1);
			}

			return retVal;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero dell'allaccio principale.", e);
			throw new RedException("Errore durante il recupero dell'allaccio principale.", e);
		} finally {
			closeStatement(ps, rs);
		}
	}
	
	/**
	 * Restituisce la lista degli allacci identificati da un document title e dalla
	 * tipologia della notifica.
	 * 
	 * @param documentTitle
	 *            Document title dell'allaccio, non può essere {@code null}.
	 * @param tipoAllaccio
	 *            Tipo allaccio, non può essere {@code null}.
	 * @param connection
	 *            Connessione al db.
	 * @return Lista di RispostaAllaccio, vuota se non esiste alcun allaccio
	 *         identificato dal document title e dal tipo specificato.
	 */
	@Override
	public List<RispostaAllaccioDTO> getAllacciOfType(final String documentTitle, final TipoAllaccioEnum tipoAllaccio, final Connection connection) {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int index = 1;
			
			String query = "SELECT * FROM ALLACCIO WHERE IDDOCUMENTO = ? AND IDTIPOALLACCIO = ? ORDER BY DATAALLACCIO ASC";
			ps = connection.prepareStatement(query);
			
			ps.setString(index++, documentTitle);
			ps.setInt(index++, tipoAllaccio.getTipoAllaccioId());
			rs = ps.executeQuery();

			return getAllacciFromResultset(rs);
		} catch (final Exception e) {
			LOGGER.error("Errore riscontrato durante il recupero degli allacci di tipo: " + tipoAllaccio, e);
			throw new RedException("Errore riscontrato durante il recupero degli allacci di tipo: " + tipoAllaccio, e);
		} finally {
			closeStatement(ps, rs);
		}
	}
	

}