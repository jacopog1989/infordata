package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.Date;

import it.ibm.red.business.enums.StatoElabNotificaNpsEnum;

/**
 * The Class NotificaNpsDTO.
 *
 * @author a.dilegge
 * 
 * 	DTO per le notifiche di NPS.
 */
public class NotificaNpsDTO extends AbstractDTO implements Serializable {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 4142282957308392793L;
	
	/**
	 * Identificativo coda
	 */
	private Long idCoda;
	
	/**
	 * Identificativo messaggio NPS
	 */
	private final String messageIdNPS;
	
	/**
	 * Identificativo messaggio NPS
	 */
	private final String idMessaggioNPS;
	
	/**
	 * Codice aoo nps
	 */
	private final String codiceAooNPS;
	
	/**
	 * Numero protocollo NPs
	 */
	private final Integer numeroProtocollo;
	
	/**
	 * Data protocollo nps
	 */
	private final Date dataProtocollo;
	
	/**
	 * Anno protocollo NPs
	 */
	private final Integer annoProtocollo;
	
	/**
	 * ID protocollo
	 */
	private final String idProtocollo;
	
	/**
	 * Tipo protocollo
	 */
	private final Integer tipoProtocollo;
	
	/**
	 * Data notifica NPS
	 */
	private final Date dataNotifica;
	
	/**
	 * Tipo notifica NPS
	 */
	private final String tipoNotifica;
	
	/**
	 * Identificativo Aoo
	 */
	private Long idAoo;
	
	/**
	 * Data ricezione notifica
	 */
	private Date dataRicezioneNotifica;
	
	/**
	 * Enum che specifica lo stato di elaborazione di una notifica
	 */
	private StatoElabNotificaNpsEnum stato;
	
	/**
	 * Errore
	 */
	private String errore;
	
	
	/**
	 * Costruttore notifica nps DTO.
	 *
	 * @param messageIdNPS the message id NPS
	 * @param idMessaggioNPS the id messaggio NPS
	 * @param codiceAooNPS the codice aoo NPS
	 * @param dataProtocollo the data protocollo
	 * @param numeroProtocollo the numero protocollo
	 * @param annoProtocollo the anno protocollo
	 * @param idProtocollo the id protocollo
	 * @param tipoProtocollo the tipo protocollo
	 * @param dataNotifica the data notifica
	 * @param tipoNotifica the tipo notifica
	 */
	public NotificaNpsDTO(final String messageIdNPS, final String idMessaggioNPS, final String codiceAooNPS, final Date dataProtocollo, 
			final Integer numeroProtocollo, final Integer annoProtocollo, final String idProtocollo, final Integer tipoProtocollo, final Date dataNotifica, 
			final String tipoNotifica) {
		super();
		this.messageIdNPS = messageIdNPS;
		this.idMessaggioNPS = idMessaggioNPS;
		this.codiceAooNPS = codiceAooNPS;
		this.numeroProtocollo = numeroProtocollo;
		this.dataProtocollo = dataProtocollo;
		this.annoProtocollo = annoProtocollo;
		this.idProtocollo = idProtocollo;
		this.tipoProtocollo = tipoProtocollo;
		this.dataNotifica = dataNotifica;
		this.tipoNotifica = tipoNotifica;
	}
	
	
	/**
	 * Costruttore notifica nps DTO.
	 *
	 * @param idCoda
	 *            the id coda
	 * @param messageIdNPS
	 *            the message id NPS
	 * @param idDocumentoNPS
	 *            the id documento NPS
	 * @param codiceAooNPS
	 *            the codice aoo NPS
	 * @param dataProtocollo
	 *            the data protocollo
	 * @param numeroProtocollo
	 *            the numero protocollo
	 * @param annoProtocollo
	 *            the anno protocollo
	 * @param idProtocollo
	 *            the id protocollo
	 * @param tipoProtocollo
	 *            the tipo protocollo
	 * @param dataNotifica
	 *            the data notifica
	 * @param tipoNotifica
	 *            the tipo notifica
	 * @param idAoo
	 *            the id aoo
	 * @param dataRicezioneNotifica
	 *            the data ricezione notifica
	 * @param stato
	 *            the stato
	 * @param errore
	 *            the errore
	 */
	public NotificaNpsDTO(final Long idCoda, final String messageIdNPS, final String idDocumentoNPS, final String codiceAooNPS,
			final Date dataProtocollo, final int numeroProtocollo, final int annoProtocollo, final String idProtocollo, final int tipoProtocollo,
			final Date dataNotifica, final String tipoNotifica,  final Long idAoo, final Date dataRicezioneNotifica,
			final StatoElabNotificaNpsEnum stato, final String errore) {
		this(messageIdNPS, idDocumentoNPS, codiceAooNPS, dataProtocollo, numeroProtocollo, annoProtocollo, 
				idProtocollo, tipoProtocollo, dataNotifica, tipoNotifica);
		this.idCoda = idCoda;
		this.idAoo = idAoo;
		this.dataRicezioneNotifica = dataRicezioneNotifica;
		this.stato = stato;
		this.errore = errore;
	}

	/** 
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/** 
	 *
	 * @return the id coda
	 */
	public Long getIdCoda() {
		return idCoda;
	}

	/** 
	 *
	 * @return the message id NPS
	 */
	public String getMessageIdNPS() {
		return messageIdNPS;
	}

	/** 
	 *
	 * @return the id messaggio NPS
	 */
	public String getIdMessaggioNPS() {
		return idMessaggioNPS;
	}

	/** 
	 *
	 * @return the codice aoo NPS
	 */
	public String getCodiceAooNPS() {
		return codiceAooNPS;
	}

	/** 
	 *
	 * @return the numero protocollo
	 */
	public Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/** 
	 *
	 * @return the data protocollo
	 */
	public Date getDataProtocollo() {
		return dataProtocollo;
	}

	/** 
	 *
	 * @return the anno protocollo
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/** 
	 *
	 * @return the id protocollo
	 */
	public String getIdProtocollo() {
		return idProtocollo;
	}

	/** 
	 *
	 * @return the tipo protocollo
	 */
	public Integer getTipoProtocollo() {
		return tipoProtocollo;
	}

	/** 
	 *
	 * @return the data notifica
	 */
	public Date getDataNotifica() {
		return dataNotifica;
	}

	/** 
	 *
	 * @return the tipo notifica
	 */
	public String getTipoNotifica() {
		return tipoNotifica;
	}

	/** 
	 *
	 * @return the id aoo
	 */
	public Long getIdAoo() {
		return idAoo;
	}

	/** 
	 *
	 * @param idAoo the new id aoo
	 */
	public void setIdAoo(final Long idAoo) {
		this.idAoo = idAoo;
	}

	/** 
	 *
	 * @return the data ricezione notifica
	 */
	public Date getDataRicezioneNotifica() {
		return dataRicezioneNotifica;
	}

	/** 
	 *
	 * @return the stato
	 */
	public StatoElabNotificaNpsEnum getStato() {
		return stato;
	}

	/** 
	 *
	 * @return the errore
	 */
	public String getErrore() {
		return errore;
	}
	
}