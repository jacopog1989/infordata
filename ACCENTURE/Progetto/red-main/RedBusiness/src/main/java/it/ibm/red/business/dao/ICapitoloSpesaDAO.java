package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.CapitoloSpesaDTO;
import it.ibm.red.business.dto.RicercaCapitoloSpesaDTO;

/**
 * Interfaccia DAO per la gestione dei capitoli di spesa.
 * 
 * @author mcrescentini
 */
public interface ICapitoloSpesaDAO extends Serializable {

	/**
	 * Restituisce il capitolo spesa identificato dal {@code codice}.
	 * 
	 * @param codice
	 *            Codice del capitolo spesa da recuperare.
	 * @param con
	 *            Connessione al database.
	 * @return Capitolo spesa recuperato.
	 */
	CapitoloSpesaDTO getByCodice(String codice, Connection con);

	/**
	 * Effettua l'inserimento di un nuovo capitolo di spesa con le informazioni
	 * passate come parametro.
	 * 
	 * @param codice
	 *            Codice del capitolo spesa da inserire.
	 * @param descrizione
	 *            Descrizione del capitolo spesa da inserire.
	 * @param annoFinanziario
	 *            Anno finanziario a cui si riferisce il capitolo spesa.
	 * @param flagSentenza
	 *            Flag associato alla sentenza del capitolo spesa in fase di
	 *            inserimento.
	 * @param con
	 *            Connessione al database.
	 */
	void inserisci(String codice, String descrizione, Integer annoFinanziario, Boolean flagSentenza, Connection con);

	/**
	 * Effettua l'aggiornamento del capitolo spesa identificato dal {@code codice}.
	 * .
	 * 
	 * @param codice
	 *            Codice del capitolo spesa da aggiornare.
	 * @param descrizione
	 *            Descrizione da impostare sul capitolo spesa da aggiornare.
	 * @param annoFinanziario
	 *            Anno finanziario da impostare sul capitolo spesa da aggiornare.
	 * @param flagSentenza
	 *            Flag sentenza da impostare sul capitolo spesa da aggiornare.
	 * @param con
	 *            Connessione al database.
	 */
	void aggiorna(String codice, String descrizione, Integer annoFinanziario, Boolean flagSentenza, Connection con);

	/**
	 * Effettua un insert multipla dei capitoli spesa passati come lista.
	 * 
	 * @param capitoliSpesa
	 *            Lista dei capitoli di spesa da inserire.
	 * @param con
	 *            Connessione al database.
	 */
	void inserisciMultipli(List<CapitoloSpesaDTO> capitoliSpesa, Connection con);

	/**
	 * Effettua l'inserimento delle relazioni tra capitoli di spesa e aree
	 * organizzative popolando una tabella di cross tra le due entità.
	 * 
	 * @param codiciCapitoliSpesa
	 *            Codici dei capitoli di spesa da associare all'area organizzativa
	 *            identificata dall' {@code idAoo}.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa a a cui associare i
	 *            capitoli di spesa.
	 * @param con
	 *            Connessione al database.
	 */
	void inserisciAssociazioniAoo(List<String> codiciCapitoliSpesa, Long idAoo, Connection con);

	/**
	 * Effettua l'eliminazione delle associazioni dei capitoli di spesa identificati
	 * dai {@code codiciCapitoliSpesa} con l'area organizzativa identificata dall'
	 * {@code idAoo}.
	 * 
	 * @param codiciCapitoliSpesa
	 *            Codici dei capitoli di spesa per cui rimuovere le associazioni con
	 *            l'AOO.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa da cui rimuovere le
	 *            associazioni.
	 * @param con
	 *            Connessione al database.
	 */
	void eliminaAssociazioniAoo(List<String> codiciCapitoliSpesa, Long idAoo, Connection con);

	/**
	 * Esegue la ricerca dei capitoli di spesa che rispettano i parametri definiti
	 * dal DTO di ricerca.
	 * 
	 * @param con
	 *            Connessione al database.
	 * @param ricercaDTO
	 *            Parametri con cui effettuare la ricerca.
	 * @return Collezione dei capitoli di spesa recuperati dalla ricerca.
	 */
	Collection<CapitoloSpesaDTO> ricerca(Connection con, RicercaCapitoloSpesaDTO ricercaDTO);

}
