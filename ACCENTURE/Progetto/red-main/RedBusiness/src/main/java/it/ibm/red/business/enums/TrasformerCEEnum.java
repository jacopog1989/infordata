package it.ibm.red.business.enums;

/**
 * The Enum TrasformerCEEnum.
 *
 * @author CPIERASC
 * 
 *         Enum per la gestione delle trasformazioni degli oggetti del CE.
 */
public enum TrasformerCEEnum {
	
	/**
	 * Trasformazione da documento a master.
	 */
	FROM_DOCUMENTO_TO_MASTER,

	/**
	 * Trasformazione master con contesto.
	 */
	FROM_DOCUMENTO_TO_MASTER_CONTEXT,
	/**
	 * Trasformazione da documento a dettaglio di un master del libro firma.
	 */
	FROM_DOCUMENTO_TO_DETAIL, 

	
	/**
	 * Trasformazione da un documento CE al DetailDocumentRedDTO.
	 */
	FROM_DOCUMENTO_TO_DETAIL_RED, 

	/**
	 * Trasformazione da un documento CE alLA VERSIONE LIGHT di DetailDocumentRedDTO.
	 * associa solo alcuni campi
	 */
	FROM_DOCUMENTO_TO_LIGHT_DETAIL_RED, 
	
	/**
	 * Trasformazione da documento a fascicolo di un dettaglio del libro firma.
	 */
	FROM_DOCUMENTO_TO_FASCICOLO, 

	/**
	 * Trasformazione da documento a documento allegato ad un fascicolo fascicolo di un dettaglio del libro firma.
	 */
	FROM_DOCUMENTO_TO_DOCUMENTO_FASCICOLO,
	
	/**
	 * Trasformazione da documento a dettaglio di un master della mail.
	 */
	FROM_DOCUMENTO_TO_MAIL_DETAIL,
	
	/**
	 * Trasformazione da documento a dettaglio di un master della mail.
	 */
	FROM_DOCUMENTO_TO_MAIL_MASTER,
	
	/**
	 * Trasformazione da documento a dettaglio di un master della mail anche con il content.
	 */
	FROM_DOCUMENTO_TO_MAIL_MASTER_WITH_CONTENT,
	
	/**
	 * Trasformazione da documentoa allegato della mail.
	 */
	FROM_DOCUMENTO_TO_ALLEGATO_MAIL,
	
	/**
	 * Trasformazione da documento ad allegato della mail. Il contenuto delle email in byte non viene scartato.
	 */
	FROM_DOCUMENTO_TO_ALLEGATO_CONTENT_MAIL,
	
	/**
	 * Trasformazione da documentoa allegato della mail. Il contenuto delle email in byte non deve essere caricato. 
	 */
	FROM_DOCUMENTO_TO_ALLEGATO_DETAIL_MAIL,

	/**
	 * Trasformazione da documento a DTO per la modifica iter.
	 */
	MODIFICA_ITER,
	
	/**
	 * Trasformazione da documento a DTO generico per servire le code di RED.
	 */
	FROM_DOCUMENTO_TO_GENERIC_DOC,


	/**
	 * Da docuennt to documento generico con contesto.
	 */
	FROM_DOCUMENTO_TO_GENERIC_DOC_CONTEXT,
	
	/**
	 * Trasformazione da documento a DTO generico rappresentante un allegato per la ricerca avanzata full-text.
	 */
	FROM_DOCUMENTO_TO_ALLEGATO_RICERCA_AVANZATA_CONTEXT,
	
	/**
	 * Trasformazione del document di ALLEGATO_NSD ad AllegatoDTO .
	 */
	FROM_DOCUMENT_TO_ALLEGATO_CONTEXT,
	
	/**
	 * Trasformazione del document in faldone (Faldone -> FaldoneDTO)  .
	 */
	FROM_DOCUMENT_TO_FALDONE,
	

	/**
	 * Da Document to content applet.
	 */
	FROM_DOCUMENTO_TO_APPLET_CONTENT,

	/**
	 * Da documento to faldone.
	 */
	FROM_DOCUMENT_TO_FALDONE_TREE,
	
	/**
	 * Trasformazione da documento filenet a VersioneDTO del documento stesso .
	 */
	FROM_DOCUMENT_TO_RED_VERSION, 
	
	/**
	 * Trasformazione del Document nei metadati necessari per modificarne la sicurezza.
	 */
	FROM_DOCUMENT_TO_DETAIL_ASSEGNAZIONE,
	
	/**
	 * Aggiunge le informzioni necessarie per modificare la sicurezza del coordinatore.
	 */
	FROM_DOCUMENT_TO_DETAIL_ASSEGNAZIONE_COORDINATORE,
	
	/**
	 * Trasforma il document in Fattura.
	 */
	FROM_DOCUMENT_TO_FATTURA,
	
	/**
	 * Trasforma il document in DD.
	 */
	FROM_DOCUMENTO_TO_DD,
	
	/**
	 * Trasformazione da Document di Filenet a DTO del Registro Protocollo.
	 */
	FROM_DOCUMENTO_TO_REGISTRO_PROTOCOLLO_PMEF,
	
	/**
	 * Trasformazione da Document di Filenet a DTO SIGI.
	 */
	FROM_DOCUMENTO_TO_DOC_SIGI,
	
	/**
	 * Trasformazione per tutti gli id del documento utili all'applicativo.
	 */
	FROM_DOCUMENTO_TO_ID_DOCUMENTO,
	
	/**
	 * Trasformazione per il dettaglio corrente di un documento 
	 * quando si esegue l'aggiornamento di quest'ultimo.
	 */
	FROM_DOCUMENTO_TO_UPDATE_DETAIL_RED,
	
	/**
	 * Restituisce un DetailDocumentRedDTO valorizzato opportunamente per essere verificato
	 * come riferimento di protocollo in entrata.
	 */
	FROM_DOCUMENTO_TO_PROTOCOLLO_DA_VERIFICARE,
	
	/**
	 * Trasformazione da Document di FileNet a MasterRedDTO di un documento cartaceo.
	 */
	FROM_DOCUMENTO_TO_MASTER_CARTACEO,
 
 	/**
	 * Trasformazione da Document di FileNet a DocumentoWsDTO.
	 */
	FROM_DOCUMENT_TO_DOC_WS,
	
	/**
	 * Trasformazione da Document di FileNet a DocumentoWsDTO (e-mail).
	 */
	FROM_DOCUMENT_TO_DOC_EMAIL_WS,
	
	/**
	 * Trasformazione da Document di FileNet a FascicoloWsDTO.
	 */
	FROM_DOCUMENT_TO_FASCICOLO_WS,
	/**
	 * Trasformer utilizzato per la ricerca delle mail.
	 */
	FROM_DOCUMENTO_TO_MAIL_RICERCA,
	

	/**
	 * From document to associa integrazione dati con contesto.
	 */
	FROM_DOCUMENTO_TO_ASSOCIA_INTEGRAZIONE_DATI_CONTEXT;
}
