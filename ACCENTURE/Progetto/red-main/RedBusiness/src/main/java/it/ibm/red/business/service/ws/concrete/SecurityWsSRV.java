package it.ibm.red.business.service.ws.concrete;

import java.sql.Connection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IRedWsClientDAO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.concrete.AbstractService;
import it.ibm.red.business.service.ws.ISecurityWsSRV;
import it.ibm.red.webservice.model.documentservice.dto.Servizio;

/**
 * The Class SecurityWsSRV.
 *
 * @author a.dilegge
 * 
 *         Servizio gestione security.
 */
@Service
public class SecurityWsSRV extends AbstractService implements ISecurityWsSRV {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -2370909204921499965L;
	
	/**
	 * DAO.
	 */
	@Autowired
	private IRedWsClientDAO redWsClientDAO;
	
	/**
	 * DAO.
	 */
	@Autowired
	private INodoDAO nodoDAO;
	
	/**
	 * Servizio.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;
	
	/**
	 * @see it.ibm.red.business.service.ws.ISecurityWsSRV#autorizzaClient(it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.dto.Servizio,
	 *      java.sql.Connection).
	 */
	@Override
	public void autorizzaClient(final RedWsClient client, final Servizio servizio, final Connection con) {
		
		final RedWsClient clientDB = redWsClientDAO.getById(client.getIdClient(), con);
		
		if (clientDB == null) {
			client.setAuthorized(false);
			client.setAuthorizationErrorMessage("Client sconosciuto");
		} else {
			if (!clientDB.getPwdClient().equals(client.getPwdClient())) {
				client.setAuthorized(false);
				client.setAuthorizationErrorMessage("Password del client errata");
			} else {
			
				Long idAooCalcolata = null;
				
				client.setServiziAbilitati(clientDB.getServiziAbilitati());
				
				if (!client.getServiziAbilitati().contains(servizio.getDescription())) {
					client.setAuthorized(false);
					client.setAuthorizationErrorMessage("Il client non è abilitato al servizio richiesto: " + servizio.getDescription());
				} else {
					
					client.setAuthorized(true);
					client.setAooCompetenti(clientDB.getAooCompetenti());
					client.setNodiCompetenti(clientDB.getNodiCompetenti());
					
					if (client.getIdAoo() != null && client.getIdAoo() != 0) {
						if (!client.getAooCompetenti().contains(client.getIdAoo())) {
							client.setAuthorized(false);
							client.setAuthorizationErrorMessage("Il client non è competente per l'Aoo " + client.getIdAoo());
						} else {
							idAooCalcolata = client.getIdAoo();
						}
					}
					
					if (client.getIdNodo() != null && client.getIdNodo() != 0) {
						if (!client.getNodiCompetenti().contains(client.getIdNodo())) {
							client.setAuthorized(false);
							client.setAuthorizationErrorMessage("Il client non è competente per l'ufficio " + client.getIdNodo());
						} else {
							final Nodo nodo = nodoDAO.getNodo(client.getIdNodo(), con);
							idAooCalcolata = nodo.getAoo().getIdAoo();
						}
					}
					
					if (client.getIdUtente() != null && client.getIdUtente() != 0) {
						final UtenteDTO utente = utenteSRV.getById(client.getIdUtente(), con);
						if (!client.getNodiCompetenti().contains(utente.getIdUfficio())) {
							client.setAuthorized(false);
							client.setAuthorizationErrorMessage("Il client non è competente per l'ufficio di default dell'utente " + client.getIdUtente());
						} else {
							final Nodo nodo = nodoDAO.getNodo(utente.getIdUfficio(), con);
							idAooCalcolata = nodo.getAoo().getIdAoo();
						}
					}
					
					client.setIdAoo(idAooCalcolata);
					
				}
			}
		}
		
	}
	
}