package it.ibm.red.business.dto.filenet;

import java.util.Map;

import it.ibm.red.business.dto.AbstractDTO;

/**
 * DTO che definisce un fascicolo RED Fn.
 */
public class FaldoneRedFnDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -1815158916832852768L;

	/**
	 * Nome faldone.
	 */
	private String nomeFaldone;
	
	/**
	 * Classe documentale.
	 */
	private String classeDocumentale;
		
	/**
	 * Metadati.
	 */
	private transient Map<String, Object> metadati;
	
	/**
	 * Restituisce il nome del faldone.
	 * @return nome faldone
	 */
	public String getNomeFaldone() {
		return nomeFaldone;
	}

	/**
	 * Imposta il nome del faldone.
	 * @param nomeFaldone
	 */
	public void setNomeFaldone(final String nomeFaldone) {
		this.nomeFaldone = nomeFaldone;
	}

	/**
	 * Restituisce la classe documentale.
	 * @return classe documentale faldone
	 */
	public String getClasseDocumentale() {
		return classeDocumentale;
	}

	/**
	 * Imposta la classe documentale.
	 * @param classeDocumentale
	 */
	public void setClasseDocumentale(final String classeDocumentale) {
		this.classeDocumentale = classeDocumentale;
	}

	/**
	 * Restituisce la Map dei metadati del faldone.
	 * @return metadati faldone
	 */
	public Map<String, Object> getMetadati() {
		return metadati;
	}

	/**
	 * Imposta il metadati.
	 * @param metadati
	 */
	public void setMetadati(final Map<String, Object> metadati) {
		this.metadati = metadati;
	}
}