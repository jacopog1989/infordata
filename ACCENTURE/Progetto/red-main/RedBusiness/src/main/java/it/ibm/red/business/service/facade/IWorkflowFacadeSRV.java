package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;

/**
 * The Interface IWorkflowSRV.
 *
 * @author m.crescentini
 * 
 *         Facade del servizio di gestione dei workflow su DB.
 */
public interface IWorkflowFacadeSRV extends Serializable {

	/**
	 * Verifica se il documento è in coda.
	 * @param utente
	 * @param queue
	 * @param documentTitle
	 * @param idNodoDestinatario
	 * @param idUtenteDestinatario
	 * @param idsTipoAssegnazioni
	 * @return true o false
	 */
	boolean isDocumentInQueue(UtenteDTO utente, DocumentQueueEnum queue, String documentTitle, Long idNodoDestinatario, Long idUtenteDestinatario, List<Long> idsTipoAssegnazioni);
	
	/**
	 * Ottiene l'id del workflow dal procedimento.
	 * @param procedimento
	 * @return id del workflow
	 */
	int getWorkflowId(TipoProcedimentoDTO procedimento);
}
