package it.ibm.red.business.service.ws;

import java.util.List;

import it.ibm.red.business.service.ws.facade.IStampigliaturaDocWsFacadeSRV;
import it.ibm.red.webservice.model.documentservice.types.documentale.Posizione;

/**
 * The Interface IStampigliaturaDocWsSRV.
 *
 * @author m.crescentini
 * 
 *         Interfaccia servizio stampigliatura documenti web service.
 */
public interface IStampigliaturaDocWsSRV extends IStampigliaturaDocWsFacadeSRV {

	/**
	 * @param content
	 * @param contentType
	 * @param nomeFile
	 * @param protocolloStampigliatura
	 * @param isEntrata
	 * @return
	 */
	byte[] stampigliaProtocollo(byte[] content, String contentType, String nomeFile, String protocolloStampigliatura, boolean isEntrata);

	
	/**
	 * @param content
	 * @param contentType
	 * @param nomeFile
	 * @param postilla
	 * @return
	 */
	byte[] stampigliaPostilla(byte[] content, String contentType, String nomeFile, String postilla);

	
	/**
	 * @param content
	 * @param contentType
	 * @param nomeFile
	 * @param inIdDocumentoStampigliatura
	 * @return
	 */
	byte[] stampigliaIdDocumento(byte[] content, String contentType, String nomeFile, String inIdDocumentoStampigliatura);

	
	/**
	 * @param content
	 * @param contentType
	 * @param nomeFile
	 * @param firmatari
	 * @param altezzaFooter
	 * @param spaziaturaFirma
	 * @return
	 */
	byte[] inserisciCampoFirma(byte[] content, String contentType, String nomeFile, List<Integer> firmatari, Integer altezzaFooter, Integer spaziaturaFirma);

	
	/**
	 * @param content
	 * @param contentType
	 * @param testoApprovazione
	 * @param posizione
	 * @return
	 */
	byte[] stampigliaApprovazione(byte[] content, String contentType, String testoApprovazione, Posizione posizione);

	
	/**
	 * @param content
	 * @param testoWatermark
	 * @return
	 */
	byte[] stampigliaWatermark(byte[] content, String testoWatermark);


	/**
	 * Metodo per la stampigliatura della postilla.
	 * @param idAOORED
	 * @param content
	 * @param contentType
	 * @param nomeFile
	 * @param inPostilla
	 * @return stampigliatura
	 */
	byte[] stampigliaPostilla(Integer idAOORED, byte[] content, String contentType,
			String nomeFile, String inPostilla);

	/**
	 * Metodo per la stampigliatura del protocollo.
	 * @param idAOORED
	 * @param content
	 * @param contentType
	 * @param nomeFile
	 * @param protocolloStampigliatura
	 * @param isEntrata
	 * @return stampigliatura
	 */
	byte[] stampigliaProtocollo(Integer idAOORED, byte[] content, String contentType, String nomeFile,
			String protocolloStampigliatura, boolean isEntrata);
	
}