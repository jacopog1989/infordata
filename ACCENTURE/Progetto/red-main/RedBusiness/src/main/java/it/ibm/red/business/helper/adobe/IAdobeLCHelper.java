package it.ibm.red.business.helper.adobe;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

import org.apache.commons.fileupload.FileItem;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AttachmentDTO;
import it.ibm.red.business.helper.adobe.dto.ConversioneOutputDTO;
import it.ibm.red.business.helper.adobe.dto.CountSignsOutputDTO;
import it.ibm.red.business.helper.adobe.dto.approvazioni.FormType;

/**
 * The Interface IAdobeLCHelper.
 *
 * @author CPIERASC
 * 
 *         Oggetto per la gestione di Asobe LiveCycle
 */
public interface IAdobeLCHelper extends Serializable {
	
	/**
	 * Mwtodo per la predisposizione per la firma digitale.
	 * 
	 * @param siglaVisto1		sigla visto 1
	 * @param siglaVisto2		sigla visto 2
	 * @param idDocumento		identificativo documento
	 * @param protocollo		protocollo
	 * @param postilla			postilla
	 * @param content			content
	 * @return					content modificato
	 */
	byte[] predisposizioneFirmaDigitale(String siglaVisto1, String siglaVisto2, String idDocumento, String protocollo, String postilla, byte[] content);
	
	/**
	 * Predispone ed effettua la conversione in PDF del doc in input secondo
	 * i parametri inviati in firma al metodo
	 * 
	 * @param doc
	 * @param preview se true, viene generata anche la preview
	 * @param protocollo la stringa di protocollo da stampigliare sul documento
	 * @param altezzaFooter
	 * @param spaziaturaFirma
	 * @param commaSeparatedIdFirmatari
	 * @param numeroDocumento
	 * @param documentTitle
	 * 
	 * @return il documento convertito e, in caso preview sia true, anche la preview
	 * 
	 * @throws AdobeException
	 */
	ConversioneOutputDTO convertiInPdf(FileItem doc, boolean preview, String protocollo, Integer altezzaFooter, Integer spaziaturaFirma, 
			String commaSeparatedIdFirmatari, Integer numeroDocumento, String documentTitle);
	
	/**
	 * Metodo che conta i campi firma e le firme del documento in input
	 * 
	 * @param document
	 * @return
	 * @throws AdobeException
	 */
	CountSignsOutputDTO countsSignedFields(InputStream document);
	
	/**
	 * Metodo che ricerca i tag firmatari sul doc in input.
	 * 
	 * @param doc
	 * @return
	 */
	int hasTagFirmatario(FileItem doc) throws IOException;
	
	/**
	 * @param doc
	 * @return
	 */
	int hasTagFirmatario(AllegatoDTO doc);
	
	/**
	 * Metodo che converte un byte array che codifica codice HTML in un documento PDF.
	 * 
	 * @param htmlContent
	 * @return
	 * @throws AdobeException
	 */
	InputStream htmlToPdf(byte[] htmlContent);

	/**
	 * @param accessContentStream
	 * @param string
	 * @return
	 */
	InputStream insertWatermark(InputStream accessContentStream, String string);
	
	/**
	 * @param documento
	 * @param testo
	 * @return
	 */
	byte[] insertWatermark(byte[] documento, String testo);

	/**
	 * Genera il pdf delle approvazioni da stampigliare
	 * 
	 * @param codiceAoo dell'utente che esegue l'operazione serve a selezionare la corretta operazione
	 * @param form un oggetto utilizzato dal servizio che contiene tutte i necessari input
	 * @return 	Un oggetto con il titolo del file
	 */
	InputStream generateApprovazioniPdf(String codiceAoo, FormType form);
	
	/**
	 * Stampiglia la postilla per attestare per copia conforme
	 * 
	 * @param reason
	 * @param content
	 * @return
	 */
	byte[] insertPostillaCopiaConforme(String reason, byte[] content);

	/**
	 * Richiama il processo Adobe nativo per la trasformazione in PDF.
	 * 
	 * @param fileName
	 * @param content
	 * @param flagPDFA
	 * @return
	 */
	byte[] trasformazionePDF(String fileName, byte[] content, boolean flagPDFA, String documentTitle);

	/**
	 * Inserisce un nuovo campo firma.
	 * 
	 * @param content
	 * @param contentType
	 * @param nomeFile
	 * @param firmatari
	 * @param altezzaFooter
	 * @param spaziaturaFirma
	 * @return
	 */
	byte[] insertCampoFirma(byte[] content, String contentType, String nomeFile, List<Integer> firmatari, Integer altezzaFooter, Integer spaziaturaFirma);

	/**
	 * @param contentOriginale
	 * @param contentType
	 * @param testoApprovazione
	 * @param toRight
	 * @return
	 */
	byte[] insertApprovazione(byte[] contentOriginale, String contentType, String testoApprovazione, boolean toRight);

	/**
	 * @param contentOriginale
	 * @param contentType
	 * @param nomeFile
	 * @param idDocumento
	 * @return
	 */
	byte[] insertIdDocumento(byte[] contentOriginale, String contentType, String nomeFile, Integer idDocumento);

	/**
	 * @param contentOriginale
	 * @param contentType
	 * @param nomeFile
	 * @param protocollo
	 * @return
	 */
	byte[] insertProtocollo(byte[] contentOriginale, String contentType, String nomeFile, String protocollo);

	/**
	 * @param contentOriginale
	 * @param contentType
	 * @param nomeFile
	 * @param postilla
	 * @return
	 */
	byte[] insertPostilla(byte[] contentOriginale, String contentType, String nomeFile, String postilla);

	/**
	 * M.C.
	 * Inserisce un campo firma richiamando lo stesso processo Adobe usato da FWS.
	 * Il campo firma NON viene inserito secondo i criteri di RED (e.g. in alto a dx, sopra il testo), 
	 * pertando questo metodo non dovrebbe essere utilizzato (ma non si sa mai).
	 * 
	 * @param content
	 * @param firmatari
	 * @param altezzaFooter
	 * @param spaziaturaFirma
	 * @return
	 */
	byte[] insertCampoFirmaFws(byte[] content, List<Integer> firmatari, Integer altezzaFooter, Integer spaziaturaFirma);

	/**
	 * Ricerca i tag firmatari sul doc in input.
	 * @param doc documento allegato
	 * @return
	 */
	int hasTagFirmatario(AttachmentDTO doc);

	/**
	 * Inserisce la postilla con il processo adobe LC.
	 * 
	 * @param idDocumento
	 * @param postilla
	 * @param content
	 * 
	 * @return il file modificato
	 * 
	 */
	byte[] predisposizionePostilla(String idDocumento, String postilla, byte[] content);

}
