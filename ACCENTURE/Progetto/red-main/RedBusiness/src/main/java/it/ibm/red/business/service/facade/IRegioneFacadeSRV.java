package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.RegioneDTO;

/**
 * Facade del servizio che gestisce le regioni.
 */
public interface IRegioneFacadeSRV extends Serializable {
	
	/**
	 * Ottiene tutte le regioni.
	 * @return lista di regioni
	 */
	List<RegioneDTO> getAll();
	
	/**
	 * Ottiene le regioni tramite query.
	 * @param query
	 * @return lista di regioni
	 */
	List<RegioneDTO> getRegioni(String query);

	/**
	 * Ottiene la regione tramite id.
	 * @param idRegione
	 * @return regione
	 */
	RegioneDTO get(Long idRegione);
	
}
