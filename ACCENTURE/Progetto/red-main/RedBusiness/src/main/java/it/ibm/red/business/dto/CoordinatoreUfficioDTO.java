package it.ibm.red.business.dto;

/**
 * 
 * Coordinatore ufficio DTO.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public class CoordinatoreUfficioDTO extends AbstractDTO {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificativo.
	 */
	private Integer id;
	
	/**
	 * Nome.
	 */
	private String nome;
	
	/**
	 * Cognome.
	 */
	private String cognome;
	
	/**
	 * Costruttore.
	 * 
	 * @param inId		identificativo
	 * @param inNome	nome
	 * @param inCognome	cognome
	 */
	public CoordinatoreUfficioDTO(final Integer inId, final String inNome, final String inCognome) {
		super();
		this.id = inId;
		this.nome = inNome;
		this.cognome = inCognome;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Getter.
	 * 
	 * @return	nome
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	cognome
	 */
	public String getCognome() {
		return cognome;
	}

}
