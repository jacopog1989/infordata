package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.collection.PageIterator;
import com.filenet.api.collection.PageMark;
import com.filenet.api.core.Document;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dao.ILookupDAO;
import it.ibm.red.business.dao.IRegistroRepertorioDAO;
import it.ibm.red.business.dao.IStampigliaturaSegnoGraficoDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dto.GestioneLockDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.PEDocumentoDTO;
import it.ibm.red.business.dto.RegistroRepertorioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.ContextTrasformerPEEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.trasform.TrasformPE;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IEventoLogSRV;
import it.ibm.red.business.service.IMasterPaginatiSRV;
import it.ibm.red.business.service.IRegistroRepertorioSRV;
import it.ibm.red.business.service.ISiebelSRV;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Service master paginati.
 */
@Service
public class MasterPaginatiSRV extends AbstractService implements IMasterPaginatiSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 1161340760950361002L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(MasterPaginatiSRV.class.getName());

	/**
	 * Servizio.
	 */
	@Autowired
	private ListaDocumentiSRV listaDocumentiSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private ILookupDAO lookupDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private ISiebelSRV siebelSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IRegistroRepertorioDAO registroRepertorioDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private IRegistroRepertorioSRV registroRepertorioSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IStampigliaturaSegnoGraficoDAO stampigliaturaSiglaDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private IDocumentoFacadeSRV docSRV;
	
	/**
	 * Servizio.
	 */
	@Autowired
	private IEventoLogSRV eventLogSRV;
	

	/******************************************************************************************************************************************
	 * Creazione dell'oggetto che consente l'iterazione per l'accesso alla scrivania
	 * utente e per l'organigramma
	 ******************************************************************************************************************************************/

	/**
	 * Recupera i workflow da una coda filenet.
	 * 
	 * @param queue
	 * @param utente
	 * @param connection
	 * @return
	 */
	private Object getQueryWorkflow(final DocumentQueueEnum queue, final Collection<String> documentTitlesToFilterBy, final UtenteDTO utente, final Connection connection) {
		Object output;

		if (queue.getTextFilter()) {
			// Caso FEPA: potrebbe essere una query sul roster o su queue, questo impone di
			// restituire un Object in quanto rosterquery e queuequery non hanno
			// un padre in comune nell'albero gerarchico.
			output = listaDocumentiSRV.getFepaQueryObject(queue, documentTitlesToFilterBy, utente);
		} else {
			output = listaDocumentiSRV.getQueueFilenetPerPaginazione(queue, utente, documentTitlesToFilterBy, connection);

		}

		return output;
	}

	/**
	 * Dato un insieme di workflow esegue una query paginata sul CE di filenet e
	 * restituisce la struttura atta a gestirlo.
	 * 
	 * @param utente
	 * @param workflows
	 * @return
	 */

	private MasterPageIterator createPaginator(final UtenteDTO utente, final Object query, final DocumentQueueEnum queue) {
		// Caricare tutti i wf da pe
		MasterPageIterator output = null;
		Connection connection = null;
		IFilenetCEHelper fceh = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			output = new MasterPageIterator(query, queue);
			if (CollectionUtils.isEmpty(output.getDocumentTitleSet())) {
				output.setMorePages(false);
			} else {
				final PageIterator piDocuments = fceh.getPageIteratorForMasters(
						PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), output.getDocumentTitleSet(),
						getPageSize(queue));
				output.setPageIterator(piDocuments);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei documenti per popolare i Master: " + e.getMessage(), e);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}

		return output;
	}

	private static Integer getPageSize(final DocumentQueueEnum queue) {
		Integer pageSize = 25;
		PropertiesNameEnum pne = null;
		if (queue != null) {
			pne = queue.getMaxResult();
		}
		if (pne != null) {
			final String strPageSize = PropertiesProvider.getIstance().getParameterByKey(pne);
			if (!StringUtils.isNullOrEmpty(strPageSize)) {
				pageSize = Integer.valueOf(strPageSize);
			}
		}

		return Math.max(0, pageSize);
	}

	/**
	 * Restituisce l'oggetto in grado di gestire la paginazione per le code di un
	 * utente.
	 */
	@Override
	public MasterPageIterator getMastersRaw(final DocumentQueueEnum queue, final Collection<String> documentTitlesToFilterBy, final UtenteDTO utente) {
		MasterPageIterator output = null;
		// caricare tutti i wf da pe
		Object query = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			query = getQueryWorkflow(queue, documentTitlesToFilterBy, utente, connection);
			output = createPaginator(utente, query, queue);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei Documenti per popolare i Master ( " + queue.getName() + " ): " + e.getMessage(), e);
		} finally {
			closeConnection(connection);
		}

		return output;
	}

	/**
	 * Restituisce l'oggetto in grado di gestire la paginazione della scrivania di
	 * uno dei nodi dell'organigramma acceduto da un altro utente.
	 */
	@Override
	public MasterPageIterator getMastersRawUtenteOrganigramma(final UtenteDTO utente, final NodoOrganigrammaDTO nodoOrganigramma, final DocumentQueueEnum... queues) {
		Connection connection = null;
		MasterPageIterator output = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final UtenteDTO datiAccesso = new UtenteDTO(utente.getFcDTO(), nodoOrganigramma);
			final Collection<Object> queries = new ArrayList<>();
			for (final DocumentQueueEnum queue : queues) {
				queries.add(getQueryWorkflow(queue, null, datiAccesso, connection));
			}
			DocumentQueueEnum q = null;
			if (queues != null && queues.length > 0) {
				q = queues[0];
			}
			// La coda serve esclusivamente per determinare la dimensione del paginatore,
			// nel caso di code organigramma
			// si considera l'ultima coda considerata per determinare la size
			output = createPaginator(datiAccesso, queries, q);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei Documenti per popolare i Master: " + e.getMessage(), e);
		} finally {
			closeConnection(connection);
		}

		return output;
	}

	/******************************************************************************************************************************************
	 * Creazione dell'oggetto che consente l'iterazione per l'accesso alla scrivania
	 * utente e per l'organigramma
	 ******************************************************************************************************************************************/

	/**
	 * Il metodo esegue il merge tra un elemento del CE e l'insieme dei workflow as
	 * esso associati (per ogni elemento del PE saranno duplicate le informazioni
	 * del CE).
	 * 
	 * @param utente     utente
	 * @param filteredWF workflow associati all'elemento del CE
	 * @param masterCE   elemento del CE
	 * @param connection connessione al database
	 * @return lista di master (risultato del merge tra le info del CE e del PE)
	 */
	private Collection<MasterDocumentRedDTO> merge(final UtenteDTO utente, final Collection<PEDocumentoDTO> filteredWF, final MasterDocumentRedDTO masterCE,
			final Connection connection) {
		final Collection<MasterDocumentRedDTO> output = new ArrayList<>();

		try {
			for (final PEDocumentoDTO wf : filteredWF) {
				// Se ci sono risultati vanno ciclati e mergiati n volte con il clone
				// predisposto
				final MasterDocumentRedDTO dolly = new MasterDocumentRedDTO(masterCE);
				dolly.fill(wf);
				listaDocumentiSRV.updateIcone(dolly, utente, connection);
				output.add(dolly);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
		}

		return output;
	}

	/**
	 * Metodo per il recupero di una nuova pagina di master.
	 */
	@Override
	public Collection<MasterDocumentRedDTO> refineMasters(final UtenteDTO utente, final MasterPageIterator pagedResult) {
		final Collection<MasterDocumentRedDTO> output = new ArrayList<>();
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			// TX WITH CONTEXT!
			// INIZIO
			final Map<ContextTrasformerCEEnum, Object> context = new EnumMap<>(ContextTrasformerCEEnum.class);

			final Map<Long, String> idsTipoDoc = lookupDAO.getDescTipoDocumento(utente.getIdAoo().intValue(), connection);
			context.put(ContextTrasformerCEEnum.IDENTIFICATIVI_TIPO_DOC, idsTipoDoc);

			final Map<Long, String> tps = tipoProcedimentoDAO.getAllDesc(connection);
			context.put(ContextTrasformerCEEnum.IDENTIFICATIVI_TIPO_PROC, tps);

			context.put(ContextTrasformerCEEnum.GESTISCI_RESPONSE, true);

			final List<String> documentTitleList = new ArrayList<>();
			// ### Gestione Mittente / Destinatari -> START
			final Map<String, String> contattiMap = new HashMap<>();

			final Map<String, RegistroRepertorioDTO> registroRepertorioMap = new HashMap<>();

			final Map<String, String> iconaStampigliaturaMap = new HashMap<>();

			FilenetCEHelper.refreshCredential(utente);

			final PageIterator pi = pagedResult.getPageIterator();
			if (pi != null && pi.nextPage()) {
				final PageMark pm = pi.getPageMark();

				final RegistroRepertorioDTO denominazioneRegistro = registroRepertorioDAO.getDenominazioneRegistroUfficiale(utente.getIdAoo(), connection);

				final Map<Long, List<RegistroRepertorioDTO>> registriRepertorioConfigurati = registroRepertorioSRV.getRegistriRepertorioConfigurati(utente.getIdAoo(),
						connection);

				for (final Object document : pi.getCurrentPage()) {
					final Document doc = (Document) document;
					// Gestione Mittente / Destinatari -> START
					final String mittente = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.MITTENTE_METAKEY);

					if (!StringUtils.isNullOrEmpty(mittente)) {
						final String[] mittenteSplit = mittente.split("\\,");

						if (mittenteSplit.length >= 2 && contattiMap.get(mittenteSplit[0]) == null) {
							contattiMap.put(mittenteSplit[0], mittenteSplit[1]);
						}
					} else {
						final Collection<?> destinatari = (Collection<?>) TrasformerCE.getMetadato(doc, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
						// Si normalizza il metadato per evitare errori comuni durante lo split delle
						// singole stringhe dei destinatari
						final List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(destinatari);

						if (!CollectionUtils.isEmpty(destinatariDocumento)) {

							for (final String[] destSplit : destinatariDocumento) {
								if (destSplit.length >= 5) {
									final TipologiaDestinatarioEnum tde = TipologiaDestinatarioEnum.getByTipologia(destSplit[3]);
									if (TipologiaDestinatarioEnum.INTERNO == tde && "0".equals(destSplit[1])) {
										contattiMap.put(destSplit[0], destSplit[2]);
									} else if (TipologiaDestinatarioEnum.INTERNO == tde && contattiMap.get(destSplit[1]) == null) {
										contattiMap.put(destSplit[1], destSplit[2]);
									} else if (TipologiaDestinatarioEnum.ESTERNO == tde && contattiMap.get(destSplit[0]) == null) {
										contattiMap.put(destSplit[0], destSplit[1]);
									}
								}
							}
						}
					}
					// Gestione Mittente / Destinatari -> END

					// Gestione Siebel -> START
					final String documentTitle = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
					if (!StringUtils.isNullOrEmpty(documentTitle)) {
						documentTitleList.add(documentTitle);
					}

					// Gestione Siebel -> END

					// Gestione stampigliatura segno grafico -> START
					final String placeholder = stampigliaturaSiglaDAO.getPlaceholderByIdDoc(documentTitle, connection);
					if (!StringUtils.isNullOrEmpty(placeholder)) {
						iconaStampigliaturaMap.put(documentTitle, placeholder);
					}
					// Gestione stampigliatura segno grafico -> END
					

					// Gestione Registro Repertorio ->
					final String registroRepertorio = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.REGISTRO_PROTOCOLLO_FN_METAKEY);

					if (registroRepertorio == null) {

						final Integer idTipoDocumento = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
						final Integer idTipoProcedimento = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);

						Boolean isRegistroRepertorioPreProtocollo = Boolean.FALSE;
						if (idTipoDocumento != null && idTipoProcedimento != null) {
							isRegistroRepertorioPreProtocollo = registroRepertorioSRV.checkIsRegistroRepertorio(utente.getIdAoo(), idTipoDocumento.longValue(),
									idTipoProcedimento.longValue(), registriRepertorioConfigurati, connection);
						}

						registroRepertorioMap.put(documentTitle, new RegistroRepertorioDTO(isRegistroRepertorioPreProtocollo, null));

					} else {

						if (registroRepertorio != null && !registroRepertorio.equalsIgnoreCase(denominazioneRegistro.getDescrizioneRegistroRepertorio())) {
							registroRepertorioMap.put(documentTitle, new RegistroRepertorioDTO(true, registroRepertorio));
						} else {
							registroRepertorioMap.put(documentTitle, new RegistroRepertorioDTO(false, denominazioneRegistro.getDescrizioneRegistroRepertorio()));
						}

					}

					// Gestione Registro Repertorio -> END

				}

				pi.reset(pm);
			}
			
			HashMap<String,Integer> mapDocTitleUffCreatore = eventLogSRV.getIdUfficioCreatore(documentTitleList, utente.getIdAoo());
			if (mapDocTitleUffCreatore!=null && !mapDocTitleUffCreatore.isEmpty()) {
				context.put(ContextTrasformerCEEnum.UFF_CREATORE_MAP, mapDocTitleUffCreatore);
			}

			final HashMap<Long, GestioneLockDTO> listaLock = docSRV.getLockDocumentiMaster(documentTitleList, utente.getIdAoo());

			if (listaLock != null && !listaLock.isEmpty()) {
				context.put(ContextTrasformerCEEnum.MAPLOCK, listaLock);
			}

			if (!iconaStampigliaturaMap.isEmpty()) {
				context.put(ContextTrasformerCEEnum.LIBRO_FIRMA_PLACEHOLDER, iconaStampigliaturaMap);
			}
			if (!registroRepertorioMap.isEmpty()) {
				context.put(ContextTrasformerCEEnum.REGISTRO_PROTOCOLLO, registroRepertorioMap);
			}

			if (!contattiMap.isEmpty()) {
				context.put(ContextTrasformerCEEnum.CONTATTI_DISPLAY_NAME, contattiMap);
			}

			final Collection<MasterDocumentRedDTO> mastersCE = TrasformCE.transform(pi, TrasformerCEEnum.FROM_DOCUMENTO_TO_GENERIC_DOC_CONTEXT, context);
			// FINE

			mergeMastersFromCE(output, mastersCE, pagedResult, utente, connection);
		} catch (final Exception e) {
			LOGGER.error(e);
		} finally {
			closeConnection(connection);
		}

		if (output == null || output.isEmpty() || output.size() < getPageSize(pagedResult.getQueue())) {
			pagedResult.setMorePages(false);
		}

		return output;
	}

	/**
	 * Metodo che, dato un set di Document Title ottenuto dal PE di FileNet,
	 * recupera i documenti dal CE senza paginazione filtrandoli su alcune proprietà
	 * con la stringa ricercata.
	 */
	@Override
	public Collection<MasterDocumentRedDTO> getFilteredMasters(final String filterString, final MasterPageIterator pagedResult, final UtenteDTO utente) {
		final Collection<MasterDocumentRedDTO> output = new ArrayList<>();
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			final Collection<MasterDocumentRedDTO> mastersCE = listaDocumentiSRV.getFilteredMastersFromCE(pagedResult.getDocumentTitleSet(), filterString, utente, connection);

			if (!CollectionUtils.isEmpty(mastersCE)) {
				mergeMastersFromCE(output, mastersCE, pagedResult, utente, connection);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
		} finally {
			closeConnection(connection);
		}

		return output;
	}

	private void mergeMastersFromCE(final Collection<MasterDocumentRedDTO> mergedMasters, final Collection<MasterDocumentRedDTO> mastersCE,
			final MasterPageIterator pagedResult, final UtenteDTO utente, final Connection connection) {
		final EnumMap<ContextTrasformerPEEnum, Object> context = new EnumMap<>(ContextTrasformerPEEnum.class);
		final Collection<VWWorkObject> masterWorkflows = new ArrayList<>();
		final Collection<Long> idUfficiMittenti = new HashSet<>();
		final Collection<Long> idUtentiMittenti = new HashSet<>();

		for (final MasterDocumentRedDTO masterCE : mastersCE) {
			masterWorkflows.addAll(pagedResult.filterWFByDocumentTitle(masterCE.getDocumentTitle()));
		}

		Long idUfficioMittente = null;
		Long idUtenteMittente = null;
		for (final VWWorkObject workflow : masterWorkflows) {
			idUfficioMittente = Long.valueOf((Integer) TrasformerPE.getMetadato(workflow, PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY));
			idUfficiMittenti.add(idUfficioMittente);

			idUtenteMittente = Long.valueOf((Integer) TrasformerPE.getMetadato(workflow, PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY));
			idUtentiMittenti.add(idUtenteMittente);
		}

		final Map<Long, String> ufficiMittentiMap = lookupDAO.getDescUffici(idUfficiMittenti, connection);
		final Map<Long, String> utentiMittentiMap = lookupDAO.getDescUtenti(idUtentiMittenti, connection);

		if (!mastersCE.isEmpty()) {
			addSiebelFlag(mastersCE, utente.getIdAoo(), connection);
		}

		for (final MasterDocumentRedDTO masterCE : mastersCE) {
			final Collection<VWWorkObject> filteredWF = pagedResult.filterWFByDocumentTitle(masterCE.getDocumentTitle());

			context.clear();
			if (!ufficiMittentiMap.isEmpty()) {
				context.put(ContextTrasformerPEEnum.UFFICI_MITTENTI, ufficiMittentiMap);
			}
			if (!utentiMittentiMap.isEmpty()) {
				context.put(ContextTrasformerPEEnum.UTENTI_MITTENTI, utentiMittentiMap);
			}

			context.put(ContextTrasformerPEEnum.UCB, utente.isUcb());

			final Collection<PEDocumentoDTO> filteredWFDTO = TrasformPE.transform(filteredWF, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO_CONTEXT, context);

			// Si effettua il merge
			final Collection<MasterDocumentRedDTO> refinedMasters = merge(utente, filteredWFDTO, masterCE, connection);

			mergedMasters.addAll(refinedMasters);
		}
	}

	/**
	 * Metodo per il recupero di una nuova pagina di master per le code
	 * dell'organigramma.
	 */
	@Override
	public Collection<MasterDocumentRedDTO> refineMastersOrganigramma(final UtenteDTO utente, final NodoOrganigrammaDTO nodoOrganigramma,
			final MasterPageIterator pagedResult) {
		final UtenteDTO datiAccesso = new UtenteDTO(utente.getFcDTO(), nodoOrganigramma);
		return refineMasters(datiAccesso, pagedResult);
	}

	/**
	 * Permette di recuperare le informazioni relative a siebel, al fine di mostrare
	 * o meno l'iconcina con la S maiuscola.
	 * 
	 * Se il documento ha delle RDS associate ha avuto a che fare con Siebel.
	 * 
	 * @param mastersCE  lista di master incompleta.
	 * @param connection Connessione al database applicativo.
	 */
	@Override
	public void addSiebelFlag(final Collection<MasterDocumentRedDTO> mastersCE, final Long idAOO, final Connection connection) {

		final List<MasterDocumentRedDTO> filteredWithId = mastersCE.stream().filter(m -> !StringUtils.isNullOrEmpty(m.getDocumentTitle())).collect(Collectors.toList());

		final Set<String> documentTitleList = filteredWithId.stream().map(MasterDocumentRedDTO::getDocumentTitle).collect(Collectors.toSet());

		if (!documentTitleList.isEmpty()) {
			final Map<String, Boolean> docTitle2flagSiebel = siebelSRV.hasRds(documentTitleList, idAOO);
			filteredWithId.forEach(m -> m.setFlagSiebel(docTitle2flagSiebel.get(m.getDocumentTitle())));
		}
	}

}