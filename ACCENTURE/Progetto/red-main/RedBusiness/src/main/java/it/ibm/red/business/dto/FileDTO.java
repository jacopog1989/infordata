package it.ibm.red.business.dto;

import java.util.UUID;

import com.google.common.net.MediaType;

/**
 * The Class FileDTO.
 *
 * @author CPIERASC
 * 
 * 	Classe utilizzata per modellare un file.
 */
public class FileDTO extends AbstractDTO {

	/**
	 * Serialization.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * UUID del file (optional)
	 */
	private String uuid;
	
	/**
	 * Nome del file.
	 */
	private String fileName;
	
	/**
	 * Nome del file firmato.
	 */
	private String fileNameSigned;
	
	/**
	 * Contenuto del file.
	 */
	private byte[] content;
	
	/**
	 * contenuto del file firmato
	 */
	private byte[] contentSigned;

	/**
	 * Tipo mime.
	 */
	private String mimeType;

	/**
	 * Tipo mime da settare dopo la firma.
	 */
	private String mimeTypeSigned;
	
	/**
	 * Descrizione.
	 */
	private final String description;
	
	/**
	 * Flag abilita firma.
	 */
	private Boolean flagEnableFirma;
	
	/**
	 * Boolean identifica se è firmato.
	 */
	private Boolean isSigned;
	
	/**
	 * Booleano che identifica se la firma pades è possibile.
	 */
	private Boolean canPades;
	
	/**
	 * Byte associati all'annotation.
	 */
	private byte[] annotation;
	
	/**
	 * Indica se l'attributo content proviene dall'annotation FileNet CONTENT_LIBRO_FIRMA
	 */
	private boolean contentAnnotationLibroFirma;
	
	
	/**
	 * Booleano che identifica se il file in uscita deve avere o meno il campo firma
	 */
	private boolean campoFirma;
	
	/**
	 * Costruttore.
	 * 
	 * @param inFileName	nome del file
	 * @param inContent		contenuto
	 * @param inMimeType	mime type
	 */
	public FileDTO(final String inUuid, final String inFileName, final byte[] inContent, final String inMimeType, final Boolean inIsSigned) {
		this.fileName = inFileName;
		this.content = inContent;
		this.mimeType = inMimeType;
		this.description = null;
		this.uuid = inUuid;
		this.isSigned = inIsSigned;
		if (this.mimeType != null && this.mimeType.equals(MediaType.PDF.toString())) {
			this.canPades = true;
		} else {
			this.canPades = false;
		}
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param inFileName	nome del file
	 * @param inContent		contenuto
	 * @param inMimeType	mime type
	 */
	public FileDTO(final String inFileName, final byte[] inContent, final String inMimeType) {
		this.uuid = UUID.randomUUID().toString();
		this.fileName = inFileName;
		this.content = inContent;
		this.mimeType = inMimeType;
		this.description = null;
	}

	/**
	 * Costruttore.
	 * 
	 * @param inFileName	nome del file
	 * @param inContent		content
	 * @param inMimeType	tipo mime
	 * @param inDescription	descrizione
	 */
	public FileDTO(final String inFileName, final byte[] inContent, final String inMimeType, final String inDescription) {
		this.fileName = inFileName;
		this.content = inContent;
		this.mimeType = inMimeType;
		this.description = inDescription;
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param inFileName					nome del file
	 * @param inContent						content
	 * @param inMimeType					tipo mime
	 * @param inContentAnnotationLibroFirma	indica se il content proviene dall'annotation "CONTENT_LIBRO_FIRMA"
	 */
	public FileDTO(final String inFileName, final byte[] inContent, final String inMimeType, final boolean inContentAnnotationLibroFirma) {
		this.fileName = inFileName;
		this.content = inContent;
		this.mimeType = inMimeType;
		this.description = null;
		this.contentAnnotationLibroFirma = inContentAnnotationLibroFirma;
	}
	

	/**
	 * Getter.
	 * 
	 * @return	descrizione
	 */
	public final String getDescription() {
		return description;
	}

	/**
	 * Getter nome file.
	 * 
	 * @return	nome file
	 */
	public final String getFileName() {
		return fileName;
	}
	
	/**
	 * Setter nome file.
	 * 
	 * @param fileName
	 */
	public void setFileName(final String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Getter contenuto.
	 * 
	 * @return	contenuto
	 */
	public byte[] getContent() {
		return content;
	}

	/**
	 * Getter mime type.
	 * 
	 * @return	mime type
	 */
	public final String getMimeType() {
		return mimeType;
	}

	/**
	 * @param mimeType the mimeType to set
	 */
	public void setMimeType(final String mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 *
	 * tags:
	 * @return the flagEnableFirma
	 */
	public Boolean getFlagEnableFirma() {
		return flagEnableFirma;
	}

	/**
	 *
	 * tags:
	 * @param flagEnableFirma the flagEnableFirma to set
	 */
	public void setFlagEnableFirma(final Boolean flagEnableFirma) {
		this.flagEnableFirma = flagEnableFirma;
	}

	/**
	 * Restituisce l'uuid.
	 * @return uuid
	 */
	public String getUuid() {
		return uuid;
	}
	
	/**
	 * @param uuid the uuid to set
	 */
	public final void setUuid(final String uuid) {
		this.uuid = uuid;
	}

	/**
	 * Restituisce true se firmato, false altrimenti.
	 * @return true se firmato, false altrimenti
	 */
	public Boolean getIsSigned() {
		return isSigned;
	}

	/**
	 * Imposta il flag associato alla firma.
	 * @param isSigned
	 */
	public void setIsSigned(final Boolean isSigned) {
		this.isSigned = isSigned;
	}
	
	/**
	 * @return canPades
	 */
	public Boolean getCanPades() {
		return canPades;
	}

	/**
	 * @param canPades
	 */
	public void setCanPades(final Boolean canPades) {
		this.canPades = canPades;
	}

	/**
	 * Restituisce il content firmato sotto forma di byte array.
	 * @return content firmato
	 */
	public byte[] getContentSigned() {
		return contentSigned;
	}

	/**
	 * Imposta il content firmato.
	 * @param contentSigned
	 */
	public void setContentSigned(final byte[] contentSigned) {
		this.contentSigned = contentSigned;
	}

	/**
	 *.
	 * tags:
	 * @return the mimeTypeSigned
	 */
	public String getMimeTypeSigned() {
		return mimeTypeSigned;
	}

	/**
	 *.
	 * tags:
	 * @param mimeTypeSigned the mimeTypeSigned to set
	 */
	public void setMimeTypeSigned(final String mimeTypeSigned) {
		this.mimeTypeSigned = mimeTypeSigned;
	}

	/**
	 *
	 * tags:
	 * @return the fileNameSigned
	 */
	public String getFileNameSigned() {
		return fileNameSigned;
	}

	/**
	 *
	 * tags:
	 * @param fileNameSigned the fileNameSigned to set
	 */
	public void setFileNameSigned(final String fileNameSigned) {
		this.fileNameSigned = fileNameSigned;
	}

	/**
	 * Imposta il content.
	 * @param content
	 */
	public void setContent(final byte[] content) {
		this.content = content;
	}

	/**
	 * Restituisce il flag associato al campo firma.
	 * @return flag campo firma
	 */
	public boolean isCampoFirma() {
		return campoFirma;
	}

	/**
	 * Imposta il flag campo firma.
	 * @param campoFirma
	 */
	public void setCampoFirma(boolean campoFirma) {
		this.campoFirma = campoFirma;
	}

	/**
	 * @return the annotation
	 */
	public byte[] getAnnotation() {
		return annotation;
	}

	/**
	 * @param annotation the annotation to set
	 */
	public void setAnnotation(byte[] annotation) {
		this.annotation = annotation;
	}

	/**
	 * @return the contentAnnotationLibroFirma
	 */
	public boolean isContentAnnotationLibroFirma() {
		return contentAnnotationLibroFirma;
	}
	
}
