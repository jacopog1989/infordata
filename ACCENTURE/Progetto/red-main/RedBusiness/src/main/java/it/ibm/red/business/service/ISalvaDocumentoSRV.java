package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.Collection;
import java.util.Date;

import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.service.facade.ISalvaDocumentoFacadeSRV;


/**
 * Servizio per il salvataggio (creazione/aggiornamento) di un documento.
 * 
 * @author m.crescentini
 *
 */
public interface ISalvaDocumentoSRV extends ISalvaDocumentoFacadeSRV {

	/**
	 * @param idDocumento
	 * @param metadatiEstesi
	 * @param idAoo
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param isModalitaInsert
	 * @param con
	 */
	void salvaMetadatiEstesi(Integer idDocumento, Collection<MetadatoDTO> metadatiEstesi, Long idAoo, 
			Long idTipologiaDocumento, Long idTipoProcedimento, Date inDataCreazione, boolean isModalitaInsert, Connection con,
			IFilenetCEHelper fceh);
	
}