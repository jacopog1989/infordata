package it.ibm.red.business.enums;

/**
 * Enum PeriodoReportEnum.
 */
public enum PeriodoReportEnum {
	
	/**
	 * Periodo generazione report Primo semestre
	 */
	I_SEMESTRE("Primo semestre"),
	
	/**
	 * Periodo generazione report Secondo semestre
	 */
	II_SEMESTRE("Secondo semestre"),
	
	/**
	 * Periodo generazione report Annuale
	 */
	ANNUALE("Annuale"),
	
	/**
	 * Periodo generazione report Personale
	 */
	PERSONALE("Personale"),
	
	/**
	 * Periodo generazione report Ad oggi
	 */
	AD_OGGI("Ad oggi");
	
	
	/** 
	 * Label
	 */
	private String label;
	
	/**
	 * Costruttore periodo report enum.
	 *
	 * @param l the l
	 */
	PeriodoReportEnum(final String l) {
		this.label = l;
	}
	
	/** 
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
}