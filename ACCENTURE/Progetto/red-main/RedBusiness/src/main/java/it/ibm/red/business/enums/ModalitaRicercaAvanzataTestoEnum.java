package it.ibm.red.business.enums;

/**
 * Modalità di ricerca di testo per la ricerca avanzata dei documenti.
 * 
 * @author m.crescentini
 *
 */
public enum ModalitaRicercaAvanzataTestoEnum {
	
	/**
	 * Valore.
	 */
	TUTTE_LE_PAROLE("fulltext"),
	
	/**
	 * Valore.
	 */
	UNA_DELLE_PAROLE("anytext");
	
	
	/**
	 * Valore.
	 */
	private String valore;
	
	/**
	 * Costruttore.
	 * @param valore
	 */
	ModalitaRicercaAvanzataTestoEnum(final String valore) {
		this.valore = valore;
	}
	
	/**
	 * @return the valore
	 */
	public String getValore() {
		return valore;
	}
	
	/**
	 * Restituisce l'enum associata al nome.
	 * @param name
	 * @return enum
	 */
	public static ModalitaRicercaAvanzataTestoEnum get(final String name) {
		ModalitaRicercaAvanzataTestoEnum output = null;
		
		for (ModalitaRicercaAvanzataTestoEnum modalita : ModalitaRicercaAvanzataTestoEnum.values()) {
			if (modalita.name().equalsIgnoreCase(name)) {
				output = modalita;
				break;
			}
		}
		
		return output;
	}
}