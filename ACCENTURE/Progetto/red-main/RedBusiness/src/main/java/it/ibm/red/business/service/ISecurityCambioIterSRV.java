package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.ISecurityCambioIterFacadeSRV;

/**
 * The Interface ISecurityCambioIterSRV.
 *
 * @author CPIERASC
 * 
 *         Interfaccia servizio gestione security filenet.
 */
public interface ISecurityCambioIterSRV extends ISecurityCambioIterFacadeSRV {
}
