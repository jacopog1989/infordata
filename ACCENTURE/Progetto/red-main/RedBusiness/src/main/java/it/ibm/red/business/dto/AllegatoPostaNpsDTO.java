package it.ibm.red.business.dto;

import java.io.Serializable;

import it.ibm.red.business.enums.TipoAllegatoPostaNpsEnum;

/**
 * DTO per la gestione degli allegati posta NPS.
 */
public class AllegatoPostaNpsDTO implements Serializable {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = 7991307380147806887L;
	
	/**
	 * Tipologia allegato.
	 */
	private TipoAllegatoPostaNpsEnum tipoAllegato;

	/**
	 * @return the tipoAllegato
	 */
	public TipoAllegatoPostaNpsEnum getTipoAllegato() {
		return tipoAllegato;
	}

}
