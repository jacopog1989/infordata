package it.ibm.red.business.enums;

/**
 * @author SLac
 */
public enum TestoPredefinitoEnum {

	/**
	 * Valore 1.
	 */
	TESTO_PREDEFINITO_MAIL(1),

	/**
	 * Valore 2.
	 */
	TESTO_PREDEFINITO_RIFIUTO_MAIL(2),

	/**
	 * Valore 3.
	 */
	TESTO_PREDEFINITO_INOLTRO_MAIL(3),

	/**
	 * Valore 4.
	 */
	TESTO_PREDEFINITO_INOLTRO_MAIL_DA_CODA_ATTIVA(4),

	/**
	 * Valore 5.
	 */
	TESTO_PREDEFINITO_APRI_RDS_SIEBEL(5),

	/**
	 * Valore 6.
	 */
	TESTO_PREDEFINITO_RIFIUTO_MAIL_CON_PROTOCOLLO(6),

	/**
	 * Valore 7.
	 */
	TESTO_PREDEFINITO_INOLTRO_MAIL_CON_PROTOCOLLO(7);

	/**
	 * ID.
	 */
	private int id;

	/**
	 * Costruttore di default.
	 * @param id
	 */
	TestoPredefinitoEnum(final int id) {
		this.id = id;
	}

	/**
	 * Restituisce il valore dell'enum corrispondente all'id in input.
	 * @param id
	 * @return testoPredefinitoEnum
	 */
	public static TestoPredefinitoEnum getById(final int id) {
		TestoPredefinitoEnum out = null;
		for (TestoPredefinitoEnum t : TestoPredefinitoEnum.values()) {
			if (t.getId() == id) {
				out = t;
				break;
			}
		}
		return out;
	}

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}
}
