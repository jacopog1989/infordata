package it.ibm.red.business.helper.adobe.dto;

/**
 * DTO che gestisce i count signs output.
 */
public class CountSignsOutputDTO {

	/**
	 * Numero campi firma.
	 */
	private Integer numSignFields;
	
	/**
	 * Numero campi firma vuoti.
	 */
	private Integer numEmptySignFields;

	/**
	 * Numero firme.
	 */
	private Integer numSigns;

	/**
	 * Costruttore vuoto.
	 */
	public CountSignsOutputDTO() {
		// Costruttore di default.
	}

	/**
	 * Restituisce il numero di campi firma.
	 * @return numSignFields
	 */
	public Integer getNumSignFields() {
		return numSignFields;
	}

	/**
	 * Imposta il numero di campi firma.
	 * @param numSignFields
	 */
	public void setNumSignFields(final Integer numSignFields) {
		this.numSignFields = numSignFields;
	}

	/**
	 * Restituisce il numero di campi firma vuoti.
	 * @return numEmptySignFields
	 */
	public Integer getNumEmptySignFields() {
		return numEmptySignFields;
	}

	/**
	 * Imposta il numero di campi firma vuoti.
	 * @param numEmptySignFields
	 */
	public void setNumEmptySignFields(final Integer numEmptySignFields) {
		this.numEmptySignFields = numEmptySignFields;
	}

	/**
	 * Restituisce il numero di firme.
	 * @return numSigns
	 */
	public Integer getNumSigns() {
		return numSigns;
	}

	/**
	 * Imposta il numero di firme.
	 * @param numSigns
	 */
	public void setNumSigns(final Integer numSigns) {
		this.numSigns = numSigns;
	}
	
}