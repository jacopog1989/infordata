/**
 * 
 */
package it.ibm.red.business.dto;

/**
 * @author m.crescentini
 *
 */
public class ContattoPostaNpsDTO extends AbstractDTO {

	private static final long serialVersionUID = -3776038116197626436L;


    /**
     * Display name contatto.
     */
	private final String displayName;
	

    /**
     * Mail contatto.
     */
	private final String mail;

	/**
	 * Costruttore del DTO.
	 * @param displayName
	 * @param mail
	 */
	public ContattoPostaNpsDTO(final String displayName, final String mail) {
		this.displayName = displayName;
		this.mail = mail;
	}


	/**
	 * @return displayName
	 */
	public String getDisplayName() {
		return displayName;
	}


	/**
	 * @return mail
	 */
	public String getMail() {
		return mail;
	}
	
}
