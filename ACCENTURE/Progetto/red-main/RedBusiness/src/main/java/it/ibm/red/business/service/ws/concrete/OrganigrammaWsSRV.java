package it.ibm.red.business.service.ws.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipologiaNodoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.IOrganigrammaSRV;
import it.ibm.red.business.service.concrete.AbstractService;
import it.ibm.red.business.service.ws.IOrganigrammaWsSRV;
import it.ibm.red.business.service.ws.auth.DocumentServiceAuthorizable;
import it.ibm.red.webservice.model.documentservice.dto.Servizio;
import it.ibm.red.webservice.model.documentservice.types.messages.RedGetOrganigrammaType;
import it.ibm.red.webservice.model.documentservice.types.organigramma.Ufficio;
import it.ibm.red.webservice.model.documentservice.types.organigramma.Utente;

/**
 * The Class OrganigrammaWsSRV.
 *
 * @author a.dilegge
 * 
 *         Servizio gestione organigramma.
 */
@Service
public class OrganigrammaWsSRV extends AbstractService implements IOrganigrammaWsSRV {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -2370909204921499965L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(OrganigrammaWsSRV.class.getName());

	/**
	 * Servizio.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IOrganigrammaSRV organigrammaSRV;

	/**
	 * @see it.ibm.red.business.service.ws.facade.IOrganigrammaWsFacadeSRV#getOrganigramma(it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.RedGetOrganigrammaType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_GET_ORGANIGRAMMA)
	public List<Ufficio> getOrganigramma(final RedWsClient client, final RedGetOrganigrammaType request) {

		final List<Ufficio> uffici = new ArrayList<>();
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final Aoo aoo = aooSRV.recuperaAoo(client.getIdAoo().intValue(), con);

			final NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
			nodoRadice.setIdAOO(aoo.getIdAoo());
			nodoRadice.setIdNodo(aoo.getIdNodoRadice());
			nodoRadice.setCodiceAOO(aoo.getCodiceAoo());
			nodoRadice.setUtentiVisible(false);

			final List<NodoOrganigrammaDTO> list = new ArrayList<>();
			list.add(nodoRadice);
			retrieveRecursiveOrganigramma(list, nodoRadice, request.isFirmatari());

			Ufficio ufficio = null;
			Utente utente = null;
			final Map<Long, Ufficio> ufficioMap = new HashMap<>();
			for (final NodoOrganigrammaDTO n : list) {

				if (ufficio == null || (!TipologiaNodoEnum.UTENTE.equals(n.getTipoNodo()) && !ufficio.getId().equals(n.getIdNodo().intValue()))) {
					ufficio = new Ufficio();
					if (n.getIdNodoPadre() == null || n.getIdNodoPadre() == 0L) {
						uffici.add(ufficio);
					} else {
						(ufficioMap.get(n.getIdNodoPadre())).getUffici().add(ufficio);
					}

				}

				if (TipologiaNodoEnum.UTENTE.equals(n.getTipoNodo())) {
					utente = new Utente();
					utente.setId(n.getIdUtente().intValue());
					utente.setNome(n.getNomeUtente());
					utente.setCognome(n.getCognomeUtente());
					ufficio.getUtenti().add(utente);
				} else {
					ufficio.setId(n.getIdNodo().intValue());
					ufficio.setDescrizione(n.getDescrizioneNodo());
					ufficioMap.put(n.getIdNodo(), ufficio);
				}

			}

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero dell'organigramma: " + e.getMessage(), e);
			throw new RedException("Errore durante il recupero dell'organigramma: " + e.getMessage());
		} finally {
			closeConnection(con);
		}

		return uffici;
	}

	private void retrieveRecursiveOrganigramma(final List<NodoOrganigrammaDTO> list, final NodoOrganigrammaDTO nodoRadice, final boolean firmatari) {
		final List<NodoOrganigrammaDTO> output = new ArrayList<>();
		List<NodoOrganigrammaDTO> figli = null;
		if (firmatari) {
			figli = organigrammaSRV.getFigliAlberoUtentiAssegnatariFilteredByPermesso(nodoRadice.getIdNodo(), nodoRadice, ResponsesRedEnum.INVIO_IN_FIRMA);
		} else {
			figli = organigrammaSRV.getFigliAlberoCompletoUtenti(nodoRadice.getIdNodo(),false, nodoRadice);
		}

		if (!figli.isEmpty()) {

			for (final NodoOrganigrammaDTO figlio : figli) {
				output.add(figlio);
				if (!TipologiaNodoEnum.UTENTE.equals(figlio.getTipoNodo())) {
					retrieveRecursiveOrganigramma(output, figlio, firmatari);
				}
			}

		}

		list.addAll(output);
	}

}