package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.EventoDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.RuoloDTO;
import it.ibm.red.business.dto.SearchEventiDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.EventoCalendarioEnum;
import it.ibm.red.business.persistence.model.Ente;
import it.ibm.red.business.persistence.model.Evento;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.TestiDefault;

/**
 * Facade del servizio di gestione calendar.
 */
public interface ICalendarioFacadeSRV extends Serializable {
	
	/**
	 * Crea l'evento.
	 * @param evento
	 * @return true se l'evento è stato creato, false in caso contrario
	 */
	boolean creaEvento(EventoDTO evento);
	
	/**
	 * Cancella l'evento.
	 * @param idEvento
	 * @return true se l'evento è stato cancellato, false in caso contrario
	 */
	boolean cancellaEvento(int idEvento);
	
	/**
	 * Modifica l'evento.
	 * @param evento
	 * @return true se l'evento è stato modificato, false in caso contrario.
	 */
	boolean modificaEvento(EventoDTO evento);
	
	/**
	 * Ottiene l'ente.
	 * @param idAoo
	 * @return ente
	 */
	Ente getEnte(Long idAoo);
	
	/**
	 * Ottiene i ruoli.
	 * @param idAoo - id dell'Aoo
	 * @return lista di ruoli
	 */
	List<RuoloDTO> getCategorieRuoli(Long idAoo);
	
	/**
	 * Trasforma l'evento in dto.
	 * @param evt evento
	 * @return EventoDTO
	 */
	EventoDTO changeEventoToEventoDTO(Evento evt);
	
	/**
	 * Ottiene gli eventi del calendario.
	 * @param searchEventiDTO - eventi da ricercare
	 * @param utente
	 * @param idEnte - id dell'ente
	 * @return
	 */
	Map<EventoCalendarioEnum, List<Evento>> getEventiForCalendario(SearchEventiDTO searchEventiDTO, UtenteDTO utente, Integer idEnte);

	/**
	 * Ottiene i testi di default.
	 * @return lista di testi di default
	 */
	List<TestiDefault> getAllTestiDefault();

	/**
	 * Ottiene i nodi dagli ids.
	 * @param ids
	 * @param idAoo - id dell'Aoo
	 * @return lista di nodi
	 */
	List<Nodo> getNodiByIds(List<Integer> ids, int idAoo);
	
	/**
	 * Ottiene gli eventi dei documenti in scadenza.
	 * @param searchEventiDTO - eventi da ricercare
	 * @param idUtente - id dell'utente
	 * @param idUfficio - id dell'ufficio
	 * @param fcUtente - credenziali filenet dell'utente
	 * @param idAoo - id dell'Aoo
	 * @return lista di eventi
	 */
	List<Evento> getEventiDocumentiInScadenza(SearchEventiDTO searchEventiDTO, Long idUtente, Long idUfficio, FilenetCredentialsDTO fcUtente, Long idAoo);
	
	/**
	 * Ottiene i documenti in scadenza dal PE.
	 * @param idUtente - id dell'utente
	 * @param idUfficio - id dell'ufficio
	 * @param fcUtente - credenziali filenet dell'utente
	 * @return eventi
	 */
	Collection<EventoDTO> getDocInScadenzaFromPe(Long idUtente, Long idUfficio, FilenetCredentialsDTO fcUtente);
	
	/**
	 * Ottiene gli eventi dal DB.
	 * @param dataInizio - data di inizio
	 * @param dataFine - data di fine
	 * @param titolo
	 * @param tipoevento - tipologia di evento
	 * @param idUtente - id dell'utente
	 * @param idUfficio - id dell'ufficio
	 * @param idRuolo - id del ruolo
	 * @param idAoo - id dell'Aoo
	 * @param bForCalendar
	 * @return eventi
	 */
	Collection<EventoDTO> getEventiFromDb(Date dataInizio, Date dataFine, String titolo, EventoCalendarioEnum tipoevento, Long idUtente, Long idUfficio, Long idRuolo, Long idAoo, Boolean bForCalendar);
	
	/**
	 * Ottiene l'allegato dell'evento.
	 * @param idEvento - id dell'evento
	 * @return allegato
	 */
	byte[] getAllegatoEvento(Integer idEvento);
	
	/**
	 * Ricerca gli eventi.
	 * @param dataInizio - data di inizio
	 * @param dataFine - data di fine
	 * @param titolo
	 * @param tipoevento - tipologia di evento
	 * @param idUtente - id dell'utente
	 * @param idUfficio - id dell'ufficio
	 * @param idRuolo - id del ruolo
	 * @param idAoo - id dell'Aoo
	 * @param fcUtente - credenziali filenet dell'utente
	 * @return mappa degli eventi
	 */
	Map<Integer, Collection<EventoDTO>> ricercaEventi(Date dataInizio, Date dataFine, String titolo, EventoCalendarioEnum tipoevento, Long idUtente, Long idUfficio, Long idRuolo, Long idAoo, FilenetCredentialsDTO fcUtente);

	/**
	 * Ottiene le news.
	 * @param utente
	 * @param nDaysBefore - numero di giorni precedenti
	 * @param nDaysAfter - numero di giorni successivi
	 * @return eventi
	 */
	Collection<EventoDTO> getNews(UtenteDTO utente, Integer nDaysBefore, Integer nDaysAfter);

	/**
	 * Ottiene gli eventi del calendario.
	 * @param searchEventiDTO - eventi da ricercare
	 * @param utente
	 * @param idEnte - id dell'ente
	 * @param con
	 * @return mappa lista di eventi per evento del calendario
	 */
	Map<EventoCalendarioEnum, List<Evento>> getEventiForCalendarioHomepage(SearchEventiDTO searchEventiDTO, UtenteDTO utente, Integer idEnte, Connection con);

	

}