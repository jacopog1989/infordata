package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.HashMap; 
 

/**
 * 
 * @author Vingenito
 *
 */
public interface IStampigliaturaSegnoGraficoFacadeSRV extends Serializable {
 
	/**
	 * Ottiene i firmatari.
	 * @param siglatariString
	 * @param idNodo
	 * @param tipoFirma
	 * @return firmatari
	 */
	String[] getFirmatariStringArray(String[] siglatariString, Long idNodo, Integer tipoFirma);
	
	/**
	 * Ottiene il tipo di firma.
	 * @param idIterApprovativo
	 * @return tipo di firma
	 */
	Integer getTipoFirma(Integer idIterApprovativo);

	/**
	 * Ottiene l'icona tramite il placeholder.
	 * @param placeholder
	 * @param numDocumento
	 * @return mappa icona descrizione
	 */
	HashMap<String, String> getIconByPlaceholder(String placeholder, String numDocumento);
 
}
