package it.ibm.red.business.helper.filenet.ce.trasform;

import java.util.EnumMap;

import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.NoTrasformerException;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextFromDocumentoToAllegatoRicercaAvanzataTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextFromDocumentoToAssociaIntegrazioneDati;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextFromDocumentoToDocumentoEmailWsTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextFromDocumentoToGenericDoc;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextFromDocumentoToMasterTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FormDocumentoToDocSigiTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentToFaldoneDtoTransformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentToFaldoneTreeDtoTransformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentToRedVersionTransformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentToRegistroProtocolloTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToAllegatoContentMailTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToAllegatoDetailTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToAllegatoDtoPerDettaglioTransformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToAllegatoMailTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToAppletContent;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToDDTransformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToDetailAssegnazione;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToDetailAssegnazioneCoordinatore;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToDetailRedTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToDetailTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToDocumentoFascicoloTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextFromDocumentoToDocumentoWsTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextFromDocumentoToFascicoloWsTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToFascicoloTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToFatturaTransformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToGenericDoc;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToIdDocumentDTOTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToLightDetailRedTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToMailDetailTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToMailMasterTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToMailMasterWithContentTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToMailRicercaTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToMasterCartaceoTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToMasterTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToProtocolloDaVerificare;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToUpdateDetailRedTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.ModificaIterTrasformer;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;

/**
 * A factory for creating TrasformerCE objects.
 *
 * @author CPIERASC
 * 
 *         Factory per la gestione delle trasformazioni dei documenti presenti
 *         nel CE.
 */
@SuppressWarnings("rawtypes")
public final class TrasformerCEFactory {
	
	/**
	 * Insieme dei trasformer.
	 */
	private static EnumMap<TrasformerCEEnum, TrasformerCE> trasformers = new EnumMap<>(TrasformerCEEnum.class);
	
	/**
	 * Istanza della factory.
	 */
	private static TrasformerCEFactory instance = new TrasformerCEFactory();
	
	/**
	 * Costruttore.
	 */
	private TrasformerCEFactory() {
		addTrasformer(new FromDocumentoToMasterTrasformer());
		addTrasformer(new ContextFromDocumentoToMasterTrasformer());
		addTrasformer(new FromDocumentoToDetailTrasformer());
		addTrasformer(new FromDocumentoToDetailRedTrasformer());
		addTrasformer(new FromDocumentoToLightDetailRedTrasformer());
		addTrasformer(new FromDocumentoToDocumentoFascicoloTrasformer());
		addTrasformer(new FromDocumentoToFascicoloTrasformer());
		addTrasformer(new FromDocumentoToMailDetailTrasformer());
		addTrasformer(new FromDocumentoToAllegatoMailTrasformer());
		addTrasformer(new FromDocumentoToMailMasterTrasformer());
		addTrasformer(new FromDocumentoToMailMasterWithContentTrasformer());
		addTrasformer(new ModificaIterTrasformer());
		addTrasformer(new FromDocumentoToGenericDoc());
		addTrasformer(new FromDocumentoToGenericDoc());
		addTrasformer(new ContextFromDocumentoToGenericDoc());
		addTrasformer(new FromDocumentToFaldoneDtoTransformer());
		addTrasformer(new FromDocumentoToAppletContent());
		addTrasformer(new FromDocumentToFaldoneTreeDtoTransformer());
		addTrasformer(new FromDocumentToRedVersionTransformer());
		addTrasformer(new FromDocumentoToAllegatoContentMailTrasformer());
		addTrasformer(new FromDocumentoToAllegatoDetailTrasformer());
		addTrasformer(new FromDocumentoToDetailAssegnazione());
		addTrasformer(new FromDocumentoToDetailAssegnazioneCoordinatore());
		addTrasformer(new FromDocumentoToFatturaTransformer());
		addTrasformer(new FromDocumentoToDDTransformer());
		addTrasformer(new FromDocumentToRegistroProtocolloTrasformer());
		addTrasformer(new FormDocumentoToDocSigiTrasformer());
		addTrasformer(new FromDocumentoToIdDocumentDTOTrasformer());
		addTrasformer(new FromDocumentoToUpdateDetailRedTrasformer());
		addTrasformer(new FromDocumentoToProtocolloDaVerificare());
		addTrasformer(new ContextFromDocumentoToAllegatoRicercaAvanzataTrasformer());
		addTrasformer(new FromDocumentoToMasterCartaceoTrasformer());
		addTrasformer(new FromDocumentoToAllegatoDtoPerDettaglioTransformer());
		addTrasformer(new ContextFromDocumentoToDocumentoWsTrasformer());
		addTrasformer(new ContextFromDocumentoToDocumentoEmailWsTrasformer());
		addTrasformer(new ContextFromDocumentoToFascicoloWsTrasformer());
		addTrasformer(new FromDocumentoToMailRicercaTrasformer());
		addTrasformer(new ContextFromDocumentoToAssociaIntegrazioneDati());
	}

	/**
	 * Metodo per aggiungere un trasformer associato ad una specifica enum.
	 * 
	 * @param t	trasformer
	 */
	private static void addTrasformer(final TrasformerCE t) {
		trasformers.put(t.getEnumKey(), t);
	}

	/**
	 * Metodo per il recupero dell'istanza della factory.
	 * 
	 * @return	istanza della factory
	 */
	public static TrasformerCEFactory getInstance() {
		return instance;
	}

	/**
	 * Recupero di un trasformer a partire dall'enum che identifica una trasformazione.
	 * 
	 * @param te	identificatore trasformazione
	 * @return		trasformer
	 */
	public static TrasformerCE getTrasformer(final TrasformerCEEnum te) {
		TrasformerCE tr = trasformers.get(te);
		if (tr == null) {
			throw new NoTrasformerException(te);
		}
		return tr;
	}

}