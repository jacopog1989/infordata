package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IRicercaSigiFacadeSRV;

/**
 * The Interface IRicercaFepaSRV.
 *
 * @author m.crescentini
 * 
 *         Interfaccia del servizio per la gestione della ricerca SIGI.
 */
public interface IRicercaSigiSRV extends IRicercaSigiFacadeSRV {

}