package it.ibm.red.business.service;

import java.sql.Connection;

import it.ibm.red.business.dto.CodaTrasformazioneParametriDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.service.facade.ITrasformazionePDFFacadeSRV;

/**
 * The Interface ITrasformazionePDFSRV.
 *
 * @author a.dilegge
 * 
 *         Servizio di gestione delle trasformazioni in PDF dei documenti.
 */
public interface ITrasformazionePDFSRV extends ITrasformazionePDFFacadeSRV {

	/**
	 * Inserisce il documento nella coda trasformazione PDF.
	 * @param utente
	 * @param parametri
	 * @param chiamante
	 * @param con
	 */
	void insertSchedule(UtenteDTO utente, CodaTrasformazioneParametriDTO parametri, String chiamante, Connection con);

	/**
	 * Avvia la trasformazione in PDF per la nuova assegnazione firma sigla visto.
	 * @param idDocumento
	 * @param guid
	 * @param idTipoAssegnazione
	 * @param utente
	 * @param firmatari
	 * @param tipoFirma
	 * @param chiamante
	 * @param fceh
	 * @param con
	 * @param isModificaIter
	 */
	void avviaTrasformazionePDFPerNuovaAssegnazioneFirmaSiglaVisto(String idDocumento, String guid,
			Integer idTipoAssegnazione, UtenteDTO utente, String[] firmatari, Integer tipoFirma, String chiamante,
			IFilenetCEHelper fceh, Connection con, boolean isModificaIter);
	
}