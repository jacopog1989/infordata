package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.xalan.xsltc.runtime.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.IComunicazioneProtocolliArgoDAO;
import it.ibm.red.business.dto.ComunicazioneProtocolloArgoDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.StatoElabMessaggioPostaNpsEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.provider.WebServiceClientProvider;
import it.ibm.red.business.service.IComunicazioneProtocolliArgoSRV;
import it.ibm.red.business.utils.StringUtils;


/**
 * @author m.crescentini
 *
 */
@Service
@Component
public class ComunicazioneProtocolliArgoSRV extends AbstractService implements IComunicazioneProtocolliArgoSRV {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -8605423483706846556L;

	/**
	 * Label Protocollo.
	 */
	private static final String PROTOCOLLO_LABEL = "] protocollo [";

	/**
	 * Messaggio generico sull'id della coda. Occorre appendere l'id della coda al messaggio.
	 */
	private static final String ELABORA_COMUNICAZIONE_MSG = "elaboraComunicazioneProtocollo idcoda [";
	
	/**
	 * Timeout di default ms.
	 */
	private static final int DEFAULT_TIMEOUT_MS = 25000;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ComunicazioneProtocolliArgoSRV.class.getName());
	
	/**
	 * DAO.
	 */
	@Autowired
	private IComunicazioneProtocolliArgoDAO comunicazioneProtocolliArgoDAO;
	
	/**
	 * DAO.
	 */
	@Autowired
	private IAooDAO aooDAO;


	/**
	 * @see it.ibm.red.business.service.facade.IProtocollazioneArgoFacadeSRV#elaboraComunicazioneProtocollo
	 *      (it.ibm.red.business.dto.ComunicazioneProtocolloArgoDTO).
	 */
	@Override
	public boolean elaboraComunicazioneProtocollo(final ComunicazioneProtocolloArgoDTO comunicazioneProtocollo) {
		boolean responseArgoOk = false;
		final PropertiesProvider pp = PropertiesProvider.getIstance();
		
		final Long idCoda = comunicazioneProtocollo.getIdCoda();
		
		LOGGER.info(ELABORA_COMUNICAZIONE_MSG + idCoda + "]");
		
		final String protocollo = comunicazioneProtocollo.getNumeroProtocollo() + "/" + comunicazioneProtocollo.getAnnoProtocollo();
		
		LOGGER.info(ELABORA_COMUNICAZIONE_MSG + idCoda + PROTOCOLLO_LABEL + protocollo + "]");
		
		final CloseableHttpClient client = WebServiceClientProvider.getIstance().getCloseableHttpClient();
		
		Connection con = null;
		
		Document docFilenet = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			
			LOGGER.info(ELABORA_COMUNICAZIONE_MSG + idCoda + PROTOCOLLO_LABEL + protocollo + "] connessione aperta");
			
			docFilenet = getDocumentoFilenet(String.valueOf(comunicazioneProtocollo.getIdDocumento()), comunicazioneProtocollo.getIdAoo(), con);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero di una comunicazione di protocollo verso il sistema ARGO dalla coda REDBATCH_CODA_PROT_ARGO", e);
		} finally {
			closeConnection(con);
		}
			
		try {
			LOGGER.info(ELABORA_COMUNICAZIONE_MSG + idCoda + PROTOCOLLO_LABEL + protocollo + "] documento prelevato da CE");
			
			final Integer numeroDocumento = (Integer) TrasformerCE.getMetadato(docFilenet, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			final Integer annoDocumento = (Integer) TrasformerCE.getMetadato(docFilenet, PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY);
			
			LOGGER.info(ELABORA_COMUNICAZIONE_MSG + idCoda + PROTOCOLLO_LABEL + protocollo 
					+ "] numeroDocumento [" + numeroDocumento + "] annoDocumento [" + annoDocumento + "]");
			
			// Creazione della request HTTP POST verso l'endpoint presente nelle properties
		    final HttpPost requestComunicazioneProtocollo = new HttpPost(pp.getParameterByKey(PropertiesNameEnum.INTEGRAZIONE_PROTOCOLLO_ARGO_ENDPOINT));
		    
		    LOGGER.info(ELABORA_COMUNICAZIONE_MSG + idCoda + PROTOCOLLO_LABEL + protocollo + "] endpoint: " 
		    		+ pp.getParameterByKey(PropertiesNameEnum.INTEGRAZIONE_PROTOCOLLO_ARGO_ENDPOINT));
		    
		    final int connectionTimeout = getTimeout(pp);
		    
			final RequestConfig config = RequestConfig.custom()
					.setConnectTimeout(connectionTimeout)
					.setConnectionRequestTimeout(connectionTimeout)
					.setSocketTimeout(connectionTimeout).build();
			
			requestComunicazioneProtocollo.setConfig(config);
		    
			LOGGER.info(ELABORA_COMUNICAZIONE_MSG + idCoda + PROTOCOLLO_LABEL + protocollo + "] impostato timeout: " + connectionTimeout);
		    
		    // Impostazione degli header
		    requestComunicazioneProtocollo.setHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());
		    
		    LOGGER.info(ELABORA_COMUNICAZIONE_MSG + idCoda + PROTOCOLLO_LABEL + protocollo + "] settato header json");
		    
		    // Creazione dell'oggetto JSON che contiene i dati del protocollo da comunicare
		    // e le informazioni necessarie per la riconciliazione (numero documento e anno documento)
		    final StringBuilder protocolloJson = new StringBuilder()
		    		
		    .append(StringUtils.addParamToJsonObject("numero_documento", Constants.EMPTYSTRING + numeroDocumento, true, true, false))
		    
		    .append(StringUtils.addParamToJsonObject("anno_documento", Constants.EMPTYSTRING + annoDocumento, true, false, false))
		    
		    .append(StringUtils.addParamToJsonObject("numero_protocollo", 
		    		Constants.EMPTYSTRING + comunicazioneProtocollo.getNumeroProtocollo(), true, false, false))
		    
		    .append(StringUtils.addParamToJsonObject("id_protocollo", comunicazioneProtocollo.getIdProtocollo(), true, false, false))
		    		    
		    .append(StringUtils.addParamToJsonObject("data_protocollo", 
		    		new SimpleDateFormat("ddMMyyyy").format(comunicazioneProtocollo.getDataRegistrazioneProtocollo()), true, false, true));
		    
		    LOGGER.info(ELABORA_COMUNICAZIONE_MSG + idCoda + PROTOCOLLO_LABEL + protocollo + "] request: " + protocolloJson.toString());
		    
		    final StringEntity entity = new StringEntity(protocolloJson.toString());
		    
		    // Impostazione dell'oggetto JSON appena creato nel body della richiesta HTTP
		    requestComunicazioneProtocollo.setEntity(entity);
		    
		    final CloseableHttpResponse responseComunicazioneProtocollo = client.execute(requestComunicazioneProtocollo);
		    
		    LOGGER.info(ELABORA_COMUNICAZIONE_MSG + idCoda + PROTOCOLLO_LABEL + protocollo + "] request eseguita con esito "
		    		+ responseComunicazioneProtocollo.getStatusLine().getStatusCode());
		    
		    // Il sistema ARGO ha risposto 200 OK
		    if (HttpStatus.SC_OK == responseComunicazioneProtocollo.getStatusLine().getStatusCode()) {
		    	final HttpEntity entityResponse = responseComunicazioneProtocollo.getEntity();
		    	
		    	LOGGER.info(ELABORA_COMUNICAZIONE_MSG + idCoda + PROTOCOLLO_LABEL + protocollo + "] content type response: " 
		    			+ entityResponse.getContentType());
		    	
		    	if (entityResponse.getContentType() != null && ContentType.APPLICATION_JSON.getMimeType().equalsIgnoreCase(entityResponse.getContentType().getValue())) {
		    		
	    			final String responseJson = EntityUtils.toString(entityResponse);
	    			
	    			LOGGER.info(ELABORA_COMUNICAZIONE_MSG + idCoda + PROTOCOLLO_LABEL + protocollo + "] response: " + responseJson);
	    			
	    			if (!StringUtils.isNullOrEmpty(responseJson) && responseJson.contains("{\"codice\":\"0\",\"descrizione\":\"Protocollo aggiornato\"}")) {
	    		    	responseArgoOk = true;
	    		    	LOGGER.info("Il sistema ARGO ha risposto con esito OK per il protocollo " + comunicazioneProtocollo.getNumeroProtocollo() 
    						+ " del " + comunicazioneProtocollo.getDataRegistrazioneProtocollo());
	    			} else {
	    				String errore = ELABORA_COMUNICAZIONE_MSG + idCoda + PROTOCOLLO_LABEL + protocollo + "] Il sistema ARGO ha risposto con esito KO. Risposta : " + responseJson;
	    				LOGGER.error(errore);
	    		    	throw new RedException(errore);
	    			}
		    	} else {
		    		String errore = ELABORA_COMUNICAZIONE_MSG + idCoda + PROTOCOLLO_LABEL + protocollo + "] Il sistema ARGO ha restituito una risposta non in linea con le aspettative: " + entityResponse.toString();
		    		LOGGER.error(errore);
		    		throw new RedException(errore);
		    	}
		    	
		    	con = setupConnection(getDataSource().getConnection(), false);
		    	
		    	// Si aggiorna lo stato della comunicazione in coda impostando l'avvenuta elaborazione
		    	comunicazioneProtocolliArgoDAO.aggiornaStatoComunicazioneProtocollo(idCoda, StatoElabMessaggioPostaNpsEnum.ELABORATA.getId(), con);
		    	
		    	LOGGER.info(ELABORA_COMUNICAZIONE_MSG + idCoda + PROTOCOLLO_LABEL + protocollo + "] item aggiornato sulla base dati");
		    	
		    } else {
		    	final String errore = ELABORA_COMUNICAZIONE_MSG + idCoda + PROTOCOLLO_LABEL + protocollo + "] Il sistema ARGO ha risposto con un HTTP status code diverso da 200 OK: " 
		    			+ responseComunicazioneProtocollo.getStatusLine().getStatusCode();
		    	LOGGER.error(errore);
		    	throw new RedException(errore);
		    }
		    
		    if(!responseArgoOk) {
		    	String errore = ELABORA_COMUNICAZIONE_MSG + idCoda + PROTOCOLLO_LABEL + protocollo + "] L'item non è stato elaborato con successo.";
		    	LOGGER.error(errore);
		    	throw new RedException(errore);
		    }
		    
		} catch (final Exception e) {
			
			if (idCoda != null) {
				LOGGER.error("Errore durante la comunicazione del protocollo " + protocollo + " al sistema ARGO. ID coda: " + idCoda, e);
				Connection conErrore = null;
				
				try {
					conErrore = setupConnection(getDataSource().getConnection(), false);
					
					// Si aggiorna lo stato della comunicazione in coda impostando l'errore
					comunicazioneProtocolliArgoDAO.aggiornaStatoComunicazioneProtocollo(idCoda, StatoElabMessaggioPostaNpsEnum.IN_ERRORE.getId(), 
							"Errore durante la comunicazione del protocollo: " + e.getMessage(), conErrore);
				} catch (final SQLException sqlEx) {
					LOGGER.error("Errore durante l'aggiornamento dello stato di una comunicazione di protocollo verso il sistema ARGO nella coda " 
							+ "REDBATCH_CODA_PROT_ARGO. ID coda: " + idCoda, sqlEx);
				} finally {
					closeConnection(conErrore);
				}
			} else {
				LOGGER.error("Errore durante il recupero di una comunicazione di protocollo verso il sistema ARGO dalla coda REDBATCH_CODA_PROT_ARGO", e);
			}
			
		} finally {
			closeConnection(con);
		}
		
		return responseArgoOk;
	}


	/**
	 * Recupero tempo di timeout della connessione.
	 * @param pp
	 * @return tempo timeout
	 */
	private static int getTimeout(final PropertiesProvider pp) {
		int connectionTimeout = DEFAULT_TIMEOUT_MS;
		try {
			connectionTimeout = Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.INTEGRAZIONE_PROTOCOLLO_ARGO_CONNECTION_TIMEOUT_MS));
		} catch (final Exception e) {
			LOGGER.error("Configurare il timeout per l'integrazione protocollo ARGO con la property: " 
					+ PropertiesNameEnum.INTEGRAZIONE_PROTOCOLLO_ARGO_CONNECTION_TIMEOUT_MS.getKey(), e);
		}
		return connectionTimeout;
	}


	/**
	 * @see it.ibm.red.business.service.facade.IProtocollazioneArgoFacadeSRV#recuperaComunicazioneProtocolloDaLavorare(java.lang.Long).
	 */
	@Override
	public ComunicazioneProtocolloArgoDTO recuperaComunicazioneProtocolloDaLavorare(final long idAoo) {
		ComunicazioneProtocolloArgoDTO comunicazioneProtocollo = null;
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), true);
			
			// Si recupera una richiesta di comunicazione protocollo da lavorare
			comunicazioneProtocollo = comunicazioneProtocolliArgoDAO.getAndLockComunicazioneProtocollo(idAoo, con);
			
			// Si aggiorna lo stato della richiesta in coda impostando la presa in carico
			if (comunicazioneProtocollo != null) {
				comunicazioneProtocolliArgoDAO.aggiornaStatoComunicazioneProtocollo(comunicazioneProtocollo.getIdCoda(), 
						StatoElabMessaggioPostaNpsEnum.IN_ELABORAZIONE.getId(), con);
			}
			
			commitConnection(con);
		} catch (final Exception e) {
			rollbackConnection(con);
			LOGGER.error("Si è verificato un errore durante il recupero della prossima comunicazione di protocollo verso il sistema ARGO da lavorare. "
					+ "ID AOO: " + idAoo, e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		
		return comunicazioneProtocollo;
	}


	/**
	 * @see it.ibm.red.business.service.IProtocollazioneArgoSRV#accodaComunicazioneProtocollo(
	 *      it.ibm.red.business.dto.ComunicazioneProtocolloArgoDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public Long accodaComunicazioneProtocollo(final ComunicazioneProtocolloArgoDTO comunicazioneProtocollo, final Connection con) {
		Long idCoda = null;
		
		try {
			
			idCoda = comunicazioneProtocolliArgoDAO.inserisciComunicazioneProtocolloInCoda(comunicazioneProtocollo, con);
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento in coda di una comunicazione di protocollo verso il sistema ARGO." 
					+ " ID documento: " + comunicazioneProtocollo.getIdDocumento(), e);
			throw new RedException(e);
		}
		
		return idCoda;
	}
	
	
	/**
	 * Restituisce il documento filenet recuperato dal {@code documentTitle}.
	 * 
	 * @param documentTitle
	 *            Identificativo del documento.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param con
	 *            Connessione al database.
	 * @return Documento recuperato.
	 */
	private Document getDocumentoFilenet(final String documentTitle, final Long idAoo, final Connection con) {
		Document docFilenet = null;
		IFilenetCEHelper fceh = null;
		
		try {
			final Aoo aoo = aooDAO.getAoo(idAoo, con);
			final AooFilenet aooFilenet = aoo.getAooFilenet();
			
			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(), 
					aooFilenet.getObjectStore(), idAoo);
			
			// Si recupera il documento da FileNet
			docFilenet = fceh.getDocumentByDTandAOO(documentTitle, idAoo);
			
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero da FileNet del documento con ID: " + documentTitle + "[AOO: " + idAoo + "]", e);
			throw new RedException("Errore durante il recupero da FileNet del documento con ID: " + documentTitle + "[AOO: " + idAoo + "]", e);
		} finally {
			popSubject(fceh);
		}
		
		return docFilenet;
	}
}