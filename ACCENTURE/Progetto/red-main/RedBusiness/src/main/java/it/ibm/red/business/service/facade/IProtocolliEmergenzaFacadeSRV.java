package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.webservice.model.interfaccianps.dto.ParametersElaboraMessaggioProtocollazioneEmergenza;

/**
 * The Interface IProtocolliEmergenzaFacadeSRV.
 *
 * @author a.dilegge
 * 
 *         Servizio gestione protocolli di emergenza.
 */
public interface IProtocolliEmergenzaFacadeSRV extends Serializable {

	/**
	 * Riconcilia i dati del protocollo ufficiale.
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	String riconciliaDatiProtocolloUfficiale(ParametersElaboraMessaggioProtocollazioneEmergenza parameters);
	
}
