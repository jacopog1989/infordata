package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.INpsConfigurationDAO;
import it.ibm.red.business.dto.TipologiaDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.persistence.model.NpsAzioniConfig;
import it.ibm.red.business.persistence.model.NpsConfiguration;

/**
 * The Class NpsConfigurationDAO.
 *
 * @author CPIERASC
 * 
 * 	Dao per la gestione della configurazione di NPS.
 */
@Repository
public class NpsConfigurationDAO extends AbstractDAO implements INpsConfigurationDAO {

	private static final String UTENTE_ADMIN = "UTENTE_ADMIN";
	private static final String UTENTE = "UTENTE";
	private static final String SERVIZIO_ADMIN = "SERVIZIO_ADMIN";
	private static final String SERVIZIO = "SERVIZIO";
	private static final String PASSWORD_ADMIN = "PASSWORD_ADMIN";
	private static final String PASSWORD = "PASSWORD";
	private static final String IDAOO = "IDAOO";
	private static final String ACTOR = "ACTOR";
	private static final String APPLICAZIONE = "APPLICAZIONE";
	private static final String CLIENT = "CLIENT";
	private static final String CODICE_AMMINISTRAZIONE = "CODICE_AMMINISTRAZIONE";
	private static final String CODICE_AOO = "CODICE_AOO";
	private static final String REGISTRO_UFFICIALE = "REGISTRO_UFFICIALE";
	private static final String DENOMINAZIONE_AMMINISTRAZIONE = "DENOMINAZIONE_AMMINISTRAZIONE";
	private static final String DENOMINAZIONE_AOO = "DENOMINAZIONE_AOO";
	private static final String DENOMINAZIONE_REGISTRO = "DENOMINAZIONE_REGISTRO";
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -2660110890077227843L;

	/**
	 * Gets the by id aoo.
	 *
	 * @param idAoo the id aoo
	 * @param connection the connection
	 * @return the by id aoo
	 */
	@Override
	public final NpsConfiguration getByIdAoo(final int idAoo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final String querySQL = "SELECT c.* FROM Nps_Configuration c WHERE c.idaoo = " + sanitize(idAoo);
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			if (rs.next()) {
				return new NpsConfiguration(idAoo, 
											rs.getString(CODICE_AOO),
											rs.getString(DENOMINAZIONE_AOO),
											rs.getString(CODICE_AMMINISTRAZIONE),
											rs.getString(DENOMINAZIONE_AMMINISTRAZIONE),
											rs.getString(REGISTRO_UFFICIALE),
											rs.getString(DENOMINAZIONE_REGISTRO),
											rs.getString(UTENTE),
											rs.getString(CLIENT),
											rs.getString(PASSWORD),
											rs.getString(SERVIZIO),
											rs.getString(ACTOR),
											rs.getString(APPLICAZIONE),
											rs.getString(UTENTE_ADMIN),
											rs.getString(PASSWORD_ADMIN),
											rs.getString(SERVIZIO_ADMIN));
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero delle configurazione NPS tramite idAoo=" + idAoo, e);
		} finally {
			closeStatement(ps, rs);
		}
		return null;
	}
	
	/**
	 * Gets the by codice aoo.
	 *
	 * @param codiceAoo the codice aoo
	 * @param connection the connection
	 * @return the by id aoo
	 */
	@Override
	public final NpsConfiguration getByCodiceAoo(final String codiceAoo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final String querySQL = "SELECT c.* FROM Nps_Configuration c WHERE c.codice_aoo=" + sanitize(codiceAoo);
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			if (rs.next()) {
				return new NpsConfiguration(rs.getInt(IDAOO), 
											rs.getString(CODICE_AOO),
											rs.getString(DENOMINAZIONE_AOO),
											rs.getString(CODICE_AMMINISTRAZIONE),
											rs.getString(DENOMINAZIONE_AMMINISTRAZIONE),
											rs.getString(REGISTRO_UFFICIALE),
											rs.getString(DENOMINAZIONE_REGISTRO),
											rs.getString(UTENTE),
											rs.getString(CLIENT),
											rs.getString(PASSWORD),
											rs.getString(SERVIZIO),
											rs.getString(ACTOR),
											rs.getString(APPLICAZIONE),
											rs.getString(UTENTE_ADMIN),
											rs.getString(PASSWORD_ADMIN),
											rs.getString(SERVIZIO_ADMIN));
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero delle configurazione NPS tramite codiceAoo=" + codiceAoo, e);
		} finally {
			closeStatement(ps, rs);
		}
		return null;
	}

	/**
	 * @see it.ibm.red.business.dao.INpsConfigurationDAO#getAzioneConfig(java.lang.Integer,
	 *      java.lang.String, java.lang.String, java.sql.Connection).
	 */
	@Override
	public NpsAzioniConfig getAzioneConfig(final Integer idAoo, final String methodName, final String className, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final String querySQL = "SELECT * FROM NPS_AZIONI_CONFIG WHERE idaoo = ? and method_name = ? and class_name = ?";
			ps = connection.prepareStatement(querySQL);
			
			ps.setInt(1, idAoo);
			ps.setString(2, methodName);
			ps.setString(3, className);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				return new NpsAzioniConfig(rs.getInt(IDAOO), 
											rs.getString("METHOD_NAME"),
											rs.getString("CLASS_NAME"),
											rs.getInt("INVIA_POST_EMERGENZA") == 1);
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero delle configurazione NPS per l'aoo " + idAoo, e);
		} finally {
			closeStatement(ps, rs);
		}
		return null;
	}

	/**
	 * @see it.ibm.red.business.dao.INpsConfigurationDAO#getTipologia(java.lang.Boolean,
	 *      java.lang.Integer, java.lang.String, java.sql.Connection).
	 */
	@Override
	public TipologiaDTO getTipologia(final Boolean isIngresso, final Integer idAoo, final String tipologiaNPS, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		TipologiaDTO out = null;
		try {
			Integer flagTipoCategoria = 1;
			if (isIngresso == null || !isIngresso) {
				flagTipoCategoria = 2;
			}
			//1 > entrata
			final String querySQL = "SELECT nps.*, nvl(evo.datadisattivazione,to_date('31/12/9999','dd/mm/yyyy')) as datadisattivazionecalc FROM NPS_TIPOLOGIA nps, TIPOLOGIADOCUMENTO evo WHERE evo.idaoo = nps.npt_id_aoo AND evo.idtipologiadocumento = nps.npt_id_tipo_doc and evo.idtipocategoria = ? AND nps.NPT_ID_AOO = ? AND nps.NPT_TIPOLOGIA = ? order by datadisattivazionecalc desc";
			ps = con.prepareStatement(querySQL);
			ps.setInt(1, flagTipoCategoria);
			ps.setInt(2, idAoo);
			ps.setString(3, tipologiaNPS);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				out = new TipologiaDTO();
				out.setIdTipoDocumento(rs.getInt("NPT_ID_TIPO_DOC"));
				out.setIdTipoProcedimento(rs.getInt("NPT_ID_TIPO_PROC"));
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero della tipologia su NPS_TIPOLOGIA", e);
		} finally {
			closeStatement(ps, rs);
		}
		return out;
	}

	/**
	 * @see it.ibm.red.business.dao.INpsConfigurationDAO#getTipologia(java.lang.Integer,
	 *      it.ibm.red.business.dto.TipologiaDTO, java.sql.Connection).
	 */
	@Override
	public String getTipologia(final Integer idAoo, final TipologiaDTO tipologia, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String out = null;
		try {
			final String querySQL = "SELECT * FROM NPS_TIPOLOGIA WHERE NPT_ID_AOO = ? AND NPT_ID_TIPO_DOC = ? AND NPT_ID_TIPO_PROC = ? ";
			ps = con.prepareStatement(querySQL);
			ps.setInt(1, idAoo);
			ps.setInt(2, tipologia.getIdTipoDocumento());
			ps.setInt(3, tipologia.getIdTipoProcedimento());
			
			rs = ps.executeQuery();
			if (rs.next()) {
				out = rs.getString("NPT_TIPOLOGIA");
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero della tipologia su NPS_TIPOLOGIA", e);
		} finally {
			closeStatement(ps, rs);
		}
		return out;
	}

	/**
	 * @see it.ibm.red.business.dao.INpsConfigurationDAO#getAll(java.sql.Connection).
	 */
	@Override
	public List<NpsConfiguration> getAll(final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		final List<NpsConfiguration> output = new ArrayList<>();
		
		try {
			final String querySQL = "SELECT * FROM Nps_Configuration c";
			ps = con.prepareStatement(querySQL);
			rs = ps.executeQuery();
			while (rs.next()) {
				output.add(new NpsConfiguration(rs.getInt(IDAOO), 
											rs.getString(CODICE_AOO),
											rs.getString(DENOMINAZIONE_AOO),
											rs.getString(CODICE_AMMINISTRAZIONE),
											rs.getString(DENOMINAZIONE_AMMINISTRAZIONE),
											rs.getString(REGISTRO_UFFICIALE),
											rs.getString(DENOMINAZIONE_REGISTRO),
											rs.getString(UTENTE),
											rs.getString(CLIENT),
											rs.getString(PASSWORD),
											rs.getString(SERVIZIO),
											rs.getString(ACTOR),
											rs.getString(APPLICAZIONE),
											rs.getString(UTENTE_ADMIN),
											rs.getString(PASSWORD_ADMIN),
											rs.getString(SERVIZIO_ADMIN)));
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero delle configurazioni NPS", e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

}
