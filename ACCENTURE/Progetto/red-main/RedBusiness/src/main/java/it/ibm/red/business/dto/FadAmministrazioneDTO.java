/**
 * 
 */
package it.ibm.red.business.dto;

/**
 * @author APerquoti
 *
 */
public class FadAmministrazioneDTO extends AbstractDTO  {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 2645639202118873317L;
	
	/**
	 * Codice amministrazione.
	 */
	private String codiceAmministrazione;
	
	/**
	 * Descrizione amministrazione.
	 */
	private String descrizioneAmministrazione;

	/**
	 * FadAmministrazioneDTO.
	 */
	public FadAmministrazioneDTO() { }
	
	/**
	 * @param codiceAmministrazione
	 * @param descrizioneAmministrazione
	 */
	public FadAmministrazioneDTO(final String codiceAmministrazione, final String descrizioneAmministrazione) {
		super();
		this.codiceAmministrazione = codiceAmministrazione;
		this.descrizioneAmministrazione = descrizioneAmministrazione;
	}

	/**
	 * @return the codiceAmministrazione
	 */
	public final String getCodiceAmministrazione() {
		return codiceAmministrazione;
	}

	/**
	 * @return the descrizioneAmministrazione
	 */
	public final String getDescrizioneAmministrazione() {
		return descrizioneAmministrazione;
	}
	
	

}
