package it.ibm.red.business.helper.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.concrete.FepaSRV.ProvenienzaType;
import it.ibm.red.business.utils.DateUtils;

/**
 * Helper ATTO DECRETO Xml.
 */
public class AttoDecretoXmlHelper extends AbstractXmlHelper {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 1524781741709897123L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AttoDecretoXmlHelper.class.getName());

	/**
	 * Nome file allegato.
	 */
	private static final String FILENAME_ALLEGATO = "Allegati";

	/**
	 * Nome file documento.
	 */
	private static final String FILENAME_DOCUMENTO = "Documento";

	/**
	 * Nome file oggetto.
	 */
	private static final String FILENAME_OGGETTO = "Oggetto";

	/**
	 * Attributo nome.
	 */
	private static final String ATTRIBUTO_NOME = "nome";

	/**
	 * Descrizione nome file.
	 */
	private static final String FILENAME_DESCRIZIONE = "Descrizione";

	/**
	 * Metadato associato.
	 */
	private static final String FILENAME_METADATO_ASSOCIATO = "MetadatoAssociato";

	/**
	 * Intestazione.
	 */
	private static final String FILENAME_INTESTAZIONE = "Intestazione";

	/**
	 * Identificatore.
	 */
	private static final String FILENAME_IDENTIFICATORE = "Identificatore";

	/**
	 * Metadat interni.
	 */
	private static final String FILENAME_METADATIINTERNI = "MetadatiInterni";
		
	/**
	 * Codice.
	 */
	private static final String FILENAME_CODEVAL = "Codice";

	/**
	 * Valore.
	 */
	private static final String FILENAME_VALORE = "Valore";
	
	/**
	 * Aoo procollo amministrazione.
	 */
	private static final String AOO_PROT_AMM = "AOO_PROT_AMM";

	/**
	 * Numero protocollo amministrazione.
	 */
	private static final String NUME_PROT_AMM = "NUME_PROT_AMM";

	/**
	 * Data protocollo amministrazione.
	 */
	private static final String DATA_PROT_AMM = "DATA_PROT_AMM";

	/**
	 * Aoo protocollo UCB.
	 */
	private static final String AOO_PROT_UCB = "AOO_PROT_UCB";

	/**
	 * Numero protocollo UCB ingresso.
	 */
	private static final String NUME_PROT_UCB_I = "NUME_PROT_UCB";

	/**
	 * Data protocollo UCB ingresso.
	 */
	private static final String DATA_PROT_UCB_I = "DATA_PROT_UCB";

	/**
	 * Numero protocollo uscita.
	 */
	private static final String NUME_PROT_UCB_U = "NumeroRegistrazione";

	/**
	 * Data protocollo uscita.
	 */
	private static final String DATA_PROT_UCB_U = "DataRegistrazione";

	/**
	 * Ragioneria.
	 */
	private static final String RAGIONERIA = "RAGIONERIA";

	/**
	 * Amministrazione.
	 */
	private static final String AMMINISTRAZIONE = "AMMINISTRAZIONE";

	/**
	 * Costruttore di default.
	 */
	public AttoDecretoXmlHelper() {
		super();
	}

	/**
	 * Imposta la segnatura xml.
	 * @param segnaturaStream
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public void setSegnaturaXml(final InputStream segnaturaStream) {
		super.setXmlDocument(segnaturaStream);
	}

	/**
	 * Restituisce il codice amministrazione.
	 * @return codice amministrazione
	 */
	public String getCodiceAmministrazione() {
		String result = null;
		try {
			result = getValueMetdatoAssociato(AMMINISTRAZIONE);
		} catch (final Exception e) {
			LOGGER.error(e);
		}
		return result;
	}

	/**
	 * Restituisce il codice ragioneria.
	 * 
	 * @return codice ragioneria
	 */
	public String getCodiceRagioneria() {
		String result = null;
		try {
			result = getValueMetdatoAssociato(RAGIONERIA);
		} catch (final Exception e) {
			LOGGER.error(e);
		}
		return result;
	}

	/**
	 * Restituisce il valore associato al metadato di riferimento al protocollo amministrazione.
	 * @return tipo protocollo
	 */
	public DatiProtocollo getProtocolloAmm() {
		DatiProtocollo result = null;
		
		String aoo = null;
		final String tipoProtocollo = Constants.Protocollo.TIPOLOGIA_PROTOCOLLO_USCITA;
		Integer numeroProtocollo = null;
		String dataProtocollo = null;
		
		try {
			
			aoo = getValueMetdatoAssociato(AOO_PROT_AMM);
			numeroProtocollo = Integer.parseInt(getValueMetdatoAssociato(NUME_PROT_AMM));
			dataProtocollo = getValueMetdatoAssociato(DATA_PROT_AMM);
			
			result = new DatiProtocollo(aoo, tipoProtocollo, numeroProtocollo, dataProtocollo, ProvenienzaType.AMMINISTRAZIONE);
		
		} catch (final Exception e) {
			throw new RedException("Errore nel recupero del protocollo in uscita dell'amministrazione", e);
		}
		
		return result;
	}

	/**
	 * Restituisce il tipo protocollo relativo ad ingressi UCB.
	 * @return tipo protocollo definito da TipoProtocollo
	 */
	public DatiProtocollo getProtocolloIngressoUCB() {
		DatiProtocollo result = null;
		
		String aoo = null;
		final String tipoProtocollo = Constants.Protocollo.TIPOLOGIA_PROTOCOLLO_INGRESSO;
		Integer numeroProtocollo = null;
		String dataProtocollo = null;
		
		try {
			
			aoo = getValueMetdatoAssociato(AOO_PROT_UCB);
			numeroProtocollo = Integer.parseInt(getValueMetdatoAssociato(NUME_PROT_UCB_I));
			dataProtocollo = getValueMetdatoAssociato(DATA_PROT_UCB_I);
			
			result = new DatiProtocollo(aoo, tipoProtocollo, numeroProtocollo, dataProtocollo, ProvenienzaType.UCB);
		
		} catch (final Exception e) {
			throw new RedException("Errore nel recupero del protocollo in ingresso dell'UCB", e);
		}
		
		return result;
	}

	/**
	 * Restituisce il tipo protocollo relativo ad uscite UCB.
	 * @return tipo protocollo definito da TipoProtocollo
	 */
	public DatiProtocollo getProtocolloUscitaUCB() {
		DatiProtocollo result = null;
		
		String aoo = null;
		final String tipoProtocollo = Constants.Protocollo.TIPOLOGIA_PROTOCOLLO_USCITA;
		Integer numeroProtocollo = null;
		String dataProtocollo = null;
		
		try {
			
			aoo = getValueMetdatoAssociato(AOO_PROT_UCB);
			numeroProtocollo = Integer.parseInt(getInfoProtocolloUscitaUCB(NUME_PROT_UCB_U));
			dataProtocollo = getInfoProtocolloUscitaUCB(DATA_PROT_UCB_U);
			
			Date convertedDate = null;
			convertedDate = eseguiConversioneData(dataProtocollo);
			
			dataProtocollo = DateUtils.dateToString(convertedDate, true);
			
			result = new DatiProtocollo(aoo, tipoProtocollo, numeroProtocollo, dataProtocollo, ProvenienzaType.UCB);
		
		} catch (final Exception e) {
			throw new RedException("Errore nel recupero del protocollo in uscita dell'UCB", e);
		}
		
		return result;
	}

	/**
	 * Esegue la conversione della data nel formato YYYY_MM_DD. @see DateUtils.
	 * @param dataProtocollo
	 * @return data convertita
	 */
	private static Date eseguiConversioneData(final String dataProtocollo) {
		final SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.YYYY_MM_DD);
		sdf.setTimeZone(TimeZone.getDefault());
		Date convertedDate = null;

		try {
			convertedDate = sdf.parse(dataProtocollo);
		} catch (final ParseException e) {
			throw new RedException("Errore nella conversione della data. " + e);
		}
		return convertedDate;
	}
	
	/**
	 * Restituisce le info sul protocollo uscita.
	 * @param codiceInfo
	 * @return info protocollo uscita
	 */
	private String getInfoProtocolloUscitaUCB(final String codiceInfo) {
		Element eElement = getPrincipalNodeElements();
		eElement = (Element) eElement.getElementsByTagName(FILENAME_INTESTAZIONE).item(0);
		eElement = (Element) eElement.getElementsByTagName(FILENAME_IDENTIFICATORE).item(0);
		eElement = (Element) eElement.getElementsByTagName(codiceInfo).item(0);
		if (eElement != null) {
			return eElement.getTextContent();
		}
		return null;
	}

	/**
	 * Restituisce il valore del metadato associato identificato dal codice.
	 * @param codiceMetadatoAssociato
	 * @return valore metadato
	 */
	private String getValueMetdatoAssociato(final String codiceMetadatoAssociato) {
		String result = null;
		try {
			Node metadatoAssociato = null;
			final NodeList metadatoAssociatoList = getXmlMetadatiInterni().getElementsByTagName(FILENAME_METADATO_ASSOCIATO);
			for (int i = 0; i < metadatoAssociatoList.getLength(); i++) {
				metadatoAssociato = metadatoAssociatoList.item(i);
				if (metadatoAssociato.getNodeType() == Node.ELEMENT_NODE) {
					final Element e = (Element) metadatoAssociato;
					final Node codice = e.getElementsByTagName(FILENAME_CODEVAL).item(0);
					if (codice.getTextContent().equals(codiceMetadatoAssociato)) {
						result = e.getElementsByTagName(FILENAME_VALORE).item(0).getTextContent();
						result = result.trim();
					}
				}
			}
		} catch (final Exception e) {
			throw new RedException("Errore nel recupero di " + codiceMetadatoAssociato, e);
		}
		return result;
	}

	/**
	 * Restituisce i metadati interni.
	 * @return metadati interni come Document
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	private Document getXmlMetadatiInterni() throws SAXException, IOException, ParserConfigurationException {
		return this.getDocumentBuilder().parse(new ByteArrayInputStream((getMetadatiInterni()).getBytes(StandardCharsets.UTF_8)));
	}

	/**
	 * Restituisce i metadati interni come string.
	 * @return metadati interni
	 */
	private String getMetadatiInterni() {
		Element eElement = getPrincipalNodeElements();
		eElement = (Element) eElement.getElementsByTagName(FILENAME_METADATIINTERNI).item(0);
		return eElement.getTextContent();
	}

	/**
	 * Restituisce l'oggetto dell'intestazione.
	 * @return oggetto come stringa
	 */
	public String getOggettoIntestazione() {
		Element eElement = getPrincipalNodeElements();
		eElement = (Element) eElement.getElementsByTagName(FILENAME_INTESTAZIONE).item(0);
		final NodeList oggetto = eElement.getElementsByTagName(FILENAME_OGGETTO);
		final Element oggettoElement = (Element) oggetto.item(0);
		if (oggettoElement != null) {
			return oggettoElement.getTextContent();
		}
		return null;
	}

	/**
	 * Restituisce il nome file del documento principale.
	 * @return nome file come String
	 */
	public String getFileNameDocumentoPrincipale() {
		Element eElement = getPrincipalNodeElements();
		eElement = (Element) eElement.getElementsByTagName(FILENAME_DESCRIZIONE).item(0);
		final NodeList documenti = eElement.getElementsByTagName(FILENAME_DOCUMENTO);
		final Element documento = (Element) documenti.item(0);
		if (documento != null && documento.getAttribute(ATTRIBUTO_NOME) != null) {
			return documento.getAttribute(ATTRIBUTO_NOME);
		}
		return null;
	}

	/**
	 * Restituisce l'oggetto del documento principale.
	 * @return oggetto documento principale.
	 */
	public String getOggettoDocumentoPrincipale() {
		Element eElement = getPrincipalNodeElements();
		eElement = (Element) eElement.getElementsByTagName(FILENAME_DESCRIZIONE).item(0);
		eElement = (Element) eElement.getElementsByTagName(FILENAME_DOCUMENTO).item(0);
		eElement = (Element) eElement.getElementsByTagName(FILENAME_OGGETTO).item(0);
		if (eElement != null) {
			return eElement.getTextContent();
		}
		return null;
	}

	/**
	 * Restituisce il numero degli allegati.
	 * @return numero allegati
	 */
	public int getNumeroAllegati() {
		Element eElement = getPrincipalNodeElements();
		
		if (eElement.getElementsByTagName(FILENAME_ALLEGATO) != null 
				&& eElement.getElementsByTagName(FILENAME_ALLEGATO).getLength() > 0) {
			eElement = (Element) eElement.getElementsByTagName(FILENAME_ALLEGATO).item(0);
			final NodeList documenti = eElement.getElementsByTagName(FILENAME_DOCUMENTO);
			if (documenti != null) {
				return documenti.getLength();
			}
		}
		
		return 0;
	}

	/**
	 * Restituisce l'oggetto di uno specifico allegato.
	 * @param indiceAllegato per il quale occorre l'oggetto
	 * @return oggetto dell'allegato specificato
	 */
	public String getOggettoAllegato(final int indiceAllegato) {
		Element eElement = getPrincipalNodeElements();
		
		if (eElement.getElementsByTagName(FILENAME_ALLEGATO) != null 
				&& eElement.getElementsByTagName(FILENAME_ALLEGATO).getLength() > 0) {
			eElement = (Element) eElement.getElementsByTagName(FILENAME_ALLEGATO).item(0);
			final NodeList documenti = eElement.getElementsByTagName(FILENAME_DOCUMENTO);
			if (documenti != null && indiceAllegato <= documenti.getLength()) {
				final Element oggettoElement = (Element) documenti.item(indiceAllegato);
				final NodeList oggetto = oggettoElement.getElementsByTagName(FILENAME_OGGETTO);
				if (oggetto.getLength() > 0) {
					return oggetto.item(0).getTextContent();
				}
			}
		}
		
		return null;
	}

	/**
	 * Restituisce il nome del file dell'allegato specificato.
	 * @param indiceAllegato per il quale occorre il nome file
	 * @return nome file dell'allegato specificato dall'indice
	 */
	public String getFileNameAllegato(final int indiceAllegato) {
		Element eElement = getPrincipalNodeElements();
		
		if (eElement.getElementsByTagName(FILENAME_ALLEGATO) != null 
				&& eElement.getElementsByTagName(FILENAME_ALLEGATO).getLength() > 0) {
			eElement = (Element) eElement.getElementsByTagName(FILENAME_ALLEGATO).item(0);
			final NodeList documenti = eElement.getElementsByTagName(FILENAME_DOCUMENTO);
			if (documenti != null && indiceAllegato <= documenti.getLength()) {
				return documenti.item(indiceAllegato).getAttributes().getNamedItem(ATTRIBUTO_NOME).getTextContent();
			}
		}
		
		return null;
	}

}
