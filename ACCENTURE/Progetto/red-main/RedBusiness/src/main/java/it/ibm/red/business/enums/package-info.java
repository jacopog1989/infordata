/**
 * @author CPIERASC
 *
 *	Questo package conterrà gli enum applicativi: in molti casi per esplicitare il dominio di una entità si utilizzano
 *	gli enum in maniera tale che l'utilizzatore non possa improvvisare dei valori, ma dovrà sceglierne uno di quelli
 *	censiti.
 *
 */
package it.ibm.red.business.enums;
