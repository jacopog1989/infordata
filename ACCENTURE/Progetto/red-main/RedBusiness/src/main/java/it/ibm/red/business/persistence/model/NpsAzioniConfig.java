package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * The Class NpsAzioniConfig.
 *
 * @author a.dilegge
 * 
 *         Entità che mappa la tabella NPS_AZIONI_CONFIG.
 */
public class NpsAzioniConfig implements Serializable {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/** 
	 * Identificativo Aoo 
	 */
	private Integer idAoo;
	
	/** 
	 * Nome del metodo 
	 */
	private String methodName;
	
	/** 
	 * Nome della classe 
	 */
	private String className;
	
	/** 
	 * Invio post emergenza 
	 */
	private boolean inviaPostEmergenza;
	
	/**
	 * Costruttore nps azioni config.
	 *
	 * @param idAoo the id aoo
	 * @param methodName the method name
	 * @param className the class name
	 * @param inviaPostEmergenza the invia post emergenza
	 */
	public NpsAzioniConfig(final Integer idAoo, final String methodName, final String className, final boolean inviaPostEmergenza) {
		super();
		this.idAoo = idAoo;
		this.methodName = methodName;
		this.className = className;
		this.inviaPostEmergenza = inviaPostEmergenza;
	}
	
	/**
	 * Getter id aoo.
	 *
	 * @return the id aoo
	 */
	public Integer getIdAoo() {
		return idAoo;
	}
	
	/**
	 * Setter id aoo.
	 *
	 * @param idAoo the new id aoo
	 */
	public void setIdAoo(final Integer idAoo) {
		this.idAoo = idAoo;
	}
	
	/**
	 * Getter method name.
	 *
	 * @return the method name
	 */
	public String getMethodName() {
		return methodName;
	}
	
	/**
	 * Setter method name.
	 *
	 * @param methodName the new method name
	 */
	public void setMethodName(final String methodName) {
		this.methodName = methodName;
	}
	
	/**
	 * Getter class name.
	 *
	 * @return the class name
	 */
	public String getClassName() {
		return className;
	}
	
	/**
	 * Setter class name.
	 *
	 * @param className the new class name
	 */
	public void setClassName(final String className) {
		this.className = className;
	}
	
	/**
	 * Controllo se è necessario inviare post emergenza.
	 *
	 * @return true, se invia post emergenza
	 */
	public boolean isInviaPostEmergenza() {
		return inviaPostEmergenza;
	}
	
	/**
	 * Setter del invia post emergenza.
	 *
	 * @param inviaPostEmergenza the new invia post emergenza
	 */
	public void setInviaPostEmergenza(final boolean inviaPostEmergenza) {
		this.inviaPostEmergenza = inviaPostEmergenza;
	}

}