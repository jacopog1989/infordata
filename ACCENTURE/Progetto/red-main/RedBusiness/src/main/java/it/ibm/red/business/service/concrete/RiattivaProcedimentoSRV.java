package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.IEventoLogDAO;
import it.ibm.red.business.dao.IFascicoloDAO;
import it.ibm.red.business.dao.IIterApprovativoDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EventoLogDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.builder.Builder;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IEventoLogSRV;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.IOperationWorkFlowSRV;
import it.ibm.red.business.service.IRiattivaProcedimentoSRV;

/**
 * Service che gestisce la funzionalità di riattivazione procedimento.
 */
@Service
public class RiattivaProcedimentoSRV extends AbstractService implements IRiattivaProcedimentoSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 323163442948463693L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RiattivaProcedimentoSRV.class.getName());

	/**
	 * Messaggio errore riattivazione workflow.
	 */
	private static final String ERROR_RIATTIVAZIONE_WORKFLOW = "Errore nella riattivazione del workflow per il documento ";

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * DAO.
	 */
	@Autowired
	private IEventoLogDAO eventLogDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IFascicoloDAO fascicoloDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IIterApprovativoDAO iterApprovativoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * Service.
	 */
	@Autowired
	private INpsSRV npsSRV;
	
	/**
	 * Service operazioni su wf.
	 */
	@Autowired
	private IOperationWorkFlowSRV opWorkflowSRV;

	/**
	 * Service gestione eventi.
	 */
	@Autowired
	private IEventoLogSRV eventoSRV;

	/**
	 * Post construct service.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRiattivaProcedimentoFacadeSRV#riattiva(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String).
	 */
	@Override
	public EsitoOperazioneDTO riattiva(final String documentTitle, final UtenteDTO utente, final String motivazione) {
		return riattiva(documentTitle, utente, motivazione, false);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRiattivaProcedimentoFacadeSRV#riattiva(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String, boolean).
	 */
	@Override
	public EsitoOperazioneDTO riattiva(final String documentTitle, final UtenteDTO utente, final String motivazione, final boolean fromPredisponi) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(0);
		Integer numDoc = null;
		Connection connection = null;
		Connection filenetConnection = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			filenetConnection = setupConnection(getFilenetDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final long idAoo = utente.getIdAoo();

			final Document document = fceh.getDocumentByDTandAOO(documentTitle, idAoo);
			
			numDoc = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			final Integer idCategoriaDoc = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY);

			final boolean isEntrata = CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0].equals(idCategoriaDoc)
					|| CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA.getIds()[0].equals(idCategoriaDoc)
					|| CategoriaDocumentoEnum.INTERNO.getIds()[0].equals(idCategoriaDoc);

			EventoLogDTO eventLog = null;
			if (!fromPredisponi) {
				eventLog = eventLogDAO.getAttiEvent(filenetConnection, documentTitle, idAoo);
			} else {
				eventLog = eventLogDAO.getPredisponiEvent(documentTitle, idAoo, filenetConnection);
			}
			if(eventLog == null) {
				eventLog = eventLogDAO.getPredisponiEventWfAperto(documentTitle, idAoo, filenetConnection);
			}
			VWWorkObject workflowEntrata = fpeh.getWorkflowPrincipale(documentTitle, utente.getFcDTO().getIdClientAoo());
			final boolean hasWorkflowPrincipale = workflowEntrata != null;

			if (eventLog!=null && isEntrata && !hasWorkflowPrincipale) {

				String idNodoDestinatarioNuovo = null;
				final String idNodoDestinatario = eventLog.getIdUfficioMittente() + Constants.EMPTY_STRING;
				String idUtenteDestinatario = eventLog.getIdUtenteMittente() + Constants.EMPTY_STRING;
				String msg = Constants.EMPTY_STRING;

				if (idUtenteDestinatario != null && !"0".equals(idUtenteDestinatario)) {
					final Utente utenteDes = utenteDAO.getUtente(eventLog.getIdUtenteMittente(), connection);
					if (utenteDes.getDataDisattivazione() != null) {
						if (StringUtils.isNotBlank(motivazione)) {
							if (idNodoDestinatario != null && !"0".equals(idNodoDestinatario)) {
								final Nodo nodoCorriereSegreteria = nodoDAO.getCorriereSegreteriaFromSottoNodo(eventLog.getIdUfficioMittente().intValue(), connection);

								idNodoDestinatarioNuovo = String.valueOf(nodoCorriereSegreteria.getIdNodo());
								idUtenteDestinatario = "0";
								msg += "al Corriere della segreteria d'Ispettorato " + nodoCorriereSegreteria.getDescrizione() + ".";
							}
						} else {
							throw new RedException("Il campo motivazione è obbligatorio quando l'utente originale è disattivato.");
						}
					} else if (idNodoDestinatario != null && !"".equals(idNodoDestinatario) && !"0".equals(idNodoDestinatario)) {
						msg += "all'Utente " + utenteDes.getNome() + " " + utenteDes.getCognome() + " ";
						final UfficioDTO nodoPostMigrazione = nodoDAO.getNewIdFromTabMigrazione(Long.parseLong(idNodoDestinatario), connection);
						if (nodoPostMigrazione != null) {
							idNodoDestinatarioNuovo = String.valueOf(nodoPostMigrazione.getId());
							msg += "dell'Ufficio " + nodoPostMigrazione.getDescrizione() + ".";
						} else {
							idNodoDestinatarioNuovo = idNodoDestinatario;
							msg += "dell'Ufficio " + nodoDAO.getNodo(Long.parseLong(idNodoDestinatario), connection).getDescrizione() + ".";
						}
					}
				}

				if (idNodoDestinatarioNuovo != null && !"".equals(idNodoDestinatarioNuovo) && !"0".equals(idNodoDestinatarioNuovo)) {
					final String wfName = iterApprovativoDAO.getWFNameGenerico(idAoo, connection);
					final int idFascicoloAuto = fascicoloDAO.getIdFascicoloProcedimentale(documentTitle, idAoo, connection);
					final Aoo aoo = aooDAO.getAoo(utente.getIdAoo(), connection);
					final HashMap<String, Object> map = new HashMap<>();

					map.put(pp.getParameterByKey(PropertiesNameEnum.ID_CLIENT_METAKEY), aoo.getAooFilenet().getIdClientAoo());
					map.put(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY), documentTitle);
					map.put(pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY), idFascicoloAuto);
					map.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), utente.getIdUfficio());
					map.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId());
					map.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), idNodoDestinatarioNuovo);
					map.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), idUtenteDestinatario);
					map.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivazione);
					map.put(pp.getParameterByKey(PropertiesNameEnum.FLAG_RIATTIVA), 1);

					final String newWobNumber = fpeh.avviaWF(wfName, map, Integer.parseInt(documentTitle), aoo.getAooFilenet().getIdClientAoo(), true);

					if (StringUtils.isBlank(newWobNumber)) {
						throw new RedException("Errore nella creazione del WF.");
					}
				} else {
					throw new RedException("Errore nel recuperare il nuovo destinatario.");
				}
				// Cambio stato protocollo NPS
				if (TipologiaProtocollazioneEnum.isProtocolloNPS(utente.getIdTipoProtocollo())) {
					final String idProtocollo = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);
					if (idProtocollo != null) {
						final Integer numeroProtocollo = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
						final Integer annoProtocollo = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);

						final String numeroAnnoProtocollo = numeroProtocollo + "/" + annoProtocollo;

						npsSRV.cambiaStatoProtEntrata(idProtocollo, numeroAnnoProtocollo, utente, Builder.IN_LAVORAZIONE, documentTitle, connection);
					}
				}
				esito.setEsito(true);
				esito.setIdDocumento(numDoc);
				esito.setNote("Il documento è stato riattivato correttamente e assegnato " + msg);
				
			} else if(isEntrata && hasWorkflowPrincipale) {
				
				if(fpeh.isWobInCoda(workflowEntrata.getWorkObjectNumber(), DocumentQueueEnum.SOSPESO)) {
					
					opWorkflowSRV.spostaInLavorazione(utente, workflowEntrata.getWorkObjectNumber());
					esito.setNote("Il documento è stato riattivato correttamente ed accodato in 'Da Lavorare'.");
					
				} else if(fpeh.isWobInCoda(workflowEntrata.getWorkObjectNumber(), DocumentQueueEnum.IN_LAVORAZIONE_UCB)) {
					final Map<String, Object> metadati = new HashMap<>();
					
					metadati.put(pp.getParameterByKey(PropertiesNameEnum.IN_LAVORAZIONE_METAKEY), BooleanFlagEnum.NO.getIntValue());
					fpeh.updateWorkflow(workflowEntrata, metadati);

					String motivoAssegnamento = "Assegnazione per competenza in seguito ad un annullamento.";
					aggiornaStorico(document, utente, motivoAssegnamento , filenetConnection);
					
					esito.setNote("Il documento è stato riattivato correttamente ed accodato in 'Da Lavorare'.");

				} else {
					esito.setNote("Impossibile riattivare il documento " + numDoc);
				}
				esito.setEsito(true);
				esito.setIdDocumento(numDoc);
				
			} else {
				throw new RedException("Il documento: " + numDoc + " ha procedimenti ancora attivi oppure non è un documento in entrata.");
			}
		} catch (final RedException e) {
			LOGGER.error(ERROR_RIATTIVAZIONE_WORKFLOW + documentTitle + ".", e);
			esito.setIdDocumento(numDoc);
			esito.setNote(e.getMessage());
		} catch (final Exception e) {
			LOGGER.error(ERROR_RIATTIVAZIONE_WORKFLOW + documentTitle + ".", e);
			esito.setIdDocumento(numDoc);
			esito.setNote("Errore nella riattivazione del workflow.");
		} finally {
			closeConnection(connection);
			closeConnection(filenetConnection);
			logoff(fpeh);
			popSubject(fceh);
		}

		return esito;
	}

	/**
	 * Aggiorna lo storico del {@code document} inserendo lo step di assegnazione
	 * per competenza all' {@code utente}.
	 * 
	 * Note: Il cast sull'object viene fatto sopprimendo il warning poiché è sempre
	 * possibile effettuare il cast a {@code List<String>} del metadato
	 * {@link PropertiesNameEnum#ASSEGNATARI_COMPETENZA_METAKEY}.
	 * 
	 * @param document
	 *            Documento il quale storico viene aggiornato.
	 * @param utente
	 *            Utente assegnatario per competenza del {@code document}.
	 * @param motivazioneAssegnazione
	 *            Motivo dell' assegnazione per competenza.
	 * @param filenetConnection
	 *            Connession a DWH per l'aggiornamento della <em> event log </em>.
	 */
	@SuppressWarnings("unchecked")
	private void aggiornaStorico(final Document document, final UtenteDTO utente, String motivazioneAssegnazione, Connection filenetConnection) {
		String documentTitle = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
		List<String> assegnatari = (List<String>) TrasformerCE.getMetadato(document, PropertiesNameEnum.ASSEGNATARI_COMPETENZA_METAKEY);
		
		Long idUfficio = Long.valueOf(assegnatari.get(0).split(",")[0]);
		Long idUtente = Long.valueOf(assegnatari.get(0).split(",")[1]);

		eventoSRV.inserisciEventoLog(Integer.valueOf(documentTitle), utente.getIdUfficio(), 0L, idUfficio, 
				idUtente, Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()),
				EventTypeEnum.ASSEGNAZIONE_DIRETTA_A_UTENTE, motivazioneAssegnazione , 0, utente.getIdAoo(), filenetConnection);
	}

	/**
	 * Sarebbe meglio usare questo controllo anche dentro a riattiva.
	 */
	@Override
	public boolean isRiattivabile(final String documentTitle, final UtenteDTO utente) {
		final long idAoo = utente.getIdAoo();
		Connection con = null;
		Connection filenetCon = null;
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			filenetCon = setupConnection(getFilenetDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			final Document d = fceh.getDocumentByDTandAOO(documentTitle, idAoo);
			final Integer idCategoriaDoc = (Integer) TrasformerCE.getMetadato(d, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY);

			final boolean checkCategoria = CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0].equals(idCategoriaDoc)
					|| CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA.getIds()[0].equals(idCategoriaDoc)
					|| CategoriaDocumentoEnum.INTERNO.getIds()[0].equals(idCategoriaDoc);
			if (!checkCategoria) {
				return checkCategoria;
			}

			final boolean hasWorkflowPrincipale = fpeh.getWorkflowPrincipale(documentTitle, utente.getFcDTO().getIdClientAoo()) != null;
			if (hasWorkflowPrincipale) {
				return false;
			}

			return eventLogDAO.getAttiEvent(filenetCon, documentTitle, idAoo) != null;

		} catch (final Exception e) {
			LOGGER.error(ERROR_RIATTIVAZIONE_WORKFLOW + documentTitle + ".", e);
			return false;
		} finally {
			closeConnection(con);
			closeConnection(filenetCon);
			popSubject(fceh);
		}
	}
}