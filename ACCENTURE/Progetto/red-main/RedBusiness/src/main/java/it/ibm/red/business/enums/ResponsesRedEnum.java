/**
 * 
 */
package it.ibm.red.business.enums;

import java.util.Collection;

import it.ibm.red.business.dto.ResponsesDTO;
import it.ibm.red.business.utils.StringUtils;

/**
 * Enum per la gestione delle response Applicative e Filenet.
 * 
 * @author APerquoti
 * 
 *
 */
public enum ResponsesRedEnum {

	/**
	 * Valore.
	 */
	INSERISCI_NOTA(1, ExecutionTypeEnum.MULTI, SourceTypeEnum.APP, null, TaskTypeEnum.HUMAN_TASK, "InserisciNotaBean", "Inserisci Nota", "fa fa-edit",
			PostExecEnum.AGGIORNA_ITEM, false),

	/**
	 * Valore.
	 */
	TRACCIA_PROCEDIMENTO(2, "Traccia Procedimento", TaskTypeEnum.SERVICE_TASK, "tracciaProcedimento", "Traccia Procedimento", "fa fa-bullseye", PostExecEnum.NESSUNA_AZIONE,
			false),

	/**
	 * Valore.
	 */
	NO_TRACCIA_PROCEDIMENTO(3, "Non Tracciare Procedimento", TaskTypeEnum.SERVICE_TASK, "noTracciaProcedimento", "Non Tracciare Procedimento", "fa fa-trash",
			PostExecEnum.NESSUNA_AZIONE, false),

	/**
	 * Valore.
	 */
	// SPEDISCI_MAIL(4, ExecutionTypeEnum.MULTI, SourceTypeEnum.APP, null,
	// TaskTypeEnum.HUMAN_TASK, "dlgSpedisciMail", "Spedisci Mail", "fa
	// fa-paper-plane"),
	SPEDISCI_MAIL(4, ExecutionTypeEnum.MULTI, SourceTypeEnum.FILENET, "Spedisci mail", TaskTypeEnum.SERVICE_TASK, "spedisciMail", "Spedisci Mail", ResponsesRedEnum.FA_FA_PAPER_PLANE,
			PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	VISTO(5, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Visto", TaskTypeEnum.HUMAN_TASK, "VistoBean", "Visto", ResponsesRedEnum.FA_FA_CHECK, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	VERIFICATO(6, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Verificato", TaskTypeEnum.HUMAN_TASK, "VerificatoBean", "Verificato", ResponsesRedEnum.FA_FA_CHECK,
			PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	SIGLA_E_INVIA(8, "Sigla e Invia", TaskTypeEnum.HUMAN_TASK, "SiglaBean", "Sigla e Invia", ResponsesRedEnum.FA_FA_CHECK, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	RIFIUTA(9, "Rifiuta", TaskTypeEnum.HUMAN_TASK, ResponsesRedEnum.RIFIUTA_BEAN, "Rifiuta", ResponsesRedEnum.FA_FA_BAN, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	MODIFICA(10, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Modifica", TaskTypeEnum.BEAN_TASK, "modificaApplet", "Modifica", "fa fa-pencil-square-o",
			PostExecEnum.NESSUNA_AZIONE, false),

	/**
	 * Valore.
	 */
	ASSEGNA_UFFICIO_UCP(11, "Assegna a Ufficio UCP", TaskTypeEnum.SERVICE_TASK, "assegnaUfficioUCP", "Assegna a Uffico UCP", ResponsesRedEnum.FA_FA_USERS, PostExecEnum.DISABILITA_ITEM,
			false),
	/**
	 * Valore.
	 */
	ASSEGNA_UFFICIO_UCP_2(110, "Assegna a ufficio UCP", TaskTypeEnum.SERVICE_TASK, "assegnaUfficioUCP", "Assegna a Uffico UCP", ResponsesRedEnum.FA_FA_USERS, PostExecEnum.DISABILITA_ITEM,
			false),

	/**
	 * Valore.
	 */
	ASSOCIA_DECRETI(12, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Associa Decreti", TaskTypeEnum.HUMAN_TASK, "AssociaDecretiBean", "Associa Decreti", "fa fa-chain",
			PostExecEnum.AGGIORNA_ITEM, false),

	/**
	 * Valore.
	 */
	MODIFICA_ASSOCIAZIONI(13, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Modifica Associazioni", TaskTypeEnum.HUMAN_TASK, "ModificaAssociazioniDecretiBean",
			"Modifica Associazioni", ResponsesRedEnum.FA_FA_DROPBOX, PostExecEnum.AGGIORNA_ITEM, false),

	/**
	 * Valore.
	 */
	ATTI(14, "Atti", TaskTypeEnum.HUMAN_TASK, "AttiBean", "Atti", "fa fa-archive", PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	CHIUDI(15, "Chiudi", TaskTypeEnum.SERVICE_TASK, "chiudi", "Chiudi", ResponsesRedEnum.FA_FA_BAN, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	// Qusta Response NON viene attivata dai Master ma solo a seguito di un
	// 'aggiornaDocumento' e sotto delle specifiche condizioni. Utilizzata per
	// avanzare il WorkFlow.
	DA_PROTOCOLLARE(16, "Da protocollare", TaskTypeEnum.BEAN_TASK, "daProtocollare", "Da Protocollare", "fa fa-file-powerpoint-o", PostExecEnum.AGGIORNA_ITEM, false),

	/**
	 * Valore.
	 */
	FIRMA_AUTOGRAFA(17, ExecutionTypeEnum.MULTI, SourceTypeEnum.APP, "Firma autografa", TaskTypeEnum.BEAN_TASK, "firmaAutografa", "Firma Autografa", ResponsesRedEnum.FA_FA_PENCIL,
			PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	FIRMA_REMOTA(18, ExecutionTypeEnum.MULTI, SourceTypeEnum.APP, "firma remota", TaskTypeEnum.BEAN_TASK, "firmaRemota", "Firma Remota", ResponsesRedEnum.FA_FA_PENCIL,
			PostExecEnum.DISABILITA_ITEM, false),
	/**
	 * Valore.
	 */
	PREDISPONI_DOCUMENTO(19, "Predisponi documento", TaskTypeEnum.BEAN_TASK, "predisponiDocumento", "Predisponi Documento", ResponsesRedEnum.FA_FA_KEYBOARD_O, PostExecEnum.DISABILITA_ITEM,
			false),

	/**
	 * Valore.
	 */
	PREDISPONI_DICHIARAZIONE(20, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Predisponi dichiarazione", TaskTypeEnum.BEAN_TASK, "predisponiDichiarazione",
			"Predisponi Dichiarazione", ResponsesRedEnum.FA_FA_KEYBOARD_O, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	SPEDITO(21, "Spedito", TaskTypeEnum.HUMAN_TASK, "SpeditoBean", "Spedito", ResponsesRedEnum.FA_FA_CHECK, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	STAMPA_PROTOCOLLO(22, "Stampa Protocollo", TaskTypeEnum.BEAN_TASK, "stampaProtocolloApplet", "Stampa Protocollo", "fa fa-print", PostExecEnum.NESSUNA_AZIONE, false),

	/**
	 * Valore.
	 */
	INVIA_UFFICIO_PROPONENTE(23, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Invia a Ufficio proponente", TaskTypeEnum.HUMAN_TASK, "UfficioProponenteBean",
			"Invia a Ufficio Proponente", ResponsesRedEnum.FA_FA_PAPER_PLANE, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	FIRMATO_E_SPEDITO(24, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Firmato e spedito", TaskTypeEnum.BEAN_TASK, "firmatoESpedito", "Firmato e Spedito",
			ResponsesRedEnum.FA_FA_PENCIL, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	FIRMATO(25, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Firmato", TaskTypeEnum.BEAN_TASK, "firmato", "Firmato", ResponsesRedEnum.FA_FA_PENCIL, PostExecEnum.DISABILITA_ITEM,
			false),

	/**
	 * Valore.
	 */
	INVIA_UCP(26, "Invia a UCP", TaskTypeEnum.SERVICE_TASK, "inviaUCP", "Invia a UCP", ResponsesRedEnum.FA_FA_PAPER_PLANE, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	PREDISPOSIZIONE_FIRMA_AUTOGRAFA(27, "Predisposizione per firma autografa", TaskTypeEnum.HUMAN_TASK, "PredisposizioneFirmaAutografaBean",
			"Predisposizione per Firma Autografa", ResponsesRedEnum.FA_FA_KEYBOARD_O, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	REINVIA(28, "Reinvia", TaskTypeEnum.SERVICE_TASK, "reinvia", "Reinvia", ResponsesRedEnum.FA_FA_PAPER_PLANE, PostExecEnum.NESSUNA_AZIONE, false),

	/**
	 * Valore.
	 */
	RICHIEDI_VERIFICA(29, ExecutionTypeEnum.MULTI, SourceTypeEnum.FILENET, "Richiedi verifica", TaskTypeEnum.HUMAN_TASK, "RichiediVerificaBean", "Richiedi Verifica",
			ResponsesRedEnum.FA_FA_BULLHORN, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	RICHIEDI_VISTO(30, "Richiedi visto", TaskTypeEnum.HUMAN_TASK, "RichiediVistiBean", "Richiedi Visto", ResponsesRedEnum.FA_FA_BULLHORN, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	RICHIEDI_VISTO_ISPETTORE(31, "Richiedi visto ispettore", TaskTypeEnum.HUMAN_TASK, "RichiediVistoIspettoreBean", "Richiedi Visto Ispettore", ResponsesRedEnum.FA_FA_BULLHORN,
			PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	RICHIEDI_VISTO_UFFICIO(32, ExecutionTypeEnum.MULTI, SourceTypeEnum.FILENET, "Richiedi visto ufficio", TaskTypeEnum.HUMAN_TASK, "RichiediVistoUfficioBean",
			"Richiedi Visto Ufficio", ResponsesRedEnum.FA_FA_BULLHORN, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	RICHIEDI_VISTO_INTERNO(33, ExecutionTypeEnum.MULTI, SourceTypeEnum.FILENET, "Richiedi visto interno", TaskTypeEnum.HUMAN_TASK, "RichiediVistoUfficioBean",
			"Richiedi Visto Interno", ResponsesRedEnum.FA_FA_BULLHORN, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	RICHIESTA_VISTI(34, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Richiesta visti", TaskTypeEnum.HUMAN_TASK, "RichiestaVistiBean", "Richiesta visti",
			ResponsesRedEnum.FA_FA_BULLHORN, PostExecEnum.DISABILITA_ITEM, false),
	/**
	 * Valore.
	 */
	RICHIESTA_VISTI_2(340, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Richiesta Visti", TaskTypeEnum.HUMAN_TASK, "RichiestaVistiBean", "Richiesta Visti",
			ResponsesRedEnum.FA_FA_BULLHORN, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	SIGLA_RICHIEDI_VISTI(35, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Sigla e Richiedi visti", TaskTypeEnum.HUMAN_TASK, "SiglaERichiediVistiBean",
			"Sigla e Richiedi Visti", ResponsesRedEnum.FA_FA_CHECK, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	RIFIUTA_SPEDIZIONE(36, "Rifiuta Spedizione", TaskTypeEnum.HUMAN_TASK, "RifiutaSpedizioneBean", "Rifiuta Spedizione", ResponsesRedEnum.FA_FA_BAN, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	INSERISCI_CONTRIBUTO(37, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Inserisci contributo", TaskTypeEnum.HUMAN_TASK, ResponsesRedEnum.INSERISCI_CONTRIBUTO_BEAN,
			"Inserisci contributo", "fa fa-plus-square-o", PostExecEnum.DISABILITA_ITEM, false),
	/**
	 * Valore.
	 */
	INSERISCI_CONTRIBUTO_2(370, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Inserisci Contributo", TaskTypeEnum.HUMAN_TASK, ResponsesRedEnum.INSERISCI_CONTRIBUTO_BEAN,
			"Inserisci Contributo", "fa fa-plus-square-o", PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	VALIDA_CONTRIBUTI(38, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Valida Contributi", TaskTypeEnum.HUMAN_TASK, ResponsesRedEnum.INSERISCI_CONTRIBUTO_BEAN, "Valida Contributi",
			"fa fa-certificate", PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	CHIUDI_SENZA_ASSOCIAZIONE(39, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Chiudi senza Associazione", TaskTypeEnum.HUMAN_TASK, "ChiudiSenzaAssociazioneBean",
			"Chiudi Senza Associazione", ResponsesRedEnum.FA_FA_BAN, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	INVIO_IN_FIRMA(40, "Invio in firma", TaskTypeEnum.HUMAN_TASK, "InviaInFirmaSiglaVistoBean", "Invio in Firma", ResponsesRedEnum.FA_FA_PAPER_PLANE, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	INVIO_IN_SIGLA(41, "Invio in sigla", TaskTypeEnum.HUMAN_TASK, "InviaInFirmaSiglaVistoBean", "Invio in Sigla", ResponsesRedEnum.FA_FA_PAPER_PLANE, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	METTI_IN_CONOSCENZA(42, "Metti in conoscenza", TaskTypeEnum.HUMAN_TASK, "MettiInConoscenzaBean", "Metti in Conoscenza", ResponsesRedEnum.FA_FA_DROPBOX, PostExecEnum.NESSUNA_AZIONE,
			false),

	/**
	 * Valore.
	 */
	MODIFICA_ITER(43, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Modifica iter", TaskTypeEnum.HUMAN_TASK, "ModificaIterBean", "Modifica Iter", "fa fa-pencil-square-o",
			PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	STORNA(44, "Storna", TaskTypeEnum.HUMAN_TASK, "StornaBean", "Storna", ResponsesRedEnum.FA_FA_USERS, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	STORNA_UTENTE(45, "Storna a utente", TaskTypeEnum.HUMAN_TASK, "StornaAUtenteBean", "Storna a Utente", ResponsesRedEnum.FA_FA_USERS, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	ASSEGNA(46, ResponsesRedEnum.ASSEGNA_RESPONSE, TaskTypeEnum.HUMAN_TASK, "AssegnaBean", ResponsesRedEnum.ASSEGNA_RESPONSE, ResponsesRedEnum.FA_FA_USERS, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	RICHIEDI_CONTRIBUTO(47, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Richiedi contributo", TaskTypeEnum.HUMAN_TASK, ResponsesRedEnum.RICHIEDI_CONTRIBUTO_BEAN, "Richiedi Contributo",
			ResponsesRedEnum.FA_FA_BULLHORN, PostExecEnum.NESSUNA_AZIONE, false),

	/**
	 * Valore.
	 */
	RICHIEDI_CONTRIBUTO_INTERNO(48, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Richiedi contributo interno", TaskTypeEnum.HUMAN_TASK, ResponsesRedEnum.RICHIEDI_CONTRIBUTO_BEAN,
			"Richiedi Contributo Interno", ResponsesRedEnum.FA_FA_BULLHORN, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	RICHIEDI_CONTRIBUTO_UTENTE(49, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Richiedi contributo a utente", TaskTypeEnum.HUMAN_TASK, ResponsesRedEnum.RICHIEDI_CONTRIBUTO_BEAN,
			"Richiedi Contributo a Utente", ResponsesRedEnum.FA_FA_BULLHORN, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	RICHIESTA_CONTRIBUTO(50, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Richiesta contributo", TaskTypeEnum.HUMAN_TASK, ResponsesRedEnum.RICHIEDI_CONTRIBUTO_BEAN,
			"Richiesta Contributo", ResponsesRedEnum.FA_FA_BULLHORN, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	ELIMINA(51, "Elimina", TaskTypeEnum.SERVICE_TASK, "eliminaPrecensiti", "Elimina", "fa fa-trash", PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	FALDONA(52, "Faldona", TaskTypeEnum.HUMAN_TASK, "FaldonaBean", "Faldona", "fa fa-folder", PostExecEnum.NESSUNA_AZIONE, false),

	/**
	 * Valore.
	 */
	INVIO_DECRETO_FIRMA(53, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Invio Decreto in Firma", TaskTypeEnum.HUMAN_TASK, "InvioDecretoFirmaBean",
			"Invio Decreto in Firma", ResponsesRedEnum.FA_FA_PAPER_PLANE, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	FIRMA_DIGITALE(54, "Firma digitale", TaskTypeEnum.BEAN_TASK, "firmaDigitale", "Firma Digitale", ResponsesRedEnum.FA_FA_PENCIL, PostExecEnum.NESSUNA_AZIONE, false),

	/**
	 * Valore.
	 */
	PRESA_VISIONE(55, "Presa Visione", TaskTypeEnum.SERVICE_TASK, "presaVisione", "Presa Visione", ResponsesRedEnum.FA_FA_CHECK, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	PROCEDI(56, "Procedi", TaskTypeEnum.SERVICE_TASK, "procedi", "Procedi", ResponsesRedEnum.FA_FA_SHARE_SQUARE_O, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	PROSEGUI(57, "Prosegui", TaskTypeEnum.SERVICE_TASK, "prosegui", "Prosegui", ResponsesRedEnum.FA_FA_SHARE_SQUARE_O, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	INVIA_AL_DIRETTORE(58, "Invia al Direttore", TaskTypeEnum.SERVICE_TASK, "inviaDirettore", "Invia al Direttore", ResponsesRedEnum.FA_FA_PAPER_PLANE, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	INVIA_ISPETTORE(59, "Invia all'Ispettore", TaskTypeEnum.SERVICE_TASK, "inviaIspettore", "Invia all'Ispettore", ResponsesRedEnum.FA_FA_PAPER_PLANE, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	STORNA_AL_CORRIERE(60, "Storna al corriere", TaskTypeEnum.HUMAN_TASK, "StornaACorriereBean", "Storna al Corriere", ResponsesRedEnum.FA_FA_USERS, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	METTI_IN_SOSPESO(61, "Metti in sospeso", TaskTypeEnum.SERVICE_TASK, "mettiInSospeso", "Metti in Sospeso", ResponsesRedEnum.FA_FA_DROPBOX, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	METTI_ATTI(62, "Metti agli Atti", TaskTypeEnum.SERVICE_TASK, "MettiagliAtti", "Metti agli Atti", ResponsesRedEnum.FA_FA_DROPBOX, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	INVIA_UFFICIO_COORDINAMENTO(63, "Invia a ufficio di coordinamento", TaskTypeEnum.SERVICE_TASK, "inviaUffCoordinamento", "Invia a Ufficio di Coordinamento",
			ResponsesRedEnum.FA_FA_PAPER_PLANE, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	INVIA_FIRMA_DIGITALE(64, "Invia in firma digitale", TaskTypeEnum.SERVICE_TASK, "inviaFirmaDigitale", "Invia in Firma Digitale", ResponsesRedEnum.FA_FA_PAPER_PLANE,
			PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	SPOSTA_IN_LAVORAZIONE(65, "Sposta in lavorazione", TaskTypeEnum.SERVICE_TASK, "spostaInLavorazione", "Sposta in Lavorazione", "fa fa-rotate-left",
			PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	INOLTRA(66, ResponsesRedEnum.INOLTRA_RESPONSE, TaskTypeEnum.HUMAN_TASK, "InoltraBean", ResponsesRedEnum.INOLTRA_RESPONSE, "fa fa-mail-forward", PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	NON_VERIFICATO(67, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Non verificato", TaskTypeEnum.HUMAN_TASK, "NonVerificatoBean", "Non Verificato", ResponsesRedEnum.FA_FA_BAN,
			PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	VALIDAZIONE_CONTRIBUTO(68, ExecutionTypeEnum.MULTI, SourceTypeEnum.FILENET, "Validazione Contributo", TaskTypeEnum.HUMAN_TASK, "ValidazioneContributoBean",
			"Validazione Contributo", "fa fa-certificate", PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	ATTESTA_COPIA_CONFORME(69, "Attesta Copia Conforme", TaskTypeEnum.BEAN_TASK, "attestaCopiaConforme", "Attesta Copia Conforme", ResponsesRedEnum.FA_FA_PENCIL,
			PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	RICHIEDI_SOLLECITO(70, "Richiedi Sollecito", TaskTypeEnum.HUMAN_TASK, "RichiediSollecitoBean", "Richiedi Sollecito", ResponsesRedEnum.FA_FA_PAPER_PLANE, PostExecEnum.NESSUNA_AZIONE,
			false),

	/**
	 * Valore.
	 */
	COLLEGA_CONTRIBUTO_ESTERNO(71, "Collega Contributo", TaskTypeEnum.HUMAN_TASK, "CollegaContributoEsternoBean", "Collega Contributo", "fa fa-chain",
			PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	RICHIEDI_FIRMA_COPIA_CONFORME(72, "Richiedi firma Copia Conforme", TaskTypeEnum.SERVICE_TASK, "richiediFirmaCopiaConforme", "Richiedi firma Copia Conforme",
			ResponsesRedEnum.FA_FA_PENCIL, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	RIATTIVA_PROCEDIMENTO(73, ExecutionTypeEnum.SINGLE, SourceTypeEnum.APP, "riattiva procedimento", TaskTypeEnum.HUMAN_TASK, "RiattivaProcedimentoBean",
			"Riattiva Procedimento", "fa fa-rotate-left", PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	INVIO_IN_VISTO(74, "Invio in visto", TaskTypeEnum.HUMAN_TASK, "", "Invio in Visto", ResponsesRedEnum.FA_FA_PAPER_PLANE, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	RIASSEGNA(75, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Riassegna", TaskTypeEnum.HUMAN_TASK, "RiassegnaBean", "Riassegna", ResponsesRedEnum.FA_FA_USERS,
			PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	RICHIEDI_CONTRIBUTO_ESTERNO(76, ExecutionTypeEnum.SINGLE, SourceTypeEnum.APP, "Richiedi Contributo Esterno", TaskTypeEnum.HUMAN_TASK, "RichiediContributoEsternoBean",
			"Richiedi Contributo Esterno", ResponsesRedEnum.FA_FA_BULLHORN, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	INOLTRA_DA_CODA_ATTIVA(77, ExecutionTypeEnum.SINGLE, SourceTypeEnum.APP, "Inoltra Mail", TaskTypeEnum.HUMAN_TASK, "InoltraMailDaCodaAttivaBean", "Inoltra Mail",
			"fa fa-mail-forward", PostExecEnum.NESSUNA_AZIONE, false),

	/**
	 * Valore.
	 */
	APRI_RDS_SIEBEL(78, ExecutionTypeEnum.SINGLE, SourceTypeEnum.APP, "Apri RDS Siebel", TaskTypeEnum.HUMAN_TASK, "ApriRdsSiebelBean", "Apri RDS Siebel", "fa fa-ticket",
			PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	RIFIUTATA(79, "Rifiutata", TaskTypeEnum.HUMAN_TASK, ResponsesRedEnum.RIFIUTA_BEAN, "Rifiutata", ResponsesRedEnum.FA_FA_BAN, PostExecEnum.DISABILITA_ITEM, false), // Rifiuto della firma per copia
																																		// conforme

	/**
	 * Valore.
	 */
	INVIO_IN_FIRMA_MULTIPLA(80, "Invio in FirmaMultipla", TaskTypeEnum.HUMAN_TASK, "InviaInFirmaMultiplaBean", "Invio in Firma Multipla", ResponsesRedEnum.FA_FA_PAPER_PLANE,
			PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	FIRMA_REMOTA_MULTIPLA(81, ExecutionTypeEnum.MULTI, SourceTypeEnum.APP, "firma remota multipla", TaskTypeEnum.BEAN_TASK, "firmaRemotaMultipla", "Firma Remota Multipla",
			ResponsesRedEnum.FA_FA_PENCIL, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	FIRMA_DIGITALE_MULTIPLA(82, "Firma Digitale Multipla", TaskTypeEnum.BEAN_TASK, "firmaDigitaleMultipla", "Firma Digitale Multipla", ResponsesRedEnum.FA_FA_PENCIL,
			PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	RECALL(83, "RECALL", TaskTypeEnum.HUMAN_TASK, "RecallBean", "Richiama", ResponsesRedEnum.FA_FA_SHARE_SQUARE_O, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	SPOSTA_NEL_LIBRO_FIRMA(84, "Sposta nel libro Firma", TaskTypeEnum.SERVICE_TASK, "spostaNelLibroFirma", "Sposta nel Libro Firma", ResponsesRedEnum.FA_FA_SHARE_SQUARE_O,
			PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	ASSEGNA_NUOVA(85, ResponsesRedEnum.ASSEGNA_RESPONSE, TaskTypeEnum.HUMAN_TASK, "AssegnaNuovaBean", ResponsesRedEnum.ASSEGNA_RESPONSE, ResponsesRedEnum.FA_FA_USERS, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	RITIRO_AUTOTUTELA(86, "Ritiro Autotutela", TaskTypeEnum.HUMAN_TASK, "SelezionaDocumentoBean", "Ritiro Autotutela", ResponsesRedEnum.FA_FA_SHARE_SQUARE_O, PostExecEnum.DISABILITA_ITEM,
			false),

	/**
	 * Valore.
	 */
	RICHIESTA_INTEGRAZIONI(87, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Richiesta Integrazioni", TaskTypeEnum.HUMAN_TASK,
			ResponsesRedEnum.PREDISPONI_DA_REGISTRO_AUSILIARIO_RESPONSE_BEAN, "Richiesta Integrazioni", ResponsesRedEnum.FA_FA_SHARE_SQUARE_O, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	PREDISPONI_VISTO(88, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Predisponi Visto", TaskTypeEnum.HUMAN_TASK, ResponsesRedEnum.PREDISPONI_DA_REGISTRO_AUSILIARIO_RESPONSE_BEAN,
			"Predisponi Visto", ResponsesRedEnum.FA_FA_CHECK_SQUARE, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	PREDISPONI_OSSERVAZIONE(89, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Predisponi Osservazione", TaskTypeEnum.HUMAN_TASK,
			ResponsesRedEnum.PREDISPONI_DA_REGISTRO_AUSILIARIO_RESPONSE_BEAN, "Predisponi Osservazione", ResponsesRedEnum.FA_FA_CHECK_SQUARE, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	PREDISPONI_RESTITUZIONE(90, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Predisponi Restituzione", TaskTypeEnum.HUMAN_TASK,
			ResponsesRedEnum.PREDISPONI_DA_REGISTRO_AUSILIARIO_RESPONSE_BEAN, "Predisponi Restituzione", ResponsesRedEnum.FA_FA_SHARE_SQUARE_O, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	RISPONDI_INGRESSO(91, ExecutionTypeEnum.MULTI, SourceTypeEnum.FILENET, "Rispondi Ingresso", TaskTypeEnum.BEAN_TASK, "rispondiIngresso", "Rispondi", ResponsesRedEnum.FA_FA_KEYBOARD_O,
			PostExecEnum.RICARICA_CODA, false),

	/**
	 * Valore.
	 */
	INOLTRA_INGRESSO(92, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Inoltra Ingresso", TaskTypeEnum.BEAN_TASK, "inoltraIngresso", ResponsesRedEnum.INOLTRA_RESPONSE, ResponsesRedEnum.FA_FA_SHARE_SQUARE_O,
			PostExecEnum.RICARICA_CODA, false),

	/**
	 * Valore.
	 */
	SPEDISCI_USCITA(93, "Spedisci Uscita", TaskTypeEnum.SERVICE_TASK, "spedisciUscita", "Spedisci Uscita", ResponsesRedEnum.FA_FA_SHARE_SQUARE_O, PostExecEnum.DISABILITA_ITEM, false),

	/**
	 * Valore.
	 */
	ANNULLA_VISTO(94, ExecutionTypeEnum.MULTI, SourceTypeEnum.APP, null, TaskTypeEnum.HUMAN_TASK, ResponsesRedEnum.RIFIUTA_BEAN, "Annulla Visto", ResponsesRedEnum.FA_FA_BAN, PostExecEnum.AGGIORNA_ITEM,
			false), // Rifiuto del visto dal libro firma

	/**
	 * Valore.
	 */
	ANNULLA_OSSERVAZIONE(95, ExecutionTypeEnum.MULTI, SourceTypeEnum.APP, null, TaskTypeEnum.HUMAN_TASK, ResponsesRedEnum.RIFIUTA_BEAN, "Annulla Osservazione", ResponsesRedEnum.FA_FA_BAN,
			PostExecEnum.AGGIORNA_ITEM, false), // Rifiuto dell'osservazione dal libro firma

	/**
	 * Valore.
	 */
	ANNULLA_RICHIESTA_INTEGRAZIONI(96, ExecutionTypeEnum.MULTI, SourceTypeEnum.APP, null, TaskTypeEnum.HUMAN_TASK, ResponsesRedEnum.RIFIUTA_BEAN, "Annulla Richiesta Integrazioni",
			ResponsesRedEnum.FA_FA_BAN, PostExecEnum.AGGIORNA_ITEM, false), // Rifiuto della richiesta integrazioni dal libro firma

	/**
	 * Valore.
	 */
	CONFERMA_ANNULLAMENTO(97, ExecutionTypeEnum.MULTI, SourceTypeEnum.APP, null, TaskTypeEnum.SERVICE_TASK, "confermaAnnullamento", "Conferma Annullamento", ResponsesRedEnum.FA_FA_BAN,
			PostExecEnum.AGGIORNA_ITEM, false), // Conferma annullamento visto/osservazione/richiesta chiarimenti

	/**
	 * Valore.
	 */
	VALIDA_INTEGRAZIONE_DATI(98, ExecutionTypeEnum.MULTI, SourceTypeEnum.APP, null, TaskTypeEnum.SERVICE_TASK, "validaIntegrazioneDati", "Valida Integrazione Dati",
			ResponsesRedEnum.FA_FA_BAN, PostExecEnum.AGGIORNA_ITEM, false), // Valida Integrazione dati

	/**
	 * Valore.
	 */
	ASSOCIA_INTEGRAZIONE_DATI(99, ExecutionTypeEnum.SINGLE, SourceTypeEnum.APP, null, TaskTypeEnum.HUMAN_TASK, "SelezionaDocumentoBean", "Associa Integrazione Dati",
			ResponsesRedEnum.FA_FA_BAN, PostExecEnum.AGGIORNA_ITEM, false), // Associa Integrazione Dati
	
	/**
	 * Valore.
	 */
	PREDISPONI_RELAZIONE_POSITIVA(100, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Predisponi Relazione Positiva", TaskTypeEnum.HUMAN_TASK,
			ResponsesRedEnum.PREDISPONI_DA_REGISTRO_AUSILIARIO_RESPONSE_BEAN,"Predisponi Relazione Positiva", ResponsesRedEnum.FA_FA_CHECK_SQUARE, PostExecEnum.DISABILITA_ITEM, false),
	
	/**
	 * Valore.
	 */
	PREDISPONI_RELAZIONE_NEGATIVA(101, ExecutionTypeEnum.SINGLE, SourceTypeEnum.FILENET, "Predisponi Relazione Negativa", TaskTypeEnum.HUMAN_TASK,
			ResponsesRedEnum.PREDISPONI_DA_REGISTRO_AUSILIARIO_RESPONSE_BEAN, "Predisponi Relazione Negativa", ResponsesRedEnum.FA_FA_CHECK_SQUARE, PostExecEnum.DISABILITA_ITEM, false),
	
	/**
	 * Valore.
	 */
	ANNULLA_RELAZIONE_POSITIVA(102, ExecutionTypeEnum.MULTI, SourceTypeEnum.APP, null, TaskTypeEnum.HUMAN_TASK, ResponsesRedEnum.RIFIUTA_BEAN, "Annulla Relazione Positiva", ResponsesRedEnum.FA_FA_BAN,
			PostExecEnum.AGGIORNA_ITEM, false),
	
	/**
	 * Valore.
	 */
	ANNULLA_RELAZIONE_NEGATIVA(103, ExecutionTypeEnum.MULTI, SourceTypeEnum.APP, null, TaskTypeEnum.HUMAN_TASK, ResponsesRedEnum.RIFIUTA_BEAN, "Annulla Relazione Negativa", ResponsesRedEnum.FA_FA_BAN,
			PostExecEnum.AGGIORNA_ITEM, false),
	
	/**
	 * Valore.
	 */
	RETRY (104, ExecutionTypeEnum.MULTI, SourceTypeEnum.APP, null, TaskTypeEnum.HUMAN_TASK, "RetryBean", "Riprova firma", 
			"fa fa-repeat", PostExecEnum.AGGIORNA_ITEM, false),
	;
	
	/**
	 * Label response Assegna.
	 */
	private static final String ASSEGNA_RESPONSE = "Assegna";
	
	/**
	 * Label response Inoltra.
	 */
	private static final String INOLTRA_RESPONSE = "Inoltra";
	
	/**
	 * Label InserisciContributoBean.
	 */
	private static final String INSERISCI_CONTRIBUTO_BEAN = "InserisciContributoBean";
	
	/**
	 * Label PredisponiDaRegistroAusiliarioResponseBean.
	 */
	private static final String PREDISPONI_DA_REGISTRO_AUSILIARIO_RESPONSE_BEAN = "PredisponiDaRegistroAusiliarioResponseBean";
	
	/**
	 * Label RichiediContributoBean.
	 */
	private static final String RICHIEDI_CONTRIBUTO_BEAN = "RichiediContributoBean";
	
	/**
	 * Label RifiutaBean.
	 */
	private static final String RIFIUTA_BEAN = "RifiutaBean";
	
	/**
	 * Icona.
	 */
	private static final String FA_FA_BAN = "fa fa-ban";
	
	/**
	 * Icona.
	 */
	private static final String FA_FA_BULLHORN = "fa fa-bullhorn";
	
	/**
	 * Icona.
	 */
	private static final String FA_FA_CHECK = "fa fa-check";
	
	/**
	 * Icona.
	 */
	private static final String FA_FA_CHECK_SQUARE = "fa fa-check-square";
	
	/**
	 * Icona.
	 */
	private static final String FA_FA_DROPBOX = "fa fa-dropbox";
	
	/**
	 * Icona.
	 */
	private static final String FA_FA_KEYBOARD_O = "fa fa-keyboard-o";
	
	/**
	 * Icona.
	 */
	private static final String FA_FA_PAPER_PLANE = "fa fa-paper-plane";
	
	/**
	 * Icona.
	 */
	private static final String FA_FA_PENCIL = "fa fa-pencil";
	
	/**
	 * Icona.
	 */
	private static final String FA_FA_SHARE_SQUARE_O = "fa fa-share-square-o";
	
	/**
	 * Icona.
	 */
	private static final String FA_FA_USERS = "fa fa-users";
	
	/**
	 * Id
	 */
	private Integer id;

	/**
	 * Response name
	 */
	private String response;

	/**
	 * target della response che può indicare nel caso di un Human Task il pannello
	 * da renderizzare e nel caso dei ServiceTask e dei BeanTask i metodi da
	 * invocare tramite reflection
	 */
	private String target;

	/**
	 * Nome visualizzato front end
	 */
	private String displayName;

	/**
	 * tipo task
	 */
	private TaskTypeEnum taskType;

	/**
	 * Tipo response
	 */
	private SourceTypeEnum responseType;

	/**
	 * tipo esecuzione
	 */
	private ExecutionTypeEnum et;

	/**
	 * icona della response
	 */
	private String icon;

	/**
	 * Azione a termine esecuzione.
	 */
	private PostExecEnum actionPostExec;

	/**
	 * Indica se aggiornare o meno i contatori del menu laterale, al termine delle
	 * response
	 */
	private boolean updateCounters;

	/**
	 * Costruttore
	 * 
	 * @param id
	 * @param responseFilenet
	 * @param fullyQualifiedName
	 * @param displayName
	 */
	ResponsesRedEnum(final Integer inId, final ExecutionTypeEnum inEt, final SourceTypeEnum inResponseType, final String inResponse, final TaskTypeEnum inTaskType,
			final String inTarget, final String inDisplayName, final String inIcon, final PostExecEnum inActionPostExec, final boolean update) {
		id = inId;
		responseType = inResponseType;
		response = inResponse;
		target = inTarget;
		displayName = inDisplayName;
		taskType = inTaskType;
		et = inEt;
		icon = inIcon;
		actionPostExec = inActionPostExec;
		updateCounters = update;
	}

	/**
	 * Costruttore.
	 * 
	 * @param inId
	 * @param inResponse
	 * @param inTaskType
	 * @param inTarget
	 * @param inDisplayName
	 * @param inIcon
	 * @param inActionPostExec
	 * @param update
	 */
	ResponsesRedEnum(final Integer inId, final String inResponse, final TaskTypeEnum inTaskType, final String inTarget, final String inDisplayName, final String inIcon,
			final PostExecEnum inActionPostExec, final boolean update) {
		id = inId;
		responseType = SourceTypeEnum.FILENET;
		response = inResponse;
		target = inTarget;
		displayName = inDisplayName;
		taskType = inTaskType;
		et = ExecutionTypeEnum.MULTI;
		icon = inIcon;
		actionPostExec = inActionPostExec;
		updateCounters = update;
	}

	/**
	 * Restituisce l'action post esecuzione.
	 * 
	 * @return action post esecuzione
	 */
	public PostExecEnum getActionPostExec() {
		return actionPostExec;
	}

	/**
	 * Restituisce l'id.
	 * 
	 * @return id
	 */
	public final Integer getId() {
		return id;
	}

	/**
	 * Restituisce il tipo task.
	 * 
	 * @return tipo task
	 */
	public TaskTypeEnum getTaskType() {
		return taskType;
	}

	/**
	 * Restituisce la response.
	 * 
	 * @return response
	 */
	public final String getResponse() {
		return response;
	}

	/**
	 * Restituisce il target.
	 * 
	 * @return target
	 */
	public final String getTarget() {
		return target;
	}

	/**
	 * Restituisce il display name.
	 * 
	 * @return displayName
	 */
	public final String getDisplayName() {
		return displayName;
	}

	/**
	 * Restituisce l'execution type.
	 * 
	 * @return execution type
	 */
	public ExecutionTypeEnum getEt() {
		return et;
	}

	/**
	 * Restituisce il tipo response.
	 * 
	 * @return tipo response
	 */
	public SourceTypeEnum getResponseType() {
		return responseType;
	}

	/**
	 * Restituisce l'icona.
	 * 
	 * @return icon
	 */
	public final String getIcon() {
		return icon;
	}

	/**
	 * Metodo per il recupero di un'enum dato il valore caratteristico.
	 * 
	 * @param value valore
	 * @return enum
	 */
	public static ResponsesRedEnum get(final String response) {
		ResponsesRedEnum output = null;
		for (final ResponsesRedEnum r : ResponsesRedEnum.values()) {
			if (!StringUtils.isNullOrEmpty(r.getResponse()) && r.getResponse().equals(response)) {
				output = r;
				break;
			}
		}
		return output;
	}

	/**
	 * Restituisce la structure DTO della response.
	 * 
	 * @param response
	 * @param onlyMulti
	 * @return response
	 */
	public static ResponsesDTO getStructuredDTO(final Collection<String> response, final Boolean onlyMulti) {
		final ResponsesDTO output = new ResponsesDTO();

		if (response != null && !response.isEmpty()) {
			for (final String s : response) {
				final ResponsesRedEnum rre = get(s);

				if (rre != null) {
					if ((Boolean.TRUE.equals(onlyMulti) && ExecutionTypeEnum.MULTI.equals(rre.getEt())) || Boolean.FALSE.equals(onlyMulti)) {
						output.getResponsesEnum().add(rre);
					}
				} else {
					output.getResponseIssues().add(s);
				}
			}
		}

		return output;
	}

	/**
	 * Restituisce l'enum associata all'id.
	 * 
	 * @param id
	 * @return enum associata all'id
	 */
	public static ResponsesRedEnum getById(final Integer id) {
		ResponsesRedEnum output = null;
		for (final ResponsesRedEnum r : ResponsesRedEnum.values()) {
			if (r.getId().equals(id)) {
				output = r;
				break;
			}
		}
		return output;
	}

	/**
	 * Restituisce il flag associato all'update dei contatori.
	 * 
	 * @return flag associato all'update dei contatori
	 */
	public boolean isUpdateCounters() {
		return updateCounters;
	}

	/**
	 * Imposta il flag associato all'update dei contatori.
	 * 
	 * @param updateCounters
	 */
	protected void setUpdateCounters(final boolean updateCounters) {
		this.updateCounters = updateCounters;
	}

}
