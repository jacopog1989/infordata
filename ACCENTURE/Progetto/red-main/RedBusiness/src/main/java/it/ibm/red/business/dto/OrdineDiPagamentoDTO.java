package it.ibm.red.business.dto;

import java.util.Date;

import com.filenet.api.core.CustomObject;

/**
 * DTO che definisce un ordine di pagamento.
 */
public class OrdineDiPagamentoDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 6005291015355785612L;

	/**
	 * Numero op.
	 */
	private Integer numeroOP;

	/**
	 * Id fascicolo FEPA.
	 */
	private String idFascicoloFEPA;

	/**
	 * Amministrazione.
	 */
	private String amministrazione;

	/**
	 * Codice ragioneria.
	 */
	private String codiceRagioneria;

	/**
	 * Capitolo numeri SIRGS.
	 */
	private String capitoloNumeriSIRGS;

	/**
	 * Tipologia op.
	 */
	private Integer tipoOp;

	/**
	 * Piano gestione.
	 */
	private Integer pianoGestione;

	/**
	 * Beneficiario.
	 */
	private String beneficiario;

	/**
	 * Esercizio.
	 */
	private Integer esercizio;

	/**
	 * Data emissione OP.
	 */
	private Date dataEmissioneOP;

	/**
	 * Stato op.
	 */
	private String statoOP;

	/**
	 * Custom obj.
	 */
	private CustomObject customObject;

	/**
	 * @return the numeroOP
	 */
	public Integer getNumeroOP() {
		return numeroOP;
	}
	/**
	 * @param numeroOP the numeroOP to set
	 */
	public void setNumeroOP(final Integer numeroOP) {
		this.numeroOP = numeroOP;
	}
	/**
	 * @return the idFascicoloFEPA
	 */
	public String getIdFascicoloFEPA() {
		return idFascicoloFEPA;
	}
	/**
	 * @param idFascicoloFEPA the idFascicoloFEPA to set
	 */
	public void setIdFascicoloFEPA(final String idFascicoloFEPA) {
		this.idFascicoloFEPA = idFascicoloFEPA;
	}
	/**
	 * @return the amministraz
	 */
	public String getAmministrazione() {
		return amministrazione;
	}
	/**
	 * @param amministraz the amministraz to set
	 */
	public void setAmministrazione(final String amministrazione) {
		this.amministrazione = amministrazione;
	}
	/**
	 * @return the codiceRagioneria
	 */
	public String getCodiceRagioneria() {
		return codiceRagioneria;
	}
	/**
	 * @param codiceRagioneria the codiceRagioneria to set
	 */
	public void setCodiceRagioneria(final String codiceRagioneria) {
		this.codiceRagioneria = codiceRagioneria;
	}
	/**
	 * @return the capitoloNumeriSIRGS
	 */
	public String getCapitoloNumeriSIRGS() {
		return capitoloNumeriSIRGS;
	}
	/**
	 * @param capitoloNumeriSIRGS the capitoloNumeriSIRGS to set
	 */
	public void setCapitoloNumeriSIRGS(final String capitoloNumeriSIRGS) {
		this.capitoloNumeriSIRGS = capitoloNumeriSIRGS;
	}
	/**
	 * @return the tipoOp
	 */
	public Integer getTipoOp() {
		return tipoOp;
	}
	/**
	 * @param tipoOp the tipoOp to set
	 */
	public void setTipoOp(final Integer tipoOp) {
		this.tipoOp = tipoOp;
	}
	/**
	 * @return the pianoGestione
	 */
	public Integer getPianoGestione() {
		return pianoGestione;
	}
	/**
	 * @param pianoGestione the pianoGestione to set
	 */
	public void setPianoGestione(final Integer pianoGestione) {
		this.pianoGestione = pianoGestione;
	}
	/**
	 * @return the beneficiario
	 */
	public String getBeneficiario() {
		return beneficiario;
	}
	/**
	 * @param beneficiario the beneficiario to set
	 */
	public void setBeneficiario(final String beneficiario) {
		this.beneficiario = beneficiario;
	}
	/**
	 * @return the esercizio
	 */
	public Integer getEsercizio() {
		return esercizio;
	}
	/**
	 * @param esercizio the esercizio to set
	 */
	public void setEsercizio(final Integer esercizio) {
		this.esercizio = esercizio;
	}
	/**
	 * @return the dataEmissioneOP
	 */
	public Date getDataEmissioneOP() {
		return dataEmissioneOP;
	}
	/**
	 * @param dataEmissioneOP the dataEmissioneOP to set
	 */
	public void setDataEmissioneOP(final Date dataEmissioneOP) {
		this.dataEmissioneOP = dataEmissioneOP;
	}

	/**
	 * @return the statoOP
	 */
	public String getStatoOP() {
		return statoOP;
	}
	/**
	 * @param statoOP the statoOP to set
	 */
	public void setStatoOP(final String statoOP) {
		this.statoOP = statoOP;
	}

	/**
	 * @return customObject
	 */
	public CustomObject getCustomObject() {
		return customObject;
	}

	/**
	 * @param customObject
	 */
	public void setCustomObject(final CustomObject customObject) {
		this.customObject = customObject;
	}
}
