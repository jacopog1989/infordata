package it.ibm.red.business.service.ws.facade;

import java.io.Serializable;


/**
 * The Interface ISecurityWsFacadeSRV.
 *
 * @author a.dilegge
 * 
 *         Facade servizio gestione security web service.
 */
public interface ISecurityWsFacadeSRV extends Serializable {
	
	
	
}
