package it.ibm.red.business.dto;

import java.util.Date;
import java.util.List;

/**
 * Classe NotificaInteroperabilitaEmailDTO.
 */
public class NotificaInteroperabilitaEmailDTO extends MessaggioEmailDTO {

	/** 
	 * The Constant serialVersionUID. 
	 */
	private static final long serialVersionUID = 786180277417291531L;

	/** 
	 * Numero protocollo 
	 */
	private Integer numeroProtocollo;
	
	/** 
	 * Anno protocollo 
	 */
	private Integer annoProtocollo;
	
	/** 
	 * Data protocollo 
	 */
	private Date dataProtocollo;
	
	/** 
	 * Id protocollo 
	 */
	private String idProtocollo;
	
	/** 
	 * Tipo notifica interoperabilita 
	 */
	private String tipoNotificaInterop;
	
	
	/**
	 * Costruttore notifica interoperabilita email DTO.
	 */
	public NotificaInteroperabilitaEmailDTO() {
		super();
	}
	

	/**
	 * Costruttore notifica interoperabilita email DTO.
	 *
	 * @param inDestinatari
	 *            the in destinatari
	 * @param inDestinatariCC
	 *            the in destinatari CC
	 * @param inMittente
	 *            the in mittente
	 * @param inOggetto
	 *            the in oggetto
	 * @param inTipologiaInvioId
	 *            the in tipologia invio id
	 * @param inTipologiaMessaggioId
	 *            the in tipologia messaggio id
	 * @param inMsgID
	 *            the in msg ID
	 * @param inHasAllegati
	 *            the in has allegati
	 * @param inAllegati
	 *            the in allegati
	 * @param inDataRicezione
	 *            the in data ricezione
	 * @param inFolder
	 *            the in folder
	 * @param idDocumentoMessaggioPostaNps
	 *            the id documento messaggio posta nps
	 * @param protocollo
	 *            the protocollo
	 */
	public NotificaInteroperabilitaEmailDTO(final String inDestinatari, final String inDestinatariCC, final String inMittente, final String inOggetto, 
			final int inTipologiaInvioId, final int inTipologiaMessaggioId, final String inMsgID,	final Boolean inHasAllegati, final List<AllegatoDTO> inAllegati, 
			final Date inDataRicezione, final String inFolder, final String idDocumentoMessaggioPostaNps, final ProtocolloNpsDTO protocollo) {
		super(inDestinatari, inDestinatariCC, inMittente, inOggetto, inTipologiaInvioId, inTipologiaMessaggioId,
				inMsgID, inHasAllegati, inAllegati, inDataRicezione, inFolder, idDocumentoMessaggioPostaNps, null, null, null, null, null);
		this.numeroProtocollo = protocollo.getNumeroProtocollo();
		this.annoProtocollo = Integer.valueOf(protocollo.getAnnoProtocollo());
		this.dataProtocollo = protocollo.getDataProtocollo();
		this.idProtocollo = protocollo.getIdProtocollo();
	}


	/** 
	 *
	 * @return the numeroProtocollo
	 */
	public Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}


	/** 
	 *
	 * @return the annoProtocollo
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}


	/** 
	 *
	 * @return the dataProtocollo
	 */
	public Date getDataProtocollo() {
		return dataProtocollo;
	}


	/** 
	 *
	 * @return the idProtocollo
	 */
	public String getIdProtocollo() {
		return idProtocollo;
	}
	

	/** 
	 *
	 * @return the tipoNotificaInterop
	 */
	public String getTipoNotificaInterop() {
		return tipoNotificaInterop;
	}


	/** 
	 *
	 * @param tipoNotificaInterop the tipoNotificaInterop to set
	 */
	public void setTipoNotificaInterop(final String tipoNotificaInterop) {
		this.tipoNotificaInterop = tipoNotificaInterop;
	}


	/**
	 * Hash code.
	 *
	 * @return the int
	 */
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((annoProtocollo == null) ? 0 : annoProtocollo.hashCode());
		result = prime * result + ((dataProtocollo == null) ? 0 : dataProtocollo.hashCode());
		result = prime * result + ((idProtocollo == null) ? 0 : idProtocollo.hashCode());
		result = prime * result + ((numeroProtocollo == null) ? 0 : numeroProtocollo.hashCode());
		return result;
	}


	/**
	 * Equals.
	 *
	 * @param obj the obj
	 * @return true, if successful
	 */
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof NotificaInteroperabilitaEmailDTO)) {
			return false;
		}
		final NotificaInteroperabilitaEmailDTO other = (NotificaInteroperabilitaEmailDTO) obj;
		if (annoProtocollo == null) {
			if (other.annoProtocollo != null) {
				return false;
			}
		} else if (!annoProtocollo.equals(other.annoProtocollo)) {
			return false;
		}
		if (dataProtocollo == null) {
			if (other.dataProtocollo != null) {
				return false;
			}
		} else if (!dataProtocollo.equals(other.dataProtocollo)) {
			return false;
		}
		if (idProtocollo == null) {
			if (other.idProtocollo != null) {
				return false;
			}
		} else if (!idProtocollo.equals(other.idProtocollo)) {
			return false;
		}
		if (numeroProtocollo == null) {
			if (other.numeroProtocollo != null) {
				return false;
			}
		} else if (!numeroProtocollo.equals(other.numeroProtocollo)) {
			return false;
		}
		return true;
	}
	
}
