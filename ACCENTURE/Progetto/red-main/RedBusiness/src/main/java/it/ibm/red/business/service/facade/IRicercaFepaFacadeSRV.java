package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * The Interface IRicercaFepaFacadeSRV.
 *
 * @author m.crescentini
 * 
 *         Facade del servizio per la gestione della ricerca FEPA.
 */
public interface IRicercaFepaFacadeSRV extends Serializable {
	
	/**
	 * Recupera la descrizione della tipologia del documento.
	 * @param i
	 * @return tipologia del documento
	 */
	String recuperaTipologiaDocValues(String i);

	/**
	 * Esegue la ricerca FEPA.
	 * @param paramsRicercaFepa
	 * @param utente
	 * @param orderbyInQuery
	 * @return documenti master
	 */
	Collection<MasterDocumentRedDTO> eseguiRicercaFepa(ParamsRicercaAvanzataDocDTO paramsRicercaFepa, UtenteDTO utente,
			boolean orderbyInQuery);
}