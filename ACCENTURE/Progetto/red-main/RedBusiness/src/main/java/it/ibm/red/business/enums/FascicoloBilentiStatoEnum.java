package it.ibm.red.business.enums;

/**
 * Enum stati fascicoli bilenti.
 */
public enum FascicoloBilentiStatoEnum {

	/**
	 * Valore.
	 */
	DA_ASSEGNARE("1"),
	
	/**
	 * Valore.
	 */
	IN_LAVORAZIONE("2"),
	
	/**
	 * Valore.
	 */
	AGLI_ATTI("3");
	
	/**
	 * Stato.
	 */
	private final String stato;
	
	/**
	 * Costruttore dell'Enum.
	 * @param stato
	 */
	FascicoloBilentiStatoEnum(final String stato) {
		this.stato = stato;
	}

	/**
	 * @return the stato
	 */
	public String getStato() {
		return stato;
	}

}
