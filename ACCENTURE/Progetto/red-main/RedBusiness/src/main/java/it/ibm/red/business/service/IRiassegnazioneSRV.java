/**
 * 
 */
package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.Collection;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.service.facade.IRiassegnazioneFacadeSRV;

/**
 * @author APerquoti
 *
 */
public interface IRiassegnazioneSRV extends IRiassegnazioneFacadeSRV {

	/**
	 * @param utente
	 * @param wobNumbers
	 * @param response
	 * @param idNodoDestinatarioNew
	 * @param idUtenteDestinatarioNew
	 * @param motivoAssegnazione
	 * @param fceh
	 * @param fpeh
	 * @param con
	 * @return
	 */
	Collection<EsitoOperazioneDTO> riassegna(UtenteDTO utente, Collection<String> wobNumbers, ResponsesRedEnum response,
			Long idNodoDestinatarioNew, Long idUtenteDestinatarioNew, String motivoAssegnazione, IFilenetCEHelper fceh,
			FilenetPEHelper fpeh, Connection con);

	/**
	 * @param documentTitle
	 * @param workflowPrincipale
	 * @param motivoAssegnazione
	 * @param fceh
	 * @param fpeh
	 * @param con
	 * @return
	 */
	Collection<EsitoOperazioneDTO> assegnaADirigenteUfficio(String documentTitle, VWWorkObject workflowPrincipale, String motivoAssegnazione, 
			IFilenetCEHelper fceh, FilenetPEHelper fpeh, Connection con);
	
}