package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * 
 * 
 * @author m.crescentini
 *
 */
public interface IPredisponiFirmaAutografaFacadeSRV extends Serializable {
	
	/**
	 * Metodo per la predisposizione per la firma autografa.
	 * @param wobNumber
	 * @param utente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO eseguiPredisposizionePerFirmaAutografa(String wobNumber, UtenteDTO utente);
}