package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;

import com.filenet.api.collection.FolderSet;
import com.filenet.api.collection.Integer32List;
import com.filenet.api.collection.StringList;
import com.filenet.api.constants.CompoundDocumentState;
import com.filenet.api.core.Document;
import com.filenet.api.core.Folder;
import com.filenet.apiimpl.property.PropertyImpl;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.DocumentoWsDTO;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.StringUtils;

/**
 * Trasformer documento WS.
 * 
 * @author m.crescentini
 */
public class ContextFromDocumentoToDocumentoEmailWsTrasformer extends ContextTrasformerCE<DocumentoWsDTO> {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -5550530174350382835L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContextFromDocumentoToDocumentoEmailWsTrasformer.class.getName());
	
	/**
	 * Costruttore.
	 */
	public ContextFromDocumentoToDocumentoEmailWsTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENT_TO_DOC_EMAIL_WS);
	}

	/**
	 * Trasform.
	 *
	 * @param document
	 *            the document
	 * @param connection
	 *            the connection
	 * @return the DocumentoWs DTO
	 */
	@SuppressWarnings("unchecked")
	@Override
	public final DocumentoWsDTO trasform(final Document document, final Connection connection, final Map<ContextTrasformerCEEnum, Object> context) {
		DocumentoWsDTO docWs = null;
		
		try {
			final String guid = StringUtils.cleanGuidToString(document.get_Id());
			final String classeDocumentale = document.getClassName();
			final String documentTitle = (String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			final String oggetto = (String) getMetadato(document, PropertiesNameEnum.OGGETTO_EMAIL_METAKEY);
			final Date dataModifica = document.get_DateCheckedIn(); // cfr. FWS
			final Integer numeroVersione = document.get_MajorVersionNumber();
			
			List<DocumentoWsDTO> allegati = null;

			// Folder
			String folder = Constants.EMPTY_STRING;
			final FolderSet folders = document.get_FoldersFiledIn();
			
			if (folders != null && !folders.isEmpty()) {
				final Iterator<?> itFolders = folders.iterator();
				
				if (itFolders.hasNext()) {
					folder = ((Folder) itFolders.next()).get_FolderName();
				}
			}
			
			// Metadati (se richiesti)
			final boolean withMetadati = Boolean.TRUE.equals(context.get(ContextTrasformerCEEnum.RECUPERA_METADATI));
			
			Map<String, Object> metadati = null;
			if (withMetadati) {
				metadati = new HashMap<>();
				final Set<String> classDefinitionPropertyList = (Set<String>) context.get(ContextTrasformerCEEnum.CLASS_DEFINITION_PROPERTIES);
				
				final Iterator<?> itMetadati = document.getProperties().iterator();
				while (itMetadati.hasNext()) {
					final PropertyImpl property = (PropertyImpl) itMetadati.next();
					
					if (classDefinitionPropertyList.contains(property.getKey())) {
						final Object valoreMetadato = property.getObjectValue();
						
						if (valoreMetadato != null) {
							if (valoreMetadato instanceof StringList || valoreMetadato instanceof Integer32List) {
								final List<String> valore = new ArrayList<>();
								
								for (final Object valoreSingolo : ((StringList) valoreMetadato)) {
									valore.add(String.valueOf(valoreSingolo));
								}
								
								if (!CollectionUtils.isEmpty(valore)) {
									metadati.put((String) property.getKey(), valore);
								}
							} else {
								final String valore = String.valueOf(valoreMetadato);
								
								if (!StringUtils.isNullOrEmpty(valore)) {
									metadati.put((String) property.getKey(), valore);
								}
							}
						}
					}
				}
			}
			
			// Allegati
			if (CompoundDocumentState.COMPOUND_DOCUMENT_AS_INT == document.get_CompoundDocumentState().getValue()) {
				allegati = new ArrayList<>();
				DocumentoWsDTO allegato = null;
				byte[] content = null;
				String contentType = null;
				String guidAll = null;
				String documentTitleAll = null;
				String classeDocumentaleAll = null;
				Integer numeroVersioneAll = null;
				
				final Iterator<?> itAllegati = document.get_ChildDocuments().iterator();
				while (itAllegati.hasNext()) {
					final Document allegatoFilenet = (Document) itAllegati.next();
					
					guidAll = StringUtils.cleanGuidToString(document.get_Id());
					documentTitleAll = (String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
					classeDocumentaleAll = document.getClassName();
					numeroVersioneAll = document.get_MajorVersionNumber();

					// Content allegati e-mail
					if (FilenetCEHelper.hasDocumentContentTransfer(allegatoFilenet)) {
						content = FilenetCEHelper.getDocumentContentAsByte(allegatoFilenet);
						contentType = document.get_MimeType();
					} else {
						LOGGER.warn("Non è stato trovato il content dell'allegato con GUID: " + guidAll + " della mail: " + documentTitle);
					}
					
					allegato = new DocumentoWsDTO(documentTitleAll, guidAll, classeDocumentaleAll, numeroVersioneAll, content, contentType);
					
					allegati.add(allegato);
				}
			}
			
			docWs = new DocumentoWsDTO(guid, documentTitle, oggetto, dataModifica, allegati, numeroVersione, classeDocumentale, folder, metadati);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero di un metadato durante la trasformazione da documento FileNet a DocumentoWsDTO", e);
		}
		
		return docWs;
	}
	
	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextTrasformerCE#trasform(com.filenet.api.core.Document,
	 *      java.util.Map).
	 */
	@Override
	public DocumentoWsDTO trasform(final Document document, final Map<ContextTrasformerCEEnum, Object> context) {
		throw new RedException();
	}
	
}
