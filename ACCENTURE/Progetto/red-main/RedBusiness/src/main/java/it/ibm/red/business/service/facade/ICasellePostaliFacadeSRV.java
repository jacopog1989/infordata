package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.FunzionalitaEnum;
import it.ibm.red.business.enums.MailOperazioniEnum;

/**
 * Facade del servizio di gestione caselle postali.
 */
public interface ICasellePostaliFacadeSRV extends Serializable {
	
	/**
	 * Ottiene le caselle postali.
	 * @param utente
	 * @param f - funzionalità
	 * @return lista di caselle postali
	 */
	List<CasellaPostaDTO> getCasellePostali(UtenteDTO utente, FunzionalitaEnum f);

	/**
	 * Ottiene la casella postale.
	 * @param utente
	 * @param nomeCasella - nome della casella postale
	 * @return casella postale
	 */
	CasellaPostaDTO getCasellaPostale(UtenteDTO utente, String nomeCasella);
	
	/**
	 * Ottiene le response di recupero delle caselle postali.
	 * @param idNodo - id del nodo
	 * @param casellaPostale
	 * @return lista di enum
	 */
	List<MailOperazioniEnum> getResponses(Long idNodo, String casellaPostale);

	/**
	 * Ottiene la dimensiona massima permessa della casella postale.
	 * @param mittente
	 * @return dimensione massima
	 */
	Integer getMaxSizeContentCasellaPostale(String mittente);
	
}