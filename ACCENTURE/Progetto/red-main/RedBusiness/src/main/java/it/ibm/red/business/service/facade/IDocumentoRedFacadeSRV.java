package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DetailFatturaFepaDTO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EventoLogDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.ResponsabileCopiaConformeDTO;
import it.ibm.red.business.dto.TemplateMetadatoValueDTO;
import it.ibm.red.business.dto.TemplateValueDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.VersionDTO;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;

/**
 * Facade del servizio che gestisce i documenti RED.
 */
public interface IDocumentoRedFacadeSRV extends Serializable {

	/**
	 * Recupero il dettaglio del documento completo dei dati del CE del PE e dati
	 * accessori.
	 * 
	 * @param documentTitle
	 * @param utente
	 * @param wobNumber         può essere null
	 * @param documentClassName può essere null (se null ->
	 *                          PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY)
	 * @return
	 */
	DetailDocumentRedDTO getDocumentDetail(String documentTitle, UtenteDTO utente, String wobNumber, String documentClassName);

	/**
	 * Recupera solo i dati dei campi della classe documentale Documento_NSD sul CE.
	 * 
	 * @param fceh
	 * @param documentTitle
	 * @param utente
	 * @return
	 */
	DetailDocumentRedDTO getDetailFromCEDocumentoNSD(IFilenetCEHelper fceh, String documentTitle, UtenteDTO utente);

	/**
	 * Recupera la mail spedizione legata al documento con l'utenza admin.
	 * 
	 * @param documentTitle
	 * @param utente
	 * @return
	 */
	EmailDTO getMailByDocumentTitleAndIdAOObyAdmin(String documentTitle, UtenteDTO utente);

	/**
	 * @param guidMail
	 * @param utente
	 * @return
	 */
	List<DetailDocumentRedDTO> getAllegatiMail(String guidMail, UtenteDTO utente);

	/**
	 * Utilizzato in DocumentoRedSRV oltre a restituire se è modificabile oppure no
	 * setta anche le code del documento in input.
	 * 
	 * @param detail
	 * @param utente
	 * @return
	 */
	boolean isDocumentoModificabile(DetailDocumentRedDTO detail, UtenteDTO utente);

	/**
	 * Restituisce le versioni del documento.
	 * 
	 * @param guid
	 * @param idAOO
	 * @param utente
	 * @return
	 */
	List<VersionDTO> getVersioniDocumento(String guid, Integer idAOO, UtenteDTO utente);

	/**
	 * Restituisce le versioni di un allegato.
	 * 
	 * @param guid
	 * @param idAoo
	 * @param idCategoriaDocPrincipale
	 * @param utente
	 * @return
	 */
	List<VersionDTO> getVersioniDocumentoAllegato(String guid, Integer idAoo, Integer idCategoriaDocPrincipale, UtenteDTO utente, boolean isRicercaRiservato, String codiceFlussoPrincipale);

	/**
	 * Elimina il documento interno.
	 * 
	 * @param documentTitle
	 * @param idFascicolo
	 * @param utente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO eliminaDocumentoInterno(String documentTitle, int idFascicolo, UtenteDTO utente);

	/**
	 * Ottiene il dettaglio della fattura FEPA.
	 * 
	 * @param documentTitle
	 * @param utente
	 * @param wobNumberIN
	 * @param documentClassName
	 * @return dettaglio della fattura FEPA
	 */
	DetailFatturaFepaDTO getFatturaDetail(String documentTitle, UtenteDTO utente, String wobNumberIN, String documentClassName);

	/**
	 * Annulla i documenti.
	 * 
	 * @param docTitleDaAnnullare
	 * @param motivoAnnullamento
	 * @param utente
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> annullaDocumenti(Collection<String> docTitleDaAnnullare, String motivoAnnullamento, UtenteDTO utente);

	/**
	 * Annulla il documento.
	 * 
	 * @param docTitleDaAnnullare
	 * @param motivoAnnullamento
	 * @param utente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO annullaDocumento(String docTitleDaAnnullare, String motivoAnnullamento, UtenteDTO utente);

	/**
	 * Annulla il protocollo se esiste. Annulla il documento.
	 * 
	 * @param docTitleDaAnnullare    documentTitle
	 * @param motivoAnnullamento     stringa impostata dall'utente
	 * @param provvedimentoAnnullato stringa impostata dall'utente
	 * @param utente
	 */
	EsitoOperazioneDTO annullaDocumentoOAnnullaProtocollo(String docTitleDaAnnullare, String motivoAnnullamento, String provvedimentoAnnullato, UtenteDTO utente);

	/**
	 * Annulla il documento o il protocollo.
	 * 
	 * @param docTitleDaAnnullare
	 * @param motivoAnnullamento
	 * @param provvedimentoAnnullato
	 * @param utente
	 * @param fceh
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO annullaDocumentoOAnnullaProtocollo(String docTitleDaAnnullare, String motivoAnnullamento, String provvedimentoAnnullato, UtenteDTO utente,
			IFilenetCEHelper fceh);

	/**
	 * Aggiorna il content del documento.
	 * 
	 * @param documentTitle
	 * @param idTipologiaDocumento
	 * @param content
	 * @param mimeType
	 * @param fileName
	 * @param idIterApprovativo
	 * @param assegnazioni
	 * @param templateDocUscitaMap
	 * @param idTipoAssegnazione
	 * @param utente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO aggiornaContentDocumento(String documentTitle, Integer idTipologiaDocumento, byte[] content, String mimeType, String fileName,
			Integer idIterApprovativo, List<AssegnazioneDTO> assegnazioni, Map<TemplateValueDTO, List<TemplateMetadatoValueDTO>> templateDocUscitaMap,
			Integer idTipoAssegnazione, UtenteDTO utente);

	/**
	 * Esegue delle verifiche sul content del documento che si sta inserendo
	 * (estensione, campi firma, etc.) per capire se il file può essere o meno
	 * inserito su RED.
	 * 
	 * @param documentTitle
	 * @param idTipologiaDocumento
	 * @param content
	 * @param mimeType
	 * @param fileName
	 * @param utente
	 * @return EsitoOperazioneDTO true se il file ha superato le verifiche e può
	 *         essere inserito su RED, false altrimenti.
	 */
	EsitoOperazioneDTO verificaContent(String documentTitle, Integer idTipologiaDocumento, byte[] content, String mimeType, String fileName, UtenteDTO utente);

	/**
	 * Restituisce solamente le info del file, documentTitle, guid, fileName,
	 * content, mimeType, data creazione.
	 * 
	 * @param documentTitle
	 * @param idAOO
	 * @param fcDto
	 * @return
	 */
	DetailDocumentRedDTO getDocumentoDopoTrasformazione(String documentTitle, Long idAOO, FilenetCredentialsDTO fcDto);

	/**
	 * Aggiorna il dettaglio del documento con la lista di fascicoli, eventuali
	 * faldoni e il fascicolo procedimentale
	 * 
	 * @param detailDocument Dettaglio del documento da inizializzare
	 * @param utente         Utente che richiede l'operazione
	 */
	void retrieveFascicoliAndSetProcedimentale(DetailDocumentRedDTO detailDocument, UtenteDTO utente);

	/**
	 * Permette di aggiornare le informazioni contenute in un dettaglio mediante le
	 * informazioni contenute nel suo fascicolo procedimentale
	 * 
	 * @param procedimentale
	 * @param detailDTO
	 */
	void setProcedimentale(DetailDocumentRedDTO detailDTO, FascicoloDTO procedimentale);

	/**
	 * Esewgue alcune verifica che sia apribile il dettaglio del documento. Verifica
	 * che il documento non abbia una tipologiadocumento light. Un docuemnto light
	 * deriva da una migrazione e quindi non sono garantiti tutti i metadati
	 * necessari a mostrare il dettaglio del documento.
	 * 
	 * @param tipologiaDocumento Tipologia del documento da analizzare.
	 * @return Il risultato della verifica.
	 */
	boolean hasDocumentDetail(String tipologiaDocumento);

	/**
	 * Verifica se l'utente in input è il competente del documento
	 * 
	 * @param idDocumento
	 * @param utente
	 * @return true se l'utente in input è l'assegnatario per competenza, sigla,
	 *         visto, firma, rifiuto_firma, rifiuto_visto, rifiuto_sigla,
	 *         rifiuto_assegnazione, spedizione<br/>
	 *         false altrimenti
	 */
	boolean isCompetenteDocumento(Integer idDocumento, UtenteDTO utente);

	/**
	 * Verrifica se il documenti si trova nel Libro Firma del Responsabile per la
	 * Copia Conforme.
	 * 
	 * @param documentTitle
	 * @param allegati
	 * @param responsabileCopiaConforme
	 * @param utente
	 * @return
	 */
	boolean isInLibroFirmaResponsabileCopiaConforme(String documentTitle, List<AllegatoDTO> allegati, ResponsabileCopiaConformeDTO responsabileCopiaConforme,
			UtenteDTO utente);

	/**
	 * @param allegati
	 * @return
	 */
	boolean hasAllegatiCopiaConforme(List<AllegatoDTO> allegati);

	/**
	 * @param documentTitle
	 * @param utente
	 * @param wobNumber
	 * @param documentClassName
	 * @param isRicercaRiservato
	 * @return
	 */
	DetailDocumentRedDTO getDocumentDetail(String documentTitle, UtenteDTO utente, String wobNumber, String documentClassName, boolean isRicercaRiservato);

	/**
	 * @param guid
	 * @param idAoo
	 * @param utente
	 * @param isRicercaRiservato
	 * @return
	 */
	List<VersionDTO> getVersioniDocumento(String guid, Integer idAoo, UtenteDTO utente, boolean isRicercaRiservato);

	/**
	 * @param docTitleDocUscita
	 * @param utente
	 * @return EsitoOperazioneDTO
	 */
	EsitoOperazioneDTO riattivaDocIngressoAfterChiusuraRisposta(String docTitleDocUscita, UtenteDTO utente);

	/**
	 * Converte da EventoLogDTO a AssegnazioneDTO.
	 * 
	 * @param event
	 * @return AssegnazioneDTO
	 */
	AssegnazioneDTO eventoLogDTO2AssegnazioneDTO(EventoLogDTO event);
}
