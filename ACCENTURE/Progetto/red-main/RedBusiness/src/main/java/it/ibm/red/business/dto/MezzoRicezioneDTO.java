package it.ibm.red.business.dto;

import java.io.Serializable;

import it.ibm.red.business.enums.FormatoDocumentoEnum;

/**
 * DTO che definisce un mezzo di ricezione.
 */
public class MezzoRicezioneDTO  implements Serializable, Comparable<MezzoRicezioneDTO> {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Id del mezzo di ricezione.
	 */
	private Integer idMezzoRicezione;
	
	/**
	 * Descrizione del mezzo di ricezione.
	 */
	private String descrizione;
	
	/**
	 * Formato del documento associato al mezzo.
	 */
	private FormatoDocumentoEnum formatoDocumento;
	
	/**
	 * Costruttore vuoto.
	 */
	public MezzoRicezioneDTO() {
	}
	
	/**
	 * Costruttore.
	 */
	public MezzoRicezioneDTO(final Integer idMezzoRicezione, 
			final String descrizione,
			final FormatoDocumentoEnum formatoDocumento) {
		this.idMezzoRicezione = idMezzoRicezione;  
		this.descrizione = descrizione;
		this.formatoDocumento = formatoDocumento; 
	}
	
	/***GET & SET*************************/
	/**
	 * Restituisce l'id del mezzo di ricezione.
	 * @return id mezzo ricezione
	 */
	public Integer getIdMezzoRicezione() {
		return idMezzoRicezione;
	}

	/**
	 * Restituisce la descrizione del mezzo di ricezione.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Restituisce il formato documento.
	 * @return formato documento
	 */
	public FormatoDocumentoEnum getFormatoDocumento() {
		return formatoDocumento;
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object).
	 */
	@Override
	public int compareTo(final MezzoRicezioneDTO o) {
		return this.descrizione.compareTo(o.getDescrizione());
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descrizione == null) ? 0 : descrizione.hashCode());
		result = prime * result + ((idMezzoRicezione == null) ? 0 : idMezzoRicezione.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MezzoRicezioneDTO other = (MezzoRicezioneDTO) obj;
		if (descrizione == null) {
			if (other.descrizione != null)
				return false;
		} else if (!descrizione.equals(other.descrizione))
			return false;
		if (idMezzoRicezione == null) {
			if (other.idMezzoRicezione != null)
				return false;
		} else if (!idMezzoRicezione.equals(other.idMezzoRicezione))
			return false;
		return true;
	}
	
}
