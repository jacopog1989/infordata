package it.ibm.red.business.exception;

/**
 * The Class RedException.
 *
 * @author CPIERASC
 * 
 *         Eccezione generica dell'applicativo.
 */
public class RedException extends RuntimeException {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Costruttore.
	 * 
	 */
	public RedException() {
		super();
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param e	eccezione
	 */
	public RedException(final Exception e) {
		super(e);
	}

	/**
	 * Costruttore.
	 * 
	 * @param msg	messaggio
	 */
	public RedException(final String msg) {
		super(msg);
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param msg	messaggio
	 * @param e		eccezione root
	 */
	public RedException(final String msg, final Exception e) {
		super(msg, e);
	}
	
	/**
	 * Costruttore.
	 * @param msg 	messaggio
	 * @param t 	errore come throwable
	 */
	public RedException(final String msg, final Throwable t) {
		super(msg, t);
	}
	
}
