package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.Collection;
import java.util.Date;

import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaDTO;
import it.ibm.red.business.service.facade.IRegistrazioniAusiliarieFacadeSRV;

/**
 * Interfaccia del servizio che gestisce le registrazioni ausiliarie.
 * 
 * @author SimoneLungarella
 */
public interface IRegistrazioniAusiliarieSRV extends IRegistrazioniAusiliarieFacadeSRV {

	/**
	 * Consente di memorizzare sulla base dati le informazioni associate ad una
	 * registrazione ausiliaria eseguita sul documento identificato dal
	 * <code> documentTitle </code>.
	 * 
	 * @param idAoo
	 *            identificativo dell'area organizzativa dell'utente che ha eseguito
	 *            la registrazione ausiliaria
	 * @param documentTitle
	 *            identificativo del documento per cui è stata creata la
	 *            registrazione ausiliaria
	 * @param idRegistro
	 *            identificativo del registro ausiliario utilizzato per la
	 *            registrazione
	 * @param metadati
	 *            metadati del registro ausiliario e del template della
	 *            registrazione ausiliaria
	 * @param con
	 *            connessione alla base dati
	 */
	void saveRegistrazioneAusiliaria(String documentTitle, int idAoo, int idRegistro, Collection<MetadatoDTO> metadati, Connection con);

	/**
	 * Elimina tutte le informazioni sulle registrazioni ausiliarie eventualmente
	 * esistenti che facciano riferimento al documento identificato dal
	 * <code> documentTitle </code>.
	 * 
	 * @param documentTitle
	 *            identificativo del documento per il quale rimuoverere le
	 *            registrazioni ausiliarie
	 * @param idAoo
	 *            identificativo dell'area organizzativa dell'utente che esegue
	 *            l'eliminazione della registrazione ausiliaria
	 * @param con
	 *            connessione al database
	 */
	void deleteRegistrazioneAusiliaria(String documentTitle, int idAoo, Connection con);

	/**
	 * Restituisce la registrazione ausiliaria esistente associata al documento
	 * identificato dal <code> documentTitle </code>. Restituisce
	 * <code> null </code> se non esiste alcuna registrazione ausiliaria memorizzata
	 * per il documento.
	 * 
	 * @param documentTitle
	 *            identificativo documento associato alla registrazione recuperata
	 * @param idAoo
	 *            identificativo dell'area organizzativa a cui fa riferimento la
	 *            registrazione ausiliaria
	 * @param con
	 *            connessione al database
	 * @return registrazione ausiliaria completa di tutte le informazioni
	 */
	RegistrazioneAusiliariaDTO getRegistrazioneAusiliaria(String documentTitle, int idAoo, Connection con);

	/**
	 * Esegue l'aggiornamento delle informazioni sulla registrazione ausiliaria
	 * recuperando i metadati tipici di una registrazione ausiliaria dalla lista
	 * <code> metadati </code>. Questo metodo offre la possiblità di impostare una
	 * registrazione come definitiva, il che vuol dire che per la registrazione
	 * ausiliaria è stato già effettuato il processo di rigenerazione del content e
	 * quindi non è più modificabile in quanto la registrazione ausiliaria risulta
	 * firmata.
	 * 
	 * @param documentTitle
	 *            identificativo del documento a cui è associata la registrazione
	 *            ausiliaria
	 * @param idAoo
	 *            identificativo dell'area organizzativa a cui fa riferimento la
	 *            registrazione ausiliaria
	 * @param metadati
	 *            metadati della registrazione ausiliaria con cui effettuare
	 *            l'aggiornamento
	 * @param documentoDefinitivo
	 *            se <code> true </code> il documento è stato firmato quindi la
	 *            registrazione ausiliaria è completa e non modificabile, se
	 *            <code> false </code> la registrazione ausiliaria risulterà ancora
	 *            modificabile.
	 * @param con
	 *            connessione al database
	 */
	void updateDatiRegistrazioneAusiliaria(String documentTitle, int idAoo, Collection<MetadatoDTO> metadati, boolean documentoDefinitivo, Connection con);

	/**
	 * Metodo che consente di effettuare un update a numero registrazione,
	 * identificativo registrazione e data registrazione per la registrazione
	 * ausiliaria associata al documento identificato dal
	 * <code> documentTitle </code>. Questo metodo non modifica i rimanenti metadati
	 * quindi non può consentire l'impostazione della registrazione ausiliaria come
	 * definitiva, per impostare la registrazione ausiliaria come definitiva occorre
	 * utilizzare
	 * {@link #updateDatiRegistrazioneAusiliaria(String, int, Collection, boolean, Connection)}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param documentTitle
	 *            identificativo del documento a cui è associata la registrazione
	 *            ausiliaria
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @param numeroRegistrazione
	 *            numero progressivo della registrazione ausiliaria staccato da NPS
	 * @param idRegistrazione
	 *            identificativo della registrazione ausiliaria fornito da NPS
	 * @param dataRegistrazione
	 *            data di avvenuta registrazione ausiliaria
	 */
	void updateParzialeRegistrazioneAusiliaria(Connection connection, String documentTitle, Long idAoo, Integer numeroRegistrazione, String idRegistrazione,
			Date dataRegistrazione);

}