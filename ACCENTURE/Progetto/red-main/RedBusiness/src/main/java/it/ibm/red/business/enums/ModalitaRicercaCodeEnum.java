package it.ibm.red.business.enums;

/**
 * Enum che definisce le diverse modalita di ricerca code.
 */
public enum ModalitaRicercaCodeEnum {
	
	/**
	 * Valore.
	 */
	ENTRATA,
	
	/**
	 * Valore.
	 */
	USCITA,
	
	/**
	 * Valore.
	 */
	FEPA_FATTURE,
	
	/**
	 * Valore.
	 */
	FEPA_DD,
	
	/**
	 * Valore.
	 */
	FEPA_DSR,
	
	/**
	 * Valore.
	 */
	ENTRATA_USCITA;


}
