package it.ibm.red.business.helper.pdf;

/**
 * DTO cella tabella.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public class TableCellValue {

	/**
	 * Label.
	 */
	private String label;
	
	/**
	 * Proprietà.
	 */
	private String property;
	
	/**
	 * Formatter.
	 */
	private Formatter formatter;

	/**
	 * Costruttore.
	 * 
	 * @param inLabel		label
	 * @param inProperty	property
	 */
	public TableCellValue(final String inLabel, final String inProperty) {
		this(inLabel, inProperty, new SimpleFormatter());
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param inLabel		label
	 * @param inProperty	property
	 * @param inFormatter	formatter
	 */
	public TableCellValue(final String inLabel, final String inProperty, final Formatter inFormatter) {
		super();
		this.label = inLabel;
		this.property = inProperty;
		this.formatter = inFormatter;
	}

	/**
	 * Getter.
	 * 
	 * @return	label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Getter.
	 * 
	 * @return	property
	 */
	public String getProperty() {
		return property;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	formatter
	 */
	public Formatter getFormatter() {
		return formatter;
	}
	
}

