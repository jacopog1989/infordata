package it.ibm.red.business.helper.xml;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.exception.RedException;

/**
 * Classe abstract che consente la lavorazione di xml in maniera centralizzata.
 */
public abstract class AbstractXmlHelper implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 2648691141076368937L;

	/**
	 * Documento xml.
	 */
	private transient Document xmlDocument;

	/**
	 * Builder documento.
	 */
	private transient DocumentBuilder documentBuilder;

	/**
	 * Costruttore di default.
	 */
	protected AbstractXmlHelper() {
		super();
	}

	/**
	 * Imposta il documento xml da un InputStream.
	 * @param xmlStream
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public void setXmlDocument(final InputStream xmlStream) {
		if (xmlDocument != null) {
			xmlDocument = null;
		}
		try {
			xmlDocument = this.getDocumentBuilder().parse(xmlStream);
		} catch (SAXException | IOException | ParserConfigurationException e) {
			throw new RedException(e);
		}
	}

	/**
	 * Restituisce il documento xml.
	 * @return
	 */
	protected Document getXmlDocument() {
		return xmlDocument;
	}

	/**
	 * Restituisce il DocumentBuilder.
	 * @return DocumentBuilder
	 * @throws ParserConfigurationException
	 */
	protected DocumentBuilder getDocumentBuilder() throws ParserConfigurationException {
		if (this.documentBuilder == null) {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setAttribute(Constants.ACCESS_EXTERNAL_DTD, "");
			factory.setAttribute(Constants.ACCESS_EXTERNAL_SCHEMA, "");
			factory.setNamespaceAware(true);
			documentBuilder = factory.newDocumentBuilder();
		}
		return documentBuilder;
	}

	/**
	 * Restituisce il content dell'elemento come Stringa.
	 * @param elementName
	 * @return content element
	 */
	protected String getStringFromElements(final String elementName) {
		String result = null;
		Element eElement = getPrincipalNodeElements();
		if (eElement != null) {
			NodeList elementsList = eElement.getElementsByTagName(elementName);
			if (elementsList != null && elementsList.item(0) != null) {
				result = elementsList.item(0).getTextContent();
			}
		}	 
	 return result;
	}
	
	/**
	 * Restituisce l'elemento identificato dal nome passato come parametro.
	 * @param elementName
	 * @return elemento completo
	 */
	protected NodeList getElementsByTagName(final String elementName) {
		Element eElement = getPrincipalNodeElements();
		if (eElement != null) {
			NodeList elementsList = eElement.getElementsByTagName(elementName);
			if (elementsList != null) {
				 return elementsList;
			}
		}	 
	 return null;
	}
	
	/**
	 * Restituisce il nodo principale.
	 * @return elemento principale
	 */
	protected Element getPrincipalNodeElements() {
		Element eElement = null;
			NodeList nList = getXmlDocument().getElementsByTagName(getXmlDocument().getDocumentElement().getNodeName());
			 if (nList != null) {
				Node nNode = nList.item(0);
				eElement = (Element) nNode;
			 }
			return eElement;
	}
	
	/**
	 * Restituisce i valori in una lista recuperandoli dalla lista degli elementi definiti dal tag.
	 * @param elementName
	 * @return lista valori
	 */
	protected List<String> getStringListFromElementsList(final String elementName) {
		List<String> stringList = null;
		Node node = getPrincipalNodeElements();
		Element firstElement = (Element) node;
		NodeList nList = firstElement.getElementsByTagName(elementName);
		 if (nList != null) {
			stringList = new ArrayList<>(); 
			for (int i = 0; i < nList.getLength(); i++) {
				Element element = (Element) nList.item(i);
				stringList.add(element.getTextContent());
			}
		 }
		 return stringList;
	}
}
