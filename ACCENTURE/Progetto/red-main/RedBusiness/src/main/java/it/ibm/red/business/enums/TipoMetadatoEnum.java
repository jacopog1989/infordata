package it.ibm.red.business.enums;

/**
 * Tipologie dei metadati. @see MetadatoDTO.
 */
public enum TipoMetadatoEnum {

	/**
	 * Valore.
	 */
	TEXT_AREA("T", "TEXT AREA", false),

	/**
	 * Valore.
	 */
	STRING("S", "STRINGA"),

	/**
	 * Valore.
	 */
	INTEGER("I", "INTERO"),

	/**
	 * Valore.
	 */
	DOUBLE("U", "DOUBLE"),

	/**
	 * Valore.
	 */
	DATE("D", "DATA"),

	/**
	 * Valore.
	 */
	LOOKUP_TABLE("L", "LOOKUP TABLE"),

	/**
	 * Valore.
	 */
	CAPITOLI_SELECTOR("C", "SELETTORE CAPITOLI"),

	/**
	 * Valore.
	 */
	PERSONE_SELECTOR("P", "SELETTORE PERSONE FISICHE");


	/**
	 * Flag wizard (chiave utilizzata nel wizard).
	 */
	private Boolean wizard;

	/**
	 * Codice.
	 */
	private String code;

	/**
	 * Descrizione.
	 */
	private String description;

	TipoMetadatoEnum(final String code, final String description) {
		this.code = code;
		this.description = description;
		wizard = true;
	}
	
	TipoMetadatoEnum(final String code, final String description, final Boolean inWizard) {
		this(code, description);
		wizard = inWizard;		
	}
	
	/**
	 * Restituisce il codice.
	 * @return codice
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * Restituisce la descrizione.
	 * @return descrizione
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Restituisce il flag associato al wizard.
	 * @return flag associato al wizer
	 */
	public Boolean getWizard() {
		return wizard;
	}

	/**
	 * Restituisce il enum associata al valore.
	 * @param inCode
	 * @return enum associata
	 */
	public static TipoMetadatoEnum get(final String inCode) {
		TipoMetadatoEnum output = null;
		
		for (final TipoMetadatoEnum e: TipoMetadatoEnum.values()) {
			if (e.getCode().equalsIgnoreCase(inCode)) {
				output = e;
				break;
			}
		}
		
		return output;
	}

}
