package it.ibm.red.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ISottoscrizioniDAO;
import it.ibm.red.business.dto.CanaleTrasmissioneDTO;
import it.ibm.red.business.dto.CategoriaEventoDTO;
import it.ibm.red.business.dto.DisabilitazioneNotificheUtenteDTO;
import it.ibm.red.business.dto.EventoSottoscrizioneDTO;
import it.ibm.red.business.dto.MailAddressDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.concrete.NotificaSRV.MittenteAzione;
import it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV.InfoDocumenti;
import oracle.jdbc.OracleTypes;

/**
 * DAO per la gestione delle sottoscrizioni.
 */
@Repository
public class SottoscrizioniDAO extends AbstractDAO implements ISottoscrizioniDAO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SottoscrizioniDAO.class.getName());
	
	/**
	 * Query parziale.
	 */
	private static final String AND_SE_IDNODO = " and se.IDNODO = ? ";

	/**
	 * Query parziale.
	 */
	private static final String AND_SE_IDUTENTE = " and se.IDUTENTE = ? ";

	/**
	 * Query parziale.
	 */
	private static final String FROM_SE_SOTTOSCRIZIONE_EVENTO_DOC_SED_SE_SOTTOSCRIZIONE_EVENTO_SE = " from SE_SOTTOSCRIZIONE_EVENTO_DOC sed, SE_SOTTOSCRIZIONE_EVENTO se  ";

	/**
	 * Messaggio errore rimozione sottoscrizione. Occorre appendere l'id della sottoscrizione.
	 */
	private static final String ERROR_RIMOZIONE_SOTTOSCRIZIONE_MSG = "Errore durante la rimozione della sottoscrizione all'id=";
	
	/**
	 * Messaggio errore inserimento sottoscrizione. Occorre appendere l'id della sottoscrizione.
	 */
	private static final String ERROR_INSERIMENTO_SOTTOSCRIZIONE_MSG = "Errore durante l'inserimento della sottoscrizione all'id=";
	
	/**
	 * Tipo evento rifiuto firma.
	 */
	private static final Integer[] EVENTI_RIFIUTO_FIRMA = {1012, 1106, 3005};

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#insertSottoscrizione(java.sql.Connection,
	 *      int, java.lang.Long, java.lang.Long, java.lang.Long, int, int).
	 */
	@Override
	public int insertSottoscrizione(final Connection conn, final int idEvento, final Long idNodo, final Long idUtente,
			final Long idRuolo, final int tracciamento, final int idCanaleTrasmissione) throws SQLException {
		int executionRsult = 0;
		int index = 1;
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					"INSERT INTO SE_SOTTOSCRIZIONE_EVENTO (IDEVENTO, IDUTENTE, IDNODO, IDRUOLO, DATASOTTOSCRIZIONE, TRACCIAMENTO, IDCANALETRASMISSIONE) VALUES(?,?,?,?,?,?,?)");
			ps.setInt(index++, idEvento);
			ps.setLong(index++, idUtente);
			ps.setLong(index++, idNodo);
			ps.setLong(index++, idRuolo);
			ps.setDate(index++, new java.sql.Date((new Date()).getTime()));
			ps.setInt(index++, tracciamento);
			ps.setInt(index++, idCanaleTrasmissione);
			executionRsult = ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
		return executionRsult;
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#getUsernameEventoRifiutoFirma(java.sql.Connection,
	 *      java.lang.String).
	 */
	@Override
	public String getUsernameEventoRifiutoFirma(final Connection conn, final String idDocumento) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			final String query = "SELECT UTENTE.USERNAME FROM D_EVENTI_CUSTOM, UTENTE WHERE IDDOCUMENTO = ?" 
					+ " AND idutentemittente = idutente AND TIPOEVENTO in (?, ?, ?) AND Rownum <= 1 order by id desc";
			
			ps = conn.prepareStatement(query);
			ps.setInt(index++, Integer.parseInt(idDocumento));
			// Popolamento parametri tipo evento
			for (Integer idTipoEvento : EVENTI_RIFIUTO_FIRMA) {
				ps.setInt(index++, idTipoEvento);
			}
			
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString("USERNAME");
			}
		} catch (final Exception e) {
			LOGGER.error("Errore riscontrato: ", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return null;
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#updateTracciamentoAndCanaleNotifica(java.sql.Connection,
	 *      int, java.lang.Long, java.lang.Long, java.lang.Long, int, int).
	 */
	@Override
	public int updateTracciamentoAndCanaleNotifica(final Connection conn, final int idEvento, final Long idNodo,
			final Long idUtente, final Long idRuolo, final int tracciamento, final int idCanaleTrasmissione)
			throws SQLException {
		int executionRsult = 0;
		int index = 1;
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(
					"UPDATE SE_SOTTOSCRIZIONE_EVENTO  " 
							+ " set TRACCIAMENTO = ?, IDCANALETRASMISSIONE = ? "
							+ " WHERE IDEVENTO = ? AND IDUTENTE = ? AND IDNODO = ? AND IDRUOLO = ?");
			ps.setInt(index++, tracciamento);
			ps.setInt(index++, idCanaleTrasmissione);
			ps.setInt(index++, idEvento);
			ps.setLong(index++, idUtente);
			ps.setLong(index++, idNodo);
			ps.setLong(index++, idRuolo);
			executionRsult = ps.executeUpdate();
			return executionRsult;
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#getSottoscrizioniListByUser(java.sql.Connection,
	 *      java.lang.Long, java.lang.Long, java.lang.Long).
	 */
	@Override
	public List<EventoSottoscrizioneDTO> getSottoscrizioniListByUser(final Connection conn, final Long idutente,
			final Long idNodo, final Long idRuolo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			final String sql = " SELECT se.*, te.*, ce.*, n.IDAOO, ct.NOME "
					+ " FROM SE_SOTTOSCRIZIONE_EVENTO se, SE_TIPO_EVENTO te, SE_CATEGORIA_EVENTO ce, NODO n, SE_CANALE_TRASMISSIONE ct "
					+ " WHERE se.IDUTENTE = ? " + " AND se.IDNODO = ? " + " AND se.IDRUOLO = ? "
					+ " AND se.IDNODO = n.IDNODO " + " AND se.IDEVENTO = te.IDEVENTO "
					+ " AND te.IDCATEGORIA=ce.IDCATEGORIA " + " AND ct.ID = se.IDCANALETRASMISSIONE";
			ps = conn.prepareStatement(sql);
			ps.setInt(index++, idutente.intValue());
			ps.setInt(index++, idNodo.intValue());
			ps.setInt(index++, idRuolo.intValue());
			rs = ps.executeQuery();
			return fitResultsetIntoObj(rs);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#getIdSottoscrizione(java.sql.Connection,
	 *      java.lang.Long, java.lang.Long, java.lang.Long, int).
	 */
	@Override
	public Integer getIdSottoscrizione(final Connection conn, final Long idUtente, final Long idNodo,
			final Long idRuolo, final int idEvento) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			ps = conn.prepareStatement("SELECT * " + " from SE_SOTTOSCRIZIONE_EVENTO  " + " where IDUTENTE = ? "
					+ " and IDNODO = ? " + " and IDRUOLO = ? " + " and IDEVENTO = ? ");
			ps.setLong(index++, idUtente);
			ps.setLong(index++, idNodo);
			ps.setLong(index++, idRuolo);
			ps.setInt(index++, idEvento);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getInt(Constants.Sottoscrizioni.EVENTO_FIELD_IDSOTTOSCRIZIONE);
			}
		} finally {
			closeStatement(ps, rs);
		}
		return null;
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#rimuoviSottoscrizioni(java.sql.Connection,
	 *      int, int, int).
	 */
	@Override
	public void rimuoviSottoscrizioni(final Connection conn, final int idutente, final int idNodo, final int idRuolo)
			throws SQLException {
		PreparedStatement ps = null;
		int index = 1;
		try {
			ps = conn.prepareStatement(
					"DELETE FROM SE_SOTTOSCRIZIONE_EVENTO WHERE IDUTENTE = ? AND IDNODO = ? AND IDRUOLO = ?");
			ps.setInt(index++, idutente);
			ps.setInt(index++, idNodo);
			ps.setInt(index++, idRuolo);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#insertListSottoscrizioni(java.sql.Connection,
	 *      java.util.List, java.lang.Long, java.lang.Long, java.lang.Long, int,
	 *      int).
	 */
	@Override
	public int insertListSottoscrizioni(final Connection conn, final List<EventoSottoscrizioneDTO> listaSottoscrizioni,
			final Long idNodo, final Long idUtente, final Long idRuolo, final int tracciamento,
			final int idCanaleNotifica) throws SQLException {
		int num = 0;
		if (listaSottoscrizioni != null) {
			for (final EventoSottoscrizioneDTO ev : listaSottoscrizioni) {
				num += insertSottoscrizione(conn, ev.getIdEvento(), idNodo, idUtente, idRuolo, tracciamento,
						idCanaleNotifica);
			}
		}
		return num;
	}

	/**
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private static List<EventoSottoscrizioneDTO> fitResultsetIntoObj(final ResultSet rs) throws SQLException {
		final List<EventoSottoscrizioneDTO> sottoscrizioniList = new ArrayList<>();
		if (null != rs) {
			while (rs.next()) {
				sottoscrizioniList.add(getEventoSottoscrizioni(rs));
			}
		}
		return sottoscrizioniList;
	}

	private static EventoSottoscrizioneDTO getEventoSottoscrizioni(final ResultSet rs) throws SQLException {
		EventoSottoscrizioneDTO subscriptionUser = null;
		subscriptionUser = new EventoSottoscrizioneDTO();
		subscriptionUser.setIdSottoscrizione(rs.getInt(Constants.Sottoscrizioni.EVENTO_FIELD_IDSOTTOSCRIZIONE));
		subscriptionUser.setDettaglioEvento(rs.getString(Constants.Sottoscrizioni.EVENTO_FIELD_DETTAGLIOEVENTO));
		subscriptionUser.setIdAoo(rs.getInt(Constants.Sottoscrizioni.EVENTO_FIELD_IDAOO));
		subscriptionUser.setIdEvento(rs.getInt(Constants.Sottoscrizioni.EVENTO_FIELD_IDEVENTO));
		subscriptionUser.setNomeEvento(rs.getString(Constants.Sottoscrizioni.EVENTO_FIELD_DESCRIZIONEEVENTO));
		subscriptionUser.setTipologiaEvento(rs.getString(Constants.Sottoscrizioni.EVENTO_FIELD_DESCRIZIONECATEGORIA));
		subscriptionUser.setIdNodo(rs.getInt(Constants.Sottoscrizioni.EVENTO_FIELD_IDNODO));
		subscriptionUser.setIdRuolo(rs.getInt(Constants.Sottoscrizioni.EVENTO_FIELD_IDRUOLO));
		subscriptionUser.setIdUtente(rs.getInt(Constants.Sottoscrizioni.EVENTO_FIELD_IDUTENTE));
		subscriptionUser.setScadenzaDa(rs.getInt(Constants.Sottoscrizioni.EVENTO_FIELD_SCADENZA_DA));
		subscriptionUser.setScadenzaA(rs.getInt(Constants.Sottoscrizioni.EVENTO_FIELD_SCADENZA_A));
		subscriptionUser.setTracciamento(rs.getInt(Constants.Sottoscrizioni.EVENTO_FIELD_TRACCIAMENTO));
		subscriptionUser.setSelected(true);
		final CanaleTrasmissioneDTO canaleTrasmissione = new CanaleTrasmissioneDTO();
		canaleTrasmissione.setId(rs.getInt(Constants.Sottoscrizioni.EVENTO_FIELD_IDCANALETRASMISSIONE));
		canaleTrasmissione.setNome(rs.getString(Constants.Sottoscrizioni.CANALE_TRASMISSIONE_FIELD_NOME));
		subscriptionUser.setCanaleTrasmissione(canaleTrasmissione);
		return subscriptionUser;
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#rimuoviSottoscrizione(java.sql.Connection,
	 *      int, int, int, int).
	 */
	@Override
	public int rimuoviSottoscrizione(final Connection conn, final int idutente, final int idNodo, final int idEvento,
			final int idRuolo) throws SQLException {
		PreparedStatement ps = null;
		int index = 1;
		int result = 0;
		try {
			ps = conn.prepareStatement(
					"DELETE FROM SE_SOTTOSCRIZIONE_EVENTO WHERE IDUTENTE = ? AND IDNODO = ? AND IDEVENTO = ? AND IDRUOLO = ?");
			ps.setInt(index++, idutente);
			ps.setInt(index++, idNodo);
			ps.setInt(index++, idEvento);
			ps.setInt(index++, idRuolo);
			result = ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#getSottoscrizioniListByIdCategoria(java.sql.Connection,
	 *      int, int).
	 */
	@Override
	public List<EventoSottoscrizioneDTO> getSottoscrizioniListByIdCategoria(final Connection con, final int idCategoria,
			final int idAoo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			ps = con.prepareStatement("select se.*, te.*, ce.*, n.IDAOO, ct.NOME  "
					+ " from SE_SOTTOSCRIZIONE_EVENTO se, SE_TIPO_EVENTO te, NODO n, SE_CATEGORIA_EVENTO ce, SE_CANALE_TRASMISSIONE ct "
					+ " where se.IDEVENTO in ( select IDEVENTO from SE_TIPO_EVENTO where IDCATEGORIA = ?) "
					+ " and se.IDEVENTO = te.IDEVENTO  " + " and se.IDNODO = n.IDNODO  " + " and n.IDAOO = ? "
					+ " and ce.IDCATEGORIA = te.IDCATEGORIA " + " and ct.ID = se.IDCANALETRASMISSIONE");
			ps.setInt(index++, idCategoria);
			ps.setInt(index++, idAoo);
			rs = ps.executeQuery();
			return fitResultsetIntoObj(rs);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#getSottoscrizioniListByIdEventi(java.sql.Connection,
	 *      java.lang.Integer[], int).
	 */
	@Override
	public List<EventoSottoscrizioneDTO> getSottoscrizioniListByIdEventi(final Connection con,
			final Integer[] idEventiList, final int idAoo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			final int numEventi = idEventiList.length;
			final StringBuilder idEventi = new StringBuilder();
			for (int i = 0; i < numEventi; i++) {
				idEventi.append("?");
				if (i < (numEventi - 1)) {
					idEventi.append(" , ");
				}
			}

			String sql = "select se.*, te.*, ce.*, n.IDAOO, ct.NOME  "
					+ " from SE_SOTTOSCRIZIONE_EVENTO se, SE_TIPO_EVENTO te, NODO n, SE_CATEGORIA_EVENTO ce, SE_CANALE_TRASMISSIONE ct  "
					+ " where se.IDEVENTO in (STRING_TO_REPLACE) " + " and se.IDEVENTO = te.IDEVENTO  "
					+ " and se.IDNODO = n.IDNODO  " + " and n.IDAOO = ?  " + " and ce.IDCATEGORIA = te.IDCATEGORIA  "
					+ " and ct.ID = se.IDCANALETRASMISSIONE";
			sql = sql.replace("STRING_TO_REPLACE", idEventi.toString());
			ps = con.prepareStatement(sql);

			for (final Integer id : idEventiList) {
				ps.setInt(index++, id);
			}

			ps.setInt(index++, idAoo);

			rs = ps.executeQuery();
			return fitResultsetIntoObj(rs);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#getEventiListByIdAoo(java.sql.Connection,
	 *      int, it.ibm.red.business.dto.CanaleTrasmissioneDTO).
	 */
	@Override
	public List<EventoSottoscrizioneDTO> getEventiListByIdAoo(final Connection con, final int idAoo,
			final CanaleTrasmissioneDTO canaleDefault) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			ps = con.prepareStatement(
					"SELECT te.IDEVENTO, te.DESCRIZIONEEVENTO, te.DETTAGLIOEVENTO, ate.IDAOO, ce.DESCRIZIONECATEGORIA, te.SCADENZADA, te.SCADENZAA "
							+ " FROM SE_TIPO_EVENTO te, SE_CATEGORIA_EVENTO ce, SE_AOO_TIPO_EVENTO ate "
							+ " WHERE ce.IDCATEGORIA = te.IDCATEGORIA " + " AND te.IDEVENTO = ate.IDEVENTO "
							+ " AND ate.IDAOO = ? " + " ORDER BY te.IDCATEGORIA");
			ps.setInt(index++, idAoo);
			rs = ps.executeQuery();

			final List<EventoSottoscrizioneDTO> eventi = new ArrayList<>();
			EventoSottoscrizioneDTO evento = null;
			while (rs.next()) {

				evento = new EventoSottoscrizioneDTO(rs.getInt("IDEVENTO"), rs.getString("DESCRIZIONEEVENTO"),
						rs.getString("DETTAGLIOEVENTO"), rs.getString("DESCRIZIONECATEGORIA"), rs.getInt("IDAOO"),
						rs.getInt("SCADENZADA"), rs.getInt("SCADENZAA"), canaleDefault);

				eventi.add(evento);
			}

			return eventi;
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#rimuoviDoc(java.lang.Long,
	 *      java.lang.Long, java.lang.Long, int, java.sql.Connection).
	 */
	@Override
	public int rimuoviDoc(final Long idUtente, final Long idNodo, final Long idRuolo, final int idDocumento,
			final Connection con) {
		PreparedStatement ps = null;
		int index = 1;
		try {
			final List<EventoSottoscrizioneDTO> sottoscrizioniUtente = getSottoscrizioniListByUser(con, idUtente, idNodo,
					idRuolo);
			ps = con.prepareStatement(
					"DELETE FROM se_sottoscrizione_evento_doc WHERE idsottoscrizione = ? AND iddocumento = ?");
			for (final EventoSottoscrizioneDTO sottoscrizione : sottoscrizioniUtente) {
				index = 1;
				ps.setInt(index++, sottoscrizione.getIdSottoscrizione());
				ps.setInt(index++, idDocumento);
				ps.executeUpdate();
			}
			return 0;
		} catch (final Exception e) {
			LOGGER.error(ERROR_RIMOZIONE_SOTTOSCRIZIONE_MSG + idDocumento, e);
			throw new RedException(ERROR_RIMOZIONE_SOTTOSCRIZIONE_MSG + idDocumento, e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#rimuoviDoc(java.lang.Long,
	 *      java.lang.Long, java.lang.Long, int, int, java.sql.Connection).
	 */
	@Override
	public int rimuoviDoc(final Long idUtente, final Long idNodo, final Long idRuolo, final int idDocumento,
			final int idEvento, final Connection con) {
		PreparedStatement ps = null;
		int index = 1;
		try {

			final Integer idSottoscrizione = getIdSottoscrizione(con, idUtente, idNodo, idRuolo, idEvento);

			ps = con.prepareStatement(
					"DELETE FROM se_sottoscrizione_evento_doc WHERE idsottoscrizione = ? AND iddocumento = ?");
			index = 1;
			ps.setInt(index++, idSottoscrizione);
			ps.setInt(index++, idDocumento);
			ps.executeUpdate();
			return 0;
		} catch (final Exception e) {
			LOGGER.error(ERROR_RIMOZIONE_SOTTOSCRIZIONE_MSG + idDocumento, e);
			throw new RedException(ERROR_RIMOZIONE_SOTTOSCRIZIONE_MSG + idDocumento, e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#aggiungiDoc(java.sql.Connection,
	 *      java.lang.Long, java.lang.Long, java.lang.Long, int).
	 */
	@Override
	public int aggiungiDoc(final Connection con, final Long idUtente, final Long idNodo, final Long idRuolo,
			final int idDocumento) {
		PreparedStatement ps = null;
		int index = 1;
		try {
			final List<EventoSottoscrizioneDTO> sottoscrizioniUtente = getSottoscrizioniListByUser(con, idUtente, idNodo,
					idRuolo);
			ps = con.prepareStatement(
					"INSERT INTO se_sottoscrizione_evento_doc (IDSOTTOSCRIZIONE, IDDOCUMENTO) values (?, ?)");
			for (final EventoSottoscrizioneDTO sottoscrizione : sottoscrizioniUtente) {
				index = 1;
				ps.setInt(index++, sottoscrizione.getIdSottoscrizione());
				ps.setInt(index++, idDocumento);
				ps.executeUpdate();
			}
			return 0;
		} catch (final Exception e) {
			LOGGER.error(ERROR_INSERIMENTO_SOTTOSCRIZIONE_MSG + idDocumento, e);
			throw new RedException(ERROR_INSERIMENTO_SOTTOSCRIZIONE_MSG + idDocumento, e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#aggiungiDoc(java.sql.Connection,
	 *      java.lang.Long, java.lang.Long, java.lang.Long, int, int).
	 */
	@Override
	public int aggiungiDoc(final Connection con, final Long idUtente, final Long idNodo, final Long idRuolo,
			final int idDocumento, final int idEvento) {
		PreparedStatement ps = null;
		int index = 1;
		try {
			final Integer idSottoscrizione = getIdSottoscrizione(con, idUtente, idNodo, idRuolo, idEvento);
			ps = con.prepareStatement(
					"INSERT INTO se_sottoscrizione_evento_doc (IDSOTTOSCRIZIONE, IDDOCUMENTO) values (?, ?)");
			index = 1;
			ps.setInt(index++, idSottoscrizione);
			ps.setInt(index++, idDocumento);
			ps.executeUpdate();
			return 0;
		} catch (final Exception e) {
			LOGGER.error(ERROR_INSERIMENTO_SOTTOSCRIZIONE_MSG + idDocumento, e);
			throw new RedException(ERROR_INSERIMENTO_SOTTOSCRIZIONE_MSG + idDocumento, e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#isDocTracciato(java.sql.Connection,
	 *      java.lang.Long, java.lang.Long, java.lang.Long, int).
	 */
	@Override
	public boolean isDocTracciato(final Connection con, final Long idUtente, final Long idNodo, final Long idRuolo,
			final int idDocumento) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			ps = con.prepareStatement(
					"SELECT count(*) " + FROM_SE_SOTTOSCRIZIONE_EVENTO_DOC_SED_SE_SOTTOSCRIZIONE_EVENTO_SE
							+ " where sed.iddocumento = ? " + " and sed.idsottoscrizione = se.idsottoscrizione  "
							+ AND_SE_IDUTENTE + AND_SE_IDNODO + " and se.IDRUOLO = ?");
			ps.setInt(index++, idDocumento);
			ps.setLong(index++, idUtente);
			ps.setLong(index++, idNodo);
			ps.setLong(index++, idRuolo);
			rs = ps.executeQuery();
			if (rs.next()) {
				final int count = rs.getInt(1);
				return count != 0;
			}
		} finally {
			closeStatement(ps, rs);
		}
		return false;

	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#getDocumentiTracciati(java.sql.Connection,
	 *      java.lang.Long, java.lang.Long, java.lang.Long).
	 */
	@Override
	public List<Integer> getDocumentiTracciati(final Connection conn, final Long idUtente, final Long idNodo,
			final Long idRuolo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		final List<Integer> idDocTracciati = new ArrayList<>();
		try {
			ps = conn.prepareStatement("SELECT distinct sed.iddocumento  "
					+ FROM_SE_SOTTOSCRIZIONE_EVENTO_DOC_SED_SE_SOTTOSCRIZIONE_EVENTO_SE
					+ " where  sed.idsottoscrizione = se.idsottoscrizione  " + AND_SE_IDUTENTE
					+ AND_SE_IDNODO + " and se.IDRUOLO = ?");
			ps.setLong(index++, idUtente);
			ps.setLong(index++, idNodo);
			ps.setLong(index++, idRuolo);
			rs = ps.executeQuery();
			while (rs.next()) {
				idDocTracciati.add(rs.getInt(1));
			}
		} finally {
			closeStatement(ps, rs);
		}
		return idDocTracciati;
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#getDocumentiTracciati(java.sql.Connection,
	 *      java.lang.Long, java.lang.Long, java.lang.Long, int).
	 */
	@Override
	public List<Integer> getDocumentiTracciati(final Connection conn, final Long idUtente, final Long idNodo,
			final Long idRuolo, final int idEvento) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		final List<Integer> idDocTracciati = new ArrayList<>();
		try {
			ps = conn.prepareStatement("SELECT distinct sed.iddocumento  "
					+ FROM_SE_SOTTOSCRIZIONE_EVENTO_DOC_SED_SE_SOTTOSCRIZIONE_EVENTO_SE
					+ " where  sed.idsottoscrizione = se.idsottoscrizione " + AND_SE_IDUTENTE
					+ AND_SE_IDNODO + " and se.IDRUOLO = ? " + " and se.IDEVENTO = ?");
			ps.setLong(index++, idUtente);
			ps.setLong(index++, idNodo);
			ps.setLong(index++, idRuolo);
			ps.setInt(index++, idEvento);
			rs = ps.executeQuery();
			while (rs.next()) {
				idDocTracciati.add(rs.getInt(1));
			}
		} finally {
			closeStatement(ps, rs);
		}
		return idDocTracciati;
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#getTemplate(java.sql.Connection,
	 *      int).
	 */
	@Override
	public CategoriaEventoDTO getTemplate(final Connection connection, final int idEvento) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		CategoriaEventoDTO template = null;

		try {
			ps = connection.prepareStatement(
					"SELECT ce.OGGETTOMAILTEMPLATE, ce.TESTOMAILTEMPLATE, te.DESCRIZIONEEVENTO, ce.IDCATEGORIA "
							+ " FROM SE_CATEGORIA_EVENTO ce, SE_TIPO_EVENTO te "
							+ " where ce.IDCATEGORIA = te.IDCATEGORIA " + " AND te.IDEVENTO = ? ");
			ps.setInt(index++, idEvento);
			rs = ps.executeQuery();
			while (rs.next()) {
				template = populateTemplate(rs);
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero del template dell'e-mail", e);
		} finally {
			closeStatement(ps, rs);
		}

		return template;
	}

	private static CategoriaEventoDTO populateTemplate(final ResultSet rs) throws SQLException {
		final CategoriaEventoDTO templateNotificaSottoscrizione = new CategoriaEventoDTO();
		templateNotificaSottoscrizione.setOggettoMail(rs.getString("oggettomailtemplate"));
		templateNotificaSottoscrizione.setCorpoMail(rs.getString("testomailtemplate"));
		templateNotificaSottoscrizione.setDescrizioneEvento(rs.getString("descrizioneevento"));
		templateNotificaSottoscrizione.setIdCategoria(rs.getInt("idcategoria"));
		return templateNotificaSottoscrizione;
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#populateMittente(java.sql.Connection,
	 *      int, int,
	 *      it.ibm.red.business.service.concrete.NotificaSRV.MittenteAzione).
	 */
	@Override
	public void populateMittente(final Connection connection, final int idEvento, final int idDocumento,
			final int idNodoDestinatario, final int idUtenteDestinatario, final MittenteAzione mittente) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			ps = connection.prepareStatement(
					"select n.DESCRIZIONE as NODO_MITTENTE, u.COGNOME||' '||u.NOME as UTENTE_MITTENTE "
							+ "  from D_EVENTI_CUSTOM ec, NODO n, UTENTE u "
							+ " where ec.IDDOCUMENTO = ? and ec.TIPOEVENTO = ? "
							+ "   and ec.IDNODODESTINATARIO = ? and ec.IDUTENTEDESTINATARIO = ? "
							+ "   and ec.IDNODOMITTENTE = n.IDNODO and ec.IDUTENTEMITTENTE = u.IDUTENTE "
							+ " order by ec.timestampoperazione desc  ");
			ps.setInt(index++, idDocumento);
			ps.setInt(index++, idEvento);
			ps.setInt(index++, idNodoDestinatario);
			ps.setInt(index++, idUtenteDestinatario);
			rs = ps.executeQuery();
			if (rs.next()) {
				mittente.setUfficio(rs.getString("NODO_MITTENTE"));
				mittente.setUtente(rs.getString("UTENTE_MITTENTE"));
			}
		} catch (final SQLException e) {
			throw new RedException("Si è verificato un errore durante il recupero del mittente", e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#getDisabilitazioniByUser(java.sql.Connection,
	 *      java.lang.String).
	 */
	@Override
	public List<DisabilitazioneNotificheUtenteDTO> getDisabilitazioniByUser(final Connection connection,
			final String idUtente) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final int index = 1;

			final String sql = "select * " + " from SE_DISAB_NOTIFICA_UTENTE " + " where IDUTENTE = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(index, idUtente);
			rs = ps.executeQuery();

			final ArrayList<DisabilitazioneNotificheUtenteDTO> resultList = new ArrayList<>();
			while (rs.next()) {
				resultList.add(populateDisabilitazioneNotificheUtenteDTO(rs));
			}

			return resultList;
		} catch (final SQLException e) {
			throw new RedException(
					"Si è verificato un errore durante il recupero delle disabilitazioni per l'utente: " + idUtente, e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	private static DisabilitazioneNotificheUtenteDTO populateDisabilitazioneNotificheUtenteDTO(final ResultSet rs) throws SQLException {
		final DisabilitazioneNotificheUtenteDTO disabilitazioneNotificheUtente = new DisabilitazioneNotificheUtenteDTO();
		disabilitazioneNotificheUtente.setIdUtente(rs.getString("idutente"));
		disabilitazioneNotificheUtente.setIdCanaleTrasmissione(rs.getInt("idcanaletrasmissione"));
		disabilitazioneNotificheUtente.setDataDa(rs.getDate("datada"));
		disabilitazioneNotificheUtente.setDataA(rs.getDate("dataa"));

		return disabilitazioneNotificheUtente;
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#getDisabilitazioniByUserAndCanaleTrasmissioneToday(java.lang.String,
	 *      int, java.sql.Connection).
	 */
	@Override
	public DisabilitazioneNotificheUtenteDTO getDisabilitazioniByUserAndCanaleTrasmissioneToday(final String idUtente,
			final int idCanaleTrasmissione, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		DisabilitazioneNotificheUtenteDTO disabilitazioneNotificheUtente = null;

		try {
			int index = 1;
			final String sql = "select * " + " from SE_DISAB_NOTIFICA_UTENTE " + " where IDUTENTE = ? "
					+ " and IDCANALETRASMISSIONE = ? " + " and ( " + " (trunc(SYSDATE) between DATADA and DATAA) "
					+ " or (DATADA is null and DATAA is null) " + " )";
			ps = con.prepareStatement(sql);
			ps.setString(index++, idUtente);
			ps.setInt(index, idCanaleTrasmissione);
			rs = ps.executeQuery();
			if (rs.next()) {
				disabilitazioneNotificheUtente = populateDisabilitazioneNotificheUtenteDTO(rs);
			}
		} catch (final SQLException e) {
			throw new RedException(
					"Si è verificato un errore durante il recupero delle disabilitazioni per l'utente: " + idUtente, e);
		} finally {
			closeStatement(ps, rs);
		}

		return disabilitazioneNotificheUtente;
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#getDocumentiPostSiglaVisto(java.sql.Connection, int, int, int, int).
	 */
	@Override
	public List<InfoDocumenti> getDocumentiPostSiglaVisto(final Connection con, final int idAoo, final int idEvento,
			final int idUtente, final int idNodo) {
		CallableStatement cs = null;
		ResultSet rs = null;
		InfoDocumenti item = null;
		final List<InfoDocumenti> items = new ArrayList<>();

		try {
			cs = con.prepareCall("{call p_cerca_doc_post_visto_sigla(?,?,?,?,?)}");
			cs.setInt(1, idAoo);
			cs.setInt(2, idEvento);
			cs.setInt(3, idUtente);
			cs.setInt(4, idNodo);
			cs.registerOutParameter(5, OracleTypes.CURSOR);

			cs.execute();

			rs = (ResultSet) cs.getObject(5);
			while (rs.next()) {
				item = InfoDocumenti.getInstance();
				item.setIdDocumento(new Integer(rs.getString(1)));
				items.add(item);
			}

			return items;
		} catch (final SQLException e) {
			throw new RedException(
					"Si è verificato un errore durante il recupero dei documenti post sigla visto per l'utente: "
							+ idUtente + ", ufficio: " + idNodo + ", evento: " + idEvento,
					e);
		} finally {
			closeStatement(cs, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#delete(it.ibm.red.business.dto.EventoSottoscrizioneDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public void delete(final EventoSottoscrizioneDTO sottoscrizioneToDelete, final Connection con) {
		PreparedStatement ps = null;
		int index = 1;

		try {
			ps = con.prepareStatement(
					"DELETE FROM SE_SOTTOSCRIZIONE_EVENTO WHERE IDUTENTE = ? AND IDNODO = ? AND IDEVENTO = ? AND IDRUOLO = ?");
			index = 1;
			ps.setInt(index++, sottoscrizioneToDelete.getIdUtente());
			ps.setInt(index++, sottoscrizioneToDelete.getIdNodo());
			ps.setInt(index++, sottoscrizioneToDelete.getIdEvento());
			ps.setInt(index++, sottoscrizioneToDelete.getIdRuolo());

			ps.executeUpdate();
		} catch (final Exception e) {
			LOGGER.error(
					"Errore durante la rimozione della sottoscrizione =" + sottoscrizioneToDelete.getIdSottoscrizione(),
					e);
			throw new RedException(
					"Errore durante la rimozione della sottoscrizione =" + sottoscrizioneToDelete.getIdSottoscrizione(),
					e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#deleteMails(java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public void deleteMails(final Integer idUtente, final Connection con) {
		PreparedStatement ps = null;
		int index = 1;
		try {

			ps = con.prepareStatement("DELETE FROM SE_MAIL_ADDRESS_UTENTE WHERE IDUTENTE=?");
			index = 1;
			ps.setString(index++, idUtente.toString());
			ps.executeUpdate();

		} catch (final Exception e) {
			LOGGER.error("Errore durante la rimozione delle email dell'utente =" + idUtente, e);
			throw new RedException("Errore durante la rimozione delle email dell'utente =" + idUtente, e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#insertMail(it.ibm.red.business.dto.MailAddressDTO, java.sql.Connection).
	 */
	@Override
	public void insertMail(final MailAddressDTO mail, final Connection con) {
		PreparedStatement ps = null;
		int index = 1;
		try {

			ps = con.prepareStatement("INSERT INTO SE_MAIL_ADDRESS_UTENTE (ADDRESS,IDUTENTE) VALUES (?,?)");
			index = 1;
			ps.setString(index++, mail.getAddress());
			ps.setString(index++, Integer.toString(mail.getIdUtente()));
			ps.executeUpdate();

		} catch (final Exception e) {
			LOGGER.error("Errore durante la registrazione della mail dell'utente =" + mail.getIdUtente(), e);
			throw new RedException("Errore durante la registrazione della mail dell'utente =" + mail.getIdUtente(), e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#getMailsSottoscrizioni(java.sql.Connection, int).
	 */
	@Override
	public List<MailAddressDTO> getMailsSottoscrizioni(final Connection connection, final int idUtente)
			throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		final List<MailAddressDTO> mails = new ArrayList<>();
		try {
			ps = connection.prepareStatement("SELECT * FROM SE_MAIL_ADDRESS_UTENTE WHERE IDUTENTE = ?  ");
			ps.setInt(index++, idUtente);
			rs = ps.executeQuery();
			while (rs.next()) {
				mails.add(new MailAddressDTO(Integer.parseInt(rs.getString("IDUTENTE")),
						Integer.parseInt(rs.getString("IDMAIL")), rs.getString("ADDRESS")));
			}
		} finally {
			closeStatement(ps, rs);
		}

		return mails;
	}

	/**
	 * @see it.ibm.red.business.dao.ISottoscrizioniDAO#getCountSottoscrizioni(java.sql.Connection,
	 *      java.lang.Long, int, java.lang.Long).
	 */
	@Override
	public int getCountSottoscrizioni(final Connection connection, final Long idUtente, final int ultimiNgiorni,
			final Long idAOO) {
		int numeroNotifiche = 0;

		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			ps = connection.prepareStatement(" SELECT count(sn.idnotifica) AS NUMERO_NOTIFICHE "
					+ "  FROM SE_NOTIFICA_SOTTOSCRIZIONE sn, NODO N " + " WHERE N.IDNODO = SN.IDNODO "
					+ "   AND SN.IDUTENTE = ? " + "   AND SN.DATAEVENTO > (SYSDATE - ?) " + "   AND SN.ELIMINATO = 0 "
					+ "   AND SN.VISUALIZZATO = 0 " + "   AND N.IDAOO = ?");
			ps.setLong(index++, idUtente);
			ps.setInt(index++, ultimiNgiorni);
			ps.setLong(index++, idAOO);

			rs = ps.executeQuery();
			if (rs.next()) {
				numeroNotifiche = rs.getInt("NUMERO_NOTIFICHE");
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return numeroNotifiche;

	}

}