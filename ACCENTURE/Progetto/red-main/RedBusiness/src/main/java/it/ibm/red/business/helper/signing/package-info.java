/**
 * @author CPIERASC
 *
 *	In questo package saranno presenti le classi di utilità per la gestione della firma digitale remota in modalità
 *	CADES e PADES (su dispositivi mobili la firma locale incontrerebbe molte difficoltà, prima tra tutte l'impossibilità
 *	di utilizzare applet per l'accesso al certificato presente nella smartcard).
 *
 */
package it.ibm.red.business.helper.signing;
