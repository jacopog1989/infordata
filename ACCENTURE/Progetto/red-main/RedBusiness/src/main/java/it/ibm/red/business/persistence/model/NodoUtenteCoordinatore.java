package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * Model per nodo utente coordinatore
 */
public class NodoUtenteCoordinatore implements Serializable {

	/** 
	 * The Constant serialVersionUID. 
	 */
	private static final long serialVersionUID = -7263285443526837700L;

	/**
	 * Identificativo ufficio coordinatore
	 */
	private Long idUfficioCoordinatore;
	
	/**
	 * Identificativo utente coordinatore
	 */
	private Long idUtenteCoordinatore;
	
	/**
	 * Nome utente coordinatore
	 */
	private String nome;
	
	/**
	 * Cognome utente coordinatore
	 */
	private String cognome;
	
	/**
	 * Getter id ufficio coordinatore.
	 *
	 * @return the idUfficioCoordinatore
	 */
	public Long getIdUfficioCoordinatore() {
		return idUfficioCoordinatore;
	}


	/**
	 * Setter id ufficio coordinatore.
	 *
	 * @param idUfficioCoordinatore the idUfficioCoordinatore to set
	 */
	public void setIdUfficioCoordinatore(final Long idUfficioCoordinatore) {
		this.idUfficioCoordinatore = idUfficioCoordinatore;
	}


	/**
	 * Getter id utente coordinatore.
	 *
	 * @return the idUtenteCoordinatore
	 */
	public Long getIdUtenteCoordinatore() {
		return idUtenteCoordinatore;
	}


	/**
	 * Setter id utente coordinatore.
	 *
	 * @param idUtenteCoordinatore the idUtenteCoordinatore to set
	 */
	public void setIdUtenteCoordinatore(final Long idUtenteCoordinatore) {
		this.idUtenteCoordinatore = idUtenteCoordinatore;
	}


	/**
	 * Getter nome.
	 *
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}


	/**
	 * Setter nome.
	 *
	 * @param nome the nome to set
	 */
	public void setNome(final String nome) {
		this.nome = nome;
	}


	/**
	 * Getter del cognome.
	 *
	 * @return the cognome
	 */
	public String getCognome() {
		return cognome;
	}


	/**
	 * Setter del cognome.
	 *
	 * @param cognome the cognome to set
	 */
	public void setCognome(final String cognome) {
		this.cognome = cognome;
	}	
}