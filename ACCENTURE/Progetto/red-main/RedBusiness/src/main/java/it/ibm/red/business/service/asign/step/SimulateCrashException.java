package it.ibm.red.business.service.asign.step;

import it.ibm.red.business.exception.RedException;

/**
 * Exception fittizia, viene lanciata per mandare Evo in errore per effettuare test.
 */
public class SimulateCrashException extends RedException {

	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = 6343402422636174579L;

}
