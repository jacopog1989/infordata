package it.ibm.red.business.enums;

/**
 * Tipologia notifiche interoperabili.
 */
public enum TipoNotificaInteropEnum {
	
	/**
	 * Valore.
	 */
	CONFERMA_RICEZIONE("CONFERMA RICEZIONE"),
	
	/**
	 * Valore.
	 */
	NOTIFICA_ECCEZIONE("NOTIFICA ECCEZIONE"),
	
	/**
	 * Valore.
	 */
	AGGIORNAMENTO_CONFERMA("AGGIORNAMENTO CONFERMA"),
	
	/**
	 * Valore.
	 */
	ANNULLAMENTO_PROTOCOLLAZIONE("ANNULLAMENTO PROTOCOLLAZIONE");

	/**
	 * Display name.
	 */
	private String displayName;
	

	/**
	 * @param displayName
	 */
	TipoNotificaInteropEnum(final String displayName) {
		this.displayName = displayName;
	}


	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}


	/**
	 * @param nome
	 * @return
	 */
	public static TipoNotificaInteropEnum get(final String nome) {
		TipoNotificaInteropEnum output = null;
		
		for (final TipoNotificaInteropEnum tipoNotificaInterop : TipoNotificaInteropEnum.values()) {
			if (tipoNotificaInterop.name().equalsIgnoreCase(nome)) {
				output = tipoNotificaInterop;
				break;
			}
		}
		
		return output;
	}
	
	
	/**
	 * @param displayName
	 * @return
	 */
	public static TipoNotificaInteropEnum getByDisplayName(final String displayName) {
		TipoNotificaInteropEnum output = null;
		
		for (final TipoNotificaInteropEnum tipoNotificaInterop : TipoNotificaInteropEnum.values()) {
			if (tipoNotificaInterop.getDisplayName().equalsIgnoreCase(displayName)) {
				output = tipoNotificaInterop;
				break;
			}
		}
		
		return output;
	}
	
}
