package it.ibm.red.business.service.asign.step;

import java.sql.Connection;
import java.util.EnumMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignStepResultDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IASignSRV;
import it.ibm.red.business.service.ISignSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.concrete.AbstractService;

/**
 * Astrazione di uno step di firma asincrona.
 * 
 * @author CristianoPierascenzi
 *
 */
@Service
public abstract class ASignStepAbstract extends AbstractService implements IASignStep {

	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = 7829952075234471016L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ASignStepAbstract.class.getName());

	/**
	 * Servizio firma asincrona.
	 */
	@Autowired
	protected IASignSRV aSignSRV;
	
	/**
	 * Servizio firma sincrona.
	 */
	@Autowired
	protected ISignSRV signSRV;
	
	/**
	 * Servizio gestione utenti.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;


	/**
	 * Esegue lo step sull'item fornito simulando una eccezione subito prima di eseguire il commit; in questo modo si innescherà il meccanismo di rollback nella condizione
	 * peggiore e si potrà valutare la resilienza dello step stesso.
	 * @return	esito dell'esecuzione
	 */
	public final ASignStepResultDTO simulateCrash(ASignItemDTO item) {
		return execute(item, true);
	}

	/**
	 * Esegue lo step sull'item fornito.
	 * @return	esito dell'esecuzione
	 */
	public ASignStepResultDTO execute(ASignItemDTO item) {
		return execute(item, false);
	}

	/**
	 * Esegue lo step sull'item fornito, eventualmente generando una eccezione fittizia per testare la resilienza dello step stesso.
	 * 
	 * @param endWithCrash	flag per richiedere la generazione della eccezione fittizia
	 * @return	esito dell'esecuzione
	 */
	private ASignStepResultDTO execute(ASignItemDTO item, Boolean endWithCrash) {
		ASignStepResultDTO out = new ASignStepResultDTO(item.getId(), getStep());
		Connection con = null;
		Map<ContextASignEnum, Object> context = new EnumMap<>(ContextASignEnum.class);
		try {
			con = setupConnection(getDataSource().getConnection(), true);
			out = run(con, item, context);
			if (Boolean.TRUE.equals(endWithCrash)) {
				throw new SimulateCrashException();
			}
			commitConnection(con);
		} catch (Exception e) {
			out.appendException(e);
			rollbackConnection(con);
			out = recovery(out, item, context);
		} finally {
			closeConnection(con);
		}
		return out;
	}
	
	/**
	 * @param item
	 * @param con
	 * @return
	 */
	protected UtenteDTO getUtenteFirmatario(ASignItemDTO item, Connection con) {
		return utenteSRV.getById(item.getIdFirmatario(), item.getIdRuoloFirmatario(), item.getIdUfficioFirmatario(), con);
	}
	
	/**
	 * @param item
	 * @param out
	 * @param utenteFirmatario
	 * @return
	 */
	protected VWWorkObject getWorkflow(ASignItemDTO item, ASignStepResultDTO out, UtenteDTO utenteFirmatario) {
		return getWorkflow(item, out, utenteFirmatario, null);
	}

	/**
	 * @param item
	 * @param out	wVWWorkObject - workflow
	 * @param utenteFirmatario
	 * @param fpeh  FilenetPEHelper, questo metodo non lo chiude, ma lo apre se null
	 * */
	protected VWWorkObject getWorkflow(ASignItemDTO item, ASignStepResultDTO out, UtenteDTO utenteFirmatario, FilenetPEHelper fpeh) {
		VWWorkObject wob = null;
		
		try {
			if (fpeh == null) {
				fpeh = new FilenetPEHelper(utenteFirmatario.getFcDTO());
			}
			
			wob = fpeh.getWorkFlowByWob(item.getWobNumber(), true);
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero del workflow per l'item: " + item.getId(), e);
			out.setStatus(false);
			out.appendMessage("[KO] Errore durante il recupero del workflow: " + e.getMessage());
		} 
		
		return wob;
	}
	
	/**
	 * Questo metodo racchiude la logica di esecuzione dello step. Il metodo è astratto in quanto rappresenta la logica di business che caratterizza in maniera unica uno step concreto.
	 * L'implementazione del metodo non dovrebbe eseguire il commit della transazione, questo è affidato all'infrastruttura.
	 * 
	 * @param item	item su cui operare
	 * @param con		connessione da utilizzare
	 * @param context	contenitore di informazioni che specificano le attività svolte nella lavorazione (utile all'Orchestratore per eseguire un'azione di recovery efficacie)
	 * @return			esito dell'esecuzione
	 */
	protected abstract ASignStepResultDTO run(Connection con, ASignItemDTO item, Map<ContextASignEnum, Object> context);
	
	/**
	 * Questo metodo racchiude la logica di recovery standard per un processo
	 * associato ad uno step. Si limita ad aggiornare il log e lo stato dello Step
	 * in quanto ogni processo che gestisce uno step effettua il rollback delle
	 * transazioni riportando l'applicativo ad uno stato coerente. Se però vengono
	 * invocati dei servizi esterni, occorre effettuare l'override di questo metodo
	 * e implementare le contromisure che si occupano di annullare le modifiche fate
	 * in fase di esecuzione.
	 * 
	 * @param in
	 *            Informazioni raccolte durante il run dello step, queste devono
	 *            essere arricchite con le informazioni raccolte in fase di
	 *            recovery.
	 * @param item
	 *            Item su cui operare.
	 * @param context
	 *            Contesto della sessione per il recupero di eventuali informazioni.
	 * @return Esito dell'esecuzione.
	 */
	protected ASignStepResultDTO recovery(ASignStepResultDTO in, ASignItemDTO item, Map<ContextASignEnum, Object> context) {
		LOGGER.info("recovery -> START [ID item: " + item.getId() + "]");
		ASignStepResultDTO out = in;
		
		// Status a false per impostare il retry
		out.setStatus(false);
		if (StringUtils.isBlank(out.getLog())) {
			out.appendMessage(getErrorMessage());
		}
		
		LOGGER.info("recovery -> END [ID item: " + item.getId() + "]");
		return out;
	}

	/**
	 * Restituisce lo @see StepEnum a cui è associato la classe.
	 * 
	 * @return Step di riferimento della classe.
	 */
	protected abstract StepEnum getStep();

	/**
	 * Restituisce il messaggio di errore specifico per la classe che implementa
	 * questa classe.
	 * 
	 * @return Messaggio di errore associato allo Step di riferimento della classe
	 *         che viene usato in fase di
	 *         {@link #recovery(ASignStepResultDTO, ASignItemDTO, Map)}.
	 */
	protected abstract String getErrorMessage();
}
