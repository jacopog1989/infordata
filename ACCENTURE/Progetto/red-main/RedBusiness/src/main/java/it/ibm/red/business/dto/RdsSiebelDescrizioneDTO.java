package it.ibm.red.business.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.enums.StatoRdSSiebelEnum;


/**
 * Oggetto con informazioni accessorie necessarie alla visualizzazione della tabella storico.
 */
public class RdsSiebelDescrizioneDTO extends RdsSiebelDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 5018733308548882182L;
	/**
	 * Descrizione del nodo apertura rds
	 */
	private String nodoAperturaRds;
	/**
	 * Cognome dell'utente che apre rds.
	 */
	private String cognomeUtenteAperturaRds;

	/**
	 * Nome dell'utente che apre rds.
	 */
	private String nomeUtenteAperturaRds;
	/**
	 * Descrizione tipologia e gruppo apertura rds.
	 */
	private String descrizioneTipologia;

	/**
	 * Descrizione del gruppo.
	 */
	private String descrizioneGruppo;

	/**
	 * Costruttore.
	 * @return
	 */
	public String getStatoRdsSiebel() {
		final StatoRdSSiebelEnum stato = StatoRdSSiebelEnum.getByCode(getIdStatoRdsSiebel());
		return stato == null ? "-" : stato.toString();
	}
	
	/**
	 * Se sono presenti ritorna cognome spazio nome.
	 * Altrimenti ritorna cognome o nome. 
	 * Altrimenti ritorna il trattino.
	 * @return 	la giusta descrizione dipendentemente dalle informazioni presenti per l'utente che ha aperto l'rds.
	 * 
	 */
	public String getDescrizioneUtenteAperturaRds() {
		List<String> descrizioneList = new ArrayList<>(2);
		descrizioneList.add(cognomeUtenteAperturaRds);
		descrizioneList.add(nomeUtenteAperturaRds);
		
		descrizioneList = descrizioneList.stream()
				.filter(s -> StringUtils.isNotEmpty(s))
				.collect(Collectors.toList());
		if (descrizioneList.isEmpty()) {
			return " - ";
		}
		return String.join(" ", descrizioneList);
	}

	/**
	 * Restituisce il nodo apertura RDS.
	 * @return nodo apertura RDS
	 */
	public String getNodoAperturaRds() {
		return nodoAperturaRds;
	}

	/**
	 * Imposta il nodo apertura RDS.
	 * @param nodoAperturaRds
	 */
	public void setNodoAperturaRds(final String nodoAperturaRds) {
		this.nodoAperturaRds = nodoAperturaRds;
	}

	/**
	 * Restituisce il cognome dell'utente che apre Rds.
	 * @return cognome utente che apre Rds
	 */
	public String getCognomeUtenteAperturaRds() {
		return cognomeUtenteAperturaRds;
	}

	/**
	 * Imposta il cognome dell'utente che apre Rds.
	 * @param cognomeUtenteAperturaRds
	 */
	public void setCognomeUtenteAperturaRds(final String cognomeUtenteAperturaRds) {
		this.cognomeUtenteAperturaRds = cognomeUtenteAperturaRds;
	}

	/**
	 * Restituisce il nome dell'utente che apre Rds.
	 * @return nome utente che apre Rds
	 */
	public String getNomeUtenteAperturaRds() {
		return nomeUtenteAperturaRds;
	}

	/**
	 * Imposta il cognome dell'utente che apre Rds.
	 * @param nomeUtenteAperturaRds
	 */
	public void setNomeUtenteAperturaRds(final String nomeUtenteAperturaRds) {
		this.nomeUtenteAperturaRds = nomeUtenteAperturaRds;
	}

	/**
	 * Restituisce la descrizione della tipologia.
	 * @return descrizione
	 */
	public String getDescrizioneTipologia() {
		return descrizioneTipologia;
	}

	/**
	 * Imposta la descrizione della tipologia.
	 * @param descrizioneTipologia
	 */
	public void setDescrizioneTipologia(final String descrizioneTipologia) {
		this.descrizioneTipologia = descrizioneTipologia;
	}

	/**
	 * Restituisce la descrizione del gruppo.
	 * @return descrizione gruppo
	 */
	public String getDescrizioneGruppo() {
		return descrizioneGruppo;
	}

	/**
	 * Imposta la descrizione del gruppo.
	 * @param descrizioneGruppo
	 */
	public void setDescrizioneGruppo(final String descrizioneGruppo) {
		this.descrizioneGruppo = descrizioneGruppo;
	}
	
}
