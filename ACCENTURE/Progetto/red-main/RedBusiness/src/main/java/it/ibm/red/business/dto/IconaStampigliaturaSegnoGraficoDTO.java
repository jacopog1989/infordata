package it.ibm.red.business.dto;

/**
 * 
 * @author Vingenito
 *
 */
public class IconaStampigliaturaSegnoGraficoDTO extends AbstractDTO {

	private static final long serialVersionUID = 4851470071796018018L;


    /**
     * Awesome icon.
     */
	private String fontAwesomeIcon;
	
    /**
     * Descrizione icona.
     */
	private String descrIcona;
	
	/**
	 * Recupera la descrizione dell'icona.
	 * @return descrizione dell'icona
	 */
	public String getDescrIcona() {
		return descrIcona;
	}
	
	/**
	 * Imposta la descrizione dell'icona.
	 * @param descrIcona descrizione dell'icona
	 */
	public void setDescrIcona(final String descrIcona) {
		this.descrIcona = descrIcona;
	}
	
	/**
	 * Recupera l'attributo fontAwesomeIcon.
	 * @return fontAwesomeIcon
	 */
	public String getFontAwesomeIcon() {
		return fontAwesomeIcon;
	}	
	
	/**
	 * Imposta l'attributo fontAwesomeIcon.
	 * @param fontAwesomeIcon
	 */
	public void setFontAwesomeIcon(final String fontAwesomeIcon) {
		this.fontAwesomeIcon = fontAwesomeIcon;
	}
	
}
