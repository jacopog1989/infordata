package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

import fepa.types.v3.DocumentoFascicoloType;
import fepa.types.v3.FascicoloBaseType;

/**
 * DTO per la gestione delle informazioni di un fascicolo FEPA.
 */
public class FascicoloFEPAInfoDTO implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -6277752797304608747L;

	/**
	 * Lista documenti.
	 */
	private List<DocumentoFascicoloFepaDTO> listaDocumenti;

	/**
	 * Fascicolo principale.
	 */
	private FascicoloFEPADTO fascicoloPrincipale;

	/**
	 * Fascicoli contenuti.
	 */
	private List<FascicoloFEPADTO> fascicoliContenuti;

	/**
	 * Restituisce i fascicoli contenuti.
	 * @return fascicoli contenuti
	 */
	public List<FascicoloFEPADTO> getFascicoliContenuti() {
		return fascicoliContenuti;
	}

	/**
	 * Restituisce il fascicolo principale.
	 * @return fascicolo principale
	 */
	public FascicoloFEPADTO getFascicoloPrincipale() {
		return fascicoloPrincipale;
	}

	/**
	 * Restituisce la lista documenti.
	 * @return lista documenti
	 */
	public List<DocumentoFascicoloFepaDTO> getListaDocumenti() {
		return listaDocumenti;
	}

	/**
	 * Popola il documento FEPA con le informazioni dei documenti.
	 * @param idFascicolo
	 * @param tipoFascicoloFepaValue
	 * @param documenti
	 * @param idFascicoloRiferimento
	 */
	public void fillDocumenti(final String idFascicolo, final String tipoFascicoloFepaValue, final List<DocumentoFascicoloType> documenti, final String idFascicoloRiferimento) {
		listaDocumenti = new ArrayList<>();
		if (documenti != null && !documenti.isEmpty()) {
			for (int i = 0; i < documenti.size(); i++) {
				final DocumentoFascicoloType documento = documenti.get(i);
				final DocumentoFascicoloFepaDTO documentoFepa = new DocumentoFascicoloFepaDTO();
				documentoFepa.setGuid(documento.getIdDocumento());
				documentoFepa.setIdCategoriaDocumento(1);
				documentoFepa.setTipologiaDocumento(documento.getTipoDocumento().getDescrizione());
				documentoFepa.setDescrizione(documento.getDescrizione());
				documentoFepa.setTipoFascicolo(tipoFascicoloFepaValue);
				
				if (documento.getDataCreazione() != null) {
					documentoFepa.setDataCreazione(documento.getDataCreazione().toGregorianCalendar().getTime());
				}
				
				if (documento.getMetadatiDocumento() != null && !CollectionUtils.isEmpty(documento.getMetadatiDocumento().getProtocollo())) {
					documentoFepa.setNumeroProtocollo(documento.getMetadatiDocumento().getProtocollo().get(0).getNumeroProtocollo());
					documentoFepa.setAooProtocollo(documento.getMetadatiDocumento().getProtocollo().get(0).getAoo());
					documentoFepa.setDataProtocollo(documento.getMetadatiDocumento().getProtocollo().get(0).getData().toGregorianCalendar().getTime());
						
				}
				
				documentoFepa.setFileName(documento.getFileName());
				documentoFepa.setIdFascicolo(idFascicolo);
				documentoFepa.setIdFascicoloRiferimento(idFascicoloRiferimento);
				
				listaDocumenti.add(documentoFepa);
			}
		}
	}

	/**
	 * Popola il {@link #fascicoloPrincipale} con le informazioni passate come parametri.
	 * @param fascicolo
	 */
	public void fillFascicoloPrincipale(final FascicoloBaseType fascicolo) {
		fascicoloPrincipale = new FascicoloFEPADTO(fascicolo);		
	}

	/**
	 * Aggiunge il fascicolo ai {@link #fascicoliContenuti}.
	 * @param fascicoloContenuto
	 * @param idFascicoloRiferimento
	 */
	public void addFascicoloContenuto(final FascicoloBaseType fascicoloContenuto, final String idFascicoloRiferimento) {
		if (fascicoliContenuti == null) {
			fascicoliContenuti = new ArrayList<>();
		}
		final FascicoloFEPADTO fascicoloFEPADTO = new FascicoloFEPADTO(fascicoloContenuto);
		fascicoloFEPADTO.setIdFascicoloRiferimento(idFascicoloRiferimento);
		fascicoliContenuti.add(fascicoloFEPADTO);
	}

}
