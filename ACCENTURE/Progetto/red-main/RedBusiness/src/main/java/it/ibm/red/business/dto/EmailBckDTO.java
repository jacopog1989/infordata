package it.ibm.red.business.dto;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;

/**
 * @author a.dilegge
 *
 */
public class EmailBckDTO implements Serializable {


	private static final long serialVersionUID = 225127846262539039L;
	
	/**
	 * Identiifcativo.
	 */
	private String messageId;
	
	/**
	 * Oggetto.
	 */
	private String oggetto;
	
	/**
	 * Mittente.
	 */
	private String mittente;
	
	/**
	 * Casella.
	 */
	private String casella;
	
	/**
	 * EMail.
	 */
	private transient InputStream email;
	
	/**
	 * Data invio.
	 */
	private Date dataInvio;
	
	/**
	 * Data scarico.
	 */
	private Date dataScarico;
	
	/**
	 * Flag notifica.
	 */
	private int isNotifica;
	
	/**
	 * Flag pec.
	 */
	private int isPec;
	
	/**
	 * Classe mail (message id).
	 */
	private String messageIdClasseEmail;
	
	/**
	 * Recupera l'id del messaggio.
	 * @return id del messaggio
	 */
	public String getMessageId() {
		return messageId;
	}
	
	/**
	 * Imposta l'id del messaggio.
	 * 
	 * @param messageId id del messaggio
	 */
	public void setMessageId(final String messageId) {
		this.messageId = messageId;
	}
	
	/**
	 * Recupera l'oggetto della mail.
	 * @return oggetto della mail
	 */
	public String getOggetto() {
		return oggetto;
	}
	
	/**
	 * Imposta l'oggetto della mail.
	 * @param oggetto oggetto della mail
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}
	
	/**
	 * Recupera l'attributo isEmail.
	 * @return email
	 */
	public InputStream getISEmail() {
		return email;
	}
	
	/**
	 * Imposta l'attributo isEmail.
	 * @param email email
	 */
	public void setISEmail(final InputStream email) {
		this.email = email;
	}

	/**
	 * Recupera la data di invio della mail.
	 * @return data di invio della mail
	 */
	public Date getDataInvio() {
		return dataInvio;
	}
	
	/**
	 * Imposta la data di invio della mail.
	 * @param dataInvio data di invio della mail
	 */
	public void setDataInvio(final Date dataInvio) {
		this.dataInvio = dataInvio;
	}
	
	/**
	 * Recupera la data di scarico della mail.
	 * @return data di scarico della mail
	 */
	public Date getDataScarico() {
		return dataScarico;
	}
	
	/**
	 * Imposta la data di scarico della mail.
	 * @param dataScarico data di scarico della mail
	 */
	public void setDataScarico(final Date dataScarico) {
		this.dataScarico = dataScarico;
	}
	
	/**
	 * Recupera il mittente della mail.
	 * @return mittente della mail
	 */
	public String getMittente() {
		return mittente;
	}
	
	/**
	 * Imposta il mittente della mail.
	 * @param mittente mittente della mail
	 */
	public void setMittente(final String mittente) {
		this.mittente = mittente;
	}
	
	/**
	 * Recupera l'attributo isNotifica.
	 * @return isNotifica
	 */
	public int getIsNotifica() {
		return isNotifica;
	}
	
	/**
	 * Imposta l'attributo isNotifica.
	 * 
	 * @param isNotifica
	 */
	public void setIsNotifica(final int isNotifica) {
		this.isNotifica = isNotifica;
	}
	
	/**
	 * Recupera la casella email.
	 * @return casella email
	 */
	public String getCasella() {
		return casella;
	}
	
	/**
	 * Imposta la casella email.
	 * 
	 * @param casella casella email
	 */
	public void setCasella(final String casella) {
		this.casella = casella;
	}
	
	/**
	 * Recupera l'attributo isPec.
	 * @return isPec
	 */
	public int getIsPec() {
		return isPec;
	}
	
	/**
	 * Imposta l'attributo isPec.
	 * @param isPec
	 */
	public void setIsPec(final int isPec) {
		this.isPec = isPec;
	}
	
	/**
	 * Recupera l'id del messaggio della classe email.
	 * @return id del messaggio della classe email
	 */
	public String getMessageIdClasseEmail() {
		return messageIdClasseEmail;
	}
	
	/**
	 * Imposta l'id del messaggio della classe email.
	 * @param messageIdClasseEmail id del messaggio della classe email
	 */
	public void setMessageIdClasseEmail(final String messageIdClasseEmail) {
		this.messageIdClasseEmail = messageIdClasseEmail;
	}
	
}
