/**
 * 
 */
package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;

import it.ibm.red.business.monitoring.attodecreto.AttoDecretoMonitoring;

/**
 * @author a.dilegge
 *
 */
public interface IAttoDecretoMonitoringDAO extends Serializable {

	/**
	 * Restituisce il prossimo id dalla sequence associata alla tabella
	 * 
	 * @param con
	 * @return Next Id recuperato.
	 * @throws DAOException
	 */
	int getNextId(Connection con);

	/**
	 * Inserisce le informazioni di monitoring
	 * 
	 * @param monitoringInfo
	 * @param con
	 * @throws DAOException
	 */
	void inserisciOperation(AttoDecretoMonitoring monitoringInfo, Connection con);

	/**
	 * Aggiorna informazioni di monitoring
	 * 
	 * @param monitoringInfo
	 * @param connection
	 */
	void aggiornaOperation(AttoDecretoMonitoring monitoringInfo, Connection connection);

}
