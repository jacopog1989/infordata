package it.ibm.red.business.exception;

/**
 * The Class RedException.
 *
 * @author CPIERASC
 * 
 *         Eccezione generica dell'applicativo.
 */
public class ProtocolloException extends RuntimeException {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Costruttore.
	 * 
	 * @param e	eccezione
	 */
	public ProtocolloException(final Exception e) {
		super(e);
	}

	/**
	 * Costruttore.
	 * 
	 * @param msg	messaggio
	 */
	public ProtocolloException(final String msg) {
		super(msg);
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param msg	messaggio
	 * @param e		eccezione root
	 */
	public ProtocolloException(final String msg, final Exception e) {
		super(msg, e);
	}
	
}
