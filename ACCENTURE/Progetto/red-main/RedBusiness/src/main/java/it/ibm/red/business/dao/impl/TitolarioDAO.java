package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ITitolarioDAO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * DAO per la gestione dei titolari.
 */
@Repository
public class TitolarioDAO extends AbstractDAO implements ITitolarioDAO {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = -8023831637039601609L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TitolarioDAO.class);
	
	/**
	 * Clausula Order by sull'indice di classificazione.
	 */
	private static final String ORDER_BY_INDICECLASSIFICAZIONE = " ORDER BY t.indiceclassificazione";

	/**
	 * Restituisce i titolari figli.
	 */
	@Override
	public List<TitolarioDTO> getFigliRadice(final Long idAOO, final Long idNodo, final Connection connection) {
		PreparedStatement ps = null;
		int i = 1;

		try {
			String querySQL = "SELECT t.*, "
					+ "       (SELECT COUNT(*) AS NUMCHILD " 
					+ "          FROM titolario t1 "
					+ "         WHERE t1.indiceclassificazionepadre = t.indiceclassificazione "
					+ "           AND t1.idaoo = t.idaoo) as NUMCHILD " 
					+ "  FROM titolario t, titolarionodo tn "
					+ " WHERE t.indiceclassificazione = tn.indiceclassificazione " 
					+ "   AND t.idaoo = ? "
					+ "   AND tn.idnodo = ? " 
					+ " AND t.dataDisattivazione IS NULL "
					+ ORDER_BY_INDICECLASSIFICAZIONE;
			ps = connection.prepareStatement(querySQL);
			ps.setLong(i++, idAOO);
			ps.setLong(i++, idNodo);
			
			return getLista(ps, true);

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeStatement(ps);
		}

	}
	
	/**
	 * Restituisce la lista dei titolari a partire dello statement in input.
	 * @param ps
	 * @param numChild
	 * @return
	 */
	private static List<TitolarioDTO> getLista(final PreparedStatement ps, final boolean numChild){
		ResultSet rs = null;
		
		try {
		
			rs = ps.executeQuery();
	
			final List<TitolarioDTO> list = new ArrayList<>();
			while (rs.next()) {
				list.add(new TitolarioDTO(rs.getString("indiceClassificazione"),
						rs.getString("indiceClassificazionePadre"), rs.getString("descrizione"), rs.getInt("livello"),
						rs.getLong("idAOO"), rs.getDate("dataAttivazione"), rs.getDate("dataDisattivazione"),
						numChild ? rs.getInt("NUMCHILD") : 0));
			}
			
			return list;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeResultset(rs);
		}
		
	}

	/**
	 * @see it.ibm.red.business.dao.ITitolarioDAO#getFigliRadiceNonDisattivati(java.lang.Long,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<TitolarioDTO> getFigliRadiceNonDisattivati(final Long idAOO, final Long idNodo, final Connection connection) {
		PreparedStatement ps = null;
		int i = 1;

		try {
			String querySQL = "SELECT t.*, "
					+ "       (SELECT COUNT(*) AS NUMCHILD " 
					+ "          FROM titolario t1 "
					+ "         WHERE t1.indiceclassificazionepadre = t.indiceclassificazione "
					+ "           AND t1.idaoo = t.idaoo) as NUMCHILD " 
					+ "  FROM titolario t, titolarionodo tn "
					+ " WHERE t.indiceclassificazione = tn.indiceclassificazione " 
					+ "   AND t.idaoo = ? "
					+ "   AND tn.idnodo = ? " 
					+ ORDER_BY_INDICECLASSIFICAZIONE;
			ps = connection.prepareStatement(querySQL);
			ps.setLong(i++, idAOO);
			ps.setLong(i++, idNodo);
			
			return getLista(ps, true);

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeStatement(ps);
		}

	}

	/**
	 * @see it.ibm.red.business.dao.ITitolarioDAO#getFigliByIndice(java.lang.Long,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public List<TitolarioDTO> getFigliByIndiceNonDisattivati(final Long idAOO, final String indice, final Connection connection) {

		return getFigliByIndice(idAOO, indice, true, connection);
	}

	/**
	 * @see it.ibm.red.business.dao.ITitolarioDAO#getFigliByIndiceNonDisattivati(java.lang.Long,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public List<TitolarioDTO> getFigliByIndice(final Long idAOO, final String indice, final Connection connection) {
		
		return getFigliByIndice(idAOO, indice, false, connection);
	}

	/**
	 * Restutisce i figli dell'organigramma del titolario identificato dall'indice
	 * di classificazione {@code indice}.
	 * 
	 * @param idAOO
	 *            Identificativo dell'area organizzativa.
	 * @param indice
	 *            Indice del titolario dal quale recuperare i figli.
	 * @param onlyNonDisattivati
	 *            Se {@code true} restituisce solo i figli non disattivati.
	 * @param connection
	 *            Connessione al database.
	 * @return Figli del titolario.
	 */
	private static List<TitolarioDTO> getFigliByIndice(final Long idAOO, final String indice, final boolean onlyNonDisattivati, final Connection connection) {
		
		PreparedStatement ps = null;
		int i = 1;
		
		try {
			StringBuilder querySQL = new StringBuilder(" SELECT t.*, (SELECT COUNT(*) FROM titolario t1 ")
					.append("  WHERE t1.indiceclassificazionepadre = t.indiceclassificazione ")
					.append("  AND t1.idaoo = t.idaoo) as NUMCHILD FROM titolario t WHERE t.idaoo = ? ")
					.append("  AND t.indiceclassificazionepadre = ? ");
			
			if (onlyNonDisattivati) {
				querySQL.append(" AND t.dataDisattivazione IS NULL ");
			}
			
			querySQL.append(ORDER_BY_INDICECLASSIFICAZIONE);
			
			ps = connection.prepareStatement(querySQL.toString());
			ps.setLong(i++, idAOO);
			ps.setString(i++, indice);
			
			return getLista(ps, true);
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeStatement(ps);
		}
	}
	
	/**
	 * @see it.ibm.red.business.dao.ITitolarioDAO#getNodoByIndice(java.lang.Long,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public TitolarioDTO getNodoByIndice(final Long idAOO, final String indice, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		TitolarioDTO item = null;
		int i = 1;
		
		try {
			String querySQL = "SELECT * FROM titolario t WHERE t.idaoo = ? AND t.indiceclassificazione = ?";
			ps = connection.prepareStatement(querySQL);
			ps.setLong(i++, idAOO);
			ps.setString(i++, indice);
			rs = ps.executeQuery();

			if (rs.next()) {
				item = new TitolarioDTO(rs.getString("indiceClassificazione"),
						rs.getString("indiceClassificazionePadre"), rs.getString("descrizione"), rs.getInt("livello"),
						rs.getLong("idAOO"), rs.getDate("dataAttivazione"), rs.getDate("dataDisattivazione"), 0);
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeStatement(ps, rs);
		}

		return item;
	}

	/**
	 * @see it.ibm.red.business.dao.ITitolarioDAO#getFigliNodoTitolarioAutocomplete(java.lang.Long,
	 *      java.lang.String, java.lang.String, java.sql.Connection).
	 */
	@Override
	public List<TitolarioDTO> getFigliNodoTitolarioAutocomplete(final Long idAOO, final String indicePartenza, final String parola, 
			final Connection connection) {
		PreparedStatement ps = null;
		int i = 1;

		try {
			String sanitizeParolaLowerCase = sanitize("%" + parola.toLowerCase() + "%");
			
			String querySQL = "SELECT DISTINCT * FROM titolario t WHERE t.idaoo = ? AND t.datadisattivazione IS NULL "
					+ "AND ("
					+ "LOWER(t.indiceclassificazione) LIKE " + sanitizeParolaLowerCase + " OR LOWER(t.descrizione) LIKE " + sanitizeParolaLowerCase
					+ ") CONNECT BY PRIOR t.indiceclassificazione = t.indiceclassificazionepadre " 
					+ "START WITH t.indiceclassificazione = ?";
			
			ps = connection.prepareStatement(querySQL);
			ps.setLong(i++, idAOO);
			ps.setString(i++, indicePartenza);
			
			return getLista(ps, false);
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeStatement(ps);
		}

	}

}
