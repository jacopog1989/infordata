package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.AnnotationSet;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Annotation;
import com.filenet.api.core.Document;
import com.filenet.apiimpl.core.SubSetImpl;

import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.IIterApprovativoDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO;
import it.ibm.red.business.dto.DetailAssegnazioneDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.ModificaIterCEDTO;
import it.ibm.red.business.dto.ModificaIterPEDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.TrasformPE;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IModificaProcedimentoSRV;
import it.ibm.red.business.service.ISecurityCambioIterSRV;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.service.ITipoProcedimentoSRV;
import it.ibm.red.business.service.ITrasformazionePDFSRV;
import it.ibm.red.business.utils.OrganigrammaRetrieverUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Servizio per la modifica di un iter.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
@Service
public class ModificaProcedimentoSRV extends AbstractService implements IModificaProcedimentoSRV {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 7041062484086004044L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ModificaProcedimentoSRV.class.getName());

	/**
	 * Dao per la gestione dei tipi procedimento.
	 */
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;

	/**
	 * Dao per la gestione dei nodi.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Dao per la gestione delle aoo.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * Servizio per la gestione delle security.
	 */
	@Autowired
	private ISecuritySRV securitySRV;

	/**
	 * Servizio per la gestione delle security.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * Servizio per la gestione delle security specifiche per il cambio dell'iter.
	 */
	@Autowired
	private ISecurityCambioIterSRV securityCambioIterSRV;

	/**
	 * Servizio per la trasformazione pdf.
	 */
	@Autowired
	private ITrasformazionePDFSRV trasfPdfSRV;

	/**
	 * Servizio per il tipo procedimento.
	 */
	@Autowired
	private ITipoProcedimentoSRV tipoProcedimentoSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IIterApprovativoDAO iterApprovativoDAO;

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	@Override
	public final EsitoOperazioneDTO gestioneCambioIterManuale(final TipoAssegnazioneEnum tipoAssegnazione, final Boolean bFlagUrgente, final UtenteDTO utente,
			final String wobNumber, final Long idNodoDestinatarioNew, final Long idUtenteDestinatarioNew) {
		EsitoOperazioneDTO esito = null;
		Connection connection = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		ModificaIterCEDTO modIterCEDTO = null;

		try {

			LOGGER.info("START MODIFICA ITER MANUALE SRV UTENTE " + utente.getUsername());
			connection = setupConnection(getDataSource().getConnection(), true);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final VWWorkObject wf = fpeh.getWorkFlowByWob(wobNumber, true);
			final ModificaIterPEDTO modIterPEDTO = TrasformPE.transform(wf, TrasformerPEEnum.MODIFICA_ITER);

			final Document doc = fceh.getDocumentForModificaIter(modIterPEDTO.getIdDocumento());
			modIterCEDTO = TrasformCE.transform(doc, TrasformerCEEnum.MODIFICA_ITER);

//			<-- NOTA BENE: nel caso del cambio iter Manuale non è necessario verificare se c'è un destinatario cartaceo perche non esiste il flag Firma digitale RGS -->
			String flagUrgente = "0";
			if (bFlagUrgente != null && bFlagUrgente) {
				flagUrgente = "1";
			}
			
			final HashMap<String, Object> metadati = getMetadatiModificaIterManuale(utente, modIterCEDTO, modIterPEDTO, flagUrgente, tipoAssegnazione, 
					idUtenteDestinatarioNew, idNodoDestinatarioNew, doc, fceh, connection);
			
			LOGGER.info("START AVVIA NUOVO ITER MANUALE SRV UTENTE idUtenteDestinatarioNew "+ idUtenteDestinatarioNew);
			LOGGER.info("START AVVIA NUOVO ITER MANUALE SRV UTENTE idNodoDestinatarioNew "+ idNodoDestinatarioNew);
			// Avvio la procedura per avviare un nuovo wf
			avviaNuovoIterManuale(utente, modIterCEDTO, modIterPEDTO, metadati, flagUrgente, fceh, fpeh, connection);

			LOGGER.info("END AVVIA NUOVO ITER MANUALE SRV");

			// Cancellazione del vecchio wf
			LOGGER.info("Eliminazione workflow [" + wobNumber + "]");
			fpeh.deleteWF(wf);

			LOGGER.info("WF Eliminato");

			commitConnection(connection);
			esito = new EsitoOperazioneDTO(null, null, modIterCEDTO.getNumDoc(), wobNumber);
			esito.setNote("Operazione effettuata con successo.");
		} catch (final Exception e) {
			esito = manageError(e, wobNumber, modIterCEDTO, connection);
			LOGGER.warn("ERRORE AVVIA NUOVO ITER MANUALE SRV");
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
			LOGGER.info("END AVVIA NUOVO ITER MANUALE SRV");
		}

		return esito;
	}

	@Override
	public final void avviaNuovoIterManuale(final UtenteDTO utente, final ModificaIterCEDTO modIterCEDTO, final ModificaIterPEDTO modIterPEDTO,
			final Map<String, Object> metadati, final String flagUrgente, final IFilenetCEHelper fceh, final FilenetPEHelper fpeh, final Connection connection) {

		final Aoo aoo = aooDAO.getAoo(utente.getIdAoo(), connection);
		final String wfName = tipoProcedimentoDAO.getWorkflowNameByIdTipoProcedimento(modIterCEDTO.getIdTipoProcedimento(), connection);

		fpeh.avviaWF(wfName, metadati, modIterPEDTO.getIdDocumento(), aoo.getAooFilenet().getIdClientAoo(), false);

		// Recupero di nuovo il documento per non andare incontro all'eccezione di
		// FileNet (FNRCE0050E)
		// che non permette il salvataggio di un Document che è stato gia' modificato
		// (vedi securities)
		final Document newDoc = fceh.getDocumentForModificaIter(modIterPEDTO.getIdDocumento());

		final HashMap<String, Object> metadatiCE = new HashMap<>();
		metadatiCE.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY), Integer.parseInt(flagUrgente));
		// Red non lo fa, rimane l'iter con cui è stato creato il documento, ma non è
		// l'iter attuale del documento
		metadatiCE.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ITER_APPROVATIVO_METAKEY), IterApprovativoDTO.ITER_FIRMA_MANUALE.toString());
		fceh.updateMetadati(newDoc, metadatiCE);

	}

	@Override
	public final HashMap<String, Object> getMetadatiModificaIterManuale(final UtenteDTO utente, final ModificaIterCEDTO modIterCEDTO, final ModificaIterPEDTO modIterPEDTO,
			final String flagUrgente, final TipoAssegnazioneEnum tipoAssegnazione, final Long idUtenteDestinatarioNew, final Long idNodoDestinatarioNew, final Document doc,
			final IFilenetCEHelper fceh, final Connection connection) {
		final HashMap<String, Object> metadati = new HashMap<>();

		// Raccolta metadati necessari per avviare il WF
		metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY), modIterPEDTO.getIdDocumento());
		metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), utente.getIdUfficio().intValue());
		metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId().intValue());
		metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY), modIterCEDTO.getRegistroRiservato());
		metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY), modIterPEDTO.getIdFascicolo());
		metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY), modIterCEDTO.getDataScadenza());
		metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ELENCO_CONOSCENZA_STORICO_METAKEY), modIterPEDTO.getConoscenze());
		metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ELENCO_CONTRIBUTI_STORICO_METAKEY), modIterPEDTO.getContributi());
		metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ELENCO_DESTINATARI_INTERNI_METAKEY), modIterPEDTO.getDestinatariInterni());

		// controllo se il check 'Urgente' è stato selezionato
		metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY), flagUrgente);

		// Viene verificata l'effettiva presenza del metadato e del valore associato su
		// CE.
		// Se ha ragione di esistere su CE viene preso in considerazione nella lista di
		// metadati utilizzati per avviare il nuovo WF altrimenti NO!.
		if (!StringUtils.isNullOrEmpty(modIterCEDTO.getDestinatariContributoEsterno())) {
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.CONTRIBUTO_ESTERNO), 1);
		}

		Long idRagGenDelloStato = null;
		try {
			idRagGenDelloStato = utenteDAO.getIdRagioniereGeneraleDelloStato(connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dell'ID del Ragioniere dello stato", e);
		}

//		<-- Se verificata la condizione il 'Flag Firma Digitale RGS' è sempre false (Cambio iter Manuale)  -->
		if ((TipoAssegnazioneEnum.FIRMA.equals(tipoAssegnazione) || TipoAssegnazioneEnum.FIRMA_MULTIPLA.equals(tipoAssegnazione))
				&& idUtenteDestinatarioNew.equals(idRagGenDelloStato)) {
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FIRMA_DIG_RGS_WF_METAKEY), "0");
		}

		boolean delegato = true;
		if (Boolean.TRUE.equals(modIterCEDTO.getIsRiservato())) {
			delegato = false;
		}

		final String[] assegnazioneNew = new String[1];
		assegnazioneNew[0] = idNodoDestinatarioNew + "," + idUtenteDestinatarioNew;

		// inizio della logica esclusiva per "l'Iter Manuale"
		if (TipoAssegnazioneEnum.FIRMA.equals(tipoAssegnazione) || TipoAssegnazioneEnum.FIRMA_MULTIPLA.equals(tipoAssegnazione)
				|| TipoAssegnazioneEnum.SIGLA.equals(tipoAssegnazione) || TipoAssegnazioneEnum.VISTO.equals(tipoAssegnazione)
				|| TipoAssegnazioneEnum.COPIA_CONFORME.equals(tipoAssegnazione)) {

			aggiornaSecurityPerCambioIter(utente, assegnazioneNew, modIterCEDTO, modIterPEDTO, delegato, fceh, connection);

			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ELENCO_LIBROFIRMA_METAKEY), assegnazioneNew);

			if (TipoAssegnazioneEnum.COPIA_CONFORME.equals(tipoAssegnazione)) {
				final Boolean flagFirmaCopiaConforme = !idNodoDestinatarioNew.equals(0L);
				metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FIRMA_COPIA_CONFORME_METAKEY), flagFirmaCopiaConforme);
				metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UFFICIO_COPIA_CONFORME_METAKEY), idNodoDestinatarioNew);
				metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_COPIA_CONFORME_METAKEY), idUtenteDestinatarioNew);
			}

		} else {
			// Se si è selezionato qualsiasi altro tipo di assegnazione diversa da quelle
			// sopra
			LOGGER.info("Avvio Workflow di un documento Standard generico.");
			LOGGER.info("Adeguamento e modifica delle security del Documento: [ " + modIterPEDTO.getIdDocumento() + " ]");

			aggiornaSecurityPerCambioIter(utente, assegnazioneNew, modIterCEDTO, modIterPEDTO, delegato, fceh, connection);

			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), idNodoDestinatarioNew);
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), idUtenteDestinatarioNew);
		}

		if (!modIterCEDTO.getIdCategoriaDocumento().equals(CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0])) {
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), tipoAssegnazione.getId());
		} else {
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_FORMATO_DOCUMENTO), modIterCEDTO.getIdFormatoDocumento());
		}

		avviaTrasformazionePDFIterManuale(utente, modIterCEDTO, modIterPEDTO, doc, assegnazioneNew, tipoAssegnazione, fceh, connection);

		return metadati;
	}

	private void avviaTrasformazionePDFIterManuale(final UtenteDTO utente, final ModificaIterCEDTO modIterCEDTO, final ModificaIterPEDTO modIterPEDTO, final Document doc,
			final String[] elencoLibroFirma, final TipoAssegnazioneEnum tipoAssegnazione, final IFilenetCEHelper fceh, final Connection connection) {
		if (!FilenetCEHelper.hasDocumentContentTransfer(doc)) {
			// Verifica che il documento abbia il content, se false mostra un messaggio
			// all'utente
			throw new RedException("Il documento selezionato deve avere associato un content.");
		} else {
			// ### TRASFORMAZIONE PDF -> START
			// Si avvia la trasformazione PDF asincrona solo nel caso in cui il nuovo
			// procedimento vada verso un iter manuale per firma/sigla/visto/copia conforme
			trasfPdfSRV.avviaTrasformazionePDFPerNuovaAssegnazioneFirmaSiglaVisto(String.valueOf(modIterPEDTO.getIdDocumento()), modIterCEDTO.getGuid(),
					tipoAssegnazione.getId(), utente, elencoLibroFirma, IterApprovativoDTO.ITER_FIRMA_MANUALE, this.getClass().getSimpleName(), fceh, connection, true);
		}
	}

	/**
	 * @param utente
	 * @param idNodoDestinatarioNew
	 * @param idUtenteDestinatarioNew
	 * @param elencoLibroFirma
	 * @param modIterCEDTO
	 * @param connection
	 * @param fceh
	 * @param modIterPEDTO
	 * @param delegato
	 */
	private void aggiornaSecurityPerCambioIter(final UtenteDTO utente, final String[] elencoLibroFirma, final ModificaIterCEDTO modIterCEDTO,
			final ModificaIterPEDTO modIterPEDTO, final boolean delegato, final IFilenetCEHelper fceh, final Connection connection) {
		// Aggiorno le securities
		// Nuove securities per la riassegnazione
		final ListSecurityDTO securitiesPerRiassegnazione = securitySRV.getSecurityPerRiassegnazione(utente, modIterPEDTO.getIdDocumento().toString(),
				elencoLibroFirma, null, delegato, true, modIterCEDTO.getIsRiservato(), false, connection);

		try {
			// Aggiorno le securities del documento con quelle nuove
			securitySRV.aggiornaEVerificaSecurityDocumento(modIterPEDTO.getIdDocumento().toString(), utente.getIdAoo(), securitiesPerRiassegnazione, true, false, fceh,
					connection);
		} catch (final Exception e) {
			throw new RedException("Errore durante l'aggiornamento delle security per il documento con id: " + modIterPEDTO.getIdDocumento(), e);
		}

		// Se l'update va a buon fine aggiorno di conseguenza anche le securities dei
		// documenti allacciati e dei contributi inseriti
		// Aggiorno le Security per tutti i documenti allacciati...
		securitySRV.aggiornaSecurityDocumentiAllacciati(utente, Integer.parseInt(modIterPEDTO.getIdDocumento().toString()), securitiesPerRiassegnazione, elencoLibroFirma,
				fceh, connection);
		// ...e di conseguenza anche di tutti i contributi Interni
		securitySRV.aggiornaSecurityContributiInseriti(utente, modIterPEDTO.getIdDocumento().toString(), elencoLibroFirma, fceh, connection);

		try {
			// Modifica delle security del FASCICOLO e per la RIASSEGNAZIONE
			final ListSecurityDTO securitiesModificaFascicolo = securitySRV.getSecurityPerRiassegnazioneFascicolo(utente,
					modIterPEDTO.getIdDocumento().toString(), elencoLibroFirma, null, delegato, connection);
			// Aggiorno le securities del fascicolo con quelle nuove
			securitySRV.modificaSecurityFascicoli(utente, securitiesModificaFascicolo, modIterPEDTO.getIdDocumento().toString());
		} catch (final Exception e) {
			throw new RedException("Errore durante l'aggiornamento delle security per il documento con id: " + modIterPEDTO.getIdDocumento(), e);
		}
	}

	/**
	 * Operazione per il cambio iter automatico.
	 *
	 * @param iterSelezionato      the iter selezionato
	 * @param flagCheckFirmaDigRGS the flag check firma dig RGS
	 * @param bFlagUrgente         the b flag urgente
	 * @param idUtenteCoordinatore the id utente coordinatore
	 * @param utente               the utente
	 * @param wobNumber            the wob number
	 * @return the esito operazione DTO
	 */
	@Override
	public final EsitoOperazioneDTO gestioneCambioIterAutomatico(final Integer tipoAssegnazioneSelected, final Integer idIterSelezionato, final Boolean flagCheckFirmaDigRGS,
			final Boolean bFlagUrgente, final Integer idUtenteCoordinatore, final UtenteDTO utente, final String wobNumber) {
		EsitoOperazioneDTO esito = null;
		ModificaIterCEDTO modIterCEDTO = null;
		Connection connection = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			final VWWorkObject wf = fpeh.getWorkFlowByWob(wobNumber, true);

			final ModificaIterPEDTO modIterPEDTO = TrasformPE.transform(wf, TrasformerPEEnum.MODIFICA_ITER);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final Document doc = fceh.getDocumentForModificaIter(modIterPEDTO.getIdDocumento());

			if (!FilenetCEHelper.hasDocumentContentTransfer(doc)) {
				// verifica che il documento abbia il content, se false mostra un messaggio
				// all'utente
				throw new RedException("Il documento selezionato deve avere associato un content.");
			}

			modIterCEDTO = TrasformCE.transform(doc, TrasformerCEEnum.MODIFICA_ITER);

			if (flagCheckFirmaDigRGS && destEsternoCartaceo(modIterCEDTO.getDestinatari())) {
//				E' presente almeno un destinatario esterno in una spedizione con mezzo cartaceo.
				throw new RedException(
						"Attenzione, se il documento deve essere firmato digitalmente, il mezzo di sepdizione per tutti i destinatari deve essere elettronico.");
			}

			final HashMap<String, Object> metadati = new HashMap<>();

			// HashMap di metadati utilizzati per avviare il WF
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY), modIterPEDTO.getIdDocumento());
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), utente.getIdUfficio().intValue());
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId().intValue());
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY), modIterCEDTO.getRegistroRiservato());
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY), modIterPEDTO.getIdFascicolo());
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY), modIterCEDTO.getDataScadenza());
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ELENCO_CONOSCENZA_STORICO_METAKEY), modIterPEDTO.getConoscenze());
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ELENCO_CONTRIBUTI_STORICO_METAKEY), modIterPEDTO.getContributi());
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ELENCO_DESTINATARI_INTERNI_METAKEY), modIterPEDTO.getDestinatariInterni());

//			<-- Nuova gestione flag firma digitale RGS-->			
			if (IterApprovativoDTO.ITER_FIRMA_RAGIONIERE.equals(idIterSelezionato)) {
				if (Boolean.TRUE.equals(flagCheckFirmaDigRGS)) {
					metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FIRMA_DIG_RGS_WF_METAKEY), "1");
				} else {
					metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FIRMA_DIG_RGS_WF_METAKEY), "0");
				}
			}

			String flagUrgente = "0";
			if (bFlagUrgente != null && bFlagUrgente) {
				flagUrgente = "1";
			}
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY), flagUrgente);

			// Viene verificata l'effettiva presenza del metadato e del valore associato su
			// CE.
			// Se ha ragione di esistere su CE viene preso in considerazione nella lista di
			// metadati utilizzati per avviare il nuovo WF altrimenti NO!.
			if (!StringUtils.isNullOrEmpty(modIterCEDTO.getDestinatariContributoEsterno())) {
				metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.CONTRIBUTO_ESTERNO), 1);
			}

			Integer idNodoCoordinatore = null;
			if (idUtenteCoordinatore != null) {
				idNodoCoordinatore = nodoDAO.getIdNodoCoordinatore(utente.getIdUfficio(), idUtenteCoordinatore, connection);
				metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_COORDINATORE_METAKEY), idUtenteCoordinatore);
				metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UFFICIO_COORDINATORE_METAKEY), idNodoCoordinatore);
			}

			// Iter automatico, inserire automaticamente il destinatario
			String[] elencoAssegnazioniAutomatiche = null;

			final Long idUtenteDirigenteUfficio = nodoDAO.getNodo(utente.getIdUfficio(), connection).getDirigente().getIdUtente();

			final IterApprovativoDTO iterSelezionato = iterApprovativoDAO.getIterApprovativoById(idIterSelezionato, connection);

			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_TIPO_FIRMA_FN_METAKEY), iterSelezionato.getTipoFirma());

			// Se si tratta di dirigente, aggiungi la security del dirigente, altrimenti
			// oltre alla security del dirigente aggiungere quella dell'ispettore
			if (iterSelezionato.isIterAutomaticoFirmaDirigenti()) {
				// Add Dirigente Ufficio
				elencoAssegnazioniAutomatiche = new String[1];
				elencoAssegnazioniAutomatiche[0] = utente.getIdUfficio() + "," + idUtenteDirigenteUfficio;
			} else {
				elencoAssegnazioniAutomatiche = new String[2];
				elencoAssegnazioniAutomatiche[0] = utente.getIdUfficio() + "," + idUtenteDirigenteUfficio;

				final String ispettore = nodoDAO.getFirmatarioIterApprovativo(utente.getIdUfficio(), iterSelezionato.getTipoFirma(), connection);
				elencoAssegnazioniAutomatiche[1] = ispettore;
			}

			aggiornaSecurity(modIterCEDTO.getIsRiservato(), idNodoCoordinatore, idUtenteCoordinatore, elencoAssegnazioniAutomatiche, connection, utente,
					modIterPEDTO.getIdDocumento() + "");

			if (IterApprovativoDTO.ITER_FIRMA_RAGIONIERE.equals(iterSelezionato.getIdIterApprovativo())) {
				// iter automatico firma ragioniere: modifico l'indirizzo mittente del documento
				// con la casella del coordinamento
				// qualora abbia il tab spedizione...

				final Annotation annotation = getAnnotation(fceh, modIterPEDTO.getIdDocumento(), "MAIL", utente.getIdAoo());
				if (annotation != null) {

					final Document mail = fceh.getDocumentByDTandAOO(modIterPEDTO.getIdDocumento() + ".html", utente.getIdAoo());

					if (mail != null) {
						// modifico l'indirizzo mittente
						mail.getProperties().putValue(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FROM_MAIL_METAKEY),
								PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MAIL_COORDINAMENTO));
						mail.save(RefreshMode.REFRESH);
					}
				}
			}

			// ### TRASFORMAZIONE PDF -> START
			// Si avvia la trasformazione PDF asincrona
			trasfPdfSRV.avviaTrasformazionePDFPerNuovaAssegnazioneFirmaSiglaVisto(String.valueOf(modIterPEDTO.getIdDocumento()), modIterCEDTO.getGuid(),
					tipoAssegnazioneSelected, utente, null, iterSelezionato.getTipoFirma(), this.getClass().getSimpleName(), fceh, connection, true);
			// ### TRASFORMAZIONE PDF -> END

			final Aoo aoo = aooDAO.getAoo(utente.getIdAoo(), connection);
			final String wfName = iterSelezionato.getWorkflowName();
			fpeh.avviaWF(wfName, metadati, modIterPEDTO.getIdDocumento(), aoo.getAooFilenet().getIdClientAoo(), false);

			final Document newDoc = fceh.getDocumentForModificaIter(modIterPEDTO.getIdDocumento());

			final HashMap<String, Object> metadatiCE = new HashMap<>();
			metadatiCE.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY), Integer.parseInt(flagUrgente));
			// Aggiorno il nome dell'iter altrimenti visualizza sempre quello con cui è
			// stato creato
			metadatiCE.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ITER_APPROVATIVO_METAKEY), iterSelezionato.getIdIterApprovativo().toString());

			fceh.updateMetadati(newDoc, metadatiCE);

			LOGGER.info("Eliminazione workflow [" + wobNumber + "]");
			// Elimino il WF precedente
			fpeh.deleteWF(wf);

			commitConnection(connection);
			esito = new EsitoOperazioneDTO(null, null, modIterCEDTO.getNumDoc(), wobNumber);
			esito.setNote("Operazione effettuata con successo.");
		} catch (final Exception e) {
			esito = manageError(e, wobNumber, modIterCEDTO, connection);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}

		return esito;
	}
	
	private static EsitoOperazioneDTO manageError(final Exception e, final String wobNumber, final ModificaIterCEDTO modIterCEDTO, final Connection connection) {
		LOGGER.error("Errore durante il cambio iter", e);
		// Genero l'esito in caso di eccezione
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		if (modIterCEDTO != null) {
			esito.setIdDocumento(modIterCEDTO.getNumDoc());
		}
		esito.setNote("Errore durante il cambio iter: " + e.getMessage() + " \n");
		rollbackConnection(connection);
		return esito;
	}

	@Override
	public final EsitoOperazioneDTO mettiInConoscenza(final String wobNumber, final List<AssegnatarioOrganigrammaDTO> inputAssegnazioniConoscenza,
			final String motivazioneAssegnazione, final UtenteDTO utente) {
		return mettiInConoscenza(wobNumber, inputAssegnazioniConoscenza, motivazioneAssegnazione, utente, null, null);
	}

	@Override
	public final EsitoOperazioneDTO mettiInConoscenza(final String wobNumber, final List<AssegnatarioOrganigrammaDTO> inputAssegnazioniConoscenza,
			final String motivazioneAssegnazione, final UtenteDTO utente, final Integer annoProtocollo, final Integer numProtocollo) {
		EsitoOperazioneDTO esito = null;
		Connection connection = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);
			final Integer idDocumento = (Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));

			if (idDocumento != null) {
				final String idDocumentoString = String.valueOf(idDocumento);

				// ### Si ottengono le attuali assegnazioni per conoscenza
				final String[] assegnazioniConoscenzaCorrenti = (String[]) TrasformerPE.getMetadato(wob,
						pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONOSCENZA_STORICO_METAKEY));

				// Si trasforma la lista delle nuove assegnazioni per conoscenza in array
				final String[] inputAssegnazioniConoscenzaArray = OrganigrammaRetrieverUtils.initArrayAssegnazioni(inputAssegnazioniConoscenza);

				// ### Si modificano le security dei fascicoli in cui è presente il documento
				final boolean esitoModificaSecurityFascicoli = securitySRV.modificaSecurityFascicoli(idDocumentoString, utente, inputAssegnazioniConoscenzaArray, connection);

				if (esitoModificaSecurityFascicoli) {
					boolean isRiservato = false;

					final DetailAssegnazioneDTO detailDocument = securitySRV.getDocumentDetailAssegnazione(fceh, utente.getIdAoo(), idDocumentoString, false);
					isRiservato = detailDocument.isRiservato();

					// ### Si ottengono le nuove security...
					final ListSecurityDTO security = securitySRV.getSecurityPerAddAssegnazione(utente, idDocumentoString, utente.getId(), utente.getIdUfficio(),
							inputAssegnazioniConoscenzaArray, true, false, isRiservato, connection);

					// ### ...e si usano per aggiornare le security del documento
					securitySRV.aggiornaEVerificaSecurityDocumento(idDocumentoString, utente.getIdAoo(), security, false, false, fceh, connection);

					// Si uniscono i due array di assegnazioni
					final String[] allAssegnazioniConoscenza = ArrayUtils.addAll(assegnazioniConoscenzaCorrenti, inputAssegnazioniConoscenzaArray);

					final Map<String, Object> metadatiWorkflow = new HashMap<>();
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONOSCENZA_METAKEY), inputAssegnazioniConoscenzaArray);
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivazioneAssegnazione);
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId().intValue());
					// Se erano già presenti assegnazioni per conoscenza, si valorizza il metadato
					// dello storico
					if (assegnazioniConoscenzaCorrenti != null) {
						metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONOSCENZA_STORICO_METAKEY), allAssegnazioniConoscenza);
					}

					// ### Si avanza del workflow
					final boolean esitoAvanzamentoWorkflow = fpeh.nextStep(wob, metadatiWorkflow, ResponsesRedEnum.METTI_IN_CONOSCENZA.getResponse());

					if (esitoAvanzamentoWorkflow) {
						// Si aggiornano le security dei documenti allacciati
						securitySRV.aggiornaSecurityDocumentiAllacciati(utente, idDocumento, security, inputAssegnazioniConoscenzaArray, fceh, connection);

						// Si aggiornano le security dei contributi inseriti
						securitySRV.aggiornaSecurityContributiInseriti(utente, idDocumentoString, inputAssegnazioniConoscenzaArray, fceh, connection);

						// Si aggiornano di tutti i workflow del documento
						metadatiWorkflow.clear();
						metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONOSCENZA_STORICO_METAKEY), allAssegnazioniConoscenza);

						// Si aggiorna il metadato in tutti i workflow coinvolti nel giro conoscenza
						aggiornaWorkflowsConoscenza(idDocumentoString, metadatiWorkflow, utente.getIdAoo(), utente.getFcDTO().getIdClientAoo(), fpeh, pp);

						esito = new EsitoOperazioneDTO(null, null, detailDocument.getNumeroDocumento(), wobNumber);
						if (annoProtocollo != null && numProtocollo != null) {
							esito.setAnnoProtocollo(annoProtocollo);
							esito.setNumeroProtocollo(numProtocollo);
						}
						esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
						LOGGER.info("Inserimento di nuovi uffici/utenti in conoscenza eseguito correttamente per il documento: " + idDocumentoString);
					} else {
						final String errore = "Errore durante l'avanzamento del workflow: " + wobNumber + " per il documento: " + idDocumentoString;
						LOGGER.error(errore);
						throw new FilenetException(errore);
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento di nuovi uffici/utenti in conoscenza.", e);
			rollbackConnection(connection);
			// Si genera l'esito in caso di eccezione
			esito = new EsitoOperazioneDTO(wobNumber);
			esito.setNote("Errore durante l'inserimento di nuovi uffici/utenti in conoscenza:" + e.getMessage() + " \n");
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}

		return esito;
	}

	private void aggiornaWorkflowsConoscenza(final String idDocumento, final Map<String, Object> metadatiConoscenza, final Long idAoo, final String idClientAoo,
			final FilenetPEHelper fpeh, final PropertiesProvider pp) {
		final String workflowSubjectProcessoConoscenza = pp.getParameterByKey(PropertiesNameEnum.WF_PROCESSO_CONOSCENZA_MAPPING_WFKEY).split("\\|")[0];

		// Si ottengono tutti i workflow associati al documento
		final VWRosterQuery rQueryNSD = fpeh.getRosterQueryWorkByIdDocumentoAndCodiceAOO(idDocumento, idClientAoo);

		while (rQueryNSD.hasNext()) {
			final VWWorkObject workflow = (VWWorkObject) rQueryNSD.next();

			final String[] metadatoElencoConoscenzaStorico = (String[]) TrasformerPE.getMetadato(workflow,
					pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONOSCENZA_STORICO_METAKEY));
			final String workflowSubject = workflow.getSubject();

			// Si procede all'aggiornamento dei metadati del workflow se si verificano
			// entrambe queste condizioni:
			// 1) È già presente il metadato dello storico conoscenza nel workflow
			// 2) F_subject del workflow è 'Assegnazione per conoscenza' o si tratta del
			// workflow padre.
			if (metadatoElencoConoscenzaStorico != null
					&& (workflowSubject != null && (workflowSubjectProcessoConoscenza.equals(workflowSubject) || isWorkflowPadrePerConoscenza(workflowSubject, idAoo)))) {
				fpeh.updateWorkflow(workflow, metadatiConoscenza);
			}
		}
	}

	/**
	 * Getter flag workflow padre per conoscenza.
	 * 
	 * @param workFlowSubject workflow subject
	 * @param idAoo           identificativo aoo
	 * @return flag workflow padre per conoscenza
	 */
	private Boolean isWorkflowPadrePerConoscenza(final String workFlowSubject, final Long idAoo) {
		Boolean output = false;

		final String processoGenericoEntrata = tipoProcedimentoSRV.getWorkflowSubjectProcessoGenericoEntrata(idAoo);
		final String processoGenericoUscita = tipoProcedimentoSRV.getWorkflowSubjectProcessoGenericoUscita(idAoo);

		if (workFlowSubject != null && (workFlowSubject.equals(processoGenericoEntrata) || workFlowSubject.equals(processoGenericoUscita))) {
			output = true;
		}

		return output;
	}

	/**
	 * Restituisce true se se è presente almeno un destinatario esterno in una
	 * spedizione con mezzo cartaceo.
	 * 
	 * @param destinatari lista destinatari (contiene anche il mezzo di spedizione)
	 * @return true se è presente almeno un destinatario esterno con spedizione
	 *         tramite mezzo cartaceo, false altrimenti.
	 */
	private static Boolean destEsternoCartaceo(final List<String[]> destinatari) {
		Boolean output = false;

		for (String[] destinatarioSplit : destinatari) {
			int length = destinatarioSplit.length;
			final int indexBeforeMevCavallo = Constants.Varie.INDEX_BEFORE_MEV_CAVALLO;
			final int index = length - indexBeforeMevCavallo;
			if (index == 0) {
				final String[] app = new String[destinatarioSplit.length + 1];
				for (int j = 0; j < destinatarioSplit.length; j++) {
					app[j] = destinatarioSplit[j];
				}
				app[app.length - 1] = "";
				destinatarioSplit = app;
				length = app.length;
			}

			final String tipoDestinatario = destinatarioSplit[length - 2];
			if (Constants.TipoDestinatario.ESTERNO.equalsIgnoreCase(tipoDestinatario)) {
				// Siamo in presenza di un destinatario esterno
				final String idTipoSpedizione = destinatarioSplit[length - Constants.Varie.INDICE_SPLIT_TIPO_SPEDIZIONE];
				if (("" + Constants.TipoSpedizione.MEZZO_CARTACEO).equals(idTipoSpedizione)) {
					// Siamo in presenza di un destinatario esterno con spedizione tramite mezzo
					// cartaceo
					output = true;
					break;
				}
			}
		}

		return output;
	}

	/**
	 * Ritorna la lista delle versioni di un documento dato il suo document title
	 * (identificativo) e l'identificativo dell'aoo di appartenenza.
	 * 
	 * @param fceh          istanza dell'helper per l'invocazione del CE di filenet
	 * @param documentTitle identificativo del documento
	 * @param idAoo         identificativo dell'aoo
	 * @return Insieme delle versioni del documento
	 */
	private static List<?> getAllDocumentVersions(final IFilenetCEHelper fceh, final Integer documentTitle, final Long idAoo) {
		final Document doc = fceh.getDocumentByDTandAOO(documentTitle.toString(), idAoo);
		final SubSetImpl ssi = (SubSetImpl) doc.get_VersionSeries().get_Versions();
		return ssi.getList();
	}

	/**
	 * Ritorna la prima versione del documento.
	 * 
	 * @param fceh        helper CE
	 * @param idDocumento identificativo documento
	 * @param idAoo       identificativo aoo
	 * @return prima versione del documento
	 */
	private static Document getFirstVersionById(final IFilenetCEHelper fceh, final Integer idDocumento, final Long idAoo) {
		final List<?> list = getAllDocumentVersions(fceh, idDocumento, idAoo);
		return (Document) list.get(list.size() - 1);
	}

	/**
	 * Ritorna l'ultima versione del documento.
	 * 
	 * @param fceh        helper CE
	 * @param idDocumento identificativo documento
	 * @param idAoo       identificativo aoo
	 * @return ultima versione del documento
	 */
	private static Document getLastVersionById(final IFilenetCEHelper fceh, final Integer idDocumento, final Long idAoo) {
		final List<?> list = getAllDocumentVersions(fceh, idDocumento, idAoo);
		return (Document) list.get(0);
	}

	/**
	 * Recupera l'annotazione di un documento con un dato identificativo ed
	 * afferente ad una specifica aoo.
	 * 
	 * @param fceh           helper del CE
	 * @param idDocumento    identificativo del documento
	 * @param nomeAnnotation nome dell'annotazione
	 * @param idAoo          identificativo aoo
	 * @return annotazione
	 */
	private static Annotation getAnnotation(final IFilenetCEHelper fceh, final Integer idDocumento, final String nomeAnnotation, final Long idAoo) {
		Annotation annotation = null;
		Document doc = null;
		if ("MAIL".equals(nomeAnnotation) || "CURVERSION".equals(nomeAnnotation)) {
			doc = getFirstVersionById(fceh, idDocumento, idAoo);
		} else if ("MAILMESSAGEID".equals(nomeAnnotation)) {
			doc = getLastVersionById(fceh, idDocumento, idAoo);
		} else {
			doc = fceh.getDocumentByDTandAOO(idDocumento.toString(), idAoo);
		}
		final AnnotationSet as = doc.get_Annotations();

		final Iterator<?> iter = as.iterator();
		while (iter.hasNext()) {
			final Annotation annObject = (Annotation) iter.next();
			if (nomeAnnotation.equals(annObject.get_DescriptiveText())) {
				annotation = annObject;
			}
		}
		return annotation;
	}

	/**
	 * Metodo per l'aggiornamento delle security del documento sottoposto al cambio
	 * iter.
	 * 
	 * @param isRiservato                   flag che indica se il documento è
	 *                                      riservato o meno
	 * @param idNodoCoordinatore            identificativo del nodo del coordinatore
	 * @param idUtenteCoordinatore          identificativo dell'utente coordinatore
	 * @param elencoAssegnazioniAutomatiche elenco delle security da gestire in
	 *                                      funzione dell'iter prescelto
	 * @param con                           connessione al database applicativo
	 * @param utente                        utente che stà eseguendo il cambio iter
	 * @param idDocumento                   identificativo del documento di oggetto
	 *                                      di cambio itert
	 */
	private void aggiornaSecurity(final Boolean isRiservato, final Integer idNodoCoordinatore, final Integer idUtenteCoordinatore,
			final String[] elencoAssegnazioniAutomatiche, final Connection con, final UtenteDTO utente, final String idDocumento) {
		String assegnazioneCoordinatore = null;
		if (idNodoCoordinatore != null && idUtenteCoordinatore != null) {
			assegnazioneCoordinatore = idNodoCoordinatore + "," + idUtenteCoordinatore;
		}

		securitySRV.modificaSecurityFascicoli(idDocumento, utente, elencoAssegnazioniAutomatiche, assegnazioneCoordinatore, false, con);

		final ListSecurityDTO securities = securityCambioIterSRV.getSecurityPerRiassegnazione(utente, idDocumento + "", elencoAssegnazioniAutomatiche,
				isRiservato, con, assegnazioneCoordinatore);

		// Aggiorno la security
		securitySRV.updateSecurity(utente, idDocumento, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FOLDER_DOCUMENTI_CE_KEY),
				new ListSecurityDTO(securities), true);

		// Aggiorna le security di tutti i documenti allacciati
		securityCambioIterSRV.aggiornaSecurityDocumentiAllacciati(idDocumento, securities, con, elencoAssegnazioniAutomatiche, utente);

		// Aggiorna le security di tutti i contributi collegati
		securityCambioIterSRV.aggiornaSecurityContributiInseriti(idDocumento, con, elencoAssegnazioniAutomatiche, utente);
	}

}