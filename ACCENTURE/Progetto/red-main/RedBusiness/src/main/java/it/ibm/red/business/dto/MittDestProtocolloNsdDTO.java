/**
 * 
 */
package it.ibm.red.business.dto;

/**
 * @author VINGENITO
 *
 */
public class MittDestProtocolloNsdDTO extends AbstractDTO {

	private static final long serialVersionUID = 1481851765406292948L;

	/**
	 * Numero protocollo mittente.
	 */
	private String nProtMittente;

	/**
	 * Destinatario.
	 */
	private String mittDest;

	/**
	 * Codice fiscale.
	 */
	private String cFpIva;

	/**
	 * @return the mittDest
	 */
	public String getMittDest() {
		return mittDest;
	}

	/**
	 * @param mittDest the mittDest to set
	 */
	public void setMittDest(final String mittDest) {
		this.mittDest = mittDest;
	}

	/**
	 * @return the nProtMittente
	 */
	public String getnProtMittente() {
		return nProtMittente;
	}

	/**
	 * @param nProtMittente the nProtMittente to set
	 */
	public void setnProtMittente(final String nProtMittente) {
		this.nProtMittente = nProtMittente;
	}

	/**
	 * @return the cFpIva
	 */
	public String getcFpIva() {
		return cFpIva;
	}

	/**
	 * @param cFpIva the cFpIva to set
	 */
	public void setcFpIva(final String cFpIva) {
		this.cFpIva = cFpIva;
	}

}
