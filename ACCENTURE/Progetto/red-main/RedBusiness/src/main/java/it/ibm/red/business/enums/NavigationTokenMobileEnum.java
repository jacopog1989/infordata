package it.ibm.red.business.enums;

/**
 * The Enum NavigationTokenEnum.
 *
 * @author CPIERASC
 * 
 *         Enum gestione navigation token.
 */
public enum NavigationTokenMobileEnum {
	
	/**
	 * Pagina di login.
	 */
	LOGINPAGE(1, "login_tok", "/views/help/login.html", "RedMobile: Login"),
	
	/**
	 * Pagina di homepage.
	 */
	HOMEPAGE(2, "homepage_tok", "/views/help/homepage.html", "RedMobile: Pagina iniziale"),
	
	/**
	 * Pagina libro firma.
	 */
	LIBRO_FIRMA(3, NavigationTokenMobileEnum.LIBRO_TOK, NavigationTokenMobileEnum.LIBRO_FIRMA_HTML, "RedMobile: Libro firma"),
	
	/**
	 * Pagina Coda Corriere.
	 */
	CODA_CORRIERE(4, NavigationTokenMobileEnum.LIBRO_TOK, NavigationTokenMobileEnum.LIBRO_FIRMA_HTML, "RedMobile: Corriere ufficio"),
	
	/**
	 * Pagina Coda Corriere.
	 */
	DA_LAVORARE(5, NavigationTokenMobileEnum.LIBRO_TOK, NavigationTokenMobileEnum.LIBRO_FIRMA_HTML, "RedMobile: Corriere utente"),
	
	/**
	 * Pagina di ricerca.
	 */
	RICERCA(6, "ricerca_tok", "/views/help/ricerca.html", "RedMobile: Ricerca generica"),
	
	/**
	 * Scadenzario.
	 */
	SCADENZARIO(7, "scadenzario_tok", "/views/help/scadenzario.html", "RedMobile: Scadenzario"),
	
	/**
	 * Pagina coda Mail.
	 * 
	 * @author AndreaP
	 */
	CODA_MAIL(8, "mail_tok", "/views/help/mail.html", "RedMobile: Mail"),
	
	/**
	 * Pagina Firma Rapida.
	 * 
	 * @author AndreaP
	 */
	FIRMA_RAPIDA(9, "firma_tok", "/views/help/firmaRapida.html", "RedMobile: Firma Rapida");
	
	private static final String LIBRO_TOK = "libro_tok";
	private static final String LIBRO_FIRMA_HTML = "/views/help/libroFirma.html";
	
	/**
	 * Identificativo.
	 */
	private Integer id;	
	
	/**
	 * Token navigation rule.
	 */
	private String token;
	
	/**
	 * Pagina di help associata alla pagina.
	 */
	private String helpPage;
	
	/**
	 * Titolo della pagina.
	 */
	private String titlePage;

	/**
	 * Costruttore.
	 * 
	 * @param inId			identificativo
	 * @param inToken		token
	 * @param inHelpPage	help page
	 * @param inTitlePage	title page
	 */
	NavigationTokenMobileEnum(final Integer inId, final String inToken, final String inHelpPage, final String inTitlePage) {
		id = inId;
		token = inToken;
		helpPage = inHelpPage;
		titlePage = inTitlePage;
	}

	/**
	 * Getter.
	 * 
	 * @return	id
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Getter token.
	 * 
	 * @return	token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Getter help page.
	 * 
	 * @return	help page
	 */
	public String getHelpPage() {
		return helpPage;
	}

	/**
	 * Getter title page.
	 * 
	 * @return	title page
	 */
	public String getTitlePage() {
		return titlePage;
	}

}