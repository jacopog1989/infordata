package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * DTO che definisce un registro.
 */
public class RegistroDTO implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 3494907653020844621L;
	
	/**
	 * Id.
	 */
	private final Integer id;
	
	/**
	 * Name.
	 */
	private final String nome;
	
	/**
	 * Tipo.
	 */
	private Integer idTipo;
	
	/**
	 * Descrizione tipo.
	 */
	private String descrizioneTipo;
	
	/**
	 * Display name.
	 */
	private String displayName;
	
	/**
	 * Costruttore.
	 * @param id
	 * @param nome
	 */
	public RegistroDTO(final Integer id, final String nome) {
		super();
		this.id = id;
		this.nome = nome;
	}
	
	/**
	 * Costruttore.
	 * @param id
	 * @param nome
	 * @param idTipo
	 * @param descrizioneTipo
	 */
	public RegistroDTO(final Integer id, final String nome, final Integer idTipo, final String descrizioneTipo) {
		this(id, nome);
		this.idTipo = idTipo;
		this.descrizioneTipo = descrizioneTipo;
	}

	/**
	 * Restituisce l'id del del registro.
	 * @return id registro
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Restituisce il nome del registro.
	 * @return nome registro
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Restituisce il tipo del registro.
	 * @return tipo registro
	 */
	public Integer getIdTipo() {
		return idTipo;
	}

	/**
	 * Restituisce la descrizione del registro.
	 * @return descrizione registro
	 */
	public String getDescrizioneTipo() {
		return descrizioneTipo;
	}

	/**
	 * Restituisce il display name del registro.
	 * @return display name registro
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Imposta il display name.
	 * @param displayName
	 */
	public void setDisplayName(final String displayName) {
		this.displayName = displayName;
	}
	
}
