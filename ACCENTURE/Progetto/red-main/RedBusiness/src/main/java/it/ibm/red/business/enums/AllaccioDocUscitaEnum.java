/**
 * 
 */
package it.ibm.red.business.enums;

/**
 * @author VINGENITO
 *
 */
public enum AllaccioDocUscitaEnum {
	
	/**
	 * Dcoumento uscita da lavorare.
	 */
	DA_LAVORARE(0, "Documento da lavorare"),

	/**
	 * Documento in lavorazione.
	 */
	IN_LAVORAZIONE(1, "Documento in lavorazione"),

	/**
	 * Documento lavorato.
	 */
	LAVORATO(2, "Documento lavorato"),

	/**
	 * Docuemnto in errore.
	 */
	ERRORE(3, "Documento in errore");
	
	/**
	 * id.
	 */
	private Integer stato;

	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Costruttore.
	 * 
	 * @param inStato stato
	 * @param inDescrizione	descrizione
	 */
	AllaccioDocUscitaEnum(final int inStato, final String inDescrizione) {
		this.stato = inStato;
		this.descrizione = inDescrizione;
	}

	/**
	 * Getter descrizione.
	 * 
	 * @return	descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Getter stato.
	 * 
	 * @return	stato
	 */
	public Integer getStato() {
		return stato;
	}

}
