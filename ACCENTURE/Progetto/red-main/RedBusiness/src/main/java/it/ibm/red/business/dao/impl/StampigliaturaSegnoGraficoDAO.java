package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IStampigliaturaSegnoGraficoDAO;
import it.ibm.red.business.dto.IconaStampigliaturaSegnoGraficoDTO;
import it.ibm.red.business.dto.StampigliaturaSegnoGraficoDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.StringUtils;

/**
 * 
 * @author Vingenito
 *
 */
@Repository
public class StampigliaturaSegnoGraficoDAO extends AbstractDAO implements IStampigliaturaSegnoGraficoDAO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -7951803663699728074L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(StampigliaturaSegnoGraficoDAO.class.getName());

	/**
	 * Label Placeholder.
	 */
	private static final String PLACEHOLDER = "PLACEHOLDER";

	/**
	 * Messaggio errore update placeholder. Occorre appendere l'identificativo del
	 * placeholder al messaggio.
	 */
	private static final String ERROR_UPDATE_PLACEHOLDER_MSG = "Errore durante l'update del placeholder : ";

	/**
	 * Messaggio errore insert placeholder da parte del job. Occorre appendere
	 * l'identificativo del job al messaggio.
	 */
	private static final String ERROR_INSERT_PLACEHOLDER_MSG = "Errore durante l'insert del placeholder da parte del job : ";

	/**
	 * @see it.ibm.red.business.dao.IStampigliaturaSegnoGraficoDAO#insertOrUpdatePlaceholderJob(java.lang.String,
	 *      java.lang.Long, java.lang.Long, java.lang.Integer, java.util.List,
	 *      java.sql.Connection).
	 */
	@Override
	public int insertOrUpdatePlaceholderJob(final String idDocumento, final Long idUffIstruttore, final Long idUtente, final Integer idTipoProc,
			final List<String> placeholderList, final Connection conn) {
		final PreparedStatement ps = null;
		int output = -1;
		try {
			final String placeholder = getDocumentById(idDocumento, conn);
			StringBuilder placeholderNuovo = new StringBuilder("");
			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(placeholder)) {
				placeholderNuovo = new StringBuilder(placeholder);
			}

			if (!CollectionUtils.isEmpty(placeholderList)) {
				for (final String placeholderString : placeholderList) {
					final boolean checkPlaceholder = !it.ibm.red.business.utils.StringUtils.isNullOrEmpty(placeholder) && placeholder.contains(placeholderString);
					if (!checkPlaceholder) {
						if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(placeholderNuovo.toString())) {
							placeholderNuovo = new StringBuilder(placeholderString);
						} else {
							placeholderNuovo.append("||").append(placeholderString);
						}

					}
				}
			}

			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(placeholderNuovo.toString())) {
				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(placeholder)) {
					output = insertPlaceholder(idDocumento, idUffIstruttore, idUtente, idTipoProc, placeholderNuovo.toString(), conn);
				} else {
					output = updatePlaceholder(idDocumento, idUffIstruttore, idUtente, idTipoProc, placeholderNuovo.toString(), conn);
				}
			}

		} catch (final Exception ex) {
			LOGGER.error(ERROR_INSERT_PLACEHOLDER_MSG + ex);
			throw new RedException(ERROR_INSERT_PLACEHOLDER_MSG + ex);
		} finally {
			closeStatement(ps);
		}

		return output;
	}

	private static String getDocumentById(final String idDocumento, final Connection conn) {
		String placeholder = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM DOCUMENTO_PLACEHOLDER WHERE ID_DOC = " + idDocumento);
			ps = conn.prepareStatement(sb.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				placeholder = rs.getString(PLACEHOLDER);
			}
		} catch (final Exception ex) {
			LOGGER.error("getDocumentById -> Errore durante il recupero dei placeholder : " + ex);
			throw new RedException("getDocumentById -> Errore durante il recupero dei placeholder : " + ex);
		} finally {
			closeStatement(ps, rs);
		}
		return placeholder;
	}

	private static int insertPlaceholder(final String idDocumento, final Long idUffIstruttore, final Long idUtente, final Integer idTipoProc, final String placeholder,
			final Connection conn) {
		PreparedStatement ps = null;
		try {
			final StringBuilder sb = new StringBuilder();
			sb.append("INSERT INTO DOCUMENTO_PLACEHOLDER (ID_DOC, ID_UFF_ISTRUTTORE, ID_UTENTE, ID_TIPO_PROC, PLACEHOLDER)" + " VALUES (?, ?, ?, ?, ?)");
			int index = 1;
			ps = conn.prepareStatement(sb.toString());
			ps.setLong(index++, Long.valueOf(idDocumento));
			ps.setLong(index++, idUffIstruttore);
			ps.setLong(index++, idUtente);
			ps.setInt(index++, idTipoProc);
			ps.setString(index++, placeholder);
			return ps.executeUpdate();
		} catch (final Exception ex) {
			LOGGER.error(ERROR_INSERT_PLACEHOLDER_MSG + ex);
			throw new RedException(ERROR_INSERT_PLACEHOLDER_MSG + ex);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IStampigliaturaSegnoGraficoDAO#updatePlaceholder(java.lang.String,
	 *      java.lang.Long, java.lang.Integer, java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public int updatePlaceholder(final String idDocumento, final Long idUffIstruttore, final Integer idTipoProc, final String placeholder, final Connection conn) {
		PreparedStatement ps = null;
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("UPDATE DOCUMENTO_PLACEHOLDER SET ID_UFF_ISTRUTTORE = ? ,ID_TIPO_PROC = ?,PLACEHOLDER = ? WHERE ID_DOC = ?");
			ps = conn.prepareStatement(sb.toString());
			ps.setLong(index++, idUffIstruttore);
			ps.setInt(index++, idTipoProc);
			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(placeholder)) {
				ps.setString(index++, placeholder);
			} else {
				ps.setString(index++, null);
			}
			ps.setLong(index++, Long.valueOf(idDocumento));
			return ps.executeUpdate();
		} catch (final Exception ex) {
			LOGGER.error(ERROR_UPDATE_PLACEHOLDER_MSG + ex);
			throw new RedException(ERROR_UPDATE_PLACEHOLDER_MSG + ex);
		} finally {
			closeStatement(ps);
		}
	}

	private static int updatePlaceholder(final String idDocumento, final Long idUffIstruttore, final Long idUtente, final Integer idTipoProc, final String placeholder,
			final Connection conn) {
		PreparedStatement ps = null;
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("UPDATE DOCUMENTO_PLACEHOLDER SET ID_UFF_ISTRUTTORE = ? ,ID_UTENTE = ?,ID_TIPO_PROC = ?,PLACEHOLDER = ? WHERE ID_DOC = ?");
			ps = conn.prepareStatement(sb.toString());
			ps.setLong(index++, idUffIstruttore);
			ps.setLong(index++, idUtente);
			ps.setInt(index++, idTipoProc);
			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(placeholder)) {
				ps.setString(index++, placeholder);
			} else {
				ps.setString(index++, null);
			}
			ps.setLong(index++, Long.valueOf(idDocumento));
			return ps.executeUpdate();
		} catch (final Exception ex) {
			LOGGER.error(ERROR_UPDATE_PLACEHOLDER_MSG + ex);
			throw new RedException(ERROR_UPDATE_PLACEHOLDER_MSG + ex);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IStampigliaturaSegnoGraficoDAO#getPlaceholderByIdDoc(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public String getPlaceholderByIdDoc(final String idDocumento, final Connection conn) {
		String placeholder = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM DOCUMENTO_PLACEHOLDER WHERE ID_DOC = " + Long.valueOf(idDocumento));
			ps = conn.prepareStatement(sb.toString());
			rs = ps.executeQuery();

			while (rs.next()) {
				placeholder = rs.getString(PLACEHOLDER);
			}

		} catch (final Exception ex) {
			LOGGER.error("Errore durante il recupero del placeholder: " + ex);
			throw new RedException("Errore durante il recupero del placeholder : " + ex);
		} finally {
			closeStatement(ps, rs);
		}
		return placeholder;
	}

	/**
	 * @see it.ibm.red.business.dao.IStampigliaturaSegnoGraficoDAO#getIconByPlaceholder(java.lang.String,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public IconaStampigliaturaSegnoGraficoDTO getIconByPlaceholder(final String placeholder, final String numDocumento, final Connection conn) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final IconaStampigliaturaSegnoGraficoDTO iconOutput = new IconaStampigliaturaSegnoGraficoDTO();
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT DP.PLACEHOLDER,GSU.FONT_AWESOME,GSU.IMMAGINE, GSU.DESCR_ICONA ");
			sb.append("FROM GLIFO_UTENTE GSU JOIN DOCUMENTO_PLACEHOLDER DP ");
			sb.append("ON GSU.ID_UTENTE = DP.ID_UTENTE ");
			sb.append("WHERE GSU.PLACEHOLDER = ? AND DP.ID_DOC = ?");
			ps = conn.prepareStatement(sb.toString());
			ps.setString(index++, placeholder);
			ps.setString(index++, numDocumento);
			rs = ps.executeQuery();
			while (rs.next()) {
				iconOutput.setDescrIcona(rs.getString("DESCR_ICONA"));
				iconOutput.setFontAwesomeIcon(rs.getString("FONT_AWESOME"));
			}
		} catch (final Exception ex) {
			LOGGER.error("Errore durante il recupero dell'icon: " + ex);
			throw new RedException("Errore durante il recupero dell'icon : " + ex);
		} finally {
			closeStatement(ps, rs);
		}
		return iconOutput;
	}

	/**
	 * @see it.ibm.red.business.dao.IStampigliaturaSegnoGraficoDAO#getPlaceholderSegnoGrafico(java.lang.Long,
	 *      java.lang.Long, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public HashMap<String, byte[]> getPlaceholderSegnoGrafico(final Long idUtente, final Long idUffIstruttore, final Integer idTipoDoc, final Connection conn) {
		return getPlaceholderSegnoGrafico(idUtente, idUffIstruttore, idTipoDoc, null, conn);
	}

	/**
	 * @see it.ibm.red.business.dao.IStampigliaturaSegnoGraficoDAO#getPlaceholderSegnoGrafico(java.lang.Long,
	 *      java.lang.Long, java.lang.Integer, java.util.List, java.sql.Connection).
	 */
	@Override
	public HashMap<String, byte[]> getPlaceholderSegnoGrafico(final Long idUtente, final Long idUffIstruttore, final Integer idTipoDoc,
			final List<StampigliaturaSegnoGraficoDTO> listStampigliaturaSegnoGrafico, final Connection conn) {
		final HashMap<String, byte[]> placeholderList = new HashMap<>();

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM GLIFO_UTENTE WHERE ID_UTENTE = ? AND ID_UFF_ISTRUTTORE = ? AND ID_TIPO_DOC = ? ");
			ps = conn.prepareStatement(sb.toString());
			ps.setLong(index++, idUtente);
			ps.setLong(index++, idUffIstruttore);
			ps.setInt(index++, idTipoDoc);
			rs = ps.executeQuery();

			while (rs.next()) {
				placeholderList.put(rs.getString(PLACEHOLDER), rs.getBytes("GLIFO"));
				if (listStampigliaturaSegnoGrafico != null) {
					final HashMap<String, byte[]> placeHolderPiuColori = new HashMap<>();
					placeHolderPiuColori.put(rs.getString(PLACEHOLDER), rs.getBytes("GLIFO"));

					final String red = rs.getString("RED");
					final Integer redInt = checkColorAndInterval(red);
					final String green = rs.getString("GREEN");
					final Integer greenInt = checkColorAndInterval(green);
					final String blue = rs.getString("BLUE");
					final Integer blueInt = checkColorAndInterval(blue);

					final StampigliaturaSegnoGraficoDTO stampigliatura = new StampigliaturaSegnoGraficoDTO(placeHolderPiuColori, redInt, greenInt, blueInt);
					listStampigliaturaSegnoGrafico.add(stampigliatura);
				}
			}

		} catch (final Exception ex) {
			LOGGER.error("Errore nel recupero del placeholder di sigla: " + ex);
			throw new RedException("Errore nel recupero del placeholder di sigla: " + ex);
		} finally {
			closeStatement(ps, rs);
		}
		
		return placeholderList;
	}

	@Override
	public final Integer getTipoFirma(final Integer idIterApprovativo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer output = null;
		try {
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT IA.TIPOFIRMA FROM ITERAPPROVATIVO IA WHERE IDITERAPPROVATIVO = ?");

			ps = con.prepareStatement(sb.toString());
			ps.setInt(1, idIterApprovativo);
			rs = ps.executeQuery();
			while (rs.next()) {
				output = rs.getInt("TIPOFIRMA");
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero del tipo firma dell'iter approvativo. ", e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IStampigliaturaSegnoGraficoDAO#deleteRowPlaceholderEmpty(java.lang.String,
	 *      java.lang.Long, java.lang.Long, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public int deleteRowPlaceholderEmpty(final String idDocumento, final Long idUffIstruttore, final Long idUtente, final Integer idTipoProc, final Connection conn) {
		PreparedStatement ps = null;
		try {
			int index = 1;
			ps = conn.prepareStatement("DELETE FROM DOCUMENTO_PLACEHOLDER WHERE ID_DOC = ? AND ID_UFF_ISTRUTTORE = ? AND ID_UTENTE = ? AND ID_TIPO_PROC = ? ");
			ps.setString(index++, idDocumento);
			ps.setLong(index++, idUffIstruttore);
			ps.setLong(index++, idUtente);
			ps.setLong(index++, idTipoProc);

			return ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'eliminazione del record documento_placeholder: " + idDocumento, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	private Integer checkColorAndInterval(final String color) {
		Integer output = null;
		if (!StringUtils.isNullOrEmpty(color) && NumberUtils.isParsable(color)) {
			output = Integer.parseInt(color);
			if (output < 0 || output > 255) {
				output = null;
			}
		}
		return output;
	}

}
