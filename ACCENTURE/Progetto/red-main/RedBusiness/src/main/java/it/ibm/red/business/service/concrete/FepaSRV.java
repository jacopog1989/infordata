/**
 * 
 */
package it.ibm.red.business.service.concrete;

import static fepa.types.v3.StatoFascicoloDocumentaleType.CHIUSO;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.xml.datatype.DatatypeConfigurationException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.collection.IndependentObjectSet;
import com.filenet.api.collection.LinkSet;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.CustomObject;
import com.filenet.api.core.Document;
import com.filenet.api.core.Link;
import com.filenet.api.exception.EngineRuntimeException;
import com.filenet.api.property.Properties;

import fepa.header.v3.BaseServiceResponseType;
import fepa.header.v3.EsitoType;
import fepa.header.v3.ServiceErrorType;
import fepa.messages.v3.RispostaAddDocumentoFascicolo;
import fepa.messages.v3.RispostaDownloadDocumentoFascicolo;
import fepa.messages.v3.RispostaGetFascicolo;
import fepa.services.v3.InterfacciaFascicoli;
import fepa.types.v3.DocumentoMetadataType;
import fepa.types.v3.FascicoloBaseType;
import fepa.types.v3.FascicoloType;
import fepa.types.v3.RichiestaAddDocumentoFascicoloType;
import fepa.types.v3.RichiestaAddDocumentoFascicoloType.Documento;
import fepa.types.v3.RichiestaChangeStatoFascicoloType;
import fepa.types.v3.RichiestaDownloadDocumentoFascicoloType;
import fepa.types.v3.RichiestaGetFascicoloType;
import fepa.types.v3.RispostaChangeStatoFascicoloType;
import fepa.types.v3.TipoDettaglioEstrazioneFascicoloType;
import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWWorkObject;
import internal.demdec.v1.it.gov.mef.servizi.common.headeraccessoapplicativo.AccessoApplicativo;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembil.InterfacciaRaccoltaProvvisoriaDEMBIL;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.CodiceDescrizioneType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.DocumentoContentType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.DocumentoMetadataRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.FascicoloMetadataRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.ProtocolloType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RaggruppamentoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaAddDocumentoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaChangeStatoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaCreateFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaModifyMetadataFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaOperazioneDocumentaleType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaChangeStatoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaCreateFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetFascicoloRaccoltaProvvisoria;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaModifyMetadataFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.StatoFascicoloDocumentaleType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.TipoDocumentoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.TipoEstrazioneType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.TipoFlussoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.TipoOperazioneType;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.IFadDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AllegatoOPDTO;
import it.ibm.red.business.dto.DecretoDirigenzialeDTO;
import it.ibm.red.business.dto.DetailDocumentoDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.DocumentoFascicoloFepaDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FadAmministrazioneDTO;
import it.ibm.red.business.dto.FadRagioneriaDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FascicoloFEPAInfoDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.OrdineDiPagamentoDTO;
import it.ibm.red.business.dto.PEDocumentoFEPADTO;
import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.dto.RaccoltaFadDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.TrasmissioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipoAccessoFascicoloFepaEnum;
import it.ibm.red.business.enums.TipologiaDocumentoEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.fepa.DocumentoFepa;
import it.ibm.red.business.helper.fepa.DocumentoFepaMapper;
import it.ibm.red.business.helper.fepa.FepaMetadataMapper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.TrasformPE;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.monitoring.attodecreto.AttoDecretoMonitoring;
import it.ibm.red.business.monitoring.attodecreto.AttoDecretoParsedResponse;
import it.ibm.red.business.monitoring.attodecreto.AttoDecretoProxy;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.provider.WebServiceClientProvider;
import it.ibm.red.business.service.IAllegatoSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.IFepaSRV;
import it.ibm.red.business.service.IMailSRV;
import it.ibm.red.business.service.ITipologiaDocumentoSRV;
import it.ibm.red.business.service.facade.IAooFacadeSRV;
import it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.SerializationUtils;

/**
 * Service FEPA.
 */
@Service
@Component
public class FepaSRV extends AbstractService implements IFepaSRV {

	/**
	 * Label.
	 */
	private static final String DI_TIPO_LITERAL = " di tipo ";

	private static final long serialVersionUID = 3123859422085722339L;

	/**
	 * Messaggio invio documento.
	 */
	private static final String INVIO_DOCUMENTO_LITERAL = "Invio documento ";

	/**
	 * Messaggio riferimento fascicolo.
	 */
	private static final String AL_FASCICOLO_LITERAL = " al fascicolo ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FepaSRV.class.getName());

	/**
	 * 
	 * Enum per la gestione della provenienza.
	 *
	 */
	public enum ProvenienzaType {

		/**
		 * Valore.
		 */
		ALTRO,

		/**
		 * Valore.
		 */
		AMMINISTRAZIONE,

		/**
		 * Valore.
		 */
		UCB,

		/**
		 * Valore.
		 */
		RED,

		/**
		 * Valore.
		 */
		NPS,

		/**
		 * Valore.
		 */
		CORTE;
	}

	/**
	 * DAO.
	 */
	@Autowired
	private IAooDAO aooDao;

	/**
	 * DAO.
	 */
	@Autowired
	private IFadDAO fadDao;

	/**
	 * Service.
	 */
	@Autowired
	private IFascicoloSRV fascicoloSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IListaDocumentiFacadeSRV listaDocSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IAllegatoSRV allegatoSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipologiaDocumentoDAO tipologiaDocumentoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IMailSRV mailSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ITipologiaDocumentoSRV tipologiaDocumentoSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IAooFacadeSRV aooSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IUtenteFacadeSRV utenteSRV;

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * Post contruct del Service.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#creaRaccoltaFad(it.ibm.red.business.dto.RaccoltaFadDTO,
	 *      java.lang.Boolean, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public String creaRaccoltaFad(final RaccoltaFadDTO fdaDTO, final Boolean processoManuale, final UtenteDTO utente) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return creaRaccoltaFad(fdaDTO, processoManuale, utente, connection);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IFepaSRV#creaRaccoltaFad(it.ibm.red.business.dto.RaccoltaFadDTO,
	 *      java.lang.Boolean, it.ibm.red.business.dto.UtenteDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public String creaRaccoltaFad(final RaccoltaFadDTO fdaDTO, final Boolean processoManuale, final UtenteDTO utente, final Connection connection) {
		String idFascicoloRaccoltaProvvisoria = null;
		FilenetPEHelper fpeh = null;

		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			// raccolta dati per creazione request
			final RichiestaCreateFascicoloRaccoltaProvvisoriaType metadatiRequest = new RichiestaCreateFascicoloRaccoltaProvvisoriaType();
			metadatiRequest.setIdFascicoloRaccoltaProvvisoria(null);
			metadatiRequest.setDescrizione(fdaDTO.getOggetto());
			metadatiRequest.setAttivo(true);
			metadatiRequest.setDaInviare(true);
			final FascicoloMetadataRaccoltaProvvisoriaType metadatiFascicoloRequest = getMetadatiFascicoloForRequest(fdaDTO, utente, processoManuale, connection);
			metadatiRequest.setDatiFascicolo(metadatiFascicoloRequest);

			final InterfacciaRaccoltaProvvisoriaDEMBIL stub = AttoDecretoProxy
					.newInstance(WebServiceClientProvider.getIstance().getInterfacciaRaccoltaProvvisoriaDEMBILClient(), new AttoDecretoMonitoring(fdaDTO, utente));
			final AccessoApplicativo accessoApplicativo = WebServiceClientProvider.getIstance().getAccessoInterfacciaRaccoltaProvvisoriaDEMBILClient();
			// Invocazione WS
			final RispostaCreateFascicoloRaccoltaProvvisoriaType responseCreazioneFascicolo = stub.createFascicoloRaccoltaProvvisoria(accessoApplicativo, metadatiRequest);
			final AttoDecretoParsedResponse response = AttoDecretoProxy.parseResponse(responseCreazioneFascicolo, Integer.parseInt(fdaDTO.getDocumentTitle()));
			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(response.getIdRaccoltaProvvisoria())) {
				idFascicoloRaccoltaProvvisoria = response.getIdRaccoltaProvvisoria();
				gestioneResponseCreazioneOK(idFascicoloRaccoltaProvvisoria, fdaDTO, utente, processoManuale, fpeh);
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante la creazione della raccolta Provvisoria FAD", e);
			throw new RedException("Errore durante la creazione della raccolta Provvisoria FAD", e);
		} finally {
			logoff(fpeh);
		}

		return idFascicoloRaccoltaProvvisoria;
	}

	private static List<ProtocolloType> getProtocolliFAD(final boolean processoManuale, final String aooToSend, final RaccoltaFadDTO fadDTO) {
		final List<ProtocolloType> protocolliOutput = new ArrayList<>();
		ProtocolloType protocolloType = null;
		if (!processoManuale) {
			// protocollo uscita amministrazione
			protocolloType = new ProtocolloType();
			protocolloType.setNumeroProtocollo(fadDTO.getNumeroProtocolloAmmUscita());
			try {
				protocolloType.setData(DateUtils.buildXmlGregorianCalendarFromDate(fadDTO.getDataProtocolloAmmUscita()));
			} catch (final DatatypeConfigurationException e) {
				LOGGER.error("Errore nella formattazione della data del protocollo Amministrazione Uscita", e);
			}
			protocolloType.setAoo(fadDTO.getAooProtocolloAmmUscita());
			protocolloType.setProvenienza(ProvenienzaType.AMMINISTRAZIONE.toString());
			protocolloType.setTipo(Constants.Protocollo.TIPOLOGIA_PROTOCOLLO_USCITA);
			protocolliOutput.add(protocolloType);

			// protocollo ingresso ucb
			protocolloType = new ProtocolloType();
			protocolloType.setNumeroProtocollo(fadDTO.getNumeroProtocolloUcbIngresso());
			try {
				protocolloType.setData(DateUtils.buildXmlGregorianCalendarFromDate(fadDTO.getDataProtocolloUcbIngresso()));
			} catch (final DatatypeConfigurationException e) {
				LOGGER.error("Errore nella formattazione della data del protocollo UCB Ingresso", e);
			}
			protocolloType.setAoo(fadDTO.getAooProtocolloUcbIngresso());
			protocolloType.setProvenienza(ProvenienzaType.UCB.toString());
			protocolloType.setTipo(Constants.Protocollo.TIPOLOGIA_PROTOCOLLO_INGRESSO);
			protocolliOutput.add(protocolloType);

			// protocollo uscita ucb
			protocolloType = new ProtocolloType();
			protocolloType.setNumeroProtocollo(fadDTO.getNumeroProtocolloUcbUscita());
			try {
				protocolloType.setData(DateUtils.buildXmlGregorianCalendarFromDate(fadDTO.getDataProtocolloUcbUscita()));
			} catch (final DatatypeConfigurationException e) {
				LOGGER.error("Errore nella formattazione della data del protocollo UCB Uscita", e);
			}
			protocolloType.setAoo(fadDTO.getAooProtocolloUcbUscita());
			protocolloType.setProvenienza(ProvenienzaType.UCB.toString());
			protocolloType.setTipo(Constants.Protocollo.TIPOLOGIA_PROTOCOLLO_USCITA);

			protocolliOutput.add(protocolloType);
		}

		// dati protocollo red
		protocolloType = new ProtocolloType();
		protocolloType.setNumeroProtocollo(fadDTO.getNumeroProtocollo());

		// Se processo è manuale
		if (processoManuale) {
			try {
				protocolloType.setData(DateUtils.buildXmlGregorianCalendarFromDate(fadDTO.getDataProtocolloFromManuale()));
			} catch (final Exception e) {
				LOGGER.error("Errore nella formattazione della data del protocollo from manuale", e);
			}
		} else {
			try {
				protocolloType.setData(DateUtils.buildXmlGregorianCalendarFromDate(DateUtils.parseDate(fadDTO.getDataProtocollo())));
			} catch (final Exception e) {
				LOGGER.error("Errore nella formattazione della data del protocollo", e);
			}
		}

		protocolloType.setAoo(aooToSend);
		protocolloType.setProvenienza(ProvenienzaType.RED.toString());
		protocolloType.setTipo(Constants.Protocollo.TIPOLOGIA_PROTOCOLLO_INGRESSO);
		protocolliOutput.add(protocolloType);

		return protocolliOutput;
	}

	/**
	 * Recupero dei metadati del fascicolo necessari per la creazione della raccolta
	 * FAD.
	 * 
	 * @param fadDTO
	 * @param utente
	 * @param processoManuale
	 * @param connection
	 * @return
	 */
	private FascicoloMetadataRaccoltaProvvisoriaType getMetadatiFascicoloForRequest(final RaccoltaFadDTO fadDTO, final UtenteDTO utente, final Boolean processoManuale,
			final Connection inConnection) {
		final FascicoloMetadataRaccoltaProvvisoriaType metadatiFascicoloRequest = new FascicoloMetadataRaccoltaProvvisoriaType();

		final String tipologiaDocLabel = TipologiaDocumentoEnum.ATTO_DECRETO.getDescrizione().toUpperCase();
		final String idFascicolo = fadDTO.getPrefixFascicoloLabel();

		String tipologiaProcLabel = null;
		String amministrazioneLabel = null;
		String ragioneriaLabel = null;
		String aooToSend = null;
		String idNodoCreatore = null;
		String descrizioneNodoCreatore = null;
		String idUtenteCreatore = null;
		String descrizioneUtenteCreatore = null;

		Connection connection = inConnection;
		try {
			if (connection == null) {
				connection = setupConnection(getDataSource().getConnection(), false);
			}

			aooToSend = getAooToSend(utente, connection);

			if (Boolean.TRUE.equals(processoManuale)) {
				amministrazioneLabel = fadDTO.getAmministrazioneFadLabel();
				ragioneriaLabel = fadDTO.getRagioneriaFadLabel();
			} else {

				amministrazioneLabel = getLabelAmministrazione(fadDTO, connection);
				ragioneriaLabel = getLabelRagioneria(fadDTO, connection);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di apertura della connessione al db", e);
			throw new RedException("Errore in fase di apertura della connessione al db", e);
		}

		// codice descrizione Amministrazione
		final CodiceDescrizioneType codDescAmministrazione = new CodiceDescrizioneType();
		codDescAmministrazione.setDescrizione(amministrazioneLabel);
		codDescAmministrazione.setCodice(fadDTO.getAmministrazioneFad());
		metadatiFascicoloRequest.setAmministrazione(codDescAmministrazione);

		// codice descrizione ragioneria
		final CodiceDescrizioneType codDescRagioneria = new CodiceDescrizioneType();
		codDescRagioneria.setCodice(fadDTO.getRagioneriaFad());
		codDescRagioneria.setDescrizione(ragioneriaLabel);
		metadatiFascicoloRequest.setRagioneria(codDescRagioneria);

		// Se processo è manuale
		if (Boolean.TRUE.equals(processoManuale)) {
			// Codice descrizione Nodo
			idNodoCreatore = utente.getIdUfficio().toString();
			descrizioneNodoCreatore = utente.getNodoDesc();
			// tipo Flusso
			metadatiFascicoloRequest.setTipoFlusso(TipoFlussoType.MANUALE);
			// tipologia procedimento label
			tipologiaProcLabel = Constants.Varie.TIPOLOGIA_PROC_PROCESSO_MANUALE_ATTO_DECRETO_LABEL;
		} else {
			// Codice Descrizione Nodo
			idNodoCreatore = fadDTO.getIdNodoDirigente();
			descrizioneNodoCreatore = fadDTO.getDescrizioneNodo();
			// tipo Flusso
			metadatiFascicoloRequest.setTipoFlusso(TipoFlussoType.AUTOMATICO);
			// tipologia procedimento label
			tipologiaProcLabel = Constants.Varie.TIPOLOGIA_PROC_PROCESSO_AUTOMATICO_ATTO_DECRETO_LABEL;
		}

		final List<ProtocolloType> protocolli = getProtocolliFAD(processoManuale, aooToSend, fadDTO);
		for (final ProtocolloType protocolloType : protocolli) {
			metadatiFascicoloRequest.getProtocollo().add(protocolloType);
		}

		// Codice descrizione utente
		idUtenteCreatore = utente.getId().toString();
		descrizioneUtenteCreatore = utente.getUsername();
		final CodiceDescrizioneType codDescUtente = new CodiceDescrizioneType();
		codDescUtente.setCodice(idUtenteCreatore);
		codDescUtente.setDescrizione(descrizioneUtenteCreatore);
		metadatiFascicoloRequest.setUtenteCreatore(codDescUtente);

		// Codice descrizione Nodo
		final CodiceDescrizioneType codDescNodo = new CodiceDescrizioneType();
		codDescNodo.setCodice(idNodoCreatore);
		codDescNodo.setDescrizione(descrizioneNodoCreatore);
		metadatiFascicoloRequest.setUfficioCreatoreRED(codDescNodo);

		// identificativo raccoltaProvvisoria
		String identificaticoRp = "";
		identificaticoRp = idFascicolo + "_" + tipologiaDocLabel + "_" + tipologiaProcLabel;
		metadatiFascicoloRequest.setIdentificativoRaccoltaProvvisoria(identificaticoRp);

		metadatiFascicoloRequest.setStatoFascicoloRaccoltaProvvisoria(Constants.Varie.STATO_FASCICOLO_FEPA_INSERITO);
		return metadatiFascicoloRequest;
	}

	/**
	 * Recupera la label dell'amministrazione.
	 * 
	 * @param fadDTO
	 * @param amministrazioneLabel
	 * @param connection
	 * @return descrizione amministrazione
	 */
	private String getLabelAmministrazione(final RaccoltaFadDTO fadDTO, final Connection connection) {
		String amministrazioneLabel = null;

		try {
			final FadAmministrazioneDTO admin = fadDao.getAmministrazioneByCodiceAmministrazione(fadDTO.getAmministrazioneFad(), connection);
			amministrazioneLabel = admin.getDescrizioneAmministrazione();
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero della descrizione dell'amministrazione FAD", e);
			throw new RedException("Errore nel recupero della descrizione dell'amministrazione FAD", e);
		}
		return amministrazioneLabel;
	}

	/**
	 * Recupera la label della ragioneria.
	 * 
	 * @param fadDTO
	 * @param connection
	 * @return descrizione ragioneria
	 */
	private String getLabelRagioneria(final RaccoltaFadDTO fadDTO, final Connection connection) {
		String ragioneriaLabel = null;

		try {
			final FadRagioneriaDTO ragioneria = fadDao.getRagioneriaByCodiceRagioneria(fadDTO.getRagioneriaFad(), connection);
			ragioneriaLabel = ragioneria.getDescrizioneRagioneria();
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero della descrizione della ragioneria FAD", e);
			throw new RedException("Errore nel recupero della descrizione della ragioneria FAD", e);
		}
		return ragioneriaLabel;
	}

	/**
	 * @param utente
	 * @param connection
	 * @param aooToSend
	 * @return
	 */
	private String getAooToSend(final UtenteDTO utente, final Connection connection) {
		String aooToSend = "";
		try {
			final Aoo aoo = aooDao.getAoo(utente.getIdAoo(), connection);
			aooToSend = aoo.getCodiceAoo() + " - " + aoo.getDescrizione();
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dell'AOO", e);
			throw new RedException("Errore nel recupero dell'AOO", e);
		}
		return aooToSend;
	}

	/**
	 * Gestione della response con riposta OK.
	 * 
	 * @param idFascicoloRaccoltaProvvisoria
	 * @param fadDTO
	 * @param utente
	 * @param processoManuale
	 * @param fpeh
	 * @param connection
	 */
	private void gestioneResponseCreazioneOK(final String idFascicoloRaccoltaProvvisoria, final RaccoltaFadDTO fadDTO, final UtenteDTO utente, final Boolean processoManuale,
			final FilenetPEHelper fpeh) {
		IFilenetCEHelper fceh = null;
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			// se la classe documentale non è AttoDecreto, la modifico
			if (Boolean.TRUE.equals(processoManuale)
					&& !PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_CLASSNAME_FN_METAKEY).equals(fadDTO.getClasseDocumentale())) {
				updateDocumentClass(fceh, fadDTO.getDocumentTitle(), utente.getIdAoo());
			}
			final HashMap<String, Object> metadatiToUpdate = new HashMap<>();

			metadatiToUpdate.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_RACCOLTA_FAD_METAKEY), idFascicoloRaccoltaProvvisoria);
			metadatiToUpdate.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.AMMINISTRAZIONE_FAD_METAKEY), fadDTO.getAmministrazioneFad());
			metadatiToUpdate.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.RAGIONERIA_FAD_METAKEY), fadDTO.getRagioneriaFad());
			metadatiToUpdate.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY),
					Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_TIPO_DOC_VALUE)));

			if (Boolean.TRUE.equals(processoManuale)) {
				metadatiToUpdate.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY),
						Long.parseLong(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_MANUALE_VALUE)));
			} else {
				metadatiToUpdate.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY),
						Long.parseLong(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_AUTOMATICO_VALUE)));
			}

			fceh.updateMetadatiOnCurrentVersion(utente.getIdAoo(), Integer.parseInt(fadDTO.getDocumentTitle()), metadatiToUpdate);
			// una volta aggiornati i metadati del documento tramite CE
			// aggiorno anche quelli del WF con i nuovi metadati raccolti utilizzando il PE
			if (fadDTO.getNewMetadati() == null) { // i metadati sono vuoti quando si arriva dal processo manuale
				fadDTO.setNewMetadati(new HashMap<>());
			}
			fadDTO.getNewMetadati().put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_RACCOLTA_PROVVISORIA_METAKEY), idFascicoloRaccoltaProvvisoria);
			if (Boolean.TRUE.equals(processoManuale)) {
				final VWWorkObject wo = fpeh.getWorkflowPrincipale(fadDTO.getDocumentTitle(), utente.getFcDTO().getIdClientAoo());
				// Aggiornamento del workflow
				fpeh.updateWorkflow(wo, fadDTO.getNewMetadati());
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la gestione della response 'OK' ottenuta da DEMBIL e del relativo aggiornamento dei matadati", e);
			throw new RedException("Errore durante la gestione della response 'OK' ottenuta da DEMBIL e del relativo aggiornamento dei matadati", e);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * aggiornamento della classe documentale nel caso di processo manuale.
	 * 
	 * @param fceh
	 * @param documentTitle
	 * @param idAoo
	 */
	private static void updateDocumentClass(final IFilenetCEHelper fceh, final String documentTitle, final Long idAoo) {
		try {
			final HashMap<String, Object> newMetadatiDocumento = new HashMap<>();
			newMetadatiDocumento.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY),
					Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_TIPO_DOC_VALUE)));
			newMetadatiDocumento.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY),
					Long.parseLong(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_MANUALE_VALUE)));

			fceh.cambiaClasseDocumentale(documentTitle, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_CLASSNAME_FN_METAKEY),
					newMetadatiDocumento, null, idAoo);
		} catch (final Exception e) {
			LOGGER.error("Errore nella modifica della classe documentale per il processo manuale di creazione Raccolta FAD", e);
			throw new RedException("Errore nella modifica della classe documentale per il processo manuale di creazione Raccolta FAD", e);
		}
	}

	/**
	 * Invocazione WS per aggiornamento dati Fascicolo Raccolta Provvisoria.
	 * 
	 * @param idFascicoloRaccoltaProvvisoria
	 * @param processoManuale
	 * @param utente
	 * @param idNodoDirigente
	 * @param descNodoDirigente
	 * @param fadDto
	 * @return
	 */
	@Override
	public Boolean updateCreatoreRaccoltaProvvisoria(final RaccoltaFadDTO fadDto, final String idFascicoloRaccoltaProvvisoria, final Boolean processoManuale,
			final UtenteDTO utente, final String idNodoDirigente, final String descNodoDirigente) {
		Boolean modificato = false;

		try {
			final InterfacciaRaccoltaProvvisoriaDEMBIL stub = AttoDecretoProxy.newInstance(
					WebServiceClientProvider.getIstance().getInterfacciaRaccoltaProvvisoriaDEMBILClient(),
					new AttoDecretoMonitoring(fadDto, utente, idFascicoloRaccoltaProvvisoria));
			final AccessoApplicativo accessoApplicativo = WebServiceClientProvider.getIstance().getAccessoInterfacciaRaccoltaProvvisoriaDEMBILClient();

			// Raccolta metadati Fascicolo
			final FascicoloMetadataRaccoltaProvvisoriaType metadatiFascicolo = getFascicoloRaccoltaProvvisoria(idFascicoloRaccoltaProvvisoria, stub, accessoApplicativo);

			if (Boolean.TRUE.equals(processoManuale)) {
				metadatiFascicolo.getUfficioCreatoreRED().setCodice(utente.getIdUfficio().toString());
				metadatiFascicolo.getUfficioCreatoreRED().setDescrizione(utente.getNodoDesc());
			} else {
				metadatiFascicolo.getUfficioCreatoreRED().setCodice(idNodoDirigente);
				metadatiFascicolo.getUfficioCreatoreRED().setDescrizione(descNodoDirigente);
			}

			metadatiFascicolo.getUtenteCreatore().setCodice(utente.getId().toString());
			metadatiFascicolo.getUtenteCreatore().setDescrizione(utente.getUsername());

			// Imposto il GUID
			final RichiestaModifyMetadataFascicoloRaccoltaProvvisoriaType datiRequest = new RichiestaModifyMetadataFascicoloRaccoltaProvvisoriaType();
			datiRequest.setFascicoloMetadata(metadatiFascicolo);
			datiRequest.setIdFascicoloRaccoltaProvvisoria(idFascicoloRaccoltaProvvisoria);
			datiRequest.setAttivo(true);
			datiRequest.setCondivisibile(true);
			datiRequest.setDaInviare(true);

			// invocazione WS tramite lo stub
			final RispostaModifyMetadataFascicoloRaccoltaProvvisoriaType risposta = stub.modifyMetadatiFascicoloRaccoltaProvvisoria(accessoApplicativo, datiRequest);
			final AttoDecretoParsedResponse response = AttoDecretoProxy.parseResponse(risposta, Integer.parseInt(fadDto.getDocumentTitle()));
			modificato = response.getEsito() > 0;

		} catch (final Exception e) {
			// RED non lancia eccezione vedi UtilityFepaService
			// updateCreatoreRaccoltaProvvisoria
			LOGGER.error("Errore durante l aggiornamento della raccolta provvisoria", e);

		}
		return modificato;
	}

	/**
	 * Invocazione WS per il recupero del Fascicolo.
	 * 
	 * @param idFascicoloRaccoltaProvvisoria
	 * @param stub
	 * @param accessoApplicativo
	 * @return
	 */
	private static FascicoloMetadataRaccoltaProvvisoriaType getFascicoloRaccoltaProvvisoria(final String idFascicoloRaccoltaProvvisoria,
			final InterfacciaRaccoltaProvvisoriaDEMBIL stub, final AccessoApplicativo accessoApplicativo) {
		RispostaGetFascicoloRaccoltaProvvisoria responseFascicolo = new RispostaGetFascicoloRaccoltaProvvisoria();

		try {
			// raccolta dati per la request
			final RichiestaGetFascicoloRaccoltaProvvisoriaType requestRaccolta = new RichiestaGetFascicoloRaccoltaProvvisoriaType();
			requestRaccolta.setIdFascicoloRaccoltaProvvisoria(idFascicoloRaccoltaProvvisoria);
			requestRaccolta.setTipoEstrazioneFascicolo(TipoEstrazioneType.METADATA);

			// invoco attrvesrso lo stub il WS
			responseFascicolo = stub.getFascicoloRaccoltaProvvisoria(accessoApplicativo, requestRaccolta);

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del Fascicolo per l'aggiornamento della raccolta provvisoria", e);
			throw new RedException("Errore durante il recupero del Fascicolo per l'aggiornamento della raccolta provvisoria", e);
		}
		return responseFascicolo.getDettaglioFascicolo().getDatiFascicolo();
	}

	/**
	 * 
	 * @param fadDto
	 * @param utente
	 * @param processoManuale
	 * @param connection
	 */
	@Override
	public void addDocumentoFascicoloRaccoltaProvvisoria(final RaccoltaFadDTO fadDto, final UtenteDTO utente, final Boolean processoManuale, final Connection connection) {
		String aooToSend = "";
		Document document = null;
		IFilenetCEHelper fceh = null;

		try {
			final String idFascicoloRaccoltaProvvisoria = fadDto.getIdFascicoloRaccoltaProvvisoria();

			if (StringUtils.isNotBlank(idFascicoloRaccoltaProvvisoria)) {
				aooToSend = getAooToSend(utente, connection);

				RaggruppamentoDocumenti gruppo = null;
				fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

				// aggiungo la versione corrente del documento principale come Doc principale
				// della raccolta
				document = fceh.getDocumentByIdGestionale(fadDto.getDocumentTitle(), null, null, utente.getIdAoo().intValue(), null, null);
				if (document != null) {

					fadDto.setNumeroProtocollo("" + document.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY)));
					fadDto.setAnnoProtocollo("" + document.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY)));
					fadDto.setDataProtocollo(
							DateUtils.dateToString(document.getProperties().getDateTimeValue(pp.getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY)), true));

					gruppo = new RaggruppamentoDocumenti("GR_DOCPRINC", "Documento principale", (short) 3, (short) 1);
					addDocumentoFascicoloRaccoltaProvvisoria(document, fadDto, processoManuale, true, idFascicoloRaccoltaProvvisoria, aooToSend, gruppo, utente);
				}

				// aggiungo la prima versione del documento come allegato
				document = fceh.getFirstVersionById(fadDto.getDocumentTitle(), utente.getIdAoo().intValue());
				if (document != null) {
					gruppo = new RaggruppamentoDocumenti("GR_DOCPRINC", "Documento principale", (short) 3, (short) 2);
					addDocumentoFascicoloRaccoltaProvvisoria(document, fadDto, processoManuale, false, idFascicoloRaccoltaProvvisoria, aooToSend, gruppo, utente);
				}

				// gestione allegati
				if (document != null) {
					final Collection<Document> docAllegati = fceh.getAllegatiConContentFirstVersion(document);
					if (docAllegati != null && !docAllegati.isEmpty()) {

						String nomeAllegato = null;
						short numArchivi = 0;
						for (final Document allegato : docAllegati) {
							nomeAllegato = allegato.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));
							if (isArchivioDembil(nomeAllegato)) {
								numArchivi++;
							}
						}

						int contaGruppoArchivio = 1;
						short numGruppoArchivio = 4;
						final short numGruppoNonArchivio = (short) (numGruppoArchivio + numArchivi);
						short numDocNonArchivio = 1;
						for (final Document allegato : docAllegati) {
							nomeAllegato = allegato.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));
							if (isArchivioDembil(nomeAllegato)) {
								gruppo = new RaggruppamentoDocumenti("GR_ALL_ARCH_" + contaGruppoArchivio, "Archivio " + contaGruppoArchivio, numGruppoArchivio, (short) 1);
								contaGruppoArchivio++;
								numGruppoArchivio++;
							} else if (nomeAllegato.equals(Constants.Varie.REPORT_PROTOCOLLI_PDF_FILENAME)) {
								gruppo = new RaggruppamentoDocumenti("GR_REPORT", "Report Protocolli", (short) 2, (short) 1);
							} else {
								gruppo = new RaggruppamentoDocumenti("GR_ALL", "Allegati", numGruppoNonArchivio, numDocNonArchivio);
								numDocNonArchivio++;
							}
							addDocumentoFascicoloRaccoltaProvvisoria(allegato, fadDto, processoManuale, false, idFascicoloRaccoltaProvvisoria, aooToSend, gruppo, utente);
						}

					}
				}

				// gestione email originale
				if (Boolean.FALSE.equals(processoManuale)) {
					gruppo = new RaggruppamentoDocumenti("GR_EMLORIG", "Email originale", (short) 1, (short) 1);
					addMailOriginaleFascicoloRaccoltaProvvisoria(fadDto, processoManuale, idFascicoloRaccoltaProvvisoria, aooToSend, gruppo, utente);
				}

				// cambio lo stato del fascicolo
				final RispostaChangeStatoFascicoloRaccoltaProvvisoriaType risposta = changeStatoFascicoloRaccoltaProvvisoria(fadDto, utente, idFascicoloRaccoltaProvvisoria,
						StatoFascicoloDocumentaleType.APERTO);
				final AttoDecretoParsedResponse response = AttoDecretoProxy.parseResponse(risposta, Integer.parseInt(fadDto.getDocumentTitle()));
				// se processo manuale mando il documento nella coda in sospeso
				if (Boolean.TRUE.equals(processoManuale) && response.getEsito() > 0) {
					inviaDocumentoInSospeso(fadDto, utente, processoManuale);
				}
			}
		} catch (final Exception e) {
			// NSD non lancia eccezione, lasciare cosi
			LOGGER.error("Attenzione, errore nell'inserimento del documento o dell'allegato nel fascicolo fad", e);
		} finally {
			popSubject(fceh);
		}
	}

	private class RaggruppamentoDocumenti {

		/**
		 * Codice gruppo.
		 */
		private final String codiceGruppo;

		/**
		 * Descriione gruppo.
		 */
		private final String descrizioneGruppo;

		/**
		 * Posizione gruppo.
		 */
		private final Short posizioneGruppo;

		/**
		 * Posizione documento.
		 */
		private final Short posizioneDocumento;

		/**
		 * Costruttore dell innter class che gestisce i raggruppamenti.
		 * 
		 * @param codiceGruppo
		 * @param descrizioneGruppo
		 * @param posizioneGruppo
		 * @param posizioneDocumento
		 */
		RaggruppamentoDocumenti(final String codiceGruppo, final String descrizioneGruppo, final Short posizioneGruppo, final Short posizioneDocumento) {
			super();
			this.codiceGruppo = codiceGruppo;
			this.descrizioneGruppo = descrizioneGruppo;
			this.posizioneGruppo = posizioneGruppo;
			this.posizioneDocumento = posizioneDocumento;
		}

		/**
		 * Restituisce il codice gruppo.
		 * 
		 * @return codice gruppo
		 */
		public String getCodiceGruppo() {
			return codiceGruppo;
		}

		/**
		 * Restituisce la descrizione del gruppo.
		 * 
		 * @return descrizione gruppo
		 */
		public String getDescrizioneGruppo() {
			return descrizioneGruppo;
		}

		/**
		 * Restituisce la posizione del gruppo.
		 * 
		 * @return posizione gruppo
		 */
		public Short getPosizioneGruppo() {
			return posizioneGruppo;
		}

		/**
		 * Restituisce la posizione del documento.
		 * 
		 * @return posizione documento
		 */
		public Short getPosizioneDocumento() {
			return posizioneDocumento;
		}

	}

	private static boolean isArchivioDembil(final String nomeFile) {
		final String ext = FilenameUtils.getExtension(nomeFile).toLowerCase();
		return "zip".equals(ext) || "p7m".equals(ext) || "rar".equals(ext);
	}

	private void addMailOriginaleFascicoloRaccoltaProvvisoria(final RaccoltaFadDTO fadDto, final Boolean processoManuale, final String idFascicoloRaccoltaProvvisoria,
			final String aooToSend, final RaggruppamentoDocumenti gruppo, final UtenteDTO utente) {

		IFilenetCEHelper fceh = null;

		try {
			final String username = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.SYSTEM_ADMIN_USERNAME_KEY);

			fceh = FilenetCEHelperProxy.newInstance(username, utente.getFcDTO().getPassword(), utente.getFcDTO().getUri(), utente.getFcDTO().getStanzaJAAS(),
					utente.getFcDTO().getObjectStore(), utente.getIdAoo());

			String nomeEmail = Constants.Varie.FAD_EMAIL_ORIGINALE_PREFIX_FILENAME;
			String descrEmail = Constants.Varie.FAD_EMAIL_ORIGINALE_DESCR_PREFIX;
			nomeEmail = nomeEmail + fadDto.getNumeroProtocollo() + "del" + fadDto.getAnnoProtocollo() + ".eml";
			descrEmail = descrEmail + fadDto.getNumeroProtocollo() + " del " + fadDto.getAnnoProtocollo();

			final DocumentoAllegabileDTO emailOriginale = mailSRV.getMailOriginale(fadDto.getDocumentTitle(), nomeEmail, utente.getIdAoo(), fceh);

			if (emailOriginale != null) {
				addDocumentoFascicoloRaccoltaProvvisoria(emailOriginale.getGuid(), emailOriginale.getContenuto().getInputStream(), nomeEmail, fadDto, processoManuale, false,
						idFascicoloRaccoltaProvvisoria, aooToSend, gruppo, emailOriginale.getContentType(), emailOriginale.getDataDocumento(), utente);
			}

		} catch (final Exception e) {
			LOGGER.warn("Errore in fase di recupero della mail originale", e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}

	}

	private void addDocumentoFascicoloRaccoltaProvvisoria(final Document document, final RaccoltaFadDTO fadDTO, final Boolean processoManuale, final Boolean docPrincipale,
			final String guidFascicoloRaccoltaProvvisoria, final String aooToSend, final RaggruppamentoDocumenti raggruppamento, final UtenteDTO utente) {
		addDocumentoFascicoloRaccoltaProvvisoria(document.get_Id().toString(), document.accessContentStream(0),
				document.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY)), fadDTO, processoManuale, docPrincipale,
				guidFascicoloRaccoltaProvvisoria, aooToSend, raggruppamento, document.get_MimeType(), document.get_DateCreated(), utente);
	}

	/**
	 * 
	 * @param document
	 * @param fadDTO
	 * @param processoManuale
	 * @param docPrincipale
	 * @param stub
	 * @param accessoApplicativo
	 * @param guidFascicoloRaccoltaProvvisoria
	 * @param aooToSend
	 * @throws Exception
	 */
	private void addDocumentoFascicoloRaccoltaProvvisoria(final String guidDocumento, final InputStream contentIs, final String nomeFile, final RaccoltaFadDTO fadDTO,
			final Boolean processoManuale, final Boolean docPrincipale, final String guidFascicoloRaccoltaProvvisoria, final String aooToSend,
			final RaggruppamentoDocumenti raggruppamento, final String mimeType, final Date dataDocumento, final UtenteDTO utente) {
		try {

			final RichiestaAddDocumentoFascicoloRaccoltaProvvisoriaType requestAddDocToFascicolo = new RichiestaAddDocumentoFascicoloRaccoltaProvvisoriaType();

			// Guid Fascicolo
			requestAddDocToFascicolo.setIdFascicoloRaccoltaProvvisoria(guidFascicoloRaccoltaProvvisoria);

			// pulisco l'id dalle parentesi
			String guidDocumentoString = guidDocumento;
			guidDocumentoString = guidDocumentoString.replace("{", "");
			guidDocumentoString = guidDocumentoString.replace("}", "");
			requestAddDocToFascicolo.setIdDocumento(guidDocumentoString);

			// tipo documento
			final TipoDocumentoType tipoDocumento = new TipoDocumentoType();
			if (Boolean.TRUE.equals(docPrincipale)) {
				tipoDocumento.setCodice(Constants.Varie.TIPO_DOCUMENTO_FAD_PRINCIPALE);
				tipoDocumento.setDescrizione(Constants.Varie.TIPO_DOCUMENTO_FAD_PRINCIPALE);
			} else {
				if (nomeFile != null && nomeFile.equals(Constants.Varie.REPORT_PROTOCOLLI_PDF_FILENAME)) {
					tipoDocumento.setCodice(Constants.Varie.TIPO_DOCUMENTO_FAD_REPORT_PROTOCOLLI);
					tipoDocumento.setDescrizione(Constants.Varie.TIPO_DOCUMENTO_FAD_REPORT_PROTOCOLLI);
				} else if (nomeFile != null && nomeFile.startsWith(Constants.Varie.FAD_EMAIL_ORIGINALE_PREFIX_FILENAME)) {
					tipoDocumento.setCodice(Constants.Varie.TIPO_DOCUMENTO_FAD_EMAIL_ORIGINALE);
					tipoDocumento.setDescrizione(Constants.Varie.TIPO_DOCUMENTO_FAD_EMAIL_ORIGINALE);
				} else {
					tipoDocumento.setCodice(Constants.Varie.TIPO_DOCUMENTO_FAD_ALLEGATO);
					tipoDocumento.setDescrizione(Constants.Varie.TIPO_DOCUMENTO_FAD_ALLEGATO);
				}
			}
			requestAddDocToFascicolo.setTipoDocumento(tipoDocumento);

			// Da Inviare
			requestAddDocToFascicolo.setDaInviare(true);
			// Da Condividere
			requestAddDocToFascicolo.setCondivisibile(true);

			// Tipo Operazione
			final RichiestaOperazioneDocumentaleType tipoOperazione = new RichiestaOperazioneDocumentaleType();

			final boolean isP7M = nomeFile.toLowerCase().endsWith("p7m");
			if (!isP7M) {
				if (Boolean.TRUE.equals(processoManuale)) {
					tipoOperazione.setTipoOperazione(TipoOperazioneType.FIRMA_IMMAGINE);
				} else {
					tipoOperazione.setTipoOperazione(TipoOperazioneType.FIRMA_AUTOMATICA);
				}
				requestAddDocToFascicolo.setOperazione(tipoOperazione);
			}

			// Attivazione
			requestAddDocToFascicolo.setAttivo(true);

			if (contentIs != null || mimeType != null) {

				// Content del Documento
				final DocumentoContentType contenteDocument = new DocumentoContentType();
				contenteDocument.setIdDocumento(guidDocumentoString);
				contenteDocument.setContent(FileUtils.toDataHandler(contentIs));
				contenteDocument.setDescrizione("Documento/allegato raccolta provvisoria");
				contenteDocument.setFileName(nomeFile);
				contenteDocument.setMimeType(mimeType);
				requestAddDocToFascicolo.setDocumentoContent(contenteDocument);

				final DocumentoMetadataRaccoltaProvvisoriaType datiDocumento = new DocumentoMetadataRaccoltaProvvisoriaType();
				datiDocumento.setDataDocumento(DateUtils.buildXmlGregorianCalendarFromDate(dataDocumento));

				final List<ProtocolloType> protocolli = getProtocolliFAD(processoManuale, aooToSend, fadDTO);
				for (final ProtocolloType protocolloType : protocolli) {
					datiDocumento.getProtocollo().add(protocolloType);
				}

				requestAddDocToFascicolo.setDatiDocumento(datiDocumento);
			}

			// raggruppamento
			final RaggruppamentoType gruppo = new RaggruppamentoType();

			final CodiceDescrizioneType codiceRaggruppamento = new CodiceDescrizioneType();
			codiceRaggruppamento.setCodice(raggruppamento.getCodiceGruppo()); // codice del gruppo
			codiceRaggruppamento.setDescrizione(raggruppamento.getDescrizioneGruppo()); // descrizione del gruppo
			gruppo.setCodiceGruppo(codiceRaggruppamento);
			gruppo.setPosizioneGruppo(raggruppamento.getPosizioneGruppo()); // numero del gruppo
			gruppo.setPosizioneDocumento(raggruppamento.getPosizioneDocumento()); // numero del documento nel gruppo

			requestAddDocToFascicolo.setRaggruppamento(gruppo);

			// invoco il WS
			final InterfacciaRaccoltaProvvisoriaDEMBIL stub = AttoDecretoProxy.newInstance(
					WebServiceClientProvider.getIstance().getInterfacciaRaccoltaProvvisoriaDEMBILClient(),
					new AttoDecretoMonitoring(fadDTO, utente, guidFascicoloRaccoltaProvvisoria, nomeFile));
			final AccessoApplicativo accessoApplicativo = WebServiceClientProvider.getIstance().getAccessoInterfacciaRaccoltaProvvisoriaDEMBILClient();
			stub.addDocumentoFascicoloRaccoltaProvvisoria(accessoApplicativo, requestAddDocToFascicolo);
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		}
	}

	/**
	 * 
	 * @param guidFascicoloRaccoltaProvvisoria
	 * @param stub
	 * @param accessoApplicativo
	 * @param nuovoStatoFascicolo
	 * @return
	 */
	private static RispostaChangeStatoFascicoloRaccoltaProvvisoriaType changeStatoFascicoloRaccoltaProvvisoria(final RaccoltaFadDTO fadDto, final UtenteDTO utente,
			final String guidFascicoloRaccoltaProvvisoria, final StatoFascicoloDocumentaleType nuovoStatoFascicolo) {
		try {
			final RichiestaChangeStatoFascicoloRaccoltaProvvisoriaType requestChangeStato = new RichiestaChangeStatoFascicoloRaccoltaProvvisoriaType();
			requestChangeStato.setIdFascicoloRaccoltaProvvisoria(guidFascicoloRaccoltaProvvisoria);
			requestChangeStato.setStatoFascicoloDocumentale(nuovoStatoFascicolo);

			final InterfacciaRaccoltaProvvisoriaDEMBIL stub = AttoDecretoProxy.newInstance(
					WebServiceClientProvider.getIstance().getInterfacciaRaccoltaProvvisoriaDEMBILClient(),
					new AttoDecretoMonitoring(fadDto, utente, guidFascicoloRaccoltaProvvisoria));
			final AccessoApplicativo accessoApplicativo = WebServiceClientProvider.getIstance().getAccessoInterfacciaRaccoltaProvvisoriaDEMBILClient();
			return stub.changeStatoFascicoloRaccoltaProvvisoria(accessoApplicativo, requestChangeStato);
		} catch (final Exception e) {
			LOGGER.error("Cambio stato delFascicolo Non riuscito", e);
			throw new RedException("Cambio stato delFascicolo Non riuscito", e);
		}
	}

	/**
	 * 
	 * @param fadDTO
	 * @param utente
	 * @param processoManuale
	 */
	@Override
	public void inviaDocumentoInSospeso(final RaccoltaFadDTO fadDTO, final UtenteDTO utente, final Boolean processoManuale) {
		FilenetPEHelper fpeh = null;
		final HashMap<String, Object> metadati = new HashMap<>();
		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			if (Boolean.TRUE.equals(processoManuale)) {
				metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId().intValue());
				metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), utente.getIdUfficio().intValue());
			} else {
				metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY),
						Integer.parseInt(fadDTO.getIdUtenteDirigente()));
				metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), Integer.parseInt(fadDTO.getIdNodoDirigente()));
			}

			final VWWorkObject wo = fpeh.getWorkFlowByWob(fadDTO.getWobNumber(), true);
			fpeh.nextStep(wo, metadati, ResponsesRedEnum.METTI_IN_SOSPESO.getResponse());

		} catch (final Exception e) {
			LOGGER.error("Cambio stato del fascicolo non riuscito", e);
			throw new RedException("Cambio stato del fascicolo non riuscito", e);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IFepaSRV#getComboAmministrazioni(java.sql.Connection).
	 */
	@Override
	public List<FadAmministrazioneDTO> getComboAmministrazioni(final Connection con) {
		return fadDao.getAmministrazioni(con);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#getComboAmministrazioni().
	 */
	@Override
	public List<FadAmministrazioneDTO> getComboAmministrazioni() {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return fadDao.getAmministrazioni(connection);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#findRagioneriaByCodiceAmministrazione(java.lang.String).
	 */
	@Override
	public FadRagioneriaDTO findRagioneriaByCodiceAmministrazione(final String codiceAmministrazione) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return fadDao.findRagioneriaByCodiceAmministrazione(codiceAmministrazione, connection);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IFepaSRV#findRagioneriaByCodiceAmministrazione(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public FadRagioneriaDTO findRagioneriaByCodiceAmministrazione(final String codiceAmministrazione, final Connection con) {
		return fadDao.findRagioneriaByCodiceAmministrazione(codiceAmministrazione, con);
	}

	/************ Decreto ************************/
	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#associaDecreto(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.util.List).
	 */
	@Override
	public EsitoOperazioneDTO associaDecreto(final UtenteDTO utente, final String wobNumber, final List<String> wobNumbersDaAggiungere) {
		final List<String> wobNumbersDaRimuovere = new ArrayList<>();
		return associaDecretiOrModificaAssociazioni(utente, wobNumber, wobNumbersDaAggiungere, wobNumbersDaRimuovere, ResponsesRedEnum.ASSOCIA_DECRETI);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#modificaAssociazioni(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.util.List, java.util.List).
	 */
	@Override
	public EsitoOperazioneDTO modificaAssociazioni(final UtenteDTO utente, final String wobNumber, final List<String> wobNumbersDaAggiungere,
			final List<String> wobNumbersDaRimuovere) {
		return associaDecretiOrModificaAssociazioni(utente, wobNumber, wobNumbersDaAggiungere, wobNumbersDaRimuovere, ResponsesRedEnum.MODIFICA_ASSOCIAZIONI);
	}

	/*
	 * Se le condizioni non sono supportate non ritorno errore ma non faccio niente.
	 * 
	 * @param utente
	 * 
	 * @param wobNumber
	 * 
	 * @param wobNumbersDaAggiungere
	 * 
	 * @param wobNumbersDaRimuovere
	 * 
	 * @return
	 */
	private EsitoOperazioneDTO associaDecretiOrModificaAssociazioni(final UtenteDTO utente, final String wobNumber, final List<String> wobNumbersDaAggiungere,
			final List<String> wobNumbersDaRimuovere, final ResponsesRedEnum response) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			// recuperare il workflow
			final VWWorkObject workObject = fpeh.getWorkFlowByWob(wobNumber, true);

			// idDocumento, idFascicolo del workflow
			Integer idDocumentoDichiarazioneInteger = (Integer) TrasformerPE.getMetadato(workObject, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
			String idDocumentoDichiarazione = String.valueOf(idDocumentoDichiarazioneInteger);
			
			Integer idFascicoloDichiarazioneInteger = (Integer) TrasformerPE.getMetadato(workObject, pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY));
			String idFascicoloDichiarazione = idFascicoloDichiarazioneInteger.toString();
			
			//Imposto il numeroDocumento dell'esito operazione DTO
			List<String> listaParametri = new ArrayList<>(5);
			listaParametri.add(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY));
			final Document doc = fceh.getDocumentByIdGestionale(idDocumentoDichiarazione, listaParametri, null, utente.getIdAoo().intValue(), null,
					pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));
			final Integer numeroDocumento = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			esito.setIdDocumento(numeroDocumento);

			final Collection<DocumentoFascicoloDTO> docFascicoloDSR = fceh.getOnlyDocumentiRedFascicolo(Integer.parseInt(idFascicoloDichiarazione), utente.getIdAoo());
			// Per ogni document presi dalla lista di documentTitleDAAggiungere
			// fascicolare (idfasdcicolo con docAttuale)
			// fascicolare (fascicolo attuale con idDoc)
			final Collection<String> documentTitleIds = new ArrayList<>(wobNumbersDaAggiungere.size());
			if (!CollectionUtils.isEmpty(wobNumbersDaAggiungere)) {
				final VWRosterQuery vwrq = fpeh.getDocumentWorkFlowsByWobNumbers(wobNumbersDaAggiungere);
				final Collection<PEDocumentoFEPADTO> workFlowsDaAggiungere = TrasformPE.transform(vwrq, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO_FEPA);

				for (final PEDocumentoFEPADTO wfDecreto : workFlowsDaAggiungere) {
					if (!isSelezionato(wfDecreto, wobNumber) || ResponsesRedEnum.ASSOCIA_DECRETI == response) {
						// Prevengo eventuali inconsistenze, se ho selezionato associa vuol dire che non
						// ho decreti associati,
						// potrebbe essere rimasto in stato inconsistente da una precedente rimozione
						documentTitleIds.add(wfDecreto.getIdDocumento());
						boolean fascicola = true;
						if (docFascicoloDSR != null) {
							for (final DocumentoFascicoloDTO docFascDSR : docFascicoloDSR) {
								if (docFascDSR.getDocumentTitle().equals(wfDecreto.getIdDocumento())) {
									fascicola = false;
									break;
								}
							}
						}

						if (fascicola) {
							fascicoloSRV.fascicolaSuFilenet(idFascicoloDichiarazione, Integer.parseInt(wfDecreto.getIdDocumento()), utente.getIdAoo(), fceh);
							fascicoloSRV.fascicolaSuFilenet(wfDecreto.getIdFascicolo().toString(), idDocumentoDichiarazioneInteger, utente.getIdAoo(), fceh);
						}
					}
				}
			}

			// fare lo stesso ma l'operazione inversa di undo per l'altra lista
			final Collection<String> documentTitleDaRimuovere = new ArrayList<>(wobNumbersDaRimuovere.size());
			if (!CollectionUtils.isEmpty(wobNumbersDaRimuovere)) {
				final VWRosterQuery vwrq = fpeh.getDocumentWorkFlowsByWobNumbers(wobNumbersDaRimuovere);
				final Collection<PEDocumentoFEPADTO> workFlowsDaAggiungere = TrasformPE.transform(vwrq, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO_FEPA);
				for (final PEDocumentoFEPADTO wfDecreto : workFlowsDaAggiungere) {
					if (isSelezionato(wfDecreto, wobNumber)) {
						documentTitleDaRimuovere.add(wfDecreto.getIdDocumento());

						disassociaDocumentoDaFascicolo(utente.getIdAoo(), fceh, idFascicoloDichiarazione, wfDecreto.getIdDocumento());
						disassociaDocumentoDaFascicolo(utente.getIdAoo(), fceh, wfDecreto.getIdFascicolo().toString(), idDocumentoDichiarazione);
					}
				}
			}

			if (ResponsesRedEnum.ASSOCIA_DECRETI == response && !CollectionUtils.isEmpty(documentTitleIds)) {
				final PEDocumentoFEPADTO wf = TrasformPE.transform(workObject, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO_FEPA);
				final List<String> finalArrayList = new ArrayList<>();
				final List<String> elencoDecretiFepa = wf.getElencoDecretiFepa() == null ? new ArrayList<>(0) : Arrays.asList(wf.getElencoDecretiFepa());
				finalArrayList.addAll(elencoDecretiFepa);
				finalArrayList.addAll(documentTitleIds);

				final String[] s = new String[finalArrayList.size()];

				final Map<String, Object> map = new HashMap<>();
				map.put(pp.getParameterByKey(PropertiesNameEnum.FEPA_ELENCO_DECRETI_WFKEY), finalArrayList.toArray(s));
				fpeh.nextStep(workObject, map, response.getResponse());
			} else if (ResponsesRedEnum.MODIFICA_ASSOCIAZIONI == response
					&& (!CollectionUtils.isEmpty(documentTitleIds) || !CollectionUtils.isEmpty(documentTitleDaRimuovere))) {
				final Map<String, Object> map = new HashMap<>();

				map.put(pp.getParameterByKey(PropertiesNameEnum.FEPA_DECRETI_DA_AGGIUNGERE_WF_METAKEY), documentTitleIds.toArray(new String[0]));

				map.put(pp.getParameterByKey(PropertiesNameEnum.FEPA_DECRETI_DA_ELIMINARE_WF_METAKEY), documentTitleDaRimuovere.toArray(new String[0]));

				fpeh.nextStep(workObject, map, response.getResponse());
			} else {
				throw new RedException("Associare o rimuovere almeno un decreto valido");
			}

			esito.setEsito(true);
			esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
		} catch (final FilenetException fileNet) {
			LOGGER.error("Attenzione, errore durante la response" + response.toString() + " con wobnumber " + wobNumber + ": errori fileNet", fileNet);
			esito.setNote("Impossibile eseguire l'operazione: errore durante il recupero dei decreti");
		} catch (final RedException e) {
			LOGGER.error(e.getMessage(), e);
			esito.setNote(e.getMessage());
		} catch (final Exception e) {
			LOGGER.error("Attenzione, errore durante la response" + response.toString() + " con wobnumber " + wobNumber, e);
			esito.setNote("Impossibile eseguire l'operazione.");
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}

		return esito;
	}

	/**
	 * Gestisce la disassociazione del documento dal fascicolo.
	 * 
	 * @param utente
	 * @param fceh
	 * @param idFascicolo
	 * @param wfDecreto
	 */
	private void disassociaDocumentoDaFascicolo(final Long idAoo, final IFilenetCEHelper fceh, final String idFascicolo, final String idDocumento) {
		try {
			fascicoloSRV.disassociaDocumentoFascicolo(idFascicolo, idDocumento, idAoo, fceh);
		} catch (final EngineRuntimeException e) {
			if (!"The specified containee is not filed in this folder.".equals(e.getMessage())) {
				throw e;
			} else {
				LOGGER.warn(e);
			}
		}
	}

	/*
	 * In fase di test questo metodo potrebbe rompersi perché non è chiaro in NSD
	 * cosa è il workflow.getWorkflowNumber Per ora assumiamo che sia il wobNumer
	 * 
	 * Il metodo verifica che il decreto sia associato al wobNumber selezionato
	 * 
	 * Mi aspetto che comunque siano tutte delle dichiarazioni servizi resi
	 * 
	 * @param wf
	 * 
	 * @param wobNumber
	 * 
	 * @return
	 */
	private static boolean isSelezionato(final PEDocumentoFEPADTO wf, final String wobNumber) {
		if (!wf.hasWobDichiarazioneServiziResi()) {
			throw new IllegalArgumentException(
					"Le dichiarazioni servizi resi non possono non avere valorizzato il metadato wobDichiarazioneServizi resi wobNUmber:" + wf.getWobNumber());
		}
		final List<String> wobDichiarazioniServiziResiList = wf.getWobDichiarazioneServiziResi() == null ? new ArrayList<>(0)
				: Arrays.asList(wf.getWobDichiarazioneServiziResi());
		return wobDichiarazioniServiziResiList.contains(wobNumber);
	}

	@Override
	public final EsitoOperazioneDTO invioDecretoInFirma(final UtenteDTO utente, final String wobNumber, final Long idUfficioFirma, final Long idUtenteFirma,
			final String oggetto) {
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		Connection connection = null;
		Integer numeroDocumento = 0;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			final VWWorkObject workObject = fpeh.getWorkFlowByWob(wobNumber, true);
			final Integer idDocumento = (Integer) TrasformerPE.getMetadato(workObject, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
			final String idDocumentoString = String.valueOf(idDocumento);

			Document decreto = fceh.getDocumentByDTandAOO(idDocumentoString, utente.getIdAoo());

			final Map<String, Object> metadati = new HashMap<>(2);
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), oggetto);

			// in caso il tipo procedimento non sia valorizzato, popolarlo con il tipo
			// procedimento di default per la tipologia documentale
			Integer idTipoProcedimento = (Integer) TrasformerCE.getMetadato(decreto, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);
			if (idTipoProcedimento == null || idTipoProcedimento == 0) {
				connection = setupConnection(getDataSource().getConnection(), false);
				final Integer idTipoDocumento = (Integer) TrasformerCE.getMetadato(decreto, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
				idTipoProcedimento = (int) tipoProcedimentoDAO.getTipiProcedimentoByTipologiaDocumento(connection, idTipoDocumento).get(0).getTipoProcedimentoId();
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY), idTipoProcedimento);
			}

			// ### Si aggiorna l'oggetto aggiornando il relativo metadato nel CE
			fceh.updateMetadatiOnCurrentVersion(utente.getIdAoo(), idDocumento, metadati);

			// ### Si controlla che i documenti aggiuntivi siano associati almeno ad un
			// ordine di pagamento (OP), altrimenti bisogna associarli a tutti gli OP del
			// decreto.
			decreto = fceh.getDocumentByDTandAOO(idDocumentoString, utente.getIdAoo());
			numeroDocumento = (Integer) TrasformerCE.getMetadato(decreto, pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY));

			final DocumentSet allegatiFilenet = fceh.getAllegatiByGuid(decreto.get_Id(), null);

			if (allegatiFilenet != null && !allegatiFilenet.isEmpty()) {
				final Iterator<?> itAllegatiFilenet = allegatiFilenet.iterator();
				while (itAllegatiFilenet.hasNext()) {
					final Document allegatoFilenet = (Document) itAllegatiFilenet.next();
					// Si recupera la lista degli ordini di pagamento
					final LinkSet ordiniPagamento = (LinkSet) allegatoFilenet.getProperties().getIndependentObjectSetValue("listaOP");

					// Se la lista è vuota, si associa l'allegato agli OP
					if (ordiniPagamento != null && ordiniPagamento.isEmpty()) {
						fceh.associaAllegatoAOrdineDiPagamento(decreto, allegatoFilenet);
					}
				}
			}

			metadati.clear();
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.FEPA_ID_NODO_FIRMA_METAKEY), idUfficioFirma);
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.FEPA_ID_UTENTE_FIRMA_METAKEY), idUtenteFirma);

			// ### Si avanza il workflow
			final boolean esitoAvanzamentoOk = fpeh.nextStep(wobNumber, metadati, ResponsesRedEnum.INVIO_DECRETO_FIRMA.getResponse());

			if (esitoAvanzamentoOk) {
				esito = new EsitoOperazioneDTO(null, null, numeroDocumento, wobNumber);
				esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
			} else {
				final String errore = "Errore durante l'avanzamento del workflow: " + wobNumber + " per il documento: " + idDocumento + "con la response: "
						+ ResponsesRedEnum.INVIO_DECRETO_FIRMA.name();
				LOGGER.error(errore);
				throw new FilenetException(errore);
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'invio del decreto in firma per il workflow: " + wobNumber, e);
			esito.setNote("Si è verificato un errore durante l'invio del decreto in firma.");
			esito.setEsito(false);
			esito.setIdDocumento(numeroDocumento);
		} finally {
			logoff(fpeh);
			popSubject(fceh);
			closeConnection(connection);
		}

		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#getDecretiDaLavorare(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<DecretoDirigenzialeDTO> getDecretiDaLavorare(final String selectedDSRWFNumber, final UtenteDTO utente) {
		final List<DecretoDirigenzialeDTO> decretiDirigenzialiRED = new ArrayList<>();
		IFilenetCEHelper fceh = null;
		Connection connection = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final VWQueueQuery query = (VWQueueQuery) listaDocSRV.getFepaQueryObject(DocumentQueueEnum.DD_DA_LAVORARE, null, utente);
			connection = setupConnection(getDataSource().getConnection(), false);

			while (query.hasNext()) {
				final VWWorkObject wo = (VWWorkObject) query.next();

				if (wo.hasFieldName(pp.getParameterByKey(PropertiesNameEnum.FEPA_DICHIARAZIONE_SERVIZI_RESI_WFKEY))) {
					final Document doc = fceh.getDocumentByIdGestionale(wo.getFieldValue(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY)).toString(), null,
							null, Integer.parseInt(utente.getIdAoo() + ""),
							pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY) + " <> "
									+ pp.getParameterByKey(PropertiesNameEnum.ID_TIPOLOGIA_DECRETO_IVA),
							pp.getParameterByKey(PropertiesNameEnum.DECRETO_DIRIGENZIALE_FEPA_CLASSNAME_FN_METAKEY));

					if (doc != null) {
						final DecretoDirigenzialeDTO decretoDirigenzialeRED = TrasformCE.transform(doc, TrasformerCEEnum.FROM_DOCUMENTO_TO_DD, connection);
						decretoDirigenzialeRED.setWobNumber(wo.getWorkObjectNumber());
						decretoDirigenzialeRED.setIdFascicoloProcedimentale(wo.getFieldValue(pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY)).toString());
						final String[] wobDichiarazioneServiziResi = (String[]) wo
								.getFieldValue(pp.getParameterByKey(PropertiesNameEnum.FEPA_DICHIARAZIONE_SERVIZI_RESI_WFKEY));
						if (contains(wobDichiarazioneServiziResi, selectedDSRWFNumber)) {
							decretoDirigenzialeRED.setSelected(true);
						}

						decretiDirigenzialiRED.add(decretoDirigenzialeRED);
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei decreti.", e);
			throw new RedException("Si è verificato un errore durante il recupero dei decreti");
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}

		return decretiDirigenzialiRED;
	}

	private static boolean contains(final String[] fieldValue, final String currentIdDichiarazione) {
		if (fieldValue == null || fieldValue.length == 0) {
			return false;
		}
		for (final String value : fieldValue) {
			if (value.equals(currentIdDichiarazione)) {
				return true;
			}
		}

		return false;
	}

	private OrdineDiPagamentoDTO restituisciOrdineDiPagamento(final CustomObject ordineP) {
		final OrdineDiPagamentoDTO op = new OrdineDiPagamentoDTO();
		op.setNumeroOP(ordineP.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_NUMERO_OP)));
		op.setIdFascicoloFEPA(ordineP.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA)));
		op.setAmministrazione(ordineP.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_AMMINISTRAZ)));
		op.setCodiceRagioneria(ordineP.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_CODICE_RAGIONERIA)));
		op.setCapitoloNumeriSIRGS(ordineP.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_CAPITOLO_NUMERI_SIRGS)));
		op.setTipoOp(ordineP.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_TIPO_OP)));
		op.setPianoGestione(ordineP.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_PIANO_GESTIONE)));
		op.setBeneficiario(ordineP.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_BENEFICIARIO)));
		op.setEsercizio(ordineP.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_ESERCIZIO)));
		final Date date = ordineP.getProperties().getDateTimeValue(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_DATA_EMISSIONE_OP));
		if (date != null) {
			op.setDataEmissioneOP(date);
		}
		op.setStatoOP(ordineP.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_STATO_OP)));

		op.setCustomObject(ordineP);
		return op;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#getOrdinePagamentoDecreto(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	public List<OrdineDiPagamentoDTO> getOrdinePagamentoDecreto(final UtenteDTO utente, final String documentTitleDecreto) {
		final List<OrdineDiPagamentoDTO> listaOrdinePagamentoDecreto = new ArrayList<>();
		// Load NUMERI OP
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final Document decreto = fceh.getDocumentByIdGestionale(documentTitleDecreto, null, null, Integer.parseInt("" + utente.getIdAoo()), null,
					pp.getParameterByKey(PropertiesNameEnum.DECRETO_DIRIGENZIALE_FEPA_CLASSNAME_FN_METAKEY));
			final CustomObject registroOrdinePagamento = (CustomObject) decreto.getProperties()
					.getObjectValue(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_OP_PROXY));

			OrdineDiPagamentoDTO ordinePagamento = null;
			IndependentObjectSet elencoOrdinePagamento = null;
			if (registroOrdinePagamento != null) {
				elencoOrdinePagamento = registroOrdinePagamento.getProperties().getIndependentObjectSetValue(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_ELENCO_OP));
				if (elencoOrdinePagamento != null) {
					for (final Iterator<?> iterator = elencoOrdinePagamento.iterator(); iterator.hasNext();) {
						final CustomObject ordineP = (CustomObject) iterator.next();
						ordinePagamento = restituisciOrdineDiPagamento(ordineP);
						listaOrdinePagamentoDecreto.add(ordinePagamento);
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero degli Ordini di Pagamento.", e);
			throw new RedException("Si è verificato un errore durante il recupero degli Ordini di Pagamento");
		} finally {
			popSubject(fceh);
		}

		return listaOrdinePagamentoDecreto;
	}

	private static AllegatoOPDTO restituisciAllegatoOP(final AllegatoDTO allegato) {
		final AllegatoOPDTO allegatoOP = new AllegatoOPDTO();
		allegatoOP.setIdDocumentoAllegatoOp(allegato.getDocumentTitle());
		allegatoOP.setTipologiaDocumento(allegato.getTipologia());
		allegatoOP.setOggetto(allegato.getOggetto());
		allegatoOP.setDataCreazione(allegato.getDateCreated());
		allegatoOP.setNomeDocumento(allegato.getNomeFile());
		allegatoOP.setDataHandler(allegato.getContent());
		allegatoOP.setGuid(allegato.getGuid());
		allegatoOP.setMimeType(allegato.getMimeType());
		return allegatoOP;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#getAllegatiOP(it.ibm.red.
	 *      business.dto.UtenteDTO, java.lang.String).
	 */
	@Override
	public List<AllegatoOPDTO> getAllegatiOP(final UtenteDTO utente, final String guid) {
		List<AllegatoOPDTO> allegatiOP = null;
		IFilenetCEHelper fceh = null;
		Connection connection = null;
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			connection = setupConnection(getDataSource().getConnection(), false);
			/**
			 * ALLEGATI START
			 ***************************************************************************/
			final DocumentSet dSet = fceh.getAllegatiByGuid(guid, null);
			if (!dSet.isEmpty()) {
				allegatiOP = new ArrayList<>();
				final Iterator<?> it = dSet.iterator();
				Document alleDocument = null;
				final Map<ContextTrasformerCEEnum, Object> context = tipologiaDocumentoSRV.getContextTipologieDocumento(CategoriaDocumentoEnum.ENTRATA.getIds()[0], utente,
						connection);
				while (it.hasNext()) {
					alleDocument = (Document) it.next();
					final AllegatoDTO allegatoDTO = TrasformCE.transform(alleDocument, TrasformerCEEnum.FROM_DOCUMENT_TO_ALLEGATO_CONTEXT, context, connection);
					final String tipologiaDoc = (String) TrasformerCE.getMetadato(alleDocument,
							PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FEPA_CODICE_TIPO_DOCUMENTO_METAKEY));
					allegatoDTO.setTipologia(tipologiaDoc);
					allegatiOP.add(restituisciAllegatoOP(allegatoDTO));
				}
			}
			/**
			 * ALLEGATI END
			 ****************************************************************************/
		} catch (final Exception e) {
			throw new RedException("Si è verificato un errore durante il recupero degli allegati all'ordine di pagamento.", e);
		} finally {
			popSubject(fceh);
			closeConnection(connection);
		}

		return allegatiOP;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#deleteAllegatiOP(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String).
	 */
	@Override
	public void deleteAllegatiOP(final UtenteDTO utente, final String idDecreto, final String guid) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final List<String> selectList = new ArrayList<>();
			selectList.add("ID");

			final Document decreto = fceh.getDocumentByIdGestionale(idDecreto, selectList, null, utente.getIdAoo().intValue(), null, null);
			fceh.deleteAllegatiByGuid(decreto.get_Id().toString(), Arrays.asList("{" + guid + "}"));

		} catch (final Exception e) {
			final String msg = "Attenzione, errore nell eliminazione degli allegati";
			LOGGER.error(msg, e);
			throw new RedException(msg);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#selectOPAllegatiAlDocAggiuntivo(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.util.List).
	 */
	@Override
	public void selectOPAllegatiAlDocAggiuntivo(final UtenteDTO utente, final String guidDocumentoAggiuntivo, final List<OrdineDiPagamentoDTO> ordiniDiPagamento) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			// resetto le checkbox
			for (final OrdineDiPagamentoDTO ordineDiPagamento : ordiniDiPagamento) {
				ordineDiPagamento.setSelected(false);
			}

			final Document allegato = fceh.getDocumentByGuid(guidDocumentoAggiuntivo);
			final LinkSet linksop = (LinkSet) allegato.getProperties().getIndependentObjectSetValue(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_LISTA_OP));
			for (final Iterator<?> iterator = linksop.iterator(); iterator.hasNext();) {
				final CustomObject customObjcetOPAllegato = (CustomObject) ((Link) iterator.next()).get_Tail();
				for (final OrdineDiPagamentoDTO ordineDiPagamento : ordiniDiPagamento) {
					if (customObjcetOPAllegato.get_Id().equals(ordineDiPagamento.getCustomObject().get_Id())) {
						ordineDiPagamento.setSelected(true);
						break;
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Attenzione: errore nell'eliminazione degli allegati.", e);
			throw new RedException("Si è verificato un errore nell'eliminazione degli allegati");
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#salvaAssociazioneAllegatoOP(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.List, java.lang.String).
	 */
	@Override
	public void salvaAssociazioneAllegatoOP(final UtenteDTO utente, final List<OrdineDiPagamentoDTO> ordiniDiPagamento, final String guidAllegato) {
		IFilenetCEHelper fceh = null;

		try {
			// controllo se esiste almeno un op associato al documento
			boolean almenoUnOpAssociato = false;
			for (final OrdineDiPagamentoDTO ordineDiPagamento : ordiniDiPagamento) {
				if (Boolean.TRUE.equals(ordineDiPagamento.getSelected())) {
					almenoUnOpAssociato = true;
				}
			}

			if (almenoUnOpAssociato) {
				fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
				final Document allegato = fceh.getDocumentByGuid(guidAllegato);
				final IndependentObjectSet pSet = fceh.getOrdiniPagamentoAllegatoOp(allegato.get_Id().toString());
				final Iterator<?> it = pSet.iterator();
				while (it.hasNext()) {
					final Link link = (Link) it.next();
					link.get_Name();
					link.delete();
					link.save(RefreshMode.REFRESH);
				}
				for (final OrdineDiPagamentoDTO ordineDiPagamento : ordiniDiPagamento) {
					if (ordineDiPagamento != null && !"Annullato".equals(ordineDiPagamento.getStatoOP()) && Boolean.TRUE.equals(ordineDiPagamento.getSelected())) {
						fceh.salvaOPAllegato(ordineDiPagamento.getCustomObject(), allegato);
					}
				}
			} else {
				throw new RedException("Associare almeno un OP al documento");
			}
		} catch (final RedException e) {
			LOGGER.error(e);
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Attenzione: errore nell'eliminazione degli allegati.", e);
			throw new RedException("Si è verificato un errore nell'eliminazione degli allegati");
		} finally {
			popSubject(fceh);
		}
	}

	private List<DocumentoFascicoloFepaDTO> getFascicoliFepa(final String idFascicolo, final TipoAccessoFascicoloFepaEnum tipoFascicoloFepa) {
		return getFascicoliFepa(false, idFascicolo, null, tipoFascicoloFepa, null, null).getListaDocumenti();
	}

	private FascicoloFEPAInfoDTO getFascicoliFepa(final Boolean fullDetail, final String idFascicolo, final String metadatoTipoFascicoloFepa,
			final TipoAccessoFascicoloFepaEnum tipoFascicoloFepa, final String codiceAoo, final String idFascicoloRiferimento) {
		final FascicoloFEPAInfoDTO output = new FascicoloFEPAInfoDTO();
		try {
			fepa.header.v3.AccessoApplicativo accesso = null;
			if (StringUtils.isNotBlank(codiceAoo)) {
				accesso = WebServiceClientProvider.getIstance().getAccessoInterfacciaFepa3Client(tipoFascicoloFepa.getTipoAccesso(), codiceAoo);
			} else {
				accesso = WebServiceClientProvider.getIstance().getAccessoInterfacciaFepa3Client(tipoFascicoloFepa.getTipoAccesso());
			}

			final InterfacciaFascicoli fepa = WebServiceClientProvider.getIstance().getInterfacciaFepa3Client();

			String tipoFascicoloFepaValue = tipoFascicoloFepa.getTipoFascicolo();
			// Per SICOGE il tipo fascicolo non è univoco, ma può assumere sei valori
			// differenti.
			// Il valore specifico viene recuperato dai metadati FileNet del fascicolo.
			if (StringUtils.isBlank(tipoFascicoloFepaValue)) {
				tipoFascicoloFepaValue = metadatoTipoFascicoloFepa;
			}

			final RichiestaGetFascicoloType richiesta = new RichiestaGetFascicoloType();
			richiesta.setIdFascicolo(idFascicolo);
			richiesta.setTipoFascicolo(tipoFascicoloFepaValue);
			if (idFascicoloRiferimento != null) {
				richiesta.setIdFascicoloRiferimento(idFascicoloRiferimento);
			}
			richiesta.getTipoEstrazione().add(TipoDettaglioEstrazioneFascicoloType.DOCUMENTI);
			richiesta.getTipoEstrazione().add(TipoDettaglioEstrazioneFascicoloType.METADATI_DOCUMENTO);
			if (Boolean.TRUE.equals(fullDetail)) {
				richiesta.getTipoEstrazione().add(TipoDettaglioEstrazioneFascicoloType.ELENCO_FASCICOLICONTENUTI);
				richiesta.getTipoEstrazione().add(TipoDettaglioEstrazioneFascicoloType.ELENCO_FASCICOLICONTENUTI_METADATI);
			}

			final RispostaGetFascicolo resp = fepa.getFascicolo(accesso, richiesta);

			if (resp.getEsito().equals(EsitoType.OK)) {
				output.fillDocumenti(idFascicolo, tipoFascicoloFepaValue, resp.getDettaglioFascicolo().getDocumenti(), idFascicoloRiferimento);
				if (Boolean.TRUE.equals(fullDetail)) {
					final FascicoloType fascicolo = resp.getDettaglioFascicolo();
					output.fillFascicoloPrincipale(fascicolo);
					for (final FascicoloBaseType f : fascicolo.getFascicoliContenuti()) {
						output.addFascicoloContenuto(f, fascicolo.getIdFascicolo());
					}
				}
			} else {
				logErrorResponse(resp, "getFascicoloFepa " + idFascicolo + DI_TIPO_LITERAL + tipoFascicoloFepa);
			}
		} catch (final Exception e) {
			LOGGER.error("Fascicolo FEPA non trovato", e);
			throw new RedException("Fascicolo FEPA non trovato");
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#getFascicoliOP(java.lang.String).
	 */
	@Override
	public List<DocumentoFascicoloFepaDTO> getFascicoliOP(final String idFascicolo) {
		return getFascicoliFepa(idFascicolo, TipoAccessoFascicoloFepaEnum.OP);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#getFascicoliFattura(java.lang.String).
	 */
	@Override
	public List<DocumentoFascicoloFepaDTO> getFascicoliFattura(final String idFascicolo) {
		return getFascicoliFepa(idFascicolo, TipoAccessoFascicoloFepaEnum.DOCUMENTO_CONTABILE);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#getFascicoliVerbaleRevisione(java.lang.String).
	 */
	@Override
	public List<DocumentoFascicoloFepaDTO> getFascicoliVerbaleRevisione(final String idFascicolo) {
		return getFascicoliFepa(idFascicolo, TipoAccessoFascicoloFepaEnum.VERBALE_REVISIONE);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#getFascicoliBilancioEnti(java.lang.String).
	 */
	@Override
	public List<DocumentoFascicoloFepaDTO> getFascicoliBilancioEnti(final String idFascicolo) {
		return getFascicoliFepa(idFascicolo, TipoAccessoFascicoloFepaEnum.BILANCIO_ENTI);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#getFascicoliSicoge(java.lang.Boolean,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String).
	 */
	@Override
	public FascicoloFEPAInfoDTO getFascicoliSicoge(final Boolean fullDetail, final String idFascicolo, final String tipoFascicoloFepaSicoge, final String codiceAoo,
			final String idFascicoloRiferimento) {
		return getFascicoliFepa(fullDetail, idFascicolo, tipoFascicoloFepaSicoge, TipoAccessoFascicoloFepaEnum.SICOGE, codiceAoo, idFascicoloRiferimento);
	}

	private FileDTO downloadDocumentoFascicolo(final String guidFascicolo, final String guidDocumento, final TipoAccessoFascicoloFepaEnum tipoFascicoloFepa) {
		return downloadDocumentoFascicolo(guidFascicolo, guidDocumento, null, tipoFascicoloFepa, null, null);
	}

	private FileDTO downloadDocumentoFascicolo(final String guidFascicolo, final String guidDocumento, final String tipoFascicolo,
			final TipoAccessoFascicoloFepaEnum tipoFascicoloFepa, final String codiceAoo, final String guidFascicoloRiferimento) {
		try {
			FileDTO retVal = null;

			fepa.header.v3.AccessoApplicativo accesso = null;
			if (StringUtils.isNotBlank(codiceAoo)) {
				accesso = WebServiceClientProvider.getIstance().getAccessoInterfacciaFepa3Client(tipoFascicoloFepa.getTipoAccesso(), codiceAoo);
			} else {
				accesso = WebServiceClientProvider.getIstance().getAccessoInterfacciaFepa3Client(tipoFascicoloFepa.getTipoAccesso());
			}

			final InterfacciaFascicoli fepa = WebServiceClientProvider.getIstance().getInterfacciaFepa3Client();

			// Per SICOGE il tipo fascicolo non è univoco, ma può assumere sei valori
			// differenti:
			// deve quindi essere recuperato dal documento in input.
			String tipoFascicoloValue = tipoFascicoloFepa.getTipoFascicolo();
			if (StringUtils.isBlank(tipoFascicoloValue)) {
				tipoFascicoloValue = tipoFascicolo;
			}

			LOGGER.info("BEGIN CALL: FEPA.downloadFile. IdDocumento: " + guidDocumento + DI_TIPO_LITERAL + tipoFascicoloValue);

			final RichiestaDownloadDocumentoFascicoloType richiestaDownload = new RichiestaDownloadDocumentoFascicoloType();
			richiestaDownload.setIdDocumento(guidDocumento);
			richiestaDownload.setIdFascicolo(guidFascicolo);
			richiestaDownload.setTipoFascicolo(tipoFascicoloValue);
			if (guidFascicoloRiferimento != null) {
				richiestaDownload.setIdFascicoloRiferimento(guidFascicoloRiferimento);
			}

			final RispostaDownloadDocumentoFascicolo downloadFileContentResponse = fepa.downloadDocumentoFascicolo(accesso, richiestaDownload);

			if (downloadFileContentResponse.getEsito().equals(EsitoType.OK)) {

				final fepa.types.v3.DocumentoContentType doc = downloadFileContentResponse.getDocumentoContent();

				final byte[] documentContent = doc.getContent();
				final String contentType = doc.getMimeType();
				String filename = null;
				if (documentContent != null) {
					filename = doc.getFileName();
				}

				retVal = new FileDTO(filename, documentContent, contentType);

			} else {

				logErrorResponse(downloadFileContentResponse, "download documento " + guidDocumento + DI_TIPO_LITERAL + tipoFascicoloValue);

			}

			LOGGER.info("END CALL: FEPA.downloadFile. IdDocumento: " + guidDocumento + DI_TIPO_LITERAL + tipoFascicoloValue);

			return retVal;

		} catch (final RedException e) {
			LOGGER.error(e);
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Attenzione, errore durante il download", e);
			throw new RedException("Errore durante il download");
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#downloadDocumentoFascicoloOP(java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public FileDTO downloadDocumentoFascicoloOP(final String guidFascicolo, final String guidDocumento) {
		return downloadDocumentoFascicolo(guidFascicolo, guidDocumento, TipoAccessoFascicoloFepaEnum.OP);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#downloadDocumentoFascicoloFattura(java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public FileDTO downloadDocumentoFascicoloFattura(final String guidFascicolo, final String guidDocumento) {
		return downloadDocumentoFascicolo(guidFascicolo, guidDocumento, TipoAccessoFascicoloFepaEnum.DOCUMENTO_CONTABILE);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#downloadDocumentoVerbaliRevisione(java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public FileDTO downloadDocumentoVerbaliRevisione(final String guidFascicolo, final String guidDocumento) {
		return downloadDocumentoFascicolo(guidFascicolo, guidDocumento, TipoAccessoFascicoloFepaEnum.VERBALE_REVISIONE);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#downloadDocumentoBilancioEnti(java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public FileDTO downloadDocumentoBilancioEnti(final String guidFascicolo, final String guidDocumento) {
		return downloadDocumentoFascicolo(guidFascicolo, guidDocumento, TipoAccessoFascicoloFepaEnum.BILANCIO_ENTI);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#downloadDocumentoSicoge(java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String).
	 */
	@Override
	public FileDTO downloadDocumentoSicoge(final String guidFascicolo, final String guidDocumento, final String tipoFascicolo, final String codiceAoo,
			final String guidFascicoloRiferimento) {
		return downloadDocumentoFascicolo(guidFascicolo, guidDocumento, tipoFascicolo, TipoAccessoFascicoloFepaEnum.SICOGE, codiceAoo, guidFascicoloRiferimento);
	}

	private static boolean associaProtocollo(final String idFascicolo, final List<String> idDocumenti, final String numeroProtocollo, final Date dataProtocollo,
			final TipoAccessoFascicoloFepaEnum tipoFascicoloFepa) {
		boolean result = true;
		try {
			fepa.types.v3.RispostaAssociaProtocolloType rapt = null;
			final fepa.types.v3.RichiestaAssociaProtocolloType richiesta = new fepa.types.v3.RichiestaAssociaProtocolloType();

			final fepa.types.v3.ProtocolloType protocollo = new fepa.types.v3.ProtocolloType();
			protocollo.setAoo("RGS");
			protocollo.setData(DateUtils.buildXmlGregorianCalendarFromDate(dataProtocollo));
			protocollo.setTipo("INGRESSO");
			protocollo.setNumeroProtocollo(numeroProtocollo);
			protocollo.setProvenienza("RED");

			richiesta.setProtocollo(protocollo);
			richiesta.setIdFascicolo(idFascicolo);
			richiesta.setTipoFascicolo(tipoFascicoloFepa.getTipoFascicolo());

			final fepa.header.v3.AccessoApplicativo accesso = WebServiceClientProvider.getIstance().getAccessoInterfacciaFepa3Client(tipoFascicoloFepa.getTipoAccesso());
			final InterfacciaFascicoli fepa = WebServiceClientProvider.getIstance().getInterfacciaFepa3Client();

			for (final String idDocumento : idDocumenti) {

				richiesta.setIdDocumento(idDocumento);
				rapt = fepa.associaProtocollo(accesso, richiesta);
				result &= rapt.getEsito().equals(EsitoType.OK);

				if (!rapt.getEsito().equals(EsitoType.OK)) {
					logErrorResponse(rapt, "associaProtocollo " + idFascicolo + "," + idDocumento + "," + numeroProtocollo + "," + tipoFascicoloFepa);
				}

			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante l'operazione associaProtocollo di Bilancio Enti", e);
			throw new RedException(e);
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#associaProtocolloVerbaleRevisione(java.lang.String,
	 *      java.util.List, java.lang.String, java.util.Date).
	 */
	@Override
	public boolean associaProtocolloVerbaleRevisione(final String idFascicolo, final List<String> idDocumenti, final String numeroProtocollo, final Date dataProtocollo) {
		return associaProtocollo(idFascicolo, idDocumenti, numeroProtocollo, dataProtocollo, TipoAccessoFascicoloFepaEnum.VERBALE_REVISIONE);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#associaProtocolloBilancioEnti(java.lang.String,
	 *      java.util.List, java.lang.String, java.util.Date).
	 */
	@Override
	public boolean associaProtocolloBilancioEnti(final String idFascicolo, final List<String> idDocumenti, final String numeroProtocollo, final Date dataProtocollo) {
		return associaProtocollo(idFascicolo, idDocumenti, numeroProtocollo, dataProtocollo, TipoAccessoFascicoloFepaEnum.BILANCIO_ENTI);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#getTrasmissione(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public TrasmissioneDTO getTrasmissione(final String id, final UtenteDTO utente) {
		TrasmissioneDTO trasmissione = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());

			final Document docMail = fceh.getMailAllegata(id);

			if (docMail != null) {
				trasmissione = new TrasmissioneDTO();

				final Properties props = docMail.getProperties();
				String inTesto = Constants.EMPTY_STRING;
				if (docMail.get_ContentElements() != null && !docMail.get_ContentElements().isEmpty()) {
					inTesto = new String(FileUtils.getByteFromInputStream(((ContentTransfer) docMail.get_ContentElements().get(0)).accessContentStream()));
					inTesto = new String(inTesto.replace("\\r", "").replace("\\n", "<br>").getBytes(StandardCharsets.UTF_8));
				}
				trasmissione.setCc(props.getStringValue(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_CC_EMAIL_METAKEY)));
				trasmissione.setDataRicezione(props.getDateTimeValue(pp.getParameterByKey(PropertiesNameEnum.DATA_RICEZIONE_MAIL_METAKEY)));
				trasmissione.setFrom(props.getStringValue(pp.getParameterByKey(PropertiesNameEnum.FROM_MAIL_METAKEY)));
				trasmissione.setOggetto(props.getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_EMAIL_METAKEY)));
				trasmissione.setTesto(inTesto);
				trasmissione.setTo(props.getStringValue(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY)));

				final List<AllegatoDTO> allegatiList = allegatoSRV.extractMinimalAllegati(docMail);
				trasmissione.getAllegati().addAll(allegatiList);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
		} finally {
			popSubject(fceh);
		}

		return trasmissione;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#mettiAgliAttiSysFineLavorazione(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public EsitoOperazioneDTO mettiAgliAttiSysFineLavorazione(final String wobNumber, final UtenteDTO utente) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;

		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			final VWWorkObject wo = fpeh.getWorkFlowByWob(wobNumber, true);

			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			// idDocumento, idFascicolo del workflow
			final Integer idDocumentoDichiarazioneInteger = (Integer) TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
			final String idDocumentoDichiarazione = String.valueOf(idDocumentoDichiarazioneInteger);

			// Imposto il numeroDocuemnto dell'esito operazione DTO
			final List<String> listaParametri = new ArrayList<>(5);
			listaParametri.add(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY));
			final Document doc = fceh.getDocumentByIdGestionale(idDocumentoDichiarazione, listaParametri, null, utente.getIdAoo().intValue(), null,
					pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));

			final Integer numeroDocumento = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			fpeh.nextStep(wo, null, "SYS_Fine Lavorazione");

			esito.setEsito(true);
			esito.setIdDocumento(numeroDocumento);
			esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
		} catch (final Exception e) {
			LOGGER.error("Errore nella messa agli atti.", e);
			throw new RedException("Si è verificato un errore nella messa agli atti");
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}

		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#ottieniElencoDecretiFepa(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public String[] ottieniElencoDecretiFepa(final String wobNumber, final UtenteDTO utente) {
		try {
			final FilenetPEHelper fpeh = new FilenetPEHelper(utente.getFcDTO());
			final VWWorkObject workObject = fpeh.getWorkFlowByWob(wobNumber, true);
			return (String[]) TrasformerPE.getMetadato(workObject, pp.getParameterByKey(PropertiesNameEnum.FEPA_ELENCO_DECRETI_WFKEY));

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei decreti associati", e);
			throw new RedException("Errore durante il recupero dei decreti associati ");
		}

	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#getTipologieDocumentiAggiuntivi().
	 */
	@Override
	public List<TipologiaDocumentoDTO> getTipologieDocumentiAggiuntivi() {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return tipologiaDocumentoDAO.getTipologieDocumentiAggiuntivi(connection);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}
	
	/**
	 * @param response
	 * @param methodName
	 */
	private static void logErrorResponse(final BaseServiceResponseType response, final String methodName) {
		logKOResponse(response, methodName, false);
	}
	
	/**
	 * @param response
	 * @param methodName
	 * @param warning
	 */
	private static void logKOResponse(final BaseServiceResponseType response, final String methodName, final boolean warning) {
		LOGGER.error("Errore durante la procedura di " + methodName + " tramite servizi FEPA");
		if (response != null && !CollectionUtils.isEmpty(response.getErrorList())) {
			for (ServiceErrorType set : response.getErrorList()) {
				if (!warning) {
					LOGGER.error("Errore FEPA3: " + set.getErrorMessageString());
				} else {
					LOGGER.warn("Errore FEPA3: " + set.getErrorMessageString());
				}
			}
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#showBilEntiFasc(java.lang.Integer).
	 */
	@Override
	public boolean showBilEntiFasc(final Integer idTipologiaDocumento) {
		return idTipologiaDocumento == Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.ID_TIPOLOGIA_BILANCIO_ENTI_ENTRATA));
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#showRevIGFFasc(java.lang.Integer).
	 */
	@Override
	public boolean showRevIGFFasc(final Integer idTipologiaDocumento) {
		return idTipologiaDocumento == Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.ID_TIPOLOGIA_REVISORI_IGF_ENTRATA));
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#mettiAgliAttiBilancioEnti(java.lang.String).
	 */
	@Override
	public boolean mettiAgliAttiBilancioEnti(final String idFascicolo) {
		boolean esito = false;
		try {
			if (idFascicolo != null) {

				final fepa.header.v3.AccessoApplicativo accesso = WebServiceClientProvider.getIstance()
						.getAccessoInterfacciaFepa3Client(TipoAccessoFascicoloFepaEnum.BILANCIO_ENTI.getTipoAccesso());
				final InterfacciaFascicoli fepa = WebServiceClientProvider.getIstance().getInterfacciaFepa3Client();

				final RichiestaChangeStatoFascicoloType richiesta = new RichiestaChangeStatoFascicoloType();
				richiesta.setIdFascicolo(idFascicolo);
				richiesta.setTipoFascicolo(TipoAccessoFascicoloFepaEnum.BILANCIO_ENTI.getTipoFascicolo());
				richiesta.setStatoFascicoloDocumentale(CHIUSO);
				final RispostaChangeStatoFascicoloType response = fepa.changeStatoFascicolo(accesso, richiesta);

				if (!response.getEsito().equals(EsitoType.OK)) {
					logErrorResponse(response, "mettiAgliAttiBilancioEnti " + idFascicolo);
				}

				esito = response.getEsito().equals(EsitoType.OK);

			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'operazione changeStatoFascicoloBilancioEnti", e);
			throw new RedException(e);
		}
		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#addDocumentoToFascicoloBilancioEnti(java.lang.String,
	 *      it.ibm.red.business.dto.DetailDocumentoDTO,
	 *      it.ibm.red.business.dto.ProtocolloDTO, java.lang.String, boolean,
	 *      java.lang.Integer).
	 */
	@Override
	public void addDocumentoToFascicoloBilancioEnti(final String idFascicolo, final DetailDocumentoDTO documentDetailDTO, final ProtocolloDTO protocollo,
			final String tipologiaProtocollo, final boolean firmato, final Integer idAoo) {

		addDocumentoToFascicolo(TipoAccessoFascicoloFepaEnum.BILANCIO_ENTI, idFascicolo, Constants.Varie.TIPO_DOCUMENTO_BILANCIO_ENTI_RISPOSTA,
				documentDetailDTO.getDataCreazione(), idAoo, protocollo.getDataProtocollo(), protocollo.getNumeroProtocollo(), tipologiaProtocollo,
				documentDetailDTO.getGuid(), documentDetailDTO.getContent(), documentDetailDTO.getNomeFile(), firmato, documentDetailDTO.getMimeType(),
				documentDetailDTO.getDocumentTitle(), null);

	}
	
	/**
	 * @param tipoFascicoloFepa
	 * @param idFascicolo
	 * @param tipoDocumentoString
	 * @param dataCreazione
	 * @param idAoo
	 * @param dataProtocollo
	 * @param numeroProtocollo
	 * @param tipologiaProtocollo
	 * @param guidDocumento
	 * @param contentDocumento
	 * @param nomeDocumento
	 * @param firmato
	 * @param mimeTypeDocumento
	 * @param documentTitle
	 * @param metadata
	 */
	private void addDocumentoToFascicolo(final TipoAccessoFascicoloFepaEnum tipoFascicoloFepa, final String idFascicolo, final String tipoDocumentoString,
			final Date dataCreazione, final Integer idAoo, final Date dataProtocollo, final Integer numeroProtocollo, final String tipologiaProtocollo,
			final String guidDocumento, final byte[] contentDocumento, final String nomeDocumento, final Boolean firmato, final String mimeTypeDocumento,
			final String documentTitle, final Object metadata) {

		try {
			final fepa.header.v3.AccessoApplicativo accesso = WebServiceClientProvider.getIstance().getAccessoInterfacciaFepa3Client(tipoFascicoloFepa.getTipoAccesso());
			final InterfacciaFascicoli fepa = WebServiceClientProvider.getIstance().getInterfacciaFepa3Client();

			final RichiestaAddDocumentoFascicoloType richiesta = new RichiestaAddDocumentoFascicoloType();
			richiesta.setIdFascicolo(idFascicolo);
			richiesta.setTipoFascicolo(tipoFascicoloFepa.getTipoFascicolo());

			// Tipo documento
			final fepa.types.v3.TipoDocumentoType tipoDocumento = new fepa.types.v3.TipoDocumentoType();
			tipoDocumento.setCodice(tipoDocumentoString);
			tipoDocumento.setDescrizione(tipoDocumentoString);
			richiesta.setTipoDocumento(tipoDocumento);

			// protocollo
			final DocumentoMetadataType documentoMetadataType = new DocumentoMetadataType();
			documentoMetadataType.setDataDocumento(DateUtils.buildXmlGregorianCalendarFromDate(dataCreazione));
			if (metadata != null) {
				documentoMetadataType.setMetadata(SerializationUtils.jaxbMarshall(metadata));
				documentoMetadataType.setVersioneMetadata(metadata.getClass().getPackage().getName());
			}

			if (dataProtocollo != null) {
				final fepa.types.v3.ProtocolloType protocolloType = new fepa.types.v3.ProtocolloType();
				protocolloType.setAoo("" + idAoo);
				protocolloType.setData(DateUtils.buildXmlGregorianCalendarFromDate(dataProtocollo));
				protocolloType.setNumeroProtocollo(Integer.toString(numeroProtocollo));
				protocolloType.setTipo(tipologiaProtocollo);
				documentoMetadataType.setProtocolloMittente(protocolloType);
			}

			richiesta.setDatiDocumento(documentoMetadataType);

			// Documento
			final Documento documento = new Documento();

			String cleanedGuid = null;
			if (guidDocumento != null) {
				cleanedGuid = it.ibm.red.business.utils.StringUtils.cleanGuidToString(guidDocumento);
			}

			documento.setIdDocumento(cleanedGuid);

			final fepa.types.v3.DocumentoContentType documentoContent = new fepa.types.v3.DocumentoContentType();
			documentoContent.setContent(contentDocumento);
			documentoContent.setDescrizione(cleanedGuid != null ? cleanedGuid : nomeDocumento);
			documentoContent.setFileName(nomeDocumento);
			if (firmato != null) {
				documentoContent.setFirmato(firmato);
			}
			documentoContent.setIdDocumento(cleanedGuid);
			documentoContent.setMimeType(mimeTypeDocumento);

			documento.setDocumentoContent(documentoContent);

			richiesta.setDocumento(documento);
			richiesta.setCondivisibile(true);
			richiesta.setAttivo(true);
			richiesta.setDaInviare(true);

			final RispostaAddDocumentoFascicolo risposta = fepa.addDocumentoFascicolo(accesso, richiesta);
			if (risposta != null && EsitoType.OK.equals(risposta.getEsito())) {

				String idDocumento = null;
				if (risposta.getDatiDocumento() != null) {
					idDocumento = risposta.getDatiDocumento().getIdDocumento();
				}
				LOGGER.info("###########  Documento " + idDocumento + " (documentTitle " + documentTitle + ") inserito con successo nel fascicolo fepa " + idFascicolo);
			} else if (risposta != null && !EsitoType.OK.equals(risposta.getEsito()) ) {
				// Gestione degli errori restituiti da FEPA che non devono sollevare eccezione
				boolean actualError = false;
				if (!CollectionUtils.isEmpty(risposta.getErrorList())) {
					for (ServiceErrorType e : risposta.getErrorList()) {
						
						if (!pp.existsInPropertiesList(PropertiesNameEnum.FEPA_WARNING_CODICI, e.getErrorCode() + Constants.EMPTY_STRING) &&
								!pp.existsInPropertiesList(PropertiesNameEnum.FEPA_WARNING_DESCRIZIONI, e.getErrorMessageString())) {
							actualError = true;
							break;
						}
						
					}
				}
				
				logKOResponse(risposta, "addDocumentoToFascicolo " + idFascicolo + "," + documentTitle, !actualError);
				if (actualError) {
					throw new RedException("Errore in fase di aggiunta del documento al fascicolo: FEPA ha restituito esito negativo.");
				}
			}

		} catch (RedException e) {
			throw e;
		} catch (Exception e) {
			LOGGER.error("Eccezione in fase di aggiunta del documento al fascicolo: " + e.getMessage(), e);
			throw new RedException(e);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#inviaDDDSRFirmati(java.lang.String,
	 *      java.util.List).
	 */
	@Override
	public int inviaDDDSRFirmati(final String idFascicolo, final List<String> wobDichiarazioneServiziResi) {
		Document docDecretoFirmato = null;
		final List<Document> docDichiarazioneServiziResi = new ArrayList<>();
		final List<Document> docFatture = new ArrayList<>();
		Boolean decretoInviato = false;
		Boolean dsrInviata = false;

		String idFascicoloFepa = null;
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;

		try {
			// recupera l'AOO
			final Integer idAoo = Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.IDAOO_RGS));
			final Aoo aoo = aooSRV.recuperaAoo(idAoo);

			// inizializza accesso al content engine in modalità administrator
			final AooFilenet aooFilenet = aoo.getAooFilenet();

			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), idAoo.longValue());

			fpeh = new FilenetPEHelper(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(), aooFilenet.getConnectionPoint());

			/**
			 * Dato il fascicolo del decreto, si deve iterare sulle fatture, Per ogni
			 * fattura, si invia tutto il contenuto del fascicolo fattura, compreso
			 * l'attuale decreto firmato e i dsr
			 */

			/**
			 * Si recuperano il drs e il decreto firmato.
			 */
			LOGGER.info("individuazione dei documenti dsr e dd");
			final Document fascicoloDecretoFn = fceh.getFascicolo(idFascicolo, idAoo);
			final FascicoloDTO fascicoloDecreto = TrasformCE.transform(fascicoloDecretoFn, TrasformerCEEnum.FROM_DOCUMENTO_TO_FASCICOLO);
			DocumentSet documentiFascicolo = fceh.getDocumentiFascicolo(Integer.parseInt(idFascicolo), (long) idAoo);
			final Iterator<?> itDocumentiFascicolo = documentiFascicolo.iterator();

			while (itDocumentiFascicolo.hasNext()) {
				final Object iter = itDocumentiFascicolo.next();
				if (iter instanceof Document) {
					final Document docFascicolo = (Document) iter;
					if (pp.getParameterByKey(PropertiesNameEnum.DECRETO_DIRIGENZIALE_FEPA_CLASSNAME_FN_METAKEY).equals(docFascicolo.getClassName())) {
						LOGGER.info("individuato decreto dirigenziale con id [" + docFascicolo.get_Name() + "]");
						docDecretoFirmato = docFascicolo;
					}
					if (pp.getParameterByKey(PropertiesNameEnum.DSR_CLASSNAME_FN_METAKEY).equals(docFascicolo.getClassName())) {
						LOGGER.info("individuata dichiarazione servizi resi con id [" + docFascicolo.get_Name() + "]");
						docDichiarazioneServiziResi.add(docFascicolo);
					}
					if (pp.getParameterByKey(PropertiesNameEnum.FATTURA_FEPA_CLASSNAME_FN_METAKEY).equals(docFascicolo.getClassName())) {
						LOGGER.info("individuata fattura con id [" + docFascicolo.get_Name() + "]");
						docFatture.add(docFascicolo);
					}
				}
			}

			if (docDecretoFirmato == null || docDichiarazioneServiziResi.isEmpty()) {
				throw new RedException("documenti non trovati");
			}

			/**
			 * Si esegue l'invio dei fascicoli fatture.
			 * 
			 * NOTA: Di seguito la struttura dei folder
			 * 
			 * /root/ --/fascicoli/idFascicolo della fattura -- Documenti, di questi, si
			 * devono inviare solamente la classe CLASSE_ALLEGATO --/FolderNSD -- non si
			 * deve trasmettere nulla
			 * 
			 * Per ogni documento fattura si deve: Risalire alla sua cartella che
			 * rappresenta il fascicolo della fattura. trovato il fascicolo, si trasmettono
			 * tutti i documenti della Classe Allegato Si trasmette, il Decreto Dirigenziale
			 * e la relativa Dichiarazione dei Servizi Resi
			 */
			LOGGER.info("inizio invio fascicoli fattura. ");

			for (final Document docFattura : docFatture) {

				LOGGER.info("individuata fattura con id [" + docFattura.get_Name() + "], si cerca la cartella corretta contenente i documenti da trasmettere.");

				final List<FascicoloDTO> fascicoliFattura = fceh.getFascicoliDocumento(docFattura, idAoo);

				final Iterator<FascicoloDTO> folderFatture = fascicoliFattura.iterator();
				while (folderFatture.hasNext()) {
					final FascicoloDTO fascicoloFattura = folderFatture.next();

					LOGGER.info("verifica folder [" + fascicoloFattura.getIdFascicolo() + "]");
					if (!fascicoloFattura.getIdFascicolo().equals(fascicoloDecreto.getIdFascicolo())) {
						/**
						 * Trovato il corretto folder, si inviano tutti gli allegati della fattura.
						 */
						LOGGER.info("Trovata cartella " + fascicoloFattura.getIdFascicolo() + " si procede all'invio di eventuali allegati");
						idFascicoloFepa = fascicoloFattura.getIdFascicoloFEPA();

						documentiFascicolo = fceh.getDocumentiFascicolo(Integer.parseInt(idFascicolo), (long) idAoo);
						final Iterator<?> docsDaTrasmettere = documentiFascicolo.iterator();

						while (docsDaTrasmettere.hasNext()) {
							final Object iterDoc = docsDaTrasmettere.next();
							if (iterDoc instanceof Document) {
								final Document docDaTrasmettere = (Document) iterDoc;
								LOGGER.info("Verifica della classe documentale del doc [" + docDaTrasmettere.get_Name() + "], className [" + docDaTrasmettere.getClassName()
										+ "]");
								if (pp.getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY).equals(docDaTrasmettere.getClassName())) {
									// Trasmetto l'allegato
									LOGGER.info("il documento e un allegato. già trasmesso? " + docDaTrasmettere.getProperties()
											.getBooleanValue(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_TRASMESSO)));
									if (Boolean.FALSE.equals(docDaTrasmettere.getProperties()
											.getBooleanValue(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_TRASMESSO)))
											&& !docDaTrasmettere.get_ContentElements().isEmpty()) {
										LOGGER.info("il documento verra trasmesso a fepa");
										if (inviaAllegatoFattura(docDaTrasmettere, idFascicoloFepa)) {
											LOGGER.info("trasmissione avvenuta con successo");
											docDaTrasmettere.getProperties()
													.putValue(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_TRASMESSO), true);
											docDaTrasmettere.save(RefreshMode.NO_REFRESH);
										}
									}
								}
							}
						}
						// Spediti i documenti, si invia anche il
						// decreto firmato e il dsr

						LOGGER.info("trasmissione del decreto a fepa");
						final List<String> idFascicoliOP = fceh.getAllOPId(docDecretoFirmato);

						if (Boolean.FALSE.equals(decretoInviato)) {
							decretoInviato = inviaDecreto(docDecretoFirmato, idFascicoliOP);
							LOGGER.info("Trasmissione decreto (" + docDecretoFirmato.get_Name() + ") " + (Boolean.TRUE.equals(decretoInviato) ? "avvenuta" : "fallita"));

							if ((!docDecretoFirmato.get_ContentElements().isEmpty()) && (Boolean.TRUE.equals(decretoInviato))) {
								// una eccezione all'invio allegati causa il reinvio del decreto???
								inviaAllegatiDecreto(docDecretoFirmato, idFascicoliOP, fceh);
								LOGGER.info("Allegati del decreto [" + docDecretoFirmato.get_Name() + "] trasmessi");
							}
						} else {
							LOGGER.info("Decreto gia trasmesso per la fattura corrente.");
						}

						if ((docFattura.getProperties().getDateTimeValue(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_DATA_TRASMISSIONE_DD)) == null)
								&& Boolean.TRUE.equals(decretoInviato)) {
							docFattura.getProperties().putValue(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_DATA_TRASMISSIONE_DD), new Date());
							docFattura.save(RefreshMode.REFRESH);
						} else {
							LOGGER.info("Decreto ancora non trasmesso o fattura già valorizzata con data di trasmissione ["
									+ docFattura.getProperties().getDateTimeValue(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_DATA_TRASMISSIONE_DD)).getTime()
									+ "]");
						}

						if (Boolean.FALSE.equals(dsrInviata)) {
							for (final Document doc : docDichiarazioneServiziResi) {
								LOGGER.info("trasmissione della dichiarazionse servizi resi " + doc.get_Name());
								inviaDsr(doc, idFascicoliOP);
								LOGGER.info("Trasmissione DSR (" + doc.get_Name() + ") avvenuta");
							}
							LOGGER.info("trasmissione delle dichiarazioni servizi resi terminato");
							dsrInviata = true;
						}

						fceh.aggiornaFatturaConDSR(docFattura, docDichiarazioneServiziResi);

						fpeh.nextStepWithOrWithoutResponseIdDocumento(aooFilenet.getIdClientAoo(), docFattura.get_Name(), "SYS_Fine Lavorazione");

					}

				}
			}

			LOGGER.info("trasmissione eseguita delle fatture eseguita, si aggiorna il WF dei Servizi resi");
			for (final String wob : wobDichiarazioneServiziResi) {
				LOGGER.info("trasmissione della dichiarazionse servizi resi con wob [" + wob + "]");
				fpeh.nextStepWithOrWithoutResponseWob(wob, "SYS_DecretoLavorato");
			}
			LOGGER.info("si aggiorna il metadato trasmesso del decreto");
			docDecretoFirmato.getProperties().putValue(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_TRASMESSO), true);
			docDecretoFirmato.save(RefreshMode.NO_REFRESH);

		} catch (final Exception ex) {
			LOGGER.error("Errore in fase di invio DD e DSR inviati", ex);
			return 0;
		} finally {
			popSubject(fceh);
			logoff(fpeh);
		}

		return 1;
	}

	private boolean inviaAllegatoFattura(final Document docDaTrasmettere, final String idFascicoloFepa) throws IOException {
		return inviaDocumento(docDaTrasmettere, idFascicoloFepa, Constants.Varie.TIPO_DOCUMENTO_GENERICO_FEPA, TipoAccessoFascicoloFepaEnum.DOCUMENTO_CONTABILE);
	}

	private boolean inviaDecreto(final Document docDecretoFirmato, final List<String> idFascicoliOPDecreto) throws IOException {
		return this.inviaDocumenti(docDecretoFirmato, idFascicoliOPDecreto, Constants.Varie.TIPO_DOCUMENTO_DECRETO_LIQUIDAZIONE_FEPA);
	}

	private boolean inviaDsr(final Document docDsr, final List<String> idFascicoliOPDecreto) throws IOException {
		return this.inviaDocumenti(docDsr, idFascicoliOPDecreto, Constants.Varie.TIPO_DOCUMENTO_LETTERA_SERVIZI_RESI_FEPA);
	}

	/**
	 * Invia a FEPA i documenti aggiuntivi associati al decreto
	 * 
	 * @param docDecretoFirmato
	 * @param idFascicoliOPDecreto elenco fascicoli OP associati al decreto,
	 *                             utilizzato se l'allegato non ha associazioni
	 *                             dirette con alcun OP
	 * @throws IOException
	 */
	private void inviaAllegatiDecreto(final Document docDecretoFirmato, final List<String> idFascicoliOPDecreto, final IFilenetCEHelper fceh) throws IOException {

		final DocumentSet allegati = docDecretoFirmato.get_ChildDocuments();

		if (allegati == null || allegati.isEmpty()) {
			LOGGER.warn("Nessun documento aggiuntivo associato al decreto" + docDecretoFirmato.get_Name());
			return;
		}

		final DocumentoFepaMapper fepaObjectMapper = new DocumentoFepaMapper();
		DocumentoFepa allegatoFepa = null;

		for (@SuppressWarnings("rawtypes")
		final Iterator iterator = allegati.iterator(); iterator.hasNext();) {

			final Document allegato = (Document) iterator.next();
			if (!allegato.get_ContentElements().isEmpty()) {
				allegatoFepa = fepaObjectMapper.getDocumentoFepa(allegato);
				List<String> idFascicoliOPAllegato = fceh.getOPIdsAllegato(allegato);

				if (idFascicoliOPAllegato == null || idFascicoliOPAllegato.isEmpty()) {
					idFascicoliOPAllegato = idFascicoliOPDecreto;
				}
				for (final String idFascicoloOPAllegato : idFascicoliOPAllegato) {
					try {
						LOGGER.info(INVIO_DOCUMENTO_LITERAL + allegatoFepa.toString() + AL_FASCICOLO_LITERAL + idFascicoloOPAllegato);
						inviaDocumento(idFascicoloOPAllegato, allegatoFepa, TipoAccessoFascicoloFepaEnum.OP);
					} catch (final Exception e) {
						LOGGER.error("Errore invio allegato " + allegato.get_Name() + " a decreto " + docDecretoFirmato.get_Name() + " - " + e.getMessage(), e);
					}
				}
			} else {
				LOGGER.warn("Allegato " + allegato.get_Name() + " a decreto " + docDecretoFirmato.get_Name() + " non ha content: non viene inviato");
			}
		}
	}
	private boolean inviaDocumenti(final Document doc, final List<String> idFascicoliOPDecreto, final String tipologiaDocumento) throws IOException {

		boolean result = true;

		if (idFascicoliOPDecreto == null || idFascicoliOPDecreto.isEmpty()) {
			return result;
		}

		for (final String idFascicoloOP : idFascicoliOPDecreto) {
			result &= inviaDocumento(doc, idFascicoloOP, tipologiaDocumento, TipoAccessoFascicoloFepaEnum.OP);
		}
		return result;
	}
	private boolean inviaDocumento(final Document doc, final String idFascicoloFepa, final String tipologiaDocumento, final TipoAccessoFascicoloFepaEnum tipoFascicoloFepa) throws IOException {

		boolean result = false;
		final DocumentoFepaMapper fepaObjectMapper = new DocumentoFepaMapper();
		// recupera utente firma
		final String usernameUtenteFirma = pp.getParameterByKey(PropertiesNameEnum.FEPA_UTENTE_FIRMA);
		final UtenteDTO utenteFirma = utenteSRV.getByUsername(usernameUtenteFirma);

		final String usernameUtenteUfficioTerzo = pp.getParameterByKey(PropertiesNameEnum.FEPA_UTENTE_UFFICIO_TERZO);
		final UtenteDTO utenteUfficioTerzo = utenteSRV.getByUsername(usernameUtenteUfficioTerzo);

		final DocumentoFepa docFepa = fepaObjectMapper.getDocumentoFepa(doc, tipologiaDocumento, utenteFirma.getNome() + " " + utenteFirma.getCognome(),
				utenteUfficioTerzo.getNome() + " " + utenteUfficioTerzo.getCognome());

		try {
			LOGGER.info(INVIO_DOCUMENTO_LITERAL + docFepa + AL_FASCICOLO_LITERAL + idFascicoloFepa);

			addDocumentoToFascicolo(tipoFascicoloFepa, idFascicoloFepa, tipologiaDocumento, docFepa.getDataCreazione(), null, null, null, null, null, docFepa.getContent(),
					docFepa.getFilename(), null, docFepa.getContentType(), docFepa.getDocumentTitle(), FepaMetadataMapper.getMetadata(docFepa));
			result = true;
		} catch (final Exception e) {
			LOGGER.error("Errore invio " + tipologiaDocumento + " " + doc.get_Name() + " - " + e.getMessage(), e);
			if (!e.getMessage().contains(Constants.Varie.SKIP_FAULT_CODE_DOCUMENT_CONTENT_ALREADY_EXIST)) {
				throw e;
			}
		}
		return result;
	}
	private boolean inviaDocumento(final String idFascicoloFepa, final DocumentoFepa docFepa, final TipoAccessoFascicoloFepaEnum tipoFascicoloFepa) {

		boolean result = false;
		
		try {
			LOGGER.info(INVIO_DOCUMENTO_LITERAL + docFepa + AL_FASCICOLO_LITERAL + idFascicoloFepa);

			addDocumentoToFascicolo(tipoFascicoloFepa, idFascicoloFepa, docFepa.getTipoDocumento(), docFepa.getDataCreazione(), null, null, null, null, null,
					docFepa.getContent(), docFepa.getFilename(), null, docFepa.getContentType(), docFepa.getDocumentTitle(), FepaMetadataMapper.getMetadata(docFepa));
			result = true;
		} catch (final Exception e) {
			LOGGER.error("Errore invio " + docFepa.getTipoDocumento() + " " + docFepa.getFilename() + " - " + e.getMessage(), e);
			if (!e.getMessage().contains(Constants.Varie.SKIP_FAULT_CODE_DOCUMENT_CONTENT_ALREADY_EXIST)) {
				throw e;
			}
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFepaFacadeSRV#inviaDD(java.lang.String).
	 */
	@Override
	public int inviaDD(final String idDecreto) {
		IFilenetCEHelper fceh = null;

		try {
			// recupera l'AOO
			final Integer idAoo = Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.IDAOO_RGS));
			final Aoo aoo = aooSRV.recuperaAoo(idAoo);

			// inizializza accesso al content engine in modalità administrator
			final AooFilenet aooFilenet = aoo.getAooFilenet();

			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), idAoo.longValue());

			final Document decreto = fceh.getDocumentByIdGestionale(idDecreto, null, null, aoo.getIdAoo().intValue(), null,
					pp.getParameterByKey(PropertiesNameEnum.DECRETO_DIRIGENZIALE_FEPA_CLASSNAME_FN_METAKEY));

			final List<String> idFascicoliOP = fceh.getAllOPId(decreto);
			LOGGER.info("trasmissione del decreto " + idDecreto + " nei fascicoli OP ");
			inviaDecreto(decreto, idFascicoliOP);
			inviaAllegatiDecreto(decreto, idFascicoliOP, fceh);
		} catch (final Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
			return 0;
		} finally {
			popSubject(fceh);
		}
		return 1;
	}

}
