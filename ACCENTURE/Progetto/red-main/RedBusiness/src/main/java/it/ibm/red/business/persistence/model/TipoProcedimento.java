package it.ibm.red.business.persistence.model;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class TipoProcedimento.
 *
 * @author CPIERASC
 * 
 *         Entità che mappa la tabella TIPO_PROCEDIMENTO.
 */
public class TipoProcedimento implements Serializable {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Data attivazione.
	 */
	private final Date dataAttivazione;

	/**
	 * Data disattivazione.
	 */
	private final Date datadisattivazione;

	/**
	 * Descrizione.
	 */
	private final String descrizione;

	/**
	 * Flag generico entrata.
	 */
	private final int flagGenericoEntrata;

	/**
	 * Flag generico uscita.
	 */
	private final int flagGenericoUscita;

	/**
	 * Identificativo aoo.
	 */
	private final int idAoo;

	/**
	 * Identificativo tipo procedimento.
	 */
	private final long tipoProcedimentoId;

	/**
	 * Identificativo workflow.
	 */
	private final Long workFlowId;

	/**
	 * Subject workflow.
	 */
	private final String workflowSubject;
	
	
	/**
	 * Codice del tipo gestione.
	 */
	private String tipoGestione;
	
	
	/**
	 * 
	 * @param inTipoProcedimentoId
	 * @param inDescrizione
	 */
	public TipoProcedimento(final long inTipoProcedimentoId, final String inDescrizione) {
		super();
		this.tipoProcedimentoId = inTipoProcedimentoId;
		this.descrizione = inDescrizione;
		this.dataAttivazione = null;
		this.datadisattivazione = null;
		this.workFlowId = null;
		this.workflowSubject = null;
		this.idAoo = 0;
		this.flagGenericoEntrata = 0;
		this.flagGenericoUscita = 0;
	}
	
	
	/**
	 * Costruttore per tipo gestione.
	 * 
	 * @param inTipoProcedimentoId
	 * @param inDescrizione
	 * @param inTipogestione
	 */
	public TipoProcedimento(final long inTipoProcedimentoId, final String inDescrizione, final String inTipoGestione) {
		this(inTipoProcedimentoId, inDescrizione);
		this.tipoGestione = inTipoGestione;
	}

	
	/**
	 * Costruttore.
	 * 
	 * @param inTipoProcedimentoId	identificativo
	 * @param inDescrizione			descrizione
	 * @param inDataAttivazione		data attivazione
	 * @param inDatadisattivazione	data disattivazione
	 * @param inWorkFlowId			id workflow
	 * @param inWorkflowSubject		subject workflow
	 * @param inIdAoo				identificativo aoo
	 * @param inFlagGenericoEntrata	flag genrico entrata
	 * @param inFlagGenericoUscita	flag generico uscita
	 */
	public TipoProcedimento(final long inTipoProcedimentoId, final String inDescrizione, final Date inDataAttivazione, final Date inDatadisattivazione, 
			final Long inWorkFlowId, final String inWorkflowSubject, final int inIdAoo, final int inFlagGenericoEntrata, final int inFlagGenericoUscita) {
		super();
		this.tipoProcedimentoId = inTipoProcedimentoId;
		this.descrizione = inDescrizione;
		this.dataAttivazione = inDataAttivazione;
		this.datadisattivazione = inDatadisattivazione;
		this.workFlowId = inWorkFlowId;
		this.workflowSubject = inWorkflowSubject;
		this.idAoo = inIdAoo;
		this.flagGenericoEntrata = inFlagGenericoEntrata;
		this.flagGenericoUscita = inFlagGenericoUscita;
	}
	
	/**
	 * Getter data attivazione.
	 * 
	 * @return	data attivazione
	 */
	public final Date getDataAttivazione() {
		return dataAttivazione;
	}

	/**
	 * Getter data disattivazione.
	 * 
	 * @return	data disattivazione
	 */
	public final Date getDatadisattivazione() {
		return datadisattivazione;
	}

	/**
	 * Getter descrizione.
	 * 
	 * @return	descrizione
	 */
	public final String getDescrizione() {
		return descrizione;
	}

	/**
	 * Getter flag generico entrata.
	 * 
	 * @return	flag genrico entrata
	 */
	public final int getFlagGenericoEntrata() {
		return flagGenericoEntrata;
	}

	/**
	 * Getter flag generico uscita.
	 * 
	 * @return	flag genrico uscita
	 */
	public final int getFlagGenericoUscita() {
		return flagGenericoUscita;
	}

	/**
	 * Getter identificativo aoo.
	 * 
	 * @return	identificativo aoo
	 */
	public final int getIdAoo() {
		return idAoo;
	}

	/**
	 * Getter identificativo tipo procedimento.
	 * 
	 * @return	identificativo tipo procedimento
	 */
	public final long getTipoProcedimentoId() {
		return tipoProcedimentoId;
	}

	/**
	 * Getter identificativo workflow.
	 * 
	 * @return	identificativo workflow
	 */
	public final Long getWorkFlowId() {
		return workFlowId;
	}

	/**
	 * Getter subject workflow.
	 * 
	 * @return	subject workflow
	 */
	public final String getWorkflowSubject() {
		return workflowSubject;
	}


	/**
	 * @return the idTipoGestione
	 */
	public String getTipoGestione() {
		return tipoGestione;
	}
	
}
