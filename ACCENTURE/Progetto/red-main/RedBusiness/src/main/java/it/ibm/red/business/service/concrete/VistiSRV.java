package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipoApprovazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.ILookupSRV;
import it.ibm.red.business.service.IVistiSRV;
import it.ibm.red.business.service.facade.IEventoLogFacadeSRV;
import it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV;

/**
 * @author adilegge
 */
@Service
public class VistiSRV extends AbstractService implements IVistiSRV {

	/**
	 * serial version UID.
	 */
	private static final long serialVersionUID = -7319614039900345733L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(VistiSRV.class.getName());

	/**
	 * Servizio.
	 */
	/**
	 * Servizio.
	 */
	@Autowired
	private ILookupSRV lookupSRV;

	/**
	 * DAO.
	 */
	/**
	 * DAO.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Servizio.
	 */
	/**
	 * Servizio.
	 */
	@Autowired
	private IOperationWorkFlowFacadeSRV operationWorkflowSRV;
	/**
	 * Servizio.
	 */
	/**
	 * Servizio.
	 */
	@Autowired
	private IEventoLogFacadeSRV eventoLogSRV;
	
	/**
	 * Fornitore della properties dell'applicazione.
	 */
	private PropertiesProvider pp;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IVistiFacadeSRV#terminaWfVisti(java.lang.String,
	 *      java.lang.String, java.lang.Long).
	 */
	@Override
	public int terminaWfVisti(final String idDocumento, final String fWorkflowNumber, final Long idUfficio) {
		Connection connection = null;
		Integer esito = 1;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			LOGGER.info("Recupero le credenziali filenet per l'ufficio " + idUfficio);

			final Nodo ufficio = lookupSRV.getUfficio(idUfficio, connection);

			final AooFilenet aooFilenet = ufficio.getAoo().getAooFilenet();

			final FilenetPEHelper fpeh = new FilenetPEHelper(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getConnectionPoint());

			final List<VWWorkObject> wos = fpeh.getWorkflowsVisti(idDocumento, fWorkflowNumber, ufficio.getAoo().getCodiceAoo(), true);

			Integer idSegreteriaNodoOwner = 0;
			for (final VWWorkObject wo : wos) {
				if (!wo.getWorkflowNumber().equals(fWorkflowNumber)) {
					idSegreteriaNodoOwner = nodoDAO.getSegreteriaDirezione((Integer) 
							wo.getFieldValue(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY)),
							connection);
					if (idUfficio.intValue() == idSegreteriaNodoOwner.intValue()) {
						fpeh.chiudiWFConStorico(wo, "76");
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			rollbackConnection(connection);
			esito = -1;
		} finally {
			closeConnection(connection);
		}
		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IVistiFacadeSRV#avanzaWfNonVistiNonContributi(java.lang.String,
	 *      java.lang.String, java.lang.Long).
	 */
	@Override
	public int avanzaWfNonVistiNonContributi(final String idDocumento, final String fWorkflowNumber, final Long idUfficio) {
		Connection connection = null;
		Integer esito = 1;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			LOGGER.info("Recupero le credenziali filenet per l'ufficio " + idUfficio);

			final Nodo ufficio = lookupSRV.getUfficio(idUfficio, connection);

			final AooFilenet aooFilenet = ufficio.getAoo().getAooFilenet();

			final FilenetPEHelper fpeh = new FilenetPEHelper(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getConnectionPoint());

			final List<VWWorkObject> wos = fpeh.getWorkflowsVisti(idDocumento, fWorkflowNumber, ufficio.getAoo().getCodiceAoo(), false);
			if (wos.isEmpty()) {

				final String queueNameAlGiroVisti = pp.getParameterByKey(PropertiesNameEnum.AL_GIROVISTI_STEPNAME_WFKEY).split("\\|")[0];

				// se non ci sono più assegnazioni per visto attive, avanzo i workflow non
				// contributi e non visti
				final List<VWWorkObject> wosDaAvanzare = fpeh.getWorkflowsNonVistiNonContributi(idDocumento, fWorkflowNumber, ufficio.getAoo().getCodiceAoo());
				for (final VWWorkObject wo : wosDaAvanzare) {
					if (!wo.getWorkflowNumber().equals(fWorkflowNumber)) {
						if (wo.getCurrentQueueName().equalsIgnoreCase(queueNameAlGiroVisti)) {
							LOGGER.info("Workflow " + wo.getWorkflowNumber() + " su coda " + wo.getCurrentQueueName() + ": da avanzare");
							fpeh.nextStepFromStepElement(wo, null);
						} else {
							LOGGER.info("Workflow " + wo.getWorkflowNumber() + " su coda " + wo.getCurrentQueueName() + ": non serve avanzarlo");
						}
					}
				}
			} else {
				LOGGER.info("Sono presenti ancora " + wos.size() + " workflow assegnati per visto sul documento " 
						+ idDocumento + " oltre a " + fWorkflowNumber);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			rollbackConnection(connection);
			esito = -1;
		} finally {
			closeConnection(connection);
		}
		return esito;
	}

	@Override
	public final Collection<EsitoOperazioneDTO> visto(final UtenteDTO utente, final Collection<String> wobNumbers, final TipoApprovazioneEnum tipoVisto,
			final String motivoAssegnazione) {
		return vistoOVerificato(utente, wobNumbers, ResponsesRedEnum.VISTO, tipoVisto, motivoAssegnazione);
	}

	@Override
	public final EsitoOperazioneDTO visto(final UtenteDTO utente, final String wobNumber, final TipoApprovazioneEnum tipoVisto, final String motivoAssegnazione) {
		return vistoOVerificato(utente, wobNumber, ResponsesRedEnum.VISTO, tipoVisto, motivoAssegnazione);
	}

	@Override
	public final Collection<EsitoOperazioneDTO> vistoPositivo(final UtenteDTO utente, final Collection<String> wobNumbers, final String motivoAssegnazione) {
		return vistoOVerificato(utente, wobNumbers, ResponsesRedEnum.VISTO, TipoApprovazioneEnum.VISTO_POSITIVO, motivoAssegnazione);
	}

	@Override
	public final EsitoOperazioneDTO vistoPositivo(final UtenteDTO utente, final String wobNumber, final String motivoAssegnazione) {
		return vistoOVerificato(utente, wobNumber, ResponsesRedEnum.VISTO, TipoApprovazioneEnum.VISTO_POSITIVO, motivoAssegnazione);
	}

	@Override
	public final Collection<EsitoOperazioneDTO> verificato(final UtenteDTO utente, final Collection<String> wobNumbers, final TipoApprovazioneEnum tipoVisto,
			final String motivoAssegnazione) {
		return vistoOVerificato(utente, wobNumbers, ResponsesRedEnum.VERIFICATO, tipoVisto, motivoAssegnazione);
	}

	@Override
	public final EsitoOperazioneDTO verificato(final UtenteDTO utente, final String wobNumbers, final TipoApprovazioneEnum tipoVisto, final String motivoAssegnazione) {
		return vistoOVerificato(utente, wobNumbers, ResponsesRedEnum.VERIFICATO, tipoVisto, motivoAssegnazione);
	}

	private Collection<EsitoOperazioneDTO> vistoOVerificato(final UtenteDTO utente, final Collection<String> wobNumbers, final ResponsesRedEnum response,
			final TipoApprovazioneEnum tipoVisto, final String motivoAssegnazione) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();

		for (final String wobNumber : wobNumbers) {
			esiti.add(vistoOVerificato(utente, wobNumber, response, tipoVisto, motivoAssegnazione));
		}
		return esiti;
	}

	private EsitoOperazioneDTO vistoOVerificato(final UtenteDTO utente, final String wobNumber, final ResponsesRedEnum response, final TipoApprovazioneEnum tipoVisto,
			final String motivoAssegnazione) {
		EsitoOperazioneDTO esito = null;
		Integer numDocumento = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final Map<String, Object> metadatiWorkflow = new HashMap<>();
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_APPROVAZIONE_FN_METAKEY), tipoVisto.getId());
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.NOTE_VISTO_FN_METAKEY), motivoAssegnazione);
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), "Visto del documento");
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId());

			final VWWorkObject worfklow = fpeh.getWorkFlowByWob(wobNumber, true);
			final Integer idDocumento = (Integer) TrasformerPE.getMetadato(worfklow, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
			numDocumento = fceh.getNumDocByDocumentTitle(idDocumento.toString());
			// Avanzamento del workflow
			final boolean avanzamentoWorkflowOk = fpeh.nextStep(wobNumber, metadatiWorkflow, response.getResponse());

			if (avanzamentoWorkflowOk) {
				// Recupero del wobnumber del workflow padre.
				String wobNumberPadre = (String) TrasformerPE.getMetadato(worfklow, pp.getParameterByKey(PropertiesNameEnum.WF_PARENT_NUMBER_METAKEY));
				// Storno giro visti ispettorato-uffici
				stornaDalGiroVistiByIdDocumento(idDocumento, fpeh, utente);
				// Update del metadato idUtenteDestinatario dopo aver avanzato il WF padre.
				updateWorkflowPadre(wobNumber,wobNumberPadre, idDocumento,utente.getIdAoo(), fpeh);
			} else {
				LOGGER.error("Attenzione! Avanzamento non riuscito per il workflow: " + wobNumber);
				throw new RedException("Attenzione! Avanzamento non riuscito per il workflow: " + wobNumber);
			}
			esito = new EsitoOperazioneDTO(null, null, numDocumento, wobNumber);
			esito.setNote(ResponsesRedEnum.VISTO.equals(response) ? "Visto del documento effettuato con successo." : "Il documento è stato verificato.");
		} catch (final Exception e) {
			final String errore = "Errore durante l'esecuzione della response: " + response.name() 
				+ " richiamata dall'utente: " + utente.getUsername() + " per il workflow: " + wobNumber;
			LOGGER.error(errore, e);
			esito = new EsitoOperazioneDTO(wobNumber, false, errore);
			esito.setIdDocumento(numDocumento);
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}
		return esito;
	}


	/**
	 * @param utente
	 * @param fpeh
	 * @param metadatiWorkflow
	 * @param wobNumberPadre
	 */
	@Override
	public void updateWorkflowPadre(String wobGiroVisti, String wobNumberPadre, Integer idDocumento, final Long idAOO, FilenetPEHelper fpeh) {
		Map<String, Object> newMetadati = new HashMap<>();
		
		try {
			Collection<VWWorkObject> wfCollection = fpeh.fetchWorkflowsAlGiroVistiByDocumentoId(idDocumento);
			//Se ho chiuso tutto il giro visti
			if(CollectionUtils.isEmpty(wfCollection)) {
				// Update fatto in un secondo momento perchè altrimenti viene sovrascritto dal nextstep fatto in precedenza.
				VWWorkObject worfklowPadre = fpeh.getWorkFlowByWob(wobNumberPadre, true);
				
				// Recupero dell'id utente delegato scelto durante la 'Richiesta Visto' 
				Integer newIdUtenteDest = (Integer) TrasformerPE.getMetadato(worfklowPadre, pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DELEGATO_VISTO_WF_METAKEY));
				if (newIdUtenteDest != null && newIdUtenteDest!=0) {
					newMetadati.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), newIdUtenteDest);
					fpeh.updateWorkflow(worfklowPadre, newMetadati);
					
					Integer idDoc = (Integer) TrasformerPE.getMetadato(worfklowPadre, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
					Integer idUffDestinatario = (Integer) TrasformerPE.getMetadato(worfklowPadre, pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY));
					
					eventoLogSRV.inserisciEventoLog(idDoc, idUffDestinatario.longValue(), newIdUtenteDest.longValue(), idUffDestinatario.longValue(), newIdUtenteDest.longValue(),
							new Date(), EventTypeEnum.ASSEGNAZIONE_FIRMA,	"Il documento è transitato nel libro firma del delegato", 0, idAOO);
				}
			}
			
		} catch (Exception e) {
			LOGGER.error("Errore durante l'update del workflow padre: ", e);
			throw new RedException("Errore durante l'update del workflow padre: ", e);
		}
		
	}

	@Override
	public final Collection<EsitoOperazioneDTO> nonVerificato(final UtenteDTO utente, final Collection<String> wobNumbers, final String motivoAssegnazione) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();

		for (final String wobNumber : wobNumbers) {
			esiti.add(nonVerificato(utente, wobNumber, motivoAssegnazione));
		}
		return esiti;
	}

	@Override
	public final EsitoOperazioneDTO nonVerificato(final UtenteDTO utente, final String wobNumber, final String motivoAssegnazione) {
		final EsitoOperazioneDTO esito = 
				operationWorkflowSRV.avanzaWorkflowEStornaWfContributi(utente, wobNumber, motivoAssegnazione, ResponsesRedEnum.NON_VERIFICATO);
		if (esito.isEsito()) {
			esito.setNote("L'esito della verifica è stato comunicato con successo.");
		}
		return esito;
	}

	/**
	 * Metodo per lo storno dal giro visti per id documento.
	 *
	 * @param documentoId id documento
	 * @param fpeh        helper gestione pe
	 */
	@Override
	public void stornaDalGiroVistiByIdDocumento(final Integer idDocumento, final FilenetPEHelper fpeh, final UtenteDTO utente) {
		// Si recuperano i workflow al giro visti per il documento
		final Collection<VWWorkObject> wfCollection = fpeh.fetchWorkflowsAlGiroVistiByDocumentoId(idDocumento);

		// Si esegue lo storno avanzando i workflow
		for (final Iterator<?> iterator = wfCollection.iterator(); iterator.hasNext();) {
			final VWWorkObject workflow = (VWWorkObject) iterator.next();
			fpeh.nextStep(workflow, new HashMap<>(), ResponsesRedEnum.SPOSTA_IN_LAVORAZIONE.getResponse());
		}
	}
}