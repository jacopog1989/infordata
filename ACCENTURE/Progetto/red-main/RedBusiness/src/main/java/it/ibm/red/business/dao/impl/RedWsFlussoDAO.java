package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IRedWsFlussoDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.RedWsFlusso;

/**
 * DAO per la gestione dei flussi associati ai web service esposti da Red EVO.
 * 
 * @author m.crescentini
 */
@Repository
public class RedWsFlussoDAO extends AbstractDAO implements IRedWsFlussoDAO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -7749947485498962185L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RedWsFlussoDAO.class.getName());


	/**
	 * @see it.ibm.red.business.dao.IRedWsFlussoDAO#getById(int,
	 *      java.sql.Connection).
	 */
	@Override
	public RedWsFlusso getById(final int idFlusso, final Connection con) {
		RedWsFlusso redWsFlusso = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			final String querySQL = "SELECT * FROM REDWS_FLUSSO WHERE IDFLUSSO = ?";
			ps = con.prepareStatement(querySQL);
			ps.setInt(1, idFlusso);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				redWsFlusso = populate(rs);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del flusso con ID: " + idFlusso, e);
			throw new RedException("Errore durante il recupero del flusso con ID: " + idFlusso, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return redWsFlusso;
	}

	/**
	 * @see it.ibm.red.business.dao.IRedWsFlussoDAO#gertByCodice(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public RedWsFlusso getByCodice(final String codiceFlusso, final Connection con) {
		RedWsFlusso redWsFlusso = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			final String querySQL = "SELECT * FROM REDWS_FLUSSO WHERE CODICE = ?";
			ps = con.prepareStatement(querySQL);
			ps.setString(1, codiceFlusso);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				redWsFlusso = populate(rs);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del flusso con codice: " + codiceFlusso, e);
			throw new RedException("Errore durante il recupero del flusso con codice: " + codiceFlusso, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return redWsFlusso;
	}

	
	private static RedWsFlusso populate(final ResultSet rs) throws SQLException {
		Integer flagUscita = rs.getInt("FLAGUSCITA");
		if (rs.wasNull()) {
			flagUscita = null;
		}
		
		return new RedWsFlusso(rs.getInt("IDFLUSSO"), rs.getString("CODICE"), rs.getString("INTERFACENAME"), flagUscita);
	}
}
