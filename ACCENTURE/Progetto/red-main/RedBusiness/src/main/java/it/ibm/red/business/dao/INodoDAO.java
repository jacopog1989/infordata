package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.persistence.model.Nodo;

/**
 * Dao per la gestione del nodo.
 * 
 * @author CPIERASC
 */
public interface INodoDAO extends Serializable {

	/**
	 * Recupera l'identificativo del ragioniere generale dello stato.
	 * 
	 * @param con
	 *            Connessione al database.
	 * @return Identificativo del ragioniere generale dello stato.
	 */
	Integer getRagGenDelloStato(Connection con);

	/**
	 * Restituisce una rappresentazione del firmatario dell'iter approvativo
	 * selezionato.
	 * 
	 * @param idUfficioUtente
	 *            Identificativo ufficio.
	 * @param idTipoFirma
	 *            Identificativo tipologia firma.
	 * @param con
	 *            Connessione al database.
	 * @return Firmatario recuperato.
	 */
	String getFirmatarioIterApprovativo(Long idUfficioUtente, Integer idTipoFirma, Connection con);

	/**
	 * Restituisce identificativo dell'ufficio del coordinatore.
	 * 
	 * @param idUfficioMittente
	 *            Identificativo ufficio mittente.
	 * @param idUtenteCoordinatore
	 *            Identificativo utente coordinatore.
	 * @param connection
	 *            Connessione al database.
	 * @return Identificativo ufficio del coordinatore.
	 */
	Integer getIdNodoCoordinatore(Long idUfficioMittente, Integer idUtenteCoordinatore, Connection connection);

	/**
	 * Recupera nodo identificato dal {@code idNodo}.
	 * 
	 * @param idNodo
	 *            Identificativo nodo.
	 * @param connection
	 *            Connessione al database.
	 * @return Nodo recuperato.
	 */
	Nodo getNodo(Long idNodo, Connection connection);

	/**
	 * Recupera id nodo corriere identificato dal {@code idNodo}.
	 * 
	 * @param idNodo
	 *            Identificativo del nodo.
	 * @param connection
	 *            Connessione al database.
	 * @return Identificativo nodo corriere.
	 */
	Long getNodoCorriereId(Long idNodo, Connection connection);

	/**
	 * Recupero id alberatura ufficio.
	 * 
	 * @param idNodo
	 *            Identificativo ufficio.
	 * @param connection
	 *            Connessione al database.
	 * @return Identificativo alberatura ufficio.
	 */
	List<Long> getAlberaturaUfficio(Long idNodo, Connection connection);

	/**
	 * Recupera il nodo Ufficio Centrale Protocollo per il nodo in input.
	 * 
	 * @param idNodo
	 *            Identificativo nodo.
	 * @param connection
	 *            Connessione al database.
	 * @return Identificativo nodo UCP.
	 */
	Long getNodoUCP(Long idNodo, Connection connection);

	/**
	 * Ottiene l'alberatura bottom-up.
	 * 
	 * @param idAoo
	 *            Identificativo area organizzativa.
	 * @param idUfficio
	 *            Identificativo ufficio.
	 * @param con
	 *            Connessione al database.
	 * @return Lista di nodi.
	 */
	List<Nodo> getAlberaturaBottomUp(Long idAoo, Long idUfficio, Connection con);

	/**
	 * Recupera l'ufficio di segreteria dell'ufficio in input.
	 * 
	 * @param idUfficio
	 *            Identificativo ufficio.
	 * @param connection
	 *            Connessione al database.
	 * @return Identificativo segreteria direzione.
	 */
	Integer getSegreteriaDirezione(Integer idUfficio, Connection connection);

	/**
	 * Ottiene il Corriere Segreteria dall'id del nodo.
	 * 
	 * @param idNodo
	 *            Identificativo ufficio.
	 * @param con
	 *            Connessione al database.
	 * @return Nodo recuperato.
	 */
	Nodo getCorriereSegreteriaFromSottoNodo(int idNodo, Connection con);

	/**
	 * Ottiene il nuovo ufficio dal tab migrazione.
	 * 
	 * @param idNodo
	 *            Identificativo ufficio.
	 * @param con
	 *            Connessione al database.
	 * @return Ufficio recuperato.
	 */
	UfficioDTO getNewIdFromTabMigrazione(long idNodo, Connection con);

	/**
	 * Ottiene l'ufficio UCR.
	 * 
	 * @param idAOO
	 *            Identificativo area organizzativa.
	 * @param con
	 *            Connessione al database.
	 * @return Ufficio UCR rrecuperato.
	 */
	UfficioDTO getUfficioUCR(Long idAOO, Connection con);

	/**
	 * Controlla se il nodo appartiene all'ufficio UCP.
	 * 
	 * @param idNodo
	 *            Identificativo ufficio.
	 * @return Esito della ricerca.
	 */
	Boolean isNodoUcp(Long idNodo, Connection con);

	/**
	 * Ricerca i nodi dell'ispettorato per lo scadenziario e ritorna anche i figli
	 * di interesse.
	 * 
	 * @param idAOO
	 *            Identificativo area organizzativa.
	 * @param con
	 *            Connessione al database.
	 * @return Sotto nodi ispettorato recuperati.
	 */
	Map<UfficioDTO, List<UfficioDTO>> getSottoNodiIspettorato(Long idAOO, Connection con);

	/**
	 * Ottiene il flag di assegnazione diretta per ufficio.
	 * 
	 * @param idNodo
	 *            Identificativo ufficio.
	 * @param indirizzoEmail
	 *            Indirizzo e-mail.
	 * @param con
	 *            Connessione al database.
	 * @return True o false in base al valore del flag assegnazione diretta.
	 */
	Boolean getFlagAssegnazioneDiretta(Long idNodo, String indirizzoEmail, Connection con);

	/**
	 * Predispone la preselezione del ribalta titolario.
	 * 
	 * @param idUfficio
	 *            Identificativo ufficio.
	 * @param idAoo
	 *            Identificativo area organizzativa.
	 * @param conn
	 *            Connessione al database.
	 * @return True o false.
	 */
	boolean preselezionaRibaltaTitolario(Long idUfficio, Long idAoo, Connection conn);

	/**
	 * Ottiene l'id del nodo tramite la descrizione e il codice dell'aoo.
	 * 
	 * @param descrizioneNodo
	 *            Descrizione nodo.
	 * @param codiceAoo
	 *            Codice area organizzativa.
	 * @param connection
	 *            Connessione al database.
	 * @return Identificativo del nodo.
	 */
	Long getIdNodoByDescAndCodiceAoo(String descrizioneNodo, String codiceAoo, Connection connection);

	/**
	 * Otteniamo la mappa di id nodo descrizione per idAoo
	 * 
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param conn
	 *            Connessione al database.
	 * @return Descrizioni dei nodi dell'area organizzativa.
	 */
	HashMap<Integer, String> getDescrizioneByIdNodo(Integer idAoo, Connection conn);

	/**
	 * Restituisce la lista dei nodi appartenenti alla stessa AOO identificata da
	 * <code> idAoo </code>.
	 * 
	 * @param idAoo
	 *            Identificativo area organizzativa.
	 * @param connection
	 *            Connessione al database.
	 * @return Lista dei nodi recuperati.
	 */
	List<Long> getIdNodiByAoo(Long idAoo, Connection connection);

}