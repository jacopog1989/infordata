package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.JobConfigurationDTO;

/**
 * 
 * @author a.dilegge
 *
 *	Interfaccia del DAO per la gestione delle properties.
 */
public interface IJobConfigurationDAO extends Serializable {

	/**
	 * Metodo per il recupero di tutti i job configurati sulla base dati.
	 * 
	 * @param connection	connessione al db
	 * @return				collezione di job recuperati
	 */
	List<JobConfigurationDTO> getAll(Connection connection);

}