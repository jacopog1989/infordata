package it.ibm.red.business.dto;

import java.util.Date;

/**
 * DTO che definisce la chiusura della dichiarazione.
 */
public class ChiudiDichiarazioneDTO extends AbstractDTO {

	/**
	 * UID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Nome file.
	 */
	private String nomeFile;
	
	/**
	 * Data creazione.
	 */
	private Date dateCreated;

	/**
	 * Restituisce la data di creazione.
	 * @return dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}

	/**
	 * Imposta la data di creazione.
	 * @param dateCreated
	 */
	public void setDateCreated(final Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * Restituisce il nome del file.
	 * @return nomeFile
	 */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * Imposta il nome del file.
	 * @param nomeFile
	 */
	public void setNomeFile(final String nomeFile) {
		this.nomeFile = nomeFile;
	}

}
