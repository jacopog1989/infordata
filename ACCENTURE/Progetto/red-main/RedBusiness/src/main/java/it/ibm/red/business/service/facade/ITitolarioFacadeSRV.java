package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.TitolarioDTO;

/**
 * Facade del servizio che gestisce il titolario.
 */
public interface ITitolarioFacadeSRV extends Serializable {

	/**
	 * @param idAOO
	 * @param idNodo
	 * @return
	 */
	List<TitolarioDTO> getFigliRadice(Long idAOO, Long idNodo);
	
	/**
	 * @param idAOO
	 * @param indice
	 * @return
	 */
	List<TitolarioDTO> getFigliByIndice(Long idAOO, String indice);
	
	/**
	 * @param idAOO
	 * @param indice
	 * @return
	 */
	TitolarioDTO getNodoByIndice(Long idAOO, String indice);
	
	/**
	 * @param idAOO
	 * @param idUfficio
	 * @param parola
	 * @return
	 */
	List<TitolarioDTO> getNodiAutocomplete(Long idAOO, Long idUfficio, String parola);

	/**
	 * Ottiene i figli radice non disattivati.
	 * @param idAOO
	 * @param idNodo
	 * @param connection
	 * @return lista di titolari
	 */
	List<TitolarioDTO> getFigliRadiceNonDisattivati(Long idAOO, Long idNodo, Connection connection);

	/**
	 * Ottiene i figli radice non disattivati.
	 * @param idAOO
	 * @param idNodo
	 * @return lista di titolari
	 */
	List<TitolarioDTO> getFigliRadiceNonDisattivati(Long idAOO, Long idNodo);

	/**
	 * Ottiene i figli non disattivati tramite l'indice.
	 * @param idAOO
	 * @param indice
	 * @return lista di titolari
	 */
	List<TitolarioDTO> getFigliByIndiceNonDisattivati(Long idAOO, String indice);
}
