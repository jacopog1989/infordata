package it.ibm.red.business.dto;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.SignModeEnum;
import it.ibm.red.business.enums.SignTypeEnum.SignTypeGroupEnum;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.asign.step.StatusEnum;
import it.ibm.red.business.service.asign.step.StepEnum;
import it.ibm.red.business.utils.DateUtils;

/**
 * DTO per la gestione degli item di firma asincrona.
 */
public class ASignItemDTO extends AbstractDTO {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = 5988759687430749534L;
	
	/**
	 * Id item di firma.
	 */
	private Long id;
	
	/**
	 * Status dell'item.
	 */
	private StatusEnum status;
	
	/**
	 * Numero retry raggiunto attualmente.
	 */
	private Integer nRetry;
	
	/**
	 * Data ultimo retry - se null è stato eseguito solo il primo tentativo ma mai un retry.
	 */
	private Date dataUltimoRetry;
	
	/**
	 * Document title sul quale fa riferimento l'item.
	 */
	private String documentTitle;
	
	/**
	 * Numero documento sul qual fa riferimento l'item.
	 */
	private Integer numeroDocumento;
	
	/**
	 * Wobnumb del procedimento su cui fa riferimento l'item.
	 */
	private String wobNumber;
	
	/**
	 * Step attuale dell'item.
	 */
	private StepEnum step;
	
	/**
	 * ID AOO di riferimento dell'item.
	 */
	private Long idAoo;
	
	/**
	 * Priorità di lavorazione dell'item.
	 */
	private Integer priority;
	
	/**
	 * Quando un item è in errore questo flag definisce lo stato della comunicazione dell'errore.
	 */
	private Boolean flagComunicato;
	
	/**
	 * Numero protocollo del documento a cui fa riferimento l'item.
	 */
	private Integer numeroProtocollo;
	
	/**
	 * Anno protocollo del documento a cui fa riferimento l'item.
	 */
	private Integer annoProtocollo;

	/**
	 * Id del firmatario del documento su cui fa riferimento l'item.
	 */
	private Long idFirmatario;
	
	/**
	 * Id del ruolo del firmatario.
	 */
	private Long idRuoloFirmatario;
	
	/**
	 * Id dell'ufficio del firmatario.
	 */
	private Long idUfficioFirmatario;
	
	/**
	 * Modalità di firma.
	 */
	private SignModeEnum signMode;
	
	/**
	 * Tipologia della firma.
	 */
	private SignTypeGroupEnum signType;

	/**
	 * Insieme degli step.
	 */
	private Collection<ASignStepDTO> steps;
	
	/**
	 * Insieme degli stati.
	 */
	private Collection<ASignStatusDTO> states;

	/**
	 * Coda sulla quale viene mostrato l'item durante il processo di firma.
	 */
	private DocumentQueueEnum queue;

	/**
	 * Costruttore.
	 */
	public ASignItemDTO() {
		steps = new ArrayList<>();
		states = new ArrayList<>();
	}
	
	/**
	 * Restituisce gli step.
	 * @return informazioni sugli step
	 */
	public Collection<ASignStepDTO> getSteps() {
		return steps;
	}

	/**
	 * Restituisce gli stati.
	 * @return informazioni sugli stati
	 */
	public Collection<ASignStatusDTO> getStates() {
		return states;
	}
	
	/**
	 * Restituisce la data di ultimo retry.
	 * @return data ultimo retry
	 */
	public Date getDataUltimoRetry() {
		return dataUltimoRetry;
	}

	/**
	 * Imposta la data dell'ultimo retry.
	 * @param dataUltimoRetry
	 */
	public void setDataUltimoRetry(Date dataUltimoRetry) {
		this.dataUltimoRetry = dataUltimoRetry;
	}

	/**
	 * Restituisce l'id dell'item.
	 * @return id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Imposta l'id dell'item.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Restituisce il numero di retry.
	 * @return numero retry
	 */
	public Integer getnRetry() {
		return nRetry;
	}

	/**
	 * Imposta il numero di retry.
	 * @param nRetry
	 */
	public void setnRetry(Integer nRetry) {
		this.nRetry = nRetry;
	}

	/**
	 * Restituisce lo step attuale.
	 * @return step
	 */
	public StepEnum getStep() {
		return step;
	}

	/**
	 * Imposta lo step attuale.
	 * @param step
	 */
	public void setStep(StepEnum step) {
		this.step = step;
	}

	/**
	 * Restituisce lo stato attuale.
	 * @return status
	 */
	public StatusEnum getStatus() {
		return status;
	}

	/**
	 * Imposta lo status attuale.
	 * @param status
	 */
	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	/**
	 * Restituisce il document title del documento di riferimento dell'item.
	 * @return doc title
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta il document title del documento di riferimento dell'item.
	 * @param documentTitle
	 */
	public void setDocumentTitle(String documentTitle) {
		this.documentTitle = documentTitle;
	}
	
	/**
	 * Restituisce il numero documento di riferimento dell'item.
	 * @return numero documento
	 */
	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * Imposta il numero documento di riferimento dell'item.
	 * @param numeroDocumento
	 */
	public void setNumeroDocumento(Integer numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * Restituisce l'id dell'AOO.
	 * @return id aoo
	 */
	public Long getIdAoo() {
		return idAoo;
	}

	/**
	 * Imposta l'id dell'AOO.
	 * @param idAoo
	 */
	public void setIdAoo(Long idAoo) {
		this.idAoo = idAoo;
	}

	/**
	 * Restituisce la priorità di lavorazione dell'item.
	 * @return priorità
	 */
	public Integer getPriority() {
		return priority;
	}

	/**
	 * Imposta la priorità dell'item.
	 * @param priority
	 */
	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	/**
	 * Restituisce il flag associato all'avvenuta comunicazione dell'errore.
	 * @return flag comunicazione errore
	 */
	public Boolean getFlagComunicato() {
		return flagComunicato;
	}

	/**
	 * Imposta il flag associato all'avvenuta comunicazione dell'errore.
	 * @param flagComunicato
	 */
	public void setFlagComunicato(Boolean flagComunicato) {
		this.flagComunicato = flagComunicato;
	}

	/**
	 * Restituisce il numero protocollo se il documento a cui fa riferimento l'item è protocollato.
	 * @return numero protocollo se protocollato, null altrimenti
	 */
	public Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Imposta il numero protocollo se il documento a cui fa riferimento l'item è protocollato.
	 * @param numeroProtocollo
	 */
	public void setNumeroProtocollo(Integer numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	/**
	 * Restituisce l'anno protocollo se il documento a cui fa riferimento l'item è protocollato.
	 * @return anno protocollo se protocollato, null altrimenti
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * Imposta l'anno protocollo se il documento a cui fa riferimento l'item è protocollato.
	 * @param annoProtocollo
	 */
	public void setAnnoProtocollo(Integer annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/**
	 * Restituisce l'id del firmatario.
	 * @return id firmatario
	 */
	public Long getIdFirmatario() {
		return idFirmatario;
	}

	/**
	 * Imposta l'id del firmatario.
	 * @param idFirmatario
	 */
	public void setIdFirmatario(Long idFirmatario) {
		this.idFirmatario = idFirmatario;
	}

	/**
	 * Restituisce l'id del ruolo del firmatario.
	 * @return id ruolo firmatario
	 */
	public Long getIdRuoloFirmatario() {
		return idRuoloFirmatario;
	}

	/**
	 * Imposta l'id del ruolo del firmatario.
	 * @param idRuoloFirmatario
	 */
	public void setIdRuoloFirmatario(Long idRuoloFirmatario) {
		this.idRuoloFirmatario = idRuoloFirmatario;
	}

	/**
	 * Restituisce l'id dell'ufficio del firmatario.
	 * @return id ufficio firmatario
	 */
	public Long getIdUfficioFirmatario() {
		return idUfficioFirmatario;
	}

	/**
	 * Imposta l'id dell'ufficio del firmatario.
	 * @param idUfficioFirmatario
	 */
	public void setIdUfficioFirmatario(Long idUfficioFirmatario) {
		this.idUfficioFirmatario = idUfficioFirmatario;
	}
	
	/**
	 * Restituisce la modalità di firma.
	 * @return modalità firma
	 */
	public SignModeEnum getSignMode() {
		return signMode;
	}

	/**
	 * Imposta la modalità di firma.
	 * @param signMode
	 */
	public void setSignMode(SignModeEnum signMode) {
		this.signMode = signMode;
	}

	/**
	 * Restituisce la tipologia di firma.
	 * @return tipo firma
	 */
	public SignTypeGroupEnum getSignType() {
		return signType;
	}

	/**
	 * Imposta la tipologia di firma.
	 * @param signType
	 */
	public void setSignType(SignTypeGroupEnum signType) {
		this.signType = signType;
	}
	
	/**
	 * Restituisce il wobnumber a cui fa riferimento l'item.
	 * @return wobnumber
	 */
	public String getWobNumber() {
		return wobNumber;
	}

	/**
	 * Imposta il wobnumber a cui fa riferimento l'item.
	 * @param wobNumber
	 */
	public void setWobNumber(String wobNumber) {
		this.wobNumber = wobNumber;
	}

	/**
	 * Restituisce la coda sulla quale si trova l'item in fase di processamento degli step di firma.
	 * @return coda item
	 */
	public DocumentQueueEnum getQueue() {
		return queue;
	}

	/**
	 * Imposta la coda sulla quale si trova l'item in fase di processamento degli step di firma.
	 * @param queue
	 */
	public void setQueue(DocumentQueueEnum queue) {
		this.queue = queue;
	}
	
	/**
	 * Restituisce le informazioni per il popolamento del master in fase di visualizzazione dalle code.
	 * @return informazioni master
	 */
	public ASignMasterDTO getMaster() {
		ASignMasterDTO out = new ASignMasterDTO();
		out.setStatus(status);
		
		String retryInfo = "";
		if (StatusEnum.RETRY.equals(status)) {
			String ultimoRetry = "ND";
			if (dataUltimoRetry!=null) {
				ultimoRetry = new SimpleDateFormat(DateUtils.DD_MM_YYYY_HH_MM_SS).format(dataUltimoRetry);
			}
			
			retryInfo = " [Tentativi:" + nRetry + "/" + PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ASIGN_MAX_RETRY) +  " - Data ultimo tentativo: " + ultimoRetry + "]";
		}
		String st = "ND";
		if (step!=null) {
			st = step.getDisplayName();
		}
		String coda = "ND";
		if (queue!=null) {
			coda = queue.getName();
		}
		out.setInfo("Coda: " + coda + " - " + "Stato: " + status + " - Step in corso: " + st + retryInfo);
		return out;
	}

}
