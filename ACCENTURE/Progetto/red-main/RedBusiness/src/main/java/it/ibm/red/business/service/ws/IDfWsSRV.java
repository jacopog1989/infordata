package it.ibm.red.business.service.ws;

import it.ibm.red.business.service.ws.facade.IDfWsFacadeSRV;

/**
 * The Interface IDfWsSRV.
 *
 * @author a.dilegge
 * 
 *         Interfaccia servizio gestione web service dedicati al DF.
 */
public interface IDfWsSRV extends IDfWsFacadeSRV {

	
	
}