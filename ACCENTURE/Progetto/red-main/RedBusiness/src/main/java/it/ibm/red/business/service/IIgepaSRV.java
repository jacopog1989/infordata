package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IIgepaFacadeSRV;

/**
 * Interface del servizio gestione Igepa.
 */
public interface IIgepaSRV extends IIgepaFacadeSRV {

}
