package it.ibm.red.business.dto;

/**
 * Parametri di ricerca elenco notifiche e conti consuntivi.
 */
public class ParamsRicercaContiConsuntiviDTO extends AbstractDTO {

	/**
	 * serial version UID.
	 */
	private static final long serialVersionUID = 5882280745370624877L;

	/**
	 * Numero esercizio.
	 */
	private Integer esercizio;
	
	/**
	 * Sede estera selezionata.
	 */
	private String sedeEstera;
	
	/**
	 * Restituisce il numero esercizio.
	 * 
	 * @return numero esercizio
	 */
	public Integer getEsercizio() {
		return esercizio;
	}

	/**
	 * Imposta il numero esercizio.
	 * 
	 * @param esercizio
	 */
	public void setEsercizio(Integer esercizio) {
		this.esercizio = esercizio;
	}

	/**
	 * Restituisce la sede estera selezionata.
	 * 
	 * @return sede estera selezionata
	 */
	public String getSedeEstera() {
		return sedeEstera;
	}

	/**
	 * Imposta la sede estera selezionata.
	 * 
	 * @param sedeEstera
	 */
	public void setSedeEstera(String sedeEstera) {
		this.sedeEstera = sedeEstera;
	}
	
}
