package it.ibm.red.business.dto;

import java.util.HashMap;
import java.util.Map;

/**
 * DTO che definisce il risultato SIGI.
 */
public class SigiResultDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 3637717642256254546L;
	
	/**
	 * Risultato.
	 */
	private final Map<String, Map<Integer, Boolean>> result;

	/**
	 * Costruttore.
	 */
	public SigiResultDTO() {
		result = new HashMap<>();
	}

	/**
	 * Aggiunge il decreto da lavorare identificato dall'id alla lista dei risultati SIGI.
	 * @param numDecreto
	 * @param numeroOP
	 * @param bOk
	 */
	public void add(final String numDecreto, final Integer numeroOP, final Boolean bOk) {
		result.computeIfAbsent(numDecreto, k -> new HashMap<>()).put(numeroOP, bOk);
	}

	/**
	 * Restituisce la lista dei risultati.
	 * @return risultati
	 */
	public Map<String, Map<Integer, Boolean>> getResult() {
		return result;
	}

}
