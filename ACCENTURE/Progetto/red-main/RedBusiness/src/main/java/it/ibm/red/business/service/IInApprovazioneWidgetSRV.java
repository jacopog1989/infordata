package it.ibm.red.business.service;
 
import it.ibm.red.business.service.facade.IInApprovazioneWidgetFacadeSRV;

/**
 * Interface del servizio gestione wiget di approvazione.
 */
public interface IInApprovazioneWidgetSRV extends IInApprovazioneWidgetFacadeSRV {
	
}
