/**
 * 
 */
package it.ibm.red.business.service.concrete;

import java.util.Comparator;

import it.ibm.red.business.dto.MasterDocumentRedDTO;

/**
 * @author APerquoti
 * 
 * Comparatore di documenti utilizzato per i DTO di liste di documneti Generici
 *
 */
public class ListaDocumentiComparator implements Comparator<MasterDocumentRedDTO> {

	/**
	 * Compare.
	 *
	 * @param master1
	 *            the master 1
	 * @param master2
	 *            the master 2
	 * @return the int
	 */
	@Override
	public int compare(final MasterDocumentRedDTO master1, final MasterDocumentRedDTO master2) {
		int output;
		if (master1.equals(master2)) {
			output = 0;
		} else {
			output = master2.getDataCreazione().compareTo(master1.getDataCreazione());
			if (output == 0) {
				output = master2.getWobNumber().compareTo(master1.getWobNumber());
			}
		}
		return output;
	}

}
