package it.ibm.red.business.enums;

/**
 * Enum che definisce i tipi di metadati per template.
 */
public enum TipoTemplateMetadatoEnum {

	/**
	 * Valore.
	 */
	TEXTBOX(1),
	
	/**
	 * Valore.
	 */
	TEXTAREA(2);

	/**
	 * Identificativo.
	 */
	private Integer id;
	
	TipoTemplateMetadatoEnum(final Integer id) {
		this.id = id;
	}

	/**
	 * Restituisce l'id.
	 * @return id
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Restituisce l'enum associato al value.
	 * @param id
	 * @return enum associato
	 */
	public static TipoTemplateMetadatoEnum getById(final Integer id) {
		for (final TipoTemplateMetadatoEnum m: TipoTemplateMetadatoEnum.values()) {
			if (m.getId().equals(id)) {
				return m;
			}
		}
		
		return null;
	}
	
}
