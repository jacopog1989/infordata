package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.Date;

import it.ibm.red.business.enums.StatoElabNotificaNpsEnum;
import it.ibm.red.business.utils.EmailUtils;

/**
 * The Class NotificaOperazioneNpsDTO.
 *
 * @author a.dilegge
 * 
 * 	DTO per le notifiche operazioni di NPS.
 */
public class NotificaOperazioneNpsDTO extends NotificaNpsDTO implements Serializable {

	/** 
	 * The Constant serialVersionUID. 
	 */
	private static final long serialVersionUID = -1144979263335596998L;

	/** 
	 * Identificativo messaggio email 
	 */

	private final String messageIdMail;
	
	/** 
	 * Esito notifica  
	 */

	private final String esitoNotifica;
	
	/** 
	 * Email destinatario 
	 */

	private final String emailDestinatario;
	
	
	/**
	 * Costruttore notifica operazione nps DTO.
	 *
	 * @param messageIdNPS
	 *            the message id NPS
	 * @param idMessaggioNPS
	 *            the id messaggio NPS
	 * @param messageIdMail
	 *            the message id mail
	 * @param codiceAooNPS
	 *            the codice aoo NPS
	 * @param dataProtocollo
	 *            the data protocollo
	 * @param numeroProtocollo
	 *            the numero protocollo
	 * @param annoProtocollo
	 *            the anno protocollo
	 * @param tipoProtocollo
	 *            the tipo protocollo
	 * @param idProtocollo
	 *            the id protocollo
	 * @param dataNotifica
	 *            the data notifica
	 * @param tipoNotifica
	 *            the tipo notifica
	 * @param esitoNotifica
	 *            the esito notifica
	 * @param emailDestinatario
	 *            the email destinatario
	 */
	public NotificaOperazioneNpsDTO(final String messageIdNPS, final String idMessaggioNPS, final String messageIdMail, final String codiceAooNPS,
			final Date dataProtocollo, final int numeroProtocollo, final int annoProtocollo, final int tipoProtocollo, final String idProtocollo,
			final Date dataNotifica, final String tipoNotifica, final String esitoNotifica, final String emailDestinatario) {
		super(messageIdNPS, idMessaggioNPS, codiceAooNPS, dataProtocollo, numeroProtocollo, annoProtocollo, 
				idProtocollo, tipoProtocollo, dataNotifica, tipoNotifica);
		this.messageIdMail = EmailUtils.getMessageIdConParentesi(messageIdMail);
		this.esitoNotifica = esitoNotifica;
		this.emailDestinatario = emailDestinatario;
	}
	
	/**
	 * Costruttore notifica operazione nps DTO.
	 *
	 * @param idCoda the id coda
	 * @param messageIdNPS the message id NPS
	 * @param idDocumentoNPS the id documento NPS
	 * @param messageIdMail the message id mail
	 * @param codiceAooNPS the codice aoo NPS
	 * @param dataProtocollo the data protocollo
	 * @param numeroProtocollo the numero protocollo
	 * @param annoProtocollo the anno protocollo
	 * @param tipoProtocollo the tipo protocollo
	 * @param idProtocollo the id protocollo
	 * @param dataNotifica the data notifica
	 * @param tipoNotifica the tipo notifica
	 * @param esitoNotifica the esito notifica
	 * @param emailDestinatario the email destinatario
	 * @param idAoo the id aoo
	 * @param dataRicezioneNotifica the data ricezione notifica
	 * @param stato the stato
	 * @param errore the errore
	 */
	public NotificaOperazioneNpsDTO(final Long idCoda, final String messageIdNPS, final String idDocumentoNPS, final String messageIdMail, final String codiceAooNPS,
			final Date dataProtocollo, final int numeroProtocollo, final int annoProtocollo, final int tipoProtocollo, final String idProtocollo,
			final Date dataNotifica, final String tipoNotifica, final String esitoNotifica, final String emailDestinatario, final Long idAoo, final Date dataRicezioneNotifica,
			final StatoElabNotificaNpsEnum stato, final String errore) {
		super(idCoda, messageIdNPS, idDocumentoNPS, codiceAooNPS, dataProtocollo, numeroProtocollo, annoProtocollo, idProtocollo, 
				tipoProtocollo, dataNotifica, tipoNotifica, idAoo, dataRicezioneNotifica, stato, errore);
		this.messageIdMail = EmailUtils.getMessageIdConParentesi(messageIdMail);
		this.esitoNotifica = esitoNotifica;
		this.emailDestinatario = emailDestinatario;
	}

	/** 
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	/** 
	 *
	 * @return the message id mail
	 */
	public String getMessageIdMail() {
		return messageIdMail;
	}

	/** 
	 *
	 * @return the esito notifica
	 */
	public String getEsitoNotifica() {
		return esitoNotifica;
	}
	
	/** 
	 *
	 * @return the email destinatario
	 */
	public String getEmailDestinatario() {
		return emailDestinatario;
	}
	
}