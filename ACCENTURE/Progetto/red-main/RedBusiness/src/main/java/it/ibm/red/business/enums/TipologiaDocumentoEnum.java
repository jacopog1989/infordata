package it.ibm.red.business.enums;

/**
 * The Enum TipologiaDocumentoEnum.
 *
 * @author CPIERASC
 * 
 *         Enumerato utilizzato per definire la tipologia di un documento.
 */
public enum TipologiaDocumentoEnum {
	
	/**
	 * Parere.
	 */
	PARERE("Parere"),
	
	/**
	 * Atto Decreto
	 */
	ATTO_DECRETO("Atto Decreto"), // id salvato tra le properties su DB, cambia tra ambienti diversi
	
	/**
	 * Bilancio enti entrata
	 */
	BILANCIO_ENTI_ENTRATA("Bilancio enti entrata"),
	
	/**
	 * Bilancio enti uscita
	 */
	BILANCIO_ENTI_USCITA("Bilancio enti uscita");

	/**
	 * Descrizione tipologia.
	 */
	private String descrizione;
	
	/**
	 * Costruttore.
	 * 
	 * @param inId			identificativo
	 * @param inDescrizione	descrizione
	 */
	TipologiaDocumentoEnum(final String inDescrizione) {
		this.descrizione = inDescrizione;
	}

	/**
	 * Getter descrizione.
	 * 
	 * @return	descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}



}
