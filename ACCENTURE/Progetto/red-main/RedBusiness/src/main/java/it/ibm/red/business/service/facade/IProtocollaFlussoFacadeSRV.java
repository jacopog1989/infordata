/**
 * 
 */
package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.persistence.model.AooFilenet; 

/**
 * Facade del servizio di protocollazione flusso.
 */
public interface IProtocollaFlussoFacadeSRV extends Serializable {
	
	/**
	 * @param documentTitle
	 * @param aooFilenet
	 * @return
	 */
	String getWobNumberSospeso(String documentTitle, AooFilenet aooFilenet);

	/**
	 * @param documentTitle
	 * @param aooFilenet
	 * @return
	 */
	String getQueueNameWorkflowPrincipale(String documentTitle, AooFilenet aooFilenet);
	
	/**
	 * @param aooFilenet
	 * @param flusso
	 * @param idProcesso
	 * @return
	 */
	String getDocumentTitleByIdProcesso(final AooFilenet aooFilenet, TipoContestoProceduraleEnum flusso, String idProcesso);

	/**
	 * @param descrizioneNodo
	 * @param codiceAoo
	 * @return
	 */
	Long getIdNodoByDescAndCodiceAoo(String descrizioneNodo, String codiceAoo);


}
