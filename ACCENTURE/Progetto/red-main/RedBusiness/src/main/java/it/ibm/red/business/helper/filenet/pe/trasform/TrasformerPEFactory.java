package it.ibm.red.business.helper.filenet.pe.trasform;

import java.util.EnumMap;

import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.exception.NoTrasformerException;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.ContextFromWFToDocumentoFEPATrasformer;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.ContextFromWFToDocumentoTrasformer;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.FromWFToDocumentoScadenzatoTrasformer;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.FromWFToDocumentoTrasformer;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.FromWFToEvento;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.ModificaIterTrasformer;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;

/**
 * Factory delle trasformazioni PE.
 */
@SuppressWarnings("rawtypes")
public final class TrasformerPEFactory {
	
	/**
	 * Trasformatori.
	 */
	private static EnumMap<TrasformerPEEnum, TrasformerPE> trasformers;
	
	/**
	 * Singleton.
	 */
	private static TrasformerPEFactory instance = new TrasformerPEFactory();
	
	/**
	 * Costruttore.
	 */
	private TrasformerPEFactory() {
		trasformers = new EnumMap<>(TrasformerPEEnum.class);
		addTrasformer(new FromWFToDocumentoTrasformer());
		addTrasformer(new ModificaIterTrasformer());
		addTrasformer(new ContextFromWFToDocumentoFEPATrasformer());
		addTrasformer(new FromWFToDocumentoScadenzatoTrasformer());
		addTrasformer(new FromWFToEvento());
		addTrasformer(new ContextFromWFToDocumentoTrasformer());
	}

	/**
	 * Metodo per aggiungere una trasformazione.
	 * 
	 * @param t	trasformatore
	 */
	private static void addTrasformer(final TrasformerPE t) {
		trasformers.put(t.getEnumKey(), t);
	}

	/**
	 * Recupero istanza singleton.
	 * 
	 * @return	istanza
	 */
	public static TrasformerPEFactory getInstance() {
		return instance;
	}

	/**
	 * Metodo per il recupero di un trasformatore.
	 * 
	 * @param te	enum caratteristica trasformatore
	 * @return		trasformatore
	 */
	public static TrasformerPE getTrasformer(final TrasformerPEEnum te) {
		TrasformerPE tr = trasformers.get(te);
		if (tr == null) {
			throw new NoTrasformerException(te);
		}
		return tr;
	}

}