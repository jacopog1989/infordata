/**
 * 
 */
package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IRegistroAusiliarioFacadeSRV;

/**
 * Interfaccia del servizio di gestione registri ausiliari.
 */
public interface IRegistroAusiliarioSRV extends IRegistroAusiliarioFacadeSRV {

}
