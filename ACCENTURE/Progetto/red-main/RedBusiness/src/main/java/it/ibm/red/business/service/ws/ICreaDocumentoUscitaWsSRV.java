package it.ibm.red.business.service.ws;

import it.ibm.red.business.service.ws.facade.ICreaDocumentoUscitaWsFacadeSRV;


/**
 * Servizio per la creazione di documenti in uscita tramite web service.
 * 
 * @author m.crescentini
 *
 */
public interface ICreaDocumentoUscitaWsSRV extends ICreaDocumentoUscitaWsFacadeSRV {

	
}