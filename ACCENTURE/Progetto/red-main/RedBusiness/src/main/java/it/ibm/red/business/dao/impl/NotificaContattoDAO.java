package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.INotificaContattoDAO;
import it.ibm.red.business.enums.StatoNotificaContattoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.NotificaContatto;

/**
 * DAO gestione notifiche contatto.
 */
@Repository
public class NotificaContattoDAO extends AbstractDAO implements INotificaContattoDAO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 5174569776307091664L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotificaContattoDAO.class);
	
	/**
	 * Query parziale.
	 */
	private static final String FROM_NOTIFICACONTATTO_TAB1_LEFT_OUTER_JOIN_STATO_NOTIFICA_RUBRICA_SNR_ON_TAB1_IDNOTIFICACONTATTO_SNR_IDNOTIFICARUBRICA = "FROM NOTIFICACONTATTO tab1 LEFT OUTER JOIN STATO_NOTIFICA_RUBRICA SNR ON tab1.IDNOTIFICACONTATTO = SNR.IDNOTIFICARUBRICA ";

	/**
	 * Query parziale.
	 */
	private static final String AND_SNR_IDUTENTE = " AND SNR.IDUTENTE = ? ";

	/**
	 * Messaggio errore inserimento notifica.
	 */
	private static final String ERROR_INSERIMENTO_NOTIFICA_CONTATTO_MSG = "Errore durante l'inserimento della notificaContatto al DB. ";

	/**
	 * Messaggio errore recupero lista notifiche.
	 */
	private static final String ERROR_RECUPERO_LISTA_NOTIFICA_MSG = "Errore durante il recupero della lista notifica contatti: ";

	/**
	 * @see it.ibm.red.business.dao.INotificaContattoDAO#getListaNotificaByIdContatto(int,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public List<NotificaContatto> getListaNotificaByIdContatto(final int idContatto, final String tipoNotifica, final Connection con)  {
		PreparedStatement ps = null;
		ResultSet rs = null;
			
		final StringBuilder query = new StringBuilder();
		try {

			query.append("SELECT * FROM NOTIFICACONTATTO");
			query.append(" WHERE idContatto = " + idContatto);
			if (tipoNotifica != null && !"".equals(tipoNotifica)) {
				query.append(" AND UPPER(NOTIFICACONTATTO.tiponotifica) = UPPER('" + tipoNotifica + "')");
			}
			
			LOGGER.info("ricerca notrifica in base all'id contatto: " + query.toString());

			ps = con.prepareStatement(query.toString());
			rs = ps.executeQuery();
			
			return fetchMultiResult(rs, false);
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_LISTA_NOTIFICA_MSG + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}
	
	/**
	 * @see it.ibm.red.business.dao.INotificaContattoDAO#getNotificaContatto(int,
	 *      java.sql.Connection).
	 */
	@Override
	public NotificaContatto getNotificaContatto(final int idNotifica, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		NotificaContatto notifica = null;
		
		final StringBuilder query = new StringBuilder();
		try {

			query.append("SELECT * FROM NOTIFICACONTATTO");
			query.append(" WHERE idNotificaContatto = " + idNotifica);
			
			LOGGER.info("ricerca notrifica in base all'id contatto: " + query.toString());

			ps = con.prepareStatement(query.toString());
			rs = ps.executeQuery();
			
			if (rs.next()) {
				notifica = populateVO(rs);
				notifica.setStatoNotificaRubrica(rs.getInt("STATO_NOTIFICA"));
				cleanNotificaContatto(notifica);
			}
			
			return notifica;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero della notifica con idNotificaContatto= " + idNotifica + " " + e);
			throw new RedException("Errore durante il recupero della notificaContatto con idNotificaContatto: ");
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}
	
	/**
	 * @see it.ibm.red.business.dao.INotificaContattoDAO#insertNotificaContatto(it.ibm.red.business.persistence.model.NotificaContatto,
	 *      java.sql.Connection).
	 */
	@Override
	public void insertNotificaContatto(final NotificaContatto notificaContatto, final Connection con) {
		PreparedStatement ps = null;
		final ResultSet rs = null;
		try {
			int index = 1;
			ps = con.prepareStatement(" INSERT INTO NOTIFICACONTATTO (IDNOTIFICACONTATTO, IDCONTATTO, IDNODOMODIFICA, IDUTENTEMODIFICA, UTENTE, DATAOPERAZIONE, OPERAZIONEEFFETTUATA, ALIAS, NOME, COGNOME, MAIL, MAILPEC, TIPONOTIFICA, VECCHIOVALORE, NUOVOVALORE, NOTE, TIPOLOGIACONTATTO, MOTIVORIFIUTO, STATO, IDAOO) " 
					+ " VALUES (SEQ_NOTIFICACONTATTO.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
			ps.setLong(index++, notificaContatto.getIdContatto());
			ps.setLong(index++, notificaContatto.getIdNodoModifica());
			ps.setLong(index++, notificaContatto.getIdUtenteModifica());
			ps.setString(index++, notificaContatto.getUtente());
			ps.setDate(index++, new java.sql.Date((notificaContatto.getDataOperazione()).getTime()));
			ps.setString(index++, notificaContatto.getOperazioneEnum().getTipoOperazione());
			ps.setString(index++, notificaContatto.getAlias());
			ps.setString(index++, notificaContatto.getNome()); 
			ps.setString(index++, notificaContatto.getCognome()); 
			ps.setString(index++, notificaContatto.getMail()); 
			ps.setString(index++, notificaContatto.getMailPec()); 
			ps.setString(index++, notificaContatto.getTipoNotifica()); 
			ps.setString(index++, notificaContatto.getVecchioValore()); 
			ps.setString(index++, notificaContatto.getNuovoValore()); 
			ps.setString(index++, notificaContatto.getNote()); 
			ps.setString(index++, notificaContatto.getTipologiaContatto()); 
			ps.setString(index++, notificaContatto.getMotivoRifiuto());
			ps.setString(index++, notificaContatto.getStato().getNome());
			ps.setLong(index++, notificaContatto.getIdAoo());
			ps.executeUpdate();
			
		} catch (final SQLException e) {
			LOGGER.error(ERROR_INSERIMENTO_NOTIFICA_CONTATTO_MSG + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}
	
	/**
	 * Popola un singolo bean NotificaContatto.
	 * 
	 * @param rs
	 *            ResultSet dal quale recuperare le informazioni della notifica.
	 * @return Notifica Contatto popolata.
	 * @throws SQLException
	 */
	private static NotificaContatto populateVO(final ResultSet rs) throws SQLException {
		
		final NotificaContatto notificaContatto = new NotificaContatto();
		notificaContatto.setIdNotificaContatto(rs.getInt("IDNOTIFICACONTATTO"));
		notificaContatto.setIdContatto(rs.getLong("IDCONTATTO"));
		notificaContatto.setIdNodoModifica(rs.getLong("IDNODOMODIFICA"));
		notificaContatto.setDataOperazione(rs.getTimestamp("DATAOPERAZIONE"));
		notificaContatto.setOperazioneEffettuata(rs.getString("OPERAZIONEEFFETTUATA"));
		notificaContatto.setIdUtenteModifica(rs.getLong("IDUTENTEMODIFICA"));
		notificaContatto.setAlias(rs.getString("ALIAS"));
		notificaContatto.setNome(rs.getString("NOME"));
		notificaContatto.setCognome(rs.getString("COGNOME"));
		notificaContatto.setMail(rs.getString("MAIL"));
		notificaContatto.setMailPec(rs.getString("MAILPEC"));
		notificaContatto.setUtente(rs.getString("UTENTE"));
		notificaContatto.setTipoNotifica(rs.getString("TIPONOTIFICA"));
		notificaContatto.setVecchioValore(rs.getString("VECCHIOVALORE"));
		notificaContatto.setNuovoValore(rs.getString("NUOVOVALORE"));
		notificaContatto.setNote(rs.getString("NOTE"));
		notificaContatto.setTipologiaContatto(rs.getString("TIPOLOGIACONTATTO"));
		notificaContatto.setMotivoRifiuto(rs.getString("MOTIVORIFIUTO"));
		notificaContatto.setStato(StatoNotificaContattoEnum.get(rs.getString("STATO")));
		notificaContatto.setIdAoo(rs.getLong("IDAOO"));
		return notificaContatto;
	}
	
	/**
	 * Metodo che setta a stringa vuota i valori che sono a NULL.
	 * @param nc
	 */
	private static void cleanNotificaContatto(final NotificaContatto nc) {
		if (nc.getNome() == null) {
			nc.setNome("");
		}
		if (nc.getUtente() == null) {
			nc.setUtente("");
		}
		if (nc.getOperazioneEffettuata() == null) {
			nc.setOperazioneEffettuata("");
		}
		if (nc.getAlias() == null) {
			nc.setAlias("");
		}
		if (nc.getCognome() == null) {
			nc.setCognome("");
		}
		if (nc.getMail() == null) {
			nc.setMail("");
		}
		if (nc.getMailPec() == null) {
			nc.setMailPec("");
		}
		if (nc.getVecchioValore() == null) {
			nc.setVecchioValore("");
		}
		if (nc.getNuovoValore() == null) {
			nc.setNuovoValore("");
		} 
		if (nc.getNote() == null) {
			nc.setNote("");
		}
		if (nc.getTipoNotifica() == null) {
			nc.setTipoNotifica("");
		}
		if (nc.getMotivoRifiuto() == null) {
			nc.setMotivoRifiuto("");
		}
	}
	
	/**
	 * Ritorna una lista di beans NotificaContatto in base al resultSet.
	 * 
	 * @param rs
	 *            ResultSet
	 * @throws SQLException
	 */
	private static List<NotificaContatto> fetchMultiResult(final ResultSet rs, final boolean consideraStatoNotifica)
			throws SQLException {
		final List<NotificaContatto> resultList = new ArrayList<>();
		while (rs.next()) {
			
			NotificaContatto notificaContatto = populateVO(rs);
			if (consideraStatoNotifica) {
				notificaContatto.setStatoNotificaRubrica(rs.getInt("STATO_NOTIFICA"));
			}

			cleanNotificaContatto(notificaContatto);
			resultList.add(notificaContatto);
		}
		return resultList;
	}
	
	private static List<NotificaContatto> fetchMultiResultNew(final ResultSet rs) throws SQLException {
		final List<NotificaContatto> resultList = new ArrayList<>();
		while (rs.next()) {
			NotificaContatto notificaContatto = populateVO(rs);
			notificaContatto.setStatoNotificaRubrica(rs.getInt("STATO_NOTIFICA_RUBRICA"));
			cleanNotificaContatto(notificaContatto);
			
			resultList.add(notificaContatto);
		}
		return resultList;
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaContattoDAO#getListaNotificheRichieste(int,
	 *      java.lang.Integer, java.lang.Long, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<NotificaContatto> getListaNotificheRichieste(final int numGiorni, final Integer inNumRow, final Long idAoo, final Long idUtente, final Connection con) {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
				
		try {
			int index = 1;
			ps = con.prepareStatement("SELECT * FROM(SELECT NOTCONT.*,SNR.IDNOTIFICARUBRICA,SNR.IDUTENTE,SNR.STATO_NOTIFICA AS STATO_NOTIFICA_RUBRICA "
					+ " FROM NOTIFICACONTATTO "
					+ " NOTCONT LEFT OUTER JOIN STATO_NOTIFICA_RUBRICA SNR ON"
					+ " NOTCONT.IDNOTIFICACONTATTO = SNR.IDNOTIFICARUBRICA"
					+ AND_SNR_IDUTENTE
					+ " WHERE UPPER(NOTCONT.tiponotifica) = UPPER(?) "
					+ " AND NOTCONT.DATAOPERAZIONE >= sysdate - ? "
					+ " AND UPPER(OPERAZIONEEFFETTUATA) not like '%AUTOM%'"
					+ " AND NOTCONT.STATO like ? "
					+ " AND NOTCONT.IDAOO = ? "
					+ " )RISULTATI JOIN CONTATTO CONT"
					+ " ON RISULTATI.IDCONTATTO = CONT.IDCONTATTO"
					+ " ORDER BY RISULTATI.DATAOPERAZIONE ASC");
			ps.setLong(index++, idUtente);
			ps.setString(index++, NotificaContatto.TIPO_NOTIFICA_RICHIESTA);
			ps.setInt(index++, numGiorni);
			ps.setString(index++, StatoNotificaContattoEnum.IN_ATTESA.getNome());
			ps.setLong(index++, idAoo);
			rs = ps.executeQuery();
			final List<NotificaContatto> listaNotifiche = fetchMultiResultNew(rs);
			if(inNumRow!=null) {
				int numRow = inNumRow;	
				numRow = numRow > listaNotifiche.size() ? listaNotifiche.size() : numRow;
				listaNotifiche.subList(0, numRow);
			}		
			return listaNotifiche;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero della lista notifiche richieste.", e);
			throw new RedException("Errore durante il recupero della lista notifiche richieste.", e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaContattoDAO#getListaNotificheRichiesteHome(int,
	 *      int, int, java.lang.Long, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<NotificaContatto> getListaNotificheRichiesteHome(final int numGiorni, final int maxNotificheNonLette, final int inNumRow, final Long idAoo, final Long idUtente, final Connection con) {
		
		PreparedStatement ps = null;
		ResultSet rs = null; 		
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder("SELECT * FROM (SELECT NOTCONT.*,SNR.IDNOTIFICARUBRICA,SNR.IDUTENTE,SNR.STATO_NOTIFICA AS STATO_NOTIFICA_RUBRICA ");
			sb.append(getFromClauseNotificaContatto(maxNotificheNonLette));
			
			sb.append("UNION ALL ");
			sb.append("SELECT NOTCONT.*,SNR.IDNOTIFICARUBRICA,SNR.IDUTENTE,SNR.STATO_NOTIFICA AS STATO_NOTIFICA_RUBRICA "); 
			sb.append("FROM NOTIFICACONTATTO  NOTCONT LEFT OUTER JOIN STATO_NOTIFICA_RUBRICA SNR "); 
			sb.append("ON NOTCONT.IDNOTIFICACONTATTO = SNR.IDNOTIFICARUBRICA ");
			sb.append(AND_SNR_IDUTENTE);
			sb.append("WHERE NOTCONT.IDAOO = ? "); 
			sb.append("AND NOTCONT.DATAOPERAZIONE >= sysdate - ? AND snr.stato_notifica = 1 ");
			if (inNumRow > 0) {
				sb.append("AND ROWNUM < ?");
			}
			
			sb.append(") RISULTATI"); 
			sb.append(" ORDER BY RISULTATI.DATAOPERAZIONE ASC");
			ps = con.prepareStatement(sb.toString());
			
			ps.setLong(index++, idUtente);
			ps.setString(index++, NotificaContatto.TIPO_NOTIFICA_RICHIESTA);
			ps.setString(index++, StatoNotificaContattoEnum.IN_ATTESA.getNome());
			ps.setLong(index++, idAoo);
			if (maxNotificheNonLette > 0) {
				ps.setInt(index++, maxNotificheNonLette); 
			}
			ps.setLong(index++, idUtente);
			 
			ps.setLong(index++, idAoo);
			ps.setInt(index++, numGiorni);
			if (inNumRow > 0) {
				ps.setInt(index++, inNumRow);
			}
			
			rs = ps.executeQuery();
			return fetchMultiResultNew(rs);
			  
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_LISTA_NOTIFICA_MSG + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}
	
	/**
	 * @see it.ibm.red.business.dao.INotificaContattoDAO#getCountListaNotificheRichieste(java.lang.Long,
	 *      int, java.sql.Connection).
	 */
	@Override
	public int getCountListaNotificheRichieste(final Long idAoo, final int numGiorni, final Connection con) {
		int numeroNotifiche = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			int index = 1;
			ps = con.prepareStatement("SELECT COUNT(T.IDNOTIFICACONTATTO) AS NUMERO_NOTIFICHE "
					+ " FROM NOTIFICACONTATTO T "
					+ " WHERE UPPER(T.STATO) like ? "
					+ " AND T.DATAOPERAZIONE > SYSDATE - ? "
					+ " AND IDAOO = ? ");
			ps.setString(index++, StatoNotificaContattoEnum.IN_ATTESA.getNome());
			ps.setInt(index++, numGiorni);
			ps.setLong(index++, idAoo);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				numeroNotifiche = rs.getInt("NUMERO_NOTIFICHE");
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero del conteggio  notifiche contatti: " + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
		
		return numeroNotifiche;
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaContattoDAO#getListaNotificheEvase(java.lang.Long,
	 *      int, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public List<NotificaContatto> getListaNotificheEvase(final Long idNodo, final int numGiorni, final Integer inNumRow, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int index = 1;
			ps = con.prepareStatement("SELECT * FROM " 
					+ "(SELECT tab1.IDNOTIFICACONTATTO, " 
					+ "  tab1.IDCONTATTO, " 
					+ "  tab1.IDNODOMODIFICA, " 
					+ " tab1.IDUTENTEMODIFICA, " 
					+ "  tab1.UTENTE, " 
					+ "  tab1.DATAOPERAZIONE, " 
					+ "  tab1.TIPONOTIFICA, " 
					+ "  tab1.OPERAZIONEEFFETTUATA, " 
					+ "  tab1.ALIAS, " 
					+ "  tab1.NOME, " 
					+ "  tab1.COGNOME, " 
					+ "  tab1.MAIL, " 
					+ "  tab1.MAILPEC, " 
					+ "  tab1.VECCHIOVALORE, " 
					+ "  tab1.NUOVOVALORE, " 
					+ "  tab1.NOTE, " 
					+ "  tab1.TIPOLOGIACONTATTO, " 
					+ "  tab1.MOTIVORIFIUTO, " 
					+ "  tab1.STATO, " 
					+ "  tab1.IDAOO " 
					+ " FROM NOTIFICACONTATTO tab1 " 
					+ " RIGHT JOIN PREFERITI tab2 " 
					+ " ON (tab1.IDCONTATTO = tab2.IDCONTATTORED OR tab1.IDCONTATTO  = tab2.IDCONTATTOIPA OR tab1.IDCONTATTO  = tab2.IDCONTATTOMEF) " 
					+ " WHERE UPPER(tab1.TIPONOTIFICA) LIKE UPPER(?) " 
					+ " AND tab2.IDNODO = ? " 
					+ " AND tab1.DATAOPERAZIONE >= sysdate - ? " 
					+ " UNION " 
					+ " SELECT tab1.IDNOTIFICACONTATTO, " 
					+ " tab1.IDCONTATTO, " 
					+ "  tab1.IDNODOMODIFICA, " 
					+ " tab1.IDUTENTEMODIFICA, " 
					+ " tab1.UTENTE, " 
					+ "  tab1.DATAOPERAZIONE, " 
					+ " tab1.TIPONOTIFICA, " 
					+ "  tab1.OPERAZIONEEFFETTUATA, " 
					+ "  tab1.ALIAS, " 
					+ "  tab1.NOME, " 
					+ "  tab1.COGNOME, " 
					+ "  tab1.MAIL, " 
					+ "  tab1.MAILPEC, " 
					+ "  tab1.VECCHIOVALORE, " 
					+ "  tab1.NUOVOVALORE, " 
					+ "  tab1.NOTE, " 
					+ "  tab1.TIPOLOGIACONTATTO, " 
					+ "   tab1.MOTIVORIFIUTO, " 
					+ "   tab1.STATO, " 
					+ "   tab1.IDAOO " 
					+ " FROM NOTIFICACONTATTO tab1 " 
					+ " RIGHT JOIN GRUPPOCONTATTO tab3 " 
					+ " ON (tab3.IDCONTATTORED = tab1.IDCONTATTO OR tab3.IDCONTATTOMEF  = tab1.IDCONTATTO OR tab3.IDCONTATTOIPA  = tab1.IDCONTATTO) " 
					+ " LEFT JOIN GRUPPO tab4 " 
					+ " ON (tab4.IDGRUPPO = tab3.IDGRUPPO) " 
					+ " WHERE UPPER(tab1.TIPONOTIFICA) LIKE UPPER(?) " 
					+ " AND tab4.ISCANCELLATO= 0 " 
					+ " AND tab4.IDNODO= ? " 
					+ " AND tab1.DATAOPERAZIONE >= sysdate - ? )" 
					+ " ORDER BY DATAOPERAZIONE asc");

			ps.setString(index++, NotificaContatto.TIPO_NOTIFICA_EVASA);
			ps.setLong(index++, idNodo);
			ps.setInt(index++, numGiorni);
			ps.setString(index++, NotificaContatto.TIPO_NOTIFICA_EVASA);
			ps.setLong(index++, idNodo);
			ps.setInt(index++, numGiorni);
			rs = ps.executeQuery();
			final List<NotificaContatto> listaNotifiche = fetchMultiResult(rs, false);
			if(inNumRow!=null) {
				int numRow = inNumRow;	
				numRow = numRow > listaNotifiche.size() ? listaNotifiche.size() : numRow;
				listaNotifiche.subList(0, numRow);
			}
			return listaNotifiche;
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_LISTA_NOTIFICA_MSG + e.getMessage(), e);
			throw new RedException("Errore durante il recupero della lista notifiche.", e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaContattoDAO#updateStatoNotifica(int,
	 *      java.lang.String, java.lang.String, java.sql.Connection).
	 */
	@Override
	public int updateStatoNotifica(final int idNotifica, final String statoNotifica, final String motivoRifiuto, final Connection con) {
		PreparedStatement ps = null;
		final ResultSet rs = null;
		int result;
		try {
			int index = 1;
			ps = con.prepareStatement("update NOTIFICACONTATTO set STATO = ?, MOTIVORIFIUTO = ? where IDNOTIFICACONTATTO = ?");
			ps.setString(index++, statoNotifica);
			ps.setString(index++, motivoRifiuto);
			ps.setInt(index++, idNotifica);
			result = ps.executeUpdate(); 
		} catch (final SQLException e) {
			LOGGER.error(ERROR_INSERIMENTO_NOTIFICA_CONTATTO_MSG + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaContattoDAO#updateStatoNotificaRubrica(java.lang.Integer,
	 *      java.lang.Long, java.lang.Long, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public int updateStatoNotificaRubrica(final Integer statoNotifica, final Long idUtente, final Long idAoo, final Integer idNotificaRubrica, final Connection conn) {
		PreparedStatement ps = null;
		final ResultSet rs = null;
		int result;
		try {
			int index = 1;
			ps = conn.prepareStatement("UPDATE STATO_NOTIFICA_RUBRICA SET STATO_NOTIFICA = ? WHERE IDUTENTE = ? AND IDAOO = ? AND IDNOTIFICARUBRICA = ?");
			ps.setInt(index++, statoNotifica);
			ps.setLong(index++, idUtente);
			ps.setLong(index++, idAoo);
			ps.setInt(index++, idNotificaRubrica);
			result = ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error(ERROR_INSERIMENTO_NOTIFICA_CONTATTO_MSG + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
		return result;
	}
	
	/**
	 * @see it.ibm.red.business.dao.INotificaContattoDAO#getListaNotifiche(java.lang.Long,
	 *      int, int, java.sql.Connection, java.lang.String).
	 */
	@Override
	public List<NotificaContatto> getListaNotifiche(final Long idNodo, final int numGiorni, final int inNumRow, final Connection con, final String utente)  {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int numRow = inNumRow;
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT tab1.IDNOTIFICACONTATTO,tab1.IDCONTATTO,tab1.IDNODOMODIFICA,tab1.IDUTENTEMODIFICA,tab1.UTENTE,tab1.DATAOPERAZIONE,tab1.TIPONOTIFICA,tab1.OPERAZIONEEFFETTUATA,tab1.ALIAS,tab1.NOME,tab1.COGNOME,tab1.MAIL,tab1.MAILPEC,tab1.VECCHIOVALORE,tab1.NUOVOVALORE,tab1.NOTE,tab1.TIPOLOGIACONTATTO,tab1.MOTIVORIFIUTO,tab1.STATO,tab1.IDAOO,SNR.STATO_NOTIFICA ");
			sb.append(FROM_NOTIFICACONTATTO_TAB1_LEFT_OUTER_JOIN_STATO_NOTIFICA_RUBRICA_SNR_ON_TAB1_IDNOTIFICACONTATTO_SNR_IDNOTIFICARUBRICA);
		    sb.append("WHERE tab1.DATAOPERAZIONE >= sysdate - ? AND tab1.UTENTE = ? AND SNR.STATO_NOTIFICA != 2 ORDER BY DATAOPERAZIONE desc");
			
			ps = con.prepareStatement(sb.toString());

			ps.setInt(index++, numGiorni);
			ps.setString(index++, utente);  
			
			rs = ps.executeQuery();
			final List<NotificaContatto> listaNotifiche = fetchMultiResult(rs, true);
			
			numRow = numRow > listaNotifiche.size() ? listaNotifiche.size() : numRow;  
			return listaNotifiche.subList(0, numRow);
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_LISTA_NOTIFICA_MSG + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}
	
	/**
	 * @see it.ibm.red.business.dao.INotificaContattoDAO#getListaNotificheHomePage(java.lang.Long, int, int, int, java.sql.Connection, java.lang.String, java.lang.Long).
	 */
	@Override
	public List<NotificaContatto> getListaNotificheHomePage(final Long idNodo, final int numGiorni, final int maxNotificheNonLette, final int inNumRow, final Connection con, final String utente, final Long idUtente)  {
		PreparedStatement ps = null;
		ResultSet rs = null; 
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM (SELECT tab1.IDNOTIFICACONTATTO,tab1.IDCONTATTO,tab1.IDNODOMODIFICA,tab1.IDUTENTEMODIFICA,tab1.UTENTE,tab1.DATAOPERAZIONE,tab1.TIPONOTIFICA,tab1.OPERAZIONEEFFETTUATA,tab1.ALIAS,tab1.NOME,tab1.COGNOME,tab1.MAIL,tab1.MAILPEC,tab1.VECCHIOVALORE,tab1.NUOVOVALORE,tab1.NOTE,tab1.TIPOLOGIACONTATTO,tab1.MOTIVORIFIUTO,tab1.STATO,tab1.IDAOO,SNR.STATO_NOTIFICA "); 
			sb.append(FROM_NOTIFICACONTATTO_TAB1_LEFT_OUTER_JOIN_STATO_NOTIFICA_RUBRICA_SNR_ON_TAB1_IDNOTIFICACONTATTO_SNR_IDNOTIFICARUBRICA);
			sb.append(AND_SNR_IDUTENTE);
			sb.append("WHERE tab1.UTENTE = ? AND (SNR.STATO_NOTIFICA IS NULL OR SNR.STATO_NOTIFICA = 0) ");
			if (maxNotificheNonLette > 0) {
				sb.append(" and tab1.DATAOPERAZIONE >= sysdate - ? ");
			}
			sb.append("UNION ALL "); 
			sb.append("SELECT tab1.IDNOTIFICACONTATTO,tab1.IDCONTATTO,tab1.IDNODOMODIFICA,tab1.IDUTENTEMODIFICA,tab1.UTENTE,tab1.DATAOPERAZIONE,tab1.TIPONOTIFICA,tab1.OPERAZIONEEFFETTUATA,tab1.ALIAS,tab1.NOME,tab1.COGNOME,tab1.MAIL,tab1.MAILPEC,tab1.VECCHIOVALORE,tab1.NUOVOVALORE,tab1.NOTE,tab1.TIPOLOGIACONTATTO,tab1.MOTIVORIFIUTO,tab1.STATO,tab1.IDAOO,SNR.STATO_NOTIFICA "); 
			sb.append(FROM_NOTIFICACONTATTO_TAB1_LEFT_OUTER_JOIN_STATO_NOTIFICA_RUBRICA_SNR_ON_TAB1_IDNOTIFICACONTATTO_SNR_IDNOTIFICARUBRICA); 
			sb.append(AND_SNR_IDUTENTE);
			sb.append("WHERE tab1.DATAOPERAZIONE >= sysdate - ? AND SNR.STATO_NOTIFICA = 1 and tab1.utente = ? ");
			if (inNumRow > 0) {
				sb.append("and rownum < ?");
			}
			
			sb.append(")RISULTATI ");
			sb.append("ORDER BY RISULTATI.DATAOPERAZIONE desc");
			
			ps = con.prepareStatement(sb.toString());
			
			ps.setLong(index++, idUtente);
			ps.setString(index++, utente);
			if (maxNotificheNonLette > 0) {
				ps.setInt(index++, maxNotificheNonLette); 
			}
			
			ps.setLong(index++, idUtente);
			ps.setInt(index++, numGiorni); 
			ps.setString(index++, utente); 
			if (inNumRow > 0) {
				ps.setInt(index++, inNumRow);
			}
			
			rs = ps.executeQuery();
			return fetchMultiResult(rs, true);
			   
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_LISTA_NOTIFICA_MSG + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaContattoDAO#getListaNotificheByStato(java.lang.Long,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public List<NotificaContatto> getListaNotificheByStato(final Long idContatto, final String statoNotifica, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int index = 1;
			ps = con.prepareStatement("select * from NOTIFICACONTATTO where IDCONTATTO = ? and STATO = ?");

			ps.setLong(index++, idContatto);
			ps.setString(index++, statoNotifica);

			rs = ps.executeQuery();
			return fetchMultiResult(rs, false);
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_LISTA_NOTIFICA_MSG + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaContattoDAO#removeNotifica(int,
	 *      java.sql.Connection).
	 */
	@Override
	public int removeNotifica(final int idNotifica, final Connection con) {
		PreparedStatement ps = null;
		final ResultSet rs = null;
		int result;
		try {
			int index = 1;
			ps = con.prepareStatement("delete FROM NOTIFICACONTATTO where IDNOTIFICACONTATTO = ?");
			ps.setInt(index++, idNotifica);
			result = ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'eliminazione della notificaContatto al DB. " + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
		return result;
	}
	
	/**
	 * @see it.ibm.red.business.dao.INotificaContattoDAO#getCountListaNotificheRichiesteHome(int,
	 *      int, java.lang.Long, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public Integer getCountListaNotificheRichiesteHome(final int maxNotificheNonLette, final int inNumRow, 
			final Long idAoo, final Long idUtente, final Connection con) {
		Integer contatoreOutput = 0;
		PreparedStatement ps = null;
		ResultSet rs = null; 		
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder("SELECT count(*) as counter ");
			sb.append(getFromClauseNotificaContatto(maxNotificheNonLette));

			ps = con.prepareStatement(sb.toString());
			ps.setLong(index++, idUtente);
			ps.setString(index++, NotificaContatto.TIPO_NOTIFICA_RICHIESTA);
			ps.setString(index++, StatoNotificaContattoEnum.IN_ATTESA.getNome());
			ps.setLong(index++, idAoo);
			if (maxNotificheNonLette > 0) {
				ps.setInt(index++, maxNotificheNonLette); 
			}

			rs = ps.executeQuery();
			if (rs.next()) {
				contatoreOutput = rs.getInt("counter");
			}
			 
			return contatoreOutput;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la count della lista notifica contatti: " + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Costruisce la clausula <em> From </em> per il recupero di informazioni sulla
	 * notifica contatto.
	 * 
	 * @param maxNotificheNonLette
	 * @return Query parziale - clausula From.
	 */
	private static StringBuilder getFromClauseNotificaContatto(final int maxNotificheNonLette) {
		StringBuilder sb = new StringBuilder("");
		sb.append("FROM NOTIFICACONTATTO NOTCONT LEFT OUTER JOIN STATO_NOTIFICA_RUBRICA SNR ON "); 
		sb.append("NOTCONT.IDNOTIFICACONTATTO = SNR.IDNOTIFICARUBRICA "); 
		sb.append(AND_SNR_IDUTENTE);
		sb.append("WHERE UPPER(NOTCONT.tiponotifica) = UPPER(?) ");
		sb.append("AND NOTCONT.STATO like ? ");  
		sb.append("AND NOTCONT.IDAOO = ? AND (SNR.stato_notifica is null OR SNR.stato_notifica = 0) ");
		if (maxNotificheNonLette > 0) {
			sb.append("AND NOTCONT.DATAOPERAZIONE >= sysdate - ? ");
		}
		
		return sb;
	}
	
	/**
	 * @see it.ibm.red.business.dao.INotificaContattoDAO#getCountListaNotificheHomePage(int,
	 *      int, java.sql.Connection, java.lang.String, java.lang.Long).
	 */
	@Override
	public Integer getCountListaNotificheHomePage(final int maxNotificheNonLette, 
			final int inNumRow, final Connection con, final String utente, final Long idUtente)  {
		Integer contatoreOutput = 0;
		PreparedStatement ps = null;
		ResultSet rs = null; 
		try {
			
			int index = 1;
			final StringBuilder sb = new StringBuilder("SELECT count(*) as counter ");
			sb.append(FROM_NOTIFICACONTATTO_TAB1_LEFT_OUTER_JOIN_STATO_NOTIFICA_RUBRICA_SNR_ON_TAB1_IDNOTIFICACONTATTO_SNR_IDNOTIFICARUBRICA);
			sb.append(AND_SNR_IDUTENTE);
			sb.append("WHERE tab1.UTENTE = ? AND (SNR.STATO_NOTIFICA IS NULL OR SNR.STATO_NOTIFICA = 0) ");
			
			if (maxNotificheNonLette > 0) {
				sb.append(" and tab1.DATAOPERAZIONE >= sysdate - ? ");
			} 
			ps = con.prepareStatement(sb.toString());
			
			ps.setLong(index++, idUtente);
			ps.setString(index++, utente);
			if (maxNotificheNonLette > 0) {
				ps.setInt(index++, maxNotificheNonLette); 
			}
			 
			rs = ps.executeQuery();
			if (rs.next()) {
				contatoreOutput = rs.getInt("counter");
			}
			  
			return contatoreOutput;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero della count notifica contatti: " + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}
}
