package it.ibm.red.business.enums;

/**
 * Enum PostaEnum.
 *
 * @author a.dilegge
 * 
 *         Enum tipo di posta utilizzata dall'applicazione.
 */
public enum PostaEnum {
	
	/**
	 * Posta gestita interamente da Red.
	 */
	POSTA_INTERNA(0),
	
	/**
	 * Posta gestita dal sistema di protocollo. 
	 * La posta interoperabile valida NON ribalta automaticamente le informazioni della segnatura sul documento in ingresso.
	 */
	POSTA_ESTERNA_INTEROPERABILE_NON_AUTOMATICA(1),
	
	/**
	 * Posta gestita dal sistema di protocollo. 
	 * La posta interoperabile valida ribalta automaticamente le informazioni della segnatura sul documento in ingresso.
	 */
	POSTA_ESTERNA_INTEROPERABILE_AUTOMATICA(2);
		
	/**
	 * Valore dell'enum.
	 */
	private Integer value;
	

	/**
	 * Costruttore.
	 * 
	 * @param inValue	valore
	 */
	PostaEnum(final Integer inValue) {
		this.value = inValue;
	}

	/** 
	 * @return	valore
	 */
	public Integer getValue() {
		return value;
	}
	
	
	/** 
	 * @param value the value
	 * @return the by value
	 */
	public static PostaEnum getByValue(final int value) {
		for (final PostaEnum p: PostaEnum.values()) {
			if (p.getValue().intValue() == value) {
				return p;
			}
		}
		
		return null;
	}

	
	/**
	 * Controlla se il messaggio di posta in arrivo è interoperabile
	 *
	 * @param value the value
	 * @return true, if is interoperabile
	 */
	public static boolean isInteroperabile(final int value) {
		boolean isInteroperabile = false;
		
		if (value == POSTA_ESTERNA_INTEROPERABILE_AUTOMATICA.getValue().intValue() 
				|| value == POSTA_ESTERNA_INTEROPERABILE_NON_AUTOMATICA.getValue().intValue()) {
			isInteroperabile = true;
		}
		
		return isInteroperabile;
	}
}
