package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.ValutaMetadatiDTO;

/**
 * Interfaccia DAO valuta.
 * @author DarioVentimiglia
 */
public interface IValutaDAO extends Serializable {

	/**
	 * Recupera i metadati valuta dalla lista dei metadati completi.
	 * @param metadatiValutaDoc
	 * @param connection
	 * @return metadati tipo valuta
	 */
	List<ValutaMetadatiDTO> raggruppaValutaMetadati(List<MetadatoDTO> metadatiValutaDoc, Connection connection);

	/**
	 * Restituisce i metadati per la conversione.
	 * @param dataCreazione
	 * @param connection
	 * @return mappa valuta - valore
	 */
	Map<String, Double> getMetadatiConversione(Date dataCreazione, Connection connection);

	/**
	 * Restituisce i metadati per la conversione.
	 * @param codiceValuta
	 * @param dataCreazione
	 * @param connection
	 * @return mappa valuta - valore
	 */
	Map<String, Double> getMetadatiConversione(String codiceValuta, Date dataCreazione, Connection connection);
}
