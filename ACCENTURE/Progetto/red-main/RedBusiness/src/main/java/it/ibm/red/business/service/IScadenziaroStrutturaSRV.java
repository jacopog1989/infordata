package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IScadenziaroStrutturaFacadeSRV;

/**
 * Interfaccia del servizio di gestione scadenziario.
 */
public interface IScadenziaroStrutturaSRV extends IScadenziaroStrutturaFacadeSRV {

}
