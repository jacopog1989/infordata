package it.ibm.red.business.enums;

/**
 * Enum che definisce la tipologia dei protocolli.
 */
public enum TipoProtocolloEnum {
	
	/**
	 * Valore.
	 */
	NESSUNO(0, "", ""),
	
	/**
	 * Valore.
	 */
	ENTRATA(1, "E", "ENTRATA"),
	
	/**
	 * Valore.
	 */
	USCITA(2, "U", "USCITA");
	
	/**
	 * Identificativo.
	 */
	private Integer id;
	/**
	 * Codice.
	 */
	private String codice;
	/**
	 * Codice NPS.
	 */
	private String codiceEstesoNPS;
	
	/**
	 * Costruttore.
	 * @param inId
	 * @param inCodice
	 * @param inCodiceEstesoNPS
	 */
	TipoProtocolloEnum(final Integer inId, final String inCodice, final String inCodiceEstesoNPS) {
		this.id = inId;
		this.codice = inCodice;
		this.codiceEstesoNPS = inCodiceEstesoNPS;
	}
	
	/**
	 * Restituisce l'enum associata all'id.
	 * @param id
	 * @return enum associata all'id
	 */
	public static TipoProtocolloEnum getById(final Integer id) {
		TipoProtocolloEnum output = NESSUNO;
		
		if (id != null) {
			for (TipoProtocolloEnum t : TipoProtocolloEnum.values()) {
				if (t.getId().equals(id)) {
					output = t;
					break;
				}
			}
		}
		
		return output;
	}
	
	/**
	 * Restituisce l'enum associata al nome.
	 * @param nome
	 * @return enum associata al nome
	 */
	public static TipoProtocolloEnum getByName(final String nome) {
		TipoProtocolloEnum output = NESSUNO;
		
		if (nome != null && !nome.trim().isEmpty()) {
			for (TipoProtocolloEnum t : TipoProtocolloEnum.values()) {
				if (t.toString().equalsIgnoreCase(nome)) {
					output = t;
					break;
				}
			}
		}
		
		return output;
	}

	/**
	 * Restituisce l'id dell'enum.
	 * @return id
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Restituisce il codice dell'enum.
	 * @return codice
	 */
	public String getCodice() {
		return codice;
	}
	
	/**
	 * Restituisce il codice esteso NPS dell'enum
	 * @return codice esteso NPS
	 */
	public String getCodiceEstesoNPS() {
		return codiceEstesoNPS;
	}

}