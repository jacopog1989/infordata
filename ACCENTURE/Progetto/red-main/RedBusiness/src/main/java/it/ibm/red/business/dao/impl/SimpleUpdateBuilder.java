package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.ibm.red.business.utils.StringUtils;

/**
 * Classe builder per la costruzione di query di update.
 */
public class SimpleUpdateBuilder extends SimpleQueryBuilder {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = -2109673651574801558L;

	/**
	 * Costruttore.
	 * @param inTable
	 */
	public SimpleUpdateBuilder(final String inTable) {
		super(inTable);
	}

	/**
	 * Consente l'esecuzione di un update sulla tabella di riferimento.
	 * @param con
	 * @param whereKey
	 * @param whereValue
	 * @return PreparedStatement dopo l'esecuzione dell'update
	 * @throws SQLException
	 */
	public PreparedStatement update(final Connection con, final String whereKey, final Object whereValue) throws SQLException {
		PreparedStatement psUpdate = null;
		StringBuilder exps = new StringBuilder("");

		//Gestiamo parametri
		final List<String> paramKeys = new ArrayList<>(getParams().keySet());

		if (!paramKeys.isEmpty()) {
			for (final String paramKey:paramKeys) {
				exps.append(paramKey).append("=?,");
			}
			exps = new StringBuilder(StringUtils.cutLast(exps.toString()));
		}

		final String sqlUpdate = "UPDATE " + getTable() + " SET " + exps + " WHERE " + whereKey + "=?";
		
		psUpdate = con.prepareStatement(sqlUpdate);
		Integer index = 1;
		for (final String paramKey:paramKeys) {
			handleParams(psUpdate, index, getParams().get(paramKey));
			index++;
		}
		handleParams(psUpdate, index, whereValue);
		psUpdate.executeUpdate();

		return psUpdate;
	}

	/**
	 * Gestisce l'aggiunta della condizione all'update.
	 * Non gestisce null value.
	 * @param key
	 * @param value
	 */
	public void addParams(final String key, final Object value) {
		addParams(false, key, value);
	}

}
