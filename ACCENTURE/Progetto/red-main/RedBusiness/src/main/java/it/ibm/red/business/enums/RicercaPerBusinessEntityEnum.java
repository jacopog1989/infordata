/**
 * 
 */
package it.ibm.red.business.enums;

/**
 * Enum di Entità di Business utilizzate dalla ricerca generica
 * 
 * @author APerquoti
 *
 */
public enum RicercaPerBusinessEntityEnum {

	/**
	 * Entita Documento.
	 */
	DOCUMENTI(1, "Documenti"),
	
	/**
	 * Entita Fascicolo.
	 */
	FASCICOLI(2, "Fascicoli"),
	
	/**
	 * Entita Faldoni.
	 */
	FALDONI(3, "Faldoni"),
	
	/**
	 * Entita Documento e Fascicolo.
	 */
	DOCUMENTI_FASCICOLI(4, "Entrambi");
	
	
	
	/**
	 * Chiave.
	 */
	private int key;

	/**
	 * Label.
	 */
	private String label;

	/**
	 * Costruttore.
	 * 
	 * @param inKey		chiave
	 * @param inLabel	label
	 */
	RicercaPerBusinessEntityEnum(final int inKey, final String inLabel) {
		this.key = inKey;
		this.label = inLabel;
	}


	/**
	 * Getter chiave.
	 * 
	 * @return chiave
	 */
	public int getKey() {
		return key;
	}

	/**
	 * Getter label.
	 * 
	 * @return label
	 */
	public String getLabel() {
		return label;
	}
	
	/**
	 * Restituisce l'enum associata alla chiave passata in ingresso.
	 * @param ricercaPerBusinessEntityEnumObj
	 * @return enum associata alla key
	 */
	public static RicercaPerBusinessEntityEnum get(final String ricercaPerBusinessEntityEnumObj) {
		RicercaPerBusinessEntityEnum output = null;
		for (RicercaPerBusinessEntityEnum e:RicercaPerBusinessEntityEnum.values()) {
			if (String.valueOf(e.getKey()).equals(ricercaPerBusinessEntityEnumObj)) {
				 output = e;
			}
		}
		return output;
	}
}
