package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio spediti.
 */
public interface ISpeditoFacadeSRV extends Serializable {
	
	/**
	 * Metodo di spedizione.
	 * @param utente
	 * @param dataSped
	 * @param wobNumber
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO spedito(UtenteDTO utente, Date dataSped, String wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param dataSped
	 * @param wobNumbers
	 * @return
	 * 
	 * @author APerquoti
	 */
	Collection<EsitoOperazioneDTO> spedito(UtenteDTO utente, Date dataSped, Collection<String> wobNumbers);
}