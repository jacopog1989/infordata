package it.ibm.red.business.service.concrete;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List; 

import javax.annotation.PostConstruct;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.Document;
import com.filenet.api.property.Properties;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dao.IGestioneNotificheEmailDAO;
import it.ibm.red.business.dao.INotificaDAO;
import it.ibm.red.business.dao.ISottoscrizioniDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.CategoriaEventoDTO; 
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.MailAddressDTO;
import it.ibm.red.business.dto.NotificaDTO;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.PropertiesNameEnum; 
import it.ibm.red.business.exception.EmailException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.email.NotificheEmailHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet; 
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.INotificaSRV;
import it.ibm.red.business.utils.CollectionUtils;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Service di gestione notifiche.
 */
@Service
@Component
public class NotificaSRV extends AbstractService implements INotificaSRV {
	
	/**
	 * La costante serial Verison UID.
	 */
	private static final long serialVersionUID = 8447337068295228612L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotificaSRV.class.getName());

	/**
	 * Messaggio errore recupero notifiche utente.
	 */
	private static final String ERROR_RECUPERO_NOTIFICHE_MSG = "Errore in fase di recupero delle notifiche utente";

	/**
	 * Messaggio errore aggiornamento notifica.
	 */
	private static final String ERROR_AGGIORNAMENTO_NOTIFICHE_MSG = "Eccezione in fase di aggiornamento della notifica ";

	/**
	 * DAO sottoscrizioni eventi.
	 */
	@Autowired
	private ISottoscrizioniDAO sottoscrizioniDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private INotificaDAO notificaDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * DAO gestione notifiche mail.
	 */
	@Autowired
	private IGestioneNotificheEmailDAO gestNotificheEmailDAO;
	
	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public void postCostruct() {
		// Non deve eseguire alcuna azione nel post construct.
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaFacadeSRV#inviaNextNotifica(int).
	 */
	@Override
	public void inviaNextNotifica(final int idAoo) {

		Connection con = null; 
		Connection connApp = null;

		NotificaDTO notifica = null;
		

		try {


			con = setupConnection(getFilenetDataSource().getConnection(), true); 
			connApp = setupConnection(getDataSource().getConnection(), true); 
			
			// recupera notifica
			notifica = notificaDAO.getNext(idAoo, con);

			if (notifica != null) {
				
				gestisciInvioNotifica(idAoo, con, connApp, notifica);
			}

			commitConnection(con); 
			commitConnection(connApp);

		} catch (final Exception e) {

			LOGGER.error("Errore in fase gestione delle notifiche dell'aoo " + idAoo, e);
			rollbackConnection(con);
			rollbackConnection(connApp);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (final SQLException e) {
					LOGGER.error("Errore in fase di chiusura della connessione", e);
				}
			}
			if (connApp != null) {
				try {
					connApp.close();
				} catch (final SQLException e) {
					LOGGER.error("Errore in fase di chiusura della connessione applicativa", e);
				}
			}
		}

	}

	/**
	 * Gestisce l'invio della sottoscrizione e gli eventuali errori riscontrati.
	 * 
	 * @param idAoo
	 * @param con
	 * @param connApp
	 * @param notifica
	 */
	private void gestisciInvioNotifica(final int idAoo, final Connection con, final Connection connApp, final NotificaDTO notifica) {

		try {
			inviaNotificaSottoscrizione(notifica, idAoo, con, connApp);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di invio della notifica " + notifica.getIdNotifica() + ". Ripristino in corso con errore... ", e);
			aggiornaNotificaConErrore(notifica.clonaNotifica(), e, con);
		}
	}

	/**
	 * Aggiorna la notifica impostandone l'errore.
	 * 
	 * @param notifica
	 *            Notifica da aggiornare.
	 * @param ex
	 *            Eccezione da memorizzare nel log che descrive l'errore
	 *            riscontrato.
	 * @param con
	 *            Connessione al database.
	 */
	private void aggiornaNotificaConErrore(final NotificaDTO notifica, final Exception ex, final Connection con) {

		String descErrore = null;
		if (ex instanceof EmailException) {
			descErrore = "Errore di invio mail";
		} else if (ex instanceof SQLException) {
			descErrore = "Errore di sullo strato di persistenza";
		} else {
			descErrore = "Errore generico";
		}
		
		notifica.setDescErrore(descErrore);
		notifica.setStatoNotifica(NotificaDTO.ERRORE);
		
		notificaDAO.updateNotifica(notifica, con);
	}
	
	/**
	 * Invia la notifica di sottoscrizione.
	 * 
	 * @param notifica
	 *            Notifica da inviare.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param con
	 *            Connessione al database.
	 * @param connApp
	 *            Connessione al database App.
	 */
	private void inviaNotificaSottoscrizione(final NotificaDTO notifica, final int idAoo, final Connection con, final Connection connApp) {
		IFilenetCEHelper fceh = null;
		
		try {
			// recupero il template per invio mail
			final CategoriaEventoDTO template = sottoscrizioniDAO.getTemplate(con, notifica.getIdEvento());

			// recupera il documento
			final Aoo aoo = aooSRV.recuperaAoo(idAoo);
			final AooFilenet aooFilenet = aoo.getAooFilenet();
			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(),
					aooFilenet.getUri(), aooFilenet.getStanzaJaas(), aooFilenet.getObjectStore(),(long)idAoo);

			final Document doc = fceh.getDocumentByDTandAOO(notifica.getIdDocumento(), (long) idAoo);

			// costruisci ufficio e utente mittente
			final MittenteAzione mittente = getEmptyMittenteAzione();
			if (isEventoPostSiglaOVisto(notifica.getIdEvento())) {
				setUtenteMittente(connApp, doc, mittente);
			}else if (isEventoPostRifiutaFirma(notifica.getIdEvento())) {
				setUtenteMittenteRifiutoFromDEventiCustom(connApp,con,doc,mittente);
			} else {
				sottoscrizioniDAO.populateMittente(con, notifica.getIdEvento(),
						Integer.parseInt(notifica.getIdDocumento()), 
						Integer.parseInt(notifica.getIdNodo()),
						Integer.parseInt(notifica.getIdUtente()),
						mittente);
			}
			
			// valorizza le informazioni a partire dal template
			template.replace(CategoriaEventoDTO.REPLACE_IDDOCUMENTO,
					(TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY)) + "");
			template.replace(CategoriaEventoDTO.REPLACE_DESCEVENTO, template.getDescrizioneEvento());
			template.replace(CategoriaEventoDTO.REPLACE_OGGETTODOCUMENTO,
					(String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.OGGETTO_METAKEY));
			
			if (!StringUtils.isNullOrEmpty(mittente.getUtente())) {
				template.replace(CategoriaEventoDTO.REPLACE_UTENTEMITTENTE, mittente.getUtente());
			}else {
				template.replace(CategoriaEventoDTO.REPLACE_UTENTEMITTENTE, " ");
			}
			if (!StringUtils.isNullOrEmpty(mittente.getUfficio())) {
				template.replace(CategoriaEventoDTO.REPLACE_UFFICIOMITTENTE, "(" + mittente.getUfficio() + ")");
			} else {
				template.replace(CategoriaEventoDTO.REPLACE_UFFICIOMITTENTE, " ");
			}
			
			if (StringUtils.isNullOrEmpty(mittente.getUfficio()) && StringUtils.isNullOrEmpty(mittente.getUtente())) {
				template.replace("L'operazione è stata effettuata da", " ");
			}
			
			//informazioni flusso
			String informazioniFlusso = " ";
			final String codiceFlusso = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY);
			final String identificatoreProcesso = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.IDENTIFICATORE_PROCESSO_METAKEY);
			if (!StringUtils.isNullOrEmpty(codiceFlusso)) {
				informazioniFlusso += "[flusso = " + codiceFlusso + "]";
			}
			if (!StringUtils.isNullOrEmpty(identificatoreProcesso)) {
				informazioniFlusso += "[processo = " + identificatoreProcesso + "]";
			}
						
			template.replace(CategoriaEventoDTO.REPLACE_INFORMAZIONIFLUSSO, informazioniFlusso);
			
			// se è un evento relativo alla coda del libro firma
			if (isAssegnazioneInLibroFirma(notifica.getIdEvento(), template)) {

				final Utente utente = utenteDAO.getUtente(Long.parseLong(notifica.getIdUtente()), connApp);
				final FilenetPEHelper fpeh = new FilenetPEHelper(utente.getUsername(), aooFilenet.getPassword(),
						aooFilenet.getUri(), aooFilenet.getStanzaJaas(), aooFilenet.getConnectionPoint());

				// aggiungo l'informazione sulla data scadenza
				replaceDataScadenza(template, notifica, fpeh);
				
				// aggiungo l'url del libro firma di EVO
				replaceUrlLibroFirmaEvo(template, notifica, utente);
				
				// aggiungo l'url del libro firma mobile
				if (utenteDAO.hasLibroFirmaMobile((long) notifica.getIdRuolo(), aoo, connApp)) {
					replaceUrlLibroFima(template, notifica, utente);
				} else {
					template.replace(CategoriaEventoDTO.REPLACE_URLLIBROFIRMA, " ");
					template.replace(CategoriaEventoDTO.REPLACE_URLLFMOB, " ");
				}
				
			} else {
				template.replace(CategoriaEventoDTO.REPLACE_DATASCADENZA, " ");
				template.replace(CategoriaEventoDTO.REPLACE_URLLIBROFIRMAEVO, " ");
				template.replace(CategoriaEventoDTO.REPLACE_URLLIBROFIRMA, " ");
				template.replace(CategoriaEventoDTO.REPLACE_URLLFEVO, " ");
				template.replace(CategoriaEventoDTO.REPLACE_URLLFMOB, " ");
				
			}

			if(EventTypeEnum.NOTIFICA_MANCATA_SPEDIZIONE.getIntValue().equals(notifica.getIdEvento())) { 
				List<String> destinatariMancataSpedizione = gestNotificheEmailDAO.getDestinatariNotificheByStatoRicevuta((long)idAoo, String.valueOf(notifica.getIdDocumento()), connApp);
				if(!CollectionUtils.isEmptyOrNull(destinatariMancataSpedizione)) {
					StringBuilder destPlaceholer = new StringBuilder();
					for(String emailDest : destinatariMancataSpedizione) {
						destPlaceholer.append(emailDest +";");
					}
					template.replace(CategoriaEventoDTO.REPLACE_DESTINATARINOTIFICA, destPlaceholer.toString());
				} 
			}			
			
			// recupero gli indirizzi mail
			final List<String> mails = getMailsUtente(con, Integer.parseInt(notifica.getIdUtente()));
			
		 
			// invio la mail
			final NotificheEmailHelper neh = new NotificheEmailHelper();
			String resultInvio = null;
			if (template.getIdCategoria()==1) {
				 resultInvio = neh.inviaNotifica(notifica, mails, template.getOggettoMail(),null, template.getCorpoMail());
			}else {
				 resultInvio = neh.inviaNotifica(notifica, mails, template.getOggettoMail(), template.getCorpoMail(), null);
			}

			// aggiorna notificaSottoscrizione con esito dell'invio
			notificaDAO.updateNotifica(notifica.getIdNotifica(), resultInvio, con);
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * Imposta l'utente mittente.
	 * 
	 * @param connApp
	 *            Connessione al database.
	 * @param doc
	 *            Documento da cui recuperare l'utente mittente.
	 * @param mittente
	 *            Mittente azione da impostare.
	 */
	private void setUtenteMittente(final Connection connApp, final Document doc, final MittenteAzione mittente) {
		final String userNameUltimaModifica = doc.get_LastModifier();
		final Utente utente = utenteDAO.getByUsername(userNameUltimaModifica, connApp);
		mittente.setUtente(utente.getCognome() + " " + utente.getNome());
	}
	
	private void setUtenteMittenteRifiutoFromDEventiCustom(final Connection connApp,final Connection connFileNet, final Document doc, final MittenteAzione mittente) {
		final String userNameUltimaModifica = sottoscrizioniDAO.getUsernameEventoRifiutoFirma(connFileNet, (String)TrasformerCE.getMetadato(doc, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
		final Utente utente = utenteDAO.getByUsername(userNameUltimaModifica, connApp);
		mittente.setUtente(utente.getCognome() + " " + utente.getNome());
	}
	
	/**
	 * Restituisce le mail dell'utente.
	 * 
	 * @param connection
	 *            Connessione al database.
	 * @param idUtente
	 *            Identificativo dell'utente dal quale recuperare le e-mail.
	 * @return Lista delle E-mail recuperate.
	 * @throws SQLException
	 */
	private List<String> getMailsUtente(final Connection connection, final int idUtente) throws SQLException {
		final List<String> mails = new ArrayList<>();
		final List<MailAddressDTO> mailsDTO = sottoscrizioniDAO.getMailsSottoscrizioni(connection, idUtente);
		for (final MailAddressDTO mailDTO: mailsDTO) {
			mails.add(mailDTO.getAddress());
		}
		return mails;
	}

	private static boolean isEventoPostSiglaOVisto(final int idEvento) {
		final List<Integer> idEventi = Arrays
				.asList(Integer.parseInt(EventTypeEnum.MODIFICATO_DOPO_VISTO.getValue()),
						Integer.parseInt(EventTypeEnum.MODIFICATO_DOPO_SIGLA.getValue()));

		return idEventi.contains(idEvento);
	}
 
	private static boolean isEventoPostRifiutaFirma(final int idEvento) {
		final List<Integer> idEventi = Arrays
				.asList(Integer.parseInt(EventTypeEnum.RIFIUTO_FIRMA.getValue()));

		return idEventi.contains(idEvento);
	} 
	
	/**
	 * controlla se l'evento è relativo alla coda librofirma.
	 * 
	 * @param idEvento
	 * @param template
	 * @return
	 */
	private static boolean isAssegnazioneInLibroFirma(final int idEvento, final CategoriaEventoDTO template) {

		final List<Integer> eventiLibroFirma = Arrays
				.asList(Integer.parseInt(EventTypeEnum.ASSEGNAZIONE_FIRMA.getValue()),
						Integer.parseInt(EventTypeEnum.ASSEGNAZIONE_SIGLA.getValue()),
						Integer.parseInt(EventTypeEnum.ASSEGNAZIONE_VISTO.getValue()),
						Integer.parseInt(EventTypeEnum.RICHIESTA_VISTO.getValue()),
						Integer.parseInt(EventTypeEnum.ASSEGNAZIONE_PER_FIRMA_COPIA_CONFORME.getValue()));

		return eventiLibroFirma.contains(idEvento) && 1 == template.getIdCategoria();

	}

	/**
	 * Mittente azione.
	 */
	public class MittenteAzione {
		/**
		 * Utente.
		 */
		private String utente;


		/**
		 * Ufficio.
		 */
		private String ufficio;

		/**
		 * Costruttore.
		 * @param utente
		 * @param ufficio
		 */
		public MittenteAzione(final String utente, final String ufficio) {
			super();
			this.utente = utente;
			this.ufficio = ufficio;
		}

		/**
		 * Restituisce l'utente.
		 * @return utente
		 */
		public String getUtente() {
			return utente;
		}

		/**
		 * Imposta l'utente.
		 * @param utente
		 */
		public void setUtente(final String utente) {
			this.utente = utente;
		}

		/**
		 * Restituisce l'ufficio.
		 * @return ufficio
		 */
		public String getUfficio() {
			return ufficio;
		}

		/**
		 * Imposta l'ufficio.
		 * @param ufficio
		 */
		public void setUfficio(final String ufficio) {
			this.ufficio = ufficio;
		}
	}

	private MittenteAzione getEmptyMittenteAzione() {
		return new MittenteAzione("", "");
	}

	/**
	 * Aggiungo la data scadenza sull'informazione della notifica.
	 * 
	 * @param template
	 * @param notificaSottoscrizione
	 */
	private void replaceDataScadenza(final CategoriaEventoDTO template, final NotificaDTO notifica, final FilenetPEHelper fpeh) {
		String dataScadenzaDescr = " ";

		try {
			final VWWorkObject workflow = fpeh.getWorkflowPrincipale(notifica.getIdDocumento(), null);
			if (workflow != null) {
				final Date dataScadenza = (Date) TrasformerPE.getMetadato(workflow,
						PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY));
				if (dataScadenza != null) {
					final Date nullDate = Date.from(DateUtils.NULL_DATE.atZone(ZoneId.systemDefault()).toInstant());
					if (!dataScadenza.equals(nullDate)) {
						dataScadenzaDescr = " Il documento scadrà in data " + DateUtils.dateToString(dataScadenza, true)
								+ ". ";
					}
				}
			}

		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero della data scadenza", e);
		}

		template.replace(CategoriaEventoDTO.REPLACE_DATASCADENZA, dataScadenzaDescr);
	}

	/**
	 * aggiungo l'url del libro firma sulla notifica.
	 * 
	 * @param template
	 * @param notifica
	 * @param utente
	 */
	private static void replaceUrlLibroFima(final CategoriaEventoDTO template, final NotificaDTO notifica, final Utente utente) {
		String urlLibroFirma = " ";
		StringBuilder sb = new StringBuilder(" ");
		StringBuilder sb3 = new StringBuilder("");

		try {

			final int idRuolo = notifica.getIdRuolo();
			final String username = utente.getUsername();
			final String host = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.SOTTOSCRIZIONI_URL_LIBROFIRMA);
			final String contextPath = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.SOTTOSCRIZIONI_CONTEXTPATH_MOBILEAPP);
			final String idNodo = notifica.getIdNodo();
			final String documentTitle = notifica.getIdDocumento();
			urlLibroFirma = "http://" + host + "/" + contextPath + "/views/libro.jsf?mailLink="
					+ getEncryptPath(documentTitle, idRuolo, idNodo, username);
			//urlLibroFirma = urlLibroFirma
				//	+ " se non è possibile aprire il collegamento fare copia incolla del link sul browser"
			String href="<a href=\""+urlLibroFirma+"\">qui</a>";
			
			sb.append("Per accedere al sistema e visionare il documento da RED Mobile &egrave; possibile cliccare ");
			sb.append(href);
			
			sb3.append("Nel caso non sia possibile utilizzare il collegamento a RED Mobile potr&agrave; copiare e incollare il seguente link sul browser: ").append(urlLibroFirma);

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di creazione della URL per l'accesso diretto al libro firma ", e);
		}

		template.replace(CategoriaEventoDTO.REPLACE_URLLIBROFIRMA, sb.toString());
		template.replace(CategoriaEventoDTO.REPLACE_URLLFMOB, sb3.toString());
	}
	
	/**
	 * aggiungo l'url del libro firma di EVO sulla notifica. Per il test di questo
	 * metodo occorre fare qualcosa di questo tipo. <code> 
	 * public static void main(String[] args) {
	 *		CategoriaEventoDTO template = new CategoriaEventoDTO();
	 *		template.setCorpoMail(CategoriaEventoDTO.REPLACE_URLLIBROFIRMAEVO);
	 *		template.setOggettoMail("oggetto");
	 *		NotificaDTO notifica = new NotificaDTO();
	 *		notifica.setIdNodo("2701"); // 28 default
	 *		notifica.setIdRuolo(1006); // 101 default
	 *		notifica.setIdDocumento("391262");
	 *		Utente utente = new Utente(null, null, null, "irene.pellegrini", null, null, null, null, null, null);
	 *		replaceUrlLibroFirmaEvo(template, notifica, utente);
	 *		LOGGER.error("URL LibroFirma ---> " + template.getCorpoMail());
	 * } 
	 * </code>
	 * 
	 * @param template
	 * @param notifica
	 * @param utente
	 */
	private static void replaceUrlLibroFirmaEvo(final CategoriaEventoDTO template, final NotificaDTO notifica, final Utente utente) {
		StringBuilder sb = new StringBuilder(" ");
		StringBuilder sb2 = new StringBuilder("");
		StringBuilder sb3 = new StringBuilder("");
		
		try {
			
			// Utente
			int idRuolo = notifica.getIdRuolo();
			String username = utente.getUsername();
			String idNodo = notifica.getIdNodo();

			// Documento
			String documentTitle = notifica.getIdDocumento();
			
			// Applicativo
			String host = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.SOTTOSCRIZIONI_URL_LIBROFIRMA_EVO);
			String contextPath = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.SOTTOSCRIZIONI_CONTEXTPATH_EVOAPP);
			
			sb.append("Per accedere al sistema e visionare il documento da RED &egrave; possibile cliccare ");
	
			sb2.append("https://").append(host).append("/").append(contextPath).append("/views/coda/coda.jsf?mailLink=");
			sb2.append(getEncryptPath(documentTitle, idRuolo, idNodo, username));
			String href="<a href=\""+sb2.toString()+"\">qui</a>";
			sb.append(href);
			
			sb3.append("Nel caso non sia possibile utilizzare il collegamento a RED potr&agrave; copiare e incollare il seguente link sul browser: ").append(sb2.toString());
		} catch (Exception e) {
			LOGGER.error("Errore in fase di creazione della URL per l'accesso diretto al libro firma ", e);
			sb = new StringBuilder(" ");
		}
		
		template.replace(CategoriaEventoDTO.REPLACE_URLLIBROFIRMAEVO, sb.toString());
		template.replace(CategoriaEventoDTO.REPLACE_URLLFEVO, sb3.toString());
	}
	
	/**
	 * esegue l'encrypt della stringa e l'encode e la restituisce.
	 * 
	 * @param documentTitle
	 * @param idRuolo
	 * @param idNodo
	 * @param username
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private static String getEncryptPath(final String documentTitle, final int idRuolo, final String idNodo, final String username)
			throws UnsupportedEncodingException {
		final String toEncrypt = "documentTitle=" + documentTitle + "&idRuolo=" + idRuolo + "&idNodo=" + idNodo + "&username="
				+ username;
		final boolean doLog = !"true".equals(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DISABLE_LOG_DOWNLOAD_CONTENT_SERVLET));
		return URLEncoder.encode(it.ibm.red.business.crypt.DesCrypterNew.encrypt(doLog, toEncrypt), "UTF-8");
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.INotificaFacadeSRV#recuperaNotifiche(java.lang.Long,
	 *      java.lang.Long, it.ibm.red.business.dto.FilenetCredentialsDTO,
	 *      java.lang.Integer).
	 */
	@Override
	public List<NotificaDTO> recuperaNotifiche(final Long idUtente, final Long idAoo, final FilenetCredentialsDTO fcUtente, final Integer aPartireDaNgiorni) {
		List<NotificaDTO> list = null;
		Connection dwhCon = null;
		
		try {
			dwhCon = setupConnection(getFilenetDataSource().getConnection(), false);
			
			list = recuperaNotifiche(idUtente, idAoo, fcUtente, aPartireDaNgiorni, dwhCon);
		} catch (final SQLException e) {
			LOGGER.error("Si è verificato un errore di connessione verso il database durante il recupero delle notifiche.", e);
		} finally {
			closeConnection(dwhCon);
		}
		
		return list;
	}
	
	/**
	 * @see it.ibm.red.business.service.INotificaSRV#recuperaNotifiche(java.lang.Long,
	 *      java.lang.Long, it.ibm.red.business.dto.FilenetCredentialsDTO,
	 *      java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public List<NotificaDTO> recuperaNotifiche(final Long idUtente, final Long idAoo, final FilenetCredentialsDTO fcUtente, final Integer inAPartireDaNgiorni, final Connection dwhCon){
		return recuperaNotifiche(idUtente,idAoo,fcUtente, inAPartireDaNgiorni,null, false,0,dwhCon);
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.INotificaFacadeSRV#recuperaNotifiche(java.lang.Long,
	 *      java.lang.Long, it.ibm.red.business.dto.FilenetCredentialsDTO,
	 *      java.lang.Integer, java.lang.Integer, boolean, int,
	 *      java.sql.Connection).
	 */
	@Override
	public List<NotificaDTO> recuperaNotifiche(final Long idUtente, final Long idAoo, final FilenetCredentialsDTO fcUtente, 
			final Integer inAPartireDaNgiorni,final Integer maxRowNum,final boolean fromeHomepage,final int maxNotificheNonLette, final Connection dwhCon) {
		
		List<NotificaDTO> list = null;
		IFilenetCEHelper fceh = null; 
		final List<String> listQueryIn = new ArrayList<>();
		
		try {
			 
			if (fromeHomepage) {
				list = notificaDAO.getNotificheHomepage(idAoo, idUtente, inAPartireDaNgiorni, maxRowNum,maxNotificheNonLette,dwhCon);
			} else {
				Integer aPartireDaNgiorni = inAPartireDaNgiorni;
				// imposta eventuale default ai giorni di latenza della ricerca
				aPartireDaNgiorni = (aPartireDaNgiorni != null) ? aPartireDaNgiorni : 7300;
				list = notificaDAO.getNotifiche(idAoo, idUtente, aPartireDaNgiorni, dwhCon);
			}
			
			for (final NotificaDTO notifica : list) {
				listQueryIn.add(notifica.getIdDocumento());
			}
			
			if (CollectionUtils.isEmptyOrNull(list)) {
				return list;
			}
			
			fceh = FilenetCEHelperProxy.newInstance(fcUtente,idAoo);
			final String whereCondition = buildWhereCondition(listQueryIn);

			Iterator<NotificaDTO> i = list.iterator();
			final DocumentSet docs = fceh.getDocuments(idAoo.intValue(), whereCondition, 
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), true, false);
			
			if (docs != null) {
				final Iterator<?> it = docs.iterator();
				while (it.hasNext()) {
					final Document doc = (Document) it.next();
					final Properties prop = doc.getProperties();
					
					i = list.iterator();
					while (i.hasNext()) {
						final NotificaDTO notificaDTO = i.next();
						
						if (notificaDTO.getIdDocumento().equals(prop.getStringValue(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)))) {
							populateNotificaDTO(prop, notificaDTO);
						}
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore in fase di recupero delle notifiche.", e);
		} finally {
			popSubject(fceh);
		}
		
		return list;
	}

	/**
	 * @param listQueryIn
	 * @return where condition
	 */
	private static String buildWhereCondition(final List<String> listQueryIn) {
		final StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(StringUtils.createInCondition(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), listQueryIn.iterator(), true));
		queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
		queryBuilder.append(")");
		
		return queryBuilder.toString();
	}

	/**
	 * @param prop
	 * @param notificaDTO
	 */
	private void populateNotificaDTO(final Properties prop, final NotificaDTO notificaDTO) {
		PropertiesProvider pp = PropertiesProvider.getIstance();
		try {
			notificaDTO.setDataScadenzaDocumento(prop.getDateTimeValue(pp.getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY)));
			notificaDTO.setNumeroDocumento(prop.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY)).toString());
			notificaDTO.setAnnoDocumento(prop.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY)));
			notificaDTO.setNumeroProtocollo(prop.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY)));
			notificaDTO.setAnnoProtocollo(prop.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY)));
			notificaDTO.setOggettoDocumento(prop.getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY)));
			notificaDTO.setIdCategoriaDocumento(prop.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY)));
			notificaDTO.setClasseDocumentale(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));
			notificaDTO.setSottoCategoriaDocUscita(prop.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.SOTTOCATEGORIA_DOCUMENTO_USCITA)));
			notificaDTO.setTipologiaDocumentoId(prop.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY)));
			notificaDTO.setUfficioCreatoreId(prop.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY)));
			if (prop.isPropertyPresent(pp.getParameterByKey(PropertiesNameEnum.ITER_APPROVATIVO_SEMAFORO_METAKEY))) {
				final String iterApp = prop.getStringValue(pp.getParameterByKey(PropertiesNameEnum.ITER_APPROVATIVO_SEMAFORO_METAKEY));
				notificaDTO.setTipoFirma(-1);
				if (iterApp!=null) {
					final Integer iterAppInt = Integer.parseInt(iterApp);
					notificaDTO.setTipoFirma(iterAppInt);
				}
			}
			 
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero da FileNet dei dati della notifica afferente al document title: " + notificaDTO.getIdDocumento(), e);
		}
	}
	
	/**
	 * Gestisce l'eliminazione della notifica.
	 * 
	 * @param notificheSelezionate
	 * @return true se la notifica viene eliminata, false altrimenti.
	 */
	@Override
	public boolean eliminaNotifiche(final Collection<NotificaDTO> notificheSelezionate) {
		Connection con = null;
		
		try {
			con = setupConnection(getFilenetDataSource().getConnection(), true);
			boolean isOk = true;
			for (final NotificaDTO notifica: notificheSelezionate) {
				isOk = notificaDAO.eliminaNotifica(notifica.getIdNotifica(), con);
				if (!isOk) {
					throw new RedException(ERROR_AGGIORNAMENTO_NOTIFICHE_MSG + notifica.getIdNotifica());
				}
			}
			
			commitConnection(con); 
			return true;
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_NOTIFICHE_MSG, e);
			rollbackConnection(con);
		} finally {
			closeConnection(con);
		}
		return false;
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaFacadeSRV#contrassegnaNotifiche(it.ibm.red.business.dto.NotificaDTO).
	 */
	@Override
	public boolean contrassegnaNotifiche(final NotificaDTO notificaSelezionata) {
		Connection con = null;
		
		try {
			con = setupConnection(getFilenetDataSource().getConnection(), true);
			boolean isOk = true;
			isOk = notificaDAO.contrassegnaNotifica(notificaSelezionata.getIdNotifica(), con);
			if (!isOk) {
					throw new RedException(ERROR_AGGIORNAMENTO_NOTIFICHE_MSG + notificaSelezionata.getIdNotifica());
			}
			
			commitConnection(con); 
			return true;
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_NOTIFICHE_MSG, e);
			rollbackConnection(con);
		} finally {
			closeConnection(con);
		}
		return false;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.INotificaFacadeSRV#eliminaNotifiche(it.ibm.red.business.dto.NotificaDTO).
	 */
	@Override
	public boolean eliminaNotifiche(final NotificaDTO notificaSelezionata) {
		Connection con = null;
		
		try {
			con = setupConnection(getFilenetDataSource().getConnection(), true);
			boolean isOk = true;
			isOk = notificaDAO.eliminaNotifica(notificaSelezionata.getIdNotifica(), con);
				if (!isOk) {
					throw new RedException(ERROR_AGGIORNAMENTO_NOTIFICHE_MSG + notificaSelezionata.getIdNotifica());
				}
			commitConnection(con); 
			return true;
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_NOTIFICHE_MSG, e);
			rollbackConnection(con);
		} finally {
			closeConnection(con);
		}
		return false;
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaFacadeSRV#contrassegnaNotifiche(java.util.Collection).
	 */
	@Override
	public boolean contrassegnaNotifiche(final Collection<NotificaDTO> notificheSelezionate) {
		Connection con = null;
		
		try {
			con = setupConnection(getFilenetDataSource().getConnection(), true);
			boolean isOk = true;
			for (final NotificaDTO notifica: notificheSelezionate) {
				isOk = notificaDAO.contrassegnaNotifica(notifica.getIdNotifica(), con);
				if (!isOk) {
					throw new RedException(ERROR_AGGIORNAMENTO_NOTIFICHE_MSG + notifica.getIdNotifica());
				}
			}
			
			commitConnection(con); 
			return true;
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_NOTIFICHE_MSG, e);
			rollbackConnection(con);
		} finally {
			closeConnection(con);
		}
		return false;
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaFacadeSRV#recuperaNotifiche(java.lang.Integer,
	 *      java.lang.Long, java.lang.Long,
	 *      it.ibm.red.business.dto.FilenetCredentialsDTO).
	 */
	@Override
	public List<NotificaDTO> recuperaNotifiche(final Integer idNotifica, final Long idUtente, final Long idAoo, final FilenetCredentialsDTO fcUtente) {
		List<NotificaDTO> list = null;
		IFilenetCEHelper fceh = null;
		Connection dwhCon = null;
		PropertiesProvider pp = PropertiesProvider.getIstance();
		
		try {  
			dwhCon = setupConnection(getFilenetDataSource().getConnection(), false);
			list = notificaDAO.getNotifiche(idNotifica, idUtente, idAoo,dwhCon);
			if (CollectionUtils.isEmptyOrNull(list)) {
				return list;
			}
			
			fceh = FilenetCEHelperProxy.newInstance(fcUtente,idAoo);
			
			final StringBuilder whereCondition = new StringBuilder();
			whereCondition.append("d.").append(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)).append(" IN (");
			Iterator<NotificaDTO> i = list.iterator();
			while (i.hasNext()) {
				whereCondition.append("'").append(i.next().getIdDocumento()).append("', ");
			}
			
			whereCondition.delete(whereCondition.length() - 2, whereCondition.length());
			whereCondition.append(")");
			
			final DocumentSet docs = fceh.getDocuments(idAoo.intValue(), whereCondition.toString(), 
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), true, false);
			
			if (docs != null) {
				final Iterator<?> it = docs.iterator();
				while (it.hasNext()) {
					final Document doc = (Document) it.next();
					final Properties prop = doc.getProperties();
					
					i = list.iterator();
					while (i.hasNext()) {
						final NotificaDTO notificaDTO = i.next();
						if (notificaDTO.getIdDocumento().equals(prop.getStringValue(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)))) {
							
							notificaDTO.setDataScadenzaDocumento(prop.getDateTimeValue(pp.getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY)));
							notificaDTO.setNumeroDocumento(prop.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY)).toString());
							notificaDTO.setAnnoDocumento(prop.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY)));
							notificaDTO.setNumeroProtocollo(prop.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY)));
							notificaDTO.setAnnoProtocollo(prop.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY)));
							notificaDTO.setOggettoDocumento(prop.getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY)));
							notificaDTO.setIdCategoriaDocumento(prop.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY)));
							notificaDTO.setClasseDocumentale(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));
						}
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore in fase di recupero delle notifiche.", e);
		} finally {
			popSubject(fceh);
			closeConnection(dwhCon);
		}
		
		return list;
	}
	
	/**
	 * @see it.ibm.red.business.service.INotificaSRV#countNotificheSottoscrizione(java.lang.Long,
	 *      java.lang.Long, java.lang.Integer, int).
	 */
	@Override
	public Integer countNotificheSottoscrizione(final Long idUtente, final Long idAoo,final Integer maxRowNum,final int maxNotificheNonLette) {
		Integer contatoreOutput = 0; 
		Connection dwhCon = null;
		try {
			dwhCon = setupConnection(getFilenetDataSource().getConnection(), false);
			contatoreOutput = notificaDAO.getCountNotificheHomepage(idAoo, idUtente,maxRowNum,maxNotificheNonLette,dwhCon);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore in fase di recupero della count .", e);
		} finally {
			closeConnection(dwhCon);
		}
		return contatoreOutput;
	}
	
}