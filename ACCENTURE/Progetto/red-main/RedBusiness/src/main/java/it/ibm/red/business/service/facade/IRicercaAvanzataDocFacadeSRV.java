package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocSalvataDTO;
import it.ibm.red.business.dto.ParamsRicercaEsitiDTO;
import it.ibm.red.business.dto.RegistroDTO;
import it.ibm.red.business.dto.RicercaAvanzataDocFormDTO;
import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.persistence.model.RicercaAvanzataSalvata;

/**
 * The Interface IRicercaAvanzataDocFacadeSRV.
 *
 * @author m.crescentini
 * 
 *         Facade del servizio per la gestione della ricerca avanzata dei
 *         documenti.
 */
public interface IRicercaAvanzataDocFacadeSRV extends Serializable {

	/**
	 * Esegue la ricerca avanzata dei documenti in base ai parametri definiti in
	 * <code> paramsRicercaAvanzata </code>.
	 * 
	 * @param paramsRicercaAvanzata
	 *            parametri della ricerca
	 * @param utente
	 *            utente in sessione
	 * @param con
	 *            connession al database
	 * @param orderbyInQuery
	 *            flag di ordinamento risultati
	 * @param setCap
	 *            flag associato all'impostazione del capitolo spesa
	 * @param ignoraAnnoCreazioneDocumento
	 *            flag che permette di ignorare l'anno creazione documento nei
	 *            risultati
	 * @return collection dei master dei documenti recuperati
	 */
	Collection<MasterDocumentRedDTO> eseguiRicercaDocumenti(ParamsRicercaAvanzataDocDTO paramsRicercaAvanzata, UtenteDTO utente, Connection con, boolean orderbyInQuery,
			boolean setCap, boolean ignoraAnnoCreazioneDocumento);

	/**
	 * Esegue la ricerca avanzata dei documenti in base ai parametri definiti in
	 * <code> paramsRicercaAvanzata </code>.
	 * 
	 * @param paramsRicercaAvanzata
	 *            parametri della ricerca
	 * @param utente
	 *            utente in sessione
	 * @param orderbyInQuery
	 *            flag di ordinamento risultati
	 * @param ignoraAnnoCreazioneDocumento
	 *            flag che permette di ignorare l'anno creazione documento nei
	 *            risultati
	 * @return collection dei master dei documenti recuperati
	 */
	Collection<MasterDocumentRedDTO> eseguiRicercaDocumenti(ParamsRicercaAvanzataDocDTO paramsRicercaAvanzata, UtenteDTO utente, boolean orderbyInQuery,
			boolean ignoraAnnoCreazioneDocumento);

	/**
	 * Inizializza tutte le combo.
	 * 
	 * @param utente
	 * @param ricercaAvanzataForm
	 * @param onlyUlterioriFiltri
	 */
	void initAllCombo(UtenteDTO utente, RicercaAvanzataDocFormDTO ricercaAvanzataForm, boolean onlyUlterioriFiltri);

	/**
	 * Ottiene i tipi procedimento dal tipo documento.
	 * 
	 * @param descrTipologiaDocumento
	 * @param idAOO
	 * @return lista dei tipi procedimento
	 */
	List<TipoProcedimentoDTO> getTipiProcedimentoByTipologiaDocumento(String descrTipologiaDocumento, Long idAOO);

	/**
	 * Ottiene il tipo procedimento dal tipo documento e dalla descrizione del tipo
	 * procedimento.
	 * 
	 * @param descrTipologiaProcedimento
	 * @param idAOO
	 * @param idTipologiaDocumento
	 * @return tipo procedimento
	 */
	TipoProcedimentoDTO getTipiProcedimentoByIdTipologiaDocumentoDescrProc(String descrTipologiaProcedimento, Long idAOO, Integer idTipologiaDocumento);

	/**
	 * Restituisce il permesso associato al tipo operazione identificato dall'id:
	 * <code> idTipoOperazione </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IRicercaAvanzataDocFacadeSRV#
	 *      getPermessoByTipoOperazione(java.lang.Integer).
	 * @param idTipoOperazione
	 *            identificativo tipo operazione
	 * @return permesso associato al tipo operazione, @see PermessiEnum
	 */
	PermessiEnum getPermessoByTipoOperazione(Integer idTipoOperazione);

	/**
	 * Salva la ricerca avanzata.
	 * 
	 * @param nomeRicercaAvanzata
	 * @param paramsRicercaAvanzataSalvata
	 * @param utente
	 */
	void salvaRicercaAvanzata(String nomeRicercaAvanzata, ParamsRicercaAvanzataDocSalvataDTO paramsRicercaAvanzataSalvata, UtenteDTO utente);

	/**
	 * Ottiene tutte le ricerche avanzate salvate per utente.
	 * 
	 * @param idUtente
	 * @param idUfficio
	 * @param idRuolo
	 * @return lista delle ricerche avanzate salvate
	 */
	List<RicercaAvanzataSalvata> getAllRicercheSalvateByUtente(Long idUtente, Long idUfficio, Long idRuolo);

	/**
	 * Ottiene i parametri della ricerca avanzata salvata.
	 * 
	 * @param idRicerca
	 * @return parametri della ricerca avanzata salvata
	 */
	ParamsRicercaAvanzataDocSalvataDTO getParamsRicercaAvanzataSalvata(Integer idRicerca);

	/**
	 * Elimina la ricerca avanzata salvata.
	 * 
	 * @param idRicerca
	 */
	void eliminaRicercaAvanzataSalvata(Integer idRicerca);

	/**
	 * Ottiene la lista dei tipi documento per aoo.
	 * 
	 * @param idAoo
	 * @return lista dei tipi documento
	 */
	List<TipologiaDocumentoDTO> getListaTipologieDocumento(Long idAoo);

	/**
	 * Ottiene i registri ausiliari tramite i tipi documento.
	 * 
	 * @param idsTipologieDocumento
	 * @return registri
	 */
	Collection<RegistroDTO> getRegistriAusiliariByTipologieDocumento(Collection<Integer> idsTipologieDocumento);

	/**
	 * Ricerca gli esiti/documenti uscita.
	 * 
	 * @param paramsRicerca
	 * @param fceh
	 * @return document titles
	 */
	Collection<String> ricercaEsitiUscita(ParamsRicercaEsitiDTO paramsRicerca, IFilenetCEHelper fceh);

}