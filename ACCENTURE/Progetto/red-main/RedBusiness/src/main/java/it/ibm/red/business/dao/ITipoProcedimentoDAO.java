package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.persistence.model.TipoProcedimento;

/**
 * Interfaccia dao tipo procedimento.
 * @author m.crescentini
 * @author CPIERASC
 *
 */
public interface ITipoProcedimentoDAO extends Serializable {
	
	/**
	 * Recupero di tutti i tipi procedimento.
	 * 
	 * @param connection	connessione al db
	 * @return				collezione di tipi procedimento
	 */
	Collection<TipoProcedimento> getAll(Connection connection);
	
	/**
	 * Recupero Tipo Procedimento da un ID
	 * 
	 * @param connection
	 * @return
	 */
	TipoProcedimento getTPbyId(Connection connection, Integer idProcedimento);
	
	/**
	 * Restituisce tutte le info relative al tipo procedimento (inclusi i dati del wf)
	 * @param connection
	 * @param idProcedimento
	 * @return
	 */
	TipoProcedimento getTPCompletobyId(Connection connection, Integer idProcedimento);
	
	/**
	 * @param connection
	 * @param idTipologiaDocumento
	 * @return
	 */
	List<TipoProcedimento> getTipiProcedimentoByTipologiaDocumento(Connection connection, Integer idTipologiaDocumento);
	
	/**
	 * @param idTipoProcedimento
	 * @param connection
	 * @return
	 */
	String getWorkflowNameByIdTipoProcedimento(long idTipoProcedimento, Connection connection);

	/**
	 * @param descrizioneTipoProcedimento
	 * @param connection
	 * @return
	 */
	List<Integer> getIdsByDescrizione(String descrizioneTipoProcedimento, Connection connection);

	/**
	 * @param connection
	 * @return
	 */
	Map<Long, String> getAllDesc(Connection connection);
	
	/**
	 * @param idUffIstruttore
	 * @param idTipologiaDoc
	 * @param conn
	 * @return
	 */
	boolean checkStampigliaturaSiglaVisible(Long idUffIstruttore, Integer idTipologiaDoc, Connection conn);
	
	/**
	 * Esegue il salvataggio del procedimento effettuandone la insert sul database.
	 * 
	 * @see it.ibm.red.business.dao.ITipoProcedimentoDAO#save(it.ibm.red.business.dto.TipoProcedimentoDTO,
	 *      java.sql.Connection).
	 * @param procedimento
	 *            procedimento da salvare sulla base dati
	 * @param connection
	 *            connessione al database
	 * @return id del procedimento generato dalla sequence:
	 *         <code> SEQ_TIPOLOGIAPROCEDIMENTO </code>
	 */
	Long save(TipoProcedimentoDTO procedimento, Connection connection);

	/**
	 * Restituisce l'id della tipologia procedimento associata alla descrizione:
	 * <code> descrizione </code> e associata alla tipologia documento identificata
	 * da <code> idTipologiaDocumento </code>.
	 * 
	 * @see it.ibm.red.business.dao.ITipoProcedimentoDAO#getTipoProcedimentoByDescrizioneAndTipologiaDocumento(java.lang.String,
	 *      java.lang.Integer, java.sql.Connection).
	 * @param descrizione
	 *            descrizione tipo procedimento
	 * @param idTipologiaDocumento
	 *            identificativo tipologia documento
	 * @param con
	 *            connessione al database
	 * @return identificativo tipo procedimento
	 */
	Long getIdTipoProcedimentoByDescrizioneAndTipologiaDocumento(String descrizione, Integer idTipologiaDocumento, Connection con);

	/**
	 * 
	 * Metodo che consente il recupero della tipologia procedimento da selezionare dato un tipo documento e un registro ausiliario
	 * NB: Questo metodo è valido solo per registri ausiliari che fanno riferimento ad OSSERVAZIONI in quanto solo in quel caso esiste una relazione 
	 * sul nome tra il tipo procedimento in uscita e il registro ausiliario. 
	 * 
	 * @param idTipologiaDocumento		id della tipologia del documento per cui occorre conoscere il procedimento
	 * @param idRegistroAusiliario		id del registro ausiliario per la quale occorre conoscere il procedimento associato, fa riferimento ad un
	 * 									registro di tipo OSSERVAZIONE
	 * @param con
	 * @return 							Long se esiste il tipo procedimento, null altrimenti
	 */
	Long getIdTipoProcedimentoByTipologiaDocumentoAndRegistroAusiliario(int idTipologiaDocumento, int idRegistroAusiliario, Connection con);

	/**
	 * Salva la relazione tra tipologia documento e tipologia procedimento.
	 * @param connection
	 * @param idProcedimento
	 * @param idDocumento
	 * @param procedimento
	 */
	void saveCrossProcedimentoDocumento(Connection connection, Long idProcedimento, int idDocumento, TipoProcedimentoDTO procedimento);
	
	/**
	 * Salva la relazione procedimento.
	 * @param idProcedimento
	 * @param idAoo
	 * @param connection
	 */
	void saveCrossProcedimentoAOO(Long idProcedimento, Long idAoo, Connection connection);
}
