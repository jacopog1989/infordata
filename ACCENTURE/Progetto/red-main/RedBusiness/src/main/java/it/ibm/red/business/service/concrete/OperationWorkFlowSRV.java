package it.ibm.red.business.service.concrete;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Document;
import com.google.common.collect.ImmutableMap;
import com.google.common.net.MediaType;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.PEProperty;
import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.ICodaTrasformazioneDAO;
import it.ibm.red.business.dao.IContributoDAO;
import it.ibm.red.business.dao.IGestioneNotificheEmailDAO;
import it.ibm.red.business.dao.IIterApprovativoDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.DetailAssegnazioneDTO;
import it.ibm.red.business.dto.DocumentAppletContentDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.WobNumberWsDTO;
import it.ibm.red.business.dto.WorkFlowWsDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.LibroFirmaPerFirmaResponseEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.StatoCodaTrasformazioneEnum;
import it.ibm.red.business.enums.StatoContributoEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.NodoUtenteRuolo;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.provider.ServiceTaskProvider.ServiceTask;
import it.ibm.red.business.service.IOperationDocumentSRV;
import it.ibm.red.business.service.IOperationWorkFlowSRV;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 *
 * Service di gestione dei workflow.
 *
 * @author CRISTIANOPierascenzi
 */
@Service
public class OperationWorkFlowSRV extends AbstractService implements IOperationWorkFlowSRV {

	/**
	 * Messaggio errore avanzamento workflow. Occorre appendere il wobNumber al
	 * messaggio.
	 */
	private static final String ERROR_AVANZAMENTO_WORKFLOW_MSG = "Si è verificato un errore durante l'avanzamento del workflow: ";

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -4949939893523725915L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(OperationWorkFlowSRV.class.getName());
	
	/**
	 * Service per il recupero/aggiornamento delle security FN.
	 */
	@Autowired
	private ISecuritySRV securitySRV;

	/**
	 * Service.
	 */
	@Autowired
	private IOperationDocumentSRV operationDocumentSRV;

	/**
	 * DAO coda.
	 */
	@Autowired
	private ICodaTrasformazioneDAO codaDAO;

	/**
	 * DAO notifiche.
	 */
	@Autowired
	private IGestioneNotificheEmailDAO gestioneNotificheEmailDAO;

	/**
	 * DAO contributi.
	 */
	@Autowired
	private IContributoDAO contributoDAO;

	/**
	 * DAO uffici (nodi).
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * DAO utenti.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IIterApprovativoDAO iterApprovativoDAO;

	/**
	 * Dao gestione aoo.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * Service.
	 */
	@Autowired
	private ISignFacadeSRV signSRV;

	/**
	 * Messaggi response ok.
	 */
	private static final Map<String, String> MESSAGGI_CUSTOM_RESPONSE_ESITO_OK = ImmutableMap.<String, String>builder()
			.put(ResponsesRedEnum.PROCEDI.getResponse(), "Il documento è stato inviato nel libro firma del Direttore/Ispettore.")
			.put(ResponsesRedEnum.STORNA_AL_CORRIERE.getResponse(), "Il documento è stato stornato al proprio corriere.")
			.put(ResponsesRedEnum.INVIA_ISPETTORE.getResponse(), "Il documento è stato inviato in FIRMA all'Ispettore.")
			.put(ResponsesRedEnum.PRESA_VISIONE.getResponse(), "Presa visione del documento effettuata con successo.")
			.put(ResponsesRedEnum.METTI_IN_SOSPESO.getResponse(), "Il documento è stato spostato nella coda In Sospeso.")
			.put(ResponsesRedEnum.INVIA_FIRMA_DIGITALE.getResponse(), "Il documento è stato inviato in FIRMA al Ragioniere Generale.").build();

	/**
	 * @see it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV#genericNextStep(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String, java.lang.String,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      it.ibm.red.business.helper.filenet.pe.FilenetPEHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public EsitoOperazioneDTO genericNextStep(final String wobNumber, final UtenteDTO utente, final String response, final String motivazioneAssegnazione,
			final IFilenetCEHelper fceh, final FilenetPEHelper fpeh, final Connection connection) {

		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		Integer numDoc = null;
		try {
			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);

			final PropertiesProvider pp = PropertiesProvider.getIstance();
			// Controllo non presente su NSD. una motivazione plausibile può essere che in
			// fase di test questo metadato non era sempre valorizzato
			final Integer nIdDocumento = (Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
			if (nIdDocumento == null) {
				throw new RedException("ID documento non trovato per il documento da avanzare.");
			}
			final String idDocumento = nIdDocumento.toString();
			final String queueName = wob.getCurrentQueueName();
			numDoc = fceh.getNumDocByDocumentTitle(idDocumento);

			if (Boolean.TRUE.equals(esitoSemaforo(idDocumento, queueName, utente.getIdAoo().intValue(), utente, fceh, connection))) {
				final HashMap<String, Object> metadati = new HashMap<>();
				// preparazione metadati da aggiornare durante l'avanzamento del wf
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId().intValue());
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), utente.getIdUfficio().intValue());
				// forzo la cancellazione di una eventuale motivazione per l'assegnazione
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivazioneAssegnazione);

				fpeh.nextStep(wob, metadati, response);
				generaSecurity(wobNumber, utente, fceh, fpeh, connection, idDocumento);

				esito.setEsito(true);
				esito.setIdDocumento(numDoc);
				if (ResponsesRedEnum.INVIA_FIRMA_DIGITALE.getResponse().equals(response) && ("DF").equals(utente.getCodiceAoo())) {
					esito.setNote(MESSAGGI_CUSTOM_RESPONSE_ESITO_OK.get(response).replace("Ragioniere Generale", "Capo Dipartimento"));
				} else {
					esito.setNote(MESSAGGI_CUSTOM_RESPONSE_ESITO_OK.get(response));
				}
				if (StringUtils.isNullOrEmpty(esito.getNote())) {
					esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
				}
				return esito;
			} else {
				LOGGER.error("La verifica del semaforo ha dato esito negativo per il documento NUMERO DOCUMENTO ----> " + numDoc + " RESPONSE " + response);
				esito.setNote("Errore durante l'avanzamento del workflow: esito semaforo negativo.");
				esito.setIdDocumento(numDoc);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'avanzamento del WF. Response: " + response + " NUmDoc: " + numDoc, e);
			esito.setNote("Errore durante l'avanzamento del WF.");
			esito.setIdDocumento(numDoc);
		}
		return esito;
	}

	/**
	 * @param wobNumber
	 * @param utente
	 * @param fceh
	 * @param fpeh
	 * @param connection
	 * @param idDocumento
	 */
	private void generaSecurity(final String wobNumber, final UtenteDTO utente, final IFilenetCEHelper fceh, final FilenetPEHelper fpeh, final Connection connection,
			final String idDocumento) {
		final boolean isRiservato = isRiservato(fceh, idDocumento);

		try {
			if (isRiservato) {
				securitySRV.generateRiservatoSecurity(utente, idDocumento, wobNumber, fpeh, connection);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante le operazioni post avanzamento del workflow durante l'aggiornamento delle security.", e);
		}
	}

	/**
	 * Metodo per implementare la transizione generica
	 * (HomeGenericNoWindowController).
	 *
	 * @param wobNumber
	 * @param utente
	 * @param response
	 * @return the esito operazione DTO
	 */
	@Override
	public EsitoOperazioneDTO genericNextStep(final String wobNumber, final UtenteDTO utente, final String response) {
		EsitoOperazioneDTO esito = null;
		Connection connection = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			esito = genericNextStep(wobNumber, utente, response, null, fceh, fpeh, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'avanzamento del WF. Response: " + response, e);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}
		return esito;
	}

	/**
	 * @param wobNumbers
	 * @param utente
	 * @param response
	 * @return the collection
	 */
	@Override
	public final Collection<EsitoOperazioneDTO> genericNextStep(final Collection<String> wobNumbers, final UtenteDTO utente, final String response) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		for (final String wobNumber : wobNumbers) {
			esiti.add(genericNextStep(wobNumber, utente, response));
		}
		return esiti;
	}

	/**
	 * Metodo per il calcolo del semaforo.
	 * 
	 * @param idDocumento identificativo documento
	 * @param queueName   nome coda
	 * @param utente      utente
	 * @param fceh
	 * @param connection
	 * @return esito check semaforo
	 */
	private Boolean esitoSemaforo(final String idDocumento, final String queueName, final Integer idAOO, final UtenteDTO utente, final IFilenetCEHelper fceh,
			final Connection connection) {
		final PropertiesProvider pp = PropertiesProvider.getIstance();
		boolean okConversioneECampoFirma = true;
		// Campi utilizzati per la query
		final String[] selectArray = { PropertyNames.ID, // UTILIZZATO PIU AVANTI PER RECUPERARE GLI ALLEGATI CON CONTENT
				pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY), pp.getParameterByKey(PropertiesNameEnum.ITER_APPROVATIVO_METAKEY),
				pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), pp.getParameterByKey(PropertiesNameEnum.CONTENT_ELEMENTS_METAKEY) };
		// manca il metadato riservato perchè viene recuperato piu avanti solo se
		// effettivamente il wf viene avanzato con successo

		final Document doc = fceh.getDocumentByIdGestionale(idDocumento, Arrays.asList(selectArray), null, idAOO, null,
				pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));

		String nomeFile = null;
		if (doc.getProperties() != null && doc.getProperties().get(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY)) != null) {
			nomeFile = doc.getProperties().get(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY)).getStringValue();
		}
		// Se il file è un P7M, si saltano tutti i controlli
		if (nomeFile != null && !nomeFile.toLowerCase().endsWith("p7m")) {
			int numeroDocumento = 0;
			if (doc.getProperties() != null && doc.getProperties().get(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY)) != null) {
				numeroDocumento = doc.getProperties().get(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY)).getInteger32Value();
			}
			String iterSem = "";
			IterApprovativoDTO iterApprovativo = null;
			if (doc.getProperties() != null && doc.getProperties().get(pp.getParameterByKey(PropertiesNameEnum.ITER_APPROVATIVO_METAKEY)) != null) {
				iterSem = doc.getProperties().get(pp.getParameterByKey(PropertiesNameEnum.ITER_APPROVATIVO_METAKEY)).getStringValue();
				if (!StringUtils.isNullOrEmpty(iterSem)) {
					iterApprovativo = iterApprovativoDAO.getIterApprovativoById(Integer.parseInt(iterSem), connection);
				}
			}
			if ((queueName != null && queueName.equalsIgnoreCase(DocumentQueueEnum.CORRIERE.getName())) && iterSem != null && iterApprovativo != null
					&& iterApprovativo.isIterAutomaticoMoreThanDirigente()) {
				int stato = -1;

				// verifico il mime type del doc principale
				if (nomeFile.toLowerCase().endsWith("pdf")) {
					try {
						final Aoo aoo = aooDAO.getAoo(utente.getIdAoo(), connection);
						final boolean isFirmato = signSRV.hasSignaturesForSpedizione(doc.accessContentStream(0), utente, aoo.getSignerTimbro());
						// verifico che nel content del doc principale siano presenti dei campi firma da
						// firmare o che sia già firmato
						if (isFirmato || PdfHelper.checkCampoFirma(doc.accessContentStream(0))) {
							final DocumentSet allegati = fceh.getAllegatiConContent(StringUtils.cleanGuidToString(doc.get_Id()));
							// recupero gli allegati del doc principale e verifico che non ci siano problemi
							// con il mimetype o con i campi firma
							if (allegati != null) {
								final Iterator<?> it = allegati.iterator();
								while (it.hasNext()) {
									final Document docAllegato = (Document) it.next();

									// boolean formato originle
									boolean isFormatoOriginale = false;
									final Boolean formatoOriginaleAllegato = (Boolean) TrasformerCE.getMetadato(docAllegato, PropertiesNameEnum.IS_FORMATO_ORIGINALE_METAKEY);
									if (formatoOriginaleAllegato != null && formatoOriginaleAllegato) {
										isFormatoOriginale = formatoOriginaleAllegato;
									}

									// se il metadato formato originale non è stato gestito allora controlla
									// direttamente il mimeType
									final String mime = (String) TrasformerCE.getMetadato(docAllegato, PropertyNames.MIME_TYPE);
									final Integer formatoAllegato = (Integer) TrasformerCE.getMetadato(docAllegato, PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY);

									// prima condizione controlla la conversione.
									if (!isFormatoOriginale && mime != null) {
										if (MediaType.PDF.toString().equalsIgnoreCase(mime)
												|| FileUtils.isP7MFile(mime)) {
											// verifico che ci sia almeno un campo firma da firmare
											if (hasTagFirmatario(docAllegato, formatoAllegato)) {
												// se l'allegato ha un campo firma da firmare ???
												okConversioneECampoFirma = false;
												final String idAllegato = (String) TrasformerCE.getMetadato(docAllegato, PropertyNames.ID);
												LOGGER.error("Attenzione, Allegato " + idAllegato + " non ha il campo firma, si prega di contattare l'assistenza");
											}
										} else {
											// se uno degli allegati e' ancora formato originale o comunque diverso da
											// pdf o pkcs7 imposto il flag a false e recupero lo stato sulla coda trasformazione
											okConversioneECampoFirma = false;
											stato = getStatoDocumentoFromCodaTrasformazione(idDocumento, idAOO, connection);
										}

										if (!okConversioneECampoFirma) {
											break;
										}
									}
								}
							}
						} else {
							// se e' PDF e NON è da firmare
							okConversioneECampoFirma = false;
							LOGGER.error("Documento principale " + numeroDocumento + " non contiene ancora il campo firma");
						}
					} catch (final Exception e) {
						LOGGER.error("Si è verificato un errore durante la verifica campo firma.", e);
						throw new RedException("Si è verificato un errore durante la verifica campo firma.", e);
					}
				} else {
					// se il doc principale non e' PDF
					okConversioneECampoFirma = false;
					stato = getStatoDocumentoFromCodaTrasformazione(idDocumento, idAOO, connection);
					LOGGER.error("Attenzione, risulta che il documento --> " + numeroDocumento + " con nome --> " + nomeFile
							+ " non sia stato trasformato correttamente. ID STATO CODA TRASFORMAZIONE --> " + stato);
				}
				if (!okConversioneECampoFirma && (stato == 0 || stato == StatoCodaTrasformazioneEnum.ERRORE.getStatus())) {
					// se è stato rilevato un qualsiasi errore nella procedure del semaforo ne tengo
					// traccia attraverso un metadato del documento
					doc.getProperties().putValue(pp.getParameterByKey(PropertiesNameEnum.TRASFORMAZIONE_PDF_ERRORE_METAKEY), Constants.Varie.TRASFORMAZIONE_PDF_IN_ERRORE);
					doc.save(RefreshMode.REFRESH);
				}
			}
		}
		// se la coda attuale del workflow è diversa da CORRIERE o comunque l'iter
		// approvativo NON è uno tra FIRMA ISPETTORE o FIRMA RAGIONIERE va tutto bene.
		return okConversioneECampoFirma;
	}

	/**
	 * controlla se allegato NON ha almeno un campo da firmare.
	 *
	 * @param docAllegato     documento
	 * @param formatoAllegato formato
	 * @return esito verifica
	 */
	private static boolean hasTagFirmatario(final Document docAllegato, final Integer formatoAllegato) {
		return (
//				verifica il metadato 'DaFirmare' 
		(FilenetCEHelper.isDaFirmare(docAllegato))
//				verifica che il doc non sia firmato digitalmente 
				&& (!FormatoAllegatoEnum.FIRMATO_DIGITALMENTE.getId().equals(formatoAllegato))
//				verifica che ci siano campi firma (?)
				&& (allSignatureNumber(docAllegato.accessContentStream(0)) > 0) && // su NSD lo compara con == 1 ???
//				verifica che l'effettivo numero dei campi firma da firmare sia superiore a zero
				(!PdfHelper.checkCampoFirma(docAllegato.accessContentStream(0))));
	}

	/**
	 * Metodo che ritorna il numero di firme.
	 *
	 * @param docAllegato documento
	 * @return numero di firme
	 */
	private static int allSignatureNumber(final InputStream docAllegato) {
		// recupero il totale dei campi firma presenti in un documento sia firmati che
		// non
		final byte[] bytes = FileUtils.getByteFromInputStream(docAllegato);
		final int a = PdfHelper.getSignatureNumber(bytes);
		final int b = PdfHelper.getBlankSignatureNumber(bytes);
		return a + b;
	}

	/**
	 * Recupero dello stato del Documento dalla coda trasformazione.
	 *
	 * @param idDocumento identificativo documento
	 * @param connection  connessione
	 * @return stato documento
	 */
	private int getStatoDocumentoFromCodaTrasformazione(final String idDocumento, final Integer idAoo, final Connection connection) {
		final int id = Integer.parseInt(idDocumento);
		int stato = 0;
		try {
			stato = codaDAO.getStatoDocumentoFromCodaTrasformazione(id, idAoo, connection);
		} catch (final Exception e) {
			LOGGER.error("Attenzione, errore nel recupero dello stato del documento: " + idDocumento, e);
		}
		return stato;
	}

	/**
	 * Checks if is riservato.
	 *
	 * @param fceh
	 * @param idDocumento
	 * @return flag che indica se il documento è riservato
	 */
	private static boolean isRiservato(final IFilenetCEHelper fceh, final String idDocumento) {
		// recupero dati ce epr verificare se il documento è riservato
		final PropertiesProvider pp = PropertiesProvider.getIstance();
		final Document documentCE = fceh.getDocumentForDetail(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), idDocumento);
		final Integer riservato = (Integer) TrasformerCE.getMetadato(documentCE, PropertiesNameEnum.RISERVATO_METAKEY);
		return riservato != null && riservato > 0;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV#procediDaCorriere(it.ibm.red.business.dto.FilenetCredentialsDTO,
	 *      java.lang.String, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public EsitoOperazioneDTO procediDaCorriere(final FilenetCredentialsDTO fcDTO, final String wobNumber, final UtenteDTO utente) {
		FilenetPEHelper fpeh = null;
		try {
			fpeh = new FilenetPEHelper(fcDTO);
			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);
			final String[] responses = fpeh.getResponses(wob);
			final String response = LibroFirmaPerFirmaResponseEnum.PROCEDI_DA_CORRIERE.firstIn(responses);

			return genericNextStep(wobNumber, utente, response);
		} finally {
			logoff(fpeh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV#procediDaCorriere(java.util.Collection,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<EsitoOperazioneDTO> procediDaCorriere(final Collection<String> wobNumbers, final UtenteDTO utente) {
		final FilenetCredentialsDTO fcDTO = utente.getFcDTO();
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		for (final String wobNumber : wobNumbers) {
			esiti.add(procediDaCorriere(fcDTO, wobNumber, utente));
		}
		return esiti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV#spedisciMail(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "spedisciMail")
	public Collection<EsitoOperazioneDTO> spedisciMail(final UtenteDTO utente, final Collection<String> wobNumbers) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		for (final String wobNumber : wobNumbers) {
			esiti.add(spedisciMail(utente, wobNumber));
		}
		return esiti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV#spedisciMail(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "spedisciMail")
	public EsitoOperazioneDTO spedisciMail(final UtenteDTO utente, final String wobNumber) {
		final EsitoOperazioneDTO output = new EsitoOperazioneDTO(wobNumber);
		FilenetPEHelper fpeh = null;
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);
			final Integer nIdDocumento = (Integer) TrasformerPE.getMetadato(wob,
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
			output.setIdDocumento(nIdDocumento);

			// Response spedisci_mail
			final Integer nUpdate = gestioneNotificheEmailDAO.updateStatoRicevuta(connection, nIdDocumento, 0);

			if (nUpdate == 0) {
				output.setNote("Aggiornamento non avvenuto per il documento: " + nIdDocumento);
			} else {
				output.setEsito(true);
				output.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la gestione della response spedisci mail.", e);
			output.setNote("Si è verificato un errore durante la gestione della response spedisci mail.");
		} finally {
			closeConnection(connection);
			logoff(fpeh);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV#spostaInLavorazione(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "spostaInLavorazione")
	public EsitoOperazioneDTO spostaInLavorazione(final UtenteDTO utente, final String wobNumber) {
		final EsitoOperazioneDTO output = new EsitoOperazioneDTO(wobNumber);
		Integer numDoc = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);
			final Integer documentTitle = (Integer) TrasformerPE.getMetadato(wob,
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
			numDoc = fceh.getNumDocByDocumentTitle(documentTitle.toString());

			// Si avanza il workflow
			fpeh.nextStep(wob, null, ResponsesRedEnum.SPOSTA_IN_LAVORAZIONE.getResponse());

			output.setEsito(true);
			output.setIdDocumento(numDoc);
			output.setNote("Storno del documento effettuato con successo.");
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la gestione della response storna.", e);
			output.setIdDocumento(numDoc);
			output.setNote("Si è verificato un errore durante l'operazione di storno.");
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV#spostaInLavorazione(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "spostaInLavorazione")
	public final Collection<EsitoOperazioneDTO> spostaInLavorazione(final UtenteDTO utente, final Collection<String> wobNumbers) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		for (final String wobNumber : wobNumbers) {
			esiti.add(spostaInLavorazione(utente, wobNumber));
		}
		return esiti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV#avanzaWorkflowEStornaWfContributi(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String,
	 *      it.ibm.red.business.enums.ResponsesRedEnum).
	 */
	@Override
	public final EsitoOperazioneDTO avanzaWorkflowEStornaWfContributi(final UtenteDTO utente, final String wobNumber, final String motivoAssegnazione,
			final ResponsesRedEnum tipoResponse) {
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		final PropertiesProvider pp = PropertiesProvider.getIstance();
		Connection connection = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		try {
			// Fetch resource
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final VWWorkObject workObject = fpeh.getWorkFlowByWob(wobNumber, true);
			final Integer documentTitleInteger = (Integer) TrasformerPE.getMetadato(workObject, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
			final String documentTitle = documentTitleInteger.toString();

			final List<String> listaParametri = new ArrayList<>(1);
			listaParametri.add(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY));
			final Document doc = fceh.getDocumentByIdGestionale(documentTitle, listaParametri, null, utente.getIdAoo().intValue(), null, null);
			final Integer numeroDocumento = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			esito.setIdDocumento(numeroDocumento);

			final PropertiesProvider prop = PropertiesProvider.getIstance();
			final Map<String, Object> map = new HashMap<>();
			map.put(prop.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivoAssegnazione);
			map.put(prop.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId().toString());

			final boolean result = fpeh.nextStep(workObject, map, tipoResponse.getResponse());

			if (!result) {
				esito = new EsitoOperazioneDTO(wobNumber, false, "Si è verificato un errore durante la gestione della response " + tipoResponse.name() + ".");
			} else {
				// Storna workflow per contributo
				operationDocumentSRV.stornaWorkflowPerContributo(wobNumber, documentTitle, fpeh, utente.getIdAoo());

				if (ResponsesRedEnum.VALIDAZIONE_CONTRIBUTO.equals(tipoResponse)) {
					connection = setupConnection(getDataSource().getConnection(), false);

					contributoDAO.updateStatoContributoByIdUfficio(utente.getIdUfficio(), documentTitleInteger, StatoContributoEnum.VALIDATO.getId(), connection);
				}
				esito = new EsitoOperazioneDTO(null, null, numeroDocumento, wobNumber);
			}
		} catch (final FilenetException fne) {
			LOGGER.error("Attenzione, avanzamento del workFlow ( wobNumber: " + wobNumber + " ) non riuscito.", fne);
			esito.setEsito(false);
			esito.setNote("Attenzione, avanzamento del workflow non riuscito");
		} catch (final Exception e) {
			LOGGER.error("Attenzione, esecuzione della response " + ResponsesRedEnum.INOLTRA + " non riuscita.", e);
			esito.setEsito(false);
			esito.setNote("Si è verificato un errore durante il tentato sollecito della response 'INOLTRA'");
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}
		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV#getDocumentInfoForApplet(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public DocumentAppletContentDTO getDocumentInfoForApplet(final String wobNumber, final UtenteDTO utente) {
		DocumentAppletContentDTO output = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);
			final Integer nIdDocumento = (Integer) TrasformerPE.getMetadato(wob,
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));

			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final Document documento = fceh.getLastEditableVersion(String.valueOf(nIdDocumento), utente.getIdAoo().intValue());

			if (documento != null) {
				output = TrasformCE.transform(documento, TrasformerCEEnum.FROM_DOCUMENTO_TO_APPLET_CONTENT);
			}
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV#richiediFirmaCopiaConforme(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "richiediFirmaCopiaConforme")
	public final Collection<EsitoOperazioneDTO> richiediFirmaCopiaConforme(final UtenteDTO utente, final Collection<String> wobNumbers) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final String[] assegnazioniFirmatari = getAssegnazioniFirmatari(utente, con);

			EsitoOperazioneDTO esito = null;
			for (final String wobNumber : wobNumbers) {
				esito = new EsitoOperazioneDTO(wobNumber);
				esiti.add(richiediFirmaCopiaConforme(esito, utente, wobNumber, assegnazioniFirmatari, con));
			}
		} catch (final SQLException e) {
			LOGGER.error("Si è verificato un errore nella connessione verso il DB durante le richieste di firma Copia Conforme", e);
		} finally {
			closeConnection(con);
		}
		return esiti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV#richiediFirmaCopiaConforme(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "richiediFirmaCopiaConforme")
	public EsitoOperazioneDTO richiediFirmaCopiaConforme(final UtenteDTO utente, final String wobNumber) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final String[] assegnazioniFirmatari = getAssegnazioniFirmatari(utente, con);

			richiediFirmaCopiaConforme(esito, utente, wobNumber, assegnazioniFirmatari, con);
		} catch (final SQLException e) {
			LOGGER.error("Si è verificato un errore nella connessione verso il DB durante la richiesta di firma Copia Conforme per il workflow: " + wobNumber, e);
			esito.setNote("Si è verificato un errore durante la richiesta di firma Copia Conforme");
		} finally {
			closeConnection(con);
		}
		return esito;
	}

	private String[] getAssegnazioniFirmatari(final UtenteDTO utente, final Connection con) {
		final String[] assegnazioniFirmatari = new String[1];

		// Si calcola l'assegnazione del firmatario (il dirigente dell'ufficio)
		Long idNodoPredefinitoDirigenteUfficio = null;
		final Long idUtenteDirigenteUfficio = nodoDAO.getNodo(utente.getIdUfficio(), con).getDirigente().getIdUtente();

		final Collection<NodoUtenteRuolo> nurList = utenteDAO.getNodoUtenteRuolo(idUtenteDirigenteUfficio, null, con);
		for (final NodoUtenteRuolo nur : nurList) {
			if (!nur.getPredefinito().equals(0L)) {
				idNodoPredefinitoDirigenteUfficio = nur.getIdnodo();
				break;
			}
		}
		assegnazioniFirmatari[0] = idNodoPredefinitoDirigenteUfficio + "," + idUtenteDirigenteUfficio;

		return assegnazioniFirmatari;
	}

	private EsitoOperazioneDTO richiediFirmaCopiaConforme(final EsitoOperazioneDTO esito, final UtenteDTO utente, final String wobNumber, final String[] assegnazioniFirmatari,
			final Connection con) {
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final PropertiesProvider pp = PropertiesProvider.getIstance();

			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);
			final boolean flagCopiaConforme = Boolean.TRUE.equals(TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.FIRMA_COPIA_CONFORME_METAKEY)));

			if (flagCopiaConforme) {
				final Integer idDocumento = (Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
				final String idDocumentoString = String.valueOf(idDocumento);

				final Integer idUfficioCopiaConforme = (Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.UFFICIO_COPIA_CONFORME_WF_METAKEY));
				final Integer idUtenteCopiaConforme = (Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.UTENTE_COPIA_CONFORME_WF_METAKEY));

				final String[] assegnazioniCopiaConforme = new String[1];
				assegnazioniCopiaConforme[0] = idUfficioCopiaConforme + "," + (idUtenteCopiaConforme != null ? idUtenteCopiaConforme : "0");

				// ### Si modificano le security dei fascicoli in cui è presente il documento
				final boolean esitoModificaSecurityFascicoli = securitySRV.modificaSecurityFascicoli(idDocumentoString, utente, assegnazioniCopiaConforme, con);

				if (esitoModificaSecurityFascicoli) {
					final DetailAssegnazioneDTO detailDocument = securitySRV.getDocumentDetailAssegnazione(fceh, utente.getIdAoo(), idDocumentoString, false);
					final boolean isRiservato = Boolean.TRUE.equals(detailDocument.isRiservato());

					// ### Si ottengono le nuove security...
					final ListSecurityDTO securityDoc = securitySRV.getSecurityPerRiassegnazioneDocumento(utente, idDocumentoString,
							Long.valueOf(idUtenteCopiaConforme), Long.valueOf(idUfficioCopiaConforme), isRiservato, con);

					// ### ...e si usano per aggiornare le security del documento
					securitySRV.aggiornaEVerificaSecurityDocumento(idDocumentoString, utente.getIdAoo(), securityDoc, true, false, fceh, con);

					final Map<String, Object> metadatiWorkflow = new HashMap<>();
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_LIBROFIRMA_METAKEY), assegnazioniFirmatari);
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId());

					// ### Si avanza il workflow su FileNet
					final boolean avanzamentoWorkflowOk = fpeh.nextStep(wobNumber, metadatiWorkflow, ResponsesRedEnum.RICHIEDI_FIRMA_COPIA_CONFORME.getResponse());

					if (avanzamentoWorkflowOk) {
						// Si aggiornano le security dei documenti allacciati
						securitySRV.aggiornaSecurityDocumentiAllacciati(utente, idDocumento, securityDoc, assegnazioniFirmatari, fceh, con);

						// Si aggiornano le security dei contributi
						securitySRV.aggiornaSecurityContributiInseriti(utente, idDocumentoString, assegnazioniFirmatari, fceh, con);

						esito.setEsito(true);
						esito.setNote("Invio in firma per copia conforme effettuato con successo.");
					} else {
						throw new RedException(ERROR_AVANZAMENTO_WORKFLOW_MSG + wobNumber + " con la response: " + ResponsesRedEnum.RICHIEDI_FIRMA_COPIA_CONFORME.getResponse()
								+ " per il documento: " + idDocumentoString);
					}
				} else {
					throw new RedException("Si è verificato un errore durante la modifica delle security per il documento: " + idDocumentoString);
				}
			} else {
				esito.setNote("La tipologia documento selezionata non è associata alla firma per Copia Conforme");
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			esito.setNote("Si è verificato un errore durante la richiesta di firma Copia Conforme");
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}
		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.IOperationWorkFlowSRV#avanzaWorkFlowDaWs(java.lang.String,
	 *      java.util.Map, java.lang.String, boolean,
	 *      it.ibm.red.business.helper.filenet.pe.FilenetPEHelper).
	 */
	@Override
	public WorkFlowWsDTO avanzaWorkFlowDaWs(final String wobNumber, final Map<String, Object> metadati, final String response, final boolean isWithRouting,
			final FilenetPEHelper fpeh) {
		WorkFlowWsDTO output = null;

		try {

			final VWWorkObject workObj = fpeh.getWorkFlowByWob(wobNumber, true);
			final Boolean isAdvanced = fpeh.nextStep(workObj, metadati, response);

			if (Boolean.TRUE.equals(isAdvanced)) {
				output = new WorkFlowWsDTO();
				// Recupero il workflow number assegnato al workobject utilizzato
				// per l'avanzamento e viene restituito al chiamante.
				output.setWorkflowNumber(workObj.getWorkflowNumber());

				if (isWithRouting) {
					final List<WobNumberWsDTO> wobNumbers = fpeh.getWobNumbers(workObj.getWorkflowNumber());
					output.setWobNumbers(wobNumbers);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_AVANZAMENTO_WORKFLOW_MSG, e);
			throw new RedException(ERROR_AVANZAMENTO_WORKFLOW_MSG, e);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.IOperationWorkFlowSRV#avviaIstanzaWorkFlowDaWs(java.lang.String,
	 *      java.lang.String, java.lang.String, java.util.Map, java.lang.String,
	 *      boolean, it.ibm.red.business.helper.filenet.pe.FilenetPEHelper).
	 */
	@Override
	public WorkFlowWsDTO avviaIstanzaWorkFlowDaWs(final String name, String subject, final String comment, final Map<String, Object> metadati, final String response,
			final boolean isWithRouting, final FilenetPEHelper fpeh) {
		WorkFlowWsDTO output = null;
		List<WobNumberWsDTO> wobNumbers = new ArrayList<>();
		try {
			if (subject == null || subject.isEmpty()) {
				subject = fpeh.getValueByKey(name, PEProperty.F_SUBJECT);
				if (subject == null || subject.isEmpty()) {
					throw new FilenetException("Nessun Subject di default trovata per questo tipo di Workflow.");
				}
			}
			final String workflowNumber = fpeh.avviaIstanzaWorkflow(name, subject, comment, metadati, response);

			if (isWithRouting) {
				wobNumbers = fpeh.getWobNumbers(workflowNumber);
			}
			if (workflowNumber != null && !workflowNumber.isEmpty()) {
				output = new WorkFlowWsDTO();
				output.setWorkflowNumber(workflowNumber);
				output.setSubject(subject);
				output.setWobNumbers(wobNumbers);
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'avvio di una nuova istanza del workflow con name [ " + name + " ]: ", e);
			throw new RedException("Si è verificato un errore durante l'avvio di una nuova istanza del workflow con name [ " + name + " ]: ", e);
		}
		return output;
	}
}