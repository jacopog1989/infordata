package it.ibm.red.business.service.concrete;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants.Mail;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IMailSRV;
import it.ibm.red.business.service.IPredisponiFirmaAutografaSRV;
import it.ibm.red.business.utils.DestinatariRedUtils;

/**
 * Service di gestione predisposizione firma autografa.
 */
@Service
@Component
public class PredisponiFirmaAutografaSRV extends AbstractService implements IPredisponiFirmaAutografaSRV {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 8447337068295228612L;
	
	/**
	 * LOGGER.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PredisponiFirmaAutografaSRV.class.getName());
	
	/**
	 * Service.
	 */	
	@Autowired
	private IMailSRV mailSRV;
	
	/**
	 * Fornitore della properties dell'applicazione.
	 */
	private PropertiesProvider pp;
	
	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}
	
	
	@Override
	public final EsitoOperazioneDTO eseguiPredisposizionePerFirmaAutografa(final String wobNumber, final UtenteDTO utente) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		String documentTitle = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			
			// Si recupera il workflow da FileNet
			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);

			// Si recupera l'ID del documento dal workflow
			final Integer idDocumentoFilenet = (Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
			
			if (idDocumentoFilenet != null) {
				final HashMap<String, Object> metadati = new HashMap<>();
				documentTitle = String.valueOf(idDocumentoFilenet);
				
				final Document doc = fceh.getDocumentByIdGestionale(documentTitle, null, null, utente.getIdAoo().intValue(), null, null);
				
				final List<?> destinatariFilenet = (List<?>) TrasformerCE.getMetadato(doc, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
				// Si normalizza il metadato per evitare errori comuni durante lo split delle singole stringhe dei destinatari
				final List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(destinatariFilenet);
				
				// Si convertono tutti i destinatari elettronici in cartacei...
				final String[] nuoviDestinatariFilenet = convertiDestinatariInCartacei(destinatariDocumento);
				
				// ...e si aggiorna il relativo metadato su FileNet (CE)
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY), nuoviDestinatariFilenet);
				fceh.updateMetadati(doc, metadati);
				
				// Si elimina l'e-mail bozza da FileNet
				mailSRV.eliminaBozzaEmailDocumento(documentTitle, utente.getFcDTO(), utente.getIdAoo());
				
				metadati.clear();
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId());
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), null);
				
				// Si esegue l'avanzamento del workflow con la response selezionata
				final boolean avanzamentoWorkflowOk = fpeh.nextStep(wob, metadati, ResponsesRedEnum.PREDISPOSIZIONE_FIRMA_AUTOGRAFA.getResponse());
				
				if (avanzamentoWorkflowOk) {
					final Integer numeroDocumento = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
					
					esito.setEsito(true);
					esito.setIdDocumento(numeroDocumento);
					esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("predisponiPerFirmaAutografa -> Si è verificato un errore durante la predisposizione per la firma autografa per il documento: " + documentTitle, e);
			esito.setNote("Si è verificato un errore durante la predisposizione per la firma autografa.");
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}
		
		return esito;
	}
	
	
	private String[] convertiDestinatariInCartacei(final List<String[]> destinatariFilenet) {
		String[] destinatariCartaceiFilenet = new String[0];
		
		if (!CollectionUtils.isEmpty(destinatariFilenet)) {
			final List<String> nuoviDestinatari = new ArrayList<>();
			String nuovoDestinatario;
			
			for (final String[] destinatarioFilenetSplit : destinatariFilenet) {
				nuovoDestinatario = StringUtils.join(destinatarioFilenetSplit, ",");
				
				if (TipologiaDestinatarioEnum.ESTERNO.getTipologia().equals(destinatarioFilenetSplit[Mail.IE_MAIL_POSITION])) {
					destinatarioFilenetSplit[Mail.MEZZO_SPEDIZIONE_POSITION] = MezzoSpedizioneEnum.CARTACEO.getId().toString();
					nuovoDestinatario = StringUtils.join(destinatarioFilenetSplit, ",");
				}
				
				nuoviDestinatari.add(nuovoDestinatario);
			}
			
			destinatariCartaceiFilenet = new String[nuoviDestinatari.size()];
			destinatariCartaceiFilenet = nuoviDestinatari.toArray(destinatariCartaceiFilenet);
		}
		
		return destinatariCartaceiFilenet;
	}
}