package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * The Class AooFilenet.
 *
 * @author CPIERASC
 * 
 *         Entità che mappa la tabella AOO_FILENET.
 */
public class AooFilenet implements Serializable {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Aoo.
	 */	
	private final Aoo aoo;

	/**
	 * Connection point.
	 */	
	private final String connectionPoint;

	/**
	 * Identificativo Client AOO.
	 */	
	private final String idClientAoo;

	/**
	 * Object store.
	 */	
	private final String objectStore;

	/**
	 * Password.
	 */	
	private final String password;

	/**
	 * Pk AOO Filenet.
	 */	
	private final Long pkAooFilenet;

	/**
	 * Stanza Jaas.
	 */	
	private final String stanzaJaas;

	/**
	 * Uri.
	 */	
	private final String uri;

	/**
	 * Username.
	 */	
	private final String username;

	/**
	 * Costruttore.
	 * 
	 * @param inUri				uri
	 * @param inPkAooFilenet	pk
	 * @param inUsername		username
	 * @param inPassword		password
	 * @param inObjectStore		object store
	 * @param inIdClientAoo		id client aoo
	 * @param inConnectionPoint	connection point
	 * @param inStanzaJaas		stanza jass
	 * @param inAoo				aoo
	 */
	public AooFilenet(final String inUri, final Long inPkAooFilenet, final String inUsername, final String inPassword, final String inObjectStore, 
			final String inIdClientAoo, final String inConnectionPoint, final String inStanzaJaas, final Aoo inAoo) {
		uri = inUri;
		pkAooFilenet = inPkAooFilenet;
		username = inUsername;
		password = inPassword;
		objectStore = inObjectStore;
		idClientAoo = inIdClientAoo;
		connectionPoint = inConnectionPoint;
		stanzaJaas = inStanzaJaas;
		aoo = inAoo;
	}

	/**
	 * Getter aoo.
	 * 
	 * @return aoo
	 */
	public final Aoo getAoo() {
		return aoo;
	}

	/**
	 * Getter connection endpoint.
	 * 
	 * @return connection endpoint
	 */
	public final String getConnectionPoint() {
		return connectionPoint;
	}

	/**
	 * Getter id client aoo.
	 * 
	 * @return id client aoo
	 */
	public final String getIdClientAoo() {
		return idClientAoo;
	}

	/**
	 * Getter object store.
	 * 
	 * @return object store
	 */
	public final String getObjectStore() {
		return objectStore;
	}

	/**
	 * Getter password.
	 * 
	 * @return password
	 */
	public final String getPassword() {
		return password;
	}

	/**
	 * Getter pk aoo filenet.
	 * 
	 * @return	pk aoo filenet
	 */
	public final Long getPkAooFilenet() {
		return pkAooFilenet;
	}

	/**
	 * Getter stanza jaas.
	 * 
	 * @return	stanza jaas
	 */
	public final String getStanzaJaas() {
		return stanzaJaas;
	}

	/**
	 * Getter uri.
	 * 
	 * @return	uri
	 */
	public final String getUri() {
		return uri;
	}

	/**
	 * Getter username.
	 * 
	 * @return	username
	 */
	public final String getUsername() {
		return username;
	}

}