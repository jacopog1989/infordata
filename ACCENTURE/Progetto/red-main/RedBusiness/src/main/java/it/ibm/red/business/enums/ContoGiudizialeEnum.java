package it.ibm.red.business.enums;

/**
 * Enum contenente i nomi dei metadati
 * relativi ai conti giudiziali UCB.
 */
public enum ContoGiudizialeEnum {
	
	/**
	 * Metadato ESERCIZIO - Anno di esercizio.
	 */
	ESERCIZIO("ESERCIZIO"),
	
	/**
	 * Lookup Tipo Cont.
	 */
	TIPO_CONT("TIPO_CONT"),
	
	/**
	 * Lookup Agente Cont.
	 */
	AGECONT("AGECONT"),
	
	/**
	 * Lookup Agente Cont.
	 */
	AGE_CONT("AGE_CONT");;
	
	/**
	 * Nome metadato.
	 */
	private String metadatoName;
	
	/**
	 * Costruttore.
	 * 
	 * @param metadatoName
	 */
	private ContoGiudizialeEnum(String metadatoName) {
		this.metadatoName = metadatoName;
	}

	/**
	 * Restituisce il nome del metadato.
	 * 
	 * @return nome metadato
	 */
	public String getMetadatoName() {
		return metadatoName;
	}

}
