package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.ICasellaPostaleDAO;
import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.FunzionalitaEnum;
import it.ibm.red.business.enums.MailOperazioniEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.ICasellePostaliSRV;

/**
 * Service gestione caselle postali.
 */
@Service
public class CasellePostaliSRV extends AbstractService implements ICasellePostaliSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -8020677603411323955L;

	/**
	 * Messaggio di errore recupero caselle postali.
	 */
	private static final String ERROR_RECUPERO_CASELLE_MSG = "Errore nel recupero delle caselle postali";
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CasellePostaliSRV.class.getName());
	
	/**
	 * DAO casella postale.
	 */
	@Autowired
	private ICasellaPostaleDAO casellaPostaleDAO;

	/**
	 * @see it.ibm.red.business.service.ICasellePostaliSRV#getCasellePostali(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      it.ibm.red.business.enums.FunzionalitaEnum).
	 */
	@Override
	public List<CasellaPostaDTO> getCasellePostali(final UtenteDTO utente, final IFilenetCEHelper fceh, final FunzionalitaEnum f) {
		Connection connection =  null;
		final List<CasellaPostaDTO> caselleAbilitate = new ArrayList<>();
		try {
			
			final List<CasellaPostaDTO> casellePostali = fceh.getCasellePostaliDTO();
			
			connection = setupConnection(getDataSource().getConnection(), false);
			final Long idNodo = utente.getIdUfficio();
			for (final CasellaPostaDTO cp: casellePostali) {
				LOGGER.info("####INFO#### Inizio Ordinamento CP Abilitata");
				final int ordinamento = casellaPostaleDAO.getOrdinamentoCPAbilitata(cp.getIndirizzoEmail(), idNodo, f, connection);
				LOGGER.info("####INFO#### Fine Ordinamento CP Abilitata");
				if (ordinamento != -1) {
					cp.setOrdinamento(ordinamento);
					caselleAbilitate.add(cp);
				}
			}
			
			LOGGER.info("####INFO#### Inizio Sort CP Abilitata");
			//ordina la lista di caselle
			Collections.sort(caselleAbilitate);
			LOGGER.info("####INFO#### Fine Sort CP Abilitata");
			
			return caselleAbilitate;
			
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_CASELLE_MSG, e);
			throw new RedException(ERROR_RECUPERO_CASELLE_MSG, e);
		} finally {
			closeConnection(connection);
		}
		
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICasellePostaliFacadeSRV#getCasellePostali(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.enums.FunzionalitaEnum).
	 */
	@Override
	public List<CasellaPostaDTO> getCasellePostali(final UtenteDTO utente, final FunzionalitaEnum f) {
		IFilenetCEHelper fceh = null;
		
		try {
			
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			return getCasellePostali(utente, fceh, f);
			
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_CASELLE_MSG, e);
			throw new RedException(ERROR_RECUPERO_CASELLE_MSG, e);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ICasellePostaliSRV#getCasellaPostale(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      java.lang.String).
	 */
	@Override
	public CasellaPostaDTO getCasellaPostale(final IFilenetCEHelper fceh, final String nomeCasella) {
		return fceh.getCasellaPostaleDTO(nomeCasella);
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICasellePostaliFacadeSRV#getCasellaPostale(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	public CasellaPostaDTO getCasellaPostale(final UtenteDTO utente, final String nomeCasella) {
		IFilenetCEHelper fceh = null;
		try {
			
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			return getCasellaPostale(fceh, nomeCasella);
			
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_CASELLE_MSG, e);
			throw new RedException(ERROR_RECUPERO_CASELLE_MSG, e);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICasellePostaliFacadeSRV#getResponses(java.lang.Long,
	 *      java.lang.String).
	 */
	@Override
	public List<MailOperazioniEnum> getResponses(final Long idNodo, final String casellaPostale) {
		Connection connection =  null;
		try {
			
			connection = setupConnection(getDataSource().getConnection(), false);
			
			final List<Integer> responseIds = casellaPostaleDAO.getCasellaPostaleNodoResponse(casellaPostale, idNodo, connection);
			final List<MailOperazioniEnum> responses = new ArrayList<>();
			for (final Integer responseId: responseIds) {
				responses.add(MailOperazioniEnum.get(responseId));
			}
			return responses;
		} catch (final Exception e) {
			LOGGER.error("Errore nella verifica dell'associazione fra la response recupero delle caselle postali", e);
			throw new RedException("Errore nella verifica dell'associazione fra la response recupero delle caselle postali", e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICasellePostaliFacadeSRV#getMaxSizeContentCasellaPostale(java.lang.String).
	 */
	@Override
	public Integer getMaxSizeContentCasellaPostale(final String casellaPostale) {
		Connection connection =  null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			
			return casellaPostaleDAO.getMaxSizeContentCasellaPostale(casellaPostale, connection);
 
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero della dimensiona massima permessa della casella postale " + casellaPostale, e);
			throw new RedException("Errore nel recupero della dimensiona massima permessa della casella postale " + casellaPostale, e);
		} finally {
			closeConnection(connection);
		}
	}

}