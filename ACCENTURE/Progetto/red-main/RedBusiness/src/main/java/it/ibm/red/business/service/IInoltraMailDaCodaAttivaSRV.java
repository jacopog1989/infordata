package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IInoltraMailDaCodaAttivaFacadeSRV;

/**
 * Interface del servizio di inoltro mail da coda attiva.
 */
public interface IInoltraMailDaCodaAttivaSRV extends IInoltraMailDaCodaAttivaFacadeSRV {

}
