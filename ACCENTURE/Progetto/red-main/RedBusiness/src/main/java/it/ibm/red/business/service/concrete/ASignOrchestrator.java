package it.ibm.red.business.service.concrete;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignStepResultDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.SignStrategyEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IASignOrchestrator;
import it.ibm.red.business.service.IASignSRV;
import it.ibm.red.business.service.asign.step.IASignStep;
import it.ibm.red.business.service.asign.step.StatusEnum;
import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * Orchestrator di firma asincrona - gestisce l'esecuzione degli step per un item nell'ordine appropriato e
 * saltando eventuali step non necessari.
 */
@Service
public class ASignOrchestrator implements IASignOrchestrator {

	private static final String SKIPPING_STEP = "Skipping step ";

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 4443804598375563726L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ASignOrchestrator.class.getName());

	/**
	 * Servizio firma asincrona.
	 */
	@Autowired
	private IASignSRV aSignSRV;

	/**
	 * Esegue il run di uno step simulandone un crash.
	 * @param idItem
	 * @param crashStep
	 * @return id item
	 */
	@Override
	public Long playSimulatingCrash(Long idItem, StepEnum crashStep) {
		ASignItemDTO item = aSignSRV.getItem(idItem);
		if (StatusEnum.RETRY.equals(item.getStatus())) {
			resume(item, crashStep);
		} else {
			play(item.getId(), crashStep);
		}
		return item.getId();
	}
	
	/**
	 * Esegue il run del primo item lavorabile.
	 * @return id item
	 */
	@Override
	public Long play() {
		Long idItem = aSignSRV.getFirstItem();
		if (idItem!=null) {
			play(idItem, null);
		}
		return idItem;
	}
	
	/**
	 * Esegue il run di uno step identificato dall'id.
	 * @return id item
	 */
	@Override
	public Long play(Long idItem) {
		if (idItem!=null) {
			play(idItem, null);
		}
		return idItem;
	}

	/**
	 * Esegue il play di uno step simulando un crash se <code>crashStep</code> non è null.
	 * @param idItem
	 * @param crashStep
	 */
	private void play(Long idItem, StepEnum crashStep) {
		LOGGER.info("Play " + idItem);
		aSignSRV.updateStatus(idItem, StatusEnum.IN_ELABORAZIONE);
		StepEnum finalStep = executeSteps(idItem, StepEnum.START, crashStep);
		
		// Se nel processo di Play non si raggiunge lo stato finale, l'item viene portato nello stato
		// Retry impostando a 0 il numero dei retry in quanto l'ultimo tentativo non era un "Retry".
		if(finalStep!=StepEnum.STOP) {
			Integer nRetry = 0;
			aSignSRV.setRetry(idItem, nRetry, true);
			aSignSRV.updateStatus(idItem, StatusEnum.RETRY);
		}
	}

	/**
	 * Esegue il resume per il primo item resumable recuperato.
	 * @return id item se lavorato, null se nessun item lavorabile esiste
	 */
	@Override
	public Long resume() {
		ASignItemDTO item = aSignSRV.getFirstResumableItem();
		if(item!=null) {
			resume(item, null);
			return item.getId();
		} else {
			return null;
		}
	}
	
	/**
	 * Esegue il resume per un item.
	 * @param item da lavorare
	 * @return id item se lavorato, null se nessun item lavorabile esiste
	 */
	@Override
	public Long resume(ASignItemDTO item) {
		if(item!=null) {
			resume(item, null);
			return item.getId();
		} else {
			return null;
		}
	}

	/**
	 * Esegue il resume per un item simulando un crash se <code>crashStep</code> non è null.
	 * @param item
	 * @param crashStep
	 */
	private void resume(ASignItemDTO item, StepEnum crashStep) {
		LOGGER.info("Resume " + item.getId());
		if (StatusEnum.RETRY.equals(item.getStatus())) {
			StepEnum cs = item.getStep();
			LOGGER.info("Executing steps");
			StepEnum finalStep = executeSteps(item.getId(), cs, crashStep);
			
			if(!StepEnum.STOP.equals(finalStep)) {
				if (finalStep != null && finalStep.equals(cs)) {
					LOGGER.info("Same state");
					handleRetry(item);
				} else {
					// Se l'errore si ha su uno Step diverso dal precedente, allora è il primo tentativo non andato a buon fine su questo Step.
					Integer nRetry = 1;
					aSignSRV.setRetry(item.getId(), nRetry, false);
					aSignSRV.updateStatus(item.getId(), StatusEnum.RETRY);
				}
			}
		}
	}

	/**
	 * Gestisce i retry in caso di errore durante un {@link #play()}.
	 * @param item
	 */
	private void handleRetry(ASignItemDTO item) {
		Integer nMaxRetry = Integer.valueOf(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ASIGN_MAX_RETRY)) - 1;
		if (item.getnRetry() < nMaxRetry) {
			LOGGER.info("Add retry");
			aSignSRV.setRetry(item.getId(), item.getnRetry() + 1, false);
			aSignSRV.updateStatus(item.getId(), StatusEnum.RETRY);
		} else {
			LOGGER.info("Set error state");
			aSignSRV.setRetry(item.getId(), 1, true);
			aSignSRV.updateStatus(item.getId(), StatusEnum.ERRORE);
		}
	}

	/**
	 * Manda in esecuzione tutti gli step partendo dall'ultimo step raggiunto senza
	 * errori per un item. Salta gli eventuali step non previsti per un item in base
	 * allo stato dello stesso e aggiorna il numero dei retry in caso di errore.
	 * Simula un crash se <code>crashStep</code> non è null.
	 * 
	 * @param idItem
	 * @param currentStep
	 * @param crashStep
	 * @return step enum raggiunto, se diverso da <code>StepEnum.STOP</code>
	 *         gestisce l'errore
	 */
	private StepEnum executeSteps(Long idItem, StepEnum currentStep, StepEnum crashStep) {
		StepEnum cStep = currentStep;
		ASignStepResultDTO result = null;
		LOGGER.info("Executing from step " + currentStep);
		while (cStep != StepEnum.STOP) {
			LOGGER.info("Executing step " + cStep);
			switch (cStep) {
			case PROTOCOLLAZIONE:
				result = handleProtocollazione(idItem, crashStep, cStep);
				break;
			// Step di registrazione ausiliaria
			case REG_AUX_PROTOCOLLAZIONE:
			case REG_AUX_RIGENERAZIONE_CONTENT:
				result = handleRegistrazioneAusiliaria(idItem, crashStep, cStep);
				break;
			// Tutti gli altri step devono essere sempre lavorati in maniera asincrona
			default:
				result = executeStep(idItem, cStep, crashStep);
				break;
			}

			if (result != null && !Boolean.TRUE.equals(result.getStatus())) {
				LOGGER.info("Error executing step");
				break;
			}
			cStep = StepEnum.nextStep(cStep);
		}
		if (StepEnum.STOP.equals(cStep)) {
			LOGGER.info("Orchestration end.");
			aSignSRV.updateStatus(idItem, StatusEnum.ELABORATO);
			aSignSRV.insertStopStep(idItem);
		} else if(cStep != null){
			LOGGER.info("Need to retry.");
		}
		return cStep;
	}

	/**
	 * Esegue gli step della registrazione ausiliaria per l'item identificato dall'
	 * {@code idItem}. Se il {@code crashStep} è valorizzato con
	 * {@code StepEnum.REG_AUX_PROTOCOLLAZIONE} oppure con
	 * {@code StepEnum.REG_AUX_RIGENERAZIONE_CONTENT} viene simulato un errore
	 * durante l'esecuzione di quello step.
	 * 
	 * @param idItem
	 *            Identificativo dell'item da processare.
	 * @param crashStep
	 *            Step per il quale deve essere simulato il crash.
	 * @param cStep
	 *            Step da eseguire.
	 * @return Risultato dell'esecuzione dello step.
	 */
	private ASignStepResultDTO handleRegistrazioneAusiliaria(Long idItem, StepEnum crashStep, StepEnum cStep) {
		
		ASignStepResultDTO result = null;
		// Se la strategia prevede la lavorazione dello step in maniera asincrona
		if(aSignSRV.getSignStrategy(idItem).equals(SignStrategyEnum.ASYNC_STRATEGY_B)) {
			if(!Boolean.TRUE.equals(aSignSRV.hasRegistrazioneAusiliaria(aSignSRV.getItem(idItem)))) {
				LOGGER.info("Need Registrazione Ausiliaria");
				result  = executeStep(idItem, cStep, crashStep);
			}
		}
		// Se la strategia prevede la lavorazione dello step in maniera sincrona, questa attività sarà già stata eseguita a questo livello
		else {
			LOGGER.info(SKIPPING_STEP + cStep);
		}
		return result;
	}

	/**
	 * Esegue lo step di protocollazione per l'item identificato dall'
	 * {@code idItem}. Se il {@code crashStep} è valorizzato con
	 * {@code StepEnum.PROTOCOLLAZIONE} viene simulato un errore durante
	 * l'esecuzione dello step.
	 * 
	 * @param idItem
	 *            Identificativo dell'item da processare.
	 * @param crashStep
	 *            Step per il quale deve essere simulato il crash.
	 * @param cStep
	 *            Step da eseguire.
	 * @return Risultato dell'esecuzione dello step.
	 */
	private ASignStepResultDTO handleProtocollazione(Long idItem, StepEnum crashStep, StepEnum cStep) {
		
		ASignStepResultDTO result = null;
		if(!Boolean.TRUE.equals(aSignSRV.isIterFirmaMultipla(idItem))) {
			// Se la strategia prevede la lavorazione dello step in maniera asincrona
			if(aSignSRV.getSignStrategy(idItem).equals(SignStrategyEnum.ASYNC_STRATEGY_B)) {
				Boolean bIsProtocollato = aSignSRV.isProtocollato(idItem);
				if (!Boolean.TRUE.equals(bIsProtocollato)) {
					LOGGER.info("Need Protocollazioni");
					result = executeStep(idItem, cStep, crashStep);
				}
			} 
			// Se la strategia prevede la lavorazione dello step in maniera sincrona, questa attività sarà già stata eseguita a questo livello
			else {
				LOGGER.info(SKIPPING_STEP + cStep);
			}
		} 
		// Se l'iter è quello di firma multipla, lo step di protocollazione deve essere saltato
		else {
			LOGGER.info(SKIPPING_STEP + cStep);
		}
		return result;
	}

	/**
	 * Esegue il run di uno step simulando un crash se <code>crashStep!=null</code>.
	 * @param idItem
	 * @param nextStep
	 * @param crashStep
	 * @return risultato dell'esecuzione
	 */
	private ASignStepResultDTO executeStep(Long idItem, StepEnum nextStep, StepEnum crashStep) {
		ASignStepResultDTO out = null;
		try {
			Class<? extends IASignStep> clazz = nextStep.getExecutor();
			if (clazz != null) {
				ASignItemDTO item = aSignSRV.getItem(idItem);
				IASignStep executorStep = ApplicationContextProvider.getApplicationContext().getBean(clazz);
				if (nextStep.equals(crashStep)) {
					LOGGER.info("Simulating crash.");
					out = executorStep.simulateCrash(item);
				} else {
					LOGGER.info("Normal running.");
					out = executorStep.execute(item);
				}
			}
		} catch (Exception e) {
			throw new RedException("Problemi nell'esecuzione dello step " + nextStep, e);
		} finally {
			if(out != null) {
				aSignSRV.updateStep(out);
			}
		}
		return out;
	}

}
