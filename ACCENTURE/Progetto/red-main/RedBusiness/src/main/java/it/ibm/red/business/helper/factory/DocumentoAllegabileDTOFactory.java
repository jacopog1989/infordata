package it.ibm.red.business.helper.factory;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import javax.activation.DataHandler;

import org.apache.axiom.attachments.ByteArrayDataSource;
import org.apache.commons.io.IOUtils;

import com.filenet.api.core.Document;
import com.filenet.api.property.Properties;
import com.google.common.net.MediaType;

import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO.DocumentoAllegabileType;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.FileUtils;

/**
 * Factory per documenti allegabili.
 */
public final class DocumentoAllegabileDTOFactory {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DocumentoAllegabileDTOFactory.class.getName());

	/**
	 * Costruttore vuoto.
	 */
	private DocumentoAllegabileDTOFactory() {
		// Costruttore vuoto.
	}

	/**
	 * Crea il documento (principale) allegabile DTO e lo restituisce sotto forma di
	 * DTO che contiene tutte le informazioni relative ad esso.
	 * 
	 * @param documento
	 * @param fascicolo
	 * @param withContent
	 * @return DTO con informazioni sul documento allegabile
	 */
	public static DocumentoAllegabileDTO getDocumentoAllegabileDTO(final Document documento, final FascicoloDTO fascicolo, final boolean withContent) {
		try {
			final Properties propDocumentoPrincipale = documento.getProperties();

			final PropertiesProvider pp = PropertiesProvider.getIstance();

			final String documentTitle = propDocumentoPrincipale.getStringValue(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
			final String documentTitleDocumentoPrincipale = propDocumentoPrincipale.getStringValue(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
			final String oggetto = propDocumentoPrincipale.getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY));
			final Integer idTipologiaDocumento = (Integer) propDocumentoPrincipale.getObjectValue(pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY));
			final DocumentoAllegabileType tipo = DocumentoAllegabileType.DOCUMENTO;
			final String contentType = documento.get_MimeType();
			DataHandler contenuto = FilenetCEHelper.getDocumentContentAsDataHandler(documento);
			final BigDecimal dimensione = getDimensione(contenuto);
			final String nomeFile = propDocumentoPrincipale.getStringValue(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));

			// Gestione attributo NumProt/Id
			final Integer annoProtocollo = (Integer) propDocumentoPrincipale.getObjectValue(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY));
			final Date dataProtocollo = (Date) propDocumentoPrincipale.getObjectValue(pp.getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY));
			final Integer numeroProtocollo = (Integer) propDocumentoPrincipale.getObjectValue(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY));
			final Integer annoDocumento = (Integer) propDocumentoPrincipale.getObjectValue(pp.getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY));
			final Integer numeroDocumento = (Integer) propDocumentoPrincipale.getObjectValue(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY));

			final String numProtId = getNumProtId(annoProtocollo, numeroProtocollo, numeroDocumento, annoDocumento, nomeFile);

			final String guid = documento.get_Id().toString();
			final Date dataDocumento = documento.get_DateCreated();

			final Integer idCategoria = (Integer) propDocumentoPrincipale.getObjectValue(pp.getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY));

			if (!withContent) {
				contenuto = null;
			}

			return new DocumentoAllegabileDTO(documentTitleDocumentoPrincipale, documentTitle, numProtId, oggetto, nomeFile, contentType, contenuto, dimensione, tipo,
					fascicolo, idTipologiaDocumento, guid, dataDocumento, numeroProtocollo, dataProtocollo, annoProtocollo, numeroDocumento, annoDocumento, idCategoria, false,
					false, null);

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di creazione del documento (principale) allegabile DTO", e);
			throw new RedException(e);
		}
	}

	/**
	 * Crea il documento allegabile a partire dai parametri in ingresso e lo
	 * restituisce in uscita.
	 * 
	 * @param allegato
	 * @param documentoPrincipale
	 * @param fascicolo
	 * @param withContent
	 * @return DocumentoAllegabileDTO
	 */
	public static DocumentoAllegabileDTO getDocumentoAllegabileDTO(final Document allegato, final Document documentoPrincipale, final FascicoloDTO fascicolo,
			final boolean withContent) {
		try {

			final Properties propAllegato = allegato.getProperties();
			final Properties propDocumentoPrincipale = documentoPrincipale.getProperties();

			final PropertiesProvider pp = PropertiesProvider.getIstance();

			final String documentTitle = propAllegato.getStringValue(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
			final String documentTitleDocumentoPrincipale = propDocumentoPrincipale.getStringValue(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
			final String oggetto = propAllegato.getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY));
			final Integer idTipologiaDocumento = (Integer) propDocumentoPrincipale.getObjectValue(pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY));
			final DocumentoAllegabileType tipo = DocumentoAllegabileType.ALLEGATO;
			final String contentType = allegato.get_MimeType();
			DataHandler contenuto = FilenetCEHelper.getDocumentContentAsDataHandler(allegato);
			final BigDecimal dimensione = getDimensione(contenuto);
			final String nomeFile = propAllegato.getStringValue(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));

			// Gestione attributo NumProt/Id
			final Integer annoProtocollo = (Integer) propDocumentoPrincipale.getObjectValue(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY));
			final Date dataProtocollo = (Date) propDocumentoPrincipale.getObjectValue(pp.getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY));
			final Integer numeroProtocollo = (Integer) propDocumentoPrincipale.getObjectValue(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY));
			final Integer annoDocumento = (Integer) propDocumentoPrincipale.getObjectValue(pp.getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY));
			final Integer numeroDocumento = (Integer) propDocumentoPrincipale.getObjectValue(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY));
			final String nomeDocumento = propDocumentoPrincipale.getStringValue(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));

			final String numProtId = getNumProtId(annoProtocollo, numeroProtocollo, numeroDocumento, annoDocumento, nomeDocumento);

			final String guid = allegato.get_Id().toString();
			final Date dataDocumento = allegato.get_DateCreated();

			final Integer idCategoria = (Integer) propDocumentoPrincipale.getObjectValue(pp.getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY));

			boolean allegatoNonSbustato = false;
			final Boolean allegatoNonSbustatoBool = propAllegato.getBooleanValue(pp.getParameterByKey(PropertiesNameEnum.ALLEGATO_NON_SBUSTATO));

			if (allegatoNonSbustatoBool != null) {
				allegatoNonSbustato = allegatoNonSbustatoBool;
			}

			boolean visualizzaInfoVerificaFirma = false;
			ValueMetadatoVerificaFirmaEnum valoreVerificaFirma = null;
			// Va avviato il controllo di firma solo su file PDF e P7M e se...
			if ((MediaType.PDF.toString().equalsIgnoreCase(contentType) || FileUtils.isP7MFile(contentType))) {
				visualizzaInfoVerificaFirma = true;
				final Integer metadatoValiditaFirma = propAllegato.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA));

				if (metadatoValiditaFirma != null) {
					valoreVerificaFirma = ValueMetadatoVerificaFirmaEnum.get(metadatoValiditaFirma);
				}
			}

			if (!withContent) {
				contenuto = null;
			}

			return new DocumentoAllegabileDTO(documentTitleDocumentoPrincipale, documentTitle, numProtId, oggetto, nomeFile, contentType, contenuto, dimensione, tipo,
					fascicolo, idTipologiaDocumento, guid, dataDocumento, numeroProtocollo, dataProtocollo, annoProtocollo, numeroDocumento, annoDocumento, idCategoria,
					allegatoNonSbustato, visualizzaInfoVerificaFirma, valoreVerificaFirma);

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di creazione del documento (allegato) allegabile DTO", e);
			throw new RedException(e);
		}
	}

	/**
	 * Crea documento allegabile e ne restituisce il DTO aggiornato.
	 * 
	 * @param documentTitle
	 * @param emailOriginaleContent
	 * @param oggettoMail
	 * @param nomeEmail
	 * @param tipo
	 * @param contentType
	 * @param guid
	 * @param dataDocumento
	 * @return DTO contenente tutte le informazioni sul documento allegabile.
	 */
	public static DocumentoAllegabileDTO getDocumentoAllegabileDTO(final String documentTitle, final byte[] emailOriginaleContent, final String oggettoMail,
			final String nomeEmail, final DocumentoAllegabileType tipo, final String contentType, final String guid, final Date dataDocumento) {
		try {

			final DataHandler contenuto = new DataHandler(new ByteArrayDataSource(emailOriginaleContent));
			final BigDecimal dimensione = getDimensione(contenuto);

			return new DocumentoAllegabileDTO(documentTitle, oggettoMail, nomeEmail, contentType, contenuto, dimensione, tipo, guid, dataDocumento);

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di creazione del documento (email) allegabile DTO", e);
			throw new RedException(e);
		}

	}

	private static String getNumProtId(final Integer annoProt, final Integer numeroProt, final Integer numeroDoc, final Integer annoDoc, final String nomeDoc) {
		final StringBuilder numProtIdString = new StringBuilder();

		if (annoProt != null && annoProt > 0) {
			if (numeroProt != null && numeroProt > 0) {
				numProtIdString.append(numeroProt).append("/").append(annoProt).append(" - ");
				if (numeroDoc != null && numeroDoc > 0) {
					numProtIdString.append(numeroDoc);
				} else {
					numProtIdString.append(nomeDoc);
				}
			} else {
				numProtIdString.append(annoProt).append(" - ").append(nomeDoc);
			}
		} else if (numeroDoc != null && numeroDoc > 0) {
			numProtIdString.append(numeroDoc).append(" - ").append(annoDoc);
		} else {
			numProtIdString.append(nomeDoc).append(" - ").append(annoDoc);
		}

		return numProtIdString.toString();
	}

	private static BigDecimal getDimensione(final DataHandler dh) throws IOException {
		BigDecimal dimensione = BigDecimal.valueOf(0);

		if (dh != null) {
			final int lunghezzaByte = IOUtils.toByteArray(dh.getInputStream()).length;
			dimensione = BigDecimal.valueOf(lunghezzaByte).divide(BigDecimal.valueOf(1024), 0, RoundingMode.HALF_UP); // espressa in KB
		}

		return dimensione;
	}

}