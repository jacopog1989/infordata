package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;

import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.persistence.model.Contributo;

/**
 * 
 * @author CPIERASC
 *
 *	Dao per la gestione dei contributi interno o esterni.
 */
public interface IContributoDAO extends Serializable {
	
	
	/**
	 * Recupero contributi esterni per id documento.
	 * 
	 * @param documentTitle	identificativo documento
	 * @param idAoo			identificativo AOO
	 * @param connection	connessione
	 * @return				contributi esterni del documento
	 */
	Collection<Contributo> getContributiEsterni(String documentTitle, Long idAoo, Connection connection);

	/**
	 * Se non è presente un guid associato genera un pdf on-the-fly contenente la nota, altrimenti ritorna null (al service il compito di recuperare il documento su filenet).
	 * 	
	 * @param id			identificativo del contributo
	 * @param connection	connessione
	 * @param idDocumento	identificativo documento
	 * @return				il contenuto del contributo
	 */
	FileDTO getNotaContributoInternoContent(Long id, Connection connection, Integer idDocumento);
	
	/**
	 * Se non è presente un guid associato genera un pdf on-the-fly contenente la nota, altrimenti ritorna null (al service il compito di recuperare il documento su filenet).
	 * 	
	 * @param id			identificativo del contributo
	 * @param connection	connessione
	 * @param idDocumento	identificativo documento
	 * @return				il contenuto del contributo
	 */
	FileDTO getNotaContributoEsternoContent(Long id, Connection connection, Integer idDocumento);
	
	/**
	 * Recupera il contenuto dato id.
	 * 
	 * @param id			identificativo contributo
	 * @param flagEsterno	indica se il contributo è esterno o meno
	 * @param connection	connessione al database
	 * @return				guid contenuto
	 */
	ContributoDTO get(Long id, Connection connection, Boolean flagEsterno);

	/**
	 * Recupero numero contributi.
	 * 
	 * @param documentTitle	id documento
	 * @param connection	connessione
	 * @return				numero contributi
	 */
	Long countContributi(String documentTitle, Connection connection);
	
	/**
	 * Aggiorna lo stato del contributo tramite l'id dell'ufficio.
	 * @param idNodo - id del nodo
	 * @param idDocumento - id documento
	 * @param stato
	 * @param connection
	 * @return
	 */
	Integer updateStatoContributoByIdUfficio(Long idNodo, Integer idDocumento, Integer stato, Connection connection);
	
	/**
	 * Inserisce il contributo esterno per il documento.
	 * @param contributo
	 * @param con
	 * @return
	 */
	long insertContributoEsterno(Contributo contributo, Connection con);
	
	/**
	 * Inserisce il contributo interno per il documento.
	 * @param contributo
	 * @param connection
	 * @return
	 */
	long insertContributoInterno(Contributo contributo, Connection connection);

	/**
	 * Aggiorna lo stato del contributo.
	 * @param idContributo - id del contributo
	 * @param guidContributo
	 * @param isEsterno
	 * @param con
	 * @return
	 */
	Integer updateGuidContributo(Long idContributo, String guidContributo, boolean isEsterno, Connection con);

	/**
	 * Aggiorna la validità della firma del contributo esterno tramite il guid.
	 * @param guid
	 * @param parseInt
	 * @param connection
	 */
	void updateValiditaFirmaByGuid(String guid, int parseInt, Connection connection);

	/**
	 * Aggiorna lo stato del contributo.
	 * @param idContributo - id del contributo
	 * @param stato
	 * @param isEsterno
	 * @param con
	 * @return
	 */
	int updateStatoContributo(Long idContributo, Integer stato, boolean isEsterno, Connection con);

	/**
	 * Elimina il contributo esterno tramite l'id.
	 * @param idContributo
	 * @param con
	 * @return
	 */
	int deleteContributoEsterno(Long idContributo, Connection con);

	/**
	 * Aggiorna il contributo esterno tramite il protocollo.
	 * @param protocollo
	 * @param idAoo - id dell'Aoo
	 * @param mittente
	 * @param nota
	 * @param con
	 * @return
	 */
	int updateContributoEsternoByProtocollo(String protocollo, Long idAoo, String mittente, String nota, Connection con);

	/**
	 * Aggiorna il contributo interno tramite l'id dell'ufficio e l'id dell'utente.
	 * @param contributo
	 * @param idUfficio - id dell'ufficio
	 * @param idUtente - id dell'utente
	 * @param con
	 * @return
	 */
	int updateContributoInterno(ContributoDTO contributo, Long idUfficio, Long idUtente, Connection con);

	/**
	 * Ottiene i contributi esterni tramite il protocollo.
	 * @param protocollo
	 * @param idAoo - id dell'Aoo
	 * @param connection
	 * @return contributi esterni
	 */
	Collection<Contributo> getContributiEsterniByProtocollo(String protocollo, Long idAoo, Connection connection);
	
	/**
	 * Elimina i contributi esterni tramite il protocollo.
	 * @param protocollo
	 * @param idAoo - id dell'Aoo
	 * @param connection
	 * @return
	 */
	int deleteContributiEsterniByProtocollo(String protocollo, Long idAoo, Connection connection);
 
	/**
	 * Ottiene il contributo delle sottoscrizioni tramite id e id dell'Aoo.
	 * @param id
	 * @param idAoo - id dell'Aoo
	 * @param connection
	 * @return
	 */
	ContributoDTO getContributoSottoscrizioniByIDandAOO(Long id, Long idAoo, Connection connection);

	/**
	 * Ottiene i contributi interni.
	 * @param documentTitle - document title
	 * @param idAOO - id dell'Aoo
	 * @param connection
	 * @return contributi interni
	 */
	Collection<Contributo> getContributiInterni(String documentTitle, Long idAOO, Connection connection);
 
}