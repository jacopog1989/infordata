package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ResponsesRedEnum;

/**
 * Interfaccia dei metodi delle operazioni di sigla/vista/rifiuta.
 * 
 * @author CPAOLUZI
 *
 */
public interface IOperationDocumentFacadeSRV extends Serializable {

	/**
	 * Metodo per visto.
	 * 
	 * @param fcDTO       credenziali filenet
	 * @param wobNumber   wob number
	 * @param idUtente    identificativo utente
	 * @param idUfficio   identificativo ufficio
	 * @param motivoVista motivo visto
	 * @return esito operazione
	 */
	EsitoOperazioneDTO vista(FilenetCredentialsDTO fcDTO, String wobNumber, Long idUtente, Long idUfficio, String motivoVista);

	/**
	 * Vista.
	 *
	 * @param fcDTO       credenziali filenet
	 * @param wobNumbers  collezione wob number
	 * @param idUtente    identificativo utente
	 * @param idUfficio   identificativo ufficio
	 * @param motivoVista motivo visto
	 * @return esito operazione
	 */
	Collection<EsitoOperazioneDTO> vista(FilenetCredentialsDTO fcDTO, Collection<String> wobNumbers, Long idUtente, Long idUfficio, String motivoVista);

	/**
	 * Sigla.
	 *
	 * @param fcDTO       credenziali filenet
	 * @param wobNumber   wob number
	 * @param utente      Utente
	 * @param motivoSigla motivo
	 * @return esito operazione
	 */
	EsitoOperazioneDTO sigla(FilenetCredentialsDTO fcDTO, String wobNumber, UtenteDTO utente, String motivoSigla);

	/**
	 * Sigla.
	 *
	 * @param fcDTO       credenziali filenet
	 * @param wobNumbers  collezione wob number
	 * @param utente      Utente
	 * @param motivoSigla motivo
	 * @return esito operazione
	 */
	Collection<EsitoOperazioneDTO> sigla(FilenetCredentialsDTO fcDTO, Collection<String> wobNumbers, UtenteDTO utente, String motivoSigla);

	/**
	 * Response "Rifiuta" massiva.
	 * 
	 * @param utente
	 * @param wobNumbers
	 * @param motivoRifiuto
	 * @param responseEnum
	 * @param isOneKindOfCorriere
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> rifiuta(UtenteDTO utente, Collection<String> wobNumbers, String motivoRifiuto, ResponsesRedEnum responseEnum, boolean isOneKindOfCorriere);

	/**
	 * Response "Rifiuta".
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param motivoRifiuto
	 * @param responseEnum
	 * @param isOneKindOfCorriere
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO rifiuta(UtenteDTO utente, String wobNumber, String motivoRifiuto, ResponsesRedEnum responseEnum, boolean isOneKindOfCorriere);

	/**
	 * Response "Richiedi Sollecito".
	 * 
	 * @param wobNumber                  WOB Number del documento su cui si esegue
	 *                                   la response.
	 * @param documentTitle              Document Title del documento su cui si
	 *                                   esegue la response.
	 * @param documentTitleDocContributo Document Title del documento contributo
	 *                                   esterno per cui si vuole richiedere il
	 *                                   sollecito.
	 * @param oggettoDocContributo       Oggetto del documento contributo esterno
	 *                                   per cui si vuole richiedere il sollecito.
	 * @param mailDestinatari            Destinatari selezionati a cui si vuole
	 *                                   inviare il sollecito.
	 * @param utente                     Utente che esegue l'operazione
	 * @return
	 */
	EsitoOperazioneDTO richiediSollecito(String wobNumber, String documentTitle, String documentTitleDocContributo, String oggettoDocContributo,
			Collection<String> mailDestinatari, UtenteDTO utente);

	/**
	 * Response "Recall".
	 * 
	 * @param utente.
	 * @param wobNumber.
	 * @param motivoRecall
	 * @return
	 */
	EsitoOperazioneDTO recall(UtenteDTO utente, String wobNumber, String motivoRecall);

	/**
	 * Response "StornaACorriere".
	 * 
	 * @param fcDTO.
	 * @param wobNumber.
	 * @param stornaACorriere
	 * @param idAoo
	 * @return
	 */
	EsitoOperazioneDTO stornaACorriere(FilenetCredentialsDTO fcDTO, String wobNumber, UtenteDTO utente, String stornaACorriere);

	/**
	 * Response "StornaACorriere" massiva.
	 * 
	 * @param fcDTO
	 * @param wobNumbers
	 * @param utente
	 * @param stornaACorriere
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> stornaACorriere(FilenetCredentialsDTO fcDTO, Collection<String> wobNumbers, UtenteDTO utente, String stornaACorriere);

}
