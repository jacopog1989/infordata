package it.ibm.red.business.service.ws.auth;

import java.sql.Connection;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.business.service.concrete.AbstractService;
import it.ibm.red.business.service.ws.ISecurityWsSRV;

/**
 * Classe authorization.
 */
@Aspect
@Service
public class Authorization extends AbstractService {
	
	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = -2227447521839282881L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Authorization.class.getName());
	
	/**
	 * Security ws client.
	 */
	@Autowired
	private ISecurityWsSRV securityWsClient;

	/**
	 * @param proceedingJoinPoint
	 * @param documentServiceAuthorizable
	 * @return
	 * @throws Throwable 
	 */
	@Around("@annotation(documentServiceAuthorizable)")
	public Object authorizeClient(final ProceedingJoinPoint proceedingJoinPoint, final DocumentServiceAuthorizable documentServiceAuthorizable) throws Throwable {
		
		LOGGER.info("authorizeClient (" + proceedingJoinPoint.getSignature().getName() + ") before");
		Object value = null;
		Connection con = null;
		
		try {
			
			con = setupConnection(getDataSource().getConnection(), false);
			
			final Object[] args = proceedingJoinPoint.getArgs();
			if (args == null || args.length == 0 || !(args[0] instanceof RedWsClient)) {
				LOGGER.error("Informazioni del client assenti sul backend (il primo parametro deve essere RedWsClient)");
				throw new RedException("Informazioni del client assenti sul backend (il primo parametro deve essere RedWsClient)");
			}
			
			final RedWsClient client = (RedWsClient) args[0];
			
			securityWsClient.autorizzaClient(client, documentServiceAuthorizable.servizio(), con);
			
			if (client.isAuthorized()) {
			
				value = proceedingJoinPoint.proceed();
			
			} else {
				
				LOGGER.error(
						"Si è verificato un errore durante il recupero del client WS con ID: " + client.getIdClient() + " [" + client.getAuthorizationErrorMessage() + "]");
				throw new RedException("Errore durante il recupero del client WS con ID: " + client.getIdClient() + " [" + client.getAuthorizationErrorMessage() + "]");
				
			}
			
		} catch (final RedException e) {
			
			throw e;
			
		} catch (final Exception e) {
			
			throw new RedException("Errore generico in fase di autorizzazione del client", e);
			
		} finally {
			
			closeConnection(con);
			
		}
		LOGGER.info("authorizeClient (" + proceedingJoinPoint.getSignature().getName() + ") after");
		return value;
	}

	
}
