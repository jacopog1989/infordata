/**
 * 
 */
package it.ibm.red.business.dto;

/**
 * @author VINGENITO
 *
 */
public class TracciaDocUscitaDTO {

	/**
	 * Nodo.
	 */
	private Long idNodo;
	
	/**
	 * Utente.
	 */
	private Long idUtente;
	
	/**
	 * Ruolo.
	 */
	private Long idRuolo;
	
	/**
	 * Aoo.
	 */
	private Long idAoo;
	
	/**
	 * Costruttore.
	 * @param inIdNodo
	 * @param inIdUtente
	 * @param inIdRuolo
	 * @param inIdAoo
	 */
	public TracciaDocUscitaDTO(final Long inIdNodo, final Long inIdUtente, final Long inIdRuolo, final Long inIdAoo) {
		idNodo = inIdNodo;
		idUtente = inIdUtente;
		idRuolo = inIdRuolo;
		idAoo = inIdAoo;
	}
	
	/**
	 * Restituisce l'id del nodo.
	 * @return id nodo
	 */
	public Long getIdNodo() {
		return idNodo;
	}
	
	/**
	 * Imposta l'id del nodo.
	 * @param idNodo
	 */
	public void setIdNodo(final Long idNodo) {
		this.idNodo = idNodo;
	}
	
	/**
	 * Restituisce l'id utente.
	 * @return id utente
	 */
	public Long getIdUtente() {
		return idUtente;
	}
	
	/**
	 * Imposta l'id dell'utente.
	 * @param idUtente
	 */
	public void setIdUtente(final Long idUtente) {
		this.idUtente = idUtente;
	}
	
	/**
	 * Restitusce l'id del ruolo.
	 * @return id ruolo
	 */
	public Long getIdRuolo() {
		return idRuolo;
	}
	
	/**
	 * Imposta l'id del ruolo.
	 * @param idRuolo
	 */
	public void setIdRuolo(final Long idRuolo) {
		this.idRuolo = idRuolo;
	}
	
	/**
	 * Restituisce l'id dell'AOO.
	 * @return id AOO
	 */
	public Long getIdAoo() {
		return idAoo;
	}
	
	/**
	 * Imposta l'id dell'AOO.
	 * @param idAoo
	 */
	public void setIdAoo(final Long idAoo) {
		this.idAoo = idAoo;
	}
}

