package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * DTO che individua gli assegnatari selezionati da un organigramma e per ciascuna assegnazione indica se il WF relativo è attivo o meno.
 */
public class AssegnatarioOrganigrammaWFAttivoDTO extends AssegnatarioOrganigrammaDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8410736447133025666L;
	
	/**
	 * Wob number.
	 */
	protected String wobNumber;
	
	/**
	 * Flag wf attivo.
	 */
	protected boolean wfAttivo;

	/**
	 * Costruttore di classe.
	 * @param idNodo
	 * @param inIdUtente
	 * @param wobNumber
	 * @param wfAttivo
	 */
	public AssegnatarioOrganigrammaWFAttivoDTO(final Long idNodo, final Long inIdUtente, final String wobNumber, final boolean wfAttivo) {
		super(idNodo, inIdUtente);
		setWobNumber(wobNumber);
		setWfAttivo(wfAttivo);
	}

	/**
	 * Restituisce il wobnumber associato.
	 * @return wobNumber
	 */
	public String getWobNumber() {
		return wobNumber;
	}

	/**
	 * Imposta il wob number.
	 * @param wobNumber
	 */
	public void setWobNumber(final String wobNumber) {
		this.wobNumber = wobNumber;
	}

	/**
	 * Restituisce il flag associato allo stato del workflow.
	 * @return true se attivo, false altrimenti
	 */
	public boolean isWfAttivo() {
		return wfAttivo;
	}

	/**
	 * Imposta il flag associato allo stato del workflow, se attivo o disattivo.
	 * @param wfAttivo
	 */
	public void setWfAttivo(final boolean wfAttivo) {
		this.wfAttivo = wfAttivo;
	}

	/**
	 * @see it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO#hashCode().
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((wobNumber == null) ? 0 : wobNumber.hashCode());
		return result;
	}

	/**
	 * @see it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final AssegnatarioOrganigrammaWFAttivoDTO other = (AssegnatarioOrganigrammaWFAttivoDTO) obj;
		if (wobNumber == null) {
			if (other.wobNumber != null) {
				return false;
			}
		} else if (!wobNumber.equals(other.wobNumber)) {
			return false;
		}
		return true;
	}
	
	
	
}
