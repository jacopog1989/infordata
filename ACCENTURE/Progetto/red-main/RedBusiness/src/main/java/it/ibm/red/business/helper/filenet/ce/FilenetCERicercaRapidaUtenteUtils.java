package it.ibm.red.business.helper.filenet.ce;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import it.ibm.red.business.constants.Constants.Ricerca;
import it.ibm.red.business.enums.CampoRicercaEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * Utility di ricerca Filenet CE.
 */
public final class FilenetCERicercaRapidaUtenteUtils  {

	/**
	 * Pattern.
	 */
	private static final Pattern NUMERIC_PATTERN = Pattern.compile("\\d*");
	
	/**
	 * Costruttore non usare.
	 */
	private FilenetCERicercaRapidaUtenteUtils() {
		throw new UnsupportedOperationException("Le classi statiche non possono essere istanziate"); 
	}

	/**
	 * Trasforma la stringa inserita dall'utente in un array di stringhe dette token
	 * 
	 * Le stringhe sono ripulite dai caratteri speciali indicanti gli spazi e trimmmate.
	 * 
	 * @param key
	 * 	Stringa inserta dall'utente
	 * @param type
	 * 	Se corrisponde a ESATTA ritorna una lista con un solo elemento corrispondente alla riga ripulita dei caratteri speciali
	 * @return
	 */
	public static List<String> tokenizeFreeText(final String key, final RicercaGenericaTypeEnum type) {
		//String keyLowerCase = key.toLowerCase(); // Gestione case insensitive
		String cleanKey = it.ibm.red.business.utils.StringUtils.deleteSpecialCharacterForSpace(key).trim();
		List<String> valoriDaRicercareList = new ArrayList<>();
		if (org.apache.commons.lang3.StringUtils.isNotBlank(cleanKey)) {
			
			if (type.equals(RicercaGenericaTypeEnum.ESATTA)) {
				valoriDaRicercareList.add(cleanKey);
			} else {
				valoriDaRicercareList.addAll(
					Arrays.asList(cleanKey.split(" "))
				);
			}
			
			valoriDaRicercareList = valoriDaRicercareList.stream()
				.filter(value->org.apache.commons.lang3.StringUtils.isNotBlank(value))
				.map(value->value.trim())
				.collect(Collectors.toList());
		}
		return valoriDaRicercareList;
	}
	
	/**
	 * 
	 * @param valoriDaRicercareList Una lista di token presenti nell'input
	 * @param operatore indica se ciascun token deve essere analizzato in OR o in AND
	 * @param anno Se l'utente non valorizza l'anno, i token devono includere una ricerca nell'anno
	 * @param fullText Flag che abilita la ricerca fullText
	 * @param campoRicerca  Indica quale/i metadato/i deve matchare con valoriDaRicercareList
	 * @param searchSQL Nel caso la ricerca full text sia abilitata impostiamo un massimale ai valori ricercati
	 * @return
	 */
	public static String generateWhereConditionFromFreeText(final List<String> valoriDaRicercareList, final String operatore, final Integer anno, 
			final boolean fullText, final CampoRicercaEnum campoRicerca, final PropertiesProvider pp) {
		String freeTextCondition = ""; // IN and
		
		if (fullText && !valoriDaRicercareList.isEmpty()) {
			// Ricerca del testo nel contents
			freeTextCondition = generateFullTextWhereCondition(valoriDaRicercareList, operatore);
		} else {			
			List<String> freeTextContitionList = valoriDaRicercareList.stream()
					.map(a-> generateDocumentConditionForFreeTextSearch(a, anno, campoRicerca, pp))
					.filter(condition -> org.apache.commons.lang3.StringUtils.isNotBlank(condition))
					.collect(Collectors.toList());		
			freeTextCondition = String.join(" ) " + operatore + " ( ", freeTextContitionList);
			//chiudo le parentesi se ho scritto qualcosa
			// Racchiudo anche tutti gli Or tra parentesi più grandi E.G. ( (condition) or (condition) or (condition) )
			// Se c'è un solo elemento sarà racchiuso tra due parentesi... pazienza
			if (!freeTextContitionList.isEmpty()) {
				freeTextCondition = org.apache.commons.lang3.StringUtils.join("( ", freeTextCondition, " )");
			}
		}
		if (org.apache.commons.lang3.StringUtils.isNotBlank(freeTextCondition)) {			
			freeTextCondition = org.apache.commons.lang3.StringUtils.join("( ", freeTextCondition, ") ");
		}
		
		return freeTextCondition;
	}

	/**
	 * Qualora non sia possibile generare la condizione genera una condizione vuota
	 * 
	 * @param valoriDaRicercareList
	 * @param operatore
	 * @return
	 */
	public static String generateFullTextWhereCondition(final List<String> valoriDaRicercareList, final String operatore) {
		List<String> valoriModificati = valoriDaRicercareList.stream()
			.map(a -> it.ibm.red.business.utils.StringUtils.duplicaApici(a))
			.collect(Collectors.toList());			
		String fulltext = String.join(" " + operatore + " ", valoriModificati);
		
		return org.apache.commons.lang3.StringUtils.join(" CONTAINS(d.*, '", fulltext, "') ");
	}

	/**
	 * Restituisce lo statement FROM per una ricerca generica.
	 * @param fullText
	 * @param valoriDaRicercareList
	 * @param pp
	 * @return statement FROM
	 */
	public static String generateFromRicercaGenerica(final boolean fullText, final List<String> valoriDaRicercareList, final PropertiesProvider pp) {
		String fromStatement = " FROM " + pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY) + " d ";
		
		if (fullText && !valoriDaRicercareList.isEmpty()) {
			fromStatement = org.apache.commons.lang3.StringUtils.join(fromStatement, " INNER JOIN ContentSearch c ON d.This = c.QueriedObject ");
		}
		
		return fromStatement;
	}

	/**
	 * Restituisce la WHERE condition generata.
	 * @param valoreDaricercare
	 * @param anno
	 * @param campoRicerca
	 * @param pp
	 * @return WHERE condition
	 */
	public static String generateDocumentConditionForFreeTextSearch(final String valoreDaricercare, final Integer anno, final CampoRicercaEnum campoRicerca, 
			final PropertiesProvider pp) {
		StringBuilder whereConditionForKeySQL = new StringBuilder();
		
		if (CampoRicercaEnum.ALL.equals(campoRicerca)) {
			
			if (NUMERIC_PATTERN.matcher(valoreDaricercare.trim()).matches()) {
				whereConditionForKeySQL.append("d." + pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY) + " = " + valoreDaricercare);
				whereConditionForKeySQL.append(" OR ");
				whereConditionForKeySQL.append(" d." + pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY) + " = " + valoreDaricercare);
				whereConditionForKeySQL.append(" OR ");
				
				if (anno == null) {
					whereConditionForKeySQL.append(generateYearCondition(valoreDaricercare, pp));
					whereConditionForKeySQL.append(" OR ");
				}
			}
			
			whereConditionForKeySQL.append("d." + pp.getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY) + " = '" + valoreDaricercare + "'");
			whereConditionForKeySQL.append(" OR ");
			whereConditionForKeySQL.append("d." + pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY) + " LIKE '%" + valoreDaricercare + "%'");
			
		} else {
			whereConditionForKeySQL.append("d." + pp.getParameterByKey(campoRicerca.getChiaveMetadato()));
			
			if (Ricerca.TIPO_CAMPO_RICERCA_NUMERICO.equals(campoRicerca.getTipoRicerca())) {
				whereConditionForKeySQL.append(" = " + valoreDaricercare);
				
			} else if (Ricerca.TIPO_CAMPO_RICERCA_TESTO_LIKE.equals(campoRicerca.getTipoRicerca())) {
				whereConditionForKeySQL.append(" LIKE '%" + valoreDaricercare + "%'");
				
			} else if (Ricerca.TIPO_CAMPO_RICERCA_TESTO_ESATTO.equals(campoRicerca.getTipoRicerca())) {
				whereConditionForKeySQL.append(" = '" + valoreDaricercare + "'");
			}
		}
		
		return whereConditionForKeySQL.toString();
	}

	/**
	 * Genera la parte di query che gestisce la condizione sull'anno.
	 * @param anno
	 * @param pp
	 * @return OR condition
	 */
	public static String generateYearCondition(final String anno, final PropertiesProvider pp) {	
		String annoDocumento = org.apache.commons.lang3.StringUtils.join(" ", " d." + pp.getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY), " = ", anno, " ");
		String annoProtocollo = org.apache.commons.lang3.StringUtils.join(" ", " d." + pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY), " = ", anno, " ");
		return org.apache.commons.lang3.StringUtils.join(" ( ", annoDocumento, " OR ", annoProtocollo, " ) ");
	}

	/**
	 * Genera la parte di query che gestisce la condizione sull'anno del fascicolo.
	 * @param anno
	 * @param pp
	 * @return OR condition
	 */
	public static String generateYearFascicoloCondition(final String anno, final PropertiesProvider pp) {
		String controlloAnno = "";
		String dataCreazione = pp.getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY);
		controlloAnno = org.apache.commons.lang3.StringUtils.join(" ( ", dataCreazione, " >= ", anno, "0101T000000Z AND ", dataCreazione, " <= ", anno, "1231T235959Z ) ");
		return controlloAnno;
	}
	
	/**
	 * Genera la WHERE condition per la ricerca dei fascicoli.
	 * @param valoreDaricercare
	 * @param anno
	 * @param pp
	 * @return WHERE condition
	 */
	public static String generateFascicoliConditionForFreeTextSearch(final String valoreDaricercare, final PropertiesProvider pp) {
		String campoIndiceClassificazione = pp.getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY); //titolario
		String campoOggetto = pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY); //oggetto
		
		StringBuilder whereConditionForKeySQL = new StringBuilder();
		whereConditionForKeySQL.append(pp.getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY) + " = '" + valoreDaricercare + "' ");
		whereConditionForKeySQL.append(" OR ");
		whereConditionForKeySQL.append("( " + campoOggetto + ") LIKE '%" + valoreDaricercare + "%'");
		whereConditionForKeySQL.append(" OR ");
		whereConditionForKeySQL.append("( " + campoIndiceClassificazione + ") = '" + valoreDaricercare + "' ");
		
		return whereConditionForKeySQL.toString();
	}
	
}
