package it.ibm.red.business.helper.email;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.exception.EmailException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.utils.StringUtils;

/**
 * Helper per mail.
 */
public class EmailHelper {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(EmailHelper.class.getName());

	/**
	 * Caller id.
	 */
	private String callId;

	/**
	 * Casella postale.
	 */
	private CasellaPostaDTO casellaPostale;

	/**
	 * Messaggio.
	 */
	private MessaggioEmailDTO messaggio;

	/**
	 * Flag autorizzato.
	 */
	private boolean auth;

	/**
	 * Costruttore.
	 * 
	 * @param callId         - id della call
	 * @param casellaPostale - casella di posta
	 * @param messaggio      - messaggio della mail
	 */
	public EmailHelper(final String callId, final CasellaPostaDTO casellaPostale, final MessaggioEmailDTO messaggio) {
		this.callId = callId;
		this.casellaPostale = casellaPostale;
		this.messaggio = messaggio;
		auth = casellaPostale.getUsernameInvio() != null && !"".equals(casellaPostale.getUsernameInvio().trim()) && casellaPostale.getPasswordInvio() != null
				&& !"".equals(casellaPostale.getPasswordInvio().trim());
	}

	/**
	 * Costruttore.
	 */
	public EmailHelper() {

	}

	/**
	 * Invia la mail.
	 * 
	 * @return true se la mail è stata inviata, false in caso contrario
	 */
	public boolean sendEmail() {
		try {
			final java.util.Properties props = new java.util.Properties();
			boolean inviata = false;
			final boolean isPEC = messaggio.getTipologiaMessaggioId() == Constants.TipologiaMessaggio.TIPO_MSG_PEC;
			final boolean isFax = messaggio.getTipologiaMessaggioId() == Constants.TipologiaMessaggio.TIPO_MSG_FAX;

			if ((casellaPostale.getProxyHost() != null && !"".equals(casellaPostale.getProxyHost().trim()))
					&& (casellaPostale.getProxyPort() != null && !"".equals(casellaPostale.getProxyPort().trim()))) {
				props.put("proxySet", "true");
				props.put("https.proxyHost", casellaPostale.getProxyHost());
				props.put("https.proxyPort", casellaPostale.getProxyPort());
			}

			if (isPEC || isFax) {

				if (casellaPostale.getServerOutAutenticazione() > 0) {
					props.put("mail.smtps.host", casellaPostale.getHostInvio());
					props.put("mail.smtps.port", casellaPostale.getPortInvio());
					props.put("mail.smtps.starttls.enable", "true");
					props.put("mail.smtps.timeout", 180000);
					props.put("mail.smtps.connectiontimeout", 180000);
					props.put("mail.smtps.auth", "true");
					props.put("mail.transport.protocol", "smtps");
					if (casellaPostale.isAbilitaTLS12()) {
						props.put("mail.smtps.ssl.protocols", "TLSv1.2");
					}
				} else {
					props.put("mail.smtp.host", casellaPostale.getHostInvio());
					props.put("mail.smtp.port", casellaPostale.getPortInvio());
					props.put("mail.smtp.starttls.enable", "true");
					props.put("mail.smtp.timeout", 180000);
					props.put("mail.smtp.connectiontimeout", 180000);
					props.put("mail.smtp.auth", "true");
					if (casellaPostale.isAbilitaTLS12()) {
						props.put("mail.smtp.ssl.protocols", "TLSv1.2");
					}
				}
			} else {
				props.put("mail.smtp.host", casellaPostale.getHostInvio());
				props.put("mail.smtp.port", casellaPostale.getPortInvio());
				if (auth) {
					props.put("mail.smtp.auth", "true");
				}

			}

			System.setProperty("mail.mime.encodefilename", "true");
			System.setProperty("mail.mime.encodeparameters", "false");
			System.setProperty("mail.mime.charset", StandardCharsets.UTF_8.name());

			inviata = sendMessage(props, isPEC);

			return inviata;
		} catch (final Exception e) {
			throw new EmailException(e);
		}
	}

	private boolean sendMessage(final Properties props, final boolean isPEC) {
		boolean mailInviata = false;
		Transport tr = null;
		LOGGER.info(callId + " Messaggio da inviare: " + messaggio);

		try {

			Session session = null;
			if (auth) {
				session = Session.getInstance(props, new javax.mail.Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(casellaPostale.getUsernameInvio(), casellaPostale.getPasswordInvio());
					}
				});
			} else {
				session = Session.getInstance(props);
			}

			final MimeMessage message = new MimeMessage(session);

			// header tipo ricevuta
			if (isPEC && messaggio.getTipoRicevuta() != null) {
				message.setHeader("X-TipoRicevuta", messaggio.getTipoRicevuta());
			}

			// mittente
			message.setFrom(new InternetAddress(messaggio.getMittente()));

			// destinatari
			String destinatari;
			if (messaggio.getDestinatario() != null && !messaggio.getIsSpedizioneUnica()) { // Gestione spedizione unica
				destinatari = messaggio.getDestinatario().toLowerCase();
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destinatari));
			} else {
				destinatari = messaggio.getDestinatari();
				if (messaggio.getDestinatari().contains(";")) {
					destinatari = messaggio.getDestinatari().replace(";", ",");
				}
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destinatari.toLowerCase()));

				// destinatari x conoscenza (opzionali)
				String toCC = messaggio.getDestinatariCC();
				if (toCC != null && toCC.length() > 0) {
					if (toCC.contains(";")) {
						toCC = toCC.replace(";", ",");
					}
					message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(toCC.toLowerCase()));
				}
			}

			// oggetto
			String oggetto = messaggio.getOggetto();
			if (!StringUtils.isNullOrEmpty(oggetto)) { // bonifica degli eventuali caratteri "a capo" presenti nell'oggetto
				oggetto = oggetto.replaceAll("\\r|\\n|\\r\\n", " ");
			}
			message.setSubject(oggetto, StandardCharsets.UTF_8.name());

			// sent date
			message.setSentDate(new Date());

			MimeBodyPart[] bodyParts = null;
			if (messaggio.getAllegatiMail() != null && !messaggio.getAllegatiMail().isEmpty()) {
				bodyParts = new MimeBodyPart[messaggio.getAllegatiMail().size() + 1];
			} else {
				bodyParts = new MimeBodyPart[1];
			}

			// aggiunge il testo (ultimo spazio necessario affinchè non si perda l'ultimo carattere)
		    bodyParts[0] = new MimeBodyPart();
		    if(messaggio.getTestoHTML()==null) {
			    String text = messaggio.getTesto();
			    text = text != null && text.length() > 0 ? text + " " : " ";
			    bodyParts[0].setText(text, StandardCharsets.UTF_8.name());
		    }else {
		    	String text = messaggio.getTestoHTML();
		    	bodyParts[0].setContent(text,"text/html");
		    }

			if (messaggio.getAllegatiMail() != null && !messaggio.getAllegatiMail().isEmpty()) {
				// aggiunge gli allegati
				for (int i = 0; i < messaggio.getAllegatiMail().size(); i++) {
					bodyParts[i + 1] = new MimeBodyPart();
					final DataSource source = new ByteArrayDataSource(messaggio.getAllegatiMail().get(i).getContent(), messaggio.getAllegatiMail().get(i).getMimeType());
					bodyParts[i + 1].setDataHandler(new DataHandler(source));
					bodyParts[i + 1].setFileName(messaggio.getAllegatiMail().get(i).getNomeFile());
				}
			}

			// creazione dell'oggetto multipart
			final Multipart multipart = new MimeMultipart();
			for (int i = 0; i < bodyParts.length; i++) {
				multipart.addBodyPart(bodyParts[i]);
			}

			// imposta come contenuto del messaggio l'oggetto multipart
			message.setContent(multipart);
			message.saveChanges();

			final String msgID = message.getHeader("Message-ID")[0];
			messaggio.setMsgID(msgID);

			if (casellaPostale.getServerOutAutenticazione() > 0) {
				tr = session.getTransport("smtps");
			} else {
				tr = session.getTransport("smtp");
			}

			LOGGER.info(callId + "Connessione server mail in corso...");
			if (auth) {
				tr.connect(casellaPostale.getHostInvio(), casellaPostale.getUsernameInvio(), casellaPostale.getPasswordInvio());
			}

			LOGGER.info(callId + "Connessione effettuata");

			if (auth) {
				tr.sendMessage(message, message.getAllRecipients());
			} else {
				Transport.send(message);
			}
			mailInviata = true;

			LOGGER.info(callId + "Messaggio inviato correttamente.");

		} catch (final Exception e) {
			throw new EmailException("Errore invio della mail - [" + e.getMessage() + "]", e);
		} finally {
			try {
				if (tr != null) {
					tr.close();
				}
			} catch (final Exception e) {
				LOGGER.error(callId + "Errore in fase di chiusura del Transport", e);
			}
		}

		return mailInviata;

	}

	/**
	 * Restituisce l'email pec o peo a seconda della presenza in
	 * destinatariTO/destinatariCC della mail spedizione
	 * 
	 * @param contatto
	 * @param destinatariTo indirizzi email separati da ;
	 * @param destinatariCC indirizzi email separati da ;
	 * @return [peo, pec]
	 */
	public static String[] getSelectedMail(final Contatto contatto, final String destinatariTo, final String destinatariCC) {
		final String[] email = new String[2];
		boolean trovato = false;

		// Se il contatto ha valorizzata la PEC
		if (!StringUtils.isNullOrEmpty(contatto.getMailPec())) {

			// Valorizza il destinatario con la pec
			email[1] = contatto.getMailPec();

			// Se è fra quelle scelte, ho finito
			if ((!StringUtils.isNullOrEmpty(destinatariTo) && destinatariTo.indexOf(contatto.getMailPec()) != -1)
					|| (!StringUtils.isNullOrEmpty(destinatariCC) && destinatariCC.indexOf(contatto.getMailPec()) != -1)) {
				trovato = true;
			}
		}

		// Se non ho trovato la PEC fra la selezione in maschera e il contatto ha la PEO
		// valorizzata
		if ((!trovato && !StringUtils.isNullOrEmpty(contatto.getMail()))
				// Se la PEO è fra quelle scelte o se il contatto non ha valorizzata la PEC, ho
				// finito
				&& ((!StringUtils.isNullOrEmpty(destinatariTo) && destinatariTo.indexOf(contatto.getMail()) != -1)
						|| (!StringUtils.isNullOrEmpty(destinatariCC) && destinatariCC.indexOf(contatto.getMail()) != -1) || email[1] == null)) {

			email[0] = contatto.getMail();
			email[1] = null;
		}

		// Se non sono passato per la PEO, ho il default della PEC

		return email;
	}

}
