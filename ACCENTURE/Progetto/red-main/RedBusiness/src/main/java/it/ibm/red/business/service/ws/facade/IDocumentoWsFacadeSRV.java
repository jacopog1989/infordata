package it.ibm.red.business.service.ws.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.ConservazioneWsDTO;
import it.ibm.red.business.dto.DocumentoWsDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.PropertyTemplateDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.webservice.model.documentservice.types.messages.AggiungiAllacciAsyncType;
import it.ibm.red.webservice.model.documentservice.types.messages.AssociateDocumentoProtocolloToAsyncNpsType;
import it.ibm.red.webservice.model.documentservice.types.messages.GestisciDestinatariInterniType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedConservaDocumentoType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedElencoPropertiesType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedElencoVersioniDocumentoType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedGetVersioneDocumentoType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedModificaMetadatiType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedModificaSecurityType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedProtocolloNPSSyncType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedTrasformazionePDFType;
import it.ibm.red.webservice.model.documentservice.types.messages.SpedisciProtocolloUscitaAsyncType;
import it.ibm.red.webservice.model.documentservice.types.messages.TrasformazionePDFType;
import it.ibm.red.webservice.model.documentservice.types.messages.UploadDocToAsyncNpsType;


/**
 * The Interface IDocumentoWsFacadeSRV.
 *
 * @author m.crescentini
 * 
 *         Facade per il servizio che trasforma i documenti RED in DTO per il web service.
 */
public interface IDocumentoWsFacadeSRV extends Serializable {
	
	/**
	 * @param client
	 * @param request
	 * @return
	 */
	List<DocumentoWsDTO> getElencoVersioniDocumento(RedWsClient client, RedElencoVersioniDocumentoType request);

	
	/**
	 * @param client
	 * @param request
	 * @return
	 */
	DocumentoWsDTO getVersioneDocumento(RedWsClient client, RedGetVersioneDocumentoType request);
	
	/**
	 * @param client
	 * @param request
	 */
	List<PropertyTemplateDTO> getElencoProperies(RedWsClient client, RedElencoPropertiesType request);

	/**
	 * @param client
	 * @param request
	 */
	void modificaMetadati(RedWsClient client, RedModificaMetadatiType request);


	/**
	 * @param client
	 * @param request
	 * @return
	 */
	DocumentoWsDTO redTrasformazionePDF(RedWsClient client, RedTrasformazionePDFType request);
	
	/**
	 * @param client
	 * @param request
	 * @return
	 */
	FileDTO trasformazionePDF(RedWsClient client, TrasformazionePDFType request);
	
	/**
	 * @param client
	 * @param request
	 */
	ConservazioneWsDTO conservaDocumento(RedWsClient client, RedConservaDocumentoType request);
	
	/**
	 * @param client
	 * @param request
	 */
	void modificaSecurity(RedWsClient client, RedModificaSecurityType request);
	
	/**
	 * Richiede protocollo NPS in modo sincrono.
	 * @param client
	 * @param request
	 * @return protocollo NPS
	 */
	ProtocolloNpsDTO richiediProtocolloNPSync(RedWsClient client, RedProtocolloNPSSyncType request);
 
	/**
	 * Metodo per l'upload del documento.
	 * @param client
	 * @param request
	 * @return
	 */
	int uploadDocToAsyncNps(RedWsClient client, UploadDocToAsyncNpsType request);
 
	/**
	 * Aggiunge allacci in modo asincrono.
	 * @param client
	 * @param request
	 * @return true o false
	 */
	boolean aggiungiAllacciAsync(RedWsClient client, AggiungiAllacciAsyncType request);
 
	/**
	 * Spedisce il protocollo in uscita in modo asincrono.
	 * @param client
	 * @param request
	 * @return true o false
	 */
	boolean spedisciProtocolloUscitaAsync(RedWsClient client, SpedisciProtocolloUscitaAsyncType request);

	/**
	 * Metodo per l'associazione del documento.
	 * @param client
	 * @param request
	 * @return true o false
	 */
	boolean associateDocumentoProtocolloToAsyncNps(RedWsClient client, AssociateDocumentoProtocolloToAsyncNpsType request);

	/**
	 * Gestisce i destinatari interni.
	 * @param client
	 * @param request
	 * @return true o false
	 */
	boolean gestisciDestinatariInterni(RedWsClient client, GestisciDestinatariInterniType request);

	
}