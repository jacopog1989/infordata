/**
 * 
 */
package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import it.ibm.red.business.dto.CanaleTrasmissioneDTO;
import it.ibm.red.business.dto.CategoriaEventoDTO;
import it.ibm.red.business.dto.DisabilitazioneNotificheUtenteDTO;
import it.ibm.red.business.dto.EventoSottoscrizioneDTO;
import it.ibm.red.business.dto.MailAddressDTO;
import it.ibm.red.business.service.concrete.NotificaSRV.MittenteAzione;
import it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV.InfoDocumenti;

/**
 * @author APerquoti
 *
 */
public interface ISottoscrizioniDAO extends Serializable {

	/**
	 * Registra una serie di dati attinenti alle sottoscrizioni di eventi
	 * predefiniti sulla base dati. La sottoscrizione avviene attraverso il pannello
	 * delle Sottoscrizioni dell'applicativo
	 * 
	 * @param conn : Connessione verso la base dati.
	 * @return
	 * @throws SQLException
	 */
	int insertSottoscrizione(Connection conn, int idEvento, Long idUfficio, Long idUtente, Long idRuolo, int tracciamento, int idCanaleNotifica) throws SQLException;

	/**
	 * Aggiorna il flag di tracciamento e il canale di notifica.
	 * 
	 * @param conn : Connessione verso la base dati.
	 * @return
	 * @throws SQLException
	 */
	int updateTracciamentoAndCanaleNotifica(Connection conn, int idEvento, Long idUfficio, Long idUtente, Long idRuolo, int tracciamento, int idCanaleNotifica)
			throws SQLException;

	/**
	 * Registra sulla base dati più eventi a cui l'utente si è sottoscritto
	 * attraverso il pannello delle Sottoscrizioni dell'applicativo
	 * 
	 * @param conn : Connessione verso la base dati.
	 * @return
	 * @throws SQLException
	 */
	int insertListSottoscrizioni(Connection conn, List<EventoSottoscrizioneDTO> listaSottoscrizioni, Long idNodo, Long idUtente, Long idRuolo, int tracciamento,
			int idCanaleNotifica) throws SQLException;

	/**
	 * Restituisce la lista delle sottoscrizioni registrate dall'utente.
	 * 
	 * @param conn     : Connessione verso la base dati.
	 * @param idUtente : Codice identificativo dell'utente.
	 * @return
	 * @throws SQLException
	 */
	List<EventoSottoscrizioneDTO> getSottoscrizioniListByUser(Connection conn, Long idUtente, Long idUfficio, Long idRuolo) throws SQLException;

	/**
	 * Restituisce gli eventi associati ad un determinato utente e ad un determinato
	 * evento.
	 * 
	 * @param conn
	 * @param idUtente
	 * @param idNodo
	 * @param idRuolo
	 * @param idEvento
	 * @return
	 * @throws SQLException
	 */
	Integer getIdSottoscrizione(Connection conn, Long idUtente, Long idNodo, Long idRuolo, int idEvento) throws SQLException;

	/**
	 * Gestisce la fase di disinscrizione ad un evento da parte dell'utente. Si
	 * procede con la <code>REMOVE</code> rimuovendo i riferimenti dalla base di
	 * dati.
	 * 
	 * @param conn     : Connessione verso la base dati.
	 * @param idUtente : Codice identificativo dell'utente.
	 * @param idNodo   : Codice identificativo del nodo.
	 * @return
	 * @throws SQLException
	 */
	void rimuoviSottoscrizioni(Connection conn, int idutente, int idNodo, int idRuolo) throws SQLException;

	/**
	 * Rimuove le sottoscrizioni.
	 * 
	 * @param conn
	 * @param idUtente
	 * @param idUfficio
	 * @param idEvento
	 * @param idRuolo
	 * @return
	 * @throws SQLException
	 */
	int rimuoviSottoscrizione(Connection conn, int idUtente, int idUfficio, int idEvento, int idRuolo) throws SQLException;

	/**
	 * Ottiene la lista delle sottoscrizioni tramite l'id della categoria.
	 * 
	 * @param con
	 * @param idCategoria
	 * @param idAoo
	 * @return lista degli eventi di sottoscrizione
	 * @throws SQLException
	 */
	List<EventoSottoscrizioneDTO> getSottoscrizioniListByIdCategoria(Connection con, int idCategoria, int idAoo) throws SQLException;

	/**
	 * Ottiene la lista delle sottoscrizioni tramite l'id degli eventi.
	 * 
	 * @param con
	 * @param idEventi
	 * @param idAoo
	 * @return lista degli eventi di sottoscrizione
	 * @throws SQLException
	 */
	List<EventoSottoscrizioneDTO> getSottoscrizioniListByIdEventi(Connection con, Integer[] idEventi, int idAoo) throws SQLException;

	/**
	 * Ottiene la lista degli eventi tramite l'id dell'aoo.
	 * 
	 * @param con
	 * @param idAoo
	 * @param canaleDefault
	 * @return lista degli eventi di sottoscrizione
	 * @throws SQLException
	 */
	List<EventoSottoscrizioneDTO> getEventiListByIdAoo(Connection con, int idAoo, CanaleTrasmissioneDTO canaleDefault) throws SQLException;

	/**
	 * Rimuove il tracciamento del documento in input per tutti gli eventi
	 * sottoscritti dall'utente in input.
	 * 
	 * @param con
	 * @param idUfficio
	 * @param idUtente
	 * @param idRuolo
	 * @param idDocumento
	 * @throws SQLException
	 */
	int rimuoviDoc(Long idUtente, Long idUfficio, Long idRuolo, int idDocumento, Connection con) throws SQLException;

	/**
	 * Rimuove il tracciamento del documento in input per l'evento sottoscritto
	 * dall'utente in input.
	 * 
	 * @param con
	 * @param idUfficio
	 * @param idUtente
	 * @param idRuolo
	 * @param idDocumento
	 * @param idEvento
	 * @throws SQLException
	 */
	int rimuoviDoc(Long idUtente, Long idUfficio, Long idRuolo, int idDocumento, int idEvento, Connection con) throws SQLException;

	/**
	 * Aggiunge il tracciamento del documento in input per tutti gli eventi
	 * sottoscritti dall'utente in input
	 * 
	 * @param con
	 * @param idUfficio
	 * @param idUtente
	 * @param idRuolo
	 * @param idDocumento
	 * @throws SQLException
	 */
	int aggiungiDoc(Connection con, Long idUtente, Long idUfficio, Long idRuolo, int idDocumento) throws SQLException;

	/**
	 * Aggiunge il tracciamento del documento in input per tutti l'evento
	 * sottoscritto dall'utente in input.
	 * 
	 * @param con
	 * @param idUfficio
	 * @param idUtente
	 * @param idRuolo
	 * @param idDocumento
	 * @param idEvento
	 * @throws SQLException
	 */
	int aggiungiDoc(Connection con, Long idUtente, Long idUfficio, Long idRuolo, int idDocumento, int idEvento) throws SQLException;

	/**
	 * Indica se l'utente ha tracciato il documento.
	 * 
	 * @param con
	 * @param idUtente
	 * @param idUfficio
	 * @param idRuolo
	 * @param idDocumento
	 * @return
	 * @throws SQLException
	 */
	boolean isDocTracciato(Connection con, Long idUtente, Long idUfficio, Long idRuolo, int idDocumento) throws SQLException;

	/**
	 * Restituisce gli id dei documenti tracciati dall'utente
	 * 
	 * @param con
	 * @param idUtente
	 * @param idUfficio
	 * @param idRuolo
	 * @return
	 * @throws SQLException
	 */
	List<Integer> getDocumentiTracciati(Connection con, Long idUtente, Long idUfficio, Long idRuolo) throws SQLException;

	/**
	 * Restituisce gli id dei documenti tracciati dall'utente per l'evento in input.
	 * 
	 * @param con
	 * @param idUtente
	 * @param idUfficio
	 * @param idRuolo
	 * @return
	 * @throws SQLException
	 */
	List<Integer> getDocumentiTracciati(Connection connection, Long idUtente, Long idUfficio, Long idRuolo, int idEvento) throws SQLException;

	/**
	 * Recupera la categoria dell'evento in input con il template associato.
	 * 
	 * @param connection
	 * @param idEvento
	 * @return
	 * @throws SQLException
	 */
	CategoriaEventoDTO getTemplate(Connection connection, int idEvento) throws SQLException;

	/**
	 * Recupera il mittente dagli eventi del documento in input
	 * 
	 * @param connection
	 * @param idEvento
	 * @param idDocumento
	 * @param idNodoDestinatario
	 * @param idUtenteDestinatario
	 * @param mittente
	 * @throws SQLException
	 */
	void populateMittente(Connection connection, int idEvento, int idDocumento, int idNodoDestinatario, int idUtenteDestinatario, MittenteAzione mittente) throws SQLException;

	/**
	 * Recupera gli intervalli di disabilitazione di ricezione delle notifiche
	 * relativi all'utente.
	 * 
	 * @param connection
	 * @param idUtente
	 * @return
	 * @throws SQLException
	 */
	List<DisabilitazioneNotificheUtenteDTO> getDisabilitazioniByUser(Connection connection, String idUtente) throws SQLException;

	/**
	 * Recupera l'intervallo di disabilitazione di ricezione delle notifiche
	 * relativo all'utente ed al canale di trasmissione in input nella giornata
	 * odierna.
	 * 
	 * @param idUtente
	 * @param idCanaleTrasmissione
	 * @param con
	 * @return
	 */
	DisabilitazioneNotificheUtenteDTO getDisabilitazioniByUserAndCanaleTrasmissioneToday(String idUtente, int idCanaleTrasmissione, Connection con) throws SQLException;

	/**
	 * Recupera i documenti vistati/siglati dall'utente/ufficio in input.
	 * 
	 * @param conn
	 * @param idAoo
	 * @param idEvento
	 * @param idUtente
	 * @param idNodo
	 * @return
	 * @throws SQLException
	 */
	List<InfoDocumenti> getDocumentiPostSiglaVisto(Connection conn, int idAoo, int idEvento, int idUtente, int idNodo) throws SQLException;

	/**
	 * Elimina la sottoscrizione
	 * 
	 * @param sottoscrizioneToDelete
	 * @param con
	 */
	void delete(EventoSottoscrizioneDTO sottoscrizioneToDelete, Connection con);

	/**
	 * Elimina gli indirizzi email dell'utente.
	 * 
	 * @param idUtente
	 * @param con
	 */
	void deleteMails(Integer idUtente, Connection con);

	/**
	 * Inserisci la mail in input
	 * 
	 * @param mail
	 * @param con
	 */
	void insertMail(MailAddressDTO mail, Connection con);

	/**
	 * Recupera gli indirizzi email sul quale l'utente intende essere notificato
	 * 
	 * @param connection
	 * @param idUtente
	 * @return
	 * @throws SQLException
	 */
	List<MailAddressDTO> getMailsSottoscrizioni(Connection connection, int idUtente) throws SQLException;

	/**
	 * Recupera il numero di sottoscrizioni non lette dall'utente.
	 * 
	 * @param connection
	 * @param idUtente
	 * @param ultimiNgiorni
	 * @param idAOO
	 * @return
	 */
	int getCountSottoscrizioni(Connection connection, Long idUtente, int ultimiNgiorni, Long idAOO);

	/**
	 * Ottiene lo username relativo all'evento di rifiuto firma.
	 * 
	 * @param conn
	 * @param idDocumento
	 * @return username
	 * @throws Exception
	 */
	String getUsernameEventoRifiutoFirma(Connection conn, String idDocumento);

}
