package it.ibm.red.business.dto;

import java.util.Calendar;

import it.ibm.red.business.enums.TipoMessaggioPostaNpsIngressoEnum;
import it.ibm.red.business.enums.TipoNotificaInteropEnum;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraNotificaInteroperabilitaProtocolloType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.ProtIdentificatoreProtocolloType;

/**
 * Rappresenta un messaggio di notifica di interoperabilità in ingresso ricevuto tramite NPS.
 * 
 * @author m.crescentini
 *
 */
public class NotificaInteropPostaNpsDTO extends MessaggioPostaNpsDTO {

	/** 
	 * The Constant serialVersionUID. 
	 */
	private static final long serialVersionUID = 2983640695454694220L;
		
	/**
	 * Enum che identifica il tipo di notifica interoperabilita
	 */
	private TipoNotificaInteropEnum tipoNotificaInterop;
	
	/**
	 * DTO Protocollo Mittente
	 */
	private final ProtocolloNpsDTO protocolloMittente;
	 
	/**
	 * Costruttore per notifica di interoperabilità.
	 *
	 * @param richiesta the richiesta
	 */
	public NotificaInteropPostaNpsDTO(final RichiestaElaboraNotificaInteroperabilitaProtocolloType richiesta) {
		super(richiesta.getIdMessaggio(), richiesta.getIdDocumento(), TipoMessaggioPostaNpsIngressoEnum.NOTIFICA_INTEROPERABILITA,
				richiesta.getMessaggioRicevuto());
		
		// Si sovrascrivono la data e il corpo del messaggio con i dati relativi alla notifica di interoperabilità
		super.setData(richiesta.getTipoNotificaInteroperabile().getDataRicezione().toGregorianCalendar().toZonedDateTime().toLocalDateTime());
		super.setCorpo(richiesta.getTipoNotificaInteroperabile().getTestoMessaggio());
		
		// Tipo della notifica di interoperabilità
		if (richiesta.getTipoNotificaInteroperabile() != null) {
			this.tipoNotificaInterop = TipoNotificaInteropEnum.get(richiesta.getTipoNotificaInteroperabile().getTipoMessaggio());
		}
		
		// Protocollo -> START
		final ProtIdentificatoreProtocolloType inProtocollo = richiesta.getProtocollo().getProtocolloMittente();
		this.protocolloMittente = new ProtocolloNpsDTO();
		
		protocolloMittente.setIdProtocollo(inProtocollo.getIdProtocollo());
		protocolloMittente.setNumeroProtocollo(inProtocollo.getNumeroRegistrazione());
		protocolloMittente.setDataProtocollo(inProtocollo.getDataRegistrazione().toGregorianCalendar().getTime());
		final Calendar cal = Calendar.getInstance();
		cal.setTime(protocolloMittente.getDataProtocollo());
		protocolloMittente.setAnnoProtocollo(String.valueOf(cal.get(Calendar.YEAR)));
		// Protocollo -> END
	}


	/** 
	 *
	 * @return the tipoNotificaInterop
	 */
	public TipoNotificaInteropEnum getTipoNotificaInterop() {
		return tipoNotificaInterop;
	}


	/** 
	 *
	 * @return the protocolloMittente
	 */
	public ProtocolloNpsDTO getProtocolloMittente() {
		return protocolloMittente;
	}

}
