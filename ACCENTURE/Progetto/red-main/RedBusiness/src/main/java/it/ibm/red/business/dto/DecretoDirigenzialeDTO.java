package it.ibm.red.business.dto;

import java.util.Date;

/**
 * DTO che definisce un decreto dirigenziale.
 */
public class DecretoDirigenzialeDTO extends AbstractDTO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -27988808971959821L;

	/**
	 * Tipologia documento.
	 */
	private final String tipologiaDocumento;

	/**
	 * Oggetto.
	 */
	private final String oggetto;

	/**
	 * Numero protocollo.
	 */
	private final Integer numeroProtocollo;

	/**
	 * Numero documento.
	 */
	private final Integer numeroDocumento;

	/**
	 * Data protocollazione.
	 */
	private final Date dataProtocollazione;

	/**
	 * Data creazione.
	 */
	private final Date dataCreazione;

	/**
	 * Anno protocollo.
	 */
	private final Integer annoProtocollo;

	/**
	 * Anno documento.
	 */
	private Integer annoDocumento;

	/**
	 * Id workflow.
	 */
	private String wobNumber;

	/**
	 * Identificativo fascicolo procedimentale.
	 */
	private String idFascicoloProcedimentale;

	/**
	 * Costruttore che imposta tutte le variabili a null.
	 */
	public DecretoDirigenzialeDTO() {
		dataCreazione = null;
		numeroDocumento = null;
		oggetto = null;
		tipologiaDocumento = null;
		numeroProtocollo = null;
		annoProtocollo = null;
		wobNumber = null;
		dataProtocollazione = null;
	}

	/**
	 * Costruttore completo.
	 * 
	 * @param inNumeroDocumento
	 * @param inOggetto
	 * @param inTipologiaDocumento
	 * @param inNumeroProtocollo
	 * @param inAnnoProtocollo
	 * @param inDataCreazione
	 * @param inAnnoDocumento
	 * @param inDataProtocollazione
	 */
	public DecretoDirigenzialeDTO(final Integer inNumeroDocumento, final String inOggetto,
			final String inTipologiaDocumento, final Integer inNumeroProtocollo, final Integer inAnnoProtocollo,
			final Date inDataCreazione, final Integer inAnnoDocumento, final Date inDataProtocollazione) {
		super();
		dataCreazione = inDataCreazione;
		numeroDocumento = inNumeroDocumento;
		oggetto = inOggetto;
		tipologiaDocumento = inTipologiaDocumento;
		numeroProtocollo = inNumeroProtocollo;
		annoProtocollo = inAnnoProtocollo;
		annoDocumento = inAnnoDocumento;
		dataProtocollazione = inDataProtocollazione;
	}

	/**
	 * Restituisce la tipologia del documento del decreto dirigenziale.
	 * 
	 * @return tipologiaDocumento
	 */
	public String getTipologiaDocumento() {
		return tipologiaDocumento;
	}

	/**
	 * Restituisce l'oggetto del decreto dirigenziale.
	 * 
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Restituisce il numero di protocollo del decreto dirigenziale.
	 * 
	 * @return numeroProtocollo
	 */
	public Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Restituisce il numero documento del decreto dirigenziale.
	 * 
	 * @return numeroDocumento
	 */
	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * Restituisce la data di protocollazione del decreto dirigenziale.
	 * 
	 * @return dataProtocollazione
	 */
	public Date getDataProtocollazione() {
		return dataProtocollazione;
	}

	/**
	 * Restituisce la data di creazione del decreto dirigenziale.
	 * 
	 * @return dataCreazione
	 */
	public Date getDataCreazione() {
		return dataCreazione;
	}

	/**
	 * Restituisce l'anno del protocollo del decreto dirigenziale.
	 * 
	 * @return annoProtocollo
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * Restituisce l'anno del documento del decreto dirigenziale.
	 * 
	 * @return annoDocumento
	 */
	public Integer getAnnoDocumento() {
		return annoDocumento;
	}

	/**
	 * Restituisce l'id del fascicolo procedimentale associato al decreto
	 * dirigenziale.
	 * 
	 * @return idFascicoloProcedimentale
	 */
	public String getIdFascicoloProcedimentale() {
		return idFascicoloProcedimentale;
	}

	/**
	 * Restituisce il wob number del decreto dirigenziale.
	 * 
	 * @return wobNumber
	 */
	public String getWobNumber() {
		return wobNumber;
	}

	/**
	 * Imposta l'id del fascicolo procedimentale associato al decreto dirigenziale.
	 * 
	 * @param idFascicoloProcedimentale
	 */
	public void setIdFascicoloProcedimentale(final String idFascicoloProcedimentale) {
		this.idFascicoloProcedimentale = idFascicoloProcedimentale;
	}

	/**
	 * Imposta il wob number associato al decreto dirigenziale.
	 * 
	 * @param wobNumber
	 */
	public void setWobNumber(final String wobNumber) {
		this.wobNumber = wobNumber;
	}

}
