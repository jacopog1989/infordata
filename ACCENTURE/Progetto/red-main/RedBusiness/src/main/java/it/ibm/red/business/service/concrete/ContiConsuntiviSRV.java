package it.ibm.red.business.service.concrete;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import it.ibm.red.business.dto.ParamsRicercaContiConsuntiviDTO;
import it.ibm.red.business.dto.RisultatoRicercaContiConsuntiviDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.fop.FOPHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IContiConsuntiviFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.FileUtils;

/**
 * Servizio di gestione dei conti consuntivi.
 * 
 * @author SimoneLungarella
 */
@Service
public class ContiConsuntiviSRV extends XmlAbstractSRV implements IContiConsuntiviFacadeSRV{

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = -8468263821308029316L;

	/**
	 * Label di riferimento all'ente per la quale è valido il report dei conti consuntivi.
	 */
	private static final String ENTE_VALIDITA_REPORT = "UCB";
	
	/**
	 * Logger per la gestione degli errori e dei warning in console.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContiConsuntiviSRV.class);

	/**
	 * Messaggio di errore generico sulla generazione report dei conti consuntivi.
	 */
	private static final String GENERIC_ERROR_MSG = "Errore riscontrato durante la generazione del report dei conti consuntivi.";
	
	/**
	 * Genera il pdf del report a partire dai parametri della ricerca e dei
	 * risultati.
	 * 
	 * @param paramsRicerca
	 *            parametri della ricerca
	 * @param results
	 *            elenco dei risultati della ricerca
	 * @param utente
	 *            utente in sessione
	 * @return pdf generato
	 */
	@Override
	public byte[] generaPdfReport(ParamsRicercaContiConsuntiviDTO paramsRicerca, List<RisultatoRicercaContiConsuntiviDTO> results, UtenteDTO utente) {
		byte[] pdfFile = null;
		
		// Recupero dell'xslt specifico per la generazione del report
		byte[] xslt = FileUtils.getFileFromInternalResources("report/contiConsuntivi.xslt");
		try {
			// Trasformazione in pdf
			pdfFile = FOPHelper.transformToPdf(getCompleteXml(paramsRicerca, results, utente).getBytes(StandardCharsets.UTF_8), xslt);
		} catch (Exception ex) {
			LOGGER.error(GENERIC_ERROR_MSG, ex);
			throw new RedException(GENERIC_ERROR_MSG, ex);
		}
		return pdfFile;
	}
	
	/**
	 * Restituisce l'xml completo per il completamento dei placeholders dell'xslt
	 * per la generazione del file pdf associato al report delle notifiche e dei
	 * conti consuntivi.
	 * 
	 * @param paramsRicerca
	 *            parametri della ricerca
	 * @param results
	 *            risultati della ricerca
	 * @param utente
	 *            utente in sessione
	 * @return xml completo
	 */
	private String getCompleteXml(ParamsRicercaContiConsuntiviDTO paramsRicerca, List<RisultatoRicercaContiConsuntiviDTO> results, UtenteDTO utente) {
		Document document = prepareParameters();
		addToDocument("desc_aoo", utente.getAooDesc(), document);
		addToDocument("ente", ENTE_VALIDITA_REPORT, document);
		addToDocument("giorno_stampa", new Date(), document);
		addToDocument("orario_stampa", new SimpleDateFormat(DateUtils.HH24_MM_SS).format(new Date()), document);
		addToDocument("numero_risultati", results.size(), document);
		addToDocument("timestamp_footer", new SimpleDateFormat(DateUtils.DD_MM_YYYY_HH_MM_SS).format(new Date()), document);
		
		addToDocument("esercizio", paramsRicerca.getEsercizio(), document);
		addToDocument("sede_estera", paramsRicerca.getSedeEstera(), document);
		
		createXmlFromResults(results, document);
		
		return getXmlFromDocument(document);
	}

	/**
	 * Restituisce l'xml generato dai risultati della ricerca per il completamento
	 * del report dei conti consuntivi.
	 * 
	 * @param results
	 *            risultati della ricerca con cui popolare i parametri del template
	 * @param document
	 *            documento da popolare con i parametri
	 */
	private void createXmlFromResults(List<RisultatoRicercaContiConsuntiviDTO> results, Document document) {
		Element risultati = document.createElement("risultati");
		document.getLastChild().appendChild(risultati);
		for(RisultatoRicercaContiConsuntiviDTO risultato : results) {
			Element elementoRisultato = document.createElement("risultato");
			addToElement("codice_sede_estera", risultato.getCodiceSedEstera(), document, elementoRisultato);
			addToElement("sede_estera", risultato.getDescrizioneSedeEstera(), document, elementoRisultato);
			addToElement("esercizio", risultato.getEsercizio(), document, elementoRisultato);
			addToElement("data_notifica", risultato.getDataNotifica(), document, elementoRisultato);
			addToElement("numero_notifica", risultato.getNumeroNotifica(), document, elementoRisultato);
			addToElement("data_documento", risultato.getDataDocumento(), document, elementoRisultato);
			addToElement("numero_documento", risultato.getNumeroDocumento(), document, elementoRisultato);
			addToElement("assegnatario", risultato.getDescrizioneAssegnatario(), document, elementoRisultato);
			addToElement("stato", risultato.getStato(), document, elementoRisultato);
			addToElement("controllo_ucb", risultato.getControlloUcb(), document, elementoRisultato);
			addToElement("data_chiusura", risultato.getDataChiusura(), document, elementoRisultato);
			
			risultati.appendChild(elementoRisultato);
		}
	}
	
}
