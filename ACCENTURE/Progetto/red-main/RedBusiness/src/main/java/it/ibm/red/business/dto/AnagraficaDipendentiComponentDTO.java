/**
 * 
 */
package it.ibm.red.business.dto;

import java.text.SimpleDateFormat;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.enums.TipoDocumentoModeEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;

/**
 * @author
 *
 */
public class AnagraficaDipendentiComponentDTO extends MetadatoDTO {

	private static final long serialVersionUID = -6759234668288487499L;

	/**
	 * Collezione anagrafiche selezionate.
	 */
	private Collection<AnagraficaDipendenteDTO> selectedValues;

	/**
	 * @param name
	 * @param displayName
	 * @param visibility
	 * @param editability
	 * @param obligatoriness
	 * @param flagRange
	 * @param flagOut
	 */
	public AnagraficaDipendentiComponentDTO(final String name, final String displayName, final TipoDocumentoModeEnum visibility, final TipoDocumentoModeEnum editability, 
			final TipoDocumentoModeEnum obligatoriness, final Boolean flagRange, final Boolean flagOut) {
		super(null, name, TipoMetadatoEnum.PERSONE_SELECTOR, displayName, null, null, null, 
				visibility, editability, obligatoriness, flagRange, flagOut, null, false);
	}

	/**
	 * @param name
	 * @param selectedValues
	 * @param displayName
	 * @param visibility
	 * @param editability
	 * @param obligatoriness
	 */
	public AnagraficaDipendentiComponentDTO(final String name, final Collection<AnagraficaDipendenteDTO> selectedValues, final String displayName,
			final TipoDocumentoModeEnum visibility, final TipoDocumentoModeEnum editability, final TipoDocumentoModeEnum obligatoriness) {
		this(name, displayName, visibility, editability, obligatoriness, null, null);
		setSelectedValues(selectedValues);
	}

	/**
	 * Costruttore di default.
	 */
	public AnagraficaDipendentiComponentDTO() {
	}

	/**
	 * @return selectedValues
	 */
	public Collection<AnagraficaDipendenteDTO> getSelectedValues() {
		return selectedValues;
	}

	/**
	 * @param selectedValues
	 */
	public void setSelectedValues(final Collection<AnagraficaDipendenteDTO> selectedValues) {
		this.selectedValues = selectedValues;
	}

	/**
	 * Restituisce il valore selezionato dell'anagrafica sotto forma di stringa.
	 */
	@Override
	public String getValue4AttrExt() {
		final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		final StringBuilder output = new StringBuilder();
		if (getSelectedValues() != null && !getSelectedValues().isEmpty()) {
			for (final AnagraficaDipendenteDTO a : getSelectedValues()) {
				String dataDiNascita = "";
				if (a.getDataNascita() != null) {
					dataDiNascita = sdf.format(a.getDataNascita());
				}

				String sesso = "";
				if (a.getFlagMaschio()) {
					sesso = "M";
				} else if (!a.getFlagMaschio()) {
					sesso = "F";
				}

				output.append("<AnagraficaDipendente>").append("<NomeDipendente>").append(a.getNome()).append("</NomeDipendente>").append("<CognomeDipendente>")
						.append(a.getCognome()).append("</CognomeDipendente>").append("<DataNascitaDipendente>").append(dataDiNascita).append("</DataNascitaDipendente>")
						.append("<SessoDipendente>").append(sesso).append("</SessoDipendente>").append("<ComuneNascitaDipendente>")
						.append((StringUtils.isEmpty(a.getComuneNascita()) ? "" : a.getComuneNascita())).append("</ComuneNascitaDipendente>")
						.append("<CodiceFiscaleDipendente>").append(a.getCodiceFiscale()).append("</CodiceFiscaleDipendente>").append("</AnagraficaDipendente>");
			}
		}
		return output.toString();
	}

}
