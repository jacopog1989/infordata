/**
 * 
 */
package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.EventoSottoscrizioneDTO;
import it.ibm.red.business.service.facade.ISottoscrizioniFacadeSRV;

/**
 * @author APerquoti
 *
 */
public interface ISottoscrizioniSRV extends ISottoscrizioniFacadeSRV {

	/**
	 * Ottiene gli eventi sottoscritti tramite la categoria.
	 * @param idAoo
	 * @param idCategoriaEvento
	 * @param con
	 * @return lista degli eventi di sottoscrizione
	 */
	List<EventoSottoscrizioneDTO> getEventiSottoscrittiByCategoria(int idAoo, int idCategoriaEvento, Connection con); 
	
	/**
	 * Ottiene gli eventi sottoscritti tramite la lista degli eventi.
	 * @param idAoo
	 * @param idEventi
	 * @param con
	 * @return lista degli eventi di sottoscrizione
	 */
	List<EventoSottoscrizioneDTO> getEventiSottoscrittiByListaEventi(int idAoo, Integer[] idEventi, Connection con); 

}
