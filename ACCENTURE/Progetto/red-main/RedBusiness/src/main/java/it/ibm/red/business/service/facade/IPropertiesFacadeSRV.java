package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Map;

import it.ibm.red.business.enums.PropertiesNameEnum;

/**
 * The Interface IPropertiesFacadeSRV.
 *
 * @author CPIERASC
 * 
 *         Facade del servizio di gestione delle properties.
 */
public interface IPropertiesFacadeSRV extends Serializable {
	
	/**
	 * Metodo per il recupero di tutte le properties.
	 * 
	 * @return	properties recuperate
	 */
	Map<String, String> getAll();
	
	/**
	 * Metodo per il recupero di una property.
	 * 
	 * @param param	enum della property
	 * @return		valore della property
	 */
	String getByEnum(PropertiesNameEnum param);

}
