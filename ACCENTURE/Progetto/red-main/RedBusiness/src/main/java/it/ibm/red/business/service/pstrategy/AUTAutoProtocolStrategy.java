package it.ibm.red.business.service.pstrategy;

import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.MessaggioPostaNpsFlussoDTO;
import it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO;
import it.ibm.red.business.enums.AUTMessageIDEnum;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.builder.Builder;

/**
 * Strategy Protocollazione automatica AUT.
 */
public class AUTAutoProtocolStrategy extends AbstractAutoProtocolStrategy<MessaggioPostaNpsFlussoDTO> {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AUTAutoProtocolStrategy.class.getName());

	/**
	 * @see it.ibm.red.business.service.pstrategy.IAutoProtocolStrategy#protocol(it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO,
	 *      java.lang.String).
	 */
	@Override
	public EsitoSalvaDocumentoDTO protocol(final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessagio, final String guidMail) {
		return protocol(richiestaElabMessagio, guidMail, TipoContestoProceduraleEnum.FLUSSO_AUT);
	}

	/**
	 * Gestisce la protocollazione.
	 * 
	 * @param richiestaElabMessagio
	 * @param guidMail
	 * @param tcp
	 * @return Esito della protocollazione.
	 */
	protected EsitoSalvaDocumentoDTO protocol(final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessagio, final String guidMail, final TipoContestoProceduraleEnum tcp) {
		LOGGER.info("Avvio protocollazione automatica " + tcp.getId());
		EsitoSalvaDocumentoDTO out = null;
		final Integer idMessaggio = richiestaElabMessagio.getMessaggio().getDatiFlusso().getIdentificativo();
		final AUTMessageIDEnum msg = AUTMessageIDEnum.getEnumById(idMessaggio);
		
		if (AUTMessageIDEnum.MESSAGGIO_INIZIALE.equals(msg)) {
			LOGGER.info("Messaggio iniziale.");
			out = creaNuovoProcesso(richiestaElabMessagio, guidMail, tcp);
		} else if (AUTMessageIDEnum.DATI_INTEGRATIVI_RICHIESTI.equals(msg)) {
			LOGGER.info("Dati integrativi richiesti.");
			out = riconciliaIntegrazioneDati(richiestaElabMessagio, guidMail, tcp, false);
		} else if (AUTMessageIDEnum.DATI_INTEGRATIVI_SPONTANEI.equals(msg)) {
			LOGGER.info("Dati integrativi spontanei.");
			out = integraDatiSpontanei(richiestaElabMessagio, guidMail, tcp, Builder.AGLI_ATTI, false, false);
		} else if (AUTMessageIDEnum.RICHIESTA_RITIRO_AUTOTUTELA.equals(msg)) {
			LOGGER.info("Richiesta ritiro in autotutela.");
			out = ritiraDocumento(richiestaElabMessagio, guidMail, tcp);
		}
		
		LOGGER.info("Fine protocollazione automatica " + tcp.getId());
		return out;
	}
	
}
