package it.ibm.red.business.dto;

/**
 * DTO doc indice.
 */
public class DocIndiceDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 644681309315515816L;
	
	/**
	 * Id workflow.
	 */
	private String wobNumber;
	
	/**
	 * Document title.
	 */
	private String documentTitle;
	
	/**
	 * Indice di classificazione.
	 */
	private String indiceClassificazione;
	
	/**
	 * Descrizione indice di classificazione.
	 */
	private String indiceClassificazioneDescrizione;
	
	/**
	 * Numero documento.
	 */
	private String numeroDoc;
	
	/**
	 * Fascicolo procedimentale.
	 */
	private FascicoloDTO fascicoloProc;
	
	/**
	 * Identificativo aoo.
	 */
	private long idAoo;
	
	/**
	 * Flag modificato.
	 */
	private boolean modified;
	
	/**
	 * Identificativo protocollo.
	 */
	private String idProtocollo;
	
	/**
	 * Oggetto.
	 */
	private String oggetto;
    
	/**
	 * Descrizione tipologia documento.
	 */
	private String descrizioneTipologiaDocumento;

	/**
	 * Restituisce l'id protocollo.
	 * @return id protocollo
	 */
	public String getIdProtocollo() {
		return idProtocollo;
	}

	/**
	 * Imposta l'id del protocollo.
	 * @param idProtocollo
	 */
	public void setIdProtocollo(final String idProtocollo) {
		this.idProtocollo = idProtocollo;
	}

	/**
	 * Restituisce l'oggetto.
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Imposta l'oggetto.
	 * @param oggetto
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * Restituisce la descrizione della tipologia del documento.
	 * @return descrizione tipo documento
	 */
	public String getDescrizioneTipologiaDocumento() {
		return descrizioneTipologiaDocumento;
	}

	/**
	 * Imposta la descrizione della tipologia del documento.
	 * @param descrizioneTipologiaDocumento
	 */
	public void setDescrizioneTipologiaDocumento(final String descrizioneTipologiaDocumento) {
		this.descrizioneTipologiaDocumento = descrizioneTipologiaDocumento;
	}

	/**
	 * Restituisce il wob number.
	 * @return wob number
	 */
	public String getWobNumber() {
		return wobNumber;
	}

	/**
	 * Imposta il wob number.
	 * @param wobNumber
	 */
	public void setWobNumber(final String wobNumber) {
		this.wobNumber = wobNumber;
	}

	/**
	 * Restituisce il document title.
	 * @return documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta il document title.
	 * @param documentTitle
	 */
	public void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * Restituisce l'indice di classificazione.
	 * @return indice di classificazione
	 */
	public String getIndiceClassificazione() {
		return indiceClassificazione;
	}

	/**
	 * Imposta l'indice di classificazione.
	 * @param indiceClassificazione
	 */
	public void setIndiceClassificazione(final String indiceClassificazione) {
		this.indiceClassificazione = indiceClassificazione;
	}

	/**
	 * Restituisce il numero del documento.
	 * @return numero documento
	 */
	public String getNumeroDoc() {
		return numeroDoc;
	}

	/**
	 * Imposta il numero del documento.
	 * @param numeroDoc
	 */
	public void setNumeroDoc(final String numeroDoc) {
		this.numeroDoc = numeroDoc;
	}

	/**
	 * Restituisce true se modificato, false altrimenti.
	 * @return modified
	 */
	public boolean isModified() {
		return modified;
	}

	/**
	 * Imposta il flag: modified.
	 * @param modified
	 */
	public void setModified(final boolean modified) {
		this.modified = modified;
	}

	/**
	 * Restituisce l'id dell'Area Organizzativa Omogenea.
	 * @return id AOO
	 */
	public long getIdAoo() {
		return idAoo;
	}

	/**
	 * Imposta l'id dell'Area Organizzativa Omogenea.
	 * @param idAoo
	 */
	public void setIdAoo(final long idAoo) {
		this.idAoo = idAoo;
	}

	/**
	 * Restituisce il fascicolo procedimentale.
	 * @return fasciclo procedimentale
	 */
	public FascicoloDTO getFascicoloProc() {
		return fascicoloProc;
	}

	/**
	 * Imposta il fascicolo procedimentale.
	 * @param fascicoloProc
	 */
	public void setFascicoloProc(final FascicoloDTO fascicoloProc) {
		this.fascicoloProc = fascicoloProc;
	}

	/**
	 * Restituisce l'indice di classificazione.
	 * @return indice di classificazione
	 */
	public String getIndiceClassificazioneDescrizione() {
		return indiceClassificazioneDescrizione;
	}

	/**
	 * Imposta l'indice di classificazione.
	 * @param indiceClassificazioneDescrizione
	 */
	public void setIndiceClassificazioneDescrizione(final String indiceClassificazioneDescrizione) {
		this.indiceClassificazioneDescrizione = indiceClassificazioneDescrizione;
	}
	
}