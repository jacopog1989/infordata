package it.ibm.red.business.dto;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import it.ibm.red.business.enums.TipoAssegnazioneEnum;

/**
 * DTO assegnatario interoperabile.
 */
public class AssegnatarioInteropDTO extends AssegnatarioDTO {

	/**
	 * La costante serial version UID.
	 */
	private static final long serialVersionUID = -6394446732044340826L;

	/**
	 * @param idUfficio
	 * @param descrizioneUfficio
	 */
	public AssegnatarioInteropDTO(final Long idUfficio, final String descrizioneUfficio) {
		super(idUfficio, descrizioneUfficio);
	}
	
	/**
	 * @param idUfficio
	 * @param idUtente
	 */
	public AssegnatarioInteropDTO(final Long idUfficio, final Long idUtente) {
		super(idUfficio, idUtente);
	}
	
	/**
	 * @param idUfficio
	 * @param idUtente
	 * @param descrizioneUfficio
	 * @param descrizioneUtente
	 */
	public AssegnatarioInteropDTO(final Long idUfficio, final Long idUtente, final String descrizioneUfficio, final String descrizioneUtente) {
		super(idUfficio, idUtente, descrizioneUfficio, descrizioneUtente);
	}
	
	/**
	 * @param metadatoAssegnatarioInterop
	 */
	public AssegnatarioInteropDTO(final String metadatoAssegnatarioInterop) {
		super();
		
		if (StringUtils.isNotBlank(metadatoAssegnatarioInterop)) {
			final String[] metadatoAssegnatarioInteropSplit = metadatoAssegnatarioInterop.split("\\|");
			
			final String ufficio = metadatoAssegnatarioInteropSplit[0];
			String utente = null;
			if (metadatoAssegnatarioInteropSplit.length > 1) {
				utente = metadatoAssegnatarioInteropSplit[1];
			}
			
			// L'ufficio deve essere sempre presente
			if (NumberUtils.isParsable(ufficio)) {
				setIdUfficio(Long.parseLong(ufficio));
			} else {
				setDescrizioneUfficio(ufficio);
			}
			
			// L'utente non deve necessariamente essere presente
			if (utente != null) {
				if (NumberUtils.isParsable(utente)) {
					setIdUtente(Long.parseLong(utente));
				} else {
					setDescrizioneUtente(utente);
				}
			}
		}
	}
	
	/**
	 * @return
	 */
	public AssegnazioneDTO getAssegnazioneCompetenza() {
		final UfficioDTO uff = new UfficioDTO(getIdUfficio(), getDescrizioneUfficio());
		UtenteDTO utente = null;
		String descrizionePreassegnatario = getDescrizioneUfficio();
		if (getIdUtente() != null || getDescrizioneUtente() != null) {
			utente = new UtenteDTO();
			utente.setId(getIdUtente());
			utente.setIdUfficio(getIdUfficio());
			descrizionePreassegnatario += " - " + getDescrizioneUtente();
		}
		
		return new AssegnazioneDTO(null, TipoAssegnazioneEnum.COMPETENZA, null, null, descrizionePreassegnatario, utente, uff);
	}
	
}
