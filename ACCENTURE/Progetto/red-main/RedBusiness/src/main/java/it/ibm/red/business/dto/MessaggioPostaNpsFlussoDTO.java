/**
 * 
 */
package it.ibm.red.business.dto;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import it.ibm.red.business.enums.TipoMessaggioPostaNpsIngressoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.webservice.model.interfaccianps.npstypes.EmailMessageType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.ProtIdentificatoreProtocolloType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.WkfDatiContestoProceduraleType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.WkfDatiProtocollazioneCodaType;

/**
 * Rappresenta un messaggio proveniente da NPS afferente a un flusso.
 * 
 * @author m.crescentini
 *
 */
public class MessaggioPostaNpsFlussoDTO extends MessaggioPostaNpsDTO {

	private static final long serialVersionUID = -2913356870458530435L;

	/**
	 * Dati flusso.
	 */
	private DatiFlussoDTO datiFlusso;

	/**
	 * Costruttore per flusso ORDINARIO/SILICE/AUT/CG2 (messaggio di posta PEO/PEC).
	 * 
	 * @param idMessaggio
	 * @param idDocumento
	 * @param messaggioRicevuto
	 * @param protocollo
	 * @param codiceFlusso
	 * @param datiContestoProcedurale
	 * @param identificatoreProcesso
	 */
	public MessaggioPostaNpsFlussoDTO(final String idMessaggio, final String idDocumento, final EmailMessageType messaggioRicevuto,
			final ProtIdentificatoreProtocolloType protocollo, final ProtIdentificatoreProtocolloType protocolloNotifica, final String codiceFlusso, final WkfDatiContestoProceduraleType datiContestoProcedurale,
			final String identificatoreProcesso) {
		super(idMessaggio, idDocumento, TipoMessaggioPostaNpsIngressoEnum.PEO_PEC_FLUSSO, messaggioRicevuto, protocollo, protocolloNotifica);

		if (datiContestoProcedurale != null) {
			Integer identificativoMessaggio = null;
			if (NumberUtils.isCreatable(datiContestoProcedurale.getIdentificativoMessaggio())) {
				identificativoMessaggio = NumberUtils.createInteger(datiContestoProcedurale.getIdentificativoMessaggio());
			}

			this.datiFlusso = new DatiFlussoDTO(identificativoMessaggio, identificatoreProcesso, codiceFlusso, datiContestoProcedurale.getDatiEstesi());
		} else {
			throw new RedException("Contesto procedurale del flusso non presente");
		}
	}

	/**
	 * Costruttore per SICOGE.
	 * 
	 * @param idMessaggio
	 * @param protocollo
	 * @param codiceFlusso
	 * @param datiFlussoSicoge
	 * @param identificatoreProcesso
	 * @param idFascicoloFepa
	 * @param tipoFascicoloFepa
	 */
	public MessaggioPostaNpsFlussoDTO(final String idMessaggio, final ProtIdentificatoreProtocolloType protocollo, final String codiceFlusso,
			final WkfDatiProtocollazioneCodaType datiFlussoSicoge, final String identificatoreProcesso, final String idFascicoloFepa, final String tipoFascicoloFepa) {
		super(idMessaggio, TipoMessaggioPostaNpsIngressoEnum.NO_MAIL, protocollo);

		if (datiFlussoSicoge != null) {
			Integer identificativoMessaggio = null;
			if (StringUtils.isNotBlank(datiFlussoSicoge.getIdentificativoMessaggio())) {
				identificativoMessaggio = NumberUtils.createInteger(datiFlussoSicoge.getIdentificativoMessaggio());
			}

			this.datiFlusso = new DatiFlussoDTO(identificativoMessaggio, identificatoreProcesso, codiceFlusso, datiFlussoSicoge.getDatiEstesi(), idFascicoloFepa,
					tipoFascicoloFepa);
		} else {
			throw new RedException("Dati del flusso SICOGE non presenti");
		}
	}

	/**
	 * @return the datiFlusso
	 */
	public DatiFlussoDTO getDatiFlusso() {
		return datiFlusso;
	}

}
