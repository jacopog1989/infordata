package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.FormatoAllegatoDTO;

/**
 * @author SLac
 *
 */
public interface IFormatoAllegatoDAO extends Serializable {

	/**
	 * @param connection
	 * @return formati.
	 */
	List<FormatoAllegatoDTO> getAll(Connection connection); 
	
}
