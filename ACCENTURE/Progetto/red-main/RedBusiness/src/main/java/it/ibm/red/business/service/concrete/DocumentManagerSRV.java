package it.ibm.red.business.service.concrete;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.util.JavaScriptUtils;

import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;
import com.google.common.net.MediaType;

import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants.Modalita;
import it.ibm.red.business.constants.Constants.TipologiaInvio;
import it.ibm.red.business.dao.ICasellaPostaleDAO;
import it.ibm.red.business.dao.IContattoDAO;
import it.ibm.red.business.dao.IDatiPredefinitiMozioneDAO;
import it.ibm.red.business.dao.IEventoLogDAO;
import it.ibm.red.business.dao.IFormatoAllegatoDAO;
import it.ibm.red.business.dao.IIterApprovativoDAO;
import it.ibm.red.business.dao.ILegislaturaDAO;
import it.ibm.red.business.dao.IMezzoRicezioneDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.ITemplateDAO;
import it.ibm.red.business.dao.ITestoPredefinitoDAO;
import it.ibm.red.business.dao.ITipoAssegnazioneDAO;
import it.ibm.red.business.dao.ITipoFileDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentAppletContentDTO;
import it.ibm.red.business.dto.DocumentManagerDTO;
import it.ibm.red.business.dto.EsitoDTO;
import it.ibm.red.business.dto.EsitoVerificaProtocolloDTO;
import it.ibm.red.business.dto.EventoLogDTO;
import it.ibm.red.business.dto.FadAmministrazioneDTO;
import it.ibm.red.business.dto.FadRagioneriaDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.MezzoRicezioneDTO;
import it.ibm.red.business.dto.RaccoltaFadDTO;
import it.ibm.red.business.dto.ResponsabileCopiaConformeDTO;
import it.ibm.red.business.dto.RicercaRubricaDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.TestoPredefinitoDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.FunzionalitaEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.PermessiAOOEnum;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.RiservatezzaEnum;
import it.ibm.red.business.enums.TestoPredefinitoEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.enums.TipoGestioneEnum;
import it.ibm.red.business.enums.TipoRubricaEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.adobe.AdobeLCHelper;
import it.ibm.red.business.helper.adobe.IAdobeLCHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.DatiPredefinitiMozione;
import it.ibm.red.business.persistence.model.NodoUtenteCoordinatore;
import it.ibm.red.business.persistence.model.TipoFile;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.ICasellePostaliSRV;
import it.ibm.red.business.service.ICodaMailSRV;
import it.ibm.red.business.service.IDocumentManagerSRV;
import it.ibm.red.business.service.IDocumentoRedSRV;
import it.ibm.red.business.service.IFepaSRV;
import it.ibm.red.business.service.IRegistroRepertorioSRV;
import it.ibm.red.business.service.ISiebelSRV;
import it.ibm.red.business.service.ITipologiaDocumentoSRV;
import it.ibm.red.business.service.ITitolarioSRV;
import it.ibm.red.business.service.facade.IFascicoloFacadeSRV;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.business.service.facade.ITracciaDocumentiFacadeSRV;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Service gestione document manager.
 */
@Service
@Component
public class DocumentManagerSRV extends AbstractService implements IDocumentManagerSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 2538886103833217330L;

	/**
	 * Informativa: DOC-CONTENT MODIFICABILE.
	 */
	private static final String DOCCONTENTMODIFICABILE_LABEL = "[CHECK DOC-CONTENT MODIFICABILE][";

	/**
	 * Messaggio di errore associato al documento non allacciabile.
	 */
	private static final String ERROR_DOCUMENTO_NON_ALLACCIABILE_MSG = "Il documento in ingresso verificato non è allacciabile in quanto non di competenza dell'utente.";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DocumentManagerSRV.class);

	/**
	 * Conserva le logica della tipologia del documento.
	 */
	@Autowired
	private ITipologiaDocumentoSRV tipologiaDocumentoSRV;

	/**
	 * Dao.
	 */
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private IMezzoRicezioneDAO mezzoRicezioneDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private IContattoDAO contattoDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private ITracciaDocumentiFacadeSRV tracciaDocumentiSRV;

	/**
	 * Dao.
	 */
	@Autowired
	private IIterApprovativoDAO iterApprovativoDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private ITipoAssegnazioneDAO tipoAssegnazioneDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private ITemplateDAO templateDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private ITestoPredefinitoDAO testoPredefinitoDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private ILegislaturaDAO legislaturaDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private ITipoFileDAO tipoFileDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private IFormatoAllegatoDAO formatoAllegatoDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private IEventoLogDAO eventDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private ICasellaPostaleDAO casellaPostaleDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private IDatiPredefinitiMozioneDAO mozioneDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private ITitolarioSRV titolarioSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IFepaSRV fepaSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IFascicoloFacadeSRV fascicoloSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IDocumentoRedSRV documentoRedSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ISignFacadeSRV signSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ICasellePostaliSRV casellePostaliSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ISiebelSRV siebelSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IRegistroRepertorioSRV registroRepertorioSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ICodaMailSRV codaMailSRV;

	private static void calcoloModalitaMaschera(final DocumentManagerDTO dmDTO, final DetailDocumentRedDTO detail) {
		// azzero per sicurezza tutto quanto
		dmDTO.setInModifica(false);
		dmDTO.setInModificaIngresso(false);
		dmDTO.setInModificaUscita(false);
		dmDTO.setInCreazioneIngresso(false);
		dmDTO.setInCreazioneUscita(false);

		final CategoriaDocumentoEnum categoriaDOC = CategoriaDocumentoEnum.getExactly(detail.getIdCategoriaDocumento());
		dmDTO.setCategoria(categoriaDOC);
		dmDTO.setInModifica(!StringUtils.isNullOrEmpty(detail.getDocumentTitle()));

		if (dmDTO.isInModifica()) {
			dmDTO.setModalitaMaschera(Modalita.MODALITA_UPD);
			if (CategoriaDocumentoEnum.ENTRATA.equals(categoriaDOC) || CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.equals(categoriaDOC)) {
				dmDTO.setInModificaIngresso(true);
			} else if (CategoriaDocumentoEnum.DOCUMENTO_USCITA.equals(categoriaDOC) || CategoriaDocumentoEnum.USCITA.equals(categoriaDOC)
					|| CategoriaDocumentoEnum.MOZIONE.equals(categoriaDOC) || CategoriaDocumentoEnum.CONTRIBUTO_ESTERNO.equals(categoriaDOC)) {
				dmDTO.setInModificaUscita(true);
			}

		} else { // altrimenti è in creazione perché il documentTitle = null
			dmDTO.setModalitaMaschera(Modalita.MODALITA_INS);

			if (CategoriaDocumentoEnum.ENTRATA.equals(categoriaDOC) || CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.equals(categoriaDOC)) {
				dmDTO.setInCreazioneIngresso(true);
			} else if (CategoriaDocumentoEnum.USCITA.equals(categoriaDOC) || CategoriaDocumentoEnum.DOCUMENTO_USCITA.equals(categoriaDOC)
					|| CategoriaDocumentoEnum.MOZIONE.equals(categoriaDOC) || CategoriaDocumentoEnum.CONTRIBUTO_ESTERNO.equals(categoriaDOC)) {
				dmDTO.setInCreazioneUscita(true);
			}

		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#prepareMascheraCreazioneOModifica(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.DetailDocumentRedDTO, boolean).
	 */
	@Override
	public DocumentManagerDTO prepareMascheraCreazioneOModifica(final UtenteDTO utente, final DetailDocumentRedDTO detail, final boolean fromProtocollaMail) {
		final DocumentManagerDTO item = new DocumentManagerDTO();
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			calcoloModalitaMaschera(item, detail);
			final CategoriaDocumentoEnum categoria = item.getCategoria();
			final String modalitaMaschera = item.getModalitaMaschera();
			item.setFlagProtocollaMail(fromProtocollaMail);

			// GESTIONE FORMATI START
			item.setComboFormatiDocumentoVisible(isComboFormatiDocumentoVisible(modalitaMaschera, categoria));
			// la combo formato o si vede nel formato del documento in entrata oppure per il
			// mezzo di spedizione dei destinatari
			item.setComboFormatiDocumento(getComboFormatoDocumentoByPermessiUtente(utente.getPermessi()));
			// GESTIONE FORMATI END

			/**
			 * TIPOLOGIA DOCUMENTO START
			 ***************************************************************************************************************/
			// IMPORTANTE: se il documento e' in entrata la categoria passata per la query
			// e' quella di ENTRATA se invece e' di tipo MOZIONE/USCITA
			// la query deve farla con categoria USCITA
			final TipoCategoriaEnum tce = (CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.equals(item.getCategoria())
					|| CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA.equals(item.getCategoria())) ? TipoCategoriaEnum.ENTRATA : TipoCategoriaEnum.USCITA;
			final Integer idTipologiaDocumento = item.isInModifica() ? detail.getIdTipologiaDocumento() : null;
			final List<TipologiaDocumentoDTO> listTipologiaDocumentoPrincipale = tipologiaDocumentoSRV
					.getComboTipologieByUtenteAndTipoCategoriaAndTipologiaSelezionataNoAutomatici(utente.getIdAoo(), utente.getIdUfficio(), tce.getIdTipoCategoria(),
							idTipologiaDocumento, connection);
			item.setComboTipologiaDocumento(listTipologiaDocumentoPrincipale);
			/**
			 * TIPOLOGIA DOCUMENTO END
			 *****************************************************************************************************************/

			/**
			 * REGISTRO REPERTORIO START
			 ***************************************************************************************************************/
			item.setRegistriRepertorioConfigured(registroRepertorioSRV.getRegistriRepertorioConfigurati(utente.getIdAoo(), connection));
			/**
			 * REGISTRO REPERTORIO END
			 ***************************************************************************************************************/

			item.setMittenteVisible(this.isMittentiVisible(categoria));

			// la lista la carico comunque perche' serve sia al mittente che ai destinatari
			final List<MezzoRicezioneDTO> listMezziRicezione = mezzoRicezioneDAO.getAllMezziRicezione(connection);
			if (detail.getMezzoRicezione() != null && detail.getMezzoRicezione().intValue() > 0) {
				final MezzoRicezioneDTO m = mezzoRicezioneDAO.getMezzoRicezioneByIdMezzo(detail.getMezzoRicezione(), connection);
				detail.setMezzoRicezioneDescrizione(m.getDescrizione());
			}
			item.setAllMezziRicezione(listMezziRicezione);
			if (isMittentiVisible(categoria)) {
				// Non esiste più distinzione, esistono solo i formati cartacei
				if (fromProtocollaMail) {
					item.setComboMezziRicezione(filtraMezziRicezioneByFormatoDocumento(listMezziRicezione, FormatoDocumentoEnum.ELETTRONICO));

					if (detail.getMittenteContatto() != null) {
						boolean isPec = true;
						if (StringUtils.isNullOrEmpty(detail.getMittenteContatto().getMailPec())) {
							isPec = false;
						}

						for (final MezzoRicezioneDTO mrDTO : item.getComboMezziRicezione()) {
							if ((isPec && mrDTO.getDescrizione().toUpperCase().contains("PEC")) || (!isPec && mrDTO.getDescrizione().toUpperCase().contains("PEO"))) {
								detail.setMezzoRicezioneDTO(mrDTO);
								detail.setMezzoRicezione(mrDTO.getIdMezzoRicezione());
								detail.setMezzoRicezioneDescrizione(mrDTO.getDescrizione());
								break;
							}
						}
					}

				} else {
					item.setComboMezziRicezione(filtraMezziRicezioneByFormatoDocumento(listMezziRicezione, FormatoDocumentoEnum.CARTACEO));
				}

				// serve solo la protocollazione da mail start
				if (DestinatariRedUtils.is0orNull(detail.getMezzoRicezione()) && !StringUtils.isNullOrEmpty(detail.getProtocollazioneMailGuid())) {
					// la protocollazione è da mail devo quindi selezionare di base la mail PEC
					for (final MezzoRicezioneDTO mezzoRicezioneDTO : listMezziRicezione) {
						if (mezzoRicezioneDTO.getDescrizione().toUpperCase().contains("PEC")) {
							detail.setMezzoRicezione(mezzoRicezioneDTO.getIdMezzoRicezione());
							break;
						}
					}
				}
				// serve solo la protocollazione da mail end
			}

			item.setDestinatariVisible(this.isDestinatariVisible(categoria));

			/** GESTIONE CONTATTI PREFERITI START ***************************************/
			detail.setContattiPreferitiSenzaGruppi(true);
			if (item.isInCreazioneUscita() || item.isInModificaUscita()) {
				detail.setContattiPreferitiSenzaGruppi(false);
			}
			item.setContattiPreferiti(gestisciContattiPreferitiPerComboDestinatario(connection, utente, detail));
			/** GESTIONE CONTATTI PREFERITI END ***************************************/

			if (item.isDestinatariVisible()) {
				// INTERNO / ESTERNO
				final List<TipologiaDestinatarioEnum> listComboTipologiaDestinatari = new ArrayList<>();
				listComboTipologiaDestinatari.addAll(Arrays.asList(TipologiaDestinatarioEnum.values()));
				item.setComboTipologiaDestinatari(listComboTipologiaDestinatari);

				// TO / CC
				final List<ModalitaDestinatarioEnum> listComboModalitaDestinatario = retrieveComboModalitaDestinatario();
				item.setComboModalitaDestinatario(listComboModalitaDestinatario);

				// CARTACEO / ELETTRONICO
				final List<MezzoSpedizioneEnum> listComboMezziSpedizione = new ArrayList<>();
				for (final MezzoSpedizioneEnum mse : MezzoSpedizioneEnum.values()) {
					if (mse == MezzoSpedizioneEnum.NON_SPECIFICATO) {
						continue;
					}
					listComboMezziSpedizione.add(mse);
				}
				item.setComboMezziSpedizione(listComboMezziSpedizione);
			}

			/** TRACCIA PROCEDIMENTO START ***************************/
			// controllo prima che il tracciamento sia visibile altrimenti levo la checkbox
			item.setTracciamentoVisible(tracciaDocumentiSRV.hasTracciamentiSingoli(utente));
			if (item.isTracciamentoVisible() && !StringUtils.isNullOrEmpty(detail.getDocumentTitle())) {
				final int documentTitle = Integer.parseInt(detail.getDocumentTitle());
				detail.setTracciaProcedimento(tracciaDocumentiSRV.isDocumentoTracciato(utente, documentTitle));
			}
			/** TRACCIA PROCEDIMENTO END *****************************/

			/** INTEGRAZIONE DATI VISIBILE ************/
			item.setIntegrazioneDatiVisible(utente.isUcb() && TipoCategoriaEnum.ENTRATA.equals(tce));
			/** INTEGRAZIONE DATI VISIBILE ************/

			/** RISERVATEZZA START ********************************************************/
			item.setComboRiservatezza(initComboRiservatezza());
			/** RISERVATEZZA END ********************************************************/

			/**
			 * ITER APPROVATIVI START
			 ******************************************************/
			final List<IterApprovativoDTO> listIter = iterApprovativoDAO.getAllByIdAOO(utente.getIdAoo().intValue(), connection);
			listIter.add(new IterApprovativoDTO(0, "Manuale", "Iter approvativo con assegnazione manuale", 0, 0, 0, utente.getIdAoo().intValue(), null, null, false, false));
			item.setComboIterApprovativi(listIter);
			/** ITER APPROVATIVI END ******************************************************/

			/**
			 * TIPO ASSEGNAZIONE MANUALE START
			 ***************************************************************************/
			item.setComboTipoAssegnazione(getComboTipoAssegnazione(detail, item.isInModifica(), false, utente.getIdAoo(), connection));
			/**
			 * TIPO ASSEGNAZIONE MANUALE END
			 *****************************************************************************/

			/**
			 * MOMENTO PROTOCOLLAZIONE START
			 ******************************************************************************/
			item.setComboMomentoProtocollazione(new ArrayList<>());
			item.getComboMomentoProtocollazione().add(MomentoProtocollazioneEnum.PROTOCOLLAZIONE_NON_STANDARD);
			item.getComboMomentoProtocollazione().add(MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD);
			/**
			 * MOMENTO PROTOCOLLAZIONE END
			 ********************************************************************************/

			item.setDaNonProtocollareVisible(isDaNonProtocollareVisible(item.isInModifica(), categoria));

			item.setRegistroRiservatoVisible(utente.getRegistroRiservato());

			item.setIterDocUscitaVisible(isIterDocUscitaVisible(categoria));
			if (item.isIterDocUscitaVisible()) {
				item.setComboCoordinatori(utenteDAO.getComboNodiUtenteCoordinatore(utente.getIdUfficio(), connection));
				if (item.getComboCoordinatori() != null && !item.getComboCoordinatori().isEmpty()) {
					final NodoUtenteCoordinatore n = new NodoUtenteCoordinatore();
					n.setIdUtenteCoordinatore(0L);
					n.setIdUfficioCoordinatore(0L);
					item.getComboCoordinatori().add(0, n);
				}
			}

			// responsabili copia conforme
			item.setComboResponsabileCopiaConforme(utenteDAO.getComboResponsabiliCopiaConforme(utente.getIdAoo(), connection));

			// corte dei conti
			item.setFlagCorteContiVisible(utente.isPermessoAooCorteDeiConti());

			item.setIdRagioniere(utenteDAO.getIdRagioniereGeneraleDelloStato(connection));

			item.setMaxAnno(Calendar.getInstance().get(Calendar.YEAR));

			item.setBarcodePanelVisible(isBarcodePanelVisible(modalitaMaschera, categoria, detail.getFormatoDocumentoEnum()));

			if (!StringUtils.isNullOrEmpty(detail.getDocumentTitle()) && detail.getNumeroRDP() != null && detail.getNumeroRDP().intValue() > 0) {
				item.setNumeroRdpReadOnly(true);
			}

			item.setRdpPanelVisible(isRdpPanelVisible(categoria, detail.getDescTipoProcedimento()));

			item.setDocPrincipalePanelVisible(this.isDocPrincipalePanelVisible(modalitaMaschera, detail.getFormatoDocumentoEnum(), item.getCategoria(), detail.getStorico()));

			item.setAssegnatarioPerCompetenzaVisible(isAssegnatarioPerCompetenzaVisible(categoria));

			item.setIndiceDiClassificazioneVisible(isIndiceDiClassificazioneVisible(detail.getFormatoDocumentoEnum()));
			if (item.isIndiceDiClassificazioneVisible()) {
				item.setTitolarioRootList(titolarioSRV.getFigliRadice(utente.getIdAoo(), utente.getIdUfficio(), connection));
				item.setIndiceDiClassificazioneRequired(isIndiceDiClassificazioneRequired(categoria));
			}

			item.setComboRicercaFascicoloModalita(Arrays.asList(RicercaGenericaTypeEnum.values()));

			item.setProtocolloMittenteRispostaVisible(isProtocolloMittenteRispostaVisible(categoria, detail.getFormatoDocumentoEnum()));

			item.setRaccoltaFadVisible(isRaccoltaFadVisible(detail));
			item.setComboFadAmministrazioni(fepaSRV.getComboAmministrazioni(connection));

			/**
			 * ALLEGATI START
			 ***********************************************************************************/
			item.setComboFormatiAllegato(formatoAllegatoDAO.getAll(connection));

			item.setAllegatiResponsabiliCopiaConformeVisible(
					this.isResponsabiliCopiaConformeForAllegatoVisible(categoria, item.getComboResponsabileCopiaConforme().isEmpty()));
			item.setAllegatiCheckDaFirmareVisible(this.isAllegatiCheckDaFirmareVisible(categoria));

			LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] prepareMascheraCreazioneOModifica {" + utente.getUsername() + " - uff: "
					+ utente.getIdUfficio() + "}");
			if (detail.getModificabile() == null) {
				Boolean documentoModificabile = true;
				Boolean contentModificabile = true;
				if (detail.getDocumentTitle() != null) {
					final boolean modificabilePerAssegnazione = documentoRedSRV.isDocumentoModificabile(detail, utente);

					LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] isDocModificabileByAssegnazione " + modificabilePerAssegnazione);
					LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] utente.isGestioneApplicativa() " + utente.isGestioneApplicativa());

					documentoModificabile = !utente.isGestioneApplicativa() && modificabilePerAssegnazione && !isDocFirmato(detail, item);
					contentModificabile = !utente.isGestioneApplicativa() && modificabilePerAssegnazione && isDocPrincipaleRimuovibile(utente, detail, item);
				}
				detail.setModificabile(documentoModificabile);
				detail.setContentModificabile(contentModificabile);

				LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] detail.getModificabile " + detail.getModificabile());
				LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] detail.getContentModificabile " + detail.getContentModificabile());
			}

			item.setAllegatiModificabili(!item.isInModificaIngresso() && detail.getModificabile());
			/**
			 * ALLEGATI END
			 ***********************************************************************************/

			/**
			 * SPEDIZIONE START
			 ***************************************************************************************************/

			item.setTabSpedizioneVisible(isTabSpedizioneVisible(categoria));
			if (item.isTabSpedizioneVisible()) {
				final String pattern = "yyyy-MM-dd HH:mm:ss.SSS";
				final SimpleDateFormat sdf = new SimpleDateFormat(pattern);
				LOGGER.info("START##AnalisiSped - tabSpedizioneVisibile : " + sdf.format(new Date()));
				final List<CasellaPostaDTO> caselle = casellePostaliSRV.getCasellePostali(utente, FunzionalitaEnum.TAB_SPEDIZIONE);
				item.setCaselleSpedizioneByUser(new ArrayList<>());
				if (caselle != null) {
					for (final CasellaPostaDTO casellaPostaDTO : caselle) {
						if (casellaPostaleDAO.getAbilitazioniCasellaPostale(casellaPostaDTO.getIndirizzoEmail(), utente.getIdUfficio(), FunzionalitaEnum.TAB_SPEDIZIONE,
								connection)) {
							item.getCaselleSpedizioneByUser().add(casellaPostaDTO);
						}
					}
				}

//				Una volta calcolate le caselle mail per l'utente devo controllare che l'utente sia il ragioniere,
//				oppure che l'iter di firma sia ragioniere oppure se l'assegnatario per firma sia il ragioniere.
//				Se è così devo mettere la casella preimpostata altrimenti metto le caselle calcolate sopra.
				item.setComboCaselleSpedizione(new ArrayList<>());
				if (isCasellaTabSpedizioniConfigurata(utente, item, detail.getIdIterApprovativo(), detail.getTipoAssegnazioneEnum(), detail.getAssegnazioni())) {
					final CasellaPostaDTO c = new CasellaPostaDTO();
					c.setIndirizzoEmail(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MAIL_COORDINAMENTO));
					item.getComboCaselleSpedizione().add(c);
				} else {
					item.setComboCaselleSpedizione(item.getCaselleSpedizioneByUser());
				}

				// Siamo nel caso in cui il documento è in modifica, devo verificare che la
				// casella con cui è stato creato ci sia
				// tra le caselle calcolate, se non c'è la devo aggiungere
				boolean aggiungiCasellaSalvata = detail.getMailSpedizione() != null && !StringUtils.isNullOrEmpty(detail.getMailSpedizione().getMittente());
				if (aggiungiCasellaSalvata && item.getComboCaselleSpedizione() != null && !item.getComboCaselleSpedizione().isEmpty()) {
					for (final CasellaPostaDTO casellaPostaDTO : item.getComboCaselleSpedizione()) {
						if (casellaPostaDTO.getIndirizzoEmail().equals(detail.getMailSpedizione().getMittente())) {
							aggiungiCasellaSalvata = false;
							break;
						}
					}
				}

				if (aggiungiCasellaSalvata) {
					final CasellaPostaDTO c = new CasellaPostaDTO();
					c.setIndirizzoEmail(detail.getMailSpedizione().getMittente());
					item.getCaselleSpedizioneByUser().add(c);
				}

				item.setComboTestiPredefiniti(new ArrayList<>());
				item.getComboTestiPredefiniti().add(new TestoPredefinitoDTO(0L, "", "", "", 0L, 0));
				item.getComboTestiPredefiniti()
						.addAll(testoPredefinitoDAO.getTestiByTipoAndAOO(TestoPredefinitoEnum.TESTO_PREDEFINITO_MAIL.getId(), utente.getIdAoo(), connection));

				item.setPrefixOggettoMail(getPrefixOggettoMail(detail, utente.getCodiceAoo()));

				if (!StringUtils.isNullOrEmpty(detail.getIdFascicoloProcedimentale())) {
					// Se esiste un fascicolo carico i suoi documenti
					item.setAllegatiMail(getAllegatiMail(detail.getIdFascicoloProcedimentale(), detail.getDocumentTitle(), utente));
				}

				item.setTabSpedizioneDisabled(true);
				if (detail.getDestinatari() != null && !detail.getDestinatari().isEmpty()) {
					for (final DestinatarioRedDTO d : detail.getDestinatari()) {
						if (d.getMezzoSpedizioneEnum() == MezzoSpedizioneEnum.ELETTRONICO) {
							item.setTabSpedizioneDisabled(false);
							break;
						}
					}
				}
				LOGGER.info("END##AnalisiSped - tabSpedizioneVisibile : " + sdf.format(new Date()));
			}

			/**
			 * SPEDIZIONE END
			 *****************************************************************************************************/

			/**
			 * Eventuale legislatura start
			 ***********************************************************************************************/
			if (item.isInCreazioneUscita() || item.isInModificaUscita()) {
				item.setUltimaLegislatura(legislaturaDAO.getLegislaturaCorrente(connection));
			}
			/**
			 * Eventuale legislatura end
			 *************************************************************************************************/

			/**
			 * CAMPI DA DISABILITARE IN MODIFICA START
			 *****************************************************/
			if (item.isInModifica()) {
				item.setAssegnazioneBtnDisabled(true);
			}
			/**
			 * CAMPI DA DISABILITARE IN MODIFICA END
			 *****************************************************/

			/**
			 * Estensioni start
			 *********************************************************************************************/
			Collection<TipoFile> extsAmmesse = null;
			final Collection<TipoFile> extsAll = tipoFileDAO.getAll(connection);

			item.setEstensioniAmmesse(new ArrayList<>());
			item.setEstensioniAll(new ArrayList<>());

			if (item.isInCreazioneUscita() || item.isInModificaUscita()) {
				extsAmmesse = tipoFileDAO.getTipiFileDocUscita(connection);
			} else {
				extsAmmesse = new ArrayList<>(extsAll);
			}

			for (final TipoFile tipoFile : extsAmmesse) {
				item.getEstensioniAmmesse().add(tipoFile.getEstensione());
			}
			for (final TipoFile tipoFile : extsAll) {
				item.getEstensioniAll().add(tipoFile.getEstensione());
			}
			/**
			 * Estensioni end
			 *********************************************************************************************/

			/** CONTRIBUTI ESTERNI -> START */
			if (CategoriaDocumentoEnum.CONTRIBUTO_ESTERNO.equals(categoria)) {
				item.setContributoNumAnnoEsternoVisible(true);
			}

			if (PermessiUtils.hasPermesso(utente.getPermessiAOO(), PermessiAOOEnum.CONTRIBUTO_ESTERNO)) {
				item.setTabContributiEsterniVisible(true);
			}
			/********************************/

			/***** SIEBEL *******************/
			final boolean rdsSiebel = siebelSRV.hasRds(detail.getDocumentTitle(), utente.getIdAoo(), connection);
			item.setSiebel(rdsSiebel);
			/********************************/

			/** TEMPLATE DOC USCITA *********/
			final boolean isTemplateDocUscitaAttivo = PermessiUtils.hasPermesso(utente.getPermessiAOO(), PermessiAOOEnum.TEMPLATE_DOC_USCITA);
			if (isTemplateDocUscitaAttivo) {
				final boolean isGeneratedByTemplateDocUscita = MapUtils.isNotEmpty(detail.getTemplateDocUscitaMap());
				item.setTemplateDocUscitaVisible(isTemplateDocUscitaButtonVisible(isTemplateDocUscitaAttivo, detail.getContentModificabile(), isGeneratedByTemplateDocUscita,
						item.isInCreazioneUscita(), item.isInModificaUscita(), !StringUtils.isNullOrEmpty(detail.getNomeFile())));
				item.setTemplateFileListVisible(false);
				item.setGeneratedByTemplateDocUscita(isGeneratedByTemplateDocUscita);
			} else {
				item.setTemplateFileList(templateDAO.getAll(connection, utente.getIdAoo(), true));
				item.setTemplateFileListVisible(isTemplateButtonVisible(modalitaMaschera, categoria));
			}
			/********************************/

			/** SHORTCUTS CONTATTI START ***********************************************/
			item.setInserisciContattoItem(new Contatto());
			final RicercaRubricaDTO ricercaRubricaDTO = new RicercaRubricaDTO();
			ricercaRubricaDTO.setRicercaRED(true);
			ricercaRubricaDTO.setRicercaIPA(true);
			ricercaRubricaDTO.setRicercaMEF(true);
			ricercaRubricaDTO.setIsUfficio(true);
			item.setRicercaRubricaDTO(ricercaRubricaDTO);
			/** SHORTCUTS CONTATTI END ***********************************************/

			item.setVerificaFirmaDocPrincipaleVisible(isVerificaFirmaDocPrincipaleVisible(detail.getMimeType()));
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return item;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#
	 * getComboTipoAssegnazione( it.ibm.red.business.dto.DetailDocumentRedDTO,
	 * boolean, boolean, java.lang.Long)
	 */
	@Override
	public final List<TipoAssegnazioneEnum> getComboTipoAssegnazione(final DetailDocumentRedDTO detail, final boolean inModifica, final boolean onlyFirma, final Long idAoo) {
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			return getComboTipoAssegnazione(detail, inModifica, onlyFirma, idAoo, con);
		} catch (final SQLException e) {
			LOGGER.error("Errore di connessione verso il DB durante il recupero delle tipologie di assegnazione", e);
			throw new RedException("Errore durante il recupero delle tipologie di assegnazione", e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @param detail
	 * @param inModifica
	 * @param onlyFirma
	 * @param idAoo
	 * @param connection
	 * @return
	 */
	private List<TipoAssegnazioneEnum> getComboTipoAssegnazione(final DetailDocumentRedDTO detail, final boolean inModifica, final boolean onlyFirma, final Long idAoo,
			final Connection connection) {
		final List<TipoAssegnazioneEnum> comboTipoAssegnazione = new ArrayList<>();

		if (onlyFirma) {
			comboTipoAssegnazione.add(TipoAssegnazioneEnum.FIRMA);
			comboTipoAssegnazione.add(TipoAssegnazioneEnum.SIGLA);
		} else if (inModifica) {
			if (detail.getAssegnazioni() != null) {
				for (final AssegnazioneDTO assegnazione : detail.getAssegnazioni()) {
					if ((detail.getWobNumberPrincipale() != null && detail.getWobNumberPrincipale().equals(assegnazione.getWobNumber()))
							|| (detail.getWobNumberPrincipale() == null)) {
						comboTipoAssegnazione.add(assegnazione.getTipoAssegnazione());
						break;
					}
				}
			}
		} else {
			List<TipoAssegnazioneEnum> tipiAssegnazioni = tipoAssegnazioneDAO.getTipiAssegnazioneManualeByIdAoo(idAoo.intValue(), connection);
			
			// Se la creazione avviene dalla predisposizione di una registrazione ausiliaria viene rimosso il tipo assegnazione SPEDIZIONE
			if (detail.getIdRegistroAusiliario() != null) {
				
				tipiAssegnazioni.remove(TipoAssegnazioneEnum.SPEDIZIONE);
			}
			
			comboTipoAssegnazione.addAll(tipiAssegnazioni);
		}

		return comboTipoAssegnazione;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#
	 *      retrieveComboModalitaDestinatario().
	 */
	@Override
	public List<ModalitaDestinatarioEnum> retrieveComboModalitaDestinatario() {
		return EnumSet.allOf(ModalitaDestinatarioEnum.class).stream().collect(Collectors.toList());
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#
	 *      isCasellaTabSpedizioniConfigurata( it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.DocumentManagerDTO, java.lang.Integer,
	 *      it.ibm.red.business.enums.TipoAssegnazioneEnum, java.util.List).
	 */
	@Override
	public boolean isCasellaTabSpedizioniConfigurata(final UtenteDTO utente, final DocumentManagerDTO item, final Integer idIterApprovativo,
			final TipoAssegnazioneEnum tipoAssegnazioneEnum, final List<AssegnazioneDTO> assegnazioni) {
		boolean isConfigurata = false;
		if (utente.getId().equals(item.getIdRagioniere())
				|| (idIterApprovativo != null && IterApprovativoDTO.ITER_FIRMA_RAGIONIERE.intValue() == idIterApprovativo.intValue())) {
			isConfigurata = true;
		} else if (idIterApprovativo != null && IterApprovativoDTO.ITER_FIRMA_MANUALE.intValue() == idIterApprovativo.intValue()
				&& tipoAssegnazioneEnum == TipoAssegnazioneEnum.FIRMA) {
			for (final AssegnazioneDTO a : assegnazioni) {
				if (a.getTipoAssegnazione() == TipoAssegnazioneEnum.FIRMA && a.getUtente().getId().equals(item.getIdRagioniere())) {
					isConfigurata = true;
					break;
				}
			}
		}
		return isConfigurata;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#getAllegatiMail(java.lang.String,
	 *      java.lang.String, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<DetailDocumentRedDTO> getAllegatiMail(final String idFascicoloProcedimentale, final String documentTitle, final UtenteDTO utente) {
		List<DetailDocumentRedDTO> list = null;
		try {
			list = fascicoloSRV.getDocumentiByIdFascicoloPadre(Integer.valueOf(idFascicoloProcedimentale), utente);
			if (list == null || list.isEmpty()) {
				return list;
			}

			int index = -1;
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getDocumentTitle().equals(documentTitle)) {
					index = i;
					break;
				}
			}
			if (index > -1) {
				list.remove(index);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}
		return list;
	}

	/**
	 * @see it.ibm.red.business.service.IDocumentManagerSRV#getComboFormatoDocumentoByPermessiUtente(java.lang.Long).
	 */
	@Override
	public List<FormatoDocumentoEnum> getComboFormatoDocumentoByPermessiUtente(final Long permessi) {
		final List<FormatoDocumentoEnum> comboFormatiDocumento = new ArrayList<>();

		for (final FormatoDocumentoEnum formatoDocumentoEnum : FormatoDocumentoEnum.values()) {
			if (!(!PermessiUtils.hasPermesso(permessi, PermessiEnum.PRECENSIMENTO) && (formatoDocumentoEnum == FormatoDocumentoEnum.PRECENSITO))) {
				comboFormatiDocumento.add(formatoDocumentoEnum);
			}
		}

		return comboFormatiDocumento;
	}

	/**
	 * @see it.ibm.red.business.service.IDocumentManagerSRV#isComboFormatiDocumentoVisible(java.lang.String,
	 *      it.ibm.red.business.enums.CategoriaDocumentoEnum).
	 */
	@Override
	public boolean isComboFormatiDocumentoVisible(final String modalitaMaschera, final CategoriaDocumentoEnum categoria) {
		return !((!modalitaMaschera.equals(Modalita.MODALITA_INS)) // se non siamo in inserimento
				|| (categoria == CategoriaDocumentoEnum.USCITA) || (categoria == CategoriaDocumentoEnum.DOCUMENTO_USCITA) // o se non è una di queste categorie
				|| (categoria == CategoriaDocumentoEnum.INTERNO) || (categoria == CategoriaDocumentoEnum.MOZIONE) || (categoria == CategoriaDocumentoEnum.CONTRIBUTO_ESTERNO)
		// allora non va resa visibile
		);
//		PS: sembra complicato ma mi sono dovuto basare su tutte le volte che RED toglie questa combo: il negato quindi la mostra
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#getTipiProcedimentoByTipologiaDocumento(java.lang.Integer,
	 *      java.lang.Integer).
	 */
	@Override
	public final List<TipoProcedimento> getTipiProcedimentoByTipologiaDocumento(final Integer idTipologiaDocumento, final Integer idTipoProcedimento) {
		List<TipoProcedimento> tipoProcedimentoList = null;

		if (idTipologiaDocumento != null) {
			Connection connection = null;

			try {
				connection = setupConnection(getDataSource().getConnection(), false);

				tipoProcedimentoList = tipoProcedimentoDAO.getTipiProcedimentoByTipologiaDocumento(connection, idTipologiaDocumento);

				if (!CollectionUtils.isEmpty(tipoProcedimentoList)) {
					final Date now = new Date();

					tipoProcedimentoList = tipoProcedimentoList.stream() // MODIFICA OR (ATTIVA AND NOT AUTOMATICA)
							.filter(t -> (idTipoProcedimento != null && t.getTipoProcedimentoId() == idTipoProcedimento.longValue())
									|| ((t.getDatadisattivazione() == null || t.getDatadisattivazione().after(now))
											&& !TipoGestioneEnum.AUTOMATICA.equals(TipoGestioneEnum.get(t.getTipoGestione()))))
							.collect(Collectors.toList());
				}
			} catch (final SQLException e) {
				LOGGER.error(e.getMessage(), e);
			} finally {
				closeConnection(connection);
			}
		}

		return tipoProcedimentoList;
	}

	/**
	 * @see it.ibm.red.business.service.IDocumentManagerSRV#isMittentiVisible(it.ibm.red.business.enums.CategoriaDocumentoEnum).
	 */
	@Override
	public boolean isMittentiVisible(final CategoriaDocumentoEnum categoria) {
		// se non e' una di queste categorie allora va mostrato
		return !(categoria == CategoriaDocumentoEnum.USCITA || categoria == CategoriaDocumentoEnum.DOCUMENTO_USCITA || categoria == CategoriaDocumentoEnum.INTERNO
				|| categoria == CategoriaDocumentoEnum.MOZIONE || categoria == CategoriaDocumentoEnum.CONTRIBUTO_ESTERNO);
	}

	/**
	 * @see it.ibm.red.business.service.IDocumentManagerSRV#isDestinatariVisible(it.ibm.red.business.enums.CategoriaDocumentoEnum).
	 */
	@Override
	public boolean isDestinatariVisible(final CategoriaDocumentoEnum categoria) {
		return !(categoria == CategoriaDocumentoEnum.ENTRATA || categoria == CategoriaDocumentoEnum.DOCUMENTO_ENTRATA
				|| categoria == CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#filtraMezziRicezioneByFormatoDocumento(java.util.List,
	 *      it.ibm.red.business.enums.FormatoDocumentoEnum).
	 */
	@Override
	public List<MezzoRicezioneDTO> filtraMezziRicezioneByFormatoDocumento(final List<MezzoRicezioneDTO> listMezziRicezione, final FormatoDocumentoEnum f) {
		List<MezzoRicezioneDTO> list = new ArrayList<>();

		list.add(new MezzoRicezioneDTO(0, "", null));

		for (final MezzoRicezioneDTO mezzoRicezioneDTO : listMezziRicezione) {
			if (mezzoRicezioneDTO.getFormatoDocumento() == f) {
				list.add(mezzoRicezioneDTO);
			}
		}

		if (list.size() > 1) {
			final List<MezzoRicezioneDTO> mezzList = new ArrayList<>(list);
			// implementa Comparable
			Collections.sort(mezzList);
			list = mezzList;
		}

		return list;
	}

	/**
	 * Creo la lista di preferiti compresi i contatti che si trovano già nel
	 * dettaglio, altrimenti la combo non e' capace di gestirli IMPORTANTE: questa
	 * lista deve essere incrementata di ogni contatto che viene selezionato dalla
	 * rubrica se non e' gia' presente (se il contatto non e' contenuto in questa
	 * lista non può essere selezionato lato front end perche' e' una select).
	 * 
	 * @param connection
	 * @param utente
	 * @param detail
	 * @return
	 */
	private List<Contatto> gestisciContattiPreferitiPerComboDestinatario(final Connection connection, final UtenteDTO utente, final DetailDocumentRedDTO detail) {

		List<Contatto> contattiPreferitiSenzaGruppi = new ArrayList<>();
		final List<Contatto> contattiPreferitiTutti = contattoDAO.getContattiPreferiti(utente.getIdUfficio(), utente.getIdAoo(), true, connection);

		if (detail.getContattiPreferitiSenzaGruppi()) {
			for (final Contatto contatto : contattiPreferitiTutti) {
				contatto.setIsPreferito(true);
				if (contatto.getTipoRubricaEnum() != TipoRubricaEnum.GRUPPO) {
					contattiPreferitiSenzaGruppi.add(contatto);
				}
			}
		} else {
			contattiPreferitiSenzaGruppi = contattiPreferitiTutti;
		}

		// se esistono destinatari che non fanno parte dei preferiti devo aggiungerli
		// alla lista
		if (detail.getDestinatari() != null && !detail.getDestinatari().isEmpty()) {
			Contatto cToCheck = null;
			boolean aggiungi = true;
			for (final DestinatarioRedDTO d : detail.getDestinatari()) {
				cToCheck = d.getContatto();

				if (cToCheck == null || cToCheck.getContattoID() == null) { // allora è un contatto interno e non va aggiunto
					continue;
				}

				aggiungi = true;
				for (final Contatto contatto : contattiPreferitiSenzaGruppi) {
					if (contatto.getContattoID().equals(cToCheck.getContattoID())) {
						aggiungi = false;
						break;
					}
				}

				if (aggiungi) {
					contattiPreferitiSenzaGruppi.add(cToCheck);
				}
			}
		}
		return contattiPreferitiSenzaGruppi;

	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#getContattiPreferitiPerComboDestinatario(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.DetailDocumentRedDTO).
	 */
	@Override
	public List<Contatto> getContattiPreferitiPerComboDestinatario(final UtenteDTO utente, final DetailDocumentRedDTO detail) {

		Connection connection = null;
		List<Contatto> list = new ArrayList<>();
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			list = this.gestisciContattiPreferitiPerComboDestinatario(connection, utente, detail);
		} catch (final SQLException e) {
			LOGGER.error(e.getMessage(), e);
		} finally {
			closeConnection(connection);
		}

		return list;
	}

	/**
	 * Inizializza la combo box della riservatezza restituendone gli item.
	 * 
	 * @return Item dell'enum @see RiservatezzaEnum.
	 */
	private static List<RiservatezzaEnum> initComboRiservatezza() {
		final List<RiservatezzaEnum> list = new ArrayList<>();
		list.addAll(Arrays.asList(RiservatezzaEnum.values()));
		
		return list;
	}

	/**
	 * @see it.ibm.red.business.service.IDocumentManagerSRV#isDaNonProtocollareVisible(boolean,
	 *      it.ibm.red.business.enums.CategoriaDocumentoEnum).
	 */
	@Override
	public boolean isDaNonProtocollareVisible(final boolean isDocInModifica, final CategoriaDocumentoEnum categoria) {
		return !(categoria == CategoriaDocumentoEnum.USCITA || categoria == CategoriaDocumentoEnum.DOCUMENTO_USCITA || categoria == CategoriaDocumentoEnum.INTERNO
				|| categoria == CategoriaDocumentoEnum.MOZIONE || categoria == CategoriaDocumentoEnum.CONTRIBUTO_ESTERNO || isDocInModifica);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#isIterDocUscitaVisible(it.ibm.red.business.enums.CategoriaDocumentoEnum).
	 */
	@Override
	public boolean isIterDocUscitaVisible(final CategoriaDocumentoEnum categoria) {
		return !(categoria == CategoriaDocumentoEnum.ENTRATA || categoria == CategoriaDocumentoEnum.DOCUMENTO_ENTRATA
				|| categoria == CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#verificaAllaccioProtocollo(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.RispostaAllaccioDTO,
	 *      it.ibm.red.business.enums.TipoCategoriaEnum).
	 */
	@Override
	public EsitoVerificaProtocolloDTO verificaAllaccioProtocollo(final UtenteDTO utente, final RispostaAllaccioDTO r, final TipoCategoriaEnum t) {
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;
		Connection connection = null;
		Connection connectionDWH = null;
		DetailDocumentRedDTO detailDocument = null;
		final EsitoVerificaProtocolloDTO esito = new EsitoVerificaProtocolloDTO();
		esito.setEntrata(TipoCategoriaEnum.ENTRATA.equals(t));
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			
			final Document docFilenet = documentoRedSRV.verificaProtocollo(r.getAnnoProtocollo(), r.getNumeroProtocollo(), t, utente.getIdAoo(), fceh, connection);
			if (docFilenet == null) {
				esito.setNote("Il protocollo " + r.getNumeroProtocollo() + "/" + r.getAnnoProtocollo() + " non è stato trovato nel sistema");
				esito.setEntrata(!TipoCategoriaEnum.ENTRATA.equals(t));
				return esito;
			}
			detailDocument = TrasformCE.transform(docFilenet, TrasformerCEEnum.FROM_DOCUMENTO_TO_PROTOCOLLO_DA_VERIFICARE);

			if (TipoCategoriaEnum.ENTRATA.equals(t)) {
				final VWRosterQuery query = fpeh.getRosterQueryWorkByIdDocumentoAndCodiceAOO(detailDocument.getDocumentTitle(), utente.getFcDTO().getIdClientAoo());
				final List<AssegnazioneDTO> assegnazioniList = fpeh.getAssegnazioni(detailDocument.getDocumentTitle(), utente.getCodiceAoo(), query);
				detailDocument.setAssegnazioni(assegnazioniList);
				
				boolean hasCompetenza = false;
				boolean zeroCompetenze = true;
				
				if(assegnazioniList != null && !assegnazioniList.isEmpty()) {
					for (final AssegnazioneDTO assegnazioneDTO : assegnazioniList) {
						
						zeroCompetenze = zeroCompetenze && assegnazioneDTO.getTipoAssegnazione() != TipoAssegnazioneEnum.COMPETENZA;
						if (assegnazioneDTO.getTipoAssegnazione() == TipoAssegnazioneEnum.COMPETENZA
								&& utente.getIdUfficio().equals(assegnazioneDTO.getUfficio().getId())
								&& assegnazioneDTO.getUtente() != null 
								&& utente.getId().equals(assegnazioneDTO.getUtente().getId())) {
							hasCompetenza = true;
							break;
						}
						
					}
				}
				
				if(zeroCompetenze) {
					connectionDWH = setupConnection(getFilenetDataSource().getConnection(), false);
					final EventoLogDTO event = eventDAO.getLastChiusuraIngressoEvent(connectionDWH, Integer.valueOf(detailDocument.getDocumentTitle()), utente.getIdAoo().intValue());
					if (event == null || event.getIdUfficioMittente().intValue() != utente.getIdUfficio().intValue()) {
						esito.setNote(ERROR_DOCUMENTO_NON_ALLACCIABILE_MSG);
						return esito;
					} else {
						hasCompetenza = true;
					}
					r.setChiusuraDisabled(true);
					r.setDocumentoDaChiudere(true);
				}
				
				if (!hasCompetenza) {
					esito.setNote(ERROR_DOCUMENTO_NON_ALLACCIABILE_MSG);
					return esito;
				}

				/** FASCICOLO PROCEDIMENTALE START*****************************************************************************************/
				final FascicoloDTO f = fascicoloSRV.getFascicoloProcedimentale(detailDocument.getDocumentTitle(), utente.getIdAoo().intValue(), utente);
				if (f == null) {
					esito.setNote(ERROR_DOCUMENTO_NON_ALLACCIABILE_MSG);
					return esito;
				}
	
				//mi serve per sapere se il fascicolo procedimentale è chiuso
				detailDocument.setFascicoli(new ArrayList<>());
				detailDocument.getFascicoli().add(f);
				/** FASCICOLO PROCEDIMENTALE END*******************************************************************************************/
			}
			
			esito.setEsito(true);
			esito.setDocument(detailDocument);

		} catch (final SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
			closeConnection(connectionDWH);
			popSubject(fceh);
			logoff(fpeh);
		}

		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#isBarcodePanelVisible(java.lang.String,
	 *      it.ibm.red.business.enums.CategoriaDocumentoEnum,
	 *      it.ibm.red.business.enums.FormatoDocumentoEnum).
	 */
	@Override
	public boolean isBarcodePanelVisible(final String modalitaMaschera, final CategoriaDocumentoEnum categoria, final FormatoDocumentoEnum f) {
		return (f != null && f == FormatoDocumentoEnum.CARTACEO) && !(categoria == CategoriaDocumentoEnum.USCITA || categoria == CategoriaDocumentoEnum.DOCUMENTO_USCITA
				|| categoria == CategoriaDocumentoEnum.INTERNO || categoria == CategoriaDocumentoEnum.MOZIONE || categoria == CategoriaDocumentoEnum.CONTRIBUTO_ESTERNO);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#isRdpPanelVisible(it.ibm.red.business.enums.CategoriaDocumentoEnum,
	 *      java.lang.String).
	 */
	@Override
	public boolean isRdpPanelVisible(final CategoriaDocumentoEnum categoria, final String descTipoProcedimento) {
		return categoria != CategoriaDocumentoEnum.DOCUMENTO_ENTRATA && "PRELEX".equalsIgnoreCase(descTipoProcedimento);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#isDocPrincipalePanelVisible(java.lang.String,
	 *      it.ibm.red.business.enums.FormatoDocumentoEnum,
	 *      it.ibm.red.business.enums.CategoriaDocumentoEnum, java.util.Collection).
	 */
	@Override
	public boolean isDocPrincipalePanelVisible(final String modalitaMaschera, final FormatoDocumentoEnum f, final CategoriaDocumentoEnum categoria,
			final Collection<StoricoDTO> storico) {
		return (f == null || f == FormatoDocumentoEnum.ELETTRONICO);

	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#isTemplateButtonVisible(java.lang.String,
	 *      it.ibm.red.business.enums.CategoriaDocumentoEnum).
	 */
	@Override
	public boolean isTemplateButtonVisible(final String modalitaMaschera, final CategoriaDocumentoEnum categoria) {
		return (Modalita.MODALITA_INS.equals(modalitaMaschera)
				&& (categoria == CategoriaDocumentoEnum.MOZIONE || categoria == CategoriaDocumentoEnum.DOCUMENTO_USCITA || categoria == CategoriaDocumentoEnum.USCITA));
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#isVerificaFirmaDocPrincipaleVisible(java.lang.String).
	 */
	@Override
	public boolean isVerificaFirmaDocPrincipaleVisible(final String mimeType) {
		return FileUtils.isP7MFile(mimeType);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#isFlagFirmaCopiaConformeVisible(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.enums.TipoAssegnazioneEnum).
	 */
	@Override
	public boolean isFlagFirmaCopiaConformeVisible(final UtenteDTO utente, final TipoAssegnazioneEnum t) {
		return t == TipoAssegnazioneEnum.FIRMA && utente.isPermessoAooFirmaCopiaConforme();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#isIndiceDiClassificazioneVisible(it.ibm.red.business.enums.FormatoDocumentoEnum).
	 */
	@Override
	public boolean isIndiceDiClassificazioneVisible(final FormatoDocumentoEnum f) {
		return f == null || f != FormatoDocumentoEnum.PRECENSITO;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#isAssegnatarioPerCompetenzaVisible(it.ibm.red.business.enums.CategoriaDocumentoEnum).
	 */
	@Override
	public boolean isAssegnatarioPerCompetenzaVisible(final CategoriaDocumentoEnum categoria) {
		return !(categoria == CategoriaDocumentoEnum.USCITA || categoria == CategoriaDocumentoEnum.DOCUMENTO_USCITA || categoria == CategoriaDocumentoEnum.INTERNO
				|| categoria == CategoriaDocumentoEnum.MOZIONE || categoria == CategoriaDocumentoEnum.CONTRIBUTO_ESTERNO);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#componiDescrizioneFascicolo(it.ibm.red.business.dto.DetailDocumentRedDTO).
	 */
	@Override
	public String componiDescrizioneFascicolo(final DetailDocumentRedDTO detail) {
		final String descTipologiadocumento = StringUtils.isNullOrEmpty(detail.getDescTipologiaDocumento()) ? "" : detail.getDescTipologiaDocumento().trim() + " - ";
		final String oggetto = StringUtils.isNullOrEmpty(detail.getOggetto()) ? "" : detail.getOggetto().trim();
		return descTipologiadocumento + oggetto;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#isProtocolloMittenteRispostaVisible(it.ibm.red.business.enums.CategoriaDocumentoEnum,
	 *      it.ibm.red.business.enums.FormatoDocumentoEnum).
	 */
	@Override
	public boolean isProtocolloMittenteRispostaVisible(final CategoriaDocumentoEnum categoria, final FormatoDocumentoEnum f) {
		return !(categoria == CategoriaDocumentoEnum.USCITA || categoria == CategoriaDocumentoEnum.DOCUMENTO_USCITA || categoria == CategoriaDocumentoEnum.INTERNO // OR
																																									// INTERNO
																																									// NSD
				|| categoria == CategoriaDocumentoEnum.MOZIONE || categoria == CategoriaDocumentoEnum.CONTRIBUTO_ESTERNO || f == FormatoDocumentoEnum.PRECENSITO);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#isRaccoltaFadVisible(it.ibm.red.business.dto.DetailDocumentRedDTO).
	 */
	@Override
	public boolean isRaccoltaFadVisible(final DetailDocumentRedDTO detail) {
		final Integer idAttoDecreto = Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_TIPO_DOC_VALUE));
		return (detail.getNumeroProtocollo() != null && detail.getNumeroProtocollo().intValue() > 0) && (detail.getDataProtocollo() != null)
				&& (detail.getAnnoProtocollo() != null) && idAttoDecreto.equals(detail.getIdTipologiaDocumento());
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#creaRaccoltaFadManuale(it.ibm.red.business.dto.DetailDocumentRedDTO,
	 *      it.ibm.red.business.dto.FadAmministrazioneDTO,
	 *      it.ibm.red.business.dto.FadRagioneriaDTO,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public String creaRaccoltaFadManuale(final DetailDocumentRedDTO detail, final FadAmministrazioneDTO f, final FadRagioneriaDTO r, final UtenteDTO utente) {
		try {
			final RaccoltaFadDTO fdaDTO = new RaccoltaFadDTO();
			fdaDTO.setAmministrazioneFad(f.getCodiceAmministrazione());
			fdaDTO.setAmministrazioneFadLabel(f.getDescrizioneAmministrazione());
			fdaDTO.setRagioneriaFad(r.getCodiceRagioneria());
			fdaDTO.setRagioneriaFadLabel(r.getDescrizioneRagioneria());

			fdaDTO.setAnnoDocumento(detail.getAnnoDocumento());
			fdaDTO.setClasseDocumentale(detail.getDocumentClass());

			fdaDTO.setDataProtocolloFromManuale(detail.getDataProtocollo());
			fdaDTO.setDescrizioneNodo(detail.getUfficioMittente().getDescrizione());
			fdaDTO.setDocumentTitle(detail.getDocumentTitle());
			fdaDTO.setIdFascicolo(detail.getIdFascicoloProcedimentale());

			fdaDTO.setNumeroProtocollo(String.valueOf(detail.getNumeroProtocollo()));
			fdaDTO.setAnnoProtocollo(String.valueOf(detail.getAnnoProtocollo()));
			fdaDTO.setOggetto(detail.getOggetto());
			fdaDTO.setPrefixFascicoloLabel(detail.getPrefixFascicoloProcedimentale());
			fdaDTO.setWobNumber(detail.getWobNumberSelected());

			final boolean processoManuale = true; // essendo chiamato dalla maschera di modifica di un documento e' per forza
													// manuale

			return fepaSRV.creaRaccoltaFad(fdaDTO, processoManuale, utente);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#getUfficioUCR(java.lang.Long).
	 */
	@Override
	public UfficioDTO getUfficioUCR(final Long idAOO) {

		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return nodoDAO.getUfficioUCR(idAOO, connection);
		} catch (final SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#isResponsabiliCopiaConformeForAllegatoVisible(it.ibm.red.business.enums.CategoriaDocumentoEnum,
	 *      boolean).
	 */
	@Override
	public boolean isResponsabiliCopiaConformeForAllegatoVisible(final CategoriaDocumentoEnum categoria, final boolean isComboResponsabileCopiaConformeEmpty) {

		return (!isComboResponsabileCopiaConformeEmpty
				&& (categoria == CategoriaDocumentoEnum.USCITA || categoria == CategoriaDocumentoEnum.DOCUMENTO_USCITA || categoria == CategoriaDocumentoEnum.MOZIONE));
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#isAllegatiCheckDaFirmareVisible(it.ibm.red.business.enums.CategoriaDocumentoEnum).
	 */
	@Override
	public boolean isAllegatiCheckDaFirmareVisible(final CategoriaDocumentoEnum categoria) {

		return (categoria == CategoriaDocumentoEnum.USCITA || categoria == CategoriaDocumentoEnum.DOCUMENTO_USCITA || categoria == CategoriaDocumentoEnum.MOZIONE);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#selectResponsabileById(java.lang.String,
	 *      java.util.List).
	 */
	@Override
	public ResponsabileCopiaConformeDTO selectResponsabileById(final String id, final List<ResponsabileCopiaConformeDTO> list) {
		ResponsabileCopiaConformeDTO result = null;
		if (StringUtils.isNullOrEmpty(id)) {
			throw new RedException("Nessun responsabile selezionato.");
		}

		final String[] valori = id.split(" ");
		Long idNodo = null;
		Long idUtente = null;

		switch (valori.length) {
		case 0:
			throw new RedException("Nessun responsabile selezionato.");
		case 1:
			idNodo = Long.valueOf(valori[0]);
			break;
		default: // 2
			idNodo = Long.valueOf(valori[0]);
			idUtente = Long.valueOf(valori[1]);
			break;
		}

		for (final ResponsabileCopiaConformeDTO r : list) {
			if (r.getIdNodo().longValue() == idNodo.longValue() && idUtente != null && r.getIdUtente().longValue() == idUtente.longValue()) {
				result = r;
				break;
			}
		}
		return result;
	}

	private static String getPrefixOggettoMail(final DetailDocumentRedDTO detail, final String codiceAoo) {
		String prefix = "MEF - " + codiceAoo;

		if (detail.getDocumentTitle() != null) {
			final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			if (detail.getNumeroProtocollo() != null && detail.getNumeroProtocollo().intValue() > 0 && detail.getAnnoProtocollo() != null
					&& detail.getAnnoProtocollo().intValue() > 0) {

				prefix += " Prot. " + detail.getNumeroProtocollo().intValue() + "/" + detail.getAnnoProtocollo().intValue() + " del " + sdf.format(detail.getDataProtocollo());
			} else if (detail.getNumeroDocumento() != null && detail.getNumeroDocumento().intValue() > 0 && detail.getAnnoDocumento() != null
					&& detail.getAnnoDocumento().intValue() > 0) {
				prefix += " Documento " + detail.getNumeroDocumento().intValue() + "/" + detail.getAnnoDocumento().intValue();
			}

			if (detail.getNumeroCircolare() != null && detail.getNumeroCircolare().intValue() > 0 && detail.getAnnoProtocollo() != null
					&& detail.getAnnoProtocollo().intValue() > 0) {
				prefix += " - Num Circolare: " + detail.getNumeroCircolare();
			}

		} else {
			prefix += " - N. PROTOCOLLO";
		}

		return prefix;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#getStreamDocumentoByGuid(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public InputStream getStreamDocumentoByGuid(final String guid, final UtenteDTO utente) {
		InputStream is = null;
		IFilenetCEHelper fcehAdmin = null;

		try {

			final FilenetCredentialsDTO fcAdmin = new FilenetCredentialsDTO(utente.getFcDTO());
			fcehAdmin = FilenetCEHelperProxy.newInstance(fcAdmin, utente.getIdAoo());

			final Document d = fcehAdmin.getDocumentByGuid(guid);
			is = FilenetCEHelper.getDocumentContentAsInputStream(d);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fcehAdmin);
		}

		return is;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#getDocumentByGuid(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Document getDocumentByGuid(final String guid, final UtenteDTO utente) {
		Document d = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			d = fceh.getDocumentByGuid(guid);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}

		return d;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#getDocContent(com.filenet.api.core.Document).
	 */
	@Override
	public byte[] getDocContent(final Document d) {
		byte[] doc = null;
		try {
			doc = FilenetCEHelper.getDocumentContentAsByte(d);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}
		return doc;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#getDocumentForAppletByGuid(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public DocumentAppletContentDTO getDocumentForAppletByGuid(final String guid, final UtenteDTO utente) {
		DocumentAppletContentDTO dCont = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final Document d = fceh.getDocumentByGuid(guid);

			final ContentTransfer content = FilenetCEHelper.getDocumentContentTransfer(d);
			final InputStream isTemplate = content.accessContentStream();
			final String strBase64 = URLEncoder.encode(FileUtils.encodeBase64(isTemplate), StandardCharsets.UTF_8.name());

			String contentType = d.get_MimeType();
			if (d.get_Name().endsWith("x")) {
				contentType = MediaType.MICROSOFT_WORD.toString();
			}

			final String nomeTemplate = JavaScriptUtils.javaScriptEscape(d.get_Name());

			dCont = new DocumentAppletContentDTO(nomeTemplate, contentType, strBase64);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}

		return dCont;

	}

	/**
	 * @see it.ibm.red.business.service.IDocumentManagerSRV#isTabSpedizioneVisible(it.ibm.red.business.enums.CategoriaDocumentoEnum).
	 */
	@Override
	public boolean isTabSpedizioneVisible(final CategoriaDocumentoEnum categoria) {
		return CategoriaDocumentoEnum.USCITA.equals(categoria) || CategoriaDocumentoEnum.DOCUMENTO_USCITA.equals(categoria) || CategoriaDocumentoEnum.MOZIONE.equals(categoria)
				|| CategoriaDocumentoEnum.CONTRIBUTO_ESTERNO.equals(categoria);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#comboCoordinatoriVisible(java.lang.Integer,
	 *      java.util.List).
	 */
	@Override
	public boolean comboCoordinatoriVisible(final Integer i, final List<NodoUtenteCoordinatore> comboCoordinatori) {

		if (comboCoordinatori != null && !comboCoordinatori.isEmpty() && i != IterApprovativoDTO.ITER_FIRMA_MANUALE.intValue()) {

			Connection connection = null;
			try {
				connection = setupConnection(getDataSource().getConnection(), false);
				final IterApprovativoDTO iter = iterApprovativoDAO.getIterApprovativoById(i, connection);
				return iter.isIterAutomaticoMoreThanDirigente();
			} catch (final Exception e) {
				LOGGER.error(e.getMessage(), e);
				throw new RedException(e);
			} finally {
				closeConnection(connection);
			}

		}

		return false;

	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#getUtenteProtocollatoreMailByGuidMail(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public UtenteDTO getUtenteProtocollatoreMailByGuidMail(final String mailGuid, final UtenteDTO utente) {
		UtenteDTO utenteProtocollatore = null;
		Connection connection = null;
		IFilenetCEHelper fceh = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final PropertiesProvider pp = PropertiesProvider.getIstance();
			final String[] metadati = new String[] { pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_PROTOCOLLATORE_METAKEY),
					pp.getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY) };
			final Document mail = fceh.getDocumentByGuid(mailGuid, metadati);
			final Long currentIdUtenteProtocollatore = Long.valueOf((Integer) TrasformerCE.getMetadato(mail, PropertiesNameEnum.ID_UTENTE_PROTOCOLLATORE_METAKEY));

			utenteProtocollatore = new UtenteDTO(utenteDAO.getUtente(currentIdUtenteProtocollatore, connection));
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}

		return utenteProtocollatore;
	}

	private static boolean isIndiceDiClassificazioneRequired(final CategoriaDocumentoEnum categoria) {
		return !(categoria == CategoriaDocumentoEnum.ENTRATA || categoria == CategoriaDocumentoEnum.DOCUMENTO_ENTRATA
				|| categoria == CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#isTabAssegnazioniDisabled(java.lang.Integer,
	 *      it.ibm.red.business.enums.CategoriaDocumentoEnum).
	 */
	@Override
	public boolean isTabAssegnazioniDisabled(final Integer idIterApprovativo, final CategoriaDocumentoEnum categoria) {
		return !(categoria == CategoriaDocumentoEnum.ENTRATA || categoria == CategoriaDocumentoEnum.DOCUMENTO_ENTRATA
				|| categoria == CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA) || IterApprovativoDTO.ITER_FIRMA_MANUALE.equals(idIterApprovativo);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#isDocPrincipaleRimuovibile(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.DetailDocumentRedDTO,
	 *      it.ibm.red.business.dto.DocumentManagerDTO).
	 */
	@Override
	public boolean isDocPrincipaleRimuovibile(final UtenteDTO utente, final DetailDocumentRedDTO detail, final DocumentManagerDTO documentManagerDTO) {
		boolean isRimuovibile = !documentManagerDTO.isInModifica();

		LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] documentManagerDTO.isInModifica() " + documentManagerDTO.isInModifica());

		if (isRimuovibile) {
			// Se sono in creazione devo poterlo modificare
			return isRimuovibile;
		}

		// Altrimenti se sono in MODIFICA
		LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] documentManagerDTO.getCategoria() " + documentManagerDTO.getCategoria());

		if (CategoriaDocumentoEnum.USCITA == documentManagerDTO.getCategoria() || CategoriaDocumentoEnum.MOZIONE == documentManagerDTO.getCategoria()
				|| CategoriaDocumentoEnum.DOCUMENTO_USCITA == documentManagerDTO.getCategoria()) {
			// Se è un documento in USCITA!!!

			boolean isUltimo = detail.getStorico().size() == 1;
			boolean isFirmato = false;
			boolean isFirmatoFirmaMultipla = false;
			String descrUfficio = null;

			final List<StoricoDTO> list = new ArrayList<>(detail.getStorico());
			for (int index = 0; index < list.size(); index++) {

				if (EventTypeEnum.FIRMATO_FIRMA_MULTIPLA.getIntValue().equals(list.get(index).getIdTipoOperazione())) {
					isFirmatoFirmaMultipla = true;
				} else if (EventTypeEnum.FIRMATO.getIntValue().equals(list.get(index).getIdTipoOperazione())
						|| EventTypeEnum.FIRMATO_SPEDITO.getIntValue().equals(list.get(index).getIdTipoOperazione())) {
					isFirmato = true;
					isUltimo = (index == list.size() - 1);
					descrUfficio = list.get(index).getUfficioAssegnatario();
					break;
				}
			}

			LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] isFirmatoFirmaMultipla " + isFirmatoFirmaMultipla);
			LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] isFirmato " + isFirmato);
			LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] isUltimo " + isUltimo);
			LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] descrUfficio " + descrUfficio);
			LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] DESC_UFF_UCRA PROPERTIES "
					+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DESC_UFF_UCRA));

			// È rimuovibile se:
			// a) L'ultimo evento è una firma standard e l'assegnatario è l'UCRA (gestione
			// firma autografa), oppure
			// b) Il documento non è firmato né in modo standard né in firma multipla
			if ((isFirmato && isUltimo && PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DESC_UFF_UCRA).equalsIgnoreCase(descrUfficio))
					|| (!isFirmato && !isFirmatoFirmaMultipla)) {
				isRimuovibile = true;
			}

		} else {
			// Se è un documento in INGRESSO!!!
			LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] documentManagerDTO.isFlagProtocollaMail() " + documentManagerDTO.isFlagProtocollaMail());
			LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] detail.getNumeroProtocollo() " + detail.getNumeroProtocollo());
			isRimuovibile = (!documentManagerDTO.isFlagProtocollaMail() && (detail.getNumeroProtocollo() == null || detail.getNumeroProtocollo().intValue() <= 0)
					&& isUtenteAssegnatarioPrincipale(detail, utente));

		}

		LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] isDocPrincipaleRimuovibile() " + isRimuovibile);

		return isRimuovibile;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#isDocFirmato(it.ibm.red.business.dto.DetailDocumentRedDTO,
	 *      it.ibm.red.business.dto.DocumentManagerDTO).
	 */
	@Override
	public boolean isDocFirmato(final DetailDocumentRedDTO detail, final DocumentManagerDTO documentManagerDTO) {

		LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] documentManagerDTO.isInModifica() " + documentManagerDTO.isInModifica());

		if (!documentManagerDTO.isInModifica()) {
			// Se sono in creazione non è firmato
			return false;
		}

		// Altrimenti se sono in MODIFICA

		LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] documentManagerDTO.getCategoria() " + documentManagerDTO.getCategoria());

		if (CategoriaDocumentoEnum.USCITA == documentManagerDTO.getCategoria() || CategoriaDocumentoEnum.MOZIONE == documentManagerDTO.getCategoria()
				|| CategoriaDocumentoEnum.DOCUMENTO_USCITA == documentManagerDTO.getCategoria()) {
			// Se è un documento in USCITA!!!

			final List<StoricoDTO> list = new ArrayList<>(detail.getStorico());
			for (int i = 0; i < list.size(); i++) {

				LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] storico " + list.get(i).getIdTipoOperazione());

				if (EventTypeEnum.FIRMATO.getIntValue().equals(list.get(i).getIdTipoOperazione())
						|| EventTypeEnum.FIRMATO_SPEDITO.getIntValue().equals(list.get(i).getIdTipoOperazione())
						|| EventTypeEnum.FIRMATO_FIRMA_MULTIPLA.getIntValue().equals(list.get(i).getIdTipoOperazione())) {
					LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] isDocFirmato true");
					return true;
				}

			}
		}

		LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] isDocFirmato false");
		return false;
	}

	/**
	 * Verifico che esista un assegnatario principale, in tal caso verifico che
	 * l'assegnatario principale corrisponda all'utente.
	 * 
	 * @param utente
	 * @param detail
	 * @return
	 */
	private static boolean isUtenteAssegnatarioPrincipale(final DetailDocumentRedDTO detail, final UtenteDTO utente) {
		boolean isRimuovibile = true;
		AssegnazioneDTO principale = null;
		if (detail.getWobNumberPrincipale() != null) { // caso atti
			for (final AssegnazioneDTO a : detail.getAssegnazioni()) {
				if (detail.getWobNumberPrincipale().equals(a.getWobNumber())) {
					principale = a;
					break;
				}
			}

			LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] assegnatario principale " + principale);

			// Significa: se l'assegnazione principale non ha lo stesso ufficio dell'utente
			// oppure l'utente dell'assegnazione (se c'è) non è l'utente loggato
			// non devo poter rimuovere il content
			if (principale != null && !(principale.getUfficio().getId().equals(utente.getIdUfficio())
					&& (principale.getUtente() == null || principale.getUtente().getId().equals(utente.getId())))) {
				isRimuovibile = false;
			}
		}
		return isRimuovibile;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#checkUploadFilePrincipale(it.ibm.red.business.dto.FileDTO,
	 *      it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.DocumentManagerDTO,
	 *      it.ibm.red.business.enums.TipoAssegnazioneEnum, java.lang.String,
	 *      boolean).
	 */
	@Override
	public EsitoDTO checkUploadFilePrincipale(final FileDTO f, final UtenteDTO utente, final DocumentManagerDTO dm, final TipoAssegnazioneEnum tipoAssegnazione,
			final String classDocMainDoc, final boolean maxSizeRispettate) {
		final EsitoDTO esito = new EsitoDTO();
		esito.setEsito(true);

		try {
			// Controllo se il file è vuoto
			if (f.getContent() == null || f.getContent().length == 0) {
				esito.setNote("Il file è vuoto.");
				esito.setEsito(false);
				return esito;
			}

			// controllo estensioni
			final String[] extSplit = f.getFileName().split("\\.");
			if (extSplit.length == 0) {
				esito.setNote("Il file non ha nessuna estensione.");
				esito.setEsito(false);
				return esito;
			}
			final String ext = extSplit[extSplit.length - 1];
			if (!dm.getEstensioniAmmesse().contains(ext)) {
				esito.setNote("Estensione del file non ammessa: '" + ext + "'.");
				esito.setEsito(false);
				return esito;
			}

			if (((dm.isInCreazioneUscita() || dm.isInModificaUscita() || CategoriaDocumentoEnum.CONTRIBUTO_ESTERNO.equals(dm.getCategoria())) && !maxSizeRispettate)
					|| ((dm.isInCreazioneIngresso() || dm.isInModificaIngresso()) && utente.getMaxSizeEntrata() != 0 && f.getContent().length > utente.getMaxSizeEntrata())) {
				esito.setNote("La dimensione dei file contenuti nel procedimento supera la dimensione massima consentita dalla casella di posta mittente.");
				esito.setEsito(false);
				return esito;
			}

			final boolean isPDF = "pdf".equalsIgnoreCase(ext);
			final Aoo aoo = aooSRV.recuperaAoo(utente.getIdAoo().intValue());

			final ByteArrayInputStream bisContent = new ByteArrayInputStream(f.getContent());
			boolean hasSignedField = false;
			if (isPDF) {
				final IAdobeLCHelper adobeHelper = new AdobeLCHelper();
				hasSignedField = FileUtils.hasEmptySignFields(bisContent, adobeHelper);
			}

			final PropertiesProvider pp = PropertiesProvider.getIstance();
			// Se DSR
			if (pp.getParameterByKey(PropertiesNameEnum.DSR_CLASSNAME_FN_METAKEY).equals(classDocMainDoc)) {
				if (!isPDF || !hasSignedField) {
					esito.setNote("La Dichiarazione dei Servizi Resi deve contenere un campo firma vuoto.");
					esito.setEsito(false);
					return esito;
				}
				// Se non DSR, i seguenti controlli sono eseguiti solo se:
				// a) l'AOO NON ha configurato il PK Handler per i Timbri, oppure
				// b) l'iter selezionato è manuale per Spedizione
			} else if (aoo.getPkHandlerTimbro() == null) {
				boolean isSigned = "p7m".equalsIgnoreCase(ext);

				if (!isSigned && isPDF && (dm.isInCreazioneUscita() || dm.isInModificaUscita() || CategoriaDocumentoEnum.CONTRIBUTO_ESTERNO.equals(dm.getCategoria()))) {
					bisContent.reset();
					isSigned = signSRV.hasSignatures(bisContent, utente);
				}

				if (hasSignedField) {
					esito.setNote("Il documento principale contiene un campo firma. Selezionare un documento senza il campo firma.");
					esito.setEsito(false);
					return esito;
				} else if (isSigned && (dm.isInCreazioneUscita() || dm.isInModificaUscita() || CategoriaDocumentoEnum.INTERNO.equals(dm.getCategoria())
						|| CategoriaDocumentoEnum.CONTRIBUTO_ESTERNO.equals(dm.getCategoria()))) {
					esito.setNote("Il documento principale risulta firmato digitalmente. Selezionare un documento non firmato.");
					esito.setEsito(false);
					return esito;
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}

		return esito;

	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#checkUploadFileAllegato(it.ibm.red.business.dto.FileDTO,
	 *      it.ibm.red.business.dto.AllegatoDTO, it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.DocumentManagerDTO,
	 *      it.ibm.red.business.enums.FormatoAllegatoEnum).
	 */
	@Override
	public EsitoDTO checkUploadFileAllegato(final FileDTO f, final AllegatoDTO allegato, final UtenteDTO utente, final DocumentManagerDTO dm,
			final FormatoAllegatoEnum formato) {

		final EsitoDTO esito = new EsitoDTO();
		esito.setEsito(true);

		try {
			if (StringUtils.isNullOrEmpty(f.getMimeType())) {
				esito.setNote("Estensione del file non presente.");
				esito.setEsito(false);
				return esito;

			}
			final String[] keys = f.getFileName().split("\\.");
			final String ext = keys[keys.length - 1];
			if (!dm.getEstensioniAll().contains(ext)) {
				esito.setNote("Estensione del file non permessa.");
				esito.setEsito(false);
				return esito;
			}
			if (formato == FormatoAllegatoEnum.FIRMATO_DIGITALMENTE && !signSRV.hasSignatures(new ByteArrayInputStream(f.getContent()), utente)) {
				allegato.setFormatoSelected(FormatoAllegatoEnum.ELETTRONICO);
				allegato.setFormato(FormatoAllegatoEnum.ELETTRONICO.getId());
				return esito;
			}
			if (formato == FormatoAllegatoEnum.ELETTRONICO
					&& ("p7m".equalsIgnoreCase(ext) || ("pdf".equalsIgnoreCase(ext) && signSRV.hasSignatures(new ByteArrayInputStream(f.getContent()), utente)))) {
				allegato.setFormatoSelected(FormatoAllegatoEnum.FIRMATO_DIGITALMENTE);
				allegato.setFormato(FormatoAllegatoEnum.FIRMATO_DIGITALMENTE.getId());
				esito.setNote("Uno o più file risultano firmati digitalmente. Per tali file è stato selezionato il formato FIRMATO DIGITALMENTE");
				return esito;
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}

		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#checkDestinatariInterni(it.ibm.red.business.dto.DetailDocumentRedDTO,
	 *      java.util.List, it.ibm.red.business.dto.DocumentManagerDTO).
	 */
	@Override
	public void checkDestinatariInterni(final DetailDocumentRedDTO detail, final List<DestinatarioRedDTO> destinatariInModifica, final DocumentManagerDTO documentManagerDTO) {
		// Se esiste tutti i destinatari sono interni la protocollazione deve essere
		// STANDARD e disabilitata perché non può essere cambiata (escluso iter
		// spedizione dove rimane non standard e non accetta destinatari interni in fase
		// di registrazione)

		if (detail == null || documentManagerDTO == null) {
			return;
		}

		if ((detail.getIdIterApprovativo() == null || detail.getIdIterApprovativo() == 0) && TipoAssegnazioneEnum.SPEDIZIONE.equals(detail.getTipoAssegnazioneEnum())) {
			documentManagerDTO.setComboMomentoProtocollazioneDisabled(true);
			return;
		} else {
			documentManagerDTO.setComboMomentoProtocollazioneDisabled(false);
		}

		if (destinatariInModifica == null || destinatariInModifica.isEmpty()) {
			return;
		}

		int countdestinatariInterni = 0;
		for (final DestinatarioRedDTO dest : destinatariInModifica) {
			if (dest != null && TipologiaDestinatarioEnum.INTERNO.equals(dest.getTipologiaDestinatarioEnum())) {
				countdestinatariInterni++;
			}
		}

		if (destinatariInModifica.size() == countdestinatariInterni) {
			detail.setMomentoProtocollazioneEnum(MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD);
			documentManagerDTO.setComboMomentoProtocollazioneDisabled(true);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#gestioneCheckFirmaDigitaleRGS(int,
	 *      long).
	 */
	@Override
	public boolean gestioneCheckFirmaDigitaleRGS(final int idTipologiaDocumento, final long idTipoProcedimento) {
		Connection connection = null;
		boolean checked = false;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			final DatiPredefinitiMozione d = mozioneDAO.getFirmaDigitaleRGS(idTipologiaDocumento, idTipoProcedimento, connection);
			checked = (d != null);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return checked;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#getFirmaDigitaleRGSFromWF(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public boolean getFirmaDigitaleRGSFromWF(final String documentTitle, final UtenteDTO utente) {
		boolean output = false;
		FilenetPEHelper fpeh = null;

		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			final VWWorkObject vwwO = fpeh.getWorkflowPrincipale(documentTitle, utente.getFcDTO().getIdClientAoo());

			if (vwwO != null) {
				final Integer firmaDigRGS = (Integer) TrasformerPE.getMetadato(vwwO, PropertiesNameEnum.FIRMA_DIG_RGS_WF_METAKEY);
				if (firmaDigRGS != null && firmaDigRGS > 0) {
					output = true;
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			logoff(fpeh);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#hasValidDetail(java.lang.String).
	 */
	@Override
	public boolean hasValidDetail(final String tipologiaDocumento) {
		return org.apache.commons.lang3.StringUtils.isBlank(tipologiaDocumento) || !org.apache.commons.lang3.StringUtils.endsWith(tipologiaDocumento, "_LIGHT");
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#isTemplateDocUscitaButtonVisible(java.lang.Boolean,
	 *      java.lang.Boolean, boolean, boolean, boolean, boolean).
	 */
	@Override
	public boolean isTemplateDocUscitaButtonVisible(final Boolean isTemplateDocUscitaAttivo, final Boolean isContentModificabile, final boolean isGeneratedByTemplateDocUscita,
			final boolean isInCreazioneUscita, final boolean isInModificaUscita, final boolean filePresente) {
		return (isTemplateDocUscitaAttivo && (isInCreazioneUscita || (isInModificaUscita && Boolean.TRUE.equals(isContentModificabile)))
				&& (isGeneratedByTemplateDocUscita || !filePresente));
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#preselezionaRibaltaTitolario(java.lang.Long,
	 *      java.lang.Long).
	 */
	@Override
	public boolean preselezionaRibaltaTitolario(final Long idUfficio, final Long idAoo) {
		boolean preseleziona = false;
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			preseleziona = nodoDAO.preselezionaRibaltaTitolario(idUfficio, idAoo, conn);
		} catch (final Exception ex) {
			rollbackConnection(conn);
			LOGGER.error("Errore durante il recupero dei dati dalla tabella per la preselezione del ribalta titolario :" + ex);
			throw new RedException(ex);
		} finally {
			closeConnection(conn);
		}
		return preseleziona;

	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#inviaMailSpedizione(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.util.List, java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public Long inviaMailSpedizione(final UtenteDTO utente, final String documentTitle, final String mittente, final List<DestinatarioRedDTO> destinatari,
			final String oggettoMail, final String testoMail) {

		Connection con = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		Long idNotificaSped = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final Document document = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());
			final String guid = document.get_Id().toString();

			final List<String> destinatariStringListTo = new ArrayList<>();
			final List<String> destinatariStringListCc = new ArrayList<>();
			final StringBuilder destinatariMailTo = new StringBuilder();
			final StringBuilder destinatariMailCc = new StringBuilder();

			// Creazione stringhe con mail destinatari TO/CC separati da ";" e stringhe con
			// info complete destinatari
			for (final DestinatarioRedDTO destinatario : destinatari) {
				final Contatto contatto = destinatario.getContatto();
				if (!ModalitaDestinatarioEnum.CC.equals(destinatario.getModalitaDestinatarioEnum())) {
					if (destinatariMailTo.length() > 0) {
						destinatariMailTo.append(";");
					}
					destinatariMailTo.append(contatto.getMailSelected());
				} else {
					if (destinatariMailCc.length() > 0) {
						destinatariMailCc.append(";");
					}
					destinatariMailCc.append(contatto.getMailSelected());
				}

				final StringBuilder destinatarioFullString = new StringBuilder().append(contatto.getContattoID()).append(",").append(contatto.getMailSelected());

				if (!ModalitaDestinatarioEnum.CC.equals(destinatario.getModalitaDestinatarioEnum())) {
					// es. 298434,direzione.uslnordovest@postacert.toscana.it,2,E,TO
					destinatarioFullString.append(",2,E,TO");
					destinatariStringListTo.add(destinatarioFullString.toString());
				} else {
					// es. 298434,direzione.uslnordovest@postacert.toscana.it,2,E,CC
					destinatarioFullString.append(",2,E,CC");
					destinatariStringListCc.add(destinatarioFullString.toString());
				}
			}

			final List<String> destinatariStringList = new ArrayList<>();
			destinatariStringList.addAll(destinatariStringListTo);
			destinatariStringList.addAll(destinatariStringListCc);

			Integer tipologiaCasellaId = fceh.getTipoCasellaPostaleFromNome(mittente);
			if (tipologiaCasellaId == null) {
				// Attenzione, in questo caso vuol dire che la mail non è configurata su FileNet
				LOGGER.warn("Attenzione il documento con id " + documentTitle + " ha la mail del mittente non impostata su FileNet");
				LOGGER.warn("in questo caso non riesce a recuperare la tipologia del destinatario, di default sarà PEO");
				tipologiaCasellaId = 1;
			}

			idNotificaSped = codaMailSRV.creaMailEInserisciInCoda(documentTitle, guid, mittente, destinatariStringList, destinatariMailTo.toString(),
					destinatariMailCc.toString(), oggettoMail, testoMail, null, null, TipologiaInvio.NUOVOMESSAGGIO, tipologiaCasellaId, null, false, true, null, null, utente,
					con, null, null, true, false);

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di registrazione della mail in coda", e);
		} finally {
			closeConnection(con);
			logoff(fpeh);
			popSubject(fceh);
		}
		return idNotificaSped;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV#checkStampigliaturaSiglaVisible(java.lang.Long,
	 *      java.lang.Integer).
	 */
	@Override
	public boolean checkStampigliaturaSiglaVisible(final Long idUffIstruttore, final Integer idTipologiaDoc) {
		boolean output = false;
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			output = tipoProcedimentoDAO.checkStampigliaturaSiglaVisible(idUffIstruttore, idTipologiaDoc, conn);
		} catch (final Exception ex) {
			LOGGER.error("Errore nel recupero della stampigliatura di sigla : " + ex);
			throw new RedException("Errore nel recupero della stampigliatura di sigla : " + ex);
		} finally {
			closeConnection(conn);
		}
		return output;

	}
}
