package it.ibm.red.business.service.facade;

import java.io.Serializable;

import com.google.common.net.MediaType;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;


/**
 * The Interface ISalvaDocumentoFacadeSRV.
 *
 * @author m.crescentini
 * 
 * Servizi per il salvataggio dei documenti.
 */
public interface ISalvaDocumentoFacadeSRV extends Serializable {

	/**
	 * Salva il documento.
	 * @param detailDocInput
	 * @param parametri
	 * @param utente
	 * @param provenienza
	 * @return esito del salvataggio del documento
	 */
	EsitoSalvaDocumentoDTO salvaDocumento(DetailDocumentRedDTO detailDocInput, SalvaDocumentoRedParametriDTO parametri,	UtenteDTO utente, ProvenienzaSalvaDocumentoEnum provenienza);

	/**
	 * Verifica la validità della firma per l'iter di spedizione.
	 * @param nomeFile
	 * @param contentFile
	 * @param utente
	 * @param destCartacei
	 * @param signerTimbro
	 * @return true o false
	 */
	boolean verificaFirmaIterSpedizione(String nomeFile, byte[] contentFile, UtenteDTO utente, boolean destCartacei,
			String signerTimbro);

	/**
	 * Verifica la validità della firma per l'iter di spedizione.
	 * @param nomeFile
	 * @param contentFile
	 * @param utente
	 * @param destCartacei
	 * @param signerTimbro
	 * @param mediaType
	 * @return true o false
	 */
	boolean verificaFirmaIterSpedizione(String nomeFile, byte[] contentFile, UtenteDTO utente, boolean destCartacei,
			String signerTimbro, MediaType mediaType);
	
}