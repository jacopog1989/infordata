package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IRicercaFepaFacadeSRV;

/**
 * The Interface IRicercaFepaSRV.
 *
 * @author m.crescentini
 * 
 *         Interfaccia del servizio per la gestione della ricerca FEPA.
 */
public interface IRicercaFepaSRV extends IRicercaFepaFacadeSRV {

}