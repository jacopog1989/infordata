package it.ibm.red.business.enums;

/**
 * Enum messaggi AUT.
 */
public enum AUTMessageIDEnum {
	
	/**
	 * Messaggio iniziale.
	 */
	MESSAGGIO_INIZIALE(101),
	
	/**
	 * Dati integrativi richiesti.
	 */
	DATI_INTEGRATIVI_RICHIESTI(102),
	
	/**
	 * Dati integrativi spontanei.
	 */
	DATI_INTEGRATIVI_SPONTANEI(103),
	
	/**
	 * Richiesta ritiro autotutela.
	 */
	RICHIESTA_RITIRO_AUTOTUTELA(104),
	
	/**
	 * Visto.
	 */
	VISTO(201),
	
	/**
	 * Richiesta integrazione.
	 */
	RICHIESTA_INTEGRAZIONE(202),
	
	/**
	 * Osservazioni.
	 */
	OSSERVAZIONE(203),
	
	/**
	 * Restituzione.
	 */
	RESTITUZIONE(206);

	
	/**
	 * Identificativo.
	 */
	private final Integer id;
	
	AUTMessageIDEnum(final Integer id) {
		this.id = id;
	}

	/**
	 * Restituisce l'id.
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Restituisce l'enum associata all'id passato come parametro.
	 * @param id per il quale occorre l'enum associata
	 * @return l'enum associata all'id
	 */
	public static AUTMessageIDEnum getEnumById(final Integer id) {
		AUTMessageIDEnum output = null;
		for (AUTMessageIDEnum item : AUTMessageIDEnum.values()) {
			if (item.getId().equals(id)) {
				output = item;
				break;
			}
		}

		return output;
	}

	
}