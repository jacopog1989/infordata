package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;

import com.filenet.api.collection.AccessPermissionList;

import it.ibm.red.business.dto.DetailAssegnazioneDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.SecurityDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.service.facade.ISecurityFacadeSRV;

/**
 * The Interface ISecuritySRV.
 *
 * @author CPIERASC
 * 
 *         Interfaccia servizio gestione security filenet.
 */
public interface ISecuritySRV extends ISecurityFacadeSRV {

	/**
	 * Metodo per recupero delle security storico.
	 * 
	 * @param utente             utente
	 * @param idDocumento        identificativo documento
	 * @param isDocumento        flag documento
	 * @param addUfficio         flag aggiungi ufficio
	 * @param isRiservato        flag riservato
	 * @param escludiCreatore se true esclude il creatore dalle acl
	 * @param destinatariInterni lista destinatari esterni
	 * @param connection         connessione
	 * @return lista security
	 */
	ListSecurityDTO getStoricoSecurity(UtenteDTO utente, String idDocumento, boolean isDocumento, boolean addUfficio, boolean isRiservato,
			boolean escludiCreatore, List<String> destinatariInterni, Connection connection);

	/**
	 * Recupero security assegnazione.
	 * 
	 * @param assegnazione assegnazione
	 * @param isDocumento  flag documento
	 * @param addUfficio   flag aggiungi ufficio
	 * @param isRiservato  flag riservato
	 * @param connection   connessione
	 * @return lista security
	 */
	ListSecurityDTO getSecurityAssegnazione(String assegnazione, boolean isDocumento, boolean addUfficio, boolean isRiservato, Connection connection);

	/**
	 * Metodo per la creazione di una security.
	 * 
	 * @param nodoCorrenteId   identificativo nodo corrente
	 * @param idUtenteCorrente identificativo utente corrente
	 * @param idNodoCorriere   identificativo nodo corriere
	 * @return struttura sicurezza
	 */
	SecurityDTO creaSecurity(Long nodoCorrenteId, Long idUtenteCorrente, Long idNodoCorriere);

	/**
	 * Genera le security per un documento riservato.
	 * 
	 * @param utente         - Credenziali utente
	 * @param idDocumento    - Identificativo del docucmento
	 * @param workflowNumber - Workflow del documento
	 * @param fpeh           - Connessione al Process Engine di Filenet
	 * @param connection     - Connessione al DB
	 */
	void generateRiservatoSecurity(UtenteDTO utente, String idDocumento, String workflowNumber, FilenetPEHelper fpeh, Connection connection);
	
	/**
	 * Genera le security per un documento riservato.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param workflowNumber
	 * @param fceh
	 * @param fpeh
	 * @param connection
	 */
	void generateRiservatoSecurity(UtenteDTO utente, String idDocumento, String workflowNumber, IFilenetCEHelper fceh, FilenetPEHelper fpeh, Connection connection);

	/**
	 * Recupero security.
	 * 
	 * @param utente             credenziali utente
	 * @param idDocumento        identificativo documento
	 * @param idNodoDestinatario identificativo nodo destinatario
	 * @param destinatariInterni destinatari interni
	 * @param coordinatore       coordinatore
	 * @param isRiservato        flag riservato
	 * @return lista di security
	 */
	ListSecurityDTO getSecurityUtentePerUfficio(UtenteDTO utente, String idDocumento, String idNodoDestinatario, List<String> destinatariInterni,
			String coordinatore, Boolean isRiservato);

	/**
	 * Recupero security.
	 * 
	 * @param utente             credenziali utente
	 * @param idDocumento        identificativo documento
	 * @param idNodoDestinatario identificativo nodo destinatario
	 * @param destinatariInterni destinatari interni
	 * @param coordinatore       coordinatore
	 * @param isRiservato        indica che il documento è riservato
	 * @return lista security
	 */
	ListSecurityDTO getSecurityUtentePerUfficioFascicolo(UtenteDTO utente, String idDocumento, String idNodoDestinatario, List<String> destinatariInterni,
			String coordinatore, Boolean isRiservato);

	/**
	 * Ottiene le securities per registro giornaliero.
	 * 
	 * @param utentiAssegnazioni
	 * @param connection
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityPerRegistroGiornaliero(Collection<UtenteDTO> utentiAssegnazioni, Connection connection);

	/**
	 * Ottiene le securities per documento.
	 * 
	 * @param idDocumento
	 * @param account
	 * @param assegnazioniCompetenza
	 * @param assegnazioniConoscenza
	 * @param assegnazioniContributo
	 * @param assegnazioniFirma
	 * @param assegnazioniSigla
	 * @param assegnazioniVisto
	 * @param assegnazioniFirmaMultipla
	 * @param idNodoCreatore
	 * @param idUtenteCreatore
	 * @param isDocumento
	 * @param isDocEntrata
	 * @param idIterApprovativo
	 * @param isRiservato
	 * @param assegnazioneCoordinatore
	 * @param addDelegato
	 * @param isNodoUcp
	 * @param destinatariInterni
	 * @param connection
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityPerDocumento(String idDocumento, UtenteDTO account, String[] assegnazioniCompetenza, String[] assegnazioniConoscenza,
			String[] assegnazioniContributo, String[] assegnazioniFirma, String[] assegnazioniSigla, String[] assegnazioniVisto, String[] assegnazioniFirmaMultipla,
			Long idNodoCreatore, Long idUtenteCreatore, Boolean isDocumento, Boolean isDocEntrata, int idIterApprovativo, Boolean isRiservato, String assegnazioneCoordinatore,
			Boolean addDelegato, Boolean isNodoUcp, String[] destinatariInterni, Connection connection);

	/**
	 * Ottiene le securities per documenti allacciati.
	 * 
	 * @param idDocumento
	 * @param account
	 * @param assegnazioniCompetenza
	 * @param assegnazioniConoscenza
	 * @param assegnazioniContributo
	 * @param assegnazioniFirma
	 * @param assegnazioniSigla
	 * @param assegnazioniVisto
	 * @param assegnazioniFirmaMultipla
	 * @param idNodoCreatore
	 * @param idUtenteCreatore
	 * @param isDocumento
	 * @param idIterApprovativo
	 * @param isRiservato
	 * @param assegnazioneCoordinatore
	 * @param aggiungiDelegato
	 * @param destinatariInterni
	 * @param connection
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityDocumentoPerDocumentiAllacciati(String idDocumento, UtenteDTO account, String[] assegnazioniCompetenza,
			String[] assegnazioniConoscenza, String[] assegnazioniContributo, String[] assegnazioniFirma, String[] assegnazioniSigla, String[] assegnazioniVisto,
			String[] assegnazioniFirmaMultipla, Long idNodoCreatore, Long idUtenteCreatore, Boolean isDocumento, int idIterApprovativo, Boolean isRiservato,
			String assegnazioneCoordinatore, Boolean aggiungiDelegato, String[] destinatariInterni, Connection connection);

	/**
	 * Ottiene le securities per bozza.
	 * 
	 * @param idNodoCreatore
	 * @param idUtenteCreatore
	 * @param isDocumento
	 * @param connection
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityPerBozza(Long idNodoCreatore, Long idUtenteCreatore, boolean isDocumento, Connection connection);

	/**
	 * Verifica le securities.
	 * 
	 * @param securities
	 * @param connection
	 */
	void verificaSecurities(ListSecurityDTO securities, Connection connection);

	/**
	 * Ottiene le securities per gruppo.
	 * 
	 * @param organigramma
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityByGruppo(List<Nodo> organigramma);

	/**
	 * Ottiene le securities per ufficio utente.
	 * 
	 * @param idUfficio
	 * @param idUtente
	 * @param con
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityPuntuale(Long idUfficio, Long idUtente, Connection con);

	/**
	 * Ottiene le securities per utente delegato.
	 * 
	 * @param idUfficioDelegato
	 * @param con
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityUtenteDelegato(Long idUfficioDelegato, Connection con);

	/**
	 * Aggiorna le securities dei documenti allacciati.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param securityDocAllacciati
	 * @param securityFascicoloDocAllacciati
	 * @param fceh
	 * @param con
	 */
	void aggiornaSecurityDocumentiAllacciati(UtenteDTO utente, int idDocumento, ListSecurityDTO securityDocAllacciati,
			ListSecurityDTO securityFascicoloDocAllacciati, IFilenetCEHelper fceh, Connection con);

	/**
	 * Come aggiorna securities
	 * 
	 * In più aggiunge lo username alle securities dell'utente, invocando il metodo
	 * verificaSecurities
	 * 
	 * 
	 * @param idDocumento
	 * @param idAoo
	 * @param securityDoc
	 * @param allVersion
	 * @param securityToOverride
	 * @param fceh
	 * @param con
	 */
	void aggiornaEVerificaSecurityDocumento(String idDocumento, Long idAoo, ListSecurityDTO securityDoc, boolean allVersion, boolean securityToOverride,
			IFilenetCEHelper fceh, Connection con);

	/**
	 * Aggiorna le securities del documento
	 * 
	 * !Attenzione il campo securityDoc deve essere passato per il metodo
	 * verificaSecurities! altrimenti si verifica un errore di FileNet
	 * 
	 * @param idDocumento
	 * @param idAoo
	 * @param securityDoc        la lista delle sicurezze del documento da
	 *                           sovrascrivere con lo username aggiornato vedi il
	 *                           metodo verificaSecurities
	 * @param allVersion
	 * @param securityToOverride
	 * @param fceh
	 */
	void aggiornaSecurityDocumento(String idDocumento, Long idAoo, ListSecurityDTO securityDoc, boolean allVersion, boolean securityToOverride,
			IFilenetCEHelper fceh);

	/**
	 * Aggiorna le securities del documento.
	 * 
	 * @param idDocumento
	 * @param idAoo
	 * @param acl
	 * @param allVersion
	 * @param securityToOverride
	 * @param fceh
	 */
	void aggiornaSecurityDocumento(String idDocumento, Long idAoo, AccessPermissionList acl, boolean allVersion, boolean securityToOverride, IFilenetCEHelper fceh);

	/**
	 * Ottiene lo storico delle securities per il documento.
	 * 
	 * @param idDocumento
	 * @param isDocEntrata
	 * @param utente
	 * @param isRiservato
	 * @param fceh
	 * @param con
	 * @return lista delle securities
	 */
	ListSecurityDTO getStoricoSecurity(int idDocumento, boolean isDocEntrata, UtenteDTO utente, boolean isRiservato, IFilenetCEHelper fceh, Connection con);

	/**
	 * Ottiene le securities per la riassegnazione del fascicolo.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param idUtenteDestinatario
	 * @param idUfficioDestinatario
	 * @param con
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityPerRiassegnazioneFascicolo(UtenteDTO utente, String idDocumento, Long idUtenteDestinatario, Long idUfficioDestinatario,
			Connection con);

	/**
	 * Recupero Security per la riassegnazione del fascicolo.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param idUtenteDestinatario
	 * @param idUfficioDestinatario
	 * @param coordinatore
	 * @param con
	 * @return
	 */
	ListSecurityDTO getSecurityPerRiassegnazioneFascicolo(UtenteDTO utente, String idDocumento, Long idUtenteDestinatario, Long idUfficioDestinatario,
			String coordinatore, Connection con);

	/**
	 * Ottiene le securities per la riassegnazione del documento.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param idUtenteDestinatario
	 * @param idUfficioDestinatario
	 * @param isRiservato
	 * @param con
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityPerRiassegnazioneDocumento(UtenteDTO utente, String idDocumento, Long idUtenteDestinatario, Long idUfficioDestinatario,
			boolean isRiservato, Connection con);

	/**
	 * Ottiene le securities per la riassegnazione.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param idUtenteDestinatario
	 * @param idUfficioDestinatario
	 * @param coordinatore
	 * @param aggiungiDelegato
	 * @param isDocumento
	 * @param isRiservato
	 * @param escludiCreatore se true esclude il creatore dalle acl
	 * @param con
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityPerRiassegnazione(UtenteDTO utente, String idDocumento, Long idUtenteDestinatario, Long idUfficioDestinatario, String coordinatore,
			boolean aggiungiDelegato, boolean isDocumento, boolean isRiservato, boolean escludiCreatore, Connection con);

	/**
	 * Ottiene le securities per contributo.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param isRiservato
	 * @param con
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityPerContributo(UtenteDTO utente, String idDocumento, boolean isRiservato, Connection con);

	/**
	 * Ottiene le securities per l'aggiornamento.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param idUtenteDestinatario
	 * @param idNodoDestinatario
	 * @param isRiservato
	 * @param con
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityPerAggiornamento(UtenteDTO utente, String idDocumento, String idUtenteDestinatario, String idNodoDestinatario, boolean isRiservato,
			Connection con);

	/**
	 * Ottiene le securities post bozza.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param assegnazioniCompetenza
	 * @param assegnazioniConoscenza
	 * @param assegnazioniContributo
	 * @param assegnazioniFirma
	 * @param assegnazioniSigla
	 * @param assegnazioniVisto
	 * @param assegnazioniFirmaMultipla
	 * @param isDocumento
	 * @param idIterAutomatico
	 * @param isRiservato
	 * @param assegnazioneCoordinatore
	 * @param aggiungiDelegato
	 * @param destinatariInterni
	 * @param fceh
	 * @param con
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityDopoBozza(UtenteDTO utente, String idDocumento, String[] assegnazioniCompetenza, String[] assegnazioniConoscenza,
			String[] assegnazioniContributo, String[] assegnazioniFirma, String[] assegnazioniSigla, String[] assegnazioniVisto, String[] assegnazioniFirmaMultipla,
			boolean isDocumento, int idIterAutomatico, Boolean isRiservato, String assegnazioneCoordinatore, Boolean aggiungiDelegato, String[] destinatariInterni,
			IFilenetCEHelper fceh, Connection con);

	/**
	 * Ottiene le securities per la riassegnazione del fascicolo.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param assegnazioni
	 * @param coordinatore
	 * @param aggiungiDelegato
	 * @param con
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityPerRiassegnazioneFascicolo(UtenteDTO utente, String idDocumento, String[] assegnazioni, String coordinatore,
			boolean aggiungiDelegato, Connection con);

	/**
	 * Modifica le securities dei fascicoli associati al documento.
	 * 
	 * @param idDocumento
	 * @param utente
	 * @param assegnazioni
	 * @param con
	 * @return true o false
	 */
	boolean modificaSecurityFascicoli(String idDocumento, UtenteDTO utente, String[] assegnazioni, Connection con);

	/**
	 * Modifica le securities dei fascicoli associati al documento.
	 * 
	 * @param idAoo
	 * @param securities
	 * @param idDocumento
	 * @param fceh
	 * @return true o false
	 */
	boolean modificaSecurityFascicoli(Long idAoo, ListSecurityDTO securities, String idDocumento, IFilenetCEHelper fceh);

	/**
	 * Modifica le securities dei fascicoli associati al documento.
	 * 
	 * @param idDocumento
	 * @param utente
	 * @param assegnazioni
	 * @param coordinatore
	 * @param aggiungiDelegato
	 * @param con
	 * @return true o false
	 */
	boolean modificaSecurityFascicoli(String idDocumento, UtenteDTO utente, String[] assegnazioni, String coordinatore, boolean aggiungiDelegato, Connection con);

	/**
	 * Ottiene le securities per la riassegnazione del documento.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param assegnazioni
	 * @param isRiservato
	 * @param con
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityPerRiassegnazioneDocumento(UtenteDTO utente, String idDocumento, String[] assegnazioni, boolean isRiservato, Connection con);

	/**
	 * Ottiene le securities per la riassegnazione.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param assegnazioni
	 * @param coordinatore
	 * @param aggiungiDelegato
	 * @param isDocumento
	 * @param isRiservato
	 * @param escludiCreatore se true esclude la memorizzazione dell'assegnatario, 
	 * 		viene usato in caso di protcollazione riservata con ufficio UCP per garantire la correttezza delle acl
	 * @param con
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityPerRiassegnazione(UtenteDTO utente, String idDocumento, String[] assegnazioni, String coordinatore, boolean aggiungiDelegato,
			boolean isDocumento, boolean isRiservato, boolean escludiCreatore, Connection con);

	/**
	 * 
	 * @param utente
	 * @param idDocumentoPrincipale è anche un documentTitle
	 * @param securityDocAllacciati
	 * @param assegnazioni
	 * @param fceh
	 * @param con
	 */
	void aggiornaSecurityDocumentiAllacciati(UtenteDTO utente, int idDocumentoPrincipale, ListSecurityDTO securityDocAllacciati, String[] assegnazioni,
			IFilenetCEHelper fceh, Connection con);

	/**
	 * Aggiorna le securities dei contributi interni.
	 * 
	 * @param utente
	 * @param idDocumentoPrincipale
	 * @param assegnazioni
	 * @param fceh
	 * @param con
	 */
	void aggiornaSecurityContributiInterni(UtenteDTO utente, String idDocumentoPrincipale, String[] assegnazioni, IFilenetCEHelper fceh, Connection con);

	/**
	 * Aggiorna le securities dei coontributi inseriti.
	 * 
	 * @param utente
	 * @param documentTitle
	 * @param idUtenteDdestinatario
	 * @param idNodoDestinatario
	 * @param fceh
	 * @param connection
	 */
	void aggiornaSecurityContributiInseriti(UtenteDTO utente, String idDocumento, String idUtenteDestinatario, String idNodoDestinatario, IFilenetCEHelper fceh,
			Connection connection);

	/**
	 * Aggiorna le securities dei coontributi inseriti.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param assegnazioni
	 * @param fceh
	 * @param connection
	 */
	void aggiornaSecurityContributiInseriti(UtenteDTO utente, String idDocumento, String[] assegnazioni, IFilenetCEHelper fceh, Connection connection);

	/**
	 * Ottiene le securities per aggiungere l'assegnazione.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param idUtenteCreatore
	 * @param idUfficioCreatore
	 * @param assegnazioni
	 * @param isDocumento
	 * @param aggiungiUfficio
	 * @param isRiservato
	 * @param aggiungiDelegato
	 * @param assegnazioneCoordinatore
	 * @param con
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityPerAddAssegnazione(UtenteDTO utente, String idDocumento, Long idUtenteCreatore, Long idUfficioCreatore, String[] assegnazioni,
			boolean isDocumento, boolean aggiungiUfficio, boolean isRiservato, boolean aggiungiDelegato, String assegnazioneCoordinatore, Connection con);

	/**
	 * Ottiene le securities per aggiungere l'assegnazione.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param idUtenteCreatore
	 * @param idUfficioCreatore
	 * @param assegnazioni
	 * @param isDocumento
	 * @param aggiungiUfficio
	 * @param isRiservato
	 * @param con
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityPerAddAssegnazione(UtenteDTO utente, String idDocumento, Long idUtenteCreatore, Long idUfficioCreatore, String[] assegnazioni,
			boolean isDocumento, boolean aggiungiUfficio, boolean isRiservato, Connection con);

	/**
	 * Ottiene le securities per mittente.
	 * 
	 * @param utente
	 * @param idUtenteCreatore
	 * @param idUfficioCreatore
	 * @param isDocumento
	 * @param isRiservato
	 * @param connection
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityMittente(UtenteDTO utente, Long idUtenteCreatore, Long idUfficioCreatore, boolean isDocumento, boolean isRiservato,
			Connection connection);

	/**
	 * Recupera da filenet tutte le informazioni necessarie a parametrizzare il
	 * recupero delle SecurityDTO.
	 * 
	 * @param fceh
	 * @param idAooUtente  idAoo dell'utente che esegue l'operazione
	 * @param idDocumento  id documento recuperato dal workflow (PE document)
	 * @param coordinatore Un flag che identifica se è necessario o meno recuperare
	 *                     le informaizoni relative al coordinatore
	 * 
	 * @return Le informazioni di nostro interesse recuperate da FileNet CE
	 */
	DetailAssegnazioneDTO getDocumentDetailAssegnazione(IFilenetCEHelper fceh, Long idAooUtente, String idDocumento, boolean coordinatore);

	/**
	 * Ottiene le securities per la fascicolazione.
	 * 
	 * @param idFascicolo
	 * @param idAoo
	 * @param fceh
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityPerFascicolazione(String idFascicolo, Long idAoo, IFilenetCEHelper fceh);

	/**
	 * Ottiene le acl dalle securities.
	 * 
	 * @param securityList
	 * @param fceh
	 * @return acl
	 */
	String getAclFromSecurity(List<SecurityDTO> securityList, IFilenetCEHelper fceh);

	/**
	 * Ottiene le securities per l'assegnazione per WS.
	 * 
	 * @param assegnazione
	 * @param isDocumento
	 * @param aggiungiUfficio
	 * @param isRiservato
	 * @param connection
	 * @param utente
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityAssegnazionePerWS(String assegnazione, boolean isDocumento, boolean aggiungiUfficio, boolean isRiservato, Connection connection,
			UtenteDTO utente);
}