package it.ibm.red.business.enums;

/**
 * Enum per la definizione dei colori associati all'interoperabilità.
 */
public enum ColoreInteroperabilitaEnum {

	/**
	 * Segnatura errata.
	 */
	ROSSO(0, "red", "SEGNATURA_ERRATA"),
	
	/**
	 * Segnatura non conforme.
	 */
	GIALLO(1, "yellow", "SEGNATURA_NON_CONFORME"),
	
	/**
	 * Segnatura valida.
	 */
	VERDE(2, "green", "SEGNATURA_VALIDA");
	
	/**
	 * Identificativo.
	 */
	private Integer id;
	
	/**
	 * Colore.
	 */
	private String color;
	
	/**
	 * Descrizione.
	 */
	private String descrizione;
	
	/**
	 * Costruttore enum.
	 * @param inId
	 * @param inColor
	 * @param inDescrizione
	 */
	ColoreInteroperabilitaEnum(final Integer inId, final String inColor, final String inDescrizione) {
		id = inId;
		color = inColor;
		descrizione = inDescrizione;
	}

	/**
	 * Restituisce l'id dell'Enum.
	 * @return id
	 */
	public final Integer getId() {
		return id;
	}

	/**
	 * Restituisce il colore associato all'Enum.
	 * @return
	 */
	public final String getColor() {
		return color;
	}

	/**
	 * Restituisce la descrizione associata all'Enum.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Imposta la descrizione.
	 * @param descrizione
	 */
	protected void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * Imposta il colore.
	 * @param color
	 */
	protected void setColor(final String color) {
		this.color = color;
	}

	/**
	 * Restituisce il colore associato all'Enum definita dall'id.
	 * @param id
	 * @return colore come String
	 */
	public static String get(final int id) {
		String output = null;
		
		for (final ColoreInteroperabilitaEnum esitoValidazioneInterop : ColoreInteroperabilitaEnum.values()) {
			if (esitoValidazioneInterop.getId() == id) {
				output = esitoValidazioneInterop.getColor();
				break;
			}
		}
		return output;
	}

	/**
	 * Restituisce l'Enum caratterizzata da una specifica descrizione.
	 * @param esitoValidazione
	 * @return ColoreInteroperabilitaEnum
	 */
	public static ColoreInteroperabilitaEnum get(final String esitoValidazione) {
		ColoreInteroperabilitaEnum output = null;
		
		for (final ColoreInteroperabilitaEnum esitoValidazioneInterop : ColoreInteroperabilitaEnum.values()) {
			if (esitoValidazioneInterop.getDescrizione().equalsIgnoreCase(esitoValidazione)) {
				output = esitoValidazioneInterop;
				break;
			}
		}
		
		return output;
	}	
}

