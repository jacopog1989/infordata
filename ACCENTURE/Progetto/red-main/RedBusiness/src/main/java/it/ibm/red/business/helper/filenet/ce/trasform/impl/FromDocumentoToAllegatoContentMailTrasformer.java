package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import it.ibm.red.business.dto.MailAttachmentDTO;
import it.ibm.red.business.enums.TrasformerCEEnum;

/**
 * FromDocumentoToAllegatoMailTrasformer recuperava il content del
 * MailAttachment, ma non glielo assegnava questo lo fa.
 */
public class FromDocumentoToAllegatoContentMailTrasformer extends FromDocumentoToAllegatoMailTrasformer {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -2948271476494427708L;

	/**
	 * Costruttore.
	 */
	public FromDocumentoToAllegatoContentMailTrasformer() {
		super();
		super.setEnumKey(TrasformerCEEnum.FROM_DOCUMENTO_TO_ALLEGATO_CONTENT_MAIL);
	}
	
	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToAllegatoMailTrasformer#buildMailAttachment(java.lang.String,
	 *      java.lang.String, byte[], java.lang.String, java.lang.String,
	 *      java.lang.Boolean, java.lang.String).
	 */
	@Override
	protected MailAttachmentDTO buildMailAttachment(final String id, final String inFileName, final byte[] inContent, final String mimeType,
			final String inDescription, final Boolean inNeedSign, final String inDocuemenTitle) {
		
		MailAttachmentDTO mail = super.buildMailAttachment(id, inFileName, inContent, mimeType, inDescription, inNeedSign, inDocuemenTitle);
		mail.setContent(inContent);
		return mail;
	}
}
