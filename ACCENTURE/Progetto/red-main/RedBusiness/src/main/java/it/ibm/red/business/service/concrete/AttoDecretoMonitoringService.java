package it.ibm.red.business.service.concrete;

import java.sql.Connection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IAttoDecretoMonitoringDAO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.monitoring.attodecreto.AttoDecretoMonitoring;
import it.ibm.red.business.service.IAttoDecretoMonitoringService;

/**
 * Service monitoring Atto Decreto.
 */
@Service
public class AttoDecretoMonitoringService extends AbstractService implements IAttoDecretoMonitoringService {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 7702456990337611220L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AttoDecretoMonitoringService.class.getName());
	
	/**
	 * DAO monitoraggio atto decreto.
	 */
	@Autowired
	private IAttoDecretoMonitoringDAO attoDecretoMonitoringDAO;
	
	/**
	 * 	Viene registrata l'operazione sulla base dati.
	 * 
	 * @param beanInfo 
	 * @return true, se l'operazione è andata a buon fine, false altrimenti
	 */
	@Override
	public boolean insertOperation(final AttoDecretoMonitoring beanInfo) {
		
		Connection connection =  null;
		
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			
			final Integer id = attoDecretoMonitoringDAO.getNextId(connection);
			
			beanInfo.setIdMonitoring(id);
			
			attoDecretoMonitoringDAO.inserisciOperation(beanInfo, connection);
			
			return true;
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento dell'operazione per il documento " + beanInfo.getIdDocumento(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
		
		return false;
		
	}
	
	/**
	 * 	Viene aggiornata l'operazione sulla base dati.
	 * 
	 * @param beanInfo 
	 * @return true, se l'operazione è andata a buon fine, false altrimenti
	 */
	@Override
	public boolean updateOperation(final AttoDecretoMonitoring beanInfo) {
		
		Connection connection =  null;
		
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			
			attoDecretoMonitoringDAO.aggiornaOperation(beanInfo, connection);
			
			return true;
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento dell'operazione per il documento " + beanInfo.getIdDocumento(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
		
		return false;
		
	}
	
}