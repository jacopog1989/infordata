package it.ibm.red.business.dto;

import it.ibm.red.business.enums.TipoAzioneEnum;

/**
 * The Class ContatoriSchedulerDTO.
 *
 * @author CPIERASC
 * 
 * 	DTO per gestire i contatori dello scheduler.
 */
public class ContatoriSchedulerDTO extends AbstractDTO {
	
	/**
	 * Serializzable.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Numero eventi.
	 */
	private Integer numEventi;

	/**
	 * Numero documenti.
	 */
	private Integer numDocumenti;

	/**
	 * Costruttore.
	 */
	public ContatoriSchedulerDTO() {
		this(0, 0);
	}

	/**
	 * Costruttore.
	 * 
	 * @param nEventi		numero eventi
	 * @param nDocumenti	numero documenti	
	 */
	public ContatoriSchedulerDTO(final Integer nEventi, final Integer nDocumenti) {
		numEventi = nEventi;
		numDocumenti = nDocumenti;
	}

	/**
	 * Getter.
	 * 
	 * @return	numero eventi
	 */
	public final Integer getNumEventi() {
		return numEventi;
	}

	/**
	 * Getter.
	 * 
	 * @return	numero documenti
	 */
	public final Integer getNumDocumenti() {
		return numDocumenti;
	}

	/**
	 * Getter.
	 * 
	 * @return	numero totale
	 */
	public final Integer getNumTotale() {
		return numEventi + numDocumenti;
	}

	/**
	 * Metodo per aggiungere una unità ai contatori sulla base del tipo di azione.
	 * 
	 * @param tipo	tipologia di azione
	 */
	public final void add(final TipoAzioneEnum tipo) {
		if (TipoAzioneEnum.EVENTO.equals(tipo)) {
			numEventi++;
		} else if (TipoAzioneEnum.SCADENZA_DOCUMENTO.equals(tipo)) {
			numDocumenti++;
		}
	}

}
