package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.INotificaDAO;
import it.ibm.red.business.dto.NotificaDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * Dao per la gestione delle notifiche.
 * 
 * @author CPIERASC
 */
@Repository
public class NotificaDAO extends AbstractDAO implements INotificaDAO {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -2276156038968479436L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotificaDAO.class);
	
	/**
	 * Query parziale.
	 */
	private static final String WHERE_SN_IDEVENTO_TE_IDEVENTO_AND_N_IDNODO_SN_IDNODO_AND_SN_IDUTENTE = "WHERE SN.IDEVENTO = TE.IDEVENTO AND N.IDNODO = SN.IDNODO AND SN.IDUTENTE = ? ";

	/**
	 * Query parziale.
	 */
	private static final String FROM_SE_NOTIFICA_SOTTOSCRIZIONE_SN_SE_TIPO_EVENTO_TE_NODO_N = "FROM SE_NOTIFICA_SOTTOSCRIZIONE SN, SE_TIPO_EVENTO TE, NODO N ";

	/**
	 * Messaggio errore recupero notifica.
	 */
	private static final String ERROR_RECUPERO_NOTIFICA_MSG = "Errore durante il recupero della notifica ";

	/**
	 * Insert.
	 *
	 * @param notifica
	 *            the notifica
	 * @param connection
	 *            the connection
	 */
	@Override
	public final void insert(final NotificaDTO notifica, final Connection connection) {
		int index = 1;
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement("INSERT INTO SE_NOTIFICA_SOTTOSCRIZIONE (IDUTENTE, IDNODO, IDDOCUMENTO, IDEVENTO, STATONOTIFICA, VISUALIZZATO, DATAEVENTO) VALUES(?,?,?,?,?,?,?)");
			ps.setString(index++, notifica.getIdUtente());
			ps.setString(index++, notifica.getIdNodo());
			ps.setString(index++, notifica.getIdDocumento());
			ps.setInt(index++, notifica.getIdEvento());
			ps.setInt(index++, notifica.getStatoNotifica());
			ps.setInt(index++, notifica.getVisualizzato());
			ps.setDate(index++, new java.sql.Date((new Date()).getTime()));
			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'inserimento della notifica.", e);
		} finally {
			closeStatement(ps);
		}
		
	}
	
	/**
	 * @see it.ibm.red.business.dao.INotificaDAO#getNext(int, java.sql.Connection).
	 */
	@Override
	public NotificaDTO getNext(final int idAoo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
			
		try {

			final StringBuilder query = new StringBuilder("select sn.*, se.idruolo from SE_NOTIFICA_SOTTOSCRIZIONE sn, SE_SOTTOSCRIZIONE_EVENTO se where sn.IDEVENTO = se.IDEVENTO and sn.IDNODO = se.IDNODO and sn.IDUTENTE = se.IDUTENTE and statonotifica = 0 and sn.idnodo in (select n.idnodo from nodo n where n.idaoo = ?) and sn.dataevento = (select min(dataevento) from SE_NOTIFICA_SOTTOSCRIZIONE where idevento = sn.IDEVENTO and idnodo = sn.idnodo and idutente = sn.idutente and statonotifica = 0) and ROWNUM <= 1 FOR UPDATE SKIP LOCKED");			
			
			ps = connection.prepareStatement(query.toString());
			ps.setInt(1, idAoo);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				return populateVO(rs);
			} else {
				return null;
			}
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_NOTIFICA_MSG, e);
			throw new RedException(ERROR_RECUPERO_NOTIFICA_MSG);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	private static NotificaDTO populateVO(final ResultSet rs) throws SQLException {
		final NotificaDTO notificaSottoscrizione = new NotificaDTO();
		notificaSottoscrizione.setIdNotifica(rs.getInt("idnotifica"));
		notificaSottoscrizione.setIdUtente(rs.getString("idutente"));
		notificaSottoscrizione.setIdNodo(rs.getString("idnodo"));
		notificaSottoscrizione.setIdDocumento(rs.getString("iddocumento"));
		notificaSottoscrizione.setIdEvento(rs.getInt("idevento"));
		notificaSottoscrizione.setIdRuolo(rs.getInt("idruolo"));
		notificaSottoscrizione.setStatoNotifica(rs.getInt("statonotifica"));
		notificaSottoscrizione.setDataEvento(rs.getDate("dataevento"));
		notificaSottoscrizione.setDescErrore(rs.getString("descerrore"));
		notificaSottoscrizione.setDataInvio(rs.getDate("datainvio"));
		return notificaSottoscrizione;
	}
	
	private static NotificaDTO populateVOForLista(final ResultSet rs) throws SQLException {
		final NotificaDTO notificaSottoscrizione = new NotificaDTO();
		notificaSottoscrizione.setIdNotifica(rs.getInt("idnotifica"));
		notificaSottoscrizione.setIdUtente(rs.getString("idutente"));
		notificaSottoscrizione.setIdNodo(rs.getString("idnodo"));
		notificaSottoscrizione.setIdDocumento(rs.getString("iddocumento"));
		notificaSottoscrizione.setIdEvento(rs.getInt("idevento"));
		notificaSottoscrizione.setStatoNotifica(rs.getInt("statonotifica"));
		notificaSottoscrizione.setDataEvento(rs.getDate("dataevento"));
		notificaSottoscrizione.setDescErrore(rs.getString("descerrore"));
		notificaSottoscrizione.setDataInvio(rs.getDate("datainvio"));
		notificaSottoscrizione.setDescrizioneEvento(rs.getString("descrizioneevento"));
		notificaSottoscrizione.setIdAoo(rs.getLong("idaoo"));
		notificaSottoscrizione.setVisualizzato(rs.getInt("visualizzato"));
		notificaSottoscrizione.setEliminato(rs.getInt("eliminato"));
		return notificaSottoscrizione;
	}
	
	/**
	 * @see it.ibm.red.business.dao.INotificaDAO#updateNotifica(int,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public int updateNotifica(final int idNotifica, final String descErrore, final Connection connection) {
		int stato = NotificaDTO.ERRORE;
		Date dataInvio = null;
		if ("".equals(descErrore)) {
			stato = NotificaDTO.INVIATO;
			dataInvio = new java.sql.Date(new Date().getTime());
		}
		
		return updateNotifica(idNotifica, stato, dataInvio, descErrore, connection);
	}
	
	/**
	 * @see it.ibm.red.business.dao.INotificaDAO#updateNotifica(it.ibm.red.business.dto.NotificaDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public int updateNotifica(final NotificaDTO notifica, final Connection con) {
		return updateNotifica(notifica.getIdNotifica(), notifica.getStatoNotifica(), notifica.getDataInvio(), notifica.getDescErrore(), con);
	}
	
	private static int updateNotifica(final int idNotifica, final int stato, final Date dataInvio, final String descErrore, final Connection con) {
		PreparedStatement ps = null;
		int result = 0;
		
		try {
			int index = 1;
			final String sql = "update SE_NOTIFICA_SOTTOSCRIZIONE " 
					+ " set STATONOTIFICA = ?, "
					+ " DATAINVIO = ?, "
					+ " DESCERRORE = ? "
					+ " where idnotifica = ?";
			ps = con.prepareStatement(sql);
			ps.setInt(index++, stato);
			if (dataInvio != null) {
				ps.setDate(index++, new java.sql.Date(dataInvio.getTime()));
			} else {
				ps.setNull(index++, Types.DATE);
			}
			ps.setString(index++, descErrore);
			ps.setInt(index++, idNotifica);
			result = ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento della notifica con id: " + idNotifica + ".", e);
			throw new RedException("Errore durante l'aggiornamento della notifica con id: " + idNotifica + ".");
		} finally {
			closeStatement(ps);
		}
		
		return result;
	}
	
	/**
	 * @see it.ibm.red.business.dao.INotificaDAO#isPresente(it.ibm.red.business.dto.NotificaDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean isPresente(final NotificaDTO notifica, final Connection con) {
		int executionResult = 0;
		int index = 1;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final String sql = "select count(*) NUM_NOTIFICHE from SE_NOTIFICA_SOTTOSCRIZIONE " 
					+ " where IDNODO = ? "
					+ " and IDUTENTE = ? "
					+ " and IDDOCUMENTO = ? "
					+ " and IDEVENTO = ? "
					+ " and DATAEVENTO = TRUNC(SYSDATE)";
			ps = con.prepareStatement(sql);
			ps.setString(index++, notifica.getIdNodo());
			ps.setString(index++, notifica.getIdUtente());
			ps.setString(index++, notifica.getIdDocumento());
			ps.setInt(index++, notifica.getIdEvento());
			rs = ps.executeQuery();
			if (rs.next()) {
				executionResult = rs.getInt("NUM_NOTIFICHE");
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la ricerca della notifica.", e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
		
		return executionResult > 0;
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaDAO#getNotifiche(java.lang.Long,
	 *      java.lang.Long, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public List<NotificaDTO> getNotifiche(final Long idAoo, final Long idUtente, final Integer aPartireDaNgiorni, final Connection connection) {
		PreparedStatement ps = null;
		Integer index = 1;
		
		try {

			final StringBuilder query = new StringBuilder("SELECT SN.*, TE.DESCRIZIONEEVENTO, TE.DETTAGLIOEVENTO, N.IDNODO, N.IDAOO " 
					+ "  FROM SE_NOTIFICA_SOTTOSCRIZIONE SN, SE_TIPO_EVENTO TE, NODO N " 
					+ " WHERE SN.IDEVENTO = TE.IDEVENTO "
					+ "   AND N.IDNODO = SN.IDNODO "
					+ "   AND SN.IDUTENTE = ? " 
					+ "   AND SN.DATAEVENTO > (SYSDATE - ?) " 
					+ "   AND N.IDAOO = ? " 
					+ "   AND SN.ELIMINATO = 0 " 
					+ "   AND VISUALIZZATO IN (0, 1) " 
					+ " ORDER BY SN.DATAEVENTO DESC");			
			
			ps = connection.prepareStatement(query.toString());
			ps.setInt(index++, idUtente.intValue());
			ps.setInt(index++, aPartireDaNgiorni);
			ps.setLong(index++, idAoo);
			
			return recuperaLista(ps);
		
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_NOTIFICA_MSG, e);
			throw new RedException(ERROR_RECUPERO_NOTIFICA_MSG);
		} finally {
			closeStatement(ps);
		}
	}
	
	/**
	 * @see it.ibm.red.business.dao.INotificaDAO#getNotificheHomepage(java.lang.Long,
	 *      java.lang.Long, java.lang.Integer, java.lang.Integer, int,
	 *      java.sql.Connection).
	 */
	@Override
	public List<NotificaDTO> getNotificheHomepage(final Long idAoo, final Long idUtente, final Integer aPartireDaNgiorni, final Integer maxRowNum, final int maxNotificheNonLette, final Connection connection) {
		PreparedStatement ps = null;
		Integer index = 1;
		
		try {

			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM(SELECT SN.*,TE.DESCRIZIONEEVENTO, TE.DETTAGLIOEVENTO, N.IDAOO "); 
			sb.append(FROM_SE_NOTIFICA_SOTTOSCRIZIONE_SN_SE_TIPO_EVENTO_TE_NODO_N); 
			sb.append(WHERE_SN_IDEVENTO_TE_IDEVENTO_AND_N_IDNODO_SN_IDNODO_AND_SN_IDUTENTE); 
			sb.append("AND N.IDAOO = ? AND (SN.ELIMINATO = 0 or SN.ELIMINATO is null) AND (VISUALIZZATO = 0 or VISUALIZZATO is null) ");
			if (maxNotificheNonLette > 0) {
				sb.append("AND SN.DATAEVENTO > (SYSDATE - ?) ");
			}
			sb.append("UNION ALL ");
			sb.append("SELECT SN.*, TE.DESCRIZIONEEVENTO, TE.DETTAGLIOEVENTO,N.IDAOO ");
			sb.append(FROM_SE_NOTIFICA_SOTTOSCRIZIONE_SN_SE_TIPO_EVENTO_TE_NODO_N); 
			sb.append(WHERE_SN_IDEVENTO_TE_IDEVENTO_AND_N_IDNODO_SN_IDNODO_AND_SN_IDUTENTE);
			sb.append("AND (SN.ELIMINATO = 0 or SN.ELIMINATO is null) ");
			sb.append("AND N.IDAOO = ? AND VISUALIZZATO = 1  AND SN.DATAEVENTO > (SYSDATE - ?) ");
			if (aPartireDaNgiorni > 0) {
				sb.append("AND ROWNUM <? ");	
			}
			
			sb.append(") RISULTATI ");
		 	sb.append("ORDER BY RISULTATI.DATAEVENTO ASC ");			
			 
			 
			ps = connection.prepareStatement(sb.toString());
			ps.setInt(index++, idUtente.intValue());
			ps.setLong(index++, idAoo);
			if (maxNotificheNonLette > 0) {
				ps.setInt(index++, maxNotificheNonLette);
			}
			ps.setInt(index++, idUtente.intValue());
			ps.setLong(index++, idAoo);
			if (aPartireDaNgiorni > 0) {
				ps.setInt(index++, aPartireDaNgiorni);	
			}
			
			ps.setInt(index++, maxRowNum);

			return recuperaLista(ps);
		
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_NOTIFICA_MSG, e);
			throw new RedException(ERROR_RECUPERO_NOTIFICA_MSG);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaDAO#eliminaNotifica(int, java.sql.Connection).
	 */
	@Override
	public boolean eliminaNotifica(final int idNotifica, final Connection con) {
		final String sql = "UPDATE SE_NOTIFICA_SOTTOSCRIZIONE SET ELIMINATO = 1 WHERE IDNOTIFICA = ?";
		return aggiornaNotifica(idNotifica, con, sql);
	}
	
	private static boolean aggiornaNotifica(final int idNotifica, final Connection con, final String sql) {
		int executionResult = 0;
		int index = 1;
		PreparedStatement ps = null;
				
		try {
			
			ps = con.prepareStatement(sql);
			ps.setInt(index++, idNotifica);
			executionResult = ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento della notifica.", e);
		} finally {
			closeStatement(ps);
		}
		
		return executionResult > 0;
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaDAO#contrassegnaNotifica(int,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean contrassegnaNotifica(final int idNotifica, final Connection con) {
		final String sql = "UPDATE SE_NOTIFICA_SOTTOSCRIZIONE SET VISUALIZZATO = 1 WHERE IDNOTIFICA = ?";
		return aggiornaNotifica(idNotifica, con, sql);
	}

	/**
	 * @see it.ibm.red.business.dao.INotificaDAO#getNotifiche(java.lang.Integer,
	 *      java.lang.Long, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<NotificaDTO> getNotifiche(final Integer idNotifica, final Long idUtente, final Long idAoo, final Connection connection) {
		PreparedStatement ps = null;
		Integer index = 1;
		
		try {

			final StringBuilder query = new StringBuilder("SELECT SN.*, TE.DESCRIZIONEEVENTO, TE.DETTAGLIOEVENTO, N.IDNODO, N.IDAOO " 
					+ " FROM SE_NOTIFICA_SOTTOSCRIZIONE SN, SE_TIPO_EVENTO TE, NODO N " 
					+ " WHERE SN.IDEVENTO = TE.IDEVENTO "
					+ " AND N.IDNODO = SN.IDNODO "
					+ " AND SN.IDNOTIFICA = ?"
					+ " AND SN.IDUTENTE = ? " 
					+ " AND N.IDAOO = ? " 
					+ " AND SN.ELIMINATO = 0 " 
					+ " AND VISUALIZZATO IN (0, 1) " 
					+ " ORDER BY SN.DATAEVENTO DESC");			
			
			ps = connection.prepareStatement(query.toString());
			ps.setInt(index++, idNotifica.intValue()); 
			ps.setInt(index++, idUtente.intValue()); 
			ps.setLong(index++, idAoo);
			
			return recuperaLista(ps);
		
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_NOTIFICA_MSG, e);
			throw new RedException(ERROR_RECUPERO_NOTIFICA_MSG);
		} finally {
			closeStatement(ps);
		}
	}
	
	private static List<NotificaDTO> recuperaLista(final PreparedStatement ps) {
		
		final List<NotificaDTO> notifiche = new ArrayList<>();
		ResultSet rs = null;
		
		try{ 
			rs = ps.executeQuery();
			
			while (rs.next()) {
				notifiche.add(populateVOForLista(rs));
			} 
			
			return notifiche;
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_NOTIFICA_MSG, e);
			throw new RedException(ERROR_RECUPERO_NOTIFICA_MSG);
		} finally {
			closeResultset(rs);
		}	
	}
	
	/**
	 * @see it.ibm.red.business.dao.INotificaDAO#getCountNotificheHomepage(java.lang.Long,
	 *      java.lang.Long, java.lang.Integer, int, java.sql.Connection).
	 */
	@Override
	public Integer getCountNotificheHomepage(final Long idAoo, final Long idUtente,
			final Integer maxRowNum, final int maxNotificheNonLette, final Connection connection) {
		Integer contatoreOutput = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer index = 1;
		
		try { 
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT count(*) as counter "); 
			sb.append(FROM_SE_NOTIFICA_SOTTOSCRIZIONE_SN_SE_TIPO_EVENTO_TE_NODO_N); 
			sb.append(WHERE_SN_IDEVENTO_TE_IDEVENTO_AND_N_IDNODO_SN_IDNODO_AND_SN_IDUTENTE); 
			sb.append("AND N.IDAOO = ? AND (SN.ELIMINATO = 0 or SN.ELIMINATO is null) AND (VISUALIZZATO = 0 or VISUALIZZATO is null) ");
			if (maxNotificheNonLette > 0) {
				sb.append("AND SN.DATAEVENTO > (SYSDATE - ?) ");
			}
			 		  
			ps = connection.prepareStatement(sb.toString());
			ps.setInt(index++, idUtente.intValue());
			ps.setLong(index++, idAoo);
			if (maxNotificheNonLette > 0) {
				ps.setInt(index++, maxNotificheNonLette);
			}
			 
			rs = ps.executeQuery();
			
			while (rs.next()) {
				contatoreOutput = rs.getInt("counter");
			}  
			return contatoreOutput; 
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la count recupero della notifica ", e);
			throw new RedException("Errore durante la count recupero della notifica");
		} finally {
			closeStatement(ps, rs);
		}
	}
}
