package it.ibm.red.business.dto;

/**
 * DTO per la gestione delle informazioni su un estratto conto UCB.
 */
public class EstrattiContoUcbDTO extends AbstractDTO {

	/**
	 * serial version UID.
	 */
	private static final long serialVersionUID = 2492195449475160555L;

	/**
	 * Esercizio di riferimento.
	 */
	private Integer esercizio;
	
	/**
	 * Trimestre di riferimento.
	 */
	private Integer trimestre;
	
	/**
	 * Codice sede estera di riferimento.
	 */
	private String codiceSedeEstera;
	
	/**
	 * Sede estera associata a {@link #codiceSedeEstera}.
	 */
	private String sedeEstera;
	
	/**
	 * Codice valuta.
	 */
	private String codiceValuta;
	
	/**
	 * Tasso di cambio.
	 */
	private Double cambio;
	
	/**
	 * Saldo 1.
	 */
	private Double saldoCCVT1;
	
	/**
	 * Saldo 2.
	 */
	private Double saldoCCVT2;
	
	/**
	 * Saldo 3.
	 */
	private Double saldoCCVT3;
	
	/**
	 * Saldo 4.
	 */
	private Double saldoCCVT4;

	/**
	 * Costruttore completo.
	 * @param esercizio
	 * @param trimestre
	 * @param codiceSedeEstera
	 * @param sedeEstera
	 * @param codiceValuta
	 * @param cambio
	 * @param saldoCCVT1
	 * @param saldoCCVT2
	 * @param saldoCCVT3
	 * @param saldoCCVT4
	 */
	public EstrattiContoUcbDTO(Integer esercizio, Integer trimestre, String codiceSedeEstera, String sedeEstera,
			String codiceValuta, Double cambio, Double saldoCCVT1, Double saldoCCVT2, Double saldoCCVT3, Double saldoCCVT4) {
		super();
		this.esercizio = esercizio;
		this.trimestre = trimestre;
		this.codiceSedeEstera = codiceSedeEstera;
		this.sedeEstera = sedeEstera;
		this.codiceValuta = codiceValuta;
		this.cambio = cambio;
		this.saldoCCVT1 = saldoCCVT1;
		this.saldoCCVT2 = saldoCCVT2;
		this.saldoCCVT3 = saldoCCVT3;
		this.saldoCCVT4 = saldoCCVT4;
	}

	/**
	 * Restituisce l'anno di riferimento.
	 * @return esercizio
	 */
	public Integer getEsercizio() {
		return esercizio;
	}

	/**
	 * Imposta l'anno di riferimento.
	 * @param esercizio
	 */
	public void setEsercizio(Integer esercizio) {
		this.esercizio = esercizio;
	}

	/**
	 * Restituisce il trimestre di riferimento.
	 * @return
	 */
	public Integer getTrimestre() {
		return trimestre;
	}

	/**
	 * Imposta il trimestre di riferimento.
	 * @param trimestre
	 */
	public void setTrimestre(Integer trimestre) {
		this.trimestre = trimestre;
	}

	/**
	 * Restituisce il codice sede estera.
	 * @return codice sede estera
	 */
	public String getCodiceSedeEstera() {
		return codiceSedeEstera;
	}

	/**
	 * Imposta il codice sede estera.
	 * @param codiceSedeEstera
	 */
	public void setCodiceSedeEstera(String codiceSedeEstera) {
		this.codiceSedeEstera = codiceSedeEstera;
	}

	/**
	 * Restituisce la sede estera di riferimento.
	 * @return sede estera
	 */
	public String getSedeEstera() {
		return sedeEstera;
	}

	/**
	 * Imposta la sede estera.
	 * @param sedeEstera
	 */
	public void setSedeEstera(String sedeEstera) {
		this.sedeEstera = sedeEstera;
	}

	/**
	 * Restituisce il codice valuta.
	 * @return codice valuta
	 */
	public String getCodiceValuta() {
		return codiceValuta;
	}

	/**
	 * Imposta il codice valuta.
	 * @param codiceValuta
	 */
	public void setCodiceValuta(String codiceValuta) {
		this.codiceValuta = codiceValuta;
	}

	/**
	 * Restituisce il tasso di cambio.
	 * @return tasso di cambio
	 */
	public Double getCambio() {
		return cambio;
	}

	/**
	 * Imposta il tasso di cambio.
	 * @param cambio
	 */
	public void setCambio(Double cambio) {
		this.cambio = cambio;
	}

	/**
	 * Restituisce saldo 1.
	 * @return saldo 1
	 */
	public Double getSaldoCCVT1() {
		return saldoCCVT1;
	}

	/**
	 * Imposta il saldo 1.
	 * @param saldoCCVT1
	 */
	public void setSaldoCCVT1(Double saldoCCVT1) {
		this.saldoCCVT1 = saldoCCVT1;
	}

	/**
	 * Restituisce il saldo 2.
	 * @return saldo 2
	 */
	public Double getSaldoCCVT2() {
		return saldoCCVT2;
	}

	/**
	 * Imposta il saldo 2.
	 * @param saldoCCVT2
	 */
	public void setSaldoCCVT2(Double saldoCCVT2) {
		this.saldoCCVT2 = saldoCCVT2;
	}

	/**
	 * Restituisce il saldo 3.
	 * @return saldo 3
	 */
	public Double getSaldoCCVT3() {
		return saldoCCVT3;
	}

	/**
	 * Imposta il saldo 3.
	 * @param saldoCCVT3
	 */
	public void setSaldoCCVT3(Double saldoCCVT3) {
		this.saldoCCVT3 = saldoCCVT3;
	}

	/**
	 * Restituisce il saldo 4.
	 * @return saldo 4
	 */
	public Double getSaldoCCVT4() {
		return saldoCCVT4;
	}

	/**
	 * Imposta il saldo 4.
	 * @param saldoCCVT4
	 */
	public void setSaldoCCVT4(Double saldoCCVT4) {
		this.saldoCCVT4 = saldoCCVT4;
	}
}
