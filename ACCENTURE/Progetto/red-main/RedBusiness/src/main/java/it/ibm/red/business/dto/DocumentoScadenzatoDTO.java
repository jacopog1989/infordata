package it.ibm.red.business.dto;

import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.utils.DateUtils;

/**
 * DTO documento scadenzato.
 */
public class DocumentoScadenzatoDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Documento.
	 */
	private IdDocumentDTO ce;
	
	/**
	 * Dati process engine.
	 */
	private PEDocumentoScadenzatoDTO pe;
	
	/**
	 * Ufficio destinatario.
	 */
	private UfficioDTO ufficioDestinatario;
	
	/**
	 * Utente destinatario.
	 */
	private Utente utenteDestinatario;

	/**
	 * Costruttore del DTO.
	 * @param inCe
	 * @param inPe
	 * @param inUfficio
	 * @param inUtente
	 */
	public DocumentoScadenzatoDTO(final IdDocumentDTO inCe, final PEDocumentoScadenzatoDTO inPe, final UfficioDTO inUfficio, final Utente inUtente) {
		this.ce = inCe;
		this.pe = inPe;
		
		this.ufficioDestinatario = inUfficio;
		this.utenteDestinatario = inUtente;
	}

	/**
	 * Restituisce il CE.
	 * @return IdDocumentoDTO
	 */
	public IdDocumentDTO getCe() {
		return ce;
	}

	/**
	 * Restituisce il PE.
	 * @return PEDocumentoScadenzatoDTO
	 */
	public PEDocumentoScadenzatoDTO getPe() {
		return pe;
	}

	/**
	 * Restituisce l'ufficio del destinatario.
	 * @return ufficio destinatario
	 */
	public UfficioDTO getUfficioDestinatario() {
		return ufficioDestinatario;
	}

	/**
	 * Imposta l'ufficio del destinatario.
	 * @param ufficioDestinatario
	 */
	public void setUfficioDestinatario(final UfficioDTO ufficioDestinatario) {
		this.ufficioDestinatario = ufficioDestinatario;
	}

	/**
	 * Restituisce l'utente destinatario.
	 * @return utente destinatario
	 */
	public Utente getUtenteDestinatario() {
		return utenteDestinatario;
	}

	/**
	 * Imposta l'utente destinatario.
	 * @param utenteDestinatario
	 */
	public void setUtenteDestinatario(final Utente utenteDestinatario) {
		this.utenteDestinatario = utenteDestinatario;
	}

	/**
	 * Restituisce il tipo assegnazione.
	 * @see TipoAssegnazioneEnum.
	 * @return tipo assegnazione
	 */
	public String getTipoAssegnazione() {
		Integer idTipoAssegnazione = pe.getTipoAssegnazioneId();
		return TipoAssegnazioneEnum.get(idTipoAssegnazione).getDescrizione();
	}

	/**
	 * Restituisce l'associazione tra numero documento e data creazione o se esistenti
	 * numero protocollo e anno protocollo.
	 * @return di
	 */
	public String getDocumento() {
		String documento = "";
		if (ce.getAnnoProtocollo() != null && ce.getNumeroProtocollo() != null) {
			documento = ce.getNumeroProtocollo() + " / " + ce.getAnnoProtocollo() + " - " + ce.getNumeroDocumento().toString(); 
		} else if (ce.getNumeroDocumento() != null) {
			documento = ce.getNumeroDocumento().toString() + " - " + DateUtils.dateToString(ce.getDataCreazione(), "yyyy"); 
		}
		return documento;
	}

	/**
	 * Restituisce il destinatario come concatenazione di nome e cognome se un utente,
	 * descrizione ufficio se un ufficio.
	 * @return descrizione del destinatario
	 */
	public String getDestinatario() {
		String destinatario = "";
		if (ufficioDestinatario != null) {
			destinatario = ufficioDestinatario.getDescrizione();
		}
		if (utenteDestinatario != null) {
			if (StringUtils.isNotBlank(destinatario)) {
				destinatario += " - ";
			}
			destinatario +=  utenteDestinatario.getNome() + " " + utenteDestinatario.getCognome();
		} 
		
		return destinatario;
	}

	/**
	 * Verifica che {@link #utenteDestinatario} sia lo stesso utente passato come parametro.
	 * @param utente
	 * @return true se sono uguale, false altrimenti
	 */
	public boolean isDestinatario(final UtenteDTO utente) {
		return utenteDestinatario != null && utente.getId().equals(utenteDestinatario.getIdUtente());
	}
}
