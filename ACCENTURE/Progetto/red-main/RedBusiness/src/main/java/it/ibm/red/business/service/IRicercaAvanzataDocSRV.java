package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.service.facade.IRicercaAvanzataDocFacadeSRV;

/**
 * The Interface IRicercaAvanzataDocSRV.
 *
 * @author m.crescentini
 * 
 *         Interfaccia del servizio per la gestione della ricerca avanzata dei documenti.
 */
public interface IRicercaAvanzataDocSRV extends IRicercaAvanzataDocFacadeSRV {

	/**
	 * Metodo per il recupero delle tipologie Documento per popolare la view.
	 * 
	 * @param con
	 * @param idAoo
	 * @return
	 */
	List<TipologiaDocumentoDTO> getListaTipologieDocumento(Connection con, Long idAoo);

}