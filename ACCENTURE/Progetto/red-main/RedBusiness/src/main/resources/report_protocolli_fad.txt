<html>
<head>
<title>Documento</title>
<style type="text/css">

@page {
    size: 210mm 500mm
}

.docsubtitle { 
	font-family: sans-serif; 
	font-size: 16px;
	font-weight: bold;
	font-style: italic;
	color: #0F3B82;	
}

.z-vlayout-inner {
	width: 480px;
	border-bottom: 2px solid #7EAAC6;
}

.scriptaSection{
	color: #0F3B82;
    font-family: sans-serif; 
    font-size: 18px;
    font-style: normal;
    font-weight: normal;
}
.scripta{
	font-family: sans-serif; 
	font-size: 12px;
    font-style: normal;
}
.scriptaLabelBold{
	font-family: sans-serif; 
	font-size: 12px;
    font-style: italic;
	font-weight: bold;
	color: black;
}
.scriptaBold{
	font-family: sans-serif; 
	font-size: 14px;
    font-style: normal;
	font-weight: bold;
}
.msgQueue {
    background-color: #F5FAFF;
    border-bottom: 1px solid #EEF0FF;
    height: 26px;
}
.z-tabs-scroll .z-tabs-space {
    background: none repeat scroll 0 0 transparent;
    border: 0 none;
    height: auto;
}
.z-tabs .z-tabs-cnt {
    background: none repeat scroll 0 0 transparent;
    border-bottom: 1px solid #7EAAC6;
    display: block;
    list-style: none outside none;
    margin: 0;
    padding-left: 5px;
    
}
.z-tabs, .z-tabs-ver {
    background: none repeat scroll 0 0 transparent;
    border: 0 none;
    margin: 0;
    overflow: hidden;
    padding: 0;
    position: relative;
}
.z-tabs-scroll {
    background: none repeat scroll 0 0 #C7E3F3;
    border: 1px solid #7EAAC6;
}
.z-tabs-header {
    margin: 0;
    overflow: hidden;
    position: relative;
    width: 100%;
}
.z-tabs-header .z-clear {
    height: 0;
}
.z-tabs-scroll .z-tabs-cnt {
    -moz-user-select: none;
    border-bottom: 1px solid #7EAAC6;
    display: block;
    list-style: none outside none;
    margin: 0;
    padding-left: 5px;
    padding-top: 1px;
	height: 25px;
}
.z-tabs-cnt li {
    -moz-user-select: none;
    cursor: default;
    display: block;
    text-align: center;
    margin: 0;
    padding: 0;
    position: relative;
}

.z-tabpanel, .z-tabbox-ver .z-tabpanels-ver {
    border-color: #7EAAC6;
    border-style: solid;
    border-width: 1px;
	padding: 3px;
}

</style>
</head>
<body>
<form name="frm">
<table>

	<tr>
		<td>
			<div id="pJHP76" class="z-tabs z-tabs-scroll" style="width: 500px;">
					<ul id="pJHP76-cave" class="z-tabs-cnt">
						<li id="pJHP86" class="z-tab z-tab-seld">
							<div id="pJHP86-hr" class="z-tab-hr">
								<div id="pJHP86-hm" class="z-tab-hm " >
									<span class="scriptaSection">SEGNATURE DI PROTOCOLLO</span>
								</div>										
							</div>
						</li>
					</ul>					
				</div>
		</td>
	</tr>
	
	<tr>
		<td>
			<div style="width: 500px; padding-left: 10px; padding-top: 10px;">
								
				<div id="pJHP56-chdex" class="z-vlayout-inner" >
					<span class="docsubtitle">Protocollo in uscita da Amministrazione mittente</span>
				 </div>
				 <table border="0" style="width: 650px;">
				 	 <tr>
					 	<td width="40%"><label class="scriptaLabelBold">AOO :</label></td>
					 	<td><label class="scripta">{AOO_PROT_AMM}</label></td>
					 </tr>
					 
					 <tr>
					 	<td><label class="scriptaLabelBold">Numero protocollo :</label></td>
					 	<td><label class="scripta">{NUM_PROT_AMM}</label></td>
					 </tr>
					 
					 <tr>
					 	<td><label class="scriptaLabelBold">Data protocollo :</label></td>
					 	<td><label class="scripta">{DATA_PROT_AMM}</label></td>
					 </tr>
				 </table>
				 
				 <br/>
				 
				 <div id="pJHP56-chdex" class="z-vlayout-inner" >
					<span class="docsubtitle">Protocollo in ingresso UCB</span>
				 </div>
				 
				 <table border="0" style="width: 650px;">
				 	 <tr>
					 	<td width="40%"><label class="scriptaLabelBold">AOO :</label></td>
					 	<td><label class="scripta">{AOO_PROT_UCB_INGRESSO}</label></td>
					 </tr>
					 
					 <tr>
					 	<td><label class="scriptaLabelBold">Numero protocollo :</label></td>
					 	<td><label class="scripta">{NUM_PROT_UCB_INGRESSO}</label></td>
					 </tr>
					 
					 <tr>
					 	<td><label class="scriptaLabelBold">Data protocollo :</label></td>
					 	<td><label class="scripta">{DATA_PROT_UCB_INGRESSO}</label></td>
					 </tr>
				 </table>
				 
				 <br/>
				 
				 <div id="pJHP56-chdex" class="z-vlayout-inner" >
					<span class="docsubtitle">Protocollo in uscita UCB</span>
				 </div>
				 <table border="0" style="width: 650px;">
				 	 <tr>
					 	<td width="40%"><label class="scriptaLabelBold">AOO :</label></td>
					 	<td><label class="scripta">{AOO_PROT_UCB_USCITA}</label></td>
					 </tr>
					 
					 <tr>
					 	<td><label class="scriptaLabelBold">Numero protocollo :</label></td>
					 	<td><label class="scripta">{NUM_PROT_UCB_USCITA}</label></td>
					 </tr>
					 
					 <tr>
					 	<td><label class="scriptaLabelBold">Data protocollo :</label></td>
					 	<td><label class="scripta">{DATA_PROT_UCB_USCITA}</label></td>
					 </tr>
				 </table>
				 
				 <br/>
				 
				 <div id="pJHP56-chdex" class="z-vlayout-inner" >
					<span class="docsubtitle">Protocollo in ingresso MEF-RGS</span>
				 </div>
				 <table border="0" style="width: 650px;">
				 	 <tr>
					 	<td width="40%"><label class="scriptaLabelBold">AOO :</label></td>
					 	<td><label class="scripta">{AOO_PROT_RED}</label></td>
					 </tr>
					 
					 <tr>
					 	<td><label class="scriptaLabelBold">Numero protocollo :</label></td>
					 	<td><label class="scripta">{NUM_PROT_RED}</label></td>
					 </tr>
					 
					 <tr>
					 	<td><label class="scriptaLabelBold">Data protocollo :</label></td>
					 	<td><label class="scripta">{DATA_PROT_RED}</label></td>
					 </tr>
				 </table>
			 </div>
		</td>
	</tr>
</table>

</form>
</body>
</html>