<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	extension-element-prefixes="date" xmlns:date="http://exslt.org/dates-and-times"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:output method="xml" indent="yes"/>

	<xsl:attribute-set name="title">
		<xsl:attribute name="font-family">'Times New Roman', Times, serif</xsl:attribute>
		<xsl:attribute name="font-size">18pt</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="text">
		<xsl:attribute name="font-family">'Times New Roman', Times, serif</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="line-height">18pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="centered_text">
		<xsl:attribute name="font-family">'Times New Roman', Times, serif</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="line-height">18pt</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="bold_text" use-attribute-sets="text">
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="table_title">
		<xsl:attribute name="color">#FFFFFF</xsl:attribute>
		<xsl:attribute name="font-family">'Times New Roman', Times, serif</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="line-height">18pt</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="table_head">
		<xsl:attribute name="border">1pt solid black</xsl:attribute>
		<xsl:attribute name="height">0.5cm</xsl:attribute>
		<xsl:attribute name="background-color">#808080</xsl:attribute>
		<xsl:attribute name="display-align">center</xsl:attribute>
		<xsl:attribute name="padding">2pt</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:template name="VoidLine5pt">
		<fo:block white-space-collapse="false" font-size="5pt">&#x00A0;</fo:block>
	</xsl:template>

	<xsl:template name="VoidLine14pt">
		<fo:block white-space-collapse="false" font-size="14pt">&#x00A0;</fo:block>
	</xsl:template>

	<xsl:template match="/parameters">
		<fo:root>
			<fo:layout-master-set>
				<fo:simple-page-master master-name="A4-portrait" page-height="21.0cm" page-width="29.7cm" margin="2cm">
					<fo:region-body/>
					<fo:region-after region-name="xsl-region-after" extent=".5in"/>
        		</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="A4-portrait" id="end">
				<fo:static-content flow-name="xsl-region-after">
					<xsl:call-template name="VoidLine14pt"/>
					<xsl:call-template name="VoidLine14pt"/>
					<fo:table width="100%">
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell column-number="1">
										<fo:block xsl:use-attribute-sets="text" text-align="left"><xsl:value-of select="./timestamp_footer"/></fo:block>
									</fo:table-cell>
									<fo:table-cell column-number="2">
										<fo:block xsl:use-attribute-sets="text" text-align="right">Pagina <fo:page-number/> di <fo:page-number-citation-last ref-id="end"/></fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
				</fo:static-content>
				
				<fo:flow flow-name="xsl-region-body">
					<fo:block>
						<xsl:call-template name="VoidLine5pt"/>
						<fo:block xsl:use-attribute-sets="title">
							Elenco Divisionale
				        </fo:block>
						
						<xsl:call-template name="VoidLine14pt"/>
						<xsl:call-template name="VoidLine14pt"/>
						
						<fo:table width="100%">
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell column-number="1">
										<fo:block text-align="left" xsl:use-attribute-sets="text">Aoo di appartenenza: <xsl:value-of select="./desc_aoo"/></fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell column-number="1">
										<fo:block text-align="left" xsl:use-attribute-sets="text">Ufficio assegnante: <xsl:value-of select="./ufficio_assegnante"/></fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell column-number="1">
										<fo:block text-align="left" xsl:use-attribute-sets="text">Ufficio assegnatario: <xsl:value-of select="./ufficio_assegnatario"/></fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell column-number="1">
										<xsl:choose>
											<xsl:when test="./utente_assegnatario = ''">
												<fo:block text-align="left" xsl:use-attribute-sets="text">Utente assegnatario: Nessun utente selezionato</fo:block>
											</xsl:when>
											<xsl:otherwise>
												<fo:block text-align="left" xsl:use-attribute-sets="text">Utente assegnatario: <xsl:value-of select="./utente_assegnatario"/></fo:block>
											</xsl:otherwise>
										</xsl:choose>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell column-number="1">
										<xsl:choose>
											<xsl:when test="./tipologia_documento = ''">
												<fo:block text-align="left" xsl:use-attribute-sets="text">Tipo documento selezionato: Nessun tipo documento selezionato</fo:block>
											</xsl:when>
											<xsl:otherwise>
												<fo:block text-align="left" xsl:use-attribute-sets="text">Tipo documento selezionato: <xsl:value-of select="./tipologia_documento"/></fo:block>
											</xsl:otherwise>
										</xsl:choose>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell column-number="1">
										<fo:block text-align="left" xsl:use-attribute-sets="text">Data assegnazione: <xsl:value-of select="./data_assegnazione"/></fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>

						<xsl:call-template name="VoidLine14pt"/>
						<xsl:call-template name="VoidLine14pt"/>

						<fo:table width="100%">
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell column-number="1">
										<fo:block text-align="left" xsl:use-attribute-sets="text">Stampa del <xsl:value-of select="./giorno_stampa"/> alle <xsl:value-of select="./orario_stampa"/></fo:block>
									</fo:table-cell>
									<fo:table-cell column-number="2">
										<fo:block text-align="left" xsl:use-attribute-sets="text">Risultati restituiti: <xsl:value-of select="./numero_risultati"/></fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						
						<xsl:call-template name="VoidLine5pt"/>
						
						<fo:table border="1pt solid black">
							<fo:table-header border="inherit">
								<fo:table-row border="inherit">
									<fo:table-cell xsl:use-attribute-sets="table_head">
										<fo:block xsl:use-attribute-sets="table_title">Numero/Data</fo:block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="table_head">
										<fo:block xsl:use-attribute-sets="table_title">Data protocollo</fo:block>
									</fo:table-cell >
									<fo:table-cell xsl:use-attribute-sets="table_head">
										<fo:block xsl:use-attribute-sets="table_title">Mittente</fo:block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="table_head">
										<fo:block xsl:use-attribute-sets="table_title">Classificazione</fo:block>
									</fo:table-cell>
									<fo:table-cell xsl:use-attribute-sets="table_head">
										<fo:block xsl:use-attribute-sets="table_title">Oggetto</fo:block>
									</fo:table-cell>
									<xsl:choose>
										<xsl:when test="./tipologia_documento = ''">
											<fo:table-cell xsl:use-attribute-sets="table_head">
												<fo:block xsl:use-attribute-sets="table_title">Tipo documento</fo:block>
											</fo:table-cell>
										</xsl:when>
									</xsl:choose>
									<fo:table-cell xsl:use-attribute-sets="table_head">
										<fo:block xsl:use-attribute-sets="table_title">Utente assegnatario</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-header>

							<fo:table-body border="inherit">
								<xsl:for-each select="./risultati/risultato">
									<fo:table-row border="1pt solid gray">
										<fo:table-cell>
											<fo:block xsl:use-attribute-sets="centered_text"><xsl:value-of select="./numero_data" /></fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block xsl:use-attribute-sets="centered_text"><xsl:value-of select="./data_protocollo" /></fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block xsl:use-attribute-sets="centered_text"><xsl:value-of select="./mittente" /></fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block xsl:use-attribute-sets="centered_text"><xsl:value-of select="./classificazione" /></fo:block>
										</fo:table-cell>
										<fo:table-cell >
											<fo:block xsl:use-attribute-sets="centered_text"><xsl:value-of select="./oggetto" /></fo:block>
										</fo:table-cell>
										<xsl:choose>
											<xsl:when test="/parameters/tipologia_documento = ''">
												<fo:table-cell>
													<fo:block xsl:use-attribute-sets="centered_text"><xsl:value-of select="./tipologia_documento" /></fo:block>
												</fo:table-cell>
											</xsl:when>
										</xsl:choose>
										<fo:table-cell>
											<fo:block xsl:use-attribute-sets="centered_text"><xsl:value-of select="./utente_assegnatario" /></fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:for-each>
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>