package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.RicercaPerBusinessEntityEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * Test 18.
 */
public class Test18 extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test18.class);
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue le azioni prima del test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test 18.
	 */
	@Test
	public final void test() {
		
		//Creazione documento
		UtenteDTO utente = getUtente(USERNAME_ASCI);
		String oggetto = createObj();
		String indiceFascicoloProcedimentale = "A";
		Boolean bRiservato = false;

		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(createDestinatarioEsterno(MezzoSpedizioneEnum.CARTACEO, ID_CONTATTO_GIANCARLO_ROSSI, ModalitaDestinatarioEnum.TO));

		List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneCompetenzaUfficio(ID_IGF, DESC_IGF));

		List<AllegatoDTO> allegati = null;
		EsitoSalvaDocumentoDTO esitoUscita = createDocUscita(allegati, false, oggetto, utente, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, assegnazioni, destinatari, indiceFascicoloProcedimentale, MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD, IterApprovativoDTO.ITER_FIRMA_MANUALE, bRiservato, null);
		checkAssertion(esitoUscita.isEsitoOk(), "Errore in fase di creazione documento in uscita.");

		String dt = esitoUscita.getDocumentTitle();

		//ROBERTO.NUNZI coda corriere -> Assegna a Roberto Nunzi

		EsitoOperazioneDTO esitoAssegnaCorriere = assegnaDaCorriere(USERNAME_NUNZI, dt, USERNAME_NUNZI);
		checkAssertion(esitoAssegnaCorriere.isEsito(), "Errore in fase di assegnazione corriere.");

		waitSeconds(30);
		
		// ROBERTO.NUNZI coda da lavorare -> Invio in sigla a IGF - Tanzi Gianfranco
		EsitoOperazioneDTO esitoInvioSigla = invioSigla(DocumentQueueEnum.DA_LAVORARE, USERNAME_NUNZI, dt, ID_IGF.longValue(), ID_GIANFANCO_TANZI.longValue(), false);
		checkAssertion(esitoInvioSigla.isEsito(), "Errore in fase di invio sigla.");
		
		//GIANFRANCO.TANZI coda corriere/Procedi
		EsitoOperazioneDTO esitoProcedi = procedi(USERNAME_TANZI, dt);
		checkAssertion(esitoProcedi.isEsito(), "Errore in fase di procedi");
		
		//GIANFRANCO.TANZI coda libro firma/Modifica iter/iter manuale sigla assegnatario IGF - Nunzi Roberto
		EsitoOperazioneDTO esitoModificaIter = modificaIterManuale(USERNAME_TANZI, dt, TipoAssegnazioneEnum.SIGLA, false, ID_IGF.longValue(), ID_ROBERTO_NUNZI.longValue());
		checkAssertion(esitoModificaIter.isEsito(), "Errore in fase di modifica iter");

		EsitoOperazioneDTO esitoSiglaInvia = siglaInvia(USERNAME_NUNZI, dt);
		checkAssertion(esitoSiglaInvia.isEsito(), "Errore in fase di sigla invia");

		//ROBERTO.NUNZI ricerca rapida per numero documento creato, il documento viene trovato
		Integer numDoc = getNumDoc(USERNAME_NUNZI, dt);
		Collection<MasterDocumentRedDTO> results = ricerca(numDoc + "", null, RicercaGenericaTypeEnum.TUTTE, RicercaPerBusinessEntityEnum.DOCUMENTI, USERNAME_NUNZI);
		checkAssertion(results != null && results.size() == 1, "Deve essere trovato un solo risultato.");
		
		if (results == null) {
			LOGGER.error("Collection dei master non inizializzata prima della chiamata ad un metodo della Collection.");
			throw new RedException("Collection dei master non inizializzato prima della chiamata ad un metodo della Collection.");
		}
		
		MasterDocumentRedDTO master = results.iterator().next();
		checkAssertion(master.getDocumentTitle().equals(dt), "Il document title del documento deve coincidere con quello generato.");
		
		LOGGER.info("DOCUMENT TITLE : " + dt);
		LOGGER.info("OGGETTO : " + oggetto);
		LOGGER.info("NUM DOC : " + numDoc);
	}

}
