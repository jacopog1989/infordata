package it.ibm.business.test.suite;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.filenet.api.core.ObjectStore;

import filenet.vw.api.VWSession;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;

/**
 * Test Warm Up.
 */
public class WarmUpTest extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(WarmUpTest.class.getName());
	
    /**
     * Nome utente.
     */
	private static final String USERNAME = "ANTONIO.GAI";

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}
	
	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test di warmUp.
	 */
	@Test
	public final void warmUp() {
		Connection conAppDS = null;
		Connection conFilenetDS = null;
		
		try {
			conAppDS = createNewDBConnection();
			checkAssertion(conAppDS != null, "La connessione al database applicativo deve essere disponibile.");
			conFilenetDS = createNewDBFileneConnection();
			checkAssertion(conFilenetDS != null, "La connessione al database filenet deve essere disponibile.");
			final IUtenteFacadeSRV usrFacade = ApplicationContextProvider.getApplicationContext()
					.getBean(IUtenteFacadeSRV.class);
			checkAssertion(usrFacade != null, "Lo IOC Container deve essere disponibile.");
			final UtenteDTO utente = usrFacade.getByUsername(USERNAME);
			checkAssertion(utente != null, "Irene pellegrini devere essere presente come utente nella banca dati.");
			final VWSession session = getPESession(utente);
			checkAssertion(session != null, "La connessione al PE deve essere disponibile.");
			final ObjectStore os = getObjectStore(utente);
			checkAssertion(os != null, "La connessione al CE deve essere disponibile.");
		} catch (final Exception e) {
			LOGGER.error("Errore nel warm up.", e);
		} finally {
			closeConnections(conAppDS, conFilenetDS);
		}
	}

	/**
	 * Gestisce la chiusura delle connessioni Oracle e Filenet.
	 * @param conAppDS
	 * @param conFilenetDS
	 */
	private static void closeConnections(final Connection conAppDS, final Connection conFilenetDS) {
		try {
			if (conAppDS != null) {
				conAppDS.close();
			}

			if (conFilenetDS != null) {
				conFilenetDS.close();
			}
		} catch (final SQLException sqle) {
			LOGGER.error("Errore riscontrato nella chisuura delle connessioni.", sqle);
		}
	}


}
