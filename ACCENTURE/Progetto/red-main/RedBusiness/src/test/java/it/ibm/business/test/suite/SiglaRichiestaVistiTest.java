package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.TipoApprovazioneEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Classe di test siglia richiesta visti.
 */
public class SiglaRichiestaVistiTest extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SiglaRichiestaVistiTest.class);
	
	/**
	 * Errore generico di verifica.
	 */
	private static final String ERROR_VERIFICA_MSG = "Errore in fase di verifica.";

	/**
	 * Num doc label.
	 */
	private static final String NUM_DOC = "NUM DOC : ";
	
	/**
	 * Oggetto label.
	 */
	private static final String OGGETTO = "OGGETTO : ";
	
	/**
	 * Document title label.
	 */
	private static final String DOCUMENT_TITLE = "DOCUMENT TITLE : ";

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	private String testNonVerificato(final String oggetto) {
		// RICCARDI CREA UN DOCUMENTO IN USCITA
		UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		String indiceFascicoloProcedimentale = "A";

		Boolean bRiservato = false;

		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(createDestinatarioEsterno(MezzoSpedizioneEnum.ELETTRONICO, ID_CONTATTO_GIANCARLO_ROSSI,
				ModalitaDestinatarioEnum.TO));

		List<AssegnazioneDTO> assegnazioni = null;
		List<AllegatoDTO> allegati = null;
		EsitoSalvaDocumentoDTO esitoUscita = createDocUscita(allegati, false, oggetto, utente,
				ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, assegnazioni, destinatari,
				indiceFascicoloProcedimentale, MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD,
				FIRMA_RAGIONIERE, bRiservato, null);
		checkAssertion(esitoUscita.isEsitoOk(), "Errore in fase di creazione documento in uscita.");

		String dt = esitoUscita.getDocumentTitle();

		EsitoOperazioneDTO esitoSigla = siglaRichiediVisti(USERNAME_MAZZOTTA, dt, ID_IGB); // libro firma
		checkAssertion(esitoSigla.isEsito(), "Errore in fase di sigla.");

		waitSeconds(60);

		EsitoOperazioneDTO esitoRichiediVistoInterno = richiediVistoInterno(USERNAME_MAZZOTTA, dt, ID_IGB_UFFICIO_I); // giro
																														// visti
		checkAssertion(esitoRichiediVistoInterno.isEsito(), "Errore in fase di richiesta visto interno.");

		EsitoOperazioneDTO esitoRichiediVisto = richiediVisto(USERNAME_ALI, dt, USERNAME_ALI);
		checkAssertion(esitoRichiediVisto.isEsito(), "Errore in fase di richiedi visti.");

		EsitoOperazioneDTO esitoVerifica = richiediVerifica(USERNAME_ALI, dt, ID_IGB_UFFICIO_I, ID_FRANCESCO_ALI);
		checkAssertion(esitoVerifica.isEsito(), ERROR_VERIFICA_MSG);

		EsitoOperazioneDTO esitoNonVerificato = nonVerificato(USERNAME_ALI, dt);
		checkAssertion(esitoNonVerificato.isEsito(), "Errore in fase di non verificato.");

		return dt;
	}

	/**
	 * Test Sigla richiesta visti 1.
	 */
	@Test
	public final void test1() {

		String oggetto = createObj();
		String dt = testNonVerificato(oggetto);
		Integer numDoc = getNumDoc(USERNAME_MAZZOTTA, dt);

		EsitoOperazioneDTO esitoVisto = visto(USERNAME_ALI, dt, TipoApprovazioneEnum.VISTO_POSITIVO);
		checkAssertion(esitoVisto.isEsito(), ERROR_VERIFICA_MSG);

		LOGGER.info(DOCUMENT_TITLE + dt);
		LOGGER.info(OGGETTO + oggetto);
		LOGGER.info(NUM_DOC + numDoc);
	}

	/**
	 * Test Sigla richiesta visti 2.
	 */
	@Test
	public final void test2() {

		String oggetto = createObj();
		String dt = testNonVerificato(oggetto);
		Integer numDoc = getNumDoc(USERNAME_MAZZOTTA, dt);

		EsitoOperazioneDTO esitoVisto = visto(USERNAME_ALI, dt, TipoApprovazioneEnum.VISTO_NEGATIVO);
		checkAssertion(esitoVisto.isEsito(), ERROR_VERIFICA_MSG);

		LOGGER.info(DOCUMENT_TITLE + dt);
		LOGGER.info(OGGETTO + oggetto);
		LOGGER.info(NUM_DOC + numDoc);
	}

	/**
	 * Test Sigla richiesta visti 3.
	 */
	@Test
	public final void test3() {

		String oggetto = createObj();
		String dt = testNonVerificato(oggetto);
		Integer numDoc = getNumDoc(USERNAME_MAZZOTTA, dt);

		EsitoOperazioneDTO esitoVisto = visto(USERNAME_ALI, dt, TipoApprovazioneEnum.VISTO_CONDIZIONATO);
		checkAssertion(esitoVisto.isEsito(), ERROR_VERIFICA_MSG);

		LOGGER.info(DOCUMENT_TITLE + dt);
		LOGGER.info(OGGETTO + oggetto);
		LOGGER.info(NUM_DOC + numDoc);
	}
}
