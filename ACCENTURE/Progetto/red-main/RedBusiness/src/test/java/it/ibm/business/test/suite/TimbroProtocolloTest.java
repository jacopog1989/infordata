package it.ibm.business.test.suite;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.adobe.idp.Document;
import com.adobe.idp.dsc.InvocationRequest;
import com.adobe.idp.dsc.InvocationResponse;
import com.adobe.idp.dsc.clientsdk.ServiceClient;
import com.adobe.idp.dsc.clientsdk.ServiceClientFactory;
import com.adobe.idp.dsc.clientsdk.ServiceClientFactoryProperties;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.Adobe;
import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.adobe.AdobeLCHelper;
import it.ibm.red.business.helper.adobe.IAdobeLCHelper;
import it.ibm.red.business.helper.adobe.dto.ConversioneOutputDTO;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.helper.signing.SignHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.FileUtils;

/**
 * Test Timbro Protocollo.
 */
public class TimbroProtocolloTest extends AbstractTest {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TimbroProtocolloTest.class.getName());
	
	/**
	 * Codice fiscale di test.
	 */
	private static final String COD_FIS_TEST = "DNFMSM65R13H501V";

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	private static SignHelper createSignHelper() {
		
		return new SignHelper("https://pkcubecoll.tesoro.it:8443/pkserver/servlet/FirmaRemotaDThandler", FileUtils.getFileFromFS("U:\\Sviluppo\\Files\\MEF\\certificatofirmaremotadthandler.cer"), false);
	}

	/**
	 * Test timbro protocollo ingresso uscita.
	 */
	@Test
	public static final void timbroProtocolloIngressoUscita() {
//		Verifica che se firmi prima in ingresso e poi uscita le firme si vedano e siano valide
		SignHelper sh = createSignHelper();
		byte[] signedContentSecondaUscita = sh.applicaTimbroProtocolloPrincipaleUscita(COD_FIS_TEST, "494990", 
				FileUtils.getFileFromFS("D:\\Sviluppo\\Files\\MEF\\Documenti test\\fabio_numerodoc_top_left.pdf"), "MEF - RGS - Prot. 2377 del 26/06/2019 - U", false);
		FileUtils.saveToFile(signedContentSecondaUscita, "D:\\Sviluppo\\Files\\MEF\\Documenti test\\fabio_numerodoc_top_left_prot.pdf");
	}
	
	/**
	 * Test timbro numero documento.
	 */
	@Test
	public static final void timbroNumeroDocumento() {
		SignHelper sh = createSignHelper();
		byte[] content = FileUtils.getFileFromFS("U:\\Sviluppo\\Files\\MEF\\Documenti test\\test_spedizione.pdf");
		
		byte[] signedContentEntrata = sh.applicaTimbroNumeroDocumento(COD_FIS_TEST, "234978", content, "12345", false, "II Dipartimento Ragioneria Generale dello Stato");
		
		FileUtils.saveToFile(signedContentEntrata, "D:\\Sviluppo\\Files\\MEF\\Documenti test\\fabio_numerodoc_top_left.pdf");
	}
	
	/**
	 * Test timbro postilla.
	 */
	@Test
	public static final void timbroPostilla() {
		SignHelper sh = createSignHelper();
		byte[] content = FileUtils.getFileFromFS("D:\\Sviluppo\\Files\\MEF\\Documenti test\\test_quattro_pagine_numdoc_campo_firma.pdf");
		
		byte[] signedContentEntrata = sh.applicaTimbroPostilla(COD_FIS_TEST, "315313", content, false);
		
		FileUtils.saveToFile(signedContentEntrata, "D:\\Sviluppo\\Files\\MEF\\Documenti test\\test_test_quattro_pagine_numdoc_campo_firma.pdf");
	}
	
	/**
	 * Test timbro approvazione.
	 */
	@Test
	public static final void timbroApprovazione() {
		SignHelper sh = createSignHelper();
		byte[] content = FileUtils.getFileFromFS("D:\\Sviluppo\\Files\\MEF\\Documenti test\\fabio_approvazione.pdf");
		
		byte[] signedContentEntrata = sh.applicaTimbroApprovazione(COD_FIS_TEST, "062682", content, "IGF2 USER1", true, false);
				
		FileUtils.saveToFile(signedContentEntrata, "D:\\Sviluppo\\Files\\MEF\\Documenti test\\fabio_approvazioni.pdf");
	}
	
	/**
	 * Test inserimento campo firma.
	 */
	@Test
	public static final void inserisciCampoFirma() {
		IAdobeLCHelper adobe = new AdobeLCHelper();
		byte[] content = FileUtils.getFileFromFS("D:\\Sviluppo\\Files\\MEF\\Documenti test\\test_crea_raccolta.pdf");
		
		List<Integer> firmatari = Arrays.asList(25, 4, 89);
		
		byte[] signedContentEntrata = adobe.insertCampoFirma(content, "test_crea_raccolta.pdf", ContentType.PDF, firmatari, 59/*altezzaFooter*/, 66/*spaziaturaFirma*/);
		
		FileUtils.saveToFile(signedContentEntrata, "D:\\Sviluppo\\Files\\MEF\\Documenti test\\fabio_campo_firma.pdf");
	}
	
	/**
	 * Test trasformazione PDF.
	 */
	@Test
	public static final void trasformazionePDF() {
		IAdobeLCHelper adobe = new AdobeLCHelper();
		byte[] content = FileUtils.getFileFromFS("C:\\Users\\m.crescentini\\Downloads\\prova.docx");
		
		byte[] signedContentEntrata = adobe.trasformazionePDF("prova.docx", content, false, UUID.randomUUID().toString());
		
		FileUtils.saveToFile(signedContentEntrata, "C:\\Users\\m.crescentini\\Downloads\\prova.pdf");
	}
	
	/**
	 * Test apposizione campo firma.
	 */
	@Test
	public static final void apponiCampoFirma() {
		IAdobeLCHelper adobe = new AdobeLCHelper();
		byte[] content = FileUtils.getFileFromFS("D:\\Sviluppo\\Files\\MEF\\Documenti test\\ragioniere.docx");
		
		byte[] signedContent = adobe.trasformazionePDF("prova.docx", content, false, UUID.randomUUID().toString());
		
		signedContent = PdfHelper.inserisciCampiFirma(signedContent, Arrays.asList(64), 59, 66, false, true);
		
		FileUtils.saveToFile(signedContent, "C:\\Users\\m.crescentini\\Downloads\\ragioniere.pdf");
	}
	
	/**
	 * Test generico timbro protocollo.
	 */
	@Test
	public final void tessssst() {
		byte[] in = FileUtils.getFileFromFS("D:\\Sviluppo\\Files\\MEF\\Documenti test\\test_crea_raccolta.pdf");
		Integer altezzaFooter = 59;
		Integer spaziaturaFirma = 66;
		String commaSeparatedIdFirmatari = "474";
		Integer numeroDocumento = 9999;
		String protocollo = null;
		
		PropertiesProvider pp = PropertiesProvider.getIstance();
		String soapEndpoint = pp.getParameterByKey(PropertiesNameEnum.DSC_DEFAULT_SOAP_ENDPOINT_ADOBEKEY);
		String serverType = pp.getParameterByKey(PropertiesNameEnum.DSC_SERVER_TYPE_ADOBEKEY);
		String username = pp.getParameterByKey(PropertiesNameEnum.DSC_CREDENTIAL_USERNAME_ADOBEKEY);
		String password = pp.getParameterByKey(PropertiesNameEnum.DSC_CREDENTIAL_PASSWORD_ADOBEKEY);
		if (soapEndpoint == null || serverType == null || username == null || password == null) {
			throw new RedException("Non sono presenti sul DB le credenziali di accesso per i servizi AdobeLC");
		}
        final Properties connectionProps = new Properties();
		connectionProps.setProperty(ServiceClientFactoryProperties.DSC_TRANSPORT_PROTOCOL, ServiceClientFactoryProperties.DSC_SOAP_PROTOCOL);
        connectionProps.setProperty(ServiceClientFactoryProperties.DSC_DEFAULT_SOAP_ENDPOINT, soapEndpoint);
        connectionProps.setProperty("DSC_SERVER_TYPE", serverType);
        connectionProps.setProperty("DSC_CREDENTIAL_USERNAME", username);
        connectionProps.setProperty("DSC_CREDENTIAL_PASSWORD", password);
        ServiceClientFactory serviceClientFactory = ServiceClientFactory.createInstance(connectionProps);
        
		String processName = Adobe.PROCESS_NAME_PREDISPOSIZIONE_DOCUMENTONSD;

		ServiceClient serviceClient = serviceClientFactory.getServiceClient();
		Map<String, Object> params = new HashMap<>(10);
		ConversioneOutputDTO result = new ConversioneOutputDTO();
		
		Document inDoc = new Document(new ByteArrayInputStream(in));
		inDoc.setAttribute("file", "test_crea_raccolta.pdf");
		inDoc.setContentTypeFromBasename();
		inDoc.passivate();
		
		params.put("inDoc", inDoc);
		params.put("preview", (BooleanFlagEnum.NO.getEnglishValue()));
		
		params.put("protocollo", protocollo);
		params.put("altezzaFooter", altezzaFooter);
		params.put("spaziaturaFirma", spaziaturaFirma);
		params.put("firmatariStringList", commaSeparatedIdFirmatari);
		params.put("idDocumento", numeroDocumento);
		
		try {
			InvocationRequest esRequest = serviceClientFactory.createInvocationRequest(processName, "invoke", params, true);
			InvocationResponse inResp = serviceClient.invoke(esRequest);
			
			Document outDoc = (Document) inResp.getOutputParameter(Constants.Adobe.CONVERSIONE_OUTDOC);
			FileItem fi = (new DiskFileItemFactory()).createItem(outDoc.getAttribute("file").toString(), outDoc.getContentType(), false, outDoc.getAttribute("file").toString());
			IOUtils.copy(new ByteArrayInputStream(AdobeLCHelper.readDocument(outDoc)), fi.getOutputStream());
			result.setDocumento(fi);
			outDoc.dispose();
			if (inResp.getOutputParameters().get(Constants.Adobe.CONVERSIONE_PREVIEWDOC) != null) {
				Document previewDoc = (Document) inResp.getOutputParameter(Constants.Adobe.CONVERSIONE_PREVIEWDOC);
				FileItem fi2 = (new DiskFileItemFactory()).createItem(previewDoc.getAttribute("file").toString(), previewDoc.getContentType(), false, previewDoc.getAttribute("file").toString());
				IOUtils.copy(new ByteArrayInputStream(AdobeLCHelper.readDocument(previewDoc)), fi2.getOutputStream());
				result.setPreview(fi2);
				previewDoc.dispose();
			}
			
			inDoc.dispose();
			
			FileUtils.saveToFile(result.getDocumento().get(), "D:\\Sviluppo\\Files\\MEF\\Documenti test\\test_prova_adobe_.pdf");
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}
	
	/**
	 * Test apposizione campo firma.
	 */
	@Test
	public static final void addFooterToPdf() {
		
		try {
			
			byte[] content = FileUtils.getFileFromFS("C:\\Users\\a.dilegge\\Documents\\Doc x Test\\Principali\\PROVAFirmatario1.pdf");
			content = PdfHelper.addFooterToPDF(content, "Viale Pier Luigi Nervi, 270 – 04100 Latina; tel.: 0773682000 – fax: 0773682905; e-mail: uff7dcsii.dag@tesoro.it; PEC: dcsii.dag@pec.mef.gov.it");
			FileUtils.saveToFile(content, "C:\\Users\\a.dilegge\\Documents\\Doc x Test\\Principali\\footer.pdf");
			content = FileUtils.getFileFromFS("C:\\Users\\a.dilegge\\Documents\\Doc x Test\\Principali\\footer.pdf");
			content = PdfHelper.inserisciCampiFirma(content, Arrays.asList(64), 59, 66, false, true);
			FileUtils.saveToFile(content, "C:\\Users\\a.dilegge\\Documents\\Doc x Test\\Principali\\footerCampoFirma.pdf");
		
		} catch (Exception e) {
			LOGGER.error(e);
		}
		
	}
	
}
