package it.ibm.business.test.asign;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.filenet.api.core.Document;

import it.ibm.business.test.suite.AbstractTest;
import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignStepResultDTO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentoDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.RecuperoCodeDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.SignStrategyEnum;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IASignOrchestrator;
import it.ibm.red.business.service.IASignSRV;
import it.ibm.red.business.service.IRegistrazioniAusiliarieSRV;
import it.ibm.red.business.service.asign.step.ASignStepAbstract;
import it.ibm.red.business.service.asign.step.IASignStep;
import it.ibm.red.business.service.asign.step.StatusEnum;
import it.ibm.red.business.service.asign.step.StepEnum;
import it.ibm.red.business.service.facade.IAooFacadeSRV;

/**
 * Classe astratta utilizzata per contenere le caratteristiche comuni dei test.
 * 
 * @author CPIERASC
 */
public abstract class AbstractAsignTest extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AbstractAsignTest.class.getName());
	
	/**
	 * Utente Luigi Minnielli - username.
	 */
	protected static final String USERNAME_RGS = "LUIGI.MINNIELLI";
	
	/**
	 * Utente Biagio Mazzotta - username.
	 */
	protected static final String USERNAME_RGS2 = "BIAGIO.MAZZOTTA";
	
	/**
	 * ID tipo procedimento generico RGS.
	 */
	protected static final Integer ID_TIPO_PROC_GENERICO_RGS_USCITA = 233;
	
	/**
	 * ID tipo documento generico RGS.
	 */
	protected static final Integer ID_TIPO_DOC_GENERICO_RGS_USCITA = 60;

	/**
	 * ID tipo procedimento bilancio enti RGS.
	 */
	protected static final Integer ID_TIPO_PROC_BILANCIO_ENTI_RGS_USCITA = 343;
	
	/**
	 * ID tipo documento bilancio enti RGS.
	 */
	protected static final Integer ID_TIPO_DOC_BILANCIO_ENTI_RGS_USCITA = 490;
	
	/**
	 * Utente Alessandra Courrier - username.
	 */
	protected static final String USERNAME_DF = "ALESSANDRA.COURRIER";
	
	/**
	 * ID tipo procedimento generico DF.
	 */
	protected static final Integer ID_TIPO_PROC_GENERICO_DF_USCITA = 1001;
	
	/**
	 * ID tipo documento generico DF.
	 */
	protected static final Integer ID_TIPO_DOC_GENERICO_DF_USCITA = 2301;

	/**
	 * Utente Lorenzo Quinzi - username.
	 */
	protected static final String USERNAME_LAVORO = "LORENZO.QUINZI";
	
	/**
	 * ID tipo procedimento generico UCB_LAV.
	 */
	protected static final Integer ID_TIPO_PROC_GENERICO_LAVORO_USCITA = 1301;
	
	/**
	 * ID tipo documento generico UCB_LAV.
	 */
	protected static final Integer ID_TIPO_DOC_GENERICO_LAVORO_USCITA = 4527;
	
	/**
	 * Utente Giulia Balocchi - username utente GA RGS.
	 */
	protected static final String USERNAME_GA_RGS = "GIULIA.BALOCCHI";

	/**
	 * Service firma asincrona.
	 */
	private IASignSRV aSignSRV;
	
	/**
	 * Orchestrator di firma asincrona.
	 */
	private IASignOrchestrator orchestrator;
	
	/**
	 * Service AOO.
	 */
	private IAooFacadeSRV aooSrv;
	
	/**
	 * Service registrazioni ausiliaire.
	 */
	private IRegistrazioniAusiliarieSRV regAusSRV;
	
	/**
	 * Restituisce il service di firma asincrona.
	 * @return
	 */
	protected IASignSRV getaSignSRV() {
		return aSignSRV;
	}

	/**
	 * Esegue la logica prima del test.
	 */
	@Override
	protected final void before() {
		super.before();
		orchestrator = ApplicationContextProvider.getApplicationContext().getBean(IASignOrchestrator.class);
		aSignSRV = ApplicationContextProvider.getApplicationContext().getBean(IASignSRV.class);
		regAusSRV = ApplicationContextProvider.getApplicationContext().getBean(IRegistrazioniAusiliarieSRV.class);
		aooSrv = ApplicationContextProvider.getApplicationContext().getBean(IAooFacadeSRV.class);
	}

	/**
	 * Restituisce true se il documento identificato dal documentTitle ha una
	 * registrazione ausiliaria associata, false altrimenti.
	 * 
	 * @param documentTitle
	 *            identificativo del documento
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @return true se il documento ha una registrazione ausiliaria
	 */
	protected Boolean hasRegAus(String documentTitle, Integer idAoo) {
		Connection con = createNewDBConnection();
		RegistrazioneAusiliariaDTO regAus = regAusSRV.getRegistrazioneAusiliaria(documentTitle, idAoo, con);
		return regAus != null && regAus.getNumeroRegistrazione() != null;
	}

	/**
	 * Restituisce la registrazione ausiliaria associata al documento se esistente,
	 * restituisce {@code null} altrimenti.
	 * 
	 * @param documentTitle
	 *            identificativo documento dal quale recuperare la registrazione
	 *            ausiliaria
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @return registrazione ausiliaria del documento identificato dal documentTitle
	 *         se esiste, null altrimenti.
	 */
	protected RegistrazioneAusiliariaDTO getRegAus(String documentTitle, Integer idAoo) {
		Connection con = createNewDBConnection();
		return regAusSRV.getRegistrazioneAusiliaria(documentTitle, idAoo, con);
	}

	/**
	 * Restituisce true se l'AOO è uno degli uffici centrale di bilancio (UCB).
	 * 
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @return true se l'AOO è UCB, false altrimenti
	 */
	protected Boolean isUCB(Integer idAoo) {
		Aoo aoo = aooSrv.recuperaAoo(idAoo);
		return aoo.getFlagUCB() != 0;
	}

	/**
	 * Restituisce l'AOO associato all'id.
	 * @param idAoo
	 * @return
	 */
	protected Aoo recuperaAoo(Long idAoo) {
		return aooSrv.recuperaAoo(idAoo.intValue());
	}

	/**
	 * Crea un documento utilizzando l' {@code utente} e lo assegna a se stesso in
	 * firma.
	 * 
	 * @param allegati
	 *            allegati del documento
	 * @param utente
	 *            utente creatore e assegnatario del documento creato
	 * @param idTipologiaDocumento
	 *            identificativo della tipologia documento
	 * @param idTipologiaProcedimento
	 *            identificativo della tipologia procedimento
	 * @param idFascicoloFepa
	 *            identificativo fascicolo FEPA
	 * @return esito creazione
	 */
	protected EsitoSalvaDocumentoDTO createDocAutoassegnatoFirma(List<AllegatoDTO> allegati, UtenteDTO utente, final Integer idTipologiaDocumento, final Integer idTipologiaProcedimento, final String idFascicoloFepa) {
		return createDocAutoassegnatoFirma(allegati, utente, idTipologiaDocumento, idTipologiaProcedimento, null, null, null, idFascicoloFepa);
	}

	/**
	 * Crea un documento utilizzando l' {@code utente} e lo assegna a se stesso in
	 * firma.
	 * 
	 * @param allegati
	 *            allegati del documento creato
	 * @param utente
	 *            utente creatore e assegnatario del documento creato
	 * @param idTipologiaDocumento
	 *            identificativo tipologia documento
	 * @param idTipologiaProcedimento
	 *            identificativo tipologia procedimento
	 * @param idNodoDestInt
	 *            identificativo nodo destinatario interno
	 * @param idUtenteDestInt
	 *            identificativo utente destinatario interno
	 * @param idContattoDestInt
	 *            identificativo contatto destinatario interno
	 * @param idFascicoloFepa
	 *            identificativo fascicolo FEPA
	 * @return esito creazione
	 */
	protected EsitoSalvaDocumentoDTO createDocAutoassegnatoFirma(List<AllegatoDTO> allegati, UtenteDTO utente, final Integer idTipologiaDocumento, final Integer idTipologiaProcedimento, final Integer idNodoDestInt, final Integer idUtenteDestInt,
			final Integer idContattoDestInt, final String idFascicoloFepa) {
		List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneFirma(utente.getIdUfficio().intValue(), utente.getId().intValue(), utente.getNome(), utente.getCognome()));

		String oggetto = createObj();

		Boolean bRiservato = false;

		String indiceFascicoloProcedimentale = null;

		if (utente.getIdAoo() == 25L) {
			indiceFascicoloProcedimentale = "A";
		} else if (utente.getIdAoo() == 33L) {
			indiceFascicoloProcedimentale = "daef.01";
		} else if (utente.getIdAoo() == 34L) {			
			indiceFascicoloProcedimentale = "01";
		}		
		
		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(createDestinatarioEsterno(MezzoSpedizioneEnum.ELETTRONICO, ID_CONTATTO_UNO, ModalitaDestinatarioEnum.TO));
		if (idNodoDestInt!=null) {
			destinatari.add(createDestinatarioInterno(idNodoDestInt, idUtenteDestInt, idContattoDestInt, ModalitaDestinatarioEnum.CC));
		}
		
		return createDocUscita(allegati, false, oggetto, utente, idTipologiaDocumento, idTipologiaProcedimento, assegnazioni, 
				destinatari, indiceFascicoloProcedimentale, MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD, IterApprovativoDTO.ITER_FIRMA_MANUALE, bRiservato, idFascicoloFepa);
	}

	/**
	 * Esegue la firma del documento identificato dal {@code wobn}.
	 * 
	 * @param utente
	 *            utente firmatario
	 * @param wobn
	 *            identificativo del documento da firmare
	 * @return esito della firma
	 */
	protected EsitoOperazioneDTO sign(UtenteDTO utente, String wobn) {
		String pin = "1234";
		String otp = "5678";
		Collection<String> wobNumbers = new ArrayList<>();
		wobNumbers.add(wobn);
		return aSignSRV.firmaRemota(wobNumbers, pin, otp, utente, SignTypeEnum.PADES_VISIBLE, false).iterator().next();
	}

	/**
	 * Restituisce l'orchestrator per la firma asincrona.
	 * 
	 * @return orchestrator orchestrator della firma asincrona
	 */
	protected IASignOrchestrator getOrchestrator() {
		return orchestrator;
	}

	/**
	 * Verifica che l'item a cui si riferisce l'esito: EsitoSalvaDocumentoDTO si
	 * trova nella coda: {@link DocumentQueueEnum#PREPARAZIONE_ALLA_SPEDIZIONE} o
	 * {@link DocumentQueueEnum#PREPARAZIONE_ALLA_SPEDIZIONE_FEPA}.
	 * 
	 * @param utente
	 *            utente di gestione applicativa che ha accesso alla coda
	 *            {@link DocumentQueueEnum#PREPARAZIONE_ALLA_SPEDIZIONE} o
	 *            {@link DocumentQueueEnum#PREPARAZIONE_ALLA_SPEDIZIONE_FEPA}
	 * @param esitoCreazione
	 *            esito creazione documento
	 * @return item se presente in coda, lancia eccezione se non è presente nella
	 *         coda
	 */
	protected ASignItemDTO checkIsIncoda(UtenteDTO utente, EsitoSalvaDocumentoDTO esitoCreazione) {
		ASignItemDTO out = null;
		List<ASignItemDTO> items = getaSignSRV().getItems(utente, DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE);
		Boolean bFound = false;
		for (ASignItemDTO item:items) {
			if (item.getDocumentTitle().equals(esitoCreazione.getDocumentTitle())) {
				bFound = true;
				out= item;
				break;
			}
		}
		checkAssertion(bFound, "Il documento deve essere nella coda.");
		return out;
	}

	/**
	 * Esegue la verifica dell'esito di firma facendo asserzioni sullo stato dei
	 * valori contenuti nel risultato dell'item.
	 * 
	 * @param utente
	 *            utente firmatario
	 * @param esitoCreazione
	 *            esito creazione documento
	 * @param classeDoc
	 *            classe documentale documento
	 * @param idItem
	 *            identificativo item per il quale verificarne lo stato
	 * @param playedIdItem
	 *            item per cui è stato eseguito un <em> play </em>
	 */
	protected void checkEndASign(UtenteDTO utente, EsitoSalvaDocumentoDTO esitoCreazione, String classeDoc, Long idItem,
			Long playedIdItem) {
		RecuperoCodeDTO codeUscita;
		// Verifichiamo esito della firma
		checkAssertion(playedIdItem.equals(idItem), "Impostare la proprietà al massimo non è bastato per lavorare il nostro elemento");
		ASignItemDTO playedItem = getaSignSRV().getItem(playedIdItem);
		checkAssertion(playedItem != null, "Non è stato recuperato alcun item");
		checkAssertion(playedItem.getId() != null, "L'identificativo non può essere vuoto");
		checkAssertion(playedItem.getnRetry() == 0, "Il numero di retry deve essere 0");
		checkAssertion(StatusEnum.ELABORATO.equals(playedItem.getStatus()), "Stato iniziale errato");
		checkAssertion(StepEnum.STOP.equals(playedItem.getStep()), "Step iniziale errato");
		for (StepEnum s:StepEnum.values()) {
			ASignStepResultDTO info = getaSignSRV().getInfo(playedIdItem, s);
			checkAssertion(info != null, "Le informazioni dello step " + s + " sono vuote");
		}
		List<ASignStepResultDTO> infos = getaSignSRV().getInfos(playedIdItem);
		checkAssertion(infos != null && !infos.isEmpty(), "Le informazioni complessive sulla firma sono vuote");

		codeUscita = getQueues(utente.getUsername(), esitoCreazione.getDocumentTitle(), classeDoc, CategoriaDocumentoEnum.USCITA.getIds()[0]);
		checkAssertionCode(codeUscita, DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE, DocumentQueueEnum.SPEDIZIONE, DocumentQueueEnum.PROCEDIMENTI);
	}

	/**
	 * Restituisce {@code true} se esiste un item associato al documento
	 * identificato dal {@code dt} nella lista in input: {@code items}.
	 * 
	 * @param dt
	 *            identificativo documento associato all'item da ricercare
	 * @param items
	 *            items nella quale ricercare l'item associato al documento
	 * @return {@code true} se l'item esiste, {@code false} altrimenti
	 */
	protected boolean isIn(String dt, List<ASignItemDTO> items) {
		boolean out = false;
		for (ASignItemDTO item: items) {
			if (dt.equals(item.getDocumentTitle())) {
				out = true;
				break;
			}
		}
		return out;
	}

	/**
	 * Crea un documento della tipologia specificata dall'
	 * {@code idTipologiaDocumento} mandandolo in firma.
	 * 
	 * @param allegati
	 *            allegati del documento
	 * @param utente
	 *            utente firmatario
	 * @param idTipologiaDocumento
	 *            identificativo tipologia documento
	 * @param idTipologiaProcedimento
	 *            identificativo tipologia procedimento
	 * @param idFascicoloFepa
	 *            identificativo fascicolo FEPA
	 * @return document title del documento creato e firmato
	 */
	protected String createASign(List<AllegatoDTO> allegati, UtenteDTO utente, final Integer idTipologiaDocumento, final Integer idTipologiaProcedimento, final String idFascicoloFepa) {
		EsitoSalvaDocumentoDTO esitoCreazione = createDocAutoassegnatoFirma(allegati, utente, idTipologiaDocumento, idTipologiaProcedimento, idFascicoloFepa);
		String classeDoc = null;
		RecuperoCodeDTO codeUscita = getQueues(utente.getUsername(), esitoCreazione.getDocumentTitle(), classeDoc, CategoriaDocumentoEnum.USCITA.getIds()[0]);
		checkAssertionCode(codeUscita, DocumentQueueEnum.NSD, DocumentQueueEnum.PROCEDIMENTI, DocumentQueueEnum.RICHIAMA_DOCUMENTI);
		sign(utente, esitoCreazione.getWobNumber());
		return esitoCreazione.getDocumentTitle();
	}

	/**
	 * Esegue il metodo di esecuzione associato allo {@code step} processando l'item
	 * identificato dall' {@code idItem}.
	 * 
	 * @param step
	 *            step da cui recuperare la classe executor
	 * @param idItem
	 *            identificativo item da eseguire
	 * @return risultato del processamento dello step
	 */
	protected ASignStepResultDTO run(StepEnum step, Long idItem) {
		ASignItemDTO item = aSignSRV.getItem(idItem);
		Class<? extends IASignStep> clazz = step.getExecutor();
		return ((ASignStepAbstract) ApplicationContextProvider.getApplicationContext().getBean(clazz)).execute(item);
	}

	/**
	 * Simula un crash sullo {@code step} processando l'item identificato dall'
	 * {@code idItem}.
	 * 
	 * @param step
	 *            step in cui simulare il crash sull'item
	 * @param idItem
	 *            identificativo item da mandare in crash
	 */
	protected void crash(StepEnum step, Long idItem) {
		ASignItemDTO item = aSignSRV.getItem(idItem);
		Class<? extends IASignStep> clazz = step.getExecutor();
		((ASignStepAbstract) ApplicationContextProvider.getApplicationContext().getBean(clazz)).simulateCrash(item);
	}

	/**
	 * Recupera l'aoo dell'utente indicando la strategia adotta dall'AOO in console.
	 * 
	 * @param utente
	 *            utente in sessione che fa riferimento all'AOO ottenuta
	 * @return informazioni sull'area organizzativa
	 */
	protected Aoo dumpTestInfo(UtenteDTO utente) {
		LOGGER.info("UTENTE > " + utente.getDescrizione());
		Aoo aoo = recuperaAoo(utente.getIdAoo());
		LOGGER.info("AOO > " + aoo.getDescrizione());
		LOGGER.info("UCB > " + (aoo.getFlagUCB()==1));
		SignStrategyEnum out = SignStrategyEnum.get(aoo.getFlagStrategiaFirma());
		LOGGER.info("FLAG STRATEGIA FIRMA > " + out);
		return aoo;
	}

	/**
	 * Accoda l'item di firma asincrona.
	 * 
	 * @param username
	 *            utente firmatario
	 * @param idTipoDoc
	 *            identificativo tipologia documento
	 * @param idTipoProc
	 *            identificativo tipologia procedimento
	 * @param strategy
	 *            strategy adottata dall'area organizzativa
	 * @param idFascicoloFepa
	 *            identificativo fascicolo FEPA
	 * @return item accodato
	 */
	protected ASignItemDTO accoda(String username, Integer idTipoDoc, Integer idTipoProc, SignStrategyEnum strategy, final String idFascicoloFepa) {
		return accoda(username, idTipoDoc, idTipoProc, strategy, null, null, null, idFascicoloFepa);
	}

	/**
	 * Accoda l'item di firma asincrona.
	 * 
	 * @param username
	 *            utente firmatario
	 * @param idTipoDoc
	 *            identificativo tipologia documento
	 * @param idTipoProc
	 *            identificativo tipologia procedimento
	 * @param strategy
	 *            strategy adottata dall'area organizzativa
	 * @param idNodoDestInt
	 *            identificativo del nodo destinatario interno
	 * @param idUtenteDestInt
	 *            identificativo dell'utente destinatario interno
	 * @param idContattoDestInt
	 *            identificativo contatto destinatario interno
	 * @param idFascicoloFepa
	 *            identificativo fascicolo FEPA
	 * @return item accodato
	 */
	protected ASignItemDTO accoda(String username, Integer idTipoDoc, Integer idTipoProc, SignStrategyEnum strategy, final Integer idNodoDestInt, final Integer idUtenteDestInt,
			final Integer idContattoDestInt, final String idFascicoloFepa) {
		UtenteDTO utente = getUtente(username);
		SignStrategyEnum flagStrategia = SignStrategyEnum.get(dumpTestInfo(utente).getFlagStrategiaFirma());
		checkAssertion(strategy.equals(flagStrategia), "La data preparation è errata, strategia non conforme.");
		EsitoSalvaDocumentoDTO esitoCreazione = createDocAutoassegnatoFirma(null, utente, idTipoDoc, idTipoProc, idNodoDestInt, idUtenteDestInt, idContattoDestInt, idFascicoloFepa);
		sign(utente, esitoCreazione.getWobNumber());
		return checkIsIncoda(utente, esitoCreazione);
	}

	/**
	 * Restituisce i dettagli del documento associato all'item di firma asincrona
	 * identificato dall' {@code itemPre}.
	 * 
	 * @param username
	 *            username dell'utente in sessione
	 * @param itemPre
	 *            item associato al documento per il quale occorrono i dettagli
	 * @return dettagli del documento associato all'item
	 */
	protected DetailDocumentoDTO getDocDetail(String username, ASignItemDTO itemPre) {
		UtenteDTO utente = getUtente(username);
		IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
		Document documentForDetail = fceh.getDocumentRedForDetail(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), itemPre.getDocumentTitle());
		return TrasformCE.transform(documentForDetail, TrasformerCEEnum.FROM_DOCUMENTO_TO_DETAIL);
	}
	
}