package it.ibm.business.test.asign;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.enums.SignStrategyEnum;
import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * Classe di test step di upload verso NPS - firma asincrona.
 */
public class Test09StepUploadNPS extends AbstractAsignTest {

	/**
	 * Esegue le operazioni di data preparation prima dell'esecuzione del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue le azioni a valle dell'esecuzione del test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Esegue lo step di upload verso NPS.
	 */
	@Test
	public final void run() {
		ASignItemDTO itemPre = accoda(USERNAME_RGS, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, SignStrategyEnum.ASYNC_STRATEGY_B, null);
		run(StepEnum.PROTOCOLLAZIONE, itemPre.getId());
		run(StepEnum.REG_AUX_PROTOCOLLAZIONE, itemPre.getId());
		run(StepEnum.STAMPIGLIATURE, itemPre.getId());
		run(StepEnum.AGGIORNAMENTO_METADATI, itemPre.getId());
		run(StepEnum.GESTIONE_ALLACCI, itemPre.getId());
		run(StepEnum.AVANZAMENTO_PROCESSI, itemPre.getId());
		run(StepEnum.INVIO_MAIL, itemPre.getId());
		run(StepEnum.ALLINEAMENTO_NPS, itemPre.getId());
	}

}
