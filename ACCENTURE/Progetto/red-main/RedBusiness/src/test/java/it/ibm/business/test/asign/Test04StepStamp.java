package it.ibm.business.test.asign;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.SignStrategyEnum;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.asign.step.StepEnum;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;

/**
 * Classe di test per stemp stampigliature. @see StepEnum.STAMPIGLIATURE.
 */
public class Test04StepStamp extends AbstractAsignTest {

	/**
	 * Numero timbri registrazioni.
	 */
	static final Integer NUMERO_TIMBRI_REGISTRAZIONI = 1;
	
	/**
	 * Numero timbri stampigliature.
	 */
	static final Integer NUMERO_TIMBRI_STAMPIGLIATURE = 1;

	/**
	 * Servizio di gestione documenti.
	 */
	private IDocumentoFacadeSRV documentoSRV;

	/**
	 * Esegue le operazioni di data preparation prima dell'esecuzione del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);
	}

	/**
	 * Esegue le azioni a valle dell'esecuzione del test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Esegue lo step di stampigliatura.
	 */
	@Test
	public final void run() {
		
		ASignItemDTO itemPre = accoda(USERNAME_RGS, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, SignStrategyEnum.ASYNC_STRATEGY_B, null);
		run(StepEnum.PROTOCOLLAZIONE, itemPre.getId());
		run(StepEnum.REG_AUX_PROTOCOLLAZIONE, itemPre.getId());

		Integer countFirmePre = countTimbri(USERNAME_RGS, itemPre.getDocumentTitle());
		checkAssertion(countFirmePre.equals(NUMERO_TIMBRI_REGISTRAZIONI), "Il numero di stampigliature in fase di registrazione è errato.");

		run(StepEnum.STAMPIGLIATURE, itemPre.getId());
		
		Integer countFirmePost = countTimbri(USERNAME_RGS, itemPre.getDocumentTitle());
		checkAssertion((countFirmePost-countFirmePre)==NUMERO_TIMBRI_STAMPIGLIATURE, "Il numero di stampigliature in fase di stampigliatura è errato");

	}

	/**
	 * Simula un crash durante l'esecuzione dello step di stampigliatura.
	 */
	@Test
	public final void crash() {
		ASignItemDTO itemPre = accoda(USERNAME_RGS, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, SignStrategyEnum.ASYNC_STRATEGY_B, null);
		run(StepEnum.PROTOCOLLAZIONE, itemPre.getId());
		run(StepEnum.REG_AUX_PROTOCOLLAZIONE, itemPre.getId());

		Integer countFirmePre = countTimbri(USERNAME_RGS, itemPre.getDocumentTitle());
		checkAssertion(countFirmePre.equals(NUMERO_TIMBRI_REGISTRAZIONI), "Il numero di stampigliature in fase di registrazione è errato.");

		crash(StepEnum.STAMPIGLIATURE, itemPre.getId());

		countFirmePre = countTimbri(USERNAME_RGS, itemPre.getDocumentTitle());
		checkAssertion(countFirmePre.equals(NUMERO_TIMBRI_REGISTRAZIONI), "Il numero di stampigliature eseguendo ed annullando lo step di registraione è errato.");

		run(StepEnum.STAMPIGLIATURE, itemPre.getId());
		Integer countFirmePost = countTimbri(USERNAME_RGS, itemPre.getDocumentTitle());
		checkAssertion((countFirmePost-countFirmePre)==NUMERO_TIMBRI_STAMPIGLIATURE, "Il numero di stampigliature in fase di stampigliatura è errato");
	}

	private Integer countTimbri(String username, String documentTitle) {
		UtenteDTO utente = getUtente(username);
		FileDTO file = documentoSRV.getDocumentoContentPreview(utente.getFcDTO(), documentTitle, false, utente.getIdAoo());
		return PdfHelper.getSignatureNumber(file.getContent());
	}
	
}
