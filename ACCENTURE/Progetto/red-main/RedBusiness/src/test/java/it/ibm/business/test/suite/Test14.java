package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.net.MediaType;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Test 14.
 */
public class Test14 extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test14.class);
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue le azioni prima del test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test 14.
	 */
	@Test
	public final void test() {
		// 1) Creazione USCITA

		// - Tab allegati: allegato (firmato) da firmare

		UtenteDTO utente = getUtente(USERNAME_TANZI);
		String oggetto = createObj();
		String indiceFascicoloProcedimentale = "A";
		Boolean bRiservato = false;

		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(createDestinatarioEsterno(MezzoSpedizioneEnum.CARTACEO, ID_CONTATTO_GIANCARLO_ROSSI,
				ModalitaDestinatarioEnum.TO));

		List<AssegnazioneDTO> assegnazioni = null;
		List<AllegatoDTO> allegati = new ArrayList<>();
		AllegatoDTO allegato = new AllegatoDTO();
		allegato.setDaFirmareBoolean(true);
		allegato.setIdTipologiaDocumento(ID_TIPO_DOC_GENERICO_RGS_USCITA);
		allegato.setFormatoSelected(FormatoAllegatoEnum.ELETTRONICO);
		allegato.setNomeFile("fileTest.pdf");
		allegato.setContent(FileUtils.getFileFromInternalResources("fileTest.pdf"));
		allegato.setMimeType(MediaType.PDF.toString());

		allegati.add(allegato);

		EsitoSalvaDocumentoDTO esitoUscita = createDocUscita(allegati, false, oggetto, utente,
				ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, assegnazioni, destinatari,
				indiceFascicoloProcedimentale, MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD,
				FIRMA_ISPETTORE, bRiservato, null);
		checkAssertion(esitoUscita.isEsitoOk(), "Errore in fase di creazione documento in uscita.");

		String dt = esitoUscita.getDocumentTitle();

		// 2) CODA: libro firma RESPONSE: "Sigla e invia"

		EsitoOperazioneDTO esitoSiglaInvia = siglaInvia(USERNAME_TANZI, dt);
		checkAssertion(esitoSiglaInvia.isEsito(), "Errore in fase di sigla e invia.");

		waitSeconds(30);

		// 3) CODA: corriere RESPONSE "Invia all'ispettore"
		EsitoOperazioneDTO esitoInviaIspettore = inviaIspettore(USERNAME_TANZI, dt);
		checkAssertion(esitoInviaIspettore.isEsito(), "Errore in fase di invia ispettore.");

		waitSeconds(30);

		// 4) Firma remota

		String otp = "090513";
		String pin = "87654321";
		Collection<EsitoOperazioneDTO> esiti = firmaRemota(USERNAME_TANZI, SignTypeEnum.PADES_VISIBLE, dt, otp, pin);
		EsitoOperazioneDTO esitoFirma = esiti.iterator().next();
		LOGGER.info(esitoFirma.getNote());
		checkAssertion(esitoFirma.isEsito(), "Errore in fase di firma remota.");

		VWWorkObject wo = fromDocumentTitleToWF(getPESession(getUtente(USERNAME_TANZI)), DocumentQueueEnum.SPEDIZIONE,
				dt, ID_IGF, ID_GIANFANCO_TANZI);
		checkAssertion(!StringUtils.isNullOrEmpty(wo.getWorkObjectNumber()),
				"Il workflow deve essere presente nella coda SPEDIZIONE per TANZI.");

		// 5) Download allegato firmato e verifica che sia firmato.
	}

}
