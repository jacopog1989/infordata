package it.ibm.business.test;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.UtenteDTO;

/**
 * Test Home Generic.
 */
public class HomeGenericNoWindowTest extends AbstractTest {

	/**
	 * Metodo preliminare al test.
	 */
	@Before
	public static final void beforeTest() {
		// Metodo intenzionalmente vuoto.
	}

	/**
	 * Esegue il test.
	 */
	@Test
	public final void test() {

		Collection<QueueInfo> r1 = executeQueueQueryAllQueue(new UtenteDTO(), null, "ulteriori");
		
		//DOCUMENTOUTENTESTATO 
		dumpWFInfo(r1);

//		37745	30	0	64	03-SEP-18
//		37745	30	1964	64	03-SEP-18 Irene

	}

}
