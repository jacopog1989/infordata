package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.collection.PageIterator;
import com.filenet.api.core.Document;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.query.SearchSQL;
import com.filenet.api.query.SearchScope;
import com.google.common.net.MediaType;

import filenet.vw.api.VWSession;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.MapRedKey;
import it.ibm.red.business.constants.Constants.Modalita;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IProtocollaDsrSRV;
import it.ibm.red.business.service.facade.IChiudiDichiarazioneFacadeSRV;
import it.ibm.red.business.service.facade.IFepaFacadeSRV;
import it.ibm.red.business.service.facade.IREDServiceFacadeSRV;
import it.ibm.red.business.service.facade.ISalvaDocumentoFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.webservice.model.redservice.header.v1.CredenzialiUtenteRed;
import it.ibm.red.webservice.model.redservice.header.v1.ObjectFactory;
import it.ibm.red.webservice.model.redservice.types.v1.ClasseDocumentaleRed;
import it.ibm.red.webservice.model.redservice.types.v1.DocumentoRed;
import it.ibm.red.webservice.model.redservice.types.v1.IdentificativoUnitaDocumentaleFEPATypeRed;
import it.ibm.red.webservice.model.redservice.types.v1.IdentificativoUnitaDocumentaleTypeRed;
import it.ibm.red.webservice.model.redservice.types.v1.MapRed;
import it.ibm.red.webservice.model.redservice.types.v1.MapRed.Elemento;
import it.ibm.red.webservice.model.redservice.types.v1.MetadatoRed;

/**
 * Classe di test FEPA.
 */
public class FepaTest extends AbstractTest {

	/**
	 * Nome del file di test.
	 */
	private static final String FILE_TEST_NAME = "fileTest.pdf";

	/**
	 * Messaggio di errore associato ad un document title assente.
	 */
	private static final String ERROR_DOCUMENT_TITLE_NULL_MSG = "Il document title della fattura non può essere null.";

	/**
	 * Label DOCUMENT TITLE.
	 */
	private static final String DOCUMENT_TITLE_LABEL = "DOCUMENT TITLE : ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FepaTest.class);

	/**
	 * Service.
	 */
	private IREDServiceFacadeSRV redSRV;

	/**
	 * Service.
	 */
	private ISalvaDocumentoFacadeSRV creaDocFacade;

	/**
	 * Service.
	 */
	private IProtocollaDsrSRV protocollaDsr;

	/**
	 * Service.
	 */
	private IChiudiDichiarazioneFacadeSRV chiudiDichiarazioneFacade;

	/**
	 * Service.
	 */
	private IFepaFacadeSRV fepaFacade;

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		redSRV = ApplicationContextProvider.getApplicationContext().getBean(IREDServiceFacadeSRV.class);
		creaDocFacade = ApplicationContextProvider.getApplicationContext().getBean(ISalvaDocumentoFacadeSRV.class);
		protocollaDsr = ApplicationContextProvider.getApplicationContext().getBean(IProtocollaDsrSRV.class);
		chiudiDichiarazioneFacade = ApplicationContextProvider.getApplicationContext().getBean(IChiudiDichiarazioneFacadeSRV.class);
		fepaFacade = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Esegue la query.
	 */
	@Test
	public void executeQ() {
		final UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		final ObjectStore os = getObjectStore(utente);

		final SearchScope scope = new SearchScope(os);

		final SearchSQL searchSQL = new SearchSQL();
		searchSQL.setQueryString("SELECT * from DOCUMENTO_NSD where documentTitle>'39000'");

		Integer n = 1;

		Long start = new Date().getTime();
		DocumentSet myDoc = (DocumentSet) scope.fetchObjects(searchSQL, 10, null, true);
		Long stop = new Date().getTime();
		LOGGER.info("PAGINATA" + n++ + " " + (stop - start));
		final PageIterator pi = myDoc.pageIterator();

		while (pi.nextPage()) {
			LOGGER.info("----------------- PAGE START");
			for (final Object docTmp : pi.getCurrentPage()) {
				final Document doc = (Document) docTmp;
				LOGGER.info(doc.get_Id().toString());
			}
			LOGGER.info((n++).toString());
			LOGGER.info("----------------- PAGE END");
		}
		start = new Date().getTime();
		scope.fetchObjects(searchSQL, 10, null, false);
		stop = new Date().getTime();

		LOGGER.info("NON PAGINATA" + n + " " + (stop - start));
	}

	/**
	 * Creazione fattura per continuare la lavorazione su RED.
	 */
	@Test
	public final void testCreazioneFatturaAutoassegnata() {

		// Lattanzio decide di continuare la lavorazione della fattura su RED.
		final String dtFattura = createFattura(UUID.randomUUID().toString(), UUID.randomUUID().toString(), ID_AOO_RGS, ID_IGICS_UFFICIO_I, ID_GM_LATTANZIO + "");
		checkAssertion(!StringUtils.isNullOrEmpty(dtFattura), ERROR_DOCUMENT_TITLE_NULL_MSG);

		LOGGER.info(DOCUMENT_TITLE_LABEL + dtFattura);
	}

	/**
	 * Creazione fattura per assegnarla su RED ad un altro utente e consentirgli la
	 * lavorazione.
	 */
	@Test
	public final void testCreazioneFatturaAssegnata() {

		final Integer idAoo = ID_AOO_RGS;
		final Integer idUfficioMittenteFattura = ID_IGICS_UFFICIO_I;
		final String idUtenteMittenteFattura = ID_PAOLO_POGGESI + "";
		final Integer idUfficioDestinatarioFattura = ID_IGICS_UFFICIO_I;
		final String idUtenteDestinatarioFattura = ID_GM_LATTANZIO + "";
		final String idFascicoloFEPA = UUID.randomUUID().toString();
		final String idDocumentoFEPA = UUID.randomUUID().toString();

		// Il mittente si autoassegna la fattura.
		String dtFattura = createFattura(idFascicoloFEPA, idDocumentoFEPA, idAoo, idUfficioMittenteFattura, idUtenteMittenteFattura);
		checkAssertion(!StringUtils.isNullOrEmpty(dtFattura), ERROR_DOCUMENT_TITLE_NULL_MSG);

		LOGGER.info(DOCUMENT_TITLE_LABEL + dtFattura);

		// Colui che ha in carico la fattura (ovvero il mittente) l'assegna ad un altro
		// utente.
		dtFattura = assegnaFattura(idFascicoloFEPA, idDocumentoFEPA, idAoo, idUfficioMittenteFattura, idUtenteMittenteFattura, idUfficioDestinatarioFattura,
				idUtenteDestinatarioFattura);

		checkAssertion(!StringUtils.isNullOrEmpty(dtFattura), ERROR_DOCUMENT_TITLE_NULL_MSG);

		LOGGER.info(DOCUMENT_TITLE_LABEL + dtFattura);
	}

	/**
	 * Suppongo che l'utente voglia continuare la lavorazione da un sistema esterno
	 * su RED. Essendo generati random idFascicolo e idDocumento FEPA non potremmo
	 * riconciliare con FEPA, ma in questo modo siamo indipendenti dal sistema
	 * stesso (in pratica con queste fatture non possiamo integrarci con i sistemi
	 * esterni).
	 * 
	 * @param idFascicoloFEPA identificativo fascicolo
	 * @param idDocumentoFEPA identificativo documento
	 * @param idAoo           aoo di riferimento
	 * @param idUfficio       ufficio di riferimento
	 * @param idUtente        utente di riferimento
	 * @return document title fattura
	 */
	private String createFattura(final String idFascicoloFEPA, final String idDocumentoFEPA, final Integer idAoo, final Integer idUfficio, final String idUtente) {
		// Assegni la fattura a te stesso in fase di creazione.
		return assegnaFattura(idFascicoloFEPA, idDocumentoFEPA, idAoo, idUfficio, idUtente, idUfficio, idUtente);
	}

	private String assegnaFattura(final String idFascicoloFEPA, final String idDocumentoFEPA, final Integer idAoo, final Integer idUfficioMittenteFattura,
			final String idUtenteMittenteFattura, final Integer idUfficioDestinatarioFattura, final String idUtenteDestinatarioFattura) {

		final String oggettoDocumentoRED = createObj();

		final ObjectFactory of = new ObjectFactory();

		final CredenzialiUtenteRed credenziali = new CredenzialiUtenteRed();
		credenziali.setIdAoo(idAoo);
		credenziali.setIdGruppo(idUfficioMittenteFattura);
		credenziali.setIdUtente(of.createCredenzialiUtenteRedIdUtente(idUtenteMittenteFattura));

		final MapRed mapRed = new MapRed();

		final Elemento eUtenteCedente = new Elemento();
		eUtenteCedente.setKey(MapRedKey.UTENTE_CEDENTE_KEY);
		eUtenteCedente.setValore(idUtenteMittenteFattura);
		mapRed.getElemento().add(eUtenteCedente);

		final Elemento eDestinatario = new Elemento();
		eDestinatario.setKey(MapRedKey.UTENTE_DESTINATARIO_KEY);
		eDestinatario.setValore(idUtenteDestinatarioFattura);
		mapRed.getElemento().add(eDestinatario);

		final Elemento eUfficioCedente = new Elemento();
		eUfficioCedente.setKey(MapRedKey.UFFICIO_DESTINATARIO_KEY);
		eUfficioCedente.setValore(idUfficioDestinatarioFattura + "");
		mapRed.getElemento().add(eUfficioCedente);

		final String inFileName = FILE_TEST_NAME;
		final byte[] inContent = FileUtils.getFileFromInternalResources(FILE_TEST_NAME);
		final String inMimeType = MediaType.PDF.toString();

		return redSRV.assegnaFascicoloFatturaWithoutFepa(credenziali, idFascicoloFEPA, mapRed, idDocumentoFEPA, oggettoDocumentoRED, inFileName, inContent, inMimeType);
	}

	/**
	 * Creazione decreto.
	 */
	@Test
	public final void testCreazioneDecreto() {
		final String dtDecreto = creaDecreto(ID_FORMISANO);
		LOGGER.info("DOCUMENT TITLE DECRETO : " + dtDecreto);
	}

	private String creaDecreto(final Integer idDestinatario) {
		final String idFascicoloFEPA = UUID.randomUUID().toString();
		final String idDocumentoFEPA = UUID.randomUUID().toString();
		final String idFascicoloFEPAdd = UUID.randomUUID().toString();
		// Lattanzio decide di continuare la lavorazione della fattura su RED.
		final String dtFattura = createFattura(idFascicoloFEPA, idDocumentoFEPA, ID_AOO_RGS, ID_IGICS_UFFICIO_I, idDestinatario + "");
		checkAssertion(!StringUtils.isNullOrEmpty(dtFattura), ERROR_DOCUMENT_TITLE_NULL_MSG);

		final CredenzialiUtenteRed credenziali = new CredenzialiUtenteRed();
		credenziali.setIdAoo(ID_AOO_RGS);
		credenziali.setIdGruppo(ID_IGICS_UFFICIO_I);
		credenziali.setIdUtente(new ObjectFactory().createCredenzialiUtenteRedIdUtente(idDestinatario + ""));

		final String numeroProtocolloDecreto = "1";
		final Date dataProtocollo = new Date();

		// Le fatture saranno indicizzate tramite gli di fascicolo fepa associati.
		final it.ibm.red.webservice.model.redservice.types.v1.ObjectFactory of = new it.ibm.red.webservice.model.redservice.types.v1.ObjectFactory();
		final IdentificativoUnitaDocumentaleFEPATypeRed fascicolo = new IdentificativoUnitaDocumentaleFEPATypeRed();
		final IdentificativoUnitaDocumentaleTypeRed value = of.createIdentificativoUnitaDocumentaleTypeRed();
		value.setID(idFascicoloFEPA);
		fascicolo.setID(value);

		final List<IdentificativoUnitaDocumentaleFEPATypeRed> fascicoli = new ArrayList<>();
		fascicoli.add(fascicolo);

		final String inFileName = "fileTest.docx";
		final byte[] inContent = FileUtils.getFileFromInternalResources("fileTest.docx");
		final String inMimeType = Constants.ContentType.DOCX;

		final DocumentoRed documento = new DocumentoRed();
		documento.setDataHandler(of.createDocumentoRedDataHandler(inContent));
		final ClasseDocumentaleRed cdr = of.createClasseDocumentaleRed();

		final MetadatoRed eOggettoProtocollo = new MetadatoRed();
		eOggettoProtocollo.setKey(of.createMetadatoRedKey(MapRedKey.OGGETTO_PROTOCOLLO_FEPA));
		eOggettoProtocollo.setValue(of.createMetadatoRedValue(createObj()));
		cdr.getMetadato().add(eOggettoProtocollo);

		final MetadatoRed eNomeFile = new MetadatoRed();
		eNomeFile.setKey(of.createMetadatoRedKey(MapRedKey.NOME_FILE_FEPA));
		eNomeFile.setValue(of.createMetadatoRedValue(inFileName));
		cdr.getMetadato().add(eNomeFile);

		final MetadatoRed eNumeroProtocollo = new MetadatoRed();
		eNumeroProtocollo.setKey(of.createMetadatoRedKey(MapRedKey.NUM_PROTOCOLLO_FEPA));
		eNumeroProtocollo.setValue(of.createMetadatoRedValue(numeroProtocolloDecreto));
		cdr.getMetadato().add(eNumeroProtocollo);

		final MetadatoRed eDataProtocollo = new MetadatoRed();
		eDataProtocollo.setKey(of.createMetadatoRedKey(MapRedKey.DATA_PROTOCOLLO_FEPA));
		eDataProtocollo.setValue(of.createMetadatoRedValue(DateUtils.dateToString(dataProtocollo, DateUtils.DD_MM_YYYY)));
		cdr.getMetadato().add(eDataProtocollo);

		documento.setIdFascicolo(of.createDocumentoRedIdFascicolo(idFascicoloFEPAdd));

		final MapRed metadati = new MapRed();

		documento.setContentType(of.createDocumentoRedContentType(inMimeType));

		documento.setClasseDocumentale(of.createDocumentoRedClasseDocumentale(cdr));

		final String dtDecreto = redSRV.inserimentoFascicoloDecreto(credenziali, metadati, documento, fascicoli);
		checkAssertion(!StringUtils.isNullOrEmpty(dtDecreto), "Il document title del decreto non può essere null.");
		return dtDecreto;
	}

	private String protocollaDSR() {
		final UtenteDTO utente = getUtente(USERNAME_GOZZI);

		final String guidMail = "AF65EA02-AF84-45A5-9D27-4DACE4C8D90C"; // Content annotation creata per il nuovo documento

		final String mailOggetto = "Una DSR di test.";
		final Contatto contatto = getContatto(ID_CONTATTO_IRENE);

		DetailDocumentRedDTO detailsProt = new DetailDocumentRedDTO();

		detailsProt.setContent(FileUtils.getFileFromInternalResources(FILE_TEST_NAME));
		detailsProt.setNomeFile(FILE_TEST_NAME);
		detailsProt.setMimeType(MediaType.PDF.toString());

		detailsProt.setProtocollazioneMailGuid(guidMail);
		detailsProt.setMittenteContatto(contatto);
		detailsProt.setOggetto(mailOggetto);
		detailsProt.setIsNotifica(false);
		detailsProt.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0]); // Vuol dire che è in
																									// entrata

		detailsProt = protocollaDsr.initializeDetailDocumentForProtocollazioneDsr(detailsProt, utente); // in automatico
																										// assegna per
																										// competenza
																										// all'"ufficio
																										// FEPA"

		final SalvaDocumentoRedParametriDTO parametri = new SalvaDocumentoRedParametriDTO();
		parametri.setInputMailGuid(guidMail);
		parametri.setModalita(Modalita.MODALITA_INS_MAIL);
		parametri.setContentVariato(true);

		final EsitoSalvaDocumentoDTO esito = creaDocFacade.salvaDocumento(detailsProt, parametri, utente, ProvenienzaSalvaDocumentoEnum.FEPA_DSR);
		final String dt = esito.getDocumentTitle();
		checkAssertion(esito.isEsitoOk(), "La dsr non è stata protocollata correttamente.");
		return dt;
	}

	private String predisposizioneDichiarazione(final String dtDSRIngresso) {
		final UtenteDTO utentePredisposizione = getUtente(USERNAME_DINUZZO);
		utentePredisposizione.setIdUfficio(ID_IGICS.longValue());

		final UtenteDTO utente = getUtente(USERNAME_GOZZI);
		final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(utentePredisposizione.getFcDTO(), utentePredisposizione.getIdAoo());

		final List<FascicoloDTO> fascicoli = fceh.getFascicoliDocumento(dtDSRIngresso, utente.getIdAoo().intValue());

		DetailDocumentRedDTO detailsPredisposizione = new DetailDocumentRedDTO();
		detailsPredisposizione = protocollaDsr.initializeDetailDocumentForProtocollazioneDsr(detailsPredisposizione, utentePredisposizione); // Esiste un analogo per
																																				// predisponi dichiarazione?

		final AssegnazioneDTO perFirma = createAssegnazioneFirma(ID_IGICS, utentePredisposizione.getId().intValue(), "CARMINE", "DI NUZZO");
		detailsPredisposizione.getAssegnazioni().add(perFirma);

		// Assegno per firma a DI NUZZO

		detailsPredisposizione.setContent(FileUtils.getFileFromInternalResources("dsr1.pdf"));
		detailsPredisposizione.setNomeFile("dsr1.pdf");
		detailsPredisposizione.setMimeType(MediaType.PDF.toString());

		final DestinatarioRedDTO destinatario = createDestinatarioInterno(ID_IGICS_UFFICIO_I, ID_EM_ZAZZA, ID_CONTATTO_IRENE, ModalitaDestinatarioEnum.TO);
		final List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(destinatario);
		detailsPredisposizione.setDestinatari(destinatari);
		detailsPredisposizione.setOggetto("PREDISPOSIZIONE DSR");

		detailsPredisposizione.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_USCITA.getIds()[0]); // Vuol
																												// dire
																												// che è
																												// in
																												// uscita
		detailsPredisposizione.setIdTipologiaDocumento(409);
		detailsPredisposizione.setIdTipologiaProcedimento(288);

		detailsPredisposizione.setFascicoli(fascicoli);

		final Document d = fceh.getDocumentByDTandAOO(dtDSRIngresso, utente.getIdAoo());
		final Integer annoProtocolloIngresso = (Integer) TrasformerCE.getMetadato(d, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
		final Integer numeroProtocolloIngresso = (Integer) TrasformerCE.getMetadato(d, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);

		final RispostaAllaccioDTO allaccio = new RispostaAllaccioDTO();
		allaccio.setAnnoProtocollo(annoProtocolloIngresso);
		allaccio.setNumeroProtocollo(numeroProtocolloIngresso);
		allaccio.setIdDocumentoAllacciato(dtDSRIngresso);
		final List<RispostaAllaccioDTO> allacci = new ArrayList<>();
		allacci.add(allaccio);
		detailsPredisposizione.setAllacci(allacci);

		final SalvaDocumentoRedParametriDTO parametriPredisposizione = new SalvaDocumentoRedParametriDTO();
		parametriPredisposizione.setModalita(Modalita.MODALITA_INS);
		parametriPredisposizione.setContentVariato(true);
		final EsitoSalvaDocumentoDTO esitoPredisposizione = creaDocFacade.salvaDocumento(detailsPredisposizione, parametriPredisposizione, utentePredisposizione,
				ProvenienzaSalvaDocumentoEnum.FEPA_DSR);
		checkAssertion(esitoPredisposizione.isEsitoOk(), "La dsr non è stata protocollata correttamente.");

		return esitoPredisposizione.getDocumentTitle();
	}

	/**
	 * Test protocollazione e firma del DSR.
	 */
	@Test
	public final void testProtocollaEFirmaDSR() {
		final String otp = "452348";
		final String dtUscita = signDSR(otp);
		LOGGER.info("DOCUMENT TITLE DSR Firmata : " + dtUscita);
	}

	/**
	 * Test predisposizione DSR.
	 */
	@Test
	public final void testPredisponiDSR() {
		final String dtDSRIngresso = "452400";
		final String dtDSRUscita = predisposizioneDichiarazione(dtDSRIngresso);
		LOGGER.info("DOCUMENT TITLE DSR Predisposta e inviata in firma : " + dtDSRUscita);
	}

	/**
	 * Esegue la firma del DSR.
	 */
	@Test
	public final void firmaDSR() {
		final String dtDSRUscita = "452424";
		final String otp = "176724";
		final EsitoOperazioneDTO esitoFirma = firmaRemota(DocumentQueueEnum.NSD, ID_IGICS, USERNAME_DINUZZO, SignTypeEnum.CADES, dtDSRUscita, otp, "12345678").iterator()
				.next();

		checkAssertion(esitoFirma.isEsito(), "LA firma remota della DSR in uscita non funziona.");
		LOGGER.info("DOCUMENT TITLE DSR Firmata : " + dtDSRUscita);
	}

	private String signDSR(final String otp) {

		final String dtDSRIngresso = protocollaDSR();

		final EsitoOperazioneDTO esitoAss = assegnaDaCorriere(USERNAME_GOZZI, dtDSRIngresso, USERNAME_GOZZI, ID_IGICS);
		checkAssertion(esitoAss.isEsito(), "L'assegnazione della DSR non funziona.");

		final String dtDSRUscita = predisposizioneDichiarazione(dtDSRIngresso);

		final EsitoOperazioneDTO esitoFirma = firmaRemota(DocumentQueueEnum.NSD, ID_IGICS, USERNAME_DINUZZO, SignTypeEnum.CADES, dtDSRUscita, otp, "12345678").iterator()
				.next();

		checkAssertion(esitoFirma.isEsito(), "LA firma remota della DSR in uscita non funziona.");

		return dtDSRUscita;
	}

	/**
	 * Test chiusura senza associazioni.
	 */
	@Test
	public final void testChiudiSenzaAssociazioni() {
		final String otp = "466875";
		final String dt = signDSR(otp);
		final UtenteDTO operatore = getUtente(USERNAME_FORMISANO);
		operatore.setIdUfficio(ID_IGICS_UFFICIO_I.longValue());
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, null, dt, operatore.getIdUfficio().intValue(), operatore.getId().intValue());

		final EsitoOperazioneDTO esitoChiusura = chiudiDichiarazioneFacade.chiudiDichiarazione(wo.getWorkflowNumber(), operatore);
		checkAssertion(esitoChiusura.isEsito(), "La chiusura senza associazioni della DSR in uscita non funziona.");
	}

	/**
	 * Test modifica associazione.
	 */
	@Test
	public final void testModificaAssociazione() {

		final String otpDSR = "663047";
		final String dtDSRFirmata = signDSR(otpDSR);
		final String dtDecretoErrato = creaDecreto(ID_FORMISANO);

		final UtenteDTO operatore = getUtente(USERNAME_FORMISANO);

		final VWSession session = getPESession(operatore);
		final VWWorkObject woDSR = fromDocumentTitleToWF(session, null, dtDSRFirmata, operatore.getIdUfficio().intValue(), operatore.getId().intValue());
		final VWWorkObject woDecretoErrato = fromDocumentTitleToWF(session, null, dtDecretoErrato, operatore.getIdUfficio().intValue(), operatore.getId().intValue());

		final List<String> wobNumbersDecretoErrato = new ArrayList<>();
		wobNumbersDecretoErrato.add(woDecretoErrato.getWorkflowNumber());

		final String wobNumberDSR = woDSR.getWorkflowNumber();
		final EsitoOperazioneDTO esitoAssociazione = fepaFacade.associaDecreto(operatore, wobNumberDSR, wobNumbersDecretoErrato);
		checkAssertion(esitoAssociazione.isEsito(), "L'associazione della DSR al decreto non funziona.");

		final String dtDecretoCorretto = creaDecreto(ID_FORMISANO);

		final VWWorkObject woDecretoCorretto = fromDocumentTitleToWF(session, null, dtDecretoCorretto, operatore.getIdUfficio().intValue(), operatore.getId().intValue());

		final List<String> wobNumbersDecretoCorretto = new ArrayList<>();
		wobNumbersDecretoCorretto.add(woDecretoCorretto.getWorkflowNumber());

		final EsitoOperazioneDTO esitoModificaAssociazione = fepaFacade.modificaAssociazioni(operatore, wobNumberDSR, wobNumbersDecretoCorretto, wobNumbersDecretoErrato);
		checkAssertion(esitoModificaAssociazione.isEsito(), "La modifica dell'associazione della DSR al decreto non funziona.");

	}

	/**
	 * Test firma del decreto.
	 */
	@Test
	public final void testFirmaDecreto() {
		final UtenteDTO operatore = getUtente(USERNAME_FORMISANO);

		final VWSession session = getPESession(operatore);

		final String otpDSR = "935545";
		final String dtDSRFirmata = signDSR(otpDSR);
		final String dtDecreto = creaDecreto(ID_FORMISANO);
		final VWWorkObject woDSR = fromDocumentTitleToWF(session, null, dtDSRFirmata, operatore.getIdUfficio().intValue(), operatore.getId().intValue());
		final VWWorkObject woDecreto = fromDocumentTitleToWF(session, null, dtDecreto, operatore.getIdUfficio().intValue(), operatore.getId().intValue());
		final List<String> wobNumbersDecreto = new ArrayList<>();
		wobNumbersDecreto.add(woDecreto.getWorkflowNumber());

		final String wobNumberDSR = woDSR.getWorkflowNumber();
		final EsitoOperazioneDTO esitoAssociazione = fepaFacade.associaDecreto(operatore, wobNumberDSR, wobNumbersDecreto);
		checkAssertion(esitoAssociazione.isEsito(), "L'associazione della DSR al decreto non funziona.");

		createOrdinePagamento(dtDecreto, USERNAME_FORMISANO);

		final EsitoOperazioneDTO esitoInviaFirmaDecreto = fepaFacade.invioDecretoInFirma(operatore, woDecreto.getWorkflowNumber(), ID_IGICS_UFFICIO_I.longValue(),
				ID_ANGELA_TOMARO.longValue(), "Un invio in firma di prova.");
		checkAssertion(esitoInviaFirmaDecreto.isEsito(), "Invio in firma del decreto non funziona.");

		LOGGER.info(dtDecreto);

	}

}
