package it.ibm.business.test;

import static org.junit.Assert.assertFalse;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.service.concrete.OrganigrammaSRV;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;

/**
 * Classe di test organigramma.
 */
public class OrganigrammaTest extends AbstractTest {

	/**
	 * Servizio.
	 */
	private IOrganigrammaFacadeSRV organigrammaSRV;
	
	/**
	 * Utente.
	 */
	private UtenteDTO utente;
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public void initializeContext() {
		before();
		organigrammaSRV = getBean(OrganigrammaSRV.class);
		IUtenteFacadeSRV facadeUtente = getBean(IUtenteFacadeSRV.class);
		utente = facadeUtente.getByUsername("BIAGIO.MAZZOTTA");
		
		
	}
	
	/**
	 * Esegue il test recupero tree node.
	 */
	@Test
	public void testRecuperoTreeNode() {
		NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
		nodoRadice.setIdAOO(utente.getIdAoo());
		nodoRadice.setIdNodo(utente.getIdNodoRadiceAOO());
		nodoRadice.setCodiceAOO(utente.getCodiceAoo());
		nodoRadice.setUtentiVisible(false);
		List<NodoOrganigrammaDTO> listRoot = organigrammaSRV.getFigliAlberoUtentiAssegnatariFilteredByPermesso(utente.getIdUfficio(), nodoRadice, ResponsesRedEnum.INVIO_IN_FIRMA);
		assertFalse(listRoot.isEmpty());
	}
	
	
}
