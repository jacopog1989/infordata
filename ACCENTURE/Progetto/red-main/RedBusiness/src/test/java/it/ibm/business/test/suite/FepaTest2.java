package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.filenet.api.core.Document;
import com.google.common.net.MediaType;

import filenet.vw.api.VWSession;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.MapRedKey;
import it.ibm.red.business.constants.Constants.Modalita;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IProtocollaDsrSRV;
import it.ibm.red.business.service.facade.IFepaFacadeSRV;
import it.ibm.red.business.service.facade.IREDServiceFacadeSRV;
import it.ibm.red.business.service.facade.ISalvaDocumentoFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.webservice.model.redservice.header.v1.CredenzialiUtenteRed;
import it.ibm.red.webservice.model.redservice.header.v1.ObjectFactory;
import it.ibm.red.webservice.model.redservice.types.v1.ClasseDocumentaleRed;
import it.ibm.red.webservice.model.redservice.types.v1.DocumentoRed;
import it.ibm.red.webservice.model.redservice.types.v1.IdentificativoUnitaDocumentaleFEPATypeRed;
import it.ibm.red.webservice.model.redservice.types.v1.IdentificativoUnitaDocumentaleTypeRed;
import it.ibm.red.webservice.model.redservice.types.v1.MapRed;
import it.ibm.red.webservice.model.redservice.types.v1.MapRed.Elemento;
import it.ibm.red.webservice.model.redservice.types.v1.MetadatoRed;

/**
 * Classe di test FEPA.
 */
public class FepaTest2 extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FepaTest2.class);
	
	/**
	 * Nome del file di test.
	 */
	private static final String FILE_TEST_NAME = "fileTest.pdf";

	/**
	 * Service.
	 */
	private IREDServiceFacadeSRV redSRV;

	/**
	 * Service.
	 */
	private ISalvaDocumentoFacadeSRV creaDocFacade;

	/**
	 * Service.
	 */
	private IProtocollaDsrSRV protocollaDsr;

	/**
	 * Service.
	 */
	private IFepaFacadeSRV fepaFacade;

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		redSRV = ApplicationContextProvider.getApplicationContext().getBean(IREDServiceFacadeSRV.class);
		creaDocFacade = ApplicationContextProvider.getApplicationContext().getBean(ISalvaDocumentoFacadeSRV.class);
		protocollaDsr = ApplicationContextProvider.getApplicationContext().getBean(IProtocollaDsrSRV.class);
		fepaFacade = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Crea il DSR Uscita a partire da un GUID di una mail "per test DSR". Mostra in console il numero documento da firmare
	 * comunicando l'utente con cui firmare e il document title da usare per la creazione del DD in uscita.
	 * Per recuperare la guid della mail occorre cercare nelle mail di ROSELLA.GOZZI una mail con oggetto like "per test DSR" e ripristinarla.
	 */
	@Test
	public void creaDSRUscita() {
		final String protocollatore = USERNAME_ZAZZA;
		final String guidMail = "AF65EA02-AF84-45A5-9D27-4DACE4C8D90C";
		final String utenteCorriere = USERNAME_GOZZI;
		final String utenteAssegnatario = USERNAME_GOZZI;
		final String utenteFirmatarioDSR = USERNAME_DINUZZO;
		final Integer ufficioFirmatarioDSR = ID_IGICS;
		final UtenteDTO destinatarioDSR = getUtente(USERNAME_ZAZZA);

		//Protocolliamo la DSR
		final String dtDSRIngresso = protocollaDSR(protocollatore, guidMail);

		//Dal corriere all'assegnatario
		final EsitoOperazioneDTO esitoAss = assegnaDaCorriere(utenteCorriere, dtDSRIngresso, utenteAssegnatario, ID_IGICS);
		checkAssertion(esitoAss.isEsito(), "L'assegnazione della DSR non funziona.");
		
		//Generiamo la DSR in uscita assegnandola in firma e definendo il destinatario
		final EsitoSalvaDocumentoDTO esito = predisposizioneDichiarazione(dtDSRIngresso,  utenteFirmatarioDSR, ufficioFirmatarioDSR, destinatarioDSR);				
		final String dtDSRUscita = esito.getDocumentTitle();
		checkAssertion(!StringUtils.isNullOrEmpty(dtDSRUscita), "DSR non generata correttamente.");
		LOGGER.info("Numero documento da firmare sul libro firma di " + utenteFirmatarioDSR + " [" + esito.getNumeroDocumento() + "]");
		LOGGER.info("Document title da usare per la creazione del DD in uscita [" + esito.getDocumentTitle() + "]");
	}

	/**
	 * Crea la DD in uscita.
	 */
	@Test
	public void creaDDUscita() {
		final UtenteDTO destinatarioDecreto = getUtente(USERNAME_ZAZZA); // Deve coincidere col destinatario della DD
		final UtenteDTO utenteFirmatarioDDDTO = getUtente(USERNAME_PAOLUCCI);
		final String dtDSRUscita = "467634";
		
		//Creiamo DD assegnato al destinatario ed associamogli una fattura
		final String dtDecreto = creaDecreto(destinatarioDecreto.getId().intValue());
		checkAssertion(!StringUtils.isNullOrEmpty(dtDecreto), "DD non creato correttamente.");

		//Se il destinatario della DSR generata da "creaDSRUscita" e quello della DD coincidono questi potranno essere associati

		final VWSession session = getPESession(destinatarioDecreto);
		final VWWorkObject woDSR = fromDocumentTitleToWF(session, null, dtDSRUscita, destinatarioDecreto.getIdUfficio().intValue(), destinatarioDecreto.getId().intValue());
		final VWWorkObject woDecreto = fromDocumentTitleToWF(session, null, dtDecreto, destinatarioDecreto.getIdUfficio().intValue(), destinatarioDecreto.getId().intValue());
		final List<String> wobNumbersDecreto = new ArrayList<>();
		wobNumbersDecreto.add(woDecreto.getWorkflowNumber());
		final String wobNumberDSR = woDSR.getWorkflowNumber();
		final EsitoOperazioneDTO esitoAssociazione = fepaFacade.associaDecreto(destinatarioDecreto, wobNumberDSR, wobNumbersDecreto);
		checkAssertion(esitoAssociazione.isEsito(), "L'associazione della DSR al decreto non funziona.");

		//Creiamo ordine di pagamento ed associao al sdecreto
				
		createOrdinePagamento(dtDecreto, destinatarioDecreto.getUsername());

		//Inviamo in firma il decreto

		final EsitoOperazioneDTO esitoInviaFirmaDecreto = fepaFacade.invioDecretoInFirma(destinatarioDecreto,
				woDecreto.getWorkflowNumber(), utenteFirmatarioDDDTO.getIdUfficio(), utenteFirmatarioDDDTO.getId(), "Un invio in firma di prova.");
		checkAssertion(esitoInviaFirmaDecreto.isEsito(), "Invio in firma del decreto non funziona.");
		LOGGER.info("Numero documento da firmare sul libro firma di " + utenteFirmatarioDDDTO.getUsername() + " [" + esitoInviaFirmaDecreto.getIdDocumento() + "]");

	}

	
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
	
	private String createFattura(final String idFascicoloFEPA, final String idDocumentoFEPA, final Integer idAoo,
			final Integer idUfficio, final String idUtente) {
		// Assegni la fattura a te stesso in fase di creazione.
		return assegnaFattura(idFascicoloFEPA, idDocumentoFEPA, idAoo, idUfficio, idUtente, idUfficio, idUtente);
	}

	private String assegnaFattura(final String idFascicoloFEPA, final String idDocumentoFEPA, final Integer idAoo,
			final Integer idUfficioMittenteFattura, final String idUtenteMittenteFattura,
			final Integer idUfficioDestinatarioFattura, final String idUtenteDestinatarioFattura) {

		final String oggettoDocumentoRED = createObj();

		final ObjectFactory of = new ObjectFactory();

		final CredenzialiUtenteRed credenziali = new CredenzialiUtenteRed();
		credenziali.setIdAoo(idAoo);
		credenziali.setIdGruppo(idUfficioMittenteFattura);
		credenziali.setIdUtente(of.createCredenzialiUtenteRedIdUtente(idUtenteMittenteFattura));

		final MapRed mapRed = new MapRed();

		final Elemento eUtenteCedente = new Elemento();
		eUtenteCedente.setKey(MapRedKey.UTENTE_CEDENTE_KEY);
		eUtenteCedente.setValore(idUtenteMittenteFattura);
		mapRed.getElemento().add(eUtenteCedente);

		final Elemento eDestinatario = new Elemento();
		eDestinatario.setKey(MapRedKey.UTENTE_DESTINATARIO_KEY);
		eDestinatario.setValore(idUtenteDestinatarioFattura);
		mapRed.getElemento().add(eDestinatario);

		final Elemento eUfficioCedente = new Elemento();
		eUfficioCedente.setKey(MapRedKey.UFFICIO_DESTINATARIO_KEY);
		eUfficioCedente.setValore(idUfficioDestinatarioFattura + "");
		mapRed.getElemento().add(eUfficioCedente);

		final String inFileName = FILE_TEST_NAME;
		final byte[] inContent = FileUtils.getFileFromInternalResources(FILE_TEST_NAME);
		final String inMimeType = MediaType.PDF.toString();

		return redSRV.assegnaFascicoloFatturaWithoutFepa(credenziali, idFascicoloFEPA, mapRed, idDocumentoFEPA,
				oggettoDocumentoRED, inFileName, inContent, inMimeType);
	}

	private String creaDecreto(final Integer idDestinatario) {
		final String idFascicoloFEPA = UUID.randomUUID().toString();
		final String idDocumentoFEPA = UUID.randomUUID().toString();
		final String idFascicoloFEPAdd = UUID.randomUUID().toString();
		// Lattanzio decide di continuare la lavorazione della fattura su RED.
		final String dtFattura = createFattura(idFascicoloFEPA, idDocumentoFEPA, ID_AOO_RGS, ID_IGICS_UFFICIO_I,
				idDestinatario + "");
		checkAssertion(!StringUtils.isNullOrEmpty(dtFattura), "Il document title della fattura non può essere null.");

		final CredenzialiUtenteRed credenziali = new CredenzialiUtenteRed();
		credenziali.setIdAoo(ID_AOO_RGS);
		credenziali.setIdGruppo(ID_IGICS_UFFICIO_I);
		credenziali.setIdUtente(new ObjectFactory().createCredenzialiUtenteRedIdUtente(idDestinatario + ""));

		final String numeroProtocolloDecreto = "1";
		final Date dataProtocollo = new Date();

		// Le fatture saranno indicizzate tramite gli di fascicolo fepa associati.
		final it.ibm.red.webservice.model.redservice.types.v1.ObjectFactory of = new it.ibm.red.webservice.model.redservice.types.v1.ObjectFactory();
		final IdentificativoUnitaDocumentaleFEPATypeRed fascicolo = new IdentificativoUnitaDocumentaleFEPATypeRed();
		final IdentificativoUnitaDocumentaleTypeRed value = of.createIdentificativoUnitaDocumentaleTypeRed();
		value.setID(idFascicoloFEPA);
		fascicolo.setID(value);

		final List<IdentificativoUnitaDocumentaleFEPATypeRed> fascicoli = new ArrayList<>();
		fascicoli.add(fascicolo);

		final String inFileName = "fileTest.docx";
		final byte[] inContent = FileUtils.getFileFromInternalResources("fileTest.docx");
		final String inMimeType = Constants.ContentType.DOCX;

		final DocumentoRed documento = new DocumentoRed();
		documento.setDataHandler(of.createDocumentoRedDataHandler(inContent));
		final ClasseDocumentaleRed cdr = of.createClasseDocumentaleRed();

		final MetadatoRed eOggettoProtocollo = new MetadatoRed();
		eOggettoProtocollo.setKey(of.createMetadatoRedKey(MapRedKey.OGGETTO_PROTOCOLLO_FEPA));
		eOggettoProtocollo.setValue(of.createMetadatoRedValue(createObj()));
		cdr.getMetadato().add(eOggettoProtocollo);

		final MetadatoRed eNomeFile = new MetadatoRed();
		eNomeFile.setKey(of.createMetadatoRedKey(MapRedKey.NOME_FILE_FEPA));
		eNomeFile.setValue(of.createMetadatoRedValue(inFileName));
		cdr.getMetadato().add(eNomeFile);

		final MetadatoRed eNumeroProtocollo = new MetadatoRed();
		eNumeroProtocollo.setKey(of.createMetadatoRedKey(MapRedKey.NUM_PROTOCOLLO_FEPA));
		eNumeroProtocollo.setValue(of.createMetadatoRedValue(numeroProtocolloDecreto));
		cdr.getMetadato().add(eNumeroProtocollo);

		final MetadatoRed eDataProtocollo = new MetadatoRed();
		eDataProtocollo.setKey(of.createMetadatoRedKey(MapRedKey.DATA_PROTOCOLLO_FEPA));
		eDataProtocollo
				.setValue(of.createMetadatoRedValue(DateUtils.dateToString(dataProtocollo, DateUtils.DD_MM_YYYY)));
		cdr.getMetadato().add(eDataProtocollo);

		documento.setIdFascicolo(of.createDocumentoRedIdFascicolo(idFascicoloFEPAdd));

		final MapRed metadati = new MapRed();

		documento.setContentType(of.createDocumentoRedContentType(inMimeType));

		documento.setClasseDocumentale(of.createDocumentoRedClasseDocumentale(cdr));

		final String dtDecreto = redSRV.inserimentoFascicoloDecreto(credenziali, metadati, documento, fascicoli);
		checkAssertion(!StringUtils.isNullOrEmpty(dtDecreto), "Il document title del decreto non può essere null.");
		return dtDecreto;
	}

	private String protocollaDSR(final String protocollatore, final String guidMail) {
		final UtenteDTO utente = getUtente(protocollatore);

		final String mailOggetto = "Una DSR di test.";
		final Contatto contatto = getContatto(ID_CONTATTO_IRENE);

		DetailDocumentRedDTO detailsProt = new DetailDocumentRedDTO();

		detailsProt.setContent(FileUtils.getFileFromInternalResources(FILE_TEST_NAME));
		detailsProt.setNomeFile(FILE_TEST_NAME);
		detailsProt.setMimeType(MediaType.PDF.toString());

		detailsProt.setProtocollazioneMailGuid(guidMail);
		detailsProt.setMittenteContatto(contatto);
		detailsProt.setOggetto(mailOggetto);
		detailsProt.setIsNotifica(false);
		detailsProt.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0]); // Vuol dire che è in
																									// entrata

		detailsProt = protocollaDsr.initializeDetailDocumentForProtocollazioneDsr(detailsProt, utente); // in automatico
																										// assegna per
																										// competenza
																										// all'"ufficio
																										// FEPA"

		final SalvaDocumentoRedParametriDTO parametri = new SalvaDocumentoRedParametriDTO();
		parametri.setInputMailGuid(guidMail);
		parametri.setModalita(Modalita.MODALITA_INS_MAIL);
		parametri.setContentVariato(true);

		final EsitoSalvaDocumentoDTO esito = creaDocFacade.salvaDocumento(detailsProt, parametri, utente, ProvenienzaSalvaDocumentoEnum.FEPA_DSR);
		final String dt = esito.getDocumentTitle();
		checkAssertion(esito.isEsitoOk(), "La dsr non è stata protocollata correttamente.");
		return dt;
	}

	private EsitoSalvaDocumentoDTO predisposizioneDichiarazione(final String dtDSRIngresso, final String utenteFirmatario, final Integer ufficioFirmatario, final UtenteDTO destinatarioDTO) {
		final UtenteDTO utentePredisposizione = getUtente(utenteFirmatario);
		utentePredisposizione.setIdUfficio(ufficioFirmatario.longValue());

		final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(utentePredisposizione.getFcDTO(), utentePredisposizione.getIdAoo());

		final List<FascicoloDTO> fascicoli = fceh.getFascicoliDocumento(dtDSRIngresso, utentePredisposizione.getIdAoo().intValue());

		DetailDocumentRedDTO detailsPredisposizione = new DetailDocumentRedDTO();
		detailsPredisposizione = protocollaDsr.initializeDetailDocumentForProtocollazioneDsr(detailsPredisposizione,
				utentePredisposizione); // Esiste un analogo per predisponi dichiarazione?

		final AssegnazioneDTO perFirma = createAssegnazioneFirma(utentePredisposizione.getIdUfficio().intValue(), utentePredisposizione.getId().intValue(),
				utentePredisposizione.getNome(), utentePredisposizione.getCognome());
		detailsPredisposizione.getAssegnazioni().add(perFirma);

		// Assegno per firma a DI NUZZO

		detailsPredisposizione.setContent(FileUtils.getFileFromInternalResources("dsr1.pdf"));
		detailsPredisposizione.setNomeFile("dsr1.pdf");
		detailsPredisposizione.setMimeType(MediaType.PDF.toString());

		final DestinatarioRedDTO destinatario = createDestinatarioInterno(destinatarioDTO.getIdUfficio().intValue(), destinatarioDTO.getId().intValue(), ID_CONTATTO_IRENE, ModalitaDestinatarioEnum.TO);
		final List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(destinatario);
		detailsPredisposizione.setDestinatari(destinatari);
		detailsPredisposizione.setOggetto("PREDISPOSIZIONE DSR");

		detailsPredisposizione.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_USCITA.getIds()[0]); // Vuol
																												// dire
																												// che è
																												// in
																												// uscita
		detailsPredisposizione.setIdTipologiaDocumento(409);
		detailsPredisposizione.setIdTipologiaProcedimento(288);

		detailsPredisposizione.setFascicoli(fascicoli);

		final RispostaAllaccioDTO allaccio = new RispostaAllaccioDTO();
		final Document docDSR = fceh.getDocumentByDTandAOO(dtDSRIngresso, utentePredisposizione.getIdAoo());
		
		final Integer annoProtocolloDSRIngresso = (Integer) TrasformerCE.getMetadato(docDSR, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
		final Integer numeroProtocolloDSRIngresso = (Integer) TrasformerCE.getMetadato(docDSR, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
				
		allaccio.setNumeroProtocollo(numeroProtocolloDSRIngresso);
		allaccio.setAnnoProtocollo(annoProtocolloDSRIngresso);
		allaccio.setIdDocumentoAllacciato(dtDSRIngresso);
		final List<RispostaAllaccioDTO> allacci = new ArrayList<>();
		allacci.add(allaccio);
		detailsPredisposizione.setAllacci(allacci);

		final SalvaDocumentoRedParametriDTO parametriPredisposizione = new SalvaDocumentoRedParametriDTO();
		parametriPredisposizione.setModalita(Modalita.MODALITA_INS);
		parametriPredisposizione.setContentVariato(true);
		final EsitoSalvaDocumentoDTO esitoPredisposizione = creaDocFacade.salvaDocumento(
				detailsPredisposizione, parametriPredisposizione, utentePredisposizione, ProvenienzaSalvaDocumentoEnum.FEPA_DSR);
		checkAssertion(esitoPredisposizione.isEsitoOk(), "La dsr non è stata protocollata correttamente.");

		return esitoPredisposizione;
	}

}
