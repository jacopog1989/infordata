package it.ibm.business.test.ucb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.net.MediaType;

import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.CollectedParametersDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.RecuperoCodeDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Test 04 - Restituizione.
 */
public class Test04Restituzione extends AbstractUCBTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test04Restituzione.class);

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test restituzione firmata.
	 */
	@Test
	public final void restituzioneFirmata() {
		/*
		 * 
		 * 0. Creo un documento in ingresso 1. Andrea produce documento in entrata di
		 * tipo ritiro in autotutela (RITIRO PROVVEDIMENTO?!?). 2. su questo doc
		 * response predisponi restituzione (coda da lavorare); è soppressa sempre a
		 * meno che non è di tipo ritiro in autotutela. protocolloRiferimento è il
		 * protocollo del documento in ingresso che vuoi ritirare.
		 * 
		 * template da Simone
		 * 
		 * 
		 * 3. firmo l'uscita ed ho finito
		 * 
		 */

		final UtenteDTO utente = getUtente(USERNAME_BRIAMONTE);

//		STEP 1
//		BRIAMONTE crea ed assegna al CORRIERE dell'ULAV

		final EsitoSalvaDocumentoDTO esitoCreazione = createDoc(utente, ID_TIPO_DOC_ATTI_SOGGETTI_VISTO_ENTRATA, ID_TIPO_PROC_ATTI_SOGGETTI_VISTO_ENTRATA);

		final String dtIngresso = esitoCreazione.getDocumentTitle();

		dumpCreazioneDocumento(esitoCreazione);
		checkAssertion((esitoCreazione.getErrori() == null || esitoCreazione.getErrori().isEmpty()), "La creazione del documento non è andata a buon fine.");

//		STEP 2
//		Il CORRIERE ULAV lo assegna a BRIAMONTE che se lo ritrova in DA LAVORARE

		final EsitoOperazioneDTO esitoAssegnazione = assegnaDaCorriere(USERNAME_BRIAMONTE, esitoCreazione.getDocumentTitle(), USERNAME_BRIAMONTE);
		checkAssertion(esitoAssegnazione.getFlagEsito(), "L'assegnazione del corriere non è andata a buon fine.");

//		STEP 3
//		BRIAMONTE crea un documento di tipo RITIRO PROVVEDIMENTO

		final String protocolloRiferimento = esitoCreazione.getDocumentTitle();
		final EsitoSalvaDocumentoDTO esitoCreazioneRitiroProv = createDoc(utente, ID_TIPO_DOC_RITIRO_PROV, ID_TIPO_PROC_RITIRO_PROV, protocolloRiferimento);

		final String dtIngressoRP = esitoCreazioneRitiroProv.getDocumentTitle();
		final Integer idDocIngressoRP = esitoCreazioneRitiroProv.getNumeroDocumento();

		dumpCreazioneDocumento(esitoCreazioneRitiroProv);
		checkAssertion((esitoCreazioneRitiroProv.getErrori() == null || esitoCreazioneRitiroProv.getErrori().isEmpty()),
				"La creazione del documento non è andata a buon fine.");

//		STEP 4
//		Il CORRIERE ULAV assegna il RITIRO PROVVEIDMENTO a BRIAMONTE che se lo ritrova in DA LAVORARE

		final EsitoOperazioneDTO esitoAssegnazione2 = assegnaDaCorriere(USERNAME_BRIAMONTE, dtIngressoRP, USERNAME_BRIAMONTE);
		checkAssertion(esitoAssegnazione2.getFlagEsito(), "L'assegnazione del corriere non è andata a buon fine.");

//		STEP 5
//		BRIAMONTE predispone la richiesta restituzione a partire dal RITIRO

		final Integer idRegistro = ID_REGISTRO_RESTITUZIONE;

		// il registro lo popoliamo sulla base del documento di ritiro perchè su quello
		// azioneremo la predisposizione

		final MasterDocumentRedDTO masterIngressoRP = getDocumentForDT(utente, dtIngressoRP, idDocIngressoRP);
		checkAssertion(masterIngressoRP != null, "Documento non trovato.");

		final String classeDocRP = masterIngressoRP.getClasseDocumentale();
		final Integer idCatDocIngressoRP = masterIngressoRP.getIdCategoriaDocumento();

		RecuperoCodeDTO codeIngresso = getQueues(utente.getUsername(), dtIngresso, classeDocRP, idCatDocIngressoRP);
		RecuperoCodeDTO codeIngressoRP = getQueues(utente.getUsername(), dtIngressoRP, classeDocRP, idCatDocIngressoRP);
		checkAssertionCode(codeIngresso, DocumentQueueEnum.DA_LAVORARE_UCB, DocumentQueueEnum.ASSEGNATE);
		checkAssertionCode(codeIngressoRP, DocumentQueueEnum.DA_LAVORARE_UCB, DocumentQueueEnum.ASSEGNATE);

		final Collection<MetadatoDTO> metadatiRegistroAusiliarioList = getMetadatiRegistro(idRegistro, utente, dtIngressoRP);

		final CollectedParametersDTO c = new CollectedParametersDTO();

		for (final MetadatoDTO m : metadatiRegistroAusiliarioList) {
			c.addStringParam(m.getName(), m.getName());
		}

		final FileDTO contentUscita = new FileDTO("template.pdf", createPdf(idRegistro, c.getStringParams()), MediaType.PDF.toString());

		final DetailDocumentRedDTO detail = predisponiRitiro(utente, masterIngressoRP, contentUscita, idRegistro, metadatiRegistroAusiliarioList);

//		STEP 6
//		BRIAMONTE finalizza la risposta fornendo i dati mancanti: 
//		- iter di firma manuale verso se stesso
//		- tab spedizione
//		- Indice di classificazoine
//		- momento protocollazione

		// Valorizzazione iter di firma
		detail.setIdIterApprovativo(IterApprovativoDTO.ITER_FIRMA_MANUALE);
		final List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneFirma(ID_UFFICIO_DIREZIONE_ULAV, ID_FRANCESCO_BRIAMONTE, "FRANCESCO", "BRIAMONTE"));
		detail.setAssegnazioni(assegnazioni);

		// Valorizzare Tab spedizione
		if (detail.getDestinatari() != null && !detail.getDestinatari().isEmpty()
				&& MezzoSpedizioneEnum.ELETTRONICO.equals(detail.getDestinatari().get(0).getMezzoSpedizioneEnum())) {
			final EmailDTO mailSpedizione = new EmailDTO();
			mailSpedizione.setMittente("testred@pec.mef.gov.it");
			mailSpedizione.setDestinatari("a@b.c");
			mailSpedizione.setOggetto("MEF - RGS - Un numero di protocollo valido - " + detail.getOggetto());
			mailSpedizione.setTesto("Ciao Mondo!");
			detail.setMailSpedizione(mailSpedizione);
		}

		// Valorizzare Indice di classificazione
		detail.setIndiceClassificazioneFascicoloProcedimentale("A");

		// Protocollazione standard
		detail.setMomentoProtocollazioneEnum(MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD);

		// finalizza creazione documento uscita

		final EsitoSalvaDocumentoDTO esitoSalvataggioRitiro = salvaDocumento(detail, utente);

		dumpCreazioneDocumento(esitoSalvataggioRitiro);
		checkAssertion((esitoSalvataggioRitiro.getErrori() == null || esitoSalvataggioRitiro.getErrori().isEmpty()),
				"La creazione della richiesta di ritiro non è andata a buon fine.");

		final String dtUscitaRP = esitoSalvataggioRitiro.getDocumentTitle();
		final String wfUscitaRP = esitoSalvataggioRitiro.getWobNumber();
		final Integer idDocUscitaRP = esitoSalvataggioRitiro.getNumeroDocumento();

		LOGGER.info("#####USCITA : DT > " + dtUscitaRP + " NUM > " + idDocUscitaRP);

		// Dobbiamo analizzare 3 documenti:
		// Ingresso iniziale
		// Ingresso ritiro provvedimento
		// Uscita ritiro provvedimento

		codeIngresso = getQueues(utente.getUsername(), dtIngresso, classeDocRP, idCatDocIngressoRP);
		codeIngressoRP = getQueues(utente.getUsername(), dtIngressoRP, classeDocRP, idCatDocIngressoRP);
		final RecuperoCodeDTO codeUscitaRP = getQueues(utente.getUsername(), dtUscitaRP, classeDocRP, CategoriaDocumentoEnum.USCITA.getIds()[0]);

		checkAssertionCode(codeIngresso, DocumentQueueEnum.ASSEGNATE);
		checkAssertionCode(codeIngressoRP, DocumentQueueEnum.ASSEGNATE);
		checkAssertionCode(codeUscitaRP, DocumentQueueEnum.NSD);

		checkStorico(utente, null, dtIngresso, EventTypeEnum.PREDISPOSIZIONE_RESTITUZIONE);
		checkStorico(utente, null, dtIngressoRP, EventTypeEnum.PREDISPOSIZIONE_RESTITUZIONE);
		checkStorico(utente, null, dtUscitaRP, EventTypeEnum.ASSEGNAZIONE_COMPETENZA);

		final EsitoOperazioneDTO esito = sign(utente, wfUscitaRP);
		// Verifico l'esito della firma.
		checkAssertion(esito.getFlagEsito(), "Errore in fase di firma.");

		// Verifico lo storico dell'ingresso.
		checkStorico(utente, wfUscitaRP, dtUscitaRP, EventTypeEnum.FIRMATO);

		final RecuperoCodeDTO codeUscita = getQueues(utente.getUsername(), dtUscitaRP, classeDocRP, CategoriaDocumentoEnum.USCITA.getIds()[0]);
		checkAssertionCode(codeUscita, DocumentQueueEnum.SPEDIZIONE, DocumentQueueEnum.PROCEDIMENTI);

	}

}
