package it.ibm.business.test.ucb;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import it.ibm.business.test.nps.interfaccianotifiche.InterfacciaNotificheNPS;
import it.ibm.business.test.nps.interfaccianotifiche.InterfacciaNotificheNPSFlusso;
import it.ibm.business.test.nps.interfaccianotifiche.InterfacciaNotificheNPSFlusso_Service;
import it.ibm.business.test.nps.interfaccianotifiche.InterfacciaNotificheNPS_Service;
import it.ibm.business.test.suite.AbstractTest;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoPredisponiDocumentoDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.NotificaAzioneNpsDTO;
import it.ibm.red.business.dto.NotificaNpsDTO;
import it.ibm.red.business.dto.ParamsRicercaEsitiDTO;
import it.ibm.red.business.dto.RecuperoCodeDTO;
import it.ibm.red.business.dto.RisultatiRicercaUcbDTO;
import it.ibm.red.business.dto.TimerRichiesteIntegrazioniDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.StepDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IRicercaAvanzataDocUCBSRV;
import it.ibm.red.business.service.ITimerRichiesteIntegrazioniSRV;
import it.ibm.red.business.service.concrete.IntegrazioneDatiSRV;
import it.ibm.red.business.service.facade.IAttrExtFacadeSRV;
import it.ibm.red.business.service.facade.IConfermaAnnullamentoFacadeSRV;
import it.ibm.red.business.service.facade.INotificaNpsFacadeSRV;
import it.ibm.red.business.service.facade.IPredisponiDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.IRegistroAusiliarioFacadeSRV;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.business.service.facade.IStoricoFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.webservice.model.interfaccianps.dto.ParametersElaboraNotificaAzione;

/**
 * Classe astratta utilizzata per contenere le caratteristiche comuni dei test.
 * 
 * @author CPIERASC
 */

public abstract class AbstractUCBTest extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AbstractUCBTest.class.getName());

	/**
	 * Identificativo registro restituzione.
	 */
	protected static final Integer ID_REGISTRO_RESTITUZIONE = 11;

	/**
	 * Identificativo tipo documento.
	 */
	protected static final Integer ID_TIPO_DOC_RITIRO_PROV = 462;

	/**
	 * Identificativo tipo procedimento.
	 */
	protected static final Integer ID_TIPO_PROC_RITIRO_PROV = 1200;

	/**
	 * Identificativo tipo documento.
	 */
	protected static final Integer ID_TIPO_DOC_ATTI_SOGGETTI_VISTO_ENTRATA = 4714;

	/**
	 * Identificativo tipo procedimento.
	 */
	protected static final Integer ID_TIPO_PROC_ATTI_SOGGETTI_VISTO_ENTRATA = 3564;

	/**
	 * Username utente.
	 */
	protected static final String USERNAME_BUONGIORNO = "M.BUONGIORNO";

	/**
	 * Username utente.
	 */
	protected static final String USERNAME_MINNIELLI = "LUIGI.MINNIELLI";

	/**
	 * Username utente.
	 */
	protected static final String USERNAME_BRIAMONTE = "FRANCESCO.BRIAMONTE";

	/**
	 * Username utente.
	 */
	protected static final String USERNAME_QUINZI = "LORENZO.QUINZI";

	/**
	 * Utente MISE - Alberto Giansanti.
	 */
	protected static final String USERNAME_GIANSANTI = "ALBERTO.GIANSANTI";
	
	/**
	 * Utente MISE - Maurilio Stizzo.
	 */
	protected static final String USERNAME_STIZZO = "MAURILIO.STIZZO";
	
	/**
	 * Username utente.
	 */
	protected static final String USERNAME_ROSCIOLI = "PAOLO.ROSCIOLI";

	/**
	 * Username utente.
	 */
	protected static final String USERNAME_BALDUCCI = "giovanni.balducci";

	/**
	 * Username utente.
	 */
	protected static final String USERNAME_FALCO = "claudio.falco";

	/**
	 * Identificativo utente.
	 */
	protected static final Integer ID_LUIGI_MINNIELLI = 959;

	/**
	 * Identificativo utente.
	 */
	protected static final Integer ID_MARIANNA_BUONGIORNO = 37004;

	/**
	 * Identificativo utente.
	 */
	protected static final Integer ID_FRANCESCO_BRIAMONTE = 37002;

	/**
	 * Identificativo contatto.
	 */
	protected static final Integer ID_CONTATTO_IRENE = 8118;

	/**
	 * Identificativo ufficio.
	 */
	protected static final Integer ID_UFFICIO_DIREZIONE_ULAV = 3203;

	/**
	 * Descrizione ufficio.
	 */
	protected static final String DESC_UFFICIO_DIREZIONE_ULAV = "UFFICIO DI DIREZIONE";

	/**
	 * Identiicativo tipo documento.
	 */
	protected static final Integer ID_TIPO_DOC_VISTO = 402;

	/**
	 * Identificativo tipo procedimento.
	 */
	protected static final Integer ID_TIPO_PROC_VISTO = 2886;

	/**
	 * Identificativo registro visto ex art 6.
	 */
	protected static final Integer ID_REGISTRO_VISTO_EX_ART_6 = 9;

	/**
	 * Identificativo registro ossevazioni ex art 6.
	 */
	protected static final Integer ID_OSSERVAZIONI_EX_ART_6 = 3;

	/**
	 * Identificativo registro richiesta integrazione ex art 14bis comma 2.
	 */
	protected static final Integer ID_RICHIESTA_INTEGRAZIONE_EX_ART_14BIS_COMMA_2 = 5;

	/**
	 * Tipo documento.
	 */
	protected static final Integer ID_TIPO_DOC_JU_E = 424;

	/**
	 * Tipo procedimento.
	 */
	protected static final Integer ID_TIPO_PROC_JU_E = 2890;

	/**
	 * Tipo documento.
	 */
	protected static final Integer ID_TIPO_DOC_JU_U = 425;

	/**
	 * Tipo procedimento.
	 */
	protected static final Integer ID_TIPO_PROC_JU_U = 2891;

	/**
	 * Tipo procedimento.
	 */
	protected static final Integer ID_TIPO_PROC_TEST_GIORNI = 2885;

	/**
	 * Tipo documento.
	 */
	protected static final Integer ID_TIPO_DOC_TEST_MARCO_E = 402;

	/**
	 * Tipo documento.
	 */
	protected static final Integer ID_TIPO_DOC_TEST_MARCO_U = 403;

	/**
	 * Tipo documento.
	 */
	protected static final Integer ID_TIPO_DOC_TESTULAV_SL = 353;

	/**
	 * Tipo procedimento.
	 */
	protected static final Integer ID_TIPO_PROC_REG_MET = 2874;

	/**
	 * Tipo procedimento.
	 */
	protected static final Integer ID_TIPO_PROC_GENERICO_MARCO = 2886;

	/**
	 * Tipo documento.
	 */
	protected static final Integer ID_IGIT_UFFICIO_XI = 2426;

	/**
	 * Descrizione documento.
	 */
	protected static final String DESC_IGIT_UFFICIO_XI = "IGIT UFFICIO XI";

	/**
	 * Tipo documento.
	 */
	protected static final Integer ID_TIPO_DOC_OSSERVAZIONE_123_USCITA_ULAV = 427;

	/**
	 * Tipo procedimento.
	 */
	protected static final Integer ID_TIPO_PROC_OSSERVAZIONE_EX_ART_6_ULAV = 2893;

	/**
	 * Tipo documento.
	 */
	protected static final Integer ID_TIPO_DOC_INTEGRAZIONE_DATI = 438;

	/**
	 * Tipo procedimento.
	 */
	protected static final Integer ID_TIPO_PROC_INTEGRAZIONE_DATI_GENERICO = 1200;

	/**
	 * Id tipo procedimento generico per UCB_LAV.
	 */
	protected static final int TIPOLOGIA_GENERICA_ID_PROC = 1300;

	/**
	 * Id tipologia documento generico per UCB_LAV.
	 */
	protected static final int TIPOLOGIA_GENERICA_ID_DOC = 2600;

	/**
	 * Descrizione tipologia documento generico per UCB_LAV.
	 */
	protected static final String TIPOLOGIA_GENERICA_DESC = "TIPOLOGIA GENERICA";

	/**
	 * Servizio.
	 */
	private IPredisponiDocumentoFacadeSRV predisponiSRV;

	/**
	 * Servizio.
	 */
	private IStoricoFacadeSRV storicoSRV;

	/**
	 * Servizio.
	 */
	private ISignFacadeSRV signSRV;

	/**
	 * Servizio.
	 */
	private IRegistroAusiliarioFacadeSRV regAusSRV;

	/**
	 * Servizio.
	 */
	private IRicercaAvanzataDocUCBSRV ricercaAvUCBSRV;

	/**
	 * Servizio.
	 */
	private IConfermaAnnullamentoFacadeSRV confSRV;

	/**
	 * Servizio.
	 */
	private ITimerRichiesteIntegrazioniSRV timerSRV;

	/**
	 * Servizio.
	 */
	private IntegrazioneDatiSRV integrazioneDatiSRV;

	/**
	 * Servizio.
	 */
	private IAttrExtFacadeSRV attrExtSRV;

	/**
	 * Servizio.
	 */
	private INotificaNpsFacadeSRV notificaNpsSRV;

	/**
	 * @see it.ibm.business.test.suite.AbstractTest#before().
	 */
	@Override
	protected final void before() {
		super.before();
		predisponiSRV = ApplicationContextProvider.getApplicationContext().getBean(IPredisponiDocumentoFacadeSRV.class);
		storicoSRV = ApplicationContextProvider.getApplicationContext().getBean(IStoricoFacadeSRV.class);
		signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignFacadeSRV.class);
		regAusSRV = ApplicationContextProvider.getApplicationContext().getBean(IRegistroAusiliarioFacadeSRV.class);
		ricercaAvUCBSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaAvanzataDocUCBSRV.class);
		confSRV = ApplicationContextProvider.getApplicationContext().getBean(IConfermaAnnullamentoFacadeSRV.class);
		timerSRV = ApplicationContextProvider.getApplicationContext().getBean(ITimerRichiesteIntegrazioniSRV.class);
		integrazioneDatiSRV = ApplicationContextProvider.getApplicationContext().getBean(IntegrazioneDatiSRV.class);
		attrExtSRV = ApplicationContextProvider.getApplicationContext().getBean(IAttrExtFacadeSRV.class);
		notificaNpsSRV = ApplicationContextProvider.getApplicationContext().getBean(INotificaNpsFacadeSRV.class);
	}

	/**
	 * Esegue la predisposizione di una richiesta integrazione dati del tipo
	 * specificato dal {@code idRegistroAusiliario} sul documento {@code master}.
	 * 
	 * @param utente
	 *            Utente operatore che effettua la predisposizione.
	 * @param master
	 *            Master del documento sul quale viene predisposta la richesta di
	 *            integrazione.
	 * @param contentUscita
	 *            Template associato alla richiesta di integrazione dati.
	 * @param idRegistroAusiliario
	 *            Identificativo del registro ausiliario che specifica il tipo di
	 *            richiesta integrazione dati.
	 * @param metadatiRegistroAusiliarioList
	 *            Metadati del registro che identificano i parametri del template e
	 *            del documento di richiesta integrazione dati.
	 * @return Documento in uscita per la predisposizione della richiesta
	 *         integrazione dati.
	 */
	protected DetailDocumentRedDTO predisponiRichiestaIntegrazione(final UtenteDTO utente, final MasterDocumentRedDTO master, final FileDTO contentUscita,
			final Integer idRegistroAusiliario, final Collection<MetadatoDTO> metadatiRegistroAusiliarioList) {
		return predisponiUscita(utente, ResponsesRedEnum.RICHIESTA_INTEGRAZIONI, null, master, contentUscita, idRegistroAusiliario, metadatiRegistroAusiliarioList);
	}

	/**
	 * Esegue la predisposizione di un' osservazione del tipo specificato dal
	 * {@code idRegistroAusiliario} sul documento {@code master}.
	 * 
	 * @param utente
	 *            Utente operatore che effettua la predisposizione.
	 * @param master
	 *            Master del documento sul quale viene predisposta l'osservazione.
	 * @param contentUscita
	 *            Template associato all'osservazione.
	 * @param idRegistroAusiliario
	 *            Identificativo del registro ausiliario che specifica il tipo di
	 *            osservazione da predisporre.
	 * @param metadatiRegistroAusiliarioList
	 *            Metadati del registro che identificano i parametri del template e
	 *            del documento dell'osservazione.
	 * @return Documento in uscita per la predisposizione dell'osservazione.
	 */
	protected DetailDocumentRedDTO predisponiOsservazione(final UtenteDTO utente, final MasterDocumentRedDTO master, final FileDTO contentUscita,
			final Integer idRegistroAusiliario, final Collection<MetadatoDTO> metadatiRegistroAusiliarioList) {
		return predisponiUscita(utente, ResponsesRedEnum.PREDISPONI_OSSERVAZIONE, null, master, contentUscita, idRegistroAusiliario, metadatiRegistroAusiliarioList);
	}

	/**
	 * Esegue la predisposizione di un visto del tipo specificato dal
	 * {@code idRegistroAusiliario} sul documento {@code master}.
	 * 
	 * @param utente
	 *            Utente operatore che effettua la predisposizione del visto.
	 * @param master
	 *            Master del documento sul quale viene predisposta il visto.
	 * @param contentUscita
	 *            Template associato al visto predisposto.
	 * @param idRegistroAusiliario
	 *            Identificativo del registro ausiliario che specifica il tipo di
	 *            visto da predisporre.
	 * @param metadatiRegistroAusiliarioList
	 *            Metadati del registro che identificano i parametri del template e
	 *            del documento del visto.
	 * @return Documento in uscita per la predisposizione del visto.
	 */
	protected DetailDocumentRedDTO predisponiVisto(final UtenteDTO utente, final MasterDocumentRedDTO master, final FileDTO contentUscita, final Integer idRegistroAusiliario,
			final Collection<MetadatoDTO> metadatiRegistroAusiliarioList) {
		return predisponiUscita(utente, ResponsesRedEnum.PREDISPONI_VISTO, null, master, contentUscita, idRegistroAusiliario, metadatiRegistroAusiliarioList);
	}

	/**
	 * Esegue la predisposizione di una restituzione del tipo specificato dal
	 * {@code idRegistroAusiliario} sul documento {@code master}.
	 * 
	 * @param utente
	 *            Utente operatore che effettua la predisposizione della
	 *            restituzione.
	 * @param master
	 *            Master del documento sul quale viene predisposta la restituzione.
	 * @param contentUscita
	 *            Template associato alla restituzione.
	 * @param idRegistroAusiliario
	 *            Identificativo del registro ausiliario che specifica il tipo di
	 *            restituzione da predisporre.
	 * @param metadatiRegistroAusiliarioList
	 *            Metadati del registro che identificano i parametri del template e
	 *            del documento della restituzione.
	 * @return Documento in uscita per la predisposizione della restituzione.
	 */
	protected DetailDocumentRedDTO predisponiRitiro(final UtenteDTO utente, final MasterDocumentRedDTO master, final FileDTO contentUscita, final Integer idRegistroAusiliario,
			final Collection<MetadatoDTO> metadatiRegistroAusiliarioList) {
		return predisponiUscita(utente, ResponsesRedEnum.PREDISPONI_RESTITUZIONE, null, master, contentUscita, idRegistroAusiliario, metadatiRegistroAusiliarioList);
	}

	/**
	 * Effettua la predisposizione di una risposta all'ingresso identificato dal
	 * {@code master}.
	 * 
	 * @param utente
	 *            Utente operatore che effettua la predisposizione.
	 * @param master
	 *            Documento sul quale viene predisposta la risposta.
	 * @return Documento creato in seguito alla predisposizione.
	 */
	protected DetailDocumentRedDTO predisponiRispondiIngresso(final UtenteDTO utente, final MasterDocumentRedDTO master) {
		return predisponiUscita(utente, ResponsesRedEnum.RISPONDI_INGRESSO, null, master, null, null, null);
	}

	/**
	 * Effettua la predisposizione di una inoltro all'ingresso identificato dal
	 * {@code master}.
	 * 
	 * @param utente
	 *            Utente operatore che effettua la predisposizione.
	 * @param master
	 *            Documento sul quale viene predisposto l'inoltro.
	 * @return Documento creato in seguito alla predisposizione.
	 */
	protected DetailDocumentRedDTO predisponiInoltraIngresso(final UtenteDTO utente, final MasterDocumentRedDTO master) {
		return predisponiUscita(utente, ResponsesRedEnum.INOLTRA_INGRESSO, null, master, null, null, null);
	}

	/**
	 * Effettua la predisposizione di un documento a partire dal documento
	 * identificato dal {@code master}.
	 * 
	 * @param utente
	 *            Utente operatore che effettua la predisposizione.
	 * @param master
	 *            Documento sul quale viene predisposto il documento in uscita.
	 * @return Documento creato in seguito alla predisposizione.
	 */
	protected DetailDocumentRedDTO predisponiDocumento(final UtenteDTO utente, final MasterDocumentRedDTO master) {
		return predisponiUscita(utente, ResponsesRedEnum.PREDISPONI_DOCUMENTO, null, master, null, null, null);
	}

	/**
	 * Effettua la predisposizione di un documento a partire dal documento
	 * identificato dal {@code master}.
	 * 
	 * @param utente
	 *            Utente operatore che effettua la predisposizione dell'uscita.
	 * @param responseSollecitata
	 *            Response sollecitata per la predisposizione.
	 * @param masters
	 * @param master
	 * @param contentUscita
	 * @param idRegistroAusiliario
	 *            Identificativo del registro che definisce la predisposizione.
	 * @param metadatiRegistroAusiliarioList
	 *            Metadati del registro ausiliario identificato dall'
	 *            {@code idRegistroAusiliario}.
	 * @return Documento creato nella predisposizione.
	 */
	private DetailDocumentRedDTO predisponiUscita(final UtenteDTO utente, final ResponsesRedEnum responseSollecitata, final List<MasterDocumentRedDTO> masters,
			final MasterDocumentRedDTO master, final FileDTO contentUscita, final Integer idRegistroAusiliario, final Collection<MetadatoDTO> metadatiRegistroAusiliarioList) {
		DetailDocumentRedDTO docPredisposto = null;

		final EsitoPredisponiDocumentoDTO esito = predisponiSRV.predisponiUscita(utente, responseSollecitata, predisponiSRV.trasformaInRisposteAllaccio(masters),
				predisponiSRV.trasformaInRispostaAllaccio(master), contentUscita, idRegistroAusiliario, metadatiRegistroAusiliarioList, new ArrayList<>());
		if (esito.isEsito()) {
			docPredisposto = esito.getDocPredisposto();
		}

		return docPredisposto;
	}

	/**
	 * Restituisce il documento identificato dal {@code dt} e dal {@code idDoc}.
	 * 
	 * @param utente
	 *            Utente operatore della ricerca.
	 * @param dt
	 *            Document title - identificativo del documento.
	 * @param idDoc
	 *            Id Documento.
	 * @return Documento recuperato se esistente, {@code null} altrimenti.
	 */
	protected MasterDocumentRedDTO getDocumentForDT(final UtenteDTO utente, final String dt, final Integer idDoc) {
		final Collection<MasterDocumentRedDTO> masters = ricercaDocumentiPerKey(idDoc + "", 2020, utente);
		MasterDocumentRedDTO master = null;
		for (final MasterDocumentRedDTO m : masters) {
			if (m.getDocumentTitle().contentEquals(dt)) {
				master = m;
				break;
			}
		}
		return master;
	}

	/**
	 * Esegue la creazione di un documento della tipologia specificata dall'
	 * {@code idTipologiaDocumento} e lo restituisce in output.
	 * 
	 * @param utente
	 *            Utente operatore della creazione documento.
	 * @param idTipologiaDocumento
	 *            Identificativo della tipologia documento.
	 * @param idTipologiaProcedimento
	 *            Identificativo della tipologia procedimento.
	 * @return Esito della creazione.
	 */
	protected EsitoSalvaDocumentoDTO createDoc(final UtenteDTO utente, final Integer idTipologiaDocumento, final Integer idTipologiaProcedimento) {
		return createDoc(utente, idTipologiaDocumento, idTipologiaProcedimento, null);
	}

	/**
	 * Esegue la creazione di un documento della tipologia specificata dall'
	 * {@code idTipologiaDocumento} con protocollo di riferimento identificato dal
	 * {@code protocolloRiferimento} e lo restituisce in output.
	 * 
	 * @param utente
	 *            Utente operatore della creazione.
	 * @param idTipologiaDocumento
	 *            Identificativo della tipologia documento.
	 * @param idTipologiaProcedimento
	 *            Identificativo della tipologia procedimento.
	 * @param protocolloRiferimento
	 *            protocollo di riferimento del documento creato.
	 * @return Esito creazione.
	 */
	protected EsitoSalvaDocumentoDTO createDoc(final UtenteDTO utente, final Integer idTipologiaDocumento, final Integer idTipologiaProcedimento,
			final String protocolloRiferimento) {
		final Boolean daNonProtocollare = false;

		final Integer idMezzoRicezione = ID_MEZZO_RICEZIONE_POSTA_ELET_DA_PEC_PER_ELETTRONICI;

		final List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneCompetenzaUfficio(ID_UFFICIO_DIREZIONE_ULAV, DESC_UFFICIO_DIREZIONE_ULAV));

		assegnazioni.add(createAssegnazioneConoscenzaUtente(ID_UFFICIO_DIREZIONE_ULAV, ID_MARIANNA_BUONGIORNO, "MARIANNA", "BUONGIORNO"));
		final String indiceFascicoloProcedimentale = null;

		final String oggetto = createObj();

		final Boolean riservato = false;
		final List<AllegatoDTO> allegati = null;
		return createDocIngresso(allegati, oggetto, utente, ID_CONTATTO_IRENE, FormatoDocumentoEnum.ELETTRONICO, idTipologiaDocumento, idTipologiaProcedimento, assegnazioni,
				daNonProtocollare, idMezzoRicezione, indiceFascicoloProcedimentale, riservato, protocolloRiferimento);
	}

	/**
	 * Esegue la creazione di un documento della tipologia specificata dall'
	 * {@code idTipologiaDocumento} e lo assegna in compenteza ad un ufficio RGS
	 * identificato dal: {@link AbstractTest#ID_IGB_UFFICIO_I}.
	 * 
	 * @param utente
	 *            Utente operatore della creazione documento.
	 * @param idTipologiaDocumento
	 *            Identificativo della tipologia documento.
	 * @param idTipologiaProcedimento
	 *            Identificativo della tipologia procedimento.
	 * @return Esito creazione.
	 */
	protected EsitoSalvaDocumentoDTO createDocRGS(final UtenteDTO utente, final Integer idTipologiaDocumento, final Integer idTipologiaProcedimento) {
		final Boolean daNonProtocollare = false;

		final Integer idMezzoRicezione = ID_MEZZO_RICEZIONE_POSTA_ELET_DA_PEC_PER_ELETTRONICI;

		final List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneCompetenzaUfficio(ID_IGB_UFFICIO_I, DESC_IGB_UFFICIO_I));

		final String indiceFascicoloProcedimentale = null;

		final String oggetto = createObj();

		final Boolean riservato = false;
		final List<AllegatoDTO> allegati = null;
		return createDocIngresso(allegati, oggetto, utente, ID_CONTATTO_IRENE, FormatoDocumentoEnum.ELETTRONICO, idTipologiaDocumento, idTipologiaProcedimento, assegnazioni,
				daNonProtocollare, idMezzoRicezione, indiceFascicoloProcedimentale, riservato);
	}

	/**
	 * Restituisce lo storico visuale del documento identificato dal {@code wobn}.
	 * 
	 * @param utente
	 *            Utente operatore che recupera lo storico.
	 * @param wobn
	 *            Identificativo del processo.
	 * @param dt
	 *            Identificativo del documento.
	 * @return Step per la costruzione dello storico visuale.
	 */
	protected List<StepDTO> getStoricoStep(final UtenteDTO utente, final String wobn, final String dt) {
		return storicoSRV.getModelloStoricoVisuale(utente.getFcDTO(), wobn, dt, utente.getIdAoo(), null, false);
	}

	/**
	 * Esegue la firma remota sul documento identificato dal {@code wobn}
	 * utilizzando un <em> pin </em> ed un <em> otp </em> mockati.
	 * 
	 * @param utente
	 *            Utente firmatario.
	 * @param wobn
	 *            Identificativo del documento da firmare.
	 * @return Esito della firma.
	 */
	protected EsitoOperazioneDTO sign(final UtenteDTO utente, final String wobn) {
		final String pin = "1234";
		final String otp = "5678";
		final Collection<String> wobNumbers = new ArrayList<>();
		wobNumbers.add(wobn);
		return signSRV.firmaRemota("test" + UUID.randomUUID().toString(), SignTypeEnum.PADES_VISIBLE, wobNumbers, utente, otp, pin, false).iterator().next();
	}

	/**
	 * Effettua un controllo sugli step e lancia un'eccezione se non tutti gli
	 * eventi necessari sono presenti.
	 * 
	 * @param utente
	 *            Utente in sessione.
	 * @param wobn
	 *            Wob number identificativo del documento.
	 * @param dt
	 *            Document title identificativo del documento.
	 * @param eventiNecessari
	 *            Eventi necessari che devono essere presenti.
	 */
	protected void checkStorico(final UtenteDTO utente, final String wobn, final String dt, final EventTypeEnum... eventiNecessari) {
		for (final EventTypeEnum e : eventiNecessari) {
			LOGGER.info("Eventi necessari: " + e);
		}

		final List<StepDTO> steps = getStoricoStep(utente, wobn, dt);

		for (final StepDTO s : steps) {
			LOGGER.info("Eventi trovati: " + EventTypeEnum.get(new Integer(s.getHeader().getEventType())));
		}

		Boolean bCheck = true;
		for (final EventTypeEnum e : eventiNecessari) {
			boolean bFlag = false;
			for (final StepDTO s : steps) {
				if (("" + e.getIntValue()).equals(s.getHeader().getEventType())) {
					bFlag = true;
					break;
				}
			}
			if (!bFlag) {
				bCheck = false;
				break;
			}
		}

		checkAssertion(bCheck, "Non tutti gli step nello storico sono presenti.");
	}

	/**
	 * @see it.ibm.business.test.suite.AbstractTest#checkAssertionCode(it.ibm.red.business.dto.RecuperoCodeDTO,
	 *      it.ibm.red.business.enums.DocumentQueueEnum[]).
	 */
	@Override
	protected void checkAssertionCode(final RecuperoCodeDTO codeAttuali, final DocumentQueueEnum... codeAccettate) {

		for (final DocumentQueueEnum c : codeAccettate) {
			LOGGER.info("Coda Accettata: " + c);
		}

		for (final String c : codeAttuali.getCodeNonCensite()) {
			LOGGER.info("Coda non censita: " + c);
		}

		for (final DocumentQueueEnum c : codeAttuali.getCode()) {
			LOGGER.info("Coda individuata: " + c);
		}

		final Collection<String> codeNonCensite = codeAttuali.getCodeNonCensite();
		final Collection<DocumentQueueEnum> codeNonAccettate = new ArrayList<>();

		for (final DocumentQueueEnum codaAttuale : codeAttuali.getCode()) {
			boolean bFound = false;
			for (final DocumentQueueEnum codaAccettata : codeAccettate) {
				if (codaAccettata.equals(codaAttuale)) {
					bFound = true;
					break;
				}
			}
			if (!bFound) {
				codeNonAccettate.add(codaAttuale);
			}
		}

		checkAssertion(codeNonCensite.isEmpty(), "Il documento è presente in code non censite.");
		checkAssertion(codeNonAccettate.isEmpty(), "Il documento deve essere presente solamente nelle code accettate.");
		checkAssertion(codeAttuali.getCode().size() == codeAccettate.length, "Il documento deve essere presente in tutte le code accettate.");
	}

	/**
	 * Crea il tepmlate associato al registro identificato da {@code idRegistro} non
	 * definitivo quindi con sfondo colorato in background ai parametri della
	 * registrazione ausiliaria.
	 * 
	 * @param idRegistro
	 *            Identificativo del registro ausiliario.
	 * @param stringParams
	 *            Parametri del template.
	 * @return Byte array del pdf <em> fake </em> generato.
	 */
	protected byte[] createPdf(final Integer idRegistro, final Map<String, String> stringParams) {
		return regAusSRV.createFakePdf(idRegistro, stringParams);
	}

	/**
	 * Restituisce i metadati associati al registro identificato dal
	 * {@code idRegistro} inizianizzandoli con le informazioni del documento
	 * identificato dal {@code documentTitle}.
	 * 
	 * @param idRegistro
	 *            Identificativo del registro.
	 * @param utente
	 *            Utente in sessione.
	 * @param documentTitle
	 *            Identificativo del documento.
	 * @return Collezione dei metadati per il popolamento del template.
	 */
	protected Collection<MetadatoDTO> getMetadatiRegistro(final Integer idRegistro, final UtenteDTO utente, final String documentTitle) {
		return regAusSRV.getMetadatiRegistro(idRegistro, utente, documentTitle);
	}

	/**
	 * Esegue la ricerca esiti Uffici centrale bilancio.
	 * 
	 * @param utente
	 *            Utente in sessione.
	 * @param esito
	 *            Sottocategoria del documento.
	 * @return Documenti ottenuti dalla ricerca.
	 */
	protected Collection<MasterDocumentRedDTO> ricercaEsiti(final UtenteDTO utente, final SottoCategoriaDocumentoUscitaEnum esito) {
		final ParamsRicercaEsitiDTO paramsRicerca = new ParamsRicercaEsitiDTO();
		final Date today = DateUtils.dropTimeInfo(new Date());
		paramsRicerca.setDataCreazioneDa(today);
		paramsRicerca.setTipoEsito(esito);
		final RisultatiRicercaUcbDTO out = ricercaAvUCBSRV.eseguiRicercaEsitiUCB(paramsRicerca, utente);
		return out.getMasters();
	}

	/**
	 * Esegue la conferma dell'annullamento per il documento identificato dal
	 * {@code wobNumber}.
	 * 
	 * @param utente
	 *            Utente in sessione.
	 * @param wobNumber
	 *            Identificativo del documento in fase di annullamento.
	 * @return Esito dell'operazione.
	 */
	protected EsitoOperazioneDTO confermaAnnullamento(final UtenteDTO utente, final String wobNumber) {
		return confSRV.confermaAnnullamento(utente, wobNumber);
	}

	/**
	 * Simula l'invio della mail e la scadenza.
	 * 
	 * @param dtIngresso
	 *            Document title del documento in ingresso.
	 * @param dtUscita
	 *            Document title del documento in uscita.
	 * @param utente
	 *            Utente in sessione.
	 */
	protected void simulaInvioMailEFaiScadere(final String dtIngresso, final String dtUscita, final UtenteDTO utente) {
		Connection conn = createNewDBConnection();
		final MessaggioEmailDTO msg = new MessaggioEmailDTO();
		msg.setIdDocumento(dtUscita);

		timerSRV.registraDataInvio(msg.getIdDocumento(), utente.getIdAoo(), FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo()), conn);
		commitConnection(conn);
		PreparedStatement ps = null;
		try {
			final String querySQL = "UPDATE TIMER_RICHIESTA_CHIARIMENTI SET DATA_ULTIMA_ELABORAZIONE = ?, DATAINVIO = ? WHERE IDDOCUMENTO = ?";
			conn = createNewDBConnection();
			ps = conn.prepareStatement(querySQL);
			final java.sql.Date longTimeAgo = new java.sql.Date(new SimpleDateFormat("dd/MM/yyyy").parse("01/01/1900").getTime());
			ps.setDate(1, longTimeAgo);
			ps.setDate(2, longTimeAgo);
			ps.setString(3, dtIngresso);
			final Integer updatedRows = ps.executeUpdate();
			checkAssertion(updatedRows == 1, "Deve essere presente un'unica richiesta chiarimenti per il document title indicato.");
			commitConnection(conn);
		} catch (final Exception e) {
			rollbackConnection(conn);
			LOGGER.error(e);
		} finally {
			closeOpenedStatement(ps);
		}

	}

	/**
	 * Gestisce la chiusura dello statement.
	 * 
	 * @param ps
	 */
	private static void closeOpenedStatement(final PreparedStatement ps) {
		try {
			if (ps != null) {
				ps.close();
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la chisura dello statement.", e);
		}
	}

	/**
	 * Rimette in lavorazione il documento identificato dal {@code dtEntrata}.
	 * 
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param dtEntrata
	 *            Identificativo del documento in entrata.
	 */
	protected void rimettiInLavorazione(final Long idAoo, final String dtEntrata) {
		final Collection<TimerRichiesteIntegrazioniDTO> items = timerSRV.getNextDaLavorare();

		for (final TimerRichiesteIntegrazioniDTO item : items) {
			checkAssertion(dtEntrata.equals(item.getIdDocumento() + ""), "Il documento da lavorare non è quello indicato.");
			timerSRV.elaboraItem(item, idAoo);
		}

	}

	/**
	 * Esegue la validazione dell'integrazione dati restituendone l'esito.
	 * 
	 * @param utente
	 *            Utente in sessione.
	 * @param wobNumber
	 *            Identificativo del documento.
	 * @return Esito operazione.
	 */
	protected EsitoOperazioneDTO validaIntegrazioneDati(final UtenteDTO utente, final String wobNumber) {
		return integrazioneDatiSRV.validaIntegrazioneDati(utente, wobNumber);
	}

	/**
	 * Esegue l'associazione dell'integrazione dati identificata dall'
	 * {@code idDocumentoIntegrazioneDati} sul documento identificato dal
	 * {@code wobNumber}.
	 * 
	 * @param utente
	 *            Utente operatore che effettua l'associazione.
	 * @param wobNumber
	 *            Identificativo documento a cui viene associata l'integrazione dati.
	 * @param idDocumentoIntegrazioneDati
	 *            Identificativo integrazione dati da associare.
	 * @return Esito dell'associazione integrazione dati.
	 */
	protected EsitoOperazioneDTO associaIntegrazioneDati(final UtenteDTO utente, final String wobNumber, final String idDocumentoIntegrazioneDati) {
		return integrazioneDatiSRV.associaIntegrazioneDati(utente, wobNumber, idDocumentoIntegrazioneDati);
	}

	/**
	 * Recupera l'integrazione dati per l'associazione.
	 * 
	 * @param utente
	 *            Utente in sessione.
	 * @return Documentiin ingresso di tipo Integrazione Dati che insistono sulla
	 *         coda Corriere o Da Lavorare dell' {@code utente}.
	 */
	protected Collection<MasterDocumentRedDTO> getIntegrazioneDati4Associa(final UtenteDTO utente) {
		return integrazioneDatiSRV.getIntegrazioneDati4Associa(utente);
	}

	/**
	 * Esegue l'export sample per la coppia tipologia documento, tipologia
	 * procedimento identificate dagli id.
	 * 
	 * @param utente
	 *            Utente in sessione.
	 * @param idTipoDocumento
	 *            Identificativo del tipo documento.
	 * @param idTipoProcedimento
	 *            Identificativo del tipo procedimento.
	 * @return Sample per l'esportazione.
	 */
	protected String exportSample(final UtenteDTO utente, final Integer idTipoDocumento, final Integer idTipoProcedimento) {
		return attrExtSRV.exportSample(utente, idTipoDocumento, idTipoProcedimento);
	}

	/**
	 * Esegue l'accodamento della notifica azione utilizzando la {@code richiesta}.
	 * 
	 * @param richiesta
	 *            Request per l'accodamento della notifica azione.
	 * @return Identificativo.
	 */
	protected Long accodaNotificaAzione(final ParametersElaboraNotificaAzione richiesta) {
		return notificaNpsSRV.accodaNotificaAzione(richiesta, 0L);
	}

	/**
	 * Restituisce la richiesta da elaborare identificata dall' {@code idCoda} in
	 * input aggiornandone lo stato IN LAVORAZIONE.
	 * 
	 * @param idCoda
	 *            Identificativo della coda.
	 * @return Notifica di richiesta elaborazione.
	 */
	protected NotificaNpsDTO recuperaRichiestaElab(final int idCoda) {
		return notificaNpsSRV.recuperaRichiestaElab(idCoda);
	}

	/**
	 * Esegue l'elaborazione della notifica azione.
	 * 
	 * @param item
	 *            Notifica azione da elaborare.
	 * @return String.
	 */
	protected String elaboraAzione(final NotificaAzioneNpsDTO item) {
		return notificaNpsSRV.elaboraNotificaAzione(item);
	}
	
	/**
	 * Crea l'interfaccia notifiche per il client NPS.
	 * 
	 * @param endpointNotifiche
	 *            Endpoint per l'interfaccia.
	 * @return Interfaccia creata.
	 */
	protected InterfacciaNotificheNPS createInterfacciaNotificheNPSClient(final String endpointNotifiche) {
		
		try {
			final InterfacciaNotificheNPS_Service service = new InterfacciaNotificheNPS_Service(new URL(endpointNotifiche));
			return service.getInterfacciaNotificheNPSSOAPPort();
		} catch (MalformedURLException e) {
			LOGGER.error("Errore in fase di inizializzazione dell'interfaccia notifiche NPS", e);
			return null;
		}
		
	}

	/**
	 * Crea l'interfaccia notifiche flusso per il client NPS.
	 * 
	 * @param endpointNotificheFlusso
	 *            Endpoint per l'interfaccia.
	 * @return Interfaccia creata.
	 */
	protected InterfacciaNotificheNPSFlusso createInterfacciaNotificheNPSFlussoClient(final String endpointNotificheFlusso) {
		
		try {
			final InterfacciaNotificheNPSFlusso_Service service = new InterfacciaNotificheNPSFlusso_Service(new URL(endpointNotificheFlusso));
			return service.getInterfacciaNotificheFlussoNPSSOAPPORT();
		} catch (MalformedURLException e) {
			LOGGER.error("Errore in fase di inizializzazione dell'interfaccia notifiche flusso NPS", e);
			return null;
		}
		
	}

	/**
	 * Crea l'accesso applicativo e lo restituisce.
	 * 
	 * @return Accesso applicativo.
	 */
	protected it.ibm.red.webservice.model.interfaccianps.headeraccessoapplicativo.AccessoApplicativo createAccessoApplicativo(){
		it.ibm.red.webservice.model.interfaccianps.headeraccessoapplicativo.ObjectFactory ofHeader = new it.ibm.red.webservice.model.interfaccianps.headeraccessoapplicativo.ObjectFactory();
		it.ibm.red.webservice.model.interfaccianps.headeraccessoapplicativo.AccessoApplicativo accessoApplicativo = ofHeader.createAccessoApplicativo();
		accessoApplicativo.setActor(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NPS_ACTOR_NPSKEY));
		accessoApplicativo.setApplicazione(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NPS_APPLICATION_NPSKEY));
		accessoApplicativo.setClient(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NPS_CLIENT_NPSKEY));
		accessoApplicativo.setMessageId(java.util.UUID.randomUUID().toString());
		accessoApplicativo.setMustUnderstand(true);
		accessoApplicativo.setPassword(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NPS_PWD_NPSKEY));
		accessoApplicativo.setServizio(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NPS_SERVICE_NPSKEY));
		accessoApplicativo.setUtente(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NPS_USER_NPSKEY));
		
		return accessoApplicativo;
	}
	
}