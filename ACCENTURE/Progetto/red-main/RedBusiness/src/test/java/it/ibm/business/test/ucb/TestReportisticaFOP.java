package it.ibm.business.test.ucb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.ParamsRicercaContiConsuntiviDTO;
import it.ibm.red.business.dto.ParamsRicercaElencoDivisionaleDTO;
import it.ibm.red.business.dto.RisultatoRicercaContiConsuntiviDTO;
import it.ibm.red.business.dto.RisultatoRicercaElencoDivisionaleDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.fop.FOPHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IContiConsuntiviFacadeSRV;
import it.ibm.red.business.service.IElencoDivisionaleFacadeSRV;
import it.ibm.red.business.utils.FileUtils;

/**
 * @author s.lungarella.ibm
 * Classe di test creazione report pdf.
 */
public class TestReportisticaFOP extends AbstractUCBTest {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TestReportisticaFOP.class);

	/**
	 * Path folder dei file di test.
	 */
	private static final String PATH = "";
	
	/**
	 * Filename dell'xml per la generazione dell'elenco divisionale.
	 */
	private static final String XML_ELENCODIVISIONALE_FILENAME = "test.xml";
	
	/**
	 * Filename dell'xml per la generazione dell'elenco notifiche e conti consuntivi.
	 */
	private static final String XML_ELENCONOTIFICHE_FILENAME = "testNotifiche.xml";
	
	/**
	 * Filename del pdf generato associato all'elenco divisionale.
	 */
	private static final String PDF_ELENCODIVISIONALE_FILENAME = "test.pdf";
	
	/**
	 * Filename del pdf generato associato all'elenco notifiche e conti consuntivi.
	 */
	private static final String PDF_ELENCONOTIFICHE_FILENAME = "testNotifiche.pdf";

	/**
	 * Errore generico elenco divisionale.
	 */
	private static final String ERROR_ELENCODIVISIONALE = "Errore riscontrato durante la generazione del report dell'elenco divisionale.";
	
	/**
	 * Errore generico conti consuntivi.
	 */
	private static final String ERROR_CONTICONSUNTIVI = "Errore riscontrato durante la generazione del report dei conti consuntivi.";
	
	/**
	 * Service per la creazione dell'elenco divisionale.
	 */
	private IElencoDivisionaleFacadeSRV elencoDivisionaleSRV;
	
	/**
	 * Service per la creazione del report associato alla ricerca dei conti consuntivi.
	 */
	private IContiConsuntiviFacadeSRV contiConsuntiviSRV;
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		elencoDivisionaleSRV = ApplicationContextProvider.getApplicationContext().getBean(IElencoDivisionaleFacadeSRV.class);
		contiConsuntiviSRV  = ApplicationContextProvider.getApplicationContext().getBean(IContiConsuntiviFacadeSRV.class);
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Genera il pdf dell'elenco divisionale utilizzando l'xslt nelle risorse e l'xml sul filesystem.
	 * Occorre fornire un file xml contenente tutti i parametri per il popolamento dell'xslt
	 * in accordo con gli xpath che vengono usati nel template.
	 */
	@Test
	public void generateElencoDivisionaleFromXml() {
		byte[] pdfFile = null;
		
		// Recupero dell'xslt specifico per la generazione del report
		byte[] xslt = FileUtils.getFileFromInternalResources("report/elencoDivisionale.xslt");
		byte[] xml = FileUtils.getFileFromFS(PATH + XML_ELENCODIVISIONALE_FILENAME);
		try {
			// Trasformazione in pdf
			pdfFile = FOPHelper.transformToPdf(xml, xslt);
		} catch (Exception ex) {
			LOGGER.error(ERROR_ELENCODIVISIONALE, ex);
			throw new RedException(ERROR_ELENCODIVISIONALE, ex);
		}
		
		FileUtils.saveToFile(pdfFile, PATH + PDF_ELENCODIVISIONALE_FILENAME);
		
	}

	/**
	 * Genera il pdf dell'elenco divisionale a partire dai dto dei parametri e dei risultati di ricerca.
	 * Utilizza il service @see ElencoDivisionaleSRV.
	 */
	@Test
	public void generateElencoDivisionaleFromParams() {
		byte[] pdfFile = null;
		try {
			
			// Definizione Params
			ParamsRicercaElencoDivisionaleDTO paramsRicerca = new ParamsRicercaElencoDivisionaleDTO();
			paramsRicerca.setDataAssegnazione(new Date());
			paramsRicerca.setTipologiaDocumento("GENERICO");
			paramsRicerca.setUfficioAssegnante(new UfficioDTO(123L, "Descrizione ufficio"));
			paramsRicerca.setUfficioAssegnatario(new UfficioDTO(234L, "Descrizione ufficio assegnatario"));
			
			List<UtenteDTO> utentiAssegnatari = new ArrayList<>();
			
			// Utente assegnatario 1
			UtenteDTO utenteAss = new UtenteDTO();
			utenteAss.setId(345L);
			utenteAss.setNome("Luigi");
			utenteAss.setCognome("Minnielli");
			utentiAssegnatari.add(utenteAss);
			
			// Utente assegnatario 1
			UtenteDTO utenteAssegnatario = new UtenteDTO();
			utenteAssegnatario.setId(346L);
			utenteAssegnatario.setNome("Giovanni");
			utenteAssegnatario.setCognome("Rossi");
			utentiAssegnatari.add(utenteAssegnatario);
			
			paramsRicerca.setUtentiAssegnatari(utentiAssegnatari);
			
			// Definizione results
			List<RisultatoRicercaElencoDivisionaleDTO> results = new ArrayList<>();
			RisultatoRicercaElencoDivisionaleDTO first = new RisultatoRicercaElencoDivisionaleDTO();
			first.setAnnoElenco(2021);
			first.setDataProtocollo(new Date());
			first.setDescrizioneTitolario("Titolario A");
			first.setNumeroElenco(441);
			first.setOggetto("Test Elenco divisione");
			first.setTipologiaDocumento("Generico");
			first.setDescrizioneMittente("Ufficio XI - Luigi Minnielli");
			first.setUtenteAssegnatario("Test assegnatario");
			results.add(first);
			
			RisultatoRicercaElencoDivisionaleDTO second = new RisultatoRicercaElencoDivisionaleDTO();
			second.setAnnoElenco(2020);
			second.setDataProtocollo(new Date());
			second.setDescrizioneTitolario("Titolario B");
			second.setNumeroElenco(442);
			second.setOggetto("Test Elenco divisione");
			second.setTipologiaDocumento("Generico");
			second.setDescrizioneMittente("Corte dei conti");
			second.setUtenteAssegnatario("Test assegnatario 2");
			results.add(second);
			
			// Definizione descrizione aoo
			String descrizioneAoo = "Descrizione dell'AOO";
			
			// Trasformazione in pdf
			pdfFile = elencoDivisionaleSRV.generaPdfReport(paramsRicerca, results, descrizioneAoo);
		} catch (Exception ex) {
			LOGGER.error(ERROR_ELENCODIVISIONALE, ex);
			throw new RedException(ERROR_ELENCODIVISIONALE, ex);
		}
		
		FileUtils.saveToFile(pdfFile, PATH + PDF_ELENCODIVISIONALE_FILENAME);
	}
	
	/**
	 * Genera il pdf dell'"elenco Notifiche e Conti Consuntivi" utilizzando l'xslt nelle risorse e l'xml sul filesystem.
	 * Occorre fornire un file xml contenente tutti i parametri per il popolamento dell'xslt
	 * in accordo con gli xpath che vengono usati nel template.
	 */
	@Test
	public void generateElencoNotificheFromXml() {
		byte[] pdfFile = null;
		
		// Recupero dell'xslt specifico per la generazione del report
		byte[] xslt = FileUtils.getFileFromInternalResources("report/contiConsuntivi.xslt");
		byte[] xml = FileUtils.getFileFromFS(PATH + XML_ELENCONOTIFICHE_FILENAME);
		try {
			// Trasformazione in pdf
			pdfFile = FOPHelper.transformToPdf(xml, xslt);
		} catch (Exception ex) {
			LOGGER.error(ERROR_ELENCODIVISIONALE, ex);
			throw new RedException(ERROR_ELENCODIVISIONALE, ex);
		}
		
		FileUtils.saveToFile(pdfFile, PATH + PDF_ELENCONOTIFICHE_FILENAME);
		
	}
	
	/**
	 * Genera il pdf dei conti consuntivi a partire dai dto dei parametri e dei risultati di ricerca.
	 * Utilizza il service @see ContiConsuntiviSRV.
	 */
	@Test
	public void generateContiConsuntiviFromParams() {
		byte [] pdfGenerato = null;
		try {

			// Popolazione parametri di ricerca
			ParamsRicercaContiConsuntiviDTO parametriRicerca = new ParamsRicercaContiConsuntiviDTO();
			parametriRicerca.setEsercizio(2020);
			parametriRicerca.setSedeEstera("Svizzera");

			// Popolazione risultati
			List<RisultatoRicercaContiConsuntiviDTO> risultatiRicerca = new ArrayList<>();
			for (int i = 1; i<5; i++) {
				RisultatoRicercaContiConsuntiviDTO firstResult = new RisultatoRicercaContiConsuntiviDTO();
				firstResult.setCodiceSedEstera("CH");
				firstResult.setControlloUcb("SI");
				firstResult.setDataChiusura(null);
				firstResult.setDataDocumento(new Date());
				firstResult.setDataNotifica(new Date());
				firstResult.setDescrizioneAssegnatario(null);
				firstResult.setDescrizioneSedeEstera("Svizzera");
				firstResult.setEsercizio(2021);
				firstResult.setNumeroDocumento(1);
				firstResult.setNumeroNotifica(12);
				firstResult.setStato(null);
				risultatiRicerca.add(firstResult);
			}
			
			for (int i = 1; i<5; i++) {
				RisultatoRicercaContiConsuntiviDTO secondResult = new RisultatoRicercaContiConsuntiviDTO();
				secondResult.setCodiceSedEstera("FR");
				secondResult.setControlloUcb("NO");
				secondResult.setDataDocumento(new Date());
				secondResult.setDescrizioneAssegnatario("Luigi Minnielli");
				secondResult.setDescrizioneSedeEstera("Francia");
				secondResult.setEsercizio(2020);
				secondResult.setNumeroDocumento(2);
				secondResult.setNumeroNotifica(13);
				risultatiRicerca.add(secondResult);
			}
			
			// Popolazione parametri utente
			UtenteDTO utente = new UtenteDTO();
			utente.setAooDesc("Descrizione AOO");
			utente.setDescrizioneEnte("UCB");
			
			pdfGenerato = contiConsuntiviSRV.generaPdfReport(parametriRicerca, risultatiRicerca, utente);
		} catch (Exception e){
			LOGGER.error(ERROR_CONTICONSUNTIVI, e);
			throw new RedException(ERROR_CONTICONSUNTIVI, e);
		}
		
		FileUtils.saveToFile(pdfGenerato, PATH + PDF_ELENCONOTIFICHE_FILENAME);
	}
	
}


