package it.ibm.business.test.ucb;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.TextMarginFinder;

import it.ibm.red.business.helper.adobe.AdobeLCHelper;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.utils.FileUtils;

/**
 * Classe di test Warm Up.
 */
public class TestWarmup extends AbstractUCBTest {

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

    /**
     * Aggiunge margin  rectangle.
     * @param src
     * @param dest
     * @throws IOException
     * @throws DocumentException
     */
    public void addMarginRectangle(final String src, final String dest) throws IOException, DocumentException {
	    final PdfReader reader = new PdfReader(src);
	    final PdfReaderContentParser parser = new PdfReaderContentParser(reader);
	    final PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
	    TextMarginFinder finder;
	    for (int i = 1; i <= reader.getNumberOfPages(); i++) {
	        finder = parser.processContent(i, new TextMarginFinder());
	        final PdfContentByte cb = stamper.getOverContent(i);
	        cb.rectangle(finder.getLlx(), finder.getLly(),
	            finder.getWidth(), finder.getHeight());
	        cb.stroke();
	    }
	    stamper.close();
	    reader.close();
	}

    /**
     * Test posizione PDF.
     */
	@Test
	public static final void testPositionPDF() {
		final byte[] content = FileUtils.getFileFromFS("C:\\Users\\CristianoPierascenzi\\Downloads\\campo firma df.pdf");
		
		final byte [] newContent = PdfHelper.inserisciCampiFirma(content, Arrays.asList(64), 59, 66, false, true);
		FileUtils.saveToFile(newContent, "C:\\HOME\\EXAMPLE_RESOURCES\\pdf_test_apposizione_cf\\" + new Date().getTime() + ".pdf");
		
	}

	/**
	 * Test sulla velina del documento.
	 */
	@Test
	public static final void testVelina() {
		final StringBuilder velinaHTML = new StringBuilder("<html>\r\n") 
				.append("<head>\r\n") 
				.append("<title>Documento</title>\r\n") 
				.append("<style type=\"text/css\">\r\n") 
				.append("\r\n") 
				.append("@page {\r\n") 
				.append("    size: 210mm 500mm\r\n") 
				.append("}\r\n\r\n") 
				.append(".docsubtitle { \r\n") 
				.append(" text-align: left;\r\nfont-family: sans-serif; \r\n") 
				.append(" font-size: 18px;\r\nfont-weight: bold;\r\n") 
				.append(" font-style: italic;\r\n") 
				.append(" color: black;\r\n}\r\n") 
				.append("\r\n") 
				.append(".scriptaSection{\r\n") 
				.append(" color: #0F3B82;\r\n") 
				.append("    font-family: sans-serif; \r\n") 
				.append("    font-size: 18px;\r\n") 
				.append("    font-style: normal;\r\n    font-weight: normal;\r\n") 
				.append("}\r\n.scripta{\r\n font-family: sans-serif; \r\n") 
				.append(" font-size: 12px;\r\n") 
				.append("    font-style: normal;\r\n") 
				.append("}\r\n.scriptaLabelBold{\r\n") 
				.append(" font-family: sans-serif; \r\n") 
				.append(" font-size: 12px;\r\n") 
				.append("    font-style: italic;\r\n") 
				.append(" font-weight: bold;\r\n") 
				.append(" color: black;\r\n}\r\n") 
				.append(".scriptaBold{\r\n") 
				.append(" font-family: sans-serif; \r\n") 
				.append(" font-size: 14px;\r\n") 
				.append("    font-style: normal;\r\n") 
				.append(" font-weight: bold;\r\n")
				.append("}\r\n.msgQueue {\r\n") 
				.append("    background-color: #F5FAFF;\r\n") 
				.append("    border-bottom: 1px solid #EEF0FF;\r\n") 
				.append("    height: 26px;\r\n") 
				.append("}\r\n.z-tabs-scroll .z-tabs-space {\r\n    background: none repeat scroll 0 0 transparent;\r\n") 
				.append("    border: 0 none;\r\n") 
				.append("    height: auto;\r\n") 
				.append("}\r\n.z-tabs .z-tabs-cnt {\r\n")
				.append("    background: none repeat scroll 0 0 transparent;\r\n") 
				.append("    border-bottom: 1px solid #7EAAC6;\r\n    display: block;\r\n")
				.append("    list-style: none outside none;\r\n    margin: 0;\r\n")
				.append("    padding-left: 5px;\r\n")
				.append("    \r\n}\r\n") 
				.append(".z-tabs, .z-tabs-ver {\r\n")
				.append("    background: none repeat scroll 0 0 transparent;\r\n")
				.append("    border: 0 none;\r\n    margin: 0;\r\n") 
				.append("    overflow: hidden;\r\n") 
				.append("    padding: 0;\r\n    position: relative;\r\n")
				.append("}\r\n.z-tabs-scroll {\r\n") 
				.append("    background: none repeat scroll 0 0 #C7E3F3;\r\n") 
				.append("    border: 1px solid #7EAAC6;\r\n") 
				.append("}\r\n.z-tabs-header {\r\n") 
				.append("    margin: 0;\r\n    overflow: hidden;\r\n") 
				.append("    position: relative;\r\n") 
				.append("    width: 100%;\r\n}\r\n") 
				.append(".z-tabs-header .z-clear {\r\n") 
				.append("    height: 0;\r\n") 
				.append("}\r\n.z-tabs-scroll .z-tabs-cnt {\r\n") 
				.append("    -moz-user-select: none;\r\n") 
				.append("    border-bottom: 1px solid #7EAAC6;\r\n") 
				.append("    display: block;\r\n") 
				.append("    list-style: none outside none;\r\n") 
				.append("    margin: 0;\r\n") 
				.append("    padding-left: 5px;\r\n") 
				.append("    padding-top: 1px;\r\n") 
				.append(" height: 25px;\r\n") 
				.append("}\r\n.z-tabs-cnt li {\r\n") 
				.append("    -moz-user-select: none;\r\n") 
				.append("    cursor: default;\r\n") 
				.append("    display: block;\r\n") 
				.append("    float: left;\r\n") 
				.append("    margin: 0;\r\n") 
				.append("    padding: 0;\r\n") 
				.append("    position: relative;\r\n") 
				.append("}\r\n.z-tabs-ver {\r\n") 
				.append("    float: left;\r\n") 
				.append("}\r\n.z-tabpanel, .z-tabbox-ver .z-tabpanels-ver {\r\n") 
				.append("    border-color: #7EAAC6;\r\n") 
				.append("    border-style: solid;\r\n") 
				.append("    border-width: 1px;\r\n") 
				.append(" padding: 3px;\r\n") 
				.append("}\r\n</style>\r\n") 
				.append("</head>\r\n") 
				.append("<body>\r\n") 
				.append("<form name=\"frm\">\r\n") 
				.append("<table>\r\n") 
				.append(" <tr>\r\n") 
				.append(" <td>\r\n") 
				.append(" <div id=\"pJHP75\" class=\"msgQueue\"></div>\r\n") 
				.append(" </td>\r\n") 
				.append(" </tr>\r\n") 
				.append(" <tr>\r\n") 
				.append(" <td>\r\n") 
				.append(" <br/>\r\n") 
				.append(" <div style=\"width: 661;\">")
				.append("     <table border=\"0\" style=\"width: 650px;\">")
				.append("          <tr><td><label class=\"docsubtitle\">Prot. 220/2020</label></td></tr>")
				.append("          <tr><td colspan=\"6\"><hr style=\"width: 100%\"></hr></td></tr>")
				.append("          <tr><td><label class=\"scriptaLabelBold\">Tipologia Documento :</label></td>")
				.append("               <td><label id=\"tipoDocumento\" name=\"tipoDocumento\" class=\"scripta\">Test&#95;Doc&#95;JU</label></td></tr>")
				.append("          <tr><td><label class=\"scriptaLabelBold\">Indice di classificazione del Fascicolo: </label></td>")
				.append("          <td><label id=\"indiceClassificazione\" name=\"indiceClassificazione\" class=\"scripta\"></label></td>")
				.append("          </tr><tr><td><label class=\"scriptaLabelBold\">Nome del fascicolo (NUMERO_ANNO_OGGETTO): </label></td>")
				.append("               <td><label id=\"nomeFascicolo\" name=\"nomeFascicolo\" class=\"scripta\"></label></td>")
				.append("          </tr><tr><td><label class=\"scriptaLabelBold\">Oggetto:</label></td><td colspan=\"3\">")
				.append("               <div id=\"oggetto\" name=\"oggetto\" class=\"scripta\" >")
				.append("Test Test00&#38;&#35;46;risposta eseguito sulla macchina 192&#38;&#35;46;")
				.append("168&#38;&#35;46;1&#38;&#35;46;101 in data 07&#38;&#35;47;07&#38;&#35;47;2020 19:42:36</div></td>          </tr>")
				.append("          <tr><td><label class=\"scriptaLabelBold\">Numero di raccomandata / Plico: </label></td>")
				.append("               <td><label id=\"numRaccomandata\" name=\"numRaccomandata\" class=\"scripta\"></label></td>          </tr>")
				.append("          <tr><td><label class=\"scriptaLabelBold\">Data Scadenza :</label></td>")
				.append("               <td><label id=\"dataScadenza\" name=\"dataScadenza\" class=\"scripta\"></label></td>")
				.append("          </tr>")
				.append("          <tr><td><label class=\"scriptaLabelBold\">Note:</label></td><td colspan=\"3\">")
				.append("          <div id=\"note\" name=\"note\" class=\"scripta\" ></div></td>")
				.append("          </tr>")
				.append("     </table><br/>\r\n") 
				.append(" <div id=\"pJHP56-chdex\" class=\"z-vlayout-inner\" style=\"padding-bottom:5px\">\r\n") 
				.append("  <div id=\"pJHP76\" class=\"z-tabs z-tabs-scroll\" style=\"width: 660px;\">\r\n") 
				.append("   <ul id=\"pJHP76-cave\" class=\"z-tabs-cnt\">\r\n") 
				.append("    <li id=\"pJHP86\" class=\"z-tab z-tab-seld\">\r\n") 
				.append("     <div id=\"pJHP86-hl\" class=\"z-tab-hl\">\r\n") 
				.append("      <div id=\"pJHP86-hr\" class=\"z-tab-hr\">\r\n") 
				.append("       <div id=\"pJHP86-hm\" class=\"z-tab-hm \">\r\n")
				.append("        <span class=\"scriptaSection\">Dettaglio - Documento in Entrata</span>\r\n") 
				.append("       </div> \r\n") 
				.append("      </div>\r\n") 
				.append("     </div>\r\n")
				.append("    </li>\r\n") 
				.append("   </ul> \r\n") 
				.append("  </div>\r\n") 
				.append(" </div>\r\n") 
				.append(" <div id=\"sCJQzd\" class=\"z-tabpanel\">\r\n") 
				.append("  <div id=\"sCJQzd-cave\" class=\"z-tabpanel-cnt\"><table border=\"0\" style=\"width: 650px;\"><tr><td><label class=\"scriptaLabelBold\">")
				.append("Formato documento in entrata: </label></td><td colspan=\"5\"><label id=\"formatoDocumento\" name=\"formatoDocumento\" class=\"scripta\">")
				.append("Elettronico</label></td></tr><tr><td><label class=\"scriptaLabelBold\">Tipo procedimento: </label></td><td colspan=\"5\">")
				.append("<label id=\"tipoProcedimento\" name=\"tipoProcedimento\" class=\"scripta\">PROC&#95;JU</label></td></tr>")
				.append("<tr><td><label class=\"scriptaLabelBold\">Mittente: </label></td><td colspan=\"5\">")
				.append("<label id=\"mittente\" name=\"mittente\" class=\"scripta\">irene&#46;pellegrini&#46;cap&#64;")
				.append("tesoro&#46;it irene&#46;pellegrini&#46;cap&#64;tesoro&#46;it")
				.append("</label></td></tr><tr><td><label class=\"scriptaLabelBold\">Mezzo di ricezione: </label></td><td colspan=\"5\">")
				.append("<label id=\"mezzoRicezione\" name=\"mezzoRicezione\" class=\"scripta\"></label></td></tr><tr><td>")
				.append("<label class=\"scriptaLabelBold\">Riservato: </label>")
				.append("</td><td colspan=\"5\"><label id=\"riservato\" name=\"riservato\" class=\"scripta\">No</label></td></tr><tr>")
				.append("<td><label class=\"scriptaLabelBold\">Urgente: </label></td><td colspan=\"5\">")
				.append("<label id=\"urgente\" name=\"urgente\" class=\"scripta\">No</label></td>")
				.append("</tr><tr><td><label class=\"scriptaLabelBold\">Da non protocollare: </label></td><td colspan=\"5\">")
				.append("<label id=\"protocollazione\" name=\"protocollazione\" class=\"scripta\">No</label></td></tr><tr><td><label class=\"scriptaLabelBold\">")
				.append("Protocollo mittente (data - anno / numero): </label></td><td colspan=\"5\">")
				.append("<label id=\"protocolloMittente\" name=\"protocolloMittente\" class=\"scripta\">")
				.append("</label></td></tr><tr><td><label class=\"scriptaLabelBold\">In risposta al protocollo (anno / numero): </label></td><td colspan=\"5\">")
				.append("<label id=\"protocolloRisposta\" name=\"protocolloRisposta\" class=\"scripta\"></label></td></tr><tr><td>")
				.append("<label class=\"scriptaLabelBold\">Barcode:</label></td>")
				.append("<td colspan=\"5\"><label id=\"barcode\" name=\"barcode\" class=\"scripta\"></label></td></tr><tr><td>")
				.append("<label class=\"scriptaLabelBold\">Assegnatario per Competenza:</label>")
				.append("</td><td colspan=\"5\"><label id=\"assegnatarioCompetenza\" name=\"assegnatarioCompetenza\" ")
				.append("class=\"scripta\">UFFICIO DI DIREZIONE</label></td></tr>")
				.append("<tr><td colspan=\"6\"><hr style=\"width: 100%\"></hr></td></tr></table> </div>\r\n") 
				.append(" </div>\r\n") 
				.append(".</div>\r\n")
				.append(" </td>\r\n") 
				.append(" </tr>\r\n")
				.append("</table>\r\n") 
				.append("\r\n") 
				.append("</form>\r\n") 
				.append("</body>\r\n") 
				.append("</html>");
		
		final byte[] htmlContent = velinaHTML.toString().getBytes();
		final InputStream is = new AdobeLCHelper().htmlToPdf(htmlContent);
		FileUtils.saveToFile(FileUtils.getByteFromInputStream(is), "C:\\Users\\CristianoPierascenzi\\Desktop\\call\\v1.pdf");
	}

}
