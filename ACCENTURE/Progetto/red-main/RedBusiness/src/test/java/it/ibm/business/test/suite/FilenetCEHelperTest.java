package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.Folder;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * Classe di test Helper Filenet CE.
 */
public class FilenetCEHelperTest extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FilenetCEHelperTest.class);
	
	/**
	 * Esegue la logica prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}
	
	/**
	 * Test.
	 */
	@Test
	public void getFolderTitolarioNotNullTest() {
		
		// Nome del titolario è composto da idAOO_indiceClassificazione
		final String nomeT = "25_H";
		
		final Folder folderTitolario = null;
		
		LOGGER.info(" Utente utilizzato ----> " + USERNAME_MAZZOTTA);
		LOGGER.info(" WHERE condition - nomeTitolario = ----> " + nomeT);
		LOGGER.info(" Valore del titolario ----> " + folderTitolario);
		
		Assert.assertNotNull(folderTitolario);
		
	}
	
	/**
	 * Restituisce il document for master non vuoto.
	 */
	@Test
	public void getDocumentForMasterNotEmptyTest() {
		final UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
		
		final Collection<String> documentTitlesToFilterBy = new ArrayList<>();
		documentTitlesToFilterBy.add("39245");
		documentTitlesToFilterBy.add("39222");
		documentTitlesToFilterBy.add("39246");
		documentTitlesToFilterBy.add("39204");
		documentTitlesToFilterBy.add("39193");

		final DocumentSet dsFileNet = fceh.getDocumentsForMasters(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitlesToFilterBy);
		
		Assert.assertFalse(dsFileNet.isEmpty());
		
	}
	
}
