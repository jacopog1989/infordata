package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoStrutturaNodoEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Test Verificato.
 */
public class VerificatoTest extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(VerificatoTest.class);
	
	/**
	 * Messaggio errore creazione documento uscita.
	 */
	private static final String ERROR_CREAZIONE_DOC_USCITA_MSG = "Errore in fase di creazione documento in uscita.";
	
	/**
	 * Messaggio errore richiesta visti.
	 */
	private static final String ERROR_RICHIESTA_VISTI_MSG = "Errore in fase di richiesta visti.";
	
	/**
	 * Label Num doc.
	 */
	private static final String NUM_DOC = "NUM DOC : ";
	
	/**
	 * Label Oggetto.
	 */
	private static final String OGGETTO = "OGGETTO : ";

	/**
	 * Label document title.
	 */
	private static final String DOCUMENT_TITLE = "DOCUMENT TITLE : ";

	/**
	 * Test verificato.
	 */
	@Test
	public final void test1VerificatoTest() {
		test1Protected();
	}

	/**
	 * Test.
	 */
	protected void test1Protected() {

		// Caso 1: S1 S2 -> MAZZOTTA IGF

		// Creazione documento
		UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		String oggetto = createObj();
		String indiceFascicoloProcedimentale = "A";
		Boolean bRiservato = false;

		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(createDestinatarioEsterno(MezzoSpedizioneEnum.CARTACEO, ID_CONTATTO_GIANCARLO_ROSSI,
				ModalitaDestinatarioEnum.TO));

		List<AssegnazioneDTO> assegnazioni = null;
		List<AllegatoDTO> allegati = null;

		EsitoSalvaDocumentoDTO esitoUscita = createDocUscita(allegati, false, oggetto, utente,
				ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, assegnazioni, destinatari,
				indiceFascicoloProcedimentale, MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD,
				FIRMA_DIRIGENTE_RGS, bRiservato, null);
		checkAssertion(esitoUscita.isEsitoOk(), ERROR_CREAZIONE_DOC_USCITA_MSG);

		String dt = esitoUscita.getDocumentTitle();
		Integer numDoc = getNumDoc(USERNAME_MAZZOTTA, dt);

		List<AssegnazioneDTO> assegnazioneVistoList = new ArrayList<>();

		AssegnazioneDTO assIspVisto = new AssegnazioneDTO(null, TipoAssegnazioneEnum.VISTO,
				TipoStrutturaNodoEnum.ISPETTORATO, null, null, null, null, new UfficioDTO(ID_IGF.longValue(), null));
		assegnazioneVistoList.add(assIspVisto);

		dumpDTInfo(dt);

		EsitoOperazioneDTO esitoRichiestaVisti = richiestaVisti(USERNAME_MAZZOTTA, dt, assegnazioneVistoList);
		checkAssertion(esitoRichiestaVisti.isEsito(), ERROR_RICHIESTA_VISTI_MSG);

		LOGGER.info(DOCUMENT_TITLE + dt);
		LOGGER.info(OGGETTO + oggetto);
		LOGGER.info(NUM_DOC + numDoc);
	}

	/**
	 * Test verificato.
	 */
	@Test
	public final void test2VerificatoTest() {
		test2Protected();
	}

	/**
	 * Test.
	 */
	protected void test2Protected() {
		// Caso 2: S1 S3 -> MAZZOTTA IGB UFFICIO I

		// Creazione documento
		UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		String oggetto = createObj();
		String indiceFascicoloProcedimentale = "A";
		Boolean bRiservato = false;

		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(createDestinatarioEsterno(MezzoSpedizioneEnum.CARTACEO, ID_CONTATTO_GIANCARLO_ROSSI,
				ModalitaDestinatarioEnum.TO));

		List<AssegnazioneDTO> assegnazioni = null;
		List<AllegatoDTO> allegati = null;

		EsitoSalvaDocumentoDTO esitoUscita = createDocUscita(allegati, false, oggetto, utente,
				ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, assegnazioni, destinatari,
				indiceFascicoloProcedimentale, MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD,
				FIRMA_DIRIGENTE_RGS, bRiservato, null);
		checkAssertion(esitoUscita.isEsitoOk(), ERROR_CREAZIONE_DOC_USCITA_MSG);

		String dt = esitoUscita.getDocumentTitle();
		Integer numDoc = getNumDoc(USERNAME_MAZZOTTA, dt);

		List<AssegnazioneDTO> assegnazioneVistoList = new ArrayList<>();
		AssegnazioneDTO assUffVisto = new AssegnazioneDTO("", TipoAssegnazioneEnum.VISTO, TipoStrutturaNodoEnum.IGNOTO,
				null, null, "IGB - UFFICIO I", null, new UfficioDTO(ID_IGB_UFFICIO_I.longValue(), "IGB - UFFICIO I"));
		assegnazioneVistoList.add(assUffVisto);

		EsitoOperazioneDTO esitoRichiestaVisti = richiestaVisti(USERNAME_MAZZOTTA, dt, assegnazioneVistoList);
		checkAssertion(esitoRichiestaVisti.isEsito(), ERROR_RICHIESTA_VISTI_MSG);

		LOGGER.info(DOCUMENT_TITLE + dt);
		LOGGER.info(OGGETTO + oggetto);
		LOGGER.info(NUM_DOC + numDoc);
	}
	
	/**
	 * Test verificato.
	 */
	@Test
	public final void test3VerificatoTest() {
		test3Protected();
	}

	/**
	 * Metodo di test - protected.
	 */
	protected void test3Protected() {
		// Caso 3: S1 S2 S3 -> MAZZOTTA IGF / IGB UFFICIO I

		// Creazione documento
		UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		String oggetto = createObj();
		String indiceFascicoloProcedimentale = "A";
		Boolean bRiservato = false;

		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(createDestinatarioEsterno(MezzoSpedizioneEnum.CARTACEO, ID_CONTATTO_GIANCARLO_ROSSI,
				ModalitaDestinatarioEnum.TO));

		List<AssegnazioneDTO> assegnazioni = null;
		List<AllegatoDTO> allegati = null;

		EsitoSalvaDocumentoDTO esitoUscita = createDocUscita(allegati, false, oggetto, utente,
				ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, assegnazioni, destinatari,
				indiceFascicoloProcedimentale, MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD,
				FIRMA_DIRIGENTE_RGS, bRiservato, null);
		checkAssertion(esitoUscita.isEsitoOk(), ERROR_CREAZIONE_DOC_USCITA_MSG);

		String dt = esitoUscita.getDocumentTitle();
		Integer numDoc = getNumDoc(USERNAME_MAZZOTTA, dt);

		List<AssegnazioneDTO> assegnazioneVistoList = new ArrayList<>();
		AssegnazioneDTO assUffVisto = new AssegnazioneDTO(null, TipoAssegnazioneEnum.VISTO,
				TipoStrutturaNodoEnum.SETTORE, null, null, null, null, new UfficioDTO(ID_IGB_UFFICIO_I.longValue(), null));
		AssegnazioneDTO assIspVisto = new AssegnazioneDTO(null, TipoAssegnazioneEnum.VISTO,
				TipoStrutturaNodoEnum.ISPETTORATO, null, null, null, null, new UfficioDTO(ID_IGF.longValue(), null));
		assegnazioneVistoList.add(assIspVisto);
		assegnazioneVistoList.add(assUffVisto);

		EsitoOperazioneDTO esitoRichiestaVisti = richiestaVisti(USERNAME_MAZZOTTA, dt, assegnazioneVistoList);
		checkAssertion(esitoRichiestaVisti.isEsito(), ERROR_RICHIESTA_VISTI_MSG);

		LOGGER.info(DOCUMENT_TITLE + dt);
		LOGGER.info(OGGETTO + oggetto);
		LOGGER.info(NUM_DOC + numDoc);
		// Caso 1: S1 S2 -> MAZZOTTA IGF
		// Caso 2: S1 S3 -> MAZZOTTA IGB UFFICIO I
		// Caso 3: S1 S2 S3 -> MAZZOTTA IGF / IGB UFFICIO I
	}

}
