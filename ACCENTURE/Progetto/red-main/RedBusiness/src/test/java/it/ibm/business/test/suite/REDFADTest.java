package it.ibm.business.test.suite;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAooSRV;

public class REDFADTest extends AbstractTest {

	/**
	 * Servizio.
	 */
	private IAooSRV aooSRV;

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		aooSRV = ApplicationContextProvider.getApplicationContext().getBean(IAooSRV.class);
		pp = PropertiesProvider.getIstance();

	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Esegue il test di terminazione raccolta.
	 */
	@Test
	public void terminaRaccolta() {

		String idFascicoloRaccoltaProvvisoria = "B3F0FA7C-EB96-F8BB-E053-6546250A47B2";
		String idDocumento = "41953";

		// recupera l'AOO
		Aoo aoo = aooSRV.recuperaAoo(Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.IDAOO_RGS)));

		// inizializza accesso al content engine in modalità administrator
		AooFilenet aooFilenet = aoo.getAooFilenet();

		FilenetPEHelper pe = new FilenetPEHelper(aooFilenet.getUsername(), aooFilenet.getPassword(),
				aooFilenet.getUri(), aooFilenet.getStanzaJaas(), aooFilenet.getConnectionPoint());

		pe.terminaAttoDecreto(idFascicoloRaccoltaProvvisoria);

		VWWorkObject vw = pe.getWorkflowPrincipale(idDocumento, null);

		checkAssertion(vw == null, "Il documento non è stato messo agli atti");
	}

}