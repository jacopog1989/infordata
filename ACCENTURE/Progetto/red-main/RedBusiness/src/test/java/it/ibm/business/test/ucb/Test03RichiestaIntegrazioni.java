package it.ibm.business.test.ucb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.filenet.api.core.Document;
import com.google.common.net.MediaType;

import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.CollectedParametersDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.RecuperoCodeDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * Test 03 - Richiesta Integrazione.
 */
public class Test03RichiestaIntegrazioni extends AbstractUCBTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test03RichiestaIntegrazioni.class);

	/**
	 * Messaggio di errore creazione documento.
	 */
	private static final String ERROR_CREAZIONE_DOCUMENTO_MSG = "La creazione del documento non è andata a buon fine.";

	/**
	 * Enum dei risultati del test.
	 */
	enum TestResultFields {

		/**
		 * Valore.
		 */
		INGRESSO_DT,

		/**
		 * Valore.
		 */
		INGRESSO_ID_DOC,

		/**
		 * Valore.
		 */
		USCITA_DT,

		/**
		 * Valore.
		 */
		USCITA_ID_DOC,

		/**
		 * Valore.
		 */
		CLASSE_DOCUMENTALE;
	}

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test richiesta integrazione scaduta.
	 */
	@Test
	public final void richiestaIntegrazioneScaduta() {
		testRichiestaIntegrazioni(true, true);
	}

	/**
	 * Test richiesta integrazione riconicliata protocollatore.
	 */
	@Test
	public final void richiestaIntegrazioneRiconciliataProtocollatore() {
		final UtenteDTO utente = getUtente(USERNAME_BRIAMONTE);

		final Map<TestResultFields, Object> in = testRichiestaIntegrazioni(true, false); // Firmata non riportata in automatico in lavorazione

		final String dtIngresso = (String) in.get(TestResultFields.INGRESSO_DT);
		final String classeDoc = (String) in.get(TestResultFields.CLASSE_DOCUMENTALE);

		final String nomeFascicoloIngresso = getFascicoloProcedimentale(dtIngresso, utente).getDescrizione();

		final IFilenetCEHelper ce = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
		final Document fDoc = ce.getDocumentByDTandAOO(dtIngresso, utente.getIdAoo());

		final Integer annoProt = fDoc.getProperties().getInteger32Value(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY));
		final Integer numProt = fDoc.getProperties().getInteger32Value(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY));

//			STEP 7: creazoine documento in ingresso che rappresenta l'integrazione con indicazione del protocolloRiferimento

		final Map<FascicoloDTO, List<DocumentoAllegabileDTO>> fascicoli = getDocsProtRiferimento(utente, numProt, annoProt, true);

		FascicoloDTO fascicolo = null;
		DocumentoAllegabileDTO documento = null;
		for (final Entry<FascicoloDTO, List<DocumentoAllegabileDTO>> e : fascicoli.entrySet()) {
			fascicolo = e.getKey();
			if (nomeFascicoloIngresso.equalsIgnoreCase(fascicolo.getDescrizione())) {
				for (final DocumentoAllegabileDTO d : e.getValue()) {
					if (!d.isDisabled()) {
						documento = d;
					}
				}
				break;
			}
		}

		checkAssertion(documento != null, "Impossibile selezionare il documento di interesse");

		if (documento != null) {

			final String protocolloRiferimento = documento.getDocumentTitle();

			final EsitoSalvaDocumentoDTO esitoCreazioneIntegrazioneDati = createDoc(utente, ID_TIPO_DOC_INTEGRAZIONE_DATI, ID_TIPO_PROC_INTEGRAZIONE_DATI_GENERICO,
					protocolloRiferimento);
			checkAssertion((esitoCreazioneIntegrazioneDati.getErrori() == null || esitoCreazioneIntegrazioneDati.getErrori().isEmpty()), ERROR_CREAZIONE_DOCUMENTO_MSG);

			final EsitoOperazioneDTO esitoAssegnazione = assegnaDaCorriere(USERNAME_BRIAMONTE, esitoCreazioneIntegrazioneDati.getDocumentTitle(), USERNAME_BRIAMONTE);
			checkAssertion(esitoAssegnazione.getFlagEsito(), "L'assegnazione del documento non è andata a buon fine.");

//		STEP 8: 

			final String wobIntegrazioneDati = esitoCreazioneIntegrazioneDati.getWobNumber();

			final EsitoOperazioneDTO esito = validaIntegrazioneDati(utente, wobIntegrazioneDati);
			checkAssertion(esito.getFlagEsito(), "Validazione integrazione dati non è andata a buon fine.");

			final RecuperoCodeDTO codeDocIntDati = getQueues(utente.getUsername(), esitoCreazioneIntegrazioneDati.getDocumentTitle(), classeDoc,
					CategoriaDocumentoEnum.ENTRATA.getIds()[0]);
			checkAssertionCode(codeDocIntDati, DocumentQueueEnum.CHIUSE, DocumentQueueEnum.ATTI);

			final RecuperoCodeDTO codeDocIngresso = getQueues(utente.getUsername(), dtIngresso, classeDoc, CategoriaDocumentoEnum.ENTRATA.getIds()[0]);
			checkAssertionCode(codeDocIngresso, DocumentQueueEnum.DA_LAVORARE_UCB, DocumentQueueEnum.ASSEGNATE);
			checkStorico(utente, null, dtIngresso, EventTypeEnum.RICONCILIAZIONE_INTEGRAZIONE_DATI);
		}

	}

	/**
	 * Test richiesta integrazione riconciliata utente.
	 */
	@Test
	public final void richiestaIntegrazioneRiconciliataUtente() {
		final UtenteDTO utente = getUtente(USERNAME_BRIAMONTE);

//		STEP 7: creazoine documento in ingresso che rappresenta l'integrazione

		final Map<TestResultFields, Object> in = testRichiestaIntegrazioni(true, false); // Firmata non riportata in automatico in lavorazione

		final String dtIngresso = (String) in.get(TestResultFields.INGRESSO_DT);
		final String classeDoc = (String) in.get(TestResultFields.CLASSE_DOCUMENTALE);

		final String wobIngresso = getWobNumber(utente, DocumentQueueEnum.SOSPESO, dtIngresso);

		final EsitoSalvaDocumentoDTO esitoCreazioneIntegrazioneDati = createDoc(utente, ID_TIPO_DOC_INTEGRAZIONE_DATI, ID_TIPO_PROC_INTEGRAZIONE_DATI_GENERICO);
		checkAssertion((esitoCreazioneIntegrazioneDati.getErrori() == null || esitoCreazioneIntegrazioneDati.getErrori().isEmpty()), ERROR_CREAZIONE_DOCUMENTO_MSG);

		final String dtDocIntegrazioneDati = esitoCreazioneIntegrazioneDati.getDocumentTitle();

//		STEP 8: 

		MasterDocumentRedDTO documentoSelezionato = null;
		final Collection<MasterDocumentRedDTO> documenti = getIntegrazioneDati4Associa(utente);
		for (final MasterDocumentRedDTO documento : documenti) {
			if (documento.getDocumentTitle().equals(dtDocIntegrazioneDati)) {
				documentoSelezionato = documento;
				break;
			}
		}
		checkAssertion(documentoSelezionato != null, "La selezione del documento da associare non è andata a buon fine.");

		if (documentoSelezionato != null) {

			final EsitoOperazioneDTO esito = associaIntegrazioneDati(utente, wobIngresso, documentoSelezionato.getDocumentTitle());
			checkAssertion(esito.getFlagEsito(), "Associazione integrazione dati non è andata a buon fine.");
			final RecuperoCodeDTO codeDocIntDati = getQueues(utente.getUsername(), dtDocIntegrazioneDati, classeDoc, CategoriaDocumentoEnum.ENTRATA.getIds()[0]);
			checkAssertionCode(codeDocIntDati, DocumentQueueEnum.CHIUSE, DocumentQueueEnum.ATTI);

			final RecuperoCodeDTO codeDocIngresso = getQueues(utente.getUsername(), dtIngresso, classeDoc, CategoriaDocumentoEnum.ENTRATA.getIds()[0]);
			checkAssertionCode(codeDocIngresso, DocumentQueueEnum.DA_LAVORARE_UCB, DocumentQueueEnum.ASSEGNATE);
			checkStorico(utente, null, dtIngresso, EventTypeEnum.RICONCILIAZIONE_INTEGRAZIONE_DATI);
		}

	}

	/**
	 * Test richiesta integrazione annullata.
	 */
	@Test
	public final void richiestaIntegrazioneAnnullata() {
		testRichiestaIntegrazioni(false, null);
	}

	private Map<TestResultFields, Object> testRichiestaIntegrazioni(final Boolean sign, final Boolean expired) {
		final EnumMap<TestResultFields, Object> out = new EnumMap<>(TestResultFields.class);
		final UtenteDTO utente = getUtente(USERNAME_BRIAMONTE);

//		STEP 1
//		BRIAMONTE crea ed assegna al CORRIERE dell'ULAV

		final EsitoSalvaDocumentoDTO esitoCreazione = createDoc(utente, ID_TIPO_DOC_ATTI_SOGGETTI_VISTO_ENTRATA, ID_TIPO_PROC_ATTI_SOGGETTI_VISTO_ENTRATA);

		final String dtIngresso = esitoCreazione.getDocumentTitle();
		final Integer idDocIngresso = esitoCreazione.getNumeroDocumento();

		dumpCreazioneDocumento(esitoCreazione);
		checkAssertion((esitoCreazione.getErrori() == null || esitoCreazione.getErrori().isEmpty()), ERROR_CREAZIONE_DOCUMENTO_MSG);

//		STEP 2
//		Il CORRIERE ULAV lo assegna a BRIAMONTE che se lo ritrova in DA LAVORARE

		final EsitoOperazioneDTO esitoAssegnazione = assegnaDaCorriere(USERNAME_BRIAMONTE, esitoCreazione.getDocumentTitle(), USERNAME_BRIAMONTE);
		checkAssertion(esitoAssegnazione.getFlagEsito(), "L'assegnazione del corriere non è andata a buon fine.");

//		STEP 3
//		BRIAMONTE predispone la richiesta integrazione

		final Integer idRegistro = ID_RICHIESTA_INTEGRAZIONE_EX_ART_14BIS_COMMA_2;

		final MasterDocumentRedDTO masterIngresso = getDocumentForDT(utente, dtIngresso, idDocIngresso);
		checkAssertion(masterIngresso != null, "Documento non trovato.");

		final Collection<MetadatoDTO> metadatiRegistroAusiliarioList = getMetadatiRegistro(idRegistro, utente, dtIngresso);

		final CollectedParametersDTO c = new CollectedParametersDTO();

		for (final MetadatoDTO m : metadatiRegistroAusiliarioList) {
			c.addStringParam(m.getName(), m.getName());
		}

		final FileDTO contentUscita = new FileDTO("template.pdf", createPdf(idRegistro, c.getStringParams()), MediaType.PDF.toString());

		final DetailDocumentRedDTO detail = predisponiRichiestaIntegrazione(utente, masterIngresso, contentUscita, idRegistro, metadatiRegistroAusiliarioList);
		LOGGER.info("#####INGRESSO : DT > " + dtIngresso + " NUM > " + idDocIngresso);

		final String classeDoc = masterIngresso.getClasseDocumentale();
		final Integer idCatDocIngresso = masterIngresso.getIdCategoriaDocumento();

//		STEP 4
//		BRIAMONTE finalizza la risposta fornendo i dati mancanti: 
//		- iter di firma manuale verso se stesso
//		- tab spedizione
//		- Indice di classificazoine
//		- momento protocollazione

		// Valorizzazione iter di firma
		detail.setIdIterApprovativo(IterApprovativoDTO.ITER_FIRMA_MANUALE);
		final List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneFirma(ID_UFFICIO_DIREZIONE_ULAV, ID_FRANCESCO_BRIAMONTE, "FRANCESCO", "BRIAMONTE"));
		detail.setAssegnazioni(assegnazioni);

		// Valorizzare Tab spedizione
		if (detail.getDestinatari() != null && !detail.getDestinatari().isEmpty()
				&& MezzoSpedizioneEnum.ELETTRONICO.equals(detail.getDestinatari().get(0).getMezzoSpedizioneEnum())) {
			final EmailDTO mailSpedizione = new EmailDTO();
			mailSpedizione.setMittente("testred@pec.mef.gov.it");
			mailSpedizione.setDestinatari("a@b.c");
			mailSpedizione.setOggetto("MEF - RGS - Un numero di protocollo valido - " + detail.getOggetto());
			mailSpedizione.setTesto("Ciao Mondo!");
			detail.setMailSpedizione(mailSpedizione);
		}

		// Valorizzare Indice di classificazione
		detail.setIndiceClassificazioneFascicoloProcedimentale("A");

		// Protocollazione standard
		detail.setMomentoProtocollazioneEnum(MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD);

		// finalizza creazione documento uscita

		final EsitoSalvaDocumentoDTO esitoSalvataggioRisposta = salvaDocumento(detail, utente);

		dumpCreazioneDocumento(esitoSalvataggioRisposta);
		checkAssertion((esitoSalvataggioRisposta.getErrori() == null || esitoSalvataggioRisposta.getErrori().isEmpty()),
				"La creazione dell'uscita in risposta non è andata a buon fine.");

		final String dtUscita = esitoSalvataggioRisposta.getDocumentTitle();
		final String wfUscita = esitoSalvataggioRisposta.getWobNumber();
		final Integer idDocUscita = esitoSalvataggioRisposta.getNumeroDocumento();

		out.put(TestResultFields.USCITA_DT, dtUscita);
		out.put(TestResultFields.USCITA_ID_DOC, idDocUscita);

		LOGGER.info("#####USCITA : DT > " + dtUscita + " NUM > " + idDocUscita);

		RecuperoCodeDTO codeIngresso = getQueues(utente.getUsername(), dtIngresso, classeDoc, idCatDocIngresso);
		RecuperoCodeDTO codeUscita = getQueues(utente.getUsername(), dtUscita, classeDoc, CategoriaDocumentoEnum.USCITA.getIds()[0]);

		LOGGER.info("Verifica code ingresso");
		checkAssertionCode(codeIngresso, DocumentQueueEnum.SOSPESO, DocumentQueueEnum.ASSEGNATE);

		LOGGER.info("Verifica code uscita");
		checkAssertionCode(codeUscita, DocumentQueueEnum.NSD);

		// verifica storico ingresso

		checkStorico(utente, null, dtIngresso, EventTypeEnum.RICHIESTA_INTEGRAZIONI);

		out.put(TestResultFields.CLASSE_DOCUMENTALE, classeDoc);

		out.put(TestResultFields.INGRESSO_DT, dtIngresso);
		out.put(TestResultFields.INGRESSO_ID_DOC, idDocIngresso);

//		STEP 5
//		BRIAMONTE ricerca il visto dalla ricerca esiti. 

		// verifica ricerca esito
		final Collection<MasterDocumentRedDTO> result = ricercaEsiti(utente, SottoCategoriaDocumentoUscitaEnum.RICHIESTA_INTEGRAZIONI);

		Boolean bOk = false;
		for (final MasterDocumentRedDTO master : result) {
			if (master.getDocumentTitle().equals(dtIngresso) && master.getIdTipologiaDocumento().equals(ID_TIPO_DOC_ATTI_SOGGETTI_VISTO_ENTRATA)
					&& master.getIdTipoProcedimento().equals(ID_TIPO_PROC_ATTI_SOGGETTI_VISTO_ENTRATA)) {
				bOk = true;
				break;
			}
		}

		checkAssertion(bOk, "Il documento in ingresso non è ricercabile per esito.");

		if (Boolean.TRUE.equals(sign)) {
//			STEP 6A
//			BRIAMONTE firma il documento

			signRichiestaIntegrazione(utente, masterIngresso.getClasseDocumentale(), idCatDocIngresso, dtIngresso, null, dtUscita, wfUscita);

			if (Boolean.TRUE.equals(expired)) {
//				STEP 7AA: la richiesta di integrazione viene spedita e simuliamo che sia scaduto il tempo di attesa in SOSPESO.

				simulaInvioMailEFaiScadere(dtIngresso, dtUscita, utente);

//				STEP 8AA: il batch rimette in lavorazione il documento.

				rimettiInLavorazione(utente.getIdAoo(), dtIngresso);

				codeIngresso = getQueues(utente.getUsername(), dtIngresso, classeDoc, idCatDocIngresso);
				checkAssertionCode(codeIngresso, DocumentQueueEnum.DA_LAVORARE_UCB, DocumentQueueEnum.ASSEGNATE);

			}

		} else {
//			STEP 6B
//			BRIAMONTE annulla l'osservazione in qualità di firmatario del visto
			final EsitoOperazioneDTO esitoAnnullamento = annulla(utente, wfUscita, "Test annullamento visto.", ResponsesRedEnum.ANNULLA_RICHIESTA_INTEGRAZIONI);
			checkAssertion(esitoAnnullamento.getFlagEsito(), "Annullamento fallito.");

			// check code uscita
			codeUscita = getQueues(utente.getUsername(), dtUscita, classeDoc, CategoriaDocumentoEnum.USCITA.getIds()[0]);
			checkAssertionCode(codeUscita, DocumentQueueEnum.DA_LAVORARE_UCB);

			// Verifico lo storico dell'uscita.
			checkStorico(utente, null, dtUscita, EventTypeEnum.RIFIUTO);

//			STEP 7B
//			BRIAMONTE conferma annullamento in quanto creatore del visto

			final EsitoOperazioneDTO confermaAnnuallmento = confermaAnnullamento(utente, wfUscita);
			checkAssertion(confermaAnnuallmento.getFlagEsito(), "Conferma Annullamento fallito.");

			// check code ingresso
			codeIngresso = getQueues(utente.getUsername(), dtIngresso, classeDoc, idCatDocIngresso);
			checkAssertionCode(codeIngresso, DocumentQueueEnum.DA_LAVORARE_UCB, DocumentQueueEnum.ASSEGNATE);

			// Verifico lo storico dell'uscita.
			checkStorico(utente, null, dtUscita, EventTypeEnum.ANNULLATO); // se passo il wob dell'uscita mi da errore, probabilmente annullamento lo ha
																			// killato, se passo solo document title trovo DOCUMENTO,
																			// ASSEGNAZIONE_CONOSCENZA.ASSEGNAZIONE_CONOSCENZ, ASSEGNAZIONE_COMPETENZA,
																			// ASSEGNAZIONE_DIRETTA_A_UTENTE, PREDISPOSIZIONE_VISTO ma niente ANNULLATO

			// Verifico lo storico dell'entrata.
			checkStorico(utente, null, dtIngresso, EventTypeEnum.RICHIESTA_INTEGRAZIONE_ANNULLATA);

		}

		return out;
	}

	private void signRichiestaIntegrazione(final UtenteDTO utente, final String classeDocumentale, final Integer idCategioriaDocumentoIngresso, final String dtIngresso,
			final String wobNumberIngresso, final String dtUscita, final String wobNumberUscita) {
		final EsitoOperazioneDTO esito = sign(utente, wobNumberUscita);

		// Verifico l'esito della firma.
		checkAssertion(esito.getFlagEsito(), "Errore in fase di firma.");

		// Verifico lo storico dell'ingresso.
		checkStorico(utente, wobNumberIngresso, dtIngresso, EventTypeEnum.RICHIESTA_INTEGRAZIONI);

		// Verifico lo storico dell'uscita.
		checkStorico(utente, wobNumberUscita, dtUscita, EventTypeEnum.FIRMATO);

		// Verifico le code in cui si trova l'ingresso dopo la firma.
		final RecuperoCodeDTO codeIngresso = getQueues(utente.getUsername(), dtIngresso, classeDocumentale, idCategioriaDocumentoIngresso);
		checkAssertionCode(codeIngresso, DocumentQueueEnum.ASSEGNATE, DocumentQueueEnum.SOSPESO);

		// Verifico le code in cui si trova l'uscita dopo la firma.
		final RecuperoCodeDTO codeUscita = getQueues(utente.getUsername(), dtUscita, classeDocumentale, CategoriaDocumentoEnum.USCITA.getIds()[0]);
		checkAssertionCode(codeUscita, DocumentQueueEnum.SPEDIZIONE, DocumentQueueEnum.PROCEDIMENTI);
	}

}
