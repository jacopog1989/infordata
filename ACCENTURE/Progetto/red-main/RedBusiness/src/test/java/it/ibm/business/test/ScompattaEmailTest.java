package it.ibm.business.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.HierarchicalFileWrapperDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.PkHandler;
import it.ibm.red.business.persistence.model.TipoFile;
import it.ibm.red.business.service.concrete.ScompattaMailSRV;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;

/**
 * Classe di test scompatta mail.
 */
public class ScompattaEmailTest extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ScompattaEmailTest.class);

	/**
	 * Messaggio di test.
	 */
	private static final String TEST_MESSAGE = "MESSAGGIO_TEST_SCOMPATTARE.msg";

	/**
	 * Serrvizio.
	 */
	private ScompattaMailSRV mailSRV;

	/**
	 * Informazioni uente.
	 */
	private UtenteDTO utente;

	/**
	 * Tipo dati.
	 */
	private Collection<TipoFile> allTipoDati;

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public void initialize() {
		before();
		mailSRV = getBean(ScompattaMailSRV.class);
		final IUtenteFacadeSRV facadeUtente = getBean(IUtenteFacadeSRV.class);
		utente = facadeUtente.getByUsername("BIAGIO.MAZZOTTA");
		allTipoDati = new ArrayList<>(3);
		allTipoDati.add(new TipoFile(1, "pdf", "application/pdf", 1, 1));
		allTipoDati.add(new TipoFile(1, "txt", "plain/txt", 1, 1));
		allTipoDati.add(new TipoFile(1, "html", "text/html", 1, 1));
	}

	/**
	 * Test scompattatore ricorsivo
	 * 
	 * @throws IOException
	 */
//	@Test
	public void testScompattatoreRicorsivo() throws IOException {
		final File file = new File("src/test/resources/doppio_allegato.msg");
		final byte[] array = IOUtils.toByteArray(new FileInputStream(file));
		final HashMap<String, byte[]> pathContentMailScompattata = new HashMap<>();

		final PkHandler pkHandler = utente.getSignerInfo().getPkHandlerVerifica();
		final HierarchicalFileWrapperDTO mail = mailSRV.recursiveScompattaMail(utente, 0, TEST_MESSAGE, array, allTipoDati, TEST_MESSAGE, pkHandler, pathContentMailScompattata);
		final String printed = printTree(mail, 0).toString();
		LOGGER.info(printed);
	}

	/**
	 * Test contenuto di scompattatore ricorsivo.
	 * 
	 * @throws IOException
	 */
	@Test
	public void testContenutoDiScompattatoreRicorsivo() throws IOException {
		final File file = new File("src/test/resources/doppio_allegato.msg");
		final byte[] array = IOUtils.toByteArray(new FileInputStream(file));

		final Deque<HierarchicalFileWrapperDTO> stack = mailSRV.parseHierarchicalFileWrapperDTONodeId("1:allegto_test.txt");

		final PkHandler pkHandler = utente.getSignerInfo().getPkHandlerVerifica();
		final byte[] content = mailSRV.recursiveGetContentsScompattaMail(utente, TEST_MESSAGE, array, stack, pkHandler);

		final String s = new String(content);

		LOGGER.info(s);

	}

	private static StringBuilder printTree(final HierarchicalFileWrapperDTO node, final int deep) {
		final StringBuilder toPrint = new StringBuilder(printTab(deep));

		if (node.isAttachmentSelectable()) {
			toPrint.append(createNodeDescription(node) + " mime_type: ");
			toPrint.append(node.getFile().getTipoFile().getMimeType());
		} else {
			toPrint.append(createNodeDescription(node) + " corretto: ");
			toPrint.append(node.getFile().getTipoFile() == null);
			for (final HierarchicalFileWrapperDTO son : node.getSons()) {
				toPrint.append("\n ");
				toPrint.append(printTree(son, deep + 1));
			}
		}
		return toPrint;
	}

	private static String createNodeDescription(final HierarchicalFileWrapperDTO node) {
		return "name: " + node.getFile().getName() + " nodeId: " + node.getNodeId() + " ";
	}

	private static String printTab(final int deep) {
		final StringBuilder tab = new StringBuilder("");
		for (int i = 0; i < deep; i++) {
			tab.append("\t");
		}
		return tab.toString();
	}
}
