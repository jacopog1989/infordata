package it.ibm.business.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import javax.security.auth.Subject;
import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.filenet.api.core.Connection;
import com.filenet.api.core.Factory;
import com.filenet.api.util.UserContext;

import filenet.vw.api.VWException;
import filenet.vw.api.VWFetchType;
import filenet.vw.api.VWQueue;
import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWRoster;
import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWSession;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.SourceTypeEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;

/**
 * Abstract classi di test.
 */
public abstract class AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AbstractTest.class.getName());

	/**
	 * Path file configurazione test.
	 */
	private static final String TEST_APPLICATION_CONTEXT_FILENAME = "it\\ibm\\business\\test\\applicationContextTest.xml";

	/**
	 * Datasource db appicativo.
	 */
	private DataSource dataSource;

	/**
	 * Datasouce db dwh.
	 */
	private DataSource dataSourceFilenet;

	/**
	 * Metodo eseguito per la data preparation.
	 */
	protected void before() {
		final ApplicationContext context = new ClassPathXmlApplicationContext(TEST_APPLICATION_CONTEXT_FILENAME);
		ApplicationContextProvider.setApplicationContextForTestPurpose(context);
		dataSource = (DataSource) ApplicationContextProvider.getApplicationContext().getBean("DataSource");
		dataSourceFilenet = (DataSource) ApplicationContextProvider.getApplicationContext()
				.getBean("FilenetDataSource");
	}

	/**
	 * Restituisce il bean a partire dalla classe {@code beanCls}.
	 * 
	 * @param beanCls
	 *            Classe del bean.
	 * @return Bean recuperato.
	 */
	protected <T> T getBean(final Class<T> beanCls) {
		return ApplicationContextProvider.getApplicationContext().getBean(beanCls);
	}

	/**
	 * Crea una nuova connessione al database restituendola.
	 * 
	 * @return Connessione al database.
	 */
	protected java.sql.Connection createNewDBFileneConnection() {
		return createDBConnection(dataSourceFilenet);
	}

	/**
	 * Crea una nuova connessione al database restituendola.
	 * 
	 * @return Connessione al database.
	 */
	protected java.sql.Connection createNewDBConnection() {
		return createDBConnection(dataSource);
	}

	/**
	 * Crea una nuova connessione al database a partire dal datasource: {@code ds}.
	 * 
	 * @param ds
	 *            Datasource per la creazione della connessione.
	 * @return Connessione al database creata.
	 */
	private static java.sql.Connection createDBConnection(final DataSource ds) {
		java.sql.Connection connection = null;
		try {
			connection = ds.getConnection();
			// Impedisce letture sporche: se una transazione in esecuzione modifica un
			// record questo non può essere letto da un'altra sino a che non avviene il
			// commit.
			connection.setTransactionIsolation(java.sql.Connection.TRANSACTION_READ_COMMITTED);
			// Richiede il commit esplicito a termine esecuzione.
			connection.setAutoCommit(false);
			return connection;
		} catch (final SQLException e) {
			LOGGER.error(e);
			closeConnection(connection);
			throw new RedException(e);
		}
	}

	/**
	 * Chiude la connessione verso la base dati.
	 * 
	 * @param connection
	 */
	public static void closeConnection(final java.sql.Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e1) {
				LOGGER.error(e1);
			}
		}
	}

	/**
	 * Esegue il rollback della connessione.
	 * 
	 * @param connection
	 *            Connessione su cui fare il rollback.
	 */
	protected void rollbackConnection(final java.sql.Connection connection) {
		try {
			connection.rollback();
			connection.close();
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		}
	}

	/**
	 * Esegue il commit della connessione.
	 * 
	 * @param connection
	 *            Connessione da committare.
	 */
	protected void commitConnection(final java.sql.Connection connection) {
		try {
			connection.commit();
			connection.close();
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		}
	}

	private Connection initUserContext(final String userId, final String password, final String uri,
			final String stanzaJAAS) {
		final UserContext uc = UserContext.get();
		final Connection con = Factory.Connection.getConnection(uri);
		final Subject sub = UserContext.createSubject(con, userId, password, stanzaJAAS);
		uc.pushSubject(sub);
		return con;
	}

	private VWSession getPESession(final String uri, final String userId, final String connectionPoint,
			final String password, final String stanzaJAAS) {
		VWSession pesession = null;
		try {
			initUserContext(userId, password, uri, stanzaJAAS);
			pesession = new filenet.vw.api.VWSession();
			pesession.setBootstrapCEURI(uri);
			pesession.logon(userId, password, connectionPoint);
		} catch (final VWException e) {
			LOGGER.error(e);
		}
		return pesession;
	}

	/**
	 * Restituisce la sessione PE.
	 * 
	 * @return PE Sessione.
	 */
	protected VWSession getPESession() {
		return getPESession("http://10.38.60.61:9080/wsi/FNCEWS40MTOM/", "P8ADMIN", "ConnPt3", "Password1",
				"FileNetP8WSI");
	}

	private static Collection<QueueInfo> executeRosterQuery(final VWSession session, final String rosterName,
			final String queryFilter) {
		final Collection<QueueInfo> output = new ArrayList<>();
		try {
			final VWRoster roster = session.getRoster(rosterName);
			final VWRosterQuery vwRQ = roster.createQuery(null, null, null, VWRoster.QUERY_NO_OPTIONS, queryFilter,
					null, VWFetchType.FETCH_TYPE_WORKOBJECT);
			while (vwRQ.hasNext()) {
				final VWWorkObject wf = (VWWorkObject) vwRQ.next();
				output.add(new QueueInfo(wf));
			}
		} catch (final VWException e) {
			LOGGER.error(e);
		}
		return output;
	}

	/**
	 * Esegue la query Roseter.
	 * 
	 * @param rosterName
	 *            Nome del Roster.
	 * @param queryFilter
	 *            Query filter.
	 * @return Queue Info recuperate.
	 */
	protected Collection<QueueInfo> executeRosterQuery(final String rosterName, final String queryFilter) {
		final VWSession session = getPESession();
		return executeRosterQuery(session, rosterName, queryFilter);
	}

	/**
	 * Esegue la query sulla coda {@code queueName}.
	 * 
	 * @param queueName
	 *            Nome Coda.
	 * @param queryFilter
	 *            Query filter.
	 * @return Queue Info recuperate.
	 */
	protected Collection<QueueInfo> executeQueueQuery(final String queueName, final String queryFilter) {
		final Collection<QueueInfo> output = new ArrayList<>();
		try {
			final VWSession session = getPESession();
			final VWQueue queueQ = session.getQueue(queueName);
			final VWQueueQuery query = queueQ.createQuery(null, null, null, VWRoster.QUERY_NO_OPTIONS, queryFilter,
					null, VWFetchType.FETCH_TYPE_WORKOBJECT);
			while (query.hasNext()) {
				final VWWorkObject wf = (VWWorkObject) query.next();
				output.add(new QueueInfo(wf));
			}
		} catch (final VWException e) {
			LOGGER.error(e);
		}
		return output;
	}

	/**
	 * Esegue la query su tutte le code.
	 * 
	 * @param queryFilter
	 *            Query filter.
	 * @return Queue info recuperate.
	 */
	protected Collection<QueueInfo> executeQueueQueryAllQueue(final String queryFilter) {
		final Collection<QueueInfo> output = new ArrayList<>();
		for (final DocumentQueueEnum q : DocumentQueueEnum.values()) {
			if (q.getType().equals(SourceTypeEnum.FILENET) && !"NSD_Roster".equalsIgnoreCase(q.getName())) {
				output.addAll(executeQueueQuery(q.getName(), queryFilter));
			}
		}
		return output;
	}

	/**
	 * Esegue la query su tutte le code.
	 * 
	 * @param utente
	 *            Utente in sessione.
	 * @param queryFilter
	 *            Query filter.
	 * @param response
	 *            Response.
	 * @return Queue Info recuperate.
	 */
	protected Collection<QueueInfo> executeQueueQueryAllQueue(final UtenteDTO utente, final String queryFilter,
			final String... response) {

		Collection<QueueInfo> output = new ArrayList<>();
		final Collection<QueueInfo> result = executeQueueQueryAllQueue(queryFilter);
		final StringBuilder sb = new StringBuilder("");
		for (final QueueInfo w : result) {
			sb.append(w.toString()).append("\n");
		}
		// create a temporary file
		final String timeLog = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		final File logFile = new File(timeLog);

		try (BufferedWriter writer = new BufferedWriter(new FileWriter(logFile));) {
			// This will output the full path where the file will be written to...
			LOGGER.info(logFile.getCanonicalPath());
			writer.write(sb.toString());
		} catch (final Exception e) {
			LOGGER.error(e);
		}

		if (response != null && response.length > 0) {
			for (final QueueInfo w : result) {
				for (int i = 0; i < w.getResponses().length; i++) {
					final String resp = w.getResponses()[i];
					for (int j = 0; j < response.length; j++) {
						final String r = response[j];
						if (resp.toLowerCase().contains(r.toLowerCase())) {
							output.add(w);
							break;
						}
					}
				}
			}
		} else {
			output = result;
		}
		return output;
	}

	/**
	 * Esegue il dump delle informazioni sul workflow.
	 * 
	 * @param data
	 *            Informazioni da mostrare in console.
	 */
	protected void dumpWFInfo(final Collection<QueueInfo> data) {
		for (final QueueInfo d : data) {
			LOGGER.info("--============--");
			LOGGER.info(d.toString());
			LOGGER.info("--============--");
		}
	}

	/**
	 * Restituisce l'utente identificato dall' {@code idUtente}.
	 * 
	 * @param idUtente
	 *            Identificativo dell'utente da recuperare.
	 * @param idNodo
	 *            Identificativo dell'ufficio dell'utente da recuperare.
	 * @return Utente recuperato.
	 */
	protected UtenteDTO getUtente(final Long idUtente, final Long idNodo) {

		final IUtenteFacadeSRV utenteSRV = getBean(IUtenteFacadeSRV.class);
		return utenteSRV.getById(idUtente, null, idNodo);

	}

}