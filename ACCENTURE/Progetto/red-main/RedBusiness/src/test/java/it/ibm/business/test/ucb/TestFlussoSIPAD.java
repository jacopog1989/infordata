package it.ibm.business.test.ucb;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.DocumentException;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.adobe.AdobeLCHelper;
import it.ibm.red.business.helper.html.HtmlFileHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IFlussoSipadFacadeSRV;
import it.ibm.red.business.utils.FileUtils;

/**
 * Test trasformazione xml in pdf per flusso SIPAD.
 * 
 * @author SimoneLungarella
 */
public class TestFlussoSIPAD extends AbstractUCBTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TestFlussoSIPAD.class);

	/**
	 * Errore generico conversione pdf.
	 */
	private static final String GENERIC_ERROR_MSG = "Errore nella conversione in PDF.";

	/**
	 * File path.
	 */
	private static final String FOLDER_PATH = "";

	/**
	 * Nome file xml.
	 */
	private static final String FILENAME_XML = "MASTROIANNIDEC.xml";

	/**
	 * Nome file xslt.
	 */
	private static final String FILENAME_XSLT = "decreto.xslt";

	/**
	 * Nome file pdf.
	 */
	private static final String FILENAME_PDF = "test.pdf";

	/**
	 * Nome file html.
	 */
	private static final String FILENAME_HTML = "dirty.html";

	/**
	 * Service per lagestione della trasformazione SIPAD.
	 */
	private IFlussoSipadFacadeSRV flussoSipadSRV;

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		flussoSipadSRV = ApplicationContextProvider.getApplicationContext().getBean(IFlussoSipadFacadeSRV.class);
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test completo trasformazione. Converte l'xml in html e successivamente in
	 * pdf.
	 */
	@Test
	public void fromXmlToPdf() {
		byte[] xml = FileUtils.getFileFromFS(FOLDER_PATH + FILENAME_XML);
		byte[] xslt = FileUtils.getFileFromFS(FOLDER_PATH + FILENAME_XSLT);

		LOGGER.info("Conversione xml -> html.");
		byte[] cleanHtml = convertToHTML(xml, xslt);

		// Fix su layout della tabella.
		cleanHtml = HtmlFileHelper.replaceTextFromProperties(cleanHtml);
		try {
			LOGGER.info("Conversione html -> pdf.");
			AdobeLCHelper adobeHelper = new AdobeLCHelper();
			InputStream pdfStream = adobeHelper.htmlToPdf(cleanHtml);
			LOGGER.info("Trasformazione completata.");

			byte[] pdf = new byte[pdfStream.available()];
			int bytes = pdfStream.read(pdf);

			if (bytes != -1) {
				LOGGER.info("Salvataggio nel file [" + FILENAME_PDF + "] nella folder [" + FOLDER_PATH + "]");
				FileUtils.saveToFile(pdf, FOLDER_PATH + FILENAME_PDF);
			}
		} catch (IOException ioe) {
			LOGGER.error(GENERIC_ERROR_MSG, ioe);
		}

	}

	/**
	 * Esegue la conversione dell'xml in un file <em> html </em>.
	 * 
	 * @param xml
	 *            content da trasformare
	 * @param xslt
	 *            trasformata da applicare
	 * @return byte array del file html
	 */
	private static byte[] convertToHTML(byte[] xml, byte[] xslt) {
		try {
			// Creazione Stream Sources
			StreamSource ssXML = new StreamSource(new ByteArrayInputStream(FileUtils.removeBOM(xml)));
			StreamSource ssXSLT = new StreamSource(new StringReader(new String(xslt)));
			final ByteArrayOutputStream out = new ByteArrayOutputStream();

			TransformerFactory transformerFactory = javax.xml.transform.TransformerFactory.newInstance();
			transformerFactory.setAttribute(Constants.ACCESS_EXTERNAL_DTD, "");
			transformerFactory.setAttribute(Constants.ACCESS_EXTERNAL_SCHEMA, "");

			Transformer transformer = transformerFactory.newTransformer(ssXSLT);

			// Trasformazione
			transformer.transform(ssXML, new StreamResult(out));
			out.flush();
			out.close();
			return out.toByteArray();
		} catch (Exception e) {
			throw new RedException(e);
		}
	}

	/**
	 * Esegue la trasformazione del documento <em> xhtml </em> in pdf. @see
	 * ITextRenderer.
	 * 
	 * @param xhtml
	 *            content da trasformare
	 * @return byte array del pdf generato
	 * @throws IOException
	 */
	private static byte[] convertToPdf(String xhtml) throws IOException {
		ITextRenderer iTextRenderer = new ITextRenderer();
		iTextRenderer.setDocumentFromString(xhtml);
		iTextRenderer.layout();
		ByteArrayOutputStream os = new ByteArrayOutputStream();

		try {
			iTextRenderer.createPDF(os);
		} catch (DocumentException e) {
			LOGGER.error(GENERIC_ERROR_MSG, e);
		} finally {
			os.close();
		}

		return os.toByteArray();
	}

	/**
	 * Esegue la trasformazione dell'xml in html effettuando la modifica della
	 * codifica da UTF-8BOM ad UTF-8.
	 */
	@Test
	public void fromXmlToHtml() {
		byte[] xml = FileUtils.getFileFromFS(FOLDER_PATH + FILENAME_XML);
		byte[] xslt = FileUtils.getFileFromFS(FOLDER_PATH + FILENAME_XSLT);
		LOGGER.info("Conversione xml -> html.");
		byte[] html = convertToHTML(xml, xslt);
		LOGGER.info("Salvataggio [" + FILENAME_HTML + "] in [" + FOLDER_PATH + "]");
		FileUtils.saveToFile(html, FOLDER_PATH + FILENAME_HTML);
	}

	/**
	 * Test trasformazione da html a pdf.
	 */
	@Test
	public void fromHtmlToPdf() {
		byte[] html = FileUtils.getFileFromFS(FOLDER_PATH + FILENAME_HTML);

		try {
			byte[] cleanHtml = HtmlFileHelper.quickFix(html, false);
			String temp = new String(cleanHtml, StandardCharsets.UTF_8);
			temp = temp.replace("fixed", "auto");
			LOGGER.info("Conversione html -> pdf.");
			InputStream pdfStream = new ByteArrayInputStream(convertToPdf(temp));
			LOGGER.info("Trasformazione completata.");
			byte[] pdf = new byte[pdfStream.available()];
			int bytes = pdfStream.read(pdf);

			if (bytes != -1) {
				LOGGER.info("Salvataggio [" + FILENAME_PDF + "] in [" + FOLDER_PATH + "]");
				FileUtils.saveToFile(pdf, FOLDER_PATH + FILENAME_PDF);
			}
		} catch (IOException ioe) {
			LOGGER.error(GENERIC_ERROR_MSG, ioe);
		}
	}

	/**
	 * Esegue la trasformazione completa per generare il pdf a partire dall'xslt e
	 * dall'xml. Utilizza il service @see FlussoSipadSRV.
	 */
	@Test
	public void sipadTransform() {
		byte[] xml = FileUtils.getFileFromFS(FOLDER_PATH + FILENAME_XML);
		byte[] xslt = FileUtils.getFileFromFS(FOLDER_PATH + FILENAME_XSLT);

		FileDTO filePdf = flussoSipadSRV.transformXmlForSipad(xml, xslt);

		checkAssertion(filePdf != null, "File non convertito correttamente.");
		if (filePdf != null && filePdf.getContent() != null && filePdf.getFileName() != null) {
			FileUtils.saveToFile(filePdf.getContent(), FOLDER_PATH + filePdf.getFileName());
			LOGGER.info("File pdf generato e salvato in: " + FOLDER_PATH + filePdf.getFileName());
		}
	}

}
