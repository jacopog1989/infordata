package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Test 11.
 */
public class Test11 extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test11.class);
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test 11.
	 */
	@Test
	public final void test() {

		//PIETROBONO CREA UN DOCUMENTO IN USCITA
		UtenteDTO utente = getUtente(USERNAME_PIETROBONO);
		String oggetto = createObj();
		String indiceFascicoloProcedimentale = "A";

		Boolean bRiservato = false;

		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(createDestinatarioEsterno(MezzoSpedizioneEnum.ELETTRONICO, ID_CONTATTO_GIANCARLO_ROSSI, ModalitaDestinatarioEnum.TO));

		List<AssegnazioneDTO> assegnazioni = null;
		List<AllegatoDTO> allegati = null;
		EsitoSalvaDocumentoDTO esitoUscita = createDocUscita(allegati, true, oggetto, utente, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, assegnazioni, destinatari, indiceFascicoloProcedimentale, MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD, FIRMA_RAGIONIERE, bRiservato, null);
		checkAssertion(esitoUscita.isEsitoOk(), "Errore in fase di creazione documento in uscita.");

		String dt = esitoUscita.getDocumentTitle();

		// MONSURRO: LIBRO FIRMA -> CORRIERE
		EsitoOperazioneDTO esitoSiglaInviaMazzotta = siglaInvia(USERNAME_MONSURRO, dt);
		checkAssertion(esitoSiglaInviaMazzotta.isEsito(), "Errore in fase di sigla invia.");
		
		waitSeconds(30);
		
		// NUNZI: CORRIERE -> LIBRO FIRMA
		EsitoOperazioneDTO esitoProcediNunzi = procedi(USERNAME_NUNZI, dt);
		checkAssertion(esitoProcediNunzi.isEsito(), "Errore in fase di procedi.");

		// TANZI: LIBRO FIRMA -> CORRIERE
		EsitoOperazioneDTO esitoSiglaInviaTanzi = siglaInvia(USERNAME_TANZI, dt);
		checkAssertion(esitoSiglaInviaTanzi.isEsito(), "Errore in fase di sigla invia.");

		// RUCCIA: CORRIERE -> LIBRO FIRMA
		EsitoOperazioneDTO esitoInviaFirmaDigitaleRuccia = inviaFirmaDigitale(USERNAME_RUCCIA, dt);
		checkAssertion(esitoInviaFirmaDigitaleRuccia.isEsito(), "Errore in fase di invia firma digitale.");

		waitSeconds(30);

		// RAGIONIERE: LIBRO FIRMA -> FIRMA AUTOGRAFA
		EsitoOperazioneDTO esitoFirmaAutRag = firmaAutografa(USERNAME_RAGIONIERE, dt);
		checkAssertion(esitoFirmaAutRag.isEsito(), "Errore in fase di firma autografa.");

		waitSeconds(30);

		// PIETROBONO: SPEDIZIONE -> CORRIERE
		EsitoOperazioneDTO esitoAssegnaUCP = assegnaUCP(USERNAME_PIETROBONO, dt);
		checkAssertion(esitoAssegnaUCP.isEsito(), "Errore in fase di assegna ad UCP.");

		// ASCI: SPEDIZIONE
		EsitoOperazioneDTO esitoRifiutoSpedizione = rifiutoSpedizione(USERNAME_ASCI, dt);
		checkAssertion(esitoRifiutoSpedizione.isEsito(), "Errore in fase di assegna ad UCP.");

		// PIETROBONO: SPEDIZIONE
		EsitoOperazioneDTO esitoSpedito = spedito(USERNAME_PIETROBONO, dt);
		checkAssertion(esitoSpedito.isEsito(), "Errore in fase di spedito.");

		LOGGER.info("DOCUMENT TITLE : " + dt);
		LOGGER.info("OGGETTO : " + oggetto);
		LOGGER.info("NUM DOC : " + getNumDoc(USERNAME_NUNZI, dt));

	}

}
