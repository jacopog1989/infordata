package it.ibm.business.test.suite;

import java.util.Calendar;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.RicercaResultDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.RicercaPerBusinessEntityEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IRicercaFacadeSRV;

/**
 * Classe di test ricerca nuova.
 */
public class RicercaNuovaTest extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaNuovaTest.class);

	/**
	 * Label TEST.
	 */
	private static final String TEST_LABEL = " TEST ";

	/**
	 * Oggetto predisposizione.
	 */
	private static final String PREDISPOSIZIONE_DOCUMENTI = " PREDISPOSIZIONE DOCUMENTI DI FINANZA PUBBLICA E DI BILANCIO  ";

	/**
	 * Messaggio qualsiasi per test.
	 */
	private static final String MESSAGGIO_BREVE_TEST = " La mia frase interessante  ";

	/**
	 * Anno di riferimento per la ricerca.
	 */
	private static final String SEARCH_YEAR = " 2019 ";

	/**
	 * Servizio.
	 */
	private IRicercaFacadeSRV ricercaSRV;

	/**
	 * Utente.
	 */
	private UtenteDTO mazzotta;

	/**
	 * Data.
	 */
	private Calendar startDate;

	/**
	 * Esegue azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		ricercaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaFacadeSRV.class);
		mazzotta = getUtente(USERNAME_MAZZOTTA);

		startDate = Calendar.getInstance();
	}

	/**
	 * Test ricerca documenti.
	 */
	@Test
	public final void testRicercaDocumenti() {
		final RicercaResultDTO resultsEsatta = ricercaSRV.ricercaRapidaUtente(TEST_LABEL, null, RicercaGenericaTypeEnum.ESATTA, RicercaPerBusinessEntityEnum.DOCUMENTI, false,
				mazzotta, false);
		final RicercaResultDTO resultsTutte = ricercaSRV.ricercaRapidaUtente(TEST_LABEL, null, RicercaGenericaTypeEnum.TUTTE, RicercaPerBusinessEntityEnum.DOCUMENTI, false,
				mazzotta, false);
		final RicercaResultDTO resultsQualsiasi = ricercaSRV.ricercaRapidaUtente(TEST_LABEL, null, RicercaGenericaTypeEnum.QUALSIASI, RicercaPerBusinessEntityEnum.DOCUMENTI, false,
				mazzotta, false);

		Assert.assertFalse(resultsEsatta.getDocumenti().isEmpty());
		Assert.assertFalse(resultsTutte.getDocumenti().isEmpty());
		Assert.assertFalse(resultsQualsiasi.getDocumenti().isEmpty());

		Assert.assertFalse(resultsEsatta.getDocumenti().size() < 99);
		Assert.assertFalse(resultsTutte.getDocumenti().size() < 99);
		Assert.assertFalse(resultsQualsiasi.getDocumenti().size() < 99);
	}

	/**
	 * Test ricerca documenti anno.
	 */
	@Test
	public final void testRicercaDocumentiAnno() {
		final RicercaResultDTO results = ricercaSRV.ricercaRapidaUtente("  ", 2019, RicercaGenericaTypeEnum.ESATTA, RicercaPerBusinessEntityEnum.DOCUMENTI, false, mazzotta, false);

		final RicercaResultDTO resultsFullText = ricercaSRV.ricercaRapidaUtente(SEARCH_YEAR, null, RicercaGenericaTypeEnum.ESATTA, RicercaPerBusinessEntityEnum.DOCUMENTI, false,
				mazzotta, false);

		final RicercaResultDTO resultsQualsiasi = ricercaSRV.ricercaRapidaUtente(SEARCH_YEAR, null, RicercaGenericaTypeEnum.QUALSIASI, RicercaPerBusinessEntityEnum.DOCUMENTI, false,
				mazzotta, false);

		Assert.assertFalse(results.getDocumenti().isEmpty());

		Assert.assertFalse(resultsFullText.getDocumenti().isEmpty());
		Assert.assertTrue(results.getDocumenti().size() <= resultsFullText.getDocumenti().size());
		Assert.assertTrue(resultsFullText.getDocumenti().size() <= resultsQualsiasi.getDocumenti().size());
	}

	/**
	 * Test ricerca documenti anno.
	 */
	@Test
	public final void testRicercaDocumentiQualsiasiAnno() {
		final RicercaResultDTO results = ricercaSRV.ricercaRapidaUtente(" We come from the land of the ice and snow  ", 2018, RicercaGenericaTypeEnum.ESATTA,
				RicercaPerBusinessEntityEnum.DOCUMENTI, false, mazzotta, false);

		final RicercaResultDTO resultsFreeText = ricercaSRV.ricercaRapidaUtente(" We come from the land of the ice and snow 2018 ", null, RicercaGenericaTypeEnum.TUTTE,
				RicercaPerBusinessEntityEnum.DOCUMENTI, false, mazzotta, false);

		Assert.assertFalse(results.getDocumenti().isEmpty());

		Assert.assertFalse(resultsFreeText.getDocumenti().isEmpty());
		Assert.assertTrue(results.getDocumenti().size() <= resultsFreeText.getDocumenti().size());
	}

	/**
	 * Test ricerca documenti full text.
	 */
	@Test
	public final void testRicercaDocumentiFullTextResearch() {
		final RicercaResultDTO results = ricercaSRV.ricercaRapidaUtente(MESSAGGIO_BREVE_TEST, 2019, RicercaGenericaTypeEnum.ESATTA, RicercaPerBusinessEntityEnum.DOCUMENTI, true,
				mazzotta, false);
		final RicercaResultDTO resultsTUTTE = ricercaSRV.ricercaRapidaUtente(MESSAGGIO_BREVE_TEST, 2019, RicercaGenericaTypeEnum.TUTTE, RicercaPerBusinessEntityEnum.DOCUMENTI, true,
				mazzotta, false);
		final RicercaResultDTO resultsQUALSIASI = ricercaSRV.ricercaRapidaUtente(MESSAGGIO_BREVE_TEST, 2019, RicercaGenericaTypeEnum.QUALSIASI,
				RicercaPerBusinessEntityEnum.DOCUMENTI, true, mazzotta, false);

		final RicercaResultDTO resultsFreeText = ricercaSRV.ricercaRapidaUtente(" Venite pure avanti, voi con il naso corto, signori imbellettati, io più non vi sopporto, ", 2019,
				RicercaGenericaTypeEnum.TUTTE, RicercaPerBusinessEntityEnum.DOCUMENTI, false, mazzotta, false);

		Assert.assertFalse(results.getDocumenti().isEmpty());

		Assert.assertFalse(resultsFreeText.getDocumenti().isEmpty());
		Assert.assertTrue(results.getDocumenti().size() == resultsFreeText.getDocumenti().size());
		Assert.assertTrue(resultsTUTTE.getDocumenti().size() >= resultsFreeText.getDocumenti().size());
		Assert.assertTrue(resultsQUALSIASI.getDocumenti().size() >= resultsFreeText.getDocumenti().size());
	}

	/**
	 * Test ricerca fascicoli.
	 */
	@Test
	public final void testRicercaFascicoli() {
		final RicercaResultDTO resultsFreeText = ricercaSRV.ricercaRapidaUtente(" 6859 ", null, RicercaGenericaTypeEnum.TUTTE, RicercaPerBusinessEntityEnum.FASCICOLI, false,
				mazzotta, false);

		final RicercaResultDTO resultsEmptyText = ricercaSRV.ricercaRapidaUtente(" 6859 ", 2019, RicercaGenericaTypeEnum.TUTTE, RicercaPerBusinessEntityEnum.FASCICOLI, false,
				mazzotta, false);

		Assert.assertFalse(resultsFreeText.getFascicoli().isEmpty());
		Assert.assertTrue(resultsEmptyText.getFascicoli().isEmpty());
	}

	/**
	 * Test ricerca fascicoli.
	 */
	@Test
	public final void testRicercaFascicoliAll() {
		final String oggetto = PREDISPOSIZIONE_DOCUMENTI;

		final RicercaResultDTO results = ricercaSRV.ricercaRapidaUtente(oggetto, 2018, RicercaGenericaTypeEnum.ESATTA, RicercaPerBusinessEntityEnum.FASCICOLI, true, mazzotta,
				false);
		final RicercaResultDTO resultsTUTTE = ricercaSRV.ricercaRapidaUtente(oggetto, 2018, RicercaGenericaTypeEnum.TUTTE, RicercaPerBusinessEntityEnum.FASCICOLI, true, mazzotta,
				false);
		final RicercaResultDTO resultsQUALSIASI = ricercaSRV.ricercaRapidaUtente(oggetto, 2018, RicercaGenericaTypeEnum.QUALSIASI, RicercaPerBusinessEntityEnum.FASCICOLI, true,
				mazzotta, false);

		final RicercaResultDTO resultsFreeText = ricercaSRV.ricercaRapidaUtente("6859", 2018, RicercaGenericaTypeEnum.TUTTE, RicercaPerBusinessEntityEnum.FASCICOLI, false, mazzotta,
				false);

		Assert.assertFalse(results.getFascicoli().isEmpty());

		Assert.assertFalse(resultsFreeText.getFascicoli().isEmpty());
		Assert.assertTrue(results.getFascicoli().size() >= resultsFreeText.getFascicoli().size());
		Assert.assertTrue(resultsTUTTE.getFascicoli().size() >= resultsFreeText.getFascicoli().size());
		Assert.assertTrue(resultsQUALSIASI.getFascicoli().size() >= resultsFreeText.getFascicoli().size());
	}

	/**
	 * Test ricerca fascicoli.
	 */
	@Test
	public final void testRicercaFascicoliTutte() {
		final String oggetto = PREDISPOSIZIONE_DOCUMENTI;
		final RicercaResultDTO resultsTUTTE = ricercaSRV.ricercaRapidaUtente(oggetto, 2018, RicercaGenericaTypeEnum.TUTTE, RicercaPerBusinessEntityEnum.FASCICOLI, true, mazzotta,
				false);

		Assert.assertFalse(resultsTUTTE.getFascicoli().isEmpty());
	}

	/**
	 * Test ricerca fascicoli qualsiasi.
	 */
	@Test
	public final void testRicercaFascicoliQUalsiasi() {
		final String oggetto = PREDISPOSIZIONE_DOCUMENTI;
		final RicercaResultDTO results2018QUALSIASI = ricercaSRV.ricercaRapidaUtente(oggetto, 2018, RicercaGenericaTypeEnum.QUALSIASI, RicercaPerBusinessEntityEnum.FASCICOLI, false,
				mazzotta, false);
		final RicercaResultDTO results2019QUALSIASI = ricercaSRV.ricercaRapidaUtente(oggetto, 2019, RicercaGenericaTypeEnum.QUALSIASI, RicercaPerBusinessEntityEnum.FASCICOLI, false,
				mazzotta, false);

		Assert.assertTrue(results2018QUALSIASI.getFascicoli().size() >= results2019QUALSIASI.getFascicoli().size());
	}

	/**
	 * Test ricerca documenti fascicoli.
	 */
	@Test
	public final void testRicercaDocumentiFascicoli() {
		final String oggetto = SEARCH_YEAR;
		final RicercaResultDTO results = ricercaSRV.ricercaRapidaUtente(oggetto, null, RicercaGenericaTypeEnum.QUALSIASI, RicercaPerBusinessEntityEnum.DOCUMENTI_FASCICOLI, false,
				mazzotta, false);

		Assert.assertFalse(results.getDocumenti().isEmpty());
		Assert.assertFalse(results.getFascicoli().isEmpty());
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
		final long last = Calendar.getInstance().getTimeInMillis() - this.startDate.getTimeInMillis();
		LOGGER.info("OPERAZIONE DURATA " + last);
	}
}
