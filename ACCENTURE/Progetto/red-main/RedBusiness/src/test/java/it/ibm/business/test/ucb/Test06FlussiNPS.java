package it.ibm.business.test.ucb;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.NotImplementedException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.business.test.nps.interfaccianotifiche.GenericFault;
import it.ibm.business.test.nps.interfaccianotifiche.InterfacciaNotificheNPS;
import it.ibm.business.test.nps.interfaccianotifiche.InterfacciaNotificheNPSFlusso;
import it.ibm.business.test.nps.interfaccianotifiche.SecurityFault;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.INpsConfigurationDAO;
import it.ibm.red.business.dto.AnagraficaDipendenteDTO;
import it.ibm.red.business.dto.AnagraficaDipendentiComponentDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.LookupTableDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.dto.SelettoreCapitoliDTO;
import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.AUTMessageIDEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.PostaEnum;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.builder.Builder;
import it.ibm.red.business.nps.dto.PersonaFisicaDTO;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.NpsConfiguration;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.WebServiceClientProvider;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.IRubricaSRV;
import it.ibm.red.business.service.ITipoProcedimentoSRV;
import it.ibm.red.business.service.ITipologiaDocumentoSRV;
import it.ibm.red.business.service.facade.ITipoProcedimentoFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.CodiceAOO;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.CodiceAmministrazione;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.ContestoProcedurale;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.Identificativo;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.TipoContestoProcedurale;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioProtocollazioneAutomaticaType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioProtocollazioneFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraNotificaAzioneType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.AllChiaveEsternaType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.AllCodiceDescrizioneType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.AnaPersonaFisicaType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.EmailIndirizzoEmailType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.EmailMessageType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.IdentificativoProtocolloRequestType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.OrgAmministrazioneType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.OrgAooType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.OrgCasellaEmailType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.OrgOrganigrammaAooType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.OrgOrganigrammaType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.OrgOrganigrammaUnitaOrganizzativaType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.ProtIdentificatoreProtocolloCollegatoType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.ProtIdentificatoreProtocolloType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.ProtRegistroProtocolloType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.ProtTipoProtocolloType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.WkfAzioneAggiornamentoCollegatiType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.WkfAzioneRispondiAProtocolloType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.WkfDatiContestoProceduraleType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.WkfIdentificatoreProtcolloType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.WkfTipoNotificaAzioneType;
import it.ibm.red.webservice.model.interfaccianps.npstypesestesi.MetaDatoAssociato;
import it.ibm.red.webservice.model.interfaccianps.npstypesestesi.TIPOLOGIA;
import npstypes.v1.it.gov.mef.ProtDestinatarioType;

/**
 * Classe di test per i flussi - Lato NPS.
 */
public class Test06FlussiNPS extends AbstractUCBTest {

	/**
	 * Errore chiusura connessione.
	 */
	private static final String ERRORE_CHIUSURA_CONNESSIONE = "Errore chiusura connessione.";

	/**
	 * Errore creazione data registrazione.
	 */
	private static final String ERRORE_NELLA_CREAZIONE_DELLA_DATA_REGISTRAZIONE = "Errore nella creazione della data registrazione.";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test06FlussiNPS.class.getName());

	/**
	 * Oggetto documento di protocollazione automatica.
	 */
	private static final String OGGETTO_PROT_AUTO = "Test protocollazione automatica";
	
	/**
	 * Suffisso valori mock.
	 */
	private static final String SUFFIX = "_value";
	
	/**
	 * Capitolo spesa valido per UCB_LAV.
	 */
	private static final String CAP_SPESA_UCB_LAV = "2552/01";
	
	/**
	 * Messaggio generico di errore in fase di protocollazione automatica.
	 */
	private static final String ERROR_PROTOCOLLAZIONE_AUTOMATICA_MSG = "Errore elaborazione messaggio protocollazione automatica.";
	
	/**
	 * Prefisso oggetto di test.
	 */
	private static final String OGGETTO_PREFIX = "Test flusso: ";
	
	/**
	 * Email valida per contatto NPS.
	 */
	private static final String MAIL_CONTATTO_NPS = "test.nps1@pec.mef.gov.it";

	/**
	 * Endpoint collaudo - Notifiche NPS.
	 */
	private static final String ENDPOINT_NOTIFICHE_COLL_NPS = "http://redevoucb-coll.mef.gov.it/RedEvoWSWeb/Interfaccia_NotificheNPS?wsdl";
	
	/**
	 * Endpoint Notifiche collaudo Flusso NPS.
	 */
	private static final String ENDPOINT_NOTIFICHE_FLUSSO_COLL_NPS = "http://redevoucb-coll.mef.gov.it/RedEvoWSWeb/Interfaccia_NotificheNPS_Flusso?wsdl";
	
	/**
	 * Service NPS.
	 */
	private INpsSRV npsSRV;
	
	/**
	 * Service per la gestione dei contatti.
	 */
	private IRubricaSRV rubricaSrv;
	
	/**
	 * Service per la gestione delle tipologie documento.
	 */
	private ITipologiaDocumentoSRV tipologiaDocumentoSrv;
	
	/**
	 * Service per la gestione delle tipologie procedimento.
	 */
	private ITipoProcedimentoFacadeSRV tipoProcedimentoSrv;
	
	/**
	 * Dao per la gestione delle configurazioni NPS.
	 */
	private INpsConfigurationDAO npsConfigurationDAO;
	
	/**
	 * Dao per la gestione degli uffici.
	 */
	private INodoDAO nodoDAO;
	
	/**
	 * Indirizzo mail generico.
	 */
	private static final String MAIL_TEST = "test@mail.it";
	
	/**
	 * Path content documento.
	 */
	private static final String CONTENT_PATH = "C:\\Users\\Administrator\\Downloads\\PDF_FLUSSI_UCB\\";
	
	/**
	 * Nome file documento.
	 */
	private static final String CONTENT_FILENAME = "prova.pdf";
	
	/**
	 * Path content documento.
	 */
	private static final String CONTENT_PATH_1 = "C:\\Users\\Administrator\\Downloads\\PDF_FLUSSI_UCB\\";
	
	/**
	 * Nome file documento.
	 */
	private static final String CONTENT_FILENAME_1 = "prova1.pdf";
	
	/**
	 * Path content documento.
	 */
	private static final String CONTENT_PATH_ALL = "C:\\Users\\Administrator\\Downloads\\PDF_FLUSSI_UCB\\";
	
	/**
	 * Nome file documento.
	 */
	private static final String CONTENT_FILENAME_ALL = "provaAll.pdf";
	
	/**
	 * nome file email.
	 */
	private static final String MAIL_FILENAME = "email.eml";
	
	/**
	 * Path email.
	 */
	private static final String MAIL_PATH = "C:\\Users\\Administrator\\Downloads\\PDF_FLUSSI_UCB\\";
	
	/**
	 * nome file email.
	 */
	private static final String MAIL_FILENAME_1 = "email1.eml";
	
	/**
	 * Path email.
	 */
	private static final String MAIL_PATH_1 = "C:\\Users\\Administrator\\Downloads\\PDF_FLUSSI_UCB\\";
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		npsSRV = ApplicationContextProvider.getApplicationContext().getBean(INpsSRV.class);
		rubricaSrv = ApplicationContextProvider.getApplicationContext().getBean(IRubricaSRV.class);
		tipologiaDocumentoSrv = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoSRV.class);
		tipoProcedimentoSrv = ApplicationContextProvider.getApplicationContext().getBean(ITipoProcedimentoSRV.class);
		npsConfigurationDAO = ApplicationContextProvider.getApplicationContext().getBean(INpsConfigurationDAO.class);
		nodoDAO = ApplicationContextProvider.getApplicationContext().getBean(INodoDAO.class);
		WebServiceClientProvider.getIstance();
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Esegue l'iter completo di protocollazione automatica.
	 */
	@Test
	public final void protocollazioneAutomatica() {
		
		AooTest aooToTest = AooTest.UCB_MAE;
		
		EmailProtocollo creaEmailProtocollo = creaEmailProtocollo(aooToTest, MAIL_FILENAME, MAIL_PATH, Constants.ContentType.EML, 
				CONTENT_FILENAME, CONTENT_PATH, Constants.ContentType.PDF, CONTENT_FILENAME_ALL, CONTENT_PATH_ALL, Constants.ContentType.PDF, OGGETTO_PROT_AUTO, 
				aooToTest.idTipoDocGen, aooToTest.idTipoProcGen);
		
		// Notifica protocollazione automatica
		inviaNotificaNPSProtocollazioneAutomatica(ENDPOINT_NOTIFICHE_COLL_NPS, creaEmailProtocollo.getProtocolloCreato(), creaEmailProtocollo.getIdEmail(), aooToTest.codiceAoo);
	
	}
	
	/**
	 * Esegue l'iter completo di protocollazione flusso ordinario.
	 */
	@Test
	public final void flussoOrdinario() {
		
		startFlusso(TipoContestoProceduraleEnum.ORD, AooTest.UCB_MISE);
	
	}
	
	/**
	 * Esegue l'iter completo di protocollazione flusso silice.
	 */
	@Test
	public final void flussoSilice() {
		
		startFlusso(TipoContestoProceduraleEnum.SILICE, AooTest.UCB_MISE);
	
	}
	
	/**
	 * Esegue l'iter completo di protocollazione flusso ordinario/silice.
	 */
	private final void startFlusso(TipoContestoProceduraleEnum tcp, AooTest rifAoo) {
		
		EmailProtocollo creaEmailProtocollo = creaEmailProtocollo(rifAoo, MAIL_FILENAME, MAIL_PATH, Constants.ContentType.EML, 
				CONTENT_FILENAME, CONTENT_PATH, Constants.ContentType.PDF, CONTENT_FILENAME_ALL, CONTENT_PATH_ALL, Constants.ContentType.PDF, OGGETTO_PREFIX + tcp, 
				rifAoo.idDocAsav, rifAoo.idProcAsav);

		// Notifica protocollazione automatica
		inviaNotificaNPSFlusso(ENDPOINT_NOTIFICHE_FLUSSO_COLL_NPS, creaEmailProtocollo.getProtocolloCreato(), creaEmailProtocollo.getIdEmail(), 
				tcp, rifAoo.idDocAsav, rifAoo.idProcAsav, rifAoo.descAsav, rifAoo);
	
	}
	
	/**
	 * Esegue l'iter completo di protocollazione flusso AUT.
	 */
	@Test
	public final void flussoAUT() {
		
		AooTest aooToTest = AooTest.UCB_MAE;
		
		startFlussoAUT(aooToTest, TipoContestoProceduraleEnum.FLUSSO_AUT, aooToTest.idDocContoConsuntivo, aooToTest.idProcContoConsuntivo, aooToTest.descContoConsuntivo);
	
	}
	
	/**
	 * Esegue l'iter completo di flusso AUT agganciato ad una notifica.
	 */
	@Test
	public final void flussoAUTNotifica() {
		
		AooTest aooToTest = AooTest.UCB_MAE;
		
		// Creazione Notifica
		EmailProtocollo notifica= creaEmailProtocollo(aooToTest, MAIL_FILENAME, MAIL_PATH, Constants.ContentType.EML, CONTENT_FILENAME, CONTENT_PATH, Constants.ContentType.PDF, 
				CONTENT_FILENAME_ALL, CONTENT_PATH_ALL, Constants.ContentType.PDF, OGGETTO_PREFIX + TipoContestoProceduraleEnum.ORD, aooToTest.idDocNotifica, aooToTest.idProcNotifica);
		
		
		inviaNotificaNPSFlusso(ENDPOINT_NOTIFICHE_FLUSSO_COLL_NPS, notifica.getProtocolloCreato(), notifica.getIdEmail(), 
				TipoContestoProceduraleEnum.ORD, aooToTest.idDocNotifica, aooToTest.idProcNotifica, aooToTest.descNotifica, aooToTest);
		
		// Creazione protocollo flsso AUT con riferimento alla notifica
		EmailProtocollo protocolloConNotifica = creaEmailProtocollo(aooToTest, MAIL_FILENAME, MAIL_PATH, Constants.ContentType.EML, CONTENT_FILENAME, CONTENT_PATH, Constants.ContentType.PDF, 
				CONTENT_FILENAME_ALL, CONTENT_PATH_ALL, Constants.ContentType.PDF, OGGETTO_PREFIX + TipoContestoProceduraleEnum.FLUSSO_AUT, aooToTest.idDocAsav, aooToTest.idProcAsav);
		
		String identificatoreProcesso = java.util.UUID.randomUUID().toString();
		inviaNotificaNPSFlussoAUT(ENDPOINT_NOTIFICHE_FLUSSO_COLL_NPS, protocolloConNotifica.getProtocolloCreato(), protocolloConNotifica.getIdEmail(), TipoContestoProceduraleEnum.FLUSSO_AUT,
				aooToTest.idDocAsav, aooToTest.idProcAsav, aooToTest.descAsav, aooToTest, 
				AUTMessageIDEnum.MESSAGGIO_INIZIALE, identificatoreProcesso, notifica.getProtocolloCreato());
		
	}
	
	/**
	 * Esegue l'iter completo di protocollazione flusso CG2.
	 */
	@Test
	public final void flussoCG2() {
		AooTest aooToTest = AooTest.UCB_MAE;
		
		EmailProtocollo mailProtocollo= creaEmailProtocollo(aooToTest, MAIL_FILENAME, MAIL_PATH, Constants.ContentType.EML, 
				CONTENT_FILENAME, CONTENT_PATH, Constants.ContentType.PDF, CONTENT_FILENAME_ALL, CONTENT_PATH_ALL, Constants.ContentType.PDF, OGGETTO_PREFIX + TipoContestoProceduraleEnum.FLUSSO_CG2, 
				aooToTest.idDocCG, aooToTest.idProcCG);
		
		// Notifica protocollazione automatica
		String identificatoreProcesso = java.util.UUID.randomUUID().toString();
		inviaNotificaNPSFlussoAUT(ENDPOINT_NOTIFICHE_FLUSSO_COLL_NPS, mailProtocollo.getProtocolloCreato(), mailProtocollo.getIdEmail(), 
				TipoContestoProceduraleEnum.FLUSSO_CG2, aooToTest.idDocCG, aooToTest.idProcCG, aooToTest.descCG, aooToTest, 
				AUTMessageIDEnum.MESSAGGIO_INIZIALE, identificatoreProcesso, null);
		
	}

	/**
	 * Esegue l'invio della notifica ai aggiornamento protocolli collegati inerente il protocollo di risposta di SIPATR. 
	 */
	@Test
	public final void flussoSipatr() {
		
		AooTest aooToTest = AooTest.UCB_MISE;
		
		//crea 1° documento in ingresso
		EmailProtocollo creaEmailProtocollo = creaEmailProtocollo(aooToTest, MAIL_FILENAME, MAIL_PATH, Constants.ContentType.EML, 
				CONTENT_FILENAME, CONTENT_PATH, Constants.ContentType.PDF, CONTENT_FILENAME_ALL, CONTENT_PATH_ALL, Constants.ContentType.PDF, OGGETTO_PROT_AUTO, 
				aooToTest.idTipoDocGen, aooToTest.idTipoProcGen);
		
		// Notifica protocollazione automatica
		inviaNotificaNPSProtocollazioneAutomatica(ENDPOINT_NOTIFICHE_COLL_NPS, creaEmailProtocollo.getProtocolloCreato(), creaEmailProtocollo.getIdEmail(), aooToTest.codiceAoo);
	
		//crea 2° documento in ingresso
		EmailProtocollo creaEmailProtocollo1 = creaEmailProtocollo(aooToTest, MAIL_FILENAME_1, MAIL_PATH_1, Constants.ContentType.EML, 
				CONTENT_FILENAME_1, CONTENT_PATH_1, Constants.ContentType.PDF, CONTENT_FILENAME_ALL, CONTENT_PATH_ALL, Constants.ContentType.PDF, OGGETTO_PROT_AUTO, 
				aooToTest.idTipoDocGen, aooToTest.idTipoProcGen);
		
		// Notifica protocollazione automatica
		inviaNotificaNPSProtocollazioneAutomatica(ENDPOINT_NOTIFICHE_COLL_NPS, creaEmailProtocollo1.getProtocolloCreato(), creaEmailProtocollo1.getIdEmail(), aooToTest.codiceAoo);
		
		//creazione protocollo uscita
		EmailProtocollo creaProtocolloUscita = creaProtocolloUscita(aooToTest, CONTENT_FILENAME, CONTENT_PATH, Constants.ContentType.PDF, 
				CONTENT_FILENAME_1, CONTENT_PATH_1, Constants.ContentType.PDF, CONTENT_FILENAME_ALL, CONTENT_PATH_ALL, Constants.ContentType.PDF, 
				"Creazione risposta automatica", aooToTest.codiceTitolario, aooToTest.descrizioneTitolario);
		
		//invio notifica azione AGGIORNA_COLLEGATI
		try {
			inviaNotificaAzioneNPSFlusso(ENDPOINT_NOTIFICHE_COLL_NPS, creaProtocolloUscita.getProtocolloCreato(), 
					creaEmailProtocollo.getProtocolloCreato(), creaEmailProtocollo1.getProtocolloCreato(), aooToTest.firmatario, WkfTipoNotificaAzioneType.AGGIORNAMENTO_COLLEGATI);
		} catch (Exception e) {
			LOGGER.error("Errore riscontrato durante l'invio della notifica azione.", e);
		}

	}
	
	/**
	 * Esegue l'integrazione dati di NPS.
	 */
	@Test
	public final void integrazioneDatiAUT() {
		
		AooTest aooToTest = AooTest.UCB_MISE;
		
		EmailProtocollo mailProtocollo = creaEmailProtocollo(aooToTest, MAIL_FILENAME, MAIL_PATH,
				Constants.ContentType.EML, CONTENT_FILENAME, CONTENT_PATH, Constants.ContentType.PDF,
				CONTENT_FILENAME_ALL, CONTENT_PATH_ALL, Constants.ContentType.PDF,
				OGGETTO_PREFIX + TipoContestoProceduraleEnum.FLUSSO_AUT, aooToTest.idDocAsav, aooToTest.idProcAsav);
		
		// Notifica protocollazione automatica
		String identificatoreProcesso = java.util.UUID.randomUUID().toString();
		inviaNotificaNPSFlussoAUT(ENDPOINT_NOTIFICHE_FLUSSO_COLL_NPS, mailProtocollo.getProtocolloCreato(), mailProtocollo.getIdEmail(), 
				TipoContestoProceduraleEnum.FLUSSO_AUT, aooToTest.idDocAsav, aooToTest.idProcAsav, aooToTest.descAsav, aooToTest, 
				AUTMessageIDEnum.MESSAGGIO_INIZIALE, identificatoreProcesso, null);
		
		EmailProtocollo mailIntegrazioneDati = creaEmailProtocollo(aooToTest, MAIL_FILENAME, MAIL_PATH,
				Constants.ContentType.EML, CONTENT_FILENAME, CONTENT_PATH, Constants.ContentType.PDF,
				CONTENT_FILENAME_ALL, CONTENT_PATH_ALL, Constants.ContentType.PDF,
				OGGETTO_PREFIX + TipoContestoProceduraleEnum.FLUSSO_AUT, ID_TIPO_DOC_ATTI_SOGGETTI_VISTO_ENTRATA,
				ID_TIPO_PROC_ATTI_SOGGETTI_VISTO_ENTRATA);

		inviaNotificaNPSFlussoAUT(ENDPOINT_NOTIFICHE_FLUSSO_COLL_NPS, mailIntegrazioneDati.getProtocolloCreato(), mailIntegrazioneDati.getIdEmail(), 
				TipoContestoProceduraleEnum.FLUSSO_AUT, aooToTest.idDocAsav, aooToTest.idProcAsav, aooToTest.descAsav, aooToTest, 
				AUTMessageIDEnum.DATI_INTEGRATIVI_SPONTANEI, identificatoreProcesso, null);
		
	}
	
	/**
	 * Esegue l'azione Rispondi A sul documento del flusso sicoge - Da sollecitare
	 * dopo l'esecuzione delle request 1001 e 1002 che vanno a creare i documenti
	 * iniziali e i documenti aggiuntivi rispettivamente.
	 */
	@Test
	public final void rispondiASicoge () {
		
		AooTest aooToTest = AooTest.UCB_LAV;
		
		UtenteDTO utente = getUtente(aooToTest.firmatario);
		Connection connection = createNewDBConnection();
		NpsConfiguration npsConf = npsConfigurationDAO.getByIdAoo(utente.getIdAoo().intValue(), connection);
		
		// Recupero documentazione iniziale
		ProtocolloNpsDTO docIniziale = new ProtocolloNpsDTO();
		docIniziale.setNumeroProtocollo(100105);
		docIniziale.setIdProtocollo("9B0B44B2-2D47-4601-E053-0A64CB60BA2E");
		docIniziale.setDataProtocollo(DateUtils.parseDate("22/10/2020"));
		docIniziale.setAnnoProtocollo("2020");
		docIniziale.setCodiceAoo(aooToTest.codiceAoo);
		
		PersonaFisicaDTO protocollatore = new PersonaFisicaDTO();
		protocollatore.setNomePF(utente.getNome());
		protocollatore.setCognomePF(utente.getCognome());
		protocollatore.setCodiceFiscalePF(utente.getCodFiscale());
		
		docIniziale.setProtocollatore(protocollatore);
		
		// Creazione protocollo uscita
		ProtocolloNpsDTO creaProtocolloUscita = getProtocolloUscita("Creazione risposta automatica - RISPONDI A", aooToTest.idTipoDocGen,
				aooToTest.idTipoProcGen, aooToTest.codiceTitolario, aooToTest.descrizioneTitolario, utente, connection, npsConf, "virred3108");
		
		// Invio notifica azione RISPONDI A
		try {
			inviaNotificaAzioneNPSFlusso(ENDPOINT_NOTIFICHE_COLL_NPS, creaProtocolloUscita, docIniziale, null, aooToTest.firmatario, WkfTipoNotificaAzioneType.RISPONDI_A);
		} catch (Exception e) {
			LOGGER.error("Errore riscontrato durante l'invio della notifica azione: RISPONDI A", e);
		}
	
		LOGGER.info("Invio notifica azione completato.");
	}
	
	/**
	 * Esegue l'iter completo di protocollazione flusso aut/cg2.
	 * @param tcp
	 * @param idTipoDOc
	 * @param idTipoProc
	 * @param docDesc
	 * @return
	 */
	private final String startFlussoAUT(AooTest riferimentoAoo, TipoContestoProceduraleEnum tcp, Integer idTipoDOc, Integer idTipoProc, String docDesc) {
		
		EmailProtocollo creaEmailProtocollo = creaEmailProtocollo(riferimentoAoo, MAIL_FILENAME, MAIL_PATH,
				Constants.ContentType.EML, CONTENT_FILENAME, CONTENT_PATH, Constants.ContentType.PDF,
				CONTENT_FILENAME_ALL, CONTENT_PATH_ALL, Constants.ContentType.PDF, OGGETTO_PREFIX + tcp, idTipoDOc,
				idTipoProc);
		
		// Notifica protocollazione automatica
		String identificatoreProcesso = java.util.UUID.randomUUID().toString();
		inviaNotificaNPSFlussoAUT(ENDPOINT_NOTIFICHE_FLUSSO_COLL_NPS, creaEmailProtocollo.getProtocolloCreato(), creaEmailProtocollo.getIdEmail(), 
				tcp, idTipoDOc, idTipoProc, docDesc, riferimentoAoo,
				AUTMessageIDEnum.MESSAGGIO_INIZIALE, identificatoreProcesso, null);
		
		return identificatoreProcesso;
	
	}

	/**
	 * Creazione e invio della notifica.
	 * @param endpointNPS
	 * @param protocolloCreato
	 * @param idMail
	 */
	private void inviaNotificaNPSProtocollazioneAutomatica(final String endpointNPS, ProtocolloNpsDTO protocolloCreato, String idMail, String codiceAoo) {
		InterfacciaNotificheNPS interfaccia = createInterfacciaNotificheNPSClient(endpointNPS);
		it.ibm.red.webservice.model.interfaccianps.npsmessages.ObjectFactory of = new it.ibm.red.webservice.model.interfaccianps.npsmessages.ObjectFactory();
		it.ibm.red.webservice.model.interfaccianps.npstypes.ObjectFactory ofTypes = new it.ibm.red.webservice.model.interfaccianps.npstypes.ObjectFactory();
		RichiestaElaboraMessaggioProtocollazioneAutomaticaType parameters = of.createRichiestaElaboraMessaggioProtocollazioneAutomaticaType();

		// Creazione mail
		EmailMessageType email = createEmailObj(ofTypes, codiceAoo);
		
		// Creazione protocollo
		WkfIdentificatoreProtcolloType protocollo = createProtocolloObj(protocolloCreato, ofTypes, codiceAoo);
		
		// Valorizzazione Parameters
		parameters.setIdDocumento(idMail);
		parameters.setIdMessaggio(java.util.UUID.randomUUID().toString());
		parameters.setMessaggioRicevuto(email);
		parameters.setProtocollo(protocollo);
		try {
			interfaccia.elaboraMessaggioProtocollazioneAutomatica(createAccessoApplicativo(), parameters);
		} catch (GenericFault | SecurityFault e) {
			LOGGER.error(ERROR_PROTOCOLLAZIONE_AUTOMATICA_MSG, e);
		}
	}
	
	/**
	 * Creazione e invio della notifica.
	 * @param endpointNPS
	 * @param protocolloCreato
	 * @param idMail
	 * @param tcp
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param tipologiaDocumentoDesc
	 * @param idAoo
	 */
	private void inviaNotificaNPSFlusso(final String endpointNPS, ProtocolloNpsDTO protocolloCreato, String idMail, TipoContestoProceduraleEnum tcp,
			Integer idTipologiaDocumento, Integer idTipoProcedimento, String tipologiaDocumentoDesc, AooTest rifAoo) {
		InterfacciaNotificheNPSFlusso interfaccia = createInterfacciaNotificheNPSFlussoClient(endpointNPS);
		it.ibm.red.webservice.model.interfaccianps.npsmessages.ObjectFactory of = new it.ibm.red.webservice.model.interfaccianps.npsmessages.ObjectFactory();
		it.ibm.red.webservice.model.interfaccianps.npstypes.ObjectFactory ofTypes = new it.ibm.red.webservice.model.interfaccianps.npstypes.ObjectFactory();
		it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.ObjectFactory ofCP = new it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.ObjectFactory();
		it.ibm.red.webservice.model.interfaccianps.npstypesestesi.ObjectFactory ofDatiEstesi = new it.ibm.red.webservice.model.interfaccianps.npstypesestesi.ObjectFactory();
		
		RichiestaElaboraMessaggioProtocollazioneFlussoType parameters = of.createRichiestaElaboraMessaggioProtocollazioneFlussoType();
		
		// Creazione mail
		EmailMessageType email = createEmailObj(ofTypes, rifAoo.codiceAoo);
		
		// Creazione protocollo
		WkfIdentificatoreProtcolloType protocollo = createProtocolloObj(protocolloCreato, ofTypes, rifAoo.codiceAoo);
		
		// Valorizzazione Parameters
		String idMessaggio = AUTMessageIDEnum.MESSAGGIO_INIZIALE.getId().toString();
		parameters.setIdDocumento(idMail);
		parameters.setIdMessaggio(idMessaggio);
		parameters.setMessaggioRicevuto(email);
		parameters.setProtocollo(protocollo);
		parameters.setCodiceFlusso(tcp.getId());
		parameters.setIdentificatoreProcesso(java.util.UUID.randomUUID().toString());

		WkfDatiContestoProceduraleType contestoProcedurale = ofTypes.createWkfDatiContestoProceduraleType();
		contestoProcedurale.setIdentificativoMessaggio(idMessaggio);
		
		ContestoProcedurale cp = ofCP.createContestoProcedurale();
		
		TipoContestoProcedurale tipoCP = ofCP.createTipoContestoProcedurale();
		tipoCP.setContent(tcp.getId());
		cp.setTipoContestoProcedurale(tipoCP);
		CodiceAmministrazione codiceAmministrazione = ofCP.createCodiceAmministrazione();
		codiceAmministrazione.setContent("m_ae");
		cp.setCodiceAmministrazione(codiceAmministrazione );
		CodiceAOO codiceAOO = ofCP.createCodiceAOO();
		codiceAOO.setContent(rifAoo.codiceAoo);
		cp.setCodiceAOO(codiceAOO );
		Identificativo identificativoMessaggio = ofCP.createIdentificativo();
		identificativoMessaggio.setContent(AUTMessageIDEnum.MESSAGGIO_INIZIALE.getId().toString());
		cp.setIdentificativo(identificativoMessaggio );
		contestoProcedurale.setContestoProcedurale(cp);
		
		// Impostazione dei metadati estesi.
		String tipologiaDocumentoNPS = npsSRV.getTipologiaDocumentoNPS(idTipologiaDocumento, idTipoProcedimento, tipologiaDocumentoDesc, rifAoo.idAoo);
		List<MetadatoDTO> metadatiEstesi = tipologiaDocumentoSrv.caricaMetadatiEstesiPerGUI(idTipologiaDocumento, idTipoProcedimento, true, rifAoo.idAoo, null);
		TIPOLOGIA datiEstesi = ofDatiEstesi.createTIPOLOGIA();
		datiEstesi.setFAMIGLIA(0);
		datiEstesi.setNOME(tipologiaDocumentoNPS);
		datiEstesi.setVERSIONE(0);
		
		for(MetadatoDTO m: metadatiEstesi) {
			
			MetaDatoAssociato mNPS = ofDatiEstesi.createMetaDatoAssociato();
			
			mNPS.setCodice(m.getName());
			
			String value = m.getName() + SUFFIX;
			switch (m.getType()) {
			case CAPITOLI_SELECTOR:
				value = CAP_SPESA_UCB_LAV;
				break;
			case DATE:
				value = DateUtils.dateToString(new Date(), true);
				break;
			case LOOKUP_TABLE:
				LookupTableDTO mLook = (LookupTableDTO)m;
				value = mLook.getLookupValues().get(0).getDescription();
				break;
			case PERSONE_SELECTOR:
				value = "<AnagraficaDipendente><CodiceFiscaleDipendente>ABDBDG56H78H501T</CodiceFiscaleDipendente><NomeDipendente>nome</NomeDipendente><CognomeDipendente>cognome</CognomeDipendente><ComuneNascitaDipendente>roma</ComuneNascitaDipendente><SessoDipendente>M</SessoDipendente><DataNascitaDipendente>15/04/1987</DataNascitaDipendente></AnagraficaDipendente>";
				break;
			case DOUBLE:
				value = "0,9";
				break;
			case INTEGER:
				value = "5";
				break;
			default:
				break;
			}
			mNPS.setValore(value);
			
			datiEstesi.getMetadatoAssociato().add(mNPS );
		}
		
		contestoProcedurale.setDatiEstesi(datiEstesi);
		
		parameters.getContestoProcedurale().add(contestoProcedurale );
		try {
			interfaccia.elaboraMessaggioProtocollazioneFlusso(createAccessoApplicativo(), parameters);
		} catch (GenericFault | SecurityFault e) {
			LOGGER.error(ERROR_PROTOCOLLAZIONE_AUTOMATICA_MSG, e);
		}
	}
	
	/**
	 * Creazione e invio della notifica.
	 * @param endpointNPS
	 * @param protocolloCreato
	 * @param idMail
	 * @param tcp
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param tipologiaDocumentoDesc
	 * @param idAoo
	 * @param identificativoMessaggioValue
	 * @param identificatoreProcessoValue
	 */
	private void inviaNotificaNPSFlussoAUT(final String endpointNPS, ProtocolloNpsDTO protocolloCreato, String idMail, TipoContestoProceduraleEnum tcp,
			Integer idTipologiaDocumento, Integer idTipoProcedimento, String tipologiaDocumentoDesc, AooTest rifAoo, AUTMessageIDEnum identificativoMessaggioValue,
			String identificatoreProcessoValue, ProtocolloNpsDTO protocolloNotifica) {
		InterfacciaNotificheNPSFlusso interfaccia = createInterfacciaNotificheNPSFlussoClient(endpointNPS);
		it.ibm.red.webservice.model.interfaccianps.npsmessages.ObjectFactory of = new it.ibm.red.webservice.model.interfaccianps.npsmessages.ObjectFactory();
		it.ibm.red.webservice.model.interfaccianps.npstypes.ObjectFactory ofTypes = new it.ibm.red.webservice.model.interfaccianps.npstypes.ObjectFactory();
		it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.ObjectFactory ofCP = new it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.ObjectFactory();
		it.ibm.red.webservice.model.interfaccianps.npstypesestesi.ObjectFactory ofDatiEstesi = new it.ibm.red.webservice.model.interfaccianps.npstypesestesi.ObjectFactory();
		
		RichiestaElaboraMessaggioFlussoType parameters = of.createRichiestaElaboraMessaggioFlussoType();

		// Creazione mail
		EmailMessageType email = createEmailObj(ofTypes, rifAoo.codiceAoo);
		
		// Creazione protocollo
		WkfIdentificatoreProtcolloType protocollo = createProtocolloObj(protocolloCreato, ofTypes, rifAoo.codiceAoo);
		
		if(protocolloNotifica != null) {
			WkfIdentificatoreProtcolloType protocolloNot = createProtocolloObj(protocolloNotifica, ofTypes, rifAoo.codiceAoo);
			parameters.setProtocolloNotifica(protocolloNot);
		}
		
		
		// Valorizzazione Parameters
		String idMessaggio = identificativoMessaggioValue.getId().toString();
		parameters.setIdDocumento(idMail);
		parameters.setIdMessaggio(idMessaggio);
		parameters.setMessaggioRicevuto(email);
		parameters.setProtocollo(protocollo);
		parameters.setCodiceFlusso(tcp.getId());
		parameters.setIdentificatoreProcesso(identificatoreProcessoValue);

		WkfDatiContestoProceduraleType contestoProcedurale = ofTypes.createWkfDatiContestoProceduraleType();
		contestoProcedurale.setIdentificativoMessaggio(idMessaggio);
		
		ContestoProcedurale cp = ofCP.createContestoProcedurale();
		
		TipoContestoProcedurale tipoCP = ofCP.createTipoContestoProcedurale();
		tipoCP.setContent(tcp.getId());
		cp.setTipoContestoProcedurale(tipoCP);
		CodiceAmministrazione codiceAmministrazione = ofCP.createCodiceAmministrazione();
		codiceAmministrazione.setContent("m_ae");
		cp.setCodiceAmministrazione(codiceAmministrazione );
		CodiceAOO codiceAOO = ofCP.createCodiceAOO();
		codiceAOO.setContent(rifAoo.codiceAoo);
		cp.setCodiceAOO(codiceAOO );
		Identificativo identificativoMessaggio = ofCP.createIdentificativo();
		identificativoMessaggio.setContent(identificativoMessaggioValue.getId().toString());
		cp.setIdentificativo(identificativoMessaggio );
		contestoProcedurale.setContestoProcedurale(cp);
		
		
		String tipologiaDocumentoNPS = npsSRV.getTipologiaDocumentoNPS(idTipologiaDocumento, idTipoProcedimento, tipologiaDocumentoDesc, rifAoo.idAoo);
		
		List<MetadatoDTO> metadatiEstesi = tipologiaDocumentoSrv.caricaMetadati(idTipologiaDocumento, idTipoProcedimento, null, rifAoo.idAoo);
		
		TIPOLOGIA datiEstesi = ofDatiEstesi.createTIPOLOGIA();
		datiEstesi.setFAMIGLIA(0);
		datiEstesi.setNOME(tipologiaDocumentoNPS);
		datiEstesi.setVERSIONE(0);
		
		for(MetadatoDTO m: metadatiEstesi) {
			
			MetaDatoAssociato mNPS = ofDatiEstesi.createMetaDatoAssociato();
			
			mNPS.setCodice(m.getName());
			
			String value = m.getName() + SUFFIX;
			switch (m.getType()) {
			case CAPITOLI_SELECTOR:
				value = CAP_SPESA_UCB_LAV;
				break;
			case DATE:
				value = DateUtils.dateToString(new Date(), true);
				break;
			case LOOKUP_TABLE:
				LookupTableDTO mLook = (LookupTableDTO)m;
				value = mLook.getLookupValues().get(0).getDescription();
				break;
			case PERSONE_SELECTOR:
				value = "<AnagraficaDipendente><CodiceFiscaleDipendente>ABDBDG56H78H501T</CodiceFiscaleDipendente><NomeDipendente>nome</NomeDipendente><CognomeDipendente>cognome</CognomeDipendente><ComuneNascitaDipendente>roma</ComuneNascitaDipendente><SessoDipendente>M</SessoDipendente><DataNascitaDipendente>15/04/1987</DataNascitaDipendente></AnagraficaDipendente>";
				break;
			case DOUBLE:
				value = "0,9";
				break;
			case INTEGER:
				value = "5";
				break;
			default:
				break;
			}
			mNPS.setValore(value);
			
			datiEstesi.getMetadatoAssociato().add(mNPS );
		}
		
		contestoProcedurale.setDatiEstesi(datiEstesi);
		
		parameters.getContestoProcedurale().add(contestoProcedurale );
		try {
			interfaccia.elaboraMessaggioFlusso(createAccessoApplicativo(), parameters);
		} catch (GenericFault | SecurityFault e) {
			LOGGER.error(ERROR_PROTOCOLLAZIONE_AUTOMATICA_MSG, e);
		}
	}

	/**
	 * Restituisce EmailMessageType per il popolamento dei parameters.
	 * @param ofTypes object factory
	 * @return messaggio
	 */
	private static EmailMessageType createEmailObj(it.ibm.red.webservice.model.interfaccianps.npstypes.ObjectFactory ofTypes, String codiceAoo) {
		EmailMessageType email = ofTypes.createEmailMessageType();
		
		// Codice messaggio
		email.setCodiMessaggio(java.util.UUID.randomUUID().toString());

		// Casella mail
		OrgCasellaEmailType casella = ofTypes.createOrgCasellaEmailType();
		EmailIndirizzoEmailType mailAddress = ofTypes.createEmailIndirizzoEmailType();
		mailAddress.setEmail(MAIL_CONTATTO_NPS);
		casella.setIndirizzoEmail(mailAddress);
		OrgAooType aoo = ofTypes.createOrgAooType();
		aoo.setCodiceAOO(codiceAoo);
		casella.setAoo(aoo );
		email.setCasellaEmail(casella);
		
		// Mittente
		EmailIndirizzoEmailType mittente = ofTypes.createEmailIndirizzoEmailType();
		mittente.setEmail(MAIL_CONTATTO_NPS);
		email.setMittente(mittente);

		// Destinatari
		EmailIndirizzoEmailType destinatario = ofTypes.createEmailIndirizzoEmailType();
		destinatario.setEmail(MAIL_TEST);
		email.getDestinatari().add(destinatario);
		
		// Oggetto
		email.setOggettoMessaggio("Oggetto messaggio test");
		
		// Data
		XMLGregorianCalendar xmlDate = null;
		try {
			xmlDate = DateUtils.buildXmlGregorianCalendarFromDate(new Date());
		} catch (Exception e) {
			LOGGER.error("Errore nella creazione della data messaggio.", e);
		}
		email.setDataMessaggio(xmlDate);

		email.setTipoMessaggio("GENERICO_EMAIL");
		
		return email;
	}
	
	/**
	 * Restituisce WkfIdentificatoreProtcolloType per il popolamento dei parameters.
	 * @param protocolloCreato protocollo creato su Nps
	 * @param ofTypes object factory
	 * @return WkfIdentificatoreProtcolloType
	 */
	private static WkfIdentificatoreProtcolloType createProtocolloObj(ProtocolloNpsDTO protocolloCreato,
			it.ibm.red.webservice.model.interfaccianps.npstypes.ObjectFactory ofTypes, String codiceAoo) {
		
		WkfIdentificatoreProtcolloType protocollo = ofTypes.createWkfIdentificatoreProtcolloType();

		// Creazione ProtIdentificatoreProtocolloType
		ProtIdentificatoreProtocolloType prot = new ProtIdentificatoreProtocolloType();
		prot.setIdProtocollo(protocolloCreato.getIdProtocollo());

		// Registro protocollo
		ProtRegistroProtocolloType registro = ofTypes.createProtRegistroProtocolloType();
		OrgOrganigrammaAooType aoo = ofTypes.createOrgOrganigrammaAooType();
		aoo.setCodiceAOO(codiceAoo);
		registro.setAoo(aoo);
		AllCodiceDescrizioneType codiceRegistro = ofTypes.createAllCodiceDescrizioneType();
		codiceRegistro.setCodice("REGISTRO UFFICIALE");
		registro.setCodice(codiceRegistro );
		prot.setRegistro(registro);

		// Tipo protocollo
		prot.setTipoProtocollo(ProtTipoProtocolloType.ENTRATA);
		
		// Numero e Data Registrazione
		prot.setNumeroRegistrazione(protocolloCreato.getNumeroProtocollo());
		XMLGregorianCalendar dataRegistrazione = null;
		try {
			dataRegistrazione = DateUtils.buildXmlGregorianCalendarFromDate(protocolloCreato.getDataProtocollo());
		} catch (Exception e) {
			LOGGER.error(ERRORE_NELLA_CREAZIONE_DELLA_DATA_REGISTRAZIONE, e);
		}
		prot.setDataRegistrazione(dataRegistrazione);

		protocollo.setProtocollo(prot);
		
		return protocollo;
	}

	/**
	 * Restituisce il dto per la creazione del protocollo.
	 * @param documentTitle
	 * @param oggetto
	 * @param utente
	 * @return dto SalvaDocumentoRedDTO
	 */
	private SalvaDocumentoRedDTO getSalvaDocumentoDTO(String documentTitle, String oggetto, UtenteDTO utente, Integer idTipologiaDocumento, Integer idTipoProcedimento) {
		SalvaDocumentoRedDTO docInput = null;
		docInput = new SalvaDocumentoRedDTO();
		
		// Tipo documento
		docInput.setTipologiaDocumento(getTipologiaDocumento(idTipologiaDocumento));
		docInput.setTipoProcedimento(getTipoProcedimento(idTipoProcedimento));
		docInput.setMetadatiEstesi(getMetadatiEstesi(idTipologiaDocumento, idTipoProcedimento, utente.getIdAoo().intValue()));
		
		// Doc title e oggetto
		docInput.setDocumentTitle(documentTitle);
		docInput.setOggetto(oggetto);
		
		// Mittente e destinatario documento
		Contatto mittenteContatto = (rubricaSrv.getContattoFromMail(MAIL_TEST, utente.getIdAoo().intValue(), utente.getIdUfficio())).get(0);
		docInput.setMittenteContatto(mittenteContatto );
		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		DestinatarioRedDTO dest = new DestinatarioRedDTO(mittenteContatto.getContattoID().intValue(), MezzoSpedizioneEnum.CARTACEO.toString(), ModalitaDestinatarioEnum.TO.toString());
		dest.setContatto(mittenteContatto);
		dest.setIdNodo(utente.getIdUfficio());
		dest.setIdUtente(utente.getId());
		destinatari.add(dest);
		docInput.setDestinatari(destinatari );

		return docInput;
	}

	private Collection<MetadatoDTO> getMetadatiEstesi(Integer idTipologiaDocumento, Integer idTipoProcedimento, Integer idAoo) {
		List<MetadatoDTO> metadatiEstesi = tipologiaDocumentoSrv.caricaMetadatiEstesiPerGUI(idTipologiaDocumento, idTipoProcedimento, true, idAoo, null);
		
		for(MetadatoDTO m: metadatiEstesi) {
			
			switch (m.getType()) {
			case CAPITOLI_SELECTOR:
				SelettoreCapitoliDTO mCap = (SelettoreCapitoliDTO)m;
				mCap.setLookupValueSelected(new SelectItemDTO(CAP_SPESA_UCB_LAV, CAP_SPESA_UCB_LAV));
				break;
			case DATE:
				m.setSelectedValue(new Date());
				break;
			case LOOKUP_TABLE:
				LookupTableDTO mLook = (LookupTableDTO)m;
				mLook.setLookupValueSelected(mLook.getLookupValues().get(0));
				break;
			case PERSONE_SELECTOR:
				AnagraficaDipendenteDTO anagraficaDipendenteDTO = new AnagraficaDipendenteDTO("nome", "cognome", 
						"ABDBDG56H78H501T", DateUtils.parseDate("15/04/1987"), "roma", Boolean.TRUE);
				AnagraficaDipendentiComponentDTO mAnag = (AnagraficaDipendentiComponentDTO)m;
				mAnag.setSelectedValues(Arrays.asList(anagraficaDipendenteDTO));
				break;
			case DOUBLE:
				m.setSelectedValue(0.9);
				break;
			case INTEGER:
				m.setSelectedValue(5);
				break;
			default:
				m.setSelectedValue(m.getName() + SUFFIX);
				break;
			}
						
			
		}
		
		return metadatiEstesi;
		
	}

	private TipologiaDocumentoDTO getTipologiaDocumento(Integer idTipologiaDocumento) {
		return tipologiaDocumentoSrv.getById(idTipologiaDocumento);
	}
	
	private TipoProcedimentoDTO getTipoProcedimento(Integer idTipoProcedimento) {
		return tipoProcedimentoSrv.getTipoProcedimentoById(idTipoProcedimento);
	}
	
	/**
	 * Esegue l'upload di un documento su NPS.
	 * @param fileName nome del documento su cui effettuare l'upload
	 * @param path path del documento comprensivo di nome del file.
	 * @param mimeType mimetype del file su cui effettuare l'upload
	 * @param idAoo
	 * @return id del documento che è stato caricato su NPS
	 */
	private String uploadDocumentoToNPS(String fileName, String path, String mimeType, Integer idAoo) {
		final String idDocumento = java.util.UUID.randomUUID().toString();
		InputStream content = new ByteArrayInputStream(FileUtils.getFileFromFS(path));
		try {
			npsSRV.uploadDocToNps(idAoo, false, fileName, mimeType, fileName, content, new Date(), idDocumento);
			
			LOGGER.info("Documento uploadato su NPS con id: " + idDocumento);
			
		} catch (Exception e) {
			LOGGER.warn("Errore in fase di upload del documento su NPS con id: " + idDocumento, e);
		}
		return idDocumento;
	}
	
	/**
	 * Crea Una mail e un protocollo associandoli su NPS e restituendone le informazioni.
	 * @param username
	 * @param emailFileName
	 * @param emailPath
	 * @param emailMimeType
	 * @param docPrincFileName
	 * @param docPrincPath
	 * @param docPrincMimeType
	 * @param oggettoProtocollo
	 * @param assegnazioniCompetenza
	 * @return informazioni sulla mail e sul protocollo creati
	 */
	private final EmailProtocollo creaEmailProtocollo(AooTest riferimentoAoo, String emailFileName, String emailPath, String emailMimeType, 
			String docPrincFileName, String docPrincPath, String docPrincMimeType, String allFileName, String allPath, String allMimeType, 
			String oggettoProtocollo, Integer idTipologiaDocumento, Integer idTipoProcedimento) {
		
		UtenteDTO utente = getUtente(riferimentoAoo.firmatario);

		//upload eml su NPS
		final String idEmail = uploadDocumentoToNPS(emailFileName, emailPath + emailFileName, emailMimeType, utente.getIdAoo().intValue());
		
		//upload documento principale protocollo
		final String idDocumentoPrincipale = uploadDocumentoToNPS(docPrincFileName, docPrincPath + docPrincFileName, docPrincMimeType, utente.getIdAoo().intValue());
		
		//upload allegato protocollo
		final String idAllegato = uploadDocumentoToNPS(allFileName, allPath + allFileName, allMimeType, utente.getIdAoo().intValue());
		
		String documentTitle = "123456"; // Non viene utilizzato se non nelle loggate
		SalvaDocumentoRedDTO docInput = getSalvaDocumentoDTO(documentTitle, oggettoProtocollo, utente, idTipologiaDocumento, idTipoProcedimento);
		
		Connection connection = createNewDBConnection();
		
		String[] assegnazioniCompetenza = new String[]{riferimentoAoo.idUfficioAss + "," + riferimentoAoo.idUtenteAss};
		// Crea protocollo ingresso
		ProtocolloNpsDTO protocolloCreato = npsSRV.creaProtocolloEntrata(docInput, "" , utente, assegnazioniCompetenza , null, null, null, null, null, null, false, null, connection);
		checkAssertion(protocolloCreato != null, "Protocollazione non avvenuta con successo.");
		
		//associa documento principale a protocollo
		npsSRV.associateDocumentoProtocolloToNps(0, idDocumentoPrincipale, protocolloCreato.getIdProtocollo(), 
				docPrincFileName, docPrincFileName, utente, true, false, null, null, documentTitle, connection);
		
		//associa allegato a protocollo
		npsSRV.associateDocumentoProtocolloToNps(0, idAllegato, protocolloCreato.getIdProtocollo(), 
				allFileName, allFileName, utente, false, false, null, null, documentTitle, connection);
		
		try {
			connection.close();
		} catch (SQLException e) {
			LOGGER.error(ERRORE_CHIUSURA_CONNESSIONE, e);
		}
		
		return new EmailProtocollo(idEmail, protocolloCreato);

	}
	
	/**
	 * Classe per gestione associazione Email e protocollo.
	 */
	public static class EmailProtocollo{
		/**
		 * Id mail.
		 */
		private String idEmail;
		/**
		 * Informazioni su protocollo.
		 */
		private ProtocolloNpsDTO protocolloCreato;
		
		/**
		 * Costruttore completo.
		 * @param idEmail
		 * @param protocolloCreato
		 */
		public EmailProtocollo(String idEmail, ProtocolloNpsDTO protocolloCreato) {
			super();
			this.idEmail = idEmail;
			this.protocolloCreato = protocolloCreato;
		}
		
		/**
		 * Restituisce l'id della mail.
		 * @return id mail
		 */
		public String getIdEmail() {
			return idEmail;
		}
	
		/**
		 * Restituisce il dto associato al protocollo.
		 * @return informazioni sul protocollo
		 */
		public ProtocolloNpsDTO getProtocolloCreato() {
			return protocolloCreato;
		}
	}
	
	/**
	 * Crea un protocollo in uscita su NPS restituendone le informazioni.
	 * @param username
	 * @param emailFileName
	 * @param emailPath
	 * @param emailMimeType
	 * @param docPrincFileName
	 * @param docPrincPath
	 * @param docPrincMimeType
	 * @param oggettoProtocollo
	 * @param indiceFascicolo 
	 * @param descrIndiceFascicolo 
	 * @return informazioni sulla mail e sul protocollo creati
	 */
	private final EmailProtocollo creaProtocolloUscita(AooTest rifAoo, String docPrincFileName, String docPrincPath, String docPrincMimeType,  
			String docPrincFileName2Version, String docPrincPath2Version, String docPrincMimeType2Version, String allFileName, String allPath, String allMimeType, 
			String oggettoProtocollo, String indiceFascicolo, String descrIndiceFascicolo) {
		
		UtenteDTO utente = getUtente(rifAoo.firmatario);

		//upload documento principale protocollo
		final String idDocumentoPrincipale = uploadDocumentoToNPS(docPrincFileName, docPrincPath + docPrincFileName, docPrincMimeType, utente.getIdAoo().intValue());
		
		//upload documento principale protocollo seconda versione
		final String idDocumentoPrincipale2Version = uploadDocumentoToNPS(docPrincFileName2Version, docPrincPath2Version + docPrincFileName2Version, docPrincMimeType2Version, utente.getIdAoo().intValue());
		
		//upload allegato
		final String idAllegato = uploadDocumentoToNPS(allFileName, allPath + allFileName, allMimeType, utente.getIdAoo().intValue());

		String documentTitle = "123456"; // Non viene utilizzato se non nelle loggate

		Connection connection = createNewDBConnection();

		// Crea protocollo uscita
		final NpsConfiguration npsConf = npsConfigurationDAO.getByIdAoo(utente.getIdAoo().intValue(), connection);
		ProtocolloNpsDTO protocolloCreato = getProtocolloUscita(oggettoProtocollo, rifAoo.idTipoDocGen,
				rifAoo.idTipoProcGen, indiceFascicolo, descrIndiceFascicolo, utente, connection, npsConf, "");
		checkAssertion(protocolloCreato != null, "Protocollazione non avvenuta con successo.");
		
		//associa documento principale a protocollo
		npsSRV.associateDocumentoProtocolloToNps(0, idDocumentoPrincipale, protocolloCreato.getIdProtocollo(), 
				docPrincFileName, docPrincFileName, utente, true, false, null, null, documentTitle, connection);
		
		//associa seconda versione documento principale a protocollo
		npsSRV.associateDocumentoProtocolloToNps(0, idDocumentoPrincipale2Version, protocolloCreato.getIdProtocollo(), 
				docPrincFileName2Version, docPrincFileName2Version, utente, true, false, idDocumentoPrincipale, npstypes.v1.it.gov.mef.DocTipoOperazioneType.COLLEGA_DOCUMENTO, 
				documentTitle, connection);
		
		//associa allegato a protocollo
		npsSRV.associateDocumentoProtocolloToNps(0, idAllegato, protocolloCreato.getIdProtocollo(), 
				allFileName, allFileName, utente, false, false, null, null, documentTitle, connection);
		
		try {
			connection.close();
		} catch (SQLException e) {
			LOGGER.error(ERRORE_CHIUSURA_CONNESSIONE, e);
		}
		
		return new EmailProtocollo(null, protocolloCreato);

	}

	/**
	 * Crea protocllo uscita senza content.
	 * 
	 * @param oggettoProtocollo
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param indiceFascicolo
	 * @param descrIndiceFascicolo
	 * @param utente
	 * @param connection
	 * @param npsConf
	 * @param acl
	 * @return Protocollo creato.
	 */
	private ProtocolloNpsDTO getProtocolloUscita(String oggettoProtocollo, Integer idTipologiaDocumento,
			Integer idTipoProcedimento, String indiceFascicolo, String descrIndiceFascicolo, UtenteDTO utente,
			Connection connection, final NpsConfiguration npsConf, final String acl) {
		
		TipologiaDocumentoDTO tipologiaDocumento = getTipologiaDocumento(idTipologiaDocumento);
		List<MetadatoDTO> metadatiEstesi = tipologiaDocumentoSrv.caricaMetadatiEstesiPerGUI(idTipologiaDocumento, idTipoProcedimento, true, utente.getIdAoo(), null);
		Contatto destinatarioContatto = (rubricaSrv.getContattoFromMail(MAIL_TEST, utente.getIdAoo().intValue(), utente.getIdUfficio())).get(0);
		String email = destinatarioContatto.getMailPec() != null ? destinatarioContatto.getMailPec() : destinatarioContatto.getMail();
		
		ProtDestinatarioType[] destinatariArray = new ProtDestinatarioType[] {
				npsSRV.getDestinatarioNPS(MezzoSpedizioneEnum.CARTACEO, destinatarioContatto.getTipoPersona(), destinatarioContatto.getNome(), destinatarioContatto.getCognome(), 
						destinatarioContatto.getCodiceFiscalePIva(), destinatarioContatto.getAliasContatto(), email, (destinatarioContatto.getMailPec() != null), 
						destinatarioContatto.getContattoID(), PostaEnum.POSTA_ESTERNA_INTEROPERABILE_AUTOMATICA, npsConf.getCodiceAoo(), npsConf.getDenominazioneAoo(), MAIL_CONTATTO_NPS, true)};

		return npsSRV.creaProtocolloUscita(destinatariArray, acl, utente.getIdUfficio(), utente.getId(), 
				utente, oggettoProtocollo, idTipologiaDocumento, idTipoProcedimento, tipologiaDocumento.getDescrizione(), 
				indiceFascicolo, descrIndiceFascicolo, Builder.DA_SPEDIRE, null, metadatiEstesi, false, connection);
	}
	
	/**
	 * Creazione e invio della notifica.
	 * 
	 * @param endpointNPS
	 * @param protocolloUscitaCreato
	 * @param protocolloIngressoCreato1
	 * @param protocolloIngressoCreato2
	 * @param username
	 * @param tipologiaAzione
	 *            Tipologia dell'azione specificata.
	 * @throws DatatypeConfigurationException
	 */
	private void inviaNotificaAzioneNPSFlusso(final String endpointNPS, ProtocolloNpsDTO protocolloUscitaCreato, ProtocolloNpsDTO protocolloIngressoCreato1, 
			ProtocolloNpsDTO protocolloIngressoCreato2, String username, final WkfTipoNotificaAzioneType tipologiaAzione) throws DatatypeConfigurationException {
		InterfacciaNotificheNPS interfaccia = createInterfacciaNotificheNPSClient(endpointNPS);
		it.ibm.red.webservice.model.interfaccianps.npsmessages.ObjectFactory of = new it.ibm.red.webservice.model.interfaccianps.npsmessages.ObjectFactory();
		it.ibm.red.webservice.model.interfaccianps.npstypes.ObjectFactory ofTypes = new it.ibm.red.webservice.model.interfaccianps.npstypes.ObjectFactory();
		
		OrgOrganigrammaType operatore = null;
		Connection connection = createNewDBConnection();
		UtenteDTO utente = getUtente(username);
		NpsConfiguration npsConf = npsConfigurationDAO.getByIdAoo(utente.getIdAoo().intValue(), connection);
		Nodo nodo = nodoDAO.getNodo(utente.getIdUfficio(), connection);
		
		RichiestaElaboraNotificaAzioneType parameters = of.createRichiestaElaboraNotificaAzioneType();
		XMLGregorianCalendar dataOperazione = DateUtils.buildXmlGregorianCalendarFromDate(new Date());
		
		parameters.setDataAzione(dataOperazione);
		parameters.setIdMessaggio(java.util.UUID.randomUUID().toString());
		parameters.setTipoAzione(tipologiaAzione);
		
		Object datiAzione = null;
		try {
			connection.close();
		} catch (SQLException e) {
			LOGGER.error(ERRORE_CHIUSURA_CONNESSIONE, e);
		}
		
		switch (tipologiaAzione) {
		case AGGIORNAMENTO_COLLEGATI:
			datiAzione = ofTypes.createWkfAzioneAggiornamentoCollegatiType();
			
			((WkfAzioneAggiornamentoCollegatiType)datiAzione).setDataOperazione(dataOperazione);
			
			operatore =  buildOrganigrammaPf(ofTypes, String.valueOf(utente.getId()), utente.getCognome(), 
					utente.getNome(), utente.getCodFiscale(), npsConf.getCodiceAmministrazione(), npsConf.getDenominazioneAmministrazione(), 
					npsConf.getCodiceAoo(), npsConf.getDenominazioneAoo(), nodo.getCodiceNodo(), nodo.getDescrizione(), "" + utente.getIdUfficio());
			
			((WkfAzioneAggiornamentoCollegatiType)datiAzione).setOperatore(operatore );
			
			((WkfAzioneAggiornamentoCollegatiType)datiAzione).setSistemaAusiliario("SIPATR");
			((WkfAzioneAggiornamentoCollegatiType)datiAzione).setSistemaProduttore("RED");
			((WkfAzioneAggiornamentoCollegatiType)datiAzione).setStatoCollegati(Builder.CHIUSO);
			((WkfAzioneAggiornamentoCollegatiType)datiAzione).getCollegati().add(createProtocolloObj4Collegati(protocolloIngressoCreato1, ofTypes));
			((WkfAzioneAggiornamentoCollegatiType)datiAzione).getCollegati().add(createProtocolloObj4Collegati(protocolloIngressoCreato2, ofTypes));

			parameters.setIdentificativoProtocollo(createProtocolloObj4NotificaAzione(protocolloUscitaCreato, ofTypes, utente.getCodiceAoo()));
			parameters.setDatiAggiornamentoCollegati(((WkfAzioneAggiornamentoCollegatiType)datiAzione));
			break;
		case RISPONDI_A:
			datiAzione = ofTypes.createWkfAzioneRispondiAProtocolloType();
			
			((WkfAzioneRispondiAProtocolloType)datiAzione).setSistemaAusiliario("SPESE");
			((WkfAzioneRispondiAProtocolloType)datiAzione).setSistemaProduttore("RED");
			
			OrgOrganigrammaType protocollatore = buildOrganigrammaPf(ofTypes, String.valueOf(utente.getId()), utente.getCognome(), 
					utente.getNome(), utente.getCodFiscale(), npsConf.getCodiceAmministrazione(), npsConf.getDenominazioneAmministrazione(), 
					npsConf.getCodiceAoo(), npsConf.getDenominazioneAoo(), nodo.getCodiceNodo(), nodo.getDescrizione(), "" + utente.getIdUfficio());
			
			((WkfAzioneRispondiAProtocolloType)datiAzione).setProtocollatore(protocollatore);
			
			((WkfAzioneRispondiAProtocolloType)datiAzione).setProtocolloRisposta(createProtocolloObj4NotificaAzione(protocolloIngressoCreato1, ofTypes, utente.getCodiceAoo()));
			
			parameters.setIdentificativoProtocollo(createProtocolloObj4NotificaAzione(protocolloUscitaCreato, ofTypes, utente.getCodiceAoo()));
			parameters.setDatiRispondiA(((WkfAzioneRispondiAProtocolloType)datiAzione));
			break;
		default:
			throw new NotImplementedException("Tipologia azione non gestita dal metodo.");
		}
		
		try {
			interfaccia.elaboraNotificaAzione(createAccessoApplicativo(), parameters);
		} catch (GenericFault | SecurityFault e) {
			LOGGER.error("Errore elaborazione notifica azione.", e);
		}
	}
	
	/**
	 * Restituisce WkfIdentificatoreProtcolloType per il popolamento dei parameters.
	 * @param protocolloCreato protocollo creato su Nps
	 * @param ofTypes object factory
	 * @param codiceAoo 
	 * @return WkfIdentificatoreProtcolloType
	 * @throws DatatypeConfigurationException 
	 */
	private static IdentificativoProtocolloRequestType createProtocolloObj4NotificaAzione(ProtocolloNpsDTO protocolloCreato,
			it.ibm.red.webservice.model.interfaccianps.npstypes.ObjectFactory ofTypes, String codiceAoo) throws DatatypeConfigurationException {
		
		IdentificativoProtocolloRequestType prot = ofTypes.createIdentificativoProtocolloRequestType();

		prot.setIdProtocollo(protocolloCreato.getIdProtocollo());

		// Registro protocollo
		ProtRegistroProtocolloType registro = ofTypes.createProtRegistroProtocolloType();
		OrgAmministrazioneType amministrazione = ofTypes.createOrgAmministrazioneType();
		amministrazione.setCodiceAmministrazione("m_ef");

		OrgOrganigrammaAooType aoo = ofTypes.createOrgOrganigrammaAooType();
		aoo.setCodiceAOO(codiceAoo);
		aoo.setAmministrazione(amministrazione);
		
		registro.setAoo(aoo);
		AllCodiceDescrizioneType codiceReg = ofTypes.createAllCodiceDescrizioneType();
		codiceReg.setCodice("REGISTRO UFFICIALE");
		registro.setCodice(codiceReg);
		
		prot.setRegistro(registro);

		// Numero e Data Registrazione
		prot.setNumeroRegistrazione(protocolloCreato.getNumeroProtocollo());
		XMLGregorianCalendar dataRegistrazione = DateUtils.buildXmlGregorianCalendarFromDate(new Date());
		
		if (protocolloCreato.getDataProtocollo() != null) {
			try {
				dataRegistrazione = DateUtils.buildXmlGregorianCalendarFromDate(protocolloCreato.getDataProtocollo());
			} catch (Exception e) {
				LOGGER.error(ERRORE_NELLA_CREAZIONE_DELLA_DATA_REGISTRAZIONE, e);
			}
		}
		prot.setDataRegistrazione(dataRegistrazione);
		
		return prot;
	}
	
	/**
	 * Restituisce WkfIdentificatoreProtcolloType per il popolamento dei parameters.
	 * @param protocolloCreato protocollo creato su Nps
	 * @param ofTypes object factory
	 * @return WkfIdentificatoreProtcolloType
	 */
	private static ProtIdentificatoreProtocolloCollegatoType createProtocolloObj4Collegati(ProtocolloNpsDTO protocolloCreato,
			it.ibm.red.webservice.model.interfaccianps.npstypes.ObjectFactory ofTypes) {
		
		// Creazione ProtIdentificatoreProtocolloType
		ProtIdentificatoreProtocolloCollegatoType prot = ofTypes.createProtIdentificatoreProtocolloCollegatoType();
		prot.setIdProtocollo(protocolloCreato.getIdProtocollo());

		// Tipo protocollo
		prot.setTipoProtocollo(ProtTipoProtocolloType.ENTRATA);
		
		// Numero e Data Registrazione
		prot.setNumeroRegistrazione(protocolloCreato.getNumeroProtocollo());
		XMLGregorianCalendar dataRegistrazione = null;
		try {
			dataRegistrazione = DateUtils.buildXmlGregorianCalendarFromDate(protocolloCreato.getDataProtocollo());
		} catch (Exception e) {
			LOGGER.error(ERRORE_NELLA_CREAZIONE_DELLA_DATA_REGISTRAZIONE, e);
		}
		prot.setDataRegistrazione(dataRegistrazione);
		
		return prot;
	}
	
	/**
	 * Costruisce un organigramma type.
	 * @param ofTypes
	 * @param idUtente
	 * @param cognome
	 * @param nome
	 * @param codiceFiscale
	 * @param codiceAmministrazione
	 * @param denominazioneAmministrazione
	 * @param codiceAOO
	 * @param denominazioneAOO
	 * @param codiceUO
	 * @param denominazioneUO
	 * @param idUO
	 * @return OrgOrganigrammaType
	 */
	public static OrgOrganigrammaType buildOrganigrammaPf(it.ibm.red.webservice.model.interfaccianps.npstypes.ObjectFactory ofTypes,
			String idUtente, String cognome, String nome, String codiceFiscale,
			String codiceAmministrazione, String denominazioneAmministrazione, String codiceAOO,
			String denominazioneAOO, String codiceUO, String denominazioneUO, String idUO) {

		OrgAmministrazioneType amministrazione = ofTypes.createOrgAmministrazioneType();
		amministrazione.setCodiceAmministrazione(codiceAmministrazione);
		amministrazione.setDenominazione(denominazioneAmministrazione);

		OrgOrganigrammaAooType aoo = ofTypes.createOrgOrganigrammaAooType();
		aoo.setAmministrazione(amministrazione);
		aoo.setCodiceAOO(codiceAOO);
		if (denominazioneAOO != null) {
			aoo.setDenominazione(denominazioneAOO);
		}

		OrgOrganigrammaUnitaOrganizzativaType unitaOrganizzativa = ofTypes.createOrgOrganigrammaUnitaOrganizzativaType();
		unitaOrganizzativa.setAoo(aoo);
		unitaOrganizzativa.setDenominazione(denominazioneUO);
		unitaOrganizzativa.setCodiceUO(codiceUO);
		AllChiaveEsternaType chiaveEsternaUO = ofTypes.createAllChiaveEsternaType();
		chiaveEsternaUO.setValue(idUO);
		unitaOrganizzativa.setChiaveEsterna(chiaveEsternaUO);

		AnaPersonaFisicaType utente = ofTypes.createAnaPersonaFisicaType();
		utente.setCognome(cognome);
		utente.setNome(nome);
		if(codiceFiscale!=null) {
			utente.setCodiceFiscale(codiceFiscale);
		}
		AllChiaveEsternaType chiaveEsternaUtente = ofTypes.createAllChiaveEsternaType();
		chiaveEsternaUtente.setValue(idUO + "." + idUtente);
		utente.setChiaveEsterna(chiaveEsternaUtente);

		AllChiaveEsternaType chiaveEsternaOperatore = ofTypes.createAllChiaveEsternaType();
		chiaveEsternaOperatore.setValue(idUO + "." + idUtente);
		OrgOrganigrammaType output = ofTypes.createOrgOrganigrammaType();
		output.setUnitaOrganizzativa(unitaOrganizzativa);
		output.setUtente(utente);
		output.setChiaveEsterna(chiaveEsternaOperatore);
		return output;
	}
	
	/**
	 * Enum che definisce tutte le informazioni necessarie per l'esecuzione dei test sui flussi NPS.
	 */
	private enum AooTest {
		
		/**
		 * Aoo MISE.
		 */
		UCB_MISE (35, "UCB_MISE", 
				USERNAME_GIANSANTI, "37226", "3207", // Utente
				2600,1300, // Tipologia generica
				"00", "Generico", // Titolario
				4714, 3564, "ATTI_SOGGETTI_VISTO", // Atti soggetti visto
				6774, 6894, "CONTO_GIUDIZIALE_DA_FLUSSO", // Conto giudiziale da flusso
				6139, 6263, "CONTO_CONSUNTIVO_SE_DA_FLUSSO", // Conto consuntivo da flusso
				6136, 6259, "NOTIFICA_DA_FLUSSO"),// Notifica
		
		/**
		 * Aoo MAE.
		 */
		UCB_MAE (46, "UCB_MAE", 
				USERNAME_STIZZO, "44052", "4301", // Utente
				6714, 6834, // Tipologia generica
				"01", "Predisposizione, gestione e rendicontazione del bilancio finanaziario", // Titolario
				6734, 6854, "ATTI_SOGGETTI_A_VISTO_DA_FLUSSO", // Atti soggetti visto
				6880, 6894, "CONTO_GIUDIZIALE_DA_FLUSSO", // Conto giudiziale da flusso
				6876, 6924, "CONTO_CONSUNTIVO_SE_DA_FLUSSO", // Conto consuntivo da flusso
				6883, 6978, "NOTIFICA_DA_FLUSSO"),// Notifica
		
		/**
		 * Aoo ULAV.
		 */
		UCB_LAV (34, "UCB_LAV", 
				USERNAME_ROSCIOLI, null, null, // Utente
				null, null, // Tipologia generica
				"00", "Generico", // Titolario
				null, null, null, // Atti soggetti visto
				null, null, null, // Conto giudiziale da flusso
				null, null, null, // Conto consuntivo da flusso
				null, null, null) // Notifica
		;
		
		/**
		 * Utente firmatario.
		 */
		private String firmatario;
		
		/**
		 * Identificativo di un utente da usare come assegnatario.
		 */
		private String idUtenteAss;
		
		/**
		 * Identificativo di un ufficio da usare come assegnatario.
		 */
		private String idUfficioAss;
		
		/**
		 * Identificativo tipologia documento generico.
		 */
		private Integer idTipoDocGen;
		
		/**
		 * Identificativo tipologia procedimento generico.
		 */
		private Integer idTipoProcGen;
		
		/**
		 * Identificaito dell'aoo.
		 */
		private Integer idAoo;
		
		/**
		 * Codice dell'aoo da testare.
		 */
		private String codiceAoo;
		
		/**
		 * Codice di un titolario valido per l'aoo.
		 */
		private String codiceTitolario;
		
		/**
		 * Descrizione di un titolario valido per l'aoo.
		 */
		private String descrizioneTitolario;
		
		/**
		 * Identificativo tipologia documento: Atti soggetti a visto.
		 */
		private Integer idDocAsav;
		
		/**
		 * Identificativo tipologia procedimento: Atti soggetti a visto.
		 */
		private Integer idProcAsav;
		
		/**
		 * Descrizione tipologia documento: Atti soggetti a visto, concorde con la descrizione NPS.
		 */
		private String descAsav;
		
		/**
		 * Identificativo tipologia documento: Conto Giudiziale da flusso.
		 */
		private Integer idDocCG;
		
		/**
		 * Identificativo tipologia procedimento: Conto Giudiziale da flusso.
		 */
		private Integer idProcCG;
		
		/**
		 * Descrizione tipologia documento: Conto Giudiziale da flusso, concorde con la descrizione NPS.
		 */
		private String descCG;

		/**
		 * Identificativo tipologia documento: Conto Consuntivo da flusso.
		 */
		private Integer idDocContoConsuntivo;
		
		/**
		 * Identificativo tipologia procedimento: Conto Consuntivo da flusso.
		 */
		private Integer idProcContoConsuntivo;
		
		/**
		 * Descrizione tipologia documento: Conto Consuntivo da flusso, concorde con la descrizione NPS.
		 */
		private String descContoConsuntivo;
		
		/**
		 * Identificativo tipologia documento: Notifica da flusso.
		 */
		private Integer idDocNotifica;
		
		/**
		 * Identificativo tipologia procedimento: Notifica da flusso.
		 */
		private Integer idProcNotifica;
		
		/**
		 * Descrizione tipologia documento: Notifica da flusso, concorde con la descrizione NPS.
		 */
		private String descNotifica;
		
		/**
		 * Costruttore completo.
		 * 
		 * @param idAoo
		 * @param codiceAoo
		 * @param inFirmatario
		 * @param inIdUtenteAss
		 * @param inIdUfficioAss
		 * @param idGen
		 * @param idProcGen
		 * @param codiceTitolario
		 * @param descTitolario
		 * @param idAsav
		 * @param idProcAsav
		 * @param descAsav
		 */
		AooTest(Integer idAoo, String codiceAoo, 
				String inFirmatario, String inIdUtenteAss, String inIdUfficioAss, // Utente
				Integer idGen, Integer idProcGen, // Tipologia Generica
				String codiceTitolario, String descTitolario, // Titolario
				Integer idAsav, Integer idProcAsav, String descAsav, // Atti soggetti visto
				Integer idDocCG, Integer idProcCG, String descCG, // Conto giudiziale
				Integer idDocCC, Integer idProcCC, String descCC, // Conto consuntivo
				Integer idDocNotifica, Integer idProcNotifica, String descNotifica)
		{ 

			this.idAoo = idAoo;
			this.codiceAoo = codiceAoo;
			this.firmatario = inFirmatario;
			this.idUfficioAss = inIdUfficioAss;
			this.idUtenteAss = inIdUtenteAss;
			this.idTipoDocGen = idGen;
			this.idTipoProcGen = idProcGen;
			this.codiceTitolario = codiceTitolario;
			this.descrizioneTitolario = descTitolario;
			this.idDocAsav = idAsav;
			this.idProcAsav = idProcAsav;
			this.descAsav = descAsav;
			this.idDocCG = idDocCG;
			this.idProcCG = idProcCG;
			this.descCG = descCG;
			this.idDocContoConsuntivo = idDocCC;
			this.idProcContoConsuntivo = idProcCC;
			this.descContoConsuntivo = descCC;
			this.idDocNotifica = idDocNotifica;
			this.idProcNotifica = idProcNotifica;
			this.descNotifica = descNotifica;
		}

	}
	
}