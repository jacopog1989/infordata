package it.ibm.business.test.suite;

import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.EventoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.ICalendarioSRV;

/**
 * Classe di test del calendario.
 */
public class CalendarioTest extends AbstractTest {

	/**
	 * Servizio calendario.
	 */
	private ICalendarioSRV calSRV;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CalendarioTest.class.getName());
	
	/**
	 * Azioni da eseguire prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		calSRV = ApplicationContextProvider.getApplicationContext().getBean(ICalendarioSRV.class);
	}
	
	/**
	 * Azioni da eseguire dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Restituisce i documenti in scadenza dal PE.
	 */
	@Test
	public final void getDocInScadenzaFromPe() {
		UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		Collection<EventoDTO> eventi = calSRV.getDocInScadenzaFromPe(utente.getId(), utente.getIdUfficio(), utente.getFcDTO());
		LOGGER.info(String.valueOf(eventi.size()));
	}

	/**
	 * Restituisce gli eventi dal database.
	 */
	@Test
	public static final void getEventiFromDb() {
		// Test non implementato.
	}
	
	/**
	 * Ricerca eventi.
	 */
	@Test
	public static final void testRicercaEventi() {
		// Test non implementato.
	}

}
