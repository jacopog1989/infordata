package it.ibm.business.test.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

/**
 * 
 * @author CRISTIANOPierascenzi
 */

@Suite.SuiteClasses({
	WarmUpTest.class,
	Test00.class,
	Test04.class,
	Test05.class,
	Test06.class,
//	Test07.class, La firma remota richiede OTP!
	Test08.class,
	Test09.class,
	Test10.class,
	Test11.class,
	Test12.class,
	Test13.class,
//	Test14.class, La firma remota richiede OTP! Semaforo fallisce per assenza campo firma documento principale
//	Test17.class, Come creare assegnazione per spedizione?
	Test18.class
})

public class JunitTestSuite {   
}  
