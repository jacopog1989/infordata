package it.ibm.business.test.asign;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.DetailDocumentoDTO;
import it.ibm.red.business.enums.SignStrategyEnum;
import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * Classe di test dello step di Protocollazione.
 */
public class Test02StepProtocollazione extends AbstractAsignTest {

	/**
	 * Messaggio di errore sulla valorizzazione del numero protocollo.
	 */
	private static final String VALORIZZA_NUMERO_PROTOCOLLO = "Numero protocollo deve essere valorizzato";
	
	/**
	 * Messaggio di errore sulla valorizzazione dell'anno protocollo.
	 */
	private static final String VALORIZZA_ANNO_PROTOCOLLO = "Anno protocollo deve essere valorizzato";

	/**
	 * Esegue le azioni prima dell'esecuzione del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue le azioni necessarie subito dopo l'esecuzione del test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Esegue la logica dello step di protocollazione su un documento non protocollato.
	 */
	@Test
	public final void eseguiStepSuDocNonProtocollato() {
		//Prepariamo il documento per la firma asincrona (PROTOCOLLAZIONE ASINCRONA)
		ASignItemDTO itemPre = accoda(USERNAME_RGS, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, SignStrategyEnum.ASYNC_STRATEGY_B, null);
		//Eseguiamo step di protocollazione
		run(StepEnum.PROTOCOLLAZIONE, itemPre.getId());
		//Asserzioni
		ASignItemDTO itemPost = getaSignSRV().getItem(itemPre.getId(), true);
		
		checkAssertion(itemPost.getAnnoProtocollo()!=null, VALORIZZA_ANNO_PROTOCOLLO);
		checkAssertion(itemPost.getNumeroProtocollo()!=null, VALORIZZA_NUMERO_PROTOCOLLO);
	}

	/**
	 * Esegue la logica dello step di protocollazione su un documento protocollato.
	 */
	@Test
	public final void eseguiStepSuDocProtocollato() {
		//Prepariamo il documento per la firma asincrona (PROTOCOLLAZIONE SINCRONA)
		ASignItemDTO itemPre = accoda(USERNAME_LAVORO, ID_TIPO_DOC_GENERICO_LAVORO_USCITA, ID_TIPO_PROC_GENERICO_LAVORO_USCITA, SignStrategyEnum.ASYNC_STRATEGY_A, null);
		//Eseguiamo step di protocollazione
		run(StepEnum.PROTOCOLLAZIONE, itemPre.getId());
		//Asserzioni
		ASignItemDTO itemPost = getaSignSRV().getItem(itemPre.getId(), true);

		checkAssertion(itemPost.getAnnoProtocollo()!=null, VALORIZZA_ANNO_PROTOCOLLO);
		checkAssertion(itemPost.getNumeroProtocollo()!=null, VALORIZZA_NUMERO_PROTOCOLLO);
	}

	/**
	 * Simula un crash durante il processamento del documento non protocollato e in fase di protocollazione.
	 */
	@Test
	public final void crashDocNonProtocollato() {
		//Prepariamo il documento per la firma asincrona (PROTOCOLLAZIONE ASINCRONA)
		ASignItemDTO itemPre = accoda(USERNAME_RGS, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, SignStrategyEnum.ASYNC_STRATEGY_B, null);
		//Simuliamo crash
		crash(StepEnum.PROTOCOLLAZIONE, itemPre.getId());
		//Asserzioni
		DetailDocumentoDTO documentDetailDTO = getDocDetail(USERNAME_RGS, itemPre);
		checkAssertion(documentDetailDTO.getAnnoProtocollo()!=null, VALORIZZA_ANNO_PROTOCOLLO);
		checkAssertion(documentDetailDTO.getNumeroProtocollo()!=null, VALORIZZA_NUMERO_PROTOCOLLO);
		ASignItemDTO itemPost = getaSignSRV().getItem(itemPre.getId(), true);
		run(StepEnum.PROTOCOLLAZIONE, itemPost.getId());
		itemPost = getaSignSRV().getItem(itemPre.getId(), true);
		checkAssertion(itemPost.getAnnoProtocollo()!=null, VALORIZZA_ANNO_PROTOCOLLO);
		checkAssertion(itemPost.getNumeroProtocollo()!=null, VALORIZZA_NUMERO_PROTOCOLLO);
		checkAssertion(itemPost.getAnnoProtocollo().equals(documentDetailDTO.getAnnoProtocollo()), "Non devi staccare un altro protocollo!");
		checkAssertion(itemPost.getNumeroProtocollo().equals(documentDetailDTO.getNumeroProtocollo()), "Non devi staccare un altro protocollo!");
	}

}
