package it.ibm.business.test.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.business.test.dto.BonificaMessaAgliAttiDTO;

/**
 * 
 * 
 * @author a.dilegge
 *
 *	Dao bonifiche.
 */
public interface IBonificaDAO extends Serializable {
	
	/**
	 * Recupera gli items da bonificare.
	 * @param numMax - numero massimo
	 * @param conn
	 * @return lista degli items da bonificare
	 */
	List<BonificaMessaAgliAttiDTO> recuperaItemsDaBonificare(Integer numMax, Connection conn);
	
	/**
	 * Aggiorna l'item sottoposto a bonifica.
	 * @param idProtocollo - id del protocollo
	 * @param stato
	 * @param descrizione
	 * @param conn
	 */
	void aggiornaItemBonificaProtocolliAgliAtti(String idProtocollo, Integer stato, String descrizione, Connection conn);
	
}
