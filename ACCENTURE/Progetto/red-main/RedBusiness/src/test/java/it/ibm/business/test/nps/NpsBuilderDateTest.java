package it.ibm.business.test.nps;

import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.namespace.QName;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.business.test.suite.AbstractTest;
import it.ibm.red.business.dao.INpsConfigurationDAO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.builder.Builder;
import it.ibm.red.business.nps.builder.Director;
import it.ibm.red.business.persistence.model.NpsConfiguration;
import it.ibm.red.business.provider.ApplicationContextProvider;
import npsmessages.v1.it.gov.mef.RichiestaReportRegistroGiornalieroType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType;

/**
 * Builder per date NPS.
 */
public class NpsBuilderDateTest extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NpsBuilderDateTest.class);
	
	/**
	 * Data a.
	 */
	private Date dateA;

	/**
	 * Data da.
	 */
	private Date dateDa;

	/**
	 * Codice registro.
	 */
	private String codiceRegistro;

	/**
	 * Denomninazione registro.
	 */
	private String denominazioneRegistro;

	/**
	 * Operatore.
	 */
	private OrgOrganigrammaType operatore;

	/**
	 * Connessione.
	 */
	private Connection con;
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public void setBefore() {
		super.before();
		dateA = new Date(1549321200000L);
		dateDa = new Date(1501538400000L);
		codiceRegistro = "REGISTRO UFFICIALE";
		denominazioneRegistro = "REGISTRO UFFICIALE";
		
		INpsConfigurationDAO npsConfigurationDAO = ApplicationContextProvider.getApplicationContext().getBean(INpsConfigurationDAO.class);
		UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		
		con = createNewDBConnection();
		
		NpsConfiguration npsConfigurationAoo = npsConfigurationDAO.getByIdAoo(utente.getIdAoo().intValue(), con);
		
		operatore = Builder.buildOrganigrammaPf(String.valueOf(utente.getId()),	utente.getCognome(), utente.getNome(), utente.getCodFiscale(),
				npsConfigurationAoo.getCodiceAmministrazione(),	npsConfigurationAoo.getDenominazioneAmministrazione(), npsConfigurationAoo.getCodiceAoo(), 
				npsConfigurationAoo.getDenominazioneAmministrazione(), utente.getCodiceUfficio(), utente.getNodoDesc(), String.valueOf(utente.getIdUfficio()));
	}
	
	/**
	 * Esegue il test di crazione XML.
	 * @throws DatatypeConfigurationException
	 * @throws JAXBException
	 */
	@Test
	public void createXml() {
		try {
			RichiestaReportRegistroGiornalieroType obj = Director.constructReportRegistroGiornalieroInput(dateDa, dateA, operatore, codiceRegistro, denominazioneRegistro);
			QName name = new QName("root");
			JAXBElement<RichiestaReportRegistroGiornalieroType> elem = new JAXBElement<>(name, RichiestaReportRegistroGiornalieroType.class, obj);
			
			JAXBContext jaxbContext = JAXBContext.newInstance(RichiestaReportRegistroGiornalieroType.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			
			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(elem, sw);
			
			String xmlString = sw.toString();
			LOGGER.info(xmlString);
		} catch (JAXBException | DatatypeConfigurationException e) {
			throw new RedException(e);
		}
	}
	
	/**
	 * Esegue le azioni dopo il test.
	 * @throws SQLException
	 */
	@After
	public void closeResource() throws SQLException {
		if (con != null) {
			con.close();
		}
	}
}
