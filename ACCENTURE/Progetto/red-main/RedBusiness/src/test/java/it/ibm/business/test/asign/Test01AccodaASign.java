package it.ibm.business.test.asign;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.SignStrategyEnum;

/**
 * Classe di test per l'accodamento degli item della firma asincrona.
 */
public class Test01AccodaASign extends AbstractAsignTest {

	/**
	 * Messaggio di errore che comunica l'assenza dell'item nella coda dell'utente.
	 */
	private static final String ITEM_IN_CODA_UTENTE = "L'item deve essere nella coda dell'utente";
	
	/**
	 * Messaggio generico di errore sulla firma.
	 */
	private static final String SIGN_ERROR_MSG = "La firma non è andata a buon fine.";

	/**
	 * Esegue tutte le azioni di data preparation per la corretta esecuzione dei test presenti nella classe.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue azioni finali subito dopo l'esecuzione del test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Esegue una firma completa seguendo la strategia A. @see AAsyncSignStrategy.
	 */
	@Test
	public final void firmaStrategiaA() {
		//La strategia A è PROTOCOLLAZIONE SINCRONA
		UtenteDTO utente = getUtente(USERNAME_DF);
		SignStrategyEnum flagStrategia = SignStrategyEnum.get(dumpTestInfo(utente).getFlagStrategiaFirma());
		checkAssertion(SignStrategyEnum.ASYNC_STRATEGY_A.equals(flagStrategia), "La data preparation è errata, stiamo testando la strategia A.");
		EsitoSalvaDocumentoDTO esitoCreazione = createDocAutoassegnatoFirma(null, utente, ID_TIPO_DOC_GENERICO_DF_USCITA, ID_TIPO_PROC_GENERICO_DF_USCITA, null);
		EsitoOperazioneDTO esito = sign(utente, esitoCreazione.getWobNumber());
		checkAssertion(esito.getFlagEsito(), SIGN_ERROR_MSG);
		checkAssertion(esito.getNumeroProtocollo() != null && esito.getNumeroProtocollo() > 0, "Il documento deve essere protocollato");
		
		if (Boolean.TRUE.equals(isUCB(utente.getIdAoo().intValue()))) {
			checkAssertion(hasRegAus(esitoCreazione.getDocumentTitle(), utente.getIdAoo().intValue()), "Il documento deve avere la registrazione ausiliaria.");
		}
		
		checkIsIncoda(utente, esitoCreazione);
	}

	/**
	 * Esegue una firma completa seguendo la strategia B. @see BAsyncSignStrategy.
	 */
	@Test
	public final void firmaStrategiaB() {
		//La strategia B è PROTOCOLLAZIONE ASINCRONA
		UtenteDTO utente = getUtente(USERNAME_RGS);
		SignStrategyEnum flagStrategia = SignStrategyEnum.get(dumpTestInfo(utente).getFlagStrategiaFirma());
		checkAssertion(SignStrategyEnum.ASYNC_STRATEGY_B.equals(flagStrategia), "La data preparation è errata, stiamo testando la strategia B.");
		EsitoSalvaDocumentoDTO esitoCreazione = createDocAutoassegnatoFirma(null, utente, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, null);
		EsitoOperazioneDTO esito = sign(utente, esitoCreazione.getWobNumber());
		checkAssertion(esito.getFlagEsito(), SIGN_ERROR_MSG);
		checkAssertion(esito.getNumeroProtocollo() == null || esito.getNumeroProtocollo() == 0, "Il documento non deve essere protocollato");
		checkAssertion(!hasRegAus(esitoCreazione.getDocumentTitle(), utente.getIdAoo().intValue()), "Il documento non deve avere la registrazione ausiliaria.");
		checkIsIncoda(utente, esitoCreazione);
	}

	/**
	 * Esegue una firma completa utilizzando il processo asincrono e segue la strategia tipica degli UCB ma senza registrazione ausiliaria.
	 */
	@Test
	public final void firmaStrategiaUCBNoRegAus() {
		UtenteDTO utente = getUtente(USERNAME_LAVORO);
		checkAssertion(dumpTestInfo(utente).getFlagUCB()==1, "Data preparation errata, stiamo testando gli UCB.");
		EsitoSalvaDocumentoDTO esitoCreazione = createDocAutoassegnatoFirma(null, utente, ID_TIPO_DOC_GENERICO_LAVORO_USCITA, ID_TIPO_PROC_GENERICO_LAVORO_USCITA, null);
		EsitoOperazioneDTO esito = sign(utente, esitoCreazione.getWobNumber());
		checkAssertion(esito.getFlagEsito(), SIGN_ERROR_MSG);
		checkAssertion(esito.getNumeroProtocollo() != null && esito.getNumeroProtocollo() > 0, "Il documento deve essere protocollato");
		checkAssertion(!hasRegAus(esitoCreazione.getDocumentTitle(), utente.getIdAoo().intValue()), "Il documento deve avere la registrazione ausiliaria.");
		checkIsIncoda(utente, esitoCreazione);
	}

	/**
	 * Esegue il test sulla visibilità dei documenti durante il processo di firma asincrona.
	 */
	@Test
	public final void conoVisibilita() {
		UtenteDTO utenteRGS1 = getUtente(USERNAME_RGS);
		UtenteDTO utenteRGS2 = getUtente(USERNAME_RGS2);
		UtenteDTO utenteGA = getUtente(USERNAME_GA_RGS);

		String dtRGS1 = createASign(null, utenteRGS1, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, null);
		String dtRGS2 = createASign(null, utenteRGS2, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, null);

		List<ASignItemDTO> itemsRGS1 = getaSignSRV().getItems(utenteRGS1, DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE);
		List<ASignItemDTO> itemsRGS2 = getaSignSRV().getItems(utenteRGS2, DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE);
		List<ASignItemDTO> itemsGA = getaSignSRV().getItems(utenteGA, DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE);
		
		checkAssertion(isIn(dtRGS1, itemsRGS1), ITEM_IN_CODA_UTENTE);
		checkAssertion(!isIn(dtRGS1, itemsRGS2), "L'item non deve essere nella coda dell'utente");
		checkAssertion(isIn(dtRGS1, itemsGA), ITEM_IN_CODA_UTENTE);

		checkAssertion(!isIn(dtRGS2, itemsRGS1), "L'item non deve essere nella coda dell'utente");
		checkAssertion(isIn(dtRGS2, itemsRGS2), ITEM_IN_CODA_UTENTE);
		checkAssertion(isIn(dtRGS2, itemsGA), ITEM_IN_CODA_UTENTE);

	}

}
