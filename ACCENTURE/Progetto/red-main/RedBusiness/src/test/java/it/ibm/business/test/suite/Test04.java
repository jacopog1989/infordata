package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Test 04.
 */
public class Test04 extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test04.class);
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}
	
	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test.
	 */
	@Test
	public final void test() {
		//Creazoine documento ingresso in competenza ad IGF UFFICIO I
		UtenteDTO utente = getUtente(USERNAME_TANZI);
		Boolean daNonProtocollare = false;
		List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneCompetenzaUfficio(ID_IGF_UFFICIO_I, DESC_IGF_UFFICIO_I));
		assegnazioni.add(createAssegnazioneConoscenzaUtente(ID_IGF_UFFICIO_III, ID_GIANCARLO_ROSSI, "GIANCARLO", "ROSSI"));
		Integer idMezzoRicezione = ID_MEZZO_RICEZIONE_POSTA_ELET_DA_PEC_PER_ELETTRONICI;
		String indiceFascicoloProcedimentale = null;

		Boolean riservato = false;
		String oggetto = createObj();
		List<AllegatoDTO> allegati = null;
		EsitoSalvaDocumentoDTO esitoCreazione = createDocIngresso(allegati, oggetto, utente, ID_CONTATTO_CTP_RIMINI, FormatoDocumentoEnum.ELETTRONICO, ID_TIPO_DOC_GENERICO_RGS_ENTRATA, ID_TIPO_PROC_GENERICO_RGS_ENTRATA, assegnazioni, daNonProtocollare, idMezzoRicezione, indiceFascicoloProcedimentale, riservato);
		checkAssertion(esitoCreazione.isEsitoOk(), "Errore in fase di creazione.");
		String dt = esitoCreazione.getDocumentTitle();
		EsitoOperazioneDTO esitoStorno = storna(USERNAME_ANDREANI, dt, ID_IGF_UFFICIO_II, null);
		checkAssertion(esitoStorno.getFlagEsito(), "Errore in fase di storno.");
		EsitoOperazioneDTO esitoPresaVisione = presaVisione(USERNAME_ROSSI, dt);
		checkAssertion(esitoPresaVisione.getFlagEsito(), "Errore in fase di presa visione.");
		EsitoOperazioneDTO esitoSecondoStorno = storna(USERNAME_CRUPI, dt, ID_IGF_UFFICIO_II, ID_SABRINA_CRUPI);
		checkAssertion(esitoSecondoStorno.getFlagEsito(), "Errore in fase di secondo storno.");

		setTitolario(USERNAME_CRUPI, dt, "A");
		
		EsitoOperazioneDTO esitoMessaAtti = messaAgliAtti(USERNAME_CRUPI, dt);
		checkAssertion(esitoMessaAtti.getFlagEsito(), "Errore in fase di Messa atti.");
		
		LOGGER.info("DOCUMENT TITLE : " + dt);
		LOGGER.info("OGGETTO : " + oggetto);
		LOGGER.info("NUM DOC : " + getNumDoc(USERNAME_NUNZI, dt));
	}

}
