package it.ibm.business.test.suite;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.security.auth.Subject;
import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.filenet.api.collection.IndependentObjectSet;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.CustomObject;
import com.filenet.api.core.Document;
import com.filenet.api.core.Domain;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.property.FilterElement;
import com.filenet.api.property.PropertyFilter;
import com.filenet.api.query.SearchSQL;
import com.filenet.api.query.SearchScope;
import com.filenet.api.util.UserContext;
import com.google.common.net.MediaType;

import filenet.vw.api.VWException;
import filenet.vw.api.VWFetchType;
import filenet.vw.api.VWQueue;
import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWRoster;
import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWSession;
import filenet.vw.api.VWWorkObject;
import it.ibm.business.test.QueueInfo;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NotaDTO;
import it.ibm.red.business.dto.RecuperoCodeDTO;
import it.ibm.red.business.dto.RicercaResultDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ColoreNotaEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.RicercaPerBusinessEntityEnum;
import it.ibm.red.business.enums.SalvaDocumentoErroreEnum;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.enums.TipoApprovazioneEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoStrutturaNodoEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAssegnaUfficioSRV;
import it.ibm.red.business.service.IAttiSRV;
import it.ibm.red.business.service.IChiudiSRV;
import it.ibm.red.business.service.IContributoSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.IInvioInFirmaSRV;
import it.ibm.red.business.service.IModificaProcedimentoSRV;
import it.ibm.red.business.service.INextStepSRV;
import it.ibm.red.business.service.IOperationDocumentSRV;
import it.ibm.red.business.service.IReinviaSRV;
import it.ibm.red.business.service.IRiassegnazioneSRV;
import it.ibm.red.business.service.IRicercaSRV;
import it.ibm.red.business.service.IRichiesteVistoSRV;
import it.ibm.red.business.service.IRifiutaSpedizioneSRV;
import it.ibm.red.business.service.ISignSRV;
import it.ibm.red.business.service.IStoricoSRV;
import it.ibm.red.business.service.IVistiSRV;
import it.ibm.red.business.service.concrete.AttiSRV;
import it.ibm.red.business.service.facade.IASignFacadeSRV;
import it.ibm.red.business.service.facade.IFaldoneFacadeSRV;
import it.ibm.red.business.service.facade.IFirmatoSpeditoFacadeSRV;
import it.ibm.red.business.service.facade.INotaFacadeSRV;
import it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV;
import it.ibm.red.business.service.facade.IRiattivaProcedimentoFacadeSRV;
import it.ibm.red.business.service.facade.IRubricaFacadeSRV;
import it.ibm.red.business.service.facade.ISalvaDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.ISpeditoFacadeSRV;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;
import it.ibm.red.business.utils.FileUtils;

/**
 * Classe astratta utilizzata per contenere le caratteristiche comuni dei test.
 * 
 * @author CPIERASC
 */
public abstract class AbstractTest {

	/**
	 * Key del documento per query.
	 */
	private static final String ID_DOCUMENTO_LABEL = "idDocumento = ";

	/**
	 * Nome file del file pdf per test.
	 */
	private static final String FILENAME_TEST = "fileTest.pdf";

	/**
	 * Messaggio comunicativo test semplice.
	 */
	private static final String SIMPLE_TEST = "Un semplice test.";

	/**
	 * Messaggio comunicativo test assegnazione.
	 */
	private static final String TEST_ASSEGNAZIONE_MESSAGE = "Test di assegnazione.";

	/**
	 * Messaggio di comunicazione esecuzione di Junit.
	 */
	private static final String JUNIT_TEST_MESSAGE = "Junit Test!";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AbstractTest.class.getName());

	/**
	 * Id firma dirigente.
	 */
	protected static final Integer FIRMA_DIRIGENTE_RGS = 1;

	/**
	 * Id firma ispettore.
	 */
	protected static final Integer FIRMA_ISPETTORE = 2;

	/**
	 * Id firma ragioniere.
	 */
	protected static final Integer FIRMA_RAGIONIERE = 3;

	/**
	 * Path del file di configurazione di Spring.
	 */
	private static final String PATH = "it/ibm/business/test/applicationContextTest.xml";

	/**
	 * Servizio gestione utente.
	 */
	private IUtenteFacadeSRV usrFacade;

	/**
	 * Servizio riattivazione procedimento.
	 */
	private IRiattivaProcedimentoFacadeSRV riattivaSRV;

	/**
	 * Servizio gestione nota.
	 */
	private INotaFacadeSRV notaSRV;

	/**
	 * Servizio ricerca.
	 */
	private IRicercaSRV ricercaSRV;

	/**
	 * Utente.
	 */
	protected static final String USERNAME_PELLEGRINI = "IRENE.PELLEGRINI";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_TRIMARCHI = "P.TRIMARCHI";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_MARCHIAN = "SERGIO.MARCHIAN";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_ROSSI = "GIANCARLO.ROSSI";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_MAZZOTTA = "BIAGIO.MAZZOTTA";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_TANZI = "GIANFRANCO.TANZI"; // Corriere IGF

	/**
	 * Utente.
	 */
	protected static final String USERNAME_ALI = "FRANCESCO.ALI"; // Corriere IGF

	/**
	 * Utente.
	 */
	protected static final String USERNAME_ANDREANI = "ADRIANA.ANDREANI"; // Un utente di IGF UFFICIO I

	/**
	 * Utente.
	 */
	protected static final String USERNAME_CRUPI = "SABRINA.CRUPI";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_NUNZI = "ROBERTO.NUNZI";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_CORBO = "FABRIZIO.CORBO"; // 1465

	/**
	 * Utente.
	 */
	protected static final String USERNAME_LUCARELLI = "MARIA.LUCARELLI";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_PIETROBONO = "LINO.PIETROBONO";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_RUCCIA = "RITA.RUCCIA";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_MONSURRO = "NATALE.MONSURRO";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_BONTEMPI = "FABIO.BONTEMPI";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_TUCCI = "RENATO.TUCCI";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_TONETTI = "ALESSANDRO.TONETTI";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_MICOCCI = "MARIATERESA.MICOCCI";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_SMERIGLIO = "MANUELA.SMERIGLIO";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_ASCI = "RENZO.ASCI";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_RAGIONIERE = "RAGIONIERE.GENERALE";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_RICCARDI = "GIAMPIERO.RICCARDI";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_DINUZZO = "CARMINE.DINUZZO";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_FORMISANO = "ONOFRIO.FORMISANO";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_LATTANZIO = "GM.LATTANZIO";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_TOMARO = "ANGELA.TOMARO";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_DARI = "BETTINA.DARI";
	
	/**
	 * Utente.
	 */
	protected static final String USERNAME_GOZZI = "ROSELLA.GOZZI";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_ZAZZA = "EMANUELA.ZAZZA";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_MINNIELLI = "LUIGI.MINNIELLI";

	/**
	 * Utente.
	 */
	protected static final String USERNAME_PAOLUCCI = "MARIAADELE.PAOLUCCI";

	/**
	 * ID Ufficio.
	 */
	protected static final Integer ID_UCRA = 228;

	/**
	 * Descrizione Ufficio.
	 */
	protected static final String DESC_UCRA = "UCRA";

	/**
	 * ID Ufficio.
	 */
	protected static final Integer ID_IGF = 96;

	/**
	 * Descrizione Ufficio.
	 */
	protected static final String DESC_IGF = "IGF";

	/**
	 * ID Ufficio.
	 */
	protected static final Integer ID_IGB = 64;

	/**
	 * Descrizione Ufficio.
	 */
	protected static final String DESC_IGB = "IGB";

	/**
	 * ID Ufficio.
	 */
	protected static final Integer ID_IGF_CORRIERE_UFFICIO_I = 107;

	/**
	 * Descrizione Ufficio.
	 */
	protected static final String DESC_IGF_CORRIERE_UFFICIO_I = "IGF - UFFICIO I - Corriere";

	/**
	 * ID Ufficio.
	 */
	protected static final Integer ID_IGB_UFFICIO_I = 288;

	/**
	 * Descrizione Ufficio.
	 */
	protected static final String DESC_IGB_UFFICIO_I = "IGB - Ufficio I";

	/**
	 * ID Ufficio.
	 */
	protected static final Integer ID_IGF_UFFICIO_I = 106;

	/**
	 * Descrizione Ufficio.
	 */
	protected static final String DESC_IGF_UFFICIO_I = "IGF - Ufficio I";

	/**
	 * ID Ufficio.
	 */
	protected static final Integer ID_IGF_UFFICIO_II = 108;

	/**
	 * Descrizione Ufficio.
	 */
	protected static final String DESC_IGF_UFFICIO_II = "IGF - Ufficio II";

	/**
	 * ID Ufficio.
	 */
	protected static final Integer ID_IGF_UFFICIO_III = 110;

	/**
	 * Descrizione Ufficio.
	 */
	protected static final String DESC_IGF_UFFICIO_III = "IGF - Ufficio III";

	/**
	 * ID Ufficio.
	 */
	protected static final Integer ID_UCP_RGS = 30;

	/**
	 * Descrizione Ufficio.
	 */
	protected static final String DESC_UCP_RGS = "UCP - RGS";

	/**
	 * ID Ufficio.
	 */
	protected static final Integer ID_IGICS_UFFICIO_I = 130;

	/**
	 * Descrizione Ufficio.
	 */
	protected static final String DESC_IGICS_UFFICIO_I = "IGICS - UFFICIO I";

	/**
	 * ID Ufficio.
	 */
	protected static final Integer ID_IGICS = 128;

	/**
	 * Descrizione Ufficio.
	 */
	protected static final String DESC_IGICS = "IGICS";

	/**
	 * ID Ufficio.
	 */
	protected static final Integer ID_IGICS_UFFICIO_IV = 242;

	/**
	 * Descrizione Ufficio.
	 */
	protected static final String DESC_IGICS_UFFICIO_IV = "IGICS UFFICIO IV";

	/**
	 * Identiicativo comune.
	 */
	protected static final Integer ID_COMUNE_MONSELICE = 342641;

	/**
	 * Identificativo contatto.
	 */
	protected static final Integer ID_CONTATTO_CTP_RIMINI = 903580;

	/**
	 * Identificativo contatto.
	 */
	protected static final Integer ID_CONTATTO_IRENE = 8118;

	/**
	 * Identificativo contatto.
	 */
	protected static final Integer ID_CONTATTO_UNO = 915003;

	/**
	 * Identificativo contatto.
	 */
	protected static final Integer ID_CONTATTO_GIANCARLO_ROSSI = 1043436;

	/**
	 * Identificativo tipo documento.
	 */
	protected static final Integer ID_TIPO_DOC_GENERICO_RGS_ENTRATA = 22;

	/**
	 * Identificativo tipo procedimento.
	 */
	protected static final Integer ID_TIPO_PROC_GENERICO_RGS_ENTRATA = 128;

	/**
	 * Identificativo tipo procedumento.
	 */
	protected static final Integer ID_TIPO_PROC_GENERICO_RGS_USCITA = 233;

	/**
	 * Identificativo tipo documento.
	 */
	protected static final Integer ID_TIPO_DOC_GENERICO_RGS_USCITA = 60;

	/**
	 * Identificativo mezzo spedizione.
	 */
	protected static final Integer ID_MEZZO_RICEZIONE_SCANNER_PER_CARTACEI = 16;

	/**
	 * Identificativo mezzo spedizione.
	 */
	protected static final Integer ID_MEZZO_RICEZIONE_POSTA_ELET_DA_PEC_PER_ELETTRONICI = 2;

	/**
	 * Identificativo doc ingresso.
	 */
	protected static final Integer ID_TIPO_CATEGORIA_ENTRATA_DOC_INGRESSO = 1;

	/**
	 * Identificativo doc uscita.
	 */
	protected static final Integer ID_TIPO_CATEGORIA_USCITA_DOC_USCITA = 2;

	/**
	 * Id Utente.
	 */
	protected static final Integer ID_GIANCARLO_ROSSI = 797;

	/**
	 * Id Utente.
	 */
	protected static final Integer ID_SABRINA_CRUPI = 911;

	/**
	 * Id Utente.
	 */
	protected static final Integer ID_GIANFANCO_TANZI = 1476;

	/**
	 * Id Utente.
	 */
	protected static final Integer ID_ADRIANA_ANDREANI = 1370;

	/**
	 * Id Utente.
	 */
	protected static final Integer ID_ROBERTO_NUNZI = 1221;

	/**
	 * Id Utente.
	 */
	protected static final Integer ID_RENZO_ASCI = 1040;

	/**
	 * Id Utente.
	 */
	protected static final Integer ID_GIORGIO_ALESSANDRI = 1258;

	/**
	 * Id Utente.
	 */
	protected static final Integer ID_MAZZOTTA = 474;

	/**
	 * Id Utente.
	 */
	protected static final Integer ID_RICCARDI = 1541;

	/**
	 * Id Utente.
	 */
	protected static final Integer ID_CORBO = 1465;

	/**
	 * Id Utente.
	 */
	protected static final Integer ID_FRANCESCO_ALI = 396;

	/**
	 * Id Utente.
	 */
	protected static final Integer ID_GM_LATTANZIO = 444;

	/**
	 * Id Utente.
	 */
	protected static final Integer ID_EM_ZAZZA = 447;

	/**
	 * Id Utente.
	 */
	protected static final Integer ID_PAOLO_POGGESI = 445;

	/**
	 * Id Utente.
	 */
	protected static final Integer ID_ZAZZA = 447;

	/**
	 * Id Aoo.
	 */
	protected static final Integer ID_AOO_RGS = 25;

	/**
	 * Id Utente.
	 */
	protected static final Integer ID_FORMISANO = 1418;

	/**
	 * Id Utente.
	 */
	protected static final Integer ID_CARMINE_DINUZZO = 317;

	/**
	 * Id Utente.
	 */
	protected static final Integer ID_ANGELA_TOMARO = 230;

	/**
	 * Id Utente.
	 */
	protected static final Integer ID_SERGIO_MARCHIAN = 452;

	/**
	 * Servizio salvataggio documento.
	 */
	private ISalvaDocumentoFacadeSRV creaDocFacade;

	/**
	 * Servizio gestione rubrica.
	 */
	private IRubricaFacadeSRV rubricaSRV;

	/**
	 * Servizio riassegnazione.
	 */
	private IRiassegnazioneSRV riassegnazioneSRV;

	/**
	 * Servizio atti.
	 */
	private IAttiSRV attiSRV;

	/**
	 * Servizio next step pe.
	 */
	private INextStepSRV nextStepSRV;

	/**
	 * Servizio modifica procedimento.
	 */
	private IModificaProcedimentoSRV modProcSRV;

	/**
	 * Servizio azioni sui documenti.
	 */
	private IOperationDocumentSRV operationDocSRV;

	/**
	 * Servizio azioni sui workflow.
	 */
	private IOperationWorkFlowFacadeSRV operationWFSRV;

	/**
	 * Servizio gestione fascicolo.
	 */
	private IFascicoloSRV fascicoloSRV;

	/**
	 * Servizio firma.
	 */
	private ISignSRV signSRV;

	/**
	 * Servizio gestione firma asincrona.
	 */
	private IASignFacadeSRV aSignSRV;
	
	/**
	 * Servizio gestione spedizione.
	 */
	private ISpeditoFacadeSRV speditoSRV;

	/**
	 * Servizio gestione faldone.
	 */
	private IFaldoneFacadeSRV faldoneSRV;

	/**
	 * Servizio gestione firma e spedizione.
	 */
	private IFirmatoSpeditoFacadeSRV firmatoSpeditoSRV;

	/**
	 * Servizio gestione rifiuto spedizione.
	 */
	private IRifiutaSpedizioneSRV rifiutaSpedizioneSRV;

	/**
	 * Servizio assegnazione ufficio.
	 */
	private IAssegnaUfficioSRV assegnaUfficioSRV;

	/**
	 * Servizio chiusura documenti.
	 */
	private IChiudiSRV chiudiSRV;

	/**
	 * Servizio reinvia documenti.
	 */
	private IReinviaSRV reinviaSRV;

	/**
	 * Servizio modifica procedimento.
	 */
	private IModificaProcedimentoSRV modificaIterSRV;

	/**
	 * Servizio invio in firma.
	 */
	private IInvioInFirmaSRV invioInFirmaSRV;

	/**
	 * Servizio gestione visti.
	 */
	private IVistiSRV vistisrv;

	/**
	 * Servizio richiesta visto.
	 */
	private IRichiesteVistoSRV richiesteVistoSrv;

	/**
	 * Servizio gestione contributi.
	 */
	private IContributoSRV contributiSRV;

	/**
	 * Servizio gestione storico.
	 */
	private IStoricoSRV storicoSRV;

	/**
	 * Datasource applicativo.
	 */
	private DataSource dataSource;

	/**
	 * Datasource dwh.
	 */
	private DataSource dataSourceFilenet;
	
	/**
	 * Chiave property op proxy.
	 */
	private static final String PROPERTY_OP_PROXY = "opProxy";

	/**
	 * Chiave property anno protocollo.
	 */
	private static final String PROPERTY_ANNO_PROTOCOLLO = "annoProtocollo";

	/**
	 * Chiave property numero protocollo.
	 */
	private static final String PROPERTY_NUMERO_PROTOCOLLO = "numeroProtocollo";

	/**
	 * Classe elenco ordini pagamento.
	 */
	private static final String CLASSNAME_ELENCO_ORDINI_PAGAMENTO_DD = "ElencoOrdiniPagamentoDD";

	/**
	 * Chiave property beneficiario.
	 */
	private static final String PROPERTY_BENEFICIARIO = "beneficiario";

	/**
	 * Chiave property oggetto.
	 */
	private static final String PROPERTY_OGGETTO = "oggetto";

	/**
	 * Chiave property data emissione OP.
	 */
	private static final String PROPERTY_DATA_EMISSIONE_OP = "dataEmissioneOP";

	/**
	 * Chiave property id fascicolo FEPA.
	 */
	private static final String PROPERTY_ID_FASCICOLO_FEPA = "idFascicoloFEPA";

	/**
	 * Chiave property tipo op.
	 */
	private static final String PROPERTY_TIPO_OP = "tipoOp";

	/**
	 * Chiave property stato op.
	 */
	private static final String PROPERTY_STATO_OP = "statoOP";

	/**
	 * Chiave property piano gestionale.
	 */
	private static final String PROPERTY_PIANO_GESTIONE = "pianoGestione";

	/**
	 * Chiave property numero op.
	 */
	private static final String PROPERTY_NUMERO_OP = "numeroOP";

	/**
	 * Chiave property capitolo.
	 */
	private static final String PROPERTY_CAPITOLO = "CAPITOLO_NUMERI_SIRGS";

	/**
	 * Chiave property codice ragioneria.
	 */
	private static final String PROPERTY_CODICE_RAGIONERIA = "codiceRagioneria";

	/**
	 * Chiave property amministrazione.
	 */
	private static final String PROPERTY_AMMINISTRAZIONE = "amministraz";

	/**
	 * Chiave property esercizio.
	 */
	private static final String PROPERTY_ESERCIZIO = "ESERCIZIO";

	/**
	 * Chiave property proxy decreto.
	 */
	private static final String PROPERTY_PROXY_DECRETO = "proxyDecreto";

	/**
	 * Chiave property classe ordine di pagamento DD.
	 */
	private static final String CLASSNAME_ORDINE_DI_PAGAMENTO_DD = "OrdineDiPagamentoDD";
	
	/**
	 * Get utente.
	 * 
	 * @param username username
	 * @return utente
	 */
	public UtenteDTO getUtente(final String username) {
		return usrFacade.getByUsername(username);
	}

	/**
	 * Metodo da eseguire prima di avviare il test.
	 */
	protected void before() {
		System.setProperty("com.mchange.v2.log.MLog", "com.mchange.v2.log.FallbackMLog");
		System.setProperty("com.mchange.v2.log.FallbackMLog.DEFAULT_CUTOFF_LEVEL", "WARNING");

		final ApplicationContext ctx = new ClassPathXmlApplicationContext("./" + PATH);
		ApplicationContextProvider.setApplicationContextForTestPurpose(ctx);
		dataSource = (DataSource) ApplicationContextProvider.getApplicationContext().getBean("DataSource");
		dataSourceFilenet = (DataSource) ApplicationContextProvider.getApplicationContext().getBean("FilenetDataSource");
		usrFacade = ApplicationContextProvider.getApplicationContext().getBean(IUtenteFacadeSRV.class);
		creaDocFacade = ApplicationContextProvider.getApplicationContext().getBean(ISalvaDocumentoFacadeSRV.class);
		rubricaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaFacadeSRV.class);
		riassegnazioneSRV = ApplicationContextProvider.getApplicationContext().getBean(IRiassegnazioneSRV.class);
		attiSRV = ApplicationContextProvider.getApplicationContext().getBean(AttiSRV.class);
		riattivaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRiattivaProcedimentoFacadeSRV.class);
		notaSRV = ApplicationContextProvider.getApplicationContext().getBean(INotaFacadeSRV.class);
		ricercaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaSRV.class);
		modProcSRV = ApplicationContextProvider.getApplicationContext().getBean(IModificaProcedimentoSRV.class);
		operationDocSRV = ApplicationContextProvider.getApplicationContext().getBean(IOperationDocumentSRV.class);
		nextStepSRV = ApplicationContextProvider.getApplicationContext().getBean(INextStepSRV.class);
		fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloSRV.class);
		operationWFSRV = ApplicationContextProvider.getApplicationContext().getBean(IOperationWorkFlowFacadeSRV.class);
		aSignSRV = ApplicationContextProvider.getApplicationContext().getBean(IASignFacadeSRV.class);
		signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignSRV.class);
		speditoSRV = ApplicationContextProvider.getApplicationContext().getBean(ISpeditoFacadeSRV.class);
		faldoneSRV = ApplicationContextProvider.getApplicationContext().getBean(IFaldoneFacadeSRV.class);
		firmatoSpeditoSRV = ApplicationContextProvider.getApplicationContext().getBean(IFirmatoSpeditoFacadeSRV.class);
		assegnaUfficioSRV = ApplicationContextProvider.getApplicationContext().getBean(IAssegnaUfficioSRV.class);
		chiudiSRV = ApplicationContextProvider.getApplicationContext().getBean(IChiudiSRV.class);
		reinviaSRV = ApplicationContextProvider.getApplicationContext().getBean(IReinviaSRV.class);
		modificaIterSRV = ApplicationContextProvider.getApplicationContext().getBean(IModificaProcedimentoSRV.class);
		invioInFirmaSRV = ApplicationContextProvider.getApplicationContext().getBean(IInvioInFirmaSRV.class);
		rifiutaSpedizioneSRV = ApplicationContextProvider.getApplicationContext().getBean(IRifiutaSpedizioneSRV.class);
		vistisrv = ApplicationContextProvider.getApplicationContext().getBean(IVistiSRV.class);
		contributiSRV = ApplicationContextProvider.getApplicationContext().getBean(IContributoSRV.class);
		richiesteVistoSrv = ApplicationContextProvider.getApplicationContext().getBean(IRichiesteVistoSRV.class);
		storicoSRV = ApplicationContextProvider.getApplicationContext().getBean(IStoricoSRV.class);
	}

	/**
	 * Esegue la ricerca generica.
	 * 
	 * @param key
	 *            chiave
	 * @param anno
	 *            anno ricerca
	 * @param type
	 *            tipo ricerca
	 * @param entity
	 *            entità della ricerca
	 * @param username
	 *            utente per la ricerca
	 * @return documenti recuperati
	 */
	protected Collection<MasterDocumentRedDTO> ricerca(final String key, final Integer anno, final RicercaGenericaTypeEnum type, final RicercaPerBusinessEntityEnum entity,
			final String username) {
		final UtenteDTO utente = getUtente(username);
		return ricercaSRV.ricercaGenerica(key, anno, type, entity, utente);
	}

	/**
	 * Esegue la response <em> riattiva </em> sul documento identificato dal
	 * {@code documentTitle}.
	 * 
	 * @param usernameOperatore
	 *            utente operatore che esegue la response
	 * @param documentTitle
	 *            identificativo del documento su cui eseguire la response
	 * @return esito dell'esecuzione
	 */
	protected EsitoOperazioneDTO riattiva(final String usernameOperatore, final String documentTitle) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		return riattivaSRV.riattiva(documentTitle, operatore, "Un semplice test di riattivazione.");
	}

	/**
	 * Esegue la response <em> metti in sospeso </em> sul documento identificato dal
	 * {@code documentTitle}.
	 * 
	 * @param usernameOperatore
	 *            utente operatore che esegue la response
	 * @param documentTitle
	 *            identificativo del documento su cui eseguire la response
	 * @return esito dell'esecuzione
	 */
	protected EsitoOperazioneDTO messaInSospeso(final String usernameOperatore, final String documentTitle) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.DA_LAVORARE, documentTitle, operatore.getIdUfficio().intValue(),
				operatore.getId().intValue());
		return nextStepSRV.mettiInSospeso(operatore, wo.getWorkObjectNumber());
	}

	/**
	 * Aggiunge una nota sul documento identificato dal {@code wobNumber}.
	 * 
	 * @param usernameOperatore
	 *            utente che crea e aggiunge la nota
	 * @param colore
	 *            colore della nota
	 * @param wobNumber
	 *            identificativo del documento su cui aggiungere la nota
	 * @param testo
	 *            testo della nota
	 * @return esito dell'aggiunta della nota
	 */
	protected EsitoOperazioneDTO setNota(final String usernameOperatore, final ColoreNotaEnum colore, final String wobNumber, final String testo) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final NotaDTO nota = new NotaDTO();
		nota.setColore(colore);
		nota.setContentNota(testo);
		return notaSRV.registraNotaFromWorkflow(operatore, wobNumber, nota, null, false);
	}

	/**
	 * Metodo da invocare dopo l'esecuzione del test.
	 */
	protected static final void after() {
	}

	/**
	 * Esegue l'assertion della condizione {@code conditionToVerify} lanciando un
	 * eccezione in caso la condizione sia {@code false} mostrando il messaggio
	 * {@code errorMessage}.
	 * 
	 * @param conditionToVerify
	 *            condizione da verificare
	 * @param errorMessage
	 *            messaggio di errore se la condizione risulta {@code false}
	 */
	protected static void checkAssertion(final Boolean conditionToVerify, final String errorMessage) {
		assertTrue(errorMessage, conditionToVerify);
	}

	/**
	 * Crea e restituisce una connessione al database DWH per l'utilizzo della
	 * stessa nei test.
	 * 
	 * @return connessione a dwh
	 */
	protected Connection createNewDBFileneConnection() {
		return createDBConnection(dataSourceFilenet);
	}

	/**
	 * Crea e restituisce una connessione al database per l'utilizzo della stessa
	 * nei test.
	 * 
	 * @return connessione al database
	 */
	protected Connection createNewDBConnection() {
		return createDBConnection(dataSource);
	}

	/**
	 * Crea la connessione dal datasource fornito.
	 * 
	 * @param ds
	 *            datasource per la connessione
	 * @return connessione
	 */
	private static Connection createDBConnection(final DataSource ds) {
		Connection connection = null;
		try {
			connection = ds.getConnection();
			// Impedisce letture sporche: se una transazione in esecuzione modifica un
			// record questo non può essere letto da un'altra sino a che non avviene il
			// commit.
			connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			// Richiede il commit esplicito a termine esecuzione.
			connection.setAutoCommit(false);
			return connection;
		} catch (final SQLException e) {
			LOGGER.error(e);
			it.ibm.business.test.AbstractTest.closeConnection(connection);
			throw new RedException(e);
		}
	}

	/**
	 * Esegue il rollback della connessione.
	 * 
	 * @param connection
	 *            connessione da rollbackare
	 */
	protected void rollbackConnection(final Connection connection) {
		try {
			connection.rollback();
			connection.close();
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		}
	}

	/**
	 * Esegue il commit della connessione chiudendola.
	 * 
	 * @param connection
	 *            connessione da committare e chiudere
	 */
	protected void commitConnection(final Connection connection) {
		try {
			connection.commit();
			connection.close();
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		}
	}

	/**
	 * Crea lo statement con i parametri in ingresso.
	 * 
	 * @param ps
	 *            prepared statement 
	 * @param parameters
	 *            parametri per il popolamento della query {@code sql}
	 */
	private static void setPreparedStatement(final PreparedStatement ps, final List<? extends Object> parameters) {
		
		try {
			Integer index = 1;
			for (final Object p : parameters) {
				if (p instanceof String) {
					ps.setString(index++, (String) p);
				} else if (p instanceof Integer) {
					ps.setInt(index++, (Integer) p);
				} else {
					throw new RedException("Tipo non riconosciuto");
				}
			}
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		}
	}

	/**
	 * Esegue un update sql.
	 * 
	 * @param con
	 *            connessione al database
	 * @param sql
	 *            query di update
	 * @param parameters
	 *            parametri per il popolamento della query {@code sql}
	 */
	protected void executeUpdate(final Connection con, final String sql, final Object... parameters) {
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql);
			setPreparedStatement(ps, Arrays.asList(parameters));
			ps.executeUpdate();
			commitConnection(con);
		} catch (final SQLException e) {
			rollbackConnection(con);
			throw new RedException(e);
		} finally {
			if(ps != null) {
				try {
					ps.close();
				} catch (SQLException e1) {
					LOGGER.error(e1);
				}
			}
		}
	}

	private com.filenet.api.core.Connection initUserContext(final String userId, final String password, final String uri, final String stanzaJAAS) {
		final UserContext uc = UserContext.get();
		final com.filenet.api.core.Connection con = Factory.Connection.getConnection(uri);
		final Subject sub = UserContext.createSubject(con, userId, password, stanzaJAAS);
		uc.pushSubject(sub);
		return con;
	}

	/**
	 * Restituisce il <em> VWSession </em>.
	 * 
	 * @param uri
	 *            uri del VWSession
	 * @param userId
	 *            id utente
	 * @param connectionPoint
	 *            connection point
	 * @param password
	 *            password
	 * @param stanzaJAAS
	 *            stanza JAAS
	 * @return VWSession
	 */
	protected VWSession getPESession(final String uri, final String userId, final String connectionPoint, final String password, final String stanzaJAAS) {
		VWSession pesession = null;
		try {
			initUserContext(userId, password, uri, stanzaJAAS);
			pesession = new filenet.vw.api.VWSession();
			pesession.setBootstrapCEURI(uri);
			pesession.logon(userId, password, connectionPoint);
		} catch (final VWException e) {
			LOGGER.error(e);
		}
		return pesession;
	}

	private VWSession getAdminPESession() {
		final UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		final FilenetCredentialsDTO fcDTO = utente.getFcDTO();
		return getPESession(fcDTO.getUri(), "p8admin", fcDTO.getConnectionPoint(), fcDTO.getPassword(), fcDTO.getStanzaJAAS());
	}

	/**
	 * Restituisce il <em> VWSession </em>.
	 * 
	 * @param utente
	 *            utente in sessione
	 * @return VWSession
	 */
	protected VWSession getPESession(final UtenteDTO utente) {
		final FilenetCredentialsDTO fcDTO = utente.getFcDTO();
		return getPESession(fcDTO.getUri(), fcDTO.getUserName(), fcDTO.getConnectionPoint(), fcDTO.getPassword(), fcDTO.getStanzaJAAS());
	}

	/**
	 * Restituisce l' <em> Object Store </em> admin.
	 * 
	 * @return object store
	 */
	protected final ObjectStore getObjectStoreAdmin() {
		final UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		final FilenetCredentialsDTO fcDTO = utente.getFcDTO();
		return getObjectStore("p8admin", fcDTO.getPassword(), fcDTO.getUri(), fcDTO.getStanzaJAAS(), utente.getFcDTO().getObjectStore());
	}

	/**
	 * Restituisce l' <em> Object Store </em> con l'utente fornito.
	 * @param utente utente col quale recuperare l'Object Store
	 * @return ObjectStore
	 */
	protected final ObjectStore getObjectStore(final UtenteDTO utente) {
		final FilenetCredentialsDTO fcDTO = utente.getFcDTO();
		return getObjectStore(fcDTO.getUserName(), fcDTO.getPassword(), fcDTO.getUri(), fcDTO.getStanzaJAAS(), utente.getFcDTO().getObjectStore());
	}

	/**
	 * Restituisce l' <em> Object Store </em>.
	 * 
	 * @param userId
	 *            id utente
	 * @param password
	 * @param uri
	 * @param stanzaJAAS
	 * @param objectStore
	 * @return ObjectStore
	 */
	protected final ObjectStore getObjectStore(final String userId, final String password, final String uri, final String stanzaJAAS, final String objectStore) {
		final com.filenet.api.core.Connection con = initUserContext(userId, password, uri, stanzaJAAS);
		final PropertyFilter pf = new PropertyFilter();
		pf.addIncludeProperty(new FilterElement(0, null, Boolean.TRUE, PropertyNames.NAME, null));
		pf.addIncludeProperty(new FilterElement(0, null, Boolean.TRUE, PropertyNames.DOMAIN, null));
		final Domain dom = Factory.Domain.fetchInstance(con, null, pf);
		final ObjectStore os = Factory.ObjectStore.getInstance(dom, objectStore);
		os.fetchProperties(pf);
		return os;
	}

	private static AssegnazioneDTO createAssegnazioneUfficio(final TipoAssegnazioneEnum tipo, final Integer idUfficio, final String descUfficio) {
		final UfficioDTO ufficio = new UfficioDTO(idUfficio.longValue(), descUfficio);
		return new AssegnazioneDTO(null, tipo, null, null, null, null, ufficio);
	}

	private static AssegnazioneDTO createAssegnazioneUtente(final TipoAssegnazioneEnum tipo, final Integer idUfficio, final Integer idUtente, final String nomeUtente,
			final String cognomeUtente) {
		final UtenteDTO inUtente = new UtenteDTO();
		inUtente.setId(idUtente.longValue());
		inUtente.setIdUfficio(idUfficio.longValue());
		inUtente.setNome(nomeUtente);
		inUtente.setCognome(cognomeUtente);
		return new AssegnazioneDTO(null, tipo, null, null, null, inUtente, new UfficioDTO(idUfficio.longValue(), null));
	}

	/**
	 * Crea un'assegnazione per firma all'utente identificato dall'
	 * {@code idUfficio}.
	 * 
	 * @param idUfficio
	 *            identificativo ufficio
	 * @param idUtente
	 *            identificativo utente
	 * @param nomeUtente
	 *            nome utente
	 * @param cognomeUtente
	 *            cognome utente
	 * @return assegnazione creata
	 */
	protected AssegnazioneDTO createAssegnazioneFirma(final Integer idUfficio, final Integer idUtente, final String nomeUtente, final String cognomeUtente) {
		return createAssegnazioneUtente(TipoAssegnazioneEnum.FIRMA, idUfficio, idUtente, nomeUtente, cognomeUtente);
	}

	/**
	 * Crea un'assegnazione per Sigla.
	 * 
	 * @param idUfficio
	 *            identificativo ufficio
	 * @param idUtente
	 *            identificativo utente
	 * @param nomeUtente
	 *            nome utente
	 * @param cognomeUtente
	 *            cognome utente
	 * @return assegnazione per sigla creata
	 */
	protected AssegnazioneDTO createAssegnazioneSigla(final Integer idUfficio, final Integer idUtente, final String nomeUtente, final String cognomeUtente) {
		return createAssegnazioneUtente(TipoAssegnazioneEnum.SIGLA, idUfficio, idUtente, nomeUtente, cognomeUtente);
	}

	/**
	 * Crea un'assegnazione per Visto.
	 * 
	 * @param idUfficio
	 *            identificativo ufficio
	 * @param idUtente
	 *            identificativo utente
	 * @param nomeUtente
	 *            nome utente
	 * @param cognomeUtente
	 *            cognome utente
	 * @return assegnazione per visto
	 */
	protected AssegnazioneDTO createAssegnazioneVisto(final Integer idUfficio, final Integer idUtente, final String nomeUtente, final String cognomeUtente) {
		return createAssegnazioneUtente(TipoAssegnazioneEnum.VISTO, idUfficio, idUtente, nomeUtente, cognomeUtente);
	}

	/**
	 * Crea assegnazione per competenza per l'utente identificato dall'
	 * {@code idUtente}.
	 * 
	 * @param idUfficio
	 *            identificativo ufficio
	 * @param idUtente
	 *            identificativo utente
	 * @param nomeUtente
	 *            nome utente
	 * @param cognomeUtente
	 *            cognome utente
	 * @return assegnazione utente
	 */
	protected AssegnazioneDTO createAssegnazioneCompetenzaUtente(final Integer idUfficio, final Integer idUtente, final String nomeUtente, final String cognomeUtente) {
		return createAssegnazioneUtente(TipoAssegnazioneEnum.COMPETENZA, idUfficio, idUtente, nomeUtente, cognomeUtente);
	}

	/**
	 * Crea assegnazione per conoscenza per l'utente identificato dall'
	 * {@code idUtente}.
	 * 
	 * @param idUfficio
	 *            identificativo ufficio
	 * @param idUtente
	 *            identificativo utente
	 * @param nomeUtente
	 *            nome utente
	 * @param cognomeUtente
	 *            cognome utente
	 * @return assegnazione utente
	 */
	protected AssegnazioneDTO createAssegnazioneConoscenzaUtente(final Integer idUfficio, final Integer idUtente, final String nomeUtente, final String cognomeUtente) {
		return createAssegnazioneUtente(TipoAssegnazioneEnum.CONOSCENZA, idUfficio, idUtente, nomeUtente, cognomeUtente);
	}

	/**
	 * Crea e restituisce l'assegnazione per competenza sull'ufficio.
	 * 
	 * @param idUfficio
	 *            identificativo ufficio
	 * @param descUfficio
	 *            descrizione ufficio
	 * @return assegnazione creata
	 */
	protected AssegnazioneDTO createAssegnazioneCompetenzaUfficio(final Integer idUfficio, final String descUfficio) {
		return createAssegnazioneUfficio(TipoAssegnazioneEnum.COMPETENZA, idUfficio, descUfficio);
	}

	/**
	 * Crea l'assegnazione per conoscenza ufficio.
	 * 
	 * @param idUfficio
	 *            identificativo ufficio
	 * @param descUfficio
	 *            descrizione ufficio
	 * @return assegnazione per conoscenza creata
	 */
	protected AssegnazioneDTO createAssegnazioneConoscenzaUfficio(final Integer idUfficio, final String descUfficio) {
		return createAssegnazioneUfficio(TipoAssegnazioneEnum.CONOSCENZA, idUfficio, descUfficio);
	}

	/**
	 * Crea obj.
	 * 
	 * @return object creato
	 */
	protected String createObj() {
		String obj = "Documento di Test";

		try (DatagramSocket socket = new DatagramSocket()) {
			String clsName = null;
			String mtdName = null;
			final StackTraceElement[] stacks = new Throwable().getStackTrace();
			for (final StackTraceElement stack : stacks) {
				final Class<?> cls = Class.forName(stack.getClassName());
				for (final Method m : cls.getMethods()) {
					if (m.getName().equals(stack.getMethodName()) && m.getAnnotation(org.junit.Test.class) != null) {
						clsName = cls.getSimpleName();
						mtdName = m.getName();
						break;
					}
				}
			}
			obj = "Test " + clsName + "." + mtdName + " eseguito sulla macchina ";
			socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
			obj += socket.getLocalAddress().getHostAddress();
			obj += " in data " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());
		} catch (final Exception e) {
			LOGGER.warn(e);
		}
		return obj;
	}

	/**
	 * Crea un documento senza necessità di utilizzo di GUI, utile per la creazione
	 * di documenti in fase di test.
	 * 
	 * @param allegati
	 *            allegati da aggiungere al documento
	 * @param flagFirmaDigitaleRGS
	 *            flag associato alla checkbox <em> firma digitale </em>
	 * @param oggetto
	 *            oggetto del documento creato
	 * @param utenteCreatore
	 *            utente che risulterà creatore del documento
	 * @param idMittenteContatto
	 *            identificativo dell'utente mittente del documento creato
	 * @param formato
	 *            formato del documento creato
	 * @param categoria
	 *            categoria documento
	 * @param idTipologiaDocumento
	 *            identificativo della tipologia documento
	 * @param idTipologiaProcedimento
	 *            identificativo della tipologia procedimento
	 * @param assegnazioni
	 *            assegnazioni che sono associate al documento
	 * @param destinatari
	 *            destinatari del documento creato
	 * @param indiceFascicoloProcedimentale
	 *            indice del fascicolo procedimentale in cui si trova il documento
	 *            creato
	 * @param daNonProtocollare
	 *            flag associato alla protocollazione del documento creato
	 * @param mezzoRicezione
	 *            mezzo di ricezione del documento
	 * @param momentoProtocollazioneEnum
	 *            momento di protocollazione
	 * @param iterApprovativo
	 *            iter approvativo
	 * @param riservato
	 *            flag associato alla checkbox <em> riservato </em>
	 * @param protocolloRiferimento
	 *            protocollo di riferimento del documento creato
	 * @param idFascicoloFepa
	 *            identificativo del fascicolo FEPA in cui si trova il documento
	 * @return esito della creazione del documento
	 */
	private EsitoSalvaDocumentoDTO createDocument(final List<AllegatoDTO> allegati, final Boolean flagFirmaDigitaleRGS,
			final String oggetto, final UtenteDTO utenteCreatore, final Integer idMittenteContatto,
			final FormatoDocumentoEnum formato, final Integer categoria, final Integer idTipologiaDocumento,
			final Integer idTipologiaProcedimento, final List<AssegnazioneDTO> assegnazioni,
			final List<DestinatarioRedDTO> destinatari, final String indiceFascicoloProcedimentale,
			final Boolean daNonProtocollare, final Integer mezzoRicezione,
			final MomentoProtocollazioneEnum momentoProtocollazioneEnum, final Integer iterApprovativo,
			final Integer riservato, final String protocolloRiferimento, final String idFascicoloFepa) {
		final SalvaDocumentoRedParametriDTO parametri = new SalvaDocumentoRedParametriDTO();
		parametri.setModalita(Constants.Modalita.MODALITA_INS); // Siamo in inserimento
		parametri.setContentVariato(true); // essendo nuovo il content è variato

		if (idFascicoloFepa != null) {
			if (parametri.getMetadatiFascicolo()==null) {
				parametri.setMetadatiFascicolo(new HashMap<>());
			}
			parametri.getMetadatiFascicolo().put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA), idFascicoloFepa);
		}
		
		final DetailDocumentRedDTO document = new DetailDocumentRedDTO();

		document.setProtocolloRiferimento(protocolloRiferimento);

		document.setContent(FileUtils.getFileFromInternalResources(FILENAME_TEST));
		document.setNomeFile(FILENAME_TEST);
		document.setMimeType(MediaType.PDF.toString());

		document.setRiservato(riservato);

		document.setOggetto(oggetto);

		document.setIdCategoriaDocumento(categoria);

		document.setIdTipologiaDocumento(idTipologiaDocumento);
		document.setIdTipologiaProcedimento(idTipologiaProcedimento);

		document.setAssegnazioni(assegnazioni);
		document.setDescrizioneFascicoloProcedimentale(UUID.randomUUID().toString());

		if (allegati != null && !allegati.isEmpty()) {
			document.setAllegati(allegati);
		}

		if (CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0].equals(categoria)) {
			// INGRESSO
			document.setDaNonProtocollare(daNonProtocollare);
			final Contatto contatto = getContatto(idMittenteContatto);
			document.setMittenteContatto(contatto);
			document.setMezzoRicezione(mezzoRicezione);
			document.setFormatoDocumentoEnum(formato);
			document.setIndiceClassificazioneFascicoloProcedimentale(indiceFascicoloProcedimentale);
		} else {
			// USCITA
			boolean destElettronico = false;
			for (final DestinatarioRedDTO destinatario : destinatari) {
				if (MezzoSpedizioneEnum.ELETTRONICO.equals(destinatario.getMezzoSpedizioneEnum())) {
					destElettronico = true;
					break;
				}
			}
			if (destElettronico) {
				final EmailDTO mailSpedizione = new EmailDTO();
				mailSpedizione.setMittente("testred@pec.mef.gov.it");
				mailSpedizione.setDestinatari("a@b.c");
				mailSpedizione.setOggetto("MEF - RGS - Un numero di protocollo valido - Se avessi l'oggetto del documento ce lo metterei.");
				mailSpedizione.setTesto("Ciao Mondo!");
				document.setMailSpedizione(mailSpedizione);
			}

			document.setDestinatari(destinatari);
			document.setIndiceClassificazioneFascicoloProcedimentale(indiceFascicoloProcedimentale);
			document.setMomentoProtocollazioneEnum(momentoProtocollazioneEnum);
			document.setFormatoDocumentoEnum(FormatoDocumentoEnum.ELETTRONICO);
			if (iterApprovativo != null) {
				document.setIdIterApprovativo(iterApprovativo);
			}
			document.setCheckFirmaDigitaleRGS(flagFirmaDigitaleRGS);
		}

		return creaDocFacade.salvaDocumento(document, parametri, utenteCreatore, ProvenienzaSalvaDocumentoEnum.GUI_RED);
	}

	/**
	 * Crea un documento in ingresso.
	 * 
	 * @param allegati
	 *            allegati associati al documento
	 * @param oggetto
	 *            oggetto del documento
	 * @param utenteCreatore
	 *            utente creatore del documento
	 * @param idMittenteContatto
	 *            identificativo mittente del documento
	 * @param formato
	 *            formato del documento creato
	 * @param idTipologiaDocumento
	 *            identificativo tipologia del documento
	 * @param idTipologiaProcedimento
	 *            identificativo tipologia procedimento
	 * @param assegnazioni
	 *            assegnazioni associate al documento
	 * @param daNonProtocollare
	 *            flag associato alla protocollazione del documento
	 * @param mezzoRicezione
	 *            mezzo di ricezione del documento
	 * @param indiceFascicoloProcedimentale
	 *            indice del fascicolo procedimentale
	 * @param bRiservato
	 *            flag associato alla checkbox <em> riservato </em> della maschera
	 * @return esito creazione documento in ingresso
	 */
	protected EsitoSalvaDocumentoDTO createDocIngresso(final List<AllegatoDTO> allegati, final String oggetto, final UtenteDTO utenteCreatore,
			final Integer idMittenteContatto, final FormatoDocumentoEnum formato, final Integer idTipologiaDocumento, final Integer idTipologiaProcedimento,
			final List<AssegnazioneDTO> assegnazioni, final Boolean daNonProtocollare, final Integer mezzoRicezione, final String indiceFascicoloProcedimentale,
			final Boolean bRiservato) {
		return createDocIngresso(allegati, oggetto, utenteCreatore, idMittenteContatto, formato, idTipologiaDocumento, idTipologiaProcedimento, assegnazioni,
				daNonProtocollare, mezzoRicezione, indiceFascicoloProcedimentale, bRiservato, null);
	}

	/**
	 * Crea un documento in ingresso.
	 * 
	 * @param allegati
	 *            allegati associati al documento
	 * @param oggetto
	 *            oggetto del documento
	 * @param utenteCreatore
	 *            utente creatore del documento
	 * @param idMittenteContatto
	 *            identificativo mittente del documento
	 * @param formato
	 *            formato del documento creato
	 * @param idTipologiaDocumento
	 *            identificativo tipologia del documento
	 * @param idTipologiaProcedimento
	 *            identificativo tipologia procedimento
	 * @param assegnazioni
	 *            assegnazioni associate al documento
	 * @param daNonProtocollare
	 *            flag associato alla protocollazione del documento
	 * @param mezzoRicezione
	 *            mezzo di ricezione del documento
	 * @param indiceFascicoloProcedimentale
	 *            indice del fascicolo procedimentale
	 * @param bRiservato
	 *            flag associato alla checkbox <em> riservato </em> della maschera
	 * @param protocolloRiferimento
	 *            protocollo di riferimento del documento
	 * @return esito creazione documento in ingresso
	 */
	protected EsitoSalvaDocumentoDTO createDocIngresso(final List<AllegatoDTO> allegati, final String oggetto, final UtenteDTO utenteCreatore,
			final Integer idMittenteContatto, final FormatoDocumentoEnum formato, final Integer idTipologiaDocumento, final Integer idTipologiaProcedimento,
			final List<AssegnazioneDTO> assegnazioni, final Boolean daNonProtocollare, final Integer mezzoRicezione, final String indiceFascicoloProcedimentale,
			final Boolean bRiservato, final String protocolloRiferimento) {
		Integer riservato = 0;
		if (bRiservato != null && bRiservato) {
			riservato = 1;
		}
		return createDocument(allegati, null, oggetto, utenteCreatore, idMittenteContatto, formato,
				ID_TIPO_CATEGORIA_ENTRATA_DOC_INGRESSO, idTipologiaDocumento, idTipologiaProcedimento, assegnazioni,
				null, indiceFascicoloProcedimentale, daNonProtocollare, mezzoRicezione, null, null, riservato, protocolloRiferimento, null);
	}
	
	/**
	 * Crea un documento in uscita.
	 * 
	 * @param allegati
	 *            allegati associati al documento
	 * @param oggetto
	 *            oggetto del documento
	 * @param utenteCreatore
	 *            utente creatore del documento
	 * @param idTipologiaDocumento
	 *            identificativo tipologia del documento
	 * @param idTipologiaProcedimento
	 *            identificativo tipologia procedimento
	 * @param assegnazioni
	 *            assegnazioni associate al documento
	 * @param indiceFascicoloProcedimentale
	 *            indice del fascicolo procedimentale
	 * @param bRiservato
	 *            flag associato alla checkbox <em> riservato </em> della maschera
	 * @param flagFirmaDigitaleRGS
	 *            flag associato alla checkbox <em> firma digitale RGS </em>
	 * @param destinatari
	 *            destinatari del documento
	 * @param momentoProtocollazioneEnum
	 *            momento protocollazione
	 * @param iterApprovativo
	 *            iter approvativo del documento
	 * @param idFascicoloFepa
	 *            identificativo fascicolo FEPA
	 * @return esito creazione documento in ingresso
	 */
	protected EsitoSalvaDocumentoDTO createDocUscita(final List<AllegatoDTO> allegati, final Boolean flagFirmaDigitaleRGS, final String oggetto,
			final UtenteDTO utenteCreatore, final Integer idTipologiaDocumento, final Integer idTipologiaProcedimento, final List<AssegnazioneDTO> assegnazioni,
			final List<DestinatarioRedDTO> destinatari, final String indiceFascicoloProcedimentale, final MomentoProtocollazioneEnum momentoProtocollazioneEnum,
			final Integer iterApprovativo, final Boolean bRiservato, final String idFascicoloFepa) {
		Integer riservato = 0;
		if (bRiservato != null && bRiservato) {
			riservato = 1;
		}
		return createDocument(allegati, flagFirmaDigitaleRGS, oggetto, utenteCreatore, null, null,
				ID_TIPO_CATEGORIA_USCITA_DOC_USCITA, idTipologiaDocumento, idTipologiaProcedimento, assegnazioni,
				destinatari, indiceFascicoloProcedimentale, null, null, momentoProtocollazioneEnum, iterApprovativo,
				riservato, null, idFascicoloFepa);
	}

	/**
	 * Restituisce il contatto identificato dall' {@code idContatto}.
	 * 
	 * @param idContatto
	 *            identificativo contatto
	 * @return contatto recuperato
	 */
	protected Contatto getContatto(final Integer idContatto) {
		return rubricaSRV.getContattoByID(idContatto.longValue());
	}

	/**
	 * Crea un destinatario interno.
	 * 
	 * @param idNodo
	 *            identificativo nodo
	 * @param idUtente
	 *            identificativo utente
	 * @param idContatto
	 *            identificativo contatto
	 * @param mode
	 *            modalità destinatario
	 * @return destinatario
	 */
	protected DestinatarioRedDTO createDestinatarioInterno(final Integer idNodo, final Integer idUtente, final Integer idContatto, final ModalitaDestinatarioEnum mode) {
		final Contatto contatto = getContatto(idContatto);
		final DestinatarioRedDTO destInt = new DestinatarioRedDTO(idNodo.longValue(), idUtente.longValue(), TipologiaDestinatarioEnum.INTERNO.getTipologia(), mode.toString());
		destInt.setContatto(contatto);
		return destInt;
	}

	/**
	 * Crea destinatario esterno.
	 * 
	 * @param mezzoSpedizione
	 *            mezzo di spedizione
	 * @param idContatto
	 *            identificativo contatto
	 * @param mode
	 *            modalità destinatario
	 * @return destinatario esterno creato
	 */
	protected DestinatarioRedDTO createDestinatarioEsterno(final MezzoSpedizioneEnum mezzoSpedizione, final Integer idContatto, final ModalitaDestinatarioEnum mode) {
		final Contatto contatto = getContatto(idContatto);
		final DestinatarioRedDTO destEst = new DestinatarioRedDTO(mezzoSpedizione.getId(), TipologiaDestinatarioEnum.ESTERNO.getTipologia(), mode.toString());
		destEst.setContatto(contatto);
		return destEst;
	}

	/**
	 * Crea assegnazione per spedizione utente.
	 * 
	 * @param idUfficio
	 *            identificativo ufficio
	 * @param idUtente
	 *            identificativo utente
	 * @param nomeUtente
	 *            nome utente
	 * @param cognomeUtente
	 *            cognome utente
	 * @return assegnazione creata
	 */
	protected AssegnazioneDTO createAssegnazioneSpedizioneUtente(final Integer idUfficio, final Integer idUtente, final String nomeUtente, final String cognomeUtente) {
		return createAssegnazioneUtente(TipoAssegnazioneEnum.COMPETENZA, idUfficio, idUtente, nomeUtente, cognomeUtente);
	}

	// - Iter manuale: Spedizione UCP - RGS - Asci Renzo

	/**
	 * Dump creazione documento.
	 * 
	 * @param esito
	 */
	protected void dumpCreazioneDocumento(final EsitoSalvaDocumentoDTO esito) {
		for (final SalvaDocumentoErroreEnum e : esito.getErrori()) {
			LOGGER.info("------------");
			LOGGER.info("CAMPO " + e.getCampo());
			LOGGER.info("CODICE" + e.getCodice());
			LOGGER.info("MESSAGGIO" + e.getMessaggio());
		}
		LOGGER.info("DOC TITLE: " + esito.getDocumentTitle());
		LOGGER.info("NUM DOC: " + esito.getNumeroDocumento());
	}

	/**
	 * Dump informazioni document title.
	 * 
	 * @param documentTitle
	 */
	protected void dumpDTInfo(final String documentTitle) {
		for (final QueueInfo qi : getRosterWFFromDocumentTitle(documentTitle)) {
			LOGGER.info("=====================");
			LOGGER.info(qi.toString());
			LOGGER.info("=====================");
		}
	}

	private Collection<QueueInfo> getRosterWFFromDocumentTitle(final String documentTitle) {
		final Collection<QueueInfo> output = new ArrayList<>();
		final VWSession session = getAdminPESession();
		final String queryFilter = ID_DOCUMENTO_LABEL + documentTitle;
		final VWRoster roster = session.getRoster("NSD_Roster");
		final VWRosterQuery query = roster.createQuery(null, null, null, VWRoster.QUERY_NO_OPTIONS, queryFilter, null, VWFetchType.FETCH_TYPE_WORKOBJECT);
		while (query.hasNext()) {
			final VWWorkObject wf = (VWWorkObject) query.next();
			final QueueInfo data = new QueueInfo(wf);
			output.add(data);
		}
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final String querySQL = "SELECT u.USERNAME, n.DESCRIZIONE, IDSTATOLAVORAZIONE " + "FROM documentoutentestato dus "
					+ "LEFT OUTER JOIN UTENTE u ON dus.IDUTENTE = u.IDUTENTE " + "LEFT OUTER JOIN NODO n ON dus.IDNODO = n.IDNODO " + "WHERE dus.iddocumento = ?";
			final Connection con = createNewDBConnection();
			ps = con.prepareStatement(querySQL);
			ps.setString(1, documentTitle);
			rs = ps.executeQuery();
			while (rs.next()) {
				final String descUtente = rs.getString(1);
				final String descNodo = rs.getString(2);
				final Integer idStatoLavorazione = rs.getInt(3);
				final QueueInfo data = new QueueInfo(idStatoLavorazione, descNodo, descUtente);
				output.add(data);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	/**
	 * Gestisce la chiusura dello statement.
	 * 
	 * @param ps
	 *            prepared statement
	 */
	private static void closeStatement(final PreparedStatement ps) {
		try {
			if (ps != null) {
				ps.close();
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la chisura dello statement.", e);
		}
	}
	
	/**
	 * Gestisce la chiusura dello statement e del result set.
	 * 
	 * @param ps
	 *            prepared statement
	 * @param rs
	 *            result set
	 */
	private static void closeStatement(final PreparedStatement ps, final ResultSet rs) {
		closeStatement(ps);
		
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la chisura del result set.", e);
		}
	}

	/**
	 * Restituisce il roster del workflow identificato dal {@code wobNumber}.
	 * 
	 * @param wobNumber
	 *            identificativo del workflow
	 * @return VWWorkObject
	 */
	protected VWWorkObject getRosterWFFromNumber(final String wobNumber) {
		return getRosterWFFromNumber(wobNumber, USERNAME_MAZZOTTA);
	}

	/**
	 * Restituisce il roster del workflow identificato dal {@code wobNumber}.
	 * 
	 * @param wobNumber
	 *            identificativo del workflow
	 * @param username
	 *            nome utente
	 * @return workflow object recuperato
	 */
	protected VWWorkObject getRosterWFFromNumber(final String wobNumber, final String username) {
		final UtenteDTO utente = getUtente(username);
		final FilenetCredentialsDTO fcDTO = utente.getFcDTO();
		final FilenetPEHelper fpeh = new FilenetPEHelper(fcDTO);
		return fpeh.getWorkFlowByWob(wobNumber, true);
	}

	/**
	 * Restituisce il workflow object.
	 * 
	 * @param session
	 *            sessione del workflow
	 * @param queue
	 *            coda di lavoro
	 * @param documentTitle
	 *            identificativo documento
	 * @param idNodoDestinatario
	 *            identificativo nodo destinatario
	 * @param idUtenteDestinatario
	 *            identificativo utente destinatario
	 * @return workflow recuperato
	 */
	protected VWWorkObject fromDocumentTitleToWF(final VWSession session, final DocumentQueueEnum queue, final String documentTitle, final Integer idNodoDestinatario,
			final Integer idUtenteDestinatario) {
		String queryFilter = ID_DOCUMENTO_LABEL + documentTitle + " AND idNodoDestinatario=" + idNodoDestinatario;
		if (idUtenteDestinatario != null) {
			queryFilter += " AND idUtenteDestinatario = " + idUtenteDestinatario;
		}
		Collection<VWWorkObject> wobs;
		if (queue != null) {
			wobs = executeQueueQuery(session, queue, queryFilter);
		} else {
			wobs = executeRosterQuery(session, queryFilter);
		}
		return wobs.iterator().next();
	}

	/**
	 * Esegue la query roster.
	 * 
	 * @param session
	 * @param queryFilter
	 * @return work object recuperati
	 */
	protected Collection<VWWorkObject> executeRosterQuery(final VWSession session, final String queryFilter) {
		final Collection<VWWorkObject> output = new ArrayList<>();
		try {
			final VWRoster roster = session.getRoster("NSD_Roster");
			final VWRosterQuery query = roster.createQuery(null, null, null, VWRoster.QUERY_NO_OPTIONS, queryFilter, null, VWFetchType.FETCH_TYPE_WORKOBJECT);
			while (query.hasNext()) {
				final VWWorkObject wf = (VWWorkObject) query.next();
				output.add(wf);
			}
		} catch (final VWException e) {
			LOGGER.error(e);
			throw new RedException(e);
		}
		return output;
	}

	/**
	 * Esegue la query queue.
	 * 
	 * @param session
	 * @param queue
	 * @param queryFilter
	 * @return work object recuperati
	 */
	protected Collection<VWWorkObject> executeQueueQuery(final VWSession session, final DocumentQueueEnum queue, final String queryFilter) {
		final Collection<VWWorkObject> output = new ArrayList<>();
		try {
			final VWQueue queueQ = session.getQueue(queue.getName());
			final VWQueueQuery query = queueQ.createQuery(null, null, null, VWRoster.QUERY_NO_OPTIONS, queryFilter, null, VWFetchType.FETCH_TYPE_WORKOBJECT);
			while (query.hasNext()) {
				final VWWorkObject wf = (VWWorkObject) query.next();
				output.add(wf);
			}
		} catch (final VWException e) {
			LOGGER.error(e);
			throw new RedException(e);
		}
		return output;
	}

	/**
	 * Effettua la presa visione del documento identificato dal
	 * {@code documentTitle}.
	 * 
	 * @param username
	 *            utente che prende visione
	 * @param documentTitle
	 *            identificativo del documento
	 * @return esito della presa visione
	 */
	protected EsitoOperazioneDTO presaVisione(final String username, final String documentTitle) {
		final UtenteDTO utente = getUtente(username);
		final VWSession session = getPESession(utente);
		final VWWorkObject wf = fromDocumentTitleToWF(session, DocumentQueueEnum.DA_LAVORARE, documentTitle, utente.getIdUfficio().intValue(), utente.getId().intValue());
		return nextStepSRV.presaVisione(utente, wf.getWorkObjectNumber());
	}

	/**
	 * Effettua lo storno al corriere del documento identificato dal
	 * {@code documentTitle}.
	 * 
	 * @param usernameCorriere
	 *            username del corriere
	 * @param documentTitle
	 *            identificativo del documento
	 * @param idUfficio
	 *            identificativo dell'ufficio
	 * @param idUtente
	 *            identificativo dell'utente
	 * @return esito dello storno
	 */
	protected EsitoOperazioneDTO storna(final String usernameCorriere, final String documentTitle, final Integer idUfficio, final Integer idUtente) {
		final UtenteDTO corriere = getUtente(usernameCorriere);
		final VWSession sessionPEcorriere = getPESession(corriere);
		final VWWorkObject wo = fromDocumentTitleToWF(sessionPEcorriere, DocumentQueueEnum.CORRIERE, documentTitle, corriere.getIdUfficio().intValue(), null);
		final String wobNumerDocAssegnato = wo.getWorkObjectNumber();
		final Collection<String> wobNumbers = new ArrayList<>();
		wobNumbers.add(wobNumerDocAssegnato);
		Long idUt = 0L;
		if (idUtente != null) {
			idUt = idUtente.longValue();
		}
		final Collection<EsitoOperazioneDTO> out = riassegnazioneSRV.riassegna(corriere, wobNumbers, ResponsesRedEnum.STORNA, idUfficio.longValue(), idUt, "Test di storno.");
		return out.iterator().next();
	}

	/**
	 * Esegue lo spostamento in lavorazione del documento identificato dal
	 * {@code documentTitle}.
	 * 
	 * @param usernameOperatore
	 *            username dell'utente operatore
	 * @param documentTitle
	 *            identificativo del documento
	 * @return esito dello spostamento in lavorazione
	 */
	protected EsitoOperazioneDTO spostaLavorazione(final String usernameOperatore, final String documentTitle) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.SOSPESO, documentTitle, operatore.getIdUfficio().intValue(), operatore.getId().intValue());
		return operationWFSRV.spostaInLavorazione(operatore, wo.getWorkObjectNumber());
	}

	/**
	 * Effettua lo storno al corriere del documento identificato dal
	 * {@code wobNumber}.
	 * 
	 * @param username
	 *            username dell'utente che effettua lo storno
	 * @param wobNumber
	 *            identificativo del documento
	 * @return esito dello storno a corriere
	 */
	protected EsitoOperazioneDTO stornaAlCorriere(final String username, final String wobNumber) {
		final UtenteDTO utente = getUtente(username);
		return nextStepSRV.stornaAlCorriere(utente, wobNumber);
	}

	/**
	 * Effettua l'assegnamento al corriere del documento identificato dal
	 * {@code documentTitle}.
	 * 
	 * @param usernameCorriere
	 *            nome utente del corriere
	 * @param documentTitle
	 *            identificativo del documento
	 * @param usernameAssegnatario
	 *            nome utente dell'assegnatario
	 * @return esito dell'assegnazione
	 */
	protected EsitoOperazioneDTO assegnaDaCorriere(final String usernameCorriere, final String documentTitle, final String usernameAssegnatario) {
		final UtenteDTO corriere = getUtente(usernameCorriere);
		final VWSession sessionPE = getPESession(corriere);
		final VWWorkObject wo = fromDocumentTitleToWF(sessionPE, DocumentQueueEnum.CORRIERE, documentTitle, corriere.getIdUfficio().intValue(), null);
		final String wobNumerDocAssegnato = wo.getWorkObjectNumber();
		final Collection<String> wobNumbers = new ArrayList<>();
		wobNumbers.add(wobNumerDocAssegnato);
		final UtenteDTO utenteAssegnatario = getUtente(usernameAssegnatario);
		final Collection<EsitoOperazioneDTO> out = riassegnazioneSRV.riassegna(corriere, wobNumbers, ResponsesRedEnum.ASSEGNA, utenteAssegnatario.getIdUfficio(),
				utenteAssegnatario.getId(), TEST_ASSEGNAZIONE_MESSAGE);
		return out.iterator().next();
	}

	/**
	 * Effettua l'assegnamento dal corriere all'utente assegnatario identificato
	 * dall' {@code idUfficioCorriereAssegnatario} e dal nome utente:
	 * {@code usernameAssegnatario}.
	 * 
	 * @param usernameCorriere
	 *            nome utente del corriere
	 * @param documentTitle
	 *            identificativo del documento
	 * @param usernameAssegnatario
	 *            nome utente assegnatario del documento
	 * @param idUfficioCorriereAssegnatario
	 *            identificativo ufficio corriere assegnatario
	 * @return esito dell'assegnazione da corriere
	 */
	protected EsitoOperazioneDTO assegnaDaCorriere(final String usernameCorriere, final String documentTitle, final String usernameAssegnatario,
			final Integer idUfficioCorriereAssegnatario) {
		final UtenteDTO corriere = getUtente(usernameCorriere);
		final VWSession sessionPE = getPESession(corriere);
		final VWWorkObject wo = fromDocumentTitleToWF(sessionPE, DocumentQueueEnum.CORRIERE, documentTitle, idUfficioCorriereAssegnatario, null);
		final String wobNumerDocAssegnato = wo.getWorkObjectNumber();
		final Collection<String> wobNumbers = new ArrayList<>();
		wobNumbers.add(wobNumerDocAssegnato);
		final UtenteDTO utenteAssegnatario = getUtente(usernameAssegnatario);
		final Collection<EsitoOperazioneDTO> out = riassegnazioneSRV.riassegna(corriere, wobNumbers, ResponsesRedEnum.ASSEGNA, idUfficioCorriereAssegnatario.longValue(),
				utenteAssegnatario.getId(), TEST_ASSEGNAZIONE_MESSAGE);
		return out.iterator().next();
	}

	/**
	 * Assegna da corrier ad ufficio del documento identificato dal
	 * {@code documentTitle}.
	 * 
	 * @param usernameCorriere
	 *            nome utente corriere
	 * @param documentTitle
	 *            identificativo del documento
	 * @param idUfficio
	 *            identificativo dell'ufficio
	 * @return esito dell'assegnazione
	 */
	protected EsitoOperazioneDTO assegnaDaCorriereAdUfficio(final String usernameCorriere, final String documentTitle, final Integer idUfficio) {
		final UtenteDTO corriere = getUtente(usernameCorriere);
		final VWSession sessionPE = getPESession(corriere);
		final VWWorkObject wo = fromDocumentTitleToWF(sessionPE, DocumentQueueEnum.CORRIERE, documentTitle, corriere.getIdUfficio().intValue(), null);
		final String wobNumerDocAssegnato = wo.getWorkObjectNumber();
		final Collection<String> wobNumbers = new ArrayList<>();
		wobNumbers.add(wobNumerDocAssegnato);
		final Collection<EsitoOperazioneDTO> out = riassegnazioneSRV.riassegna(corriere, wobNumbers, ResponsesRedEnum.ASSEGNA, idUfficio.longValue(), null,
				TEST_ASSEGNAZIONE_MESSAGE);
		return out.iterator().next();
	}

	/**
	 * Esegue la messa in conoscenza del documento identificato dal
	 * {@code wobNumber}.
	 * 
	 * @param wobNumber
	 *            identificativo del documento
	 * @param idUfficio
	 *            identificativo dell'ufficio
	 * @param idUtente
	 *            identificativo dell'utente
	 * @param usernameOperatore
	 *            username dell'utente operatore
	 * @return esito della messa in conoscenza
	 */
	protected EsitoOperazioneDTO mettiInConoscenza(final String wobNumber, final Integer idUfficio, final Integer idUtente, final String usernameOperatore) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final List<AssegnatarioOrganigrammaDTO> nuoveAssegnazioniConoscenza = new ArrayList<>();
		nuoveAssegnazioniConoscenza.add(new AssegnatarioOrganigrammaDTO(idUfficio.longValue(), idUtente.longValue()));
		final String motivazioneAssegnazione = "assegnazione di test.";
		return modProcSRV.mettiInConoscenza(wobNumber, nuoveAssegnazioniConoscenza, motivazioneAssegnazione, operatore);
	}

	/**
	 * Esegue il rifiuto dal libro firma.
	 * 
	 * @param usernameOperatore
	 *            utente operatore
	 * @param dt
	 *            identificativo del documento rifiutato
	 * @return esito del rifiuto
	 */
	protected EsitoOperazioneDTO rifiutaDaLIbroFirma(final String usernameOperatore, final String dt) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWWorkObject wo = getWF(usernameOperatore, DocumentQueueEnum.NSD, dt);
		final String motivoRifiuto = "Rifiuto di test.";
		return operationDocSRV.rifiuta(operatore, wo.getWorkObjectNumber(), motivoRifiuto, ResponsesRedEnum.RIFIUTA, false);
	}

	/**
	 * Effettua il rifiuto dal corriere.
	 * 
	 * @param usernameOperatore
	 *            nome utente operatore
	 * @param dt
	 *            identificativo del documento
	 * @return esito del rifiuto
	 */
	protected EsitoOperazioneDTO rifiutaDaCorriere(final String usernameOperatore, final String dt) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWWorkObject wo = getWF(usernameOperatore, DocumentQueueEnum.CORRIERE, dt);
		final String motivoRifiuto = "Rifiuto di test.";
		return operationDocSRV.rifiuta(operatore, wo.getWorkObjectNumber(), motivoRifiuto, ResponsesRedEnum.RIFIUTA, true);
	}

	/**
	 * Esegue la messa agli atti del documento identificato dal
	 * {@code documentTitle}.
	 * 
	 * @param usernameOperatore
	 *            nome utente operatore
	 * @param documentTitle
	 *            identificativo del documento
	 * @return esito della messa agli atti
	 */
	protected EsitoOperazioneDTO messaAgliAtti(final String usernameOperatore, final String documentTitle) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWWorkObject woDaLavorare = getWF(usernameOperatore, DocumentQueueEnum.DA_LAVORARE, documentTitle);
		final String wobNumerDocDaMettereAtti = woDaLavorare.getWorkObjectNumber();
		return attiSRV.mettiAgliAtti(wobNumerDocDaMettereAtti, operatore, "Messa agli atti di test.");
	}

	/**
	 * Restituisce il workflow associato al documento identificato dal
	 * {@code documentTitle}.
	 * 
	 * @param usernameOperatore
	 *            nome utente operatore
	 * @param queue
	 *            coda di lavoro del documento
	 * @param documentTitle
	 *            identificativo del documento
	 * @return work object recuperato
	 */
	protected VWWorkObject getWF(final String usernameOperatore, final DocumentQueueEnum queue, final String documentTitle) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final Collection<VWWorkObject> wosDaLavorare = executeQueueQuery(session, queue, ID_DOCUMENTO_LABEL + documentTitle);
		return wosDaLavorare.iterator().next();
	}

	/**
	 * Restituisce il workflow associato al documento identificato dal
	 * {@code documentTitle}.
	 * 
	 * @param operatore
	 *            utente operatore
	 * @param queue
	 *            coda di lavoro
	 * @param documentTitle
	 *            identificativo del documento
	 * @return work object recuperato
	 */
	protected VWWorkObject getWF(final UtenteDTO operatore, final DocumentQueueEnum queue, final String documentTitle) {
		final VWSession session = getPESession(operatore);
		return getWF(session, queue, documentTitle);
	}

	/**
	 * Restituisce il workflow associato al documento identificato dal
	 * {@code documentTitle}.
	 * 
	 * @param session
	 *            sessione
	 * @param queue
	 *            coda di lavoro
	 * @param documentTitle
	 *            identificativo del documento
	 * @return work object recuperato
	 */
	protected VWWorkObject getWF(final VWSession session, final DocumentQueueEnum queue, final String documentTitle) {
		final Collection<VWWorkObject> wosDaLavorare = executeQueueQuery(session, queue, ID_DOCUMENTO_LABEL + documentTitle);
		return wosDaLavorare.iterator().next();
	}

	/**
	 * Imposta il {@code titolario} sul documento identificato dal
	 * {@code documentTitle}.
	 * 
	 * @param usernameOperatore
	 *            utente operatore
	 * @param documentTitle
	 *            identificativo del documento
	 * @param titolario
	 *            titolario da impostare sul documento
	 */
	protected void setTitolario(final String usernameOperatore, final String documentTitle, final String titolario) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final FascicoloDTO fp = fascicoloSRV.getFascicoloProcedimentale(documentTitle, operatore.getIdAoo().intValue(), operatore);
		fascicoloSRV.associaATitolario(operatore, fp.getIdFascicolo(), titolario);
	}

	/**
	 * Assegna nella coda <em> Da Lavorare </em> il documento identificato dal
	 * {@code documentTitle}.
	 * 
	 * @param usernameOperatore
	 *            Nome utente operatore.
	 * @param documentTitle
	 *            Identificativo del documento da assegnare.
	 * @param usernameAssegnatario
	 *            Nome utente assegnatario del documento.
	 * @return Esito dell'assegnamento.
	 */
	protected EsitoOperazioneDTO assegnaDaDaLavorare(final String usernameOperatore, final String documentTitle, final String usernameAssegnatario) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession sessionPE = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(sessionPE, DocumentQueueEnum.DA_LAVORARE, documentTitle, operatore.getIdUfficio().intValue(),
				operatore.getId().intValue());
		final String wobNumerDocAssegnato = wo.getWorkObjectNumber();
		final Collection<String> wobNumbers = new ArrayList<>();
		wobNumbers.add(wobNumerDocAssegnato);
		final UtenteDTO utenteAssegnatario = getUtente(usernameAssegnatario);
		final Collection<EsitoOperazioneDTO> out = riassegnazioneSRV.riassegna(operatore, wobNumbers, ResponsesRedEnum.ASSEGNA, utenteAssegnatario.getIdUfficio(),
				utenteAssegnatario.getId(), TEST_ASSEGNAZIONE_MESSAGE);
		return out.iterator().next();
	}

	/**
	 * Esegue la firma remota.
	 * 
	 * @param usernameOperatore
	 * @param signType
	 * @param docTitle
	 * @param otp
	 * @param pin
	 * @return esiti
	 */
	public Collection<EsitoOperazioneDTO> firmaRemota(final String usernameOperatore, final SignTypeEnum signType, final String docTitle, final String otp, final String pin) {
		return firmaRemota(DocumentQueueEnum.NSD, null, usernameOperatore, signType, docTitle, otp, pin);
	}

	/**
	 * Firma remota per decreti FEPA.
	 * 
	 * @param usernameOperatore
	 * @param signType
	 * @param docTitle
	 * @param otp
	 * @param pin
	 * @return esiti
	 */
	public Collection<EsitoOperazioneDTO> firmaRemotaDecretiFepa(final String usernameOperatore, final SignTypeEnum signType, final String docTitle, final String otp,
			final String pin) {
		return firmaRemota(DocumentQueueEnum.DD_DA_FIRMARE, ID_IGICS, usernameOperatore, signType, docTitle, otp, pin);
	}

	/**
	 * Esegue la firma remota.
	 * 
	 * @param queue
	 * @param idUfficio
	 * @param usernameOperatore
	 * @param signType
	 * @param docTitle
	 * @param otp
	 * @param pin
	 * @return esiti
	 */
	public Collection<EsitoOperazioneDTO> firmaRemota(final DocumentQueueEnum queue, final Integer idUfficio,
			final String usernameOperatore, final SignTypeEnum signType, final String docTitle, final String otp,
			final String pin) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession sessionPE = getPESession(operatore);
		final Collection<String> selectedWobNumbers = new ArrayList<>();
		Integer idUfficioTmp = idUfficio;
		if (idUfficioTmp == null) {
			idUfficioTmp = operatore.getIdUfficio().intValue();
		}
		operatore.setIdUfficio(idUfficioTmp.longValue());
		final VWWorkObject wo = fromDocumentTitleToWF(sessionPE, queue, docTitle, idUfficioTmp, operatore.getId().intValue());
		final String wobDaFirmare = wo.getWorkObjectNumber();
		selectedWobNumbers.add(wobDaFirmare);
		return aSignSRV.firmaRemota(selectedWobNumbers, pin, otp, operatore, signType, false);
	}

	/**
	 * @param usernameOperatore
	 * @param documentTitle
	 * @return EsitoOperazioneDTO
	 */
	public EsitoOperazioneDTO spedito(final String usernameOperatore, final String documentTitle) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession sessionPE = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(sessionPE, DocumentQueueEnum.SPEDIZIONE, documentTitle, operatore.getIdUfficio().intValue(), null);
		final Date dataSped = new Date();
		return speditoSRV.spedito(operatore, dataSped, wo.getWorkObjectNumber());
	}

	/**
	 * Esegue la response <em> procedi </em> sul documento identificat dal
	 * {@code documentTitle}.
	 * 
	 * @param usernameOperatore
	 *            utente operatore
	 * @param documentTitle
	 *            identificativo del documento
	 * @return esito della response
	 */
	protected EsitoOperazioneDTO procedi(final String usernameOperatore, final String documentTitle) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.CORRIERE, documentTitle, operatore.getIdUfficio().intValue(), null);
		return nextStepSRV.procedi(operatore, wo.getWorkObjectNumber());
	}

	/**
	 * Mette in attesa il thread per 30 secondi.
	 */
	protected void waitTransformation() {
		try {
			Thread.sleep(30000);
		} catch (final InterruptedException e) {
			LOGGER.error(e);
			Thread.currentThread().interrupt();
		}
	}

	/**
	 * Mette il thread in attesa per {@code nSeconds} secondi.
	 * 
	 * @param nSeconds
	 *            secondi di wait del thread
	 */
	protected void waitSeconds(final Integer nSeconds) {
		try {
			Thread.sleep(Long.valueOf(nSeconds) * 1000);
		} catch (final InterruptedException e) {
			LOGGER.error(e);
			Thread.currentThread().interrupt();
		}
	}

	/**
	 * Esegue il processo di faldonatura del documento identificato dal {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            utente operatore
	 * @param dt
	 *            document title identificativo del documento
	 * @param nomeFaldone
	 *            nome del faldone in cui viene faldonato il documento
	 * @return esito della faldonatura
	 */
	protected EsitoOperazioneDTO faldona(final String usernameOperatore, final String dt, final String nomeFaldone) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.DA_LAVORARE, dt, operatore.getIdUfficio().intValue(), operatore.getId().intValue());
		final FascicoloDTO fascicolo = fascicoloSRV.getFascicoloProcedimentale(dt, operatore.getIdAoo().intValue(), operatore);
		return faldoneSRV.faldona(operatore, nomeFaldone, wo.getWorkObjectNumber(), fascicolo.getIdFascicolo());
	}

	/**
	 * Esegue il processo di siglatura e invio del documento identificato dal
	 * {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            utente operatore
	 * @param dt
	 *            identificativo del documento
	 * @return esito operazione
	 */
	protected EsitoOperazioneDTO siglaInvia(final String usernameOperatore, final String dt) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.NSD, dt, operatore.getIdUfficio().intValue(), operatore.getId().intValue());
		return operationDocSRV.sigla(operatore.getFcDTO(), wo.getWorkObjectNumber(), operatore, "Sigla di test.");
	}

	/**
	 * Esegue la response di invio all'ispettore del documento identificato dal
	 * {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            utente operatore
	 * @param dt
	 *            identificativo del documento inviato all'ispettore
	 * @return esito dell'invio
	 */
	protected EsitoOperazioneDTO inviaIspettore(final String usernameOperatore, final String dt) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.CORRIERE, dt, operatore.getIdUfficio().intValue(), null);
		return nextStepSRV.inviaIspettore(operatore, wo.getWorkObjectNumber());
	}

	/**
	 * Esegue l'invio all'ufficio centrale di bilancio il documento identificato dal
	 * {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Utente operatore.
	 * @param dt
	 *            Identificativo del documento.
	 * @return Esito dell'invio all'ufficio centrale di bilancio.
	 */
	protected EsitoOperazioneDTO inviaUCP(final String usernameOperatore, final String dt) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.CORRIERE, dt, operatore.getIdUfficio().intValue(), null);
		return nextStepSRV.inviaUCP(operatore, wo.getWorkObjectNumber());
	}

	/**
	 * Esegue la response <em> firmato </em> sul documento identificato dal
	 * {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            utente operatore
	 * @param dt
	 *            identificativo del documento
	 * @return esito della response
	 */
	protected EsitoOperazioneDTO firmato(final String usernameOperatore, final String dt) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.CORRIERE, dt, operatore.getIdUfficio().intValue(), null);
		return firmatoSpeditoSRV.firmato(operatore, wo.getWorkObjectNumber(), null);
	}

	/**
	 * Esegue l'invio all'ufficio di coordinamento del documento identificato dal
	 * {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            utente operatore
	 * @param dt
	 *            identificativo del documento
	 * @return esito invio
	 */
	protected EsitoOperazioneDTO inviaUfficioCoordinamento(final String usernameOperatore, final String dt) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.CORRIERE, dt, operatore.getIdUfficio().intValue(), null);
		return nextStepSRV.inviaUffCoordinamento(operatore, wo.getWorkObjectNumber());
	}

	/**
	 * Esegue l'invio in firma digitale del documento identificato dal {@code dt}.
	 * @param usernameOperatore utente operatore.
	 * @param dt identificativo del documento - document title.
	 * @return esito invio in firma.
	 */
	protected EsitoOperazioneDTO inviaFirmaDigitale(final String usernameOperatore, final String dt) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.CORRIERE, dt, operatore.getIdUfficio().intValue(), null);
		return nextStepSRV.inviaFirmaDigitale(operatore, wo.getWorkObjectNumber());
	}

	/**
	 * Esegue il rifiuto spedizione del documento identificato dal {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            utente operatore.
	 * @param dt
	 *            identificativo del documento - document title.
	 * @return esito rifiuto spedizione
	 */
	protected EsitoOperazioneDTO rifiutoSpedizione(final String usernameOperatore, final String dt) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.SPEDIZIONE, dt, operatore.getIdUfficio().intValue(), null);
		return rifiutaSpedizioneSRV.rifiuta(operatore.getFcDTO(), wo.getWorkObjectNumber(), "Test rifito.", operatore.getId().intValue(), 1);
	}

	/**
	 * Esegue l'assegnamento ad ufficio centrale di protocollo del documento
	 * identificato dal {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            utente operatore.
	 * @param dt
	 *            identificativo del documento - document title.
	 * @return esito dell'assegnamento
	 */
	protected EsitoOperazioneDTO assegnaUCP(final String usernameOperatore, final String dt) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.SPEDIZIONE, dt, operatore.getIdUfficio().intValue(), null);
		return assegnaUfficioSRV.assegnaUCP(operatore, wo.getWorkObjectNumber());
	}

	/**
	 * Esegue la response <em> firmato spedito </em> sul documento identificato dal
	 * {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            utente operatore.
	 * @param dt
	 *            identificativo del documento - document title.
	 * @return esito esecuzione response
	 */
	protected EsitoOperazioneDTO firmatoSpedito(final String usernameOperatore, final String dt) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.CORRIERE, dt, operatore.getIdUfficio().intValue(), null);
		return firmatoSpeditoSRV.firmatoESpedito(operatore, wo.getWorkObjectNumber(), null);
	}

	/**
	 * Esegue la response <em> reinvia </em> del documento identificato dal
	 * {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            utente operatore.
	 * @param dt
	 *            identificativo del documento - document title.
	 * @return esito reinvio
	 */
	protected EsitoOperazioneDTO reinvia(final String usernameOperatore, final String dt) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.DA_LAVORARE, dt, operatore.getIdUfficio().intValue(), null);
		return reinviaSRV.reinvia(operatore, wo.getWorkObjectNumber());
	}

	/**
	 * Esegue la modifica dell'iter manuale del documento identificato dal
	 * {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Utente operatore.
	 * @param dt
	 *            Identificativo documento.
	 * @param tipoAss
	 *            Tipo assegnazione documento.
	 * @param bFlagUrgente
	 *            Flag associato alla checkbox <em> urgente </em>.
	 * @param idNodoDestinatarioNew
	 *            Identificativo ufficio destinatario a cui assegnare il documento.
	 * @param idUtenteDestinatarioNew
	 *            Identificativo utente destinatario a cui assegnare il documento.
	 * @return esito della modifica iter.
	 */
	protected EsitoOperazioneDTO modificaIterManuale(final String usernameOperatore, final String dt, final TipoAssegnazioneEnum tipoAss, final Boolean bFlagUrgente,
			final Long idNodoDestinatarioNew, final Long idUtenteDestinatarioNew) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.NSD, dt, operatore.getIdUfficio().intValue(), null);
		return modificaIterSRV.gestioneCambioIterManuale(tipoAss, bFlagUrgente, operatore, wo.getWorkObjectNumber(), idNodoDestinatarioNew, idUtenteDestinatarioNew);
	}

	/**
	 * Esegue la response <em> chiudi </em> sul documento identificato dal
	 * {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            utente operatore.
	 * @param dt
	 *            identificativo del documento - document title.
	 * @return esito chisurua del documento
	 */
	protected EsitoOperazioneDTO chiudi(final String usernameOperatore, final String dt) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.CORRIERE, dt, operatore.getIdUfficio().intValue(), null);
		return chiudiSRV.chiudi(operatore, wo.getWorkObjectNumber());
	}

	/**
	 * Esegue la firma autografa sul documento identificato dal {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Utente operatore.
	 * @param dt
	 *            Identificativo del documento - document title.
	 * @return Esito firma autografa
	 */
	protected EsitoOperazioneDTO firmaAutografa(final String usernameOperatore, final String dt) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.NSD, dt, operatore.getIdUfficio().intValue(), null);
		final List<MasterDocumentRedDTO> documentiSelezionati = new ArrayList<>();
		final MasterDocumentRedDTO master = new MasterDocumentRedDTO();
		master.setDocumentTitle(dt);
		documentiSelezionati.add(master);
		signSRV.convertiDestinatariElettroniciInCartacei(operatore, documentiSelezionati);
		final Collection<String> wobs = new ArrayList<>();
		wobs.add(wo.getWorkObjectNumber());
		Collection<EsitoOperazioneDTO> esiti = aSignSRV.firmaAutografa(wobs, operatore, null);
		return esiti.iterator().next();
	}

	/**
	 * Esegue la response <em> riassegna </em> sul documento identificato dal
	 * {@code documentTitle}.
	 * 
	 * @param usernameCorriere
	 *            Utente corriere.
	 * @param documentTitle
	 *            Identificativo del documento in riassegnamento.
	 * @param idUfficio
	 *            Identificativo dell'ufficio assegnatario destinatario.
	 * @param idUtente
	 *            Identificativo dell'utente assegnatario destinatario.
	 * @return Esito riassegnamento.
	 */
	protected EsitoOperazioneDTO riassegna(final String usernameCorriere, final String documentTitle, final Integer idUfficio, final Integer idUtente) {
		final UtenteDTO corriere = getUtente(usernameCorriere);
		final VWSession sessionPEcorriere = getPESession(corriere);
		final VWWorkObject wo = fromDocumentTitleToWF(sessionPEcorriere, DocumentQueueEnum.CORRIERE, documentTitle, corriere.getIdUfficio().intValue(), null);
		final String wobNumerDocAssegnato = wo.getWorkObjectNumber();
		final Collection<String> wobNumbers = new ArrayList<>();
		wobNumbers.add(wobNumerDocAssegnato);
		Long idUt = 0L;
		if (idUtente != null) {
			idUt = idUtente.longValue();
		}
		final Collection<EsitoOperazioneDTO> out = riassegnazioneSRV.riassegna(corriere, wobNumbers, ResponsesRedEnum.RIASSEGNA, idUfficio.longValue(), idUt,
				"Test di storno.");
		return out.iterator().next();
	}

	/**
	 * Effettua l'invio in Sigla del documento identificato dal {@code dt}.
	 * 
	 * @param coda
	 *            Coda di riferimento del documento.
	 * @param usernameOperatore
	 *            Utente operatore dell'invio in sigla.
	 * @param dt
	 *            Document title, identificativo del documento.
	 * @param idNodoDestinatarioNew
	 *            Identificativo dell'ufficio destinatario.
	 * @param idUtenteDestinatarioNew
	 *            Identificativo dell'utente destinatario.
	 * @param urgente
	 *            Flag associato all'urgenza del documento.
	 * @return Esito dell'invio in sigla.
	 */
	protected EsitoOperazioneDTO invioSigla(final DocumentQueueEnum coda, final String usernameOperatore, final String dt, final Long idNodoDestinatarioNew,
			final Long idUtenteDestinatarioNew, final Boolean urgente) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, coda, dt, operatore.getIdUfficio().intValue(), operatore.getId().intValue());
		final Collection<String> wobNumbers = new ArrayList<>();
		wobNumbers.add(wo.getWorkObjectNumber());
		final List<EsitoOperazioneDTO> results = invioInFirmaSRV.invioInSigla(operatore, wobNumbers, idNodoDestinatarioNew, idUtenteDestinatarioNew, "Assegnazione di test",
				urgente);
		return results.iterator().next();
	}

	/**
	 * Esegue l'invio in firma del documento identificato dal {@code dt}
	 * assegnandolo all'utente identificato dall' {@code idUtenteDestinatarioNew}.
	 * 
	 * @param coda
	 *            Coda di lavoro in cui si trova il documento.
	 * @param usernameOperatore
	 *            Nome utente dell'operatore che esegue l'invio in firma.
	 * @param dt
	 *            Identificativo della firma.
	 * @param idNodoDestinatarioNew
	 *            Identificativo dell'ufficio dell'utente destinatario.
	 * @param idUtenteDestinatarioNew
	 *            Identificativo dell'utente destinatario.
	 * @param urgente
	 *            Flag associato all'urgenza dell'invio in firma.
	 * @return Esito dell'invio in firma.
	 */
	protected EsitoOperazioneDTO invioFirma(final DocumentQueueEnum coda, final String usernameOperatore, final String dt, final Long idNodoDestinatarioNew,
			final Long idUtenteDestinatarioNew, final Boolean urgente) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, coda, dt, operatore.getIdUfficio().intValue(), operatore.getId().intValue());
		final Collection<String> wobNumbers = new ArrayList<>();
		wobNumbers.add(wo.getWorkObjectNumber());
		final List<EsitoOperazioneDTO> results = invioInFirmaSRV.invioInFirma(operatore, wobNumbers, idNodoDestinatarioNew, idUtenteDestinatarioNew, "Assegnazione di test",
				urgente);
		return results.iterator().next();
	}

	/**
	 * Restituisce il numero del documento identificato dal {@code dt}.
	 * 
	 * @param username
	 *            Nome dell'utente in sessione.
	 * @param dt
	 *            Identificativo del documento.
	 * @return Numero documento recuperato.
	 */
	protected Integer getNumDoc(final String username, final String dt) {
		Document d = null;
		IFilenetCEHelper fceh = null;

		try {
			final UtenteDTO operatore = getUtente(username);
			fceh = FilenetCEHelperProxy.newInstance(operatore.getFcDTO(), operatore.getIdAoo());

			d = fceh.getDocumentByDTandAOO(dt, Long.parseLong(operatore.getIdAoo().toString()));
		} finally {
			if (fceh != null) {
				fceh.popSubject();
			}
		}

		return (Integer) TrasformerCE.getMetadato(d, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
	}

	/**
	 * Esegue la richiesta visti per il documento identificato dal {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Nome utente operatore che esegue la richiesta visti.
	 * @param dt
	 *            Identificativo del documento.
	 * @param assegnazioneVistoList
	 *            Lista delle assegnazioni per visto.
	 * @return Esito dell'operazione di richiesta visti.
	 */
	protected EsitoOperazioneDTO richiestaVisti(final String usernameOperatore, final String dt, final List<AssegnazioneDTO> assegnazioneVistoList) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.NSD, dt, operatore.getIdUfficio().intValue(), operatore.getId().intValue());
		return richiesteVistoSrv.richiestaVisti(operatore, wo.getWorkObjectNumber(), null, assegnazioneVistoList, false);
	}

	/**
	 * Esegue la richiesta visti da lavorare per il documento identificato dal
	 * {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Nome utente dell'operatore che esegue la richiesta visti da
	 *            lavorare.
	 * @param dt
	 *            Identificativo del documento.
	 * @param assegnazioneVistoList
	 *            Lista delle assegnazioni visto.
	 * @return Esito operazione.
	 */
	protected EsitoOperazioneDTO richiestaVistiDaLavorare(final String usernameOperatore, final String dt, final List<AssegnazioneDTO> assegnazioneVistoList) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.DA_LAVORARE, dt, operatore.getIdUfficio().intValue(), operatore.getId().intValue());
		return richiesteVistoSrv.richiestaVisti2(operatore, wo.getWorkObjectNumber(), null, assegnazioneVistoList, false);
	}

	/**
	 * Recupera le code in cui si trova il documento identificato dal {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Nome utente dell'operatore.
	 * @param dt
	 *            Identificativo del documento.
	 * @return Code recuperate.
	 */
	protected RecuperoCodeDTO getQueues(final String usernameOperatore, final String dt) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		return ricercaSRV.getQueueNameFromDocumentTitle(operatore, dt);
	}

	/**
	 * Recupera le code in cui si trova il documento identificato dal {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Nome utente dell'operatore.
	 * @param dt
	 *            Identificativo del documento.
	 * @param classeDocumentale
	 *            Classe documentale del documento ricercato.
	 * @param idCategoriaDocumento
	 *            Identificativo della categoria del documento ricercato.
	 * @return Code recuperate.
	 */
	protected RecuperoCodeDTO getQueues(final String usernameOperatore, final String dt, final String classeDocumentale, final Integer idCategoriaDocumento) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		return ricercaSRV.getQueueNameFromDocumentTitle(operatore, dt, classeDocumentale, idCategoriaDocumento, true);
	}

	/**
	 * Effettua una richiesta di contributo per il documento identificato dal
	 * {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Nome utente a cui viene richiesto il contributo.
	 * @param dt
	 *            Identificativo del documento.
	 * @param idUfficio
	 *            Identifativo dell'ufficio dell'utente operatore.
	 * @return Esito operazione richiesta contributo.
	 */
	protected EsitoOperazioneDTO richiediContributo(final String usernameOperatore, final String dt, final Integer idUfficio) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.CORRIERE, dt, operatore.getIdUfficio().intValue(), null);
		final List<AssegnatarioOrganigrammaDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(new AssegnatarioOrganigrammaDTO(idUfficio.longValue()));
		return contributiSRV.richiediContributo(operatore, wo.getWorkObjectNumber(), assegnazioni, SIMPLE_TEST, false);
	}

	/**
	 * Effettua una richiesta di contributo interno per il documento identificato
	 * dal {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Nome utente operatore.
	 * @param dt
	 *            Identificativo del documento.
	 * @param idUfficio
	 *            Identificativo dell'ufficio dell'operatore.
	 * @return Esito operazione richiesta contributo interno.
	 */
	protected EsitoOperazioneDTO richiediContributoInterno(final String usernameOperatore, final String dt, final Integer idUfficio) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.CORRIERE, dt, operatore.getIdUfficio().intValue(), null);
		final List<AssegnatarioOrganigrammaDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(new AssegnatarioOrganigrammaDTO(idUfficio.longValue()));
		return contributiSRV.richiediContributoInterno(operatore, wo.getWorkObjectNumber(), assegnazioni, SIMPLE_TEST, false);
	}

	/**
	 * Effettua una richiesta di contributo interno per il documento identificato
	 * dal {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Nome utente operatore.
	 * @param dt
	 *            Identificativo del documento.
	 * @param idUfficio
	 *            Identificativo dell'ufficio dell'operatore.
	 * @param idUtente
	 *            Identificativo dell'utente operatore.
	 * @return Esito operazione richiesta contributo interno.
	 */
	protected EsitoOperazioneDTO richiediContributoUtente(final String usernameOperatore, final String dt, final Integer idUfficio, final Integer idUtente) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.CORRIERE, dt, operatore.getIdUfficio().intValue(), null);
		final List<AssegnatarioOrganigrammaDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(new AssegnatarioOrganigrammaDTO(idUfficio.longValue(), idUtente.longValue()));
		return contributiSRV.richiediContributoUtente(operatore, wo.getWorkObjectNumber(), assegnazioni, SIMPLE_TEST, false);
	}

	/**
	 * Effettua l'inserimento del contributo per il documento identificato dal
	 * {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Nome utente operatore che effettua l'inserimento.
	 * @param dt
	 *            Identificativo del documento.
	 * @return Esito operazione di inserimento contributo.
	 */
	protected EsitoOperazioneDTO inserisciContributo(final String usernameOperatore, final String dt) {
		final String filename = FILENAME_TEST;
		final ContributoDTO contributo = new ContributoDTO(filename, MediaType.PDF.toString(), FileUtils.getFileFromInternalResources(filename));
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.DA_LAVORARE, dt, operatore.getIdUfficio().intValue(), operatore.getId().intValue());
		return contributiSRV.inserisciContributo(operatore, wo.getWorkObjectNumber(), contributo);
	}

	/**
	 * Esegue la validazione dei contributi associati al documento identificato dal
	 * {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Nome utente operatore.
	 * @param dt
	 *            Identificativo del documento.
	 * @return Esito validazione contributi.
	 */
	protected EsitoOperazioneDTO validaContributi(final String usernameOperatore, final String dt) {
		final String filename = FILENAME_TEST;
		final ContributoDTO contributo = new ContributoDTO(filename, MediaType.PDF.toString(), FileUtils.getFileFromInternalResources(filename));
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.CORRIERE, dt, operatore.getIdUfficio().intValue(), null);
		return contributiSRV.validaContributi(operatore, wo.getWorkObjectNumber(), contributo);
	}

	/**
	 * Esegue la validazione del contributo per il documento identificato dal
	 * {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Nome utente operatore.
	 * @param dt
	 *            Identificativo del documento.
	 * @return Esito operazione di validazione contributo.
	 */
	protected EsitoOperazioneDTO validazioneContributo(final String usernameOperatore, final String dt) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.CORRIERE, dt, operatore.getIdUfficio().intValue(), null);
		return contributiSRV.validazioneContributo(operatore, wo.getWorkObjectNumber(), "Validazione di prova");
	}

	/**
	 * Esegue la richiesta di verifica del documento identificato dal {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Nome utente operatore che richiede la verifica.
	 * @param dt
	 *            Identificativo del documento.
	 * @param idNodo
	 *            Identificativo dell'ufficio dell'utente assegnatario per la
	 *            verifica.
	 * @param idUtente
	 *            Identificativo dell'utente assegnaatrio per la verifica.
	 * @return Esito di richiesta verifica.
	 */
	protected EsitoOperazioneDTO richiediVerifica(final String usernameOperatore, final String dt, final Integer idNodo, final Integer idUtente) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.NSD, dt, operatore.getIdUfficio().intValue(), null);
		final AssegnatarioOrganigrammaDTO assegnatario = new AssegnatarioOrganigrammaDTO(idNodo.longValue(), idUtente.longValue());
		return richiesteVistoSrv.richiediVerifica(operatore, wo.getWorkObjectNumber(), assegnatario, JUNIT_TEST_MESSAGE);
	}

	/**
	 * Esegue la richiesta di un visto per il documento identificato dal {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Nome utente operatore che richiede il visto.
	 * @param dt
	 *            Identificativo del documento per il quale richiedere il visto.
	 * @param usernameAssegnatario
	 *            Nome utente assegnatario a cui richiedere il visto.
	 * @return Esito richiesta visto.
	 */
	protected EsitoOperazioneDTO richiediVisto(final String usernameOperatore, final String dt, final String usernameAssegnatario) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.CORRIERE, dt, operatore.getIdUfficio().intValue(), null);
		final Collection<String> wobNumbers = new ArrayList<>();
		wobNumbers.add(wo.getWorkObjectNumber());
		final UtenteDTO utenteAssegnatario = getUtente(usernameAssegnatario);
		final AssegnazioneDTO assegnazione = new AssegnazioneDTO(null, null, null, null, null, utenteAssegnatario, null);
		final Collection<AssegnazioneDTO> assegnatari = new ArrayList<>();
		assegnatari.add(assegnazione);
		return richiesteVistoSrv.richiediVisti(operatore, wo.getWorkObjectNumber(), JUNIT_TEST_MESSAGE, assegnatari);
	}

	/**
	 * Esegue la response <em> verificato </em> sul visto associato al documento
	 * identificato dal {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Nome utente operatore che esegue la response.
	 * @param dt
	 *            Identificativo del documento su cui eseguire la response.
	 * @return Esito response.
	 */
	protected EsitoOperazioneDTO verificato(final String usernameOperatore, final String dt) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.DA_LAVORARE, dt, operatore.getIdUfficio().intValue(), operatore.getId().intValue());
		return vistisrv.verificato(operatore, wo.getWorkObjectNumber(), TipoApprovazioneEnum.VISTO_POSITIVO, JUNIT_TEST_MESSAGE);
	}

	/**
	 * Esegue una richiesta di contributo per il documento identificato dal
	 * {@code dt} all'utente identificato dal {@code usernameTarget}.
	 * 
	 * @param usernameOperatore
	 *            Nome utente operatore che richiede il contributo.
	 * @param dt
	 *            Identificativo del documento per il quale viene richiesto il
	 *            contributo.
	 * @param usernameTarget
	 *            Nome utente a cui viene richiesto il contributo.
	 * @return Esito richiesta contributo.
	 */
	protected EsitoOperazioneDTO richiestaContributo(final String usernameOperatore, final String dt, final String usernameTarget) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.DA_LAVORARE, dt, operatore.getIdUfficio().intValue(), operatore.getId().intValue());
		final UtenteDTO userTarget = getUtente(usernameTarget);
		final List<AssegnatarioOrganigrammaDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(new AssegnatarioOrganigrammaDTO(userTarget.getIdUfficio(), userTarget.getId()));
		return contributiSRV.richiestaContributo(operatore, wo.getWorkObjectNumber(), assegnazioni, SIMPLE_TEST, false);
	}

	/**
	 * Esegue la sigla e la richiesta visti per il documento identifcicato dal
	 * {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Nome Utente operatore che esegue la sigla e la richiesta visti.
	 * @param dt
	 *            Identificativo del documento.
	 * @param idUfficio
	 *            Identificativo dell'ufficio.
	 * @return Esito della sigla e della richiesta visti.
	 */
	protected EsitoOperazioneDTO siglaRichiediVisti(final String usernameOperatore, final String dt, final Integer idUfficio) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.NSD, dt, operatore.getIdUfficio().intValue(), operatore.getId().intValue());
		final AssegnazioneDTO assegnazione = new AssegnazioneDTO(null, null, TipoStrutturaNodoEnum.SETTORE, null, null, null, null, new UfficioDTO(idUfficio.longValue(), ""));
		final List<AssegnazioneDTO> assegnatari = new ArrayList<>();
		assegnatari.add(assegnazione);
		return richiesteVistoSrv.siglaERichiediVisti(operatore, wo.getWorkObjectNumber(), assegnatari, SIMPLE_TEST);
	}

	/**
	 * Esegue la richiesta visto interno per il documento identificato dal
	 * {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Nome utente operatore che esegue la richiesta visto interno.
	 * @param dt
	 *            Identificato del documento.
	 * @param idUfficio
	 *            Identificativo dell'ufficio.
	 * @return Esito operazione di richiesta visto interno.
	 */
	protected EsitoOperazioneDTO richiediVistoInterno(final String usernameOperatore, final String dt, final Integer idUfficio) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.GIRO_VISTI, dt, operatore.getIdUfficio().intValue(), null);
		final List<AssegnatarioOrganigrammaDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(new AssegnatarioOrganigrammaDTO(idUfficio.longValue()));
		return richiesteVistoSrv.richiediVistoInterno(operatore, wo.getWorkObjectNumber(), SIMPLE_TEST, assegnazioni);
	}

	/**
	 * Esegue la response <em> non verificato </em> sul documento identificato dal
	 * {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Nome utente operatore che esegue la response.
	 * @param dt
	 *            Identificativo del documento su cui viene eseguita la response.
	 * @return Esito response.
	 */
	protected EsitoOperazioneDTO nonVerificato(final String usernameOperatore, final String dt) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.DA_LAVORARE, dt, operatore.getIdUfficio().intValue(), operatore.getId().intValue());
		return vistisrv.nonVerificato(operatore, wo.getWorkObjectNumber(), SIMPLE_TEST);
	}

	/**
	 * Esegue la response <em> visto </em> sul documento identificato dal
	 * {@code dt}.
	 * 
	 * @param usernameOperatore
	 *            Nome utente operatore che esegue la response.
	 * @param dt
	 *            Identificativo del documento.
	 * @param esito
	 *            tipo approvazione del visto da applicare.
	 * @return Esito response.
	 */
	protected EsitoOperazioneDTO visto(final String usernameOperatore, final String dt, final TipoApprovazioneEnum esito) {
		final UtenteDTO operatore = getUtente(usernameOperatore);
		final VWSession session = getPESession(operatore);
		dumpDTInfo(dt);
		final VWWorkObject wo = fromDocumentTitleToWF(session, DocumentQueueEnum.NSD, dt, operatore.getIdUfficio().intValue(), operatore.getId().intValue());
		return vistisrv.visto(operatore, wo.getWorkflowNumber(), esito, SIMPLE_TEST);
	}

	/**
	 * Crea un ordine di pagamento sul documento identificato dal
	 * {@code documentTitle}.
	 * 
	 * @param documentTitle
	 *            Identificativo del documento su cui creare l'ordine di pagamento.
	 * @param username
	 *            Nome utente operatore.
	 */
	protected void createOrdinePagamento(final String documentTitle, final String username) {
		final UtenteDTO utente = getUtente(username);
		final Document decreto = fetchDecreto(documentTitle, utente);

		CustomObject elencoOP = null;

		if (decreto != null) {
			elencoOP = (CustomObject) decreto.getProperties().getObjectValue(PROPERTY_OP_PROXY);

			if (elencoOP == null) {
				elencoOP = createElencoOP(decreto, utente);
			}
		}

		final CustomObject opFilenet = com.filenet.api.core.Factory.CustomObject.createInstance(getObjectStore(utente), CLASSNAME_ORDINE_DI_PAGAMENTO_DD);

		opFilenet.getProperties().putValue(PROPERTY_PROXY_DECRETO, elencoOP);
		opFilenet.getProperties().putValue(PROPERTY_ESERCIZIO, 2018); // short
		opFilenet.getProperties().putValue(PROPERTY_AMMINISTRAZIONE, "1"); // String
		opFilenet.getProperties().putValue(PROPERTY_CODICE_RAGIONERIA, "2"); // String
		opFilenet.getProperties().putValue(PROPERTY_CAPITOLO, "1234"); // String
		opFilenet.getProperties().putValue(PROPERTY_NUMERO_OP, 1234); // short
		opFilenet.getProperties().putValue(PROPERTY_PIANO_GESTIONE, 1234); // short
		opFilenet.getProperties().putValue(PROPERTY_STATO_OP, "Inviato"); // EnumRedStatoDTO
		opFilenet.getProperties().putValue(PROPERTY_TIPO_OP, 1); // short

		final Object obj = decreto != null ? decreto.getProperties().getObjectValue("IdFascicoloFEPA") : null;
		if (obj != null) {
			opFilenet.getProperties().putValue(PROPERTY_ID_FASCICOLO_FEPA, obj.toString());
		}
		opFilenet.getProperties().putValue(PROPERTY_DATA_EMISSIONE_OP, new Date());
		opFilenet.getProperties().putValue(PROPERTY_OGGETTO, "testOP");
		opFilenet.getProperties().putValue(PROPERTY_BENEFICIARIO, PROPERTY_BENEFICIARIO);
		opFilenet.save(RefreshMode.REFRESH);
	}

	private CustomObject createElencoOP(final Document decreto, final UtenteDTO utente) {
		final CustomObject co = com.filenet.api.core.Factory.CustomObject.createInstance(getObjectStore(utente), CLASSNAME_ELENCO_ORDINI_PAGAMENTO_DD);
		co.getProperties().putValue(PROPERTY_NUMERO_PROTOCOLLO, Integer.parseInt(decreto.getProperties().getObjectValue("NumeroProtocollo").toString()));

		if (decreto.getProperties().getObjectValue("AnnoProtocollo") != null) {
			co.getProperties().putValue(PROPERTY_ANNO_PROTOCOLLO, Integer.parseInt(decreto.getProperties().getObjectValue("AnnoProtocollo").toString()));
		} else {
			co.getProperties().putValue(PROPERTY_ANNO_PROTOCOLLO, 0);
		}
		co.save(RefreshMode.REFRESH);

		decreto.getProperties().putObjectValue(PROPERTY_OP_PROXY, co);
		decreto.save(RefreshMode.NO_REFRESH);

		return co;
	}

	private Document fetchDecreto(final String idDocumento, final UtenteDTO utente) {
		final SearchScope ss = new SearchScope(getObjectStore(utente));
		final String queryString = "SELECT This, * FROM DecretoDirigenzialeFEPA WHERE IsCurrentVersion=TRUE AND [DocumentTitle] = '" + idDocumento + "'";
		final SearchSQL searchSQL = new SearchSQL(queryString);
		final IndependentObjectSet rows = ss.fetchObjects(searchSQL, null, null, false);
		final Iterator<?> iter = rows.iterator();
		Document decreto = null;
		while (iter.hasNext()) {
			decreto = (Document) iter.next();
		}

		return decreto;
	}

	/**
	 * Esegue il salvataggio del documento: {@code document}.
	 * 
	 * @param document
	 *            Documento da salvare.
	 * @param utenteCreatore
	 *            Utente creatore del documento.
	 * @return Esito del salvataggio del documento.
	 */
	protected EsitoSalvaDocumentoDTO salvaDocumento(final DetailDocumentRedDTO document, final UtenteDTO utenteCreatore) {
		final SalvaDocumentoRedParametriDTO parametri = new SalvaDocumentoRedParametriDTO();
		parametri.setModalita(Constants.Modalita.MODALITA_INS); // Siamo in inserimento
		parametri.setContentVariato(true); // essendo nuovo il content è variato
		return creaDocFacade.salvaDocumento(document, parametri, utenteCreatore, ProvenienzaSalvaDocumentoEnum.GUI_RED);
	}

	/**
	 * Recupera e restituisce i documenti ricercandoli per la chiave: {@code key}.
	 * 
	 * @param key
	 *            Chiave di ricerca documenti.
	 * @param anno
	 *            Anno del documento.
	 * @param utente
	 *            Utente operatore della ricerca.
	 * @return Collezione dei master recuperati.
	 */
	protected Collection<MasterDocumentRedDTO> ricercaDocumentiPerKey(final String key, final Integer anno, final UtenteDTO utente) {
		final RicercaResultDTO result = ricercaSRV.ricercaRapidaUtente(key, anno, RicercaGenericaTypeEnum.ESATTA, RicercaPerBusinessEntityEnum.DOCUMENTI, false, utente,
				false);
		return result.getDocumenti();
	}

	/**
	 * Esegue la response <em> annulla </em> sul documento identificato dal
	 * {@code wobNumber}.
	 * 
	 * @param utente
	 *            Utente operatore che esegue l'annullamento.
	 * @param wobNumber
	 *            Identificativo del documento.
	 * @param motivoRifiuto
	 *            Motivo dell'annunllamento.
	 * @param response
	 *            Response da applicare.
	 * @return Esito dell'annullamento.
	 */
	protected EsitoOperazioneDTO annulla(final UtenteDTO utente, final String wobNumber, final String motivoRifiuto, final ResponsesRedEnum response) {
		return operationDocSRV.rifiuta(utente, wobNumber, motivoRifiuto, response, false);
	}

	/**
	 * Restituisce i documenti associati ai fascicoli che fanno riferimento al
	 * protocollo {@code numProt}.
	 * 
	 * @param utente
	 *            Utente operatore della ricerca.
	 * @param numProt
	 *            Numero protocollo di riferimento dei documenti recuperati.
	 * @param annoProt
	 *            Anno protocollo del protocollo di riferimento dei documenti
	 *            recuperati.
	 * @param isForIntegrazioneDati
	 *            se {@code true} recupera i documenti validi per l'integrazione
	 *            dati, altrimenti li recupera tutti.
	 * @return Collezione dei documenti associati ai fascicoli.
	 */
	protected Map<FascicoloDTO, List<DocumentoAllegabileDTO>> getDocsProtRiferimento(final UtenteDTO utente, final Integer numProt, final Integer annoProt,
			final boolean isForIntegrazioneDati) {
		return fascicoloSRV.getDocsProtRiferimento(utente, numProt, annoProt, isForIntegrazioneDati);
	}

	/**
	 * Restituisce il fascicolo procedimentale del documento identificato dal
	 * {@code documentTitle}.
	 * 
	 * @param documentTitle
	 *            Identificativo del documento per il quale viene recuperato il
	 *            fascicolo procedimentale.
	 * @param utente
	 *            Utente operatore della ricerca.
	 * @return Fascicolo procedimentale del documento.
	 */
	protected FascicoloDTO getFascicoloProcedimentale(final String documentTitle, final UtenteDTO utente) {
		return fascicoloSRV.getFascicoloProcedimentale(documentTitle, utente.getIdAoo().intValue(), utente);
	}

	/**
	 * Restituisce il wob number del documento identificato dal {@code dt}.
	 * 
	 * @param utente
	 *            Utente operatore della ricerca.
	 * @param queue
	 *            Coda di lavoro in cui si trova il documento.
	 * @param dt
	 *            Identificativo del documento.
	 * @return Wob number del documento recuperato.
	 */
	protected String getWobNumber(final UtenteDTO utente, final DocumentQueueEnum queue, final String dt) {
		final VWSession session = getPESession(utente);
		final VWWorkObject wo = fromDocumentTitleToWF(session, queue, dt, utente.getIdUfficio().intValue(), utente.getId().intValue());
		return wo.getWorkflowNumber();
	}

	/**
	 * Verifica che le code passata come primo parametro siano esistenti nella lista
	 * delle code accettate lanciando un'eccezione in caso negativo.
	 * 
	 * @param codeAttuali
	 *            Code per le quali effettuare l' {@code assert}.
	 * @param codeAccettate
	 *            Lista delle code accettate.
	 */
	protected void checkAssertionCode(RecuperoCodeDTO codeAttuali, DocumentQueueEnum... codeAccettate) {
		
		for (DocumentQueueEnum c:codeAccettate) {
			LOGGER.info("Coda Accettata: " + c);
		}
		
		for (String c:codeAttuali.getCodeNonCensite()) {
			LOGGER.info("Coda non censita: " + c);
		}
		
		for (DocumentQueueEnum c:codeAttuali.getCode()) {
			LOGGER.info("Coda individuata: " + c);
		}
		
		Collection<String> codeNonCensite = codeAttuali.getCodeNonCensite();
		Collection<DocumentQueueEnum> codeNonAccettate = new ArrayList<>();
		
		for (DocumentQueueEnum codaAttuale:codeAttuali.getCode()) {
			Boolean bFound = false;
			for (DocumentQueueEnum codaAccettata:codeAccettate) {
				if (codaAccettata.equals(codaAttuale)) {
					bFound = true;
					break;
				}
			}
			if (!Boolean.TRUE.equals(bFound)) {
				codeNonAccettate.add(codaAttuale);
			}
		}
		
		checkAssertion(codeNonCensite.isEmpty(), "Il documento è presente in code non censite.");
		checkAssertion(codeNonAccettate.isEmpty(), "Il documento deve essere presente solamente nelle code accettate.");
		checkAssertion(codeAttuali.getCode().size() == codeAccettate.length, "Il documento deve essere presente in tutte le code accettate.");
	}

	/**
	 * Restituisce lo storico del documento identificato dal {@code documentTitle}.
	 * 
	 * @param username
	 *            Nome utente operatore.
	 * @param documentTitle
	 *            Identificativo del documento.
	 * @return Storico associato al documento.
	 */
	protected Collection<StoricoDTO> getStorico(final String username, final Integer documentTitle) {
		final UtenteDTO utente = getUtente(username);
		return storicoSRV.getStorico(documentTitle, utente.getIdAoo().intValue());
	}

}