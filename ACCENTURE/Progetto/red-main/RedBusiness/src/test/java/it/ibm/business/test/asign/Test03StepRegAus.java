package it.ibm.business.test.asign;

import java.sql.Connection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.SignStrategyEnum;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IRegistrazioniAusiliarieSRV;
import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * @author SimoneLungarella
 * Test legato allo step di registrazione ausiliaria. @see StepEnum.REG_AUX_PROTOCOLLAZIONE e StepEnum.REG_AUX_RIGENERAZIONE_CONTENT.
 *
 */
public class Test03StepRegAus extends AbstractAsignTest {

	/**
	 * Servizio gestione registrazioni ausiliarie.
	 */
	private IRegistrazioniAusiliarieSRV regAusSRV;

	/**
	 * Esegue le operazioni di data preparation prima dell'esecuzione del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		regAusSRV = ApplicationContextProvider.getApplicationContext().getBean(IRegistrazioniAusiliarieSRV.class);
	}

	/**
	 * Esegue le azioni finali dopo l'esecuzione del test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Esegue lo step di registrazione ausiliaria con utenti RGS.
	 */
	@Test
	public final void runRGS() {
		UtenteDTO utente = getUtente(USERNAME_RGS);

		ASignItemDTO itemPre = accoda(USERNAME_RGS, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, SignStrategyEnum.ASYNC_STRATEGY_B, null);
		run(StepEnum.PROTOCOLLAZIONE, itemPre.getId());
		checkAssertion(!hasRegAus(itemPre.getDocumentTitle(), utente.getIdAoo().intValue()), "Il documento prima dell'esecuzione degli step di reg aux non deve avere la registrazione ausiliaria.");
		
		// Esecuzione dei due step di registrazione ausiliaria
		run(StepEnum.REG_AUX_PROTOCOLLAZIONE, itemPre.getId());
		run(StepEnum.REG_AUX_RIGENERAZIONE_CONTENT, itemPre.getId());
		
		// Recupero informazioni successive alla registrazione ausiliaria
		ASignItemDTO itemPost = getaSignSRV().getItem(itemPre.getId(), true);
		Connection con = createNewDBConnection();
		RegistrazioneAusiliariaDTO regAux = regAusSRV.getRegistrazioneAusiliaria(itemPost.getDocumentTitle(), utente.getIdAoo().intValue(), con);
		
		checkAssertion(hasRegAus(itemPre.getDocumentTitle(), utente.getIdAoo().intValue()), "Il documento dopo l'esecuzione degli step di reg aux deve avere la registrazione ausiliaria.");
		checkAssertion("".equals(regAux.getIdRegistrazione()), "Non risulta presente un id valido a conferma dell'avvenuta registrazione ausiliaria");
		checkAssertion(regAux.isDocumentoDefinitivo(), "Il documento deve essere DEFINITIVO, se non lo è vuol dire che il template non è stato aggiornato correttamente");
	}

	/**
	 * Esegue lo step di registrazione ausiliaria con utenti UCB e quindi seguendo la strategia UCB.
	 */
	@Test
	public final void runUCB() {
		UtenteDTO utente = getUtente(USERNAME_LAVORO);

		ASignItemDTO itemPre = accoda(USERNAME_LAVORO, ID_TIPO_DOC_GENERICO_LAVORO_USCITA, ID_TIPO_PROC_GENERICO_LAVORO_USCITA, SignStrategyEnum.ASYNC_STRATEGY_A, null);
		run(StepEnum.PROTOCOLLAZIONE, itemPre.getId());
		checkAssertion(!hasRegAus(itemPre.getDocumentTitle(), utente.getIdAoo().intValue()), "Il documento prima dell'esecuzione degli step di reg aux non deve avere la registrazione ausiliaria.");
		
		// Esecuzione dei due step di registrazione ausiliaria
		run(StepEnum.REG_AUX_PROTOCOLLAZIONE, itemPre.getId());
		run(StepEnum.REG_AUX_RIGENERAZIONE_CONTENT, itemPre.getId());
		
		// Recupero informazioni successive alla registrazione ausiliaria
		ASignItemDTO itemPost = getaSignSRV().getItem(itemPre.getId(), true);
		Connection con = createNewDBConnection();
		RegistrazioneAusiliariaDTO regAux = regAusSRV.getRegistrazioneAusiliaria(itemPost.getDocumentTitle(), utente.getIdAoo().intValue(), con);
		
		checkAssertion(hasRegAus(itemPre.getDocumentTitle(), utente.getIdAoo().intValue()), "Il documento dopo l'esecuzione degli step di reg aux deve avere la registrazione ausiliaria.");
		checkAssertion("".equals(regAux.getIdRegistrazione()), "Non risulta presente un id valido a conferma dell'avvenuta registrazione ausiliaria");
		checkAssertion(regAux.isDocumentoDefinitivo(), "Il documento deve essere DEFINITIVO, se non lo è vuol dire che il template non è stato aggiornato correttamente");
	}

	/**
	 *  Simula un crash durante l'esecuzione dello step di registrazione ausiliaria.
	 */
	@Test
	public final void crash() {
		UtenteDTO utente = getUtente(USERNAME_LAVORO);
		Connection con = createNewDBConnection();

		ASignItemDTO itemPre = accoda(USERNAME_LAVORO, ID_TIPO_DOC_GENERICO_LAVORO_USCITA, ID_TIPO_PROC_GENERICO_LAVORO_USCITA, SignStrategyEnum.ASYNC_STRATEGY_A, null);
		checkAssertion(!hasRegAus(itemPre.getDocumentTitle(), utente.getIdAoo().intValue()), "Il documento non deve avere la registrazione ausiliaria.");
		
		run(StepEnum.PROTOCOLLAZIONE, itemPre.getId());
		ASignItemDTO itemPost = getaSignSRV().getItem(itemPre.getId(), true);
		
		// Crash prima fase
		crash(StepEnum.REG_AUX_PROTOCOLLAZIONE, itemPost.getId());
		checkAssertion(!hasRegAus(itemPost.getDocumentTitle(), utente.getIdAoo().intValue()), "Se si verifica un crash, il documento non deve avere una registrazione ausiliaria");
		
		// Run prima fase
		run(StepEnum.REG_AUX_PROTOCOLLAZIONE, itemPost.getId());
		checkAssertion(hasRegAus(itemPost.getDocumentTitle(), utente.getIdAoo().intValue()), "Il documento deve avere la registrazione ausiliaria.");
	
		// Crash seconda fase
		crash(StepEnum.REG_AUX_RIGENERAZIONE_CONTENT, itemPost.getId());
		RegistrazioneAusiliariaDTO regAux = regAusSRV.getRegistrazioneAusiliaria(itemPost.getDocumentTitle(), utente.getIdAoo().intValue(), con);
		
		checkAssertion(hasRegAus(itemPost.getDocumentTitle(), utente.getIdAoo().intValue()), "Se si verifica un crash durante la seconda fase, il documento deve avere la registrazione ausiliaria.");
		checkAssertion(!regAux.isDocumentoDefinitivo(), "Se si verifica un crash durante la seconda fase il documento non deve risultare come DEFINITIVO");
		
		// Run seconda fase
		run(StepEnum.REG_AUX_RIGENERAZIONE_CONTENT, itemPost.getId());
		regAux = regAusSRV.getRegistrazioneAusiliaria(itemPost.getDocumentTitle(), utente.getIdAoo().intValue(), con);
		
		checkAssertion(hasRegAus(itemPost.getDocumentTitle(), utente.getIdAoo().intValue()), "Il documento deve avere la registrazione ausiliaria.");
		checkAssertion(!regAux.isDocumentoDefinitivo(), "Il documento deve risultare come DEFINITIVO perché il template sia stato aggiornato correttamente");
	}
}
