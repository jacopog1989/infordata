package it.ibm.business.test.suite;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dao.impl.SimpleInsertBuilder;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.StringUtils;

/**
 * Classe di test Prop.
 */
public class PropTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PropTest.class);
	
	/**
	 * Line per leggibilità del log.
	 */
	private static final String DASHED_LINE = "------==========================================================------";

	/**
	 * Costruttore privato per impedirne l'inizializzazione dall'esterno.
	 */
	private PropTest() {
		// Costruttore vuoto.
	}
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public static final void beforeTest() {
		// Metodo intenzionalmente vuoto.
	}

	private static Map<String, String> getProp(final Connection con) throws SQLException {
		final String querySQL = "select key, value, blob_value from PROPERTIES order by 1";
		final Map<String, String> map = new HashMap<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement(querySQL);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				final String key = rs.getString("key");
				String value = rs.getString("value");
				final Blob blob = rs.getBlob("blob_value");
				if (blob != null) {
					value = "PRESENTE";
				}
				map.put(key, value);
			}

		} catch (final Exception e) {
			LOGGER.error(e);
		} finally {
			SimpleInsertBuilder.closeStatement(ps, rs);
		}
		
		return map;
	}
	
	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public static final void afterTest() {
		// Metodo intenzionalmente vuoto.
	}

	/**
	 * Test.
	 */
	@Test
	public static final void test() {
		Connection conCollaudo = null;
		Connection conSviluppo = null;
		try {
			conCollaudo = DriverManager.getConnection("jdbc:oracle:thin:@xdc1-scan.tesoro.it:1521/GESC", "NSD", "NSD");
			conSviluppo = DriverManager.getConnection("jdbc:oracle:thin:@10.38.60.61:1521:db521pres", "NSD_WS", "NSD_WS");

			final Map<String, String> mapCollaudo = getProp(conCollaudo);
			final Map<String, String> mapSviluppo = getProp(conSviluppo);
			
			LOGGER.info(DASHED_LINE);
			LOGGER.info("CHIAVI PRESENTI IN AMBIENTE DI COLLAUDO MA NON IN AMBIENTE DI SVILUPPO");
			for (final Entry<String, String> entry: mapCollaudo.entrySet()) {
				final String value = mapSviluppo.get(entry.getKey());
				if (StringUtils.isNullOrEmpty(value)) {
					LOGGER.info("<" + entry.getKey() + ">");
				}
			}
			LOGGER.info(DASHED_LINE);

			LOGGER.info("CHIAVI PRESENTI IN AMBIENTE DI SVILUPPO MA NON IN AMBIENTE DI COLLAUDO");
			for (final Entry<String, String> entry: mapSviluppo.entrySet()) {
				final String value = mapCollaudo.get(entry.getKey());
				if (StringUtils.isNullOrEmpty(value)) {
					LOGGER.info("<" + entry.getKey() + ">");
				}
			}
			LOGGER.info(DASHED_LINE);
			    
		} catch (SQLException e) {
			LOGGER.error(e);
		} finally {
			closeConnection (conCollaudo, conSviluppo);
		}
	}
	
	/**
	 * Chiude in maniera sicura le connessioni.
	 * @param cons
	 */
	private static void closeConnection (Connection... cons) {
		for (Connection c : cons) {
			try {
				if (!c.isClosed()) {
					c.close();
				}
			} catch (Exception e) {
				LOGGER.error("Errore riscontrato durante la chiusura della connessione.", e);
			}
		}
	}
}
