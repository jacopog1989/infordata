package it.ibm.business.test.suite;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IRubricaFacadeSRV;

/**
 * Classe di test rubrica.
 */
public class RubricaTest extends AbstractTest {

    /**
     * Logger.
     */
	private static final REDLogger LOGGER = REDLogger.getLogger(RubricaTest.class);

    /**
     * Servizio.
     */
	private IRubricaFacadeSRV rubricaSRV;

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		rubricaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaFacadeSRV.class);
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test inserimento del contatto "on the fly".
	 */
	@Test
	public void inserisciOnTheFly() {
		UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		String inNome = "Federfarma Teramo";
		String inCognome = null;
		String inAliasContatto = "DGROB_DIVISIONE VII - TRATTAMENTO ECONOMICO";
		String inMail = null;
		Contatto inContatto = new Contatto(inNome, inCognome, null, null, inMail, null, inAliasContatto, 1, utente.getIdAoo());
		Contatto outContatto = rubricaSRV.insertContattoFlussoAutomatico(inContatto, utente);
		LOGGER.info(outContatto.toString());

	}

}