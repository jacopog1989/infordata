package it.ibm.business.test.suite;

import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.concrete.MasterPageIterator;
import it.ibm.red.business.service.facade.IMasterPaginatiFacadeSRV;

/**
 * Classe di test paginator.
 */
public class PaginatorTest extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PaginatorTest.class);
	
	/**
	 * Servizio.
	 */
	private IMasterPaginatiFacadeSRV masterPaginatiSRV;
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		masterPaginatiSRV = ApplicationContextProvider.getApplicationContext().getBean(IMasterPaginatiFacadeSRV.class);
	}
	
	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Esegue il test di firma decreto.
	 */
	@Test
	public final void testFirmaDecreto() {
		
		UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);

		MasterPageIterator pagedResult = masterPaginatiSRV.getMastersRaw(DocumentQueueEnum.NSD, null, utente);

		Integer tot = 0;
		for (;;) {
			Collection<MasterDocumentRedDTO> tmp = masterPaginatiSRV.refineMasters(utente, pagedResult);
			tot += tmp.size();
			
			checkAssertion(tmp.size() <= 10, "Pagina troppo grande.");
			
			LOGGER.info(String.valueOf(tmp.size()));
			if (tmp.isEmpty()) {
				break;
			}
		}
		
		LOGGER.info(String.valueOf(tot));
	}
	
}
