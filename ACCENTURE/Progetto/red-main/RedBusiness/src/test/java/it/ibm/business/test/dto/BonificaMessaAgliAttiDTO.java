package it.ibm.business.test.dto;

import java.util.Date;

import it.ibm.red.business.dto.AbstractDTO;

/**
 * Dto per la gestione delle bonifiche messagliatti su NPS.
 * 
 * @author a.dilegge
 *
 */
public class BonificaMessaAgliAttiDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8033966326501768489L;
	
	/**
	 * Identificativo utente.
	 */
	private Long idUtente;
	
	/**
	 * Identificativo nodo.
	 */
	private Long idNodo;
	
	/**
	 * Timestamp operatore.
	 */
	private Date timestampOperazione;
	
	/**
	 * Identificativo protocollo.
	 */
	private String idProtocollo;
	
	/**
	 * Numero protocollo.
	 */
	private Integer numeroProtocollo;
	
	/**
	 * Anno protocollo.
	 */
	private Integer annoProtocollo;
	
	/**
	 * Data protocollo.
	 */
	private Date dataProtocollo;
	
	/**
	 * Flag entrata/uscita.
	 */
	private String flagIU;
	
	/**
	 * Document title.
	 */
	private String documentTitle;

	/**
	 * Costruttore del dto.
	 * @param idUtente
	 * @param idNodo
	 * @param timestampOperazione
	 * @param idProtocollo
	 * @param numeroProtocollo
	 * @param annoProtocollo
	 * @param dataProtocollo
	 * @param flagIU
	 * @param documentTitle
	 */
	public BonificaMessaAgliAttiDTO(final Long idUtente, final Long idNodo, final Date timestampOperazione, final String idProtocollo, final Integer numeroProtocollo,
			final Integer annoProtocollo, final Date dataProtocollo, final String flagIU, final String documentTitle) {
		super();
		this.idUtente = idUtente;
		this.idNodo = idNodo;
		this.timestampOperazione = timestampOperazione;
		this.idProtocollo = idProtocollo;
		this.numeroProtocollo = numeroProtocollo;
		this.annoProtocollo = annoProtocollo;
		this.dataProtocollo = dataProtocollo;
		this.flagIU = flagIU;
		this.documentTitle = documentTitle;
	}

	/**
	 * Restituisce l'id dell'utente come Long.
	 * @return idUtente
	 */
	public Long getIdUtente() {
		return idUtente;
	}

	/**
	 * Imposta l'id dell'utente.
	 * @param idUtente
	 */
	public void setIdUtente(final Long idUtente) {
		this.idUtente = idUtente;
	}

	/**
	 * Restituisce l'id del nodo.
	 * @return idNodo
	 */
	public Long getIdNodo() {
		return idNodo;
	}

	/**
	 * Imposta l'id del nodo.
	 * @param idNodo
	 */
	public void setIdNodo(final Long idNodo) {
		this.idNodo = idNodo;
	}

	/**
	 * Restituisce il timestamp associato all'operazione.
	 * @return timestampOperazione
	 */
	public Date getTimestampOperazione() {
		return timestampOperazione;
	}

	/**
	 * Imposta il timestamp associato all'operazione.
	 * @param timestampOperazione
	 */
	public void setTimestampOperazione(final Date timestampOperazione) {
		this.timestampOperazione = timestampOperazione;
	}

	/**
	 * Restituisce l'id del protocollo.
	 * @return idProtocollo
	 */
	public String getIdProtocollo() {
		return idProtocollo;
	}

	/**
	 * Imposta l'id del protocollo.
	 * @param idProtocollo
	 */
	public void setIdProtocollo(final String idProtocollo) {
		this.idProtocollo = idProtocollo;
	}

	/**
	 * Restituisce il numero del protocollo.
	 * @return numeroProtocollo
	 */
	public Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Imposta il numero del protocollo.
	 * @param numeroProtocollo
	 */
	public void setNumeroProtocollo(final Integer numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	/**
	 * Restituisce l'anno del protocollo.
	 * @return annoProtocollo
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * Imposta l'anno del protocollo.
	 * @param annoProtocollo
	 */
	public void setAnnoProtocollo(final Integer annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/**
	 * Restituisce la data del protocollo.
	 * @return dataProtocollo
	 */
	public Date getDataProtocollo() {
		return dataProtocollo;
	}

	/**
	 * Imposta la data del protocollo.
	 * @param dataProtocollo
	 */
	public void setDataProtocollo(final Date dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}

	/**
	 * Restituisce il flag legato all'IU.
	 * @return flagIU
	 */
	public String getFlagIU() {
		return flagIU;
	}

	/**
	 * Imposta il flagIU.
	 * @param flagIU
	 */
	public void setFlagIU(final String flagIU) {
		this.flagIU = flagIU;
	}

	/**
	 * Restituisce il documentTitle associato al documento.
	 * @return documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta il document title.
	 * @param documentTitle
	 */
	public void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}
	
}