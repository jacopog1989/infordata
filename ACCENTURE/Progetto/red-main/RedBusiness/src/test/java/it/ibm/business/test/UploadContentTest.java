package it.ibm.business.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.service.facade.IUpdateContentFacadeSRV;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;

/**
 * Classe di test upload content.
 */
public class UploadContentTest extends AbstractTest {

	/**
	 * Content.
	 */
	private IUpdateContentFacadeSRV updateContent;

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Wf.
	 */
	private String wobNumber;

	/**
	 * Content.
	 */
	private byte[] newContent;
	
	/**
	 * Esegue le azioni prima del test.
	 * @throws IOException
	 */
	@Before
	public void initialize() throws IOException {
		before();
		updateContent = getBean(IUpdateContentFacadeSRV.class);
		IUtenteFacadeSRV facadeUtente = getBean(IUtenteFacadeSRV.class);
		utente = facadeUtente.getByUsername("BIAGIO.MAZZOTTA");
		wobNumber = "D8DF671014157548995EBC4A97839700";
		newContent = Files.readAllBytes(Paths.get("C:\\Users\\RobertoBitto\\Desktop\\Test.docx"));

	}
	
	/**
	 * Test upload file.
	 */
	@Test
	public void uploadContent() {
		updateContent.updateContent(wobNumber, newContent, utente);
	}
}
