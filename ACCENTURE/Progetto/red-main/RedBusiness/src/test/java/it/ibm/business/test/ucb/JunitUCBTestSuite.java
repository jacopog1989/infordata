package it.ibm.business.test.ucb;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

/**
 * 
 * @author CRISTIANOPierascenzi
 */

@Suite.SuiteClasses({
	Test00RispostaInoltra.class,
	Test01Visto.class,
	Test02Osservazione.class,
	Test03RichiestaIntegrazioni.class,
	Test04Restituzione.class,
	Test05AttrExt.class,
	Test06Flussi.class
})

public class JunitUCBTestSuite {   
}  
