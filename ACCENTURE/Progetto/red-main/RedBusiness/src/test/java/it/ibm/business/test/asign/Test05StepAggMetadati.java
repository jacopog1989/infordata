package it.ibm.business.test.asign;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignStepResultDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.SignStrategyEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * Test step di aggiornamento metadati. @see StepEnum.AGGIORNAMENTO_METADATI.
 */
public class Test05StepAggMetadati extends AbstractAsignTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test05StepAggMetadati.class.getName());
	
	/**
	 * Servizio gestione fascicoli.
	 */
	private IFascicoloSRV fascicoloSRV;

	/**
	 * Esegue le azioni prima dell'esecuzione del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloSRV.class);
	}

	/**
	 * Esegue le azioni dopo l'esecuzione del test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Esegue lo step di aggiornamento dei metadati.
	 */
	@Test
	public final void run() {

		UtenteDTO destInterno = getUtente(USERNAME_TANZI);
		Integer idNodoDestInt = destInterno.getIdUfficio().intValue();
		Integer idUtenteDestInt = destInterno.getId().intValue();
		Integer idContattoDestInt = ID_CONTATTO_UNO;

		ASignItemDTO itemPre = accoda(USERNAME_RGS, ID_TIPO_DOC_BILANCIO_ENTI_RGS_USCITA, ID_TIPO_PROC_BILANCIO_ENTI_RGS_USCITA, SignStrategyEnum.ASYNC_STRATEGY_B, idNodoDestInt, idUtenteDestInt, idContattoDestInt, "idFascFEPA");
		
		Boolean bCheck = false;
		FascicoloDTO fascicolo = null;
		try {
			fascicolo = fascicoloSRV.getFascicoloProcedimentale(itemPre.getDocumentTitle(), destInterno.getIdAoo().intValue(), destInterno);
		} catch (Exception e) {
			LOGGER.warn(e);
			bCheck = true;
		}
		checkAssertion(bCheck, "Il fascicolo non deve essere visibile al destinatario interno prima di aprire il cono.");

		run(StepEnum.PROTOCOLLAZIONE, itemPre.getId());
		run(StepEnum.REG_AUX_PROTOCOLLAZIONE, itemPre.getId());
		run(StepEnum.STAMPIGLIATURE, itemPre.getId());

		ASignStepResultDTO updateData = run(StepEnum.AGGIORNAMENTO_METADATI, itemPre.getId());

		checkAssertion(updateData.getStatus(), "Aggiornamento metadati con invocazione bislancio enti avvenuto con successo.");
		checkAssertion(fascicolo != null, "Il fascicolo deve essere visibile al destinatario interno dopo aver aperto il cono.");
	}

}
