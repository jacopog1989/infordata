package it.ibm.business.test.suite;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.AssegnatarioOrganigrammaWFAttivoDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.helper.assegnazioni.AssegnazioniEntrataHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;

/**
 * Classe di test dello storico.
 */
public class StoricoTest extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(StoricoTest.class);
	
	/**
	 * Label ufficio.
	 */
	private static final String UFFICIO = "UFFICIO : ";
	
	/**
	 * Label utente.
	 */
	private static final String UTENTE = "UTENTE : ";
	
	/**
	 * Label WF.
	 */
	private static final String WF = "WF : ";
	
	/**
	 * Label Attivo.
	 */
	private static final String ATTIVO = "ATTIVO : ";
	
	/**
	 * Line per leggibilita console.
	 */
	private static final String DASHED_LINE = "---------";

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}
	
	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test Storico.
	 */
	@Test
	public final void test() {

		final Integer documentTitle = 456771;
		final String username = USERNAME_MINNIELLI;
		final List<StoricoDTO> storico = (List<StoricoDTO>) getStorico(username, documentTitle);
		
		final UtenteDTO utente = getUtente(username);
		final FilenetCredentialsDTO fcDTO = utente.getFcDTO();
		final FilenetPEHelper fpeh = new FilenetPEHelper(fcDTO);
		
		final AssegnazioniEntrataHelper helper = new AssegnazioniEntrataHelper();
		helper.calcolaAssegnazioni(storico, fpeh);
		
		LOGGER.info(DASHED_LINE + " COMPETENZA");
		for (final AssegnatarioOrganigrammaWFAttivoDTO ass: helper.getAssegnatariCompetenza()) {
			LOGGER.info(UFFICIO + ass.getIdNodo() + "\n" + UTENTE + ass.getIdUtente() + "\n" 
					+ WF + ass.getWobNumber() + ATTIVO + ass.isWfAttivo() + "\n" + DASHED_LINE);
		}

		LOGGER.info(DASHED_LINE + " CONOSCENZA");
		for (final AssegnatarioOrganigrammaWFAttivoDTO ass: helper.getAssegnatariConoscenza()) {
			LOGGER.info(UFFICIO + ass.getIdNodo() + "\n" + UTENTE + ass.getIdUtente() + "\n"
					+ WF + ass.getWobNumber() + ATTIVO + ass.isWfAttivo() + DASHED_LINE);
		}
		
		LOGGER.info("---------- CONTRIBUTO");
		for (final AssegnatarioOrganigrammaWFAttivoDTO ass: helper.getAssegnatariContributo()) {
			LOGGER.info(UFFICIO + ass.getIdNodo() + "\n" + UTENTE + ass.getIdUtente() + "\n" 
					+ WF + ass.getWobNumber() + ATTIVO + ass.isWfAttivo() + "\n" + DASHED_LINE);
		}
	
		
	}

}
