package it.ibm.business.test;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;

import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.VerifyInfoDTO;
import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;
import it.ibm.red.business.helper.signing.SignHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.PkHandler;
import it.ibm.red.business.utils.FileUtils;

/**
 * Classe di test per la firma XADES.
 * 
 * @author SimoneLungarella
 */
public class XadesTest extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(XadesTest.class);

	/**
	 * File path.
	 */
	private static final String FOLDER_PATH = "";
	
	/**
	 * Nome file xml.
	 */
	private static final String FILENAME_XML = "DADDABBODEC.xml";
	
	/**
	 * Id nodo lorenzo.quinzi.
	 */
	private static final long NODO_QUINZI_ID = 3203;
	
	/**
	 * Id utente lorenzo.quinzi.
	 */
	private static final long UTENTE_QUINZI_ID = 37003;

	/**
	 * Esegue azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue la verifica della firma xades.
	 */
	@Test
	public void verificaFirmaXades() {
		UtenteDTO utente = getUtente(UTENTE_QUINZI_ID, NODO_QUINZI_ID);
		assertTrue("Utente non recuperato correttamente.", utente != null);

		PkHandler pkHandler = utente.getSignerInfo().getPkHandlerVerifica();
		assertTrue("Handler di firma non recuperato correttamente", pkHandler != null);
		
		byte[] contentFile = FileUtils.getFileFromFS(FOLDER_PATH + FILENAME_XML);
		assertTrue("Content file non recuperato correttamente.", contentFile != null);

		// Verifica firma.
		SignHelper shVerifica = new SignHelper(pkHandler.getHandler(), null, utente.getDisableUseHostOnly());
		VerifyInfoDTO infoFirma = shVerifica.infoFirmaDocumento(new ByteArrayInputStream(contentFile), true);
		Integer valueMetadato = infoFirma.getMetadatoValiditaFirmaValue();
		
		if (ValueMetadatoVerificaFirmaEnum.FIRMA_OK.getValue().equals(valueMetadato)) {
			LOGGER.info("Firma valida.");
		} else if (ValueMetadatoVerificaFirmaEnum.FIRMA_KO.getValue().equals(valueMetadato)) {
			LOGGER.info("Firma non valida.");
		}

		else if(infoFirma.getErrorCodePK() != null) {
			LOGGER.error("Codice errore PK: " + infoFirma.getErrorCodePK());
		}
	}

}
