package it.ibm.business.test.asign;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ITrasformazionePDFFacadeSRV;

/**
 * @author s.lungarella.ibm
 * Classe che consente di eseguire la trasformazione dei documenti senza necessità di eseguire il job di trasformazione: @see PDFTransformerAooJob.
 */

public class PDFTransformerTest extends AbstractAsignTest {

	/**
	 * Service trasformazione.
	 */
	private ITrasformazionePDFFacadeSRV trasformazionePDFSRV;

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Crea un documento e ne esegue la trasformazione.
	 */
	@Test
	public final void createDocAndTransform() {
		int idAoo = 25;
		UtenteDTO utente = getUtente(USERNAME_RGS);
		createDocAutoassegnatoFirma(null, utente, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, null);
		trasformazionePDFSRV = ApplicationContextProvider.getApplicationContext().getBean(ITrasformazionePDFFacadeSRV.class);
		trasformazionePDFSRV.cleanTransformation(idAoo);
	}
	
	/**
	 * Consente la simulazione di una esecuzione singola del job di trasformazione.
	 */
	@Test
	public final void transform() {
		int idAooUCB = 34;
		trasformazionePDFSRV = ApplicationContextProvider.getApplicationContext().getBean(ITrasformazionePDFFacadeSRV.class);
		trasformazionePDFSRV.cleanTransformation(idAooUCB);
	}
	
}
