package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IRicercaSRV;
import it.ibm.red.business.service.concrete.MasterPageIterator;
import it.ibm.red.business.service.facade.IMasterPaginatiFacadeSRV;

/**
 * Classe di test del context.
 */
public class ContextTest extends AbstractTest {

    /**
     * Service.
     */
	private IMasterPaginatiFacadeSRV masterPaginatiSRV;

    /**
     * Service.
     */
	private IRicercaSRV ricercaSRV;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContextTest.class.getName());
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		masterPaginatiSRV = ApplicationContextProvider.getApplicationContext().getBean(IMasterPaginatiFacadeSRV.class);
		ricercaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaSRV.class);
	}
	
	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Esegue test del contesto.
	 */
	@Test
	public final void testContext() {
		UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		Collection<Long> times = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			Long start = new Date().getTime();
			MasterPageIterator pagedResult = masterPaginatiSRV.getMastersRaw(DocumentQueueEnum.NSD, null, utente);
			masterPaginatiSRV.refineMasters(utente, pagedResult);
			Long stop = new Date().getTime();
			times.add(stop - start);
		}
		for (Long t:times) {
			LOGGER.info(t.toString());
		}
	}
	
	/**
	 * Esegue la ricerca.
	 */
	@Test
	public final void ricerca() {
		UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		
		Collection<Long> times = new ArrayList<>();
		for (int i = 0; i < 1; i++) {
			Long start = new Date().getTime();
			
			//UPPER 63
			
//			red	31
//			Red 11
//			rEd	0
//			reD	0
//			rED	0
//			ReD 0
//			REd 0
//			RED 25
			
			Collection<MasterDocumentRedDTO> res = ricercaSRV.ricercaRapidaDocumenti("test", 2018, RicercaGenericaTypeEnum.TUTTE, utente);
			
			Long stop = new Date().getTime();
			times.add(stop - start);
			LOGGER.info(String.valueOf(res.size()));
		}
		for (Long t:times) {
			LOGGER.info(t.toString());
		}
	}
	//1393
}
