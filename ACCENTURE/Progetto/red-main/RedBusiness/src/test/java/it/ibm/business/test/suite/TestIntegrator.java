package it.ibm.business.test.suite;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.SicogeResultDTO;
import it.ibm.red.business.dto.SigiResultDTO;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.concrete.IntegratorSRV;

/**
 * Test Integrator.
 */
public class TestIntegrator extends AbstractTest {

	/**
	 * Servizio.
	 */
	private IntegratorSRV integratorSRV;

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		integratorSRV = ApplicationContextProvider.getApplicationContext().getBean(IntegratorSRV.class);
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test SICOGE.
	 */
	@Test
	public final void testSICOGE() {
		Integer idAooRGS = 25;
		SicogeResultDTO resultSicoge = integratorSRV.syncSICOGE(idAooRGS);
		checkAssertion(resultSicoge != null && resultSicoge.getResult() != null && !resultSicoge.getResult().keySet().isEmpty(), "Il batch SICOGE deve elaborare degli ordini di pagamento.");
	}

	/**
	 * Test SIGI.
	 */
	@Test
	public final void testSIGI() {
		Integer idAooRGS = 25;
		SigiResultDTO resultSigi = integratorSRV.syncSIGI(idAooRGS);
		checkAssertion(resultSigi != null && resultSigi.getResult() != null && !resultSigi.getResult().keySet().isEmpty(), "Il batch SIGI deve elaborare degli ordini di pagamento.");
	}

}
