package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Test 08.
 */
public class Test08 extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test08.class);
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}
	
	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test 08.
	 */
	@Test
	public final void test() {
		String oggetto = createObj();
		//Creazoine documento ingresso in competenza ad IGF UFFICIO I
		
		UtenteDTO utente = getUtente(USERNAME_LUCARELLI);
		
		Boolean bRiservato = false;
		String indiceFascicoloProcedimentale = "A";

		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(createDestinatarioInterno(ID_IGF, ID_GIANFANCO_TANZI, ID_CONTATTO_GIANCARLO_ROSSI, ModalitaDestinatarioEnum.TO));
		destinatari.add(createDestinatarioInterno(ID_IGF, ID_ROBERTO_NUNZI, ID_CONTATTO_GIANCARLO_ROSSI, ModalitaDestinatarioEnum.TO));
		
		List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneSigla(ID_IGF, ID_GIANFANCO_TANZI, "GIANFRANCO", "TANZI"));
		List<AllegatoDTO> allegati = null;
		
		EsitoSalvaDocumentoDTO esitoUscita = createDocUscita(allegati, false, oggetto, utente, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, assegnazioni, destinatari, indiceFascicoloProcedimentale, MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD, IterApprovativoDTO.ITER_FIRMA_MANUALE, bRiservato, null);
		checkAssertion(esitoUscita.isEsitoOk(), "Errore in fase di creazione documento in uscita.");
		String dt = esitoUscita.getDocumentTitle();

		// TANZI: CORRIERE -> LIBRO FIRMA
		EsitoOperazioneDTO esitoProcedi = procedi(USERNAME_LUCARELLI, dt);
		checkAssertion(esitoProcedi.isEsito(), "Errore in fase di procedi da corriere.");
	
		// TANZI: LIBRO FIRMA -> DA LAVORARE
		EsitoOperazioneDTO esitoSiglaInvia = siglaInvia(USERNAME_TANZI, dt);
		checkAssertion(esitoSiglaInvia.isEsito(), "Errore in fase di sigla ed invia.");

		// LUCARELLI: DA LAVORARE -> DA LAVORARE
		EsitoOperazioneDTO esitoFaldona = faldona(USERNAME_LUCARELLI, dt, "210");
		checkAssertion(esitoFaldona.isEsito(), "Errore in fase di creazione documento in uscita.");

		// LUCARELLI: DA LAVORARE -> ATTI
		EsitoOperazioneDTO esitoMessaAtti = messaAgliAtti(USERNAME_LUCARELLI, dt);
		checkAssertion(esitoMessaAtti.isEsito(), "Errore in fase di messa agli atti.");

		LOGGER.info("DOCUMENT TITLE : " + dt);
		LOGGER.info("OGGETTO : " + oggetto);
		LOGGER.info("NUM DOC : " + getNumDoc(USERNAME_NUNZI, dt));
	}

}
