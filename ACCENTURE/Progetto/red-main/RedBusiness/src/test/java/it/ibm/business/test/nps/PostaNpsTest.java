package it.ibm.business.test.nps;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.UUID;

import javax.xml.datatype.DatatypeConfigurationException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.business.test.suite.AbstractTest;
import it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.builder.Builder;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IPostaNpsSRV;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.AOO;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.CodiceAOO;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.Descrizione;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.Documento;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.IndirizzoTelematico;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.Intestazione;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.Mittente;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.Note;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.Oggetto;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.Origine;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.Segnatura;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioPostaType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.EmailIndirizzoEmailType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.EmailMessageType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.OrgAooType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.OrgCasellaEmailType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.ProtValidazioneMessaggioType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.ProtValidazioneSegnaturaType;
import it.ibm.red.webservice.model.interfaccianps.postacerttypes.PostacertType;

/**
 * Classe di test posta NPS.
 */
public class PostaNpsTest extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PostaNpsTest.class);
	
	/**
	 * Struttura invocazione SOAP.
	 */
	private it.ibm.red.webservice.model.interfaccianps.npsmessages.ObjectFactory ofMessages;
	
	/**
	 * Struttura invocazione SOAP.
	 */
	private it.ibm.red.webservice.model.interfaccianps.npstypes.ObjectFactory ofTypes;

	/**
	 * Struttura invocazione SOAP.
	 */
	private it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.ObjectFactory ofDigitPa;
	
	/**
	 * Struttura invocazione SOAP.
	 */
	private it.ibm.red.webservice.model.interfaccianps.postacerttypes.ObjectFactory ofPostacertTypes;
	
	/**
	 * Connesione.
	 */
	private Connection con;
	
	/**
	 * Servizio.
	 */
	private IPostaNpsSRV postaNpsSrv;
	
	/**
	 * Codice AOO.
	 */
	private static final String CODICE_AOO = "RGS";
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public void setBefore() {
		super.before();
		
		con = createNewDBConnection();
		postaNpsSrv = ApplicationContextProvider.getApplicationContext().getBean(IPostaNpsSRV.class);
		
		ofMessages = new it.ibm.red.webservice.model.interfaccianps.npsmessages.ObjectFactory();
		ofTypes = new it.ibm.red.webservice.model.interfaccianps.npstypes.ObjectFactory();
		ofDigitPa = new it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.ObjectFactory();
		ofPostacertTypes = new it.ibm.red.webservice.model.interfaccianps.postacerttypes.ObjectFactory();
	}
	
	/**
	 * Esegue il test crea richiesta elaborazione messaggio posta NPS.
	 * @throws DatatypeConfigurationException
	 */
	@Test
	public void createRichiestaElaborazioneMessaggioPostaNps() throws DatatypeConfigurationException {
		RichiestaElaboraMessaggioPostaType richiestaElabMessaggioPosta = ofMessages.createRichiestaElaboraMessaggioPostaType();
		
		// MESSAGGIO -> START
		EmailMessageType messaggio = ofTypes.createEmailMessageType();
		messaggio.setCodiMessaggio("<95CBB4E4821947FEBA81F8FF90039FC8@mef.gov.it>");
		messaggio.setOggettoMessaggio("Oggetto del messaggio");
		messaggio.setDataMessaggio(Builder.buildXmlGregorianCalendarFromDate(new Date()));
		messaggio.setCorpoMessaggio("Corpo messaggio");
		messaggio.setTipoMessaggio("PEC_ACCETTAZIONE");
		
		EmailIndirizzoEmailType mittente = ofTypes.createEmailIndirizzoEmailType();
		mittente.setDisplayName("Nome mittente");
		mittente.setEmail("mittente@mittenti.mef.gov.it");
		messaggio.setMittente(mittente);
		
		OrgAooType aoo = ofTypes.createOrgAooType();
		aoo.setCodiceAOO(CODICE_AOO);
		EmailIndirizzoEmailType mittenteCasella = ofTypes.createEmailIndirizzoEmailType();
		mittenteCasella.setDisplayName("Casella mittente");
		mittenteCasella.setEmail("testred2@pec.mef.gov.it");
		
		OrgCasellaEmailType casellaMittente = ofTypes.createOrgCasellaEmailType();
		casellaMittente.setIndirizzoEmail(mittenteCasella);
		casellaMittente.setAoo(aoo);
		messaggio.setCasellaEmail(casellaMittente);
		
		EmailIndirizzoEmailType dest1 = ofTypes.createEmailIndirizzoEmailType();
		dest1.setDisplayName("Destinatario 1");
		dest1.setEmail("destinatario1@destinatari.mef.gov.it");
		messaggio.getDestinatari().add(dest1);
		// MESSAGGIO -> END
		
		// SEGNATURA -> START
		Segnatura segnatura = ofDigitPa.createSegnatura();
		
		Note note = ofDigitPa.createNote();
		note.setContent("Nota di test");
		
		Intestazione intestazioneSegnatura = ofDigitPa.createIntestazione();
		intestazioneSegnatura.setNote(note);
		Oggetto oggettoSegnatura = ofDigitPa.createOggetto();
		oggettoSegnatura.setContent("Oggetto segnatura");
		
		intestazioneSegnatura.setOggetto(oggettoSegnatura);
		
		Origine origineSegnatura = ofDigitPa.createOrigine();
		
		IndirizzoTelematico indirizzoMail = ofDigitPa.createIndirizzoTelematico();
		indirizzoMail.setContent("mittentesegnatura@mittenti.mef.gov.it");
		origineSegnatura.setIndirizzoTelematico(indirizzoMail);
		
		Mittente mittenteSegnatura = ofDigitPa.createMittente();
		AOO aooSegnatura = ofDigitPa.createAOO();
		CodiceAOO codiceAooSegnatura = ofDigitPa.createCodiceAOO();
		codiceAooSegnatura.setContent(CODICE_AOO);
		aooSegnatura.setCodiceAOO(codiceAooSegnatura);
		mittenteSegnatura.setAOO(aooSegnatura);
		origineSegnatura.setMittente(mittenteSegnatura);
		
		intestazioneSegnatura.setOrigine(origineSegnatura);
		
		segnatura.setIntestazione(intestazioneSegnatura);
		
		Descrizione descrizioneSegnatura = ofDigitPa.createDescrizione();
		descrizioneSegnatura.setNote(note);
		
		Documento docPrincipale = ofDigitPa.createDocumento();
		docPrincipale.setNome("Nome del documento principale.pdf");
		
		descrizioneSegnatura.setDocumento(docPrincipale);
		
		segnatura.setDescrizione(descrizioneSegnatura);
		// SEGNATURA -> END
		
		// DATICERT -> START
		PostacertType datiCert = ofPostacertTypes.createPostacertType();
		datiCert.setTipo("accettazione");
		it.ibm.red.webservice.model.interfaccianps.postacerttypes.Intestazione intestazioneDatiCert = ofPostacertTypes.createIntestazione();
		
		it.ibm.red.webservice.model.interfaccianps.postacerttypes.Mittente mittenteDatiCert = ofPostacertTypes.createMittente();
		mittenteDatiCert.setContent("mittentedaticert@mittentidaticert.mef.gov.it");
		it.ibm.red.webservice.model.interfaccianps.postacerttypes.Oggetto oggettoDatiCert = ofPostacertTypes.createOggetto();
		oggettoDatiCert.setContent("Oggetto Daticert");
		intestazioneDatiCert.setOggetto(oggettoDatiCert);
		intestazioneDatiCert.setMittente(mittenteDatiCert);
		
		it.ibm.red.webservice.model.interfaccianps.postacerttypes.Destinatari destinatario1DatiCert = ofPostacertTypes.createDestinatari();
		destinatario1DatiCert.setTipo("esterno");
		destinatario1DatiCert.setContent("destinatarioesterno@mef.gov.it");
		intestazioneDatiCert.getDestinatari().add(destinatario1DatiCert);
		
		it.ibm.red.webservice.model.interfaccianps.postacerttypes.Destinatari destinatario2DatiCert = ofPostacertTypes.createDestinatari();
		destinatario2DatiCert.setTipo("certificato");
		destinatario2DatiCert.setContent("destinatariocertificato@mef.gov.it");
		intestazioneDatiCert.getDestinatari().add(destinatario2DatiCert);
		
		it.ibm.red.webservice.model.interfaccianps.postacerttypes.Dati datiDatiCert = ofPostacertTypes.createDati();
		
		it.ibm.red.webservice.model.interfaccianps.postacerttypes.Msgid msgIdDatiCert = ofPostacertTypes.createMsgid();
		msgIdDatiCert.setContent("&lt;95CBB4E4821947FEBA81F8FF90039FC8@mef.gov.it&gt;");
		it.ibm.red.webservice.model.interfaccianps.postacerttypes.Identificativo identificativoDatiCert = ofPostacertTypes.createIdentificativo();
		identificativoDatiCert.setContent("opec2102.20190918135723.23498.20.1.209@pupec.mef.gov.it");
		
		datiDatiCert.setMsgid(msgIdDatiCert);
		datiDatiCert.setIdentificativo(identificativoDatiCert);
		
		it.ibm.red.webservice.model.interfaccianps.postacerttypes.GestoreEmittente gestoreEmittenteDatiCert = ofPostacertTypes.createGestoreEmittente();
		gestoreEmittenteDatiCert.setContent("Namirial S.p.A.");
		
		it.ibm.red.webservice.model.interfaccianps.postacerttypes.Data dataDatiCert = ofPostacertTypes.createData();
		it.ibm.red.webservice.model.interfaccianps.postacerttypes.Ora oraDataDatiCert = ofPostacertTypes.createOra();
		oraDataDatiCert.setContent("13:57:23");
		it.ibm.red.webservice.model.interfaccianps.postacerttypes.Giorno giornoDatiCert = ofPostacertTypes.createGiorno();
		giornoDatiCert.setContent("18/09/2019");
		dataDatiCert.setZona("+0200");
		dataDatiCert.setGiorno(giornoDatiCert);
		dataDatiCert.setOra(oraDataDatiCert);
		
		it.ibm.red.webservice.model.interfaccianps.postacerttypes.Ricevuta ricevutaDatiCert = ofPostacertTypes.createRicevuta();
		ricevutaDatiCert.setTipo("completa");

		datiCert.setIntestazione(intestazioneDatiCert);
		datiCert.setDati(datiDatiCert);
		// DATICERT -> END

		richiestaElabMessaggioPosta.setIdMessaggio("987654321");
		richiestaElabMessaggioPosta.setIdDocumento(UUID.randomUUID().toString());
		richiestaElabMessaggioPosta.setMessaggio(messaggio);
		richiestaElabMessaggioPosta.setDatiCert(datiCert);
		richiestaElabMessaggioPosta.setDatiSegnatura(segnatura);
		
		ProtValidazioneMessaggioType validazioneMessaggio = ofTypes.createProtValidazioneMessaggioType();
		ProtValidazioneSegnaturaType validazioneSegnatura = ofTypes.createProtValidazioneSegnaturaType();
		validazioneSegnatura.setEsitoValidazione("SEGNATURA_VALIDA");
		validazioneMessaggio.setValidazioneSegnatura(validazioneSegnatura);
		richiestaElabMessaggioPosta.setValidazioneMessaggio(validazioneMessaggio);
	
		Long idRichiesta = postaNpsSrv.accodaMessaggioPosta("C4D3F503-66FD-7E43-9F69-0464D15EE673", richiestaElabMessaggioPosta, -1L);
		LOGGER.info("-------> ITEM INSERITO ==========> ID CODA: " + idRichiesta);
	}
	
	/**
	 * Test elaborazione messaggio.
	 */
	@Test
	public void elaboraMessaggio() {
		Long idAoo = 25L;
		RichiestaElabMessaggioPostaNpsDTO richiestaDaElaborare = postaNpsSrv.recuperaRichiestaElabDaLavorare(idAoo);
		
		if (richiestaDaElaborare != null) {
			postaNpsSrv.elaboraMessaggioPostaNpsInIngresso(richiestaDaElaborare);
		}
	}
	
	/**
	 * Gestisce la chiusura delle  risorse dopo il test.
	 * @throws SQLException
	 */
	@After
	public void closeResource() throws SQLException {
		if (con != null) {
			con.close();
		}
	}
}
