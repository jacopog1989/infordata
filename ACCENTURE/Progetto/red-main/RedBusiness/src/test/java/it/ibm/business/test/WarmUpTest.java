package it.ibm.business.test;

import org.junit.Before;

/**
 * Classe di test di warmup.
 */
public class WarmUpTest extends AbstractTest {

	/**
	 * Esegue azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}
	
}
