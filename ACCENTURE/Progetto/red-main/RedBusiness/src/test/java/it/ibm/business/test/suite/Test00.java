package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ColoreNotaEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Test 00.
 */
public class Test00 extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test00.class);
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test creazione documento in ingresso senza classificazione.
	 */
	@Test
	public final void creaDocIngressoCompetenzaConoscenzaSenzaClassificazione() {
		UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		Boolean daNonProtocollare = false;

		Integer idMezzoRicezione = ID_MEZZO_RICEZIONE_POSTA_ELET_DA_PEC_PER_ELETTRONICI;

		List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneCompetenzaUfficio(ID_IGF_UFFICIO_I, DESC_IGF_UFFICIO_I));

		assegnazioni
				.add(createAssegnazioneConoscenzaUtente(ID_IGF_UFFICIO_III, ID_GIANCARLO_ROSSI, "GIANCARLO", "ROSSI"));
		String indiceFascicoloProcedimentale = null;

		String oggetto = createObj();

		Boolean riservato = false;
		List<AllegatoDTO> allegati = null;
		EsitoSalvaDocumentoDTO esito = createDocIngresso(allegati, oggetto, utente, ID_CONTATTO_IRENE,
				FormatoDocumentoEnum.ELETTRONICO, ID_TIPO_DOC_GENERICO_RGS_ENTRATA, ID_TIPO_PROC_GENERICO_RGS_ENTRATA,
				assegnazioni, daNonProtocollare, idMezzoRicezione, indiceFascicoloProcedimentale, riservato);

		dumpCreazioneDocumento(esito);

	}

	/**
	 * Test creazione documento uscita iter automatico.
	 */
	@Test
	public final void creaDocUscitaIterAutomatico() {
		UtenteDTO utenteCreatore = getUtente(USERNAME_MAZZOTTA);

		List<DestinatarioRedDTO> destinatari = new ArrayList<>();

		DestinatarioRedDTO destRossi = createDestinatarioInterno(ID_IGF_UFFICIO_III, ID_GIANCARLO_ROSSI,
				ID_CONTATTO_GIANCARLO_ROSSI, ModalitaDestinatarioEnum.TO);
		destinatari.add(destRossi);

		DestinatarioRedDTO destUno = createDestinatarioEsterno(MezzoSpedizioneEnum.ELETTRONICO, ID_CONTATTO_UNO,
				ModalitaDestinatarioEnum.CC);
		destinatari.add(destUno);

		String indiceFascicoloProcedimentale = "A";
		Boolean riservato = false;

		String oggetto = createObj();
		List<AllegatoDTO> allegati = null;
		EsitoSalvaDocumentoDTO esito = createDocUscita(allegati, false, oggetto, utenteCreatore,
				ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, null, destinatari,
				indiceFascicoloProcedimentale, MomentoProtocollazioneEnum.PROTOCOLLAZIONE_NON_STANDARD,
				FIRMA_DIRIGENTE_RGS, riservato, null);

		dumpCreazioneDocumento(esito);
	}

	/**
	 * Test creazione documento entrata non protocollato.
	 */
	@Test
	public final void creaDocEntrataNonProtocollato() {
		UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		Boolean daNonProtocollare = true;

		Integer idMezzoRicezione = ID_MEZZO_RICEZIONE_POSTA_ELET_DA_PEC_PER_ELETTRONICI;

		List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneCompetenzaUfficio(ID_IGF_UFFICIO_I, DESC_IGF_UFFICIO_I));

		String indiceFascicoloProcedimentale = "A";
		Boolean riservato = false;

		String oggetto = createObj();
		List<AllegatoDTO> allegati = null;
		EsitoSalvaDocumentoDTO esitoCreazione = createDocIngresso(allegati, oggetto, utente, ID_CONTATTO_IRENE,
				FormatoDocumentoEnum.ELETTRONICO, ID_TIPO_DOC_GENERICO_RGS_ENTRATA, ID_TIPO_PROC_GENERICO_RGS_ENTRATA,
				assegnazioni, daNonProtocollare, idMezzoRicezione, indiceFascicoloProcedimentale, riservato);
		checkAssertion(esitoCreazione.isEsitoOk(), "La creazione del documento non è andato a buon fine.");

		dumpCreazioneDocumento(esitoCreazione);

	}

	/**
	 * Test messa agli atti.
	 */
	@Test
	public final void testMessaAtti() {
		// Credo documento come BIAGIO.MAZZOTTA ed assegno per competenza ad IGF
		// UFFICIO1
		UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		Boolean daNonProtocollare = false;

		Integer idMezzoRicezione = ID_MEZZO_RICEZIONE_POSTA_ELET_DA_PEC_PER_ELETTRONICI;

		List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneCompetenzaUfficio(ID_IGF_UFFICIO_I, DESC_IGF_UFFICIO_I));

		String indiceFascicoloProcedimentale = "A";
		Boolean riservato = false;

		String oggetto = createObj();
		List<AllegatoDTO> allegati = null;
		EsitoSalvaDocumentoDTO esitoCreazione = createDocIngresso(allegati, oggetto, utente, ID_CONTATTO_IRENE,
				FormatoDocumentoEnum.ELETTRONICO, ID_TIPO_DOC_GENERICO_RGS_ENTRATA, ID_TIPO_PROC_GENERICO_RGS_ENTRATA,
				assegnazioni, daNonProtocollare, idMezzoRicezione, indiceFascicoloProcedimentale, riservato);
		checkAssertion(esitoCreazione.isEsitoOk(), "La creazione del documento non è andato a buon fine.");

		dumpCreazioneDocumento(esitoCreazione);

		String documentTitle = esitoCreazione.getDocumentTitle();
		// ANDREANI, uno dei corrieri di IGF UFFICIO1, assegna il documento ad ANDREANI
		EsitoOperazioneDTO esitoAssegnazione = assegnaDaCorriere(USERNAME_ANDREANI, documentTitle, USERNAME_ANDREANI);
		checkAssertion(esitoAssegnazione.getFlagEsito(), "L'assegnazione del corriere non è andata a buon fine");

		// ANDREANI lo mette agli atti
		EsitoOperazioneDTO esitoMessaAtti = messaAgliAtti(USERNAME_ANDREANI, documentTitle);
		esitoMessaAtti.getWobNumber();

		EsitoOperazioneDTO esitoRiattivazione = riattiva(USERNAME_ANDREANI, documentTitle);
		checkAssertion(esitoRiattivazione.getFlagEsito(), "La riattivazione non è andata a buon fine");

		String wobRiattivato = getWF(USERNAME_ANDREANI, DocumentQueueEnum.DA_LAVORARE, documentTitle)
				.getWorkObjectNumber();

		EsitoOperazioneDTO esitoSetNota = setNota(USERNAME_ANDREANI, ColoreNotaEnum.ROSSO, wobRiattivato, "Una nota");
		checkAssertion(esitoSetNota.getFlagEsito(), "L'inserimento della nota non è andata a buon fine");

		LOGGER.info(documentTitle);
		LOGGER.info(String.valueOf(getNumDoc(USERNAME_MAZZOTTA, documentTitle)));
	}

	/*
	 * 
	 * INVIA_UFFICIO_PROPONENTE(23, ExecutionTypeEnum.SINGLE,
	 * SourceTypeEnum.FILENET, "Invia a Ufficio proponente",
	 * TaskTypeEnum.HUMAN_TASK, "pnlUfficioProponente",
	 * "Invia a Ufficio Proponente"), INVIO_IN_FIRMA(40,"Invio in firma",
	 * TaskTypeEnum.HUMAN_TASK, "", "Invio in Firma"),
	 * INVIO_IN_SIGLA(41,"Invio in sigla", TaskTypeEnum.HUMAN_TASK, "",
	 * "Invio in Sigla"), PROSEGUI(57,"Prosegui", TaskTypeEnum.SERVICE_TASK,
	 * "prosegui", "Prosegui"), SIGLA(7,"Sigla", TaskTypeEnum.HUMAN_TASK, "",
	 * "Sigla"), //possibile RIuso mobile OperationDocumentSRV.sigla
	 * CHIUDI(15,"Chiudi", TaskTypeEnum.SERVICE_TASK, "chiudi", "Chiudi"), //
	 * ChiudiSRV.public EsitoOperazioneDTO chiudi(UtenteDTO utente, String
	 * wobNumber) FIRMATO(25,"Firmato", TaskTypeEnum.SERVICE_TASK, "firmato",
	 * "Firmato"), FIRMA_AUTOGRAFA(17, ExecutionTypeEnum.MULTI, SourceTypeEnum.APP,
	 * "firma autografa", TaskTypeEnum.BEAN_TASK, "firmaAutografa",
	 * "Firma Autografa"),
	 */

}
