package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.RicercaPerBusinessEntityEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Test 05.
 */
public class Test05 extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test05.class);
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test.
	 */
	@Test
	public final void test() {
		// Creazoine documento
		UtenteDTO utente = getUtente(USERNAME_NUNZI);
		Boolean daNonProtocollare = true;
		String indiceFascicoloProcedimentale = null;

		List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneCompetenzaUfficio(ID_IGF, DESC_IGF));
		assegnazioni.add(createAssegnazioneConoscenzaUtente(ID_IGF, ID_GIANFANCO_TANZI, "GIANFRANCO", "TANZI"));

		String oggetto = createObj();

		Boolean riservato = true;
		Integer idMezzoRicezione = ID_MEZZO_RICEZIONE_POSTA_ELET_DA_PEC_PER_ELETTRONICI;
		List<AllegatoDTO> allegati = null;
		EsitoSalvaDocumentoDTO esito = createDocIngresso(allegati, oggetto, utente, ID_CONTATTO_IRENE,
				FormatoDocumentoEnum.ELETTRONICO, ID_TIPO_DOC_GENERICO_RGS_ENTRATA, ID_TIPO_PROC_GENERICO_RGS_ENTRATA,
				assegnazioni, daNonProtocollare, idMezzoRicezione, indiceFascicoloProcedimentale, riservato);

		checkAssertion(esito.isEsitoOk(), "Errore in fase di creazione del documento.");

		assegnaDaCorriere(USERNAME_NUNZI, esito.getDocumentTitle(), USERNAME_CORBO);

		Collection<MasterDocumentRedDTO> docs = ricerca(esito.getDocumentTitle(), null, RicercaGenericaTypeEnum.ESATTA,
				RicercaPerBusinessEntityEnum.DOCUMENTI, USERNAME_ANDREANI);
		checkAssertion(docs.isEmpty(), "Il documento no ndeve essere trovato essendo riservato");
		VWWorkObject wob = getWF(USERNAME_TANZI, DocumentQueueEnum.DA_LAVORARE, esito.getDocumentTitle());
		EsitoOperazioneDTO esitoMessaConoscenza = mettiInConoscenza(wob.getWorkObjectNumber(), ID_IGF_UFFICIO_I,
				ID_ADRIANA_ANDREANI, USERNAME_TANZI);
		checkAssertion(esitoMessaConoscenza.getFlagEsito(), "Messa in conoscenza fallita");
		Collection<MasterDocumentRedDTO> docsAfter = ricerca(esito.getDocumentTitle(), null,
				RicercaGenericaTypeEnum.ESATTA, RicercaPerBusinessEntityEnum.DOCUMENTI, USERNAME_ANDREANI);
		checkAssertion(!docsAfter.isEmpty(), "Il documento deve essere trovato essendo di nostra conoscenza");

		EsitoOperazioneDTO esitoPresaVisione = presaVisione(USERNAME_ANDREANI, esito.getDocumentTitle());
		checkAssertion(esitoPresaVisione.getFlagEsito(), "La presa visione è andata in errore.");

		LOGGER.info("DOCUMENT TITLE : " + esito.getDocumentTitle());
		LOGGER.info("OGGETTO : " + oggetto);
		LOGGER.info("NUM DOC : " + getNumDoc(USERNAME_NUNZI, esito.getDocumentTitle()));
	}

}
