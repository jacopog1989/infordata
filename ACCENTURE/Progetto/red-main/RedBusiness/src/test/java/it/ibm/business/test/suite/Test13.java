package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Test 13.
 */
public class Test13 extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test13.class);
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test 13.
	 */
	@Test
	public final void test() {

		UtenteDTO utente = getUtente(USERNAME_NUNZI);
		String oggetto = createObj();
		String indiceFascicoloProcedimentale = "A";
		Boolean bRiservato = false;
		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(createDestinatarioEsterno(MezzoSpedizioneEnum.ELETTRONICO, ID_CONTATTO_GIANCARLO_ROSSI,
				ModalitaDestinatarioEnum.TO));
		destinatari.add(createDestinatarioEsterno(MezzoSpedizioneEnum.CARTACEO, ID_CONTATTO_GIANCARLO_ROSSI,
				ModalitaDestinatarioEnum.CC));

		List<AssegnazioneDTO> assegnazioni = null;
		List<AllegatoDTO> allegati = null;
		EsitoSalvaDocumentoDTO esitoUscita = createDocUscita(allegati, false, oggetto, utente,
				ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, assegnazioni, destinatari,
				indiceFascicoloProcedimentale, MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD,
				FIRMA_DIRIGENTE_RGS, bRiservato, null);
		checkAssertion(esitoUscita.isEsitoOk(), "Errore in fase di creazione documento in uscita.");

		String dt = esitoUscita.getDocumentTitle();

		EsitoOperazioneDTO esitoRifiuto1 = rifiutaDaLIbroFirma(USERNAME_TANZI, dt);
		checkAssertion(esitoRifiuto1.isEsito(), "Errore in fase di rifiuto.");

		waitSeconds(30);

		EsitoOperazioneDTO esitoReinvia = reinvia(USERNAME_NUNZI, dt);
		checkAssertion(esitoReinvia.isEsito(), "Errore in fase di reinvio.");

		EsitoOperazioneDTO esitoModificaIter = modificaIterManuale(USERNAME_TANZI, dt, TipoAssegnazioneEnum.SIGLA,
				false, ID_IGF.longValue(), ID_ROBERTO_NUNZI.longValue());
		checkAssertion(esitoModificaIter.isEsito(), "Errore in fase di modifica iter.");

		EsitoOperazioneDTO esitoRifiuto2 = rifiutaDaLIbroFirma(USERNAME_NUNZI, dt);
		checkAssertion(esitoRifiuto2.isEsito(), "Errore in fase di rifiuto.");

		LOGGER.info("DOCUMENT TITLE : " + dt);
		LOGGER.info("OGGETTO : " + oggetto);
		LOGGER.info("NUM DOC : " + getNumDoc(USERNAME_NUNZI, dt));
	}

}
