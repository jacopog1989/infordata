package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Test 12.
 */
public class Test12 extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test12.class);
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}
	
	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test 12.
	 */
	@Test
	public final void test() {

		//PIETROBONO CREA UN DOCUMENTO IN USCITA
		UtenteDTO utente = getUtente(USERNAME_TANZI);
		String oggetto = createObj();
		String indiceFascicoloProcedimentale = "A";

		Boolean bRiservato = false;

		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(createDestinatarioEsterno(MezzoSpedizioneEnum.CARTACEO, ID_CONTATTO_GIANCARLO_ROSSI, ModalitaDestinatarioEnum.TO));

		List<AssegnazioneDTO> assegnazioni = null;
		List<AllegatoDTO> allegati = null;
		EsitoSalvaDocumentoDTO esitoUscita = createDocUscita(allegati, false, oggetto, utente, 
				ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, 
				assegnazioni, destinatari, 
				indiceFascicoloProcedimentale, MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD, 
				FIRMA_RAGIONIERE, bRiservato, null);
		
		checkAssertion(esitoUscita.isEsitoOk(), "Errore in fase di creazione documento in uscita.");

		String dt = esitoUscita.getDocumentTitle();

		EsitoOperazioneDTO esitoSiglaInvia1 = siglaInvia(USERNAME_TANZI, dt);
		checkAssertion(esitoSiglaInvia1.isEsito(), "Errore in fase di sigla invia.");

		waitSeconds(30);
		
		EsitoOperazioneDTO esitoProcediNunzi = procedi(USERNAME_TANZI, dt);
		checkAssertion(esitoProcediNunzi.isEsito(), "Errore in fase di procedi.");

		EsitoOperazioneDTO esitoSiglaInvia2 = siglaInvia(USERNAME_TANZI, dt);
		checkAssertion(esitoSiglaInvia2.isEsito(), "Errore in fase di sigla invia.");

		waitSeconds(30);

		EsitoOperazioneDTO esitoInviaCoordinamento = inviaUfficioCoordinamento(USERNAME_TUCCI, dt);
		checkAssertion(esitoInviaCoordinamento.isEsito(), "Errore in fase di invio a coordinamento.");
		
		EsitoOperazioneDTO esitoFirmatoSpedito = firmatoSpedito(USERNAME_SMERIGLIO, dt);
		checkAssertion(esitoFirmatoSpedito.isEsito(), "Errore in fase di firmato e spedito.");
		
		waitSeconds(30);
		
		EsitoOperazioneDTO esitoChiudi = chiudi(USERNAME_SMERIGLIO, dt);
		checkAssertion(esitoChiudi.isEsito(), "Errore in fase di chiudi.");

		LOGGER.info("DOCUMENT TITLE : " + dt);
		LOGGER.info("OGGETTO : " + oggetto);
		LOGGER.info("NUM DOC : " + getNumDoc(USERNAME_NUNZI, dt));

	}

}
