package it.ibm.business.test.asign;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.SignStrategyEnum;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * Classe di test step di avviamento processi - firma asincrona.
 */
public class Test07StepAvviamentoProcessi extends AbstractAsignTest {

	/**
	 * Messaggio generico in fase di assertion su firma digitale non rispettata.
	 */
	private static final String MSG_ASSERTION_FIRMA_DIGITALE = "non deve esserci firma digitale";

	/**
	 * Esegue le operazioni di data preparation prima dell'esecuzione del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue le azioni dopo l'esecuzione del test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	private Boolean canFirmaDigitale(String username, String dt) {
		UtenteDTO utente = getUtente(username);
		FilenetPEHelper fpeh = new FilenetPEHelper(utente.getFcDTO());
		VWWorkObject wob = fpeh.getWorkflowPrincipale(dt, utente.getFcDTO().getIdClientAoo());
		String[] responses =  wob.getStepResponses();
		Boolean bFound = false;
		if (responses!=null) {
			for (String response:responses) {
				if (ResponsesRedEnum.FIRMA_DIGITALE.getResponse().equalsIgnoreCase(response)) {
					bFound = true;
					break;
				}
			}
		}
		return bFound;
	}

	/**
	 * Esegue lo step di avviamento processi.
	 */
	@Test
	public final void run() {
		ASignItemDTO itemPre = accoda(USERNAME_RGS, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, SignStrategyEnum.ASYNC_STRATEGY_B, null);
		run(StepEnum.PROTOCOLLAZIONE, itemPre.getId());
		run(StepEnum.REG_AUX_PROTOCOLLAZIONE, itemPre.getId());
		run(StepEnum.STAMPIGLIATURE, itemPre.getId());
		run(StepEnum.AGGIORNAMENTO_METADATI, itemPre.getId());
		run(StepEnum.GESTIONE_ALLACCI, itemPre.getId());
		
		checkAssertion(canFirmaDigitale(USERNAME_RGS, itemPre.getDocumentTitle()), "deve esserci firma digitale");
		run(StepEnum.AVANZAMENTO_PROCESSI, itemPre.getId());
		checkAssertion(!canFirmaDigitale(USERNAME_RGS, itemPre.getDocumentTitle()), MSG_ASSERTION_FIRMA_DIGITALE);
	}

	/**
	 * Simula un crash durante l'esecuzione dello step di avviamento processi.
	 */
	@Test
	public final void crash() {
		ASignItemDTO itemPre = accoda(USERNAME_RGS, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, SignStrategyEnum.ASYNC_STRATEGY_B, null);
		run(StepEnum.PROTOCOLLAZIONE, itemPre.getId());
		run(StepEnum.REG_AUX_PROTOCOLLAZIONE, itemPre.getId());
		run(StepEnum.STAMPIGLIATURE, itemPre.getId());
		run(StepEnum.AGGIORNAMENTO_METADATI, itemPre.getId());
		run(StepEnum.GESTIONE_ALLACCI, itemPre.getId());

		checkAssertion(canFirmaDigitale(USERNAME_RGS, itemPre.getDocumentTitle()), "deve esserci firma digitale");
		crash(StepEnum.AVANZAMENTO_PROCESSI, itemPre.getId());
		checkAssertion(!canFirmaDigitale(USERNAME_RGS, itemPre.getDocumentTitle()), MSG_ASSERTION_FIRMA_DIGITALE);
		run(StepEnum.AVANZAMENTO_PROCESSI, itemPre.getId());
		checkAssertion(!canFirmaDigitale(USERNAME_RGS, itemPre.getDocumentTitle()), MSG_ASSERTION_FIRMA_DIGITALE);
	}

}
