package it.ibm.business.test.suite;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.filenet.api.core.Document;
import com.google.common.net.MediaType;

import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.MapRedKey;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.facade.IREDServiceFacadeSRV;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.webservice.model.redservice.header.v1.CredenzialiUtenteRed;
import it.ibm.red.webservice.model.redservice.header.v1.ObjectFactory;
import it.ibm.red.webservice.model.redservice.messages.v1.GetDocumentiFascicoloResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.GetVersioneDocumentoResponse;
import it.ibm.red.webservice.model.redservice.types.v1.ClasseDocumentaleRed;
import it.ibm.red.webservice.model.redservice.types.v1.DocumentoRed;
import it.ibm.red.webservice.model.redservice.types.v1.IdentificativoUnitaDocumentaleFEPATypeRed;
import it.ibm.red.webservice.model.redservice.types.v1.IdentificativoUnitaDocumentaleTypeRed;
import it.ibm.red.webservice.model.redservice.types.v1.MapRed;
import it.ibm.red.webservice.model.redservice.types.v1.MapRed.Elemento;
import it.ibm.red.webservice.model.redservice.types.v1.MetadatoRed;
import it.ibm.red.webservice.model.redservice.types.v1.RispostaDocRed;
import it.ibm.red.webservice.model.redservice.types.v1.RispostaElencoVersioniDocRed;

/**
 * Classe di test REDService.
 */
public class REDServiceTest extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(REDServiceTest.class);

	/**
	 * Messaggio di comunicazione document title fattura non valorizzato.
	 */
	private static final String DOC_TITLE_NULL = "Il document title della fattura non può essere null.";
	
	/**
	 * Nome file di test.
	 */
	private static final String FILE_TEST_NAME = "fileTest.pdf";

	/**
	 * Service.
	 */
	private IREDServiceFacadeSRV redSRV;

	/**
	 * Service.
	 */
	private IUtenteFacadeSRV utenteSRV;

	/**
	 * Decreto test.
	 */
	private String decretoTest;

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		redSRV = ApplicationContextProvider.getApplicationContext().getBean(IREDServiceFacadeSRV.class);
		utenteSRV = ApplicationContextProvider.getApplicationContext().getBean(IUtenteFacadeSRV.class);
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Creazione fattura per assegnarla su RED ad un altro utente e consentirgli la
	 * lavorazione.
	 */
	@Test
	public final void testCreazioneFatturaAssegnata() {

		final Integer idAoo = ID_AOO_RGS;
		final Integer idUfficioMittenteFattura = ID_IGICS_UFFICIO_I;
		final String idUtenteMittenteFattura = ID_PAOLO_POGGESI + "";
		final Integer idUfficioDestinatarioFattura = ID_IGICS_UFFICIO_I;
		final String idUtenteDestinatarioFattura = ID_EM_ZAZZA + "";
		final String idFascicoloFEPA = UUID.randomUUID().toString();
		final String idDocumentoFEPA = UUID.randomUUID().toString();

		// Il mittente si autoassegna la fattura.
		String dtFattura = createFattura(idFascicoloFEPA, idDocumentoFEPA, idAoo, idUfficioMittenteFattura, idUtenteMittenteFattura);
		checkAssertion(!StringUtils.isNullOrEmpty(dtFattura), DOC_TITLE_NULL);

		LOGGER.info("DOCUMENT TITLE: " + dtFattura);

		// Colui che ha in carico la fattura (ovvero il mittente) l'assegna ad un altro
		// utente.
		dtFattura = assegnaFattura(idFascicoloFEPA, idDocumentoFEPA, idAoo, idUfficioMittenteFattura, idUtenteMittenteFattura, idUfficioDestinatarioFattura,
				idUtenteDestinatarioFattura);

		checkAssertion(!StringUtils.isNullOrEmpty(dtFattura), DOC_TITLE_NULL);

		LOGGER.info("DOCUMENT TITLE: " + dtFattura);
	}

	/**
	 * Suppongo che l'utente voglia continuare la lavorazione da un sistema esterno
	 * su RED. Essendo generati random idFascicolo e idDocumento FEPA non potremmo
	 * riconciliare con FEPA, ma in questo modo siamo indipendenti dal sistema
	 * stesso (in pratica con queste fatture non possiamo integrarci con i sistemi
	 * esterni).
	 * 
	 * @param idFascicoloFEPA identificativo fascicolo
	 * @param idDocumentoFEPA identificativo documento
	 * @param idAoo           aoo di riferimento
	 * @param idUfficio       ufficio di riferimento
	 * @param idUtente        utente di riferimento
	 * @return document title fattura
	 */
	private String createFattura(final String idFascicoloFEPA, final String idDocumentoFEPA, final Integer idAoo, final Integer idUfficio, final String idUtente) {
		// Assegni la fattura a te stesso in fase di creazione.
		return assegnaFattura(idFascicoloFEPA, idDocumentoFEPA, idAoo, idUfficio, idUtente, idUfficio, idUtente);
	}

	private String assegnaFattura(final String idFascicoloFEPA, final String idDocumentoFEPA, final Integer idAoo, final Integer idUfficioMittenteFattura,
			final String idUtenteMittenteFattura, final Integer idUfficioDestinatarioFattura, final String idUtenteDestinatarioFattura) {

		final String oggettoDocumentoRED = createObj();

		final ObjectFactory of = new ObjectFactory();

		final CredenzialiUtenteRed credenziali = new CredenzialiUtenteRed();
		credenziali.setIdAoo(idAoo);
		credenziali.setIdGruppo(idUfficioMittenteFattura);
		credenziali.setIdUtente(of.createCredenzialiUtenteRedIdUtente(idUtenteMittenteFattura));

		final MapRed mapRed = new MapRed();

		final Elemento eUtenteCedente = new Elemento();
		eUtenteCedente.setKey(MapRedKey.UTENTE_CEDENTE_KEY);
		eUtenteCedente.setValore(idUtenteMittenteFattura);
		mapRed.getElemento().add(eUtenteCedente);

		final Elemento eDestinatario = new Elemento();
		eDestinatario.setKey(MapRedKey.UTENTE_DESTINATARIO_KEY);
		eDestinatario.setValore(idUtenteDestinatarioFattura);
		mapRed.getElemento().add(eDestinatario);

		final Elemento eUfficioCedente = new Elemento();
		eUfficioCedente.setKey(MapRedKey.UFFICIO_DESTINATARIO_KEY);
		eUfficioCedente.setValore(idUfficioDestinatarioFattura + "");
		mapRed.getElemento().add(eUfficioCedente);

		final String inFileName = FILE_TEST_NAME;
		final byte[] inContent = FileUtils.getFileFromInternalResources(FILE_TEST_NAME);
		final String inMimeType = MediaType.PDF.toString();

		return redSRV.assegnaFascicoloFatturaWithoutFepa(credenziali, idFascicoloFEPA, mapRed, idDocumentoFEPA, oggettoDocumentoRED, inFileName, inContent, inMimeType);
	}

	/**
	 * Creazione decreto.
	 */
	@Test
	public final void testCreazioneDecreto() {
		final String dtDecreto = creaDecreto(ID_EM_ZAZZA);
		this.decretoTest = dtDecreto;
		LOGGER.info("DOCUMENT TITLE DECRETO: " + dtDecreto);
	}

	private String creaDecreto(final Integer idDestinatario) {
		final String idFascicoloFEPA = UUID.randomUUID().toString();
		final String idDocumentoFEPA = UUID.randomUUID().toString();
		final String idFascicoloFEPADD = UUID.randomUUID().toString();
		// Lattanzio decide di continuare la lavorazione della fattura su RED.
		final String dtFattura = createFattura(idFascicoloFEPA, idDocumentoFEPA, ID_AOO_RGS, ID_IGICS_UFFICIO_I, idDestinatario + "");
		checkAssertion(!StringUtils.isNullOrEmpty(dtFattura), DOC_TITLE_NULL);

		final CredenzialiUtenteRed credenziali = new CredenzialiUtenteRed();
		credenziali.setIdAoo(ID_AOO_RGS);
		credenziali.setIdGruppo(ID_IGICS_UFFICIO_I);
		credenziali.setIdUtente(new ObjectFactory().createCredenzialiUtenteRedIdUtente(idDestinatario + ""));

		final String numeroProtocolloDecreto = "1";
		final Date dataProtocollo = new Date();

		// Le fatture saranno indicizzate tramite gli di fascicolo fepa associati.
		final it.ibm.red.webservice.model.redservice.types.v1.ObjectFactory of = new it.ibm.red.webservice.model.redservice.types.v1.ObjectFactory();
		final IdentificativoUnitaDocumentaleFEPATypeRed fascicolo = new IdentificativoUnitaDocumentaleFEPATypeRed();
		final IdentificativoUnitaDocumentaleTypeRed value = of.createIdentificativoUnitaDocumentaleTypeRed();
		value.setID(idFascicoloFEPA);
		fascicolo.setID(value);

		final List<IdentificativoUnitaDocumentaleFEPATypeRed> fascicoli = new ArrayList<>();
		fascicoli.add(fascicolo);

		final String inFileName = "fileTest.docx";
		final byte[] inContent = FileUtils.getFileFromInternalResources("fileTest.docx");
		final String inMimeType = Constants.ContentType.DOCX;

		final DocumentoRed documento = new DocumentoRed();
		documento.setDataHandler(of.createDocumentoRedDataHandler(inContent));
		final ClasseDocumentaleRed cdr = of.createClasseDocumentaleRed();

		final MetadatoRed eOggettoProtocollo = new MetadatoRed();
		eOggettoProtocollo.setKey(of.createMetadatoRedKey(MapRedKey.OGGETTO_PROTOCOLLO_FEPA));
		eOggettoProtocollo.setValue(of.createMetadatoRedValue(createObj()));
		cdr.getMetadato().add(eOggettoProtocollo);

		final MetadatoRed eNomeFile = new MetadatoRed();
		eNomeFile.setKey(of.createMetadatoRedKey(MapRedKey.NOME_FILE_FEPA));
		eNomeFile.setValue(of.createMetadatoRedValue(inFileName));
		cdr.getMetadato().add(eNomeFile);

		final MetadatoRed eNumeroProtocollo = new MetadatoRed();
		eNumeroProtocollo.setKey(of.createMetadatoRedKey(MapRedKey.NUM_PROTOCOLLO_FEPA));
		eNumeroProtocollo.setValue(of.createMetadatoRedValue(numeroProtocolloDecreto));
		cdr.getMetadato().add(eNumeroProtocollo);

		final MetadatoRed eDataProtocollo = new MetadatoRed();
		eDataProtocollo.setKey(of.createMetadatoRedKey(MapRedKey.DATA_PROTOCOLLO_FEPA));
		eDataProtocollo.setValue(of.createMetadatoRedValue(DateUtils.dateToString(dataProtocollo, DateUtils.DD_MM_YYYY)));
		cdr.getMetadato().add(eDataProtocollo);

		documento.setIdFascicolo(of.createDocumentoRedIdFascicolo(idFascicoloFEPADD));

		final MapRed metadati = new MapRed();

		documento.setContentType(of.createDocumentoRedContentType(inMimeType));

		documento.setClasseDocumentale(of.createDocumentoRedClasseDocumentale(cdr));

		final String dtDecreto = redSRV.inserimentoFascicoloDecreto(credenziali, metadati, documento, fascicoli);
		checkAssertion(!StringUtils.isNullOrEmpty(dtDecreto), "Il document title del decreto non può essere null.");
		return dtDecreto;
	}

	/**
	 * Test checkout e Undo operazioni.
	 */
	@Test
	public final void testCheckoutEUndo() {
		redSRV.checkOut(40064 + "", ID_GM_LATTANZIO.toString());
		final UtenteDTO utente = utenteSRV.getById(ID_GM_LATTANZIO.longValue());
		final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
		Document d = fceh.getDocumentByDTandAOO(40064 + "", utente.getIdAoo());
		checkAssertion(d.get_IsReserved(), "Il documento non è in checkout");
		redSRV.undoCheckOut(40064 + "", ID_GM_LATTANZIO.toString());
		d = fceh.getDocumentByDTandAOO(40064 + "", utente.getIdAoo());
		checkAssertion(!d.get_IsReserved(), "Il documento è ancora in checkout");
	}

	/**
	 * Test check in.
	 * 
	 * @throws IOException
	 */
	@Test
	public final void testCheckIn() throws IOException {
		redSRV.checkOut(40064 + "", ID_GM_LATTANZIO.toString());
		final UtenteDTO utente = utenteSRV.getById(ID_GM_LATTANZIO.longValue());
		final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
		final Document d = fceh.getDocumentByDTandAOO(40064 + "", utente.getIdAoo());
		checkAssertion(d.get_IsReserved(), "Il documento non è in checkout");
		final int versione = d.get_MajorVersionNumber();
		final byte[] content = IOUtils.toByteArray(FilenetCEHelper.getDocumentContentAsInputStream(d));
		final RispostaDocRed response = redSRV.checkIn(40064 + "", ID_GM_LATTANZIO.toString(), null, null, content);
		checkAssertion(response.getIdVersione() > versione, "Il checkIn è fallito");
	}

	/**
	 * Test recupero versione documento.
	 */
	@Test
	public final void testGetVersioneDoc() {
		final GetVersioneDocumentoResponse response = redSRV.getVersioneDocumento(ID_GM_LATTANZIO.toString(), 40064 + "", 1);
		checkAssertion(response.getReturn().getValue().getDescErrore() == null, "errore GetVersioneDoc");
	}

	/**
	 * Test recupero elenco versioni documento.
	 */
	@Test
	public final void testElencoVersioniDoc() {
		final RispostaElencoVersioniDocRed response = redSRV.elencoVersioniDocumento(ID_GM_LATTANZIO.toString(), 40064 + "");
		checkAssertion(response.getDescErrore() == null, "errore GetVersioneDoc");
	}

	/**
	 * Test recupero documenti dal fascicolo.
	 */
	@Test
	public final void testGetDocumentiFascicolo() {
		final GetDocumentiFascicoloResponse response = redSRV.getDocumentiFascicolo(ID_GM_LATTANZIO.toString(), "8930");
		checkAssertion(response.getDocumenti().get(0).getIdDocumento().getValue().contains(2605 + ""), "errore GetDocumentiFascicolo");
	}

	/**
	 * Test di modifica dei metadati del documento.
	 */
	@Test
	public final void testModificaMetadati() {
		final it.ibm.red.webservice.model.redservice.types.v1.ObjectFactory of = new it.ibm.red.webservice.model.redservice.types.v1.ObjectFactory();
		final List<MetadatoRed> metadati = new ArrayList<>();
		final MetadatoRed metadato = new MetadatoRed();
		metadato.setKey(of.createMetadatoRedKey("note"));
		metadato.setValue(of.createMetadatoRedValue("test1"));
		metadati.add(metadato);
		redSRV.modificaMetadati(ID_GM_LATTANZIO.toString(), "40064", metadati);
		final UtenteDTO utente = utenteSRV.getById(ID_GM_LATTANZIO.longValue());
		final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
		final Document d = fceh.getDocumentByDTandAOO(40064 + "", utente.getIdAoo());
		checkAssertion("test1".equals(d.getProperties().getStringValue("note")), "Il documento non è stato modificato");
	}

	/**
	 * Test di modifica fascicolo decreto.
	 */
	@Test
	public final void testModificaFascicoloDecreto() {
		final Integer idAoo = ID_AOO_RGS;
		final Integer idUfficioMittenteFattura = ID_IGICS_UFFICIO_I;
		final String idUtenteMittenteFattura = ID_GM_LATTANZIO + "";
		final String idFascicoloFEPA = UUID.randomUUID().toString();
		final String idDocumentoFEPA = UUID.randomUUID().toString();

		// Il mittente si autoassegna la fattura.
		final String dtFattura = createFattura(idFascicoloFEPA, idDocumentoFEPA, idAoo, idUfficioMittenteFattura, idUtenteMittenteFattura);
		checkAssertion(!StringUtils.isNullOrEmpty(dtFattura), DOC_TITLE_NULL);

		final List<String> fattureDaRimuovereList = new ArrayList<>();
		fattureDaRimuovereList.add("b25d63b3-1e89-468d-b131-e44ec3287c47");

		final List<String> fattureDaAggiungereList = new ArrayList<>();
		fattureDaAggiungereList.add(idFascicoloFEPA);

		final byte[] inContent = FileUtils.getFileFromInternalResources(FILE_TEST_NAME);
		final String inMimeType = MediaType.PDF.toString();

		redSRV.modificaDecreto(idUfficioMittenteFattura.toString(), idUtenteMittenteFattura, "41007", fattureDaAggiungereList, fattureDaRimuovereList, inContent, inMimeType);

		final UtenteDTO utente = utenteSRV.getById(ID_GM_LATTANZIO.longValue());
		final FilenetPEHelper fpeh = new FilenetPEHelper(utente.getFcDTO());
		final List<Long> idDecreto = new ArrayList<>();
		idDecreto.add(Long.parseLong("41007"));
		final VWRosterQuery rQueryNSD = fpeh.getDocumentWorkFlowsByIdDocumenti(idDecreto);
		final VWWorkObject wf = (VWWorkObject) rQueryNSD.next();
		final String idFattura = ((String[]) fpeh.getMetadato(wf.getWorkflowNumber(), PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ELENCO_FATTURE_FEPA)))[0];

		checkAssertion(idFattura.equals(idFascicoloFEPA), "Il documento non è stato modificato");
	}

	/**
	 * Test annullamento.
	 */
	@Test
	public final void testAnnullamento() {
		redSRV.annullaDocumento(ID_GM_LATTANZIO.toString(), decretoTest + "", "Test");
		final UtenteDTO utente = utenteSRV.getById(ID_GM_LATTANZIO.longValue());
		final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
		final Document d = fceh.getDocumentByDTandAOO(decretoTest + "", utente.getIdAoo());
		checkAssertion(d.getProperties().getDateTimeValue("dataAnnullamentoDocumento") != null, "Il documento non è stato annullato");
	}
}
