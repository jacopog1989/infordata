package it.ibm.business.test.suite;

import org.junit.After;
import org.junit.Before;

/**
 * Classe di test reportistica.
 */
public class ReportTest extends AbstractTest {

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}
	
	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}
}