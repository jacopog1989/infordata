package it.ibm.business.test.ucb;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.net.MediaType;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.INpsAsyncRequestDAO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.CollectedParametersDTO;
import it.ibm.red.business.dto.DatiFlussoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.MessaggioPostaNpsDTO;
import it.ibm.red.business.dto.MessaggioPostaNpsFlussoDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.NotificaAzioneNpsDTO;
import it.ibm.red.business.dto.NotificaNpsDTO;
import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.RecuperoCodeDTO;
import it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.AUTMessageIDEnum;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.SICOGEMessageIDEnum;
import it.ibm.red.business.enums.StatoElabMessaggioPostaNpsEnum;
import it.ibm.red.business.enums.TipoAllaccioEnum;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.enums.TipoNotificaAzioneNPSEnum;
import it.ibm.red.business.enums.TipoProtocolloEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.dto.DocumentoNpsDTO;
import it.ibm.red.business.nps.dto.PersonaFisicaDTO;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.WebServiceClientProvider;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.IMockSRV;
import it.ibm.red.business.service.pstrategy.AutoProtocolStrategyFactory;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.ContestoProcedurale;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.Identificativo;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.TipoContestoProcedurale;
import it.ibm.red.webservice.model.interfaccianps.dto.ParametersAzioneRispostaAutomatica;
import it.ibm.red.webservice.model.interfaccianps.dto.ParametersElaboraNotificaAzione;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioProtocollazioneFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.EmailIndirizzoEmailType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.EmailMessageType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.OrgAooType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.OrgCasellaEmailType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.WkfDatiContestoProceduraleType;
import it.ibm.red.webservice.model.interfaccianps.npstypesestesi.TIPOLOGIA;

/**
 * Classe di test per i flussi - lato RED.
 */
public class Test06Flussi extends AbstractUCBTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test06Flussi.class.getName());

	/**
	 * Messaggio di errore nell'assegnazione al corriere.
	 */
	private static final String ERROR_ASSEGNAZIONE_CORRIERE_MSG = "L'assegnazione del corriere non è andata a buon fine.";
	
	/**
	 * GUID messaggio.
	 */
	private static final String GUID_MESSAGGIO = "{5226087E-08CF-4F5A-8846-210B2BBA3E79}";
	
	/**
	 * Codice AOO UCB Lavoro.
	 * NB: Non modificare con UCB_LAV. Viene usato per il recupero delle sequence che hanno un nome obsoleto e vengono usate per recupero dati mockati.
	 */
	private static final String CODICE_AOO_UCB_LAV = "ULAV";
	
	/**
	 * Id univoco dell'Area UCB Lavoro.
	 */
	private static final long ID_AOO_UCB_LAV = 34L;
	
	/**
	 * Mail di test.
	 */
	private static final String MAIL_TEST = "a@b.c";

	/**
	 * Messaggio errore protocollazione documento.
	 */
	private static final String ERROR_PROTOCOLLAZIONE_MSG = "La protocollazione del documento non è andata a buon fine.";

	/**
	 * Path.
	 */
	private static final String FILESYSTEM_PATH = "";

	/**
	 * Service per il recupero dei fascicoli.
	 */
	private IFascicoloSRV fascicoloSRV;
	
	/**
	 * Dao per la verifica di esistenza item nella tabella di processamento asincrona di Nps.
	 */
	private INpsAsyncRequestDAO npsDAO;
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloSRV.class);
		npsDAO = ApplicationContextProvider.getApplicationContext().getBean(INpsAsyncRequestDAO.class);
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}
	
	/**
	 * Test SICOGE.
	 */
	@Test
	public final void flussoSICOGETour() {
		final UtenteDTO utente = getUtente(USERNAME_BRIAMONTE);
		final String guidFilenetMessaggio = GUID_MESSAGGIO;
		String dtIngresso;
		RecuperoCodeDTO code;
		final String identificatoreProcesso = java.util.UUID.randomUUID().toString();
		Integer numDocIngresso;
		final String idFascicoloFepa = null; // INSERIRE UN VALORE E CAPIRE IN QUALI STEP DEI SEGUENTI VA PASSATO
		final String tipoFascicoloFepa = null; // INSERIRE UN VALORE E CAPIRE IN QUALI STEP DEI SEGUENTI VA PASSATO

		// STEP1: NPS notifica la creazione di un titolo di pagamento di
		// identificatoreProcesso
		final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> messaggioProtocollaTitolo = createRequest(TipoContestoProceduraleEnum.SICOGE,
				SICOGEMessageIDEnum.SICOGE_PROTOCOLLA_TITOLO.getId(), ID_AOO_UCB_LAV, CODICE_AOO_UCB_LAV, identificatoreProcesso, null, idFascicoloFepa, tipoFascicoloFepa);
		final EsitoSalvaDocumentoDTO esitoProtocollaTitolo = AutoProtocolStrategyFactory.protocol(TipoContestoProceduraleEnum.SICOGE, messaggioProtocollaTitolo,
				guidFilenetMessaggio);

		checkAssertion((esitoProtocollaTitolo.getErrori() == null || esitoProtocollaTitolo.getErrori().isEmpty()), ERROR_PROTOCOLLAZIONE_MSG);

		numDocIngresso = esitoProtocollaTitolo.getNumeroDocumento();
		dtIngresso = esitoProtocollaTitolo.getDocumentTitle();

		// STEP2: Il corriere assegna a BRIAMONTE
		final EsitoOperazioneDTO esitoAssegnazione = assegnaDaCorriere(USERNAME_BRIAMONTE, dtIngresso, USERNAME_BRIAMONTE);
		checkAssertion(esitoAssegnazione.getFlagEsito(), ERROR_ASSEGNAZIONE_CORRIERE_MSG);

		checkStorico(utente, null, dtIngresso, EventTypeEnum.ASSEGNAZIONE_COMPETENZA);
		code = getQueues(utente.getUsername(), dtIngresso, null, CategoriaDocumentoEnum.ENTRATA.getIds()[0]);
		checkAssertionCode(code, DocumentQueueEnum.DA_LAVORARE_UCB, DocumentQueueEnum.ASSEGNATE);

		// STEP3: NPS notifica una integrazione documentale spontanea
		final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> messaggioIntegrazioneSpontanea = createRequest(TipoContestoProceduraleEnum.SICOGE,
				SICOGEMessageIDEnum.SICOGE_DOCUMENTAZIONE_AGGIUNTIVA.getId(), ID_AOO_UCB_LAV, CODICE_AOO_UCB_LAV, identificatoreProcesso, null, idFascicoloFepa, tipoFascicoloFepa);
		final EsitoSalvaDocumentoDTO esitoIntegrazioneSpontanea = AutoProtocolStrategyFactory.protocol(TipoContestoProceduraleEnum.SICOGE, messaggioIntegrazioneSpontanea,
				guidFilenetMessaggio);

		checkAssertion((esitoIntegrazioneSpontanea.getErrori() == null || esitoIntegrazioneSpontanea.getErrori().isEmpty()), ERROR_PROTOCOLLAZIONE_MSG);

		checkStorico(utente, null, dtIngresso, EventTypeEnum.ASSEGNAZIONE_COMPETENZA);
		code = getQueues(utente.getUsername(), dtIngresso, null, CategoriaDocumentoEnum.ENTRATA.getIds()[0]);
		checkAssertionCode(code, DocumentQueueEnum.DA_LAVORARE_UCB, DocumentQueueEnum.ASSEGNATE);

		// STEP4: BRIAMONTE richiede una integrazione dati per il documento in ingresso
		// iniziale di identificatoreProcesso
		richiediIntegrazione(dtIngresso, numDocIngresso, ID_RICHIESTA_INTEGRAZIONE_EX_ART_14BIS_COMMA_2);

		// STEP5: NPS notifica una integrazione documentale su richiesta
		final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> messaggioIntegrazioneRichiesta = createRequest(TipoContestoProceduraleEnum.SICOGE,
				SICOGEMessageIDEnum.SICOGE_DOCUMENTAZIONE_AGGIUNTIVA.getId(), ID_AOO_UCB_LAV, CODICE_AOO_UCB_LAV, identificatoreProcesso, null, idFascicoloFepa, tipoFascicoloFepa);
		final EsitoSalvaDocumentoDTO esitoIntegrazioneRichiesta = AutoProtocolStrategyFactory.protocol(TipoContestoProceduraleEnum.SICOGE, messaggioIntegrazioneRichiesta,
				guidFilenetMessaggio);

		checkAssertion((esitoIntegrazioneRichiesta.getErrori() == null || esitoIntegrazioneRichiesta.getErrori().isEmpty()), ERROR_PROTOCOLLAZIONE_MSG);

		checkStorico(utente, null, esitoProtocollaTitolo.getDocumentTitle(), EventTypeEnum.RICHIESTA_INTEGRAZIONI);
		code = getQueues(utente.getUsername(), esitoProtocollaTitolo.getDocumentTitle(), null, CategoriaDocumentoEnum.ENTRATA.getIds()[0]);
		checkAssertionCode(code, DocumentQueueEnum.DA_LAVORARE_UCB, DocumentQueueEnum.ASSEGNATE);

		// STEP6: NPS notifica una integrazione documentale spontanea
		final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> messaggioIntegrazioneSpontanea2 = createRequest(TipoContestoProceduraleEnum.SICOGE,
				SICOGEMessageIDEnum.SICOGE_DOCUMENTAZIONE_AGGIUNTIVA.getId(), ID_AOO_UCB_LAV, CODICE_AOO_UCB_LAV, identificatoreProcesso, null, idFascicoloFepa, tipoFascicoloFepa);
		final EsitoSalvaDocumentoDTO esitoIntegrazioneSpontanea2 = AutoProtocolStrategyFactory.protocol(TipoContestoProceduraleEnum.SICOGE, messaggioIntegrazioneSpontanea2,
				guidFilenetMessaggio);

		checkAssertion((esitoIntegrazioneSpontanea2.getErrori() == null || esitoIntegrazioneSpontanea2.getErrori().isEmpty()), ERROR_PROTOCOLLAZIONE_MSG);

		checkStorico(utente, null, dtIngresso, EventTypeEnum.RICHIESTA_INTEGRAZIONI);
		code = getQueues(utente.getUsername(), dtIngresso, null, CategoriaDocumentoEnum.ENTRATA.getIds()[0]);
		checkAssertionCode(code, DocumentQueueEnum.DA_LAVORARE_UCB, DocumentQueueEnum.ASSEGNATE);

	}

	/**
	 * Test flusso ORDINARIO.
	 */
	@Test
	public final void flussoORDTour() {
		accordoDiServizio(TipoContestoProceduraleEnum.ORD);
	}
	
	/**
	 * Test flussi ORDINARIO e SILICE.
	 * 
	 * @param tcp
	 */
	private final void accordoDiServizio(final TipoContestoProceduraleEnum tcp) {
		final UtenteDTO utente = getUtente(USERNAME_BRIAMONTE);
		final String guidFilenetMessaggio = GUID_MESSAGGIO;
		final Integer identificativoMSG = null;
		final String identificatoreProcesso = null;

		// STEP1: NPS notifica un messaggio da protocollare in maniera interoperabile
		final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> messaggio = createRequest(tcp, identificativoMSG, ID_AOO_UCB_LAV, CODICE_AOO_UCB_LAV,
				identificatoreProcesso, null, null, null);
		final EsitoSalvaDocumentoDTO esito = AutoProtocolStrategyFactory.protocol(tcp, messaggio, guidFilenetMessaggio);
		checkAssertion((esito.getErrori() == null || esito.getErrori().isEmpty()), ERROR_PROTOCOLLAZIONE_MSG);

		final String dtIngresso = esito.getDocumentTitle();

		checkStorico(utente, null, dtIngresso, EventTypeEnum.ASSEGNAZIONE_COMPETENZA);
		final RecuperoCodeDTO code = getQueues(utente.getUsername(), dtIngresso, null, CategoriaDocumentoEnum.ENTRATA.getIds()[0]);
		checkAssertionCode(code, DocumentQueueEnum.CORRIERE_INDIRETTO, DocumentQueueEnum.PROCEDIMENTI);
	}
	
	/**
	 * Test flusso SILICE.
	 */
	@Test
	public final void flussoSILICETour() {
		accordoDiServizio(TipoContestoProceduraleEnum.SILICE);
	}

	/**
	 * Test flusso BASIC.
	 */
	@Test
	public final void flussoBASICTour() {
		final UtenteDTO utente = getUtente(USERNAME_BRIAMONTE);
		final String guidFilenetMessaggio = GUID_MESSAGGIO;
		final Integer identificativoMSG = null;
		final String identificatoreProcesso = null;

		// STEP1: NPS notifica un messaggio da protocollare in maniera interoperabile
		final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> messaggio = createRequest(TipoContestoProceduraleEnum.BASIC, identificativoMSG, ID_AOO_UCB_LAV, CODICE_AOO_UCB_LAV,
				identificatoreProcesso, null, null, null);
		final EsitoSalvaDocumentoDTO esito = AutoProtocolStrategyFactory.protocol(TipoContestoProceduraleEnum.BASIC, messaggio, guidFilenetMessaggio);
		checkAssertion((esito.getErrori() == null || esito.getErrori().isEmpty()), ERROR_PROTOCOLLAZIONE_MSG);

		final String dtIngresso = esito.getDocumentTitle();

		checkStorico(utente, null, dtIngresso, EventTypeEnum.ASSEGNAZIONE_COMPETENZA);
		final RecuperoCodeDTO code = getQueues(utente.getUsername(), dtIngresso, null, CategoriaDocumentoEnum.ENTRATA.getIds()[0]);
		checkAssertionCode(code, DocumentQueueEnum.CORRIERE_INDIRETTO, DocumentQueueEnum.PROCEDIMENTI);
	}

	/**
	 * Test flusso AUT.
	 */
	@Test
	public final void flussoAUTTour() {
		flussiAUTTour(TipoContestoProceduraleEnum.FLUSSO_AUT);
	}
	
	/**
	 * Test flusso CG2.
	 */
	@Test
	public final void flussoCG2Tour() {
		flussiAUTTour(TipoContestoProceduraleEnum.FLUSSO_CG2);
	}
	
	private final void flussiAUTTour(final TipoContestoProceduraleEnum tcp) {
		final UtenteDTO utente = getUtente(USERNAME_BRIAMONTE);
		final String guidFilenetMessaggio = GUID_MESSAGGIO;
		String dtIngresso;
		Integer idDocIngresso;
		RecuperoCodeDTO code;
		final String identificatoreProcesso = java.util.UUID.randomUUID().toString(); //5069231f-4d85-48e8-bdce-c7b523c4b9ce

		// STEP1: NPS notifica un 101 per identificatoreProcesso
		final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> messaggio101 = createRequest(tcp, AUTMessageIDEnum.MESSAGGIO_INIZIALE.getId(), ID_AOO_UCB_LAV, CODICE_AOO_UCB_LAV, identificatoreProcesso);
		final EsitoSalvaDocumentoDTO esito101 = AutoProtocolStrategyFactory.protocol(tcp, messaggio101, guidFilenetMessaggio);

		checkAssertion((esito101.getErrori() == null || esito101.getErrori().isEmpty()), ERROR_PROTOCOLLAZIONE_MSG);

		dtIngresso = esito101.getDocumentTitle();
		idDocIngresso = esito101.getNumeroDocumento();

		final EsitoOperazioneDTO esitoAssegnazione = assegnaDaCorriere(USERNAME_BRIAMONTE, dtIngresso, USERNAME_BRIAMONTE);
		checkAssertion(esitoAssegnazione.getFlagEsito(), ERROR_ASSEGNAZIONE_CORRIERE_MSG);

		checkStorico(utente, null, dtIngresso, EventTypeEnum.ASSEGNAZIONE_COMPETENZA);
		code = getQueues(utente.getUsername(), dtIngresso, null, CategoriaDocumentoEnum.ENTRATA.getIds()[0]);
		checkAssertionCode(code, DocumentQueueEnum.DA_LAVORARE_UCB, DocumentQueueEnum.ASSEGNATE);

		// STEP2: BRIAMONTE richiede una integrazione dati per il documento in ingresso
		// iniziale di identificatoreProcesso
		richiediIntegrazione(dtIngresso, idDocIngresso, ID_RICHIESTA_INTEGRAZIONE_EX_ART_14BIS_COMMA_2);

		// STEP3: NPS notifica un 102 per rispondere alla richiesta di integrazione su
		// identificatoreProcesso
		final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> messaggio102 = createRequest(tcp, AUTMessageIDEnum.DATI_INTEGRATIVI_RICHIESTI.getId(), ID_AOO_UCB_LAV, CODICE_AOO_UCB_LAV, identificatoreProcesso);
		final EsitoSalvaDocumentoDTO esito102 = AutoProtocolStrategyFactory.protocol(tcp, messaggio102, guidFilenetMessaggio);
		checkAssertion((esito102.getErrori() == null || esito102.getErrori().isEmpty()), ERROR_PROTOCOLLAZIONE_MSG);

		checkStorico(utente, null, dtIngresso, EventTypeEnum.RICHIESTA_INTEGRAZIONI);
		code = getQueues(utente.getUsername(), dtIngresso, null, CategoriaDocumentoEnum.ENTRATA.getIds()[0]);
		checkAssertionCode(code, DocumentQueueEnum.DA_LAVORARE_UCB, DocumentQueueEnum.ASSEGNATE);

		// STEP4: NPS notifica un 103 per fornire spontaneamente ulteriore dcumentazione
		// su identificatoreProcesso
		final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> messaggio103 = createRequest(tcp, AUTMessageIDEnum.DATI_INTEGRATIVI_SPONTANEI.getId(), ID_AOO_UCB_LAV, CODICE_AOO_UCB_LAV, identificatoreProcesso);
		final EsitoSalvaDocumentoDTO esito103 = AutoProtocolStrategyFactory.protocol(tcp, messaggio103, guidFilenetMessaggio);
		checkAssertion((esito103.getErrori() == null || esito103.getErrori().isEmpty()), ERROR_PROTOCOLLAZIONE_MSG);

		code = getQueues(utente.getUsername(), dtIngresso, null, CategoriaDocumentoEnum.ENTRATA.getIds()[0]);
		checkAssertionCode(code, DocumentQueueEnum.DA_LAVORARE_UCB, DocumentQueueEnum.ASSEGNATE);

		// STEP5: NPS notifica un 104 per ritirare in autotutela su
		// identificatoreProcesso
		final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> messaggio104 = createRequest(tcp, AUTMessageIDEnum.RICHIESTA_RITIRO_AUTOTUTELA.getId(), ID_AOO_UCB_LAV, CODICE_AOO_UCB_LAV, identificatoreProcesso);
		final EsitoSalvaDocumentoDTO esito104 = AutoProtocolStrategyFactory.protocol(tcp, messaggio104, guidFilenetMessaggio);
		checkAssertion((esito104.getErrori() == null || esito104.getErrori().isEmpty()), ERROR_PROTOCOLLAZIONE_MSG);
	}

	private static RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> createRequest(final TipoContestoProceduraleEnum flusso, final Integer identificativo,
			final Long idAoo, final String codiceAOO, final String identificatoreProcesso) {
		return createRequest(flusso, identificativo, idAoo, codiceAOO, identificatoreProcesso, null, null, null);
	}

	/**
	 * Crea la request.
	 * 
	 * @param flusso
	 *            flusso specificato, @see TipoContestoProceduraleEnum
	 * @param identificativo
	 *            identificativo del documento
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @param codiceAOO
	 *            codice dell'area organizzativa
	 * @param identificatoreProcesso
	 *            identificativo processo
	 * @param metadatiEstesi
	 *            metadati estesi del documento
	 * @param idFascicoloFepa
	 *            identificativo fascicolo FEPA
	 * @param tipoFascicoloFepa
	 *            tipo di fascicolo
	 * @return request
	 */
	private static RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> createRequest(final TipoContestoProceduraleEnum flusso, final Integer identificativo,
			final Long idAoo, final String codiceAOO, final String identificatoreProcesso, final TIPOLOGIA metadatiEstesi, final String idFascicoloFepa,
			final String tipoFascicoloFepa) {
		final String codiceFlusso = flusso.getId();
		final DatiFlussoDTO datiFlusso = new DatiFlussoDTO(identificativo, identificatoreProcesso, codiceFlusso, metadatiEstesi, idFascicoloFepa, tipoFascicoloFepa);

		final RichiestaElaboraMessaggioProtocollazioneFlussoType req = new RichiestaElaboraMessaggioProtocollazioneFlussoType();
		final EmailMessageType emt = getEmailMessageMocked(codiceAOO);

		req.setMessaggioRicevuto(emt);

		final WkfDatiContestoProceduraleType el = new WkfDatiContestoProceduraleType();
		final ContestoProcedurale contesto = getContestoProceduraleMocked(identificativo, codiceFlusso);
		
		el.setContestoProcedurale(contesto);
		req.setIdentificatoreProcesso(java.util.UUID.randomUUID().toString());
		req.getContestoProcedurale().add(el);

		MessaggioPostaNpsFlussoDTO messaggio = null;

		if (TipoContestoProceduraleEnum.SICOGE.getId().equals(codiceFlusso)) {
			messaggio = new MessaggioPostaNpsFlussoDTO(req.getIdMessaggio(), req.getProtocollo().getProtocollo(), codiceFlusso, req.getProtocollazioneCoda(),
					req.getIdentificatoreProcesso(), idFascicoloFepa, tipoFascicoloFepa);
		} else {
			messaggio = new MessaggioPostaNpsFlussoDTO(req.getIdMessaggio(), req.getIdDocumento(), req.getMessaggioRicevuto(), req.getProtocollo().getProtocollo(),
					req.getProtocolloNotifica().getProtocollo(), codiceFlusso, req.getContestoProcedurale().get(0), req.getIdentificatoreProcesso());
		}

		final ProtocolloNpsDTO protocollo = getProtocolloMocked(identificativo, codiceAOO, identificatoreProcesso, codiceFlusso, null);
		forceSet(MessaggioPostaNpsDTO.class, "protocollo", messaggio, protocollo);
		forceSet(MessaggioPostaNpsFlussoDTO.class, "datiFlusso", messaggio, datiFlusso);

		final String messageIdNps = "123";
		final Long idTrack = 0L;

		return new RichiestaElabMessaggioPostaNpsDTO<>(messageIdNps, messaggio, StatoElabMessaggioPostaNpsEnum.DA_ELABORARE, idAoo, idTrack);
	}

	private static XMLGregorianCalendar getXMLGregorianCalendar() {
		try {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar();
		} catch (final DatatypeConfigurationException e) {
			LOGGER.error(e);
			throw new RedException(e);
		}
	}

	private static void forceSet(final Class<? extends Object> cls, final String fieldName, final Object obj, final Object value) {
		try {
			final Field field = cls.getDeclaredField(fieldName);
			field.setAccessible(true);
			field.set(obj, value);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}
	}

	private void richiediIntegrazione(final String dtIngresso, final Integer idDocIngresso, final Integer idRegistro) {
		final UtenteDTO utente = getUtente(USERNAME_BRIAMONTE);

		final MasterDocumentRedDTO masterIngresso = getDocumentForDT(utente, dtIngresso, idDocIngresso);
		checkAssertion(masterIngresso != null, "Documento non trovato.");

		final Collection<MetadatoDTO> metadatiRegistroAusiliarioList = getMetadatiRegistro(idRegistro, utente, dtIngresso);

		final CollectedParametersDTO c = new CollectedParametersDTO();

		for (final MetadatoDTO m : metadatiRegistroAusiliarioList) {
			c.addStringParam(m.getName(), m.getName());
		}

		final FileDTO contentUscita = new FileDTO("template.pdf", createPdf(idRegistro, c.getStringParams()), MediaType.PDF.toString());

		final DetailDocumentRedDTO detail = predisponiRichiestaIntegrazione(utente, masterIngresso, contentUscita, idRegistro, metadatiRegistroAusiliarioList);

		final String classeDoc = masterIngresso.getClasseDocumentale();
		final Integer idCatDocIngresso = masterIngresso.getIdCategoriaDocumento();

		// Valorizzazione iter di firma
		detail.setIdIterApprovativo(IterApprovativoDTO.ITER_FIRMA_MANUALE);
		final List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneFirma(ID_UFFICIO_DIREZIONE_ULAV, ID_FRANCESCO_BRIAMONTE, "FRANCESCO", "BRIAMONTE"));
		detail.setAssegnazioni(assegnazioni);

		// Valorizzare Tab spedizione
		if (detail.getDestinatari() != null && !detail.getDestinatari().isEmpty()
				&& MezzoSpedizioneEnum.ELETTRONICO.equals(detail.getDestinatari().get(0).getMezzoSpedizioneEnum())) {
			final EmailDTO mailSpedizione = new EmailDTO();
			mailSpedizione.setMittente("testred@pec.mef.gov.it");
			mailSpedizione.setDestinatari(MAIL_TEST);
			mailSpedizione.setOggetto("MEF - RGS - Un numero di protocollo valido - " + detail.getOggetto());
			mailSpedizione.setTesto("Ciao Mondo!");
			detail.setMailSpedizione(mailSpedizione);
		}

		// Valorizzare Indice di classificazione
		detail.setIndiceClassificazioneFascicoloProcedimentale("A");

		// Protocollazione standard
		detail.setMomentoProtocollazioneEnum(MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD);

		// finalizza creazione documento uscita

		final EsitoSalvaDocumentoDTO esitoSalvataggioRisposta = salvaDocumento(detail, utente);

		checkAssertion((esitoSalvataggioRisposta.getErrori() == null || esitoSalvataggioRisposta.getErrori().isEmpty()),
				"La creazione dell'uscita in risposta non è andata a buon fine.");

		final RecuperoCodeDTO codeIngresso = getQueues(utente.getUsername(), dtIngresso, classeDoc, idCatDocIngresso);

		checkAssertionCode(codeIngresso, DocumentQueueEnum.SOSPESO, DocumentQueueEnum.ASSEGNATE);
	}

	/**
	 * Test rispondi A.
	 */
	@Test
	public final void rispondiATour() {
		final UtenteDTO utente = getUtente(USERNAME_FALCO);

		final String messageIdNps = java.util.UUID.randomUUID().toString().toUpperCase();
		final String codiceAooNPS = "UCB_LAV";
		final String sistemaAusiliario = "SPESE";
		final String idProtocolloEntrata = "0C15ABB6-72E9-8001-E053-0A64CB5BACF4"; // 100773 - 452525
		final String idProtocolloUscita = "A9F9B8B6-259E-3E02-E053-0A64CB60DB88"; // 100797

		final String[] ulterioriIdProtocollo = new String[] { "CA4EACB6-F175-A201-E053-0A64CB60B0B1"// 100774 - 452526
		};
		final TipoProtocolloEnum[] tipiUlterioriIdProtocollo = new TipoProtocolloEnum[] { TipoProtocolloEnum.ENTRATA };

		final ParametersElaboraNotificaAzione richiesta = creaRequestNotificaAzione(messageIdNps, codiceAooNPS, utente.getNome() + " " + utente.getCognome(),
				sistemaAusiliario, idProtocolloEntrata, idProtocolloUscita, TipoNotificaAzioneNPSEnum.RISPONDI_A, Arrays.asList(ulterioriIdProtocollo),
				Arrays.asList(tipiUlterioriIdProtocollo));

		// accodamento della notifica azione di tipo RISPONDI_A
		final Long idCoda = accodaNotificaAzione(richiesta);

		elaboraRispondiA(idCoda);
	}

	private void elaboraRispondiA(final Long idCoda) {
		// lettura dell'item inserito
		final NotificaNpsDTO item = recuperaRichiestaElab(idCoda.intValue());

		// elaborazione dell'item
		if (item instanceof NotificaAzioneNpsDTO) {
			WebServiceClientProvider.getIstance().checkInitializationBdNPS();
			elaboraAzione((NotificaAzioneNpsDTO) item);
		} else {
			assertTrue("La notifica non risulta essere di tipo azione", true);
		}
	}

	/**
	 * Test elaborazione Rispondi A.
	 */
	@Test
	public final void elaboraRispondiA() {
		final Long idCoda = 472L;
		elaboraRispondiA(idCoda);
	}

	/**
	 * Crea la request per la notifica azione.
	 * 
	 * @param messageIdNps
	 *            identificativo messaggio NPS
	 * @param codiceAooNPS
	 *            codice area organizzativa NPS
	 * @param protocollatore
	 *            utente protollatore
	 * @param sistemaAusiliario
	 *            sistema ausiliario NPS
	 * @param idProtocolloEntrata
	 *            identificativo protocollo entrata
	 * @param idProtocolloUscita
	 *            identificativo protocollo uscita
	 * @param tipoAzione
	 *            tipologia dell'azione
	 * @param ulterioriIdProtocollo
	 *            identificativi ulteriori protocolli
	 * @param tipiUlterioriIdProtocollo
	 *            tipologia dei protocolli ulteriori
	 * @return parametri per l'elabora notifica azione
	 */
	private static ParametersElaboraNotificaAzione creaRequestNotificaAzione(final String messageIdNps, final String codiceAooNPS, final String protocollatore,
			final String sistemaAusiliario, final String idProtocolloEntrata, final String idProtocolloUscita, final TipoNotificaAzioneNPSEnum tipoAzione,
			final List<String> ulterioriIdProtocollo, final List<TipoProtocolloEnum> tipiUlterioriIdProtocollo) {

		final ParametersElaboraNotificaAzione parametersDTO = new ParametersElaboraNotificaAzione(messageIdNps);
		parametersDTO.setIdMessaggioNPS(java.util.UUID.randomUUID().toString().toUpperCase());
		parametersDTO.setCodiceAooNPS(codiceAooNPS);
		parametersDTO.setDataAzione(new Date());
		final ParametersAzioneRispostaAutomatica datiRispondiA = new ParametersAzioneRispostaAutomatica(idProtocolloEntrata, null, null, null, null, TipoProtocolloEnum.ENTRATA.getId(),
				protocollatore, sistemaAusiliario, TipoNotificaAzioneNPSEnum.RISPONDI_A.name());
		if (ulterioriIdProtocollo != null) {
			for (int i = 0; i < ulterioriIdProtocollo.size(); i++) {
				datiRispondiA.addUlterioreAllaccio(ulterioriIdProtocollo.get(i), null, null, null, null, tipiUlterioriIdProtocollo.get(i).getId());
			}
		}
		parametersDTO.setDatiRispondiA(datiRispondiA);
		parametersDTO.setIdProtocollo(idProtocolloUscita);
		parametersDTO.setTipoAzione(tipoAzione.toString());

		return parametersDTO;
	}
	
	/**
	 * Test flusso AUT MAE - Documento notifica associato all' entrata.
	 */
	@Test
	public void flussoAUTNotifica() {
		final UtenteDTO utente = getUtente(USERNAME_QUINZI);
		final String identificatoreProcesso = java.util.UUID.randomUUID().toString();
		final Integer numeroProtocolloNotifica = 1726; // Deve essere il numero protocollo di un documento tipo Notifica protocollato.
		
		// Simulazione ricezione messaggio 101.
		RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> messaggio101 = null;
		messaggio101 = createRequestFlussoAUT(AUTMessageIDEnum.MESSAGGIO_INIZIALE.getId(), ID_AOO_UCB_LAV, CODICE_AOO_UCB_LAV, identificatoreProcesso, numeroProtocolloNotifica);
		
		final EsitoSalvaDocumentoDTO esito101 = AutoProtocolStrategyFactory.protocol(TipoContestoProceduraleEnum.FLUSSO_AUT, messaggio101, null);
		
		// Verifica errori su esito
		checkAssertion(CollectionUtils.isEmpty(esito101.getErrori()), ERROR_PROTOCOLLAZIONE_MSG);

		// Verifica parametri importanti in output
		String dtIngresso = esito101.getDocumentTitle();
		Integer idDocIngresso = esito101.getNumeroDocumento();
		checkAssertion(idDocIngresso != null && dtIngresso != null, "Document title o numero documento dell'esito non popolati correttamente.");
		
		// Assegnazione documento per verifica worklfow
		final EsitoOperazioneDTO esitoAssegnazione = assegnaDaCorriere(USERNAME_QUINZI, dtIngresso, USERNAME_QUINZI);
		checkAssertion(esitoAssegnazione.getFlagEsito(), ERROR_ASSEGNAZIONE_CORRIERE_MSG);

		// Verifica correttezza storico
		checkStorico(utente, null, dtIngresso, EventTypeEnum.ASSEGNAZIONE_COMPETENZA);
		
		// Verifica correttezza code
		RecuperoCodeDTO code = getQueues(utente.getUsername(), dtIngresso, null, CategoriaDocumentoEnum.ENTRATA.getIds()[0]);
		checkAssertionCode(code, DocumentQueueEnum.DA_LAVORARE_UCB, DocumentQueueEnum.ASSEGNATE);
	
		// Recupero fascicolo del documento principale
		FascicoloDTO fascicolo = fascicoloSRV.getFascicoloProcedimentale(esito101.getDocumentTitle(), utente.getIdAoo().intValue(), utente);
		checkAssertion(fascicolo != null, "Documento principale non fascicolato correttamente.");
		
		// Verifica fascicolazione notifica
		DocumentoFascicoloDTO notifica = null;
		Collection<DocumentoFascicoloDTO> documentiFascicolati = fascicoloSRV.getDocumentiFascicolo(Integer.parseInt(fascicolo.getIdFascicolo()), utente);
		for (DocumentoFascicoloDTO docFascicolato : documentiFascicolati) {
			if(TipoAllaccioEnum.NOTIFICA.toString().equals(docFascicolato.getTipoDocumento())) {
				notifica = docFascicolato;
				break;
			}
		}
		
		checkAssertion(notifica != null, "Documento notifica non fascicolato nello stesso fascicolo correttamente.");
		
		// Verifica notifica nps dell'allaccio.
		Connection connection = null;
		try {
			connection = createNewDBConnection();
			boolean allaccioNotificato = npsDAO.isActivityPresent(esito101.getDocumentTitle(), "aggiungiAllacci", connection);
			checkAssertion(allaccioNotificato, "Allaccio non presente nella tabella di sincronizzazione Nps.");
			
		} catch (Exception e) {
			LOGGER.error("Errore recupero protocollo Nps.", e);
		} finally {
			safeCloseConnection(connection);
		}
	}

	/**
	 * Gestisce la chiusura della connessione.
	 * @param connection
	 */
	private static void safeCloseConnection(Connection connection) {
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			LOGGER.error("Errore nella chiusura della connessione.", e);
		}
	}

	/**
	 * Crea una request per flusso AUT restituendo il messaggio 101 per la protocollazione attraverso @AutoProtocolStrategyFactory fatta da {@link #flussoAUTNotifica()}.
	 * @param identificativo
	 * @param idAoo
	 * @param codiceAOO
	 * @param identificatoreProcesso
	 * @param numeroProtocolloNotifica
	 * @return messaggio101 simulato
	 */
	private static RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> createRequestFlussoAUT(final Integer identificativo,
			final Long idAoo, final String codiceAOO, final String identificatoreProcesso, final Integer numeroProtocolloNotifica) {
		
		final String codiceFlusso = TipoContestoProceduraleEnum.FLUSSO_AUT.getId();
		final DatiFlussoDTO datiFlusso = getDatiFlussoMocked(identificativo, identificatoreProcesso);

		final RichiestaElaboraMessaggioProtocollazioneFlussoType req = new RichiestaElaboraMessaggioProtocollazioneFlussoType();
		final EmailMessageType emt = getEmailMessageMocked(codiceAOO);
		req.setMessaggioRicevuto(emt);

		final WkfDatiContestoProceduraleType el = new WkfDatiContestoProceduraleType();
		final ContestoProcedurale contesto = getContestoProceduraleMocked(identificativo, codiceFlusso);

		el.setContestoProcedurale(contesto);
		req.getContestoProcedurale().add(el);
		req.setIdentificatoreProcesso(java.util.UUID.randomUUID().toString());
		
		MessaggioPostaNpsFlussoDTO messaggio = new MessaggioPostaNpsFlussoDTO(req.getIdMessaggio(), req.getIdDocumento(), req.getMessaggioRicevuto(), 
				null, null, codiceFlusso, req.getContestoProcedurale().get(0), req.getIdentificatoreProcesso());

		
		final ProtocolloNpsDTO protocollo = getProtocolloMocked(identificativo, codiceAOO, identificatoreProcesso, codiceFlusso, null);
		final ProtocolloNpsDTO protocolloNotifica = getProtocolloMocked(identificativo, codiceAOO, identificatoreProcesso, codiceFlusso, numeroProtocolloNotifica);
		
		forceSet(MessaggioPostaNpsDTO.class, "protocollo", messaggio, protocollo);
		forceSet(MessaggioPostaNpsDTO.class, "protocolloNotifica", messaggio, protocolloNotifica);
		forceSet(MessaggioPostaNpsFlussoDTO.class, "datiFlusso", messaggio, datiFlusso);

		final String messageIdNps = "123";
		final Long idTrack = 0L;
		return new RichiestaElabMessaggioPostaNpsDTO<>(messageIdNps, messaggio, StatoElabMessaggioPostaNpsEnum.DA_ELABORARE, idAoo, idTrack);
	}

	/**
	 * Crea e restituisce un ContestoProcedurale per la creazione di una request.
	 * @param identificativo
	 * @param codiceFlusso
	 * @return contesto procedurale correttamente popolato per la costruzione di una request
	 */
	private static ContestoProcedurale getContestoProceduraleMocked(final Integer identificativo, final String codiceFlusso) {
		final ContestoProcedurale contesto = new ContestoProcedurale();
		final Identificativo id = new Identificativo();
		id.setContent("" + identificativo);
		contesto.setIdentificativo(id);
		final TipoContestoProcedurale tipoProc = new TipoContestoProcedurale();
		tipoProc.setContent(codiceFlusso);
		contesto.setTipoContestoProcedurale(tipoProc);
		return contesto;
	}

	/**
	 * Crea, popola e restituisce un @EmailMessageType per la costruzione della request.
	 * @param codiceAOO
	 * @return EmailmessageType per la creazione della request 101
	 */
	private static EmailMessageType getEmailMessageMocked(final String codiceAOO) {
		final EmailMessageType emt = new EmailMessageType();
		emt.setCodiMessaggio("se non ci scrivo niente scoppi");
		emt.setDataMessaggio(getXMLGregorianCalendar());
		final EmailIndirizzoEmailType mittente = new EmailIndirizzoEmailType();
		mittente.setDisplayName("uno qualunque");
		mittente.setEmail(MAIL_TEST);
		emt.setMittente(mittente);

		final OrgCasellaEmailType casella = new OrgCasellaEmailType();
		final EmailIndirizzoEmailType eiet = new EmailIndirizzoEmailType();
		eiet.setEmail(MAIL_TEST);
		casella.setIndirizzoEmail(eiet);
		final OrgAooType aoo = new OrgAooType();
		aoo.setCodiceAOO(codiceAOO);
		casella.setAoo(aoo);
		emt.setCasellaEmail(casella);
		return emt;
	}

	/**
	 * Crea e restituisce un DTO di DatiFlusso popolato correttamente per il superamento della fase di protocollazione fatta da {@link #flussoAUTNotifica()}.
	 * @param identificativo
	 * @param identificatoreProcesso
	 * @return DatiFlussoDTO per il superamento del test {@link #flussoAUTNotifica()}
	 */
	private static DatiFlussoDTO getDatiFlussoMocked(final Integer identificativo, final String identificatoreProcesso) {
		TIPOLOGIA metadatiEstesi = new TIPOLOGIA();
		metadatiEstesi.setFAMIGLIA(1);
		metadatiEstesi.setNOME("ASAV");
		metadatiEstesi.setVERSIONE(1);
		String codiceFlusso = TipoContestoProceduraleEnum.FLUSSO_AUT.getId();
		
		return new DatiFlussoDTO(identificativo, identificatoreProcesso, codiceFlusso , metadatiEstesi , null, null);
	}

	/**
	 * Restituisce un ProtocolloNpsDTO popolato con tutti i campi obbligatori per l'esecuzione di un test.
	 * @param identificativo utilizzato per il popolamento dell'oggetto protocollo
	 * @param codiceAOO codice AOO dell'utente utilizzato per la protocollazione
	 * @param identificatoreProcesso identificato del processo
	 * @param codiceFlusso codice del flusso associato al protocollo
	 * @param numeroProtocollo se diverso da null viene usato come numero protocollo, se null ne viene creato uno mockato
	 * @return ProtocolloNpsDTO correttamente popolato per la creazione di una request
	 */
	private static ProtocolloNpsDTO getProtocolloMocked(final Integer identificativo, final String codiceAOO,
			final String identificatoreProcesso, final String codiceFlusso, final Integer numeroProtocollo) {
		final ProtocolloNpsDTO protocollo = new ProtocolloNpsDTO();
		protocollo.setIdProtocollo("idProtocollo");
		protocollo.setOggetto(codiceFlusso + " " + identificatoreProcesso + " " + identificativo);
		protocollo.setDescrizioneTipologiaDocumento("ASAV");
		final PersonaFisicaDTO pf = new PersonaFisicaDTO();
		pf.setCodiceFiscalePF("QNZLNZ63A26H501Y");
		protocollo.setProtocollatore(pf);
		final Contatto mit = new Contatto(37003L, "LORENZO", "QUINZI", "F", "QNZLNZ63A26H501Y", null, null, null);
		mit.setDatacreazione(new Date());
		mit.setPubblico(1);
		protocollo.setMittente(mit);
		protocollo.setDescrizioneTitolario("A");
		final DocumentoNpsDTO documentoPrincipale = new DocumentoNpsDTO();
		documentoPrincipale.setContentType(Constants.ContentType.PDF);
		documentoPrincipale.setNomeFile("test.pdf");
		documentoPrincipale.setInputStream(new ByteArrayInputStream(FileUtils.getFileFromFS(FILESYSTEM_PATH)));
		protocollo.setDocumentoPrincipale(documentoPrincipale);
		
		protocollo.setTipoProtocollo(TipoProtocolloEnum.ENTRATA.getId().toString());
		
		final IMockSRV mockSRV = ApplicationContextProvider.getApplicationContext().getBean(IMockSRV.class);
		final ProtocolloDTO protocolloNEW = mockSRV.getNewProtocollo(codiceAOO, TipoProtocolloEnum.ENTRATA);

		if(numeroProtocollo != null) {
			protocollo.setNumeroProtocollo(numeroProtocollo);
		} else {
			protocollo.setNumeroProtocollo(protocolloNEW.getNumeroProtocollo());
		}
		
		protocollo.setAnnoProtocollo(protocolloNEW.getAnnoProtocollo());
		protocollo.setDataProtocollo(protocolloNEW.getDataProtocollo());
		return protocollo;
	}

}
