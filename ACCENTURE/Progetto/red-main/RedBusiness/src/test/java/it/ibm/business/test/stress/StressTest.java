package it.ibm.business.test.stress;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.business.test.suite.AbstractTest;

/**
 * Classe di test - Stress.
 */
public class StressTest extends AbstractTest {

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}
	
	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Stress test.
	 */
	@Test
	public static final void test() {
		// Metodo da implementare.
	}
}
