package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Test 09.
 */
public class Test09 extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test09.class);
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}
	
	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test 09.
	 */
	@Test
	public final void test() {

		//TONETTI CREA UN DOCUMENTO IN INGRESSO
		UtenteDTO utente = getUtente(USERNAME_TONETTI);
		String oggetto = createObj();
		String indiceFascicoloProcedimentale = "A";

		Boolean bRiservato = false;

		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(createDestinatarioEsterno(MezzoSpedizioneEnum.CARTACEO, ID_CONTATTO_UNO, ModalitaDestinatarioEnum.TO));

		List<AllegatoDTO> allegati = null;
		List<AssegnazioneDTO> assegnazioni = null;
		EsitoSalvaDocumentoDTO esitoUscita = createDocUscita(allegati, false, oggetto, utente, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, assegnazioni, destinatari, indiceFascicoloProcedimentale, MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD, FIRMA_RAGIONIERE, bRiservato, null);
		checkAssertion(esitoUscita.isEsitoOk(), "Errore in fase di creazione documento in uscita.");
		String dt = esitoUscita.getDocumentTitle();

		// CORBO: LIBRO FIRMA -> CORRIERE
		EsitoOperazioneDTO esitoSiglaInvia = siglaInvia(USERNAME_CORBO, dt);
		checkAssertion(esitoSiglaInvia.isEsito(), "Errore in fase di sigla ed invia.");

		waitSeconds(60);
		
		// MICOCCI: CORRIERE -> LIBRO FIRMA
		EsitoOperazioneDTO esitoProcediMicocci = procedi(USERNAME_MICOCCI, dt);
		checkAssertion(esitoProcediMicocci.isEsito(), "Errore in fase di procedi da corriere.");

		// TANZI: LIBRO FIRMA -> CORRIERE
		EsitoOperazioneDTO esitoSiglaInviaTanzi = siglaInvia(USERNAME_TANZI, dt);
		checkAssertion(esitoSiglaInviaTanzi.isEsito(), "Errore in fase di sigla ed invia.");

		// TUCCI: CORRIERE -> CORRIERE
		EsitoOperazioneDTO esitoInviaCoordinamento = inviaUfficioCoordinamento(USERNAME_TUCCI, dt); //"Invia a ufficio di coordinamento"
		checkAssertion(esitoInviaCoordinamento.isEsito(), "Errore in fase di invia ufficio coordinamento.");

		// SMERIGLIO: CORRIERE -> CORRIERE
		EsitoOperazioneDTO esitoFirmato = firmato(USERNAME_SMERIGLIO, dt); //"Firmato"
		checkAssertion(esitoFirmato.isEsito(), "Errore in fase di firmato.");

		waitSeconds(15);

		// SMERIGLIO: CORRIERE -> CORRIERE
		EsitoOperazioneDTO esitoInviaUCP = inviaUCP(USERNAME_SMERIGLIO, dt); //"Invia a UCP"
		checkAssertion(esitoInviaUCP.isEsito(), "Errore in fase di invio ad UCP.");

		// ASCI: CORRIERE -> SPEDITO
		EsitoOperazioneDTO esitoSpedito = spedito(USERNAME_ASCI, dt);
		checkAssertion(esitoSpedito.isEsito(), "Errore in fase di spedito.");

		LOGGER.info("DOCUMENT TITLE : " + dt);
		LOGGER.info("OGGETTO : " + oggetto);
		LOGGER.info("NUM DOC : " + getNumDoc(USERNAME_NUNZI, dt));
	}

}
