package it.ibm.business.test;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.service.IInvioInFirmaSRV;
import it.ibm.red.business.service.concrete.InvioInFirmaSRV;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;

/**
 * Test invio in sigla.
 */
public class InvioInSiglaTest extends AbstractTest {

	/**
	 * Urgente.
	 */
	private static final Integer TRUE_URGENTE = 1;

	/**
	 * Nodo destinatario.
	 */
	private static final Long ID_NODO_DESTINATARIO_NEW = 228L;

	/**
	 * Utente destinatario.
	 */
	private static final Long ID_UTENTE_DESTINATARIO_NEW = 1875L;

	/**
	 * Servizio.
	 */
	private IInvioInFirmaSRV invioInFirma;

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Label.
	 */
	private static final String MOTIVAZIONE = "test invio firma";

	/**
	 * WFS.
	 */
	private Collection<String> wobNumbers;

	/**
	 * Metodo per l'inizializzazione.
	 */
	@Before
	public void initialize() {
		before();
		invioInFirma = getBean(InvioInFirmaSRV.class);
		final IUtenteFacadeSRV facadeUtente = getBean(IUtenteFacadeSRV.class);
		utente = facadeUtente.getByUsername("BIAGIO.MAZZOTTA");
		final String[] wobNumbersArray = { "7A42106B02358E4CAB8553A2C0649011" };
		wobNumbers = Arrays.asList(wobNumbersArray);
	}

	/**
	 * Test di invio in sigla.
	 */
	@Test
	public void invioInSiglaTest() {

		invioInFirma.invioInFirmaSiglaVisto(utente, wobNumbers, ID_NODO_DESTINATARIO_NEW, ID_UTENTE_DESTINATARIO_NEW, MOTIVAZIONE, ResponsesRedEnum.INVIO_IN_FIRMA,
				TRUE_URGENTE);
	}

}
