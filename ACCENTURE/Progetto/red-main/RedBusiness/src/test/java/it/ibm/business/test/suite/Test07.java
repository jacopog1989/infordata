package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Test 07.
 */
public class Test07 extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test07.class);
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}
	
	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test.
	 */
	@Test
	public final void test() {
		String oggetto = createObj();

		//Simulo il predisponi documento creando un documento in uscita.
		// TANZI: EMPTY -> LIBRO FIRMA
		UtenteDTO utente = getUtente(USERNAME_TANZI);
		
		Boolean bRiservato = false;
		String indiceFascicoloProcedimentale = "A";

		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(createDestinatarioEsterno(MezzoSpedizioneEnum.ELETTRONICO, ID_CONTATTO_UNO, ModalitaDestinatarioEnum.TO));
		
		List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneFirma(ID_IGF, ID_GIANFANCO_TANZI, "GIANFRANCO", "TANZI"));
		List<AllegatoDTO> allegati = null;
		EsitoSalvaDocumentoDTO esitoUscita = createDocUscita(allegati, false, oggetto, utente, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, assegnazioni, destinatari, indiceFascicoloProcedimentale, MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD, IterApprovativoDTO.ITER_FIRMA_MANUALE, bRiservato, null);
		String dt = esitoUscita.getDocumentTitle();
		
		checkAssertion(esitoUscita.isEsitoOk(), "Errore in fase di creazione documento in uscita.");

		// TANZI: CORRIERE -> LIBRO FIRMA
		EsitoOperazioneDTO esitoProcedi = procedi(USERNAME_TANZI, esitoUscita.getDocumentTitle());
		checkAssertion(esitoProcedi.isEsito(), "Errore in fase di procedi da corriere.");
		
		// TANZI: LIBRO FIRMA -> SPEDIZIONE
		//		Firma remota
		
		waitTransformation();
		
		String otp = "984916";
		String pin = "87654321";
		Collection<EsitoOperazioneDTO> esiti = firmaRemota(USERNAME_TANZI, SignTypeEnum.PADES_VISIBLE, dt, otp, pin);
		EsitoOperazioneDTO esitoFirma = esiti.iterator().next();
		LOGGER.info(esitoFirma.getNote());
		checkAssertion(esitoFirma.isEsito(), "Errore in fase di firma remota.");

		LOGGER.info("DOCUMENT TITLE : " + dt);
		LOGGER.info("OGGETTO : " + oggetto);
		LOGGER.info("NUM DOC : " + getNumDoc(USERNAME_NUNZI, dt));
	}

}
