package it.ibm.business.test.ucb;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.utils.StringUtils;

/**
 * Test 05 - Attr Ext.
 */
public class Test05AttrExt extends AbstractUCBTest {

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test attr ext.
	 */
	@Test
	public final void testAttrTextSample() {
		UtenteDTO utente = getUtente(USERNAME_BRIAMONTE);
		Integer idTipoDocumento = ID_TIPO_DOC_OSSERVAZIONE_123_USCITA_ULAV;
		Integer idTipoProcedimento = ID_TIPO_PROC_OSSERVAZIONE_EX_ART_6_ULAV;
		
		String sample = exportSample(utente, idTipoDocumento, idTipoProcedimento);

		checkAssertion(!StringUtils.isNullOrEmpty(sample), "Sample nullo.");
	}

}
