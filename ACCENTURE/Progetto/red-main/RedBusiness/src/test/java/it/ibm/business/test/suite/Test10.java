package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import filenet.vw.api.VWSession;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.StringUtils;

/**
 * Test 10.
 */
public class Test10 extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test10.class);
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}
	
	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test 10.
	 */
	@Test
	public final void test() {

		//RICCARDI CREA UN DOCUMENTO IN USCITA
		UtenteDTO utente = getUtente(USERNAME_RICCARDI);
		String oggetto = createObj();
		String indiceFascicoloProcedimentale = "A";

		Boolean bRiservato = false;

		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(createDestinatarioEsterno(MezzoSpedizioneEnum.ELETTRONICO, ID_CONTATTO_GIANCARLO_ROSSI, ModalitaDestinatarioEnum.TO));

		List<AssegnazioneDTO> assegnazioni = null;
		List<AllegatoDTO> allegati = null;
		EsitoSalvaDocumentoDTO esitoUscita = createDocUscita(allegati, false, oggetto, utente, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, assegnazioni, destinatari, indiceFascicoloProcedimentale, MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD, FIRMA_RAGIONIERE, bRiservato, null);
		checkAssertion(esitoUscita.isEsitoOk(), "Errore in fase di creazione documento in uscita.");
		String dt = esitoUscita.getDocumentTitle();

		
		// RICCARDI: LIBRO FIRMA -> DA LAVORARE
		EsitoOperazioneDTO esitoSiglaInvia = siglaInvia(USERNAME_RICCARDI, dt);
		checkAssertion(esitoSiglaInvia.isEsito(), "Errore in fase di sigla ed invia.");

		waitSeconds(30);

		// MAZZOTTA: CORRIERE -> LIBRO FIRMA
		EsitoOperazioneDTO esitoProcediMazzotta = procedi(USERNAME_MAZZOTTA, dt);
		checkAssertion(esitoProcediMazzotta.isEsito(), "Errore in fase di procedi.");

		// MAZZOTTA: LIBRO FIRMA -> DA LAVORARE
		EsitoOperazioneDTO esitoSiglaInviaMazzotta = siglaInvia(USERNAME_MAZZOTTA, dt);
		checkAssertion(esitoSiglaInviaMazzotta.isEsito(), "Errore in fase di sigla invia.");
 
		// MAZZOTTA: DA LAVORARE -> LIBRO FIRMA
		EsitoOperazioneDTO esitoInviaFirmaDigitale = inviaFirmaDigitale(USERNAME_TUCCI, dt);
		checkAssertion(esitoInviaFirmaDigitale.isEsito(), "Errore in fase di invia firma digitale.");

		// RAGIONIERE trova documento nella coda libro firma
		UtenteDTO rg = getUtente(USERNAME_RAGIONIERE);
		VWSession sessionPERG = getPESession(rg);
		VWWorkObject wo = fromDocumentTitleToWF(sessionPERG, DocumentQueueEnum.NSD, dt, rg.getIdUfficio().intValue(), null);
		checkAssertion(!StringUtils.isNullOrEmpty(wo.getWorkObjectNumber()), "Il documento non è presente nella coda libro firma.");

		LOGGER.info("DOCUMENT TITLE : " + dt);
		LOGGER.info("OGGETTO : " + oggetto);
		LOGGER.info("NUM DOC : " + getNumDoc(USERNAME_NUNZI, dt));

	}

}
