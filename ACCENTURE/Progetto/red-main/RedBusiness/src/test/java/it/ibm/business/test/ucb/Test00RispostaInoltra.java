package it.ibm.business.test.ucb;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.net.MediaType;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.RecuperoCodeDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.FileUtils;

/**
 * Test 00 - Risposta Inoltra.
 */
public class Test00RispostaInoltra extends AbstractUCBTest {

	private static final String NUM = " NUM > ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test00RispostaInoltra.class);

	/**
	 * Nome file pdf di test.
	 */
	private static final String FILENAME_PDF = "fileTest.pdf";
	
	/**
	 * Prefisso messaggio informativo document title.
	 */
	private static final String USCITA_DT = "#####USCITA : DT > ";
	
	/**
	 * Label verifica code uscita.
	 */
	private static final String VERIFICA_CODE_USCITA = "Verifica code uscita";

	/**
	 * Label verifica code entrata.
	 */
	private static final String VERIFICA_CODE_INGRESSO = "Verifica code ingresso";

	/**
	 * Testo mail della mail di test.
	 */
	private static final String TESTO_MAIL_TEST = "Ciao Mondo!";

	/**
	 * Mail destinatario di test.
	 */
	private static final String DESTINATARIO_MAIL_TEST = "a@b.c";

	/**
	 * Mail mittente test.
	 */
	private static final String MITTENTE_MAIL_TEST = "testred@pec.mef.gov.it";

	/**
	 * Oggetto test.
	 */
	private static final String OGGETTO_TEST = "MEF - RGS - Un numero di protocollo valido - ";

	/**
	 * Messaggio errore creazione uscita in risposta.
	 */
	private static final String ERROR_CREAZIONE_USCITA_IN_RISPOSTA_MSG = "La creazione dell'uscita in risposta non è andata a buon fine.";

	/**
	 * Doc non trovato.
	 */
	private static final String DOCUMENTO_NON_TROVATO = "Documento non trovato.";

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test risposta.
	 */
	@Test
	public final void risposta() {

//		STEP 1
//		BRIAMONTE crea ed assegna al CORRIERE dell'ULAV

		final UtenteDTO utente = getUtente(USERNAME_BRIAMONTE);

		final EsitoSalvaDocumentoDTO esitoCreazione = createDoc(utente, ID_TIPO_DOC_JU_E, ID_TIPO_PROC_JU_E);

		dumpCreazioneDocumento(esitoCreazione);
		checkAssertion((esitoCreazione.getErrori() == null || esitoCreazione.getErrori().isEmpty()), "La creazione del documento non è andata a buon fine.");

//		STEP 2
//		Il CORRIERE ULAV lo assegna a BRIAMONTE che se lo ritrova in DA LAVORARE

		final EsitoOperazioneDTO esitoAssegnazione = assegnaDaCorriere(USERNAME_BRIAMONTE, esitoCreazione.getDocumentTitle(), USERNAME_BRIAMONTE);
		checkAssertion(esitoAssegnazione.getFlagEsito(), "L'assegnazione del corriere non è andata a buon fine.");

//		STEP 3
//		BRIAMONTE predispone la risposta

		final String dtIngresso = esitoCreazione.getDocumentTitle();
		final Integer idDocIngresso = esitoCreazione.getNumeroDocumento();

		final MasterDocumentRedDTO masterIngresso = getDocumentForDT(utente, dtIngresso, idDocIngresso);

		checkAssertion(masterIngresso != null, DOCUMENTO_NON_TROVATO);
		final DetailDocumentRedDTO detail = predisponiRispondiIngresso(utente, masterIngresso);
		LOGGER.info("#####INGRESSO : DT > " + dtIngresso + NUM + idDocIngresso);

//		STEP 4
//		BRIAMONTE finalizza la risposta fornendo i dati mancanti: 
//		- il content
//		- iter di firma manuale verso se stesso
//		- tab spedizione
//		- Indice di classificazoine
//		- momento protocollazione

		// Valorizzazione content
		detail.setContent(FileUtils.getFileFromInternalResources(FILENAME_PDF));
		detail.setNomeFile(FILENAME_PDF);
		detail.setMimeType(MediaType.PDF.toString());

		// Valorizzazione iter di firma
		detail.setIdIterApprovativo(IterApprovativoDTO.ITER_FIRMA_MANUALE);
		final List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneFirma(ID_UFFICIO_DIREZIONE_ULAV, ID_FRANCESCO_BRIAMONTE, "FRANCESCO", "BRIAMONTE"));
		detail.setAssegnazioni(assegnazioni);

		// Valorizzare Tab spedizione
		if (detail.getDestinatari() != null && !detail.getDestinatari().isEmpty()
				&& MezzoSpedizioneEnum.ELETTRONICO.equals(detail.getDestinatari().get(0).getMezzoSpedizioneEnum())) {
			final EmailDTO mailSpedizione = new EmailDTO();
			mailSpedizione.setMittente(MITTENTE_MAIL_TEST);
			mailSpedizione.setDestinatari(DESTINATARIO_MAIL_TEST);
			mailSpedizione.setOggetto(OGGETTO_TEST + detail.getOggetto());
			mailSpedizione.setTesto(TESTO_MAIL_TEST);
			detail.setMailSpedizione(mailSpedizione);
		}

		// Valorizzare Indice di classificazione
		detail.setIndiceClassificazioneFascicoloProcedimentale("A");

		// Protocollazione standard
		detail.setMomentoProtocollazioneEnum(MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD);

		// finalizza creazione documento uscita

		final EsitoSalvaDocumentoDTO esitoSalvataggioRisposta = salvaDocumento(detail, utente);

		dumpCreazioneDocumento(esitoSalvataggioRisposta);
		checkAssertion((esitoSalvataggioRisposta.getErrori() == null || esitoSalvataggioRisposta.getErrori().isEmpty()), ERROR_CREAZIONE_USCITA_IN_RISPOSTA_MSG);

		final String dtUscita = esitoSalvataggioRisposta.getDocumentTitle();
		final Integer idDocUscita = esitoSalvataggioRisposta.getNumeroDocumento();

		LOGGER.info(USCITA_DT + dtUscita + NUM + idDocUscita);

		final RecuperoCodeDTO codeIngresso = getQueues(utente.getUsername(), dtIngresso, masterIngresso.getClasseDocumentale(), masterIngresso.getIdCategoriaDocumento());
		RecuperoCodeDTO codeUscita = getQueues(utente.getUsername(), dtUscita, masterIngresso.getClasseDocumentale(), CategoriaDocumentoEnum.USCITA.getIds()[0]);

		LOGGER.info(VERIFICA_CODE_INGRESSO);
		checkAssertionCode(codeIngresso, DocumentQueueEnum.ASSEGNATE);

		LOGGER.info(VERIFICA_CODE_USCITA);
		checkAssertionCode(codeUscita, DocumentQueueEnum.NSD);

		// verifica storico ingresso

		checkStorico(utente, detail.getWobNumberSelected(), dtIngresso, EventTypeEnum.PREDISPOSIZIONE_RISPOSTA);

//		STEP 5
//		BRIAMONTE firma il documento

		final EsitoOperazioneDTO esito = sign(utente, esitoSalvataggioRisposta.getWobNumber());
		checkAssertion(esito.getFlagEsito(), "Errore in fase di firma.");

		codeUscita = getQueues(utente.getUsername(), dtUscita, masterIngresso.getClasseDocumentale(), CategoriaDocumentoEnum.USCITA.getIds()[0]);
		checkAssertionCode(codeUscita, DocumentQueueEnum.SPEDIZIONE, DocumentQueueEnum.PROCEDIMENTI);

		checkStorico(utente, esitoSalvataggioRisposta.getWobNumber(), dtUscita, EventTypeEnum.FIRMATO);
	}

	/**
	 * Test inoltro.
	 */
	@Test
	public final void inoltra() {

//		STEP 1
//		BRIAMONTE crea ed assegna al CORRIERE dell'ULAV

		final UtenteDTO utente = getUtente(USERNAME_BRIAMONTE);

		final EsitoSalvaDocumentoDTO esitoCreazione = createDoc(utente, ID_TIPO_DOC_JU_E, ID_TIPO_PROC_JU_E);

		dumpCreazioneDocumento(esitoCreazione);
		checkAssertion((esitoCreazione.getErrori() == null || esitoCreazione.getErrori().isEmpty()), "La creazione del documento non è andata a buon fine.");

//		STEP 2
//		Il CORRIERE ULAV lo assegna a BRIAMONTE che se lo ritrova in DA LAVORARE

		final EsitoOperazioneDTO esitoAssegnazione = assegnaDaCorriere(USERNAME_BRIAMONTE, esitoCreazione.getDocumentTitle(), USERNAME_BRIAMONTE);
		checkAssertion(esitoAssegnazione.getFlagEsito(), "L'assegnazione del corriere non è andata a buon fine.");

//		STEP 3
//		BRIAMONTE predispone la risposta

		final String dtIngresso = esitoCreazione.getDocumentTitle();
		final Integer idDocIngresso = esitoCreazione.getNumeroDocumento();

		final MasterDocumentRedDTO masterIngresso = getDocumentForDT(utente, dtIngresso, idDocIngresso);

		checkAssertion(masterIngresso != null, DOCUMENTO_NON_TROVATO);
		final DetailDocumentRedDTO detail = predisponiInoltraIngresso(utente, masterIngresso);
		LOGGER.info("#####INGRESSO : DT > " + dtIngresso + NUM + idDocIngresso);

//		STEP 4
//		BRIAMONTE finalizza la risposta fornendo i dati mancanti: 
//		- destinatari
//		- il content
//		- iter di firma manuale verso se stesso
//		- tab spedizione
//		- Indice di classificazoine
//		- momento protocollazione

		final List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		final DestinatarioRedDTO destRossi = createDestinatarioInterno(ID_UFFICIO_DIREZIONE_ULAV, ID_MARIANNA_BUONGIORNO, ID_CONTATTO_IRENE, ModalitaDestinatarioEnum.TO);
		destinatari.add(destRossi);
		detail.setDestinatari(destinatari);

		// Valorizzazione content
		detail.setContent(FileUtils.getFileFromInternalResources(FILENAME_PDF));
		detail.setNomeFile(FILENAME_PDF);
		detail.setMimeType(MediaType.PDF.toString());

		// Valorizzazione iter di firma
		detail.setIdIterApprovativo(IterApprovativoDTO.ITER_FIRMA_MANUALE);
		final List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneFirma(ID_UFFICIO_DIREZIONE_ULAV, ID_FRANCESCO_BRIAMONTE, "FRANCESCO", "BRIAMONTE"));
		detail.setAssegnazioni(assegnazioni);

		// Valorizzare Tab spedizione
		if (detail.getDestinatari() != null && !detail.getDestinatari().isEmpty()
				&& MezzoSpedizioneEnum.ELETTRONICO.equals(detail.getDestinatari().get(0).getMezzoSpedizioneEnum())) {
			final EmailDTO mailSpedizione = new EmailDTO();
			mailSpedizione.setMittente(MITTENTE_MAIL_TEST);
			mailSpedizione.setDestinatari(DESTINATARIO_MAIL_TEST);
			mailSpedizione.setOggetto(OGGETTO_TEST + detail.getOggetto());
			mailSpedizione.setTesto(TESTO_MAIL_TEST);
			detail.setMailSpedizione(mailSpedizione);
		}

		// Valorizzare Indice di classificazione
		detail.setIndiceClassificazioneFascicoloProcedimentale("A");

		// Protocollazione standard
		detail.setMomentoProtocollazioneEnum(MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD);

		// finalizza creazione documento uscita

		final EsitoSalvaDocumentoDTO esitoSalvataggioRisposta = salvaDocumento(detail, utente);
		dumpCreazioneDocumento(esitoSalvataggioRisposta);
		checkAssertion((esitoSalvataggioRisposta.getErrori() == null || esitoSalvataggioRisposta.getErrori().isEmpty()), ERROR_CREAZIONE_USCITA_IN_RISPOSTA_MSG);

		final String dtUscita = esitoSalvataggioRisposta.getDocumentTitle();
		final Integer idDocUscita = esitoSalvataggioRisposta.getNumeroDocumento();
		final String wn = esitoSalvataggioRisposta.getWobNumber();

		LOGGER.info(USCITA_DT + dtUscita + NUM + idDocUscita);

		final RecuperoCodeDTO codeIngresso = getQueues(utente.getUsername(), dtIngresso, masterIngresso.getClasseDocumentale(), masterIngresso.getIdCategoriaDocumento());
		RecuperoCodeDTO codeUscita = getQueues(utente.getUsername(), dtUscita, masterIngresso.getClasseDocumentale(), CategoriaDocumentoEnum.USCITA.getIds()[0]);

		LOGGER.info(VERIFICA_CODE_INGRESSO);
		checkAssertionCode(codeIngresso, DocumentQueueEnum.ASSEGNATE);

		LOGGER.info(VERIFICA_CODE_USCITA);
		checkAssertionCode(codeUscita, DocumentQueueEnum.NSD);

		// verifica storico ingresso

		checkStorico(utente, null, dtIngresso, EventTypeEnum.PREDISPOSIZIONE_INOLTRO);

//		STEP 5
//		BRIAMONTE firma il documento

		final EsitoOperazioneDTO esito = sign(utente, wn);
		checkAssertion(esito.getFlagEsito(), "Errore in fase di firma.");

		codeUscita = getQueues(utente.getUsername(), dtUscita, masterIngresso.getClasseDocumentale(), CategoriaDocumentoEnum.USCITA.getIds()[0]);
		checkAssertionCode(codeUscita, DocumentQueueEnum.RISPOSTA);

		checkStorico(utente, null, dtUscita, EventTypeEnum.FIRMATO);
	}

	/**
	 * Test predisposizione documento RGS.
	 */
	@Test
	public final void predisponiDocumentoRGS() {

		// STEP1: BIAGIO.MAZZOTTA crea documento ed assegno per competenza ad
		// IGF.UFFICIO1
		final UtenteDTO utenteMazzotta = getUtente(USERNAME_MAZZOTTA);
		final Boolean daNonProtocollare = false;

		final Integer idMezzoRicezione = ID_MEZZO_RICEZIONE_POSTA_ELET_DA_PEC_PER_ELETTRONICI;

		final List<AssegnazioneDTO> assegnazioniIngresso = new ArrayList<>();
		assegnazioniIngresso.add(createAssegnazioneCompetenzaUfficio(ID_IGF_UFFICIO_I, DESC_IGF_UFFICIO_I));

		final String indiceFascicoloProcedimentale = "A";
		final Boolean riservato = false;

		final String oggetto = createObj();
		final List<AllegatoDTO> allegati = null;
		final EsitoSalvaDocumentoDTO esitoCreazione = createDocIngresso(allegati, oggetto, utenteMazzotta, ID_CONTATTO_IRENE, FormatoDocumentoEnum.ELETTRONICO,
				ID_TIPO_DOC_GENERICO_RGS_ENTRATA, ID_TIPO_PROC_GENERICO_RGS_ENTRATA, assegnazioniIngresso, daNonProtocollare, idMezzoRicezione, indiceFascicoloProcedimentale,
				riservato);
		checkAssertion(esitoCreazione.isEsitoOk(), "La creazione del documento non è andato a buon fine.");

		dumpCreazioneDocumento(esitoCreazione);

		final String documentTitle = esitoCreazione.getDocumentTitle();

		// STEP2: ANDREANI, uno dei corrieri di IGF UFFICIO1, assegna il documento ad
		// ANDREANI
		final EsitoOperazioneDTO esitoAssegnazione = assegnaDaCorriere(USERNAME_ANDREANI, documentTitle, USERNAME_ANDREANI);
		checkAssertion(esitoAssegnazione.getFlagEsito(), "L'assegnazione del corriere non è andata a buon fine");

		// STEP3: ANDREANI predispone il documento in uscita sulla base dell'ingresso e
		// lo salva assegnandolo in firma a MAZZOTTA

		final UtenteDTO utenteAndreani = getUtente(USERNAME_ANDREANI);

		final MasterDocumentRedDTO master = getDocumentForDT(utenteAndreani, esitoCreazione.getDocumentTitle(), esitoCreazione.getNumeroDocumento());

		RecuperoCodeDTO codeIngressoAndreani = getQueues(utenteAndreani.getUsername(), esitoCreazione.getDocumentTitle(), master.getClasseDocumentale(),
				master.getIdCategoriaDocumento());

		LOGGER.info(VERIFICA_CODE_INGRESSO);
		checkAssertionCode(codeIngressoAndreani, DocumentQueueEnum.ASSEGNATE, DocumentQueueEnum.DA_LAVORARE);

		checkAssertion(master != null, DOCUMENTO_NON_TROVATO);
		final DetailDocumentRedDTO detail = predisponiDocumento(utenteAndreani, master);

		// Valorizzazione content
		detail.setContent(FileUtils.getFileFromInternalResources(FILENAME_PDF));
		detail.setNomeFile(FILENAME_PDF);
		detail.setMimeType(MediaType.PDF.toString());

		// Valorizzazione iter di firma
		detail.setIdIterApprovativo(IterApprovativoDTO.ITER_FIRMA_MANUALE);
		final List<AssegnazioneDTO> assegnazioniUscita = new ArrayList<>();
		assegnazioniUscita.add(createAssegnazioneFirma(ID_IGIT_UFFICIO_XI, ID_LUIGI_MINNIELLI, "LUIGI", "MINNIELLI"));
		detail.setAssegnazioni(assegnazioniUscita);

		// Valorizzare Tab spedizione
		if (detail.getDestinatari() != null && !detail.getDestinatari().isEmpty()
				&& MezzoSpedizioneEnum.ELETTRONICO.equals(detail.getDestinatari().get(0).getMezzoSpedizioneEnum())) {
			final EmailDTO mailSpedizione = new EmailDTO();
			mailSpedizione.setMittente(MITTENTE_MAIL_TEST);
			mailSpedizione.setDestinatari(DESTINATARIO_MAIL_TEST);
			mailSpedizione.setOggetto(OGGETTO_TEST + detail.getOggetto());
			mailSpedizione.setTesto(TESTO_MAIL_TEST);
			detail.setMailSpedizione(mailSpedizione);
		}

		// Valorizzare Indice di classificazione
		detail.setIndiceClassificazioneFascicoloProcedimentale("A");

		// Protocollazione standard
		detail.setMomentoProtocollazioneEnum(MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD);

		detail.setIdTipologiaDocumento(ID_TIPO_DOC_GENERICO_RGS_USCITA);
		detail.setIdTipologiaProcedimento(ID_TIPO_PROC_GENERICO_RGS_USCITA);

		for (final RispostaAllaccioDTO ra : detail.getAllacci()) {
			ra.setDocumentoDaChiudere(true);
		}
		// finalizza creazione documento uscita

		final EsitoSalvaDocumentoDTO esitoSalvataggioRisposta = salvaDocumento(detail, utenteAndreani);

		dumpCreazioneDocumento(esitoSalvataggioRisposta);
		checkAssertion((esitoSalvataggioRisposta.getErrori() == null || esitoSalvataggioRisposta.getErrori().isEmpty()), ERROR_CREAZIONE_USCITA_IN_RISPOSTA_MSG);

		final String dtUscita = esitoSalvataggioRisposta.getDocumentTitle();
		final Integer idDocUscita = esitoSalvataggioRisposta.getNumeroDocumento();

		LOGGER.info(USCITA_DT + dtUscita + NUM + idDocUscita);

		final UtenteDTO utenteMinnielli = getUtente(USERNAME_MINNIELLI);

		// Avendo chiesto la chiusura degli allacci il documento in ingresso sarà in
		// ASSEGNATE
		codeIngressoAndreani = getQueues(utenteAndreani.getUsername(), esitoCreazione.getDocumentTitle(), master.getClasseDocumentale(), master.getIdCategoriaDocumento()); // ASSEGNATE
		final RecuperoCodeDTO codeUscitaMinnielli = getQueues(utenteMinnielli.getUsername(), dtUscita, master.getClasseDocumentale(),
				CategoriaDocumentoEnum.USCITA.getIds()[0]); // NSD
		LOGGER.info(VERIFICA_CODE_INGRESSO);
		checkAssertionCode(codeIngressoAndreani, DocumentQueueEnum.ASSEGNATE);

		LOGGER.info(VERIFICA_CODE_USCITA);
		checkAssertionCode(codeUscitaMinnielli, DocumentQueueEnum.NSD);

	}

}
