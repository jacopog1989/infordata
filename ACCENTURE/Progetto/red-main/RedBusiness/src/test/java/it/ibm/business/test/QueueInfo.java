package it.ibm.business.test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import filenet.vw.api.VWException;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.QueueGroupEnum;
import it.ibm.red.business.enums.StatoLavorazioneEnum;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.DateUtils;

/**
 * Model di una coda.
 */
public class QueueInfo {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(QueueInfo.class.getName());

	/**
	 * Nome coda.
	 */
	private String queueName;

	/**
	 * Nome wf.
	 */
	private String workflowName;

	/**
	 * Id wf.
	 */
	private String wobNumber;

	/**
	 * Lista response.
	 */
	private String[] responses;

	/**
	 * Id doc.
	 */
	private Integer idDocumento;

	/**
	 * Descrizione utente destinatario.
	 */
	private String descUtenteDestinatario;

	/**
	 * Descrizione ufficio destinatario.
	 */
	private String descNodoDestinatario;

	/**
	 * Id utente dest.
	 */
	private Integer idUtenteDestinatario;

	/**
	 * Id nodo destinatario.
	 */
	private Integer idNodoDestinatario;

	/**
	 * Id stato lavorazione.
	 */
	private Integer idStatoLavorazione;

	/**
	 * Data creazione.
	 */
	private String dataCreazione;

	/**
	 * Costruttore.
	 * @param wf workflow
	 */
	public QueueInfo(final VWWorkObject wf) {
		try {
			queueName = wf.getCurrentQueueName();
			workflowName = wf.getWorkflowName();
			wobNumber = wf.getWorkObjectNumber();
			responses = wf.getStepResponses();
			idDocumento = (Integer) TrasformerPE.getMetadato(wf, "idDocumento");
			idUtenteDestinatario = (Integer) TrasformerPE.getMetadato(wf, "idUtenteDestinatario");
			idNodoDestinatario = (Integer) TrasformerPE.getMetadato(wf, "idNodoDestinatario");
			dataCreazione = DateUtils.dateToString((Date) wf.getFieldValue("F_CreateTime"), DateUtils.DD_MM_YYYY);
		} catch (VWException e) {
			LOGGER.error(e);
		}
	}

	/**
	 * Costruttore.
	 * @param idStatoLavorazione - id dello stato lavorazione
	 * @param descNodoDestinatario - descrizione del nodo destinatario
	 * @param descUtenteDestinatario - descrizione dell'utente destinatario
	 */
	public QueueInfo(final Integer idStatoLavorazione, final String descNodoDestinatario, final String descUtenteDestinatario) {
		this.descNodoDestinatario = descNodoDestinatario;
		this.descUtenteDestinatario = descUtenteDestinatario;
		this.idStatoLavorazione = idStatoLavorazione;
		StatoLavorazioneEnum sle = StatoLavorazioneEnum.get(idStatoLavorazione.longValue());
		QueueGroupEnum group = QueueGroupEnum.UFFICIO;
		if (idUtenteDestinatario != null) {
			group = QueueGroupEnum.UTENTE;
		}
		List<DocumentQueueEnum> coda = DocumentQueueEnum.getQueue(sle, group);
		queueName = coda.get(0).getDisplayName();
	}
	
	/**
	 * @see java.lang.Object#toString().
	 */
	@Override
	public String toString() {
		return "QueueInfo [queueName=" + queueName + ", workflowName=" + workflowName + ", wobNumber=" + wobNumber
				+ ", responses=" + Arrays.toString(responses) + ", idDocumento=" + idDocumento
				+ ", descUtenteDestinatario=" + descUtenteDestinatario + ", descNodoDestinatario=" + descNodoDestinatario
				+ ", idUtenteDestinatario=" + idUtenteDestinatario + ", idNodoDestinatario=" + idNodoDestinatario
				+ ", dataCreazione=" + dataCreazione
				+ ", idStatoLavorazione=" + idStatoLavorazione + "]";
	}

	/**
	 * Recupera la descrizione dell'utente destinatario.
	 * @return descrizione dell'utente destinatario
	 */
	public String getDescUtenteDestinatario() {
		return descUtenteDestinatario;
	}

	/**
	 * Recupera la descrizione del nodo destinatario.
	 * @return descrizione del nodo destinatario
	 */
	public String getDescNodoDestinatario() {
		return descNodoDestinatario;
	}
	
	/**
	 * Recupera il nome della coda.
	 * @return nome della coda
	 */
	public String getQueueName() {
		return queueName;
	}

	/**
	 * Recupera il nome del workflow.
	 * @return nome del workflow
	 */
	public String getWorkflowName() {
		return workflowName;
	}

	/**
	 * Recupera il wobNumber.
	 * @return wobNumber
	 */
	public String getWobNumber() {
		return wobNumber;
	}

	/**
	 * Recupera le response.
	 * @return array di response
	 */
	public String[] getResponses() {
		return responses;
	}

	/**
	 * Recupera l'id dell'utente destinatario.
	 * @return id dell'utente destinatario
	 */
	public Integer getIdUtenteDestinatario() {
		return idUtenteDestinatario;
	}

	/**
	 * Recupera l'id del nodo destinatario.
	 * @return id del nodo destinatario
	 */
	public Integer getIdNodoDestinatario() {
		return idNodoDestinatario;
	}

	/**
	 * Recupera l'id del documento.
	 * @return id del documento
	 */
	public Integer getIdDocumento() {
		return idDocumento;
	}

	/**
	 * Recupera l'id dello stato lavorazione.
	 * @return id dello stato lavorazione
	 */
	public Integer getIdStatoLavorazione() {
		return idStatoLavorazione;
	}
}
