package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoStrutturaNodoEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Classe di test richiesta visti.
 */
public class RichiestaVistiTest extends VerificatoTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RichiestaVistiTest.class);
	
	/**
	 * Messaggio errore riscontrato durante la richiesta visti.
	 */
	private static final String ERROR_RICHIESTA_VISTI_MSG = "Errore in fase di richiesta visti.";
	
	/**
	 * Messaggio errore creazione documento uscita.
	 */
	private static final String ERROR_CREAZIONE_DOC_USCITA_MSG = "Errore in fase di creazione documento in uscita.";
	
	/**
	 * Label num doc.
	 */
	private static final String NUM_DOC = "NUM DOC : ";
	
	/**
	 * Label oggetto.
	 */
	private static final String OGGETTO = "OGGETTO : ";
	
	/**
	 * Label document title.
	 */
	private static final String DOCUMENT_TITLE = "DOCUMENT TITLE : ";

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue la azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test richiesta visti.
	 */
	@Test
	public final void test1() {
		test1Protected();
	}
	
	/**
	 * Test richiesta visti.
	 */
	@Test
	public final void test2() {
		test2Protected();
	}
	
	/**
	 * Test richiesta visti.
	 */
	@Test
	public final void test3() {
		test3Protected();
	}

	/**
	 * Test richiesta visti.
	 */
	@Test
	public final void test4() {
		UtenteDTO utente = getUtente(USERNAME_TANZI);
		String oggetto = createObj();
		String indiceFascicoloProcedimentale = "A";
		Boolean bRiservato = false;

		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(createDestinatarioEsterno(MezzoSpedizioneEnum.CARTACEO, ID_CONTATTO_GIANCARLO_ROSSI,
				ModalitaDestinatarioEnum.TO));

		List<AssegnazioneDTO> assegnazioni = null;
		List<AllegatoDTO> allegati = null;

		EsitoSalvaDocumentoDTO esitoUscita = createDocUscita(allegati, false, oggetto, utente,
				ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, assegnazioni, destinatari,
				indiceFascicoloProcedimentale, MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD,
				FIRMA_DIRIGENTE_RGS, bRiservato, null);
		checkAssertion(esitoUscita.isEsitoOk(), ERROR_CREAZIONE_DOC_USCITA_MSG);

		String dt = esitoUscita.getDocumentTitle();
		Integer numDoc = getNumDoc(USERNAME_TANZI, dt);

		waitSeconds(30);

		List<AssegnazioneDTO> assegnazioneVistoList = new ArrayList<>();
		AssegnazioneDTO assUffVisto = new AssegnazioneDTO(null, TipoAssegnazioneEnum.VISTO,
				TipoStrutturaNodoEnum.SETTORE, null, null, null, null, new UfficioDTO(ID_IGB.longValue(), null));
		assegnazioneVistoList.add(assUffVisto);

		EsitoOperazioneDTO esitoRichiestaVisti = richiestaVisti(USERNAME_TANZI, dt, assegnazioneVistoList);
		checkAssertion(esitoRichiestaVisti.isEsito(), ERROR_RICHIESTA_VISTI_MSG);

		EsitoOperazioneDTO esitoRichiestaVistoIntero = richiediVistoInterno(USERNAME_TANZI, dt, ID_IGF_UFFICIO_I);
		checkAssertion(esitoRichiestaVistoIntero.isEsito(), "Errore in fase di richiesta visti interno.");

		EsitoOperazioneDTO esitoRichiestaVisto = richiediVisto(USERNAME_CORBO, dt, USERNAME_CORBO);
		checkAssertion(esitoRichiestaVisto.isEsito(), "Errore in fase di richiesta visto.");

		EsitoOperazioneDTO esitoRichiediVerifica = richiediVerifica(USERNAME_CORBO, dt, ID_IGF_UFFICIO_I,
				ID_ADRIANA_ANDREANI);
		checkAssertion(esitoRichiediVerifica.isEsito(), "Errore in fase di richiedi verifica.");

		EsitoOperazioneDTO esitoVerificato = verificato(USERNAME_ANDREANI, dt);
		checkAssertion(esitoVerificato.isEsito(), "Errore in fase di verificato.");

		LOGGER.info(DOCUMENT_TITLE + dt);
		LOGGER.info(OGGETTO + oggetto);
		LOGGER.info(NUM_DOC + numDoc);
	}

	/**
	 * Test chiara richiesta visti.
	 */
	@Test
	public final void testChiaraRichiestaVisti2() {
		UtenteDTO utente = getUtente(USERNAME_MARCHIAN);
		String oggetto = createObj();
		String indiceFascicoloProcedimentale = "A";
		Boolean bRiservato = false;

		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(createDestinatarioEsterno(MezzoSpedizioneEnum.CARTACEO, ID_CONTATTO_GIANCARLO_ROSSI,
				ModalitaDestinatarioEnum.TO));

		List<AllegatoDTO> allegati = null;

		// Aggiungere competenza SERGIO MARCHIANE di ufficio X
		List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni
				.add(createAssegnazioneCompetenzaUtente(ID_IGICS_UFFICIO_IV, ID_SERGIO_MARCHIAN, "SERGIO", "MARCHIAN"));
		EsitoSalvaDocumentoDTO esitoUscita = createDocUscita(allegati, false, oggetto, utente,
				ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, assegnazioni, destinatari,
				indiceFascicoloProcedimentale, MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD,
				IterApprovativoDTO.ITER_FIRMA_MANUALE, bRiservato, null);
		String dt = esitoUscita.getDocumentTitle();
		checkAssertion(esitoUscita.isEsitoOk(), ERROR_CREAZIONE_DOC_USCITA_MSG);

		waitSeconds(30);

		List<AssegnazioneDTO> assegnazioneVistoList = new ArrayList<>();
		AssegnazioneDTO assUffVisto = new AssegnazioneDTO(null, TipoAssegnazioneEnum.VISTO,
				TipoStrutturaNodoEnum.SETTORE, null, null, null, null, new UfficioDTO(ID_IGICS_UFFICIO_IV.longValue(), null));
		assegnazioneVistoList.add(assUffVisto);

		// Marchian fa richiesta visti ad igics ufficio IV

		EsitoOperazioneDTO esitoRichiestaVisti = richiestaVistiDaLavorare(USERNAME_MARCHIAN, dt, assegnazioneVistoList);
		checkAssertion(esitoRichiestaVisti.isEsito(), ERROR_RICHIESTA_VISTI_MSG);
	}

}
