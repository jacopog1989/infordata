package it.ibm.business.test.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.business.test.dao.IBonificaDAO;
import it.ibm.business.test.dto.BonificaMessaAgliAttiDTO;
import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * The Class BonificaDAO.
 *
 * @author a.dilegge
 * 
 * 	Dao per la gestione delle bonifiche.
 */
@Repository
public class BonificaDAO extends AbstractDAO implements IBonificaDAO {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 825657946267375095L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(BonificaDAO.class.getName());

	/**
	 * @see it.ibm.business.test.dao.IBonificaDAO#recuperaItemsDaBonificare(java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public List<BonificaMessaAgliAttiDTO> recuperaItemsDaBonificare(final Integer numMax, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<BonificaMessaAgliAttiDTO> list = null;
		
		try {
			ps = con.prepareStatement(
				"SELECT * FROM APPO_BONIFICA_ATTI_NPS WHERE STATO = 0 ORDER BY IDPROTOCOLLO");
			
			ps.setMaxRows(numMax);
			rs = ps.executeQuery();
			
			list = new ArrayList<>();
			while (rs.next()) {
				list.add(new BonificaMessaAgliAttiDTO(
						rs.getLong("IDUTENTE"), 
						rs.getLong("IDNODO"), 
						getDateWithTime(rs, "TIMESTAMPOPERAZIONE"),
						rs.getString("IDPROTOCOLLO"), 
						rs.getInt("NUMEROPROTOCOLLO"), 
						rs.getInt("ANNOPROTOCOLLO"),
						rs.getDate("DATAPROTOCOLLO"),
						rs.getString("FLAG_IU"),
						rs.getString("DOCUMENTTITLE")));
			}
			
			return list;
		} catch (final SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}
	
	private static Date getDateWithTime(final ResultSet resultSet, final String attribute) throws SQLException {
		Date out = null;
		final Timestamp timestamp = resultSet.getTimestamp(attribute);
		if (timestamp != null) {
			out = new java.util.Date(timestamp.getTime());
		}
		return out;
	}

	/**
	 * @see it.ibm.business.test.dao.IBonificaDAO#aggiornaItemBonificaProtocolliAgliAtti(java.lang.String,
	 *      java.lang.Integer, java.lang.String, java.sql.Connection).
	 */
	@Override
	public void aggiornaItemBonificaProtocolliAgliAtti(final String idProtocollo, final Integer stato, final String descrizione, final Connection conn) {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("UPDATE APPO_BONIFICA_ATTI_NPS SET STATO = ?, DESCRIZIONE = ? WHERE IDPROTOCOLLO = ?");
			int index = 1;
			
			ps.setInt(index++, stato);
			ps.setString(index++, descrizione);
			ps.setString(index++, idProtocollo);
			ps.executeUpdate();
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'aggiornamento dell'item con id = " + idProtocollo, e);
			throw new RedException("Errore durante l'aggiornamento dell'item con id = " + idProtocollo, e);
		} finally {
			closeStatement(ps);
		}
		
	}

}