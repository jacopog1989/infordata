package it.ibm.business.test.suite;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.net.MediaType;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.HierarchicalFileWrapperDTO;
import it.ibm.red.business.dto.NamedStreamDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.TipoFile;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IProtocollaDsrFacadeSRV;

/**
 * Classe di test della protocollazione mail DSR.
 */
public class DsrProtocollaMailTest extends AbstractTest {

	/**
	 * Servizio.
	 */
	private IProtocollaDsrFacadeSRV protocollaDsr;

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		protocollaDsr = ApplicationContextProvider.getApplicationContext().getBean(IProtocollaDsrFacadeSRV.class);

	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Protocolla DSR test 1.
	 */
	@Test
	public final void test1() {

		// Creazione documento
		final UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		final Contatto contatto = getContatto(ID_CONTATTO_IRENE);
		final List<HierarchicalFileWrapperDTO> emptyList = new ArrayList<>();
		final HierarchicalFileWrapperDTO principale = new HierarchicalFileWrapperDTO();
		principale.setNodeId("E0F01266-0000-CC1F-B93E-997ABD9ABCE2\\0-&-{854BACF9-DB60-437E-B09E-E226C82E95C1}", 0);
		final NamedStreamDTO file = new NamedStreamDTO();
		file.setName("DSR.pdf");
		final TipoFile tf = new TipoFile(null, "PDF", MediaType.PDF.toString(), null, null);
		file.setTipoFile(tf);
		principale.setFile(file);
		final boolean notificaProtocolla = false;
		final DetailDocumentRedDTO detailDocument = protocollaDsr.createDetailFromMail(utente, contatto, emptyList, principale, notificaProtocolla,
				"E0F01266-0000-CC1F-B93E-997ABD9ABCE2", "per test dsr ");
		final EsitoSalvaDocumentoDTO esito = protocollaDsr.protocollaMailDsr(utente, detailDocument);

		assertTrue(esito.isEsitoOk());
	}
}