package it.ibm.business.test.suite;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.CountPer;
import it.ibm.red.business.dto.DocumentoScadenzatoDTO;
import it.ibm.red.business.dto.PEDocumentoScadenzatoDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PeriodoScadenzaEnum;
import it.ibm.red.business.enums.ScopeScadenziarioStrutturaEnum;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IScadenziaroStrutturaFacadeSRV;

/**
 * Classe di test struttura scadenziario.
 */
public class ScadenziarioStrutturaTest extends AbstractTest {

	/**
	 * Servizio.
	 */
	private IScadenziaroStrutturaFacadeSRV scadenziarioSRV;
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public void inizializzaTest() {
		before();
		scadenziarioSRV = ApplicationContextProvider.getApplicationContext().getBean(IScadenziaroStrutturaFacadeSRV.class);
	}
	
	/**
	 * Test uffici da Ragioneria.
	 */
	@Test
	public void testUfficiDaRagioneria() {
		UtenteDTO utente = getUtente(USERNAME_RAGIONIERE);
		Map<CountPer<UfficioDTO>, List<CountPer<UfficioDTO>>> mapRes = scadenziarioSRV.retrieveCountDocumentoInScadenzaPerUfficiDaSegreteria(utente);
		Assert.assertNotNull(mapRes);
		Assert.assertFalse(mapRes.keySet().isEmpty());
	}
	
	/**
	 * Test uffici da Ispettorato.
	 */
	@Test
	public void testUfficiDaIspettorato() {
		UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		List<CountPer<UfficioDTO>> mapRes = scadenziarioSRV.retrieveCountDocumentoInScadenzaPerUfficiDaIspettorato(utente);
		Assert.assertNotNull(mapRes);
		Assert.assertFalse(mapRes.isEmpty());
	}
	
	/**
	 * Test recupero documento in scadenza in periodo scadenza.
	 */
	@Test
	public void testretrieveDocumentoInScadenzaPerPeriodoScadenza() {
		UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		Long idUfficio = utente.getIdUfficio();
		Map<CountPer<PeriodoScadenzaEnum>, List<PEDocumentoScadenzatoDTO>> mapRes = scadenziarioSRV.retrieveDocumentoInScadenzaPerPeriodoScadenza(idUfficio, utente);
		Assert.assertNotNull(mapRes);
		Assert.assertFalse(mapRes.isEmpty());
	}
	
	/**
	 * Test recupero scope scadenziario struttura.
	 */
	@Test
	public void testgetScopeScadenziarioStruttura() {
		UtenteDTO mazzotta = getUtente(USERNAME_MAZZOTTA);
		ScopeScadenziarioStrutturaEnum ispettorato = scadenziarioSRV.getScopeScadenziarioStruttura(mazzotta);
		Assert.assertEquals(ispettorato, ScopeScadenziarioStrutturaEnum.ISPETTORATO);
		
		UtenteDTO ragioniere = getUtente(USERNAME_RAGIONIERE);
		ScopeScadenziarioStrutturaEnum segreteria = scadenziarioSRV.getScopeScadenziarioStruttura(ragioniere);
		Assert.assertEquals(segreteria, ScopeScadenziarioStrutturaEnum.SEGRETERIA);
		
		UtenteDTO rossi = getUtente(USERNAME_ROSSI);
		ScopeScadenziarioStrutturaEnum ufficio = scadenziarioSRV.getScopeScadenziarioStruttura(rossi);
		Assert.assertEquals(ufficio, ScopeScadenziarioStrutturaEnum.UFFICIO);
	}
	
	/**
	 * Test recupero id documento.
	 */
	@Test
	public void  retrieveIdDocumentoByPeIdDocumentTest() {
		UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		Long idUfficio = utente.getIdUfficio();
		List<DocumentoScadenzatoDTO> resList = null;
		
		Map<CountPer<PeriodoScadenzaEnum>, List<PEDocumentoScadenzatoDTO>> mapRes = scadenziarioSRV.retrieveDocumentoInScadenzaPerPeriodoScadenza(idUfficio, utente);
		for (Entry<CountPer<PeriodoScadenzaEnum>, List<PEDocumentoScadenzatoDTO>> entry : mapRes.entrySet()) {
			if (CollectionUtils.isNotEmpty(entry.getValue())) {
				resList = scadenziarioSRV.retrieveIdDocumentoByPeIdDocument(entry.getValue(), utente);
				break;
			}
		}
		
		Assert.assertNotNull(resList);
		Assert.assertFalse(resList == null || resList.isEmpty());
	}
 }
