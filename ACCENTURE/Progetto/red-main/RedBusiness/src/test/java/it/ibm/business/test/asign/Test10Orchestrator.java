package it.ibm.business.test.asign;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.net.MediaType;

import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.NotificaDTO;
import it.ibm.red.business.dto.RecuperoCodeDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.helper.email.NotificheEmailHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.ICodaMailSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.asign.step.StatusEnum;
import it.ibm.red.business.service.asign.step.StepEnum;
import it.ibm.red.business.service.facade.IAooFacadeSRV;
import it.ibm.red.business.utils.CollectionUtils;
import it.ibm.red.business.utils.FileUtils;

/**
 * Classe di test che compre la maggior parte delle funzionalità della firma asincrona.
 */
public class Test10Orchestrator extends AbstractAsignTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test10Orchestrator.class);

	/**
	 * Messaggio breve step sbagliato.
	 */
	private static final String WRONG_STEP = "Step errato";
	
	/**
	 * Messaggio breve stato sbagliato.
	 */
	private static final String WRONG_STATUS = "Stato errato";
	
	/**
	 * Messaggio che comunica la necessita che il numero di retry sia pari a zero.
	 */
	private static final String NUMERO_RETRY_NOT_ZERO = "Il numero di retry deve essere 0";
	
	/**
	 * Service per la gestione della coda mail.
	 */
	ICodaMailSRV codaMailSRV;
	
	/**
	 * Service per la gestione degli utenti.
	 */
	IUtenteSRV utenteSRV;
	
	/**
	 * Service per la gestione delle AOO.
	 */
	IAooFacadeSRV aooSrv;
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		codaMailSRV = ApplicationContextProvider.getApplicationContext().getBean(ICodaMailSRV.class);
		utenteSRV = ApplicationContextProvider.getApplicationContext().getBean(IUtenteSRV.class);
		aooSrv = ApplicationContextProvider.getApplicationContext().getBean(IAooFacadeSRV.class);
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test sul funzionamento completo della firma asincrona.
	 */
	@Test
	public final void play() {

		//Creiamo un documento e firmiamolo
		UtenteDTO utente = getUtente(USERNAME_RGS);
		EsitoSalvaDocumentoDTO esitoCreazione = createDocAutoassegnatoFirma(null, utente, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, null);
		String classeDoc = null;
		
		RecuperoCodeDTO codeUscita = getQueues(utente.getUsername(), esitoCreazione.getDocumentTitle(), classeDoc, CategoriaDocumentoEnum.USCITA.getIds()[0]);
		checkAssertionCode(codeUscita, DocumentQueueEnum.NSD, DocumentQueueEnum.PROCEDIMENTI, DocumentQueueEnum.RICHIAMA_DOCUMENTI);

		EsitoOperazioneDTO esito = sign(utente, esitoCreazione.getWobNumber());

		checkIsIncoda(utente, esitoCreazione);
		codeUscita = getQueues(utente.getUsername(), esitoCreazione.getDocumentTitle(), classeDoc, CategoriaDocumentoEnum.USCITA.getIds()[0]);
		checkAssertionCode(codeUscita, DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE, DocumentQueueEnum.PROCEDIMENTI, DocumentQueueEnum.RICHIAMA_DOCUMENTI);

		Long idItem = getaSignSRV().getIdItem(esito.getDocumentTitle());

		checkAssertion(idItem!=null, "Non è stato inserito alcun item");

		ASignItemDTO item = getaSignSRV().getItem(idItem);

		checkAssertion(item!=null, "Non è stato recuperato alcun item");
		checkAssertion(item.getId()!=null, "L'identificativo non può essere vuoto");
		checkAssertion(item.getnRetry()==0, NUMERO_RETRY_NOT_ZERO);
		checkAssertion(StatusEnum.PRESO_IN_CARICO.equals(item.getStatus()), "Stato iniziale errato");
		checkAssertion(StepEnum.START.equals(item.getStep()), "Step iniziale errato");

		//Impostiamo al massimo la sua priorità
		getaSignSRV().setMaxPriority(esito.getDocumentTitle());

		//Chiediamo di completare la firma
		Long playedIdItem = getOrchestrator().play();
		
		checkEndASign(utente, esitoCreazione, classeDoc, idItem, playedIdItem);

	}

	/**
	 * Esegue il test per la verifica della funzionalità di reset dei retry associati ad un item.
	 */
	@Test
	public final void resetRetry() {
		UtenteDTO utente = getUtente(USERNAME_RGS);
		EsitoSalvaDocumentoDTO esitoCreazione = createDocAutoassegnatoFirma(null, utente, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, null);

		EsitoOperazioneDTO esito = sign(utente, esitoCreazione.getWobNumber());
		String classeDoc = null;

		Long idItem = getaSignSRV().getIdItem(esito.getDocumentTitle());
		getOrchestrator().playSimulatingCrash(idItem, StepEnum.PROTOCOLLAZIONE);
		LOGGER.info("!!!!!!!!!!!"+esitoCreazione.getDocumentTitle());
		ASignItemDTO item = getaSignSRV().getItem(idItem);
		checkAssertion(item.getDocumentTitle().equals(esitoCreazione.getDocumentTitle()), "Non è stato recuperato l'item corretto");
		checkAssertion(item!=null, "Non è stato recuperato alcun item");
		checkAssertion(item.getDataUltimoRetry() == null, "La data ultima esecuzione deve essere null");
		checkAssertion(item.getnRetry()==0, NUMERO_RETRY_NOT_ZERO);
		checkAssertion(StatusEnum.RETRY.equals(item.getStatus()), WRONG_STATUS);
		checkAssertion(StepEnum.PROTOCOLLAZIONE.equals(item.getStep()), WRONG_STEP);

		Integer nMaxRetry = Integer.valueOf(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ASIGN_MAX_RETRY));
		for (int a = 0; a < nMaxRetry; a++) {
			getOrchestrator().playSimulatingCrash(idItem, StepEnum.PROTOCOLLAZIONE);
			ASignItemDTO itemRetry = getaSignSRV().getItem(idItem);
			checkAssertion(itemRetry.getnRetry() == a+1, "Contatore retry errato");
			checkAssertion(itemRetry.getDataUltimoRetry() != null, "La data di ultimo retry deve essere valorizzata");
		}
		getOrchestrator().playSimulatingCrash(idItem, StepEnum.PROTOCOLLAZIONE);
		ASignItemDTO itemLastRetry = getaSignSRV().getItem(idItem);
		//Devi essere in errore
		checkAssertion(itemLastRetry.getnRetry() == 0, "Contatore retry errato");
		checkAssertion(itemLastRetry.getDataUltimoRetry() == null, "La data di ultimo retry non deve essere valorizzata");
		checkAssertion(StatusEnum.ERRORE.equals(itemLastRetry.getStatus()), WRONG_STATUS);

		getaSignSRV().resetRetry(idItem);
		checkAssertion(item.getDataUltimoRetry() == null, "La data ultima esecuzione deve essere null");
		checkAssertion(item.getnRetry()==0, NUMERO_RETRY_NOT_ZERO);
		checkAssertion(StatusEnum.RETRY.equals(item.getStatus()), WRONG_STATUS);

		Long resumedIdItem = getOrchestrator().resume();

		checkEndASign(utente, esitoCreazione, classeDoc, idItem, resumedIdItem);

	}

	/**
	 * Test sulla funzione di rollback associata ad ogni step in caso di errore nell'esecuzione dello Step.
	 */
	@Test
	public final void rollback() {
		UtenteDTO utente = getUtente(USERNAME_RGS);
		EsitoSalvaDocumentoDTO esitoCreazione = createDocAutoassegnatoFirma(null, utente, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, null);

		EsitoOperazioneDTO esito = sign(utente, esitoCreazione.getWobNumber());

		Long idItem = getaSignSRV().getIdItem(esito.getDocumentTitle());
		StepEnum crashStep = StepEnum.PROTOCOLLAZIONE;
		Long playedIdItem = getOrchestrator().playSimulatingCrash(idItem, crashStep);
		ASignItemDTO playedItem = getaSignSRV().getItem(playedIdItem);
		checkAssertion(StatusEnum.RETRY.equals(playedItem.getStatus()), WRONG_STATUS);
		checkAssertion(StepEnum.PROTOCOLLAZIONE.equals(playedItem.getStep()), WRONG_STEP);
		
		while (crashStep != StepEnum.STOP) {
			
			Long resumedIdItem = getOrchestrator().playSimulatingCrash(idItem, crashStep);
			ASignItemDTO resumedItem = getaSignSRV().getItem(resumedIdItem);
			
			checkAssertion(resumedItem.getnRetry()==1, "Il numero di retry deve essere 1");
			checkAssertion(StatusEnum.RETRY.equals(resumedItem.getStatus()), WRONG_STATUS);
			checkAssertion(crashStep.equals(resumedItem.getStep()), WRONG_STEP);

			crashStep = StepEnum.nextStep(crashStep);
		}
		getOrchestrator().playSimulatingCrash(idItem, null);
		ASignItemDTO finishItem = getaSignSRV().getItem(idItem);
		checkAssertion(finishItem.getnRetry()==0, "Il numero di retry deve essere 1");
		checkAssertion(StatusEnum.ELABORATO.equals(finishItem.getStatus()), WRONG_STATUS);
		checkAssertion(finishItem.getDataUltimoRetry() == null, WRONG_STEP);
	}
	
	
	/**
	 * Esegue il play sull'item associato al documento con idDocumento specificato.
	 * Se l'item è in stato di RETRY l'Orchestrator eseguirà il metodo resume.
	 * */
	@Test
	public void playFromIdDocumento() {
		
		// Id del documento da lavorare
		long idDocumento = 2017;
		// Id dell'aoo dell'utente
		long idAoo = 25;
		// Step in cui si vuole simulare il crash
		StepEnum crashStep = null;

		Long idItem = getaSignSRV().getIdItemFromIdDocumento(idDocumento, idAoo);
		checkAssertion(idItem!=null, "Nessun item da lavorare associato al documento con id: " + idDocumento + " - AOO: " + idAoo);
		
		ASignItemDTO item = getaSignSRV().getItem(idItem);
		checkAssertion(!StatusEnum.ERRORE.equals(item.getStatus()), "Il documento risulta in ERRORE, non può essere processato");
		
		// Se l'item esiste e non è in errore, viene lavorato
		getOrchestrator().playSimulatingCrash(idItem, crashStep);
	}
	
	/**
	 * Simula l'esecuzione del job PlayItemASignJob.
	 */
	@Test
	public void playAllItems() {
		Long idAoo = 25l;
		
		List<Long> itemsToPlay = getaSignSRV().getPlayableItems(idAoo);
		
		for (Long item : itemsToPlay) {
			getOrchestrator().play(item);
		}
	}
	
	/**
	 * Simula l'esecuzione del job ResumeItemASignJob.
	 */
	@Test
	public void resumeAllItems() {
		Long idAoo = 25l;
		
		List<ASignItemDTO> itemsToResume = getaSignSRV().getResumableItems(idAoo);
		
		for (ASignItemDTO item : itemsToResume) {
			getOrchestrator().resume(item);
		}
	}
	
	/**
	 * Test comunicazione errori firma.
	 * Occorre che vi siano documenti in errore per la corretta esecuzione del test.
	 */
	@Test
	public void comunicaErroriFirma(){
		Long idAoo = 25l;
		
		// Recupero degli item che necessitano di manutenzione
		List<ASignItemDTO> itemsFailed = getaSignSRV().getItemsDTFailed(idAoo);
		
		if(!CollectionUtils.isEmptyOrNull(itemsFailed)) {
			for (ASignItemDTO item : itemsFailed) {
					
				// Definizione mail
				String oggettoMail = "Comunicazione errore firma";
				String testoMail = "Si è verificato un errore durante il processo di firma asincrona per il documento: " + item.getDocumentTitle();
				
				List<String> destinatariList = getaSignSRV().recuperaEmailDestinatari(item);
				
				// Invio mail ad ogni destinatario
				NotificheEmailHelper neh = new NotificheEmailHelper();
				NotificaDTO notifica = new NotificaDTO();
				notifica.setIdNotifica(0);
				
				// Per testare questa parte occorre che esiste un server predisposto all'invio mail e che sia correttamente configurato
				String resultInvio = neh.inviaNotifica(notifica, destinatariList, oggettoMail, testoMail, null);
				
				LOGGER.info(resultInvio);
				if("".equals(resultInvio)) { 
					getaSignSRV().setIsNotified(item.getId(), true);
				}
			}
		}
	}
	
	/**
	 * DTO per la gestione delle informazioni di un documento creato per l'esecuzione dei test sulla firma asincrona.
	 */
	private class DocInfoDTO {

		/**
		 * Nome dell'utente nel cui libro firma saranno generati i documenti.
		 */
		private String usr;
		
		/**
		 * Tipologia documento.
		 */
		private Integer inIdTipologiaDocumento;
		
		/**
		 * Tipologia procedimento del documento.
		 */
		private Integer inIdTipologiaProcedimento;
		
		/**
		 * Costruttore della inner class.
		 * @param usr
		 * @param inIdTipologiaDocumento
		 * @param inIdTipologiaProcedimento
		 */
		public DocInfoDTO(String usr, Integer inIdTipologiaDocumento, Integer inIdTipologiaProcedimento) {
			super();
			this.usr = usr;
			this.inIdTipologiaDocumento = inIdTipologiaDocumento;
			this.inIdTipologiaProcedimento = inIdTipologiaProcedimento;
		}
		
		/**
		 * Restituisce il nome utente.
		 * @return utente
		 */
		public String getUsr() {
			return usr;
		}
		
		/**
		 * Restituisce la tipologia del documento.
		 * @return tipologia documento
		 */
		public Integer getInIdTipologiaDocumento() {
			return inIdTipologiaDocumento;
		}
		
		/**
		 * Restituisce la tipologia del procedimento.
		 * @return tipologia procedimento
		 */
		public Integer getInIdTipologiaProcedimento() {
			return inIdTipologiaProcedimento;
		}
	}
	
	/**
	 * Crea documento e invia in firma.
	 */
	@Test
	public void creaEInviaDocumentoInFirma () {
		Integer nDoc = 15;
		Integer nAllegatiPerDoc = 0;
		Collection<DocInfoDTO> c = new ArrayList<>();

		c.add(new DocInfoDTO(USERNAME_RGS, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA));
		
		for (DocInfoDTO d:c) {
			List<AllegatoDTO> allegati = getAllegati(d.getInIdTipologiaDocumento(), nAllegatiPerDoc);		
			creaDoc(d, allegati, nDoc);
		}
	}

	/**
	 * Crea documenti in libro firma per l'esecuzione dei test.
	 * @param info informazioni sul documento
	 * @param allegati documenti allegati ai documenti creati
	 * @param size numero di documenti da creare
	 */
	private void creaDoc(DocInfoDTO info, List<AllegatoDTO> allegati, Integer size) {
		UtenteDTO utente = getUtente(info.getUsr());
		Integer inIdTipologiaDocumento = info.getInIdTipologiaDocumento();
		Integer inIdTipologiaProcedimento = info.getInIdTipologiaProcedimento();
		for (int n=0; n<size; n++) {
			EsitoSalvaDocumentoDTO esitoCreazione =  createDocAutoassegnatoFirma(allegati, utente, inIdTipologiaDocumento, inIdTipologiaProcedimento, null);
			if(esitoCreazione.isEsitoOk()) {
				LOGGER.info("Numero documento creato: " + esitoCreazione.getNumeroDocumento());
			} else {
				LOGGER.info("Errore durante la creazione del documento con utente: " + utente.getUsername());
			}
		}
	}

	/**
	 * Crea e restituisce una lista di allegati da associare ad un documento.
	 * @param inIdTipologiaDocumento
	 * @param size numero di allegati da creare
	 * @return lista di allegati
	 */
	private List<AllegatoDTO> getAllegati(Integer inIdTipologiaDocumento, Integer size) {
		List<AllegatoDTO> allegati = new ArrayList<>();
		for (int n=0; n<size; n++) {
			AllegatoDTO allegato = new AllegatoDTO();
			allegato.setDaFirmareBoolean(true);
			allegato.setDaFirmare(1);
			allegato.setFormatoOriginale(false);
			allegato.setIdTipologiaDocumento(inIdTipologiaDocumento);
			allegato.setFormatoSelected(FormatoAllegatoEnum.ELETTRONICO);
			allegato.setNomeFile("fileTest.pdf");
			allegato.setContent(FileUtils.getFileFromInternalResources("fileTest.pdf"));
			allegato.setMimeType(MediaType.PDF.toString());
			allegati.add(allegato);
		}
		return allegati;
	}
	
}
