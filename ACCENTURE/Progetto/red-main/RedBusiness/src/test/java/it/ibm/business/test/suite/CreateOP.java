package it.ibm.business.test.suite;

import org.junit.Before;
import org.junit.Test;

/**
 * Classe di test creazione OP.
 */
public class CreateOP extends AbstractTest {
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public void initialize() {
		before();

	}
	
	//Modificare idDocumento
	/**
	 * Crea fascicolo OP.
	 */
	@Test
	public void createOPTest() {
		createOrdinePagamento("452398", "emanuela.zazza");
	}


}

