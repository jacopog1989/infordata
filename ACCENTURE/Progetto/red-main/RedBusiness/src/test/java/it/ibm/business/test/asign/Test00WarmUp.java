package it.ibm.business.test.asign;

import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignStepResultDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.service.asign.step.StatusEnum;
import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * Test 0 - Warm up.
 */
public class Test00WarmUp extends AbstractAsignTest {

	/**
	 * Messaggio generico di errore legato al numero di retry.
	 */
	private static final String NUMERO_RETRY_ERROR = "Il numero di retry non è corretto";
	
	/**
	 * Label fine.
	 */
	private static final String FINE = "Fine";
	
	/**
	 * Label inizio.
	 */
	private static final String INIZIO = "Inizio";

	/**
	 * Esegue le operazioni di data preparation prima dell'esecuzione del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue le azioni dopo l'esecuzione del test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Effettua un warm up del meccanismo di firma asincrona.
	 */
	@Test
	public final void warmUP() {
		UtenteDTO utente = getUtente(USERNAME_RGS);

		// Creazione
		EsitoSalvaDocumentoDTO esitoCreazione = createDocAutoassegnatoFirma(null, utente, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, null);

		// Firma
		EsitoOperazioneDTO esito = sign(utente, esitoCreazione.getWobNumber());
		
		String documentTitle = esitoCreazione.getDocumentTitle();
		Integer numProtocollo = esito.getNumeroProtocollo();
		Integer annoProtocollo = esito.getAnnoProtocollo();
		
		List<ASignItemDTO> items = getaSignSRV().getItems(utente, DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE);

		checkAssertion((items != null && !items.isEmpty()), "La lista degli item non può essere vuota");
		Boolean bFound = false;
		ASignItemDTO item = null;
		for (ASignItemDTO it : items) {
			if (it.getDocumentTitle().contentEquals(documentTitle)) {
				item = it;
				bFound = true;
				break;
			}
		}
		checkAssertion(bFound, "Tra gli item deve essere presente quello appena inserito.");
		
		Long idItem = item.getId();

		checkAssertion(item.getStep().equals(StepEnum.START), "L'item deve essere nello step di START");

		ASignStepResultDTO lastResult = new ASignStepResultDTO(idItem, StepEnum.PROTOCOLLAZIONE, INIZIO);
		getaSignSRV().updateStep(lastResult);
		item = getaSignSRV().getItem(idItem);
		
		checkAssertion(item.getId().equals(idItem), "PK non coincidente.");
		checkAssertion(null == item.getAnnoProtocollo() || item.getAnnoProtocollo().equals(annoProtocollo), "Anno protocollo non coincidente.");
		checkAssertion(null == item.getNumeroProtocollo() || item.getNumeroProtocollo().equals(numProtocollo), "Numero protocollo non coincidente.");
		checkAssertion(item.getDocumentTitle().equals(documentTitle), "Document Title non coincidente.");
		checkAssertion(item.getIdRuoloFirmatario().equals(utente.getIdRuolo()), "Ruolo firmatario non coincidente.");
		checkAssertion(item.getIdUfficioFirmatario().equals(utente.getIdUfficio()), "Ufficio firmatario non coincidente.");
		checkAssertion(item.getIdFirmatario().equals(utente.getId()), "Firmatario non coincidente.");
		checkAssertion(item.getIdAoo().equals(utente.getIdAoo()), "AOO non coincidente.");
		
		checkAssertion(item.getDataUltimoRetry() == null, "La data di ultimo retry deve essere null.");
		checkAssertion(item.getPriority() != null, "La priotià non deve essere null.");
		checkAssertion(!item.getFlagComunicato(), "L'item deve risultare non comunicato.");
		
		checkAssertion(item.getnRetry() == 0, "Il numero di retry deve essere 0.");
		checkAssertion(item.getStates().size() == 1, "Deve esserci un solo stato");
		checkAssertion(item.getSteps().size() == 2, "Deve esserci un solo step");

		checkAssertion(item.getStatus().equals(StatusEnum.PRESO_IN_CARICO), "L'item deve risultare preso in carico");
		checkAssertion(item.getStep().equals(StepEnum.PROTOCOLLAZIONE), "L'item deve risultare in step START");
		
		lastResult = new ASignStepResultDTO(idItem, StepEnum.PROTOCOLLAZIONE, FINE);
		lastResult.setStatus(true);
		
		getaSignSRV().updateStep(lastResult);

		ASignStepResultDTO info = getaSignSRV().getInfo(idItem, StepEnum.PROTOCOLLAZIONE);
		checkAssertion(info.getStep().equals(StepEnum.PROTOCOLLAZIONE), "L'item deve essere nello step di PROTOCOLLAZIONE");
		checkAssertion(info.getStart() != null, "Lo step deve essere avviato");
		checkAssertion(!"00:00:00".equals(new SimpleDateFormat("HH:mm:ss").format(info.getStart())), "Deve essere presente l'orario");
		checkAssertion(info.getStop() != null, "Lo step deve essere terminato");
		checkAssertion(info.getLog().contains(INIZIO)&&info.getLog().contains(FINE), "Il log deve essere valorizzato correttamente");
		
		List<ASignStepResultDTO> infos = getaSignSRV().getInfos(idItem);
		checkAssertion(!CollectionUtils.isEmpty(infos), "Devono esserci degli step");

		bFound = false;
		for (ASignStepResultDTO i : infos) {
			if (i.getStep().equals(StepEnum.PROTOCOLLAZIONE)) {
				bFound = true;
				break;
			}
		}

		checkAssertion(bFound, "Deve essere presente lo step di protocollazione.");

		getaSignSRV().setPriority(idItem, -1);
		item = getaSignSRV().getItem(idItem);
		
		checkAssertion(item.getPriority() == -1, "La priorità deve essere pari a -1.");
		
		if (annoProtocollo!=null) {
			getaSignSRV().setMaxPriority(numProtocollo, annoProtocollo);
		} else {
			getaSignSRV().setMaxPriority(documentTitle);
		}
		item = getaSignSRV().getItem(idItem);
		checkAssertion(item.getPriority()>-1, "La priorità deve essere positiva");
		
		Long idFirstItem = getaSignSRV().getFirstItem();
		checkAssertion(idFirstItem.equals(idItem), "Deve essere selezionato il nostro elemento");

		getaSignSRV().updateStatus(idItem, StatusEnum.RETRY);
		item = getaSignSRV().getItem(idFirstItem);
		
		// Verifica che l'item creato sia ancora da lavorare
		checkAssertion(item != null, "Nessun item da lavorare, l'item creato è stato preso in carico da un altro processo");
		
		checkAssertion(item.getStatus().equals(StatusEnum.RETRY), "Deve essere nello stato RETRY");
		
		ASignItemDTO resumableItem = getaSignSRV().getFirstResumableItem();
		checkAssertion(resumableItem.getId().equals(idItem), "Deve essere selezionato il nostro elemento");
		
		getaSignSRV().setRetry(idItem, 10, false);
		item = getaSignSRV().getItem(idFirstItem);
		checkAssertion(item.getnRetry().equals(10), NUMERO_RETRY_ERROR);
		
		getaSignSRV().setRetry(idItem, 20, true);
		item = getaSignSRV().getItem(idFirstItem);
		checkAssertion(item.getnRetry().equals(20), NUMERO_RETRY_ERROR);
		checkAssertion(item.getDataUltimoRetry() == null, "La data di ultimo retry deve essere null");

		getaSignSRV().updateStatus(idItem, StatusEnum.ERRORE);
		item = getaSignSRV().getItem(idFirstItem);
		checkAssertion(item.getStatus().equals(StatusEnum.ERRORE), "Deve essere nello stato ERRORE");
		getaSignSRV().resetRetry(idItem);
		item = getaSignSRV().getItem(idFirstItem);
		checkAssertion(item.getnRetry().equals(0), NUMERO_RETRY_ERROR);
		checkAssertion(item.getDataUltimoRetry() == null, "La data di ultimo retry deve essere null");
		checkAssertion(item.getStatus().equals(StatusEnum.RETRY), "Deve essere nello stato RETRY");
		
	}
	

}
