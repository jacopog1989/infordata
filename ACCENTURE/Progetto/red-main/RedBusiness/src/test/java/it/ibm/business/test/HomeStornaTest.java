package it.ibm.business.test;

import org.junit.Test;

/**
 * Test Home storna.
 */
public class HomeStornaTest extends HomeSpedisciMailTest {

    /**
	 * Esegue il test.
	 */
	@Test
	public final void testSpostaInLav() {
		testAbstract("sposta_in_lavorazione");
	}

	@Override
	void run() {
		// Metodo intenzionalmente vuoto.
	}
	
	
}
