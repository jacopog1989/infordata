package it.ibm.business.test.ucb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.net.MediaType;

import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.CollectedParametersDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.RecuperoCodeDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Test 01 Visto.
 */
public class Test01Visto extends AbstractUCBTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test01Visto.class);

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test visto firmato.
	 */
	@Test
	public final void vistoFirmato() {
		testVisto(true);
	}

	/**
	 * Test visto annullato.
	 */
	@Test
	public final void vistoAnnullato() {
		testVisto(false);
	}

	private void testVisto(final Boolean sign) {
		final UtenteDTO utente = getUtente(USERNAME_BRIAMONTE);

//		STEP 1
//		BRIAMONTE crea ed assegna al CORRIERE dell'ULAV

		final EsitoSalvaDocumentoDTO esitoCreazione = createDoc(utente, ID_TIPO_DOC_VISTO, ID_TIPO_PROC_VISTO);

		final String dtIngresso = esitoCreazione.getDocumentTitle();
		final Integer idDocIngresso = esitoCreazione.getNumeroDocumento();

		dumpCreazioneDocumento(esitoCreazione);
		checkAssertion((esitoCreazione.getErrori() == null || esitoCreazione.getErrori().isEmpty()), "La creazione del documento non è andata a buon fine.");

//		STEP 2
//		Il CORRIERE ULAV lo assegna a BRIAMONTE che se lo ritrova in DA LAVORARE

		final EsitoOperazioneDTO esitoAssegnazione = assegnaDaCorriere(USERNAME_BRIAMONTE, esitoCreazione.getDocumentTitle(), USERNAME_BRIAMONTE);
		checkAssertion(esitoAssegnazione.getFlagEsito(), "L'assegnazione del corriere non è andata a buon fine.");

//		STEP 3
//		BRIAMONTE predispone il visto

		final MasterDocumentRedDTO masterIngresso = getDocumentForDT(utente, dtIngresso, idDocIngresso);
		checkAssertion(masterIngresso != null, "Documento non trovato.");

		final Collection<MetadatoDTO> metadatiRegistroAusiliarioList = getMetadatiRegistro(ID_REGISTRO_VISTO_EX_ART_6, utente, dtIngresso);

		final CollectedParametersDTO c = new CollectedParametersDTO();

		for (final MetadatoDTO m : metadatiRegistroAusiliarioList) {
			c.addStringParam(m.getName(), m.getName());
		}

		final FileDTO contentUscita = new FileDTO("template.pdf", createPdf(ID_REGISTRO_VISTO_EX_ART_6, c.getStringParams()), MediaType.PDF.toString());

		final DetailDocumentRedDTO detail = predisponiVisto(utente, masterIngresso, contentUscita, ID_REGISTRO_VISTO_EX_ART_6, metadatiRegistroAusiliarioList);
		LOGGER.info("#####INGRESSO : DT > " + dtIngresso + " NUM > " + idDocIngresso);

		final String classeDoc = masterIngresso.getClasseDocumentale();
		final Integer idCatDocIngresso = masterIngresso.getIdCategoriaDocumento();

//		STEP 4
//		BRIAMONTE finalizza la risposta fornendo i dati mancanti: 
//		- iter di firma manuale verso se stesso
//		- tab spedizione
//		- Indice di classificazoine
//		- momento protocollazione

		// Valorizzazione iter di firma
		detail.setIdIterApprovativo(IterApprovativoDTO.ITER_FIRMA_MANUALE);
		final List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneFirma(ID_UFFICIO_DIREZIONE_ULAV, ID_FRANCESCO_BRIAMONTE, "FRANCESCO", "BRIAMONTE"));
		detail.setAssegnazioni(assegnazioni);

		// Valorizzare Tab spedizione
		if (detail.getDestinatari() != null && !detail.getDestinatari().isEmpty()
				&& MezzoSpedizioneEnum.ELETTRONICO.equals(detail.getDestinatari().get(0).getMezzoSpedizioneEnum())) {
			final EmailDTO mailSpedizione = new EmailDTO();
			mailSpedizione.setMittente("testred@pec.mef.gov.it");
			mailSpedizione.setDestinatari("a@b.c");
			mailSpedizione.setOggetto("MEF - RGS - Un numero di protocollo valido - " + detail.getOggetto());
			mailSpedizione.setTesto("Ciao Mondo!");
			detail.setMailSpedizione(mailSpedizione);
		}

		// Valorizzare Indice di classificazione
		detail.setIndiceClassificazioneFascicoloProcedimentale("A");

		// Protocollazione standard
		detail.setMomentoProtocollazioneEnum(MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD);

		// finalizza creazione documento uscita

		final EsitoSalvaDocumentoDTO esitoSalvataggioRisposta = salvaDocumento(detail, utente);

		dumpCreazioneDocumento(esitoSalvataggioRisposta);
		checkAssertion((esitoSalvataggioRisposta.getErrori() == null || esitoSalvataggioRisposta.getErrori().isEmpty()),
				"La creazione dell'uscita in risposta non è andata a buon fine.");

		final String dtUscita = esitoSalvataggioRisposta.getDocumentTitle();
		final String wfUscita = esitoSalvataggioRisposta.getWobNumber();
		final Integer idDocUscita = esitoSalvataggioRisposta.getNumeroDocumento();

		LOGGER.info("#####USCITA : DT > " + dtUscita + " NUM > " + idDocUscita);

		RecuperoCodeDTO codeIngresso = getQueues(utente.getUsername(), dtIngresso, classeDoc, idCatDocIngresso);
		RecuperoCodeDTO codeUscita = getQueues(utente.getUsername(), dtUscita, classeDoc, CategoriaDocumentoEnum.USCITA.getIds()[0]);

		LOGGER.info("Verifica code ingresso");
		checkAssertionCode(codeIngresso, DocumentQueueEnum.ASSEGNATE, DocumentQueueEnum.IN_LAVORAZIONE_UCB);

		LOGGER.info("Verifica code uscita");
		checkAssertionCode(codeUscita, DocumentQueueEnum.NSD);

		// verifica storico ingresso

		checkStorico(utente, detail.getWobNumberSelected(), dtIngresso, EventTypeEnum.PREDISPOSIZIONE_VISTO);

//		STEP 5
//		BRIAMONTE ricerca il visto dalla ricerca esiti. 

		// verifica ricerca esito
		final Collection<MasterDocumentRedDTO> result = ricercaEsiti(utente, SottoCategoriaDocumentoUscitaEnum.VISTO);

		Boolean bOk = false;
		for (final MasterDocumentRedDTO master : result) {
			if (master.getDocumentTitle().equals(dtIngresso) && master.getIdTipologiaDocumento().equals(ID_TIPO_DOC_VISTO)
					&& master.getIdTipoProcedimento().equals(ID_TIPO_PROC_VISTO)) {
				bOk = true;
				break;
			}
		}
		checkAssertion(bOk, "Il documento in ingresso non è ricercabile per esito.");

		if (Boolean.TRUE.equals(sign)) {
//			STEP 6A
//			BRIAMONTE firma il documento

			signVisto(utente, masterIngresso.getClasseDocumentale(), idCatDocIngresso, dtIngresso, detail.getWobNumberSelected(), dtUscita, wfUscita);

		} else {
//			STEP 6B
//			BRIAMONTE annulla il visto in qualità di firmatario del visto
			final EsitoOperazioneDTO esitoAnnullamento = annulla(utente, wfUscita, "Test annullamento visto.", ResponsesRedEnum.ANNULLA_VISTO); // updateWorkflow va in errore
																																				// >>
																																				// filenet.vw.api.VWException:
																																				// Field name parameter is not
																																				// found. annullaTemplate
			checkAssertion(esitoAnnullamento.getFlagEsito(), "Annullamento fallito.");

			// check code uscita
			codeUscita = getQueues(utente.getUsername(), dtUscita, classeDoc, CategoriaDocumentoEnum.USCITA.getIds()[0]);
			checkAssertionCode(codeUscita, DocumentQueueEnum.DA_LAVORARE_UCB);

			// Verifico lo storico dell'uscita.
			checkStorico(utente, null, dtUscita, EventTypeEnum.RIFIUTO);

//			STEP 7B
//			BRIAMONTE conferma annullamento in quanto creatore del visto

			final EsitoOperazioneDTO confermaAnnuallmento = confermaAnnullamento(utente, wfUscita);
			checkAssertion(confermaAnnuallmento.getFlagEsito(), "Conferma Annullamento fallito.");

			// check code ingresso
			codeIngresso = getQueues(utente.getUsername(), dtIngresso, classeDoc, idCatDocIngresso);
			checkAssertionCode(codeIngresso, DocumentQueueEnum.DA_LAVORARE_UCB, DocumentQueueEnum.ASSEGNATE);

			// Verifico lo storico dell'uscita.
			checkStorico(utente, null, dtUscita, EventTypeEnum.ANNULLATO); // se passo il wob dell'uscita mi da errore, probabilmente annullamento lo ha
																			// killato, se passo solo document title trovo DOCUMENTO,
																			// ASSEGNAZIONE_CONOSCENZA.ASSEGNAZIONE_CONOSCENZ, ASSEGNAZIONE_COMPETENZA,
																			// ASSEGNAZIONE_DIRETTA_A_UTENTE, PREDISPOSIZIONE_VISTO ma niente ANNULLATO

			// Verifico lo storico dell'entrata.
			checkStorico(utente, detail.getWobNumberSelected(), dtIngresso, EventTypeEnum.VISTO_ANNULLATO);

		}

	}

	private void signVisto(final UtenteDTO utente, final String classeDocumentale, final Integer idCategioriaDocumentoIngresso, final String dtIngresso,
			final String wobNumberIngresso, final String dtUscita, final String wobNumberUscita) {
		final EsitoOperazioneDTO esito = sign(utente, wobNumberUscita);

		// Verifico l'esito della firma.
		checkAssertion(esito.getFlagEsito(), "Errore in fase di firma.");

		// Verifico lo storico dell'ingresso.
		checkStorico(utente, wobNumberIngresso, dtIngresso, EventTypeEnum.PREDISPOSIZIONE_VISTO, EventTypeEnum.AVVENUTA_APPROVAZIONE);

		// Verifico lo storico dell'uscita.
		checkStorico(utente, wobNumberUscita, dtUscita, EventTypeEnum.FIRMATO);

		// Verifico le code in cui si trova l'ingresso dopo la firma.
		final RecuperoCodeDTO codeIngresso = getQueues(utente.getUsername(), dtIngresso, classeDocumentale, idCategioriaDocumentoIngresso);
		checkAssertionCode(codeIngresso, DocumentQueueEnum.ASSEGNATE);

		// Verifico le code in cui si trova l'uscita dopo la firma.
		final RecuperoCodeDTO codeUscita = getQueues(utente.getUsername(), dtUscita, classeDocumentale, CategoriaDocumentoEnum.USCITA.getIds()[0]);
		checkAssertionCode(codeUscita, DocumentQueueEnum.SPEDIZIONE, DocumentQueueEnum.PROCEDIMENTI);
	}

}
