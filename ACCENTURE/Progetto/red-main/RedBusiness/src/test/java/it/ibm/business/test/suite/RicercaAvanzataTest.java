package it.ibm.business.test.suite;

import java.util.Calendar;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO;
import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataFaldDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataFascDTO;
import it.ibm.red.business.dto.RegistroProtocolloDTO;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IRicercaAvanzataDocFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaAvanzataFaldFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaAvanzataFascFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaRegistroProtocolloFacadeSRV;

public class RicercaAvanzataTest extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaAvanzataTest.class);
	
	/**
	 * Servizio.
	 */
	private IRicercaAvanzataDocFacadeSRV ricercaAvanzataDocSRV;

	/**
	 * Servizio.
	 */
	private IRicercaAvanzataFascFacadeSRV ricercaAvanzataFascSRV;

	/**
	 * Servizio.
	 */
	private IRicercaAvanzataFaldFacadeSRV ricercaAvanzataFaldSRV;

	/**
	 * Servizio.
	 */
	private IRicercaRegistroProtocolloFacadeSRV ricercaRegistroProtocolloSRV;

	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		ricercaAvanzataDocSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaAvanzataDocFacadeSRV.class);
		ricercaAvanzataFascSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaAvanzataFascFacadeSRV.class);
		ricercaAvanzataFaldSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaAvanzataFaldFacadeSRV.class);
		ricercaRegistroProtocolloSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaRegistroProtocolloFacadeSRV.class);
	}
	
	/**
	 * Esegue il test di ricerca documenti.
	 */
	@Test
	public final void testRicercaDocumenti() {
		ParamsRicercaAvanzataDocDTO paramsRicercaAvanzata = new ParamsRicercaAvanzataDocDTO();
		paramsRicercaAvanzata.setAnnoDocumento(2018);
		paramsRicercaAvanzata.setTipoAssegnazione(TipoAssegnazioneEnum.COMPETENZA);
		paramsRicercaAvanzata.setAssegnatario(new AssegnatarioOrganigrammaDTO(64L, 474L));
		Calendar dataCreazioneDa = Calendar.getInstance();
		dataCreazioneDa.set(2018, 8, 1);
		paramsRicercaAvanzata.setDataCreazioneDa(dataCreazioneDa.getTime());
		paramsRicercaAvanzata.setOggetto("TEST");
		paramsRicercaAvanzata.setNomeFascicolo("TEST");
		
		Collection<MasterDocumentRedDTO> results = ricercaAvanzataDocSRV.eseguiRicercaDocumenti(paramsRicercaAvanzata, getUtente(USERNAME_MAZZOTTA), false, false);
		
		LOGGER.info("=========> NUMERO DI DOCUMENTI RESTITUITI DALLA RICERCA AVANZATA DOCUMENTI: " + results.size());
	}
	
	/**
	 * Esegue il test di ricerca fascicoli.
	 */
	@Test
	public final void testRicercaFascicoli() {
		ParamsRicercaAvanzataFascDTO paramsRicercaAvanzata = new ParamsRicercaAvanzataFascDTO();
		paramsRicercaAvanzata.setAnnoFascicolo(2018);
		Calendar dataCreazioneDa = Calendar.getInstance();
		dataCreazioneDa.set(2018, 8, 1);
		paramsRicercaAvanzata.setDataCreazioneDa(dataCreazioneDa.getTime());
		paramsRicercaAvanzata.setStatoFascicolo(FilenetStatoFascicoloEnum.APERTO);
		paramsRicercaAvanzata.setFaldonato(false);
		paramsRicercaAvanzata.setDescrizioneFascicolo("TEST");
		
		Collection<FascicoloDTO> results = ricercaAvanzataFascSRV.eseguiRicercaFascicoli(paramsRicercaAvanzata, getUtente(USERNAME_MAZZOTTA), false);
		
		LOGGER.info("=========> NUMERO DI DOCUMENTI RESTITUITI DALLA RICERCA AVANZATA FASCICOLI: " + results.size());
	}
	
	/**
	 * Esegue il test ricerca faldoni.
	 */
	@Test
	public final void testRicercaFaldoni() {
		ParamsRicercaAvanzataFaldDTO paramsRicercaAvanzata = new ParamsRicercaAvanzataFaldDTO();
		paramsRicercaAvanzata.setDescrizioneFaldone("test");
		Calendar dataCreazioneDa = Calendar.getInstance();
		dataCreazioneDa.set(2018, 9, 1);
		paramsRicercaAvanzata.setDataCreazioneDa(dataCreazioneDa.getTime());
		
		Collection<FaldoneDTO> results = ricercaAvanzataFaldSRV.eseguiRicercaFaldoni(paramsRicercaAvanzata, getUtente(USERNAME_MAZZOTTA), false);
		
		LOGGER.info("=========> NUMERO DI DOCUMENTI RESTITUITI DALLA RICERCA AVANZATA FALDONI: " + results.size());
	}
	
	/**
	 * Test ricerca registro protocollo.
	 */
	@Test
	public final void testRicercaRegistroProtocollo() {
		ParamsRicercaAvanzataDocDTO paramsRicercaAvanzata = new ParamsRicercaAvanzataDocDTO();
		Calendar dataCreazioneDa = Calendar.getInstance();
		dataCreazioneDa.set(2018, 9, 1);
		paramsRicercaAvanzata.setDataCreazioneDa(dataCreazioneDa.getTime());
		
		Collection<RegistroProtocolloDTO> results = ricercaRegistroProtocolloSRV.eseguiRicercaPerStampaRegistroProtocollo(paramsRicercaAvanzata, getUtente(USERNAME_MAZZOTTA));
		
		LOGGER.info("=========> NUMERO DI DOCUMENTI RESTITUITI DALLA RICERCA REGISTRO PROTOCOLLO: " + results.size());
	}
	
	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}
}