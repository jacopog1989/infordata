package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Test 06.
 */
public class Test06 extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test06.class);
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}
	
	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test.
	 */
	@Test
	public final void test() {

		UtenteDTO utente = getUtente(USERNAME_TANZI);
		Boolean daNonProtocollare = false;

		Integer idMezzoRicezione = ID_MEZZO_RICEZIONE_POSTA_ELET_DA_PEC_PER_ELETTRONICI;
		
		List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneCompetenzaUfficio(ID_IGF, DESC_IGF));
		String indiceFascicoloProcedimentale = null;

		String oggetto = createObj();
		
		//CORRIERE
		List<AllegatoDTO> allegati = null;
		EsitoSalvaDocumentoDTO esito = createDocIngresso(allegati, oggetto, utente, ID_CONTATTO_IRENE, FormatoDocumentoEnum.ELETTRONICO, ID_TIPO_DOC_GENERICO_RGS_ENTRATA, ID_TIPO_PROC_GENERICO_RGS_ENTRATA, assegnazioni, daNonProtocollare, idMezzoRicezione, indiceFascicoloProcedimentale, false);
		checkAssertion(esito.isEsitoOk(), "Errore in fase di creazione documento.");
		
		//DA LAVORARE - TANZI
		EsitoOperazioneDTO esitoAssegnazione = assegnaDaCorriere(USERNAME_TANZI, esito.getDocumentTitle(), USERNAME_TANZI);
		checkAssertion(esitoAssegnazione.getFlagEsito(), "Errore in fase di assegnazione.");

		//ATTI
		EsitoOperazioneDTO esitoMessAtti = messaAgliAtti(USERNAME_TANZI, esito.getDocumentTitle());
		checkAssertion(esitoMessAtti.getFlagEsito(), "Errore in fase di messa agli atti.");

		//DA LAVORARE - TANZI
		EsitoOperazioneDTO esitoRiattivazione = riattiva(USERNAME_TANZI, esito.getDocumentTitle());
		checkAssertion(esitoRiattivazione.getFlagEsito(), "Errore in fase di riattivazione.");

		//IN SOSPESO - TANZI
		EsitoOperazioneDTO esitoMessaSospeso = messaInSospeso(USERNAME_TANZI, esito.getDocumentTitle());
		checkAssertion(esitoMessaSospeso.getFlagEsito(), "Errore in fase di messa in sospeso.");

		//DA LAVORARE - TANZI
		EsitoOperazioneDTO esitoSpostaLavorazione = spostaLavorazione(USERNAME_TANZI, esito.getDocumentTitle()); //accede coda in sospeso e fa "Sposta in lavorazione"
		checkAssertion(esitoSpostaLavorazione.getFlagEsito(), "Errore in fase di spostamento in lavorazione.");

		//CORRIERE
		String wobNumberRefresh = getWF(USERNAME_TANZI, DocumentQueueEnum.DA_LAVORARE, esito.getDocumentTitle()).getWorkObjectNumber();
		EsitoOperazioneDTO esitoStornaCorriere = stornaAlCorriere(USERNAME_TANZI, wobNumberRefresh); //accede coda da lavorare e fai "Storna al corriere"
		checkAssertion(esitoStornaCorriere.getFlagEsito(), "Errore in fase di storno al corriere.");

		//DA LAVORARE - TANZI
		EsitoOperazioneDTO esitoStorna = storna(USERNAME_TANZI, esito.getDocumentTitle(), utente.getIdUfficio().intValue(), utente.getId().intValue()); //Poi coda corriere e fai "Storna" a IGF - GIANFRANCO.TANZI
		checkAssertion(esitoStorna.getFlagEsito(), "Errore in fase di storno.");

		//CORRIERE
		String wobNumberRefresh2 = getWF(USERNAME_TANZI, DocumentQueueEnum.DA_LAVORARE, esito.getDocumentTitle()).getWorkObjectNumber();
		EsitoOperazioneDTO esitoStornaCorriere2 = stornaAlCorriere(USERNAME_TANZI, wobNumberRefresh2);
		checkAssertion(esitoStornaCorriere2.getFlagEsito(), "Errore in fase di storno.");
		
		//ASSEGNA A IGF - Ufficio I
		EsitoOperazioneDTO esitoAssegnaDaCorriereAdUff = assegnaDaCorriereAdUfficio(USERNAME_TANZI, esito.getDocumentTitle(), ID_IGF_UFFICIO_I);
		checkAssertion(esitoAssegnaDaCorriereAdUff.getFlagEsito(), "Errore in fase di assegna da corriere ad ufficio.");

		EsitoOperazioneDTO esitoRifiuto = rifiutaDaCorriere(USERNAME_ANDREANI, esito.getDocumentTitle());
		checkAssertion(esitoRifiuto.getFlagEsito(), "Errore in fase di rifiuto.");
		
		LOGGER.info("DOCUMENT TITLE : " + esito.getDocumentTitle());
		LOGGER.info("OGGETTO : " + oggetto);
		LOGGER.info("NUM DOC : " + getNumDoc(USERNAME_NUNZI, esito.getDocumentTitle()));
	}
	

}
