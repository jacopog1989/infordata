package it.ibm.business.test;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;

/**
 * Test home spedisci mail.
 */
public class HomeSpedisciMailTest extends AbstractTest {

    /**
     * Username.
     */
	private static final String USERNAME = "IGF.USER1";

	/**
	 * Metodo preliminare al test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue il test.
	 */
	@Test
	public final void test() {
		testAbstract("spedisci_mail");
	}

	/**
	 * @param response
	 */
	protected void testAbstract(final String response) {
		IUtenteFacadeSRV facadeUtente = getBean(IUtenteFacadeSRV.class);
		UtenteDTO utente = facadeUtente.getByUsername(USERNAME);

		
		Collection<QueueInfo> r11 = executeQueueQueryAllQueue(utente, null, response);
		dumpWFInfo(r11);
	}

	void run() {
		// Metodo intenzionalmente vuoto.
	}

}
