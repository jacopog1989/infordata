package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.RecuperoCodeDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.RicercaPerBusinessEntityEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * Test 17.
 */
public class Test17 extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test17.class);
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}

	/**
	 * Esegue le azioni prima del test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test 17.
	 */
	@Test
	public final void test() {
		
		dumpDTInfo("38739");
		
		
		//Creazione documento
		UtenteDTO utente = getUtente(USERNAME_ASCI);
		String oggetto = createObj();
		String indiceFascicoloProcedimentale = "A";
		Boolean bRiservato = false;

		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(createDestinatarioEsterno(MezzoSpedizioneEnum.CARTACEO, ID_CONTATTO_GIANCARLO_ROSSI, ModalitaDestinatarioEnum.TO));

		List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneSpedizioneUtente(ID_UCP_RGS, ID_RENZO_ASCI, "Renzo", "Asci"));

		List<AllegatoDTO> allegati = null;
		EsitoSalvaDocumentoDTO esitoUscita = createDocUscita(allegati, false, oggetto, utente, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, assegnazioni, destinatari, indiceFascicoloProcedimentale, MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD, IterApprovativoDTO.ITER_FIRMA_MANUALE, bRiservato, null);
		checkAssertion(esitoUscita.isEsitoOk(), "Errore in fase di creazione documento in uscita.");

		String dt = esitoUscita.getDocumentTitle();

		// RENZO.ASCI -> In spedizione response "Spedito"
		EsitoOperazioneDTO esitoSpedito = spedito(USERNAME_ASCI, dt);
		checkAssertion(esitoSpedito.isEsito(), "Errore in fase di spedito.");

		//RENZO.ASCI Ricerca rapida documento per frase esatta sul numero documento
		Integer numDoc = getNumDoc(USERNAME_ASCI, dt);
		Collection<MasterDocumentRedDTO> results = ricerca(numDoc + "", null, RicercaGenericaTypeEnum.ESATTA, RicercaPerBusinessEntityEnum.DOCUMENTI, USERNAME_ASCI);
		checkAssertion(results != null && results.size() == 1, "Deve essere trovato un solo risultato.");
		
		if (results == null) {
			LOGGER.error("Collection dei master non inizializzata prima della chiamata ad un metodo della Collection.");
			throw new RedException("Collection dei master non inizializzato prima della chiamata ad un metodo della Collection.");
		}
		
		MasterDocumentRedDTO master = results.iterator().next();
		checkAssertion(master.getDocumentTitle().equals(dt), "Il document title del documento deve coincidere con quello generato.");

		//RENZO.ASCI Recupero set code documento, verifica presenza coda chiuse
		RecuperoCodeDTO queues = getQueues(USERNAME_ASCI, dt);
		Boolean found = false;
		for (DocumentQueueEnum queue:queues.getCode()) {
			if (queue.equals(DocumentQueueEnum.CHIUSE)) {
				found = true;
			}
		}
		checkAssertion(found, "Il documento deve essere nella coda chiusi.");
		
		LOGGER.info("DOCUMENT TITLE : " + dt);
		LOGGER.info("OGGETTO : " + oggetto);
		LOGGER.info("NUM DOC : " + numDoc);
	}
	
}
