package it.ibm.business.test.asign;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import com.filenet.api.core.Document;
import com.google.common.net.MediaType;

import filenet.vw.api.VWSession;
import filenet.vw.api.VWWorkObject;
import it.ibm.business.test.suite.AbstractTest;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.MapRedKey;
import it.ibm.red.business.constants.Constants.Modalita;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IProtocollaDsrSRV;
import it.ibm.red.business.service.facade.IFepaFacadeSRV;
import it.ibm.red.business.service.facade.IREDServiceFacadeSRV;
import it.ibm.red.business.service.facade.ISalvaDocumentoFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.webservice.model.redservice.header.v1.CredenzialiUtenteRed;
import it.ibm.red.webservice.model.redservice.header.v1.ObjectFactory;
import it.ibm.red.webservice.model.redservice.types.v1.ClasseDocumentaleRed;
import it.ibm.red.webservice.model.redservice.types.v1.DocumentoRed;
import it.ibm.red.webservice.model.redservice.types.v1.IdentificativoUnitaDocumentaleFEPATypeRed;
import it.ibm.red.webservice.model.redservice.types.v1.IdentificativoUnitaDocumentaleTypeRed;
import it.ibm.red.webservice.model.redservice.types.v1.MapRed;
import it.ibm.red.webservice.model.redservice.types.v1.MapRed.Elemento;
import it.ibm.red.webservice.model.redservice.types.v1.MetadatoRed;

/**
 * Classe di test per le funzionalità FEPA.
 */
public class Test11Fepa extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Test11Fepa.class.getName());

	/**
	 * Nome file pdf.
	 */
	private static final String FILENAME_PDF = "fileTest.pdf";
	
	/**
	 * Service RED.
	 */
	private IREDServiceFacadeSRV redSRV;
	
	/**
	 * Service salvataggio documento.
	 */
	private ISalvaDocumentoFacadeSRV creaDocFacade;
	
	/**
	 * Service protocollazione Dsr.
	 */
	private IProtocollaDsrSRV protocollaDsr;
	
	/**
	 * Service FEPA.
	 */
	private IFepaFacadeSRV fepaFacade;

	/**
	 * Esegue le azioni prima dell'esecuzione del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		redSRV = ApplicationContextProvider.getApplicationContext().getBean(IREDServiceFacadeSRV.class);
		creaDocFacade = ApplicationContextProvider.getApplicationContext().getBean(ISalvaDocumentoFacadeSRV.class);
		protocollaDsr = ApplicationContextProvider.getApplicationContext().getBean(IProtocollaDsrSRV.class);
		fepaFacade = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);
	}

	/**
	 * Esegue un test di creazion DSR uscita.
	 */
	@Test
	public void creaDSRUscita() {
		String protocollatore = USERNAME_ZAZZA;
		String guidMail = "AF65EA02-AF84-45A5-9D27-4DACE4C8D90C";// Cerca mail con GOZZI con oggetto like "per test DSR" e metti in arrivo
		String utenteCorriere = USERNAME_GOZZI;
		String utenteAssegnatario = USERNAME_GOZZI;
		String utenteFirmatarioDSR = USERNAME_DINUZZO;
		Integer ufficioFirmatarioDSR = ID_IGICS;
		UtenteDTO destinatarioDSR = getUtente(USERNAME_ZAZZA);

		//Protocolliamo la DSR
		String dtDSRIngresso = protocollaDSR(protocollatore, guidMail);

		//Dal corriere all'assegnatario
		EsitoOperazioneDTO esitoAss = assegnaDaCorriere(utenteCorriere, dtDSRIngresso, utenteAssegnatario, ID_IGICS);
		checkAssertion(esitoAss.isEsito(), "L'assegnazione della DSR non funziona.");
		
		//Generiamo la DSR in uscita assegnandola in firma e definendo il destinatario
		EsitoSalvaDocumentoDTO esito = predisposizioneDichiarazione(dtDSRIngresso,  utenteFirmatarioDSR, ufficioFirmatarioDSR, destinatarioDSR);				
		String dtDSRUscita = esito.getDocumentTitle();
		checkAssertion(!StringUtils.isNullOrEmpty(dtDSRUscita), "DSR non generata correttamente.");
		LOGGER.info("Numero documento da firmare sul libro firma di " + utenteFirmatarioDSR + " [" + esito.getNumeroDocumento() + "]");
		LOGGER.info("Document title da usare per la creazione del DD in uscita [" + esito.getDocumentTitle() + "]");
	}

	/**
	 * Crea un decreto dirigenziale in uscita.
	 */
	@Test
	public void creaDDUscita() {
		UtenteDTO destinatarioDecreto = getUtente(USERNAME_ZAZZA); // Deve coincidere col destinatario della DD
		UtenteDTO utenteFirmatarioDDDTO = getUtente(USERNAME_PAOLUCCI);
		String dtDSRUscita = "452547";
		
		//Creiamo DD assegnato al destinatario ed associamogli una fattura
		String dtDecreto = creaDecreto(destinatarioDecreto.getId().intValue());
		checkAssertion(!StringUtils.isNullOrEmpty(dtDecreto), "DD non creato correttamente.");

		//Se il destinatario della DSR generata da "creaDSRUscita" e quello della DD coincidono questi potranno essere associati

		VWSession session = getPESession(destinatarioDecreto);
		VWWorkObject woDSR = fromDocumentTitleToWF(session, null, dtDSRUscita, destinatarioDecreto.getIdUfficio().intValue(), destinatarioDecreto.getId().intValue());
		VWWorkObject woDecreto = fromDocumentTitleToWF(session, null, dtDecreto, destinatarioDecreto.getIdUfficio().intValue(), destinatarioDecreto.getId().intValue());
		List<String> wobNumbersDecreto = new ArrayList<>();
		wobNumbersDecreto.add(woDecreto.getWorkflowNumber());
		String wobNumberDSR = woDSR.getWorkflowNumber();
		EsitoOperazioneDTO esitoAssociazione = fepaFacade.associaDecreto(destinatarioDecreto, wobNumberDSR, wobNumbersDecreto);
		checkAssertion(esitoAssociazione.isEsito(), "L'associazione della DSR al decreto non funziona.");

		//Creiamo ordine di pagamento ed associao al sdecreto
				
		createOrdinePagamento(dtDecreto, destinatarioDecreto.getUsername());

		//Inviamo in firma il decreto

		EsitoOperazioneDTO esitoInviaFirmaDecreto = fepaFacade.invioDecretoInFirma(destinatarioDecreto,
				woDecreto.getWorkflowNumber(), utenteFirmatarioDDDTO.getIdUfficio(), utenteFirmatarioDDDTO.getId(), "Un invio in firma di prova.");
		checkAssertion(esitoInviaFirmaDecreto.isEsito(), "Invio in firma del decreto non funziona.");
		LOGGER.info("Numero documento da firmare sul libro firma di " + utenteFirmatarioDDDTO.getUsername() + " [" + esitoInviaFirmaDecreto.getIdDocumento() + "]");

	}

	
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
	
	private String createFattura(final String idFascicoloFEPA, final String idDocumentoFEPA, final Integer idAoo,
			final Integer idUfficio, final String idUtente) {
		// Assegni la fattura a te stesso in fase di creazione.
		return assegnaFattura(idFascicoloFEPA, idDocumentoFEPA, idAoo, idUfficio, idUtente, idUfficio, idUtente);
	}

	private String assegnaFattura(final String idFascicoloFEPA, final String idDocumentoFEPA, final Integer idAoo,
			final Integer idUfficioMittenteFattura, final String idUtenteMittenteFattura,
			final Integer idUfficioDestinatarioFattura, final String idUtenteDestinatarioFattura) {

		String oggettoDocumentoRED = createObj();

		ObjectFactory of = new ObjectFactory();

		CredenzialiUtenteRed credenziali = new CredenzialiUtenteRed();
		credenziali.setIdAoo(idAoo);
		credenziali.setIdGruppo(idUfficioMittenteFattura);
		credenziali.setIdUtente(of.createCredenzialiUtenteRedIdUtente(idUtenteMittenteFattura));

		MapRed mapRed = new MapRed();

		Elemento eUtenteCedente = new Elemento();
		eUtenteCedente.setKey(MapRedKey.UTENTE_CEDENTE_KEY);
		eUtenteCedente.setValore(idUtenteMittenteFattura);
		mapRed.getElemento().add(eUtenteCedente);

		Elemento eDestinatario = new Elemento();
		eDestinatario.setKey(MapRedKey.UTENTE_DESTINATARIO_KEY);
		eDestinatario.setValore(idUtenteDestinatarioFattura);
		mapRed.getElemento().add(eDestinatario);

		Elemento eUfficioCedente = new Elemento();
		eUfficioCedente.setKey(MapRedKey.UFFICIO_DESTINATARIO_KEY);
		eUfficioCedente.setValore(idUfficioDestinatarioFattura + "");
		mapRed.getElemento().add(eUfficioCedente);
		
		String inFileName = FILENAME_PDF;
		byte[] inContent = FileUtils.getFileFromInternalResources(FILENAME_PDF);
		String inMimeType = MediaType.PDF.toString();

		return redSRV.assegnaFascicoloFatturaWithoutFepa(credenziali, idFascicoloFEPA, mapRed, idDocumentoFEPA,
				oggettoDocumentoRED, inFileName, inContent, inMimeType);
	}

	private String creaDecreto(final Integer idDestinatario) {
		String idFascicoloFEPA = UUID.randomUUID().toString();
		String idDocumentoFEPA = UUID.randomUUID().toString();
		String idFascicoloFepaDecretoD = UUID.randomUUID().toString();
		// Lattanzio decide di continuare la lavorazione della fattura su RED.
		String dtFattura = createFattura(idFascicoloFEPA, idDocumentoFEPA, ID_AOO_RGS, ID_IGICS_UFFICIO_I,
				idDestinatario + "");
		checkAssertion(!StringUtils.isNullOrEmpty(dtFattura), "Il document title della fattura non può essere null.");

		CredenzialiUtenteRed credenziali = new CredenzialiUtenteRed();
		credenziali.setIdAoo(ID_AOO_RGS);
		credenziali.setIdGruppo(ID_IGICS_UFFICIO_I);
		credenziali.setIdUtente(new ObjectFactory().createCredenzialiUtenteRedIdUtente(idDestinatario + ""));

		String numeroProtocolloDecreto = "1";
		Date dataProtocollo = new Date();

		// Le fatture saranno indicizzate tramite gli di fascicolo fepa associati.
		it.ibm.red.webservice.model.redservice.types.v1.ObjectFactory of = new it.ibm.red.webservice.model.redservice.types.v1.ObjectFactory();
		IdentificativoUnitaDocumentaleFEPATypeRed fascicolo = new IdentificativoUnitaDocumentaleFEPATypeRed();
		IdentificativoUnitaDocumentaleTypeRed value = of.createIdentificativoUnitaDocumentaleTypeRed();
		value.setID(idFascicoloFEPA);
		fascicolo.setID(value);

		List<IdentificativoUnitaDocumentaleFEPATypeRed> fascicoli = new ArrayList<>();
		fascicoli.add(fascicolo);

		String inFileName = "fileTest.docx";
		byte[] inContent = FileUtils.getFileFromInternalResources("fileTest.docx");
		String inMimeType = Constants.ContentType.DOCX;

		DocumentoRed documento = new DocumentoRed();
		documento.setDataHandler(of.createDocumentoRedDataHandler(inContent));
		ClasseDocumentaleRed cdr = of.createClasseDocumentaleRed();

		MetadatoRed eOggettoProtocollo = new MetadatoRed();
		eOggettoProtocollo.setKey(of.createMetadatoRedKey(MapRedKey.OGGETTO_PROTOCOLLO_FEPA));
		eOggettoProtocollo.setValue(of.createMetadatoRedValue(createObj()));
		cdr.getMetadato().add(eOggettoProtocollo);

		MetadatoRed eNomeFile = new MetadatoRed();
		eNomeFile.setKey(of.createMetadatoRedKey(MapRedKey.NOME_FILE_FEPA));
		eNomeFile.setValue(of.createMetadatoRedValue(inFileName));
		cdr.getMetadato().add(eNomeFile);

		MetadatoRed eNumeroProtocollo = new MetadatoRed();
		eNumeroProtocollo.setKey(of.createMetadatoRedKey(MapRedKey.NUM_PROTOCOLLO_FEPA));
		eNumeroProtocollo.setValue(of.createMetadatoRedValue(numeroProtocolloDecreto));
		cdr.getMetadato().add(eNumeroProtocollo);

		MetadatoRed eDataProtocollo = new MetadatoRed();
		eDataProtocollo.setKey(of.createMetadatoRedKey(MapRedKey.DATA_PROTOCOLLO_FEPA));
		eDataProtocollo
				.setValue(of.createMetadatoRedValue(DateUtils.dateToString(dataProtocollo, DateUtils.DD_MM_YYYY)));
		cdr.getMetadato().add(eDataProtocollo);

		documento.setIdFascicolo(of.createDocumentoRedIdFascicolo(idFascicoloFepaDecretoD));

		MapRed metadati = new MapRed();

		documento.setContentType(of.createDocumentoRedContentType(inMimeType));

		documento.setClasseDocumentale(of.createDocumentoRedClasseDocumentale(cdr));

		String dtDecreto = redSRV.inserimentoFascicoloDecreto(credenziali, metadati, documento, fascicoli);
		checkAssertion(!StringUtils.isNullOrEmpty(dtDecreto), "Il document title del decreto non può essere null.");
		return dtDecreto;
	}


	private String protocollaDSR(String protocollatore, String guidMail) {
		UtenteDTO utente = getUtente(protocollatore);

		String mailOggetto = "Una DSR di test.";
		Contatto contatto = getContatto(ID_CONTATTO_IRENE);

		DetailDocumentRedDTO detailsProt = new DetailDocumentRedDTO();

		detailsProt.setContent(FileUtils.getFileFromInternalResources(FILENAME_PDF));
		detailsProt.setNomeFile(FILENAME_PDF);
		detailsProt.setMimeType(MediaType.PDF.toString());

		detailsProt.setProtocollazioneMailGuid(guidMail);
		detailsProt.setMittenteContatto(contatto);
		detailsProt.setOggetto(mailOggetto);
		detailsProt.setIsNotifica(false);
		detailsProt.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0]); // Vuol dire che è in
																									// entrata

		detailsProt = protocollaDsr.initializeDetailDocumentForProtocollazioneDsr(detailsProt, utente); // in automatico
																										// assegna per
																										// competenza
																										// all'"ufficio
																										// FEPA"

		SalvaDocumentoRedParametriDTO parametri = new SalvaDocumentoRedParametriDTO();
		parametri.setInputMailGuid(guidMail);
		parametri.setModalita(Modalita.MODALITA_INS_MAIL);
		parametri.setContentVariato(true);

		EsitoSalvaDocumentoDTO esito = creaDocFacade.salvaDocumento(detailsProt, parametri, utente, ProvenienzaSalvaDocumentoEnum.FEPA_DSR);
		String dt = esito.getDocumentTitle();
		checkAssertion(esito.isEsitoOk(), "La dsr non è stata protocollata correttamente.");
		return dt;
	}

	private EsitoSalvaDocumentoDTO predisposizioneDichiarazione(final String dtDSRIngresso, String utenteFirmatario, Integer ufficioFirmatario, UtenteDTO destinatarioDTO) {
		UtenteDTO utentePredisposizione = getUtente(utenteFirmatario);
		utentePredisposizione.setIdUfficio(ufficioFirmatario.longValue());
		
		IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(utentePredisposizione.getFcDTO(), utentePredisposizione.getIdAoo());
		
		List<FascicoloDTO> fascicoli = fceh.getFascicoliDocumento(dtDSRIngresso, utentePredisposizione.getIdAoo().intValue());

		DetailDocumentRedDTO detailsPredisposizione = new DetailDocumentRedDTO();
		detailsPredisposizione = protocollaDsr.initializeDetailDocumentForProtocollazioneDsr(detailsPredisposizione,
				utentePredisposizione); // Esiste un analogo per predisponi dichiarazione?

		AssegnazioneDTO perFirma = createAssegnazioneFirma(utentePredisposizione.getIdUfficio().intValue(), utentePredisposizione.getId().intValue(),
				utentePredisposizione.getNome(), utentePredisposizione.getCognome());
		detailsPredisposizione.getAssegnazioni().add(perFirma);

		// Assegno per firma a DI NUZZO

		detailsPredisposizione.setContent(FileUtils.getFileFromInternalResources("dsr1.pdf"));
		detailsPredisposizione.setNomeFile("dsr1.pdf");
		detailsPredisposizione.setMimeType(MediaType.PDF.toString());

		DestinatarioRedDTO destinatario = createDestinatarioInterno(destinatarioDTO.getIdUfficio().intValue(), destinatarioDTO.getId().intValue(), ID_CONTATTO_IRENE, ModalitaDestinatarioEnum.TO);
		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		destinatari.add(destinatario);
		detailsPredisposizione.setDestinatari(destinatari);
		detailsPredisposizione.setOggetto("PREDISPOSIZIONE DSR");

		detailsPredisposizione.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_USCITA.getIds()[0]); // Vuol
																												// dire
																												// che è
																												// in
																												// uscita
		detailsPredisposizione.setIdTipologiaDocumento(409);
		detailsPredisposizione.setIdTipologiaProcedimento(288);

		detailsPredisposizione.setFascicoli(fascicoli);

		RispostaAllaccioDTO allaccio = new RispostaAllaccioDTO();
		Document docDSR = fceh.getDocumentByDTandAOO(dtDSRIngresso, utentePredisposizione.getIdAoo());
		
		Integer annoProtocolloDSRIngresso = (Integer)TrasformerCE.getMetadato(docDSR, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
		Integer numeroProtocolloDSRIngresso = (Integer)TrasformerCE.getMetadato(docDSR, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
				
		allaccio.setNumeroProtocollo(numeroProtocolloDSRIngresso);
		allaccio.setAnnoProtocollo(annoProtocolloDSRIngresso);
		allaccio.setIdDocumentoAllacciato(dtDSRIngresso);
		List<RispostaAllaccioDTO> allacci = new ArrayList<>();
		allacci.add(allaccio);
		detailsPredisposizione.setAllacci(allacci);

		SalvaDocumentoRedParametriDTO parametriPredisposizione = new SalvaDocumentoRedParametriDTO();
		parametriPredisposizione.setModalita(Modalita.MODALITA_INS);
		parametriPredisposizione.setContentVariato(true);
		EsitoSalvaDocumentoDTO esitoPredisposizione = creaDocFacade.salvaDocumento(
				detailsPredisposizione, parametriPredisposizione, utentePredisposizione, ProvenienzaSalvaDocumentoEnum.FEPA_DSR);
		checkAssertion(esitoPredisposizione.isEsitoOk(), "La dsr non è stata protocollata correttamente.");

		return esitoPredisposizione;
	}

}
