package it.ibm.business.test;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.filenet.api.core.Document;

import it.ibm.business.test.dao.IBonificaDAO;
import it.ibm.business.test.dto.BonificaMessaAgliAttiDTO;
import it.ibm.red.business.dto.DetailDocumentoDTO;
import it.ibm.red.business.dto.ParamsRicercaProtocolloDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.builder.Builder;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.INpsSRV;

/**
 * Classe di test servizi NPS.
 */
public class NPSTest extends AbstractTest {
 	
	/**
	 * Label RED.
	 */
	private static final String RED_LABEL = " RED>";

	/**
	 * Numero massimo item.
	 */
	private static final Integer NUMERO_MAX_ITEM = 300;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NPSTest.class.getName());
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public void initialize() {
		before();
	}
	
	private void mettiAgliAtti(final UtenteDTO utente, final String guidProtocollo, final Integer numeroProtocollo, final Integer annoProtocollo, final String idDocumento, final Connection con) {
		final String infoProtocollo = new StringBuilder()
				.append(numeroProtocollo).append("/").append(annoProtocollo).toString();
		
		final INpsSRV npsSRV = getBean(INpsSRV.class);
		npsSRV.cambiaStatoProtEntrata(guidProtocollo, infoProtocollo, utente, Builder.AGLI_ATTI, idDocumento, con);
	}
	
	/**
	 * Test di bonifica protocolli agli atti.
	 */
	@Test
	public void bonificaProtocolliAgliAtti() {
		
		Connection conn = null;
		List<BonificaMessaAgliAttiDTO> items = null;
		final IBonificaDAO bonificaDAO = getBean(IBonificaDAO.class);
		try {
			conn = createNewDBConnection();
			
			//leggi a blocchi di n
			items = bonificaDAO.recuperaItemsDaBonificare(NUMERO_MAX_ITEM, conn);
			
			commitConnection(conn);
			
		} catch (final Exception e) {
			LOGGER.error(e);
			rollbackConnection(conn);
			throw new RedException(e);
			
		} 
		
		for (final BonificaMessaAgliAttiDTO item: items) {
			
			try {
				
				conn = createNewDBConnection();
				
				//recupera utente
				final UtenteDTO utente = getUtente(item.getIdUtente(), item.getIdNodo());
				
				final String output = isValidRow(utente, item.getAnnoProtocollo(), item.getNumeroProtocollo(), item.getFlagIU(), item.getDocumentTitle());
				
				if (output.isEmpty()) {
					mettiAgliAtti(utente, item.getIdProtocollo(), item.getNumeroProtocollo(), item.getAnnoProtocollo(), item.getDocumentTitle(), conn);
					//aggiorna a ok l'item
					bonificaDAO.aggiornaItemBonificaProtocolliAgliAtti(item.getIdProtocollo(), 1, "Aggiornamento avvenuto con successo", conn);
				} else {
					bonificaDAO.aggiornaItemBonificaProtocolliAgliAtti(item.getIdProtocollo(), -1, "Errore in fase di aggiornamento: " + output, conn);
				}
				commitConnection(conn);
				
			} catch (final Exception e) {
				LOGGER.error(e);
				rollbackConnection(conn);
				//aggiorna a errore l'item
				aggiornaItemBonificaProtocolliAgliAtti(item.getIdProtocollo(), -1, "Errore in fase di aggiornamento: " + e.getMessage());
				
			} 
			
			
		}
		
	}

	private void aggiornaItemBonificaProtocolliAgliAtti(final String idProtocollo, final int stato, final String descrizione) {
		Connection conn = null;
		
		final IBonificaDAO bonificaDAO = getBean(IBonificaDAO.class);
		try {
			conn = createNewDBConnection();
			
			bonificaDAO.aggiornaItemBonificaProtocolliAgliAtti(idProtocollo, stato, descrizione, conn);
			
			commitConnection(conn);
			
		} catch (final Exception e) {
			LOGGER.error(e);
			rollbackConnection(conn);
			throw new RedException(e);
			
		} 
		
	}
	
	/**
	 * Esegue la validazione della riga.
	 * @param colUtente
	 * @param colAnnoProtocollo
	 * @param colNumeroProtocollo
	 * @param colFlagIU
	 * @param colDataProtocollo
	 * @param dt
	 * @return StringBuffer
	 */
	public final String isValidRow(final UtenteDTO colUtente, final Integer colAnnoProtocollo, final Integer colNumeroProtocollo, final String colFlagIU, final String dt) {
        Boolean out = true;
        final StringBuilder sb = new StringBuilder();
        try {
            //Recupero documento RED
            final DetailDocumentoDTO detailRED = getRedDetail(colUtente, dt);
			out = check(colNumeroProtocollo, colAnnoProtocollo, detailRED != null, "DOCUMENTO NON TROVATO SU RED", sb);
            //CHECK RED VS TEMPLATE
            
            out = out && check(colNumeroProtocollo, colAnnoProtocollo, detailRED.getAnnoProtocollo().equals(colAnnoProtocollo), "ANNO PROTOCOLLO: TEMPLATE>" +  colAnnoProtocollo + RED_LABEL + detailRED.getAnnoProtocollo(), sb);
            out = out && check(colNumeroProtocollo, colAnnoProtocollo, detailRED.getNumeroProtocollo().equals(colNumeroProtocollo), "NUMERO PROTOCOLLO: TEMPLATE>" +  colNumeroProtocollo + RED_LABEL + detailRED.getNumeroProtocollo(), sb);
            String flagIU = "I";
            if (detailRED.getIdCategoriaDocumento().equals(CategoriaDocumentoEnum.DOCUMENTO_USCITA.getIds()[0])) {
                flagIU = "U";
            }
            out = out && check(colNumeroProtocollo, colAnnoProtocollo, flagIU.equalsIgnoreCase(colFlagIU), "FLAG UI: TEMPLATE>" +  colFlagIU + RED_LABEL + flagIU, sb);
            //CHECK RED VS NPS
            
            final Collection<ProtocolloNpsDTO> detaislNPS = getNpsDetail(colNumeroProtocollo, colAnnoProtocollo, colUtente);
            out = out && check(colNumeroProtocollo, colAnnoProtocollo, detaislNPS.size() == 1, "Esiste più di un protocollo su NPS per questo anno e numero", sb);
            final ProtocolloNpsDTO detailNPS = detaislNPS.iterator().next();
            out = out && check(colNumeroProtocollo, colAnnoProtocollo, ("" + detailRED.getAnnoProtocollo()).equals(detailNPS.getAnnoProtocollo()), "Su NPS non coincide anno protocollo", sb);
            out = out && check(colNumeroProtocollo, colAnnoProtocollo, Integer.valueOf(detailNPS.getNumeroProtocollo()).equals(detailRED.getNumeroProtocollo()), "Su NPS non coincide numero protocollo", sb);
            String flagIUNPS = "I";
            if ("USCITA".equalsIgnoreCase(detailNPS.getTipoProtocollo())) {
                flagIUNPS = "U";
            }
            out = out && check(colNumeroProtocollo, colAnnoProtocollo, flagIUNPS.equals(flagIU), "Su NPS non coincide entrata/uscita protococllo", sb);
            out = out && check(colNumeroProtocollo, colAnnoProtocollo, "IN LAVORAZIONE".equalsIgnoreCase(detailNPS.getStatoProtocollo()), "Su NPS il documento non risulta in lavorazione", sb);
            
        } catch (final Exception e) {
        	LOGGER.error(e);
            out = false;
            check(colNumeroProtocollo, colAnnoProtocollo, false, "EXCEPTION CLASS: " + e.getClass() + " EXCEPTION MSG: " + e.getMessage(), sb);
        }
        LOGGER.error("Esito finale: " + out);
        return sb.toString();
    }
    
    private DetailDocumentoDTO getRedDetail(final UtenteDTO colUtente, final String dt) {
		final IFilenetCEHelper fce = FilenetCEHelperProxy.newInstance(colUtente.getFcDTO(), colUtente.getIdAoo());
        final Document documentForDetail = fce.getDocumentRedForDetail(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), dt);
        return TrasformCE.transform(documentForDetail, TrasformerCEEnum.FROM_DOCUMENTO_TO_DETAIL);
    }
    
    private Collection<ProtocolloNpsDTO> getNpsDetail(final Integer numeroProtocollo, final Integer annoProtocollo, final UtenteDTO utente) {
        final ParamsRicercaProtocolloDTO dp = new ParamsRicercaProtocolloDTO();
        dp.setAnnoProtocollo(annoProtocollo);
        final Integer numeroProtocolloDa = numeroProtocollo;
        final Integer numeroProtocolloA = numeroProtocollo;
        dp.setNumeroProtocolloA(numeroProtocolloA);
        dp.setNumeroProtocolloDa(numeroProtocolloDa);
        final INpsSRV npsSRV = getBean(INpsSRV.class);
        return npsSRV.ricercaProtocolli(dp, utente);
    }
    
    private static boolean check(final Integer colNumeroProtocollo, final Integer colAnnoProtocollo, final Boolean mustBeTrue, final String msg, final StringBuilder sb) {
        boolean out = true;
        if (Boolean.FALSE.equals(mustBeTrue)) {
            LOGGER.error("[" + colNumeroProtocollo + "/" + colAnnoProtocollo + "] " + msg);
            sb.append(msg + ";");
            out = false;
        }
        return out;
    }

	
}
