package it.ibm.business.test.suite;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.net.MediaType;

import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IIgepaFacadeSRV;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Classe di test Igepa.
 */
public class IgepaWSTest extends AbstractTest {

	/**
	 * Data valida per test.
	 */
	private static final String MOCK_DATE = "19/12/2018";

	/**
	 * Nome del file di test.
	 */
    private static final String FILE_TEST_NAME = "fileTest.pdf";
	
    /**
     * Servizio.
     */
	private IIgepaFacadeSRV igepaSRV;

	/**
	 * Metodo preliminare al test.
	 */
	@Before
	public final void beforeTest() {
		before();
		igepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IIgepaFacadeSRV.class);
	}
	
	/**
	 * Metodo succesivo al test.
	 */
	@After
	public final void afterTest() {
		after();
	}
	
	/**
	 * Test di acquisizione DCP.
	 */
//	@Test
	public final void acquisizioneDCPTest() {
		String iddocumento = igepaSRV.insertDocumento("test igepa ws", "2019_A4305_o_COMUNE_20170307093514.pdf", ID_IGB, ID_MAZZOTTA,
				124312, MOCK_DATE, FileUtils.getFileFromInternalResources(FILE_TEST_NAME),
				MediaType.PDF.toString(), FileUtils.getFileFromInternalResources(FILE_TEST_NAME), MediaType.PDF.toString(), FILE_TEST_NAME, "test");
		checkAssertion(!StringUtils.isNullOrEmpty(iddocumento), "Il document title della dcp non può essere null.");
	}
	
	/**
	 * Test per l'allegato IGEPA.
	 */
//	@Test
	public final void allegatoIgepaTest() {
		String iddocumento = igepaSRV.allegaRichiestaIgepa("2018", 1, 1, "test", 1, FILE_TEST_NAME, FileUtils.getFileFromInternalResources(FILE_TEST_NAME), MediaType.PDF.toString());
		checkAssertion(!StringUtils.isNullOrEmpty(iddocumento), "Il document title dell'allegato non può essere null.");
	}
	
	/**
	 * Test di invio richiesta OPF IGEPA.
	 */
	@Test
	public final void inviaRichiestaOPFIgepaTest() {
		String iddocumento = igepaSRV.insertDocumentoOPF(ID_IGB, ID_MAZZOTTA, FileUtils.getFileFromInternalResources(FILE_TEST_NAME), MediaType.PDF.toString(), 1, MOCK_DATE, "B.05", "1", "1", MOCK_DATE, 1, 70, 319, "test", "MEF_MEF", FILE_TEST_NAME);
		checkAssertion(!StringUtils.isNullOrEmpty(iddocumento), "Il document title dell'opf non può essere null.");
	}

}
