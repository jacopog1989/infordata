package it.ibm.business.test.suite;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Classe di test dei contributi.
 */
public class ContributiTest extends AbstractTest {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContributiTest.class.getName());

	/**
	 * Label document title.
	 */
	private static final String DOC_TITLE = "DOCUMENT TITLE: ";
	
	/**
	 * Label oggetto.
	 */
	private static final String OGGETTO_LABEL = "OGGETTO: ";
	
	/**
	 * Label numero doc.
	 */
	private static final String NUM_DOC = "NUM DOC: ";
	
	/**
	 * Messaggio errore creazione documento in ingresso.
	 */
	private static final String ERROR_CREAZIONE_DOCUMENTO_MSG = "Errore in fase di creazione documento in ingresso.";
	
	/**
	 * Messaggio errore richiesta contributo.
	 */
	private static final String ERROR_RICHIESTA_CONTRIBUTO_MSG = "Errore in fase di richiesta contributo.";
	
	/**
	 * Esegue le azioni prima del test.
	 */
	@Before
	public final void beforeTest() {
		before();
	}
	
	/**
	 * Esegue le azioni dopo il test.
	 */
	@After
	public final void afterTest() {
		after();
	}
	
	/**
	 * Test contributo 1.
	 */
	@Test
	public final void test1() {
		//Creazoine documento
		UtenteDTO utente = getUtente(USERNAME_NUNZI);
		Boolean daNonProtocollare = true;
		String indiceFascicoloProcedimentale = null;

		List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneCompetenzaUfficio(ID_IGF, DESC_IGF));

		String oggetto = createObj();

		Boolean riservato = true;
		Integer idMezzoRicezione = ID_MEZZO_RICEZIONE_POSTA_ELET_DA_PEC_PER_ELETTRONICI;
		List<AllegatoDTO> allegati = null;
		EsitoSalvaDocumentoDTO esito = createDocIngresso(allegati, oggetto, utente, ID_CONTATTO_IRENE, FormatoDocumentoEnum.ELETTRONICO, ID_TIPO_DOC_GENERICO_RGS_ENTRATA, ID_TIPO_PROC_GENERICO_RGS_ENTRATA, assegnazioni, daNonProtocollare, idMezzoRicezione, indiceFascicoloProcedimentale, riservato);

		String dt = esito.getDocumentTitle();
		Integer numDoc = getNumDoc(USERNAME_TANZI, dt);
		
		checkAssertion(esito.isEsitoOk(), ERROR_CREAZIONE_DOCUMENTO_MSG);
		
		//CORRIERE giangranco.tanzi chiede contributo ad IGB
		
		EsitoOperazioneDTO esitoRichiediContributo = richiediContributo(USERNAME_TANZI, dt, ID_IGB);
		checkAssertion(esitoRichiediContributo.getFlagEsito(), ERROR_RICHIESTA_CONTRIBUTO_MSG);
		
		//richiedi contributo interno e metti IGB UFFICIO I
		
		EsitoOperazioneDTO esitoRichiediContributoInterno = richiediContributoInterno(USERNAME_MAZZOTTA, dt, ID_IGB_UFFICIO_I);
		checkAssertion(esitoRichiediContributoInterno.getFlagEsito(), "Errore in fase di richiesta contributo interno.");
		
		//Richiedi contributo utente a biagio mazzotta
		
		EsitoOperazioneDTO esitoRichiediContributoUtente = richiediContributoUtente(USERNAME_RICCARDI, dt, ID_IGB_UFFICIO_I, ID_RICCARDI);
		checkAssertion(esitoRichiediContributoUtente.getFlagEsito(), "Errore in fase di richiesta contributo utente.");
		
		waitSeconds(60);
		
		//a chi è stato chiesto il contributo fa inserisci contributo.
		EsitoOperazioneDTO esitoInserisciContributo = inserisciContributo(USERNAME_RICCARDI, dt);
		checkAssertion(esitoInserisciContributo.getFlagEsito(), "Errore in fase di inserimento contributo.");

		waitSeconds(60);
		
		//Verifica contributo ritorna ufficio che ha richiesto ad utente e deve fare valida.
		EsitoOperazioneDTO esitoVerificaContributo = validaContributi(USERNAME_RICCARDI, dt);
		checkAssertion(esitoVerificaContributo.getFlagEsito(), "Errore in fase di valida contributi.");

		waitSeconds(60);

		EsitoOperazioneDTO esitoValidazioneContributo = validazioneContributo(USERNAME_MAZZOTTA, dt);
		checkAssertion(esitoValidazioneContributo.getFlagEsito(), "Errore in fase di validazione contributo.");

		LOGGER.info(DOC_TITLE + dt);
		LOGGER.info(OGGETTO_LABEL + oggetto);
		LOGGER.info(NUM_DOC + numDoc);
	}

	/**
	 * Test contributo 2.
	 */
	@Test
	public final void test2() {
		dumpDTInfo("39107");
		
		//Creazoine documento
		UtenteDTO utente = getUtente(USERNAME_NUNZI);
		Boolean daNonProtocollare = true;
		String indiceFascicoloProcedimentale = null;

		List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneCompetenzaUfficio(ID_IGF, DESC_IGF));

		String oggetto = createObj();

		Boolean riservato = true;
		Integer idMezzoRicezione = ID_MEZZO_RICEZIONE_POSTA_ELET_DA_PEC_PER_ELETTRONICI;
		List<AllegatoDTO> allegati = null;
		EsitoSalvaDocumentoDTO esito = createDocIngresso(allegati, oggetto, utente, ID_CONTATTO_IRENE, FormatoDocumentoEnum.ELETTRONICO, ID_TIPO_DOC_GENERICO_RGS_ENTRATA, ID_TIPO_PROC_GENERICO_RGS_ENTRATA, assegnazioni, daNonProtocollare, idMezzoRicezione, indiceFascicoloProcedimentale, riservato);

		String dt = esito.getDocumentTitle();
		Integer numDoc = getNumDoc(USERNAME_TANZI, dt);
		
		checkAssertion(esito.isEsitoOk(), ERROR_CREAZIONE_DOCUMENTO_MSG);
		
		//CORRIERE giangranco.tanzi chiede contributo ad IGB
		
		EsitoOperazioneDTO esitoRichiediContributo = richiediContributo(USERNAME_TANZI, dt, ID_IGB);
		checkAssertion(esitoRichiediContributo.getFlagEsito(), ERROR_RICHIESTA_CONTRIBUTO_MSG);
		
		//richiedi contributo interno e metti IGB UFFICIO I
		
		EsitoOperazioneDTO esitoRichiediContributoInterno = richiediContributoInterno(USERNAME_MAZZOTTA, dt, ID_IGB);
		checkAssertion(esitoRichiediContributoInterno.getFlagEsito(), "Errore in fase di richiesta contributo interno.");
		
		//Richiedi contributo utente a biagio mazzotta
		
		EsitoOperazioneDTO esitoRichiediContributoUtente = richiediContributoUtente(USERNAME_MAZZOTTA, dt, ID_IGB, ID_MAZZOTTA);
		checkAssertion(esitoRichiediContributoUtente.getFlagEsito(), "Errore in fase di richiesta contributo utente.");
		
		LOGGER.info(DOC_TITLE + dt);
		LOGGER.info(OGGETTO_LABEL + oggetto);
		LOGGER.info(NUM_DOC + numDoc);
	}
	
	/**
	 * Test contributo 3.
	 */
	@Test
	public final void test3() {
		//Creazoine documento
		UtenteDTO utente = getUtente(USERNAME_MAZZOTTA);
		Boolean daNonProtocollare = true;
		String indiceFascicoloProcedimentale = null;

		List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
		assegnazioni.add(createAssegnazioneCompetenzaUfficio(ID_IGB, DESC_IGB));

		String oggetto = createObj();

		Boolean riservato = true;
		Integer idMezzoRicezione = ID_MEZZO_RICEZIONE_POSTA_ELET_DA_PEC_PER_ELETTRONICI;
		List<AllegatoDTO> allegati = null;
		EsitoSalvaDocumentoDTO esito = createDocIngresso(allegati, oggetto, utente, ID_CONTATTO_IRENE, FormatoDocumentoEnum.ELETTRONICO, ID_TIPO_DOC_GENERICO_RGS_ENTRATA, ID_TIPO_PROC_GENERICO_RGS_ENTRATA, assegnazioni, daNonProtocollare, idMezzoRicezione, indiceFascicoloProcedimentale, riservato);

		String dt = esito.getDocumentTitle();
		Integer numDoc = getNumDoc(USERNAME_MAZZOTTA, dt);
		
		checkAssertion(esito.isEsitoOk(), ERROR_CREAZIONE_DOCUMENTO_MSG);
		
		EsitoOperazioneDTO esitoAssegna = assegnaDaCorriere(USERNAME_MAZZOTTA, dt, USERNAME_MAZZOTTA);
		checkAssertion(esitoAssegna.getFlagEsito(), "Errore in fase di assegnazione da corriere.");
		
		EsitoOperazioneDTO esitoRichiestaContributo = richiestaContributo(USERNAME_MAZZOTTA, dt, USERNAME_BONTEMPI);
		checkAssertion(esitoRichiestaContributo.getFlagEsito(), ERROR_RICHIESTA_CONTRIBUTO_MSG);

		LOGGER.info(DOC_TITLE + dt);
		LOGGER.info(OGGETTO_LABEL + oggetto);
		LOGGER.info(NUM_DOC + numDoc);
	}
	
	
	
}
