package it.ibm.business.test.asign;

import java.sql.Connection;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.dao.IGestioneNotificheEmailDAO;
import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.enums.SignStrategyEnum;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * @author s.lungarella.ibm
 */

public class Test08StepInvioMail extends AbstractAsignTest {
	
	/**
	 * Dao gestione notifiche.
	 */
	private IGestioneNotificheEmailDAO gestioneMailDAO;
	
	/**
	 * Esegue le azioni necessarie prima dell'esecuzione del test.
	 */
	@Before
	public final void beforeTest() {
		before();
		gestioneMailDAO = ApplicationContextProvider.getApplicationContext().getBean(IGestioneNotificheEmailDAO.class);
	
	}

	/**
	 * Esegue le azioni necessarie dopo l'esecuzione del test.
	 */
	@After
	public final void afterTest() {
		after();
	}

	/**
	 * Test di esecuzione step di invio mail.
	 */
	@Test
	public final void eseguiStepSuDocNonProtocollato() {
		
		ASignItemDTO itemPre = accoda(USERNAME_RGS, ID_TIPO_DOC_GENERICO_RGS_USCITA, ID_TIPO_PROC_GENERICO_RGS_USCITA, SignStrategyEnum.ASYNC_STRATEGY_B, null);
		// Esecuzione step antecedenti ad Invio Mail
		run(StepEnum.PROTOCOLLAZIONE, itemPre.getId());
		run(StepEnum.REG_AUX_PROTOCOLLAZIONE, itemPre.getId());
		run(StepEnum.STAMPIGLIATURE, itemPre.getId());
		run(StepEnum.AGGIORNAMENTO_METADATI, itemPre.getId());
		run(StepEnum.GESTIONE_ALLACCI, itemPre.getId());
		run(StepEnum.AVANZAMENTO_PROCESSI, itemPre.getId());
		
		// Esecuzione invio mail
		run(StepEnum.INVIO_MAIL, itemPre.getId());

		ASignItemDTO itemPost = getaSignSRV().getItem(itemPre.getId(), true);
		Connection connection = createNewDBConnection();
		// Recupero mail inserita in coda
		MessaggioEmailDTO mailPost = gestioneMailDAO.getMail(itemPost.getDocumentTitle(), new Date(), connection);
		checkAssertion(mailPost != null, "Mail non esistente in coda mail");
		
	}
}
