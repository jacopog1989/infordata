/**
 * @author a.dilegge
 * 
 * Il package conterrà i job che implementano i singoli flussi verticali di protocollazione PEC
 * 
 */
package it.ibm.red.batch.jobs.protocollazione.pec;