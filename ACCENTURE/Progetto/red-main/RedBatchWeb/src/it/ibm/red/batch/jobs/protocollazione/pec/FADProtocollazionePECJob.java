package it.ibm.red.batch.jobs.protocollazione.pec;

import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IProtocollaPECFADSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Job FAD Protocollazione PEC.
 */
public class FADProtocollazionePECJob extends AbstractProtocollazionePECJob {

	/**
	 * Call ID.
	 */
	@Override
	protected void setCallID() {
		callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();
	}

	/**
	 * LOGGER.
	 */
	@Override
	protected void setLogger() {
		logger = REDLogger.getLogger(FADProtocollazionePECJob.class);
	}

	/**
	 * Imposta la property key radix.
	 */
	@Override
	protected void setPropertyKeyRadix() {
		propertyKeyRadix = "protocollazione.automatica.pec.fad.";
	}

	/**
	 * Inizializza il service ProtocollaSRV.
	 */
	@Override
	protected void setProtocollaSRV() {
		protocollaSRV = ApplicationContextProvider.getApplicationContext().getBean(IProtocollaPECFADSRV.class);
	}
	
}
