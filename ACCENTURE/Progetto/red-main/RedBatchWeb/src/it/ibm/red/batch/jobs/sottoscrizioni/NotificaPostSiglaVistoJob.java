package it.ibm.red.batch.jobs.sottoscrizioni;

import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.INotificaPostSiglaVistoSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Classe NotificaPostSiglaVistoJob.
 */
public class NotificaPostSiglaVistoJob extends AbstractNotificaJob {

	/**
	 * @see it.ibm.red.batch.jobs.sottoscrizioni.AbstractNotificaJob#setCallID().
	 */
	@Override
	protected void setCallID() {
		callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();
	}

	/**
	 * @see it.ibm.red.batch.jobs.sottoscrizioni.AbstractNotificaJob#setLogger().
	 */
	@Override
	protected void setLogger() {
		logger = REDLogger.getLogger(NotificaPostSiglaVistoJob.class);
	}
	
	/**
	 * @see it.ibm.red.batch.jobs.sottoscrizioni.AbstractNotificaJob#setNotificaSRV().
	 */
	@Override
	protected void setNotificaSRV() {
		notificaSRV = ApplicationContextProvider.getApplicationContext().getBean(INotificaPostSiglaVistoSRV.class);
	}

}