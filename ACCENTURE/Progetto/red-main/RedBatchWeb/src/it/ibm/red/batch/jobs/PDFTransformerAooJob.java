package it.ibm.red.batch.jobs;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.dto.CodaTrasformazioneDTO;
import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.enums.StatoCodaTrasformazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.ICodaMailSRV;
import it.ibm.red.business.service.facade.ITrasformazionePDFFacadeSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Job che si occupa della trasformazione pdf asincrona dei documenti.
 */
public class PDFTransformerAooJob implements Job {

	/**
	 * Chiamante
	 */
	private final String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();

	/**
	 * LOGGER
	 */
	private final REDLogger logger = REDLogger.getLogger(PDFTransformerAooJob.class);

	/**
	 * Punto di esecuzione del job di trasformazione pdf.
	 *
	 * @param context the context
	 * @throws JobExecutionException the job execution exception
	 */
	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {

		logger.info("-------- START " + callID + " --------");

		try {
			
			final JobDataMap data = context.getJobDetail().getJobDataMap();
			final JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
			final int idAoo = jobDB.getIdAoo();
			
			logger.info(callID + " trasformazione PDF per l'aoo " + idAoo);

			final ITrasformazionePDFFacadeSRV trasformazionePDFSRV = ApplicationContextProvider.getApplicationContext().getBean(ITrasformazionePDFFacadeSRV.class);
			final CodaTrasformazioneDTO codaTrasformazione = trasformazionePDFSRV.getItem(idAoo);

			if (codaTrasformazione != null) {

				eseguiTrasformazione(trasformazionePDFSRV, codaTrasformazione);
			} else {
				logger.info("Nessuna Trasformazione da eseguire");
			}

		} catch (final Exception e) {
			logger.error(callID + " errore generico", e);
		}

		logger.info("-------- END " + callID + " --------");

	}

	/**
	 * @param trasformazionePDFSRV
	 * @param codaTrasformazione
	 */
	private void eseguiTrasformazione(final ITrasformazionePDFFacadeSRV trasformazionePDFSRV, final CodaTrasformazioneDTO codaTrasformazione) {
		try {
			// trasforma
			final long now = System.currentTimeMillis();
			trasformazionePDFSRV.doTransformation(codaTrasformazione.getParametri());
			final long secondElapsed = System.currentTimeMillis() - now;

			// nessun errore, item done
			trasformazionePDFSRV.setItemDone(codaTrasformazione.getIdCoda(), secondElapsed);

			logger.info("Trasformazione del documento " + codaTrasformazione.getIdDocumento() + " eseguita con successo");

			if (codaTrasformazione.getParametri().getIdNotificaSped() != null) {
				final ICodaMailSRV codaMailSRV = ApplicationContextProvider.getApplicationContext().getBean(ICodaMailSRV.class);
				codaMailSRV.updateNotifica(codaTrasformazione.getParametri().getIdNotificaSped());
				logger.info("Invio email spedizione iter manuale");
			}
		} catch (final Exception e) {
			logger.error("Si è verificato un errore nella trasformazione PDF del documento " + codaTrasformazione.getIdDocumento(), e);
			trasformazionePDFSRV.changeStatusItem(codaTrasformazione.getIdCoda(), StatoCodaTrasformazioneEnum.ERRORE, e.getMessage());
		}
	}
	
}