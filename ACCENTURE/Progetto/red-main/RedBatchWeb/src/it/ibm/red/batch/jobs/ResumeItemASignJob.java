package it.ibm.red.batch.jobs;

import java.util.List;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IASignOrchestrator;
import it.ibm.red.business.service.IASignSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * @author SimoneLungarella
 * Job che si occupa dell'esecuzione della firma asincrona nei casi di Retry.
 */
public class ResumeItemASignJob implements Job{

	/**
	 * Id.
	 */
	private String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();
	
	/**
	 * LOGGER.
	 */
	private REDLogger logger = REDLogger.getLogger(ResumeItemASignJob.class);
	
	/**
	 * Logica di esecuzione RESUME degli item di firma asincrona.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.info("-------- START "+ callID + " --------");

		try {
			JobDataMap data = context.getJobDetail().getJobDataMap();
			JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
			int idAoo = jobDB.getIdAoo();
			
			IASignOrchestrator orchestrator = ApplicationContextProvider.getApplicationContext().getBean(IASignOrchestrator.class);
			IASignSRV aSignSRV = ApplicationContextProvider.getApplicationContext().getBean(IASignSRV.class);
			
			// Esecuzione di resume su tutti gli item resumable
			List<ASignItemDTO> itemsToResume = aSignSRV.getResumableItems(Long.valueOf(idAoo));
			for (ASignItemDTO item : itemsToResume) {
				orchestrator.resume(item);
			}
			
		}catch (Exception e) {
			logger.error(callID + " errore generico", e);
		}

		logger.info("-------- END "+ callID + " --------");
	}
}
