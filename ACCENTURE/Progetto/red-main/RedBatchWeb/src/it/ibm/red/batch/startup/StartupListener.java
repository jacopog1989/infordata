package it.ibm.red.batch.startup;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import it.ibm.red.batch.exception.RedSchedulerException;
import it.ibm.red.batch.scheduler.RedScheduler;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.WebServiceClientProvider;

/**
 * StartupListener che gestisce l'avvio dell'applicazione.
 */
public class StartupListener extends it.ibm.red.business.startup.BusinessStartupListener implements ServletContextListener {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(StartupListener.class.getName());
	
	/**
	 * Distrugge lo scheduler @see RedScheduler.
	 */
	@Override
	public final void contextDestroyed(final ServletContextEvent event) {
		LOGGER.info("STARTUP LISTENER contextDestroyed(): Listener context destroyed in esecuzione...");
		try {
			RedScheduler.getInstance().destroyScheduler();
		} catch (RedSchedulerException e) {
			LOGGER.error("Errore durante lo STARTUP LISTENER contextDestroyed():", e);
		}
	}
 
	/**
	 * Inizializza il context.
	 */
	@Override 
	public final void contextInitialized(final ServletContextEvent event) {
		ApplicationContextProvider.setApplicationContext(event);
		loadLoggingProperties(PropertiesNameEnum.RED_BATCH_LOGGING_PROPERTIES);
		loadRedScheduler();
		WebServiceClientProvider.resetInstance();
		LOGGER.info("ServletContext implementation major version: " + event.getServletContext().getMajorVersion() + "." + event.getServletContext().getMinorVersion());
		LOGGER.info("ServletContext info: " + event.getServletContext().getServerInfo());
		LOGGER.info("STARTUP LISTENER contextInitialized(): Listener context initialized in esecuzione...");
	}
	
	/**
	 * Inizializza lo scheduler: @see RedScheduler.
	 */
	private void loadRedScheduler() {
		LOGGER.info("Inizializzazione RedScheduler");
		try {
			RedScheduler.getInstance().startScheduler();
			LOGGER.info("RedScheduler inizializzato.");
		} catch (RedSchedulerException e) {
			LOGGER.error("Errore durante lo STARTUP LISTENER loadRedScheduler():", e);
		}
	}

} 