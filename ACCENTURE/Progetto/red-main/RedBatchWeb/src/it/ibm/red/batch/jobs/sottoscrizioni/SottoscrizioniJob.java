package it.ibm.red.batch.jobs.sottoscrizioni;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.INotificaSRV;
import it.ibm.red.business.service.facade.INotificaFacadeSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Job che gestisce le sottoscrizioni.
 */
public class SottoscrizioniJob implements Job {

	/**
	 * Caller.
	 */
	private final String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(SottoscrizioniJob.class);

	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext).
	 */
	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {

		logger.info("-------- START " + callID + " --------");

		try {

			final INotificaFacadeSRV notificaSRV = ApplicationContextProvider.getApplicationContext().getBean(INotificaSRV.class);

			final JobDataMap data = context.getJobDetail().getJobDataMap();
			final JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
			final int idAoo = jobDB.getIdAoo();

			logger.info(callID + " invio notifiche sottoscrizioni per l'aoo " + idAoo);

			notificaSRV.inviaNextNotifica(idAoo);

		} catch (final Exception e) {
			logger.error(callID + " errore generico", e);
		}

		logger.info("-------- END " + callID + " --------");

	}

}