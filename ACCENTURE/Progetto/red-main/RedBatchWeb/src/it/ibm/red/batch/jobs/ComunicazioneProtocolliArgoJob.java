package it.ibm.red.batch.jobs;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.dto.ComunicazioneProtocolloArgoDTO;
import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IComunicazioneProtocolliArgoFacadeSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Job che si occupa della comunicazione protocolli Argo.
 */
public class ComunicazioneProtocolliArgoJob implements Job {

	/**
	 * Caller.
	 */
	private final String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(ComunicazioneProtocolliArgoJob.class);

	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext).
	 */
	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {
		logger.info("-------- START " + callID + " --------");

		try {

			final JobDataMap data = context.getJobDetail().getJobDataMap();
			final JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
			final long idAoo = jobDB.getIdAoo();

			logger.info(callID + " - AOO: " + idAoo);

			final IComunicazioneProtocolliArgoFacadeSRV comunicazioneProtocolliArgoSRV = ApplicationContextProvider.getApplicationContext()
					.getBean(IComunicazioneProtocolliArgoFacadeSRV.class);

			final ComunicazioneProtocolloArgoDTO item = comunicazioneProtocolliArgoSRV.recuperaComunicazioneProtocolloDaLavorare(idAoo);

			if (item != null) {

				// Processa l'item
				final boolean responseArgoOk = comunicazioneProtocolliArgoSRV.elaboraComunicazioneProtocollo(item);

				if (responseArgoOk) {
					logger.info(callID + " - Elaborazione dell'item numero " + item.getIdCoda() + " eseguita con successo");
				}

			} else {
				logger.info(callID + " - Nessun item da elaborare");
			}

		} catch (final Exception e) {
			logger.error(callID + " - Errore generico", e);
		}

		logger.info("-------- END " + callID + " --------");
	}

}