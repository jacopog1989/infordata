package it.ibm.red.batch.jobs.sottoscrizioni;

import java.util.List;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.dto.NotificaEmailDTO;
import it.ibm.red.business.enums.StatoCodaEmailEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo; 
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.ICodaMailSRV;
import it.ibm.red.business.service.INotificaMancataSpedizioneSRV;
import it.ibm.red.business.service.facade.IAooFacadeSRV;
import it.ibm.red.business.utils.CollectionUtils;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Job di gestione notifica mancata spedizione.
 */
public class NotificaMancataSpedizioneJob implements Job{
	
	/**
	 * Caller ID.
	 */
	private String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();
	
	/**
	 * LOGGER.
	 */
	private REDLogger logger = REDLogger.getLogger(NotificaMancataSpedizioneJob.class);
	
	/**
	 * Execute del job.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
 		logger.info("-------- START "+ callID + " --------");
		
		try {
			JobDataMap data = context.getJobDetail().getJobDataMap();
			JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
			long idAoo = jobDB.getIdAoo();
			
			IAooFacadeSRV aooSRV = ApplicationContextProvider.getApplicationContext().getBean(IAooFacadeSRV.class);
			ICodaMailSRV codaMailSRV = ApplicationContextProvider.getApplicationContext().getBean(ICodaMailSRV.class);
			 
			Aoo aoo = aooSRV.recuperaAoo((int)idAoo);
			logger.info(callID + " aoo " + idAoo);
			List<NotificaEmailDTO> notifiche = codaMailSRV.getNotificheByStatoRicevuta(idAoo, StatoCodaEmailEnum.ATTESA_SPEDIZIONE_NPS,aoo.getGiorniNotificaMancataSpedizione());
			  
			if (!CollectionUtils.isEmptyOrNull(notifiche)) {
				for(NotificaEmailDTO notifica : notifiche)	{
					updateDataProcessamento(idAoo, notifica);
				} 
			}  
		} catch (Exception e) {
			logger.error(callID + " errore generico", e);
		}

		logger.info("-------- END "+ callID + " --------");
	}

	/**
	 * Esegue l'update della data processamento della notifica.
	 * @param idAoo
	 * @param notifica
	 */
	private void updateDataProcessamento(long idAoo, NotificaEmailDTO notifica) {
		
		ICodaMailSRV codaMailSRV = ApplicationContextProvider.getApplicationContext().getBean(ICodaMailSRV.class);
		INotificaMancataSpedizioneSRV notificaMancataSpedizioneSRV = ApplicationContextProvider.getApplicationContext().getBean(INotificaMancataSpedizioneSRV.class);
		try {
			notificaMancataSpedizioneSRV.writeNotifiche((int) idAoo, notifica);
			codaMailSRV.upateDataProcessamentoByDT(idAoo, notifica.getIdDocumento());
			logger.info("Elaborazione dell'item " + notifica.getIdMessaggio() + " eseguita con successo");
		} catch(Exception ex) {
			logger.error("Errore scrittura notifica : " + ex);
		}
	}
}
