package it.ibm.red.batch.scheduler;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import it.ibm.red.batch.exception.RedSchedulerException;
import it.ibm.red.batch.jobs.RedJobsOrchestrator;
import it.ibm.red.business.logger.REDLogger;

/**
 * Schedulatore dei job di Evo.
 */
public final class RedScheduler {

	/**
	 * Separatore per migliorare la leggibilita in console.
	 */
	private static final String LOG_LINE_SEPARATOR = "##############################################################";

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(RedScheduler.class);

	/**
	 * RedScheduler.
	 */
	private static RedScheduler instance = null;
	
	/**
	 * Scheduler.
	 */
	private Scheduler scheduler = null;

	private RedScheduler() {
	}

	/**
	 * Restituisce l'instance dello Scheduler, se null ne crea una nuova.
	 * 
	 * @return instance dello Scheduler
	 */
	public static RedScheduler getInstance() {
		if (instance == null) {
			instance = new RedScheduler();
		}

		return instance;
	}

	/**
	 * Esegue lo shutdown dello scheduler.
	 * 
	 * @throws RedSchedulerException
	 */
	public void destroyScheduler() throws RedSchedulerException {
		if (scheduler != null) {
			try {
				scheduler.shutdown(true);
				logger.info(LOG_LINE_SEPARATOR);
				logger.info("#############        DESTROY SCHEDULER           #############");
				logger.info(LOG_LINE_SEPARATOR);
			} catch (final SchedulerException e) {
				logger.error("Errore durante il rilascio dei Job ", e);
				throw new RedSchedulerException(e);
			}
		}
	}

	/**
	 * Avvia lo scheduler.
	 * 
	 * @throws RedSchedulerException
	 */
	public void startScheduler() throws RedSchedulerException {
		JobDetail demoneJob = null;
		Trigger trigger = null;

		try {
			destroyScheduler();

			demoneJob = JobBuilder.newJob(RedJobsOrchestrator.class).withIdentity("demoneJob", "demoneGroup").build();

			trigger = TriggerBuilder.newTrigger().withIdentity("demoneJobTrig", "demoneGroup").withSchedule(CronScheduleBuilder.cronSchedule("0 0/1 * * * ?")).build();

			scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.scheduleJob(demoneJob, trigger);
			scheduler.start();
			logger.info(LOG_LINE_SEPARATOR);
			logger.info("#############         START SCHEDULER            #############");
			logger.info("#############    START DEMONE GESTORE JOBS       #############");
			logger.info(LOG_LINE_SEPARATOR);

		} catch (final Exception e) {
			logger.error("Errore durante lo start dei Job ", e);
			throw new RedSchedulerException(e);
		}
	}

}
