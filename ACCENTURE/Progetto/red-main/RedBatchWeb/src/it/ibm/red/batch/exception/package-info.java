/**
 * Il package contiene tutte le eccezioni custom di Red Batch
 */
/**
 * @author a.dilegge
 *
 */
package it.ibm.red.batch.exception;