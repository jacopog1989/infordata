package it.ibm.red.batch.jobs;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.ICodeDocumentiSRV;
import it.ibm.red.business.service.facade.ICodeDocumentiFacadeSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Job che gestisce l'archiviazione code applicative.
 */
public class ArchiviazioneCodeApplicativeJob implements Job {

	/**
	 * Call id.
	 */
	private final String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();

	/**
	 * Logger.
	 */
	REDLogger logger = REDLogger.getLogger(ArchiviazioneCodeApplicativeJob.class);
	
	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext).
	 */
	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {
		
		logger.info("-------- START " + callID + " --------");
		
		try {
			
			final ICodeDocumentiFacadeSRV codeDocumentiSRV = ApplicationContextProvider.getApplicationContext().getBean(ICodeDocumentiSRV.class);
						
			//archiviazione delle code applicative
			codeDocumentiSRV.archiviaCodeApplicative();
			
			logger.info(callID + " archiviazione eseguita");
						
				
		} catch (final Exception e) {
			logger.error(callID + " errore generico", e);
		} 
		
		logger.info("-------- END " + callID + " --------");
		
	}
	
}
