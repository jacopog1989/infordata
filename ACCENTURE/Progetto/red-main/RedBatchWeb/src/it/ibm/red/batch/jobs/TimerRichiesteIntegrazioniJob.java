package it.ibm.red.batch.jobs;

import java.util.Collection;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.dto.TimerRichiesteIntegrazioniDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ITimerRichiesteIntegrazioniFacadeSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * @author d.ventimiglia
 * 
 * 	Batch di gestione documenti con timestamp impostato
 */
public class TimerRichiesteIntegrazioniJob implements Job {

	/**
	 * Caller.
	 */
	private final String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();
	
	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(TimerRichiesteIntegrazioniJob.class);

	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext).
	 */
	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {
		logger.info("-------- START " + callID + " --------");
		
		try {
			
			final JobDataMap data = context.getJobDetail().getJobDataMap();
			final JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
			final long idAoo = jobDB.getIdAoo();
			
			logger.info(callID + " aoo " + idAoo);
			
			final ITimerRichiesteIntegrazioniFacadeSRV timerSRV = ApplicationContextProvider.getApplicationContext().getBean(ITimerRichiesteIntegrazioniFacadeSRV.class);
			
			final Collection<TimerRichiesteIntegrazioniDTO> itemsToProcess = timerSRV.getNextDaLavorare();
			
			for (final TimerRichiesteIntegrazioniDTO item : itemsToProcess) {
				timerSRV.elaboraItem(item, idAoo);
			}
			
		} catch (final Exception e) {
			logger.error(callID + " errore generico", e);
		}
		logger.info("-------- END " + callID + " --------");
	}
}
