package it.ibm.red.batch.jobs;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.filenet.api.collection.ContentElementList;
import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;
import com.google.common.net.MediaType;

import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.StatoVerificaFirmaEnum;
import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.ISignSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.concrete.SignSRV.VerificaFirmaItem;
import it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Job che si occupa della verifica della firma in maniera asincrona.
 */
public class VerificaFirmaJob implements Job {

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(VerificaFirmaJob.class);
	
	/**
	 * Label - Documento.
	 */
	private static final String DOCUMENTO = "Documento ";

	/**
	 * Caller.
	 */
	private final String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();

	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext).
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.info("-------- START "+ callID + " --------");
		
		try {
			final IAooSRV aooSRV = ApplicationContextProvider.getApplicationContext().getBean(IAooSRV.class);
			final IUtenteSRV utenteSRV = ApplicationContextProvider.getApplicationContext().getBean(IUtenteSRV.class);
			final ISignSRV signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignSRV.class);
			final IDocumentManagerFacadeSRV documentManagerSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentManagerFacadeSRV.class);

			final PropertiesProvider pp = PropertiesProvider.getIstance();

			final JobDataMap data = context.getJobDetail().getJobDataMap();
			final JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
			final int idAoo = jobDB.getIdAoo();

			logger.info(callID + " verifica firma dei documenti per l'aoo " + idAoo);

			//recupera l'AOO
			final Aoo aoo = aooSRV.recuperaAoo(idAoo);

			final String usernameKey = aoo.getCodiceAoo() + "." + PropertiesNameEnum.AOO_SISTEMA_AMMINISTRATORE.getKey();
			final String username = pp.getParameterByString(usernameKey);
			final UtenteDTO utente = utenteSRV.getByUsername(username);

			logger.info(callID + " " + usernameKey + " => " + username);

			//inizializza accesso al content engine in modalita administrator
			final AooFilenet aooFilenet = aoo.getAooFilenet();

			//estrai documentTitle e classi documentali su DWH_RED 
			final List<VerificaFirmaItem> docsDaVerificare = signSRV.recuperaContentDaVerificareNew(aoo);
			if (docsDaVerificare != null && !docsDaVerificare.isEmpty()) {

				for (final VerificaFirmaItem docDaVerificare: docsDaVerificare) {
					aggiornaVerificaFirma(signSRV, aoo, username, utente, aooFilenet, docDaVerificare, documentManagerSRV);
				} 

			}
		} catch (final Exception e) {
			logger.error(callID + " errore generico", e);
		} 

		logger.info("-------- END "+ callID + " --------");
	}

	private void aggiornaVerificaFirma(final ISignSRV signSRV, final Aoo aoo, final String username, final UtenteDTO utente, final AooFilenet aooFilenet,
			final VerificaFirmaItem docDaVerificare, final IDocumentManagerFacadeSRV documentManagerSRV) {

		IFilenetCEHelper fceh = null;
		final PropertiesProvider pp = PropertiesProvider.getIstance();
		try {
			fceh = FilenetCEHelperProxy.newInstance(username, aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(), aooFilenet.getObjectStore(),
					aoo.getIdAoo());
			logger.info(callID + DOCUMENTO + docDaVerificare.getDocumentTitle() + " da verificare");
			final String searchCondition = " " + pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY) + "='" + docDaVerificare.getDocumentTitle() + "'";

			// verifica firma e aggiorna il metadato verificaFirma su CE
			final boolean verifica = docDaVerificare.getClasseDocumentale().contains("_DocumentoGenerico")
					|| docDaVerificare.getClasseDocumentale().equals(pp.getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY));

			if(verifica) { 
				DocumentSet documenti = fceh.getDocumentsForVerificaFirma(aoo.getIdAoo().intValue(), searchCondition, docDaVerificare.getClasseDocumentale(), docDaVerificare.getVersioneDocumento());
				if (documenti != null && !documenti.isEmpty()) {
					Iterator<?> docIt = documenti.iterator();
					while(docIt.hasNext()) {			
						Document doc = (Document) docIt.next();
						boolean isFirmato=false;
						if(docDaVerificare.getClasseDocumentale().equals(pp.getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY)) ||
								docDaVerificare.getClasseDocumentale().contains("_DocumentoGenerico")) {
							String mymeType = doc.getProperties().getStringValue((pp.getParameterByKey(PropertiesNameEnum.MIMETYPE_FN_METAKEY)));
							if(MediaType.PDF.toString().equalsIgnoreCase(mymeType) || documentManagerSRV.isVerificaFirmaDocPrincipaleVisible(mymeType)) {
								isFirmato = true;
							}else {
								Map<String, Object> mapToUpdate = new HashMap<>();
								mapToUpdate.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA), ValueMetadatoVerificaFirmaEnum.FIRMA_NON_PRESENTE.getValue());
								fceh.updateMetadati(doc, mapToUpdate);
							}
						}
						if(isFirmato) {
							InputStream docContent = getDocumentContent(doc); 
							signSRV.aggiornaVerificaFirma(doc, docContent, fceh, utente);

							Document docPrincipale = recuperaDocPrincipale(fceh, docDaVerificare, doc);
							signSRV.aggiornaVerificaFirmaDocAllegato(fceh, docPrincipale);

							List<Integer> docElaboratoId = new ArrayList<>();
							docElaboratoId.add(docDaVerificare.getNextVal());
							signSRV.updateStatoDocumentTitleVerificato(docElaboratoId,StatoVerificaFirmaEnum.ELABORATO.getStatoVerificaFirma());
						}else {
							List<Integer> docElaboratoId = new ArrayList<>();
							docElaboratoId.add(docDaVerificare.getNextVal());
							signSRV.updateStatoDocumentTitleVerificato(docElaboratoId,StatoVerificaFirmaEnum.SCARTATO.getStatoVerificaFirma());
						}
					}
				}
			}else {
				List<Integer> docElaboratoId = new ArrayList<>();
				docElaboratoId.add(docDaVerificare.getNextVal());
				signSRV.updateStatoDocumentTitleVerificato(docElaboratoId,StatoVerificaFirmaEnum.SCARTATO.getStatoVerificaFirma());
			}

		} catch(Exception ex) {
			logger.error(callID + " errore durante la verifica firma del documento", ex);
			List<Integer> docElaboratoId = new ArrayList<>();
			docElaboratoId.add(docDaVerificare.getNextVal());
			signSRV.updateStatoDocumentTitleVerificato(docElaboratoId,StatoVerificaFirmaEnum.IN_ERRORE.getStatoVerificaFirma());
		} finally {
			if (fceh != null) {
				fceh.popSubject();
			}
		}
	}

	/**
	 * @param doc
	 * @return InputStream del document
	 */
	private InputStream getDocumentContent(Document doc) {
		InputStream docContent = null; 
		try {
			ContentElementList ceList = doc.get_ContentElements();
			ContentTransfer ct = (ContentTransfer) ceList.get(0);
			docContent = ct.accessContentStream();
		}	catch (Exception e) {
			logger.warn(DOCUMENTO + doc.get_Id().toString() + " senza content", e); 
			throw new RedException(DOCUMENTO + doc.get_Id().toString() + " senza content" ,e); 
		}
		return docContent;
	}
	
	/**
	 * @param fceh
	 * @param docDaVerificare
	 * @param doc
	 * @return documento principale
	 */
	private Document recuperaDocPrincipale(final IFilenetCEHelper fceh, final VerificaFirmaItem docDaVerificare, final Document doc) {
		Document docPrincipale = doc;

		if (docDaVerificare.getClasseDocumentale().equals(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY))) {
			try {
				docPrincipale = fceh.getDocumentForAllegato(doc.get_Id().toString(), false);
			} catch (final Exception ex) {
				logger.error(callID + " errore generico", ex);
				docPrincipale = null;
			}
		}
		return docPrincipale;
	}
}