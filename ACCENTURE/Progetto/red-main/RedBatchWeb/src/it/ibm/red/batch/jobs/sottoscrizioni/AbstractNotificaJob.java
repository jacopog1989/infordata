package it.ibm.red.batch.jobs.sottoscrizioni;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.facade.INotificaWriteFacadeSRV;

/**
 * Abstract del job notifiche.
 */
public abstract class AbstractNotificaJob implements Job {

	/**
	 * Id del chiamante.
	 */
	protected String callID;

	/**
	 * Logger.
	 */
	protected REDLogger logger;

	/**
	 * Servizio notifica.
	 */
	protected INotificaWriteFacadeSRV notificaSRV;
	
	protected AbstractNotificaJob() {
		setCallID();
		setLogger();
		setNotificaSRV();

		logger.info(callID + " Job di notifica inizializzato correttamente " + this.toString());
	}

	/**
	 * Il metodo setta il nome del singolo processo di notifica che verrà
	 * visualizzato sui log applicativi.
	 * 
	 */
	protected abstract void setCallID();

	/**
	 * Il metodo inizializza il log del singolo processo di notifica.
	 * 
	 */
	protected abstract void setLogger();

	/**
	 * Il metodo recupera il servizio di notifica dal contesto di Spring, relativo
	 * al singolo processo di notifica.
	 * 
	 */
	protected abstract void setNotificaSRV();


	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext).
	 */
	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {

		logger.info("-------- START " + callID + " --------");

		try {

			final JobDataMap data = context.getJobDetail().getJobDataMap();
			final JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
			final int idAoo = jobDB.getIdAoo();

			logger.info(callID + " invio notifiche sottoscrizioni per l'aoo " + idAoo);

			notificaSRV.writeNotifiche(idAoo);

		} catch (final Exception e) {
			logger.error(callID + " errore generico", e);
		}

		logger.info("-------- END " + callID + " --------");

	}

}