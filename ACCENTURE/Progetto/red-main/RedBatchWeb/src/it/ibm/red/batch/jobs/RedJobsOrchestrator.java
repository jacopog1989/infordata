package it.ibm.red.batch.jobs;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;

import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IJobConfigurationFacadeSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Job orchestrator che gestisce tutti gli altri job.
 */
public class RedJobsOrchestrator implements Job {

	/**
	 * ID Job Label.
	 */
	private static final String JOBID_LABEL = "[JOBID] : ";

	/**
	 * Classname Label.
	 */
	private static final String CLASSNAME_LABEL = " - [CLASSNAME] : ";
	
	/**
	 * jobsGroup Label.
	 */
	private static final String JOBSGROUP_LABEL ="jobsGroup";

	/**
	 * Caller.
	 */
	private final String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(RedJobsOrchestrator.class);

	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext).
	 */
	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {

		logger.info("-------- START " + callID + " --------");

		try {

			final IJobConfigurationFacadeSRV jobConfigurationSRV = ApplicationContextProvider.getApplicationContext().getBean(IJobConfigurationFacadeSRV.class);
			final List<JobConfigurationDTO> listJobConfiguration = jobConfigurationSRV.getAll();

			if (listJobConfiguration != null) {
				for (final JobConfigurationDTO jobDB : listJobConfiguration) {

					final Scheduler scheduler = new StdSchedulerFactory().getScheduler();
					final String jobIdentityName = createJobIdentityName(jobDB);
					boolean jobDBSchedulato = false;
					for (final JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(JOBSGROUP_LABEL))) {
						final String jobName = jobKey.getName();
						if (jobName.equalsIgnoreCase(jobIdentityName)) {
							jobDBSchedulato = true;
							// se il jobState è attivo (uguale a 1) e il serverName è null o uguale al
							// serverName su cui sto girando,
							// allora effettuo i controlli, altrimenti elimino il job dallo scheduler
							if (jobDB.isJobState() && (jobDB.getServerName() == null || jobDB.getServerName().equalsIgnoreCase(ServerUtils.getServerFullName()))) {
								final Iterator<Entry<String, Object>> iterEntryJob = scheduler.getJobDetail(jobKey).getJobDataMap().entrySet().iterator();
								// viene inserito solo un elemento ed è 'jobDB' e quindi è presente solo una
								// jobConfiguration
								if (iterEntryJob.hasNext()) {
									final Entry<String, Object> jobEntryMap = iterEntryJob.next();
									final JobConfigurationDTO jobConfigurationValue = (JobConfigurationDTO) jobEntryMap.getValue();

									// ricrea il job se il numero di trigger o la classe da richiamare o lo
									// scheduling differiscono da quelli salvati in tabella
									if (jobConfigurationValue.getTriggerNumber() != jobDB.getTriggerNumber()
											|| !(jobDB.getClassName().equalsIgnoreCase(jobConfigurationValue.getClassName()))
											|| (!jobConfigurationValue.getChronSchedule().equalsIgnoreCase(jobDB.getChronSchedule()))) {

										// elimina job da scheduler
										scheduler.deleteJob(jobKey);

										// avvia
										final Map<JobDetail, Set<? extends Trigger>> jobMap = createMapJob(jobIdentityName, jobDB);
										scheduler.scheduleJobs(jobMap, true);

										logger.info(JOBID_LABEL + jobDB.getIdJobConfiguration() + CLASSNAME_LABEL + jobDB.getClassName() + ": SCHEDULAZIONE AGGIORNATA");
									} else {
										logger.info(JOBID_LABEL + jobDB.getIdJobConfiguration() + CLASSNAME_LABEL + jobDB.getClassName() + ": SCHEDULAZIONE CONFERMATA");
									}

								}
							} else {
								// elimina job da scheduler
								scheduler.deleteJob(jobKey);
								logger.info(JOBID_LABEL + jobDB.getIdJobConfiguration() + CLASSNAME_LABEL + jobDB.getClassName() + ": SCHEDULAZIONE ELIMINATA");
							}
							break;
						}
					}

					// Se il job non è schedulato, se il jobState è attivo (uguale a 1) e il
					// serverName è null o uguale al serverName su cui sto girando,
					// allora effettuo i controlli, altrimenti elimino il job dallo scheduler
					if ((!jobDBSchedulato && jobDB.isJobState())
							&& (jobDB.getServerName() == null || jobDB.getServerName().equalsIgnoreCase(ServerUtils.getServerFullName()))) {
						final Map<JobDetail, Set<? extends Trigger>> jobMap = createMapJob(jobIdentityName, jobDB);
						if (jobMap != null) {
							scheduler.scheduleJobs(jobMap, true);
							logger.info(JOBID_LABEL + jobDB.getIdJobConfiguration() + CLASSNAME_LABEL + jobDB.getClassName() + ": SCHEDULAZIONE INSERITA");
						}
					}

				}
			}

		} catch (final SchedulerException e) {
			logger.error(callID + " errore in fase di schedulazione dei job", e);
		} catch (final Exception e) {
			logger.error(callID + " errore generico", e);
		}

		logger.info("-------- END " + callID + " --------");

	}

	// identificativo del job
	private static String createJobIdentityName(final JobConfigurationDTO jobDB) {
		return String.valueOf(Integer.valueOf(jobDB.getIdJobConfiguration()));
	}

	// Crea un nuovo job
	@SuppressWarnings("unchecked")
	private Map<JobDetail, Set<? extends Trigger>> createMapJob(final String jobIdentityName, final JobConfigurationDTO jobDB) {
		// avvia
		Class<? extends Job> className = null;
		try {
			className = (Class<? extends Job>) Class.forName(jobDB.getClassName());
		} catch (final ClassNotFoundException e) {
			logger.warn("Il job " + jobDB + " non è implementato ", e);
			return null;
		}

		final JobDetail job = JobBuilder.newJob(className).withIdentity(jobIdentityName, JOBSGROUP_LABEL).build();
		final Map<JobDetail, Set<? extends Trigger>> jobMap = new HashMap<>();
		final Set<Trigger> listTrigger = new LinkedHashSet<>();
		for (int i = 0; i < jobDB.getTriggerNumber(); i++) {
			final Trigger trigger = TriggerBuilder.newTrigger().withIdentity(jobIdentityName + className.getName() + "Trig" + i, JOBSGROUP_LABEL)
					.withSchedule(CronScheduleBuilder.cronSchedule(jobDB.getChronSchedule())).build();
			listTrigger.add(trigger);
		}
		job.getJobDataMap().put("jobDB", jobDB);
		jobMap.put(job, listTrigger);
		return jobMap;
	}

}
