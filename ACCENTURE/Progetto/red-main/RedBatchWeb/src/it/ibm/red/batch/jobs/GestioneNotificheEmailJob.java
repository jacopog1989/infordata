package it.ibm.red.batch.jobs;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.ICodaMailSRV;
import it.ibm.red.business.service.facade.ICodaMailFacadeSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Job che gestisce le notifiche Mail.
 */
public class GestioneNotificheEmailJob extends AbstractMailJob implements Job {

	/**
	 * Caller.
	 */
	private final String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(GestioneNotificheEmailJob.class);

	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext).
	 */
	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {

		logger.info("-------- START " + callID + " --------");

		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;

		try {

			final ICodaMailFacadeSRV codaMailSRV = ApplicationContextProvider.getApplicationContext().getBean(ICodaMailSRV.class);
			final Aoo aoo = getAooFromContext(context);

			logger.info(callID + " gestione notifiche per l'aoo " + aoo.getIdAoo());
			
			fceh = getFilenetCEHelperAsAdmin(aoo);
			fpeh = getFilenetPEHelperAsAdmin(aoo);

			// riconcilia notifiche per le email in attesa
			codaMailSRV.checkAndUpdateNotificaMailStatus(fceh, aoo);

			logger.info(callID + " notifiche riconciliate per l'aoo " + aoo.getIdAoo());

			// elimino i documenti in stato chiuso
			codaMailSRV.chiudiDocumentiRiconciliati(fpeh, fceh, aoo);

			logger.info(callID + " chiusi documenti riconciliati per l'aoo " + aoo.getIdAoo());

			// gestione dei misti quando viene lanciato prima il batch e poi si clicca sulla
			// response spedito
			codaMailSRV.chiudiDocumentiDestinatariMisti(fceh, fpeh, aoo);

			logger.info(callID + " chiusi documenti con destinatari cartacei per l'aoo " + aoo.getIdAoo());

		} catch (final Exception e) {
			logger.error(callID + " errore generico", e);
		} finally {
			if (fceh != null) {
				fceh.popSubject();
			}
			if (fpeh != null) {
				fpeh.logoff();
			}
		}

		logger.info("-------- END " + callID + " --------");

	}

}
