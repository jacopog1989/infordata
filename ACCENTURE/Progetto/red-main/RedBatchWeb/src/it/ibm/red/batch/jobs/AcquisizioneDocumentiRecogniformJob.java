package it.ibm.red.batch.jobs;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IRecogniformSRV;
import it.ibm.red.business.service.facade.IRecogniformFacadeSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Job che gestisce l'acquisizione di documenti recogniform.
 */
public class AcquisizioneDocumentiRecogniformJob implements Job {

	/**
	 * Caller id.
	 */
	private final String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(AcquisizioneDocumentiRecogniformJob.class);

	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext).
	 */
	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {
		logger.info("-------- START " + callID + " --------");
		
		try {
			final IRecogniformFacadeSRV recogniformSRV = ApplicationContextProvider.getApplicationContext().getBean(IRecogniformSRV.class);
			
			final JobDataMap data = context.getJobDetail().getJobDataMap();
			final JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
			final int idAoo = jobDB.getIdAoo();
			
			logger.info(callID + " acquisizione documenti in coda per l'aoo " + idAoo);
			
			recogniformSRV.acquisisciDocumentiInCoda(idAoo);
			
		} catch (final Exception e) {
			logger.error(callID + " errore generico", e);
		} 
		
		logger.info("-------- END " + callID + " --------");
	}
}