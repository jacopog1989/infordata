package it.ibm.red.batch.jobs;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.constants.Constants.TipologiaInvio;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.PostaEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.ICodaMailSRV;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.facade.ICodaMailFacadeSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Job che gestisce l'invio delle mail.
 */
public class InvioEmailJob extends AbstractMailJob implements Job {

	/**
	 * Caller.
	 */
	private final String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(InvioEmailJob.class);

	/**
	 * Numero item.
	 */
	private static final Integer NUM_ITEM_TO_SEND = 1;

	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext).
	 */
	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {

		logger.info("-------- START " + callID + " --------");

		try {

			final ICodaMailFacadeSRV codaMailSRV = ApplicationContextProvider.getApplicationContext().getBean(ICodaMailSRV.class);
			final INpsSRV npsSRV = ApplicationContextProvider.getApplicationContext().getBean(INpsSRV.class);
			
			final Aoo aoo = getAooFromContext(context);

			logger.info(callID + " recupero item da inviare per l'aoo " + aoo.getIdAoo());
			
			// Indica se la posta dell'AOO è gestita dall'applicativo (Posta Interna) o no
			// (Posta Esterna)
			final boolean isPostaInterna = (PostaEnum.POSTA_INTERNA.getValue().intValue() == aoo.getPostaInteropEsterna());

			// Recupera NUM_ITEM_TO_SEND mail da inviare
			final List<MessaggioEmailDTO> messaggi = codaMailSRV.recuperaMailDaInviare(aoo.getIdAoo().intValue(), isPostaInterna, NUM_ITEM_TO_SEND);

			if (!CollectionUtils.isEmpty(messaggi)) {

				final Iterator<MessaggioEmailDTO> itMessaggi = messaggi.iterator();
				while (itMessaggi.hasNext()) {
					final MessaggioEmailDTO msgNext = itMessaggi.next();

					logger.info(callID + " email " + msgNext);

					// Se l'AOO
					// - non è attestata su NPS, oppure
					// - la posta è gestita dall'applicativo, oppure
					// - stiamo parlando di un messaggio di risposta (rifiuto/notifica
					// protocollazione)/inoltro, oppure
					// - l'azione è Inoltro da coda attiva
					
					final boolean inoltroFromCodaAttiva = msgNext.getTipoEvento() != null && msgNext.getTipoEvento().intValue() == EventTypeEnum.INOLTRO_MAIL_DA_CODA_ATTIVA.getIntValue();
					if (!TipologiaProtocollazioneEnum.isProtocolloNPS(aoo.getTipoProtocollo().getIdTipoProtocollo()) 
							|| isPostaInterna || msgNext.getTipologiaInvioId() != TipologiaInvio.NUOVOMESSAGGIO 
							|| inoltroFromCodaAttiva) {

						// Se è != da Posta interna significa che è o automatica o semiautomatica
						if (!isPostaInterna) {
							codaMailSRV.inviaMessaggioPostaNps(msgNext, aoo);
						} else {
							// prendi in carico l'invio della posta ai destinatari
							 codaMailSRV.invioMailDaCoda(msgNext, aoo); 
							 //Cambia stato prot NPS 
							if (TipologiaProtocollazioneEnum.isProtocolloNPS(aoo.getTipoProtocollo().getIdTipoProtocollo())
									&& (msgNext.getTipologiaInvioId() != null && msgNext.getTipologiaInvioId() == TipologiaInvio.NUOVOMESSAGGIO) 
									&& !inoltroFromCodaAttiva) {
								
								npsSRV.cambiaStatoProtUscitaToSpedito(aoo,msgNext);	 
							 }
							 
						}

					} else {

						// altrimenti, spedisci il protocollo tramite NPS
						codaMailSRV.spedisciProtocolloNPSDaCoda(msgNext, aoo.getIdAoo().intValue());

					}

				}

			}

		} catch (final Exception e) {
			logger.error(callID + " errore generico", e);
		}

		logger.info("-------- END " + callID + " --------");

	}

}
