package it.ibm.red.batch.jobs.sottoscrizioni;
 

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.INotificaMancataSpedizioneCartMistaSRV;
import it.ibm.red.business.service.facade.IAooFacadeSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * @author VINGENITO 
 * Job che procede ad inviare la notifica di mancata spedizione per i documenti cartacei o misti non spediti.
 */
public class NotificaMancataSpedizioneCartaceaJob implements Job {
	
	/**
	 * Calle ID.
	 */
	private String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();
	
	/**
	 * LOGGER.
	 */
	private REDLogger logger = REDLogger.getLogger(NotificaMancataSpedizioneCartaceaJob.class);
	
	/**
	 * Execute del job.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
 		logger.info("-------- START "+ callID + " --------");
		
		try {
			JobDataMap data = context.getJobDetail().getJobDataMap();
			JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
			long idAoo = jobDB.getIdAoo();
			 
			IAooFacadeSRV aooSRV = ApplicationContextProvider.getApplicationContext().getBean(IAooFacadeSRV.class);
			INotificaMancataSpedizioneCartMistaSRV notificaMancaSpedSRV = ApplicationContextProvider.getApplicationContext().getBean(INotificaMancataSpedizioneCartMistaSRV.class);
			 
			//recupera l'AOO
			Aoo aoo = aooSRV.recuperaAoo((int)idAoo); 
			notificaMancaSpedSRV.getNotificaMancataSpedCartMista(aoo); 
		} catch (Exception e) {
			logger.error(callID + " errore generico", e); 
		}

		logger.info("-------- END "+ callID + " --------");
	}
}
