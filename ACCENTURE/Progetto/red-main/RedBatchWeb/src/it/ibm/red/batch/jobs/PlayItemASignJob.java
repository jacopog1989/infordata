package it.ibm.red.batch.jobs;

import java.util.List;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IASignOrchestrator;
import it.ibm.red.business.service.IASignSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * @author SimoneLungarella
 * Job che si occupa dell'esecuzione della firma asincrona nel normale funzionamento.
 */
public class PlayItemASignJob implements Job {

	/**
	 * ID.
	 */
	private String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();
	
	/**
	 * Logger per la comunicazione delle informazioni su console.
	 */
	private REDLogger logger = REDLogger.getLogger(PlayItemASignJob.class);
	
	/**
	 * Logica di esecuzione PLAY degli item di firma asincrona.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.info("-------- START "+ callID + " --------");

		try {
			JobDataMap data = context.getJobDetail().getJobDataMap();
			JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
			int idAoo = jobDB.getIdAoo();
			
			IASignOrchestrator orchestrator = ApplicationContextProvider.getApplicationContext().getBean(IASignOrchestrator.class);
			IASignSRV aSignSRV = ApplicationContextProvider.getApplicationContext().getBean(IASignSRV.class);			
			
			// Esecuzione di play su tutti gli item playable
			List<Long> itemsToPlay = aSignSRV.getPlayableItems(Long.valueOf(idAoo));
			for (Long item : itemsToPlay) {
				orchestrator.play(item);
			}
			
		}catch (Exception e) {
			logger.error(callID + " errore generico", e);
		}

		logger.info("-------- END "+ callID + " --------");
	}
}
