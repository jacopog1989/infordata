package it.ibm.red.batch.jobs.protocollazione.pec;

import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IProtocollaPECBESRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Job Protocollazione PEC.
 */
public class BEProtocollazionePECJob extends AbstractProtocollazionePECJob {

	/**
	 * Call ID.
	 */
	@Override
	protected void setCallID() {
		callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();
	}

	/**
	 * LOGGER.
	 */
	@Override
	protected void setLogger() {
		logger = REDLogger.getLogger(BEProtocollazionePECJob.class);
	}

	/**
	 * Imposta la property key radix.
	 */
	@Override
	protected void setPropertyKeyRadix() {
		propertyKeyRadix = "protocollazione.automatica.pec.be.";
	}

	/**
	 * Inizializza il protocolloSRV.
	 */
	@Override
	protected void setProtocollaSRV() {
		protocollaSRV = ApplicationContextProvider.getApplicationContext().getBean(IProtocollaPECBESRV.class);
	}
	
}
