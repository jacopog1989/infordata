package it.ibm.red.batch.jobs;

import java.util.Map;
import java.util.Map.Entry;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.dto.SicogeResultDTO;
import it.ibm.red.business.dto.SigiResultDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IIntegratorFacadeSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Job che integra Sigi e Sicoge.
 */
public class IntegratorSigiSicogeJob implements Job {

	/**
	 * Caller.
	 */
	private final String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(IntegratorSigiSicogeJob.class);

	/**
	 * Service.
	 */
	private IIntegratorFacadeSRV integratorSRV;

	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext).
	 */
	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {
		logger.info("-------- START " + callID + " --------");

		try {
			final StringBuilder logResult = new StringBuilder();
			integratorSRV = ApplicationContextProvider.getApplicationContext().getBean(IIntegratorFacadeSRV.class);

			final JobDataMap data = context.getJobDetail().getJobDataMap();
			final JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
			final int idAoo = jobDB.getIdAoo();

			logger.info(callID + " AOO " + idAoo);

			// ### SIGI
			gestisciSigi(idAoo, logResult);

			logResult.setLength(0);

			// ### SICOGE
			gestisciSicoge(idAoo, logResult);

		} catch (final Exception e) {
			logger.error(callID + " errore generico", e);
		}

		logger.info("-------- END " + callID + " --------");
	}

	private void gestisciSigi(final int idAoo, final StringBuilder logResult) {
		try {
			final SigiResultDTO sigiResult = integratorSRV.syncSIGI(idAoo);

			for (final Entry<String, Map<Integer, Boolean>> sigiResultEntry : sigiResult.getResult().entrySet()) {
				for (final Entry<Integer, Boolean> numDecretoResult : sigiResultEntry.getValue().entrySet()) {
					logResult.append("Esito OP [Numero: ").append(numDecretoResult.getKey()).append("] => ").append(numDecretoResult.getValue()).append(" || ");
				}

				logger.info("[AOO: " + idAoo + "] Esiti lavorazione per il decreto [Numero: " + sigiResultEntry.getKey() + "] ===> " + logResult.toString());
			}
		} catch (final Exception e) {
			logger.error(callID + ", errore nella chiamata a SIGI: " + e.getMessage(), e);
		}
	}

	private void gestisciSicoge(final int idAoo, final StringBuilder logResult) {
		try {
			final SicogeResultDTO sicogeResult = integratorSRV.syncSICOGE(idAoo);

			for (final Entry<String, Map<String, Boolean>> sicogeResultEntry : sicogeResult.getResult().entrySet()) {
				for (final Entry<String, Boolean> idDecretoResult : sicogeResultEntry.getValue().entrySet()) {
					logResult.append("Esito OP [ID: ").append(idDecretoResult.getKey()).append("] => ").append(idDecretoResult.getValue()).append(" || ");
				}

				logger.info("[AOO: " + idAoo + "] Esiti lavorazione per il decreto [ID: " + sicogeResultEntry.getKey() + "] ===> " + logResult.toString());
			}
		} catch (final Exception e) {
			logger.error(callID + ", errore nella chiamata a SICOGE: " + e.getMessage(), e);
		}
	}

}