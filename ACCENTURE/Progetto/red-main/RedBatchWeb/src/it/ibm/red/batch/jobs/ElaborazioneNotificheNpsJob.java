package it.ibm.red.batch.jobs;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.dto.NotificaAzioneNpsDTO;
import it.ibm.red.business.dto.NotificaNpsDTO;
import it.ibm.red.business.dto.NotificaOperazioneNpsDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.INotificaNpsFacadeSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Job che elabore le notifiche Nps.
 */
public class ElaborazioneNotificheNpsJob implements Job {

	/**
	 * Caller.
	 */
	private final String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(ElaborazioneNotificheNpsJob.class);

	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext).
	 */
	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {
		logger.info("-------- START " + callID + " --------");

		try {

			final JobDataMap data = context.getJobDetail().getJobDataMap();
			final JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
			final long idAoo = jobDB.getIdAoo();

			logger.info(callID + " aoo " + idAoo);

			final INotificaNpsFacadeSRV notificheNpsSRV = ApplicationContextProvider.getApplicationContext().getBean(INotificaNpsFacadeSRV.class);

			final NotificaNpsDTO item = notificheNpsSRV.recuperaProssimaRichiestaElabDaLavorare(idAoo);

			if (item != null) {

				// Processa l'item
				if (item instanceof NotificaOperazioneNpsDTO) {

					// Viene gestita solamente la notifica dell'operazione di avvenuta spedizione di
					// protocollo, effettuata da NPS
					notificheNpsSRV.elaboraNotificaSpedizione((NotificaOperazioneNpsDTO) item);

				} else if (item instanceof NotificaAzioneNpsDTO) {

					// Viene gestita solamente la notifica dell'azione di risposta a un protocollo,
					// effettuata su un sistema esterno a RED e notificata da NPS
					notificheNpsSRV.elaboraNotificaAzione((NotificaAzioneNpsDTO) item);

				}

				logger.info("Elaborazione dell'item " + item.getIdCoda() + " eseguita con successo");

			} else {
				logger.info("Nessun item da elaborare");
			}

		} catch (final Exception e) {
			logger.error(callID + " errore generico", e);
		}

		logger.info("-------- END " + callID + " --------");
	}

}