package it.ibm.red.batch.jobs.protocollazione.pec;

import java.util.Arrays;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.dto.ProtocollazionePECConfigDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.facade.IProtocollaPECFacadeSRV;

/**
 * Abstract del job di protocollazione PEC.
 */
public abstract class AbstractProtocollazionePECJob implements Job {

	/**
	 * Identificativo chiamante.
	 */
	protected String callID;

	/**
	 * Logger.
	 */
	protected REDLogger logger;

	/**
	 * Suffisso chiave proprietà.
	 */
	protected String propertyKeyRadix;

	/**
	 * Servizio protocollazione.
	 */
	protected IProtocollaPECFacadeSRV protocollaSRV;

	/**
	 * Casella postale.
	 */
	private String casellaPostale;

	/**
	 * Id Nodo destinatario.
	 */
	private Integer idNodoDestinatario;

	/**
	 * Id ufficio protollatore.
	 */
	private Integer idUfficioProtocollatore;

	/**
	 * Id utente protocollatore.
	 */
	private Integer idUtenteProtocollatore;

	/**
	 * Id tipologia documento.
	 */
	private Integer idTipologiaDocumento;

	/**
	 * Id tipologia documento.
	 */
	private Integer idTipologiaProcedimento;

	/**
	 * Indice di classificazione.
	 */
	private String indiceClassificazione;

	/**
	 * Id faldone.
	 */
	private Integer idFaldone;

	/**
	 * Lista estensioni ammesse.
	 */
	private List<String> allowedFileExtensions;

	protected AbstractProtocollazionePECJob() {
		setCallID();
		setLogger();
		setPropertyKeyRadix();
		setProtocollaSRV();

		getConfiguration();

		logger.info(callID + " Job di protocollazione inizializzato correttamente " + this.toString());
	}

	/**
	 * Il metodo setta il nome del singolo flusso verticale che verrà visualizzato
	 * sui log applicativi.
	 * 
	 */
	protected abstract void setCallID();

	/**
	 * Il metodo inizializza il log del singolo flusso verticale.
	 * 
	 */
	protected abstract void setLogger();

	/**
	 * Il metodo setta la radice delle chiavi di properties da cui recuperare la
	 * configurazione del flusso verticale da protocollare.
	 * 
	 */
	protected abstract void setPropertyKeyRadix();

	/**
	 * Il metodo recupera il servizio di protocollazione dal contesto di Spring,
	 * relativo al flusso verticale da protocollare.
	 * 
	 */
	protected abstract void setProtocollaSRV();

	/**
	 * Recupera le configurazioni per il job.
	 * 
	 */
	protected void getConfiguration() {
		PropertiesProvider pp = PropertiesProvider.getIstance();

		casellaPostale = pp.getParameterByString(propertyKeyRadix + "casellaPostale");
		idNodoDestinatario = Integer.parseInt(pp.getParameterByString(propertyKeyRadix + "idNodoDestinatario"));
		idUfficioProtocollatore = Integer.parseInt(pp.getParameterByString(propertyKeyRadix + "idUfficioProtocollatore"));
		idUtenteProtocollatore = Integer.parseInt(pp.getParameterByString(propertyKeyRadix + "idUtenteProtocollatore"));
		idTipologiaDocumento = Integer.parseInt(pp.getParameterByString(propertyKeyRadix + "idTipologiaDocumento"));
		idTipologiaProcedimento = Integer.parseInt(pp.getParameterByString(propertyKeyRadix + "idTipologiaProcedimento"));

		indiceClassificazione = null;
		if (pp.getParameterByString(propertyKeyRadix + "indiceClassificazione") != null) {
			indiceClassificazione = pp.getParameterByString(propertyKeyRadix + "indiceClassificazione");
		}

		idFaldone = null;
		if (pp.getParameterByString(propertyKeyRadix + "idFaldone") != null) {
			idFaldone = Integer.parseInt(pp.getParameterByString(propertyKeyRadix + "idFaldone"));
		}

		allowedFileExtensions = null;
		if (pp.getParameterByString(propertyKeyRadix + "allowedFileExtensions") != null) {
			allowedFileExtensions = Arrays.asList(pp.getParameterByString(propertyKeyRadix + "allowedFileExtensions").split(","));
		}
	}

	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext).
	 */
	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {

		logger.info("-------- START " + callID + " --------");

		try {

			final JobDataMap data = context.getJobDetail().getJobDataMap();
			final JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
			final int idAoo = jobDB.getIdAoo();

			final ProtocollazionePECConfigDTO config = new ProtocollazionePECConfigDTO(idAoo, casellaPostale, idNodoDestinatario, idUfficioProtocollatore,
					idUtenteProtocollatore, idTipologiaDocumento, idTipologiaProcedimento, indiceClassificazione, idFaldone, allowedFileExtensions);

			logger.info(callID + " protocollazione mail " + config);

			protocollaSRV.protocollaPEC(config);

			logger.info(callID + " protocollazione eseguita");

		} catch (final Exception e) {
			logger.error(callID + " errore generico", e);
		}

		logger.info("-------- END " + callID + " --------");

	}
}