package it.ibm.red.batch.jobs;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IMailSRV;
import it.ibm.red.business.service.facade.IMailFacadeSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Job che si occupa dello spostamento mail obsolete.
 */
public class SpostaMailObsoleteJob extends AbstractMailJob implements Job {
	
	/**
	 * Caller.
	 */
	private final String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(SpostaMailObsoleteJob.class);

	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext).
	 */
	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {
		
		logger.info("-------- START " + callID + " --------");
		
		IFilenetCEHelper fceh = null;
		
		try { 
			final IMailFacadeSRV mailSRV = ApplicationContextProvider.getApplicationContext().getBean(IMailSRV.class);
			final Aoo aoo = getAooFromContext(context);
			
			logger.info(callID + " sposta mail per l'aoo " + aoo.getIdAoo());
			
			fceh = getFilenetCEHelperAsAdmin(aoo);
			
			//sposta email
			mailSRV.spostaMail(fceh);
			
		} catch (final Exception e) {
			logger.error(callID + " errore generico", e);
		} finally {
			if (fceh != null) {
				fceh.popSubject();
			}
		}
		
		logger.info("-------- END " + callID + " --------");
		
	}

}
