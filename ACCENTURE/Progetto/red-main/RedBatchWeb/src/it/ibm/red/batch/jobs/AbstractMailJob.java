package it.ibm.red.batch.jobs;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.facade.IAooFacadeSRV;

/**
 * Abstract dei job che eseguono lavorazioni sulle mail.
 * 
 * @author SimoneLungarella
 */
public abstract class AbstractMailJob {

	/**
	 * Restituisce le informazioni sull'area organizzativa recuperata dai dettagli
	 * del context del job.
	 * 
	 * @param context
	 *            Context del job dal quale recupero l'identificativo dell'area
	 *            omogenea da recuperare.
	 * @return Tutte le informazioni sull'area organizzativa.
	 */
	protected Aoo getAooFromContext(final JobExecutionContext context) {
		final IAooFacadeSRV aooSRV = ApplicationContextProvider.getApplicationContext().getBean(IAooSRV.class);

		final JobDataMap data = context.getJobDetail().getJobDataMap();
		final JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
		final int idAoo = jobDB.getIdAoo();

		// recupera l'AOO
		return aooSRV.recuperaAoo(idAoo);
	}

	/**
	 * Restituisce l'helper per il CE di Filenet.
	 * 
	 * @param aoo
	 *            Aoo del context del job.
	 * @return Helper di Filenet per il CE.
	 */
	protected IFilenetCEHelper getFilenetCEHelperAsAdmin(final Aoo aoo) {
		// inizializza accesso al content engine in modalità administrator
		final AooFilenet aooFilenet = aoo.getAooFilenet();
	
		return FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
				aooFilenet.getObjectStore(), aoo.getIdAoo());
	}

	/**
	 * Restituisce l'helper per il PE di Filenet.
	 * 
	 * @param aoo
	 *            Aoo del context del job.
	 * @return Helper di Filenet per il PE.
	 */
	protected FilenetPEHelper getFilenetPEHelperAsAdmin(final Aoo aoo) {
		FilenetPEHelper fpeh;
		// inizializza accesso al content engine in modalità administrator
		final AooFilenet aooFilenet = aoo.getAooFilenet();
		fpeh = new FilenetPEHelper(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(), aooFilenet.getConnectionPoint());
		return fpeh;
	}
}
