package it.ibm.red.batch.jobs;

import java.util.List;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.dto.NotificaDTO;
import it.ibm.red.business.helper.email.NotificheEmailHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IASignSRV;
import it.ibm.red.business.utils.CollectionUtils;
import it.ibm.red.business.utils.ServerUtils;

/**
 * @author SimoneLungarella
 * Job che gestisce la comunicazione di eventuali item in errore durante il processo di firma asincrona.
 */
public class ComunicaErroriASignJob implements Job{

	/**
	 * Id.
	 */
	private String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();
	
	/**
	 * Logger.
	 */
	private REDLogger logger = REDLogger.getLogger(ComunicaErroriASignJob.class);
	
	/**
	 * Logica che gestisce la comunicazione.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.info("-------- START "+ callID + " --------");
		
		try {
			JobDataMap data = context.getJobDetail().getJobDataMap();
			JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
			int idAoo = jobDB.getIdAoo();
			
			IASignSRV aSignSRV = ApplicationContextProvider.getApplicationContext().getBean(IASignSRV.class);
			
			// Recupero degli item in stato di errore
			List<ASignItemDTO> itemsFailed = aSignSRV.getItemsDTFailed(Long.valueOf(idAoo));
			
			
			if(!CollectionUtils.isEmptyOrNull(itemsFailed)) {
				for (ASignItemDTO item : itemsFailed) {
					
					// Definizione mail
					String oggettoMail = "Comunicazione errore firma";
					String testoMail = "Si è verificato un errore durante il processo di firma asincrona per il documento: " + item.getDocumentTitle();
					
					List<String> destinatariList = aSignSRV.recuperaEmailDestinatari(item);
					
					// Invio mail ad ogni destinatario
					NotificheEmailHelper neh = new NotificheEmailHelper();
					NotificaDTO notifica = new NotificaDTO();
					notifica.setIdNotifica(0);
					String resultInvio = neh.inviaNotifica(notifica, destinatariList, oggettoMail, testoMail, null);
					
					// Se non si sono verificati errori nell'invio della mail, viene impostato il flag "Comunicato" sull'item a true
					if("".equals(resultInvio)) { 
						aSignSRV.setIsNotified(item.getId(), true);
					} else {
						logger.info(resultInvio);
					}
				}
			}
		} catch (Exception e) {
			logger.error(callID + " errore generico", e);
		}

		logger.info("-------- END "+ callID + " --------");
	}
	
}
