package it.ibm.red.batch.jobs;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.facade.INpsFacadeSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * The Class NPSAsyncRequestJob.
 */
public class NPSAsyncRequestJob implements Job {


	/**
	 * Caller.
	 */
	private final String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();


	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(NPSAsyncRequestJob.class);

	/**
	 * Metodo execute del job.
	 *
	 * @param context the context
	 * @throws JobExecutionException the job execution exception
	 */
	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {
		
		logger.info("-------- START " + callID + " --------");
		
		try {
			final INpsFacadeSRV npsSRV = ApplicationContextProvider.getApplicationContext().getBean(INpsSRV.class);
			final IAooSRV aooSRV = ApplicationContextProvider.getApplicationContext().getBean(IAooSRV.class);
			
			final JobDataMap data = context.getJobDetail().getJobDataMap();
			final JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
			final int idAoo = jobDB.getIdAoo();
			
			logger.info(callID + " invia richieste asincrone NPS per l'aoo " + idAoo);
			
			// Si recupera l'AOO
			final Aoo aoo = aooSRV.recuperaAoo(idAoo);
			
			//se siamo in regime di emergenza, non procedere con il colloquio con NPS
			if (aoo.getTipoProtocollo().getIdTipoProtocollo().equals(TipologiaProtocollazioneEnum.EMERGENZANPS.getId())) {
				logger.warn("Non è possibile inviare item ad NPS in regime di emergenza");
				return;
			}
			//lavora la prossima richiesta asincrona
			npsSRV.workNextAsyncRequests(idAoo);
		} catch (final Exception e) {
			logger.error(callID + " errore generico", e);
		} 
		logger.info("-------- END " + callID + " --------");
	}
}