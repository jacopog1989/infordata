package it.ibm.red.batch.jobs;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.dto.AggiornaSecurityNpsDTO;
import it.ibm.red.business.dto.AllaccioDocUscitaDTO;
import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.SecurityDTO;
import it.ibm.red.business.enums.AllaccioDocUscitaEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IAllaccioDocUscitaSRV;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * @author VINGENITO
 * Job che gestisce l'aggiornamento delle ACL.
 */
public class AggiornamentoAclJob implements Job {

	/**
	 * Caller id.
	 */
	private final String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(AggiornamentoAclJob.class);

	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext).
	 */
	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {

		logger.info("-------- START " + callID + " --------");

		try {
			final IAllaccioDocUscitaSRV allacciaDocUscitaSRV = ApplicationContextProvider.getApplicationContext().getBean(IAllaccioDocUscitaSRV.class);
			final INpsSRV npsSRV = ApplicationContextProvider.getApplicationContext().getBean(INpsSRV.class);
			final IAooSRV aooSRV = ApplicationContextProvider.getApplicationContext().getBean(IAooSRV.class);
			final JobDataMap data = context.getJobDetail().getJobDataMap();
			final JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
			final int idAoo = jobDB.getIdAoo();

			final Aoo aoo = aooSRV.recuperaAoo(idAoo);

			// Recupero tutti i record relativi allo stesso doc in uscita
			final List<AllaccioDocUscitaDTO> docAllaccioList = allacciaDocUscitaSRV.getAllaccioDocUscitaByAoo(idAoo, AllaccioDocUscitaEnum.DA_LAVORARE.getStato());
			Integer docUscitaDaTracciare = null;
			if (!CollectionUtils.isEmpty(docAllaccioList)) {
				final List<SecurityDTO> securityDTOList = new ArrayList<>();

				docUscitaDaTracciare = Integer.parseInt(docAllaccioList.get(0).getDocTitleUscita());
				// Recupero Acl Doc In uscita
				final AggiornaSecurityNpsDTO aggiornaSecurityDTO = new AggiornaSecurityNpsDTO();
				aggiornaSecurityDTO.setAclDocUscita(
						allacciaDocUscitaSRV.getAclDocUscitaAndSetSecurity(docAllaccioList.get(0).getDocTitleUscita(), securityDTOList, aoo, aggiornaSecurityDTO));

				final ListSecurityDTO securityDocIngresso = new ListSecurityDTO();
				final List<Integer> idUtenteDaTracciare = new ArrayList<>();

				for (final AllaccioDocUscitaDTO docAllaccio : docAllaccioList) {
					// Recupero le security per i doc in ingresso e le aggiorno sul documento in
					// uscita
					allacciaDocUscitaSRV.aggiornaSecurityDocumento(aoo, securityDTOList, aggiornaSecurityDTO.getAclDocUscita(), securityDocIngresso, docAllaccio,
							idUtenteDaTracciare);
				}

				if (aggiornaSecurityDTO.getAggiornaAclNps()) {
					npsSRV.aggiornaAcl(aggiornaSecurityDTO.getUtente(), aggiornaSecurityDTO.getInfoProtocollo(), aggiornaSecurityDTO.getIdProtocollo(),
							aggiornaSecurityDTO.getAclDocUscita(), docAllaccioList.get(0).getDocTitleUscita());
				}

				for (final Integer idUtente : idUtenteDaTracciare) {
					allacciaDocUscitaSRV.tracciaProcedimentoAllaccioDoc(idAoo, idUtente, docUscitaDaTracciare);
				}
			}
		} catch (final Exception e) {
			logger.error(callID + " errore generico", e);
		}

		logger.info("-------- END " + callID + " --------");

	}

}
