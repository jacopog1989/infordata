package it.ibm.red.batch.jobs;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.enums.StatoMailEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IMailSRV;
import it.ibm.red.business.service.facade.IMailFacadeSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Job che gestisce l'archiviazione delle mail.
 */
public class ArchiviazioneEmailJob extends AbstractMailJob implements Job {

	/**
	 * Identificativo chiamante.
	 */
	private final String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(ArchiviazioneEmailJob.class);

	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext).
	 */
	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {

		logger.info("-------- START " + callID + " --------");

		IFilenetCEHelper fceh = null;

		try {

			final IMailFacadeSRV mailSRV = ApplicationContextProvider.getApplicationContext().getBean(IMailSRV.class);
			final Aoo aoo = getAooFromContext(context);

			logger.info(callID + " archiviazione mail per l'aoo " + aoo.getIdAoo());
			
			fceh = getFilenetCEHelperAsAdmin(aoo);

			// archiviazione delle email eliminate
			mailSRV.archiviaMail(fceh, StatoMailEnum.ELIMINATA);

			logger.info(callID + " archiviazione eseguita per le mail eliminate dell'aoo " + aoo.getIdAoo());

			// archiviazione delle email inoltrate
			mailSRV.archiviaMail(fceh, StatoMailEnum.INOLTRATA);

			logger.info(callID + " archiviazione eseguita per le mail inoltrate dell'aoo " + aoo.getIdAoo());

			// archiviazione delle email rifiutate
			mailSRV.archiviaMail(fceh, StatoMailEnum.RIFIUTATA);

			logger.info(callID + " archiviazione eseguita per le mail rifiutate dell'aoo " + aoo.getIdAoo());

		} catch (final Exception e) {
			logger.error(callID + " errore generico", e);
		} finally {
			if (fceh != null) {
				fceh.popSubject();
			}
		}

		logger.info("-------- END " + callID + " --------");

	}

}
