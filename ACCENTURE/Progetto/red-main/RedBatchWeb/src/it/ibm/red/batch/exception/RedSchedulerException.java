package it.ibm.red.batch.exception;


/**
 * Eccezione sollevata dallo scheduler Red
 * 
 * @author a.dilegge
 *
 */
@SuppressWarnings("serial")
public class RedSchedulerException extends Exception {

	/**
	 * Costruttore di default.
	 */
	public RedSchedulerException() {
		super();
	}

	/**
	 * Costruttore completo.
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public RedSchedulerException(final String message, final Throwable cause, final boolean enableSuppression,
			final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * Costruttore.
	 * @param message messaggio di errore
	 * @param cause causa dell'eccezione come Throwable
	 */
	public RedSchedulerException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 * Costruttore.
	 * @param message messaggio di errore
	 */
	public RedSchedulerException(final String message) {
		super(message);
	}

	/**
	 * Costruttore.
	 * @param cause causa dell'eccezione come Throwable
	 */
	public RedSchedulerException(final Throwable cause) {
		super(cause);
	}

}
