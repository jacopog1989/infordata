package it.ibm.red.batch.jobs;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.dto.ProtocollaMailAooConfDTO;
import it.ibm.red.business.dto.ProtocollaMailCodaDTO;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.IProtocollaMailSRV;
import it.ibm.red.business.service.facade.IAooFacadeSRV;
import it.ibm.red.business.service.facade.IProtocollaMailFacadeSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Job che gestisce l'inoltro rifiuto con protocollo.
 */
public class InoltroRifiutoConProtocolloJob implements Job {

	/**
	 * Caller.
	 */
	private final String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(InoltroRifiutoConProtocolloJob.class);

	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext).
	 */
	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {

		logger.info("-------- START " + callID + " --------");

		try {

			final JobDataMap data = context.getJobDetail().getJobDataMap();
			final JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
			final long idAoo = jobDB.getIdAoo();

			logger.info(callID + " aoo " + idAoo);

			final IAooFacadeSRV aooSRV = ApplicationContextProvider.getApplicationContext().getBean(IAooSRV.class);
			final Aoo aoo = aooSRV.recuperaAoo((int) idAoo);
			if (aoo.getTipoProtocollo().getIdTipoProtocollo().equals(TipologiaProtocollazioneEnum.EMERGENZANPS.getId())) {
				logger.warn("Non è possibile protocollare le mail in regime di emergenza");
				return;
			}

			final IProtocollaMailFacadeSRV protocollaMailSRV = ApplicationContextProvider.getApplicationContext().getBean(IProtocollaMailSRV.class);

			final ProtocollaMailCodaDTO item = protocollaMailSRV.getFirstItemToProcess(idAoo);

			if (item != null) {

				// recupera la configurazione per l'aoo relativa all'operazione associata
				// all'item da elaborare
				final ProtocollaMailAooConfDTO conf = protocollaMailSRV.getConfAoo(idAoo, item.getOperazione());

				// processa l'item
				protocollaMailSRV.processItem(item, conf);

				logger.info("Elaborazione dell'item " + item.getIdCoda() + " eseguita con successo");

			} else {
				logger.info("Nessun item da elaborare");
			}

		} catch (final Exception e) {
			logger.error(callID + " errore generico", e);
		}

		logger.info("-------- END " + callID + " --------");

	}

}