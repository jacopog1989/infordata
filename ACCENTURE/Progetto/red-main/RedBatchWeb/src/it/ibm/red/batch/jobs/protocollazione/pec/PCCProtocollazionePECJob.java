package it.ibm.red.batch.jobs.protocollazione.pec;

import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IProtocollaPECPCCSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Classe PCCProtocollazionePECJob.
 */
public class PCCProtocollazionePECJob extends AbstractProtocollazionePECJob {

	/**
	 * @see it.ibm.red.batch.jobs.protocollazione.pec.AbstractProtocollazionePECJob#setCallID().
	 */
	@Override
	protected void setCallID() {
		callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();
	}

	/**
	 * @see it.ibm.red.batch.jobs.protocollazione.pec.AbstractProtocollazionePECJob#setLogger().
	 */
	@Override
	protected void setLogger() {
		logger = REDLogger.getLogger(PCCProtocollazionePECJob.class);
	}

	/**
	 * @see it.ibm.red.batch.jobs.protocollazione.pec.AbstractProtocollazionePECJob#setPropertyKeyRadix().
	 */
	@Override
	protected void setPropertyKeyRadix() {
		propertyKeyRadix = "protocollazione.automatica.pec.pcc.";
	}

	/**
	 * @see it.ibm.red.batch.jobs.protocollazione.pec.AbstractProtocollazionePECJob#setProtocollaSRV().
	 */
	@Override
	protected void setProtocollaSRV() {
		protocollaSRV = ApplicationContextProvider.getApplicationContext().getBean(IProtocollaPECPCCSRV.class);
	}

}
