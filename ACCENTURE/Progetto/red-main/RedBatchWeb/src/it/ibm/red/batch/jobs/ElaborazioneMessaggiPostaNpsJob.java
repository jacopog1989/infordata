package it.ibm.red.batch.jobs;

import org.apache.commons.lang3.StringUtils;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.dto.MessaggioPostaNpsDTO;
import it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IPostaNpsFacadeSRV;
import it.ibm.red.business.utils.ServerUtils;

/**
 * Job che elabora i messaggi posta NPS.
 */
public class ElaborazioneMessaggiPostaNpsJob implements Job {

	/**
	 * Caller..
	 */
	private final String callID = " " + this.getClass().getSimpleName() + " ON SERVER: " + ServerUtils.getServerFullName();

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(ElaborazioneMessaggiPostaNpsJob.class);

	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext).
	 */
	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {
		logger.info("-------- START " + callID + " --------");

		try {

			final JobDataMap data = context.getJobDetail().getJobDataMap();
			final JobConfigurationDTO jobDB = (JobConfigurationDTO) data.get("jobDB");
			final long idAoo = jobDB.getIdAoo();

			logger.info(callID + " aoo " + idAoo);

			final IPostaNpsFacadeSRV postaNpsSRV = ApplicationContextProvider.getApplicationContext().getBean(IPostaNpsFacadeSRV.class);

			final RichiestaElabMessaggioPostaNpsDTO<? extends MessaggioPostaNpsDTO> item = postaNpsSRV.recuperaRichiestaElabDaLavorare(idAoo);

			if (item != null) {

				// Processa l'item
				final String guidMessaggio = postaNpsSRV.elaboraMessaggioPostaNpsInIngresso(item);

				if (StringUtils.isNotBlank(guidMessaggio)) {
					logger.info("Elaborazione dell'item " + item.getIdCoda() + " eseguita con successo. GUID registrato: " + guidMessaggio);
				}

			} else {
				logger.info("Nessun item da elaborare");
			}

		} catch (final Exception e) {
			logger.error(callID + " errore generico", e);
		}

		logger.info("-------- END " + callID + " --------");
	}

}