package it.ibm.red.webservice.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.ErrorCode;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.exception.RedWSException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IREDServiceFacadeSRV;
import it.ibm.red.webservice.handlers.LoggingHandler;
import it.ibm.red.webservice.model.redservice.header.v1.CredenzialiUtenteRed;
import it.ibm.red.webservice.model.redservice.messages.v1.AnnullaDocumentoRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.AnnullaDocumentoResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.AssegnaFascicoloFatturaRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.AssegnaFascicoloFatturaResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.CheckInRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.CheckInResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.CheckOutRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.CheckOutResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.ElencoVersioniDocumentoRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.ElencoVersioniDocumentoResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.GetDocumentiFascicoloRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.GetDocumentiFascicoloResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.GetVersioneDocumentoRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.GetVersioneDocumentoResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.InserimentoFascicoloDecretoRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.InserimentoFascicoloDecretoResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.ModificaFascicoloDecretoRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.ModificaFascicoloDecretoRequest.FascicoliFattura;
import it.ibm.red.webservice.model.redservice.messages.v1.ModificaFascicoloDecretoResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.ModificaMetadatiRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.ModificaMetadatiResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.UndoCheckOutRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.UndoCheckOutResponse;
import it.ibm.red.webservice.model.redservice.types.v1.DocumentoRed;
import it.ibm.red.webservice.model.redservice.types.v1.FascicoloRed;
import it.ibm.red.webservice.model.redservice.types.v1.IdentificativoUnitaDocumentaleFEPATypeRed;
import it.ibm.red.webservice.model.redservice.types.v1.MapRed;
import it.ibm.red.webservice.model.redservice.types.v1.MetadatoRed;
import it.ibm.red.webservice.model.redservice.types.v1.ObjectFactory;
import it.ibm.red.webservice.model.redservice.types.v1.RispostaDocRed;
import it.ibm.red.webservice.model.redservice.types.v1.RispostaElencoVersioniDocRed;
import it.ibm.red.webservice.model.redservice.types.v1.RispostaRed;
import it.ibm.red.webservice.model.redservice.types.v1.RispostaVersioneDocRed;
import it.ibm.red.webservice.model.redservice.types.v1.TipoTrattamentoFascicoloFatturaEnum;
import it.ibm.red.webservice.services.IREDService;

@WebService(portName = "REDServicePort", serviceName = "REDService", targetNamespace = "http://redwsevo.mef.gov.it/ibm/REDService", endpointInterface = "it.ibm.red.webservice.services.IREDService")
@HandlerChain(file = "/handlers.xml")
@SuppressWarnings("unused")
public class REDService implements IREDService {

	private static final String REMOVE = "REMOVE";

	/**
	 * Messaggio associato all'assenza contenuti request.
	 */
	private static final String DATO_ASSENTE_IN_REQUEST_MSG = "MESSAGE: Nessun dato contenuto nella request";

	/**
	 * Label Messaggio.
	 */
	private static final String MESSAGE_LABEL = "MESSAGE: ";

	/**
	 * Label id documento.
	 */
	private static final String ID_DOCUMENTO_LABEL = "ID DOCUMENTO: ";

	/**
	 * Label id documento assente.
	 */
	private static final String ERROR_ID_DOCUMENTO_ASSENTE_MSG = "ID DOCUMENTO NON PRESENTE";

	/**
	 * Messaggio errore applicativo.
	 */
	private static final String ERROR_ERRORE_APPLICATIVO_MSG = "ERROR: si è verificato un errore applicativo in GetDocumentiFascicoloService.";

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(REDService.class);

	/**
	 * Context.
	 */
	@Resource
	private WebServiceContext context;

	/**
	 * Service.
	 */
	private IREDServiceFacadeSRV redServiceSRV;

	/**
	 * Factory.
	 */
	private ObjectFactory of;

	/**
	 * Factory.
	 */
	it.ibm.red.webservice.model.redservice.messages.v1.ObjectFactory of2;

	/**
	 * Costruttore.
	 */
	public REDService() {
		try {
			of = new ObjectFactory();
			of2 = new it.ibm.red.webservice.model.redservice.messages.v1.ObjectFactory();
			redServiceSRV = ApplicationContextProvider.getApplicationContext().getBean(IREDServiceFacadeSRV.class);
		} catch (final Exception e) {
			logger.error("Errore in fase di costruzione del servizio", e);
		}
	}

	/**
	 * @see it.ibm.red.webservice.services.IREDService#getVersioneDocumento(it.ibm.red.webservice.model.redservice.messages.v1.GetVersioneDocumentoRequest).
	 */
	@Override
	public GetVersioneDocumentoResponse getVersioneDocumento(final GetVersioneDocumentoRequest getVersioneDocumentoRequest) {
		GetVersioneDocumentoResponse getVersioneDocumentoResponse = null;
		try {
			if (getVersioneDocumentoRequest != null) {
				String idDocumento = "";
				int numeroVersione = -1;
				final String idUtente = getVersioneDocumentoRequest.getCredenzialiUtente().getValue().getIdUtente().getValue();

				if (getVersioneDocumentoRequest.getVersioneDocumentoInput() != null) {
					// CASO VALORIZZAZIONE DELL'IDFASCICOLO
					if ((getVersioneDocumentoRequest.getVersioneDocumentoInput().getValue().getIdFascicolo() != null)
							&& (getVersioneDocumentoRequest.getVersioneDocumentoInput().getValue().getIdFascicolo().getValue().length() > 0)
							&& (!"?".equals(getVersioneDocumentoRequest.getVersioneDocumentoInput().getValue().getIdFascicolo().getValue()))) {
						throw new RedException("ERROR: si è verificato un errore applicativo in GetVersioneDocumentoService. " + "MESSAGE: Funzionalità non implementata");
					} else {
						idDocumento = getVersioneDocumentoRequest.getVersioneDocumentoInput().getValue().getIdDocumento().getValue();
						// CONTROLLO SE IL NUMERO VERSIONE E' STATO INSERITO.. ALTRIMENTI RECUPERO
						// L'ULTIMA VERSIONE
						if (getVersioneDocumentoRequest.getVersioneDocumentoInput().getValue().getNumVersione() != 0) {
							numeroVersione = getVersioneDocumentoRequest.getVersioneDocumentoInput().getValue().getNumVersione();
						}
					}
				}

				getVersioneDocumentoResponse = redServiceSRV.getVersioneDocumento(idUtente, idDocumento, numeroVersione);

			} else {
				throw new RedWSException("ERROR: si è verificato un errore applicativo in GetVersioneDocumentoService. " + "MESSAGE: Non è presente alcun dato nella request",
						Constants.ErrorCode.NO_DATA_INTO_REQUEST_ERROR);
			}

			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
		} catch (final Exception e) {
			logger.error(ERROR_ERRORE_APPLICATIVO_MSG + MESSAGE_LABEL + e.getMessage());
			if (getVersioneDocumentoResponse == null) {
				getVersioneDocumentoResponse = new GetVersioneDocumentoResponse();
			}
			final RispostaVersioneDocRed rispostaVersioneDocRed = new RispostaVersioneDocRed();
			if (e.getClass() == RedWSException.class) {
				rispostaVersioneDocRed.setCodiceErrore(((RedWSException) e).getCode());
			}
			rispostaVersioneDocRed.setDescErrore(of.createRispostaRedDescErrore(ERROR_ERRORE_APPLICATIVO_MSG + MESSAGE_LABEL + e.getMessage()));
			rispostaVersioneDocRed.setEsito(false);
			getVersioneDocumentoResponse.setReturn(of2.createGetVersioneDocumentoResponseReturn(rispostaVersioneDocRed));
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}

		logger.info("END CALL: RedWS.getVersioneDocumento");
		return getVersioneDocumentoResponse;
	}

	/**
	 * @see it.ibm.red.webservice.services.IREDService#modificaMetadati(it.ibm.red.webservice.model.redservice.messages.v1.ModificaMetadatiRequest).
	 */
	@Override
	public ModificaMetadatiResponse modificaMetadati(final ModificaMetadatiRequest modificaMetadatiRequest) {
		logger.info("BEGIN CALL: RedWS.modificaMetadati");
		ModificaMetadatiResponse modificaMetadatiResponse = null;
		try {
			if (modificaMetadatiRequest != null) {
				modificaMetadatiResponse = new ModificaMetadatiResponse();
				final String idUtente = modificaMetadatiRequest.getCredenzialiUtente().getValue().getIdUtente().getValue();
				List<MetadatoRed> metadati = null;
				int idDOcumento = -1;
				if (modificaMetadatiRequest.getDocumento() != null && modificaMetadatiRequest.getDocumento().getValue().getClasseDocumentale() != null) {
					metadati = modificaMetadatiRequest.getDocumento().getValue().getClasseDocumentale().getValue().getMetadato();
					idDOcumento = Integer.parseInt(modificaMetadatiRequest.getDocumento().getValue().getIdDocumento().getValue());
				}

				redServiceSRV.modificaMetadati(idUtente, idDOcumento + "", metadati);

				final RispostaRed rispostaRed = new RispostaRed();
				rispostaRed.setEsito(true);
				modificaMetadatiResponse.setReturn(of2.createModificaMetadatiResponseReturn(rispostaRed));
			} else {
				throw new RedWSException("ERROR: si è verificato un errore applicativo in ModificaMetadatiService. " + DATO_ASSENTE_IN_REQUEST_MSG,
						Constants.ErrorCode.NO_DATA_INTO_REQUEST_ERROR);
			}
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
		} catch (final Exception e) {
			logger.error("ERROR: si è verificato un errore applicativo in ModificaMetadatiService." + MESSAGE_LABEL + e.getMessage());
			if (modificaMetadatiResponse == null) {
				modificaMetadatiResponse = new ModificaMetadatiResponse();
			}
			final RispostaRed rispostaRed = new RispostaRed();
			rispostaRed.setEsito(false);
			rispostaRed.setDescErrore(
					of.createRispostaRedDescErrore("ERROR: si è verificato un errore applicativo in ModificaMetadatiService." + MESSAGE_LABEL + e.getMessage()));
			if (e.getClass() == RedWSException.class) {
				rispostaRed.setCodiceErrore(((RedWSException) e).getCode());
			}
			modificaMetadatiResponse.setReturn(of2.createModificaMetadatiResponseReturn(rispostaRed));

			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
		logger.info("END CALL: RedWS.modificaMetadati");
		return modificaMetadatiResponse;
	}

	/**
	 * @see it.ibm.red.webservice.services.IREDService#checkOut(it.ibm.red.webservice.model.redservice.messages.v1.CheckOutRequest).
	 */
	@Override
	public CheckOutResponse checkOut(final CheckOutRequest checkOutRequest) {
		logger.info("BEGIN CALL: RedWS.checkOut");
		CheckOutResponse checkOutResponse = null;
		try {
			String documentTitle = null;
			if (checkOutRequest != null) {
				if (checkOutRequest.getDocumento() != null) {
					if (checkOutRequest.getDocumento().getValue().getIdDocumento() != null
							&& !"".equals(checkOutRequest.getDocumento().getValue().getIdDocumento().getValue())) {
						documentTitle = checkOutRequest.getDocumento().getValue().getIdDocumento().getValue();
						logger.info(ID_DOCUMENTO_LABEL + documentTitle);
					} else {
						logger.error(ERROR_ID_DOCUMENTO_ASSENTE_MSG);
					}
				}
				final String idUtente = checkOutRequest.getCredenzialiUtente().getValue().getIdUtente().getValue();
				redServiceSRV.checkOut(documentTitle, idUtente);

				checkOutResponse = new CheckOutResponse();
				final RispostaRed rispostaRed = new RispostaRed();
				rispostaRed.setEsito(true);
				checkOutResponse.setReturn(of2.createCheckOutResponseReturn(rispostaRed));
			} else {
				throw new RedWSException("ERROR: si è verificato un errore applicativo in CheckOutRequestService. " + DATO_ASSENTE_IN_REQUEST_MSG,
						Constants.ErrorCode.NO_DATA_INTO_REQUEST_ERROR);
			}
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
		} catch (final Exception e) {
			logger.error("ERROR: si è verificato un errore applicativo in CheckOutRequestService." + MESSAGE_LABEL + e.getMessage());
			if (checkOutResponse == null) {
				checkOutResponse = new CheckOutResponse();
			}
			final RispostaRed rispostaRed = new RispostaRed();
			rispostaRed.setEsito(false);
			rispostaRed
					.setDescErrore(of.createRispostaRedDescErrore("ERROR: si è verificato un errore applicativo in CheckOutRequestService." + MESSAGE_LABEL + e.getMessage()));
			if (e.getClass() == RedWSException.class) {
				rispostaRed.setCodiceErrore(((RedWSException) e).getCode());
			}
			checkOutResponse.setReturn(of2.createModificaMetadatiResponseReturn(rispostaRed));

			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
		logger.info("END CALL: RedWS.checkOut");
		return checkOutResponse;
	}

	/**
	 * @see it.ibm.red.webservice.services.IREDService#elencoVersioniDocumento(it.ibm.red.webservice.model.redservice.messages.v1.ElencoVersioniDocumentoRequest).
	 */
	@Override
	public ElencoVersioniDocumentoResponse elencoVersioniDocumento(final ElencoVersioniDocumentoRequest elencoVersioniDocumentoRequest) {
		logger.info("BEGIN CALL: RedWS.elencoVersioniDocumento");
		ElencoVersioniDocumentoResponse elencoVersioniDocumentoResponse = null;
		try {
			if (elencoVersioniDocumentoRequest != null) {

				if (elencoVersioniDocumentoRequest.getDocumento() != null) {
					if (elencoVersioniDocumentoRequest.getDocumento().getValue().getIdDocumento() != null) {
						logger.info(ID_DOCUMENTO_LABEL + elencoVersioniDocumentoRequest.getDocumento().getValue().getIdDocumento().getValue());
					} else {
						logger.error(ERROR_ID_DOCUMENTO_ASSENTE_MSG);
					}
				}
				logger.info("BEGIN CALL FWS.elencoVersioniDocumento");

				elencoVersioniDocumentoResponse = new ElencoVersioniDocumentoResponse();
				final String idUtente = elencoVersioniDocumentoRequest.getCredenzialiUtente().getValue().getIdUtente().getValue();
				final String idDocumento = elencoVersioniDocumentoRequest.getDocumento().getValue().getIdDocumento().getValue();
				final RispostaElencoVersioniDocRed rispostaElencoVersioniDoc = redServiceSRV.elencoVersioniDocumento(idUtente, idDocumento);
				logger.info("END CALL FWS.elencoVersioniDocumento");
				rispostaElencoVersioniDoc.setEsito(true);
				elencoVersioniDocumentoResponse.setReturn(of2.createElencoVersioniDocumentoResponseReturn(rispostaElencoVersioniDoc));
			} else {
				logger.error("ERROR: si è verificato un errore applicativo in ElencoVersioniDocumentoService. " + DATO_ASSENTE_IN_REQUEST_MSG);
				throw new RedWSException("ERROR: si è verificato un errore applicativo in ElencoVersioniDocumentoService. " + DATO_ASSENTE_IN_REQUEST_MSG,
						Constants.ErrorCode.NO_DATA_INTO_REQUEST_ERROR);
			}
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
		} catch (final Exception e) {
			logger.error("ERROR: si è verificato un errore applicativo in ElencoVersioniDocumentoService." + MESSAGE_LABEL + e.getMessage());
			if (elencoVersioniDocumentoResponse == null) {
				elencoVersioniDocumentoResponse = new ElencoVersioniDocumentoResponse();
			}
			final RispostaElencoVersioniDocRed rispostaRed = new RispostaElencoVersioniDocRed();
			rispostaRed.setEsito(false);
			rispostaRed.setDescErrore(
					of.createRispostaRedDescErrore("ERROR: si è verificato un errore applicativo in ElencoVersioniDocumentoService." + MESSAGE_LABEL + e.getMessage()));
			if (e.getClass() == RedWSException.class) {
				rispostaRed.setCodiceErrore(((RedWSException) e).getCode());
			}
			elencoVersioniDocumentoResponse.setReturn(of2.createElencoVersioniDocumentoResponseReturn(rispostaRed));
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
		logger.info("END CALL: RedWS.elencoVersioniDocumento");
		return elencoVersioniDocumentoResponse;
	}

	/**
	 * @see it.ibm.red.webservice.services.IREDService#getDocumentiFascicolo(it.ibm.red.webservice.model.redservice.messages.v1.GetDocumentiFascicoloRequest).
	 */
	@Override
	public GetDocumentiFascicoloResponse getDocumentiFascicolo(final GetDocumentiFascicoloRequest getDocumentiFascicoloRequest) {
		logger.info("BEGIN CALL: RedWS.getDocumentiFascicolo");
		GetDocumentiFascicoloResponse getDocumentiFascicoloResponse = null;
		try {
			if (getDocumentiFascicoloRequest != null) {

				String idFascicoloFEPA = null;
				getDocumentiFascicoloResponse = new GetDocumentiFascicoloResponse();

				if (getDocumentiFascicoloRequest.getIdentificativoFascicolo() == null) {
					throw new RedWSException("ERROR: si è verificato un errore applicativo in GetDocumentiFascicoloService. "
							+ "MESSAGE: non è stato definito alcun id fascicolo nella request");
				} else {
					idFascicoloFEPA = getDocumentiFascicoloRequest.getIdentificativoFascicolo().getValue();
				}
				final String idUtente = getDocumentiFascicoloRequest.getCredenzialiUtente().getValue().getIdUtente().getValue();
				getDocumentiFascicoloResponse = redServiceSRV.getDocumentiFascicolo(idUtente, idFascicoloFEPA);
			} else {
				throw new RedWSException("ERROR: si è verificato un errore applicativo in GetDocumentiFascicoloService. " + DATO_ASSENTE_IN_REQUEST_MSG,
						Constants.ErrorCode.NO_DATA_INTO_REQUEST_ERROR);
			}
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
		} catch (final Exception e) {
			logger.error(ERROR_ERRORE_APPLICATIVO_MSG + MESSAGE_LABEL + e.getMessage());
			if (getDocumentiFascicoloResponse == null) {
				getDocumentiFascicoloResponse = new GetDocumentiFascicoloResponse();
			}
			final RispostaRed rispostaRed = new RispostaRed();
			rispostaRed.setEsito(false);
			rispostaRed.setDescErrore(of.createRispostaRedDescErrore(ERROR_ERRORE_APPLICATIVO_MSG + MESSAGE_LABEL + e.getMessage()));
			if (e.getClass() == RedWSException.class) {
				rispostaRed.setCodiceErrore(((RedWSException) e).getCode());
			}
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
		logger.info("END CALL: RedWS.getDocumentiFascicolo");
		return getDocumentiFascicoloResponse;
	}

	/**
	 * @see it.ibm.red.webservice.services.IREDService#inserimentoFascicoloDecreto(it.ibm.red.webservice.model.redservice.messages.v1.InserimentoFascicoloDecretoRequest).
	 */
	@Override
	public InserimentoFascicoloDecretoResponse inserimentoFascicoloDecreto(final InserimentoFascicoloDecretoRequest inserimentoFascicoloDecretoRequest) {
		logger.info("BEGIN CALL: RedWS.inserimentoFascicoloDecreto");
		InserimentoFascicoloDecretoResponse inserimentoFascicoloDecretoResponse = null;
		try {
			if (inserimentoFascicoloDecretoRequest != null) {
				final List<IdentificativoUnitaDocumentaleFEPATypeRed> fascicoli = inserimentoFascicoloDecretoRequest.getFascicoliFEPA();
				final DocumentoRed documento = inserimentoFascicoloDecretoRequest.getDocumento().getValue();
				final MapRed metadati = inserimentoFascicoloDecretoRequest.getMetadatiAssegnazione().getValue();
				final CredenzialiUtenteRed credenziali = inserimentoFascicoloDecretoRequest.getCredenzialiUtente().getValue();

				final String idDocRitornato = redServiceSRV.inserimentoFascicoloDecreto(credenziali, metadati, documento, fascicoli);

				inserimentoFascicoloDecretoResponse = new InserimentoFascicoloDecretoResponse();
				documento.setNumVersione(1);
				documento.setIdDocumento(of.createDocumentoRedIdDocumento(idDocRitornato));
				inserimentoFascicoloDecretoResponse.setDocumento(documento);
				final RispostaRed rispostaRed = new RispostaRed();
				rispostaRed.setCodiceErrore(Constants.ErrorCode.SUCCESS);
				rispostaRed.setEsito(true);
			} else {
				logger.error("ERROR: si è verificato un errore applicativo in InserimentoFascicoloDecretoService. " + DATO_ASSENTE_IN_REQUEST_MSG);
				throw new RedWSException(
						"ERROR: si è verificato un errore applicativo in InserimentoFascicoloDecretoService. MESSAGE: Non è presente alcun dato nella request",
						Constants.ErrorCode.NO_DATA_INTO_REQUEST_ERROR);
			}
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
		} catch (final Exception e) {
			logger.error(e.getMessage(), e);
			if (inserimentoFascicoloDecretoResponse == null) {
				inserimentoFascicoloDecretoResponse = new InserimentoFascicoloDecretoResponse();
			}
			final RispostaRed rispostaRed = new RispostaRed();
			rispostaRed.setEsito(false);
			rispostaRed.setDescErrore(
					of.createRispostaRedDescErrore("ERROR: si è verificato un errore applicativo in InserimentoFascicoloDecretoService." + MESSAGE_LABEL + e.getMessage()));
			if (e.getClass() == RedWSException.class) {
				rispostaRed.setCodiceErrore(((RedWSException) e).getCode());
			}
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
		logger.info("END CALL: RedWS.inserimentoFascicoloDecreto");
		return inserimentoFascicoloDecretoResponse;
	}

	/**
	 * @see it.ibm.red.webservice.services.IREDService#assegnaFascicoloFattura(it.ibm.red.webservice.model.redservice.messages.v1.AssegnaFascicoloFatturaRequest).
	 */
	@Override
	public AssegnaFascicoloFatturaResponse assegnaFascicoloFattura(final AssegnaFascicoloFatturaRequest assegnaFascicoloFatturaRequest) {
		logger.info("BEGIN CALL: RedWS.assegnaFascicoloFattura");
		AssegnaFascicoloFatturaResponse assegnaFascicoloFatturaResponse = null;
		try {
			if (assegnaFascicoloFatturaRequest != null) {
				String idFascicoloFEPA = null;

				if (assegnaFascicoloFatturaRequest.getFascicoloDest() != null && assegnaFascicoloFatturaRequest.getFascicoloDest().getValue().getID() != null
						&& assegnaFascicoloFatturaRequest.getFascicoloDest().getValue().getID().getID() != null) {

					idFascicoloFEPA = assegnaFascicoloFatturaRequest.getFascicoloDest().getValue().getID().getID();
				}

				if (idFascicoloFEPA == null || ("".equals(idFascicoloFEPA.trim()))) {
					throw new RedWSException("ERROR: Id fascicolo FEPA non presente", Constants.ErrorCode.ID_FASCICOLO_FEPA_NON_PRESENTE);
				}

				MapRed mapRed = null;
				if (assegnaFascicoloFatturaRequest.getMetadatiAssegnazione() != null) {
					mapRed = assegnaFascicoloFatturaRequest.getMetadatiAssegnazione().getValue();
				}

				final String idDocRitornato = redServiceSRV.assegnaFascicoloFattura(assegnaFascicoloFatturaRequest.getCredenzialiUtente().getValue(), idFascicoloFEPA, mapRed);

				logger.info("IdDocumento inserito da AssegnaFascicoloFattura: " + idDocRitornato);

				assegnaFascicoloFatturaResponse = new AssegnaFascicoloFatturaResponse();
				final FascicoloRed fascicoloRed = new FascicoloRed();
				fascicoloRed.setIdFascicolo(of.createFascicoloRedIdFascicolo(idDocRitornato));
				assegnaFascicoloFatturaResponse.setFascicolo(of2.createAssegnaFascicoloFatturaResponseFascicolo(fascicoloRed));
				final RispostaRed rispostaRed = new RispostaRed();
				rispostaRed.setCodiceErrore(Constants.ErrorCode.SUCCESS);
				rispostaRed.setEsito(true);
				logger.info("*** IdDocumento inserito da AssegnaFascicoloFattura: " + idDocRitornato);
			} else {
				throw new RedWSException("ERROR: si è verificato un errore applicativo in AssegnaFascicoloFatturaService. " + "MESSAGE: Nessun dato presente nella request",
						Constants.ErrorCode.NO_DATA_INTO_REQUEST_ERROR);
			}
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
		} catch (final RedWSException e) {
			logger.error(e.getMessage(), e);
			if (assegnaFascicoloFatturaResponse == null) {
				assegnaFascicoloFatturaResponse = new AssegnaFascicoloFatturaResponse();
			}
			final RispostaRed rispostaRed = new RispostaRed();
			rispostaRed.setCodiceErrore(e.getCode());
			rispostaRed.setDescErrore(of.createRispostaRedDescErrore(e.getMessage()));
			rispostaRed.setEsito(false);
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		} catch (final Exception e) {
			logger.error("FATAL ERROR: impossibile invocare il servizio AssegnaFascicoloFatturaService. "
					+ "Servizio inesistente o errata definizione della factory RedServiceFactory", e);
			if (assegnaFascicoloFatturaResponse == null) {
				assegnaFascicoloFatturaResponse = new AssegnaFascicoloFatturaResponse();
			}
			final RispostaRed rispostaRed = new RispostaRed();
			rispostaRed.setDescErrore(
					of.createRispostaRedDescErrore("ERROR: si è verificato un errore applicativo in AssegnaFascicoloFatturaService." + MESSAGE_LABEL + e.getMessage()));
			rispostaRed.setEsito(false);
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
		logger.info("END CALL: RedWS.assegnaFascicoloFattura");
		return assegnaFascicoloFatturaResponse;
	}

	/**
	 * @see it.ibm.red.webservice.services.IREDService#annullaDocumento(it.ibm.red.webservice.model.redservice.messages.v1.AnnullaDocumentoRequest).
	 */
	@Override
	public AnnullaDocumentoResponse annullaDocumento(final AnnullaDocumentoRequest annullaDocumentoRequest) {
		logger.info("BEGIN CALL: RedWS.annullaDocumento");
		AnnullaDocumentoResponse annullaDocumentoResponse = null;
		try {
			if (annullaDocumentoRequest != null) {
				final String idUtente = annullaDocumentoRequest.getCredenzialiUtente().getValue().getIdUtente().getValue();
				final String idDocumento = annullaDocumentoRequest.getDocumento().getValue().getIdDocumento().getValue();
				final String motivazioneAnnullamento = annullaDocumentoRequest.getMotivazioneAnnullamento().getValue().getMotivazione().getValue();

				redServiceSRV.annullaDocumento(idUtente, idDocumento, motivazioneAnnullamento);

				annullaDocumentoResponse = new AnnullaDocumentoResponse();
				annullaDocumentoResponse.getReturn().getValue().setEsito(true);
			} else {
				throw new RedWSException("ERROR: si è verificato un errore applicativo in AnnullaDocumentoService. " + "MESSAGE: Non è presente alcun dato nella request",
						ErrorCode.NO_DATA_INTO_REQUEST_ERROR);
			}
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
		} catch (final Exception e) {
			if (annullaDocumentoResponse == null) {
				annullaDocumentoResponse = new AnnullaDocumentoResponse();
			}
			final RispostaDocRed rispostaDocRed = new RispostaDocRed();
			if (e.getClass() == RedWSException.class) {
				rispostaDocRed.setCodiceErrore(((RedWSException) e).getCode());
			}
			rispostaDocRed.setDescErrore(of.createRispostaRedDescErrore(e.getMessage()));
			rispostaDocRed.setEsito(false);
			annullaDocumentoResponse.setReturn(of2.createAnnullaDocumentoResponseReturn(rispostaDocRed));
			logger.error(
					"FATAL ERROR: impossibile invocare il servizio AnnullaDocumentoService. " + "Servizio inesistente o errata definizione della factory RedServiceFactory");
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
		logger.info("END CALL: RedWS.annullaDocumento");
		return annullaDocumentoResponse;
	}

	/**
	 * @see it.ibm.red.webservice.services.IREDService#checkIn(it.ibm.red.webservice.model.redservice.messages.v1.CheckInRequest).
	 */
	@Override
	public CheckInResponse checkIn(final CheckInRequest checkInRequest) {
		logger.info("BEGIN CALL: RedWS.checkIn");
		CheckInResponse checkInResponse = null;
		try {
			String documentTitle = null;
			if (checkInRequest != null) {
				if (checkInRequest.getDocumento() != null) {
					if (checkInRequest.getDocumento().getValue().getIdDocumento() != null
							&& !"".equals(checkInRequest.getDocumento().getValue().getIdDocumento().getValue())) {
						documentTitle = checkInRequest.getDocumento().getValue().getIdDocumento().getValue();
						logger.info(ID_DOCUMENTO_LABEL + documentTitle);
					} else {
						logger.error(ERROR_ID_DOCUMENTO_ASSENTE_MSG);
					}
				}
				final String idUtente = checkInRequest.getCredenzialiUtente().getValue().getIdUtente().getValue();

				Map<String, Object> metadatiMap = null;

				if (checkInRequest.getDocumento().getValue().getClasseDocumentale().getValue() != null
						&& checkInRequest.getDocumento().getValue().getClasseDocumentale().getValue().getMetadato() != null
						&& !checkInRequest.getDocumento().getValue().getClasseDocumentale().getValue().getMetadato().isEmpty()) {

					metadatiMap = new HashMap<>();

					for (final MetadatoRed i : checkInRequest.getDocumento().getValue().getClasseDocumentale().getValue().getMetadato()) {
						if (i.getValue() != null) {
							metadatiMap.put(i.getKey().getValue(), i.getValue().getValue());
						} else {
							metadatiMap.put(i.getKey().getValue(), i.getMultiValues());
						}
					}
				}

				checkInResponse = new CheckInResponse();
				checkInResponse.setReturn(of2.createCheckInResponseReturn(redServiceSRV.checkIn(documentTitle, idUtente, metadatiMap,
						checkInRequest.getDocumento().getValue().getAllegati(), checkInRequest.getDocumento().getValue().getDataHandler().getValue())));
			} else {
				throw new RedWSException("ERROR: si è verificato un errore applicativo in CheckInRequestService. " + DATO_ASSENTE_IN_REQUEST_MSG,
						Constants.ErrorCode.NO_DATA_INTO_REQUEST_ERROR);
			}
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
		} catch (final Exception e) {
			logger.error("ERROR: si è verificato un errore applicativo in CheckInRequestService." + MESSAGE_LABEL + e.getMessage());
			if (checkInResponse == null) {
				checkInResponse = new CheckInResponse();
			}
			final RispostaDocRed rispostaRed = new RispostaDocRed();
			rispostaRed.setEsito(false);
			rispostaRed
					.setDescErrore(of.createRispostaRedDescErrore("ERROR: si è verificato un errore applicativo in CheckInRequestService." + MESSAGE_LABEL + e.getMessage()));
			if (e.getClass() == RedWSException.class) {
				rispostaRed.setCodiceErrore(((RedWSException) e).getCode());
			}
			checkInResponse.setReturn(of2.createCheckInResponseReturn(rispostaRed));
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
		logger.info("END CALL: RedWS.checkIn");
		return checkInResponse;
	}

	/**
	 * @see it.ibm.red.webservice.services.IREDService#undoCheckOut(it.ibm.red.webservice.model.redservice.messages.v1.UndoCheckOutRequest).
	 */
	@Override
	public UndoCheckOutResponse undoCheckOut(final UndoCheckOutRequest undoCheckOutRequest) {
		logger.info("BEGIN CALL: RedWS.undoCheckOut");
		UndoCheckOutResponse undoCheckOutResponse = null;
		try {
			String documentTitle = null;
			if (undoCheckOutRequest != null) {
				if (undoCheckOutRequest.getDocumento() != null) {
					if (undoCheckOutRequest.getDocumento().getValue().getIdDocumento() != null
							&& !"".equals(undoCheckOutRequest.getDocumento().getValue().getIdDocumento().getValue())) {
						documentTitle = undoCheckOutRequest.getDocumento().getValue().getIdDocumento().getValue();
						logger.info(ID_DOCUMENTO_LABEL + documentTitle);
					} else {
						logger.error(ERROR_ID_DOCUMENTO_ASSENTE_MSG);
					}
				}

				final String idUtente = undoCheckOutRequest.getCredenzialiUtenteRed().getValue().getIdUtente().getValue();
				redServiceSRV.undoCheckOut(documentTitle, idUtente);

				undoCheckOutResponse = new UndoCheckOutResponse();
				final RispostaRed rispostaRed = new RispostaRed();
				rispostaRed.setEsito(true);
				undoCheckOutResponse.setReturn(of2.createUndoCheckOutResponseReturn(rispostaRed));
			} else {
				throw new RedWSException("ERROR: si è verificato un errore applicativo in UndoCheckOutService. " + DATO_ASSENTE_IN_REQUEST_MSG,
						Constants.ErrorCode.NO_DATA_INTO_REQUEST_ERROR);
			}
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
		} catch (final Exception e) {
			logger.error("ERROR: si è verificato un errore applicativo in UndoCheckOutService." + MESSAGE_LABEL + e.getMessage());
			if (undoCheckOutResponse == null) {
				undoCheckOutResponse = new UndoCheckOutResponse();
			}
			final RispostaRed rispostaRed = new RispostaRed();
			rispostaRed.setEsito(false);
			rispostaRed.setDescErrore(of.createRispostaRedDescErrore("ERROR: si è verificato un errore applicativo in UndoCheckOutService." + MESSAGE_LABEL + e.getMessage()));
			if (e.getClass() == RedWSException.class) {
				rispostaRed.setCodiceErrore(((RedWSException) e).getCode());
			}
			undoCheckOutResponse.setReturn(of2.createModificaMetadatiResponseReturn(rispostaRed));
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
		logger.info("END CALL: RedWS.undoCheckOut");
		return undoCheckOutResponse;
	}

	/**
	 * @see it.ibm.red.webservice.services.IREDService#modificaFascicoloDecreto(it.ibm.red.webservice.model.redservice.messages.v1.ModificaFascicoloDecretoRequest).
	 */
	@Override
	public ModificaFascicoloDecretoResponse modificaFascicoloDecreto(final ModificaFascicoloDecretoRequest modificaFascicoloDecretoRequest) {
		logger.info("BEGIN CALL: RedWS.modificaFascicoloDecreto");
		ModificaFascicoloDecretoResponse modificaFascicoloDecretoResponse = null;
		try {
			// recupero decreto dirigenziale
			final DocumentoRed documentoRed = modificaFascicoloDecretoRequest.getDocumento();
			final CredenzialiUtenteRed credenzialiUtenteRed = modificaFascicoloDecretoRequest.getCredenzialiUtente().getValue();

			int idGruppo;
			String idUtente = null;

			boolean modificaContent = false;
			boolean modificaFascicoloDecreto = false;

			byte[] content = null;
			String contentType = null;

			if (documentoRed != null && documentoRed.getDataHandler() != null && documentoRed.getContentType() != null) {
				contentType = documentoRed.getContentType().getValue();
				content = documentoRed.getDataHandler().getValue();
			}

			if (credenzialiUtenteRed != null) {
				idGruppo = credenzialiUtenteRed.getIdGruppo();
				idUtente = credenzialiUtenteRed.getIdUtente().getValue();
			} else {
				throw new RedWSException("ERROR: Credenziali utente non definite.", ErrorCode.CREDENZIALI_UTENTE_NON_DEFINITE);
			}

			if (idGruppo <= 0) {
				throw new RedWSException("ERROR: Id gruppo (ufficio) non definito.", ErrorCode.ID_GRUPPO_NON_DEFINITO);
			}

			if (idUtente == null || (idUtente != null && "".equals(idUtente.trim()))) {
				throw new RedWSException("ERROR: Id utente non definito.", ErrorCode.ID_UTENTE_NON_DEFINITO);
			}

			if (documentoRed != null && documentoRed.getIdDocumento() != null) {
				final String idDocumentoDecreto = documentoRed.getIdDocumento().getValue();
				// recupero fatture da aggiungere/rimuovere dal decreto dirigenziale
				final List<FascicoliFattura> fascicoliFattura = modificaFascicoloDecretoRequest.getFascicoliFattura();

				List<String> fattureDaAggiungere = null;
				List<String> fattureDaRimuovere = null;

				if (fascicoliFattura != null && !fascicoliFattura.isEmpty()) {

					fattureDaAggiungere = new ArrayList<>();
					fattureDaRimuovere = new ArrayList<>();

					for (final FascicoliFattura fascicolo : fascicoliFattura) {
						if (fascicolo != null && fascicolo.getFascicolo() != null && fascicolo.getFascicolo().getID() != null
								&& fascicolo.getFascicolo().getID().getID() != null) {

							if (fascicolo.getTIPOTRATTAMENTO().equals(TipoTrattamentoFascicoloFatturaEnum.AGGIUNGI)) {
								fattureDaAggiungere.add(fascicolo.getFascicolo().getID().getID());
							} else if (fascicolo.getTIPOTRATTAMENTO().equals(TipoTrattamentoFascicoloFatturaEnum.ELIMINA)) {
								fattureDaRimuovere.add(fascicolo.getFascicolo().getID().getID());
							}
						}
					}
				}

				final HashMap<String, List<String>> map = eliminaDuplicati(fattureDaAggiungere, fattureDaRimuovere);
				fattureDaAggiungere = map.get("ADD");
				fattureDaRimuovere = map.get(REMOVE);

				if (!((fattureDaAggiungere == null || (fattureDaAggiungere != null && fattureDaAggiungere.isEmpty()))
						&& (fattureDaRimuovere == null || (fattureDaRimuovere != null && fattureDaRimuovere.isEmpty())))) {
					modificaFascicoloDecreto = true;
				}

				if (content != null && contentType != null) {
					modificaContent = true;
				}

				if (!modificaFascicoloDecreto && !modificaContent) {
					throw new RedWSException(
							"ERRORE: Non è possibile effettuare alcuna operazione. Definire nella request delle fatture da aggiungere/rimuovere o un nuovo content da inserire.",
							ErrorCode.MODIFICA_FASCICOLO_NO_OPERATION);
				}

				redServiceSRV.modificaDecreto(idGruppo + "", idUtente, idDocumentoDecreto, fattureDaAggiungere, fattureDaRimuovere, content, contentType);

				modificaFascicoloDecretoResponse = new ModificaFascicoloDecretoResponse();
				modificaFascicoloDecretoResponse.setDocumento(documentoRed);
				final RispostaRed rispostaRed = new RispostaRed();
				modificaFascicoloDecretoResponse.setCodiceErrore(of2.createModificaFascicoloDecretoResponseCodiceErrore(ErrorCode.SUCCESS));
				rispostaRed.setEsito(true);
			} else {
				throw new RedWSException(
						"ERROR: L'id del decreto dirigenziale non è definito. MESSAGE: Non è possibile aggiungere/rimuovere fatture dal decreto dirigenziale.",
						ErrorCode.ID_DECRETO_DIRIGENZIALE_NON_PRESENTE);
			}
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
		} catch (final Exception e) {
			if (modificaFascicoloDecretoResponse == null) {
				modificaFascicoloDecretoResponse = new ModificaFascicoloDecretoResponse();
			}
			final RispostaRed rispostaRed = new RispostaRed();
			if (e.getClass() == RedWSException.class) {
				modificaFascicoloDecretoResponse.setCodiceErrore(of2.createModificaFascicoloDecretoResponseCodiceErrore(((RedWSException) e).getCode()));
			}
			rispostaRed.setDescErrore(of.createRispostaRedDescErrore(e.getMessage()));
			rispostaRed.setEsito(false);
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
		logger.info("END CALL: RedWS.modificaFascicoloDecreto");
		return modificaFascicoloDecretoResponse;
	}

	private static HashMap<String, List<String>> eliminaDuplicati(final List<String> fattureDaAggiungere, final List<String> fattureDaRimuovere) {
		final HashMap<String, List<String>> map = new HashMap<>();
		map.put("ADD", fattureDaAggiungere);
		map.put(REMOVE, fattureDaRimuovere);

		if (fattureDaAggiungere != null && !fattureDaAggiungere.isEmpty() && fattureDaRimuovere != null && !fattureDaRimuovere.isEmpty()) {

			final ArrayList<String> rmvList = new ArrayList<>();

			for (int i = 0; i < fattureDaAggiungere.size(); i++) {
				for (int j = 0; j < fattureDaRimuovere.size(); j++) {
					if (fattureDaAggiungere.get(i).equals(fattureDaRimuovere.get(j)) && !rmvList.contains(fattureDaAggiungere.get(i))) {
						rmvList.add(fattureDaAggiungere.get(i));
					}
				}
			}

			for (int i = 0; i < rmvList.size(); i++) {
				while (fattureDaAggiungere.contains(rmvList.get(i))) {
					fattureDaAggiungere.remove(rmvList.get(i));
				}
				while (fattureDaRimuovere.contains(rmvList.get(i))) {
					fattureDaRimuovere.remove(rmvList.get(i));
				}
			}
		}
		HashSet<String> hs = new HashSet<>();
		hs.addAll(fattureDaAggiungere);
		fattureDaAggiungere.clear();
		fattureDaAggiungere.addAll(hs);

		hs = new HashSet<>();
		hs.addAll(fattureDaRimuovere);
		fattureDaRimuovere.clear();
		fattureDaRimuovere.addAll(hs);

		map.put("ADD", fattureDaAggiungere);
		map.put(REMOVE, fattureDaRimuovere);

		return map;
	}

}