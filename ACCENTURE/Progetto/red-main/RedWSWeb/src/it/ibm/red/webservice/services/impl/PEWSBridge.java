package it.ibm.red.webservice.services.impl;

import java.util.Arrays;

import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IFepaSRV;
import it.ibm.red.business.service.IVistiSRV;
import it.ibm.red.business.service.facade.IFepaFacadeSRV;
import it.ibm.red.business.service.facade.IVistiFacadeSRV;
import it.ibm.red.webservice.handlers.LoggingHandler;
import it.ibm.red.webservice.services.IPEWSBridge;

@WebService(
		portName = "PEWSBridgePort",
        serviceName = "PEWSBridgeService",
        targetNamespace = "http://redwsevo.mef.gov.it/ibm/PEWSBridge",
        endpointInterface = "it.ibm.red.webservice.services.IPEWSBridge")
@HandlerChain(file = "/handlers.xml")
public class PEWSBridge implements IPEWSBridge {
	
	/**
	 * Label comunicazione fine operazione.
	 */
	private static final String WORK_DONE = "workDone";

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(PEWSBridge.class);
	
	/**
	 * Contesto.
	 */
	@Resource
    private WebServiceContext context;
	
	/**
	 * Servizio.
	 */
	private IVistiFacadeSRV vistiSRV;
	
	/**
	 * Servizio.
	 */
	private IFepaFacadeSRV fepaSRV;
	
	/**
	 * Costruttore.
	 */
	public PEWSBridge() {
		try {
			vistiSRV = ApplicationContextProvider.getApplicationContext().getBean(IVistiSRV.class);
			fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaSRV.class);
		} catch (final Exception e) {
			logger.error("Errore in fase di costruzione del servizio", e);
		}
	}

	/**
	 * @see it.ibm.red.webservice.services.IPEWSBridge#terminaWfVisti(java.lang.Integer,
	 *      java.lang.String, java.lang.Integer).
	 */
	@Override
	public String terminaWfVisti(final Integer idDocumento, final String fWorkflowNumber, final Integer idNodoChiamante) {
		
		logger.info("terminaWfVisti (" + idDocumento + ", " + fWorkflowNumber + ", " + idNodoChiamante + ")");

		final int esito = vistiSRV.terminaWfVisti("" + idDocumento, fWorkflowNumber, (long) idNodoChiamante);
		if (esito == 1) {
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
		} else {
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
		
		return WORK_DONE;
	
	}

	/**
	 * @see it.ibm.red.webservice.services.IPEWSBridge#avanzaWf(java.lang.Integer,
	 *      java.lang.String, java.lang.Integer).
	 */
	@Override
	public String avanzaWf(final Integer idDocumento, final String fWorkflowNumber, final Integer idNodoChiamante) {
		
		logger.info("avanzaWf (" + idDocumento + ", " + fWorkflowNumber + ", " + idNodoChiamante + ")");

		final int esito = vistiSRV.avanzaWfNonVistiNonContributi("" + idDocumento, fWorkflowNumber, (long) idNodoChiamante);
		if (esito == 1) {
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
		} else {
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
		
		return WORK_DONE;
		
	}

	/**
	 * @see it.ibm.red.webservice.services.IPEWSBridge#inviaFascicoloFepa(java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public String inviaFascicoloFepa(final String idFascicolo, final String wobDichiarazioneServiziResi) {
		logger.info("inviaFascicoloFepa (" + idFascicolo + ", {" + wobDichiarazioneServiziResi +  "})");

		final String[] wobs = StringUtils.split(wobDichiarazioneServiziResi, ",");
		
		final int esito = fepaSRV.inviaDDDSRFirmati(idFascicolo, Arrays.asList(wobs));
		if (esito == 1) {
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
		} else {
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
		
		return WORK_DONE;
	}
	
	/**
	 * @see it.ibm.red.webservice.services.IPEWSBridge#inviaDecretoIvaFepa(java.lang.String).
	 */
	@Override
	public String inviaDecretoIvaFepa(final String idDecreto) {
		logger.info("inviaDecretoIvaFepa (" + idDecreto +  ")");

		final int esito = fepaSRV.inviaDD(idDecreto);
		if (esito == 1) {
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
		} else {
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
		return WORK_DONE;
	}

}