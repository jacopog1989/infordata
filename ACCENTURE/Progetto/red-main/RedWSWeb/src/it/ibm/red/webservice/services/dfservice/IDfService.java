package it.ibm.red.webservice.services.dfservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

import it.ibm.red.webservice.model.dfservice.types.messages.RedMappingOrganigrammaResponseType;
import it.ibm.red.webservice.model.dfservice.types.messages.RedMappingOrganigrammaType;

@WebService(name = "DfService", targetNamespace = "http://dfservice.model.webservice.red.ibm.it")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
	it.ibm.red.webservice.model.dfservice.types.messages.ObjectFactory.class,
	it.ibm.red.webservice.model.documentservice.types.security.ObjectFactory.class
})
public interface IDfService {

	/**
	 * Ottiene il mapping dell'organigramma.
	 * @param parameters
	 * @return resonse di recupero mapping dell'organigramma
	 */
	@WebMethod(operationName = "redMappingOrganigramma", action = "urn:#redMappingOrganigramma")
	@WebResult(name = "redMappingOrganigrammaResponse", targetNamespace = "http://messages.types.dfservice.model.webservice.red.ibm.it", partName = "parameters")
	RedMappingOrganigrammaResponseType redMappingOrganigramma(
			@WebParam(name = "redMappingOrganigramma", targetNamespace = "http://messages.types.dfservice.model.webservice.red.ibm.it", partName = "parameters") RedMappingOrganigrammaType parameters);
	
}