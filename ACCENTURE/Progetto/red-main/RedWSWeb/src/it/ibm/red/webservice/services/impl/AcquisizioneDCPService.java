package it.ibm.red.webservice.services.impl;

import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IIgepaFacadeSRV;
import it.ibm.red.webservice.handlers.LoggingHandler;
import it.ibm.red.webservice.model.igepa.psi.types.xsd.AcquisizioneDCPRequest;
import it.ibm.red.webservice.model.igepa.psi.types.xsd.AcquisizioneDCPResponse;
import it.ibm.red.webservice.model.igepa.psi.types.xsd.BaseResponse;
import it.ibm.red.webservice.model.igepa.psi.types.xsd.DocumentoCertificazionePattoType;
import it.ibm.red.webservice.model.igepa.psi.types.xsd.InvocationStatus;
import it.ibm.red.webservice.model.igepa.psi.types.xsd.ObjectFactory;
import it.ibm.red.webservice.services.IAcquisizioneDCPService;

@WebService(portName = "AcquisizioneDCPServicePort", serviceName = "AcquisizioneDCPService", targetNamespace = "http://redwsevo.mef.gov.it/ibm/AcquisizioneDCPService", endpointInterface = "it.ibm.red.webservice.services.IAcquisizioneDCPService")
@HandlerChain(file = "/handlers.xml")
public class AcquisizioneDCPService implements IAcquisizioneDCPService {

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(AcquisizioneDCPService.class);

	/**
	 * Context.
	 */
	@Resource
	private WebServiceContext context;

	/**
	 * Servizio igepa.
	 */
	private IIgepaFacadeSRV igepaSRV;

	/**
	 * Inizializza l'attributo igepaSRV.
	 */
	public AcquisizioneDCPService() {
		try {
			igepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IIgepaFacadeSRV.class);
		} catch (final Exception e) {
			logger.error("Errore in fase di costruzione del servizio", e);
		}
	}

	/**
	 * Effettua l'acquisizione DCP.
	 * @param request
	 * @return response
	 */
	@Override
	public AcquisizioneDCPResponse acquisizioneDCP(final AcquisizioneDCPRequest request) {
		final BaseResponse baseResponse = new BaseResponse();
		final AcquisizioneDCPResponse r = new AcquisizioneDCPResponse();
		final ObjectFactory of = new ObjectFactory();
		try {

			logger.info("REQUEST ACQUISIZIONE_DCP" + request);

			final DocumentoCertificazionePattoType docCertPrequest = request.getAcquisizioneDCPRequest().getValue();

			String oggetto = null;
			if (docCertPrequest.getOggettoProtocollo() != null) {
				oggetto = docCertPrequest.getOggettoProtocollo().getValue();
			}

			final int idNodoDestinatario = docCertPrequest.getIdUfficioDestinatario() != null && docCertPrequest.getIdUfficioDestinatario().getValue() != null
					? docCertPrequest.getIdUfficioDestinatario().getValue()
					: 0;
			final int idUtenteDestinatario = docCertPrequest.getIdUtenteDestinatario() != null && docCertPrequest.getIdUtenteDestinatario().getValue() != null
					? docCertPrequest.getIdUtenteDestinatario().getValue()
					: 0;

			int numProtocollo = 0;
			if (docCertPrequest.getNumeroProtocollo() != null) {
				numProtocollo = docCertPrequest.getNumeroProtocollo().getValue();
			}

			String dataProtocollo = null;
			if (docCertPrequest.getDataProtocollo() != null && docCertPrequest.getDataProtocollo().getValue() != null
					&& docCertPrequest.getDataProtocollo().getValue().getDataFormatType() != null) {
				dataProtocollo = docCertPrequest.getDataProtocollo().getValue().getDataFormatType().getValue();
			}

			String nomeFile = null;
			byte[] content = null;
			String contentType = null;
			if (docCertPrequest.getDocumentoPrincipale() != null && docCertPrequest.getDocumentoPrincipale().getValue() != null) {
				if (docCertPrequest.getDocumentoPrincipale().getValue().getFileName() != null) {
					nomeFile = docCertPrequest.getDocumentoPrincipale().getValue().getFileName().getValue();
				}

				if (docCertPrequest.getDocumentoPrincipale().getValue().getFileBase64() != null
						&& docCertPrequest.getDocumentoPrincipale().getValue().getFileBase64().getValue() != null) {

					if (docCertPrequest.getDocumentoPrincipale().getValue().getFileBase64().getValue().getBase64Binary() != null) {
						content = docCertPrequest.getDocumentoPrincipale().getValue().getFileBase64().getValue().getBase64Binary().getValue();
					}

					if (docCertPrequest.getDocumentoPrincipale().getValue().getFileBase64().getValue().getContentType() != null
							&& docCertPrequest.getDocumentoPrincipale().getValue().getFileBase64().getValue().getContentType().getValue() != null
							&& docCertPrequest.getDocumentoPrincipale().getValue().getFileBase64().getValue().getContentType().getValue().getContentTypeType0() != null) {
						contentType = docCertPrequest.getDocumentoPrincipale().getValue().getFileBase64().getValue().getContentType().getValue().getContentTypeType0()
								.getValue();
					}

				}
			}

			String nomeAll = null;
			byte[] contentAll = null;
			String contentTypeAll = null;
			if (docCertPrequest.getAllegato() != null && docCertPrequest.getAllegato().getValue() != null) {
				if (docCertPrequest.getAllegato().getValue().getFileName() != null) {
					nomeAll = docCertPrequest.getAllegato().getValue().getFileName().getValue();
				}

				if (docCertPrequest.getAllegato().getValue().getFileBase64() != null && docCertPrequest.getAllegato().getValue().getFileBase64().getValue() != null
						&& docCertPrequest.getAllegato().getValue().getFileBase64().getValue().getBase64Binary() != null) {

					contentAll = docCertPrequest.getAllegato().getValue().getFileBase64().getValue().getBase64Binary().getValue();
					if (docCertPrequest.getAllegato().getValue().getFileBase64().getValue().getContentType() != null
							&& docCertPrequest.getAllegato().getValue().getFileBase64().getValue().getContentType().getValue() != null
							&& docCertPrequest.getAllegato().getValue().getFileBase64().getValue().getContentType().getValue().getContentTypeType0() != null) {

						contentTypeAll = docCertPrequest.getAllegato().getValue().getFileBase64().getValue().getContentType().getValue().getContentTypeType0().getValue();
					}
				}
			}

			String aliasMittente = null;
			if (docCertPrequest.getEnteMittente() != null) {
				aliasMittente = docCertPrequest.getEnteMittente().getValue();
			}

			final String iddocumento = igepaSRV.insertDocumento(oggetto, nomeFile, idNodoDestinatario, idUtenteDestinatario, numProtocollo, dataProtocollo, content,
					contentType, contentAll, contentTypeAll, nomeAll, aliasMittente);

			if (iddocumento == null) {
				throw new RedException("Errore in fase di creazione del documento");
			} else {
				logger.info("ACQUISIZIONE_DCP IDDOCUMENTO " + iddocumento);
				final InvocationStatus is = new InvocationStatus();
				is.setValue(of.createInvocationStatusValue("99 - Operazione Eseguita"));
				baseResponse.setStatus(of.createBaseResponseStatus(is));
				baseResponse.setDescription(of.createBaseResponseDescription("Documento creato con id: " + iddocumento));
				r.setAcquisizioneDCPResponse(of.createAcquisizioneDCPResponseAcquisizioneDCPResponse(baseResponse));
			}

			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

		} catch (final Exception e) {
			logger.error(e.getMessage(), e);
			baseResponse.setDescription(of.createBaseResponseDescription(e.getMessage()));
			final InvocationStatus is = new InvocationStatus();
			is.setValue(of.createInvocationStatusValue("98 - Errore generico"));
			baseResponse.setStatus(of.createBaseResponseStatus(is));
			r.setAcquisizioneDCPResponse(of.createAcquisizioneDCPResponseAcquisizioneDCPResponse(baseResponse));
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
		return r;
	}

}