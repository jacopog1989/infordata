package it.ibm.red.webservice.services.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import it.ibm.red.business.enums.SiebelErrorEnum;
import it.ibm.red.business.enums.StatoRdSSiebelEnum;
import it.ibm.red.business.exception.SiebelException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ISiebelFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.webservice.handlers.LoggingHandler;
import it.ibm.red.webservice.model.redsiebel.types.AggiornaRdsRequestType;
import it.ibm.red.webservice.model.redsiebel.types.AggiornaRdsResponseType;
import it.ibm.red.webservice.model.redsiebel.types.AllegatoType;
import it.ibm.red.webservice.model.redsiebel.types.ObjectFactory;
import it.ibm.red.webservice.services.IRedSiebelService;

@WebService(
		portName = "RedSiebelServicePort",
        serviceName = "RedSiebelService",
        targetNamespace = "http://redwsevo.mef.gov.it/ibm/RedSiebelService",
        endpointInterface = "it.ibm.red.webservice.services.IRedSiebelService")
@HandlerChain(file = "/handlers.xml")
public class RedSiebelService implements IRedSiebelService {
	
	/**
	 * Label.
	 */
	private static final String SISTEMA_ORIGINE = "SIEBEL";
	
	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(RedSiebelService.class);
				
	/**
	 * Context.
	 */
	@Resource
    private WebServiceContext context;
	
	/**
	 * Service.
	 */
	private ISiebelFacadeSRV siebelSRV;
		
	/**
	 * Factory.
	 */
	private ObjectFactory of;
	
	/**
	 * Costruttore.
	 */
	public RedSiebelService() {
		try {
			siebelSRV = ApplicationContextProvider.getApplicationContext().getBean(ISiebelFacadeSRV.class);
	        of = new ObjectFactory();
		} catch (final Exception e) {
			logger.error("Errore in fase di costruzione del servizio", e);
		}
    }

	/**
	 * @see it.ibm.red.webservice.services.IRedSiebelService#aggiornaRds(it.ibm.red.webservice.model.redsiebel.types.AggiornaRdsRequestType).
	 */
	@Override
	public AggiornaRdsResponseType aggiornaRds(final AggiornaRdsRequestType request) {
		
		final String tidSiebel = request.getTid();
		final String logRadix = "[RedSiebelService][aggiornaRds][" + tidSiebel + "] ";
		
		final AggiornaRdsResponseType responseType = of.createAggiornaRdsResponseType();
		
		try {
			
			logger.info(logRadix + "start");
			
			responseType.setDataElaborazioneRed(DateUtils.buildXmlGregorianCalendarFromDate(new Date()));
			responseType.setIdRdsSiebel(request.getIdRdsSiebel());
			responseType.setErrorCode(0);
			responseType.setEsito("OK");
			
			//valida i dati in input
			final String validationError = validaRequest(request);
			if (validationError != null) {
				
				responseType.setErrorCode(SiebelErrorEnum.VALIDATION_ERROR.getErrorCode());
				responseType.setEsito(validationError);
			
			} else {
				
				//controlla che l'id rds esista su red e che i dati di protocollo siano effettivamente associati all'id rds
				checkAndClose(request, logRadix, responseType);
			}
			
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
		
		} catch (final Exception e) {
			logger.error(logRadix + "errore generico", e);
			responseType.setErrorCode(SiebelErrorEnum.GENERIC_ERROR.getErrorCode());
			responseType.setEsito("Errore generico");
			
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
		
		logger.info(logRadix + "response: [" + responseType.getErrorCode() + "] " + responseType.getEsito());
		logger.info(logRadix + "end");
		
		return responseType;
	}

	/**
	 * @param request
	 * @param logRadix
	 * @param responseType
	 */
	private void checkAndClose(final AggiornaRdsRequestType request, final String logRadix, final AggiornaRdsResponseType responseType) {
		try {
			
			final String checkInfoAggiornaRdSError = siebelSRV.checkInfoAggiornaRds(request.getIdRdsSiebel(),
				request.getNumeroProtocolloRed(), request.getAnnoProtocolloRed());
			
			if (checkInfoAggiornaRdSError != null) {
				
				responseType.setErrorCode(SiebelErrorEnum.INFO_UNKNOWN_ERROR.getErrorCode());
				responseType.setEsito(checkInfoAggiornaRdSError);
				
				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
			
			} else {
				
				gestisciChiusuraSiebel(request, logRadix, responseType);
			}
			
		} catch (final SiebelException e) {
			logger.error(logRadix + "errore durante il recupero della Rds " + request.getIdRdsSiebel() + " sul database", e);
			responseType.setErrorCode(e.getCode());
			responseType.setEsito(e.getMessage());
			
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
	}

	/**
	 * Gestisce la chiusura Siebel.
	 * @param request
	 * @param logRadix
	 * @param responseType
	 */
	private void gestisciChiusuraSiebel(final AggiornaRdsRequestType request, final String logRadix,
			final AggiornaRdsResponseType responseType) {
		try {
		
			List<AllegatoType> allegati = null;
			if (request.getAllegati() != null) {
				allegati = request.getAllegati().getAllegato();
			}
			
			siebelSRV.aggiornaRds(request.getIdRdsSiebel(),
				request.getNumeroProtocolloRed(), request.getAnnoProtocolloRed(),
				request.getDataChiusuraRdsSiebel().toGregorianCalendar().getTime(), 
				request.getDataEvento().toGregorianCalendar().getTime(),
				request.getSoluzione(), request.getTipoEvento(),
				allegati);
		
		} catch (final SiebelException e) {
			logger.error(logRadix + "errore in fase di aggiornamento della Rds " + request.getIdRdsSiebel(), e);
			responseType.setErrorCode(e.getCode());
			responseType.setEsito(e.getMessage());
			
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
	}
	
	/**
	 * Esegue i controlli formali sui dati in input
	 * 
	 * @param request
	 * @return
	 */
	private static String validaRequest(final AggiornaRdsRequestType request) {
		
		
		if (StringUtils.isNullOrEmpty(request.getTid())) {
			return "tid deve essere valorizzato";
		}
		
		if (StringUtils.isNullOrEmpty(request.getSistemaOrigine())) {
			return "sistemaOrigine deve essere valorizzato";
		} else if (!request.getSistemaOrigine().equals(SISTEMA_ORIGINE)) {
			return "sistemaOrigine non riconosciuto";
		}
		
		if (StringUtils.isNullOrEmpty(request.getIdRdsSiebel())) {
			return "idRdsSiebel deve essere valorizzato";
		}
		
		if (StringUtils.isNullOrEmpty(request.getTipoEvento())) {
			return "tipoEvento deve essere valorizzato";
		} else if (StatoRdSSiebelEnum.getByEvento(request.getTipoEvento()) == null) {
			return "tipoEvento non riconosciuto";
		}
		
		if (request.getDataChiusuraRdsSiebel() == null) {
			return "dataChiusuraRdsSiebel deve essere valorizzata";
		}
		
		if (request.getDataEvento() == null) {
			return "dataEvento deve essere valorizzata";
		}
			
		return null;
	}
	
}