package it.ibm.red.webservice.services.impl;

import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IIgepaFacadeSRV;
import it.ibm.red.webservice.handlers.LoggingHandler;
import it.ibm.red.webservice.model.igepa.allegatorichiestaopf.types.xsd.AllegatoIgepaRequest;
import it.ibm.red.webservice.model.igepa.allegatorichiestaopf.types.xsd.AllegatoIgepaResponse;
import it.ibm.red.webservice.model.igepa.allegatorichiestaopf.types.xsd.AllegatoRichiestaOPF;
import it.ibm.red.webservice.model.igepa.allegatorichiestaopf.types.xsd.BaseResponse;
import it.ibm.red.webservice.model.igepa.allegatorichiestaopf.types.xsd.InvocationStatus;
import it.ibm.red.webservice.model.igepa.allegatorichiestaopf.types.xsd.ObjectFactory;
import it.ibm.red.webservice.services.IAllegatoIgepaService;

@WebService(portName = "AllegatoIgepaServicePort", serviceName = "AllegatoIgepaService", targetNamespace = "http://redwsevo.mef.gov.it/ibm/AllegatoIgepaService", endpointInterface = "it.ibm.red.webservice.services.IAllegatoIgepaService")
@HandlerChain(file = "/handlers.xml")
public class AllegatoIgepaService implements IAllegatoIgepaService {

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(AllegatoIgepaService.class);

	/**
	 * Context.
	 */
	@Resource
	private WebServiceContext context;

	/**
	 * Servizio igepa.
	 */
	private IIgepaFacadeSRV igepaSRV;

	/**
	 * Imposta igepaSRV.
	 */
	public AllegatoIgepaService() {
		try {
			igepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IIgepaFacadeSRV.class);
		} catch (final Exception e) {
			logger.error("Errore in fase di costruzione del servizio", e);
		}
	}

	/**
	 * @param request
	 * @return response
	 */
	@Override
	public AllegatoIgepaResponse allegatoRichiestaIgepa(final AllegatoIgepaRequest request) {

		final AllegatoIgepaResponse allegatoIgepaResponse = new AllegatoIgepaResponse();
		final BaseResponse baseReponse = new BaseResponse();
		final ObjectFactory of = new ObjectFactory();

		try {
			logger.info("REQUEST ACQUISIZIONE_DCP" + request);

			final AllegatoRichiestaOPF allegatoPrequestOpf = request.getAllegatoIgepaRequest().getValue();

			Integer numeroRichiesta = null;
			if (allegatoPrequestOpf.getNumeroRichiesta() != null) {
				numeroRichiesta = allegatoPrequestOpf.getNumeroRichiesta().getValue();
			}

			String annoRichiesta = null;
			if (allegatoPrequestOpf.getAnnoRichiesta() != null && allegatoPrequestOpf.getAnnoRichiesta().getValue() != null
					&& allegatoPrequestOpf.getAnnoRichiesta().getValue().getAnnoFormatType() != null) {
				annoRichiesta = allegatoPrequestOpf.getAnnoRichiesta().getValue().getAnnoFormatType().getValue();
			}

			int numProtocollo = 0;
			if (allegatoPrequestOpf.getNumeroProtocollo() != null) {
				numProtocollo = allegatoPrequestOpf.getNumeroProtocollo().getValue();
			}

			String oggetto = null;
			if (allegatoPrequestOpf.getOggettoAllegato() != null) {
				oggetto = allegatoPrequestOpf.getOggettoAllegato().getValue();
			}

			int tipologia = 0;
			if (allegatoPrequestOpf.getTipologiaAllegato() != null) {
				tipologia = allegatoPrequestOpf.getTipologiaAllegato().getValue();
			}

			String filename = null;
			byte[] content = null;
			String mimeType = null;
			if (allegatoPrequestOpf.getDocumentoAllegato() != null && allegatoPrequestOpf.getDocumentoAllegato().getValue() != null) {
				if (allegatoPrequestOpf.getDocumentoAllegato().getValue().getFileName() != null) {
					filename = allegatoPrequestOpf.getDocumentoAllegato().getValue().getFileName().getValue();
				}

				if (allegatoPrequestOpf.getDocumentoAllegato().getValue().getFileBase64() != null
						&& allegatoPrequestOpf.getDocumentoAllegato().getValue().getFileBase64().getValue() != null
						&& allegatoPrequestOpf.getDocumentoAllegato().getValue().getFileBase64().getValue().getBase64Binary() != null) {

					content = allegatoPrequestOpf.getDocumentoAllegato().getValue().getFileBase64().getValue().getBase64Binary().getValue();

					if (allegatoPrequestOpf.getDocumentoAllegato().getValue().getFileBase64().getValue().getContentType() != null
							&& allegatoPrequestOpf.getDocumentoAllegato().getValue().getFileBase64().getValue().getContentType().getValue() != null && allegatoPrequestOpf
									.getDocumentoAllegato().getValue().getFileBase64().getValue().getContentType().getValue().getContentTypeType0() != null) {
						mimeType = allegatoPrequestOpf.getDocumentoAllegato().getValue().getFileBase64().getValue().getContentType().getValue().getContentTypeType0()
								.getValue();
					}

				}
			}

			final String iddocumento = igepaSRV.allegaRichiestaIgepa(annoRichiesta, numeroRichiesta, numProtocollo, oggetto, tipologia, filename, content, mimeType);

			baseReponse.setDescription(of.createBaseResponseDescription("00 - Operazione Eseguita. Inserito allegato per la richiesta con id: " + iddocumento));
			final InvocationStatus is = new InvocationStatus();
			is.setValue(of.createInvocationStatusValue("00 - Operazione Eseguita"));
			baseReponse.setStatus(of.createBaseResponseStatus(is));

			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

		} catch (final Exception e) {
			logger.error(e.getMessage(), e);
			baseReponse.setDescription(of.createBaseResponseDescription(e.getMessage()));
			final InvocationStatus is = new InvocationStatus();
			is.setValue(of.createInvocationStatusValue("99 - Errore"));
			baseReponse.setStatus(of.createBaseResponseStatus(is));

			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}

		allegatoIgepaResponse.setAllegatoIgepaResponse(of.createAllegatoIgepaResponseAllegatoIgepaResponse(baseReponse));
		logger.info("END CALL: AllegatoIgepaService.allegatoRichiestaIgepa");
		return allegatoIgepaResponse;
	}

}