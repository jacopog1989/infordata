package it.ibm.red.webservice.services.impl.documentservice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.ws.WebServiceContext;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.dto.ConservazioneWsDTO;
import it.ibm.red.business.dto.DocumentoWsDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.FaldoneWsDTO;
import it.ibm.red.business.dto.FascicoloWsDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.PropertyTemplateDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.RicercaRubricaResultDTO;
import it.ibm.red.business.dto.RisultatoRicercaWsDTO;
import it.ibm.red.business.dto.SecurityWsDTO;
import it.ibm.red.business.dto.SecurityWsDTO.GruppoWs;
import it.ibm.red.business.dto.SecurityWsDTO.UtenteWs;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.facade.IAooFacadeSRV;
import it.ibm.red.business.service.ws.facade.ICheckDocumentWsFacadeSRV;
import it.ibm.red.business.service.ws.facade.IContattoWsFacadeSRV;
import it.ibm.red.business.service.ws.facade.ICreaDocumentoEntrataWsFacadeSRV;
import it.ibm.red.business.service.ws.facade.ICreaDocumentoUscitaWsFacadeSRV;
import it.ibm.red.business.service.ws.facade.IDocumentoWsFacadeSRV;
import it.ibm.red.business.service.ws.facade.IFaldoneWsFacadeSRV;
import it.ibm.red.business.service.ws.facade.IFascicoloWsFacadeSRV;
import it.ibm.red.business.service.ws.facade.IOrganigrammaWsFacadeSRV;
import it.ibm.red.business.service.ws.facade.IRedStampigliaturaDocWsFacadeSRV;
import it.ibm.red.business.service.ws.facade.IRicercaWsFacadeSRV;
import it.ibm.red.business.service.ws.facade.IStampigliaturaDocWsFacadeSRV;
import it.ibm.red.business.service.ws.facade.IWorkFlowWsFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.webservice.handlers.LoggingHandler;
import it.ibm.red.webservice.model.documentservice.dto.ErrorCode;
import it.ibm.red.webservice.model.documentservice.types.documentale.ClasseDocumentale;
import it.ibm.red.webservice.model.documentservice.types.documentale.Documento;
import it.ibm.red.webservice.model.documentservice.types.documentale.Faldone;
import it.ibm.red.webservice.model.documentservice.types.documentale.Fascicolo;
import it.ibm.red.webservice.model.documentservice.types.documentale.Metadato;
import it.ibm.red.webservice.model.documentservice.types.documentale.PropertyClasseDocumentale;
import it.ibm.red.webservice.model.documentservice.types.documentale.TipoProperty;
import it.ibm.red.webservice.model.documentservice.types.messages.*;
import it.ibm.red.webservice.model.documentservice.types.organigramma.Ufficio;
import it.ibm.red.webservice.model.documentservice.types.processo.Workflow;
import it.ibm.red.webservice.model.documentservice.types.rubrica.Contatto;
import it.ibm.red.webservice.model.documentservice.types.security.Gruppo;
import it.ibm.red.webservice.model.documentservice.types.security.Security;
import it.ibm.red.webservice.model.documentservice.types.security.Utente;
import it.ibm.red.webservice.services.documentservice.IDocumentService;
import it.ibm.red.webservice.services.documentservice.validator.ValidatorUtility;

@WebService(portName = "DocumentServicePort", serviceName = "DocumentService", targetNamespace = "http://documentservice.model.webservice.red.ibm.it", endpointInterface = "it.ibm.red.webservice.services.documentservice.IDocumentService")
@HandlerChain(file = "/handlers.xml")
public class DocumentService implements IDocumentService {

	/**
	 * Messaggio di errore elaborazione. Occorre appendere messaggio di errore
	 * specifico.
	 */
	private static final String ERROR_ELABORAZIONE_MSG = "[DocumentService][redProtocolloNPSSync] Errore di elaborazione: ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DocumentService.class);

	/**
	 * Context.
	 */
	@Resource
	private WebServiceContext context;

	/**
	 * factory.
	 */
	private ObjectFactory of;

	/**
	 * Servizio.
	 */
	private IAooFacadeSRV aooSRV;

	/**
	 * Servizio.
	 */
	private ICreaDocumentoUscitaWsFacadeSRV creaDocumentoUscitaWSSRV;

	/**
	 * Servizio.
	 */
	private ICreaDocumentoEntrataWsFacadeSRV creaDocumentoEntrataWSSRV;

	/**
	 * Servizio.
	 */
	private IRicercaWsFacadeSRV ricercaWSSRV;

	/**
	 * Servizio.
	 */
	private IOrganigrammaWsFacadeSRV organigrammaWSSRV;

	/**
	 * Servizio.
	 */
	private IFascicoloWsFacadeSRV fascicoloWSSRV;

	/**
	 * Servizio.
	 */
	private IRedStampigliaturaDocWsFacadeSRV redStampigliaturaDocWSSRV;

	/**
	 * Servizio.
	 */
	private IStampigliaturaDocWsFacadeSRV stampigliaturaDocWSSRV;

	/**
	 * Servizio.
	 */
	private IFaldoneWsFacadeSRV faldoneWSSRV;

	/**
	 * Servizio.
	 */
	private IDocumentoWsFacadeSRV documentoWSSRV;

	/**
	 * Servizio.
	 */
	private ICheckDocumentWsFacadeSRV checkWSSRV;

	/**
	 * Servizio.
	 */
	private IWorkFlowWsFacadeSRV workFlowWSSRV;

	/**
	 * Servizio.
	 */
	private IContattoWsFacadeSRV contattoWSSRV;

	/**
	 * Costruttore, inizializza tutti i service che usa la classe.
	 */
	public DocumentService() {
		try {
			of = new ObjectFactory();
			aooSRV = ApplicationContextProvider.getApplicationContext().getBean(IAooSRV.class);
			creaDocumentoUscitaWSSRV = ApplicationContextProvider.getApplicationContext().getBean(ICreaDocumentoUscitaWsFacadeSRV.class);
			creaDocumentoEntrataWSSRV = ApplicationContextProvider.getApplicationContext().getBean(ICreaDocumentoEntrataWsFacadeSRV.class);
			ricercaWSSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaWsFacadeSRV.class);
			organigrammaWSSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaWsFacadeSRV.class);
			fascicoloWSSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloWsFacadeSRV.class);
			redStampigliaturaDocWSSRV = ApplicationContextProvider.getApplicationContext().getBean(IRedStampigliaturaDocWsFacadeSRV.class);
			stampigliaturaDocWSSRV = ApplicationContextProvider.getApplicationContext().getBean(IStampigliaturaDocWsFacadeSRV.class);
			faldoneWSSRV = ApplicationContextProvider.getApplicationContext().getBean(IFaldoneWsFacadeSRV.class);
			documentoWSSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoWsFacadeSRV.class);
			checkWSSRV = ApplicationContextProvider.getApplicationContext().getBean(ICheckDocumentWsFacadeSRV.class);
			workFlowWSSRV = ApplicationContextProvider.getApplicationContext().getBean(IWorkFlowWsFacadeSRV.class);
			contattoWSSRV = ApplicationContextProvider.getApplicationContext().getBean(IContattoWsFacadeSRV.class);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di costruzione del servizio", e);
		}
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redCreazioneDocumentoUscita
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedCreazioneDocumentoUscitaType)
	 * @param request
	 * @return RedCreazioneDocumentoUscitaResponseType response
	 */
	@Override
	public RedCreazioneDocumentoUscitaResponseType redCreazioneDocumentoUscita(final RedCreazioneDocumentoUscitaType request) {
		LOGGER.info("[DocumentService][redCreazioneDocumentoUscita][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedCreazioneDocumentoUscitaResponseType response = of.createRedCreazioneDocumentoUscitaResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedCreazioneDocumentoUscita(request);

		if (erroreValidazione == null) {
			try {

				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null,
						request.getIdUfficioCreatore().longValue(), null);

				// Creazione del documento in uscita
				final EsitoSalvaDocumentoDTO esito = creaDocumentoUscitaWSSRV.creaDocumentoUscitaRed(client, request);

				response.setEsito(createEsitoTypeOk());
				response.setNumeroDocumento(esito.getNumeroDocumento());

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redCreazioneDocumentoUscita] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redCreazioneDocumentoEntrata
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedCreazioneDocumentoEntrataType)
	 * @param request
	 * @return RedCreazioneDocumentoEntrataResponseType response
	 */
	@Override
	public RedCreazioneDocumentoEntrataResponseType redCreazioneDocumentoEntrata(final RedCreazioneDocumentoEntrataType request) {
		LOGGER.info("[DocumentService][redCreazioneDocumentEntrata][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedCreazioneDocumentoEntrataResponseType response = of.createRedCreazioneDocumentoEntrataResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedCreazioneDocumentoEntrata(request);

		if (erroreValidazione == null) {
			try {

				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null,
						request.getIdUfficioCreatore().longValue(), null);

				// Creazione del documento in entrata
				final EsitoSalvaDocumentoDTO esito = creaDocumentoEntrataWSSRV.creaDocumentoEntrataRed(client, request);

				response.setEsito(createEsitoTypeOk());
				response.setNumeroDocumento(esito.getNumeroDocumento());

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redCreazioneDocumentEntrata] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redRicercaDocumenti
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedRicercaDocumentiType)
	 * @param request
	 * @return RedRicercaDocumentiResponseType response
	 */
	@Override
	public RedRicercaDocumentiResponseType redRicercaDocumenti(final RedRicercaDocumentiType request) {
		LOGGER.info("[DocumentService][redRicercaDocumenti][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedRicercaDocumentiResponseType response = of.createRedRicercaDocumentiResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedRicercaDocumenti(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null,
						request.getIdUtente().longValue());

				// Ricerca dei documenti
				final RisultatoRicercaWsDTO risultatoRicerca = ricercaWSSRV.redRicercaDocumenti(client, request);

				if (risultatoRicerca != null) {
					final List<DocumentoWsDTO> documentiRisultatoRicerca = risultatoRicerca.getDocumenti();

					if (!CollectionUtils.isEmpty(documentiRisultatoRicerca)) {
						for (final DocumentoWsDTO documentoRisultatoRicerca : documentiRisultatoRicerca) {
							response.getDocumenti().add(getDocumentoWs(documentoRisultatoRicerca));
						}
					}
				}
				response.setEsito(createEsitoTypeOk());

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redRicercaDocumenti] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redRicercaFascicoli
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedRicercaFascicoliType)
	 * @param request
	 * @return RedRicercaFascicoliResponseType response
	 */
	@Override
	public RedRicercaFascicoliResponseType redRicercaFascicoli(final RedRicercaFascicoliType request) {
		LOGGER.info("[DocumentService][redRicercaFascicoli][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedRicercaFascicoliResponseType response = of.createRedRicercaFascicoliResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedRicercaFascicoli(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null,
						request.getIdUtente().longValue());

				// Ricerca dei faldoni
				final RisultatoRicercaWsDTO risultatoRicerca = ricercaWSSRV.redRicercaFascicoli(client, request);

				if (risultatoRicerca != null) {
					final List<FascicoloWsDTO> fascicoliRisultatoRicerca = risultatoRicerca.getFascicoli();

					if (!CollectionUtils.isEmpty(fascicoliRisultatoRicerca)) {
						for (final FascicoloWsDTO fascicoloRisultatoRicerca : fascicoliRisultatoRicerca) {
							response.getFascicoli().add(getFascicoloWs(fascicoloRisultatoRicerca));
						}
					}
				}
				response.setEsito(createEsitoTypeOk());

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redRicercaFascicoli] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redRicercaFaldoni
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedRicercaFaldoniType)
	 * @param request
	 * @return RedRicercaFaldoniResponseType response
	 */
	@Override
	public RedRicercaFaldoniResponseType redRicercaFaldoni(final RedRicercaFaldoniType request) {
		LOGGER.info("[DocumentService][redRicercaFaldoni][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedRicercaFaldoniResponseType response = of.createRedRicercaFaldoniResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedRicercaFaldoni(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null,
						request.getIdUtente().longValue());

				// Ricerca dei faldoni
				final RisultatoRicercaWsDTO risultatoRicerca = ricercaWSSRV.redRicercaFaldoni(client, request);

				if (risultatoRicerca != null) {
					final List<FaldoneWsDTO> faldoniRisultatoRicerca = risultatoRicerca.getFaldoni();

					if (!CollectionUtils.isEmpty(faldoniRisultatoRicerca)) {
						for (final FaldoneWsDTO faldoneRisultatoRicerca : faldoniRisultatoRicerca) {
							response.getFaldoni().add(getFaldoneWs(faldoneRisultatoRicerca));
						}
					}
				}
				response.setEsito(createEsitoTypeOk());

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redRicercaFaldoni] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redGetOrganigramma
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedGetOrganigrammaType)
	 * @param request
	 * @return RedGetOrganigrammaResponseType response
	 */
	@Override
	public RedGetOrganigrammaResponseType redGetOrganigramma(final RedGetOrganigrammaType request) {
		LOGGER.info("[DocumentService][redGetOrganigramma][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedGetOrganigrammaResponseType response = of.createRedGetOrganigrammaResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedGetOrganigramma(request);

		if (erroreValidazione == null) {
			try {
				final Aoo aoo = getAoo(request.getIdAoo(), request.getCodiceAoo());

				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), aoo.getIdAoo(), null, null);

				// Creazione del documento in uscita
				final Collection<Ufficio> uffici = organigrammaWSSRV.getOrganigramma(client, request);

				response.setEsito(createEsitoTypeOk());
				response.getUffici().addAll(uffici);

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redGetOrganigramma] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redFascicolazioneDocumento
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedFascicolazioneDocumentoType)
	 * @param request
	 * @return RedFascicolazioneDocumentoResponseType response
	 */
	@Override
	public RedFascicolazioneDocumentoResponseType redFascicolazioneDocumento(final RedFascicolazioneDocumentoType request) {
		LOGGER.info("[DocumentService][redFascicolazioneDocumento][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedFascicolazioneDocumentoResponseType response = of.createRedFascicolazioneDocumentoResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedFascicolazioneDocumento(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null,
						request.getIdUtente().longValue());

				// Fascicolazione documento
				fascicoloWSSRV.redFascicolazioneDocumento(client, request);

				response.setEsito(createEsitoTypeOk());

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redFascicolazioneDocumento] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redEliminaFascicolo
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedEliminaFascicoloType)
	 * @param request
	 * @return RedEliminaFascicoloResponseType response
	 */
	@Override
	public RedEliminaFascicoloResponseType redEliminaFascicolo(final RedEliminaFascicoloType request) {
		LOGGER.info("[DocumentService][redEliminaFascicolo][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedEliminaFascicoloResponseType response = of.createRedEliminaFascicoloResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedEliminaFascicolo(request);

		if (erroreValidazione == null) {
			try {
				final Aoo aoo = getAoo(request.getIdAoo(), request.getCodiceAoo());

				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), aoo.getIdAoo().longValue(),
						null, null);

				fascicoloWSSRV.redEliminaFascicolo(client, request);

				response.setEsito(createEsitoTypeOk());

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redEliminaFascicolo] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redCambiaStatoFascicolo
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedCambiaStatoFascicoloType)
	 * @param request
	 * @return RedCambiaStatoFascicoloResponseType response
	 */
	@Override
	public RedCambiaStatoFascicoloResponseType redCambiaStatoFascicolo(final RedCambiaStatoFascicoloType request) {
		LOGGER.info("[DocumentService][redCambiaStatoFascicolo][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedCambiaStatoFascicoloResponseType response = of.createRedCambiaStatoFascicoloResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedCambiaStatoFascicolo(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null,
						request.getIdnodo().longValue(), null);

				fascicoloWSSRV.redCambiaStatoFascicolo(client, request);

				response.setEsito(createEsitoTypeOk());

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redCambiaStatoFascicolo] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redModificaFascicolo
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedModificaFascicoloType)
	 * @param request
	 * @return RedModificaFascicoloResponseType response
	 */
	@Override
	public RedModificaFascicoloResponseType redModificaFascicolo(final RedModificaFascicoloType request) {
		LOGGER.info("[DocumentService][redModificaFascicolo][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedModificaFascicoloResponseType response = of.createRedModificaFascicoloResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedModificaFascicolo(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null,
						request.getIdnodo().longValue(), null);

				fascicoloWSSRV.redModificaFascicolo(client, request);

				response.setEsito(createEsitoTypeOk());

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redModificaFascicolo] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redAssociaFascicoloATitolario
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedAssociaFascicoloATitolarioType)
	 * @param request
	 * @return RedAssociaFascicoloATitolarioResponseType response
	 */
	@Override
	public RedAssociaFascicoloATitolarioResponseType redAssociaFascicoloATitolario(final RedAssociaFascicoloATitolarioType request) {
		LOGGER.info("[DocumentService][redAssociaFascicoloATitolario][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedAssociaFascicoloATitolarioResponseType response = of.createRedAssociaFascicoloATitolarioResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedAssociaFascicoloATitolario(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null,
						request.getIdnodo().longValue(), null);

				fascicoloWSSRV.redAssociaFascicoloATitolario(client, request);

				response.setEsito(createEsitoTypeOk());

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redAssociaFascicoloATitolario] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redCreaFascicolo (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedCreaFascicoloType)
	 * @param request
	 * @return RedCreaFascicoloResponseType response
	 */
	@Override
	public RedCreaFascicoloResponseType redCreaFascicolo(final RedCreaFascicoloType request) {
		LOGGER.info("[DocumentService][redCreaFascicolo][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedCreaFascicoloResponseType response = of.createRedCreaFascicoloResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedCreaFascicolo(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null,
						request.getIdnodo().longValue(), null);

				final String idFascicolo = fascicoloWSSRV.redCreaFascicolo(client, request);

				response.setNomeFascicolo(idFascicolo);
				response.setEsito(createEsitoTypeOk());

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redCreaFascicolo] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redInserisciProtEntrataCert
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedInserisciProtEntrataCertType)
	 * @param request
	 * @return RedInserisciProtEntrataResponseType response
	 */
	@Override
	public RedInserisciProtEntrataResponseType redInserisciProtEntrata(final RedInserisciProtEntrataType request) {
		LOGGER.info("[DocumentService][redInserisciProtEntrata][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedInserisciProtEntrataResponseType response = of.createRedInserisciProtEntrataResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedInserisciProtEntrata(request);

		if (erroreValidazione == null) {
			try {
				final Aoo aoo = getAoo(request.getIdAoo(), request.getCodiceAoo());

				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), aoo.getIdAoo(), null, null);

				final DocumentoWsDTO documentoStampigliato = redStampigliaturaDocWSSRV.redStampigliaProtocolloEntrata(client, request);

				gestisciEsitoOkContentDocRed(response, documentoStampigliato);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redInserisciProtEntrata] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redInserisciProtUscitaCert
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedInserisciProtUscitaCertType)
	 * @param request
	 * @return RedInserisciProtUscitaResponseType response
	 */
	@Override
	public RedInserisciProtUscitaResponseType redInserisciProtUscita(final RedInserisciProtUscitaType request) {
		LOGGER.info("[DocumentService][redInserisciProtUscita][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedInserisciProtUscitaResponseType response = of.createRedInserisciProtUscitaResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedInserisciProtUscita(request);

		if (erroreValidazione == null) {
			try {
				final Aoo aoo = getAoo(request.getIdAoo(), request.getCodiceAoo());

				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), aoo.getIdAoo(), null, null);

				final DocumentoWsDTO documentoStampigliato = redStampigliaturaDocWSSRV.redStampigliaProtocolloUscita(client, request);

				gestisciEsitoOkContentDocRed(response, documentoStampigliato);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redInserisciProtUscita] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redInserisciCampoFirma
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedInserisciCampoFirmaType)
	 * @param request
	 * @return RedInserisciCampoFirmaResponseType response
	 */
	@Override
	public RedInserisciCampoFirmaResponseType redInserisciCampoFirma(final RedInserisciCampoFirmaType request) {
		LOGGER.info("[DocumentService][redInserisciCampoFirma][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedInserisciCampoFirmaResponseType response = of.createRedInserisciCampoFirmaResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedInserisciCampoFirma(request);

		if (erroreValidazione == null) {
			try {
				final Aoo aoo = getAoo(request.getIdAoo(), request.getCodiceAoo());

				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), aoo.getIdAoo(), null, null);

				final DocumentoWsDTO documentoStampigliato = redStampigliaturaDocWSSRV.redInserisciCampoFirma(client, request);

				gestisciEsitoOkContentDocRed(response, documentoStampigliato);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redInserisciCampoFirma] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redInserisciPostilla
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedInserisciPostillaType)
	 * @param request
	 * @return RedInserisciPostillaResponseType response
	 */
	@Override
	public RedInserisciPostillaResponseType redInserisciPostilla(final RedInserisciPostillaType request) {
		LOGGER.info("[DocumentService][redInserisciPostilla][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedInserisciPostillaResponseType response = of.createRedInserisciPostillaResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedInserisciPostilla(request);

		if (erroreValidazione == null) {
			try {
				final Aoo aoo = getAoo(request.getIdAoo(), request.getCodiceAoo());

				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), aoo.getIdAoo(), null, null);

				final DocumentoWsDTO documentoStampigliato = redStampigliaturaDocWSSRV.redStampigliaPostilla(client, request);

				gestisciEsitoOkContentDocRed(response, documentoStampigliato);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redInserisciPostilla] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redInserisciId (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedInserisciIdType)
	 * @param request
	 * @return RedInserisciIdResponseType response
	 */
	@Override
	public RedInserisciIdResponseType redInserisciId(final RedInserisciIdType request) {
		LOGGER.info("[DocumentService][redInserisciId][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedInserisciIdResponseType response = of.createRedInserisciIdResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedInserisciId(request);

		if (erroreValidazione == null) {
			try {
				final Aoo aoo = getAoo(request.getIdAoo(), request.getCodiceAoo());

				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), aoo.getIdAoo(), null, null);

				final DocumentoWsDTO documentoStampigliato = redStampigliaturaDocWSSRV.redStampigliaIdDocumento(client, request);

				gestisciEsitoOkContentDocRed(response, documentoStampigliato);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redInserisciId] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redInserisciWatermark
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedInserisciWatermarkType)
	 * @param request
	 * @return RedInserisciWatermarkResponseType response
	 */
	@Override
	public RedInserisciWatermarkResponseType redInserisciWatermark(final RedInserisciWatermarkType request) {
		LOGGER.info("[DocumentService][redInserisciWatermark][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedInserisciWatermarkResponseType response = of.createRedInserisciWatermarkResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedInserisciWatermark(request);

		if (erroreValidazione == null) {
			try {
				final Aoo aoo = getAoo(request.getIdAoo(), request.getCodiceAoo());

				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), aoo.getIdAoo(), null, null);

				final DocumentoWsDTO documentoStampigliato = redStampigliaturaDocWSSRV.redStampigliaWatermark(client, request);

				gestisciEsitoOkContentDocRed(response, documentoStampigliato);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redInserisciWatermark] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redInserisciApprovazione
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedInserisciApprovazioneType)
	 * @param request
	 * @return RedInserisciApprovazioneResponseType response
	 */
	@Override
	public RedInserisciApprovazioneResponseType redInserisciApprovazione(final RedInserisciApprovazioneType request) {
		LOGGER.info("[DocumentService][redInserisciApprovazione][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedInserisciApprovazioneResponseType response = of.createRedInserisciApprovazioneResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedInserisciApprovazione(request);

		if (erroreValidazione == null) {
			try {
				final Aoo aoo = getAoo(request.getIdAoo(), request.getCodiceAoo());

				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), aoo.getIdAoo(), null, null);

				final DocumentoWsDTO documentoStampigliato = redStampigliaturaDocWSSRV.redStampigliaApprovazione(client, request);

				gestisciEsitoOkContentDocRed(response, documentoStampigliato);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redInserisciApprovazione] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redEliminaFaldone
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedEliminaFaldoneType)
	 * @param request
	 * @return RedEliminaFaldoneResponseType response
	 */
	@Override
	public RedEliminaFaldoneResponseType redEliminaFaldone(final RedEliminaFaldoneType request) {
		LOGGER.info("[DocumentService][redEliminaFaldone][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedEliminaFaldoneResponseType response = of.createRedEliminaFaldoneResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedEliminaFaldone(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null,
						request.getIdNodo().longValue(), null);

				faldoneWSSRV.redEliminaFaldone(client, request);

				response.setEsito(createEsitoTypeOk());

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redEliminaFaldone] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redCreaFaldone (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedCreaFaldoneType)
	 * @param request
	 * @return RedCreaFaldoneResponseType response
	 */
	@Override
	public RedCreaFaldoneResponseType redCreaFaldone(final RedCreaFaldoneType request) {
		LOGGER.info("[DocumentService][redCreaFaldone][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedCreaFaldoneResponseType response = of.createRedCreaFaldoneResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedCreaFaldone(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null,
						request.getIdnodo().longValue(), null);

				final String nomeFaldone = faldoneWSSRV.redCreaFaldone(client, request);

				response.setEsito(createEsitoTypeOk());
				response.setNomeFaldone(nomeFaldone);

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redCreaFaldone] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redAssociaFascicoloFaldone
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedAssociaFascicoloFaldoneType)
	 * @param request
	 * @return RedAssociaFascicoloFaldoneResponseType response
	 */
	@Override
	public RedAssociaFascicoloFaldoneResponseType redAssociaFascicoloFaldone(final RedAssociaFascicoloFaldoneType request) {
		LOGGER.info("[DocumentService][redAssociaFascicoloFaldone][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedAssociaFascicoloFaldoneResponseType response = of.createRedAssociaFascicoloFaldoneResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedAssociaFascicoloFaldone(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null,
						request.getIdnodo().longValue(), null);

				faldoneWSSRV.redAssociaFascicoloFaldone(client, request);

				response.setEsito(createEsitoTypeOk());

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redAssociaFascicoloFaldone] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redRimuoviAssociazioneFascicoloFaldone
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedRimuoviAssociazioneFascicoloFaldoneType)
	 * @param request
	 * @return RedRimuoviAssociazioneFascicoloFaldoneResponseType response
	 */
	@Override
	public RedRimuoviAssociazioneFascicoloFaldoneResponseType redRimuoviAssociazioneFascicoloFaldone(final RedRimuoviAssociazioneFascicoloFaldoneType request) {
		LOGGER.info("[DocumentService][redRimuoviAssociazioneFascicoloFaldone][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedRimuoviAssociazioneFascicoloFaldoneResponseType response = of.createRedRimuoviAssociazioneFascicoloFaldoneResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedRimuoviAssociazioneFascicoloFaldone(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null,
						request.getIdUtente().longValue());

				faldoneWSSRV.redRimuoviAssociazioneFascicoloFaldone(client, request);

				response.setEsito(createEsitoTypeOk());

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redRimuoviAssociazioneFascicoloFaldone] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redSpostaFascicoloFaldone
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedSpostaFascicoloFaldoneType)
	 * @param request
	 * @return RedSpostaFascicoloFaldoneResponseType response
	 */
	@Override
	public RedSpostaFascicoloFaldoneResponseType redSpostaFascicoloFaldone(final RedSpostaFascicoloFaldoneType request) {
		LOGGER.info("[DocumentService][redSpostaFascicoloFaldone][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedSpostaFascicoloFaldoneResponseType response = of.createRedSpostaFascicoloFaldoneResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedSpostaFascicoloFaldone(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null,
						request.getIdUtente().longValue());

				faldoneWSSRV.redSpostaFascicoloFaldone(client, request);

				response.setEsito(createEsitoTypeOk());

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redSpostaFascicoloFaldone] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redElencoVersioniDocumento
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedElencoVersioniDocumentoType)
	 * @param request
	 * @return RedElencoVersioniDocumentoResponseType response
	 */
	@Override
	public RedElencoVersioniDocumentoResponseType redElencoVersioniDocumento(final RedElencoVersioniDocumentoType request) {
		LOGGER.info("[DocumentService][redElencoVersioniDocumento][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedElencoVersioniDocumentoResponseType response = of.createRedElencoVersioniDocumentoResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedElencoVersioniDocumento(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null,
						request.getIdNodo().longValue(), request.getIdUtente().longValue());

				final List<DocumentoWsDTO> versioniDocumentoList = documentoWSSRV.getElencoVersioniDocumento(client, request);

				if (!CollectionUtils.isEmpty(versioniDocumentoList)) {
					for (final DocumentoWsDTO versioneDocumento : versioniDocumentoList) {
						response.getVersioniDocumento().add(getDocumentoWs(versioneDocumento));
					}
				}
				response.setEsito(createEsitoTypeOk());

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redElencoVersioniDocumento] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redGetVersioneDocumento
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedGetVersioneDocumentoType)
	 * @param request
	 * @return RedGetVersioneDocumentoResponseType response
	 */
	@Override
	public RedGetVersioneDocumentoResponseType redGetVersioneDocumento(final RedGetVersioneDocumentoType request) {
		LOGGER.info("[DocumentService][redGetVersioneDocumento][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedGetVersioneDocumentoResponseType response = of.createRedGetVersioneDocumentoResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedGetVersioneDocumento(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null,
						request.getIdNodo().longValue(), request.getIdUtente().longValue());

				final DocumentoWsDTO versioneDocumento = documentoWSSRV.getVersioneDocumento(client, request);

				response.setDocumento(getDocumentoWs(versioneDocumento));
				response.setEsito(createEsitoTypeOk());

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redGetVersioneDocumento] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see
	 * it.ibm.red.webservice.services.documentservice.IDocumentService#redCheckIn
	 * (it.ibm.red.webservice.model.documentservice.types.messages.RedCheckInType)
	 * @param request
	 * @return RedCheckOutResponseType response
	 */
	@Override
	public RedCheckOutResponseType redCheckOut(final RedCheckOutType request) {
		LOGGER.info("[DocumentService][redCheckOut][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedCheckOutResponseType response = of.createRedCheckOutResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedCheckOut(request);

		if (erroreValidazione == null) {

			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null,
						request.getIdUtente().longValue());

				checkWSSRV.checkOut(client, request);

				response.setEsito(createEsitoTypeOk());
				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redCheckOut] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}

		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see
	 * it.ibm.red.webservice.services.documentservice.IDocumentService#redCheckOut
	 * (it.ibm.red.webservice.model.documentservice.types.messages.RedCheckOutType)
	 * @param request
	 * @return RedUndoCheckOutResponseType response
	 */
	@Override
	public RedUndoCheckOutResponseType redUndoCheckOut(final RedUndoCheckOutType request) {
		LOGGER.info("[DocumentService][redUndoCheckOut][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedUndoCheckOutResponseType response = of.createRedUndoCheckOutResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedUndoCheckOut(request);

		if (erroreValidazione == null) {

			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null,
						request.getIdUtente().longValue());

				checkWSSRV.undoCheckout(client, request);

				response.setEsito(createEsitoTypeOk());
				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redUndoCheckOut] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}

		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see
	 * it.ibm.red.webservice.services.documentservice.IDocumentService#redCheckIn
	 * (it.ibm.red.webservice.model.documentservice.types.messages.RedCheckInType)
	 * @param request
	 * @return RedCheckInResponseType response
	 */
	@Override
	public RedCheckInResponseType redCheckIn(final RedCheckInType request) {
		LOGGER.info("[DocumentService][redCheckIn][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedCheckInResponseType response = of.createRedCheckInResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedCheckIn(request);

		if (erroreValidazione == null) {

			final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null,
					request.getIdUtente().longValue());

			checkWSSRV.checkIn(client, request);

			response.setEsito(createEsitoTypeOk());
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redGetVersioneDocumento
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedGetVersioneDocumentoType)
	 * @param request
	 * @return RedRicercaAvanzataResponseType response
	 */
	@Override
	public RedRicercaAvanzataResponseType redRicercaAvanzata(final RedRicercaAvanzataType request) {
		LOGGER.info("[DocumentService][redRicercaAvanzata][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedRicercaAvanzataResponseType response = of.createRedRicercaAvanzataResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedRicercaAvanzata(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null,
						request.getIdUtente().longValue());

				final RisultatoRicercaWsDTO risultatoRicerca = ricercaWSSRV.redRicercaAvanzata(client, request);

				if (risultatoRicerca != null) {
					final List<DocumentoWsDTO> documentiRisultatoRicerca = risultatoRicerca.getDocumenti();

					if (!CollectionUtils.isEmpty(documentiRisultatoRicerca)) {
						for (final DocumentoWsDTO documentoRisultatoRicerca : documentiRisultatoRicerca) {
							response.getDocumenti().add(getDocumentoWs(documentoRisultatoRicerca));
						}
					}

					final List<FascicoloWsDTO> fascicoliRisultatoRicerca = risultatoRicerca.getFascicoli();

					if (!CollectionUtils.isEmpty(fascicoliRisultatoRicerca)) {
						for (final FascicoloWsDTO fascicoloRisultatoRicerca : fascicoliRisultatoRicerca) {
							response.getFascicoli().add(getFascicoloWs(fascicoloRisultatoRicerca));
						}
					}

					response.setNumeroElementi(risultatoRicerca.getNumeroElementi());
				}
				response.setEsito(createEsitoTypeOk());

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redRicercaAvanzata] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redAvanzaWorkflow
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedAvanzaWorkflowType)
	 * @param request
	 * @return RedAvanzaWorkflowResponseType response
	 */
	@Override
	public RedAvanzaWorkflowResponseType redAvanzaWorkflow(final RedAvanzaWorkflowType request) {
		LOGGER.info("[DocumentService][redAvanzaWorkflow][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedAvanzaWorkflowResponseType response = of.createRedAvanzaWorkflowResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedAvanzaWorkflow(request);

		if (erroreValidazione == null) {

			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null,
						request.getIdUtente().longValue());

				final Workflow workflow = workFlowWSSRV.avanzaWorkFlow(client, request);

				response.setEsito(createEsitoTypeOk());
				response.setWorkflow(workflow);
				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redAvanzaWorkflow] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}

		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * Avvia instanza workflow.
	 * @param request
	 * @return RedAvviaInstanzaWorkflowResponseType
	 */
	@Override
	public RedAvviaIstanzaWorkflowResponseType redAvviaIstanzaWorkflow(final RedAvviaIstanzaWorkflowType request) {
		LOGGER.info("[DocumentService][redAvanzaWorkflow][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedAvviaIstanzaWorkflowResponseType response = of.createRedAvviaIstanzaWorkflowResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedAvviaIstanzaWorkflow(request);

		if (erroreValidazione == null) {

			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null,
						request.getIdUtente().longValue());

				final Workflow workflow = workFlowWSSRV.avviaIstanzaWorkFlow(client, request);

				response.setEsito(createEsitoTypeOk());
				response.setWorkflow(workflow);
				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redAvanzaWorkflow] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}

		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redTrasformazionePDF
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedTrasformazionePDFType)
	 * @param request
	 * @return RedTrasformazionePDFResponseType response
	 */
	@Override
	public RedTrasformazionePDFResponseType redTrasformazionePDF(final RedTrasformazionePDFType request) {
		LOGGER.info("[DocumentService][redTrasformazionePDF][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedTrasformazionePDFResponseType response = of.createRedTrasformazionePDFResponseType();
		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedTrasformazionePdf(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null,
						request.getIdUtente().longValue());

				final DocumentoWsDTO docRedWs = documentoWSSRV.redTrasformazionePDF(client, request);

				gestisciEsitoOkContentDocRed(response, docRedWs);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redTrasformazionePDF] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * trasformazionePDF
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * TrasformazionePDFType)
	 * @param request
	 * @return TrasformazionePDFResponseType response
	 */
	@Override
	public TrasformazionePDFResponseType trasformazionePDF(final TrasformazionePDFType request) {
		LOGGER.info("[DocumentService][trasformazionePDF][" + request.getCredenzialiWS().getIdClient() + "]");
		final TrasformazionePDFResponseType response = of.createTrasformazionePDFResponseType();
		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaTrasformazionePdf(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null, null);

				final FileDTO docRedWs = documentoWSSRV.trasformazionePDF(client, request);

				gestisciEsitoOkContent(response, docRedWs);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][trasformazionePDF] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * inserisciProtEntrata
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * InserisciProtEntrataType)
	 * @param request
	 * @return InserisciProtEntrataResponseType response
	 */
	@Override
	public InserisciProtEntrataResponseType inserisciProtEntrata(final InserisciProtEntrataType request) {
		LOGGER.info("[DocumentService][inserisciProtEntrata][" + request.getCredenzialiWS().getIdClient() + "]");
		final InserisciProtEntrataResponseType response = of.createInserisciProtEntrataResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaInserisciProtEntrata(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null, null);

				final FileDTO contentConProtocollo = stampigliaturaDocWSSRV.stampigliaProtocolloEntrata(client, request);

				gestisciEsitoOkContent(response, contentConProtocollo);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][inserisciProtEntrata] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * inserisciProtUscita
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * InserisciProtUscitaType)
	 * @param request
	 * @return InserisciProtUscitaResponseType response
	 */
	@Override
	public InserisciProtUscitaResponseType inserisciProtUscita(final InserisciProtUscitaType request) {
		LOGGER.info("[DocumentService][inserisciProtUscita][" + request.getCredenzialiWS().getIdClient() + "]");
		final InserisciProtUscitaResponseType response = of.createInserisciProtUscitaResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaInserisciProtUscita(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null, null);

				final FileDTO contentConProtocollo = stampigliaturaDocWSSRV.stampigliaProtocolloUscita(client, request);

				gestisciEsitoOkContent(response, contentConProtocollo);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][inserisciProtUscita] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see
	 * it.ibm.red.webservice.services.documentservice.IDocumentService#inserisciId
	 * (it.ibm.red.webservice.model.documentservice.types.messages.InserisciIdType)
	 * @param request
	 * @return InserisciIdResponseType response
	 */
	@Override
	public InserisciIdResponseType inserisciId(final InserisciIdType request) {
		LOGGER.info("[DocumentService][inserisciId][" + request.getCredenzialiWS().getIdClient() + "]");
		final InserisciIdResponseType response = of.createInserisciIdResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaInserisciId(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null, null);

				final FileDTO contentConId = stampigliaturaDocWSSRV.stampigliaIdDocumento(client, request);

				gestisciEsitoOkContent(response, contentConId);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][inserisciId] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * inserisciCampoFirma
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * InserisciCampoFirmaType)
	 * @param request
	 * @return InserisciCampoFirmaResponseType response
	 */
	@Override
	public InserisciCampoFirmaResponseType inserisciCampoFirma(final InserisciCampoFirmaType request) {
		LOGGER.info("[DocumentService][inserisciCampoFirma][" + request.getCredenzialiWS().getIdClient() + "]");
		final InserisciCampoFirmaResponseType response = of.createInserisciCampoFirmaResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaInserisciCampoFirma(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null, null);

				final FileDTO contentConCampoFirma = stampigliaturaDocWSSRV.inserisciCampoFirma(client, request);

				gestisciEsitoOkContent(response, contentConCampoFirma);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][inserisciCampoFirma] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * Inserisce la postilla.
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * inserisciPostilla
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * InserisciPostillaType)
	 * @param request
	 * @return InserisciPostillaResponseType response
	 */
	@Override
	public InserisciPostillaResponseType inserisciPostilla(final InserisciPostillaType request) {
		LOGGER.info("[DocumentService][inserisciPostilla][" + request.getCredenzialiWS().getIdClient() + "]");
		final InserisciPostillaResponseType response = of.createInserisciPostillaResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaInserisciPostilla(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null, null);

				final FileDTO contentConPostilla = stampigliaturaDocWSSRV.stampigliaPostilla(client, request);

				gestisciEsitoOkContent(response, contentConPostilla);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][inserisciPostilla] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * Inserisce il watermark.
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * inserisciWatermark
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * InserisciWatermarkType)
	 * @param request
	 * @return InserisciWatermarkResponseType response
	 */
	@Override
	public InserisciWatermarkResponseType inserisciWatermark(final InserisciWatermarkType request) {
		LOGGER.info("[DocumentService][inserisciWatermark][" + request.getCredenzialiWS().getIdClient() + "]");
		final InserisciWatermarkResponseType response = of.createInserisciWatermarkResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaInserisciWatermark(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null, null);

				final FileDTO contentConWatermark = stampigliaturaDocWSSRV.stampigliaWatermark(client, request);

				gestisciEsitoOkContent(response, contentConWatermark);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][inserisciWatermark] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * Inserisce l'approvazione.
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * inserisciApprovazione
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * InserisciApprovazioneType)
	 * @param request
	 * @return InserisciApprovazioneResponseType response
	 */
	@Override
	public InserisciApprovazioneResponseType inserisciApprovazione(final InserisciApprovazioneType request) {
		LOGGER.info("[DocumentService][inserisciApprovazione][" + request.getCredenzialiWS().getIdClient() + "]");
		final InserisciApprovazioneResponseType response = of.createInserisciApprovazioneResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaInserisciApprovazione(request);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null, null);

				final FileDTO contentConApprovazione = stampigliaturaDocWSSRV.stampigliaApprovazione(client, request);

				gestisciEsitoOkContent(response, contentConApprovazione);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][inserisciApprovazione] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * Recupera l'elenco delle properties.
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redElencoProperties
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedElencoPropertiesType)
	 * @param request
	 * @return RedElencoPropertiesResponseType response
	 */
	@Override
	public RedElencoPropertiesResponseType redElencoProperties(final RedElencoPropertiesType request) {
		LOGGER.info("[DocumentService][redElencoProperties][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedElencoPropertiesResponseType response = of.createRedElencoPropertiesResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedElencoProperties(request);

		if (erroreValidazione == null) {

			try {
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null,
						request.getIdUtente().longValue());

				final List<PropertyTemplateDTO> properties = documentoWSSRV.getElencoProperies(client, request);

				response.setEsito(createEsitoTypeOk());
				response.getProperty().addAll(getPropertiesWs(properties));
				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redElencoProperties] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}

		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * Modifica i metadati.
	 * @see it.ibm.red.webservice.services.documentservice.IDocumentService#
	 * redModificaMetadati
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedModificaMetadatiType)
	 * @param request
	 * @return RedModificaMetadatiResponseType response
	 */
	@Override
	public RedModificaMetadatiResponseType redModificaMetadati(final RedModificaMetadatiType request) {
		LOGGER.info("[DocumentService][redModificaMetadati][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedModificaMetadatiResponseType response = of.createRedModificaMetadatiResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedModificaMetadati(request);

		if (erroreValidazione == null) {

			try {

				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), null, null,
						request.getIdUtente().longValue());

				documentoWSSRV.modificaMetadati(client, request);

				response.setEsito(createEsitoTypeOk());
				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redModificaMetadati] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}

		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * Conserva il documento.
	 * @param request
	 * @return RedConservaDocumentoResponseType response
	 */
	@Override
	public RedConservaDocumentoResponseType redConservaDocumento(final RedConservaDocumentoType request) {
		LOGGER.info("[DocumentService][redConservaDocumento][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedConservaDocumentoResponseType response = of.createRedConservaDocumentoResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedConservaDocumento(request);

		if (erroreValidazione == null) {

			try {

				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), request.getIdAoo().longValue(),
						null, request.getIdUtente().longValue());

				final ConservazioneWsDTO conserve = documentoWSSRV.conservaDocumento(client, request);

				response.setIdApplicativo(conserve.getIdApplicativo());
				response.setNomeDocumento(conserve.getNomeDocumento());
				response.setUuidDocumento(conserve.getUuidDocumento());
				response.setStato(conserve.getStato());
				response.setEsito(createEsitoTypeOk());
				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redConservaDocumento] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}

		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * Modifica security.
	 * @param request
	 * @return RedModificaSecurityResponseType response
	 */
	@Override
	public RedModificaSecurityResponseType redModificaSecurity(final RedModificaSecurityType request) {
		LOGGER.info("[DocumentService][redModificaSecurity][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedModificaSecurityResponseType response = of.createRedModificaSecurityResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedModificaSecurity(request);

		if (erroreValidazione == null) {

			try {

				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), request.getIdAoo().longValue(),
						null, request.getIdUtente().longValue());

				documentoWSSRV.modificaSecurity(client, request);

				response.setEsito(createEsitoTypeOk());
				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redModificaSecurity] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}

		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * Recupera contatti.
	 * @param request
	 * @return RedGetContattiResponseType response
	 */
	@Override
	public RedGetContattiResponseType redGetContatti(final RedGetContattiType request) {
		LOGGER.info("[DocumentService][redGetContatti][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedGetContattiResponseType response = of.createRedGetContattiResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedGetContatti(request);

		if (erroreValidazione == null) {

			try {

				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), request.getCredenzialiWS().getPwdClient(), request.getIdAoo().longValue(),
						null, null);

				final RicercaRubricaResultDTO ricercaResult = contattoWSSRV.getContatti(client, request);

				response.getContatti().addAll(getContattiWs(ricercaResult.getContatti()));
				response.setMessage(ricercaResult.getMessaggio());
				response.setEsito(createEsitoTypeOk());
				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][redGetContatti] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}

		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @param idAoo
	 * @param codiceAoo
	 * @return
	 */
	private Aoo getAoo(final Integer idAoo, final String codiceAoo) {
		Aoo aoo = null;
		if (idAoo == null || idAoo == 0) {
			aoo = aooSRV.recuperaAoo(codiceAoo);
		} else {
			aoo = aooSRV.recuperaAoo(idAoo);
		}

		if (aoo == null) {
			throw new RedException("Impossibile recuperare l'AOO di riferimento");
		}

		return aoo;
	}

	/**
	 * @param docRedWs
	 * @param request
	 * @param response
	 * @throws DatatypeConfigurationException
	 */
	private void gestisciEsitoOkContentDocRed(final BaseDocumentoVersioneResponseType response, final DocumentoWsDTO docRedWs) throws DatatypeConfigurationException {
		if (StringUtils.isNotBlank(docRedWs.getIdDocumento())) {
			response.setDocumento(getDocumentoWs(docRedWs));
		}
		response.setIdVersione(docRedWs.getNumeroVersione());
		response.setEsito(createEsitoTypeOk());

		LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
	}

	/**
	 * @param docRedWs
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	private Documento getDocumentoWs(final DocumentoWsDTO docRedWs) throws DatatypeConfigurationException {
		final it.ibm.red.webservice.model.documentservice.types.documentale.ObjectFactory ofDoc = new it.ibm.red.webservice.model.documentservice.types.documentale.ObjectFactory();

		final Documento docOut = ofDoc.createDocumento();
		docOut.setGuid(docRedWs.getGuid());
		docOut.setIdDocumento(docRedWs.getIdDocumento());
		docOut.setNumeroDocumento(docRedWs.getNumeroDocumento());
		docOut.setAnnoDocumento(docRedWs.getAnnoDocumento());
		docOut.setNumeroProtocollo(docRedWs.getNumeroProtocollo());
		docOut.setAnnoProtocollo(docRedWs.getAnnoProtocollo());
		docOut.setOggetto(docRedWs.getOggetto());
		docOut.setDataHandler(docRedWs.getContent());
		docOut.setContentType(docRedWs.getContentType());
		docOut.setNumVersione(docRedWs.getNumeroVersione());
		docOut.setFolder(docRedWs.getFolder());
		docOut.setStato(docRedWs.getStato());
		docOut.setIdFascicolo(docRedWs.getIdFascicolo());
		if (docRedWs.getDataModifica() != null) {
			docOut.setDataModifica(docRedWs.getDataModifica().toString());
		}

		// Allegati
		final List<DocumentoWsDTO> allegati = docRedWs.getAllegati();
		if (!CollectionUtils.isEmpty(allegati)) {
			for (final DocumentoWsDTO allegato : allegati) {
				docOut.getAllegati().add(getDocumentoWs(allegato));
			}
		}

		// Classe documentale
		final ClasseDocumentale classeDoc = ofDoc.createClasseDocumentale();
		setClasseDocumentale(classeDoc, docRedWs.getClasseDocumentale(), docRedWs.getMetadati(), ofDoc);
		docOut.setClasseDocumentale(classeDoc);

		// Security
		setSecurity(docRedWs.getSecurity(), docOut.getSecurity());

		return docOut;
	}

	/**
	 * @param fascicoloRedWs
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	private Fascicolo getFascicoloWs(final FascicoloWsDTO fascicoloRedWs) throws DatatypeConfigurationException {
		final it.ibm.red.webservice.model.documentservice.types.documentale.ObjectFactory ofDoc = new it.ibm.red.webservice.model.documentservice.types.documentale.ObjectFactory();

		final Fascicolo fascicoloOut = ofDoc.createFascicolo();
		fascicoloOut.setIdFascicolo(fascicoloRedWs.getIdFascicolo());
		fascicoloOut.setNomeFascicolo(fascicoloRedWs.getIdFascicolo());
		fascicoloOut.setOggetto(fascicoloRedWs.getOggetto());
		fascicoloOut.setTitolario(fascicoloRedWs.getTitolario());

		// Classe documentale
		final ClasseDocumentale classeDoc = ofDoc.createClasseDocumentale();
		setClasseDocumentale(classeDoc, fascicoloRedWs.getClasseDocumentale(), fascicoloRedWs.getMetadati(), ofDoc);
		fascicoloOut.setClasseDocumentale(classeDoc);

		// Security
		setSecurity(fascicoloRedWs.getSecurity(), fascicoloOut.getSecurity());

		return fascicoloOut;
	}

	/**
	 * @param faldoneRedWs
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	private Faldone getFaldoneWs(final FaldoneWsDTO faldoneRedWs) throws DatatypeConfigurationException {
		final it.ibm.red.webservice.model.documentservice.types.documentale.ObjectFactory ofDoc = new it.ibm.red.webservice.model.documentservice.types.documentale.ObjectFactory();

		final Faldone faldoneOut = ofDoc.createFaldone();
		faldoneOut.setNomeFaldone(faldoneRedWs.getNomeFaldone());
		faldoneOut.setOggetto(faldoneRedWs.getOggetto());
		faldoneOut.setDescrizione(faldoneRedWs.getDescrizione());
		faldoneOut.setParentFaldone(faldoneRedWs.getParentFaldone());

		// Classe documentale
		final ClasseDocumentale classeDoc = ofDoc.createClasseDocumentale();
		setClasseDocumentale(classeDoc, faldoneRedWs.getClasseDocumentale(), faldoneRedWs.getMetadati(), ofDoc);
		faldoneOut.setClasseDocumentale(classeDoc);

		// Security
		setSecurity(faldoneRedWs.getSecurity(), faldoneOut.getSecurity());

		return faldoneOut;
	}

	/**
	 * @param securityIn
	 * @param securityOut
	 * @throws DatatypeConfigurationException
	 */
	private void setSecurity(final List<SecurityWsDTO> securityIn, final List<Security> securityOut) throws DatatypeConfigurationException {
		if (!CollectionUtils.isEmpty(securityIn)) {
			final it.ibm.red.webservice.model.documentservice.types.security.ObjectFactory ofSec = new it.ibm.red.webservice.model.documentservice.types.security.ObjectFactory();

			for (final SecurityWsDTO sec : securityIn) {
				final Security security = ofSec.createSecurity();

				if (sec.getGruppo().getIdUfficio() > 0) {
					final GruppoWs gruppoSec = sec.getGruppo();

					final Gruppo gruppo = ofSec.createGruppo();
					gruppo.setIdUfficio(gruppoSec.getIdUfficio());
					gruppo.setIdSiap(gruppoSec.getIdSiap());
					gruppo.setIdUfficioPadre(gruppoSec.getIdUfficioPadre());
					gruppo.setDescrizione(gruppoSec.getDescrizione());

					security.setGruppo(gruppo);
				} else if (sec.getUtente().getIdUtente() > 0) {
					final UtenteWs utenteSec = sec.getUtente();

					final Utente utente = ofSec.createUtente();
					utente.setIdUtente(utenteSec.getIdUtente());
					utente.setIdUtenteSiap(utenteSec.getIdSiap());
					utente.setUsername(utenteSec.getUsername());
					utente.setNome(utenteSec.getNome());
					utente.setCognome(utenteSec.getCognome());
					utente.setCodfis(utenteSec.getCodFis());
					utente.setEmail(utenteSec.getEmail());
					utente.setDataDisattivazione(DateUtils.buildXmlGregorianCalendarFromDate(utenteSec.getDataDisattivazione()));

					security.setUtente(utente);
				}

				securityOut.add(security);
			}
		}
	}

	/**
	 * @param fascicoloRedWs
	 * @param ofDoc
	 * @param classeDoc
	 */
	@SuppressWarnings("unchecked")
	private void setClasseDocumentale(final ClasseDocumentale classeDoc, final String nomeClasseDocumentale, final Map<String, Object> metadatiRedWs,
			final it.ibm.red.webservice.model.documentservice.types.documentale.ObjectFactory ofDoc) {
		// Nome classe documentale
		classeDoc.setNomeClasse(nomeClasseDocumentale);

		// Metadati
		if (!MapUtils.isEmpty(metadatiRedWs)) {
			Metadato metadato = null;

			for (final Entry<String, Object> metadatoRedWs : metadatiRedWs.entrySet()) {
				metadato = ofDoc.createMetadato();

				metadato.setKey(metadatoRedWs.getKey());

				final Object valore = metadatoRedWs.getValue();
				if (valore instanceof Collection) {
					for (final String valoreSingolo : ((Collection<String>) valore)) {
						metadato.getMultiValues().add(valoreSingolo);
					}
				} else {
					metadato.setValue(String.valueOf(metadatoRedWs.getValue()));
				}

				classeDoc.getMetadato().add(metadato);
			}
		}
	}

	private static List<Contatto> getContattiWs(final List<it.ibm.red.business.persistence.model.Contatto> contattiIn) {
		final List<Contatto> output = new ArrayList<>();
		final it.ibm.red.webservice.model.documentservice.types.rubrica.ObjectFactory ofRub = new it.ibm.red.webservice.model.documentservice.types.rubrica.ObjectFactory();

		try {

			for (final it.ibm.red.business.persistence.model.Contatto contatto : contattiIn) {

				final Contatto c = ofRub.createContatto();
				c.setIdContatto(contatto.getContattoID().intValue());
				c.setCap(contatto.getCAP());
				c.setIndirizzo(contatto.getIndirizzo());
				c.setMail(contatto.getMail());
				c.setMailPec(contatto.getMailPec());
				c.setAliasContatto(contatto.getAliasContatto());
				c.setNome(contatto.getNome());
				c.setCognome(contatto.getCognome());
				c.setTipoPersona(contatto.getTipoPersona());
				c.setCellulare(contatto.getCellulare());
				c.setTelefono(contatto.getTelefono());
				c.setPubblico(contatto.getPubblico());
				c.setDataAttivazione(DateUtils.buildXmlGregorianCalendarFromDate(contatto.getDataattivazione()));
				c.setDataDisattivazione(DateUtils.buildXmlGregorianCalendarFromDate(contatto.getDatadisattivazione()));
				c.setRiferimento(contatto.getRiferimento());
				c.setFax(contatto.getFax());
				c.setPIvaCf(contatto.getCodiceFiscalePIva());
				c.setIdComuneIstat(contatto.getIdComuneIstat());
				c.setIdRegioneIstat(contatto.getIdRegioneIstat());
				c.setIdProvinciaIstat(contatto.getIdProvinciaIstat());
				c.setRegione(contatto.getRegione());
				c.setProvincia(contatto.getProvincia());
				c.setComune(contatto.getComune());

				c.setRubrica(contatto.getTipoRubrica());
				c.setIsInterno(false);

				output.add(c);
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante la trasformazione dei dati da restituire come response. ", e);
			throw new RedException("Errore durante la trasformazione dei dati da restituire come response. ", e);
		}

		return output;
	}

	private static List<PropertyClasseDocumentale> getPropertiesWs(final List<PropertyTemplateDTO> propertiesIn) {
		final List<PropertyClasseDocumentale> output = new ArrayList<>();
		final it.ibm.red.webservice.model.documentservice.types.documentale.ObjectFactory ofDoc = new it.ibm.red.webservice.model.documentservice.types.documentale.ObjectFactory();

		for (final PropertyTemplateDTO prop : propertiesIn) {

			final PropertyClasseDocumentale propOut = ofDoc.createPropertyClasseDocumentale();
			propOut.setNomeProperty(prop.getNomeProperty());
			propOut.setTipo(getTipoProperty(prop.getTipo()));

			output.add(propOut);

		}

		return output;
	}

	private static TipoProperty getTipoProperty(final int valueTipo) {
		TipoProperty output = null;

		if (PropertyTemplateDTO.TIPOINT == valueTipo) {
			output = TipoProperty.TIPOINT;
		} else if (PropertyTemplateDTO.TIPODATE == valueTipo) {
			output = TipoProperty.TIPODATE;
		} else if (PropertyTemplateDTO.TIPOSTRING == valueTipo) {
			output = TipoProperty.TIPOSTRING;
		} else if (PropertyTemplateDTO.TIPOBINARY == valueTipo) {
			output = TipoProperty.TIPOBINARY;
		}

		return output;
	}

	/**
	 * @param response
	 * @param contentFile
	 */
	private void gestisciEsitoOkContent(final BaseContentResponseType response, final FileDTO contentFile) {
		response.setContent(contentFile.getContent());
		response.setContentType(contentFile.getMimeType());
		response.setNomeFile(contentFile.getFileName());

		response.setEsito(createEsitoTypeOk());

		LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
	}

	/**
	 * @param response
	 * @param errore
	 */
	private void gestisciEsitoErrore(final BaseResponseType response, final ErrorCode errore) {
		response.setEsito(createEsitoType(errore));
		LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
	}

	/**
	 * @param response
	 * @param errore
	 * @param e
	 */
	private void gestisciEsitoErroreEccezione(final BaseResponseType response, final ErrorCode errore, final Exception e) {
		response.setEsito(createEsitoType(errore));
		if (e instanceof RedException) {
			response.getEsito().setDescErrore(response.getEsito().getDescErrore() + ": " + e.getMessage());
		}
		LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
	}

	/**
	 * @param code
	 * @return
	 */
	private EsitoType createEsitoType(final ErrorCode code) {
		final EsitoType esito = of.createEsitoType();
		esito.setEsito(code == null);

		if (code != null) {
			esito.setCodiceErrore(code.getCode());
			esito.setDescErrore(code.getDescription());
		}

		return esito;
	}

	/**
	 * @return
	 */
	private EsitoType createEsitoTypeOk() {
		final EsitoType esito = of.createEsitoType();
		esito.setEsito(true);
		return esito;
	}

	/**
	 * @param parameters
	 * @return RedProtocolloNPSSyncResponseType response
	 */
	@Override
	public RedProtocolloNPSSyncResponseType redProtocolloNPSSync(final RedProtocolloNPSSyncType parameters) {
		LOGGER.info("[DocumentService][redProtocolloNPSSync][" + parameters.getCredenzialiWS().getIdClient() + "]");
		final RedProtocolloNPSSyncResponseType response = of.createRedProtocolloNPSSyncResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaProtocolloNPSSync(parameters);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(parameters.getCredenzialiWS().getIdClient(), parameters.getCredenzialiWS().getPwdClient(),
						(long) parameters.getIdAoo(), null, null);
				final ProtocolloNpsDTO protocollo = documentoWSSRV.richiediProtocolloNPSync(client, parameters);
				response.setAnnoProtocollo(protocollo.getAnnoProtocollo());
				if (response.getDescrizioneTipologiaDoc() == null) {
					response.setDescrizioneTipologiaDoc("");
				}
				response.setIdProtocollo(protocollo.getIdProtocollo());
				response.setNumProtocollo(protocollo.getNumeroProtocollo());
				response.setOggetto(protocollo.getOggetto());

				saveDataProtocollo(response, protocollo);
				response.setEsito(createEsitoTypeOk());
				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error(ERROR_ELABORAZIONE_MSG + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}

		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * Memorizza la data del protocollo nella response.
	 * 
	 * @param response
	 * @param protocollo
	 */
	private static void saveDataProtocollo(final RedProtocolloNPSSyncResponseType response, final ProtocolloNpsDTO protocollo) {
		try {
			if (protocollo.getDataProtocollo() != null) {
				final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH.mm.ss");
				final String strDate = sdf.format(protocollo.getDataProtocollo());
				response.setDataProtocollo(strDate);
			}
		} catch (final Exception ex) {
			LOGGER.error("Errore durante il settaggio della data protocollo", ex);
		}
	}

	/**
	 * Associa documento al protocollo NPS.
	 * @param parameters
	 * @return AssociateDocumentoProtocolloToAsyncNpsResponseType response
	 */
	@Override
	public AssociateDocumentoProtocolloToAsyncNpsResponseType associateDocumentoProtocolloToAsyncNps(final AssociateDocumentoProtocolloToAsyncNpsType parameters) {
		LOGGER.info("[DocumentService][associateDocumentoProtocolloToAsyncNps][" + parameters.getCredenzialiWS().getIdClient() + "]");
		final AssociateDocumentoProtocolloToAsyncNpsResponseType response = of.createAssociateDocumentoProtocolloToAsyncNpsResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaAssociateDocumentoProtocolloToAsyncNps(parameters);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(parameters.getCredenzialiWS().getIdClient(), parameters.getCredenzialiWS().getPwdClient(),
						(long) parameters.getIdAOO(), null, null);
				final boolean output = documentoWSSRV.associateDocumentoProtocolloToAsyncNps(client, parameters);
				response.setOutput(output);
				response.setEsito(createEsitoTypeOk());
				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][associateDocumentoProtocolloToAsyncNps] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}

		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * Upload documento su NPS in maniera asincrona.
	 * @param parameters
	 * @return UploadDocToAsyncNpsResponseType response
	 */
	@Override
	public UploadDocToAsyncNpsResponseType uploadDocToAsyncNps(final UploadDocToAsyncNpsType parameters) {
		LOGGER.info("[DocumentService][uploadDocToAsyncNps][" + parameters.getCredenzialiWS().getIdClient() + "]");
		final UploadDocToAsyncNpsResponseType response = of.createUploadDocToAsyncNpsResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaUploadDocToAsyncNps(parameters);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(parameters.getCredenzialiWS().getIdClient(), parameters.getCredenzialiWS().getPwdClient(),
						(long) parameters.getIdAOO(), null, null);
				final int output = documentoWSSRV.uploadDocToAsyncNps(client, parameters);
				response.setOutput(output);
				response.setEsito(createEsitoTypeOk());
				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error(ERROR_ELABORAZIONE_MSG + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}

		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * Aggiunge allacci in maniera asincrona.
	 * @param parameters
	 * @return AggiungiAllacciAsyncResponseType response
	 */
	@Override
	public AggiungiAllacciAsyncResponseType aggiungiAllacciAsync(final AggiungiAllacciAsyncType parameters) {
		LOGGER.info("[DocumentService][aggiungiAllacciAsync][" + parameters.getCredenzialiWS().getIdClient() + "]");
		final AggiungiAllacciAsyncResponseType response = of.createAggiungiAllacciAsyncResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaAggiungiAllacciAsync(parameters);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(parameters.getCredenzialiWS().getIdClient(), parameters.getCredenzialiWS().getPwdClient(),
						(long) parameters.getIdAOO(), null, null);
				final boolean output = documentoWSSRV.aggiungiAllacciAsync(client, parameters);
				response.setOutput(output);
				response.setEsito(createEsitoTypeOk());
				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error(ERROR_ELABORAZIONE_MSG + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}

		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * @param parameters
	 * @return SpedisciProtocolloUscitaAsyncResponseType response
	 */
	@Override
	public SpedisciProtocolloUscitaAsyncResponseType spedisciProtocolloUscitaAsync(final SpedisciProtocolloUscitaAsyncType parameters) {
		LOGGER.info("[DocumentService][spedisciProtocolloUscitaAsync][" + parameters.getCredenzialiWS().getIdClient() + "]");
		final SpedisciProtocolloUscitaAsyncResponseType response = of.createSpedisciProtocolloUscitaAsyncResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaSpedisciProtocolloUscitaAsync(parameters);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(parameters.getCredenzialiWS().getIdClient(), parameters.getCredenzialiWS().getPwdClient(),
						(long) parameters.getIdAOO(), null, null);
				final boolean output = documentoWSSRV.spedisciProtocolloUscitaAsync(client, parameters);

				response.setOutput(output);
				response.setEsito(createEsitoTypeOk());
				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][spedisciProtocolloUscitaAsync] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}

		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

	/**
	 * Gestisce i destinatari interni.
	 * @param parameters
	 * @return GestisciDestinatariInterniResponseType response
	 */
	@Override
	public GestisciDestinatariInterniResponseType gestisciDestinatariInterni(final GestisciDestinatariInterniType parameters) {
		LOGGER.info("[DocumentService][spedisciProtocolloUscitaAsync][" + parameters.getCredenzialiWS().getIdClient() + "]");
		final GestisciDestinatariInterniResponseType response = of.createGestisciDestinatariInterniResponseType();

		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaGestisciDestinatariInterni(parameters);

		if (erroreValidazione == null) {
			try {
				final RedWsClient client = new RedWsClient(parameters.getCredenzialiWS().getIdClient(), parameters.getCredenzialiWS().getPwdClient(),
						(long) parameters.getIdAoo(), null, null);
				final boolean output = documentoWSSRV.gestisciDestinatariInterni(client, parameters);

				response.setOutput(output);
				response.setEsito(createEsitoTypeOk());
				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

			} catch (final Exception e) {
				LOGGER.error("[DocumentService][gestisciDestinatariInterni] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}

		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}

		return response;
	}

}
