package it.ibm.red.webservice.services;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

/**
 * Web service.
 */
@WebService(
		name = "PEWSBridge", 
		targetNamespace = "http://redwsevo.mef.gov.it/ibm/PEWSBridge")
public interface IPEWSBridge {

	/**
	 * Recupera tutti i workflow attivi assegnati per visto su idDocumento, 
	 * diversi da fWorkflowNumber e dal Giro Visti Ispettorato
	 * ed il cui ufficio mittente ha come ufficio di segreteria idNodoChiamante.
	 * 
	 * Per ognuno di questi: killa il workflow e starta il workflow di chiusura dello storico. 
	 * 
	 * Use Case: Chiusura visti ufficio in caso il visto ispettore avvenga prima.
	 * 
	 * @param idDocumento identificativo del documento
	 * @param fWorkflowNumber identificativo del workflow che sta chiamando l'invoke
	 * @param idNodoChiamante ufficio dell'utente che ha movimentato fWorkflowNumber
	 * @return
	 */
	@WebMethod(operationName = "terminaWfVisti")
	@WebResult(name = "esito")
	String terminaWfVisti(
			@WebParam(name = "idDocumento")
			Integer idDocumento, 
			@WebParam(name = "workflowNumber")
			String fWorkflowNumber, 
			@WebParam(name = "idNodoChiamante")
			Integer idNodoChiamante);
	
	/**
	 * Avanza tutti i workflow attivi, non visti e non contributi, che insistono su <code>idDocumento</code>, diversi da <code>fWorkflowNumber</code>.
	 * 
	 * @param idDocumento identificativo del documento
	 * @param fWorkflowNumber identificativo del workflow che sta chiamando l'invoke
	 * @param idNodoChiamante ufficio dell'utente che ha movimentato fWorkflowNumber
	 * @return
	 */
	@WebMethod(operationName = "avanzaWf")
	@WebResult(name = "esito")
	String avanzaWf(
			@WebParam(name = "idDocumento")
			Integer idDocumento, 
			@WebParam(name = "workflowNumber")
			String fWorkflowNumber, 
			@WebParam(name = "idNodoChiamante")
			Integer idNodoChiamante);
	
	/**
	 * 
	 * Invia DD e DSR a FEPA post firma e avanza il wf delle DSR in input
	 * 
	 * @param idFascicolo identificativo del fascicolo contenente i documenti da inviare
	 * @param wobDichiarazioneServiziResi
	 * @return
	 */
	@WebMethod(operationName = "inviaFascicoloFepa")
	@WebResult(name = "esito")
	String inviaFascicoloFepa(
			@WebParam(name = "idFascicolo")
			String idFascicolo,
			@WebParam(name = "wobDichiarazioneServiziResi")
			String wobDichiarazioneServiziResi);
	
	/**
	 * 
	 * Invia DD e allegati a FEPA 
	 * 
	 * @param idDecreto identificativo del decreto da inviare
	 * @return
	 */
	@WebMethod(operationName = "inviaDecretoIvaFepa")
	@WebResult(name = "esito")
	String inviaDecretoIvaFepa(
			@WebParam(name = "idDecreto")
			String idDecreto);
	
}