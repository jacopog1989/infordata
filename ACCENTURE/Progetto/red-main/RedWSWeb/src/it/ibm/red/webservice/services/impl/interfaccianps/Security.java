package it.ibm.red.webservice.services.impl.interfaccianps;

import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.webservice.model.interfaccianps.headeraccessoapplicativo.AccessoApplicativo;
import it.ibm.red.webservice.services.interfaccianps.SecurityFault;

public final class Security {

    /**
     * Logger.
     */
	private static final REDLogger LOGGER = REDLogger.getLogger(Security.class);
	
    /**
     * Properties.
     */
	private static PropertiesProvider pp;
	static {
		pp = PropertiesProvider.getIstance();
	}
	
	private Security() {
	}
	
	/**
	 * Verifica dell'header di accesso.
	 * 
	 * @param headerAccesso
	 * @param operation
	 * @param logRadix
	 * @throws SecurityFault
	 */
	public static void checkSecurity(final AccessoApplicativo headerAccesso, final String operation, final String logRadix, final String invoker) throws SecurityFault {
		String message = null;
		final String application = pp.getParameterByKey(PropertiesNameEnum.NPS_APPLICATION_NPSKEY);
		final String pwd = pp.getParameterByKey(PropertiesNameEnum.NPS_PWD_NPSKEY);
		final String service = pp.getParameterByKey(PropertiesNameEnum.NPS_SERVICE_NPSKEY);
		final String user = pp.getParameterByKey(PropertiesNameEnum.NPS_USER_NPSKEY);
		
		if (!application.equals(headerAccesso.getApplicazione())) {
			message = "applicazione non riconosciuta";
		}
		
		if (!service.equals(headerAccesso.getServizio())) {
			message = "servizio non riconosciuto";
		}
		
		if (!user.equals(headerAccesso.getUtente())) {
			message = "utente non riconosciuto";
		}
		
		if (!pwd.equals(headerAccesso.getPassword())) {
			message = "password non riconosciuta";
		}
		
		if (message != null) {
			LOGGER.warn(logRadix + "errore di sicurezza: " + message);
			final it.ibm.red.webservice.model.interfaccianps.headerfault.SecurityFault securityFaultInfo = new it.ibm.red.webservice.model.interfaccianps.headerfault.SecurityFault();
			securityFaultInfo.setAPPLICAZIONE(application);
			securityFaultInfo.setSERVICE(invoker);
			securityFaultInfo.setOPERATION(operation);
			throw new SecurityFault(message, securityFaultInfo);
		}
		
		LOGGER.info(logRadix + "input di sicurezza formalmente corretti");
	}
	
}
