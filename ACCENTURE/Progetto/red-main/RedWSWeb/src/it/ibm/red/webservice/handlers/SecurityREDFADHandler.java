package it.ibm.red.webservice.handlers;

import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.MessageContext.Scope;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.w3c.dom.Node;

import it.ibm.red.business.logger.REDLogger;

/**
 * Handler delle security Red Fad.
 */
public class SecurityREDFADHandler implements SOAPHandler<SOAPMessageContext> {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SecurityREDFADHandler.class.getName());
	
	/**
	 * Key utente.
	 */
	private static final String USER_KEY = "Utente";

	/**
	 * Key password.
	 */
	private static final String PASSWORD_KEY = "Password";

	/**
	 * Key client.
	 */
	private static final String CLIENT_KEY = "Client";

	/**
	 * Key servizio.
	 */
	private static final String SERVICE_KEY = "Servizio";

	/**
	 * Key applicazione.
	 */
	private static final String APPLICATION_KEY = "Applicazione";

	/**
	 * @see javax.xml.ws.handler.soap.SOAPHandler#getHeaders().
	 */
	@Override
	public Set<QName> getHeaders() {
		return null;
	}

	/**
	 * @see javax.xml.ws.handler.Handler#close(javax.xml.ws.handler.MessageContext).
	 */
	@Override
	public void close(final MessageContext context) {
		// Metodo ereditato dall'interfaccia.
	}

	/**
	 * @see javax.xml.ws.handler.Handler#handleFault(javax.xml.ws.handler.MessageContext).
	 */
	@Override
	public boolean handleFault(final SOAPMessageContext context) {
		return false;
	}

	/**
	 * @see javax.xml.ws.handler.Handler#handleMessage(javax.xml.ws.handler.MessageContext).
	 */
	@Override
	public boolean handleMessage(final SOAPMessageContext context) {
		// controllo sicurezza
		try {

			final SOAPMessage message = context.getMessage();
			final SOAPHeader header = message.getSOAPHeader();

			final Node accessoApp = header.getFirstChild().getNextSibling();

			String applicazione = null;
			String servizio = null;
			String client = null;
			String utente = null;
			String password = null;

			if (accessoApp != null && accessoApp.getAttributes().getNamedItem(APPLICATION_KEY) != null) {
				applicazione = accessoApp.getAttributes().getNamedItem(APPLICATION_KEY).getNodeValue();
			}
			if (accessoApp != null && accessoApp.getAttributes().getNamedItem(SERVICE_KEY) != null) {
				servizio = accessoApp.getAttributes().getNamedItem(SERVICE_KEY).getNodeValue();
			}
			if (accessoApp != null && accessoApp.getAttributes().getNamedItem(CLIENT_KEY) != null) {
				client = accessoApp.getAttributes().getNamedItem(CLIENT_KEY).getNodeValue();
			}
			if (accessoApp != null && accessoApp.getAttributes().getNamedItem(USER_KEY) != null) {
				utente = accessoApp.getAttributes().getNamedItem(USER_KEY).getNodeValue();
			}
			if (accessoApp != null && accessoApp.getAttributes().getNamedItem(PASSWORD_KEY) != null) {
				password = accessoApp.getAttributes().getNamedItem(PASSWORD_KEY).getNodeValue();
			}

			LOGGER.info("# # CREDENZIALI RICHIESTA # # ");
			LOGGER.info("applicazione: " + applicazione + " servizio: " + servizio + " client: " + client + " utente: " + utente + " passowrd: " + password);

			context.put(APPLICATION_KEY, applicazione);
			context.setScope(APPLICATION_KEY, Scope.APPLICATION);

			context.put(SERVICE_KEY, servizio);
			context.setScope(SERVICE_KEY, Scope.APPLICATION);

			context.put(CLIENT_KEY, client);
			context.setScope(CLIENT_KEY, Scope.APPLICATION);

			context.put(USER_KEY, utente);
			context.setScope(USER_KEY, Scope.APPLICATION);

			context.put(PASSWORD_KEY, password);
			context.setScope(PASSWORD_KEY, Scope.APPLICATION);

			return true;
		} catch (final SOAPException e) {
			LOGGER.warn(e);
			return false;
		}

	}

}
