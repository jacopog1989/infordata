package it.ibm.red.webservice.services.impl.interfaccianps;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.enums.ErroriAttrExtEnum;

public class ErrorCollector {
	

    /**
	 * Errori.
	 */
	private final EnumMap<ErroriAttrExtEnum, List<String>> errors;

	/**
	 * Istanzia un oggetto HashMap.
	 */
	public ErrorCollector() {
		errors = new EnumMap<>(ErroriAttrExtEnum.class);
	}

	/**
	 * Ottiene gli errori.
	 * @return mappa di errori
	 */
	public Map<ErroriAttrExtEnum, List<String>> getErrors() {
		return errors;
	}
	
	/**
	 * Controlla che non ci siano errori.
	 * @return true se non ci sono errori, false in caso contrario
	 */
	public Boolean noErrors() {
		return errors.isEmpty();
	}

	/**
	 * Controlla se ci sono errori.
	 * @return false se non ci sono errori, true in caso contrario
	 */
	public Boolean errors() {
		return !errors.isEmpty();
	}

	/**
	 * Accumula gli errori.
	 * @param error errore
	 */
	public void collect(final ErroriAttrExtEnum error) {
		collectIF(null, true, error);
	}

	/**
	 * Accumula gli errori
	 * @param check
	 * @param error errore
	 */
	public void collectIF(final Boolean check, final ErroriAttrExtEnum error) {
		collectIF(null, check, error);
	}

	/**
	 * Accumula gli errori condizionatamente.
	 * @param descMetadato descrizione del metadato
	 * @param check
	 * @param error errore
	 */
	public void collectIF(final String descMetadato, final Boolean check, final ErroriAttrExtEnum error) {
		if (Boolean.FALSE.equals(check)) {
			List<String> metErrors = errors.get(error);
			if (metErrors == null) {
				metErrors = new ArrayList<>();
			}
			metErrors.add(descMetadato);
			errors.put(error, metErrors);
		}
	}
}
