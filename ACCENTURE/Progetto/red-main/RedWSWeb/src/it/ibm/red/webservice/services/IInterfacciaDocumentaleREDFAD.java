package it.ibm.red.webservice.services;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.soap.SOAPException;

import it.ibm.red.webservice.model.redfad.interfacciadocumentaleredfad.SecurityFault;
import it.ibm.red.webservice.model.redfad.interfacciadocumentaleredfadtypes_1.RichiestaTerminaFlussoAttoDecretoType;
import it.ibm.red.webservice.model.redfad.interfacciadocumentaleredfadtypes_1.RispostaTerminaFlussoAttoDecretoType;

@WebService(
		name = "InterfacciaDocumentaleREDFAD", 
		targetNamespace = "http://redwsevo.mef.gov.it/ibm/InterfacciaDocumentaleREDFAD")
public interface IInterfacciaDocumentaleREDFAD {

	/**
	 * Termina il flusso dell'atto decreto.
	 * @param request
	 * @return response
	 * @throws SecurityFault
	 * @throws SOAPException
	 */
	@WebMethod(operationName = "terminaFlussoAttoDecreto")
	@WebResult(name = "RispostaTerminaFlussoAttoDecreto")
	RispostaTerminaFlussoAttoDecretoType terminaFlussoAttoDecreto(
			@WebParam(name = "richiestaTerminaFlussoAttoDecreto")
			RichiestaTerminaFlussoAttoDecretoType request) throws SecurityFault, SOAPException;
	
	
}