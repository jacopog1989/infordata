package it.ibm.red.webservice.services;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import it.ibm.red.webservice.model.igepa.psi.types.xsd.AcquisizioneDCPRequest;
import it.ibm.red.webservice.model.igepa.psi.types.xsd.AcquisizioneDCPResponse;

@WebService(
		name = "AcquisizioneDCPService", 
		targetNamespace = "http://redwsevo.mef.gov.it/ibm/AcquisizioneDCPService")
public interface IAcquisizioneDCPService {

	/**
	 * Metodo per l'acquisizione DCP.
	 * @param request
	 * @return response
	 */
	@WebMethod(operationName = "AcquisizioneDCP")
	@WebResult(name = "AcquisizioneDCPResponse")
	AcquisizioneDCPResponse acquisizioneDCP(
			@WebParam(name = "AcquisizioneDCPRequest")
			AcquisizioneDCPRequest request);
	
	
}