package it.ibm.red.webservice.handlers;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.MessageContext.Scope;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.apache.commons.io.output.ByteArrayOutputStream;

import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.ws.IRedWsTrackSRV;
import it.ibm.red.business.service.ws.facade.IRedWsTrackFacadeSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * Handler del Logging.
 */
public class LoggingHandler implements SOAPHandler<SOAPMessageContext> {

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(LoggingHandler.class);

	/**
	 * Valore stringa id track.
	 */
	public static final String ID_TRACK = "idTrack";
	
	/**
	 * Valore stringa esito operazione.
	 */
	public static final String ESITO_OPERAZIONE = "esitoOperazione";
	
	/**
	 * Codice di ok: 1. 
	 */
	public static final Integer OK = 1;
	
	/**
	 * Codice di errore: -1.
	 */
	public static final Integer KO = -1;

	/**
	 * @see javax.xml.ws.handler.soap.SOAPHandler#getHeaders().
	 */
	@Override
	public Set<QName> getHeaders() {
		return null;
	}

	/**
	 * @see javax.xml.ws.handler.Handler#close(javax.xml.ws.handler.MessageContext).
	 */
	@Override
	public void close(final MessageContext context) {
		// Metodo ereditato dall'interfaccia.
	}

	/**
	 * @see javax.xml.ws.handler.Handler#handleFault(javax.xml.ws.handler.MessageContext).
	 */
	@Override
	public boolean handleFault(final SOAPMessageContext context) {
		log(context);
		return true;
	}

	/**
	 * @see javax.xml.ws.handler.Handler#handleMessage(javax.xml.ws.handler.MessageContext).
	 */
	@Override
	public boolean handleMessage(final SOAPMessageContext context) {
		log(context);
		return true;
	}

	private void log(final SOAPMessageContext smc) {
		
		final IRedWsTrackFacadeSRV redWsTrackSRV = ApplicationContextProvider.getApplicationContext().getBean(IRedWsTrackSRV.class);

		final Boolean outboundProperty = (Boolean) smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		try {
			String tipoMessaggio = "response";
			if (!outboundProperty.booleanValue()) {
				tipoMessaggio = "request";        
			}
			final SOAPMessage message = smc.getMessage();
			logger.info(tipoMessaggio);
			final ByteArrayOutputStream stream = new ByteArrayOutputStream();
			message.writeTo(stream);
			final String soapMessage = stream.toString();
			logger.info(soapMessage);
			logger.info("=====================================");
			
			final String serviceName = ((QName) smc.get(MessageContext.WSDL_SERVICE)).getLocalPart();
			final String remoteAddress = getIp((HttpServletRequest) smc.get(MessageContext.SERVLET_REQUEST));
			if (!outboundProperty.booleanValue()) {
				final Long idTrack = redWsTrackSRV.logRequest(soapMessage, serviceName, remoteAddress);
				smc.put(ID_TRACK, idTrack);
	            smc.setScope(ID_TRACK, Scope.APPLICATION);
			} else {
				final Long idTrack = (Long) smc.get(ID_TRACK);
				Integer esitoOperazione = (Integer) smc.get(ESITO_OPERAZIONE);
				if (esitoOperazione == null) {
					esitoOperazione = 0;
				}
				redWsTrackSRV.logResponse(idTrack, esitoOperazione, soapMessage);
			}
		} catch (final Exception e) {
			logger.error("Exception in LoggingHandler", e);
		}
	}

	private String getIp(final HttpServletRequest request) {
		String ipAddress = request.getHeader("X-FORWARDED-FOR");  
		if (StringUtils.isNullOrEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getRemoteAddr();  
		} else {
			final int indicePrimaVirgola = ipAddress.indexOf(",");
			if (indicePrimaVirgola != -1) {
				ipAddress = ipAddress.substring(0, indicePrimaVirgola);
			}
		}
		return ipAddress;
	}

	/**
	 * Setta nel contesto l'esito dell'operazione
	 * 
	 * @param smc
	 * @param esitoOperazione
	 */
	public static void setEsito(final MessageContext smc, final Integer esitoOperazione) {
		smc.put(ESITO_OPERAZIONE, esitoOperazione);
        smc.setScope(ESITO_OPERAZIONE, Scope.APPLICATION);
	}

	/**
	 * Restituisce l'id track registrato nel message context
	 * @param smc
	 * @return
	 */
	public static Long getIdTrack(final MessageContext smc) {
		return (Long) smc.get(ID_TRACK);
	}
}
