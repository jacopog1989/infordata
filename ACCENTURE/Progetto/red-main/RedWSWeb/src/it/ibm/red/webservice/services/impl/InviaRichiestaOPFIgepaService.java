package it.ibm.red.webservice.services.impl;

import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IIgepaFacadeSRV;
import it.ibm.red.webservice.handlers.LoggingHandler;
import it.ibm.red.webservice.model.igepa.inviarichiestaopf.types.xsd.BaseResponse;
import it.ibm.red.webservice.model.igepa.inviarichiestaopf.types.xsd.DocumentoRichiestaOPFIgepa;
import it.ibm.red.webservice.model.igepa.inviarichiestaopf.types.xsd.InvocationStatus;
import it.ibm.red.webservice.model.igepa.inviarichiestaopf.types.xsd.ObjectFactory;
import it.ibm.red.webservice.model.igepa.inviarichiestaopf.types.xsd.RichiestaOPFIgepaRequest;
import it.ibm.red.webservice.model.igepa.inviarichiestaopf.types.xsd.RichiestaOPFIgepaResponse;
import it.ibm.red.webservice.services.IInviaRichiestaOPFIgepaService;

@WebService(portName = "InviaRichiestaOPFIgepaServicePort", serviceName = "InviaRichiestaOPFIgepaService", targetNamespace = "http://redwsevo.mef.gov.it/ibm/InviaRichiestaOPFIgepaService", endpointInterface = "it.ibm.red.webservice.services.IInviaRichiestaOPFIgepaService")
@HandlerChain(file = "/handlers.xml")
public class InviaRichiestaOPFIgepaService implements IInviaRichiestaOPFIgepaService {

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(InviaRichiestaOPFIgepaService.class);

	/**
	 * Context.
	 */
	@Resource
	private WebServiceContext context;

	/**
	 * Servizio.
	 */
	private IIgepaFacadeSRV igepaSRV;

	/**
	 * Costruttore.
	 */
	public InviaRichiestaOPFIgepaService() {
		try {
			igepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IIgepaFacadeSRV.class);
		} catch (final Exception e) {
			logger.error("Errore in fase di costruzione del servizio", e);
		}
	}

	/**
	 * Invia richiesta OPF Igepa.
	 * @param request
	 * @return response
	 */
	@Override
	public RichiestaOPFIgepaResponse inviaRichiestaOPFIgepa(final RichiestaOPFIgepaRequest request) {
		final BaseResponse baseResponse = new BaseResponse();
		final RichiestaOPFIgepaResponse r = new RichiestaOPFIgepaResponse();
		final ObjectFactory of = new ObjectFactory();
		try {

			logger.info("REQUEST ACQUISIZIONE_DCP" + request);

			final DocumentoRichiestaOPFIgepa docRichiestaPrequest = request.getRichiestaOPFIgepaRequest().getValue();

			final int idNodoDest = docRichiestaPrequest.getIdNodoDestinatario() != null && docRichiestaPrequest.getIdNodoDestinatario().getValue() != null
					? docRichiestaPrequest.getIdNodoDestinatario().getValue()
					: 0;
			final int idUtenteDest = docRichiestaPrequest.getIdUtenteDestinatario() != null && docRichiestaPrequest.getIdUtenteDestinatario().getValue() != null
					? docRichiestaPrequest.getIdUtenteDestinatario().getValue()
					: 0;

			String fileName = null;
			byte[] content = null;
			String contentType = null;
			if (docRichiestaPrequest.getDocumentoPrincipale() != null && docRichiestaPrequest.getDocumentoPrincipale().getValue() != null) {
				if (docRichiestaPrequest.getDocumentoPrincipale().getValue().getFileName() != null) {
					fileName = docRichiestaPrequest.getDocumentoPrincipale().getValue().getFileName().getValue();
				}

				if (docRichiestaPrequest.getDocumentoPrincipale().getValue().getFileBase64() != null
						&& docRichiestaPrequest.getDocumentoPrincipale().getValue().getFileBase64().getValue() != null) {

					if (docRichiestaPrequest.getDocumentoPrincipale().getValue().getFileBase64().getValue().getBase64Binary() != null) {
						content = docRichiestaPrequest.getDocumentoPrincipale().getValue().getFileBase64().getValue().getBase64Binary().getValue();
					}

					if (docRichiestaPrequest.getDocumentoPrincipale().getValue().getFileBase64().getValue().getContentType() != null
							&& docRichiestaPrequest.getDocumentoPrincipale().getValue().getFileBase64().getValue().getContentType().getValue() != null
							&& docRichiestaPrequest.getDocumentoPrincipale().getValue().getFileBase64().getValue().getContentType().getValue().getContentTypeType0() != null) {
						contentType = docRichiestaPrequest.getDocumentoPrincipale().getValue().getFileBase64().getValue().getContentType().getValue().getContentTypeType0()
								.getValue();
					}

				}
			}

			Integer numeroRichiesta = null;
			if (docRichiestaPrequest.getNumeroRichiesta() != null) {
				numeroRichiesta = docRichiestaPrequest.getNumeroRichiesta().getValue();
			}

			String annoRichiesta = null;
			if (docRichiestaPrequest.getAnnoRichiesta() != null && docRichiestaPrequest.getAnnoRichiesta().getValue() != null
					&& docRichiestaPrequest.getAnnoRichiesta().getValue().getAnnoFormatType() != null) {
				annoRichiesta = docRichiestaPrequest.getAnnoRichiesta().getValue().getAnnoFormatType().getValue();
			}

			String indiceClassificazione = null;
			if (docRichiestaPrequest.getIndiceClassificazione() != null) {
				indiceClassificazione = docRichiestaPrequest.getIndiceClassificazione().getValue();
			}

			String numeroConto = null;
			if (docRichiestaPrequest.getNumeroConto() != null) {
				numeroConto = docRichiestaPrequest.getNumeroConto().getValue();
			}

			String sezione = null;
			if (docRichiestaPrequest.getSezione() != null) {
				sezione = docRichiestaPrequest.getSezione().getValue();
			}

			String dataProtocollo = null;
			if (docRichiestaPrequest.getDataProtocollo() != null && docRichiestaPrequest.getDataProtocollo().getValue() != null
					&& docRichiestaPrequest.getDataProtocollo().getValue().getDataFormatType() != null) {
				dataProtocollo = docRichiestaPrequest.getDataProtocollo().getValue().getDataFormatType().getValue();
			}

			int numProtocollo = 0;
			if (docRichiestaPrequest.getNumeroProtocollo() != null) {
				numProtocollo = docRichiestaPrequest.getNumeroProtocollo().getValue();
			}

			int tipoDocumento = 0;
			if (docRichiestaPrequest.getTipoDocumento() != null) {
				tipoDocumento = docRichiestaPrequest.getTipoDocumento().getValue();
			}

			int tipoProcedimento = 0;
			if (docRichiestaPrequest.getTipoProcedimento() != null) {
				tipoProcedimento = docRichiestaPrequest.getTipoProcedimento().getValue();
			}

			String oggetto = null;
			if (docRichiestaPrequest.getOggettoRichiesta() != null) {
				oggetto = docRichiestaPrequest.getOggettoRichiesta().getValue();
			}

			String enteMittente = null;
			if (docRichiestaPrequest.getEnteMittente() != null && docRichiestaPrequest.getEnteMittente().getValue() != null
					&& docRichiestaPrequest.getEnteMittente().getValue().getEnteMittenteFormatType() != null) {
				enteMittente = docRichiestaPrequest.getEnteMittente().getValue().getEnteMittenteFormatType().getValue();
			}

			final String iddocumento = igepaSRV.insertDocumentoOPF(idNodoDest, idUtenteDest, content, contentType, numeroRichiesta, annoRichiesta, indiceClassificazione,
					numeroConto, sezione, dataProtocollo, numProtocollo, tipoDocumento, tipoProcedimento, oggetto, enteMittente, fileName);

			logger.info("ACQUISIZIONE_DCP IDDOCUMENTO " + iddocumento);
			final InvocationStatus is = new InvocationStatus();
			is.setValue(of.createInvocationStatusValue("00 - Operazione Eseguita"));
			baseResponse.setStatus(of.createBaseResponseStatus(is));
			baseResponse.setDescription(of.createBaseResponseDescription(is.getValue().getValue() + ". Inserito documento con id: " + iddocumento));
			r.setRichiestaOPFIgepaResponse(of.createRichiestaOPFIgepaResponseRichiestaOPFIgepaResponse(baseResponse));
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
		} catch (final Exception e) {
			logger.error(e.getMessage(), e);
			baseResponse.setDescription(of.createBaseResponseDescription(e.getMessage()));
			final InvocationStatus is = new InvocationStatus();
			is.setValue(of.createInvocationStatusValue("99 - Errore"));
			baseResponse.setStatus(of.createBaseResponseStatus(is));
			r.setRichiestaOPFIgepaResponse(of.createRichiestaOPFIgepaResponseRichiestaOPFIgepaResponse(baseResponse));
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
		return r;
	}

}