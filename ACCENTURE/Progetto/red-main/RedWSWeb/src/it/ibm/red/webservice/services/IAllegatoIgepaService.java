package it.ibm.red.webservice.services;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import it.ibm.red.webservice.model.igepa.allegatorichiestaopf.types.xsd.AllegatoIgepaRequest;
import it.ibm.red.webservice.model.igepa.allegatorichiestaopf.types.xsd.AllegatoIgepaResponse;

@WebService(
		name = "AllegatoIgepaService", 
		targetNamespace = "http://redwsevo.mef.gov.it/ibm/AllegatoIgepaService")
public interface IAllegatoIgepaService {

	/**
	 * Metodo per l'allegato alla richiesta IGEPA.
	 * @param request
	 * @return response
	 */
	@WebMethod(operationName = "AllegatoRichiestaIgepa")
	@WebResult(name = "AllegatoIgepaResponse")
	AllegatoIgepaResponse allegatoRichiestaIgepa(
			@WebParam(name = "AllegatoIgepaRequest")
			AllegatoIgepaRequest request);
	
	
}