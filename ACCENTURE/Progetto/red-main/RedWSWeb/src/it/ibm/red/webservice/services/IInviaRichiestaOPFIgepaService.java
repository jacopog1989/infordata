package it.ibm.red.webservice.services;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import it.ibm.red.webservice.model.igepa.inviarichiestaopf.types.xsd.RichiestaOPFIgepaRequest;
import it.ibm.red.webservice.model.igepa.inviarichiestaopf.types.xsd.RichiestaOPFIgepaResponse;

@WebService(
		name = "InviaRichiestaOPFIgepaService", 
		targetNamespace = "http://redwsevo.mef.gov.it/ibm/InviaRichiestaOPFIgepaService")
public interface IInviaRichiestaOPFIgepaService {

	/**
	 * Invia la richiesta OPF IGEPA.
	 * @param request
	 * @return response
	 */
	@WebMethod(operationName = "InviaRichiestaOPFIgepa")
	@WebResult(name = "RichiestaOPFIgepaResponse")
	RichiestaOPFIgepaResponse inviaRichiestaOPFIgepa(
			@WebParam(name = "RichiestaOPFIgepaRequest")
			RichiestaOPFIgepaRequest request);
	
	
}