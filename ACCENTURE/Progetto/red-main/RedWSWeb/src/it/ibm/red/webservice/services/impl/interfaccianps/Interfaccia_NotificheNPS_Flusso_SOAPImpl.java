package it.ibm.red.webservice.services.impl.interfaccianps;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.xml.ws.WebServiceContext;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.dto.CapitoloSpesaDTO;
import it.ibm.red.business.dto.ErroreValidazioneDTO;
import it.ibm.red.business.dto.LookupTableDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.RicercaCapitoloSpesaDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.dto.TipologiaDTO;
import it.ibm.red.business.enums.AUTMessageIDEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.ErroriAttrExtEnum;
import it.ibm.red.business.enums.FascicoloFepaEnum;
import it.ibm.red.business.enums.SICOGEMessageIDEnum;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.enums.TipoDocumentoModeEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.ILookupTableSRV;
import it.ibm.red.business.service.IProtocollaFlussoSRV;
import it.ibm.red.business.service.facade.ICapitoloSpesaFacadeSRV;
import it.ibm.red.business.service.facade.IPostaNpsFacadeSRV;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.webservice.handlers.LoggingHandler;
import it.ibm.red.webservice.model.interfaccianps.headeraccessoapplicativo.AccessoApplicativo;
import it.ibm.red.webservice.model.interfaccianps.headerfault.SeverityType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.BaseServiceResponseType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.EsitoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraComunicazioneGenericaFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraErroreFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraErroreProtocollazioneFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioProtocollazioneFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaValidazioneStatoFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RispostaElaboraComunicazioneGenericaFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RispostaElaboraErroreFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RispostaElaboraErroreProtocollazioneFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RispostaElaboraMessaggioFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RispostaElaboraMessaggioProtocollazioneFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RispostaValidazioneStatoFlusso;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.ServiceErrorType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.AllCodiceDescrizioneType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.EmailMessageType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.ProtIdentificatoreProtocolloType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.WkfDatiContestoProceduraleType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.WkfDatiFlussoEntrataType;
import it.ibm.red.webservice.model.interfaccianps.npstypesestesi.MetaDatoAssociato;
import it.ibm.red.webservice.model.interfaccianps.npstypesestesi.TIPOLOGIA;
import it.ibm.red.webservice.services.interfaccianps.GenericFault;
import it.ibm.red.webservice.services.interfaccianps.SecurityFault;

@javax.jws.WebService(endpointInterface = "it.ibm.red.webservice.services.interfaccianps.InterfacciaNotificheNPSFlusso", targetNamespace = "http://mef.gov.it.v1.nps/servizi/InterfacciaNPS", serviceName = "Interfaccia_NotificheNPS_Flusso", portName = "Interfaccia_Notifiche_FlussoNPS_SOAPPORT")
@HandlerChain(file = "/handlers.xml")
public class Interfaccia_NotificheNPS_Flusso_SOAPImpl {

	/**
	 * Label START.
	 */
	private static final String START_LABEL = "start";

	/**
	 * Nome invoker del servizio.
	 */
	private static final String INVOKER_INTERFACCIA_NOTIFICHENPS_FLUSSO = "Interfaccia_NotificheNPS_Flusso";

	/**
	 * logger..
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Interfaccia_NotificheNPS_Flusso_SOAPImpl.class);

	/**
	 * Errore generico.
	 */
	private static final int GENERIC_ERROR_CODE = 99;

	/**
	 * Codice errore validazione metadati.
	 */
	private static final String CODICE_ERRORE_VALIDAZIONE_METADATI = "E104";

	/**
	 * Context.
	 */
	@Resource
	private WebServiceContext context;

	/**
	 * messaggi.
	 */
	private it.ibm.red.webservice.model.interfaccianps.npsmessages.ObjectFactory ofMessages;

	/**
	 * Servizio.
	 */
	private IPostaNpsFacadeSRV postaNpsSRV;

	/**
	 * Servizio.
	 */
	private ICapitoloSpesaFacadeSRV capitoloSpesaSRV;

	/**
	 * Servizio.
	 */
	private ITipologiaDocumentoFacadeSRV tipoDocSRV;

	/**
	 * Servizio.
	 */
	private IProtocollaFlussoSRV protocollaFlussoSRV;

	/**
	 * Servizio.
	 */
	private IAooSRV aooSRV;

	/**
	 * Servizio gestione lookup Tables.
	 */
	private ILookupTableSRV lookupSRV;

	/**
	 * Costruttore.
	 */
	public Interfaccia_NotificheNPS_Flusso_SOAPImpl() {
		try {
			ofMessages = new it.ibm.red.webservice.model.interfaccianps.npsmessages.ObjectFactory();
			postaNpsSRV = ApplicationContextProvider.getApplicationContext().getBean(IPostaNpsFacadeSRV.class);
			capitoloSpesaSRV = ApplicationContextProvider.getApplicationContext().getBean(ICapitoloSpesaFacadeSRV.class);
			tipoDocSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoFacadeSRV.class);
			protocollaFlussoSRV = ApplicationContextProvider.getApplicationContext().getBean(IProtocollaFlussoSRV.class);
			aooSRV = ApplicationContextProvider.getApplicationContext().getBean(IAooSRV.class);
			lookupSRV = ApplicationContextProvider.getApplicationContext().getBean(ILookupTableSRV.class);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di costruzione del servizio", e);
		}
	}

	/**
	 * Flusso AUT/CG2.
	 * 
	 * @param headerAccesso
	 * @param parameters
	 * @return
	 * @throws GenericFault
	 * @throws SecurityFault
	 */
	public RispostaElaboraMessaggioFlussoType elaboraMessaggioFlusso(final AccessoApplicativo headerAccesso, final RichiestaElaboraMessaggioFlussoType parameters)
			throws GenericFault, SecurityFault {
		final String messageId = headerAccesso.getMessageId();
		final Long idTrack = LoggingHandler.getIdTrack(context.getMessageContext());
		final String logRadix = "[Interfaccia_NotificheNPS_Flusso_SOAPImpl][elaboraMessaggioFlusso][" + messageId + "][" + idTrack + "] ";

		LOGGER.info(logRadix + START_LABEL);

		final RispostaElaboraMessaggioFlussoType response = ofMessages.createRispostaElaboraMessaggioFlussoType();
		response.setMessageId(messageId);

		Security.checkSecurity(headerAccesso, "elaboraMessaggioFlusso", logRadix, INVOKER_INTERFACCIA_NOTIFICHENPS_FLUSSO);

		final ErroreValidazioneDTO retrieveParamsError = checkParametersElaboraMessaggioFlusso(parameters, logRadix);

		if (retrieveParamsError == null) {

			try {
				postaNpsSRV.accodaMessaggioFlusso(headerAccesso.getMessageId(), parameters, idTrack);

				response.setEsito(EsitoType.OK);

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
			} catch (final Exception e) {
				LOGGER.error(logRadix, e);
				handleResponseElaborationError(response, GENERIC_ERROR_CODE, e.getMessage());
			}

		} else {
			handleResponseElaborationError(response, retrieveParamsError.getCodice(), retrieveParamsError.getDescrizione());
		}

		LOGGER.info(logRadix + "end");

		return response;
	}

	/**
	 * Flusso Ordinario, Silice e Sicoge
	 * 
	 * @param headerAccesso
	 * @param parameters
	 * @return
	 * @throws SecurityFault
	 */
	public RispostaElaboraMessaggioProtocollazioneFlussoType elaboraMessaggioProtocollazioneFlusso(final AccessoApplicativo headerAccesso,
			final RichiestaElaboraMessaggioProtocollazioneFlussoType parameters) throws SecurityFault {
		final String messageId = headerAccesso.getMessageId();
		final Long idTrack = LoggingHandler.getIdTrack(context.getMessageContext());
		final String logRadix = "[Interfaccia_NotificheNPS_Flusso_SOAPImpl][elaboraMessaggioProtocollazioneFlusso][" + messageId + "][" + idTrack + "] ";

		LOGGER.info(logRadix + START_LABEL);

		final RispostaElaboraMessaggioProtocollazioneFlussoType response = ofMessages.createRispostaElaboraMessaggioProtocollazioneFlussoType();
		response.setMessageId(messageId);

		Security.checkSecurity(headerAccesso, "elaboraMessaggioProtocollazioneFlusso", logRadix, INVOKER_INTERFACCIA_NOTIFICHENPS_FLUSSO);

		final ErroreValidazioneDTO retrieveParamsError = checkParametersElaboraMessaggioProtocollazioneFlusso(parameters, logRadix);

		if (retrieveParamsError == null) {

			try {
				postaNpsSRV.accodaMessaggioProtocollazioneFlusso(headerAccesso.getMessageId(), parameters, idTrack);

				response.setEsito(EsitoType.OK);

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
			} catch (final Exception e) {
				LOGGER.error(logRadix, e);
				handleResponseElaborationError(response, GENERIC_ERROR_CODE, e.getMessage());
			}

		} else {
			handleResponseElaborationError(response, retrieveParamsError.getCodice(), retrieveParamsError.getDescrizione());
		}

		LOGGER.info(logRadix + "end");

		return response;
	}

	/**
	 * @param headerAccesso
	 * @param parameters
	 * @return
	 * @throws GenericFault
	 */
	public RispostaElaboraComunicazioneGenericaFlussoType elaboraComunicazioneGenericaFlusso(final AccessoApplicativo headerAccesso,
			final RichiestaElaboraComunicazioneGenericaFlussoType parameters) throws GenericFault {
		throwNotImplementedFault();
		return null;
	}

	/**
	 * @param headerAccesso
	 * @param parameters
	 * @return
	 */
	public RispostaValidazioneStatoFlusso validazioneStatoFlusso(final AccessoApplicativo headerAccesso, final RichiestaValidazioneStatoFlussoType parameters) {
		final String messageId = headerAccesso.getMessageId();
		final Long idTrack = LoggingHandler.getIdTrack(context.getMessageContext());
		final String logRadix = "[Interfaccia_NotificheNPS_Flusso_SOAPImpl][validazioneStatoFlusso][" + messageId + "][" + idTrack + "] ";
		LOGGER.info(logRadix + START_LABEL);
		final RispostaValidazioneStatoFlusso out = new RispostaValidazioneStatoFlusso();
		out.setMessageId(parameters.getIdMessaggio());
		out.setEsito(it.ibm.red.webservice.model.interfaccianps.npsmessages.EsitoType.OK);

		final ErrorCollector ec = new ErrorCollector();

		try {
			// STEP1: METADATI - Check configurazioni iniziali
			final WkfDatiContestoProceduraleType cpType = parameters.getContestoProcedurale().get(0);
			final String codiceAOO = parameters.getAoo().getCodiceAOO();

			ec.collectIF(!StringUtils.isNullOrEmpty(codiceAOO), ErroriAttrExtEnum.ERRORE_RECUPERO_AOO);

			final String codiceFlusso = parameters.getCodiceFlusso();
			ec.collectIF(!StringUtils.isNullOrEmpty(codiceFlusso), ErroriAttrExtEnum.CODICE_FLUSSO_NON_PRESENTE);

			final TIPOLOGIA tipologia = cpType.getDatiEstesi();
			final String nomeTipologia = tipologia.getNOME();

			ec.collectIF(!StringUtils.isNullOrEmpty(nomeTipologia), ErroriAttrExtEnum.ERRORE_RECUPERO_NOME_TIPOLOGIA);

			final List<MetadatoDTO> metadatiConfigurati = getMetadati(nomeTipologia, codiceAOO);
			ec.collectIF(metadatiConfigurati != null, ErroriAttrExtEnum.ERRORE_CONFIGURAZIONE_NOME_TIPOLOGIA);

			if (Boolean.TRUE.equals(ec.noErrors())) {
				// STEP2: METADATI - check metadati
				final List<MetaDatoAssociato> metadatiForniti = tipologia.getMetadatoAssociato();

				// Verifichiamo che tutti i metadati forniti siano esistenti ed abbiano un
				// valore valido
				for (final MetaDatoAssociato metadatiFornito : metadatiForniti) {
					for (final MetadatoDTO metadatiDichiarato : metadatiConfigurati) {
						if (metadatiFornito.getCodice().equalsIgnoreCase(metadatiDichiarato.getName())) {
							ec.collectIF(metadatiDichiarato.getName(), validate(metadatiDichiarato, metadatiFornito.getValore(), codiceFlusso, codiceAOO),
									ErroriAttrExtEnum.VALORE_NON_VALIDO);
							break;
						}
					}
				}

				// Verifichiamo che tutti i metadati obbligatori siano forniti
				for (final MetadatoDTO metadatiDichiarato : metadatiConfigurati) {
					if (isMandatatoryOnEntry(metadatiDichiarato)) {
						MetadatoDTO mFound = null;
						for (final MetaDatoAssociato metadatiFornito : metadatiForniti) {
							if (metadatiFornito.getCodice().equalsIgnoreCase(metadatiDichiarato.getName())) {
								mFound = metadatiDichiarato;
								break;
							}
						}
						ec.collectIF(metadatiDichiarato.getName(), mFound != null, ErroriAttrExtEnum.VALORE_NON_PRESENTE);
					}
				}
			}

			// Controlli flusso AUT/CG2/SICOGE
			TipoContestoProceduraleEnum tcp = TipoContestoProceduraleEnum.getEnumById(codiceFlusso);
			if (tcp != null && tcp.isFamigliaFlussiAut()) {
				validaFlussiAUT(parameters, ec, tcp);
			} else if (TipoContestoProceduraleEnum.SICOGE.equals(tcp)) {
				validaFlussoSICOGE(parameters, ec);
			}

			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

		} catch (final Exception e) {
			LOGGER.error("Errore generico in fase di validazione del flusso per message id " + parameters.getIdMessaggio(), e);
			out.setEsito(it.ibm.red.webservice.model.interfaccianps.npsmessages.EsitoType.ERRORE_ELABORAZIONE);
			ec.collect(ErroriAttrExtEnum.GENERICO);
		}

		if (Boolean.TRUE.equals(ec.errors())) {
			out.setEsito(it.ibm.red.webservice.model.interfaccianps.npsmessages.EsitoType.ERRORE_ELABORAZIONE);
			StringBuilder descrizioneErrore = null; 
			for (final Entry<ErroriAttrExtEnum, List<String>> error : ec.getErrors().entrySet()) {

				StringBuilder metadati = new StringBuilder("");
				for (final String metadato : error.getValue()) {
					metadati.append(metadato).append(",");
				}
				metadati = new StringBuilder(StringUtils.cutLast(metadati.toString()));

				final ServiceErrorType err = new ServiceErrorType();
				err.setErrorCode(error.getKey().getValue());
				
				final String descrErroreIesimo = error.getKey().getDescription().replace("<<METADATI>>", metadati);
				err.setErrorMessageString(descrErroreIesimo);
				out.getErrorList().add(err);
				
				if(descrizioneErrore == null) {
					descrizioneErrore = new StringBuilder("Errore nei metadati: ");
				}
				descrizioneErrore.append(descrErroreIesimo + ";");

			}
			
			if(descrizioneErrore != null) {
				AllCodiceDescrizioneType errore = new AllCodiceDescrizioneType();
				errore.setCodice(CODICE_ERRORE_VALIDAZIONE_METADATI);
				errore.setDescrizione(descrizioneErrore.toString());
				out.setErrore(errore);
			}
			
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}

		LOGGER.info(logRadix + "end");
		return out;
	}

	/**
	 * Loica di validazione dei dati per il flussi AUT.
	 * 
	 * @param parameters
	 * @param ec
	 * @param tcp
	 */
	private void validaFlussiAUT(final RichiestaValidazioneStatoFlussoType parameters, final ErrorCollector ec, final TipoContestoProceduraleEnum tcp) {
		final Integer idMessaggio = Integer.valueOf(parameters.getIdentificativoMessaggio());
		final AUTMessageIDEnum msg = AUTMessageIDEnum.getEnumById(idMessaggio);

		// STEP 1: FLUSSI AUT - ID MESSAGGIO NON PRESENTE
		ec.collectIF(null, msg != null, ErroriAttrExtEnum.ID_MESSAGGIO_NON_CONFORME);

		if (Boolean.TRUE.equals(ec.noErrors())) {
			final Integer idAoo = tipoDocSRV.getIdAOOByCodiceNPS(parameters.getAoo().getCodiceAOO());
			final Aoo aoo = aooSRV.recuperaAoo(idAoo);
			final AooFilenet aooFilenet = aoo.getAooFilenet();
			final String idProcesso = parameters.getIdentificatoreProcesso();
			
			final String dt = protocollaFlussoSRV.getDocumentTitleByIdProcesso(aooFilenet, tcp, idProcesso);
			if (!StringUtils.isNullOrEmpty(dt)) {
				// Esiste processo
				ec.collectIF(null, !AUTMessageIDEnum.MESSAGGIO_INIZIALE.equals(msg), ErroriAttrExtEnum.ID_PROCESSO_ESISTENTE); // Non devi essere messaggio iniziale

				if (Boolean.TRUE.equals(ec.noErrors())) {
					final String queueName = protocollaFlussoSRV.getQueueNameWorkflowPrincipale(dt, aoo.getAooFilenet());

					if (queueName == null) {
						// Devi essere in almeno una coda
						ec.collectIF(null, true, ErroriAttrExtEnum.STATO_DOCUMENTO_NON_CONFORME);
					} else if (AUTMessageIDEnum.DATI_INTEGRATIVI_RICHIESTI.equals(msg)) { // 102
						// Se ti fornisco dati aggiuntivi a seguito di una richiesta devi essere in
						// sospeso
						ec.collectIF(null, oneOf(queueName, DocumentQueueEnum.SOSPESO), ErroriAttrExtEnum.STATO_DOCUMENTO_NON_CONFORME);

					} else if (AUTMessageIDEnum.DATI_INTEGRATIVI_SPONTANEI.equals(msg) || AUTMessageIDEnum.RICHIESTA_RITIRO_AUTOTUTELA.equals(msg)) { // 103 e 104
						// Se ti fornisco dati aggiuntivi in maniera spontanea, oppure ti richiedo di
						// ritirare il documento, devi essere da lavorare, corriere o sospeso
						ec.collectIF(null, oneOf(queueName, DocumentQueueEnum.DA_LAVORARE, DocumentQueueEnum.CORRIERE, DocumentQueueEnum.SOSPESO),
								ErroriAttrExtEnum.STATO_DOCUMENTO_NON_CONFORME);
					}
				}
			} else {
				// Non esiste processo
				ec.collectIF(null, AUTMessageIDEnum.MESSAGGIO_INIZIALE.equals(msg), ErroriAttrExtEnum.ID_PROCESSO_NON_ESISTENTE); // Devi essere messaggio iniziale (101)
			}

		}
	}

	/**
	 * Logica di validazione dei dati per il flusso SICOGE.
	 * 
	 * @param parameters
	 * @param ec
	 */
	private void validaFlussoSICOGE(final RichiestaValidazioneStatoFlussoType parameters, final ErrorCollector ec) {
		final Integer idMessaggio = Integer.parseInt(parameters.getIdentificativoMessaggio());
		final SICOGEMessageIDEnum msg = SICOGEMessageIDEnum.getEnumById(idMessaggio);

		// STEP 1: FLUSSO SICOGE - ID MESSAGGIO NON PRESENTE
		ec.collectIF(null, msg != null, ErroriAttrExtEnum.ID_MESSAGGIO_NON_CONFORME);

		// STEP 2: FLUSSO SICOGE - ID FASCICOLO FEPA NON PRESENTE
		ec.collectIF(null, parameters.getChiaveFascicolo() != null, ErroriAttrExtEnum.ID_FASCICOLO_FEPA_NON_PRESENTE);

		// STEP 3: Si esegue la validazione sul processo
		if (Boolean.TRUE.equals(ec.noErrors())) {
			final String idProcesso = parameters.getChiaveFascicolo(); // Per il flusso SICOGE, l'identificatore del processo è il fascicolo FEPA
			final String codiceAooNps = parameters.getAoo().getCodiceAOO();
			final Aoo aoo = aooSRV.recuperaAooFromNPS(codiceAooNps);
			final AooFilenet aooFilenet = aoo.getAooFilenet();

			final String dt = protocollaFlussoSRV.getDocumentTitleByIdProcesso(aooFilenet, TipoContestoProceduraleEnum.SICOGE, idProcesso);
			if (!StringUtils.isNullOrEmpty(dt)) {
				// Esiste processo
				ec.collectIF(null, !SICOGEMessageIDEnum.SICOGE_PROTOCOLLA_TITOLO.equals(msg), ErroriAttrExtEnum.ID_PROCESSO_ESISTENTE); // NON devi essere messaggio iniziale
																																		// (1002)
			} else {
				// Non esiste processo
				ec.collectIF(null, SICOGEMessageIDEnum.SICOGE_PROTOCOLLA_TITOLO.equals(msg), ErroriAttrExtEnum.ID_PROCESSO_NON_ESISTENTE); // Devi essere messaggio iniziale
																																			// (1001)
			}

		}
	}

	private static Boolean oneOf(final String codaAttuale, final DocumentQueueEnum... codeAccettate) {
		Boolean bFound = false;

		for (final DocumentQueueEnum codaAccettata : codeAccettate) {
			if (codaAccettata.getName().equals(codaAttuale)) {
				bFound = true;
				break;
			}
		}

		return bFound;
	}

	/**
	 * Errori elaborati a fronte di inviaAtto (flussI AUT)
	 * 
	 * @param headerAccesso
	 * @param parameters
	 * @return
	 * @throws SecurityFault
	 */
	public RispostaElaboraErroreFlussoType elaboraErroreFlusso(final AccessoApplicativo headerAccesso, final RichiestaElaboraErroreFlussoType parameters)
			throws SecurityFault {
		final String messageId = headerAccesso.getMessageId();
		final Long idTrack = LoggingHandler.getIdTrack(context.getMessageContext());
		final String logRadix = "[Interfaccia_NotificheNPS_Flusso_SOAPImpl][elaboraErroreFlusso][" + messageId + "][" + idTrack + "] ";

		LOGGER.info(logRadix + START_LABEL);

		final RispostaElaboraErroreFlussoType response = ofMessages.createRispostaElaboraErroreFlussoType();
		response.setMessageId(messageId);

		Security.checkSecurity(headerAccesso, "elaboraErroreFlusso", logRadix, INVOKER_INTERFACCIA_NOTIFICHENPS_FLUSSO);

		try {

			response.setEsito(EsitoType.OK);
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

		} catch (final Exception e) {
			LOGGER.error(logRadix, e);
			handleResponseElaborationError(response, GENERIC_ERROR_CODE, e.getMessage());
		}

		LOGGER.info(logRadix + "end");

		return response;
	}

	/**
	 * @param m
	 * @return
	 */
	private static boolean isMandatatoryOnEntry(final MetadatoDTO m) {
		return m.getObligatoriness() != null
				&& (m.getObligatoriness().equals(TipoDocumentoModeEnum.SOLO_ENTRATA) || m.getObligatoriness().equals(TipoDocumentoModeEnum.SEMPRE));
	}

	/**
	 * Esegue la validazione di un metadato. La validazione dipende dalla tipologia del metadato.
	 * @param metadatoToValidate metadato dichiarato
	 * @param value valore del metadato fornito
	 * @param codiceFlusso codice del flusso
	 * @param codiceAOO codice dell'AOO
	 * @return true se valido, false altrimenti
	 */
	private Boolean validate(final MetadatoDTO metadatoToValidate, final String value, final String codiceFlusso, final String codiceAOO) {
		Boolean out = true;

		if (StringUtils.isNullOrEmpty(value)) {
			return out;
		}

		if (TipoMetadatoEnum.STRING.equals(metadatoToValidate.getType()) || TipoMetadatoEnum.TEXT_AREA.equals(metadatoToValidate.getType())) {
			final String tmp = value;
			out = tmp.length() <= metadatoToValidate.getDimension();
		} else if (TipoMetadatoEnum.INTEGER.equals(metadatoToValidate.getType()) || TipoMetadatoEnum.DOUBLE.equals(metadatoToValidate.getType())) {
			if (metadatoToValidate.getDimension() != null && metadatoToValidate.getDimension() > 0) {
				try {
					final Integer tmp = Integer.parseInt(value);
					out = tmp.toString().length() <= metadatoToValidate.getDimension();
				} catch (final Exception e) {
					out = false;
					LOGGER.error("Errore in fase di validazione del metadato " + metadatoToValidate.getName(), e);
				}
			}
		} else if (TipoMetadatoEnum.DATE.equals(metadatoToValidate.getType())) {
			try {
				final SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
				sdf.parse(value);
			} catch (final Exception e) {
				out = false;
				LOGGER.error("Errore in fase di validazione del metadato " + metadatoToValidate.getName(), e);
			}
		} else if (TipoMetadatoEnum.LOOKUP_TABLE.equals(metadatoToValidate.getType())) {
			Boolean bFound = false;
			final LookupTableDTO lm = (LookupTableDTO) metadatoToValidate;
			boolean compareWithLike = lookupSRV.hasToCompareWithLike(lm.getId(), codiceAOO);
			for (final SelectItemDTO si : lm.getLookupValues()) {
				final String tmp = si.getDescription().toUpperCase();
				final String v = value.toUpperCase();
				if((!compareWithLike && tmp.equals(v)) 
						|| (compareWithLike && tmp.startsWith(v))) {

					bFound = true;
					break;
				}
			}
			out = bFound;
		} else if (TipoMetadatoEnum.CAPITOLI_SELECTOR.equals(metadatoToValidate.getType())) {

			Boolean bFound = false;

			if (!TipoContestoProceduraleEnum.SICOGE.equals(TipoContestoProceduraleEnum.getEnumById(codiceFlusso))) {
				bFound = capitoloExists(codiceAOO, value);
			} else {
				bFound = true;
			}

			out = bFound;
		} else if (TipoMetadatoEnum.PERSONE_SELECTOR.equals(metadatoToValidate.getType())) {
			out = true;
		} else {
			out = false;
		}
		return out;
	}

	/**
	 * @param nomeTipologia
	 * @param codiceAOO
	 * @return
	 */
	private List<MetadatoDTO> getMetadati(final String nomeTipologia, final String codiceAOO) {
		final Integer idAoo = tipoDocSRV.getIdAOOByCodiceNPS(codiceAOO);
		final TipologiaDTO tipologa = tipoDocSRV.getTipologiaFromTipologiaNPS(true, idAoo, nomeTipologia);
		return tipoDocSRV.caricaMetadatiEstesiPerGUI(tipologa.getIdTipoDocumento(), tipologa.getIdTipoProcedimento(), true, idAoo, null);
	}

	private boolean capitoloExists(final String codiceAOO, final String codiceCapitolo) {
		final Integer idAoo = tipoDocSRV.getIdAOOByCodiceNPS(codiceAOO);

		final RicercaCapitoloSpesaDTO ricercaDTO = new RicercaCapitoloSpesaDTO(idAoo.longValue());
		ricercaDTO.setCodice(codiceCapitolo);
		ricercaDTO.setDescrizione("");
		final Collection<CapitoloSpesaDTO> capitoli = capitoloSpesaSRV.ricerca(ricercaDTO);

		return capitoli != null && !capitoli.isEmpty();
	}

	/**
	 * @param parameters
	 * @param logRadix
	 * @return
	 */
	private ErroreValidazioneDTO checkParametersElaboraMessaggioProtocollazioneFlusso(final RichiestaElaboraMessaggioProtocollazioneFlussoType parameters,
			final String logRadix) {
		return checkParametersElaboraFlussoEntrata(parameters, logRadix);
	}

	/**
	 * @param parameters
	 * @param logRadix
	 * @return
	 */
	private ErroreValidazioneDTO checkParametersElaboraMessaggioFlusso(final RichiestaElaboraMessaggioFlussoType parameters, final String logRadix) {
		return checkParametersElaboraFlussoEntrata(parameters, logRadix);
	}

	/**
	 * @param parameters
	 * @param logRadix
	 * @return
	 */
	private ErroreValidazioneDTO checkParametersElaboraFlussoEntrata(final WkfDatiFlussoEntrataType parameters, final String logRadix) {
		ErroreValidazioneDTO retrieveParamsError = null;
		String retrieveParametersError = null;
		int retrieveParametersErrorCode = 0;

		try {
			if (StringUtils.isNullOrEmpty(parameters.getIdMessaggio())) {
				retrieveParametersError = "ID Messaggio non presente";
				retrieveParametersErrorCode = 1;
				throw new RedException(retrieveParametersError);
			}

			if (StringUtils.isNullOrEmpty(parameters.getCodiceFlusso())) {
				retrieveParametersError = "Codice flusso non presente";
				retrieveParametersErrorCode = 2;
				throw new RedException(retrieveParametersError);
			}

			if (StringUtils.isNullOrEmpty(parameters.getIdentificatoreProcesso())) {
				retrieveParametersError = "Identificatore processo non presente";
				retrieveParametersErrorCode = 3;
				throw new RedException(retrieveParametersError);
			}

			final TipoContestoProceduraleEnum tcp = TipoContestoProceduraleEnum.getEnumById(parameters.getCodiceFlusso());

			// SICOGE
			if (TipoContestoProceduraleEnum.SICOGE.equals(tcp)) {
				if (parameters.getProtocollazioneCoda() == null) {
					retrieveParametersError = "Dati SICOGE (protocollazione coda) non presenti [SICOGE]";
					retrieveParametersErrorCode = 4;
					throw new RedException(retrieveParametersError);
				}

				if (StringUtils.isNullOrEmpty(parameters.getProtocollazioneCoda().getIdentificativoMessaggio())
						|| !NumberUtils.isCreatable(parameters.getProtocollazioneCoda().getIdentificativoMessaggio())) {
					retrieveParametersError = "Identificativo messaggio non presente [SICOGE]";
					retrieveParametersErrorCode = 5;
					throw new RedException(retrieveParametersError);
				}

				final SICOGEMessageIDEnum smid = SICOGEMessageIDEnum.getEnumById(NumberUtils.createInteger(parameters.getProtocollazioneCoda().getIdentificativoMessaggio()));
				if (smid == null) {
					retrieveParametersError = "Identificativo messaggio non censito [SICOGE]";
					retrieveParametersErrorCode = 6;
					throw new RedException(retrieveParametersError);
				} else if (SICOGEMessageIDEnum.SICOGE_PROTOCOLLA_TITOLO.equals(smid)) {
					if (StringUtils.isNullOrEmpty(parameters.getChiaveFascicolo())) {
						retrieveParametersError = "Chiave fascicolo (ID fascicolo FEPA) non presente [SICOGE]";
						retrieveParametersErrorCode = 7;
						throw new RedException(retrieveParametersError);
					}

					if (StringUtils.isNullOrEmpty(parameters.getTipoFascicolo())) {
						retrieveParametersError = "Tipo fascicolo (tipo fascicolo FEPA) non presente [SICOGE]";
						retrieveParametersErrorCode = 8;
						throw new RedException(retrieveParametersError);
					}

					final FascicoloFepaEnum tffs = FascicoloFepaEnum.getEnumById(parameters.getTipoFascicolo());
					if (tffs == null) {
						retrieveParametersError = "Tipo fascicolo (tipo fascicolo FEPA) non censito [SICOGE]";
						retrieveParametersErrorCode = 9;
						throw new RedException(retrieveParametersError);
					}
				}

				if (parameters.getProtocollazioneCoda().getAoo() == null || StringUtils.isNullOrEmpty(parameters.getProtocollazioneCoda().getAoo().getCodiceAOO())) {
					retrieveParametersError = "Dati dell'AOO non presenti [SICOGE]";
					retrieveParametersErrorCode = 10;
					throw new RedException(retrieveParametersError);
				}

				// Flusso ORDINARIO, SILICE o AUT, CG2
			} else {
				if (CollectionUtils.isEmpty(parameters.getContestoProcedurale())) {
					retrieveParametersError = "Contesto procedurale non presente";
					retrieveParametersErrorCode = 11;
					throw new RedException(retrieveParametersError);
				} else {
					boolean contestoProceduralePresente = false;

					for (final WkfDatiContestoProceduraleType contestoProc : parameters.getContestoProcedurale()) {
						if (contestoProc.getContestoProcedurale().getTipoContestoProcedurale() != null
								&& TipoContestoProceduraleEnum.getEnumById(contestoProc.getContestoProcedurale().getTipoContestoProcedurale().getContent()) != null) {
							contestoProceduralePresente = true;
							break;
						}
					}

					if (!contestoProceduralePresente) {
						retrieveParametersError = "Nessuna corrispondenza trovata con i contesti procedurali gestiti da RED";
						retrieveParametersErrorCode = 12;
						throw new RedException(retrieveParametersError);
					}

					if (parameters.getMessaggioRicevuto() == null) {
						retrieveParametersError = "Messaggio di posta non presente";
						retrieveParametersErrorCode = 13;
						throw new RedException(retrieveParametersError);
					}

					final EmailMessageType messaggio = parameters.getMessaggioRicevuto();
					if (StringUtils.isNullOrEmpty(messaggio.getCodiMessaggio())) {
						retrieveParametersError = "Message-ID del messaggio di posta non presente";
						retrieveParametersErrorCode = 14;
						throw new RedException(retrieveParametersError);
					}

					if (messaggio.getCasellaEmail() == null || StringUtils.isNullOrEmpty(messaggio.getCasellaEmail().getIndirizzoEmail().getEmail())) {
						retrieveParametersError = "Casella e-mail ricevente del messaggio di posta non presente";
						retrieveParametersErrorCode = 15;
						throw new RedException(retrieveParametersError);
					}

					if (messaggio.getMittente() == null) {
						retrieveParametersError = "Mittente del messaggio di posta non presente";
						retrieveParametersErrorCode = 16;
						throw new RedException(retrieveParametersError);
					}

					if (CollectionUtils.isEmpty(messaggio.getDestinatari())) {
						retrieveParametersError = "Destinatari del messaggio di posta non presenti";
						retrieveParametersErrorCode = 17;
						throw new RedException(retrieveParametersError);
					}

					if (StringUtils.isNullOrEmpty(messaggio.getOggettoMessaggio())) {
						retrieveParametersError = "Oggetto del messaggio di posta non presente";
						retrieveParametersErrorCode = 18;
						throw new RedException(retrieveParametersError);
					}

					if (messaggio.getDataMessaggio() == null) {
						retrieveParametersError = "Data del messaggio di posta non presente";
						retrieveParametersErrorCode = 19;
						throw new RedException(retrieveParametersError);
					}
				}
			}

			if (parameters.getProtocollo() == null || parameters.getProtocollo().getProtocollo() == null) {
				retrieveParametersError = "Protocollo non presente";
				retrieveParametersErrorCode = 20;
				throw new RedException(retrieveParametersError);
			}

			final ProtIdentificatoreProtocolloType protocollo = parameters.getProtocollo().getProtocollo();
			if (StringUtils.isNullOrEmpty(protocollo.getRegistro().getAoo().getCodiceAOO())) {
				retrieveParametersError = "Codice AOO non presente nel protocollo";
				retrieveParametersErrorCode = 21;
				throw new RedException(retrieveParametersError);
			}

			if (StringUtils.isNullOrEmpty(protocollo.getIdProtocollo())) {
				retrieveParametersError = "Identificativo del protocollo non presente";
				retrieveParametersErrorCode = 22;
				throw new RedException(retrieveParametersError);
			}

			if (protocollo.getNumeroRegistrazione() < 1 || protocollo.getDataRegistrazione() == null) {
				retrieveParametersError = "Dati del protocollo non corretti";
				retrieveParametersErrorCode = 23;
				throw new RedException(retrieveParametersError);
			}

			if (protocollo.getTipoProtocollo() == null) {
				retrieveParametersError = "Tipo del protocollo non presente";
				retrieveParametersErrorCode = 24;
				throw new RedException(retrieveParametersError);
			}

			LOGGER.info(logRadix + "parametri in input formalmente corretti");

		} catch (final Exception e) {
			retrieveParamsError = handleParametersException(e, logRadix, retrieveParametersErrorCode, retrieveParametersError);
		}

		return retrieveParamsError;
	}

	/**
	 * Gestisce le eccezioni di validazione delle richieste ricevute dal Web
	 * Service.
	 * 
	 * @param e
	 * @param logRadix
	 * @param parametersErrorCode
	 * @param parametersError
	 * @throws GenericFault
	 */
	private static ErroreValidazioneDTO handleParametersException(final Exception e, final String logRadix, final int parametersErrorCode, final String parametersError) {
		LOGGER.error(logRadix, e);
		return new ErroreValidazioneDTO(parametersErrorCode, parametersError);
	}

	/**
	 * @param response
	 * @param errorCode
	 * @param description
	 */
	private void handleResponseElaborationError(final BaseServiceResponseType response, final int errorCode, final String description) {
		response.setEsito(EsitoType.ERRORE_ELABORAZIONE);

		final ServiceErrorType errorType = new ServiceErrorType();
		errorType.setErrorCode(errorCode);
		errorType.setErrorMessageString(description);
		response.getErrorList().add(errorType);

		LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
	}

	/**
	 * Ingressi Flusso ORDINARIO, SILICE e flusso SICOGE in errore. Se ho mail la salvo fra
	 * le mail rifiutate automaticamente, altrimenti ignoro.
	 * 
	 * @param headerAccesso
	 * @param parameters
	 * @return
	 * @throws SecurityFault
	 */
	public RispostaElaboraErroreProtocollazioneFlussoType elaboraErroreProtocollazioneFlusso(final AccessoApplicativo headerAccesso,
			final RichiestaElaboraErroreProtocollazioneFlussoType parameters) throws SecurityFault {
		final String messageId = headerAccesso.getMessageId();
		final Long idTrack = LoggingHandler.getIdTrack(context.getMessageContext());
		final String logRadix = "[Interfaccia_NotificheNPS_Flusso_SOAPImpl][elaboraErroreProtocollazioneFlusso][" + messageId + "][" + idTrack + "] ";

		LOGGER.info(logRadix + START_LABEL);

		final RispostaElaboraErroreProtocollazioneFlussoType response = ofMessages.createRispostaElaboraErroreProtocollazioneFlussoType();
		response.setMessageId(messageId);

		Security.checkSecurity(headerAccesso, "elaboraErroreProtocollazioneFlusso", logRadix, INVOKER_INTERFACCIA_NOTIFICHENPS_FLUSSO);

		try {

			response.setEsito(EsitoType.OK);
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);

		} catch (final Exception e) {
			LOGGER.error(logRadix, e);
			handleResponseElaborationError(response, GENERIC_ERROR_CODE, e.getMessage());
		}

		LOGGER.info(logRadix + "end");

		return response;
	}

	/**
	 * @throws GenericFault
	 */
	private void throwNotImplementedFault() throws GenericFault {
		final it.ibm.red.webservice.model.interfaccianps.headerfault.ServiceErrorType errorType = new it.ibm.red.webservice.model.interfaccianps.headerfault.ServiceErrorType();
		errorType.setErrorCode(-1);
		errorType.setErrorMessageString("Il metodo non è stato implementato");
		errorType.setSeverity(SeverityType.CRITICAL);
		final it.ibm.red.webservice.model.interfaccianps.headerfault.GenericFault genericFaultInfo = new it.ibm.red.webservice.model.interfaccianps.headerfault.GenericFault();
		genericFaultInfo.setErrorDetails(errorType);
		genericFaultInfo.setFAULTCODE("METHOD_NOT_IMPLEMENTED");
		throw new GenericFault("Il metodo non è stato implementato", genericFaultInfo);
	}

}