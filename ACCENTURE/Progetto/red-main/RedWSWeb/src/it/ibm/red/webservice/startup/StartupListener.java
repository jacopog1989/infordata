package it.ibm.red.webservice.startup;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.WebServiceClientProvider;

/**
 * Listener dello startup.
 */
public class StartupListener extends it.ibm.red.business.startup.BusinessStartupListener implements ServletContextListener {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(StartupListener.class.getName());
	
	@Override
	public final void contextDestroyed(final ServletContextEvent event) {
		LOGGER.info("STARTUP LISTENER contextDestroyed(): Listener context destroyed in esecuzione...");
	}
 
	@Override 
	public final void contextInitialized(final ServletContextEvent event) {
		ApplicationContextProvider.setApplicationContext(event);
		loadLoggingProperties(PropertiesNameEnum.RED_WS_LOGGING_PROPERTIES);
		WebServiceClientProvider.resetInstance();
		LOGGER.info("ServletContext implementation major version: " + event.getServletContext().getMajorVersion() + "." + event.getServletContext().getMinorVersion());
		LOGGER.info("ServletContext info: " + event.getServletContext().getServerInfo());
		LOGGER.info("STARTUP LISTENER contextInitialized(): Listener context initialized in esecuzione...");
	}
	
} 