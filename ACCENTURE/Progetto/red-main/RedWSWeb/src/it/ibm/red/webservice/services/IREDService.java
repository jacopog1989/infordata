package it.ibm.red.webservice.services;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import it.ibm.red.webservice.model.redservice.messages.v1.AnnullaDocumentoRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.AnnullaDocumentoResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.AssegnaFascicoloFatturaRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.AssegnaFascicoloFatturaResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.CheckInRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.CheckInResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.CheckOutRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.CheckOutResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.ElencoVersioniDocumentoRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.ElencoVersioniDocumentoResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.GetDocumentiFascicoloRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.GetDocumentiFascicoloResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.GetVersioneDocumentoRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.GetVersioneDocumentoResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.InserimentoFascicoloDecretoRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.InserimentoFascicoloDecretoResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.ModificaFascicoloDecretoRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.ModificaFascicoloDecretoResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.ModificaMetadatiRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.ModificaMetadatiResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.UndoCheckOutRequest;
import it.ibm.red.webservice.model.redservice.messages.v1.UndoCheckOutResponse;

@WebService(
		name = "REDService", 
		targetNamespace = "http://redwsevo.mef.gov.it/ibm/REDService")
public interface IREDService {

	/**
	 * Ottiene la versione del documento.
	 * @param getVersioneDocumentoRequest
	 * @return response del recupero della versione del documento
	 */
	@WebMethod(operationName = "getVersioneDocumento")
	@WebResult(name = "esito")
	GetVersioneDocumentoResponse getVersioneDocumento(
			@WebParam(name = "getVersioneDocumentoRequest")
			GetVersioneDocumentoRequest getVersioneDocumentoRequest);
	
	/**
	 * Modifica i metadati.
	 * @param modificaMetadatiRequest
	 * @return response di modifica dei metadati
	 */
	@WebMethod(operationName = "modificaMetadati")
	@WebResult(name = "esito")
	ModificaMetadatiResponse modificaMetadati(
			@WebParam(name = "modificaMetadatiRequest")
			ModificaMetadatiRequest modificaMetadatiRequest);
	
	/**
	 * Effettua il check out.
	 * @param checkOutRequest
	 * @return response del check out
	 */
	@WebMethod(operationName = "checkOut")
	@WebResult(name = "esito")
	CheckOutResponse checkOut(
			@WebParam(name = "checkOutRequest")
			CheckOutRequest checkOutRequest);
	
	/**
	 * Ottiene l'elenco delle versioni del documento.
	 * @param elencoVersioniDocumentoRequest
	 * @return response del recupero delle versioni del documento
	 */
	@WebMethod(operationName = "elencoVersioniDocumento")
	@WebResult(name = "esito")
	ElencoVersioniDocumentoResponse elencoVersioniDocumento(
			@WebParam(name = "elencoVersioniDocumentoRequest")
			ElencoVersioniDocumentoRequest elencoVersioniDocumentoRequest);
	
	/**
	 * Ottiene i documenti del fascicolo.
	 * @param getDocumentiFascicoloRequest
	 * @return response di recupero dei documenti del fascicolo
	 */
	@WebMethod(operationName = "getDocumentiFascicolo")
	@WebResult(name = "esito")
	GetDocumentiFascicoloResponse getDocumentiFascicolo(
			@WebParam(name = "getDocumentiFascicoloRequest")
			GetDocumentiFascicoloRequest getDocumentiFascicoloRequest);
	
	/**
	 * Inserisce il fascicolo decreto.
	 * @param inserimentoFascicoloDecretoRequest
	 * @return response di inserimento del fascicolo decreto
	 */
	@WebMethod(operationName = "inserimentoFascicoloDecreto")
	@WebResult(name = "esito")
	InserimentoFascicoloDecretoResponse inserimentoFascicoloDecreto(
			@WebParam(name = "inserimentoFascicoloDecretoRequest")
			InserimentoFascicoloDecretoRequest inserimentoFascicoloDecretoRequest);
	
	/**
	 * Assegna il fascicolo alla fattura.
	 * @param assegnaFascicoloFatturaRequest
	 * @return response di assegnazione del fascicolo alla fattura
	 */
	@WebMethod(operationName = "assegnaFascicoloFattura")
	@WebResult(name = "esito")
	AssegnaFascicoloFatturaResponse assegnaFascicoloFattura(
			@WebParam(name = "assegnaFascicoloFatturaRequest")
			AssegnaFascicoloFatturaRequest assegnaFascicoloFatturaRequest);
	
	/**
	 * Annulla il documento.
	 * @param annullaDocumentoRequest
	 * @return response di annullamento del documento
	 */
	@WebMethod(operationName = "annullaDocumento")
	@WebResult(name = "esito")
	AnnullaDocumentoResponse annullaDocumento(
			@WebParam(name = "annullaDocumentoRequest")
			AnnullaDocumentoRequest annullaDocumentoRequest);
	
	/**
	 * Effettua il check in.
	 * @param checkInRequest
	 * @return response del check in
	 */
	@WebMethod(operationName = "checkIn")
	@WebResult(name = "esito")
	CheckInResponse checkIn(
			@WebParam(name = "checkInRequest")
			CheckInRequest checkInRequest);

	/**
	 * Annulla il check in.
	 * @param undoCheckOutRequest
	 * @return response di annullamento del check in
	 */
	@WebMethod(operationName = "undoCheckOut")
	@WebResult(name = "esito")
	UndoCheckOutResponse undoCheckOut(
			@WebParam(name = "undoCheckOutRequest")
			UndoCheckOutRequest undoCheckOutRequest);
	
	/**
	 * Modifica il fascicolo decreto.
	 * @param modificaFascicoloDecretoRequest
	 * @return response di modifica del fascicolo decreto
	 */
	@WebMethod(operationName = "modificaFascicoloDecreto")
	@WebResult(name = "esito")
	ModificaFascicoloDecretoResponse modificaFascicoloDecreto(
			@WebParam(name = "modificaFascicoloDecretoRequest")
			ModificaFascicoloDecretoRequest modificaFascicoloDecretoRequest);
}