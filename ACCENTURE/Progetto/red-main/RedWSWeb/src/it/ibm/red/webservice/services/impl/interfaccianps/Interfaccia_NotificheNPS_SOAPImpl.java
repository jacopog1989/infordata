package it.ibm.red.webservice.services.impl.interfaccianps;

import java.util.Calendar;
import java.util.Date;

import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.xml.ws.WebServiceContext;

import org.springframework.util.CollectionUtils;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.ErroreValidazioneDTO;
import it.ibm.red.business.enums.EsitoValidazioneSegnaturaInteropEnum;
import it.ibm.red.business.enums.TipoProtocolloEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.INotificaNpsFacadeSRV;
import it.ibm.red.business.service.facade.IPostaNpsFacadeSRV;
import it.ibm.red.business.service.facade.IProtocolliEmergenzaFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.webservice.handlers.LoggingHandler;
import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.Segnatura;
import it.ibm.red.webservice.model.interfaccianps.dto.ParametersAzioneRispostaAutomatica;
import it.ibm.red.webservice.model.interfaccianps.dto.ParametersElaboraMessaggioProtocollazioneEmergenza;
import it.ibm.red.webservice.model.interfaccianps.dto.ParametersElaboraNotificaAzione;
import it.ibm.red.webservice.model.interfaccianps.dto.ParametersElaboraNotificaOperazione;
import it.ibm.red.webservice.model.interfaccianps.headeraccessoapplicativo.AccessoApplicativo;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.BaseServiceResponseType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.EsitoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraErroreProtocollazioneAutomaticaType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioPostaType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioProtocollazioneAutomaticaType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioProtocollazioneEmergenzaType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraNotificaAzioneType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraNotificaInteroperabilitaProtocolloType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraNotificaOperazioneType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraNotificaPECType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RispostaElaboraErroreProtocollazioneAutomaticaType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RispostaElaboraMessaggioPostaType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RispostaElaboraMessaggioProtocollazioneAutomaticaType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RispostaElaboraMessaggioProtocollazioneEmergenzaType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RispostaElaboraNotificaAzioneType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RispostaElaboraNotificaInteroperabilitaProtocolloType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RispostaElaboraNotificaOperazioneType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RispostaElaboraNotificaPECType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.EmailMessageType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.IdentificativoProtocolloRequestType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.OrgOrganigrammaType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.ProtIdentificatoreProtocolloCollegatoType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.ProtIdentificatoreProtocolloType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.ProtTipoProtocolloType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.WkfAzioneAggiornamentoCollegatiType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.WkfAzioneRispondiAProtocolloType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.WkfDatiNotificaErroreProtocollazioneAutomaticaType.NotificaInviata;
import it.ibm.red.webservice.model.interfaccianps.npstypes.WkfEsitoNotificaOperazioneType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.WkfTipoNotificaAzioneType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.WkfTipoNotificaOperazioneType;
import it.ibm.red.webservice.services.interfaccianps.SecurityFault;

@javax.jws.WebService(endpointInterface = "it.ibm.red.webservice.services.interfaccianps.InterfacciaNotificheNPS", targetNamespace = "http://mef.gov.it.v1.nps/servizi/InterfacciaNPS", serviceName = "Interfaccia_NotificheNPS", portName = "Interfaccia_NotificheNPS_SOAPPort")
@HandlerChain(file = "/handlers.xml")
public class Interfaccia_NotificheNPS_SOAPImpl {

	private static final String DATI_DEL_PROTOCOLLO_COLLEGATO_NUMERO = "Dati del protocollo collegato numero ";

	private static final String NON_VALIDO = ") non valido";

	/**
	 * Label inizio Log.
	 */
	private static final String START_LABEL = "start";

	/**
	 * Invoker del servizio.
	 */
	private static final String INVOKER_INTERFACCIA_NOTIFICHENPS = "Interfaccia_NotificheNPS";

	/**
	 * Messaggio di comunicazione input formalmente corretto.
	 */
	private static final String SUCCESS_PARAMETRI_FORMALMENTE_CORRETTI_MSG = "parametri in input formalmente corretti";

	/**
	 * Messaggio errore protocollo non presente.
	 */
	private static final String ERROR_PROTOCOLLO_ASSENTE_MSG = "Protocollo non presente";

	/**
	 * Messaggio errore: messaggio non presente.
	 */
	private static final String ERROR_MESSAGGIO_ASSENTE_MSG = "Messaggio non presente";

	/**
	 * Messaggio errore Message-ID non presente.
	 */
	private static final String ERROR_MESSAGE_ID_ASSENTE_MSG = "Message-ID del messaggio di posta non presente";

	/**
	 * Messaggio errore: ID messaggio non presente.
	 */
	private static final String ERROR_ID_MESSAGGIO_ASSENTE_MSG = "ID Messaggio non presente";

	/**
	 * Messaggio errore ID documento non presente.
	 */
	private static final String ERROR_ID_DOCUMENTO_ASSENTE_MSG = "ID Documento non presente";

	/**
	 * Messaggio errore codice AOO non presente.
	 */
	private static final String ERROR_CODICE_AOO_ASSENTE_MSG = "Codice AOO non presente nel protocollo";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(Interfaccia_NotificheNPS_SOAPImpl.class);

	/**
	 * Properties.
	 */
	private static final int GENERIC_ERROR_CODE = 99;

	/**
	 * Contesto.
	 */
	@Resource
	private WebServiceContext context;

	/**
	 * Messaggi.
	 */
	private it.ibm.red.webservice.model.interfaccianps.npsmessages.ObjectFactory ofMessages;

	/**
	 * Servizio.
	 */
	private IProtocolliEmergenzaFacadeSRV protocolliEmergenzaSRV;

	/**
	 * Servizio.
	 */
	private IPostaNpsFacadeSRV postaNpsSRV;

	/**
	 * Servizio.
	 */
	private INotificaNpsFacadeSRV notificaNpsSRV;

	/**
	 * Costruttore.
	 */
	public Interfaccia_NotificheNPS_SOAPImpl() {
		try {
			ofMessages = new it.ibm.red.webservice.model.interfaccianps.npsmessages.ObjectFactory();
			protocolliEmergenzaSRV = ApplicationContextProvider.getApplicationContext().getBean(IProtocolliEmergenzaFacadeSRV.class);
			postaNpsSRV = ApplicationContextProvider.getApplicationContext().getBean(IPostaNpsFacadeSRV.class);
			notificaNpsSRV = ApplicationContextProvider.getApplicationContext().getBean(INotificaNpsFacadeSRV.class);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di costruzione del servizio", e);
		}
	}

	/**
	 * @param headerAccesso
	 * @param parameters
	 * @return
	 * @throws SecurityFault
	 */
	public RispostaElaboraMessaggioPostaType elaboraMessaggioPosta(final AccessoApplicativo headerAccesso, final RichiestaElaboraMessaggioPostaType parameters)
			throws SecurityFault {
		final String messageId = headerAccesso.getMessageId();
		final Long idTrack = LoggingHandler.getIdTrack(context.getMessageContext());
		final String logRadix = "[Interfaccia_NotificheNPS][elaboraMessaggioPosta][" + messageId + "][" + idTrack + "] ";

		LOGGER.info(logRadix + START_LABEL);

		final RispostaElaboraMessaggioPostaType response = ofMessages.createRispostaElaboraMessaggioPostaType();
		response.setMessageId(messageId);

		Security.checkSecurity(headerAccesso, "elaboraMessaggioPosta", logRadix, INVOKER_INTERFACCIA_NOTIFICHENPS);

		final ErroreValidazioneDTO retrieveParamsError = checkParametersElaboraMessaggioPosta(parameters, logRadix);

		if (retrieveParamsError == null) {

			try {
				postaNpsSRV.accodaMessaggioPosta(headerAccesso.getMessageId(), parameters, idTrack);

				response.setEsito(EsitoType.OK);

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
			} catch (final Exception e) {
				LOGGER.error(logRadix, e);
				handleResponseElaborationError(response, GENERIC_ERROR_CODE, e.getMessage());
			}

		} else {
			handleResponseElaborationError(response, retrieveParamsError.getCodice(), retrieveParamsError.getDescrizione());
		}

		LOGGER.info(logRadix + "end");

		return response;
	}

	/**
	 * @param headerAccesso
	 * @param parameters
	 * @return
	 * @throws SecurityFault
	 */
	public RispostaElaboraNotificaPECType elaboraNotificaPEC(final AccessoApplicativo headerAccesso, final RichiestaElaboraNotificaPECType parameters)
			throws SecurityFault {
		final String messageId = headerAccesso.getMessageId();
		final Long idTrack = LoggingHandler.getIdTrack(context.getMessageContext());
		final String logRadix = "[Interfaccia_NotificheNPS][elaboraNotificaPEC][" + messageId + "][" + idTrack + "] ";

		LOGGER.info(logRadix + START_LABEL);

		final RispostaElaboraNotificaPECType response = ofMessages.createRispostaElaboraNotificaPECType();
		response.setMessageId(messageId);

		Security.checkSecurity(headerAccesso, "elaboraNotificaPEC", logRadix, INVOKER_INTERFACCIA_NOTIFICHENPS);

		final ErroreValidazioneDTO retrieveParamsError = checkParametersElaboraNotificaPEC(parameters, logRadix);

		if (retrieveParamsError == null) {

			try {
				postaNpsSRV.accodaNotificaPEC(headerAccesso.getMessageId(), parameters, idTrack);

				response.setEsito(EsitoType.OK);

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
			} catch (final Exception e) {
				LOGGER.error(logRadix, e);
				handleResponseElaborationError(response, GENERIC_ERROR_CODE, e.getMessage());
			}

		} else {
			handleResponseElaborationError(response, retrieveParamsError.getCodice(), retrieveParamsError.getDescrizione());
		}

		LOGGER.info(logRadix + "end");

		return response;
	}

	/**
	 * @param headerAccesso
	 * @param parameters
	 * @return
	 * @throws SecurityFault
	 */
	public RispostaElaboraNotificaInteroperabilitaProtocolloType elaboraNotificaInteroperabilitaProtocollo(final AccessoApplicativo headerAccesso,
			final RichiestaElaboraNotificaInteroperabilitaProtocolloType parameters) throws SecurityFault {
		final String messageId = headerAccesso.getMessageId();
		final Long idTrack = LoggingHandler.getIdTrack(context.getMessageContext());
		final String logRadix = "[Interfaccia_NotificheNPS][elaboraNotificaInteroperabilitaProtocollo][" + messageId + "][" + idTrack + "] ";

		LOGGER.info(logRadix + START_LABEL);

		final RispostaElaboraNotificaInteroperabilitaProtocolloType response = ofMessages.createRispostaElaboraNotificaInteroperabilitaProtocolloType();
		response.setMessageId(messageId);

		Security.checkSecurity(headerAccesso, "elaboraNotificaInteroperabilitaProtocollo", logRadix, INVOKER_INTERFACCIA_NOTIFICHENPS);

		final ErroreValidazioneDTO retrieveParamsError = checkParametersElaboraNotificaInteroperabilitaProtocollo(parameters, logRadix);

		if (retrieveParamsError == null) {

			try {
				postaNpsSRV.accodaNotificaInteroperabilita(headerAccesso.getMessageId(), parameters, idTrack);

				response.setEsito(EsitoType.OK);

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
			} catch (final Exception e) {
				LOGGER.error(logRadix, e);
				handleResponseElaborationError(response, GENERIC_ERROR_CODE, e.getMessage());
			}

		} else {
			handleResponseElaborationError(response, retrieveParamsError.getCodice(), retrieveParamsError.getDescrizione());
		}

		LOGGER.info(logRadix + "end");

		return response;
	}

	/**
	 * @param headerAccesso
	 * @param parameters
	 * @return
	 * @throws SecurityFault
	 */
	public RispostaElaboraNotificaOperazioneType elaboraNotificaOperazione(final AccessoApplicativo headerAccesso, final RichiestaElaboraNotificaOperazioneType parameters)
			throws SecurityFault {
		final String messageId = headerAccesso.getMessageId();
		final Long idTrack = LoggingHandler.getIdTrack(context.getMessageContext());
		final String logRadix = "[Interfaccia_NotificheNPS][elaboraNotificaOperazione][" + messageId + "][" + idTrack + "] ";

		LOGGER.info(logRadix + START_LABEL);

		final RispostaElaboraNotificaOperazioneType response = ofMessages.createRispostaElaboraNotificaOperazioneType();
		response.setMessageId(messageId);

		Security.checkSecurity(headerAccesso, "elaboraNotificaOperazione", logRadix, INVOKER_INTERFACCIA_NOTIFICHENPS);

		final ParametersElaboraNotificaOperazione parametersDTO = new ParametersElaboraNotificaOperazione(headerAccesso.getMessageId());

		ErroreValidazioneDTO retrieveParamsError = null;
		if (parameters.getProtocollo() != null) {
			retrieveParamsError = checkParametersElaboraNotificaOperazione(parameters, parametersDTO, logRadix);
		}

		if (retrieveParamsError == null) {

			try {
				if (parameters.getProtocollo() != null) {
					notificaNpsSRV.accodaNotificaOperazione(parametersDTO);
				} else {
					final String codiMessaggio = parameters.getCodiMessaggio().replace(".nps.tesoro.it", "");
					notificaNpsSRV.updateStatoAndManageEmailByCodiMessage(codiMessaggio, messageId);
				}

				response.setEsito(EsitoType.OK);
				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
			} catch (final Exception e) {
				LOGGER.error(logRadix, e);
				handleResponseElaborationError(response, GENERIC_ERROR_CODE, e.getMessage());
			}

		} else {
			handleResponseElaborationError(response, retrieveParamsError.getCodice(), retrieveParamsError.getDescrizione());
		}

		LOGGER.info(logRadix + "end");

		return response;
	}

	/**
	 * @param headerAccesso
	 * @param parameters
	 * @return
	 * @throws SecurityFault
	 */
	public RispostaElaboraMessaggioProtocollazioneEmergenzaType elaboraMessaggioProtocollazioneEmergenza(final AccessoApplicativo headerAccesso,
			final RichiestaElaboraMessaggioProtocollazioneEmergenzaType parameters) throws SecurityFault {
		final String messageId = headerAccesso.getMessageId();
		final Long idTrack = LoggingHandler.getIdTrack(context.getMessageContext());
		final String logRadix = "[Interfaccia_NotificheNPS][elaboraMessaggioProtocollazioneEmergenza][" + messageId + "][" + idTrack + "] ";

		LOGGER.info(logRadix + START_LABEL);

		final RispostaElaboraMessaggioProtocollazioneEmergenzaType response = ofMessages.createRispostaElaboraMessaggioProtocollazioneEmergenzaType();
		response.setMessageId(messageId);

		Security.checkSecurity(headerAccesso, "elaboraMessaggioProtocollazioneEmergenza", logRadix, INVOKER_INTERFACCIA_NOTIFICHENPS);

		final ParametersElaboraMessaggioProtocollazioneEmergenza parametersDTO = new ParametersElaboraMessaggioProtocollazioneEmergenza(headerAccesso.getMessageId());
		final ErroreValidazioneDTO retrieveParamsError = checkParameters(parameters, parametersDTO, logRadix);

		if (retrieveParamsError == null) {

			try {
				// 1) Si estraggono i dati dai parametri e si memorizzano nella base dati

				final String errorMessage = protocolliEmergenzaSRV.riconciliaDatiProtocolloUfficiale(parametersDTO);
				if (StringUtils.isNullOrEmpty(errorMessage)) {

					response.setEsito(EsitoType.OK);

					LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
				} else {

					EsitoType esito = EsitoType.fromValue(errorMessage);
					if (esito == null) {
						esito = EsitoType.ERRORE_ELABORAZIONE;
					}

					response.setEsito(esito);

					LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
				}
			} catch (final Exception e) {
				LOGGER.error(logRadix, e);
				handleResponseElaborationError(response, GENERIC_ERROR_CODE, e.getMessage());
			}

		} else {
			handleResponseElaborationError(response, retrieveParamsError.getCodice(), retrieveParamsError.getDescrizione());
		}

		LOGGER.info(logRadix + "end");

		return response;
	}

	/**
	 * @param headerAccesso
	 * @param parameters
	 * @return
	 * @throws SecurityFault
	 */
	public RispostaElaboraMessaggioProtocollazioneAutomaticaType elaboraMessaggioProtocollazioneAutomatica(final AccessoApplicativo headerAccesso,
			final RichiestaElaboraMessaggioProtocollazioneAutomaticaType parameters) throws SecurityFault {
		final String messageId = headerAccesso.getMessageId();
		final Long idTrack = LoggingHandler.getIdTrack(context.getMessageContext());
		final String logRadix = "[Interfaccia_NotificheNPS][elaboraMessaggioProtocollazioneAutomatica][" + messageId + "][" + idTrack + "] ";

		LOGGER.info(logRadix + START_LABEL);

		final RispostaElaboraMessaggioProtocollazioneAutomaticaType response = ofMessages.createRispostaElaboraMessaggioProtocollazioneAutomaticaType();
		response.setMessageId(messageId);

		Security.checkSecurity(headerAccesso, "elaboraMessaggioProtocollazioneAutomatica", logRadix, INVOKER_INTERFACCIA_NOTIFICHENPS);

		final ErroreValidazioneDTO retrieveParamsError = checkParametersElaboraMessaggioProtocollazioneAutomatica(parameters, logRadix);

		if (retrieveParamsError == null) {

			try {
				postaNpsSRV.accodaMessaggioProtocollazioneAutomatica(headerAccesso.getMessageId(), parameters, idTrack);

				response.setEsito(EsitoType.OK);

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
			} catch (final Exception e) {
				LOGGER.error(logRadix, e);
				handleResponseElaborationError(response, GENERIC_ERROR_CODE, e.getMessage());
			}

		} else {
			handleResponseElaborationError(response, retrieveParamsError.getCodice(), retrieveParamsError.getDescrizione());
		}

		LOGGER.info(logRadix + "end");

		return response;
	}

	/**
	 * @param headerAccesso
	 * @param parameters
	 * @return
	 * @throws SecurityFault
	 */
	public RispostaElaboraNotificaAzioneType elaboraNotificaAzione(final AccessoApplicativo headerAccesso, final RichiestaElaboraNotificaAzioneType parameters)
			throws SecurityFault {
		final String messageId = headerAccesso.getMessageId();
		final Long idTrack = LoggingHandler.getIdTrack(context.getMessageContext());
		final String logRadix = "[Interfaccia_NotificheNPS][elaboraNotificaAzione][" + messageId + "][" + idTrack + "] ";

		LOGGER.info(logRadix + START_LABEL);

		final RispostaElaboraNotificaAzioneType response = ofMessages.createRispostaElaboraNotificaAzioneType();
		response.setMessageId(messageId);

		Security.checkSecurity(headerAccesso, "elaboraNotificaAzione", logRadix, INVOKER_INTERFACCIA_NOTIFICHENPS);

		final ParametersElaboraNotificaAzione parametersDTO = new ParametersElaboraNotificaAzione(headerAccesso.getMessageId());
		final ErroreValidazioneDTO retrieveParamsError = checkParametersElaboraNotificaAzione(parameters, parametersDTO, logRadix);

		if (retrieveParamsError == null) {

			try {
				notificaNpsSRV.accodaNotificaAzione(parametersDTO, idTrack);

				response.setEsito(EsitoType.OK);

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
			} catch (final Exception e) {
				LOGGER.error(logRadix, e);
				handleResponseElaborationError(response, GENERIC_ERROR_CODE, e.getMessage());
			}

		} else {
			handleResponseElaborationError(response, retrieveParamsError.getCodice(), retrieveParamsError.getDescrizione());
		}

		LOGGER.info(logRadix + "end");

		return response;
	}

	/**
	 * @param headerAccesso
	 * @param parameters
	 * @return
	 * @throws SecurityFault
	 */
	public RispostaElaboraErroreProtocollazioneAutomaticaType elaboraErroreProtocollazioneAutomatica(final AccessoApplicativo headerAccesso,
			final RichiestaElaboraErroreProtocollazioneAutomaticaType parameters) throws SecurityFault {
		final String messageId = headerAccesso.getMessageId();
		final Long idTrack = LoggingHandler.getIdTrack(context.getMessageContext());
		final String logRadix = "[Interfaccia_NotificheNPS][elaboraErroreProtocollazioneAutomatica][" + messageId + "][" + idTrack + "] ";

		LOGGER.info(logRadix + START_LABEL);

		final RispostaElaboraErroreProtocollazioneAutomaticaType response = ofMessages.createRispostaElaboraErroreProtocollazioneAutomaticaType();
		response.setMessageId(messageId);

		Security.checkSecurity(headerAccesso, "elaboraErroreProtocollazioneAutomatica", logRadix, INVOKER_INTERFACCIA_NOTIFICHENPS);

		final ErroreValidazioneDTO retrieveParamsError = checkParametersElaboraNotificaErroreProtocollazioneAutomatica(parameters, logRadix);

		if (retrieveParamsError == null) {

			try {
				postaNpsSRV.accodaNotificaErroreProtocollazioneAutomatica(headerAccesso.getMessageId(), parameters, idTrack);

				response.setEsito(EsitoType.OK);

				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
			} catch (final Exception e) {
				LOGGER.error(logRadix, e);
				handleResponseElaborationError(response, GENERIC_ERROR_CODE, e.getMessage());
			}

		} else {
			handleResponseElaborationError(response, retrieveParamsError.getCodice(), retrieveParamsError.getDescrizione());
		}

		LOGGER.info(logRadix + "end");

		return response;
	}

	private static ErroreValidazioneDTO checkParameters(final RichiestaElaboraMessaggioProtocollazioneEmergenzaType parameters,
			final ParametersElaboraMessaggioProtocollazioneEmergenza parametersDTO, final String logRadix) {
		ErroreValidazioneDTO retrieveParamsError = null;
		String retrieveParametersError = null;
		int retrieveParametersErrorCode = 0;

		try {
			retrieveParametersError = "Impossibile recuperare il codice AOO";
			retrieveParametersErrorCode = 1;
			final String codiceAooNPS = parameters.getProtocolloEmergenza().getProtocollo().getRegistro().getAoo().getCodiceAOO();
			if (StringUtils.isNullOrEmpty(codiceAooNPS)) {
				throw new RedException("codice AOO vuoto");
			}

			LOGGER.info(logRadix + "codiceAooNPS " + codiceAooNPS);

			retrieveParametersError = "Impossibile recuperare il numero di protocollo di emergenza";
			retrieveParametersErrorCode = 2;
			final int numeroProtocolloEmergenza = parameters.getProtocolloEmergenza().getProtocollo().getNumeroRegistrazione();
			if (numeroProtocolloEmergenza <= 0) {
				throw new RedException("numero di protocollo di emergenza (" + numeroProtocolloEmergenza + NON_VALIDO);
			}

			LOGGER.info(logRadix + "numeroProtocolloEmergenza " + numeroProtocolloEmergenza);

			retrieveParametersError = "Impossibile recuperare la data di registrazione del protocollo di emergenza";
			retrieveParametersErrorCode = 3;
			final Date dataProtocolloEmergenza = parameters.getProtocolloEmergenza().getProtocollo().getDataRegistrazione().toGregorianCalendar().getTime();
			if (dataProtocolloEmergenza == null) {
				throw new RedException("data registrazione del protocollo di emergenza assente");
			}

			LOGGER.info(logRadix + "dataProtocollazioneEmergenza " + dataProtocolloEmergenza);

			retrieveParametersError = "Impossibile recuperare l'anno di registrazione del protocollo di emergenza";
			retrieveParametersErrorCode = 4;
			final Calendar cal = Calendar.getInstance();
			cal.setTime(dataProtocolloEmergenza);
			final int annoProtocolloEmergenza = cal.get(Calendar.YEAR);
			if (annoProtocolloEmergenza <= 0) {
				throw new RedException("anno protocollo (" + annoProtocolloEmergenza + NON_VALIDO);
			}

			LOGGER.info(logRadix + "annoProtocollazioneEmergenza " + annoProtocolloEmergenza);

			retrieveParametersError = "Impossibile recuperare l'id del protocollo ufficiale";
			retrieveParametersErrorCode = 5;
			final String idProtocolloUfficiale = parameters.getProtocollo().getProtocollo().getIdProtocollo();
			if (StringUtils.isNullOrEmpty(idProtocolloUfficiale)) {
				throw new RedException("idProtocolloUfficiale vuoto");
			}

			LOGGER.info(logRadix + "idProtocolloUfficiale " + idProtocolloUfficiale);

			retrieveParametersError = "Impossibile recuperare l'id del messaggio NPS";
			retrieveParametersErrorCode = 6;
			final String idMessaggioNPS = parameters.getIdMessaggio();
			if (StringUtils.isNullOrEmpty(idMessaggioNPS)) {
				throw new RedException("id messaggio NPS vuoto");
			}

			LOGGER.info(logRadix + "idMessaggioNPS " + idMessaggioNPS);

			retrieveParametersError = "Impossibile recuperare la tipologia del protocollo";
			retrieveParametersErrorCode = 7;
			final ProtTipoProtocolloType tipoProtocollo = parameters.getProtocolloEmergenza().getProtocollo().getTipoProtocollo();
			if (tipoProtocollo == null) {
				throw new RedException("tipo protocollo vuoto");
			}

			LOGGER.info(logRadix + "tipoProtocollo " + tipoProtocollo);

			retrieveParametersError = null;

			parametersDTO.setIdMessaggioNPS(idMessaggioNPS);
			parametersDTO.setCodiceAooNPS(codiceAooNPS);
			parametersDTO.setNumeroProtocolloEmergenza(numeroProtocolloEmergenza);
			parametersDTO.setDataProtocolloEmergenza(dataProtocolloEmergenza);
			parametersDTO.setAnnoProtocolloEmergenza(annoProtocolloEmergenza);
			parametersDTO.setIdProtocolloUfficiale(idProtocolloUfficiale);
			parametersDTO.setEntrata(tipoProtocollo.equals(ProtTipoProtocolloType.ENTRATA));

			LOGGER.info(logRadix + SUCCESS_PARAMETRI_FORMALMENTE_CORRETTI_MSG);

		} catch (final Exception e) {
			retrieveParamsError = handleParametersException(e, logRadix, retrieveParametersErrorCode, retrieveParametersError);
		}

		return retrieveParamsError;
	}

	private static ErroreValidazioneDTO checkParametersElaboraMessaggioPosta(final RichiestaElaboraMessaggioPostaType parameters, final String logRadix) {
		ErroreValidazioneDTO retrieveParamsError = null;
		String retrieveParametersError = null;
		int retrieveParametersErrorCode = 0;

		try {
			if (StringUtils.isNullOrEmpty(parameters.getIdMessaggio())) {
				retrieveParametersError = ERROR_ID_MESSAGGIO_ASSENTE_MSG;
				retrieveParametersErrorCode = 1;
				throw new RedException(retrieveParametersError);
			}

			if (StringUtils.isNullOrEmpty(parameters.getIdDocumento())) {
				retrieveParametersError = ERROR_ID_DOCUMENTO_ASSENTE_MSG;
				retrieveParametersErrorCode = 2;
				throw new RedException(retrieveParametersError);
			}

			if (parameters.getMessaggio() == null) {
				retrieveParametersError = ERROR_MESSAGGIO_ASSENTE_MSG;
				retrieveParametersErrorCode = 3;
				throw new RedException(retrieveParametersError);
			}

			if (StringUtils.isNullOrEmpty(parameters.getMessaggio().getCodiMessaggio())) {
				retrieveParametersError = ERROR_MESSAGE_ID_ASSENTE_MSG;
				retrieveParametersErrorCode = 4;
				throw new RedException(retrieveParametersError);
			}

			EsitoValidazioneSegnaturaInteropEnum esitoEnum = EsitoValidazioneSegnaturaInteropEnum.ASSENTE;
			if (parameters.getValidazioneMessaggio() != null && parameters.getValidazioneMessaggio().getValidazioneSegnatura() != null) {
				esitoEnum = EsitoValidazioneSegnaturaInteropEnum.get(parameters.getValidazioneMessaggio().getValidazioneSegnatura().getEsitoValidazione());
			}

			if (parameters.getDatiSegnatura() != null && EsitoValidazioneSegnaturaInteropEnum.VALIDA.equals(esitoEnum)) {
				final Segnatura segnatura = parameters.getDatiSegnatura();

				if (segnatura.getIntestazione().getOrigine() == null) {
					retrieveParametersError = "Origine non presente nella segnatura";
					retrieveParametersErrorCode = 5;
					throw new RedException(retrieveParametersError);
				}

				if (segnatura.getIntestazione().getOrigine().getMittente() == null) {
					retrieveParametersError = "Mittente non presente nella segnatura";
					retrieveParametersErrorCode = 6;
					throw new RedException(retrieveParametersError);
				}

				if (segnatura.getIntestazione().getDestinazione() == null) {
					retrieveParametersError = "Destinazione non presente nella segnatura";
					retrieveParametersErrorCode = 7;
					throw new RedException(retrieveParametersError);
				}

				if (segnatura.getIntestazione().getOggetto() == null || Constants.EMPTY_STRING.equals(segnatura.getIntestazione().getOggetto().getContent())) {
					retrieveParametersError = "Oggetto non presente nella segnatura";
					retrieveParametersErrorCode = 8;
					throw new RedException(retrieveParametersError);
				}

				if (segnatura.getIntestazione().getOrigine().getIndirizzoTelematico() == null
						|| Constants.EMPTY_STRING.equals(segnatura.getIntestazione().getOrigine().getIndirizzoTelematico().getContent())) {
					retrieveParametersError = "Indirizzo telematico dell'origine non presente nella segnatura";
					retrieveParametersErrorCode = 9;
					throw new RedException(retrieveParametersError);
				}

				if (segnatura.getDescrizione().getDocumento() == null && segnatura.getDescrizione().getTestoDelMessaggio() == null) {
					retrieveParametersError = "La segnatura non specifica qual è il documento principale";
					retrieveParametersErrorCode = 10;
					throw new RedException(retrieveParametersError);
				}
			}

			LOGGER.info(logRadix + SUCCESS_PARAMETRI_FORMALMENTE_CORRETTI_MSG);

		} catch (final Exception e) {
			retrieveParamsError = handleParametersException(e, logRadix, retrieveParametersErrorCode, retrieveParametersError);
		}

		return retrieveParamsError;
	}

	private static ErroreValidazioneDTO checkParametersElaboraNotificaPEC(final RichiestaElaboraNotificaPECType parameters, final String logRadix) {
		ErroreValidazioneDTO retrieveParamsError = null;
		String retrieveParametersError = null;
		int retrieveParametersErrorCode = 0;

		try {
			if (StringUtils.isNullOrEmpty(parameters.getIdDocumento())) {
				retrieveParametersError = ERROR_ID_DOCUMENTO_ASSENTE_MSG;
				retrieveParametersErrorCode = 1;
				throw new RedException(retrieveParametersError);
			}

			if (parameters.getMessaggio() == null) {
				retrieveParametersError = ERROR_MESSAGGIO_ASSENTE_MSG;
				retrieveParametersErrorCode = 2;
				throw new RedException(retrieveParametersError);
			}

			if (parameters.getDatiCert() == null) {
				retrieveParametersError = "Daticert non presente";
				retrieveParametersErrorCode = 3;
				throw new RedException(retrieveParametersError);
			}

			if (parameters.getTipoNotificaPEC() == null) {
				retrieveParametersError = "Tipo di notifica PEC non presente";
				retrieveParametersErrorCode = 4;
				throw new RedException(retrieveParametersError);
			}

			if (parameters.getProtocollo() != null) {
				if (StringUtils.isNullOrEmpty(parameters.getProtocollo().getRegistro().getAoo().getCodiceAOO())) {
					retrieveParametersError = ERROR_CODICE_AOO_ASSENTE_MSG;
					retrieveParametersErrorCode = 5;
					throw new RedException(retrieveParametersError);
				}

				if (parameters.getProtocollo().getNumeroRegistrazione() < 1 || parameters.getProtocollo().getDataRegistrazione() == null
						|| parameters.getProtocollo().getTipoProtocollo() == null) {
					retrieveParametersError = "Dati del protocollo non presenti";
					retrieveParametersErrorCode = 6;
					throw new RedException(retrieveParametersError);
				}

				if (parameters.getProtocollo().getRegistro() == null) {
					retrieveParametersError = "Dati del protocollo non corretti";
					retrieveParametersErrorCode = 7;
					throw new RedException(retrieveParametersError);
				}
			}

			LOGGER.info(logRadix + SUCCESS_PARAMETRI_FORMALMENTE_CORRETTI_MSG);

		} catch (final Exception e) {
			retrieveParamsError = handleParametersException(e, logRadix, retrieveParametersErrorCode, retrieveParametersError);
		}

		return retrieveParamsError;
	}

	private static ErroreValidazioneDTO checkParametersElaboraNotificaOperazione(final RichiestaElaboraNotificaOperazioneType parameters,
			final ParametersElaboraNotificaOperazione parametersDTO, final String logRadix) {
		ErroreValidazioneDTO retriveParamsError = null;
		String retrieveParametersError = null;
		int retrieveParametersErrorCode = 0;
		try {
			retrieveParametersError = "Impossibile recuperare il codice AOO";
			retrieveParametersErrorCode = 1;
			final String codiceAooNPS = parameters.getProtocollo().getRegistro().getAoo().getCodiceAOO();
			if (StringUtils.isNullOrEmpty(codiceAooNPS)) {
				throw new RedException("codice AOO vuoto");
			}

			LOGGER.info(logRadix + "codiceAooNPS " + codiceAooNPS);

			retrieveParametersError = "Impossibile recuperare il numero di protocollo";
			retrieveParametersErrorCode = 2;
			final int numeroProtocollo = parameters.getProtocollo().getNumeroRegistrazione();
			if (numeroProtocollo <= 0) {
				throw new RedException("numero di protocollo (" + numeroProtocollo + NON_VALIDO);
			}

			LOGGER.info(logRadix + "numeroProtocollo " + numeroProtocollo);

			retrieveParametersError = "Impossibile recuperare la data di registrazione del protocollo";
			retrieveParametersErrorCode = 3;
			final Date dataProtocollo = parameters.getProtocollo().getDataRegistrazione().toGregorianCalendar().getTime();
			if (dataProtocollo == null) {
				throw new RedException("data registrazione del protocollo assente");
			}

			LOGGER.info(logRadix + "dataProtocollazione " + dataProtocollo);

			retrieveParametersError = "Impossibile recuperare l'anno di registrazione del protocollo";
			retrieveParametersErrorCode = 4;
			final Calendar cal = Calendar.getInstance();
			cal.setTime(dataProtocollo);
			final int annoProtocollo = cal.get(Calendar.YEAR);
			if (annoProtocollo <= 0) {
				throw new RedException("anno protocollo (" + annoProtocollo + NON_VALIDO);
			}

			LOGGER.info(logRadix + "annoProtocollazione " + annoProtocollo);

			retrieveParametersError = "Impossibile recuperare l'id del protocollo ufficiale";
			retrieveParametersErrorCode = 5;
			final String idProtocollo = parameters.getProtocollo().getIdProtocollo();
			if (StringUtils.isNullOrEmpty(idProtocollo)) {
				throw new RedException("idProtocollo vuoto");
			}

			LOGGER.info(logRadix + "idProtocollo " + idProtocollo);

			retrieveParametersError = "Impossibile recuperare l'id del messaggio NPS";
			retrieveParametersErrorCode = 6;
			final String idMessaggioNPS = parameters.getIdMessaggio();
			if (StringUtils.isNullOrEmpty(idMessaggioNPS)) {
				throw new RedException("id messaggio NPS vuoto");
			}

			LOGGER.info(logRadix + "idMessaggioNPS " + idMessaggioNPS);

			retrieveParametersError = "Impossibile recuperare la tipologia del protocollo";
			retrieveParametersErrorCode = 7;
			final ProtTipoProtocolloType tipoProtocollo = parameters.getProtocollo().getTipoProtocollo();
			if (tipoProtocollo == null) {
				throw new RedException("tipo protocollo vuoto");
			}

			LOGGER.info(logRadix + "tipoProtocollo " + tipoProtocollo);

			retrieveParametersError = "Impossibile recuperare il tipo di notifica";
			retrieveParametersErrorCode = 8;
			final WkfTipoNotificaOperazioneType tipoNotifica = parameters.getTipoNotifica();
			if (!WkfTipoNotificaOperazioneType.SPEDIZIONE.equals(tipoNotifica)) {
				throw new RedException("tipo notifica non gestita");
			}

			retrieveParametersError = "Impossibile recuperare il tipo di esito";
			retrieveParametersErrorCode = 9;
			final WkfEsitoNotificaOperazioneType tipoEsito = parameters.getTipoEsito();
			if (!WkfEsitoNotificaOperazioneType.SPEDIZIONE_DESTINATARIO.equals(tipoEsito)) {
				throw new RedException("tipo esito non gestito");
			}

			retrieveParametersError = "Impossibile recuperare la data della notifica";
			retrieveParametersErrorCode = 11;
			final Date dataNotifica = parameters.getDataNotifica().toGregorianCalendar().getTime();
			if (dataNotifica == null) {
				throw new RedException("data notifica vuota");
			}

			retrieveParametersError = "Impossibile recuperare il destinatario";
			retrieveParametersErrorCode = 12;
			final String emailDestinatario = parameters.getProtocollo().getDestinatario().getEmail();
			if (StringUtils.isNullOrEmpty(emailDestinatario)) {
				throw new RedException("Email destinatario vuota");
			}

			retrieveParametersError = "Impossibile recuperare il message id della mail spedita";
			retrieveParametersErrorCode = 13;
			final String messageIdMail = parameters.getCodiMessaggio();
			if (StringUtils.isNullOrEmpty(messageIdMail)) {
				throw new RedException("message id della mail spedita vuoto");
			}

			retrieveParametersError = null;

			parametersDTO.setIdMessaggioNPS(idMessaggioNPS);
			parametersDTO.setCodiceAooNPS(codiceAooNPS);
			parametersDTO.setNumeroProtocollo(numeroProtocollo);
			parametersDTO.setDataProtocollo(dataProtocollo);
			parametersDTO.setAnnoProtocollo(annoProtocollo);
			parametersDTO.setIdProtocollo(idProtocollo);
			parametersDTO.setEntrata(tipoProtocollo.equals(ProtTipoProtocolloType.ENTRATA));
			parametersDTO.setDataNotifica(dataNotifica);
			parametersDTO.setEsitoNotifica(tipoEsito.toString());
			parametersDTO.setMessageIdMail(messageIdMail);
			parametersDTO.setTipoNotifica(tipoNotifica.toString());
			parametersDTO.setEmailDestinatario(emailDestinatario);

			LOGGER.info(logRadix + SUCCESS_PARAMETRI_FORMALMENTE_CORRETTI_MSG);

		} catch (final Exception e) {
			retriveParamsError = handleParametersException(e, logRadix, retrieveParametersErrorCode, retrieveParametersError);
		}

		return retriveParamsError;
	}

	private static ErroreValidazioneDTO checkParametersElaboraNotificaInteroperabilitaProtocollo(final RichiestaElaboraNotificaInteroperabilitaProtocolloType parameters,
			final String logRadix) {
		ErroreValidazioneDTO retrieveParamsError = null;
		String retrieveParametersError = null;
		int retrieveParametersErrorCode = 0;

		try {
			if (StringUtils.isNullOrEmpty(parameters.getIdMessaggio())) {
				retrieveParametersError = ERROR_ID_MESSAGGIO_ASSENTE_MSG;
				retrieveParametersErrorCode = 1;
				throw new RedException(retrieveParametersError);
			}

			if (StringUtils.isNullOrEmpty(parameters.getIdDocumento())) {
				retrieveParametersError = ERROR_ID_DOCUMENTO_ASSENTE_MSG;
				retrieveParametersErrorCode = 2;
				throw new RedException(retrieveParametersError);
			}

			if (parameters.getMessaggioRicevuto() == null) {
				retrieveParametersError = ERROR_MESSAGGIO_ASSENTE_MSG;
				retrieveParametersErrorCode = 3;
				throw new RedException(retrieveParametersError);
			}

			if (parameters.getProtocollo() == null || parameters.getProtocollo().getProtocolloMittente() == null) {
				retrieveParametersError = ERROR_PROTOCOLLO_ASSENTE_MSG;
				retrieveParametersErrorCode = 4;
				throw new RedException(retrieveParametersError);
			}

			final ProtIdentificatoreProtocolloType protocollo = parameters.getProtocollo().getProtocolloMittente();
			if (StringUtils.isNullOrEmpty(protocollo.getRegistro().getAoo().getCodiceAOO())) {
				retrieveParametersError = ERROR_CODICE_AOO_ASSENTE_MSG;
				retrieveParametersErrorCode = 5;
				throw new RedException(retrieveParametersError);
			}

			if (StringUtils.isNullOrEmpty(protocollo.getIdProtocollo())) {
				retrieveParametersError = "Identificativo del protocollo non presente";
				retrieveParametersErrorCode = 6;
				throw new RedException(retrieveParametersError);
			}

			if (protocollo.getNumeroRegistrazione() < 1 || protocollo.getDataRegistrazione() == null) {
				retrieveParametersError = "Dati del protocollo non corretti";
				retrieveParametersErrorCode = 7;
				throw new RedException(retrieveParametersError);
			}

			if (parameters.getTipoNotificaInteroperabile() == null) {
				retrieveParametersError = "Tipo di notifica di interoperabilita non presente";
				retrieveParametersErrorCode = 8;
				throw new RedException(retrieveParametersError);
			}

			LOGGER.info(logRadix + SUCCESS_PARAMETRI_FORMALMENTE_CORRETTI_MSG);

		} catch (final Exception e) {
			retrieveParamsError = handleParametersException(e, logRadix, retrieveParametersErrorCode, retrieveParametersError);
		}

		return retrieveParamsError;
	}

	private ErroreValidazioneDTO checkParametersElaboraMessaggioProtocollazioneAutomatica(final RichiestaElaboraMessaggioProtocollazioneAutomaticaType parameters,
			final String logRadix) {
		ErroreValidazioneDTO retrieveParamsError = null;
		String retrieveParametersError = null;
		int retrieveParametersErrorCode = 0;

		try {
			if (StringUtils.isNullOrEmpty(parameters.getIdMessaggio())) {
				retrieveParametersError = ERROR_ID_MESSAGGIO_ASSENTE_MSG;
				retrieveParametersErrorCode = 1;
				throw new RedException(retrieveParametersError);
			}

			if (StringUtils.isNullOrEmpty(parameters.getIdDocumento())) {
				retrieveParametersError = ERROR_ID_DOCUMENTO_ASSENTE_MSG;
				retrieveParametersErrorCode = 2;
				throw new RedException(retrieveParametersError);
			}

			if (parameters.getProtocollo() == null || parameters.getProtocollo().getProtocollo() == null) {
				retrieveParametersError = ERROR_PROTOCOLLO_ASSENTE_MSG;
				retrieveParametersErrorCode = 3;
				throw new RedException(retrieveParametersError);
			}

			final ProtIdentificatoreProtocolloType protocollo = parameters.getProtocollo().getProtocollo();
			if (StringUtils.isNullOrEmpty(protocollo.getRegistro().getAoo().getCodiceAOO())) {
				retrieveParametersError = ERROR_CODICE_AOO_ASSENTE_MSG;
				retrieveParametersErrorCode = 4;
				throw new RedException(retrieveParametersError);
			}

			if (StringUtils.isNullOrEmpty(protocollo.getIdProtocollo())) {
				retrieveParametersError = "Identificativo protocollo non presente";
				retrieveParametersErrorCode = 5;
				throw new RedException(retrieveParametersError);
			}

			if (protocollo.getNumeroRegistrazione() < 1 || protocollo.getDataRegistrazione() == null) {
				retrieveParametersError = "Dati protocollo non corretti";
				retrieveParametersErrorCode = 6;
				throw new RedException(retrieveParametersError);
			}

			if (protocollo.getTipoProtocollo() == null) {
				retrieveParametersError = "Tipo protocollo non presente";
				retrieveParametersErrorCode = 7;
				throw new RedException(retrieveParametersError);
			}

			if (parameters.getMessaggioRicevuto() == null) {
				retrieveParametersError = "Messaggio di posta non presente";
				retrieveParametersErrorCode = 8;
				throw new RedException(retrieveParametersError);
			}

			final EmailMessageType messaggio = parameters.getMessaggioRicevuto();
			if (StringUtils.isNullOrEmpty(messaggio.getCodiMessaggio())) {
				retrieveParametersError = ERROR_MESSAGE_ID_ASSENTE_MSG;
				retrieveParametersErrorCode = 9;
				throw new RedException(retrieveParametersError);
			}

			if (messaggio.getCasellaEmail() == null || StringUtils.isNullOrEmpty(messaggio.getCasellaEmail().getIndirizzoEmail().getEmail())) {
				retrieveParametersError = "Casella e-mail ricevente del messaggio di posta non presente";
				retrieveParametersErrorCode = 10;
				throw new RedException(retrieveParametersError);
			}

			if (messaggio.getMittente() == null) {
				retrieveParametersError = "Mittente del messaggio di posta non presente";
				retrieveParametersErrorCode = 11;
				throw new RedException(retrieveParametersError);
			}

			if (CollectionUtils.isEmpty(messaggio.getDestinatari())) {
				retrieveParametersError = "Destinatari del messaggio di posta non presenti";
				retrieveParametersErrorCode = 12;
				throw new RedException(retrieveParametersError);
			}

			if (StringUtils.isNullOrEmpty(messaggio.getOggettoMessaggio())) {
				retrieveParametersError = "Oggetto del messaggio di posta non presente";
				retrieveParametersErrorCode = 13;
				throw new RedException(retrieveParametersError);
			}

			if (messaggio.getDataMessaggio() == null) {
				retrieveParametersError = "Data del messaggio di posta non presente";
				retrieveParametersErrorCode = 14;
				throw new RedException(retrieveParametersError);
			}

			if (!postaNpsSRV.isCasellaPostaleProtocollazioneAutomatica(messaggio.getCasellaEmail())) {
				retrieveParametersError = "Casella e-mail ricevente del messaggio di posta non configurata per le protocollazioni automatiche";
				retrieveParametersErrorCode = 15;
				throw new RedException(retrieveParametersError);
			}

			LOGGER.info(logRadix + SUCCESS_PARAMETRI_FORMALMENTE_CORRETTI_MSG);

		} catch (final Exception e) {
			retrieveParamsError = handleParametersException(e, logRadix, retrieveParametersErrorCode, retrieveParametersError);
		}

		return retrieveParamsError;
	}

	private static ErroreValidazioneDTO checkParametersElaboraNotificaAzione(final RichiestaElaboraNotificaAzioneType parameters, final ParametersElaboraNotificaAzione parametersDTO,
			final String logRadix) {
		ErroreValidazioneDTO retrieveParamsError = null;
		String retrieveParametersError = null;
		int retrieveParametersErrorCode = 0;

		try {
			if (StringUtils.isNullOrEmpty(parameters.getIdMessaggio())) {
				retrieveParametersError = ERROR_ID_MESSAGGIO_ASSENTE_MSG;
				retrieveParametersErrorCode = 1;
				throw new RedException(retrieveParametersError);
			}

			if (parameters.getIdentificativoProtocollo() == null) {
				retrieveParametersError = ERROR_PROTOCOLLO_ASSENTE_MSG;
				retrieveParametersErrorCode = 2;
				throw new RedException(retrieveParametersError);
			}

			final IdentificativoProtocolloRequestType protocollo = parameters.getIdentificativoProtocollo();
			if (StringUtils.isNullOrEmpty(protocollo.getIdProtocollo())
					&& (protocollo.getNumeroRegistrazione() == null || protocollo.getNumeroRegistrazione() < 1 || protocollo.getDataRegistrazione() == null)) {
				retrieveParametersError = "Dati del protocollo non presenti";
				retrieveParametersErrorCode = 3;
				throw new RedException(retrieveParametersError);
			}

			if (parameters.getTipoAzione() == null) {
				retrieveParametersError = "Tipo azione non presente";
				retrieveParametersErrorCode = 4;
				throw new RedException(retrieveParametersError);
			}

			if (parameters.getDataAzione() == null) {
				retrieveParametersError = "Data azione non presente";
				retrieveParametersErrorCode = 5;
				throw new RedException(retrieveParametersError);
			}

			if (WkfTipoNotificaAzioneType.RISPONDI_A.equals(parameters.getTipoAzione())) {
				if (parameters.getDatiRispondiA() == null || parameters.getDatiRispondiA().getProtocolloRisposta() == null
						|| StringUtils.isNullOrEmpty(parameters.getDatiRispondiA().getSistemaAusiliario()) || parameters.getDatiRispondiA().getProtocollatore() == null) {
					retrieveParametersError = "Dati azione non presenti per l'azione: " + parameters.getTipoAzione().toString();
					retrieveParametersErrorCode = 6;
					throw new RedException(retrieveParametersError);
				} else {
					final WkfAzioneRispondiAProtocolloType datiRispondiA = parameters.getDatiRispondiA();
					final IdentificativoProtocolloRequestType protocolloRispondiA = datiRispondiA.getProtocolloRisposta();

					if (StringUtils.isNullOrEmpty(protocolloRispondiA.getIdProtocollo()) && (protocolloRispondiA.getNumeroRegistrazione() == null
							|| protocolloRispondiA.getNumeroRegistrazione() < 1 || protocolloRispondiA.getDataRegistrazione() == null)) {
						retrieveParametersError = "Dati del protocollo risposta non presenti";
						retrieveParametersErrorCode = 7;
						throw new RedException(retrieveParametersError);
					}

					Date dataProtocolloRisposta = null;
					Integer annoProtocolloRisposta = null;
					if (protocolloRispondiA.getDataRegistrazione() != null) {
						dataProtocolloRisposta = protocolloRispondiA.getDataRegistrazione().toGregorianCalendar().getTime();
						final Calendar cal = Calendar.getInstance();
						cal.setTime(dataProtocolloRisposta);
						annoProtocolloRisposta = cal.get(Calendar.YEAR);
					}

					// Protocollatore del protocollo risposta
					String protocollatoreStr = Constants.EMPTY_STRING;
					if (datiRispondiA.getProtocollatore() != null && datiRispondiA.getProtocollatore().getUtente() != null) {
						final OrgOrganigrammaType protocollatore = datiRispondiA.getProtocollatore();
						protocollatoreStr = protocollatore.getUtente().getNome() + " " + protocollatore.getUtente().getCognome();
					}

					// Dati specifici del tipo di azione (protocollo della risposta)
					final ParametersAzioneRispostaAutomatica paramsRispondiA = new ParametersAzioneRispostaAutomatica(protocolloRispondiA.getIdProtocollo(),
							protocolloRispondiA.getNumeroRegistrazione(), dataProtocolloRisposta, annoProtocolloRisposta, datiRispondiA.getStatoUscita(),
							TipoProtocolloEnum.ENTRATA.getId(), protocollatoreStr, datiRispondiA.getSistemaAusiliario(), parameters.getTipoAzione().value());

					int i = 0;
					for (final ProtIdentificatoreProtocolloCollegatoType ulterioreAllaccio : datiRispondiA.getCollegati()) {
						i++;

						if (StringUtils.isNullOrEmpty(ulterioreAllaccio.getIdProtocollo()) && 
								(ulterioreAllaccio.getNumeroRegistrazione() < 1 || ulterioreAllaccio.getDataRegistrazione() == null)) {
							retrieveParametersError = DATI_DEL_PROTOCOLLO_COLLEGATO_NUMERO + i + " non presenti";
							retrieveParametersErrorCode = 8;
							throw new RedException(retrieveParametersError);
						}

						Date dataProtocolloCollegato = null;
						Integer annoProtocolloCollegato = null;
						if (ulterioreAllaccio.getDataRegistrazione() != null) {
							dataProtocolloCollegato = ulterioreAllaccio.getDataRegistrazione().toGregorianCalendar().getTime();
							final Calendar cal = Calendar.getInstance();
							cal.setTime(dataProtocolloCollegato);
							annoProtocolloCollegato = cal.get(Calendar.YEAR);
						}

						Integer tipoProtocollo = TipoProtocolloEnum.ENTRATA.getId();
						if (ProtTipoProtocolloType.USCITA.equals(ulterioreAllaccio.getTipoProtocollo())) {
							tipoProtocollo = TipoProtocolloEnum.USCITA.getId();
						}

						paramsRispondiA.addUlterioreAllaccio(ulterioreAllaccio.getIdProtocollo(), ulterioreAllaccio.getNumeroRegistrazione(), dataProtocolloCollegato,
								annoProtocolloCollegato, ulterioreAllaccio.getStato(), tipoProtocollo);

					}

					parametersDTO.setDatiRispondiA(paramsRispondiA);
				}
				
			} else if (WkfTipoNotificaAzioneType.AGGIORNAMENTO_COLLEGATI.equals(parameters.getTipoAzione())) {
				
				if (parameters.getDatiAggiornamentoCollegati() == null || parameters.getDatiAggiornamentoCollegati().getCollegati().isEmpty()
						|| StringUtils.isNullOrEmpty(parameters.getDatiAggiornamentoCollegati().getSistemaAusiliario()) 
						|| parameters.getDatiAggiornamentoCollegati().getOperatore() == null) {
					retrieveParametersError = "Dati azione non presenti per l'azione: " + parameters.getTipoAzione().toString();
					retrieveParametersErrorCode = 9;
					throw new RedException(retrieveParametersError);
				} else {
					
					final WkfAzioneAggiornamentoCollegatiType datiAggiornamentoCollegati = parameters.getDatiAggiornamentoCollegati();
					
					int i = 0;
					ParametersAzioneRispostaAutomatica paramsRispondiA = null;
					for(final ProtIdentificatoreProtocolloCollegatoType allaccio: datiAggiornamentoCollegati.getCollegati()) {
						i++;
						
						if (StringUtils.isNullOrEmpty(allaccio.getIdProtocollo()) && 
								(allaccio.getNumeroRegistrazione() < 1 || allaccio.getDataRegistrazione() == null)) {
							retrieveParametersError = DATI_DEL_PROTOCOLLO_COLLEGATO_NUMERO + i + " non presenti";
							retrieveParametersErrorCode = 10;
							throw new RedException(retrieveParametersError);
						}

						Date dataProtocolloCollegato = null;
						Integer annoProtocolloCollegato = null;
						if (allaccio.getDataRegistrazione() != null) {
							dataProtocolloCollegato = allaccio.getDataRegistrazione().toGregorianCalendar().getTime();
							final Calendar cal = Calendar.getInstance();
							cal.setTime(dataProtocolloCollegato);
							annoProtocolloCollegato = cal.get(Calendar.YEAR);
						}

						Integer tipoProtocollo = TipoProtocolloEnum.ENTRATA.getId();
						if (ProtTipoProtocolloType.USCITA.equals(allaccio.getTipoProtocollo())) {
							if(i == 1) {
								retrieveParametersError = DATI_DEL_PROTOCOLLO_COLLEGATO_NUMERO + i + " non validi: il primo collegamento deve essere un entrata";
								retrieveParametersErrorCode = 11;
								throw new RedException(retrieveParametersError);
							} else {
								tipoProtocollo = TipoProtocolloEnum.USCITA.getId();
							}
						}
						
						if(i == 1) {
							
							// Protocollatore del protocollo risposta
							String protocollatoreStr = Constants.EMPTY_STRING;
							if (datiAggiornamentoCollegati.getOperatore() != null && datiAggiornamentoCollegati.getOperatore().getUtente() != null) {
								final OrgOrganigrammaType protocollatore = datiAggiornamentoCollegati.getOperatore();
								protocollatoreStr = protocollatore.getUtente().getNome() + " " + protocollatore.getUtente().getCognome();
							}

							// Dati specifici del tipo di azione (protocollo della risposta)
							paramsRispondiA = new ParametersAzioneRispostaAutomatica(allaccio.getIdProtocollo(),
									allaccio.getNumeroRegistrazione(), dataProtocolloCollegato, annoProtocolloCollegato, allaccio.getStato(),
									tipoProtocollo, protocollatoreStr, datiAggiornamentoCollegati.getSistemaAusiliario(), parameters.getTipoAzione().value());
							
						} else {
							
							paramsRispondiA.addUlterioreAllaccio(allaccio.getIdProtocollo(), allaccio.getNumeroRegistrazione(), dataProtocolloCollegato,
									annoProtocolloCollegato, allaccio.getStato(), tipoProtocollo);
						
						}
						
					}
					
					parametersDTO.setDatiRispondiA(paramsRispondiA);
					
				}
				
			}

			parametersDTO.setIdMessaggioNPS(parameters.getIdMessaggio());
			if (!StringUtils.isNullOrEmpty(protocollo.getIdProtocollo())) {
				parametersDTO.setIdProtocollo(protocollo.getIdProtocollo());
			}
			if (protocollo.getRegistro() != null && protocollo.getRegistro().getAoo() != null) {
				parametersDTO.setCodiceAooNPS(protocollo.getRegistro().getAoo().getCodiceAOO());
			}
			if (protocollo.getNumeroRegistrazione() != null) {
				parametersDTO.setNumeroProtocollo(protocollo.getNumeroRegistrazione());
			}
			if (protocollo.getDataRegistrazione() != null) {
				parametersDTO.setDataProtocollo(protocollo.getDataRegistrazione().toGregorianCalendar().getTime());
				final Calendar cal = Calendar.getInstance();
				cal.setTime(parametersDTO.getDataProtocollo());
				final int annoProtocollo = cal.get(Calendar.YEAR);
				parametersDTO.setAnnoProtocollo(annoProtocollo);
			}
			parametersDTO.setDataAzione(parameters.getDataAzione().toGregorianCalendar().getTime());
			parametersDTO.setTipoAzione(parameters.getTipoAzione().value());

			LOGGER.info(logRadix + SUCCESS_PARAMETRI_FORMALMENTE_CORRETTI_MSG);
		} catch (final Exception e) {
			retrieveParamsError = handleParametersException(e, logRadix, retrieveParametersErrorCode, retrieveParametersError);
		}

		return retrieveParamsError;
	}

	private ErroreValidazioneDTO checkParametersElaboraNotificaErroreProtocollazioneAutomatica(final RichiestaElaboraErroreProtocollazioneAutomaticaType parameters,
			final String logRadix) {
		ErroreValidazioneDTO retrieveParamsError = null;
		String retrieveParametersError = null;
		int retrieveParametersErrorCode = 0;

		try {
			if (StringUtils.isNullOrEmpty(parameters.getIdMessaggio())) {
				retrieveParametersError = ERROR_ID_MESSAGGIO_ASSENTE_MSG;
				retrieveParametersErrorCode = 1;
				throw new RedException(retrieveParametersError);
			}

			if (StringUtils.isNullOrEmpty(parameters.getIdDocumento())) {
				retrieveParametersError = ERROR_ID_DOCUMENTO_ASSENTE_MSG;
				retrieveParametersErrorCode = 2;
				throw new RedException(retrieveParametersError);
			}

			final NotificaInviata notifica = parameters.getNotificaInviata();
			if (notifica == null) {
				retrieveParametersError = "Notifica non presente";
				retrieveParametersErrorCode = 3;
				throw new RedException(retrieveParametersError);
			}

			if (notifica.getDataInvio() == null) {
				retrieveParametersError = "Data di invio della notifica non presente";
				retrieveParametersErrorCode = 4;
				throw new RedException(retrieveParametersError);
			}

			if (StringUtils.isNullOrEmpty(notifica.getTipoMessaggio())) {
				retrieveParametersError = "Tipo di notifica non presente";
				retrieveParametersErrorCode = 5;
				throw new RedException(retrieveParametersError);
			}

			if (StringUtils.isNullOrEmpty(notifica.getTestoMessaggio())) {
				retrieveParametersError = "Testo della notifica non presente";
				retrieveParametersErrorCode = 6;
				throw new RedException(retrieveParametersError);
			}

			if (parameters.getMessaggioRicevuto() == null) {
				retrieveParametersError = "Messaggio di posta non presente";
				retrieveParametersErrorCode = 7;
				throw new RedException(retrieveParametersError);
			}

			final EmailMessageType messaggio = parameters.getMessaggioRicevuto();
			if (StringUtils.isNullOrEmpty(messaggio.getCodiMessaggio())) {
				retrieveParametersError = ERROR_MESSAGE_ID_ASSENTE_MSG;
				retrieveParametersErrorCode = 8;
				throw new RedException(retrieveParametersError);
			}

			if (messaggio.getCasellaEmail() == null || StringUtils.isNullOrEmpty(messaggio.getCasellaEmail().getIndirizzoEmail().getEmail())) {
				retrieveParametersError = "Casella e-mail ricevente del messaggio di posta non presente";
				retrieveParametersErrorCode = 9;
				throw new RedException(retrieveParametersError);
			}

			if (messaggio.getMittente() == null) {
				retrieveParametersError = "Mittente del messaggio di posta non presente";
				retrieveParametersErrorCode = 10;
				throw new RedException(retrieveParametersError);
			}

			if (CollectionUtils.isEmpty(messaggio.getDestinatari())) {
				retrieveParametersError = "Destinatari del messaggio di posta non presenti";
				retrieveParametersErrorCode = 11;
				throw new RedException(retrieveParametersError);
			}

			if (StringUtils.isNullOrEmpty(messaggio.getOggettoMessaggio())) {
				retrieveParametersError = "Oggetto del messaggio di posta non presente";
				retrieveParametersErrorCode = 12;
				throw new RedException(retrieveParametersError);
			}

			if (messaggio.getDataMessaggio() == null) {
				retrieveParametersError = "Data del messaggio di posta non presente";
				retrieveParametersErrorCode = 13;
				throw new RedException(retrieveParametersError);
			}

			if (!postaNpsSRV.isCasellaPostaleProtocollazioneAutomatica(messaggio.getCasellaEmail())) {
				retrieveParametersError = "Casella e-mail ricevente del messaggio di posta non configurata per le protocollazioni automatiche";
				retrieveParametersErrorCode = 14;
				throw new RedException(retrieveParametersError);
			}

			LOGGER.info(logRadix + SUCCESS_PARAMETRI_FORMALMENTE_CORRETTI_MSG);

		} catch (final Exception e) {
			retrieveParamsError = handleParametersException(e, logRadix, retrieveParametersErrorCode, retrieveParametersError);
		}

		return retrieveParamsError;
	}

	/**
	 * Gestisce le eccezioni di validazione delle richieste ricevute dal Web
	 * Service.
	 * 
	 * @param e
	 * @param logRadix
	 * @param parametersErrorCode
	 * @param parametersError
	 * @throws GenericFault
	 */
	private static ErroreValidazioneDTO handleParametersException(final Exception e, final String logRadix, final int parametersErrorCode, final String parametersError) {
		LOGGER.error(logRadix, e);
		return new ErroreValidazioneDTO(parametersErrorCode, parametersError);
	}

	/**
	 * @param response
	 * @param errorCode
	 * @param description
	 */
	private void handleResponseElaborationError(final BaseServiceResponseType response, final int errorCode, final String description) {
		response.setEsito(EsitoType.ERRORE_ELABORAZIONE);

		final it.ibm.red.webservice.model.interfaccianps.npsmessages.ServiceErrorType errorType = new it.ibm.red.webservice.model.interfaccianps.npsmessages.ServiceErrorType();
		errorType.setErrorCode(errorCode);
		errorType.setErrorMessageString(description);
		response.getErrorList().add(errorType);

		LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
	}

}