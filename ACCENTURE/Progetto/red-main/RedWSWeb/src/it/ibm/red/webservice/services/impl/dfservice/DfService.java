package it.ibm.red.webservice.services.impl.dfservice;

import java.util.List;

import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import it.ibm.red.business.dto.MappingOrgNsdDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.ws.facade.IDfWsFacadeSRV;
import it.ibm.red.webservice.handlers.LoggingHandler;
import it.ibm.red.webservice.model.dfservice.dto.ErrorCode;
import it.ibm.red.webservice.model.dfservice.types.messages.AssegnatarioCompetenzaNps;
import it.ibm.red.webservice.model.dfservice.types.messages.BaseResponseType;
import it.ibm.red.webservice.model.dfservice.types.messages.DestinatariNps;
import it.ibm.red.webservice.model.dfservice.types.messages.EsitoType;
import it.ibm.red.webservice.model.dfservice.types.messages.MappingOrgDF;
import it.ibm.red.webservice.model.dfservice.types.messages.MittenteNps;
import it.ibm.red.webservice.model.dfservice.types.messages.ObjectFactory;
import it.ibm.red.webservice.model.dfservice.types.messages.RedMappingOrganigrammaResponseType;
import it.ibm.red.webservice.model.dfservice.types.messages.RedMappingOrganigrammaType;
import it.ibm.red.webservice.model.dfservice.types.messages.UtenteProtocollatoreNps;
import it.ibm.red.webservice.services.dfservice.IDfService;
import it.ibm.red.webservice.services.dfservice.validator.ValidatorUtility;

@WebService(
		portName = "DfServicePort",
        serviceName = "DfService",
        targetNamespace = "http://dfservice.model.webservice.red.ibm.it",
        endpointInterface = "it.ibm.red.webservice.services.dfservice.IDfService")
@HandlerChain(file = "/handlers.xml")
public class DfService implements IDfService {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DfService.class);
				
	/**
	 * Contesto.
	 */
	@Resource
    private WebServiceContext context;
	
	/**
	 * Factory.
	 */
	private ObjectFactory of;
	
	/**
	 * Service.
	 */
	private IDfWsFacadeSRV dfWsSRV;
	
	/**
	 * Identificativo AOO.
	 */
	private long idAoo;

	/**
	 * Costruttore.
	 */
	public DfService() {
		try {
			of = new ObjectFactory();
			dfWsSRV = ApplicationContextProvider.getApplicationContext().getBean(IDfWsFacadeSRV.class);
			idAoo = Long.parseLong(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IDAOO_DF));
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di costruzione del servizio", e);
		}
	}
	
	/**
	 * @param response
	 * @param errore
	 */
	private void gestisciEsitoErrore(final BaseResponseType response, final ErrorCode errore) {
		response.setEsito(createEsitoType(errore));
		LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
	}

	/**
	 * @param response
	 * @param errore
	 * @param e
	 */
	private void gestisciEsitoErroreEccezione(final BaseResponseType response, final ErrorCode errore, final Exception e) {
		response.setEsito(createEsitoType(errore));
		if (e instanceof RedException) {
			response.getEsito().setDescErrore(response.getEsito().getDescErrore() + ": " + e.getMessage());
		}
		LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
	}

	/**
	 * @param code
	 * @return
	 */
	private EsitoType createEsitoType(final ErrorCode code) {
		final EsitoType esito = of.createEsitoType();
		esito.setEsito(code == null);
		
		if (code != null) {
			esito.setCodiceErrore(code.getCode());
			esito.setDescErrore(code.getDescription());
		}
		
		return esito;
	}

	/**
	 * @return
	 */
	private EsitoType createEsitoTypeOk() {
		final EsitoType esito = of.createEsitoType();
		esito.setEsito(true);
		return esito;
	}

	/**
	 * Mapping organigramma.
	 * @param request
	 * @return response
	 */
	@Override
	public RedMappingOrganigrammaResponseType redMappingOrganigramma(final RedMappingOrganigrammaType request) {
		LOGGER.info("[DfService][redMappingOrganigramma][" + request.getCredenzialiWS().getIdClient() + "]");
		final RedMappingOrganigrammaResponseType response = of.createRedMappingOrganigrammaResponseType();
		
		// Validazione della request
		final ErrorCode erroreValidazione = ValidatorUtility.validaRedMappingOrganigramma(request);
		
		if (erroreValidazione == null) {
			try {
			
				final RedWsClient client = new RedWsClient(request.getCredenzialiWS().getIdClient(), 
						request.getCredenzialiWS().getPwdClient(), idAoo, null, null);
				
				 				
 				final List<MappingOrgNsdDTO> mappingCompleto = dfWsSRV.mappingOrganigramma(client, request);
 				for (final MappingOrgNsdDTO mapping : mappingCompleto) {
 					 response.getMapping().add(getMappingWs(mapping)); 
 				} 
				
				response.setEsito(createEsitoTypeOk());
				
				LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
				
			} catch (final Exception e) {
				LOGGER.error("[Df][redGetOrganigramma] Errore di elaborazione: " + e.getMessage(), e);
				gestisciEsitoErroreEccezione(response, ErrorCode.ERRORE_ELABORAZIONE, e);
			}
		} else {
			gestisciEsitoErrore(response, erroreValidazione);
		}
		
		return response;
	}

	private AssegnatarioCompetenzaNps getMappingAssegnatario(final MappingOrgNsdDTO mappingOrgNsdDTO) {
		final AssegnatarioCompetenzaNps output = of.createAssegnatarioCompetenzaNps();
		output.setCodiceAmm(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getCodiceAmm());
		output.setDenominazioneAmm(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getDenominazioneAmm());
		output.setCodiceAoo(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getCodiceAoo());
		output.setDenominazioneAoo(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getDenominazioneAoo());
		output.setCodUO(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getCodUO());
		output.setDenominazioneUO(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getDenominazioneUO());
		output.setChiaveEsternaOperatore(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getChiaveEsternaOperatore());
		output.setNome(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getNome());
		output.setCognome(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getCognome());
		output.setEmail(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getEmail());
		return output;
	}
	private DestinatariNps getMappingDestinatari(final MappingOrgNsdDTO mappingOrgNsdDTO) {
		final DestinatariNps output = of.createDestinatariNps();
		output.setCodFiscale(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getCodFiscale());
		output.setCodiceAmm(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getCodiceAmm());
		output.setCodiceAoo(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getCodiceAoo());
		output.setCodUO(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getCodUO());
		output.setDenominazioneUO(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getDenominazioneUO());
		output.setCognome(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getCognome());
		output.setDenominazioneAmm(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getDenominazioneAmm());
		output.setDenominazioneAoo(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getDenominazioneAoo());
		output.setEmail(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getEmail());
		output.setNome(mappingOrgNsdDTO.getParamNpsDTO().getAssCompetenzaDestNpsDfWSDTO().getNome());
		
		return output;
	}
	
	private MappingOrgDF getMappingWs(final MappingOrgNsdDTO mappingOrgNsdDTO) {
		final MappingOrgDF output = of.createMappingOrgDF();
		
		output.setAcl(mappingOrgNsdDTO.getParamNpsDTO().getAcl());
		output.setCodiceAmministrazione(mappingOrgNsdDTO.getParamNpsDTO().getConfigurazioneNps().getCodiceAmministrazione());
		output.setCodAoo(mappingOrgNsdDTO.getParamNpsDTO().getConfigurazioneNps().getCodiceAoo());
		output.setDenominazioneAmministrazione(mappingOrgNsdDTO.getParamNpsDTO().getConfigurazioneNps().getDenominazioneAmministrazione());
		output.setDenominazioneAOO(mappingOrgNsdDTO.getParamNpsDTO().getConfigurazioneNps().getDenominazioneAoo());
		output.setDenominazioneRegistro(mappingOrgNsdDTO.getParamNpsDTO().getConfigurazioneNps().getDenominazioneRegistro());
		output.setTipoProtocollo(mappingOrgNsdDTO.getParamNpsDTO().getTipoProtocollo());
		
		final UtenteProtocollatoreNps utenteProtocollatore = getMappingUtenteProtocollatore(mappingOrgNsdDTO);
		output.setUtenteProtocollatore(utenteProtocollatore);
		
		final AssegnatarioCompetenzaNps assegnatario = getMappingAssegnatario(mappingOrgNsdDTO);
		output.setAssegnatarioCompetenza(assegnatario);
		
		final MittenteNps mittente = of.createMittenteNps(); 
		mittente.setNome(mappingOrgNsdDTO.getParamNpsDTO().getNomeMittente());
		mittente.setCognome(mappingOrgNsdDTO.getParamNpsDTO().getCognomeMittente());
		mittente.setCodiceFiscale(mappingOrgNsdDTO.getParamNpsDTO().getCfMittente());
		output.setMittente(mittente);
		
		final DestinatariNps destinatari = getMappingDestinatari(mappingOrgNsdDTO);
		output.setDestinatario(destinatari);
		return output;
	}
	
	private UtenteProtocollatoreNps getMappingUtenteProtocollatore(final MappingOrgNsdDTO mappingOrgNsdDTO) {
		final UtenteProtocollatoreNps utenteProtocollatore = of.createUtenteProtocollatoreNps(); 
		utenteProtocollatore.setChiaveEsternaOperatore(mappingOrgNsdDTO.getParamNpsDTO().getUtenteProtocollatoreDTO().getChiaveEsternaOperatore());
		utenteProtocollatore.setCodAoo(mappingOrgNsdDTO.getParamNpsDTO().getUtenteProtocollatoreDTO().getCodAoo());
		utenteProtocollatore.setCodiceAmministrazione(mappingOrgNsdDTO.getParamNpsDTO().getUtenteProtocollatoreDTO().getCodiceAmministrazione());
		utenteProtocollatore.setCodUO(mappingOrgNsdDTO.getParamNpsDTO().getUtenteProtocollatoreDTO().getCodUO());
		utenteProtocollatore.setDenominazioneUO(mappingOrgNsdDTO.getParamNpsDTO().getUtenteProtocollatoreDTO().getDenominazioneUO());
		utenteProtocollatore.setCognome(mappingOrgNsdDTO.getParamNpsDTO().getUtenteProtocollatoreDTO().getCognome());
		utenteProtocollatore.setDenominazioneAmministrazione(mappingOrgNsdDTO.getParamNpsDTO().getUtenteProtocollatoreDTO().getDenominazioneAmministrazione());
		utenteProtocollatore.setDenominazioneAoo(mappingOrgNsdDTO.getParamNpsDTO().getUtenteProtocollatoreDTO().getDenominazioneAoo());
		utenteProtocollatore.setNome(mappingOrgNsdDTO.getParamNpsDTO().getUtenteProtocollatoreDTO().getNome());
		return utenteProtocollatore;
	}
}
