package it.ibm.red.webservice.services;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import it.ibm.red.webservice.model.redsiebel.types.AggiornaRdsRequestType;
import it.ibm.red.webservice.model.redsiebel.types.AggiornaRdsResponseType;

@WebService(
		name = "RedSiebelService", 
		targetNamespace = "http://redwsevo.mef.gov.it/ibm/RedSiebelService")
public interface IRedSiebelService {

	/**
	 * Aggiorna Rds.
	 * @param aggiornaRdsRequest
	 * @return response di aggiornamento Rds
	 */
	@WebMethod(operationName = "aggiornaRds")
	@WebResult(name = "aggiornaRdsResponse")
	AggiornaRdsResponseType aggiornaRds(
			@WebParam(name = "aggiornaRdsRequest")
			AggiornaRdsRequestType aggiornaRdsRequest);
	
	
}