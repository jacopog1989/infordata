package it.ibm.red.webservice.services.documentservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

import it.ibm.red.webservice.model.documentservice.types.messages.*;



@WebService(name = "DocumentService", targetNamespace = "http://documentservice.model.webservice.red.ibm.it")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
	it.ibm.red.webservice.model.documentservice.types.messages.ObjectFactory.class,
	it.ibm.red.webservice.model.documentservice.types.documentale.ObjectFactory.class,
	it.ibm.red.webservice.model.documentservice.types.organigramma.ObjectFactory.class,
	it.ibm.red.webservice.model.documentservice.types.processo.ObjectFactory.class,
	it.ibm.red.webservice.model.documentservice.types.ricerca.ObjectFactory.class,
	it.ibm.red.webservice.model.documentservice.types.rubrica.ObjectFactory.class,
	it.ibm.red.webservice.model.documentservice.types.sign.ObjectFactory.class,
	it.ibm.red.webservice.model.documentservice.types.security.ObjectFactory.class
})
public interface IDocumentService {

	/**
	 * Crea il documento in uscita.
	 * @param parameters
	 * @return response di creazione documento in uscita
	 */
	@WebMethod(operationName = "redCreazioneDocumentoUscita", action = "urn:#redCreazioneDocumentoUscita")
	@WebResult(name = "redCreazioneDocumentoUscitaResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedCreazioneDocumentoUscitaResponseType redCreazioneDocumentoUscita(
			@WebParam(name = "redCreazioneDocumentoUscita", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedCreazioneDocumentoUscitaType parameters);
	
	/**
	 * Crea il documento in entrata.
	 * @param parameters
	 * @return response di creazione documento in entrata.
	 */
	@WebMethod(operationName = "redCreazioneDocumentoEntrata", action = "urn:#redCreazioneDocumentoEntrata")
	@WebResult(name = "redCreazioneDocumentoEntrataResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedCreazioneDocumentoEntrataResponseType redCreazioneDocumentoEntrata(
			@WebParam(name = "redCreazioneDocumentoEntrata", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedCreazioneDocumentoEntrataType parameters);
	
	/**
	 * Ricerca documenti.
	 * @param parameters
	 * @return response di ricerca documenti
	 */
	@WebMethod(operationName = "redRicercaDocumenti", action = "urn:#redRicercaDocumenti")
	@WebResult(name = "redRicercaDocumentiResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedRicercaDocumentiResponseType redRicercaDocumenti(
			@WebParam(name = "redRicercaDocumenti", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedRicercaDocumentiType parameters);
	
	/**
	 * Ricerca fascicoli.
	 * @param parameters
	 * @return response di ricerca fascicoli
	 */
	@WebMethod(operationName = "redRicercaFascicoli", action = "urn:#redRicercaFascicoli")
	@WebResult(name = "redRicercaFascicoliResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedRicercaFascicoliResponseType redRicercaFascicoli(
			@WebParam(name = "redRicercaFascicoli", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedRicercaFascicoliType parameters);
	
	/**
	 * Ricerca faldoni.
	 * @param parameters
	 * @return response di ricerca faldoni
	 */
	@WebMethod(operationName = "redRicercaFaldoni", action = "urn:#redRicercaFaldoni")
	@WebResult(name = "redRicercaFaldoniResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedRicercaFaldoniResponseType redRicercaFaldoni(
			@WebParam(name = "redRicercaFaldoni", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedRicercaFaldoniType parameters);
	
	/**
	 * Ottiene l'organigramma.
	 * @param parameters
	 * @return response di recupero organigramma
	 */
	@WebMethod(operationName = "redGetOrganigramma", action = "urn:#redGetOrganigramma")
	@WebResult(name = "redGetOrganigrammaResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedGetOrganigrammaResponseType redGetOrganigramma(
			@WebParam(name = "redGetOrganigramma", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedGetOrganigrammaType parameters);

	/**
	 * Fascicola il documento.
	 * @param parameters
	 * @return response di fascicolazione documento
	 */
	@WebMethod(operationName = "redFascicolazioneDocumento", action = "urn:#redFascicolazioneDocumento")
	@WebResult(name = "redFascicolazioneDocumentoResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedFascicolazioneDocumentoResponseType redFascicolazioneDocumento(
			@WebParam(name = "redFascicolazioneDocumento", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedFascicolazioneDocumentoType parameters);
	
	/**
	 * Elimina il fascicolo.
	 * @param parameters
	 * @return response di eliminazione fascicolo
	 */
	@WebMethod(operationName = "redEliminaFascicolo", action = "urn:#redEliminaFascicolo")
	@WebResult(name = "redEliminaFascicoloResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedEliminaFascicoloResponseType redEliminaFascicolo(
			@WebParam(name = "redEliminaFascicolo", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedEliminaFascicoloType parameters);
	
	/**
	 * Modifica lo stato del fascicolo.
	 * @param parameters
	 * @return response di modifica stato del fascicolo
	 */
	@WebMethod(operationName = "redCambiaStatoFascicolo", action = "urn:#redCambiaStatoFascicolo")
	@WebResult(name = "redCambiaStatoFascicoloResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedCambiaStatoFascicoloResponseType redCambiaStatoFascicolo(
			@WebParam(name = "redCambiaStatoFascicolo", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedCambiaStatoFascicoloType parameters);
	
	/**
	 * Modifica il fascicolo.
	 * @param parameters
	 * @return response di modifica fascicolo
	 */
	@WebMethod(operationName = "redModificaFascicolo", action = "urn:#redModificaFascicolo")
	@WebResult(name = "redModificaFascicoloResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedModificaFascicoloResponseType redModificaFascicolo(
			@WebParam(name = "redModificaFascicolo", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedModificaFascicoloType parameters);
	
	/**
	 * Associa il fascicolo al titolario.
	 * @param parameters
	 * @return response di associazione fascicolo a titolario
	 */
	@WebMethod(operationName = "redAssociaFascicoloATitolario", action = "urn:#redAssociaFascicoloATitolario")
	@WebResult(name = "redAssociaFascicoloATitolarioResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedAssociaFascicoloATitolarioResponseType redAssociaFascicoloATitolario(
			@WebParam(name = "redAssociaFascicoloATitolario", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedAssociaFascicoloATitolarioType parameters);
	
	/**
	 * Crea il fascicolo.
	 * @param parameters
	 * @return response di creazione fascicolo
	 */
	@WebMethod(operationName = "redCreaFascicolo", action = "urn:#redCreaFascicolo")
	@WebResult(name = "redCreaFascicoloResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedCreaFascicoloResponseType redCreaFascicolo(
			@WebParam(name = "redCreaFascicolo", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedCreaFascicoloType parameters);
	
	/**
	 * Inserisce il protocollo in entrata.
	 * @param parameters
	 * @return response di inserimento protocollo in entrata
	 */
	@WebMethod(operationName = "redInserisciProtEntrata", action = "urn:#redInserisciProtEntrata")
	@WebResult(name = "redInserisciProtEntrataResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedInserisciProtEntrataResponseType redInserisciProtEntrata(
			@WebParam(name = "redInserisciProtEntrata", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedInserisciProtEntrataType parameters);

	/**
	 * Inserisce il protocollo in uscita.
	 * @param parameters
	 * @return response di inserimento protocollo in uscita
	 */
	@WebMethod(operationName = "redInserisciProtUscita", action = "urn:#redInserisciProtUscita")
	@WebResult(name = "redInserisciProtUscitaResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedInserisciProtUscitaResponseType redInserisciProtUscita(
			@WebParam(name = "redInserisciProtUscita", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedInserisciProtUscitaType parameters);

	/**
	 * Inserisce cil campo firma.
	 * @param parameters
	 * @return response di inserimento campo firma
	 */
	@WebMethod(operationName = "redInserisciCampoFirma", action = "urn:#redInserisciCampoFirma")
	@WebResult(name = "redInserisciCampoFirmaResponseType", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedInserisciCampoFirmaResponseType redInserisciCampoFirma(
			@WebParam(name = "redInserisciCampoFirma", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedInserisciCampoFirmaType parameters);
	
	/**
	 * Inserisce la postilla.
	 * @param parameters
	 * @return response di inserimento postilla
	 */
	@WebMethod(operationName = "redInserisciPostilla", action = "urn:#redInserisciPostilla")
	@WebResult(name = "redInserisciPostillaResponseType", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedInserisciPostillaResponseType redInserisciPostilla(
			@WebParam(name = "redInserisciPostilla", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedInserisciPostillaType parameters);
	
	/**
	 * Inserisce l'id.
	 * @param parameters
	 * @return response di inserimento id
	 */
	@WebMethod(operationName = "redInserisciId", action = "urn:#redInserisciId")
	@WebResult(name = "redInserisciIdResponseType", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedInserisciIdResponseType redInserisciId(
			@WebParam(name = "redInserisciId", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedInserisciIdType parameters);
	
	/**
	 * Inserisce watermark.
	 * @param parameters
	 * @return response di inserimento watermark
	 */
	@WebMethod(operationName = "redInserisciWatermark", action = "urn:#redInserisciWatermark")
	@WebResult(name = "redInserisciWatermarkResponseType", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedInserisciWatermarkResponseType redInserisciWatermark(
			@WebParam(name = "redInserisciWatermark", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedInserisciWatermarkType parameters);
	
	/**
	 * Inserisce l'approvazione.
	 * @param parameters
	 * @return response di inserimento approvazione
	 */
	@WebMethod(operationName = "redInserisciApprovazione", action = "urn:#redInserisciApprovazione")
	@WebResult(name = "redInserisciApprovazioneResponseType", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedInserisciApprovazioneResponseType redInserisciApprovazione(
			@WebParam(name = "redInserisciApprovazione", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedInserisciApprovazioneType parameters);
	
	/**
	 * Elimina il faldone.
	 * @param parameters
	 * @return response di eliminazione faldone
	 */
	@WebMethod(operationName = "redEliminaFaldone", action = "urn:#redEliminaFaldone")
	@WebResult(name = "redEliminaFaldoneResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedEliminaFaldoneResponseType redEliminaFaldone(
			@WebParam(name = "redEliminaFaldone", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedEliminaFaldoneType parameters);
	
	/**
	 * Crea il faldone.
	 * @param parameters
	 * @return esito di creazione faldone
	 */
	@WebMethod(operationName = "redCreaFaldone", action = "urn:#redCreaFaldone")
	@WebResult(name = "redCreaFaldoneResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedCreaFaldoneResponseType redCreaFaldone(
			@WebParam(name = "redCreaFaldone", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedCreaFaldoneType parameters);
	
	/**
	 * Associa il fascicolo al faldone.
	 * @param parameters
	 * @return response di associazione fascicolo a faldone
	 */
	@WebMethod(operationName = "redAssociaFascicoloFaldone", action = "urn:#redAssociaFascicoloFaldone")
	@WebResult(name = "redAssociaFascicoloFaldoneResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedAssociaFascicoloFaldoneResponseType redAssociaFascicoloFaldone(
			@WebParam(name = "redAssociaFascicoloFaldone", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedAssociaFascicoloFaldoneType parameters);
	
	/**
	 * Rimuove l'associazione del fascicolo al faldone.
	 * @param parameters
	 * @return response di rimozione dell'associazione fascicolo a faldone
	 */
	@WebMethod(operationName = "redRimuoviAssociazioneFascicoloFaldone", action = "urn:#redRimuoviAssociazioneFascicoloFaldone")
	@WebResult(name = "redRimuoviAssociazioneFascicoloFaldoneResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedRimuoviAssociazioneFascicoloFaldoneResponseType redRimuoviAssociazioneFascicoloFaldone(
			@WebParam(name = "redRimuoviAssociazioneFascicoloFaldone", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedRimuoviAssociazioneFascicoloFaldoneType parameters);
	
	/**
	 * Sposta il fascicolo di faldone.
	 * @param parameters
	 * @return response dello spastamento del fascicolo di faldone
	 */
	@WebMethod(operationName = "redSpostaFascicoloFaldone", action = "urn:#redSpostaFascicoloFaldone")
	@WebResult(name = "redSpostaFascicoloFaldoneResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedSpostaFascicoloFaldoneResponseType redSpostaFascicoloFaldone(
			@WebParam(name = "redSpostaFascicoloFaldone", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedSpostaFascicoloFaldoneType parameters);
	
	/**
	 * Ottiene l'elenco delle versioni del documento.
	 * @param parameters
	 * @return response di recupero versioni del documento
	 */
	@WebMethod(operationName = "redElencoVersioniDocumento", action = "urn:#redElencoVersioniDocumento")
	@WebResult(name = "redElencoVersioniDocumentoResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedElencoVersioniDocumentoResponseType redElencoVersioniDocumento(
			@WebParam(name = "redElencoVersioniDocumento", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedElencoVersioniDocumentoType parameters);
	
	/**
	 * Ottiene la versione del documento.
	 * @param parameters
	 * @return response di recupero versione del documento
	 */
	@WebMethod(operationName = "redGetVersioneDocumento", action = "urn:#redGetVersioneDocumento")
	@WebResult(name = "redGetVersioneDocumentoResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedGetVersioneDocumentoResponseType redGetVersioneDocumento(
			@WebParam(name = "redGetVersioneDocumento", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedGetVersioneDocumentoType parameters);

	/**
	 * Esegue il check out.
	 * @param parameters
	 * @return response di check out
	 */
	@WebMethod(operationName = "redCheckOut", action = "urn:#redCheckOut")
	@WebResult(name = "redCheckOutResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedCheckOutResponseType redCheckOut(
			@WebParam(name = "redCheckOut", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedCheckOutType parameters);

	/**
	 * Reverta il check out.
	 * @param parameters
	 * @return response di revert del check out
	 */
	@WebMethod(operationName = "redUndoCheckOut", action = "urn:#redUndoCheckOut")
	@WebResult(name = "redUndoCheckOutResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedUndoCheckOutResponseType redUndoCheckOut(
			@WebParam(name = "redUndoCheckOut", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedUndoCheckOutType parameters);
	
	/**
	 * Esegue il check in.
	 * @param parameters
	 * @return ressponse di check in
	 */
	@WebMethod(operationName = "redCheckIn", action = "urn:#redCheckIn")
	@WebResult(name = "redCheckInResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedCheckInResponseType redCheckIn(
			@WebParam(name = "redCheckIn", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedCheckInType parameters);
	
	/**
	 * Esegue la ricerca avanzata.
	 * @param parameters
	 * @return response di ricerca avanzata
	 */
	@WebMethod(operationName = "redRicercaAvanzata", action = "urn:#redRicercaAvanzata")
	@WebResult(name = "redRicercaAvanzataResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedRicercaAvanzataResponseType redRicercaAvanzata(
			@WebParam(name = "redRicercaAvanzata", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedRicercaAvanzataType parameters);
	
	/**
	 * Avanza il workflow.
	 * @param parameters
	 * @return response di avanzamento workflow
	 */
	@WebMethod(operationName = "redAvanzaWorkflow", action = "urn:#redAvanzaWorkflow")
	@WebResult(name = "redAvanzaWorkflowResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedAvanzaWorkflowResponseType redAvanzaWorkflow(
			@WebParam(name = "redAvanzaWorkflow", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedAvanzaWorkflowType parameters);
	
	/**
	 * Avvia un'istanza del workflow.
	 * @param parameters
	 * @return response di avvio istanza del workflow
	 */
	@WebMethod(operationName = "redAvviaIstanzaWorkflow", action = "urn:#redAvviaIstanzaWorkflow")
	@WebResult(name = "redAvviaIstanzaWorkflowResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedAvviaIstanzaWorkflowResponseType redAvviaIstanzaWorkflow(
			@WebParam(name = "redAvviaIstanzaWorkflow", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedAvviaIstanzaWorkflowType parameters);

	/**
	 * Trasforma in PDF.
	 * @param parameters
	 * @return response di trasformazione in PDF
	 */
	@WebMethod(operationName = "redTrasformazionePDF", action = "urn:#redTrasformazionePDF")
	@WebResult(name = "redTrasformazionePDFResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedTrasformazionePDFResponseType redTrasformazionePDF(
			@WebParam(name = "redTrasformazionePDF", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedTrasformazionePDFType parameters);
	
	/**
	 * Trasforma in PDF.
	 * @param parameters
	 * @return response di trasformazione in PDF
	 */
	@WebMethod(operationName = "trasformazionePDF", action = "urn:#trasformazionePDF")
	@WebResult(name = "trasformazionePDFResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	TrasformazionePDFResponseType trasformazionePDF(
			@WebParam(name = "trasformazionePDF", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") TrasformazionePDFType parameters);
	
	/**
	 * Inserisce il protocollo in entrata.
	 * @param parameters
	 * @return response di inserimento protocollo in entrata
	 */
	@WebMethod(operationName = "inserisciProtEntrata", action = "urn:#inserisciProtEntrata")
	@WebResult(name = "inserisciProtEntrataResponseType", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	InserisciProtEntrataResponseType inserisciProtEntrata(
			@WebParam(name = "inserisciProtEntrata", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") InserisciProtEntrataType parameters);
	
	/**
	 * Inserisce il protocollo in uscita.
	 * @param parameters
	 * @return response di inserimento protocollo in uscita
	 */
	@WebMethod(operationName = "inserisciProtUscita", action = "urn:#inserisciProtUscita")
	@WebResult(name = "inserisciProtUscitaResponseType", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	InserisciProtUscitaResponseType inserisciProtUscita(
			@WebParam(name = "inserisciProtUscita", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") InserisciProtUscitaType parameters);
	
	/**
	 * Inserisce l'id.
	 * @param parameters
	 * @return response di inserimento id
	 */
	@WebMethod(operationName = "inserisciId", action = "urn:#inserisciId")
	@WebResult(name = "inserisciIdResponseType", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	InserisciIdResponseType inserisciId(
			@WebParam(name = "inserisciId", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") InserisciIdType parameters);
	
	/**
	 * Inseisce il campo firma.
	 * @param parameters
	 * @return response di inserimento capo firma
	 */
	@WebMethod(operationName = "inserisciCampoFirma", action = "urn:#inserisciCampoFirma")
	@WebResult(name = "inserisciCampoFirmaResponseType", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	InserisciCampoFirmaResponseType inserisciCampoFirma(
			@WebParam(name = "inserisciCampoFirma", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") InserisciCampoFirmaType parameters);
	
	/**
	 * Inserisce la postilla.
	 * @param parameters
	 * @return response di inserimento postilla
	 */
	@WebMethod(operationName = "inserisciPostilla", action = "urn:#inserisciPostilla")
	@WebResult(name = "inserisciPostillaResponseType", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	InserisciPostillaResponseType inserisciPostilla(
			@WebParam(name = "inserisciPostilla", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") InserisciPostillaType parameters);
	
	/**
	 * Inserisce watermark.
	 * @param parameters
	 * @return response di inserimento watermark
	 */
	@WebMethod(operationName = "inserisciWatermark", action = "urn:#inserisciWatermark")
	@WebResult(name = "inserisciWatermarkResponseType", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	InserisciWatermarkResponseType inserisciWatermark(
			@WebParam(name = "inserisciWatermark", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") InserisciWatermarkType parameters);
	
	/**
	 * Inserisce l'approvazione.
	 * @param parameters
	 * @return response di inserimento approvazione
	 */
	@WebMethod(operationName = "inserisciApprovazione", action = "urn:#inserisciApprovazione")
	@WebResult(name = "inserisciApprovazioneResponseType", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	InserisciApprovazioneResponseType inserisciApprovazione(
			@WebParam(name = "inserisciApprovazione", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") InserisciApprovazioneType parameters);
	
	/**
	 * Ottiene l'elenco delle properties.
	 * @param parameters
	 * @return response di recupero properties
	 */
	@WebMethod(operationName = "redElencoProperties", action = "urn:#redElencoProperties")
	@WebResult(name = "redElencoPropertiesResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedElencoPropertiesResponseType redElencoProperties(
			@WebParam(name = "redElencoProperties", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedElencoPropertiesType parameters);
	
	/**
	 * Modifica i metadati.
	 * @param parameters
	 * @return response di modifica metadati
	 */
	@WebMethod(operationName = "redModificaMetadati", action = "urn:#redModificaMetadati")
	@WebResult(name = "redModificaMetadatiResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedModificaMetadatiResponseType redModificaMetadati(
			@WebParam(name = "redModificaMetadati", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedModificaMetadatiType parameters);
	
	/**
	 * Conserva il documento.
	 * @param parameters
	 * @return response di conservazione documento
	 */
	@WebMethod(operationName = "redConservaDocumento", action = "urn:#redConservaDocumento")
	@WebResult(name = "redConservaDocumentoResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedConservaDocumentoResponseType redConservaDocumento(
			@WebParam(name = "redConservaDocumento", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedConservaDocumentoType parameters);
	
	/**
	 * Modifica la security.
	 * @param parameters
	 * @return response di modifica security
	 */
	@WebMethod(operationName = "redModificaSecurity", action = "urn:#redModificaSecurity")
	@WebResult(name = "redModificaSecurityResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedModificaSecurityResponseType redModificaSecurity(
			@WebParam(name = "redModificaSecurity", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedModificaSecurityType parameters);
	
	/**
	 * Ottiene i contatti.
	 * @param parameters
	 * @return response di recupero contatti
	 */
	@WebMethod(operationName = "redGetContatti", action = "urn:#redGetContatti")
	@WebResult(name = "redGetContattiResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedGetContattiResponseType redGetContatti(
			@WebParam(name = "redGetContatti", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedGetContattiType parameters);
	
	/**
	 * Ottiene il protocollo NPS in modo sincrono.
	 * @param parameters
	 * @return response di recupero protocollo NPS
	 */
	@WebMethod(operationName = "RedProtocolloNPSSync", action = "urn:#redProtocolloNPSSync")
	@WebResult(name = "redProtocolloNPSSyncResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	RedProtocolloNPSSyncResponseType redProtocolloNPSSync(
			@WebParam(name = "redProtocolloNPSSync", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") RedProtocolloNPSSyncType parameters);
	
	/**
	 * Metodo per l'associazione del documento.
	 * @param parameters
	 * @return response di associazione documento
	 */
	@WebMethod(operationName = "AssociateDocumentoProtocolloToAsyncNps", action = "urn:#associateDocumentoProtocolloToAsyncNps")
	@WebResult(name = "associateDocumentoProtocolloToAsyncNpsResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	AssociateDocumentoProtocolloToAsyncNpsResponseType associateDocumentoProtocolloToAsyncNps(
			@WebParam(name = "associateDocumentoProtocolloToAsyncNps", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") AssociateDocumentoProtocolloToAsyncNpsType parameters);

	/**
	 * Metodo per l'upload del documento.
	 * @param parameters
	 * @return response di upload documento
	 */
	@WebMethod(operationName = "UploadDocToAsyncNps", action = "urn:#uploadDocToAsyncNps")
	@WebResult(name = "uploadDocToAsyncNpsResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	UploadDocToAsyncNpsResponseType uploadDocToAsyncNps(
			@WebParam(name = "uploadDocToAsyncNps", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") UploadDocToAsyncNpsType parameters);
	
	/**
	 * Aggiunge allacci in modo asincrono.
	 * @param parameters
	 * @return response di aggiunta allacci
	 */
	@WebMethod(operationName = "AggiungiAllacciAsync", action = "urn:#aggiungiAllacciAsync")
	@WebResult(name = "aggiungiAllacciAsyncResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	AggiungiAllacciAsyncResponseType aggiungiAllacciAsync(
			@WebParam(name = "aggiungiAllacciAsync", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") AggiungiAllacciAsyncType parameters);
	
	/**
	 * Spedisce il protocollo in uscita in modo asincrono.
	 * @param parameters
	 * @return response di spedizione protocollo in uscita
	 */
	@WebMethod(operationName = "SpedisciProtocolloUscitaAsync", action = "urn:#spedisciProtocolloUscitaAsync")
	@WebResult(name = "spedisciProtocolloUscitaAsyncResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	SpedisciProtocolloUscitaAsyncResponseType spedisciProtocolloUscitaAsync(
			@WebParam(name = "spedisciProtocolloUscitaAsync", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") SpedisciProtocolloUscitaAsyncType parameters);

	/**
	 * Gestisce i destinatari interni.
	 * @param parameters
	 * @return response di gestione destinatari interni
	 */
	@WebMethod(operationName = "GestisciDestinatariInterni", action = "urn:#gestisciDestinatariInterni")
	@WebResult(name = "gestisciDestinatariInterniResponse", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters")
	GestisciDestinatariInterniResponseType gestisciDestinatariInterni(
			@WebParam(name = "gestisciDestinatariInterni", targetNamespace = "http://messages.types.documentservice.model.webservice.red.ibm.it", partName = "parameters") GestisciDestinatariInterniType parameters);

		
}