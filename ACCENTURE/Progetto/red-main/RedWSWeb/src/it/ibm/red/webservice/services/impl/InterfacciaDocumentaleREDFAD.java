package it.ibm.red.webservice.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.soap.SOAPException;
import javax.xml.ws.WebServiceContext;

import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.facade.IAooFacadeSRV;
import it.ibm.red.webservice.handlers.LoggingHandler;
import it.ibm.red.webservice.model.redfad.interfacciadocumentaleredfad.SecurityFault;
import it.ibm.red.webservice.model.redfad.interfacciadocumentaleredfadtypes_1.EsitoType;
import it.ibm.red.webservice.model.redfad.interfacciadocumentaleredfadtypes_1.RichiestaTerminaFlussoAttoDecretoType;
import it.ibm.red.webservice.model.redfad.interfacciadocumentaleredfadtypes_1.RispostaTerminaFlussoAttoDecretoType;
import it.ibm.red.webservice.model.redfad.interfacciadocumentaleredfadtypes_1.RisultatoTerminaFlussoAttoDecretoType;
import it.ibm.red.webservice.model.redfad.interfacciadocumentaleredfadtypes_1.ServiceErrorType;
import it.ibm.red.webservice.services.IInterfacciaDocumentaleREDFAD;

@WebService(
		portName = "InterfacciaDocumentaleREDFADPort",
        serviceName = "InterfacciaDocumentaleREDFAD",
        targetNamespace = "http://redwsevo.mef.gov.it/ibm/InterfacciaDocumentaleREDFAD",
        endpointInterface = "it.ibm.red.webservice.services.IInterfacciaDocumentaleREDFAD")
@HandlerChain(file = "/handlersREDFAD.xml")
public class InterfacciaDocumentaleREDFAD implements IInterfacciaDocumentaleREDFAD {
	
	/**
	 * Key recupero password.
	 */
	private static final String PASSWORD_KEY = "Password";

	/**
	 * Key recupero utente.
	 */
	private static final String USER_KEY = "Utente";

	/**
	 * Key recupero client.
	 */
	private static final String CLIENT_KEY = "Client";

	/**
	 * Key recupero del servizio.
	 */
	private static final String SERVICE_KEY = "Servizio";

	/**
	 * Key recupero dell'applicazione.
	 */
	private static final String APPLICATION_KEY = "Applicazione";

	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(InterfacciaDocumentaleREDFAD.class);
	
	/**
	 * Context.
	 */
	@Resource
    private WebServiceContext context;

	/**
	 * Servizio.
	 */
	private IAooFacadeSRV aooSRV;
	
	/**
	 * Costruttore.
	 */
	public InterfacciaDocumentaleREDFAD() {
		try {
			aooSRV = ApplicationContextProvider.getApplicationContext().getBean(IAooFacadeSRV.class);
		} catch (final Exception e) {
			logger.error("Errore in fase di costruzione del servizio", e);
		}
	}

	/**
	 * Termina flusso atto decreto.
	 * @param request
	 * @return response
	 * @throws SOAPException, SecurityFault
	 */
	@Override
	public RispostaTerminaFlussoAttoDecretoType terminaFlussoAttoDecreto(final RichiestaTerminaFlussoAttoDecretoType request) throws SecurityFault, SOAPException {
		RispostaTerminaFlussoAttoDecretoType risposta = null;
		try {
			risposta = new RispostaTerminaFlussoAttoDecretoType();

			//dumb security
			if (context.getMessageContext().get(APPLICATION_KEY) == null || !context.getMessageContext().get(APPLICATION_KEY).equals(APPLICATION_KEY)) {
				throw new SecurityFault("APPLICAZIONE NON AUTORIZZATA");
			}
			if (context.getMessageContext().get(SERVICE_KEY) == null || !context.getMessageContext().get(SERVICE_KEY).equals(SERVICE_KEY)) {
				throw new SecurityFault("SERVIZIO NON AUTORIZZATO");
			}
			if (context.getMessageContext().get(CLIENT_KEY) == null || !context.getMessageContext().get(CLIENT_KEY).equals(CLIENT_KEY)) {
				throw new SecurityFault("CLIENT NON AUTORIZZATO");
			}
			if (context.getMessageContext().get(USER_KEY) == null || !context.getMessageContext().get(USER_KEY).equals(USER_KEY)) {
				throw new SecurityFault("UTENTE NON RICONOSCIUTO");
			}
			if (context.getMessageContext().get(PASSWORD_KEY) == null || !context.getMessageContext().get(PASSWORD_KEY).equals(PASSWORD_KEY)) {
				throw new SecurityFault("PASSWORD ERRATA");
			}
			
			final List<RisultatoTerminaFlussoAttoDecretoType> param =  new ArrayList<>();
	 		for (final String idFascicoloRaccoltaProvvisoria : request.getIdFascicoloRaccoltaProvvisoria()) {
	 			logger.info("processing " + idFascicoloRaccoltaProvvisoria);
	 			
	 			final RisultatoTerminaFlussoAttoDecretoType risultatoOperazione = gestisciFascicoloRaccoltaProvvisoria(idFascicoloRaccoltaProvvisoria);
	 			param.add(risultatoOperazione);
	 		}
	 		risposta.getStatoOperazione().addAll(param);
	 		
		} catch (final Exception e) {
 			final RisultatoTerminaFlussoAttoDecretoType risultatoOperazione = new RisultatoTerminaFlussoAttoDecretoType();
			logger.error(e.getMessage(), e);
			risultatoOperazione.setEsito(EsitoType.ERRORE_DATI_NON_PRESENTI);
			final ServiceErrorType serviceError = new ServiceErrorType();
			serviceError.setErrorCode(-1);
			serviceError.setErrorMessageString(e.getMessage());
			risultatoOperazione.getErrorList().add(serviceError);
	 		
			if (risposta == null) {
				throw new RedException("risposta null prima della verifica dello stato operazione.");
			}
			
			risposta.getStatoOperazione().add(risultatoOperazione);
	 		LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
 		return risposta;
	}

	/**
	 * @param idFascicoloRaccoltaProvvisoria
	 * @return esito operazioni
	 */
	private RisultatoTerminaFlussoAttoDecretoType gestisciFascicoloRaccoltaProvvisoria(final String idFascicoloRaccoltaProvvisoria) {
		final RisultatoTerminaFlussoAttoDecretoType risultatoOperazione = new RisultatoTerminaFlussoAttoDecretoType();
		risultatoOperazione.setIdFascicoloRaccoltaProvvisoria(idFascicoloRaccoltaProvvisoria);
		try {
			//recupera l'AOO
			final Aoo aoo = aooSRV.recuperaAoo(Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IDAOO_RGS)));
			
			//inizializza accesso al content engine in modalità administrator
			final AooFilenet aooFilenet = aoo.getAooFilenet();
			
			final FilenetPEHelper pe = new FilenetPEHelper(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), 
					aooFilenet.getStanzaJaas(), aooFilenet.getConnectionPoint());
			
			pe.terminaAttoDecreto(idFascicoloRaccoltaProvvisoria);
			risultatoOperazione.setEsito(EsitoType.OK);
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.OK);
		} catch (final Exception e) {
			logger.error(e.getMessage(), e);
			risultatoOperazione.setEsito(EsitoType.ERRORE_DATI_NON_PRESENTI);
			final ServiceErrorType serviceError = new ServiceErrorType();
			serviceError.setErrorCode(-1);
			serviceError.setErrorMessageString(e.getMessage());
			risultatoOperazione.getErrorList().add(serviceError);
			LoggingHandler.setEsito(context.getMessageContext(), LoggingHandler.KO);
		}
		return risultatoOperazione;
	}
}