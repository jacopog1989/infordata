package it.ibm.red.webservice.services.documentservice.validator;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.webservice.model.documentservice.dto.ErrorCode;
import it.ibm.red.webservice.model.documentservice.types.documentale.Allegato;
import it.ibm.red.webservice.model.documentservice.types.documentale.Destinatario;
import it.ibm.red.webservice.model.documentservice.types.documentale.TipoDestinatario;
import it.ibm.red.webservice.model.documentservice.types.documentale.TipologiaPDFOutput;
import it.ibm.red.webservice.model.documentservice.types.messages.AggiungiAllacciAsyncType;
import it.ibm.red.webservice.model.documentservice.types.messages.AssociateDocumentoProtocolloToAsyncNpsType;
import it.ibm.red.webservice.model.documentservice.types.messages.BaseContentRequestType;
import it.ibm.red.webservice.model.documentservice.types.messages.BaseRedContentRequestType;
import it.ibm.red.webservice.model.documentservice.types.messages.GestisciDestinatariInterniType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciApprovazioneType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciCampoFirmaType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciIdType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciPostillaType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciProtEntrataType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciProtType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciProtUscitaType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciWatermarkType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedAssociaFascicoloATitolarioType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedAssociaFascicoloFaldoneType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedAvanzaWorkflowType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedAvviaIstanzaWorkflowType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCambiaStatoFascicoloType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCheckInType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCheckOutType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedConservaDocumentoType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCreaFaldoneType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCreaFascicoloType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCreazioneDocumentoEntrataType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCreazioneDocumentoUscitaType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedElencoPropertiesType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedElencoVersioniDocumentoType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedEliminaFaldoneType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedEliminaFascicoloType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedFascicolazioneDocumentoType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedGetContattiType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedGetOrganigrammaType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedGetVersioneDocumentoType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciApprovazioneType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciCampoFirmaType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciIdType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciPostillaType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciProtEntrataType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciProtType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciProtUscitaType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciWatermarkType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedModificaFascicoloType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedModificaMetadatiType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedModificaSecurityType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedProtocolloNPSSyncType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedRicercaAvanzataType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedRicercaDocumentiType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedRicercaFaldoniType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedRicercaFascicoliType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedRimuoviAssociazioneFascicoloFaldoneType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedSpostaFascicoloFaldoneType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedTrasformazionePDFType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedUndoCheckOutType;
import it.ibm.red.webservice.model.documentservice.types.messages.SpedisciProtocolloUscitaAsyncType;
import it.ibm.red.webservice.model.documentservice.types.messages.TrasformazionePDFType;
import it.ibm.red.webservice.model.documentservice.types.messages.UploadDocToAsyncNpsType;
import it.ibm.red.webservice.model.documentservice.types.ricerca.EntitaRicerca;
import it.ibm.red.webservice.model.documentservice.types.ricerca.OperatoriRicerca;
import it.ibm.red.webservice.model.documentservice.types.ricerca.ParametriRicerca;
import it.ibm.red.webservice.model.documentservice.types.rubrica.ContattoBase;
import it.ibm.red.webservice.model.documentservice.types.security.CredenzialiWS;
import it.ibm.red.webservice.model.documentservice.types.security.Gruppo;
import it.ibm.red.webservice.model.documentservice.types.security.Security;
import it.ibm.red.webservice.model.documentservice.types.security.Utente;

public final class ValidatorUtility {

	/**
	 * Messaggio di errore validazione.
	 */
	private static final String ERROR_VALIDAZIONE_MSG = "[DocumentService][redProtocolloNPSSync] Errore di validazione: ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ValidatorUtility.class);

	private ValidatorUtility() {

	}

	private static ErrorCode validaCredenziali(final CredenzialiWS credenziali) {
		ErrorCode erroreValidazioneCredenziali = null;

		if (StringUtils.isBlank(credenziali.getIdClient())) {
			erroreValidazioneCredenziali = ErrorCode.ID_CLIENT_NON_PRESENTE;
		} else if (StringUtils.isBlank(credenziali.getPwdClient())) {
			erroreValidazioneCredenziali = ErrorCode.PWD_CLIENT_NON_PRESENTE;
		}

		if (erroreValidazioneCredenziali != null) {
			LOGGER.warn("[DocumentService][validaCredenziali] Errore di validazione: " + erroreValidazioneCredenziali.getDescription());
		}

		return erroreValidazioneCredenziali;
	}

	/**
	 * Validazione creazione documento in uscita.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedCreazioneDocumentoUscita(final RedCreazioneDocumentoUscitaType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null) {

				// Destinatari
				boolean almenoUnDestinatarioElettronico = false;
				if (!CollectionUtils.isEmpty(request.getDestinatari())) {
					for (final Destinatario dest : request.getDestinatari()) {
						final ContattoBase nuovoContattoDestinatario = dest.getDestinatario();

						// Destinatario già presente in rubrica (ID destinatario) oppure interno (ID
						// Ufficio)
						if (dest.getIdDestinatario() == null && dest.getIdUfficio() == null
								&& (nuovoContattoDestinatario == null || StringUtils.isBlank(nuovoContattoDestinatario.getNome()))) {
							erroreValidazione = ErrorCode.DESTINATARIO_ID_NON_PRESENTE;
							throw new RedException(erroreValidazione.getDescription());
						}

						// Tipo destinatario
						if (dest.getTipoDestinatario() == null || ((dest.getIdUfficio() == null && TipoDestinatario.INTERNO.equals(dest.getTipoDestinatario()))
								|| (dest.getIdUfficio() != null && !TipoDestinatario.INTERNO.equals(dest.getTipoDestinatario())))) {
							erroreValidazione = ErrorCode.DESTINATARIO_TIPO_ERRATO;
							throw new RedException(erroreValidazione.getDescription());
						}

						// TO/CC (se il destinatario è elettronico)
						if ((TipoDestinatario.PEO.equals(dest.getTipoDestinatario()) || TipoDestinatario.PEC.equals(dest.getTipoDestinatario()))) {
							almenoUnDestinatarioElettronico = true;

							if (dest.getToCc() == null) {
								erroreValidazione = ErrorCode.DESTINATARIO_TIPO_SPEDIZIONE_ERRATO;
								throw new RedException(erroreValidazione.getDescription());
							}
						}

						// Destinatario NON presente in rubrica
						if (nuovoContattoDestinatario != null) {
							// Nome / Ragione Sociale
							if (StringUtils.isBlank(nuovoContattoDestinatario.getNome())) {
								erroreValidazione = ErrorCode.DESTINATARIO_NOME_RAGIONE_SOCIALE_NON_PRESENTE;
								throw new RedException(erroreValidazione.getDescription());
							}

							// Tipo Persona
							if (StringUtils.isBlank(nuovoContattoDestinatario.getTipoPersona()) || (!"F".equalsIgnoreCase(nuovoContattoDestinatario.getTipoPersona()))
									&& !"G".equalsIgnoreCase(nuovoContattoDestinatario.getTipoPersona())) {
								erroreValidazione = ErrorCode.DESTINATARIO_TIPO_PERSONA_NON_PRESENTE_ERRATO;
								throw new RedException(erroreValidazione.getDescription());
							}

							// Cognome
							if ("F".equalsIgnoreCase(nuovoContattoDestinatario.getTipoPersona()) && StringUtils.isBlank(nuovoContattoDestinatario.getCognome())) {
								erroreValidazione = ErrorCode.DESTINATARIO_COGNOME_NON_PRESENTE;
								throw new RedException(erroreValidazione.getDescription());
							}

							// Se il destinatario è elettronico, deve essere presente almeno un indirizzo
							// e-mail
							if ((TipoDestinatario.PEO.equals(dest.getTipoDestinatario()) && StringUtils.isBlank(nuovoContattoDestinatario.getMail()))
									|| (TipoDestinatario.PEC.equals(dest.getTipoDestinatario()) && StringUtils.isBlank(nuovoContattoDestinatario.getMailPec()))) {
								erroreValidazione = ErrorCode.DESTINATARIO_MAIL_NON_PRESENTE;
								throw new RedException(erroreValidazione.getDescription());
							}
						}
					}
				}

				if (request.getIdIter() == null) {
					erroreValidazione = ErrorCode.ID_ITER_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdIter() == 0) {
					if (request.getIdTipoAssegnazione() == null) {
						erroreValidazione = ErrorCode.ID_TIPO_ASSEGNAZIONE_NON_PRESENTE;
						throw new RedException(erroreValidazione.getDescription());
					}

					if (request.getAssegnatarioCompetenza() == null) {
						erroreValidazione = ErrorCode.ASSEGNATARIO_NON_PRESENTE;
						throw new RedException(erroreValidazione.getDescription());
					}
				}

				if (StringUtils.isBlank(request.getIndiceClassificazione())) {
					erroreValidazione = ErrorCode.INDICE_CLASSIFICAZIONE_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (almenoUnDestinatarioElettronico) {
					if (StringUtils.isBlank(request.getMittenteMail())) {
						erroreValidazione = ErrorCode.MITTENTE_MAIL_NON_PRESENTE;
						throw new RedException(erroreValidazione.getDescription());
					}

					if (StringUtils.isBlank(request.getOggettoMail())) {
						erroreValidazione = ErrorCode.OGGETTO_MAIL_NON_PRESENTE;
						throw new RedException(erroreValidazione.getDescription());
					}

					if (StringUtils.isBlank(request.getTestoMail())) {
						erroreValidazione = ErrorCode.TESTO_MAIL_NON_PRESENTE;
						throw new RedException(erroreValidazione.getDescription());
					}
				}

				erroreValidazione = validaDocumentoBase(request.getNomeFile(), request.getDocType(), request.getDataHandler(), request.getOggetto(),
						request.getIdTipologiaDocumento(), request.getIdTipologiaProcedimento(), request.getIdUtenteCreatore(), request.getIdUfficioCreatore());

			}
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redCreazioneDocumentoUscita] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione creazione documento entrata RED.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedCreazioneDocumentoEntrata(final RedCreazioneDocumentoEntrataType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request -> START
			if (erroreValidazione == null) {

				if (request.getIdMittente() == null) {
					erroreValidazione = ErrorCode.ID_MITTENTE_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				// Allegati
				if (!CollectionUtils.isEmpty(request.getAllegati())) {
					for (final Allegato allegato : request.getAllegati()) {
						if (Boolean.TRUE.equals(allegato.isDaFirmare())) {
							erroreValidazione = ErrorCode.ALLEGATO_DOC_ENTRATA_DA_FIRMARE;
							throw new RedException(erroreValidazione.getDescription());
						}
					}
				}

				// Assegnatario
				if (request.getAssegnatarioCompetenza() == null) {
					erroreValidazione = ErrorCode.ASSEGNATARIO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				erroreValidazione = validaDocumentoBase(request.getNomeFile(), request.getDocType(), request.getDataHandler(), request.getOggetto(),
						request.getIdTipologiaDocumento(), request.getIdTipologiaProcedimento(), request.getIdUtenteCreatore(), request.getIdUfficioCreatore());

			}
			// Validazione della request -> END
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redCreazioneDocumentoEntrata] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	private static ErrorCode validaDocumentoBase(final String nomeFile, final String contentType, final byte[] content, final String oggetto,
			final Integer idTipologiaDocumento, final Integer idTipoProcedimento, final Integer idUtenteCreatore, final Integer idUfficioCreatore) {
		ErrorCode erroreValidazione = null;

		try {
			if (StringUtils.isBlank(contentType)) {
				erroreValidazione = ErrorCode.CONTENT_TYPE_NON_PRESENTE;
				throw new RedException(erroreValidazione.getDescription());
			}

			if (content == null || content.length == 0) {
				erroreValidazione = ErrorCode.CONTENT_NON_PRESENTE;
				throw new RedException(erroreValidazione.getDescription());
			}

			if (StringUtils.isBlank(nomeFile)) {
				erroreValidazione = ErrorCode.NOME_FILE_NON_PRESENTE;
				throw new RedException(erroreValidazione.getDescription());
			}

			if (StringUtils.isBlank(oggetto)) {
				erroreValidazione = ErrorCode.OGGETTO_NON_PRESENTE;
				throw new RedException(erroreValidazione.getDescription());
			}

			if (idTipologiaDocumento == null) {
				erroreValidazione = ErrorCode.ID_TIPOLOGIA_DOCUMENTO_NON_PRESENTE;
				throw new RedException(erroreValidazione.getDescription());
			}

			if (idTipoProcedimento == null) {
				erroreValidazione = ErrorCode.ID_TIPOLOGIA_PROCEDIMENTO_NON_PRESENTE;
				throw new RedException(erroreValidazione.getDescription());
			}

			if (idUtenteCreatore == null) {
				erroreValidazione = ErrorCode.ID_UTENTE_CREATORE_NON_PRESENTE;
				throw new RedException(erroreValidazione.getDescription());
			}

			if (idUfficioCreatore == null) {
				erroreValidazione = ErrorCode.ID_UFFICIO_CREATORE_NON_PRESENTE;
				throw new RedException(erroreValidazione.getDescription());
			}

			erroreValidazione = validaFormatoDocumento(nomeFile, contentType);

		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][validaDocumentoBase] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	private static ErrorCode validaFormatoDocumento(final String nomeFile, final String contentType) {
		ErrorCode erroreValidazione = null;

		// Formato ed estensione del documento
		if ((ContentType.DOC.equalsIgnoreCase(contentType) && !nomeFile.toLowerCase().endsWith(".doc"))
				|| (ContentType.DOCX.equalsIgnoreCase(contentType) && !nomeFile.toLowerCase().endsWith(".docx"))
				|| (ContentType.PDF.equalsIgnoreCase(contentType) && !nomeFile.toLowerCase().endsWith(".pdf"))) {

			erroreValidazione = ErrorCode.FORMATO_DOC_NON_COERENTE;

		} else if (!ContentType.DOC.equalsIgnoreCase(contentType) && !ContentType.DOCX.equalsIgnoreCase(contentType) && !ContentType.PDF.equalsIgnoreCase(contentType)) {

			erroreValidazione = ErrorCode.FORMATO_DOC_NON_GESTITO;

		}

		if (erroreValidazione != null) {
			LOGGER.warn("[DocumentService][validaFormatoDocumento] Errore di validazione: " + erroreValidazione.getDescription());
		}

		return erroreValidazione;
	}

	/**
	 * Validazione fascicolazione documento RED.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedFascicolazioneDocumento(final RedFascicolazioneDocumentoType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null) {

				if (request.getDocumentTitle() == null || request.getDocumentTitle() <= 0) {
					erroreValidazione = ErrorCode.ID_DOCUMENTO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdFascicolo() == null || request.getIdFascicolo() <= 0) {
					erroreValidazione = ErrorCode.ID_FASCICOLO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

			}
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redCreazioneDocumentoUscita] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione ricerca documenti RED.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedRicercaDocumenti(final RedRicercaDocumentiType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null) {

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getAnnoRicerca() == null || request.getAnnoRicerca() <= 0) {
					erroreValidazione = ErrorCode.ANNO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (StringUtils.isBlank(request.getValueString())) {
					erroreValidazione = ErrorCode.PARAMETRO_RICERCA_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getCampoRicerca() == null) {
					erroreValidazione = ErrorCode.CAMPO_RICERCA_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

			}
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redRicercaDocumenti] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione ricerca fascicoli RED.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedRicercaFascicoli(final RedRicercaFascicoliType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null) {

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (StringUtils.isBlank(request.getValueString())) {
					erroreValidazione = ErrorCode.PARAMETRO_RICERCA_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

			}
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redRicercaFascicoli] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione ricerca faldoni RED.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedRicercaFaldoni(final RedRicercaFaldoniType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null) {

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (StringUtils.isBlank(request.getValueString())) {
					erroreValidazione = ErrorCode.PARAMETRO_RICERCA_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

			}
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redRicercaFaldoni] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione eliminazione fascicolo.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedEliminaFascicolo(final RedEliminaFascicoloType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null) {

				if (StringUtils.isEmpty(request.getIdFascicolo())) {
					erroreValidazione = ErrorCode.ID_FASCICOLO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if ((request.getIdAoo() == null || request.getIdAoo() <= 0) && StringUtils.isEmpty(request.getCodiceAoo())) {
					erroreValidazione = ErrorCode.PARAMETRI_AOO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

			}
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redEliminaFascicolo] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione recupero organigramma.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedGetOrganigramma(final RedGetOrganigrammaType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null && (request.getIdAoo() == null || request.getIdAoo() <= 0) && StringUtils.isEmpty(request.getCodiceAoo())) {
				erroreValidazione = ErrorCode.PARAMETRI_AOO_NON_VALIDO;
				throw new RedException(erroreValidazione.getDescription());
			}
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redGetOrganigramma] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione cambia stato fascicolo.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedCambiaStatoFascicolo(final RedCambiaStatoFascicoloType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null) {

				if (StringUtils.isEmpty(request.getIdFascicolo())) {
					erroreValidazione = ErrorCode.ID_FASCICOLO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getStato() == null || (request.getStato() != 0 && request.getStato() != 1)) {
					erroreValidazione = ErrorCode.STATO_FASCICOLO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdnodo() == null || request.getIdnodo() <= 0) {
					erroreValidazione = ErrorCode.ID_NODO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

			}
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redCambiaStatoFascicolo] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione modifica fascicolo.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedModificaFascicolo(final RedModificaFascicoloType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null) {

				if (StringUtils.isEmpty(request.getIdFascicolo())) {
					erroreValidazione = ErrorCode.ID_FASCICOLO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getStato() != null && request.getStato() != 0 && request.getStato() != 1) {
					erroreValidazione = ErrorCode.STATO_FASCICOLO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdnodo() == null || request.getIdnodo() <= 0) {
					erroreValidazione = ErrorCode.ID_NODO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getStato() == null && StringUtils.isEmpty(request.getOggetto())) {
					erroreValidazione = ErrorCode.PARAMETRO_MODIFICA_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

			}
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redModificaFascicolo] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione associazione fascicolo titolario.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedAssociaFascicoloATitolario(final RedAssociaFascicoloATitolarioType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null) {

				if (StringUtils.isEmpty(request.getIdFascicolo())) {
					erroreValidazione = ErrorCode.ID_FASCICOLO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (StringUtils.isEmpty(request.getIdTitolario())) {
					erroreValidazione = ErrorCode.INDICE_CLASSIFICAZIONE_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdnodo() == null || request.getIdnodo() <= 0) {
					erroreValidazione = ErrorCode.ID_NODO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

			}
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redAssociaFascicoloATitolario] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione creazione fascicolo.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedCreaFascicolo(final RedCreaFascicoloType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null) {

				if (StringUtils.isEmpty(request.getIdTitolario())) {
					erroreValidazione = ErrorCode.INDICE_CLASSIFICAZIONE_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (StringUtils.isEmpty(request.getOggetto())) {
					erroreValidazione = ErrorCode.OGGETTO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdnodo() == null || request.getIdnodo() <= 0) {
					erroreValidazione = ErrorCode.ID_NODO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

			}
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redCreaFascicolo] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione inserimento protocollo entrata.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedInserisciProtEntrata(final RedInserisciProtEntrataType request) {
		ErrorCode erroreValidazione = null;

		try {

			erroreValidazione = validaRedInserisciProt(request);

		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redInserisciProtEntrata] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione inserimento protocollo uscita.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedInserisciProtUscita(final RedInserisciProtUscitaType request) {
		ErrorCode erroreValidazione = null;

		try {

			erroreValidazione = validaRedInserisciProt(request);

		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redInserisciProtUscita] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione inserimento campo firma.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedInserisciCampoFirma(final RedInserisciCampoFirmaType request) {
		ErrorCode erroreValidazione = null;

		try {

			erroreValidazione = validaBaseRedContentRequestType(request);

			// Validazione specifica per la request
			if (erroreValidazione == null && CollectionUtils.isEmpty(request.getFirmatari())) {
				erroreValidazione = ErrorCode.ID_FIRMATARIO_NON_PRESENTE;
				throw new RedException(erroreValidazione.getDescription());
			}

		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redInserisciCampoFirma] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione inserimento postilla.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedInserisciPostilla(final RedInserisciPostillaType request) {
		ErrorCode erroreValidazione = null;

		try {

			erroreValidazione = validaBaseRedContentRequestType(request);

		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redInserisciPostilla] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione inserimento id.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedInserisciId(final RedInserisciIdType request) {
		ErrorCode erroreValidazione = null;

		try {
			erroreValidazione = validaBaseRedContentRequestType(request);

			// Validazione specifica per la request
			if (erroreValidazione == null && StringUtils.isBlank(request.getIdDocumentoStampigliatura())) {
				erroreValidazione = ErrorCode.ID_DOCUMENTO_STAMPIGLIATURA_NON_PRESENTE;
				throw new RedException(erroreValidazione.getDescription());
			}

		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redInserisciId] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione inserimento watermark.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedInserisciWatermark(final RedInserisciWatermarkType request) {
		ErrorCode erroreValidazione = null;

		try {

			erroreValidazione = validaBaseRedContentRequestType(request);

			// Validazione specifica per la request
			if (erroreValidazione == null && StringUtils.isBlank(request.getTesto())) {
				erroreValidazione = ErrorCode.TESTO_WATERMARK_NON_PRESENTE;
				throw new RedException(erroreValidazione.getDescription());
			}

		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redInserisciWatermark] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione inserimento approvazione.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedInserisciApprovazione(final RedInserisciApprovazioneType request) {
		ErrorCode erroreValidazione = null;

		try {

			erroreValidazione = validaBaseRedContentRequestType(request);

			// Validazione specifica per la request
			if (erroreValidazione == null && StringUtils.isBlank(request.getTesto())) {
				erroreValidazione = ErrorCode.TESTO_APPROVAZIONE_NON_PRESENTE;
				throw new RedException(erroreValidazione.getDescription());
			}

		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redInserisciApprovazione] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione eliminazione faldone.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedEliminaFaldone(final RedEliminaFaldoneType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null) {

				if (StringUtils.isEmpty(request.getIdFaldone())) {
					erroreValidazione = ErrorCode.ID_FALDONE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdNodo() == null || request.getIdNodo() <= 0) {
					erroreValidazione = ErrorCode.ID_NODO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

			}
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redEliminaFaldone] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione creazione faldone.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedCreaFaldone(final RedCreaFaldoneType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null) {

				if (request.getIdnodo() == null || request.getIdnodo() <= 0) {
					erroreValidazione = ErrorCode.ID_NODO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (StringUtils.isEmpty(request.getOggettoFaldone())) {
					erroreValidazione = ErrorCode.OGGETTO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

			}
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redCreaFaldone] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione associazione fascicolo faldone.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedAssociaFascicoloFaldone(final RedAssociaFascicoloFaldoneType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null) {

				if (request.getIdnodo() == null || request.getIdnodo() <= 0) {
					erroreValidazione = ErrorCode.ID_NODO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (StringUtils.isEmpty(request.getIdFascicolo())) {
					erroreValidazione = ErrorCode.ID_FASCICOLO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (StringUtils.isEmpty(request.getNomeFaldone())) {
					erroreValidazione = ErrorCode.NOME_FALDONE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

			}
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redAssociaFascicoloFaldone] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione rimozione associazione fascicolo faldone.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedRimuoviAssociazioneFascicoloFaldone(final RedRimuoviAssociazioneFascicoloFaldoneType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null) {

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdFascicolo() == null || request.getIdFascicolo() <= 0) {
					erroreValidazione = ErrorCode.ID_FASCICOLO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdFaldone() == null || request.getIdFaldone() <= 0) {
					erroreValidazione = ErrorCode.ID_FALDONE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

			}
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redRimuoviAssociazioneFascicoloFaldone] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione sopostamento fascicolo faldone.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedSpostaFascicoloFaldone(final RedSpostaFascicoloFaldoneType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null) {

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdFascicolo() == null || request.getIdFascicolo() <= 0) {
					erroreValidazione = ErrorCode.ID_FASCICOLO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdFaldoneOld() == null || request.getIdFaldoneOld() <= 0) {
					erroreValidazione = ErrorCode.ID_FALDONE_OLD_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdFaldoneNew() == null || request.getIdFaldoneNew() <= 0) {
					erroreValidazione = ErrorCode.ID_FALDONE_NEW_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

			}
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redSpostaFascicoloFaldone] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione elenco versioni documento.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedElencoVersioniDocumento(final RedElencoVersioniDocumentoType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null) {

				if (request.getIdNodo() == null || request.getIdNodo() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_NODO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getDocumentTitle() == null || request.getDocumentTitle() <= 0) {
					erroreValidazione = ErrorCode.ID_DOCUMENTO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}
			}
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redElencoVersioniDocumento] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione recupero versione documento.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedGetVersioneDocumento(final RedGetVersioneDocumentoType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null) {

				if (request.getIdNodo() == null || request.getIdNodo() <= 0) {
					erroreValidazione = ErrorCode.ID_NODO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getDocumentTitle() == null || request.getDocumentTitle() <= 0) {
					erroreValidazione = ErrorCode.ID_DOCUMENTO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getNumeroVersione() == null) {
					erroreValidazione = ErrorCode.NUMERO_VERSIONE_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}
			}
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redGetVersioneDocumento] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione checkin.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedCheckIn(final RedCheckInType request) {
		// Validazione delle credenziali
		ErrorCode erroreValidazione = validaCredenziali(request.getCredenzialiWS());

		try {

			if (erroreValidazione == null) {

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (!StringUtils.isNotBlank(request.getDocumento().getIdDocumento())) {
					erroreValidazione = ErrorCode.ID_DOCUMENTO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getDocumento().getDataHandler() == null || request.getDocumento().getDataHandler().length == 0) {
					erroreValidazione = ErrorCode.CONTENT_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

			}

		} catch (final Exception e) {
			LOGGER.warn("[DocumentService][redCheckIn] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione checkout.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedCheckOut(final RedCheckOutType request) {
		// Validazione delle credenziali
		ErrorCode erroreValidazione = null;

		try {

			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			if (erroreValidazione == null) {

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (!StringUtils.isNotBlank(request.getDocumentTitle())) {
					erroreValidazione = ErrorCode.ID_DOCUMENTO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

			}

		} catch (final Exception e) {
			LOGGER.warn("[DocumentService][redCheckOut] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione undo checkout.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedUndoCheckOut(final RedUndoCheckOutType request) {
		// Validazione delle credenziali
		ErrorCode erroreValidazione = null;

		try {

			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			if (erroreValidazione == null) {

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (!StringUtils.isNotBlank(request.getDocumentTitle())) {
					erroreValidazione = ErrorCode.ID_DOCUMENTO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

			}

		} catch (final Exception e) {
			LOGGER.warn("[DocumentService][redUndoCheckOut] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione ricerca avanzata.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedRicercaAvanzata(final RedRicercaAvanzataType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null) {

				if (request.getIdUtente() == null || request.getIdUtente() < 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getEntitaRicerca() == null) {
					erroreValidazione = ErrorCode.ENTITA_RICERCA_AV_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (EntitaRicerca.DOCUMENTO.equals(request.getEntitaRicerca()) && StringUtils.isBlank(request.getClasseDocumentale())) {
					erroreValidazione = ErrorCode.CLASSE_DOCUMENTALE_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (!EntitaRicerca.DOCUMENTO.equals(request.getEntitaRicerca()) && !EntitaRicerca.FASCICOLO.equals(request.getEntitaRicerca())
						&& !EntitaRicerca.EMAIL.equals(request.getEntitaRicerca())) {
					erroreValidazione = ErrorCode.ENTITA_RICERCA_AV_NON_GESTITA;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (StringUtils.isBlank(request.getWhereCondition())) {
					final List<ParametriRicerca> paramsRicerca = request.getParametriRicerca();

					if (CollectionUtils.isEmpty(paramsRicerca) && StringUtils.isBlank(request.getFullTextAnd()) && StringUtils.isBlank(request.getFullTextOr())) {
						erroreValidazione = ErrorCode.PARAMETRI_RICERCA_AV_NON_PRESENTI;
						throw new RedException(erroreValidazione.getDescription());
					}

					// In caso di FULL TEXT, si possono ricercare solo documenti
					if ((StringUtils.isNotBlank(request.getFullTextAnd()) || StringUtils.isNotBlank(request.getFullTextOr()))
							&& !EntitaRicerca.DOCUMENTO.equals(request.getEntitaRicerca())) {
						erroreValidazione = ErrorCode.ENTITA_RICERCA_AV_FULL_TEXT_NON_VALIDA;
						throw new RedException(erroreValidazione.getDescription());
					}

					if (!CollectionUtils.isEmpty(paramsRicerca)) {
						for (int paramsIndex = 0; paramsIndex < paramsRicerca.size(); paramsIndex++) {
							final ParametriRicerca paramRicerca = paramsRicerca.get(paramsIndex);

							if (StringUtils.isBlank(paramRicerca.getChiave())) {
								erroreValidazione = ErrorCode.CHIAVE_RICERCA_AV_NON_PRESENTE;
								throw new RedException(erroreValidazione.getDescription());
							}

							if (paramRicerca.getOperatoreRicerca() == null) {
								erroreValidazione = ErrorCode.OPERATORE_RICERCA_AV_NON_PRESENTE;
								throw new RedException(erroreValidazione.getDescription());
							}

							if ((!OperatoriRicerca.UGUALE.equals(paramRicerca.getOperatoreRicerca()) && !OperatoriRicerca.DIVERSO.equals(paramRicerca.getOperatoreRicerca()))
									&& StringUtils.isBlank(paramRicerca.getValore())) {
								erroreValidazione = ErrorCode.VALORE_PARAMETRO_RICERCA_AV_NON_PRESENTE;
								throw new RedException(erroreValidazione.getDescription());
							}

							if (paramsIndex < (paramsRicerca.size() - 1)) {
								if (paramRicerca.getOperatoreLogico() == null) {
									erroreValidazione = ErrorCode.OPERATORE_LOGICO_SUCCESSIVO_RICERCA_AV_NON_PRESENTE;
									throw new RedException(erroreValidazione.getDescription());
								}
							} else {
								if (paramRicerca.getOperatoreLogico() != null) {
									erroreValidazione = ErrorCode.OPERATORE_LOGICO_SUCCESSIVO_RICERCA_AV_NON_VALIDO;
									throw new RedException(erroreValidazione.getDescription());
								}
							}
						}
					}
				}
			}
		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redRicercaAvanzata] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione avanzamento workflow.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedAvanzaWorkflow(final RedAvanzaWorkflowType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null) {

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getWorkflow() == null) {
					erroreValidazione = ErrorCode.WORK_FLOW_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (StringUtils.isBlank(request.getWorkflow().getWorkFlowNumber())) {
					erroreValidazione = ErrorCode.WORK_FLOW_NUMBER_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (StringUtils.isBlank(request.getWorkflow().getResponse())) {
					erroreValidazione = ErrorCode.RESPONSE_NAME_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

			}

		} catch (final Exception e) {
			LOGGER.warn("[DocumentService][redAvanzaWorkflow] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione avviamento istanza workflow.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedAvviaIstanzaWorkflow(final RedAvviaIstanzaWorkflowType request) {
		// Validazione delle credenziali
		ErrorCode erroreValidazione = null;

		try {

			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			if (erroreValidazione == null) {

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getWorkflow() == null) {
					erroreValidazione = ErrorCode.WORK_FLOW_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (StringUtils.isBlank(request.getWorkflow().getName())) {
					erroreValidazione = ErrorCode.WORK_FLOW_NAME_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

			}

		} catch (final Exception e) {
			LOGGER.warn("[DocumentService][redAvviaIstanzaWorkflow] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione trasformazione Pdf RED.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedTrasformazionePdf(final RedTrasformazionePDFType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione delle credenziali
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			// Validazione della request
			if (erroreValidazione == null && (request.getIdUtente() == null || request.getIdUtente() <= 0)) {
				erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
				throw new RedException(erroreValidazione.getDescription());
			}
		} catch (final Exception e) {
			LOGGER.warn("[DocumentService][redTrasformazionePdf] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione trasformazione Pdf.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaTrasformazionePdf(final TrasformazionePDFType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione base
			erroreValidazione = validaBaseContentRequestType(request);

			if (erroreValidazione == null) {

				if (request.getTipologiaOutput() == null) {
					erroreValidazione = ErrorCode.TIPOLOGIA_PDF_OUTPUT_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdDocumentoDaStampigliare() != null && !TipologiaPDFOutput.PDF_ID_DOC.equals(request.getTipologiaOutput())) {
					erroreValidazione = ErrorCode.TIPOLOGIA_PDF_OUTPUT_NON_COERENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

			}
		} catch (final Exception e) {
			LOGGER.warn("[DocumentService][trasformazionePdf] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione inserimento protocollo entrata.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaInserisciProtEntrata(final InserisciProtEntrataType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione della request
			erroreValidazione = validaInserisciProt(request);

		} catch (final Exception e) {
			LOGGER.warn("[DocumentService][inserisciProtEntrata] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione inserimento protocollo uscita.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaInserisciProtUscita(final InserisciProtUscitaType request) {
		ErrorCode erroreValidazione = null;

		try {
			// Validazione della request
			erroreValidazione = validaInserisciProt(request);

		} catch (final Exception e) {
			LOGGER.warn("[DocumentService][inserisciProtUscita] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione inserimento ID.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaInserisciId(final InserisciIdType request) {
		ErrorCode erroreValidazione = null;

		try {
			erroreValidazione = validaBaseContentRequestType(request);

			// Validazione specifica per la request
			if (erroreValidazione == null && StringUtils.isBlank(request.getIdDocumentoStampigliatura())) {
				erroreValidazione = ErrorCode.ID_DOCUMENTO_STAMPIGLIATURA_NON_PRESENTE;
				throw new RedException(erroreValidazione.getDescription());
			}

		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][inserisciId] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione creazione documento in uscita.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaInserisciCampoFirma(final InserisciCampoFirmaType request) {
		ErrorCode erroreValidazione = null;

		try {
			erroreValidazione = validaBaseContentRequestType(request);

			// Validazione specifica per la request
			if (erroreValidazione == null) {

				if (CollectionUtils.isEmpty(request.getFirmatari())) {
					erroreValidazione = ErrorCode.ID_FIRMATARIO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getAltezzaFooter() == null || request.getSpaziaturaFirma() == null) {
					erroreValidazione = ErrorCode.PARAMETRI_FIRMA_NON_PRESENTI;
					throw new RedException(erroreValidazione.getDescription());
				}

			}

		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][inserisciCampoFirma] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione inserimento postilla.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaInserisciPostilla(final InserisciPostillaType request) {
		ErrorCode erroreValidazione = null;

		try {

			erroreValidazione = validaBaseContentRequestType(request);

		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][inserisciPostilla] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione inserimento watermark.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaInserisciWatermark(final InserisciWatermarkType request) {
		ErrorCode erroreValidazione = null;

		try {
			erroreValidazione = validaBaseContentRequestType(request);

			// Validazione specifica per la request
			if (erroreValidazione == null && StringUtils.isBlank(request.getTesto())) {
				erroreValidazione = ErrorCode.TESTO_WATERMARK_NON_PRESENTE;
				throw new RedException(erroreValidazione.getDescription());
			}

		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][inserisciWatermark] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione inserimento approvazione.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaInserisciApprovazione(final InserisciApprovazioneType request) {
		ErrorCode erroreValidazione = null;

		try {
			erroreValidazione = validaBaseContentRequestType(request);

			// Validazione specifica per la request
			if (erroreValidazione == null && StringUtils.isBlank(request.getTesto())) {
				erroreValidazione = ErrorCode.TESTO_APPROVAZIONE_NON_PRESENTE;
				throw new RedException(erroreValidazione.getDescription());
			}

		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][inserisciApprovazione] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione elenco properties.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedElencoProperties(final RedElencoPropertiesType request) {
		ErrorCode erroreValidazione = null;

		try {

			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			if (erroreValidazione == null && (request.getIdUtente() == null || request.getIdUtente() <= 0)) {
				erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
				throw new RedException(erroreValidazione.getDescription());
			}

		} catch (final RedException e) {
			LOGGER.warn("[DocumentService][redElencoProperties] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione modifica metadati RED.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedModificaMetadati(final RedModificaMetadatiType request) {
		ErrorCode erroreValidazione = null;

		try {

			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			if (erroreValidazione == null) {

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdAoo() == null || request.getIdAoo() <= 0) {
					erroreValidazione = ErrorCode.PARAMETRI_AOO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (!StringUtils.isNotBlank(request.getDocumento().getIdDocumento())) {
					erroreValidazione = ErrorCode.ID_DOCUMENTO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getDocumento().getClasseDocumentale() == null || request.getDocumento().getClasseDocumentale().getMetadato() == null
						|| request.getDocumento().getClasseDocumentale().getMetadato().isEmpty()) {
					erroreValidazione = ErrorCode.METADATI_DOCUMENTO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

			}

		} catch (final Exception e) {
			LOGGER.warn("[DocumentService][redModificaMetadati] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione conservazione documento RED.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedConservaDocumento(final RedConservaDocumentoType request) {
		ErrorCode erroreValidazione = null;

		try {

			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			if (erroreValidazione == null) {

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdAoo() == null || request.getIdAoo() <= 0) {
					erroreValidazione = ErrorCode.PARAMETRI_AOO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (!StringUtils.isNotBlank(request.getDocumentTitle())) {
					erroreValidazione = ErrorCode.ID_DOCUMENTO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

			}

		} catch (final Exception e) {
			LOGGER.warn("[DocumentService][redConservaDocumento] Errore di validazione", e);
		}

		return erroreValidazione;
	}

	/**
	 * Validazione modifica security RED.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedModificaSecurity(final RedModificaSecurityType request) {
		ErrorCode erroreValidazione = null;

		try {

			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			if (erroreValidazione == null) {

				if (request.getIdUtente() == null || request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdAoo() == null || request.getIdAoo() <= 0) {
					erroreValidazione = ErrorCode.PARAMETRI_AOO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (!StringUtils.isNotBlank(request.getDocumentTitle())) {
					erroreValidazione = ErrorCode.ID_DOCUMENTO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getSecurity() == null || request.getSecurity().isEmpty()) {
					erroreValidazione = ErrorCode.SECURITY_DOCUMENTO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				erroreValidazione = validaSecurity(request.getSecurity());
				if (erroreValidazione != null) {
					throw new RedException(erroreValidazione.getDescription());
				}

			}

		} catch (final Exception e) {
			LOGGER.warn("[DocumentService][redModificaSecurity] Errore di validazione", e);
		}

		return erroreValidazione;

	}

	/**
	 * Validazione recupero contatti RED.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaRedGetContatti(final RedGetContattiType request) {
		ErrorCode erroreValidazione = null;

		try {

			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			if (erroreValidazione == null) {

				if (request.getIdAoo() == null || request.getIdAoo() <= 0) {
					erroreValidazione = ErrorCode.PARAMETRI_AOO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.isRicercaRED() == null) {
					erroreValidazione = ErrorCode.FILTRO_RUBRICA_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.isRicercaIPA() == null) {
					erroreValidazione = ErrorCode.FILTRO_RUBRICA_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.isRicercaMEF() == null) {
					erroreValidazione = ErrorCode.FILTRO_RUBRICA_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

			}

		} catch (final Exception e) {
			LOGGER.warn("[DocumentService][redGetContatti] Errore di validazione", e);
		}

		return erroreValidazione;

	}

	private static ErrorCode validaSecurity(final List<Security> securityRequest) {
		ErrorCode erroreValidazione = null;

		for (final Security security : securityRequest) {

			if (security == null) {
				erroreValidazione = ErrorCode.NO_OBJECT_SECURITY;
			} else {

				final Gruppo g = security.getGruppo();
				final Utente u = security.getUtente();

				if (g == null || g.getIdUfficio() == null || g.getIdUfficio() <= 0) {
					erroreValidazione = ErrorCode.NO_GROUPPO_SECURITY;
				}

				if (u == null || u.getIdUtente() == null || u.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.NO_UTENTE_SECURITY;
				}
			}

			if (erroreValidazione != null) {
				break;
			}
		}

		return erroreValidazione;
	}

	private static ErrorCode validaInserisciProt(final InserisciProtType request) {
		ErrorCode erroreValidazione = null;

		// Validazione della request
		erroreValidazione = validaBaseContentRequestType(request);

		if (erroreValidazione == null && StringUtils.isBlank(request.getProtocolloStampigliatura())) {
			erroreValidazione = ErrorCode.PROTOCOLLO_STAMPIGLIATURA_NON_PRESENTE;
			throw new RedException(erroreValidazione.getDescription());
		}

		return erroreValidazione;
	}

	private static ErrorCode validaBaseContentRequestType(final BaseContentRequestType request) {
		// Validazione delle credenziali
		ErrorCode erroreValidazione = validaCredenziali(request.getCredenzialiWS());

		// Validazione della request
		if (erroreValidazione == null) {

			if (request.getContent() == null || request.getContent().length <= 0) {
				erroreValidazione = ErrorCode.CONTENT_NON_PRESENTE;
				throw new RedException(erroreValidazione.getDescription());
			}

			if (StringUtils.isBlank(request.getContentType())) {
				erroreValidazione = ErrorCode.CONTENT_TYPE_NON_PRESENTE;
				throw new RedException(erroreValidazione.getDescription());
			}

			if (StringUtils.isBlank(request.getNomeFile())) {
				erroreValidazione = ErrorCode.NOME_FILE_NON_PRESENTE;
				throw new RedException(erroreValidazione.getDescription());
			}

		}

		return erroreValidazione;
	}

	private static ErrorCode validaRedInserisciProt(final RedInserisciProtType request) {
		// Validazione della request base
		ErrorCode erroreValidazione = validaBaseRedContentRequestType(request);

		// Validazione della request specifica
		if (erroreValidazione == null && StringUtils.isBlank(request.getProtocolloStampigliatura())) {
			erroreValidazione = ErrorCode.PROTOCOLLO_STAMPIGLIATURA_NON_PRESENTE;
			throw new RedException(erroreValidazione.getDescription());
		}

		return erroreValidazione;
	}

	private static ErrorCode validaBaseRedContentRequestType(final BaseRedContentRequestType request) {
		// Validazione delle credenziali
		ErrorCode erroreValidazione = validaCredenziali(request.getCredenzialiWS());

		// Validazione della request
		if (erroreValidazione == null) {

			if ((request.getIdAoo() == null || request.getIdAoo() <= 0) && StringUtils.isEmpty(request.getCodiceAoo())) {
				erroreValidazione = ErrorCode.PARAMETRI_AOO_NON_VALIDO;
				throw new RedException(erroreValidazione.getDescription());
			}

			if (request.getDocumentTitle() == null || request.getDocumentTitle() <= 0) {
				erroreValidazione = ErrorCode.ID_DOCUMENTO_NON_PRESENTE;
				throw new RedException(erroreValidazione.getDescription());
			}
		}

		return erroreValidazione;
	}

	/**
	 * Validazione protocollo NPS sync.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaProtocolloNPSSync(final RedProtocolloNPSSyncType request) {
		ErrorCode erroreValidazione = null;

		try {
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			if (erroreValidazione == null) {

				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(request.getWobNumber())) {
					erroreValidazione = ErrorCode.WOB_NUMBER_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(request.getDocumentTitle())) {
					erroreValidazione = ErrorCode.DOCUMENTTITLE_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}
				if (request.getIdAoo() <= 0) {
					erroreValidazione = ErrorCode.PARAMETRI_AOO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}
				if (request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdUfficio() < 0) {
					erroreValidazione = ErrorCode.ID_UFFICIO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}
			}

		} catch (final Exception e) {
			LOGGER.warn(ERROR_VALIDAZIONE_MSG, e);
		}

		return erroreValidazione;

	}

	/**
	 * Validazione associazione documento protocollo NPS.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaAssociateDocumentoProtocolloToAsyncNps(final AssociateDocumentoProtocolloToAsyncNpsType request) {
		ErrorCode erroreValidazione = null;

		try {
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			if (erroreValidazione == null) {
				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(request.getDocumentTitle())) {
					erroreValidazione = ErrorCode.DOCUMENTTITLE_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(request.getProtocolloGuid())) {
					erroreValidazione = ErrorCode.DOCUMENTTITLE_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(request.getOggetto())) {
					erroreValidazione = ErrorCode.OGGETTO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdAOO() <= 0) {
					erroreValidazione = ErrorCode.PARAMETRI_AOO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdUfficio() < 0) {
					erroreValidazione = ErrorCode.ID_UFFICIO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}
				if (request.getIdRuolo() <= 0) {
					erroreValidazione = ErrorCode.ID_RUOLO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(request.getNumeroAnnoProtocollo())) {
					erroreValidazione = ErrorCode.ANNO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}
			}

		} catch (final Exception e) {
			LOGGER.warn("[DocumentService][associateDocumentoProtocolloToAsyncNps] Errore di validazione", e);
		}

		return erroreValidazione;

	}

	/**
	 * Validazione upload documento NPS.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaUploadDocToAsyncNps(final UploadDocToAsyncNpsType request) {
		ErrorCode erroreValidazione = null;

		try {
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			if (erroreValidazione == null) {
				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(request.getDocumentTitle())) {
					erroreValidazione = ErrorCode.DOCUMENTTITLE_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(request.getProtocolloGuid())) {
					erroreValidazione = ErrorCode.DOCUMENTTITLE_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(request.getOggetto())) {
					erroreValidazione = ErrorCode.OGGETTO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}
				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(request.getNumeroAnnoProtocollo())) {
					erroreValidazione = ErrorCode.ANNO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdAOO() <= 0) {
					erroreValidazione = ErrorCode.PARAMETRI_AOO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}
				if (request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdUfficio() < 0) {
					erroreValidazione = ErrorCode.ID_UFFICIO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}
				if (request.getIdRuolo() <= 0) {
					erroreValidazione = ErrorCode.ID_RUOLO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}
			}

		} catch (final Exception e) {
			LOGGER.warn(ERROR_VALIDAZIONE_MSG, e);
		}

		return erroreValidazione;

	}

	/**
	 * Validazione aggiunta allacci.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaAggiungiAllacciAsync(final AggiungiAllacciAsyncType request) {
		ErrorCode erroreValidazione = null;

		try {
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			if (erroreValidazione == null) {
				if (request.getIdAOO() <= 0) {
					erroreValidazione = ErrorCode.PARAMETRI_AOO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}
				if (request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}
				if (request.getIdUfficio() < 0) {
					erroreValidazione = ErrorCode.ID_UFFICIO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}
				if (request.getIdRuolo() <= 0) {
					erroreValidazione = ErrorCode.ID_RUOLO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(request.getNumeroAnnoProtocollo())) {
					erroreValidazione = ErrorCode.ANNO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(request.getIdProtocollo())) {
					erroreValidazione = ErrorCode.DOCUMENTTITLE_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}
			}

		} catch (final Exception e) {
			LOGGER.warn(ERROR_VALIDAZIONE_MSG, e);
		}

		return erroreValidazione;

	}

	/**
	 * Validazione spedizione protocollo uscita Async.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaSpedisciProtocolloUscitaAsync(final SpedisciProtocolloUscitaAsyncType request) {
		ErrorCode erroreValidazione = null;

		try {
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			if (erroreValidazione == null) {

				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(request.getDataSpedizione())) {
					erroreValidazione = ErrorCode.DATA_SPED_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(request.getNumeroAnnoProtocollo())) {
					erroreValidazione = ErrorCode.ANNO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(request.getDocumentTitle())) {
					erroreValidazione = ErrorCode.DOCUMENTTITLE_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(request.getProtocolloGuid())) {
					erroreValidazione = ErrorCode.DOCUMENTTITLE_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(request.getInfoProtocollo())) {
					erroreValidazione = ErrorCode.INFO_PROT_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdAOO() <= 0) {
					erroreValidazione = ErrorCode.PARAMETRI_AOO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}
				if (request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdUfficio() < 0) {
					erroreValidazione = ErrorCode.ID_UFFICIO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}
				if (request.getIdRuolo() <= 0) {
					erroreValidazione = ErrorCode.ID_RUOLO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}
			}

		} catch (final Exception e) {
			LOGGER.warn(ERROR_VALIDAZIONE_MSG, e);
		}

		return erroreValidazione;

	}

	/**
	 * Validazione gestione destinatari interni.
	 * 
	 * @param request
	 * @return errore eventualmente riscontrato, se null la validazione ha avuto
	 *         esito positivo
	 */
	public static ErrorCode validaGestisciDestinatariInterni(final GestisciDestinatariInterniType request) {
		ErrorCode erroreValidazione = null;

		try {
			erroreValidazione = validaCredenziali(request.getCredenzialiWS());

			if (erroreValidazione == null) {

				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(request.getWobNumber())) {
					erroreValidazione = ErrorCode.WOB_NUMBER_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(request.getDocumentTitle())) {
					erroreValidazione = ErrorCode.DOCUMENTTITLE_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}
				if (request.getIdAoo() <= 0) {
					erroreValidazione = ErrorCode.PARAMETRI_AOO_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}
				if (request.getIdUtente() <= 0) {
					erroreValidazione = ErrorCode.ID_UTENTE_NON_VALIDO;
					throw new RedException(erroreValidazione.getDescription());
				}

				if (request.getIdUfficio() < 0) {
					erroreValidazione = ErrorCode.ID_UFFICIO_NON_PRESENTE;
					throw new RedException(erroreValidazione.getDescription());
				}

			}

		} catch (final Exception e) {
			LOGGER.warn("[DocumentService][gestisciDestinatariInterni] Errore di validazione", e);
		}

		return erroreValidazione;

	}

}
