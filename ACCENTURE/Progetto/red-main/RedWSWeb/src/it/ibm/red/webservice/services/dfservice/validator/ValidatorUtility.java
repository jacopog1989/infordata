package it.ibm.red.webservice.services.dfservice.validator;

import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.webservice.model.dfservice.dto.ErrorCode;
import it.ibm.red.webservice.model.dfservice.types.messages.RedMappingOrganigrammaType;
import it.ibm.red.webservice.model.documentservice.types.security.CredenzialiWS;

public final class ValidatorUtility {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ValidatorUtility.class);

	private ValidatorUtility() {
		
	}
	
	private static ErrorCode validaCredenziali(final CredenzialiWS credenziali) {
		ErrorCode erroreValidazioneCredenziali = null;
		if (StringUtils.isBlank(credenziali.getIdClient())) {
			erroreValidazioneCredenziali = ErrorCode.ID_CLIENT_NON_PRESENTE;
		} else if (StringUtils.isBlank(credenziali.getPwdClient())) {
			erroreValidazioneCredenziali = ErrorCode.PWD_CLIENT_NON_PRESENTE;
		}
		if (erroreValidazioneCredenziali != null) {
			LOGGER.warn("[DfService][validaCredenziali] Errore di validazione: " + erroreValidazioneCredenziali.getDescription());
		}
		return erroreValidazioneCredenziali;
	}

	/**
	 * Valida credenziali e request.
	 * @param request
	 * @return errorCode enum
	 */
	public static ErrorCode validaRedMappingOrganigramma(final RedMappingOrganigrammaType request) {
		// Validazione delle credenziali
		ErrorCode erroreValidazione = validaCredenziali(request.getCredenzialiWS());

		// Validazione della request
		if (erroreValidazione == null && StringUtils.isEmpty(request.getCodAoo())) {
			erroreValidazione = ErrorCode.COD_AOO_NON_PRESENTE;
			throw new RedException(erroreValidazione.getDescription());
		}
		return erroreValidazione;
	}
}
