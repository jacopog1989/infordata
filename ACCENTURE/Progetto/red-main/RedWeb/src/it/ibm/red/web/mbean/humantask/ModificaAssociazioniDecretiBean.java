package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.DecretoDirigenzialeDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IFepaFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;

/**
 * Bean che gestisce la modifica dell'associazione decreti.
 */
@Named(ConstantsWeb.MBean.MODIFICA_ASSOCIAZIONI_DECRETI_BEAN)
@ViewScoped
public class ModificaAssociazioniDecretiBean extends AbstractAssociazioniDecretiResponse {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 7241483325857176721L;
	
	/**
	 * Messaggio errore sollecito response.
	 */
	private static final String ERROR_SOLLECITO_RESPONSE_MSG = "Si è verificato un errore durante il tentato sollecito della response ";
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ModificaAssociazioniDecretiBean.class.getName());

	/**
	 * Post construct del bean.
	 */
	@PostConstruct
	@Override
	protected void postConstruct() {
		super.postConstruct();
	}

	/**
	 * Esegue la response associata al bean.
	 */
	@Override
	public void doResponse() {
		EsitoOperazioneDTO eOpe = null;
		String wobNumber = null;
		final List<String> wobNumbersDaAggiungere = new ArrayList<>();
		final List<String> wobNumbersDaRimuovere = new ArrayList<>();

		try {
			final IFepaFacadeSRV fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);
			//pulisco gli esiti
			ldb.cleanEsiti();
			//prendo il wobnumber
			wobNumber = documentiSelezionati.get(0).getWobNumber();
			//Prendo i decreti selezionati
			final List<DecretoDirigenzialeDTO> decretiSelezionati = decretiDisponibiliTab.getDecretiDTH().getSelectedMasters();
			
			//prendo i wobnumbers da aggiungere
			for (final DecretoDirigenzialeDTO decreto : decretiSelezionati) {
				if (decreto.getWobNumber() != null) {
					wobNumbersDaAggiungere.add(decreto.getWobNumber());
				}
			}
			
			//prendo i decreti non selezionati (e quindi da rimuovere)
			final List<DecretoDirigenzialeDTO> decretiNonSelezionati = (List<DecretoDirigenzialeDTO>) decretiDisponibiliTab.getDecretiDTH().getMasters();
			decretiNonSelezionati.removeAll(decretiSelezionati);
			//prendo i wobnumbers dei decreti non selezionati
			for (final DecretoDirigenzialeDTO decreto : decretiNonSelezionati) {
				if (decreto.getWobNumber() != null) {
					wobNumbersDaRimuovere.add(decreto.getWobNumber());
				}
			}
			eOpe = fepaSRV.modificaAssociazioni(utente, wobNumber, wobNumbersDaAggiungere, wobNumbersDaRimuovere);
			
			//setto gli esiti con i nuovi e eseguo un refresh
			ldb.getEsitiOperazione().add(eOpe);
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.MODIFICA_ASSOCIAZIONI_DECRETI_BEAN);			
			
		} catch (final Exception e) {
			LOGGER.error(ERROR_SOLLECITO_RESPONSE_MSG + ldb.getSelectedResponse().getDisplayName(), e);
			showError(ERROR_SOLLECITO_RESPONSE_MSG + ldb.getSelectedResponse().getDisplayName());
		}

	}
	
	/**
	 * Esegue la messa agli atti gestendo eventuali errori. A valle della procedura viene mostrato l'esito delle azioni.
	 */
	public void mettiAgliAtti() {
		EsitoOperazioneDTO eOpe = null;
		String wobNumber = null;
		
		try {
			final IFepaFacadeSRV fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);
			//pulisco gli esiti
			ldb.cleanEsiti();
			//prendo il wobnumber
			wobNumber = documentiSelezionati.get(0).getWobNumber();
			
			eOpe = fepaSRV.mettiAgliAttiSysFineLavorazione(wobNumber, utente);
			
			ldb.getEsitiOperazione().add(eOpe);
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.MODIFICA_ASSOCIAZIONI_DECRETI_BEAN);	
		} catch (final Exception e) {
			LOGGER.error(ERROR_SOLLECITO_RESPONSE_MSG + ldb.getSelectedResponse().getDisplayName(), e);
			showError(ERROR_SOLLECITO_RESPONSE_MSG + ldb.getSelectedResponse().getDisplayName());
		}
	}
	
	/**
	 * @return flagMettiAgliAtti
	 */
	public boolean isFlagMettiAgliAtti() {
		return flagMettiAgliAtti;
	}
	
	/**
	 * @param flagMettiAgliAtti
	 */
	public void setFlagMettiAgliAtti(final boolean flagMettiAgliAtti) {
		this.flagMettiAgliAtti = flagMettiAgliAtti;
	}
}