package it.ibm.red.web.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ITitolarioFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.SessionBean;

/**
 * Converter titolario.
 */
@FacesConverter("titolarioConverter")
public class TitolarioConverter implements Converter {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TitolarioConverter.class.getName());

	/**
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.String).
	 */
	@Override
	public Object getAsObject(final FacesContext fc, final UIComponent uic, final String value) {
		TitolarioDTO tReturn = null;
		
		try {
			final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
			final UtenteDTO utente = sessionBean.getUtente();
			final ITitolarioFacadeSRV titolarioSRV = ApplicationContextProvider.getApplicationContext().getBean(ITitolarioFacadeSRV.class);
			tReturn = titolarioSRV.getNodoByIndice(utente.getIdAoo(), value);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}
		
		return tReturn;
		
	}

	/**
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.Object).
	 */
	@Override
	public String getAsString(final FacesContext fc, final UIComponent uic, final Object object) {
		if (object == null) {
			return null;
		}
		if (object instanceof TitolarioDTO) {
			final TitolarioDTO t = (TitolarioDTO) object;
			return t.getIndiceClassificazione();
		}
		return object.toString();
	}

	
}
