package it.ibm.red.web.mbean.interfaces;

import java.util.List;

import org.primefaces.model.StreamedContent;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.VersionDTO;
import it.ibm.red.web.dto.ListElementDTO;

/**
 * Interfaccia del componente del tab documenti.
 * @author a.difolca
 *
 */
public interface IDocumentManagerDataTabDocumenti {
	
	/**
	 * La lista delle versioni principali del documento.
	 * @return
	 */
	List<VersionDTO> getVersioni();
	
	/**
	 * Verifica la firma per il documento e aggiorna il campo di verifica.
	 * 
	 * @param index
	 *  indice dell'elemento nella coda.
	 */
	void aggiornaVerificaFirma(VersionDTO v);
	
	
	/**
	 * Consente il download della versione corrispondente.
	 * @param index
	 */
	StreamedContent downloadVersioneDocumentoPrincipale(VersionDTO v);
	
	/**
	 * Permette di aggiornare la verifica della firma dell'allegato.
	 * @param v
	 *  Versione selezionata dall'utente.
	 */
	void aggiornaVerificaFirmaAllegato(VersionDTO v);
	
	/**
	 * Consente di scaricare la versione del DTO selelzionata.
	 * @param v
	 *  versione selezionata.
	 */
	StreamedContent downloadVersioneDocumentoAllegato(VersionDTO v);
		
	/**
	 * Nome del file principale.
	 * 
	 * @return
	 * 
	 *  Il nome del file principale.
	 */
	String getNomeFile();

	/**
	 * Ritorna gli allegati del documento.
	 * @return
	 */
	List<ListElementDTO<AllegatoDTO>> getAllegati();
}
