package it.ibm.red.web.listener;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import it.ibm.red.business.tlocal.CurrentUserTL;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Listener dei timeout di sessione.
 */
public class SessionTimeoutListener implements PhaseListener {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 2639152532235352192L;

	/**
	 * @see javax.faces.event.PhaseListener#afterPhase(javax.faces.event.PhaseEvent).
	 */
	@Override
	public void afterPhase(final PhaseEvent ev) {
		// Questo metodo è lasciato intenzionalmente vuoto.
	}

	/**
	 * @see javax.faces.event.PhaseListener#beforePhase(javax.faces.event.PhaseEvent).
	 */
	@Override
	public void beforePhase(final PhaseEvent ev) {
		FacesHelper.executeJS("resetSessionTimer()");
		final String trackID = (String) FacesHelper.getObjectFromSession("TRACK_ID", true);
		CurrentUserTL.setValue(trackID); 
	}

	/**
	 * @see javax.faces.event.PhaseListener#getPhaseId().
	 */
	@Override
	public PhaseId getPhaseId() {
		return PhaseId.RESTORE_VIEW;
	}

}
