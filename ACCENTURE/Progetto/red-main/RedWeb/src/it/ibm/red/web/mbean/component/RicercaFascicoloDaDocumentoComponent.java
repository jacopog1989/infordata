package it.ibm.red.web.mbean.component;

import java.util.ArrayList;
import java.util.List;

import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.DocumentiEFascicoliDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.facade.IRicercaAvanzataDocFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.mbean.component.AbstractComponent;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.FascicoloManagerBean;
import it.ibm.red.web.mbean.interfaces.IUpdatableDetailCaller;

/**
 * Classe per la ricerca dei fascicoli a partire dai documenti e per la loro rappresentazione grafica.
 * @author 
 */

public class RicercaFascicoloDaDocumentoComponent extends AbstractComponent {
	
	/**
	 * Serial Version
	 */
	private static final long serialVersionUID = 6929738454442724598L;
	
	/**
	 * LOGGER
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaFascicoloDaDocumentoComponent.class);
	
	/**
	 * Parametri di ricerca dei documenti
	 */
	private ParamsRicercaAvanzataDocDTO documenti;
	
	/**
	 * Servizio.
	 */
	private final IRicercaAvanzataDocFacadeSRV ricercaAvanzataDocumentiSRV;
	
	/**
	 * Servizio.
	 */
	private final IFascicoloSRV fascicoloSRV; 
	
	/**
	 * Datatable risultati.
	 */
	private SimpleDetailDataTableHelper<MasterDocumentRedDTO> risultatiRicercaDTH;
	
	/**
	 * Model della treeTable.
	 */
	private transient TreeNode root;
	
	/**
	 * Utente.
	 */
	private final UtenteDTO utente;
	
	/**
	 * Variabile per la renderizzazione della tabella una volta effettuata la ricerca.
	 */
	private boolean renderRicerca; 
	
	/**
	 * Bean chiamante.
	 */
	private String mBeanChiamante;

	/**
	 * Fascicolo selezionato.
	 */
	private FascicoloDTO fascicoloSelezionato;
	
	/**
	 * Costruttore.
	 */
	public RicercaFascicoloDaDocumentoComponent(final UtenteDTO inUtente) {
		this.utente = inUtente;
		documenti = new ParamsRicercaAvanzataDocDTO();
		renderRicerca = false;
		risultatiRicercaDTH = new SimpleDetailDataTableHelper<>();
		ricercaAvanzataDocumentiSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaAvanzataDocFacadeSRV.class);
		fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloSRV.class);
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param inUtente
	 */
	public RicercaFascicoloDaDocumentoComponent(final UtenteDTO inUtente, final String inBeanChiamante) {
		this(inUtente);
		this.mBeanChiamante = inBeanChiamante;
	}
	
	/**
	 * Il metodo esegue la ricerca dei documenti a partire dai parametri inseriti.
	 * @return lista dei risultati della ricerca
	 */
	public List<MasterDocumentRedDTO> ricercaDocumenti() {
		List<MasterDocumentRedDTO> risultati = new ArrayList<>();
		//anno documento è obbligatorio
		if (documenti.getAnnoDocumento() != null) {
			risultati = (List<MasterDocumentRedDTO>) ricercaAvanzataDocumentiSRV.eseguiRicercaDocumenti(documenti, utente, false, false);
			//settiamo i master con i risultati della ricerca
			risultatiRicercaDTH.setMasters(risultati);
			//crea l'albero per la treetable
			creaTreeTable(risultati);
			//renderizza la tabella
			renderRicerca = true;
		} else {
			showErrorMessage("Compilare i campi obbligatori");
		}
		return risultati;
	}
	
	/**
	 * Recupera i fascicoli che contengono il documento selezionato
	 * 
	 * @param doc - Il documento di cui si vuole recuperare la lista dei fascicoli che lo contengono  
	 * 
	 * @return lista dei fascicoli del documento
	 */
	private List<FascicoloDTO> prendiFascicoliCheContengonoDocumento(final MasterDocumentRedDTO doc) {
		return fascicoloSRV.recuperaFascicoliDelDocumento(doc, utente);
	}
	
	/****** Metodi per la costruzione del TreeTable *******/
	
	/**
	 * Il metodo crea l'albero per la treetable dei documenti e fascicoli
	 */
	public void creaTreeTable(final List<MasterDocumentRedDTO> docFigli) {
		root = new DefaultTreeNode("Root", null);
		final MasterDocumentRedDTO f = new MasterDocumentRedDTO();
		f.setOggetto("Fascicolo fittizio");
		f.setDocumentTitle("0");
		// I documenti saranno i figli di primo livello della radice
		for (final MasterDocumentRedDTO d : docFigli) {
			final DocumentiEFascicoliDTO doc = new DocumentiEFascicoliDTO(d);
			final DefaultTreeNode figlioDiRoot = new DefaultTreeNode(doc, root);
			figlioDiRoot.setExpanded(false);
			figlioDiRoot.setSelectable(false);
			figlioDiRoot.setType("documento");
			//figlio fittizio (per permettere l'expand)
			final DefaultTreeNode nodoFittizio = new DefaultTreeNode(f, figlioDiRoot);
			LOGGER.info("nodoFittizio creato di tipo " + nodoFittizio.getType() + " per il documento " + d.getDocumentTitle());
		}
	}
	
	/**
	 * Il metodo popola l'albero con i fascicoli del documento indicato
	 */
	public void attribuisciFascicoliAlDocumento(final NodeExpandEvent event) {
		final TreeNode docNode = event.getTreeNode();
		docNode.getChildren().remove(0);
		final DocumentiEFascicoliDTO doc = (DocumentiEFascicoliDTO) docNode.getData();
		final MasterDocumentRedDTO documento = new MasterDocumentRedDTO();
		documento.setDocumentTitle(doc.getDocumentTitle());
		documento.setClasseDocumentale(doc.getClasseDocumentale());
		//recuperiamo i fascicoli
		final List<FascicoloDTO> fascicoliDelDoc = prendiFascicoliCheContengonoDocumento(documento);
		for (final FascicoloDTO f : fascicoliDelDoc) {
			final DocumentiEFascicoliDTO fas = new DocumentiEFascicoliDTO(f);
			final DefaultTreeNode nodoFascicolo = new DefaultTreeNode(fas, docNode);
			nodoFascicolo.setSelectable(true);
			nodoFascicolo.setExpanded(false);
			nodoFascicolo.setType("fascicolo");
		}
	}


	//*************** Metodi per la selezione del fascicolo ********************//
	
	/**
	 * Il metodo seleziona il fascicolo indicato nella scheda Ricerca per Documenti
	 * @param event
	 */
	public void selectFascicoloDelDocumento(final NodeSelectEvent event) {
		try {
			final DocumentiEFascicoliDTO fascicolo = (DocumentiEFascicoliDTO) event.getTreeNode().getData();
			fascicoloSelezionato = fascicolo.getFascicoloOriginale();
			if (mBeanChiamante != null 
					&& (mBeanChiamante.equals(MBean.DOCUMENT_MANAGER_BEAN) || mBeanChiamante.equals(MBean.COLLEGA_CONTRIBUTO_ESTERNO_BEAN))) {
				aggiornaChiamante(fascicoloSelezionato);
			} else if (mBeanChiamante != null && mBeanChiamante.equals(MBean.FASCICOLO_MANAGER_BEAN)) {
				final FascicoloManagerBean fmb = FacesHelper.getManagedBean(mBeanChiamante);
				fmb.setFlagRicercaFascicolo(true);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, "Si è verificato un errore durante l'operazione: " + e.getMessage());
		}
	}
	
	/**
	 * Si occupa di aggiornare il Managed Bean che ha aperto la ricerca del fascicolo.
	 * 
	 * @param fascicoloProcedimentale
	 */
	private void aggiornaChiamante(final FascicoloDTO fascicoloProcedimentale) {
		resetSearch();
		final IUpdatableDetailCaller<FascicoloDTO> beanChiamante = FacesHelper.getManagedBean(mBeanChiamante);
		if (beanChiamante != null) {
			beanChiamante.updateDetail(fascicoloProcedimentale);
		}
	}
	
	
	/**
	 * Il metodo resetta la ricerca
	 */
	private void resetSearch() {
		documenti = new ParamsRicercaAvanzataDocDTO();
		renderRicerca = false;
		risultatiRicercaDTH = new SimpleDetailDataTableHelper<>();
	}
	
	/*** Getter e Setter ***/
	public ParamsRicercaAvanzataDocDTO getDocumenti() {
		return documenti;
	}

	/**
	 * Imposta i documenti.
	 * @param documenti
	 */
	public void setDocumenti(final ParamsRicercaAvanzataDocDTO documenti) {
		this.documenti = documenti;
	}
	
	/**
	 * Restituisce il datatable helper che gestisce il datatable dei risultati della ricerca.
	 * @return datatable helper risultati ricerca
	 */
	public SimpleDetailDataTableHelper<MasterDocumentRedDTO> getRisultatiRicercaDTH() {
		return risultatiRicercaDTH;
	}
	
	/**
	 * Imposta il datatable helper che gestisce il datatable dei risultati della ricerca.
	 * @param risultatiRicerca
	 */
	public void setRisultatiRicercaDTH(final SimpleDetailDataTableHelper<MasterDocumentRedDTO> risultatiRicerca) {
		this.risultatiRicercaDTH = risultatiRicerca;
	}
	
	/**
	 * Restituisce la root del tree.
	 * @return root
	 */
	public TreeNode getRoot() {
		return root;
	}
	
	/**
	 * Imposta la root del tree.
	 * @param root
	 */
	public void setRoot(final TreeNode root) {
		this.root = root;
	}
	
	/**
	 * Restituisce true se la ricerca è visibile, false altrimenti.
	 * @return true se la ricerca è visibile, false altrimenti
	 */
	public boolean isRenderRicerca() {
		return renderRicerca;
	}
	
	/**
	 * Imposta la visibilita della ricerca.
	 * @param renderRicerca
	 */
	public void setRenderRicerca(final boolean renderRicerca) {
		this.renderRicerca = renderRicerca;
	}

	/**
	 * Restituisce il fascicolo selezionato.
	 * @return fascicolo selezionato
	 */
	public FascicoloDTO getFascicoloSelezionato() {
		return fascicoloSelezionato;
	}

	/**
	 * Restituisce il nome del bean chiamante.
	 * @return nome bean chiamante
	 */
	public String getmBeanChiamante() {
		return mBeanChiamante;
	}

	/**
	 * Imposta il nome del bean chiamante.
	 * @param mBeanChiamante
	 */
	public void setmBeanChiamante(final String mBeanChiamante) {
		this.mBeanChiamante = mBeanChiamante;
	}
	
}
