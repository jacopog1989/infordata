package it.ibm.red.web.scadenziario.mbean.component;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;

import it.ibm.red.business.dto.CountPer;
import it.ibm.red.business.dto.DocumentoScadenzatoDTO;
import it.ibm.red.business.dto.PEDocumentoScadenzatoDTO;
import it.ibm.red.business.enums.PeriodoScadenzaEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IScadenziaroStrutturaFacadeSRV;
import it.ibm.red.web.document.mbean.component.AbstractComponent;
import it.ibm.red.web.scadenziario.helper.CountPerPeriodoComparator;
import it.ibm.red.web.scadenziario.mbean.ScadenziarioBean;

/**
 * Component periodo scadenza link.
 */
public class PeriodoScadenzaLinkComponent extends AbstractComponent {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 7365590576761810246L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PeriodoScadenzaLinkComponent.class.getName());

	/**
	 * Servizio.
	 */
	private final IScadenziaroStrutturaFacadeSRV scadenziarioSRV;

	/**
	 * Bean Scadenziario.
	 */
	private final ScadenziarioBean bean;

	/**
	 * Mappatura documenti/periodi.
	 */
	private Map<CountPer<PeriodoScadenzaEnum>, List<PEDocumentoScadenzatoDTO>> periodo2documenti;

	/**
	 * Lista conteggi per periodo.
	 */
	private List<CountPer<PeriodoScadenzaEnum>> orderedList;

	/**
	 * Costruttore.
	 * 
	 * @param idUfficio
	 * @param inBean
	 */
	public PeriodoScadenzaLinkComponent(final Long idUfficio, final ScadenziarioBean inBean) {
		this.bean = inBean;

		scadenziarioSRV = ApplicationContextProvider.getApplicationContext().getBean(IScadenziaroStrutturaFacadeSRV.class);

		final String msg = "Impossibile recuperare i documenti associati allo specificio periodo dello scadenziario";
		try {
			periodo2documenti = scadenziarioSRV.retrieveDocumentoInScadenzaPerPeriodoScadenza(idUfficio, bean.getUtente());

			// Questo per ritornare una lista ordinata in modo tale che sia visualizzabile
			// almeno in ordine
			orderedList = periodo2documenti.keySet().stream().sorted(new CountPerPeriodoComparator()).collect(Collectors.toList());

		} catch (final Exception e) {
			LOGGER.error(msg, e);
		}

		if (periodo2documenti == null) {
			showErrorMessage(msg);
		}
	}

	/**
	 * Costruttore.
	 * 
	 * @param scadenziarioBean
	 */
	public PeriodoScadenzaLinkComponent(final ScadenziarioBean scadenziarioBean) {
		this(scadenziarioBean.getUtente().getIdUfficio(), scadenziarioBean);
	}

	/**
	 * 
	 * Prende i peDocumento associati a quel periodo e poi recupera le informazioni
	 * mancanti per la visualizzazione
	 * 
	 * Non dovrebbe essere possibile cliccare su una riga vuota o nulla
	 * 
	 * @param inPeriodoSelezionato
	 */
	public void setPeriodoSelezionato(final CountPer<PeriodoScadenzaEnum> inPeriodoSelezionato) {

		final List<PEDocumentoScadenzatoDTO> lista = periodo2documenti.get(inPeriodoSelezionato);
		if (CollectionUtils.isEmpty(lista)) {
			LOGGER.error("Operaizione non consentita: selezionato un periodo senza documenti associati");
			showErrorMessage("Impossibile visualizzare il dettaglio documento quando non sono presenti documenti nel periodo selezionato");
			return;
		}

		final List<DocumentoScadenzatoDTO> docList = scadenziarioSRV.retrieveIdDocumentoByPeIdDocument(lista, bean.getUtente());

		final Calendar cal = Calendar.getInstance();
		// Ordinamento ascendente
		Collections.sort(docList, new Comparator<DocumentoScadenzatoDTO>() {

			@Override
			public int compare(final DocumentoScadenzatoDTO o1, final DocumentoScadenzatoDTO o2) {
				cal.setTime(o1.getCe().getDataCreazione());
				final Integer anno1 = Integer.valueOf(cal.get(Calendar.YEAR));

				cal.setTime(o2.getCe().getDataCreazione());
				final Integer anno2 = Integer.valueOf(cal.get(Calendar.YEAR));

				int dataC = anno1.compareTo(anno2);
				if (dataC == 0) {
					dataC = o1.getCe().getNumeroDocumento().compareTo(o2.getCe().getNumeroDocumento());
				}
				return dataC;
			}

		});
		// Ordinamento discendente
		Collections.reverse(docList);

		bean.setDocumentList(docList);

		// inizializzo il selected perche venga gestito correttamente il css
		periodo2documenti.keySet().stream().forEach(counter -> counter.setSelected(false));
		inPeriodoSelezionato.setSelected(true);
	}

	/**
	 * Restituisce i periodi di scadenza ordinati.
	 * 
	 * @return periodi di scadenza
	 */
	public Collection<CountPer<PeriodoScadenzaEnum>> getPeriodi() {
		return orderedList;
	}
}
