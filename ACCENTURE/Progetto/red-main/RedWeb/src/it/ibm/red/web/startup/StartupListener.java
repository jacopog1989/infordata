package it.ibm.red.web.startup;

import javax.servlet.ServletContextListener;

/**
 * Questa classe è lasciata intenzionalmente vuota per mantenere la compatibilità con la struttura dell'applicativo mobile.
 */
public class StartupListener extends it.ibm.red.business.startup.BusinessStartupListener implements ServletContextListener {

	// Questa classe è lasciata intenzionalmente vuota per mantenere la compatibilità con la struttura dell'applicativo mobile.
	
}