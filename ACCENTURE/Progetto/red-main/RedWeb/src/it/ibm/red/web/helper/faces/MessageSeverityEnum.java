package it.ibm.red.web.helper.faces;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;

/**
 * The Enum MessageSeverityEnum.
 *
 * @author CPIERASC
 * 
 *         Enum contenente le severity dei messaggi JSF.
 */
public enum MessageSeverityEnum {
	/**
	 * Messaggio informativo.
	 */
	INFO(FacesMessage.SEVERITY_INFO), 
	/**
	 * Messaggio d'avviso.
	 */
	WARN(FacesMessage.SEVERITY_WARN), 
	/**
	 * Messaggio d'errore.
	 */
	ERROR(FacesMessage.SEVERITY_ERROR);

	/**
	 * Severity messaggio.
	 */
	private Severity severity;
	
	/**
	 * Costruttore.
	 * 
	 * @param inSeverity	severity messaggio
	 */
	MessageSeverityEnum(final Severity inSeverity) {
		severity = inSeverity;
	}
	
	/**
	 * Getter severity messaggio.
	 * 
	 * @return	severity messaggio
	 */
	public Severity getSeverity() {
		return severity;
	}

}
