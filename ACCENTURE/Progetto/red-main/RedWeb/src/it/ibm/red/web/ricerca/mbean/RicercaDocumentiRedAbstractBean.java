package it.ibm.red.web.ricerca.mbean;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.enums.DescrizioneTipologiaDocumentoEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IRegistroRepertorioSRV;
import it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.enums.DettaglioDocumentoFileEnum;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.DettaglioDocumentoBean;
import it.ibm.red.web.mbean.DettaglioDocumentoFepaBean;
import it.ibm.red.web.mbean.interfaces.IFascicoloProcedimentaleManagerInitalizer;

/**
 * Abstract bean per la ricerca documenti red.
 */
public abstract class RicercaDocumentiRedAbstractBean extends RicercaAbstractBean<MasterDocumentRedDTO> {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = -7450184760837763248L;

	/**
	 * Servizio.
	 */
	protected IListaDocumentiFacadeSRV listaDocumentiSRV;

	/**
	 * Dettaglio documento da caricare.
	 */
	private DettaglioDocumentoFileEnum dettaglioDaCaricare;

	/**
	 * Lista descrizione registri.
	 */	 	
	private List<String> descrRegStampaEtichetteList;

	/**
	 * Componente per la riattivazione dei documenti.
	 */
	private RicercaRiattivaProcedimentoBean riattivaComponent;

	/**
	 * Non funziona come bean perché viene ricreato più volte. Devo gestire in
	 * maniera custom la vita di questo bean che deve essere legata alla vita del
	 * bean di ricerca.
	 */
	private RicercaAnnullaProcedimentoBean annullaComponent;

	/**
	 * Post construct del bean.
	 */
	@Override
	protected void postConstruct() {
		super.postConstruct();

		if (isRiattivaProcedimento()) {
			riattivaComponent = new RicercaRiattivaProcedimentoBean(this);
		}

		if (isAnnullaDocumentoVisibile()) {
			annullaComponent = new RicercaAnnullaProcedimentoBean();
			annullaComponent.setRicerca(this);
		}

		listaDocumentiSRV = ApplicationContextProvider.getApplicationContext().getBean(IListaDocumentiFacadeSRV.class);
		dettaglioDaCaricare = DettaglioDocumentoFileEnum.GENERICO;

		final IRegistroRepertorioSRV registroRepertorioSRV = ApplicationContextProvider.getApplicationContext().getBean(IRegistroRepertorioSRV.class);
		if (utente.getStampaEtichetteResponse()) {
			descrRegStampaEtichetteList = new ArrayList<>();
			descrRegStampaEtichetteList = registroRepertorioSRV.getDescrForShowEtichetteByAoo(utente.getIdAoo());
		}
	}

	@Override
	public abstract MasterDocumentRedDTO getSelectedDocument();

	/**
	 * Gestisce l'evento di selezione del master identificato da <code> m </code>.
	 * @param m master selezionato
	 */
	@Override
	public void selectDetail(final MasterDocumentRedDTO m) {
		boolean showEastButtons = false;
		final String tipologiaDocumento = m.getTipologiaDocumento() == null ? "" : m.getTipologiaDocumento();

		if (DescrizioneTipologiaDocumentoEnum.DICHIARAZIONE_SERVIZI_RESI.getDescrizione().equalsIgnoreCase(tipologiaDocumento)) {
			dettaglioDaCaricare = DettaglioDocumentoFileEnum.FEPADSR;
			// Attenzione questo controllo serve a evitare che sia possibile modificare la
			// DSR, la DSR può essere modificata solo quando questo controllo non è
			// verificato
			showEastButtons = !listaDocumentiSRV.isDocumentoInCoda(utente, m.getDocumentTitle(), DocumentQueueEnum.DSR);
		} else if (DescrizioneTipologiaDocumentoEnum.FATTURA_FEPA.getDescrizione().equalsIgnoreCase(tipologiaDocumento)) {
			dettaglioDaCaricare = DettaglioDocumentoFileEnum.FEPAFATTURA;
		} else if (DescrizioneTipologiaDocumentoEnum.DECRETO_DIRIGENZIALE_FEPA.getDescrizione().equalsIgnoreCase(tipologiaDocumento)) {
			dettaglioDaCaricare = DettaglioDocumentoFileEnum.FEPADD;
		} else {
			dettaglioDaCaricare = DettaglioDocumentoFileEnum.GENERICO;
		}

		if (DettaglioDocumentoFileEnum.FEPAFATTURA.equals(dettaglioDaCaricare) || DettaglioDocumentoFileEnum.FEPADD.equals(dettaglioDaCaricare)
				|| DettaglioDocumentoFileEnum.FEPADSR.equals(dettaglioDaCaricare)) {
			final DettaglioDocumentoFepaBean fepaBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_FEPA_BEAN);
			fepaBean.setDetail(m.getDocumentTitle(), m.getWobNumber(), m.getClasseDocumentale(), dettaglioDaCaricare);
			fepaBean.setNomeFile(FILE_PRINCIPALE + m.getNomeFile());

			fepaBean.setShowEastButtons(showEastButtons);
		} else {
			dettaglioDaCaricare = DettaglioDocumentoFileEnum.GENERICO;
			final DettaglioDocumentoBean ddBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN);

			if (!CollectionUtils.isEmpty(descrRegStampaEtichetteList)) {
				for (final String descrRegistro : descrRegStampaEtichetteList) {
					if (m.getNumeroProtocollo() != null
							&& ((m.getDescrizioneRegistroRepertorio() != null && m.getDescrizioneRegistroRepertorio().equalsIgnoreCase(descrRegistro))
									|| m.getDescrizioneRegistroRepertorio() == null || "REGISTRO UFFICIALE".equals(m.getDescrizioneRegistroRepertorio()))) {
						m.setStampaEtichette(true);
						break;
					}
				}
			} else if (utente.getStampaEtichetteResponse() && m.getNumeroProtocollo() != null
					&& (m.getDescrizioneRegistroRepertorio() == null || "REGISTRO UFFICIALE".equals(m.getDescrizioneRegistroRepertorio()))) {
				m.setStampaEtichette(true);
			}

			ddBean.setDetail(m);
		}
	}

	/**
	 * Gestisce l'apertura del fascicolo procedimentale.
	 */
	public void openFascicoloProcedimentale() {
		IFascicoloProcedimentaleManagerInitalizer initializer;
		if (DettaglioDocumentoFileEnum.FEPAFATTURA.equals(dettaglioDaCaricare) || DettaglioDocumentoFileEnum.FEPADD.equals(dettaglioDaCaricare)
				|| DettaglioDocumentoFileEnum.FEPADSR.equals(dettaglioDaCaricare)) {
			initializer = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_FEPA_BEAN);
		} else {
			dettaglioDaCaricare = DettaglioDocumentoFileEnum.GENERICO;
			initializer = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN);
		}

		initializer.openFascicoloProcedimentale();
	}

	/**
	 * Verifica che l'utente mpossa visualiuzzare e eseguire le operazioni legate
	 * alla riattivazione del procedimento
	 */
	public boolean isRiattivaProcedimento() {
		return PermessiUtils.isRiattivaProcedimento(utente.getPermessi());
	}

	/**
	 * Verifica che l'utente possa annullare il procedimento
	 * 
	 * @return
	 */
	public boolean isAnnullaDocumentoVisibile() {
		return PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.ANNULLAMENTO_PROTOCOLLO);
	}

	/**
	 * Restituisce il bean per la riattivazione del procedimento.
	 * 
	 * @return il bean
	 */
	public RicercaRiattivaProcedimentoBean getRiattivaComponent() {
		return riattivaComponent;
	}

	/**
	 * Restituisce il bean per l'annullamento del procedimento.
	 * 
	 * @return il bean
	 */
	public RicercaAnnullaProcedimentoBean getAnnullaComponent() {
		return annullaComponent;
	}

	/**
	 * Restituisce il dettaglio da caricare.
	 */
	@Override
	public DettaglioDocumentoFileEnum getDettaglioDaCaricare() {
		return dettaglioDaCaricare;
	}

	/**
	 * @param dettaglioDaCaricare the dettaglioDaCaricare to set
	 */
	public void setDettaglioDaCaricare(final DettaglioDocumentoFileEnum dettaglioDaCaricare) {
		this.dettaglioDaCaricare = dettaglioDaCaricare;
	}

}
