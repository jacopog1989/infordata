package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IInoltraFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * Bean che gestisce la response di inoltro.
 */
@Named(ConstantsWeb.MBean.INOLTRA)
@ViewScoped
public class InoltraBean extends AbstractBean implements InitHumanTaskInterface {
	
	/**
	 * Seriale autogenerato.
	 */
	private static final long serialVersionUID = -3260466892066481530L;
	
	/**
	 * Messaggio di errore generico riscontrato durante il tentativo del sollecito della response: INOLTRA.
	 */
	private static final String GENERIC_ERROR_SOLLECITO_RESPONSE_MSG = "Si è verificato un errore durante il tentato sollecito della response 'INOLTRA'";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(InoltraBean.class.getName());
	
    /**
     * Bean di sessione.
     */
	private SessionBean sb;
	
    /**
     * Lista documenti bean.
     */
	private ListaDocumentiBean ldb;
	
	/**
	 * Utente che ha iniziato l'operazione.
	 */
	private UtenteDTO utente;
	
	/**
	 * Input proveniente dalla maschera.
	 */
	private String motivoAssegnazioneNew;

    /**
     * Lista master.
     */
	private List<MasterDocumentRedDTO> masters;

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		masters = inDocsSelezionati;
	}


	@Override
	protected void postConstruct() {
		// Non deve fare nulla
	}
	
	/**
	 * Sollecita la response 'INOLTRA'.
	 */
	public void inoltraResponse() {
		final Collection<String> wobNumbers = new ArrayList<>();
		
		final IInoltraFacadeSRV inoltraSRV = ApplicationContextProvider.getApplicationContext().getBean(IInoltraFacadeSRV.class);
		sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
		utente = sb.getUtente();
		try {
			//pulisco gli esiti
			ldb.cleanEsiti();
			
			
			//raccolgo i wobnumber necessari ad invocare il service 
			for (final MasterDocumentRedDTO m : masters) {
				if (!StringUtils.isNullOrEmpty(m.getWobNumber())) {
					wobNumbers.add(m.getWobNumber());
				}
			}
			
			if (wobNumbers.isEmpty()) {
				throw new RedException(GENERIC_ERROR_SOLLECITO_RESPONSE_MSG);
			}
			
			// Eseguire la response
			EsitoOperazioneDTO esito;
			for (final String wobNumber : wobNumbers) {		
				if (ResponsesRedEnum.INOLTRA.equals(ldb.getSelectedResponse())) {
					esito = inoltraSRV.inoltra(utente, wobNumber, motivoAssegnazioneNew);
				} else {
					throw new RedException("Nessuna response supportata per questo controller");
				}
				ldb.getEsitiOperazione().add(esito);
			}			
			
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.INOLTRA);
		} catch (final Exception e) {
			LOGGER.error(GENERIC_ERROR_SOLLECITO_RESPONSE_MSG, e);
			showError(GENERIC_ERROR_SOLLECITO_RESPONSE_MSG);
		}
	}
	
	/**
	 * Recupera session bean.
	 * @return session bean
	 */
	public SessionBean getSb() {
		return sb;
	}

	/**
	 * Imposta session bean.
	 * @param sb session bean
	 */
	public void setSb(final SessionBean sb) {
		this.sb = sb;
	}

	/**
	 * Recupera la lista documenti bean.
	 * @return lista documenti bean
	 */
	public ListaDocumentiBean getLdb() {
		return ldb;
	}

	/**
	 * Imposta la lista documenti bean.
	 * @param ldb lista documenti bean
	 */
	public void setLdb(final ListaDocumentiBean ldb) {
		this.ldb = ldb;
	}

	/**
	 * Recupera l'utente.
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'utente.
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Recupera il motivo dell'assegnazione.
	 * @return motivo dell'assegnazione
	 */
	public String getMotivoAssegnazioneNew() {
		return motivoAssegnazioneNew;
	}

	/**
	 * Imposta il motivo dell'assegnazione.
	 * @param motivoAssegnazioneNew motivo dell'assegnazione
	 */
	public void setMotivoAssegnazioneNew(final String motivoAssegnazioneNew) {
		this.motivoAssegnazioneNew = motivoAssegnazioneNew;
	}

}
