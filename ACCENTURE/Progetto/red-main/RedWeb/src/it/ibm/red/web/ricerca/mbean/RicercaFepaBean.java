package it.ibm.red.web.ricerca.mbean;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IREDServiceFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaFepaFacadeSRV;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;

/**
 * Bean che gestisca la ricerca FEPA.
 */
@Named(ConstantsWeb.MBean.RICERCA_FEPA_BEAN)
@ViewScoped
public class RicercaFepaBean extends AbstractBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 6966869141664621770L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaFepaBean.class);
	
	/**
	 * Utente.
	 */	
	private UtenteDTO utente;
	
	/**
	 * Lista documenti.
	 */
	private SimpleDetailDataTableHelper<MasterDocumentRedDTO> documentiDTH;
	
	/**
	 * Flag dettaglio documento.
	 */
	private boolean dettaglioDocumentoVisible;
	
	/* Parametri di ricerca */
	/**
	 * Anno creazione.
	 */
	private Integer annoCreazione;
	
	/**
	 * Tipologia docuento.
	 */
	private String tipologiaDocumentoSelected;
	
	/**
	 * Data creazione da.
	 */
	private Date dataCreazioneDa;

	/**
	 * Data creazione a.
	 */
	private Date dataCreazioneA;
	
	/**
	 * Numero id.
	 */
	private Integer numeroId;

	/**
	 * Anno.
	 */
	private Integer annoInCorso;
	
	/**
	 * Flag permesso annullamento procedimento.
	 */
	private Boolean permessoAnnullamentoProcedimento;
	
	/**
	 * Flag permesso annullamento protocollo.
	 */
	private Boolean permessoAnnullaProtocollo;
	
	/**
	 * Motivo annullamento.
	 */
	private String motivazioneAnnullamento;
	
	/**
	 * Flag ricerca fepa.
	 */
	private boolean flagRicercaFepa;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		
		//Permessi
		final Long permessiUtente = utente.getPermessi();
		permessoAnnullamentoProcedimento = PermessiUtils.hasPermesso(permessiUtente, PermessiEnum.ANNULLAMENTO_PROTOCOLLO);
		permessoAnnullaProtocollo = PermessiUtils.hasPermesso(permessiUtente, PermessiEnum.ANNULLA_PROTOCOLLO_NPS);
		
		//initializing values
		documentiDTH = new SimpleDetailDataTableHelper<>();
		tipologiaDocumentoSelected = "1"; // 1 DSR - 2 Decreto Dirigenziale
		annoInCorso = Calendar.getInstance().get(Calendar.YEAR);
		annoCreazione = annoInCorso;
		dataCreazioneDa = null;
		dataCreazioneA = null;
		numeroId = null;
		motivazioneAnnullamento = "";
		
		flagRicercaFepa = false;
	}

	/**
	 * Gestisce l'evento di selezione della riga richiamando il caricamento del dettaglio del documento selezionato
	 * @param se
	 */
	public void docRowSelector(final SelectEvent se) {
		documentiDTH.rowSelector(se);
	}
	
	/**
	 * Imposta i parametri di ricerca recuperati dal form di ricerca Fepa, esegue la ricerca e valorizza
	 * il datatable dei risultati permettendo la renderizzazione dello stesso.
	 * Esegue una validazione a monte mostrando eventualmente l'errore riscontrato.
	 */
	public void ricercaFepa() {
		//controllo che i criteri di ricerca siano impostati
		if (!checkDates() && numeroId == null) {
			showWarnMessage("Restingere la ricerca immettendo almeno un criterio.");
			return;
		}
		
		try {
			final ParamsRicercaAvanzataDocDTO paramsRicercaAvanzata = new ParamsRicercaAvanzataDocDTO();
			final IRicercaFepaFacadeSRV ricercaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaFepaFacadeSRV.class);
			final String tipologia = ricercaSRV.recuperaTipologiaDocValues(tipologiaDocumentoSelected);
			
			paramsRicercaAvanzata.setAnnoDocumento(annoCreazione);
			paramsRicercaAvanzata.setDataCreazioneDa(dataCreazioneDa);
			paramsRicercaAvanzata.setDataCreazioneA(dataCreazioneA);
			paramsRicercaAvanzata.setDescrizioneTipologiaDocumento(tipologia);
			paramsRicercaAvanzata.setNumeroDocumento(numeroId);
			
			documentiDTH.setMasters(ricercaSRV.eseguiRicercaFepa(paramsRicercaAvanzata, utente, false));
			flagRicercaFepa = true;
		} catch (final Exception e) {
			LOGGER.error("Errore durante la ricerca fepa", e);
			showError("Errore durante la ricerca fepa: " + e);
		}
		
	}
	
	private boolean checkDates() {
		boolean res = false;
		fillVoidDate();
		if (dataCreazioneDa != null && dataCreazioneA != null) {
			if (dataCreazioneDa.before(dataCreazioneA) || dataCreazioneDa.equals(dataCreazioneA)) {
				res = true;
			} else {
				showWarnMessage("La data A non può precedere la data DA!");
			}
		}
		return res;
	}
	
	private void fillVoidDate() {
		//controllo che almeno una delle due date sia valorizzata e setto l'altra 
		//se A non è valorizzata...
		if (dataCreazioneDa != null && dataCreazioneA == null) {
			dataCreazioneA = new Date(); //setto data ad oggi
		}
		//se DA non è valorizzata...
		if (dataCreazioneDa == null && dataCreazioneA != null) {
			//setto data dell'anno di creazione selezionato
			try {
				final SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
				final String dateInString = "01-01-" + annoCreazione + " 00:00:00";
				dataCreazioneDa = sdf.parse(dateInString);
			} catch (final Exception e) {
				LOGGER.error("Errore nel setting della data.", e);
			}
		}
	}
	
	/**
	 * Esegue la logica di annullamento del documento selezionato dal {@link #documentiDTH}.
	 */
	public void annullaDocumento() {
		String idDocumento = null; 
		final Long idUtente = utente.getId();
		try {
			final IREDServiceFacadeSRV redSRV = ApplicationContextProvider.getApplicationContext().getBean(IREDServiceFacadeSRV.class);
			if (documentiDTH != null) {
				for (final MasterDocumentRedDTO docDTH : documentiDTH.getSelectedMasters()) {
					idDocumento = docDTH.getDocumentTitle();
					redSRV.annullaDocumento(idUtente.toString(), idDocumento, motivazioneAnnullamento);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Errore durante l'annullamento del Documento: " + e.getMessage());
		}
	}
	
	/**
	 * Esegue l'annullamento del documento NPS.
	 */
	public void annullaDocumentoNPS() {
		// Non implementato.
	}
	
	/**
	 * Effettua un reset dei campi della ricerca.
	 */
	public void reset() {
		documentiDTH = new SimpleDetailDataTableHelper<>();
		annoCreazione = annoInCorso;
		tipologiaDocumentoSelected = "";
		dataCreazioneDa = null;
		dataCreazioneA = null;
		numeroId = null;
		motivazioneAnnullamento = "";
		flagRicercaFepa = false;
	}
	/* Getter e Setter */
	/**
	 * Restituisce il datatable helper che gestisce il datatable dei documenti.
	 * @return datatable helper documenti
	 */
	public SimpleDetailDataTableHelper<MasterDocumentRedDTO> getDocumentiDTH() {
		return documentiDTH;
	}

	/**
	 * Imposta il datatable helper che gestisce il datatable dei documenti.
	 * @param documentiDTH
	 */
	public void setDocumentiDTH(final SimpleDetailDataTableHelper<MasterDocumentRedDTO> documentiDTH) {
		this.documentiDTH = documentiDTH;
	}

	/**
	 * Restituisce true se il dettaglio del documento è visibile, false altrimenti.
	 * @return true se il dettaglio del documento è visibile, false altrimenti
	 */
	public boolean isDettaglioDocumentoVisible() {
		return dettaglioDocumentoVisible;
	}

	/**
	 * Imposta la visibilita del dettaglio del documento.
	 * @param dettaglioDocumentoVisible
	 */
	public void setDettaglioDocumentoVisible(final boolean dettaglioDocumentoVisible) {
		this.dettaglioDocumentoVisible = dettaglioDocumentoVisible;
	}

	/**
	 * Restituisce l'anno di creazione.
	 * @return anno creazione
	 */
	public Integer getAnnoCreazione() {
		return annoCreazione;
	}

	/**
	 * Restituisce la data iniziale de range di creazione.
	 * @return data iniziale creazione
	 */
	public Date getDataCreazioneDa() {
		return dataCreazioneDa;
	}

	/**
	 * Restituisce la data finale de range di creazione.
	 * @return data finale creazione
	 */
	public Date getDataCreazioneA() {
		return dataCreazioneA;
	}

	/**
	 * Restituisce il numero id.
	 * @return numero id
	 */
	public Integer getNumeroId() {
		return numeroId;
	}

	/**
	 * Imposta l'anno di creazione.
	 * @param annoCreazione
	 */
	public void setAnnoCreazione(final Integer annoCreazione) {
		this.annoCreazione = annoCreazione;
	}

	/**
	 * Imposta la data iniziale de range di creazione.
	 * @param dataCreazioneDa
	 */
	public void setDataCreazioneDa(final Date dataCreazioneDa) {
		this.dataCreazioneDa = dataCreazioneDa;
	}

	/**
	 * Imposta la data finale de range di creazione.
	 * @param dataCreazioneA
	 */
	public void setDataCreazioneA(final Date dataCreazioneA) {
		this.dataCreazioneA = dataCreazioneA;
	}

	/**
	 * Imposta il numero id.
	 * @param numeroId
	 */
	public void setNumeroId(final Integer numeroId) {
		this.numeroId = numeroId;
	}

	/**
	 * Restituisce la tipologia del documento selezionato.
	 * @return tipologia documento selezionata
	 */
	public String getTipologiaDocumentoSelected() {
		return tipologiaDocumentoSelected;
	}

	/**
	 * Imposta la tipologia documento selezionata.
	 * @param tipologiaDocumentoSelected
	 */
	public void setTipologiaDocumentoSelected(final String tipologiaDocumentoSelected) {
		this.tipologiaDocumentoSelected = tipologiaDocumentoSelected;
	}

	/**
	 * Restituisce true se {@link #utente} detiene il permesso di annullamento procedimento.
	 * @return true se ha il permesso di annullamento procedimento, false altrimenti
	 */
	public Boolean getPermessoAnnullamentoProcedimento() {
		return permessoAnnullamentoProcedimento;
	}

	/**
	 * Imposta il permesso di annullamento procedimento dell'utente: {@link #utente}.
	 * @param permessoAnnullamentoProcedimento
	 */
	public void setPermessoAnnullamentoProcedimento(final Boolean permessoAnnullamentoProcedimento) {
		this.permessoAnnullamentoProcedimento = permessoAnnullamentoProcedimento;
	}

	/**
	 * Restituisce true se {@link #utente} detiene il permesso di annullamento protocollo.
	 * @return true se ha il permesso di annullamento protocollo, false altrimenti
	 */
	public Boolean getPermessoAnnullaProtocollo() {
		return permessoAnnullaProtocollo;
	}

	/**
	 * Imposta il permesso di annullamento protocollo dell'utente: {@link #utente}.
	 * @param permessoAnnullaProtocollo
	 */
	public void setPermessoAnnullaProtocollo(final Boolean permessoAnnullaProtocollo) {
		this.permessoAnnullaProtocollo = permessoAnnullaProtocollo;
	}

	/**
	 * Restituisce la motivazione dell'annullamento.
	 * @return motivazione annullamento
	 */
	public String getMotivazioneAnnullamento() {
		return motivazioneAnnullamento;
	}

	/**
	 * Imposta la motivazione dell'annullamento.
	 * @param motivazioneAnnullamento
	 */
	public void setMotivazioneAnnullamento(final String motivazioneAnnullamento) {
		this.motivazioneAnnullamento = motivazioneAnnullamento;
	}

	/**
	 * Restituisce true se in ricerca Fepa, false altrimenti.
	 * @return true se in ricerca Fepa, false altrimenti
	 */
	public boolean isFlagRicercaFepa() {
		return flagRicercaFepa;
	}

	/**
	 * Imposta il flag associato alla ricerca Fepa.
	 * @param flagRicercaFepa
	 */
	public void setFlagRicercaFepa(final boolean flagRicercaFepa) {
		this.flagRicercaFepa = flagRicercaFepa;
	}
	
	/**
	 * Restituisce il nome del file per l'export della coda.
	 * @return nome file per export della ricerca
	 */
	public String getNomeFileExportCoda() {
		
		String nome = "";
		if (utente != null) {
			nome += utente.getNome().toLowerCase() + "_" + utente.getCognome().toLowerCase() + "_";
		}
		
		nome += "ricercafepa_";
		
		final SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH.mm");
		nome += sdf.format(new Date()); 
		
		return nome;
	}

}
