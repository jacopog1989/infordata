package it.ibm.red.web.servlets;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.ibm.red.business.utils.StringUtils;

/**
 * Servlet implementation class PrefServlet.
 */
public class DummyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DummyServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response) {
		final String dump = request.getParameter("dump");
		Boolean dumpFlag = false;
		if (!StringUtils.isNullOrEmpty(dump) && "on".equalsIgnoreCase(dump)) {
			dumpFlag = true;
		}
    	new DummyClass().ricaricaMenuContatori(dumpFlag);
	}

}
