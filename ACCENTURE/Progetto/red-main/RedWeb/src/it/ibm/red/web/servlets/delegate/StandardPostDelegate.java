package it.ibm.red.web.servlets.delegate;

import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.enums.TrasformazionePDFInErroreEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IASignFacadeSRV;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.web.helper.lsign.LocalSignHelper.DocSignDTO;
import it.ibm.red.web.helper.lsign.LocalSignHelper.TipoDoc;
import it.ibm.red.web.servlets.delegate.exception.DelegateException;

/**
 * Delegate post standard.
 */
public class StandardPostDelegate extends AbstractPostDelegate {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(StandardPostDelegate.class.getName());
	
	@Override
	public final EsitoOperazioneDTO manageSignedDoc(final String signTransactionId, final DocSignDTO doc, final byte[] docSigned, final UtenteDTO utente, final SignTypeEnum ste, final boolean isLastDocument, 
			final boolean isFirmaMultipla) throws DelegateException {
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(doc.getWobNumber(), doc.getDocumentTitle());
		esito.setIdDocumento(doc.getNumeroDocumentoPrincipale());
		esito.setNomeDocumento(doc.getNomeFile());
		Integer metadatoPdfErrorValue = Constants.Varie.TRASFORMAZIONE_PDF_OK;
		String logId = signTransactionId + doc.getDocumentTitle();
		
		try {
			LOGGER.info(logId + " => " + doc.getTipoDoc() + " " + (!isLastDocument ? "non " : "") + "ultimo documento di " + doc.getIdDocPrincipale());
			LOGGER.info(logId + " => content stampigliato size: " + (doc.getContentStampigliato() != null ? doc.getContentStampigliato().length : 0));
			LOGGER.info(logId + " => docSigned size: " + (docSigned != null ? docSigned.length : 0));
			
			ISignFacadeSRV signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignFacadeSRV.class);
			IASignFacadeSRV aSignSRV = ApplicationContextProvider.getApplicationContext().getBean(IASignFacadeSRV.class);
			Document document=null;
			if(TipoDoc.PRINCIPALE.equals(doc.getTipoDoc())) {
				document = aSignSRV.findDocAggiornato(doc.getDocumentTitle(), utente, true);
			}else if(TipoDoc.ALLEGATO.equals(doc.getTipoDoc())) {
				document = aSignSRV.findDocAggiornato(doc.getDocumentTitle(), utente, false);
			}
			
			LOGGER.info(logId + " => recuperato content su Filenet");
			
			if(document!=null) {
				doc.setGuid(document.get_Id().toString());
			}
			
			// Gestione del content firmato (e.g. se firma PAdES, merge del content stampigliato con quello firmato e con il glifo di firma)
			byte[] contentToPersist = signSRV.manageSignedContent(utente, ste, doc.getContentStampigliato(), docSigned, doc.getDataFirma(), doc.getDocumentTitle(), 
					TipoDoc.PRINCIPALE.equals(doc.getTipoDoc()), false);
			LOGGER.info(logId + " => eseguita la gestione del content firmato");
			
			// Inserimento del documento firmato nella tabella di bufferizzazione
			aSignSRV.insertIntoSignedDocsBuffer(doc.getWobNumber(), doc.getGuid(), contentToPersist, TipoDoc.PRINCIPALE.equals(doc.getTipoDoc()));
			LOGGER.info(logId + " => eseguito l'inserimento nella tabella di bufferizzazione");
		
 
			esito.setEsito(true);
			if(TipoDoc.PRINCIPALE.equals(doc.getTipoDoc())) {
				esito.setNumeroProtocollo((Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY));
				esito.setAnnoProtocollo((Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY));
			}
			esito.setCodiceErrore(null);
			esito.setNote(null);
			
			// Se è l'ultimo documento associato a documentTitlePrincipale, finalizza il processo di firma locale
			if (isLastDocument) {
				aSignSRV.postFirmaLocale(logId, esito, utente, ste, false);
				LOGGER.info(logId + " => eseguita la finalizzazione della firma locale con esito: " + esito.isEsito());
				
				if (!esito.isEsito()) {
					throw new RedException("Errore durante la finalizzazione della firma locale");
				}
			} else {
				esito.setEsito(true);
			}
		} catch (Exception e) {
			LOGGER.error(logId + " => errore in fase di gestione del documento firmato: " + e.getMessage(), e);
			metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.KO_GENERICO.getValue();
			throw new DelegateException("Errore in fase di gestione del documento firmato", e);
		} finally {
			aggiornaStatoDocumento(logId, utente.getId(), doc.getIdDocPrincipale(), metadatoPdfErrorValue);
		}
		return esito;
	}
}