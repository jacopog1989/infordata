package it.ibm.red.web.mbean;

import java.util.ArrayList;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.ScheduleEvent;

import it.ibm.red.business.dto.EventoDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.RicercaEventoDTO;
import it.ibm.red.business.enums.EventoCalendarioEnum;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.dto.ScheduleEventoDTO;
import it.ibm.red.web.enums.NavigationTokenEnum;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Bean che gestisce il calendario.
 */
@Named(ConstantsWeb.MBean.CALENDARIO_BEAN)
@ViewScoped
public class CalendarioBean extends AbstractCalendario {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -6079781053466032519L;

	/**
	 * Container name di centralSectionForm:readUpdateDialog.
	 */
	private static final String READUPDATEDIALOG_CONTAINER_NAME = "centralSectionForm:readUpdateDialog";
	
	/**
	 * Javascript che consente di visualizzare il componente Primefaces identificato da: comNewDialogWV.
	 */
	private static final String SHOW_COMNEWDIALOGWV_JS = "PF('comNewDialogWV').show();";
 
	/**
	 * Tipo evento selezionato.
	 */
	private EventoCalendarioEnum tipoEventoSelected;

	/**
	 * Flag visualizza scadenzario.
	 */
	private boolean showScadenzario;
	
	/**
	 * Widget evento.
	 */
	private EventoDTO eventoWidget;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		super.postConstruct();
		showScadenzario = false; 
		if (getSessionBean().getActivePageInfo().equals(NavigationTokenEnum.DASHBOARD)) { 
			caricaEventiOggi();
		} 
		
	}
     
  
	/**
	 * Carica gli eventi associati ad "oggi".
	 */
	public void caricaEventiOggi() {
		setRicercaDTO(new RicercaEventoDTO());
		final Date oggi = new Date();

		getRicercaDTO().setDataInizio(DateUtils.dropTimeInfo(oggi));
		getRicercaDTO().setDataScadenza(DateUtils.setDateTo2359(oggi));
		ricercaEventi();
		setRicercaDTO(new RicercaEventoDTO());
	}
 
	
	private void viewScadenzario(final boolean showScadenzario) {
		setShowScadenzario(showScadenzario);
	}

	/**
	 * Permette la visualizzazione dello scadenzario.
	 */
	public void loadScadenzario() {
		viewScadenzario(true);
	}

	/**
	 * Inibisce la visibilità dello scadenzario.
	 */
	public void destroyScadenzario() {
		viewScadenzario(false);
	}
	
	 

	/**
	 * @return the eventoSelected
	 */
	public final EventoCalendarioEnum getTipoEventoSelected() {
		return tipoEventoSelected;
	}

	/**
	 * @param eventoSelected the eventoSelected to set
	 */
	public final void setTipoEventoSelected(final EventoCalendarioEnum tipoEventoSelected) {
		this.tipoEventoSelected = tipoEventoSelected;
	}

	 
	/**
	 * Restituisce il flag legato alla visibilità dello scadenzario.
	 * @return showScadenzario
	 */
	public boolean getShowScadenzario() {
		return showScadenzario;
	}

	/**
	 * Imposta il flag legato alla visibilità dello scadenzario.
	 * @param showScadenzario
	 */
	public void setShowScadenzario(final boolean showScadenzario) {
		this.showScadenzario = showScadenzario;
	} 

	/**
	 * Gestisce la selezione della data associata al widget.
	 * @param selectEvent
	 */
	public void onDateSelectWidget(final SelectEvent selectEvent) {
		setEventoCreazione(new EventoDTO());
		 
		final ScheduleEvent event = new DefaultScheduleEvent("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject());
		getEventoCreazione().setDataInizio(event.getStartDate());
		getEventoCreazione().setDataScadenza(event.getEndDate());
		FacesHelper.executeJS("PF('creationDialogWidget').show()");
	} 

	
	/**
	 * Esegue la modifica dell'evento selezionato associato al widget.
	 */
	public void modificaEventoSelezionatoWidget() {
		setModDlg(false);
		setAllegatoUploaded(null);
		setIdNodiSelezionati(new ArrayList<>());
		setDescNodiSelezionati(new ArrayList<>());
		
		if (EventoCalendarioEnum.SCADENZA.equals(eventoWidget.getTipologia())) {
			setRicercaScadenza();
		} else {
			setEvento();
			if (eventoWidget.getIdUtente().equals(getUtente().getId())) {
				setModDlg(true);
			}
			
			getDescrAssegnatarioPerCompetenza().clear();
			actionPostVisModEventoSelWidget();
 		}
	}


	private void setRicercaScadenza() {
		final Date targetDate = eventoWidget.getDataScadenza();
		setRicercaDTO(new RicercaEventoDTO());
		getRicercaDTO().setDataInizio(targetDate);
		getRicercaDTO().setDataScadenza(targetDate);
		getRicercaDTO().setTipologia(EventoCalendarioEnum.SCADENZA);
		ricercaEventi();
		setRicercaDTO(new RicercaEventoDTO());
	}

	/**
	 * Definisce la logica da eseguire in fase di selezione evento di widget.
	 * @param selectEvent
	 */
	public void onEventSelectWidget(final SelectEvent selectEvent) {
		final ScheduleEventoDTO event = (ScheduleEventoDTO) selectEvent.getObject();
		 
		setRicercaDTO(new RicercaEventoDTO());
		getRicercaDTO().setDataInizio(event.getDataInizio());
		getRicercaDTO().setDataScadenza(event.getDataScadenza());
		ricercaEventi();
		setRicercaDTO(new RicercaEventoDTO());
	}

	/**
	 * Restituisce l'evento del widget.
	 * @return eventoWidget
	 */
	public EventoDTO getEventoWidget() {
		return eventoWidget;
	}

	/**
	 * Imposta l'evento del widget.
	 * @param eventoWidget
	 */
	public void setEventoWidget(final EventoDTO eventoWidget) {
		this.eventoWidget = eventoWidget;
	}

	/**
	 * Visualizza dettagli sull'evento selezionato.
	 */
	public void visualizzaEventoSelezionatoWidget() {
		setModDlg(false);
		setAllegatoUploaded(null);
		setIdNodiSelezionati(new ArrayList<>());
		setDescNodiSelezionati(new ArrayList<>());
		
		if (EventoCalendarioEnum.SCADENZA.equals(eventoWidget.getTipologia())) {
			setRicercaScadenza();
		} else {
			setEvento(); 
			
			getDescrAssegnatarioPerCompetenza().clear();
			actionPostVisModEventoSelWidget();
 		}
	}


	private void actionPostVisModEventoSelWidget() {
		if (eventoWidget.getDescrizioneNodi() != null) {
			for (int i = 0; i < eventoWidget.getDescrizioneNodi().size(); i++) {
				getDescrAssegnatarioPerCompetenza().add(new NodoOrganigrammaDTO(eventoWidget.getDescrizioneNodi().get(i), eventoWidget.getIdNodiTarget().get(i), null));
			}			
		} else {
			getEventSelected().setDescrizioneNodi(new ArrayList<>());
		}
  
		FacesHelper.update(READUPDATEDIALOG_CONTAINER_NAME);
		FacesHelper.executeJS(SHOW_COMNEWDIALOGWV_JS);
	}


	private void setEvento() {
		setEventSelected(new ScheduleEventoDTO(eventoWidget)); 
		if (!StringUtils.isNullOrEmpty(getEventSelected().getNomeAllegato())) {
			setAllegatoUploaded(new FileDTO(getEventSelected().getNomeAllegato(), 
					null, 
					getEventSelected().getMimeTypeAllegato()));
		}
	} 
	
	 

}