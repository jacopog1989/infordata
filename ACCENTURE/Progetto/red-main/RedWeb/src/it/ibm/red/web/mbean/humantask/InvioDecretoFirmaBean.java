package it.ibm.red.web.mbean.humantask;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.facade.IFepaFacadeSRV;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * Bean che gestisce l'invio di un decreto in firma.
 */
@Named(ConstantsWeb.MBean.INVIO_DECRETO_FIRMA_BEAN)
@ViewScoped
public class InvioDecretoFirmaBean extends AbstractBean implements InitHumanTaskInterface {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -4246402497908196970L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(InvioDecretoFirmaBean.class.getName());

	/**
	 * Oggetto.
	 */
	private String oggetto;
	
	/**
	 * Servizio.
	 */
	private IFepaFacadeSRV fepaSRV;
	
	/**
	 * Bean di sessione.
	 */
	private SessionBean sb;
	
	/**
	 * Lista documenti bean.
	 */
	private ListaDocumentiBean ldb;
	
	/**
	 * Documenti selezionati.
	 */
	private List<MasterDocumentRedDTO> documentiSelezionati;
	
	/**
	 * Informazoini utente.
	 */
	private UtenteDTO utente;
	
	/**
	 * Informazioni utente firmatario.
	 */
	private UtenteDTO utenteFirmatario;
	
	/**
	 * Descrizione ufficio utente firmatario.
	 */
	private String descrizioneUfficioFirma;
	
	/**
	 * Servizio.
	 */
	private IUtenteFacadeSRV utenteSRV;

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		setDocumentiSelezionati(inDocsSelezionati);
	}

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);
		utenteSRV = ApplicationContextProvider.getApplicationContext().getBean(IUtenteFacadeSRV.class);
		sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sb.getUtente();
		
		utenteFirmatario = recuperaUtenteFirmatario();
		descrizioneUfficioFirma = recuperaUfficioFirmatario();
		
	}
	
	/**
	 * Solleciat la response 'INVIO DECRETO IN FIRMA'.
	 */
	public void invioDecretoFirmaResponse() {
		//E' una respons singola
		EsitoOperazioneDTO eOpe = null;
		String wobNumber = null;
		try {	
			ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
			
			//pulisco eventuali esiti
			ldb.cleanEsiti();
			
			//prendo il wobnumber del documento
			wobNumber = documentiSelezionati.get(0).getWobNumber();
			
			//prendo gli id
			final Long idUtenteFirma = utenteFirmatario.getId();
			final Long idUfficioFirma = utenteFirmatario.getIdUfficio();
			//chiamo il service 
			eOpe = fepaSRV.invioDecretoInFirma(utente, wobNumber, idUfficioFirma, idUtenteFirma, oggetto); //verificare idUfficioFirma e idutentefirma
			
			ldb.getEsitiOperazione().add(eOpe);
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.INVIO_DECRETO_FIRMA_BEAN);
			
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il tentato sollecito della response 'INVIO DECRETO IN FIRMA'", e);
			showError("Si è verificato un errore durante il tentato sollecito della response 'INVIO DECRETO IN FIRMA'");
		}
	}
	
	private UtenteDTO recuperaUtenteFirmatario() {
		try {
			final String usernameFirmatario = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FEPA_UTENTE_UFFICIO_TERZO);
			utenteFirmatario = utenteSRV.getByUsername(usernameFirmatario);///
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'utente firmatario ", e);
			showError("Errore durante il recupero dell'utente firmatario ");
		}
		return utenteFirmatario;
	}
	
	private String recuperaUfficioFirmatario() {
		String ufficioFirmatario = null;
		try {
			final PropertiesProvider pp = PropertiesProvider.getIstance();
			ufficioFirmatario = pp.getParameterByKey(PropertiesNameEnum.FEPA_NODO_UFFICIO_TERZO);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'ufficio del firmatario ", e);
			showError("Errore durante il recupero dell'ufficio del firmatario ");
		}
		return ufficioFirmatario;
	}
	
	/**
	 * Recupera i documenti selezionati.
	 * @return documenti selezionati
	 */
	public List<MasterDocumentRedDTO> getDocumentiSelezionati() {
		return documentiSelezionati;
	}

	/**
	 * Imposta i documenti selezionati.
	 * @param documentiSelezionati documenti selezionati
	 */
	public void setDocumentiSelezionati(final List<MasterDocumentRedDTO> documentiSelezionati) {
		this.documentiSelezionati = documentiSelezionati;
	}

	/**
	 * Recupera l'oggetto.
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Recupera session bean.
	 * @return session bean
	 */
	public SessionBean getSb() {
		return sb;
	}

	/**
	 * Recupera la lista documenti bean.
	 * @return lista documenti bean
	 */
	public ListaDocumentiBean getLdb() {
		return ldb;
	}

	/**
	 * Recupera l'utente.
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'oggetto
	 * @param oggetto
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * Imposta session bean.
	 * @param sb session bean
	 */
	public void setSb(final SessionBean sb) {
		this.sb = sb;
	}

	/**
	 * Imposta la lista documenti bean.
	 * @param ldb lista documenti bean
	 */
	public void setLdb(final ListaDocumentiBean ldb) {
		this.ldb = ldb;
	}

	/**
	 * Imposta l'utente.
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Recupera l'utente firmatario.
	 * @return utente firmatario
	 */
	public UtenteDTO getUtenteFirmatario() {
		return utenteFirmatario;
	}

	/**
	 * Imposta l'utente firmatario.
	 * @param utenteFirmatario utente firmatario
	 */
	public void setUtenteFirmatario(final UtenteDTO utenteFirmatario) {
		this.utenteFirmatario = utenteFirmatario;
	}

	/**
	 * Recupera l'attributo utenteSRV.
	 * @return utenteSRV
	 */
	public IUtenteFacadeSRV getUtenteSRV() {
		return utenteSRV;
	}

	/**
	 * Imposta l'attributo utenteSRV.
	 * @param utenteSRV
	 */
	public void setUtenteSRV(final IUtenteFacadeSRV utenteSRV) {
		this.utenteSRV = utenteSRV;
	}

	/**
	 * Recupera la descrizione dell'ufficio di firma.
	 * @return descrizione dell'ufficio di firma
	 */
	public String getDescrizioneUfficioFirma() {
		return descrizioneUfficioFirma;
	}

	/**
	 * Imposta la descrizione dell'ufficio di firma.
	 * @param descrizioneUfficioFirma descrizione dell'ufficio di firma
	 */
	public void setDescrizioneUfficioFirma(final String descrizioneUfficioFirma) {
		this.descrizioneUfficioFirma = descrizioneUfficioFirma;
	}

}
