/**
 * 
 */
package it.ibm.red.web.mbean.interfaces;

import java.util.List;

import it.ibm.red.business.dto.MasterDocumentRedDTO;

/**
 * Interfaccia utilizzata dai Bean HumanTask di Scope View.
 * 
 * @author APerquoti
 *
 */
public interface InitHumanTaskInterface {
	
	/**
	 * Inizializza i documenti selezionati per preparare l'esecuzione dello humanTask.
	 * 
	 * @param inDocsSelezionati
	 */
	void initBean(List<MasterDocumentRedDTO> inDocsSelezionati);

}
