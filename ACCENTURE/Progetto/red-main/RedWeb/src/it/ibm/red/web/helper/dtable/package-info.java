/**
 * @author CPIERASC
 * 
 *	In questo package avremo il middleware per la gestione dei datatable, componenti utilizzati nelle gui per rappresentare
 *	tabelle (facilitano le operazioni di selezione e l'implementazione del concetto di master/detail).
 *
 */
package it.ibm.red.web.helper.dtable;
