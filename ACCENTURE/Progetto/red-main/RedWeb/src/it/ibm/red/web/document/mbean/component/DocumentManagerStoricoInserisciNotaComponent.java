package it.ibm.red.web.document.mbean.component;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ColoreNotaEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.facade.INotaFacadeSRV;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.interfaces.IInserisciNota;

/**
 * Component Document Managaer - storico inserisci note.
 */
public class DocumentManagerStoricoInserisciNotaComponent extends AbstractComponent implements IInserisciNota {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -5220347833251162336L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DocumentManagerStoricoInserisciNotaComponent.class.getName());
	
	/**
	 * Service per la gestione delle note dei documenti
	 */
	private final INotaFacadeSRV notaSRV;
    
	/**
	 * INFORMAZIONI CHE NON VENGONO MODIFICATE ALLA SELEZIONE SULLA TABELLA DELLO STORICO
	 */
	private final UtenteDTO utente;
    
	/**
	 * Document title.
	 */
	private final Integer documentTitle;
	
	/**
	 * testo contenente la nota.
	 */
	private String testoNota;
	
	/**
	 * Colore della nota.
	 */
	private Integer coloreNota = 0;
	
	/**
	 * Costruttore del component.
	 * 
	 * @param notaSRV
	 *            Service per la gestione delle note da impostare.
	 * @param utente
	 *            Utente da impostare.
	 * @param inDocumentTitle
	 *            Identificativo del documento da impostare.
	 */
	protected DocumentManagerStoricoInserisciNotaComponent(final INotaFacadeSRV notaSRV, final UtenteDTO utente, final Integer inDocumentTitle) {
		this.notaSRV = notaSRV;
		this.utente = utente;
		this.documentTitle = inDocumentTitle;
		
		close();
	}

	/**
	 * @see it.ibm.red.web.mbean.interfaces.IInserisciNota#inserisciNotaResponse().
	 */
	@Override
	public void inserisciNotaResponse() {
		try {
			if (!validate()) {
				return;
			}
			
			ColoreNotaEnum colore = null;
			if (coloreNota != null) {
				colore = ColoreNotaEnum.get(coloreNota);
			}
			
			notaSRV.registraNotaFromDocumento(utente, documentTitle, colore, testoNota);
			showInfoMessage("Nota inserita correttamente.");
			FacesHelper.executeJS("PF('wdgInserisciNota_DMDTSN').hide();");
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'inserimento della nota.", e);
			showErrorMessage("Si è verificato un errore durante l'inserimento della nota.");
		}
	}

	/**
	 * Gestisce la validazione del colore della nota.
	 * 
	 * @return true se valido, false altrimenti
	 */
	public boolean validate() {
		boolean valid = true;
		
		if (coloreNota == null || coloreNota < 0) {
			showErrorMessage("Attenzione, è necessario selezionare un colore.");
			valid = false;
		}
		
		return valid;
	}

	/**
	 * @see it.ibm.red.web.mbean.interfaces.IInserisciNota#eliminaNota().
	 */
	@Override
	public void eliminaNota() {
		// Questa funzionalità non è contemplata.
	}

	/**
	 * @see it.ibm.red.web.mbean.interfaces.IInserisciNota#close().
	 */
	@Override
	public void close() {
		testoNota = null;
		coloreNota = 0;
	}

	/**
	 * @see it.ibm.red.web.mbean.interfaces.IInserisciNota#getTestoNota().
	 */
	@Override
	public String getTestoNota() {
		return testoNota;
	}

	/**
	 * @see it.ibm.red.web.mbean.interfaces.IInserisciNota#setTestoNota(java.lang.String).
	 */
	@Override
	public void setTestoNota(final String testoNota) {
		this.testoNota = testoNota;
	}

	/**
	 * @see it.ibm.red.web.mbean.interfaces.IInserisciNota#getColoreNota().
	 */
	@Override
	public Integer getColoreNota() {
		return coloreNota;
	}

	/**
	 * @see it.ibm.red.web.mbean.interfaces.IInserisciNota#setColoreNota(java.lang.Integer).
	 */
	@Override
	public void setColoreNota(final Integer coloreNota) {
		this.coloreNota = coloreNota;
	}

}
