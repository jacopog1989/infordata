package it.ibm.red.web.document.mbean.component;

import it.ibm.red.business.dto.DetailProtocolloPregressoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.mbean.AbstractBean;

/**
 * @author VINGENITO
 * 
 */
public class DettaglioProtocolloNsdComponent extends AbstractBean {

	private static final long serialVersionUID = 510047887019619181L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DettaglioProtocolloNsdComponent.class);

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Dettaglio.
	 */
	private DetailProtocolloPregressoDTO detail;

	/**
	 * Costruttore del component.
	 * 
	 * @param utenteIn
	 */
	public DettaglioProtocolloNsdComponent(final UtenteDTO utenteIn) {
		try {
			this.utente = utenteIn;
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}
	}
	
	@Override
	protected void postConstruct() {
		// Metodo intenzionalmente vuoto.
	}

	/**
	 * Restituisce l'utente.
	 * 
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'utente.
	 * 
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Restituisce il dettaglio.
	 * 
	 * @return detail
	 */
	public DetailProtocolloPregressoDTO getDetail() {
		return detail;
	}

	/**
	 * Imposta il dettaglio.
	 * 
	 * @param detail
	 */
	public void setDetail(final DetailProtocolloPregressoDTO detail) {
		this.detail = detail;
	}
}