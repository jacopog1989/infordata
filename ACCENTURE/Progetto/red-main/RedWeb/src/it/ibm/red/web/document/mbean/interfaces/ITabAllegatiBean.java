package it.ibm.red.web.document.mbean.interfaces;

import java.io.Serializable;
import java.util.List;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentManagerDTO;

/**
 * Interfaccia bean che gestisce il tab allegati.
 */
public interface ITabAllegatiBean extends Serializable {

	/**
	 * Rimuove l'allegato.
	 * @param index
	 */
	void rimuoviAllegato(Object index);

	/**
	 * Aggiunge l'allegato.
	 */
	void addAllegato();

	/**
	 * Gestisce l'upload del file allegato.
	 * @param event
	 */
	void handleFileUploadAllegato(FileUploadEvent event);

	/**
	 * Rimuove il file allegato.
	 * @param index
	 */
	void rimuoviFileAllegato(Object index);

	/**
	 * Gestisce l'evento di modifica associato al formato dell'allegato.
	 * @param index
	 */
	void onChangeComboFormatoAllegato(Object index);

	/**
	 * Esegue il download dell'allegato.
	 * @param index
	 * @return file
	 */
	StreamedContent downloadAllegato(Object index);

	/**
	 * Ottiene il dettaglio del documento.
	 * @return dettaglio del documento
	 */
	DetailDocumentRedDTO getDetail();

	/**
	 * Ottiene gli allegati.
	 * @return lista di allegati
	 */
	List<AllegatoDTO> getAllegati();

	/**
	 * Imposta gli allegati.
	 * @param allegati lista di allegati
	 */
	void setAllegati(List<AllegatoDTO> allegati);

	/**
	 * Ottiene l'attributo documentManagerDTO.
	 * @return documentManagerDTO
	 */
	DocumentManagerDTO getDocumentManagerDTO();
	
	/**
	 * Ottiene l'attributo flagNotDownload.
	 * @return flagNotDownload
	 */
	Boolean getFlagNotDownload();
	
	/**
	 * Gestisce l'upload degli allegati.
	 * @param event
	 */
	void gestisciUploadAllegati(FileUploadEvent event);
}