package it.ibm.red.web.document.mbean.component;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.FileUploadEvent;

import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IContributoFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.interfaces.IGestoreAggiungiModificaContributoEst;

/**
 * Component per la gestione della dialog di inserimento/modifica di un
 * contributo esterno.
 * 
 * @author m.crescentini
 *
 */
public class AggiungiModificaContributoEstComponent extends AbstractComponent {

	private static final long serialVersionUID = 8664865074443528468L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AggiungiModificaContributoEstComponent.class);

	/**
	 * Chiamante.
	 */
	private final IGestoreAggiungiModificaContributoEst chiamante;

	/**
	 * Document title.
	 */
	private final String documentTitle;

	/**
	 * Identificativo fascicolo.
	 */
	private final String idFascicolo;

	/**
	 * Utente.
	 */
	private final UtenteDTO utente;

	/**
	 * Contributo.
	 */
	private ContributoDTO contributo;

	/**
	 * Lista contributi.
	 */
	private transient List<FileDTO> fileContributoList;

	/**
	 * Flag nuovo contributo.
	 */
	private boolean nuovoContributo;

	/**
	 * Costruttore di classe.
	 * 
	 * @param chiamante
	 * @param documentTitle
	 * @param idFascicolo
	 * @param utente
	 */
	public AggiungiModificaContributoEstComponent(final IGestoreAggiungiModificaContributoEst chiamante, final String documentTitle, final String idFascicolo,
			final UtenteDTO utente) {
		this.chiamante = chiamante;
		this.documentTitle = documentTitle;
		this.idFascicolo = idFascicolo;
		this.utente = utente;
		this.contributo = new ContributoDTO();
		this.fileContributoList = new ArrayList<>();
	}

	/**
	 * Gestisce l'inserimento/aggiornamento di un Contributo Esterno.
	 */
	public void registra() {
		if (StringUtils.isBlank(contributo.getMittente()) || StringUtils.isBlank(contributo.getNota()) || CollectionUtils.isEmpty(fileContributoList)) {
			showErrorMessage("Inserire il mittente, testo e file del contributo esterno");
		} else {
			EsitoOperazioneDTO esito = null;
			final IContributoFacadeSRV contributoSRV = ApplicationContextProvider.getApplicationContext().getBean(IContributoFacadeSRV.class);

			// Si nasconde la dialog di inserimento/modifica
			FacesHelper.executeJS("PF('dlgAggiungiModificaContributoEst').hide()");

			// Inserimento di un nuovo Contributo Esterno
			if (nuovoContributo) {
				if (!CollectionUtils.isEmpty(fileContributoList)) {

					// Inserimento dei documenti del Contributo Esterno
					for (final FileDTO fileContributo : fileContributoList) {
						contributo.setFile(fileContributo.getFileName(), fileContributo.getMimeType(), fileContributo.getContent());

						// Inserimento del documento
						esito = contributoSRV.inserisciContributoEsternoManuale(documentTitle, contributo, idFascicolo, utente);

						if (!esito.isEsito()) {
							break;
						}
					}
				}
				// Aggiornamento di un Contributo Esterno esistente
			} else {
				// Si procede con l'aggiornamento del contributo esterno
				esito = contributoSRV.aggiornaContributo(contributo, utente);
			}

			if (esito != null) {
				if (esito.isEsito()) {
					showInfoMessage(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, esito.getNote());
					chiamante.aggiornaPostInserimentoModifica();
				} else {
					showErrorMessage(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, esito.getNote());
				}
			} else {
				showErrorMessage(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, "Si è verificato un errore durante l'inserimento del contributo esterno");
			}
		}
	}

	/**
	 * Gestisce l'upload di un file.
	 * 
	 * @param event
	 */
	public void handleFileUpload(final FileUploadEvent inEvent) {
		FileUploadEvent event = inEvent;
		try {
			event = checkFileName(event);
			if (event == null) {
				return;
			}
			final FileDTO fileContributo = new FileDTO(event.getFile().getFileName(), event.getFile().getContents(), event.getFile().getContentType());
			fileContributoList.add(fileContributo);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'upload del documento: " + event.getFile().getFileName(), e);
			showErrorMessage("Si è verificato un errore durante l'operazione di caricamento del documento");
		}
	}

	/**
	 * Rimuove il file selezionato dalla lista.
	 * 
	 * @param index
	 */
	public void rimuoviDoc(final Integer index) {
		fileContributoList.remove(index.intValue());
	}

	/**
	 * Ripulisce la maschera.
	 */
	public void reset() {
		this.fileContributoList.clear();
	}

	/** Getter e setter **/
	/**
	 * @return
	 */
	public List<FileDTO> getFileContributoList() {
		return fileContributoList;
	}

	/**
	 * @return
	 */
	public ContributoDTO getContributo() {
		return contributo;
	}

	/**
	 * @param contributo
	 */
	public void setContributo(final ContributoDTO contributo) {
		this.contributo = contributo;
	}

	/**
	 * @param fileContributoList
	 */
	public void setFileContributoList(final List<FileDTO> fileContributoList) {
		this.fileContributoList = fileContributoList;
	}

	/**
	 * @return
	 */
	public boolean isNuovoContributo() {
		return nuovoContributo;
	}

	/**
	 * @param nuovoContributo
	 */
	public void setNuovoContributo(final boolean nuovoContributo) {
		this.nuovoContributo = nuovoContributo;
	}

}
