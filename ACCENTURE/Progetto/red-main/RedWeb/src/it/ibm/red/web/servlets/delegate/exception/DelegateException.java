package it.ibm.red.web.servlets.delegate.exception;

/**
 * Exception delegate.
 */
public class DelegateException extends Exception {
	
	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 1174859823282216613L;

	/**
	 * Costruttore dell'Exception.
	 */
	public DelegateException() {
		super();
	}

	/**
	 * Costruttore completo che gestisce messaggio, cause, e i flag: enableSuppression e writableStackTrace.
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public DelegateException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * Costruttore che gestisce messaggio e cause.
	 * @param message
	 * @param cause
	 */
	public DelegateException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 * Costruttore che gestisce solo il message.
	 * @param message
	 */
	public DelegateException(final String message) {
		super(message);
	}

	/**
	 * Costruttore che gestisce solo la cause.
	 * @param cause
	 */
	public DelegateException(final Throwable cause) {
		super(cause);
	}

}