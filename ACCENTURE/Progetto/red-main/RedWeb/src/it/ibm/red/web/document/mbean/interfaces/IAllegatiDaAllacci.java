package it.ibm.red.web.document.mbean.interfaces;

import java.math.BigDecimal;
import java.util.List;

import it.ibm.red.web.mbean.interfaces.DocumentiAllegabiliSelectableInterface;

/**
 * Interface allegati da allacci.
 */
public interface IAllegatiDaAllacci extends IDocumentoAllegabileDownloader {
	
	/**
	 * Lista delle datatable per documento.
	 * Devono essere presenti ulteriori informazioni.
	 * 
	 * @return
	 */
	List<? extends DocumentiAllegabiliSelectableInterface> getAllegabiliDatatable();
	
	/**
	 * Somma delle dimensioni dei documenti selezionati.
	 */
	BigDecimal getDimensioneSelected();
}
