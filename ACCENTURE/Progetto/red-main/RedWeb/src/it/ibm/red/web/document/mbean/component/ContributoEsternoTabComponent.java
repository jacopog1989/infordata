package it.ibm.red.web.document.mbean.component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.SelectEvent;

import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.helper.ContributoHelper;
import it.ibm.red.web.mbean.VisualizzaDocumentoPrincipaleComponent;
import it.ibm.red.web.mbean.interfaces.IGestoreAggiungiModificaContributoEst;
import it.ibm.red.web.mbean.interfaces.IVisualizzaDocumentoComponent;
import it.ibm.red.web.servlets.DownloadContentServlet.DocumentTypeEnum;

/**
 * N.B. Nel caso di contributi esterni, si ha una visualizzazione master -> detail.
 * I master sono i documenti i cui content (documento principale ed eventuali allegati) sono i veri e propri documenti di contributo esterno,
 * associati al documento attualmente in esame (cioè al "detail" dell'AbstractContributoTabComponent),
 * I contributi esterni costituiscono per l'appunto il detail di questi master.
 * 
 * @author m.crescentini
 *
 */
public class ContributoEsternoTabComponent extends AbstractContributoTabComponent implements IGestoreAggiungiModificaContributoEst {
	
	private static final long serialVersionUID = 9135068403410607802L;
	
	/**
	 * Lista dei documenti di contributo esterno (detail).
	 */
	private transient List<ContributoHelper> detailDocumentiContributoList;
	
	/**
	 * Component visualizzazione documento.
	 */
	private final IVisualizzaDocumentoComponent viewDettaglioContributo;
	
	/**
	 * Dettaglio contributo.
	 */
	private DetailDocumentRedDTO detailContributo;
	
	/**
	 * Helper gestione contributi.
	 */
	private transient ContributoHelper lastMasterContributoSelected;
	
	/**
	 * Componnete aggiunta/modifica contributo.
	 */
	private final AggiungiModificaContributoEstComponent aggiungiModificaComponent;

	/**
	 * Costruttore del Component.
	 * @param detail
	 * @param utente
	 */
	public ContributoEsternoTabComponent(final DetailDocumentRedDTO detail, final UtenteDTO utente) {
		super(detail, utente);
		detailDocumentiContributoList = new ArrayList<>();
		viewDettaglioContributo = new VisualizzaDocumentoPrincipaleComponent(utente, DocumentTypeEnum.CONTRIBUTO);
		aggiungiModificaComponent = new AggiungiModificaContributoEstComponent(this, detail.getDocumentTitle(), detail.getIdFascicoloProcedimentale(), utente);
	}

	/**
	 * @see it.ibm.red.web.document.mbean.component.AbstractContributoTabComponent#updateContributo().
	 */
	@Override
	protected void updateContributo() {
		contributoList = detail.getContributi().stream()
				.filter(c -> Boolean.TRUE.equals(c.getFlagEsterno())) // Solo i contributi esterni
				.collect(Collectors.groupingBy(ContributoDTO::getProtocollo, // GroupBy Protocollo (documento in entrata da cui provengono i contributi esterni))
						Collectors.collectingAndThen(Collectors.toList(), contributiGroupBy -> contributiGroupBy.get(0)))) // È sufficiente il primo della lista
				.values()
				.stream().map(c -> new ContributoHelper(c, utente))
				.collect(Collectors.toList());
	}

	/**
	 * Gestisce l'evento select del Master nel Datatable dei Master dei Contributi Esterni.
	 * @param selectEvent
	 */
	public void onSelectMaster(final SelectEvent selectEvent) {
		lastMasterContributoSelected = (ContributoHelper) ((DataTable) selectEvent.getSource()).getSelection();
		updateDetailDocumentiContributoList(lastMasterContributoSelected);
	}

	private void updateDetailDocumentiContributoList(final ContributoHelper masterContributo) {
		detailDocumentiContributoList = detail.getContributi().stream()
				.filter(c -> masterContributo.getData().getProtocollo().equals(c.getProtocollo()))
				.filter(Objects::nonNull)
				.map(c -> new ContributoHelper(c, utente)).collect(Collectors.toList());
	}

	/**
	 * Gestisce l'eliminazione del contributo.
	 * @param contributo
	 */
	public void eliminaContributiByDocumentoContributi(final ContributoHelper contributo) {
		lastMasterContributoSelected = contributo;
		final EsitoOperazioneDTO esito = contributoSRV.eliminaContributiEsterniByProtocollo(String.valueOf(contributo.getData().getIdDocumento()), 
				contributo.getData().getProtocollo(), utente);
		
		if (esito.isEsito()) {
			this.reload();
			showInfoMessage(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, esito.getNote());
		} else {
			showErrorMessage(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, esito.getNote());
		}
	}

	/**
	 * Gestisce la visualizzazione del contributo.
	 * @param contributo
	 */
	public void openVisualizzaDocContributo(final ContributoDTO contributo) {
		detailContributo = new DetailDocumentRedDTO();
		detailContributo.setDocumentTitle(String.valueOf(contributo.getIdContributo()));
		detailContributo.setGuid(contributo.getGuidContributo());
		detailContributo.setNomeFile(contributo.getFilename());
		detailContributo.setMimeType(contributo.getMimeType());
		
		viewDettaglioContributo.setDetail(detailContributo);		
	}

	/**
	 * @see it.ibm.red.web.document.mbean.component.AbstractContributoTabComponent#eliminaContributo(it.ibm.red.business.dto.ContributoDTO,
	 *      java.lang.Integer).
	 */
	@Override
	protected EsitoOperazioneDTO eliminaContributo(final ContributoDTO contributo, final Integer idDocumento) {
		return contributoSRV.eliminaContributoEsterno(contributo.getIdContributo(), contributo.getGuidContributo(), idDocumento, utente);
	}
	
	/**
	 * @see it.ibm.red.web.document.mbean.component.AbstractContributoTabComponent#reload().
	 */
	@Override
	protected void reload() {
		super.reload();
		// Si ricaricano anche i documenti del contributo esterno
		if (lastMasterContributoSelected != null) {
			updateDetailDocumentiContributoList(lastMasterContributoSelected);
		}
	}

	/**
	 * Preparazione maschera aggiunta e modifica.
	 */
	public void preparaMascheraAggiungiModifica() {
		preparaMascheraAggiungiModifica(null);
	}

	/**
	 * Preparazione maschera aggiunta e modifica.
	 * @param inContributo
	 */
	public void preparaMascheraAggiungiModifica(final ContributoDTO inContributo) {
		// Inserimento di nuovo contributo esterno
		if (inContributo == null) {
			aggiungiModificaComponent.setNuovoContributo(true);
			aggiungiModificaComponent.setContributo(new ContributoDTO());
		// Modifica di un documento esistente del contributo esterno
		} else {
			aggiungiModificaComponent.setNuovoContributo(false);
			aggiungiModificaComponent.setContributo(inContributo);
		}
		aggiungiModificaComponent.reset();
	}
	
	
	/**
	 * Verifica della firma per un documento del contributo esterno selezionato.
	 * 
	 * @param contributo
	 */
	public void aggiornaVerificaFirma(final ContributoDTO contributo) {
		super.aggiornaVerificaFirma(contributo.getGuidContributo(), String.valueOf(contributo.getIdContributo()), utente);
		contributo.setValoreVerificaFirma(infoFirma.getMetadatoValiditaFirma());
	}
	
	
	/**
	 * @see it.ibm.red.web.mbean.interfaces.IAggiungiModificaContributoEstHandler#aggiornaPostInserimentoModifica().
	 */
	@Override
	public void aggiornaPostInserimentoModifica() {
		this.reload();
	}
	

	/** Getter e setter. **/
	public List<ContributoHelper> getDetailDocumentiContributoList() {
		return detailDocumentiContributoList;
	}

	/**
	 * Restituisce il component per la gestione della visualizzazione del documento.
	 * 
	 * @return IVisualizzaDocumentoComponent
	 */
	public IVisualizzaDocumentoComponent getViewDettaglioContributo() {
		return viewDettaglioContributo;
	}

	/**
	 * Restituisce il DTO del dettaglio del contributo.
	 * 
	 * @return detailContributo
	 */
	public DetailDocumentRedDTO getDetailContributo() {
		return detailContributo;
	}

	/**
	 * Restituisce il component per l'aggiunta e la modifica del contributo.
	 * @return AggiungiModificaContributoEstComponent
	 */
	public AggiungiModificaContributoEstComponent getAggiungiModificaComponent() {
		return aggiungiModificaComponent;
	}

}
