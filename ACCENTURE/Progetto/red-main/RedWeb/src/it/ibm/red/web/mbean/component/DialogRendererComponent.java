package it.ibm.red.web.mbean.component;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Gestisce la renderizzazione dei dialog indipendetemente dai Bean dei dialog,
 * in questo modo impedisce di passare per il post contruct
 * 
 * Permette di registrare un identificativo di dialogo da renderizzare
 * 
 * Per renderizzare un nuovo dialog si fa così:
 * 
 * e. g.
 * 
 * Nel postContruct del bean oppure dove si vuole inizializzare il bean:
 * DialogRendererComponent dialogRenderer = sessionBean.getDialogRender();
 * dialogRenderer.setDialog("wdgDialog"); ...
 * 
 * 
 * Nell'xhtml: <p:dialog rendered =
 * "SessionBean.dialogRenderer.hasDialog('wdgDialog')"
 * 
 * 'wdgDialog' è una costante che può essere il widget del dialog oppure il nome
 * del bean per evitare duplicazioni
 * 
 */
public class DialogRendererComponent implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Id.
	 */
	private final Set<String> idDialog;

	/**
	 * Costruttore.
	 */
	public DialogRendererComponent() {
		idDialog = new HashSet<>();
	}

	/**
	 * Nuova istanza con una lista vuota di dialoghi registrati.
	 * 
	 * @return
	 */
	public static DialogRendererComponent newInstance() {
		return new DialogRendererComponent();
	}
	
	/**
	 * Usare questo metodo nel renderer del dialog.
	 * 
	 * @param dialog Nuova dialog inserita
	 * @return
	 */
	public boolean hasDialog(final String dialog) {
		return idDialog.contains(dialog);
	}

	/**
	 * 
	 * Permette di registrare un nuovo dialogo da renderizzare
	 * 
	 * Imposta il dialog nell'init bean del dialog e richiamarlo nel xhtml mediante
	 * un getter dentro al dialog
	 * 
	 * e. g. public void init() { DialogRendererComponent dialogRenderer =
	 * sessionBean.getDialogRender(); dialogRenderer.setDialog("wdgDialog"); ... }
	 * 
	 * public String getDdialogDesctiption() { return "wdgDialog"; }
	 * 
	 * @param widgetNameDelDialog nome univoco del dialog nella pagina richiamata Si
	 *                            consiglia di utilizzare il nome widget Un altra
	 *                            possibilità è utilizzare il nome del bean (Ogni
	 *                            bean è singleton per sessione e vista di
	 *                            conseguenza non è possibile avere duplicazioni)
	 * 
	 * @throws IllegalArgumentException Se si prova a registrare un dialog già
	 *                                  presente nella lista
	 */
	public void setDialog(final String widgetNameDelDialog) {
		if (hasDialog(widgetNameDelDialog)) {
			throw new IllegalArgumentException(
					"Attenzione si sta registrando un dialog già presente. O non hai rimosso la stringa dal register oppure hai aggiunto una stringa già usata nel progetto per un altra dialog.");
		} else {
			idDialog.add(widgetNameDelDialog);
		}
	}

	/**
	 * Permette di rimuovere dai dialog registrati da renderizzare un campo.
	 * 
	 * @param dialog
	 * @throws IllegalArgumentException quando si prova a eliminare una string non
	 *                                  presente nel register
	 */
	public void removeDialog(final String dialog) {
		if (!idDialog.remove(dialog)) {
			throw new IllegalArgumentException("Si è tentato di rimuovere un dialog non presente nel registro. I dialog presenti attualmente sono: " + dialog);
		}
	}
}
