package it.ibm.red.web.mbean.interfaces;

import org.primefaces.model.StreamedContent;

import it.ibm.red.business.dto.VerifyInfoDTO;

/**
 * Interfaccia del componente per il pannello di verifica della firma.
 * 
 * @author m.crescentini
 *
 */
public interface IVerificaFirmaComponent {
	
	/**
	 * Info per il pannello di verifica firma, che registra le informazioni di firma appena utilizzate.
	 * 
	 * @return Un oggetto con le informazioni riguardanti la versione di cui si è appena verificata la firma.
	 */
	VerifyInfoDTO getInfoFirma();
	
	
	/**
	 * Permette di scaricare il dettaglio del'operazione verifica firma, appena effettuata.
	 *  
	 * @return Il content del riepilogo dell'operaizone di verifica firma.
	 */
	StreamedContent downloadExportVerificaFirma();
}
