package it.ibm.red.web.servlets.delegate;

import java.util.List;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.LocalSignContentDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.web.helper.lsign.LocalSignHelper.DocSignDTO;
import it.ibm.red.web.servlets.delegate.exception.DelegateException;

/**
 * 
 * @author CPIERASC
 *
 *	Interfaccia degli oggetti usati per la gestione delle get con cui l'applet di firma locale recupera i content da firmare.
 */
public interface IGetDelegate extends IDelegate {

    /**
     * Data la pk di un documento da firmare ne restituisce il content.
     * 
     * @param doc			documento da firmare
     * @param utente
     * @param ste			tipo di firma richiesta
     * @param firmaMultipla	indica se si tratta di Firma Multipla
     * @param isGlifoDelegato 
     * @return content e digest del documento da firmare
     */
	LocalSignContentDTO getDocToSignContent(String signTransactionId, DocSignDTO doc, UtenteDTO utente, SignTypeEnum ste, boolean firmaMultipla/*, Boolean isGlifoDelegato*/) throws DelegateException;

	/**
	 * Ricerca gli eventuali allegati da firmare a partire dalla pk di un documento in input.
	 * 
	 * @param documentTitle
	 * @param guid
	 * @param utente
	 * @param ste tipo di firma richiesta
	 * @return lista dei document title/nome file relativi agli allegati
	 */
	List<AllegatoDTO> getAllegatiToSign(String signTransactionId, String documentTitle, String guid, UtenteDTO utente, SignTypeEnum ste) throws DelegateException;
 

}
