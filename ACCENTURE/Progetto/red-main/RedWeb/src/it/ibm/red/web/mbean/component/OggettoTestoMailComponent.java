package it.ibm.red.web.mbean.component;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.dto.TestoPredefinitoDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.document.mbean.component.AbstractComponent;

/**
 * Componente che gestisce le interazioni tra l'oggetto e il testo di una mail.
 * Gestisce inoltre la possibilità di inserire un testo e un oggetto di default recuperato da una select.
 * 
 * @author a.difolca
 */
public class OggettoTestoMailComponent extends AbstractComponent {

	private static final long serialVersionUID = -7678905009745037837L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(OggettoTestoMailComponent.class);

	/**
	 * Gestisce i testi predefiniti per l'utente.
	 */
	private List<TestoPredefinitoDTO> testiPredefinitiList;
	/**
	 * Utilizzato da primafaces per recupaerare la selezione dell'utente.
	 */
	private Long idTestoPredefinito;

	/**
	 * Index non selezionato.
	 */
	private static final Long NOT_SELECTED = -1L;

	/**
	 * Oggetto dell'email.
	 */
	private String oggetto;

	/**
	 * Oggetto di default.
	 */
	private String defaultOggetto;

	/**
	 * Corpo dell'email.
	 */
	private String body;

	/**
	 * Costruttore di default.
	 * @param inTestiPredefinitiList
	 * @param inDefaultOggetto
	 */
	public OggettoTestoMailComponent(final List<TestoPredefinitoDTO> inTestiPredefinitiList, final String inDefaultOggetto) {
		this.testiPredefinitiList = inTestiPredefinitiList;
		this.defaultOggetto = inDefaultOggetto;
		initializeOggetto();
	}

	private void initializeOggetto() {
		if (StringUtils.isBlank(defaultOggetto)) {
			throw new RedException("Non risulta inizializzato il documento di riferimento per la resposne INOLTRA MAIL: 'master is null'");
		} else {
			this.oggetto = this.defaultOggetto;
		}
	}

	/**
	 * Siccome primefaces non consente di pescare l'oggetto come chiave dei select item tocca a noi recuperare l'oggetto in base al value.
	 * In realtà si può usare un converter per fare questa operazione ma così si scrive di meno.
	 * @return
	 * La CasellaPostaDTO selelzionata dall'utente.
	 */
	private TestoPredefinitoDTO getSelectedTesto() {
		TestoPredefinitoDTO selectedTesto = null;
		if (idTestoPredefinito != null) {
			final Optional<TestoPredefinitoDTO> optional = this.testiPredefinitiList.stream()
			.filter(m -> idTestoPredefinito.equals(m.getIdTestoPredefinito()))
			.findFirst();
			if (optional.isPresent()) {
				selectedTesto = optional.get();
			}
		}
		return selectedTesto;
	}

	/**
	 * Gestisce il cambiamento del testo predefinito.
	 */
	public void onChangeTestoPredefinito() {
		try {
			
			if (!NOT_SELECTED.equals(this.idTestoPredefinito)) {				
				final TestoPredefinitoDTO selectedTesto = getSelectedTesto();
				if (selectedTesto == null) {
					showErrorMessage("Si e' verificato un errore si prega di ricaricare la pagina.");
					throw new RedException("Durante l'evento di onchange il teto selezionato risulta essere nullo");
				}
				initializeOggetto();
				oggetto = StringUtils.join(oggetto, " ", selectedTesto.getOggetto());
				body = selectedTesto.getCorpotesto();
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showErrorMessage("Si è verificato un errore si prega di ricaricare la pagina.");
		}
	}

	/**
	 * Validazione email.
	 * @return
	 */
	public boolean validate() {
		boolean valid = true;
		if (StringUtils.isBlank(oggetto)) {
			valid = false;
			showWarnMessage("Attenzione, l'oggetto dell'e-mail è obbligatorio.");
		}
		if (StringUtils.isBlank(body)) {
			valid = false;
			showWarnMessage("Attenzione, il testo dell'e-mail è obbligatorio.");
		}
		return valid;
	}

	/**
	 * Reset valori email.
	 */
	public void reset() {
		defaultOggetto = null;
		oggetto = null;
		idTestoPredefinito = NOT_SELECTED;
	}

	/**
	 * Restituisce la lista di testi predefiniti per le email.
	 * @return testiPredefinitiList
	 */
	public List<TestoPredefinitoDTO> getTestiPredefinitiList() {
		return testiPredefinitiList;
	}

	/**
	 * Imposta la lista di testi predefiniti per le email.
	 * @param testiPredefinitiList
	 */
	public void setTestiPredefinitiList(final List<TestoPredefinitoDTO> testiPredefinitiList) {
		this.testiPredefinitiList = testiPredefinitiList;
	}

	/**
	 * Restituisce l'id del testo predefinito dell'email.
	 * @return idTestoPredefinito
	 */
	public Long getIdTestoPredefinito() {
		return idTestoPredefinito;
	}

	/**
	 * Imposta l'id del testo predefinito dell'email.
	 * @param idTestoPredefinito
	 */
	public void setIdTestoPredefinito(final Long idTestoPredefinito) {
		this.idTestoPredefinito = idTestoPredefinito;
	}

	/**
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Imposta l'oggetto della mail.
	 * @param oggetto
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * Restituisce il corpo dell'email.
	 * @return body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * Imposta il corpo dell'email.
	 * @param body
	 */
	public void setBody(final String body) {
		this.body = body;
	}
}
