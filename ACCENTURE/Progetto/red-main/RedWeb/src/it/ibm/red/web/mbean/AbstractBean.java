package it.ibm.red.web.mbean;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.FacesEvent;

import org.apache.commons.lang3.NotImplementedException;
import org.primefaces.component.datascroller.DataScroller;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.treetable.TreeTable;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.RequestParameter;
import it.ibm.red.business.dto.AbstractDTO;
import it.ibm.red.business.dto.ComuneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NotificaDTO;
import it.ibm.red.business.dto.ProvinciaDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.ITipoFileSRV;
import it.ibm.red.business.service.facade.INotificaFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.dtable.DataTableHelper;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.helper.faces.FileUploadEventCopy;
import it.ibm.red.web.helper.faces.FileUploadEventHelper;
import it.ibm.red.web.helper.faces.MessageSeverityEnum;
import it.ibm.red.web.helper.lsign.LocalSignHelper;
import it.ibm.red.web.mbean.interfaces.IDatatableUtils;
/**
 * The Class AbstractBean.
 *
 * @author CPIERASC
 * 
 *         Managed bean base
 */
public abstract class AbstractBean implements Serializable, IDatatableUtils {
	
	private static final long serialVersionUID = 1336051634250599388L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AbstractBean.class);
	
	/**
	 * Insieme flag si/no.
	 */
	private final Set<SelectItemDTO> elencoSiNo;
	
	/**
	 *  Numero di caratteri a cui troncare la stringa .
	 */
	private static Integer truncateSize = 60;
	
	/**
	 * Flag contrassegnato.
	 */
	protected boolean contrassegnato;

	/**
	 * Servizio.
	 */
	private INotificaFacadeSRV notificaSRV;

	/**
	 * Costruttore.
	 */
	protected AbstractBean() {
		elencoSiNo = BooleanFlagEnum.getSelectItems();
	}
	
	/**
	 * Getter di elencoSiNo.
	 * 
	 * @return Set di SelectItemDTO
	 * */
	public Set<SelectItemDTO> getElencoSiNo() {
		return elencoSiNo;
	}

	protected static final void orderResponse(final Collection<ResponsesRedEnum> responses) {
		if (responses != null) {
			if (responses.remove(ResponsesRedEnum.FIRMA_DIGITALE_MULTIPLA)) {
				responses.add(ResponsesRedEnum.FIRMA_DIGITALE_MULTIPLA);
			}
			if (responses.remove(ResponsesRedEnum.FIRMA_REMOTA_MULTIPLA)) {
				responses.add(ResponsesRedEnum.FIRMA_REMOTA_MULTIPLA);
			}
			if (responses.remove(ResponsesRedEnum.FIRMA_DIGITALE)) {
				responses.add(ResponsesRedEnum.FIRMA_DIGITALE);
			}
			if (responses.remove(ResponsesRedEnum.FIRMA_AUTOGRAFA)) {
				responses.add(ResponsesRedEnum.FIRMA_AUTOGRAFA);
			}
			if (responses.remove(ResponsesRedEnum.FIRMA_REMOTA)) {
				responses.add(ResponsesRedEnum.FIRMA_REMOTA);
			}
		}
	}

	/**
	 * Metodo utilizzato dai bean per visualizzare un messaggio di avviso.
	 * 
	 * @param clientId	clientId
	 * @param msg		messaggio
	 */
	protected final void showWarnMessage(final String clientId, final String msg) {
		FacesHelper.showMessage(clientId, null, msg, MessageSeverityEnum.WARN);
	}
	
	/**
	 * Metodo utilizzato dai bean per visualizzare un messaggio di avviso.
	 * 
	 * @param msg	messaggio
	 */
	protected final void showWarnMessage(final String msg) {
		FacesHelper.showMessage(null, msg, MessageSeverityEnum.WARN);
	}
	
	/**
	 * Metodo per visualizzare un messaggio informativo (mostrato ogni qual volta un metodo viene invocato con successo).
	 * 
	 * @param clientId	clientId
	 * @param msg	il messaggio da visualizzare
	 */
	protected final void showInfoMessage(final String clientId, final String msg) {
		FacesHelper.showMessage(clientId, null, msg, MessageSeverityEnum.INFO);
	}

	/**
	 * Metodo per visualizzare un messaggio informativo (mostrato ogni qual volta un metodo viene invocato con successo).
	 * 
	 * @param msg	il messaggio da visualizzare
	 */
	protected final void showInfoMessage(final String msg) {
		FacesHelper.showMessage(null, msg, MessageSeverityEnum.INFO);
	}

	/**
	 * Metodo utilizzato dai bean per visualizzare un messaggio d'errore in seguito ad una eccezione.
	 * 
	 * @param e	eccezione
	 */
	protected final void showError(final Throwable e) {
		FacesHelper.showError(e);
	}
	
	/**
	 * Metodo utilizzato dai bean per visualizzare un messaggio d'errore in seguito ad una eccezione.
	 * 
	 * @param clientId	clientId
	 * @param e			eccezione
	 */
	protected final void showError(final String clientId, final Throwable e) {
		FacesHelper.showError(clientId, e);
	}
	
	/**
	 * Metodo utilizzato dai bean per visualizzare un messaggio d'errore.
	 * 
	 * @param errorMsg	il messaggio da visualizzare
	 */
	protected final void showError(final String clientId, final String errorMsg) {
		FacesHelper.showMessage(clientId, null, errorMsg, MessageSeverityEnum.ERROR);
	}
	
	/**
	 * Metodo utilizzato dai bean per visualizzare un messaggio d'errore.
	 * 
	 * @param errorMsg	il messaggio da visualizzare
	 */
	protected final void showError(final String errorMsg) {
		FacesHelper.showMessage(null, errorMsg, MessageSeverityEnum.ERROR);
	}
	
	/**
	 * Metodo utilizzato dai bean per visualizzare un messaggio d'errore.
	 * 
	 * @param ex	l'eccezione da processare. Se l'eccezione è generica verrà mostrato un messaggio generico.
	 * se l'eccezione è una RedException verrà mostrato il messaggio di errore relativo.
	 */
	protected final void showError(final Exception inEx) {
		Exception ex = inEx;
		String redErrorMsg = "Errore durante l'esecuzione dell'operazione";
		Exception redException = null;
		
		if (ex != null) {
			if (ex instanceof RedException) {
				redException = ex;
			}
			
			while (ex.getCause() != null) {
				ex = (Exception) ex.getCause();
				if (ex instanceof RedException) {
					redException = ex;
				}
			}	
			
			if (redException != null) {
				redErrorMsg = redException.getMessage();
			}
		}
		
		FacesHelper.showMessage("", redErrorMsg, MessageSeverityEnum.ERROR);
	}


	/**
	 * Metodo invocato una sola volta dopo l'esecuzione del costruttore dell'oggetto (questo assicura che sia invocato una ed una sola volta e che alla sua esecuzione 
	 * tutte le dipendenze siano già risolte; questo non è assicurato dal normale costruttore dell'oggetto che non rientra nel ciclo di vita JSF).
	 */	
	@PostConstruct
	protected void postConstruct() {
		notificaSRV = ApplicationContextProvider.getApplicationContext().getBean(INotificaFacadeSRV.class);
	}
	

	/**
	 * Verifica che almeno uno degli input sia diverso da null.
	 * 
	 * @param objs	oggetti da verificare
	 * @return		esito check
	 */
	public static final Boolean checkAtLeastOne(final Object... objs) {
		Boolean output = false;
		for (final Object obj:objs) {
			if (obj != null) {
				output = true;
				break;
			}
		}
		return output;
	}
	
	/**
	 * Recupera i dati associati alla riga cliccata di un datatable.
	 *
	 * @param <T>
	 *            tipo dei dati associati
	 * @param event
	 *            evento della gui
	 * @return dati associati
	 */
	@SuppressWarnings("unchecked")
	protected final <T> T getDataTableClickedRow(final FacesEvent event) {
		final UIComponent component = getClickedComponent(event);
		final DataTable datatable = (DataTable) component;
		return (T) datatable.getRowData();
	}

	/**
	 * Restituisce il component cliccato.
	 * @param event
	 * @return UIComponent
	 */
	protected UIComponent getClickedComponent(final FacesEvent event) {
		UIComponent component = event.getComponent();
		while (!(component instanceof DataTable)) {
			component = component.getParent();
		}
		return component;
	}
	
	@SuppressWarnings("unchecked")
	protected final <T> T getDataScrollerClickedRow(final FacesEvent event) {
		final UIComponent component = getClickedComponentDataScroller(event);
		final DataScroller dataScroller = (DataScroller) component;
		return (T) dataScroller.getRowData();
	}

	/**
	 * Restituisce lo <em> UIComponent </em> cliccato.
	 * 
	 * @param event
	 *            evento di click
	 * @return component cliccato
	 */
	protected UIComponent getClickedComponentDataScroller(final FacesEvent event) {
		UIComponent component = event.getComponent();
		while (!(component instanceof DataScroller)) {
			component = component.getParent();
		}
		return component;
	}


	/**
	 * Recupera i dati associati alla riga cliccata di un tree.
	 *
	 * @param <T>
	 *            tipo dei dati associati
	 * @param event
	 *            evento della gui
	 * @return dati associati
	 */
	@SuppressWarnings("unchecked")
	protected final <T> T getTreeTableClickedRow(final ActionEvent event) {
		UIComponent component = event.getComponent();
		while (!(component instanceof TreeTable)) {
			component = component.getParent();
		}
		final TreeTable tn = (TreeTable) component;
		return (T) tn.getRowNode().getData();
	}

	/**
	 * Metodo per visualizzare un messaggio inseguito ad una ricerca.
	 * 
	 * @param panelFiltersName	id del pannello contenente i filtri di ricerca
	 * @param cData				risultati della ricerca
	 */
	protected final void showSearchInfoMessage(final String panelFiltersName, final Collection<? extends Object> cData) {
		if (cData == null || cData.isEmpty()) {
			showWarnMessage("La ricerca non ha prodotto alcun risultato.");
		} else {
			if (!StringUtils.isNullOrEmpty(panelFiltersName)) {
				FacesHelper.executeJS("PF('" + panelFiltersName + "').unselect(0);");
			}
			showInfoMessage("Ricerca eseguita con successo.");
		}
	}
	
	/**
	 * Metodo per generare il messaggio di pulizia dei campi.
	 */
	protected final void showClearSearchInfoMessage() {
		showInfoMessage("Campi di ricerca svuotati.");
	}

	protected static String getLoggedUsername() {
		return getLoggedUsername(true);
	}
	
	
	/**
	 * Recupero username utente loggato.
	 * 
	 * @return	utente loggato
	 */
	protected static String getLoggedUsername(final Boolean inErrorWithoutUser) {
		final String username = getParameter(RequestParameter.USERNAME);
		
		if (Boolean.TRUE.equals(inErrorWithoutUser) && StringUtils.isNullOrEmpty(username)) {
			throw new RedException("Impossibile recuperare l'utente di riferimento.");
		}
		
		FacesHelper.putObjectInSession(Constants.RequestParameter.USERNAME, username);
		
		return username;
	}
	
	
	/**
	 * Recupera un attributo da una request cercando nel seguente ordine: 
	 * - Come attributo;
	 * - Come parametro;
	 * - Come parametro inserito nell'header.
	 * 
	 * @param key
	 * @return
	 */
	protected static String getParameter(final String key) {
		String parameter = (String) FacesHelper.getAttributeFromRequest(key);
		if (parameter == null) {
			parameter = (String) FacesHelper.getParameterFromRequest(key);
			if (parameter == null) {
				parameter = (String) FacesHelper.getParameterFromHeader(key);
			}
		}
		
		return parameter;
	}
	
	/**
	 * Instanzia un nuovo LocalSignHelper registrandolo in sessione, avendone rimosso prima eventuali istanze pregresse.
	 *  
	 * @param inSource
	 * @param isGlifoDelegato 
	 * @param inAction
	 * @param ids
	 * @return 
	 */
	protected LocalSignHelper startLocalSignHelper(final String inSource, final SignTypeEnum inSte, final List<MasterDocumentRedDTO> docs, final UtenteDTO utente, 
			final boolean copiaConforme, final boolean firmaMultipla) {
		FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.SESSION_LSH, true);
		
		final LocalSignHelper lsh = LocalSignHelper.getInstance(inSource, inSte, copiaConforme, firmaMultipla);
    	lsh.sign(docs, utente, copiaConforme);
        
        FacesHelper.putObjectInSession(ConstantsWeb.SessionObject.SESSION_LSH, lsh);
        
        return lsh;
    }
	
	/** 
	 * Abbrevia il label presente in una colonna di un datatable con i puntini.
	 * Inserire il label intero con i puntini utilizzanfo l'apposito metodo getColumnTooltip
	 * 
	 * Gestisce il caso label vuoto restituisce il classico  " - "
	 * 
 	 * @param columnLabel
	 * 	Stringa da visualizzare nella colonna
	 * @param size
	 * 	massimo numero di caratteri del label che si vogliono visualizzare
	 *  
	 * @return la stringa columnLabel se maggiore di size troncata di size e aggiungendo i punti '...' alla fine
	 * se columnLabel è null ritorna " - "
	 * 
	 */
	@Override
	public String getShortColumn(final String columnLabel, final Integer size) {
		return StringUtils.truncateColumnTable(columnLabel, size);
	}
	
	/**
	 * Restituisce una label abbreviata della label passata come parametro.
	 * 
	 * @param columnLabel la label che deve essere abbreviata.
	 * @return la label abbreviata, troncata come definito dalla variabile truncateSize
	 * */
	public String getShortColumn(final String columnLabel) {
		return StringUtils.truncateColumnTable(columnLabel, truncateSize);
	}
	
	/**
	 * Quando una colonna necessita di essere abbreviata con i puntini.
	 * Usare questo metodo nel xhtml nel tooltip della colonna per permettere di visualizzare il dato per esteso qualora questo sia sostituito dai puntini
	 * 
	 * @param columnLabel
	 * 	Stringa da visualizzare nella colonna
	 * @param size
	 * 	massimo numero di caratteri del label che si vogliono visualizzare
	 * @return
	 * Se il columnLabel non supera la size allora ritorna null (Non viene visualizzato il tooltip)
	 */
	@Override
	public String getColumnTooltip(final String columnLabel, final Integer size) {
		return StringUtils.calculateColumnTooltip(columnLabel, size);
	}
	
	/**
	 * Formatta la data passata in input tramite l'opportuno formatter.
	 * Utilizzare nell'xhtml quando non è possibile utilizzare il dataConverter nativo.
	 * 
	 * @param date
	 *  Date da formattare.
	 * @param fomratter
	 *  Stringa di formattazione da utilizzare nel simpledateFormat.
	 * @return
	 *  la data formattata secondo il formatter.
	 */
	public String getDateAsString(final Date date, final String fomratter) {
		return DateUtils.dateToString(date, fomratter);
	}

	/**
	 * Getter dell'attributo truncateSize.
	 * 
	 * @return la dimensione definita come dimensione di troncamento che stabilisce la massima lunghezza delle label delle colonne
	 * */
	public static Integer getTruncateSize() {
		return truncateSize;
	}

	/**
	 * Restituisce il datatable identificato dall' {@code idComponent}.
	 * 
	 * @param idComponent
	 *            identificativo component
	 * @return datatable con identificativo {@code idComponent} se esiste
	 */
	protected DataTable getDataTableByIdComponent(final String idComponent) {
		return  (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(idComponent); 
	}

	/**
	 * Esegue una verifica sul file per cui è stato fatto l'upload ed è identificato
	 * dall' {@code evet}.
	 * 
	 * @param event
	 *            evento di upload di un file
	 * @return helper per la verifica del file
	 * @throws IOException
	 */
	protected FileUploadEvent checkFileName(final FileUploadEvent event) throws IOException {
		final FileUploadEventHelper helper = new FileUploadEventHelper(event);
		final UploadedFile file = helper.getFile();
		final ITipoFileSRV tipoFileSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipoFileSRV.class);
		final String nomeFileNormalizzato = tipoFileSRV.normalizzaNomeFile(event.getFile().getFileName());
		((FileUploadEventCopy) file).setFileName(nomeFileNormalizzato);
		try {
			tipoFileSRV.verificaFile(event.getFile().getContents(), event.getFile().getContentType(), event.getFile().getFileName());
		} catch (final Exception ex) {
			LOGGER.error("Il tipo di file caricato non risulta conforme", ex);
			showError("Il tipo di file caricato non risulta conforme");
			return null;
		}
		return helper;
	}
	
	/**
	 * Fornisce il max value per metadati di tipo intero data la dimensione del
	 * metadato stesso. Questo metodo è necessario in quanto l'attributo maxlenght
	 * presente nei component inputNumber di primefaces non funziona correttamente.
	 * 
	 * @param dimension
	 * @return max value per input number
	 */
	public String getMaxValue(Integer dimension) {
		StringBuilder temp = new StringBuilder("");
		if(dimension != null) {
			for (int i = 0; i< dimension; i++) {
				temp.append("9");
			}
		} else {
			temp.append("999999999");
		}
		
		return temp.toString();
	}
	
	/**
	 * @param element
	 */
	protected void contrassegnaNotifica(final NotificaDTO element, final SimpleDetailDataTableHelper<NotificaDTO> notificheDTH) {
		if (element.getVisualizzato() == 0) { //se non è stato già visualizzato
			//per mantenere le eventuali selezioni nella GUI
			final List<NotificaDTO> tempSelected = new ArrayList<>();
			if (!notificheDTH.getSelectedMasters().isEmpty()) {
				tempSelected.addAll(notificheDTH.getSelectedMasters());
				notificheDTH.getSelectedMasters().clear();
			}
			notificheDTH.getSelectedMasters().add(element);
			contrassegna(notificheDTH);
			if (!notificheDTH.getSelectedMasters().isEmpty()) {
				notificheDTH.getSelectedMasters().clear();
				notificheDTH.getSelectedMasters().addAll(tempSelected);
			}
		}
	}
	
	/**
	 * Contrassegna le notifiche selezionate.
	 */
	protected void contrassegna(final SimpleDetailDataTableHelper<NotificaDTO> notificheDTH) {
		try {
			if(notificaSRV == null) {
				notificaSRV = ApplicationContextProvider.getApplicationContext().getBean(INotificaFacadeSRV.class);
			}
			contrassegnato = notificaSRV.contrassegnaNotifiche(notificheDTH.getSelectedMasters());
			notificheDTH.setMasters(updateNotificheTbl(null));
		} catch (final Exception e) {
			showError("Errore durante la contrassegnazione. " + e.getMessage());
			LOGGER.error("Errore durante la contrassegnazione", e);
		}
	}
	
	/**
	 * @param element
	 */
	protected void rimuoviNotifica(final NotificaDTO element, final SimpleDetailDataTableHelper<NotificaDTO> notificheDTH) {
		if (element.getEliminato() == 0) { //se non è stato già eliminato
			//per mantenere le eventuali selezioni nella GUI
			final List<NotificaDTO> tempSelected = new ArrayList<>();
			if (!notificheDTH.getSelectedMasters().isEmpty()) {
				tempSelected.addAll(notificheDTH.getSelectedMasters());
				notificheDTH.getSelectedMasters().clear();
			}
			notificheDTH.getSelectedMasters().add(element);
			rimuovi(notificheDTH);
			if (!notificheDTH.getSelectedMasters().isEmpty()) {
				notificheDTH.getSelectedMasters().clear();
				notificheDTH.getSelectedMasters().addAll(tempSelected);
			}
		}
	}
	
	/**
	 * Gestisce l'eliminazioni delle notifiche selezionate dal datatable delle notifiche.
	 */
	protected void rimuovi(final SimpleDetailDataTableHelper<NotificaDTO> notificheDTH) {
		try {
			if(notificaSRV == null) {
				notificaSRV = ApplicationContextProvider.getApplicationContext().getBean(INotificaFacadeSRV.class);
			}
			notificaSRV.eliminaNotifiche(notificheDTH.getSelectedMasters());
			notificheDTH.setMasters(updateNotificheTbl(null));
			showInfoMessage("Rimozione andata a buon fine.");
		} catch (final Exception e) {
			showError("Errore durante l'eliminazione delle notifiche selezionate. " + e.getMessage());
			LOGGER.error("Errore durante l'eliminazione delle notifiche selezionate", e);
		}
	}
	
	/**
	 * Update table notifiche da implementare.
	 * @param idNotifica
	 * @return lista notifiche aggiornata
	 */
	protected List<NotificaDTO> updateNotificheTbl(final Integer idNotifica){
		throw new NotImplementedException("Metodo da implementare nelle sottoclassi");
	}

	/**
	 * Restituisce true se contrassegnato, false altrimenti.
	 * @return true se contrassegnato, false altrimenti
	 */
	public boolean getContrassegnato() {
		return contrassegnato;
	}
	
	/**
	 * Imposta il flag associato al contrassegno.
	 * @param contrassegnato
	 */
	public void setContrassegnato(final boolean contrassegnato) {
		this.contrassegnato = contrassegnato;
	}
	
	protected <M extends AbstractDTO, D extends AbstractDTO> void handleAllCheckBoxGeneric(final DataTableHelper<M,D> dth) {
		if (dth != null) {
			Boolean elementCheckbox = dth.getCheckAll();
			if (elementCheckbox == null) {
				elementCheckbox = false;
			}
			if (dth.getMasters() != null && !dth.getMasters().isEmpty()) {
				if (!elementCheckbox) {
					elementCheckbox = true;
				} else {
					elementCheckbox = false;
				}
				dth.getSelectedMasters().clear();
				for (final M m : dth.getMasters()) {
					m.setSelected(elementCheckbox);
					if (elementCheckbox) {
						dth.getSelectedMasters().add(m);
					}
				}	
			}
			dth.setCheckAll(elementCheckbox);
		}
	}
	
	/**
	 * Effettua un update sulle regioni del contatto selezionato.
	 * 
	 * @param contatto
	 */
	public void updateRegioniOnSelectAdd(final Contatto contatto) {
		if (contatto.getRegioneObj() == null) {
			contatto.setIdRegioneIstat(null);
			contatto.setRegione(null);
		} else {
			contatto.setIdRegioneIstat(contatto.getRegioneObj().getIdRegione());
			contatto.setRegione(contatto.getRegioneObj().getDenominazione());
		}
		contatto.setComuneObj(new ComuneDTO());
		contatto.setProvinciaObj(new ProvinciaDTO());
		contatto.setIdProvinciaIstat(null);
		contatto.setIdComuneIstat(null);
	}

	/**
	 * Effettua un update sulle province del contatto selezionato.
	 * 
	 * @param contatto
	 */
	public void updateProvinceOnSelectAdd(final Contatto contatto) {
		if (contatto.getProvinciaObj() == null) {
			contatto.setIdProvinciaIstat(null);
			contatto.setProvincia(null);
		} else {
			contatto.setIdProvinciaIstat(contatto.getProvinciaObj().getIdProvincia());
			contatto.setProvincia(contatto.getProvinciaObj().getDenominazione());
		}
		contatto.setComuneObj(new ComuneDTO());
		contatto.setIdComuneIstat(null);
	}

	/**
	 * Effettua un update sui comuni del contatto selezionato.
	 * 
	 * @param contatto
	 */
	public void updateComuniOnSelectAdd(final Contatto contatto) {
		if (contatto.getComuneObj() == null) {
			contatto.setIdComuneIstat(null);
			contatto.setComune(null);
		} else {
			contatto.setIdComuneIstat(contatto.getComuneObj().getIdComune());
			contatto.setComune(contatto.getComuneObj().getDenominazione());
		}
	}
	
	/**
	 * @param c
	 * @param d
	 */
	protected void setContattoDestinatario(final Contatto c, final DestinatarioRedDTO d) {
		d.setContatto(c);
		if (StringUtils.isNullOrEmpty(c.getMailSelected())) {
			d.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.CARTACEO);
			d.setMezzoSpedizioneDisabled(true);
		} else {
			d.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.ELETTRONICO);
			d.setMezzoSpedizioneDisabled(false);
		}

		d.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
		d.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
		d.setMezzoSpedizioneVisible(true);
	}
}
