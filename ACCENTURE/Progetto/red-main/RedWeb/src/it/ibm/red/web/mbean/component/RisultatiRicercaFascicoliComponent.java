package it.ibm.red.web.mbean.component;

import java.util.Collection;

import org.primefaces.event.SelectEvent;

import it.ibm.red.business.dto.DetailFascicoloRedDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.DettaglioFascicoloRicercaBean;
import it.ibm.red.web.mbean.interfaces.IUpdatableDetailCaller;

/**
 * Component che gestisce la visualizzazione dei risultati di ricerca fascicoli.
 */
public class RisultatiRicercaFascicoliComponent implements IUpdatableDetailCaller<DetailFascicoloRedDTO> {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 4776229113854105573L;
	
	/**
     * Fascicoli.
     */
	private final SimpleDetailDataTableHelper<FascicoloDTO> fascicoliDTH;
	
	/**
	 * Costruttore.
	 * @param fascicoli
	 */
	public RisultatiRicercaFascicoliComponent(final Collection<FascicoloDTO> fascicoli) {
		fascicoliDTH = new SimpleDetailDataTableHelper<>();
		fascicoliDTH.setMasters(fascicoli);
		fascicoliDTH.selectFirst(true);
		selectFascicoloDetail(fascicoliDTH.getCurrentMaster());
	}
	
	/**
	 * Gestisce la selezione della riga del datatable dei fascicoli.
	 * @param se
	 */
	public final void fasRowSelector(final SelectEvent se) {
		fascicoliDTH.rowSelector(se);
		selectFascicoloDetail(fascicoliDTH.getCurrentMaster());
	}
	
	/**
	 * Imposta il fascicolo selezionato.
	 * @param f
	 */
	private void selectFascicoloDetail(final FascicoloDTO f) {
		final DettaglioFascicoloRicercaBean bean = FacesHelper.getManagedBean(MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN);
		
		if (f == null) {
			bean.unsetDetail();
			return;
		}
		
		bean.setDetail(f);
		bean.setCaller(this);
	
	}

	/**
	 * Restituisce l'helper per la gestione del datatable dei fascicoli.
	 * @return helper datatable dei fascicoli
	 */
	public SimpleDetailDataTableHelper<FascicoloDTO> getFascicoliDTH() {
		return fascicoliDTH;
	}
	
	/**
	 * Gestisce l'update del dettaglio.
	 * @param detail dettaglio da aggiornare
	 */
	@Override
	public void updateDetail(final DetailFascicoloRedDTO detail) {		
		if (fascicoliDTH != null && fascicoliDTH.getMasters() != null && !fascicoliDTH.getMasters().isEmpty()) {
			for (final FascicoloDTO f : fascicoliDTH.getMasters()) {
				if (f.getIdFascicolo().equals(detail.getNomeFascicolo())) {
					f.setIndiceClassificazione(detail.getTitolarioDTO().getIndiceClassificazione());
					break;
				}
			}
		}
	}
}
