package it.ibm.red.web.dto;

import java.util.ArrayList;
import java.util.List;

import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipologiaNodoEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;

/**
 * DTO che definisce le caratteristiche dell'organigramma interoperabilità.
 */
public class OrganigrammaInteroperabilitaDTO extends AbstractDTO{

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(OrganigrammaInteroperabilitaDTO.class);

	/**
	 * Root albero.
	 */
	private DefaultTreeNode root;

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Casella mail.
	 */
	private final String casellaMail;

	/**
	 * Servizio.
	 */
	private IOrganigrammaFacadeSRV orgSRV;

	/**
	 * Pre assegnatario.
	 */
	private String descrizionePreassegnatario;

	/**
	 * Ufficio.
	 */
	private Long idUfficio;

	/**
	 * Descriione ufficio.
	 */
	private String descrizioneUfficio;

	/**
	 * Id utente.
	 */
	private Long idUtente;

	/**
	 * Descrizione utente.
	 */
	private String descrizioneUtente;

	/**
	 * Nodo organigramma selezionato.
	 */
	private NodoOrganigrammaDTO selected;

	/**
	 * Costruttore di default.
	 * @param utente
	 * @param casellaMail
	 */
	public OrganigrammaInteroperabilitaDTO(final UtenteDTO utente, final String casellaMail) {
		this.utente = utente;
		this.casellaMail = casellaMail;
		orgSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
		loadRootOrganigramma();
	}

	/**
	 * Recupera l'alberatura a partire dalle info dell'utente (aoo, ufficio) per poi costruire l'organigramma.
	 */
	public void loadRootOrganigramma() {
		try {
			final List<Nodo> alberaturaNodi = orgSRV.getAlberaturaBottomUp(utente.getIdAoo(), utente.getIdUfficio());
			
			final List<Long> alberatura = new ArrayList<>();
			for (int i = alberaturaNodi.size() - 1; i >= 0; i--) {
				alberatura.add(alberaturaNodi.get(i).getIdNodo());
			}
			//imposto un DTO utile per creare la struttura del tree
			final NodoOrganigrammaDTO radice = new NodoOrganigrammaDTO();
			radice.setIdAOO(utente.getIdAoo());
			radice.setIdNodo(utente.getIdNodoRadiceAOO());
			radice.setCodiceAOO(utente.getCodiceAoo());
			radice.setUtentiVisible(false);

			final List<NodoOrganigrammaDTO> primoLivello = getOrgSRV().getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovo(true, utente.getIdUfficio(), radice, casellaMail, false);

			// Creazione del nodo padre necessario
			root = new DefaultTreeNode();
			root.setExpanded(true);
			root.setSelectable(false);
			root.setChildren(new ArrayList<>());
			root.setType("UFF");

			// Per ogni nodo di primo livello creo il nodo da mettere nell'albero che ha come padre root
			List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
			DefaultTreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(utente.getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, root);
				nodoToAdd.setExpanded(true);
				if (n.getTipoNodo().equals(TipologiaNodoEnum.UTENTE)) {
					nodoToAdd.setType("USER");
				} else {
					nodoToAdd.setType("UFF");
				}
				if (n.getIdUtente() == null && alberatura.contains(n.getIdNodo())) {
					nodiDaAprire.add(nodoToAdd);
				}
			}
			final List<DefaultTreeNode> nodiDaAprireTemp = new ArrayList<>();
			for (int i = 0; i < alberatura.size(); i++) {
				//devo fare esattamente un numero di cicli uguale alla lunghezza dell'alberatura
				nodiDaAprireTemp.clear();
				for (final DefaultTreeNode nodo : nodiDaAprire) {
					final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) nodo.getData();
					onOpenNode(nodoExp, nodo);
					nodo.setExpanded(true);
					for (final TreeNode nodoFiglio : nodo.getChildren()) {
						final NodoOrganigrammaDTO nodoFiglioDto = (NodoOrganigrammaDTO)nodoFiglio.getData();
						if (nodoFiglioDto.getIdUtente() == null && alberatura.contains(nodoFiglioDto.getIdNodo())) {
							nodiDaAprireTemp.add((DefaultTreeNode)nodoFiglio);
						}
					}
				}
				nodiDaAprire = new ArrayList<>(nodiDaAprireTemp);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
		}
	}

	/**
	 * Recupera il nodo dall'evento e ne gestisce l'apertura.
	 * {@link #onOpenNode(NodoOrganigrammaDTO, TreeNode)}
	 * @param event
	 */
	public void onOpenNode(final NodeExpandEvent event) {
		final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) event.getTreeNode().getData();
		onOpenNode(nodoExp, event.getTreeNode());
	}

	/**
	 * Gestisce l'apertura di un nodo dell'organigramma.
	 * @param nodoExp
	 * @param treeNode
	 */
	public void onOpenNode(final NodoOrganigrammaDTO nodoExp, final TreeNode treeNode) {
		try {
			final List<NodoOrganigrammaDTO> figli = getOrgSRV().getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovo(true, utente.getIdUfficio(), nodoExp, casellaMail, false);

			DefaultTreeNode nodoToAdd = null;
			treeNode.getChildren().clear();
			for (final NodoOrganigrammaDTO n : figli) {
				n.setCodiceAOO(utente.getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, treeNode);
				if (n.getTipoNodo().equals(TipologiaNodoEnum.UTENTE)) {
					nodoToAdd.setType("USER");
				} else {
					nodoToAdd.setType("UFF");
				}
				if (n.getFigli() > 0 || n.isUtentiVisible()) {
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * Gestisce la selezione di un nodo dall'organigramma.
	 * @param event
	 */
	public void onSelectedNode(final NodeSelectEvent event) {
		descrizioneUfficio = Constants.EMPTY_STRING;
		descrizioneUtente = Constants.EMPTY_STRING;
		// Recupero l'utente selezionato dall'evento e lo utilizzo poi per l'invocazione del service
		setSelected((NodoOrganigrammaDTO) event.getTreeNode().getData());
		
		setDescrizioneUfficio(getSelected().getDescrizioneNodo());
		setIdUfficio(getSelected().getIdNodo());
		
		if (getSelected().getTipoNodo().equals(TipologiaNodoEnum.UFFICIO)) {
			setDescrizionePreassegnatario(getSelected().getDescrizioneNodo());
		} else {
			setIdUtente(getSelected().getIdUtente());
			setDescrizioneUtente(getSelected().getCognomeUtente() + "," +getSelected().getNomeUtente());
			setDescrizionePreassegnatario(getSelected().getDescrizioneNodo() + " - " + getSelected().getCognomeUtente() + " " + getSelected().getNomeUtente());
		}
	}

	/**
	 * Imposta l'utente corrente.
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * @return root
	 */
	public final TreeNode getRoot() {
		return root;
	}

	/**
	 * Restituisce il Service per l'organigramma.
	 * @return orgSRV
	 */
	public IOrganigrammaFacadeSRV getOrgSRV() {
		return orgSRV;
	}

	/**
	 * Imposta il Service per l'organigramma.
	 * @param orgSRV
	 */
	public void setOrgSRV(final IOrganigrammaFacadeSRV orgSRV) {
		this.orgSRV = orgSRV;
	}

	/**
	 * @return descrizionePreassegnatario
	 */
	public String getDescrizionePreassegnatario() {
		return descrizionePreassegnatario;
	}

	/**
	 * Imposta la descrzione pre-assegnatario.
	 * @param descrizionePreassegnatario
	 */
	public void setDescrizionePreassegnatario(final String descrizionePreassegnatario) {
		this.descrizionePreassegnatario = descrizionePreassegnatario;
	}

	/**
	 * Restituisce il nodo selezionato.
	 * @return selected
	 */
	public NodoOrganigrammaDTO getSelected() {
		return selected;
	}

	/**
	 * Imposta il nodo selezionato.
	 * @param selected
	 */
	public void setSelected(final NodoOrganigrammaDTO selected) {
		this.selected = selected;
	}

	/**
	 * Deseleziona il nodo e "pulisce" i relativi campi.
	 */
	public void clearAssegnazione() {
		selected = null;
		descrizionePreassegnatario = Constants.EMPTY_STRING;
		descrizioneUfficio = Constants.EMPTY_STRING;
		descrizioneUtente = Constants.EMPTY_STRING;
		idUfficio = null;
		idUtente = null;
	}

	/**
	 * @return descrizioneUfficio
	 */
	public String getDescrizioneUfficio() {
		return descrizioneUfficio;
	}

	/**
	 * Imposta la descrizione dell'ufficio.
	 * @param descrizioneUfficio
	 */
	public void setDescrizioneUfficio(final String descrizioneUfficio) {
		this.descrizioneUfficio = descrizioneUfficio;
	}

	/**
	 * @return descrizioneUtente
	 */
	public String getDescrizioneUtente() {
		return descrizioneUtente;
	}

	/**
	 * Imposta la descrizione del'utente nel nodo.
	 * @param descrizioneUtente
	 */
	public void setDescrizioneUtente(final String descrizioneUtente) {
		this.descrizioneUtente = descrizioneUtente;
	}

	/**
	 * @return idUfficio
	 */
	public Long getIdUfficio() {
		return idUfficio;
	}

	/**
	 * Imposta l'id dell'ufficio.
	 * @param idUfficio
	 */
	public void setIdUfficio(final Long idUfficio) {
		this.idUfficio = idUfficio;
	}

	/**
	 * @return idUtente
	 */
	public Long getIdUtente() {
		return idUtente;
	}

	/**
	 * Imposta l'id dell'utente.
	 * @param idUtente
	 */
	public void setIdUtente(final Long idUtente) {
		this.idUtente = idUtente;
	}
}
