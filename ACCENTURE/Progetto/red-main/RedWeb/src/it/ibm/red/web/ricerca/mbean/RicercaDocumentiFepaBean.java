/**
 * 
 */
package it.ibm.red.web.ricerca.mbean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.StatoRicercaDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * @author Aperquoti
 *
 */
@Named(ConstantsWeb.MBean.RICERCA_DOCUMENTI_FEPA_BEAN)
@ViewScoped
public class RicercaDocumentiFepaBean extends RicercaDocumentiRedAbstractBean {

	private static final long serialVersionUID = -2603448777365647027L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaDocumentiFepaBean.class.getName());
	
	/**
	 * Datatable documenti fepa.
	 */
	protected SimpleDetailDataTableHelper<MasterDocumentRedDTO> documentiFepaDTH;
	
	/**
	 * Parametri ricerca.
	 */
	private ParamsRicercaAvanzataDocDTO formRicerca;
	
	/**
	 * Colonne.
	 */
	private List<Boolean> fepaColumns;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		super.postConstruct();
		
		documentiFepaDTH = new SimpleDetailDataTableHelper<>();
		documentiFepaDTH.setMasters(new ArrayList<>());
		
		fepaColumns = Arrays.asList(true, true, false, true, false, false, false, false, false, false);
		
		initPaginaRicerca();
	}

	/**
	 * Inizializza la pagina di ricerca.
	 */
	@SuppressWarnings("unchecked")
	public void initPaginaRicerca() {
		try {
			final StatoRicercaDTO stDto = (StatoRicercaDTO) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.STATO_RICERCA, true);
			if (stDto != null) {
				final Collection<MasterDocumentRedDTO> searchResult = (Collection<MasterDocumentRedDTO>) stDto.getRisultatiRicerca();
				formRicerca = (ParamsRicercaAvanzataDocDTO) stDto.getFormRicerca();
				
				documentiFepaDTH.setMasters(searchResult);
				documentiFepaDTH.selectFirst(true);
				selectDetail(documentiFepaDTH.getCurrentMaster());
				
				if (getNumMaxRisultati() != null && searchResult != null && getNumMaxRisultati().equals(searchResult.size())) {
					showWarnMessage(RicercaAbstractBean.MSG_NUM_MAX_RISULTATI);
				}
			} else {
				formRicerca = new ParamsRicercaAvanzataDocDTO();
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Non è stato possibile recuperare i risultati di ricerca");
		}
	}

	/**
	 * Gestisce la selezione della riga del datatable dei documenti FEPA.
	 * @param se
	 */
	public final void rowSelector(final SelectEvent se) {
		documentiFepaDTH.rowSelector(se);
		selectDetail(documentiFepaDTH.getCurrentMaster());
	}

	/**
	 * Metodo per la selezione e aggiornamento del dettaglio dall'esterno
	 */
	public void aggiorna() {
		if (documentiFepaDTH != null && documentiFepaDTH.getCurrentMaster() != null) {
			selectDetail(documentiFepaDTH.getCurrentMaster());
		}
	}

	/**
	 * Questo metodo è utile solo se ci sta annullaDocumento riattivaProcedimento 
	 */
	@Override
	public MasterDocumentRedDTO getSelectedDocument() {
		return documentiFepaDTH.getCurrentMaster();
	}
	
	/**
	 * Metodo per reimpostare nel session bean il form utilizzato per la ricerca e quindi visualizzarlo nel dialog
	 */
	public void ripetiRicercaRedFepa() {
		try {
			// Viene ripetuta la ricerca con i stessi filtri
			sessionBean.getRicercaFormContainer().setFepa(formRicerca);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Gestisce la selezione e la deselezione della colonna.
	 * @param event
	 */
	public final void onColumnToggleFepa(final ToggleEvent event) {
		fepaColumns.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
	}

//	<-- get/set -->

	/**
	 * @return the documentiFepaDTH
	 */
	public final SimpleDetailDataTableHelper<MasterDocumentRedDTO> getDocumentiFepaDTH() {
		return documentiFepaDTH;
	}

	/**
	 * @param documentiFepaDTH the documentiFepaDTH to set
	 */
	public final void setDocumentiFepaDTH(final SimpleDetailDataTableHelper<MasterDocumentRedDTO> documentiFepaDTH) {
		this.documentiFepaDTH = documentiFepaDTH;
	}

	/**
	 * @return the fepaColumns
	 */
	public final List<Boolean> getFepaColumns() {
		return fepaColumns;
	}

	/**
	 * @param fepaColumns the fepaColumns to set
	 */
	public final void setFepaColumns(final List<Boolean> fepaColumns) {
		this.fepaColumns = fepaColumns;
	}	
	
	/**
	 * Restituisce la label del prefisso del nome file excel per l'estrazione.
	 */
	@Override
	protected String getNomeRicerca4Excel() {
		return "ricercafepa_";
	}
	
}