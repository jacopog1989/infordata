package it.ibm.red.web.helper.faces;

import java.io.IOException;

import org.primefaces.event.FileUploadEvent;

/**
 * 
 * @author vingenito
 *
 *	
 */
public class FileUploadEventHelper extends FileUploadEvent {
	
	private static final long serialVersionUID = 795123208311986219L;

	
	/**
	 * Evento.
	 */
	private FileUploadEventCopy file;

	/**
	 * Costruttore dell'helper.
	 * @param event
	 * @throws IOException
	 */
	public FileUploadEventHelper(final FileUploadEvent event) throws IOException {
		super(event.getComponent(), event.getFile());
		file = new FileUploadEventCopy(event.getFile()); 
	}

	/**
	 * Restituisce il file come FileUploadEventCopy.
	 */
	@Override
	public FileUploadEventCopy getFile() {
		return file;
	}

	/**
	 * Imposta il file.
	 * @param file
	 */
	public void setFile(final FileUploadEventCopy file) {
		this.file = file;
	}


}
