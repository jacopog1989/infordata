package it.ibm.red.web.document.mbean;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.TrasmissioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IFepaFacadeSRV;
import it.ibm.red.business.service.facade.IPredisponiDsrFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.mbean.component.DettaglioDocumentoStoricoComponent;
import it.ibm.red.web.document.mbean.component.DsrPresentazioneBean;
import it.ibm.red.web.document.mbean.interfaces.IDsrPresentazioneBean;
import it.ibm.red.web.document.mbean.interfaces.ITabAllegatiBean;
import it.ibm.red.web.document.mbean.interfaces.ITabFascicoliBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.humantask.component.DestinatariInterniComponent;
import it.ibm.red.web.mbean.humantask.component.PredisponiDsrTabGeneraleComponent;

/**
 * Bean che gestisce il dettaglio dichiarazione.
 */
@Named(ConstantsWeb.MBean.DETTAGLIO_DICHIARAZIONE)
@ViewScoped
public class DettaglioDichiarazioneBean extends AbstractBean {

	/*
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1370561330764291484L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DettaglioDichiarazioneBean.class);

	/**
	 * Service predisposizione.
	 */
	private IPredisponiDsrFacadeSRV predisponiSRV;

	/**
	 * Info utente.
	 */
	private UtenteDTO utente;

	/**
	 * Flag abilita registrazione.
	 */
	private boolean canRegister = true;

	/**
	 * Questo bean è valorizzato inizialmente nel MailBean con questi valori.
	 * 
	 * Allegati (No content) nomeFile mimetype formatoELETTRONICO FILE PRINCIPALE
	 * nomeFile principale mimetype content anche questo potrebbe essere ricavato
	 * PROTOCOLLAZIONE protocollazioneMailGuid mittenteContatto
	 *
	 * // isNotifica // idCategoriaDocumento
	 */
	private DetailDocumentRedDTO detail;

	/**
	 * Componente tab generale.
	 */
	private PredisponiDsrTabGeneraleComponent tabGenerale;

	/**
	 * Tab fascicoli.
	 */
	private ITabFascicoliBean tabFascicoli;

	/**
	 * Bean allegati.
	 */
	private DsrDataTabAllegatiBean tabAllegati;

	/**
	 * Bean presentazione.
	 */
	private IDsrPresentazioneBean presentazione;

	/**
	 * Component destinatari interni.
	 */
	private DestinatariInterniComponent destinatariInterni;

	/**
	 * Servizio fepa.
	 */
	private IFepaFacadeSRV fepaSRV;

	/**
	 * Flag reigstrazione destinatari.
	 */
	private boolean canRegisterDestinatari;

	/**
	 * Flag tab trasmissione.
	 */
	private boolean showTabTrasmissione;

	/**
	 * Dettaglio dati trasmisione.
	 */
	private TrasmissioneDTO trasmissioneDetail;

	/**
	 * Storico.
	 */
	private DettaglioDocumentoStoricoComponent storico;

	/**
	 * @see it.ibm.red.web.mbean.AbstractBean#postConstruct().
	 */
	@Override
	@PostConstruct
	public void postConstruct() {
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();

		predisponiSRV = ApplicationContextProvider.getApplicationContext().getBean(IPredisponiDsrFacadeSRV.class);

		fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);

		if (hasTabStorico()) {
			storico = new DettaglioDocumentoStoricoComponent(getClass().getSimpleName());
		}
	}

	/**
	 * Popola il detail con i parametri passati come parametri.
	 * 
	 * @param documentTitle
	 * @param wobNumber
	 * @param canRegisterMittente
	 * @param isinEdit
	 */
	public void popolaDettaglio(final String documentTitle, final String wobNumber, final boolean canRegisterMittente, final boolean isinEdit) {
		detail = predisponiSRV.getDetailDSRPredisposta(documentTitle, wobNumber, utente);
		initializeManagerBean();
		final boolean verificaModificabilita = predisponiSRV.isModificabile(detail, utente);

		setCanRegisterDestinatari(!canRegisterMittente);

		getTabAllegati().getDocumentManagerDTO().setInModificaIngresso(isinEdit); // viene gestito al contrario dal predisponi dichiarazione
		getTabAllegati().getDocumentManagerDTO().setAllegatiModificabili(verificaModificabilita);
		setCanRegister(isinEdit && verificaModificabilita);
	}

	/**
	 * Al momento dell'apertura della maschera setta il dettaglio da mostrare.
	 * 
	 * @param detail
	 */
	private void initializeManagerBean() {
		try {
			tabGenerale = new PredisponiDsrTabGeneraleComponent(detail, utente);
			tabFascicoli = new DsrDataTabFascicoliBean(detail);
			tabAllegati = new DsrDataTabAllegatiBean(detail, utente);
			presentazione = new DsrPresentazioneBean(detail, false);
			destinatariInterni = new DestinatariInterniComponent();
			if (hasTabStorico()) {
				storico.setDetail(detail);
				storico.buildStoricoGui();
				storico.setValid(detail.getDateCreated());
			}

			initTabTrasmissione();
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}
	}

	/**
	 * Esegue la predisposizione della dichiarazione. Gestisce eventuali errori e
	 * mostra l'esito a valle delle operazioni.
	 */
	public void registra() {
		final String message = "Si è verificato un errore durante la predisposizione della dichiarazione.";
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(detail.getNumeroProtocollo(), detail.getAnnoProtocollo(), detail.getNumeroDocumento(),
				detail.getWobNumberSelected());
		try {
			// pulisco gli esiti
			byte[] content = null;
			final boolean contentVariato = tabGenerale.getUploadDocumento().isUploaded();
			if (contentVariato) {
				content = detail.getContent();
			}

			if (!tabAllegati.validateAllegati()) {
				throw new RedException("Alcuni allegati non sono stati compilati correttamente");
			}

			final EsitoSalvaDocumentoDTO esitoSd = predisponiSRV.aggiornaDichiarazione(utente, detail.getDocumentTitle(), detail.getWobNumberSelected(), detail.getNomeFile(),
					detail.getOggetto(), detail.getAllegati(), content, contentVariato);

			esito.setEsito(esitoSd.isEsitoOk());
			esito.setNote(esitoSd.getNote());
			showInfoMessage("Registrazione avvenuta con successo");
		} catch (final RedException red) {
			LOGGER.error("Si è verificato un errore durante la registrazione in un dettaglio esteso DSR", red);
			esito.setEsito(false);
			esito.setNote(red.getMessage());
			showError(message);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la registrazione in un dettaglio esteso DSR", e);
			esito.setEsito(false);
			esito.setNote("Errore durante la registrazione della dichiarazione");
			showError(message);
		} finally {
			close();
		}
	}

	private void initTabTrasmissione() {
		showTabTrasmissione = detail.getIdCategoriaDocumento().equals(CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0]);
		if (showTabTrasmissione) {
			trasmissioneDetail = fepaSRV.getTrasmissione(detail.getGuid(), utente);
			// in questo modo chiamo suolo una volta filenet e non uso la condizione '&&
			// trasmissioneServices.getMailAllegata(dichiarazione.getId()) != null)' che c'è
			// su red
			if (trasmissioneDetail == null) {
				showTabTrasmissione = false;
			}
		}

	}

	/**
	 * Si occupa della logica da eseguire alla chiusura della dialog.
	 */
	public void close() {
		// Non deve fare niente.
	}

	/**
	 * Restituisce il Component associato al tab generale.
	 * 
	 * @return tabGenerale
	 */
	public PredisponiDsrTabGeneraleComponent getTabGenerale() {
		return tabGenerale;
	}

	/**
	 * Restituisce il bean dei fascicoli.
	 * 
	 * @return tabFascicoli
	 */
	public ITabFascicoliBean getTabFascicoli() {
		return tabFascicoli;
	}

	/**
	 * Restituisce il bean degli allegati.
	 * 
	 * @return tabAllegati
	 */
	public ITabAllegatiBean getTabAllegati() {
		return tabAllegati;
	}

	/**
	 * Restituisce il bean per la presentazione DSR.
	 * 
	 * @return presentazione
	 */
	public IDsrPresentazioneBean getPresentazione() {
		return presentazione;
	}

	/**
	 * Restituisce il component per la gestione dei destinatari interni.
	 * 
	 * @return DestinatariInterniComponent
	 */
	public DestinatariInterniComponent getDestinatariInterni() {
		return destinatariInterni;
	}

	/**
	 * Restituisce il detail.
	 * 
	 * @return detail
	 */
	public DetailDocumentRedDTO getDetail() {
		return detail;
	}

	/**
	 * Imposta il detail.
	 * 
	 * @param detail
	 */
	public void setDetail(final DetailDocumentRedDTO detail) {
		this.detail = detail;
	}

	/**
	 * Restituisce canRegister.
	 * 
	 * @return canRegister
	 */
	public boolean isCanRegister() {
		return canRegister;
	}

	/**
	 * @param canRegister
	 */
	public void setCanRegister(final boolean canRegister) {
		this.canRegister = canRegister;
	}

	/**
	 * @return canRegisterDestinatari
	 */
	public boolean isCanRegisterDestinatari() {
		return canRegisterDestinatari;
	}

	/**
	 * @param canRegisterDestinatari
	 */
	public void setCanRegisterDestinatari(final boolean canRegisterDestinatari) {
		this.canRegisterDestinatari = canRegisterDestinatari;
	}

	/**
	 * Restituisce il flag associato alla visibilita del tab trasmissione.
	 * 
	 * @return showTabTrasmissione
	 */
	public boolean isShowTabTrasmissione() {
		return showTabTrasmissione;
	}

	/**
	 * Imposta il flag associato alla visibilita del tab trasmissione.
	 * 
	 * @param showTabTrasmissione
	 */
	public void setShowTabTrasmissione(final boolean showTabTrasmissione) {
		this.showTabTrasmissione = showTabTrasmissione;
	}

	/**
	 * @return the trasmissioneDetail
	 */
	public TrasmissioneDTO getTrasmissioneDetail() {
		return trasmissioneDetail;
	}

	/**
	 * @param trasmissioneDetail the trasmissioneDetail to set
	 */
	public void setTrasmissioneDetail(final TrasmissioneDTO trasmissioneDetail) {
		this.trasmissioneDetail = trasmissioneDetail;
	}

	/**
	 * Decide se il tab storico è presente o meno
	 * 
	 * Estendi per modificare per default è impostato a true
	 * 
	 * Una selezione alternativa poteva essere che veniva creato alla creaizione del
	 * get ma non mi piaceva molto.
	 * 
	 * @return
	 */
	public boolean hasTabStorico() {
		return true;
	}

	/**
	 * Restituisce il component che gestisce lo storico.
	 * 
	 * @return DettaglioDocumentoStoricoComponent
	 */
	public DettaglioDocumentoStoricoComponent getStorico() {
		return storico;
	}

}
