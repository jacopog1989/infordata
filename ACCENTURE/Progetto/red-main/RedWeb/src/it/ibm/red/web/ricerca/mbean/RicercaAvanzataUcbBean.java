/**
 * 
 */
package it.ibm.red.web.ricerca.mbean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.poi.ss.formula.eval.NotImplementedException;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO;
import it.ibm.red.business.dto.EstrattiContoUcbDTO;
import it.ibm.red.business.dto.FascicoloFlussoDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.ParamsRicercaContiConsuntiviDTO;
import it.ibm.red.business.dto.ParamsRicercaDocUCBAssegnanteAssegnatarioDTO;
import it.ibm.red.business.dto.ParamsRicercaEstrattiContoDTO;
import it.ibm.red.business.dto.RicercaAvanzataDocUcbFormDTO;
import it.ibm.red.business.dto.RisultatiRicercaUcbDTO;
import it.ibm.red.business.dto.RisultatoRicercaContiConsuntiviDTO;
import it.ibm.red.business.dto.StatoRicercaDTO;
import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.RicercaAssegnanteAssegnatarioEnum;
import it.ibm.red.business.enums.TipologiaNodoEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IRicercaAvanzataDocSRV;
import it.ibm.red.business.service.ITitolarioSRV;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaAvanzataDocUCBFacadeSRV;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.SessionObject;
import it.ibm.red.web.document.mbean.component.OrganigrammaCompleteComponent;
import it.ibm.red.web.document.mbean.interfaces.IAssOrganigrammaComponent;
import it.ibm.red.web.dto.RicercaFormContainer;
import it.ibm.red.web.enums.NavigationTokenEnum;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractTreeBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.component.DialogRendererComponent;

/**
 * @author APerquoti
 *
 */
public class RicercaAvanzataUcbBean extends AbstractTreeBean {

	private static final String PF_STATUS_DIALOG_SHOW = "PF('statusDialog').show();";

	private static final String ERRORE_IN_FASE_DI_RICERCA_AVANZATA_DEI_DOCUMENTI = "Errore in fase di ricerca avanzata dei documenti.";

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -8926437636722335919L;

	/**
	 * Messaggio visualizzato quando la ricerca non ha avuto alcun esito.
	 */
	private static final String NO_DOC_FOUND_MSG = "Nessun documento trovato.";
	
	/**
	 * Messaggio errore operazione.
	 */
	private static final String GENERIC_ERROR_OPERAZIONE_MSG = "Errore durante l'operazione: ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaAvanzataUcbBean.class.getName());

	/**
	 * Bean sessione.
	 */
	private SessionBean sessionBean;

	/**
	 * Service contenente la logica di ricerca.
	 */
	private final IRicercaAvanzataDocUCBFacadeSRV ricercaSRV;

	/**
	 * Service contenente la logica di ricerca.
	 */
	private final IRicercaAvanzataDocSRV ricercaAvanzataDoc;

	/**
	 * Service contenente la logica di recupero delle tipologie Documento.
	 */
	private final ITipologiaDocumentoFacadeSRV tipoDocSRV;

	/**
	 * Service titolario
	 */
	private final ITitolarioSRV titolarioSRV;

	/**
	 * Service per la creazione dell'organigramma uffici
	 */
	private final IOrganigrammaFacadeSRV organigrammaSRV;

	/**
	 * DTO che rappresenta i dati da caricare nel form di ricerca Documenti.
	 */
	private RicercaAvanzataDocUcbFormDTO formDocumenti;

	/**
	 * Component per l'organigramma della ricerca UCB assegnante/assegnatario.
	 */
	private IAssOrganigrammaComponent assegnatarioOrgComponent;

	/**
	 * Lista nodi.
	 */
	private List<Nodo> alberaturaNodi;

	/**
	 * Radice assegnazioni.
	 */
	private DefaultTreeNode rootAssegnazione;

	/**
	 * Radice albero protocollatore.
	 */
	private DefaultTreeNode rootProtocollatore;

	/**
	 * Indice attivo.
	 */
	private String activeIndexTabView;

	/**
	 * Componente dialog.
	 */
	private DialogRendererComponent dialogRenderer;

	/**
	 * Pagina attiva.
	 */
	private NavigationTokenEnum activePageInfo;

	/**
	 * Costruttore.
	 */
	public RicercaAvanzataUcbBean(final UtenteDTO inUtente) {
		utente = inUtente;

		// <-- Service -->
		ricercaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaAvanzataDocUCBFacadeSRV.class);
		ricercaAvanzataDoc = ApplicationContextProvider.getApplicationContext().getBean(IRicercaAvanzataDocSRV.class);
		tipoDocSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoFacadeSRV.class);
		titolarioSRV = ApplicationContextProvider.getApplicationContext().getBean(ITitolarioSRV.class);
		organigrammaSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
		alberaturaNodi = organigrammaSRV.getAlberaturaBottomUp(utente.getIdAoo(), utente.getIdUfficio());

		// <-- Init -->
		loadFormRicercaUCB();
		loadTreeAssegnazione();
		loadTreeProtocollatore();
		initTabRicercaAssegnazioneUCB();
	}
	
	@Override
	protected void postConstruct() {
		// Non deve fare nulla
	}

	/**
	 * Inizializza il form sulla ricerca UCB.
	 */
	public void loadFormRicercaUCB() {
		try {
			if (formDocumenti == null) {
				formDocumenti = new RicercaAvanzataDocUcbFormDTO();
			}

			ricercaSRV.initRicercaDoc(utente, formDocumenti);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore nel caricamento della maschera di ricerca avanzata");
		}
	}

	/**
	 * Gestisce l'evento di modifica della tipologia documento, aggiornando tipi
	 * procedimento e metadati selezionabili.
	 */
	public void onChangeTipologiaDocumento() {
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();

		if (!StringUtils.isNullOrEmpty(formContainer.getDocumentiUCB().getDescrizioneTipologiaDocumento())) {
			if ("-".equals(formContainer.getDocumentiUCB().getDescrizioneTipologiaDocumento().trim())) {

				formDocumenti.setListaMetadatiEstesi(new ArrayList<>());
				formDocumenti.setComboTipiProcedimento(new ArrayList<>());
				formDocumenti.getComboTipiProcedimento().add(new TipoProcedimentoDTO(new TipoProcedimento(0, "-")));

			} else {

				final List<TipoProcedimentoDTO> comboTipiProcedimento = ricercaAvanzataDoc
						.getTipiProcedimentoByTipologiaDocumento(formContainer.getDocumentiUCB().getDescrizioneTipologiaDocumento(), utente.getIdAoo());
				formDocumenti.setComboTipiProcedimento(comboTipiProcedimento);

				// Svuoto in ogni caso la lista dei metadati estesi.
				formDocumenti.setListaMetadatiEstesi(new ArrayList<>());
			}
		}

		formContainer.getDocumentiUCB().setDescrizioneTipoProcedimento(null);
	}

	/**
	 * Gestisce l'evento di modifica del tipo procedimento, aggiornando i metadati
	 * estesi selezionabili.
	 */
	public void onChangeTipoProcedimento() {
		final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();
		List<MetadatoDTO> listaMetadati = new ArrayList<>();
		TipologiaDocumentoDTO tipoDocSelected = null;
		TipoProcedimentoDTO tipoProcSelected = null;

		if (!StringUtils.isNullOrEmpty(formContainer.getDocumentiUCB().getDescrizioneTipoProcedimento())) {
			if ("-".equals(formContainer.getDocumentiUCB().getDescrizioneTipoProcedimento().trim())) {
				formDocumenti.setListaMetadatiEstesi(new ArrayList<>());
			} else {

				tipoDocSelected = getTipoDoc(formContainer.getDocumentiUCB().getDescrizioneTipologiaDocumento(), formDocumenti.getComboTipologieDocumento());

				if (tipoDocSelected != null) {
					tipoProcSelected = ricercaAvanzataDoc.getTipiProcedimentoByIdTipologiaDocumentoDescrProc(formContainer.getDocumentiUCB().getDescrizioneTipoProcedimento(),
							utente.getIdAoo(), tipoDocSelected.getIdTipologiaDocumento());
				}

				if (tipoDocSelected != null && tipoProcSelected != null) {
					final int idProc = (int) tipoProcSelected.getTipoProcedimentoId();
					listaMetadati = tipoDocSRV.caricaMetadatiEstesiPerGUI(tipoDocSelected.getIdTipologiaDocumento(), idProc, false, utente.getIdAoo(), null);

					if (!listaMetadati.isEmpty()) {
						formDocumenti.setListaMetadatiEstesi(listaMetadati);
					}
				}

				if (!listaMetadati.isEmpty()) {
					formDocumenti.setListaMetadatiEstesi(listaMetadati);
				} else {
					formDocumenti.setListaMetadatiEstesi(new ArrayList<>());
				}

			}
		}
	}

	private static TipologiaDocumentoDTO getTipoDoc(final String desc, final List<TipologiaDocumentoDTO> comboTipologieDocumento) {
		TipologiaDocumentoDTO output = null;

		for (final TipologiaDocumentoDTO td : comboTipologieDocumento) {
			if (desc.equals(td.getDescrizione())) {
				output = td;
				break;
			}
		}

		return output;
	}

	/**
	 * Esegue la ricerca utilizzando @see RicercaFormContainter esistente sul
	 * {@link #sessionBean} e memorizza i risultati.
	 * 
	 * @return l'url per la visibilita della view dei risultati.
	 */
	public String searchAvanzataUCB() {
		try {
			sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);

//			<-- Validazione -->
			final String msg = isFormValid();
			if (msg != null) {
				showWarnMessage(msg);
				return null;
			}

//			<-- Ricerca Business -->
			final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();
			final RisultatiRicercaUcbDTO risultatiDocumentiUCB = ricercaSRV.eseguiRicercaAvanzataUCB(formContainer.getDocumentiUCB(), formDocumenti.getListaMetadatiEstesi(),
					sessionBean.getUtente());

//			<-- Gestione risultati-->
			if (!manageResult(risultatiDocumentiUCB, formContainer.getDocumentiUCB())) {
				return null;
			}

//			<-- Clear Form-->
			sessionBean.pulisciFormRicerca();

//			<-- Navigazione -->
			return sessionBean.gotoRicercaDocumentiUCB();
		} catch (final Exception e) {
			LOGGER.error(ERRORE_IN_FASE_DI_RICERCA_AVANZATA_DEI_DOCUMENTI, e);
			showError(e);
		}

		return "";
	}

	/**
	 * Esegue la validazione dei paremtri del form di ricerca.
	 * 
	 * @return null se non ci sono errori riscontrati nella validazione, il
	 *         messaggio di errore altrimenti
	 */
	public String isFormValid() {
		String out = null;
		final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();
		if ((formContainer.getDocumentiUCB().getAnnoDocumento() <= 0) || (formContainer.getDocumentiUCB().getDataCreazioneA() == null
				&& formContainer.getDocumentiUCB().getDataCreazioneDa() == null && formContainer.getDocumentiUCB().getDataProtocolloEmergenzaA() == null
				&& formContainer.getDocumentiUCB().getDataProtocolloEmergenzaDa() == null
				&& (StringUtils.isNullOrEmpty(formContainer.getDocumentiUCB().getDescrizioneTipologiaDocumento())
						|| "-".equals(formContainer.getDocumentiUCB().getDescrizioneTipologiaDocumento()))
				&& StringUtils.isNullOrEmpty(formContainer.getDocumentiUCB().getDestinatario()) && StringUtils.isNullOrEmpty(formContainer.getDocumentiUCB().getMittente())
				&& StringUtils.isNullOrEmpty(formContainer.getDocumentiUCB().getNote()) && formContainer.getDocumentiUCB().getNumeroProtocolloA() == null
				&& formContainer.getDocumentiUCB().getNumeroProtocolloDa() == null && formContainer.getDocumentiUCB().getNumeroProtocolloEmergenzaA() == null
				&& formContainer.getDocumentiUCB().getNumeroProtocolloEmergenzaDa() == null && StringUtils.isNullOrEmpty(formContainer.getDocumentiUCB().getOggetto())
				&& StringUtils.isNullOrEmpty(formContainer.getDocumentiUCB().getDescrizioneTitolario())
				&& StringUtils.isNullOrEmpty(formContainer.getDocumentiUCB().getDescrUfficioComp())
				&& StringUtils.isNullOrEmpty(formContainer.getDocumentiUCB().getDescrUfficioAss())
				&& StringUtils.isNullOrEmpty(formContainer.getDocumentiUCB().getProtocollatore()))

		) {
			out = "Restringere la ricerca valorizzando alcuni dei campi.";
		} else if (!DateUtils.isRangeOk(formContainer.getDocumentiUCB().getDataCreazioneDa(), formContainer.getDocumentiUCB().getDataCreazioneA())) {
			out = "La data creazione da deve essere antecedente alla data creazione a.";
		} else if (!DateUtils.isRangeOk(formContainer.getDocumentiUCB().getDataProtocolloEmergenzaDa(), formContainer.getDocumentiUCB().getDataProtocolloEmergenzaA())) {
			out = "La data di 'protocollazione emergenza da' deve essere antecedente alla data di 'protocollazione emergenza a'.";
		} else if (!StringUtils.isNullOrEmpty(formContainer.getDocumentiUCB().getDescrizioneTipologiaDocumento())
				&& !"-".equals(formContainer.getDocumentiUCB().getDescrizioneTipologiaDocumento())
				&& "-".equals(formContainer.getDocumentiUCB().getDescrizioneTipoProcedimento())) {
			out = "Valorizzare anche il tipo procedimento insieme alla tipologia documento";
		}

		return out;
	}

	/**
	 * Esegue la ricerca per flussi UCB.
	 * 
	 * @return url per la visualizzazione dei risultati della ricerca
	 */
	public String searchFlussiUCB() {
		try {
			sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);

//			<-- Validazione -->
			final String msg = isFormFlussiValid();
			if (msg != null) {
				showWarnMessage(msg);
				return null;
			}

//			<-- Ricerca Business -->
			final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();
			final List<FascicoloFlussoDTO> results = ricercaSRV.eseguiRicercaFlussiUCB(formContainer.getFlussiUCB(), sessionBean.getUtente());

//			<-- Gestione risultati-->
			if (results.isEmpty()) {
				showInfoMessage(NO_DOC_FOUND_MSG);
				return null;
			} else {
				FacesHelper.putObjectInSession(ConstantsWeb.SessionObject.RISULTATO_RICERCA_FLUSSI, results);
				FacesHelper.executeJS(PF_STATUS_DIALOG_SHOW);
			}

//			<-- Clear Form-->
			sessionBean.pulisciFormRicerca();

//			<-- Navigazione -->
			return sessionBean.gotoRicercaFlussiUCB();
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di ricerca flussi.", e);
			showError(e);
		}
		return "";
	}

	/**
	 * Esegue la ricerca Esiti UCB.
	 * 
	 * @return url per la visualizzazione dei risultati della ricerca
	 */
	public String searchEsitiUCB() {

		try {
			sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);

//			<-- Validazione -->
			final String msg = isFormEsitiValid();
			if (msg != null) {
				showWarnMessage(msg);
				return null;
			}

//			<-- Ricerca Business -->
			final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();
			final RisultatiRicercaUcbDTO risultatiDocumentiUCB = ricercaSRV.eseguiRicercaEsitiUCB(formContainer.getEsitiVistiOss(), sessionBean.getUtente());

//			<-- Gestione risultati-->
			if (!manageResult(risultatiDocumentiUCB, formContainer.getDocumentiUCB())) {
				return null;
			}

//			<-- Clear Form-->
			sessionBean.pulisciFormRicerca();

//			<-- Navigazione -->
			return sessionBean.gotoRicercaDocumentiUCB();

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di ricerca esiti.", e);
			showError(e);
		}

		return "";
	}

	/**
	 * Esegue la validazione dei paremtri del form di ricerca.
	 * 
	 * @return null se non ci sono errori riscontrati nella validazione, il
	 *         messaggio di errore altrimenti
	 */
	public String isFormFlussiValid() {
		String out = null;
		final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();

		final Boolean bNoRange = formContainer.getFlussiUCB().getDataProtMittente() == null && formContainer.getFlussiUCB().getDataAperturaDa() == null
				&& formContainer.getFlussiUCB().getDataAperturaA() == null && formContainer.getFlussiUCB().getAmbitoTemporale() == null;
		final Boolean bNoPK = formContainer.getFlussiUCB().getNumeroPratica() == null && formContainer.getFlussiUCB().getIdProvvedimento() == null
				&& formContainer.getFlussiUCB().getNumProtMittente() == null;

		if (bNoRange && bNoPK) {
			out = "Restringere la ricerca.";
		} else if (!DateUtils.isRangeOk(formContainer.getFlussiUCB().getDataAperturaDa(), formContainer.getFlussiUCB().getDataAperturaA())) {
			out = "La data apertura da deve essere antecedente la data apertura a.";
		}

		return out;
	}

	/**
	 * Esegue la validazione dei paremtri del form di ricerca.
	 * 
	 * @return null se non ci sono errori riscontrati nella validazione, il
	 *         messaggio di errore altrimenti
	 */
	public String isFormEsitiValid() {
		String out = null;
		final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();

		if ((formContainer.getEsitiVistiOss().getDataCreazioneA() == null && formContainer.getEsitiVistiOss().getDataCreazioneDa() == null
				&& formContainer.getEsitiVistiOss().getNumeroProtocolloA() == null && formContainer.getEsitiVistiOss().getNumeroProtocolloDa() == null)) {

			out = "Restringere la ricerca valorizzando almeno uno dei due range.";

		} else if (!DateUtils.isRangeOk(formContainer.getEsitiVistiOss().getDataCreazioneDa(), formContainer.getEsitiVistiOss().getDataCreazioneA())) {
			out = "La data creazione da deve essere antecedente la data creazione a.";
		}

		return out;
	}

	/**
	 * Imposta l'assegnatario della ricerca UCB assegnante/assegnatario con la
	 * selezione effettuata nell'organigramma.
	 */
	public void impostaAssegnatario() {
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		sessionBean.getRicercaFormContainer().getAssegnazioneUCB()
				.setAssegnatario(new AssegnatarioOrganigrammaDTO(assegnatarioOrgComponent.getSelected().getIdNodo(), assegnatarioOrgComponent.getSelected().getIdUtente()));
		sessionBean.getRicercaFormContainer().getAssegnazioneUCB().setDescrizioneAssegnatario(assegnatarioOrgComponent.getDescrizioneAssegnatario());
	}

	/**
	 * Esegue la ricerca Assegnazione UCB.
	 * 
	 * @return url per la visualizzazione dei risultati della ricerca
	 */
	public final String ricercaAssegnazioneUCB() {
		try {
			sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
			final ParamsRicercaDocUCBAssegnanteAssegnatarioDTO paramsAssegnazioneUCB = sessionBean.getRicercaFormContainer().getAssegnazioneUCB();

			final String msg = isFormRicercaAssegnazioneUCBOk(paramsAssegnazioneUCB);
			if (msg != null) {
				showWarnMessage(msg);
				return null;
			}

			final IRicercaAvanzataDocUCBFacadeSRV ricercaAvanzataUCBSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaAvanzataDocUCBFacadeSRV.class);
			final RisultatiRicercaUcbDTO risultatiAssegnazioneUCB = ricercaAvanzataUCBSRV.eseguiRicercaDocumentiAssegnanteAssegnatario(paramsAssegnazioneUCB, utente, false);

//			<-- Gestione risultati-->
			if (!manageResult(risultatiAssegnazioneUCB, paramsAssegnazioneUCB)) {
				return null;
			}

//			<-- Clear Form-->
			sessionBean.pulisciFormRicerca();

//			<-- Navigazione -->
			return sessionBean.gotoRicercaDocumentiUCB();
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di ricerca per assegnante/assegnatario UCB", e);
			showError("Errore in fase di ricerca per assegnante/assegnatario");
		}

		return "";
	}

	/**
	 * Validazione del form per la ricerca assegnante/assegnatario.
	 * 
	 * @param paramsAssegnazioneUCB
	 * @return
	 */
	private static String isFormRicercaAssegnazioneUCBOk(final ParamsRicercaDocUCBAssegnanteAssegnatarioDTO paramsAssegnazioneUCB) {
		String out = null;

		if (paramsAssegnazioneUCB.getTipoRicerca() == null) {
			out = "Selezionare il tipo di ricerca";
		} else if (paramsAssegnazioneUCB.getAssegnatario() == null || StringUtils.isNullOrEmpty(paramsAssegnazioneUCB.getDescrizioneAssegnatario())) {
			out = "Selezionare la struttura di riferimento per l'assegnazione.";
		} else if (paramsAssegnazioneUCB.getDataCreazioneDa() == null && paramsAssegnazioneUCB.getDataCreazioneA() == null && paramsAssegnazioneUCB.getDataScadenzaDa() == null
				&& paramsAssegnazioneUCB.getDataScadenzaA() == null && paramsAssegnazioneUCB.getNumeroProtocolloDa() == null
				&& paramsAssegnazioneUCB.getNumeroProtocolloA() == null) {
			out = "Inserire almeno un range tra Data Creazione, Data Scadenza e Numero Protocollo";
		} else if (paramsAssegnazioneUCB.getDataCreazioneDa() == null && paramsAssegnazioneUCB.getDataCreazioneA() != null) {
			out = "'Data Creazione DA' deve essere impostata, se si decide di inserire 'Data Creazione A'";
		} else if (paramsAssegnazioneUCB.getDataScadenzaDa() == null && paramsAssegnazioneUCB.getDataScadenzaA() != null) {
			out = "'Data Scadenza DA' deve essere impostata, se si decide di inserire 'Data Scadenza A'";
		} else if ((!DateUtils.isRangeOk(paramsAssegnazioneUCB.getDataCreazioneDa(), paramsAssegnazioneUCB.getDataCreazioneA()))
				|| (!DateUtils.isRangeOk(paramsAssegnazioneUCB.getDataScadenzaDa(), paramsAssegnazioneUCB.getDataScadenzaA()))) {
			out = "Inserire un range di date valido";
		} else if (paramsAssegnazioneUCB.getNumeroProtocolloDa() == null && paramsAssegnazioneUCB.getNumeroProtocolloA() != null) {
			out = "Il Numero Protocollo 'DA' deve essere impostato, se si decide di inserire un Numero Protocollo 'A'";
		} else if (paramsAssegnazioneUCB.getNumeroProtocolloDa() != null && paramsAssegnazioneUCB.getNumeroProtocolloA() == null) {
			out = "Il Numero Protocollo 'A' deve essere impostato, se si decide di inserire un Numero Protocollo 'DA'";
		} else if (paramsAssegnazioneUCB.getNumeroProtocolloDa() != null && paramsAssegnazioneUCB.getNumeroProtocolloA() != null
				&& (paramsAssegnazioneUCB.getNumeroProtocolloA() < paramsAssegnazioneUCB.getNumeroProtocolloDa())) {
			out = "Il Numero Protocollo 'DA' non può essere maggiore del numero protocollo 'A'";
		} else if (paramsAssegnazioneUCB.getNumeroProtocolloDa() != null && paramsAssegnazioneUCB.getNumeroProtocolloA() != null
				&& paramsAssegnazioneUCB.getAnnoProtocollo() == null) {
			out = "L'Anno Protocollo deve essere impostato, se si decide di inserire un range per il Numero Protocollo";
		}

		return out;
	}

	private <T> boolean manageResult(final RisultatiRicercaUcbDTO result, final T formRicerca) {
		boolean isOk = false;

		if (result.getMasters().isEmpty()) {
			showInfoMessage(NO_DOC_FOUND_MSG);
		} else {
			final StatoRicercaDTO stDto = new StatoRicercaDTO(formRicerca, result);
			FacesHelper.putObjectInSession(SessionObject.STATO_RICERCA, stDto);
			FacesHelper.executeJS(PF_STATUS_DIALOG_SHOW);
			isOk = true;
		}

		return isOk;
	}

	/**
	 * Effettua un caricamento dei nodi associati all'albero dell'assegnazione.
	 */
	public void loadTreeAssegnazione() {
		try {
			final List<Long> alberatura = new ArrayList<>();
			for (int i = alberaturaNodi.size() - 1; i >= 0; i--) {
				alberatura.add(alberaturaNodi.get(i).getIdNodo());
			}

			final List<NodoOrganigrammaDTO> primoLivello = getPrimoLivelloAssegnazione();

			rootAssegnazione = new DefaultTreeNode();
			rootAssegnazione.setExpanded(true);
			rootAssegnazione.setSelectable(false);

			List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
			rootAssegnazione.setChildren(new ArrayList<>());
			DefaultTreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(utente.getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, rootAssegnazione);
				nodoToAdd.setExpanded(true);
				if (n.getIdUtente() == null && alberatura.contains(n.getIdNodo())) {
					nodiDaAprire.add(nodoToAdd);
				}
			}

			final List<DefaultTreeNode> nodiDaAprireTemp = new ArrayList<>();
			for (int i = 0; i < alberatura.size(); i++) {
				nodiDaAprireTemp.clear();
				for (final DefaultTreeNode nodo : nodiDaAprire) {
					onOpenTreeAss(nodo);
					nodo.setExpanded(true);
					for (final TreeNode nodoFiglio : nodo.getChildren()) {
						final NodoOrganigrammaDTO nodoFiglioDto = (NodoOrganigrammaDTO) nodoFiglio.getData();
						if (nodoFiglioDto.getIdUtente() == null && alberatura.contains(nodoFiglioDto.getIdNodo())) {
							nodiDaAprireTemp.add((DefaultTreeNode) nodoFiglio);
						}
					}
				}
				nodiDaAprire = new ArrayList<>(nodiDaAprireTemp);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());
		}
	}

	/**
	 * @return lista dei nodi di primo livello per l'alberatura dell'assegnatario
	 */
	private List<NodoOrganigrammaDTO> getPrimoLivelloAssegnazione() {
		final NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
		nodoRadice.setIdAOO(utente.getIdAoo());
		nodoRadice.setIdNodo(utente.getIdNodoRadiceAOO());
		nodoRadice.setCodiceAOO(utente.getCodiceAoo());
		nodoRadice.setUtentiVisible(false);
		List<NodoOrganigrammaDTO> primoLivello = null;
		primoLivello = organigrammaSRV.getFigliAlberoAssegnatarioPerCompetenzaUscita(utente, nodoRadice);
		return primoLivello;
	}

	/**
	 * Inizializza l'alberatura dell'albero per la selezione del protocollatore.
	 */
	public void loadTreeProtocollatore() {
		try {
			final List<Long> alberatura = new ArrayList<>();
			for (int i = alberaturaNodi.size() - 1; i >= 0; i--) {
				alberatura.add(alberaturaNodi.get(i).getIdNodo());
			}

			final List<NodoOrganigrammaDTO> primoLivello = getPrimoLivelloProtocollatore();

			rootProtocollatore = new DefaultTreeNode();
			rootProtocollatore.setExpanded(true);
			rootProtocollatore.setSelectable(false);

			List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
			rootProtocollatore.setChildren(new ArrayList<>());
			DefaultTreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(utente.getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, rootProtocollatore);
				nodoToAdd.setExpanded(true);
				if (n.getIdUtente() == null && alberatura.contains(n.getIdNodo())) {
					nodiDaAprire.add(nodoToAdd);
				}
			}

			final List<DefaultTreeNode> nodiDaAprireTemp = new ArrayList<>();
			for (int i = 0; i < alberatura.size(); i++) {
				nodiDaAprireTemp.clear();
				for (final DefaultTreeNode nodo : nodiDaAprire) {
					onOpenTreeProt(nodo);
					nodo.setExpanded(true);
					for (final TreeNode nodoFiglio : nodo.getChildren()) {
						final NodoOrganigrammaDTO nodoFiglioDto = (NodoOrganigrammaDTO) nodoFiglio.getData();
						if (nodoFiglioDto.getIdUtente() == null && alberatura.contains(nodoFiglioDto.getIdNodo())) {
							nodiDaAprireTemp.add((DefaultTreeNode) nodoFiglio);
						}
					}
				}
				nodiDaAprire = new ArrayList<>(nodiDaAprireTemp);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());
		}
	}

	/**
	 * @return lista dei nodi di primo livello per l'alberatura del protocollatore
	 */
	private List<NodoOrganigrammaDTO> getPrimoLivelloProtocollatore() {
		final NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
		nodoRadice.setIdAOO(utente.getIdAoo());
		nodoRadice.setIdNodo(utente.getIdNodoRadiceAOO());
		nodoRadice.setCodiceAOO(utente.getCodiceAoo());
		nodoRadice.setUtentiVisible(true);
		List<NodoOrganigrammaDTO> primoLivello = null;
		primoLivello = organigrammaSRV.getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovoConUtenti(true, utente.getIdUfficio(), nodoRadice, null, false);
		return primoLivello;
	}

	/**
	 * Gestisce l'evento "Expand" dei nodi dell'albero delle assegnazioni.
	 * 
	 * @param event
	 */
	public void onOpenTreeAss(final NodeExpandEvent event) {
		onOpenTreeAss(event.getTreeNode());
	}

	/**
	 * Inizializza l'albero delle assegnazioni.
	 * 
	 * @param tree
	 */
	public void onOpenTreeAss(final TreeNode tree) {
		try {
			final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) tree.getData();
			List<NodoOrganigrammaDTO> figli = null;
			figli = organigrammaSRV.getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovoSenzaUtenti(true, utente.getIdUfficio(), nodoExp, false);

			DefaultTreeNode nodoToAdd = null;
			tree.getChildren().clear();
			for (final NodoOrganigrammaDTO n : figli) {
				n.setCodiceAOO(utente.getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, tree);
				if (n.getFigli() > 0 || n.isUtentiVisible()) {
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());
		}

	}

	/**
	 * Gestisce l'evento "Select" dei nodi dell'albero delle assegnazioni.
	 * 
	 * @param event
	 */
	public void onSelectTreeAss(final NodeSelectEvent event) {
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();
		try {
			final NodoOrganigrammaDTO select = (NodoOrganigrammaDTO) event.getTreeNode().getData();
			final String descrUfficio = select.getDescrizioneNodo();

			formContainer.getDocumentiUCB().setDescrUfficio(descrUfficio);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());
		}
	}

	/**
	 * Gestisce l'evento "Expand" dell'organigramma per la selezione del titolario.
	 * 
	 * @param event
	 */
	public void onTitolarioUCBNodeExpandRicerca(final NodeExpandEvent event) {
		try {
			final TitolarioDTO toOpen = (TitolarioDTO) event.getTreeNode().getData();
			final List<TitolarioDTO> list = titolarioSRV.getFigliByIndiceNonDisattivati(utente.getIdAoo(), toOpen.getIndiceClassificazione());
			event.getTreeNode().getChildren().clear();
			if (list != null && !list.isEmpty()) {
				DefaultTreeNode indiceToAdd = null;
				for (final TitolarioDTO t : list) {
					indiceToAdd = new DefaultTreeNode(t, event.getTreeNode());
					if (t.getNumeroFigli() > 0) {
						new DefaultTreeNode(null, indiceToAdd);
					}
				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());

		}
	}

	/**
	 * Gestisce l'evento "Select" dell'organigramma per la selezione del titolario.
	 * 
	 * @param event
	 */
	public void onTitolarioUCBNodeSelect(final NodeSelectEvent event) {
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();
		try {
			final TitolarioDTO titolarioSelected = (TitolarioDTO) event.getTreeNode().getData();
			formContainer.getDocumentiUCB().setDescrizioneTitolario(titolarioSelected.getDescrizione());
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());
		}
	}

	/**
	 * Gestisce l'evento "Select" dell'organigramma per la selezione del
	 * protocollatore.
	 * 
	 * @param event
	 */
	public void onSelectTreeProt(final NodeSelectEvent event) {
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();

		try {
			final NodoOrganigrammaDTO select = (NodoOrganigrammaDTO) event.getTreeNode().getData();
			String protocollatore = "";
			if (TipologiaNodoEnum.UTENTE.equals(select.getTipoNodo())) {
				protocollatore = select.getCognomeUtente() + " " + select.getNomeUtente();
			} else {
				protocollatore = select.getDescrizioneNodo();
			}

			formContainer.getDocumentiUCB().setProtocollatore(protocollatore);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());
		}
	}

	/**
	 * Gestisce l'evento "Select" dell'organigramma per la selezione
	 * dell'assegnatario.
	 * 
	 * @param event
	 */
	public void onSelectTreeUfficioAss(final NodeSelectEvent event) {
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();

		try {
			final NodoOrganigrammaDTO select = (NodoOrganigrammaDTO) event.getTreeNode().getData();
			final String descrUfficioAss = select.getDescrizioneNodo();

			formContainer.getDocumentiUCB().setDescrUfficioAss(descrUfficioAss);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());
		}
	}

	/**
	 * Gestisce l'evento "Select" dell'organigramma per la selezione del firmatario.
	 * 
	 * @param event
	 */
	public void onSelectTreeUfficioFirm(final NodeSelectEvent event) {
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();
		try {
			final NodoOrganigrammaDTO select = (NodoOrganigrammaDTO) event.getTreeNode().getData();
			final String descrUfficioFirm = select.getDescrizioneNodo();

			formContainer.getDocumentiUCB().setDescrUfficioFirm(descrUfficioFirm);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());
		}
	}

	/**
	 * Inizializza l'albero per la selezione del protocollatore.
	 * 
	 * @param tree
	 */
	public void onOpenTreeProt(final TreeNode tree) {
		try {
			final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) tree.getData();
			List<NodoOrganigrammaDTO> figli = null;
			figli = organigrammaSRV.getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovoConUtenti(false, utente.getIdUfficio(), nodoExp, null, false);

			DefaultTreeNode nodoToAdd = null;
			tree.getChildren().clear();
			for (final NodoOrganigrammaDTO n : figli) {
				n.setCodiceAOO(utente.getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, tree);
				nodoToAdd.setSelectable(false);

				if (n.getIdUtente() != null && n.getIdUtente() != 0) {
					nodoToAdd.setSelectable(true);
				} else {
					if (n.getFigli() > 0) {
						new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
					}

				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());
		}
	}

	/**
	 * Gestisce l'evento "Expand" dell'organigramma per la selezione del
	 * protocollatore.
	 * 
	 * @param event
	 */
	public void onOpenTreeProt(final NodeExpandEvent event) {
		onOpenTreeProt(event.getTreeNode());
	}

	/**
	 * Effettua un reset dei campi per la ricerca UCB.
	 */
	public void pulisciRicercaUCBDoc() {
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();
		formContainer.setDocumentiUCB(new ParamsRicercaAvanzataDocDTO());
	}

	/**
	 * Effettua un reset dei campi per la ricerca Assegnazioni UCB.
	 */
	public void pulisciRicercaAssegnazioneUCB() {
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();
		formContainer.setAssegnazioneUCB(new ParamsRicercaDocUCBAssegnanteAssegnatarioDTO());
		formContainer.getAssegnazioneUCB().setTipoRicerca(RicercaAssegnanteAssegnatarioEnum.values()[0]);
		assegnatarioOrgComponent.reset();
	}

	/**
	 * Inizializza il tab della ricerca assegnante/assegnatario UCB.
	 */
	private void initTabRicercaAssegnazioneUCB() {
		assegnatarioOrgComponent = new OrganigrammaCompleteComponent(utente, true, false);
	}
	
	/**
	 * Pulisce i campi del tab della ricerca estratti conto trimestrati CCVT UCB.
	 */
	public void pulisciRicercaEstrattiContoUCB() {
		final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();
		formContainer.setEstrattiContoUCB(new ParamsRicercaEstrattiContoDTO());
	}
	
	/**
	 * Pulisce i campi del tab della ricerca elenco notifiche e conti consuntivi UCB.
	 */
	public void pulisciRicercaContiConsuntiviUCB() {
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();
		formContainer.setContiConsuntiviUCB(new ParamsRicercaContiConsuntiviDTO());
	}

	/**
	 * Restituisce l'index del tab view attivo.
	 * 
	 * @return index tabView
	 */
	public String getActiveIndexTabView() {
		return activeIndexTabView;
	}

	/**
	 * Imposta l'index del tabView attivo.
	 * 
	 * @param activeIndexTabView
	 */
	public void setActiveIndexTabView(final String activeIndexTabView) {
		this.activeIndexTabView = activeIndexTabView;
	}

	/**
	 * Restituisce il component per la gestione della dialog.
	 * 
	 * @return il component
	 */
	public DialogRendererComponent getDialogRenderer() {
		return dialogRenderer;
	}

	/**
	 * Imposta il component per la gestione della dialog.
	 * 
	 * @param dialogRenderer
	 */
	public void setDialogRenderer(final DialogRendererComponent dialogRenderer) {
		this.dialogRenderer = dialogRenderer;
	}

	/**
	 * Restituisce il navigation token della pagina attiva.
	 * 
	 * @return pagina attiva
	 */
	public NavigationTokenEnum getActivePageInfo() {
		return activePageInfo;
	}

	/**
	 * Imposta il navigation token della pagina attiva.
	 * 
	 * @param activePageInfo
	 */
	public void setActivePageInfo(final NavigationTokenEnum activePageInfo) {
		this.activePageInfo = activePageInfo;
	}

	/**
	 * Restituisce il form della ricerca avanzata UCB.
	 * 
	 * @return form ricerca
	 */
	public RicercaAvanzataDocUcbFormDTO getFormDocumenti() {
		return formDocumenti;
	}

	/**
	 * Imposta il form della ricerca avanzata UCB.
	 * 
	 * @param formDocumenti
	 */
	public void setFormDocumenti(final RicercaAvanzataDocUcbFormDTO formDocumenti) {
		this.formDocumenti = formDocumenti;
	}

	/**
	 * Restituisce l'alberatura dei nodi.
	 * 
	 * @return nodi alberatura
	 */
	public List<Nodo> getAlberaturaNodi() {
		return alberaturaNodi;
	}

	/**
	 * Imposta l'alberatura dei nodi.
	 * 
	 * @param alberaturaNodi
	 */
	public void setAlberaturaNodi(final List<Nodo> alberaturaNodi) {
		this.alberaturaNodi = alberaturaNodi;
	}

	/**
	 * Restituisce la root dell'albero delle assegnazione.
	 * 
	 * @return root assegnazioni
	 */
	public DefaultTreeNode getRootAssegnazione() {
		return rootAssegnazione;
	}

	/**
	 * Imposta la root dell'albero delle assegnazione.
	 * 
	 * @param rootAssegnazione
	 */
	public void setRootAssegnazione(final DefaultTreeNode rootAssegnazione) {
		this.rootAssegnazione = rootAssegnazione;
	}

	/**
	 * Restituisce la root dell'albero dei protocollatori.
	 * 
	 * @return root protocollatori
	 */
	public DefaultTreeNode getRootProtocollatore() {
		return rootProtocollatore;
	}

	/**
	 * Imposta la root dell'albero delle assegnazione.
	 * 
	 * @param rootProtocollatore
	 */
	public void setRootProtocollatore(final DefaultTreeNode rootProtocollatore) {
		this.rootProtocollatore = rootProtocollatore;
	}

	/**
	 * Restituisce il component per la gestione dell'organigramma delle
	 * assegnazioni.
	 * 
	 * @return assegnatarioOrgComponent
	 */
	public IAssOrganigrammaComponent getAssegnatarioOrgComponent() {
		return assegnatarioOrgComponent;
	}

	/**
	 * Imposta il component per la gestione dell'organigramma delle assegnazioni.
	 * 
	 * @param assegnatarioOrgComponent
	 */
	public void setAssegnatarioOrgComponent(final IAssOrganigrammaComponent assegnatarioOrgComponent) {
		this.assegnatarioOrgComponent = assegnatarioOrgComponent;
	}

	/**
	 * Esegue la ricerca degli estratti conto ucb.
	 * @return null se si verifica un errore, "" altrimenti
	 */
	public String searchEstrattiContoUCB() {
		try {
			sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
//			<-- Validazione -->
			final String msg = isFormEstrattiContoValid();
			if (msg != null) {
				showWarnMessage(msg);
				return null;
			}
//			<-- Ricerca Business -->
			final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();
			final Collection<EstrattiContoUcbDTO> risultatiEstrattiContoUCB = ricercaSRV.eseguiRicercaEstrattiContoUCB(formContainer.getEstrattiContoUCB(), sessionBean.getUtente());
//			<-- Gestione risultati-->
			if (risultatiEstrattiContoUCB.isEmpty()) {
				showInfoMessage(NO_DOC_FOUND_MSG);
				return null;
			} else {
				final StatoRicercaDTO stDto = new StatoRicercaDTO(formContainer.getEstrattiContoUCB(), risultatiEstrattiContoUCB);
				FacesHelper.putObjectInSession(SessionObject.RISULTATO_RICERCA_ESTRATTI_CONTO, stDto);
				FacesHelper.executeJS(PF_STATUS_DIALOG_SHOW);
			}
//			<-- Clear Form-->
			pulisciRicercaEstrattiContoUCB();
//			<-- Navigazione -->
			return sessionBean.gotoRicercaEstrattiContoUCB();
		} catch (final Exception e) {
			LOGGER.error(ERRORE_IN_FASE_DI_RICERCA_AVANZATA_DEI_DOCUMENTI, e);
			showError(e);
		}
		return "";
	}

	/**
	 * Restituisce null se i parametri superano la validazione, l'errore riscontrato altrimenti.
	 * @return null se i parametri superano la validazione, l'errore riscontrato altrimenti
	 */
	private String isFormEstrattiContoValid() {
		String out = null;
		final ParamsRicercaEstrattiContoDTO paramsRicercaEstrattiConto = sessionBean.getRicercaFormContainer().getEstrattiContoUCB();
		if(paramsRicercaEstrattiConto.getEsercizio() == null) {
			out = "Valorizzare i parametri obbligatori.";
		}
		return out;
	}
	
	/**
	 * Esegue la ricerca per elenco notifiche e conti consuntivi UCB.
	 * 
	 * @return url per la visualizzazione dei risultati della ricerca
	 */
	public String searchContiConsuntiviUCB() {
		try {
			sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
//			<-- Validazione -->
			final String msg = isFormContiConsuntiviValid();
			if (msg != null) {
				showWarnMessage(msg);
				return null;
			}
//			<-- Ricerca Business -->
			final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();
			final List<RisultatoRicercaContiConsuntiviDTO> risultatiContiConsuntiviUCB = ricercaSRV.eseguiRicercaContiConsuntiviUCB(formContainer.getContiConsuntiviUCB(), sessionBean.getUtente());
//			<-- Gestione risultati-->
			if (risultatiContiConsuntiviUCB.isEmpty()) {
				showInfoMessage(NO_DOC_FOUND_MSG);
				return null;
			} else {
				final StatoRicercaDTO stDto = new StatoRicercaDTO(formContainer.getContiConsuntiviUCB(), risultatiContiConsuntiviUCB);
				FacesHelper.putObjectInSession(SessionObject.RISULTATO_RICERCA_CONTI_CONSUNTIVI, stDto);
				FacesHelper.executeJS(PF_STATUS_DIALOG_SHOW);
			}
//			<-- Clear Form-->
			pulisciRicercaContiConsuntiviUCB();
//			<-- Navigazione -->
			return sessionBean.gotoRicercaContiConsuntiviUCB();
		} catch (final Exception e) {
			LOGGER.error(ERRORE_IN_FASE_DI_RICERCA_AVANZATA_DEI_DOCUMENTI, e);
			showError(e);
		}
		return "";
	}
	
	/**
	 * Esegue la validazione dei parametri del form di ricerca.
	 * 
	 * @return null se non ci sono errori riscontrati nella validazione, il messaggio di errore altrimenti
	 */
	private String isFormContiConsuntiviValid() {
		String out = null;
		final ParamsRicercaContiConsuntiviDTO paramsRicercaContiConsuntivi = sessionBean.getRicercaFormContainer().getContiConsuntiviUCB();
		if(paramsRicercaContiConsuntivi.getEsercizio() == null) {
			out = "Valorizzare i parametri obbligatori.";
		}
		return out;
	}

	@Override
	public void onOpenTree(TreeNode nodo) {
		throw new NotImplementedException("Metodo non implementato");
	}
}
