package it.ibm.red.web.helper.dtable;

import java.io.Serializable;
import java.util.Collection;


/**
 * The Class SimpleDetailDataTableHelper.
 *
 * @author CPIERASC
 * 
 *         Helper per gestire i datatable (Primefaces) con una gestione base del
 *         passaggio da master a detail (semplicemente il dettaglio ).
 * @param <M>
 *            il tipo della struttura del master
 */
public class SimpleDetailDataTableHelper<M extends Serializable> extends DataTableHelper<M, M> {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Costruttore.
	 */
	public SimpleDetailDataTableHelper() {
		super();
	}

	/**
	 * Costruttore.
	 * 
	 * @param inMasters	i master.
	 */
	public SimpleDetailDataTableHelper(final Collection<M> inMasters) {
		super(inMasters);
	}
	
	/**
	 * Metodo che imposta il dettaglio a partire dal master selezionato,
	 * l'implementazione prevede semplicemente la sovrascrittura del dettaglio
	 * col valore del master.
	 *
	 * @param master
	 *            the master
	 * @return il dettaglio
	 */
	@Override
	protected final M setDetailFromCurrent(final M master) {
		setRenderDetail(true);
		return master;
	}

}
