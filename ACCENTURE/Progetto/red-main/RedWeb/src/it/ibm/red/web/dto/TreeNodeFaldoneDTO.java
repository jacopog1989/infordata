package it.ibm.red.web.dto;

/**
 * DTO che definisce un tree node di un faldone.
 */
public class TreeNodeFaldoneDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -7880835036566529155L;

	/**
	 * Document title.
	 */
	private final String documentTitle;

	/**
	 * Nome del padre del nodo.
	 */
	private final String parentName;

	/**
	 * Nome del nodo.
	 */
	private final String name;

	/**
	 * Identificativo dell'ufficio a cui è associato il faldone.
	 */
	private final Integer idUfficio;

	/**
	 * path assoluto del faldone, utile per recuperare i suoi fascicoli.
	 */
	private final String absolutePathFaldone;

	/**
	 * Descrizione Faldone.
	 */
	private String descrizione;
	
	/**
	 * Costruttore.
	 * @param name
	 */
    public TreeNodeFaldoneDTO(final String name) {
    	this(null, name, null, null, null, null);
    }

    /**
     * Costruttore.
     * @param parentName
     * @param name
     * @param descrizione
     * @param idUfficio
     * @param absPathFaldone
     * @param documentTitle
     */
    public TreeNodeFaldoneDTO(final String parentName, final String name, final String descrizione, final Integer idUfficio, final String absPathFaldone, final String documentTitle) {
		super();
		this.documentTitle = documentTitle;
		this.parentName = parentName;
		this.name = name;
		this.idUfficio = idUfficio;
		this.descrizione = descrizione;
		absolutePathFaldone = absPathFaldone;
	}
    
    /**
	 * Restituisce il document title.
	 * @return document title
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta la descrizione.
	 * @param descrizione
	 */
    public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}

    /**
     * Restituisce la descrizione.
     * @return descrizione
     */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Restituisce il path assoluto del faldone.
	 * @return path assoluto del faldone
	 */
	public String getAbsolutePathFaldone() {
		return absolutePathFaldone;
	}

    /**
     * Restituisce l'id dell'ufficio.
     * @return id dell'ufficio
     */
	public Integer getIdUfficio() {
		return idUfficio;
	}
	
	/**
	 * Restituisce il nome.
	 * @return nome
	 */
	public String getName() {
		return name;
	}

	/**
	 * Restituisce il parent name.
	 * @return parent name
	 */
	public String getParentName() {
		return parentName;
	}
}
