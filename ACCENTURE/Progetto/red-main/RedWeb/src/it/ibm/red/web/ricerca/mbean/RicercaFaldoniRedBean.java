/**
 * 
 */
package it.ibm.red.web.ricerca.mbean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;

import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataFaldDTO;
import it.ibm.red.business.dto.StatoRicercaDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IPropertiesFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.DettaglioFaldoneRicercaBean;
import it.ibm.red.web.mbean.SessionBean;

/**
 * @author APerquoti
 *
 */
@Named(ConstantsWeb.MBean.RICERCA_FALDONI_RED_BEAN)
@ViewScoped
public class RicercaFaldoniRedBean extends AbstractBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 8450780492929211161L;
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaFaldoniRedBean.class.getName());
	
	/**
	 * Bean Session.
	 */
	private SessionBean sessionBean;
	
	/**
	 * Faldoni.
	 */
	protected SimpleDetailDataTableHelper<FaldoneDTO> faldoniRedDTH;
	
	/**
	 * Dettaglio faldone.
	 */
	private DettaglioFaldoneRicercaBean dettaglioFaldone;
	
	/**
	 * Parametri ricerca.
	 */
	private ParamsRicercaAvanzataFaldDTO formRicerca;
	
	/**
	 * Colonne.
	 */
	private List<Boolean> faldColumns;
	
	/**
	 * Numero massimo risultati.
	 */
	private Integer numMaxRisultati;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		
		faldoniRedDTH = new SimpleDetailDataTableHelper<>();
		faldoniRedDTH.setMasters(new ArrayList<>());
		
		dettaglioFaldone = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_FALDONE_RICERCA_BEAN);
		
		faldColumns = Arrays.asList(true, true, true, true);
		
		final IPropertiesFacadeSRV propertiesSRV = ApplicationContextProvider.getApplicationContext().getBean(IPropertiesFacadeSRV.class);
		numMaxRisultati = Integer.parseInt(propertiesSRV.getByEnum(PropertiesNameEnum.RICERCA_MAX_RESULTS));
		
		initPaginaRicerca();
	}

	/**
	 * Inizializza la pagina di ricerca.
	 */
	@SuppressWarnings("unchecked")
	public void initPaginaRicerca() {
		try {
			final StatoRicercaDTO stDto = (StatoRicercaDTO) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.STATO_RICERCA, true);
			if (stDto != null) {
				final Collection<FaldoneDTO> searchResult = (Collection<FaldoneDTO>) stDto.getRisultatiRicerca();
				formRicerca = (ParamsRicercaAvanzataFaldDTO) stDto.getFormRicerca();
				
				faldoniRedDTH.setMasters(searchResult);
				faldoniRedDTH.selectFirst(true);
				
				setDetail(faldoniRedDTH.getCurrentMaster());
				
				if (numMaxRisultati != null && searchResult != null && numMaxRisultati.equals(searchResult.size())) {
					showWarnMessage(RicercaAbstractBean.MSG_NUM_MAX_RISULTATI);
				}
			} else {
				formRicerca = new ParamsRicercaAvanzataFaldDTO();
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Non è stato possibile recuperare i risultati di ricerca");
		}
	}

	/**
	 * Gestisce la selezione della riga dal datatable dei faldoni.
	 * @param se
	 */
	public final void rowSelectorFaldoni(final SelectEvent se) {
		try {			
			faldoniRedDTH.rowSelector(se);
			setDetail(faldoniRedDTH.getCurrentMaster());
		} catch (final Exception e) {
			LOGGER.error("Errore nella selezione di una riga nei risultati della ricerca faldoni", e);
			showError(e);
		}
	}
	
	/**
	 * Inizializza il dettaglio del faldone.
	 * @param fascicolo
	 */
	private void setDetail(final FaldoneDTO faldone) {
		if (faldone == null) {
			throw new RedException("Non risulta selezionato alcun fascicolo");
		}
		dettaglioFaldone.setDetail(faldone);
	}
	
	/**
	 * Metodo per reimpostare nel session bean il form utilizzato per la ricerca e quindi visualizzarlo nel dialog.
	 */
	public void ripetiRicercaRedFaldoni() {
		try {
			// Viene ripetuta la ricerca con i stessi filtri
			sessionBean.getRicercaFormContainer().setFaldoniRed(formRicerca);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Gestisce la selezione e la deselezione della colonna.
	 * @param event
	 */
	public final void onColumnToggleFaldoni(final ToggleEvent event) {
		faldColumns.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
	}
	
//	<-- get/set -->

	/**
	 * @return the faldoniRedDTH
	 */
	public final SimpleDetailDataTableHelper<FaldoneDTO> getFaldoniRedDTH() {
		return faldoniRedDTH;
	}

	/**
	 * @param faldoniRedDTH the faldoniRedDTH to set
	 */
	public final void setFaldoniRedDTH(final SimpleDetailDataTableHelper<FaldoneDTO> faldoniRedDTH) {
		this.faldoniRedDTH = faldoniRedDTH;
	}

	/**
	 * @return the faldColumns
	 */
	public final List<Boolean> getFaldColumns() {
		return faldColumns;
	}

	/**
	 * @param faldColumns the faldColumns to set
	 */
	public final void setFaldColumns(final List<Boolean> faldColumns) {
		this.faldColumns = faldColumns;
	}

	/**
	 * Restituisce il nome del file per l'export dalla coda.
	 * @return nome file export
	 */
	public String getNomeFileExportCoda() {
		
		String nome = "";
		if (sessionBean.getUtente() != null) {
			nome += sessionBean.getUtente().getNome().toLowerCase() + "_" + sessionBean.getUtente().getCognome().toLowerCase() + "_";
		}
		
		nome += "ricercafaldoni_";
		
		final SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH.mm");
		nome += sdf.format(new Date()); 
		
		return nome;
	}

}
