package it.ibm.red.web.mbean.component;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentManagerDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.web.document.mbean.component.DocumentManagerDataIndiceDiClassificazioneComponent;

/**
 * 
 * @author m.crescentini
 *
 */
public class DocumentManagerCercaFascicoloComponent extends CercaFascicoloComponent {

	private static final long serialVersionUID = -335623691900394218L;

	/**
	 * Dettaglio.
	 */
	private DetailDocumentRedDTO detail;

	/**
	 * Document manager.
	 */
	private DocumentManagerDTO documentManager;

	/**
	 * Componente ricerca fascicolo.
	 */
	private RicercaFascicoloDaDocumentoComponent tabRicercaDoc;

	/**
	 * Bean chiamante.
	 */
	private final String mBeanChiamante;

	/**
	 * Costruttore del component.
	 * 
	 * @param mBeanChiamante
	 * @param utente
	 */
	public DocumentManagerCercaFascicoloComponent(final String mBeanChiamante, final UtenteDTO utente) {
		super(mBeanChiamante, utente);
		this.mBeanChiamante = mBeanChiamante;
		this.tabRicercaDoc = new RicercaFascicoloDaDocumentoComponent(utente, mBeanChiamante);
	}

	/**
	 * Costruttore.
	 * 
	 * @param mBeanChiamante
	 * @param utente
	 * @param detail
	 * @param documentManager
	 */
	public DocumentManagerCercaFascicoloComponent(final String mBeanChiamante, final UtenteDTO utente, final DetailDocumentRedDTO detail,
			final DocumentManagerDTO documentManager) {
		super(mBeanChiamante, utente);
		this.detail = detail;
		this.documentManager = documentManager;
		this.mBeanChiamante = mBeanChiamante;
		this.indiceDiClassificazione = new DocumentManagerDataIndiceDiClassificazioneComponent(detail, utente, documentManager);
		this.tabRicercaDoc = new RicercaFascicoloDaDocumentoComponent(utente, mBeanChiamante);
	}

	/**
	 * Resetta i campi della ricerca del fascicolo.
	 */
	@Override
	public void resetRicerca() {
		super.resetRicerca();
		this.indiceDiClassificazione = new DocumentManagerDataIndiceDiClassificazioneComponent(detail, utente, documentManager);
		this.tabRicercaDoc = new RicercaFascicoloDaDocumentoComponent(utente, mBeanChiamante);
	}

	/**
	 * @param detail the detail to set
	 */
	public void setDetail(final DetailDocumentRedDTO detail) {
		this.detail = detail;
	}

	/**
	 * @param documentManager the documentManager to set
	 */
	public void setDocumentManager(final DocumentManagerDTO documentManager) {
		this.documentManager = documentManager;
	}

	/**
	 * Restituisce il component che gestisce la ricerca di fascicoli.
	 * 
	 * @return il component
	 */
	@Override
	public RicercaFascicoloDaDocumentoComponent getTabRicercaDoc() {
		return tabRicercaDoc;
	}

	/**
	 * Imposta il component che gestisce la ricerca di fascicoli.
	 * 
	 * @param tabRicercaDoc
	 */
	@Override
	public void setTabRicercaDoc(final RicercaFascicoloDaDocumentoComponent tabRicercaDoc) {
		this.tabRicercaDoc = tabRicercaDoc;
	}

}
