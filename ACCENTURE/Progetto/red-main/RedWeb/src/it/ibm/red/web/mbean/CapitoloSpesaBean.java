package it.ibm.red.web.mbean;


import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;

import it.ibm.red.business.dto.CapitoloSpesaDTO;
import it.ibm.red.business.dto.CapitoloSpesaMetadatoDTO;
import it.ibm.red.business.dto.RicercaCapitoloSpesaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.ICapitoloSpesaSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Bean per la gestione capitoli spesa.
 */
@Named(ConstantsWeb.MBean.CAPITOLO_SPESA_BEAN)
@ViewScoped
public class CapitoloSpesaBean extends AbstractBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -2350793126659667433L;
	
	/**
	 * Servizio capitolo.
	 */
	private ICapitoloSpesaSRV capitoloSRV;
	
	/**
	 * DTO per la ricerca.
	 */
	private RicercaCapitoloSpesaDTO ricercaDTO;
	
	/**
	 * Datatable capitoli.
	 */
	private SimpleDetailDataTableHelper<CapitoloSpesaDTO> dth;
	
	/**
	 * Infomrazione utente.
	 */
	private UtenteDTO utente;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		SessionBean sessionBean = (SessionBean) FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		capitoloSRV = ApplicationContextProvider.getApplicationContext().getBean(ICapitoloSpesaSRV.class);
		resetPage();
	}

	private void resetPage() {
		ricercaDTO = new RicercaCapitoloSpesaDTO(utente.getIdAoo());
		dth = null;
	}

	/**
	 * Esegue la ricerca utilizzando il dto di ricerca popolato tramite form.
	 */
	public void ricerca() {
		Collection<CapitoloSpesaDTO> masters = capitoloSRV.ricerca(ricercaDTO);
		dth = new SimpleDetailDataTableHelper<>(masters);
	}

	/**
	 * Gestisce la selezione di una riga dal datatable.
	 * @param se
	 */
	public final void rowSelector(final SelectEvent se) {
		dth.rowSelector(se);
		
	    FacesContext context = FacesContext.getCurrentInstance();
	    CapitoloSpesaMetadatoDTO capitoloDTO = context.getApplication().evaluateExpressionGet(context, "#{cc.attrs.capDto}", CapitoloSpesaMetadatoDTO.class);
	    capitoloDTO.setCapitoloSelected(dth.getCurrentMaster().getCodice());

	    resetPage();
	}

	/**
	 * Esegue il reset della selezione.
	 */
	public final void resetSelection() {
	    FacesContext context = FacesContext.getCurrentInstance();
	    CapitoloSpesaMetadatoDTO capitoloDTO = context.getApplication().evaluateExpressionGet(context, "#{cc.attrs.capDto}", CapitoloSpesaMetadatoDTO.class);
	    capitoloDTO.setCapitoloSelected(null);
	}

	/**
	 * Esegue tutte le azioni da svolgere in chiusura della dialog.
	 */
	public void close() {
		resetPage();
	}

	/**
	 * Restituisce il dto di ricerca.
	 * @return ricercaDTO
	 */
	public RicercaCapitoloSpesaDTO getRicercaDTO() {
		return ricercaDTO;
	}

	/**
	 * Restituisce il datatable helper associato al datatable dei capitoli di spesa.
	 * @return dth
	 */
	public SimpleDetailDataTableHelper<CapitoloSpesaDTO> getDth() {
		return dth;
	}

}