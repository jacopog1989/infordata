/**
 * 
 */
package it.ibm.red.web.ricerca.mbean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;

import it.ibm.red.business.dto.DetailFascicoloRedDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataFascDTO;
import it.ibm.red.business.dto.StatoRicercaDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.DettaglioFascicoloRicercaBean;
import it.ibm.red.web.mbean.interfaces.IUpdatableDetailCaller;

/**
 * @author APerquoti
 *
 */
@Named(ConstantsWeb.MBean.RICERCA_FASCICOLI_RED_BEAN)
@ViewScoped
public class RicercaFascicoliRedBean extends RicercaAbstractBean<FascicoloDTO> implements IUpdatableDetailCaller<DetailFascicoloRedDTO> {

	private static final long serialVersionUID = 6502649784448420684L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaFascicoliRedBean.class.getName());
		
	/**
	 * Datatable fascicoli.
	 */
	protected SimpleDetailDataTableHelper<FascicoloDTO> fascicoliRedDTH;

	/**
	 * Parametri ricerca.
	 */
	private ParamsRicercaAvanzataFascDTO formRicerca;
	
	/**
	 * Colonne.
	 */
	private List<Boolean> fascColumns;
		
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		super.postConstruct();
		
		fascicoliRedDTH = new SimpleDetailDataTableHelper<>();
		fascicoliRedDTH.setMasters(new ArrayList<>());
		
		fascColumns = Arrays.asList(true, true, true, true);
		
		initPaginaRicerca();
	}
	
	/**
	 * Inizializza la pagina di ricerca fascicoli.
	 */
	@SuppressWarnings("unchecked")
	public void initPaginaRicerca() {
		try {
			final StatoRicercaDTO stDto = (StatoRicercaDTO) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.STATO_RICERCA, true);
			if (stDto != null) {
				final Collection<FascicoloDTO> searchResult = (Collection<FascicoloDTO>) stDto.getRisultatiRicerca();
				formRicerca = (ParamsRicercaAvanzataFascDTO) stDto.getFormRicerca();
				
				fascicoliRedDTH.setMasters(searchResult);
				fascicoliRedDTH.selectFirst(true);
				
				selectDetail(fascicoliRedDTH.getCurrentMaster());
				
				if (getNumMaxRisultati() != null && searchResult != null && getNumMaxRisultati().equals(searchResult.size())) {
					showWarnMessage(RicercaAbstractBean.MSG_NUM_MAX_RISULTATI);
				}
			} else {
				formRicerca = new ParamsRicercaAvanzataFascDTO();
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Non è stato possibile recuperare i risultati di ricerca");
		}
	}

	/**
	 * Gestisce la selezione della riga del datatable aggiornando il dettaglio con il fascicolo selezionato.
	 * @param se
	 */
	public final void rowSelectorFascicoli(final SelectEvent se) {
		fascicoliRedDTH.rowSelector(se);
		selectDetail(fascicoliRedDTH.getCurrentMaster());
	}
	
	/**
	 * Gestisce la selezione del dettaglio.
	 * @param f fascicolo selezionato
	 */
	@Override
	protected void selectDetail(final FascicoloDTO f) {
		final DettaglioFascicoloRicercaBean bean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN);
		if (f == null) {
			bean.unsetDetail();
			return;
		}
		bean.setDetail(f);
		bean.setCaller(this);
	}
	
	/**
	 * Restituisce il fascicolo selezionato.
	 * @return master fascicolo selezionato
	 */
	@Override
	public FascicoloDTO getSelectedDocument() {
		return fascicoliRedDTH.getCurrentMaster();
	}
	
	/**
	 * Metodo per reimpostare nel session bean il form utilizzato per la ricerca e quindi visualizzarlo nel dialog
	 */
	public void ripetiRicercaRedFascicoli() {
		try {
			// Viene ripetuta la ricerca con i stessi filtri
			sessionBean.getRicercaFormContainer().setFascicoliRed(formRicerca);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Gestisce la selezione e la deselezione delle colonne associate ai fascicoli.
	 * Nello specifico si occupa della colonna identificata dall'evento Toggle.
	 * @param event
	 */
	public final void onColumnToggleFascicoli(final ToggleEvent event) {
		fascColumns.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
	}
	

	/**
	 * @return the fascicoliRedDTH
	 */
	public final SimpleDetailDataTableHelper<FascicoloDTO> getFascicoliRedDTH() {
		return fascicoliRedDTH;
	}

	/**
	 * @param fascicoliRedDTH the fascicoliRedDTH to set
	 */
	public final void setFascicoliRedDTH(final SimpleDetailDataTableHelper<FascicoloDTO> fascicoliRedDTH) {
		this.fascicoliRedDTH = fascicoliRedDTH;
	}

	/**
	 * Effettua l'aggiornamento del dettaglio.
	 * @param detail
	 */
	@Override
	public void updateDetail(final DetailFascicoloRedDTO detail) {
		if (fascicoliRedDTH != null && fascicoliRedDTH.getCurrentMaster() != null) {
			final FascicoloDTO f = fascicoliRedDTH.getCurrentMaster();
			if (f.getIdFascicolo().equals(detail.getNomeFascicolo())) {
				f.setIndiceClassificazione(detail.getTitolarioDTO().getIndiceClassificazione());
			}
		}
	}

	/**
	 * @return the fascColumns
	 */
	public final List<Boolean> getFascColumns() {
		return fascColumns;
	}

	/**
	 * @param fascColumns the fascColumns to set
	 */
	public final void setFascColumns(final List<Boolean> fascColumns) {
		this.fascColumns = fascColumns;
	}

	/**
	 * @see it.ibm.red.web.ricerca.mbean.RicercaAbstractBean#getNomeRicerca4Excel().
	 */
	@Override
	protected String getNomeRicerca4Excel() {
		return "ricercafascicoli_";
	}

}
