package it.ibm.red.web.servlets;

import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;

/**
 * Servlet implementation class ModificaContentAppletServlet.
 */
public class HealthStatusServlet extends HttpServlet {

	/**
	 * La costante serialVersionUID.
	 */
	
	private static final long serialVersionUID = 1L;
	
    /**
     * Logger.
     */
	private static final REDLogger LOGGER = REDLogger.getLogger(HealthStatusServlet.class.getName());
 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HealthStatusServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response) {
		try (PrintWriter writer = response.getWriter()) {
			final IUtenteFacadeSRV utenteSRV = ApplicationContextProvider.getApplicationContext().getBean(IUtenteFacadeSRV.class);

			// Allocazione memoria è dinamica
			final Long freeMem = Runtime.getRuntime().freeMemory(); // Meoria disponibile
			final Long totMem = Runtime.getRuntime().totalMemory(); // Memoria allocata
			final Long maxMem = Runtime.getRuntime().maxMemory(); // Massima allocabile

			final String ip = getIpAddress();

			writer.println("NODO <" + ip + ">");
			writer.println("MEMORIA MASSIMA ALLOCABILE: " + maxMem);
			writer.println("MEMORIA ALLOCATA: " + totMem);
			writer.println("MEMORIA DISPONIBILE: " + freeMem);
			writer.println("SESSIONI APERTE: " + utenteSRV.getSessioniAperte());
			writer.println("SESSIONI APERTE PER UTENTE: " + utenteSRV.getSessioniApertePerUtente());
			writer.println("SESSIONI APERTE PER NODO: " + utenteSRV.getSessioniApertePerNodo());

		} catch (final Exception e) {
			LOGGER.error("Errore nell'aggiornamento dello stato di salute dell'applicativo. ", e);
		}
	}

	/**
	 * @return indirizzo IP
	 */
	private static String getIpAddress() {
		String ip = "";
		try {
			final InetAddress localhost = InetAddress.getLocalHost();
			ip = localhost.toString();
		} catch (final UnknownHostException e) {
			LOGGER.warn("Errore nel recupero dell'indirizzo locale della macchina: ", e);
		}
		return ip;
	}

}
