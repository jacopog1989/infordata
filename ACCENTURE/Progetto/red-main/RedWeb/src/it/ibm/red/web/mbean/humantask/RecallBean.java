package it.ibm.red.web.mbean.humantask;
 
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.StatoLavorazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV;
import it.ibm.red.business.service.facade.IOperationDocumentFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * @author VIngenito
 *
 */
@Named(ConstantsWeb.MBean.RECALL_BEAN)
@ViewScoped
public class RecallBean extends AbstractBean implements InitHumanTaskInterface {
 
	private static final long serialVersionUID = 2543963593046348453L;

    /**
     * Logger.
     */
	private static final REDLogger LOGGER = REDLogger.getLogger(RecallBean.class.getName());
	
    /**
     * Lista documenti.
     */
	private List<MasterDocumentRedDTO> masters;
	
    /**
     * Lista documenti bean.
     */
	private ListaDocumentiBean ldb;
	
    /**
     * Flag motivo recall obbligatorio.
     */
	private boolean motivoRecallObbligatorio;
	
	/**
	 * Motivo recall.
	 */
	private String motivoRecall;
  
	
	@Override
	protected void postConstruct() { 
		//non fare niente
	}

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		masters = inDocsSelezionati;
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
		
		if (ResponsesRedEnum.RECALL.equals(ldb.getSelectedResponse())) {
			motivoRecallObbligatorio = true;
		}
	}
	
	/**
	 * Esegue la funzionalita di Recall del documenti selezionati: {@link #masters}.
	 */
	public void recallResponse() {
		try {
			if (motivoRecallObbligatorio && StringUtils.isNullOrEmpty(motivoRecall)) {
				showError("La motivazione della response richiama è obbligatoria");
			} else {
				final MasterDocumentRedDTO m = masters.get(0); 
				final IListaDocumentiFacadeSRV listaDocumentiSRV = ApplicationContextProvider.getApplicationContext().getBean(IListaDocumentiFacadeSRV.class);
				final IOperationDocumentFacadeSRV opeDocumentSRV = ApplicationContextProvider.getApplicationContext().getBean(IOperationDocumentFacadeSRV.class);
				final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
				final UtenteDTO utente = sessionBean.getUtente();
				
				final String documentTitle = m.getDocumentTitle();
					//controllo che l'id stato lavorazione del document title sia quello della coda in approvazione
					if (listaDocumentiSRV.checkDocumentTitleRecall(documentTitle, utente, StatoLavorazioneEnum.RICHIAMA_DOCUMENTI_STATO.getId())) {
						//Prendo Workflow principale
						final String wobNumberRecall = listaDocumentiSRV.getWorkflowPrincipale(documentTitle, utente);
						if (wobNumberRecall != null) {
							// Si puliscono gli esiti
							ldb.cleanEsiti();
							final EsitoOperazioneDTO eOpe = opeDocumentSRV.recall(utente, wobNumberRecall, motivoRecall);
			 			
 							// Si impostano i nuovi esiti
							ldb.getEsitiOperazione().add(eOpe);
			   				
							FacesHelper.executeJS("PF('dlgGeneric').hide()");
							FacesHelper.update("centralSectionForm:idDlgShowResult");
							FacesHelper.executeJS("PF('dlgShowResult').show()");
							ldb.destroyBeanViewScope(ConstantsWeb.MBean.RECALL_BEAN);
						} else {
							setErrorRecallResponse(wobNumberRecall); 
						}
					} else { 
						setErrorRecallResponse(documentTitle);
					}
			 }
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'operazione di recall", e);
			showError("Si è verificato un errore durante l'operazione di recall");
		}
	 
	}
	
	/**
	 * Restituisce true se la motivazione di Recall è obbligatoria, false altrimenti.
	 * @return true se la motivazione di Recall è obbligatoria, false altrimenti
	 */
	public boolean getMotivoRecallObbligatorio() {
		return motivoRecallObbligatorio;
	}
	
	/**
	 * Restituisce la motivazione della Recall.
	 * @return motivazione Recall
	 */
	public String getMotivoRecall() {
		return motivoRecall;
	}
	
	/**
	 * Imposta la motivazione della Recall.
	 * @param motivoRecall
	 */
	public void setMotivoRecall(final String motivoRecall) {
		this.motivoRecall = motivoRecall;
	}

	/**
	 * Imposta esito di errore per il documento identificato dal documentTitle.
	 * @param documentTitle
	 */
	public void setErrorRecallResponse(final String documentTitle) {
		final EsitoOperazioneDTO esitoOp = new EsitoOperazioneDTO(documentTitle);
		esitoOp.setEsito(false);
		esitoOp.setNote("Documento non disponibile per l'operazione di recall");
		ldb.getEsitiOperazione().add(esitoOp); 
	}
	
	 
}
