package it.ibm.red.web.factory;

import java.util.ArrayList;
import java.util.Collection;

import it.ibm.red.business.dto.AbstractDTO;
import it.ibm.red.web.enums.HomepageEnum;

/**
 * A factory for creating Homepage objects.
 *
 * @author CPIERASC
 * 
 *         Factory homepage.
 */
public final class HomepageFactory extends AbstractDTO {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Collezione delle possibili homepage.
	 */
	private Collection<HomepageEnum> homepages;

	/**
	 * Costruttore.
	 */
	private HomepageFactory() {
		homepages = new ArrayList<>();
		homepages.add(HomepageEnum.HOMEPAGE);
	}
}
