package it.ibm.red.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.enums.NavigationTokenEnum;

/**
 * Filtro request.
 */
public class RequestFilter implements Filter {

	/**
	 * Method.
	 */
	private static final String METHOD = "GET";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RequestFilter.class.getName());

	/**
	 * Intercetta le chiamate alla @see {@link DownloadContentServlet} lasciando
	 * passare solo chiamate di tipo Get.
	 * 
	 * @param req
	 * @param res
	 * @param chain
	 * @throws IOException, ServletException
	 */
	@Override
	public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain) throws IOException, ServletException {
		try {
			final HttpServletRequest httpRequest = (HttpServletRequest) req;
			if (!httpRequest.getMethod().equalsIgnoreCase(METHOD)) {
				final HttpServletResponse httpResponse = (HttpServletResponse) res;
				httpResponse.sendRedirect(httpRequest.getContextPath() + "/views/genericError.xhtml");
				return;
			}
			if (httpRequest.getMethod().equalsIgnoreCase(METHOD)) {
				final NavigationTokenEnum libroFirmaObj = (NavigationTokenEnum) httpRequest.getSession().getAttribute("LIBRO_FIRMA_OBJ");
				if (NavigationTokenEnum.LIBROFIRMA.equals(libroFirmaObj)) {
					final HttpServletResponse httpResponse = (HttpServletResponse) res;
					httpResponse.setHeader("LibroFirma", "LibroFirma");
					httpRequest.getSession().removeAttribute("LIBRO_FIRMA_OBJ");
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
		}
		chain.doFilter(req, res);
	}

	/**
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig).
	 */
	@Override
	public void init(final FilterConfig config) throws ServletException {
		LOGGER.info("Init filtro");
	}

	/**
	 * @see javax.servlet.Filter#destroy().
	 */
	@Override
	public void destroy() {
		LOGGER.info("Destroy filtro");
	}

}
