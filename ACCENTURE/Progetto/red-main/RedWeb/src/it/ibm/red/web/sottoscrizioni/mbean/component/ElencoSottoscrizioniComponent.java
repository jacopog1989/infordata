package it.ibm.red.web.sottoscrizioni.mbean.component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;

import it.ibm.red.business.dto.CanaleTrasmissioneDTO;
import it.ibm.red.business.dto.EventoSottoscrizioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ISottoscrizioniFacadeSRV;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.mbean.AbstractBean;

/**
 * Component che gestisce l'elenco sottoscrizioni.
 */
public class ElencoSottoscrizioniComponent extends AbstractBean {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 6587682646357093267L;
	
	/**
	 * Canali di trasmissione.
	 */
	private Collection<CanaleTrasmissioneDTO> canaliTrasmissione;
	
	/**
	 * Servizio.
	 */
	private final ISottoscrizioniFacadeSRV sottoscrizioniSRV;
	
	/**
	 * Lista eventi.
	 */
	private List<EventoSottoscrizioneDTO> eventiDTO;
	
	/**
	 * Datatable sottoscrizioni.
	 */
	private SimpleDetailDataTableHelper<EventoSottoscrizioneDTO> eventiSottoscrizioneDTH;
	
	/**
	 * Utente.
	 */
	private UtenteDTO utente;
	
	/**
	 * Eventi selezionati.
	 */
	private List<EventoSottoscrizioneDTO> selectedEvents;
	
	/**
	 * Costruttore del componente.
	 * @param utente
	 */
	public ElencoSottoscrizioniComponent(final UtenteDTO utente) {
		sottoscrizioniSRV = ApplicationContextProvider.getApplicationContext().getBean(ISottoscrizioniFacadeSRV.class);
		eventiDTO = new ArrayList<>();
		this.setUtente(utente);
	}
	
	/**
	 * Inizializza il componente gestendo i service.
	 */
	public void initialize() {
		selectedEvents = sottoscrizioniSRV.getEventiSottoscritti(utente);
		canaliTrasmissione = sottoscrizioniSRV.loadCanaliTrasmissione();
		eventiDTO = sottoscrizioniSRV.getEventiByAoo(utente.getIdAoo().intValue());
		eventiSottoscrizioneDTH = new SimpleDetailDataTableHelper<>(eventiDTO);
		eventiSottoscrizioneDTH.setSelectedMasters(selectedEvents);
		
		for (final EventoSottoscrizioneDTO evento : eventiDTO) {
			for (final EventoSottoscrizioneDTO eventoSel : selectedEvents) {
				if (evento.equals(eventoSel) && Boolean.TRUE.equals(eventoSel.getSelected())) {
					evento.setSelected(true);
					evento.setCanaleTrasmissione(eventoSel.getCanaleTrasmissione());
					evento.setTracciamento(eventoSel.getTracciamento());
					break;
				}
			}
		}
		areAllChecked();
	}
	
	/**
	 * Registra le sottoscrizioni.
	 * @return messaggio di errore nel caso di errore altrimenti stringa vuota
	 */
	public String registra() {
		String msg = "";
		//Imposto il canale e il tracciamento se è stato modificato
		for (final EventoSottoscrizioneDTO eventoSel : selectedEvents) {
			for (final EventoSottoscrizioneDTO evento : eventiDTO) {
				if (eventoSel.equals(evento) && Boolean.TRUE.equals(evento.getSelected())) {
					eventoSel.setCanaleTrasmissione(evento.getCanaleTrasmissione());
					eventoSel.setTracciamento(evento.getTracciamento());
					break;
				}
			}
		}
		if (!sottoscrizioniSRV.registraSottoscrizione(utente, selectedEvents)) {
			msg = "Errore durante il salvataggio delle sottoscrizioni. ";
		}
		return msg;
	}

	/**
	 * Gestisce la selezione singola e multipla della tabella.
	 */
	public void handleAllCheckBoxEventiSottoscrizioni() {
		handleAllCheckBox(eventiSottoscrizioneDTH);
	}
	
	/**
	 * Aggiorna gli eventi delle sottoscrizioni.
	 * @param event evento della sottoscrizione
	 */
	public void updateCheckEventiSottoscrizioni(final AjaxBehaviorEvent event) {
		updateCheck(event, eventiSottoscrizioneDTH);
		areAllChecked();
	}
	
	private void handleAllCheckBox(final SimpleDetailDataTableHelper<EventoSottoscrizioneDTO> dth) {
		super.handleAllCheckBoxGeneric(dth);
	}
	
	private EventoSottoscrizioneDTO updateCheck(final AjaxBehaviorEvent event, final SimpleDetailDataTableHelper<EventoSottoscrizioneDTO> dth) {
		final EventoSottoscrizioneDTO masterChecked = getDataTableClickedRow(event);
		final Boolean newValue = ((SelectBooleanCheckbox) event.getSource()).isSelected();
		if (newValue != null && newValue) {
			dth.getSelectedMasters().add(masterChecked);
		} else {
			dth.getSelectedMasters().remove(masterChecked);
		}
		
		return masterChecked;
	}
	
	/**
	 * metodo per gestire il comportamento del check seleziona tutto della tabella.
	 */
	private void areAllChecked() {
		if (eventiDTO.size() == eventiSottoscrizioneDTH.getSelectedMasters().size()) {
			handleAllCheckBoxEventiSottoscrizioni();
		} else {
			eventiSottoscrizioneDTH.setCheckAll(false);
		}
	}
	/******************** Getter e setter **************************************/
	
	/**
	 * Recupera i canali di trasmissione.
	 * @return canali di trasmissione
	 */
	public Collection<CanaleTrasmissioneDTO> getCanaliTrasmissione() {
		return canaliTrasmissione;
	}

	/**
	 * Recupera gli eventi.
	 * @return lista degli eventi
	 */
	public List<EventoSottoscrizioneDTO> getEventiDTO() {
		return eventiDTO;
	}

	/**
	 * Recupera gli eventi della sottoscrizione.
	 * @return eventi della sottoscrizione
	 */
	public SimpleDetailDataTableHelper<EventoSottoscrizioneDTO> getEventiSottoscrizioneDTH() {
		return eventiSottoscrizioneDTH;
	}

	/**
	 * Imposta i canali di trasmissione.
	 * @param canaliTrasmissione canali di trasmissione
	 */
	public void setCanaliTrasmissione(final Collection<CanaleTrasmissioneDTO> canaliTrasmissione) {
		this.canaliTrasmissione = canaliTrasmissione;
	}

	/**
	 * Imposta gli eventi.
	 * @param eventiDTO lista degli eventi
	 */
	public void setEventiDTO(final List<EventoSottoscrizioneDTO> eventiDTO) {
		this.eventiDTO = eventiDTO;
	}

	/**
	 * Imposta gli eventi della sottoscrizione.
	 * @param eventiSottoscrizioneDTH eventi della sottoscrizione
	 */
	public void setEventiSottoscrizioneDTH(final SimpleDetailDataTableHelper<EventoSottoscrizioneDTO> eventiSottoscrizioneDTH) {
		this.eventiSottoscrizioneDTH = eventiSottoscrizioneDTH;
	}

	/**
	 * Recupera l'utente.
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'utente.
	 * @param utente utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	@Override
	protected void postConstruct() {
		// Non occorre fare niente in questo metodo.
	}
}
