/**
 * 
 */
package it.ibm.red.web.mbean;

import it.ibm.red.business.dto.AbstractDTO;

/**
 * @author APerquoti
 *
 */
public class ColumnModelDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -481625260332429128L;
	
	
	/**
	 * Header.
	 */
	private final String header;
	
	/**
	 * Valore.
	 */
	private final String value;
	
	/**
	 * Tipo di dato.
	 */
	private final String tipoDato;

	/**
	 * Costruttore di classe.
	 * @param inHeader
	 * @param inValue
	 * @param inTipoDato
	 */
	public ColumnModelDTO(final String inHeader, final String inValue, final String inTipoDato) {
		this.header = inHeader;
		this.value = inValue;
		this.tipoDato = inTipoDato;
	}

	/**
	 * @return the header
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @return the tipoDato
	 */
	public String getTipoDato() {
		return tipoDato;
	}

	
}
