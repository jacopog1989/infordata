/**
 * 
 */
package it.ibm.red.web.ricerca.mbean;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.dto.ParamsRicercaRegistrazioniAusiliarieDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaNPSDTO;
import it.ibm.red.business.dto.RicercaRegistrazioniAusiliarieFormDTO;
import it.ibm.red.business.dto.StatoRicercaDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.INpsFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaAvanzataDocFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.dto.RicercaFormContainer;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;

/**
 * Bean per la maschera di ricerca delle registrazioni ausiliarie.
 * 
 * @author m.crescentini
 *
 */
public class RicercaRegistrazioniAusiliarieBean extends AbstractBean {

	private static final long serialVersionUID = -5035209986321063513L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaRegistrazioniAusiliarieBean.class.getName());
	
	/**
	 * Bean sessione.
	 */
	private SessionBean sessionBean;
	
	/**
	 * Parametri ricerca.
	 */
	private RicercaRegistrazioniAusiliarieFormDTO form;

	/**
	 * Costruttore.
	 */
	public RicercaRegistrazioniAusiliarieBean(final Long idAoo) {
		super();
		
		initForm(idAoo);
	}

	@Override
	protected void postConstruct() {
		// Non deve fare nulla
	}
	
	/**
	 * Inizializzazione del form.
	 * 
	 * @param idAoo
	 */
	private void initForm(final Long idAoo) {
		final IRicercaAvanzataDocFacadeSRV ricercaAvanzataSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaAvanzataDocFacadeSRV.class);
		
		form = new RicercaRegistrazioniAusiliarieFormDTO();
		form.setTipologieDocumento(ricercaAvanzataSRV.getListaTipologieDocumento(idAoo));
		
		if (!CollectionUtils.isEmpty(form.getTipologieDocumento())) {
			// A partire dalle tipologie documento estratte, si caricano i registri ausiliari da mostrare nella maschera di ricerca
			form.setRegistri(ricercaAvanzataSRV.getRegistriAusiliariByTipologieDocumento(
					form.getTipologieDocumento().stream().map(TipologiaDocumentoDTO::getIdTipologiaDocumento).filter(Objects::nonNull).collect(Collectors.toList())));
		}
	}

	/**
	 * Esecuzione della ricerca.
	 * 
	 * @return
	 */
	public final String ricerca() {
		try {
			sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
			final ParamsRicercaRegistrazioniAusiliarieDTO formParamsRicerca = sessionBean.getRicercaFormContainer().getRegistrazioniAusiliarie();
			
			final String msg = isFormOk(formParamsRicerca);
			if (msg != null) {
				showWarnMessage(msg);
				return null;
			}
			
			final INpsFacadeSRV npsSRV = ApplicationContextProvider.getApplicationContext().getBean(INpsFacadeSRV.class);
			final List<RegistrazioneAusiliariaNPSDTO> risultatiRicercaRegistrazioniAusiliarie = 
					npsSRV.ricercaRegistrazioniAusiliarie(formParamsRicerca, sessionBean.getUtente());
			
//			<-- Gestione risultati-->
			if (!manageResult(risultatiRicercaRegistrazioniAusiliarie, formParamsRicerca)) {
				return null;
			}
			
//			<-- Clear Form-->
			sessionBean.pulisciFormRicerca();
			
//			<-- Navigazione -->
			return sessionBean.gotoRicercaRegistrazioniAusiliarie();
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di ricerca delle registrazioni ausiliarie", e);
			showError("Errore in fase di ricerca delle registrazioni auisiliarie");
		}
		
		return "";
	}
	
	/**
	 * Gestione della selezione delle date.
	 * 
	 * @param fieldName
	 */
	public void onChangeRicercaRegAusiliarieDateDaA(final String fieldName) {
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		final ParamsRicercaRegistrazioniAusiliarieDTO formParamsRicerca = sessionBean.getRicercaFormContainer().getRegistrazioniAusiliarie();

		if ("dataRegistrazioneDa".equals(fieldName)) {
			if (formParamsRicerca.getDataRegistrazioneDa() != null 
					&& (formParamsRicerca.getDataRegistrazioneA() == null 
						|| formParamsRicerca.getDataRegistrazioneA().before(formParamsRicerca.getDataRegistrazioneDa()))) {
				formParamsRicerca.setDataRegistrazioneA(formParamsRicerca.getDataRegistrazioneDa());
			}
		} else if ("dataRegistrazioneA".equals(fieldName)) {
			if (formParamsRicerca.getDataRegistrazioneA() != null 
					&& (formParamsRicerca.getDataRegistrazioneDa() == null
						|| formParamsRicerca.getDataRegistrazioneDa().after(formParamsRicerca.getDataRegistrazioneA()))) {
				formParamsRicerca.setDataRegistrazioneDa(formParamsRicerca.getDataRegistrazioneA());
			}
		} else if ("dataAnnullamentoDa".equals(fieldName)) {
			if (formParamsRicerca.getDataAnnullamentoDa() != null 
					&& (formParamsRicerca.getDataAnnullamentoA() == null
						|| formParamsRicerca.getDataAnnullamentoA().before(formParamsRicerca.getDataAnnullamentoDa()))) {
				formParamsRicerca.setDataAnnullamentoA(formParamsRicerca.getDataAnnullamentoDa());
				formParamsRicerca.setAnnullato(true);
			}
		} else if ("dataAnnullamentoA".equals(fieldName) 
				&& (formParamsRicerca.getDataAnnullamentoA() != null
					&& (formParamsRicerca.getDataAnnullamentoDa() == null
						|| formParamsRicerca.getDataAnnullamentoDa().after(formParamsRicerca.getDataAnnullamentoA())))) {
			formParamsRicerca.setDataAnnullamentoDa(formParamsRicerca.getDataAnnullamentoA());
			formParamsRicerca.setAnnullato(true);
		}  
	}
	
	private <T> boolean manageResult(final List<RegistrazioneAusiliariaNPSDTO> result, final T formRicerca) {
		boolean isOk = false;
		
		if (CollectionUtils.isEmpty(result)) {
			showInfoMessage("Nessun documento trovato.");
		} else {
			final StatoRicercaDTO stDto = new StatoRicercaDTO(formRicerca, result);
			FacesHelper.putObjectInSession(ConstantsWeb.SessionObject.STATO_RICERCA, stDto);
			FacesHelper.executeJS("PF('statusDialog').show();");
			isOk = true;
		}
		
		return isOk;
	}
	
	/**
	 * Validazione del form di ricerca.
	 * 
	 * @param paramsRegistrazioniAusiliarie
	 * @return
	 */
	private String isFormOk(final ParamsRicercaRegistrazioniAusiliarieDTO paramsRegistrazioniAusiliarie) {
		String out = null;
		
		if (paramsRegistrazioniAusiliarie.getAnno() == null) {
			out = "Inserire l'anno di creazione";
		} else if (StringUtils.isBlank(paramsRegistrazioniAusiliarie.getCodiceRegistro())) {
			out = "Selezionare il registro ausiliario";
		} else if ((!DateUtils.isRangeOk(paramsRegistrazioniAusiliarie.getDataRegistrazioneDa(), paramsRegistrazioniAusiliarie.getDataRegistrazioneA()))
				|| (!DateUtils.isRangeOk(paramsRegistrazioniAusiliarie.getDataAnnullamentoDa(), paramsRegistrazioniAusiliarie.getDataAnnullamentoA()))) {
			out = "Inserire un intervallo di date valido";
		} else if ((!DateUtils.isSameYearRange(paramsRegistrazioniAusiliarie.getDataRegistrazioneDa(), paramsRegistrazioniAusiliarie.getDataRegistrazioneA(), false))
				|| (!DateUtils.isSameYearRange(paramsRegistrazioniAusiliarie.getDataAnnullamentoDa(), paramsRegistrazioniAusiliarie.getDataAnnullamentoA(), false))) {
			out = "L'intervallo di date non può estendersi su due anni";
		} else if (paramsRegistrazioniAusiliarie.getNumeroRegistrazioneDa() != null && paramsRegistrazioniAusiliarie.getNumeroRegistrazioneA() != null
				&& (paramsRegistrazioniAusiliarie.getNumeroRegistrazioneA() < paramsRegistrazioniAusiliarie.getNumeroRegistrazioneDa())) {
			out = "Il numero registrazione 'DA' non può essere maggiore del numero registrazione 'A'";
		}

		return out;
	}
	
	/**
	 * Metodo per reimpostare la ricerca delle registrazioni ausiliarie.
	 */
	public void pulisciRicerca() {
		final RicercaFormContainer formContainer = sessionBean.getRicercaFormContainer();
		formContainer.setRegistrazioniAusiliarie(new ParamsRicercaRegistrazioniAusiliarieDTO());
	}

	/**
	 * @return the form
	 */
	public RicercaRegistrazioniAusiliarieFormDTO getForm() {
		return form;
	}
}
