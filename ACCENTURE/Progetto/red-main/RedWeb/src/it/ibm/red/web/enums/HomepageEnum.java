package it.ibm.red.web.enums;

import it.ibm.red.business.enums.AccessoFunzionalitaEnum;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Enum HomepageEnum.
 *
 * @author CPIERASC
 * 
 *         Tipologie di codifica.
 */
public enum HomepageEnum {
	
	/**
	 * Pagina vuota.
	 */
	HOMEPAGE("Homepage", "homepage", NavigationTokenEnum.HOMEPAGE, null),

	/**
	 * Valore.
	 */
	DASHBOARD("Dashboard", "dashboard", NavigationTokenEnum.DASHBOARD, AccessoFunzionalitaEnum.DASHBOARD),

	/**
	 * Valore.
	 */
	LIBROFIRMA("Libro Firma", "libro", NavigationTokenEnum.LIBROFIRMA, AccessoFunzionalitaEnum.LIBROFIRMA),

	/**
	 * Valore.
	 */
	DA_LAVORARE("Da Lavorare", "dalavorare", NavigationTokenEnum.DA_LAVORARE, AccessoFunzionalitaEnum.DALAVORARE),

	/**
	 * Valore.
	 */
	CORRIERE("Corriere", "corriere", NavigationTokenEnum.CORRIERE, AccessoFunzionalitaEnum.CORRIERE),

	/**
	 * Valore.
	 */
	MAIL("Posta Elettronica", "postaelettronica", NavigationTokenEnum.SCRIVANIA, AccessoFunzionalitaEnum.MAIL);

	/**
	 * Descrizione.
	 */
	private String label;
	
	/**
	 * Valore.
	 */
	private String value;
	
	/**
	 * NavigationToken.
	 */
	private NavigationTokenEnum nte;
	
	
    /**
     * Accesso.
     */
	private AccessoFunzionalitaEnum accFunz;
	
	/**
	 * Costruttore.
	 * 
	 * @param inLabel	descrizione
	 * @param inValue	valore
	 */
	HomepageEnum(final String inLabel, final String inValue, final NavigationTokenEnum inNte, final AccessoFunzionalitaEnum inaccfunz) {
		label = inLabel;
		value = inValue;
		nte = inNte;
		setAccFunz(inaccfunz);
	}

	/**
	 * Getter label.
	 * 
	 * @return	label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Getter value.
	 * 
	 * @return	value
	 */
	public String getValue() {
		return value;
	}
	
	 /**
	 * @return the nte
	 */
	public final NavigationTokenEnum getNte() {
		return nte;
	}

	/**
	 * Ottiene il valore dell'enum.
	 * @param homePageEnumValue
	 * @return valore dell'enum o valore di default
	 */
	public static HomepageEnum get(final String homePageEnumValue) {
		 HomepageEnum output = HOMEPAGE;
		 for (final HomepageEnum val:HomepageEnum.values()) {
			 if (!StringUtils.isNullOrEmpty(val.getValue()) && val.getValue().equalsIgnoreCase(homePageEnumValue)) {
				 output = val;
				 break;
			 }
		 }
		 return output;
	 }

	/**
	 * Recupera l'attributo accFunz.
	 * @return accFunz
	 */
	public AccessoFunzionalitaEnum getAccFunz() {
		return accFunz;
	}

	/**
	 * Imposta l'attributo accFunz.
	 * @param accFunz
	 */
	protected void setAccFunz(final AccessoFunzionalitaEnum accFunz) {
		this.accFunz = accFunz;
	}

}