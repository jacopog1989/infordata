package it.ibm.red.web.report.mbean.component;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.RisultatoRicercaElencoDivisionaleDTO;

/**
 * Component che gestisce i risultati del report dell'elenco divisionale.
 */
public class RisultatiElencoDivisionaleReportComponent implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Lista dei dettagli da visualizzare.
	 */
	private List<RisultatoRicercaElencoDivisionaleDTO> detailList;
	
    /**
     * Descrizione operazione.
     */
	private String descrizioneOperazione;
	
	/**
	 * Intestazione tabella.
	 */
	private StringBuilder detailTableHeader;

	/**
     * Aoo.
     */
	private Long idAoo;

	/**
	 * Costruttore del component.
	 * 
	 * @param idAoo
	 */
	public RisultatiElencoDivisionaleReportComponent(final Long idAoo) {
		this.idAoo = idAoo;
	}

	/**
	 * Restituisce la lista dei dettagli report.
	 * 
	 * @return lista dettagli report
	 */
	public List<RisultatoRicercaElencoDivisionaleDTO> getDetailList() {
		return detailList;
	}
	
	/**
	 * Imposta la lista dei dettagli report.
	 * 
	 * @param detailList
	 */
	public void setDetailList(final List<RisultatoRicercaElencoDivisionaleDTO> detailList) {
		this.detailList = detailList;
	}
	
	/**
	 * Restituisce la descrizione dell'operazione.
	 * 
	 * @return descrizione operazione
	 */
	public String getDescrizioneOperazione() {
		return descrizioneOperazione;
	}
	
	/**
	 * Imposta la descrizione dell'operazione.
	 * 
	 * @param descrizioneOperazione
	 */
	public void setDescrizioneOperazione(final String descrizioneOperazione) {
		this.descrizioneOperazione = descrizioneOperazione;
	}
	
	/**
	 * Restituisce l'intestazione della tabella.
	 * 
	 * @return intestazione tabella
	 */
	public StringBuilder getDetailTableHeader() {
		return detailTableHeader;
	}

	/**
	 * Imposta l'intestazione della tabella.
	 * 
	 * @param detailTableHeader
	 */
	public void setDetailTableHeader(StringBuilder detailTableHeader) {
		this.detailTableHeader = detailTableHeader;
	}

	/**
	 * Restituisce l'id dell'Area Organizzativa Omogenea.
	 * 
	 * @return id AOO
	 */
	public Long getIdAoo() {
		return idAoo;
	}

	/**
	 * Imposta l'id dell'Area Organizzativa Omogenea.
	 * 
	 * @param idAoo
	 */
	public void setIdAoo(final Long idAoo) {
		this.idAoo = idAoo;
	}
	
}
