package it.ibm.red.web.enums;

import it.ibm.red.business.enums.ResponsesRedEnum;

/**
 * Quando l'utente clicca su una azione relativa a un email il sistema ha la
 * necessità di ricordare quale azione è stata selezionata.
 *
 * Qualora sia necessario aggiungere l'azione.
 */
public enum GruppiResponseEnum {

	// vanno ordinate nell'ordine nel quale devono essere eseguite

	/**
	 * Valore.
	 */
	FIRMA(ResponsesRedEnum.FIRMA_REMOTA, ResponsesRedEnum.FIRMA_DIGITALE),

	/**
	 * Valore.
	 */
	SIGLA(ResponsesRedEnum.SIGLA_E_INVIA),

	/**
	 * Valore.
	 */
	VISTO(ResponsesRedEnum.VISTO),

	/**
	 * Valore.
	 */
	FIRMA_MULTIPLA(ResponsesRedEnum.FIRMA_REMOTA_MULTIPLA, ResponsesRedEnum.FIRMA_DIGITALE_MULTIPLA);

	/**
	 * Response FNET.
	 */
	private ResponsesRedEnum[] reponses;

	GruppiResponseEnum(final ResponsesRedEnum... inReponses) {
		reponses = inReponses;
	}

	/**
	 * Restituisce true se almeno una response della lista in ingresso è compresa
	 * nel gruppo.
	 * 
	 * @param responses
	 * @param gruppo
	 * @return true, se compatibili; false, altrimenti.
	 */
	public static Boolean isCompatible(final String[] responses, final GruppiResponseEnum gruppo) {
		Boolean output = false;
		for (final ResponsesRedEnum r : gruppo.reponses) {
			for (final String rIn : responses) {
				if (rIn.equalsIgnoreCase(r.getResponse())) {
					output = true;
					break;
				}
			}
		}
		return output;
	}

	/**
	 * Restituisce tutte le response relative ad un gruppo
	 * 
	 * @return response
	 */
	public ResponsesRedEnum[] getReponses() {
		return reponses;
	}
}
