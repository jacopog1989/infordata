package it.ibm.red.web.mbean.widgets;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.slf4j.LoggerFactory;

import com.sun.management.OperatingSystemMXBean;

import ch.qos.logback.classic.Level;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.HttpUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean; 

/**
 * Bean che gestisce i tool admin.
 */
@Named(ConstantsWeb.MBean.ADMIN_TOOL_BEAN)
@ViewScoped
public class AdminToolBean extends AbstractBean {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 8526942448608234409L;
 
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AdminToolBean.class.getName());
	
	/**
	 * Nodo attuale.
	 */
	private String nodoAttuale;
	
	/**
	 * Identificativo di sessione.
	 */
	private String sessionID;
	  
	/**
	 * Dimensione memoria allocata.
	 */
	private String memoriaAllocata;

	/**
	 * Dimensione memoria in uso.
	 */
	private String memoriaAllocataInUso;

	/**
	 * Dimensione memoria totale.
	 */
	private String memoriaTotaleInUso;

	/**
	 * Tempo ultimo refresh dati.
	 */
	private Date ultimoRefresh;

	/**
	 * Root.
	 */
	private String livelloRoot;
	
	/**
	 * Track id.
	 */
	private String trackId; 

	/**
	 * Percentuale CPU.
	 */
	private String cpuUsata;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {  
		ultimoRefresh = PropertiesProvider.getIstance().getLastRefreshTime();
		nodoAttuale = HttpUtils.getNodo();
		sessionID = FacesHelper.getSessionId();
		calcolaMemoria(); 
		livelloRoot = ((ch.qos.logback.classic.Logger) LoggerFactory.getLogger("root")).getLevel().toString();
	}

	/**
	 * Imposta parametri relativi all'utilizzo della memoria.
	 */
	public void calcolaMemoria() { 
		final long maxMemory = Runtime.getRuntime().maxMemory();
		final long totalMemory = Runtime.getRuntime().totalMemory();
		final long freeMemory = Runtime.getRuntime().freeMemory();
		final long usedMemory = totalMemory - freeMemory; 
		
		memoriaAllocata = getPerc(totalMemory, maxMemory);
		memoriaAllocataInUso = getPerc(usedMemory, totalMemory);
		memoriaTotaleInUso = getPerc(usedMemory, maxMemory); 
		
		final OperatingSystemMXBean operatingSystemMXBean = (com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
		cpuUsata = String.valueOf(operatingSystemMXBean.getProcessCpuTime()) + "ns";
	}
	
	/**
	 * Libera la memoria e reimposta i parametri relativi all'utilizzo della stessa.
	 */
	public void freeMemory() { 
		calcolaMemoria();
	}

	/**
	 * Imposta il LevelRoot.
	 * @param level
	 */
	public void setLevelRoot(final String level) {
		final Level logLevel = getLevel(level);
		((ch.qos.logback.classic.Logger) LoggerFactory.getLogger("root")).setLevel(logLevel);
		livelloRoot = logLevel.toString();
	}

	/**
	 * Esegue il refresh delle property.
	 */
	public void runRefreshPropertyServlet() {
		PropertiesProvider.getIstance().refreshProperties();
		ultimoRefresh = PropertiesProvider.getIstance().getLastRefreshTime();
	}

	/**
	 * Esegue il refresh del livelloRoot.
	 */
	public void refreshLivelloRoot() {
		final Level level = ((ch.qos.logback.classic.Logger) LoggerFactory.getLogger("root")).getLevel();
		livelloRoot = level.toString();
	}
	
	private static String getPerc(final Long value, final Long total) {
		final DecimalFormat df = new DecimalFormat("0.00");
		df.setRoundingMode(RoundingMode.UP);
		double output = 0D;
		if (value != null && value != 0 && total != null && total != 0) {
			output = 100 * value.doubleValue() / total.doubleValue();
		}
		return df.format(output) + "%";
	}

	/**
	 * Aggiorna l'attributo ultimoRefresh con l'effettiva data ultima di refresh.
	 */
	public void ultimoRefresh() {
		ultimoRefresh = PropertiesProvider.getIstance().getLastRefreshTime();
	}

	/**
	 * Restituisce il nodo attuale.
	 * @return nodoAttuale
	 */
	public String getNodoAttuale() {
		return nodoAttuale;
	}

	/**
	 * Imposta il nodo attuale.
	 * @param nodoAttuale
	 */
	public void setNodoAttuale(final String nodoAttuale) {
		this.nodoAttuale = nodoAttuale;
	}

	/**
	 * Restituisce l'id della sessione corrente.
	 * @return session ID
	 */
	public String recuperoSessionId() {
		return FacesHelper.getSessionId(); 
	} 

	/**
	 * Invalida la sessione.
	 */
	public void invalidaSessione() {
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("../InvalidaSessioneServlet");
		} catch (final IOException e) {
			LOGGER.error("Errore nell'invalidare la sessione " + e); 
		}
	}

	/**
	 * Restituisce l'id della sessione corrente.
	 * @return sessionID
	 */
	public String getSessionID() {
		return sessionID;
	}

	/**
	 * Imposta l'id della session.
	 * @param sessionID
	 */
	public void setSessionID(final String sessionID) {
		this.sessionID = sessionID;
	}

	/**
	 * Restituisce la memoria allocata.
	 * @return memoriaAllocata
	 */
	public String getMemoriaAllocata() {
		return memoriaAllocata;
	}

	/**
	 * Imposta la memoria allocata.
	 * @param memoriaAllocata
	 */
	public void setMemoriaAllocata(final String memoriaAllocata) {
		this.memoriaAllocata = memoriaAllocata;
	}

	/**
	 * Restituisce la memoria allocata in uso.
	 * @return memoriaAllocataInUso
	 */
	public String getMemoriaAllocataInUso() {
		return memoriaAllocataInUso;
	}

	/**
	 * Imposta la memoria allocata in uso.
	 * @param memoriaAllocataInUso
	 */
	public void setMemoriaAllocataInUso(final String memoriaAllocataInUso) {
		this.memoriaAllocataInUso = memoriaAllocataInUso;
	}

	/**
	 * Restituisce la memoria totale in uso.
	 * @return memoriaTotaleInUso
	 */
	public String getMemoriaTotaleInUso() {
		return memoriaTotaleInUso;
	}

	/**
	 * Imposta la memoria totale in uso.
	 * @param memoriaTotaleInUso
	 */
	public void setMemoriaTotaleInUso(final String memoriaTotaleInUso) {
		this.memoriaTotaleInUso = memoriaTotaleInUso;
	}

	/**
	 * Restituisce la data di ultimo refresh.
	 * @return Date data ultimo refresh
	 */
	public Date getUltimoRefresh() {
		return ultimoRefresh;
	}

	/**
	 * Imposta la data di ultimo refresh.
	 * @param ultimoRefresh
	 */
	public void setUltimoRefresh(final Date ultimoRefresh) {
		this.ultimoRefresh = ultimoRefresh;
	}

	private Level getLevel(final String level) {
		Level output = Level.ALL;
		if ("DEBUG".equalsIgnoreCase(level)) {
			output = Level.DEBUG;
		} else if ("ERROR".equalsIgnoreCase(level)) {
			output = Level.ERROR;
		} else if ("INFO".equalsIgnoreCase(level)) {
			output = Level.INFO;
		} else if ("OFF".equalsIgnoreCase(level)) {
			output = Level.OFF;
		} else if ("TRACE".equalsIgnoreCase(level)) {
			output = Level.TRACE;
		} else if ("WARN".equalsIgnoreCase(level)) {
			output = Level.WARN;
		}
		return output;
	}

	/**
	 * Restituisce il livelloRoot.
	 * @return livelloRoot
	 */
	public String getLivelloRoot() {
		return livelloRoot;
	}

	/**
	 * Imposta il livelloRoot.
	 * @param livelloRoot
	 */
	public void setLivelloRoot(final String livelloRoot) {
		this.livelloRoot = livelloRoot;
	}

	/**
	 * Genera un random UUID e lo imposta come trackId.
	 */
	public void generaTrackId() {
		trackId = UUID.randomUUID().toString();
		FacesHelper.putObjectInSession("TRACK_ID", trackId); 
	}

	/**
	 * Restituisce il track Id.
	 * @return il trackId
	 */
	public String getTrackId() {
		return trackId;
	}

	/**
	 * Imposta il trackId.
	 * @param trackId
	 */
	public void setTrackId(final String trackId) {
		this.trackId = trackId;
	}
	
	/**
	 * Avvio calcolo CPU.
	 */
	public void calcoloCpu() {
		LOGGER.info("Test");
	}

	/**
	 * Restituisce l'utilizzo della CPU.
	 * @return cpuUsata
	 */
	public String getCpuUsata() {
		return cpuUsata;
	}

	/**
	 * Imposta il valore di cpuUsata.
	 * @param cpuUsata
	 */
	public void setCpuUsata(final String cpuUsata) {
		this.cpuUsata = cpuUsata;
	}
 }
