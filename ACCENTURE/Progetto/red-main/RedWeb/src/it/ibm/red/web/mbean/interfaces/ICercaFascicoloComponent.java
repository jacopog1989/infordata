package it.ibm.red.web.mbean.interfaces;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;

/**
 * Interfaccia del component per la ricerca e selezione di un fascicolo.
 * 
 * @author m.crescentini
 *
 */
public interface ICercaFascicoloComponent {
	
	/**
	 * Ricerca il fascicolo.
	 */
	void cercaFascicolo();
	
	/**
	 * Seleziona il fascicolo.
	 * @param index
	 */
	void selectFascicolo(Integer index);
	
	/**
	 * Seleziona il fascicolo del titolario.
	 * @param event evento
	 */
	void selectFascicoloDelTitolario(SelectEvent event);
	
	/**
	 * Seleziona il fascicolo del documento.
	 * @param event evento del nodo
	 */
	void selectFascicoloDelDocumento(NodeSelectEvent event);
	
	/**
	 * Resetta la ricerca.
	 */
	void resetRicerca(); 
	
	/**
	 * Ricerca il fascicolo.
	 * @param event evento
	 */
	void selectFascicolo(SelectEvent event);
}
