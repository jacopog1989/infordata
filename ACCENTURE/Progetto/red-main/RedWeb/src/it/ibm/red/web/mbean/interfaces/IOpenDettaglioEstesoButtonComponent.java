package it.ibm.red.web.mbean.interfaces;

/**
 * Interfaccia del component che gestisce il button di apertura del dettaglio esteso.
 */
public interface IOpenDettaglioEstesoButtonComponent {

	/**
	 * Predispone l'apertura del dettaglio esteso
	 */
	void openDocumentPopup();

	/**
	 * Mostra il dettaglio dopo il lock.
	 */
	void showDettaglioAfterLock();

	/**
	 * Verifica la firma degli allegati del documento principale.
	 */
	void verificaFirmaDocPrincipaleAllegati();
	
}
