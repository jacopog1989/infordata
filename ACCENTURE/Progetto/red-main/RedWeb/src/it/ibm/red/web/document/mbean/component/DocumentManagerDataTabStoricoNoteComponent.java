package it.ibm.red.web.document.mbean.component;

import java.util.List;

import it.ibm.red.business.dto.NotaDocumentoDescrizioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.INotaFacadeSRV;
import it.ibm.red.web.mbean.interfaces.IInserisciNota;

/**
 * Component document managaer data - tab storico note.
 */
public class DocumentManagerDataTabStoricoNoteComponent extends AbstractComponent {
	
	/**
	 * Serial version autogenerato.
	 */
	private static final long serialVersionUID = 2919220615084208479L;
	
	/**
	 * Logger della classe
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DocumentManagerDataTabStoricoNoteComponent.class);

	/**
	 * Servizio.
	 */
	private final INotaFacadeSRV notaSRV;
	
// INFORMAZIONI NECESSARIE A INDIVIDUARE IL DOCUMENTO.
	/**
	 * Document title document.
	 */
	private final String documentTitle;

	/**
	 * Utente.
	 */
	private final UtenteDTO utente;
	
	/**
	 * Inserisci nota.
	 */
	private IInserisciNota inserisciNota;
	
	/**
	 * Datatable dello storico note.
	 * 
	 */
	private List<NotaDocumentoDescrizioneDTO> notaList;
	
	/**
	 * Quando ho necessita di ricaricare le note, viene settato a true. </br>
	 * Al prossimo get eseguirà il refresh delle note.
	 */
	private boolean refreshDocument;
	
	/**
	 * Quando l'utente ha il permesso per visualizzare le note cancellate, puo'valorizzare questo campo. </br>
	 * Regola se le note cancellate vengono recuperate insieme alle note attive.
	 * La cancellazione delle note nello storico è solo logica.
	 */
	private boolean recuperaNoteCancellate;
	
	/**
	 * Nel costruttore carico la coda.
	 * 
	 * @param inUtente			utente in sessione.
	 * @param inDocumentTitle		documento selezionato.
	 * @param numeroDocumento	numeroDocumento del documento selezionato.
	 */
	public DocumentManagerDataTabStoricoNoteComponent(final UtenteDTO inUtente, final String inDocumentTitle, final Integer numeroDocumento) {
		notaSRV = ApplicationContextProvider.getApplicationContext().getBean(INotaFacadeSRV.class);
		this.documentTitle = inDocumentTitle;
		this.utente = inUtente;
		reloadDocument();
		final Integer documentTitleNumber = Integer.valueOf(documentTitle);
		this.inserisciNota = new  DocumentManagerStoricoInserisciNotaComponent(notaSRV, utente, documentTitleNumber);
	}

	/**
	 * Recupera lo storico note popolando {@link #notaList}.
	 */
	public void reloadDocument() {
		try {
			this.notaList = notaSRV.getAllNotes(documentTitle, recuperaNoteCancellate, utente);
		} catch (final Exception e) {
			LOGGER.error("Errore durante la cancellazione delle note da dettaglio esteso", e);
			showErrorMessage("Impossibile recuperare lo Storico note.");
		}
	}

	/**
	 * Gestisce l'eliminazione della nota.
	 * @param nota
	 */
	public void elimina(final NotaDocumentoDescrizioneDTO nota) {
		try {			
			notaSRV.deleteNotaDocumento(nota, utente.getIdAoo().intValue());
			reloadDocument();
			showInfoMessage("Nota eliminata con successo.");
		} catch (final Exception e) {
			LOGGER.error("Errore durante la cancellazione delle note da dettaglio esteso", e);
			showErrorMessage("Errore durante la cancellazione della nota.");
		}
	}

	/**
	 * Apre la dialog di inserimento.
	 */
	public void openDlgInserisci() {
		this.refreshDocument = true;
		inserisciNota.close();
	}

	/**
	 * Restituisce la lista delle note eseguendo eventualmente un reload del document.
	 * @return notaList
	 */
	public List<NotaDocumentoDescrizioneDTO> getNotaList() {
		if (refreshDocument) {
			reloadDocument();
		}
		return notaList;
	}

	/**
	 * Imposta la lista delle note.
	 * @param notaList
	 */
	public void setNotaList(final List<NotaDocumentoDescrizioneDTO> notaList) {
		this.notaList = notaList;
	}

	/**
	 * Restituisce l'interfaccia {@link #inserisciNota}.
	 * @return
	 */
	public IInserisciNota getInserisciNota() {
		return inserisciNota;
	}

	/**
	 * Imposta l'interfaccia {@link #inserisciNota}.
	 * @param inserisciNota
	 */
	public void setInserisciNota(final IInserisciNota inserisciNota) {
		this.inserisciNota = inserisciNota;
	}

	/**
	 * @return recuperaNoteCancellate
	 */
	public boolean isRecuperaNoteCancellate() {
		return recuperaNoteCancellate;
	}

	/**
	 * @param recuperaNoteCancellate
	 */
	public void setRecuperaNoteCancellate(final boolean recuperaNoteCancellate) {
		this.recuperaNoteCancellate = recuperaNoteCancellate;
	}
}
