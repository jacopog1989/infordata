package it.ibm.red.web.document.mbean.component;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.dto.ApprovazioneDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoApprovazioneEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.helper.faces.MessageSeverityEnum;

/**
 * Componet document manager data - tab approvazioni.
 */
public class DocumentManagerDataTabApprovazioniComponent implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DocumentManagerDataTabApprovazioniComponent.class.getName());

	/**
	 * Servizio.
	 */
	private final IDocumentoFacadeSRV documentoSRV;

	/**
	 * Utente.
	 */
	private final UtenteDTO utente;

	/**
	 * Dettaglio.
	 */
	private final DetailDocumentRedDTO detail;

	/**
	 * Costruttore del component.
	 * 
	 * @param inDetail
	 * @param inUtente
	 */
	public DocumentManagerDataTabApprovazioniComponent(final DetailDocumentRedDTO inDetail, final UtenteDTO inUtente) {
		detail = inDetail;
		utente = inUtente;
		documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);
	}

	/**
	 * Costruisce un pdf con le impostazioni settate dall'utente per la
	 * stampigliatura.
	 * 
	 * @return
	 */
	public StreamedContent getStampiglia() {
		StreamedContent sc = null;
		InputStream io = null;
		try {
			final List<ApprovazioneDTO> approvazioneList = detail.getApprovazioni();

			if (isStampigliabile()) { // Il secondo caso non si deve verificare il tab è disabilitato quando quando
										// questa condizione è false
				io = documentoSRV.createPdfApprovazioniStampigliate(utente, detail.getNumeroDocumento(), detail.getDocumentTitle(), approvazioneList);

				sc = new DefaultStreamedContent(io, ContentType.PDF, "approvazioni_" + detail.getNumeroDocumento() + "_del_" + detail.getAnnoDocumento() + ".pdf");
			} else {
				LOGGER.error("Tentativo di stampigliare una lista di approvazioni vuota");
				FacesHelper.showMessage(null, "Non sono presenti approvazioni da stampigliare", MessageSeverityEnum.ERROR);
			}
		} catch (final Exception e) {
			LOGGER.error("Impossibile creare il file delle approvazioni stampigliate", e);
			FacesHelper.showMessage(null, "Impossibile creare il file delle approvazioni stampigliate", MessageSeverityEnum.ERROR);
		} finally {
			closeStream(io);
		}

		return sc;
	}

	/**
	 * Gestisce la chiusura dell'Input stream.
	 * 
	 * @param io
	 */
	private static void closeStream(final InputStream io) {
		try {
			if (io != null) {
				io.close();
			}
		} catch (final Exception e) {
			LOGGER.error("Errore riscontrato nella chiusura dell'InputStream.", e);
		}
	}

	/**
	 * Verifica che il dettaglio sia stampigliabile un dettaglio è stampigliabile
	 * quando ci sono approvazioni a lui associate.
	 * 
	 * @return true se stampigliabile, false altrimenti
	 */
	public boolean isStampigliabile() {
		return CollectionUtils.isNotEmpty(detail.getApprovazioni());
	}

	/**
	 * Restituisce true se la stampigliatura è modificabile, false altrimenti.
	 * 
	 * @return true se la stampigliatura è modificabile, false altrimenti
	 */
	public boolean isStampigliaturaModificabile() {
		return isStampigliabile() && detail.getTipoAssegnazioneEnum() == TipoAssegnazioneEnum.FIRMA && !isAtLeastOneApprovazioneFirma();
	}

	private boolean isAtLeastOneApprovazioneFirma() {
		boolean firmato = false;
		final Iterator<ApprovazioneDTO> iterator = detail.getApprovazioni().iterator();
		while (!firmato && iterator.hasNext()) {
			final ApprovazioneDTO approvazione = iterator.next();

			final Long tipoApprovazioneId = Long.valueOf(approvazione.getIdTipoApprovazione());
			firmato = TipoApprovazioneEnum.FIRMA_DIGITALE.getId().equals(tipoApprovazioneId) || TipoApprovazioneEnum.FIRMA_AUTOGRAFA.getId().equals(tipoApprovazioneId);
		}
		return firmato;
	}

	/**
	 * Restituisce il detail del documento.
	 * 
	 * @return detail
	 */
	public DetailDocumentRedDTO getDetail() {
		return detail;
	}

}
