package it.ibm.red.web.servlets.delegate;

import it.ibm.red.web.servlets.delegate.exception.DelegateException;

/**
 * 
 * @author a.dilegge
 *
 *	Interfaccia degli oggetti usati per la gestione di get e post delegate 
 */
public interface IDelegate {

	/**
	 * Aggiorna il metadato trasformazionePDFErrore che detiene l'informazione di esito della firma.
	 * 
	 * @param signTransactionId
	 * @param idUtente
	 * @param documentTitle
	 * @param metadatoPdfErrorValue
	 * @throws DelegateException
	 */
	void aggiornaStatoDocumento(String signTransactionId, Long idUtente, String documentTitle, Integer metadatoPdfErrorValue) throws DelegateException;

}
