package it.ibm.red.web.ricerca.mbean;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.google.common.net.MediaType;

import it.ibm.red.business.dto.ParamsRicercaContiConsuntiviDTO;
import it.ibm.red.business.dto.RisultatoRicercaContiConsuntiviDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.dto.StatoRicercaDTO;
import it.ibm.red.business.enums.ContiConsuntiviEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IContiConsuntiviFacadeSRV;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;

/**
 * Bean che gestisca la ricerca per conti consuntivi.
 */
@Named(ConstantsWeb.MBean.RICERCA_CONTI_CONSUNTIVI_BEAN)
@ViewScoped
public class RicercaContiConsuntiviBean extends AbstractBean {
	
	/**
	 * serial version UID.
	 */
	private static final long serialVersionUID = -8659886620858883202L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaContiConsuntiviBean.class.getName());
	
	/**
	 * Session Bean.
	 */
	private SessionBean sessionBean;
	
	/**
	 * Service per la generazione del pdf.
	 */
	private IContiConsuntiviFacadeSRV contiConsuntiviSRV;

	/**
	 * Form di ricerca elenco notifiche e conti consuntivi.
	 */
	private ParamsRicercaContiConsuntiviDTO formContiConsuntivi;
	
	/**
	 * Risultato.
	 */
	private StatoRicercaDTO result;
	
	/**
	 * Lista delle sedi estere.
	 */
	private List<SelectItemDTO> sediEstere;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		setResult((StatoRicercaDTO) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.RISULTATO_RICERCA_CONTI_CONSUNTIVI, true));
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		ITipologiaDocumentoFacadeSRV tipologiaDocumentoSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoFacadeSRV.class);
		contiConsuntiviSRV = ApplicationContextProvider.getApplicationContext().getBean(IContiConsuntiviFacadeSRV.class);
		
		if(result != null) {
			formContiConsuntivi = (ParamsRicercaContiConsuntiviDTO) result.getFormRicerca();
		}
		
		sediEstere = tipologiaDocumentoSRV.getValuesBySelector(ContiConsuntiviEnum.ANAGSEDEST.getMetadatoName(), null, sessionBean.getUtente().getIdAoo());
	}
	
	/**
	 * Metodo per reimpostare nel session bean il form di ricerca.
	 */
	public void ripetiRicercaContiConsuntiviUcb() {
		try {
			sessionBean.getRicercaFormContainer().setContiConsuntiviUCB(formContiConsuntivi);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}
	
	/**
	 * Restituisce il nome del file per l'export della coda.
	 * 
	 * @return nome file per export
	 */
	public String getNomeFileExport() {
		String nome = "";
		if (sessionBean.getUtente() != null) {
			nome += sessionBean.getUtente().getNome().toLowerCase() + "_" + sessionBean.getUtente().getCognome().toLowerCase() + "_";
		}
		
		nome += "report_elenco_notifiche_conti_consuntivi_";
		
		final SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH.mm");
		nome += sdf.format(new Date()); 
		
		return nome;
	}
	
	/**
	 * Restituisce lo streamed content.
	 */
	public StreamedContent downloadPdf() {
		StreamedContent sc = null;
		final String exportString = getNomeFileExport() + ".pdf";
		List<RisultatoRicercaContiConsuntiviDTO> risultatiContiConsuntivi = (List<RisultatoRicercaContiConsuntiviDTO>) result.getRisultatiRicerca();
		try {
			// recupero del content
			final InputStream stream = new ByteArrayInputStream(contiConsuntiviSRV.generaPdfReport(formContiConsuntivi, risultatiContiConsuntivi, sessionBean.getUtente()));
			sc = new DefaultStreamedContent(stream, MediaType.PDF.toString(), exportString);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il download del documento", e);
			showError(e);
		}
		return sc;
	}
	
	/**
	 * Restituisce il form di ricerca elenco notifiche e conti consuntivi.
	 * 
	 * @return form ricerca elenco notifiche e conti consuntivi
	 */
	public ParamsRicercaContiConsuntiviDTO getFormContiConsuntivi() {
		return formContiConsuntivi;
	}

	/**
	 * Imposta il form ricerca elenco notifiche e conti consuntivi.
	 * 
	 * @param formContiConsuntivi
	 */
	public void setFormContiConsuntivi(ParamsRicercaContiConsuntiviDTO formContiConsuntivi) {
		this.formContiConsuntivi = formContiConsuntivi;
	}

	/**
	 * Restituisce il risultato.
	 * 
	 * @return risultato
	 */
	public StatoRicercaDTO getResult() {
		return result;
	}

	/**
	 * Imposta il risultato.
	 * 
	 * @param result
	 */
	public void setResult(StatoRicercaDTO result) {
		this.result = result;
	}

	/**
	 * Lista delle sedi estere.
	 * 
	 * @return lista sedi estere
	 */
	public List<SelectItemDTO> getSediEstere() {
		return sediEstere;
	}

	/**
	 * Imposta la lista delle sedi estere.
	 * 
	 * @param sediEstere
	 */
	public void setSediEstere(List<SelectItemDTO> sediEstere) {
		this.sediEstere = sediEstere;
	}

}
