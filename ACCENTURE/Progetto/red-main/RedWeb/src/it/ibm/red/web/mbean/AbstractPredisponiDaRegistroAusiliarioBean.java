package it.ibm.red.web.mbean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;

import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.dto.CollectedParametersDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.LookupTableDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.RegistroDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.LookupTablePresentationModeEnum;
import it.ibm.red.business.enums.SeverityValidationPluginEnum;
import it.ibm.red.business.enums.TipoDocumentoModeEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IRegistrazioniAusiliarieFacadeSRV;
import it.ibm.red.business.service.facade.IRegistroAusiliarioFacadeSRV;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.SessionObject;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Abstract dei bean che gestiscono la predisposizione di un registro ausiliario.
 */
public abstract class AbstractPredisponiDaRegistroAusiliarioBean extends AbstractBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 757033006964145862L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AbstractPredisponiDaRegistroAusiliarioBean.class);

	/**
	 * Document title documento ingresso.
	 */
	private String documentTitleIngresso;

	/**
	 * Document title documento uscita.
	 */
	private String documentTitleUscita;

	/**
	 * Servizio registro ausiliario.
	 */
	private IRegistroAusiliarioFacadeSRV registroAusiliarioSRV;

	/**
	 * Lista registri.
	 */
	private Collection<SelectItemDTO> listaRegistri;

	/**
	 * Identificativo registro.
	 */
	private Integer idRegistro;

	/**
	 * Nome del file.
	 */
	private String nomeFile;

	/**
	 * Lista metadati registro.
	 */
	private Collection<MetadatoDTO> listaMetadatiRegistro;

	/**
	 * Lista ridotta metadati registro.
	 */
	private Collection<MetadatoDTO> listaMetadatiRegistroLight;

	/**
	 * Lista completa metadati registro.
	 */
	private Collection<MetadatoDTO> listaMetadatiRegistroHeavy;

	/**
	 * Metadati documento in uscita.
	 */
	private Collection<MetadatoDTO> metadatiForDocUscita;

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Flag visualizzazione anteprima.
	 */
	private boolean visualizzaAnteprima;

	/**
	 * Info documento principale.
	 */
	private FileDTO docPrincipale;

	/**
	 * Info motivazioni.
	 */
	private LookupTableDTO motivazioni;

	/**
	 * Servizio tipologia documento.
	 */
	private ITipologiaDocumentoFacadeSRV tipologiaDocSRV;

	/**
	 * Servizio registrazione ausiliarie.
	 */
	private IRegistrazioniAusiliarieFacadeSRV registrazioniAusiliarieSRV;

	/**
	 * Metodo che consente di inizializzare il bean.
	 */
	public void init() {
		listaMetadatiRegistro = new ArrayList<>();
		utente = ((SessionBean) FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN)).getUtente();
		registroAusiliarioSRV = ApplicationContextProvider.getApplicationContext().getBean(IRegistroAusiliarioFacadeSRV.class);
		tipologiaDocSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoFacadeSRV.class);
		registrazioniAusiliarieSRV = ApplicationContextProvider.getApplicationContext().getBean(IRegistrazioniAusiliarieFacadeSRV.class);
	}

	/**
	 * Gestisce la logica di predisposizione registrazione ausiliaria.
	 */
	public abstract void predisponi();

	/**
	 * Gestisce la chiusura della dialog di predisposizione registrazione
	 * ausiliaria.
	 */
	public abstract void chiudi();

	/**
	 * Recupera la lista dei metadati del registro ausiliario e li elabora per la
	 * gestione degli stessi nella view.
	 */
	public void loadMetadatiRegistro() {
		if (idRegistro != null) {
			// Per il registro selezionato, popolo la lista dei metadati
			listaMetadatiRegistro = registroAusiliarioSRV.getMetadatiRegistro(idRegistro, utente, documentTitleIngresso);
			// Popolo la lista dei metadati con i valori che già possono essere recuperati
			initListaMetadati();
		} else {
			// Se l'id è null vuol dire che l'utente ha selezionato l'opzione di non
			// selezione, quindi vanno reinizializzate le liste dei metadati
			setListaMetadatiRegistroLight(new ArrayList<>());
			setListaMetadatiRegistroHeavy(new ArrayList<>());
			setListaMetadatiRegistro(new ArrayList<>());
		}
	}

	/**
	 * Esegue l'aggiornamento dei dati della registrazione ausiliaria.
	 */
	protected void updateDatiRegistrazioneAusiliaria() {
		registrazioniAusiliarieSRV.updateDatiRegistrazioneAusiliaria(documentTitleUscita, utente.getIdAoo().intValue(), listaMetadatiRegistro, false);
	}

	/**
	 * Costruisce il template a partire dai metadati del registro ausiliario,
	 * mostrando un'anteprima.
	 * 
	 * @param isCreazione
	 *            indica se si è in creazione o in modifica
	 */
	protected void aggiornaAnteprima(final boolean isCreazione) {

		if (idRegistro == null || CollectionUtils.isEmpty(listaMetadatiRegistro)) {
			showWarnMessage("Selezionare un registro e popolare i metadati");
			return;
		}
		try {
			final CollectedParametersDTO result = registroAusiliarioSRV.collectParameters(utente, idRegistro, listaMetadatiRegistro, true);

			setMetadatiForDocUscita(registroAusiliarioSRV.getMetadatiForDocUscita(idRegistro, documentTitleIngresso, utente, listaMetadatiRegistro, isCreazione));

			if (SeverityValidationPluginEnum.ERROR.equals(result.getSeverityMessage())) {
				showError(result.getMessage());
				setPreviewHidden();
			} else {

				final byte[] pdf = registroAusiliarioSRV.createFakePdf(idRegistro, result.getStringParams());

				if (!nomeFile.endsWith(".pdf")) {
					// splitto il nome del file in caso l'utente abbia inserito un'estensione
					// diversa o semplicemente punti
					final String[] splitNomeFile = nomeFile.split("[.]");
					// utilizzo la parte di input che precede il primo punto matchato
					nomeFile = splitNomeFile[0] + ".pdf";
				}
				setDocPrincipale(new FileDTO(nomeFile, pdf, ContentType.PDF));
				FacesHelper.putObjectInSession(SessionObject.CONTENT_HIGHLIGHT_SIGN_FIELD, docPrincipale);
				setPreviewVisible();
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un problema durante la generazione del template", e);
			showError("Si è verificato un problema durante la generazione del template");
		}
	}

	/**
	 * Questo metodo inizializza le due liste per la renderizzazione della maschera
	 * di compilazione del template. Le due liste contengono diversi tipi di
	 * componenti, questo consente di gestire i diversi componenti in diverso modo
	 * sulla maschera.
	 */
	public void initListaMetadati() {
		final List<MetadatoDTO> listaMLight = new ArrayList<>();
		List<MetadatoDTO> listaMHeavy = new ArrayList<>();
		List<MetadatoDTO> fullList = null;

		// Popolo Datatable per gestire le motivazioni e lo inserisco nella lista dei
		// metadati
		final LookupTableDTO motivazioniLookup = initMotivazioni();

		for (final MetadatoDTO m : listaMetadatiRegistro) {
			if (m.getType().equals(TipoMetadatoEnum.LOOKUP_TABLE) || m.getType().equals(TipoMetadatoEnum.CAPITOLI_SELECTOR) 
					|| m.getType().equals(TipoMetadatoEnum.TEXT_AREA)
					|| m.getType().equals(TipoMetadatoEnum.PERSONE_SELECTOR)) {
				if ("motivazione-osservazione".equals(m.getName()) && (m.getType().equals(TipoMetadatoEnum.LOOKUP_TABLE))) {
					// In fase di modifica l'item sarà già selezionato, in tal caso devo prepopolare
					// con quell'item
					if (((LookupTableDTO) m).getLookupValueSelected() != null) {
						motivazioniLookup.setLookupValueSelected(((LookupTableDTO) m).getLookupValueSelected());
					}
					listaMHeavy.add(motivazioniLookup);
				} else {
					listaMHeavy.add(m);
				}
			} else {
				
				if("amministrazione-protocollo-mittente".equals(m.getName())) {
					m.setDisplayName("Amministrazione Proponente");
				}
				
				listaMLight.add(m);
			}

		}

		listaMHeavy = sortList(listaMHeavy);
		
		setListaMetadatiRegistroLight(listaMLight);
		setListaMetadatiRegistroHeavy(listaMHeavy);

		fullList = new ArrayList<>(listaMLight);
		fullList.addAll(listaMHeavy);
		setListaMetadatiRegistro(fullList);
	}
	

	/**
	 * Restituisce una lista di metadati spostando il metadato <em> destinatari
	 * </em> in modo tale che sia il primo tra le text area.
	 * 
	 * @param listToSort
	 *            Lista da ordinare.
	 * @return Lista ordinata.
	 */
	private List<MetadatoDTO> sortList(List<MetadatoDTO> listToSort) {
		
		// Viene impostato il metadato "destinatari" in prima posizione
		List<MetadatoDTO> orderedList = listToSort.stream().filter(m -> "destinatari".equals(m.getName())).collect(Collectors.toList());
		
		// Vengono aggiunti i restanti metadati
		for (MetadatoDTO metadato : listToSort) {
			if (!orderedList.contains(metadato)) {
				orderedList.add(metadato);
			}
		}
		
		// Vengono ordinati i metadati in base al tipo nell'ordine: Capitoli di spesa, Lookuptable e infine Text Area
		orderedList.sort(Comparator.comparing(MetadatoDTO::getType).reversed());
		return orderedList;
	}

	/**
	 * Inizializza la lista delle motivazioni, definendo i valori della LookupTable.
	 */
	public LookupTableDTO initMotivazioni() {
		final List<SelectItemDTO> descs = tipologiaDocSRV.getValuesBySelector("MOTIVAZIONI", null, null);

		motivazioni = new LookupTableDTO("motivazione-osservazione", "Motivazione", new SelectItemDTO(), LookupTablePresentationModeEnum.DATATABLE,
				TipoDocumentoModeEnum.SEMPRE, TipoDocumentoModeEnum.MAI, TipoDocumentoModeEnum.SEMPRE);

		motivazioni.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
		motivazioni.setLookupValues(descs);
		motivazioni.setType(TipoMetadatoEnum.LOOKUP_TABLE);
		return motivazioni;
	}

	/**
	 * Imposta a true l'attributo visualizzaAnteprima.
	 */
	public void setPreviewVisible() {
		setVisualizzaAnteprima(true);
	}

	/**
	 * Imposta a false l'attributo visualizzaAnteprima.
	 */
	public void setPreviewHidden() {
		setVisualizzaAnteprima(false);
	}

	/**
	 * Inizializza il contentuo della combobox per la selezione del registro
	 * ausiliario per cui si vuole creare il template.
	 * 
	 * @param inRegistri
	 * @param idRegistroAusiliario
	 */
	public void initListaRegistri(final Collection<RegistroDTO> inRegistri, final Integer idRegistroAusiliario) {
		listaRegistri = new ArrayList<>();

		for (final RegistroDTO t : inRegistri) {
			// Popolo la combobox per la selezione del registro in front-end
			if (idRegistroAusiliario == null || idRegistroAusiliario == 0 || idRegistroAusiliario.equals(t.getId())) {
				listaRegistri.add(new SelectItemDTO(t.getId(), t.getDisplayName()));
			}
		}
	}

	/**
	 * Restituisce un nome per il template generato in fase di creazione.
	 * 
	 * @param codiceTipoReg il codice della tipologia del registro
	 * @param numProt       il numero di protocollo del documento in Entrata
	 * 
	 * @return nome generato
	 */
	public String generateNomeTemplate(final String codiceTipoReg, final Integer numProt) {
		final StringBuilder sb = new StringBuilder();

		sb.append("template_");

		if (!StringUtils.isNullOrEmpty(codiceTipoReg)) {
			sb.append(codiceTipoReg).append("_");
		}

		if (numProt != null) {
			sb.append(numProt).append("_");
		}

		final String timeStamp = new SimpleDateFormat("ddMMyyyyhhmmss").format(new Date());
		if (!StringUtils.isNullOrEmpty(timeStamp)) {
			sb.append(timeStamp);
		}

		return sb.toString();
	}

	/**
	 * Restituisce il document title del documento in ingresso.
	 * 
	 * @return document Title
	 */
	public String getDocumentTitleIngresso() {
		return documentTitleIngresso;
	}

	/**
	 * Imposta il document title del documento.
	 * 
	 * @param documentTitle il document title da impostare
	 */
	public void setDocumentTitleIngresso(final String documentTitle) {
		this.documentTitleIngresso = documentTitle;
	}

	/**
	 * Restituisce il document title del documento in uscita.
	 * 
	 * @return document Title
	 */
	public String getDocumentTitleUscita() {
		return documentTitleUscita;
	}

	/**
	 * Imposta il document title del documento.
	 * 
	 * @param documentTitleUscita il document title da impostare
	 */
	public void setDocumentTitleUscita(final String documentTitleUscita) {
		this.documentTitleUscita = documentTitleUscita;
	}

	/**
	 * Restituisce la lista dei registri ausiliari relativi alla tipologia documento
	 * del documento in ingresso.
	 * 
	 * @return Collection di SlectItemDTO
	 */
	public Collection<SelectItemDTO> getListaRegistri() {
		return listaRegistri;
	}

	/**
	 * Restituisce la lista dei metadati del registro.
	 * 
	 * @return Collection di MetadatoDTO contenenti le informazioni sui metadati
	 *         associati al registro.
	 */
	public Collection<MetadatoDTO> getListaMetadatiRegistro() {
		return listaMetadatiRegistro;
	}

	/**
	 * Imposta la lista dei metadati del registro.
	 * 
	 * @param listaMetadatiRegistro lista dei metadati da impostare
	 */
	public void setListaMetadatiRegistro(final Collection<MetadatoDTO> listaMetadatiRegistro) {
		this.listaMetadatiRegistro = listaMetadatiRegistro;
	}

	/**
	 * Restituisce il valore dell'attributo visualizzaAnteprima.
	 * 
	 * @return true se l'anteprima è visualizzabile, false altrimenti
	 */
	public boolean isVisualizzaAnteprima() {
		return visualizzaAnteprima;
	}

	/**
	 * Imposta l'attributo visualizzaAnteprima.
	 * 
	 * @param visualizzaAnteprima
	 */
	public void setVisualizzaAnteprima(final boolean visualizzaAnteprima) {
		this.visualizzaAnteprima = visualizzaAnteprima;
	}

	/**
	 * Restituisce il documento principale come FileDTO.
	 * 
	 * @return il content del documento principale come FileDTO
	 */
	public FileDTO getDocPrincipale() {
		return docPrincipale;
	}

	/**
	 * Imposta il content del documento principale.
	 * 
	 * @param docPrincipale
	 */
	public void setDocPrincipale(final FileDTO docPrincipale) {
		this.docPrincipale = docPrincipale;
	}

	/**
	 * Restituisce la LookupTable delle motivazioni per un registro di tipo
	 * Osservazione.
	 * 
	 * @return LookupTableDTO
	 */
	public LookupTableDTO getMotivazioni() {
		return motivazioni;
	}

	/**
	 * Imposta le motivazioni per un registro di tipo Osservazione.
	 */
	public void setMotivazioni(final LookupTableDTO motivazioni) {
		this.motivazioni = motivazioni;
	}

	/**
	 * Restituisce l'id associato al registro.
	 * 
	 * @return idRegistro
	 */
	public Integer getIdRegistro() {
		return idRegistro;
	}

	/**
	 * Imposta l'id del registro.
	 * 
	 * @param idRegistro l'id del registro da impostare
	 */
	public void setIdRegistro(final Integer idRegistro) {
		this.idRegistro = idRegistro;
	}

	/**
	 * Restituisce il nome del file in fase di creazione.
	 * 
	 * @return nome File
	 */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * Imposta il nome del file in fase di creazione.
	 * 
	 * @param nomeFile
	 */
	public void setNomeFile(final String nomeFile) {
		this.nomeFile = nomeFile;
	}

	/**
	 * Getter IRegistroAusiliarioFacadeSRV.
	 * 
	 * @return il service per la gestione dei registri ausiliari
	 */
	protected IRegistroAusiliarioFacadeSRV getRegistroAusiliarioSRV() {
		return registroAusiliarioSRV;
	}

	/**
	 * Getter utente.
	 * 
	 * @return il DTO associato all'utente
	 */
	protected UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Restituisce la lista dei metadati "Light" di registro di un template. I
	 * metadati "Light" comportano una rappresentazione su una colonna sulla view.
	 * 
	 * @return Collection di MetadatoDTO
	 */
	public Collection<MetadatoDTO> getListaMetadatiRegistroLight() {
		return listaMetadatiRegistroLight;
	}

	/**
	 * Imposta la lista dei metadati di registro "Light".
	 * 
	 * @param listaMetadatiRegistroHeavy i metadati considerati "Light" da impostare
	 */
	public void setListaMetadatiRegistroLight(final Collection<MetadatoDTO> listaMetadatiRegistroLight) {
		this.listaMetadatiRegistroLight = listaMetadatiRegistroLight;
	}

	/**
	 * Restituisce la lista dei metadati "Heavy" di registro di un template. I
	 * metadati "Heavy" comportano una rappresentazione su due colonne sulla view.
	 * 
	 * @return Collection di MetadatoDTO
	 */
	public Collection<MetadatoDTO> getListaMetadatiRegistroHeavy() {
		return listaMetadatiRegistroHeavy;
	}

	/**
	 * Imposta la lista dei metadati di registro "Heavy".
	 * 
	 * @param listaMetadatiRegistroHeavy i metadati considerati "Heavy" da impostare
	 */
	public void setListaMetadatiRegistroHeavy(final Collection<MetadatoDTO> listaMetadatiRegistroHeavy) {
		this.listaMetadatiRegistroHeavy = listaMetadatiRegistroHeavy;
	}

	/**
	 * Restituisce la lista dei metadati del documento in uscita in fase di
	 * predisposizione.
	 * 
	 * @return Collection di MetadatoDTO
	 */
	public Collection<MetadatoDTO> getMetadatiForDocUscita() {
		return metadatiForDocUscita;
	}

	/**
	 * Imposta la lista dei metadati del documento in uscita, servirà in fase di
	 * predisposizione registro ausiliario.
	 * 
	 * @param metadatiForDocUscita
	 */
	public void setMetadatiForDocUscita(final Collection<MetadatoDTO> metadatiForDocUscita) {
		this.metadatiForDocUscita = metadatiForDocUscita;
	}

}
