package it.ibm.red.web.servlets;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import com.google.common.net.MediaType;

import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.constants.ConstantsWeb.SessionObject;
import it.ibm.red.web.utils.PlaceHolderUtils;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;

/**
 * The Class HighlightSignFieldServlet.
 *
 * @author CPIERASC
 * 
 *         Servlet download content.
 */
public class HighlightSignFieldServlet extends HttpServlet {

	private static final String ID_TIPO_DOC = "idTipoDoc";

	private static final String ID_UFF_CREATORE = "idUffCreatore";

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(HighlightSignFieldServlet.class.getName());
 
    /**
     * Costruttore.
     */
    public HighlightSignFieldServlet() {
        super();
    }
    
    
    @Override
    protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        try {
            // Il chiamante deve mettere FileDTO in sessione
            HttpSession session = request.getSession();
            FileDTO file = null;
            
            if (session != null) {
                file = (FileDTO) session.getAttribute(SessionObject.CONTENT_HIGHLIGHT_SIGN_FIELD);
                session.removeAttribute(SessionObject.CONTENT_HIGHLIGHT_SIGN_FIELD);
                
                Long idAoo = null;
                boolean conCampoFirma = false;
                boolean converti = false;
                boolean inEntrata = false;
                boolean inModifica = false;
                boolean mantieniFormatoOriginale = false;
                boolean convertibile = false;
                boolean firmaMultipla = false;
                boolean principale = false;
                boolean daNonProtocollare = false; 
                boolean stampigliaturaSigla = false;
                boolean contentLibroFirma = false;
                Long idSiglatario = null;
                Long idUffCreatore = null;
        		Integer idTipoDoc = null; 
        		boolean posizioneSx; 
                final JSONParser jsonParser = new JSONParser();
                JSONObject jsonObject;
                
                final String parameters = request.getParameter("parameters");
                
				jsonObject = (JSONObject) jsonParser.parse(parameters);
				idAoo = Long.valueOf((Integer) jsonObject.get("idAoo"));
				conCampoFirma = Boolean.TRUE.equals(jsonObject.get("conCampoFirma"));
				converti = Boolean.TRUE.equals(jsonObject.get("converti"));
				inEntrata = Boolean.TRUE.equals(jsonObject.get("inEntrata"));
				inModifica = Boolean.TRUE.equals(jsonObject.get("inModifica"));
				mantieniFormatoOriginale = Boolean.TRUE.equals(jsonObject.get("formatoOriginale"));
				convertibile = Boolean.TRUE.equals(jsonObject.get("convertibile"));
				firmaMultipla = Boolean.TRUE.equals(jsonObject.get("firmaMultipla"));
				principale = Boolean.TRUE.equals(jsonObject.get("principale"));
				daNonProtocollare = Boolean.TRUE.equals(jsonObject.get("daNonProtocollare"));
				stampigliaturaSigla = Boolean.TRUE.equals(jsonObject.get("stampigliaturaSigla"));
				contentLibroFirma = Boolean.TRUE.equals(jsonObject.get("contentLibroFirma"));
				idSiglatario = getIdSiglatario(idSiglatario, jsonObject);
				idUffCreatore = getIdUfficioCreatore(idUffCreatore, jsonObject);
				idTipoDoc = getIdTipoDocumento(idTipoDoc, jsonObject);		
				
				if (jsonObject.get("posizioneSx") != null) {
					posizioneSx = Boolean.TRUE.equals(jsonObject.get("posizioneSx"));
					if (posizioneSx) {
						file = (FileDTO) session.getAttribute(SessionObject.CONTENT_HIGHLIGHT_SIGN_FIELD_SX);
		                session.removeAttribute(SessionObject.CONTENT_HIGHLIGHT_SIGN_FIELD_SX);	
					} else {
						file = (FileDTO) session.getAttribute(SessionObject.CONTENT_HIGHLIGHT_SIGN_FIELD_DX);
						session.removeAttribute(SessionObject.CONTENT_HIGHLIGHT_SIGN_FIELD_DX);
					}
				}
                
                final byte[] newContent = PlaceHolderUtils.inserisciPlaceholderFirmaSigla(file, idAoo,
		                conCampoFirma, converti, inEntrata, inModifica, mantieniFormatoOriginale, contentLibroFirma,
		                convertibile, firmaMultipla, principale, daNonProtocollare, stampigliaturaSigla, idSiglatario,
		                idUffCreatore, idTipoDoc, false);  
                
                writeContent(response, newContent); 
                
            	if (newContent != null) {
            		response.setContentLength(newContent.length);
            	}
                final String contentType = MediaType.PDF + "; name=\"" + StringUtils.split(file.getFileName(), ".")[0] + ".pdf" + "\"";
                response.setContentType(contentType);
                final String header = " inline; filename=\"" + StringUtils.split(file.getFileName(), ".")[0] + ".pdf" + "\"";
                response.setHeader("Content-Disposition", header);
            }
        } catch (Exception e) {
            LOGGER.error("Si è verificato un errore durante il recupero o la conversione del file", e);
        }
    }

	private void writeContent(final HttpServletResponse response, final byte[] newContent) {
		try {
			response.getOutputStream().write(newContent);
		} catch (IOException e) {
		    LOGGER.error("Si è verificato un errore durante la scrittura del file sull'output stream", e);
		}
	}

	/**
	 * Restituisce l'id tipo documento se presente nel json, null altrimenti.
	 * @param idTipoDoc
	 * @param jsonObject
	 * @return id tipo documento se presente nel json, null altrimenti
	 */
	private static Integer getIdTipoDocumento(final Integer idTipoDoc, final JSONObject jsonObject) {
		Integer idReturn = idTipoDoc;
		if(jsonObject.get(ID_TIPO_DOC)!=null && jsonObject.get(ID_TIPO_DOC)!="") {
			try {
				idReturn = (Integer) jsonObject.get(ID_TIPO_DOC);
			}catch(Exception e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
		return idReturn;
	}


	/**
	 * Restituisce l'id dell'ufficio creatore se presente nel json, null altrimenti.
	 * @param idUffCreatore
	 * @param jsonObject
	 * @return id dell'ufficio creatore se presente nel json, null altrimenti
	 */
	private static Long getIdUfficioCreatore(final Long idUffCreatore, final JSONObject jsonObject) {
		Long idReturn = idUffCreatore;
		if(jsonObject.get(ID_UFF_CREATORE)!=null && jsonObject.get(ID_UFF_CREATORE)!="") {
			try {
				idReturn = Long.valueOf((Integer) jsonObject.get(ID_UFF_CREATORE));
			}catch(Exception e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
		return idReturn;
	}


	/**
	 * Restituisce l'id del siglatario se presente nel json, null altrimenti.
	 * @param idSiglatario
	 * @param jsonObject
	 * @return id del siglatario se presente nel json, null altrimenti
	 */
	private static Long getIdSiglatario(final Long idSiglatario, final JSONObject jsonObject) {
		Long idReturn = idSiglatario;
		if(jsonObject.get("idSiglatario")!=null) {
			try {
				idReturn = Long.valueOf((Integer) jsonObject.get("idSiglatario"));
			}catch(Exception e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
		return idReturn;
	}
    
}
