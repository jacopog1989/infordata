package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections4.CollectionUtils;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.service.facade.IRichiesteVistoFacadeSRV;
import it.ibm.red.business.utils.OrganigrammaRetrieverUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;
import it.ibm.red.web.mbean.interfaces.OrganigrammaRetriever;

/**
 * Bean che gestisce la response di richiesta verifica.
 */
@Named(ConstantsWeb.MBean.RICHIEDI_VERIFICA_BEAN)
@ViewScoped
public class RichiediVerificaBean extends AbstractBean implements InitHumanTaskInterface, OrganigrammaRetriever {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -4417275992671167301L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RichiediVerificaBean.class.getName());

	/**
	 * Documenti selezionati.
	 */
	private List<MasterDocumentRedDTO> documentiSelezionati;

	/**
	 * Motivo assegnazione.
	 */
	private String motivoAssegnazione;

	/**
	 * Session bean.
	 */
	private SessionBean sb;

	/**
	 * Lista docunenti bean.
	 */
	private ListaDocumentiBean ldb;

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Servizio.
	 */
	private IOrganigrammaFacadeSRV organigrammaSRV;

	/**
	 * Bean assegnazione.
	 */
	private MultiAssegnazioneBean assegnazioneBean;

	/**
	 * Servizio.
	 */
	private IRichiesteVistoFacadeSRV richiesteVistoSRV;

	/**
	 * Nodo.
	 */
	private transient TreeNode root;

	/**
	 * Controlla se i campi obbligatori sono stati compilati ed eventualmente esegue
	 * la response.
	 */
	public void checkEResponse() {
		// controllo se i campi obbligatori sono stati compilati ed eventualmente eseguo
		// la response
		if (assegnazioneBean != null && !assegnazioneBean.getElementList().get(0).getData().getDescrizioneNodo().isEmpty()) { // esiste solo un assegnatario
			richiediVerificaResponse();
			FacesHelper.executeJS("PF('dlgGeneric').hide()");
			FacesHelper.executeJS("PF('dlgShowResult').show()");
			FacesHelper.update("centralSectionForm:idDlgShowResult");
		} else {
			showError("Compilare il campo Assegnatario per verifica.");
		}

	}

	/**
	 * Gestisce la response per uno o più documenti ({@link #documentiSelezionati}),
	 * conserva l'esito e lo memorizza nel ListaDocumentiBean. A valle delle
	 * operazione gli esiti sulle operazioni vengono mostrati in una dialog
	 * apposita.
	 */
	public void richiediVerificaResponse() {
		// E' una response per più documento (multi)
		Collection<EsitoOperazioneDTO> eOpe = new ArrayList<>();
		final List<String> wobNumbers = new ArrayList<>();
		try {
			richiesteVistoSRV = ApplicationContextProvider.getApplicationContext().getBean(IRichiesteVistoFacadeSRV.class);

			// pulisco eventuali esiti
			ldb.cleanEsiti();

			final List<NodoOrganigrammaDTO> nodoAssegnatari = assegnazioneBean.getSelectedElementList();

			// prendo i wobNumber dei documenti
			for (final MasterDocumentRedDTO documento : documentiSelezionati) {
				if (!StringUtils.isNullOrEmpty(documento.getWobNumber())) {
					wobNumbers.add(documento.getWobNumber());
				}
			}

			if (CollectionUtils.isEmpty(nodoAssegnatari)) {
				eOpe.add(new EsitoOperazioneDTO(null, false, "Per continuare è necessario aggiungere un assegnatario per verifica"));
			} else {
				final List<AssegnatarioOrganigrammaDTO> assegnatari = nodoAssegnatari.stream().map(c -> OrganigrammaRetrieverUtils.create(c)).collect(Collectors.toList());
				// chiamo il service (multi doc) - ci sarà sempre un solo assegnatario
				eOpe = richiesteVistoSRV.richiediVerifica(utente, wobNumbers, assegnatari.get(0), motivoAssegnazione);
			}

			ldb.getEsitiOperazione().addAll(eOpe);
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.RICHIEDI_VERIFICA_BEAN);

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il tentato sollecito della response 'RICHIEDI VERIFICA'", e);
			showError("Si è verificato un errore durante il tentato sollecito della response 'RICHIEDI VERIFICA'");
		}

	}

	/**
	 * Carica l'organigramma.
	 * @return nodo rood dell'organigramma
	 */
	@Override
	public TreeNode loadRootOrganigramma() {
		try {
			root = new DefaultTreeNode("Root", null);
			root.setSelectable(true);

			final NodoOrganigrammaDTO padre = new NodoOrganigrammaDTO();
			padre.setDescrizioneNodo(utente.getNodoDesc());

			// creazione dell'unico nodo necessario (ufficio dell'utente in sessione)
			final TreeNode nodoPadre = new DefaultTreeNode(padre, root);
			nodoPadre.setExpanded(true);
			nodoPadre.setSelectable(false);

			// recupero degli utenti appartenenti al suo stesso ufficio
			final NodoOrganigrammaDTO dto = new NodoOrganigrammaDTO();
			dto.setIdAOO(utente.getIdAoo());
			dto.setIdNodo(utente.getIdUfficio());
			dto.setUtentiVisible(true);

			final List<NodoOrganigrammaDTO> utentiList = organigrammaSRV.getFigliAlberoAssegnatarioPerStornaAUtente(utente.getIdUfficio(), dto);
			DefaultTreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO utenteNodo : utentiList) {
				nodoToAdd = new DefaultTreeNode(utenteNodo, nodoPadre);
				nodoToAdd.setSelectable(true);
				nodoPadre.getChildren().add(nodoToAdd);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}
		return root;
	}

	/**
	 * Gestisce l'evento di apertura del nodo.
	 * @param node nodo da aprire
	 */
	@Override
	public void onOpenTree(final TreeNode node) {
		try {
			final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) node.getData();
			final List<NodoOrganigrammaDTO> figli = organigrammaSRV.getFigliAlberoAssegnatarioPerConoscenza(utente.getIdUfficio(), nodoExp, false);

			DefaultTreeNode nodoToAdd = null;
			node.getChildren().clear();
			for (final NodoOrganigrammaDTO n : figli) {
				n.setCodiceAOO(utente.getCodiceAoo());

				nodoToAdd = new DefaultTreeNode(n, node);
				if (n.getFigli() > 0 || n.isUtentiVisible()) {
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}

	}

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		documentiSelezionati = inDocsSelezionati;
	}

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
		utente = sb.getUtente();

		organigrammaSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);

		assegnazioneBean = new MultiAssegnazioneBean();
		assegnazioneBean.initialize(this);

	}

	/* ################### Getters e Setters ######################### */
	/**
	 * Restituisce la lista dei documenti selezionati.
	 * 
	 * @return documenti Selezionati
	 */
	public List<MasterDocumentRedDTO> getDocumentiSelezionati() {
		return documentiSelezionati;
	}

	/**
	 * Imposta la lista dei documenti selezionati.
	 * 
	 * @param documentiSelezionati
	 */
	public void setDocumentiSelezionati(final List<MasterDocumentRedDTO> documentiSelezionati) {
		this.documentiSelezionati = documentiSelezionati;
	}

	/**
	 * Restituisce il motivo dell'assegnazione.
	 * 
	 * @return motivo assegnazione
	 */
	public String getMotivoAssegnazione() {
		return motivoAssegnazione;
	}

	/**
	 * Imposta il motivo dell'assegnazione.
	 * 
	 * @param motivoAssegnazione
	 */
	public void setMotivoAssegnazione(final String motivoAssegnazione) {
		this.motivoAssegnazione = motivoAssegnazione;
	}

	/**
	 * Restituisce il session bean.
	 * 
	 * @return session bean
	 */
	public SessionBean getSb() {
		return sb;
	}

	/**
	 * Restituisce il ListaDocumentiBean
	 * 
	 * @return listaDocumentiBean
	 */
	public ListaDocumentiBean getLdb() {
		return ldb;
	}

	/**
	 * Imposta il sessionBean.
	 * 
	 * @param sb
	 */
	public void setSb(final SessionBean sb) {
		this.sb = sb;
	}

	/**
	 * Imposta il listaDocumentiBean.
	 * 
	 * @param ldb
	 */
	public void setLdb(final ListaDocumentiBean ldb) {
		this.ldb = ldb;
	}

	/**
	 * Restituisce l'utente.
	 * 
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'utente.
	 * 
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Restituisce il service che gestisce l'organigramma.
	 * 
	 * @return service organigramma
	 */
	public IOrganigrammaFacadeSRV getOrganigrammaSRV() {
		return organigrammaSRV;
	}

	/**
	 * Imposta il service che gestisce organigramma.
	 * 
	 * @param organigrammaSRV
	 */
	public void setOrganigrammaSRV(final IOrganigrammaFacadeSRV organigrammaSRV) {
		this.organigrammaSRV = organigrammaSRV;
	}

	/**
	 * Restituisce il bean che gestisce il l'assegnazione multipla.
	 * 
	 * @return bean multi assegnazione
	 */
	public MultiAssegnazioneBean getAssegnazioneBean() {
		return assegnazioneBean;
	}

	/**
	 * Imposta il bean che gestisce il l'assegnazione multipla.
	 * 
	 * @param assegnazioneBean
	 */
	public void setAssegnazioneBean(final MultiAssegnazioneBean assegnazioneBean) {
		this.assegnazioneBean = assegnazioneBean;
	}

	/**
	 * Restituisce il service per le richieste visto.
	 * 
	 * @return service richieste visto
	 */
	public IRichiesteVistoFacadeSRV getRichiesteVistoSRV() {
		return richiesteVistoSRV;
	}

	/**
	 * Imposta il service per le richieste visto.
	 * 
	 * @param richiesteVistoSRV
	 */
	public void setRichiesteVistoSRV(final IRichiesteVistoFacadeSRV richiesteVistoSRV) {
		this.richiesteVistoSRV = richiesteVistoSRV;
	}

	/**
	 * Restituisce la root del tree.
	 * 
	 * @return root
	 */
	public TreeNode getRoot() {
		return root;
	}

	/**
	 * Imposta la root del tree.
	 * 
	 * @param root
	 */
	public void setRoot(final TreeNode root) {
		this.root = root;
	}
}
