package it.ibm.red.web.mbean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.ComuneDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.ProvinciaDTO;
import it.ibm.red.business.dto.RegioneDTO;
import it.ibm.red.business.dto.RicercaRubricaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.RubricaChiamanteEnum;
import it.ibm.red.business.enums.TipoOperazioneEnum;
import it.ibm.red.business.enums.TipoRubricaEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.NotificaContatto;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.service.facade.IRubricaFacadeSRV;
import it.ibm.red.business.utils.CollectionUtils;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.mbean.component.RegioniProvinceComuniComponent;
import it.ibm.red.web.document.mbean.component.RubricaComponent;
import it.ibm.red.web.enums.ModeRubricaEnum;
import it.ibm.red.web.enums.NavigationTokenEnum;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.master.IRubricaHandler;

/**
 * Bean rubrica.
 */
@Named(ConstantsWeb.MBean.RUBRICA_BEAN)
@ViewScoped
public class RubricaBean extends AbstractRubricaBean {

	private static final String GRUPPO_UPPER = "GRUPPO";

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RubricaBean.class.getName());

	/**
	 * Path recupero ricRubrica_RG dal centralSectionForm.
	 */
	private static final String RIC_RUBRICA_RG_RESOURCE_FULL_PATH = "centralSectionForm:RubricaTab:ricRubrica_RG";

	/**
	 * Path recupero prefRubrica_RG dal centralSectionForm.
	 */
	private static final String PREF_RUBRICA_RG_RESOURCE_FULL_PATH = "centralSectionForm:RubricaTab:prefRubrica_RG";

	/**
	 * Path recupero ricRubrica_RG dal rubricaTab.
	 */
	private static final String RIC_RUBRICA_RG_RESOURCE_PATH = "RubricaTab:ricRubrica_RG";

	/**
	 * Path recupero prefRubrica_RG dal rubricaTab.
	 */
	private static final String PREF_RUBRICA_RG_RESOURCE_PATH = "RubricaTab:prefRubrica_RG";

	/**
	 * Messaggio di successo nella modifica del gruppo.
	 */
	private static final String SUCCESS_MODIFICA_GRUPPO_MSG = "Gruppo modificato con successo";

	/**
	 * Label gruppo.
	 */
	private static final String GRUPPO = "Gruppo ";

	/**
	 * Messaggio errore nota obbligatoria mancante.
	 */
	private static final String ERROR_NOTA_OBBLIGATORIA_MANCANTE_MSG = "E' obbligatorio inserire una nota";

	/**
	 * Datatable contatti preferiti.
	 */
	private SimpleDetailDataTableHelper<Contatto> contattiPreferitiDTH;

	/**
	 * Datatable contatti ricerca.
	 */
	private SimpleDetailDataTableHelper<Contatto> contattiRicercaDTH;

	/**
	 * Datatable contatti modifica.
	 */
	private SimpleDetailDataTableHelper<Contatto> contattiModificaDTH;

	/**
	 * Datatable contatti gruppo.
	 */
	private SimpleDetailDataTableHelper<Contatto> contattiNelGruppoDTH; // contatti presenti nel gruppo selezionato

	/**
	 * Datatable contatti gruppo creazione.
	 */
	private SimpleDetailDataTableHelper<Contatto> contattiNelGruppoCrazioneDTH; // contatti presenti nel gruppo che si sta creando

	/**
	 * Datatable gruppi contatti.
	 */
	private SimpleDetailDataTableHelper<Contatto> gruppiDTH; // gruppi trovati nel tab modifica di gruppi

	/**
	 * Datatable contatti scambiati tra pagine.
	 */
	private SimpleDetailDataTableHelper<Contatto> contattiDaAltrePagineDTH; // contatti scambiati tra la pagina chiamante e la rubrica

	/**
	 * Datatable notifiche richieste.
	 */
	private SimpleDetailDataTableHelper<NotificaContatto> notificheRichiesteDTH;

	/**
	 * Contatto inserimento.
	 */
	private Contatto inserisciContattoItem;

	/**
	 * Contatto modifica.
	 */
	private Contatto modificaContattoItem;

	/**
	 * Contatto elimina.
	 */
	private Contatto eliminaContattoItem;

	/**
	 * Modifica gruppo contatti.
	 */
	private Contatto modificaGruppoItem;

	/**
	 * Contatto precedente.
	 */
	private Contatto vecchioContatto;

	/**
	 * Contatto da inserire nel gruppo.
	 */
	private Contatto inserisciGruppo;

	/**
	 * Flag selezionato..
	 */
	private boolean selFlag;

	/**
	 * Numero di giorni.
	 */
	private int numGiorni;

	/**
	 * Numero di elementi.
	 */
	private int numElementi;
	
	/**
	 * Informazioni utente.
	 */
	private UtenteDTO utente;

	/**
	 * Nota rihiesta modifica.
	 */
	private String notaRichiestaModifica;

	/**
	 * Nota richiesta eliminazione.
	 */
	private String notaRichiestaEliminazione;

	/**
	 * Nota richiesta creazione.
	 */
	private String notaRichiestaCreazione;

	/**
	 * Lista contatti da aggiungere.
	 */
	private List<Contatto> contattiDaAggiungere;

	/**
	 * Lista contatti da rimuovere.
	 */
	private List<Contatto> contattiDaRimuovere;

	/**
	 * Numero giorni notifiche.
	 */
	private int numGiorniNotifiche;

	/**
	 * Numero elementi notifiche.
	 */
	private int numElementiNotifiche;

	/**
	 * Flag provenienza da altra pagina.
	 */
	private boolean chiamataDaAltraPagina;

	/**
	 * Rubrica chiamante.
	 */
	private RubricaChiamanteEnum rubricaChiamante;

	/**
	 * Handler rubrica.
	 */
	private IRubricaHandler beanMasterName;

	/**
	 * Organigramma.
	 */
	private DefaultTreeNode rootOrganigrammaComune;

	/**
	 * Creazione organigrammi.
	 */
	private IOrganigrammaFacadeSRV organigrammaSRV;

	/**
	 * Tipi di ricerca.
	 */
	private boolean ricercaRED;

	/**
	 * Flag ufficio.
	 */
	private boolean ufficioFlag;

	/**
	 * Ufficio/Personale.
	 */
	private String ufficioOPersonale;

	/**
	 * Flag aggiungi contatto a gruppo.
	 */
	private boolean aggiungiContattoAGruppoFlag;

	/**
	 * Permesso utente.
	 */
	private boolean permessoUtente;

	/**
	 * Indica se il nuovo contatto verrà aggiunto ai preferiti.
	 */
	private boolean addPreferiti;

	/**
	 * Indica l'operazione da effettuare nel tab gestione gruppo(crea o modifica).
	 */
	private String modeGruppi;

	/**
	 * Indica quali campi del form renderizzare in base alla variabile mode: se true
	 * siamo in modalita di creazione contatto se false siamo in modalità di
	 * modifica/eliminazione contatto.
	 */
	private boolean modeFlag;

	/**
	 * Flag gruppi.
	 */
	private boolean modeGruppiFlag;

	/**
	 * Flag ricerca gruppi.
	 */
	private boolean ricercaGruppoGruppi;

	/**
	 * Tipo operazione.
	 */
	private TipoOperazioneEnum operazioneEnum;

	/**
	 * Modalità rubrica.
	 */
	private ModeRubricaEnum rubricaModeEnum;

	/**
	 * Flag ricerca effettuata.
	 */
	private boolean ricercaEffettuataFlag;

	/**
	 * Viene utilizzato per controllare che il gruppo non rimanga vuoto alla
	 * modifica del gruppo.
	 */
	/**
	 * Numero contatti nel gruppo.
	 */
	private Integer numContattiNelGruppo;

	/**
	 * Flag mostra contatti interni.
	 */
	private boolean showContattiInterni;

	/**
	 * Session bean.
	 */
	private SessionBean sBean;

	/**
	 * LABERATURA DA ESPLODERE START
	 ***********************************************************************************/
	/**
	 * Alberatura nodi.
	 */
	private List<Nodo> alberaturaNodi;
	/**
	 * LABERATURA DA ESPLODERE END
	 *************************************************************************************/

	/**
	 * Messaggio rubrica da altra pagina.
	 */
	private String messaggioRubricaDaAltraPagina;

	/**
	 * Nome del form.
	 */
	private String formName;

	/**
	 * Indice attivo.
	 */
	private Integer activeIndex;

	/**
	 * Flag considera gruppi.
	 */
	private boolean consideraGruppi;

	/**
	 * Query di ricerca.
	 */
	private String queryRicerca;

	/**
	 * Radice albero.
	 */
	private DefaultTreeNode root;

	/**
	 * Lista contatti nel gruppo.
	 */
	private List<Contatto> contattiNelGruppo;

    /**
	 * Flag crea nuovo gruppo.
	 */
	private boolean creaNuovoGruppoClicked;

	/**
	 * Contatto da aggiungere al gruppo.
	 */
	private Contatto contattoDaAggiungereInGruppi;

	/**
	 * Flag azione singola.
	 */
	private boolean singleAction;

	/**
	 * Flag modifica.
	 */
	private boolean inModifica;

	/**
	 * Flag.
	 */
	private boolean disabiitaDeleteGruppiContNonUff;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		sBean = FacesHelper.getManagedBean(Constants.ManagedBeanName.SESSION_BEAN);
		sBean.setRenderRubrica(true);
		initRubrica();
	}

	/**
	 * Inizializza la rubrica.
	 */
	public void initRubrica() {
		initRubrica(true);
	}

	/**
	 * Inizializza la rubrica con la possibilita di caricamento dei contatti
	 * preferiti.
	 * 
	 * @param caricaPreferiti
	 */
	public void initRubrica(final boolean caricaPreferiti) {
		utente = sBean.getUtente();
		setRubSRV(ApplicationContextProvider.getApplicationContext().getBean(IRubricaFacadeSRV.class));
		organigrammaSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
		numGiorniNotifiche = 30;
		numElementiNotifiche = 10;
		numGiorni = 30;
		numElementi = 10;
		inserisciContattoItem = new Contatto();
		inserisciContattoItem.setIsAddTable(true);
		ufficioFlag = true;
		ufficioOPersonale = "Ufficio";

		ricercaRED = true;

		// Per tab gestione contatti
		addPreferiti = false;
		modeFlag = true;
		selFlag = false;
		modeGruppiFlag = true;
		ricercaGruppoGruppi = false;

		setContattoConsiderato(new Contatto());

		contattiDaAggiungere = new ArrayList<>();
		contattiDaRimuovere = new ArrayList<>();
		numContattiNelGruppo = 0;
		aggiungiContattoAGruppoFlag = true;

		inserisciGruppo = new Contatto();
		modificaContattoItem = new Contatto();
		modificaContattoItem.setRegioneObj(new RegioneDTO());
		modificaContattoItem.setComuneObj(new ComuneDTO());
		modificaContattoItem.setProvinciaObj(new ProvinciaDTO());
		inserisciContattoItem.setRegioneObj(new RegioneDTO());
		inserisciContattoItem.setComuneObj(new ComuneDTO());
		inserisciContattoItem.setProvinciaObj(new ProvinciaDTO());
		vecchioContatto = new Contatto();
		modificaGruppoItem = new Contatto();

		ricercaEffettuataFlag = false;

		// Il check del permessoUtente viene fatto all'interno
		loadTabNotificheRichieste();

		chiamataDaAltraPagina = false;

		alberaturaNodi = organigrammaSRV.getAlberaturaBottomUp(utente.getIdAoo(), utente.getIdUfficio());
		contattiNelGruppoCrazioneDTH = new SimpleDetailDataTableHelper<>(new ArrayList<>());
		rootOrganigrammaComune = loadRootOrganigrammaComune();

		gruppiDTH = new SimpleDetailDataTableHelper<>(new ArrayList<>());

		if (caricaPreferiti) {
			try {
				final Collection<Contatto> prefs = loadPreferiti();
				contattiPreferitiDTH = new SimpleDetailDataTableHelper<>(prefs);
			} catch (final Exception e) {
				showError("Si è verificato un errore nel caricamento dei contatti preferiti");
				LOGGER.error(e);
			}
		}

		activeIndex = 0;
		creaNuovoGruppoClicked = false;
	}

	private Collection<Contatto> loadPreferiti() {
		Collection<Contatto> preferiti = null;

		try {
			final boolean ricercaGruppi = consideraGruppi || !chiamataDaAltraPagina;
			preferiti = getRubSRV().getPreferiti(utente.getIdUfficio(), utente.getIdAoo(), ricercaGruppi);

			if (chiamataDaAltraPagina && contattiDaAltrePagineDTH != null && contattiDaAltrePagineDTH.getMasters() != null && preferiti != null) {
				for (final Contatto cont : contattiDaAltrePagineDTH.getMasters()) {
					for (final Contatto contPref : preferiti) {
						boolean selected = false;
						
						if (cont.getAliasContatto() != null && cont.getAliasContatto().startsWith("<")) {
							// gruppo
							if (contPref.getAliasContatto() != null
									&& cont.getAliasContatto().substring(1, cont.getAliasContatto().indexOf('>')).trim().equals(contPref.getAliasContatto().trim())
									&& TipoRubricaEnum.GRUPPO.equals(contPref.getTipoRubricaEnum())) {
								contPref.setSelected(true);
								selected = true;
							}
						} else if (contPref.getContattoID().equals(cont.getContattoID())) {
							contPref.setSelected(true);
							selected = true;
						}
						
						if (selected) {
							break;
						}
					}
				}
				 
			}
			if(NavigationTokenEnum.RUBRICA.equals(sBean.getActivePageInfo())) {

				for(Contatto contPref : preferiti) {
					contPref.setDisabiitaDeleteGruppiNonUff(false);
					if(!GRUPPO_UPPER.equals(contPref.getTipoRubrica())) {
						break;
					}

					if(!utente.getIdUfficio().equals(contPref.getNodoPreferiti())) {
						contPref.setDisabiitaDeleteGruppiNonUff(true);
					}
				}
			}
			LOGGER.info("Lista Preferiti per l'utente " + utente.getUsername() + " caricata con successo");
		} catch (final Exception e) {
			LOGGER.error("Errore durante il caricamento dei preferiti", e);
		}

		return preferiti;
	}

	/**
	 * Gestisce la selezione della riga del datatable dei contatti.
	 * 
	 * @param se
	 */
	public void rowSelectorContatto(final SelectEvent se) {
		contattiModificaDTH.rowSelector(se);
		impostaContattoDaModificare(contattiModificaDTH.getCurrentDetail());
	}

	/**
	 * Gestisce i parametri per la preparazione della modifica del contatto.
	 * 
	 * @param event
	 */
	public void clickOnModificaContatto(final ActionEvent event) {
		final Contatto c = getDataTableClickedRow(event);
		modificaContattoItem.copyFromContatto(c);
		inModifica = true;
		inserisciContattoItem.copyFromContatto(modificaContattoItem);
	}

	/**
	 * Gestisce i parametri per la preparazione dell'eliminazione del contatto.
	 * 
	 * @param event
	 */
	public void impostaContattoDaEliminare(final ActionEvent event) {
		final Contatto c = getDataTableClickedRow(event);
		eliminaContattoItem = new Contatto();
		eliminaContattoItem.copyFromContatto(c);
		notaRichiestaEliminazione = "";
	}

	/**
	 * Prepara alla modifica del contatto selezionato.
	 */
	private void impostaContattoDaModificare(final Contatto cont) {
		modificaContattoItem.copyFromContatto(cont);
		vecchioContatto.copyFromContatto(cont);
	}

	/**
	 * Gestisce la selezione della riga del datatable dei gruppi.
	 * 
	 * @param se
	 */
	public void rowSelectorGruppo(final SelectEvent se) {
		gruppiDTH.rowSelector(se);
		selectGroupDetail();
	}

	private void selectGroupDetail() {
		modificaGruppoItem.copyFromContatto(gruppiDTH.getCurrentDetail());
		ricercaContattiNelGruppo();
		numContattiNelGruppo = contattiNelGruppoDTH.getMasters().size();
		selFlag = true;
	}

	/**
	 * Gestisce tutte le checkbox di {@link #contattiNelGruppoDTH}.
	 */
	public void handleAllCheckBoxContattiNelGruppo() {
		handleAllCheckBox(contattiNelGruppoDTH);
	}

	/**
	 * Gestisce tutte le checkbox di {@link #gruppiDTH}.
	 */
	public void handleAllCheckBoxGruppi() {
		handleAllCheckBox(gruppiDTH);
	}

	/**
	 * Gestisce tutte le checkbox di {@link #contattiRicercaDTH}.
	 */
	public void handleAllCheckBoxRicerca() {
		handleAllCheckBox(contattiRicercaDTH);
		if (chiamataDaAltraPagina) {
			for (final Contatto cont : contattiRicercaDTH.getMasters()) {
				addRemoveDestinatario(cont);
			}
		}

	}

	/**
	 * Gestisce tutte le checkbox di {@link #contattiPreferitiDTH}.
	 */
	public void handleAllCheckBoxPreferiti() {
		handleAllCheckBox(contattiPreferitiDTH, true);
		if (chiamataDaAltraPagina) {
			for (final Contatto cont : contattiPreferitiDTH.getMasters()) {
				addRemoveDestinatario(cont);
			}
		}

	}

	private void handleAllCheckBox(final SimpleDetailDataTableHelper<Contatto> dth) {
		handleAllCheckBox(dth, false);
	}

	private void handleAllCheckBox(final SimpleDetailDataTableHelper<Contatto> dth, final boolean skipGroup) {

		if (dth != null) {

			Boolean elementCheckbox = dth.getCheckAll();

			if (elementCheckbox == null) {
				elementCheckbox = false;
			}
			if (dth.getMasters() != null && !dth.getMasters().isEmpty()) {
				if (!elementCheckbox) {
					elementCheckbox = true;
				} else {
					elementCheckbox = false;
				}
				dth.getSelectedMasters().clear();
				for (final Contatto m : dth.getMasters()) {
					if (skipGroup) {
						if (!GRUPPO_UPPER.equals(m.getTipoRubrica())) {
							m.setSelected(elementCheckbox);
							if (elementCheckbox) {
								dth.getSelectedMasters().add(m);
							}
						}
					} else {
						m.setSelected(elementCheckbox);
						if (elementCheckbox) {
							dth.getSelectedMasters().add(m);
						}
					}
				}
			}
			dth.setCheckAll(elementCheckbox);
		}
	}

	/**
	 * Aggiorna i check del {@link #gruppiDTH}.
	 * 
	 * @param event
	 */
	public void updateCheckGruppi(final AjaxBehaviorEvent event) {
		updateCheckAggContGruppo(event, gruppiDTH, null);
	}

	/**
	 * Aggiorna i check del {@link #contattiNelGruppoDTH}.
	 * 
	 * @param event
	 */
	public void updateCheckContattiNelGruppo(final AjaxBehaviorEvent event) {
		updateCheck(event, contattiNelGruppoDTH);
	}

	/**
	 * Aggiorna i check del {@link #contattiRicercaDTH}.
	 * 
	 * @param event
	 */
	public void updateCheckRicerca(final AjaxBehaviorEvent event) {
		final Contatto contattoSelected = updateCheck(event, contattiRicercaDTH);
		if (chiamataDaAltraPagina) {
			addRemoveDestinatario(contattoSelected);
		}

	}

	private Contatto updateCheck(final AjaxBehaviorEvent event, final SimpleDetailDataTableHelper<Contatto> dth) {

		final Contatto masterChecked = getDataTableClickedRow(event);
		final Boolean newValue = ((SelectBooleanCheckbox) event.getSource()).isSelected();
		if (newValue != null && newValue) {
			dth.getSelectedMasters().add(masterChecked);
		} else {
			dth.getSelectedMasters().remove(masterChecked);
		}

		return masterChecked;
	}

	private void updateCheckAggContGruppo(final AjaxBehaviorEvent event, final SimpleDetailDataTableHelper<Contatto> dth,
			final SimpleDetailDataTableHelper<Contatto> dthDestinazione) {

		final Contatto masterChecked = getDataTableClickedRow(event);
		final Boolean newValue = ((SelectBooleanCheckbox) event.getSource()).isSelected();
		if (newValue != null && newValue) {
			dth.getSelectedMasters().add(masterChecked);

			if (dthDestinazione != null) {
				boolean aggiungi = true;
				for (final Contatto contattoNeiMaster : dthDestinazione.getMasters()) {
					if (masterChecked.getContattoID().longValue() == contattoNeiMaster.getContattoID().longValue()) {
						aggiungi = false;
						break;
					}
				}
				if (aggiungi) {
					contattiDaAggiungere.add(masterChecked);
					dthDestinazione.getMasters().add(masterChecked);
				}
			}

		} else {
			dth.getSelectedMasters().remove(masterChecked);
			if (dthDestinazione != null) {
				Contatto contattoToRemove = null;
				for (final Contatto contattoNeiMaster : dthDestinazione.getMasters()) {
					if (masterChecked.getContattoID().longValue() == contattoNeiMaster.getContattoID().longValue()) {
						contattoToRemove = contattoNeiMaster;
						break;
					}
				}
				if (contattoToRemove != null) {
					contattiDaRimuovere.add(contattoToRemove);
					dthDestinazione.getMasters().remove(contattoToRemove);
				}
			}

		}
	}

	/**
	 * Gestisce la rimozione dai preferiti del contatto identificato dalla riga
	 * selezionata sul datatalbe. La rimozione viene fatta contestualmente alla
	 * rimozione logica dai {@link #contattiPreferitiDTH}, sia sui master che sui
	 * filtrati.
	 * 
	 * @param event
	 */
	public void rimuoviPreferito(final ActionEvent event) {
		try {
			final Contatto contatto = getDataTableClickedRow(event);

			getRubSRV().rimuoviPreferito(utente.getIdUfficio(), contatto.getContattoID(), contatto.getTipoRubricaEnum());

			contatto.setIsPreferito(false);
			// Rimuovo dalla lista dei preferiti
			// Rimuovo anche sui filtrati altrimenti avrei inconsistenze
			if (contattiPreferitiDTH != null) {
				Contatto contattoDaRimuovere = null;
				final Collection<Contatto> listaContattiPreferiti = contattiPreferitiDTH.getMasters();
				for (final Contatto pref : listaContattiPreferiti) {
					if (pref.getContattoID().equals(contatto.getContattoID())) {
						contattoDaRimuovere = pref;
					}
				}
				contattiPreferitiDTH.getMasters().remove(contattoDaRimuovere);
				clearFilterDataTable(PREF_RUBRICA_RG_RESOURCE_PATH, contattiPreferitiDTH);

			}

			// Aggiorno il flag se il contatto è stato già ricercato
			if (contattiRicercaDTH != null) {
				final Collection<Contatto> listaContattiRicercati = contattiRicercaDTH.getMasters();
				for (final Contatto pref : listaContattiRicercati) {
					if (pref.getContattoID().equals(contatto.getContattoID())) {
						pref.setIsPreferito(false);
					}
				}
			}
			// Aggiorno il flag se il contatto è stato cercato per la modifica
			if (contattiModificaDTH != null) {
				final Collection<Contatto> listaContattiModifica = contattiModificaDTH.getMasters();
				for (final Contatto pref : listaContattiModifica) {
					if (pref.getContattoID().equals(contatto.getContattoID())) {
						pref.setIsPreferito(false);
					}
				}
			}

			LOGGER.info("Preferito con id= " + contatto.getContattoID() + " per l'utente " + utente.getUsername() + " rimosso con successo");
		} catch (final Exception e) {
			LOGGER.error("Errore durante la rimozione del contatto/gruppo dai preferiti", e);
			showError(e);
		}
	}

	/**
	 * Gestisce l'aggiunta ai preferiti del contatto identificato dalla riga
	 * selezionata dal datatable.
	 * 
	 * @param event
	 */
	public void aggiungiAiPreferiti(final ActionEvent event) {
		try {
			final Contatto contatto = (Contatto) getDataTableClickedRow(event);
			final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			final String strDate = sdf.format(new Date());
			contatto.setUtenteCheHaAggiuntoAiPreferiti(utente.getNome() + " " + utente.getCognome());
			final Date dataAggiunta = DateUtils.parseDate(strDate);
			contatto.setDataDiAggiuntaAiPreferiti(dataAggiunta);
			aggiungiAiPreferiti(contatto);
		} catch (final Exception e) {
			LOGGER.error("Errore durante la rimozione del contatto/gruppo dai preferiti", e);
			showError(e);
		}
	}

	private void aggiungiAiPreferiti(final Contatto contatto) {
		getRubSRV().aggiungiAiPreferiti(utente.getIdUfficio(), contatto.getContattoID(), contatto.getTipoRubricaEnum(), utente.getId());

		// Aggiungo alla lista dei preferiti
		contatto.setIsPreferito(true);
		if (contattiPreferitiDTH != null) {
			contattiPreferitiDTH.getMasters().add(contatto);
		}
		// Aggiorno il flag se il contatto è stato già ricercato
		if (contattiRicercaDTH != null) {
			final Collection<Contatto> listaContattiRicercati = contattiRicercaDTH.getMasters();
			for (final Contatto pref : listaContattiRicercati) {
				if (pref.getContattoID().longValue() == contatto.getContattoID().longValue()) {
					pref.setIsPreferito(true);
				}
			}
		}
		// Aggiorno il flag se il contatto è stato cercato per la modifica
		if (contattiModificaDTH != null) {
			final Collection<Contatto> listaContattiModifica = contattiModificaDTH.getMasters();
			for (final Contatto pref : listaContattiModifica) {
				if (pref.getContattoID().longValue() == contatto.getContattoID().longValue()) {
					pref.setIsPreferito(true);
				}
			}
		}

		clearFilterDataTable(PREF_RUBRICA_RG_RESOURCE_PATH, contattiPreferitiDTH);

		//
		LOGGER.info("Contatto con id= " + contatto.getContattoID() + " per l'utente " + utente.getUsername() + " aggiunto con successo ai preferiti");
	}

	private SimpleDetailDataTableHelper<Contatto> ricerca(final RicercaRubricaDTO contatto) { // (ricercaContattoObj, contattiRicerca)
		SimpleDetailDataTableHelper<Contatto> dth = new SimpleDetailDataTableHelper<>();
		resetDataTableRubricaToDefaultParameters(RIC_RUBRICA_RG_RESOURCE_PATH);
		try {
			cleanDTH(dth);
			contatto.setIdAoo(Integer.parseInt("" + utente.getIdAoo()));

			dth = new SimpleDetailDataTableHelper<>(getRubSRV().ricerca(utente.getIdUfficio(), contatto));

			LOGGER.info("Ricerca eseguita con successo");
		} catch (final Exception e) {
			LOGGER.error("Errore durante la ricerca", e);
			showError(e);
		}
		return dth;
	}

	/**
	 * Gestisce la ricerca del contatto.
	 */
	public void ricercaContattoNew() {
		resetDataTableRubrica(PREF_RUBRICA_RG_RESOURCE_FULL_PATH);
		SimpleDetailDataTableHelper<Contatto> ric = new SimpleDetailDataTableHelper<>();
		if (!StringUtils.isNullOrEmpty(queryRicerca)) {
			cleanDTH(ric);
			List<Contatto> listContattiRicerca = null;
			if (ricercaRED) {
				final boolean ricercaMEF = true;
				final boolean ricercaIPA = false;
				listContattiRicerca = getRubSRV().ricercaCampoSingolo(queryRicerca, utente.getIdUfficio(), utente.getIdAoo().intValue(), ricercaRED, ricercaIPA, ricercaMEF,
						false, "");
			} else {
				final boolean ricercaMEF = false;
				final boolean ricercaIPA = true;
				listContattiRicerca = getRubSRV().ricercaCampoSingolo(queryRicerca, utente.getIdUfficio(), utente.getIdAoo().intValue(), ricercaRED, ricercaIPA, ricercaMEF,
						false, "");
			}
			ric = new SimpleDetailDataTableHelper<>(listContattiRicerca);
			if (ric.getMasters() != null) {
				setContattiRicercaDTH(ric);

				if (!ricercaEffettuataFlag) {
					ricercaEffettuataFlag = true;
					resetAllCheckBox(contattiPreferitiDTH);
				}

			}
		}

		if (chiamataDaAltraPagina && contattiDaAltrePagineDTH != null && contattiDaAltrePagineDTH.getMasters() != null && ric != null && ric.getMasters() != null) {
			for (final Contatto cont : contattiDaAltrePagineDTH.getMasters()) {
				for (final Contatto contPref : ric.getMasters()) {
					boolean selected = false;

					if (cont.getAliasContatto() != null && cont.getAliasContatto().startsWith("<")) {
						// gruppo
						if (contPref.getAliasContatto() != null
								&& cont.getAliasContatto().substring(1, cont.getAliasContatto().indexOf(">")).trim().equals(contPref.getAliasContatto().trim())
								&& TipoRubricaEnum.GRUPPO.equals(contPref.getTipoRubricaEnum())) {
							
							contPref.setSelected(true);
							selected = true;
						}
					} else {
						if (contPref.getContattoID().equals(cont.getContattoID())) {
							
							contPref.setSelected(true);
							selected = true;
						}
					}
					
					if (selected) {
						break;
					}
				}

			}

		}
	}

	/**
	 * Esegue la ricerca dei contatti per modifica.
	 */
	public void ricercaModificaContattoRed() {
		try {
			cleanDTH(contattiModificaDTH);
			final List<Contatto> ricercaContattiPerModifica = getRubSRV().ricercaCampoSingolo(queryRicerca, utente.getIdUfficio(), utente.getIdAoo().intValue(), true, false,
					true, false, "15", null, null, true);
			contattiModificaDTH = new SimpleDetailDataTableHelper<>(ricercaContattiPerModifica);
			ricercaEffettuataFlag = true;

			LOGGER.info("Ricerca eseguita con successo");
		} catch (final Exception e) {
			LOGGER.error("Errore durante la ricerca", e);
			showError(e);
		}
	}

	/**
	 * Gestisce la modifica del contatto {@link #modificaContattoItem}.
	 */
	public void modificaContatto() {
		try {
			modificaContattoItem = new Contatto();
			modificaContattoItem.copyFromContatto(inserisciContattoItem);
			final boolean modificaContatto = valida(modificaContattoItem);

			if (modificaContatto) {
				modificaContattoItem.setIdAOO(utente.getIdAoo());
				modifica();
				modificaContattoItem = new Contatto();
				modificaContattoItem.setRegioneObj(new RegioneDTO());
				modificaContattoItem.setComuneObj(new ComuneDTO());
				modificaContattoItem.setProvinciaObj(new ProvinciaDTO());
			}
		} catch (final Exception ex) {
			final String errorCode = "MAILESISTENTE";
			if (ex.getMessage() != null && ex.getMessage().contains(errorCode)) {
				showError("Un contatto con la stessa email è già presente all'interno del sistema per questa AOO");
			} else {
				showError(ex);
			}
		}
	}

	/**
	 * Gestisce l'evento di selezione dell'item.
	 * 
	 * @param event
	 */
	public void onItemSelect(final SelectEvent event) {
		// non fa nulla passando dal server si aggiornano i campi
	}

	/**
	 * Esegue la creazione del contatto dalle informazioni recuperati da
	 * {@link #modificaContattoItem}.
	 */
	public void creaDaContatto() {
		try {
			final boolean creaContatto = valida(modificaContattoItem);

			// Necessario nel caso in cui l'utente nello stesso form decida prima di creare
			// un nuovo contatto
			// e poi senza cliccare una nuova riga effettui una nuova modifica e prema il
			// tasto "modifica"
			final Contatto contattoDaCreare = new Contatto();
			contattoDaCreare.copyFromContatto(modificaContattoItem);
			if (permessoUtente && creaContatto) {

				// su Red viene sempre creato come non preferito.
				contattoDaCreare.setIsPreferito(false);
				inserisci(contattoDaCreare);

				modificaContattoItem = new Contatto();
				modificaContattoItem.setRegioneObj(new RegioneDTO());
				modificaContattoItem.setComuneObj(new ComuneDTO());
				modificaContattoItem.setProvinciaObj(new ProvinciaDTO());
			} else if (!permessoUtente && creaContatto) {
				final boolean isRichiestaCreazione = true;
				if (utente.getCheckUnivocitaMail()) {
					getRubSRV().checkMailPerContattoGiaPresentePerAoo(contattoDaCreare, false);
				}
				getRubSRV().inserisciContattoPerRichiestaCreazione(contattoDaCreare, isRichiestaCreazione);
				showInfoMessage("Richiesta di creazione contatto inviata");
			}
			notaRichiestaCreazione = "";
		} catch (final Exception ex) {
			showError(ex);
		}
	}

	private void inserisci(Contatto contatto) {
		try {
			contatto.setIdAOO(utente.getIdAoo());
			// si può solo inserire un contatto sulla rubrica RED
			// viene automaticamente considerato pubblico
			contatto.setTipoRubrica("RED");
			contatto.setPubblico(1);
			contatto.setDatacreazione(new Date());
			Long idUfficio = null;
			if (contatto.getIsPreferito() != null && contatto.getIsPreferito()) {
				idUfficio = utente.getIdUfficio();
			}
			getRubSRV().inserisci(idUfficio, contatto, utente.getCheckUnivocitaMail());

			// Aggiungo il contatto creato ai preferiti
			if (contatto.getIsPreferito() != null && contatto.getIsPreferito()) {
				contattiPreferitiDTH.getMasters().add(contatto);
				clearFilterDataTable(PREF_RUBRICA_RG_RESOURCE_PATH, contattiPreferitiDTH);
			}
			final Contatto contattodaaggiungere = new Contatto();
			contattodaaggiungere.copyFromContatto(contatto);

			if (contatto.getIsAddTable() != null && contatto.getIsAddTable()) {
				addDestinatario(contattodaaggiungere);
			}

			showInfoMessage("Contatto Creato");
		} catch (final Exception e) {
			showError(e);

		}
	}

	private void modifica() {
		modifica(modificaContattoItem, null, utente.getCheckUnivocitaMail());
	}

	private void elimina(final Contatto contatto) {
		elimina(contatto, null, null);
	}

	/**
	 * Gestisce l'eliminazione del contatto identificato dalla riga selezionata.
	 * 
	 * @param event
	 */
	public void eliminaContatto(final ActionEvent event) {
		try {
			final Contatto contatto = (Contatto) getDataTableClickedRow(event);
			elimina(contatto);
			contattiPreferitiDTH.getMasters().remove(contatto);
		} catch (final Exception e) {
			LOGGER.error("Errore durante la cancellazione del contatto", e);
			showError(e);
		}
	}

	/**
	 * Eliminazione contatto.
	 * 
	 * @param event
	 */
	public void eliminaContattoOld(final ActionEvent event) {
		try {
			final Contatto contatto = (Contatto) getDataTableClickedRow(event);
			elimina(contatto);

			contattiModificaDTH.getMasters().remove(contatto);
			if (chiamataDaAltraPagina && contattiDaAltrePagineDTH != null && contattiDaAltrePagineDTH.getMasters() != null) {
				Contatto contDaRimuovere = null;
				for (final Contatto contattoRemove : contattiDaAltrePagineDTH.getMasters()) {
					if (contattoRemove.getContattoID().equals(contatto.getContattoID())) {
						contDaRimuovere = contattoRemove;
					}
				}
				if (contDaRimuovere != null) {
					contattiDaAltrePagineDTH.getMasters().remove(contDaRimuovere);
					clearFilterDataTable(formName + ":panelDataTableCreaContattiDaAltraPagina", contattiDaAltrePagineDTH);
				}
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante la crezione del contatto", e);
			showError(e);
		}
	}

	private void cleanDTH(final SimpleDetailDataTableHelper<Contatto> contDTH) {
		if (contDTH != null && contDTH.getMasters() != null && !contDTH.getMasters().isEmpty()) {
			contDTH.getMasters().clear();
		}
	}

	// *PRESE DA RAD (compresi SRV, DAO e ETY->DTO)
	/**
	 * Gestisce il caricamento dell regioni.
	 * 
	 * @param query
	 * @return lista regioni
	 */
	public final List<RegioneDTO> loadRegioni(final String query) {
		List<RegioneDTO> list = null;
		try {
			list = RegioniProvinceComuniComponent.loadRegioni(query);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
		return list;
	}

	/**
	 * Gestisce il caricamento dei comuni.
	 * 
	 * @param query
	 * @return lista comuni
	 */
	public final List<ComuneDTO> loadComuniAddCreazione(final String query) {

		List<ComuneDTO> list = null;
		try {
			list = RegioniProvinceComuniComponent.loadComuniAdd(query, inserisciContattoItem.getProvinciaObj());
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
		return list;

	}

	/**
	 * Gestisce il caricamento delle province.
	 * 
	 * @param query
	 * @return lista province
	 */
	public final List<ProvinciaDTO> loadProvinceAddCreazione(final String query) {

		List<ProvinciaDTO> province = null;
		try {
			province = RegioniProvinceComuniComponent.loadProvinceAdd(query);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
		return province;

	}

	/**
	 * Restituisce la lista dei comuni per l'aggiunta.
	 * 
	 * @param query
	 * @return lista comuni
	 */
	public final List<ComuneDTO> loadComuniAddCreazioneDlg(final String query) {

		List<ComuneDTO> list = null;
		try {
			list = RegioniProvinceComuniComponent.loadComuniAdd(query, super.getContattoConsiderato().getProvinciaObj());
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
		return list;

	}

	/**
	 * Restituisce la lista dei comuni per la modifica.
	 * 
	 * @param query
	 * @return lista comuni
	 */
	public final List<ComuneDTO> loadComuniAddModifica(final String query) {

		List<ComuneDTO> list = null;
		try {
			list = RegioniProvinceComuniComponent.loadComuniAdd(query, modificaContattoItem.getProvinciaObj());
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
		return list;

	}

	/**
	 * Restituisce la lista delle province per la modifica.
	 * 
	 * @param query
	 * @return lista province
	 */
	public final List<ProvinciaDTO> loadProvinceAddModifica(final String query) {

		return loadProvinceAddCreazione(query);
	}

	/**
	 * Gestisce l'invio della notifica di eliminazione mosrandone l'esito.
	 */
	public void inviaNotificaEliminazione() {
		try {
			if (StringUtils.isNullOrEmpty(notaRichiestaEliminazione)) {
				showError(ERROR_NOTA_OBBLIGATORIA_MANCANTE_MSG);
				return;
			}
			RubricaComponent.inviaNotificaEliminazione(eliminaContattoItem, notaRichiestaEliminazione, utente);
			showInfoMessage("Richiesta di eliminazione inviata con successo");

		} catch (final Exception e) {
			showError(e);
		}
	}

	/**
	 * Gestisce l'invio della notifica di creazione mostrandone l'esito.
	 */
	public void inviaNotificaCreazione() {
		try {
			if (StringUtils.isNullOrEmpty(notaRichiestaCreazione)) {
				showError(ERROR_NOTA_OBBLIGATORIA_MANCANTE_MSG);
				return;
			}
			final FacesContext context = FacesContext.getCurrentInstance();
			inserisciContattoItem.setIdAOO(utente.getIdAoo());
			RubricaComponent.inviaNotificaCreazione(inserisciContattoItem, notaRichiestaCreazione, utente, context);
			showInfoMessage("Richiesta di creazione contatto inviata");
			FacesHelper.executeJS("PF('dialogRichCreazioneVW').hide();");
			FacesHelper.executeJS("PF('creaModificaContattoDlg_WV').hide();");
			notaRichiestaCreazione = "";
		} catch (final Exception ex) {
			showError(ex);
		}
	}

	/**
	 * Gestisce l'invio della notifica di modifica mostrandone l'esito.
	 */
	public void inviaNotificaModifica() {
		try {
			if (StringUtils.isNullOrEmpty(notaRichiestaModifica)) {
				showError(ERROR_NOTA_OBBLIGATORIA_MANCANTE_MSG);
				return;
			}
			final FacesContext context = FacesContext.getCurrentInstance();

			RubricaComponent.inviaNotificaModifica(inserisciContattoItem, modificaContattoItem, notaRichiestaModifica, utente, context);

			// Il check del permessoUtente viene fatto all'interno
			loadTabNotificheRichieste();
			notaRichiestaModifica = null;
			showInfoMessage("Richiesta di modifica inviata con successo");

		} catch (final Exception e) {
			showError(e);
		}
	}
	/* END - INVIO NOTIFICA ELIMINAZIONE - CREAZIONE - MODIFICA */

	/**
	 * Effettua un aggiornamento della mail selezionata.
	 * 
	 * @param event
	 */
	public void updateMailSelected(final SelectEvent event) {
		final String mailSel = (String) event.getObject();
		final Contatto masterChecked = getDataTableClickedRow(event);
		masterChecked.setMailSelected(mailSel);
	}

	/**
	 * Rimuove il {@code contatto} dai master del datatable gestito dal
	 * {@code currentDTH}.
	 * 
	 * @param currentDTH
	 * @param contatto
	 */
	private void removeContattofromOneDataTable(final SimpleDetailDataTableHelper<Contatto> currentDTH, final Contatto contatto) {
		if (currentDTH != null && currentDTH.getMasters() != null) {
			
			final Collection<Contatto> listaMasters = currentDTH.getMasters();
			Contatto contattoDaRimuovere = null;
			
			for (final Contatto currentContatto : listaMasters) {
				final boolean entrambiInterni = currentContatto.getTipoRubricaEnum().equals(TipoRubricaEnum.INTERNO) && contatto.getTipoRubricaEnum().equals(TipoRubricaEnum.INTERNO);
				final boolean entrambiEsterni = !currentContatto.getTipoRubricaEnum().equals(TipoRubricaEnum.INTERNO) && !contatto.getTipoRubricaEnum().equals(TipoRubricaEnum.INTERNO);
				final boolean entrambiNull = currentContatto.getAliasContatto() == null && contatto.getAliasContatto() == null;
				
				if ((entrambiInterni && (entrambiNull || (currentContatto.getAliasContatto() != null && currentContatto.getAliasContatto().equalsIgnoreCase(contatto.getAliasContatto()))))
						|| (entrambiEsterni && currentContatto.getContattoID().longValue() == contatto.getContattoID().longValue())) {

					contattoDaRimuovere = currentContatto;
					break;
				}
			}
			
			if (contattoDaRimuovere != null) {
				listaMasters.remove(contattoDaRimuovere);
			}
		}
	}

	/**
	 * Rimuove il contatto identificato dal parametro <code> contatto </code> da
	 * tutti i datatable dei contatti.
	 * 
	 * @param contatto
	 *            contatto da rimuovere
	 */
	@Override
	protected void removeContattoFromAllDataTableContatti(final Contatto contatto) {
		// Preferiti
		removeContattofromOneDataTable(contattiPreferitiDTH, contatto);

		// Ricerca
		removeContattofromOneDataTable(contattiRicercaDTH, contatto);

		// Modifica
		removeContattofromOneDataTable(contattiModificaDTH, contatto);

		// Contatti presenti in un gruppo
		removeContattofromOneDataTable(contattiNelGruppoDTH, contatto);

		clearFilterDataTable(PREF_RUBRICA_RG_RESOURCE_PATH, contattiPreferitiDTH);
		clearFilterDataTable(RIC_RUBRICA_RG_RESOURCE_PATH, contattiRicercaDTH);
	}

	/**
	 * Aggiorna il contatto in un datatable.
	 * NB: Andando a modificare l'oggetto non occorre aggiornare i filteredMasters.
	 * 
	 * @param currentDTH
	 *            datatable helper del datatable da cui rimuovere il contatto
	 * @param contatto
	 *            contatto da rimuovere
	 */
	private void updateContattoinOneDataTable(final SimpleDetailDataTableHelper<Contatto> currentDTH, final Contatto contatto) {
		if (currentDTH != null) {
			final Collection<Contatto> listaMasters = currentDTH.getMasters();
			if (listaMasters != null) {
				for (final Contatto currentContatto : listaMasters) {
					if (currentContatto.getContattoID().longValue() == contatto.getContattoID().longValue()) {
						currentContatto.copyFromContatto(contatto);
					}
				}
			}
		}
	}

	/**
	 * Aggiorna il contatto in tutti i datatable.
	 * 
	 * @param contatto
	 *            contatto da aggiornare
	 */
	@Override
	protected void updateContattoInAllDataTableContatti(final Contatto contatto) {
		// Preferiti
		updateContattoinOneDataTable(contattiPreferitiDTH, contatto);

		// Ricerca
		updateContattoinOneDataTable(contattiRicercaDTH, contatto);

		// Modifica
		updateContattoinOneDataTable(contattiModificaDTH, contatto);

		// Contatti presenti in un gruppo
		updateContattoinOneDataTable(contattiNelGruppoDTH, contatto);

		if (chiamataDaAltraPagina) {
			updateContattoinOneDataTable(contattiDaAltrePagineDTH, contatto);
		}
	}

	/**
	 * Gestisce la rimozione del contatto dal gruppo in creazione.
	 * 
	 * @param contatto
	 *            contatto da rimuovere
	 */
	public void rimuoviContattoDaGruppoCreazione(final Contatto contatto) {
		removeContattofromOneDataTable(contattiNelGruppoCrazioneDTH, contatto);
		contatto.setSelected(false);
		showInfoMessage("Contatto rimosso.");
	}

	/**
	 * Gestisce la modifica del gruppo.
	 */
	public void salvaModificheGruppo() {
		if (numContattiNelGruppo == contattiDaRimuovere.size() && contattiDaAggiungere.isEmpty()) {
			showWarnMessage("Il gruppo non può essere vuoto");
			return;
		}
		try {
			getRubSRV().modificaGruppo(modificaGruppoItem, contattiDaAggiungere, contattiDaRimuovere, utente.getIdUfficio());
			LOGGER.info(GRUPPO + gruppiDTH.getCurrentMaster().getNome() + " modificato con successo");

			updateContattoinOneDataTable(gruppiDTH, modificaGruppoItem);
			contattiDaAggiungere.clear();
			contattiDaRimuovere.clear();
			setSelFlag(!selFlag);

			showInfoMessage("Gruppo modificato con successo.");
		} catch (final Exception e) {
			LOGGER.error("Errore durante la modifica del gruppo", e);
			showError(e);
		}

	}

	/**
	 * Imposta il contatto da aggiungere in: {@link #contattoDaAggiungereInGruppi}.
	 * 
	 * @param contattoSelected
	 */
	public void impostaContattoDaAggiungereInGruppi(final Contatto contattoSelected) {
		contattoDaAggiungereInGruppi = new Contatto();
		contattoDaAggiungereInGruppi.copyFromContatto(contattoSelected);
		singleAction = true;
	}

	/**
	 * Gestisce {@link #contattoDaAggiungereInGruppi} aggiungendolo ai gruppi
	 * selezionati dal datatable dei gruppi.
	 */
	public void aggiungiContattoAGruppi() {
		contattiDaRimuovere = new ArrayList<>();
		contattiDaAggiungere = new ArrayList<>();
		contattiDaAggiungere.add(contattoDaAggiungereInGruppi);
		try {
			for (final Contatto r : gruppiDTH.getSelectedMasters()) {
				getRubSRV().modificaGruppo(r, contattiDaAggiungere, contattiDaRimuovere, utente.getIdUfficio());
				LOGGER.info(SUCCESS_MODIFICA_GRUPPO_MSG);
				showInfoMessage("Contatto/i aggiunto/i nel gruppo");
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'aggiunta del contatto nel gruppo", e);
			showError(e);
		}
	}

	// per ricerca tab
	private void aggiungiEliminaContattiDaGruppi() {
		contattiDaAggiungere = new ArrayList<>();
		contattiDaRimuovere = new ArrayList<>();

		contattiDaAggiungere.add(contattoDaAggiungereInGruppi);
		contattiDaRimuovere.add(contattoDaAggiungereInGruppi);

		for (final Contatto r : gruppiDTH.getMasters()) {
			if (Boolean.TRUE.equals(r.getSelected())) {
				getRubSRV().modificaGruppo(r, contattiDaAggiungere, contattiDaRimuovere, utente.getIdUfficio());
				showInfoMessage("Contatto/i aggiunto/i nel gruppo");
			} else {
				getRubSRV().rimuoviDAGruppo(r, contattiDaRimuovere);
			}
			LOGGER.info(SUCCESS_MODIFICA_GRUPPO_MSG);
		}
	}

	/**
	 * Gestisce l'aggiunta dei {@link #contattiDaAggiungere} aggiungendoli ai gruppi
	 * selezionati dal datatable dei gruppi.
	 */
	public void aggiungiContattiaGruppi() {
		contattiDaRimuovere = new ArrayList<>();

		if (!singleAction && !ricercaEffettuataFlag) {
			contattiDaAggiungere = contattiPreferitiDTH.getSelectedMasters();
		} else if (!singleAction) {
			contattiDaAggiungere = contattiRicercaDTH.getSelectedMasters();
		}
		try {
			if (singleAction) {
				aggiungiEliminaContattiDaGruppi();
			} else {
				for (final Contatto r : gruppiDTH.getSelectedMasters()) {
					getRubSRV().modificaGruppo(r, contattiDaAggiungere, contattiDaRimuovere, utente.getIdUfficio());
					showInfoMessage(SUCCESS_MODIFICA_GRUPPO_MSG);
				}
			}
			creaNuovoGruppoClicked = false;
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'aggiunta del contatto nel gruppo", e);
			showError(e);
		}
	}

	/**
	 * Gestisce l'eliminazione del gruppo.
	 * 
	 * @param gruppo
	 */
	public void eliminaGruppo(final Contatto gruppo) {
		try {
			resetDataTableRubrica(PREF_RUBRICA_RG_RESOURCE_FULL_PATH);
			resetDataTableRubrica(RIC_RUBRICA_RG_RESOURCE_FULL_PATH);

			getRubSRV().eliminaGruppo(gruppo, utente.getIdUfficio());
			selFlag = false;
			showInfoMessage("Gruppo eliminato con successo.");
			LOGGER.info(GRUPPO + gruppo + " eliminato con successo");
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'eliminazione dl gruppo", e);
			showError(e);
		}

		removeContattofromOneDataTable(contattiPreferitiDTH, gruppo);

	}

	/**
	 * Gestisce il caricamento del tab notifiche rubrica.
	 */
	public void loadTabNotificheRubrica() {
		setNotificheRichiesteDTH(
				new SimpleDetailDataTableHelper<>(getRubSRV().getNotifiche(utente.getIdUfficio(), numGiorniNotifiche, numElementiNotifiche, utente.getUsername())));
	}

	/**
	 * Gestisce il caricamento del tab notifiche richieste.
	 */
	public void loadTabNotificheRichieste() {
		if (getPermessoUtente()) {
			setNotificheContattiDTH(new SimpleDetailDataTableHelper<>(getRubSRV().getNotificheAdmin(utente.getIdUfficio(), utente.getIdAoo(),
					NotificaContatto.TIPO_NOTIFICA_RICHIESTA, numGiorni, numElementi, utente.getId())));
		}
	}

	/**
	 * Effettua un update sulle regioni del contatto in fase di inserimento
	 * {@link #inserisciContattoItem}.
	 * 
	 * @param event
	 */
	public void updateRegioniOnSelectAddCreazione(final AjaxBehaviorEvent event) {
		if (event instanceof SelectEvent) {
			inserisciContattoItem.setRegioneObj((RegioneDTO) ((SelectEvent) event).getObject());
		} else {
			inserisciContattoItem.setRegioneObj(null);
		}
		updateRegioniOnSelectAdd(inserisciContattoItem);
	}

	/**
	 * Effettua un update sulle province del contatto in fase di creazione dlg.
	 * 
	 * @param event
	 */
	public void updateProvinceOnSelectAddCreazioneDlg(final AjaxBehaviorEvent event) {
		if (event instanceof SelectEvent) {
			super.getContattoConsiderato().setProvinciaObj((ProvinciaDTO) ((SelectEvent) event).getObject());
		} else {
			super.getContattoConsiderato().setProvinciaObj(null);
		}
		updateProvinceOnSelectAdd(super.getContattoConsiderato());
	}

	/**
	 * Effettua un update sulle province del contatto in fase di inserimento
	 * {@link #inserisciContattoItem}.
	 * 
	 * @param event
	 */
	public void updateProvinceOnSelectAddCreazione(final AjaxBehaviorEvent event) {
		if (event instanceof SelectEvent) {
			inserisciContattoItem.setProvinciaObj((ProvinciaDTO) ((SelectEvent) event).getObject());
		} else {
			inserisciContattoItem.setProvinciaObj(null);
		}
		updateProvinceOnSelectAdd(inserisciContattoItem);

	}

	/**
	 * Effettua un update sui comuni del contatto in fase di creazione dlg.
	 * 
	 * @param event
	 */
	public void updateComuniOnSelectAddCreazioneDlg(final AjaxBehaviorEvent event) {
		if (event instanceof SelectEvent) {
			super.getContattoConsiderato().setComuneObj((ComuneDTO) ((SelectEvent) event).getObject());
		} else {
			super.getContattoConsiderato().setComuneObj(null);
		}
		updateComuniOnSelectAdd(super.getContattoConsiderato());
	}

	/**
	 * Effettua un update sui comuni del contatto in fase di inserimento
	 * {@link #inserisciContattoItem}.
	 * 
	 * @param event
	 */
	public void updateComuniOnSelectAddCreazione(final AjaxBehaviorEvent event) {
		if (event instanceof SelectEvent) {
			inserisciContattoItem.setComuneObj((ComuneDTO) ((SelectEvent) event).getObject());
		} else {
			inserisciContattoItem.setComuneObj(null);
		}
		updateComuniOnSelectAdd(inserisciContattoItem);
	}

	/**
	 * Effettua un update sulle regioni del contatto in fase di modifica
	 * {@link #modificaContattoItem}.
	 * 
	 * @param event
	 */
	public void updateRegioniOnSelectAddModifica(final AjaxBehaviorEvent event) {
		if (event instanceof SelectEvent) {
			modificaContattoItem.setRegioneObj((RegioneDTO) ((SelectEvent) event).getObject());
		} else {
			modificaContattoItem.setRegioneObj(null);
		}
		updateRegioniOnSelectAdd(modificaContattoItem);

	}

	/**
	 * Effettua un update sulle province del contatto in fase di modifica
	 * {@link #modificaContattoItem}.
	 * 
	 * @param event
	 */
	public void updateProvinceOnSelectAddModifica(final AjaxBehaviorEvent event) {
		if (event instanceof SelectEvent) {
			modificaContattoItem.setProvinciaObj((ProvinciaDTO) ((SelectEvent) event).getObject());
		} else {
			modificaContattoItem.setProvinciaObj(null);
		}
		updateProvinceOnSelectAdd(modificaContattoItem);
	}

	/**
	 * Effettua un update sui comuni del contatto in fase di modifica
	 * {@link #modificaContattoItem}.
	 * 
	 * @param event
	 */
	public void updateComuniOnSelectAddModifica(final AjaxBehaviorEvent event) {
		if (event instanceof SelectEvent) {
			modificaContattoItem.setComuneObj((ComuneDTO) ((SelectEvent) event).getObject());
		} else {
			modificaContattoItem.setComuneObj(null);
		}
		updateComuniOnSelectAdd(modificaContattoItem);
	}

	// Integrazione Rubrica nelle altre pagine START
	/**
	 * Imposta i destinatari rubrica sulla rubrica chiamante.
	 * 
	 * @param inBeanMasterName
	 * @param mode
	 * @param contattiInseriti
	 * @param r
	 * @param inShowContattiInterni
	 * @param inConsideraGruppi
	 */
	public void setDestinatariRubrica(final IRubricaHandler inBeanMasterName, final ModeRubricaEnum mode, final List<Contatto> contattiInseriti, final RubricaChiamanteEnum r,
			final boolean inShowContattiInterni, final boolean inConsideraGruppi) {
		chiamataDaAltraPagina = true;
		consideraGruppi = inConsideraGruppi;
		rubricaChiamante = r;
		setContattiDaAltrePagineDTH(new SimpleDetailDataTableHelper<>(contattiInseriti));
		showContattiInterni = inShowContattiInterni;
		beanMasterName = inBeanMasterName;
		rubricaModeEnum = mode;
		contattiPreferitiDTH = new SimpleDetailDataTableHelper<>(loadPreferiti());
	}

	/**
	 * Gestisce l'inserimento o l'eliminazione del contatto selezionato alla lista
	 * dei destinatari.
	 * 
	 * @param event
	 */
	public void manageDestinatari(final AjaxBehaviorEvent event) {
		try {
			final Contatto selectedContatto = getDataTableClickedRow(event);
			addRemoveDestinatario(selectedContatto);
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento del contatto tra i destinatari", e);
			showError(e);
		}
	}

	private void addRemoveDestinatario(final Contatto selectedContatto) {
		List<Contatto> contattiDaGruppo = null;
		if (contattiDaAltrePagineDTH == null) {
			contattiDaAltrePagineDTH = new SimpleDetailDataTableHelper<>(new ArrayList<>());
		}

		if (selectedContatto.getTipoRubricaEnum().equals(TipoRubricaEnum.GRUPPO) && !rubricaChiamante.equals(RubricaChiamanteEnum.MAILBEAN_DESTINATARI)) {
			RicercaRubricaDTO ricercaContattiNelGruppo;
			ricercaContattiNelGruppo = new RicercaRubricaDTO();

			ricercaContattiNelGruppo.setRicercaRED(true);
			ricercaContattiNelGruppo.setRicercaIPA(true);
			ricercaContattiNelGruppo.setRicercaMEF(true);
			ricercaContattiNelGruppo.setRicercaGruppo(false);
			ricercaContattiNelGruppo.setIdGruppo(selectedContatto.getContattoID());
			ricercaContattiNelGruppo.setIdAoo(Integer.parseInt("" + utente.getIdAoo()));

			contattiDaGruppo = getRubSRV().ricerca(utente.getIdUfficio(), ricercaContattiNelGruppo);
		}

		if (Boolean.TRUE.equals(selectedContatto.getSelected())) {
			if (contattiDaGruppo != null) {
				for (final Contatto selCont : contattiDaGruppo) {
					String aliasContatto = "<" + selectedContatto.getNome() + ">";
					if (!StringUtils.isNullOrEmpty(selCont.getAliasContatto())) {
						aliasContatto += selCont.getAliasContatto();
					}
					selCont.setAliasContatto(aliasContatto);
					addDestinatario(selCont);
				}
			} else {
				addDestinatario(selectedContatto);
			}
		} else {
			if (contattiDaGruppo != null) {
				for (final Contatto selCont : contattiDaGruppo) {
					removeDestinatario(selCont);
				}
			} else {
				removeDestinatario(selectedContatto);
			}
		}

	}

	private void addDestinatario(final Contatto selectedContatto) {
		boolean addDestinatario = true;
		if (contattiDaAltrePagineDTH != null && contattiDaAltrePagineDTH.getMasters() != null) {
			for (final Contatto cont : contattiDaAltrePagineDTH.getMasters()) {
				final boolean entrambiInterni = TipoRubricaEnum.INTERNO.equals(cont.getTipoRubricaEnum())
						&& TipoRubricaEnum.INTERNO.equals(selectedContatto.getTipoRubricaEnum());
				final boolean entrambiEsterni = !TipoRubricaEnum.INTERNO.equals(cont.getTipoRubricaEnum())
						&& !TipoRubricaEnum.INTERNO.equals(selectedContatto.getTipoRubricaEnum());
				if (entrambiInterni) {
					if ((cont.getAliasContatto() == null && selectedContatto.getAliasContatto() == null)
							|| (cont.getAliasContatto() != null && cont.getAliasContatto().equalsIgnoreCase(selectedContatto.getAliasContatto()))) {
						addDestinatario = false;
					}
				} else if (entrambiEsterni && cont.getContattoID().longValue() == selectedContatto.getContattoID().longValue()) {
					addDestinatario = false;
				}

				if (!addDestinatario) {
					break;
				}
			}

			if (addDestinatario) {
				contattiDaAltrePagineDTH.getMasters().add(selectedContatto);
			}
		}
	}

	private void removeDestinatario(final Contatto selectedContatto) {
		removeContattofromOneDataTable(contattiDaAltrePagineDTH, selectedContatto);

		clearFilterDataTable("contAltrePag_RG", contattiDaAltrePagineDTH);
		clearFilterDataTable(PREF_RUBRICA_RG_RESOURCE_PATH, contattiPreferitiDTH);
		clearFilterDataTable(RIC_RUBRICA_RG_RESOURCE_PATH, contattiRicercaDTH);

	}

	/**
	 * Gestisce la rimozione del contatto selezionato come destinatario
	 * {@link #removeDestinatario(Contatto)}.
	 * 
	 * @param event
	 */
	public void rimuoviDaiDestinatari(final ActionEvent event) {
		final Contatto selcontatto = (Contatto) getDataTableClickedRow(event);
		removeDestinatario(selcontatto);

	}

	/**
	 * Rende persistenti le modifiche {@link #contattiDaAltrePagineDTH}.
	 */
	public void registraModifiche() {
		boolean setShow = true;

		if (contattiDaAltrePagineDTH != null && contattiDaAltrePagineDTH.getMasters() != null) {
			if (rubricaModeEnum.equals(ModeRubricaEnum.SELEZIONE_SINGOLA) && contattiDaAltrePagineDTH.getMasters().size() > 1) {
				final FacesContext context = FacesContext.getCurrentInstance();
				setShow = false;
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore: ", "E' possibile inserire un solo contatto come mittente"));
			} else {
				setShow = beanMasterName.restituisciRisultati(contattiDaAltrePagineDTH.getMasters(), true, rubricaChiamante);
				if (setShow) {
					beanMasterName.aggiornaComponentiInterfaccia(rubricaChiamante);
				}
			}
		}
		if (setShow) {
			resetAllDataTableRubricaToDefaultParameters();
			sBean.setShowRubrica(false);
		}
	}

	/**
	 * Gestisce l'annullamento delle modifiche resettando il datatable della
	 * rubrica.
	 */
	public void annullaModifiche() {
		beanMasterName.restituisciRisultati(null, false, rubricaChiamante);
		resetAllDataTableRubricaToDefaultParameters();
		beanMasterName.aggiornaComponentiInterfaccia(rubricaChiamante);
		sBean.setShowRubrica(false);
	}

	/**
	 * Resetta tutti i datatable della rubrica.
	 */
	public void resetAllDataTableRubricaToDefaultParameters() {
		resetDataTableRubricaToDefaultParameters(PREF_RUBRICA_RG_RESOURCE_PATH);
		resetDataTableRubricaToDefaultParameters(RIC_RUBRICA_RG_RESOURCE_PATH);
		resetDataTableRubricaToDefaultParameters("contAltrePag_RG");
		resetDataTableRubricaToDefaultParameters("ricercaGruppiTabGruppi_ricrub");
		resetDataTableRubricaToDefaultParameters("RubricaTab:notificheRub_NR");
		resetDataTableRubricaToDefaultParameters("RubricaTab:richieste_RGRT");
	}

	/**
	 * Resetta i filtri del datatable dth.
	 * 
	 * @param idComponent
	 * @param dth
	 */
	public void clearFilterDataTable(final String idComponent, final SimpleDetailDataTableHelper<Contatto> dth) {
		final DataTable d = getDataTableRubricaTab(idComponent);
		if (d != null) {
			d.reset();
			if (dth != null && dth.getMasters() != null) {
				d.setValue(dth.getMasters());
			}
		}
	}

	/**
	 * Pulisce i filtri eventualmente impostati dal datatable delle notifiche.
	 * 
	 * @param idComponent
	 *            id del component datatable
	 * @param dth
	 *            datatable helper del datatable da cui rimuovere i filtri
	 *            selezionati
	 */
	@Override
	protected void clearFilterDataTableNotifiche(final String idComponent, final SimpleDetailDataTableHelper<NotificaContatto> dth) {
		final DataTable d = getDataTableRubricaTab(idComponent);
		if (d != null) {
			d.reset();
			if (dth != null && dth.getMasters() != null) {
				d.setValue(dth.getMasters());
			}
		}
	}

	/**
	 * Aggiunge il contatto identificato dal nodo selezionato
	 * {@link #addDestinatario(Contatto)}.
	 * 
	 * @param event
	 */
	public void addInternoToContactList(final NodeSelectEvent event) {
		try {
			final NodoOrganigrammaDTO select = (NodoOrganigrammaDTO) event.getTreeNode().getData();

			String aliasContatto = select.getDescrizioneNodo();
			if (!StringUtils.isNullOrEmpty(select.getCognomeUtente())) {
				aliasContatto += " - " + select.getNomeUtente() + " " + select.getCognomeUtente();
			}
			final Contatto c = new Contatto();
			c.setAliasContatto(aliasContatto);
			c.setTipoRubricaEnum(TipoRubricaEnum.INTERNO);
			c.setNome(select.getNomeUtente());
			c.setCognome(select.getCognomeUtente());
			c.setIdAOO(select.getIdAOO());
			c.setIdNodo(select.getIdNodo());
			c.setIdUtente(select.getIdUtente() != null ? select.getIdUtente() : 0);
			c.setSelected(true);

			addDestinatario(c);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}

	}

	// Integrazione Rubrica nelle altre pagine END

	/**
	 * @return the numGiorni
	 */
	public int getNumGiorni() {
		return numGiorni;
	}

	/**
	 * @param numGiorni the numGiorni to set
	 */
	public void setNumGiorni(final int numGiorni) {
		this.numGiorni = numGiorni;
	}

	/**
	 * @return the numElementi
	 */
	public int getNumElementi() {
		return numElementi;
	}

	/**
	 * @param numElementi the numElementi to set
	 */
	public void setNumElementi(final int numElementi) {
		this.numElementi = numElementi;
	}

	/**
	 * Restituisce true se in ricerca RED, false altrimenti.
	 * 
	 * @return true se in ricerca RED, false altrimenti
	 */
	public boolean getRicercaRED() {
		return ricercaRED;
	}

	/**
	 * Imposta il flag associato alla ricerca.
	 * 
	 * @param ricercaRED
	 */
	public void setRicercaRED(final boolean ricercaRED) {
		this.ricercaRED = ricercaRED;
	}

	/**
	 * Restituisce l'ufficio o personale.
	 * 
	 * @return ufficio o personale
	 */
	public String getUfficioOPersonale() {
		return ufficioOPersonale;
	}

	/**
	 * @param ufficioOPersonale
	 */
	public void setUfficioOPersonale(final String ufficioOPersonale) {
		this.ufficioOPersonale = ufficioOPersonale;
	}

	/**
	 * Restituisce il flag ufficio.
	 * 
	 * @return flag ufficio
	 */
	public boolean getUfficioFlag() {
		return ufficioFlag;
	}

	/**
	 * @param ufficioFlag
	 */
	public void setUfficioFlag(final boolean ufficioFlag) {
		this.ufficioFlag = ufficioFlag;
	}

	/**
	 * Restituisce true se aggiunto ai preferiti, false altrimenti.
	 * 
	 * @return true se aggiunto ai preferiti, false altrimenti
	 */
	public boolean getAddPreferiti() {
		return addPreferiti;
	}

	/**
	 * Imposta il flag associato ai preferiti.
	 * 
	 * @param addPreferiti
	 */
	public void setAddPreferiti(final boolean addPreferiti) {
		this.addPreferiti = addPreferiti;
	}

	/**
	 * Restituisce la modalita.
	 * 
	 * @return modeFlag
	 */
	public boolean getModeFlag() {
		return modeFlag;
	}

	/**
	 * Imposta la modalita.
	 * 
	 * @param modeFlag
	 */
	public void setModeFlag(final boolean modeFlag) {
		this.modeFlag = modeFlag;
	}

	/**
	 * Restituisce true se {@link #utente} ha il permesso di gestione rubrica.
	 * 
	 * @see PermessiEnum.
	 * @return true se l'utente ha il permesso di gestione rubrica, false
	 *         altrimenti.
	 */
	public boolean getPermessoUtente() {
		final Long permessi = utente.getPermessi();
		permessoUtente = PermessiUtils.hasPermesso(permessi, PermessiEnum.GESTIONE_RUBRICA);
		return permessoUtente;
	}

	/**
	 * @param event
	 */
	public void gestisciNotifica(final ActionEvent event) {
		gestisciNotifica((NotificaContatto) getDataTableClickedRow(event));
	}

	private DefaultTreeNode loadRootOrganigrammaComune() {
		DefaultTreeNode inRoot = null;

		try {
			final List<Long> alberatura = new ArrayList<>();
			for (int i = alberaturaNodi.size() - 1; i >= 0; i--) {
				alberatura.add(alberaturaNodi.get(i).getIdNodo());
			}

			// prendo da DB i nodi di primo livello
			final NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
			nodoRadice.setIdAOO(utente.getIdAoo());
			nodoRadice.setIdNodo(utente.getIdNodoRadiceAOO());
			nodoRadice.setCodiceAOO(utente.getCodiceAoo());
			nodoRadice.setUtentiVisible(false);
			final List<NodoOrganigrammaDTO> primoLivello = organigrammaSRV.getFigliAlberoAssegnatarioPerCompetenzaUscita(utente, nodoRadice);

			inRoot = new DefaultTreeNode();
			inRoot.setExpanded(true);
			inRoot.setSelectable(false);

			// per ogni nodo di primo livello creo il nodo da mettere nell'albero che ha
			// come padre rootAssegnazionePerCompetenza
			List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
			inRoot.setChildren(new ArrayList<>());
			DefaultTreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(utente.getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, inRoot);
				nodoToAdd.setExpanded(true);
				if (n.getIdUtente() == null && alberatura.contains(n.getIdNodo())) {
					nodiDaAprire.add(nodoToAdd);
				}
			}

			final List<DefaultTreeNode> nodiDaAprireTemp = new ArrayList<>();
			for (int i = 0; i < alberatura.size(); i++) {
				// devo fare esattamente un numero di cicli uguale alla lunghezza
				// dell'alberatura
				nodiDaAprireTemp.clear();
				for (final DefaultTreeNode nodo : nodiDaAprire) {
					onOpenTreeComune(nodo);
					nodo.setExpanded(true);
					for (final TreeNode nodoFiglio : nodo.getChildren()) {
						final NodoOrganigrammaDTO nodoFiglioDto = (NodoOrganigrammaDTO) nodoFiglio.getData();
						if (nodoFiglioDto.getIdUtente() == null && alberatura.contains(nodoFiglioDto.getIdNodo())) {
							nodiDaAprireTemp.add((DefaultTreeNode) nodoFiglio);
						}
					}
				}
				nodiDaAprire = new ArrayList<>(nodiDaAprireTemp);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}

		return inRoot;
	}

	/**
	 * Gestisce l'apertura del tree dei comuni:
	 * {@link #onOpenTreeComune(NodeExpandEvent)}.
	 * 
	 * @param event
	 */
	public void onOpenTreeComune(final NodeExpandEvent event) {
		onOpenTreeComune(event.getTreeNode());
	}

	private void onOpenTreeComune(final TreeNode tree) {
		try {
			final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) tree.getData();
			final List<NodoOrganigrammaDTO> figli = organigrammaSRV.getFigliAlberoAssegnatarioPerConoscenza(utente.getIdUfficio(), nodoExp, false);

			DefaultTreeNode nodoToAdd = null;
			tree.getChildren().clear();
			for (final NodoOrganigrammaDTO n : figli) {
				n.setCodiceAOO(utente.getCodiceAoo());

				nodoToAdd = new DefaultTreeNode(n, tree);
				if (n.getFigli() > 0 || n.isUtentiVisible()) {
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Restituisce l'helper per la gestione del datatable dei contatti in modifica.
	 * 
	 * @return helper datatable contatti
	 */
	public SimpleDetailDataTableHelper<Contatto> getContattiModificaDTH() {
		return contattiModificaDTH;
	}

	/**
	 * Imposta l'helper del datatable dei contatti in modifica.
	 * 
	 * @param contattiModificaDTH
	 */
	public void setContattiModificaDTH(final SimpleDetailDataTableHelper<Contatto> contattiModificaDTH) {
		this.contattiModificaDTH = contattiModificaDTH;
	}

	/**
	 * Restituisce il contatto in inserimento nel gruppo.
	 * 
	 * @return contatto
	 */
	public Contatto getInserisciGruppo() {
		return inserisciGruppo;
	}

	/**
	 * Imposta il contatto in inserimento nel gruppo.
	 * 
	 * @param inserisciGruppo
	 */
	public void setInserisciGruppo(final Contatto inserisciGruppo) {
		this.inserisciGruppo = inserisciGruppo;
	}

	/**
	 * @return the modificaContattoItem
	 */
	public Contatto getModificaContattoItem() {
		return modificaContattoItem;
	}

	/**
	 * @param modificaContattoItem the modificaContattoItem to set
	 */
	public void setModificaContattoItem(final Contatto modificaContattoItem) {
		this.modificaContattoItem = modificaContattoItem;
	}

	/**
	 * Restituisce la modalita dei gruppi.
	 * 
	 * @return modalita gruppi
	 */
	public String getModeGruppi() {
		return modeGruppi;
	}

	/**
	 * Imposta la modalita dei gruppi.
	 * 
	 * @param modeGruppi
	 */
	public void setModeGruppi(final String modeGruppi) {
		this.modeGruppi = modeGruppi;
	}

	/**
	 * Restituisce il flag associato alla modalita gruppi. Se true siamo in modalita
	 * di creazione contatto. Se false siamo in modalità di modifica/eliminazione
	 * contatto.
	 * 
	 * @return flag mode gruppi
	 */
	public boolean getModeGruppiFlag() {
		return modeGruppiFlag;
	}

	/**
	 * Imposta il flag associato alla modalita gruppi. Se true siamo in modalita di
	 * creazione contatto. Se false siamo in modalità di modifica/eliminazione
	 * contatto.
	 * 
	 * @param modeGruppiFlag
	 */
	public void setModeGruppiFlag(final boolean modeGruppiFlag) {
		this.modeGruppiFlag = modeGruppiFlag;
	}

	/**
	 * Restitusice l'helper per la gestione del datatable dei gruppi.
	 * 
	 * @return helper datatable gruppi
	 */
	public SimpleDetailDataTableHelper<Contatto> getGruppiDTH() {
		return gruppiDTH;
	}

	/**
	 * Imposta l'helper per la gestione del datatable dei gruppi.
	 * 
	 * @param gruppiDTH
	 */
	public void setGruppiDTH(final SimpleDetailDataTableHelper<Contatto> gruppiDTH) {
		this.gruppiDTH = gruppiDTH;
	}

	/**
	 * @return ricercaGruppo_gruppi
	 */
	public boolean getRicercaGruppoGruppi() {
		return ricercaGruppoGruppi;
	}

	/**
	 * @param ricercaGruppoGruppi
	 */
	public void setRicercaGruppoGruppi(final boolean ricercaGruppoGruppi) {
		this.ricercaGruppoGruppi = ricercaGruppoGruppi;
	}

	/**
	 * Restituisce il contatto in modifica.
	 * 
	 * @return contatto modifica
	 */
	public Contatto getModificaGruppoItem() {
		return modificaGruppoItem;
	}

	/**
	 * Imposta il contatto in modifica.
	 * 
	 * @param modificaGruppoItem
	 */
	public void setModificaGruppoItem(final Contatto modificaGruppoItem) {
		this.modificaGruppoItem = modificaGruppoItem;
	}

	/**
	 * Restituisce il flag: sel.
	 * 
	 * @return selFlag
	 */
	public boolean getSelFlag() {
		return selFlag;
	}

	/**
	 * Imposta sel.
	 * 
	 * @param selFlag
	 */
	public void setSelFlag(final boolean selFlag) {
		this.selFlag = selFlag;
	}

	/**
	 * Restituisce l'helper per la gestione del datatable che fa riferimento ai
	 * contatti nel gruppo.
	 * 
	 * @return helper datatable contatti nel gruppo
	 */
	public SimpleDetailDataTableHelper<Contatto> getContattiNelGruppoDTH() {
		return contattiNelGruppoDTH;
	}

	/**
	 * Imposta l'helper per la gestione del datatable che fa riferimento ai contatti
	 * nel gruppo.
	 * 
	 * @param contattiNelGruppoDTH
	 */
	public void setContattiNelGruppoDTH(final SimpleDetailDataTableHelper<Contatto> contattiNelGruppoDTH) {
		this.contattiNelGruppoDTH = contattiNelGruppoDTH;
	}

	/**
	 * Restituisce la lista dei contatti da aggiungere.
	 * 
	 * @return lista contatti da aggiungere
	 */
	public List<Contatto> getContattiDaAggiungere() {
		return contattiDaAggiungere;
	}

	/**
	 * Imposta la lista dei contatti da aggiungere.
	 * 
	 * @param contattiDaAggiungere
	 */
	public void setContattiDaAggiungere(final List<Contatto> contattiDaAggiungere) {
		this.contattiDaAggiungere = contattiDaAggiungere;
	}

	/**
	 * Restitusice la lista dei contatti da rimuovere.
	 * 
	 * @return lista contatti da rimuovere
	 */
	public List<Contatto> getContattiDaRimuovere() {
		return contattiDaRimuovere;
	}

	/**
	 * Imposta la lista dei contatti da rimuovere.
	 * 
	 * @param contattiDaRimuovere
	 */
	public void setContattiDaRimuovere(final List<Contatto> contattiDaRimuovere) {
		this.contattiDaRimuovere = contattiDaRimuovere;
	}

	/**
	 * @return the numGiorniNotifiche
	 */
	public int getNumGiorniNotifiche() {
		return numGiorniNotifiche;
	}

	/**
	 * @param numGiorniNotifiche the numGiorniNotifiche to set
	 */
	public void setNumGiorniNotifiche(final int numGiorniNotifiche) {
		this.numGiorniNotifiche = numGiorniNotifiche;
	}

	/**
	 * @return the numElementiNotifiche
	 */
	public int getNumElementiNotifiche() {
		return numElementiNotifiche;
	}

	/**
	 * @param numElementiNotifiche the numElementiNotifiche to set
	 */
	public void setNumElementiNotifiche(final int numElementiNotifiche) {
		this.numElementiNotifiche = numElementiNotifiche;
	}

	/**
	 * Restituisce l'helper per il datatable delle notifiche dei contatti.
	 * 
	 * @return helper datatable notifiche contatti.
	 */
	public SimpleDetailDataTableHelper<NotificaContatto> getNotificheRichiesteDTH() {
		return notificheRichiesteDTH;
	}

	/**
	 * Imposta l'helper per il datatable delle notifiche dei contatti.
	 * 
	 * @param notificheRichiesteDTH
	 */
	public void setNotificheRichiesteDTH(final SimpleDetailDataTableHelper<NotificaContatto> notificheRichiesteDTH) {
		this.notificheRichiesteDTH = notificheRichiesteDTH;
	}

	/**
	 * Restituisce il flag: aggiungiContattoAGruppoFlag.
	 * 
	 * @return aggiungiContattoAGruppoFlag
	 */
	public boolean getAggiungiContattoAGruppoFlag() {
		return aggiungiContattoAGruppoFlag;
	}

	/**
	 * Imposta aggiungiContattoAGruppoFlag.
	 * 
	 * @param aggiungiContattoAGruppoFlag
	 */
	public void setAggiungiContattoAGruppoFlag(final boolean aggiungiContattoAGruppoFlag) {
		this.aggiungiContattoAGruppoFlag = aggiungiContattoAGruppoFlag;
	}

	/**
	 * @return chiamataDaAltraPagina
	 */
	public boolean isChiamataDaAltraPagina() {
		return chiamataDaAltraPagina;
	}

	/**
	 * @param chiamataDaAltraPagina
	 */
	public void setChiamataDaAltraPagina(final boolean chiamataDaAltraPagina) {
		this.chiamataDaAltraPagina = chiamataDaAltraPagina;
	}

	/**
	 * Restituisce l'helper per il datatable dei contatti da altre pagine.
	 * 
	 * @return helper datatable contatti altre pagine
	 */
	public SimpleDetailDataTableHelper<Contatto> getContattiDaAltrePagineDTH() {
		return contattiDaAltrePagineDTH;
	}

	/**
	 * Imposta l'helper per il datatable dei contatti da altre pagine.
	 * 
	 * @param contattiDaAltrePagineDTH
	 */
	public void setContattiDaAltrePagineDTH(final SimpleDetailDataTableHelper<Contatto> contattiDaAltrePagineDTH) {
		this.contattiDaAltrePagineDTH = contattiDaAltrePagineDTH;
	}

	/**
	 * Restituisce il tipo dell'operazione.
	 * 
	 * @return tipo operazione
	 */
	public TipoOperazioneEnum getOperazioneEnum() {
		return operazioneEnum;
	}

	/**
	 * Imposta il tipo dell'operazione.
	 * 
	 * @param operazioneEnum
	 */
	public void setOperazioneEnum(final TipoOperazioneEnum operazioneEnum) {
		this.operazioneEnum = operazioneEnum;
	}

	/**
	 * Restituisce la nota di richiesta modifica.
	 * 
	 * @return nota richiesta modifica
	 */
	public String getNotaRichiestaModifica() {
		return notaRichiestaModifica;
	}

	/**
	 * Imposta la nota di richiesta modifica.
	 * 
	 * @param notaRichiestaModifica
	 */
	public void setNotaRichiestaModifica(final String notaRichiestaModifica) {
		this.notaRichiestaModifica = notaRichiestaModifica;
	}

	/**
	 * Restituisce la nota di richiesta eliminazione.
	 * 
	 * @return nota richiesta eliminazione
	 */
	public String getNotaRichiestaEliminazione() {
		return notaRichiestaEliminazione;
	}

	/**
	 * Imposta la nota di richiesta eliminazione.
	 * 
	 * @param notaRichiestaEliminazione
	 */
	public void setNotaRichiestaEliminazione(final String notaRichiestaEliminazione) {
		this.notaRichiestaEliminazione = notaRichiestaEliminazione;
	}

	/**
	 * Restituisce la nota di richiesta creazione.
	 * 
	 * @return nota richiesta creazione
	 */
	public String getNotaRichiestaCreazione() {
		return notaRichiestaCreazione;
	}

	/**
	 * Imposta la nota di richiesta creazione.
	 * 
	 * @param notaRichiestaCreazione
	 */
	public void setNotaRichiestaCreazione(final String notaRichiestaCreazione) {
		this.notaRichiestaCreazione = notaRichiestaCreazione;
	}

	/**
	 * Restituisce la root dell'organigramma dei comuni.
	 * 
	 * @return root organigramma comuni
	 */
	public DefaultTreeNode getRootOrganigrammaComune() {
		return rootOrganigrammaComune;
	}

	/**
	 * Imposta la root dell'organigramma dei comuni.
	 * 
	 * @param rootOrganigrammaComune
	 */
	public void setRootOrganigrammaComune(final DefaultTreeNode rootOrganigrammaComune) {
		this.rootOrganigrammaComune = rootOrganigrammaComune;
	}

	/**
	 * Restituisce true se mostra contatti interni, false altrimenti.
	 * 
	 * @return true se mostra contatti interni, false altrimenti
	 */
	public boolean getShowContattiInterni() {
		return showContattiInterni;
	}

	/**
	 * Imposta la visibilita dei contatti interni.
	 * 
	 * @param showContattiInterni
	 */
	public void setShowContattiInterni(final boolean showContattiInterni) {
		this.showContattiInterni = showContattiInterni;
	}

	/**
	 * @return ricercaEffettuataFlag
	 */
	public boolean getRicercaEffettuataFlag() {
		return ricercaEffettuataFlag;
	}

	/**
	 * @param ricercaEffettuataFlag
	 */
	public void setRicercaEffettuataFlag(final boolean ricercaEffettuataFlag) {
		this.ricercaEffettuataFlag = ricercaEffettuataFlag;
	}

	/**
	 * Restituisce il contatto in fase di inserimento.
	 * 
	 * @return contatto insert
	 */
	public Contatto getInserisciContattoItem() {
		return inserisciContattoItem;
	}

	/**
	 * Restituisce l'helper per la gestione del datatable che gestisce i contatti
	 * preferiti.
	 * 
	 * @return helper datatable contatti preferiti
	 */
	public SimpleDetailDataTableHelper<Contatto> getContattiPreferitiDTH() {
		return contattiPreferitiDTH;
	}

	/**
	 * Restituisce l'helper per la gestione del datatable che gestisce i contatti in
	 * ricerca.
	 * 
	 * @return helper datatable contatti ricerca
	 */
	public SimpleDetailDataTableHelper<Contatto> getContattiRicercaDTH() {
		return contattiRicercaDTH;
	}

	/**
	 * Imposta l'helper per la gestione del datatable che gestisce i contatti in
	 * ricerca.
	 * 
	 * @param contattiRicercaDTH
	 */
	public void setContattiRicercaDTH(final SimpleDetailDataTableHelper<Contatto> contattiRicercaDTH) {
		this.contattiRicercaDTH = contattiRicercaDTH;
	}

	/**
	 * Restituisce il messaggio rubrica da altra pagina.
	 * 
	 * @return messaggio rubrica
	 */
	public String getMessaggioRubricaDaAltraPagina() {
		return messaggioRubricaDaAltraPagina;
	}

	/**
	 * Imposta il messaggio rubrica da altra pagina.
	 * 
	 * @param messaggioRubricaDaAltraPagina
	 */
	public void setMessaggioRubricaDaAltraPagina(final String messaggioRubricaDaAltraPagina) {
		this.messaggioRubricaDaAltraPagina = messaggioRubricaDaAltraPagina;
	}

	/**
	 * @return contattiNelGruppoCrazioneDTH
	 */
	public SimpleDetailDataTableHelper<Contatto> getContattiNelGruppoCrazioneDTH() {
		return contattiNelGruppoCrazioneDTH;
	}

	/**
	 * @param contattiNelGruppoCrazioneDTH
	 */
	public void setContattiNelGruppoCrazioneDTH(final SimpleDetailDataTableHelper<Contatto> contattiNelGruppoCrazioneDTH) {
		this.contattiNelGruppoCrazioneDTH = contattiNelGruppoCrazioneDTH;
	}

	/**
	 * Restituisce il nome del form.
	 * 
	 * @return nome form
	 */
	@Override
	public String getFormName() {
		return formName;
	}

	/**
	 * Imposta il nome del form.
	 * 
	 * @param formName
	 */
	@Override
	public void setFormName(final String formName) {
		super.setFormName(formName);
		this.formName = formName;
	}

	/**
	 * Restituisce l'index attivo.
	 * 
	 * @return index attivo
	 */
	public Integer getActiveIndex() {
		return activeIndex;
	}

	/**
	 * Imposta l'index attivo.
	 * 
	 * @param activeIndex
	 */
	public void setActiveIndex(final Integer activeIndex) {
		this.activeIndex = activeIndex;
	}

	/**
	 * Restituisce true se considera gruppi, false altrimenti.
	 * 
	 * @return true se considera gruppi, false altrimenti
	 */
	public boolean isConsideraGruppi() {
		return consideraGruppi;
	}

	/**
	 * @param consideraGruppi
	 */
	public void setConsideraGruppi(final boolean consideraGruppi) {
		this.consideraGruppi = consideraGruppi;
	}

	/**
	 * Restituisce la query di ricerca.
	 * 
	 * @return query ricerca
	 */
	public String getQueryRicerca() {
		return queryRicerca;
	}

	/**
	 * Imposta la query di ricerca.
	 * 
	 * @param queryRicerca
	 */
	public void setQueryRicerca(final String queryRicerca) {
		this.queryRicerca = queryRicerca;
	}

	/**
	 * Restituisce la root del tree.
	 * 
	 * @return root del tree
	 */
	public DefaultTreeNode getRoot() {
		return root;
	}

	/**
	 * Imposta la root.
	 * 
	 * @param root
	 */
	public void setRoot(final DefaultTreeNode root) {
		this.root = root;
	}

	/**
	 * Check su operazioni e dati.
	 */
	@Override
	public void checkOperazioneEDati() {
		setCheckUnivocitaMail(utente.getCheckUnivocitaMail());
		super.checkOperazioneEDati();
	}

	/* METODI USATI E NUOVI */
	/**
	 * Finalizza la creazione del nuovo gruppo rendendo i valori persistenti.
	 */
	public void salvaNuovoGruppo() {
		if (!"".equals(inserisciGruppo.getNome())) {
			if (contattiNelGruppoCrazioneDTH != null) {
				if (!ricercaEffettuataFlag) {
					final DataTable dtRubUff = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(PREF_RUBRICA_RG_RESOURCE_FULL_PATH);
					if (dtRubUff != null) {
						dtRubUff.reset();
					}
					aggiungiContattoSelectedAGruppo(contattiPreferitiDTH);
				} else {
					aggiungiContattoSelectedAGruppo(contattiRicercaDTH);
				}
			} else {
				showWarnMessage("Inserire almeno un contatto nel gruppo");
			}

		} else {
			showWarnMessage("Attenzione il Nome del gruppo è un campo obbligatorio");
		}
	}

	private void aggiungiContattoSelectedAGruppo(final SimpleDetailDataTableHelper<Contatto> contattoDTH) {
		if (singleAction) {
			aggiungiContattoSelected(null);
		} else {
			aggiungiContattoSelected(contattoDTH);
		}
	}

	private void aggiungiContattoSelected(final SimpleDetailDataTableHelper<Contatto> contattoDTH) {
		if (contattoDTH != null && (contattoDTH.getSelectedMasters() != null && !contattoDTH.getSelectedMasters().isEmpty())) {
			for (final Contatto contattoSelected : contattoDTH.getSelectedMasters()) {
				contattiNelGruppoCrazioneDTH.getMasters().add(contattoSelected);
			}
		} else {
			contattiNelGruppoCrazioneDTH.getMasters().add(contattoDaAggiungereInGruppi);
		}
		if (contattiNelGruppoCrazioneDTH.getMasters() != null && !contattiNelGruppoCrazioneDTH.getMasters().isEmpty()) {
			aggiungiContattiAGruppo((List<Contatto>) contattiNelGruppoCrazioneDTH.getMasters());
			// reset
		} else if (contattoDaAggiungereInGruppi != null) {
			final List<Contatto> contattoDaAggiungereInGruppiList = new ArrayList<>();
			contattoDaAggiungereInGruppiList.add(contattoDaAggiungereInGruppi);
			aggiungiContattiAGruppo(contattoDaAggiungereInGruppiList);
		} else {
			showWarnMessage("Inserire almeno un contatto nel gruppo");
		}
	}

	private void aggiungiContattiAGruppo(final List<Contatto> contattiDaAggiungere) {
		try {
			getRubSRV().creaGruppo(inserisciGruppo, contattiDaAggiungere, utente.getIdUfficio(), utente.getId());
			cleanDTH(contattiNelGruppoCrazioneDTH);
			final Contatto inserisciGruppoTemp = new Contatto();
			inserisciGruppoTemp.copyFromContatto(inserisciGruppo);
			inserisciGruppoTemp.setSelected(true);
			// Faccio questa cosa per aggiungere sempre in testa il nuovo gruppo sia in
			// dialog che nei pref
			final List<Contatto> appendInTestaContattoGruppo = new ArrayList<>(gruppiDTH.getMasters());
			appendInTestaContattoGruppo.add(0, inserisciGruppoTemp);
			gruppiDTH.setMasters(appendInTestaContattoGruppo);
			inserisciGruppoTemp.setTipoRubrica(GRUPPO_UPPER);
			inserisciGruppoTemp.setGruppoCreatoDa(utente.getUsername());
			inserisciGruppoTemp.setDataCreazioneGruppo(new Date());
			// Faccio questa cosa per aggiungere sempre in testa il nuovo gruppo sia in
			// dialog che nei pref
			final List<Contatto> contattoSelectedTemp = contattiPreferitiDTH.getSelectedMasters();
			final List<Contatto> appendInTestaContattoPref = new ArrayList<>(contattiPreferitiDTH.getMasters());
			appendInTestaContattoPref.add(0, inserisciGruppoTemp);
			contattiPreferitiDTH.setMasters(appendInTestaContattoPref);
			contattiPreferitiDTH.setSelectedMasters(contattoSelectedTemp);
			inserisciGruppo.setNome("");
			inserisciGruppo.setAliasContatto("");
			showInfoMessage("Gruppo creato con successo.");
			LOGGER.info(GRUPPO + inserisciGruppo.getNome() + " creato con successo");
		} catch (final Exception e) {
			LOGGER.error("Errore durante creazione gruppo", e);
			showError(e);
		}
	}

	/**
	 * Effettua un resed al datatable della ricerca rubrica e reinizializza i
	 * parametri che gestiscono la ricerca.
	 */
	public void ritornaARubricaUfficio() {
		resetDataTableRubrica(RIC_RUBRICA_RG_RESOURCE_FULL_PATH);
		ricercaEffettuataFlag = false;
		queryRicerca = "";
		final Collection<Contatto> prefs = loadPreferiti();
		contattiPreferitiDTH = new SimpleDetailDataTableHelper<>(prefs);
	}

	/**
	 * Effettua la ricerca dei contatti nel gruppo e popola i masters di
	 * {@link #contattiNelGruppoDTH} con i risultati.
	 */
	public void ricercaContattiNelGruppo() {
		RicercaRubricaDTO ricercaContattiNelGruppo;
		ricercaContattiNelGruppo = new RicercaRubricaDTO();

		ricercaContattiNelGruppo.setRicercaRED(true);
		ricercaContattiNelGruppo.setRicercaIPA(true);
		ricercaContattiNelGruppo.setRicercaMEF(true);
		ricercaContattiNelGruppo.setRicercaGruppo(false);
		ricercaContattiNelGruppo.setIdGruppo(gruppiDTH.getCurrentDetail().getContattoID());

		setContattiNelGruppoDTH(ricerca(ricercaContattiNelGruppo));
		resetAllCheckBox(contattiRicercaDTH);
	}

	/**
	 * Effettua un update dei check rubrica chiamando
	 * {@link #updateCheck(AjaxBehaviorEvent, SimpleDetailDataTableHelper)} con i
	 * master del {@link #contattiPreferitiDTH}.
	 * 
	 * @param event
	 */
	public void updateCheckRubricaUfficio(final AjaxBehaviorEvent event) {
		updateCheck(event, contattiPreferitiDTH);
	}

	/**
	 * Gestisce la modalita crea nuovo gruppo.
	 */
	public void creaNuovoGruppoClick() {
		creaNuovoGruppoClicked = !creaNuovoGruppoClicked;
	}

	/**
	 * Imposta modalita tutti i gruppi.
	 */
	public void tuttiGruppi() {
		singleAction = false;
		tuttiGruppi(null);
	}

	/**
	 * Imposta il contatto selezionato come contatto da aggiungere in gruppi:
	 * {@link #contattoDaAggiungereInGruppi}.
	 * 
	 * @param contattoSelected
	 */
	public void tuttiGruppi(final Contatto contattoSelected) {
		try {
			gruppiDTH = new SimpleDetailDataTableHelper<>();
			gruppiDTH.setMasters(getRubSRV().getAllGruppi(utente.getIdUfficio()));
			if (contattoSelected != null) {
				impostaContattoDaAggiungereInGruppi(contattoSelected);
				if (!CollectionUtils.isEmptyOrNull(gruppiDTH.getMasters())) {
					getRubSRV().contattoGiaInGruppi(gruppiDTH.getMasters(), contattoSelected.getContattoID());
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);

		}
	}

	/**
	 * Mostra l'albero per la selezione dei contatti IPA.
	 * 
	 * @param event
	 */
	public void showAlberaturaContattoIPA(final ActionEvent event) {
		final Contatto contSel = getDataTableClickedRow(event);
		if (contSel.getTipoRubricaEnum().equals(TipoRubricaEnum.IPA)) {
			getRubSRV().getContattiChildIPA(contSel);
			setRoot(new DefaultTreeNode("Root", null));
			getRoot().setSelectable(true);
			// Popola l'albero dal nodo corrente fino alla root
			getRecursiveNode(contSel);

		}
	}

	private TreeNode getRecursiveNode(final Contatto contattoIPA) {
		final Contatto contattoPadre = getRubSRV().getPadreContattoIPA(contattoIPA);
		if (contattoPadre != null && contattoPadre.getAliasContatto() != null) {
			LOGGER.info(contattoPadre.getAliasContatto());
		}
		TreeNode nodoCorrente = null;
		if (contattoPadre == null) {
			nodoCorrente = new DefaultTreeNode(contattoIPA, getRoot());
			nodoCorrente.setExpanded(true);
			nodoCorrente.setSelectable(true);
			nodoCorrente.setType(contattoIPA.getTipoPersona());
		} else {
			final TreeNode nodoPadre = getRecursiveNode(contattoPadre);
			nodoCorrente = new DefaultTreeNode(contattoIPA, nodoPadre);
			nodoCorrente.setExpanded(true);
			nodoCorrente.setSelectable(true);
			nodoCorrente.setType(contattoIPA.getTipoPersona());

		}

		return nodoCorrente;
	}

	/**
	 * Gestisce l'eliminazione del contatto dal gruppo: {@link #contattiNelGruppo}.
	 * 
	 * @param contatto
	 */
	public void rimuoviContattoDaGruppoNew(final Contatto contatto) {
		if (contattiNelGruppo.isEmpty()) {
			showWarnMessage("Il gruppo deve contenere almeno un contatto!");
			return;
		}

		contattiNelGruppo.remove(contatto);
		contattiDaRimuovere.add(contatto);
		getRubSRV().rimuoviDAGruppo(modificaContattoItem, contattiDaRimuovere);
		showInfoMessage("Contatto rimosso");
	}

	/**
	 * Inizializza il contatto per la creazione.
	 */
	public void inizializzaContattoPerCreazione() {
		inserisciContattoItem = new Contatto();
		inserisciContattoItem.setTipoPersona("F");
		inModifica = false;
	}

	/**
	 * Valida il contatto {@link #inserisciContattoItem} e lo inserisce nella
	 * rubrica.
	 */
	public void validaEInserisciNew() {
		try {
			final boolean creaContatto = valida(inserisciContattoItem);
			if (permessoUtente && creaContatto) {
				RubricaComponent.inserisciContatto(inserisciContattoItem, utente);

				if (addPreferiti) {
					aggiungiAiPreferiti(inserisciContattoItem);
				}
				inserisciContattoItem = new Contatto();
				inserisciContattoItem.setRegioneObj(new RegioneDTO());
				inserisciContattoItem.setComuneObj(new ComuneDTO());
				inserisciContattoItem.setProvinciaObj(new ProvinciaDTO());
				FacesHelper.executeJS("PF('creaModificaContattoDlg_WV').hide();");
				showInfoMessage("Contatto creato con successo");
			} else if (!permessoUtente && creaContatto) {
				FacesHelper.executeJS("PF('dialogRichCreazioneVW').show();");
			}
		} catch (final Exception ex) {
			showError(ex);
		}
	}

	/**
	 * Aggiorna {@link #contattiNelGruppo} con i contatti esistenti nel gruppo
	 * selezionato.
	 * 
	 * @param gruppoSelected
	 */
	public void ricercaContattiNelGruppo(final Contatto gruppoSelected) {
		modificaContattoItem = new Contatto();
		modificaContattoItem.copyFromContatto(gruppoSelected);
		contattiNelGruppo = RubricaComponent.ricercaContattiNelGruppo(gruppoSelected, utente.getIdUfficio());
	}

	/**
	 * Reinizializza la query di ricerca.
	 */
	public void cleanQueryRicerca() {
		if (!StringUtils.isNullOrEmpty(queryRicerca)) {
			queryRicerca = "";
		}
	}

	/**
	 * Restituisce il contatto in eliminazione.
	 * 
	 * @return contatto da eliminare
	 */
	public Contatto getEliminaContattoItem() {
		return eliminaContattoItem;
	}

	/**
	 * Imposta il contatto in eliminazione.
	 * 
	 * @param eliminaContattoItem
	 */
	public void setEliminaContattoItem(final Contatto eliminaContattoItem) {
		this.eliminaContattoItem = eliminaContattoItem;
	}

	/**
	 * @return creaNuovoGruppoClicked
	 */
	public boolean getCreaNuovoGruppoClicked() {
		return creaNuovoGruppoClicked;
	}
 
   
	/**
	 * @param creaNuovoGruppoClicked
	 */
	public void setCreaNuovoGruppoClicked(final boolean creaNuovoGruppoClicked) {
		this.creaNuovoGruppoClicked = creaNuovoGruppoClicked;
	}

	/**
	 * Restituisce l'utente.
	 * 
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Restituisce i contatti all'interno del gruppo.
	 * 
	 * @return contatti nel gruppo
	 */
	public List<Contatto> getContattiNelGruppo() {
		return contattiNelGruppo;
	}

	/**
	 * Imposta i contatti nel gruppo.
	 * 
	 * @param contattiNelGruppo
	 */
	public void setContattiNelGruppo(final List<Contatto> contattiNelGruppo) {
		this.contattiNelGruppo = contattiNelGruppo;
	}

	/**
	 * Restituisce il contatto in fase di inserimento.
	 * 
	 * @return contatti in fase di inserimento
	 */
	public Contatto getContattoDaAggiungereInGruppi() {
		return contattoDaAggiungereInGruppi;
	}

	/**
	 * Imposta il contatto in fase di inserimento nel gruppo.
	 * 
	 * @param contattoDaAggiungereInGruppi
	 */
	public void setContattoDaAggiungereInGruppi(final Contatto contattoDaAggiungereInGruppi) {
		this.contattoDaAggiungereInGruppi = contattoDaAggiungereInGruppi;
	}

	/**
	 * Restituisce true se azione singola, false se azione multipla.
	 * 
	 * @return true se azione singola, false se azione multipla
	 */
	public boolean getSingleAction() {
		return singleAction;
	}

	/**
	 * Imposta la molteplicita dell'azione, true = singola, false = multipla.
	 * 
	 * @param singleAction
	 */
	public void setSingleAction(final boolean singleAction) {
		this.singleAction = singleAction;
	}

	/**
	 * Restituisce true se in modifica, false altrimenti.
	 * 
	 * @return true se in modifica, false altrimenti
	 */
	public boolean getInModifica() {
		return inModifica;
	}

	/**
	 * Imposta il flag: in modifica.
	 * 
	 * @param inModifica
	 */
	public void setInModifica(final boolean inModifica) {
		this.inModifica = inModifica;
	}

	/**
	 * Metodo che effettua il reset delle checkbox in fase di navigazione tra la
	 * view rubrica ufficio e ricerca.
	 */
	public void resetCheckBoxGruppi() {
		resetAllCheckBox(contattiPreferitiDTH);
		resetAllCheckBox(contattiRicercaDTH);
	}

	private void resetAllCheckBox(final SimpleDetailDataTableHelper<Contatto> contattoDTH) {
		if (contattoDTH != null) {
			for (final Contatto cont : contattoDTH.getSelectedMasters()) {
				cont.setSelected(false);
			}
		}

		if (!ricercaEffettuataFlag) {
			FacesHelper.update(PREF_RUBRICA_RG_RESOURCE_FULL_PATH);
		} else {
			FacesHelper.update(RIC_RUBRICA_RG_RESOURCE_FULL_PATH);
		}
	}

	private void resetDataTableRubrica(final String idComponent) {
		final DataTable d = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(idComponent);
		if (d != null) {
			d.reset();
		}
	}

	/**
	 * @see it.ibm.red.web.mbean.AbstractRubricaBean#rifiutaRichiesta().
	 */
	@Override
	public void rifiutaRichiesta() {
		super.rifiutaRichiesta();
		getRubSRV().updateStatoNotificaRubrica(utente.getId(), utente.getIdAoo(), getNotificaPerOperazione().getIdNotificaContatto());
	}

	/**
	 * Restitusice il flag associato all'eliminazione dei gruppi contatti non
	 * uffici.
	 * 
	 * @return flag eliminazione gruppi
	 */
	public boolean isDisabiitaDeleteGruppiContNonUff() {
		return disabiitaDeleteGruppiContNonUff;
	}
	
	/**
	 * Imposta il flag associato all'eliminazione dei gruppi contatti non uffici.
	 * @param disabiitaDeleteGruppiContNonUff
	 */
	public void setDisabiitaDeleteGruppiContNonUff(boolean disabiitaDeleteGruppiContNonUff) {
		this.disabiitaDeleteGruppiContNonUff = disabiitaDeleteGruppiContNonUff;
	}
}
