package it.ibm.red.web.enums;

/**
 * Enum per help page della coda mail
 *
 * @author VINGENITO
 * 
 *         Tipologie di codifica.
 */
public enum IconaMailEnum {

	/**
	 * Valore.
	 */
	SEGNATURA_VALIDA_INTEROP("1", "segnatura valida interoperabilità", IconaMailEnum.FA_FA_ARROWS_H_ICON_BORDERED,
			"Indica che la segnatura derivante dal flusso di interoperabilità è corretta", "green"),

	/**
	 * Valore.
	 */
	SEGNATURA_ERRATA_INTEROP("2", "segnatura errata interoperabilità", IconaMailEnum.FA_FA_ARROWS_H_ICON_BORDERED,
			"Indica che la segnatura derivante dal flusso di interoperabilità è errata", "red"),

	/**
	 * Valore.
	 */
	SEGNATURA_NON_CONFORME_INTEROP("3", "segnatura non conforme interoperabilità", IconaMailEnum.FA_FA_ARROWS_H_ICON_BORDERED,
			"Indica che la segnatura derivante dal flusso di interoperabilità non è conforme", "yellow"),

	/**
	 * Valore.
	 */
	PREASSEGNATARIO_VALIDO_INTEROP("4", "preassegnatraio valido interoperabilità", IconaMailEnum.FA_FA_PLAY_ICON_BORDERED,
			"Indica che il preassegnatario derivante dal flusso di interoperabilità è valido", "green"),

	/**
	 * Valore.
	 */
	PREASSEGNATARIO_NON_VALIDO_INTEROP("5", "preassegnatario non valido interoperabilita", IconaMailEnum.FA_FA_PLAY_ICON_BORDERED,
			"Indica che il preassegnatario derivante dal flusso di interoperabilità è non corretto", "red"),

	/**
	 * Valore.
	 */
	PREASSEGNATARIO_NON_ESISTENTE_INTEROP("6", "preassegnatario non presente", IconaMailEnum.FA_FA_PLAY_ICON_BORDERED,
			"Indica che il preassegnatario non è pervenuto dal flusso di interoperabilità ", "yellow"),

	/**
	 * Valore.
	 */
	MESSAGE_ID("7", "message id", "fa fa-info-circle", "identificativo univoco per la mail", ""),

	/**
	 * Valore.
	 */
	ALLEGATI_PRESENTI("8", "allegati presenti", "fa fa-paperclip", "indica se la mail contiene o meno degli allegati", ""),

	/**
	 * Valore.
	 */
	NOTA_MAIL("9", "nota mail", "fa fa-flag", "nota della mail", ""),

	/**
	 * Valore.
	 */
	MOTIVO_ELIMINAZIONE_MAIL("10", "motivo eliminazione", "fa fa-asterisk", "motivo eliminazione della mail", ""),

	/**
	 * Valore.
	 */
	LOCK_UTENTE_PROT_MAIL("11", "lock utente protocollatore", "fa fa-lock", "lock dell'utente protocollatore", "");

	/**
	 * Identiicativo.
	 */
	private String id;

	/**
	 * Title.
	 */
	private String title;

	/**
	 * Icona.
	 */
	private String icon;

	/**
	 * Descriione.
	 */
	private String descrizione;

	/**
	 * Colore.
	 */
	private String colore;
	
	private static final String FA_FA_ARROWS_H_ICON_BORDERED = "fa fa-arrows-h icon-bordered";
	private static final String FA_FA_PLAY_ICON_BORDERED = "fa fa-play icon-bordered";

	IconaMailEnum(final String inId, final String inTitle, final String inIcon, final String inDescrizione, final String inColore) {
		id = inId;
		title = inTitle;
		icon = inIcon;
		descrizione = inDescrizione;
		colore = inColore;
	}
	
	/**
	 * Recupera l'id.
	 * 
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Recupera il titolo.
	 * 
	 * @return titolo
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Recupera l'icona.
	 * 
	 * @return icona
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * Recupera la descrizione.
	 * 
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Recupera il colore.
	 * 
	 * @return colore
	 */
	public String getColore() {
		return colore;
	}
	
	/**
	 * Ottiene l'icona della mail.
	 * 
	 * @param pk
	 * @return icona della mail
	 */
	public static IconaMailEnum get(final String pk) {
		IconaMailEnum output = null;
		for (final IconaMailEnum t : IconaMailEnum.values()) {
			if (pk.equals(t.getId())) {
				output = t;
				break;
			}
		}
		return output;
	}
}
