package it.ibm.red.web.mbean.humantask;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections.CollectionUtils;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV;
import it.ibm.red.business.service.facade.IPredisponiDsrFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.mbean.DsrDataTabAllegatiBean;
import it.ibm.red.web.document.mbean.DsrDataTabFascicoliBean;
import it.ibm.red.web.document.mbean.component.DsrPresentazioneBean;
import it.ibm.red.web.document.mbean.interfaces.IDsrPresentazioneBean;
import it.ibm.red.web.document.mbean.interfaces.ITabAllegatiBean;
import it.ibm.red.web.document.mbean.interfaces.ITabFascicoliBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.beantask.EsitoCreaDocumentoDaResponseBean;
import it.ibm.red.web.mbean.humantask.component.PredisponiDsrTabGeneraleComponent;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * Classe PredisponiDichiarazioneBean.
 */
@Named(ConstantsWeb.MBean.PREDISPONI_DICHIARAZIONE)
@ViewScoped
public class PredisponiDichiarazioneBean extends AbstractBean implements InitHumanTaskInterface {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1370561330764291484L;
 
	/**
	 * LOGGER
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PredisponiDichiarazioneBean.class);
	
	/**
	 * Lista documenti bean
	 */
	private ListaDocumentiBean ldb;
	
	/**
	 * Esito crea documento da response
	 */
	private EsitoCreaDocumentoDaResponseBean esitoPredisponiDaDocBean;
		
	/**
	 * Facade service predisponi
	 */
	private IPredisponiDsrFacadeSRV predisponiSRV;
	
	/**
	 * Utente
	 */
	private UtenteDTO utente;
	
	/**
	 * Dettaglio
	 */
	private DetailDocumentRedDTO detail;
	
	/**
	 * TAb generale predisponi
	 */
	private PredisponiDsrTabGeneraleComponent tabGenerale;
	
	/**
	 * Tab fascicoli 
	 */
	private ITabFascicoliBean tabFascicoli;

	/**
	 * Tab allegati
	 */
	private DsrDataTabAllegatiBean tabAllegati;
	
	/**
	 * Presentazione dsr
	 */
	private IDsrPresentazioneBean presentazione;
		
	
	/**
	 * Post construct.
	 */
	@Override
	@PostConstruct
	public void postConstruct() {
		final SessionBean sessionBean = FacesHelper.getManagedBean(MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		
		ldb = FacesHelper.getManagedBean(MBean.LISTA_DOCUMENTI_BEAN);
		esitoPredisponiDaDocBean = FacesHelper.getManagedBean(MBean.ESITO_CREA_DOC_DA_RESPONSE_BEAN);
		
		predisponiSRV = ApplicationContextProvider.getApplicationContext().getBean(IPredisponiDsrFacadeSRV.class);
	}
	
	
	/**
	 * Inizializzazione bean predisponi dichiarazione
	 *
	 * @param inDococsSelezionati the in dococs selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDococsSelezionati) {
		if (CollectionUtils.isEmpty(inDococsSelezionati)) {
			showError("Nessun documento passato");
		} else {			
			final MasterDocumentRedDTO master = inDococsSelezionati.get(0);
			try {
				detail = predisponiSRV.getDocumentDetail(master.getDocumentTitle(), master.getWobNumber(), master.getNumeroDocumento(), utente);
				initializeManagerBean();
			} catch (final Exception e) {
				LOGGER.error(e);
				showError("Errore all'avvio di predisponi dichiarazione.");
			}
		}
	}
	
	
	/**
	 * Al momento dell'apertura della maschera setta il dettaglio da mostrare
	 * @param detail
	 */
	protected void initializeManagerBean() {
		try {
			tabGenerale = new PredisponiDsrTabGeneraleComponent(detail, utente);
			tabFascicoli = new DsrDataTabFascicoliBean(detail);
			tabAllegati = new DsrDataTabAllegatiBean(detail, utente);
			tabAllegati.getDocumentManagerDTO().setAllegatiModificabili(true);
			tabAllegati.getDocumentManagerDTO().setInModificaIngresso(true);
			presentazione = new DsrPresentazioneBean(detail, false);
		} catch (final RedException e) {
			showError(e);
		}
	}
	
	/**
	 * Metodo che permette di effettuare una registrazione da predisponi
	 */
	public void registra() {
		try {
			/** OGGETTO START**************************************/
			if (StringUtils.isNullOrEmpty(this.detail.getOggetto()) || this.detail.getOggetto().length() < 4) {
				showWarnMessage("L'oggetto deve contenere almeno 4 caratteri.");
				return;
			}
			if (!tabAllegati.validateAllegati()) {
				showError("Alcuni allegati non sono stati compilati correttamente.");
				return;
			}
			
			// pulisco gli esiti
			esitoPredisponiDaDocBean.getMessaggiEsito().clear();
			
			byte[] content = null;
			if (tabGenerale.getUploadDocumento().isUploaded()) {
				content = detail.getContent();
			}
			
			final EsitoSalvaDocumentoDTO esitoSd = predisponiSRV.predisponiDichiarazione(utente, detail.getDocumentTitle(), detail.getWobNumberSelected(), 
					detail.getNomeFile(), detail.getOggetto(), detail.getAllegati(), content);
			
			if (esitoSd.isEsitoOk()) {
				final IDocumentoRedFacadeSRV documentoRedSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoRedFacadeSRV.class);
				
				final DetailDocumentRedDTO doc = documentoRedSRV.getDocumentDetail(esitoSd.getDocumentTitle(), utente, null, null);
				
				final Map<String, String> messaggioEsitoRegistra = new HashMap<>();
				messaggioEsitoRegistra.put("Inserimento documento effettuato con successo.", "");
				messaggioEsitoRegistra.put("Identificativo:  ", doc.getNumeroDocumento() + " del " + doc.getAnnoDocumento());
				if (doc.getNumeroProtocollo() != null && doc.getNumeroProtocollo().intValue() > 0) {
					messaggioEsitoRegistra.put("Protocollo:  ",  doc.getNumeroProtocollo() + "/" + doc.getAnnoProtocollo());
				}
				messaggioEsitoRegistra.put("Inserito nel fascicolo:  ",  doc.getIdFascicoloProcedimentale() + " - " + doc.getDescrizioneFascicoloProcedimentale());
				
				esitoPredisponiDaDocBean.setMessaggiEsito(messaggioEsitoRegistra);
				
				FacesHelper.executeJS("PF('dlgGeneric').hide()");
				FacesHelper.update("idDettagliEstesiForm:idDlgEsitoCreaDocDaResponse");
				FacesHelper.executeJS("PF('dlgEsitoCreaDocDaResponse').show()");
				close();
			} else {
				showError(esitoSd.getNote());
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il tentato sollecito della response '" + ldb.getSelectedResponse() + "'", e);
			showError("Si è verificato un errore durante la predisposizione della dichiarazione.");
		}
	}
	
	/**
	 * Chiusura e distruzione bean.
	 */
	public void close() {
		ldb.destroyBeanViewScope(ConstantsWeb.MBean.PREDISPONI_DICHIARAZIONE);
	}
	
	/** 
	 * @return the tab generale
	 */
	public PredisponiDsrTabGeneraleComponent getTabGenerale() {
		return tabGenerale;
	}
	
	/** 
	 * @return the tab fascicoli
	 */
	public ITabFascicoliBean getTabFascicoli() {
		return tabFascicoli;
	}

	/** 
	 * @return the tab allegati
	 */
	public ITabAllegatiBean getTabAllegati() {
		return tabAllegati;
	}

	/** 
	 * @return the presentazione
	 */
	public IDsrPresentazioneBean getPresentazione() {
		return presentazione;
	}

	/** 
	 * @return the detail
	 */
	public DetailDocumentRedDTO getDetail() {
		return detail;
	}

	/** 
	 * @param detail the new detail
	 */
	public void setDetail(final DetailDocumentRedDTO detail) {
		this.detail = detail;
	}
}