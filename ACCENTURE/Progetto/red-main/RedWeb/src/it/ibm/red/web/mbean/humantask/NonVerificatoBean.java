package it.ibm.red.web.mbean.humantask;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IVistiFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;


/**
 * Bean per la gestione delle verifiche
 * 
 */
@Named(ConstantsWeb.MBean.NON_VERIFICATO_BEAN)
@ViewScoped
public class NonVerificatoBean extends AbstractBean implements InitHumanTaskInterface {

	/** 
	 * The Constant serialVersionUID. 
	 */
	private static final long serialVersionUID = 7422662665139545505L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NonVerificatoBean.class.getName());
	
	/** 
	 * Lista di documenti selezionati 
	 */
	private List<MasterDocumentRedDTO> documentiSelezionati;
	
	/** 
	 * Messaggio 
	 */
	private String message;
	 
	
	/** 
	 * SessionBean 
	 */
	private SessionBean sb;
	
	/** 
	 * Lista Documenti Bean 
	 */
	private ListaDocumentiBean ldb;

	/**
	 * Inits the bean.
	 *
	 * @param inDocsSelezionati the in docs selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		setDocumentiSelezionati(inDocsSelezionati);

	}

	/**
	 * Post construct.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() { 
		// Non deve fare niente.
	}
	
	/**
	 * Response singola per la verifica 
	 */
	public void verificatoResponse() {
		//E' una response per un solo documento (singola)
		EsitoOperazioneDTO eOpe = null;
		String wobNumber = null;
		try {
			final IVistiFacadeSRV vistoSRV = ApplicationContextProvider.getApplicationContext().getBean(IVistiFacadeSRV.class);
			sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
			ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
			final UtenteDTO utente = sb.getUtente();
			
			//pulisco eventuali esiti
			ldb.cleanEsiti();
			
			//prendo il wobnumber del documento
			wobNumber = documentiSelezionati.get(0).getWobNumber();
			
			//chiamo il service (singolo doc)
			eOpe = vistoSRV.nonVerificato(utente, wobNumber, message);
			
			ldb.getEsitiOperazione().add(eOpe);
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.NON_VERIFICATO_BEAN);
			
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il tentato sollecito della response 'NON VERIFICATO'", e);
			showError("Si è verificato un errore durante il tentato sollecito della response 'NON VERIFICATO'");
		}
	}

	/** 
	 *
	 * @return the documenti selezionati
	 */
	public List<MasterDocumentRedDTO> getDocumentiSelezionati() {
		return documentiSelezionati;
	}

	/** 
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/** 
	 *
	 * @return the sb
	 */
	public SessionBean getSb() {
		return sb;
	}

	/** 
	 *
	 * @return the ldb
	 */
	public ListaDocumentiBean getLdb() {
		return ldb;
	}

	/** 
	 *
	 * @param documentiSelezionati the new documenti selezionati
	 */
	public void setDocumentiSelezionati(final List<MasterDocumentRedDTO> documentiSelezionati) {
		this.documentiSelezionati = documentiSelezionati;
	}

	/** 
	 *
	 * @param message the new message
	 */
	public void setMessage(final String message) {
		this.message = message;
	}

	/** 
	 *
	 * @param sb the new sb
	 */
	public void setSb(final SessionBean sb) {
		this.sb = sb;
	}

	/** 
	 *
	 * @param ldb the new ldb
	 */
	public void setLdb(final ListaDocumentiBean ldb) {
		this.ldb = ldb;
	}

}
