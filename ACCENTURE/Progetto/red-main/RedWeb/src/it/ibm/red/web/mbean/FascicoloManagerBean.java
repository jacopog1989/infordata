package it.ibm.red.web.mbean;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DetailFascicoloRedDTO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.ParamsRicercaProtocolloDTO;
import it.ibm.red.business.dto.ProtocolliPregressiDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.EsitoFaldoneEnum;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.RicercaPerBusinessEntityEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.dto.DocumentoNpsDTO;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IAllegatoFacadeSRV;
import it.ibm.red.business.service.facade.IDetailFascicoloFacadeSRV;
import it.ibm.red.business.service.facade.IDfFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV;
import it.ibm.red.business.service.facade.IFaldoneFacadeSRV;
import it.ibm.red.business.service.facade.IFascicoloFacadeSRV;
import it.ibm.red.business.service.facade.INpsFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaFacadeSRV;
import it.ibm.red.business.service.facade.ISalvaDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.ITitolarioFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.mbean.DocumentManagerBean;
import it.ibm.red.web.document.mbean.component.DialogFaldonaComponent;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.component.RicercaFascicoloDaDocumentoComponent;
import it.ibm.red.web.mbean.interfaces.IUpdatableDetailCaller;
import it.ibm.red.web.nps.component.DetailNpsComponent;

/**
 * Bean fascicolo manager.
 */
@Named(MBean.FASCICOLO_MANAGER_BEAN)
@ViewScoped
public class FascicoloManagerBean extends AbstractBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -4530417944325519800L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FascicoloManagerBean.class);
	
	/**
	 * Location del component del dettaglio documento per un decreto dirigenziale.
	 */
	private static final String EAST_SECTION_FORM_ID_DETTAGLIO_DOCUMENTO_DD = "eastSectionForm:idDettaglioDocumento_DD";

	/**
	 * Messaggio.
	 */
	private static final String OPERAZIONE_AVVENUTA_CON_SUCCESSO = "Operazione avvenuta con successo.";

	/**
	 * Messaggio.
	 */
	private static final String FASCICOLO_ASSOCIATO_CORRETTAMENTE = "Fascicolo associato correttamente.";

	/**
	 * Messaggio.
	 */
	private static final String ERRORE_DURANTE_L_OPERAZIONE = "Errore durante l'operazione: ";

	/**
	 * Messaggio.
	 */
	private static final String ERRORE_DURANTE_L_OPERAZIONE_DI_RECUPERO_DEL_DOCUMENTO = "Errore durante l'operazione di recupero del documento.";

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Servizio.
	 */
	private IDocumentManagerFacadeSRV documentManagerSRV;
	/**
	 * Servizio.
	 */
	private IDocumentoRedFacadeSRV documentoRedSRV;
	/**
	 * Servizio.
	 */
	private IDocumentoFacadeSRV documentoSRV;
	/**
	 * Servizio.
	 */
	private IFascicoloFacadeSRV fascicoloSRV;
	/**
	 * Servizio.
	 */
	private IRicercaFacadeSRV ricercaSRV;
	/**
	 * Servizio.
	 */
	private IDetailFascicoloFacadeSRV detailFascicoloSRV;
	/**
	 * Servizio.
	 */
	private ISalvaDocumentoFacadeSRV salvaDocumentoSRV;
	/**
	 * Servizio.
	 */
	private IFaldoneFacadeSRV faldoneSRV;
	/**
	 * Servizio.
	 */
	private ITipologiaDocumentoFacadeSRV tipologiaDocumentoSRV;

	/**
	 * Dettaglio.
	 */
	private DetailFascicoloRedDTO detail;

	/**
	 * Flag popup visibile.
	 */
	private boolean popupDocumentVisible;

	/** Titolario start. ***********************************************/
	private DefaultTreeNode rootTitolario;
	/**
	 * Titolario selezionato..
	 */
	private DefaultTreeNode titolarioNodoSelected;

	/**
	 * Nodo titolario selezionato..
	 */
	private TitolarioDTO titolarioNodoSelectedDTO; // serve per l'autocomplete

	/**
	 * Servizio.
	 */
	private ITitolarioFacadeSRV titolarioSRV;
	/**
	 * Descriione titolario..
	 */
	private String descrizioneTitolario;

	/**
	 * Flag salvataggio.
	 */
	private boolean salvaTitolarioDisabled;

	/** Documenti start. ***********************************************/
	private String ricercaFascicoliParval;

	/**
	 * Anno ricerca fascicoli.
	 */
	private String ricercaFascicoliAnno;

	/**
	 * Risultato ricerca.
	 */
	private Collection<FascicoloDTO> fascicoliCercati;

	/**
	 * Fascicolo selezionato.
	 */
	private FascicoloDTO fascicoloSelected;

	/**
	 * Documento selezionato.
	 */
	private DocumentoFascicoloDTO documentoSelected;

	/**
	 * Docuemnto interno.
	 */
	private DetailDocumentRedDTO nuovoDocumentoInterno;

	/**
	 * Lista tipi documennto.
	 */
	private List<TipologiaDocumentoDTO> comboTipologiaDocumento;

	/**
	 * Documento interno.
	 */
	private transient UploadedFile docInterno;
	/** Documenti end. *************************************************/

	/**
	 * FALDONI start.
	 ****************************************************************/
	private boolean dlgFaldonaturaRendered;
	/**
	 * FALDONI end.
	 ******************************************************************/

	/**
	 * E' il nome di chi apre la dialog che deve essere aggiornato dopo le singole
	 * operazioni che cambiano lo stato del fascicolo.
	 */
	private String chiamante;

	/**
	 * Flag inserisci documneto.
	 */
	private boolean canInsertDocument;

	/**
	 * Allegato.
	 */
	private AllegatoDTO allegatoFattura;

	/**
	 * Servizio.
	 */
	private IAllegatoFacadeSRV allegatoSRV;

	/**
	 * Documento fascicolo.
	 */
	private DocumentoFascicoloDTO documentoFascicoloFattura;

	/**
	 * Flag fascicolo visibile.
	 */
	private boolean dialogFascicoloVisible;

	/**
	 * Fascicolo selezionato.
	 */
	private FascicoloDTO currentFascicolo;

	/**
	 * Flag ricerca avviata.
	 */
	private boolean flagRicercaAvviata;

	/**
	 * Flag ricerca fascicolo.
	 */
	private boolean flagRicercaFascicolo;

	/**
	 * 
	 */
	private DialogFaldonaComponent faldonaCmp;

	/**
	 * Flag apri fascicolo.
	 */
	private boolean disableApriFascicolo;

	/**
	 * Chiamante.
	 */
	private IUpdatableDetailCaller<DetailFascicoloRedDTO> caller;

	/**
	 * Azione chiusura dialog.
	 */
	private String azioneAlCloseDialog;

	/**
	 * Component visualizza documenti.
	 */
	private VisualizzaDettaglioDocumentoComponent viewDettaglioDoc;

	/**
	 * Documento selezionato.
	 */
	private DocumentoFascicoloDTO docSelectedFas;

	/**
	 * Flag visualizza dialog dettaglio.
	 */
	private Boolean visualizzaDlgDettaglioDocumento;

	/**
	 * Dettaglio.
	 */
	private DetailDocumentRedDTO documentoDto;

	/**
	 * variabile per ricerca fascicoli da documetni component.
	 */
	private RicercaFascicoloDaDocumentoComponent tabRicercaDoc;

	/**
	 * Dati protocollo.
	 */
	private ProtocolliPregressiDTO protocolloByID;

	/**
	 * Servizio.
	 */
	private IDfFacadeSRV dfSrv;
	
	/**
	 * Service NPS.
	 */
	private INpsFacadeSRV npsSRV;
	
	/**
	 * Protocollo NPS.
	 */
	private ProtocolloNpsDTO protocolloNpsDTO;
	
	/**
	 * Dettaglio doc.
	 */
	private DetailNpsComponent dettaglioDocNps;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {

		try {
			final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
			utente = sessionBean.getUtente();
			tabRicercaDoc = new RicercaFascicoloDaDocumentoComponent(utente, MBean.FASCICOLO_MANAGER_BEAN);

			documentManagerSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentManagerFacadeSRV.class);
			documentoRedSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoRedFacadeSRV.class);
			documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);
			titolarioSRV = ApplicationContextProvider.getApplicationContext().getBean(ITitolarioFacadeSRV.class);
			fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloFacadeSRV.class);
			ricercaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaFacadeSRV.class);
			detailFascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IDetailFascicoloFacadeSRV.class);
			salvaDocumentoSRV = ApplicationContextProvider.getApplicationContext().getBean(ISalvaDocumentoFacadeSRV.class);
			faldoneSRV = ApplicationContextProvider.getApplicationContext().getBean(IFaldoneFacadeSRV.class);
			tipologiaDocumentoSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoFacadeSRV.class);
			dfSrv = ApplicationContextProvider.getApplicationContext().getBean(IDfFacadeSRV.class);
			
			npsSRV = ApplicationContextProvider.getApplicationContext().getBean(INpsFacadeSRV.class);
			creaTitolario();
			salvaTitolarioDisabled = true;

			fascicoliCercati = new ArrayList<>();

			nuovoDocumentoInterno = new DetailDocumentRedDTO();

			flagRicercaAvviata = false;
			flagRicercaFascicolo = false;

			viewDettaglioDoc = new VisualizzaDettaglioDocumentoComponent(utente);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}

	}

	/**
	 * Imposta il dettaglio del fascicolo.
	 * 
	 * @param detail
	 */
	public void setDetail(final DetailFascicoloRedDTO detail) {
		this.detail = detail;
		titolarioNodoSelectedDTO = detail.getTitolarioDTO();

		disableApriFascicolo = "APERTO".equalsIgnoreCase(detail.getStato());

		final FascicoloFepaBean fascFepaBean = (FascicoloFepaBean) FacesHelper.getManagedBean(MBean.FASCICOLO_FEPA_BEAN);
		fascFepaBean.init();
	}

	/**
	 * Restituisce lo StreamedContent per il download del documento selezionato.
	 * 
	 * @param index
	 * @return StreamedContent per download
	 */
	public StreamedContent downloadDocSelected(final Object index) {

		StreamedContent file = null;
		try {
			final Integer i = (Integer) index;

			final DocumentoFascicoloDTO docSelected = this.detail.getDocumenti().get(i);

			final InputStream stream = documentManagerSRV.getStreamDocumentoByGuid(docSelected.getGuid(), utente);

			file = new DefaultStreamedContent(stream, docSelected.getMimeType(), docSelected.getNomeFile());

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ERRORE_DURANTE_L_OPERAZIONE_DI_RECUPERO_DEL_DOCUMENTO);
		}
		return file;

	}

	/**
	 * Apre il popup per la gestione del documento.
	 * 
	 * @param index
	 */
	public void openDocumentPopup(final Object index) {

		try {
			final Integer i = (Integer) index;

			final DocumentoFascicoloDTO docSelected = this.detail.getDocumenti().get(i);

			final DetailDocumentRedDTO docRedDTO = documentoRedSRV.getDocumentDetail(docSelected.getDocumentTitle(), utente, null, null);

			final DocumentManagerBean bean = FacesHelper.getManagedBean(Constants.ManagedBeanName.DOCUMENT_MANAGER_BEAN);
			bean.setChiamante(chiamante);
			bean.setDetail(docRedDTO);

			this.popupDocumentVisible = true;
			FacesHelper.executeJS("PF('dlgManageDocument_DF').show()");
			FacesHelper.executeJS("PF('dlgManageDocument_DF').toggleMaximize()");

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ERRORE_DURANTE_L_OPERAZIONE_DI_RECUPERO_DEL_DOCUMENTO);
		}
	}

	/**
	 * TITOLARIO TREE START.
	 ******************************************************************************************/
	/**
	 * Gestisce la creazione del titolario.
	 */
	public void creaTitolario() {

		final List<TitolarioDTO> nodiRoot = titolarioSRV.getFigliRadice(utente.getIdAoo(), utente.getIdUfficio());
		if (nodiRoot == null || nodiRoot.isEmpty()) {
			showWarnMessage("Nessun titolario disponibile.");
			return;
		}

		try {
			final TitolarioDTO root = new TitolarioDTO("", "", "Titolario", 0, utente.getIdAoo(), null, null, nodiRoot.size());
			rootTitolario = new DefaultTreeNode(root, null);
			for (final TitolarioDTO t : nodiRoot) {
				final DefaultTreeNode a = new DefaultTreeNode(t, rootTitolario);
				if (t.getNumeroFigli() > 0) {
					new DefaultTreeNode(null, a);
				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ERRORE_DURANTE_L_OPERAZIONE + e.getMessage());
		}

	}

	/**
	 * Gestisce l'evento "Expand" del nodo del tree associato ai titolari.
	 * 
	 * @param event
	 */
	public void onTitolarioNodeExpand(final NodeExpandEvent event) {
		try {
			final TitolarioDTO toOpen = (TitolarioDTO) event.getTreeNode().getData();
			final List<TitolarioDTO> list = titolarioSRV.getFigliByIndice(utente.getIdAoo(), toOpen.getIndiceClassificazione());
			event.getTreeNode().getChildren().clear();
			if (list != null && !list.isEmpty()) {
				DefaultTreeNode indiceToAdd = null;
				for (final TitolarioDTO t : list) {
					indiceToAdd = new DefaultTreeNode(t, event.getTreeNode());
					if (t.getNumeroFigli() > 0) {
						new DefaultTreeNode(null, indiceToAdd);
					}
				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ERRORE_DURANTE_L_OPERAZIONE + e.getMessage());

		}

	}

	/**
	 * Gestisce l'evento "Select" del nodo del tree associato ai titolari.
	 * 
	 * @param event
	 */
	public void onTitolarioNodeSelect(final NodeSelectEvent event) {

		try {
			this.titolarioNodoSelected = (DefaultTreeNode) event.getTreeNode();
			if (this.titolarioNodoSelected != null) {
				this.salvaTitolarioDisabled = false;
			} else {
				this.salvaTitolarioDisabled = true;
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ERRORE_DURANTE_L_OPERAZIONE + e.getMessage());
		}
	}

	/**
	 * Rende persistenti sul database le informazioni sull'associazione del
	 * titolario.
	 */
	public void salvaTitolario() {
		if (titolarioNodoSelected == null && titolarioNodoSelectedDTO == null) {
			showWarnMessage("Nessun indice selezionato");
			return;
		}

		try {
			if (titolarioNodoSelected != null) { // ho selezionato il titolario dall'albero
				titolarioNodoSelectedDTO = (TitolarioDTO) titolarioNodoSelected.getData();
			} // altrimenti vengo dall'autocomplete

			this.detail.setTitolarioDTO(titolarioNodoSelectedDTO);
			this.detail.setTitolario(titolarioNodoSelectedDTO.getIndiceClassificazione() + " - " + titolarioNodoSelectedDTO.getDescrizione());

			if (ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN.equalsIgnoreCase(chiamante)) {
				final DettaglioDocumentoBean ddm = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN);
				if (ddm.getDocumentCmp() != null && ddm.getDocumentCmp().getDetail() != null) {
					for (final FascicoloDTO f : ddm.getDocumentCmp().getDetail().getFascicoli()) {
						if (f.getIdFascicolo().equals(this.detail.getNomeFascicolo())) {
							f.setIndiceClassificazione(titolarioNodoSelectedDTO.getIndiceClassificazione());
							break;
						}
					}
					final String titolarioDesc = titolarioNodoSelectedDTO.getIndiceClassificazione() + '-' + titolarioNodoSelectedDTO.getDescrizione();
					fascicoloSRV.associaATitolarioAggiornaNPS(utente, this.detail.getNomeFascicolo(), titolarioNodoSelectedDTO.getIndiceClassificazione(),
							titolarioDesc);

					ddm.getDocumentCmp().getDetail().setIndiceClassificazioneFascicoloProcedimentale(titolarioNodoSelectedDTO.getIndiceClassificazione());
					ddm.getDocumentCmp().getDetail().setDescrizioneTitolarioFascicoloProcedimentale(titolarioNodoSelectedDTO.getDescrizione());
				}
			} else if (ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN.equalsIgnoreCase(chiamante)) {
				final DettaglioFascicoloRicercaBean ddrm = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN);
				if (ddrm.fascicoloCmp != null && ddrm.fascicoloCmp.getDetail() != null) {

					final String titolarioDesc = titolarioNodoSelectedDTO.getIndiceClassificazione() + '-' + titolarioNodoSelectedDTO.getDescrizione();
					fascicoloSRV.associaATitolarioAggiornaNPS(utente, this.detail.getNomeFascicolo(), titolarioNodoSelectedDTO.getIndiceClassificazione(),
							titolarioDesc);
				}
			} else {
				this.caller.updateDetail(detail);
			}

			this.salvaTitolarioDisabled = true;
			showInfoMessage("Titolario aggiornato con successo");
			FacesHelper.update(EAST_SECTION_FORM_ID_DETTAGLIO_DOCUMENTO_DD);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di associazione");
		}
	}

	/**
	 * Restituisce la lista da visualizzare.
	 * 
	 * @return
	 */
	public List<TitolarioDTO> titolarioAutocomplete(final String parola) {
		List<TitolarioDTO> titolarioAutocompleteList = new ArrayList<>();

		try {
			titolarioAutocompleteList = titolarioSRV.getNodiAutocomplete(utente.getIdAoo(), utente.getIdUfficio(), parola);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore nel recupero delle voci del titolario.");
		}

		return titolarioAutocompleteList;
	}

	/**
	 * Gestisce l'autocompletamento del titpolario.
	 * 
	 * @param event
	 */
	public void onTitolarioAutocompleteSelect(final SelectEvent event) {

		try {
			this.titolarioNodoSelectedDTO = (TitolarioDTO) event.getObject();
			salvaTitolario();
			this.descrizioneTitolario = titolarioNodoSelectedDTO.getIndiceClassificazione() + " " + titolarioNodoSelectedDTO.getDescrizione();

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ERRORE_DURANTE_L_OPERAZIONE + e.getMessage());
		}
	}

	/**
	 * TITOLARIO TREE END.
	 ********************************************************************************************/

	/**
	 * DOCUMENTI SPOSTA COPIA DOCUMENTO START.
	 ******************************************************************************/
	/**
	 * Imposta {@link #documentoSelected} con il documento selezionato identificato
	 * dall'index ed effettua un reset dei valori della ricerca fascicolo.
	 * 
	 * @param index
	 */
	public void apriSpostaCopiaDocumento(final Object index) {

		try {
			final Integer i = (Integer) index;
			documentoSelected = detail.getDocumenti().get(i);

			// RESETTO I VALORI PER LA RICERCA DEL FASCICOLO IN CUI COPIARE/SPOSTARE
			fascicoliCercati = new ArrayList<>();
			fascicoloSelected = null;
			ricercaFascicoliAnno = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
			ricercaFascicoliParval = "";

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di associazione");
		}

	}

	/**
	 * Esegue la ricerca dei fascicoli e popola {@link #fascicoliCercati} con i
	 * risultati ottenuti.
	 */
	public void ricercaFascicolo() {
		flagRicercaFascicolo = false;
		Integer anno = null;
		if (!StringUtils.isNullOrEmpty(ricercaFascicoliAnno)) {
			try {
				anno = Integer.valueOf(ricercaFascicoliAnno);
			} catch (final NumberFormatException e) {
				anno = null;
			}
		}
		try {
			final Collection<FascicoloDTO> cf = ricercaSRV.ricercaGenericaFascicoli(ricercaFascicoliParval.trim(), anno, RicercaGenericaTypeEnum.TUTTE,
					RicercaPerBusinessEntityEnum.FASCICOLI, utente);
			// devo escludere il fascicolo che l'utente sta modificando e quelli che non
			// sono aperti
			fascicoliCercati = new ArrayList<>();
			for (final FascicoloDTO f : cf) {
				if (f.getIdFascicolo().equals(detail.getNomeFascicolo())) {
					continue;
				}
				fascicoliCercati.add(f);
			}
			flagRicercaFascicolo = true;
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore nel recupero dei fascicoli.");
		}

	}

	/**
	 * Gestisce lo spostamento del documento tra fascicoli.
	 */
	public void spostaDocumentoCheck() {
		setFlagRicercaFascicolo(false);
		try {
			if (tabRicercaDoc.getFascicoloSelezionato() != null) {
				fascicoloSelected = tabRicercaDoc.getFascicoloSelezionato();
			}

			if (fascicoloSelected == null) {
				showWarnMessage("Selezionare il fascicolo di destinazione.");
				return;
			}

			/**
			 * E' L'ULTIMO DOCUMENTO DEL FASCICOLO, IL FASCICOLO VERRA' CANCELLATO
			 *********************************************/
			if (detail.getDocumenti().size() == 1) {
				FacesHelper.executeJS("PF('wdgDlgSpostaDocEcancellaFascicoloConfirm_DF').show()");
				return;
			}

			spostaDocumentoCheckPostConfirmCancellazione();

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Non è stato possibile spostare il documento a causa di un errore inaspettato.");
		}
	}
	
	/**
	 * Gestisce lo spostamento del documento tra fascicoli.
	 */
	public void spostaDocumentoCheckPostConfirmCancellazione() {
		setFlagRicercaFascicolo(false);
		try {
			if (tabRicercaDoc.getFascicoloSelezionato() != null) {
				fascicoloSelected = tabRicercaDoc.getFascicoloSelezionato();
			}

			if (fascicoloSelected == null) {
				showWarnMessage("Selezionare il fascicolo di destinazione.");
				return;
			}

			/**
			 * DEVO CONTROLLARE CHE NON CI SIANO RICHIESTE CONTRIBUTO PENDENTI
			 *****************************************************/
			final Long idAoo = Long.valueOf(detail.getIdAOO());
			final boolean hasRichieste = documentoSRV.hasRichiesteContributoPendenti(documentoSelected.getDocumentTitle(), idAoo);
			if (hasRichieste) {
				FacesHelper.executeJS("PF('wdgDlgRichiestePendentiCopiaConfirm_DF').show()");
				return;
			}

			// ALTRIMENTI
			spostaDocumento();

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Non è stato possibile spostare il documento a causa di un errore inaspettato.");
		}
	}

	/**
	 * Esegue l'effettivo spostamento del {@link #documentoSelected} nel
	 * {@link #fascicoloSelected}.
	 */
	public void spostaDocumento() {		
		try {
			if (fascicoloSRV.verificaFascicoloFlusso(detail.getNomeFascicolo(), utente)) {
				showWarnMessage("Operazione non consentita: non è possibile spostare documenti da un fascicolo dedicato a un flusso automatico.");
				return;
			}
			
			final Boolean docPresente = isDocumentAlreadyPresent(documentoSelected.getDocumentTitle(), fascicoloSelected.getIdFascicolo());
			if (Boolean.FALSE.equals(docPresente)) {
				final Long idAoo = Long.valueOf(detail.getIdAOO());
				final String indiceClass = fascicoloSelected.getIndiceClassificazione() + " - " + fascicoloSelected.getDescrizioneTitolario();

				if (fascicoloSRV.spostaDocumento(documentoSelected.getDocumentTitle(), fascicoloSelected.getIdFascicolo(), detail.getNomeFascicolo(), idAoo, utente,
						documentoSelected.getIdProtocollo(), indiceClass)) {
					if (detail.getDocumenti().size() == 1) {
						detail.setDocumenti(null);
						closeDialog();
						FacesHelper.update(EAST_SECTION_FORM_ID_DETTAGLIO_DOCUMENTO_DD);
						showInfoMessage("Operazione avvenuta con successo. Il fascicolo è stato cancellato con lo spostamento dell'ultimo documento.");
					} else {
						detail.setDocumenti(detailFascicoloSRV.getDocumentiByIdFascicolo(Integer.valueOf(detail.getNomeFascicolo()), idAoo, utente));
						showInfoMessage(OPERAZIONE_AVVENUTA_CON_SUCCESSO);
					}
				} else {
					showError("Errore nell'operazione di spostamento del documento");
				}
			} else {
				showWarnMessage("Documento già presente nel fascicolo selezionato");
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore nell'operazione di spostamento del documento");
		}
	}

	private boolean isDocumentAlreadyPresent(final String idDocumento, final String idFascicolo) {
		return fascicoloSRV.documentoGiaPresenteInFascicolo(idDocumento, idFascicolo, utente);
	}

	/**
	 * Se il {@link #documentoSelected} non è già presente nel fascicolo ne esegue
	 * una copia.
	 */
	public void copiaDocumento() {
		try {
			
			if (fascicoloSRV.verificaFascicoloFlusso(detail.getNomeFascicolo(), utente)) {
				showWarnMessage("Operazione non consentita: non è possibile copiare documenti da un fascicolo dedicato a un flusso automatico.");
				return;
			}
			
			if (tabRicercaDoc.getFascicoloSelezionato() != null) {
				fascicoloSelected = tabRicercaDoc.getFascicoloSelezionato();
			}

			if (fascicoloSelected == null) {
				showWarnMessage("Selezionare il fascicolo di destinazione.");
				return;
			}

			final Long idAoo = Long.valueOf(detail.getIdAOO());
			final Boolean docPresente = isDocumentAlreadyPresent(documentoSelected.getDocumentTitle(), fascicoloSelected.getIdFascicolo());
			/* Se il documento è già presente nel fascicolo la copia non verrà eseguita */
			if (Boolean.FALSE.equals(docPresente)) {
				if (fascicoloSRV.copiaDocumento(documentoSelected.getDocumentTitle(), fascicoloSelected.getIdFascicolo(), detail.getNomeFascicolo(), idAoo, utente)) {
					showInfoMessage(OPERAZIONE_AVVENUTA_CON_SUCCESSO);
					setFlagRicercaFascicolo(false);
					FacesHelper.executeJS("PF('wdgSpostaCopiaDocumento_FM').hide()");
					FacesHelper.update(EAST_SECTION_FORM_ID_DETTAGLIO_DOCUMENTO_DD);
				} else {
					showWarnMessage("Errore nell'operazione di copia del documento");
				}
			} else {
				showWarnMessage("Documento già presente nel fascicolo selezionato");
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore nell'operazione di copia del documento");
		}
	}

	/**
	 * Aggiorna i paramtri associati al documento interno e aggiorna la
	 * {@link #comboTipologiaDocumento}.
	 */
	public void apriInserisciDocumento() {
		try {
			nuovoDocumentoInterno = new DetailDocumentRedDTO();
			nuovoDocumentoInterno.setIdCategoriaDocumento(CategoriaDocumentoEnum.INTERNO.getIds()[0]);

			if (comboTipologiaDocumento == null) {
				comboTipologiaDocumento = tipologiaDocumentoSRV.getComboTipologieByAooAndTipoCategoriaAttivi(TipoCategoriaEnum.USCITA, utente.getIdAoo());
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore nell'operazione di apertura maschera");
		}

	}

	/**
	 * Esegue la insert del {@link #nuovoDocumentoInterno} se tutte le informazioni
	 * fornite sono sufficienti, comunica un messaggio di errore altrimenti.
	 */
	public void inserisciDocumento() {
		try {

			if (StringUtils.isNullOrEmpty(nuovoDocumentoInterno.getOggetto()) || StringUtils.isNullOrEmpty(nuovoDocumentoInterno.getNomeFile())) {
				showWarnMessage("Inserire tutti i campi obbligatori");
				return;
			}

			final SalvaDocumentoRedParametriDTO sd = new SalvaDocumentoRedParametriDTO();
			sd.setModalita(Constants.Modalita.MODALITA_INS);
			sd.setContentVariato(true);
			sd.setIdFascicoloSelezionato(detail.getNomeFascicolo());

			final EsitoSalvaDocumentoDTO e = salvaDocumentoSRV.salvaDocumento(nuovoDocumentoInterno, sd, utente,
					ProvenienzaSalvaDocumentoEnum.GUI_RED_FASCICOLO_SENZA_TRASFORMAZIONE);

			if (e.isEsitoOk()) {
				final Integer idFascicolo = Integer.valueOf(detail.getNomeFascicolo());
				final Long idAOO = Long.valueOf(detail.getIdAOO());
				detail.setDocumenti(detailFascicoloSRV.getDocumentiByIdFascicolo(idFascicolo, idAOO, utente));

				showInfoMessage("Inserimento avvenuto con successo");
				FacesHelper.update(EAST_SECTION_FORM_ID_DETTAGLIO_DOCUMENTO_DD);
			} else {
				final StringBuilder sb = new StringBuilder("<ul>");
				for (final Enum<?> err : e.getErrori()) {
					sb.append("<li>");
					sb.append(err.name());
					sb.append("</li>");
				}
				sb.append("</ul>");
				showError(sb.toString());
			}
			nuovoDocumentoInterno = new DetailDocumentRedDTO();
			FacesHelper.executeJS("PF('wdgDocumentoInterno_DDI').hide()");
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore nell'inserimento del documento.");
		}

	}

	/**
	 * Gestisce l'upload del file del documento interno: {@link #docInterno}.
	 * 
	 * @param event
	 */
	public void handleDocInternoFileUpload(FileUploadEvent event) {
		try {
			event = checkFileName(event);
			if (event == null) {
				return;
			}
			docInterno = event.getFile();

			final String fileName = docInterno.getFileName();
			this.nuovoDocumentoInterno.setNomeFile(fileName);
			this.nuovoDocumentoInterno.setMimeType(docInterno.getContentType());
			this.nuovoDocumentoInterno.setContent(docInterno.getContents());

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ERRORE_DURANTE_L_OPERAZIONE + e.getMessage());
		}
	}

	/**
	 * Rimuove il file del {@link #docInterno}.
	 */
	public void rimuoviFile() {
		docInterno = null;
		this.nuovoDocumentoInterno.setNomeFile(null);
		this.nuovoDocumentoInterno.setMimeType(null);
		this.nuovoDocumentoInterno.setContent(null);
	}

	/**
	 * Elimina completamente il documento identificato dall'index.
	 * 
	 * @param index
	 */
	public void eliminaDocInterno(final Object index) {
		try {
			final Integer i = (Integer) index;
			final DocumentoFascicoloDTO docDaEliminare = detail.getDocumenti().get(i);
			final EsitoOperazioneDTO esito = documentoRedSRV.eliminaDocumentoInterno(docDaEliminare.getDocumentTitle(), Integer.parseInt(detail.getNomeFascicolo()), utente);
			if (esito.isEsito()) {
				detail.getDocumenti().remove(i.intValue());
				showInfoMessage("Documento eliminato correttamente.");
				FacesHelper.update(EAST_SECTION_FORM_ID_DETTAGLIO_DOCUMENTO_DD);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di eliminazione del documento interno ");
		}
	}

	/**
	 * DOCUMENTI SPOSTA COPIA DOCUMENTO END.
	 ********************************************************************************/

	/**
	 * FALDONI START.
	 ***********************************************************************************************************************/
	/**
	 * Apre la dialog di faldonatura in successione all'aggiornamento dei parametri
	 * per la corretta visualizzazione dello stesso.
	 */
	public void apriFaldonaFascicolo() {
		faldonaCmp = new DialogFaldonaComponent(utente);
		faldonaCmp.setFascicolo(new FascicoloDTO(detail.getNomeFascicolo(), detail.getOggetto(), detail.getTitolario(), detail.getDataCreazione(), detail.getStato(),
				detail.getGuid(), detail.getIdFascicoloFEPA()));

		final Collection<FaldoneDTO> faldoni = new ArrayList<>();
		final FascicoloDTO fascicolo = faldonaCmp.getFascicolo();
		fascicolo.setOggetto(detail.getOggetto());
		final List<FaldoneDTO> f = detailFascicoloSRV.getFaldoniByIdFascicolo(fascicolo.getIdFascicolo(), utente.getIdAoo(), utente);
		if (f != null && !f.isEmpty()) {
			for (final FaldoneDTO faldone : f) {
				faldone.setFascicoloSelezionato(fascicolo);
			}
			faldoni.addAll(f);
		}

		faldonaCmp.setFaldoni(faldoni);

		dlgFaldonaturaRendered = true;
	}

	/**
	 * Esegue la dissociazione del faldone identificato dall'index gestendo
	 * eventuali errori riscontrati.
	 * 
	 * @param index
	 */
	public void dissociaFaldone(final Object index) {
		try {
			final Integer i = (Integer) index;
			final FaldoneDTO fSelected = detail.getFaldoni().get(i);

			// Sse la lista di faldoni è == 1 devo controllare se i documenti contenuti nei
			// fascicoli abbiano il vincolo di faldonatura
			// se ce lo hanno non posso dissociare il faldone perché devono essere contenuti
			// in almeno un faldone
			EsitoFaldoneEnum esito = null;
			final Long idAOO = Long.valueOf(detail.getIdAOO());
			if (detail.getFaldoni().size() > 1 || faldoneSRV.isFaldoneDisassociabile(fSelected.getNomeFaldone(), detail.getNomeFascicolo(), idAOO, utente)) {
				esito = faldoneSRV.disassociaFascicoloDaFaldone(fSelected.getNomeFaldone(), detail.getNomeFascicolo(), idAOO, utente);
			} else {
				esito = EsitoFaldoneEnum.FALDONE_NON_DISASSOCIABILE;
			}

			switch (esito) {
			case ERRORE_GENERICO:
				showError(EsitoFaldoneEnum.ERRORE_GENERICO.getText());
				break;
			case FALDONE_DISASSOCIATO_DA_FASCICOLO_CON_SUCCESSO:
				
				finalizzaDisassociazioneFascicolo();
				break;
			case FALDONE_NON_DISASSOCIABILE:
				showWarnMessage(EsitoFaldoneEnum.FALDONE_NON_DISASSOCIABILE.getText());
				break;

			default:
				break;
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di dissociazione del faldone.");
		}
	}

	/**
	 * Esegue azioni di finalizzazione a valle della disassociazione del fascicolo.
	 */
	private void finalizzaDisassociazioneFascicolo() {
		
		final Long idAOOFas = Long.valueOf(detail.getIdAOO());
		detail.setFaldoni(detailFascicoloSRV.getFaldoniByIdFascicolo(detail.getNomeFascicolo(), idAOOFas, utente));

		// aggiorno chi mi ha chiamato
		if (ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN.equalsIgnoreCase(chiamante)) {
			final DettaglioFascicoloRicercaBean dfb = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN);
			dfb.getFascicoloCmp().getDetail().setFaldoni(detail.getFaldoni());
		} else if (ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN.equalsIgnoreCase(chiamante)) {
			final DettaglioDocumentoBean ddm = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN);
			if (ddm.getDocumentCmp() != null && ddm.getDocumentCmp().getDetail() != null) {
				ddm.getDocumentCmp().getDetail().setFaldoni(detail.getFaldoni());
			}
		}

		showInfoMessage(EsitoFaldoneEnum.FALDONE_DISASSOCIATO_DA_FASCICOLO_CON_SUCCESSO.getText());
		FacesHelper.update(EAST_SECTION_FORM_ID_DETTAGLIO_DOCUMENTO_DD);
	}

	/**
	 * Esegue l'associazione del fascicolo aggiungendolo al faldone del
	 * {@link #detail}.
	 */
	public void faldona() {

		try {

			final List<FaldoneDTO> faldoniList = faldonaCmp.registra();
			if (faldoniList.isEmpty()) {
				// l'esito è già stato mostrato nel bottone registra
				return;
			}

			if (detail.getFaldoni() == null) {
				detail.setFaldoni(new ArrayList<>());
			}

			detail.getFaldoni().addAll(faldoniList);

			if (ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN.equalsIgnoreCase(chiamante)) {
				final DettaglioFascicoloRicercaBean dfb = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN);
				if (dfb.getFascicoloCmp() != null && dfb.getFascicoloCmp().getDetail() != null) {
					dfb.getFascicoloCmp().getDetail().setFaldoni(detail.getFaldoni());
				}
			} else if (ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN.equalsIgnoreCase(chiamante)) {
				final DettaglioDocumentoBean ddm = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN);
				if (ddm.getDocumentCmp() != null && ddm.getDocumentCmp().getDetail() != null) {
					ddm.getDocumentCmp().getDetail().setFaldoni(detail.getFaldoni());
				}
			}

			closeDlgFaldonatura();
			flagRicercaAvviata = false;
			showInfoMessage(FASCICOLO_ASSOCIATO_CORRETTAMENTE);
			FacesHelper.update(EAST_SECTION_FORM_ID_DETTAGLIO_DOCUMENTO_DD, "idDettagliEstesiForm:idTabs_FM:idFaldonaFascicolo_FM",
					"idDettagliEstesiForm:idTabs_FM:idFaldoniTable_FM");
			FacesHelper.executeJS("PF('wdgFaldonaFascicolo_DLGFALD').hide()");
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}

	}

	/**
	 * Chiude la dialog di faldonatura resettandone i parametri.
	 */
	public void closeDlgFaldonatura() {
		try {
			FacesHelper.destroyBeanViewScope(MBean.FALDONA_BEAN);
			faldonaCmp = null;
			dlgFaldonaturaRendered = false;
			setFlagRicercaAvviata(false);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione.");
		}

	}

	/**
	 * FALDONI END.
	 *************************************************************************************************************************/
	/**
	 * Gestisce l'upload dell'allegato fattura.
	 * 
	 * @param event
	 */
	public void handleFileUploadAllegatoFattura(FileUploadEvent event) {
		try {
			event = checkFileName(event);
			if (event == null) {
				return;
			}
			allegatoFattura.setNomeFile(event.getFile().getFileName());
			allegatoFattura.setMimeType(event.getFile().getContentType());
			allegatoFattura.setContent(event.getFile().getContents());

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ERRORE_DURANTE_L_OPERAZIONE + e.getMessage());
		}

	}

	/**
	 * Gestisce l'eliminazione del file allegato fattura.
	 */
	public void rimuoviFileAllegatoFattura() {
		try {
			allegatoFattura = new AllegatoDTO();

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ERRORE_DURANTE_L_OPERAZIONE + e.getMessage());
		}
	}

	/**
	 * Aggiunge {@link #allegatoFattura} fattura al
	 * {@link #documentoFascicoloFattura}.
	 */
	public void aggiungiDocumentoAllegatoFattura() {
		try {

			if (StringUtils.isNullOrEmpty(allegatoFattura.getNomeFile()) || (allegatoFattura.getContent() == null || allegatoFattura.getContent().length == 0)) {
				throw new RedException("Inserire un allegato");
			}
			allegatoSRV = ApplicationContextProvider.getApplicationContext().getBean(IAllegatoFacadeSRV.class);
			allegatoSRV.allegaAFattura(documentoFascicoloFattura.getDocumentTitle(), allegatoFattura, utente);

			if (currentFascicolo != null) {
				detail = detailFascicoloSRV.getFascicoloDTOperTabFascicoloeFaldone(currentFascicolo, utente);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Gestisce i parametri per la corretta renderizzazione della dialog associato
	 * all'allegato fattura identificato dall'index e la renderizza.
	 * 
	 * @param index
	 */
	public void inizializzaAggiungiDocAllegatoFattura(final Object index) {
		try {
			setAllegatoFattura(new AllegatoDTO());
			final Integer i = (Integer) index;
			documentoFascicoloFattura = detail.getDocumenti().get(i);
			FacesHelper.executeJS("PF('dlgAgg_Allegato_FascFatt_DF').show()");
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Crea un nuovo Allegato impostandolo come {@link #allegatoFattura} e apre la
	 * dialgo di gestione dello stesso.
	 */
	public void inizializzaAggiungiDocAllegatoFattura() {
		try {
			setAllegatoFattura(new AllegatoDTO());
			FacesHelper.executeJS("PF('dlgAgg_Allegato_FascFatt_DF').show()");
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Gestisce l'eliminazione dell'alelgato fattura identificato dall'index.
	 * 
	 * @param index
	 */
	public void eliminaAllegatoFattura(final Object index) {
		try {
			final Integer i = (Integer) index;

			allegatoSRV = ApplicationContextProvider.getApplicationContext().getBean(IAllegatoFacadeSRV.class);
			allegatoSRV.eliminaAllegatiFepa(detail.getDocumenti().get(i).getDocumentTitle(), utente);

			showInfoMessage("Allegato eliminato con successo");

			if (currentFascicolo != null) {
				detail = detailFascicoloSRV.getFascicoloDTOperTabFascicoloeFaldone(currentFascicolo, utente);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Gestisce i parametri per la corretta renderizzazione della dialog di gestione
	 * documento recuperando le informazioni dal documento identificato dall'index e
	 * la renderizza.
	 * 
	 * @param index
	 */
	public void openDocumentView(final Object index) {
		try {
			final Integer i = (Integer) index;

			final DocumentoFascicoloDTO docSelected = this.detail.getDocumenti().get(i);

			final DocumentManagerBean bean = FacesHelper.getManagedBean(Constants.ManagedBeanName.DOCUMENT_MANAGER_BEAN);
			bean.setChiamante(MBean.FASCICOLO_MANAGER_BEAN);
			bean.setDetail(documentoRedSRV.getDocumentDetail(docSelected.getDocumentTitle(), utente, null, null));

			FacesHelper.executeJS("PF('dlgManageDocument_RM').show()");

			dialogFascicoloVisible = false;

			FacesHelper.update("idDettagliEstesiForm:idPanelDocumentManager");
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ERRORE_DURANTE_L_OPERAZIONE_DI_RECUPERO_DEL_DOCUMENTO);
		}
	}

	/**
	 * Restituisce true se il dettaglio è visualizzabile, false altrimenti.
	 * 
	 * @param docSelected per il quale occorre conoscera la visibilita del dettaglio
	 * @return true se il dettaglio è visualizzabile, false altrimenti
	 */
	public boolean detailOpenable(final DocumentoFascicoloDTO docSelected) {
		final boolean interno = (org.apache.commons.lang3.StringUtils.isNotBlank(docSelected.getDescTipoCategoriaDoc())
				&& TipoCategoriaEnum.INTERNO.getDescrizione().equals(docSelected.getDescTipoCategoriaDoc()))
				|| CategoriaDocumentoEnum.INTERNO.getIds()[0].equals(docSelected.getIdCategoria());

		return !interno && documentoRedSRV.hasDocumentDetail(docSelected.getTipoDocumento());
	}

	/**
	 * GESTIONE CHIUSURA DIALOG E RESET VALORI START.
	 ****************************************************************/
	/**
	 * Resetta tutti i campi eseguendo una pulizia della maschera.
	 */
	public void resetAll() {
		detail = null;
		popupDocumentVisible = false;
		salvaTitolarioDisabled = true;
		fascicoliCercati = new ArrayList<>();
		nuovoDocumentoInterno = new DetailDocumentRedDTO();

		/** Titolario start. ***********************************************/
		titolarioNodoSelected = null;
		titolarioNodoSelectedDTO = null;
		descrizioneTitolario = null;

		/** Documenti start. ***********************************************/
		ricercaFascicoliParval = null;
		ricercaFascicoliAnno = null;
		fascicoloSelected = null;
		documentoSelected = null;

		// documento interno
		docInterno = null;
		/** Documenti end. *************************************************/

		/**
		 * FALDONI start.
		 ****************************************************************/
		dlgFaldonaturaRendered = false;
		/**
		 * FALDONI end.
		 ******************************************************************/

		/**
		 * E' il nome di chi apre la dialog che deve essere aggiornato dopo le singole
		 * operazioni che cambiano lo stato del fascicolo
		 */
		chiamante = null;
		canInsertDocument = false;
	}

	/**
	 * Gestisce la chiusura della dialog dei fascicoli.
	 */
	public void closeDialog() {
		dialogFascicoloVisible = false;

		if (caller != null) {
			caller.updateDetail(detail);
		}

		FacesHelper.update("centralSectionForm:idDivRisultatiFaldoni_Fald");
		resetAll();
	}

	/**
	 * Reinizializza il component per la gestione della ricerca.
	 */
	public void resetDlgSpCp() {
		tabRicercaDoc = new RicercaFascicoloDaDocumentoComponent(utente, MBean.FASCICOLO_MANAGER_BEAN);
		setFlagRicercaFascicolo(false);
	}

	/**
	 * Gestisce la chiusura della dialog distruggendo il bean.
	 */
	public void closeDialogDestroyBean() {
		dialogFascicoloVisible = false;
		FacesContext.getCurrentInstance().getViewRoot().getViewMap().remove(MBean.FASCICOLO_MANAGER_BEAN);

	}

	/**
	 * GESTIONE CHIUSURA DIALOG E RESET VALORI END
	 ****************************************************************/
	/**
	 * Gestisce l'apertura del fascicolo.
	 */
	public void apriFascicolo() {
		try {
			fascicoloSRV.apriFascicolo(detail.getNomeFascicolo(), Long.valueOf(detail.getIdAOO()), utente);
			detail.setStato(FilenetStatoFascicoloEnum.APERTO.getNome());
			disableApriFascicolo = true;
			showInfoMessage("Operazione avvenuta con successo");
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di apertura del fascicolo. - " + e.getMessage());
		}
	}

	/**
	 * Aggiorna le informazioni associate al titolario del {@link #detail}.
	 * 
	 * @param nuovoIndiceClassificazione
	 */
	public void aggiornaTitolario(final String nuovoIndiceClassificazione) {
		TitolarioDTO nuovoTitolario = titolarioSRV.getNodoByIndice(detail.getIdAOO().longValue(), nuovoIndiceClassificazione);
		if (nuovoTitolario == null) {
			nuovoTitolario = new TitolarioDTO();
		}
		detail.setTitolarioDTO(nuovoTitolario);
		detail.setTitolario(nuovoTitolario.getIndiceClassificazione() + " - " + nuovoTitolario.getDescrizione());

		titolarioNodoSelectedDTO = nuovoTitolario;
		dialogFascicoloVisible = true;

		FacesHelper.update("idDettagliEstesiForm:ac_titolario");
	}

	/**
	 * Restituisce il contenuto del fascicolo sotto forma di ZipInputStream.
	 * 
	 * @return StreamedCOntent per download
	 */
	public StreamedContent downloadZipAllDocument() {
		StreamedContent file = null;
		try {
			final InputStream inputStream = fascicoloSRV.getZipFascicoloContents(detail.getNomeFascicolo(), utente);
			final Calendar rightNow = Calendar.getInstance();
			final String giornoMeseAnnoOraminutisecondi = DateUtils.dateToString(rightNow.getTime(), "ddMMyyyy_HHmmss");
			final String nome = org.apache.commons.lang3.StringUtils.join("Fascicolo-", detail.getNomeFascicolo(), "_", detail.getAnno(), "_", giornoMeseAnnoOraminutisecondi,
					".zip");
			file = new DefaultStreamedContent(inputStream, Constants.ContentType.ZIP_MIME, nome);
		} catch (final Exception e) {
			LOGGER.error("Errore durante lo scaricamento dello zip dei documenti", e);
			showError("Errore durante lo scaricamento dei documenti");
		}
		return file;
	}

	/** GET & SET. *************************************************/
	/**
	 * Restituisce il dettaglio.
	 * 
	 * @return detail
	 */
	public DetailFascicoloRedDTO getDetail() {
		return detail;
	}

	/**
	 * Restituisce true se il popup documenti è visibile, false altrimenti.
	 * 
	 * @return true se il popup documenti è visibile, false altrimenti
	 */
	public boolean isPopupDocumentVisible() {
		return popupDocumentVisible;
	}

	/**
	 * Restituisce la descrizione del titolario.
	 * 
	 * @return descrizione titolario
	 */
	public String getDescrizioneTitolario() {
		return descrizioneTitolario;
	}

	/**
	 * Restituisce la root del tree del titolario.
	 * 
	 * @return root titolario
	 */
	public DefaultTreeNode getRootTitolario() {
		return rootTitolario;
	}

	/**
	 * Restituisce true se il salvataggio titolario è disabilitato, false
	 * altrimenti.
	 * 
	 * @return true se il salvataggio titolario è disabilitato, false altrimenti
	 */
	public boolean isSalvaTitolarioDisabled() {
		return salvaTitolarioDisabled;
	}

	/**
	 * Restitusce il nodo selezionato del titolario.
	 * 
	 * @return nodo titolario selezionato
	 */
	public DefaultTreeNode getTitolarioNodoSelected() {
		return titolarioNodoSelected;
	}

	/**
	 * Imposta il nodo del titolario selezionato.
	 * 
	 * @param titolarioNodoSelected
	 */
	public void setTitolarioNodoSelected(final DefaultTreeNode titolarioNodoSelected) {
		this.titolarioNodoSelected = titolarioNodoSelected;
	}

	/**
	 * Restituisce l'anno di ricerca dei fascicoli.
	 * 
	 * @return anno ricerca fascicolo
	 */
	public String getRicercaFascicoliAnno() {
		return ricercaFascicoliAnno;
	}

	/**
	 * Imposta l'anno di ricerca per i fascicoli.
	 * 
	 * @param ricercaFascicoliAnno
	 */
	public void setRicercaFascicoliAnno(final String ricercaFascicoliAnno) {
		this.ricercaFascicoliAnno = ricercaFascicoliAnno;
	}

	/**
	 * Restituisce la lista dei fascicoli creati.
	 * 
	 * @return fascicoli creati
	 */
	public Collection<FascicoloDTO> getFascicoliCercati() {
		return fascicoliCercati;
	}

	/**
	 * Restituisce il fascicolo selezionato.
	 * 
	 * @return fascicolo
	 */
	public FascicoloDTO getFascicoloSelected() {
		return fascicoloSelected;
	}

	/**
	 * Imposta il fascicolo selezionato.
	 * 
	 * @param fascicoloSelected
	 */
	public void setFascicoloSelected(final FascicoloDTO fascicoloSelected) {
		this.fascicoloSelected = fascicoloSelected;
	}

	/**
	 * Restituisce il documento selezionato.
	 * 
	 * @return documento selezionato
	 */
	public DocumentoFascicoloDTO getDocumentoSelected() {
		return documentoSelected;
	}

	/**
	 * Imposta il documento selezionato.
	 * 
	 * @param documentoSelected
	 */
	public void setDocumentoSelected(final DocumentoFascicoloDTO documentoSelected) {
		this.documentoSelected = documentoSelected;
	}

	/**
	 * Restituisce il nuovo documento interno.
	 * 
	 * @return nuovo documento
	 */
	public DetailDocumentRedDTO getNuovoDocumentoInterno() {
		return nuovoDocumentoInterno;
	}

	/**
	 * Restituisce la lista delle tipologie documento per il popolamento della
	 * combobox.
	 * 
	 * @return lista tipi documento
	 */
	public List<TipologiaDocumentoDTO> getComboTipologiaDocumento() {
		return comboTipologiaDocumento;
	}

	/**
	 * Restituisce il chiamante.
	 * 
	 * @return chiamante
	 */
	public String getChiamante() {
		return chiamante;
	}

	/**
	 * Imposta il chiamante.
	 * 
	 * @param chiamante
	 */
	public void setChiamante(final String chiamante) {
		this.chiamante = chiamante;
	}

	/**
	 * @return canInsertDocument
	 */
	public boolean isCanInsertDocument() {
		return canInsertDocument;
	}

	/**
	 * @param canInsertDocument
	 */
	public void setCanInsertDocument(final boolean canInsertDocument) {
		this.canInsertDocument = canInsertDocument;
	}

	/**
	 * Restituisce l'allegato fattura.
	 * 
	 * @return allegato fattura
	 */
	public AllegatoDTO getAllegatoFattura() {
		return allegatoFattura;
	}

	/**
	 * Imposta l'allegato fattura.
	 * 
	 * @param allegatoFattura
	 */
	public void setAllegatoFattura(final AllegatoDTO allegatoFattura) {
		this.allegatoFattura = allegatoFattura;
	}

	/**
	 * Restituisce true se la dialog fascicolo è visibile, false altrimenti.
	 * 
	 * @return true se la dialog fascicolo è visibile, false altrimenti
	 */
	public boolean isDialogFascicoloVisible() {
		return dialogFascicoloVisible;
	}

	/**
	 * Imposta la visibilita della dialog fascicolo.
	 * 
	 * @param dialogFascicoloVisible
	 */
	public void setDialogFascicoloVisible(final boolean dialogFascicoloVisible) {
		this.dialogFascicoloVisible = dialogFascicoloVisible;
	}

	/**
	 * Restituisce il fascicolo corrente.
	 * 
	 * @return fascicolo corrente
	 */
	public FascicoloDTO getCurrentFascicolo() {
		return currentFascicolo;
	}

	/**
	 * Imposta il fascicolo corrente.
	 * 
	 * @param currentFascicolo
	 */
	public void setCurrentFascicolo(final FascicoloDTO currentFascicolo) {
		this.currentFascicolo = currentFascicolo;
	}

	/**
	 * Restituisce il nodo selezionato del titolario.
	 * 
	 * @return nodo selezionato
	 */
	public TitolarioDTO getTitolarioNodoSelectedDTO() {
		return titolarioNodoSelectedDTO;
	}

	/**
	 * Imposta il nodo selezionato del titolario.
	 * 
	 * @param titolarioNodoSelectedDTO
	 */
	public void setTitolarioNodoSelectedDTO(final TitolarioDTO titolarioNodoSelectedDTO) {
		this.titolarioNodoSelectedDTO = titolarioNodoSelectedDTO;
	}

	/**
	 * Restituisce true se la ricerca è avviata, false altrimenti.
	 * 
	 * @return true se la ricerca è avviata, false altrimenti
	 */
	public boolean isFlagRicercaAvviata() {
		return flagRicercaAvviata;
	}

	/**
	 * Imposta il flag: flagRicercaAvviata.
	 * 
	 * @param flagRicercaAvviata
	 */
	public void setFlagRicercaAvviata(final boolean flagRicercaAvviata) {
		this.flagRicercaAvviata = flagRicercaAvviata;
	}

	/**
	 * @return ricercaFascicoliParval
	 */
	public String getRicercaFascicoliParval() {
		return ricercaFascicoliParval;
	}

	/**
	 * @param ricercaFascicoliParval
	 */
	public void setRicercaFascicoliParval(final String ricercaFascicoliParval) {
		this.ricercaFascicoliParval = ricercaFascicoliParval;
	}

	/**
	 * Restituisce true se la dialog faldonatura è renderizzata, false altrimenti.
	 * 
	 * @return true se la dialog faldonatura è renderizzata, false altrimenti
	 */
	public boolean isDlgFaldonaturaRendered() {
		return dlgFaldonaturaRendered;
	}

	/**
	 * Imposta la visibilita della dialog di faldonatura.
	 * 
	 * @param dlgFaldonaturaRendered
	 */
	public void setDlgFaldonaturaRendered(final boolean dlgFaldonaturaRendered) {
		this.dlgFaldonaturaRendered = dlgFaldonaturaRendered;
	}

	/**
	 * Restituisce il component per la gestione della faldonatura.
	 * 
	 * @return il component
	 */
	public DialogFaldonaComponent getFaldonaCmp() {
		return faldonaCmp;
	}

	/**
	 * @return flagRicercaFascicolo
	 */
	public boolean isFlagRicercaFascicolo() {
		return flagRicercaFascicolo;
	}

	/**
	 * @param flagRicercaFascicolo
	 */
	public void setFlagRicercaFascicolo(final boolean flagRicercaFascicolo) {
		this.flagRicercaFascicolo = flagRicercaFascicolo;
	}

	/**
	 * Restituisce true se la funzionalita dell'apertura fascicolo è disabilitata,
	 * false altrimenti.
	 * 
	 * @return true se la apertura fascicolo è disabilitata, false altrimenti
	 */
	public boolean isDisableApriFascicolo() {
		return disableApriFascicolo;
	}

	/**
	 * Imposta il caller.
	 * 
	 * @param inCaller
	 */
	public void setCaller(final IUpdatableDetailCaller<DetailFascicoloRedDTO> inCaller) {
		caller = inCaller;
	}

	/**
	 * Restituisce il documento fascicolo.
	 * 
	 * @return documento fascicolo
	 */
	public DocumentoFascicoloDTO getDocumentoFascicoloFattura() {
		return documentoFascicoloFattura;
	}

	/**
	 * Imposta il documento fascicolo fattura.
	 * 
	 * @param documentoFascicoloFattura
	 */
	public void setDocumentoFascicoloFattura(final DocumentoFascicoloDTO documentoFascicoloFattura) {
		this.documentoFascicoloFattura = documentoFascicoloFattura;
	}

	/**
	 * @return azioneAlCloseDialog
	 */
	public String getAzioneAlCloseDialog() {
		return azioneAlCloseDialog;
	}

	/**
	 * @param azioneAlCloseDialog
	 */
	public void setAzioneAlCloseDialog(final String azioneAlCloseDialog) {
		this.azioneAlCloseDialog = azioneAlCloseDialog;
	}

	/**
	 * @return docSelectedFas
	 */
	public DocumentoFascicoloDTO getDocSelectedFas() {
		return docSelectedFas;
	}

	/**
	 * Restituisce il component per la gestione della visualizzazione del dettaglio
	 * del documento.
	 * 
	 * @return il component
	 */
	public VisualizzaDettaglioDocumentoComponent getViewDettaglioDoc() {
		return viewDettaglioDoc;
	}

	/**
	 * Imposta il component per la gestione della visualizzazione del dettaglio del
	 * documento.
	 * 
	 * @param viewDettaglioDoc
	 */
	public void setViewDettaglioDoc(final VisualizzaDettaglioDocumentoComponent viewDettaglioDoc) {
		this.viewDettaglioDoc = viewDettaglioDoc;
	}

	/**
	 * Restituisce il documento.
	 * 
	 * @return documento
	 */
	public DetailDocumentRedDTO getDocumentoDto() {
		return documentoDto;
	}

	/**
	 * Restituisce il flag per la gestione della visibilita del dettaglio documento.
	 * 
	 * @return flag visibilita dettaglio documento
	 */
	public Boolean getVisualizzaDlgDettaglioDocumento() {
		return visualizzaDlgDettaglioDocumento;
	}

	/**
	 * Imposta il flag per la gestione della visibilita del dettaglio documento.
	 * 
	 * @param visualizzaDlgDettaglioDocumento
	 */
	public void setVisualizzaDlgDettaglioDocumento(final Boolean visualizzaDlgDettaglioDocumento) {
		this.visualizzaDlgDettaglioDocumento = visualizzaDlgDettaglioDocumento;
	}

	/**
	 * Apre la dialog per la visualizzazione del dettaglio dopo averne impostato i
	 * parametri recuperandoli dal documento identificato dall'index.
	 * 
	 * @param index
	 */
	public void openVisualizzaDocumento(final Object index) {
		final Integer i = (Integer) index;
		docSelectedFas = this.detail.getDocumenti().get(i);
		documentoDto = documentoRedSRV.getDocumentDetail(docSelectedFas.getDocumentTitle(), utente, null, null);
		viewDettaglioDoc.setDetail(documentoDto);
		visualizzaDlgDettaglioDocumento = true;
	}

	/**
	 * Gestisce l'apertura del dettaglio di un documento interno identificato
	 * dall'index.
	 * 
	 * @param index
	 * @param dlgWidgetVar
	 */
	public void openVisualizzaDocumento(final Object index, final String dlgWidgetVar) {
		final Integer i = (Integer) index;
		docSelectedFas = this.detail.getDocumenti().get(i);
		if (CategoriaDocumentoEnum.INTERNO.getIds()[0].equals(docSelectedFas.getIdCategoria())) {
			showWarnMessage("Non è possibile aprire il dettaglio di un documento interno.");
			return;
		}
		documentoDto = documentoRedSRV.getDocumentDetail(docSelectedFas.getDocumentTitle(), utente, null, null);
		viewDettaglioDoc.setDetail(documentoDto);
		visualizzaDlgDettaglioDocumento = true;
		FacesHelper.executeJS("PF('" + dlgWidgetVar + "').show();");
		FacesHelper.executeJS("PF('" + dlgWidgetVar + "').toggleMaximize();");
	}

	/**
	 * Restituisce il component per la gestione del tab di ricerca.
	 * 
	 * @return il component
	 */
	public RicercaFascicoloDaDocumentoComponent getTabRicercaDoc() {
		return tabRicercaDoc;
	}

	/**
	 * Imposta il component che gestisce la ricerca dei documenti.
	 * 
	 * @param ricercaTab
	 */
	public void setTabRicercaDoc(final RicercaFascicoloDaDocumentoComponent ricercaTab) {
		this.tabRicercaDoc = ricercaTab;
	}

	/**
	 * Imposta il valore di {@link #protocolloByID} recuperando le informazoni dai
	 * parametri in ingresso.
	 * 
	 * @param nProtocolloNsd
	 * @param annoProtocolloNsd
	 */
	public void getDettaglioRiferimentoStoricoNsd(final Integer nProtocolloNsd, final Integer annoProtocolloNsd) {
		final String nProtocolloNsdString = String.valueOf(nProtocolloNsd);
		final String annoProtocolloNsdString = String.valueOf(annoProtocolloNsd);
		protocolloByID = dfSrv.getProtocolloById(utente, nProtocolloNsdString, annoProtocolloNsdString);

		if (protocolloByID == null) {
			showError("Nessun protocollo trovato verso NSD");
		}
	}

	/**
	 * Restituisce il valore di: protocolloById.
	 * 
	 * @return protocolloByID
	 */
	public ProtocolliPregressiDTO getProtocolloByID() {
		return protocolloByID;
	}

	/**
	 * Imposta il valore di: protocolloById.
	 * 
	 * @param protocolloByID
	 */
	public void setProtocolloByID(final ProtocolliPregressiDTO protocolloByID) {
		this.protocolloByID = protocolloByID;
	}

	/**
	 * Restituisce l'utente.
	 * 
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Restituisce il prefisso che definisce un documento in entrata.
	 * 
	 * @param d
	 * @return label prefisso doc entrata
	 */
	public String getCharEntrata(final DocumentoFascicoloDTO d) {
		String output = "E";
		if (d != null && d.isIntegrazioneDati()) {
			output += " (" + Constants.Varie.CODICE_INTEGRAZIONE_DATI + ")";
		}
		return output;
	}

	/**
	 * Restituisce il prefisso che definisce un documento in uscita.
	 * 
	 * @param d
	 * @return label prefisso doc entrata
	 */
	public String getCharUscita(final DocumentoFascicoloDTO d) {
		String output = "U";
		if (d != null && d.getSottoCategoriaDocUscita() != null && d.getSottoCategoriaDocUscita() != 0) {
			final Integer id = d.getSottoCategoriaDocUscita();
			output += " (" + SottoCategoriaDocumentoUscitaEnum.get(id).getCodice() + ")";
		}
		return output;
	}
	
	/**
	 * Restituisce lo streamed content per il download file nps.
	 * @param idDocumento
	 * @return streamedContent
	 */
	public StreamedContent downloadFileNps(String idDocumento) {
		StreamedContent file = null; 
		try {    
			//Nel fascicolo bisogna sempre accedere con true per richiedere il download in modalità admin
			DocumentoNpsDTO doc = npsSRV.downloadDocumento(utente.getIdAoo().intValue(), idDocumento,true); 

			InputStream stream = null;
			if (doc.getInputStream() != null) {
				stream = doc.getInputStream();
			} else {
				throw new RedException("Nessun content present");
			}

			file = new DefaultStreamedContent(stream, doc.getContentType(), doc.getNomeFile());
		} catch (Exception e) { 
				LOGGER.error("Errore durante il download del file: ", e);
				showError("Errore durante il download del file"); 
		}
		return file;
	}
	
	/**
	 * Restituisce il dettaglio doc NPS.
	 * @return dettaglio doc nps
	 */
	public DetailNpsComponent getDettaglioDocNps() {
		return dettaglioDocNps;
	}

	/**
	 * Imposta il dettaglio doc nps.
	 * @param dettaglioDocNps
	 */
	public void setDettaglioDocNps(DetailNpsComponent dettaglioDocNps) {
		this.dettaglioDocNps = dettaglioDocNps;
	}

	/**
	 * Calcola dettaglio nps.
	 * @param annoProt
	 * @param idProtocollo
	 */
	public void calcolaDetailNps(Integer annoProt,Integer idProtocollo) {
		protocolloNpsDTO = new ProtocolloNpsDTO();
		try {    
			ParamsRicercaProtocolloDTO ricercaProt = new ParamsRicercaProtocolloDTO();
			ricercaProt.setAnnoProtocollo(annoProt);
			ricercaProt.setNumeroProtocolloDa(idProtocollo);
			ricercaProt.setNumeroProtocolloA(idProtocollo);
			protocolloNpsDTO = npsSRV.ricercaProtocolli(ricercaProt, utente, true).get(0);
			if (protocolloNpsDTO != null) {
				protocolloNpsDTO = npsSRV.getDettagliProtocollo(protocolloNpsDTO, utente.getIdAoo().intValue(), utente, true);
				dettaglioDocNps = new DetailNpsComponent();
				dettaglioDocNps.setDetail(protocolloNpsDTO);
				FacesHelper.update("idDettagliEstesiForm:idTabs_FM:dlgDettaglioFascicoloNpsId");
			} 
		} catch (Exception e) { 
				LOGGER.error("Errore durante il download del file: ", e);
				showError("Errore durante il download del file"); 
		} 
	}

	/**
	 * Restituisce il protocollo NPS.
	 * @return DTO protocollo NPS
	 */
	public ProtocolloNpsDTO getProtocolloNpsDTO() {
		return protocolloNpsDTO;
	}

	/**
	 * Imposta il protocollo NPS.
	 * @param protocolloNpsDTO
	 */
	public void setProtocolloNpsDTO(ProtocolloNpsDTO protocolloNpsDTO) {
		this.protocolloNpsDTO = protocolloNpsDTO;
	}
}
