package it.ibm.red.web.mbean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IPkHandlerFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb.MBean;

/**
 * @author m.crescentini
 */
@Named(MBean.LOGIN_BEAN)
@RequestScoped
public class LoginBean extends AbstractBean {

	private static final long serialVersionUID = 7218414445990671385L;

	/**
	 * PK handler.
	 */
	private List<String> pkHandlerUrlList;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		IPkHandlerFacadeSRV pkHandlerSRV = ApplicationContextProvider.getApplicationContext().getBean(IPkHandlerFacadeSRV.class);
		pkHandlerUrlList = pkHandlerSRV.getAllUrl();
	}

	/**
	 * Restituisce la lista di URL del PKHandler.
	 * @return lista di url
	 */
	public List<String> getPkHandlerUrlList() {
		return pkHandlerUrlList;
	}
}
