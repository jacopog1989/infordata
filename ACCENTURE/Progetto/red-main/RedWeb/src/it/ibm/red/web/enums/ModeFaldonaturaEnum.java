package it.ibm.red.web.enums;

/**
 * Enum che definisce la modalità di faldonatura.
 */
public enum ModeFaldonaturaEnum {
	
	/**
	 * Valore.
	 */
	CREATE("C", "Creazione Faldone"),
	
	/**
	 * Valore.
	 */
	UPDATE("U", "Aggiornamento Faldone"),

	/**
	 * Valore.
	 */
	DELETE("D", "Cancellazione Faldone");
	
	/**
	 * Type.
	 */
	private String type;	

	/**
	 * Display name.
	 */
	private String displayName;

	ModeFaldonaturaEnum(final String inType, final String inDisplayName) {
		type = inType;
		displayName = inDisplayName;
	}
	
	/**
	 * Restituisce il type associato all'enum.
	 * @return type
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Restitusice il display name dell'enum.
	 * @return display enum
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Restituisce l'enum associata al type.
	 * @param type
	 * @return
	 */
	public static ModeFaldonaturaEnum get(final String type) {
		ModeFaldonaturaEnum output = null;
		for (final ModeFaldonaturaEnum t:ModeFaldonaturaEnum.values()) {
			if (type.equals(t.getType())) {
				output = t;
				break;
			}
		}
		return output;
	}
}