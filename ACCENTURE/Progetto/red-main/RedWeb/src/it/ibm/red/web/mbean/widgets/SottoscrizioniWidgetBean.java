/** La Classe EMailBean
 *  Utilizzata per la gestione delle mail
 */

package it.ibm.red.web.mbean.widgets;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.NotificaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.INotificaFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.enums.SottoscrizioniIconEnum;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;

/**
 * Bean che gestisce il widget delle sottoscrizioni.
 */
@Named(ConstantsWeb.MBean.SOTTOSCRIZIONI_WIDGET_BEAN)
@ViewScoped
public class SottoscrizioniWidgetBean extends AbstractBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -6515254659346515469L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SottoscrizioniWidgetBean.class.getName());

	/**
	 * Datatable notifiche.
	 */
	private SimpleDetailDataTableHelper<NotificaDTO> notificheDTH;

	/**
	 * Servizio.
	 */
	private INotificaFacadeSRV notificaSRV;

	/**
	 * Notifica selezionata.
	 */
	private Integer idNotificaSelezionata;
	 
	/**
	 * Bean di sessione.
	 */
	private SessionBean sessionBean;
	 
	/**
	 * Utente.
	 */
	private UtenteDTO utente;
	 
	/**
	 * Icona sottoscrizione.
	 */
	private String iconaSottoscrizione;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	public void postConstruct() {
		sessionBean = (SessionBean) FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		notificaSRV = ApplicationContextProvider.getApplicationContext().getBean(INotificaFacadeSRV.class);
		notificheDTH = new SimpleDetailDataTableHelper<>(notificaSRV.recuperaNotifiche(utente.getId(), utente.getIdAoo(), utente.getFcDTO(), 90));
		 
		for (final NotificaDTO notifica : notificheDTH.getMasters()) {
			notifica.setIconaSottoscrizione(SottoscrizioniIconEnum.get(notifica.getDescrizioneEvento()));
		} 
	}
	
	/**
	 * Restituisce l'helper per la gestione del datatable delle notifiche.
	 * @return helper datatable notifiche
	 */
	public SimpleDetailDataTableHelper<NotificaDTO> getNotificheDTH() {
		return notificheDTH;
	}

	/**
	 * Consente di reindirizzare l'utente sulla pagina delle sottoscrizioni.
	 * @return url pagina sottoscrizioni
	 */
	public String goToNotifiche() {
		sessionBean.setIsGoToSottoscrizioni(true); 
		sessionBean.setIdNotificaSelezionataSott(getIdNotificaSelezionata());
		return sessionBean.gotoSottoscrizioni();
	}
	
	/**
	 * Restituisce l'id della notifica selezionata.
	 * @return id notifica selezionata
	 */
	public Integer getIdNotificaSelezionata() {
		return idNotificaSelezionata;
	}
	
	/**
	 * Imposta l'id della notifica selezionata.
	 * @param idNotificaSelezionata
	 */
	public void setIdNotificaSelezionata(final Integer idNotificaSelezionata) {
		this.idNotificaSelezionata = idNotificaSelezionata;
	}
	
	/**
	 * Esegue la contrassegnazione della notifica identificata dalla riga selezionata dal datatable.
	 * @param event
	 */
	public void contrassegnaSingolo(final ActionEvent event) {
		final NotificaDTO element = getDataScrollerClickedRow(event);
		contrassegnaNotifica(element, notificheDTH);
	}
	
	@Override
	protected List<NotificaDTO> updateNotificheTbl(final Integer idNotifica) {
		List<NotificaDTO> notifiche = new ArrayList<>();
		
		try { 
			if (idNotifica != null) { 																		 
				notifiche = notificaSRV.recuperaNotifiche(idNotifica, utente.getId(), utente.getIdAoo(), utente.getFcDTO());
				for (final NotificaDTO notifica : notifiche) {
					notifica.setIconaSottoscrizione(SottoscrizioniIconEnum.get(notifica.getDescrizioneEvento()));
				} 
				FacesHelper.executeJS("calculateIframeHeight()");
			} else {
				notifiche = notificaSRV.recuperaNotifiche(utente.getId(), utente.getIdAoo(), utente.getFcDTO(), 90);
				for (final NotificaDTO notifica : notifiche) {
					notifica.setIconaSottoscrizione(SottoscrizioniIconEnum.get(notifica.getDescrizioneEvento()));
				}
			}
			} catch (final Exception e) {
			showError("Errore durante l'aggiornamento delle notifiche. " + e.getMessage());
			LOGGER.error(e);
		}
		
		return notifiche;
	}
	
	/**
	 * Gestisce la rimozione della sottoscrizione selzionata dal datatable.
	 * @param event
	 */
	public void rimuoviSingolo(final ActionEvent event) {
		final NotificaDTO element = getDataScrollerClickedRow(event);
		rimuoviNotifica(element, notificheDTH);
	}
	
	/**
	 * Restituisce l'icone della sottoscrizione.
	 * @return icona sottoscrizione
	 */
	public String getIconaSottoscrizione() {
		return iconaSottoscrizione;
	}
	
	/**
	 * Imposta l'icona della sottoscrizione.
	 * @param iconaSottoscrizione
	 */
	public void setIconaSottoscrizione(final String iconaSottoscrizione) {
		this.iconaSottoscrizione = iconaSottoscrizione;
	}
}
