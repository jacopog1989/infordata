package it.ibm.red.web.mbean;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import com.google.common.net.MediaType;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.RecuperoCodeDTO;
import it.ibm.red.business.dto.ResponsesDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.VersionDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IASignFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV;
import it.ibm.red.business.service.facade.IResponseRedFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaFacadeSRV;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.mbean.component.DetailDocumentRedComponent;
import it.ibm.red.web.document.mbean.component.DettaglioDocumentoStoricoComponent;
import it.ibm.red.web.document.mbean.component.DocumentManagerDataTabApprovazioniComponent;
import it.ibm.red.web.dto.GoToCodaDTO;
import it.ibm.red.web.enums.DettaglioDocumentoFileEnum;
import it.ibm.red.web.enums.NavigationTokenEnum;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.component.DetailPreviewIntroComponent;
import it.ibm.red.web.mbean.interfaces.IGoToCodaButtonComponent;
import it.ibm.red.web.mbean.interfaces.IMenuAllegatiButtonAndPanelComponent;
import it.ibm.red.web.mbean.interfaces.IOpenDettaglioEstesoButtonComponent;
import it.ibm.red.web.servlets.DownloadContentServlet;
import it.ibm.red.web.servlets.DownloadContentServlet.DocumentTypeEnum;

/**
 * Abstract bean per la gestione dei dettagli documenti.
 */
public abstract class DettaglioDocumentoAbstractBean extends AbstractBean
		implements IGoToCodaButtonComponent, IMenuAllegatiButtonAndPanelComponent, IOpenDettaglioEstesoButtonComponent {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 2518100245925469323L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DettaglioDocumentoAbstractBean.class);

	/**
	 * Servizio.
	 */
	private IRicercaFacadeSRV ricercaSRV;

	/**
	 * Code non censite.
	 */
	private String listaCodeNonCensite;

	/**
	 * Contatori code.
	 */
	private EnumMap<DocumentQueueEnum, Integer> codeCount;

	/**
	 * Info utente.
	 */
	protected UtenteDTO utente;

	/**
	 * Servizio.
	 */
	protected IDocumentoRedFacadeSRV documentoRedSRV;

	/**
	 * Servizio.
	 */
	protected IDocumentManagerFacadeSRV documentManagerSRV;

	/**
	 * Servizio.
	 */
	protected IResponseRedFacadeSRV responseRedSRV;

	/**
	 * Servizio.
	 */
	protected IDocumentoFacadeSRV documentoSRV;
	
	/**
	 * Servizio.
	 */
	protected IASignFacadeSRV aSignSRV;

	/**
	 * Componente utilizzato per il tab dettaglio e tab documenti.
	 */
	protected DetailDocumentRedComponent documentCmp;

	/**
	 * Component per il tab storico.
	 */
	protected DettaglioDocumentoStoricoComponent storico;

	/**
	 * Items string.
	 */
	protected static final String ITEMS = "{items:";

	/**
	 * Dimensione massima file.
	 */
	protected static final int FILENAME_SIZE = 40;

	/**
	 * Id tab dettaglio.
	 */
	protected static final String UPDATE_TAB_DETTAGLIO_DEFAULT = "eastSectionForm:tabDetails";

	/**
	 * Id tab preview.
	 */
	protected static final int TAB_PREVIEW = 0;

	/**
	 * Update tab dettaglio.
	 */
	protected String updateTabDettaglio;

	/**
	 * Flag abilita update.
	 */
	protected Boolean updateEnabled;

	/**
	 * Flag popup visibile.
	 */
	protected boolean popupDocumentVisible;

	/**
	 * Document title del documento selezionato.
	 */
	protected String documentTitleSelected;

	/**
	 * mimetype del file visualizzato nella preview.
	 */
	protected String mimeTypeSelected;

	/**
	 * nome del file visualizzato nella preview.
	 */
	protected String nomeFileSelected;
	/**
	 * Menù detail.
	 */
	protected transient MenuModel menuAllegatiDetailModel;
	/**
	 * Costruisce il menu degli allegati, si trovano nell'eastSectionButton.
	 * 
	 */
	protected transient String menuAllegatiCommand;

	/**
	 * Flag modificabile.
	 */
	protected boolean modificabile;

	/**
	 * Detail.
	 */
	protected transient Collection<ResponsesRedEnum> responseDetail;

	/**
	 * Component per il solo tab della preview del documento.
	 */
	protected DetailPreviewIntroComponent previewComponent;
	/**
	 * Flag che afferma se è attivo intro della preview. Quando il flag è true il
	 * dettaglio non è ancora stato caricato, al cliccare di un pulsante il
	 * dettaglio viene caricato
	 */
	protected boolean introPreviewActive;

	/**
	 * Flag che calcola quando è corretto o meno visualizzare il dettaglio. Mi
	 * aspetto che dipenda dall'input se l'input è malformato il dettaglio non può
	 * essere visualizzato
	 */
	protected boolean visibleDetail = false;

	/**
	 * Serve a impostare il primo tab come il tab selezionato dopo il setDetail.
	 */
	protected int activeTab;

	/**
	 * Enum utilizzato quando il dettaglioAbstract viene usato anche da FEPA
	 * 
	 * Ammetto che questo abstract non semplifica ne gestisce un comportamento in
	 * comune oltre allo storico e il preview. Il senso di questo abstract oramai
	 * non esiste quasi più perché i comportamenti sono talmente tanto differenti
	 * che non avrebbe nemmeno più senso dargli una interfaccia in comune Mi piange
	 * il cuore.
	 */
	protected DettaglioDocumentoFileEnum dettaglioDocEnum;

	/**
	 * Bean di sessione.
	 */
	protected SessionBean sessionBean;

	/**
	 * Component approvazioni.
	 */
	private DocumentManagerDataTabApprovazioniComponent approvazioniTab;

	/**
	 * aSignEnabled.
	 */
	private Boolean aSignEnabled;

	/**
	 * Indica se lo storico è già stato caricato.
	 */
	private boolean storicoGiaCaricato;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	protected void postConstruct() {
		/**
		 * DEVE ESSERE IMPLEMENTATO NELLE IMPLEMENTAZIONI DI QUESTA CLASSE CON
		 * L'ANNOTAZIONE: @PostConstruct ALTRIMENTI IL METODO postConstruct() NON PARTE
		 * e ognuna deve richiamare super.postConstruct()
		 **/
		sessionBean = FacesHelper.getManagedBean(Constants.ManagedBeanName.SESSION_BEAN);
		utente = sessionBean.getUtente();

		updateEnabled = false;

		documentoRedSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoRedFacadeSRV.class);
		documentManagerSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentManagerFacadeSRV.class);
		responseRedSRV = ApplicationContextProvider.getApplicationContext().getBean(IResponseRedFacadeSRV.class);
		ricercaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaFacadeSRV.class);
		documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);
		aSignSRV = ApplicationContextProvider.getApplicationContext().getBean(IASignFacadeSRV.class);

		updateTabDettaglio = UPDATE_TAB_DETTAGLIO_DEFAULT;
		documentCmp = new DetailDocumentRedComponent(utente);
		previewComponent = new DetailPreviewIntroComponent(utente);

		// Inizializzazione del tab storico potrebbero essere necessarie ulteriori
		// iniziializzazioni qualora la stessa istanza venga
		// Richiamata in più punti mi aspetto che in ogni punto questa classe venga
		// estesa
		if (hasTabStorico()) {
			storico = new DettaglioDocumentoStoricoComponent(utente, getClass().getSimpleName());
		}

	}

	/**
	 * Invocato dai fascicoli per impostare il master.
	 * 
	 * @param master
	 */
	public void setDetail(final MasterDocumentRedDTO master) {
		documentCmp.setMaster(master);
		setDetail(master.getDocumentTitle(), master.getWobNumber(), master.getClasseDocumentale());
	}

	/**
	 * Richiamato dalle lista o dalla ricerca quando si vuole richiamare FEPA.
	 * 
	 * @param documentTitle
	 * @param wobNumber
	 * @param documentClassName
	 * @param inDettaglioDocEnum Questo Enum serve a inizializzare il giusto FEPA
	 */
	public void setDetail(final String documentTitle, final String wobNumber, final String documentClassName) {
		setDetail(documentTitle, wobNumber, documentClassName, DettaglioDocumentoFileEnum.GENERICO);
	}

	/**
	 * Richiamato dal documento quando si vuole.
	 * 
	 * @param documentTitle
	 * @param wobNumber
	 * @param documentClassName
	 */
	public void setDetail(final String documentTitle, final String wobNumber, final String documentClassName, final DettaglioDocumentoFileEnum inDettaglioDocEnum) {
		this.visibleDetail = !org.apache.commons.lang3.StringUtils.isBlank(documentTitle);
		if (this.visibleDetail) {
			this.dettaglioDocEnum = inDettaglioDocEnum;
			final DetailDocumentRedDTO detail = new DetailDocumentRedDTO();
			detail.setDocumentTitle(documentTitle);
			detail.setWobNumberPrincipale(wobNumber);
			detail.setDocumentClass(documentClassName);
			documentCmp.setDetail(detail);
			introPreviewActive = true;
			storicoGiaCaricato = false;
			previewComponent.setDetailPreview(documentTitle);
			setActiveTab(TAB_PREVIEW);
			
			aSignEnabled = aSignSRV.isPresent(documentTitle);

		} else {
			unsetDetail();
		}
	}

	/**
	 * Carica il detail .
	 */
	public void setDetail() {
		LOGGER.info("####INFO#### Inizio setDetail");
		introPreviewActive = false;
		final DetailDocumentRedDTO detail = documentCmp.getDetail();

		LOGGER.info("####INFO#### Inizio setDetailAfterPreview");
		// Mi tocca richiamare il piu generico dei due
		setDetailAfterPreview(detail.getDocumentTitle(), detail.getWobNumberPrincipale(), detail.getDocumentClass(), dettaglioDocEnum);
		LOGGER.info("####INFO#### Fine setDetailAfterPreview");

		// Se non ti trovi all'interno di una coda calcolo le code in cui ti trovi
		if (sessionBean.isNotActivePageCoda()) {
			LOGGER.info("####INFO#### Inizio calcolaCoda");
			calculateQueues();
			LOGGER.info("####INFO#### Fine calcolaCoda");
		}
		LOGGER.info("####INFO#### Fine setDetail");
	}

	/**
	 * Imposta il dettaglio dopo la preview.
	 * @param documentTitle
	 * @param wobNumber
	 * @param documentClassName
	 */
	protected void setDetailAfterPreview(final String documentTitle, final String wobNumber, final String documentClassName) {
		setDetailAfterPreview(documentTitle, wobNumber, documentClassName, DettaglioDocumentoFileEnum.GENERICO);
	}

	/**
	 * Imposta il dettaglio dopo la preview.
	 * @param documentTitle
	 * @param wobNumber
	 * @param documentClassName
	 * @param dettaglioDocEnum
	 */
	protected void setDetailAfterPreview(final String documentTitle, final String wobNumber, final String documentClassName,
			final DettaglioDocumentoFileEnum dettaglioDocEnum) {
		
		LOGGER.info("Impostazione del dettaglio preview del documento di tipologia: " + dettaglioDocEnum);
		try {
			final SessionBean sessBean = FacesHelper.getManagedBean(Constants.ManagedBeanName.SESSION_BEAN);
			if (!sessBean.getPreferenze().isFullDetailEnable()) {
				final DetailDocumentRedDTO detail = new DetailDocumentRedDTO();
				detail.setDocumentTitle(documentTitle);
				LOGGER.info("####INFO#### Inizio not full detail");
				documentCmp.setDetail(detail);
				LOGGER.info("####INFO#### Fine not full detail");
			} else {
				LOGGER.info("####INFO#### Inizio full detail");
				if (sessBean.getActivePageInfo().equals(NavigationTokenEnum.RICERCA_DOCUMENTI)
						&& PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.RICERCA_RISERVATO)) {
					documentCmp.setDetail(documentoRedSRV.getDocumentDetail(documentTitle, utente, wobNumber, documentClassName, true));
				} else {
					documentCmp.setDetail(documentoRedSRV.getDocumentDetail(documentTitle, utente, wobNumber, documentClassName));
				}
				LOGGER.info("####INFO#### Fine full detail");
			}
			if (documentCmp.getDetail() == null) {
				LOGGER.error("Impossibile recuperare il documento con Document Title: " + documentTitle);
				// mostrare una popup di errore caricamento
				throw new RedException("Errore nella fase di caricamento del dettaglio.");
			}

			documentTitleSelected = documentCmp.getDetail().getDocumentTitle(); // di default imposto quello del documento principale
			mimeTypeSelected = documentCmp.getDetail().getMimeType();
			nomeFileSelected = documentCmp.getDetail().getNomeFile();

			LOGGER.info("####INFO#### Inizio getJavascriptStandalone");
			if (hasTabStorico()) {
				onChangeDetailStorico(documentCmp.getDetail());
				// Questo serve nel caso in cui apri la pagina e visualizzi immediatamente il
				// tab dello storico
				// Attualmente con l'intro preview lo storico non viene caricato immediatamente
				// con il detail e quindi non esiste nessun javascript
				FacesHelper.executeJS(storico.getJavascriptStandalone());
			}
			LOGGER.info("####INFO#### Fine getJavascriptStandalone");

			LOGGER.info("####INFO#### Inizio createMenuAllegatiModel");
			menuAllegatiDetailModel = createMenuAllegatiModel();
			LOGGER.info("####INFO#### Fine createMenuAllegatiModel");
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Gestisce l'evento di change del dettaglio storico.
	 * @param document
	 */
	protected void onChangeDetailStorico(final DetailDocumentRedDTO document) {
		storico.setDetail(document);
		storico.buildStoricoGui();
		storico.setValid(document.getDateCreated());
	}

	/****************************
	 * BOTTONI IN ALTO A DESTRA
	 ****************************/
	/**
	 * Esempio di command: "#{DettaglioDocumentoBean.changeSelectedDocumentTitle}".
	 * 
	 * @param command
	 * @return
	 */
	protected MenuModel createMenuAllegatiModel() {
		MenuModel output = new DefaultMenuModel();
		if (documentCmp.getDetail() != null) {
			output = createSubMenuAllegatiModel(documentCmp.getDetail(), this.menuAllegatiCommand);
		}

		return output;
	}

	/**
	 * Crea un sub menu allegati.
	 * @param detail
	 * @param command
	 * @return menu model creato
	 */
	protected MenuModel createSubMenuAllegatiModel(final DetailDocumentRedDTO detail, final String command) {
		final MenuModel output = new DefaultMenuModel();

//		DOCUMENTO PRINCIPALE start%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		final DefaultMenuItem principale = new DefaultMenuItem();
		principale.setIcon(Constants.Icon.PDF);
		principale.setValue(StringUtils.truncateWithPoints(detail.getNomeFile(), FILENAME_SIZE));
		principale.setTitle(detail.getNomeFile());
		principale.setAjax(true);
		principale.setParam(Constants.RequestParameter.DOCUMENT_TITLE, detail.getDocumentTitle());
		if (!StringUtils.isNullOrEmpty(detail.getMimeType())) {
			principale.setParam(Constants.RequestParameter.DOCUMENT_MIMETYPE_FILE_SELECTED, detail.getMimeType());
		}
		if (!StringUtils.isNullOrEmpty(detail.getNomeFile())) {
			principale.setParam(Constants.RequestParameter.DOCUMENT_NAME_FILE_SELECTED, detail.getNomeFile());
		}
		principale.setCommand(command);
		principale.setUpdate(updateTabDettaglio);
		principale.setProcess("@this");

		final DefaultSubMenu dsmPrincipale = new DefaultSubMenu("Documento Principale");
		dsmPrincipale.addElement(principale);
		output.addElement(dsmPrincipale);

//		DOCUMENTO PRINCIPALE end%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//		ALLEGATI START %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		List<AllegatoDTO> list = null;
		if (CollectionUtils.isNotEmpty(detail.getAllegati())) {
			list = detail.getAllegati().stream()
					.filter(allegatoDTO -> allegatoDTO.getFormatoSelected() == FormatoAllegatoEnum.ELETTRONICO
							|| allegatoDTO.getFormatoSelected() == FormatoAllegatoEnum.FIRMATO_DIGITALMENTE
							|| allegatoDTO.getFormatoSelected() == FormatoAllegatoEnum.FORMATO_ORIGINALE)
					.collect(Collectors.toList());
		}
		if (CollectionUtils.isNotEmpty(list)) {
			final DefaultSubMenu dsmDaFirmare = new DefaultSubMenu("Allegati da firmare");
			final DefaultSubMenu dsmDaNonFirmare = new DefaultSubMenu("Allegati da non firmare");
			for (final AllegatoDTO allegato : list) {
				final String documentTitle = allegato.getDocumentTitle();
				final String nomefile = allegato.getNomeFile();
				final String mimeType = allegato.getMimeType();

				final String allegatoDocumentTitle = DocumentTypeEnum.ALLEGATO.createId(documentTitle);
				final DefaultMenuItem itemAllegato = createMenuItem(allegatoDocumentTitle, nomefile, mimeType, command);

				if (allegato.getDaFirmare() != null && allegato.getDaFirmare() == 1) {
					dsmDaFirmare.addElement(itemAllegato);
				} else {
					dsmDaNonFirmare.addElement(itemAllegato);
				}
			}

			if (dsmDaFirmare.getElements() != null && !dsmDaFirmare.getElements().isEmpty()) {
				output.addElement(dsmDaFirmare);
			}
			if (dsmDaNonFirmare.getElements() != null && !dsmDaNonFirmare.getElements().isEmpty()) {
				output.addElement(dsmDaNonFirmare);
			}
		}
//		ALLEGATI END   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//		CONTRIBUTI START %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		if (detail.getContributi() != null && !detail.getContributi().isEmpty()) {
			final DefaultSubMenu dsmContributiInterni = new DefaultSubMenu("Contributi Interni");
			final DefaultSubMenu dsmContributiEsterni = new DefaultSubMenu("Contributi Esterni");

			for (final ContributoDTO contributo : detail.getContributi()) {
				String contributoDocumentTitle;

				// Contributo da contenuto FileNet
				if (!StringUtils.isNullOrEmpty(contributo.getDocumentTitle())) {
					contributoDocumentTitle = DocumentTypeEnum.CONTRIBUTO.createId(contributo.getDocumentTitle());

					addContributoToSubMenuAllegatiModel(dsmContributiInterni, dsmContributiEsterni, contributoDocumentTitle, contributo.getFilename(),
							contributo.getMimeType(), command, contributo.getFlagEsterno());
				}

				// Contributo da nota
				if (!StringUtils.isNullOrEmpty(contributo.getNota())) {
					DocumentTypeEnum contributoType = DocumentTypeEnum.CONTRIBUTO_INTERNO_OTF;
					if (Boolean.TRUE.equals(contributo.getFlagEsterno())) {
						contributoType = DocumentTypeEnum.CONTRIBUTO_ESTERNO_OTF;
					}
					contributoDocumentTitle = contributoType.createId(contributo.getIdContributo().toString());

					addContributoToSubMenuAllegatiModel(dsmContributiInterni, dsmContributiEsterni, contributoDocumentTitle,
							ContributoDTO.getNomeNotaPdf(contributo.getIdContributo()), MediaType.PDF.toString(), command, contributo.getFlagEsterno());
				}
			}

			if (dsmContributiInterni.getElements() != null && !dsmContributiInterni.getElements().isEmpty()) {
				output.addElement(dsmContributiInterni);
			}
			if (dsmContributiEsterni.getElements() != null && !dsmContributiEsterni.getElements().isEmpty()) {
				output.addElement(dsmContributiEsterni);
			}
		}
//		CONTRIBUTI END   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

		return output;
	}

	private void addContributoToSubMenuAllegatiModel(final DefaultSubMenu dsmContributiInterni, final DefaultSubMenu dsmContributiEsterni,
			final String contributoDocumentTitle, final String contributoFileName, final String contributoMimeType, final String command, final Boolean flagEsterno) {
		final DefaultMenuItem itemContributo = createMenuItem(contributoDocumentTitle, contributoFileName, contributoMimeType, command);

		if (Boolean.TRUE.equals(flagEsterno)) {
			dsmContributiEsterni.addElement(itemContributo);
		} else {
			dsmContributiInterni.addElement(itemContributo);
		}
	}

	private DefaultMenuItem createMenuItem(final String documentTitle, final String nomefile, final String mymeType, final String command) {
		final DefaultMenuItem menuItem = new DefaultMenuItem();
		menuItem.setValue(StringUtils.truncateWithPoints(nomefile, FILENAME_SIZE));
		menuItem.setParam(Constants.RequestParameter.DOCUMENT_TITLE, documentTitle);

		if (org.apache.commons.lang3.StringUtils.isNotBlank(mymeType)) {
			menuItem.setParam(Constants.RequestParameter.DOCUMENT_MIMETYPE_FILE_SELECTED, mymeType);
		}
		if (org.apache.commons.lang3.StringUtils.isNotBlank(nomefile)) {
			menuItem.setParam(Constants.RequestParameter.DOCUMENT_NAME_FILE_SELECTED, nomefile);
		}

		menuItem.setAjax(true);
		menuItem.setTitle(nomefile);
		if (MediaType.PDF.toString().equalsIgnoreCase(mymeType)) {
			menuItem.setCommand(command);
			menuItem.setUpdate(updateTabDettaglio);
			menuItem.setProcess("@this");
		} else {
			String js = null;
			try {
				js = jsMenuAllegatiNoPdf(documentTitle, nomefile);
				// Scrivo il return false perché in questo modo impedisco che parti l'ajax
				js += " ; return false;";
				menuItem.setOnclick(js);
			} catch (UnsupportedEncodingException e) {// Questo caso si verifica solamente se non sono riuscito a generare il download
				LOGGER.error("impossibile eseguire l'url Encoding per il documentTitle: " + documentTitle, e);
				menuItem.setDisabled(true);
				menuItem.setTitle("Impossibile scaricare il seguente documento");
			}
		}

		if (StringUtils.isNullOrEmpty(mymeType)) {
			menuItem.setIcon(Constants.Icon.EMPTY);
		} else if (MediaType.PDF.toString().equalsIgnoreCase(mymeType)) {
			menuItem.setIcon(Constants.Icon.PDF);
		} else {
			menuItem.setIcon(Constants.Icon.P7M);
		}
		return menuItem;
	}

	/**
	 * Costruisce una stringa che permette di richiamare la content servlet e
	 * scaricare il file.
	 * 
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	private String jsMenuAllegatiNoPdf(final String documentTitle, final String nome) throws UnsupportedEncodingException {
		try {
			// Il prefisso è calcolato nel documentTitleSelected all'atto di creazione Menu
			// Allegati
			String cryptoIdWithPrefix = DownloadContentServlet.getCryptoDocParamById(documentTitle, null, utente, nome);
			cryptoIdWithPrefix = cryptoIdWithPrefix.replace("\r", "");
			cryptoIdWithPrefix = cryptoIdWithPrefix.replace("\n", "");
			return "downloadFile('" + cryptoIdWithPrefix + "','" + nome + "')";
		} catch (final UnsupportedEncodingException e) {
			throw e;
		}
	}

	/**
	 * Metodo richiamato al click di una voce del menu allegati solo se questi sono
	 * pdf.
	 */
	public void changeSelectedDocumentTitle() {
		/**
		 * Queste variabili sono inserite come parametri della request all'atto del
		 * click di una voce del menu allegati L'inizializzazione di queste variabili
		 * permette di scaricare diverse entità nella downloadContent Servlet
		 */
		final String docTitleSelected = (String) FacesHelper.getParameterFromRequest(Constants.RequestParameter.DOCUMENT_TITLE);
		final String fileNameSelected = (String) FacesHelper.getParameterFromRequest(Constants.RequestParameter.DOCUMENT_NAME_FILE_SELECTED);
		final String mimeTypeFileSelected = (String) FacesHelper.getParameterFromRequest(Constants.RequestParameter.DOCUMENT_MIMETYPE_FILE_SELECTED);

		changePreviewContent(docTitleSelected, fileNameSelected, mimeTypeFileSelected);
	}

	/**
	 * Modifica il content della preview.
	 * @param inDocumentTitleSelected
	 * @param inNomeFileSelected
	 * @param inMimeTypeSelected
	 */
	protected void changePreviewContent(final String inDocumentTitleSelected, final String inNomeFileSelected, final String inMimeTypeSelected) {
		documentTitleSelected = inDocumentTitleSelected;
		nomeFileSelected = inNomeFileSelected;
		mimeTypeSelected = inMimeTypeSelected;

		previewComponent.setDetailPreview(documentTitleSelected);
		setActiveTab(TAB_PREVIEW);
		documentCmp.setDocumentTitleSelected(documentTitleSelected);
		documentCmp.setMimeTypeSelected(mimeTypeSelected);
		documentCmp.setNomeFileSelected(nomeFileSelected);
		FacesHelper.executeJS(storico.getJavascriptStandalone());
	}

	// MENU ALLEGATI END

	/**
	 * Restituisce lo StreamedContent per il download della versione del documento
	 * specificata dall'index.
	 * 
	 * @param detail
	 * @param index
	 * @return StreamedContent per download
	 */
	public StreamedContent downloadVersioneDocumentoDettaglio(final DetailDocumentRedDTO detail, final Object index) {
		StreamedContent file = null;
		try {
			final Integer i = (Integer) index;

			final VersionDTO v = detail.getVersioni().get(i);

			final InputStream stream = documentManagerSRV.getStreamDocumentoByGuid(v.getGuid(), utente);

			file = new DefaultStreamedContent(stream, v.getMimeType(), v.getFileName());

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}
		return file;

	}

	/**
	 * Restituisce lo StremedContent generato per effettuarne il download del report
	 * approvazioni.
	 * 
	 * @param detail
	 * @return StreamedContent per download
	 */
	protected StreamedContent getReportApprovazioniDetail(final DetailDocumentRedDTO detail) {
		StreamedContent output = null;
		try {
			final String ispettoratoPreponente = detail.getUfficioMittente().getDescrizione();
			final byte[] content = PdfHelper.createApprovazioniDocument(ispettoratoPreponente, detail.getNumeroDocumento(), detail.getApprovazioni());
			output = new DefaultStreamedContent(new ByteArrayInputStream(content), MediaType.PDF.toString(), "report_approvazioni_" + detail.getNumeroDocumento() + ".pdf");
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di generazione del report delle approvazioni.", e);
			showError(e);
		}

		return output;
	}

	/**
	 * Restituisce lo StreamedContent per il download della versione definita
	 * dall'index degli allegati.
	 * 
	 * @param detail
	 * @param guid
	 * @param index
	 * @return StreamedContent per download allegati
	 */
	public StreamedContent downloadVersioneDocumentiAllegati(final DetailDocumentRedDTO detail, final String guid, final Object index) {
		StreamedContent file = null;
		try {
			AllegatoDTO allegatoSelected = null;
			for (final AllegatoDTO a : detail.getAllegati()) {
				if (a.getGuid().equals(guid)) {
					allegatoSelected = a;
					break;
				}
			}

			if (allegatoSelected == null) {
				throw new RedException("recupero allegato");
			}

			final Integer i = (Integer) index;
			final VersionDTO v = allegatoSelected.getVersions().get(i);

			final InputStream stream = documentManagerSRV.getStreamDocumentoByGuid(v.getGuid(), utente);

			file = new DefaultStreamedContent(stream, v.getMimeType(), v.getFileName());

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}

		return file;
	}

	/**
	 * Azioni da eseguire dopo che la preview sia stata attivata.
	 */
	@Override
	public void loadAfterIntroPreview() {
		if (isIntroPreviewActive()) {
			setDetail();
		}
	}

	/**
	 * Metodo utilizzato per pulire il dettaglio e le informazioni in esso
	 * contenute. Utile se dopo una l'utilizzo di una response la coda non contenga
	 * più elementi.
	 * 
	 */
	public void unsetDetail() {
		documentCmp.unsetDetail();
		documentTitleSelected = null;
		menuAllegatiDetailModel = null;
		mimeTypeSelected = null;
		modificabile = false;
		nomeFileSelected = null;
		popupDocumentVisible = false;
		responseDetail = new ArrayList<>();
		updateEnabled = false;
		storico.unset();
		introPreviewActive = false;
		previewComponent.unsetDetail();
	}

	/**
	 * Imposta le response valide per lo specifico documento selezionato.
	 * 
	 * @param m
	 */
	public void setResponse(final MasterDocumentRedDTO m) {
		responseDetail = new ArrayList<>();

//		<-- Recupero response Filenet e Applicative -->
		final ResponsesDTO respDto = responseRedSRV.refineReponsesSingle(m, utente, Boolean.FALSE);
		// <-- Fine recupero -->

		// <-- Predispongo dati per view -->
		responseDetail = respDto.getResponsesEnum();
		orderResponse(responseDetail);
		// <-- Fine predisposizione -->
	}

	/**
	 * Questo metodo dovrebbe aprire il dettaglio esteso.
	 */
	@Override
	public abstract void openDocumentPopup();

	/**
	 * Esegue il calcolo delle code.
	 */
	protected void calculateQueues() {
		Integer idCategoriaDocumento = null;
		String documentTitle = null;
		String classeDocumentale = null;

		if (documentCmp.getMaster() != null) {
			idCategoriaDocumento = documentCmp.getMaster().getIdCategoriaDocumento();
			documentTitle = documentCmp.getMaster().getDocumentTitle();
			classeDocumentale = documentCmp.getMaster().getClasseDocumentale();
		} else if (documentCmp.getDetail() != null) {
			documentTitle = documentCmp.getDetail().getDocumentTitle();
			classeDocumentale = documentCmp.getDetail().getDocumentClass();
			idCategoriaDocumento = documentCmp.getDetail().getIdCategoriaDocumento();
		}

		final RecuperoCodeDTO dto = ricercaSRV.getQueueNameFromDocumentTitle(utente, documentTitle, classeDocumentale, idCategoriaDocumento, true);
		listaCodeNonCensite = StringUtils.fromStringListToString(dto.getCodeNonCensite());
		final Collection<DocumentQueueEnum> queue = dto.getCode();

		codeCount = new EnumMap<>(DocumentQueueEnum.class); // azzero il contenitore prima di ricalcolare le code per non creare
															// incongruenze nella view
		if (queue != null && !queue.isEmpty()) {

			for (final DocumentQueueEnum coda : queue) {
				// queue.key.name eq 'In_Nessuna_Coda_Servizio' or queue.key.name eq
				// 'Non_Censito_Servizio'
				if (sessionBean.haAccessoAllaFunzionalita(coda.getAccessFun()) && !coda.equals(DocumentQueueEnum.NESSUNA_CODA)
						&& !coda.equals(DocumentQueueEnum.NON_CENSITO)) {
					codeCount.put(coda, Collections.frequency(queue, coda));
				}
				
			}
		}
	}

	/**
	 * metodoche utilizza la coda recuperata sopra per la navigazione nella stessa.
	 */
	@Override
	public String goToCoda(final DocumentQueueEnum q) {
		MasterDocumentRedDTO master = documentCmp.getMaster();
		if (master == null && documentCmp.getDetail() != null) {
			final DetailDocumentRedDTO detail = documentCmp.getDetail();

			master = new MasterDocumentRedDTO();
			master.setDocumentTitle(detail.getDocumentTitle());
			master.setNumeroDocumento(detail.getNumeroDocumento());
			master.setIdCategoriaDocumento(detail.getIdCategoriaDocumento());
		}

		final GoToCodaDTO goToCodaInfo = new GoToCodaDTO(master);

		FacesHelper.putObjectInSession(ConstantsWeb.SessionObject.FILTER_MASTER_GO_TO_CODA, goToCodaInfo);
		return sessionBean.gotoPage(NavigationTokenEnum.get(q));
	}

	/**
	 * Metodo per il reset della view dettaglio sintetico.
	 */
	public void resetDetailView() {
		setActiveTab(TAB_PREVIEW);
		setIntroPreviewActive(true);
		storicoGiaCaricato = false;
	}

	/**
	 * GET & SET.
	 ***********************************************************************************************/

	public DetailDocumentRedComponent getDocumentCmp() {
		return documentCmp;
	}

	/**
	 * Restituisce true se il documento risulta modificabile, false altrimenti.
	 * 
	 * @return is modificabile
	 */
	public boolean isModificabile() {
		return modificabile;
	}

	/**
	 * Restituisce true se l'update è abilitato, false altrimenti.
	 * 
	 * @return true se update abilitato
	 */
	public Boolean getUpdateEnabled() {
		return updateEnabled;
	}

	/**
	 * Restituisce true se il popupDocument è visibile.
	 * 
	 * @return popup visibile
	 */
	public boolean isPopupDocumentVisible() {
		return popupDocumentVisible;
	}

	/**
	 * Imposta la visibilità del popupDocument.
	 * 
	 * @param popupDocumentVisible
	 */
	public void setPopupDocumentVisible(final boolean popupDocumentVisible) {
		this.popupDocumentVisible = popupDocumentVisible;
	}

	/**
	 * Restituisce il Model che definisce il menu degli allegati.
	 */
	@Override
	public MenuModel getMenuAllegatiDetailModel() {
		return menuAllegatiDetailModel;
	}

	/**
	 * Restituisce il mime type selezionato.
	 * 
	 * @return mime type
	 */
	public String getMimeTypeSelected() {
		return mimeTypeSelected;
	}

	/**
	 * Restituisce il nome del file selezionato.
	 * 
	 * @return nome file
	 */
	public String getNomeFileSelected() {
		return nomeFileSelected;
	}

	/**
	 * Restituisce le response associate al documento.
	 * 
	 * @return Collection di ResponsesRedEnum
	 */
	public Collection<ResponsesRedEnum> getResponseDetail() {
		return responseDetail;
	}

	/**
	 * Restituisce il component per la gestione dello storico.
	 * 
	 * @return DettaglioDocumentoStoricoComponent
	 */
	public DettaglioDocumentoStoricoComponent getStorico() {
		return storico;
	}

	/**
	 * Imposta il component dello storico.
	 * 
	 * @param inStorico
	 */
	protected void setStorico(final DettaglioDocumentoStoricoComponent inStorico) {
		this.storico = inStorico;
	}

	/**
	 * Decide se il tab storico è presente o meno
	 * 
	 * Estendi per modificare per default è impostato a true
	 * 
	 * Una selezione alternativa poteva essere che veniva creato alla creaizione del
	 * get ma non mi piaceva molto.
	 * 
	 * @return
	 */
	public boolean hasTabStorico() {
		return true;
	}

	/**
	 * @return the listaCodeNonCensite
	 */
	@Override
	public final String getListaCodeNonCensite() {
		return listaCodeNonCensite;
	}

	/**
	 * @param listaCodeNonCensite the listaCodeNonCensite to set
	 */
	public final void setListaCodeNonCensite(final String listaCodeNonCensite) {
		this.listaCodeNonCensite = listaCodeNonCensite;
	}

	/**
	 * @return the codeCount
	 */
	@Override
	public final EnumMap<DocumentQueueEnum, Integer> getCodeCount() {
		return codeCount;
	}

	protected final void setCodeCount(final EnumMap<DocumentQueueEnum, Integer> inCodeCount) {
		codeCount = inCodeCount;
	}

	/**
	 * @return updateTabDettaglio
	 */
	public String getUpdateTabDettaglio() {
		return updateTabDettaglio;
	}

	/**
	 * @param updateTabDettaglio
	 */
	public void setUpdateTabDettaglio(final String updateTabDettaglio) {
		this.updateTabDettaglio = updateTabDettaglio;
	}

	/**
	 * Imposta il component DetailDocumentRedComponent.
	 * 
	 * @param documentCmp
	 */
	protected void setDocumentCmp(final DetailDocumentRedComponent documentCmp) {
		this.documentCmp = documentCmp;
	}

	/**
	 * Restituisce il component per la gestione della preview.
	 * 
	 * @return previewComponent
	 */
	public DetailPreviewIntroComponent getPreviewComponent() {
		return previewComponent;
	}

	/**
	 * Restituisce true se la preview intro è attiva, false altrimenti.
	 * 
	 * @return introPreviewActive
	 */
	@Override
	public boolean isIntroPreviewActive() {
		return introPreviewActive;
	}

	/**
	 * Imposta il valore di attivazione della preview intro.
	 * 
	 * @param introPreviewActive
	 */
	public void setIntroPreviewActive(final boolean introPreviewActive) {
		this.introPreviewActive = introPreviewActive;
	}

	/**
	 * @param inMenuAllegatiCommand
	 */
	protected void setMenuAllegatiCommand(final String inMenuAllegatiCommand) {
		this.menuAllegatiCommand = inMenuAllegatiCommand;
	}

	/**
	 * Restituisce il tab attivo.
	 * 
	 * @return activeTab
	 */
	public int getActiveTab() {
		return activeTab;
	}

	/**
	 * Imposta il tab attivo.
	 * 
	 * @param inActiveTab
	 */
	public void setActiveTab(final int inActiveTab) {
		activeTab = inActiveTab;
	}

	/**
	 * Restituisce true se il detail è visibile, false altrimenti.
	 * 
	 * @return true se detail visibile
	 */
	public boolean isVisibleDetail() {
		return visibleDetail;
	}

	/**
	 * Imposta la visibilita del detail.
	 * 
	 * @param visibleDetail
	 */
	public void setVisibleDetail(final boolean visibleDetail) {
		this.visibleDetail = visibleDetail;
	}

	/**
	 * Restituisce il component per la gestione delle approvazioni.
	 * 
	 * @return approvazioniTab
	 */
	public DocumentManagerDataTabApprovazioniComponent getApprovazioniTab() {
		return approvazioniTab;
	}

	/**
	 * Imposta il component per la gestione delle approvazioni.
	 * 
	 * @param approvazioniTab
	 */
	public void setApprovazioniTab(final DocumentManagerDataTabApprovazioniComponent approvazioniTab) {
		this.approvazioniTab = approvazioniTab;
	}
	
	/**
	 * @return StreamedContent
	 */
	public StreamedContent getInviaDocPrincipaleEmail() {
		StreamedContent output = null;
		StringBuilder sb = null;
		try {
			final IDocumentoFacadeSRV docSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);
			FileDTO doc = null;
			String[] docTitleAllOContr = null;
			String documentTitleDaInviare = "";
			if (documentTitleSelected == null) {
				documentTitleDaInviare = this.documentCmp.getMaster().getDocumentTitle();
			} else {
				documentTitleDaInviare = documentTitleSelected;
				if (!NumberUtils.isParsable(documentTitleSelected)) {
					docTitleAllOContr = documentTitleSelected.split("_");
					if (docTitleAllOContr != null && docTitleAllOContr.length > 0) {
						documentTitleDaInviare = docTitleAllOContr[1];
					}
				}
			}

			if (!NumberUtils.isParsable(documentTitleSelected) && docTitleAllOContr != null && docTitleAllOContr.length > 0 && "a".equals(docTitleAllOContr[0])) {
				doc = docSRV.getContentAllegato(documentTitleDaInviare, utente.getFcDTO(), utente.getIdAoo());
			} else if (!NumberUtils.isParsable(documentTitleSelected) && docTitleAllOContr != null && docTitleAllOContr.length > 0 && "c".equals(docTitleAllOContr[0])) {
				doc = docSRV.getContributoContentPreview(utente.getFcDTO(), documentTitleDaInviare, utente.getIdAoo());
			} else {
				doc = docSRV.getDocumentoContentPreview(utente.getFcDTO(), documentTitleDaInviare, false, utente.getIdAoo());
			}

			if (doc != null) {
				final byte[] contentDoc = doc.getContent();
				final String mimeType = doc.getMimeType();
				final String contentString = Base64.getEncoder().encodeToString(contentDoc);

				sb = new StringBuilder();
				sb.append("data:message/rfc822 eml;charset=utf-8,\r\n");
				sb.append("To: \r\n");
				sb.append("Subject: \r\n");
				sb.append("X-Unsent: 1\r\n");
				sb.append("Content-Type: multipart/mixed; boundary=--boundary_text_string\r\n");
				sb.append("\r\n");
				sb.append("----boundary_text_string\r\n");
				sb.append("Content-Type: text/html; charset=UTF-8\r\n");
				sb.append("\r\n");
				sb.append("\r\n");
				sb.append("----boundary_text_string\r\n");
				sb.append("Content-Type: ").append(mimeType).append("; name=").append(doc.getFileName()).append("\r\n");
				sb.append("Content-Transfer-Encoding: base64\r\n");
				sb.append("Content-Disposition: attachment\r\n");
				sb.append("\r\n");
				sb.append(contentString).append("\r\n");
				sb.append("\r\n");

				final InputStream targetStream = new ByteArrayInputStream(sb.toString().getBytes());
				output = new DefaultStreamedContent(targetStream, MediaType.PDF.toString(), doc.getFileName() + ".eml");

			} else {
				showError("Non è possibile inviare il documento per email");
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'eml originale.", e);
		}

		return output;
	}

	/**
	 * Restituisce true se abilitata la firma asincrona.
	 * @return flag firma asincrona
	 */
	public Boolean getaSignEnabled() {
		return aSignEnabled;
	}

	/**
	 * Imposta il flag firma asincrona.
	 * @param aSignEnabled
	 */
	public void setaSignEnabled(Boolean aSignEnabled) {
		this.aSignEnabled = aSignEnabled;
	}
	

	public boolean isStoricoGiaCaricato() {
		return storicoGiaCaricato;
	}

	public void loadStorico() {
		setDetail();
		storicoGiaCaricato = true;
	}

}
