/**
 * 
 */
package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ColoreNotaEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IRiassegnazioneSRV;
import it.ibm.red.business.service.facade.INotaFacadeSRV;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.service.facade.IRiassegnazioneFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * @author APerquoti
 *
 */
@Named(ConstantsWeb.MBean.STORNA_UTENTE_BEAN)
@ViewScoped
public class StornaAUtenteBean extends AbstractBean implements InitHumanTaskInterface {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -8633159812421278604L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(StornaAUtenteBean.class.getName());
	
	/**
	 * Lista master.
	 */
	private Collection<MasterDocumentRedDTO> masters;
		
	/**
	 * Descrizione nuova assegnazione.
	 */
	private String descrizioneNewAssegnazione;
	
	/**
	 * Motivo nuova assegnazione.
	 */
	private String motivoAssegnazioneNew;
	
	/**
	 * Root.
	 */
	private transient TreeNode root;
	
	/**
	 * Nodo selezionato.
	 */
	private NodoOrganigrammaDTO selected;
	
	/**
	 * Informazioni utente.
	 */
	private UtenteDTO utente;
	
	/**
	 * Flag storico.
	 */
	private boolean alsoStorico;
	
	/**
	 * Service per salvataggio motivazione su Storico Note
	 */
	private INotaFacadeSRV notaSRV;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		final IOrganigrammaFacadeSRV orgSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
		notaSRV = ApplicationContextProvider.getApplicationContext().getBean(INotaFacadeSRV.class);
		utente = sb.getUtente();
		
		//creazione della struttura del tree
		root = new DefaultTreeNode("Root", null);
		root.setSelectable(true);
		
		final NodoOrganigrammaDTO padre = new NodoOrganigrammaDTO();
		padre.setDescrizioneNodo(utente.getNodoDesc());
		
		//creazione dell'unico nodo necessario (ufficio dell'utente in sessione)
		final TreeNode nodoPadre = new DefaultTreeNode(padre, root);
		nodoPadre.setExpanded(true);
		nodoPadre.setSelectable(false);
		nodoPadre.setType("UFF");
		
//		<-- Lista Utenti -->
		//recupero degli utenti appartenenti al suo stesso ufficio
		final NodoOrganigrammaDTO dto = new NodoOrganigrammaDTO();
		dto.setIdAOO(utente.getIdAoo());
		dto.setIdNodo(utente.getIdUfficio());
		dto.setUtentiVisible(true);
		
		final List<NodoOrganigrammaDTO> utentiList = orgSRV.getFigliAlberoAssegnatarioPerStornaAUtente(utente.getIdUfficio(), dto);
		DefaultTreeNode nodoToAdd = null;
		for (final NodoOrganigrammaDTO utenteNodo : utentiList) {
			nodoToAdd = new DefaultTreeNode(utenteNodo, nodoPadre);
			nodoToAdd.setSelectable(true);
			nodoToAdd.setType("USER");
			nodoPadre.getChildren().add(nodoToAdd);
		}
//		<-- Fine Lista Utenti -->
	}
	
	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		masters = inDocsSelezionati;		
	}
	
	/**
	 * Effettua una validazione sull'assegnazione, se valida esegue la response {@link #stornaUtenteResponse()}.
	 */
	public void checkEResponse() {
		if (descrizioneNewAssegnazione == null || descrizioneNewAssegnazione.isEmpty()) {
			showWarnMessage("Attenzione: il campo Nuova Assegnazione è obbligatorio");
		} else {
			stornaUtenteResponse();
		}
	}
	
	/**
	 * Esegue la logica che gestisce la response Storna a Utente. A valle delle operazioni effettua un aggiornamento
	 * degli esiti in modo da mostrarne correttamente il contenuto.
	 */
	private void stornaUtenteResponse() {
		Collection<EsitoOperazioneDTO> eOpe = new ArrayList<>();
		final Collection<String> wobNumbers = new ArrayList<>();
		try {
			final IRiassegnazioneFacadeSRV riassegnaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRiassegnazioneSRV.class);
			final ListaDocumentiBean ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
			
			//pulisco gli esiti
			ldb.cleanEsiti(); 
			
			//raccolgo i wobnumber necessari ad invocare il service 
			for (final MasterDocumentRedDTO m : masters) {
				if (!StringUtils.isNullOrEmpty(m.getWobNumber())) {
					wobNumbers.add(m.getWobNumber());
				}
			}
			
			//invoke service 
			eOpe = riassegnaSRV.riassegna(utente, wobNumbers, ResponsesRedEnum.STORNA_UTENTE, selected.getIdNodo(), selected.getIdUtente(), motivoAssegnazioneNew);
			
			//setto gli esiti con i nuovi e eseguo un refresh
			ldb.getEsitiOperazione().addAll(eOpe);
			
			if ((!ldb.getEsitiOperazione().isEmpty() && ldb.checkEsiti(false)) && isAlsoStorico()) {
				for (final MasterDocumentRedDTO m : masters) {
					notaSRV.registraNotaFromDocumento(utente, Integer.parseInt(m.getDocumentTitle()), ColoreNotaEnum.NERO, motivoAssegnazioneNew);
				}
			}
			
			FacesHelper.executeJS("PF('dlgGeneric').hide()");
			FacesHelper.executeJS("PF('dlgShowResult').show()");
			FacesHelper.update("centralSectionForm:idDlgShowResult");
			
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.STORNA_UTENTE_BEAN);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il tentato sollecito della response 'STORNA A UTENTE'.", e);
			showError("Si è verificato un errore durante l'esecuzione dell'operazione 'STORNA A UTENTE'");
		}
	}
	
	/**
	 * Gestisce la selezione dell'utenet identificato dall'evento di selezione del nodo.
	 * @param event
	 */
	public void onSelectedUser(final NodeSelectEvent event) {
		//recupero l'utente selezionato dall'evento e lo utilizzo poi per l'invocazione del service
		selected = (NodoOrganigrammaDTO) event.getTreeNode().getData();
		
		//dall'oggetto recuperato ottengo i dati da mostrare nella casella di testo
		descrizioneNewAssegnazione = selected.getDescrizioneNodo() + " - " +  selected.getCognomeUtente() + " " + selected.getNomeUtente();
	}
	
	/**
	 * Effettua un reset dell'assegnazione.
	 */
	public void clearAssegnazione() {
		descrizioneNewAssegnazione = "";
	}


	/**
	 * @return the descrizioneNewAssegnazione
	 */
	public String getDescrizioneNewAssegnazione() {
		return descrizioneNewAssegnazione;
	}


	/**
	 * @param descrizioneNewAssegnazione the descrizioneNewAssegnazione to set
	 */
	public void setDescrizioneNewAssegnazione(final String descrizioneNewAssegnazione) {
		this.descrizioneNewAssegnazione = descrizioneNewAssegnazione;
	}


	/**
	 * @return the motivoAssegnazioneNew
	 */
	public String getMotivoAssegnazioneNew() {
		return motivoAssegnazioneNew;
	}


	/**
	 * @param motivoAssegnazioneNew the motivoAssegnazioneNew to set
	 */
	public void setMotivoAssegnazioneNew(final String motivoAssegnazioneNew) {
		this.motivoAssegnazioneNew = motivoAssegnazioneNew;
	}

	/**
	 * @return the root
	 */
	public final TreeNode getRoot() {
		return root;
	}

	/**
	 * @return alsoStorico
	 */
	public boolean isAlsoStorico() {
		return alsoStorico;
	}

	/**
	 * @param alsoStorico
	 */
	public void setAlsoStorico(final boolean alsoStorico) {
		this.alsoStorico = alsoStorico;
	}
}