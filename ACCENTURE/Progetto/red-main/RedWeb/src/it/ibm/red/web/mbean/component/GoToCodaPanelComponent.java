package it.ibm.red.web.mbean.component;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.RecuperoCodeDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IRicercaFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.mbean.component.AbstractComponent;
import it.ibm.red.web.dto.GoToCodaDTO;
import it.ibm.red.web.enums.NavigationTokenEnum;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.IGoToCodaComponent;

/**
 * Serve a implementare il pannello che si apre dopo aver cliccato il bottone affianco a una riga di una tabella di ricerca.
 * Mostra una lista di code in cui l'utente può trovare il documento rappresentato da quella riga.
 */
public class GoToCodaPanelComponent extends AbstractComponent implements IGoToCodaComponent {

	private static final long serialVersionUID = -3514092447841888377L;


    /**
     * Servizio.
     */
	private final IRicercaFacadeSRV ricercaSRV;
	

    /**
     * Bean di sessione.
     */
	protected SessionBean sessionBean;

    /**
     * Informazioni utente.
     */
	protected UtenteDTO utente;
	

    /**
     * Lista code non censite.
     */
	protected String listaCodeNonCensite;
	

    /**
     * Contatori code.
     */
	protected EnumMap<DocumentQueueEnum, Integer> codeCount;

	/**
	 * Il bean ha questa informazine in quanto conosce il DataTableHelper che possiede l'info del bottone chiamato.
	 */
	private MasterDocumentRedDTO master;

	/**
	 * Costruttore vuoto di default.
	 */
	public GoToCodaPanelComponent() {
		ricercaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaFacadeSRV.class);
		sessionBean = (SessionBean) FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
	}

	/**
	 * Capita a volte che non si abbia a disposizione un masterDocumentRedDTO.
	 * In questo caso è possibile inizializzare l'operazione tramite questi parametri primitivi
	 * 
	 * @param idDocumentTitle
	 *  documentTitle del documento selezionato
	 * @param numeroDocumento
	 * 	numeroDocumento del documento selezionato viene utilizzato per ragioni di visualizzazione
	 * @param idCategoriaDocumento
	 * 	categoria del documento utilizzata per motivi di prestazioni questo parametro è opzionale
	 */
	public void initialize(final String idDocumentTitle, final Integer numeroDocumento, final Integer idCategoriaDocumento) {
		final MasterDocumentRedDTO masterDoc = new MasterDocumentRedDTO();
		masterDoc.setDocumentTitle(idDocumentTitle);
		masterDoc.setNumeroDocumento(numeroDocumento);
		masterDoc.setIdCategoriaDocumento(idCategoriaDocumento);
		initialize(masterDoc);
	}
	
	/**
	 * CApita a volte che non si abbia a disposizione un masterDocumentRedDTO.
	 * In questo caso è possibile inizializzare l'operazione tramite questi parametri primitivi
	 * 
	 * @param idDocumentTitle
	 *  documentTitle del documento selezionato
	 * @param numeroDocumento
	 * 	numeroDocumento del documento selezionato viene utilizzato per ragioni di visualizzazione
	 */
	public void initialize(final String idDocumentTitle, final Integer numeroDocumento) {
		initialize(idDocumentTitle, numeroDocumento, null);
	}
	
	/**
	 * Quando si desidera aprire il pannello occorre richiamare questo metodo
	 * @param master
	 * 	Il documento corrispondente alla riga selezionata
	 */
	public void initialize(final MasterDocumentRedDTO selectedMaster) {
		master = selectedMaster;
		
		Integer intIdCategoria = null;
		if (master.getIdCategoriaDocumento() != null) {
			intIdCategoria = master.getIdCategoriaDocumento();
		}
		final RecuperoCodeDTO dto = ricercaSRV.getQueueNameFromDocumentTitle(utente, String.valueOf(master.getDocumentTitle()), null, intIdCategoria, false);
		
		listaCodeNonCensite = StringUtils.fromStringListToString(dto.getCodeNonCensite());
		
		Integer count;
		codeCount = new EnumMap<>(DocumentQueueEnum.class); //azzero il contenitore prima di ricalcolare le code per non creare incongruenze nella view
		final Collection<DocumentQueueEnum> queue = dto.getCode();
		if (queue != null && !queue.isEmpty()) {
			for (final DocumentQueueEnum coda: queue) {
				if (sessionBean.haAccessoAllaFunzionalita(coda.getAccessFun())) {
					count = Collections.frequency(queue, coda);
					codeCount.put(coda, count);
				}
			}
		}
	}
	
	/**
	 * Redireziona l'utente sulla coda definita dal parametro q.
	 * @see it.ibm.red.web.mbean.interfaces.IGoToCodaComponent#goToCoda(it.ibm.red.business.enums.DocumentQueueEnum)
	 * @param q
	 * @return url coda
	 */
	@Override
	public String goToCoda(final DocumentQueueEnum q) {
		final GoToCodaDTO goToCodaInfo = new GoToCodaDTO(master);
		FacesHelper.putObjectInSession(ConstantsWeb.SessionObject.FILTER_MASTER_GO_TO_CODA, goToCodaInfo);
		return sessionBean.gotoPage(NavigationTokenEnum.get(q));
	}

	/**
	 * Restituisce la mappa che associa ad ogni coda il numero di documenti presenti.
	 * @return mappa che associa ad ogni coda il numero documenti che sono contenuti nella coda
	 */
	@Override
	public EnumMap<DocumentQueueEnum, Integer> getCodeCount() {
		return codeCount;
	}

	/**
	 * Restituisce le lista code non censite.
	 * @return lista code non censite
	 */
	@Override
	public String getListaCodeNonCensite() {
		return listaCodeNonCensite;
	}
}