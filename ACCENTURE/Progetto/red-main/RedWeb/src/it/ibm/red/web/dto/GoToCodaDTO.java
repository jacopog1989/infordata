package it.ibm.red.web.dto;

import java.util.ArrayList;
import java.util.List;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.web.enums.GoToCodaProvenienzaEnum;

/**
 * DTO utilizato per passare gli elementi nella navigazione verso le code da pagine esterne.
 *
 * @author m.crescentini
 */
public class GoToCodaDTO extends AbstractDTO {

	private static final long serialVersionUID = 7307843143213121886L;
	
	/**
     * Masters.
     */
	private final List<MasterDocumentRedDTO> masters;

	/**
     * Pagina provenieneza.
     */
	private GoToCodaProvenienzaEnum provenienza;

	/**
	 * @param masters
	 * @param provenienza
	 */
	public GoToCodaDTO(final List<MasterDocumentRedDTO> masters, final GoToCodaProvenienzaEnum provenienza) {
		super();
		this.masters = masters;
		this.provenienza = provenienza;
	}

	/**
	 * @param master
	 * @param provenienza
	 */
	public GoToCodaDTO(final MasterDocumentRedDTO master, final GoToCodaProvenienzaEnum provenienza) {
		super();
		this.masters = new ArrayList<>();
		this.masters.add(master);
		this.provenienza = provenienza;
	}

	/**
	 * @param master
	 */
	public GoToCodaDTO(final MasterDocumentRedDTO master) {
		super();
		this.masters = new ArrayList<>();
		this.masters.add(master);
	}



	/**
	 * @return the masters
	 */
	public List<MasterDocumentRedDTO> getMasters() {
		return masters;
	}


	/**
	 * @return the provenienza
	 */
	public GoToCodaProvenienzaEnum getProvenienza() {
		return provenienza;
	}
}