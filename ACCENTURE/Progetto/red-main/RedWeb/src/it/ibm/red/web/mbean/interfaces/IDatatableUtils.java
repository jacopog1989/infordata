package it.ibm.red.web.mbean.interfaces;

/**
 * Contiene alcune utility contenute nel'AbstractComponent e molto usate, nei datatable.
 * 
 */
public interface IDatatableUtils {

	/** 
	 * Abbrevia il label presente in una colonna di un datatable con i puntini.
	 * Inserire il label intero con i puntini utilizzando l'apposito metodo getColumnTooltip
	 * 
	 * Gestisce il caso label vuoto restituisce il classico  " - "
	 * 
 	 * @param columnLabel
	 * 	Stringa da visualizzare nella colonna
	 * @param size
	 * 	massimo numero di caratteri del label che si vogliono visualizzare
	 *  
	 * @return la stringa columnLabel se maggiore di size troncata di size e aggiungendo i punti '...' alla fine
	 * se columnLabel è null ritorna " - "
	 * 
	 */
	String getShortColumn(String columnLabel, Integer size);
	
	/**
	 * Quando una colonna necessita di essere abbreviata con i puntini
	 * Usare questo metodo nel xhtml nel tooltip della colonna per permettere di visualizzare il dato per esteso qualora questo sia sostituito dai puntini
	 * 
	 * @param columnLabel
	 * 	Stringa da visualizzare nella colonna
	 * @param size
	 * 	massimo numero di caratteri del label che si vogliono visualizzare
	 * @return
	 * Se il columnLabel non supera la size allora ritorna null (Non viene visualizzato il tooltip)
	 */
	String getColumnTooltip(String columnLabel, Integer size);
}
