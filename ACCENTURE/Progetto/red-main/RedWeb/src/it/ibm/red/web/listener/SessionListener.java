package it.ibm.red.web.listener;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.SessioneDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * Listener della sessione.
 */
public class SessionListener implements HttpSessionListener {

	/**
	 * @see javax.servlet.http.HttpSessionListener#sessionCreated(javax.servlet.http.HttpSessionEvent).
	 */
	@Override
	public void sessionCreated(final HttpSessionEvent hse) {
    	final String status = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.SESSION_LISTENER_STATUS);
		if (!StringUtils.isNullOrEmpty(status) && "on".equalsIgnoreCase(status)) {
			final REDLogger logger = REDLogger.getLogger(SessionListener.class.getName());
			try {
				final SessioneDTO sessionInfo = createSessionInfo(hse);
				final IUtenteFacadeSRV utenteSRV = ApplicationContextProvider.getApplicationContext().getBean(IUtenteFacadeSRV.class);
				utenteSRV.registerOpenSession(sessionInfo.getId(), sessionInfo.getIp(), sessionInfo.getUser());
				logger.info("Session " + sessionInfo.getId() + " created on " + sessionInfo.getIp());
			} catch (final Exception e) {
				logger.error("Errore in fase di registrazione della creazione della sessione.", e);
			}
		}
	}

	/**
	 * @see javax.servlet.http.HttpSessionListener#sessionDestroyed(javax.servlet.http.HttpSessionEvent).
	 */
	@Override
	public void sessionDestroyed(final HttpSessionEvent hse) {
    	final String status = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.SESSION_LISTENER_STATUS);
		if (!StringUtils.isNullOrEmpty(status) && "on".equalsIgnoreCase(status)) {
			final REDLogger logger = REDLogger.getLogger(SessionListener.class.getName());
			try {
				final SessioneDTO sessionInfo = createSessionInfo(hse);
				final IUtenteFacadeSRV utenteSRV = ApplicationContextProvider.getApplicationContext().getBean(IUtenteFacadeSRV.class);
				utenteSRV.registerCloseSession(sessionInfo.getId(), sessionInfo.getUser());
				logger.info("Session " + sessionInfo.getId() + " destroyed on " + sessionInfo.getIp() + " for " + sessionInfo.getUser());
			} catch (final UnknownHostException e) {
				logger.error("Errore in fase di registrazione della chiusura della sessione.", e);
			}
		}
	}	

	private SessioneDTO createSessionInfo(final HttpSessionEvent hse) throws UnknownHostException {
		final String ip = InetAddress.getLocalHost().toString();
		final String id = hse.getSession().getId();
		final String user = (String) hse.getSession().getAttribute(Constants.RequestParameter.USERNAME);
		return new SessioneDTO(ip, id, user);
	}
	
}