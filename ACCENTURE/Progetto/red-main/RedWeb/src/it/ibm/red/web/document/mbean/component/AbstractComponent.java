package it.ibm.red.web.document.mbean.component;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.event.FacesEvent;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.treetable.TreeTable;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.ITipoFileSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.helper.faces.FileUploadEventCopy;
import it.ibm.red.web.helper.faces.FileUploadEventHelper;
import it.ibm.red.web.helper.faces.MessageSeverityEnum;
import it.ibm.red.web.mbean.interfaces.IDatatableUtils;

/**
 * @author SLac
 *
 */
public class AbstractComponent implements Serializable, IDatatableUtils {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 611500750999051285L;
	
	/**
	 * LOGGER per la gestione dei messaggi in console.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AbstractComponent.class);
	
	/**
	 * Metodo utilizzato dai bean per visualizzare un messaggio d'errore.
	 *
	 * @param clientId	componente a cui fa riferimento il messaggio
	 * @param errorMsg	il messaggio da visualizzare
	 */
	protected final void showErrorMessage(final String clientId, final String errorMsg) {
		FacesHelper.showMessage(clientId, null, errorMsg, MessageSeverityEnum.ERROR);
	}
	
	/**
	 * Metodo utilizzato dai bean per visualizzare un messaggio di avviso.
	 * 
	 * @param clientId	componente a cui fa riferimento il messaggio
	 * @param msg	messaggio
	 */
	protected final void showWarnMessage(final String clientId, final String msg) {
		FacesHelper.showMessage(clientId, null, msg, MessageSeverityEnum.WARN);
	}

	/**
	 * Metodo per visualizzare un messaggio informativo (mostrato ogni qual volta un metodo viene invocato con successo).
	 * 
	 * @param clientId	componente a cui fa riferimento il messaggio
	 * @param msg		il messaggio da visualizzare
	 */
	protected final void showInfoMessage(final String clientId, final String msg) {
		FacesHelper.showMessage(clientId, null, msg, MessageSeverityEnum.INFO);
	}

	/**
	 * Metodo utilizzato dai bean per visualizzare un messaggio d'errore.
	 * 
	 * @param errorMsg	il messaggio da visualizzare
	 */
	protected final void showErrorMessage(final String errorMsg) {
		FacesHelper.showMessage(null, errorMsg, MessageSeverityEnum.ERROR);
	}
	
	/**
	 * Metodo utilizzato dai bean per visualizzare un messaggio d'errore.
	 * 
	 * @param ex	l'eccezione da processare. Se l'eccezione è generica verrà mostrato un messaggio generico.
	 * se l'eccezione è una RedException verrà mostrato il messaggio di errore relativo.
	 */
	protected final void showErrorMessage(final Exception inEx) {
		Exception ex = inEx;
		String redErrorMsg = "Errore durante l'esecuzione dell'operazione";
		Exception redException = null;
		
		if (ex != null) {
			if (ex instanceof RedException) {
				redException = ex;
			}
			
			while (ex.getCause() != null) {
				ex = (Exception) ex.getCause();
				if (ex instanceof RedException) {
					redException = ex;
				}
			}	
			
			if (redException != null) {
				redErrorMsg = redException.getMessage();
			}
		}
		
		FacesHelper.showMessage("", redErrorMsg, MessageSeverityEnum.ERROR);
	}
	
	/**
	 * Metodo utilizzato dai bean per visualizzare un messaggio di avviso.
	 * 
	 * @param msg	messaggio
	 */
	protected final void showWarnMessage(final String msg) {
		FacesHelper.showMessage(null, msg, MessageSeverityEnum.WARN);
	}

	/**
	 * Metodo per visualizzare un messaggio informativo (mostrato ogni qual volta un
	 * metodo viene invocato con successo).
	 * 
	 * @param msg
	 *            il messaggio da visualizzare
	 */
	protected final void showInfoMessage(final String msg) {
		FacesHelper.showMessage(null, msg, MessageSeverityEnum.INFO);
	}
	
	/**
	 * Recupera i dati associati alla riga cliccata di un datatable.
	 *
	 * @param <T>
	 *            tipo dei dati associati
	 * @param event
	 *            evento della gui
	 * @return dati associati
	 */
	@SuppressWarnings("unchecked")
	protected final <T> T getDataTableClickedRow(final FacesEvent event) {
		final UIComponent component = getClickedComponent(event);
		final DataTable datatable = (DataTable) component;
		return (T) datatable.getRowData();
	}

	/**
	 * Restituisce lo <em> UIComponent </em> cliccato.
	 * 
	 * @param event
	 *            evento di click
	 * @return component cliccato
	 */
	protected UIComponent getClickedComponent(final FacesEvent event) {
		UIComponent component = event.getComponent();
		while (!(component instanceof DataTable)) {
			component = component.getParent();
		}
		return component;
	}
	
	/**
	 * Abbrevia il label presente in una colonna di un datatable con i puntini.
	 * Inserire il label intero con i puntini utilizzanfo l'apposito metodo
	 * getColumnTooltip
	 * 
	 * Gestisce il caso label vuoto restituisce il classico " - "
	 * 
	 * @param columnLabel
	 *            Stringa da visualizzare nella colonna
	 * @param size
	 *            massimo numero di caratteri del label che si vogliono visualizzare
	 * 
	 * @return la stringa columnLabel se maggiore di size troncata di size e
	 *         aggiungendo i punti '...' alla fine se columnLabel è null ritorna " -
	 *         "
	 * 
	 */
	@Override
	public String getShortColumn(final String columnLabel, final Integer size) {
		return StringUtils.truncateColumnTable(columnLabel, size);
	}
	
	/**
	 * Quando una colonna necessita di essere abbreviata con i puntini.
	 * Usare questo metodo nel xhtml nel tooltip della colonna per permettere di visualizzare il dato per esteso qualora questo sia sostituito dai puntini
	 * 
	 * @param columnLabel
	 * 	Stringa da visualizzare nella colonna
	 * @param size
	 * 	massimo numero di caratteri del label che si vogliono visualizzare
	 * @return
	 * Se il columnLabel non supera la size allora ritorna null (Non viene visualizzato il tooltip)
	 */
	@Override
	public String getColumnTooltip(final String columnLabel, final Integer size) {
		return StringUtils.calculateColumnTooltip(columnLabel, size);
	}
	
	/**
	 * Recupera i dati associati alla riga cliccata di un tree.
	 *
	 * @param <T>
	 *            tipo dei dati associati
	 * @param event
	 *            evento della gui
	 * @return dati associati
	 */
	@SuppressWarnings("unchecked")
	protected final <T> T getTreeTableClickedRow(final FacesEvent event) {
		UIComponent component = event.getComponent();
		while (!(component instanceof TreeTable)) {
			component = component.getParent();
		}
		final TreeTable tn = (TreeTable) component;
		return (T) tn.getRowNode().getData();
	}

	/**
	 * Effettua una verifica sul nome del file in fase di upload che viene
	 * identificato dall' {@code event}.
	 * 
	 * @param event
	 *            evento di upload del file
	 * @return helper dell'upload del file
	 * @throws IOException
	 */
	protected FileUploadEvent checkFileName(final FileUploadEvent event) throws IOException {
		final FileUploadEventHelper helper = new FileUploadEventHelper(event);
		final UploadedFile file = helper.getFile();
		final ITipoFileSRV tipoFileSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipoFileSRV.class);
		final String nomeFileNormalizzato = tipoFileSRV.normalizzaNomeFile(event.getFile().getFileName());
		((FileUploadEventCopy) file).setFileName(nomeFileNormalizzato);
		try {
			tipoFileSRV.analizzaFile(event.getFile().getContents(), event.getFile().getContentType(), event.getFile().getFileName());
		} catch (final Exception ex) {
			LOGGER.error("Il tipo di file caricato non risulta conforme", ex);
			showErrorMessage("Il tipo di file caricato non risulta conforme");
			return null;
		}
		return helper;
	}
}
