package it.ibm.red.web.ricerca.mbean;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.event.data.PageEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.Visibility;

import it.ibm.red.business.dto.AllegatoNsdDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DetailProtocolloPregressoDTO;
import it.ibm.red.business.dto.ParamsRicercaProtocolliPregressiDfDTO;
import it.ibm.red.business.dto.ProtocolliPregressiDTO;
import it.ibm.red.business.dto.RiferimentoStoricoNsdDTO;
import it.ibm.red.business.dto.StatoRicercaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDfFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.mbean.DocumentManagerBean;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.DettaglioProtocolloRicercaNsdBean;
import it.ibm.red.web.mbean.interfaces.IUpdatableDetailCaller;

/**
 * Bean che gestisce la ricerca dei protocolli pregressi NSD.
 */
@Named(ConstantsWeb.MBean.RICERCA_PROTOCOLLI_PREGRESSI_NSD_BEAN)
@ViewScoped
public class RicercaProtocolliPregressiNsdBean extends RicercaAbstractBean<ProtocolliPregressiDTO> implements IUpdatableDetailCaller<DetailProtocolloPregressoDTO> {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = -2447557380937197231L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaProtocolliPregressiNsdBean.class.getName());
  
	/**
	 * Parametri ricerca.
	 */
	private ParamsRicercaProtocolliPregressiDfDTO formRicerca;

	/**
	 * Lista protocolli.
	 */
	protected SimpleDetailDataTableHelper<ProtocolliPregressiDTO> protocolliNsdDTH;

	/**
	 * Colonne.
	 */
	private List<Boolean> protColumnsNsd;

	/**
	 * Servizio.
	 */
	private IDfFacadeSRV dfSRV;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() { 
		super.postConstruct();

		protocolliNsdDTH = new SimpleDetailDataTableHelper<>();
		protocolliNsdDTH.setMasters(new ArrayList<>());
		protColumnsNsd = Arrays.asList(true, true, true, true);

		dfSRV = ApplicationContextProvider.getApplicationContext().getBean(IDfFacadeSRV.class);

		init();
	}

	/**
	 * Inizializza la pagina della ricerca.
	 */
	@SuppressWarnings("unchecked")
	public void init() {
		try {
			final StatoRicercaDTO stDto = (StatoRicercaDTO) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.STATO_RICERCA, true);
			if (stDto != null) {
				final Collection<ProtocolliPregressiDTO> searchResult = (Collection<ProtocolliPregressiDTO>) stDto.getRisultatiRicerca();
				formRicerca = (ParamsRicercaProtocolliPregressiDfDTO) stDto.getFormRicerca();

				protocolliNsdDTH.setMasters(searchResult);
				protocolliNsdDTH.selectFirst(true);

				selectDetail(protocolliNsdDTH.getCurrentMaster());
			} else {
				formRicerca = new ParamsRicercaProtocolliPregressiDfDTO();
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Non è stato possibile recuperare i risultati di ricerca per i protocolli nsd");
		}
	}

	/**
	 * Gestisce la selezione della riga del datatable associato ai protocolli.
	 * @param se
	 */
	public final void rowSelectorProtocolli(final SelectEvent se) {
		protocolliNsdDTH.rowSelector(se);
		selectDetail(protocolliNsdDTH.getCurrentMaster());
	}

	/**
	 * Gestisce la selezione del dettaglio.
	 * @param protocolliDTO
	 */
	@Override
	protected void selectDetail(final ProtocolliPregressiDTO protocolliDTO) {
		final DettaglioProtocolloRicercaNsdBean bean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_PROTOCOLLO_RICERCA_NSD_BEAN);
		if (protocolliDTO == null) {
			bean.unsetDetail();
			return;
		}
		bean.setDetail(protocolliDTO);
		bean.setCaller(this);
	}
	
	/**
	 * Restituisce il master del protocollo pregresso selezionato.
	 * @return ProtocolliPregressiDTO
	 */
	@Override
	public ProtocolliPregressiDTO getSelectedDocument() {
		return protocolliNsdDTH.getCurrentMaster();
	}

	/**
	 * Permette una ripetizione della'ultima ricerca effettuata andando a popolare il form con i parametri
	 * usati per l'ultima ricerca.
	 */
	public void ripetiRicercaProtocolliPregressiNsd() {
		try { 
			sessionBean.getRicercaFormContainer().setProtocolliDF(formRicerca);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Gestisce la selezione e la deselezione della colonna dei protocolli Nsd.
	 * @param event
	 */
	public final void onColumnToggleProtocolliNsd(final ToggleEvent event) {
		protColumnsNsd.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
	}

	/**
	 * Restituisce il datatable helper per la gestione del datatable dei protocolli Nsd.
	 * @return helper datatable protocolli Nsd
	 */
	public final SimpleDetailDataTableHelper<ProtocolliPregressiDTO> getProtocolliNsdDTH() {
		return protocolliNsdDTH;
	}

	/**
	 * Imposta il datatable helper per la gestione del datatable dei protocolli Nsd.
	 * @param protocolliNsdDTH
	 */
	public final void setProtocolliNsdDTH(final SimpleDetailDataTableHelper<ProtocolliPregressiDTO> protocolliNsdDTH) {
		this.protocolliNsdDTH = protocolliNsdDTH;
	}

	@Override
	public void updateDetail(final DetailProtocolloPregressoDTO detail) {
		// Metodo intenzionalmente vuoto.
	}

	/**
	 * Restituisce il valore delle colonne dei protocolli Nsd.
	 * @return valori colonne protocolli Nsd
	 */
	public List<Boolean> getProtColumnsNsd() {
		return protColumnsNsd;
	}

	/**
	 * Imposta il valore delle colonne dei protocolli Nsd.
	 * @param protColumnsNsd
	 */
	public void setProtColumnsNsd(final List<Boolean> protColumnsNsd) {
		this.protColumnsNsd = protColumnsNsd;
	}

	/**
	 * Restituisce il prefisso file excel.
	 * @return prefisso file estrazione
	 */
	@Override
	protected String getNomeRicerca4Excel() {
		return "ricercaprotocolliNsd_";
	} 

	/**
	 * Restituisce lo StreamedContent per il download del file principale allegato.
	 * @param idDocumento
	 * @return StreamedContent per download
	 */
	public StreamedContent downloadFilePrincipaleAllegato(final String idDocumento) {
		StreamedContent file = null; 
		try {  
			final UtenteDTO utente = sessionBean.getUtente();

			final AllegatoNsdDTO documento = dfSRV.getDocumentoConContent(idDocumento, utente);

			InputStream stream = null;
			if (documento.getContent() != null && documento.getContent().length > 0) {
				stream = new ByteArrayInputStream(documento.getContent());
			} else {
				throw new RedException("Nessun content present");
			}

			file = new DefaultStreamedContent(stream, documento.getContentType(), documento.getNomeFile());
		} catch (final Exception e) {
			if ("Errore nell'accesso al Documentale".equalsIgnoreCase(e.getMessage())) {
				LOGGER.error("Errore nell'accesso al Documentale: ", e);
				showError("Errore nell'accesso al Documentale");
			} else {
				LOGGER.error("Errore durante il download del file: ", e);
				showError("Errore durante il download del file");
			}
		}
		return file;
	}

	//PAGINAZIONE START
	/**
	 * Gestisce la paginazione.
	 * @param event
	 */
	public final void pageListener(final PageEvent event) {
		int pageIndex = event.getPage(); 
		final DataTable datatable = (DataTable) getClickedComponent(event); 
		if (pageIndex + 1 >=  datatable.getPageCount() && (sessionBean.getNumPaginaAttualeProtocollo() + 1) <= Integer.parseInt(sessionBean.getNumPagineTotaliDaRicercaProt())) {
			getNewPage();
		}

	}

	private Boolean getNewPage() {
		Boolean output = false; 
		// La pagina richiesta potrebbe richiedere il recupero di nuovi dati
		Collection<ProtocolliPregressiDTO> tmp = null; 
		sessionBean.setNumPaginaAttualeProtocollo(sessionBean.getNumPaginaAttualeProtocollo() + 1);
		sessionBean.setLastRowNumProtocollo(String.valueOf(sessionBean.getNumPaginaAttualeProtocollo()));

		final int maxNumPiuUno = sessionBean.getMaxRowPageProtocolliNsd() + 1;

		final String maxnumRicerca = String.valueOf(maxNumPiuUno);
		tmp = dfSRV.ricercaProtocolliPregressi(sessionBean.getProtocolliDF(), sessionBean.getUtente(), sessionBean.getLastRowNumProtocollo(), maxnumRicerca);
		if (tmp != null && !tmp.isEmpty()) { 
			protocolliNsdDTH.getMasters().addAll(tmp); 
			output = true;
		}
		return output;
	} 

	/**
	 * Effettua la predisposizione dei documenti selezionati da {@link #protocolliNsdDTH}.
	 */
	public void predisponiDocumenti() {
		final List<ProtocolliPregressiDTO> protocolliSelezionati = protocolliNsdDTH.getSelectedMasters();

		if (protocolliSelezionati == null || protocolliSelezionati.isEmpty()) {
			showWarnMessage("Selezionare almeno un documento");
			return;
		}

		try { 
			final DetailDocumentRedDTO docRibaltato = new DetailDocumentRedDTO();
			//in questo modo gli dico che è un documento in uscita
			docRibaltato.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_USCITA.getIds()[0]);
			final List<RiferimentoStoricoNsdDTO> allaccioRifStoricoListDTO = new ArrayList<>();
			for (final ProtocolliPregressiDTO  protocolloSelezionato : protocolliSelezionati) {
				final RiferimentoStoricoNsdDTO allaccioRifStoricoDTO = new RiferimentoStoricoNsdDTO();
				allaccioRifStoricoDTO.setAnnoProtocolloNsd(Integer.parseInt(protocolloSelezionato.getAnnoProtocollo()));
				allaccioRifStoricoDTO.setNumeroProtocolloNsd(protocolloSelezionato.getIdProtocollo());
				allaccioRifStoricoDTO.setOggettoProtocolloNsd(protocolloSelezionato.getDescOggetto());
				allaccioRifStoricoDTO.setIdDocumentoPrincipale(protocolloSelezionato.getOriginalDocId());
				allaccioRifStoricoDTO.setVerificato(true);
				allaccioRifStoricoListDTO.add(allaccioRifStoricoDTO);
				final DetailDocumentRedDTO docContenuto = new DetailDocumentRedDTO();
				docContenuto.setProtocolloVerificatoNsd(protocolloSelezionato);
				allaccioRifStoricoDTO.setDocument(docContenuto);
			}
			docRibaltato.setAllaccioRifStorico(allaccioRifStoricoListDTO);


			final DocumentManagerBean bean = FacesHelper.getManagedBean(MBean.DOCUMENT_MANAGER_BEAN);
			//Pulisco nel caso sia già stato aperto 
			bean.unsetDetail();
			bean.setChiamante(MBean.RICERCA_PROTOCOLLI_PREGRESSI_NSD_BEAN);
			bean.setDetail(docRibaltato); 

			FacesHelper.executeJS("PF('dlgManageDocument_RM').show()");
			FacesHelper.executeJS("PF('dlgManageDocument_RM').toggleMaximize()");
			FacesHelper.update("idDettagliEstesiForm:idPanelDocumentManager");
			FacesHelper.update("idDettagliEstesiForm:idPanelContainerDocumentManager_T");

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Effettua un aggiornamento dei check associati ai singoli master.
	 * @param event
	 */
	public final void updateCheck(final AjaxBehaviorEvent event) {
		final ProtocolliPregressiDTO masterChecked = getDataTableClickedRow(event);
		final Boolean newValue = ((SelectBooleanCheckbox) event.getSource()).isSelected();
		if (newValue != null && newValue) {
			protocolliNsdDTH.getSelectedMasters().add(masterChecked);
		} else {
			protocolliNsdDTH.getSelectedMasters().remove(masterChecked);
		}
	}

}
