package it.ibm.red.web.mbean;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.constants.Constants;

/**
 * Bean fascicolo manager.
 */
@Named(Constants.ManagedBeanName.FASCICOLO_FATTURA_MANAGER_BEAN)
@ViewScoped
public class FascicoloFatturaManagerBean extends FascicoloManagerBean {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 5523292132253713522L;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		super.postConstruct();
	}
}
