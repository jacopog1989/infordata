package it.ibm.red.web.mbean;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.poi.ss.formula.eval.NotImplementedException;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MasterFascicoloDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataFascDTO;
import it.ibm.red.business.enums.EsitoFaldoneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IDetailFascicoloSRV;
import it.ibm.red.business.service.facade.IFaldoneFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaAvanzataFascFacadeSRV;
import it.ibm.red.business.utils.CollectionUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.mbean.component.DialogFaldonaComponent;
import it.ibm.red.web.dto.TreeNodeFaldoneDTO;
import it.ibm.red.web.enums.ModeFaldonaturaEnum;
import it.ibm.red.web.enums.TreeNodeTypeEnum;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.master.IFaldoneHandler;

/**
 * Bean faldoni.
 */
@Named(ConstantsWeb.MBean.FALDONI_BEAN)
@ViewScoped
public class FaldoniBean extends AbstractTreeBean implements IFaldoneHandler {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -3236603751880397376L;

	/**
	 * Path per il recupero del container della label faldoni.
	 */
	private static final String IDLABELFALDONIFALD_RESOURCE_NAME = "centralSectionForm:idLabelFaldoni_Fald";

	/**
	 * Path per il recupero del container dei faldoni e ricerca fascicoli.
	 */
	private static final String FALDONIERICERCAFASCICOLI_CONTAINER_NAME = "centralSectionForm:idContainerFaldoniEricercaFascicoli";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FaldoniBean.class);

	/**
	 * Datatable fascicoli.
	 */
	private SimpleDetailDataTableHelper<MasterFascicoloDTO> fascicoliDTH;

	/**
	 * Servizio.
	 */
	private IFaldoneFacadeSRV faldoneSRV;

	/**
	 * Faldone selezionato.
	 */
	private String nomeFaldoneSelected;

	/** RICERCA FASCICOLI START. ***********************************************/
	private IRicercaAvanzataFascFacadeSRV fascicoloSRV;

	/**
	 * Flag modalità ricerca fascicoli.
	 */
	private boolean modalitaRicercaFascicoli;

	/**
	 * Parametri ricerca fascicoli.
	 */
	private ParamsRicercaAvanzataFascDTO parametri;

	/**
	 * Risultati ricerca fascicoli.
	 */
	private SimpleDetailDataTableHelper<FascicoloDTO> fascicoliRicerca;
	/** RICERCA FASCICOLI END. ***********************************************/

	/**
	 * Component dialog faldonatura.
	 */
	private DialogFaldonaComponent faldonaCmp;

	/**
	 * Flag faldonatura renderizzata.
	 */
	private boolean dlgFaldonaturaRendered;

	/**
	 * Flag dettaglio documento attivo.
	 */
	private Boolean dettaglioDocumentoActive;

	/**
	 * Tree faldoni.
	 */
	private transient TreeNode objFaldo;
	
	/**
	 * Servizio.
	 */
	private IDetailFascicoloSRV detailFascicoloSRV;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		faldoneSRV = ApplicationContextProvider.getApplicationContext().getBean(IFaldoneFacadeSRV.class);
		fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaAvanzataFascFacadeSRV.class);
		detailFascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IDetailFascicoloSRV.class);

		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();

		initCentralSection();
	}
	
	/**
	 * Restituisce il faldone.
	 * 
	 * @return obj faldone
	 */
	public TreeNode getObjFaldo() {
		return objFaldo;
	}

	/**
	 * Imposta il faldone.
	 * 
	 * @param objFaldo
	 *            faldone da impostare
	 */
	public void setObjFaldo(final TreeNode objFaldo) {
		this.objFaldo = objFaldo;
	}

	/**
	 * Inizializza la sezione centrale della pagina.
	 */
	public void initCentralSection() {
		try {
			objFaldo = (TreeNode) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.FALDO_OBJ, true);

			if (objFaldo != null) {
				// E' stato selezionato un nodo faldone
				onNodeSelect(objFaldo);
			} else {
				// E' stato selezionato il button della ricerca fascicoli
				showRicerca();
			}

		} catch (final Exception e) {
			LOGGER.error("Errore nel caricamento dei fascicoli.", e);
			showError("Errore nel caricamento dei fascicoli.");
		}

	}

	private DefaultTreeNode addNodoFaldone(final TreeNode parent, final Integer idUfficio, final FaldoneDTO faldone) {
		final TreeNodeFaldoneDTO dataParent = (TreeNodeFaldoneDTO) parent.getData();
		final TreeNodeFaldoneDTO data = new TreeNodeFaldoneDTO(dataParent.getDocumentTitle(), faldone.getOggetto(), faldone.getDescrizioneFaldone(), idUfficio,
				faldone.getAbsolutePath(), faldone.getDocumentTitle());
		return createNode(parent, TreeNodeTypeEnum.FALDONE, data, false);
	}

	/**
	 * Carica tutti i faldoni di un nodo (può essere un ufficio, e quindi stiamo
	 * richiedendo tutti i suoi faldoni, oppure può essere un faldone e quindi
	 * stiamo richiedendo tutti i suoi sotto faldoni) aggiornandolo.
	 * 
	 * @param node nodo selezionato
	 */
	private void addFaldoni(final TreeNode node) {
		if (TreeNodeTypeEnum.FALDONE.getType().equals(node.getType()) || TreeNodeTypeEnum.UFFICIO.getType().equals(node.getType())) {
			final TreeNodeFaldoneDTO data = (TreeNodeFaldoneDTO) node.getData();
			String nameFaldoneParent = null;
			if (TreeNodeTypeEnum.FALDONE.getType().equals(node.getType())) {
				// Se è stato selezionato un faldone devono essere recuperati i sottofaldoni
				nameFaldoneParent = data.getDocumentTitle();
			}
			final Collection<FaldoneDTO> faldoni = faldoneSRV.getFaldoni(utente, data.getIdUfficio(), nameFaldoneParent);
			for (final FaldoneDTO faldone : faldoni) {
				addNodoFaldone(node, data.getIdUfficio(), faldone);
			}
		}
	}

	/**
	 * Gestisce la selezione di un nodo dell'albero. Dal vertical menu
	 * 
	 * @param event evento gui
	 */
	public void onNodeSelect(final NodeSelectEvent event) {
		hideRicerca();
		unsetAllDetail();
		setSelectedNode(event.getTreeNode());
		final TreeNodeFaldoneDTO data = (TreeNodeFaldoneDTO) getSelectedNode().getData();
		fascicoliDTH = new SimpleDetailDataTableHelper<>();
		if (TreeNodeTypeEnum.FALDONE.getType().equals(getSelectedNode().getType())) {
			// Se sei un faldone carica i fascicoli interni.
			setNomeFaldoneSelected(data.getName());
			final Collection<MasterFascicoloDTO> fascicoli = faldoneSRV.getFascicoliFaldone(utente, data.getAbsolutePathFaldone());
			fascicoliDTH.setMasters(fascicoli);
		}

		FacesHelper.update("westSectionForm:idTabMain_LeftAside:idAccPanFaldoni_VM:treeFaldoni", FALDONIERICERCAFASCICOLI_CONTAINER_NAME, IDLABELFALDONIFALD_RESOURCE_NAME);
	}

	/**
	 * Gestisce la selezione di un nodo dell'albero. Dal vertical menu
	 * 
	 * @param event evento gui
	 */
	public void onNodeSelect(final TreeNode treeNode) {
		hideRicerca();
		unsetAllDetail();
		setSelectedNode(treeNode);
		final TreeNodeFaldoneDTO data = (TreeNodeFaldoneDTO) getSelectedNode().getData();
		fascicoliDTH = new SimpleDetailDataTableHelper<>();
		if (TreeNodeTypeEnum.FALDONE.getType().equals(getSelectedNode().getType())) {
			// Se sei un faldone carica i fascicoli interni.
			setNomeFaldoneSelected(data.getName());
			final Collection<MasterFascicoloDTO> fascicoli = faldoneSRV.getFascicoliFaldone(utente, data.getAbsolutePathFaldone());
			fascicoliDTH.setMasters(fascicoli);

			if (!fascicoliDTH.getMasters().isEmpty()) {
				fascicoliDTH.selectFirst(true);
				apriFascicolo(fascicoliDTH.getCurrentMaster());
			}
		}
		FacesHelper.update("eastSectionForm:idDettaglioDoc");
		FacesHelper.update("westSectionForm:idTabMain_LeftAside:idAccPanFaldoni_VM:treeFaldoni", FALDONIERICERCAFASCICOLI_CONTAINER_NAME, IDLABELFALDONIFALD_RESOURCE_NAME);
	}

	/**
	 * Metodo chiamato la prima volta che il nodo viene espanso.
	 * 
	 * @param event
	 */
	public void onNodeExpand(final NodeExpandEvent event) {
		final TreeNode expandedNode = event.getTreeNode();
		expandedNode.getChildren().clear();
		addFaldoni(expandedNode);
	}

	/**
	 * Inizializza la dialgo.
	 * 
	 * @param mode
	 */
	public void setupDialog(final String mode) {
		final ModeFaldonaturaEnum modeEnum = ModeFaldonaturaEnum.get(mode);
		final FaldonaturaBean faldonaturaBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.FALDONATURA_BEAN);
		if (ModeFaldonaturaEnum.UPDATE.equals(modeEnum) || ModeFaldonaturaEnum.DELETE.equals(modeEnum)) {
			faldonaturaBean.setFaldoneSelezionatoModifica(ConstantsWeb.MBean.FALDONI_BEAN, modeEnum, getSelectedNode());
		} else {
			faldonaturaBean.setFaldoneSelezionatoCreazione(ConstantsWeb.MBean.FALDONI_BEAN, modeEnum, getSelectedNode());
		}

		FacesHelper.update("westSectionForm:dlgFaldonaturaPnl");
	}

	/**
	 * Gestisce l'operazione del faldone.
	 * 
	 * @param modeEnum
	 *            modalita faldonatura
	 */
	@Override
	public void gestisciOperazioneFaldone(final ModeFaldonaturaEnum modeEnum) {
		final TreeNodeFaldoneDTO dto = (TreeNodeFaldoneDTO) getSelectedNode().getData();
		String parentName = null;
		if (ModeFaldonaturaEnum.CREATE.equals(modeEnum)) {
			getSelectedNode().getChildren().clear();
			parentName = dto.getDocumentTitle();
			final Collection<FaldoneDTO> faldoni = faldoneSRV.getFaldoni(utente, dto.getIdUfficio(), parentName);
			for (final FaldoneDTO faldone : faldoni) {
				addNodoFaldone(getSelectedNode(), dto.getIdUfficio(), faldone);
			}
		} else if (ModeFaldonaturaEnum.UPDATE.equals(modeEnum) || ModeFaldonaturaEnum.DELETE.equals(modeEnum)) {
			getSelectedNode().getParent().getChildren().clear();
			parentName = dto.getParentName();
			final Collection<FaldoneDTO> faldoni = faldoneSRV.getFaldoni(utente, dto.getIdUfficio(), parentName);
			for (final FaldoneDTO faldone : faldoni) {
				addNodoFaldone(getSelectedNode().getParent(), dto.getIdUfficio(), faldone);
			}
		}
	}

	/**
	 * I fascicoli vengono caricati senza mostrare i documenti interni, su richiesta
	 * questi vengono calcolati da tale metodo.
	 * 
	 * @param event evento GUI
	 */
	public void onRowToggle(final ToggleEvent event) {
		// I fascicoli vengono caricati senza documenti; all'apertura di un fascicolo,
		// solo la prima volta, andiamo ad inserire i documenti.
		final MasterFascicoloDTO fascicolo = (MasterFascicoloDTO) getDataTableClickedRow(event);
		if (fascicolo.getDocumenti() == null || fascicolo.getDocumenti().isEmpty()) {
			fascicolo.setDocumenti(getDocumentiFascicolo(fascicolo));
		}
	}

	/**
	 * Gestione della selezione di un documento del fascicolo.
	 * 
	 * @param event eventoGUI
	 */
	public final void documentRowSelector(final ActionEvent event) {
		final DocumentoFascicoloDTO doc = (DocumentoFascicoloDTO) getDataTableClickedRow(event);
		showDocument(doc);
		dettaglioDocumentoActive = true;
	}

	/**
	 * Esegue la ricerca dei fascicoli utilizzando i {@link #parametri} e popolando
	 * {@link #fascicoliRicerca}.
	 */
	public void ricercaFascicoli() {
		try {
			final Collection<FascicoloDTO> resultsFascicoli = fascicoloSRV.eseguiRicercaFascicoli(parametri, utente, false);
			fascicoliRicerca.setMasters(resultsFascicoli);

			clearFilterDataTableRicerca("idRisFascicoli_Fald", fascicoliRicerca);
			if (!fascicoliRicerca.getMasters().isEmpty()) {
				fascicoliRicerca.selectFirst(false);
				apriFascicoloRicerca(fascicoliRicerca.getCurrentMaster());
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Errore durante l'operazione di ricerca.");
		}
	}

	/**
	 * Visualizza la dialog della ricerca.
	 */
	public void showRicerca() {
		unsetAllDetail();
		modalitaRicercaFascicoli = true;
		parametri = new ParamsRicercaAvanzataFascDTO();
		parametri.setAnnoFascicolo(Calendar.getInstance().get(Calendar.YEAR));
		parametri.setFaldonato(false);
		fascicoliRicerca = new SimpleDetailDataTableHelper<>();
		fascicoliRicerca.setSelectedMasters(new ArrayList<>());
		fascicoliDTH = new SimpleDetailDataTableHelper<>();
		fascicoliDTH.setSelectedMasters(new ArrayList<>());
		FacesHelper.update(FALDONIERICERCAFASCICOLI_CONTAINER_NAME, IDLABELFALDONIFALD_RESOURCE_NAME);
		FacesHelper.executeJS("showCentralEastSection();");
	}

	/**
	 * Nasconde la dialog della ricerca.
	 */
	public void hideRicerca() {
		modalitaRicercaFascicoli = false;
		fascicoliDTH = new SimpleDetailDataTableHelper<>();
	}

	/**
	 * FALDONATURA START.
	 ******************************************************************************************/
	/**
	 * Mostra il faldone associato al fascicolo selezionato.
	 */
	public void apriFaldonaFascicolo() {
		if (faldonaCmp == null) {
			faldonaCmp = new DialogFaldonaComponent(utente);
		} else {
			faldonaCmp.resetValues();
		}

		if (fascicoliRicerca != null && fascicoliRicerca.getSelectedMasters() != null && !fascicoliRicerca.getSelectedMasters().isEmpty()) {
			faldonaCmp.setListaFascicoli(fascicoliRicerca.getSelectedMasters());
		} else if (fascicoliDTH != null && fascicoliDTH.getSelectedMasters() != null && !fascicoliDTH.getSelectedMasters().isEmpty()) {
			faldonaCmp.setListaFascicoli(new ArrayList<>());
			for (final MasterFascicoloDTO mf : fascicoliDTH.getSelectedMasters()) {
				faldonaCmp.getListaFascicoli().add(new FascicoloDTO(String.valueOf(mf.getId()), null, null, mf.getDataCreazione(), null, null, null, null,
						String.valueOf(mf.getId()), mf.getOggetto(), mf.getIdAOO().intValue(), null, null, null, null, null));
			}
		} else {
			showWarnMessage("Selezionare almeno un fascicolo.");
			return;
		}
		final Collection<FaldoneDTO> faldoni = new ArrayList<>();
		for (final FascicoloDTO fascicolo : faldonaCmp.getListaFascicoli()) {
			final List<FaldoneDTO> f = detailFascicoloSRV.getFaldoniByIdFascicolo(fascicolo.getIdFascicolo(), utente.getIdAoo(), utente);
			if (f != null && !f.isEmpty()) {
				for (final FaldoneDTO faldone : f) {
					faldone.setFascicoloSelezionato(fascicolo);
				}
				faldoni.addAll(f);
			}

		}
		faldonaCmp.setFaldoni(faldoni);

		dlgFaldonaturaRendered = true;
		FacesHelper.executeJS("PF('wdgFaldonaFascicolo_Fald').show();");
	}

	/**
	 * Esegue la funzionalità di dissociazione dei faldoni gestendo eventuali
	 * errori.
	 */
	public void dissociaFaldoni() {
		try {
			List<FascicoloDTO> fascicoliDaDissociare = new ArrayList<>();
			if (modalitaRicercaFascicoli) {
				// sono i fascicoli selezionati dalla ricerca
				fascicoliDaDissociare = fascicoliRicerca.getSelectedMasters();
			} else {
				// sono i fascicoli selezionati dalla selezione di un faldone
				for (final MasterFascicoloDTO mf : fascicoliDTH.getSelectedMasters()) {
					fascicoliDaDissociare.add(new FascicoloDTO(String.valueOf(mf.getId()), null, null, mf.getDataCreazione(), null, null, null, null,
							String.valueOf(mf.getId()), mf.getOggetto(), mf.getIdAOO().intValue(), null, null, null, null, null));
				}
			}

			if (fascicoliDaDissociare.isEmpty()) {
				showWarnMessage("Selezionare almeno un fascicolo.");
				return;
			}

			final List<FascicoloDTO> fascicoliDissociabili = new ArrayList<>();
			final StringBuilder esiti = new StringBuilder();

			Long idAoo = null;
			TreeNodeFaldoneDTO faldoneSelected = null;
			for (final FascicoloDTO fascicoloDTO : fascicoliDaDissociare) {
				faldoneSelected = (TreeNodeFaldoneDTO) getSelectedNode().getData();
				idAoo = Long.valueOf(fascicoloDTO.getIdAOO());
				if (faldoneSRV.isFaldoneDisassociabile(faldoneSelected.getDocumentTitle(), fascicoloDTO.getIdFascicolo(), idAoo, utente)) {
					fascicoliDissociabili.add(fascicoloDTO);
				} else {
					esiti.append(fascicoloDTO.getOggetto()).append(" ").append(EsitoFaldoneEnum.FALDONE_NON_DISASSOCIABILE.getText()).append(". ");
				}
			}

			EsitoFaldoneEnum esito = null;
			boolean showInfo = true;
			for (final FascicoloDTO f : fascicoliDissociabili) {
				idAoo = Long.valueOf(f.getIdAOO());
				esito = faldoneSRV.disassociaFascicoloDaFaldone(faldoneSelected.getDocumentTitle(), f.getIdFascicolo(), idAoo, utente);
				esiti.append(f.getOggetto()).append(" ");
				switch (esito) {
				case ERRORE_GENERICO:
					showInfo = false;
					esiti.append(EsitoFaldoneEnum.ERRORE_GENERICO.getText()).append(".");
					break;
				case FALDONE_DISASSOCIATO_DA_FASCICOLO_CON_SUCCESSO:
					esiti.append(EsitoFaldoneEnum.FALDONE_DISASSOCIATO_DA_FASCICOLO_CON_SUCCESSO.getText()).append(".");
					break;
				case FALDONE_NON_DISASSOCIABILE:
					showInfo = false;
					esiti.append(EsitoFaldoneEnum.FALDONE_NON_DISASSOCIABILE.getText()).append(".");
					break;

				default:
					break;
				}
			}

			if (modalitaRicercaFascicoli) {
				fascicoliRicerca.getSelectedMasters().clear();
			} else {
				fascicoliDTH.getSelectedMasters().clear();
			}

			if (showInfo) {
				showInfoMessage(esiti.toString());
			} else {
				showWarnMessage(esiti.toString());
			}

			refreshFascicoliDTH();

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di dissociazione dei faldoni.");
		}
	}

	/**
	 * Esegue la faldonatura dei fascicoli selezionati dal
	 * {@link #fascicoliRicerca}.
	 */
	public void faldona() {

		try {
			if (modalitaRicercaFascicoli) {
				// sono i fascicoli selezionati dalla ricerca
				faldonaCmp.setListaFascicoli(fascicoliRicerca.getSelectedMasters());
			} else {
				// sono i fascicoli selezionati dalla selezione di un faldone
				faldonaCmp.setListaFascicoli(new ArrayList<>());
				for (final MasterFascicoloDTO mf : fascicoliDTH.getSelectedMasters()) {
					faldonaCmp.getListaFascicoli().add(new FascicoloDTO(String.valueOf(mf.getId()), mf.getOggetto(), null, mf.getDataCreazione(), null, null, null));
				}
			}
			faldonaCmp.registraMassivo();

			closeDlgFaldonatura();

			FacesHelper.update("centralSectionForm:fascicoliPnl", "centralSectionForm:idDlgFaldonatura_Fald");

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}

	}

	/**
	 * Sposta i fascicoli selezionati da un faldone ad un altro faldone, gestisce
	 * eventuali errori riscontrati. Comunica l'esito a valle delle operazioni.
	 */
	public void spostaMassivo() {
		try {
			if (faldonaCmp.getFaldonePadre() == null && StringUtils.isNullOrEmpty(faldonaCmp.getOggettoNuovoFaldone())
					&& CollectionUtils.isEmptyOrNull(faldonaCmp.getFaldoniScelti())) {
				showWarnMessage("Nessun faldone è stato selezionato o creato.");
				return;
			}
			final TreeNodeFaldoneDTO faldPartenza = (TreeNodeFaldoneDTO) getSelectedNode().getData();

			final StringBuilder faldoniSpostati = new StringBuilder("I seguenti fascicoli sono stati spostati correttamente: ");
			final StringBuilder faldoniErrore = new StringBuilder();
			EsitoFaldoneEnum esito = null;
			boolean showInfo = true;
			for (final FascicoloDTO fascDaSpostare : faldonaCmp.getListaFascicoli()) {
				for (final FaldoneDTO faldArrivo : faldonaCmp.getFaldoniScelti()) {
					if (faldonaCmp.faldona(faldArrivo.getNomeFaldone(), fascDaSpostare.getIdFascicolo())) {
						esito = faldoneSRV.disassociaFascicoloDaFaldone(faldPartenza.getDocumentTitle(), fascDaSpostare.getIdFascicolo(),
								Long.valueOf(fascDaSpostare.getIdAOO()), utente);
						switch (esito) {
						case ERRORE_GENERICO:
							showInfo = false;
							faldoniErrore.append(" - ").append(fascDaSpostare.getOggetto()).append(EsitoFaldoneEnum.ERRORE_GENERICO.getText()).append(".");
							break;
						case FALDONE_DISASSOCIATO_DA_FASCICOLO_CON_SUCCESSO:
							faldoniSpostati.append(" - ").append(fascDaSpostare.getOggetto());
							break;
						case FALDONE_NON_DISASSOCIABILE:
							showInfo = false;
							faldoniErrore.append(" - ").append(fascDaSpostare.getOggetto()).append(EsitoFaldoneEnum.FALDONE_NON_DISASSOCIABILE.getText()).append(".");
							break;

						default:
							break;
						}
					} else {
						faldoniErrore.append(" - ").append("Il fascicolo [").append(fascDaSpostare.getOggetto()).append("]  è già stato faldonato nel faldone [")
								.append(fascDaSpostare.getOggetto()).append("]. ");
					}
				}
			}
			if (showInfo) {
				showInfoMessage(faldoniSpostati.toString());
			} else {
				showWarnMessage(faldoniErrore.toString());
			}

			closeDlgFaldonatura();
			refreshFascicoliDTH();

			FacesHelper.update("centralSectionForm:fascicoliPnl", "centralSectionForm:idDlgFaldonatura_Fald");

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}

	}

	/**
	 * Effettua un refresh del datatable dei fascicoli aggiornandone lo stato in
	 * accordo allo stato di persistenza.
	 */
	public void refreshFascicoliDTH() {
		final TreeNodeFaldoneDTO faldoneToRefresh = (TreeNodeFaldoneDTO) getSelectedNode().getData();
		fascicoliDTH = new SimpleDetailDataTableHelper<>();
		final Collection<MasterFascicoloDTO> fascicoli = faldoneSRV.getFascicoliFaldone(utente, faldoneToRefresh.getAbsolutePathFaldone());
		fascicoliDTH.setMasters(fascicoli);
	}

	/**
	 * Gestisce la chiusura della dialog di faldonatura.
	 */
	public void closeDlgFaldonatura() {
		try {
			faldonaCmp = null;
			dlgFaldonaturaRendered = false;
			FacesHelper.executeJS("PF('wdgFaldonaFascicolo_Fald').hide();");
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione.");
		}

	}

	/**
	 * FALDONATURA END.
	 ******************************************************************************************/

	/****************************************
	 * DETTAGLIO FASCICOLO
	 ***************************************/
	/**
	 * Gestisce la selezione del fascicolo di ricerca, ne mostra il contenuto
	 * chiamando {@link #apriFascicoloRicerca(FascicoloDTO)}.
	 * 
	 * @param se
	 */
	public void fascicoloSelector(final SelectEvent se) {
		final FascicoloDTO fascicolo = (FascicoloDTO) se.getObject();
		apriFascicoloRicerca(fascicolo);
	}

	private void apriFascicoloRicerca(final FascicoloDTO fascicolo) {
		final DettaglioFascicoloRicercaBean dettaglioFascicolo = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN);
		dettaglioFascicolo.setDetail(fascicolo);
		dettaglioDocumentoActive = false;
	}

	/**
	 * Gestisce la selezione del fascicolo, ne mostra il contenuto chiamando
	 * {@link #apriFascicolo(MasterFascicoloDTO)}.
	 * 
	 * @param se
	 */
	public void fascicoloMasterSelector(final SelectEvent se) {
		final MasterFascicoloDTO fascicolo = (MasterFascicoloDTO) se.getObject();
		apriFascicolo(fascicolo);
	}

	private void apriFascicolo(final MasterFascicoloDTO fascicolo) {
		final DettaglioFascicoloRicercaBean dettaglioFascicolo = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN);
		final List<DocumentoFascicoloDTO> documenti = fascicolo.getDocumenti() == null ? new ArrayList<>() : new ArrayList<>(fascicolo.getDocumenti());
		dettaglioFascicolo.setDetail(fascicolo.getId(), documenti);
		dettaglioDocumentoActive = false;
	}

	/**
	 * Resetta il dettaglio del fascicolo.
	 */
	public void unsetAllDetail() {
		if (fascicoliDTH != null && fascicoliDTH.getCurrentMaster() != null) {
			fascicoliDTH.setCurrentMaster(null);
		}

		if (fascicoliRicerca != null && fascicoliRicerca.getCurrentMaster() != null) {
			fascicoliRicerca.setCurrentMaster(null);
		}

		hideDocument();
		final DettaglioFascicoloRicercaBean dettaglioFascicolo = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN);
		dettaglioFascicolo.unsetDetail();
		dettaglioDocumentoActive = true;
	}

	/**
	 * Resetta le informazioni del datatable identificato dall'idComponent e il
	 * datatableHelper in ingresso.
	 * 
	 * @param idComponent
	 * @param dth
	 */
	public void clearFilterDataTable(final String idComponent, final SimpleDetailDataTableHelper<MasterFascicoloDTO> dth) {
		final DataTable d = getDataTableByIdComponent(idComponent);
		if (d != null) {
			d.reset();
			if (dth != null && dth.getMasters() != null) {
				d.setValue(dth.getMasters());
			}
		}
	}

	/**
	 * Resetta le informazioni del datatable identificato dall'idComponent e il
	 * datatableHelper in ingresso.
	 * 
	 * @param idComponent
	 * @param dth
	 */
	public void clearFilterDataTableRicerca(final String idComponent, final SimpleDetailDataTableHelper<FascicoloDTO> dth) {
		final DataTable d = getDataTableByIdComponent(idComponent);
		if (d != null) {
			d.reset();
			if (dth != null && dth.getMasters() != null) {
				d.setValue(dth.getMasters());
			}
		}
	}

	/*****************************
	 * Selezione multipla tabella
	 ****************************************/
	/**
	 * {@link #handleAllCheckBoxFaldoni(SimpleDetailDataTableHelper)}.
	 */
	public void handleAllCheckBox() {
		handleAllCheckBoxFaldoni(fascicoliDTH);
	}

	/**
	 * {@link #handleAllCheckBoxFaldoni(SimpleDetailDataTableHelper)}.
	 */
	public void handleAllCheckBoxRicerca() {
		handleAllCheckBoxFaldoniRicerca(fascicoliRicerca);
	}

	/**
	 * Gestisce tutte le checkbox dei faldoni.
	 * 
	 * @param dth
	 */
	private void handleAllCheckBoxFaldoni(final SimpleDetailDataTableHelper<MasterFascicoloDTO> dth) {
		super.handleAllCheckBoxGeneric(dth);
	}

	private void handleAllCheckBoxFaldoniRicerca(final SimpleDetailDataTableHelper<FascicoloDTO> dth) {
		super.handleAllCheckBoxGeneric(dth);
	}

	/**
	 * {@link #updateCheckFaldoni(AjaxBehaviorEvent, SimpleDetailDataTableHelper)}.
	 * 
	 * @param event
	 */
	public void updateCheck(final AjaxBehaviorEvent event) {
		updateCheckFaldoni(event, fascicoliDTH);
	}

	/**
	 * {@link #updateCheckFaldoniRicerca(AjaxBehaviorEvent, SimpleDetailDataTableHelper)}.
	 * 
	 * @param event
	 */
	public void updateCheckRicerca(final AjaxBehaviorEvent event) {
		updateCheckFaldoniRicerca(event, fascicoliRicerca);
	}

	/**
	 * Aggiorna la selezione dei faldoni in base alla checkbox associata.
	 * 
	 * @param event
	 * @param dth
	 */
	private void updateCheckFaldoni(final AjaxBehaviorEvent event, final SimpleDetailDataTableHelper<MasterFascicoloDTO> dth) {
		final MasterFascicoloDTO masterChecked = getDataTableClickedRow(event);
		final Boolean newValue = ((SelectBooleanCheckbox) event.getSource()).isSelected();
		if (newValue != null && newValue) {
			dth.getSelectedMasters().add(masterChecked);
		} else {
			dth.getSelectedMasters().remove(masterChecked);
		}
	}

	/**
	 * Aggiorna la selezione dei faldoni di ricerca in base alla checkbox associata.
	 * 
	 * @param event
	 * @param dth
	 */
	private void updateCheckFaldoniRicerca(final AjaxBehaviorEvent event, final SimpleDetailDataTableHelper<FascicoloDTO> dth) {
		final FascicoloDTO masterChecked = getDataTableClickedRow(event);
		final Boolean newValue = ((SelectBooleanCheckbox) event.getSource()).isSelected();
		if (newValue != null && newValue) {
			dth.getSelectedMasters().add(masterChecked);
		} else {
			dth.getSelectedMasters().remove(masterChecked);
		}
	}

	/**
	 * GET & SET.
	 ***************************************************************************/
	/**
	 * Restituisce l'helper per il datatable dei fascicoli.
	 * 
	 * @return helper datatable fascicoli
	 */
	public SimpleDetailDataTableHelper<MasterFascicoloDTO> getFascicoliDTH() {
		return fascicoliDTH;
	}

	/**
	 * Restituisce true se in modalita ricerca, false altrimenti.
	 * 
	 * @return true se in modalita ricerca, false altrimenti
	 */
	public boolean isModalitaRicercaFascicoli() {
		return modalitaRicercaFascicoli;
	}

	/**
	 * Imposta la modalita di ricerca.
	 * 
	 * @param modalitaRicercaFascicoli
	 */
	public void setModalitaRicercaFascicoli(final boolean modalitaRicercaFascicoli) {
		this.modalitaRicercaFascicoli = modalitaRicercaFascicoli;
	}

	/**
	 * Restituisce i parametri per definire la ricerca dei fascicoli.
	 * 
	 * @return parametri ricerca fascicoli
	 */
	public ParamsRicercaAvanzataFascDTO getParametri() {
		return parametri;
	}

	/**
	 * Restituisce l'helper per il datatable dei fascicoli di ricerca.
	 * 
	 * @return helper datatable fascicoli ricerca
	 */
	public SimpleDetailDataTableHelper<FascicoloDTO> getFascicoliRicerca() {
		return fascicoliRicerca;
	}

	/**
	 * Restituisce true se la dialog per la faldonatura è renderizzata, false
	 * altrimenti.
	 * 
	 * @return true se la dialog per la faldonatura è renderizzata, false altrimenti
	 */
	public boolean isDlgFaldonaturaRendered() {
		return dlgFaldonaturaRendered;
	}

	/**
	 * Restituisce il component per la gestione dei faldoni.
	 * 
	 * @return component faldoni
	 */
	public DialogFaldonaComponent getFaldonaCmp() {
		return faldonaCmp;
	}

	/**
	 * Restituisce il nome del faldone selezionato.
	 * 
	 * @return nome faldone selezionato
	 */
	public String getNomeFaldoneSelected() {
		return nomeFaldoneSelected;
	}

	/**
	 * Imposta il nome del faldone selezionato.
	 * 
	 * @param nomeFaldoneSelected
	 */
	public void setNomeFaldoneSelected(final String nomeFaldoneSelected) {
		this.nomeFaldoneSelected = nomeFaldoneSelected;
	}

	/**
	 * Restituisce true se il dettaglio documento è attivo, false altrimenti.
	 * 
	 * @return true se il dettaglio documento è attivo, false altrimenti
	 */
	public Boolean isDettaglioDocumentoActive() {
		return dettaglioDocumentoActive;
	}

	@Override
	public void onOpenTree(TreeNode nodo) {
		throw new NotImplementedException("Metodo non implementato");
	}

}