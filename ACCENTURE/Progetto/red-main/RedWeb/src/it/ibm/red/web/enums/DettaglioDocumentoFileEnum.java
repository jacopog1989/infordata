package it.ibm.red.web.enums;

/**
 * Enum che definisce i possibili dettagli documento.
 */
public enum DettaglioDocumentoFileEnum {
	
	/**
	 * DSR.
	 */
	FEPADSR("../documento/dettaglioDocumento_DSRFepa.xhtml"),
	
	/**
	 * DD.
	 */
	FEPADD("../documento/dettaglioDocumento_DDFepa.xhtml"),
	
	/**
	 * Fattura FEPA.
	 */
	FEPAFATTURA("../documento/dettaglioDocumento_FatturaFepa.xhtml"),
	
	/**
	 * Async Sign.
	 */
	ASYNCSIGN("../documento/dettaglioDocumento_FirmaAsincrona.xhtml"),
	
	/**
	 * Generico.
	 */
	GENERICO("../documento/dettaglioDocumento.xhtml");

	/**
	 * Imposta la stringa di dettaglio per la visualizzazione della view.
	 */
	private String dettaglioDocFile;

	/**
	 * 
	 * @param dettaglioDocFile
	 */
	DettaglioDocumentoFileEnum(final String dettaglioDocFile) {
		this.dettaglioDocFile = dettaglioDocFile;
	}

	/**
	 * Restituisce l'url del dettaglio per la visualizzazione della view.
	 * @return dettaglioDocFile
	 */
	public String getDettaglioDocFile() {
		return dettaglioDocFile;
	}
}
