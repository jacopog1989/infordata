/**
 * 
 */
package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NotaDTO;
import it.ibm.red.business.enums.AccessoFunzionalitaEnum;
import it.ibm.red.business.enums.ColoreNotaEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.INotaFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.IInserisciNota;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * @author APerquoti
 *
 */
@Named(ConstantsWeb.MBean.INSERISCI_NOTA_BEAN)
@ViewScoped
public class InserisciNotaBean extends AbstractBean implements InitHumanTaskInterface, IInserisciNota {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -5096856120291283779L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(InserisciNotaBean.class.getName());
	
	/**
	 * Service per la gestione delle note dei documenti
	 */
	private INotaFacadeSRV notaSRV;
	
	/**
	 * Lista documenti bean.
	 */
	private ListaDocumentiBean ldb;
	
	/**
	 * Bean di sessione.
	 */
	private SessionBean sb;
	
	/**
	 * testo contenente la nota.
	 */
	private String testoNota;
	
	/**
	 * Colore nota.
	 */
	private Integer coloreNota = 0;
	
	/**
	 * Master.
	 */
	private List<MasterDocumentRedDTO> masters;
	
	/**
	 * questo è il booleano che da response (radio in maschera) permette di salvare la nota su storico (true) oppure no (false)
	 * valore di default: true.
	 */
	private boolean alsoStorico = true; 
	
	/**
	 * Flag permesso aoo multinote.
	 */
	private boolean permessoAOOMultiNote;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
		sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		notaSRV = ApplicationContextProvider.getApplicationContext().getBean(INotaFacadeSRV.class);
		
		permessoAOOMultiNote = sb.haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.MULTI_NOTE);
	}

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		masters = inDocsSelezionati;
		if (masters.get(0).getNota() != null) {
			testoNota = masters.get(0).getNota().getContentNota();
			if (masters.get(0).getNota().getColore() != null) {
				coloreNota = masters.get(0).getNota().getColore().getId();
			}
		}
	}
	
	/**
	 * Inserisce la nota.
	 */
	@Override
	public void inserisciNotaResponse() {
		try {
			if (!validate()) {
				return;
			}
			final List<String> wobNumbers = new ArrayList<>();
			ldb.cleanEsiti();
			
			final NotaDTO nota = new NotaDTO();
			nota.setContentNota(testoNota);
			if (coloreNota != null) {
				nota.setColore(ColoreNotaEnum.get(coloreNota));
			} else {
				nota.setColore(null);
			}
			
			for (final MasterDocumentRedDTO m : masters) {
				if (!StringUtils.isNullOrEmpty(m.getWobNumber())) {
					wobNumbers.add(m.getWobNumber());
				}
			}
			final Collection<EsitoOperazioneDTO> esitoOperazione = notaSRV.registraNotaFromWorkflow(sb.getUtente(), wobNumbers, nota, null, alsoStorico);
			ldb.getEsitiOperazione().addAll(esitoOperazione);
			FacesHelper.update("centralSectionForm:idDlgShowResult");
			FacesHelper.update("centralSectionForm:idDlgGeneric");
			FacesHelper.executeJS("PF('dlgGeneric').hide();PF('dlgShowResult').show();");
			ldb.destroyBeanViewScope(MBean.INSERISCI_NOTA_BEAN);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'inserimento della nota.", e);
			showError("Si è verificato un errore durante l'inserimento della nota.");
		}
	}
	
	/**
	 * Valida la selezione del colore per salvare nello storico note.
	 * @return
	 */
	public boolean validate() {
		boolean valid = true;
		
		if (alsoStorico && coloreNota < 0) {
			showWarnMessage("Attenzione, per salvare nello storico note è necessario selezionare un colore.");
			valid = false;
		}
		
		return valid;
	}
	
	/**
	 * Elimina la nota.
	 */
	@Override
	public void eliminaNota() {
		setTestoNota(null);
		setColoreNota(null);
		// Non posso cancellare lo storico dalla response.
		setAlsoStorico(false);
		inserisciNotaResponse();
	}
	
	/**
	 * Chiude.
	 */
	@Override
	public void close() {
		ldb.destroyBeanViewScope(MBean.INSERISCI_NOTA_BEAN);
	}
	
	/**
	 * Recupera l'attributo permessoAOOMultiNote.
	 * @return permessoAOOMultiNote
	 */
	public boolean hasPermessoAOOMultiNote() {
		return permessoAOOMultiNote;
	}
	
	/**
	 * @return the testoNota
	 */
	@Override
	public final String getTestoNota() {
		return testoNota;
	}

	/**
	 * @param testoNota the testoNota to set
	 */
	@Override
	public final void setTestoNota(final String testoNota) {
		this.testoNota = testoNota;
	}
	
	/**
	 * @return the coloreNota
	 */
	@Override
	public Integer getColoreNota() {
		return coloreNota;
	}

	/**
	 * @param coloreNota the coloreNota to set
	 */
	@Override
	public void setColoreNota(final Integer coloreNota) {
		this.coloreNota = coloreNota;
	}

	/**
	 * Recupera l'attributo alsoStorico.
	 * @return alsoStorico
	 */
	public boolean isAlsoStorico() {
		return alsoStorico;
	}

	/**
	 * Imposta l'attributo alsoStorico.
	 * @param alsoStorico
	 */
	public void setAlsoStorico(final boolean alsoStorico) {
		this.alsoStorico = alsoStorico;
	}
	
}