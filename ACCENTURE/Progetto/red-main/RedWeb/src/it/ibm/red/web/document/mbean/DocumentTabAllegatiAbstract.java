package it.ibm.red.web.document.mbean;

import java.io.ByteArrayInputStream;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.UploadedFile;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DocumentManagerDTO;
import it.ibm.red.business.dto.EsitoDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.ResponsabileCopiaConformeDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;

/**
 * Abstract bean del tab allegati di un documento.
 */
public abstract class DocumentTabAllegatiAbstract extends AbstractBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -9151478663612648039L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DocumentTabAllegatiAbstract.class);

	/**
	 * Messaggio generico di errore. Occorre appendere l'operazione al messaggio.
	 */
	private static final String GENERIC_ERROR_MSG = "Errore durante l'operazione: ";

	/**
	 * Service.
	 */
	private IDocumentManagerFacadeSRV documentManagerSRV;

	/**
	 * Rimuove l'allegato identificato dall'index contenuto nella lista.
	 * 
	 * @param index
	 * @param allegati
	 * @return l'allegato rimosso
	 */
	public AllegatoDTO rimuoviAllegato(final Object index, final List<AllegatoDTO> allegati) {
		AllegatoDTO rimosso = null;
		try {
			final Integer i = (Integer) index;
			rimosso = allegati.remove(i.intValue());
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_MSG + e.getMessage());
		}
		return rimosso;
	}

	/**
	 * Rimuove l'allegato identificato dall'index contenuto nella lista degli
	 * allegati.
	 * 
	 * @param index
	 * @param dmDto
	 * @param allegati
	 * @param sizeContent
	 * @return il sizeContent dopo la rimozione dell'allegato, se avvenuta
	 */
	public Float rimuoviFileAllegato(final Object index, final DocumentManagerDTO dmDto, final List<AllegatoDTO> allegati, final Float sizeContent) {
		Float size = null;
		try {
			final Integer i = (Integer) index;
			if (i >= allegati.size()) {
				throw new RedException("Errore nel caricamento file.");
			}

			final AllegatoDTO allegatoSelected = allegati.get(i);

			if ((dmDto.isInCreazioneUscita() || dmDto.isInModificaUscita()) && allegatoSelected.getContentSize() != null) {
				size = sizeContent - allegatoSelected.getContentSize();
			}

			allegatoSelected.setNomeFile(null);
			allegatoSelected.setMimeType(null);
			allegatoSelected.setContent(null);
			allegatoSelected.setContentSize(null);

			allegatoSelected.setFormatoOriginale(false);
			allegatoSelected.setCheckMantieniFormatoOriginaleDisable(false);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_MSG + e.getMessage());
		}
		return size;
	}

	/**
	 * Aggiunge un nuovo allegato alla lista degli allegati.
	 * 
	 * @param allegati
	 * @param documentManagerDTO
	 * @param mantieniFormatoOriginale
	 * @param timbroUscitaAoo
	 * @return l'allegato aggiunto
	 */
	public AllegatoDTO addAllegato(final List<AllegatoDTO> allegati, final DocumentManagerDTO documentManagerDTO, final boolean mantieniFormatoOriginale,
			final boolean timbroUscitaAoo) {
		AllegatoDTO a = null;
		try {
			final Integer index = 0;
			a = new AllegatoDTO();
			a.setFormatoSelected(FormatoAllegatoEnum.ELETTRONICO);
			a.setFormato(FormatoAllegatoEnum.ELETTRONICO.getId());
			a.setNewAllegato(true);

			a.setComboTipologieDocumentoAllegati(documentManagerDTO.getComboTipologiaDocumento());

			allegati.add(index, a); // Append sempre in testa
			onChangeComboFormatoAllegato(index, allegati, documentManagerDTO, mantieniFormatoOriginale, timbroUscitaAoo);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Errore durante l'aggiunta di un nuovo allegato.");
		}
		return a;
	}

	/**
	 * Aggiunge un nuovo allegato cartaceo alla lista degli allegati.
	 * 
	 * @param allegati
	 * @param documentManagerDTO
	 * @param mantieniFormatoOriginale
	 * @return l'allegato cartaceo aggiunto
	 */
	public AllegatoDTO addAllegatoCartaceo(final List<AllegatoDTO> allegati, final DocumentManagerDTO documentManagerDTO, final boolean mantieniFormatoOriginale) {
		AllegatoDTO a = null;
		try {
			final Integer index = 0;
			a = new AllegatoDTO();
			a.setFormatoSelected(FormatoAllegatoEnum.CARTACEO);
			a.setFormato(FormatoAllegatoEnum.CARTACEO.getId());
			a.setNewAllegato(true);

			a.setComboTipologieDocumentoAllegati(documentManagerDTO.getComboTipologiaDocumento());

			allegati.add(index, a); // Append sempre in testa
			onChangeComboFormatoAllegato(index, allegati, documentManagerDTO, mantieniFormatoOriginale, false);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Errore durante l'aggiunta di un nuovo allegato.");
		}
		return a;
	}

	/**
	 * Gestisce l'evento di modifica associato al formato dell'allegato.
	 * 
	 * @param index
	 * @param allegati
	 * @param documentManagerDTO
	 * @param mantieniFormatoOriginale
	 * @param timbroUscitaAoo
	 */
	public void onChangeComboFormatoAllegato(final Object index, final List<AllegatoDTO> allegati, final DocumentManagerDTO documentManagerDTO,
			final boolean mantieniFormatoOriginale, final boolean timbroUscitaAoo) {
		onChangeComboFormatoAllegato(index, allegati, documentManagerDTO, mantieniFormatoOriginale, true, timbroUscitaAoo);
	}

	/**
	 * Gestisce l'evento di modifica associato al formato dell'allegato.
	 * 
	 * @param index
	 * @param allegati
	 * @param documentManagerDTO
	 * @param mantieniFormatoOriginale
	 * @param svuota
	 * @param timbroUscitaAoo
	 */
	public void onChangeComboFormatoAllegato(final Object index, final List<AllegatoDTO> allegati, final DocumentManagerDTO documentManagerDTO,
			final boolean mantieniFormatoOriginale, boolean svuota, final boolean timbroUscitaAoo) {

		AllegatoDTO selected = null;

		try {
			final Integer i = (Integer) index;
			if (i >= allegati.size()) {
				throw new RedException("Errore nell'aggiornamento del formato allegato.");
			}

			selected = allegati.get(i);
			selected.setFormato(selected.getFormatoSelected().getId());

			if (selected.getContent() != null) {
				final ISignFacadeSRV signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignFacadeSRV.class);
				final SessionBean sBean = FacesHelper.getManagedBean(Constants.ManagedBeanName.SESSION_BEAN);
				final boolean hassignature = signSRV.hasSignatures(new ByteArrayInputStream(selected.getContent()), sBean.getUtente());
				if (FormatoAllegatoEnum.ELETTRONICO.equals(selected.getFormatoSelected()) && hassignature) {

					selected.setFormatoSelected(FormatoAllegatoEnum.FIRMATO_DIGITALMENTE);
					showWarnMessage("Il documento risulta firmato digitalmente non è possibile impostare il formato elettronico");
				}
				if (FormatoAllegatoEnum.FIRMATO_DIGITALMENTE.equals(selected.getFormatoSelected()) && hassignature) {
					svuota = false;
				}
			}

			switch (selected.getFormatoSelected()) {
			case CARTACEO:
			
				handleFormatoCartaceo(selected, svuota);
				break;
			case ELETTRONICO:
				
				handleFormatoElettronico(documentManagerDTO, timbroUscitaAoo, selected);
				break;
			case NON_SPECIFICATO:
				
				handleFormatoNonSpecificato(svuota, selected);
				break;
			case FIRMATO_DIGITALMENTE:
				
				handleFormatoFirmatoDigitalmente(documentManagerDTO, svuota, timbroUscitaAoo, selected);
				break;
			default:
				break;
			}

			selected.setMantieniFormatoOriginale(false);
			if (selected.isCheckMantieniFormatoOriginaleVisible()
					&& (mantieniFormatoOriginale && (selected.isNewAllegato() || !documentManagerDTO.isInModifica() || selected.isCreateFromAllacci()))) {
				selected.setMantieniFormatoOriginale(true);
				selected.setCheckMantieniFormatoOriginaleDisable(true);
				selected.setFormatoOriginale(true);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Imposta gli attributi dell'allegato selezionato quando il formato è: Firmato
	 * digitalmente.
	 * 
	 * @param documentManagerDTO
	 *            Informazioni sul documento
	 * @param svuota
	 *            Indica se occorre svuotare il content dell'allegato selezionato.
	 * @param timbroUscitaAoo
	 *            Flag associato al timbro uscita dell'Area organizzativa.
	 * @param selected
	 *            Allegato selezionato da aggiornare con le informazioni tipiche di
	 *            un allegato elettronico.
	 */
	private void handleFormatoFirmatoDigitalmente(final DocumentManagerDTO documentManagerDTO, boolean svuota,
			final boolean timbroUscitaAoo, AllegatoDTO selected) {
		selected.setFormatoOriginale(true);
		selected.setCheckMantieniFormatoOriginaleVisible(false);

		if (getDocumentManagerSrv().isAllegatiCheckDaFirmareVisible(documentManagerDTO.getCategoria())) {
			selected.setCheckDaFirmareVisible(true);
			selected.setDaFirmareBoolean(false);
			selected.setDaFirmare(0);
		} else {
			selected.setCheckDaFirmareVisible(false);
			selected.setDaFirmareBoolean(false);
			selected.setDaFirmare(0);
		}

		selected.setCheckCopiaConformeVisible(false);
		selected.setCopiaConforme(false);
		selected.setIdCopiaConforme(null);
		selected.setIdUfficioCopiaConforme(null);
		selected.setIdUtenteCopiaConforme(null);

		selected.setUploadFileVisible(true);
		// resetto eventualmente il file perché potrebbe non essere firmato
		if (svuota) {
			selected.setNomeFile(null);
			selected.setMimeType(null);
			selected.setContent(null);
		}
		selected.setPosizioneVisible(false);
		selected.setPosizione(null);
		selected.setBarcodeVisible(false);
		selected.setBarcode(null);
		selected.setStampigliaturaSigla(false);
		selected.setCheckStampigliaturaSiglaVisible(false);

		if (timbroUscitaAoo && selected.isNewAllegato()) {
			selected.setTimbroUscitaAoo(true);
			selected.setCheckTimbroUscitaAooDisabled(false);
		}
	}

	/**
	 * Imposta gli attributi dell'allegato selezionato quando non è specificato.
	 * 
	 * @param svuota
	 *            Indica se occorre svuotare il content dell'allegato selezionato.
	 * @param selected
	 *            Allegato selezionato da aggiornare con le informazioni tipiche di
	 *            un allegato cartaceo.
	 */
	private static void handleFormatoNonSpecificato(boolean svuota, AllegatoDTO selected) {
		
		selected.setFormatoOriginale(false);
		selected.setCheckMantieniFormatoOriginaleVisible(false);
		selected.setTimbroUscitaAoo(false);

		selected.setCheckDaFirmareVisible(false);
		selected.setDaFirmareBoolean(false);
		selected.setDaFirmare(0);

		selected.setCheckCopiaConformeVisible(false);
		selected.setCopiaConforme(false);
		selected.setIdCopiaConforme(null);
		selected.setIdUfficioCopiaConforme(null);
		selected.setIdUtenteCopiaConforme(null);

		selected.setUploadFileVisible(false);
		// resetto eventualmente il file
		if (svuota) {
			selected.setNomeFile(null);
			selected.setMimeType(null);
			selected.setContent(null);
		}
		selected.setPosizioneVisible(true);
		selected.setPosizione(null);
		selected.setBarcodeVisible(false);
		selected.setBarcode(null);
		selected.setCheckStampigliaturaSiglaVisible(false);
	}

	/**
	 * Imposta gli attributi dell'allegato selezionato quando elettronico.
	 * 
	 * @param documentManagerDTO
	 *            Informazioni sul documento
	 * @param timbroUscitaAoo
	 *            Flag associato al timbro uscita dell'Area organizzativa.
	 * @param selected
	 *            Allegato selezionato da aggiornare con le informazioni tipiche di
	 *            un allegato elettronico.
	 */
	private void handleFormatoElettronico(final DocumentManagerDTO documentManagerDTO, final boolean timbroUscitaAoo, AllegatoDTO selected) {
		
		selected.setFormatoOriginale(false);
		selected.setCheckMantieniFormatoOriginaleVisible(true);
		selected.setCheckCopiaConformeVisible(true);
		if (getDocumentManagerSrv().isAllegatiCheckDaFirmareVisible(documentManagerDTO.getCategoria())) {
			selected.setCheckDaFirmareVisible(true);
			selected.setDaFirmareBoolean(false);
			selected.setDaFirmare(0);
		} else {
			selected.setCheckDaFirmareVisible(false);
			selected.setDaFirmareBoolean(false);
			selected.setDaFirmare(0);
		}

		if (getDocumentManagerSrv().isResponsabiliCopiaConformeForAllegatoVisible(documentManagerDTO.getCategoria(),
				documentManagerDTO.getComboResponsabileCopiaConforme().isEmpty())) {
			selected.setCheckCopiaConformeVisible(true);
			selected.setCopiaConforme(false);
			selected.setIdCopiaConforme(null);
			selected.setIdUfficioCopiaConforme(null);
			selected.setIdUtenteCopiaConforme(null);
		}

		selected.setUploadFileVisible(true);

		selected.setPosizioneVisible(false);
		selected.setPosizione(null);
		selected.setBarcodeVisible(false);
		selected.setBarcode(null);

		selected.setCheckStampigliaturaSiglaVisible(false);
		selected.setStampigliaturaSigla(false);

		selected.setFirmaVisibile(false);
		selected.setCheckFirmaVisibileVisible(documentManagerDTO.isInCreazioneUscita() || documentManagerDTO.isInModificaUscita());

		if (documentManagerDTO.getCheckStampigliaturaSiglaVisible()) {
			selected.setCheckStampigliaturaSiglaVisible(true);
			selected.setStampigliaturaSigla(true);
			selected.setDaFirmareBoolean(true);
			selected.setDaFirmare(1);
		}

		if (timbroUscitaAoo && selected.isNewAllegato()) {
			selected.setTimbroUscitaAoo(true);
			selected.setCheckTimbroUscitaAooDisabled(false);
		}
	}

	/**
	 * Imposta gli attributi dell'allegato selezionato quando cartaceo.
	 * 
	 * @param selected
	 *            Allegato selezionato da aggiornare con le informazioni tipiche di
	 *            un allegato cartaceo.
	 * @param svuota
	 *            Indica se occorre svuotare il content dell'allegato selezionato.
	 */
	private static void handleFormatoCartaceo(AllegatoDTO selected, boolean svuota) {
		selected.setFormatoOriginale(false);
		selected.setCheckMantieniFormatoOriginaleVisible(false);
		selected.setTimbroUscitaAoo(false);

		selected.setCheckDaFirmareVisible(false);
		selected.setDaFirmareBoolean(false);
		selected.setDaFirmare(0);

		selected.setCheckCopiaConformeVisible(false);
		selected.setCopiaConforme(false);
		selected.setIdCopiaConforme(null);
		selected.setIdUfficioCopiaConforme(null);
		selected.setIdUtenteCopiaConforme(null);

		selected.setUploadFileVisible(false);

		selected.setFirmaVisibile(null);
		selected.setCheckFirmaVisibileVisible(false);
		
		// resetto eventualmente il file
		if (svuota) {
			selected.setNomeFile(null);
			selected.setMimeType(null);
			selected.setContent(null);
		}

		selected.setPosizioneVisible(true);
		selected.setPosizione(null);
		selected.setBarcodeVisible(true);
		selected.setBarcode(null);
		selected.setStampigliaturaSigla(false);
		selected.setCheckStampigliaturaSiglaVisible(false);
	}

	/**
	 * Gestisce l'upload del file allegato.
	 * 
	 * @param index
	 * @param file
	 * @param allegati
	 * @param utente
	 * @param dm
	 * @param valueAndDisableFlag
	 * @param sizeContentTotal
	 * @return la dimensione raggiunta dopo l'update
	 */
	public Float handleFileUploadAllegato(final Integer index, final UploadedFile file, final List<AllegatoDTO> allegati, final UtenteDTO utente, final DocumentManagerDTO dm,
			final Boolean valueAndDisableFlag, final Float sizeContentTotal) {
		Float size = null;
		try {
			final AllegatoDTO allegatoSelected = allegati.get(index);

			// Gestione dei MIME Type non riconosciuti dal componente di PrimeFaces (e.g.
			// file EML)
			String contentType = file.getContentType();
			if (ContentType.MIMETYPE_ENC.equals(contentType)) {
				contentType = FileUtils.getMimeType(FilenameUtils.getExtension(file.getFileName()));
			}

			final FileDTO f = new FileDTO(file.getFileName(), file.getContents(), contentType);
			final EsitoDTO esito = getDocumentManagerSrv().checkUploadFileAllegato(f, allegatoSelected, utente, dm, allegatoSelected.getFormatoSelected());
			if (!esito.isEsito()) {
				showError(esito.getNote());
				return null;
			}

			allegatoSelected.setNomeFile(f.getFileName());
			allegatoSelected.setMimeType(f.getMimeType());
			allegatoSelected.setContent(f.getContent());

			if (allegatoSelected.getContent().length > 0) {
				final Float sizeAllegatoConv = (((float) allegatoSelected.getContent().length / 1024) / 1024);
				size = sizeContentTotal + sizeAllegatoConv;
				final String sizeRoundMB = String.format("%.2f", sizeAllegatoConv).replace(",", ".");
				allegatoSelected.setContentSize(Float.parseFloat(sizeRoundMB));
			}
			if (Boolean.TRUE.equals(valueAndDisableFlag)) {
				allegatoSelected.setFormatoOriginale(true);
				allegatoSelected.setCheckMantieniFormatoOriginaleDisable(true);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_MSG + e.getMessage());
		}
		return size;
	}

	/**
	 * Effettua una validazione sul file allegato in fase di upload restituendo l'esito della validazione.
	 * 
	 * @param file
	 * @param allegatoSelected
	 * @param utente
	 * @param dm
	 * @param valueAndDisableFlag
	 * @return EsitoDTO informazioni sull'esito della validazione
	 */
	public EsitoDTO handleFileUploadAllegati(final UploadedFile file, final AllegatoDTO allegatoSelected, final UtenteDTO utente, final DocumentManagerDTO dm,
			final Boolean valueAndDisableFlag) {
		
		EsitoDTO esito = new EsitoDTO();
		esito.setEsito(false);
		esito.setNote("Errore generico in fase di upload file.");
		
		try {

			// Gestione dei MIME Type non riconosciuti dal componente di PrimeFaces (e.g.
			// file EML)
			String contentType = file.getContentType();
			if (ContentType.MIMETYPE_ENC.equals(contentType)) {
				contentType = FileUtils.getMimeType(FilenameUtils.getExtension(file.getFileName()));
			}

			final FileDTO f = new FileDTO(file.getFileName(), file.getContents(), contentType);
			esito = getDocumentManagerSrv().checkUploadFileAllegato(f, allegatoSelected, utente, dm, allegatoSelected.getFormatoSelected());
			if (!esito.isEsito()) {
				showError(esito.getNote());
			}

			if (Boolean.TRUE.equals(valueAndDisableFlag)) {
				allegatoSelected.setFormatoOriginale(true);
				allegatoSelected.setCheckMantieniFormatoOriginaleDisable(true);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_MSG + e.getMessage());
		}
		
		return esito;
	}

	/**
	 * Gestisce l'evento di modifica associato al responsabile della copia conforme.
	 * 
	 * @param index
	 * @param allegati
	 * @param documentManagerDTO
	 */
	public void onChangeResponsabileCopiaConformeAllegato(final Integer index, final List<AllegatoDTO> allegati, final DocumentManagerDTO documentManagerDTO) {
		try {
			if (index >= allegati.size()) {
				throw new RedException("Errore nell'aggiornamento del responabile copia conforme.");
			}

			final List<ResponsabileCopiaConformeDTO> list = documentManagerDTO.getComboResponsabileCopiaConforme();
			ResponsabileCopiaConformeDTO r = list.get(0);

			final AllegatoDTO allegatoSelected = allegati.get(index);

			final String id = allegatoSelected.getIdCopiaConforme();
			if (StringUtils.isNotBlank(id)) {
				r = getDocumentManagerSrv().selectResponsabileById(id, list);
			}

			allegatoSelected.setIdUfficioCopiaConforme(r.getIdNodo());
			allegatoSelected.setIdUtenteCopiaConforme(r.getIdUtente());
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Aggiorna il flag daFirmare dell'allegato identificato dalla posizione "index"
	 * nella lista "allegati".
	 * 
	 * @param index
	 * @param allegati
	 */
	public void clickCheckDaFirmare(final Integer index, final List<AllegatoDTO> allegati) {
		try {
			if (index >= allegati.size()) {
				throw new RedException("Errore nell'aggiornamento del flag Da Firmare.");
			}

			final AllegatoDTO allegatoSelected = allegati.get(index);
			allegatoSelected.setDaFirmare(allegatoSelected.isDaFirmareBoolean() ? 1 : 0);

			// Gestione del flag relativo alla Copia Conforme
			if (!allegatoSelected.isCheckCopiaConformeDisabled()) {
				if (allegatoSelected.isDaFirmareBoolean()) {
					disableCopiaConforme(allegatoSelected);
				}
			} else if (!allegatoSelected.isMantieniFormatoOriginale()) {
				allegatoSelected.setCheckCopiaConformeDisabled(false);
			}

			if (!allegatoSelected.isDaFirmareBoolean()) {
				allegatoSelected.setFirmaVisibile(false);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Aggiorna il flag invio copia conforme dell'allegato identificato dalla
	 * posizione "index" nella lista "allegati".
	 * 
	 * @param index
	 * @param allegati
	 * @param documentManagerDTO
	 */
	public void clickCheckInvioCopiaConforme(final Integer index, final List<AllegatoDTO> allegati, final DocumentManagerDTO documentManagerDTO) {
		try {
			if (index >= allegati.size()) {
				throw new RedException("Errore nell'aggiornamento del flag Invio Copia Conforme.");
			}

			final AllegatoDTO allegatoSelected = allegati.get(index);

			// Si nasconde il barcode e si resettano i flag "Da Firmare" e "Mantieni Formato
			// Originale"
			if (Boolean.TRUE.equals(allegatoSelected.getCopiaConforme())) {
				allegatoSelected.setBarcodeVisible(false);
				allegatoSelected.setMantieniFormatoOriginale(false);
				allegatoSelected.setDaFirmare(0);
				allegatoSelected.setDaFirmareBoolean(false);

				// Si gestisce la combo per la selezione del Responsabile Copia Conforme
				onChangeResponsabileCopiaConformeAllegato(index, allegati, documentManagerDTO);
			} else if (FormatoAllegatoEnum.CARTACEO.equals(allegatoSelected.getFormatoSelected())) {
				allegatoSelected.setBarcodeVisible(true);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Aggiorna il flag "Mantieni Formato Originale" dell'allegato identificato
	 * dalla posizione "index" nella lista "allegati".
	 * 
	 * @param index
	 * @param allegati
	 * @param timbroUscitaAoo
	 */
	public void clickCheckMantieniFormatoOriginale(final Integer index, final List<AllegatoDTO> allegati, final boolean timbroUscitaAoo) {
		try {
			if (index >= allegati.size()) {
				throw new RedException("Errore nell'aggiornamento del flag Mantieni Formato Originale.");
			}

			final AllegatoDTO allegatoSelected = allegati.get(index);
			allegatoSelected.setMantieniFormatoOriginale(!allegatoSelected.isMantieniFormatoOriginale());

			// Gestione del flag relativo alla Copia Conforme
			if (!allegatoSelected.isCheckCopiaConformeDisabled()) {
				if (allegatoSelected.isMantieniFormatoOriginale()) {
					disableCopiaConforme(allegatoSelected);
				}
			} else if (!allegatoSelected.isDaFirmareBoolean()) {
				allegatoSelected.setCheckCopiaConformeDisabled(false);
			}

			if (allegatoSelected.isMantieniFormatoOriginale() || allegatoSelected.getStampigliaturaSigla()) {
				allegatoSelected.setStampigliaturaSigla(false);
			}

			if (timbroUscitaAoo) {
				allegatoSelected.setTimbroUscitaAoo(!allegatoSelected.isMantieniFormatoOriginale());
				allegatoSelected.setCheckTimbroUscitaAooDisabled(allegatoSelected.isMantieniFormatoOriginale());
			}

			allegatoSelected.setFirmaVisibile(false);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Aggiorna il flag "stampiglia sigla" dell'allegato identificato dalla
	 * posizione "index" nella lista "allegati".
	 * 
	 * @param index
	 * @param allegati
	 */
	public void clickCheckStampigliaSigla(final Integer index, final List<AllegatoDTO> allegati) {
		try {
			if (index >= allegati.size()) {
				throw new RedException("Errore nell'aggiornamento del flag stampiglia sigla");
			}

			final AllegatoDTO allegatoSelected = allegati.get(index);
			if (Boolean.TRUE.equals(allegatoSelected.getFormatoOriginale())) {
				allegatoSelected.setFormatoOriginale(false);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Restituisce il SRV del documentManager, se null lo inietta.
	 * 
	 * @return documentManagerSRV
	 */
	public IDocumentManagerFacadeSRV getDocumentManagerSrv() {
		if (documentManagerSRV == null) {
			documentManagerSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentManagerFacadeSRV.class);
		}
		return documentManagerSRV;
	}

	private static void disableCopiaConforme(final AllegatoDTO allegatoSelected) {
		allegatoSelected.setCheckCopiaConformeDisabled(true);
		allegatoSelected.setCopiaConforme(false);

		allegatoSelected.setIdCopiaConforme(Constants.EMPTY_STRING);
		allegatoSelected.setIdUfficioCopiaConforme(null);
		allegatoSelected.setIdUtenteCopiaConforme(null);
	}

	@Override
	@PostConstruct
	protected void postConstruct() {
		// Non occorre fare niente nel post construct del bean.
	}

	/**
	 * Aggiungi allegato cartaceo.
	 */
	public void addAllegatoCartaceo() {
		// Non occorre fare niente in questo metodo.
	}

	/**
	 * Aggiorna il flag "timbro uscita" dell'allegato identificato dalla posizione
	 * "index" nella lista "allegati".
	 * 
	 * @param index
	 * @param allegati
	 */
	public void clickCheckTimbroUscita(final Integer index, final List<AllegatoDTO> allegati) {
		try {
			if (index >= allegati.size()) {
				throw new RedException("Errore nell'aggiornamento del flag timbro uscita");
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Aggiorna il flag "Firma Visibile" dell'allegato identificato dalla posizione
	 * "index" nella lista "allegati".
	 * 
	 * @param index
	 * @param allegati
	 */
	public void clickCheckFirmaVisibile(final Integer index, final List<AllegatoDTO> allegati) {
		try {
			if (index >= allegati.size()) {
				throw new RedException("Errore nell'aggiornamento del flag Firma Visibile.");
			}

			final AllegatoDTO allegatoSelected = allegati.get(index);

			if ((allegatoSelected.isMantieniFormatoOriginale() && Boolean.TRUE.equals(allegatoSelected.getFormatoOriginale()))
					&& Boolean.TRUE.equals(allegatoSelected.getFirmaVisibile())) {

				allegatoSelected.setFormatoOriginale(false);
			}

			if (Boolean.TRUE.equals(allegatoSelected.getFirmaVisibile())) {
				allegatoSelected.setDaFirmareBoolean(true);
				allegatoSelected.setDaFirmare(1);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_MSG + e.getMessage());
		}
	}
}
