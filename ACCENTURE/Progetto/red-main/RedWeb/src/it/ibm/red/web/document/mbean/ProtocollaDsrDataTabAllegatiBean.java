package it.ibm.red.web.document.mbean;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IScompattaMailFacadeSRV;

/**
 * Classe ProtocollaDsrDataTabAllegatiBean.
 */
public class ProtocollaDsrDataTabAllegatiBean extends DsrDataTabAllegatiBean {

	/**
	 * Constante serial version UID
	 */
	private static final long serialVersionUID = -1998731223394127735L;
	
	/**
	 * LOGGER per la gestione dei messaggi in console.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ProtocollaDsrDataTabAllegatiBean.class);

	/**
	 * Costruttore protocolla dsr data tab allegati bean.
	 *
	 * @param inDetail the in detail
	 * @param utente   the utente
	 */
	public ProtocollaDsrDataTabAllegatiBean(final DetailDocumentRedDTO inDetail, final UtenteDTO utente) {
		super(inDetail, utente);
	}

	/**
	 * Download allegato.
	 *
	 * @param index the index
	 * @return the streamed content
	 */
	@Override
	public StreamedContent downloadAllegato(final Object index) {
		StreamedContent file = null;
		final Integer i = (Integer) index;
		try {
			final IScompattaMailFacadeSRV mailSRV = ApplicationContextProvider.getApplicationContext().getBean(IScompattaMailFacadeSRV.class);
			final AllegatoDTO allegatoSelezionato = getAllegati().get(i);
			final String idAllegato = allegatoSelezionato.getNodeIdFromHierarchicalFile();
			final String mimeType = allegatoSelezionato.getMimeType();
			final String nome = allegatoSelezionato.getNomeFile();

			final InputStream stream = new ByteArrayInputStream(mailSRV.getContentutoDiscompattataMail(getUtente(), idAllegato));
			file = new DefaultStreamedContent(stream, mimeType, nome);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il download del file", e);
			showError("Errore durante il download del file");
		}
		return file;
	}

}
