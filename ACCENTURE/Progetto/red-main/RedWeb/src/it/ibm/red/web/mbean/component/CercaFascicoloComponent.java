package it.ibm.red.web.mbean.component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.faces.context.FacesContext;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.tabview.TabView;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.DocumentiEFascicoliDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MasterFascicoloDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.RicercaPerBusinessEntityEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.facade.IRicercaFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.mbean.component.AbstractComponent;
import it.ibm.red.web.document.mbean.component.AbstractIndiceDiClassificazioneComponent;
import it.ibm.red.web.document.mbean.component.FascicoliPerTitolario;
import it.ibm.red.web.document.mbean.component.IndiceDiClassificazioneGenericComponent;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.interfaces.ICercaFascicoloComponent;
import it.ibm.red.web.mbean.interfaces.IUpdatableDetailCaller;

/**
 * Component per la ricerca e selezione di un fascicolo.
 * 
 * @author m.crescentini
 *
 */
public class CercaFascicoloComponent extends AbstractComponent implements ICercaFascicoloComponent {

	private static final long serialVersionUID = -335623691900394218L;

	/**
	 * Messaggio di errore generico operazione, occorre appendere l'errore specifico
	 * al messaggio.
	 */
	private static final String GENERIC_ERROR_OPERATION_MSG = "Si è verificato un errore durante l'operazione: ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CercaFascicoloComponent.class);

	/**
	 * Info utente.
	 */
	protected UtenteDTO utente;

	/**
	 * Compoenent indice di classificazione.
	 */
	protected AbstractIndiceDiClassificazioneComponent indiceDiClassificazione;

	/**
	 * Lista modalità ricerca fascicolo.
	 */
	private final List<RicercaGenericaTypeEnum> comboRicercaFascicoloModalita;

	/**
	 * Ricerca fascicolo key.
	 */
	private String ricercaFascicoloKey;

	/**
	 * Ricerca fascicolo anno.
	 */
	private Integer ricercaFascicoloAnno;

	/**
	 * Modalità ricerca fascicolo.
	 */
	private RicercaGenericaTypeEnum ricercaFascicoloModalita;

	/**
	 * Lista risultati ricerca fascicolo.
	 */
	private List<FascicoloDTO> ricercaFascicoliResults;

	/**
	 * Risultato ricerca filtrato.
	 */
	private List<FascicoloDTO> ricercaFascicoliResultsFiltered;

	/**
	 * Fascicoli titolario.
	 */
	private transient FascicoliPerTitolario fascicoliPerTitolario;

	/**
	 * Nome del bean chiamante.
	 */
	private final String mBeanChiamante;

	/**
	 * Servizio di ricerca.
	 */
	private final IRicercaFacadeSRV ricercaSRV;

	/**
	 * Nome del component.
	 */
	private String fullComponentId;

	/**
	 * variabile per ricerca fascicoli da documenti component.
	 */
	private RicercaFascicoloDaDocumentoComponent tabRicercaDoc;

	/**
	 * Costruttore del component.
	 * 
	 * @param mBeanChiamante
	 * @param utente
	 */
	public CercaFascicoloComponent(final String mBeanChiamante, final UtenteDTO utente) {
		this.mBeanChiamante = mBeanChiamante;
		this.utente = utente;
		this.ricercaFascicoloAnno = LocalDate.now().getYear();
		this.comboRicercaFascicoloModalita = Arrays.asList(RicercaGenericaTypeEnum.values());
		this.fascicoliPerTitolario = new FascicoliPerTitolario(utente);
		this.indiceDiClassificazione = new IndiceDiClassificazioneGenericComponent(mBeanChiamante, utente);
		this.ricercaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaFacadeSRV.class);
		tabRicercaDoc = new RicercaFascicoloDaDocumentoComponent(utente, mBeanChiamante);
	}

	/**
	 * Esegue la creazione del fascicolo.
	 */
	@Override
	public void cercaFascicolo() {
		try {
			final Collection<FascicoloDTO> fasColl = ricercaSRV.ricercaGenericaFascicoli(ricercaFascicoloKey, ricercaFascicoloAnno, ricercaFascicoloModalita,
					RicercaPerBusinessEntityEnum.FASCICOLI, utente);
			if (fasColl != null && !fasColl.isEmpty()) {
				ricercaFascicoliResults = new ArrayList<>(fasColl);
				ricercaFascicoliResultsFiltered = new ArrayList<>(fasColl);
			}

			resetDataTable("idTableRicercaFascicolo");
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage("Errore durante l'operazione: " + e.getMessage());
		}
	}

	/**
	 * Esegue la selezione del fascicolo identificato dall'index passato come
	 * parametro. Se l'index non risulta valido, restituisce un errore.
	 * 
	 * @param index
	 */
	@Override
	public void selectFascicolo(final Integer index) {
		try {
			if (index >= ricercaFascicoliResultsFiltered.size()) {
				throw new RedException("Indice di riga non valido.");
			}

			final FascicoloDTO fascicoloProcedimentale = ricercaFascicoliResultsFiltered.get(index);

			aggiornaChiamante(fascicoloProcedimentale);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, GENERIC_ERROR_OPERATION_MSG + e.getMessage());
		}
	}

	/**
	 * Gestisce la selezione del fascicolo del titolario.
	 * 
	 * @param event
	 */
	@Override
	public void selectFascicoloDelTitolario(final SelectEvent event) {
		try {
			final MasterFascicoloDTO fascicoloSelezionato = (MasterFascicoloDTO) event.getObject();
			final IFascicoloSRV fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloSRV.class);
			final FascicoloDTO fProcedimentale = fascicoloSRV.getFascicoloByNomeFascicolo(fascicoloSelezionato.getId().toString(), utente);

			aggiornaChiamante(fProcedimentale);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, GENERIC_ERROR_OPERATION_MSG + e.getMessage());
		}
	}

	/**
	 * Il metodo seleziona il fascicolo indicato nella scheda Ricerca per Documenti.
	 * 
	 * @param event
	 */
	@Override
	public void selectFascicoloDelDocumento(final NodeSelectEvent event) {
		try {
			final DocumentiEFascicoliDTO fascicoloSelezionato = (DocumentiEFascicoliDTO) event.getTreeNode().getData();
			final FascicoloDTO fascicolo = fascicoloSelezionato.getFascicoloOriginale();
			aggiornaChiamante(fascicolo);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, GENERIC_ERROR_OPERATION_MSG + e.getMessage());
		}
	}

	/**
	 * Si occupa di aggiornare il Managed Bean che ha aperto la ricerca del
	 * fascicolo.
	 * 
	 * @param fascicoloProcedimentale
	 */
	private void aggiornaChiamante(final FascicoloDTO fascicoloProcedimentale) {
		resetRicerca();

		final IUpdatableDetailCaller<FascicoloDTO> beanChiamante = FacesHelper.getManagedBean(mBeanChiamante);
		if (beanChiamante != null) {
			beanChiamante.updateDetail(fascicoloProcedimentale);
		}
	}

	/**
	 * Resetta i campi della riceca del fascicolo.
	 */
	@Override
	public void resetRicerca() {
		fascicoliPerTitolario = new FascicoliPerTitolario(utente);
		indiceDiClassificazione = new IndiceDiClassificazioneGenericComponent(mBeanChiamante, utente);
		ricercaFascicoloKey = Constants.EMPTY_STRING;
		ricercaFascicoloAnno = LocalDate.now().getYear();
		ricercaFascicoliResults = new ArrayList<>();
		ricercaFascicoliResultsFiltered = new ArrayList<>();
		resetDataTable("idTableRicercaFascicolo");
		resetDataTable("idTableFascicoliDelTitolario");
		final TabView tv = (TabView) FacesContext.getCurrentInstance().getViewRoot()
				.findComponent("idDettagliEstesiForm:idTabs_DMD:dlgCercaFascicoloCmp_DMD:idCercaFascicoloTabs");
		tv.setActiveIndex(0);
	}

	/**
	 * Esegue il reset del datatable identificato dall'id.
	 * 
	 * @param dataTableId
	 */
	private void resetDataTable(final String dataTableId) {
		final DataTable dt = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(fullComponentId + ":idCercaFascicoloTabs:" + dataTableId);
		if (dt != null) {
			dt.reset();
		}
	}

	/**
	 * @return the ricercaFascicoloKey
	 */
	public String getRicercaFascicoloKey() {
		return ricercaFascicoloKey;
	}

	/**
	 * @return the ricercaFascicoloAnno
	 */
	public Integer getRicercaFascicoloAnno() {
		return ricercaFascicoloAnno;
	}

	/**
	 * @param ricercaFascicoloKey the ricercaFascicoloKey to set
	 */
	public void setRicercaFascicoloKey(final String ricercaFascicoloKey) {
		this.ricercaFascicoloKey = ricercaFascicoloKey;
	}

	/**
	 * @param ricercaFascicoloAnno the ricercaFascicoloAnno to set
	 */
	public void setRicercaFascicoloAnno(final Integer ricercaFascicoloAnno) {
		this.ricercaFascicoloAnno = ricercaFascicoloAnno;
	}

	/**
	 * @param ricercaFascicoloModalita the ricercaFascicoloModalita to set
	 */
	public void setRicercaFascicoloModalita(final RicercaGenericaTypeEnum ricercaFascicoloModalita) {
		this.ricercaFascicoloModalita = ricercaFascicoloModalita;
	}

	/**
	 * @return the ricercaFascicoloModalita
	 */
	public RicercaGenericaTypeEnum getRicercaFascicoloModalita() {
		return ricercaFascicoloModalita;
	}

	/**
	 * @return the ricercaFascicoliResults
	 */
	public List<FascicoloDTO> getRicercaFascicoliResults() {
		return ricercaFascicoliResults;
	}

	/**
	 * @return the ricercaFascicoliResultsFiltered
	 */
	public List<FascicoloDTO> getRicercaFascicoliResultsFiltered() {
		return ricercaFascicoliResultsFiltered;
	}

	/**
	 * @param ricercaFascicoliResultsFiltered the ricercaFascicoliResultsFiltered to
	 *                                        set
	 */
	public void setRicercaFascicoliResultsFiltered(final List<FascicoloDTO> ricercaFascicoliResultsFiltered) {
		this.ricercaFascicoliResultsFiltered = ricercaFascicoliResultsFiltered;
	}

	/**
	 * @return the fascicoliPerTitolario
	 */
	public FascicoliPerTitolario getFascicoliPerTitolario() {
		return fascicoliPerTitolario;
	}

	/**
	 * @return the comboRicercaFascicoloModalita
	 */
	public List<RicercaGenericaTypeEnum> getComboRicercaFascicoloModalita() {
		return comboRicercaFascicoloModalita;
	}

	/**
	 * @return the indiceDiClassificazione
	 */
	public AbstractIndiceDiClassificazioneComponent getIndiceDiClassificazione() {
		return indiceDiClassificazione;
	}

	/**
	 * @param indiceDiClassificazione the indiceDiClassificazione to set
	 */
	public void setIndiceDiClassificazione(final AbstractIndiceDiClassificazioneComponent indiceDiClassificazione) {
		this.indiceDiClassificazione = indiceDiClassificazione;
	}

	/**
	 * @return the fullComponentId
	 */
	public String getFullComponentId() {
		return fullComponentId;
	}

	/**
	 * @param fullComponentId the fullComponentId to set
	 */
	public void setFullComponentId(final String fullComponentId) {
		this.fullComponentId = fullComponentId;
	}

	/**
	 * Gestisce la selezione del fascicolo.
	 * @param event evento di selezione
	 */
	@Override
	public void selectFascicolo(final SelectEvent event) {
		try {
			final FascicoloDTO fascicoloProcedimentale = (FascicoloDTO) event.getObject();
			aggiornaChiamante(fascicoloProcedimentale);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, GENERIC_ERROR_OPERATION_MSG + e.getMessage());
		}
	}

	/**
	 * Restituisce il component del tab di ricerca.
	 * 
	 * @return
	 */
	public RicercaFascicoloDaDocumentoComponent getTabRicercaDoc() {
		return tabRicercaDoc;
	}

	/**
	 * Imposta il component del tab di ricerca.
	 * 
	 * @param ricercaTab
	 */
	public void setTabRicercaDoc(final RicercaFascicoloDaDocumentoComponent ricercaTab) {
		this.tabRicercaDoc = ricercaTab;
	}
}
