package it.ibm.red.web.document.mbean.interfaces;

import java.io.Serializable;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.web.document.mbean.component.IndiceDiClassificazioneComponent;
import it.ibm.red.web.document.mbean.component.NomeFascicoloBean;
import it.ibm.red.web.document.mbean.component.UploadDocumentoBean;

/**
 * Interfaccia tab generale DSR.
 */
public interface IDsrTabGenerale extends Serializable {

	/*********ORGANIGRAMMA PER CONOSCENZA **********************/

	/**
	 * Recupera la descrizione dell'assegnatario per competenza.
	 * @return descrizione
	 */
	String getDescrAssegnatarioPerCompetenza();

	/*********END ORGANIGRAMMA PER CONOSCENZA **********************/

	/**
	 * Recupera il dettaglio del documento.
	 * @return dettaglio del documento
	 */
	DetailDocumentRedDTO getDetail();

	/**
	 * Recupera l'indice di classificazione.
	 * @return indice di classificazione
	 */
	IndiceDiClassificazioneComponent getIndiceDiClassificazione();

	/**
	 * Recupera il nome del fascicolo.
	 * @return nome del fascicolo
	 */
	NomeFascicoloBean getNomefascicolo();

	/**
	 * Recupera l'upload del documento.
	 * @return upload del documento
	 */
	UploadDocumentoBean getUploadDocumento();

}