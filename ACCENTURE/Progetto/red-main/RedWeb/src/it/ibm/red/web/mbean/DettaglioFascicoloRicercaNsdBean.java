package it.ibm.red.web.mbean;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.DetailFascicoloPregressoDTO;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.mbean.interfaces.IUpdatableDetailCaller;

/**
 * @author VINGENITO
 * 
 */
@Named(ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_NSD_BEAN)
@ViewScoped  
public class DettaglioFascicoloRicercaNsdBean extends DettaglioFascicoloNsdAbstractBean implements IUpdatableDetailCaller<DetailFascicoloPregressoDTO> {
 
	  
	private static final long serialVersionUID = -9128487495106470207L;
	
	/**
	 * Chiamante.
	 */
	private IUpdatableDetailCaller<DetailFascicoloPregressoDTO> caller; 

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		super.postConstruct();
	}
	 
	/**
	 * Setting del caller da impostare quando si imposta il dettaglio della ricerca .
	 * 
	 * @param inCaller
	 */
	public void setCaller(final IUpdatableDetailCaller<DetailFascicoloPregressoDTO> inCaller) {
		this.caller = inCaller;
	}
	 
	/**
	 * Aggiorna il dettaglio.
	 * 
	 * @param detail
	 *            dettaglio da aggiornare.
	 */
	@Override
	public void updateDetail(final DetailFascicoloPregressoDTO detail) {
		this.fascicoloCmp.setDetail(detail);
		caller.updateDetail(detail);
	}

}
