/**
 * 
 */
package it.ibm.red.web.mbean;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.PrimeFaces;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.DelegatoDTO;
import it.ibm.red.business.dto.EsitoDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.facade.IAooFacadeSRV;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * @author APerquoti
 *
 */
@Named(ConstantsWeb.MBean.DELEGA_LIBRO_FIRMA_BEAN)
@ViewScoped
public class DelegaLibroFirmaBean extends AbstractBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -4642088279374454902L;

	/**
	 * Messaggio errore assegnazione ruolo a Delegato Libro Firma.
	 */
	private static final String ERROR_ASSEGNAZIONE_DELEGATO_AL_LIBRO_FIRMA_MSG = "Errore durante l'assegnazione del ruolo a Delegato al Libro Firma";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DelegaLibroFirmaBean.class.getName());

	/**
	 * Servizio utente.
	 */
	private IUtenteSRV utenteSRV;

	/**
	 * Servizio organigramma.
	 */
	private IOrganigrammaFacadeSRV orgSRV;

	/**
	 * Servizio aoo.
	 */
	private IAooFacadeSRV aooSRV;

	/**
	 * Flag bottone delegato.
	 */
	private boolean disableBtnDelegato;

	/**
	 * Flag bottone delegante.
	 */
	private boolean disableBtnDelegante;

	/**
	 * Root trre delegante.
	 */
	private transient TreeNode rootDelegante;

	/**
	 * Root tree delegato.
	 */
	private transient TreeNode rootDelegato;

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Delegante selezionato.
	 */
	private NodoOrganigrammaDTO deleganteSelezionato;

	/**
	 * Delegato selezionato.
	 */
	private NodoOrganigrammaDTO delegatoSelezionato;

	/**
	 * Data.
	 */
	private String tomorrow;

	/**
	 * Data disattivazione minima.
	 */
	private String minDateDisattivazione;

	/**
	 * Motivo delega.
	 */
	private String motivoDelega;

	/**
	 * Data disattivazione.
	 */
	private Date dataDisattivazione;

	/**
	 * Data attivazione.
	 */
	private Date dataAttivazione;

	/**
	 * Data table deleghe.
	 */
	protected SimpleDetailDataTableHelper<DelegatoDTO> delegheDTH;

	/**
	 * Flag seleziona tutti.
	 */
	private Boolean allElementCheckbox;

	/**
	 * Disabilita delega.
	 */
	private Boolean disableBtnAssegnaDelega;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {

		orgSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
		utenteSRV = ApplicationContextProvider.getApplicationContext().getBean(IUtenteSRV.class);
		aooSRV = ApplicationContextProvider.getApplicationContext().getBean(IAooFacadeSRV.class);

		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();

		disableBtnDelegato = Boolean.TRUE;
		dataDisattivazione = null;
		dataAttivazione = null;
		disableBtnAssegnaDelega = Boolean.FALSE;

		// Verifica della configurazione per il Ruolo di Dlegato al Libro Firma.
		checkRuoloDelegatoLiboFirma();

		delegheDTH = new SimpleDetailDataTableHelper<>();
		refresh();

		pulisciDelegato();

		loadTreeDelegante();

	}

	/**
	 * Effettua un refresh sulla lista dei delegati libro firma, rieseguendo la
	 * query per il recupero della stessa e aggiornando il datatable.
	 */
	public void refresh() {

		try {
			Collection<DelegatoDTO> masters = utenteSRV.getDelegatiLibroFirma(utente.getIdUfficio(), utente.getIdAoo(), Boolean.FALSE);
			delegheDTH.setMasters(masters);

			allElementCheckbox = false;

		} catch (final Exception e) {
			LOGGER.error("Errore durante il caricamento delle Deleghe assegnate", e);
			showError("Errore durante il caricamento delle Deleghe assegnate", e);
		}

	}

	/**
	 * Verifica che il Ruolo di Delegato a Lìbro firma sia configurato
	 * correttamente. Se non lo è, inibisce la possibilità di "Assegna delega" e
	 * mostra un messaggio di errore.
	 */
	public void checkRuoloDelegatoLiboFirma() {

		try {

			final EsitoDTO esito = aooSRV.checkRuoloDelegatoLiboFirma(utente.getIdAoo());

			if (!esito.isEsito()) {
				FacesHelper.executeJS("PF('stickyGrowl').renderMessage({'summary':'Configurazione errata', 'detail':'" + esito.getNote() + "', 'severity':'error'})");
				disableBtnAssegnaDelega = Boolean.TRUE;
				FacesHelper.update("eastSectionForm:idBtnInsertDelega");
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il controllo della configurazione del Ruolo assegnato come Delegato al Libro Firma.", e);
			showError("Errore durante il controllo della configurazione del Ruolo assegnato come Delegato al Libro Firma.", e);
		}

	}

//	<-- Gestione Tree Delegante-->	

	private void loadTreeDelegante() {

		try {

			// creazione della struttura del tree
			rootDelegante = new DefaultTreeNode("Root", null);
			rootDelegante.setSelectable(true);

			// assegnazione del nome del nodo di primo livello
			final NodoOrganigrammaDTO padre = new NodoOrganigrammaDTO();
			padre.setDescrizioneNodo(utente.getNodoDesc());
			padre.setIdAOO(utente.getIdAoo());
			padre.setIdNodo(utente.getIdUfficio());
			padre.setUtentiVisible(true);
			padre.setCodiceAOO(utente.getCodiceAoo());

			// creazione dell'unico nodo necessario (ufficio dell'utente in sessione)
			final TreeNode nodoPadre = new DefaultTreeNode(padre, rootDelegante);
			nodoPadre.setExpanded(true);
			nodoPadre.setSelectable(false);
			nodoPadre.setType("ROOT");

//			<-- Utente Dirigente ufficio attuale -->

			final NodoOrganigrammaDTO utenteDirigente = orgSRV.getDirigenteFromNodo(padre);

			DefaultTreeNode nodoToAdd = null;
			nodoToAdd = new DefaultTreeNode(utenteDirigente, nodoPadre);
			nodoToAdd.setSelectable(true);
			nodoToAdd.setType("USER");
			nodoPadre.getChildren().add(nodoToAdd);

//			<-- Primo livello -->
			final NodoOrganigrammaDTO firstLevel = new NodoOrganigrammaDTO();
			firstLevel.setIdAOO(utente.getIdAoo());
			firstLevel.setIdNodo(utente.getIdUfficio());
			firstLevel.setCodiceAOO(utente.getCodiceAoo());
			firstLevel.setUtentiVisible(false);

			List<NodoOrganigrammaDTO> listFirstLevel = null;
			listFirstLevel = orgSRV.getTreeSelezioneDelegante(firstLevel);

			TreeNode nodoFirstLevel = null;
			for (final NodoOrganigrammaDTO lfl : listFirstLevel) {

				nodoFirstLevel = new DefaultTreeNode(lfl, nodoPadre);
				nodoFirstLevel.setSelectable(false);
				nodoFirstLevel.setType("UFF");

				// se risultano dei figli per questoo nodo imposto un nodo fittizio per dare la
				// possibilità in view di essere aperto
				new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoFirstLevel);
			}
//			<-- Fine Primo livello -->

		} catch (final Exception e) {
			LOGGER.error("Errore durante il caricamento del tree per la selezione del DELEGANTE", e);
			showError("Errore durante il caricamento del tree per la selezione del DELEGANTE", e);
		}
	}

	/**
	 * Definisce il comportamento del tree dei deleganti in fase di espansione del
	 * nodo.
	 * 
	 * @param event
	 */
	public void onDeleganteNodeExpand(final NodeExpandEvent event) {

		try {

			final TreeNode nodoPadre = event.getTreeNode();

			// agiungo le code ufficio e gli utenti disponibili

			nodoPadre.getChildren().clear();
			final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) nodoPadre.getData();

//			<-- Lista Utenti Dirigenti -->
			// Recupero degli utenti Dirigenti di questo ufficio.

			final NodoOrganigrammaDTO utenteDirigente = orgSRV.getDirigenteFromNodo(nodoExp);
			DefaultTreeNode nodoUtenteToAdd = null;

			nodoUtenteToAdd = new DefaultTreeNode(utenteDirigente, nodoPadre);
			nodoUtenteToAdd.setSelectable(true);
			nodoUtenteToAdd.setType("USER");
			nodoPadre.getChildren().add(nodoUtenteToAdd);
//			<-- Fine Lista Utenti Dirigenti -->

//			<-- Uffici con Dirigente configurato -->
			final List<NodoOrganigrammaDTO> figli = orgSRV.getTreeSelezioneDelegante(nodoExp);
			if (!figli.isEmpty()) {

				DefaultTreeNode nodoUfficioToAdd = null;
				for (final NodoOrganigrammaDTO n : figli) {

					nodoUfficioToAdd = new DefaultTreeNode(n, nodoPadre);
					nodoUfficioToAdd.setType("UFF");
					nodoUfficioToAdd.setSelectable(false);
					nodoUfficioToAdd.setSelected(false);

					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoUfficioToAdd);
				}
			}
//			<-- Fine Uffici con Dirigente configurato-->

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del Dirigente di questo Ufficio e di eventuali sotto uffici", e);
			showError("Errore durante il recupero del Dirigente di questo Ufficio e di eventuali sotto uffici", e);
		}

	}

	/**
	 * Gestisce le azioni da eseguire in fase di selezione di un nodo del tree dei
	 * deleganti.
	 * 
	 * @param event
	 */
	public void onDeleganteNodeSelection(final NodeSelectEvent event) {

		try {

			// Annullamento del delegato selezionato in precedenza.
			delegatoSelezionato = null;

			final TreeNode nodeSelected = event.getTreeNode();

			// Costruzione dell'albero contenente gli utenti dello stesso ufficio del
			// Delegante.
			loadTreeDelegato(nodeSelected);

		} catch (final Exception e) {
			LOGGER.error("Errore durante il caricamento della lista di tenti selezionabili come Delegato", e);
			showError("Errore durante il caricamento della lista di tenti selezionabili come Delegato", e);
		}

	}

//	<-- Fine Gestione Tree Delegante -->

//	<-- Gestione Tree Delegato-->	

	private void loadTreeDelegato(final TreeNode nodeSelected) {

		deleganteSelezionato = (NodoOrganigrammaDTO) nodeSelected.getData();
		final NodoOrganigrammaDTO padre = (NodoOrganigrammaDTO) nodeSelected.getParent().getData();

		// creazione della struttura del tree
		rootDelegato = new DefaultTreeNode("Root", null);
		rootDelegato.setSelectable(true);

		// creazione dell'unico nodo necessario (ufficio dell'utente in sessione)
		final TreeNode nodoPadre = new DefaultTreeNode(padre, rootDelegato);
		nodoPadre.setExpanded(true);
		nodoPadre.setSelectable(false);
		nodoPadre.setType("ROOT");

//		<-- Lista Utenti senza Dirigente -->
		final List<NodoOrganigrammaDTO> listUtentiUfficio = orgSRV.getUtentiFromNodo(padre, false);
		TreeNode nodoFirstLevel = null;
		for (final NodoOrganigrammaDTO luf : listUtentiUfficio) {

			nodoFirstLevel = new DefaultTreeNode(luf, nodoPadre);
			nodoFirstLevel.setSelectable(true);
			nodoFirstLevel.setType("USER");
			nodoPadre.getChildren().add(nodoFirstLevel);
		}
//		<-- Fine Lista Utenti senza Dirigente -->

	}

	/**
	 * Gestisce la selezione del delegato.
	 * 
	 * @param event
	 */
	public void onDelegatoSelection(final NodeSelectEvent event) {
		disableBtnAssegnaDelega = Boolean.FALSE;

		try {

			final TreeNode nodeSelected = event.getTreeNode();
			delegatoSelezionato = (NodoOrganigrammaDTO) nodeSelected.getData();

			final Boolean hasPermessoCorriere = utenteSRV.checkRuoliHasPermesso(delegatoSelezionato.getIdNodo(), delegatoSelezionato.getIdUtente(),
					delegatoSelezionato.getIdAOO(), PermessiEnum.CODA_CORRIERE);

			if (Boolean.FALSE.equals(hasPermessoCorriere)) {

				disableBtnAssegnaDelega = Boolean.TRUE;

				FacesContext.getCurrentInstance().addMessage(ConstantsWeb.ASSEGNAZIONE_DELEGHE_MESSAGES_CLIENT_ID,
						new FacesMessage(FacesMessage.SEVERITY_WARN, "Attenzione!", "L'utente selezionato necessita di ulteriori configurazioni. Contattare l'assistenza."));
			} else {
				PrimeFaces.current().resetInputs("eastSectionForm:idPnlMessageAssDeleghe");
			}

			FacesHelper.update("eastSectionForm:idBtnInsertDelega");

		} catch (final Exception e) {
			LOGGER.error("Errore durante la verifica di configurazione dell'utente selezionato come Delegato", e);
			showError("Errore durante la verifica di configurazione dell'utente selezionato come Delegato", e);
		}

	}

//	<-- Fine Gestione Tree Delegato -->

	/**
	 * Esegue l'assegnazione del ruolo come delegato all'utente selezionato.
	 * Gestisce eventuali errori comunicandoli all'utente.
	 */
	public void assegnaRuoloDelegato() {
		Boolean result = Boolean.FALSE;

		try {

			if ((delegatoSelezionato == null || delegatoSelezionato.getIdNodo() == null || delegatoSelezionato.getIdUtente() == null) || dataDisattivazione == null
					|| dataAttivazione == null) {
				showWarnMessage("Attenzione inserire valori adeguati per poter assegnare la delega!");
			}

			result = utenteSRV.addRuoloDelegato(delegatoSelezionato.getIdNodo(), delegatoSelezionato.getIdUtente(), utente.getIdAoo(), dataAttivazione, dataDisattivazione,
					motivoDelega);

			if (Boolean.TRUE.equals(result)) {
				refresh();
				showInfoMessage("Assegnazione avvenuta con successo");
			} else {
				showError(ERROR_ASSEGNAZIONE_DELEGATO_AL_LIBRO_FIRMA_MSG);
			}

		} catch (final Exception e) {
			LOGGER.error(ERROR_ASSEGNAZIONE_DELEGATO_AL_LIBRO_FIRMA_MSG, e);
			showError(ERROR_ASSEGNAZIONE_DELEGATO_AL_LIBRO_FIRMA_MSG, e);
		}

	}

	/**
	 * Gestisce l'evento di selezione di una riga del datatable.
	 * 
	 * @param se
	 */
	public final void rowSelector(final SelectEvent se) {
		delegheDTH.rowSelector(se);
		fillDetail(delegheDTH.getCurrentMaster());
		disableBtnDelegante = Boolean.TRUE;
	}

	/**
	 * Popola i parametri relativi all'assegnazione di delega recuperando le
	 * informazioni dal DTO in ingresso.
	 * 
	 * @param master
	 */
	private void fillDetail(final DelegatoDTO master) {

		// Fill Delegante
		deleganteSelezionato = new NodoOrganigrammaDTO();
		deleganteSelezionato.setIdNodo(master.getIdNodo());
		deleganteSelezionato.setDescrizioneNodo(master.getDescrizioneNodo());

		// Fill Delegato
		delegatoSelezionato = new NodoOrganigrammaDTO();
		delegatoSelezionato.setIdNodo(master.getIdNodo());
		delegatoSelezionato.setDescrizioneNodo(master.getDescrizioneNodo());
		delegatoSelezionato.setIdUtente(master.getIdUtente());
		delegatoSelezionato.setNomeUtente(master.getNomeUtente());
		delegatoSelezionato.setCognomeUtente(master.getCognomeUtente());

		// Fill date
		dataDisattivazione = master.getDataDisattivazione();
		dataAttivazione = master.getDataAttivazione();

		// Fill motivo Delega
		motivoDelega = master.getMotivoDelega();
	}

	/**
	 * Metodo per gestire la pressione del pulsante per selezionare/deselezionare
	 * tutte le checkbox.
	 * 
	 * @param event evento generato alla pressione del pulsante di selezione
	 *              multipla
	 */
	public final void handleAllCheckBox() {

		if (allElementCheckbox == null || !allElementCheckbox) {
			allElementCheckbox = true;
		} else {
			allElementCheckbox = false;
		}
		delegheDTH.getSelectedMasters().clear();
		for (final DelegatoDTO m : delegheDTH.getMasters()) {
			m.setSelected(allElementCheckbox);
			if (Boolean.TRUE.equals(allElementCheckbox)) {
				delegheDTH.getSelectedMasters().add(m);
			}
		}

	}

	/**
	 * Effettua un update sul check del master selezionato.
	 * 
	 * @param event
	 */
	public final void updateCheck(final AjaxBehaviorEvent event) {

		final DelegatoDTO masterChecked = getDataTableClickedRow(event);
		final Boolean newValue = ((SelectBooleanCheckbox) event.getSource()).isSelected();
		if (newValue != null && newValue) {
			delegheDTH.getSelectedMasters().add(masterChecked);
		} else {
			delegheDTH.getSelectedMasters().remove(masterChecked);
		}

	}

	/**
	 * Gestisce la rimozione dell'assegnazione delegato libro firma comunicandone
	 * l'esito.
	 */
	public void removeAssegnazioni() {
		Boolean result = Boolean.FALSE;

		try {

			result = utenteSRV.removeRuoliDelegato(delegheDTH.getSelectedMasters(), utente.getIdAoo());

			if (Boolean.TRUE.equals(result)) {
				refresh();
				showInfoMessage("Assegnazioni Delegato Libro Firma rimosse con successo");
			} else {
				showError("Errore durante la rimozione delle assegnazione del ruolo a Delegato al Libro Firma");
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante la rimozione delle assegnazioni", e);
			showError("Errore durante la rimozione delle assegnazioni", e);
		}

	}

	/**
	 * Effettua un clear dei campi del Delegato.
	 */
	public void pulisciDelegato() {

		// pulizia del form di inserimento delega
		deleganteSelezionato = null;
		delegatoSelezionato = null;
		dataDisattivazione = null;
		dataAttivazione = null;
		motivoDelega = null;

		final Date tomorrowDate = DateUtils.addDay(new Date(), 1);
		minDateDisattivazione = new SimpleDateFormat(DateUtils.DD_MM_YYYY).format(tomorrowDate);

		disableBtnDelegante = Boolean.FALSE;
		delegheDTH.setCurrentMaster(null);

		FacesHelper.update("eastSectionForm");
	}

	/**
	 * Gestisce la data di disattivazione della delega alla modifica del campo
	 * "Delega da".
	 */
	public void onChangeDelegaDa() {
		dataDisattivazione = DateUtils.addDay(dataAttivazione, 1);
		minDateDisattivazione = new SimpleDateFormat(DateUtils.DD_MM_YYYY).format(dataDisattivazione);
	}

//	<-- Getter/Setter -->

	/**
	 * @return the disableBtnDelegato
	 */
	public boolean isDisableBtnDelegato() {
		return disableBtnDelegato;
	}

	/**
	 * @param disableBtnDelegato the disableBtnDelegato to set
	 */
	public void setDisableBtnDelegato(final boolean disableBtnDelegato) {
		this.disableBtnDelegato = disableBtnDelegato;
	}

	/**
	 * @return the rootDelegante
	 */
	public TreeNode getRootDelegante() {
		return rootDelegante;
	}

	/**
	 * @param rootDelegante the rootDelegante to set
	 */
	public void setRootDelegante(final TreeNode rootDelegante) {
		this.rootDelegante = rootDelegante;
	}

	/**
	 * @return the deleganteSelezionato
	 */
	public NodoOrganigrammaDTO getDeleganteSelezionato() {
		return deleganteSelezionato;
	}

	/**
	 * @param deleganteSelezionato the deleganteSelezionato to set
	 */
	public void setDeleganteSelezionato(final NodoOrganigrammaDTO deleganteSelezionato) {
		this.deleganteSelezionato = deleganteSelezionato;
	}

	/**
	 * @return the tomorrow
	 */
	public String getTomorrow() {
		return tomorrow;
	}

	/**
	 * @param tomorrow the tomorrow to set
	 */
	public void setTomorrow(final String tomorrow) {
		this.tomorrow = tomorrow;
	}

	/**
	 * @return the rootDelegato
	 */
	public TreeNode getRootDelegato() {
		return rootDelegato;
	}

	/**
	 * @param rootDelegato the rootDelegato to set
	 */
	public void setRootDelegato(final TreeNode rootDelegato) {
		this.rootDelegato = rootDelegato;
	}

	/**
	 * @return the delegatoSelezionato
	 */
	public NodoOrganigrammaDTO getDelegatoSelezionato() {
		return delegatoSelezionato;
	}

	/**
	 * @param delegatoSelezionato the delegatoSelezionato to set
	 */
	public void setDelegatoSelezionato(final NodoOrganigrammaDTO delegatoSelezionato) {
		this.delegatoSelezionato = delegatoSelezionato;
	}

	/**
	 * @return the dataDisattivazione
	 */
	public Date getDataDisattivazione() {
		return dataDisattivazione;
	}

	/**
	 * @param dataDisattivazione the dataDisattivazione to set
	 */
	public void setDataDisattivazione(final Date dataDisattivazione) {
		this.dataDisattivazione = dataDisattivazione;
	}

	/**
	 * @return the delegheDTH
	 */
	public SimpleDetailDataTableHelper<DelegatoDTO> getDelegheDTH() {
		return delegheDTH;
	}

	/**
	 * @param delegheDTH the delegheDTH to set
	 */
	public void setDelegheDTH(final SimpleDetailDataTableHelper<DelegatoDTO> delegheDTH) {
		this.delegheDTH = delegheDTH;
	}

	/**
	 * @return the allElementCheckbox
	 */
	public Boolean getAllElementCheckbox() {
		return allElementCheckbox;
	}

	/**
	 * @param allElementCheckbox the allElementCheckbox to set
	 */
	public void setAllElementCheckbox(final Boolean allElementCheckbox) {
		this.allElementCheckbox = allElementCheckbox;
	}

	/**
	 * @return the disableBtnAssegnaDelega
	 */
	public Boolean getDisableBtnAssegnaDelega() {
		return disableBtnAssegnaDelega;
	}

	/**
	 * @param disableBtnAssegnaDelega the disableBtnAssegnaDelega to set
	 */
	public void setDisableBtnAssegnaDelega(final Boolean disableBtnAssegnaDelega) {
		this.disableBtnAssegnaDelega = disableBtnAssegnaDelega;
	}

	/**
	 * @return the disableBtnDelegante
	 */
	public boolean isDisableBtnDelegante() {
		return disableBtnDelegante;
	}

	/**
	 * @param disableBtnDelegante the disableBtnDelegante to set
	 */
	public void setDisableBtnDelegante(final boolean disableBtnDelegante) {
		this.disableBtnDelegante = disableBtnDelegante;
	}

	/**
	 * @return the motivoDelega
	 */
	public String getMotivoDelega() {
		return motivoDelega;
	}

	/**
	 * @param motivoDelega the motivoDelega to set
	 */
	public void setMotivoDelega(final String motivoDelega) {
		this.motivoDelega = motivoDelega;
	}

	/**
	 * @return the dataAttivazione
	 */
	public Date getDataAttivazione() {
		return dataAttivazione;
	}

	/**
	 * @param dataAttivazione the dataAttivazione to set
	 */
	public void setDataAttivazione(final Date dataAttivazione) {
		this.dataAttivazione = dataAttivazione;
	}

	/**
	 * @return the minDateDisattivazione
	 */
	public String getMinDateDisattivazione() {
		return minDateDisattivazione;
	}

	/**
	 * @param minDateDisattivazione the minDateDisattivazione to set
	 */
	public void setMinDateDisattivazione(final String minDateDisattivazione) {
		this.minDateDisattivazione = minDateDisattivazione;
	}

}
