package it.ibm.red.web.mbean.widgets;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.chart.DonutChartModel;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.AccessoFunzionalitaEnum;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Ruolo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IAssegnaUfficioFacadeSRV;
import it.ibm.red.business.service.facade.ICounterFacadeSRV;
import it.ibm.red.business.service.facade.IInApprovazioneWidgetFacadeSRV;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.enums.NavigationTokenEnum;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;

/**
 * Bean che gestisce il widget in approvazione per dirigente.
 */
@Named(ConstantsWeb.MBean.INAPPROVAZIONEPERDIRIGENTE_WIDGET_BEAN)
@ViewScoped
public class InApprovazionePerDirigenteWidgetBean extends AbstractBean {

	/**
	 * Label FEPA.
	 */
	private static final String FEPA_LABEL = " (FEPA)";

	/**
	 * Label DELEGATO.
	 */
	private static final String DELEGATO_LABEL = " (DELEGATO)";

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -8494415926676508268L;

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * BEan di sessione.
	 */
	private SessionBean sessionBean;

	/**
	 * Lista uffici.
	 */
	private List<Nodo> listaUffici;

	/**
	 * Servizio.
	 */
	private IAssegnaUfficioFacadeSRV assegnaUfficioSRV;

	/**
	 * Nodo selezionato.
	 */
	private Nodo nodoSelezionato;

	/**
	 * Modello grafico.
	 */
	private DonutChartModel model;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		sessionBean = (SessionBean) FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		final IInApprovazioneWidgetFacadeSRV approvazioneWdgSRV = ApplicationContextProvider.getApplicationContext().getBean(IInApprovazioneWidgetFacadeSRV.class);
		assegnaUfficioSRV = ApplicationContextProvider.getApplicationContext().getBean(IAssegnaUfficioFacadeSRV.class);
		final ICounterFacadeSRV countSRV = ApplicationContextProvider.getApplicationContext().getBean(ICounterFacadeSRV.class);
		listaUffici = approvazioneWdgSRV.getNodiFromIdUtenteandIdAOO(utente, true);
		
		// Contatori uffici
		Map<String, Integer> counterUffici = countSRV.countTotSottoUfficiWidget(listaUffici, utente, true);
		final List<Nodo> listaNodiDelegato = new ArrayList<>();
		final List<Nodo> listaNodiFirmaFEPA = new ArrayList<>();

		for (final Nodo ufficio : listaUffici) {
			final String idUfficio = "" + ufficio.getIdNodo().intValue();
			ufficio.setCountUfficio(counterUffici.get(idUfficio));

			if (counterUffici.containsKey(idUfficio + "|DELEGATO")) {
				final Nodo ufficioLibroFirmaDelegato = new Nodo(ufficio);
				ufficioLibroFirmaDelegato.setUfficioDelegato(true);
				ufficioLibroFirmaDelegato.setCountUfficio(counterUffici.get(idUfficio + "|DELEGATO"));
				listaNodiDelegato.add(ufficioLibroFirmaDelegato);
			}

			if (counterUffici.containsKey(idUfficio + "|FEPA")) {
				final Nodo ufficioLibroFirmaFEPA = new Nodo(ufficio);
				ufficioLibroFirmaFEPA.setUfficioLibroFirmaFepa(true);
				ufficioLibroFirmaFEPA.setCountUfficio(counterUffici.get(idUfficio + "|FEPA"));
				listaNodiFirmaFEPA.add(ufficioLibroFirmaFEPA);
			}

		}
		listaUffici.addAll(listaNodiDelegato);
		listaUffici.addAll(listaNodiFirmaFEPA);

		model = new DonutChartModel();

		createDonutModel();
	}

	/**
	 * Crea il model.
	 */
	public void createDonutModel() {
		final Map<String, Number> ufficiDonut = new LinkedHashMap<>();
		final String colors = "FF0000,5FFB17,736AFF,46C7C7,8E35EF,000000,2B60DE,F6358A,8E35EF,F778A1";

		final String[] colorArray = colors.split(",");
		Nodo nodoLegenda = null;
		final List<Nodo> listaUfficiNodo = new ArrayList<>();
		for (int i = 0; i < listaUffici.size(); i++) {
			listaUffici.get(i).getCountUfficio();
			nodoLegenda = new Nodo();
			nodoLegenda.setColoreUfficio("#" + colorArray[i]);
			if (listaUffici.get(i).isUfficioDelegato()) {
				nodoLegenda.setDescrizione(listaUffici.get(i).getDescrizione() + DELEGATO_LABEL);
			} else if (listaUffici.get(i).isUfficioLibroFirmaFepa()) {
				nodoLegenda.setDescrizione(listaUffici.get(i).getDescrizione() + FEPA_LABEL);
			} else {
				nodoLegenda.setDescrizione(listaUffici.get(i).getDescrizione());
			}
			listaUfficiNodo.add(nodoLegenda);
		}
		for (int i = 0; i < listaUfficiNodo.size(); i++) {
			listaUffici.get(i).setColoreUfficio(listaUfficiNodo.get(i).getColoreUfficio());
			listaUffici.get(i).setDescrizione(listaUfficiNodo.get(i).getDescrizione());
		}
		for (final Nodo ufficio : listaUffici) {
			final int valoreFake = ufficio.getCountUfficio();
			if (ufficio.isUfficioDelegato()) {
				ufficiDonut.put(ufficio.getDescrizione() + DELEGATO_LABEL, valoreFake);
			} else if (ufficio.isUfficioLibroFirmaFepa()) {
				ufficiDonut.put(ufficio.getDescrizione() + FEPA_LABEL, valoreFake);
			} else {
				ufficiDonut.put(ufficio.getDescrizione(), valoreFake);
			}
		}

		model.setSeriesColors(colors);
		model.addCircle(ufficiDonut);
		model.setLegendCols(2);
		model.setSliceMargin(2);
		model.setTitle("");
	}

	/**
	 * Metodo per il reindirizzamento al Libro Firma.
	 * 
	 * @return Libro Firma
	 */
	public String gotoLibroFirma() {
		final Long idAoo = utente.getIdAoo();
		final List<Ruolo> listaRuoli = assegnaUfficioSRV.getRuoliFromIdNodoAndIdAOO(utente.getId(), nodoSelezionato.getIdNodo(), idAoo);
		for (final Ruolo ruolo : listaRuoli) {
			final boolean libroFirmaFepa = !nodoSelezionato.isUfficioDelegato() && nodoSelezionato.isUfficioLibroFirmaFepa() && PermessiUtils.haAccessoAllaFunzionalita(
					AccessoFunzionalitaEnum.DECRETI_DIRIGENTE, ruolo.getPermessi().longValue(), nodoSelezionato.getAoo().getParametriAOO(), nodoSelezionato.getIdTipoNodo());
			final boolean libroFirmaDelegato = nodoSelezionato.isUfficioDelegato() && !nodoSelezionato.isUfficioLibroFirmaFepa() && PermessiUtils.haAccessoAllaFunzionalita(
					AccessoFunzionalitaEnum.LIBROFIRMADELEGATO, ruolo.getPermessi().longValue(), nodoSelezionato.getAoo().getParametriAOO(), nodoSelezionato.getIdTipoNodo());
			final boolean libroFirma = !nodoSelezionato.isUfficioDelegato() && !nodoSelezionato.isUfficioLibroFirmaFepa() && PermessiUtils.haAccessoAllaFunzionalita(
					AccessoFunzionalitaEnum.LIBROFIRMA, ruolo.getPermessi().longValue(), nodoSelezionato.getAoo().getParametriAOO(), nodoSelezionato.getIdTipoNodo());

			if (libroFirmaDelegato || libroFirma) {
				if (libroFirmaDelegato) {
					nodoSelezionato.setDescrizione(nodoSelezionato.getDescrizione().replace(DELEGATO_LABEL, ""));
				}
				assegnaUfficioSRV.popolaUtente(utente, nodoSelezionato, ruolo, false);
				sessionBean.caricaRicercheDisponibili();
				sessionBean.ricaricaMenuContatori();
				sessionBean.loadOrgUtente();
				sessionBean.setIdNuovoUfficio(nodoSelezionato.getIdNodo());
				sessionBean.setIdNuovoRuolo(ruolo.getIdRuolo());
			} else if (libroFirmaFepa) {
				nodoSelezionato.setDescrizione(nodoSelezionato.getDescrizione().replace(FEPA_LABEL, ""));
				assegnaUfficioSRV.popolaUtente(utente, nodoSelezionato, ruolo, false);
				sessionBean.caricaRicercheDisponibili();
				sessionBean.ricaricaMenuContatori();
				sessionBean.loadOrgUtente();
				sessionBean.setIdNuovoUfficio(nodoSelezionato.getIdNodo());
				sessionBean.setIdNuovoRuolo(ruolo.getIdRuolo());
			}
			
			if (libroFirmaDelegato || libroFirma || libroFirmaFepa) {
				break;
			}
		}

		String gotoCoda = sessionBean.gotoLibroFirma();

		if (nodoSelezionato.isUfficioLibroFirmaFepa()) {
			gotoCoda = sessionBean.gotoDecretiDaFirmare();
		} else if (nodoSelezionato.isUfficioDelegato()) {
			gotoCoda = sessionBean.gotoLibroFirmaDelegato();
		}

		return gotoCoda;
	}

	/**
	 * Recupera gli uffici.
	 * 
	 * @return lista degli uffici
	 */
	public List<Nodo> getListaUffici() {
		return listaUffici;
	}

	/**
	 * Recupera il nodo selezionato.
	 * 
	 * @return nodo selezionato
	 */
	public Nodo getNodoSelezionato() {
		return nodoSelezionato;
	}

	/**
	 * Imposta il nodo selezionato.
	 * 
	 * @param nodoSelezionato nodo selezionato
	 */
	public void setNodoSelezionato(final Nodo nodoSelezionato) {
		this.nodoSelezionato = nodoSelezionato;
	}

	/**
	 * Ottiene il model.
	 * 
	 * @return model
	 */
	public DonutChartModel getModel() {
		return model;
	}

	/**
	 * Seleziona l'item relativo all'evento.
	 * 
	 * @param event
	 */
	public void itemSelect(final ItemSelectEvent event) {
		final Long idAoo = utente.getIdAoo();
		final Integer indexDonut = event.getItemIndex();
		nodoSelezionato = listaUffici.get(indexDonut);
		final List<Ruolo> listaRuoli = assegnaUfficioSRV.getRuoliFromIdNodoAndIdAOO(utente.getId(), nodoSelezionato.getIdNodo(), idAoo);
		for (final Ruolo ruolo : listaRuoli) {
			final boolean libroFirmaFepa = !nodoSelezionato.isUfficioDelegato() && nodoSelezionato.isUfficioLibroFirmaFepa() && PermessiUtils.haAccessoAllaFunzionalita(
					AccessoFunzionalitaEnum.DECRETI_DIRIGENTE, ruolo.getPermessi().longValue(), nodoSelezionato.getAoo().getParametriAOO(), nodoSelezionato.getIdTipoNodo());
			final boolean libroFirmaDelegato = nodoSelezionato.isUfficioDelegato() && !nodoSelezionato.isUfficioLibroFirmaFepa() && PermessiUtils.haAccessoAllaFunzionalita(
					AccessoFunzionalitaEnum.LIBROFIRMADELEGATO, ruolo.getPermessi().longValue(), nodoSelezionato.getAoo().getParametriAOO(), nodoSelezionato.getIdTipoNodo());
			final boolean libroFirma = !nodoSelezionato.isUfficioDelegato() && !nodoSelezionato.isUfficioLibroFirmaFepa() && PermessiUtils.haAccessoAllaFunzionalita(
					AccessoFunzionalitaEnum.LIBROFIRMA, ruolo.getPermessi().longValue(), nodoSelezionato.getAoo().getParametriAOO(), nodoSelezionato.getIdTipoNodo());

			if (libroFirmaDelegato || libroFirma) {
				if (libroFirmaDelegato) {
					nodoSelezionato.setDescrizione(nodoSelezionato.getDescrizione().replace(DELEGATO_LABEL, ""));
				}
				assegnaUfficioSRV.popolaUtente(utente, nodoSelezionato, ruolo, false);
				sessionBean.caricaRicercheDisponibili();
				sessionBean.ricaricaMenuContatori();
				sessionBean.loadOrgUtente();
				sessionBean.setIdNuovoUfficio(nodoSelezionato.getIdNodo());
				sessionBean.setIdNuovoRuolo(ruolo.getIdRuolo());
				sessionBean.gotoLibroFirma();
				FacesHelper.navigationRule(NavigationTokenEnum.LIBROFIRMA);
			} else if (libroFirmaFepa) {
				nodoSelezionato.setDescrizione(nodoSelezionato.getDescrizione().replace(FEPA_LABEL, ""));
				assegnaUfficioSRV.popolaUtente(utente, nodoSelezionato, ruolo, false);
				sessionBean.caricaRicercheDisponibili();
				sessionBean.ricaricaMenuContatori();
				sessionBean.loadOrgUtente();
				sessionBean.setIdNuovoUfficio(nodoSelezionato.getIdNodo());
				sessionBean.setIdNuovoRuolo(ruolo.getIdRuolo());
				sessionBean.gotoDecretiDaFirmare();
				FacesHelper.navigationRule(NavigationTokenEnum.DD_DA_FIRMARE);
			}

			if (libroFirmaDelegato || libroFirma || libroFirmaFepa) {
				break;
			}
		}

	}

}
