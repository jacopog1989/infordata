/**
 * 
 */
package it.ibm.red.web.enums;

import it.ibm.red.business.enums.AccessoFunzionalitaEnum;

/**
 * @author APerquoti
 *
 */
public enum RicercheEnum {
	
	
	/**
	 * Valore.
	 */
	RED("RED", "red", null),
	
	
	/**
	 * Valore.
	 */
	FEPA("FEPA", "fepa", AccessoFunzionalitaEnum.RICERCAFEPA),
	
	
	/**
	 * Valore.
	 */
	REGISTRO_PROTOCOLLO("Registro Protocollo", "protocollo", AccessoFunzionalitaEnum.RICERCAPROTOCOLLO),
	
	
	/**
	 * Valore.
	 */
	SIGI("SIGI", "sigi", AccessoFunzionalitaEnum.RICERCASIGI);
	
	/**
	 * Label visualizzata.
	 */
	private String displayName;

	/**
	 * Label visualizzata.
	 */
	private String value;

	/**
	 * Label visualizzata.
	 */
	private AccessoFunzionalitaEnum accesso;
	
	/**
	 * Costruttore.
	 * 
	 * @param inDisplayName
	 */
	RicercheEnum(final String inDisplayName, final String inValue, final AccessoFunzionalitaEnum inAccesso) {
		displayName = inDisplayName;
		value = inValue;
		accesso = inAccesso;
	}

	/**
	 * @return the displayName
	 */
	public final String getDisplayName() {
		return displayName;
	}
	
	/**
	 * @return the value
	 */
	public final String getValue() {
		return value;
	}

	/**
	 * @return the accesso
	 */
	public final AccessoFunzionalitaEnum getAccesso() {
		return accesso;
	}
	
}
