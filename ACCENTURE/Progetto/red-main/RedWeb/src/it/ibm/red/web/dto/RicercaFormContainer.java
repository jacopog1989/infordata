package it.ibm.red.web.dto;

import it.ibm.red.business.dto.AbstractDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataFaldDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataFascDTO;
import it.ibm.red.business.dto.ParamsRicercaContiConsuntiviDTO;
import it.ibm.red.business.dto.ParamsRicercaDocUCBAssegnanteAssegnatarioDTO;
import it.ibm.red.business.dto.ParamsRicercaEsitiDTO;
import it.ibm.red.business.dto.ParamsRicercaEstrattiContoDTO;
import it.ibm.red.business.dto.ParamsRicercaFascicoliPregressiDfDTO;
import it.ibm.red.business.dto.ParamsRicercaFlussiDTO;
import it.ibm.red.business.dto.ParamsRicercaProtocolliPregressiDfDTO;
import it.ibm.red.business.dto.ParamsRicercaProtocolloDTO;
import it.ibm.red.business.dto.ParamsRicercaRegistrazioniAusiliarieDTO;
import it.ibm.red.business.dto.RicercaVeloceRequestDTO;

/**
 * Wrapper dei form di ricerca in sessione
 * 
 * @author a.difolca
 */
public class RicercaFormContainer extends AbstractDTO {
	
	/**
	 * serial version UID.
	 */
	private static final long serialVersionUID = -6016966555713620796L;
	
	/**
	 * Form di ricerca RED ambito Documenti
	 */
	private RicercaVeloceRequestDTO rapida;

	/**
	 * Form di ricerca RED ambito Documenti
	 */
	private ParamsRicercaAvanzataDocDTO documentiRed;
	
	/**
	 * Form di ricerca RED ambito Fascicoli
	 */
	private ParamsRicercaAvanzataFascDTO fascicoliRed;
	
	/**
	 * Form di ricerca RED ambito Faldoni
	 */
	private ParamsRicercaAvanzataFaldDTO faldoniRed;
	
	/**
	 * Form di ricerca Fepa
	 */
	private ParamsRicercaAvanzataDocDTO fepa;
	
	/**
	 * Form di ricerca Sigi
	 */
	private ParamsRicercaAvanzataDocDTO sigi;
	
	/**
	 * Form di ricerca Dati Protocollo (NPS)
	 */
	private ParamsRicercaProtocolloDTO datiProtocollo;
	
	/**
	 * Form di ricerca Registro Protocollo
	 */
	private ParamsRicercaAvanzataDocDTO registroProtocollo;
	 
	/**
	 * Form di ricerca fascicoli pregressi df
	 */
	private ParamsRicercaFascicoliPregressiDfDTO fascicoliDF;
	
	/**
	 * Form di ricerca protocolli pregressi df
	 */
	private ParamsRicercaProtocolliPregressiDfDTO protocolliDF;
	
	/**
	 * Form di ricerca Documenti UCB.
	 */
	private ParamsRicercaAvanzataDocDTO documentiUCB;
	
	/**
	 * Form di ricerca assegnante/assegnatario UCB.
	 */
	private ParamsRicercaDocUCBAssegnanteAssegnatarioDTO assegnazioneUCB;
	
	/**
	 * Form di ricerca registrazioni ausiliarie.
	 */
	private ParamsRicercaRegistrazioniAusiliarieDTO registrazioniAusiliarie;
	
	/**
	 * Form di ricerca esiti visti/osservazioni.
	 */
	private ParamsRicercaEsitiDTO esitiVistiOss;

	/**
	 * Form di ricerca flussi UCB.
	 */
	private ParamsRicercaFlussiDTO flussiUCB;
	
	/**
	 * Form di ricerca flussi UCB.
	 */
	private ParamsRicercaEstrattiContoDTO estrattiContoUCB;
	
	/**
	 * Form di ricerca elenco notifiche e conti consuntivi UCB.
	 */
	private ParamsRicercaContiConsuntiviDTO contiConsuntiviUCB;
	
	/**
	 * Costruttore. Si limita ad inizializzare le variabili.
	 */
	public RicercaFormContainer() {
		this.rapida = new RicercaVeloceRequestDTO();
		this.documentiRed = new ParamsRicercaAvanzataDocDTO();
		this.fascicoliRed = new ParamsRicercaAvanzataFascDTO();
		this.faldoniRed = new ParamsRicercaAvanzataFaldDTO(); 
		this.fepa = new ParamsRicercaAvanzataDocDTO(); 
		this.sigi = new ParamsRicercaAvanzataDocDTO(); 
		this.datiProtocollo = new ParamsRicercaProtocolloDTO(); 
		this.registroProtocollo = new ParamsRicercaAvanzataDocDTO();
		this.fascicoliDF = new ParamsRicercaFascicoliPregressiDfDTO();
		this.protocolliDF = new ParamsRicercaProtocolliPregressiDfDTO();
		this.documentiUCB = new ParamsRicercaAvanzataDocDTO();
		this.assegnazioneUCB = new ParamsRicercaDocUCBAssegnanteAssegnatarioDTO();
		this.registrazioniAusiliarie = new ParamsRicercaRegistrazioniAusiliarieDTO();
		this.esitiVistiOss = new ParamsRicercaEsitiDTO();
		this.flussiUCB = new ParamsRicercaFlussiDTO();
		this.estrattiContoUCB = new ParamsRicercaEstrattiContoDTO();
		this.contiConsuntiviUCB = new ParamsRicercaContiConsuntiviDTO();
	}
	

	/**
	 * @return documentiRed
	 */
	public ParamsRicercaAvanzataDocDTO getDocumentiRed() {
		return documentiRed;
	}

	/**
	 * @param red
	 */
	public void setDocumentiRed(final ParamsRicercaAvanzataDocDTO red) {
		this.documentiRed = red;
	}
	
	/**
	 * @return the fascicoliRed
	 */
	public final ParamsRicercaAvanzataFascDTO getFascicoliRed() {
		return fascicoliRed;
	}

	/**
	 * @param fascicoliRed the fascicoliRed to set
	 */
	public final void setFascicoliRed(final ParamsRicercaAvanzataFascDTO fascicoliRed) {
		this.fascicoliRed = fascicoliRed;
	}

	/**
	 * @return the faldoniRed
	 */
	public final ParamsRicercaAvanzataFaldDTO getFaldoniRed() {
		return faldoniRed;
	}

	/**
	 * @param faldoniRed the faldoniRed to set
	 */
	public final void setFaldoniRed(final ParamsRicercaAvanzataFaldDTO faldoniRed) {
		this.faldoniRed = faldoniRed;
	}
	
	/**
	 * @return the fepa
	 */
	public final ParamsRicercaAvanzataDocDTO getFepa() {
		return fepa;
	}

	/**
	 * @param fepa the fepa to set
	 */
	public final void setFepa(final ParamsRicercaAvanzataDocDTO fepa) {
		this.fepa = fepa;
	}

	/**
	 * @return the sigi
	 */
	public final ParamsRicercaAvanzataDocDTO getSigi() {
		return sigi;
	}

	/**
	 * @param sigi the sigi to set
	 */
	public final void setSigi(final ParamsRicercaAvanzataDocDTO sigi) {
		this.sigi = sigi;
	}

	/**
	 * @return the datiProtocollo
	 */
	public final ParamsRicercaProtocolloDTO getDatiProtocollo() {
		return datiProtocollo;
	}

	/**
	 * @param datiProtocollo the datiProtocollo to set
	 */
	public final void setDatiProtocollo(final ParamsRicercaProtocolloDTO datiProtocollo) {
		this.datiProtocollo = datiProtocollo;
	}

	/**
	 * @return the registroProtocollo
	 */
	public final ParamsRicercaAvanzataDocDTO getRegistroProtocollo() {
		return registroProtocollo;
	}

	/**
	 * @param registroProtocollo the registroProtocollo to set
	 */
	public final void setRegistroProtocollo(final ParamsRicercaAvanzataDocDTO registroProtocollo) {
		this.registroProtocollo = registroProtocollo;
	}

	/**
	 * @return the rapida
	 */
	public final RicercaVeloceRequestDTO getRapida() {
		return rapida;
	}

	/**
	 * @param rapida the rapida to set
	 */
	public final void setRapida(final RicercaVeloceRequestDTO rapida) {
		this.rapida = rapida;
	}
	
	/**
	 * @return the documentiUCB
	 */
	public ParamsRicercaAvanzataDocDTO getDocumentiUCB() {
		return documentiUCB;
	}
 
	/**
	 * @return the fascicoliDF
	 */
	public final ParamsRicercaFascicoliPregressiDfDTO getFascicoliDF() {
		return fascicoliDF;
	}
	
	/**
	 * @param fascicoliDF the fascicoliDF to set
	 */
	public void setFascicoliDF(final ParamsRicercaFascicoliPregressiDfDTO fascicoliDF) {
		this.fascicoliDF = fascicoliDF;
	}
	
	/**
	 * @return
	 */
	public final ParamsRicercaProtocolliPregressiDfDTO getProtocolliDF() {
		return protocolliDF;
	}
	
	/**
	 * @param protocolliDF
	 */
	public void setProtocolliDF(final ParamsRicercaProtocolliPregressiDfDTO protocolliDF) {
		this.protocolliDF = protocolliDF;
	}
	
	/**
	 * @param documentiUCB the documentiUCB to set
	 */
	public void setDocumentiUCB(final ParamsRicercaAvanzataDocDTO documentiUCB) {
		this.documentiUCB = documentiUCB;
	}

	/**
	 * @return the assegnazioneUCB
	 */
	public ParamsRicercaDocUCBAssegnanteAssegnatarioDTO getAssegnazioneUCB() {
		return assegnazioneUCB;
	}

	/**
	 * @param assegnazioneUCB the assegnazioneUCB to set
	 */
	public void setAssegnazioneUCB(final ParamsRicercaDocUCBAssegnanteAssegnatarioDTO assegnazioneUCB) {
		this.assegnazioneUCB = assegnazioneUCB;
	}


	/**
	 * @return the registrazioniAusiliarie
	 */
	public ParamsRicercaRegistrazioniAusiliarieDTO getRegistrazioniAusiliarie() {
		return registrazioniAusiliarie;
	}


	/**
	 * @param registrazioniAusiliarie the registrazioniAusiliarie to set
	 */
	public void setRegistrazioniAusiliarie(final ParamsRicercaRegistrazioniAusiliarieDTO registrazioniAusiliarie) {
		this.registrazioniAusiliarie = registrazioniAusiliarie;
	}

	/**
	 * @return the esitiVistiOss
	 */
	public ParamsRicercaEsitiDTO getEsitiVistiOss() {
		return esitiVistiOss;
	}

	/**
	 * @param esitiVistiOss the esitiVistiOss to set
	 */
	public void setEsitiVistiOss(final ParamsRicercaEsitiDTO esitiVistiOss) {
		this.esitiVistiOss = esitiVistiOss;
	}

	/**
	 * Restituisce i parametri della ricerca flussi UCB.
	 * @return parametri ricerca
	 */
	public ParamsRicercaFlussiDTO getFlussiUCB() {
		return flussiUCB;
	}

	/**
	 * Imposta i parametri della ricerca flussi UCB.
	 * @param flussiUCB
	 */
	public void setFlussiUCB(final ParamsRicercaFlussiDTO flussiUCB) {
		this.flussiUCB = flussiUCB;
	}
	
	/**
	 * Restituisce i parametri della ricerca estratti conto trimestrali UCB.
	 * @return parametri ricerca
	 */
	public ParamsRicercaEstrattiContoDTO getEstrattiContoUCB() {
		return estrattiContoUCB;
	}

	/**
	 * Imposta i parametri della ricerca estratti conto trimestrali UCB.
	 * @param flussiUCB
	 */
	public void setEstrattiContoUCB(final ParamsRicercaEstrattiContoDTO estrattiContoUCB) {
		this.estrattiContoUCB = estrattiContoUCB;
	}

	/**
	 * Restituisce i parametri della ricerca elenco notifiche e conti consuntivi UCB.
	 * 
	 * @return parametri ricerca
	 */
	public ParamsRicercaContiConsuntiviDTO getContiConsuntiviUCB() {
		return contiConsuntiviUCB;
	}

	/**
	 * Imposta i parametri della ricerca elenco notifiche e conti consuntivi UCB.
	 * 
	 * @param contiConsuntiviUCB
	 */
	public void setContiConsuntiviUCB(ParamsRicercaContiConsuntiviDTO contiConsuntiviUCB) {
		this.contiConsuntiviUCB = contiConsuntiviUCB;
	}
	
}
