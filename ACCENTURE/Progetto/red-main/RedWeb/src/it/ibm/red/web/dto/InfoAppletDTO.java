package it.ibm.red.web.dto;

import java.io.Serializable;
import java.util.Date;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.servlets.LocalSignServlet;
import it.ibm.red.web.servlets.delegate.IGetDelegate;
import it.ibm.red.web.servlets.delegate.IPostDelegate;

/**
 * 
 * @author CPIERASC
 *
 *	Classe utilizzata per gestire i parametri dell'applet di firma locale.
 */
public class InfoAppletDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5424644255935643957L;

	/**
	 * Path base dei jar dell'applet.
	 */
	private final String codebase;

    /**
     * Action.
     */
	private final String action;

	/**
	 * Url file sicurezza applet.
	 */
	private final String imageFile;

	/**
	 * Url della servlet a cui l'applet deve inviare la get per recuperare il documento da firmare.
	 */
	private final String documentURL;
	/**
	 * Url della servlet a cui l'applet deve inviare la post per restituire il documento firmato.
	 */
	private final String postURL;

	/**
	 * Booleano utilizzato per gestire la renderizzazione del pannello di firma.
	 */
	private Boolean renderSign;
	

    /**
     * Source.
     */
	private final String source;
	
	/**
	 * Costruttore.
	 * 
	 * @param source		pagina sorgente (in cui è presente l'applet)
	 * @param inAction		action da innescare con l'applet
	 * @param getDelegate	delegato per gestire il recupero del documento da firmare
	 * @param postDelegate	delegato per gestire la restituzione del documento firmato
	 */
	public InfoAppletDTO(final String inSource, final String inAction, final Class<? extends IGetDelegate> getDelegate, final Class<? extends IPostDelegate> postDelegate) {
		final String servletName = LocalSignServlet.class.getSimpleName();
        final HttpServletRequest httpRequest = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest());
        
        String port = "";
        if ("on".equalsIgnoreCase(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.LOCALSIGN_SERVERPORT_STATUS))) {
            port = ":" + httpRequest.getServerPort();
        }

        final String reqCtxPath = "//" + httpRequest.getServerName() + port + FacesHelper.getRequest().getContextPath() + "/";
                
        
        source = inSource;
        codebase = reqCtxPath + "pknet/";
        documentURL = reqCtxPath + servletName + "?delegate=" + getDelegate.getName();
        imageFile = reqCtxPath + "pknet/pknet.gif.p7m";
        postURL = reqCtxPath + servletName + "?delegate=" + postDelegate.getName() + "&source=" + source;
        renderSign = false;
        action = inAction;
	}
	
	/**
	 * Getter codebase.
	 * 
	 * @return	codebase
	 */
	public final String getCodebase() {
		return codebase;
	}

	/**
	 * Getter url get documento da firmare.
	 * 
	 * @return	url get
	 */
	public final String getDocumentURL() {
		final Long now = (new Date()).getTime();
		final String noCache = "&t=" + now;
		return documentURL + noCache;
	}

	/**
	 * Getter url file sicurezza.
	 * 
	 * @return	url file sicurezza
	 */
	public final String getImageFile() {
		return imageFile;
	}

	/**
	 * Getter url post documento firmato.
	 * 
	 * @return	url post
	 */
	public final String getPostURL() {
		final Long now = (new Date()).getTime();
		final String noCache = "&t=" + now;
		return postURL + noCache;
	}

	/**
	 * Getter messaggio applet.
	 * 
	 * @return	messaggio applet
	 */
	public static final String getMsg(final Integer nRemainingDocs) {
		return "Avvio del processo di firma di " + nRemainingDocs  + " documenti.";
	}
	
	/**
	 * Recupera la fonte.
	 * @return fonte
	 */
	public String getSource() {
        return source;
    }

	/**
	 * Getter booleano per gestire renderizzazione del pannello di firma.
	 * 
	 * @return	booleano per gestire renderizzazione pannello di firma
	 */
	public final Boolean getRenderSign() {
		return renderSign;
	}
	
	/**
	 * Imposta l'attributo renderSign.
	 * @param inRenderSign
	 */
	public void setRenderSign(final boolean inRenderSign) {
		this.renderSign = inRenderSign;
	}
	
	/**
	 * Recupera l'azione.
	 * @return azione
	 */
	public String getAction() {
		return action;
	}

}
