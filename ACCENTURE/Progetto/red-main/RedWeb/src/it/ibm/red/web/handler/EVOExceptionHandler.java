package it.ibm.red.web.handler;
import java.util.Iterator;

import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

import org.primefaces.application.exceptionhandler.PrimeExceptionHandler;

import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.StringUtils;

/**
 * Handler eccezioni Evo.
 */
public class EVOExceptionHandler extends PrimeExceptionHandler {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(EVOExceptionHandler.class);
    
	/**
	 * Costruttore.
	 * @param wrapped
	 */
	public EVOExceptionHandler(final ExceptionHandler wrapped) {
    	super(wrapped);
    }

	/**
	 * @see org.primefaces.application.exceptionhandler.PrimeExceptionHandler#handle().
	 */
	@Override
    public void handle() throws FacesException {
    	
		String handlerStatus = "OFF";
		
		try {
			handlerStatus = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.EXCEPTION_HANDLER_VALUE);
		} catch (final Exception e) {
			LOGGER.error(PropertiesNameEnum.EXCEPTION_HANDLER_VALUE + ":  Proprietà non caricata correttamente", e);
		}
    	
    	if (!StringUtils.isNullOrEmpty(handlerStatus) && !"OFF".equalsIgnoreCase(handlerStatus)) {
    		
    		if ("PRIMEFACES".equalsIgnoreCase(handlerStatus)) {
        		super.handle();
    		} else {
    			
                final Iterator<ExceptionQueuedEvent> queue = getUnhandledExceptionQueuedEvents().iterator();
                
                while (queue.hasNext()) {
                    final ExceptionQueuedEvent item = queue.next();
                    final ExceptionQueuedEventContext exceptionQueuedEventContext = (ExceptionQueuedEventContext) item.getSource();
                	final Throwable throwable = exceptionQueuedEventContext.getException();
                	if ("CUSTOM".equalsIgnoreCase(handlerStatus)) {
                		LOGGER.error(new Exception(throwable));
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "An exception occurred", "ViewExpiredException happened!"));
                	}
                	
                    queue.remove();
                }
    		}
    	}
    }
    
}