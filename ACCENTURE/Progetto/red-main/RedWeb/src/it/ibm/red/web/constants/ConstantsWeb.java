package it.ibm.red.web.constants;

/**
 * The Class ConstantsWeb.
 *
 * @author CPIERASC
 * 
 * 	Classe delle costanti.
 */
public final class ConstantsWeb {

	/**
	 * DOCUMENT_MANAGER_MESSAGES_CLIENT_ID.
	 */
	public static final String DOCUMENT_MANAGER_MESSAGES_CLIENT_ID = "dlgDocumentManager";

	/**
	 * ASSEGNAZIONE_DELEGHE_MESSAGES_CLIENT_ID.
	 */
	public static final String ASSEGNAZIONE_DELEGHE_MESSAGES_CLIENT_ID = "pnlAssegnazioniDeleghe";

	/**
	 * Costanti oggetti di sessione.
	 */
	public static final class SessionObject {
		
		/**
		 * Content highligh sign field - label.
		 */
		public static final String CONTENT_HIGHLIGHT_SIGN_FIELD = "CONTENT_HIGHLIGHT_SIGN_FIELD";
		/**
		 * Lista documenti - label.
		 */
		public static final String LISTA_DOCUMENTI = "LISTA_DOCUMENTI";
		/**
		 * Stato ricerca - label.
		 */
		public static final String STATO_RICERCA = "STATO_RICERCA";
		/**
		 * Risultati ricerca avanzata - label.
		 */
		public static final String RISULTATI_RICERCA_AVANZATA = "RISULTATI_RICERCA_AVANZATA";
		/**
		 * Filter by per master go to coda.
		 */
		public static final String FILTER_MASTER_GO_TO_CODA = "FILTER_BY_RICERCA";
		/**
		 * Lista fascicoli - label.
		 */
		public static final String LISTA_FASCICOLI = "LISTA_FASCICOLI";
		/**
		 * Lista faldoni - label.
		 */
		public static final String LISTA_FALDONI = "LISTA_FALDONI";
		/**
		 * Session LSH - label.
		 */
		public static final String SESSION_LSH = "SESSION_LSH";
		/**
		 * Final step - label.
		 */
		public static final String FINAL_STEP = "finalstep";
		/**
		 * Object organigramma - label.
		 */
		public static final String ORG_OBJ = "objOrganigramma";
		/**
		 * Object titolario - label.
		 */
		public static final String TITO_OBJ = "objTitolario";
		/**
		 * Object faldone - label.
		 */
		public static final String FALDO_OBJ = "objFaldone";
		/**
		 * Username - label.
		 */
		public static final String USERNAME = "username";
		/**
		 * Edted Template - label.
		 */
		public static final String EDITED_TEMPLATE = "editedTemplate";
		/**
		 * NPS registro item.
		 */
		public static final String NPS_REGISTRO_ITEM_FILE = "NPS_REGISTRO_ITEM";
		/**
		 * Risultato ricerca flussi - label.
		 */
		public static final String RISULTATO_RICERCA_FLUSSI = "RISULTATO_RICERCA_FLUSSI";
		/**
		 * Print file servlet - label.
		 */
		public static final String PRINT_FILE_SERVLET = "PRINT_FILE_SERVLET";
		/**
		 * Content highligh sign field - sinistra.
		 */
		public static final String CONTENT_HIGHLIGHT_SIGN_FIELD_SX = "CONTENT_HIGHLIGHT_SIGN_FIELD_SX";
		/**
		 * Content highligh sign field - destra.
		 */
		public static final String CONTENT_HIGHLIGHT_SIGN_FIELD_DX = "CONTENT_HIGHLIGHT_SIGN_FIELD_DX"; 
		/**
		 * Risultato ricerca estratti conto trimestrali CCVT - label.
		 */
		public static final String RISULTATO_RICERCA_ESTRATTI_CONTO = "RISULTATO_RICERCA_ESTRATTI_CONTO";
		/**
		 * Risultato ricerca elenco notifiche e conti consuntivi - label.
		 */
		public static final String RISULTATO_RICERCA_CONTI_CONSUNTIVI = "RISULTATO_RICERCA_CONTI_CONSUNTIVI";
		
		private SessionObject() {
			
		}
	}

	/**
	 * The Class MBean.
	 *
	 * @author CPIERASC
	 * 
	 * 	Managed bean name.
	 */
	public static final class MBean {
		
		/**
		 * Login.
		 */
		public static final String LOGIN_BEAN = "LoginBean";

		/**
		 * Session.
		 */
		public static final String SESSION_BEAN = "SessionBean";
		/**
		 * FascicoloFepa.
		 */
		public static final String FASCICOLO_FEPA_BEAN = "FascicoloFepaBean";
		/**
		 * Dashboard.
		 */
		public static final String DASHBOARD_BEAN = "DashboardBean";
		/**
		 * Lista documenti.
		 */
		public static final String LISTA_DOCUMENTI_BEAN = "ListaDocumentiBean";
		/**
		 * Dettaglio documento.
		 */
		public static final String DETTAGLIO_DOCUMENTO_BEAN = "DettaglioDocumentoBean";
		/**
		 * Dettaglio fascicolo.
		 */
		public static final String DETTAGLIO_FASCICOLO_BEAN = "DettaglioFascicoloBean";
		/**
		 * Dettaglio fascicolo ricerca.
		 */
		public static final String DETTAGLIO_FASCICOLO_RICERCA_BEAN = "DettaglioFascicoloRicercaBean";
		/**
		 * Dettaglio faldone ricerca.
		 */
		public static final String DETTAGLIO_FALDONE_RICERCA_BEAN = "DettaglioFaldoneRicercaBean";
		/**
		 * Dettaglio SIGI.
		 */
		public static final String DETTAGLIO_SIGI_BEAN = "DettaglioSigiBean";
		/**
		 * Dettaglio NPS.
		 */
		public static final String DETTAGLIO_NPS_BEAN = "DettaglioNpsBean";
		/**
		 * Dettaglio stampa registro.
		 */
		public static final String DETTAGLIO_STAMPA_REGISTRO_BEAN = "DettaglioStampaRegistroBean";
		/**
		 * Personalizzazione.
		 */
		public static final String PERSONALIZZAZIONE_BEAN = "PersonalizzazioneBean";
		/**
		 * Calendario.
		 */
		public static final String CALENDARIO_BEAN = "CalendarioBean";
		/**
		 * Ricerca faldoni.
		 */
		public static final String RICERCA_FALDONI_BEAN = "RicercaFaldoniBean";
		/**
		 * Ricerca fascicoli.
		 */
		public static final String RICERCA_FASCICOLI_BEAN = "RicercaFascicoliBean";
		/**
		 * Ricerca fascicoli pregressi NSD.
		 */
		public static final String RICERCA_FASCICOLI_PREGRESSI_NSD_BEAN = "RicercaFascicoliPregressiNsdBean";
		/**
		 * Ricerca protocolli pregressi NSD.
		 */
		public static final String RICERCA_PROTOCOLLI_PREGRESSI_NSD_BEAN = "RicercaProtocolliPregressiNsdBean";
		/**
		 * Dettaglio fascicolo ricerca NSD.
		 */
		public static final String DETTAGLIO_FASCICOLO_RICERCA_NSD_BEAN = "DettaglioFascicoloRicercaNsdBean";
		/**
		 * Dettaglio protocollo ricerca NSD.
		 */
		public static final String DETTAGLIO_PROTOCOLLO_RICERCA_NSD_BEAN = "DettaglioProtocolloRicercaNsdBean";
		/**
		 * Ricerca documenti.
		 */
		public static final String RICERCA_DOCUMENTI_BEAN = "RicercaDocumentiBean";
		/**
		 * Faldoni.
		 */
		public static final String FALDONI_BEAN = "FaldoniBean";
		/**
		 * Fascicoli.
		 */
		public static final String FASCICOLI_BEAN = "FascicoliBean";
		/**
		 * Fascicolo Manager.
		 */
		public static final String FASCICOLO_MANAGER_BEAN = "FascicoloManagerBean";
		/**
		 * Document Manager.
		 */
		public static final String DOCUMENT_MANAGER_BEAN = "DocumentManagerBean";
		/**
		 * Ricerca.
		 */
		public static final String RUBRICA_BEAN = "RubricaBean";
		/**
		 * Widget Rubrica.
		 */
		public static final String WDG_RUBRICA_BEAN = "WidgetRubricaBean";
		/**
		 * Admin Tool.
		 */
		public static final String ADMIN_TOOL_BEAN = "AdminToolBean";
		/**
		 * Mail.
		 */
		public static final String MAIL_BEAN = "MailBean";
		/**
		 * Protocolla.
		 */
		public static final String PROTOCOLLA_BEAN = "ProtocollaBean";
		/**
		 * Dettaglio documento FEPA.
		 */
		public static final String DETTAGLIO_DOCUMENTO_FEPA_BEAN = "DettaglioDocumentoFepaBean";
		/**
		 * DSR Manager.
		 */
		public static final String DSR_MANAGER_BEAN = "DsrManagerBean";
		/**
		 * Ricerca FEPA.
		 */
		public static final String RICERCA_FEPA_BEAN = "RicercaFepaBean";
		/**
		 * Sottoscrizioni.
		 */
		public static final String SOTTOSCRIZIONI_BEAN = "SottoscrizioniBean";
		/**
		 * Report.
		 */
		public static final String REPORT_BEAN = "ReportBean";
		/**
		 * Wizar tipi documento.
		 */
		public static final String WIZARD_TIPI_DOCUMENTO_BEAN = "WizardTipiDocumentoBean";
		/**
		 * Wizard assegnazioni automatiche.
		 */
		public static final String WIZARD_ASSEGNAZIONI_AUTOMATICHE_BEAN = "WizardAssegnazioniAutomaticheBean";
		/**
		 * Assegnazione metadati.
		 */
		public static final String ASSEGNAZIONE_METADATI_BEAN = "AssegnazioneMetadatiBean";
		/**
		 * Anagrafica dipendenti.
		 */
		public static final String ANAGRAFICA_DIPENDENTI_BEAN = "AnagraficaDipendentiBean";
		/**
		 * Predisponi da registro ausiliario.
		 */
		public static final String PREDISPONI_DA_REGISTRO_AUSILIARIO_BEAN = "PredisponiDaRegistroAusiliarioBean";
		/**
		 * Capitolo Spesa.
		 */
		public static final String CAPITOLO_SPESA_BEAN = "CapitoloSpesaBean";
		/**
		 * Caricamento massivo.
		 */
		public static final String CARICAMENTO_MASSIVO_BEAN = "CaricamentoMassivoBean";
		/**
		 * Accordi servizio.
		 */
		public static final String ACCORDI_SERVIZIO_BEAN = "AccordiServizioBean";
		//Bean che gestiscono Widget
		//********* START ***************
		/**
		 * News widget.
		 */
		public static final String NEWS_WIDGET_BEAN = "NewsWidgetBean";
		/**
		 * Sottoscrizioni widget.
		 */
		public static final String SOTTOSCRIZIONI_WIDGET_BEAN = "SottoscrizioniWidgetBean";
		/**
		 * Calendario widget.
		 */
		public static final String CALENDARIO_WIDGET_BEAN = "CalendarioWidgetBean";
		/**
		 * Spedizioni widget.
		 */
		public static final String SPEDIZIONI_WIDGET_BEAN = "SpedizioniWidgetBean";
		/**
		 * INAPPROVAZIONEPERSTRUTTURA widget.
		 */
		public static final String INAPPROVAZIONEPERSTRUTTURA_WIDGET_BEAN = "InApprovazionePerStrutturaWidgetBean";
		/**
		 * INAPPROVAZIONEPERDIRIGENTE widget.
		 */
		public static final String INAPPROVAZIONEPERDIRIGENTE_WIDGET_BEAN = "InApprovazionePerDirigenteWidgetBean";
		//Bean che gestiscono Human Task
		//********* START ***************
		/**
		 * Inserisci nota.
		 */
		public static final String INSERISCI_NOTA_BEAN = "InserisciNotaBean";
		/**
		 * Spedito nota.
		 */
		public static final String SPEDITO_BEAN = "SpeditoBean";
		/**
		 * Ufficio proponente.
		 */
		public static final String UFFICIO_PROPONENTE_BEAN = "UfficioProponenteBean";
		/**
		 * Storna utente.
		 */
		public static final String STORNA_UTENTE_BEAN = "StornaAUtenteBean";
		/**
		 * Storna.
		 */
		public static final String STORNA_BEAN = "StornaBean";
		/**
		 * Assegna.
		 */
		public static final String ASSEGNA_BEAN = "AssegnaBean";
		/**
		 * Rifiuta.
		 */
		public static final String RIFIUTA_BEAN = "RifiutaBean";
		/**
		 * Sigla.
		 */
		public static final String SIGLA_BEAN = "SiglaBean";
		/**
		 * Modifica iter.
		 */
		public static final String MODIFICA_ITER_BEAN = "ModificaIterBean";
		/**
		 * Modifica spedizione.
		 */
		public static final String RIFIUTA_SPEDIZIONE_BEAN = "RifiutaSpedizioneBean";
		/**
		 * Riattiva procedimento.
		 */
		public static final String RIATTIVA_PROCEDIMENTO_BEAN = "RiattivaProcedimentoBean";
		/**
		 * Invia in firma sigla visto.
		 */
		public static final String INVIA_IN_FIRMA_SIGLA_VISTO_BEAN = "InviaInFirmaSiglaVistoBean";
		/**
		 * Inoltra.
		 */
		public static final String INOLTRA = "InoltraBean";
		/**
		 * Faldona.
		 */
		public static final String FALDONA_BEAN = "FaldonaBean";
		/**
		 * Riassegna.
		 */
		public static final String RIASSEGNA = "RiassegnaBean";
		/**
		 * Metti in conoscenza.
		 */
		public static final String METTI_IN_CONOSCESCENZA_BEAN = "MettiInConoscenzaBean";
		/**
		 * Atti.
		 */
		public static final String ATTI_BEAN = "AttiBean";
		/**
		 * Richiedi visti.
		 */
		public static final String RICHIEDI_VISTI_BEAN = "RichiediVistiBean";
		/**
		 * Richiesta visti.
		 */
		public static final String RICHIESTA_VISTI_BEAN = "RichiestaVistiBean";
		/**
		 * Richiedi visto ispettore.
		 */
		public static final String RICHIEDI_VISTO_ISPETTORE_BEAN = "RichiediVistoIspettoreBean";
		/**
		 * Richiedi contributo.
		 */
		public static final String RICHIEDI_CONTRIBUTO_BEAN = "RichiediContributoBean";
		/**
		 * Richiedi visto ufficio.
		 */
		public static final String RICHIEDI_VISTO_UFFICIO_BEAN = "RichiediVistoUfficioBean";
		/**
		 * Inserisci contributo.
		 */
		public static final String INSERISCI_CONTRIBUTO_BEAN = "InserisciContributoBean";
		/**
		 * Valida contributi.
		 */
		public static final String VALIDA_CONTRIBUTI_BEAN = "ValidaContributiBean";
		/**
		 * Validazione contributo.
		 */
		public static final String VALIDAZIONE_CONTRIBUTO_BEAN = "ValidazioneContributoBean";
		/**
		 * Visto.
		 */
		public static final String VISTO_BEAN = "VistoBean";
		/**
		 * Sigla e richiedi visti.
		 */
		public static final String SIGLA_E_RICHIEDI_VISTI_BEAN = "SiglaERichiediVistiBean";
		/**
		 * Richiedi verifica.
		 */
		public static final String RICHIEDI_VERIFICA_BEAN = "RichiediVerificaBean";
		/**
		 * Verificato.
		 */
		public static final String VERIFICATO_BEAN = "VerificatoBean";
		/**
		 * Non verificiato.
		 */
		public static final String NON_VERIFICATO_BEAN = "NonVerificatoBean";
		/**
		 * Invio decreto firma.
		 */
		public static final String INVIO_DECRETO_FIRMA_BEAN = "InvioDecretoFirmaBean";
		/**
		 * Associa decreti.
		 */
		public static final String ASSOCIA_DECRETI_BEAN = "AssociaDecretiBean";
		/**
		 * Modifica associazioni decreti.
		 */
		public static final String MODIFICA_ASSOCIAZIONI_DECRETI_BEAN = "ModificaAssociazioniDecretiBean";
		/**
		 * Chiudi senza associazione.
		 */
		public static final String CHIUDI_SENZA_ASSOCIAZIONE_BEAN = "ChiudiSenzaAssociazioneBean";
		/**
		 * Predisponi dichiarazione.
		 */
		public static final String PREDISPONI_DICHIARAZIONE = "PredisponiDichiarazioneBean";
		/**
		 * Dettaglio dichiarazione.
		 */
		public static final String DETTAGLIO_DICHIARAZIONE = "DettaglioDichiarazioneBean";
		/**
		 * Predisposizione firma autografa.
		 */
		public static final String PREDISPOSIZIONE_FIRMA_AUTOGRAFA_BEAN = "PredisposizioneFirmaAutografaBean";
		/**
		 * Inoltra mail da coda attiva.
		 */
		public static final String INOLTRA_MAIL_DA_CODA_ATTIVA = "InoltraMailDaCodaAttivaBean";
		/**
		 * Apri RDS SIEBEL.
		 */
		public static final String APRI_RDS_SIEBEL_BEAN = "ApriRdsSiebelBean";
		/**
		 * Recall.
		 */
		public static final String RECALL_BEAN = "RecallBean"; 
		/**
		 * Assegna nuova.
		 */
		public static final String ASSEGNANUOVA_BEAN = "AssegnaNuovaBean";
		/**
		 * Seleziona documento.
		 */
		public static final String SELEZIONA_DOCUMENTO_BEAN = "SelezionaDocumentoBean";
		/**
		 * Predisponi da registro ausiliario response.
		 */
		public static final String PREDISPONI_DA_REGISTRO_AUSILIARIO_RESPONSE_BEAN = "PredisponiDaRegistroAusiliarioResponseBean";
		//********* END ***************
		/**
		 * Multi assegnazione.
		 */
		public static final String MULTI_ASSEGNAZIONE_BEAN = "MultiAssegnazioneBean";
		/**
		 * Predisponi documento.
		 */
		public static final String PREDISPONI_DOCUMENTO_BEAN = "PredisponiDocumentoBean";
		/**
		 * Faldonatura.
		 */
		public static final String FALDONATURA_BEAN = "FaldonaturaBean";
		/**
		 * Lista fascicoli.
		 */
		public static final String LISTA_FASCICOLI_BEAN = "ListaFascicoliBean";
		/**
		 * Lista faldoni.
		 */
		public static final String LISTA_FALDONI_BEAN = "ListaFaldoniBean";
		/**
		 * Cambia ufficio.
		 */
		public static final String CAMBIA_UFFICIO_BEAN = "CambiaUfficioBean";
		/**
		 * Ricerca registro protocollo stampa.
		 */
		public static final String RICERCA_REGISTRO_PROTOCOLLO_STAMPA_BEAN = "RicercaRegistroProtocolloStampaBean";
		/**
		 * Ricerca riattiva procedimento.
		 */
		public static final String RICERCA_RIATTIVA_PROCEDIMENTO_BEAN = "RicercaRiattivaProcedimentoBean";
		/**
		 * Ricerca annulla procedimento.
		 */
		public static final String RICERCA_ANNULLA_PROCEDIMENTO_BEAN = "RicercaAnnullaProcedimentoBean";
		/**
		 * Notifiche.
		 */
		public static final String NOTIFICHE_BEAN = "NotificheBean";
		/**
		 * Scadenziario.
		 */
		public static final String SCADENZIARIO_BEAN = "ScadenziarioBean";
		
		// Nuovi Bean di Ricerca
		/**
		 * Ricerca rapida.
		 */
		public static final String RICERCA_RAPIDA_BEAN = "RicercaRapidaBean";
		/**
		 * Ricerca documenti.
		 */
		public static final String RICERCA_DOCUMENTI_RED_BEAN = "RicercaDocumentiRedBean";
		/**
		 * Ricerca fascicoli RED.
		 */		
		public static final String RICERCA_FASCICOLI_RED_BEAN = "RicercaFascicoliRedBean";
		/**
		 * Ricerca faldoni.
		 */
		public static final String RICERCA_FALDONI_RED_BEAN = "RicercaFaldoniRedBean";
		/**
		 * Ricerca documenti FEPA.
		 */
		public static final String RICERCA_DOCUMENTI_FEPA_BEAN = "RicercaDocumentiFepaBean";
		/**
		 * Ricerca SIGI.
		 */
		public static final String RICERCA_SIGI_BEAN = "RicercaSigiBean";
		/**
		 * Ricerca dati protocollo.
		 */
		public static final String RICERCA_DATI_PROTOCOLLO_BEAN = "RicercaDatiProtocolloBean";
		/**
		 * Ricerca stampa registro.
		 */
		public static final String RICERCA_STAMPA_REGISTRO_BEAN = "RicercaStampaRegistroBean";
		/**
		 * Ricerca documetni UCB.
		 */
		public static final String RICERCA_DOCUMENTI_UCB_BEAN = "RicercaDocumentiUcbBean";
		/**
		 * Ricerca flussi.
		 */
		public static final String RICERCA_FLUSSI_UCB_BEAN = "RicercaFlussiUcbBean";
		/**
		 * Risultati ricerca faldoni.
		 */
		public static final String RISULTATI_RICERCA_FALDONI = "RisultatiRicercaFaldoniBean";
		/**
		 * Risultati ricerca registrazioni ausiliarie.
		 */
		public static final String RISULTATI_RICERCA_REGISTRAZIONI_AUSILIARIE_BEAN = "RisultatiRicercaRegAusiliarieBean";
		/**
		 * Esito crea doc da response.
		 */
		public static final String ESITO_CREA_DOC_DA_RESPONSE_BEAN = "EsitoCreaDocDaResponseBean";
		
		// Bean per la gestione del Contributo Esterno -> START
		/**
		 * Richiedi contributo esterno.
		 */
		public static final String RICHIEDI_CONTRIBUTO_ESTERNO_BEAN = "RichiediContributoEsternoBean";
		/**
		 * Richiedi sollecito.
		 */
		public static final String RICHIEDI_SOLLECITO_BEAN = "RichiediSollecitoBean";
		/**
		 * Collega contributo esterno.
		 */
		public static final String COLLEGA_CONTRIBUTO_ESTERNO_BEAN = "CollegaContributoEsternoBean";
		// Bean per la gestione del Contributo Esterno -> END
		/**
		 * Lista cartacei acquisiti.
		 */
		public static final String LISTA_CARTACEI_ACQUISITI_BEAN = "ListaCartaceiAcquisitiBean";
		/**
		 * Invia in firma multipla.
		 */
		public static final String INVIA_IN_FIRMA_MULTIPLA_BEAN = "InviaInFirmaMultiplaBean";

		// Bean assegnazione deleghe Libro Firma.
		/**
		 * Delega libro firma.
		 */
		public static final String DELEGA_LIBRO_FIRMA_BEAN = "DelegaLibroFirmaBean";
		/**
		 * Storna a corriere.
		 */
		public static final String STORNA_A_CORRIERE_BEAN = "StornaACorriereBean";
		/**
		 * Storna a corriere.
		 */
		public static final String RICERCA_ESTRATTI_CONTO_CCVT_BEAN = "RicercaEstrattiContoCCVTBean";
		/**
		 * Ricerca elenco notifiche e conti consuntivi.
		 */
		public static final String RICERCA_CONTI_CONSUNTIVI_BEAN = "RicercaContiConsuntiviBean";
		
		/**
		 * Nome Bean che gestisce lo human task di retry firma asincrona.
		 */
		public static final String RETRY_BEAN = "RetryBean";
		
		/**
		 * Nome Bean che gestisce il dettaglio sintetico della coda PREPARAZIONE ALLA SPEDIZIONE.
		 */
		public static final String DETTAGLIO_DOCUMENTO_ASIGN_BEAN = "DettaglioDocumentoASignBean";

		/**
		 * Costruttore.
		 */
		protected MBean() {}
	}

	/**
	 * Costruttore.
	 */
	protected ConstantsWeb() {
		
	}

}
