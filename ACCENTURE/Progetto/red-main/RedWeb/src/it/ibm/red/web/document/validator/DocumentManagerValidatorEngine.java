package it.ibm.red.web.document.validator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.math.NumberUtils;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AnagraficaDipendentiComponentDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.CapitoloSpesaMetadatoDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentManagerDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.LookupTableDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.RiferimentoProtNpsDTO;
import it.ibm.red.business.dto.RiferimentoStoricoNsdDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.SalvaDocumentoErroreEnum;
import it.ibm.red.business.enums.TipoDocumentoModeEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;
import it.ibm.red.business.utils.EmailUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * @author SLac
 *
 */
public class DocumentManagerValidatorEngine implements Serializable {
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String NON_E_VERIFICATO = " non è verificato.";

	/**
	 * Tag apertura blocco list item.
	 */
	private static final String START_LI = "<li>";

	/**
	 * Tag chiusura blocco list item.
	 */
	private static final String END_LI = "</li>";

	/**
	 * Utente.
	 */
	private final UtenteDTO utente;

	/**
	 * Document mangaer.
	 */
	private final DocumentManagerDTO dmd;

	/**
	 * Dettaglio.
	 */
	private final DetailDocumentRedDTO detail;

	/**
	 * Servizio.
	 */
	private final IDocumentManagerFacadeSRV documentManagerSRV;

	/**
	 * Chiave per l'esito.
	 */
	public static final String CHIAVE_ESITO = "esito";

	/**
	 * Chiave per il testo dell'esito.
	 */
	public static final String CHIAVE_ESITO_TESTO = "testo";

	/**
	 * Chiave per la funzione dell'esito.
	 */
	public static final String CHIAVE_ESITO_FUNZIONE = "funzione";

	/**
	 * Costruttore di classe.
	 * 
	 * @param utente
	 * @param dmd
	 * @param detail
	 * @param documentManagerSRV
	 */
	public DocumentManagerValidatorEngine(final UtenteDTO utente, final DocumentManagerDTO dmd, final DetailDocumentRedDTO detail,
			final IDocumentManagerFacadeSRV documentManagerSRV) {
		this.utente = utente;
		this.dmd = dmd;
		this.detail = detail;
		this.documentManagerSRV = documentManagerSRV;
	}

	/**
	 * Gestisce eventali errori riscontrati sull'esito passato come parametro.
	 * 
	 * @see SalvaDocumentoErroreEnum.
	 * @param esito
	 */
	public void checkErrors(final EsitoSalvaDocumentoDTO esito) {

		UtenteDTO utenteProtocollatore = null; // eventuale utente protocollatore da gestire nella protocollazione da mail

		final StringBuilder errorsMsg = new StringBuilder("<br>");
		for (final SalvaDocumentoErroreEnum s : esito.getErrori()) {
			switch (s) {
			case MAIL_GIA_IN_PROTOCOLLAZIONE_MANUALE:
				utenteProtocollatore = documentManagerSRV.getUtenteProtocollatoreMailByGuidMail(detail.getProtocollazioneMailGuid(), utente);
				errorsMsg.append("Attenzione, non è possibile procedere con l'operazione perché la mail è in fase di protocollazione dall'utente: ")
						.append(utenteProtocollatore.getNome()).append(" ").append(utenteProtocollatore.getCognome()).append("<br>");
				break;
			case MAIL_GIA_PRESA_IN_CARICO:
				utenteProtocollatore = documentManagerSRV.getUtenteProtocollatoreMailByGuidMail(detail.getProtocollazioneMailGuid(), utente);
				dmd.setDialogConfirmMessage("Attenzione, la mail è già stata presa in carico dall'utente: " + utenteProtocollatore.getNome() + " "
						+ utenteProtocollatore.getCognome() + ". Continuare con la protocollazione prendendo in carico la mail?");
				break;
			case DOC_ID_RACCOLTA_FAD_NON_PRESENTE:
				dmd.setDialogConfirmMessage(SalvaDocumentoErroreEnum.DOC_ID_RACCOLTA_FAD_NON_PRESENTE.getMessaggio());
				break;
			case DOC_ID_RACCOLTA_FAD_PRESENTE:
				dmd.setDialogConfirmMessage(SalvaDocumentoErroreEnum.DOC_ID_RACCOLTA_FAD_PRESENTE.getMessaggio());
				break;
			case DOC_ASSEGNAZIONE_SPEDIZIONE_DESTINATARIO_ELETTRONICO:
				dmd.setDialogConfirmMessage(SalvaDocumentoErroreEnum.DOC_ASSEGNAZIONE_SPEDIZIONE_DESTINATARIO_ELETTRONICO.getMessaggio());
				break;
			case DOC_PUBBLICO_ALLACCIO_RISERVATO:
				dmd.setDialogConfirmMessage(SalvaDocumentoErroreEnum.DOC_PUBBLICO_ALLACCIO_RISERVATO.getMessaggio());
				break;
			default:
				errorsMsg.append(s.getMessaggio()).append("<br>");
				break;
			}
		}

		if (!StringUtils.isNullOrEmpty(dmd.getDialogConfirmMessage())) {
			// devo mostrare il messaggio di conferma
			FacesHelper.executeJS("PF('wdgDialoConfirm_DMD').show();");

		} else if (errorsMsg != null && !StringUtils.isNullOrEmpty(errorsMsg.toString().replace("<br>", ""))) {
			// devo mostrare l'errore all'utente
			final FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore!", errorsMsg.toString()));
			FacesHelper.executeJS("PF('stickyGrowl').removeAll();PF('infoGrowl').removeAll();");
		}
	}

	/**
	 * Controlla che ci siano tutti i campi obbligatori.
	 * 
	 * @return restituisce una Map con due possibili valori: - esito -> una stringa
	 *         contenente il messaggio da mostrare all'utente - function -> la
	 *         funzione javascript da eseguire per evidenziare l'errore
	 */
	public Map<String, Object> checkPreliminareFrontEnd(final AssegnazioneDTO assegnatarioPrincipale, final List<AllegatoDTO> allegati,
			final IDocumentoFacadeSRV documentoSRV) {
		final Map<String, Object> esito = new HashMap<>();
		final StringBuilder sb = new StringBuilder("<ul class=\"warn-messages-ul\">");
		/**
		 * comuni: - documento principale * - L'oggetto deve contenere almeno 4
		 * caratteri.* e non deve essere più lungo di 1000 - Inserire il content
		 * dell'allegato. *
		 * 
		 * doc in ingresso: - Inserire il mittente. * - Indicare l'assegnatario per
		 * competenza *
		 * 
		 * doc in uscita: - Indicare l'assegnatario (solo se l'iter è manuale) * -
		 * Inserire almeno un destinatario. * - Inserire l'indice di classificazione. -
		 * Compilare i campi obbligatori del tab spedizione. - Numero RDP
		 */

		// Cerco di farli vedere in ordine
		boolean isErrorPresent = false;

		/**
		 * MITTENTE START
		 *******************************************************************/
		if (dmd.isMittenteVisible() && (detail.getMittenteContatto() == null)) {
			sb.append(START_LI).append("Inserire il mittente.").append(END_LI);
		}

		// Condizione per contatto on the fly
		if (detail.getMittenteContatto() != null && StringUtils.isNullOrEmpty(detail.getMittenteContatto().getAliasContatto())) {
			sb.append(START_LI).append("Valorizzare correttamente il contatto.").append(END_LI);
			isErrorPresent = true;
		}

		/**
		 * MITTENTE END
		 *******************************************************************/

		isErrorPresent = handleDestinatari(sb, isErrorPresent);

		/** OGGETTO START **************************************/
		if (StringUtils.isNullOrEmpty(this.detail.getOggetto()) || this.detail.getOggetto().length() < 4 || this.detail.getOggetto().length() >= 1000) {
			sb.append(START_LI).append("L'oggetto deve contenere almeno 4 caratteri e non deve superare i 1000 caratteri.").append(END_LI);
			isErrorPresent = true;
		}
		/** OGGETTO END ****************************************/

		/** NOME FASCICOLO START *********************************************/
		if (StringUtils.isNullOrEmpty(this.detail.getDescrizioneFascicoloProcedimentale()) || this.detail.getDescrizioneFascicoloProcedimentale().length() < 4
				|| this.detail.getDescrizioneFascicoloProcedimentale().length() >= 950) {
			sb.append(START_LI).append("Il nome del fascicolo non deve essere più piccolo di 4 caratteri e non deve supera i 1000 caratteri.").append(END_LI);
			isErrorPresent = true;
		}
		/** NOME FASCICOLO END *********************************************/

		isErrorPresent = handleIterApprovativo(assegnatarioPrincipale, sb, isErrorPresent);

		isErrorPresent = handleIterRagioniere(sb, isErrorPresent);

		/*** DOCUMENTO PRINCIPALE START. ****************************************/
		if (detail.getFormatoDocumentoEnum() != FormatoDocumentoEnum.CARTACEO && StringUtils.isNullOrEmpty(detail.getNomeFile())) {
			sb.append(START_LI).append("Inserire il documento principale").append(END_LI);
			isErrorPresent = true;
		}
		/*** DOCUMENTO PRINCIPALE END. ****************************************/

		/*** INDICE DI CLASSIFICAZIONE START. ****************************************/
		if ((dmd.isInCreazioneUscita() || dmd.isInModificaUscita()) && StringUtils.isNullOrEmpty(detail.getIndiceClassificazioneFascicoloProcedimentale())) {
			sb.append(START_LI).append("Inserire l'indice di classificazione.").append(END_LI);
		}
		/*** INDICE DI CLASSIFICAZIONE END. ****************************************/

		/** NUMERO RDP START ********************************************/
		if (dmd.isRdpPanelVisible() && (detail.getNumeroRDP() == null || detail.getNumeroRDP().intValue() <= 0)) {

			sb.append(START_LI).append("Numero RDP mancante o non valido").append(END_LI);
			isErrorPresent = true;
		}
		/** NUMERO RDP END ********************************************/

		isErrorPresent = handleMailSpedizione(sb, isErrorPresent);

		isErrorPresent = handleAllegati(allegati, sb, isErrorPresent);

		/**
		 * CHECK PER LA FALDONATURA OBBLIGATORIA NELLA CREAZIONE DI UN DOCUMENTO IN
		 * USCITA
		 *********************************************************/
		if (dmd.isInCreazioneUscita() && documentoSRV.isDocumentoFaldonaturaObbligatoria(this.detail.getIdTipologiaDocumento(), this.detail.getIdTipologiaProcedimento())) {

			sb.append(START_LI).append("Faldonatura obbligatoria, selezionare un faldone.").append(END_LI);
			isErrorPresent = true;
		}
		/**
		 * CHECK PER LA FALDONATURA OBBLIGATORIA NELLA CREAZIONE DI UN DOCUMENTO IN
		 * USCITA
		 *********************************************************/

		isErrorPresent = handleAllacci(sb, isErrorPresent);

		isErrorPresent = handleRifStorico(sb, isErrorPresent);

		isErrorPresent = handleRispostaProtocollo(sb, isErrorPresent);

		isErrorPresent = handleMetadataExt(sb, isErrorPresent);

		/**
		 * FLUSSI AUT & ATTO DECRETO (solo destinatari elettronici) - START
		 *********************************************************************************************************/

		isErrorPresent = handleFlussi(documentoSRV, sb, isErrorPresent);

		/**
		 * FLUSSI AUT & ATTO DECRETO (solo destinatari elettronici) - END
		 *********************************************************************************************************/

		sb.append("</ul>");

		esito.put(CHIAVE_ESITO, isErrorPresent);
		if (isErrorPresent) {
			esito.put(CHIAVE_ESITO_TESTO, sb.toString());
		}

		return esito;
	}

	private boolean handleFlussi(final IDocumentoFacadeSRV documentoSRV, final StringBuilder sb, boolean isErrorPresent) {
		RispostaAllaccioDTO allaccioPrincipale = null;
		for (final RispostaAllaccioDTO allaccio : detail.getAllacci()) {
			if (allaccio.isPrincipale()) {
				allaccioPrincipale = allaccio;
			}
		}

		if ((documentoSRV.checkIsFlussoAttoDecretoUCB(utente, detail.getIdTipologiaDocumento().longValue())
				|| documentoSRV.checkIsFlussiAutUCB(null, allaccioPrincipale, utente)) && utente.isUcb() && dmd.isDestinatariVisible()) {

			boolean isDestinatarioCartaceo = false;
			for (final DestinatarioRedDTO d : detail.getDestinatari()) {
				if (MezzoSpedizioneEnum.CARTACEO.equals(d.getMezzoSpedizioneEnum()) || TipologiaDestinatarioEnum.INTERNO.equals(d.getTipologiaDestinatarioEnum())) {
					isDestinatarioCartaceo = true;
					break;
				}
			}

			if (isDestinatarioCartaceo) {
				sb.append(START_LI).append("Inserire solo destinatari esterni e elettronici").append(END_LI);
				isErrorPresent = true;
			}

		}
		return isErrorPresent;
	}

	private boolean handleMetadataExt(final StringBuilder sb, boolean isErrorPresent) {
		/**
		 * METADATI ESTESI - START
		 ********************************************************************************************************/
		if (!CollectionUtils.isEmpty(detail.getMetadatiEstesi())) {
			boolean valoreNonPresente = false;
			boolean obbligatorio = false;
			for (final MetadatoDTO m : detail.getMetadatiEstesi()) {

				obbligatorio = TipoDocumentoModeEnum.SEMPRE.equals(m.getObligatoriness())
						|| (TipoDocumentoModeEnum.SOLO_ENTRATA.equals(m.getObligatoriness()) && (dmd.isInCreazioneIngresso() || dmd.isInModificaIngresso())
								|| (TipoDocumentoModeEnum.SOLO_USCITA.equals(m.getObligatoriness())) && (dmd.isInCreazioneUscita() || dmd.isInModificaUscita()));

				if (obbligatorio) {
					switch (m.getType()) {
					case LOOKUP_TABLE:
						final SelectItemDTO lookupSelect = ((LookupTableDTO) m).getLookupValueSelected();
						valoreNonPresente = (lookupSelect == null || lookupSelect.getValue() == null);
						break;
					case CAPITOLI_SELECTOR:
						valoreNonPresente = ((CapitoloSpesaMetadatoDTO) m).getCapitoloSelected() == null;
						break;
					case PERSONE_SELECTOR:
						valoreNonPresente = CollectionUtils.isEmpty(((AnagraficaDipendentiComponentDTO) m).getSelectedValues());
						break;
					default:
						valoreNonPresente = StringUtils.isNullOrEmpty(m.getValue4AttrExt());
					}

					if (valoreNonPresente) {
						sb.append(START_LI).append("L'attributo esteso " + m.getDisplayNameView() + " è obbligatorio.").append(END_LI);
						isErrorPresent = true;
					}
				}

				if (TipoMetadatoEnum.DOUBLE.equals(m.getType()) && m.getSelectedValue() != null && !ObjectUtils.anyNotNull(m.getSelectedValue())
						&& !NumberUtils.isCreatable((String) m.getSelectedValue())) {
					sb.append(START_LI).append("L'attributo esteso " + m.getDisplayNameView() + " deve essere numerico.").append(END_LI);
					isErrorPresent = true;
				}
			}
		}
		/**
		 * METADATI ESTESI - END
		 *********************************************************************************************************/
		return isErrorPresent;
	}

	private boolean handleRispostaProtocollo(final StringBuilder sb, boolean isErrorPresent) {
		/** IN RISPOSTA AL PROTOCOLLO - START **********************************************************************************************/
		// Se il campo "In risposta al protocollo" non è correttamente valorizzato si blocca l'utente
		if (((detail.getNumeroProtocolloRisposta() != null && !detail.getNumeroProtocolloRisposta().isEmpty())
				|| detail.getAnnoProtocolloRisposta() != null)
				&& ((detail.getNumeroProtocolloRisposta() == null || detail.getNumeroProtocolloRisposta().isEmpty())
						|| detail.getAnnoProtocolloRisposta() == null)) {
			sb.append(START_LI).append("Informazioni mancanti legate al protocollo a cui si sta rispondendo.").append(END_LI);
			isErrorPresent = true;
		} else if (detail.getNumeroProtocolloRisposta() != null && detail.getAnnoProtocolloRisposta() != null
				&& (detail.getNumeroProtocolloRisposta().trim().isEmpty() || detail.getAnnoProtocolloRisposta() <= 0)) {
			sb.append(START_LI).append("Informazioni non corrette legate al protocollo a cui si sta rispondendo.").append(END_LI);
			isErrorPresent = true;
		}
		/** IN RISPOSTA AL PROTOCOLLO - END ************************************************************************************************/
		return isErrorPresent;
	}

	private boolean handleRifStorico(final StringBuilder sb, boolean isErrorPresent) {
		/** RIFERIMENTO STORICO NSD - START ************************************************************************************************************************/
		if (detail.getAllaccioRifStorico() != null && !detail.getAllaccioRifStorico().isEmpty()) {
			for (RiferimentoStoricoNsdDTO rif : detail.getAllaccioRifStorico()) {
				if (!rif.getVerificato()) {
					String id = rif.getNumeroProtocolloNsd() == null ? "" : rif.getNumeroProtocolloNsd().toString(); 
					sb.append(START_LI).append("Il riferimento storico " + id + NON_E_VERIFICATO).append(END_LI);
					isErrorPresent = true;
				}
			}
		}
		/** RIFERIMENTO STORICO NSD - END *****************************************************************************************/
		
		/** ALLACCI NPS - START ************************************************************************************************************************/
		if (detail.getAllaccioRifNps() != null && !detail.getAllaccioRifNps().isEmpty()) {
			for (RiferimentoProtNpsDTO rif : detail.getAllaccioRifNps()) {
				if (!rif.isVerificato()) {
					String id = rif.getNumeroProtocolloNps() == null ? "" : rif.getNumeroProtocolloNps().toString(); 
					sb.append(START_LI).append("Il riferimento nps " + id + NON_E_VERIFICATO).append(END_LI);
					isErrorPresent = true;
				}
			}
		}
		/** ALLACCI NPS - END *****************************************************************************************/
		return isErrorPresent;
	}

	private boolean handleAllacci(final StringBuilder sb, boolean isErrorPresent) {
		/**
		 * ALLACCI - START
		 ************************************************************************************************************************/
		if (detail.getAllacci() != null && !detail.getAllacci().isEmpty()) {
			for (final RispostaAllaccioDTO a : detail.getAllacci()) {
				if (!a.isVerificato()) {
					String id = a.getIdDocumento() == null ? "" : a.getIdDocumento().toString();
					if (a.getNumeroProtocollo() != null && a.getNumeroProtocollo().intValue() > 0 && a.getAnnoProtocollo() > 0) {
						id = a.getNumeroProtocollo().toString() + "/" + a.getAnnoProtocollo();
					}
					sb.append(START_LI).append("Il documento " + id + NON_E_VERIFICATO).append(END_LI);
					isErrorPresent = true;
				}
			}
		}
		/**
		 * ALLACCI - END
		 *****************************************************************************************/
		return isErrorPresent;
	}

	private static boolean handleAllegati(final List<AllegatoDTO> allegati, final StringBuilder sb, boolean isErrorPresent) {
		/**
		 * ALLEGATI - SE CI SONO DEVONO AVERE IL CONTENUTO SE SONO DIGITALI START
		 ****************************************************************/
		if (allegati != null && !allegati.isEmpty()) {
			for (final AllegatoDTO allegatoDTO : allegati) {
				if ((allegatoDTO.getFormatoSelected() == FormatoAllegatoEnum.ELETTRONICO || allegatoDTO.getFormatoSelected() == FormatoAllegatoEnum.FIRMATO_DIGITALMENTE)
						&& StringUtils.isNullOrEmpty(allegatoDTO.getNomeFile())) {
					sb.append(START_LI).append("Il file è obbligatorio per tutti gli allegati elettronici o firmati digitalmente.").append(END_LI);
					isErrorPresent = true;
					break;
				}
			}
		}
		/**
		 * ALLEGATI - SE CI SONO DEVONO AVERE IL CONTENUTO SE SONO DIGITALI END
		 ********************************************************************/
		return isErrorPresent;
	}

	private boolean handleMailSpedizione(final StringBuilder sb, boolean isErrorPresent) {
		/**
		 * INFO MAIL SPEDIZIONE, ALLEGATI ETC START.
		 **********************************************************************************/
		if (!dmd.isTabSpedizioneDisabled() && (this.dmd.isInCreazioneUscita() || this.dmd.isInModificaUscita())
				&& (StringUtils.isNullOrEmpty(this.detail.getMailSpedizione().getOggetto()) || StringUtils.isNullOrEmpty(this.detail.getMailSpedizione().getTesto())
						|| StringUtils.isNullOrEmpty(this.detail.getMailSpedizione().getMittente()))) {
			isErrorPresent = true;

			sb.append("<li>Nel Tab Spedizione: ").append("<ul>");

			if (StringUtils.isNullOrEmpty(this.detail.getMailSpedizione().getOggetto())) {
				sb.append(START_LI).append("Non è presente l'oggetto.").append(END_LI);
			}
			if (StringUtils.isNullOrEmpty(this.detail.getMailSpedizione().getTesto())) {
				sb.append(START_LI).append("Non è presente il testo della mail.").append(END_LI);
			}
			if (StringUtils.isNullOrEmpty(this.detail.getMailSpedizione().getMittente())) {
				sb.append(START_LI).append("Non è presente il mittente.").append(END_LI);
			}

			sb.append("</ul>").append(END_LI);
		}
		/**
		 * INFO MAIL SPEDIZIONE, ALLEGATI ETC END.
		 ************************************************************************************************/
		return isErrorPresent;
	}

	private boolean handleIterRagioniere(final StringBuilder sb, boolean isErrorPresent) {
		/***
		 * ITER FIRMA RAGIONIERE - CONTROLLO SUI DESTINATARI OMOGENEI START.
		 ****************************************/
		if (this.dmd.isIterDocUscitaVisible() && this.detail.getIdIterApprovativo().intValue() == IterApprovativoDTO.ITER_FIRMA_RAGIONIERE.intValue()
				&& this.detail.getDestinatari().size() > 1) {
			MezzoSpedizioneEnum m = null;
			for (final DestinatarioRedDTO d : this.detail.getDestinatari()) {
				
				if (d.getMezzoSpedizioneEnum() != null && m != null && d.getMezzoSpedizioneEnum() != m
						&& this.detail.getIdIterApprovativo().intValue() == IterApprovativoDTO.ITER_FIRMA_RAGIONIERE.intValue()
								&& Boolean.FALSE.equals(this.detail.getCheckFirmaDigitaleRGS())) {
					// se si tratta di Firma ragioniere generale dello stato (3) senza flag firma
					// digitale RGS
					sb.append(START_LI).append(SalvaDocumentoErroreEnum.DOC_FLUSSO_RGS_DESTINATARI_MISTI.getMessaggio()).append(END_LI);
					isErrorPresent = true;
					break;
				}

				if (d.getMezzoSpedizioneEnum() != null && d.getMezzoSpedizioneEnum().equals(MezzoSpedizioneEnum.CARTACEO) && d.getTipologiaDestinatarioEnum() != null
						&& d.getTipologiaDestinatarioEnum().equals(TipologiaDestinatarioEnum.ESTERNO) && Boolean.TRUE.equals(this.detail.getCheckFirmaDigitaleRGS())) {
					sb.append(START_LI).append(SalvaDocumentoErroreEnum.DOC_CHECK_FIRMA_RGS_DESTINATARI_CARTACEI.getMessaggio()).append(END_LI);
					isErrorPresent = true;
					break;
				}

				if (d.getMezzoSpedizioneEnum() != null) {
					m = d.getMezzoSpedizioneEnum();
				}
			}
		}
		/***
		 * ITER FIRMA RAGIONIERE - CONTROLLO SUI DESTINATARI OMOGENEI END.
		 ****************************************/
		return isErrorPresent;
	}

	private boolean handleIterApprovativo(final AssegnazioneDTO assegnatarioPrincipale, final StringBuilder sb, boolean isErrorPresent) {
		/**
		 * ITER APPROVATIVO MANUALE START
		 *****************************************************************/
		if (assegnatarioPrincipale == null) {
			if ((this.dmd.isInCreazioneUscita() || this.dmd.isInModificaUscita())
					&& this.detail.getIdIterApprovativo().intValue() == IterApprovativoDTO.ITER_FIRMA_MANUALE.intValue()) {
				sb.append(START_LI).append("Inserire l'assegnatario").append(END_LI);
				isErrorPresent = true;
			} else if (this.dmd.isInCreazioneIngresso()) {
				sb.append(START_LI).append("Inserire l'assegnatario per competenza.").append(END_LI);
				isErrorPresent = true;
			}
		} else if (this.dmd.isInCreazioneIngresso()) {
			final List<String> listUffUte = new ArrayList<>();

			for (final AssegnazioneDTO asse : detail.getAssegnazioni()) {
				final Long idAsseUff = asse.getUfficio().getId();
				final Long idAsseUte = (asse.getUtente() == null || asse.getUtente().getId() == null) ? 0L : asse.getUtente().getId();

				final String asseIdUffUte = idAsseUff.longValue() + "-" + idAsseUte.longValue();
				if (listUffUte.contains(asseIdUffUte)) {
					sb.append(START_LI).append("Assegnazioni duplicate.").append(END_LI);
					isErrorPresent = true;
					break;
				}

				listUffUte.add(asseIdUffUte);
			}
		}
		/**
		 * ITER APPROVATIVO MANUALE END
		 *****************************************************************/
		return isErrorPresent;
	}

	private boolean handleDestinatari(final StringBuilder sb, boolean isErrorPresent) {
		/**
		 * DESTINATARI START
		 *******************************************************************/
		if (dmd.isDestinatariVisible()) { 
			if (detail.getDestinatari() == null || detail.getDestinatari().isEmpty()) {
				sb.append(START_LI).append("Inserire almeno un destinatario.").append(END_LI);
				isErrorPresent = true;
			} else {
				boolean isDestinatarioInTOpresent = false;
				boolean checkContattoOnTheFlyFlag = true;
				boolean checkContattoCartaceo = true;
				boolean flagContinua = true;
				
				for (DestinatarioRedDTO d : detail.getDestinatari()) {
					if(MezzoSpedizioneEnum.ELETTRONICO.equals(d.getMezzoSpedizioneEnum())) {
						checkContattoOnTheFlyFlag = checkContattoOnTheFly(d.getContatto());
						
					} else if(TipologiaDestinatarioEnum.INTERNO.equals(d.getTipologiaDestinatarioEnum()) || 
							MezzoSpedizioneEnum.CARTACEO.equals(d.getMezzoSpedizioneEnum())){
						checkContattoCartaceo = checkContattoCartaceo(d.getContatto());
					}
					
					if(!checkContattoOnTheFlyFlag) {
						break;
					}
					
					if (d.getModalitaDestinatarioEnum() == ModalitaDestinatarioEnum.TO && 
							flagContinua) {
						isDestinatarioInTOpresent = true;
						flagContinua = false;
					}
				}

				if (!isDestinatarioInTOpresent) {
					sb.append(START_LI).append("Inserire almeno un destinatario in TO.").append(END_LI);
					isErrorPresent = true;
				}
				
				if(!checkContattoCartaceo) {
					sb.append(START_LI).append("Inserire l'alias del destinatario").append(END_LI);
					isErrorPresent = true;
				}
				
				if(!checkContattoOnTheFlyFlag) {
					sb.append(START_LI).append("Il formato della mail del destinatario non è corretto").append(END_LI);
					isErrorPresent = true;
				}

			}  
		}
		/**
		 * DESTINATARI END
		 *******************************************************************/
		return isErrorPresent;
	}

	private static boolean checkContattoOnTheFly(final Contatto contattoOnTheFly) {
		boolean isValido = true;
		if (contattoOnTheFly != null) {
			final boolean formatoOk = EmailUtils.checkFormatoPecPeo(contattoOnTheFly.getMailSelected());
			final boolean campiKo = contattoOnTheFly.getAliasContatto() == null || StringUtils.isNullOrEmpty(contattoOnTheFly.getAliasContatto()) || StringUtils.isNullOrEmpty(contattoOnTheFly.getMailSelected());
			if (campiKo || !formatoOk) {
				isValido = false;
			}
		}
		return isValido;
	}
	
	private static boolean checkContattoCartaceo(Contatto contattoOnTheFly) {
		boolean isValido = true;
		if(contattoOnTheFly != null) {
			boolean campiKo = StringUtils.isNullOrEmpty(contattoOnTheFly.getAliasContatto());
			if(campiKo) {
				isValido = false;
			}
		}
		return isValido;
	}

}
