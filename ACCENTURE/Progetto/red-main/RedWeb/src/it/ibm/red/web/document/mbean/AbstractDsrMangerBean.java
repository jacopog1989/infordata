package it.ibm.red.web.document.mbean;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.mbean.component.DsrPresentazioneBean;
import it.ibm.red.web.document.mbean.interfaces.IDsrPresentazioneBean;
import it.ibm.red.web.document.mbean.interfaces.IDsrTabGenerale;
import it.ibm.red.web.document.mbean.interfaces.ITabAllegatiBean;
import it.ibm.red.web.document.mbean.interfaces.ITabFascicoliBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;

/**
 * Bean condiviso da tutte le implementazioni dell'intefarccia di protocollazione e modifica, serve a forzare il creatore di futre implementazioni a rendersi coerente con l'
 * Anche un interfaccia avvrebbe avuto più o meno lo stesso effetto 
 * ma visto che sono molto confidente che non sia necessaria una ulteriore implementazione di questo codice l'ho messo in comune.
 *
 */
public abstract class AbstractDsrMangerBean extends AbstractBean {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6256094267067357716L;

	/**
	 * Utente.
	 */
	private UtenteDTO utente;
	
	/**
	 * Questo bean è valorizzato inizialmente nel MailBean con questi valori.
	 * 
	 * Allegati (No content)
	 *		nomeFile mimetype formatoELETTRONICO 
	 *		FILE PRINCIPALE
	 * nomeFile principale
	 * mimetype 
	 * content  anche questo potrebbe essere ricavato
	 *	PROTOCOLLAZIONE
	 * protocollazioneMailGuid
	 * mittenteContatto
	 *
	 *		// isNotifica
	 *		// idCategoriaDocumento
	 */
	private DetailDocumentRedDTO detail;
	
	/**
	 * Tab generico.
	 */
	private IDsrTabGenerale tabGenerale;
	
	/**
	 * Tab fascicoli.
	 */
	private ITabFascicoliBean tabFascicoli;
	
	/**
	 * Tab allegati.
	 */
	private ITabAllegatiBean tabAllegati;
	
	/**
	 * Presentazione.
	 */
	private IDsrPresentazioneBean presentazione;

	/**
	 * @see it.ibm.red.web.mbean.AbstractBean#postConstruct().
	 */
	@Override
	public void postConstruct() {
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
	}
	
	/**
	 * Questo metodo è utilizzato per settare tutti i dettagli da mostrare. Si
	 * occupa minoltre di settare detail alla classe per conservare la coerenza
	 * 
	 * Si suppone di invocare questo metodo al fine di reinizializzare il bean,
	 * prima che il bean venga visualizzato
	 * 
	 * @param detail
	 */
	protected void initializeManagerBean(final DetailDocumentRedDTO detail) {
		this.detail = detail;
		tabGenerale = new DsrDataTabGeneraleBean(detail, utente);
		tabFascicoli = new DsrDataTabFascicoliBean(detail);
		tabAllegati = new DsrDataTabAllegatiBean(detail, utente);
		presentazione = new DsrPresentazioneBean(detail, false);
	}

	/**
	 * Getter del dettaglio del documento.
	 * 
	 * @return DetailDocumentRedDTO, DTO che contiene tutte le informazioni sul Detail di un documento
	 * */
	public DetailDocumentRedDTO getDetail() {
		return detail;
	}

	/**
	 * Setter del dettaglio del documento.
	 * 
	 * @param detail Il dettaglio da impostare
	 * */
	protected void setDetail(final DetailDocumentRedDTO detail) {
		this.detail = detail;
	}

	/**
	 * Getter dell'attributo tabeGenerale.
	 * 
	 * @return tabGenerale
	 * */
	public IDsrTabGenerale getTabGenerale() {
		return tabGenerale;
	}

	/**
	 * Imposta il tab generale.
	 * 
	 * @param tabGenerale
	 *            tab generale da impostare
	 */
	protected void setTabGenerale(final IDsrTabGenerale tabGenerale) {
		this.tabGenerale = tabGenerale;
	}

	/**
	 * Getter dell'attributo tabFascicoli.
	 * 
	 * @return tabFascicoli
	 * */
	public ITabFascicoliBean getTabFascicoli() {
		return tabFascicoli;
	}

	/**
	 * Imposta il tab dei fascicoli.
	 * 
	 * @param tabFascicoli
	 *            tab dei fascicoli da impostare
	 */
	protected void setTabFascicoli(final ITabFascicoliBean tabFascicoli) {
		this.tabFascicoli = tabFascicoli;
	}

	/**
	 * Getter dell'attributo tabAllegati.
	 * 
	 * @return tabAllegati
	 * */
	public ITabAllegatiBean getTabAllegati() {
		return tabAllegati;
	}

	/**
	 * Setter dell'attributo tabAllegati.
	 * 
	 * @param tabAllegati
	 * */
	protected void setTabAllegati(final ITabAllegatiBean tabAllegati) {
		this.tabAllegati = tabAllegati;
	}

	/**
	 * Getter presentazione.
	 * 
	 * @return il Bean IDsrPresentazioneBean
	 * */
	public IDsrPresentazioneBean getPresentazione() {
		return presentazione;
	}

	/**
	 * Imposta la presentazione.
	 * 
	 * @param presentazione
	 *            presentazione da impostare
	 */
	protected void setPresentazione(final IDsrPresentazioneBean presentazione) {
		this.presentazione = presentazione;
	}

	/**
	 * Restituisce l'utente.
	 * 
	 * @return utente
	 */
	protected UtenteDTO getUtente() {
		return utente;
	}
	
}	