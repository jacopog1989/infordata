package it.ibm.red.web.report.mbean.component;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.dto.ReportDetailStatisticheUfficioUtenteDTO;
import it.ibm.red.business.dto.ReportMasterStatisticheUfficioUtenteDTO;
import it.ibm.red.business.enums.TargetNodoReportEnum;
import it.ibm.red.business.enums.TipoStatisticaUfficioReportEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IReportFacadeSRV;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.helper.faces.MessageSeverityEnum;

/**
 * Component che gestisce i risultati del report statistiche uffici.
 */
public class RisultatiStatisticheUfficiReportComponent implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
    /**
     * Logger.
     */
	private static final REDLogger LOGGER = REDLogger.getLogger(RisultatiStatisticheUfficiReportComponent.class);
	
    /**
     * Report service.
     */
	private final IReportFacadeSRV reportSRV;
	
    /**
     * Struttura.
     */
	private final TargetNodoReportEnum tipoStruttura;
	
	/**
	 * Lista da visualizzare nell'anteprima
	 */
	private List<ReportMasterStatisticheUfficioUtenteDTO> masterList;
	
	/**
	 * Lista da visualizzare eventualmente nel dettaglio
	 */
	private List<ReportDetailStatisticheUfficioUtenteDTO> detailList;
	
	/**
	 * Descrizione sopra alla tabella
	 */
	private String detailTableHeader;
	
	/**
	 * Descrizione operazione selezionata dall'utente 
	 */
	private String descrizioneOperazione;
	
	/**
	 * Costruttore, inizilizza i service.
	 * @param tipoStruttura
	 */
	public RisultatiStatisticheUfficiReportComponent(final TargetNodoReportEnum tipoStruttura) {
		reportSRV = ApplicationContextProvider.getApplicationContext().getBean(IReportFacadeSRV.class);
		this.tipoStruttura = tipoStruttura;
	}
	
	/**
	 * Restituisce il tipo struttura.
	 * @return tipo struttura
	 */
	public TargetNodoReportEnum getTipoStruttura() {
		return tipoStruttura;
	}
	
	/**
	 * Gestisce la visualizzazione del dettaglio del master.
	 * @param master
	 * @param tipoEstrazione
	 */
	public void showDetail(final ReportMasterStatisticheUfficioUtenteDTO master, final TipoStatisticaUfficioReportEnum tipoEstrazione) {
		try {
			final String descrUfficio = master.getUfficio().getDescrizione();
			final String descrUsername = master.getUtente() != null ? master.getUtente().getCognome() + " " + master.getUtente().getNome() : "";
			if (StringUtils.isNotBlank(descrUsername)) {
				this.detailTableHeader = descrUfficio + " - " + descrUsername + ": " + tipoEstrazione.getDescrizione();
			} else {
				this.detailTableHeader = descrUfficio + ": " + tipoEstrazione.getDescrizione();
			}
			
			this.descrizioneOperazione = tipoEstrazione.name();
			
			final ReportDetailStatisticheUfficioUtenteDTO form = 
					new ReportDetailStatisticheUfficioUtenteDTO(master.getStruttura(), master.getTipoStruttura(), 
							master.getAnno(), master.getDataDa(), master.getDataA(), tipoEstrazione);
			this.detailList = reportSRV.getStatisticheUfficiDetail(form);
		} catch (final Exception e) {
			LOGGER.error("Errore nel componente per il recupero dei report", e);
			FacesHelper.showMessage(null, "Errore durante il recupero del report", MessageSeverityEnum.ERROR);
		}
	}

	/**
	 * Restitusice la lista dei master.
	 * @return lista master
	 */
	public List<ReportMasterStatisticheUfficioUtenteDTO> getMasterList() {
		return masterList;
	}

	/**
	 * Restituisce la lista dei detail.
	 * @return lista detail
	 */
	public List<ReportDetailStatisticheUfficioUtenteDTO> getDetailList() {
		return detailList;
	}

	/**
	 * Restituisce l'header del tab dettaglio.
	 * @return header detail tab
	 */
	public String getDetailTableHeader() {
		return detailTableHeader;
	}

	/**
	 * Restituisce la descrizione dell'operazione.
	 * @return descrizione operazione
	 */
	public String getDescrizioneOperazione() {
		return descrizioneOperazione;
	}

	/**
	 * Imposta la descrizione dell'operazione.
	 * @param descrizioneOperazione
	 */
	public void setDescrizioneOperazione(final String descrizioneOperazione) {
		this.descrizioneOperazione = descrizioneOperazione;
	}

	/**
	 * Imposta la lista dei master.
	 * @param masterList
	 */
	public void setMasterList(final List<ReportMasterStatisticheUfficioUtenteDTO> masterList) {
		this.masterList = masterList;
	}
	
}