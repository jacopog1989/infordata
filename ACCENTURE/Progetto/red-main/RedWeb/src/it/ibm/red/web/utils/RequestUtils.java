package it.ibm.red.web.utils;

import javax.servlet.http.HttpServletRequest;

import it.ibm.red.business.utils.StringUtils;

/**
 * The Class RequestUtils.
 *
 * @author CPIERASC
 * 
 *         Utility per la gestione della request.
 */
public final class RequestUtils {

	private static final String OPERA = "opera";
	private static final String VERSION = "version";
	/**
	 * Posizione RV.
	 */
	private static final int IE_RV = 3;

	/**
	 * Costruttore.
	 */
	private RequestUtils() {
	}

	/**
	 * Restituisce l'ip del chiamante.
	 * 
	 * @param request	la request
	 * @return			l'ip del chiamante
	 */
	public static String getIp(final HttpServletRequest request) {
		String ipAddress = request.getHeader("X-FORWARDED-FOR");  
		if (ipAddress == null) {  
			ipAddress = request.getRemoteAddr();  
		}
		return ipAddress;
	}
    
	/**
	 * Restituisce il browser in uso.
	 * 
	 * @param request	la request
	 * @return			il browser in uso
	 */
    public static String getBrowser(final HttpServletRequest request) {
    	String userAgent = getUserAgent(request);
    	String browser = null;
    	if (!StringUtils.isNullOrEmpty(userAgent)) {
            if (userAgent.contains("msie")) {
                String substring = userAgent.substring(userAgent.indexOf("msie")).split(";")[0];
                browser = substring.split(" ")[0].replace("msie", "ie") + "-" + substring.split(" ")[1];
            } else if (userAgent.contains("safari") && userAgent.contains(VERSION)) {
                browser = (userAgent.substring(userAgent.indexOf("safari")).split(" ")[0]).split("/")[0] + "-" 
                		+ (userAgent.substring(userAgent.indexOf(VERSION)).split(" ")[0]).split("/")[1];
            } else if (userAgent.contains("opr") || userAgent.contains(OPERA)) {
                if (userAgent.contains(OPERA)) {
                    browser = (userAgent.substring(userAgent.indexOf(OPERA)).split(" ")[0]).split("/")[0] + "-" 
                    		+ (userAgent.substring(userAgent.indexOf(VERSION)).split(" ")[0]).split("/")[1];
                } else if (userAgent.contains("opr")) {
                    browser = ((userAgent.substring(userAgent.indexOf("opr")).split(" ")[0]).replace("/", "-")).replace("opr", OPERA);
                }
            } else if (userAgent.contains("chrome")) {
                browser = (userAgent.substring(userAgent.indexOf("chrome")).split(" ")[0]).replace("/", "-");
            } else if ((userAgent.indexOf("mozilla/7.0") > -1) || (userAgent.indexOf("netscape6") != -1)  
            		|| (userAgent.indexOf("mozilla/4.7") != -1) || (userAgent.indexOf("mozilla/4.78") != -1) 
            		|| (userAgent.indexOf("mozilla/4.08") != -1) || (userAgent.indexOf("mozilla/3") != -1)) {
                browser = "netscape-?";
            } else if (userAgent.contains("firefox")) {
                browser = (userAgent.substring(userAgent.indexOf("firefox")).split(" ")[0]).replace("/", "-");
            } else if (userAgent.contains("rv")) {
                browser = "ie-" + userAgent.substring(userAgent.indexOf("rv") + IE_RV, userAgent.indexOf(')'));
            } else {
                browser = "unKnown, more-info: " + userAgent;
            }
    	}
    	return browser;
    }

    /**
     * Restituisce lo user agent della request.
     * 
     * @param request	la request
     * @return			lo user agent
     */
	public static String getUserAgent(final HttpServletRequest request) {
		return request.getHeader("User-Agent").toLowerCase();
	}
    
	/**
     * Restituisce il sistema operativo in uso.
     * 
     * @param request	la request
     * @return			il sistema operativo in uso
     */
    public static String getOS(final HttpServletRequest request) {
    	String userAgent = getUserAgent(request);
    	String os = null;
    	if (!StringUtils.isNullOrEmpty(userAgent)) {
        	if (userAgent.toLowerCase().indexOf("windows") >= 0) {
        		os = "Windows";
        	} else if (userAgent.toLowerCase().indexOf("mac") >= 0) {
        		os = "Mac";
        	} else if (userAgent.toLowerCase().indexOf("x11") >= 0) {
        		os = "Unix";
        	} else if (userAgent.toLowerCase().indexOf("android") >= 0) {
        		os = "Android"; 
        	} else if (userAgent.toLowerCase().indexOf("iphone") >= 0) {
        		os = "IPhone";
        	} else { 
        		os = "UnKnown, More-Info: " + userAgent;
        	}
    	}
    	return os;
    }
}
