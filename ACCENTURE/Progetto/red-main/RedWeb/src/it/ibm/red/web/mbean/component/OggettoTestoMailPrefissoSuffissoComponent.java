package it.ibm.red.web.mbean.component;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.dto.TestoPredefinitoDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.document.mbean.component.AbstractComponent;

/**
 * Compoenente che gestisce le interazioni tra l'oggetto e il testo di una mail.
 *
 * Gestisce inoltre la possibilità di inserire un testo e un oggetto di default recuperato da una select.
 *
 * @author a.difolca
 */
public class OggettoTestoMailPrefissoSuffissoComponent extends AbstractComponent {

	private static final long serialVersionUID = -7678905009745037837L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(OggettoTestoMailPrefissoSuffissoComponent.class);

	/**
	 * Massima lunghezza oggetto.
	 */
	private static final Integer MAX_LEN_OGGETTO = 100;
	
	/**
	 * Massima lunghezza testo.
	 */
	private static final Integer MAX_LEN_TESTO = 2000;

	/**
	 * Gestisce i testi predefiniti per l'utente.
	 */
	private List<TestoPredefinitoDTO> testiPredefinitiList;

	/**
	 * Utilizzato da primafaces per recuperare la selezione dell'utente.
	 */
	private Long idTestoPredefinito;
	
	/**
	 * Index non selezionato.
	 */
	private static final Long NOT_SELECTED = -1L;

	/**
	 * Oggetto dell'email.
	 */
	private String oggetto;

	/**
	 * Prefisso oggetto di default.
	 */
	private String defaultPrefissoOggetto;

	/**
	 * Corpo dell'email.
	 */
	private String body;

	/**
	 * Sufficcio testo di default.
	 */
	private String defaultSuffissoTesto;

	/**
	 * Costruttore di default.
	 * @param inTestiPredefinitiList
	 * @param inDefaultOggetto
	 * @param inDefaultSuffissoTesto
	 */
	public OggettoTestoMailPrefissoSuffissoComponent(final List<TestoPredefinitoDTO> inTestiPredefinitiList, final String inDefaultOggetto) {
		this.testiPredefinitiList = inTestiPredefinitiList;
		this.defaultPrefissoOggetto = inDefaultOggetto;
		if (StringUtils.isBlank(defaultPrefissoOggetto)) {
			throw new RedException("Non risulta inizializzato il documento di riferimento per la resposne INOLTRA MAIL: 'master is null'");
		}
	}

	/**
	 * Siccome primefaces non consente di pescare l'oggetto come chiave dei select item tocca a noi recuperare l'oggetto in base al value.
	 * In realtà si può usare un converter per fare questa operazione ma così si scrive di meno.
	 * @return selectedTesto
	 */
	private TestoPredefinitoDTO getSelectedTesto() {
		TestoPredefinitoDTO selectedTesto = null;
		if (idTestoPredefinito != null) {
			final Optional<TestoPredefinitoDTO> optional = this.testiPredefinitiList.stream()
			.filter(m -> idTestoPredefinito.equals(m.getIdTestoPredefinito()))
			.findFirst();
			if (optional.isPresent()) {
				selectedTesto = optional.get();
			}
		}
		return selectedTesto;
	}

	/**
	 * Gestisce il cambiamento del testo predefinito.
	 */
	public void onChangeTestoPredefinito() {
		try {
			if (!NOT_SELECTED.equals(this.idTestoPredefinito)) {				
				final TestoPredefinitoDTO selectedTesto = getSelectedTesto();
				if (selectedTesto == null) {
					showErrorMessage("Si e' verificato un errore si prega di ricaricare la pagina.");
					throw new RedException("Durante l'evento di onchange il teto selezionato risulta essere nullo");
				}
				oggetto = selectedTesto.getOggetto();
				body = selectedTesto.getCorpotesto();
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showErrorMessage("Si è verificato un errore si prega di ricaricare la pagina.");
		}
	}

	/**
	 * Validazione oggetto della mail.
	 * @return
	 */
	public boolean validate() {
		boolean valid = true;
		final String completeOggetto = getCompleteOggetto();
		if (StringUtils.isBlank(completeOggetto)) {
			valid = false;
			showWarnMessage("Attenzione, l'oggetto dell'e-mail è obbligatorio.");
		} else if (completeOggetto.length() > MAX_LEN_OGGETTO) {
			valid = false;
			showWarnMessage("Attenzione, l'oggetto ha superato il numero massimo di caratteri consentiti.");
		}
		final String completeBody = getCompleteBody();
		if (StringUtils.isBlank(completeBody)) {
			valid = false;
			showWarnMessage("Attenzione, il testo dell'e-mail è obbligatorio.");
		} else if (completeBody.length() > MAX_LEN_TESTO) {
			valid = false;
			showWarnMessage("Attenzione, il testo ha superato il numero massimo di caratteri consentiti.");
		}
		return valid;
	}

	/**
	 * Reset oggetto della mail.
	 */
	public void reset() {
		defaultPrefissoOggetto = null;
		oggetto = null;
		idTestoPredefinito = NOT_SELECTED;
	}

	/**
	 * Restituisce l'oggetto completo.
	 * @return completeOggetto
	 */
	public String getCompleteOggetto() {
		return StringUtils.join(this.defaultPrefissoOggetto, " ", this.oggetto);
	}

	/**
	 * Restituisce il corpo dell'email completo.
	 * @return completeBody
	 */
	public String getCompleteBody() {
		return StringUtils.join(body, " ", this.defaultSuffissoTesto);
	}

	/**
	 * Restituisce la lista di testi predefiniti.
	 * @return testiPredefinitiList
	 */
	public List<TestoPredefinitoDTO> getTestiPredefinitiList() {
		return testiPredefinitiList;
	}

	/**
	 * Imposta la lista di testi predefiniti.
	 * @param testiPredefinitiList
	 */
	public void setTestiPredefinitiList(final List<TestoPredefinitoDTO> testiPredefinitiList) {
		this.testiPredefinitiList = testiPredefinitiList;
	}

	/**
	 * Restituisce  l'id del testo predefinito.
	 * @return idTestoPredefinito
	 */
	public Long getIdTestoPredefinito() {
		return idTestoPredefinito;
	}

	/**
	 * Imposta l'id del testo predefinito.
	 * @param idTestoPredefinito
	 */
	public void setIdTestoPredefinito(final Long idTestoPredefinito) {
		this.idTestoPredefinito = idTestoPredefinito;
	}

	/**
	 * @return oggetto dell'email.
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Imposta l'oggetto dell'email.
	 * @param oggetto
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * Restituisce il corpo dell'email.
	 * @return body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * Imposta il corpo dell'email
	 * @param body
	 */
	public void setBody(final String body) {
		this.body = body;
	}

	/**
	 * @return defaultPrefissoOggetto
	 */
	public String getDefaultPrefissoOggetto() {
		return defaultPrefissoOggetto;
	}

	/**
	 * @return defaultSuffissoTesto
	 */
	public String getDefaultSuffissoTesto() {
		return defaultSuffissoTesto;
	}
}
