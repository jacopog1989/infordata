package it.ibm.red.web.mbean;

import java.util.List;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IFaldoneFacadeSRV;
import it.ibm.red.business.service.facade.IFascicoloFacadeSRV;
import it.ibm.red.web.document.mbean.component.DettaglioFaldoneRedComponent;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Bean dettaglio faldone.
 */
public class DettaglioFaldoneAbstractBean extends AbstractBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -3632398376225486458L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DettaglioFaldoneAbstractBean.class);
	
	/**
	 * Informazioni utente.
	 */
	protected UtenteDTO utente;
	
	/**
	 * Service.
	 */
	protected IFaldoneFacadeSRV faldoneSRV;
	
	/**
	 * Service.
	 */
	protected IFascicoloFacadeSRV fascicoloSRV;
	
	/**
	 * Componente.
	 */
	private DettaglioFaldoneRedComponent faldoneCmp;

	/**
	 * Post construct del bean.
	 */
	@Override
	protected void postConstruct() {
		final SessionBean sessionBean = FacesHelper.getManagedBean(Constants.ManagedBeanName.SESSION_BEAN);
		utente = sessionBean.getUtente();
		
		faldoneSRV = ApplicationContextProvider.getApplicationContext().getBean(IFaldoneFacadeSRV.class);
		fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloFacadeSRV.class);
		
		faldoneCmp = new DettaglioFaldoneRedComponent(utente);
	}

	/**
	 * Imposta il dettaglio del faldone identificato dal parametro in ingresso.
	 * @param f
	 */
	public void setDetail(final FaldoneDTO f) {
		try {
			faldoneCmp.setDetail(f);

			/** FASCICOLI START*************************************************/
			final List<FascicoloDTO> list = fascicoloSRV.getFascicoliByFaldoneAbsolutePath(f.getAbsolutePath(), utente);
			faldoneCmp.setFascicoli(list);
			/** FASCICOLI END *************************************************/
			faldoneCmp.setFaldoniFigli(faldoneSRV.getFaldoni(utente, utente.getIdUfficio().intValue(), f.getDocumentTitle()));
			
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Errore nel caricamento del dettaglio faldone.");
		}
	}

	/**
	 * Effettua un reset sul dettaglio.
	 */
	public void unsetDetail() {
		faldoneCmp = new DettaglioFaldoneRedComponent(utente);
	}

	/**
	 * Restituisce il component che gestisce il dettaglio dei faldoni.
	 * @return il componente
	 */
	public DettaglioFaldoneRedComponent getFaldoneCmp() {
		return faldoneCmp;
	}

	/**
	 * Imposta il component che gestisce il dettaglio dei faldoni.
	 * @param faldoneCmp
	 */
	public void setFaldoneCmp(final DettaglioFaldoneRedComponent faldoneCmp) {
		this.faldoneCmp = faldoneCmp;
	}

}
