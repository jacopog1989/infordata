package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;

import it.ibm.red.business.dto.DecretoDirigenzialeDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IFepaFacadeSRV;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.mbean.AbstractBean;

/**
 * Bean che gestisce il tab disponibili dell'associazione decreti.
 */
public class AssociaDecreti_DisponibiliTabBean extends AbstractBean {

	/**
	 * UID.
	 */
	private static final long serialVersionUID = -6280045783588377955L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AssociaDecreti_DisponibiliTabBean.class.getName());

	/**
	 * Info utente.
	 */
	private final UtenteDTO utente;
	
	/**
	 * Datatable decreti.
	 */
	private SimpleDetailDataTableHelper<DecretoDirigenzialeDTO> decretiDTH;
	 

	/**
	 * Costruttore di classe.
	 * @param utente
	 * @param docSel
	 */
	public AssociaDecreti_DisponibiliTabBean(final UtenteDTO utente, final MasterDocumentRedDTO docSel) {
		this.utente = utente;
		decretiDTH = new SimpleDetailDataTableHelper<>(recuperaDecreti(docSel));
		
		decretiDTH.setSelectedMasters(new ArrayList<>());
		
		
		for (final DecretoDirigenzialeDTO decreto : decretiDTH.getMasters()) {
			if (decreto.getSelected() != null && decreto.getSelected()) {
				decretiDTH.getSelectedMasters().add(decreto);
			}
		}
		
		
	}
	
	private List<DecretoDirigenzialeDTO> recuperaDecreti(final MasterDocumentRedDTO docSel) {
		List<DecretoDirigenzialeDTO> decreti = new ArrayList<>();
		try {
			final IFepaFacadeSRV fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);
			decreti = fepaSRV.getDecretiDaLavorare(docSel.getWobNumber(), utente);
			
			
		} catch (final Exception e) {
			LOGGER.error(e);
		}
		return decreti;
	}
	
	/*####### Gestione selezione singola e multipla della tabella #######*/
	/**
	 * Gestisce le check box associate a tutti i decreti disponibili.
	 */
	public void handleAllCheckBoxDecretiDisponibili() {
		handleAllCheckBox(decretiDTH);
	}

	/**
	 * Esegue un update sui decreti disponibili.
	 * @param event
	 */
	public void updateCheckDecretiDisponibili(final AjaxBehaviorEvent event) {
		updateCheck(event, decretiDTH);
	}

	/**
	 * Gestisce tutte le check box.
	 * 
	 * @param dth
	 */
	private void handleAllCheckBox(final SimpleDetailDataTableHelper<DecretoDirigenzialeDTO> dth) {
		super.handleAllCheckBoxGeneric(dth);
	}
	
	private DecretoDirigenzialeDTO updateCheck(final AjaxBehaviorEvent event, final SimpleDetailDataTableHelper<DecretoDirigenzialeDTO> dth) {

		final DecretoDirigenzialeDTO masterChecked = getDataTableClickedRow(event);
		final Boolean newValue = ((SelectBooleanCheckbox) event.getSource()).isSelected();
		if (newValue != null && newValue) {
			dth.getSelectedMasters().add(masterChecked);
		} else {
			dth.getSelectedMasters().remove(masterChecked);
		}
		
		return masterChecked;
	}
	/*############ Getter e setter ###############*/
	/**
	 * Restituisce l'helper per il datatable.
	 * @return decretiDTH
	 */
	public SimpleDetailDataTableHelper<DecretoDirigenzialeDTO> getDecretiDTH() {
		return decretiDTH;
	}

	/**
	 * Imposta l'helper del datatable dei decreti.
	 * @param decretiDTH
	 */
	public void setDecretiDTH(final SimpleDetailDataTableHelper<DecretoDirigenzialeDTO> decretiDTH) {
		this.decretiDTH = decretiDTH;
	}

	@Override
	protected void postConstruct() {
		// Non occorre fare niente nel post construct del bean.
	}
	
}
