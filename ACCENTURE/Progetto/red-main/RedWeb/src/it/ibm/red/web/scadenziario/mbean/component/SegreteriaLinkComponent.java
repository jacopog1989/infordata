package it.ibm.red.web.scadenziario.mbean.component;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;

import it.ibm.red.business.dto.CountPer;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IScadenziaroStrutturaFacadeSRV;
import it.ibm.red.web.scadenziario.mbean.ScadenziarioBean;

/**
 * Component link segreteria.
 */
public class SegreteriaLinkComponent implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Bean gestione scadenze.
	 */
	private final ScadenziarioBean bean;

	/**
	 * Map conteggio ispettorato.
	 */
	private final Map<CountPer<UfficioDTO>, IspettoratoLinkComponent> segreteria2ispettorato;

	/**
	 * Costruttore del component.
	 * 
	 * @param bean
	 */
	public SegreteriaLinkComponent(final ScadenziarioBean bean) {
		this.bean = bean;
		final IScadenziaroStrutturaFacadeSRV scadenziarioSRV = ApplicationContextProvider.getApplicationContext().getBean(IScadenziaroStrutturaFacadeSRV.class);
		final Map<CountPer<UfficioDTO>, List<CountPer<UfficioDTO>>> inSegreteria2Ispettorato = scadenziarioSRV
				.retrieveCountDocumentoInScadenzaPerUfficiDaSegreteria(bean.getUtente());

		segreteria2ispettorato = new HashMap<>();

		for (final Entry<CountPer<UfficioDTO>, List<CountPer<UfficioDTO>>> entry : inSegreteria2Ispettorato.entrySet()) {
			final CountPer<UfficioDTO> key = entry.getKey();
			final List<CountPer<UfficioDTO>> values = entry.getValue();

			IspettoratoLinkComponent ispettoratoComponent = null;
			if (CollectionUtils.isNotEmpty(values)) {
				ispettoratoComponent = new IspettoratoLinkComponent(bean, values);
			}
			segreteria2ispettorato.put(key, ispettoratoComponent);
		}
	}

	/**
	 * Restituisce la lista degli uffici.
	 * 
	 * @return lista uffici
	 */
	public Set<CountPer<UfficioDTO>> getUfficioList() {
		return segreteria2ispettorato.keySet();
	}

	/**
	 * Imposta il component dell'ispettorato ( @see IspettoratoLinkComponent )
	 * recuperando il component associato alla key in ingresso.
	 * 
	 * @param key
	 */
	public void setSelected(final CountPer<UfficioDTO> key) {
		bean.setIspettoratoComponent(segreteria2ispettorato.get(key));

		final Set<CountPer<UfficioDTO>> keySet = segreteria2ispettorato.keySet();
		keySet.stream().forEach(counter -> counter.setSelected(false));

		key.setSelected(true);
	}
}
