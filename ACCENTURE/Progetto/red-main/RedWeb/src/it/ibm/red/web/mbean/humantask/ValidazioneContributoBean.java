package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IContributoSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * Bean che gestisce la response di validazione contributo.
 */
@Named(ConstantsWeb.MBean.VALIDAZIONE_CONTRIBUTO_BEAN)
@ViewScoped
public class ValidazioneContributoBean extends AbstractBean implements InitHumanTaskInterface {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -1888387792885610982L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ValidazioneContributoBean.class);

	/**
	 * Lista documenti selezionati.
	 */
	private List<MasterDocumentRedDTO> documentiSelezionati;

	/**
	 * Sevizio.
	 */
	private IContributoSRV contributoSRV;

	/**
	 * Servizio.
	 */
	private String motivoAssegnazioneNew;

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		setDocumentiSelezionati(inDocsSelezionati);
	}

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		// Metodo intenzionalmente vuoto.
	}

	/**
	 * Esegue la response "validazione contributo".
	 */
	public void validazioneContributoResponse() {
		final Collection<String> wobNumbers = new ArrayList<>();
		try {
			contributoSRV = ApplicationContextProvider.getApplicationContext().getBean(IContributoSRV.class);
			final ListaDocumentiBean ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
			final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
			ldb.cleanEsiti();
			//raccolgo i wobnumber necessari ad invocare il service 
			for (final MasterDocumentRedDTO m : documentiSelezionati) {
				if (!StringUtils.isNullOrEmpty(m.getWobNumber())) {
					wobNumbers.add(m.getWobNumber());
				}
			}
			if (wobNumbers.isEmpty()) {
				throw new RedException("Si è verificato un errore durante il tentato sollecito della response 'VALIDAZIONE CONTRIBUTO'");
			}
			EsitoOperazioneDTO eOpe = null;
			for (final String wobNumber : wobNumbers) {
				eOpe = contributoSRV.validazioneContributo(sb.getUtente(), wobNumber, motivoAssegnazioneNew);
				ldb.getEsitiOperazione().add(eOpe);
			}
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.VALIDAZIONE_CONTRIBUTO_BEAN);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore", e);
			showError("Si è verificato un errore");
		}
	}

	/**
	 * @return documentiSelezionati
	 */
	public List<MasterDocumentRedDTO> getDocumentiSelezionati() {
		return documentiSelezionati;
	}

	/**
	 * Imposta i documenti selezionati.
	 * @param documentiSelezionati
	 */
	public void setDocumentiSelezionati(final List<MasterDocumentRedDTO> documentiSelezionati) {
		this.documentiSelezionati = documentiSelezionati;
	}

	/**
	 * @return contributoSRV
	 */
	public IContributoSRV getContributoSRV() {
		return contributoSRV;
	}

	/**
	 * Imposta il service per la response "contributo".
	 * @param contributoSRV
	 */
	public void setContributoSRV(final IContributoSRV contributoSRV) {
		this.contributoSRV = contributoSRV;
	}

	/**
	 * Restituisce il motivo dell'assegnazione.
	 * @return motivoAssegnazioneNew
	 */
	public String getMotivoAssegnazioneNew() {
		return motivoAssegnazioneNew;
	}

	/**
	 * Imposta il motivo dell'assegnazione.
	 * @param motivoAssegnazioneNew
	 */
	public void setMotivoAssegnazioneNew(final String motivoAssegnazioneNew) {
		this.motivoAssegnazioneNew = motivoAssegnazioneNew;
	}
}
