package it.ibm.red.web.document.mbean;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentManagerDTO;
import it.ibm.red.business.dto.FormatoAllegatoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.web.document.mbean.interfaces.ITabAllegatiBean;
import it.ibm.red.web.mbean.AbstractBean;

/**
 * Bean che gestisce il tab allegati DSR.
 */
public class DsrDataTabAllegatiBean extends AbstractBean implements ITabAllegatiBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -2371922263316740906L;

	/**
	 * Dettaglio.
	 */
	private DetailDocumentRedDTO detail;

	/**
	 * Allegati.
	 */
	private List<AllegatoDTO> allegati;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DsrDataTabAllegatiBean.class);

	/**
	 * Dettagli documento.
	 */
	private DocumentManagerDTO documentManagerDTO;

	/**
	 * Service.
	 */
	private final IDocumentManagerFacadeSRV documentManagerSRV;

	/**
	 * Service.
	 */
	private final ISignFacadeSRV signSRV;

	/**
	 * Informazioni utente.
	 */
	private final UtenteDTO utente;

	/**
	 * Flag no download.
	 */
	private Boolean flagNotDownload;

	/**
	 * Costruttore del bean, inietta i service.
	 * 
	 * @param inDetail
	 * @param utente
	 */
	public DsrDataTabAllegatiBean(final DetailDocumentRedDTO inDetail, final UtenteDTO utente) {
		detail = inDetail;

		allegati = detail.getAllegati();

		this.utente = utente;

		documentManagerSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentManagerFacadeSRV.class);
		signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignFacadeSRV.class);

		documentManagerDTO = documentManagerSRV.prepareMascheraCreazioneOModifica(utente, this.detail, false);

		final List<FormatoAllegatoDTO> listaDTO = documentManagerDTO.getComboFormatiAllegato().stream().filter(
				dto -> !dto.getFormatoAllegato().equals(FormatoAllegatoEnum.FIRMATO_DIGITALMENTE) && !dto.getFormatoAllegato().equals(FormatoAllegatoEnum.FORMATO_ORIGINALE))
				.collect(Collectors.toList());

		documentManagerDTO.setComboFormatiAllegato(listaDTO);

		flagNotDownload = false;
	}

	/**
	 * @see it.ibm.red.web.document.mbean.ITabAllegatiBean#rimuoviAllegato(java.lang.
	 *      Object).
	 */
	@Override
	public void rimuoviAllegato(final Object index) {
		try {
			final Integer i = (Integer) index;
			allegati.remove(i.intValue());
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * @see it.ibm.red.web.document.mbean.ITabAllegatiBean#addAllegato().
	 */
	@Override
	public void addAllegato() {
		if (this.allegati == null) {
			this.allegati = new ArrayList<>();
			detail.setAllegati(allegati);
		}
		final AllegatoDTO a = new AllegatoDTO();
		a.setFormatoSelected(FormatoAllegatoEnum.ELETTRONICO);
		a.setFormato(FormatoAllegatoEnum.ELETTRONICO.getId());
		a.setNewAllegato(true);
		this.allegati.add(a);
		onChangeComboFormatoAllegato(this.allegati.size() - 1);
	}

	/**
	 * @see it.ibm.red.web.document.mbean.ITabAllegatiBean#handleFileUploadAllegato(org.
	 *      primefaces.event.FileUploadEvent).
	 */
	@Override
	public void handleFileUploadAllegato(FileUploadEvent event) {
		try {
			event = checkFileName(event);
			if (event == null) {
				return;
			}
			final Integer i = (Integer) event.getComponent().getAttributes().get("index");
			if (i >= this.allegati.size()) {
				throw new RedException("Errore nel caricamento file.");
			}

			final AllegatoDTO allegatoSelected = this.allegati.get(i);

			if (allegatoSelected.getFormatoSelected() == FormatoAllegatoEnum.FIRMATO_DIGITALMENTE) {
				final InputStream is = new ByteArrayInputStream(event.getFile().getContents());
				if (!signSRV.hasSignatures(is, utente)) {
					LOGGER.info("Il file inserito non risulta essere firmato.");
					return;
				}
			}

			allegatoSelected.setNomeFile(event.getFile().getFileName());
			allegatoSelected.setMimeType(event.getFile().getContentType());
			allegatoSelected.setContent(event.getFile().getContents());

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * @see it.ibm.red.web.document.mbean.ITabAllegatiBean#rimuoviFileAllegato(java.lang.
	 *      Object).
	 */
	@Override
	public void rimuoviFileAllegato(final Object index) {
		try {
			final Integer i = (Integer) index;
			if (i >= this.allegati.size()) {
				throw new RedException("Errore nel caricamento file.");
			}

			final AllegatoDTO allegatoSelected = this.allegati.get(i);
			allegatoSelected.setNomeFile(null);
			allegatoSelected.setMimeType(null);
			allegatoSelected.setContent(null);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * @see it.ibm.red.web.document.mbean.ITabAllegatiBean#onChangeComboFormatoAllegato(
	 *      java.lang.Object).
	 */
	@Override
	public void onChangeComboFormatoAllegato(final Object index) {
		try {
			final Integer i = (Integer) index;
			if (i >= this.allegati.size()) {
				throw new RedException("Errore nell'aggiornamento del formato allegato.");
			}

			final AllegatoDTO selected = this.allegati.get(i);
			selected.setFormato(selected.getFormatoSelected().getId());

			switch (selected.getFormatoSelected()) {
			case CARTACEO:
				
				handleFormatoCartaceo(selected);
				break;
			case ELETTRONICO:
				
				handleFormatoElettronico(selected);
				break;
			case NON_SPECIFICATO:
				
				handleFormatoNonSpecificato(selected);
				break;
			case FIRMATO_DIGITALMENTE:
				
				handleFormatoFirmatoDigitalmente(selected);
				break;
			default:
				break;
			}
			flagNotDownload = true;
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
		}

	}

	/**
	 * Aggiorna gli attributi dell'allegato quando firmato digitalmente.
	 * @param selected
	 *            Allegato per il quale aggiornare gli attributi.
	 */
	private static void handleFormatoFirmatoDigitalmente(final AllegatoDTO selected) {
		selected.setFormatoOriginale(true);
		selected.setCheckMantieniFormatoOriginaleVisible(false);

		selected.setCheckDaFirmareVisible(true);
		selected.setDaFirmareBoolean(false);
		selected.setDaFirmare(0);

		selected.setCheckCopiaConformeVisible(false);
		selected.setCopiaConforme(false);
		selected.setIdCopiaConforme(null);
		selected.setIdUfficioCopiaConforme(null);
		selected.setIdUtenteCopiaConforme(null);

		selected.setUploadFileVisible(true);
		selected.setNomeFile(null); // resetto eventualmente il file perché potrebbe non essere firmato
		selected.setMimeType(null);
		selected.setContent(null);

		selected.setPosizioneVisible(false);
		selected.setPosizione(null);
		selected.setBarcodeVisible(false);
		selected.setBarcode(null);
	}

	/**
	 * Aggiorna gli attributi dell'allegato quando il formato non è specificato.
	 * 
	 * @param selected
	 *            Allegato per il quale aggiornare gli attributi.
	 */
	private static void handleFormatoNonSpecificato(final AllegatoDTO selected) {
		selected.setFormatoOriginale(false);
		selected.setCheckMantieniFormatoOriginaleVisible(false);

		selected.setCheckDaFirmareVisible(false);
		selected.setDaFirmareBoolean(false);
		selected.setDaFirmare(0);

		selected.setCheckCopiaConformeVisible(false);
		selected.setCopiaConforme(false);
		selected.setIdCopiaConforme(null);
		selected.setIdUfficioCopiaConforme(null);
		selected.setIdUtenteCopiaConforme(null);

		selected.setUploadFileVisible(false);
		selected.setNomeFile(null); // resetto eventualmente il file
		selected.setMimeType(null);
		selected.setContent(null);

		selected.setPosizioneVisible(true);
		selected.setPosizione(null);
		selected.setBarcodeVisible(false);
		selected.setBarcode(null);
	}

	/**
	 * Aggiorna gli attributi dell'allegato quando il formato è elettronico.
	 * 
	 * @param selected
	 *            Allegato per il quale aggiornare gli attributi.
	 */
	private void handleFormatoElettronico(final AllegatoDTO selected) {
		selected.setFormatoOriginale(false);
		selected.setCheckMantieniFormatoOriginaleVisible(true);

		if (documentManagerSRV.isAllegatiCheckDaFirmareVisible(documentManagerDTO.getCategoria())) {
			selected.setCheckDaFirmareVisible(true);
			selected.setDaFirmareBoolean(false);
			selected.setDaFirmare(0);
		}

		if (documentManagerSRV.isResponsabiliCopiaConformeForAllegatoVisible(documentManagerDTO.getCategoria(),
				documentManagerDTO.getComboResponsabileCopiaConforme().isEmpty())) {
			selected.setCheckCopiaConformeVisible(true);
			selected.setCopiaConforme(false);
			selected.setIdCopiaConforme(null);
			selected.setIdUfficioCopiaConforme(null);
			selected.setIdUtenteCopiaConforme(null);
		}

		selected.setUploadFileVisible(true);

		selected.setPosizioneVisible(false);
		selected.setPosizione(null);
		selected.setBarcodeVisible(false);
		selected.setBarcode(null);
	}

	/**
	 * Aggiorna gli attributi dell'allegato quando il formato è cartaceo.
	 * 
	 * @param selected
	 *            Allegato per il quale aggiornare gli attributi.
	 */
	private static void handleFormatoCartaceo(final AllegatoDTO selected) {
		selected.setFormatoOriginale(false);
		selected.setCheckMantieniFormatoOriginaleVisible(false);

		selected.setCheckDaFirmareVisible(false);
		selected.setDaFirmareBoolean(false);
		selected.setDaFirmare(0);

		selected.setCheckCopiaConformeVisible(false);
		selected.setCopiaConforme(false);
		selected.setIdCopiaConforme(null);
		selected.setIdUfficioCopiaConforme(null);
		selected.setIdUtenteCopiaConforme(null);

		selected.setUploadFileVisible(false);
		selected.setNomeFile(null); // resetto eventualmente il file
		selected.setMimeType(null);
		selected.setContent(null);

		selected.setPosizioneVisible(true);
		selected.setPosizione(null);
		selected.setBarcodeVisible(true);
		selected.setBarcode(null);
	}

	/**
	 * Esegue una validazione sugli allegati gestendo eventuali errori.
	 * 
	 * @return true se esito validazione positivo, false altrimenti
	 */
	public boolean validateAllegati() {
		boolean esito = CollectionUtils.isEmpty(this.allegati);
		if (!esito) {
			esito = true;
			final Iterator<AllegatoDTO> it = this.allegati.iterator();
			while (esito && it.hasNext()) {
				final AllegatoDTO allegato = it.next();
				if (allegato.getFormatoSelected() == FormatoAllegatoEnum.ELETTRONICO) {
					esito = StringUtils.isNotBlank(allegato.getMimeType());
				}

			}

			if (!esito) {
				LOGGER.info("Validazione degli allegati non riuscita");
			}
		}

		return esito;
	}

	/**
	 * @see it.ibm.red.web.document.mbean.ITabAllegatiBean#downloadAllegato(java.lang.
	 *      Object).
	 */
	@Override
	public StreamedContent downloadAllegato(final Object index) {
		StreamedContent file = null;
		final Integer i = (Integer) index;
		try {
			final AllegatoDTO allegatoSelezionato = allegati.get(i);
			final String mimeType = allegatoSelezionato.getMimeType();
			final String nome = allegatoSelezionato.getNomeFile();

			final InputStream stream = documentManagerSRV.getStreamDocumentoByGuid(allegatoSelezionato.getGuid(), utente);
			file = new DefaultStreamedContent(stream, mimeType, nome);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il download del file: ", e);
		}
		return file;
	}

	/**
	 * @see it.ibm.red.web.document.mbean.ITabAllegatiBean#getDetail().
	 */
	@Override
	public DetailDocumentRedDTO getDetail() {
		return detail;
	}

	/**
	 * Imposta il dettaglio.
	 * 
	 * @param detail
	 */
	public void setDetail(final DetailDocumentRedDTO detail) {
		this.detail = detail;
	}

	/**
	 * @see it.ibm.red.web.document.mbean.ITabAllegatiBean#getAllegati().
	 */
	@Override
	public List<AllegatoDTO> getAllegati() {
		return allegati;
	}

	/**
	 * @see it.ibm.red.web.document.mbean.ITabAllegatiBean#setAllegati(java.util.List).
	 */
	@Override
	public void setAllegati(final List<AllegatoDTO> allegati) {
		this.allegati = allegati;
	}

	/**
	 * @see it.ibm.red.web.document.mbean.ITabAllegatiBean#getDocumentManagerDTO().
	 */
	@Override
	public DocumentManagerDTO getDocumentManagerDTO() {
		return documentManagerDTO;
	}

	/**
	 * Imposta il documentManagerDTO.
	 * 
	 * @param documentManagerDTO
	 */
	public void setDocumentManagerDTO(final DocumentManagerDTO documentManagerDTO) {
		this.documentManagerDTO = documentManagerDTO;
	}

	/**
	 * @see it.ibm.red.web.document.mbean.ITabAllegatiBean#getFlagNotDownload().
	 */
	@Override
	public Boolean getFlagNotDownload() {
		return flagNotDownload;
	}

	/**
	 * Imposta il flag: flagNotDownload.
	 * 
	 * @param flagNotDownload
	 */
	public void setFlagNotDownload(final Boolean flagNotDownload) {
		this.flagNotDownload = flagNotDownload;
	}

	@Override
	protected void postConstruct() {
		// Non occorre fare niente nel post construct del metodo.
	}

	/**
	 * Restituisce l'utente.
	 * 
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	@Override
	public void gestisciUploadAllegati(final FileUploadEvent event) {
		// Non occorre fare niente in questo metodo.
	}
}
