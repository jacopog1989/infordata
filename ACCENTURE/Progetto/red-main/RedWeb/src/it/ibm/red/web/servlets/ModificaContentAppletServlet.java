package it.ibm.red.web.servlets;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;

import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoOperazioneAppletEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IUpdateContentFacadeSRV;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;

/**
 * Servlet implementation class ModificaContentAppletServlet.
 */
public class ModificaContentAppletServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ModificaContentAppletServlet.class.getName());
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModificaContentAppletServlet() {
        super();
    }
    
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doPost(final HttpServletRequest request, final HttpServletResponse response) {
		try {
			final String tipoOperazioneApplet = request.getParameter("tipoapplet");
			final String workflowNumber = request.getParameter("workflowNumber");
			final String fileName = request.getParameter("fileName");
			final String nuovoBase64 = request.getParameter("base64");
			final byte[] newContent = Base64.decodeBase64(nuovoBase64.getBytes());
			
			if (newContent != null) {
				// Applet richiamata dalla funzionalità di Modifica Template (in fase di creazione del documento)
				if (TipoOperazioneAppletEnum.MODIFICA_TEMPLATE.getId().equals(tipoOperazioneApplet)) {
					LOGGER.info("OPERAZIONE APPLET MODIFICA TEMPLATE");
					final FileDTO template = new FileDTO(fileName, newContent, ContentType.DOC);
					request.getSession().setAttribute(ConstantsWeb.SessionObject.EDITED_TEMPLATE, template);
					
				// Applet richiamata dalla response "Modifica"
				} else if (TipoOperazioneAppletEnum.MODIFICA_DOCUMENTO.getId().equals(tipoOperazioneApplet)) {
					LOGGER.info("OPERAZIONE APPLET MODIFICA DOCUMENTO PRINCIPALE");

					final String username = (String) request.getSession().getAttribute(ConstantsWeb.SessionObject.USERNAME);
					final IUtenteFacadeSRV usrFacade = ApplicationContextProvider.getApplicationContext().getBean(IUtenteFacadeSRV.class);
					final UtenteDTO utente = usrFacade.getByUsername(username);

					final IUpdateContentFacadeSRV facade = ApplicationContextProvider.getApplicationContext().getBean(IUpdateContentFacadeSRV.class);
					facade.updateContent(workflowNumber, newContent, utente);

					request.getSession().removeAttribute(ConstantsWeb.SessionObject.USERNAME);
				}
			} else {
				LOGGER.info("L'APPLET NON HA FORNITO ALCUN NUOVO CONTENUTO");
			}
		} catch (final Exception e) {
			LOGGER.error(e);
		}
	}

}
