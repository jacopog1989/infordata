package it.ibm.red.web.mbean.interfaces;

import java.io.Serializable;

/**
 * Interfaccia del component che gestisce il popup.
 */
public interface IPopupComponent extends Serializable {

	/**
	 * Metodo per la gestione del comportamento del bottone ok
	 */
	void conferma();
	
	/**
	 * Metodo per la gestione del comportamento del bottone annulla
	 */
	void annulla();
}
