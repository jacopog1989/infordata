package it.ibm.red.web.mbean.interfaces;

import java.util.List;

import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.FascicoloDTO;

/**
 * Interfaccia del bean che gestisce i documenti allegabili.
 */
public interface DocumentiAllegabiliSelectableInterface extends IDocumentiAllegabiliSizeCounter, IDatatableUtils {

	/**
	 * Restituisce la lista degli allegati selezionati.
	 * @return List di DocumentoAllegabileDTO
	 */
	List<DocumentoAllegabileDTO> getSelectedAllegabili();

	/**
	 * Imposta gli allegati selezionabili.
	 * @param selectedAllegabili
	 */
	void setSelectedAllegabili(List<DocumentoAllegabileDTO> selectedAllegabili);

	/**
	 * Restituisce la lista dei documenti allegabili.
	 * @return List di DocumentoAllegabileDTO
	 */
	List<DocumentoAllegabileDTO> getDocumentiAllegabili();

	/**
	 * Restituisce il fascicolo.
	 * @return FascicoloDTO
	 */
	FascicoloDTO getFascicolo();

	/**
	 * @param index
	 */
	void aggiornaVerificaFirmaAllegato(Object index);
}