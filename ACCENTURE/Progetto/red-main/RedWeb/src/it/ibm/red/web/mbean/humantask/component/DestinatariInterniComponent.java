package it.ibm.red.web.mbean.humantask.component;

import java.io.Serializable;
import java.util.Collection;

import org.apache.commons.collections4.CollectionUtils;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IPredisponiDsrFacadeSRV;

/**
 * Component che gestisce i destintari interni.
 */
public class DestinatariInterniComponent implements Serializable {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 6789979281691757781L;

	/**
	 * Lista utenti ufficio.
	 */
	private Collection<UtenteDTO> utentiUfficioList;
	
	/**
	 * Utente selezionato.
	 */
	private UtenteDTO selected;

	/**
	 * Costruttore del component. Lancia un'eccezione se non è possibile recuperare la lista degli utenti dell'ufficio.
	 * @throws RedException
	 */
	public DestinatariInterniComponent() {
		final IPredisponiDsrFacadeSRV fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IPredisponiDsrFacadeSRV.class);
		utentiUfficioList = fepaSRV.retrieveDestinatariDsr();
		if (CollectionUtils.isEmpty(utentiUfficioList)) {
			throw new RedException("Impossibile recuperare i destinatari");
		}
	}

	/**
	 * Restituisce la lista di utenti dell'ufficio.
	 * @return utentiUfficioList
	 */
	public Collection<UtenteDTO> getUtentiUfficioList() {
		return utentiUfficioList;
	}

	/**
	 * Imposta la lista degli utenti dell'ufficio.
	 * @param utentiUfficioList
	 */
	public void setUtentiUfficioList(final Collection<UtenteDTO> utentiUfficioList) {
		this.utentiUfficioList = utentiUfficioList;
	}

	/**
	 * Restituisce l'utente selezionato.
	 * @return
	 */
	public UtenteDTO getSelected() {
		return selected;
	}

	/**
	 * Imposta l'utente selezionato.
	 * @param selected
	 */
	public void setSelected(final UtenteDTO selected) {
		this.selected = selected;
	}

}
