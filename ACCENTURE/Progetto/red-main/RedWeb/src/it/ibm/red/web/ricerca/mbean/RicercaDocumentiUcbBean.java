package it.ibm.red.web.ricerca.mbean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.ParamsRicercaDocUCBAssegnanteAssegnatarioDTO;
import it.ibm.red.business.dto.RisultatiRicercaUcbDTO;
import it.ibm.red.business.dto.StatoRicercaDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.SessionObject;
import it.ibm.red.web.enums.DettaglioDocumentoFileEnum;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.ColumnModelDTO;
import it.ibm.red.web.mbean.interfaces.IFascicoloProcedimentaleManagerInitalizer;

/**
 * @author SLungarella
 * Bean che gestisce le funzionalità della sezione di ricerca avanzata UCB. 
 * In particolare si occupa di prelevare i risultati ottenuti dalla ricerca e renderne possibile la visualizzazione in front-end.
 * 
 */
@Named(ConstantsWeb.MBean.RICERCA_DOCUMENTI_UCB_BEAN)
@ViewScoped
public class RicercaDocumentiUcbBean extends RicercaDocumentiRedAbstractBean {

	private static final long serialVersionUID = 1500972470805604711L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaDocumentiUcbBean.class.getName());
	
	/**
	 * Parametri ricerca.
	 */
	private ParamsRicercaAvanzataDocDTO formRicercaDocumenti;

	/**
	 * Ricerca assegnante.
	 */
	private ParamsRicercaDocUCBAssegnanteAssegnatarioDTO formRicercaAssegnante;
	
	/**
	 * Datatble documenti.
	 */
	private SimpleDetailDataTableHelper<MasterDocumentRedDTO> documentiUcb;

	/**
	 * Lista colonne.
	 */
	private List<ColumnModelDTO> columns = new ArrayList<>();
	
	/**
	 * Testo ricerca.
	 */
	private String ricercaKey;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		super.postConstruct();
		
		documentiUcb = new SimpleDetailDataTableHelper<>();
		documentiUcb.setMasters(new ArrayList<>());
		
		initPaginaRicerca();
	}

	/**
	 * Inizializza la pagina di ricerca.
	 */
	public void initPaginaRicerca() {
		try {
			
			final StatoRicercaDTO stDto = (StatoRicercaDTO) FacesHelper.getObjectFromSession(SessionObject.STATO_RICERCA, true);
			if (stDto != null) {
				final RisultatiRicercaUcbDTO searchResult = (RisultatiRicercaUcbDTO) stDto.getRisultatiRicerca();
				ricercaKey = searchResult.getRicercaKey();
				
				if (Constants.Ricerca.KEY_RICERCA_UCB_DOC.equals(ricercaKey)) {
					formRicercaDocumenti = (ParamsRicercaAvanzataDocDTO) stDto.getFormRicerca();
				} else if (Constants.Ricerca.KEY_RICERCA_UCB_ASS.equals(ricercaKey)) {
					formRicercaAssegnante = (ParamsRicercaDocUCBAssegnanteAssegnatarioDTO) stDto.getFormRicerca();
				}
				
				if (searchResult.isRisultatiOmogenei() && !searchResult.getMetadatiOut().isEmpty()) {
					loadColumns(searchResult.getMetadatiOut());
				}
				
				documentiUcb.setMasters(searchResult.getMasters());
				documentiUcb.selectFirst(true);
				selectDetail(documentiUcb.getCurrentMaster());
			
				if (getNumMaxRisultati() != null && getNumMaxRisultati().equals(searchResult.getMasters().size())) {
					showWarnMessage(RicercaAbstractBean.MSG_NUM_MAX_RISULTATI);
				}
			} else if (Constants.Ricerca.KEY_RICERCA_UCB_DOC.equals(ricercaKey)) {
				formRicercaDocumenti = new ParamsRicercaAvanzataDocDTO();
			} else if (Constants.Ricerca.KEY_RICERCA_UCB_ASS.equals(ricercaKey)) {
				formRicercaAssegnante = new ParamsRicercaDocUCBAssegnanteAssegnatarioDTO();
			}
			
		} catch (final Exception e) {
			LOGGER.error("Non è stato possibile recuperare i risultati di ricerca", e);
			showError("Non è stato possibile recuperare i risultati di ricerca");
		}
	}
	
	private void loadColumns(final Collection<MetadatoDTO> metadatiOut) {
		columns = new ArrayList<>();
		try {
			for (final MetadatoDTO m : metadatiOut) {
				final ColumnModelDTO c = new ColumnModelDTO(m.getName(), null, m.getType().getCode());
				columns.add(c);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la gestione delle colonne dinamiche.", e);
			showError("Errore durante la gestione delle colonne dinamiche.");
		}
	}

	/**
	 * Gestisce la selezione della riga del datatable.
	 * @param se
	 */
	public final void rowSelector(final SelectEvent se) {
		documentiUcb.rowSelector(se);
		selectDetail(documentiUcb.getCurrentMaster());
	}
	
	/**
	 * Restituisce il documento selezionato.
	 * @return master selezionato
	 */
	@Override
	public MasterDocumentRedDTO getSelectedDocument() {
		return documentiUcb.getCurrentMaster();
	}

	/**
	 * Aggiorna il dettaglio con il current master.
	 */
	public void aggiorna() {
		if (documentiUcb != null && documentiUcb.getCurrentMaster() != null) {
			selectDetail(documentiUcb.getCurrentMaster());
		}
	}	

	/**
	 * Valorizza i parametri in modo da consentire una ricerca con gli stessi parametri dell'ultima ricerca eseguita.
	 */
	public void ripetiRicercaUcb() {
		try {
			
			if (Constants.Ricerca.KEY_RICERCA_UCB_DOC.equals(ricercaKey)) {
				sessionBean.getRicercaFormContainer().setDocumentiUCB(formRicercaDocumenti);
			} else if (Constants.Ricerca.KEY_RICERCA_UCB_ASS.equals(ricercaKey)) {
				sessionBean.getRicercaFormContainer().setAssegnazioneUCB(formRicercaAssegnante);
			} 
				
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Gestisce l'apertura del fascicolo procedimentale.
	 */
	@Override
	public void openFascicoloProcedimentale() {
		IFascicoloProcedimentaleManagerInitalizer initializer;
		if (DettaglioDocumentoFileEnum.FEPAFATTURA.equals(getDettaglioDaCaricare()) 
				|| DettaglioDocumentoFileEnum.FEPADD.equals(getDettaglioDaCaricare()) 
				|| DettaglioDocumentoFileEnum.FEPADSR.equals(getDettaglioDaCaricare())) {
			initializer = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_FEPA_BEAN);
		} else {
			setDettaglioDaCaricare(DettaglioDocumentoFileEnum.GENERICO);
			initializer = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN);
		}
		
		initializer.openFascicoloProcedimentale();
	}

	/**
	 * Restituisce il datatable helper per i documenti ucb.
	 * @return datatable helper documenti
	 */
	public final SimpleDetailDataTableHelper<MasterDocumentRedDTO> getDocumentiUcb() {
		return documentiUcb;
	}

	/**
	 * Imposta il datatable helper per i documenti ucb.
	 * @param documentiUcb
	 */
	public final void setDocumentiUcb(final SimpleDetailDataTableHelper<MasterDocumentRedDTO> documentiUcb) {
		this.documentiUcb = documentiUcb;
	}
	
	/**
	 * Restituisce il prefisso nome ricerca excel.
	 * @return prefisso nome file excel
	 */
	@Override
	protected String getNomeRicerca4Excel() {
		return "ricercadocumenti_";
	}

	/**
	 * Restituisce le colonne.
	 * @return colonne
	 */
	public List<ColumnModelDTO> getColumns() {
		return columns;
	}

	/**
	 * Imposta le colonne.
	 * @param columns
	 */
	public void setColumns(final List<ColumnModelDTO> columns) {
		this.columns = columns;
	}

	/**
	 * @return the ricercaKey
	 */
	public String getRicercaKey() {
		return ricercaKey;
	}

	/**
	 * @param ricercaKey the ricercaKey to set
	 */
	public void setRicercaKey(final String ricercaKey) {
		this.ricercaKey = ricercaKey;
	}
	
}
