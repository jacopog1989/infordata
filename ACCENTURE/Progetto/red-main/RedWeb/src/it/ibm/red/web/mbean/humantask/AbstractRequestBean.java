/**
 * 
 */
package it.ibm.red.web.mbean.humantask;

import java.util.List;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * @author APerquoti
 *
 */
public abstract class AbstractRequestBean extends AbstractBean implements InitHumanTaskInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1980405351351183657L;
	
	@Override
	protected void postConstruct() {
		
	}


	@Override
	public void initBean(final List<MasterDocumentRedDTO> docsSelezionati) {
		
	}

}
