package it.ibm.red.web.nps.mbean;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.INpsFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.nps.component.DetailNpsComponent;

/**
 * Bean che gestisce il dettaglio nps.
 */
@Named(ConstantsWeb.MBean.DETTAGLIO_NPS_BEAN)
@ViewScoped
public class DettaglioNpsBean extends AbstractBean {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -3480464911901805411L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DettaglioNpsBean.class.getName());
	
	/**
	 * Servizio.
	 */
	private INpsFacadeSRV npsSRV;

	/**
	 * Dettaglio protocollo.
	 */
	private ProtocolloNpsDTO detail;
	
	/**
	 * Componente dettaglio nps.
	 */
	private DetailNpsComponent detailComponent;

	/**
	 * Post construct del bean.
	 */
	@PostConstruct
	@Override
	protected void postConstruct() {
		detailComponent = new DetailNpsComponent();
		npsSRV = ApplicationContextProvider.getApplicationContext().getBean(INpsFacadeSRV.class);
	}

	/**
	 * Restituisce il dettaglio.
	 * @return detail
	 */
	public ProtocolloNpsDTO getDetail() {
		return detail;
	}

	/**
	 * Imposta il dettaglio.
	 * @param idAoo
	 * @param detail
	 */
	public void setDetail(final Integer idAoo, final ProtocolloNpsDTO detail, final UtenteDTO utente) {
		try {
			final ProtocolloNpsDTO dettaglioCorposo = npsSRV.getDettagliProtocollo(detail, idAoo, utente, false);
			this.detail = dettaglioCorposo;
			detailComponent.setDetail(dettaglioCorposo);
		} catch (final Exception e) {
			LOGGER.error("Impossibile recuperare il dettaglio per il Protocollo nps", e);
			showError("Impossibile rcuperare il dettaglio");
		}
	}

	/**
	 * Restituisce il dettaglio del component.
	 * 
	 * @return
	 */
	public DetailNpsComponent getDetailComponent() {
		return detailComponent;
	}
}
