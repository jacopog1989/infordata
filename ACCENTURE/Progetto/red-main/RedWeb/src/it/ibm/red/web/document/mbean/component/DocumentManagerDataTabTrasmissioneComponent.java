package it.ibm.red.web.document.mbean.component;

import java.io.InputStream;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.TrasmissioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IAllegatoFacadeSRV;
import it.ibm.red.business.service.facade.IFepaFacadeSRV;
import it.ibm.red.business.service.facade.ISiebelFacadeSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * Si occupa a partire dal documento di recuperare le trasmissioni
 * 
 * All'occorrenza 
 * @author a.difolca
 *
 */
public class DocumentManagerDataTabTrasmissioneComponent extends AbstractComponent {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2156157219855506367L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DocumentManagerDataTabTrasmissioneComponent.class);

	/**
	 * Servizio.
	 */
	private final IFepaFacadeSRV fepaSRV;

	/**
	 * Servizio.
	 */
	private final IAllegatoFacadeSRV allegatoSRV;

	/**
	 * Servizio.
	 */
	private final ISiebelFacadeSRV siebelSRV;

	/**
	 * Utente
	 */
	private final UtenteDTO utente;

	/**
	 * Dati trasmissione.
	 */
	private TrasmissioneDTO trasmissione;
	
	/**
	 * Flag entrata.
	 */
	private boolean inEntrata;
	
	/**
	 * Flag eml.
	 */
	private boolean flagEmlDisable = false;
	
	/**
	 * Identificativo workflow principale.
	 */
	private String wobNumberPrincipale;
	
	/**
	 * Eml originale.
	 */
	private DocumentoAllegabileDTO emlOriginale;

	/**
	 * Costruttore del component, si occupa di inizializzare {@link #fepaSRV}, {@link #allegatoSRV}, {@link #siebelSRV} e di impostare i parametri del detail.
	 * @param detail
	 * @param inUtente
	 */
	public DocumentManagerDataTabTrasmissioneComponent(final DetailDocumentRedDTO detail, final UtenteDTO inUtente) {
		fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);
		allegatoSRV = ApplicationContextProvider.getApplicationContext().getBean(IAllegatoFacadeSRV.class);
		siebelSRV = ApplicationContextProvider.getApplicationContext().getBean(ISiebelFacadeSRV.class);
		
		utente = inUtente;
		
		if (detail != null && detail.getGuid() != null) {
			trasmissione = fepaSRV.getTrasmissione(detail.getGuid(), utente);
			wobNumberPrincipale = detail.getWobNumberPrincipale();
			final String documentTitle = detail.getDocumentTitle();
			if (StringUtils.isNullOrEmpty(wobNumberPrincipale)) {
				emlOriginale = siebelSRV.getMailDocumentoByDocumentTitle(utente, documentTitle);
			} else {
				emlOriginale = siebelSRV.getMailDocumentoByWobNumber(utente, wobNumberPrincipale);
			}
			
			if (emlOriginale == null) {
				flagEmlDisable = true;
			}
		}

		
		inEntrata = false;
		if (detail != null) {
			final CategoriaDocumentoEnum currentCategoria = CategoriaDocumentoEnum.get(detail.getIdCategoriaDocumento());
			inEntrata = currentCategoria == CategoriaDocumentoEnum.ENTRATA 
					|| currentCategoria == CategoriaDocumentoEnum.DOCUMENTO_ENTRATA;
		}
	}

	/**
	 * Restituisce true se il documento è protocollato, false altrimenti.
	 * @return true se il documento è protocollato, false altrimenti
	 */
	public boolean isDocumentoProtocollato() {
		return trasmissione != null && inEntrata;
	}

	/**
	 * Restituisce il DTO per la gestione della trasmissione.
	 * @return trasmissione
	 */
	public TrasmissioneDTO getTrasmissione() {
		return trasmissione;
	}

	/**
	 * Imposta il DTO che contiene informazioni sulla trasmissione.
	 * @param trasmissione
	 */
	public void setTrasmissione(final TrasmissioneDTO trasmissione) {
		this.trasmissione = trasmissione;
	}

	/**
	 * Restituisce uno StreamedContent che consente di effettuare il download del file della dichiarazione.
	 * @param allegato
	 * @return StreamedContent file dichiarazione
	 */
	public StreamedContent getDownload(final AllegatoDTO allegato) {
		InputStream io = null;
		try {
			io = allegatoSRV.getStreamByGuid(allegato, utente);
			if (io == null) {
				showErrorMessage("Impossibile recuperare il file della dichiarazione");
			}
		} catch (final Exception e) {
			final String errorMsg = "Si sono verificati problemi durante il downlod dell'allegato";
			LOGGER.error(errorMsg, e);
			showErrorMessage(errorMsg);
		}
		return new DefaultStreamedContent(io, allegato.getMimeType(), allegato.getNomeFile());
	}

	/**
	 * Restituisce lo StreamedContent per il download dell'Eml originale.
	 * @return StreamedContent eml
	 */
	public StreamedContent getDownloadEmlOriginale() {
		StreamedContent output = null;
		
		try {

			output = new DefaultStreamedContent(emlOriginale.getContenuto().getInputStream(), emlOriginale.getContenuto().getContentType(), emlOriginale.getNomeFile());
		
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'eml originale.", e);
			showErrorMessage("Errore durante il recupero dell'eml originale."); 
		}
		
		return output;
	}

	/**
	 * @return the flagEmlEnable
	 */
	public boolean isFlagEmlDisable() {
		return flagEmlDisable;
	}

	/**
	 * @param flagEmlEnable the flagEmlEnable to set
	 */
	public void setFlagEmlDisable(final boolean flagEmlDisable) {
		this.flagEmlDisable = flagEmlDisable;
	}
	
}
