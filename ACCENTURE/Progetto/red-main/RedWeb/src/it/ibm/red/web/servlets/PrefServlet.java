package it.ibm.red.web.servlets;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.ibm.red.business.dto.PreferenzeDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.mbean.SessionBean;

/**
 * Servlet implementation class PrefServlet.
 */
public class PrefServlet extends HttpServlet {
	
	/**
	 * Costante serial version UID 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PrefServlet.class.getName());

    /**
     * Costruttore pref servlet.
     *
     * @see HttpServlet#HttpServlet()
     */
    public PrefServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response) {
    	try {
    		final SessionBean sb = (SessionBean) request.getSession().getAttribute(ConstantsWeb.MBean.SESSION_BEAN);
    		final PreferenzeDTO preferenze = sb.getPreferenze();
    		response.getWriter().append(preferenze.getTextSize().toString());
    	} catch (final Exception e) {
    		LOGGER.error(e);
    	}
	}

}
