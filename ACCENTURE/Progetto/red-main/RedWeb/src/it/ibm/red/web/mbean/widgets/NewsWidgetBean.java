package it.ibm.red.web.mbean.widgets;


import java.io.ByteArrayInputStream;
import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import it.ibm.red.business.dto.EventoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ICalendarioFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;

/**
 * Bean per la gestione del widget delle news
 * 
 */
@Named(ConstantsWeb.MBean.NEWS_WIDGET_BEAN)
@ViewScoped
public class NewsWidgetBean extends AbstractBean {
 
	/** 
	 * The Constant serialVersionUID. 
	 */
	private static final long serialVersionUID = -7442687160902769133L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NewsWidgetBean.class.getName());
	
	/** 
	 * Facade service calendario
	 */
	private ICalendarioFacadeSRV calSRV;

	/** 
	 * Lista che contiene le news 
	 */
	private Collection<EventoDTO> news;
	
	/** 
	 * Allegato selezionato relativo ad un evento 
	 */
	private EventoDTO selectedAllegato;
	
	/**
	 * Post construct.
	 */
	@Override
	@PostConstruct
	public void postConstruct() {
		final SessionBean sessionBean = (SessionBean) FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		final UtenteDTO utente = sessionBean.getUtente();
		calSRV = ApplicationContextProvider.getApplicationContext().getBean(ICalendarioFacadeSRV.class);
		news = calSRV.getNews(utente, 90, 90);
	}
	
	/**
	 * Gets the news.
	 *
	 * @return the news
	 */
	public Collection<EventoDTO> getNews() {
		return news;
	}
	
	/**
	 * Download contenuto dal dettaglio.
	 *
	 * @return the streamed content
	 */
	public StreamedContent downloadFromDetail() {
		StreamedContent sc = null;  
		
		final String nome = selectedAllegato.getNomeAllegato();
		final String mimeType = selectedAllegato.getMimeTypeAllegato();
		try {
			final byte[] content = calSRV.getAllegatoEvento(selectedAllegato.getIdEvento());
			final ByteArrayInputStream stream = new ByteArrayInputStream(content);
			sc = new DefaultStreamedContent(stream, mimeType, nome);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il download del file: ", e); 
			showError("Errore durante il download del file.");
		}
		return sc;
	}
	 
	/**
	 * Gets the selected allegato.
	 *
	 * @return the selected allegato
	 */
	public EventoDTO getSelectedAllegato() {
		return selectedAllegato;
	}
	
	/**
	 * Sets the selected allegato.
	 *
	 * @param selectedAllegato the new selected allegato
	 */
	public void setSelectedAllegato(final EventoDTO selectedAllegato) {
		this.selectedAllegato = selectedAllegato;
	}

}
