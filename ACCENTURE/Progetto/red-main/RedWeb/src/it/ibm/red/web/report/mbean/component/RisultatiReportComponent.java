package it.ibm.red.web.report.mbean.component;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.dto.ReportDetailUfficioUtenteDTO;
import it.ibm.red.business.dto.ReportMasterUfficioUtenteDTO;
import it.ibm.red.business.enums.TipoOperazioneReportEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.CodaDiLavoroReport;
import it.ibm.red.business.persistence.model.TipoOperazioneReport;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IReportFacadeSRV;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.helper.faces.MessageSeverityEnum;

/**
 * Questo componente genera la tabella dei documenti per ufficio o utente non ricercati per ruolo.
 */
public class RisultatiReportComponent implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
    /**
     * Logger.
     */
	private static final REDLogger LOGGER = REDLogger.getLogger(RisultatiReportComponent.class);
	
    /**
     * Servizio.
     */
	private final IReportFacadeSRV reportSRV;
	
	/**
	 * Lista da visualizzare nell'anteprima.
	 */
	private List<ReportMasterUfficioUtenteDTO> masterList;
	
	/**
	 * Lista da visualizzare eventualmente nel dettaglio.
	 */
	private List<ReportDetailUfficioUtenteDTO> detailList;
	
	/**
	 * Selezionata dall'utente all'atto della creazione dei risultati.
	 */
	private TipoOperazioneReport tipoOperazioneSelected;
	
	/**
	 * Selezionato dall'utente all'atto della creaizone dei risultati.
	 */
	private CodaDiLavoroReport codaDiLavoroSelected;
	
	/**
	 * Qualora la ricerca sia per utente è necessario mostrare una colonna in più.
	 */
	private boolean ricercaPerUtente;
	
	/**
	 * Descrizione sopra alla tabella.
	 */
	private String detailTableHeader;
	
    /**
     * Descrizione operazione.
     */
	private String descrizioneOperazione;
	 
    /**
     * Flag tabella risultati.
     */
	private boolean showTabellaRisultati;
	 
    /**
     * Aoo.
     */
	private Long idAoo;

	/**
	 * Costruttore del component.
	 * @param idAoo
	 */
	public RisultatiReportComponent(final Long idAoo) {
		reportSRV = ApplicationContextProvider.getApplicationContext().getBean(IReportFacadeSRV.class);
		this.idAoo = idAoo;
		showTabellaRisultati = false;
	}

	/**
	 * Si occupa della visualizzazione del dettaglio.
	 * @param master
	 */
	public void showDetail(final ReportMasterUfficioUtenteDTO master) {
		try {			
			final String descrUfficio = master.getUfficio();
			final String descrUsername = master.getUsername();
			if (StringUtils.isNotBlank(descrUsername)) {
				this.detailTableHeader = descrUfficio + " - " + descrUsername;
			} else {
				this.detailTableHeader = descrUfficio;
			}
			
			this.detailList = reportSRV.ricercaDetailDocumenti(idAoo, master, tipoOperazioneSelected, codaDiLavoroSelected);
			showTabellaRisultati = true;
			
		} catch (final Exception e) {
			LOGGER.error("Errore nel componente per il recupero dei report", e);
			FacesHelper.showMessage(null, "Errore durante il recupero del report", MessageSeverityEnum.ERROR);
		}
	}

	/**
	 * Restituisce true se la coda associata a {@link #tipoOperazioneSelected} è una delle code lavoro.
	 * @see TipoOperazioneReportEnum.
	 * @return true se coda associata a coda lavoro, false altrimenti
	 */
	public boolean isCodaDilavoro() {
		boolean isCodaDilavoro = false;
		if (tipoOperazioneSelected != null) {
			final TipoOperazioneReportEnum torEnum = TipoOperazioneReportEnum.get(tipoOperazioneSelected.getCodice());
			isCodaDilavoro =  TipoOperazioneReportEnum.UFF_DOC_CODA_LAVORO == torEnum || TipoOperazioneReportEnum.UTE_DOC_IN_CODA_LAVORO == torEnum;
		}
		return isCodaDilavoro;
	}
	
	/**
	 * Restituisce la lista dei master.
	 * @return lista master
	 */
	public List<ReportMasterUfficioUtenteDTO> getMasterList() {
		return masterList;
	}

	/**
	 * Restituisce la lista dei dettagli.
	 * @return lista dettagli report
	 */
	public List<ReportDetailUfficioUtenteDTO> getDetailList() {
		return detailList;
	}

	/**
	 * Restituisce true se la ricerca è per utente, false altrimenti.
	 * @return true se la ricerca è per utente, false altrimenti
	 */
	public boolean isRicercaPerUtente() {
		return ricercaPerUtente;
	}
	
	/**
	 * Restituisce l'header del datatable del dettaglio.
	 * @return header datatable
	 */
	public String getDetailTableHeader() {
		return this.detailTableHeader;
	}

	/**
	 * Restituisce la descrizione dell'operazione.
	 * @return descrizione operazione
	 */
	public String getDescrizioneOperazione() {
		return descrizioneOperazione;
	}

	/**
	 * Imposta la lista dei master.
	 * @param masterList
	 */
	public void setMasterList(final List<ReportMasterUfficioUtenteDTO> masterList) {
		this.masterList = masterList;
	}

	/**
	 * Imposta la lista dei dettagli report.
	 * @param detailList
	 */
	public void setDetailList(final List<ReportDetailUfficioUtenteDTO> detailList) {
		this.detailList = detailList;
	}

	/**
	 * Restituisce il tipo operazione selezionata.
	 * @return tipo operazione selezionata
	 */
	public TipoOperazioneReport getTipoOperazioneSelected() {
		return tipoOperazioneSelected;
	}

	/**
	 * Impsota il tipo operazione selezionato.
	 * @param tipoOperazioneSelected
	 */
	public void setTipoOperazioneSelected(final TipoOperazioneReport tipoOperazioneSelected) {
		this.tipoOperazioneSelected = tipoOperazioneSelected;
		if (tipoOperazioneSelected != null) {
			this.descrizioneOperazione = tipoOperazioneSelected.getDescrizione();
		}
	}

	/**
	 * Imposta la coda di lavoro selezionata.
	 * @param codaDiLavoroSelected
	 */
	public void setCodaDiLavoroSelected(final CodaDiLavoroReport codaDiLavoroSelected) {
		this.codaDiLavoroSelected = codaDiLavoroSelected;
	}

	/**
	 * Imposta il flag associato alla ricerca per utente.
	 * @param ricercaPerUtente
	 */
	public void setRicercaPerUtente(final boolean ricercaPerUtente) {
		this.ricercaPerUtente = ricercaPerUtente;
	}
	
	/**
	 * Restituisce true se tabella risultati visibile, false altrimenti.
	 * @return true se tabella risultati visibile, false altrimenti
	 */
	public boolean isShowTabellaRisultati() {
		return showTabellaRisultati;
	}
	
	/**
	 * Imposta la visibilita della tabella dei risultati.
	 * @param showTabellaRisultati
	 */
	public void setShowTabellaRisultati(final boolean showTabellaRisultati) {
		this.showTabellaRisultati = showTabellaRisultati;
	}
	
	/**
	 * Restituisce l'id dell'Area Organizzatava Omogenea.
	 * @return id AOO
	 */
	public Long getIdAoo() {
		return idAoo;
	}

	/**
	 * Imposta l'id dell'AOO.
	 * @param idAoo
	 */
	public void setIdAoo(final Long idAoo) {
		this.idAoo = idAoo;
	}
}
