package it.ibm.red.web.mbean.interfaces;

import java.io.Serializable;

/**
 * Metodo implementato da un chiamante di un dettaglio detto master.
 * 
 * Quando il dettaglio viene aggiornato deve essere aggiornato anche il master.
 * 
 * Il dettaglio chiama il master mediante questa interfaccia per assicurarsi che il master sia aggiornato
 * 
 * @author a.difolca
 *
 * @param <T>
 *	dettaglio aggiornato passato al master
 */
public interface IUpdatableDetailCaller<T> extends Serializable {
	
	/**
	 * Il dettaglio chiama questo metodo per avvertire il master che il dettaglio è stato modificato 
	 * 
	 * @param detail
	 * 	dettaglio modificato sulla base del quale è necessario aggiornare il master
	 */
	void updateDetail(T detail);
}
