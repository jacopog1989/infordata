package it.ibm.red.web.document.mbean.component;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.RicercaPerBusinessEntityEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IRicercaFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.mbean.AbstractBean;

/**
 * Gestisce al visualizzazione e la ricerca dell'input del fascicolo Scrive nel
 * bean detail la selezione dell'utente
 *
 */
public class NomeFascicoloBean extends AbstractBean {

	/*
	 * 
	 */
	private static final long serialVersionUID = 97408277951643170L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NomeFascicoloBean.class);

	/**
	 * Servizio.
	 */
	private final IRicercaFacadeSRV ricercaSRV;

	/**
	 * Dettaglio.
	 */
	private DetailDocumentRedDTO detail;

	/**
	 * Utente.
	 */
	private final UtenteDTO utente;

	/**
	 * Per visualizzare i risultati della ricerca.
	 */
	private List<FascicoloDTO> ricercaFascicoliResults;

	/*** campi per il form della ricerca **/
	/**
	 * Chiave ricerca fascicolo.
	 */
	private String ricercaFascicoloKey;

	/**
	 * Ricerca fascicolo per anno.
	 */
	private Integer ricercaFascicoloAnno;

	/**
	 * Modalità ricerca.
	 */
	private RicercaGenericaTypeEnum ricercaFascicoloModalita;

	/**
	 * Lista modalità ricerca.
	 */
	private Collection<RicercaGenericaTypeEnum> comboRicercaFascicoloModalita;

	/**
	 * Costruttore del bean.
	 * 
	 * @param detail
	 * @param utente
	 */
	public NomeFascicoloBean(final DetailDocumentRedDTO detail, final UtenteDTO utente) {
		this.detail = detail;
		this.utente = utente;
		this.ricercaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaFacadeSRV.class);
		initializeRicercaForm();
	}

	private void initializeRicercaForm() {
		this.ricercaFascicoloAnno = Calendar.getInstance().get(Calendar.YEAR);
		this.comboRicercaFascicoloModalita = ricercaSRV.retrieveRicercaGenericaModalita();
	}

	/**
	 * Gestisce la creazione del fascicolo.
	 */
	public void cercaFascicolo() {
		try {
			final Collection<FascicoloDTO> fasColl = ricercaSRV.ricercaGenericaFascicoli(this.ricercaFascicoloKey, this.ricercaFascicoloAnno, this.ricercaFascicoloModalita,
					RicercaPerBusinessEntityEnum.FASCICOLI, utente);
			if (fasColl != null && !fasColl.isEmpty()) {
				ricercaFascicoliResults = new ArrayList<>(fasColl);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Impossibile recuperare i fascicoli ricercati");
		}
	}

	/**
	 * Gestisce la selezione del fascicolo.
	 * 
	 * @param index
	 */
	public void selectFascicolo(final Object index) {
		try {
			final Integer i = (Integer) index;
			if (i >= ricercaFascicoliResults.size()) {
				throw new RedException("Indice di riga non valido.");
			}

			final FascicoloDTO fProcedimentale = ricercaFascicoliResults.get(i);

			if (this.detail.getFascicoli() == null) {
				this.detail.setFascicoli(new ArrayList<>());
			}

			// se c'e' piu' di un fascicolo devo rimuovere il fascicolo procedimentale in
			// cui e' contenuto
			// e mettere quello selezionato dall'utente
			// se invece il fascicolo e' uno solo semplicemente svuoto la lista
			if (this.detail.getFascicoli().size() > 1) {
				int indexToRemove = 0;
				for (final FascicoloDTO f : this.detail.getFascicoli()) {
					if (f.getIdFascicolo().equals(this.detail.getIdFascicoloProcedimentale())) {
						break;
					}
					indexToRemove++;
				}
				this.detail.getFascicoli().remove(indexToRemove);
			} else {
				this.detail.getFascicoli().clear();
			}
			this.detail.getFascicoli().add(fProcedimentale);

			this.detail.setIdFascicoloProcedimentale(fProcedimentale.getIdFascicolo());
			if (!StringUtils.isNullOrEmpty(fProcedimentale.getOggetto())) {
				final String[] arr = fProcedimentale.getOggetto().split("_");
				if (arr.length >= 3) {
					this.detail.setPrefixFascicoloProcedimentale(arr[0] + "_" + arr[1] + "_");
					final StringBuilder descFasc = new StringBuilder("");
					for (int maxLenght = 2; maxLenght < arr.length; maxLenght++) {
						descFasc.append(StringUtils.isNullOrEmpty(descFasc.toString()) ? arr[maxLenght] : ("_" + arr[maxLenght]));
						this.detail.setDescrizioneFascicoloProcedimentale(descFasc.toString());
					}

				}
			}
			this.detail.setIndiceClassificazioneFascicoloProcedimentale(fProcedimentale.getIndiceClassificazione());

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}
	}

	/** SEMPLICI SETTER E GETTER **/
	/**
	 * Restituisce il dettaglio.
	 * 
	 * @return detail
	 */
	public DetailDocumentRedDTO getDetail() {
		return detail;
	}

	/**
	 * Imposta il dettaglio.
	 * 
	 * @param detail
	 */
	public void setDetail(final DetailDocumentRedDTO detail) {
		this.detail = detail;
	}

	/**
	 * Restituisce la lista dei fascicoli recuperati nella ricerca.
	 * 
	 * @return lista dei fascicoli recuperati nella ricerca
	 */
	public List<FascicoloDTO> getRicercaFascicoliResults() {
		return ricercaFascicoliResults;
	}

	/**
	 * Imposta la lista dei fascicoli recuperati nella ricerca.
	 * 
	 * @param ricercaFascicoliResults
	 */
	public void setRicercaFascicoliResults(final List<FascicoloDTO> ricercaFascicoliResults) {
		this.ricercaFascicoliResults = ricercaFascicoliResults;
	}

	/**
	 * Restituisce la key del fascicolo di ricerca.
	 * 
	 * @return key del fascicolo di ricerca
	 */
	public String getRicercaFascicoloKey() {
		return ricercaFascicoloKey;
	}

	/**
	 * Imposta la key del fascicolo di ricerca.
	 * 
	 * @param ricercaFascicoloKey
	 */
	public void setRicercaFascicoloKey(final String ricercaFascicoloKey) {
		this.ricercaFascicoloKey = ricercaFascicoloKey;
	}

	/**
	 * Restituisce l'anno di ricerca fascicolo.
	 * 
	 * @return anno di ricerca fascicolo
	 */
	public Integer getRicercaFascicoloAnno() {
		return ricercaFascicoloAnno;
	}

	/**
	 * Imposta l'anno di ricerca fascicolo.
	 * 
	 * @param ricercaFascicoloAnno
	 */
	public void setRicercaFascicoloAnno(final Integer ricercaFascicoloAnno) {
		this.ricercaFascicoloAnno = ricercaFascicoloAnno;
	}

	/**
	 * Restituisce la modalita di ricerca dei fascicoli.
	 * 
	 * @return modalita di ricerca dei fascicoli
	 */
	public RicercaGenericaTypeEnum getRicercaFascicoloModalita() {
		return ricercaFascicoloModalita;
	}

	/**
	 * Imposta la modalita di ricerca dei fascicoli.
	 * 
	 * @param ricercaFascicoloModalita
	 */
	public void setRicercaFascicoloModalita(final RicercaGenericaTypeEnum ricercaFascicoloModalita) {
		this.ricercaFascicoloModalita = ricercaFascicoloModalita;
	}

	/**
	 * Restituisce i tipi di modalita di ricerca per il popolamento della combobox.
	 * 
	 * @return tipi di modalita ricerca
	 */
	public Collection<RicercaGenericaTypeEnum> getComboRicercaFascicoloModalita() {
		return comboRicercaFascicoloModalita;
	}

	/**
	 * @see it.ibm.red.web.mbean.AbstractBean#postConstruct().
	 */
	@Override
	protected void postConstruct() {
		throw new UnsupportedOperationException("Questo bean non deve essere gestito dal framework");
	}
}
