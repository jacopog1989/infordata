package it.ibm.red.web.mbean;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.DetailProtocolloPregressoDTO;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.mbean.interfaces.IUpdatableDetailCaller;
/**
 * @author VINGENITO
 *
 */
@Named(ConstantsWeb.MBean.DETTAGLIO_PROTOCOLLO_RICERCA_NSD_BEAN)
@ViewScoped
public class DettaglioProtocolloRicercaNsdBean extends DettaglioProtocolloNsdAbstractBean implements IUpdatableDetailCaller<DetailProtocolloPregressoDTO> {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -9128487495106470207L;
    
	/**
	 * Caller.
	 */
	private IUpdatableDetailCaller<DetailProtocolloPregressoDTO> caller; 

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		super.postConstruct();
	}

	/**
	 * Aggiorna il dettaglio.
	 * 
	 * @param detail
	 *            dettaglio da aggiornare
	 */
	@Override
	public void updateDetail(final DetailProtocolloPregressoDTO detail) {
		this.protocolloCmp.setDetail(detail);
		caller.updateDetail(detail);
	}

	/**
	 * Setting del caller da impostare quando si imposta il dettaglio della ricerca.
	 * @param inCaller
	 */
	public void setCaller(final IUpdatableDetailCaller<DetailProtocolloPregressoDTO> inCaller) {
		this.caller = inCaller;
	}
}