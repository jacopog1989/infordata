package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;

import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.KeyValueDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IContributoFacadeSRV;
import it.ibm.red.business.service.facade.IOperationDocumentFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * @author m.crescentini
 *
 */
@Named(MBean.RICHIEDI_SOLLECITO_BEAN)
@ViewScoped
public class RichiediSollecitoBean extends AbstractBean implements InitHumanTaskInterface {

	private static final long serialVersionUID = -361970520692874816L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RichiediSollecitoBean.class);

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Servizio.
	 */
	private IContributoFacadeSRV contributoSRV;

	/**
	 * Servizio.
	 */
	private IOperationDocumentFacadeSRV opDocSRV;

	/**
	 * Destinatari.
	 */
	private SimpleDetailDataTableHelper<KeyValueDTO> destinatariContributiDTH;

	/**
	 * Documento selezionato.
	 */
	private MasterDocumentRedDTO docSelezionato;

	/**
	 * Oggetto contributo.
	 */
	private String oggettoDocContributo;

	/**
	 * Document title contributo.
	 */
	private String documentTitleDocContributo;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		utente = ((SessionBean) FacesHelper.getManagedBean(MBean.SESSION_BEAN)).getUtente();

		contributoSRV = ApplicationContextProvider.getApplicationContext().getBean(IContributoFacadeSRV.class);
		opDocSRV = ApplicationContextProvider.getApplicationContext().getBean(IOperationDocumentFacadeSRV.class);
	}

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		destinatariContributiDTH = new SimpleDetailDataTableHelper<>();

		try {
			setDocSelezionato(inDocsSelezionati.get(0));

			// ### Si verifica che il documento dal quale è stato richiesto il sollecito
			// abbia un corrispondente contributo esterno non concluso (senza risposta)
			// N.B. È possibile avere un solo Contributo Esterno con iter non concluso
			// (senza risposta) per volta
			// Si caricano i contributi esterni
			final Collection<DetailDocumentRedDTO> contributiDocSelezionato = contributoSRV.getContributiAttivi(docSelezionato.getNumeroDocumento(),
					docSelezionato.getAnnoDocumento(), utente);

			if (!CollectionUtils.isEmpty(contributiDocSelezionato)) {
				final List<KeyValueDTO> destinatariContributi = new ArrayList<>();

				// ### Si considera il contributo esterno più recente (per data creazione) come
				// non concluso (senza risposta)
				DetailDocumentRedDTO docContributoNonConcluso = null;
				Optional<DetailDocumentRedDTO> optDetail = contributiDocSelezionato.stream().max(Comparator.comparing(DetailDocumentRedDTO::getDateCreated));
				if (optDetail.isPresent()) {
					docContributoNonConcluso = optDetail.get();
				} else {
					throw new RedException("Contributo non concluso non presente.");
				}
				
				if(docContributoNonConcluso.getWobNumberPrincipale() != null && 
						!"".equals(docContributoNonConcluso.getWobNumberPrincipale())) {
					
					showError("Per questo documento è stato richiesto un contributo esterno non ancora spedito");

				} else {
					oggettoDocContributo = docContributoNonConcluso.getOggetto();
					documentTitleDocContributo = docContributoNonConcluso.getDocumentTitle();
					
					final Collection<DestinatarioRedDTO> destinatariDoc = docContributoNonConcluso.getDestinatari();
					if (!CollectionUtils.isEmpty(destinatariDoc)) {
						KeyValueDTO mappingDest = null;

						for (final DestinatarioRedDTO dest : destinatariDoc) {
							String mail = dest.getContatto().getMailPec() != null ? dest.getContatto().getMailPec() : dest.getContatto().getMail();

							if (StringUtils.isNotBlank(mail)) {
								mappingDest = new KeyValueDTO(mail, dest);
								mappingDest.setSelected(false);
								
								destinatariContributi.add(mappingDest);
							}
						}
					}
					
					destinatariContributiDTH.setMasters(destinatariContributi);
				}

			} else {
				showError("Per questo documento non è stato richiesto alcun contributo esterno; "
						+ "non esistono documenti aventi annoMittenteContributo e numeroMittenteContributo valorizzati rispettivamente " + docSelezionato.getAnnoDocumento()
						+ ", " + docSelezionato.getNumeroDocumento());
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * @param event
	 */
	public void updateSelected(final AjaxBehaviorEvent event) {
		final KeyValueDTO masterChecked = getDataTableClickedRow(event);

		final Boolean newSelectedValue = ((SelectBooleanCheckbox) event.getSource()).isSelected();
		if (Boolean.TRUE.equals(newSelectedValue)) {
			destinatariContributiDTH.getSelectedMasters().add(masterChecked);
		} else {
			destinatariContributiDTH.getSelectedMasters().remove(masterChecked);
		}
	}

	/**
	 * Esecuzione della response.
	 */
	public void executeResponse() {
		try {
			final ListaDocumentiBean ldb = FacesHelper.getManagedBean(MBean.LISTA_DOCUMENTI_BEAN);
			// Si puliscono gli esiti
			ldb.cleanEsiti();

			if (!CollectionUtils.isEmpty(destinatariContributiDTH.getSelectedMasters())) {
				// Si chiama il service per l'esecuzione della response
				final EsitoOperazioneDTO esito = opDocSRV.richiediSollecito(docSelezionato.getWobNumber(), docSelezionato.getDocumentTitle(), documentTitleDocContributo,
						oggettoDocContributo, destinatariContributiDTH.getSelectedMasters().stream().map(KeyValueDTO::getKey).collect(Collectors.toList()), utente);

				// Impostazione dell'esito e chiusura della dialog
				ldb.getEsitiOperazione().add(esito);
				FacesHelper.executeJS("PF('dlgGeneric').hide();PF('dlgShowResult').show();");
				ldb.destroyBeanViewScope(MBean.RICHIEDI_SOLLECITO_BEAN);
			} else {
				showError("Selezionare almeno un destinatario");
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Restituisce il documento selezionato.
	 * 
	 * @return documento selezionato
	 */
	public MasterDocumentRedDTO getDocSelezionato() {
		return docSelezionato;
	}

	/**
	 * Imposta il documento selezionato.
	 * 
	 * @param docSelezionato
	 */
	public void setDocSelezionato(final MasterDocumentRedDTO docSelezionato) {
		this.docSelezionato = docSelezionato;
	}

	/**
	 * Restituisce il datatable helper dei destinatari contributi.
	 * 
	 * @return helper datatable destinatari contributi
	 */
	public SimpleDetailDataTableHelper<KeyValueDTO> getDestinatariContributiDTH() {
		return destinatariContributiDTH;
	}
}