package it.ibm.red.web.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.LocalSignContentDTO;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.enums.TrasformazionePDFInErroreEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IASignFacadeSRV;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.lsign.LocalSignHelper;
import it.ibm.red.web.helper.lsign.LocalSignHelper.DocSignDTO;
import it.ibm.red.web.helper.lsign.LocalSignHelper.TipoDoc;
import it.ibm.red.web.servlets.delegate.CopiaConformeGetDelegate;
import it.ibm.red.web.servlets.delegate.CopiaConformePostDelegate;
import it.ibm.red.web.servlets.delegate.IGetDelegate;
import it.ibm.red.web.servlets.delegate.IPostDelegate;
import it.ibm.red.web.servlets.delegate.StandardGetDelegate;
import it.ibm.red.web.servlets.delegate.StandardPostDelegate;
import it.ibm.red.web.servlets.delegate.exception.DelegateException;

/**
 * Servlet firma locale.
 */
public class LocalSignServlet extends HttpServlet {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1206511287310745310L;
	
	/**
	 * Label firma documento. Occorre appendere l'identificativo del documento al messaggio.
	 */
	private static final String FIRMA_DOCUMENTO_START = "Firma del documento ";
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(LocalSignServlet.class.getName());
	
	/**
	 * Delegati get.
	 */
	private final Map<String, IGetDelegate> hmGetDelegates;
	
	/**
	 * Delegati post.
	 */
	private final Map<String, IPostDelegate> hmPostDelegates;
	
	
	/**
	 * Costruttore di default.
	 */
	public LocalSignServlet() {
		hmGetDelegates = new HashMap<>();
		hmPostDelegates = new HashMap<>();
	}

	/**
	 * Metodo per registrare un delegato per gestire la get (la chiave sarà il nome della classe).
	 * 
	 * @param clsDelegate	classe da registrare
	 */
    private void registerGetDelegate(final Class<? extends IGetDelegate> clsDelegate) {
    	try {
			hmGetDelegates.put(clsDelegate.getName(), clsDelegate.newInstance());
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di registrazione get delegate: ", e);
			throw new RedException(e);
		}
    }
    
	/**
	 * Metodo per registrare un delegato per gestire la post (la chiave sarà il nome della classe).
	 * 
	 * @param clsDelegate	la classe da registrare
	 */
    private void registerPostDelegate(final Class<? extends IPostDelegate> clsDelegate) {
    	try {
			hmPostDelegates.put(clsDelegate.getName(), clsDelegate.newInstance());
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di registrazione post delegate: ", e);
			throw new RedException(e);
		}
    }

	/**
	 * Metodo di inizializzazione della servlet.
	 */
    @Override
    public final void init() throws ServletException {
    	super.init();
        registerGetDelegate(StandardGetDelegate.class);
        registerGetDelegate(CopiaConformeGetDelegate.class);
        registerPostDelegate(StandardPostDelegate.class);
        registerPostDelegate(CopiaConformePostDelegate.class);
    }

    /**
     * Metodo per gestire la get della servlet.
     * 
	 * @param request	la request ricevuta
	 * @param response	la response generata
	 * @throws ServletException	servlet
	 * @throws IOException		gestione outstream
     */
    @Override
    public final void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException {
		LocalSignHelper lsh = null;
		byte[] content = null;
	    byte[] digest = null;
	    IGetDelegate getDelegate = null;
	    DocSignDTO docToSign = null;
    	try {
    		lsh = (LocalSignHelper) request.getSession().getAttribute(ConstantsWeb.SessionObject.SESSION_LSH);
    		if(lsh == null) {
    			throw new RedException("Impossibile recuperare il local sign helper dalla sessione");
    		} 
    		
    		IASignFacadeSRV aSignSRV = ApplicationContextProvider.getApplicationContext().getBean(IASignFacadeSRV.class);
    		
    		if(StringUtils.isNullOrEmpty(lsh.getSignTransactionId())) {
    			lsh.setSignTransactionId(aSignSRV.getSignTransactionId(lsh.getUtente(), "Locale"));
    		}
    		
    		getDelegate = hmGetDelegates.get(request.getParameter(Constants.SignServlet.DELEGATE));
	    	
	    	// Recupero l'identificativo del prossimo documento da firmare
			docToSign = lsh.nextDocToSign();
	    	
	    	LOGGER.info(lsh.getSignTransactionId() + " - " + FIRMA_DOCUMENTO_START + docToSign.getDocumentTitle() + " => "
						+ ((TipoDoc.PRINCIPALE.equals(docToSign.getTipoDoc())) ? "PRINCIPALE" : "ALLEGATO DI " + docToSign.getIdDocPrincipale()));

			if (TipoDoc.PRINCIPALE.equals(docToSign.getTipoDoc())) {
		    	// Cerca allegati da firmare
		    	List<AllegatoDTO> allegatiIdSet = getDelegate.getAllegatiToSign(lsh.getSignTransactionId(), docToSign.getDocumentTitle(), docToSign.getGuid(), lsh.getUtente(), lsh.getSte());
		    	for (AllegatoDTO allegato : allegatiIdSet) {
		    		// Aggiungi gli allegati fra i documenti da firmare
		    		lsh.addDocToSign(docToSign.getWobNumber(), allegato.getDocumentTitle(), allegato.getGuid(), docToSign.getNumeroDocumentoPrincipale(), TipoDoc.ALLEGATO, allegato.getNomeFile(), docToSign.getSottoCategoriaDocumentoUscita());
		    		LOGGER.info(lsh.getSignTransactionId() + " - " + FIRMA_DOCUMENTO_START + docToSign.getDocumentTitle() + ": aggiunto allegato " + allegato.getDocumentTitle() + " fra i documenti da firmare");
		    	}
		    	
		    	// Se è Copia Conforme non devo firmare il principale, ma solo gli allegati
		    	if (lsh.isCopiaConforme()) {
		    		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(docToSign.getWobNumber());
					esito.setEsito(true);
					esito.setIdDocumento(docToSign.getNumeroDocumentoPrincipale());
					esito.setNomeDocumento(docToSign.getNomeFile());
		    		lsh.removeDocToSign(esito);
		    		docToSign = lsh.nextDocToSign();
		    	} else {
		    		LOGGER.info(lsh.getSignTransactionId() + " - " + FIRMA_DOCUMENTO_START + docToSign.getDocumentTitle() + " => esecuzione delle operazioni propedeutiche alla firma dei content");
		    		// Esecuzione delle operazioni propedeutiche alla firma dei content. N.B. Le operazioni eseguite dipendono dalla strategia di firma dell'AOO.
		    		EsitoOperazioneDTO esitoOperazioniPreFirma = aSignSRV.preFirmaLocale(lsh.getSignTransactionId(), docToSign.getWobNumber(), lsh.getUtente(), lsh.getSte(), lsh.isFirmaMultipla());
		    		if (!esitoOperazioniPreFirma.isEsito()) {
		    			throw new DelegateException("Errore nelle operazioni propedeutiche alla firma");
		    		}
		    	}
		    }
	
		    // Invoco il delegato per il recupero del contenuto a partire dall'identificativo del prossimo documento da firmare
		    LocalSignContentDTO docContent = getDelegate.getDocToSignContent(lsh.getSignTransactionId(), docToSign, lsh.getUtente(), lsh.getSte(), lsh.isFirmaMultipla());
		    
		    // Recupero content ed eventuale digest (per firma PAdES)
		    content = docContent.getContent();
		    digest = docContent.getDigest();
		    
		    LOGGER.info(lsh.getSignTransactionId() + " - " + FIRMA_DOCUMENTO_START + docToSign.getDocumentTitle() + ": content size " + (content != null ? content.length : 0));
			LOGGER.info(lsh.getSignTransactionId() + " - " + FIRMA_DOCUMENTO_START + docToSign.getDocumentTitle() + ": digest size " + (digest != null ? digest.length : 0));

			// Salvo in sessione il content stampigliato
		    docToSign.setContentStampigliato(content);
		    docToSign.setDataFirma(docContent.getDataFirma());
		    writeData(response, content, digest);
		} catch (DelegateException e) {
			if(lsh != null) {
				manageDelegateException(e, lsh);
				lsh.getDocSigning().setDoGetError(true);
			}
		} catch (Exception e) {
			try {
				getDelegate.aggiornaStatoDocumento(lsh.getSignTransactionId(), lsh.getUtente().getId(), docToSign.getIdDocPrincipale(), TrasformazionePDFInErroreEnum.KO_GENERICO.getValue());
			}catch(Exception ex) {
				LOGGER.error("Errore nell'aggiornamento del SEMAFORO FIRMA LOCALE  - doGet()", ex);
			}
			finally {
				LOGGER.error("SIGNING SERVLET doGet(): ", e);
			}
    	}
	}
    
    /**
     * @param response
     * @param content
     * @param digest
     */
    private void writeData(final HttpServletResponse response, final byte[] content, final byte[] digest) {
    	try {
    		byte[] data = digest;
			if (data == null || data.length == 0) {
				data = content;
			}
    		response.getOutputStream().write(data);
    	} catch (Exception e) {
    		LOGGER.error("Errore nella scrittura dei dati", e);
    	}
    }
    
    private static EsitoOperazioneDTO createEsito(final String wobNumber, final String esitoDesc, final boolean esitoB, final Integer numeroDocumentoPrincipale, final String nomeFile) {
    	EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
    	esito.setEsito(esitoB);
		esito.setNote(esitoDesc);
		esito.setIdDocumento(numeroDocumentoPrincipale);
		esito.setNomeDocumento(nomeFile);
		return esito;
    }
    
    private void manageDelegateException(final DelegateException e, final LocalSignHelper lsh) {
    	LOGGER.error("SIGNING SERVLET doGet(): ", e);
    	if(lsh != null) {
			EsitoOperazioneDTO esito = createEsito(lsh.getDocSigning().getWobNumber(), e.getMessage(), false, lsh.getDocSigning().getNumeroDocumentoPrincipale(), lsh.getDocSigning().getNomeFile());
			lsh.removeDocToSign(esito);
    	}
    }

    /**
     * Metodo per gestire la post della servlet.
     * 
	 * @param request	la request ricevuta
	 * @param response	la response generata
	 * @throws ServletException	servlet
	 * @throws IOException		gestione outstream
     */
    @Override
	public final void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException {
    	IPostDelegate postDelegate = null;
    	LocalSignHelper lsh = null;
    	DocSignDTO doc = null;
    	try {
	    	String source = request.getParameter(Constants.SignServlet.SOURCE);
			String delegate = request.getParameter(Constants.SignServlet.DELEGATE);
			postDelegate = hmPostDelegates.get(delegate);
	
			lsh = (LocalSignHelper) request.getSession().getAttribute(ConstantsWeb.SessionObject.SESSION_LSH);
			
			String error = request.getHeader("Error");
			String errorDesc = request.getHeader("ErrorDesc");
			if (error != null && error.length() > 0 && error.compareTo("0") != 0) {
				LOGGER.error("SIGNING SERVLET doPost() - http heade error: [" + error + "] " + errorDesc);
				// 271 => tasto ANNULLA
			    jobDone(request, response, source, "271".equals(error));
			} else {
				
				// Recupera l'identificativo univoco del documento firmato e la lista degli id dei documenti da firmare
				doc = lsh.getDocSigning();
				
				if (!doc.isDoGetError()) {
				
					// Recupera il content del documento firmato dalla request
					byte[] docSigned = null;
					if (SignTypeEnum.CADES.equals(lsh.getSte())) {
						docSigned = FileUtils.decodeBuffer(request.getInputStream());
					} else {
						docSigned = Base64.decodeBase64( IOUtils.toByteArray(request.getInputStream()));
					}
					
					LOGGER.info(lsh.getSignTransactionId() + " - " + FIRMA_DOCUMENTO_START + doc.getDocumentTitle() + ": docSigned size " + (docSigned != null ? docSigned.length : 0));

					// Invoco il delegato per il gestire il contenuto firmato
					manageSignedDoc(postDelegate, lsh, docSigned, doc);

				} else {

					LOGGER.info(lsh.getSignTransactionId() + " - " + FIRMA_DOCUMENTO_START + doc.getDocumentTitle() + ": errore in fase di recupero del content");

				}
				
			    if (lsh.hasNextDocIdToSign()) {
				    // Invoco il delegato per aggiornare l'applet per la prossima firma
				    postDelegate.updateResponseForNextAppletIteration(request, response, doc);
				} else {
					jobDone(request, response, source, true);
				}
			}
    	} catch (final Exception e) {
    		try {
				postDelegate.aggiornaStatoDocumento(lsh.getSignTransactionId(), lsh.getUtente().getId(), doc.getIdDocPrincipale(), TrasformazionePDFInErroreEnum.KO_GENERICO.getValue());
			}catch(Exception ex) {
				LOGGER.error("Errore nell'aggiornamento del SEMAFORO FIRMA LOCALE - doPost()", ex);
			}
			finally {
				LOGGER.error("SIGNING SERVLET doPost(): ", e);
			}
       	}
	}

	private void manageSignedDoc(final IPostDelegate postDelegate, final LocalSignHelper lsh, final byte[] docSigned, final DocSignDTO doc) {
		EsitoOperazioneDTO esito;
		try {
			
			esito = postDelegate.manageSignedDoc(lsh.getSignTransactionId(), doc, docSigned, lsh.getUtente(), lsh.getSte(), lsh.isLastForDocPrincipale(), lsh.isFirmaMultipla());
			
			//Rimuovi dalla lista delle pk dei documenti da firmare quella del documento firmato
		    lsh.removeDocToSign(esito);
		    
		} catch (final DelegateException e) {
			manageDelegateException(e, lsh);
		}
	}

	/**
	 * Metodo invocato quando tutti gli id dei documenti in sessione sono stati gestiti.
	 * 
	 * @param request		la request ricevuta
	 * @param response		la reponse generata
	 * @param source		la pagina in cui è presente l'applet di firma
	 * @param esito			true indica che la request è stata gestita senza errori
	 * @throws IOException	gestione outstream
	 */
	private void jobDone(final HttpServletRequest request, final HttpServletResponse response, final String source, final Boolean esito) throws IOException {
		LocalSignHelper lsh = (LocalSignHelper) request.getSession().getAttribute(ConstantsWeb.SessionObject.SESSION_LSH);
	    //Dichiaro completata la sessione di firma fornendo l'esito della sessione
    	lsh.signComplete();
		String strEsito = "false";
		if (esito != null && esito) {
			strEsito = "true";
		}
		response.getOutputStream().write((request.getContextPath() + "/" + source + "?finalstep=" + strEsito).getBytes());
	}
}