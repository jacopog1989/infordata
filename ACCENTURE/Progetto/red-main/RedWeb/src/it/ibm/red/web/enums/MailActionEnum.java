package it.ibm.red.web.enums;

/**
 * Quando l'utente clicca su una azione relativa a un email il sistema ha la necessità di ricordare quale azione è stata selezionata
 * 
 * Qualora sia necessario aggiungere l'azione
 *
 */
public enum MailActionEnum {

	/**
	 * Valore.
	 */
	PROTOCOLLA("Protocolla"),

	/**
	 * Valore.
	 */
	PROTOCOLLA_DSR("Protocolla Dsr"),

	/**
	 * Valore.
	 */
	RIFIUTA("Rifiuta"),

	/**
	 * Valore.
	 */
	INOLTRA("Inoltra"),

	/**
	 * Valore.
	 */
	ELIMINA("Elimina"),

	/**
	 * Valore.
	 */
	RIPRISTINA("Ripristina"),

	/**
	 * Valore.
	 */
	ACTION("mailaction"),

	/**
	 * Valore.
	 */
	CON_ALLEGATI("mailConAllegato"),

	/**
	 * Valore.
	 */
	MESSAGGIO_ERRORE("messaggioErroreInvioFirma"),

	/**
	 * Valore.
	 */
	MESSAGGIO_ERRORE_ALLEGATI("Attenzione, errore nel recupero degli allegati"),

	/**
	 * Valore.
	 */
	INOLTRA_DA_CODA("InoltraDaCoda"),

	/**
	 * Valore.
	 */
	ISPEZIONA_MAIL("Ispeziona Mail");
	

	/**
	 * Valori.
	 */
	private String value;
	
	/**
	 * Costruttore enum.
	 * @param value
	 */
	MailActionEnum(final String value) {
		this.value = value;
	}
	
	/**
	 * Restituisce il valore dell'Enum.
	 * @return value
	 */
	public String getValue() {
		return this.value;
	}
	
 }
