/**
 * 
 */
package it.ibm.red.web.ricerca.mbean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;

import it.ibm.red.business.dto.ParamsRicercaRegistrazioniAusiliarieDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaNPSDTO;
import it.ibm.red.business.dto.StatoRicercaDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.SessionBean;

/**
 * Bean per la maschera di ricerca delle registrazioni ausiliarie.
 * 
 * @author m.crescentini
 *
 */
@Named(ConstantsWeb.MBean.RISULTATI_RICERCA_REGISTRAZIONI_AUSILIARIE_BEAN)
@ViewScoped
public class RisultatiRicercaRegistrazioniAusiliarieBean extends RicercaAbstractBean<RegistrazioneAusiliariaNPSDTO> {

	private static final long serialVersionUID = -5035209986321063513L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RisultatiRicercaRegistrazioniAusiliarieBean.class.getName());

    /**
     * Parametri ricerca.
     */
	private ParamsRicercaRegistrazioniAusiliarieDTO formRicerca;
	
    /**
     * Datatable registrazioni.
     */
	private SimpleDetailDataTableHelper<RegistrazioneAusiliariaNPSDTO> registrazioniAusiliarieDTH;
	
    /**
     * Colonne.
     */
	private List<Boolean> columns = new ArrayList<>();
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		super.postConstruct();
		
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		
		initPaginaRicerca();
	}
	
	/**
	 * Inizializza la pagina di ricerca delle registrazioni ausiliarie.
	 */
	@SuppressWarnings("unchecked")
	public void initPaginaRicerca() {
		try {
			registrazioniAusiliarieDTH = new SimpleDetailDataTableHelper<>();
			registrazioniAusiliarieDTH.setMasters(new ArrayList<>());
			
			columns = Arrays.asList(true, true, true, true, false, false);
			
			final StatoRicercaDTO stDto = (StatoRicercaDTO) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.STATO_RICERCA, true);
			
			if (stDto != null) {
				final List<RegistrazioneAusiliariaNPSDTO> searchResult = (List<RegistrazioneAusiliariaNPSDTO>) stDto.getRisultatiRicerca();
				formRicerca = (ParamsRicercaRegistrazioniAusiliarieDTO) stDto.getFormRicerca();
				
				registrazioniAusiliarieDTH.setMasters(searchResult);
			
				if (getNumMaxRisultati() != null && searchResult != null && getNumMaxRisultati().equals(searchResult.size())) {
					showWarnMessage(RicercaAbstractBean.MSG_NUM_MAX_RISULTATI);
				}
			} else {
				formRicerca = new ParamsRicercaRegistrazioniAusiliarieDTO();
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Non è stato possibile recuperare i risultati di ricerca");
		}
	}
	
	
	/**
	 * Metodo per reimpostare nel SessionBean il form utilizzato per la ricerca e quindi visualizzarlo nel dialog.
	 */
	public void ripetiRicerca() {
		try {
			// Viene ripetuta la ricerca con i stessi filtri
			sessionBean.getRicercaFormContainer().setRegistrazioniAusiliarie(formRicerca);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}
	
	
	/**
	 * @param event
	 */
	public final void onColumnToggle(final ToggleEvent event) {
		columns.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
	}
	

	/**
	 * @see it.ibm.red.web.ricerca.mbean.RicercaAbstractBean#getSelectedDocument()
	 */
	@Override
	protected RegistrazioneAusiliariaNPSDTO getSelectedDocument() {
		return registrazioniAusiliarieDTH.getCurrentMaster();
	}
	

	/* (non-Javadoc)
	 * @see it.ibm.red.web.ricerca.mbean.RicercaAbstractBean#selectDetail(java.lang.Object)
	 */
	@Override
	protected void selectDetail(final RegistrazioneAusiliariaNPSDTO m) {
		// Non è previsto il dettaglio per questa ricerca.
	}
	

	/**
	 * @see it.ibm.red.web.ricerca.mbean.RicercaAbstractBean#getNomeRicerca4Excel()
	 */
	@Override
	protected String getNomeRicerca4Excel() {
		return "ricercaregistrazioniausiliarie";
	}


	/**
	 * @return the columns
	 */
	public List<Boolean> getColumns() {
		return columns;
	}


	/**
	 * @param columns the columns to set
	 */
	public void setColumns(final List<Boolean> columns) {
		this.columns = columns;
	}


	/**
	 * @return the registrazioniAusiliarieDTH
	 */
	public SimpleDetailDataTableHelper<RegistrazioneAusiliariaNPSDTO> getRegistrazioniAusiliarieDTH() {
		return registrazioniAusiliarieDTH;
	}

}
