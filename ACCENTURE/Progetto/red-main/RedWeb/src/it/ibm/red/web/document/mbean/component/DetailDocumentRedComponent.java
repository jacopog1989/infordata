package it.ibm.red.web.document.mbean.component;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.google.common.net.MediaType;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.crypt.DesCrypterNew;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DetailFascicoloRedDTO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.VersionDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.facade.IContributoFacadeSRV;
import it.ibm.red.business.service.facade.IDetailFascicoloFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV;
import it.ibm.red.business.service.facade.INpsFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.helper.faces.MessageSeverityEnum;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.FascicoloManagerBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.IDetailPreviewComponent;
import it.ibm.red.web.mbean.interfaces.IUpdatableDetailCaller;

/**
 * Component gestione dettaglio documento RED.
 */
public class DetailDocumentRedComponent extends AbstractBean implements IDetailPreviewComponent, IUpdatableDetailCaller<DetailFascicoloRedDTO> {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 5934068065672932350L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DetailDocumentRedComponent.class);

	/**
	 * Messaggio di errore associato al download elle etichette.
	 */
	private static final String ERROR_DOWNLOAD_ETICHETTE_MSG = "Impossibile effettuare il download delle etichette";

	/**
	 * Messaggio di errore generico, occorre appendere l'operazione al messaggio.
	 */
	private static final String GENERIC_ERROR_MSG = "Errore durante l'operazione: ";

	/**
	 * Servizio.
	 */
	private final IDocumentManagerFacadeSRV documentManagerSRV;

	/**
	 * Servizio.
	 */
	private final IDetailFascicoloFacadeSRV fascicoloSRV;

	/**
	 * Dati utente.
	 */
	private UtenteDTO utente;

	/**
	 * Servizio.
	 */
	private final IContributoFacadeSRV contributoSRV;

	/**
	 * Servizio.
	 */
	private final IDocumentoFacadeSRV documentoSRV;

	/**
	 * Servizio.
	 */
	private final IDocumentoRedFacadeSRV documentoRedSRV;

	/**
	 * Informazioni master.
	 */
	private MasterDocumentRedDTO master;

	/**
	 * Informazioni dettaglio.
	 */
	private DetailDocumentRedDTO detail;

	/**
	 * Identificativo componnete chiamante.
	 */
	private String chiamante;

	/**
	 * Fascicolo che è stato appena aperto dal dettaglio Esteso.
	 */
	private FascicoloDTO currentFascicoloEsteso;

	/**
	 * Document title del documento selezionato .
	 */
	private String documentTitleSelected;
	/**
	 * mimetype del file visualizzato nella preview .
	 */
	private String mimeTypeSelected;
	/**
	 * nome del file visualizzato nella preview .
	 */
	private String nomeFileSelected;

	/**
	 * Servizio nps.
	 */
	private final INpsFacadeSRV npsSRV;

	/**
	 * Bean sessione.
	 */
	private final SessionBean sessionBean;

	/**
	 * Flag integrazione dati visibile.
	 */
	private boolean isIntegrazioneDatiVisible;

	/**
	 * Costruttore del component. Inizializza tutti i service che usa la classe e il
	 * SessionBean.
	 * 
	 * @param utenteIn
	 */
	public DetailDocumentRedComponent(final UtenteDTO utenteIn) {
		this.utente = utenteIn;
		documentManagerSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentManagerFacadeSRV.class);
		fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IDetailFascicoloFacadeSRV.class);
		contributoSRV = ApplicationContextProvider.getApplicationContext().getBean(IContributoFacadeSRV.class);
		documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);
		documentoRedSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoRedFacadeSRV.class);
		npsSRV = ApplicationContextProvider.getApplicationContext().getBean(INpsFacadeSRV.class);
		sessionBean = (SessionBean) FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
	}

	/**
	 * Restituisce true se il numero protocollo risulta valido, false altrimenti.
	 * 
	 * @return true se protocollo valido, false altrimenti
	 */
	public boolean isProtocollo() {
		if (detail == null) {
			return false;
		}
		return detail.getNumeroProtocollo() != null && detail.getNumeroProtocollo().intValue() > 0;
	}

	/**
	 * TAB DOCUMENTI START
	 **************************************************************************************************************/

	/**
	 * Restituisce lo StreamedContent della versione definita dall'indice del
	 * documento principale.
	 * 
	 * @param index
	 * @return StreamedContent per download
	 */
	public StreamedContent downloadVersioneDocumentoPrincipale(final Object index) {

		StreamedContent file = null;
		try {
			final Integer i = (Integer) index;

			final VersionDTO v = detail.getVersioni().get(i);

			final InputStream stream = documentManagerSRV.getStreamDocumentoByGuid(v.getGuid(), utente);

			file = new DefaultStreamedContent(stream, v.getMimeType(), v.getFileName());

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_MSG + e.getMessage());
		}
		return file;

	}

	/**
	 * Restituisce lo StreamedContent per il download. Lo StreamedContent è
	 * associato alla versione definita dall'indice passato come parametro.
	 * 
	 * @param index
	 * @param guid
	 * @return
	 */
	public StreamedContent downloadVersioneDocumentoAllegato(final Object index, final String guid) {

		StreamedContent file = null;

		try {
			AllegatoDTO allegatoSelected = null;
			for (final AllegatoDTO a : detail.getAllegati()) {
				if (a.getGuid().equals(guid)) {
					allegatoSelected = a;
					break;
				}
			}

			if (allegatoSelected == null) {
				throw new RedException("recupero allegato");
			}

			final Integer i = (Integer) index;
			final VersionDTO v = allegatoSelected.getVersions().get(i);

			final InputStream stream = documentManagerSRV.getStreamDocumentoByGuid(v.getGuid(), utente);

			file = new DefaultStreamedContent(stream, v.getMimeType(), v.getFileName());

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_MSG + e.getMessage());
		}

		return file;
	}

	/**
	 * TAB DOCUMENTI END
	 ****************************************************************************************************************/

	/**
	 * Metodo per la realizzazione del report delle approvazioni.
	 * 
	 * @return stream contenente il report
	 */
	public StreamedContent getReportApprovazioni() {
		StreamedContent output = null;
		try {
			final String ispettoratoPreponente = detail.getUfficioMittente().getDescrizione();
			// preparazione del content con all'interno le informazioni sottoposte ad
			// approvazione
			final byte[] content = PdfHelper.createApprovazioniDocument(ispettoratoPreponente, detail.getNumeroDocumento(), detail.getApprovazioni());
			output = new DefaultStreamedContent(new ByteArrayInputStream(content), MediaType.PDF.toString(), "report_approvazioni_" + detail.getNumeroDocumento() + ".pdf");
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di generazione del report delle approvazioni.", e);
			showError(e);
		}

		return output;
	}

	/**
	 * @return
	 */
	@Override
	public final String getCryptoId() {
		if (detail == null) {
			return "";
		}

		return getCryptoDocParamById(documentTitleSelected);
	}

	/**
	 * Recupera i parametri criptati utilizzati per la Servlet
	 * 'DownloadContentServlet'.
	 * 
	 * @param id - Identificativo del documento
	 * @return Parametri cryptati.
	 */
	private String getCryptoDocParamById(final String id) {
		final String toEncString = "id=" + id + "&username=" + utente.getUsername() + "&idRuolo=" + utente.getIdRuolo() + "&idNodo=" + utente.getIdUfficio() + "&ts="
				+ new Date().getTime();
		try {
			final boolean doLog = !"true".equals(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DISABLE_LOG_DOWNLOAD_CONTENT_SERVLET));
			return URLEncoder.encode(DesCrypterNew.encrypt(doLog, toEncString), "UTF-8");
		} catch (final Exception e) {
			LOGGER.error("Errore nell'encrypt dei parametri per la servlet 'DownloadContentServlet':", e);
			showError("Errore nell'encrypt dei parametri per la servlet 'DownloadContentServlet': " + e.getMessage());
		}
		return "ERROREDICODIFICA";
	}

	/**
	 * Aggiorna i parametri del component alla selezione.
	 */
	public void changeSelectedDocumentTitle() {
		documentTitleSelected = (String) FacesHelper.getParameterFromRequest(Constants.RequestParameter.DOCUMENT_TITLE);
		final String mimeType = (String) FacesHelper.getParameterFromRequest(Constants.RequestParameter.DOCUMENT_MIMETYPE_FILE_SELECTED);
		mimeTypeSelected = mimeType;
		nomeFileSelected = (String) FacesHelper.getParameterFromRequest(Constants.RequestParameter.DOCUMENT_NAME_FILE_SELECTED);
	}

	/**
	 * Utilizzato nel link sul nome del fascicolo dalla lista dei fascicoli di un
	 * documento.
	 * 
	 * @param index
	 */
	public void openFascicoloView(final Object index) {
		try {
			final Integer i = (Integer) index;
			final FascicoloDTO f = detail.getFascicoli().get(i);

			openFascicolo(f);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di recupero del fascicolo.");
		}
	}

	/**
	 * Initializza il fascicolo manager e lo apre si occupa anche di predisporre il
	 * fascicolo per l'eventuale update.
	 * 
	 * @param f
	 */
	public void openFascicolo(final FascicoloDTO f) {
		currentFascicoloEsteso = f;

		final DetailFascicoloRedDTO df = fascicoloSRV.getFascicolo(f, utente);
		final FascicoloManagerBean bean = FacesHelper.getManagedBean(MBean.FASCICOLO_MANAGER_BEAN);
		bean.setDetail(df);
		bean.setDialogFascicoloVisible(true);
		bean.setChiamante(chiamante);
		bean.setCaller(this);

		FacesHelper.executeJS("PF('dlgManageFascicolo_DF').show()");
		FacesHelper.update("idDettagliEstesiForm:fascicoloMng_pnl");
	}

	/**
	 * Utilizzato per aggiornare il dettaglio del fascicolo procedimentale In una
	 * futura rivisitazione dovremo modificare questo metodo perché gestisca anche
	 * la modifica quando viene aperto fascicolo Manager dal link in documenti
	 * fascicolo Bisognerebbe riportare tutte le casistiche che vegono modificate da
	 * uno all'altro ma non saprei sinceramente come .
	 */
	@Override
	public void updateDetail(final DetailFascicoloRedDTO fascicoloUpdated) {
		final List<DocumentoFascicoloDTO> documenti = fascicoloUpdated.getDocumenti();
		final boolean documentoSpostato = documenti == null || documenti.stream().noneMatch(d -> detail.getDocumentTitle().equals(d.getDocumentTitle()));

		if (documentoSpostato) { // il documento è stato spostato in un altro fascicolo
			documentoRedSRV.retrieveFascicoliAndSetProcedimentale(detail, utente);
		} else { // Questo ramo comprende modifiche inferiori tuttavia prestazionalmente
					// vantaggioso perché non recupera nuovamente i fascicoli

			fascicoloSRV.convertDettaglioToFascicolo(fascicoloUpdated, currentFascicoloEsteso);
			if (getDetail() != null && getDetail().getIdFascicoloProcedimentale() != null
					&& getDetail().getIdFascicoloProcedimentale().equals(fascicoloUpdated.getNomeFascicolo()) 
					&& fascicoloUpdated.getTitolarioDTO() != null) {
				documentoRedSRV.setProcedimentale(detail, currentFascicoloEsteso);	
			}
		}
	}

	/*******************************************************************************/
	@Override
	protected void postConstruct() {
		// non ne ho bisogno, deve rimanere vuoto
	}

	/*******************************************************************************/

	/** GET & SET. ****************************************************************/

	public DetailDocumentRedDTO getDetail() {
		return detail;
	}

	/**
	 * Imposta il dettaglio gestendo la visibilità dell'integrazione dati.
	 * 
	 * @param detail
	 */
	public void setDetail(final DetailDocumentRedDTO detail) {
		this.detail = detail;
		documentTitleSelected = detail.getDocumentTitle(); // di default imposto quello del documento principale
		mimeTypeSelected = detail.getMimeType();
		nomeFileSelected = detail.getNomeFile();
		final TipoCategoriaEnum tce = (CategoriaDocumentoEnum.ENTRATA.equals(CategoriaDocumentoEnum.get(detail.getIdCategoriaDocumento()))
				|| CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA.equals(CategoriaDocumentoEnum.get(detail.getIdCategoriaDocumento()))) ? TipoCategoriaEnum.ENTRATA
						: TipoCategoriaEnum.USCITA;
		setIntegrazioneDatiVisible(utente.isUcb() && TipoCategoriaEnum.ENTRATA.equals(tce));
	}

	/**
	 * Effettua un reset sul dettaglio riportando a null i parametri.
	 */
	public void unsetDetail() {
		this.detail = null;
		documentTitleSelected = null;
		mimeTypeSelected = null;
		nomeFileSelected = null;
	}

	/**
	 * Restituisce l'utente come UtenteDTO.
	 * 
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'utente.
	 * 
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Restituisce il mime type del content.
	 * 
	 * @return mimeTypeSelected
	 */
	public String getMimeTypeSelected() {
		return mimeTypeSelected;
	}

	/**
	 * Restituisce il file name del content selezionato.
	 * 
	 * @return nomeFileSelected
	 */
	public String getNomeFileSelected() {
		return nomeFileSelected;
	}

	/**
	 * Restituisce il document title selezionato.
	 * 
	 * @return documentTitleSelected
	 */
	public String getDocumentTitleSelected() {
		return documentTitleSelected;
	}

	/**
	 * Imposta il document title selezionato.
	 * 
	 * @param documentTitleSelected
	 */
	public void setDocumentTitleSelected(final String documentTitleSelected) {
		this.documentTitleSelected = documentTitleSelected;
	}

	/**
	 * Imposta il mime type selezionato.
	 * 
	 * @param mimeTypeSelected
	 */
	public void setMimeTypeSelected(final String mimeTypeSelected) {
		this.mimeTypeSelected = mimeTypeSelected;
	}

	/**
	 * Imposta il file name selezionato.
	 * 
	 * @param nomeFileSelected
	 */
	public void setNomeFileSelected(final String nomeFileSelected) {
		this.nomeFileSelected = nomeFileSelected;
	}

	/**
	 * Restituisce lo StreamedContent recuperato dal content associato al contributo
	 * identificato dall'index.
	 * 
	 * @param index
	 * @return StreamedContent
	 */
	public StreamedContent downloadContributo(final Object index) {
		StreamedContent file = null;
		try {
			final Integer i = (Integer) index;
			final ContributoDTO contributo = detail.getContributi().get(i);
			InputStream stream = null;
			final String dt = contributo.getDocumentTitle();
			byte[] content = null;
			if (dt != null) {
				// Contributo su FileNet
				final FileDTO contr = documentoSRV.getContributoContentPreview(utente.getFcDTO(), dt, utente.getIdAoo());
				content = contr.getContent();
			} else {
				// Contributo generato On-The-Fly
				final FileDTO contr = contributoSRV.getContent(utente, contributo.getFlagEsterno(), contributo.getIdContributo());
				content = contr.getContent();
			}
			if (content != null) {
				stream = new ByteArrayInputStream(content);
				file = new DefaultStreamedContent(stream, contributo.getMimeType(), contributo.getFilename());
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_MSG + e.getMessage());
		}
		return file;
	}

	/**
	 * Restituisce il chiamante.
	 * 
	 * @return chiamante
	 */
	public String getChiamante() {
		return chiamante;
	}

	/**
	 * Imposta il chiamante.
	 * 
	 * @param chiamante
	 */
	public void setChiamante(final String chiamante) {
		this.chiamante = chiamante;
	}

	/**
	 * Imposta il Master.
	 * 
	 * @see MasterDocumentRedDTO
	 * @param inMaster
	 */
	public void setMaster(final MasterDocumentRedDTO inMaster) {
		master = inMaster;
	}

	/**
	 * Restitusice il Master.
	 * 
	 * @return
	 */
	public MasterDocumentRedDTO getMaster() {
		return master;
	}

	/**
	 * Effettua il download delle etichette a partire dal wob number.
	 * 
	 * @param wobNumber
	 */
	public void getDownloadEtichette(final String wobNumber) {
		String nomeFileEtichetta = "";
		try {
			final byte[] content = stampaEtichetteResponse(wobNumber);
			if (content != null && content.length > 0) {
				final String giornoMeseAnnoOraminutisecondi = DateUtils.dateToString(Calendar.getInstance().getTime(), "ddMMyyyy_HHmmss");
				nomeFileEtichetta = org.apache.commons.lang3.StringUtils.join("StampaEtichette-", "_", giornoMeseAnnoOraminutisecondi, ".pdf");
			} else {
				LOGGER.error(ERROR_DOWNLOAD_ETICHETTE_MSG);
				FacesHelper.showMessage(null, ERROR_DOWNLOAD_ETICHETTE_MSG, MessageSeverityEnum.ERROR);
				return;
			}
			final InputStream io = new ByteArrayInputStream(content);
			sessionBean.setScStampaEtichette(new DefaultStreamedContent(io, "image/png", nomeFileEtichetta));
			FacesHelper.executeJS("stampaEtichetta();");
		} catch (final Exception e) {
			LOGGER.error(ERROR_DOWNLOAD_ETICHETTE_MSG, e);
			FacesHelper.showMessage(null, ERROR_DOWNLOAD_ETICHETTE_MSG, MessageSeverityEnum.ERROR);
		}
	}

	private byte[] stampaEtichetteResponse(final String documentTitle) {
		byte[] content = new byte[0];
		
		try {
			if (!StringUtils.isNullOrEmpty(documentTitle)) {
				content = npsSRV.stampaEtichette(documentTitle, utente);
			} else if (!StringUtils.isNullOrEmpty(detail.getDocumentTitle())) {
				content = npsSRV.stampaEtichette(detail.getDocumentTitle(), utente);
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la stampa etichette", e);
			showError("Si è verificato un errore durante la stampa etichette");
		}
		return content;

	}

	/**
	 * Restituisce true se Integrazione Dati è visibile, false altrimenti.
	 * 
	 * @return isIntegrazioneDatiVisible
	 */
	public boolean isIntegrazioneDatiVisible() {
		return isIntegrazioneDatiVisible;
	}

	/**
	 * Imposta il flag: isIntegrazioneDatiVisibile.
	 * 
	 * @param isIntegrazioneDatiVisible
	 */
	public void setIntegrazioneDatiVisible(final boolean isIntegrazioneDatiVisible) {
		this.isIntegrazioneDatiVisible = isIntegrazioneDatiVisible;
	}

}
