package it.ibm.red.web.enums;

import it.ibm.red.web.constants.ConstantsWeb;

/**
 * The Enum HomepageEnum.
 *
 * @author CPIERASC
 * 
 *         Tipologie di codifica.
 */
public enum WidgetEnum {

	/**
	 * Valore.
	 */
	CONTATORI("0", "Contatori", "/views/widgets/contatori.xhtml", 
			"Contatori", "Widget dei contatori", "contatoriMef-wgd", "fa-th", "47%", ConstantsWeb.MBean.CALENDARIO_BEAN),

	/**
	 * Valore.
	 */
	SOTTOSCRIZIONI("1", "Sottoscrizioni", "/views/widgets/sottoscrizioni.xhtml", "Sottoscrizioni", "Widget delle sottoscrizioni", "sottoscrizioniMef-wgd", "fa-eye", "47%",
			ConstantsWeb.MBean.SOTTOSCRIZIONI_WIDGET_BEAN),

	/**
	 * Valore.
	 */
	NEWS("2", "News", "/views/widgets/news.xhtml", "News", "Widget delle news", "newsMef-wgd", "fa-newspaper-o", "47%", ConstantsWeb.MBean.NEWS_WIDGET_BEAN),

	/**
	 * Valore.
	 */
	CALENDARIO("3", WidgetEnum.CALENDARIO_LABEL, "/views/widgets/calendarioWidget.xhtml", WidgetEnum.CALENDARIO_LABEL, WidgetEnum.CALENDARIO_LABEL, "calendarioMef-wgd", "fa-calendar", "47%",
			ConstantsWeb.MBean.CALENDARIO_BEAN),

	/**
	 * Valore.
	 */
	SPEDIZIONI("4", WidgetEnum.SPEDIZIONI_LABEL, "/views/widgets/spedizioniWidget.xhtml", WidgetEnum.SPEDIZIONI_LABEL, WidgetEnum.SPEDIZIONI_LABEL, "spedizioniMef-wgd", "fa-send", "47%",
			ConstantsWeb.MBean.SPEDIZIONI_WIDGET_BEAN),

	/**
	 * Valore.
	 */
	INAPPROVAZIONEPERSTRUTTURA("5", "In approvazione per struttura", "/views/widgets/inApprovazionePerStruttura.xhtml", "Approvazione per struttura",
			"Widget in approvazione per struttura", "inApprovazioneMef-wgd", "fa-pie-chart", "47%", ConstantsWeb.MBean.INAPPROVAZIONEPERSTRUTTURA_WIDGET_BEAN),

	/**
	 * Valore.
	 */
	INAPPROVAZIONEPERDIRIGENTE("6", "In approvazione", "/views/widgets/inApprovazionePerDirigente.xhtml", 
			"Approvazione per dirigente", "Widget in approvazione per dirigente",
			"inApprovazioneMef-wgd", "fa-pie-chart", "47%", ConstantsWeb.MBean.INAPPROVAZIONEPERDIRIGENTE_WIDGET_BEAN),

	/**
	 * Valore.
	 */
	RUBRICA("7", WidgetEnum.RUBRICA_LABEL, "/views/widgets/rubricaWidget.xhtml", WidgetEnum.RUBRICA_LABEL, WidgetEnum.RUBRICA_LABEL, "rubricaMef-wgd", "fa-book", "47%", ConstantsWeb.MBean.WDG_RUBRICA_BEAN),

	/**
	 * Valore.
	 */
	ADMIN_TOOL("8", WidgetEnum.ADMIN_TOOL_LABEL, "/views/widgets/adminToolWidget.xhtml", 
			WidgetEnum.ADMIN_TOOL_LABEL, WidgetEnum.ADMIN_TOOL_LABEL, "rubricaMef-wgd", "fa-book", "47%", ConstantsWeb.MBean.ADMIN_TOOL_BEAN);
	
	/**
	 * Primary key.
	 */
	private String pk;

	/**
	 * Header.
	 */
	private String header;

	/**
	 * URL.
	 */
	private String url;

	/**
	 * Tooltip.
	 */
	private String tooltip;

	/**
	 * Display name.
	 */
	private String displayName;

	/**
	 * Classe CSS.
	 */
	private String cssClass;

	/**
	 * Icona.
	 */
	private String icona;

	/**
	 * Flag eliminabile.
	 */
	private boolean isEliminabile;

	/**
	 * Larghezza.
	 */
	private String width;

	/**
	 * Nome del bean.
	 */
	private String beanName;
	
	private static final String ADMIN_TOOL_LABEL = "Admin Tool";
	private static final String RUBRICA_LABEL = "Rubrica";
	private static final String CALENDARIO_LABEL = "Calendario";
	private static final String SPEDIZIONI_LABEL = "Spedizioni";
	
	/**
	 * Costruttore.
	 * 
	 * @param inPk
	 * @param inHeader
	 * @param inUrl
	 * @param inDisplayName
	 * @param inTooltip
	 * @param inCssClass
	 * @param inIcona
	 * @param inWidth
	 * @param inBeanName
	 */
	WidgetEnum(final String inPk, final String inHeader, final String inUrl, final String inDisplayName, 
			final String inTooltip, final String inCssClass, final String inIcona,
			final String inWidth, final String inBeanName) {
		pk = inPk;
		url = inUrl;
		header = inHeader;
		displayName = inDisplayName;
		tooltip = inTooltip;
		cssClass = inCssClass;
		icona = inIcona;
		width = inWidth;
		beanName = inBeanName;
	}

	/**
	 * Restituisce PK.
	 * 
	 * @return pk
	 */
	public String getPk() {
		return pk;
	}

	/**
	 * Restituisce il tooltip.
	 * 
	 * @return tooltip
	 */
	public String getTooltip() {
		return tooltip;
	}

	/**
	 * Restituisce il display name.
	 * 
	 * @return display name
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Restituisce l'Url.
	 * 
	 * @return url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Restituisce l'header.
	 * 
	 * @return header
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * Restituisce la classe CSS.
	 * 
	 * @return classe CSS
	 */
	public String getCssClass() {
		return cssClass;
	}

	/**
	 * Restituisce l'icona.
	 * 
	 * @return icona
	 */
	public String getIcona() {
		return icona;
	}

	/**
	 * Restituisce l'enum associata alla key {@link #pk}.
	 * 
	 * @param pk
	 * @return enum associata a pk
	 */
	public static WidgetEnum get(final String pk) {
		WidgetEnum output = null;
		for (final WidgetEnum t : WidgetEnum.values()) {
			if (pk.equals(t.getPk())) {
				output = t;
				break;
			}
		}
		return output;
	}

	/**
	 * Restituisce il flag associato all'eliminabilita.
	 * 
	 * @return flag associato all'eliminabilita
	 */
	public boolean getIsEliminabile() {
		return isEliminabile;
	}

	/**
	 * Restituisce la width.
	 * 
	 * @return width
	 */
	public String getWidth() {
		return width;
	}

	/**
	 * Restituisce il bean name.
	 * 
	 * @return bean name
	 */
	public String getBeanName() {
		return beanName;
	}

}