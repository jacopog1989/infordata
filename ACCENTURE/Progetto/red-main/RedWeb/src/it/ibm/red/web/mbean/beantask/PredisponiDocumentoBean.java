package it.ibm.red.web.mbean.beantask;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections.CollectionUtils;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.helper.PredisponiDocumentoHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;


/**
 * Class PredisponiDocumentoBean.
 *
 * @author SLac
 */
@Named(MBean.PREDISPONI_DOCUMENTO_BEAN)
@ViewScoped
public class PredisponiDocumentoBean extends AbstractBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -1210242037239783482L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PredisponiDocumentoBean.class);
	
	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Lista allacci.
	 */
	private List<RispostaAllaccioDTO> allacci;
	
	/**
	 * Documenti selezionati.
	 */
	private RispostaAllaccioDTO docSelected;
	
	/**
	 * Predisponi documento helper.
	 */
	private PredisponiDocumentoHelper pdh;
	
	/**
	 * Flag chiusura obbligatoria.
	 * Flag che indica se il check "Chiudere" della maschera deve essere NON deselezionabile
	 * per il documento attualmente selezionato (docSelected)
	 */
	private boolean chiusuraObbligatoriaDocSelected;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		utente = ((SessionBean) FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN)).getUtente();
	}
	

	/** 
	 * Setta gli allacci dai master selezionati.
	 * 
	 * @param documenti the documenti
	 * @param response the response
	 */
	public void setAllaciFromMasters(final List<MasterDocumentRedDTO> documenti, final ResponsesRedEnum response) {
		pdh = new PredisponiDocumentoHelper(documenti.get(0), documenti, response, utente, true);
		chiusuraObbligatoriaDocSelected = ResponsesRedEnum.RISPONDI_INGRESSO.equals(response);
		docSelected = pdh.getDocDaRibaltareAllaccio();
		allacci = pdh.getDocsDaAllacciareAllaccioList();
		onRowSelectRadio(); // Si triggera la selezione
	}
	
	
	/**
	 * Seleziona il checkbox di chiusura per tutti gli allacci.
	 */
	public void chiudiTutti() {
		if (!CollectionUtils.isEmpty(allacci)) {
			for (final RispostaAllaccioDTO allaccio : allacci) {
				allaccio.setDocumentoDaChiudere(true);
			}
		}
	}
	
	
	/**
	 * Metodo che viene chiamato lato front end per effettuare l'operazione di Predisponi.
	 */
	public void predisponi() {
		if (pdh != null && docSelected != null) {
			// Se si è passati dalla maschera, il documento selezionato e gli allacci potrebbero essere cambiati (check GUI "chiudi tutti")
			pdh.setDocDaRibaltareAllaccio(docSelected);
			pdh.setDocsDaAllacciareAllaccioList(allacci);
			
			// ### Si predispone il documento in uscita per la risposta
			pdh.predisponiUscita();
			
			if (pdh.getEsitoPredisposizione().isEsito()) {
				// Si procede all'apertura della dialog di creazione del documento predisposto
				pdh.apriDialogCreazioneDocPredisposto();
			} else {
				showError(pdh.getEsitoPredisposizione().getNote());
			}
		}
	}
	
	
	/**
	 * Gestisce la selezione del documento flaggando come "da chiudere" il documento selezionato.
	 */
	public void onRowSelectRadio() {
		try {
			for (final RispostaAllaccioDTO ra : allacci) {
				if (!ra.getIdDocumentoAllacciato().equals(docSelected.getIdDocumentoAllacciato())) {
					ra.setDocumentoDaChiudere(false);
					ra.setDisabled(false);
				} else {
					ra.setDocumentoDaChiudere(true);
					if (chiusuraObbligatoriaDocSelected) {
						ra.setDisabled(true);
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	
	/**
	 *  SET & GET *******************************************************************************************.
	 *
	 * @return the allacci
	 */
	public List<RispostaAllaccioDTO> getAllacci() {
		return allacci;
	}

	/** 
	 * @return the doc selected
	 */
	public RispostaAllaccioDTO getDocSelected() {
		return docSelected;
	}

	/** 
	 * @param docSelected the new doc selected
	 */
	public void setDocSelected(final RispostaAllaccioDTO docSelected) {
		this.docSelected = docSelected;
	}
}