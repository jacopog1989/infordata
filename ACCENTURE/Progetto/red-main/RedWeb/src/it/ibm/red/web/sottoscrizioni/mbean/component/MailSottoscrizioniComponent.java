package it.ibm.red.web.sottoscrizioni.mbean.component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.event.ActionEvent;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.dto.MailAddressDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ISottoscrizioniFacadeSRV;
import it.ibm.red.web.dto.ListElementDTO;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.utils.ValidatorUtils;

/**
 * Seleziona e visualizza una lista di mail 
 *
 */
public class MailSottoscrizioniComponent extends AbstractBean {	
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -5758513048008522711L;


	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(MailSottoscrizioniComponent.class.getName());
	
	/**
	 * Servizio.
	 */
	private final ISottoscrizioniFacadeSRV sottoscrizioniSRV;
	
	/**
	 * Utente.
	 */
	private final UtenteDTO utente;
	
	/**
	 * Lista elmenti.
	 */
	private List<ListElementDTO<String>> elementList;
	
	/**
	 * Lista elementi selezionati.
	 */
	private ListElementDTO<String> selectedListElement;
	
	/**
	 * Costruttore del component.
	 * @param utente
	 */
	public MailSottoscrizioniComponent(final UtenteDTO utente) {
		this.utente = utente;
		this.sottoscrizioniSRV = ApplicationContextProvider.getApplicationContext().getBean(ISottoscrizioniFacadeSRV.class);
		
		final Collection<MailAddressDTO> mailAddressList = sottoscrizioniSRV.loadMailAddress(utente);
		final boolean isEmpty = CollectionUtils.isEmpty(mailAddressList);
		
		final int size = isEmpty ? 0 : mailAddressList.size();
		elementList = new ArrayList<>(size);
		if (!isEmpty) {
			int i = 0;
			for (final MailAddressDTO mail : mailAddressList) {
				final ListElementDTO<String> elem = new ListElementDTO<>(i, mail.getAddress());
				this.elementList.add(elem);
				i++;
			}
		}
	}
	
	/**
	 * Crea un nuovo ListElement e lo aggiunge alla lista.
	 */
	public void addRow() {
		new ListElementDTO<String>("", elementList);
	}
	
	/**
	 * Reinizializza l'elemento contenuto nella riga selezionta.
	 * @param event
	 */
	public void clearRow(final ActionEvent event) {
		setCurrentListElement(event);
		this.selectedListElement.setPlaceHolder("");
	}
	
	/**
	 * Gestisce la rimozione dell'elemento identificato dalla riga selezionata.
	 * @param event
	 */
	public void removeRow(final ActionEvent event) {
		if (!elementList.isEmpty()) {
			final ListElementDTO<String> toRemove = getDataTableClickedRow(event);
			final int id = toRemove.getId();
			this.elementList.remove(id);
			
			//reimposto gli id degli elementi della lista
			for (final Iterator<ListElementDTO<String>> iter = this.elementList.listIterator(); iter.hasNext();) {
				final ListElementDTO<String> currentElement = iter.next();
				if (currentElement.getId() >= id) {
					currentElement.setId(currentElement.getId() - 1);
				}
			}
			
			this.selectedListElement = null;
		} else {
			LOGGER.error("Chiamata la funzione di rimozione quando la tabella non ha elementi");
			final RuntimeException exception = new UnsupportedOperationException("Attenzione tentativo di rimuovere il primo placeholder della lista assegnaizoni multiple");
			showError(exception);
			throw exception;
		}
	}
	
	/**
	 * Gestisce la sottoscrizione della mail.
	 */
	public void registra() {
		final boolean malformed =  this.elementList.stream()
				.anyMatch(elem -> !elem.isPlaceHolder() && StringUtils.isNotBlank(elem.getData()) &&  !ValidatorUtils.mailAddress(elem.getData()));
		if (malformed) {
			throw new RedException("Alcune mail non sono compilate correttamente");
		}
		
		final List<String> addressList =  this.elementList.stream()
			.filter(elem -> !elem.isPlaceHolder() && StringUtils.isNotBlank(elem.getData()) && ValidatorUtils.mailAddress(elem.getData()))
			.map(elem -> elem.getData())
			.collect(Collectors.toList());
		 if (!sottoscrizioniSRV.registraMail(utente, addressList)) {
			throw new RedException("Errore durante la sottoscrizione della mail");
		}
	}
	
	/**
	 * Imposta {@link #selectedListElement} con l'elemento identificato dalla riga selezionata sul datatable.
	 * @param event
	 */
	void setCurrentListElement(final ActionEvent  event) {
		this.selectedListElement = getDataTableClickedRow(event);
	}
	
	/**
	 * @return elementList
	 */
	public List<ListElementDTO<String>> getElementList() {
		return elementList;
	}

	/**
	 * Imposta la lista delle listElement.
	 * @param elementList
	 */
	public void setElementList(final List<ListElementDTO<String>> elementList) {
		this.elementList = elementList;
	}

	/**
	 * Post construct del component.
	 */
	@Override
	protected void postConstruct() {
		final RuntimeException e = new UnsupportedOperationException("Il postconstruct di questo metodo non deve essere invocato");
		LOGGER.error(e);
		throw e;
	}
}
