package it.ibm.red.web.mbean.interfaces;

import java.io.Serializable;

/**
 * Interfaccia per i chiamanti della dialog di inserimento/modifica di un documento di Contributo Esterno.
 * 
 * @author m.crescentini
 *
 */
public interface IGestoreAggiungiModificaContributoEst extends Serializable {
	

	/**
	 * Operazioni da eseguire a seguito dell'avvenuto inserimento/modifica di un documento di Contributo Esterno.
	 */
	void aggiornaPostInserimentoModifica();
}
