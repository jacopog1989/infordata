package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.poi.ss.formula.eval.NotImplementedException;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IDetailFascicoloSRV;
import it.ibm.red.business.service.facade.IFaldoneFacadeSRV;
import it.ibm.red.business.service.facade.IFascicoloFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.mbean.component.DialogFaldonaComponent;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractTreeBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * Bean che gestisce la faldonatura.
 */
@Named(ConstantsWeb.MBean.FALDONA_BEAN)
@ViewScoped
public class FaldonaBean extends AbstractTreeBean implements InitHumanTaskInterface {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 3155045217068603206L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FaldonaBean.class.getName());

	/**
	 * Component faldonatura.
	 */
	private DialogFaldonaComponent faldonaCmp;
	
	/**
	 * Service faldonatura.
	 */
	private IFaldoneFacadeSRV faldoneSRV;
	
	/**
	 * Service fascicoli.
	 */
	private IFascicoloFacadeSRV fascicoloSRV;
	
	/**
	 * Utente.
	 */
	private UtenteDTO u;
	
	/**
	 * Lista documenti selezionati.
	 */
	private List<MasterDocumentRedDTO> documentiSelezionati;
	
	/**
	 * Fascicolo procedimentale.
	 */
	private FascicoloDTO fascicoloProcedimentale;

	/**
	 * Servizio.
	 */
	private IDetailFascicoloSRV detailFascicoloSRV;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		faldoneSRV = ApplicationContextProvider.getApplicationContext().getBean(IFaldoneFacadeSRV.class);
		fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloFacadeSRV.class);
		detailFascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IDetailFascicoloSRV.class);

		final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		u = sb.getUtente();
		faldonaCmp = new DialogFaldonaComponent(u);
	}

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		documentiSelezionati = inDocsSelezionati;

		final Collection<FaldoneDTO> faldoni = new ArrayList<>();
		for (final MasterDocumentRedDTO documentoSelezionato : documentiSelezionati) {
			final FascicoloDTO fascicoloProc = fascicoloSRV.getFascicoloProcedimentale(documentoSelezionato.getDocumentTitle(), u.getIdAoo().intValue(), u);
			final List<FaldoneDTO> f = detailFascicoloSRV.getFaldoniByIdFascicolo(fascicoloProc.getIdFascicolo(), u.getIdAoo(), u);
			if (f != null && !f.isEmpty()) {
				for (final FaldoneDTO faldone : f) {
					faldone.setFascicoloSelezionato(fascicoloProc);
				}
				faldoni.addAll(f);
			}
		}
		faldonaCmp.setFaldoni(faldoni);
	}

	/**
	 * Gestisce la funzionalita di faldonatura dalla response.
	 */
	public void faldonaDaResponse() {
		try {
			if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(faldonaCmp.getOggettoNuovoFaldone()) && faldonaCmp.getFaldoniScelti().isEmpty()) {
				showWarnMessage("Nessun faldone è stato selezionato o creato.");
				return;
			}

			final ListaDocumentiBean ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);

			final Collection<EsitoOperazioneDTO> esitiOperazioni = new ArrayList<>();
			String wobNumber = null;

			// Si puliscono gli esiti
			ldb.cleanEsiti();

			for (final MasterDocumentRedDTO documentoSelezionato : documentiSelezionati) {
				wobNumber = documentoSelezionato.getWobNumber();

				final FascicoloDTO fascicoloProc = fascicoloSRV.getFascicoloProcedimentale(documentoSelezionato.getDocumentTitle(), u.getIdAoo().intValue(), u);

				// Si invoca il servizio per la faldonatura
				if (fascicoloProc != null) {
					for (final FaldoneDTO fald : faldonaCmp.getFaldoniScelti()) {
						final String nomeFaldone = fald.getNomeFaldone();
						esitiOperazioni.add(faldoneSRV.faldona(u, nomeFaldone, wobNumber, fascicoloProc.getIdFascicolo()));
					}
				}
			}

			ldb.getEsitiOperazione().addAll(esitiOperazioni);
			FacesHelper.update("centralSectionForm:idDlgShowResult");
			FacesHelper.executeJS("PF('dlgGeneric').hide();PF('dlgShowResult').show();");

			ldb.destroyBeanViewScope(MBean.FALDONA_BEAN);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Si è verificato un errore durante l'operazione di faldonatura.");
		}
	}

	/**
	 * SET & GET.
	 *******************************************************************************************************************/
	/**
	 * Restituisce il fascicolo procedimentale.
	 * 
	 * @return fascicolo procedimentale
	 */
	public FascicoloDTO getFascicoloProcedimentale() {
		return fascicoloProcedimentale;
	}

	/**
	 * Imposta il fascicolo procedimentale.
	 * 
	 * @param fascicoloProcedimentale
	 */
	public void setFascicoloProcedimentale(final FascicoloDTO fascicoloProcedimentale) {
		this.fascicoloProcedimentale = fascicoloProcedimentale;
	}

	/**
	 * Restituisce il component per la gestione dei faldoni.
	 * 
	 * @return component faldoni
	 */
	public DialogFaldonaComponent getFaldonaCmp() {
		return faldonaCmp;
	}

	@Override
	public void onOpenTree(TreeNode nodo) {
		throw new NotImplementedException("Metodo non implementato");
	}
}