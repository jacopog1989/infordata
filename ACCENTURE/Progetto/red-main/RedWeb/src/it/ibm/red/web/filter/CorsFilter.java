package it.ibm.red.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * Filtro CORS.
 */
public class CorsFilter implements Filter {
	
	/**
	 * Livello sicurezza alto.
	 */
	private static final String HIGH = "HIGH";

	/**
	 * Destroy.
	 *
	 * @see Filter#destroy()
	 */
	@Override
	public void destroy() {
		/**
		 * Questo metodo è lasciato intenzionalmente vuoto.
		 */
	}

	/**
	 * doFilter method.
	 * @param request
	 * @param response
	 * @param chain
	 * @throws IOException, ServletException
	 */
	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {

		final String secLev = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ACCESS_SECURITY_LEVEL);
		if (!HIGH.equalsIgnoreCase(secLev)) {
			// Authorize (allow) all domains to consume the content
	        ((HttpServletResponse) response).addHeader("Access-Control-Allow-Origin", "*");
	        ((HttpServletResponse) response).addHeader("Access-Control-Allow-Methods", "GET, OPTIONS, HEAD, PUT, POST");
		}

        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse res = (HttpServletResponse) response;

        
        // For HTTP OPTIONS verb/method reply with ACCEPTED status code -- per CORS handshake
        if ("OPTIONS".equals(req.getMethod())) {
        	res.setStatus(HttpServletResponse.SC_ACCEPTED);
            return;
        }

        // pass the request along the filter chain
        chain.doFilter(request, response);
	}
	
	/**
	 * Init method.
	 * @param fConfig - fConfig
	 * @exception ServletException - ServletException
	 */
	@Override
	public void init(final FilterConfig fConfig) throws ServletException {
		/**
		 * Questo metodo è lasciato intenzionalmente vuoto.
		 */
	}

}
