package it.ibm.red.web.dto;

import java.io.Serializable;
import java.lang.reflect.Method;

/**
 * DTO che definisce uno bean task.
 */
public class BeanTaskDTO implements Serializable {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 5767232089767658622L;
	
	/**
	 * Nome bean.
	 */
	private final String beanName;

	/**
	 * Nome metodo.
	 */
	private transient Method beanMethod;

	/**
	 * Costruttore del DTO.
	 * @param beanName
	 * @param beanMethod
	 */
	public BeanTaskDTO(final String beanName, final Method beanMethod) {
		this.beanName = beanName;
		this.beanMethod = beanMethod;
	}
	
	/**
	 * Restituisce il nome del bean.
	 * @return beanName
	 */
	public String getBeanName() {
		return beanName;
	}

	/**
	 * Restituisce il Method del bean.
	 * @return beanMethod come Method
	 */
	public Method getBeanMethod() {
		return beanMethod;
	}
}
