package it.ibm.red.web.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class InvalidaSessioneServlet
 */
public class InvalidaSessioneServlet extends HttpServlet {
	
	private static final long serialVersionUID = 3748339388179702834L;
	
	
	/**
     * @see HttpServlet#HttpServlet()
     */
    public InvalidaSessioneServlet() {
        super();
    }
    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) 
			throws ServletException {
		HttpSession sessione = request.getSession(false);
		if (sessione != null) {
			sessione.invalidate();
		}
	}
}