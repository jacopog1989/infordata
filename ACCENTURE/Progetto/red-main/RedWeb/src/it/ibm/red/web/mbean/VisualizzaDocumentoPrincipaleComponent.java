package it.ibm.red.web.mbean;

import java.util.ArrayList;

import org.primefaces.event.SelectEvent;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.web.servlets.DownloadContentServlet.DocumentTypeEnum;

/**
 * Component visualizzazione documento principale.
 */
public class VisualizzaDocumentoPrincipaleComponent extends VisualizzaDocumentoComponentBase {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -1318542703517450989L;

    /**
     * Tipo Documento.
     */
	private final DocumentTypeEnum documentTypePrincipale;

	/**
	 * Costruttore di default.
	 * @param inUtente
	 * @param inDocumentTypePrincipale
	 */
	public VisualizzaDocumentoPrincipaleComponent(final UtenteDTO inUtente, final DocumentTypeEnum inDocumentTypePrincipale) {
		super(inUtente);
		this.documentTypePrincipale = inDocumentTypePrincipale;
	}

	/**
	 * Imposta il dettaglio.
	 * 
	 * @param dettaglio
	 *            dettaglio da impostare
	 */
	@Override
	public void setDetail(final DetailDocumentRedDTO dettaglio) {
		docpDTH.setMasters(new ArrayList<>());
		docpDTH.getMasters().add(dettaglio);
		previewComponent.setDetailPreview(dettaglio.getDocumentTitle(), documentTypePrincipale);

		docpDTH.setCurrentMaster(dettaglio);
	}

	/**
	 * Gestisce l'evento di selezione delle righe del datatable del documento principale.
	 */
	@Override
	public void rowSelectorDocPrincipale(final SelectEvent se) {
		handleEmptyRow();

		docpDTH.rowSelector(se);
		final DetailDocumentRedDTO dettaglio = docpDTH.getCurrentMaster();

		previewComponent.setDetailPreview(dettaglio.getDocumentTitle(), documentTypePrincipale);
	}
	
}
