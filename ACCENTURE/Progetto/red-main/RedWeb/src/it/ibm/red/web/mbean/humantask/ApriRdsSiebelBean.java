package it.ibm.red.web.mbean.humantask;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.EsitoCreazioneRdsDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.RdsSiebelGruppoDTO;
import it.ibm.red.business.dto.RdsSiebelTipologiaDTO;
import it.ibm.red.business.dto.TestoPredefinitoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IFascicoloFacadeSRV;
import it.ibm.red.business.service.facade.ISiebelFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.component.DocumentiMailAllegabiliComponent;
import it.ibm.red.web.mbean.component.OggettoTestoMailPrefissoSuffissoComponent;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;
import it.ibm.red.web.utils.ValidatorUtils;

/**
 * Bean che gestisce l'apertura dei Siebel RDS.
 */
@Named(MBean.APRI_RDS_SIEBEL_BEAN)
@ViewScoped
public class ApriRdsSiebelBean extends AbstractBean implements InitHumanTaskInterface {
	
	/**
	 * Auto generated serial.
	 */
	private static final long serialVersionUID = -2007779263411878115L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ApriRdsSiebelBean.class);
	
	/**
	 * Servizio siebel.
	 */
	private ISiebelFacadeSRV siebelSRV;
	
	/**
	 * Fascicolo srv.
	 */
	private IFascicoloFacadeSRV fascicoloSRV;
	
	/**
	 * Info utente.
	 */
	private UtenteDTO utente;
	
	/**
	 * Documento su cui è stata eseguita la response.
	 */
	private MasterDocumentRedDTO master;
	
	/**
	 * Gestisce il testo mail e l'oggetto insieme alle varie interazioni.
	 */
	private OggettoTestoMailPrefissoSuffissoComponent mailComponent;
	
	/**
	 * Componenti documenti mail allegabili.
	 */
	private DocumentiMailAllegabiliComponent allegabiliComponent;
	
	/**
	 * Mail alternativa a cui se valorizzata deve essere inviata l'email di operazione riuscita.
	 */
	private String mailAlternativa;
	
	/**
	 * Qualora si stia tentando di aprire un rds da un documento che ha già avuto un rds,
	 * questa stringa descrive l'rds precedentemente ritornato. 
	 */
	private String recapRdsPrecedente;
	
	/**
	 * Mostra un messaggio per ricordare il protocollo originale del file. Se il file non è stato protocollato non viene stampato.
	 */
	private String protocolloOriginale;

	/**
	 * Testi predefiniti disponibili per le siebel.
	 */
	private List<TestoPredefinitoDTO> testiPredefinitiList;

	/**
	 * Dimensione totale per siebel.
	 */
	private BigDecimal dimensioneTotale;
	
	/**
	 * Tipologie e gruppi per siebel.
	 */
	private Map<RdsSiebelTipologiaDTO, List<RdsSiebelGruppoDTO>> mappaTipiGruppiSiebel;
	
	/**
	 * Lista tipi siebel.
	 */
	private List<RdsSiebelTipologiaDTO> tipiSiebelList;
	
	/**
	 * Lista gruppi siebel.
	 */
	private List<RdsSiebelGruppoDTO> gruppiSiebelList;
	
	/**
	 * Identificativo tipologia.
	 */
	private Integer idTipologia;
	
	/**
	 * Codice gruppo.
	 */
	private String codiceGruppo;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		try {
			fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloFacadeSRV.class);
			siebelSRV = ApplicationContextProvider.getApplicationContext().getBean(ISiebelFacadeSRV.class);
			
			final SessionBean session = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
			utente = session.getUtente();
			
			testiPredefinitiList = siebelSRV.getTestiPredefiniti(utente);
			dimensioneTotale = siebelSRV.getDimensioneMassimaTotaleAllegatiMailBigDecimalKB();
		} catch (final Exception e) {
			LOGGER.error("errore durante la costruzione del bean: " + MBean.APRI_RDS_SIEBEL_BEAN, e);
			showError("Impossibile inizializzare la response.");
		}
	}
	
	/**
	 * Inizializza il bean.
	 * @param inDocsSelezionati documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		try {
			reset();
			
			master = inDocsSelezionati.get(0);
			final FascicoloDTO fascicolo = fascicoloSRV.getFascicoloProcedimentale(master.getDocumentTitle(), utente.getIdAoo().intValue(), utente);
			
			final String defaultOggetto = siebelSRV.createDefaultOggetto(master, fascicolo, utente);
			protocolloOriginale = siebelSRV.getProtocolloOriginale(master);
			mailComponent = new OggettoTestoMailPrefissoSuffissoComponent(testiPredefinitiList, defaultOggetto);
			
			final DocumentoAllegabileDTO mail = getMailByWob();
			final List<DocumentoAllegabileDTO> documentiAllegabili = fascicoloSRV.getDocumentiAllegabili(master.getWobNumber(), utente, fascicolo, false);
			allegabiliComponent = new DocumentiMailAllegabiliComponent(fascicolo, mail, documentiAllegabili, dimensioneTotale);
			
			recapRdsPrecedente = siebelSRV.getPrefissoTesto(master, utente.getIdAoo());
			
			mappaTipiGruppiSiebel = siebelSRV.getMappaTipiGruppiSiebel();
			if (mappaTipiGruppiSiebel != null && !mappaTipiGruppiSiebel.isEmpty()) {
				tipiSiebelList = new ArrayList<>(mappaTipiGruppiSiebel.keySet());
				if (!tipiSiebelList.isEmpty()) {
					gruppiSiebelList = mappaTipiGruppiSiebel.get(tipiSiebelList.get(0));
				}
			}
			
		} catch (final Exception e) {
			LOGGER.error("Errore imprevisto durante la creazione del bean", e);
			throw e;
		}
	}

	/**
	 * Esegue un tentativo di recupero mail, se non va a buon fine la logica non deve essere bloccata.
	 * @return mail del documento identificata dal wobnumber del {@link #master}
	 */
	private DocumentoAllegabileDTO getMailByWob() {
		
		DocumentoAllegabileDTO mail = null;
		try {
			mail = siebelSRV.getMailDocumentoByWobNumber(utente, master.getWobNumber());
		} catch (final Exception e) {
			//l'eccezione non deve bloccare la funzionalità che può proseguire anche senza mail originale
			LOGGER.error("Errore durante il recupero dell'email originale", e);
		}
		return mail;
	}

	/**
	 * Esegue le azioni relative al cambio lista Tipi Siebel.
	 */
	public void onChangeTipiSiebelList() {
		if (mappaTipiGruppiSiebel != null && !mappaTipiGruppiSiebel.isEmpty()) {
			if (idTipologia != null) {
				for (final RdsSiebelTipologiaDTO tipo : tipiSiebelList) {
					if (tipo.getIdTipologia().equals(idTipologia)) {
						gruppiSiebelList = mappaTipiGruppiSiebel.get(tipo);
					}
				}
			} else if (tipiSiebelList != null && !tipiSiebelList.isEmpty()) {
				gruppiSiebelList = mappaTipiGruppiSiebel.get(tipiSiebelList.get(0));
			}
		}
	}

	/**
	 * Gestisce la funzionalità registra, imposta l'esito e mostra la dialog dei risultati per dare evidenza dell'esito.
	 * @see EsitoCreazioneRdsDTO
	 */
	public void registra() {
		try {
			final List<DocumentoAllegabileDTO> allegabili = allegabiliComponent.getSelectedAllegabili();
			final String oggetto = mailComponent.getCompleteOggetto();
			final String body = mailComponent.getCompleteBody();
			
			final boolean inputValido = validate();
			
			if (inputValido) {
				final ListaDocumentiBean ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
				
				ldb.cleanEsiti();
				
				final EsitoCreazioneRdsDTO esito = siebelSRV.apriRdsSiebel(utente, master.getWobNumber(), mailAlternativa, oggetto, body, allegabili, idTipologia,
						codiceGruppo);
				
				final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>(); 
				
				esiti.add(esito);
				
				ldb.setEsitiOperazione(esiti);
				
				ldb.destroyBeanViewScope(ConstantsWeb.MBean.APRI_RDS_SIEBEL_BEAN);
				
				FacesHelper.executeJS("PF('dlgGeneric').hide();PF('dlgShowResult').show();");
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Si è verificato un errore durante l'inoltro del documento.");
		}
	}

	/**
	 * Esegue il reset dei campi.
	 */
	private void reset() {
		mailAlternativa = null;
		protocolloOriginale = "";
		recapRdsPrecedente = "";
		
		if (mailComponent != null) {
			mailComponent.reset();
		}
	}

	/**
	 * Verifica che i campi immessi nel form rispettino i vincoli, comunicando l'errore in caso negativo.
	 * @return true se non ci sono errori su alcun campo, false altrimenti.
	 */
	private boolean validate() {
		boolean valid = true;
		
		if (StringUtils.isNotBlank(mailAlternativa) && !ValidatorUtils.mailAddress(mailAlternativa)) {
			valid = false;
			showWarnMessage("Attenzione , l'email alternativa deve essere un indirizzo di posta valido");
		}
		
		if (idTipologia == null || StringUtils.isBlank(codiceGruppo)) {
			valid = false;
			showWarnMessage("Attenzione, la tipologia e il gruppo sono obbligatori.");
		}
		
		valid = mailComponent.validate() && valid;
		valid = allegabiliComponent.validate() && valid;
		
		return valid;
	}

	/**
	 * Restituisce una mail alternativa.
	 * @return mailAlternativa
	 */
	public String getMailAlternativa() {
		return mailAlternativa;
	}

	/**
	 * Imposta la mail alternativa.
	 * @param mailAlternativa
	 */
	public void setMailAlternativa(final String mailAlternativa) {
		this.mailAlternativa = mailAlternativa;
	}

	/**
	 * Restituisce il component OggettoTestoMailPrefissoSuffissoComponent.
	 * @return mailComponent
	 */
	public OggettoTestoMailPrefissoSuffissoComponent getMailComponent() {
		return mailComponent;
	}

	/**
	 * Restituisce il component DocumentiMailAllegabiliComponent.
	 * @return allegabiliComponent
	 */
	public DocumentiMailAllegabiliComponent getAllegabiliComponent() {
		return allegabiliComponent;
	}

	/**
	 * Restituisce un test che funge da recap per l'RDS precedente.
	 * @return recapRdsPrecedente
	 */
	public String getRecapRdsPrecedente() {
		return recapRdsPrecedente;
	}

	/**
	 * Restituisce il protocollo originale di riferimento.
	 * @return protocolloOriginale
	 */
	public String getProtocolloOriginale() {
		return protocolloOriginale;
	}

	/**
	 * Restituisce una List di RdsSiebelTipologiaDTO.
	 * @return tipiSiebelList
	 */
	public List<RdsSiebelTipologiaDTO> getTipiSiebelList() {
		return tipiSiebelList;
	}

	/**
	 * Imposta la lista dei Tipi Siebel: RdsSiebelTipologiaDTO.
	 * @param tipiSiebelList
	 */
	public void setTipiSiebelList(final List<RdsSiebelTipologiaDTO> tipiSiebelList) {
		this.tipiSiebelList = tipiSiebelList;
	}

	/**
	 * Restituisce la List dei Gruppi Siebel: RdsSiebelGruppoDTO.
	 * @return gruppiSiebelList
	 */
	public List<RdsSiebelGruppoDTO> getGruppiSiebelList() {
		return gruppiSiebelList;
	}

	/**
	 * Imposta la List dei Gruppi Siebel: RdsSiebelGruppoDTO.
	 * @param gruppiSiebelList
	 */
	public void setGruppiSiebelList(final List<RdsSiebelGruppoDTO> gruppiSiebelList) {
		this.gruppiSiebelList = gruppiSiebelList;
	}

	/**
	 * Restituisce l'id della tipologia.
	 * @return idTipologia
	 */
	public Integer getIdTipologia() {
		return idTipologia;
	}

	/**
	 * Imposta l'id della tipologia.
	 * @param idTipologia
	 */
	public void setIdTipologia(final Integer idTipologia) {
		this.idTipologia = idTipologia;
	}

	/**
	 * Restituisce il codice gruppo.
	 * @return codiceGruppo
	 */
	public String getCodiceGruppo() {
		return codiceGruppo;
	}

	/**
	 * Imposta il codice gruppo.
	 * @param codiceGruppo
	 */
	public void setCodiceGruppo(final String codiceGruppo) {
		this.codiceGruppo = codiceGruppo;
	}
}
