/**
 * 
 */
package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.CoordinatoreUfficioDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.ResponsabileCopiaConformeDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.facade.IModificaProcedimentoFacadeSRV;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.service.facade.ITipoAssegnazioneFacadeSRV;
import it.ibm.red.business.service.facade.IiterApprovativoFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * @author APerquoti
 *
 */
@Named(ConstantsWeb.MBean.MODIFICA_ITER_BEAN)
@ViewScoped
public class ModificaIterBean extends AbstractBean implements InitHumanTaskInterface {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 6619911573545281010L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ModificaIterBean.class.getName());

	/**
	 * Servizio.
	 */ 	
	private IiterApprovativoFacadeSRV iterApprovativoSRV;

	/**
	 * Servizio.
	 */
	private ITipoAssegnazioneFacadeSRV tipoAssegnazioneSRV;

	/**
	 * Lista master.
	 */
	private List<MasterDocumentRedDTO> masters;

	/**
	 * Informazioni utente.
	 */
	private UtenteDTO utente;

	/**
	 * Servizio.
	 */
	private IOrganigrammaFacadeSRV orgSRV;

	/**
	 * Servizio gestione utenti.
	 */
	private IUtenteSRV utenteSRV;

	/**
	 * Descrizione assegnazione.
	 */
	private String descrizioneNewAssegnazione;

	/**
	 * Root.
	 */
	private DefaultTreeNode root;

	/**
	 * Nuova assegnazione.
	 */
	private NodoOrganigrammaDTO newAssegnazione;

	/**
	 * Lista iter.
	 */
	private Collection<IterApprovativoDTO> iterApprovativoList;

	/**
	 * Lista coordinatori.
	 */
	private Collection<CoordinatoreUfficioDTO> coordinatoriList;

	/**
	 * Lista assegnazioni.
	 */
	private List<TipoAssegnazioneEnum> tipiAssegnazioniList;

	/**
	 * Id iter approvativo selezionato.
	 */
	private Integer idIterApprovativoSelected;

	/**
	 * Iter approvativo selezionato.
	 */
	private IterApprovativoDTO iterApprovativoSelected;

	/**
	 * Id coordinatore selezionato.
	 */
	private Integer idCoordinatoreSelected;

	/**
	 * Tipo assegnazione selezionata.
	 */
	private TipoAssegnazioneEnum tipoAssegnazioneSelected;

	/**
	 * Flag copia conforme.
	 */
	private boolean showCopiaConforme = false;

	/**
	 * Flag visualizza organigramma.
	 */
	private boolean showOrganigrammaSelection = true;

	/**
	 * Lista resposabili copia conforme.
	 */
	private List<ResponsabileCopiaConformeDTO> comboResponsabileCopiaConforme;

	/**
	 * Responsabili copia conforme.
	 */
	private ResponsabileCopiaConformeDTO responsabileCopiaConformeObj;

	/**
	 * Flag iter manuale.
	 */
	private boolean flagIterManuale;

	/**
	 * Flag urgente.
	 */
	private boolean flagUrgente;

	/**
	 * Flag firma digitale RGS.
	 */
	private boolean flagFirmaDigitaleRGS;

	/**
	 * Flag abilita firma digitale RGS.
	 */
	private boolean enableFirmaDigitaleRGS;

	/**
	 * Struttura nodi.
	 */
	private List<Nodo> alberaturaNodi;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		utenteSRV = ApplicationContextProvider.getApplicationContext().getBean(IUtenteSRV.class);
		orgSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
		iterApprovativoSRV = ApplicationContextProvider.getApplicationContext().getBean(IiterApprovativoFacadeSRV.class);
		tipoAssegnazioneSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipoAssegnazioneFacadeSRV.class);
		final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sb.getUtente();

		// recupero coordinatori disponibili
		coordinatoriList = loadCoordinatori();

		// recupero dei tipi assegnazione disponibili
		tipiAssegnazioniList = loadTipiAssegnazione();
		
		comboResponsabileCopiaConforme = utenteSRV.getComboResponsabiliCopiaConforme(utente.getIdAoo());

		alberaturaNodi = orgSRV.getAlberaturaBottomUp(utente.getIdAoo(), utente.getIdUfficio());
	}

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		masters = inDocsSelezionati;
		// preparazione dati frontend
		// recupero iter Approvativi
		iterApprovativoList = loadIterApprovativi();
		for(MasterDocumentRedDTO master:masters) {
			if(master.getSottoCategoriaDocumentoUscita()!=null && master.getSottoCategoriaDocumentoUscita().isRegistrazioneAusiliaria() && iterApprovativoList!=null) {
				tipiAssegnazioniList.remove(TipoAssegnazioneEnum.FIRMA_MULTIPLA);
				break;
			}
		}

	}

	/**
	 * Effettua una validazione sull'assegnazione, se valida esegue la response {@link #stornaUtenteResponse()}.
	 */
	public void checkEResponse() {
		if ((descrizioneNewAssegnazione == null || descrizioneNewAssegnazione.isEmpty())  &&  (idIterApprovativoSelected ==null || idIterApprovativoSelected==0)) {
			showWarnMessage("Attenzione: il campo Assegnatario è obbligatorio");
		} else {
			modificaIterResponse();
		}
	}
	
	/**
	 * Esegue la logica che gestisce la funzionalita di modifica iter, viene
	 * eseguita da response.
	 */
	private void modificaIterResponse() {
		EsitoOperazioneDTO eOpe = null;
		final IModificaProcedimentoFacadeSRV modificaIterSRV = ApplicationContextProvider.getApplicationContext().getBean(IModificaProcedimentoFacadeSRV.class);

		try {
			final ListaDocumentiBean ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);

			// pulisco gli esiti
			ldb.cleanEsiti();

			// recupero solo il primo checkato per questa response viene gestita solo in
			// modo singolo
			final String wobNumber = masters.get(0).getWobNumber();

			if (!flagIterManuale) {

				eOpe = modificaIterSRV.gestioneCambioIterAutomatico(TipoAssegnazioneEnum.FIRMA.getId(), idIterApprovativoSelected, flagFirmaDigitaleRGS, flagUrgente,
						idCoordinatoreSelected, utente, wobNumber);

			} else {
				LOGGER.info("START MODIFICA ITER MANUALE BEAN");
				LOGGER.info("START MODIFICA ITER MANUALE BEAN Utente " + utente.getUsername());
				Long nuovoUtente = null;
				Long idNodo = null;
				if (showCopiaConforme) {
					nuovoUtente = responsabileCopiaConformeObj.getIdUtente();
//					//eseguo un controllo perche nel caso della selezione di un ufficio il sistema si aspetta un idUtente a '0'
					if (newAssegnazione.getIdUtente() == null) {
						nuovoUtente = 0L;
					}
					idNodo = responsabileCopiaConformeObj.getIdNodo();
				} else {
					nuovoUtente = newAssegnazione.getIdUtente();
//					//eseguo un controllo perche nel caso della selezione di un ufficio il sistema si aspetta un idUtente a '0'
					if (newAssegnazione.getIdUtente() == null) {
						nuovoUtente = 0L;
					}
					idNodo = newAssegnazione.getIdNodo();
				}

				eOpe = modificaIterSRV.gestioneCambioIterManuale(tipoAssegnazioneSelected, flagUrgente, utente, wobNumber, idNodo, nuovoUtente);
			}

			// setto gli esiti con i nuovi e eseguo un refresh
			ldb.getEsitiOperazione().add(eOpe);
			
			FacesHelper.executeJS("PF('dlgGeneric').hide()");
			FacesHelper.executeJS("PF('dlgShowResult').show()");
			FacesHelper.update("centralSectionForm:idDlgShowResult");
			
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.MODIFICA_ITER_BEAN);

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'esecuzione della response 'Modifica Iter'", e);
			showError("Si è verificato un errore durante la modifica dell'iter");
			LOGGER.info("END MODIFICA ITER MANUALE BEAN EXCEPTION");
		}

		LOGGER.info("END MODIFICA ITER MANUALE BEAN");
	}

	/**
	 * Gestisce l'evento di modifica dell'iter approvativo.
	 */
	public void onChangeIterApprovativo() {
		if (idIterApprovativoSelected.intValue() == IterApprovativoDTO.ITER_FIRMA_RAGIONIERE.intValue()) {
			enableFirmaDigitaleRGS = true;
		} else {
			enableFirmaDigitaleRGS = false;
		}

		for (final IterApprovativoDTO i : iterApprovativoList) {
			if (i.getIdIterApprovativo().intValue() == idIterApprovativoSelected.intValue()) {
				iterApprovativoSelected = i;
				break;
			}
		}
	}

	/**
	 * Restituisce l'iter approvativo selezionato.
	 * 
	 * @return iter approvativo selezionato
	 */
	public IterApprovativoDTO getIterApprovativoSelected() {
		return iterApprovativoSelected;
	}

	/**
	 * Effettua un reset dei parametri che gestiscono l'assegnazione.
	 */
	public void clearAssegnazione() {
		descrizioneNewAssegnazione = "";
	}

	/**
	 * Gestisce la modifica dell'assegnazione.
	 */
	public void changeAssegnazione() {
		clearAssegnazione();
		if (tipoAssegnazioneSelected.equals(TipoAssegnazioneEnum.COPIA_CONFORME)) {
			showCopiaConforme = true;
			showOrganigrammaSelection = false;
		} else {
			showCopiaConforme = false;
			showOrganigrammaSelection = true;
		}
	}

	/**
	 * 
	 * @return
	 */
	private Collection<CoordinatoreUfficioDTO> loadCoordinatori() {
		Collection<CoordinatoreUfficioDTO> output = new ArrayList<>();

		try {
			// recupero se esisitono dei cordinatori assegnati all'ufficio dell'utente
			output = utenteSRV.getCoordinatoriUfficio(utente.getIdUfficio().intValue());
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}

		return output;
	}

	/**
	 * 
	 * @return
	 */
	private Collection<IterApprovativoDTO> loadIterApprovativi() {
		Collection<IterApprovativoDTO> output = new ArrayList<>();

		try {
			// Recupero se degli iter approvativi disponibili di questo utente
			final String documentTitle = masters.get(0).getDocumentTitle();
			output = iterApprovativoSRV.getAllByDocument(documentTitle, utente);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}

		return output;
	}

	/**
	 * 
	 * @return
	 */
	private List<TipoAssegnazioneEnum> loadTipiAssegnazione() {
		List<TipoAssegnazioneEnum> output = new ArrayList<>();

		try {
			// recupero se dei tipi assegnazione disponibili per questo utente
			output = tipoAssegnazioneSRV.getAllByIdAooIterManuale(utente.getIdAoo().intValue());
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}

		return output;
	}

	/**
	 * Permette di costruire l'alberatura per la gestione del tree associato
	 * all'assegnazione iter manuale. Il comportamento dello stesso è differente in
	 * base al tipo assegnazione selezionato ed identificato da
	 * {@link #tipoAssegnazioneSelected}.
	 */
	public void loadTreeAssegnazioneIterManuale() {
		try {
			final List<Long> alberatura = new ArrayList<>();
			for (int i = alberaturaNodi.size() - 1; i >= 0; i--) {
				alberatura.add(alberaturaNodi.get(i).getIdNodo());
			}

			// prendo da DB i nodi di primo livello
			final NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
			nodoRadice.setIdAOO(utente.getIdAoo());
			nodoRadice.setIdNodo(utente.getIdNodoRadiceAOO());
			nodoRadice.setCodiceAOO(utente.getCodiceAoo());
			nodoRadice.setUtentiVisible(false);
			List<NodoOrganigrammaDTO> primoLivello = null;

			switch (tipoAssegnazioneSelected) {
			case COMPETENZA:
				primoLivello = orgSRV.getFigliAlberoAssegnatarioPerCompetenzaUscita(utente, nodoRadice);
				break;

			case FIRMA:
			case FIRMA_MULTIPLA:
				nodoRadice.setUtentiVisible(true);
				primoLivello = orgSRV.getFigliAlberoAssegnatarioPerFirma(utente.getIdUfficio(), nodoRadice);
				break;

			case SIGLA:
				nodoRadice.setUtentiVisible(true);
				primoLivello = orgSRV.getFigliAlberoAssegnatarioPerSigla(utente.getIdUfficio(), nodoRadice);
				break;

			default:
				break;
			}

			root = new DefaultTreeNode();
			root.setExpanded(true);
			root.setSelectable(false);

			// per ogni nodo di primo livello creo il nodo da mettere nell'albero che ha
			// come padre rootAssegnazionePerCompetenza
			List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
			root.setChildren(new ArrayList<>());
			DefaultTreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(utente.getCodiceAoo());
				if (n.getIdUtente() == null && (tipoAssegnazioneSelected == TipoAssegnazioneEnum.FIRMA || tipoAssegnazioneSelected == TipoAssegnazioneEnum.FIRMA_MULTIPLA
						|| tipoAssegnazioneSelected == TipoAssegnazioneEnum.SIGLA)) {
					n.setUtentiVisible(true);
				}
				nodoToAdd = new DefaultTreeNode(n, root);
				nodoToAdd.setExpanded(true);
				if (n.getIdUtente() == null && alberatura.contains(n.getIdNodo())) {
					nodiDaAprire.add(nodoToAdd);
				}
			}

			final List<DefaultTreeNode> nodiDaAprireTemp = new ArrayList<>();
			for (int i = 0; i < alberatura.size(); i++) {
				// devo fare esattamente un numero di cicli uguale alla lunghezza
				// dell'alberatura
				nodiDaAprireTemp.clear();
				for (final DefaultTreeNode nodo : nodiDaAprire) {
					onOpenTreeAssIterManuale(nodo);
					nodo.setExpanded(true);
					for (final TreeNode nodoFiglio : nodo.getChildren()) {
						final NodoOrganigrammaDTO nodoFiglioDto = (NodoOrganigrammaDTO) nodoFiglio.getData();
						if (nodoFiglioDto.getIdUtente() == null && alberatura.contains(nodoFiglioDto.getIdNodo())) {
							nodiDaAprireTemp.add((DefaultTreeNode) nodoFiglio);
						}
					}
				}
				nodiDaAprire = new ArrayList<>(nodiDaAprireTemp);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Gestisce l'evento di "Expand" di un nodo del tree delle assegnazioni
	 * dell'iter manuale.
	 * 
	 * @param event
	 */
	public void onOpenTreeAssIterManuale(final NodeExpandEvent event) {
		onOpenTreeAssIterManuale(event.getTreeNode());
	}

	private void onOpenTreeAssIterManuale(final TreeNode tree) {

		try {
			final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) tree.getData();
			List<NodoOrganigrammaDTO> figli = null;
			switch (tipoAssegnazioneSelected) {
			case COMPETENZA:
				figli = orgSRV.getFigliAlberoAssegnatarioPerCompetenzaUscita(utente, nodoExp);
				break;

			case FIRMA:
			case FIRMA_MULTIPLA:
				figli = orgSRV.getFigliAlberoAssegnatarioPerFirma(utente.getIdUfficio(), nodoExp);
				break;

			case SIGLA:
				figli = orgSRV.getFigliAlberoAssegnatarioPerSigla(utente.getIdUfficio(), nodoExp);
				break;

			default:
				break;
			}

			DefaultTreeNode nodoToAdd = null;
			tree.getChildren().clear();
			for (final NodoOrganigrammaDTO n : figli) {
				n.setCodiceAOO(utente.getCodiceAoo());

				if ((n.getIdUtente() == null || n.getIdUtente() == 0) && (tipoAssegnazioneSelected == TipoAssegnazioneEnum.FIRMA
						|| tipoAssegnazioneSelected == TipoAssegnazioneEnum.FIRMA_MULTIPLA || tipoAssegnazioneSelected == TipoAssegnazioneEnum.SIGLA)) {
					n.setUtentiVisible(true);
				}

				nodoToAdd = new DefaultTreeNode(n, tree);
				if (n.getFigli() > 0 || n.isUtentiVisible()) {
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}

	}

	/**
	 * Gestisce l'evento "Select" associato ai nodi del tree delle assegnazioni iter
	 * manuale.
	 * 
	 * @param event
	 */
	public void onSelectTreeAssIterManuale(final NodeSelectEvent event) {

		try {
			newAssegnazione = (NodoOrganigrammaDTO) event.getTreeNode().getData();

			if ((tipoAssegnazioneSelected == TipoAssegnazioneEnum.FIRMA || tipoAssegnazioneSelected == TipoAssegnazioneEnum.FIRMA_MULTIPLA
					|| tipoAssegnazioneSelected == TipoAssegnazioneEnum.SIGLA) && (newAssegnazione.getIdUtente() == null || newAssegnazione.getIdUtente() <= 0)) {
				return;
			}

			descrizioneNewAssegnazione = newAssegnazione.getDescrizioneNodo();

			UtenteDTO ute = null;
			if (newAssegnazione.getIdUtente() != null) {
				descrizioneNewAssegnazione += " - " + newAssegnazione.getNomeUtente() + " " + newAssegnazione.getCognomeUtente();
				ute = new UtenteDTO();
				ute.setId(newAssegnazione.getIdUtente());
				ute.setIdUfficio(newAssegnazione.getIdNodo());
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}

	}

	/**
	 * @return the flagIterManuale
	 */
	public final boolean isFlagIterManuale() {
		return flagIterManuale;
	}

	/**
	 * @param flagIterManuale the flagIterManuale to set
	 */
	public final void setFlagIterManuale(final boolean flagIterManuale) {
		this.flagIterManuale = flagIterManuale;
	}

	/**
	 * @return the flagUrgente
	 */
	public final boolean isFlagUrgente() {
		return flagUrgente;
	}

	/**
	 * @param flagUrgente the flagUrgente to set
	 */
	public final void setFlagUrgente(final boolean flagUrgente) {
		this.flagUrgente = flagUrgente;
	}

	/**
	 * @return the flagFirmaDigitaleRGS
	 */
	public final boolean isFlagFirmaDigitaleRGS() {
		return flagFirmaDigitaleRGS;
	}

	/**
	 * @param flagFirmaDigitaleRGS the flagFirmaDigitaleRGS to set
	 */
	public final void setFlagFirmaDigitaleRGS(final boolean flagFirmaDigitaleRGS) {
		this.flagFirmaDigitaleRGS = flagFirmaDigitaleRGS;
	}

	/**
	 * @return the descrizioneNewAssegnazione
	 */
	public final String getDescrizioneNewAssegnazione() {
		return descrizioneNewAssegnazione;
	}

	/**
	 * @param descrizioneNewAssegnazione the descrizioneNewAssegnazione to set
	 */
	public final void setDescrizioneNewAssegnazione(final String descrizioneNewAssegnazione) {
		this.descrizioneNewAssegnazione = descrizioneNewAssegnazione;
	}

	/**
	 * @return the root
	 */
	public final DefaultTreeNode getRoot() {
		return root;
	}

	/**
	 * @param root the root to set
	 */
	public final void setRoot(final DefaultTreeNode root) {
		this.root = root;
	}

	/**
	 * @return the iterApprovativoList
	 */
	public final Collection<IterApprovativoDTO> getIterApprovativoList() {
		return iterApprovativoList;
	}

	/**
	 * @param iterApprovativoList the iterApprovativoList to set
	 */
	public final void setIterApprovativoList(final Collection<IterApprovativoDTO> iterApprovativoList) {
		this.iterApprovativoList = iterApprovativoList;
	}

	/**
	 * @return the iterApprovativoSelected
	 */
	public final Integer getIdIterApprovativoSelected() {
		return idIterApprovativoSelected;
	}

	/**
	 * @param iterApprovativoSelected the iterApprovativoSelected to set
	 */
	public final void setIdIterApprovativoSelected(final Integer idIterApprovativoSelected) {
		this.idIterApprovativoSelected = idIterApprovativoSelected;
	}

	/**
	 * @return the enableFirmaDigitaleRGS
	 */
	public final boolean isEnableFirmaDigitaleRGS() {
		return enableFirmaDigitaleRGS;
	}

	/**
	 * @param enableFirmaDigitaleRGS the enableFirmaDigitaleRGS to set
	 */
	public final void setEnableFirmaDigitaleRGS(final boolean enableFirmaDigitaleRGS) {
		this.enableFirmaDigitaleRGS = enableFirmaDigitaleRGS;
	}

	/**
	 * @return the coordinatoriList
	 */
	public final Collection<CoordinatoreUfficioDTO> getCoordinatoriList() {
		return coordinatoriList;
	}

	/**
	 * @param coordinatoriList the coordinatoriList to set
	 */
	public final void setCoordinatoriList(final Collection<CoordinatoreUfficioDTO> coordinatoriList) {
		this.coordinatoriList = coordinatoriList;
	}

	/**
	 * @return the idCoordinatoreSelected
	 */
	public final Integer getIdCoordinatoreSelected() {
		return idCoordinatoreSelected;
	}

	/**
	 * @param idCoordinatoreSelected the idCoordinatoreSelected to set
	 */
	public final void setIdCoordinatoreSelected(final Integer idCoordinatoreSelected) {
		this.idCoordinatoreSelected = idCoordinatoreSelected;
	}

	/**
	 * @return the tipiAssegnazioniList
	 */
	public final List<TipoAssegnazioneEnum> getTipiAssegnazioniList() {
		return tipiAssegnazioniList;
	}

	/**
	 * @param tipiAssegnazioniList the tipiAssegnazioniList to set
	 */
	public final void setTipiAssegnazioniList(final List<TipoAssegnazioneEnum> tipiAssegnazioniList) {
		this.tipiAssegnazioniList = tipiAssegnazioniList;
	}

	/**
	 * @return the tipoAssegnazioneSelected
	 */
	public final TipoAssegnazioneEnum getTipoAssegnazioneSelected() {
		return tipoAssegnazioneSelected;
	}

	/**
	 * @param tipoAssegnazioneSelected the tipoAssegnazioneSelected to set
	 */
	public final void setTipoAssegnazioneSelected(final TipoAssegnazioneEnum tipoAssegnazioneSelected) {
		this.tipoAssegnazioneSelected = tipoAssegnazioneSelected;
	}

	/**
	 * Restituisce true se copia conforme.
	 * 
	 * @return true se copia conforme, false altrimenti.
	 */
	public boolean isShowCopiaConforme() {
		return showCopiaConforme;
	}

	/**
	 * Impsota il flag associato alla copia conforme.
	 * 
	 * @param showCopiaConforme
	 */
	public void setShowCopiaConforme(final boolean showCopiaConforme) {
		this.showCopiaConforme = showCopiaConforme;
	}

	/**
	 * Restituisce true se mostra la selezione dell'organigramma, false altrimenti.
	 * 
	 * @return showOrganigrammaSelection
	 */
	public boolean isShowOrganigrammaSelection() {
		return showOrganigrammaSelection;
	}

	/**
	 * @param showOrganigrammaSelection
	 */
	public void setShowOrganigrammaSelection(final boolean showOrganigrammaSelection) {
		this.showOrganigrammaSelection = showOrganigrammaSelection;
	}

	/**
	 * Restituisce la lista dei respnsabili copia conforme per il popolamento della
	 * combobox.
	 * 
	 * @return lista responsabili copia conforme
	 */
	public List<ResponsabileCopiaConformeDTO> getComboResponsabileCopiaConforme() {
		return comboResponsabileCopiaConforme;
	}

	/**
	 * Imposta la lista dei respnsabili copia conforme per il popolamento della
	 * combobox.
	 * 
	 * @param comboResponsabileCopiaConforme
	 */
	public void setComboResponsabileCopiaConforme(final List<ResponsabileCopiaConformeDTO> comboResponsabileCopiaConforme) {
		this.comboResponsabileCopiaConforme = comboResponsabileCopiaConforme;
	}

	/**
	 * Restituisce il responsabile copia conforme.
	 * 
	 * @return responsabile copia conforme
	 */
	public ResponsabileCopiaConformeDTO getResponsabileCopiaConformeObj() {
		return responsabileCopiaConformeObj;
	}

	/**
	 * Imposta il responsabile copia conforme.
	 * 
	 * @param responsabileCopiaConformeObj
	 */
	public void setResponsabileCopiaConformeObj(final ResponsabileCopiaConformeDTO responsabileCopiaConformeObj) {
		this.responsabileCopiaConformeObj = responsabileCopiaConformeObj;
	}

}
