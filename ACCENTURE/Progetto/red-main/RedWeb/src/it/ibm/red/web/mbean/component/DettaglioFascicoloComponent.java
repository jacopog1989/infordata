package it.ibm.red.web.mbean.component;

import java.util.ArrayList;
import java.util.List;

import it.ibm.red.business.dto.DetailFascicoloRedDTO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.MasterFascicoloDTO;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.DettaglioFascicoloAbstractBean;
import it.ibm.red.web.mbean.FascicoloManagerBean;
import it.ibm.red.web.mbean.interfaces.IUpdatableDetailCaller;

/**
 * Permette di aprire un dettaglio di un fascicolo a partire da MasterFascicoloDTO.
 * 
 * @author a.difolca
 *
 */
public class DettaglioFascicoloComponent extends DettaglioFascicoloAbstractBean implements IUpdatableDetailCaller<DetailFascicoloRedDTO> {
	
	private static final long serialVersionUID = -3615428172068288428L;
	
	/**
	 * Master fascicolo.
	 */
	private MasterFascicoloDTO masterFascicolo;

	/**
	 * Costruttore del component.
	 */
	public DettaglioFascicoloComponent() {
		super();
		postConstruct();
	}

	/**
	 * Imposta il dettaglio del fascicolo.
	 * @param mf
	 */
	public void setDetail(final MasterFascicoloDTO mf) {
		final List<DocumentoFascicoloDTO> documenti = mf.getDocumenti() == null ? new ArrayList<>() : new ArrayList<>(mf.getDocumenti());
		super.setDetail(mf.getId(), documenti);
		this.masterFascicolo = mf;
	}

	/**
	 * Gestisce l'apertura della dialog del fascicolo.
	 */
	@Override
	public void openDialogFascicolo() {
		
		super.openDialogFascicolo();
		
		final FascicoloManagerBean bean = FacesHelper.getManagedBean(MBean.FASCICOLO_MANAGER_BEAN);
		if (bean != null) {
			bean.setCaller(this);
			bean.setChiamante(ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN);
			bean.setAzioneAlCloseDialog("");
		}
	}

	/**
	 * Apre la dialog del fascicolo.
	 */
	public void openDialogFascicoloConAzioneAlCloseDialog() {
		
		super.openDialogFascicolo();
		
		final FascicoloManagerBean bean = FacesHelper.getManagedBean(MBean.FASCICOLO_MANAGER_BEAN);
		if (bean != null) {
			bean.setCaller(this);
			bean.setChiamante(ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN);
			bean.setAzioneAlCloseDialog("rimuoviElementoClassificazione");
		}
	}
	
	/**
	 * Aggiorna il dettaglio col dettaglio passato come parametro.
	 * @param detail dettaglio da impostare
	 */
	@Override
	public void updateDetail(final DetailFascicoloRedDTO detail) {
		this.fascicoloCmp.setDetail(detail);
		masterFascicolo.setDocumenti(detail.getDocumenti());
	}
}
