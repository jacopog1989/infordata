package it.ibm.red.web.helper.lsign;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.dto.InfoAppletDTO;
import it.ibm.red.web.servlets.delegate.CopiaConformeGetDelegate;
import it.ibm.red.web.servlets.delegate.CopiaConformePostDelegate;
import it.ibm.red.web.servlets.delegate.IGetDelegate;
import it.ibm.red.web.servlets.delegate.IPostDelegate;
import it.ibm.red.web.servlets.delegate.StandardGetDelegate;
import it.ibm.red.web.servlets.delegate.StandardPostDelegate;

/**
 * 
 * @author CPIERASC
 *
 *         Classe utilizzata per gestire i parametri dell'applet di firma
 *         locale.
 */
public final class LocalSignHelper implements Serializable {

	private static final long serialVersionUID = -1062414984396759536L;

	/**
	 * Enum per la tipologia di documento.
	 */
	public enum TipoDoc {

		/**
		 * Valore.
		 */
		PRINCIPALE,

		/**
		 * Valore.
		 */
		ALLEGATO
	}

	/**
	 * Esiti.
	 */
	private List<EsitoOperazioneDTO> esiti;

	/**
	 * Documenti.
	 */
	private LinkedList<DocSignDTO> docSet;

	/**
	 * Firmatario.
	 */
	private Integer idSigner;

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Tipo firma.
	 */
	private SignTypeEnum ste;

	/**
	 * Documento.
	 */
	private DocSignDTO docSigning;

	/**
	 * Infomrazioni applet.
	 */
	private InfoAppletDTO infoApplet;

	/**
	 * Flag copia conforme.
	 */
	private boolean copiaConforme;

	/**
	 * Flag firma multipla.
	 */
	private boolean firmaMultipla;

	/**
	 * Glifo delegato.
	 */
	private boolean glifoDelegato;
	
	/**
	 * Sign transaction ID.
	 */
	private String signTransactionId;

	/**
	 * DTO del documento firmato.
	 */
	public class DocSignDTO implements Serializable {

		/**
		 * Costante serial version UID.
		 */
		private static final long serialVersionUID = -2605844572349795049L;

		/**
		 * Identificatore WF.
		 */
		private String wobNumber;

		/**
		 * Identificativo documento.
		 */
		private String documentTitle;
		
		/**
		 * GUID.
		 */
		private String guid;

		/**
		 * Tipologia documento.
		 */
		private TipoDoc tipoDoc;

		/**
		 * Identificativo documento principale.
		 */
		private String idDocPrincipale;

		/**
		 * Numero documento principale.
		 */
		private Integer numeroDocumentoPrincipale;

		/**
		 * Nome file.
		 */
		private String nomeFile;

		/**
		 * Content stampigliato.
		 */
		private byte[] contentStampigliato;

		/**
		 * Data firma.
		 */
		private Date dataFirma;

		/**
		 * Flag error.
		 */
		private boolean doGetError;
		
		/**
		 * sottocategoriadocumentouscita.
		 */
		private SottoCategoriaDocumentoUscitaEnum sottoCategoriaDocumentoUscita;

		/**
		 * Costruttore di default.
		 * 
		 * @param wobNumber
		 * @param idDoc
		 * @param tipoDoc
		 * @param idDocPrincipale
		 * @param numeroDocumentoPrincipale
		 * @param nomeFile
		 */
		public DocSignDTO(final String wobNumber, final String documentTitle, final String guid, final TipoDoc tipoDoc, 
				final String idDocPrincipale, final Integer numeroDocumentoPrincipale, final String nomeFile, final SottoCategoriaDocumentoUscitaEnum sottoCategoriaDocumentoUscita) {
			super();
			this.wobNumber = wobNumber;
			this.documentTitle = documentTitle;
			this.guid = guid;
			this.tipoDoc = tipoDoc;
			this.idDocPrincipale = idDocPrincipale;
			this.numeroDocumentoPrincipale = numeroDocumentoPrincipale;
			this.nomeFile = nomeFile;
			this.sottoCategoriaDocumentoUscita = sottoCategoriaDocumentoUscita;
			setDoGetError(false);
		}

		/**
		 * Restituisce il wobNumber.
		 * 
		 * @return wobNumber
		 */
		public String getWobNumber() {
			return wobNumber;
		}
		
		/**
		 * Imposta il guid.
		 * @param guid
		 */
		public void setGuid(String guid) {
			this.guid=guid;
		}

		/**
		 * Restituisce il documentTitle.
		 * @return document title
		 */
		public String getDocumentTitle() {
			return documentTitle;
		}

		/**
		 * Restituisce il guid.
		 * @return guid
		 */
		public String getGuid() {
			return guid;
		}

		/**
		 * Restituisce la tipologia del documento.
		 * 
		 * @return tipoDoc
		 */
		public TipoDoc getTipoDoc() {
			return tipoDoc;
		}

		/**
		 * Imposta la tipologia del documento.
		 * 
		 * @param tipoDoc
		 */
		public void setTipoDoc(final TipoDoc tipoDoc) {
			this.tipoDoc = tipoDoc;
		}

		/**
		 * Restituisce l'id del documento principale.
		 * 
		 * @return idDocPrincipale
		 */
		public String getIdDocPrincipale() {
			return idDocPrincipale;
		}
		
		/**
		 * Restituisce il numero del documento principale.
		 * 
		 * @return numero documento principale
		 */
		public Integer getNumeroDocumentoPrincipale() {
			return numeroDocumentoPrincipale;
		}

		/**
		 * Imposta il numero documento del doc. principale.
		 * 
		 * @param numeroDocumentoPrincipale
		 */
		public String getNomeFile() {
			return nomeFile;
		}

		/**
		 * Restituisce il content stampigliato.
		 * 
		 * @return contentStampigliato
		 */
		public byte[] getContentStampigliato() {
			return contentStampigliato;
		}

		/**
		 * Imposta il content stampigliato.
		 * 
		 * @param contentStampigliato
		 */
		public void setContentStampigliato(final byte[] contentStampigliato) {
			this.contentStampigliato = contentStampigliato;
		}

		/**
		 * Restituisce la data di firma.
		 * @return data firma
		 */
		public Date getDataFirma() {
			return dataFirma;
		}

		/**
		 * Imposta la data della firma.
		 * 
		 * @param dataFirma
		 */
		public void setDataFirma(final Date dataFirma) {
			this.dataFirma = dataFirma;
		}

		/**
		 * Restituisce il flag doGateError.
		 * @return flag doGetError
		 */
		public boolean isDoGetError() {
			return doGetError;
		}

		/**
		 * Imposta il flag d'errore relativo alla chiamata DoGET.
		 * 
		 * @param doGetError
		 */
		public void setDoGetError(final boolean doGetError) {
			this.doGetError = doGetError;
		}

		public SottoCategoriaDocumentoUscitaEnum getSottoCategoriaDocumentoUscita() {
			return sottoCategoriaDocumentoUscita;
		}

		public void setSottoCategoriaDocumentoUscita(SottoCategoriaDocumentoUscitaEnum sottoCategoriaDocumentoUscita) {
			this.sottoCategoriaDocumentoUscita = sottoCategoriaDocumentoUscita;
		}

	}

	/**
	 * Costruttore.
	 * 
	 * @param source       pagina sorgente (in cui è presente l'applet)
	 * @param inSte        tipo di firma richiesto (cades/pades)
	 * @param getDelegate  delegato per gestire il recupero del documento da firmare
	 * @param postDelegate delegato per gestire la restituzione del documento
	 *                     firmato
	 */
	private LocalSignHelper(final String inSource, final SignTypeEnum inSte, final boolean inCopiaConforme, final boolean inFirmaMultipla,
			final Class<? extends IGetDelegate> getDelegate, final Class<? extends IPostDelegate> postDelegate) {
		this();
		final String action = SignTypeEnum.CADES.equals(inSte) ? "SIGN" : "SIGNDIGEST";
		infoApplet = new InfoAppletDTO(inSource, action, getDelegate, postDelegate);
		esiti = new ArrayList<>();
		ste = inSte;
		copiaConforme = inCopiaConforme;
		firmaMultipla = inFirmaMultipla; 
	}

	private LocalSignHelper() {
		super();
	}
	
	private static DocSignDTO createDocSignDTO(final String wobNumber, final String documentTitle, final String guid, final TipoDoc tipo, final String idDocPrincipale, final Integer numeroDocumentoPrincipale, 
			final String nomeFile, final SottoCategoriaDocumentoUscitaEnum sottoCategoriaDocumentoUscita) {
		return (new LocalSignHelper()).new DocSignDTO(wobNumber, documentTitle, guid, tipo, idDocPrincipale, numeroDocumentoPrincipale, nomeFile, sottoCategoriaDocumentoUscita);
	}

	/**
	 * Metodo per garatinre il singeton dell'helper di firma locale.
	 * 
	 * @param inSource
	 * @param inSte
	 * @param copiaConforme
	 * @param firmaMultipla
	 * @return the instance
	 */
	public static LocalSignHelper getInstance(final String inSource, final SignTypeEnum inSte, final boolean copiaConforme, final boolean firmaMultipla) {
		if (!copiaConforme) {
			return new LocalSignHelper(inSource, inSte, copiaConforme, firmaMultipla, StandardGetDelegate.class, StandardPostDelegate.class);
		} else {
			return new LocalSignHelper(inSource, inSte, copiaConforme, firmaMultipla, CopiaConformeGetDelegate.class, CopiaConformePostDelegate.class);
		}
	}

	/**
	 * Rimuove il documento, se firmato, dall'insieme di documenti da firmare.
	 * 
	 * @param esito
	 */
	public void removeDocToSign(final EsitoOperazioneDTO esito) {
		if (esito.isEsito()) { // Se è stato firmato correttamente
			// Eliminalo fra i documenti da firmare
			docSet.remove(docSigning);
		} else { // Se NON è stato firmato correttamente
			// Elimina dai documenti da firmare quelli che hanno il suo stesso idDocPrincipale 
			removeNextDocSign();
		}

		boolean trovato = false;
		for (int i = (esiti.size() - 1); i >= 0; i--) {
			// Scorri gli esiti dall'ultimo che dovrebbe essere il documento principale dell'allegato che non è stato firmato correttamente
			if ((esiti.get(i).getIdDocumento()).equals(docSigning.getNumeroDocumentoPrincipale())) {
				if (esiti.get(i).getEsitiAllegati() == null) {
					esiti.get(i).setEsitiAllegati(new ArrayList<>());
				}
				esiti.get(i).getEsitiAllegati().add(esito);
				esiti.get(i).setAnnoProtocollo(esito.getAnnoProtocollo());
				esiti.get(i).setNumeroProtocollo(esito.getNumeroProtocollo());
				trovato = true;
				break;
			}
		}
		if (!trovato) {
			esiti.add(esito);
		}
	}

	/**
	 * Aggiunge un documento all'insieme di documenti da firmare.
	 * 
	 * @param wobNumber
	 * @param idDoc
	 * @param numeroDocumento
	 * @param tipo
	 * @param nomeFile
	 */
	public void addDocToSign(final String wobNumber, final String documentTitle, final String guid, final Integer numeroDocumento, final TipoDoc tipo, final String nomeFile, final SottoCategoriaDocumentoUscitaEnum sottoCategoriaDocumentoUscita) {
		if (docSet == null) {
			docSet = new LinkedList<>();
		}
		if (TipoDoc.PRINCIPALE.equals(tipo)) {
			docSet.add(LocalSignHelper.createDocSignDTO(wobNumber, documentTitle, guid, tipo, documentTitle, numeroDocumento, nomeFile, sottoCategoriaDocumentoUscita));
		} else if (TipoDoc.ALLEGATO.equals(tipo)) {
			//questo è il caso in cui sto aggiungendo gli allegati del documento principale che sto firmando (docSigning)
			docSet.add(1, LocalSignHelper.createDocSignDTO(docSigning.getWobNumber(), documentTitle, guid, tipo, docSigning.getDocumentTitle(), 
					docSigning.getNumeroDocumentoPrincipale(), nomeFile,  sottoCategoriaDocumentoUscita));
		}
	}

	/**
	 * Getter messaggio applet.
	 * 
	 * @return messaggio applet
	 */
	public String getMsg() {
		Integer nRemainingDocs = 0;
		if (docSet != null) {
			nRemainingDocs = docSet.size();
		}
		return InfoAppletDTO.getMsg(nRemainingDocs);
	}

	/**
	 * Metodo per firmare un insieme di contenuti.
	 * 
	 * @param idDocSet lista degli identificativi dei documenti da firmare
	 * @param idSigner id del firmatario
	 */
	public void sign(final List<MasterDocumentRedDTO> inDocumentSet, final UtenteDTO inUtente, final boolean inCopiaConforme) {
		idSigner = inUtente.getId().intValue();
		utente = inUtente;
		infoApplet.setRenderSign(true);
		docSigning = null;
		esiti.clear();
		docSet = new LinkedList<>();
		copiaConforme = inCopiaConforme;
		for (final MasterDocumentRedDTO doc : inDocumentSet) {
			addDocToSign(doc.getWobNumber(), doc.getDocumentTitle(), StringUtils.restoreGuidFromString(doc.getGuuid()), doc.getNumeroDocumento(), TipoDoc.PRINCIPALE, doc.getNomeFile(), doc.getSottoCategoriaDocumentoUscita());
		}
	}

	/**
	 * Metodo per dichiarare completata la sessione di firma.
	 */
	public void signComplete() {
		infoApplet.setRenderSign(false);
		if (docSet != null) {
			docSet.clear();
		}
		docSigning = null;
	}

	/**
	 * Restituisce il flag che indica se il documento corrente è l'ultimo
	 * dell'insieme.
	 * 
	 * @return true, se è l'ultimo; false, altrimenti.
	 */
	public boolean isLastForDocPrincipale() {
		Boolean output = true;

		for (final DocSignDTO nextDoc : docSet) {
			if (!nextDoc.equals(docSigning)) {
				if (nextDoc.getIdDocPrincipale().equals(docSigning.getIdDocPrincipale())) {
					output = false;
				}
				break;
			}
		}
		return output;
	}
	
	/**
	 * Restituisce gli id dei documenti firmati ed inviati ad NPS.
	 * 
	 * @return docSentToNPS
	 */
	public List<EsitoOperazioneDTO> getEsiti() {
		return esiti;
	}

	/**
	 * Restituisce la lista di documenti da firmare.
	 * 
	 * @return docSet
	 */
	public List<DocSignDTO> getDocSet() {
		return docSet;
	}

	/**
	 * restituisce l'id del firmatario.
	 * 
	 * @return idSigner
	 */
	public Integer getIdSigner() {
		return idSigner;
	}

	/**
	 * Ritorna il tipo di firma richiesto
	 * 
	 * @return ste
	 */
	public SignTypeEnum getSte() {
		return ste;
	}

	/**
	 * Restituisce il DTO del documento corrente da firmare.
	 * 
	 * @return
	 */
	public DocSignDTO getDocSigning() {
		return docSigning;
	}

	/**
	 * Restituisce il prossimo documento da firmare.
	 * 
	 * @return docSigning
	 */
	public DocSignDTO nextDocToSign() {
		docSigning = null;
		if (docSet != null && !docSet.isEmpty()) {
			docSigning = docSet.get(0);
		}
		return docSigning;
	}

	/**
	 * Indica se esiste un prossimo documento da firmare.
	 * 
	 * @return output
	 */
	public boolean hasNextDocIdToSign() {
		boolean output = false;
		if (docSet != null) {
			output = !docSet.isEmpty();
		}
		return output;
	}

	/**
	 * Rimuove i documenti associati al documento principale relativo al documento
	 * che è appena andato in errore fra quelli che si devono ancora firmare
	 */
	private void removeNextDocSign() {
		docSet.removeIf(i -> i.getIdDocPrincipale().equals(docSigning.getIdDocPrincipale()));
	}

	/**
	 * Restiuisce il DTO per le informazioni dell'applet.
	 * 
	 * @return infoApplet
	 */
	public InfoAppletDTO getInfoApplet() {
		return infoApplet;
	}

	/**
	 * Imposta il DTO per le informazioni dell'applet.
	 * 
	 * @param infoApplet
	 */
	public void setInfoApplet(final InfoAppletDTO infoApplet) {
		this.infoApplet = infoApplet;
	}

	/**
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Impsta l'utente corrente.
	 * 
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Indica se il documento è flaggato come "copia conforme".
	 * 
	 * @return copiaConforme
	 */
	public boolean isCopiaConforme() {
		return copiaConforme;
	}

	/**
	 * Indica se il documento è flaggato per la firma multipla.
	 * 
	 * @return firmaMultipla
	 */
	public boolean isFirmaMultipla() {
		return firmaMultipla;
	}

	/**
	 * Restituisce true se glifo delegato, false altrimenti.
	 * @return true se glifo delegato, false altrimenti
	 */
	public boolean isGlifoDelegato() {
		return glifoDelegato;
	}

	/**
	 * Imposta il flag associato al glifo delegato.
	 * @param glifoDelegato
	 */
	public void setGlifoDelegato(boolean glifoDelegato) {
		this.glifoDelegato = glifoDelegato;
	}
	
	/**
	 * Restituisce il signTransactionId.
	 * @return signTransactionId
	 */
	public String getSignTransactionId() {
		return signTransactionId;
	}

	/**
	 * Imposta il SignTransactionId.
	 * @param signTransactionId
	 */
	public void setSignTransactionId(String signTransactionId) {
		this.signTransactionId = signTransactionId;
	}

}