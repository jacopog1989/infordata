package it.ibm.red.web.report.mbean.component;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.dto.UtenteRuoloReportDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IReportFacadeSRV;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.helper.faces.MessageSeverityEnum;

/**
 * Component che gestisce i risultati del report generato per ruolo.
 */
public class RisultatiRuoloReportComponent implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
    /**
     * Logger.
     */
	private static final REDLogger LOGGER = REDLogger.getLogger(RisultatiRuoloReportComponent.class);
	
    /**
     * Service.
     */
	private final IReportFacadeSRV reportSRV;
	
	/**
	 * Lista da visualizzare nell'anteprima.
	 */
	private transient List<UtenteRuoloReportDTO> masterList;
	
	/**
	 * Lista da visualizzare eventualmente nel dettaglio.
	 */
	private transient List<UtenteRuoloReportDTO> detailList;
	
	/**
	 * Descrizione sopra alla tabella.
	 */
	private String detailTableHeader;
	
	/**
	 * Descrizione operazione selezionata dall'utente.
	 */
	private String descrizioneOperazione;
	
	/**
	 * Costruttore. Inizializza i service.
	 */
	public RisultatiRuoloReportComponent() {
		reportSRV = ApplicationContextProvider.getApplicationContext().getBean(IReportFacadeSRV.class);
	}
	
	/**
	 * Consente la visualizzazione del dettaglio associato al master.
	 * @param master
	 */
	public void showDetail(final UtenteRuoloReportDTO master) {
		try {
			final String descrUfficio = master.getUfficio();
			final String descrUsername = master.getRuolo();
			if (StringUtils.isNotBlank(descrUsername)) {
				this.detailTableHeader = descrUfficio + " - " + descrUsername;
			} else {
				this.detailTableHeader = descrUfficio;
			}
			
			this.detailList = reportSRV.ricercaDetailUtentiAttiviConUnDatoRuoloForUff(master);
		} catch (final Exception e) {
			LOGGER.error("Errore nel componente per il recupero dei report", e);
			FacesHelper.showMessage(null, "Errore durante il recupero del report", MessageSeverityEnum.ERROR);
		}
	}

	/**
	 * Restituisce la lista dei master.
	 * @return lista master
	 */
	public List<UtenteRuoloReportDTO> getMasterList() {
		return masterList;
	}

	/**
	 * Restituisce la lista dei detail.
	 * @return lista detail
	 */
	public List<UtenteRuoloReportDTO> getDetailList() {
		return detailList;
	}

	/**
	 * Restituisce l'header del detail table.
	 * @return header detail table
	 */
	public String getDetailTableHeader() {
		return detailTableHeader;
	}

	/**
	 * Restituisce la descrizione dell'operazione.
	 * @return descrizione operazione
	 */
	public String getDescrizioneOperazione() {
		return descrizioneOperazione;
	}

	/**
	 * Imposta la descrizione dell'operazione.
	 * @param descrizioneOperazione
	 */
	public void setDescrizioneOperazione(final String descrizioneOperazione) {
		this.descrizioneOperazione = descrizioneOperazione;
	}

	/**
	 * Imposta la lista dei master.
	 * @param masterList
	 */
	public void setMasterList(final List<UtenteRuoloReportDTO> masterList) {
		this.masterList = masterList;
	}
	
}