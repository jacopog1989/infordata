package it.ibm.red.web.report.model;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.enums.PeriodoReportEnum;
import it.ibm.red.business.persistence.model.CodaDiLavoroReport;
import it.ibm.red.business.persistence.model.DettaglioReport;
import it.ibm.red.business.persistence.model.Ruolo;
import it.ibm.red.business.persistence.model.TipoOperazioneReport;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.dto.AbstractDTO;

/**
 * DTO della maschera di report.
 */
public class ReportMascheraDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 9110542401837532526L;
	
	/**
	 * Anno.
	 */
	private int anno;
	
	/**
	 * Periodo.
	 */
	private PeriodoReportEnum periodo;
	
	/**
	 * Lista periodi.
	 */
	private List<PeriodoReportEnum> comboPeriodi;

	
	/**
	 * Flag date personalizzate.
	 */
	private boolean datePersonalizzateRendered;
	
	/**
	 * Data da.
	 */
	private Date dataDa;
	
	/**
	 * Data a.
	 */
	private Date dataA;
	
	
	/**
	 * Flag renderizza ruoli.
	 */
	private boolean ruoliRendered;
	
	/**
	 * Lista ruoli.
	 */
	private List<Ruolo> comboRuoli;
	
	/**
	 * Lista ruoli selezionati.
	 */
	private List<Long> ruoliSelected;

	
	/**
	 * Codice dettaglio selezionato.
	 */
	private String codiceDettaglioSelected;
	
	/**
	 * Dettaglio selezionato.
	 */
	private DettaglioReport dettaglioSelected;
	
	/**
	 * Lista dettagli report.
	 */
	private List<DettaglioReport> comboDettaglioReport;
	
	
	/**
	 * Codice tipo operazione selezionato.
	 */
	private String codiceTipoOperazioneSelected;
	
	/**
	 * Tipo operazione selezionato.
	 */
	private TipoOperazioneReport tipoOperazioneSelected;
	
	/**
	 * Lista tipi operazione.
	 */
	private List<TipoOperazioneReport> comboTipiOperazione;
	
	
	/**
	 * Flag code visualizzate..
	 */
	private boolean codeRendered;
	
	/**
	 * Codice coda di lavoro selezionata.
	 */
	private String codiceCodaDiLavoroSelected;
	
	/**
	 * Coda di lavoro selezionata.
	 */
	private CodaDiLavoroReport codaDiLavoroSelected;
	
	/**
	 * Lista code di lavoro.
	 */
	private transient List<CodaDiLavoroReport> codeDiLavoro;
	
	
	/**
	 * Flag renderizza filtri per report elenco divisionale.
	 */
	private boolean filtriElencoDivisionaleRendered;
	
	/**
	 * Data assegnazione.
	 */
	private Date dataAssegnazione;
	
	/**
	 * Tipo assegnazione.
	 */
	private String tipoAssegnazione;
	
	/**
	 * Numero protocollo da.
	 */
	private Integer numeroProtocolloDa;
	
	/**
	 * Id tipologia documento selezionato.
	 */
	private Integer idTipologiaDocumentoSelected;
	
	/**
	 * Lista tipologie documento.
	 */
	private List<TipologiaDocumentoDTO> comboTipologieDocumento;
	
	/**
	 * Flag cerca nei sottouffici.
	 */
	private boolean cercaSottouffici;
	
	
	/** Organigramma start *********************************/
	
	/**
	 * Lista nodi selezionate.
	 */
	private List<NodoOrganigrammaDTO> nodiSelected;
	
	/**
	 * Nodo selezionato.
	 */
	private transient TreeNode rootSelected;
	
	/**
	 * Utente root.
	 */
	private transient TreeNode utenteRoot;
	
	/**
	 * Ufficio root.
	 */
	private transient TreeNode ufficioRoot;
	/** Organigramma end ***********************************/
	
	/**
	 * Costruttore.
	 */
	public ReportMascheraDTO() {
		anno = Calendar.getInstance().get(Calendar.YEAR);
		codeRendered = false;
		datePersonalizzateRendered = false;
		ruoliRendered = false;
		filtriElencoDivisionaleRendered = false;
	}

	/* GET & SET *************************************************************************************************/
	/**
	 * Restituisce il tipo operazione del report.
	 * @return tipo operazione selezionato
	 */
	public TipoOperazioneReport getTipoOperazioneSelected() {
		return tipoOperazioneSelected;
	}
	
	/**
	 * Restituisce la coda di lavorazione selezionata.
	 * @return coda di lavoro selezionata
	 */
	public CodaDiLavoroReport getCodaDiLavoroSelected() {
		return codaDiLavoroSelected;
	}
	
	/**
	 * Restituisce il dettaglio selezionato.
	 * @return dettaglio report selezionato
	 */
	public DettaglioReport getDettaglioSelected() {
		return dettaglioSelected;
	}
	
	/**
	 * Restituisce l'anno.
	 * @return anno
	 */
	public int getAnno() {
		return anno;
	}

	/**
	 * Imposta l'anno.
	 * @param anno
	 */
	public void setAnno(final int anno) {
		this.anno = anno;
	}

	/**
	 * Restituisce il periodo.
	 * @return periodo
	 */
	public PeriodoReportEnum getPeriodo() {
		return periodo;
	}

	/**
	 * Imposta il periodo.
	 * @param periodo
	 */
	public void setPeriodo(final PeriodoReportEnum periodo) {
		this.periodo = periodo;
	}

	/**
	 * Restituisce true se le date personalizzate sono renderizzate, false altrimenti.
	 * @return true se le date personalizzate sono renderizzate, false altrimenti
	 */
	public boolean isDatePersonalizzateRendered() {
		return datePersonalizzateRendered;
	}

	/**
	 * @param datePersonalizzateRendered
	 */
	public void setDatePersonalizzateRendered(final boolean datePersonalizzateRendered) {
		this.datePersonalizzateRendered = datePersonalizzateRendered;
	}

	/**
	 * Restituisce la data iniziale del range.
	 * @return data iniziale
	 */
	public Date getDataDa() {
		return dataDa;
	}

	/**
	 * Imposta la data iniziale del range.
	 * @param dataDa
	 */
	public void setDataDa(final Date dataDa) {
		this.dataDa = dataDa;
	}

	/**
	 * Restituisce la data finale del range.
	 * @return data finale
	 */
	public Date getDataA() {
		return dataA;
	}

	/**
	 * Imposta la data finale del range.
	 * @param dataA
	 */
	public void setDataA(final Date dataA) {
		this.dataA = dataA;
	}

	/**
	 * Restituisce true se ruoli renderizzati, false altrimenti.
	 * @return true se ruoli renderizzati, false altrimenti
	 */
	public boolean isRuoliRendered() {
		return ruoliRendered;
	}

	/**
	 * Imposta la visibilita dei ruoli.
	 * @param ruoliRendered
	 */
	public void setRuoliRendered(final boolean ruoliRendered) {
		this.ruoliRendered = ruoliRendered;
	}

	/**
	 * Restituisce la lista dei ruoli selezionati.
	 * @return ruoli selezionati
	 */
	public List<Long> getRuoliSelected() {
		return ruoliSelected;
	}

	/**
	 * Imposta la lista dei ruoli selezionati.
	 * @param ruoli
	 */
	public void setRuoliSelected(final List<Long> ruoli) {
		this.ruoliSelected = ruoli;
	}

	/**
	 * Restituisce la lista dei dettagli report per la popolazione della combobox.
	 * @return lista dettagli report
	 */
	public List<DettaglioReport> getComboDettaglioReport() {
		return comboDettaglioReport;
	}

	/**
	 * Imposta la lista dei dettagli report per la popolazione della combobox.
	 * @param comboDettaglioReport
	 */
	public void setComboDettaglioReport(final List<DettaglioReport> comboDettaglioReport) {
		this.comboDettaglioReport = comboDettaglioReport;
	}

	/**
	 * Restituisce true se le code sono renderizzate, false altrimenti.
	 * @return true se le code sono renderizzate, false altrimenti
	 */
	public boolean isCodeRendered() {
		return codeRendered;
	}

	/**
	 * Imposta la visibilita delle code.
	 * @param codeRendered
	 */
	public void setCodeRendered(final boolean codeRendered) {
		this.codeRendered = codeRendered;
	}

	/**
	 * Restituisce la lista delle code di lavoro.
	 * @return lista code di lavoro
	 */
	public List<CodaDiLavoroReport> getCodeDiLavoro() {
		return codeDiLavoro;
	}

	/**
	 * Imposta la lista delle code di lavoro.
	 * @param codeDiLavoro
	 */
	public void setCodeDiLavoro(final List<CodaDiLavoroReport> codeDiLavoro) {
		this.codeDiLavoro = codeDiLavoro;
	}

	/**
	 * Restituisce true se i filtri sono renderizzati, false altrimenti.
	 * 
	 * @return true se i filtri sono renderizzati, false altrimenti
	 */
	public boolean isFiltriElencoDivisionaleRendered() {
		return filtriElencoDivisionaleRendered;
	}

	/**
	 * Imposta la visibilità dei filtri.
	 * 
	 * @param filtriElencoDivisionaleRendered
	 */
	public void setFiltriElencoDivisionaleRendered(boolean filtriElencoDivisionaleRendered) {
		this.filtriElencoDivisionaleRendered = filtriElencoDivisionaleRendered;
	}

	/**
	 * Restituisce la data assegnazione.
	 * 
	 * @return data assegnazione
	 */
	public Date getDataAssegnazione() {
		return dataAssegnazione;
	}

	/**
	 * Imposta la data assegnazione.
	 * 
	 * @param dataAssegnazione
	 */
	public void setDataAssegnazione(Date dataAssegnazione) {
		this.dataAssegnazione = dataAssegnazione;
	}

	/**
	 * Restituisce il tipo assegnazione.
	 * 
	 * @return tipo assegnazione
	 */
	public String getTipoAssegnazione() {
		return tipoAssegnazione;
	}

	/**
	 * Imposta il tipo assegnazione.
	 * 
	 * @param tipoAssegnazione
	 */
	public void setTipoAssegnazione(String tipoAssegnazione) {
		this.tipoAssegnazione = tipoAssegnazione;
	}

	/**
	 * Restituisce il numero protocollo da.
	 * 
	 * @return numero protocollo da
	 */
	public Integer getNumeroProtocolloDa() {
		return numeroProtocolloDa;
	}

	/**
	 * Imposta il numero protocollo da.
	 * 
	 * @param numeroProtocolloDa
	 */
	public void setNumeroProtocolloDa(Integer numeroProtocolloDa) {
		this.numeroProtocolloDa = numeroProtocolloDa;
	}

	/**
	 * Restituisce l'id tipologia documento selezionato.
	 * 
	 * @return id tipologia documento
	 */
	public Integer getIdTipologiaDocumentoSelected() {
		return idTipologiaDocumentoSelected;
	}

	/**
	 * Imposta l'id tipologia documento selezionato.
	 * 
	 * @param idTipologiaDocumento
	 */
	public void setIdTipologiaDocumentoSelected(Integer idTipologiaDocumentoSelected) {
		this.idTipologiaDocumentoSelected = idTipologiaDocumentoSelected;
	}

	/**
	 * Restituisce la lista delle tipologie documento.
	 * 
	 * @return lista tipologie documento
	 */
	public List<TipologiaDocumentoDTO> getComboTipologieDocumento() {
		return comboTipologieDocumento;
	}

	/**
	 * Imposta la lista delle tipologie documento.
	 * 
	 * @param comboTipologieDocumento
	 */
	public void setComboTipologieDocumento(List<TipologiaDocumentoDTO> comboTipologieDocumento) {
		this.comboTipologieDocumento = comboTipologieDocumento;
	}

	/**
	 * Restituisce true se cerca nei sottouffici, false altrimenti.
	 * 
	 * @return true se cerca nei sottouffici, false altrimenti
	 */
	public boolean isCercaSottouffici() {
		return cercaSottouffici;
	}

	/**
	 * Imposta il flag cerca nei sottouffici.
	 * 
	 * @param cercaSottouffici
	 */
	public void setCercaSottouffici(boolean cercaSottouffici) {
		this.cercaSottouffici = cercaSottouffici;
	}

	/**
	 * Restituisce la lista dei periodi per il popolamento della combobox.
	 * @return lista periodi report
	 */
	public List<PeriodoReportEnum> getComboPeriodi() {
		return comboPeriodi;
	}

	/**
	 * Imposta la lista dei periodo del report per il popolamento della combobox.
	 * @param comboPeriodi
	 */
	public void setComboPeriodi(final List<PeriodoReportEnum> comboPeriodi) {
		this.comboPeriodi = comboPeriodi;
	}

	/**
	 * Restituisce la lista delle tipologie delle operazione per il popolamento della combobox.
	 * @return lista tipi operazioni
	 */
	public List<TipoOperazioneReport> getComboTipiOperazione() {
		return comboTipiOperazione;
	}

	/**
	 * Imposta la lista delle tipologie delle operazione per il popolamento della combobox.
	 * @param comboTipiOperazione
	 */
	public void setComboTipiOperazione(final List<TipoOperazioneReport> comboTipiOperazione) {
		this.comboTipiOperazione = comboTipiOperazione;
	}

	/**
	 * Restituisce il codice del tipo operazione selezionato.
	 * @return codice tipo operazione.
	 */
	public String getCodiceTipoOperazioneSelected() {
		return codiceTipoOperazioneSelected;
	}

	/**
	 * Imposta il codice del tipo operazione selezionato.
	 * @param codiceTipoOperazioneSelected
	 */
	public void setCodiceTipoOperazioneSelected(final String codiceTipoOperazioneSelected) {
		this.codiceTipoOperazioneSelected = codiceTipoOperazioneSelected;
		
		tipoOperazioneSelected = null;
		
		if (comboTipiOperazione == null 
				|| comboTipiOperazione.isEmpty() 
				|| StringUtils.isNullOrEmpty(codiceTipoOperazioneSelected)) {
			return;
		}
		
		for (final TipoOperazioneReport t : comboTipiOperazione) {
			if (t.getCodice().equals(codiceTipoOperazioneSelected)) {
				tipoOperazioneSelected = t;
				break;
			}
		}
	}

	/**
	 * Restituisce il codice della coda di lavoro selezionata.
	 * @return codice coda di lavoro selezionata
	 */
	public String getCodiceCodaDiLavoroSelected() {
		return codiceCodaDiLavoroSelected;
	}

	/**
	 * Imposta il codice della coda di lavoro selezionata.
	 * @param codiceCodaDiLavoroSelected
	 */
	public void setCodiceCodaDiLavoroSelected(final String codiceCodaDiLavoroSelected) {
		this.codiceCodaDiLavoroSelected = codiceCodaDiLavoroSelected;
		
		codaDiLavoroSelected = null;
		
		if (codeDiLavoro == null 
				|| codeDiLavoro.isEmpty() 
				|| StringUtils.isNullOrEmpty(codiceCodaDiLavoroSelected)) {
			return;
		}
		
		for (final CodaDiLavoroReport t : codeDiLavoro) {
			if (t.getCodice().equals(codiceCodaDiLavoroSelected)) {
				codaDiLavoroSelected = t;
				break;
			}
		}
		
	}

	/**
	 * Restituisce la root dell'albero utenti.
	 * @return root utenti
	 */
	public TreeNode getUtenteRoot() {
		return utenteRoot;
	}

	/**
	 * Imposta la root degli utenti.
	 * @param utenteRoot
	 */
	public void setUtenteRoot(final TreeNode utenteRoot) {
		this.utenteRoot = utenteRoot;
	}

	/**
	 * Restituisce la root dell'albero degli uffici.
	 * @return root uffici
	 */
	public TreeNode getUfficioRoot() {
		return ufficioRoot;
	}

	/**
	 * Imposta la root dell'albero degli uffici.
	 * @param ufficioRoot
	 */
	public void setUfficioRoot(final TreeNode ufficioRoot) {
		this.ufficioRoot = ufficioRoot;
	}

	/**
	 * Restituisce il codice del dettaglio report selezionato.
	 * @return codice dettaglio selezionato
	 */
	public String getCodiceDettaglioSelected() {
		return codiceDettaglioSelected;
	}

	/**
	 * Imposta il codice del dettaglio selezionato.
	 * @param codiceDettaglioSelected
	 */
	public void setCodiceDettaglioSelected(final String codiceDettaglioSelected) {
		this.codiceDettaglioSelected = codiceDettaglioSelected;
		
		dettaglioSelected = null;
		
		if (comboDettaglioReport == null 
				|| comboDettaglioReport.isEmpty() 
				|| StringUtils.isNullOrEmpty(codiceDettaglioSelected)) {
			return;
		}
		
		for (final DettaglioReport t : comboDettaglioReport) {
			if (t.getCodice().equals(codiceDettaglioSelected)) {
				dettaglioSelected = t;
				break;
			}
		}
	}

	/**
	 * Restituisce la root selezionata.
	 * @return root selezionata
	 */
	public TreeNode getRootSelected() {
		return rootSelected;
	}

	/**
	 * Imposta la root selezionata.
	 * @param rootSelected
	 */
	public void setRootSelected(final TreeNode rootSelected) {
		this.rootSelected = rootSelected;
	}

	/**
	 * Restituisce la lista dei nodi dell'organigramma selezionati.
	 * @return lista dei nodi selezionati
	 */
	public List<NodoOrganigrammaDTO> getNodiSelected() {
		return nodiSelected;
	}

	/**
	 * Impsota la lista dei nodi selezionati.
	 * @param nodiSelected
	 */
	public void setNodiSelected(final List<NodoOrganigrammaDTO> nodiSelected) {
		this.nodiSelected = nodiSelected;
	}

	/**
	 * Restituisce la lista dei ruoli per il popolamento della combobox.
	 * @return lista ruoli
	 */
	public List<Ruolo> getComboRuoli() {
		return comboRuoli;
	}

	/**
	 * Imposta la lista dei ruoli per il popolamento della combobox.
	 * @param comboRuoli
	 */
	public void setComboRuoli(final List<Ruolo> comboRuoli) {
		this.comboRuoli = comboRuoli;
	}
	
}
