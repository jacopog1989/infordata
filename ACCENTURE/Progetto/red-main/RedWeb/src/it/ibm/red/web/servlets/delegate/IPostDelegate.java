package it.ibm.red.web.servlets.delegate;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.web.helper.lsign.LocalSignHelper.DocSignDTO;
import it.ibm.red.web.servlets.delegate.exception.DelegateException;

/**
 * 
 * @author CPIERASC
 *
 *         Interfaccia dei delegati per gestire le chiamate post dell'applet per
 *         restituire i contenuti firmati.
 */
public interface IPostDelegate extends IDelegate {

	/**
	 * Gestisce un documento appena firmato prendendo in ingresso la pk del
	 * documento originario e il content firmato.
	 * 
	 * @param signTransactionId
	 * @param pkDoc			 	la pk del documento firmato
	 * @param docSigned		 	la versione firmata del documento
	 * @param idUtente		 	identificativo dell'utente firmatario
	 * @param ste 				tipo di firma richiesta
	 * @param isLastDocument 	true se è l'ultimo documento associato al documento principale, false altrimenti
	 * @param isFirmaMultipla	true se si tratta di Firma Multipla (per l'apposizione di più firme su un documento)
	 * @param docSentToNPS		
	 * @return 
	 */
	EsitoOperazioneDTO manageSignedDoc(String signTransactionId, DocSignDTO doc, byte[] docSigned, UtenteDTO utente, SignTypeEnum ste, boolean isLastDocument, boolean isFirmaMultipla) throws DelegateException;

	/**
	 * L'applet ammette la firma di un solo documento, per consentire la firma
	 * multipla la response va impostata in maniera tale che venga eseguita una
	 * request alla servlet di post. In tale request devono essere riportati gli
	 * eventuali parametri per ridisegnare l'applet. Ad esempio:
	 * response.getOutputStream().println(request.getContextPath()+"/LocalSignServlet?delegate=DummyPostDelegate");
	 * response.getOutputStream().println("Action=SIGN");
	 * response.getOutputStream().println("DocumentURL="+request.getContextPath()+"/LocalSignServlet?delegate=DummyGetDelegate");
	 * response.getOutputStream().println("Message=Ultimo documento firmato " +
	 * lastDocSigned + ". Rimangono " + docsToSign.size()+ " documenti da
	 * firmare.");
	 * 
	 * @param request  richiesta gestita
	 * @param response risposta generata
	 * @param doc      chiave del documento
	 * @throws IOException
	 */
	void updateResponseForNextAppletIteration(HttpServletRequest request, HttpServletResponse response, DocSignDTO doc) throws IOException;

}
