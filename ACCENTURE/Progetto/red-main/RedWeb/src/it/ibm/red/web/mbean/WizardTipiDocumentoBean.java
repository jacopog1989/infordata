package it.ibm.red.web.mbean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;

import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.RegistroDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.dto.TipoDocumentoDTO;
import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.LookupTablePresentationModeEnum;
import it.ibm.red.business.enums.StringMatchModeEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.enums.TipoControlloEnum;
import it.ibm.red.business.enums.TipoDocumentoModeEnum;
import it.ibm.red.business.enums.TipoGestioneEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.ITipoProcedimentoSRV;
import it.ibm.red.business.service.facade.ILookupTableFacadeSRV;
import it.ibm.red.business.service.facade.IRegistroAusiliarioFacadeSRV;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;
import it.ibm.red.business.utils.CollectionUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Bean che gestisce il wizard dei tipi documento. Il wizard consente di creare
 * nuove tipologie documenti che possono essere utilizzate da EVO.
 * 
 * @author SimoneLungarella
 */
@Named(ConstantsWeb.MBean.WIZARD_TIPI_DOCUMENTO_BEAN)
@ViewScoped
public class WizardTipiDocumentoBean extends AbstractBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 9028902570955531663L;

	/**
	 * Nome del metadato numerico associato ad una registrazione ausiliaria. Non può
	 * essere usato come nome custom per un metadato, nel caso in cui un utente
	 * dovesse usare questo nome ne verrà impedito l'inserimento e verrà mostrato un
	 * warning.
	 */
	private static final String NUMERO_REGISTRO = "NUMERO_REGISTRO";

	/**
	 * Nome metadato associato al nome della registrazione ausiliaria. Non può
	 * essere usato come nome custom per un metadato, nel caso in cui un utente
	 * dovesse usare questo nome ne verrà impedito l'inserimento e verrà mostrato un
	 * warning.
	 */
	private static final String NOME_REGISTRO = "NOME_REGISTRO";

	/**
	 * Nome metadato associata alla data della registrazione ausiliaria. Non può
	 * essere usato come nome custom per un metadato, nel caso in cui un utente
	 * dovesse usare questo nome ne verrà impedito l'inserimento e verrà mostrato un
	 * warning.
	 */
	private static final String DATA_REGISTRO = "DATA_REGISTRO";

	/**
	 * Suffisso da mettere in append al nome del tipo procedimento quando
	 * l'intenzione dell'utente è quella di creare un nuovo tipo procedimento invece
	 * di usarne uno già esistente. Viene utilizzata nell' <em> autocomplete </em>
	 * del tipo procedimento.
	 */
	private static final String NOME_NUOVO_PROCEDIMENTO_SUFFIX = " - new";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(WizardTipiDocumentoBean.class.getName());

	/**
	 * Servizio.
	 */
	private ITipologiaDocumentoFacadeSRV tipologiaDocumentoSRV;
	
	/**
	 * Servizio recupero registri.
	 */
	private IRegistroAusiliarioFacadeSRV registroAusiliarioSRV;

	/**
	 * Servizio.
	 */
	private ILookupTableFacadeSRV lookupTableSRV;

	/**
	 * Tipo docuento.
	 */
	private TipoDocumentoDTO newTipoDocumento;

	/**
	 * Metadato.
	 */
	private MetadatoDTO newMetadato;

	/**
	 * Servizio.
	 */
	private ITipoProcedimentoSRV tipoProcedimentoSRV;

	/**
	 * Categoria solo entrata.
	 */
	private String tipoCategoria = "SOLO ENTRATA";

	/**
	 * Selettori lookup table.
	 */
	private Collection<SelectItemDTO> selettoriLookupTable;

	/**
	 * Selettore tipo dato.
	 */
	private Collection<SelectItemDTO> tipoDato;

	/**
	 * Selettore modalità tipo documento.
	 */
	private Collection<SelectItemDTO> tipoDocumentoMode;

	/**
	 * Selettore tipo documento light.
	 */
	private Collection<SelectItemDTO> tipoDocumentoModeLight;

	/**
	 * Selettore modalità presentazione lu.
	 */
	private Collection<SelectItemDTO> lookupTablePresentatioMode;

	/**
	 * Selettore modalità di match stringhe.
	 */
	private Collection<SelectItemDTO> matchStringMode;

	/**
	 * Selettore tipo gestione.
	 */
	private Collection<SelectItemDTO> tipoGestione;

	/**
	 * Selettore tipo controllo.
	 */
	private List<SelectItemDTO> tipoControllo;

	/**
	 * Bean di sessione.
	 */
	private SessionBean sessionBean;

	/**
	 * Informazioni utente.
	 */
	private UtenteDTO utente;

	/**
	 * Helper per la gestione dei metadati. Dal caricamento dal db alla
	 * renderizzazione in front-end. Datatable metadati.
	 */
	private SimpleDetailDataTableHelper<MetadatoDTO> metadatiDTH;

	/**
	 * Helper per la gestione dei documenti. Dal caricamento dal db alla
	 * renderizzazione in front-end. Datatable registri.
	 */
	private SimpleDetailDataTableHelper<RegistroDTO> registriDTH;

	/**
	 * Helper per la gestione dei procedimenti. Dal caricamento dal db alla
	 * renderizzazione in front-end. Datatable procedimenti.
	 */
	private SimpleDetailDataTableHelper<TipoProcedimentoDTO> procedimentiDTH;

	/**
	 * Lista porcedimenti.
	 */
	private Collection<TipoProcedimentoDTO> existingProcedimenti = new ArrayList<>();

	/**
	 * Nuovo procedimento.
	 */
	private TipoProcedimentoDTO newProcedimento;

	/**
	 * Old procedimento.
	 */
	private TipoProcedimentoDTO backupProcedimento;

	/**
	 * Lista descrizioni procedimenti.
	 */
	private List<String> descProcedimenti;

	/**
	 * Lista descrizioni procedimenti selezionati.
	 */
	private List<String> descSelectedProcedimenti;

	/**
	 * Flag editing mode procedimenti.
	 */
	private boolean procedimentoEditingMode = false;

	/**
	 * Flag visto.
	 */
	private boolean visto;

	/**
	 * Flag scadenza ingiorni.
	 */
	private boolean scadenzaInGiorni = true;

	/**
	 * Tempo di scadenza.
	 */
	private int tempoScadenza = 1;

	/**
	 * Post construct del bean.
	 * 
	 * @see it.ibm.red.web.mbean.AbstractBean#postConstruct().
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		sessionBean = FacesHelper.getManagedBean(MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		tipologiaDocumentoSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoFacadeSRV.class);
		registroAusiliarioSRV = ApplicationContextProvider.getApplicationContext().getBean(IRegistroAusiliarioFacadeSRV.class);
		lookupTableSRV = ApplicationContextProvider.getApplicationContext().getBean(ILookupTableFacadeSRV.class);
		tipoProcedimentoSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipoProcedimentoSRV.class);
		cleanTipoDocumento();
		loadSelettoriLookupTable();
		loadTipoDato();
		loadTipoDocumentoMode();
		loadLookupTablePresentationMode();
		loadMatchStringMode();
		loadTipoGestione();
		loadTipoControllo();
		registriDTH = new SimpleDetailDataTableHelper<>(registroAusiliarioSRV.getRegistri());
		descSelectedProcedimenti = new ArrayList<>();
		cleanTipoProcedimento();
		existingProcedimenti = tipoProcedimentoSRV.getTipiProcedimentoByIdAooFiltered(utente.getIdAoo(), TipoCategoriaEnum.ENTRATA);
		loadProcedimentiToShow();
	}

	private void loadTipoDato() {
		tipoDato = new ArrayList<>();
		for (final TipoMetadatoEnum tipo : TipoMetadatoEnum.values()) {
			if (Boolean.TRUE.equals(tipo.getWizard())) {
				tipoDato.add(new SelectItemDTO(tipo, tipo.getDescription()));
			}
		}
	}

	private void loadTipoDocumentoMode() {
		tipoDocumentoMode = new ArrayList<>();
		tipoDocumentoModeLight = new ArrayList<>();
		for (final TipoDocumentoModeEnum tipo : TipoDocumentoModeEnum.values()) {
			if (TipoDocumentoModeEnum.SEMPRE.equals(tipo)) {
				final SelectItemDTO si = new SelectItemDTO(tipo, "ENTRAMBI");
				tipoDocumentoMode.add(si);
				tipoDocumentoModeLight.add(si);
			} else {
				final SelectItemDTO si = new SelectItemDTO(tipo, tipo.getDescription());
				tipoDocumentoMode.add(si);
				if (!TipoDocumentoModeEnum.MAI.equals(tipo)) {
					tipoDocumentoModeLight.add(si);
				}
			}

		}
	}

	private void loadMatchStringMode() {
		matchStringMode = new ArrayList<>();
		for (final StringMatchModeEnum tipo : StringMatchModeEnum.values()) {
			matchStringMode.add(new SelectItemDTO(tipo, tipo.getDescription()));
		}
	}

	private void loadTipoGestione() {
		tipoGestione = new ArrayList<>();
		for (final TipoGestioneEnum tipo : TipoGestioneEnum.values()) {
			tipoGestione.add(new SelectItemDTO(tipo, tipo.getDescription()));
		}
	}

	private void loadLookupTablePresentationMode() {
		lookupTablePresentatioMode = new ArrayList<>();
		for (final LookupTablePresentationModeEnum tipo : LookupTablePresentationModeEnum.values()) {
			lookupTablePresentatioMode.add(new SelectItemDTO(tipo, tipo.getDescription()));
		}
	}

	/**
	 * Restituisce la lista delle modalita di presentazione delle lookup table. La
	 * modalita di presentazione può essere DATATABLE o COMBOBOX.
	 * 
	 * @return lista modalita di presentazione lookup table
	 */
	public Collection<SelectItemDTO> getLookupTablePresentatioMode() {
		return lookupTablePresentatioMode;
	}

	private void loadSelettoriLookupTable() {
		selettoriLookupTable = new ArrayList<>();

		try {

			final Collection<String> selectors = lookupTableSRV.getSelectors(utente.getIdAoo());
			for (final String o : selectors) {
				selettoriLookupTable.add(new SelectItemDTO(o, o));
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante la valorizzazione dei selettori per le lookup table", e);
			showError(e);
		}
	}

	/**
	 * Gestisce l'autocompletamento del nome del tipo procedimento.
	 * 
	 * @param query
	 * @return lista dei tipi procedimenti suggeriti dalla funzionalita di
	 *         autocompletamento.
	 */
	public List<String> completeText(final String query) {
		final List<String> results = new ArrayList<>();
		// Se non esiste una tipologia procedimento con quel nome ne viene mostrata una
		// col nome inserito
		// dall'utente poiché occorre che si verifichi un SelectEvent
		if (!descProcedimenti.contains(query)) {
			results.add(query.toUpperCase() + NOME_NUOVO_PROCEDIMENTO_SUFFIX);
		}

		for (final String s : descProcedimenti) {
			if (s.toLowerCase().startsWith(query.toLowerCase())) {
				results.add(s);
			}
		}

		return results;
	}

	/**
	 * Costruzione della collection per gestire il suggerimento all'auto-complete
	 * per la scelta del procedimenti.
	 */
	public void loadProcedimentiToShow() {
		descProcedimenti = new ArrayList<>();
		for (final TipoProcedimentoDTO tipo : existingProcedimenti) {
			descProcedimenti.add(tipo.getDescrizione());
		}
	}

	/**
	 * Valorizza {@link #tipoControllo} con la lista dei tipi di controllo per il
	 * popolamento della combobox.
	 */
	public void loadTipoControllo() {
		tipoControllo = new ArrayList<>();
		for (final TipoControlloEnum c : TipoControlloEnum.values()) {
			tipoControllo.add(new SelectItemDTO(c, c.getDescription()));
		}
	}

	/**
	 * Gestisce l'aggiunta al datatable del tipo procedimento selezionato.
	 * 
	 * @param event
	 */
	public void onProcedimentoSelected(final SelectEvent event) {
		String s = (String) event.getObject();
		if (s.endsWith(NOME_NUOVO_PROCEDIMENTO_SUFFIX)) {
			s = s.replace(NOME_NUOVO_PROCEDIMENTO_SUFFIX, "");
		}
		if (!descSelectedProcedimenti.contains(s)) {
			initProcedimento(s);
			final TipoProcedimentoDTO p = new TipoProcedimentoDTO(newProcedimento);
			newProcedimento = new TipoProcedimentoDTO();
			procedimentiDTH.getMasters().add(p);
			refreshDescSelectedProcedimenti();
			showInfoMessage("Procedimento aggiunto con successo");
		} else {
			showWarnMessage("Il procedimento è già stato inserito");
		}
	}

	/**
	 * Inizializza i parametri associati al {@link #newProcedimento} a partire dalla
	 * descrizione del procedimento.
	 * 
	 * @param descrizioneProcedimento
	 */
	public void initProcedimento(final String descrizioneProcedimento) {
		newProcedimento = new TipoProcedimentoDTO(descrizioneProcedimento);
		// Setting parametri di default del procedimento
		newProcedimento.setMinutiScadenzaSospeso(1);
		newProcedimento.setTipoControllo(TipoControlloEnum.SUCCESSIVO);
		newProcedimento.setTipoGestione(TipoGestioneEnum.IBRIDA);
		newProcedimento.setIdAoo(utente.getIdAoo().intValue());
		newProcedimento.setFlagContabile(false);
		// Setting altre variabili
		setTempoScadenza(newProcedimento.getMinutiScadenzaSospeso());
		setVisto(false);
	}

	/**
	 * Aggiunge il procedimento alla tipologia documento.
	 */
	public void addProcedimento() {
		boolean isFounded = false;
		for (final TipoProcedimentoDTO proc : procedimentiDTH.getMasters()) {
			if (proc.getDescrizione().equalsIgnoreCase(newProcedimento.getDescrizione())) {
				isFounded = true;
				break;
			}
		}

		if (!isFounded) {
			setTempoScadenzaProcedimento();
			if (newProcedimento.getMinutiScadenzaSospeso() <= 0) {
				showWarnMessage("I minuti di scadenza devono essere più di 0");
			} else {
				newProcedimento.setRegistri(registriDTH.getSelectedMasters());
				newProcedimento.setMetadati(metadatiDTH.getMasters());
				final TipoProcedimentoDTO p = new TipoProcedimentoDTO(newProcedimento);
				newProcedimento = new TipoProcedimentoDTO();
				procedimentiDTH.getMasters().add(p);
				refreshDescSelectedProcedimenti();
				FacesHelper.executeJS("PF('editingProcedimento').hide()");
				showInfoMessage("Procedimento modificato con successo");
			}

		}
	}

	private void removeFromDatatableProcedimento(final TipoProcedimentoDTO tipo) {
		final Collection<TipoProcedimentoDTO> tmp = new ArrayList<>();
		for (final TipoProcedimentoDTO procedimento : procedimentiDTH.getMasters()) {
			if (!tipo.getDescrizione().equalsIgnoreCase(procedimento.getDescrizione())) {
				tmp.add(procedimento);
			}
		}
		procedimentiDTH.setMasters(tmp);
	}

	/**
	 * Gestisce la modifica del tipo procedimento in fase di creazione.
	 * 
	 * @param event
	 */
	public void editProcedimento(final ActionEvent event) {
		final TipoProcedimentoDTO p = getDataTableClickedRow(event);
		removeFromDatatableProcedimento(p);
		newProcedimento = p;

		// Se il procedimento contiene il metadato con nome NOME_REGISTRO vuol dire che
		// il flag VISTO deve essere abilitato, quindi lo si pone a true per mostrarlo
		// correttamente in maschera
		if (!CollectionUtils.isEmptyOrNull(newProcedimento.getMetadati())) {
			for (final MetadatoDTO m : newProcedimento.getMetadati()) {
				if (m.getName().equals(NOME_REGISTRO)) {
					setVisto(true);
					break;
				}

				setVisto(false);
			}
		} else {
			setVisto(false);
		}

		setTempoScadenza(newProcedimento.getMinutiScadenzaSospeso());
		procedimentiDTH.getMasters().remove(newProcedimento);
		refreshDescSelectedProcedimenti();

		backupProcedimento = new TipoProcedimentoDTO(newProcedimento);

		if (newProcedimento.getMetadati() != null) {
			metadatiDTH.setMasters(newProcedimento.getMetadati());
		}
		else {
			metadatiDTH.setMasters(new ArrayList<>());
		}

		registriDTH.setSelectedMasters((List<RegistroDTO>) newProcedimento.getRegistri());
		metadatiDTH.setSelectedMasters((List<MetadatoDTO>) newProcedimento.getMetadati());
		FacesHelper.executeJS("PF('editingProcedimento').show()");

	}

	/**
	 * Gestisce la rimozione del tipo procedimento selezionato dalla lista dei tipi
	 * procedimento associati alla tipologia documento in fase di creazione.
	 * 
	 * @param event
	 */
	public void removeProcedimento(final ActionEvent event) {
		final TipoProcedimentoDTO toRemove = getDataTableClickedRow(event);
		removeFromDatatableProcedimento(toRemove);
		procedimentiDTH.getMasters().remove(toRemove);
		refreshDescSelectedProcedimenti();
		showInfoMessage("Procedimento rimosso con successo.");
	}

	/**
	 * Gestisce l'annullamento delle modifiche al tipo procedimento.
	 */
	public void rollbackModifiche() {
		procedimentiDTH.getMasters().add(backupProcedimento);
		refreshDescSelectedProcedimenti();
		showInfoMessage("Modifiche annullate.");
	}

	/**
	 * Gestione front-end da eseguire a valle della rimozione del procedimento.
	 */
	public void rimuoviProcedimento() {
		showInfoMessage("Procedimento rimosso con successo.");
	}

	/**
	 * Consente di inizializzare {@link #existingProcedimenti} e aggiorna le
	 * descrizioni da suggerire in fase di autocompletamento.
	 */
	public void loadTipiProcedimenti() {
		cleanTipoProcedimento();
		existingProcedimenti = new ArrayList<>(tipoProcedimentoSRV.getTipiProcedimentoByIdAooFiltered(utente.getIdAoo(), getTipoCategoriaCode()));
		loadProcedimentiToShow();
	}

	/**
	 * Effettua un refresh delle descrizioni dei procedimenti selezionati.
	 */
	public void refreshDescSelectedProcedimenti() {
		descSelectedProcedimenti = new ArrayList<>();
		for (final TipoProcedimentoDTO tipo : procedimentiDTH.getMasters()) {
			descSelectedProcedimenti.add(tipo.getDescrizione());
		}
	}

	/**
	 * Effettua un reset di tutti i campi dei form per la creazione del tipo
	 * documento.
	 */
	public void resetTipoDocumento() {
		cleanTipoDocumento();
		showInfoMessage("Tipologia documento annullata");
	}

	/**
	 * Effettua un reset di tutti i campi dei form per la creazione del tipo
	 * procedimento non resettando i campi che sono associati strettamente al tipo
	 * documento e non alla coppia.
	 */
	public void resetTipoProcedimento() {
		cleanTipoProcedimento();
		showInfoMessage("Tipologia procedimento annullata");
	}

	/**
	 * Effettua un reset di tutti i campi dei form per la creazione del tipo
	 * documento.
	 */
	public void cleanTipoDocumento() {
		metadatiDTH = new SimpleDetailDataTableHelper<>(new ArrayList<>());
		newTipoDocumento = new TipoDocumentoDTO();
		cleanTipoProcedimento();
		cleanMetadato();
	}

	/**
	 * Effettua un reset di tutti i campi dei form per la creazione del metadato.
	 */
	public void cleanMetadato() {
		newMetadato = new MetadatoDTO();
	}

	/**
	 * Gestisce l'aggiunta del metadato creato alla lista dei metadati afferenti
	 * alla coppia Tipo documento - Tipo procedimento.
	 */
	public void addMetadato() {
		boolean bFound = false;
		for (final MetadatoDTO m : metadatiDTH.getMasters()) {
			if (m.getName().equalsIgnoreCase(newMetadato.getName())) {
				bFound = true;
				break;
			}
		}
		if (!bFound) {
			if ((StringUtils.isNullOrEmpty(newMetadato.getName()) || newMetadato.getType() == null)) {
				showWarnMessage("Metadato non aggiunto [nome metadato o tipo non definito].");
			} else if (newMetadato.getName().contains(" ")) {
				showWarnMessage("Il nome del metadato non può contenere caratteri vuoti");
			} else if (newMetadato.getName().equals(DATA_REGISTRO)
					|| newMetadato.getName().equals(NOME_REGISTRO) || newMetadato.getName().equals(NUMERO_REGISTRO)) {
				showWarnMessage("Il nome del metadato che stai usando non è utilizzabile");
			} else {
				if ((TipoMetadatoEnum.STRING.equals(newMetadato.getType()) || TipoMetadatoEnum.TEXT_AREA.equals(newMetadato.getType()))
						&& newMetadato.getDimension() == null) {
					showWarnMessage("Per un metadato di tipo String è obbligatorio specificare la dimensione");
				} else if (TipoMetadatoEnum.LOOKUP_TABLE.equals(newMetadato.getType()) && StringUtils.isNullOrEmpty(newMetadato.getLookupTableSelector())) {
					showWarnMessage("Per un metadato del tipo: " + newMetadato.getType().getDescription() + " è obbligatorio scegliere un selettore");
				} else {
					if (!TipoMetadatoEnum.STRING.equals(newMetadato.getType())) {
						newMetadato.setDimension(null);
						newMetadato.setStringMatchMode(null);
					}
					if (!TipoMetadatoEnum.LOOKUP_TABLE.equals(newMetadato.getType())) {
						newMetadato.setLookupTableMode(null);
						newMetadato.setLookupTableSelector(null);
					}
					if (!TipoMetadatoEnum.DATE.equals(newMetadato.getType()) && !TipoMetadatoEnum.DOUBLE.equals(newMetadato.getType())
							&& !TipoMetadatoEnum.INTEGER.equals(newMetadato.getType())) {
						newMetadato.setFlagRange(null);
					}

					final MetadatoDTO m = new MetadatoDTO(newMetadato);
					newMetadato = new MetadatoDTO();
					metadatiDTH.getMasters().add(m);
					FacesHelper.executeJS("PF('wdgMetadatoDialog').hide()");
					showInfoMessage("Metadato aggiunto con successo.");
				}
			}
		} else {
			showWarnMessage("Metadato non aggiunto [nome metadato già utilizzato].");
		}
	}

	/**
	 * Metodo utilizzato per la creazione e l'aggiunta dei metadati standard per una
	 * coppia documento/procedimento di tipo Visto.
	 */
	public void addMetadatiForVisto(final AjaxBehaviorEvent ev) {
		if (isVisto()) {
			final MetadatoDTO dataRegistro = new MetadatoDTO();
			dataRegistro.setName(DATA_REGISTRO);
			dataRegistro.setDisplayName("Data Registro");
			dataRegistro.setType(TipoMetadatoEnum.DATE);
			dataRegistro.setVisibility(TipoDocumentoModeEnum.SOLO_USCITA);
			metadatiDTH.getMasters().add(dataRegistro);

			final MetadatoDTO nomeRegistro = new MetadatoDTO();
			nomeRegistro.setName(NOME_REGISTRO);
			nomeRegistro.setDisplayName("Nome Registro");
			nomeRegistro.setType(TipoMetadatoEnum.STRING);
			nomeRegistro.setDimension(100);
			nomeRegistro.setVisibility(TipoDocumentoModeEnum.SOLO_USCITA);
			metadatiDTH.getMasters().add(nomeRegistro);

			final MetadatoDTO numeroRegistro = new MetadatoDTO();
			numeroRegistro.setName(NUMERO_REGISTRO);
			numeroRegistro.setDisplayName("Numero Registro");
			numeroRegistro.setType(TipoMetadatoEnum.INTEGER);
			numeroRegistro.setVisibility(TipoDocumentoModeEnum.SOLO_USCITA);
			metadatiDTH.getMasters().add(numeroRegistro);

			showInfoMessage("Metadati obbligatori per documenti del tipo \"VISTO\" aggiunti");
		} else {
			boolean wasPopulated = false;
			final Collection<MetadatoDTO> temp = new ArrayList<>();
			for (final MetadatoDTO m : metadatiDTH.getMasters()) {
				if (!m.getName().equals(DATA_REGISTRO) && !m.getName().equals(NOME_REGISTRO) && !m.getName().equals(NUMERO_REGISTRO)) {
					temp.add(m);
				} else {
					wasPopulated = true;
				}
			}
			if (wasPopulated) {
				metadatiDTH.setMasters(temp);
				showInfoMessage("Metadati obbligatori per documenti del tipo \"VISTO\" rimossi");
			}

		}
	}

	/**
	 * Gestisce la rimozione del metadato dalla coppia Tipo Documento - Tipo
	 * Procedimento.
	 * 
	 * @param event
	 */
	public void removeMetadato(final ActionEvent event) {
		final MetadatoDTO m = getDataTableClickedRow(event);
		if (m != null) {
			removeFromDatatableMetadati(m);
		}
		showInfoMessage("Metadato rimosso con successo.");
	}

	private void removeFromDatatableMetadati(final MetadatoDTO m) {
		final Collection<MetadatoDTO> tmp = new ArrayList<>();
		for (final MetadatoDTO metadato : metadatiDTH.getMasters()) {
			if (!m.getName().equalsIgnoreCase(metadato.getName())) {
				tmp.add(metadato);
			}
		}
		metadatiDTH.setMasters(tmp);
	}

	/**
	 * Gestisce la modifica del metadato.
	 * 
	 * @param event
	 */
	public void editMetadato(final ActionEvent event) {
		final MetadatoDTO m = getDataTableClickedRow(event);
		removeFromDatatableMetadati(m);
		newMetadato = m;
		FacesHelper.executeJS("PF('wdgMetadatoDialog').show()");
	}

	/**
	 * Rende persisteni le caratteristiche della tipologia documento creata
	 * memorizzando le informazioni su di esso sui tipi procedimento associati e sui
	 * metadati e i registri ausiliaria associati alla coppia Tipologia Documento -
	 * Tipologia Procedimento.
	 */
	public void saveTipoDocumento() {
		if (StringUtils.isNullOrEmpty(newTipoDocumento.getName())) {
			showWarnMessage("Non è possibile creare una tipologia senza nome");
		} else if (procedimentiDTH.getMasters().isEmpty()) {
			showWarnMessage("Non è possibile creare una tipologia di documento senza alcun procedimento associato");
		} else if (nameAlreadyPresent()) {
			showWarnMessage("Una tipologia con il nome: " + newTipoDocumento.getName() + " è già esistente");
		} else {
			tipologiaDocumentoSRV.save(newTipoDocumento.getName(), newTipoDocumento.getTipoGestione().getCode(), utente.getIdAoo(), getTipoCategoriaCode(),
					procedimentiDTH.getMasters());

			final String nomeTipologia = newTipoDocumento.getName();
			cleanTipoDocumento();

			// Ricaricamento della combobox delle tipologie documento
			sessionBean.getRicercaUCB().loadFormRicercaUCB();
			FacesHelper.update("idTabRicerche:idTabRicercaUCB:ricercaAvanzataUCBForm:idComboTipoDocUcb");

			showInfoMessage("Tipologia documento salvata con successo");
			LOGGER.info("Creazione nuova tipologia documento: [" + nomeTipologia + "] eseguita");
		}

	}

	/**
	 * Metodo utilizzato per la trasformazione del tipo categoria da enum a intero
	 * per la memorizzazione in DB.
	 * 
	 * @return tipo categoria del documento
	 */
	private TipoCategoriaEnum getTipoCategoriaCode() {
		if ("SOLO ENTRATA".equals(getTipoCategoria())) {
			return TipoCategoriaEnum.ENTRATA;
		} else if ("SOLO USCITA".equals(getTipoCategoria())) {
			return TipoCategoriaEnum.USCITA;
		} else {
			// ENTRAMBI
			return TipoCategoriaEnum.ENTRATA_ED_USCITA;
		}
	}

	/**
	 * Imposta il tempo di scadenza del procedimento.
	 */
	public void setTempoScadenzaProcedimento() {
		if (scadenzaInGiorni) {
			// Conversione giorni in minuti
			getNewProcedimento().setMinutiScadenzaSospeso(tempoScadenza * 1440);
		} else {
			getNewProcedimento().setMinutiScadenzaSospeso(tempoScadenza);
		}
	}

	private boolean nameAlreadyPresent() {

		List<TipologiaDocumentoDTO> existingTipologieDocumenti;
		if (TipoCategoriaEnum.ENTRATA_ED_USCITA.equals(getTipoCategoriaCode())) {
			final List<TipologiaDocumentoDTO> docsEntrata = 
					tipologiaDocumentoSRV.getComboTipologieByAooAndTipoCategoriaAttivi(TipoCategoriaEnum.ENTRATA, utente.getIdAoo());
			final List<TipologiaDocumentoDTO> docsUscita = 
					tipologiaDocumentoSRV.getComboTipologieByAooAndTipoCategoriaAttivi(TipoCategoriaEnum.USCITA, utente.getIdAoo());
			existingTipologieDocumenti = new ArrayList<>(docsEntrata);
			existingTipologieDocumenti.addAll(docsUscita);

		} else {
			existingTipologieDocumenti = tipologiaDocumentoSRV.getComboTipologieByAooAndTipoCategoriaAttivi(getTipoCategoriaCode(), utente.getIdAoo());
		}

		for (final TipologiaDocumentoDTO tipologia : existingTipologieDocumenti) {
			if (tipologia.getDescrizione().equals(newTipoDocumento.getName())) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Restituisce l'entita del tipo documento per la memorizzazione dei parametri.
	 * 
	 * @return tipo documento in fase di creazione
	 */
	public TipoDocumentoDTO getNewTipoDocumento() {
		return newTipoDocumento;
	}

	/**
	 * Imposta l'entita del tipo documento per la memorizzazione dei parametri.
	 * 
	 * @param newTipoDocumento
	 */
	public void setNewTipoDocumento(final TipoDocumentoDTO newTipoDocumento) {
		this.newTipoDocumento = newTipoDocumento;
	}

	/**
	 * Restituisce l'helper per il datatable che gestisce i metadati.
	 * 
	 * @return helper datatable metadati
	 */
	public SimpleDetailDataTableHelper<MetadatoDTO> getMetadatiDTH() {
		return metadatiDTH;
	}

	/**
	 * Restituisce l'entita del metadato per la memorizzazione dei parametri.
	 * 
	 * @return metadato in fase di creazione
	 */
	public MetadatoDTO getNewMetadato() {
		return newMetadato;
	}

	/**
	 * Imposta l'entita del metadato per la memorizzazione dei parametri.
	 * 
	 * @param newMetadato
	 */
	public void setNewMetadato(final MetadatoDTO newMetadato) {
		this.newMetadato = newMetadato;
	}

	/**
	 * Restituisce il tipo categoria del documento.
	 * 
	 * @return tipo categoria
	 */
	public String getTipoCategoria() {
		return tipoCategoria;
	}

	/**
	 * Imposta il tipo categoria del documento.
	 * 
	 * @param tipoCategoria
	 */
	public void setTipoCategoria(final String tipoCategoria) {
		this.tipoCategoria = tipoCategoria;
	}

	/**
	 * Restituisce la lista dei selettori lookup table.
	 * 
	 * @return selettori lookup table
	 */
	public Collection<SelectItemDTO> getSelettoriLookupTable() {
		return selettoriLookupTable;
	}

	/**
	 * Restituisce il tipo del metadato.
	 * 
	 * @return tipo metadato
	 */
	public Collection<SelectItemDTO> getTipoDato() {
		return tipoDato;
	}

	/**
	 * Restituisce la modalita del tipo documento.
	 * 
	 * @return modalita tipo documento
	 */
	public Collection<SelectItemDTO> getTipoDocumentoMode() {
		return tipoDocumentoMode;
	}

	/**
	 * Restituisce il tipo acquisizione del documento.
	 * 
	 * @return tipo acquisizione documento
	 */
	public Collection<SelectItemDTO> getTipoDocumentoModeLight() {
		return tipoDocumentoModeLight;
	}

	/**
	 * Imposta il tipo acquisizione del documento.
	 * 
	 * @param tipoDocumentoModeLight
	 */
	public void setTipoDocumentoModeLight(final Collection<SelectItemDTO> tipoDocumentoModeLight) {
		this.tipoDocumentoModeLight = tipoDocumentoModeLight;
	}

	/**
	 * Restituisce la modalita di ricerca per una stringa del metadato.
	 * 
	 * @return modalita ricerca String
	 */
	public Collection<SelectItemDTO> getMatchStringMode() {
		return matchStringMode;
	}

	/**
	 * Imposta la modalita di ricerca per una stringa del metadato.
	 * 
	 * @param matchStringMode
	 */
	public void setMatchStringMode(final Collection<SelectItemDTO> matchStringMode) {
		this.matchStringMode = matchStringMode;
	}

	/**
	 * Restituisce l'helper che gestisce il datatable dei registri.
	 * 
	 * @return helper datatable registri
	 */
	public SimpleDetailDataTableHelper<RegistroDTO> getRegistriDTH() {
		return registriDTH;
	}

	/**
	 * Restituisce l'helper che gestisce il datatalbe dei procedimenti.
	 * 
	 * @return helper datatable procedimenti
	 */
	public SimpleDetailDataTableHelper<TipoProcedimentoDTO> getProcedimentiDTH() {
		return procedimentiDTH;
	}

	/**
	 * Restituisce il tipo di gestione del documento.
	 * 
	 * @return tipo gestione documento
	 */
	public Collection<SelectItemDTO> getTipoGestione() {
		return tipoGestione;
	}

	/**
	 * Restituisce la lista delle descrizioni dei procedimenti.
	 * 
	 * @return lista descrizioni procedimenti
	 */
	public List<String> getDescProcedimenti() {
		return descProcedimenti;
	}

	/**
	 * Imposta la lista delle descrizioni dei procedimenti.
	 * 
	 * @param descProcedimenti
	 */
	public void setDescProcedimenti(final List<String> descProcedimenti) {
		this.descProcedimenti = descProcedimenti;
	}

	/**
	 * Restituisce il tipo procedimento in fase di creazione.
	 * 
	 * @return tipo procedimento
	 */
	public TipoProcedimentoDTO getNewProcedimento() {
		return newProcedimento;
	}

	/**
	 * Imposta il tipo procedimento in fase di creazione.
	 * 
	 * @param newProcedimento
	 */
	public void setNewProcedimento(final TipoProcedimentoDTO newProcedimento) {
		this.newProcedimento = newProcedimento;
	}

	/**
	 * Effettua un reset dei campi del tipo procedimento.
	 */
	public void cleanTipoProcedimento() {
		procedimentiDTH = new SimpleDetailDataTableHelper<>(new ArrayList<>());
		refreshDescSelectedProcedimenti();
		setNewProcedimento(new TipoProcedimentoDTO());
	}

	/**
	 * Restituisce true se viene visualizzato il form del tipo procedimento per la
	 * modifica, false altrimenti.
	 * 
	 * @return true se viene visualizzato il procedimento per la modifica, false
	 *         altrimenti
	 */
	public boolean isProcedimentoEditingMode() {
		return procedimentoEditingMode;
	}

	/**
	 * Imposta il flag associato alla modifica del procedimento.
	 * 
	 * @param procedimentoEditingMode
	 */
	public void setProcedimentoEditingMode(final boolean procedimentoEditingMode) {
		this.procedimentoEditingMode = procedimentoEditingMode;
	}

	/**
	 * Restituisce la lista delle descrizioni dei procedimenti selezionati.
	 * 
	 * @return descrizioni procedimenti selezionati
	 */
	public List<String> getDescSelectedProcedimenti() {
		return descSelectedProcedimenti;
	}

	/**
	 * Imposta la lista delle descrizioni dei procedimenti selezionati.
	 * 
	 * @param descSelectedProcedimenti
	 */
	public void setDescSelectedProcedimenti(final List<String> descSelectedProcedimenti) {
		this.descSelectedProcedimenti = descSelectedProcedimenti;
	}

	/**
	 * Restituisce il tipo procedimento di backup per il rollback in fase di
	 * annullamento modifiche.
	 * 
	 * @return tipo procedimento di backup
	 */
	public TipoProcedimentoDTO getBackupProcedimento() {
		return backupProcedimento;
	}

	/**
	 * Imposta il tipo procedimento di backup per il rollback in fase di
	 * annullamento modifiche.
	 * 
	 * @param backupProcedimento
	 */
	public void setBackupProcedimento(final TipoProcedimentoDTO backupProcedimento) {
		this.backupProcedimento = backupProcedimento;
	}

	/**
	 * Restituisce true se il tipo procedimento è associato a registri di tipo:
	 * Visto, false altrimenti.
	 * 
	 * @return true se il tipo procedimento è associato a registri di tipo: Visto,
	 *         false altrimenti
	 */
	public boolean isVisto() {
		return visto;
	}

	/**
	 * Imposta il flag associato al tipo dei registri associati al tipo
	 * procedimento.
	 * 
	 * @param visto
	 */
	public void setVisto(final boolean visto) {
		this.visto = visto;
	}

	/**
	 * Restituisce la lista dei tipi di controllo.
	 * 
	 * @return lista tipi di controllo
	 */
	public List<SelectItemDTO> getTipoControllo() {
		return tipoControllo;
	}

	/**
	 * Imposta la lista dei tipi di controllo.
	 * 
	 * @param tipoControllo
	 */
	public void setTipoControllo(final List<SelectItemDTO> tipoControllo) {
		this.tipoControllo = tipoControllo;
	}

	/**
	 * Restituisce true se il tempo di scadenza è espresso in giorni, false se
	 * espresso in minuti.
	 * 
	 * @return true se il tempo di scadenza è espresso in giorni, false se espresso
	 *         in minuti
	 */
	public boolean isScadenzaInGiorni() {
		return scadenzaInGiorni;
	}

	/**
	 * Imposta il flag associato alla tipologia di espressione del periodo di
	 * scadenza.
	 * 
	 * @param scadenzaInGiorni
	 */
	public void setScadenzaInGiorni(final boolean scadenzaInGiorni) {
		this.scadenzaInGiorni = scadenzaInGiorni;
	}

	/**
	 * Restituisce il tempo di scadenza come valore numerico che può essere
	 * rappresentativo di giorni o minuti.
	 * 
	 * @return tempo di scadenza come valore numerico
	 */
	public int getTempoScadenza() {
		return tempoScadenza;
	}

	/**
	 * Imposta il tempo di scadenza come valore numerico che può essere
	 * rappresentativo di giorni o minuti.
	 * 
	 * @param tempoScadenza
	 */
	public void setTempoScadenza(final int tempoScadenza) {
		this.tempoScadenza = tempoScadenza;
	}

}