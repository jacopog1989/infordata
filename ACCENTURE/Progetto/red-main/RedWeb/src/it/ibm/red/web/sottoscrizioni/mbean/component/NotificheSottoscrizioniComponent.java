package it.ibm.red.web.sottoscrizioni.mbean.component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NotificaDTO;
import it.ibm.red.business.dto.RecuperoCodeDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.INotificaFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.dto.GoToCodaDTO;
import it.ibm.red.web.enums.NavigationTokenEnum;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;

/**
 * Component notifiche sottoscrizioni.
 */
public class NotificheSottoscrizioniComponent extends AbstractBean {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = -6571876769206895042L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotificheSottoscrizioniComponent.class.getName());

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Servizio.
	 */
	private INotificaFacadeSRV notificaSRV;

	/**
	 * Servizio.
	 */
	private IRicercaFacadeSRV ricercaSRV;

	/**
	 * Componente dettaglio documento.
	 */
	private final DettaglioDocumentoSottoscrizioniComponent documentDetailComponent;

	/**
	 * Lista notifiche.
	 */
	private List<NotificaDTO> notificheDTO; 

	/**
	 * Datatable notifiche.
	 */
	private SimpleDetailDataTableHelper<NotificaDTO> notificheDTH;

	/**
	 * Numero giorni.
	 */
	private Integer aPartireDaNgiorni;

	/**
	 * Partenza.
	 */
	private Map<String, Integer> aPartireDa;

	/**
	 * Notifca.
	 */
	private NotificaDTO current;

	/**
	 * Contatori code.
	 */
	private EnumMap<DocumentQueueEnum, Integer> codeCount;

	/**
	 * Code non censite.
	 */
	private String listaCodeNonCensite;

	/**
	 * Notifica.
	 */
	private NotificaDTO notificaCode;

	/**
	 * Popup documento visibile.
	 */
	private boolean popupDocumentVisible;

	/**
	 * Costruttore di default.
	 * @param utente
	 * @param idNotifica
	 */
	public NotificheSottoscrizioniComponent(final UtenteDTO utente,final Integer idNotifica) {
		this.utente = utente;
		if (idNotifica==null) {
			this.aPartireDaNgiorni = 10;
		} else {
			this.aPartireDaNgiorni = null;
		}
		this.documentDetailComponent = new DettaglioDocumentoSottoscrizioniComponent(utente);
	}

	/**
	 * Inizializza il component.
	 * @param idNotifica
	 */
	public void initialize(final Integer idNotifica) {
		notificaSRV = ApplicationContextProvider.getApplicationContext().getBean(INotificaFacadeSRV.class);
		ricercaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaFacadeSRV.class);
		popolaAPartireDaMenu();
		notificheDTO = updateNotificheTbl(idNotifica);
		notificheDTH = new SimpleDetailDataTableHelper<>(notificheDTO);
		if (CollectionUtils.isNotEmpty(notificheDTO)) {
			notificheDTH.selectMaster(notificheDTO.get(0));
			current = notificheDTO.get(0);
		}
		documentDetailComponent.setUpdateTabDettaglio("centralSectionForm:idTab_STSCZ:tabDetails-notifiche");
		documentDetailComponent.getStorico().setIdSuffix("notifiche_sottoscrizioni");
		refreshDetail();
	}

	private void popolaAPartireDaMenu() {
		aPartireDa = new HashMap<>();
		aPartireDa.put("Sempre", null);
		aPartireDa.put("10 giorni prima", 10);
		aPartireDa.put("20 giorni prima", 20);
		aPartireDa.put("30 giorni prima", 30);
	}

	/**
	 * Contrassegna l'elemento del datatable selezionato.
	 * @param event
	 */
	public void contrassegnaSingolo(final ActionEvent event) {
		final NotificaDTO element = getDataTableClickedRow(event);
		contrassegnaNotifica(element, notificheDTH);
	}
	
	/**
	 * Esegue l'update del datatable delle notifiche.
	 * {@link #updateNotificheTbl(Integer)}
	 */
	public void update() {
		notificheDTH.setMasters(updateNotificheTbl(null));
	}
	
	@Override
	protected List<NotificaDTO> updateNotificheTbl(final Integer idNotifica) {
		List<NotificaDTO> notifiche = new ArrayList<>();
		try {
			if (idNotifica != null) {
				notifiche = notificaSRV.recuperaNotifiche(idNotifica, utente.getId(), utente.getIdAoo(), utente.getFcDTO());
				FacesHelper.executeJS("calculateIframeHeight()");
			} else {
				notifiche = notificaSRV.recuperaNotifiche(utente.getId(), utente.getIdAoo(), utente.getFcDTO(), aPartireDaNgiorni);
			}
		} catch (final Exception e) {
			showError("Errore durante l'aggiornamento delle notifiche. " + e.getMessage());
			LOGGER.error(e);
		}

		return notifiche;
	}

	/**
	 * Gestisce la rimozione di una riga dal datatable.
	 * @param event
	 */
	public void rimuoviSingolo(final ActionEvent event) {
		final NotificaDTO element = getDataTableClickedRow(event);
		rimuoviNotifica(element, notificheDTH);
	}

	/**
	 * Recupera la coda del documento a partire dal suo document title.
	 * @param event
	 */
	public void showQueueDocument(final ActionEvent event) {
		Integer count;
		codeCount = new EnumMap<>(DocumentQueueEnum.class); //azzero il contenitore prima di ricalcolare le code per non creare incongruenze nella view
		notificaCode = getDataTableClickedRow(event);
		
		final RecuperoCodeDTO dto = ricercaSRV.getQueueNameFromDocumentTitle(utente, notificaCode.getIdDocumento(), notificaCode.getClasseDocumentale(), notificaCode.getIdCategoriaDocumento(), true);
		listaCodeNonCensite = StringUtils.fromStringListToString(dto.getCodeNonCensite());
		final Collection<DocumentQueueEnum> queue = dto.getCode();
		
		if (queue != null && !queue.isEmpty()) {
			final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
			for (final DocumentQueueEnum coda: queue) {
				//queue.key.name eq 'In_Nessuna_Coda_Servizio' or queue.key.name eq 'Non_Censito_Servizio'
				if (sessionBean.haAccessoAllaFunzionalita(coda.getAccessFun()) 
						&& !coda.equals(DocumentQueueEnum.NESSUNA_CODA) 
						&& !coda.equals(DocumentQueueEnum.NON_CENSITO)) {

					count = Collections.frequency(queue, coda);
					codeCount.put(coda, count);
				}
			}
		}
	}

	/**
	 * Metodo che utilizza la coda recuperata sopra per la navigazione nella stessa.
	 * @return navigationToken
	 */
	public final String goToCoda(final DocumentQueueEnum q) {
		final MasterDocumentRedDTO m = new MasterDocumentRedDTO();
		m.setDocumentTitle(notificaCode.getIdDocumento());
		if (!StringUtils.isNullOrEmpty(notificaCode.getNumeroDocumento())) {
			m.setNumeroDocumento(Integer.valueOf(notificaCode.getNumeroDocumento()));
		}
		final GoToCodaDTO goToCodaInfo = new GoToCodaDTO(m);
		FacesHelper.putObjectInSession(ConstantsWeb.SessionObject.FILTER_MASTER_GO_TO_CODA, goToCodaInfo);
		
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		return sessionBean.gotoPage(NavigationTokenEnum.get(q));
	}

	/**
	 * Post construct del component.
	 */
	@Override
	protected void postConstruct() {
		throw new UnsupportedOperationException("Questo bean è esclusivamente un helper non deve essere gestito dal framework");
	}

	/** 
	 * Gestione selezione singola e multipla della tabella.
	 */
	public void handleAllCheckBoxNotificheSottoscrizioni() {
		handleAllCheckBox(notificheDTH);
	}

	/**
	 * {@link #updateCheck(AjaxBehaviorEvent, SimpleDetailDataTableHelper)}
	 * @param event
	 */
	public void updateCheckNotificheSottoscrizioni(final AjaxBehaviorEvent event) {
		updateCheck(event, notificheDTH);
	}

	private void handleAllCheckBox(final SimpleDetailDataTableHelper<NotificaDTO> dth) {
		super.handleAllCheckBoxGeneric(dth);
	}

	private NotificaDTO updateCheck(final AjaxBehaviorEvent event, final SimpleDetailDataTableHelper<NotificaDTO> dth) {
		final NotificaDTO masterChecked = getDataTableClickedRow(event);
		final Boolean newValue = ((SelectBooleanCheckbox) event.getSource()).isSelected();
		if (newValue != null && newValue) {
			dth.getSelectedMasters().add(masterChecked);
		} else {
			dth.getSelectedMasters().remove(masterChecked);
		}
		return masterChecked;
	}

	/* Dettaglio */

	/**
	 * {@link #refreshDetail(NotificaDTO)}
	 */
	public void refreshDetail() {
		refreshDetail(current);
	}

	/**
	 * Modifica il dettaglio nel componente
	 * Come se lanciasse l'evento di modifica del dettaglio
	 * @param current
	 */
	public void refreshDetail(final NotificaDTO current) {
		String documentTitle = null;
		if (current != null) {			
			documentTitle =  current.getIdDocumento();
		}
		documentDetailComponent.setDetail(documentTitle);
	}

	/**
	 * Restituisce la notifica corrente.
	 * @return current
	 */
	public NotificaDTO getCurrent() {
		return current;
	}

	/**
	 * Imposta la notifica corrente.
	 * @param current
	 */
	public void setCurrent(final NotificaDTO current) {
		this.current = current;
		refreshDetail(current);
	}

	/* Getter e Setter */

	/**
	 * Restituisce il datatable delle notifiche.
	 * @return notificheDTH
	 */
	public SimpleDetailDataTableHelper<NotificaDTO> getNotificheDTH() {
		return notificheDTH;
	}

	/**
	 * Imposta il datatable delle notifiche.
	 * @param notificheDTH
	 */
	public void setNotificheDTH(final SimpleDetailDataTableHelper<NotificaDTO> notificheDTH) {
		this.notificheDTH = notificheDTH;
	}

	/**
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Restituisce la lista di notifiche.
	 * @return notificheDTO
	 */
	public List<NotificaDTO> getNotificheDTO() {
		return notificheDTO;
	}

	/**
	 * Imposta l'utente corrente.
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Imposta la lista di notifiche.
	 * @param notificheDTO
	 */
	public void setNotificheDTO(final List<NotificaDTO> notificheDTO) {
		this.notificheDTO = notificheDTO;
	}

	/**
	 * @return aPartireDaNgiorni
	 */
	public Integer getaPartireDaNgiorni() {
		return aPartireDaNgiorni;
	}

	/**
	 * @param aPartireDaNgiorni
	 */
	public void setaPartireDaNgiorni(final Integer aPartireDaNgiorni) {
		this.aPartireDaNgiorni = aPartireDaNgiorni;
	}

	/**
	 * @return aPartireDa
	 */
	public Map<String, Integer> getaPartireDa() {
		return aPartireDa;
	}

	/**
	 * @param aPartireDa
	 */
	public void setaPartireDa(final Map<String, Integer> aPartireDa) {
		this.aPartireDa = aPartireDa;
	}

	/**
	 * @return contrassegnato
	 */
	public boolean isContrassegnato() {
		return contrassegnato;
	}

	/**
	 * @param contrassegnato
	 */
	public void setContrassegnato(final boolean contrassegnato) {
		this.contrassegnato = contrassegnato;
	}

	/**
	 * @return codeCount
	 */
	public Map<DocumentQueueEnum, Integer> getCodeCount() {
		return codeCount;
	}

	/**
	 * Restituisce una stringa che indica la lista
	 * di code non censiste dove si trova il documento.
	 * @return listaCodeNonCensite
	 */
	public String getListaCodeNonCensite() {
		return listaCodeNonCensite;
	}

	/**
	 * Restituisce il flag di visibilità della popup del documento.
	 * @return popupDocumentVisible
	 */
	public boolean isPopupDocumentVisible() {
		return popupDocumentVisible;
	}

	/**
	 * Imposta la visibilità della popup del documento.
	 * @param popupDocumentVisible
	 */
	public void setPopupDocumentVisible(final boolean popupDocumentVisible) {
		this.popupDocumentVisible = popupDocumentVisible;
	}

	/**
	 * Restituisce il component delle sottoscrizioni
	 * per il dettaglio del documento.
	 *
	 * @return documentDetailComponent
	 */
	public DettaglioDocumentoSottoscrizioniComponent getDocumentDetailComponent() {
		return documentDetailComponent;
	}
}
