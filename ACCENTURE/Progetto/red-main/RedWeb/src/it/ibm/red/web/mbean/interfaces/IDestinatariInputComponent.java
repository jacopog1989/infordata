package it.ibm.red.web.mbean.interfaces;

import java.util.List;

import it.ibm.red.business.dto.DestinatarioRedDTO; 
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;

/**
 * Gestisce il componente che 
 * @author a.difolca
 *
 */
public interface IDestinatariInputComponent extends IDatatableUtils {
	/*****************************PULSANTI*********************************/
	/**
	 * Prepara il rubrica bean per mostrare la dialog.
	 * Inoltre aggiunge l'enum tipoRubricaEnum ai destinatari, non so bene perché fa questa operazione qua e non all'atto di inizializzazione del destinatario.
	 */
	void openRubricaDestinatari(String clientId);
	
	/***************************************DATATABLE****************************************************************************/

		/**
	 * Ritorna una lista di destinatari che rappresentano i destinatari selezionati dall'utente.
	 * @return
	 */
	List<DestinatarioRedDTO> getDestinatariInModificaPuliti();
	
	/**
	 * Ottiene le modalità destinatario.
	 * @return lista di modalità destinatario
	 */
	List<ModalitaDestinatarioEnum> getComboModalitaDestinatario();
	
	/**
	 * trigger dell'info contatto.
	 * @param d
	 */
	void selectDestinatarioDaVisualizzare(DestinatarioRedDTO d);
	
	/**
	 * Permette di recuperare il contatto da visualizzare.
	 * @return
	 */
	DestinatarioRedDTO getContattoDaVisualizzare();
	
	/**
	 * LEimina il destinatario dalla lista dei destinatari.
	 * @param d
	 */
	void eliminaDestinatario(Integer indexDestinatario);

}
