package it.ibm.red.web.servlets;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import com.google.common.net.MediaType;

import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.constants.ConstantsWeb.SessionObject;

/**
 * Classe PrintContentServlet.
 *
 * @author VINGENITO
 * 
 *         Servlet print content.
 */
public class PrintContentServlet extends HttpServlet {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PrintContentServlet.class.getName());

	/**
	 * Costruttore prints the content servlet.
	 */
	public PrintContentServlet() {
		super();
	}

	@Override
	protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) {
		try {
			// Il chiamante deve mettere FileDTO in sessione
			final HttpSession session = request.getSession();
			FileDTO file = null;

			if (session != null) {
				file = (FileDTO) session.getAttribute(SessionObject.PRINT_FILE_SERVLET);
				session.removeAttribute(SessionObject.PRINT_FILE_SERVLET);

				if (file != null && file.getContent() != null) {
					LOGGER.info("Conversione file per la stampa - START");
					final byte[] newContent = file.getContent();
					response.getOutputStream().write(newContent);
					if (newContent != null) {
						response.setContentLength(newContent.length);
					}
					final String contentType = MediaType.PDF + "; name=\"" + StringUtils.split(file.getFileName(), ".")[0] + ".pdf" + "\"";
					response.setContentType(contentType);
					final String header = " inline; filename=\"" + StringUtils.split(file.getFileName(), ".")[0] + ".pdf" + "\"";
					response.setHeader("Content-Disposition", header);
					response.getOutputStream().close();
					LOGGER.info("Conversione file per la stampa - END");
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero o la conversione del file", e);
		}
	}
}
