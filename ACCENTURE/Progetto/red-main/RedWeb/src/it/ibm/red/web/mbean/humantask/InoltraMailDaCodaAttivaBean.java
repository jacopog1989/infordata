package it.ibm.red.web.mbean.humantask;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections4.CollectionUtils;

import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.TestoPredefinitoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.FunzionalitaEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ICasellePostaliFacadeSRV;
import it.ibm.red.business.service.facade.IFascicoloFacadeSRV;
import it.ibm.red.business.service.facade.IInoltraMailDaCodaAttivaFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.enums.ModalitaSpedizioneMailEnum;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.component.DestinatariInputComponent;
import it.ibm.red.web.mbean.component.DocumentiAllegabiliComponent;
import it.ibm.red.web.mbean.component.OggettoTestoMailComponent;
import it.ibm.red.web.mbean.interfaces.DocumentiAllegabiliSelectableInterface;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * Bean che gestisce l'inoltro mail da coda attiva.
 */
@Named(MBean.INOLTRA_MAIL_DA_CODA_ATTIVA)
@ViewScoped
public class InoltraMailDaCodaAttivaBean extends AbstractBean implements InitHumanTaskInterface {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 3028195057027366213L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(InoltraMailDaCodaAttivaBean.class);
 
    /**
     * Servizio.
     */
	private IInoltraMailDaCodaAttivaFacadeSRV inoltraSRV;
	
	/**
     * Servizio.
     */
	private IFascicoloFacadeSRV fascicoloSRV;
	
    /**
     * Utente.
     */
	private UtenteDTO utente;
	
	/**
	 * Documento su cui è stata eseguita la response.
	 */
	private MasterDocumentRedDTO master;
	
	/**
	 * Litsa di mittenti con cui è possibile inviare l'email.
	 */
	private List<CasellaPostaDTO> mittenteList;

    /**
     * Id mittente.
     */
	private String mittenteId; // mittente selezionato.
	
	/**
	 * Modalita di spedizione selezionata.
	 * La prende direttamente dall'enum ModalitaDiSpedizioneEnum.
	 */
	private Integer modalitaSpedizioneMail;
	/**
	 * Gestisce il testo mail e l'oggetto insieme alle varie interazioni.
	 */
	private OggettoTestoMailComponent mailComponent;
	

    /**
     * Component destinatari.
     */
	private DestinatariInputComponent destinatariComponent;
	

    /**
     * Component documenti allegabili.
     */
	private DocumentiAllegabiliComponent allegabiliComponent;


    /**
     * Lista testi predefiniti.
     */
	private List<TestoPredefinitoDTO> testiPredefinitiList;


    /**
     * Dimensione totale.
     */
	private BigDecimal dimensioneTotale;
	
	/**
	 * Post contruct.
	 */
	@Override
	@PostConstruct
	public void postConstruct() {
		inoltraSRV = ApplicationContextProvider.getApplicationContext().getBean(IInoltraMailDaCodaAttivaFacadeSRV.class);
		final ICasellePostaliFacadeSRV casellePostaliSRV = ApplicationContextProvider.getApplicationContext().getBean(ICasellePostaliFacadeSRV.class);
		fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloFacadeSRV.class);
		
		final SessionBean session = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = session.getUtente();
		
		mittenteList = casellePostaliSRV.getCasellePostali(utente, FunzionalitaEnum.INOLTRA_MAIL_DA_CODA);
		
		testiPredefinitiList = inoltraSRV.getTestiPredefiniti(utente);
		
		dimensioneTotale = inoltraSRV.getDimensioneMassimaTotaleAllegatiMailBigDecimalKB();
	}

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		try {
			reset();
			destinatariComponent = new DestinatariInputComponent();
			
			master = inDocsSelezionati.get(0);
			final FascicoloDTO fascicolo = fascicoloSRV.getFascicoloProcedimentale(master.getDocumentTitle(), utente.getIdAoo().intValue(), utente);
			
			final String defaultOggetto = inoltraSRV.createDefaultOggetto(master, fascicolo, utente);
			mailComponent = new OggettoTestoMailComponent(testiPredefinitiList, defaultOggetto);
			
			final List<DocumentoAllegabileDTO> documentiAllegabili = fascicoloSRV.getDocumentiAllegabili(master.getWobNumber(), utente, fascicolo, false);
			allegabiliComponent = new DocumentiAllegabiliComponent(fascicolo, documentiAllegabili, dimensioneTotale);
		} catch (final Exception e) {
			LOGGER.error("Si e' verificato un errore durante l'inizializzazione dell'inizializzazione della response INOLTRA MAIL.", e);
			showError("Impossibile inizializzare la respose.");
		}
	}
	
	/**
	 * Siccome primefaces non consente di pescare l'oggetto come chiave dei select item tocca a noi recuperare l'oggetto in base al value.
	 * In realtà si può usare un converter per fare questa operazione ma così si scrive di meno.
	 * @return
	 * La CasellaPostaDTO selelzionata dall'utente.
	 */
	private CasellaPostaDTO getSelectedMittente() {
		CasellaPostaDTO selectedMittente = null;
		if (mittenteId != null) {
			final Optional<CasellaPostaDTO> optional = mittenteList.stream()
			.filter(m -> mittenteId.equals(m.getIndirizzoEmail()))
			.findFirst();
			if (optional.isPresent()) {
				selectedMittente = optional.get();
			}
		}
		return selectedMittente;
	}
	
	/**
	 * Inoltra il documento.
	 */
	public void registra() {
		try {
			final CasellaPostaDTO mittente = getSelectedMittente();
			final List<DestinatarioRedDTO> destinatari = destinatariComponent.getDestinatariInModificaPuliti();
			final List<DocumentoAllegabileDTO> allegabili = allegabiliComponent.getSelectedAllegabili();
			final String oggetto = mailComponent.getOggetto();
			final String body = mailComponent.getBody();
			
			final boolean inputValido = validate(mittente, destinatari, allegabili);
			
			if (inputValido) {
				final ListaDocumentiBean ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
				
				ldb.cleanEsiti();
				
				final EsitoOperazioneDTO esito = inoltraSRV.inviaMail(utente, master.getWobNumber(), getSelectedMittente(), destinatariComponent.getDestinatariInModificaPuliti(),
						oggetto, body, modalitaSpedizioneMail, allegabiliComponent.getSelectedAllegabili());
				
				final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>(); 
				
				esiti.add(esito);
				
				ldb.setEsitiOperazione(esiti);
				ldb.destroyBeanViewScope(ConstantsWeb.MBean.INOLTRA_MAIL_DA_CODA_ATTIVA);
				FacesHelper.executeJS("PF('dlgGeneric').hide();PF('dlgShowResult').show();");
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Si è verificato un errore durante l'inoltro del documento.");
		}
	}
	
	private boolean validate(final CasellaPostaDTO mittente, final List<DestinatarioRedDTO> destinatari, final List<DocumentoAllegabileDTO> allegabili) {
		boolean valid = true;
		if (mittente == null) {
			valid = false;
			showWarnMessage("Attenzione, è necessario selezionare il mittente.");
		}
		
		if (CollectionUtils.isEmpty(destinatari)) {
			valid = false;
			showWarnMessage("Attenzione, è necessario inserire almeno un destinatario.");
		} else {
			final Optional<DestinatarioRedDTO> atLeastOne = destinatari.stream()
			.filter(a -> ModalitaDestinatarioEnum.TO.equals(a.getModalitaDestinatarioEnum()))
			.findFirst();
			
			if (!atLeastOne.isPresent()) {
				valid = false;
				showWarnMessage("Attenzione, deve essere inserito almeno un destinatario come \"A\".");
			}
		}
		
		valid = mailComponent.validate() && valid;
		
		if (CollectionUtils.isEmpty(allegabili)) {
			valid = false;
			showWarnMessage("Attenziona, è necessario inserire almeno un procedimento da allegare.");
		} else {
			valid = allegabiliComponent.validate() && valid;
		}
		
		return valid;
	}
		
	private void reset() {
		if (mailComponent != null) {
			mailComponent.reset();
		}
		mittenteId = (mittenteList != null && !mittenteList.isEmpty()) ? mittenteList.get(0).getIndirizzoEmail() : null;
		modalitaSpedizioneMail = ModalitaSpedizioneMailEnum.SINGOLA.getValue();
	}

	/**
	 * Recupera la lista di testi predefiniti.
	 * @return lista di testi predefiniti
	 */
	public List<TestoPredefinitoDTO> getTestiPredefinitiList() {
		return testiPredefinitiList;
	}

	/**
	 * Imposta la lista di testi predefiniti.
	 * @param testiPredefinitiList lista di testi predefiniti
	 */
	public void setTestiPredefinitiList(final List<TestoPredefinitoDTO> testiPredefinitiList) {
		this.testiPredefinitiList = testiPredefinitiList;
	}

	/**
	 * Recupera la lista dei mittenti.
	 * @return lista dei mittenti
	 */
	public List<CasellaPostaDTO> getMittenteList() {
		return mittenteList;
	}

	/**
	 * Imposta la lista dei mittenti.
	 * @param mittenteList lista dei mittenti
	 */
	public void setMittenteList(final List<CasellaPostaDTO> mittenteList) {
		this.mittenteList = mittenteList;
	}

	/**
	 * Recupera l'id del mittente.
	 * @return id del mittente
	 */
	public String getMittenteId() {
		return mittenteId;
	}

	/**
	 * Imposta l'id del mittente
	 * @param mittente id del mittente
	 */
	public void setMittenteId(final String mittente) {
		this.mittenteId = mittente;
	}

	/**
	 * Recupera i destinatari.
	 * @return destinatari
	 */
	public DestinatariInputComponent getDestinatariComponent() {
		return destinatariComponent;
	}

	/**
	 * Recupera la modalità di spedizione della mail.
	 * @return modalità di spedizione della mail
	 */
	public Integer getModalitaSpedizioneMail() {
		return modalitaSpedizioneMail;
	}

	/**
	 * Imposta la modalità di spedizione della mail.
	 * @param modalitaSpedizioneMail
	 */
	public void setModalitaSpedizioneMail(final Integer modalitaSpedizioneMail) {
		this.modalitaSpedizioneMail = modalitaSpedizioneMail;
	}

	/**
	 * Recupera la mail.
	 * @return mail
	 */
	public OggettoTestoMailComponent getMailComponent() {
		return this.mailComponent;
	}

	/**
	 * Recupera gli allegati.
	 * @return allegati
	 */
	public DocumentiAllegabiliSelectableInterface getAllegabiliComponent() {
		return allegabiliComponent;
	}
}