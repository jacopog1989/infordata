package it.ibm.red.web.mbean.interfaces;

import it.ibm.red.business.dto.TitolarioDTO;

/**
 * 
 * @author m.crescentini
 *
 */
public interface ISelezionaTitolarioChiamante {
	
	/**
	 * Aggiorna l'indice di classificazione.
	 * @param titolarioSelezionato
	 */
	void aggiornaIndiceClassificazione(TitolarioDTO titolarioSelezionato);
}
