package it.ibm.red.web.mbean.interfaces;

import java.math.BigDecimal;

/**
 * Permette di conteggiare la size degli elementi selezionati.
 * 
 * L'interfaccia corrispondente razionalizza la grafica e eventuali messaggi di errore.
 *
 */
public interface IDocumentiAllegabiliSizeCounter {
	/**
	 * Dimensione totale dei documenti selezionati.
	 */
	BigDecimal getDimensioneSelected();

	/**
	 * Dimensione totale dei documenti selezionati e non.
	 */
	BigDecimal getDimensioneTotale();
	
	/**
	 * Data una determinata regola mostra un messaggio o meno, 
	 * se la dimensione dei documenti selezionati viola per qualche ragione una regola di validaizone.
	 */
	boolean isDimensioneSuperata();
}
