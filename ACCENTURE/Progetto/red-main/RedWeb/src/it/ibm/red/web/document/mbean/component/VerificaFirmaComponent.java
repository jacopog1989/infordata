package it.ibm.red.web.document.mbean.component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.google.common.net.MediaType;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.VerifyInfoDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.web.helper.export.ExportExcelHelper;
import it.ibm.red.web.mbean.interfaces.IVerificaFirmaComponent;

/**
 * Componente per il pannello di verifica della firma.
 * 
 * @author m.crescentini
 */
public class VerificaFirmaComponent extends AbstractComponent implements IVerificaFirmaComponent {

	private static final long serialVersionUID = 2828901863717150669L;

	/**
	 * Logger del componente.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(VerificaFirmaComponent.class);

	/**
	 * Informazioni verifica firma.
	 */
	protected VerifyInfoDTO infoFirma;

	/**
	 * Document title.
	 */
	protected String documentTitleInfoFirma;

	/**
	 * Costruttore vuoto.
	 */
	public VerificaFirmaComponent() {
	}

	/**
	 * Costruttore di default.
	 * @param infoFirma
	 * @param docTitle
	 */
	public VerificaFirmaComponent(final VerifyInfoDTO infoFirma, final String docTitle) {
		this.infoFirma = infoFirma;
		this.documentTitleInfoFirma = docTitle;
	}
	
	/**
	 * @return
	 */
	@Override
	public VerifyInfoDTO getInfoFirma() {
		return infoFirma;
	}

	/**
	 * @see it.ibm.red.web.mbean.interfaces.IVerificaFirmaComponent#downloadExportVerificaFirma().
	 */
	@Override
	public StreamedContent downloadExportVerificaFirma() {
		LOGGER.info("downloadExportVerificaFirma -> START");
		StreamedContent exportVerificaFirma = null;
		try {
			if (StringUtils.isNotBlank(documentTitleInfoFirma) && infoFirma != null) {				
				final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				final ExportExcelHelper exportExcelHelper = new ExportExcelHelper();
				// Si costruisce il workbook Excel
				final HSSFWorkbook verificaFirmaExportWorkbook = exportExcelHelper.getExportVerificaFirma(infoFirma.getFirmatari());
				verificaFirmaExportWorkbook.write(outputStream);
				
				exportVerificaFirma = new DefaultStreamedContent(new ByteArrayInputStream(outputStream.toByteArray()), MediaType.MICROSOFT_EXCEL.toString(), 
						documentTitleInfoFirma + "_DETTAGLIOFIRMA_" + DateUtils.dateToString(new Date(), DateUtils.DD_MM_YYYY_EXPORT) + ".xls", 
						StandardCharsets.UTF_8.name());
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'export delle informazioni di firma del documento: " + documentTitleInfoFirma, e);
			showErrorMessage("Si è verificato un errore durante l'esportazione");
		}
		LOGGER.info("downloadExportVerificaFirma -> END");
		return exportVerificaFirma;
	}

	/**
	 * Update delle informazioni del metadato verifica firma del documento in input.
	 * @param guid
	 * @param documentTitle
	 * @param utente
	 */
	public void aggiornaVerificaFirma(final String guid, final String documentTitle, final UtenteDTO utente) {
		final ISignFacadeSRV signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignFacadeSRV.class);
		infoFirma = signSRV.aggiornaVerificaFirma(guid, utente);
		documentTitleInfoFirma = documentTitle;
	}
}
