package it.ibm.red.web.ricerca.mbean;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.component.UIColumn;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IPropertiesFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.enums.DettaglioDocumentoFileEnum;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.utils.ExportUtils;

/**
 * Abstract Bean della ricerca.
 * @param <M>
 */
public abstract class RicercaAbstractBean<M> extends AbstractBean {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 8735231145923157885L;
	
	/**
	 * File principale.
	 */
	protected static final String FILE_PRINCIPALE = "File principale: ";
	
	/**
	 * Messaggio informativo per il raggiungimento del massimo numero di risultati.
	 */
	public static final String MSG_NUM_MAX_RISULTATI = "La ricerca ha raggiunto il numero massimo di elementi visualizzabili." 
			+ " Inserire ulteriori filtri di ricerca per ridurre il numero dei risultati.";
	
	/**
	 *  Numero di caratteri a cui troncare la stringa 
	 */
	private static final int TRUNCATE_SIZE = 60;

	/**
	 * Bean di sessione.
	 */
	protected SessionBean sessionBean;

	/**
	 * Utente.
	 */
	protected UtenteDTO utente;
	
	/**
	 * Numero massimo di risultati della ricerca.
	 */
	private Integer numMaxRisultati;
	
	/**
	 * Dettaglio documento.
	 */
	private DettaglioDocumentoFileEnum dettaglioDaCaricare;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	protected void postConstruct() {
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		
		dettaglioDaCaricare = DettaglioDocumentoFileEnum.GENERICO;
		
		final IPropertiesFacadeSRV propertiesSRV = ApplicationContextProvider.getApplicationContext().getBean(IPropertiesFacadeSRV.class);
		numMaxRisultati = Integer.parseInt(propertiesSRV.getByEnum(PropertiesNameEnum.RICERCA_MAX_RESULTS));
	}
	
	/**
	 * Restituisce il documento attualmente selezionato dall'utente o sul quale si desidera eseguire una operazione.
	 */
	protected abstract M getSelectedDocument();

	/**
	 * Gestisce la selezione del edttaglipartire dal master della ricerca.
	 * 
	 * @param m
	 */
	protected abstract void selectDetail(M m);
	
	/**
	 * Metodo per recuperare il valore corretto da esportare per la colonna 'descrizione' 
	 * @param column
	 * @return
	 */
	public String columnDescrizione(final UIColumn column) {
       return ExportUtils.columnCorrectValue(column);
    }
	
	/** 
	 * Abbrevia il label presente in una colonna di un datatable con i puntini.
	 * Inserire il label intero con i puntini utilizzanfo l'apposito metodo getColumnTooltip
	 * 
	 * Gestisce il caso label vuoto restituisce il classico  " - "
	 * 
 	 * @param columnLabel
	 * 	Stringa da visualizzare nella colonna
	 * @param size
	 * 	massimo numero di caratteri del label che si vogliono visualizzare
	 *  
	 * @return la stringa columnLabel se maggiore di size troncata di size e aggiungendo i punti '...' alla fine
	 * se columnLabel è null ritorna " - "
	 * 
	 */
	@Override
	public String getShortColumn(final String columnLabel, final Integer size) {
		return StringUtils.truncateColumnTable(columnLabel, size);
	}
	
	/**
	 * Restituisce la label della colonna abbreviata.
	 */
	@Override
	public String getShortColumn(final String columnLabel) {
		return StringUtils.truncateColumnTable(columnLabel, TRUNCATE_SIZE);
	}
	
	/**
	 * Restituisce il nome del file per l'export della coda.
	 * @return nome file per export
	 */
	public String getNomeFileExportCoda() {
		String nome = "";
		if (sessionBean.getUtente() != null) {
			nome += sessionBean.getUtente().getNome().toLowerCase() + "_" + sessionBean.getUtente().getCognome().toLowerCase() + "_";
		}
		
		nome += getNomeRicerca4Excel() + "_";
		
		final SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH.mm");
		nome += sdf.format(new Date()); 
		
		return nome;
	}
	
	/**
	 * Restituisce una parte della stringa del file Excel per l'export dei risultati di ricerca.
	 * @return nome file excel per export
	 */
	protected abstract String getNomeRicerca4Excel();

	/**
	 * Restituisce il dettaglio da caricare.
	 * @return dettaglio da caricare
	 */
	public DettaglioDocumentoFileEnum getDettaglioDaCaricare() {
		return dettaglioDaCaricare;
	}

	/**
	 * Restituisce il numero massimo dei risultati.
	 * @return numero massimo risultati
	 */
	public Integer getNumMaxRisultati() {
		return numMaxRisultati;
	}

	/**
	 * Imposta il numero massimo dei risultati.
	 * @param numMaxRisultati
	 */
	public void setNumMaxRisultati(final Integer numMaxRisultati) {
		this.numMaxRisultati = numMaxRisultati;
	}

}
