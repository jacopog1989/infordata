package it.ibm.red.web.mbean.humantask;

import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.asign.step.StatusEnum;
import it.ibm.red.business.service.facade.IASignFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * @author SimoneLungarella
 * Bean che gestisce la response: Retry.
 */

@Named(ConstantsWeb.MBean.RETRY_BEAN)
@ViewScoped
public class RetryBean extends AbstractBean implements InitHumanTaskInterface {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1105905636151795505L;

	/**
	 * Il service che gestisce la firma asincrona.
	 */
	private IASignFacadeSRV asyncSignSRV;
	
	/**
	 * Il bean: ListaDocumentiBean.
	 */
	private ListaDocumentiBean ldb;
	
	/**
	 * La lista dei documenti selezionati dal datatable.
	 */
	private List<MasterDocumentRedDTO> documenti;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	protected void postConstruct() {
		// Non occorre fare niente.
	}

	/**
	 * Inizializza il bean impostando i documenti selezionati in {@link #documenti}.
	 */
	@Override
	public void initBean(List<MasterDocumentRedDTO> inDocsSelezionati) {
		asyncSignSRV = ApplicationContextProvider.getApplicationContext().getBean(IASignFacadeSRV.class);
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);

		// Ottengo documenti selezionati dalla coda
		documenti = inDocsSelezionati;
	}
	
	/**
	 * Gestisce la chiusura del bean.
	 */
	public void chiudi() {
		ldb.destroyBeanViewScope(MBean.RETRY_BEAN);
	}
	
	/**
	 * Esegue la logica di reset dei tentativi di firma per i documenti selezionati: {@link #documenti}.
	 */
	public void riprovaProcessoFirma() {
		FacesHelper.executeJS("PF('dlgGeneric').hide()");
		// Si puliscono gli esiti
		ldb.cleanEsiti();
		
		for (MasterDocumentRedDTO documento : documenti) {
			// Recupero dell'id dell'item di firma da resettare.
			Long idItem = asyncSignSRV.getIdItem(documento.getDocumentTitle());
			
			// Inizializzazione esito prima dell'operazione
			EsitoOperazioneDTO eOpe = new EsitoOperazioneDTO(documento.getWobNumber());
			eOpe.setEsito(false);
			eOpe.setNote("Errore riscontrato durante l'operazione.");
			
			if(idItem != null && StatusEnum.ERRORE.equals(asyncSignSRV.checkDocStatus(documento.getDocumentTitle()))) {
				eOpe = asyncSignSRV.resetRetry(idItem);
				
				// Si imposta il nuovo esito
				ldb.getEsitiOperazione().add(eOpe);
			}
		}
		
		FacesHelper.executeJS("PF('dlgGeneric').hide()");
		FacesHelper.update("centralSectionForm:idDlgShowResult");
		FacesHelper.executeJS("PF('dlgShowResult').show()");
		ldb.destroyBeanViewScope(MBean.RETRY_BEAN);
	}

	
}
