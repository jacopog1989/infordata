package it.ibm.red.web.mbean.humantask;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections.CollectionUtils;

import it.ibm.red.business.constants.Constants.Varie;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.mbean.DocumentManagerBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;


/**
 * @author m.crescentini
 *
 */
@Named(MBean.RICHIEDI_CONTRIBUTO_ESTERNO_BEAN)
@ViewScoped
public class RichiediContributoEsternoBean extends AbstractBean implements InitHumanTaskInterface {
	
	private static final long serialVersionUID = -361970520692874816L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RichiediContributoEsternoBean.class);
	
	/**
	 * Utente.
	 */
	private UtenteDTO utente;
	
	/**
	 * Properties.
	 */
	private PropertiesProvider pp;
	
	/**
	 * Lista documenti bean.
	 */
	private ListaDocumentiBean ldb;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		utente = ((SessionBean) FacesHelper.getManagedBean(MBean.SESSION_BEAN)).getUtente();
		
		pp = PropertiesProvider.getIstance();
		
		ldb = FacesHelper.getManagedBean(MBean.LISTA_DOCUMENTI_BEAN);
	}
	
	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		preparaMascheraCreazione(inDocsSelezionati);
	}
	
	
	private void preparaMascheraCreazione(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		try {
			if (!CollectionUtils.isEmpty(inDocsSelezionati)) {
				final MasterDocumentRedDTO masterSelezionato = inDocsSelezionati.get(0);
				
				final DetailDocumentRedDTO docContributoEsterno = new DetailDocumentRedDTO();
				
				docContributoEsterno.setDocumentClass(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_GENERICO_CLASSNAME_FN_METAKEY));
				docContributoEsterno.setIdCategoriaDocumento(CategoriaDocumentoEnum.CONTRIBUTO_ESTERNO.getIds()[0]);
				docContributoEsterno.setIdTipologiaDocumento(
						Integer.parseInt(pp.getParameterByString(utente.getCodiceAoo() + "." + Varie.CONTRIBUTO_ESTERNO_USCITA_ID_KEY_SUFFIX)));
				docContributoEsterno.setNumeroMittenteContributo(masterSelezionato.getNumeroDocumento());
				docContributoEsterno.setAnnoMittenteContributo(masterSelezionato.getAnnoDocumento());
				docContributoEsterno.setWobNumberSelected(masterSelezionato.getWobNumber());
				
				final DocumentManagerBean bean = FacesHelper.getManagedBean(MBean.DOCUMENT_MANAGER_BEAN);
				// Si pulisce nel caso sia già stato aperto 
				bean.unsetDetail();
				bean.setChiamante(MBean.RICHIEDI_CONTRIBUTO_ESTERNO_BEAN);
				bean.setDetail(docContributoEsterno);
				
				ldb.destroyBeanViewScope(MBean.RICHIEDI_CONTRIBUTO_ESTERNO_BEAN);
				
				FacesHelper.update("idDettagliEstesiForm:idPanelDocumentManager");
				FacesHelper.update("idDettagliEstesiForm:idPanelContainerDocumentManager_T");
				
				// Si mostra la maschera di creazione
				FacesHelper.executeJS("PF('dlgGeneric').hide()");
				FacesHelper.executeJS("PF('dlgManageDocument_RM').show()");
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}
}