package it.ibm.red.web.document.mbean.interfaces;

import it.ibm.red.web.mbean.interfaces.IGoToCodaButtonComponent;
import it.ibm.red.web.mbean.interfaces.IMenuAllegatiButtonAndPanelComponent;
import it.ibm.red.web.mbean.interfaces.IOpenDettaglioEstesoButtonComponent;

/**
 * I bottoni del dettaglio semplice east senza i bottoni della response
 * @author a.difolca
 */
public interface IDettaglioDocumentoEastButtonNoResp extends IGoToCodaButtonComponent, IMenuAllegatiButtonAndPanelComponent, IOpenDettaglioEstesoButtonComponent {
	/**
	 * apre la visualizzione del documento
	 */
	void openVisualizzaDocumento();
}
