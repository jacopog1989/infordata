package it.ibm.red.web.mbean;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ValueChangeEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.model.DualListModel;

import it.ibm.red.business.dto.ThemeDTO;
import it.ibm.red.business.enums.ThemeEnum;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.enums.HomepageEnum;
import it.ibm.red.web.enums.MenuItemEnum;
import it.ibm.red.web.factory.ThemeFactory;
import it.ibm.red.web.helper.faces.FacesHelper;


/**
 * Classe PersonalizzazioneBean.
 */
@Named(ConstantsWeb.MBean.PERSONALIZZAZIONE_BEAN)
@ViewScoped
public class PersonalizzazioneBean extends AbstractBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -2555093161716867311L;

	/**
	 * Session Bean.
	 */
	private SessionBean sessionBean;

    /**
     * Facade service Utente.
     */
    private IUtenteFacadeSRV utenteSRV;

    /**
     * Tema selezionato.
     */
    private String temaSelezionato;

    /**
     * Delay autocomplete.
     */
    private Integer autocompleteDelay;
    
    /**
     * Tiles.
     */
    private DualListModel<MenuItemEnum> tiles;
    
    /**
     * Identificativo tema.
     */
	private Integer idTheme;

	/**
	 * Post construct del bean.
	 */
    @Override
    @PostConstruct
    protected void postConstruct() {
    	temaSelezionato = "";
    	sessionBean = (SessionBean) FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
    	List<MenuItemEnum> selectedTiles = new ArrayList<>();
    	tiles = new DualListModel<>(Arrays.asList(MenuItemEnum.values()), selectedTiles);
    	autocompleteDelay = sessionBean.getPreferenze().getAutocompleteDelay();
    }
    
	/**
	 * Permette di cambiare il tema scelto dall'utente.
	 *
	 * @param event
	 *            evento di modifica
	 */
    public void changeUserTheme(final ValueChangeEvent event) {
    	temaSelezionato = (String) event.getNewValue();
    	for (final ThemeDTO themeEnum : ThemeEnum.getThemes()) {
    		if (temaSelezionato.equals(themeEnum.getName())) {
    			idTheme = themeEnum.getId();
    		}
    	}
    }
    
    /**
     * Salva.
     */
    public void salva() {
    	// Metodo intenzionalmente vuoto.
    }
    
    /**
     * Annulla.
     *
     * @return url homepage
     */
    public String annulla() {
    	sessionBean.rinnovaSessione();
    	return sessionBean.gotoHomepage();
    }

    /** 
     * @return the autocomplete delay
     */
    public Integer getAutocompleteDelay() {
		return autocompleteDelay;
	}

	/** 
	 * @param autocompleteDelay the new autocomplete delay
	 */
	public void setAutocompleteDelay(final Integer autocompleteDelay) {
		this.autocompleteDelay = autocompleteDelay;
	}

	/** 
	 * @return the tema selezionato
	 */
	public String getTemaSelezionato() {
		return temaSelezionato;
	}

	/** 
	 * @param temaSelezionato the new tema selezionato
	 */
	public void setTemaSelezionato(final String temaSelezionato) {
		this.temaSelezionato = temaSelezionato;
	}

	/** 
	 * @return the utente SRV
	 */
	public IUtenteFacadeSRV getUtenteSRV() {
		return utenteSRV;
	}

	/** 
	 * @return the homepages
	 */
	public final Collection<HomepageEnum> getHomepages() {
		return sessionBean.getHomePages();
	} 
	
	/** 
	 * @return the themes
	 */
	public final Collection<ThemeDTO> getThemes() {
		return ThemeFactory.getInstance().getThemes();
	} 

	/** 
	 * @return the id theme
	 */
	public Integer getIdTheme() {
		return idTheme;
	}

	/** 
	 * @return the tiles
	 */
	public DualListModel<MenuItemEnum> getTiles() {
		return tiles;
	}

	/** 
	 * @param tiles the new tiles
	 */
	public void setTiles(final DualListModel<MenuItemEnum> tiles) {
		this.tiles = tiles;
	}
	
}