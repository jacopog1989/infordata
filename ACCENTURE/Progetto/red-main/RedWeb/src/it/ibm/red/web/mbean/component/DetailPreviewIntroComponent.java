package it.ibm.red.web.mbean.component;

import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.document.mbean.component.AbstractComponent;
import it.ibm.red.web.mbean.interfaces.IDetailPreviewComponent;
import it.ibm.red.web.servlets.DownloadContentServlet;
import it.ibm.red.web.servlets.DownloadContentServlet.DocumentTypeEnum;

/**
 * Component per la gestione del dettaglio preview intro.
 */
public class DetailPreviewIntroComponent extends AbstractComponent implements IDetailPreviewComponent {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 1537019127596568761L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DetailPreviewIntroComponent.class);

	/**
	 * Info utente.
	 */
	protected UtenteDTO utente;

	/**
	 * Id crypting.
	 */
	private String cryptoId;

	/**
	 * Costruttore del component.
	 * 
	 * @param inUtente
	 */
	public DetailPreviewIntroComponent(final UtenteDTO inUtente) {
		this.utente = inUtente;
		this.cryptoId = Constants.EMPTY_STRING;
	}

	/**
	 * Imposta il detail della preview.
	 * 
	 * @param id
	 */
	public void setDetailPreview(final String id) {
		setDetailPreview(id, null);
	}

	/**
	 * Imposta il detail della preview.
	 * 
	 * @param id
	 * @param documentType
	 */
	public void setDetailPreview(final String id, final DocumentTypeEnum documentType) {
		this.cryptoId = StringUtils.isBlank(id) && (documentType == null || DocumentTypeEnum.NONE == documentType) ? Constants.EMPTY_STRING
				: getCryptoDocParamById(id, documentType);
	}

	/**
	 * Imposta i parametri del cryptoId con id, nome, mimetype e documentType.
	 * 
	 * @param id
	 * @param nomeFile
	 * @param mimeType
	 * @param documentType
	 */
	public void setPreviewProtMail(final String id, final String nomeFile, final String mimeType, final DocumentTypeEnum documentType) {
		this.cryptoId = StringUtils.isBlank(id) && (documentType == null || DocumentTypeEnum.NONE == documentType) ? Constants.EMPTY_STRING
				: getCryptoDocParamById(id, nomeFile, mimeType, documentType);
	}

	/**
	 * Recupera i parametri criptati utilizzati per la Servlet
	 * 'DownloadContentServlet'. {@link DownloadContentServlet}
	 * 
	 * @param id - Identificativo del documento
	 * @return Parametri cryptati.
	 */
	public String getCryptoDocParamById(final String id, final DocumentTypeEnum documentType) {
		try {
			return DownloadContentServlet.getCryptoDocParamById(id, documentType, utente);
		} catch (final Exception e) {
			LOGGER.error("Errore nell'encrypt dei parametri per la servlet 'DownloadContentServlet':", e);
			showErrorMessage("Errore nell'encrypt dei parametri per la servlet 'DownloadContentServlet': " + e.getMessage());
		}

		return "ERROREDICODIFICA";
	}

	/**
	 * Restituisce il cryptoId recuperato dalla Servlet.
	 * 
	 * @see DownloadContentServlet.
	 * @param id
	 * @param nomeFile
	 * @param mimeType
	 * @param documentType
	 * @return cryptoId
	 */
	public String getCryptoDocParamById(final String id, final String nomeFile, final String mimeType, final DocumentTypeEnum documentType) {
		try {
			return DownloadContentServlet.getCryptoDocParamById(id, documentType, utente, nomeFile, mimeType);
		} catch (final Exception e) {
			LOGGER.error("Errore nell'encrypt dei parametri per la servlet 'DownloadContentServlet':", e);
			showErrorMessage("Errore nell'encrypt dei parametri per la servlet 'DownloadContentServlet': " + e.getMessage());
		}

		return "ERROREDICODIFICA";
	}

	/**
	 * Restituisce il cryptoId.
	 */
	@Override
	public String getCryptoId() {
		return this.cryptoId;
	}

	/**
	 * Resetta il cryptoId.
	 */
	public void unsetDetail() {
		this.cryptoId = "";
	}

}
