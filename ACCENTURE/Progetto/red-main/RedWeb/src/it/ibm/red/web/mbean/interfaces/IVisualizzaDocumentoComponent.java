package it.ibm.red.web.mbean.interfaces;

import java.io.Serializable;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.StreamedContent;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;

/**
 * Interfaccia del componente di visualizzazione del content dei documenti
 * 
 * m.crescentini
 *
 */
public interface IVisualizzaDocumentoComponent extends Serializable {
	
	/**
	 * Imposta il dettaglio.
	 * @param dettaglio
	 */
	void setDetail(DetailDocumentRedDTO dettaglio);
	
	/**
	 * Resetta il dettaglio.
	 */
	void unsetDetail();
	
	/**
	 * Metodo per la selezione della riga del documento principale.
	 * @param se
	 */
	void rowSelectorDocPrincipale(SelectEvent se);
	
	/**
	 * Metodo per il download del documento.
	 * @return file
	 */
	StreamedContent downloadDoc();
	
	/**
	 * Ottiene l'attributo docpDTH.
	 * @return docpDTH
	 */
	SimpleDetailDataTableHelper<DetailDocumentRedDTO> getDocpDTH();
	
	/**
	 * Invia il documento per email.
	 * @return file
	 */
	StreamedContent inviaDocEmail();
	
	/**
	 * Apre la stampa del documento.
	 */
	void openStampaDocumento();
	
	/**
	 * Verifica la firma sul documento principale.
	 */ 
	void verificaFirmaDocPrinc();
}
