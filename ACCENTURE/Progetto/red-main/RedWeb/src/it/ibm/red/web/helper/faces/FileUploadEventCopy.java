package it.ibm.red.web.helper.faces;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import org.primefaces.model.UploadedFile;

/**
 * 
 * @author vingenito
 *
 *	
 */
public class FileUploadEventCopy implements UploadedFile, Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Nome del file.
	 */
	private String fileName;

	/**
	 * Contenuti.
	 */
	private byte[] contents;

	/**
	 * Input stream.
	 */
	private transient InputStream inputstream;

	/**
	 * Dimensione.
	 */
	private long size;

	/**
	 * Tipo mime.
	 */
	private String contentType;

	/**
	 * Costruttore della classe.
	 * 
	 * @param file
	 * @throws IOException
	 */
	public FileUploadEventCopy(final UploadedFile file) throws IOException {
		fileName = file.getFileName();
		contents = file.getContents();
		inputstream = file.getInputstream();
		size = file.getSize();
		contentType = file.getContentType();

	}

	/**
	 * @see org.primefaces.model.UploadedFile#getFileName().
	 */
	@Override
	public String getFileName() {
		return fileName;
	}

	/**
	 * Imposta il nome del file.
	 * 
	 * @param fileName
	 */
	public void setFileName(final String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @see org.primefaces.model.UploadedFile#getContents().
	 */
	@Override
	public byte[] getContents() {
		return contents;
	}


	/**
	 * Imposta i contents come byte array.
	 * 
	 * @param contents
	 */
	public void setContents(final byte[] contents) {
		this.contents = contents;
	}

	/**
	 * @see org.primefaces.model.UploadedFile#getInputstream().
	 */
	@Override
	public InputStream getInputstream() {
		return inputstream;
	}

	/**
	 * Imposta l'inputStream.
	 * 
	 * @param inputstream
	 */
	public void setInputstream(final InputStream inputstream) {
		this.inputstream = inputstream;
	}

	/**
	 * @see org.primefaces.model.UploadedFile#getSize().
	 */
	@Override
	public long getSize() {
		return size;
	}

	/**
	 * Restituisce la dimensione.
	 * 
	 * @param size
	 */
	public void setSize(final long size) {
		this.size = size;
	}

	/**
	 * @see org.primefaces.model.UploadedFile#getContentType().
	 */
	@Override
	public String getContentType() {
		return contentType;
	}

	/**
	 * Imposta il content type.
	 * 
	 * @param contentType
	 */
	public void setContentType(final String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @see org.primefaces.model.UploadedFile#write(java.lang.String).
	 */
	@Override
	public void write(final String filePath) throws Exception {
		// Non occorre fare niente in questo metodo.
	}

}