package it.ibm.red.web.document.mbean.component;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.mbean.DocumentManagerBean;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Component dettaglio documenti - buttons laterali.
 */
public class DetailDocumentEastButtonComponent {

    /**
     * Dettaglio.
     */
	private DetailDocumentRedDTO detail;
	
    /**
     * Flag popup visibile.
     */
	private boolean popupDocumentVisible =  false;
	
	/**
	 * DettaglioDocumentoBean.openDocumentPopup.
	 * MenualllegatiDetailModel, responseDetail, codeCount, goToCoda, listaCodeNonCensite.
	 */
	public void openDocumentPopup() {
		final DocumentManagerBean bean = FacesHelper.getManagedBean(Constants.ManagedBeanName.DOCUMENT_MANAGER_BEAN);
		bean.setChiamante(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN);
		bean.setDetail(detail);

		setPopupDocumentVisible(true);
	}

	/**
	 * Restituisce true se il popup document è visibile, false altrimetni.
	 * @return
	 */
	public boolean isPopupDocumentVisible() {
		return popupDocumentVisible;
	}

	/**
	 * Imposta il flag: popupDocumentVisible.
	 * @param popupDocumentVisible
	 */
	public void setPopupDocumentVisible(final boolean popupDocumentVisible) {
		this.popupDocumentVisible = popupDocumentVisible;
	}
}
