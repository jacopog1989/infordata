package it.ibm.red.web.mbean.humantask.component;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections4.CollectionUtils;

import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.web.document.mbean.component.IndiceDiClassificazioneComponent;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.helper.faces.MessageSeverityEnum;

/**
 * Classe PredisponiDsrTabGeneraleComponent.
 */
public class PredisponiDsrTabGeneraleComponent implements Serializable {
	
	/**
	 * Serial version UID 
	 */
	private static final long serialVersionUID = 6490588282765091673L;

	/**
	 * Dettaglio documento 
	 */
	private final DetailDocumentRedDTO detail;
	
	/**
	 * Upload documento 
	 */
	private final UploadOrDownloadDocumentoComponent uploadDocumento;

	/**
	 * Indice di classificazione 
	 */
	private final IndiceDiClassificazioneComponent indiceDiClassificazione;
	
	/**
	 * Costruttore predisponi dsr tab generale component.
	 *
	 * @param inDetail the in detail
	 * @param utente the utente
	 */
	public PredisponiDsrTabGeneraleComponent(final DetailDocumentRedDTO inDetail, final UtenteDTO utente) {
		this.detail = inDetail;
		uploadDocumento = new UploadOrDownloadDocumentoComponent(inDetail, utente);
		indiceDiClassificazione = new IndiceDiClassificazioneComponent(inDetail, utente);
	}
	
	/** 
	 * Metodo che recupera gli allacci del documento
	 */
	public RispostaAllaccioDTO getRispostaAllaccio() {
		final List<RispostaAllaccioDTO> rispostaList = getDetail().getAllacci();
		if (CollectionUtils.isEmpty(rispostaList)) {
			FacesHelper.showMessage(null, "Errore nell'inizializzazione della dichiarazione", MessageSeverityEnum.ERROR);
			return null;
		}
		return rispostaList.get(0);
	}
	
	/** 
	 * Metodo che permette di rcuperare la descrizione dell'assegnatario
	 * @param tipoAssegnazione the tipo assegnazione
	 * @return the descr assegnatario
	 */
	public String getDescrAssegnatario(final TipoAssegnazioneEnum tipoAssegnazione) {
		final List<AssegnazioneDTO> assegnazioneList = detail.getAssegnazioni();
		final Optional<AssegnazioneDTO> assegnazioneOptional = assegnazioneList.stream()
			.filter(a-> a.getTipoAssegnazione() == tipoAssegnazione)
			.findFirst();
		if (!assegnazioneOptional.isPresent()) {
			FacesHelper.showMessage(null, "Errore nell'inizializzazione della dichiarazione", MessageSeverityEnum.ERROR);
			return null;
		}
		return assegnazioneOptional.get().getDescrizioneAssegnatario();
	}
	
	/** 
	 * @return the upload documento
	 */
	public UploadOrDownloadDocumentoComponent getUploadDocumento() {
		return uploadDocumento;
	}

	/** 
	 * @return the indice di classificazione
	 */
	public IndiceDiClassificazioneComponent getIndiceDiClassificazione() {
		return indiceDiClassificazione;
	}

	/** 
	 * @return the detail
	 */
	public DetailDocumentRedDTO getDetail() {
		return detail;
	}
	
}
