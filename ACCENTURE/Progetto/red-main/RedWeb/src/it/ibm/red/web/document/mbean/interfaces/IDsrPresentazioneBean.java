package it.ibm.red.web.document.mbean.interfaces;

import java.io.Serializable;

import it.ibm.red.business.dto.DetailDocumentRedDTO;

/**
 * Interface del Bean presentazione dsr.
 */
public interface IDsrPresentazioneBean extends Serializable {
	
	/**
	 * Ottiene il dettaglio del documento.
	 * @return dettaglio del documento
	 */
	DetailDocumentRedDTO getDetail();
	
	/**
	 * Metodo di onChange.
	 */
	void onChangeOggetto();
}
