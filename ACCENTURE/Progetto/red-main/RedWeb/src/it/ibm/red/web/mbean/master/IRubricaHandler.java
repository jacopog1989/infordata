package it.ibm.red.web.mbean.master;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.enums.RubricaChiamanteEnum;
import it.ibm.red.business.persistence.model.Contatto;

/**
 * Interfaccia handler rubrica.
 */
public interface IRubricaHandler extends Serializable {
	
	/**
	 * @param collection 	La collection di contatti verrà aggiornata con i contatti inseriti/eliminati nella maschera della rubrica
	 * @param salva 		indica se l'utente ha premuto salva oppure ha annullato l'operazione di modifica contatti
	 * @param r 			Serve a distinguere il metodo che ha chiamato la rubrica
	 * 
	 * @return
	 * 	true - Se la collection valutata è risultata accettabile.
	 * 	false - Se la validazione della collection non è andata a buon fine.
	 */
	boolean restituisciRisultati(Collection<Contatto> collection, boolean salva, RubricaChiamanteEnum r);
	
	/**
	 * Implementare per aggiornare l'elemento primefaces alla fine dell'Aggiorna Destinatari 
	 */
	void aggiornaComponentiInterfaccia(RubricaChiamanteEnum r);

}
