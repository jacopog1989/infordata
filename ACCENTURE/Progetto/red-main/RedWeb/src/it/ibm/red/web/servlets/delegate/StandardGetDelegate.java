package it.ibm.red.web.servlets.delegate;

import java.util.List;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.LocalSignContentDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.enums.TrasformazionePDFInErroreEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.web.helper.lsign.LocalSignHelper.DocSignDTO;
import it.ibm.red.web.helper.lsign.LocalSignHelper.TipoDoc;
import it.ibm.red.web.servlets.delegate.exception.DelegateException;

/**
 * Delegate get standard.
 */
public class StandardGetDelegate extends AbstractDelegate implements IGetDelegate {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(StandardGetDelegate.class.getName());
	
	/**
	 * Servizio.
	 */
	private ISignFacadeSRV signSRV;
	
	@Override
	public final LocalSignContentDTO getDocToSignContent(final String signTransactionId, final DocSignDTO doc, final UtenteDTO utente, final SignTypeEnum ste, final boolean firmaMultipla) throws DelegateException {
		LocalSignContentDTO file = null;
		Integer metadatoPdfErrorValue = Constants.Varie.TRASFORMAZIONE_PDF_OK;
		String logId = signTransactionId + doc.getDocumentTitle();
		try {
			signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignFacadeSRV.class);
			
			if (TipoDoc.ALLEGATO.equals(doc.getTipoDoc())) {
				file = signSRV.getAllegato4FirmaLocale(utente, doc.getDocumentTitle(), ste, doc.getSottoCategoriaDocumentoUscita());
				LOGGER.info(logId + " recuperato allegato da Filenet");
			} else {
				file = signSRV.getDocumentoPrincipale4FirmaLocale(utente, doc.getWobNumber(), doc.getDocumentTitle(), ste, doc.getSottoCategoriaDocumentoUscita());
				LOGGER.info(logId + " recuperato documento principale da Filenet");
			}
			
			if (file == null || file.getContent() == null || file.getContent().length==0 || file.getDigest()==null || file.getDigest().length==0) {
				metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.KO_GENERICO.getValue();
				throw new DelegateException("Errore nel recupero del content del documento");
			}
		} catch (final DelegateException e) {
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero del content del documento: " + doc.getDocumentTitle(), e);
			metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.WARN_GENERICO.getValue();
			throw new DelegateException(e);
		} finally {
			if (metadatoPdfErrorValue != Constants.Varie.TRASFORMAZIONE_PDF_OK) {
				aggiornaStatoDocumento(logId, utente.getId(), doc.getIdDocPrincipale(), metadatoPdfErrorValue);
			}
		}
		
		return file;
	}

	@Override
	public final List<AllegatoDTO> getAllegatiToSign(final String signTransactionId, final String documentTitle, final String guid, final UtenteDTO utente, SignTypeEnum ste) throws DelegateException {
		Integer metadatoPdfErrorValue = Constants.Varie.TRASFORMAZIONE_PDF_OK;
		String logId = signTransactionId + documentTitle;
		try {
			signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignFacadeSRV.class);

			List<AllegatoDTO> allegatiToSignByDocPrincipale = signSRV.getAllegatiToSignByDocPrincipale(documentTitle, guid, utente, ste);
			
			LOGGER.info(logId + " recuperati " + allegatiToSignByDocPrincipale.size() + " allegati da firmare da Filenet");
			
			return allegatiToSignByDocPrincipale;
		
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero degli allegati del documento: " + documentTitle, e);
			metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.WARN_GENERICO.getValue();
			throw new DelegateException("Errore nel recupero degli allegati del documento", e);
		} finally {
			if (metadatoPdfErrorValue != Constants.Varie.TRASFORMAZIONE_PDF_OK) {
				aggiornaStatoDocumento(logId, utente.getId(), documentTitle, metadatoPdfErrorValue);
			}
		}
	}

}
