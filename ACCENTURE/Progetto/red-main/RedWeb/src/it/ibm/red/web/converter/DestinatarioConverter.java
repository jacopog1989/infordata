package it.ibm.red.web.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IRubricaSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.mbean.DocumentManagerBean;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Convertere destinatario.
 */
@FacesConverter("destinatarioConverter")
public class DestinatarioConverter implements Converter {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DestinatarioConverter.class);

	/**
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.String).
	 */
	@Override
	public Object getAsObject(final FacesContext fc, final UIComponent uic, final String value) {
		final DestinatarioRedDTO destinatario = new DestinatarioRedDTO();

		try {
			if (StringUtils.isNullOrEmpty(value)) {
				return destinatario;
			}

			// DEVO CONTROLLARE CHE IL DESTINATARIO NON SIA GIA' PRESENTE ALTRIMENTI VIENE
			// RIMPIAZZATO CON UNO NUOVO
			final DocumentManagerBean dmb = FacesHelper.getManagedBean(ConstantsWeb.MBean.DOCUMENT_MANAGER_BEAN);
			if (dmb.getDestinatariInModifica() != null) {
				for (DestinatarioRedDTO de : dmb.getDestinatariInModifica()) {
					if (de == null) {
						de = new DestinatarioRedDTO();
					}
					if (value.equals(de.getIdentificativo())) {
						return de;
					}
				}
			}

			// Se non è già stato inserito devo recuperare il contatto interno o esterno
			final String[] valori = value.split("---");

			if (valori == null || valori.length < 2) {
				return null;
			}

			final TipologiaDestinatarioEnum t = TipologiaDestinatarioEnum.getByTipologia(valori[0]);
			if (TipologiaDestinatarioEnum.ESTERNO == t) {
				final IRubricaSRV rubSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaSRV.class);
				final Long idContatto = getIdContatto(valori);
				Contatto contatto = new Contatto();
				if (idContatto != null) {
					contatto = rubSRV.getContattoByID(idContatto);
				}
				destinatario.setContatto(contatto);
				if (StringUtils.isNullOrEmpty(contatto.getMailSelected())) {
					destinatario.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.CARTACEO);
					destinatario.setMezzoSpedizioneDisabled(true);
				} else {
					destinatario.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.ELETTRONICO);
					destinatario.setMezzoSpedizioneDisabled(false);
				}
			} else if (TipologiaDestinatarioEnum.INTERNO == t) {
				destinatario.setIdNodo(Long.valueOf(valori[1]));
				if (valori.length == 3) {
					destinatario.setIdUtente(Long.valueOf(valori[2]));
				}
			}

			destinatario.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
			destinatario.setTipologiaDestinatarioEnum(t);
			destinatario.setMezzoSpedizioneVisible(TipologiaDestinatarioEnum.ESTERNO == t);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
		}

		return destinatario;
	}

	/**
	 * @param valori
	 * @return
	 */
	private static Long getIdContatto(final String[] valori) {
		Long idContatto;
		try {
			idContatto = Long.parseLong(valori[1]);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			idContatto = null;
		}
		return idContatto;
	}

	/**
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.Object).
	 */
	@Override
	public String getAsString(final FacesContext fc, final UIComponent uic, final Object object) {
		if (object instanceof DestinatarioRedDTO) {
			final DestinatarioRedDTO destinatario = (DestinatarioRedDTO) object;
			return destinatario.getIdentificativo();
		}
		return (object != null) ? object.toString() : null;
	}

}
