package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.NodeExpandEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.enums.ColoreNotaEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipologiaNodoEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractAssegnazioniBean;

/**
 * @author APerquoti
 *
 */
@Named(ConstantsWeb.MBean.ASSEGNA_BEAN)
@ViewScoped
public class AssegnaBean extends AbstractAssegnazioniBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 2499315043581924913L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AssegnaBean.class.getName());

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		init();
	}

	/**
	 * Se sussistono le condizione, si effettua l'assegnazione, altrimenti viene
	 * mostrato un messaggio di errore.
	 */
	public void checkEResponse() {
		if (permessoContinua()) {
			assegnaResponse();
		} else {
			showError("Il campo Nuova Assegnazione deve essere specificato.");
		}
	}

	private void assegnaResponse() {
		Collection<EsitoOperazioneDTO> eOpe = new ArrayList<>();
		final Collection<String> wobNumbers = new ArrayList<>();
		try {
			// Pulisco gli esiti
			getLdb().cleanEsiti();

			// Raccolgo i WOB number necessari ad invocare il service
			for (final MasterDocumentRedDTO m : getMasters()) {
				if (!StringUtils.isNullOrEmpty(m.getWobNumber())) {
					wobNumbers.add(m.getWobNumber());
				}
			}

			Long nuovoUtente = getSelected().getIdUtente();
			// Eseguo un controllo perche nel caso della selezione di un ufficio il sistema
			// si aspetta un idUtente a '0'
			if (getSelected().getIdUtente() == null) {
				nuovoUtente = 0L;
			}

			// Invoke service
			eOpe = getRiassegnaSRV().riassegna(getUtente(), wobNumbers, ResponsesRedEnum.ASSEGNA, getSelected().getIdNodo(), nuovoUtente, getMotivoAssegnazione());

			// scrittura sullo storico note
			if (isAlsoStorico() && (!getLdb().getEsitiOperazione().isEmpty() && getLdb().checkEsiti(false))) {
				for (final MasterDocumentRedDTO m : getMasters()) {
					getNotaSRV().registraNotaFromDocumento(getUtente(), Integer.parseInt(m.getDocumentTitle()), ColoreNotaEnum.NERO, getMotivoAssegnazione());
				}
			}

			// Imposto gli esiti con i nuovi e eseguo un refresh
			getLdb().getEsitiOperazione().addAll(eOpe);
			// Aggiorno la dialog contenente gli esiti
			FacesHelper.update("centralSectionForm:idDlgShowResult");
			FacesHelper.executeJS("PF('dlgGeneric').hide();PF('dlgShowResult').show();");
			getLdb().destroyBeanViewScope(MBean.ASSEGNA_BEAN);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il tentato sollecito della response 'ASSEGNA'", e);
			showError("Si è verificato un errore durante il tentato sollecito della response 'ASSEGNA'");
		}
	}

	/**
	 * Effettua un clear delle assegnazioni.
	 */
	@Override
	public void clearAssegnazione() {
		setDescrizioneNewAssegnazione("");
		setSelected(null);

	}

	/**
	 * Effettua il load dell'organigramma.
	 */
	@Override
	protected void loadRootOrganigramma() {
		try {
			// creazione della struttura del tree
			setRoot(new DefaultTreeNode("Root", null));
			getRoot().setSelectable(true);

			// imposto un DTO utile per creare la struttura del tree
			final NodoOrganigrammaDTO padre = new NodoOrganigrammaDTO();
			padre.setIdAOO(getUtente().getIdAoo());
			padre.setIdNodo(getUtente().getIdUfficio());
			padre.setCodiceAOO(getUtente().getCodiceAoo());
			padre.setUtentiVisible(true);
			padre.setDescrizioneNodo(getUtente().getNodoDesc());

			// creazione del nodo padre necessario (ufficio dell'utente in sessione)
			final TreeNode nodoPadre = new DefaultTreeNode(padre, getRoot());
			nodoPadre.setExpanded(true);
			nodoPadre.setSelectable(false);
			nodoPadre.setType("UFF");

			final List<NodoOrganigrammaDTO> primoLivello = getOrgSRV().getFigliAlberoAssegnatarioPerAssegna(getUtente().getIdUfficio(), padre);

			// <-------- Gestione Primo Livello ------------>
			// per ogni nodo di primo livello creo il nodo da mettere nell'albero che ha
			// come padre rootAssegnazionePerCompetenza
			TreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(getUtente().getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, nodoPadre);
				nodoToAdd.setExpanded(false);
				nodoToAdd.setSelectable(true);
				nodoToAdd.setType("UFF");
				if (n.getTipoNodo().equals(TipologiaNodoEnum.UTENTE)) {
					nodoToAdd.setType("USER");
				} else {
					n.setFigli(1);
					n.setUtentiVisible(true);
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
				}
				nodoPadre.getChildren().add(nodoToAdd);
			}
			// <-------- Fine Gestione ------------>

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Definisce le azioni che devono essere eseguite per la corretta
	 * visualizzazione del content del nodo.
	 * 
	 * @param event
	 */
	public void onOpenNode(final NodeExpandEvent event) {
		// metodo triggerato dall'evento expand del tree node
		try {

			// <-------- Gestione Secondo Livello ------------>
			// dall'evento ottengo il nodo e il DTO che lo compone
			final NodoOrganigrammaDTO nodoPadre = (NodoOrganigrammaDTO) event.getTreeNode().getData();
			// tramite il dto recupero tutti i filgi di questo nodo, che in questo caso
			// specifico sono tutti utenti
			final List<NodoOrganigrammaDTO> figli = getOrgSRV().getFigliAlberoAssegnatarioPerAssegna(getUtente().getIdUfficio(), nodoPadre);
			TreeNode nodoFiglio = null;
			// pulisco eventuali figli nel nodo
			event.getTreeNode().getChildren().clear();
			for (final NodoOrganigrammaDTO figlio : figli) {
				figlio.setCodiceAOO(getUtente().getCodiceAoo());
				// con ogni figlio ottenuto creo un nuovo nodo(utente) da assegnare al padre
				nodoFiglio = new DefaultTreeNode(figlio, event.getTreeNode());
				nodoFiglio.setSelectable(true);
				nodoFiglio.setType("UFF");
				// controllo se è un utente e nel caso setto il type
				if (figlio.getTipoNodo().equals(TipologiaNodoEnum.UTENTE)) {
					nodoFiglio.setType("USER");
				} else {
					figlio.setFigli(1);
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoFiglio);
				}
			}
			// <-------- Fine Gestione ------------>

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}
}
