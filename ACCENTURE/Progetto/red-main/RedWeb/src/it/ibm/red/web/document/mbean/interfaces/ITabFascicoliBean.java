package it.ibm.red.web.document.mbean.interfaces;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.FascicoloDTO;

/**
 * Interfaccia del bean che gestisce il tab fascicoli.
 */
public interface ITabFascicoliBean extends Serializable {
	
	/**
	 * Rimuove il fascicolo.
	 * @param index
	 */
	void rimuoviFascicolo(Object index);

	/**
	 * Ottiene i fascicoli.
	 * @return lista di fascicoli
	 */
	List<FascicoloDTO> getFascicoli();
	
	/**
	 * Imposta i fascicoli.
	 * @param fascicoli
	 */
	void setFascicoli(List<FascicoloDTO> fascicoli);
	
	/**
	 * Ottiene il dettaglio del documento.
	 * @return dettaglio del documento
	 */
	DetailDocumentRedDTO getDetail();
}
