package it.ibm.red.web.servlets.delegate;

import java.util.List;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.LocalSignContentDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.enums.TrasformazionePDFInErroreEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.web.helper.lsign.LocalSignHelper.DocSignDTO;
import it.ibm.red.web.helper.lsign.LocalSignHelper.TipoDoc;
import it.ibm.red.web.servlets.delegate.exception.DelegateException;

/**
 * Delegate get copia conforme.
 */
public class CopiaConformeGetDelegate extends AbstractDelegate implements IGetDelegate {
	
    /**
     * Logger.
     */
	private static final REDLogger LOGGER = REDLogger.getLogger(CopiaConformeGetDelegate.class.getName());
	
    /**
     * Service firma.
     */
	private ISignFacadeSRV signSRV;
	
	
	@Override
	public final LocalSignContentDTO getDocToSignContent(final String signTransactionId, final DocSignDTO doc, final UtenteDTO utente, final SignTypeEnum ste, final boolean firmaMultipla) throws DelegateException {
		LocalSignContentDTO file = null;
		Integer metadatoPdfErrorValue = Constants.Varie.TRASFORMAZIONE_PDF_OK;
		String logId = signTransactionId + doc.getDocumentTitle();
		try {
			signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignFacadeSRV.class);
			
			if (TipoDoc.ALLEGATO.equals(doc.getTipoDoc())) {
				file = signSRV.getAllegato4CopiaConforme(utente, doc.getDocumentTitle());
				LOGGER.info(logId + " recuperato allegato da Filenet");
			} else {
				throw new DelegateException("Impossibile attestare per copia conforme un documento principale");
			}
			
			if (file == null || file.getContent() == null || file.getContent().length==0 || file.getDigest()==null || file.getDigest().length==0) {
				metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.KO_GENERICO.getValue();
				throw new DelegateException("Errore nel recupero del content del documento");
			}
		} catch (final DelegateException e) {
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero del content del documento: " + doc.getDocumentTitle(), e);
			metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.WARN_GENERICO.getValue();
			throw new DelegateException(e);
		} finally {
			if (metadatoPdfErrorValue != Constants.Varie.TRASFORMAZIONE_PDF_OK) {
				aggiornaStatoDocumento(logId, utente.getId(), doc.getIdDocPrincipale(), metadatoPdfErrorValue);
			}
		}
		
		return file;
	}

	@Override
	public final List<AllegatoDTO> getAllegatiToSign(final String signTransactionId, final String documentTitle, final String guid, final UtenteDTO utente, SignTypeEnum ste) throws DelegateException {
		Integer metadatoPdfErrorValue = Constants.Varie.TRASFORMAZIONE_PDF_OK;
		String logId = signTransactionId + documentTitle;
		try {
			signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignFacadeSRV.class);

			List<AllegatoDTO> allegatiToSignCopiaConformeByDocPrincipale = signSRV.getAllegatiToSignCopiaConformeByDocPrincipale(documentTitle, guid, utente);
			
			LOGGER.info(logId + " recuperati " + allegatiToSignCopiaConformeByDocPrincipale.size() + " allegati da firmare da Filenet");
			
			return allegatiToSignCopiaConformeByDocPrincipale;
			
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero degli allegati del documento " + documentTitle, e);
			metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.WARN_GENERICO.getValue();
			throw new DelegateException("Errore nel recupero degli allegati del documento", e);
		} finally {
			if (metadatoPdfErrorValue != Constants.Varie.TRASFORMAZIONE_PDF_OK) {
				aggiornaStatoDocumento(logId, utente.getId(), documentTitle, metadatoPdfErrorValue);
			}
		}
	}

}
