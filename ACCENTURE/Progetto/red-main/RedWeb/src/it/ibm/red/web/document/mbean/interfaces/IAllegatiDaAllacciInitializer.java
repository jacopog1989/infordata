package it.ibm.red.web.document.mbean.interfaces;

/**
 * Implementato dal parent di {@link IAllegatiDaAllacci}
 * 
 * Gestisce il ciclo di vita di allegati e la eventuale selezione dell'utente.
 *
 */
public interface IAllegatiDaAllacciInitializer {

	/**
	 * Component per gestire gli allegati da allacci. 
	 * Quando il componente e' nullo non ci sono allacci da gestire.
	 */
	IAllegatiDaAllacci getAllegatiDaAllacci();
	
	/**
	 * Aprire il div degli allegati da allacci.
	 * Metodo che permette di aprire il div e inizializza effettivamente {@link IAllegatiDaAllacci}.
	 */
	void openAllegatiDaAllacci();
	
	/**
	 * Registra il tab allegati da allacci.
	 * Eventualmente prende la selezione dell'utente e flusha le informazioni nell'interfaccia degli allegati.
	 */
	void registraAllegatiDaAllacci();
	
	/**
	 * Pulisce tutti gli oggetti non necessari.
	 */
	void closeAllegatiDaAllacci();
	
	/**
	 * Se non ci sono allacci non può essere initializzato {@link IAllegatiDaAllacci}
	 */
	boolean isAllacciExist();
}
