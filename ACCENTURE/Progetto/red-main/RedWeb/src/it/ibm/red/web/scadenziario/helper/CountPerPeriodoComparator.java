package it.ibm.red.web.scadenziario.helper;

import java.util.Comparator;

import it.ibm.red.business.dto.CountPer;
import it.ibm.red.business.enums.PeriodoScadenzaEnum;

/**
 * Comparator count per periodo.
 */
public class CountPerPeriodoComparator implements Comparator<CountPer<PeriodoScadenzaEnum>> {
	
	/**
	 * Override del compare per la classe.
	 * @param o1
	 * @param o2
	 * @return risultato della compare
	 */
	@Override
	public int compare(final CountPer<PeriodoScadenzaEnum> o1, final CountPer<PeriodoScadenzaEnum> o2) {
		final PeriodoScadenzaEnum periodo1 = o1.getData();
		final PeriodoScadenzaEnum periodo2 = o2.getData();
		return periodo1.compareTo(periodo2);
	}
}
