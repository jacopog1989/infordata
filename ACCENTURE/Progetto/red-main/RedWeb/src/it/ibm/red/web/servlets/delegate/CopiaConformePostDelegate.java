package it.ibm.red.web.servlets.delegate;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.enums.TrasformazionePDFInErroreEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.ISignSRV;
import it.ibm.red.business.service.facade.IASignFacadeSRV;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.web.helper.lsign.LocalSignHelper.DocSignDTO;
import it.ibm.red.web.helper.lsign.LocalSignHelper.TipoDoc;
import it.ibm.red.web.servlets.delegate.exception.DelegateException;

/**
 * Delegate post copia conforme.
 */
public class CopiaConformePostDelegate extends AbstractPostDelegate {

	private static final String FIRMA_DEL_DOCUMENTO = "Firma del documento: ";
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CopiaConformePostDelegate.class.getName());

	@Override
	public final EsitoOperazioneDTO manageSignedDoc(final String signTransactionId, final DocSignDTO doc, final byte[] docSigned, final UtenteDTO utente, final SignTypeEnum ste, final boolean isLastDocument,
			final boolean isFirmaMultipla) throws DelegateException {
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(doc.getWobNumber(), doc.getDocumentTitle());
		esito.setIdDocumento(doc.getNumeroDocumentoPrincipale());
		esito.setNomeDocumento(doc.getNomeFile());
		Integer metadatoPdfErrorValue = Constants.Varie.TRASFORMAZIONE_PDF_OK;
		String logId = signTransactionId + doc.getDocumentTitle();
		try {
			final ISignFacadeSRV signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignSRV.class);
			final IASignFacadeSRV aSignSRV = ApplicationContextProvider.getApplicationContext().getBean(IASignFacadeSRV.class);
			
			// Gestione del content firmato (merge del content stampigliato con quello firmato e con la postilla della Copia Conforme)
			byte[] contentToPersist = signSRV.manageSignedContent(utente, ste, doc.getContentStampigliato(), docSigned, doc.getDataFirma(), doc.getDocumentTitle(), 
					TipoDoc.PRINCIPALE.equals(doc.getTipoDoc()), true);
			LOGGER.info(logId + " => eseguita la gestione del content firmato per Copia Conforme");
			
			// Inserimento del documento firmato nella tabella di bufferizzazione
			aSignSRV.insertIntoSignedDocsBuffer(doc.getWobNumber(), doc.getGuid(), contentToPersist, TipoDoc.PRINCIPALE.equals(doc.getTipoDoc()));
			LOGGER.info(logId + " => eseguito l'inserimento nella tabella di bufferizzazione");
			
			esito.setEsito(true);
			esito.setCodiceErrore(null);
			esito.setNote(null);
			
			// Se è l'ultimo documento associato a documentTitlePrincipale, finalizza il processo processo di firma
			if (isLastDocument) {
				aSignSRV.postFirmaLocale(logId, esito, utente, ste, true);
				LOGGER.info(logId + " => eseguita la finalizzazione della firma locale per Copia Conforme con esito: " 
						+ esito.isEsito());
				
				if (!esito.isEsito()) {
					throw new RedException("Errore durante la finalizzazione della firma locale per Copia Conforme");
				}
			} else {
				esito.setEsito(true);
			}
		} catch (final Exception e) {
			LOGGER.error(FIRMA_DEL_DOCUMENTO + doc.getDocumentTitle() + " => errore in fase di gestione del documento firmato per Copia Conforme: " + e.getMessage(), e);
			metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.KO_GENERICO.getValue();
			throw new DelegateException("Errore in fase di gestione del documento firmato", e);
		} finally {
			aggiornaStatoDocumento(logId, utente.getId(), doc.getIdDocPrincipale(), metadatoPdfErrorValue);
		}

		return esito;
	}

}