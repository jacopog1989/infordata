package it.ibm.red.web.mbean;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.model.DualListModel;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.persistence.model.Widget;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IDashboardSRV;
import it.ibm.red.business.service.facade.IDashboardFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.enums.WidgetEnum;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Bean per la gestione della dashboard.
 */
@Named(ConstantsWeb.MBean.DASHBOARD_BEAN)
@ViewScoped
public class DashboardBean extends AbstractBean {

	/**
	 * Seriale.
	 */
	private static final long serialVersionUID = 2101613113729509288L;

	/**
	 * Servizio dashboard.
	 */
	private IDashboardFacadeSRV dashboardSRV;

	/**
	 * Bean di sessione.
	 */
	private SessionBean sessionBean;

	/**
	 * Info utente.
	 */
	private UtenteDTO utente;

	/**
	 * Componente selezione widget.
	 */
	private DualListModel<WidgetEnum> widgets;

	/**
	 * Lista widget.
	 */
	private List<WidgetEnum> wdgEnum;

	/**
	 * Lista widget iniziale.
	 */
	private List<WidgetEnum> wdgEnumSource;

	/**
	 * Widget per ruolo.
	 */
	private List<Widget> widgetRuolo;

	/**
	 * Flag eliminabile.
	 */
	private boolean isEliminabile;

	/**
	 * Componente selezione widget.
	 */
	private Map<WidgetEnum, Boolean> widgetsEliminabili;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		sessionBean = (SessionBean) FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		sessionBean.setRenderRubrica(false);
		sessionBean.setFromDashboard(true);
		dashboardSRV = ApplicationContextProvider.getApplicationContext().getBean(IDashboardSRV.class);
		wdgEnum = new ArrayList<>();
		wdgEnumSource = new ArrayList<>();
		widgets = new DualListModel<>();
		sessionBean.countMenuAttivita(false);
		loadWidgets();
	}

	/**
	 * Salva le preferenze dei widget.
	 * 
	 * @return url della pagina relativa alla Dashboard
	 */
	public String salva() {
		wdgEnum.clear();
		// elementi della source list
		rimuoviPreferenzeWdgSourceList();
		// elementi della target list
		salvaPreferenzeWdgTargetList();

		return sessionBean.gotoDashboard();
	}

	/**
	 * Rimuove le preferenze dell'utente.
	 */
	public void rimuoviPreferenzeWdgSourceList() {
		final List<Long> wdgSourceListLong = new ArrayList<>();

		for (int i = 0; i < widgets.getSource().size(); i++) {
			wdgEnumSource.add(getDeselectedWidget(i));
			final Long idWdgSource = Long.parseLong(wdgEnumSource.get(i).getPk());
			wdgSourceListLong.add(idWdgSource);
		}
		dashboardSRV.rimuoviPreferenze(wdgSourceListLong, utente.getId());
	}

	/**
	 * Salva le preferenze dell'utente.
	 */
	public void salvaPreferenzeWdgTargetList() {
		// elementi della target list
		final List<Long> wdgTargetListLong = new ArrayList<>();
		for (int i = 0; i < widgets.getTarget().size(); i++) {
			wdgEnum.add(getSelectedWidget(i));
			final Long idWdg = Long.parseLong(wdgEnum.get(i).getPk());
			wdgTargetListLong.add(idWdg);
		}
		dashboardSRV.salvaPreferenzeWdg(wdgTargetListLong, utente.getId(), utente.getIdRuolo());

	}

	/**
	 * Carica i widget scelti dall'utente e popola la picklist con i widget
	 * disponibili.
	 */
	public void loadWidgets() {
		WidgetEnum wdgEnumVar;
		List<Widget> loadWidget = null;

		// Metodo per mostrare i widget scelti dall'utente
		loadWidget = dashboardSRV.getWidgetPreferenze(utente.getId(), utente.getIdRuolo());

		for (final Widget wdg : loadWidget) {
			final String pk = Integer.toString(wdg.getIdWdg());
			wdgEnumVar = WidgetEnum.get(pk);
			wdgEnum.add(wdgEnumVar);

			FacesHelper.getManagedBean(wdgEnumVar.getBeanName());

		}
		// metodo utile a popolare la picklist con i widget disponibili
		caricaListeInBaseAlRuolo(utente.getIdRuolo(), wdgEnum);

		for (final WidgetEnum targ : widgets.getTarget()) {
			if (!wdgEnum.contains(targ)) {
				wdgEnum.add(targ);
			}
		}

		FacesHelper.executeJS("showWidgets()");
	}

	/**
	 * Carica la lista di tutti i widget disponibili per un determinato ruolo.
	 * Gestisce l'ordinamento degli stessi in base alle preferenze e imposta le
	 * proprietà dei widget.
	 * 
	 * @param idRuolo
	 * @param wdgEnumPref
	 */
	public void caricaListeInBaseAlRuolo(final Long idRuolo, final List<WidgetEnum> wdgEnumPref) {
		final List<WidgetEnum> widgetTarget = new ArrayList<>();
		final List<WidgetEnum> widgetSource = new ArrayList<>();

		// otteniamo tutti i widget disponibili per un determinato ruolo
		widgetRuolo = dashboardSRV.getWidgetRuolo(idRuolo);

		widgetsEliminabili = new EnumMap<>(WidgetEnum.class);
		for (final Widget wdg : widgetRuolo) {
			final WidgetEnum wdgEnumPk = WidgetEnum.get(String.valueOf(wdg.getIdWdg()));

			if (wdgEnumPk != null) {
				final Integer x = Integer.parseInt(wdgEnumPk.getPk());
				// controllo che i widget per quel ruolo non siano eliminabili e in tal caso li
				// disabilito
				// controllo poi l'ordinamento che deve essere quello espresso dalle preferenze
				if (wdg.getEliminabile()) {
					widgetsEliminabili.put(wdgEnumPk, true);
					if ((x.equals(wdg.getIdWdg()) || wdgEnumPref.contains(wdgEnumPk))) {
						widgetTarget.add(wdgEnumPk);
					}
				} else {
					widgetsEliminabili.put(wdgEnumPk, false);
					widgetEliminabiliPerRuolo(wdgEnumPref, widgetTarget, widgetSource, wdg, wdgEnumPk, x);
				}
			}

			widgets.setSource(widgetSource);
			widgets.setTarget(widgetTarget);
		}
	}

	/**
	 * Imposta il flag "eliminabile" a true per tutti i widget eliminabili associati
	 * ad uno specifico ruolo.
	 * 
	 * @param wdgEnumPref
	 * @param widgetTarget
	 * @param widgetSource
	 * @param wdg
	 * @param wdgEnum
	 * @param pk
	 * @param isEliminabile
	 */
	public void widgetEliminabiliPerRuolo(final List<WidgetEnum> wdgEnumPref, final List<WidgetEnum> widgetTarget, final List<WidgetEnum> widgetSource, final Widget wdg,
			final WidgetEnum wdgEnum, final Integer pk) {
		if ((pk.equals(wdg.getIdWdg()) && wdgEnumPref.contains(wdgEnum))) {
			widgetTarget.add(wdgEnum);
		} else if (pk.equals(wdg.getIdWdg())) {
			widgetSource.add(wdgEnum);
		}
	}

	/**
	 * Restituisce l'enum associata al Widget selezionato.
	 * 
	 * @param index del widget selezionato
	 * @return WidgetEnum
	 */
	public WidgetEnum getSelectedWidget(final Integer index) {
		WidgetEnum out = null;
		if (widgets.getTarget().size() > index) {
			final Object widget = widgets.getTarget().get(index);
			if (widget instanceof WidgetEnum) {
				out = (WidgetEnum) widget;
			} else {
				final String pk = (String) widget;
				out = WidgetEnum.get(pk);
			}
		}
		return out;
	}

	/**
	 * Retituisce l'enum associata al Widget deselezionato.
	 * 
	 * @param index del widget selezionato
	 * @return WidgetEnum
	 */
	public WidgetEnum getDeselectedWidget(final Integer index) {
		WidgetEnum in = null;
		if (widgets.getSource().size() > index) {
			final Object widget = widgets.getSource().get(index);
			if (widget instanceof WidgetEnum) {
				in = (WidgetEnum) widget;
			} else {
				final String pk = (String) widget;
				in = WidgetEnum.get(pk);
			}
		}
		return in;
	}

	/**
	 * Restituisce la lista dei WidgetEnum.
	 * 
	 * @return List di wdgEnum
	 */
	public List<WidgetEnum> getWdgEnum() {
		return wdgEnum;
	}

	/**
	 * Imposta la lista dei WidgetEnum.
	 * 
	 * @param wdgEnum
	 */
	public void setWdgEnum(final List<WidgetEnum> wdgEnum) {
		this.wdgEnum = wdgEnum;
	}

	/**
	 * Restituisce la DualListModel dei WidgetEnum.
	 * 
	 * @return DualListModel di WidgetEnum
	 */
	public DualListModel<WidgetEnum> getWidgets() {
		return widgets;
	}

	/**
	 * Imposta la DualListModel di WidgetEnum.
	 * 
	 * @param widgets
	 */
	public void setWidgets(final DualListModel<WidgetEnum> widgets) {
		this.widgets = widgets;
	}

	/**
	 * Restituisce la lista dei Widget per il ruolo.
	 * 
	 * @return widgetRuolo come List di Widget
	 */
	public List<Widget> getWidgetRuolo() {
		return widgetRuolo;
	}

	/**
	 * Imposta la lista di Widget.
	 * 
	 * @param widgetRuolo
	 */
	public void setWidgetRuolo(final List<Widget> widgetRuolo) {
		this.widgetRuolo = widgetRuolo;
	}

	/**
	 * @return true se il widget è eliminabile, false altrimenti
	 */
	public boolean getIsEliminabile() {
		return isEliminabile;
	}

	/**
	 * Imposta il flag isEliminabile.
	 * 
	 * @param isEliminabile
	 */
	public void setIsEliminabile(final boolean isEliminabile) {
		this.isEliminabile = isEliminabile;
	}

	/**
	 * Get Mappa per il widget disattivabile
	 * 
	 * @return EnumMap
	 */
	public Map<WidgetEnum, Boolean> getWidgetsEliminabili() {
		return widgetsEliminabili;
	}

	/**
	 * Set Mappa per il widget disattivabile
	 * 
	 * @param EnumMap
	 */
	public void setWidgetsEliminabili(final Map<WidgetEnum, Boolean> widgetsEliminabili) {
		this.widgetsEliminabili = widgetsEliminabili;
	}

}
