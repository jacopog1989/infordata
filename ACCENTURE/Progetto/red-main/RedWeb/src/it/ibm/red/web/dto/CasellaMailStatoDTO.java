/**
 * 
 */
package it.ibm.red.web.dto;

import java.util.List;

/**
 * @author APerquoti
 *
 */
public class CasellaMailStatoDTO extends AbstractDTO {

	/**
	 * UID.
	 */
	private static final long serialVersionUID = 2079154358430228178L;

	/**
	 * Casella mail.
	 */
	private final String casellaMail;
	
	/**
	 * Stato mail.
	 */
	private Integer statoMails;
	
	/**
	 * Counter mail.
	 */
	private Integer counterMail;

	/**
	 * Costruttore del DTO.
	 * @param casellaSelected
	 */
	public CasellaMailStatoDTO(final List<String> casellaSelected) {
		super();
		casellaMail = casellaSelected.get(0);
		statoMails = Integer.valueOf(casellaSelected.get(1));
		counterMail = Integer.valueOf(casellaSelected.get(3));
	}

	/**
	 * @return the casellaMail
	 */
	public final String getCasellaMail() {
		return casellaMail;
	}

	/**
	 * @return the statoMails
	 */
	public final Integer getStatoMails() {
		return statoMails;
	}

	/**
	 * Imposta lo stato delle mail.
	 * @param statoMails
	 */
	public void setStatoMails(final Integer statoMails) {
		this.statoMails = statoMails;
	}

	/**
	 * Restituisce il contatore delle mail.
	 * @return counterMail
	 */
	public Integer getCounterMail() {
		return counterMail;
	}

	/**
	 * Imposta il contatore delle mail.
	 * @param counterMail
	 */
	public void setCounterMail(final Integer counterMail) {
		this.counterMail = counterMail;
	}
}
