package it.ibm.red.web.mbean;


import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IFaldoneFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.dto.TreeNodeFaldoneDTO;
import it.ibm.red.web.enums.ModeFaldonaturaEnum;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.master.IFaldoneHandler;

/**
 * Bean faldonatura.
 */
@Named(ConstantsWeb.MBean.FALDONATURA_BEAN)
@ViewScoped
public class FaldonaturaBean extends AbstractBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 8142408892313236956L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FaldonaturaBean.class.getName());

    /**
     * Bean name.
     */
	private String beanMasterName;

	/**
	 * Modalità faldonatura.
	 */
	private ModeFaldonaturaEnum mode;

	/**
	 * UTente.
	 */
	private UtenteDTO utente;

	/**
	 * Flag duplicazioni.
	 */
	private Boolean canBeDuplicate;


	/**
	 * Faldone padre.
	 */
	private String faldonePadre;

	/**
	 * Oggetto.
	 */
	private String faldoneOggetto;

	/**
	 * Descrizione.
	 */
	private String faldoneDescrizione;

	/**
	 * Servizio.
	 */
	private IFaldoneFacadeSRV faldoneSRV;

	/**
	 * Faldone selezionato.
	 */
	private TreeNodeFaldoneDTO faldoneSelezionato;

	/**
	 * Radice albero.
	 */
	private transient TreeNode treeNode;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		faldoneSRV = ApplicationContextProvider.getApplicationContext().getBean(IFaldoneFacadeSRV.class);
	}

	/**
	 * Imposta i parametri del faldone selezionato in creazione dal tree nei parametri 
	 * {@link #faldoneOggetto}, {@link #faldoneDescrizione}, {@link #faldonePadre}.
	 * @param beanMasterName
	 * @param mode
	 * @param treeNode
	 */
	public void setFaldoneSelezionatoCreazione(final String beanMasterName, final ModeFaldonaturaEnum mode, final TreeNode treeNode) {
		final TreeNodeFaldoneDTO treeNodeFaldoneDTO = (TreeNodeFaldoneDTO) treeNode.getData();
		this.beanMasterName = beanMasterName;
		this.treeNode = treeNode;
		faldoneSelezionato = treeNodeFaldoneDTO;
		this.mode = mode;
		if (treeNode.getRowKey() != null && !"0".equals(treeNode.getRowKey())) {
			faldonePadre = treeNodeFaldoneDTO.getName();
		}
		else {
			faldonePadre="";
		}
		
		faldoneOggetto = null;
		faldoneDescrizione = null;
		if (!mode.equals(ModeFaldonaturaEnum.CREATE)) {
			faldoneOggetto = treeNodeFaldoneDTO.getName();
			faldoneDescrizione = treeNodeFaldoneDTO.getDescrizione();
		}
	}

	/**
	 * Imposta i parametri del faldone selezionato in modifica dal tree nei parametri 
	 * {@link #faldoneOggetto}, {@link #faldoneDescrizione}, {@link #faldonePadre}.
	 * @param beanMasterName
	 * @param mode
	 * @param treeNode
	 */
	public void setFaldoneSelezionatoModifica(final String beanMasterName, final ModeFaldonaturaEnum mode, final TreeNode treeNode) {
		final TreeNodeFaldoneDTO treeNodeFaldoneDTO = (TreeNodeFaldoneDTO) treeNode.getData();
		this.beanMasterName = beanMasterName;
		this.treeNode = treeNode;
		faldoneSelezionato = treeNodeFaldoneDTO;
		this.mode = mode;
		final TreeNode parentNode=treeNode.getParent();
		if (parentNode!=null && parentNode.getRowKey()!=null && !"0".equals(parentNode.getRowKey())) {
			final TreeNodeFaldoneDTO parentFaldoneDTO = (TreeNodeFaldoneDTO) parentNode.getData();
			faldonePadre = parentFaldoneDTO.getName();
		}
		else {
			faldonePadre="";
		}
		
		faldoneOggetto = null;
		faldoneDescrizione = null;
		if (!mode.equals(ModeFaldonaturaEnum.CREATE)) {
			faldoneOggetto = treeNodeFaldoneDTO.getName();
			faldoneDescrizione = treeNodeFaldoneDTO.getDescrizione();
		}
	}

	/**
	 * Crea un faldone mostrando l'esito all'utente.
	 */
	public void creaFaldone() {
		try {
			if (StringUtils.isNullOrEmpty(faldoneOggetto)) {
				showWarnMessage("L'oggetto è obbligatorio.");
				return;
			}
			
			//il faldone corrente sarà il parent del nuovo
			String parent = faldoneSelezionato.getDocumentTitle();
			if (treeNode.getParent().getParent() == null) {
				//Sei il figlio della radice, ovvero un ufficio, quindi in realtà il faldone padre è inesistente
				parent = null;
			}
			faldoneSRV.createFaldone(utente, faldoneOggetto, faldoneDescrizione, parent, canBeDuplicate);
			showInfoMessage("Faldone creato correttamente.");
			faldonaturaDone();
			FacesHelper.executeJS("PF('dlgFaldonatura_WV').hide()");
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	private void faldonaturaDone() {
		final IFaldoneHandler masterFaldone = (IFaldoneHandler) FacesHelper.getManagedBean(beanMasterName);
		masterFaldone.gestisciOperazioneFaldone(mode);
	}

	/**
	 * Gestisce la modifica del faldone.
	 */
	public void modificaFaldone() {
		try {
			if (StringUtils.isNullOrEmpty(faldoneOggetto)) {
				showWarnMessage("L'oggetto è obbligatorio.");
				return;
			}
			faldoneSRV.updateFaldone(utente, faldoneSelezionato.getDocumentTitle(), faldoneOggetto, faldoneDescrizione, canBeDuplicate);
			showInfoMessage("Faldone modificato correttamente.");
			faldonaturaDone();
			FacesHelper.executeJS("PF('dlgFaldonatura_WV').hide()");
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Gestisce l'eliminazione del faldone.
	 */
	public void eliminaFaldone() {
		try {
			faldoneSRV.delete(utente, faldoneSelezionato.getIdUfficio(),  faldoneSelezionato.getAbsolutePathFaldone(), faldoneSelezionato.getDocumentTitle());
			showInfoMessage("Faldone eliminato correttamente.");
			faldonaturaDone();
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Restituisce il faldone padre.
	 * @return faldone padre
	 */
	public String getFaldonePadre() {
		return faldonePadre;
	}

	/**
	 * Imposta il faldone padre.
	 * @param faldonePadre
	 */
	public void setFaldonePadre(final String faldonePadre) {
		this.faldonePadre = faldonePadre;
	}

	/**
	 * Restituisce l'oggetto del faldone.
	 * @return oggetto faldone
	 */
	public String getFaldoneOggetto() {
		return faldoneOggetto;
	}

	/**
	 * Imposta l'oggetto del faldone.
	 * @param faldoneOggetto
	 */
	public void setFaldoneOggetto(final String faldoneOggetto) {
		this.faldoneOggetto = faldoneOggetto;
	}

	/**
	 * Restituisce la descrizione del faldone.
	 * @return descrizione faldone
	 */
	public String getFaldoneDescrizione() {
		return faldoneDescrizione;
	}

	/**
	 * Imposta la descrizione del faldone.
	 * @param faldoneDescrizione
	 */
	public void setFaldoneDescrizione(final String faldoneDescrizione) {
		this.faldoneDescrizione = faldoneDescrizione;
	}

	/**
	 * Restituisce true se puo essere duplicato, false altrimenti.
	 * @return true se duplicabile, false altrimenti
	 */
	public Boolean getCanBeDuplicate() {
		return canBeDuplicate;
	}

	/**
	 * @param canBeDuplicate
	 */
	public void setCanBeDuplicate(final Boolean canBeDuplicate) {
		this.canBeDuplicate = canBeDuplicate;
	}

	/**
	 * Restituisce la modalita di faldonatura.
	 * @return modalita
	 */
	public ModeFaldonaturaEnum getMode() {
		return mode;
	}

	/**
	 * Restituisce il treeNode.
	 * @return treeNode
	 */
	public TreeNode getTreeNode() {
		return treeNode;
	}

}