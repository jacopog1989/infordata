/**
 * 
 */
package it.ibm.red.web.dto;

import java.util.Date;
import java.util.List;

import org.primefaces.model.DefaultScheduleEvent;

import it.ibm.red.business.dto.EventoDTO;
import it.ibm.red.business.enums.EventoCalendarioEnum;

/**
 * @author APerquoti
 *
 */
public class ScheduleEventoDTO extends DefaultScheduleEvent {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -8292518480556876631L;
	
	/**
	 * Data inizio.
	 */
	private Date dataInizio;
	
	/**
	 * Data scadenza.
	 */
	private Date dataScadenza;
	
	/**
	 * Titolo.
	 */
	private String titolo;
	
	/**
	 * Tipologia evento.
	 */
	private EventoCalendarioEnum tipologia;
	
	/**
	 * Id evento.
	 */
	private int idEvento;
	
	/**
	 * Id utente.
	 */
	private Long idUtente;
	
	/**
	 * Contenuto.
	 */
	private String contenuto;
	
	/**
	 * Link esterno.
	 */
	private String linkEsterno;
	
	/**
	 * Document title.
	 */
	private String documentTitle;
	
	/**
	 * Nome allegato.
	 */
	private String nomeAllegato;
	
	/**
	 * Esternsione allegato.
	 */
	private String estensioneAllegato;
	
	/**
	 * Mime type allegato.
	 */
	private String mimeTypeAllegato;
	
	/**
	 * Lista ruoli.
	 */
	private List<Long> idRuoliCategoriaTarget;
	
	/**
	 * Nodi target.
	 */
	private List<Integer> idNodiTarget;
	
	/**
	 * Descrizione nodi.
	 */
	private List<String> descrizioneNodi;
	
	/**
	 * Costruttore del DTO.
	 * @param evento
	 */
	public ScheduleEventoDTO(final EventoDTO evento) {
		super(evento.getTitolo(), evento.getDataInizio(), evento.getDataScadenza(), evento.getTipologia().getStyleClass());
		this.idUtente = evento.getIdUtente();
		this.idEvento = evento.getIdEvento();
		this.dataInizio = evento.getDataInizio();
		this.dataScadenza = evento.getDataScadenza();
		this.titolo = evento.getTitolo();
		this.tipologia = evento.getTipologia();
		this.contenuto = evento.getContenuto();
		this.linkEsterno = evento.getLinkEsterno();
		this.documentTitle = evento.getDocumentTitle();
		this.nomeAllegato = evento.getNomeAllegato();
		this.estensioneAllegato = evento.getEstensioneAllegato();
		this.mimeTypeAllegato = evento.getMimeTypeAllegato();
		this.idRuoliCategoriaTarget = evento.getIdRuoliCategoriaTarget();
		this.idNodiTarget = evento.getIdNodiTarget();
		this.descrizioneNodi = evento.getDescrizioneNodi();
	}
	
	/**
	 * Esegue la trasformazione dello ScheduleEventoDTO in un EventoDTO.
	 * @return EventoDTO generato a partire dalle informazioni dell'oggetto
	 */
	public EventoDTO trasformToEventoDTO() {
		final EventoDTO event = new EventoDTO();
		event.setIdEvento(this.idEvento);
		event.setIdUtente(this.idUtente);
		event.setDataInizio(this.dataInizio);
		event.setDataScadenza(this.dataScadenza);
		event.setTitolo(this.titolo);
		event.setTipologia(this.tipologia);
		event.setContenuto(this.contenuto);
		event.setLinkEsterno(this.linkEsterno);
		event.setDocumentTitle(this.documentTitle);
		event.setNomeAllegato(this.nomeAllegato);
		event.setEstensioneAllegato(this.estensioneAllegato);
		event.setMimeTypeAllegato(this.mimeTypeAllegato);
		event.setIdRuoliCategoriaTarget(this.idRuoliCategoriaTarget);
		event.setIdNodiTarget(this.idNodiTarget);
		event.setDescrizioneNodi(this.descrizioneNodi);
		return event;
	}

	/**
	 * @return the dataInizio
	 */
	public final Date getDataInizio() {
		return dataInizio;
	}

	/**
	 * @param dataInizio the dataInizio to set
	 */
	public final void setDataInizio(final Date dataInizio) {
		this.dataInizio = dataInizio;
	}

	/**
	 * @return the dataScadenza
	 */
	public final Date getDataScadenza() {
		return dataScadenza;
	}

	/**
	 * @param dataScadenza the dataScadenza to set
	 */
	public final void setDataScadenza(final Date dataScadenza) {
		this.dataScadenza = dataScadenza;
	}

	/**
	 * @return the titolo
	 */
	public final String getTitolo() {
		return titolo;
	}

	/**
	 * @param titolo the titolo to set
	 */
	public final void setTitolo(final String titolo) {
		this.titolo = titolo;
	}

	/**
	 * @return the tipologia
	 */
	public final EventoCalendarioEnum getTipologia() {
		return tipologia;
	}

	/**
	 * @param tipologia the tipologia to set
	 */
	public final void setTipologia(final EventoCalendarioEnum tipologia) {
		this.tipologia = tipologia;
	}

	/**
	 * @return the idEvento
	 */
	public final int getIdEvento() {
		return idEvento;
	}

	/**
	 * @param idEvento the idEvento to set
	 */
	public final void setIdEvento(final int idEvento) {
		this.idEvento = idEvento;
	}

	/**
	 * @return the idUtente
	 */
	public final Long getIdUtente() {
		return idUtente;
	}

	/**
	 * @param idUtente the idUtente to set
	 */
	public final void setIdUtente(final Long idUtente) {
		this.idUtente = idUtente;
	}

	/**
	 * @return the contenuto
	 */
	public final String getContenuto() {
		return contenuto;
	}

	/**
	 * @param contenuto the contenuto to set
	 */
	public final void setContenuto(final String contenuto) {
		this.contenuto = contenuto;
	}

	/**
	 * @return the linkEsterno
	 */
	public final String getLinkEsterno() {
		return linkEsterno;
	}

	/**
	 * @param linkEsterno the linkEsterno to set
	 */
	public final void setLinkEsterno(final String linkEsterno) {
		this.linkEsterno = linkEsterno;
	}

	/**
	 * @return the documentTitle
	 */
	public final String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * @param documentTitle the documentTitle to set
	 */
	public final void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * @return the nomeAllegato
	 */
	public final String getNomeAllegato() {
		return nomeAllegato;
	}

	/**
	 * @param nomeAllegato the nomeAllegato to set
	 */
	public final void setNomeAllegato(final String nomeAllegato) {
		this.nomeAllegato = nomeAllegato;
	}

	/**
	 * @return the estensioneAllegato
	 */
	public final String getEstensioneAllegato() {
		return estensioneAllegato;
	}

	/**
	 * @param estensioneAllegato the estensioneAllegato to set
	 */
	public final void setEstensioneAllegato(final String estensioneAllegato) {
		this.estensioneAllegato = estensioneAllegato;
	}

	/**
	 * @return the mimeTypeAllegato
	 */
	public final String getMimeTypeAllegato() {
		return mimeTypeAllegato;
	}

	/**
	 * @param mimeTypeAllegato the mimeTypeAllegato to set
	 */
	public final void setMimeTypeAllegato(final String mimeTypeAllegato) {
		this.mimeTypeAllegato = mimeTypeAllegato;
	}

	/**
	 * @return the idRuoliCategoriaTarget
	 */
	public final List<Long> getIdRuoliCategoriaTarget() {
		return idRuoliCategoriaTarget;
	}

	/**
	 * @param idRuoliCategoriaTarget the idRuoliCategoriaTarget to set
	 */
	public final void setIdRuoliCategoriaTarget(final List<Long> idRuoliCategoriaTarget) {
		this.idRuoliCategoriaTarget = idRuoliCategoriaTarget;
	}


	/**
	 * @return the idNodiTarget
	 */
	public final List<Integer> getIdNodiTarget() {
		return idNodiTarget;
	}

	/**
	 * @param idNodiTarget the idNodiTarget to set
	 */
	public final void setIdNodiTarget(final List<Integer> idNodiTarget) {
		this.idNodiTarget = idNodiTarget;
	}

	/**
	 * @return the descrizioneNodi
	 */
	public final List<String> getDescrizioneNodi() {
		return descrizioneNodi;
	}

	/**
	 * @param descrizioneNodi the descrizioneNodi to set
	 */
	public final void setDescrizioneNodi(final List<String> descrizioneNodi) {
		this.descrizioneNodi = descrizioneNodi;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		final ScheduleEventoDTO other = (ScheduleEventoDTO) obj;
		if (contenuto == null) {
			if (other.contenuto != null) {
				return false;
			}
		} else if (!contenuto.equals(other.contenuto)) {
			return false;
		}
		if (dataInizio == null) {
			if (other.dataInizio != null) {
				return false;
			}
		} else if (!dataInizio.equals(other.dataInizio)) {
			return false;
		}
		if (dataScadenza == null) {
			if (other.dataScadenza != null) {
				return false;
			}
		} else if (!dataScadenza.equals(other.dataScadenza)) {
			return false;
		}
		if (descrizioneNodi == null) {
			if (other.descrizioneNodi != null) {
				return false;
			}
		} else if (!descrizioneNodi.equals(other.descrizioneNodi)) {
			return false;
		}
		if (documentTitle == null) {
			if (other.documentTitle != null) {
				return false;
			}
		} else if (!documentTitle.equals(other.documentTitle)) {
			return false;
		}
		if (estensioneAllegato == null) {
			if (other.estensioneAllegato != null) {
				return false;
			}
		} else if (!estensioneAllegato.equals(other.estensioneAllegato)) {
			return false;
		}
		if (idEvento != other.idEvento) {
			return false;
		}
		if (idNodiTarget == null) {
			if (other.idNodiTarget != null) {
				return false;
			}
		} else if (!idNodiTarget.equals(other.idNodiTarget)) {
			return false;
		}
		if (idRuoliCategoriaTarget == null) {
			if (other.idRuoliCategoriaTarget != null) {
				return false;
			}
		} else if (!idRuoliCategoriaTarget.equals(other.idRuoliCategoriaTarget)) {
			return false;
		}
		if (idUtente == null) {
			if (other.idUtente != null) {
				return false;
			}
		} else if (!idUtente.equals(other.idUtente)) {
			return false;
		}
		if (linkEsterno == null) {
			if (other.linkEsterno != null) {
				return false;
			}
		} else if (!linkEsterno.equals(other.linkEsterno)) {
			return false;
		}
		if (mimeTypeAllegato == null) {
			if (other.mimeTypeAllegato != null) {
				return false;
			}
		} else if (!mimeTypeAllegato.equals(other.mimeTypeAllegato)) {
			return false;
		}
		if (nomeAllegato == null) {
			if (other.nomeAllegato != null) {
				return false;
			}
		} else if (!nomeAllegato.equals(other.nomeAllegato)) {
			return false;
		}
		if (tipologia != other.tipologia) {
			return false;
		}
		if (titolo == null) {
			if (other.titolo != null) {
				return false;
			}
		} else if (!titolo.equals(other.titolo)) {
			return false;
		}
		return true;
	}

	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		int out = 1;
		if (tipologia != null) {
			out = tipologia.getIdTipoCal();
		}
		return out;
	}

}
