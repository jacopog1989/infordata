package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections4.CollectionUtils;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.service.facade.IRichiesteVistoFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractTreeBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;
import it.ibm.red.web.mbean.interfaces.OrganigrammaRetriever;

/**
 * Bean che gestisce la response sigla e richiedi visti.
 */
@Named(ConstantsWeb.MBean.SIGLA_E_RICHIEDI_VISTI_BEAN)
@ViewScoped
public class SiglaERichiediVistiBean extends AbstractTreeBean implements InitHumanTaskInterface, OrganigrammaRetriever {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 7316309330295388182L;

	/**
	 * LOGGER.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SiglaERichiediVistiBean.class.getName());

	/**
	 * Documenti selezionati.
	 */
	private List<MasterDocumentRedDTO> documentiSelezionati;

	/**
	 * Bean multi assegnazione.
	 */
	private MultiAssegnazioneBean assegnazioneBean;

	/**
	 * Motivo assegnazione.
	 */
	private String motivoAssegnazione;

	/**
	 * Service.
	 */
	private IOrganigrammaFacadeSRV organigrammaSRV;

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Bean di sessione.
	 */
	private SessionBean sb;

	/**
	 * Lista documenti bean.
	 */
	private ListaDocumentiBean ldb;

	/**
	 * Servizio.
	 */
	private IRichiesteVistoFacadeSRV richiesteVistoSRV;

	/**
	 * LABERATURA DA ESPLODERE START
	 ***********************************************************************************/
	private List<Nodo> alberaturaNodi;

	/**
	 * LABERATURA DA ESPLODERE END
	 *************************************************************************************/

	@Override
	@PostConstruct
	protected void postConstruct() {
		sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
		utente = sb.getUtente();

		organigrammaSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
		richiesteVistoSRV = ApplicationContextProvider.getApplicationContext().getBean(IRichiesteVistoFacadeSRV.class);

		alberaturaNodi = organigrammaSRV.getAlberaturaBottomUp(utente.getIdAoo(), utente.getIdUfficio());

		assegnazioneBean = new MultiAssegnazioneBean();
		assegnazioneBean.initialize(this, TipoAssegnazioneEnum.VISTO);

	}

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		if (CollectionUtils.isEmpty(inDocsSelezionati)) {
			final String msg = "Non è presente nessun documento selezionato";
			LOGGER.error(msg);
			showError(msg);
		} else {
			documentiSelezionati = inDocsSelezionati;
			final Set<NodoOrganigrammaDTO> defaultNodoList = new HashSet<>();
			for (final MasterDocumentRedDTO selectedDocument : documentiSelezionati) {
				final Set<NodoOrganigrammaDTO> selectedNodeContributiSet = richiesteVistoSRV.getUfficioContributi(selectedDocument.getWobNumber(),
						selectedDocument.getDocumentTitle(), utente);
				defaultNodoList.addAll(selectedNodeContributiSet);
			}
			assegnazioneBean.initialize(this, TipoAssegnazioneEnum.VISTO, defaultNodoList);
		}
	}

	/**
	 * Esegue la logica di sigla e richiesta visto legata alla response omonima.
	 */
	public void siglaERichiediVistiResponse() {
		// E' una response per più documento (multi)
		Collection<EsitoOperazioneDTO> eOpe = new ArrayList<>();
		final List<String> wobNumbers = new ArrayList<>();
		try {
			sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
			ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);

			// pulisco eventuali esiti
			ldb.cleanEsiti();

			final List<AssegnazioneDTO> assegnatari = assegnazioneBean.getAssegnazioneList();

			// prendo i wobNumber dei documenti
			for (final MasterDocumentRedDTO documento : documentiSelezionati) {
				if (!StringUtils.isNullOrEmpty(documento.getWobNumber())) {
					wobNumbers.add(documento.getWobNumber());
				}
			}

			if (CollectionUtils.isEmpty(assegnatari)) {
				eOpe.add(new EsitoOperazioneDTO(null, false, "Per continuare è necessario aggiungere almeno un assegnatario per conoscenza"));
			} else {
				// chiamo il service (multi doc)
				eOpe = richiesteVistoSRV.siglaERichiediVisti(utente, wobNumbers, assegnatari, motivoAssegnazione);
			}

			ldb.getEsitiOperazione().addAll(eOpe);
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.SIGLA_E_RICHIEDI_VISTI_BEAN);

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il tentato sollecito della response 'VISTO'", e);
			showError("Si è verificato un errore durante il tentato sollecito della response 'VISTO'");
		}
	}

	/**
	 * Carica l'organigramma restituendone la root.
	 * @return nodo root dell'organigramma
	 */
	@Override
	public TreeNode loadRootOrganigramma() {
		DefaultTreeNode root = null;
		try {
			final List<Long> alberatura = new ArrayList<>();
			for (int i = alberaturaNodi.size() - 1; i >= 0; i--) {
				alberatura.add(alberaturaNodi.get(i).getIdNodo());
			}

			final NodoOrganigrammaDTO nodoRadice = organigrammaSRV.getNodoUCP(utente.getIdUfficio());
			nodoRadice.setCodiceAOO(utente.getCodiceAoo());
			nodoRadice.setUtentiVisible(false);
			final boolean filtraIspettorati = true;
			final List<NodoOrganigrammaDTO> primoLivello = organigrammaSRV.getFigliAlberoAssegnatarioPerRichiestaVisto(utente.getIdUfficio(), nodoRadice, filtraIspettorati);

			root = new DefaultTreeNode();
			root.setExpanded(true);
			root.setSelectable(false);

			final DefaultTreeNode nodoBase = new DefaultTreeNode(nodoRadice, root);
			nodoBase.setExpanded(true);
			nodoBase.setSelectable(false);

			// per ogni nodo di primo livello creo il nodo da mettere nell'albero che ha
			// come padre rootAssegnazionePerCompetenza
			List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
			nodoBase.setChildren(new ArrayList<>());
			DefaultTreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(utente.getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, nodoBase);
				nodoToAdd.setExpanded(true);
				if (n.getIdUtente() == null && alberatura.contains(n.getIdNodo())) {
					nodiDaAprire.add(nodoToAdd);
				}
			}

			nodiDaAprire(alberatura, nodiDaAprire);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}

		return root;
	}

	/**
	 * Gestisce l'evento di apertura di un nodo.
	 * @param treeNode nodo da aprire
	 */
	@Override
	public void onOpenTree(final TreeNode treeNode) {
		try {
			final List<NodoOrganigrammaDTO> figli = organigrammaSRV.getFigliAlberoAssegnatarioPerRichiestaVisto(utente.getIdUfficio(), (NodoOrganigrammaDTO) treeNode.getData(), false);
			
			super.onOpenTree(treeNode, figli);
			
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}

	}

	/*
	 * ################################# Getters And Setters
	 * #####################################
	 */

	/**
	 * Restituisce la lista dei documenti selezionati.
	 * 
	 * @return documenti selezionati
	 */
	public List<MasterDocumentRedDTO> getDocumentiSelezionati() {
		return documentiSelezionati;
	}

	/**
	 * Imposta la lista dei documenti selezionati.
	 * 
	 * @param documentiSelezionati
	 */
	public void setDocumentiSelezionati(final List<MasterDocumentRedDTO> documentiSelezionati) {
		this.documentiSelezionati = documentiSelezionati;
	}

	/**
	 * Restituisce il motivo dell'assegnazione.
	 * 
	 * @return motivo assegnazione
	 */
	public String getMotivoAssegnazione() {
		return motivoAssegnazione;
	}

	/**
	 * Imposta il motivo dell'assegnazione.
	 * 
	 * @param motivoAssegnazione
	 */
	public void setMotivoAssegnazione(final String motivoAssegnazione) {
		this.motivoAssegnazione = motivoAssegnazione;
	}

	/**
	 * Restituisce il bean che gestisce l'assegnazione multipla.
	 * 
	 * @return bean multi assegnazione
	 */
	public MultiAssegnazioneBean getAssegnazioneBean() {
		return assegnazioneBean;
	}

	/**
	 * Imposta il bean che gestisce l'assegnazione multipla.
	 * 
	 * @param assegnazioneBean
	 */
	public void setAssegnazioneBean(final MultiAssegnazioneBean assegnazioneBean) {
		this.assegnazioneBean = assegnazioneBean;
	}

	/**
	 * Restituisce il service per la gestione dell'organigramma.
	 * 
	 * @return service organigramma
	 */
	public IOrganigrammaFacadeSRV getOrganigrammaSRV() {
		return organigrammaSRV;
	}

	/**
	 * Imposta il service per la gestione dell'organigramma.
	 * 
	 * @param organigrammaSRV
	 */
	public void setOrganigrammaSRV(final IOrganigrammaFacadeSRV organigrammaSRV) {
		this.organigrammaSRV = organigrammaSRV;
	}

	/**
	 * Restituisce l'utente.
	 * 
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'utente.
	 * 
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Restituisce il session bean per la gestione della sessione.
	 * 
	 * @return session bean
	 */
	public SessionBean getSb() {
		return sb;
	}

	/**
	 * Restituisce lista documenti bean.
	 * 
	 * @return lista documenti bean
	 */
	public ListaDocumentiBean getLdb() {
		return ldb;
	}

	/**
	 * Imposta il session bean.
	 * 
	 * @param sb
	 */
	public void setSb(final SessionBean sb) {
		this.sb = sb;
	}

	/**
	 * Imposta il bean: lista documenti bean.
	 * 
	 * @param ldb
	 */
	public void setLdb(final ListaDocumentiBean ldb) {
		this.ldb = ldb;
	}

	/**
	 * Restituisce il service che gestisce la richiesta visto.
	 * 
	 * @return service per richiesta visto
	 */
	public IRichiesteVistoFacadeSRV getRichiesteVistoSRV() {
		return richiesteVistoSRV;
	}

	/***
	 * Imposta il service che gestisce la richiesta visto.
	 * 
	 * @param richiesteVistoSRV
	 */
	public void setRichiesteVistoSRV(final IRichiesteVistoFacadeSRV richiesteVistoSRV) {
		this.richiesteVistoSRV = richiesteVistoSRV;
	}

}
