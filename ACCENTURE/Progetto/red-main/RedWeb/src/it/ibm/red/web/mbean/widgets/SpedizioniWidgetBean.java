package it.ibm.red.web.mbean.widgets;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.StatoCodaEmailEnum;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;

/**
 * Bean che gestisce il widget sottoscrizioni.
 */
@Named(ConstantsWeb.MBean.SPEDIZIONI_WIDGET_BEAN)
@ViewScoped
public class SpedizioniWidgetBean extends AbstractBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 7025804802114382268L;

	/**
	 * Servizio.
	 */
	private IListaDocumentiFacadeSRV listaDocSRV;
	
	/**
	 * Utente.
	 */
	private UtenteDTO utente;
	
	/**
	 * Lista errori.
	 */
	private List<String> docInErrore;  
	
	/**
	 * Lista attesa riconciliazione.
	 */
	private List<String> docInAttesaDiRiconciliazione;  
	
	/**
	 * Lista spediti.
	 */
	private List<String> docSpediti;
	
	/**
	 * Conteggio doc in errore.
	 */
	private Integer numInErrore;
	
	/**
	 * Conteggio doc attesa riconciliazione.
	 */
	private Integer numInAttesaDiRiconciliazione;
	
	/**
	 * Conteggio doc spediti.
	 */
	private Integer numInSpediti;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		docInErrore = new ArrayList<>();
		docInAttesaDiRiconciliazione = new ArrayList<>();
		docSpediti = new ArrayList<>();
		
		final SessionBean sessionBean = (SessionBean) FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		listaDocSRV = ApplicationContextProvider.getApplicationContext().getBean(IListaDocumentiFacadeSRV.class);
		List<String> docInfoSpedizioni = listaDocSRV.getInfoSpedizioneWidget(utente);
		String[] subString = null;
		for (final String docInfoSpedizione : docInfoSpedizioni) {
			subString = docInfoSpedizione.split(",");
			final Integer value = Integer.parseInt(subString[1]);
			if (value.equals(StatoCodaEmailEnum.SPEDITO.getStatus())) {
				docSpediti.add(subString[0]);
			} else if (value.equals(StatoCodaEmailEnum.ERRORE.getStatus())) {
				docInErrore.add(subString[0]);
			} else {
				docInAttesaDiRiconciliazione.add(subString[0]);
			}
		}
		numInSpediti = docSpediti.size();
		numInErrore = docInErrore.size();
		numInAttesaDiRiconciliazione = docInAttesaDiRiconciliazione.size();
	}
	
	/**
	 * Restitusice la lista dei documenti in attesa di riconciliazione.
	 * @return documenti in attesa di riconciliazione
	 */
	public List<String> getDocInAttesaDiRiconciliazione() {
		return docInAttesaDiRiconciliazione;
	}
	
	/**
	 * Imposta la lista dei documenti in attesa di riconciliazione.
	 * @param docInAttesaDiRiconciliazione
	 */
	public void setDocInAttesaDiRiconciliazione(final List<String> docInAttesaDiRiconciliazione) {
		this.docInAttesaDiRiconciliazione = docInAttesaDiRiconciliazione;
	}
	
	/**
	 * Restituisce la lista dei documenti in errore.
	 * @return documenti in errore
	 */
	public List<String> getDocInErrore() {
		return docInErrore;
	}
	
	/**
	 * Imposta la lista dei documenti in errore.
	 * @param docInErrore
	 */
	public void setDocInErrore(final List<String> docInErrore) {
		this.docInErrore = docInErrore;
	}
	
	/**
	 * Restituisce la lista dei documenti spediti.
	 * @return documenti spediti
	 */
	public List<String> getDocSpediti() {
		return docSpediti;
	}
	
	/**
	 * Imposta la lista dei documenti spediti.
	 * @param docSpediti
	 */
	public void setDocSpediti(final List<String> docSpediti) {
		this.docSpediti = docSpediti;
	}
	
	/**
	 * Aggiorna {@link #docInErrore}.
	 */
	public void loadRisultati() {
		docInErrore = listaDocSRV.getDocInErroreCodaSpedizione(utente);
	}

	/**
	 * Restituisce il numero dei documenti in errore.
	 * @return numero documenti in errore
	 */
	public Integer getNumInErrore() {
		return numInErrore;
	}
	
	/**
	 * Imposta il numero dei documenti in errore.
	 * @param numInErrore
	 */
	public void setNumInErrore(final Integer numInErrore) {
		this.numInErrore = numInErrore;
	}
	
	/**
	 * Restituisce il numero dei documenti in attesa di riconciliazione.
	 * @return numero documenti in attesa di riconciliazione
	 */
	public Integer getNumInAttesaDiRiconciliazione() {
		return numInAttesaDiRiconciliazione;
	}
	
	/**
	 * Imposta il numero dei documenti in attesa di riconciliazione.
	 * @param numInAttesaDiRiconciliazione
	 */
	public void setNumInAttesaDiRiconciliazione(final Integer numInAttesaDiRiconciliazione) {
		this.numInAttesaDiRiconciliazione = numInAttesaDiRiconciliazione;
	}
	
	/**
	 * Restituisce il numero dei documenti in: Spediti.
	 * @return numero documenti in: Spediti
	 */
	public Integer getNumInSpediti() {
		return numInSpediti;
	}
	
	/**
	 * Imposta il numero dei documenti in: Spediti.
	 * @param numInSpediti
	 */
	public void setNumInSpediti(final Integer numInSpediti) {
		this.numInSpediti = numInSpediti;
	}
	
}
