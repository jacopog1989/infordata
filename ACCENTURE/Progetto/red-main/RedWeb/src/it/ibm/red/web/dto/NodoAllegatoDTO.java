package it.ibm.red.web.dto;

import it.ibm.red.business.dto.AbstractDTO;

/**
 * Classe NodoAllegatoDTO.
 */
public class NodoAllegatoDTO extends AbstractDTO {
	
	/** 
	 * The Constant serialVersionUID. 
	 */
	private static final long serialVersionUID = 4937357346209862641L;
	
	/** 
	 * Id nodo allegato 
	 */
	private String id;
	
	/** 
	 * Nome nodo allegato 
	 */
	private String nome;
	
	/**
	 * Costruttore nodo allegato DTO.
	 *
	 * @param id the id
	 * @param nome the nome
	 */
	public NodoAllegatoDTO(final String id, final String nome) {
		super();
		this.id = id;
		this.nome = nome;		
	}

	/**
	 * Getter dell'id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Setter dell'id.
	 *
	 * @param id the new id
	 */
	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * Getter del nome.
	 *
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Setter del nome.
	 *
	 * @param nome the new nome
	 */
	public void setNome(final String nome) {
		this.nome = nome;
	}
	
}
