package it.ibm.red.web.enums;

import it.ibm.red.business.enums.DocumentQueueEnum;

/**
 * The Enum NavigationTokenEnum.
 *
 * @author CPIERASC
 * 
 *         Enum gestione navigation token.
 */
public enum NavigationTokenEnum {
	
	/**
	 * Valore.
	 */
	LOGINPAGE(1, TileEnum.GENERICA, "login_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Login", null, false),

	/**
	 * Valore.
	 */
	HOMEPAGE(2, TileEnum.GENERICA, "homepage_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Homepage", null, true),

	/**
	 * Valore.
	 */
	LIBROFIRMA(3, TileEnum.SCRIVANIA, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Libro Firma", DocumentQueueEnum.NSD, true),

	/**
	 * Valore.
	 */
	DA_LAVORARE(4, TileEnum.SCRIVANIA, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Da Lavorare", DocumentQueueEnum.DA_LAVORARE, true),

	/**
	 * Valore.
	 */
	IN_SOSPESO(5, TileEnum.SCRIVANIA, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: In Sospeso", DocumentQueueEnum.SOSPESO, true),

	/**
	 * Valore.
	 */
	STORNATI(6, TileEnum.SCRIVANIA, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Stornati", DocumentQueueEnum.STORNATI, true),

	/**
	 * Valore.
	 */
	PROCEDIMENTI_ATTIVI(7, TileEnum.SCRIVANIA, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Procedimenti Attivi", DocumentQueueEnum.PROCEDIMENTI, true),

	/**
	 * Valore.
	 */
	MAIL(8, TileEnum.SCRIVANIA, "mail_tok", "/views/help/mail.xhtml", "RedEVO: Mail", null, true),

	/**
	 * Valore.
	 */
	CORRIERE(9, TileEnum.UFFICIO, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Corriere", DocumentQueueEnum.CORRIERE, true),

	/**
	 * Valore.
	 */
	ASSEGNATE(10, TileEnum.UFFICIO, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Assegnate", DocumentQueueEnum.ASSEGNATE, true),

	/**
	 * Valore.
	 */
	CHIUSE(11, TileEnum.UFFICIO, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Chiuse", DocumentQueueEnum.CHIUSE, true),

	/**
	 * Valore.
	 */
	GIRO_VISTI(12, TileEnum.UFFICIO, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Giro Visti", DocumentQueueEnum.GIRO_VISTI, true),

	/**
	 * Valore.
	 */
	IN_SPEDIZIONE(13, TileEnum.UFFICIO, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: In Spedizione", DocumentQueueEnum.SPEDIZIONE, true),


	/**
	 * Valore.
	 */
	ATTI(18, TileEnum.ARCHIVIO, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Atti", DocumentQueueEnum.ATTI, true),

	/**
	 * Valore.
	 */
	RISPOSTA(19, TileEnum.ARCHIVIO, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Risposta", DocumentQueueEnum.RISPOSTA, true),

	/**
	 * Valore.
	 */
	MOZIONE(20, TileEnum.ARCHIVIO, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Mozione", DocumentQueueEnum.MOZIONE, true),

	/**
	 * Valore.
	 */
	FALDONI(21, TileEnum.ARCHIVIO, "faldoni_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Faldoni", null, true),

	/**
	 * Valore.
	 */
	CREAZIONE_DOC_INGRESSO(22, TileEnum.CREAZIONE, "creazione_doc_ingresso_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Creazione Documento Ingresso", null, true),

	/**
	 * Valore.
	 */
	CREAZIONE_DOC_USCITA(23, TileEnum.CREAZIONE, "creazione_doc_uscita_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Creazione Documento Uscita", null, true),

	/**
	 * Valore.
	 */
	FASCICOLI(24, TileEnum.GENERICA, "fascicoli_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Fascicoli", null, true),

	/**
	 * Valore.
	 */
	RICERCA_SIGI(25, TileEnum.RICERCA, "ricerca_sigi_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Ricerca SIGI", null, false),

	/**
	 * Valore.
	 */
	RICERCA_FEPA(26, TileEnum.RICERCA, "ricerca_fepa_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Ricerca FEPA", null, false),

	/**
	 * Valore.
	 */
	RICERCA_DATI_PROTOCOLLO(27, TileEnum.RICERCA, "ricerca_dati_protocollo_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Ricerca Protocollo", null, false),

	/**
	 * Valore.
	 */
	RICERCA_STAMPA_REGISTRO(48, TileEnum.RICERCA, "ricerca_stampa_registro_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Ricerca Protocollo", null, false),

	/**
	 * Valore.
	 */
	RICERCA_DOCUMENTI(28, TileEnum.RICERCA, "ricerca_documenti_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Ricerca Documenti", null, false),

	/**
	 * Valore.
	 */
	RICERCA_FASCICOLI(29, TileEnum.RICERCA, "ricerca_fascicoli_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Ricerca Fascicoli", null, false),

	/**
	 * Valore.
	 */
	RICERCA_FALDONI(30, TileEnum.RICERCA, "ricerca_faldoni_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Ricerca Faldoni", null, false),


	/**
	 * Valore.
	 */
	RICERCA_CONTATTO(31, TileEnum.RUBRICA, "ricerca_contatto_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Ricerca Contatto", null, true),

	/**
	 * Valore.
	 */
	INSERISCI_CONTATTO(32, TileEnum.RUBRICA, "inserisci_contatto_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Inserisci Contatto", null, true),


	/**
	 * Valore.
	 */
	SCRIVANIA(33, TileEnum.GENERICA, "scrivania_tok", "/views/help/scrivania.xhtml", "RedEVO: Scrivania", null, true),

	/**
	 * Valore.
	 */
	SOTTOSCRIZIONI(34, TileEnum.EVENTI, "sottoscrizioni_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Sottoscrizioni", null, false),
	
	/**
	 * Valore.
	 */
	CLASSIFICAZIONE(37, TileEnum.CONSULTAZIONE, "classificazione_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Classificazione", null, true),

	/**
	 * Valore.
	 */
	PERSONALIZZAZIONE(38, TileEnum.UTENTE, "personalizzazione_tok", "/views/help/personalizzazione.xhtml", "RedEVO: Personalizzazione", null, true),

	/**
	 * Valore.
	 */
	RUBRICA(40, TileEnum.RUBRICA, "rubrica_tok",  NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Rubrica", null, false),
	
	/**
	 * Valore.
	 */
	ORG_SCRIVANIA(43, TileEnum.UFFICIO, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Organigramma", DocumentQueueEnum.ORG_SCRIVANIA, true),
	
	/**
	 * Valore.
	 */
	DSR(44, TileEnum.UFFICIO, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: DSR", DocumentQueueEnum.DSR, true),
		
	/**
	 * Valore.
	 */
	FATTURE_DA_LAVORARE(44, TileEnum.UFFICIO, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Fatture da Lavorare", DocumentQueueEnum.FATTURE_DA_LAVORARE, true),
	
	/**
	 * Valore.
	 */
	FATTURE_IN_LAVORAZIONE(44, TileEnum.UFFICIO, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Fatture in Lavorazione", DocumentQueueEnum.FATTURE_IN_LAVORAZIONE, true),
	
	/**
	 * Valore.
	 */
	DD_DA_LAVORARE(44, TileEnum.UFFICIO, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Decreti da Lavorare", DocumentQueueEnum.DD_DA_LAVORARE, true),
	
	/**
	 * Valore.
	 */
	DD_IN_FIRMA(44, TileEnum.UFFICIO, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Decreti in Firma", DocumentQueueEnum.DD_IN_FIRMA, true),
	
	/**
	 * Valore.
	 */
	DD_FIRMATI(44, TileEnum.UFFICIO, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Decreti Firmati", DocumentQueueEnum.DD_FIRMATI, true),
		
	/**
	 * Valore.
	 */
	DD_DA_FIRMARE(44, TileEnum.UFFICIO, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Decreti da Firmare", DocumentQueueEnum.DD_DA_FIRMARE, true),
	
	/**
	 * Valore.
	 */
	RUBRICAMAIL(45, TileEnum.RUBRICA, "rubrica_mail_tok",  NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Rubrica", null, true),
	
	/**
	 * Valore.
	 */
	REPORT(46, TileEnum.EVENTI, "report_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Report", null, false),
	
	/**
	 * Valore.
	 */
	RICERCA_RAPIDA(47, TileEnum.RICERCA, "ricerca_rapida_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Ricerca Rapida", null, false),

	/**
	 * Valore.
	 */
	LIBRO_FIRMA_DELEGATO(48, TileEnum.SCRIVANIA, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Libro Firma", DocumentQueueEnum.NSD_LIBRO_FIRMA_DELEGATO, true),
	
	/**
	 * Valore.
	 */
	IN_ACQUISIZIONE(49, TileEnum.CARTACEI, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: In acquisizione", DocumentQueueEnum.IN_ACQUISIZIONE, true),
	
	/**
	 * Valore.
	 */
	ACQUISITI(50, TileEnum.CARTACEI, "coda_cartacei_acquisiti_tok", NavigationTokenEnum.CODA_XHTML, "RedEVO: Acquisiti", DocumentQueueEnum.ACQUISITI, true),
	
	/**
	 * Valore.
	 */
	ELIMINATI(51, TileEnum.CARTACEI, "coda_cartacei_acquisiti_tok", NavigationTokenEnum.CODA_XHTML, "RedEVO: Eliminati", DocumentQueueEnum.ELIMINATI, true),
	
	/**
	 * Valore.
	 */
	HOMEPAGE_FROM_LOGIN(52, TileEnum.GENERICA, "homepage_from_login_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Homepage", null, true),

	/**
	 * Valore.
	 */
	DELEGHE(53, TileEnum.EVENTI, "deleghe_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Deleghe", null, false),
	 
	/**
	 * Valore.
	 */
	DASHBOARD(54, TileEnum.GENERICA, "dashboard_tok", "/views/help/dashboard.xhtml", "RedEVO: Dashboard", null, false),
	
	/**
	 * Valore.
	 */
	RICHIAMA_DOCUMENTI(55, TileEnum.SCRIVANIA, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Richiama documenti", DocumentQueueEnum.RICHIAMA_DOCUMENTI, true),
	
	/**
	 * Valore.
	 */
	RICERCA_FASCICOLI_NSD(56, TileEnum.RICERCA, "ricerca_fascicoli_nsd_tok", NavigationTokenEnum.CODA_XHTML, "RedEVO: Ricerca fascicoli Nsd", null, true),
	
	/**
	 * Valore.
	 */
	RICERCA_PROTOCOLLI_NSD(57, TileEnum.RICERCA, "ricerca_protocolli_nsd_tok", NavigationTokenEnum.CODA_XHTML, "RedEVO: Ricerca Protocolli Nsd", null, true),
	
	/**
	 * Valore.
	 */
	WIZARD_TIPO_DOC(58, TileEnum.SCRIVANIA, "wiz_tipo_doc_tok", "/views/help/wizardTipiDoc.xhtml", "RedEVO: Wizard Tipo Documenti", null, true),
	
	/**
	 * Valore.
	 */
	WIZARD_ASSEGNAZIONI_AUTO(59, TileEnum.SCRIVANIA, "wiz_assegnazioni_auto_tok", "/views/help/wizardAssegnazioniAutomatiche.xhtml", 
			"RedEVO: Wizard Assegnazioni Automatiche", null, true),

	/**
	 * Valore.
	 */
	CORRIERE_DIRETTO(60, TileEnum.UFFICIO, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Corriere", DocumentQueueEnum.CORRIERE_DIRETTO, true),

	/**
	 * Valore.
	 */
	CORRIERE_INDIRETTO(61, TileEnum.UFFICIO, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Da Assegnare", DocumentQueueEnum.CORRIERE_INDIRETTO, true),
		
	/**
	 * Valore.
	 */
	RICERCA_DOCUMENTI_UCB(62, TileEnum.RICERCA, "ricerca_documenti_ucb_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Ricerca Avanzata UCB", null, false),
	
	/**
	 * Valore.
	 */
	RICERCA_REGISTRAZIONI_AUSILIARIE(63, TileEnum.RICERCA, "ricerca_reg_ausiliarie_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Ricerca Registrazioni Ausiliarie", null, false),
	
	/**
	 * Valore.
	 */
	DA_LAVORARE_UCB(64, TileEnum.SCRIVANIA, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Da Lavorare", DocumentQueueEnum.DA_LAVORARE_UCB, true),

	/**
	 * Valore.
	 */
	IN_LAVORAZIONE_UCB(65, TileEnum.SCRIVANIA, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: In Lavorazione", DocumentQueueEnum.IN_LAVORAZIONE_UCB, true),

	/**
	 * Valore.
	 */
	CARICAMENTO_MASSIVO(66, TileEnum.SCRIVANIA, "caricamento_massivo_tok", "/views/help/caricamentoMassivo.xhtml", "RedEVO: Caricamento Massivo", null, true),
	
	/**
	 * Valore.
	 */
	ASSEGNAZIONE_METADATI(67, TileEnum.SCRIVANIA, "assegnazione_metadati_tok", "/views/help/assegnazioneMetadati.xhtml", "RedEVO: Assegnazione Metadati", null, true),
	
	/**
	 * Valore.
	 */
	ACCORDO_SERVIZIO(68, TileEnum.SCRIVANIA, "accordo_servizio_tok", "/views/help/accordiServizio.xhtml", "RedEVO: Accordi di Servizio", null, true),
	
	/**
	 * Valore.
	 */
	RICERCA_FLUSSI_UCB(69, TileEnum.RICERCA, "ricerca_flussi_ucb_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Ricerca Flussi UCB", null, false),
	
	/**
	 * Token pagina di ricerca estratti conto ccvt ucb.
	 */
	RICERCA_ESTRATTI_CONTO_CCVT_UCB(70, TileEnum.RICERCA, "ricerca_estratti_conto_ucb_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Ricerca Estratti Conto Trimestrali CCVT UCB", null, false),
	
	/**
	 * Valore.
	 */
	RICERCA_CONTI_CONSUNTIVI_UCB(71, TileEnum.RICERCA, "ricerca_conti_consuntivi_ucb_tok", NavigationTokenEnum.HOMEPAGE_XHTML, "RedEVO: Ricerca Elenco notifiche e conti consuntivi UCB", null, false),
	
	/**
	 * Tocken coda preparazione alla spedizione.
	 */
	PREPARAZIONE_ALLA_SPEDIZIONE(72, TileEnum.SCRIVANIA, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Preparazione alla Spedizione", DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE, true),
	
	/**
	 * Tocken coda preparazione alla spedizione FEPA.
	 */
	PREPARAZIONE_ALLA_SPEDIZIONE_FEPA(73, TileEnum.SCRIVANIA, NavigationTokenEnum.CODA_TOK, NavigationTokenEnum.CODA_XHTML, "RedEVO: Preparazione alla Spedizione", DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE_FEPA, true)
	;

	/**
	 * Identificativo.
	 */
	private Integer id;	
	
	/**
	 * Tile.
	 */
	private TileEnum tile;	

	/**
	 * Token navigation rule.
	 */
	private String token;
	
	/**
	 * Pagina di help associata alla pagina.
	 */
	private String helpPage;
	
	/**
	 * Titolo della pagina.
	 */
	private String titlePage;

	/**
	 * Coda di riferimento.
	 */
	private DocumentQueueEnum documentQueue;
	
	/**
	 * Flag menù visibile.
	 */
	private boolean btnMenuVisible;
	
	private static final String HOMEPAGE_XHTML = "/views/help/homepage.xhtml";
	private static final String CODA_TOK = "coda_tok";
	private static final String CODA_XHTML = "/views/help/coda.xhtml";
	
	/**
	 * Costruttore.
	 * 
	 * @param inId			identificativo
	 * @param inGroupId		gruppo
	 * @param inToken		token
	 * @param inHelpPage	help page
	 * @param inTitlePage	title page
	 */
	NavigationTokenEnum(final Integer inId, final TileEnum inTile, final String inToken, final String inHelpPage, final String inTitlePage, final DocumentQueueEnum inDocumentQueue, final boolean inBtnMenuVisible) {
		id = inId;
		tile = inTile;
		token = inToken;
		helpPage = inHelpPage;
		titlePage = inTitlePage;
		documentQueue = inDocumentQueue;
		btnMenuVisible = inBtnMenuVisible;
	}

	/**
	 * Restituisce la queue.
	 * @return queue
	 */
	public DocumentQueueEnum getDocumentQueue() {
		return documentQueue;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	id
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Getter token.
	 * 
	 * @return	token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Getter help page.
	 * 
	 * @return	help page
	 */
	public String getHelpPage() {
		return helpPage;
	}

	/**
	 * Getter title page.
	 * 
	 * @return	title page
	 */
	public String getTitlePage() {
		return titlePage;
	}
	
	/**
	 * Restituisce la tile del navigation token.
	 * @return tile del navigation token
	 */
	public TileEnum getTile() {
		return tile;
	}
	
	/**
	 * @return the btnMenuVisible
	 */
	public final boolean isBtnMenuVisible() {
		return btnMenuVisible;
	}

	/**
	 * Metodo per il recupero di un'enum dato il valore caratteristico.
	 * 
	 * @param value	valore
	 * @return		enum
	 */
	public static NavigationTokenEnum get(final Integer value) {
		NavigationTokenEnum output = null;
		for (NavigationTokenEnum n:NavigationTokenEnum.values()) {
			if (n.getId().equals(value)) {
				output = n;
				break;
			}
		}
		return output;
	}

	/**
	 * Restituisce il token associato alla coda come enum.
	 * @param queue
	 * @return token associato alla coda come enum
	 */
	public static NavigationTokenEnum get(final DocumentQueueEnum queue) {
		NavigationTokenEnum output = null;
		for (NavigationTokenEnum n:NavigationTokenEnum.values()) {
			if (n.getDocumentQueue() != null && n.getDocumentQueue().equals(queue)) {
				output = n;
				break;
			}
		}
		return output;
	}
	
	/**
	 * Restituisce true se il token è associato ad uno dei CORRIERI.
	 * @param token
	 * @return true se il token è associato ad uno dei CORRIERI
	 */
	public static boolean isOneKindOfCorriere(final NavigationTokenEnum token) {
		return token != null 
				&& (token.equals(NavigationTokenEnum.CORRIERE) 
						|| token.equals(NavigationTokenEnum.CORRIERE_DIRETTO) 
						|| token.equals(NavigationTokenEnum.CORRIERE_INDIRETTO));
	}

	/**
	 * Restituisce true se il token è associato ad una delle code DA LAVORARE.
	 * @param token
	 * @return true se il token è associato ad una delle code DA LAVORARE
	 */
	public static boolean isOneKindOfDaLavorare(final NavigationTokenEnum token) {
		return token != null 
				&& (token.equals(NavigationTokenEnum.DA_LAVORARE) 
						|| token.equals(NavigationTokenEnum.DA_LAVORARE_UCB));
	}
	
	/**
	 * Restituisce true se il token è associato ad una delle code IN LAVORAZIONE.
	 * @param token
	 * @return true se il token è associato ad una delle code IN LAVORAZIONE
	 */
	public static boolean isOneKindOfDaLavorareInLavorazione(final NavigationTokenEnum token) {
		return token != null 
				&& (token.equals(NavigationTokenEnum.DA_LAVORARE) 
						|| token.equals(NavigationTokenEnum.DA_LAVORARE_UCB) 
						|| token.equals(NavigationTokenEnum.IN_LAVORAZIONE_UCB));
	}
	
}