package it.ibm.red.web.mbean;


import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.interfaces.IDetailPreviewComponent;
import it.ibm.red.web.servlets.DownloadContentServlet;

/**
 * Bean dettaglio SIGI.
 */
@Named(ConstantsWeb.MBean.DETTAGLIO_SIGI_BEAN)
@ViewScoped
public class DettaglioSigiBean extends AbstractBean implements IDetailPreviewComponent {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -8772373879026765513L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DettaglioSigiBean.class);
	
	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Info cryptate.
	 */
	private String cryptoId = null;

	/**
	 * Post construct del bean.
	 */
	@PostConstruct
	@Override
	protected void postConstruct() {
		final SessionBean session = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente =  session.getUtente();
	}
	
	/**
	 * Inizializzazione del cryptoID eseguito dal bean Master.
	 * 
	 * @param id
	 */
	public void setDetail(final String id) {
		if (StringUtils.isBlank(id)) {
			LOGGER.error("errore id null per il dettaglio sigi");
			throw new RedException("Impossibile visualizzare il dettaglio");
		}
		
		this.cryptoId = getCryptoDocParamById(id);
	}
	
	@Override
	public final String getCryptoId() {
		return cryptoId;
	}
	
	/**
	 * Recupera i parametri criptati utilizzati per la Servlet 'DownloadContentServlet'.
	 * @param id - Identificativo del documento
	 * @return Parametri cryptati.
	 */
	private String getCryptoDocParamById(final String id) {
		try {
			return DownloadContentServlet.getCryptoDocParamById(id, DownloadContentServlet.DocumentTypeEnum.SIGI, utente);
		} catch (final Exception e) {
			LOGGER.error("Errore nell'encrypt dei parametri per la servlet 'DownloadContentServlet':", e);
			showError("Errore nell'encrypt dei parametri per la servlet 'DownloadContentServlet': " + e.getMessage());
		}
		return "ERROREDICODIFICA";
	}
}
