package it.ibm.red.web.dto;

import it.ibm.red.business.dto.DocumentoFascicoloDTO;

/**
 * DTO che definisce un tree node del documento.
 */
public class TreeNodeDocumentoDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -1289100430681513503L;

	/**
	 * Documento.
	 */
	private DocumentoFascicoloDTO documento;

	/**
	 * Identificativo fascicolo.
	 */
	private Integer idFascicolo;

	/**
	 * Costruttore.
	 * @param documento
	 * @param idFascicolo
	 */
	public TreeNodeDocumentoDTO(final DocumentoFascicoloDTO documento, final Integer idFascicolo) {
		super();
		this.documento = documento;
		this.idFascicolo = idFascicolo;
	}
	
	/**
	 * Restituisce il documento.
	 * @return il documento
	 */
	public DocumentoFascicoloDTO getDocumento() {
		return documento;
	}
	
	/**
	 * Imposta il documento.
	 * @param documento
	 */
	public void setDocumento(final DocumentoFascicoloDTO documento) {
		this.documento = documento;
	}
	
	/**
	 * Restituisce l'id fascicolo.
	 * @return id fascicolo
	 */
	public Integer getIdFascicolo() {
		return idFascicolo;
	}
	
	/**
	 * Imposta l'id fascicolo.
	 * @param idFascicolo
	 */
	public void setIdFascicolo(final Integer idFascicolo) {
		this.idFascicolo = idFascicolo;
	}

	
}
