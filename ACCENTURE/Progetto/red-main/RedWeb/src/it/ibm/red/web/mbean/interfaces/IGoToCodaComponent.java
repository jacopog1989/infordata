package it.ibm.red.web.mbean.interfaces;

import java.util.EnumMap;

import it.ibm.red.business.enums.DocumentQueueEnum;

/**
 * Serve a implementare il pannello che si apre dopo aver cliccato il bottone affianco a una riga di una tabella di ricerca.
 * Mostra una lista di code in cui l'utente può trovare il documento rappresentato da quella riga.
 * 
 */
public interface IGoToCodaComponent {
	
	/**
	 * Inserisce in sessione il documento selezionato 
	 * wrappato in un masterDocumentBean di cui solo il documentTitle e il numeroDocumento devono essere valorizzati
	 * 
	 * La variabile contenuta in sessione è ConstantsWeb.SessionObject.FILTER_MASTER_GO_TO_CODA
	 * 
	 * Infine linka il nuovo url usando il meccanismo di default di Primefaces
	 * 
	 * @param q
	 * 	Enum della riga di riferimento
	 * @return
	 *  Indirizzo della pagina della coda di riferimento
	 */
	String goToCoda(DocumentQueueEnum q);
	
	/**
	 * Aggiornata dall'evento di click del bottone che apre il pannello
	 * Mostra quanti documenti sono presenti nella coda
	 * 
	 * @return
	 * La mappa dei documenti presenti nella coda
	 * 
	 */
	EnumMap<DocumentQueueEnum, Integer> getCodeCount();
	
	/**
	 * Aggiornata dall'evento di click del bottone che apre il pannello
	 * Mostra le code non censite per il documento
	 * 
	 * @return
	 */
	String getListaCodeNonCensite();
}
