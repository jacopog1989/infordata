/**
 * 
 */
package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IIntegrazioneDatiFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * @author APerquoti
 *
 */
@ViewScoped
@Named(ConstantsWeb.MBean.SELEZIONA_DOCUMENTO_BEAN)
public class SelezionaDocumentoBean extends AbstractBean implements InitHumanTaskInterface {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -311426674231291745L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SelezionaDocumentoBean.class.getName());
	
	/**
	 * Servizio.
	 */
	private IIntegrazioneDatiFacadeSRV integrazioneDatiSRV;

	/**
	 * Lista documenti bean.
	 */
	private ListaDocumentiBean ldb;
	
	/**
	 * Bean di Sessione.
	 */
	private SessionBean sb;
	
	/**
	 * Master selezionato.
	 */
	private MasterDocumentRedDTO master;
	
	/**
	 * Datatable master.
	 */
	private SimpleDetailDataTableHelper<MasterDocumentRedDTO> documentiDTH;
	
	/**
	 * Response selezionata.
	 */
	private ResponsesRedEnum selectedResponse;
	
	/**
	 * Flag conferma disabilitata.
	 */
	private boolean btnConfirmDisabled;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
		sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		
	}

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		if (inDocsSelezionati.get(0) != null) {
			master = inDocsSelezionati.get(0);
		}
		
		selectedResponse = ldb.getSelectedResponse();
		documentiDTH = new SimpleDetailDataTableHelper<>();
		
		btnConfirmDisabled = true;
		
		loadDocsForSelection();
	}
	
	private void loadDocsForSelection() {
		documentiDTH.setMasters(new ArrayList<>());
		
		try {
			
			
			if (ResponsesRedEnum.ASSOCIA_INTEGRAZIONE_DATI.equals(selectedResponse)) {
				
				integrazioneDatiSRV = ApplicationContextProvider.getApplicationContext().getBean(IIntegrazioneDatiFacadeSRV.class);
				final Collection<MasterDocumentRedDTO> mastersToSelect = integrazioneDatiSRV.getIntegrazioneDati4Associa(sb.getUtente());
				documentiDTH.setMasters(mastersToSelect);
			}
			
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il caricamento dei documenti da selezionare.", e);
			showError("Si è verificato un errore durante il caricamento dei documenti da selezionare.");
		}
		
	}
	
	/**
	 * Gestisce la selezione della riga dal datatable dei documenti.
	 */
	public void onRowSelectRadio() {
		try {
			if (documentiDTH.getCurrentMaster() != null) {
				btnConfirmDisabled = false;
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}
	
	/**
	 * Esegue una validazione dei campi, successivamente esegue l'associazione dell'integrazione dati.
	 * A valle delle operazioni aggiorna gli esiti in modo che vengano correttamente visualizzati.
	 */
	public void confermaSelezione() {
		final Collection<EsitoOperazioneDTO> esitiOpe = new ArrayList<>();
		
		try {
			
			// Si puliscono gli esiti
			ldb.cleanEsiti();
			
			if (ResponsesRedEnum.ASSOCIA_INTEGRAZIONE_DATI.equals(selectedResponse)) {
				
				final MasterDocumentRedDTO masterSelected = documentiDTH.getCurrentMaster();
				final EsitoOperazioneDTO esitoOpe = integrazioneDatiSRV.associaIntegrazioneDati(sb.getUtente(), master.getWobNumber(), masterSelected.getDocumentTitle());
				esitiOpe.add(esitoOpe);
				
			} 			
			// Si impostano i nuovi esiti
			ldb.getEsitiOperazione().addAll(esitiOpe);
			
			FacesHelper.executeJS("PF('dlgGeneric').hide()");
			FacesHelper.update("centralSectionForm:idDlgShowResult");
			FacesHelper.executeJS("PF('dlgShowResult').show()");
			
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.SELEZIONA_DOCUMENTO_BEAN);
			
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'esecuzione del business per la response <" + selectedResponse + ">.", e);
			showError("Si è verificato un errore durante l'esecuzione del business per la response <" + selectedResponse + ">.");
		}
		
	}

	/**
	 * @return the documentiDTH
	 */
	public SimpleDetailDataTableHelper<MasterDocumentRedDTO> getDocumentiDTH() {
		return documentiDTH;
	}

	/**
	 * @param documentiDTH the documentiDTH to set
	 */
	public void setDocumentiDTH(final SimpleDetailDataTableHelper<MasterDocumentRedDTO> documentiDTH) {
		this.documentiDTH = documentiDTH;
	}

	/**
	 * @return the btnConfirmDisabled
	 */
	public boolean isBtnConfirmDisabled() {
		return btnConfirmDisabled;
	}

	/**
	 * @param btnConfirmDisabled the btnConfirmDisabled to set
	 */
	public void setBtnConfirmDisabled(final boolean btnConfirmDisabled) {
		this.btnConfirmDisabled = btnConfirmDisabled;
	}
	

	
}
