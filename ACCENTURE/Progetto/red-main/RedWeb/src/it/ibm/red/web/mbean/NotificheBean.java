package it.ibm.red.web.mbean;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.facade.IRubricaFacadeSRV;
import it.ibm.red.business.service.facade.ISottoscrizioniFacadeSRV;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Bean per la gestione delle notifiche
 * 
 * @author SLac
 */
@Named(MBean.NOTIFICHE_BEAN)
@ViewScoped
public class NotificheBean extends AbstractBean {

	/** 
	 * The Constant serialVersionUID. 
	 */
	private static final long serialVersionUID = -282254130254007716L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotificheBean.class);
	
	/** 
	 * Variabile utente  
	 */
	private UtenteDTO utente;
	 
	
	/** 
	 * Utils per recupeare le properties 
	 */
	private PropertiesProvider pp;

	/** 
	 * Facade delle sottoscrizioni  
	 */
	private ISottoscrizioniFacadeSRV sottoscrizioniSRV;
	
	/** 
	 * Facade della rubrica 
	 */
	private IRubricaFacadeSRV rubricaSRV;
	
	/** 
	 * Contatore sottoscrizioni stringa 
	 */
	private String countSottoscrizioni;
	
	/** 
	 * Contatore rubrica stringa 
	 */
	private String countRubrica;
	
	/** 
	 * Parametro usato per impostare l'intervallo delle notifiche 
	 */
	private Integer intervalloNotifiche;
	
	/** 
	 * Parametro usato per mostrare l'icona della notifica 
	 */
	private boolean showIconaNotifica;

	/** 
	 * PostConstruct 
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		SessionBean sessionBean = FacesHelper.getManagedBean(MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		pp = PropertiesProvider.getIstance();
		
		intervalloNotifiche = Integer.valueOf(pp.getParameterByKey(PropertiesNameEnum.NOTIFICHE_POLLING_SECONDI)) * 1000;
			
		countSottoscrizioni = "0";
		countRubrica = "0";
		loadNotifiche();
		LOGGER.info("NotificheBean.postConstruct()");
	}

	/**
	 * Per ora il metodo calcola tutte le notifiche, 
	 * lascio comunque i metodi divisi perché in futuro potremmo utilizzare i caricamenti separati.  
	 */
	public void loadNotifiche() {
		loadCountSottoscrizioni();
		if (PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.GESTIONE_RUBRICA)) {
			loadCountRubrica();
		}
		
		if ((Integer.valueOf(countSottoscrizioni) + Integer.valueOf(countRubrica)) > 0) {
			showIconaNotifica = true;
		} else {
			showIconaNotifica = false;
		}
	}
	
	/**
	 * Load count sottoscrizioni.
	 */
	public void loadCountSottoscrizioni() {
		try {
			if (sottoscrizioniSRV == null) {
				sottoscrizioniSRV = ApplicationContextProvider.getApplicationContext().getBean(ISottoscrizioniFacadeSRV.class);
			}
			Integer giorni = Integer.valueOf(pp.getParameterByKey(PropertiesNameEnum.NOTIFICHE_SOTTOSCRIZIONI_ULTIMI_N_GIORNI));
			countSottoscrizioni = sottoscrizioniSRV.getCountSottoscrizioni(utente, giorni) + "";
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore nel caricamento del numero di notifiche delle sottoscrizioni.");
		}
	}
	
	/**
	 * Load count rubrica.
	 */
	public void loadCountRubrica() {
		try {
			if (rubricaSRV == null) {
				rubricaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaFacadeSRV.class);
			}
			Integer giorni = Integer.valueOf(pp.getParameterByKey(PropertiesNameEnum.NOTIFICHE_RUBRICA_ULTIMI_N_GIORNI));
			countRubrica = rubricaSRV.getCountListaNotificheRichieste(utente.getIdAoo(), giorni) + "";
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore nel caricamento del numero di notifiche delle sottoscrizioni.");
		}
	}
	
	/**
	 *  GET & SET. **************************************************************
	 *
	 * @return the count rubrica
	 */

	public String getCountRubrica() {
		return countRubrica;
	}

	/**
	 * Getter del count sottoscrizioni.
	 *
	 * @return the count sottoscrizioni
	 */
	public String getCountSottoscrizioni() {
		return countSottoscrizioni;
	}

	/**
	 * Checks if is show icona notifica.
	 *
	 * @return true, if is show icona notifica
	 */
	public boolean isShowIconaNotifica() {
		return showIconaNotifica;
	}

	/**
	 * Getter del intervallo notifiche.
	 *
	 * @return the intervallo notifiche
	 */
	public Integer getIntervalloNotifiche() {
		return intervalloNotifiche;
	}

}
