package it.ibm.red.web.mbean.humantask;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.enums.PermessiAOOEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IContributoSRV;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * Bean che gestisce l'inserimento di contributo.
 */
@Named(ConstantsWeb.MBean.INSERISCI_CONTRIBUTO_BEAN)
@ViewScoped
public class InserisciContributoBean extends AbstractBean implements InitHumanTaskInterface {
	
	/** 
 	 * Bean per le response Inserisci contributo e Valida contributo.
	 */
	private static final long serialVersionUID = -3479140039632360387L;
	
	/**
	 * Logger del Bean.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(InserisciContributoBean.class);

	/**
	 * Documento selezionato.
	 */
	private MasterDocumentRedDTO documentoSelezionato;
	
	/**
	 * Nota.
	 */
	private String nota;
	
	/**
	 * Servizio.
	 */
	private IContributoSRV contributoSRV;
	
	/**
	 * Documento caricato.
	 */
	private transient UploadedFile docPrincipale;
	
	/**
	 * Contributo.
	 */
	private ContributoDTO contributo;
	
	/**
	 * Sessione bean.
	 */
	private SessionBean sb;
	
	/**
	 * Flag.
	 */
	private Boolean flagPermesso;
	
	/**
	 * Data cdcd.
	 */
	private Date dataCdc;
	
	/**
	 * Numero foglio.
	 */
	private Integer foglio;
	
	/**
	 * Registro.
	 */
	private Integer registro;
	
	/**
	 * Flag prosegui.
	 */
	private boolean continua;
	
	/**
	 * Response.
	 */
	private ResponsesRedEnum responseType;
	
	/**
	 * Lista documenti bean.
	 */
	private ListaDocumentiBean ldb;
	
	/**
	 * Verifica se è possibile eseguire la response.
	 */
	public void checkEResponse() {
		//controllo se i campi obbligatori sono stati compilati ed eventualmente eseguo la response
		if (!permessoContinua()) {
			final String errMsg = "Almeno uno dei campi Testo o File deve essere specificato.";
			showError(errMsg);
		} else {
			inserisciContributoResponse();
		}
	}
	
	/**
	 * Metodo per le response Inserisci Contributo e Valida Contributi
	 */
	public void inserisciContributoResponse() {
		EsitoOperazioneDTO eOpe = null;
		try {
			ldb.cleanEsiti();
			final String wobNumber = documentoSelezionato.getWobNumber();
			
			contributo.setNota(nota);
			
			//inizializzazione campi nascosti
			if (foglio == null || foglio < 0) {
				foglio = 0;
			}
			if (registro == null || registro < 0) {
				registro = 0;
			}
			contributo.setDataCdc(dataCdc);
			contributo.setFoglio((foglio.intValue()));
			contributo.setRegistro((registro.intValue()));
		
			switch(responseType) {
				case INSERISCI_CONTRIBUTO:
					eOpe = contributoSRV.inserisciContributo(sb.getUtente(), wobNumber, contributo);
					break;
				case INSERISCI_CONTRIBUTO_2:
					eOpe = contributoSRV.inserisciContributo2(sb.getUtente(), wobNumber, contributo);
					break;
				case VALIDA_CONTRIBUTI:
					eOpe = contributoSRV.validaContributi(sb.getUtente(), wobNumber, contributo);
					break;
				default: //non dovrebbe entrare mai qua!
					showError("Response non prevista");
					eOpe = new EsitoOperazioneDTO(null, false, "Response non prevista");
					break;
			}
			ldb.getEsitiOperazione().add(eOpe);
			//
			FacesHelper.executeJS("PF('dlgGeneric').hide();PF('dlgShowResult').show();");
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.INSERISCI_CONTRIBUTO_BEAN);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore", e);
			showError("Si è verificato un errore");
		}
	}
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
		responseType = ldb.getSelectedResponse();
		sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		contributoSRV = ApplicationContextProvider.getApplicationContext().getBean(IContributoSRV.class);
		//Prendo i permessi AOO dell'utente
		final Long permessiAOO = sb.getUtente().getPermessiAOO();
		flagPermesso = PermessiUtils.hasPermesso(permessiAOO, PermessiAOOEnum.CORTE_DEI_CONTI);
		contributo = new ContributoDTO(null);
		continua = false;
		
	}

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		documentoSelezionato = inDocsSelezionati.get(0);
	}
	
	/**
	 * Gestisce l'upload del file principale.
	 * @param event
	 */
	public void handleMainFileUpload(FileUploadEvent event) {
		try {
			event = checkFileName(event);
			if (event == null) {
				return;
			}
			docPrincipale = event.getFile();

			final String fileName = docPrincipale.getFileName();
			final String tipoFile = docPrincipale.getContentType(); //mimeType
			final byte[] contentFile = docPrincipale.getContents();
			
			contributo = new ContributoDTO(fileName, tipoFile, contentFile);
			
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}
    }
	
	/**
	 * Rimuove i files.
	 */
	public void rimuoviFile() {
		docPrincipale = null;
		contributo = new ContributoDTO(null, null, null);
	}
	
	/**
	 * Controlla se è possibile procedere.
	 * @return true o false
	 */
	public boolean permessoContinua() {
		
		setContinua((nota != null && !"".equals(nota)) || (docPrincipale != null && !"".equals(docPrincipale.getFileName())));
		return continua;
	}
	
	/**
	 * Recupera il documento selezionato.
	 * @return documento selezionato
	 */
	public MasterDocumentRedDTO getDocumentoSelezionato() {
		return documentoSelezionato;
	}

	/**
	 * Imposta il documento selezionato.
	 * @param documentoSelezionato documento selezionato
	 */
	public void setDocumentoSelezionato(final MasterDocumentRedDTO documentoSelezionato) {
		this.documentoSelezionato = documentoSelezionato;
	}

	/**
	 * Recupera la nota.
	 * @return nota
	 */
	public String getNota() {
		return nota;
	}

	/**
	 * Imposta la nota.
	 * @param nota
	 */
	public void setNota(final String nota) {
		this.nota = nota;
	}

	/**
	 * Recupera il documento principale.
	 * @return documento principale
	 */
	public UploadedFile getDocPrincipale() {
		return docPrincipale;
	}

	/**
	 * Imposta il documento principale.
	 * @param docPrincipale documento principale
	 */
	public void setDocPrincipale(final UploadedFile docPrincipale) {
		this.docPrincipale = docPrincipale;
	}

	/**
	 * Recupera il contributo service.
	 * @return contributo service
	 */
	public IContributoSRV getContributoSRV() {
		return contributoSRV;
	}

	/**
	 * Imposta il contributo service.
	 * @param contributoSRV contributo service
	 */
	public void setContributoSRV(final IContributoSRV contributoSRV) {
		this.contributoSRV = contributoSRV;
	}

	/**
	 * Recupera il contributo.
	 * @return contributo
	 */
	public ContributoDTO getContributo() {
		return contributo;
	}

	/**
	 * Imposta il contributo.
	 * @param contributo
	 */
	public void setContributo(final ContributoDTO contributo) {
		this.contributo = contributo;
	}

	/**
	 * Recupera session bean.
	 * @return session bean
	 */
	public SessionBean getSb() {
		return sb;
	}

	/**
	 * Imposta session bean.
	 * @param sb session bean
	 */
	public void setSb(final SessionBean sb) {
		this.sb = sb;
	}

	/**
	 * Recupera il flag permesso.
	 * @return flag permesso
	 */
	public Boolean getFlagPermesso() {
		return flagPermesso;
	}

	/**
	 * Imposta il flag permesso.
	 * @param flagPermesso flag permesso
	 */
	public void setFlagPermesso(final Boolean flagPermesso) {
		this.flagPermesso = flagPermesso;
	}

	/**
	 * Recupera la data cdc.
	 * @return data cdc
	 */
	public Date getDataCdc() {
		return dataCdc;
	}

	/**
	 * Recupera il foglio.
	 * @return foglio
	 */
	public Integer getFoglio() {
		return foglio;
	}

	/**
	 * Recupera il registro.
	 * @return registro
	 */
	public Integer getRegistro() {
		return registro;
	}

	/**
	 * Imposta la data cdc.
	 * @param dataCdc data cdc
	 */
	public void setDataCdc(final Date dataCdc) {
		this.dataCdc = dataCdc;
	}

	/**
	 * Imposta il foglio.
	 * @param foglio
	 */
	public void setFoglio(final Integer foglio) {
		this.foglio = foglio;
	}

	/**
	 * Imposta il registro.
	 * @param registro
	 */
	public void setRegistro(final Integer registro) {
		this.registro = registro;
	}

	/**
	 * Recupera il tipo di response.
	 * @return tipo di response
	 */
	public ResponsesRedEnum getResponseType() {
		return responseType;
	}

	/**
	 * Imposta il tipo di response.
	 * @param responseType tipo di response
	 */
	public void setResponseType(final ResponsesRedEnum responseType) {
		this.responseType = responseType;
	}

	/**
	 * Recupera la lista documenti bean.
	 * @return lista documenti bean
	 */
	public ListaDocumentiBean getLdb() {
		return ldb;
	}

	/**
	 * Imposta la lista documenti bean.
	 * @param ldb lista documenti bean
	 */
	public void setLdb(final ListaDocumentiBean ldb) {
		this.ldb = ldb;
	}

	/**
	 * Recupera l'attributo continua.
	 * @return continua
	 */
	public boolean isContinua() {
		return continua;
	}

	/**
	 * Imposta l'attributo continua.
	 * @param continua
	 */
	public void setContinua(final boolean continua) {
		this.continua = continua;
	}
}
