package it.ibm.red.web.mbean.interfaces;

/**
 * Interfaccia del Component che gestisce la registrazione e l'annullamento del messaggio.
 */
public interface IMessaggioRegistraAnnullaComponent {
	
	/**
	 * Chiude la dialog eccetera
	 */
	void close();
	
	/**
	 * Esegue una azione
	 */
	void registra();
	
	/**
	 * Messaggio da allegare all'azione
	 */
	String getMessaggio();
	
	/**
	 * Messaggio da allegare all'azione
	 */
	void setMessaggio(String str);
}
