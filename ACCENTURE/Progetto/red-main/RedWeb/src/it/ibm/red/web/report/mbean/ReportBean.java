package it.ibm.red.web.report.mbean;


import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;

import com.google.common.net.MediaType;

import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.ParamsRicercaElencoDivisionaleDTO;
import it.ibm.red.business.dto.ReportDetailProtocolliPerCompetenzaDTO;
import it.ibm.red.business.dto.ReportMasterStatisticheUfficioUtenteDTO;
import it.ibm.red.business.dto.ReportMasterUfficioUtenteDTO;
import it.ibm.red.business.dto.RisultatoRicercaElencoDivisionaleDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CodaDiLavoroReportEnum;
import it.ibm.red.business.enums.PeriodoReportEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TargetNodoReportEnum;
import it.ibm.red.business.enums.TipoOperazioneReportEnum;
import it.ibm.red.business.enums.TipologiaNodoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.DettaglioReport;
import it.ibm.red.business.persistence.model.TipoOperazioneReport;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IElencoDivisionaleFacadeSRV;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.service.facade.IReportFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaAvanzataDocFacadeSRV;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;
import it.ibm.red.business.utils.CollectionUtils;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.report.mbean.component.RisultatiElencoDivisionaleReportComponent;
import it.ibm.red.web.report.mbean.component.RisultatiProtocolliPerCompetenzaReportComponent;
import it.ibm.red.web.report.mbean.component.RisultatiReportComponent;
import it.ibm.red.web.report.mbean.component.RisultatiRuoloReportComponent;
import it.ibm.red.web.report.mbean.component.RisultatiStatisticheUfficiReportComponent;
import it.ibm.red.web.report.model.ReportMascheraDTO;

/**
 * Bean che gestisce i report.
 */
@Named(ConstantsWeb.MBean.REPORT_BEAN)
@ViewScoped
public class ReportBean extends AbstractBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 4113540663154236082L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ReportBean.class);
	
	/**
	 * Tag di chiusura item di lista html.
	 */
	private static final String LI_CLOSE_TAG = "</li>";

	/**
	 * Label primo gennaio.
	 */
	private static final String UNO_GENNAIO = "01/01/";

	/**
	 * Servizio.
	 */
	private IReportFacadeSRV reportSRV;
	
	/**
	 * Servizio.
	 */
	private IOrganigrammaFacadeSRV organigrammaSRV;
	
	/**
	 * Servizio.
	 */
	private IRicercaAvanzataDocFacadeSRV ricercaAvanzataDocSRV;
	
	/**
	 * Servizio.
	 */
	private IElencoDivisionaleFacadeSRV elencoDivisionaleSRV;
	
	/**
	 * Servizio.
	 */
	private ITipologiaDocumentoFacadeSRV tipologiaDocumentoSRV;
	
	/**
	 * Utente recuperato dalla sessione.
	 */
	private UtenteDTO utente;
	
	
	/**
	 * Maschera.
	 */
	private ReportMascheraDTO maschera;
	
	/**
	 * Componente per la tabella di risultati dei documenti (anteprima e dettaglio).
	 */
	private RisultatiReportComponent risultati;
	
	/**
	 * Componente per la tabella di risultati degli utenti con specifico ruolo (anteprima e dettaglio).
	 */
	private RisultatiRuoloReportComponent risultatiRuolo;
	
	/**
	 * Componente per la tabella di risultati delle statistiche uffici (anteprima e dettaglio).
	 */
	private RisultatiStatisticheUfficiReportComponent risultatiStatisticheUffici;
	
	/**
	 * Componente per la tabella di risultati dei protocolli per competenza (dettaglio).
	 */
	private RisultatiProtocolliPerCompetenzaReportComponent risultatiProtocolliPerCompetenza;
	
	/**
	 * Componente per la tabella di risultati dell'elenco divisionale (dettaglio).
	 */
	private RisultatiElencoDivisionaleReportComponent risultatiElencoDivisionale;
	
	/**
	 * Export string.
	 */
	private String exportString;
	
	/**
	 * Flag disabilita anno.
	 */
	private boolean disabilitaAnno;
	
	/**
	 * Parametri di ricerca per report elenco divisionale.
	 */
	private ParamsRicercaElencoDivisionaleDTO parametriElencoDivisionale;
	
	/**
	 * Lista di risultati di ricerca per report elenco divisionale.
	 */
	private List<RisultatoRicercaElencoDivisionaleDTO> risultatiElencoDivisionaleList;
	
	/**
	 * Post construct del bean.
	 */
	@PostConstruct
	@Override
	protected void postConstruct() {
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		
		reportSRV = ApplicationContextProvider.getApplicationContext().getBean(IReportFacadeSRV.class);
		organigrammaSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
		ricercaAvanzataDocSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaAvanzataDocFacadeSRV.class);
		elencoDivisionaleSRV = ApplicationContextProvider.getApplicationContext().getBean(IElencoDivisionaleFacadeSRV.class);
		tipologiaDocumentoSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoFacadeSRV.class);
		
		initPage();
	}
	
	/**
	 * Inizializza il bean dei Report.
	 */
	public void initPage() {
		try {
			//INIT MASCHERA START --------------------------------------
			maschera = new ReportMascheraDTO();
			
			final List<PeriodoReportEnum> periodi = reportSRV.getComboPeriodo(); 
			maschera.setComboPeriodi(periodi);
			if (!CollectionUtils.isEmptyOrNull(periodi)) {
				maschera.setPeriodo(periodi.get(0));
			}

			final List<DettaglioReport> comboDettagli = reportSRV.getComboDettaglioReport(utente.getIdAoo());
			maschera.setComboDettaglioReport(comboDettagli);
			onChangeDettaglioReport();
			
			maschera.setComboRuoli(reportSRV.getAllRuoliReport(utente.getIdAoo()));
			maschera.setRuoliSelected(new ArrayList<>());
			
			final List<TipologiaDocumentoDTO> tipologieDocumento = ricercaAvanzataDocSRV.getListaTipologieDocumento(utente.getIdAoo());
			if ("-".equals(tipologieDocumento.get(0).getDescrizione())) {
				tipologieDocumento.get(0).setIdTipologiaDocumento(0);
			}
			maschera.setComboTipologieDocumento(tipologieDocumento);
			
			//INIT MASCHERA END ----------------------------------------
			
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Gestisce l'evento di modifica del periodo.
	 */
	public void onChangePeriodo() {
		try {
			if (maschera.getPeriodo() == PeriodoReportEnum.PERSONALE) {
				maschera.setDatePersonalizzateRendered(true);
				disabilitaAnno = true;	
			} else {
				maschera.setDatePersonalizzateRendered(false);
				maschera.setDataA(null);
				maschera.setDataDa(null);
				disabilitaAnno = false;
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}
	
	/**
	 * Gestisce l'evento di modifica del dettaglio report.
	 */
	public void onChangeDettaglioReport() {
		try {
			maschera.setRuoliSelected(new ArrayList<>());
			maschera.setNodiSelected(new ArrayList<>());
			
			if (CollectionUtils.isEmptyOrNull(maschera.getComboDettaglioReport())) {
				return;
			}
			
			if (StringUtils.isNullOrEmpty(maschera.getCodiceDettaglioSelected())) {
				maschera.setCodiceDettaglioSelected(maschera.getComboDettaglioReport().get(0).getCodice());
			}
			
			for (final DettaglioReport d : maschera.getComboDettaglioReport()) {
				if (d.getCodice().equals(maschera.getCodiceDettaglioSelected())) {
					
					maschera.setComboTipiOperazione(d.getTipiOperazione());
					if (!CollectionUtils.isEmptyOrNull(d.getTipiOperazione())) {
						maschera.setCodiceTipoOperazioneSelected(d.getTipiOperazione().get(0).getCodice());
						onChangeTipoOperazione();
					}

					maschera.setCodeDiLavoro(d.getCodeDiLavoro());
					if (!CollectionUtils.isEmptyOrNull(d.getCodeDiLavoro())) {
						maschera.setCodiceCodaDiLavoroSelected(d.getCodeDiLavoro().get(0).getCodice());
					}
					
					break;
				}
			}
			
			//GESTIONE ORGANIGRAMMA LAZY START
			if (TargetNodoReportEnum.UFFICIO.getDbCode().equals(maschera.getCodiceDettaglioSelected())) {
				if (maschera.getUfficioRoot() == null) {
					maschera.setUfficioRoot(loadRootOrganigramma(maschera.getCodiceDettaglioSelected()));
				}
				maschera.setRootSelected(maschera.getUfficioRoot());
			} else if (TargetNodoReportEnum.UTENTE.getDbCode().equals(maschera.getCodiceDettaglioSelected())) {
				if (maschera.getUtenteRoot() == null) {
					maschera.setUtenteRoot(loadRootOrganigramma(maschera.getCodiceDettaglioSelected()));
				}
				maschera.setRootSelected(maschera.getUtenteRoot());
			} 
			//GESTIONE ORGANIGRAMMA LAZY END
			
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}
	
	/**
	 * Gestisce l'evento di modifica del tipo operazione.
	 */
	public void onChangeTipoOperazione() {
		try {
			for (final TipoOperazioneReport t : maschera.getComboTipiOperazione()) {
				if (t.getCodice().equals(maschera.getCodiceTipoOperazioneSelected())) {
					final TipoOperazioneReportEnum tEnum = TipoOperazioneReportEnum.get(t.getCodice());
					maschera.setCodeRendered(
							tEnum == TipoOperazioneReportEnum.UTE_DOC_IN_CODA_LAVORO || tEnum == TipoOperazioneReportEnum.UFF_DOC_CODA_LAVORO);
					
					maschera.setRuoliRendered(tEnum == TipoOperazioneReportEnum.UFF_UTENTI_ATTIVI_RUOLO);
					
					maschera.setFiltriElencoDivisionaleRendered(TipoOperazioneReportEnum.UFF_ELENCO_DIVISIONALE.equals(tEnum) 
							|| TipoOperazioneReportEnum.UTE_ELENCO_DIVISIONALE.equals(tEnum));
					
					if (TargetNodoReportEnum.UFFICIO.getDbCode().equals(maschera.getCodiceDettaglioSelected())) {
						maschera.setUfficioRoot(loadRootOrganigramma(maschera.getCodiceDettaglioSelected()));
						maschera.setRootSelected(maschera.getUfficioRoot());
					} else if (TargetNodoReportEnum.UTENTE.getDbCode().equals(maschera.getCodiceDettaglioSelected())) {
						maschera.setUtenteRoot(loadRootOrganigramma(maschera.getCodiceDettaglioSelected()));
						maschera.setRootSelected(maschera.getUtenteRoot());
					}
					
					if (TipoOperazioneReportEnum.UTE_PROT_INEVASI_COMPETENZA.equals(tEnum)) {
						maschera.setPeriodo(PeriodoReportEnum.PERSONALE);
					} else {
						maschera.setPeriodo(PeriodoReportEnum.I_SEMESTRE);
					}
					maschera.setNodiSelected(new ArrayList<>());
				
					onChangePeriodo();
					
					break;
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
		
	}
	
	// Gestione albero start
	/**
	 * Recupero dei nodi del primo livello per la gestione dell'organigramma e inizializzazione della root.
	 * @param codice
	 * @return root dell'albero
	 */
	public TreeNode loadRootOrganigramma(final String codice) {	
		DefaultTreeNode root = null;
		try {
			//prendo da DB i nodi di primo livello
			final NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
			nodoRadice.setIdAOO(utente.getIdAoo());
			if (!TipoOperazioneReportEnum.isElencoDivisionale(maschera.getCodiceTipoOperazioneSelected())) {
				nodoRadice.setIdNodo(utente.getIdUfficio());
				nodoRadice.setDescrizioneNodo(utente.getNodoDesc());
			} else {
				final List<UfficioDTO> sottoNodi = organigrammaSRV.getSottoNodi(utente.getIdNodoRadiceAOO(), utente.getIdAoo());
				final Long idPrimoNodo = sottoNodi.get(0).getId();
				final String descPrimoNodo = sottoNodi.get(0).getDescrizione();
				nodoRadice.setIdNodo(idPrimoNodo);
				nodoRadice.setDescrizioneNodo(descPrimoNodo);
			}
			nodoRadice.setCodiceAOO(utente.getCodiceAoo());
			nodoRadice.setUtentiVisible(true);
			
			root = new DefaultTreeNode();
			root.setExpanded(true);
			root.setSelectable(true);
			
			// in questo modo riesco anche a visualizzare il nodo ufficio
			final TreeNode nodoUfficio = new DefaultTreeNode(nodoRadice, root);
			nodoUfficio.setExpanded(true);
			nodoUfficio.setSelectable(true); 
			
			retrieveAndPopulateChildren(nodoUfficio, codice);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	
		return root;
	}

	/**
	 * Gestisce l'evento "Expand" del nodo selezionato.
	 * @param event
	 */
	public void onOpenTree(final NodeExpandEvent event) {
		final TreeNode treeNode = event.getTreeNode();
		onOpenTree(treeNode);
	}
	
	/**
	 * Gestisce l'evento "Expand" del nodo selezionato.
	 * @param treeNode
	 */
	public void onOpenTree(final TreeNode treeNode) {
		try {
			final String codice = maschera.getCodiceDettaglioSelected();
			
			treeNode.getChildren().clear();
			
			retrieveAndPopulateChildren(treeNode, codice);
			
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	private void retrieveAndPopulateChildren(final TreeNode treeNode, final String codice) {
		try {
			final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) treeNode.getData();
			List<NodoOrganigrammaDTO> figli = null;
			if(TipoOperazioneReportEnum.isProtocolliPerCompetenza(maschera.getCodiceTipoOperazioneSelected())){
				boolean caricaDisattivi = false;
				try {
					Integer ricercaAttiva = Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.RICERCA_UTENTI_DISATTIVI));
					caricaDisattivi = ricercaAttiva == 1;
				}catch(Exception e) {
					LOGGER.error(e);
				}
				figli = retrieveOrganigramma(codice, nodoExp, caricaDisattivi);
			}else {
				figli = retrieveOrganigramma(codice, nodoExp, false);
			}
			
			DefaultTreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO n : figli) {
				n.setCodiceAOO(utente.getCodiceAoo());
				
				nodoToAdd = new DefaultTreeNode(n, treeNode);
				if (n.getFigli() > 0 || n.isUtentiVisible()) {
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}
	
	
	private List<NodoOrganigrammaDTO> retrieveOrganigramma(final String codice, final NodoOrganigrammaDTO nodo, final boolean caricaDisattivi) {
		if (TargetNodoReportEnum.UTENTE.getDbCode().equals(codice)) {
			return organigrammaSRV.getFigliAlberoCompletoUtenti(utente.getIdUfficio(),caricaDisattivi, nodo);
		} else if (TargetNodoReportEnum.UFFICIO.getDbCode().equals(codice)) {
			return organigrammaSRV.getFigliAlberoCompletoNoUtenti(utente.getIdUfficio(), nodo);
		} else {
			throw new RedException("Inizializzazione del dettaglio report non riuscita");
		}
	}
	
	/**
	 * Recupera tutti i nodi figli del nodo corrente.
	 * 
	 * @param nodo
	 * @param nodiOutput
	 */
	private void retrieveAllChildren(final NodoOrganigrammaDTO nodo, final List<NodoOrganigrammaDTO> nodiOutput) {
		List<NodoOrganigrammaDTO> childrenCurrentNode = organigrammaSRV.getFigliAlberoCompletoNoUtenti(utente.getIdUfficio(), nodo);
		if(!childrenCurrentNode.isEmpty()) {
			nodiOutput.addAll(childrenCurrentNode);
			for(NodoOrganigrammaDTO child: childrenCurrentNode) {
				retrieveAllChildren(child, nodiOutput);
			}
		}
	}
	
	/**
	 * Gestisce l'evento "Select" del nodo selezionato.
	 * @param event
	 */
	public void onSelectTree(final NodeSelectEvent event) {
		
		try {
			final TreeNode treeNode = event.getTreeNode();
			final NodoOrganigrammaDTO nodoSelected = (NodoOrganigrammaDTO) treeNode.getData();
			
			if (TipoOperazioneReportEnum.UTE_ELENCO_DIVISIONALE.name().equals(maschera.getCodiceTipoOperazioneSelected())) {
				for (NodoOrganigrammaDTO nodo : maschera.getNodiSelected()) {
					if (!nodoSelected.getIdNodo().equals(nodo.getIdNodo())) {
						showWarnMessage("Non è possibile selezionare un utente appartenente ad un altro ufficio.");
						return;
					}
				}
			}
			
			if (TipoOperazioneReportEnum.UFF_ELENCO_DIVISIONALE.name().equals(maschera.getCodiceTipoOperazioneSelected())) {
				for (NodoOrganigrammaDTO nodo : maschera.getNodiSelected()) {
					if (!nodoSelected.getIdNodo().equals(nodo.getIdNodo())) {
						showWarnMessage("Non è possibile selezionare più di un ufficio");
						return;
					}
				}
			}
			
			final List<NodoOrganigrammaDTO> nodiToProcess = new ArrayList<>();
			
			if (TargetNodoReportEnum.UTENTE.getDbCode().equals(maschera.getCodiceDettaglioSelected())) {
				//modalità utente
				//se seleziono un utente seleziono solo lui perché è una foglia
				//se seleziono un ufficio seleziono solo i suoi figli utenti
					//a meno che il tipo operazione non sia "Protocolli pervenuti per competenza", "Protocolli inevasi per competenza" o "Protocolli lavorati per competenza":
					//in tal caso aggiungo anche l'ufficio
				if (nodoSelected.getIdUtente() != null) {
					//se è un utente prendo solo lui
					nodiToProcess.add(nodoSelected);
				} else {
					//se è un ufficio
					
					// controllo che siano già stati caricati i figli
					// altrimenti li carico
					boolean isAlreadyLoaded = !treeNode.getChildren().isEmpty();
					NodoOrganigrammaDTO nodoFiglio = null;
					if (treeNode.getChildren().size() == 1) {
						// se però ha solo un figlio potrebbe essere quello fittizio messo per
						// dare la possibilità di aprire il nodo
						nodoFiglio = (NodoOrganigrammaDTO) treeNode.getData();
						isAlreadyLoaded = nodoFiglio.getIdNodo() == null;
					}
					
					if (!isAlreadyLoaded) {
						//se non sono già stati caricati i figli li devo caricare
						retrieveAndPopulateChildren(treeNode, maschera.getCodiceDettaglioSelected());
					}
					
					// Una volta caricati i figli metto nella lista da processare solo i nodi-utente
					if (!treeNode.getChildren().isEmpty()) {
						for (final TreeNode tn : treeNode.getChildren()) {
							nodoFiglio = (NodoOrganigrammaDTO) tn.getData();
							if (TipologiaNodoEnum.UTENTE.equals(nodoFiglio.getTipoNodo())) {
								nodiToProcess.add(nodoFiglio);
							}
						}
					}
					
					// Se report protocolli per competenza metto nella lista da processare anche il nodo
					if (TipoOperazioneReportEnum.isProtocolliPerCompetenza(maschera.getCodiceTipoOperazioneSelected())) {
						nodiToProcess.add(nodoSelected);
					}
					
				}
				
				// Se non è già presente in maschera viene aggiunto
				for (final NodoOrganigrammaDTO nodo : nodiToProcess) {
					if (!maschera.getNodiSelected().contains(nodo)) {
						maschera.getNodiSelected().add(nodo);
					}
				}
				
			} else if (TargetNodoReportEnum.UFFICIO.getDbCode().equals(maschera.getCodiceDettaglioSelected())) {
				// Se è un ufficio, viene aggiunto solo l'ufficio cliccato se non è già presente
				nodiToProcess.add(nodoSelected);
				
				
				if (!maschera.getNodiSelected().contains(nodoSelected)) {
					maschera.getNodiSelected().add(nodoSelected);
				}
			}
			
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
		
	}
	/* Gestione albero end*********************************************************************************/
	
	/**
	 * Gestisce la logica di rimozione del nodo in ingresso. Mostra eventuali errori.
	 * @param nodoToRemove
	 */
	public void onRemoveNodo(final NodoOrganigrammaDTO nodoToRemove) {
		
		try {
			final StringBuilder sbToRemove = new StringBuilder();
			sbToRemove.append(nodoToRemove.getIdNodo()).append(nodoToRemove.getIdUtente());
			final StringBuilder sb = new StringBuilder();
			int i = -1;
			for (final NodoOrganigrammaDTO n : maschera.getNodiSelected()) {
				i++;
				sb.append(n.getIdNodo()).append(n.getIdUtente());
				if (sb.toString().equals(sbToRemove.toString())) {
					break;
				}
				sb.setLength(0);
			}
			if (i > -1) {
				maschera.getNodiSelected().remove(i);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
		
	}
	
	/**
	 * Esegue una validazione della maschera.
	 * @return true se validata senza errori, false altrimenti
	 */
	public boolean validateMaschera() {
		final StringBuilder sb = new StringBuilder("<ul>");
		boolean isValid = true;
		try {
			if (!TipoOperazioneReportEnum.isElencoDivisionale(maschera.getCodiceTipoOperazioneSelected()) && maschera.getAnno() <= 0) {
				sb.append("<li>").append("L'anno deve essere valorizzato.").append(LI_CLOSE_TAG);
				isValid = false;
			}
			
			if (!TipoOperazioneReportEnum.UTE_PROT_INEVASI_COMPETENZA.name().equals(maschera.getCodiceTipoOperazioneSelected())) {
				if (maschera.isDatePersonalizzateRendered() && (maschera.getDataA() == null || maschera.getDataDa() == null 
						|| (maschera.getDataDa() != null && maschera.getDataA() != null && maschera.getDataDa().after(maschera.getDataA())))) {
					sb.append("<li>").append("Inserire due date valide. La data DA deve essere precedente alla data A.").append(LI_CLOSE_TAG);
					isValid = false;
				} else if (maschera.getDataDa() != null && maschera.getDataA() != null && DateUtils.maxDeltaGiorni(maschera.getDataDa(), maschera.getDataA(), 365)) {
					sb.append("<li>").append("Il range temporale non può essere piu ampio di 365 giorni").append(LI_CLOSE_TAG);
					isValid = false;
				}
			} else {
				if (maschera.isDatePersonalizzateRendered() && maschera.getDataA() == null) {
					sb.append("<li>").append("Inserire una data valida.").append(LI_CLOSE_TAG);
					isValid = false;
				}
			}
			
			if (maschera.getNodiSelected().isEmpty()) {
				sb.append("<li>").append("Selezionare almeno un elemento dall'organigramma.").append(LI_CLOSE_TAG);
				isValid = false;
			}
			
			if (maschera.isRuoliRendered() && maschera.getRuoliSelected().isEmpty()) {
				sb.append("<li>").append("Selezionare almeno un ruolo.").append(LI_CLOSE_TAG);
				isValid = false;
			}
			
			if(maschera.getTipoOperazioneSelected() == null) {
				sb.append("<li>").append("Selezionare la tipologia dell'operazione.").append(LI_CLOSE_TAG);
				isValid = false;
			}
			
			if (maschera.isFiltriElencoDivisionaleRendered() && maschera.getDataAssegnazione() == null) {
				sb.append("<li>").append("Inserire una data valida.").append(LI_CLOSE_TAG);
				isValid = false;
			}
			
			if (maschera.isFiltriElencoDivisionaleRendered() && maschera.getTipoAssegnazione() == null) {
				sb.append("<li>").append("Selezionare il tipo di assegnazione.").append(LI_CLOSE_TAG);
				isValid = false;
			}
			
			if (TipoOperazioneReportEnum.UFF_ELENCO_DIVISIONALE.name().equals(maschera.getCodiceTipoOperazioneSelected())) {
				final int numNodiSelected = maschera.getNodiSelected().size();
				if (numNodiSelected > 1) {
					maschera.getNodiSelected().removeAll(maschera.getNodiSelected().subList(0, numNodiSelected - 1));
				}
			}
			
			sb.append("</ul>");
			
			if (!isValid) {
				showWarnMessage(sb.toString());
			}
			
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
		
		return isValid;
	}
	
	/**
	 * Effettua la creazione del report memorizzando i parametri per il recupero in {@link #risultati}.
	 */
	public void creaReport() {
		try {
			final Calendar dataInizio = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			final Calendar dataFine = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			
			impostaDateInizioFine(maschera, dataInizio, dataFine);
			
			if (!validateMaschera()) {
				return;
			}
			
			resetRisultati();
			
			final TipoOperazioneReport tSelected = maschera.getTipoOperazioneSelected();
			
			if (!TipoOperazioneReportEnum.isStatisticheUffici(maschera.getCodiceTipoOperazioneSelected())) {
			
				final List<Long> uffici = new ArrayList<>();
				final List<Long> utenti = new ArrayList<>();
				
				for (final NodoOrganigrammaDTO n : maschera.getNodiSelected()) {
					if (!uffici.contains(n.getIdNodo())) {
						uffici.add(n.getIdNodo());
					}
					
					if (TipoOperazioneReportEnum.UFF_ELENCO_DIVISIONALE.name().equals(maschera.getCodiceTipoOperazioneSelected())
							&& maschera.isCercaSottouffici()) {
						final List<NodoOrganigrammaDTO> nodiOutput = new ArrayList<>();
						retrieveAllChildren(n, nodiOutput);
						for (final NodoOrganigrammaDTO nodo : nodiOutput) {
							if (!uffici.contains(nodo.getIdNodo())) {
								uffici.add(nodo.getIdNodo());
							}
						}
					}

					if (n.getIdUtente() != null && n.getIdUtente().longValue() > 0 && !utenti.contains(n.getIdUtente())) {
						utenti.add(n.getIdUtente());
					} else if (n.getIdUtente() == null && TipoOperazioneReportEnum.isProtocolliPerCompetenza(maschera.getCodiceTipoOperazioneSelected())){
						// La ricerca deve avvenire a livello ufficio quindi includo il corriere.
						utenti.add(0L);
					}
				}
				
				final Long[] uffArr = new Long[uffici.size()];
				uffici.toArray(uffArr);
				
				
				if (isRuoloUtente()) {
					
					final Long[] ruoliArr = new Long[maschera.getRuoliSelected().size()];
					maschera.getRuoliSelected().toArray(ruoliArr);
					
					risultatiRuolo = new RisultatiRuoloReportComponent();
					if (tSelected != null) {
						risultatiRuolo.setDescrizioneOperazione(tSelected.getDescrizione());
					}
					risultatiRuolo.setMasterList(reportSRV.ricercaMasterUtentiAttiviConUnDatoRuoloForUff(uffArr, ruoliArr));
					
					exportString = "Utente_" + risultatiRuolo.getDescrizioneOperazione().replace(' ', '_');
					
				} else {
					
					Long[] uteArr = new Long[utenti.size()];
					utenti.toArray(uteArr);
					
					if (uteArr.length == 0) {
						uteArr = null;
					}
					
					if (!TipoOperazioneReportEnum.isProtocolliPerCompetenza(maschera.getCodiceTipoOperazioneSelected())
							&& !TipoOperazioneReportEnum.isElencoDivisionale(maschera.getCodiceTipoOperazioneSelected())) {
						
						final List<ReportMasterUfficioUtenteDTO> masterList = reportSRV.ricercaMasterDocumenti(
								utente.getIdAoo(), dataInizio, dataFine, uffArr, uteArr, 
								TipoOperazioneReportEnum.get(maschera.getCodiceTipoOperazioneSelected()), 
								CodaDiLavoroReportEnum.get(maschera.getCodiceCodaDiLavoroSelected()));
						
						risultati = new RisultatiReportComponent(utente.getIdAoo());
						risultati.setMasterList(masterList);
						risultati.setTipoOperazioneSelected(tSelected);
						risultati.setCodaDiLavoroSelected(maschera.getCodaDiLavoroSelected());
						risultati.setRicercaPerUtente(uteArr != null);
						
						exportString = "Ispettorato_" + risultati.getDescrizioneOperazione().replace(' ', '_');
						
					} else if (TipoOperazioneReportEnum.isProtocolliPerCompetenza(maschera.getCodiceTipoOperazioneSelected())) {
						
						final List<ReportDetailProtocolliPerCompetenzaDTO> detailList = reportSRV.ricercaDetailProtocolliPerCompetenza(
								utente.getIdAoo(), dataInizio, dataFine, uffArr, uteArr, tSelected);
						
						DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("centralSectionForm:idTabellaDettaglioCompetenza");
						if (dataTable != null) {
							dataTable.reset();
						}
						
						risultatiProtocolliPerCompetenza = new RisultatiProtocolliPerCompetenzaReportComponent(utente.getIdAoo());
						risultatiProtocolliPerCompetenza.setDetailList(detailList);
						risultatiProtocolliPerCompetenza.setDescrizioneOperazione(tSelected.getDescrizione());
						
						exportString = risultatiProtocolliPerCompetenza.getDescrizioneOperazione().replace(' ', '_');
						
					} else if (TipoOperazioneReportEnum.isElencoDivisionale(maschera.getCodiceTipoOperazioneSelected())) {
						
						// Popolamento parametri di ricerca report elenco divisionale -> START
						final Date dataAssegnazione = maschera.getDataAssegnazione();
						final UfficioDTO ufficioAssegnante = new UfficioDTO(utente.getIdUfficio(), utente.getNodoDesc());
						final UfficioDTO ufficioAssegnatario = new UfficioDTO(maschera.getNodiSelected().get(0).getIdNodo(), maschera.getNodiSelected().get(0).getDescrizioneNodo());
						parametriElencoDivisionale = new ParamsRicercaElencoDivisionaleDTO(dataAssegnazione, ufficioAssegnante, ufficioAssegnatario);
						
						if (maschera.getIdTipologiaDocumentoSelected() != 0) {
							final String tipologiaDocumento = tipologiaDocumentoSRV.getDescTipologiaDocumentoById(maschera.getIdTipologiaDocumentoSelected());
							parametriElencoDivisionale.setTipologiaDocumento(tipologiaDocumento);
						}
						final List<UtenteDTO> utentiAssegnatari = new ArrayList<>();
						for (final NodoOrganigrammaDTO nodo : maschera.getNodiSelected()) {
							if (nodo.getIdUtente() != null) {
								final UtenteDTO utenteAssegnatario = new UtenteDTO();
								utenteAssegnatario.setId(nodo.getIdUtente());
								utenteAssegnatario.setNome(nodo.getNomeUtente());
								utenteAssegnatario.setCognome(nodo.getCognomeUtente());
								
								utentiAssegnatari.add(utenteAssegnatario);
							}
						}
						parametriElencoDivisionale.setUtentiAssegnatari(utentiAssegnatari);
						// Popolamento parametri di ricerca report elenco divisionale -> END
						
						final String tipoAssegnazione = maschera.getTipoAssegnazione();
						final Integer numeroProtocolloDa = maschera.getNumeroProtocolloDa();
						Integer idTipologiaDocumento = null;
						if (maschera.getIdTipologiaDocumentoSelected() != 0) {
							idTipologiaDocumento = maschera.getIdTipologiaDocumentoSelected();
						}
						risultatiElencoDivisionaleList = reportSRV.ricercaDetailElencoDivisionale(
								utente.getIdAoo(), parametriElencoDivisionale, uffArr, uteArr, tSelected, tipoAssegnazione, numeroProtocolloDa, idTipologiaDocumento);
						
						DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("centralSectionForm:idTabellaDettaglioElencoDivisionale");
						if (dataTable != null) {
							dataTable.reset();
						}
						
						risultatiElencoDivisionale = new RisultatiElencoDivisionaleReportComponent(utente.getIdAoo());
						risultatiElencoDivisionale.setDetailList(risultatiElencoDivisionaleList);
						final String descrizioneOperazione = tSelected.getDescrizione();
						final String descrizioneUfficioAssegnatario = ufficioAssegnatario.getDescrizione();
						final StringBuilder sbHeader = new StringBuilder();
						risultatiElencoDivisionale.setDescrizioneOperazione(descrizioneOperazione);
						risultatiElencoDivisionale.setDetailTableHeader(sbHeader.append(descrizioneOperazione).append(" - ").append(descrizioneUfficioAssegnatario));
						
						exportString = descrizioneOperazione.replace(' ', '_');
						
					}
					
				}
				
			} else {
				
				final TargetNodoReportEnum tipoStruttura = TargetNodoReportEnum.getByDBCode(maschera.getCodiceDettaglioSelected());
				
				final ReportMasterStatisticheUfficioUtenteDTO form = 
						new ReportMasterStatisticheUfficioUtenteDTO(maschera.getNodiSelected(), tipoStruttura, 
								"" + maschera.getAnno(), dataInizio.getTime(), dataFine.getTime());
				
				final List<ReportMasterStatisticheUfficioUtenteDTO> statisticheUfficiMaster = reportSRV.getStatisticheUfficiMaster(form);
				
				risultatiStatisticheUffici = new RisultatiStatisticheUfficiReportComponent(tipoStruttura);
				risultatiStatisticheUffici.setMasterList(statisticheUfficiMaster);
				if (tSelected != null) {
					risultatiStatisticheUffici.setDescrizioneOperazione(tSelected.getDescrizione());
				}
				exportString = risultatiStatisticheUffici.getDescrizioneOperazione().replace(' ', '_');
				
			}
			
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}
	
	private void resetRisultati() {
		risultati = null;
		risultatiRuolo = null;
		risultatiStatisticheUffici = null;
		risultatiProtocolliPerCompetenza = null;
		risultatiElencoDivisionale = null;
	}
	
	private void impostaDateInizioFine(final ReportMascheraDTO maschera,  final Calendar dataInizio, final Calendar dataFine) throws ParseException {
		final SimpleDateFormat ddmmyyyy = new SimpleDateFormat(DateUtils.DD_MM_YYYY);
		
		if (maschera.getPeriodo() != null) {
			switch (maschera.getPeriodo()) {
			case AD_OGGI:
				dataInizio.setTime(ddmmyyyy.parse(UNO_GENNAIO + maschera.getAnno()));
				dataFine.setTime(new Date());
				break;
				
			case I_SEMESTRE:
				dataInizio.setTime(ddmmyyyy.parse(UNO_GENNAIO + maschera.getAnno()));
				dataFine.setTime(ddmmyyyy.parse("30/06/" + maschera.getAnno()));
				break;
				
			case II_SEMESTRE:
				dataInizio.setTime(ddmmyyyy.parse("01/07/" + maschera.getAnno()));
				dataFine.setTime(ddmmyyyy.parse("31/12/" + maschera.getAnno()));
				break;
				
			case PERSONALE: // Sono già impostate dall'utente
				if (maschera.getDataDa() != null) {
					dataInizio.setTime(maschera.getDataDa());
				}
				if (maschera.getDataA() != null) {
					dataFine.setTime(maschera.getDataA());
				}
				break;
				
			case ANNUALE:
			default:
				dataInizio.setTime(ddmmyyyy.parse(UNO_GENNAIO + maschera.getAnno()));
				dataFine.setTime(ddmmyyyy.parse("31/12/" + maschera.getAnno()));
				break;
			}
			
			dataInizio.setTime(DateUtils.setDateTo0000(dataInizio.getTime())); // Si imposta la data di inizio alle 00:00:00
			dataFine.setTime(DateUtils.setDateTo2359(dataFine.getTime())); // Si imposta la data di fine alle 23:59:59
		}
	}
	
	/**
	 * Restituisce true se il report è associato ad un utente e non ad un ufficio.
	 * @return true se report associato ad un utente, false se associato ad un ufficio.
	 */
	public boolean isRuoloUtente() {
		final TipoOperazioneReportEnum torEnum = TipoOperazioneReportEnum.get(maschera.getCodiceTipoOperazioneSelected());
		return  TipoOperazioneReportEnum.UFF_UTENTI_ATTIVI_RUOLO == torEnum;
	}
	
	/**
	 * Restituisce lo streamed content.
	 */
	public StreamedContent downloadPdf() {
		StreamedContent sc = null;
		try {
			// recupero del content
			final InputStream stream = new ByteArrayInputStream(elencoDivisionaleSRV.generaPdfReport(parametriElencoDivisionale, risultatiElencoDivisionaleList, utente.getAooDesc()));
			sc = new DefaultStreamedContent(stream, MediaType.PDF.toString(), exportString + ".pdf");
		} catch (final Exception e) {
			LOGGER.error("Errore durante il download del documento", e);
			showError(e);
		}
		return sc;
	}

	/* GET & SET ***************************************************************************************/
	/**
	 * Restituisce le informazioni sulla maschera.
	 * @return DTO maschera
	 */
	public ReportMascheraDTO getMaschera() {
		return maschera;
	}

	/**
	 * Restituisce il component per la gestione dei risultati.
	 * @return component
	 */
	public RisultatiReportComponent getRisultati() {
		return risultati;
	}

	/**
	 * Restituisce il component per la gestione dei risultati ruolo.
	 * @return il component
	 */
	public RisultatiRuoloReportComponent getRisultatiRuolo() {
		return risultatiRuolo;
	}
	
	/**
	 * Restituisce il component per la gestione del risultati del report statistiche uffici.
	 * @return il component
	 */
	public RisultatiStatisticheUfficiReportComponent getRisultatiStatisticheUffici() {
		return risultatiStatisticheUffici;
	}
	
	/**
	 * Restituisce il component per la gestione dei risultati dei report protocolli per competenza.
	 * 
	 * @return component
	 */
	public RisultatiProtocolliPerCompetenzaReportComponent getRisultatiProtocolliPerCompetenza() {
		return risultatiProtocolliPerCompetenza;
	}

	/**
	 * Restituisce il component per la gestione dei risultati del report elenco divisionale.
	 * 
	 * @return component
	 */
	public RisultatiElencoDivisionaleReportComponent getRisultatiElencoDivisionale() {
		return risultatiElencoDivisionale;
	}

	/**
	 * Restituisce il nome del file da esportare.
	 * @return nome file di export
	 */
	public String getExportString() {
		final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy-HHmmss");
		return exportString + "_" + sdf.format(new Date());
	}

	/**
	 * Restituisce true se disabilita anno.
	 * @return true se disabilita anno, false altrimenti
	 */
	public boolean isDisabilitaAnno() {
		return disabilitaAnno;
	}

	/**
	 * @param disabilitaAnno
	 */
	public void setDisabilitaAnno(final boolean disabilitaAnno) {
		this.disabilitaAnno = disabilitaAnno;
	}

	/**
	 * Restituisce i parametri di ricerca per il report elenco divisionale.
	 * 
	 * @return parametri di ricerca
	 */
	public ParamsRicercaElencoDivisionaleDTO getParametriElencoDivisionale() {
		return parametriElencoDivisionale;
	}

	/**
	 * Imposta i parametri di ricerca per il report elenco divisionale.
	 * 
	 * @param parametriElencoDivisionale
	 */
	public void setParametriElencoDivisionale(ParamsRicercaElencoDivisionaleDTO parametriElencoDivisionale) {
		this.parametriElencoDivisionale = parametriElencoDivisionale;
	}

}