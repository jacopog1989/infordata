package it.ibm.red.web.document.mbean.component;

import java.util.List;

import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * @author m.crescentini
 *
 */
public class AssCompetenzaConUtentiOrganigrammaComponent extends AbstractAssOrganigrammaComponent {
	
	private static final long serialVersionUID = -5210528877897084808L;
	
	/**
	 * Csotruttore.
	 * 
	 * @param inUtente
	 */
	public AssCompetenzaConUtentiOrganigrammaComponent(final UtenteDTO inUtente, final boolean loadRootOrganigramma, final boolean ricercaDisattivi) {
		init(inUtente);
		this.setRicercaDisattivi(ricercaDisattivi);
		if (loadRootOrganigramma) {
			reset();
		}
	}
	
	/**
	 * @see it.ibm.red.web.mbean.AbstractAssegnazioneOrganigrammaBean#getPrimoLivelloOrganigramma(it.ibm.red.business.dto.NodoOrganigrammaDTO).
	 */
	@Override
	protected List<NodoOrganigrammaDTO> getPrimoLivelloOrganigramma(final NodoOrganigrammaDTO radice) {
		return getOrganigrammaSRV().getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovoConUtenti(true, getUtente().getIdUfficio(), radice, null, isRicercaDisattivi());
	}

	/**
	 * @see it.ibm.red.web.mbean.AbstractAssegnazioneOrganigrammaBean#getSecondoLivelloOrganigramma(it.ibm.red.business.dto.NodoOrganigrammaDTO).
	 */
	@Override
	protected List<NodoOrganigrammaDTO> getSecondoLivelloOrganigramma(final NodoOrganigrammaDTO nodoPrimoLivello) {
		return getOrganigrammaSRV().getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovoConUtenti(true, getUtente().getIdUfficio(), nodoPrimoLivello, null, isRicercaDisattivi());
	}
	
	/**
	 * @see it.ibm.red.web.mbean.AbstractAssegnazioneOrganigrammaBean#isOrganigrammaConUtentiVisibili().
	 */
	@Override
	protected boolean isOrganigrammaConUtentiVisibili() {
		return true;
	}
	
	/**
	 * @see it.ibm.red.web.mbean.AbstractAssegnazioneOrganigrammaBean#isRootOrganigrammaSelezionabile().
	 */
	@Override
	protected boolean isRootOrganigrammaSelezionabile() {
		return false;
	}
	
}
