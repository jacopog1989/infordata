package it.ibm.red.web.utils;

import javax.faces.component.UIColumn;
import javax.faces.component.UIComponent;

/**
 * Utility di export.
 */
public final class ExportUtils {
	
	/**
	 * Costruttore vuoto.
	 */
	private ExportUtils() {
		// Costruttore vuoto.
	}
	
	/**
	 * Metodo per esportare i valori corretti di una colonna (presi dall'attributo data-export).
	 * 
	 * @param column
	 * @return la stringa da scrivere nella colonna del file esportato
	 */
	public static String columnCorrectValue(final UIColumn column) {
	       final StringBuilder value = new StringBuilder("");
	       
	       for (final UIComponent child : column.getChildren()) {
	    	   final Object exportValue = child.getAttributes().get("data-export");
	    	   if (exportValue != null) {
	    		   value.append(String.valueOf(exportValue));
	    	   }
	       }
			return value.toString();
	    }

}
