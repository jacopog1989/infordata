package it.ibm.red.web.mbean;

import java.util.Collection;
import java.util.List;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipologiaNodoEnum;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.INotaFacadeSRV;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.service.facade.IRiassegnazioneFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * Abstract bean assegnazioni.
 */
public abstract class AbstractAssegnazioniBean extends AbstractBean implements InitHumanTaskInterface {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 2279823404791167530L;
	
	/**
	 * Lista dei nodi che compongono l'alberatura.
	 * */
	protected transient List<Nodo> alberaturaNodi;
	
	/**
	 * Descrizione nuova assegnazione.
	 */
	private String descrizioneNewAssegnazione;
	
	/**
	 * Motivo assegnazione.
	 */
	private String motivoAssegnazione;
	
	/**
	 * Nodo root.
	 */
	private transient TreeNode root;
	
	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Servizio organigramma.
	 */
	private IOrganigrammaFacadeSRV orgSRV;

	/**
	 * Handler lista documenti bean.
	 */
	private ListaDocumentiBean ldb;
	
	/**
	 * Nodo selezionato.
	 */
	private NodoOrganigrammaDTO selected;
	
	/**
	 * Collezione dei master.
	 */
	private Collection<MasterDocumentRedDTO> masters;

	/**
	 * Service per la gestione della response.
	 */
	private IRiassegnazioneFacadeSRV riassegnaSRV;
	
	/**
	 * Falg anche storico.
	 */
	private boolean alsoStorico;

	/**
	 * Servizio gestione nota.
	 */
	private INotaFacadeSRV notaSRV;
	
	/**
	 * Carica l'organigramma.
	 */
	protected abstract void loadRootOrganigramma();

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDococsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDococsSelezionati) {
		masters = inDococsSelezionati;
	}
	
	
	/**
	 * Metodo che consente l'inizializzazione del bean.
	 */
	protected void init() {
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
		final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sb.getUtente();
		orgSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
		riassegnaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRiassegnazioneFacadeSRV.class);
		notaSRV = ApplicationContextProvider.getApplicationContext().getBean(INotaFacadeSRV.class);
		alberaturaNodi = orgSRV.getAlberaturaBottomUp(utente.getIdAoo(), utente.getIdUfficio());
		loadRootOrganigramma();
	}
	
	
	/**
	 * Metodo che definisce le azioni da svolgere alla selezione di un nodo.
	 * 
	 * @param event evento di selezione nodo
	 */
	public void onSelectedNode(final NodeSelectEvent event) {
		// Recupero l'utente selezionato dall'evento e lo utilizzo poi per l'invocazione del service
		setSelected((NodoOrganigrammaDTO) event.getTreeNode().getData());
		
		// Dall'oggetto recuperato ottengo i dati da mostrare nella casella di testo
		if (getSelected().getTipoNodo().equals(TipologiaNodoEnum.UFFICIO)) {
			setDescrizioneNewAssegnazione(getSelected().getDescrizioneNodo());
		} else {
			setDescrizioneNewAssegnazione(getSelected().getDescrizioneNodo() + " - " +  getSelected().getCognomeUtente() + " " + getSelected().getNomeUtente());
		}
	}
	
	
	/**
	 * Restituisce true se il nodo è stato selezionato correttamente, false altrimenti.
	 * 
	 * @return il permesso di continuare
	 */
	public boolean permessoContinua() {
		boolean continua = false;
		
		if (getSelected() != null && (getSelected().getIdUtente() != null || getSelected().getIdNodo() != null)) {
			continua = true;
		}
		
		return continua;
	}
	
	/**
	 * Deseleziona l'assegnazione svuotando i parametri.
	 * */
	public void clearAssegnazione() {
		selected = null;
		descrizioneNewAssegnazione = Constants.EMPTY_STRING;
	}

	/**
	 * @return the descrizioneNewAssegnazione
	 */
	public String getDescrizioneNewAssegnazione() {
		return descrizioneNewAssegnazione;
	}

	/**
	 * Setter della descrizione della nuova assegnazione.
	 * 
	 * @param descrizioneNewAssegnazione la descrizione da impostare alla nuova assegnazione
	 */
	public void setDescrizioneNewAssegnazione(final String descrizioneNewAssegnazione) {
		this.descrizioneNewAssegnazione = descrizioneNewAssegnazione;
	}

	/**
	 * @return the motivoAssegnazione
	 */
	public String getMotivoAssegnazione() {
		return motivoAssegnazione;
	}

	/**
	 * @param motivoAssegnazione the motivoAssegnazione to set
	 */
	public void setMotivoAssegnazione(final String motivoAssegnazione) {
		this.motivoAssegnazione = motivoAssegnazione;
	}

	/**
	 * @return the root
	 */
	public final TreeNode getRoot() {
		return root;
	}

	/**
	 * @return the selected
	 */
	public NodoOrganigrammaDTO getSelected() {
		return selected;
	}

	/**
	 * @param selected the selected to set
	 */
	public void setSelected(final NodoOrganigrammaDTO selected) {
		this.selected = selected;
	}
	
	/**
	 * Getter del DTO dell'utente.
	 * 
	 * @return il DTO dell'utente
	 * */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Getter della facade dell'OrganigrammaSRV.
	 * 
	 * @return Il Service per gestire l'organigramma
	 * */
	public IOrganigrammaFacadeSRV getOrgSRV() {
		return orgSRV;
	}
	
	/**
	 * Getter di ListaDocumentiBean.
	 * 
	 * @return Il bean ListaDocumentiBean
	 * */
	public ListaDocumentiBean getLdb() {
		return ldb;
	}
	
	/**
	 * Setter della radice dell'albero.
	 * 
	 * @param root
	 * */
	public void setRoot(final TreeNode root) {
		this.root = root;
	}
	
	/**
	 * Getter della facade RiassegnazioneFacadeSRV.
	 * 
	 * @return RiassegnaSRV
	 * */
	public IRiassegnazioneFacadeSRV getRiassegnaSRV() {
		return riassegnaSRV;
	}

	/**
	 * Getter dei masters.
	 * 
	 * @return La collection dei Master.
	 * */
	public Collection<MasterDocumentRedDTO> getMasters() {
		return masters;
	}

	/**
	 * Getter della variabile alsoStorico.
	 * 
	 * @return true se è fa riferimento allo Storico, false altrimenti
	 * */
	public boolean isAlsoStorico() {
		return alsoStorico;
	}

	/**
	 * Setter di alsoStorico.
	 * 
	 * @param alsoStorico
	 * */
	public void setAlsoStorico(final boolean alsoStorico) {
		this.alsoStorico = alsoStorico;
	}

	/**
	 * Getter della facade INotaFacadeSRV.
	 * 
	 * @return la facade.
	 * */
	public INotaFacadeSRV getNotaSRV() {
		return notaSRV;
	}

}
