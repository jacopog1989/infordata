package it.ibm.red.web.document.mbean.component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.SelectEvent;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.TemplateDTO;
import it.ibm.red.business.dto.TemplateMetadatoDTO;
import it.ibm.red.business.dto.TemplateMetadatoValueDTO;
import it.ibm.red.business.dto.TemplateMetadatoVersion;
import it.ibm.red.business.dto.TemplateValueDTO;
import it.ibm.red.business.dto.TestoPredefinitoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ITemplateDocUscitaFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.constants.ConstantsWeb.SessionObject;
import it.ibm.red.web.document.mbean.DocumentManagerBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.interfaces.IPopupComponent;

/**
 * Component per la gestione della dialog dei template applicativi per documenti
 * in uscita.
 * 
 * @author m.crescentini
 */
public class TemplateDocUscitaComponent extends AbstractComponent implements IPopupComponent {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 2545414146610312223L;

	/**
	 * Messaggio errore generazione anteprima documento.
	 */
	private static final String ERROR_GENERAZIONE_ANTEPRIMA_DOCUMENTO_MSG = "Si è verificato un errore durante la generazione dell'anteprima del documento";
	
	/**
	 * Messaggio errore generazione anteprima documento.
	 */
	private static final String ERROR_GENERAZIONE_DOCUMENTO_MSG = "Si è verificato un errore durante la generazione del documento";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TemplateDocUscitaComponent.class);

	/**
	 * Utente.
	 */
	private final UtenteDTO utente;

	/**
	 * Lista template.
	 */
	private List<TemplateDTO> templateList;

	/**
	 * Template selezionato.
	 */
	private TemplateDTO templateSelected;

	/**
	 * Id template.
	 */
	private String idTemplateSelected;

	/**
	 * Nome documento.
	 */
	private String nomeDocumento;

	/**
	 * Servizio.
	 */
	private ITemplateDocUscitaFacadeSRV templateDocUscitaSRV;

	/**
	 * Lista metadati template (view).
	 */
	private List<TemplateMetadatoView> metadatiView;

	/**
	 * Lista metadati template.
	 */
	private List<TemplateMetadatoValueDTO> metadati;

	/**
	 * Detail.
	 */
	private DetailDocumentRedDTO detail;

	/**
	 * Flag modifiche non visualizzate.
	 */
	private boolean modificheNonVisualizzate;

	/**
	 * Flag visualizza anteprima.
	 */
	private boolean visualizzaAnteprima;

	/**
	 * Lista versioni template.
	 */
	private List<TemplateMetadatoVersion> templateVersion;

	/**
	 * Versione template.
	 */
	private TemplateMetadatoVersion templateVersionSelected;

	/**
	 * Flag metadati template.
	 */
	private boolean showMetadatiTemplate;

	/**
	 * Lista template comparabili.
	 */
	private List<TemplateMetadatoVersion> templateComparabili;

	/**
	 * Template A.
	 */
	private TemplateMetadatoVersion templateConfrontato;

	/**
	 * Template B.
	 */
	private TemplateMetadatoVersion templateConfrontante;

	/**
	 * Metadati template A.
	 */
	private List<TemplateMetadatoView> metadatiViewConfrontato;

	/**
	 * Metadati template B.
	 */
	private List<TemplateMetadatoView> metadatiViewConfrontante;

	/**
	 * Modalità confronto.
	 */
	private boolean modalitaConfronto;

	/**
	 * Flag disabilita comparazione metadati.
	 */
	private boolean disabilitaComparazioneMetadati;

	/**
	 * Nome documento confrontato.
	 */
	private String nomeDocumentoConfrontato;

	/**
	 * Costruttore di default.
	 * 
	 * @param inUtente
	 */
	public TemplateDocUscitaComponent(final UtenteDTO inUtente) {
		utente = inUtente;
		init();
	}

	/**
	 * Inizializza il component.
	 */
	public void init() {
		try {
			metadati = new ArrayList<>();
			metadatiView = new ArrayList<>();

			templateDocUscitaSRV = ApplicationContextProvider.getApplicationContext().getBean(ITemplateDocUscitaFacadeSRV.class);
			templateList = templateDocUscitaSRV.getTemplateDocUscita(utente);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore in fase di inizializzazione della maschera per i template applicativi.", e);
			throw new RedException(e);
		}
	}

	/**
	 * Operazioni da eseguire alla selezione di un template.
	 */
	public void onSelectTemplate() {
		metadatiView.clear();
		modalitaConfronto = false;
		visualizzaAnteprima = false;

		if (StringUtils.isNotBlank(idTemplateSelected)) {
			showMetadatiTemplate = true;
			for (final TemplateDTO t : templateList) {
				if (t.getIdTemplate().equals(idTemplateSelected)) {
					templateSelected = t;
					break;
				}
			}
			final List<TemplateMetadatoDTO> metadatiTemplate = templateDocUscitaSRV.getMetadati(utente, templateSelected);

			for (final TemplateMetadatoDTO metadato : metadatiTemplate) {
				metadatiView.add(new TemplateMetadatoView(metadato));
			}
			modificheNonVisualizzate = true;

			impostaNomeDocumento();
		} else {
			showMetadatiTemplate = false;
			templateSelected = null;
			nomeDocumento = Constants.EMPTY_STRING;
			modificheNonVisualizzate = false;
		}
	}

	/**
	 * Operazioni da eseguire alla selezione di un testo predefinito.
	 * 
	 * @param metadato Metadato con testi predefiniti da selezionare
	 */
	public void onSelectTestoPredefinito(final TemplateMetadatoView metadato) {
		if (metadato != null) {
			if (metadato.getTestoSelected() != null) {
				metadato.setValue(metadato.getTestoSelected().getCorpotesto());
			} else {
				metadato.setValue(Constants.EMPTY_STRING);
			}
			modificheNonVisualizzate = true;
		}
	}

	/**
	 * Aggiorna l'anteprima del template.
	 */
	public void aggiornaAnteprima() {
		FileDTO nuovaAnteprima = null;
		try {
			normalizzaMetadato();
			nuovaAnteprima = generaFileDocumento();

			FacesHelper.putObjectInSession(SessionObject.CONTENT_HIGHLIGHT_SIGN_FIELD, nuovaAnteprima);

			if(nuovaAnteprima!=null) {
				visualizzaAnteprima = true;
			}else {
				visualizzaAnteprima = false;
			}
			modificheNonVisualizzate = false;
		} catch (final Exception e) {
			LOGGER.error(ERROR_GENERAZIONE_ANTEPRIMA_DOCUMENTO_MSG, e);
			showErrorMessage(ERROR_GENERAZIONE_ANTEPRIMA_DOCUMENTO_MSG);
		}
	}

	private void normalizzaMetadato() {
		if (metadatiView != null && !metadatiView.isEmpty()) {
			for (final TemplateMetadatoView metadato : metadatiView) {
				if (metadato.getValue().contains("<br>")) {
					metadato.setValue(metadato.getValue().replace("<br>", "<br/>"));
				}
			}
		}
	}

	/**
	 * Imposta i metatadi a partire del template del dettaglio in input.
	 * 
	 * @param inDetail
	 */
	public void impostaMetadatiValoreDaDettaglio(final DetailDocumentRedDTO inDetail) {
		detail = inDetail;

		final Map<TemplateValueDTO, List<TemplateMetadatoValueDTO>> detailTemplateDocUscitaMap = detail.getTemplateDocUscitaMap();

		// Si carica la maschera a partire dai dati (templateDocUscitaMap) del documento
		if (MapUtils.isNotEmpty(detailTemplateDocUscitaMap)) {
			metadatiView.clear();

			final TemplateValueDTO templateValue = detailTemplateDocUscitaMap.keySet().iterator().next();

			// Si seleziona il template corretto
			for (final TemplateDTO template : templateList) {
				if (template.getIdTemplate().equals(templateValue.getIdTemplate())) {
					templateSelected = template;
					break;
				}
			}
			if (templateSelected != null) {
				idTemplateSelected = templateSelected.getIdTemplate();
			}
			// Si imposta il nome del documento
			nomeDocumento = templateValue.getNomeDocumento();

			// Si impostano i metadati a partire dal template associato al documento
			// (detail)
			for (final TemplateMetadatoValueDTO metadatoValore : detailTemplateDocUscitaMap.get(templateValue)) {
				metadatiView.add(new TemplateMetadatoView(metadatoValore));
			}
			// Si aggiorna l'anteprima del documento
			aggiornaAnteprima();
		} else {
			onSelectTemplate();
		}
	}

	/**
	 * Viene triggerato quando si effettua la submit e uno dei valori in input è
	 * stato modificato.
	 */
	public void onChangeMetadato() {
		modificheNonVisualizzate = true;
	}

	/**
	 * Gestisce la registrazione del documento dal template.
	 * 
	 * @param forzaRegistrazione true per forzare la registrazione anche se ci sono
	 *                           modifiche ai metadati non visualizzate
	 *                           nell'anteprima
	 */
	public void registra(final boolean forzaRegistrazione) {
		boolean validazioneOk = true;

		if (modificheNonVisualizzate && !forzaRegistrazione) {
			// Si mostra la dialog di conferma dell'azione di registrazione dal template
			FacesHelper.executeJS("PF('dlgConfirmTemplateDocUscita').show()");
		} else {
			if (StringUtils.isBlank(nomeDocumento)) {
				validazioneOk = false;
				showErrorMessage("È necessario fornire un nome per il documento");
			} else if (!nomeDocumento.toLowerCase().endsWith(".pdf")) {
				if (nomeDocumento.length() <= 96) {
					nomeDocumento = nomeDocumento + ".pdf";
				} else {
					validazioneOk = false;
					showErrorMessage("Il nome del documento deve essere lungo massimo 100 caratteri (compresa l'estensione '.pdf')");
				}
			}
			if (validazioneOk) {
				FileDTO fileDocumento = null;
				try {
					fileDocumento = generaFileDocumento();

					if (fileDocumento == null) {
						LOGGER.error("File documento generato non correttamente.");
						throw new RedException("File documento generato non correttamente.");
					}

					final TemplateValueDTO templateDocumento = new TemplateValueDTO(templateSelected, detail.getDocumentTitle(), fileDocumento.getFileName());

					// Si aggiorna la maschera di creazione/modifica
					final DocumentManagerBean dmb = FacesHelper.getManagedBean(MBean.DOCUMENT_MANAGER_BEAN);
					dmb.aggiornaDocumentoDaTemplateDocUscita(templateDocumento, metadati, fileDocumento);
				} catch (final Exception e) {
					LOGGER.error(ERROR_GENERAZIONE_DOCUMENTO_MSG, e);
					showErrorMessage(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, ERROR_GENERAZIONE_DOCUMENTO_MSG);
				}
			}
		}
	}

	/**
	 * Calcola il nome del documento a partire dal template selezionato.
	 */
	private void impostaNomeDocumento() {
		nomeDocumento = templateSelected.getDescrizione() + "_" + LocalDate.now().format(DateTimeFormatter.ofPattern("ddMMyyyy")) + "_"
				+ LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss")) + ".pdf";
	}

	/**
	 * Genera il FileDTO del documento.
	 * 
	 * @return file
	 */
	private FileDTO generaFileDocumento() {
		FileDTO file = null;
		try {
			normalizzaMetadato();
			aggiornaMetadatiDaView();
			if (templateSelected != null) {
				file = templateDocUscitaSRV.generaDocumento(utente, templateSelected.getIdTemplate(), nomeDocumento, metadati);
			}
		}catch(RedException e) {
			LOGGER.warn(e);
			showErrorMessage(e.getMessage());
		}
		return file;
	}

	/**
	 * Aggiorna i metadati dalla view.
	 */
	private void aggiornaMetadatiDaView() {
		metadati.clear();

		for (final TemplateMetadatoView metadatoView : metadatiView) {
			metadati.add(metadatoView.toTemplateMetadatoValue());
		}
	}

	/**
	 * {@link #registra(boolean)}
	 */
	@Override
	public void conferma() {
		registra(true);
	}

	@Override
	public void annulla() {
		// Non deve fare nulla
	}

	/**
	 * Reset dei valori del tempate.
	 */
	public void reset() {
		detail = null;
		idTemplateSelected = null;
		templateSelected = null;
		nomeDocumento = Constants.EMPTY_STRING;
		metadati.clear();
		metadatiView.clear();
		visualizzaAnteprima = false;
		modificheNonVisualizzate = false;
	}

	/**
	 * Metadato template per la visualizzazione.
	 */
	public class TemplateMetadatoView extends TemplateMetadatoValueDTO {

		/**
		 * Costante serial version UID.
		 */
		private static final long serialVersionUID = 306015515509487723L;

		/**
		 * Testo selezionato.
		 */
		private TestoPredefinitoDTO testoSelected;

		/**
		 * Costruttore di default.
		 * 
		 * @param metadato
		 */
		public TemplateMetadatoView(final TemplateMetadatoDTO metadato) {
			popola(metadato);
			setValue(getMetadatoValue(metadato.getDefaultValue(), metadato.getIdMetadatoIntestazione()));
		}

		/**
		 * Popola il metadato e ne imposta il valore.
		 * 
		 * @param metadatoValue
		 */
		public TemplateMetadatoView(final TemplateMetadatoValueDTO metadatoValue) {
			popola(metadatoValue);
			setValue(metadatoValue.getValue());
		}

		/**
		 * Popola i parametri della classe con i parametri del metadato.
		 * @param metadato
		 */
		private void popola(final TemplateMetadatoDTO metadato) {
			setIdTemplate(metadato.getIdTemplate());
			setIdMetadato(metadato.getIdMetadato());
			setLabel(metadato.getLabel());
			setOrdine(metadato.getOrdine());
			setMaxLength(metadato.getMaxLength());
			setDefaultValue(metadato.getDefaultValue());
			setTestoPredefinito(metadato.getTestoPredefinito());
			setTipoMetadato(metadato.getTipoMetadato());
			setTemplateValue(metadato.getTemplateValue());
			setTemplateLabel(metadato.isTemplateLabel());
			setTemplateTesto(metadato.isTemplateTesto());
			setTestiPredefiniti(metadato.getTestiPredefiniti());
			setCaseSensitive(metadato.getCaseSensitive());
			setIdMetadatoIntestazione(metadato.getIdMetadatoIntestazione());
			setInfo(metadato.getInfo());
		}

		/**
		 * Ribalta le informazioni del metadato da view in un oggetto
		 * TemplateMetadatoValueDTO.
		 * 
		 * @return templateValue
		 */
		public TemplateMetadatoValueDTO toTemplateMetadatoValue() {
			final TemplateMetadatoValueDTO templateValue = new TemplateMetadatoValueDTO();

			templateValue.setDefaultValue(getDefaultValue());
			templateValue.setIdMetadato(getIdMetadato());
			templateValue.setIdTemplate(getIdTemplate());
			templateValue.setLabel(getLabel());
			templateValue.setMaxLength(getMaxLength());
			templateValue.setTemplateLabel(isTemplateLabel());
			templateValue.setTemplateTesto(isTemplateTesto());
			templateValue.setTemplateValue(getTemplateValue());
			templateValue.setTestiPredefiniti(getTestiPredefiniti());
			templateValue.setTipoMetadato(getTipoMetadato());
			templateValue.setValue(getValue());
			templateValue.setIdMetadatoIntestazione(getIdMetadatoIntestazione());
			templateValue.setInfo(getInfo());

			return templateValue;
		}

		private String getMetadatoValue(final String dbDefaultValue, final Integer idMetadatoIntestazione) {
			return templateDocUscitaSRV.getMetadatoValue(dbDefaultValue, detail.getDestinatari(), detail.getOggetto(), detail.getNumeroProtocollo(),
					detail.getAnnoProtocollo(), utente, idMetadatoIntestazione);
		}

		/**
		 * Restituisce il testo predefinito selezionato.
		 * 
		 * @return testoSelected
		 */
		public TestoPredefinitoDTO getTestoSelected() {
			return testoSelected;
		}

		/**
		 * Imposta il testo predefinito selezionato.
		 * 
		 * @param testoSelected
		 */
		public void setTestoSelected(final TestoPredefinitoDTO testoSelected) {
			this.testoSelected = testoSelected;
		}
	}

	/**
	 * Restituisce l'id del template selezionato.
	 * 
	 * @return idTemplateSelected
	 */
	public String getIdTemplateSelected() {
		return idTemplateSelected;
	}

	/**
	 * Imposta l'id del template selezionato.
	 * 
	 * @param idTemplateSelected
	 */
	public void setIdTemplateSelected(final String idTemplateSelected) {
		this.idTemplateSelected = idTemplateSelected;
	}

	/**
	 * Restituisce il template selezionato.
	 * 
	 * @return templateSelected
	 */
	public TemplateDTO getTemplateSelected() {
		return templateSelected;
	}

	/**
	 * Imposta il template selezionato.
	 * 
	 * @param templateSelected
	 */
	public void setTemplateSelected(final TemplateDTO templateSelected) {
		this.templateSelected = templateSelected;
	}

	/**
	 * Restituisce la lista di template.
	 * 
	 * @return templateList
	 */
	public List<TemplateDTO> getTemplateList() {
		return templateList;
	}

	/**
	 * @return nomeDocumento
	 */
	public String getNomeDocumento() {
		return nomeDocumento;
	}

	/**
	 * Imposta il nome del documento.
	 * 
	 * @param nomeDocumento
	 */
	public void setNomeDocumento(final String nomeDocumento) {
		this.nomeDocumento = nomeDocumento;
	}

	/**
	 * Restituisce la lista di metadati in view.
	 * 
	 * @return metadatiView
	 */
	public List<TemplateMetadatoView> getMetadatiView() {
		return metadatiView;
	}

	/**
	 * Restituisce il flag {@link #modificheNonVisualizzate}.
	 * 
	 * @return modificheNonVisualizzate
	 */
	public boolean isModificheNonVisualizzate() {
		return modificheNonVisualizzate;
	}

	/**
	 * Restituisce il flag {@link #visualizzaAnteprima}.
	 * 
	 * @return the visualizzaAnteprima
	 */
	public boolean isVisualizzaAnteprima() {
		return visualizzaAnteprima;
	}

	/**
	 * Recupera e imposta in view le versioni del documento in input.
	 * 
	 * @param idDocumento
	 */
	public void getVersioniDocumento(final Integer idDocumento) {
		templateVersion = new ArrayList<>();
		if (idDocumento != null) {
			templateVersion = templateDocUscitaSRV.getVersioniDocumento(idDocumento);
			if (templateVersion != null) {
				templateVersionSelected = templateVersion.get(0);
				templateVersionSelected.setSelected(true);
				caricaMetadatiVersioneCorrente(metadatiView, templateVersionSelected);
				aggiornaAnteprimaVersione(templateVersionSelected);
				FacesHelper.update("idDettagliEstesiForm:idVersioniDocumentoID");
			}
		}
	}

	/**
	 * Handler di selezione versione documento.
	 * 
	 * @param se
	 */
	public final void rowSelectorVersione(final SelectEvent se) {
		templateVersionSelected = (TemplateMetadatoVersion) se.getObject();
		templateVersionSelected.setSelected(true);
		for (final TemplateMetadatoVersion versione : templateVersion) {
			if (!versione.equals(templateVersionSelected)) {
				versione.setSelected(false);
			}
		}
		caricaMetadatiVersioneCorrente(metadatiView, templateVersionSelected);
		aggiornaAnteprimaVersione(templateVersionSelected);
		
	}

	/**
	 * Restituisce la versione del template.
	 * 
	 * @return templateVersion
	 */
	public List<TemplateMetadatoVersion> getTemplateVersion() {
		return templateVersion;
	}

	/**
	 * Imposta la versione del template.
	 * 
	 * @param templateVersion
	 */
	public void setTemplateVersion(final List<TemplateMetadatoVersion> templateVersion) {
		this.templateVersion = templateVersion;
	}

	/**
	 * Restituisce il flag per la visualizzazione dei metadati del template.
	 * 
	 * @return showMetadatiTemplate
	 */
	public boolean getShowMetadatiTemplate() {
		return showMetadatiTemplate;
	}

	/**
	 * Imposta il flag per la visualizzazione dei metadati del template.
	 * 
	 * @param showMetadatiTemplate
	 */
	public void setShowMetadatiTemplate(final boolean showMetadatiTemplate) {
		this.showMetadatiTemplate = showMetadatiTemplate;
	}

	/**
	 * Imposta la lista di metadati a partire dal dettaglio in input.
	 * 
	 * @param inDetail
	 * @param modificaUscita
	 */
	public void impostaMetadatiValoreDaDettaglioNew(final DetailDocumentRedDTO inDetail, final boolean modificaUscita) {
		detail = inDetail;

		final Map<TemplateValueDTO, List<TemplateMetadatoValueDTO>> detailTemplateDocUscitaMap = detail.getTemplateDocUscitaMap();

		// Si carica la maschera a partire dai dati (templateDocUscitaMap) del documento
		if (MapUtils.isNotEmpty(detailTemplateDocUscitaMap) && !modificaUscita) {
			metadatiView.clear();

			final TemplateValueDTO templateValue = detailTemplateDocUscitaMap.keySet().iterator().next();

			// Si seleziona il template corretto
			for (final TemplateDTO template : templateList) {
				if (template.getIdTemplate().equals(templateValue.getIdTemplate())) {
					templateSelected = template;
					break;
				}
			}
			if (templateSelected != null) {
				idTemplateSelected = templateSelected.getIdTemplate();
			}
			// Si imposta il nome del documento
			nomeDocumento = templateValue.getNomeDocumento();

			// Si impostano i metadati a partire dal template associato al documento
			// (detail)
			for (final TemplateMetadatoValueDTO metadatoValore : detailTemplateDocUscitaMap.get(templateValue)) {
				metadatiView.add(new TemplateMetadatoView(metadatoValore));
			}
			// Si aggiorna l'anteprima del documento
			aggiornaAnteprima();
		} else {
			idTemplateSelected = null;
			onSelectTemplate();
			aggiornaAnteprima();
			if(MapUtils.isNotEmpty(detailTemplateDocUscitaMap)) {
				TemplateValueDTO templateValue = detailTemplateDocUscitaMap.keySet().iterator().next();
				idTemplateSelected = templateValue.getIdTemplate();
			}
			templateVersionSelected = new TemplateMetadatoVersion();
		}
	}

	/**
	 * Carica i metadati della versione del template selezionata.
	 * 
	 * @param event
	 */
	public void caricaMetadatiVersioneCorrente(final ActionEvent event) {
		templateVersionSelected = new TemplateMetadatoVersion();
		templateVersionSelected = ((TemplateMetadatoVersion) getDataTableClickedRow(event));
		if (templateVersionSelected == null) {
			showErrorMessage("Errore nel recupero della versione del template");
			return;
		}
		caricaMetadatiVersioneCorrente(metadatiView, templateVersionSelected);
	}

	/**
	 * Carica la lista di metadati della versione corrente del template.
	 * 
	 * @param metadatiViewSelected
	 * @param templateVersion
	 */
	public void caricaMetadatiVersioneCorrente(final List<TemplateMetadatoView> metadatiViewSelected, final TemplateMetadatoVersion templateVersion) {
		nomeDocumento = "";
		if (StringUtils.isNotBlank(templateVersion.getIdTemplate())) {
			idTemplateSelected = templateVersion.getIdTemplate();
		}
		if (StringUtils.isNotBlank(templateVersion.getNomeTemplate())) {
			nomeDocumento = templateVersion.getNomeTemplate();
		}
		onSelectTemplateVersion(metadatiViewSelected, templateVersion.getIdDocumento(), templateVersion.getVersion());
	}

	/**
	 * Gestisce la selezione della versione del template.
	 * 
	 * @param metadatiViewSelected
	 * @param numDocumento
	 * @param versione
	 */
	public void onSelectTemplateVersion(final List<TemplateMetadatoView> metadatiViewSelected, final Integer numDocumento, final Integer versione) {
		metadatiViewSelected.clear();
		visualizzaAnteprima = false;

		if (StringUtils.isNotBlank(idTemplateSelected)) {
			showMetadatiTemplate = true;
			for (final TemplateDTO t : templateList) {
				if (t.getIdTemplate().equals(idTemplateSelected)) {
					templateSelected = t;
					break;
				}
			}
			final List<TemplateMetadatoValueDTO> valoreMetadato = templateDocUscitaSRV.getValueMetadato(idTemplateSelected, numDocumento, versione);
			for (final TemplateMetadatoValueDTO value : valoreMetadato) {
				metadatiViewSelected.add(new TemplateMetadatoView(value));
			}
			modificheNonVisualizzate = true;
			impostaNomeDocumento();
		} else {
			showMetadatiTemplate = false;
			templateSelected = null;
			nomeDocumento = Constants.EMPTY_STRING;
			modificheNonVisualizzate = false;
		}
	}

	/**
	 * Aggiorna la versione dell'anteprima.
	 * 
	 * @param versioneTemplate
	 */
	public void aggiornaAnteprimaVersione(final TemplateMetadatoVersion versioneTemplate) {
		templateVersionSelected = new TemplateMetadatoVersion();
		if (versioneTemplate != null) {
			templateVersionSelected = versioneTemplate;
			generaAnteprimaVersione();
		}
	}

	/**
	 * Aggiorna la versione dell'anteprima in base alla versione del template
	 * selezionati.
	 * 
	 * @param event
	 */
	public void aggiornaAnteprimaVersione(final ActionEvent event) {
		try {
			templateVersionSelected = new TemplateMetadatoVersion();
			templateVersionSelected = ((TemplateMetadatoVersion) getDataTableClickedRow(event));
			generaAnteprimaVersione();
		} catch (final Exception e) {
			LOGGER.error(ERROR_GENERAZIONE_ANTEPRIMA_DOCUMENTO_MSG, e);
			showErrorMessage(ERROR_GENERAZIONE_ANTEPRIMA_DOCUMENTO_MSG);
		}
	}

	private void generaAnteprimaVersione() {
		FileDTO nuovaAnteprima;
		try {
			if (templateVersionSelected == null) {
				showErrorMessage("Errore nel recupero della versione del template");
				return;
			}
			if (StringUtils.isNotBlank(templateVersionSelected.getIdTemplate())) {
				idTemplateSelected = templateVersionSelected.getIdTemplate();
				if (templateList != null && !templateList.isEmpty()) {
					for (final TemplateDTO t : templateList) {
						if (t.getIdTemplate().equals(idTemplateSelected)) {
							templateSelected = t;
							break;
						}
					}
				}
				nomeDocumento = templateVersionSelected.getNomeTemplate();
			}
			nuovaAnteprima = generaFileDocumentoNew(templateVersionSelected.getIdDocumento(), templateVersionSelected.getVersion());
			FacesHelper.putObjectInSession(SessionObject.CONTENT_HIGHLIGHT_SIGN_FIELD, nuovaAnteprima);

			visualizzaAnteprima = true;
			modificheNonVisualizzate = false;
		}catch(RedException e) {
			LOGGER.warn(e);
			showErrorMessage(e.getMessage());
		}
	}

	private FileDTO generaFileDocumentoNew(final int numDocumento, final int versione) {
		metadati.clear();
		final List<TemplateMetadatoValueDTO> valoreMetadato = templateDocUscitaSRV.getValueMetadato(idTemplateSelected, numDocumento, versione);
		for (final TemplateMetadatoValueDTO value : valoreMetadato) {
			metadati.add(value);
		}
		return templateDocUscitaSRV.generaDocumento(utente, templateSelected.getIdTemplate(), nomeDocumento, metadati);
	}

	/**
	 * Restituisce la lista di template comparabili.
	 * 
	 * @return templateComparabili
	 */
	public List<TemplateMetadatoVersion> getTemplateComparabili() {
		return templateComparabili;
	}

	/**
	 * Imposta la lista di template comparabili.
	 * 
	 * @param templateComparabili
	 */
	public void setTemplateComparabili(final List<TemplateMetadatoVersion> templateComparabili) {
		this.templateComparabili = templateComparabili;
	}

	/**
	 * Selettore di riga.
	 * 
	 * @param se
	 */
	public final void rowSelector(final SelectEvent se) {
		disabilitaComparazioneMetadati = true;
		if (templateConfrontato.getIdTemplate().equals(templateConfrontante.getIdTemplate())) {
			disabilitaComparazioneMetadati = false;
		}
	}

	/**
	 * Seleziona dell'id del template confrontante.
	 * 
	 * @param templateConf
	 */
	public final void rowSelector(final TemplateMetadatoVersion templateConf) {
		disabilitaComparazioneMetadati = true;
		if (templateConf.getIdTemplate().equals(templateConfrontante.getIdTemplate())) {
			disabilitaComparazioneMetadati = false;
		}
	}

	/**
	 * Handler di selezione del template confrontante.
	 * 
	 * @param event
	 */
	public void popolaListaDaConfrontare(final ActionEvent event) {
		templateComparabili = new ArrayList<>();
		templateConfrontante = getDataTableClickedRow(event);

		if (templateConfrontante == null) {
			showErrorMessage("Errore durante la comparazione");
			return;
		}
		if (templateVersion != null) {
			templateComparabili.addAll(templateVersion);
			// Rimuovo me stesso
			templateComparabili.remove(templateConfrontante);
		}
		if (!templateComparabili.isEmpty()) {
			templateConfrontato = templateComparabili.get(0);
			rowSelector(templateConfrontato);
		}
	}

	/**
	 * Restituisce la versione del template confrontata.
	 * 
	 * @return templateConfrontato
	 */
	public TemplateMetadatoVersion getTemplateConfrontato() {
		return templateConfrontato;
	}

	/**
	 * Imposta la versione del template confrontata.
	 * 
	 * @param templateConfrontato
	 */
	public void setTemplateConfrontato(final TemplateMetadatoVersion templateConfrontato) {
		this.templateConfrontato = templateConfrontato;
	}

	/**
	 * Restituisce il template confrontante.
	 * 
	 * @return templateConfrontante
	 */
	public TemplateMetadatoVersion getTemplateConfrontante() {
		return templateConfrontante;
	}

	/**
	 * Imposta il template confrontante.
	 * 
	 * @param templateConfrontante
	 */
	public void setTemplateConfrontante(final TemplateMetadatoVersion templateConfrontante) {
		this.templateConfrontante = templateConfrontante;
	}

	/**
	 * Confronta i metadati di due anteprime.
	 */
	public void confrontaMetadati() {
		modalitaConfronto = true;
		metadatiViewConfrontante = new ArrayList<>();
		caricaMetadatiVersioneCorrente(metadatiViewConfrontante, templateConfrontante);
		metadatiViewConfrontato = new ArrayList<>();
		caricaMetadatiVersioneCorrente(metadatiViewConfrontato, templateConfrontato);
		if (StringUtils.isNotBlank(templateConfrontante.getNomeTemplate())) {
			nomeDocumento = templateConfrontante.getNomeTemplate();
		}
		if (StringUtils.isNotBlank(templateConfrontato.getNomeTemplate())) {
			nomeDocumentoConfrontato = templateConfrontato.getNomeTemplate();
		}
		// Siccome il confronto deve essere fatto necessariamente tra due template
		// uguali
		// anche il num e l'ordine dei metadati è uguale.
		// Quindi uso nel ciclo l'indice della lista confrontante
		for (int i = 0; i < metadatiViewConfrontante.size(); i++) {
			if (metadatiViewConfrontante.get(i).getValue() == null && metadatiViewConfrontato.get(i).getValue() != null) {
				metadatiViewConfrontante.get(i).setSonoDiversi(true);
				metadatiViewConfrontato.get(i).setSonoDiversi(true);
			} else if (metadatiViewConfrontante.get(i).getValue() != null && metadatiViewConfrontato.get(i).getValue() == null) {
				metadatiViewConfrontante.get(i).setSonoDiversi(true);
				metadatiViewConfrontato.get(i).setSonoDiversi(true);
			} else if ((metadatiViewConfrontante.get(i).getValue() != null && metadatiViewConfrontato.get(i).getValue() != null)
					&& !metadatiViewConfrontante.get(i).getValue().equals(metadatiViewConfrontato.get(i).getValue())) {
				metadatiViewConfrontante.get(i).setSonoDiversi(true);
				metadatiViewConfrontato.get(i).setSonoDiversi(true);
			}
		}
	}

	/**
	 * Restituisce la lista di metadatiView dell'anteprima confrontata.
	 * 
	 * @return metadatiViewConfrontato
	 */
	public List<TemplateMetadatoView> getMetadatiViewConfrontato() {
		return metadatiViewConfrontato;
	}

	/**
	 * Imposta la lista di metadatiView dell'anteprima confrontata.
	 * 
	 * @param metadatiViewConfrontato
	 */
	public void setMetadatiViewConfrontato(final List<TemplateMetadatoView> metadatiViewConfrontato) {
		this.metadatiViewConfrontato = metadatiViewConfrontato;
	}

	/**
	 * Restitusice il flag {@link #modalitaConfronto}.
	 * 
	 * @return modalitaConfronto
	 */
	public boolean getModalitaConfronto() {
		return modalitaConfronto;
	}

	/**
	 * Imposta il flag {@link #modalitaConfronto}.
	 * 
	 * @param modalitaConfronto
	 */
	public void setModalitaConfronto(final boolean modalitaConfronto) {
		this.modalitaConfronto = modalitaConfronto;
	}

	/**
	 * Restituisce il flag {@link #disabilitaComparazioneMetadati}.
	 * 
	 * @return disabilitaComparazioneMetadati
	 */
	public boolean getDisabilitaComparazioneMetadati() {
		return disabilitaComparazioneMetadati;
	}

	/**
	 * Imposta il flag {@link #disabilitaComparazioneMetadati}.
	 * 
	 * @param disabilitaComparazioneMetadati
	 */
	public void setDisabilitaComparazioneMetadati(final boolean disabilitaComparazioneMetadati) {
		this.disabilitaComparazioneMetadati = disabilitaComparazioneMetadati;
	}

	/**
	 * Restituisce i metadati delle view confrontate.
	 * 
	 * @return metadatiViewConfrontante
	 */
	public List<TemplateMetadatoView> getMetadatiViewConfrontante() {
		return metadatiViewConfrontante;
	}

	/**
	 * Imposta i metadati delle view confrontate.
	 * 
	 * @param metadatiViewConfrontante
	 */
	public void setMetadatiViewConfrontante(final List<TemplateMetadatoView> metadatiViewConfrontante) {
		this.metadatiViewConfrontante = metadatiViewConfrontante;
	}

	/**
	 * @see {@link #aggiornaAnteprimaVersione(TemplateMetadatoVersion)}.
	 */
	public void resetConfronto() {
		aggiornaAnteprimaVersione(templateVersionSelected);
	}

	/**
	 * Confronta l'anteprima di due template.
	 */
	public void confrontoAnteprima() {
		FileDTO nuovaAnteprimaConfrontante = null;
		FileDTO nuovaAnteprimaConfrontato = null;
		try {
			metadatiViewConfrontante = new ArrayList<>();
			caricaMetadatiVersioneCorrente(metadatiViewConfrontante, templateConfrontante);
			nuovaAnteprimaConfrontante = generaFileDocumentoConfronto(metadatiViewConfrontante);
			FacesHelper.putObjectInSession(SessionObject.CONTENT_HIGHLIGHT_SIGN_FIELD_SX, nuovaAnteprimaConfrontante);

			metadatiViewConfrontato = new ArrayList<>();
			caricaMetadatiVersioneCorrente(metadatiViewConfrontato, templateConfrontato);
			nuovaAnteprimaConfrontato = generaFileDocumentoConfronto(metadatiViewConfrontato);
			FacesHelper.putObjectInSession(SessionObject.CONTENT_HIGHLIGHT_SIGN_FIELD_DX, nuovaAnteprimaConfrontato);

			visualizzaAnteprima = true;
			modificheNonVisualizzate = false;
		}catch(RedException e) {
			LOGGER.warn(e);
			showErrorMessage(e.getMessage());
		} catch (final Exception e) {
			LOGGER.error(ERROR_GENERAZIONE_ANTEPRIMA_DOCUMENTO_MSG, e);
			showErrorMessage(ERROR_GENERAZIONE_ANTEPRIMA_DOCUMENTO_MSG);
		}
	}

	private FileDTO generaFileDocumentoConfronto(final List<TemplateMetadatoView> metadatiViewDoc) {
		final List<TemplateMetadatoValueDTO> metadatiConfrontoAnteprima = aggiornaMetadatiDaViewConfronto(metadatiViewDoc);

		return templateDocUscitaSRV.generaDocumento(utente, templateSelected.getIdTemplate(), nomeDocumento, metadatiConfrontoAnteprima);
	}

	private List<TemplateMetadatoValueDTO> aggiornaMetadatiDaViewConfronto(final List<TemplateMetadatoView> metadatiViewDoc) {
		final List<TemplateMetadatoValueDTO> metadatiConfrontoAnteprima = new ArrayList<>();
		metadatiConfrontoAnteprima.clear();

		if (metadatiViewDoc != null && !metadatiViewDoc.isEmpty()) {
			for (final TemplateMetadatoView metadatoView : metadatiViewDoc) {
				metadatiConfrontoAnteprima.add(metadatoView.toTemplateMetadatoValue());
			}
		}
		return metadatiConfrontoAnteprima;
	}

	/**
	 * Restituisce il nome del documento confrontato.
	 * 
	 * @return nomeDocumentoConfrontato
	 */
	public String getNomeDocumentoConfrontato() {
		return nomeDocumentoConfrontato;
	}

	/**
	 * Imposta il nome del documento confrontato.
	 * 
	 * @param nomeDocumentoConfrontato
	 */
	public void setNomeDocumentoConfrontato(final String nomeDocumentoConfrontato) {
		this.nomeDocumentoConfrontato = nomeDocumentoConfrontato;
	}

	/**
	 * Restituisce la versione selezionata del template.
	 * 
	 * @return templateVersionSelected
	 */
	public TemplateMetadatoVersion getTemplateVersionSelected() {
		return templateVersionSelected;
	}

	/**
	 * Imposta la versione selezionata del template.
	 * 
	 * @param templateVersionSelected
	 */
	public void setTemplateVersionSelected(final TemplateMetadatoVersion templateVersionSelected) {
		this.templateVersionSelected = templateVersionSelected;
	}
}
