package it.ibm.red.web.mbean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;

import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Bean lista fascicoli.
 */
@Named(ConstantsWeb.MBean.LISTA_FASCICOLI_BEAN)
@ViewScoped
public class ListaFascicoliBean extends AbstractBean {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ListaFascicoliBean.class.getName());
	
	/**
	 * Tabella fascicoli.
	 */
	private SimpleDetailDataTableHelper<FascicoloDTO> fascicoliDTH;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		fascicoliDTH = new SimpleDetailDataTableHelper<>();
		
		List<FascicoloDTO> noFilterMasters = (List<FascicoloDTO>) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.LISTA_FASCICOLI, true);
		fascicoliDTH.setMasters(noFilterMasters);
		
		fascicoliDTH.selectFirst(true);
		if (fascicoliDTH.getCurrentMaster() != null) {
			this.selectDetail(fascicoliDTH.getCurrentMaster());
		}
		
		LOGGER.info("Lista Fascicoli caricata con successo");
	}
	
	
	/**
	 * Esegue il refresh.
	 */
	public void refresh() {
		//Questo metodo è lasciato intenzionalmente vuoto.
	}
	
	/**
	 * Gestisce la selezione della riga dal datatable dei fascicoli.
	 * @param se
	 */
	public final void rowSelector(final SelectEvent se) {
		fascicoliDTH.rowSelector(se);
		selectDetail(fascicoliDTH.getCurrentMaster());
	}
	
	
	/**
	 * Setto il fascicolo da recuperare e visualizzare nel dettaglio
	 * @param f
	 */
	public void selectDetail(final FascicoloDTO f) {
		final DettaglioFascicoloBean bean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_BEAN);
		bean.setDetail(f);
	}
	
	
	/**
	 * @return the fascicoliDTH
	 */
	public SimpleDetailDataTableHelper<FascicoloDTO> getFascicoliDTH() {
		return fascicoliDTH;
	}
	
	
	/**
	 * @param fascicoliDTH the fascicoliDTH to set
	 */
	public void setFascicoliDTH(final SimpleDetailDataTableHelper<FascicoloDTO> fascicoliDTH) {
		this.fascicoliDTH = fascicoliDTH;
	}
	
}
