package it.ibm.red.web.mbean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.MasterFascicoloDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IFascicoloFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.enums.TreeNodeTypeEnum;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * The Class AbstractBean.
 *
 * @author CPIERASC
 * 
 *         Managed bean base
 */
public abstract class AbstractTreeBean extends AbstractBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -6401007248315824267L;

	/**
	 * Nodo root.
	 */
	private transient TreeNode root;

	/**
	 * Nodo selezionato.
	 */
	private transient TreeNode selectedNode;	

	/**
	 * Servizio fascicolo.
	 */
	private IFascicoloFacadeSRV fascicoloSRV;

	/**
	 * Utente.
	 */
	protected UtenteDTO utente;

	/**
	 * Restituisce il DTO dell'utente.
	 * 
	 * @return UtenteDTO
	 * */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Init.
	 * 
	 * @param dataRoot
	 */
	protected <T> void init(final T dataRoot) {
		fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloFacadeSRV.class);
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		root = addNodoRoot(dataRoot);
	}

	/**
	 * Aggiunge un nodo root.
	 * 
	 * @param data
	 * @return T
	 */
	protected <T> DefaultTreeNode addNodoRoot(final T data) {
		return createNode(null, TreeNodeTypeEnum.ROOT, data, true);
	}

	/**
	 * Crea un nuovo nodo aggiungendolo al tree.
	 * 
	 * @param parent
	 *            Nodo parent del tree.
	 * @param type
	 *            Tipologia del nodo.
	 * @param data
	 *            Informazioni nodo.
	 * @param expanded
	 *            Flag associato all'espandibilità del nodo.
	 * @return Nodo creato.
	 */
	protected <T> DefaultTreeNode createNode(final TreeNode parent, final TreeNodeTypeEnum type, final T data, final Boolean expanded) {
		final DefaultTreeNode output = new DefaultTreeNode(type.getType(), data, parent);
		output.setExpanded(expanded);
		if (Boolean.FALSE.equals(expanded) && !TreeNodeTypeEnum.DUMMY.equals(type)) {
			createNode(output, TreeNodeTypeEnum.DUMMY, null, false);
		}
		return output;
	}

	/**
	 * Crea una foglia nodo per il tree identificato dalla root: {@code parent}.
	 * 
	 * @param parent
	 *            nodo parent.
	 * @param type
	 *            Tipologia nodo, @see TreeNodeTypeEnum.
	 * @param data
	 *            Informazioni nodo.
	 * @return Nodo foglia creato.
	 */
	protected <T> DefaultTreeNode createNodeLeaf(final TreeNode parent, final TreeNodeTypeEnum type, final T data) {
		return new DefaultTreeNode(type.getType(), data, parent);
	}

	/**
	 * Restituisce i documenti associati ad uno specifico Facscicolo.
	 * 
	 * @param fascicolo
	 *            Il fascicolo per cui occorre recuperare i documenti
	 * @return Collection di documenti
	 */
	public Collection<DocumentoFascicoloDTO> getDocumentiFascicolo(final MasterFascicoloDTO fascicolo) {
		return fascicoloSRV.getOnlyDocumentiRedFascicolo(fascicolo.getId(), utente); // N.B. Solo documenti RED (si escludono il "documento fascicolo" e i contributi)
	}

	/**
	 * Gestisce la logica di visualizzazione del documento.
	 * 
	 * @param doc
	 *            il documento da visualizzare
	 */
	public void showDocument(final DocumentoFascicoloDTO doc) {
		final DettaglioDocumentoBean bean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN);
		final MasterDocumentRedDTO master = new MasterDocumentRedDTO();

		master.setDocumentTitle(doc.getDocumentTitle());
		if (doc.getNumeroDocumento() != null) {
			master.setNumeroDocumento(doc.getNumeroDocumento());
		}

		bean.setDetail(master);
	}

	/**
	 * Gestisce la logica inversa di showDocument, permette di nascondere il
	 * documento.
	 */
	public void hideDocument() {
		final DettaglioDocumentoBean bean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN);
		bean.unsetDetail();
	}

	/**
	 * Restituisce il Service per la gestione dei fascicoli.
	 * 
	 * @return IFascicoloFacadeSRV
	 */
	public IFascicoloFacadeSRV getFascicoloSRV() {
		return fascicoloSRV;
	}

	/**
	 * Restituisce la radice dell'albero.
	 * 
	 * @return la radice dell'albero come TreeNode
	 */
	public TreeNode getRoot() {
		return root;
	}

	/**
	 * Restituisce il nodo selezionato.
	 * 
	 * @return TreeNode
	 */
	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	/**
	 * Imposta il nodo selezionato.
	 * 
	 * @param selectedNode
	 */
	public void setSelectedNode(final TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}
	
	/**
	 * @param alberatura
	 * @param nodiDaAprire
	 */
	protected void nodiDaAprire(final List<Long> alberatura, List<DefaultTreeNode> nodiDaAprire) {
		final List<DefaultTreeNode> nodiDaAprireTemp = new ArrayList<>();
		for (int i = 0; i < alberatura.size(); i++) {
			// devo fare esattamente un numero di cicli uguale alla lunghezza dell'alberatura
			nodiDaAprireTemp.clear();
			for (final DefaultTreeNode nodo : nodiDaAprire) {
				onOpenTree(nodo);
				nodo.setExpanded(true);
				for (final TreeNode nodoFiglio : nodo.getChildren()) {
					final NodoOrganigrammaDTO nodoFiglioDto = (NodoOrganigrammaDTO) nodoFiglio.getData();
					if (nodoFiglioDto.getIdUtente() == null && alberatura.contains(nodoFiglioDto.getIdNodo())) {
						nodiDaAprireTemp.add((DefaultTreeNode) nodoFiglio);
					}
				}
			}
			nodiDaAprire = new ArrayList<>(nodiDaAprireTemp);
		}
	}

	/**
	 * Gestisce l'aperura dell'albero.
	 * @param nodo
	 */
	public abstract void onOpenTree(final TreeNode nodo);

	/**
	 * Gestisce l'aperura dell'albero.
	 * @param treeNode
	 * @param listaFigli
	 */
	protected void onOpenTree(final TreeNode treeNode, List<NodoOrganigrammaDTO> listaFigli) {
		List<NodoOrganigrammaDTO> figli = listaFigli;
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		
		DefaultTreeNode nodoToAdd = null;
		treeNode.getChildren().clear();
		for (final NodoOrganigrammaDTO n : figli) {
			n.setCodiceAOO(utente.getCodiceAoo());

			nodoToAdd = new DefaultTreeNode(n, treeNode);
			if (n.getFigli() > 0 || n.isUtentiVisible()) {
				new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
			}
		}
	}
}
