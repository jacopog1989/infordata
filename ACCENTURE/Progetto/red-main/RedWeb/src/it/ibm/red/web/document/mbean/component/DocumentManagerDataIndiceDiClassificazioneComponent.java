package it.ibm.red.web.document.mbean.component;

import java.util.List;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentManagerDTO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;

/**
 * Questa classe prende in input il documentManager perché sono state attuate delle semplificazioni lato back end nel recupero della radice.
 * 
 */
public class DocumentManagerDataIndiceDiClassificazioneComponent extends IndiceDiClassificazioneComponent {

	private static final long serialVersionUID = -881064703195016391L;
	
	/**
	 * Document manager.
	 */
	private final DocumentManagerDTO documentManager;

	/**
	 * Costruttore del component.
	 * @param inDetail
	 * @param inUtente
	 * @param inDocumentManager
	 */
	public DocumentManagerDataIndiceDiClassificazioneComponent(final DetailDocumentRedDTO inDetail, final UtenteDTO inUtente, final DocumentManagerDTO inDocumentManager) {
		super(inDetail, inUtente, false);
		documentManager = inDocumentManager;
		creaTitolario();
	}
	
	/**
	 * @see it.ibm.red.web.document.mbean.component.AbstractIndiceDiClassificazioneComponent#retrieveRootChildren().
	 */
	@Override
	protected List<TitolarioDTO> retrieveRootChildren() {
		if (documentManager.getTitolarioRootList() == null || documentManager.getTitolarioRootList().isEmpty()) {
			throw new RedException("Nessun titolario disponibile.");
		}
		
		return documentManager.getTitolarioRootList();
	}
}
