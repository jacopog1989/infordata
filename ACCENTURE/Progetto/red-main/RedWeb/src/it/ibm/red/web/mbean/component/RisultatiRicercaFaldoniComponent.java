package it.ibm.red.web.mbean.component;

import java.util.Collection;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;

import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.DettaglioFaldoneRicercaBean;

/**
 * Component che si occupa della gestione e delle presentazione dei risultati a valle di una ricerca di faldoni.
 */
@Named(MBean.RISULTATI_RICERCA_FALDONI)
@ViewScoped
public class RisultatiRicercaFaldoniComponent {

    /**
     * Faldoni.
     */
	private SimpleDetailDataTableHelper<FaldoneDTO> faldoniDTH;

	/**
	 * Costruttore.
	 * @param resultsFaldoni
	 */
	public RisultatiRicercaFaldoniComponent(final Collection<FaldoneDTO> resultsFaldoni) {
		faldoniDTH.setMasters(resultsFaldoni);
		faldoniDTH.selectFirst(true);
		selectFaldoneDetail(faldoniDTH.getCurrentMaster());
	}
	
	private void selectFaldoneDetail(final FaldoneDTO dettaglioFaldone) {
		final DettaglioFaldoneRicercaBean bean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_FALDONE_RICERCA_BEAN);
		
		if (dettaglioFaldone == null) {
			bean.unsetDetail();
			return;
		}
		
		bean.setDetail(dettaglioFaldone);
	}
	
	/**
	 * Gestisce la selezione delle riga del datatable dei faldoni.
	 * @param se
	 */
	public final void faldRowSelector(final SelectEvent se) {
		faldoniDTH.rowSelector(se);
		selectFaldoneDetail(faldoniDTH.getCurrentMaster());
	}

	/**
	 * Restituisce l'helper per la gestione del datatable dei faldoni.
	 * @return helper datatable dei faldoni
	 */
	public SimpleDetailDataTableHelper<FaldoneDTO> getFaldoniDTH() {
		return faldoniDTH;
	}
	
}
