package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ColoreNotaEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IModificaProcedimentoFacadeSRV;
import it.ibm.red.business.service.facade.INotaFacadeSRV;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.utils.OrganigrammaRetrieverUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractTreeBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;
import it.ibm.red.web.mbean.interfaces.OrganigrammaRetriever;

/**
 * Bean che gestisce la response di messa in conoscenza.
 */
@Named(ConstantsWeb.MBean.METTI_IN_CONOSCESCENZA_BEAN)
@ViewScoped
public class MettiInConoscenzaBean extends AbstractTreeBean implements OrganigrammaRetriever, InitHumanTaskInterface {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -8657979233875432260L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(MettiInConoscenzaBean.class.getName());

	/**
	 * Servizio.
	 */
	private IModificaProcedimentoFacadeSRV modificaProcedimentoSRV;

	/**
	 * Servizio.
	 */
	private IOrganigrammaFacadeSRV organigrammaSRV;

	/**
	 * Servizio.
	 */
	private INotaFacadeSRV notaSRV;

	/**
	 * Lista docuenti bean.
	 */
	private ListaDocumentiBean ldb;

	/**
	 * Infomrazioni utente.
	 */
	private UtenteDTO utente;

	/**
	 * Lista master.
	 */
	private List<MasterDocumentRedDTO> masters;

	/**
	 * Input proveniente dalla maschera.
	 */
	private String motivoAssegnazioneNew;

	/**
	 * Bean multi assegnazione.
	 */
	private MultiAssegnazioneBean assegnazioneBean;

	/**
	 * Flag storico.
	 */
	private boolean alsoStorico;

	/** ALBERATURA DA ESPLODERE START***********************************************************************************/
	private List<Nodo> alberaturaNodi;
	/** ALBERATURA DA ESPLODERE END*************************************************************************************/

	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		masters = inDocsSelezionati;
	}

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
		utente = sb.getUtente();
		organigrammaSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
		alberaturaNodi = organigrammaSRV.getAlberaturaBottomUp(utente.getIdAoo(), utente.getIdUfficio());

		modificaProcedimentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IModificaProcedimentoFacadeSRV.class);
		assegnazioneBean = new MultiAssegnazioneBean();
		assegnazioneBean.initialize(this, TipoAssegnazioneEnum.CONOSCENZA);

		notaSRV = ApplicationContextProvider.getApplicationContext().getBean(INotaFacadeSRV.class);
	}


	/***********************TREEENODE**************************/
	/**
	 * lo stesso organigramma viene utilizzato sia per gli assegnatari per conoscenza, per contributo che per i destinatari interni
	 */
	@Override
	public DefaultTreeNode loadRootOrganigramma() {
		DefaultTreeNode root = null;
		try {
			final List<Long> alberatura = new ArrayList<>();
			for (int i = alberaturaNodi.size()-1; i >= 0; i--) {
				alberatura.add(alberaturaNodi.get(i).getIdNodo());
			}
			//prendo da DB i nodi di primo livello
			final NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
			nodoRadice.setIdAOO(utente.getIdAoo());
			nodoRadice.setIdNodo(utente.getIdNodoRadiceAOO());
			nodoRadice.setCodiceAOO(utente.getCodiceAoo());
			nodoRadice.setUtentiVisible(false);
			final List<NodoOrganigrammaDTO> primoLivello = organigrammaSRV.getFigliAlberoAssegnatarioPerConoscenza(utente.getIdUfficio(), nodoRadice, true);

			root = new DefaultTreeNode();
			root.setExpanded(true);
			root.setSelectable(false);

			//per ogni nodo di primo livello creo il nodo da mettere nell'albero che ha come padre rootAssegnazionePerCompetenza
			List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
			root.setChildren(new ArrayList<>());
			DefaultTreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(utente.getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, root);
				nodoToAdd.setExpanded(true);
				if (alberatura.contains(n.getIdNodo())) {
					nodiDaAprire.add(nodoToAdd);
				}
			}

			nodiDaAprire(alberatura, nodiDaAprire);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}

		return root;
	}

	/**
	 * Gestisce l'evento di "Expand" associato ad un determinato nodo.
	 * @param treeNode
	 */
	@Override
	public void onOpenTree(final TreeNode treeNode) {
		try {
			final List<NodoOrganigrammaDTO> figli = organigrammaSRV.getFigliAlberoAssegnatarioPerConoscenza(utente.getIdUfficio(), (NodoOrganigrammaDTO) treeNode.getData(), false);

			super.onOpenTree(treeNode, figli);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}
	}
	/***********************TREEENODE**END*********************/
	/**
	 * Verifica la correttezza dell'elemento selezionato.
	 */
	public void checkEResponse() {
		if (CollectionUtils.isEmpty(assegnazioneBean.getSelectedElementList())) {
			showWarnMessage("Attenzione: è obbligatorio aggiungere almeno un assegnatario per conoscenza");
		} else {
			mettiInConoscenzaResponse();
		}
	}

	private void mettiInConoscenzaResponse() {
		try {
			//pulisco gli esiti
			ldb.cleanEsiti();

			if (CollectionUtils.isEmpty(masters)) {
				throw new RedException("Nessun documento selezionato per l'esecuzione dell'operazione");
			}

			final List<AssegnatarioOrganigrammaDTO> assegnatariPerConoscenza = 
					assegnazioneBean.getSelectedElementList().stream().map(c -> OrganigrammaRetrieverUtils.create(c)).collect(Collectors.toList());

			for (final MasterDocumentRedDTO master : masters) {
				final String wobNumber = master.getWobNumber();
				final EsitoOperazioneDTO esito = modificaProcedimentoSRV.mettiInConoscenza(wobNumber, assegnatariPerConoscenza, motivoAssegnazioneNew, utente);
				ldb.getEsitiOperazione().add(esito);

				if (Boolean.TRUE.equals(esito.getFlagEsito() && isAlsoStorico())) {
					notaSRV.registraNotaFromDocumento(utente, Integer.parseInt(master.getDocumentTitle()), ColoreNotaEnum.NERO, motivoAssegnazioneNew);
				}
			}

			FacesHelper.executeJS("PF('dlgGeneric').hide()");
			FacesHelper.executeJS("PF('dlgShowResult').show()");
			FacesHelper.update("centralSectionForm:idDlgShowResult");

			ldb.destroyBeanViewScope(ConstantsWeb.MBean.METTI_IN_CONOSCESCENZA_BEAN);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'esecuzione della response 'Metti in Conoscenza'.", e);
			showError(e);
		}
	}

	/**
	 * Restituisce l'utente.
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'utente.
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Restituisce il motivo dell'assegnazione.
	 * @return motivo assegnazione
	 */
	public String getMotivoAssegnazioneNew() {
		return motivoAssegnazioneNew;
	}

	/**
	 * Imposta il motivo assegnazione.
	 * @param motivoAssegnazioneNew
	 */
	public void setMotivoAssegnazioneNew(final String motivoAssegnazioneNew) {
		this.motivoAssegnazioneNew = motivoAssegnazioneNew;
	}

	/**
	 * Restituisce il bean per la gestione dell'assegnazione multipla.
	 * @return bean per assegnazione
	 */
	public MultiAssegnazioneBean getAssegnazioneBean() {
		return assegnazioneBean;
	}

	/**
	 * Imposta il bean che gestisce l'assegnazione.
	 * @param assegnazioneBean
	 */
	public void setAssegnazioneBean(final MultiAssegnazioneBean assegnazioneBean) {
		this.assegnazioneBean = assegnazioneBean;
	}

	/**
	 * Restituisce il SRV per la gestione della modifica del procedimento.
	 * @return service per modifica del procedimento
	 */
	public IModificaProcedimentoFacadeSRV getModificaProcedimentoSRV() {
		return modificaProcedimentoSRV;
	}

	/**
	 * @return alsoStorico
	 */
	public boolean isAlsoStorico() {
		return alsoStorico;
	}

	/**
	 * @param alsoStorico
	 */
	public void setAlsoStorico(final boolean alsoStorico) {
		this.alsoStorico = alsoStorico;
	}
}
