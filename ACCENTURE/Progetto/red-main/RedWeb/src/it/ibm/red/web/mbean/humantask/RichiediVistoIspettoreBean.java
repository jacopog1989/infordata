package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IRichiesteVistoFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * Bean che gestisce la response di richiesta di visto da un ispettore.
 */
@Named(ConstantsWeb.MBean.RICHIEDI_VISTO_ISPETTORE_BEAN)
@ViewScoped
public class RichiediVistoIspettoreBean extends AbstractBean implements InitHumanTaskInterface {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -7800158375761480647L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RichiediVistoIspettoreBean.class.getName());
	
	/**
	 * Documenti.
	 */
	private List<MasterDocumentRedDTO> masters;

	/**
	 * Motivo assegnazoine.
	 */
	private String motivoAssegnazione;

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		masters = inDocsSelezionati;
	}

	/**
	 * Post construct del bean.
	 */
	@Override
	protected void postConstruct() {
		// Non occorre fare niente nel post construct del bean.
	}
	
	/**
	 * Esegue la logica della response selezionata.
	 */
	public void doResponse() {
		final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		final ListaDocumentiBean ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
		final UtenteDTO utente = sb.getUtente();
		final IRichiesteVistoFacadeSRV vistiSRV = ApplicationContextProvider.getApplicationContext().getBean(IRichiesteVistoFacadeSRV.class);
		
		final List<String> wobNumbers = new ArrayList<>();
		for (final MasterDocumentRedDTO master : masters) {
			final String wobNumber = master.getWobNumber();
			if (!StringUtils.isEmpty(wobNumber)) {
				wobNumbers.add(wobNumber);
			}
		}
		if (CollectionUtils.isEmpty(wobNumbers)) {
			LOGGER.error("Non è stato selezionato nessun documento per l'esecuzione della response" + ldb.getSelectedResponse().getResponse());
			showError("Nessun documento selezionato per l'esecuzione della response");
		} else {			
			for (final String wobNumber: wobNumbers) {
				final EsitoOperazioneDTO eistiList = vistiSRV.richiediVistoIspettore(utente, wobNumber, motivoAssegnazione);
				ldb.getEsitiOperazione().add(eistiList);
			}
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.RICHIEDI_VISTO_ISPETTORE_BEAN);
		}
		
	}

	/**
	 * Restituisce il motivo dell'assegnazione.
	 * @return motivo assegnazione
	 */
	public String getMotivoAssegnazione() {
		return motivoAssegnazione;
	}

	/**
	 * Imposta il motivo dell'assegnazione.
	 * @param motivoAssegnazione
	 */
	public void setMotivoAssegnazione(final String motivoAssegnazione) {
		this.motivoAssegnazione = motivoAssegnazione;
	}

}
