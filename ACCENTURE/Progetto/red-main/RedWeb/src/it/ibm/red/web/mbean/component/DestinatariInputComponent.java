package it.ibm.red.web.mbean.component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.tabview.TabView;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.ComuneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.ProvinciaDTO;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.RubricaChiamanteEnum;
import it.ibm.red.business.enums.TipoRubricaEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.enums.TipologiaIndirizzoEmailEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV;
import it.ibm.red.business.service.facade.IInoltraMailDaCodaAttivaFacadeSRV;
import it.ibm.red.business.service.facade.IRubricaFacadeSRV;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.mbean.component.AbstractComponent;
import it.ibm.red.web.document.mbean.component.RegioniProvinceComuniComponent;
import it.ibm.red.web.document.mbean.component.RubricaComponent;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.IDestinatariInputComponent;
import it.ibm.red.web.mbean.master.IRubricaHandler;

/**
 * Implemntazione dell'interfaccia per il documentManager.
 * 
 * @author a.difolca
 *
 */
public class DestinatariInputComponent extends AbstractComponent implements IDestinatariInputComponent, IRubricaHandler {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 2151063460586956420L;

	/**
	 * Messaggio di errore associato alla nota mancante.
	 */
	private static final String ERROR_NOTA_MANCANTE_MSG = "E' obbligatorio inserire una nota";

	/**
	 * Il logger per gestire i messaggi in console.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DestinatariInputComponent.class);

	/**
	 * Servizio inoltro.
	 */
	private final IInoltraMailDaCodaAttivaFacadeSRV inoltraSRV;

	/**
	 * Destinatari.
	 */
	private List<DestinatarioRedDTO> destinatariInModificaPuliti;

	/**
	 * Modalità destinatario.
	 */
	private final List<ModalitaDestinatarioEnum> comboModalitaDestinatario;

	/**
	 * Contatto da visualizzare.
	 */
	private DestinatarioRedDTO contattoDaVisualizzare;

	/**
	 * Contatti preferiti.
	 */
	private List<Contatto> contattiPreferiti;

	/**
	 * Servizio Rubrica.
	 */
	private final IRubricaFacadeSRV rubricaSRV;

	/**
	 * Contatti gruppo.
	 */
	private List<Contatto> contattiNelGruppo;

	/**
	 * Bean di sessione.
	 */
	private final SessionBean sessionBean;

	/**
	 * Contatto da eliminare.
	 */
	private Contatto eliminaContattoItem;

	/**
	 * Contatto da inserire.
	 */
	private Contatto inserisciContattoItem;

	/**
	 * Nota richiesta creazione.
	 */
	private String notaRichiestaCreazione;

	/**
	 * Nota richiesta eliminazione.
	 */
	private String notaRichiestaEliminazione;

	/**
	 * Nota richiesta modifica.
	 */
	private String notaRichiestaModifica;

	/**
	 * Flag ricerca RED.
	 */
	private boolean ricercaRED;

	/**
	 * Root albero.
	 */
	private DefaultTreeNode root;

	/**
	 * Flag ricerca IPA.
	 */
	private boolean ricercaIPA;

	/**
	 * Flag ricerca rubrica ufficio.
	 */
	private boolean ricercaRubricaUfficio;

	/**
	 * Query ricerca.
	 */
	private String queryRicerca;

	/**
	 * Flag nascondi tabella.
	 */
	private boolean hiddenTable;

	/**
	 * Lista contatti.
	 */
	private List<Contatto> resultRicercaContatti;
	
	/**
     * Old contatto item.
     */
	private Contatto contattoVecchioItem;

	/**
	 * Flag modifica.
	 */
	private boolean inModifica;

	/**
	 * Id client.
	 */
	private String clientId;

	/**
	 * Costruttore, inizializza i Service che saranno utili per la logica di
	 * business.
	 */
	public DestinatariInputComponent() {
		inoltraSRV = ApplicationContextProvider.getApplicationContext().getBean(IInoltraMailDaCodaAttivaFacadeSRV.class);
		destinatariInModificaPuliti = new ArrayList<>();

		final IDocumentManagerFacadeSRV documentManagerSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentManagerFacadeSRV.class);
		comboModalitaDestinatario = documentManagerSRV.retrieveComboModalitaDestinatario();
		rubricaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaFacadeSRV.class);

		sessionBean = FacesHelper.getManagedBean(MBean.SESSION_BEAN);
	}

	/**
	 * Gestisce l'apertura dei destinatari in rubrica.
	 * @param clientId
	 */
	@Override
	public void openRubricaDestinatari(final String clientId) {
		try {
			sessionBean.setShowRubrica(true);
			contattiPreferiti = rubricaSRV.getPreferiti(sessionBean.getUtente().getIdUfficio(), sessionBean.getUtente().getIdAoo(), true);
			inserisciContattoItem = new Contatto();
			inModifica = false;
			this.clientId = clientId;
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage("Si è verificato un errore durante l'apertura della rubrica");
		}
	}

	/**
	 * Recupera i risultati.
	 * @param collection
	 * @param salva
	 * @param r
	 * @return esito
	 */
	@Override
	public boolean restituisciRisultati(final Collection<Contatto> collection, final boolean salva, final RubricaChiamanteEnum r) {
		try {
			if (salva) {
				boolean somePecOrPeo = false;
				final List<DestinatarioRedDTO> newList = new ArrayList<>();

				for (final Contatto c : collection) {
					DestinatarioRedDTO destToAdd = null;
					destToAdd = getDestinatarioFrom(c);
					if (destToAdd != null) {
						// aggiorno con il nuovo contatto.
						destToAdd.setContatto(c);
					} else {
						// lo cotruisco nuovo a partire dal contratto
						destToAdd = inoltraSRV.checkAndTrasformFromContattoToDestinatario(c);
						somePecOrPeo = somePecOrPeo || destToAdd == null;
					}
					// nel caso lo inserisco
					if (destToAdd != null) {
						newList.add(destToAdd);
					}
				}

				if (somePecOrPeo) {
					showErrorMessage("Sono consentiti esclusivamente destinatari esterni con mail PEC o PEO configurata.");
					return false;
				}
				this.destinatariInModificaPuliti = newList;
			}
			// Tocca metterla lato java perché sta in pagina a volte.
			FacesHelper.executeJS("PF('wdgSelezionaDaRubrica').hide()");
		} catch (final Exception ex) {
			LOGGER.error(ex);
			showErrorMessage("Impossibile completare l'operazione");
		}
		return true;
	}

	/**
	 * Dalla lista di destinatari se trova il nostro destinatario lo restituisce.
	 * 
	 * @param c Contatto da matchare.
	 * @return il destinatario ricercato. Null se non lo ha trovato.
	 */
	private DestinatarioRedDTO getDestinatarioFrom(final Contatto c) {

		for (final DestinatarioRedDTO d : getDestinatariInModificaPuliti()) {
			if ((c.getContattoID().equals(d.getContatto().getContattoID()) && c.getContattoID() != null)
					|| (StringUtils.isNullOrEmpty(c.getAliasContatto()) && StringUtils.isNullOrEmpty(d.getContatto().getAliasContatto()))
					|| (!StringUtils.isNullOrEmpty(c.getAliasContatto()) && c.getAliasContatto().equals(d.getContatto().getAliasContatto()))) {
				return d;
			}
		}
		return null;
	}

	/**
	 * Aggiorna i componenti interfaccia.
	 * @param r
	 */
	@Override
	public void aggiornaComponentiInterfaccia(final RubricaChiamanteEnum r) {
		FacesHelper.update("centralSectionForm:pnlDestinatari_IMDCA");
	}

	/**
	 * Restituisce la lista dei destinatari in modifica puliti sotto forma di
	 * DestinatarioRedDTO.
	 */
	@Override
	public List<DestinatarioRedDTO> getDestinatariInModificaPuliti() {
		return destinatariInModificaPuliti;
	}

	/**
	 * Imposta la lista dei destinatari in modifica puliti.
	 * 
	 * @param destinatariInModificaNonPuliti
	 */
	public void setDestinatariInModificaPuliti(final List<DestinatarioRedDTO> destinatariInModificaNonPuliti) {
		this.destinatariInModificaPuliti = destinatariInModificaNonPuliti;

	}

	/**
	 * Restituisce le modalita dei destinatari.
	 * @return lista delle modalità
	 */
	@Override
	public List<ModalitaDestinatarioEnum> getComboModalitaDestinatario() {
		return comboModalitaDestinatario;
	}

	/**
	 * Restituisce il contatto da visualizzare.
	 * @return destinatario da visualizzare
	 */
	@Override
	public DestinatarioRedDTO getContattoDaVisualizzare() {
		return this.contattoDaVisualizzare;
	}

	/**
	 * Imposta il contatto d come contatto da visualizzare.
	 * @param d contatto da impostare
	 */
	@Override
	public void selectDestinatarioDaVisualizzare(final DestinatarioRedDTO d) {
		this.contattoDaVisualizzare = d;
	}

	/**
	 * Elimina il destinatario identificato dall'id: <code> d </code>.
	 */
	@Override
	public void eliminaDestinatario(final Integer d) {
		try {
			if (d == null) {
				throw new RedException("Il valore recuperato dalla pagine è null");
			}
			destinatariInModificaPuliti.remove(d.intValue());
		} catch (final Exception index) {
			LOGGER.error(index.getMessage(), index);
			showErrorMessage("Errore durante l'operazione di eliminazione");
		}
	}

	/**
	 * Effettua una ricerca nel gruppo selezionato e aggiorna l'attributo
	 * contattiNelGruppo con quelli recuperati.
	 * 
	 * @param gruppoSelected
	 */
	public void ricercaContattiNelGruppo(final Contatto gruppoSelected) {
		contattiNelGruppo = RubricaComponent.ricercaContattiNelGruppo(gruppoSelected, sessionBean.getUtente().getIdUfficio());
	}

	/**
	 * Restituisce la lista dei contatti preferiti.
	 * 
	 * @return contattiPreferiti
	 */
	public List<Contatto> getContattiPreferiti() {
		return contattiPreferiti;
	}

	/**
	 * Imposta la lista dei contatti preferiti.
	 * 
	 * @param contattiPreferiti
	 */
	public void setContattiPreferiti(final List<Contatto> contattiPreferiti) {
		this.contattiPreferiti = contattiPreferiti;
	}

	/**
	 * Restituisce la lista dei contatti nel gruppo.
	 * 
	 * @return contattiNelGruppo
	 */
	public List<Contatto> getContattiNelGruppo() {
		return contattiNelGruppo;
	}

	/**
	 * Imposta la lista dei contatti nel gruppo.
	 * 
	 * @param contattiNelGruppo
	 */
	public void setContattiNelGruppo(final List<Contatto> contattiNelGruppo) {
		this.contattiNelGruppo = contattiNelGruppo;
	}

	/**
	 * Gestisce l'eliminazione del contatto e mostra un messaggio di avvenuta
	 * eliminazione in caso di esito positivo. Viceversa mostra un messaggio di
	 * errore.
	 * 
	 * @param contattoSelected
	 * @param fromRisultatiRicerca
	 */
	public void eliminaContatto(final Contatto contattoSelected, final boolean fromRisultatiRicerca) {
		try {
			eliminaContatto(contattoSelected);
			if (fromRisultatiRicerca) {
				final DataTable dtRic = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
						.findComponent(this.clientId + ":preferitoTabInoltraMailDestV:idRicRubrica_DMDRC");
				if (dtRic != null) {
					dtRic.reset();
				}
				this.resultRicercaContatti.remove(contattoSelected);
			} else {
				final DataTable dtPref = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
						.findComponent(this.clientId + ":preferitoTabInoltraMailDestV:contattiPreferitiTbl");
				if (dtPref != null) {
					dtPref.reset();
				}
				this.contattiPreferiti.remove(contattoSelected);
			}
			showInfoMessage("Eliminazione contatto avvenuta con successo");

		} catch (final Exception ex) {
			LOGGER.error("Errore durante l'eliminazione del contatto", ex);
			showErrorMessage("Errore durante l'eliminazione del contatto");
		}
	}

	private void eliminaContatto(final Contatto contattoSelected) {
		rubricaSRV.elimina(contattoSelected.getContattoID(), null, null);
	}

	/**
	 * Restituisce il true se l'utente ha il permesso GESTIONE_RUBRICA.
	 * 
	 * @see PermessiEnum
	 * @return true se ha il permesso, false altrimenti
	 */
	public boolean isPermessoUtenteGestioneRichiesteRubrica() {
		return PermessiUtils.hasPermesso(sessionBean.getUtente().getPermessi(), PermessiEnum.GESTIONE_RUBRICA);
	}

	/**
	 * Imposta il contatto da eliminare con quello selezionato.
	 * 
	 * @param event
	 */
	public void impostaContattoDaEliminare(final ActionEvent event) {
		final Contatto c = getDataTableClickedRow(event);
		eliminaContattoItem = new Contatto();
		eliminaContattoItem.copyFromContatto(c);
		notaRichiestaEliminazione = "";
	}

	/**
	 * Restituisce il contatto da eliminare.
	 * 
	 * @return eliminaContattoItem
	 */
	public Contatto getEliminaContattoItem() {
		return eliminaContattoItem;
	}

	/**
	 * Imposta il contatto da eliminare.
	 * 
	 * @param eliminaContattoItem
	 */
	public void setEliminaContattoItem(final Contatto eliminaContattoItem) {
		this.eliminaContattoItem = eliminaContattoItem;
	}

	/**
	 * Restituisce le note associate alla richiesta di eliminazione.
	 * 
	 * @return notaRichiestaEliminazione
	 */
	public String getNotaRichiestaEliminazione() {
		return notaRichiestaEliminazione;
	}

	/**
	 * Imposta le note associate alla richiesta di eliminazione.
	 * 
	 * @param notaRichiestaEliminazione
	 */
	public void setNotaRichiestaEliminazione(final String notaRichiestaEliminazione) {
		this.notaRichiestaEliminazione = notaRichiestaEliminazione;
	}

	/**
	 * Invia la notifica di eliminazione e mostra un messaggio che mostra l'esito
	 * all'utente.
	 */
	public void inviaNotificaEliminazione() {
		try {
			if (StringUtils.isNullOrEmpty(notaRichiestaEliminazione)) {
				showErrorMessage(ERROR_NOTA_MANCANTE_MSG);
				return;
			}
			RubricaComponent.inviaNotificaEliminazione(eliminaContattoItem, notaRichiestaEliminazione, sessionBean.getUtente());
			showInfoMessage("Richiesta di eliminazione inviata con successo");
			notaRichiestaEliminazione = "";
		} catch (final Exception e) {
			showErrorMessage(e);
		}
	}

	/**
	 * Aggiunge i contatti del gruppo selezionato ai destinatari.
	 * 
	 * @param gruppoSelected
	 */
	public void addContattiGruppo(final Contatto gruppoSelected) {
		final FacesContext context = FacesContext.getCurrentInstance();
		this.destinatariInModificaPuliti = RubricaComponent.addContattiGruppo(true, gruppoSelected, sessionBean.getUtente().getIdUfficio(), this.destinatariInModificaPuliti,
				context);
	}

	/**
	 * Aggiunge il destinatario esterno identificato da idContatto. Gestisce
	 * eventuali messaggi di errore.
	 * 
	 * @param idContatto
	 */
	public void addDestinatarioEsterno(final Long idContatto) {
		try {
			for (final DestinatarioRedDTO d : this.destinatariInModificaPuliti) {
				if (idContatto != null && d.getContatto() != null && idContatto.equals(d.getContatto().getContattoID())) {
					showWarnMessage("Il contatto è già stato inserito tra i destinatari!");
					return;
				}
			}
			Contatto selezionato = null;
			for (final Contatto pref : this.contattiPreferiti) {
				if (pref.getContattoID().equals(idContatto)) {
					selezionato = pref;
				}
			}
			if (selezionato == null) {
				showWarnMessage("Il contatto non è stato trovato!");
				return;
			}

			final DestinatarioRedDTO d = new DestinatarioRedDTO();

			d.setContatto(selezionato);
			if (StringUtils.isNullOrEmpty(selezionato.getMailSelected())) {
				d.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.CARTACEO);
				d.setMezzoSpedizioneDisabled(true);
			} else {
				d.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.ELETTRONICO);
				d.setMezzoSpedizioneDisabled(false);
			}

			d.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
			d.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
			d.setMezzoSpedizioneVisible(true);
			this.destinatariInModificaPuliti.add(d);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage("Errore durante l'operazione: " + e.getMessage());
		}

	}

	/**
	 * Gestisce l'evento di selezione del comune.
	 * 
	 * @param event
	 */
	public final void onSelectComune(final AjaxBehaviorEvent event) {
		try {
			if (event instanceof SelectEvent) {
				this.inserisciContattoItem.setComuneObj((ComuneDTO) ((SelectEvent) event).getObject());
			} else {
				this.inserisciContattoItem.setComuneObj(null);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showErrorMessage(e);
		}
	}

	/**
	 * Carica la lista dei comuni utilizzando la query che viene passata come
	 * parametro.
	 * 
	 * @param query
	 * @return Lista di ComuneDTO
	 */
	public final List<ComuneDTO> loadComuni(final String query) {
		List<ComuneDTO> list = null;
		try {
			list = RegioniProvinceComuniComponent.loadComuniAdd(query, this.inserisciContattoItem.getProvinciaObj());
		} catch (final Exception e) {
			LOGGER.error(e);
			showErrorMessage(e);
		}
		return list;
	}

	/**
	 * Carica la lista delle province utilizzando la query che viene passata come
	 * parametro.
	 * 
	 * @param query
	 * @return Lista di ProvinciaDTO
	 */
	public final List<ProvinciaDTO> loadProvince(final String query) {
		List<ProvinciaDTO> province = null;
		try {
			province = RegioniProvinceComuniComponent.loadProvinceAdd(query);
		} catch (final Exception e) {
			LOGGER.error(e);
			showErrorMessage(e);
		}
		return province;
	}

	/**
	 * Esegue la validazione del contatto, se valido chiama il metodo che effettua
	 * la insert.
	 */
	public void validaEInserisciContatto() {
		try {
			final FacesContext context = FacesContext.getCurrentInstance();
			if (!RubricaComponent.valida(inserisciContattoItem, context)) {
				return;
			}

			inserisciContattoItem.setIdAOO(sessionBean.getUtente().getIdAoo());
			creaUtente();
			setInserisciContattoItem(new Contatto());
			notaRichiestaCreazione = "";
		} catch (final Exception e) {
			LOGGER.error(e);
			showErrorMessage(e);
		}
	}

	/**
	 * Esegue la insert del contatto.
	 */
	private void creaUtente() {
		this.inserisciContattoItem.setOnTheFly(0);
		RubricaComponent.inserisciContatto(this.inserisciContattoItem, sessionBean.getUtente());

		final DestinatarioRedDTO d = new DestinatarioRedDTO();
		setDestinatarioPerCreazione(d);
		this.destinatariInModificaPuliti.add(d);

		if (Boolean.TRUE.equals(this.inserisciContattoItem.getIsPreferito())) {
			final Contatto contattoDaAggiungereAiPreferiti = new Contatto();
			contattoDaAggiungereAiPreferiti.copyFromContatto(this.inserisciContattoItem);
			contattiPreferiti.add(0, contattoDaAggiungereAiPreferiti);

		}
		showInfoMessage("Contatto creato correttamente");

	}

	/**
	 * Esegue la validazione del contatto e chiama il metodo che effettua la
	 * modifica.
	 */
	public void modificaContatto() {

		try {
			final FacesContext context = FacesContext.getCurrentInstance();
			if (!RubricaComponent.valida(this.inserisciContattoItem, context)) {
				return;
			}
			this.inserisciContattoItem.setIdAOO(sessionBean.getUtente().getIdAoo());
			modificaUtente();
			setInserisciContattoItem(new Contatto());

		} catch (final Exception e) {
			final String errorCode = "MAILESISTENTE";
			if (e.getMessage().contains(errorCode)) {
				showErrorMessage("Non è possibile modificare il contatto, è già presente a sistema un contatto con la stessa mail");
			} else {
				LOGGER.error(e);
				showErrorMessage(e);
			}
		}
	}

	/**
	 * Esegue la modifica del contatto comunicandone il successo in caso di esito
	 * positivo.
	 */
	private void modificaUtente() {
		RubricaComponent.modificaContatto(this.inserisciContattoItem, sessionBean.getUtente());

		final DestinatarioRedDTO d = new DestinatarioRedDTO();
		setDestinatarioPerCreazione(d);
		this.destinatariInModificaPuliti.add(d);
		inModifica = false;
		showInfoMessage("Modifica contatto effettuata");
	}

	/**
	 * Inizializza il contatto da inserire con un nuovo contatto.
	 */
	public void pulisciUtente() {
		setInserisciContattoItem(new Contatto());
		inModifica = false;
	}

	/**
	 * Imposta il destinatario per creazione e tutti i parametri associati.
	 * 
	 * @param d
	 */
	private void setDestinatarioPerCreazione(final DestinatarioRedDTO d) {
		if (!StringUtils.isNullOrEmpty(this.inserisciContattoItem.getMailPec())) {
			this.inserisciContattoItem.setMailSelected(this.inserisciContattoItem.getMailPec());
			this.inserisciContattoItem.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEC);
		} else {
			this.inserisciContattoItem.setMailSelected(this.inserisciContattoItem.getMail());
			this.inserisciContattoItem.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEO);
		}

		d.setContatto(this.inserisciContattoItem);
		if (StringUtils.isNullOrEmpty(this.inserisciContattoItem.getMailSelected())) {
			d.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.CARTACEO);
			d.setMezzoSpedizioneDisabled(true);
		} else {
			d.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.ELETTRONICO);
			d.setMezzoSpedizioneDisabled(false);
		}

		d.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
		d.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
		d.setMezzoSpedizioneVisible(true);

	}

	/**
	 * Rimuove il contatto preferito identificato dalla riga del datatable
	 * selezionata.
	 * 
	 * @param event
	 */
	public void rimuoviPreferito(final ActionEvent event) {
		try {
			final Contatto contatto = getDataTableClickedRow(event);

			rubricaSRV.rimuoviPreferito(sessionBean.getUtente().getIdUfficio(), contatto.getContattoID(), contatto.getTipoRubricaEnum());
			contatto.setIsPreferito(false);
			int index = 0;
			for (final Contatto c : this.contattiPreferiti) {
				if (c.getContattoID().equals(contatto.getContattoID())) {
					break;
				}
				index++;
			}

			this.contattiPreferiti.remove(index);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage(e);
		}
	}

	/**
	 * Aggiunge un contatto ai preferiti identificandolo dalla riga selezionata.
	 * 
	 * @param event
	 */
	public void aggiungiPreferito(final ActionEvent event) {
		try {
			final Contatto contatto = getDataTableClickedRow(event);

			rubricaSRV.aggiungiAiPreferiti(sessionBean.getUtente().getIdUfficio(), contatto.getContattoID(), contatto.getTipoRubricaEnum(), sessionBean.getUtente().getId());
			contatto.setIsPreferito(true);
			if (this.contattiPreferiti == null) {
				setContattiPreferiti(new ArrayList<>());
			}
			this.contattiPreferiti.add(contatto);
			contatto.setIsPreferito(true);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage(e);
		}
	}

	/**
	 * Restituisce il contatto in fase di inserimento.
	 * 
	 * @return
	 */
	public Contatto getInserisciContattoItem() {
		return inserisciContattoItem;
	}

	/**
	 * Imposta il contatto che deve essere inserito.
	 * 
	 * @param inserisciContattoItem
	 */
	public void setInserisciContattoItem(final Contatto inserisciContattoItem) {
		this.inserisciContattoItem = inserisciContattoItem;
	}

	/**
	 * Restituisce le note della richiesta di creazione.
	 * 
	 * @return
	 */
	public String getNotaRichiestaCreazione() {
		return notaRichiestaCreazione;
	}

	/**
	 * Imposta le note della richiesta di creazione.
	 * 
	 * @param notaRichiestaCreazione
	 */
	public void setNotaRichiestaCreazione(final String notaRichiestaCreazione) {
		this.notaRichiestaCreazione = notaRichiestaCreazione;
	}

	/**
	 * Restituisce le note della richiesta di modifica.
	 * 
	 * @return
	 */
	public String getNotaRichiestaModifica() {
		return notaRichiestaModifica;
	}

	/**
	 * Imposta le note della richiesta di modifica.
	 * 
	 * @param notaRichiestaModifica
	 */
	public void setNotaRichiestaModifica(final String notaRichiestaModifica) {
		this.notaRichiestaModifica = notaRichiestaModifica;
	}

	/**
	 * Esegue la ricerca del contatto di rubrica in base ai parametri definiti.
	 */
	public void cercaContattoRubrica() {
		try {
			hiddenTable = false;
			if (ricercaRED || ricercaIPA) {
				final boolean ricercaMEF = ricercaRED;
				setResultRicercaContatti(rubricaSRV.ricercaCampoSingolo(queryRicerca, sessionBean.getUtente().getIdUfficio(), sessionBean.getUtente().getIdAoo().intValue(),
						ricercaRED, ricercaIPA, ricercaMEF, false, ""));
			} else if (ricercaRubricaUfficio) {
				setResultRicercaContatti(rubricaSRV.ricercaCampoSingolo(queryRicerca, sessionBean.getUtente().getIdUfficio(), sessionBean.getUtente().getIdAoo().intValue(),
						ricercaRED, ricercaIPA, true, true, ""));
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage(e);
		}
	}

	/**
	 * Restituisce la root del tree.
	 * 
	 * @return
	 */
	public DefaultTreeNode getRoot() {
		return root;
	}

	/**
	 * Imposta la root del tree.
	 * 
	 * @param root
	 */
	public void setRoot(final DefaultTreeNode root) {
		this.root = root;
	}

	/**
	 * Se l'evento associato è riferito ad IPA imposta la root in modo da mostrare
	 * l'alberatura IPA.
	 * 
	 * @see TipoRubricaEnum
	 * @param event
	 */
	public void showAlberaturaContattoIPA(final ActionEvent event) {

		final Contatto contSel = getDataTableClickedRow(event);
		if (contSel.getTipoRubricaEnum().equals(TipoRubricaEnum.IPA)) {
			rubricaSRV.getContattiChildIPA(contSel);
			setRoot(new DefaultTreeNode("Root", null));
			getRoot().setSelectable(true);
			// Popola l'albero dal nodo corrente fino alla root
			getRecursiveNode(contSel);

		}
	}

	/**
	 * Restituisce il nodo padre se esistente.
	 * 
	 * @param contattoIPA
	 * @return TreeNode
	 */
	private TreeNode getRecursiveNode(final Contatto contattoIPA) {
		final Contatto contattoPadre = rubricaSRV.getPadreContattoIPA(contattoIPA);

		TreeNode nodoCorrente = null;
		if (contattoPadre == null) {
			nodoCorrente = new DefaultTreeNode(contattoIPA, getRoot());
			nodoCorrente.setExpanded(true);
			nodoCorrente.setSelectable(true);
			nodoCorrente.setType(contattoIPA.getTipoPersona());
		} else {
			final TreeNode nodoPadre = getRecursiveNode(contattoPadre);
			nodoCorrente = new DefaultTreeNode(contattoIPA, nodoPadre);
			nodoCorrente.setExpanded(true);
			nodoCorrente.setSelectable(true);
			nodoCorrente.setType(contattoIPA.getTipoPersona());

		}

		return nodoCorrente;
	}

	/**
	 * Esegue la modifica del destinatario.
	 * 
	 * @param modContatto
	 */
	public void modificaDestinatario(final Contatto modContatto) {
		if (isPermessoUtenteGestioneRichiesteRubrica()) {
			setInserisciContattoItem(modContatto);
		} else {
			final Contatto richiediModifica = new Contatto();
			richiediModifica.copyFromContatto(modContatto);
			setInserisciContattoItem(richiediModifica);
			contattoVecchioItem = new Contatto();
			contattoVecchioItem.copyFromContatto(modContatto);
		}
		inModifica = true;
		TabView tb = null;
		tb = (TabView) FacesContext.getCurrentInstance().getViewRoot().findComponent(this.clientId + ":preferitoTabInoltraMailDestV");
		tb.setActiveIndex(2);

	}

	/**
	 * Aggiunge il contatto recuperato dall'indice ai destinatari.
	 * 
	 * @param index
	 */
	public void addDestinatarioFromRicercaRubrica(final Object index) {
		try {
			final Integer i = (Integer) index;
			final Contatto c = this.resultRicercaContatti.get(i);
			if (c == null) {
				showErrorMessage("Contatto non trovato.");
				return;
			}

			c.setMailSelected(c.getMailPec());
			if (StringUtils.isNullOrEmpty(c.getMailSelected())) {
				c.setMailSelected(c.getMail());
			}
			// Serve per disabilitare il tasto aggiungi, altrimenti può essere cliccato più
			// volte
			c.setDisabilitaElemento(true);

			for (final DestinatarioRedDTO d : this.destinatariInModificaPuliti) {
				if (c.getContattoID() != null && d.getContatto() != null && c.getContattoID().equals(d.getContatto().getContattoID())) {
					showWarnMessage("Il contatto è già stato inserito tra i destinatari!");
					return;
				}
			}

			final DestinatarioRedDTO d = new DestinatarioRedDTO();
			d.setContatto(c);
			if (StringUtils.isNullOrEmpty(c.getMailSelected())) {
				d.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.CARTACEO);
				d.setMezzoSpedizioneDisabled(true);
			} else {
				d.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.ELETTRONICO);
				d.setMezzoSpedizioneDisabled(false);
			}

			d.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
			d.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
			d.setMezzoSpedizioneVisible(true);
			this.destinatariInModificaPuliti.add(d);

		} catch (final Exception e) {
			LOGGER.error(e);
			showErrorMessage(e);
		}
	}

	/**
	 * Inizializza il tab resettando il datatable qualora non fosse già null.
	 */
	public void inizializzaTab() {
		hiddenTable = true;

		final TabView tb = (TabView) FacesContext.getCurrentInstance().getViewRoot().findComponent(this.clientId + ":preferitoTabInoltraMailDestV");
		tb.setActiveIndex(0);
		final DataTable dtPref = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
				.findComponent(this.clientId + ":preferitoTabInoltraMailDestV:contattiPreferitiTbl");
		if (dtPref != null) {
			dtPref.reset();
		}
		DataTable dtRic = null;
		dtRic = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(this.clientId + ":preferitoTabInoltraMailDestV:idRicRubrica_DMDRC");
		if (dtRic != null) {
			dtRic.reset();
		}

	}

	/**
	 * Invia la notifica di creazione gestendo eventuali errori relativi a campi
	 * obbligatori. A valle dell'invio mostra un messaggio che ne comunica l'esito.
	 */
	public void inviaNotificaCreazione() {
		try {
			if (StringUtils.isNullOrEmpty(notaRichiestaCreazione)) {
				showErrorMessage(ERROR_NOTA_MANCANTE_MSG);
				return;
			}

			final FacesContext context = FacesContext.getCurrentInstance();
			if (this.inserisciContattoItem != null) {
				inserisciContattoItem.setIdAOO(sessionBean.getUtente().getIdAoo());
				final boolean esito = RubricaComponent.inviaNotificaCreazione(inserisciContattoItem, notaRichiestaCreazione, sessionBean.getUtente(), context);
				if (!esito) {
					return;
				}
				showInfoMessage("Richiesta di creazione contatto inviata");
				notaRichiestaCreazione = "";
				final DestinatarioRedDTO d = new DestinatarioRedDTO();
				d.setContatto(inserisciContattoItem);
				d.getContatto().setOnTheFly(1);
				setDestinatarioPerCreazione(d);
				this.destinatariInModificaPuliti.add(d);
				FacesHelper.executeJS("PF('dialogRichCreazioneVW').hide();");
				FacesHelper.update("idDettagliEstesiForm:idDestinatari_DMD", "idDettagliEstesiForm:idTabs_DMD", "idDettagliEstesiForm:preferitoTabV");
			} else {
				showErrorMessage("Non è possibile procedere con l'invio della richiesta creazione");
			}
		} catch (final Exception ex) {
			showErrorMessage(ex);
		}
	}

	/**
	 * Invia la notifica di modifica gestendo eventuali errori relativi a campi
	 * obbligatori. A valle dell'invio mostra un messaggio che ne comunica l'esito.
	 */
	public void inviaNotificaModifica() {
		try {
			if (StringUtils.isNullOrEmpty(notaRichiestaModifica)) {
				showErrorMessage(ERROR_NOTA_MANCANTE_MSG);
				return;
			}

			final FacesContext context = FacesContext.getCurrentInstance();
			RubricaComponent.inviaNotificaModifica(this.inserisciContattoItem, contattoVecchioItem, notaRichiestaModifica, sessionBean.getUtente(), context);
			notaRichiestaModifica = null;
			showInfoMessage("Richiesta di modifica inviata con successo");
			final DestinatarioRedDTO d = new DestinatarioRedDTO();
			d.setContatto(this.inserisciContattoItem);
			d.getContatto().setOnTheFly(1);
			setDestinatarioPerCreazione(d);
			this.destinatariInModificaPuliti.add(d);
		} catch (final Exception e) {
			showErrorMessage(e);
		}
	}

	/**
	 * Rimuove dal gruppo il contatto selezionato.
	 * 
	 * @param contattoSelected
	 */
	public void deleteGruppoDaSingoloContatto(final Contatto contattoSelected) {
		if (contattoSelected == null) {
			showErrorMessage("Errore Generico");
			return;
		}

		this.destinatariInModificaPuliti = RubricaComponent.deleteGruppoDaSingoloContatto(contattoSelected, this.destinatariInModificaPuliti);
	}

	/**
	 * Restituisce true se si è in modalità ricerca di RED.
	 * 
	 * @return true se in ricerca RED, false altrimenti
	 */
	public boolean isRicercaRED() {
		return ricercaRED;
	}

	/**
	 * Imposta il flag che fa riferimento alla ricerca RED.
	 * 
	 * @param ricercaRED
	 */
	public void setRicercaRED(final boolean ricercaRED) {
		this.ricercaRED = ricercaRED;
	}

	/**
	 * Restituisce true se si è in modalità ricerca IPA.
	 * 
	 * @return true se in ricerca IPA, false altrimenti
	 */
	public boolean isRicercaIPA() {
		return ricercaIPA;
	}

	/**
	 * Imposta il flag che fa riferimento alla ricerca IPA.
	 * 
	 * @param ricercaIPA
	 */
	public void setRicercaIPA(final boolean ricercaIPA) {
		this.ricercaIPA = ricercaIPA;
	}

	/**
	 * Getter ricercaRubricaUfficio.
	 * 
	 * @return true se in ricerca rubrica Ufficio.
	 */
	public boolean isRicercaRubricaUfficio() {
		return ricercaRubricaUfficio;
	}

	/**
	 * Imposta il flag: ricercaRubricaUfficio.
	 * 
	 * @param ricercaRubricaUfficio
	 */
	public void setRicercaRubricaUfficio(final boolean ricercaRubricaUfficio) {
		this.ricercaRubricaUfficio = ricercaRubricaUfficio;
	}

	/**
	 * Restituisce la query di ricerca.
	 * 
	 * @return queryRicerca
	 */
	public String getQueryRicerca() {
		return queryRicerca;
	}

	/**
	 * Imposta la query per eseguire la ricerca.
	 * 
	 * @param queryRicerca
	 */
	public void setQueryRicerca(final String queryRicerca) {
		this.queryRicerca = queryRicerca;
	}

	/**
	 * Restituisce il flag hiddentTable.
	 * 
	 * @return hiddentTable
	 */
	public boolean isHiddenTable() {
		return hiddenTable;
	}

	/**
	 * Restituisce il contatto associato al vecchio item.
	 * 
	 * @return contattoVecchioItem
	 */
	public Contatto getContattoVecchioItem() {
		return contattoVecchioItem;
	}

	/**
	 * Imposta il contattoVecchioItem.
	 * 
	 * @param contattoVecchioItem
	 */
	public void setContattoVecchioItem(final Contatto contattoVecchioItem) {
		this.contattoVecchioItem = contattoVecchioItem;
	}

	/**
	 * Imposta il parametro hiddenTable.
	 * 
	 * @param hiddenTable
	 */
	public void setHiddenTable(final boolean hiddenTable) {
		this.hiddenTable = hiddenTable;
	}

	/**
	 * Restituisce la lista di contatti recuperati dalla ricerca.
	 * 
	 * @return
	 */
	public List<Contatto> getResultRicercaContatti() {
		return resultRicercaContatti;
	}

	/**
	 * Imposta la lista dei contatti recuperati dalla ricerca.
	 * 
	 * @param resultRicercaContatti
	 */
	public void setResultRicercaContatti(final List<Contatto> resultRicercaContatti) {
		this.resultRicercaContatti = resultRicercaContatti;
	}

	/**
	 * Restituisce il flag relativo alla modifica.
	 * 
	 * @return true se in modifica, false altrimenti
	 */
	public boolean isInModifica() {
		return inModifica;
	}

	/**
	 * Imposta il flag inModifica.
	 * 
	 * @param inModifica
	 */
	public void setInModifica(final boolean inModifica) {
		this.inModifica = inModifica;
	}

	/**
	 * Restituisce il client id.
	 * 
	 * @return
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * Imposta il client id.
	 * 
	 * @param clientId
	 */
	public void setClientId(final String clientId) {
		this.clientId = clientId;
	}
}
