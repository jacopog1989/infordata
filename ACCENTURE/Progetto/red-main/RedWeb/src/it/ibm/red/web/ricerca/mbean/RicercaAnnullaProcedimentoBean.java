package it.ibm.red.web.ricerca.mbean;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IASignFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;

/**
 * Bean che gestisce la funzionalità della ricerca dei procedimenti annullati.
 */
@RequestScoped
@Named(ConstantsWeb.MBean.RICERCA_ANNULLA_PROCEDIMENTO_BEAN)
public class RicercaAnnullaProcedimentoBean extends AbstractBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 1561924608515666735L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaAnnullaProcedimentoBean.class);
	
	/**
	 * Bean di ricerca.
	 */
	private RicercaDocumentiRedAbstractBean ricerca;

	/**
	 * Motivazione.
	 */
	private String motivazione;

	/**
	 * Provvedimento.
	 */
	private String provvedimento;

	/**
	 * Flag registrato.
	 */
	private boolean registred = false;

	/**
	 * Flag nps.
	 */
	private boolean beforeWasNps = false;
	
	@Override
	protected void postConstruct() {
		// Metodo intenzionalmente vuoto.
	}
	
	/**
	 * Esegue la logica di annullamento del procedimento.
	 */
	public void registra() {
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		final UtenteDTO utente = sessionBean.getUtente();
		final MasterDocumentRedDTO currentMaster = ricerca.getSelectedDocument();
		EsitoOperazioneDTO esito = null; 
		
		final IASignFacadeSRV asyncSignSRV = ApplicationContextProvider.getApplicationContext().getBean(IASignFacadeSRV.class);
		try {
			if(Boolean.TRUE.equals(asyncSignSRV.isPresent(currentMaster.getDocumentTitle()))) {
				showWarnMessage("Impossibile annullare il documento poiché in fase di invio in firma.");
				return;
			}
			IDocumentoRedFacadeSRV documentoRedSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoRedFacadeSRV.class);
			esito = documentoRedSRV.annullaDocumentoOAnnullaProtocollo(currentMaster.getDocumentTitle(), motivazione, provvedimento, utente);
			
			if (esito.isEsito()) {
				showInfoMessage("Annullamento del documento effettuato correttamente.");
				EsitoOperazioneDTO esitoRiattiva = documentoRedSRV.riattivaDocIngressoAfterChiusuraRisposta(currentMaster.getDocumentTitle(), utente);
				if(esitoRiattiva!=null){
					if(esitoRiattiva.isEsito()) {
						showInfoMessage("Riattivazione del doc ingresso associato avvenuta correttamente");
					} else {
						showError(esitoRiattiva.getNote());
					}
				}
			} else {
				showError(esito.getNote());
			}
		} catch (Exception e) {
			String msg = "Impossibile annullare il documento.";
			
			if(esito != null) {
				msg = esito.getNote();
			}
			
			LOGGER.error(msg, e);
			showError(msg);
			
		} finally {
			//update content
			ricerca.selectDetail(currentMaster);
			registred = true;
		}
	}
	
	/**
	 * Gestisce la chiusura.
	 */
	public void close() {
		setMotivazione(null);
		setProvvedimento(null);
		beforeWasNps = false;
		registred = false;
	}
	
	/**
	 * Verifica che il tipo di procedimento sia NPS.
	 * @return true se tipo procedimento è NPS, false altrimenti.
	 */
	public boolean isTipoProcedimentoNps() {
		final MasterDocumentRedDTO currentMaster = ricerca.getSelectedDocument();
		
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		final UtenteDTO utente = sessionBean.getUtente();
		if (!registred) {
			beforeWasNps = false;
			if (currentMaster != null) {
				beforeWasNps = utente !=  null 
						&& TipologiaProtocollazioneEnum.isProtocolloNPS(utente.getIdTipoProtocollo())
						//Verifichiamo che il documento sia NPS 
						&& currentMaster.getOggettoProtocolloNPS() != null 
						&& currentMaster.getOggettoProtocolloNPS().length() > 0;
			}
		}
		return beforeWasNps;
	}

	/**
	 * Restituisce la motivazione.
	 * @return motivazione
	 */
	public String getMotivazione() {
		return motivazione;
	}

	/**
	 * Imposta la motivazione.
	 * @param motivazione
	 */
	public void setMotivazione(final String motivazione) {
		this.motivazione = motivazione;
	}

	/**
	 * Restituisce il provvedimento.
	 * @return provvedimento
	 */
	public String getProvvedimento() {
		return provvedimento;
	}

	/**
	 * Imposta il provvedimento.
	 * @param provvedimento
	 */
	public void setProvvedimento(final String provvedimento) {
		this.provvedimento = provvedimento;
	}

	/**
	 * Imposta la ricerca.
	 * @param ricerca
	 */
	public void setRicerca(final RicercaDocumentiRedAbstractBean ricerca) {
		this.ricerca = ricerca;
	}
	
}
