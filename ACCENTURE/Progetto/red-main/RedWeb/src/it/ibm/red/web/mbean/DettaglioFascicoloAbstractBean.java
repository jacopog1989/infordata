package it.ibm.red.web.mbean;

import java.util.List;

import it.ibm.red.business.dto.DetailFascicoloRedDTO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDetailFascicoloFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.mbean.component.DettaglioFascicoloRedComponent;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Abstract bean dettaglio fascicolo.
 */
public abstract class DettaglioFascicoloAbstractBean extends AbstractBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 8210779264727079444L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DettaglioFascicoloAbstractBean.class);

	/**
	 * Utente.
	 */
	protected UtenteDTO utente;
	
	/**
	 * Fascciolo.
	 */
	protected DettaglioFascicoloRedComponent fascicoloCmp;
	
	/**
	 * Servizio.
	 */
	protected IDetailFascicoloFacadeSRV detailFascicoloSRV;
	
	/**
	 * Flag dialog fascicolo visibile.
	 */
	protected boolean dialogFascicoloVisible;

	/**
	 * Post construct del bean.
	 */
	@Override
	protected void postConstruct() { 
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		
		detailFascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IDetailFascicoloFacadeSRV.class);
		
		fascicoloCmp = new DettaglioFascicoloRedComponent(utente);

	}
	
	/**
	 * 
	 * @param numeroFascicolo
	 */
	public void setDetail(final FascicoloDTO fascicoloDTO) {
		try {
			fascicoloCmp.setDetail(detailFascicoloSRV.getFascicolo(fascicoloDTO, utente));
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Errore nell'operazione di recupero del fascicolo.");
		}
		
	}
	
	/**
	 * Permette di impostare il dettaglio (semplice o esteso).
	 * 
	 * @param idFascicolo
	 * 	idDelFascicolo selezionato
	 * @param documenti
	 * 	lista di documenti selezionati
	 */
	public void setDetail(final Integer idFascicolo, final List<DocumentoFascicoloDTO> documenti) {
		try {
			
			final DetailFascicoloRedDTO detail = detailFascicoloSRV.getDetailFascicolo(idFascicolo, documenti, utente);
			fascicoloCmp.setDetail(detail);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Errore nell'operazione di recupero del dettaglio del fascicolo.");
		}
	}

	/**
	 * Imposta il dettaglio del fasicolo identificato dall'id del fascicolo stesso.
	 * @param idFascicolo
	 */
	public void setDetail(final Integer idFascicolo) {
		try {
			
			final DetailFascicoloRedDTO detail = detailFascicoloSRV.getDetailFascicolo(idFascicolo, utente);
			fascicoloCmp.setDetail(detail);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Errore nell'operazione di recupero del dettaglio del fascicolo.");
		}
	}

	/**
	 * Resetta il dettaglio.
	 */
	public void unsetDetail() {
		dialogFascicoloVisible = false;
		fascicoloCmp.setDetail(null);
	}

	/**
	 * Apre la dialog per la gestione del fascicolo.
	 */
	public void openDialogFascicolo() {
		
		try {
			
			final FascicoloManagerBean bean = FacesHelper.getManagedBean(MBean.FASCICOLO_MANAGER_BEAN);
			bean.setDetail(fascicoloCmp.getDetail());
			bean.setDialogFascicoloVisible(true);
			
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di recupero del fascicolo.");
		}
		
	}

	/**
	 * Restituisce true se la dialog del fascicolo è visibile.
	 * @return dialogFascicoloVisible
	 */
	public boolean isDialogFascicoloVisible() {
		return dialogFascicoloVisible;
	}

	/**
	 * Restituisce il component per la gestione del dettaglio fascicolo.
	 * @return il componente
	 */
	public DettaglioFascicoloRedComponent getFascicoloCmp() {
		return fascicoloCmp;
	}
}
