package it.ibm.red.web.servlets.delegate;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.ISignSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;
import it.ibm.red.web.servlets.delegate.exception.DelegateException;

/**
 * Abstract delegate.
 */
public abstract class AbstractDelegate implements IDelegate {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AbstractDelegate.class.getName());

	/**
	 * Gestisce la logica di aggiornamento dello stato di un documento.
	 * 
	 * @param idUtente
	 * @param documentTitle
	 * @param metadatoPdfErrorValue
	 * @throws DelegateException
	 * */
	public void aggiornaStatoDocumento(final String signTransactionId, final Long idUtente, final String documentTitle, final Integer metadatoPdfErrorValue) throws DelegateException {
		try {
			
			IUtenteFacadeSRV utenteSRV = ApplicationContextProvider.getApplicationContext().getBean(IUtenteSRV.class);
			ISignFacadeSRV signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignSRV.class);
			
			UtenteDTO utente = utenteSRV.getById(idUtente);
			String logId = signTransactionId + documentTitle;
			signSRV.aggiornaMetadatoTrasformazione(logId, documentTitle, utente, metadatoPdfErrorValue);
			
		} catch (Exception e) {
			LOGGER.error("Errore in fase di aggiornamento del metadato metadatoPdfErrorValue per il documento: " + documentTitle, e);
			throw new DelegateException("Errore in fase di aggiornamento del metadato metadatoPdfErrorValue", e);
		}
	}
	
}
