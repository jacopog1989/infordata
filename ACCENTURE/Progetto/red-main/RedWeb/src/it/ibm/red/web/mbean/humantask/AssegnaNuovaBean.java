package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ColoreNotaEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.INotaFacadeSRV;
import it.ibm.red.business.utils.OrganigrammaRetrieverUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;
import it.ibm.red.web.mbean.interfaces.OrganigrammaRetriever;

/**
 * @author d.ventimiglia.ibm
 */
@Named(ConstantsWeb.MBean.ASSEGNANUOVA_BEAN)
@ViewScoped
public final class AssegnaNuovaBean extends AbstractBean implements InitHumanTaskInterface, OrganigrammaRetriever {

	private static final long serialVersionUID = 6389939703872017347L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AssegnaNuovaBean.class.getName());

	/**
	 * Informazione utente.
	 */
	private UtenteDTO utente;

	/**
	 * Bean di sessione.
	 */

	/**
	 * Lista documenti bean.
	 */
	private ListaDocumentiBean listaDocBean;

	/**
	 * Assegnazione bean.
	 */
	private AssegnaBean assegnaBean;

	/**
	 * Richiesta contributi bean.
	 */
	private RichiediContributoBean richiediContributoBean;

	/**
	 * Messa in conoscenza bean.
	 */
	private MettiInConoscenzaBean mettiConoscenzaBean;

	/**
	 * Documento selezionato.
	 */
	private MasterDocumentRedDTO docSelezionato;

	/**
	 * Messaggio.
	 */
	private String messaggio;

	/**
	 * Flag urgente.
	 */
	private Boolean isUrgente;

	/**
	 * booleano che da response (radio in maschera) permette di salvare la
	 * motivazione sullo storico note (true) oppure no (false). valore di default:
	 * false
	 */
	private boolean alsoStorico = false;

	/**
	 * Servizio gestione nota.
	 */
	private INotaFacadeSRV notaSRV;

	private AssegnaNuovaBean() {

	}

	@Override
	@PostConstruct
	protected void postConstruct() {
		listaDocBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
		assegnaBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.ASSEGNA_BEAN);
		richiediContributoBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.RICHIEDI_CONTRIBUTO_BEAN);
		mettiConoscenzaBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.METTI_IN_CONOSCESCENZA_BEAN);
		final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);

		notaSRV = ApplicationContextProvider.getApplicationContext().getBean(INotaFacadeSRV.class);

		utente = sb.getUtente();

		setMessaggio("");
		setIsUrgente(false);
	}

	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		docSelezionato = inDocsSelezionati.get(0);
		assegnaBean.initBean(inDocsSelezionati);
		richiediContributoBean.initBean(inDocsSelezionati);
		mettiConoscenzaBean.initBean(inDocsSelezionati);
	}

	/**
	 * Esegue le azioni relative alla funzionalità registra associata
	 * all'assegnazione. Esegue la validazione dei campi, la richiesta contributo e
	 * la messa in conoscenza, successivamente esegue l'assegnazione per competenza
	 * e chiude la dialog. Gestisce eventuali messaggi di errore.
	 */
	public void registra() {
		listaDocBean.cleanEsiti();

		// passo le info di urgenza e messaggio ai bean
		richiediContributoBean.setIsUrgente(getIsUrgente());
		richiediContributoBean.setMessage(getMessaggio());
		mettiConoscenzaBean.setMotivoAssegnazioneNew(getMessaggio());
		assegnaBean.setMotivoAssegnazione(getMessaggio());
		try {
			validazioneCampi();

			eseguiRichiediContributo();
			eseguiMettiInConoscenza();

			if (listaDocBean.getEsitiOperazione().isEmpty() || continuaRegistra(listaDocBean.getEsitiOperazione())) {
				eseguiAssegnaPerCompetenza();
			}

			if ((!listaDocBean.getEsitiOperazione().isEmpty() && listaDocBean.checkEsiti(false)) && isAlsoStorico()) {
				notaSRV.registraNotaFromDocumento(utente, Integer.parseInt(docSelezionato.getDocumentTitle()), ColoreNotaEnum.NERO, getMessaggio());
			}

			chiudi();
		} catch (final RedException rede) {
			LOGGER.error(rede);
			showError(rede);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Errore nell'esecuzione della response.");
		}
	}

	private void eseguiRichiediContributo() {
		EsitoOperazioneDTO esito = null;
		final String wobNumber = docSelezionato.getWobNumber();

		final List<NodoOrganigrammaDTO> nodiAssegnatari = richiediContributoBean.getAssegnatariBean().getSelectedElementList();
		// se il campo obbligatorio è vuoto, non eseguo la response
		if ((nodiAssegnatari == null || nodiAssegnatari.isEmpty())) {
			if (richiediContributoBean.getIsUrgente() != null && richiediContributoBean.getIsUrgente()) {
				final String e = "Inserire almeno un assegnatario per contributo";
				throw new RedException(e);
			}
			return;
		}

		final List<AssegnatarioOrganigrammaDTO> assegnatari = nodiAssegnatari.stream().map(c -> OrganigrammaRetrieverUtils.create(c)).collect(Collectors.toList());

		esito = richiediContributoBean.getContributoSRV().richiediContributo(utente, wobNumber, assegnatari, richiediContributoBean.getMessage(),
				richiediContributoBean.getIsUrgente(), docSelezionato.getAnnoProtocollo(), docSelezionato.getNumeroProtocollo());
		esito.setNote(ResponsesRedEnum.RICHIEDI_CONTRIBUTO.getResponse());
		listaDocBean.getEsitiOperazione().add(esito);
	}

	private void eseguiMettiInConoscenza() {
		// se il campo obbligatorio è vuoto, non eseguo la response
		if (mettiConoscenzaBean.getAssegnazioneBean().getSelectedElementList() == null || mettiConoscenzaBean.getAssegnazioneBean().getSelectedElementList().isEmpty()) {
			return;
		}

		final List<AssegnatarioOrganigrammaDTO> assegnatariPerConoscenza = mettiConoscenzaBean.getAssegnazioneBean().getSelectedElementList().stream()
				.map(c -> OrganigrammaRetrieverUtils.create(c)).collect(Collectors.toList());

		final String wobNumber = docSelezionato.getWobNumber();
		final EsitoOperazioneDTO esito = mettiConoscenzaBean.getModificaProcedimentoSRV().mettiInConoscenza(wobNumber, assegnatariPerConoscenza,
				mettiConoscenzaBean.getMotivoAssegnazioneNew(), utente, docSelezionato.getAnnoProtocollo(), docSelezionato.getNumeroProtocollo());
		esito.setNote(ResponsesRedEnum.METTI_IN_CONOSCENZA.getResponse());
		listaDocBean.getEsitiOperazione().add(esito);
	}

	private void eseguiAssegnaPerCompetenza() {
		// Se il campo obbligatorio è vuoto, non eseguo la response
		if (assegnaBean.getSelected() == null) {
			return;
		}

		final Collection<String> wobNumbers = new ArrayList<>();
		if (!StringUtils.isNullOrEmpty(docSelezionato.getWobNumber())) {
			wobNumbers.add(docSelezionato.getWobNumber());
		}
		Long nuovoUtente = assegnaBean.getSelected().getIdUtente();
		// Eseguo un controllo perche nel caso della selezione di un ufficio il sistema
		// si aspetta un idUtente a '0'
		if (assegnaBean.getSelected().getIdUtente() == null) {
			nuovoUtente = 0L;
		}
		final Collection<EsitoOperazioneDTO> esito = assegnaBean.getRiassegnaSRV().riassegna(assegnaBean.getUtente(), wobNumbers, ResponsesRedEnum.ASSEGNA,
				assegnaBean.getSelected().getIdNodo(), nuovoUtente, assegnaBean.getMotivoAssegnazione());
		for (final EsitoOperazioneDTO e : esito) {
			e.setNote(ResponsesRedEnum.ASSEGNA.getResponse());
		}
		listaDocBean.getEsitiOperazione().addAll(esito);
	}

	/**
	 * Chiude la dialog di assegnazione, mostrando l'esito per ogni operazione
	 * effettuata.
	 */
	public void chiudi() {
		FacesHelper.executeJS("PF('dlgGeneric').hide()");
		if (!listaDocBean.getEsitiOperazione().isEmpty() && listaDocBean.getEsitiOperazione() != null) {
			FacesHelper.update("centralSectionForm:idDlgEsiti");
			FacesHelper.executeJS("PF('dlgEsiti').show()");
		}
		listaDocBean.destroyBeanViewScope(ConstantsWeb.MBean.ASSEGNA_BEAN);
		listaDocBean.destroyBeanViewScope(ConstantsWeb.MBean.RICHIEDI_CONTRIBUTO_BEAN);
		listaDocBean.destroyBeanViewScope(ConstantsWeb.MBean.METTI_IN_CONOSCESCENZA_BEAN);
		listaDocBean.destroyBeanViewScope(ConstantsWeb.MBean.ASSEGNANUOVA_BEAN);
	}

	private static boolean continuaRegistra(final Collection<EsitoOperazioneDTO> esiti) {
		for (final EsitoOperazioneDTO esito : esiti) {
			if (Boolean.FALSE.equals(esito.getFlagEsito())) {
				return false;
			}
		}

		return true;
	}

	private void validazioneCampi() {
		if (!(!richiediContributoBean.getAssegnatarioBean().getSelectedElementList().isEmpty()
				|| !richiediContributoBean.getAssegnatariBean().getSelectedElementList().isEmpty()
				|| !mettiConoscenzaBean.getAssegnazioneBean().getSelectedElementList().isEmpty() || assegnaBean.permessoContinua())) {
			final String e = "Valorizzare almeno una tipologia di assegnazione";
			throw new RedException(e);
		}
	}

	@Override
	public TreeNode loadRootOrganigramma() {
		return null;
	}

	@Override
	public void onOpenTree(final TreeNode node) {
		// Questo metodo è lasciato intenazionalmente vuoto.
	}

	/**
	 * Restituisce il messaggio dell'assegnazione.
	 * 
	 * @return messaggio
	 */
	public String getMessaggio() {
		return messaggio;
	}

	/**
	 * Imposta il messaggio dell'assegnazione.
	 * 
	 * @param messaggio
	 */
	public void setMessaggio(final String messaggio) {
		this.messaggio = messaggio;
	}

	/**
	 * Restituisce il flag relativo all'urgenza dell'assegnazione.
	 * 
	 * @return true se urgente, false altrimenti
	 */
	public Boolean getIsUrgente() {
		return isUrgente;
	}

	/**
	 * Imposta il flag relativo all'urgenza dell'assegnazione.
	 * 
	 * @param isUrgente
	 */
	public void setIsUrgente(final Boolean isUrgente) {
		this.isUrgente = isUrgente;
	}

	/**
	 * Restituisce il flag relativo alla storicità.
	 * 
	 * @return true se è riferito allo storico, false altrimenti
	 */
	public boolean isAlsoStorico() {
		return alsoStorico;
	}

	/**
	 * Imposta il flag relativo alla storicità.
	 * 
	 * @param alsoStorico
	 */
	public void setAlsoStorico(final boolean alsoStorico) {
		this.alsoStorico = alsoStorico;
	}
}
