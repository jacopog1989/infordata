package it.ibm.red.web.mbean.interfaces;

import java.io.Serializable;

import org.primefaces.model.TreeNode;

/**
 * Un model che ha la logica di costruzione del model dell'organigramma
 * @author a.difolca
 *
 */
public interface OrganigrammaRetriever extends Serializable {
	/**
	 * Restituisce l'organigramma durante lo startup
	 * @return
	 */
	TreeNode loadRootOrganigramma();
	
	/**
	 * Aggiunge all'organigramma il nodo aperto
	 * @param node
	 */
	void onOpenTree(TreeNode node);
}
