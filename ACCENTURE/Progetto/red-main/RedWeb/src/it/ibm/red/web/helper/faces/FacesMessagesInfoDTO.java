package it.ibm.red.web.helper.faces;

/**
 * The Class FacesMessagesInfoDTO.
 *
 * @author CPIERASC
 * 
 *         Data Transfer Object usato per rappresentare i messaggi presenti
 *         (usato per le JSF).
 */
public class FacesMessagesInfoDTO {

	/**
	 * Numero messaggi informativi.
	 */
	private Integer nInfo;

	/**
	 * Numero messaggi warning.
	 */
	private Integer nWarn;

	/**
	 * Numero messaggi errore.
	 */
	private Integer nError;

	/**
	 * Getter numero messaggi informativi.
	 * 
	 * @return	numero messaggi informativi
	 */
	public final Integer getnInfo() {
		return nInfo;
	}

	/**
	 * Setter numero messaggi informativi.
	 * 
	 * @param inNInfo	numero messaggi informativi
	 */
	public final void setnInfo(final Integer inNInfo) {
		this.nInfo = inNInfo;
	}

	/**
	 * Getter numero messaggi warning.
	 * 
	 * @return	numero messaggi warning
	 */
	public final Integer getnWarn() {
		return nWarn;
	}

	/**
	 * Setter numero messaggi warning.
	 * 
	 * @param inNWarn	numero messaggi warning
	 */
	public final void setnWarn(final Integer inNWarn) {
		this.nWarn = inNWarn;
	}

	/**
	 * Getter numero messaggi errore.
	 * 
	 * @return	numero messaggi errore
	 */
	public final Integer getnError() {
		return nError;
	}

	/**
	 * Setter numero messaggi errore.
	 * 
	 * @param inNError	numero messaggi errore
	 */
	public final void setnError(final Integer inNError) {
		this.nError = inNError;
	}

	/**
	 * Getter flag messaggi presenti.
	 * 
	 * @return	flag messaggi presenti
	 */
	public final Boolean getMessagesPresent() {
		Boolean output = false;
    	if ((nInfo + nError + nWarn) > 0) {
    		output = true;
    	}
    	return output;
	}

	
}
