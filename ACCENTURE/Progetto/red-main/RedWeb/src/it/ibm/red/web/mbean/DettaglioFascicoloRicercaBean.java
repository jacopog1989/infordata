package it.ibm.red.web.mbean;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.DetailFascicoloRedDTO;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.interfaces.IUpdatableDetailCaller;

/**
 * Bean ricerca dettaglio fascicolo.
 */
@Named(ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN)
@ViewScoped
public class DettaglioFascicoloRicercaBean extends DettaglioFascicoloAbstractBean implements IUpdatableDetailCaller<DetailFascicoloRedDTO> {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 6561193040454280266L;
	
	/**
	 * Chiamante.
	 */
	private IUpdatableDetailCaller<DetailFascicoloRedDTO> caller; 

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		super.postConstruct();
	}

	/**
	 * Gestisce l'apertura della dialog dei fascicoli.
	 */
	@Override
	public void openDialogFascicolo() {
		
		super.openDialogFascicolo();
		
		final FascicoloManagerBean bean = FacesHelper.getManagedBean(MBean.FASCICOLO_MANAGER_BEAN);
		if (bean != null) {
			bean.setCaller(this);
			bean.setChiamante(ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN);
		}
		
	}
	
	/**
	 * Setting del caller da impostare quando si imposta il dettaglio della ricerca .
	 * 
	 * @param inCaller
	 */
	public void setCaller(final IUpdatableDetailCaller<DetailFascicoloRedDTO> inCaller) {
		this.caller = inCaller;
	}

	/**
	 * Aggiorna il dettaglio <code> detail </code>.
	 * 
	 * @param detail
	 *            dettaglio da aggiornare
	 */
	@Override
	public void updateDetail(final DetailFascicoloRedDTO detail) {
		this.fascicoloCmp.setDetail(detail);
		if (caller != null) {
			caller.updateDetail(detail);
		}
	}

}
