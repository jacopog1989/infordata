package it.ibm.red.web.mbean;
 
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.component.UIColumn;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.imageio.ImageIO;
import javax.inject.Named;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.primefaces.PrimeFaces;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.event.MenuActionEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.event.data.FilterEvent;
import org.primefaces.event.data.PageEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.Visibility;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuItem;
import org.primefaces.model.menu.MenuModel;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.crypt.DesCrypterNew;
import it.ibm.red.business.dto.ColonneCodeDTO;
import it.ibm.red.business.dto.DocumentAppletContentDTO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.GestioneLockDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO;
import it.ibm.red.business.dto.ResponsesDTO;
import it.ibm.red.business.dto.ServiceTaskDTO;
import it.ibm.red.business.dto.SignerInfoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.AccessoFunzionalitaEnum;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ColonneEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PostExecEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.SignErrorEnum;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.enums.SourceTypeEnum;
import it.ibm.red.business.enums.TaskTypeEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.UtenteFirma;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.provider.ServiceTaskProvider;
import it.ibm.red.business.service.ICodeDocumentiSRV;
import it.ibm.red.business.service.IDocumentoSRV;
import it.ibm.red.business.service.IRegistroRepertorioSRV;
import it.ibm.red.business.service.concrete.MasterPageIterator;
import it.ibm.red.business.service.facade.IASignFacadeSRV;
import it.ibm.red.business.service.facade.IAooFacadeSRV;
import it.ibm.red.business.service.facade.ICasellePostaliFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV;
import it.ibm.red.business.service.facade.IFirmatoSpeditoFacadeSRV;
import it.ibm.red.business.service.facade.IGestioneColonneFacadeSRV;
import it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV;
import it.ibm.red.business.service.facade.IMasterPaginatiFacadeSRV;
import it.ibm.red.business.service.facade.INodoFacadeSRV;
import it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV;
import it.ibm.red.business.service.facade.IResponseRedFacadeSRV;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.business.service.facade.IStampigliaturaSegnoGraficoFacadeSRV;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;
import it.ibm.red.business.utils.CollectionUtils;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.business.utils.XlsUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.helper.PredisponiDocumentoHelper;
import it.ibm.red.web.dto.BeanTaskDTO;
import it.ibm.red.web.dto.GoToCodaDTO;
import it.ibm.red.web.dto.OrgObjDTO;
import it.ibm.red.web.dto.QueueCountDTO;
import it.ibm.red.web.enums.DettaglioDocumentoFileEnum;
import it.ibm.red.web.enums.GoToCodaProvenienzaEnum;
import it.ibm.red.web.enums.GruppiResponseEnum;
import it.ibm.red.web.enums.NavigationTokenEnum;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.helper.faces.MessageSeverityEnum;
import it.ibm.red.web.helper.lsign.LocalSignHelper;
import it.ibm.red.web.helper.provider.BeanTaskProvider;
import it.ibm.red.web.helper.provider.BeanTaskProvider.BeanTask;
import it.ibm.red.web.mbean.beantask.PredisponiDocumentoBean;
import it.ibm.red.web.mbean.interfaces.IFascicoloProcedimentaleManagerInitalizer;
import it.ibm.red.web.mbean.interfaces.IOpenDettaglioEstesoButtonComponent;
import it.ibm.red.web.mbean.interfaces.IPopupComponent;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;
import it.ibm.red.web.utils.ExportUtils;

/**
 * @author DarioVentimiglia
 *
 */
@Named(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN)
@ViewScoped
public class ListaDocumentiBean extends AbstractBean {

	private static final String VIEWS_CODA_CODA_JSF = "views/coda/coda.jsf";

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 7471338223007987743L;

	/**
	 * Id component dettaglio documento.
	 */
	private static final String DETTAGLIODOCUMENTODD_RESOURCE_ID = "eastSectionForm:idDettaglioDocumento_DD";

	/**
	 * Id component panel firma remota.
	 */
	private static final String FIRMAREMOTA_RESOURCE_ID = "centralSectionForm:panel_FirmaRemota_id";

	/**
	 * Id component panel protocollo Emergenza.
	 */
	private static final String PNLPROTEMR_RESOURCE_ID = "centralSectionForm:idPnlProtEmr";

	/**
	 * Id component dlgGeneric.
	 */
	private static final String DLGGENERIC_RESOURCE_ID = "centralSectionForm:idDlgGeneric";

	/**
	 * Id component documenti.
	 */
	private static final String DOCUMENTI_RESOURCE_ID = "centralSectionForm:documenti";

	/**
	 * Id component dialog risultati.
	 */
	private static final String DLGSHOWRESULT_RESOURCE_ID = "centralSectionForm:idDlgShowResult";

	/**
	 * JS per la visualizzazione del componente con id: wdgProtEmergenza.
	 */
	private static final String SHOW_WDGPROTEMERGENZA_JS = "PF('wdgProtEmergenza').show()";

	/**
	 * JS per disabilitare il component con id: wdgProtEmergBtn.
	 */
	private static final String DISABLE_WDGPROTEMERGBTN_JS = "PF('wdgProtEmergBtn').disable()";

	/**
	 * JS per visualizzare il component con id: wdgDlgAvvisoDestInterni.
	 */
	private static final String SHOW_WDGDLGAVVISODESTINTERNI_JS = "PF('wdgDlgAvvisoDestInterni').show()";

	/**
	 * JS per la visualizzazione del component con id: dlgShowResult.
	 */
	private static final String SHOW_DLGSHOWRESULT_JS = "PF('dlgShowResult').show()";

	/**
	 * JS per la visualizzazione del component con id: dlgGeneric.
	 */
	private static final String SHOW_DLG_GENERIC_JS = "PF('dlgGeneric').show()";

	/**
	 * Messaggio errore: superamento dimensione.
	 */
	private static final String ERROR_MAX_DIMENSIONE_FILE_SUPERATA_MSG = "La dimensione dei file contenuti nel procedimento supera la dimensione massima consentita dalla casella di posta mittente.";

	/**
	 * Messaggio warning associato alla chisuura del documento.
	 */
	private static final String WARNING_DOCUMENTO_CHIUSO_MSG = "Attenzione, il documento in stato chiuso non è modificabile. Per proseguire selezionare “Conferma” oppure cliccare “Annulla” per interrompere l’operazione.";

	/**
	 * Messaggio warning associato al registro repertorio.
	 */
	private static final String WARNING_REGISTRO_REPERTORIO_PROTOCOLLAZIONE_EMERGENZA_MSG = "Attenzione! Il documento risulta essere un Registro di Repertorio, ed essendo attiva la Protocollazione d'Emergenza non è possibile proseguire con l'operazione";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ListaDocumentiBean.class.getName());

	/**
	 * Label.
	 */
	private static final String FILE_PRINCIPALE = "File principale: ";

	/**
	 * Indice.
	 */
	private static final String PRIMO_ATTIVO = "0";

	/**
	 * Indice.
	 */
	private static final String SECONDO_ATTIVO = "1";

	/**
	 * Dimensione coda libro firma.
	 */
	private static final String LIBRO_CODA_SIZE = "100";

	/**
	 * Dimensione coda di default.
	 */
	private static final String DEFAULT_CODA_SIZE = "30";

	/**
	 * Nome sheet.
	 */
	private static final String NOME_SHEET = "DA LAVORARE";

	/**
	 * Colonne code fnet.
	 */
	private static List<ColonneEnum> colonneCodeFilenet = Arrays.asList(ColonneEnum.values());

	/**
	 * Colonne code applicative.
	 */
	private static List<ColonneEnum> colonneCodeApp = Arrays.asList(ColonneEnum.INFO, ColonneEnum.DESCRIZIONE, ColonneEnum.PROCEDIMENTO, ColonneEnum.CREAZIONE,
			ColonneEnum.SCADENZA, ColonneEnum.MITTENTE, ColonneEnum.DESTINATARI, ColonneEnum.PROTOCOLLAZIONE, ColonneEnum.SPEDIZIONE, ColonneEnum.RISERVATO);

	/**
	 * BEan disessione.
	 */
	private SessionBean sessionBean;

	/**
	 * Informazioni utente.
	 */
	private UtenteDTO utente;

	/**
	 * Informazioni utente firmatario.
	 */
	private UtenteFirma utenteFirma;

	/**
	 * Servizio.
	 */
	private IListaDocumentiFacadeSRV listaDocumentiSRV;

	/**
	 * Servizio.
	 */
	private IMasterPaginatiFacadeSRV masterPaginatiSRV;

	/**
	 * Servizio.
	 */
	private IOperationWorkFlowFacadeSRV opSRV;

	/**
	 * Filtro.
	 */
	private String filterString;

	/**
	 * Master non filtrati.
	 */
	private Collection<MasterDocumentRedDTO> notFilterMasters;

	/**
	 * Tabella documenti.
	 */
	protected SimpleDetailDataTableHelper<MasterDocumentRedDTO> documentiDTH;

	/**
	 * Lista check.
	 */
	private List<Boolean> allElementCheckboxes;

	/**
	 * Menù code.
	 */
	private transient MenuModel modelCode;

	/**
	 * Coda attiva.
	 */
	private DocumentQueueEnum selectedQueue;

	/**
	 * Nome coda attiva.
	 */
	private String nomeCodaAttuale = "";

	/**
	 * Response multiple.
	 */
	private Collection<ResponsesRedEnum> responsesMulti;

	/**
	 * Response singole.
	 */
	private Collection<ResponsesRedEnum> responsSingle;

	/**
	 * Servizio.
	 */
	private IResponseRedFacadeSRV responseRedSRV;

	/**
	 * Response non invocabili.
	 */
	private Collection<String> responsesIssue;

	/**
	 * Response selezionata.
	 */
	private ResponsesRedEnum selectedResponse;

	/**
	 * Esiti esecuzion response.
	 */
	private Collection<EsitoOperazioneDTO> esitiOperazione;

	/**
	 * Time esito.
	 */
	private Date dataEoraEsito;

	/**
	 * Numero operazioni ok.
	 */
	private Integer countEsitiOperazioneOk;

	/**
	 * Numero operazioni ko.
	 */
	private Integer countEsitiOperazioneKo;

	/**
	 * Servizio.
	 */
	private ISignFacadeSRV signSRV;

	/**
	 * Servizio.
	 */
	private IUtenteFacadeSRV utenteSRV;

	/**
	 * Servizio.
	 */
	private IDocumentoFacadeSRV documentSRV;

	/**
	 * Servizio.
	 */
	private IGestioneColonneFacadeSRV gestioneColonneSRV;

	/**
	 * Flag stampa protocollo.
	 */
	private boolean flagStampaProtocollo;

	/**
	 * Protocollo da stampare.
	 */
	private String protocolloDaStampare;

	// acquisizione credenziali firma remota

	/**
	 * One time password.
	 */
	private String otp = "";

	/**
	 * Pin firma.
	 */
	private String pin = "";

	/**
	 * Tipo firma.
	 */
	private SignTypeEnum signType;

	/**
	 * Messaggio conferma firma remota.
	 */
	private String confirmMessageFirmaRemota = "";

	/**
	 * Dialog firma remota.
	 */
	private String confirmMessageFirmaDig = "";

	/**
	 * Flag mostra conferma firma.
	 */
	private Boolean showConfirmMessageFirmaRemota = false;

	/**
	 * Flag apri dialog firma.
	 */
	private Boolean openFirmaRemotaDialog = false;

	/**
	 * Lista tipi di firma.
	 */
	private List<String> signTypeList;

	/**
	 * Tipo firma selezionata.
	 */
	private String selectedSignType = "";

	/**
	 * Tipo di firma remota selezionata.
	 */
	private String selectedSignTypeFirmaRemota = "";

	/**
	 * Flag memorizza pin.
	 */
	private Boolean memorizzaPin = false;

	/**
	 * Flag errore OTP.
	 */
	private Boolean erroreOTP = false;

	/**
	 * Flag errore PIN.
	 */
	private Boolean errorePIN = false;

	/**
	 * Flag pin bloccato.
	 */
	private Boolean pinBloccato = false;

	/**
	 * Flag conferma firma.
	 */
	private boolean flagConfermaFirma;

	/**
	 * Messaggio firma autografa.
	 */
	private String firmaAutografaConfirmMessage;

	/**
	 * Menù response.
	 */
	private DefaultMenuModel menuContextResponse;

	/**
	 * Numero documento corrente.
	 */
	private String numDocCurrent;

	/**
	 * Master documento selezionato.
	 */
	private MasterDocumentRedDTO docSelectedForResponse;

	/**
	 * Dettaglio documento da caricare.
	 */
	private DettaglioDocumentoFileEnum dettaglioDaCaricare;

	/**
	 * Dettaglio docuento da caricare (coda corrente).
	 */
	private DettaglioDocumentoFileEnum dettaglioDaCaricareCodaCorrente;

	/**
	 * Componente conferma firma remota.
	 */
	private IPopupComponent iFirmaRemotaConfirm;

	/**
	 * Componente pre-firma remota.
	 */
	private IPopupComponent iFirmaRemotaPresign;

	/**
	 * Componente firma locale.
	 */
	private IPopupComponent iFirmaDigitale;

	/**
	 * Modello menù.
	 */
	private transient MenuModel model;

	/**
	 * WOB.
	 */
	private String modelWOB;

	/**
	 * Master selezionato.
	 */
	private MasterDocumentRedDTO selectedMaster;

	/**
	 * Paginatore master.
	 */
	private MasterPageIterator masterPageIterator;

	/**
	 * Percentuale del paginatore caricata.
	 */
	private Integer percPaginator;

	/**
	 * Messaggio esito positivo.
	 */
	private String messaggioEsitoPositivo;

	/**
	 * Flag mostra esito documenti firmati.
	 */
	private boolean mostraDocumentiFirmatiEsito;

	/**
	 * Indice pagina.
	 */
	private int pageIndex;

	/**
	 * Oggetto organigramma.
	 */
	private OrgObjDTO orgObj;

	/**
	 * Sorgente coda (Applicativa/FNet).
	 */
	private SourceTypeEnum columnSourceType;

	/**
	 * Dettaglio documento bean.
	 */
	private DettaglioDocumentoBean ddBean;

	/**
	 * Flag firma multipla.
	 */
	private boolean firmaMultipla;

	/**
	 * Flag dialog pre-response.
	 */
	private boolean dlgBeforeResponse;

//	<-- Dati Protocollo Emergenza -->
	/**
	 * Numero protocollo emergenza.
	 */
	private Integer numeroProtocolloEmergenza;

	/**
	 * Anno protocollo emergenza.
	 */
	private Integer annoProtocolloEmergenza;

	/**
	 * Protocollo emergenza.
	 */
	private ProtocolloEmergenzaDocDTO protEm;

	/**
	 * Servizio.
	 */
	private IFirmatoSpeditoFacadeSRV firmatoSpeditoSRV;

	/**
	 * Servizio.
	 */
	private ICodeDocumentiSRV codeApplicativeSRV;

	/**
	 * Lista response no recall.
	 */
	private List<ResponsesRedEnum> responseNoChangeRecallList;

	/**
	 * Flag visualizza repsonse.
	 */
	private boolean renderResponse;

	/**
	 * Dimensione coda.
	 */
	private String configSizeCoda = DEFAULT_CODA_SIZE;

	/**
	 * Dimensione coda.
	 */
	private String templateMaxCoda = DEFAULT_CODA_SIZE;

	/**
	 * Flag firma multipla.
	 */
	private boolean hasLibroFirmaMultipla;

	/**
	 * Flag messaggio conferma firma approvazioni.
	 */
	private boolean showConfirmMessageFirmaApprovazioni;

	/**
	 * Messaggio conferma firma approvazioni.
	 */
	private String confirmMessageFirmaApprovazioni;

	/**
	 * Flag carica tutti elementi.
	 */
	private boolean caricaTuttiGliElementi;

	/**
	 * Lista stampa etichette.
	 */
	private List<String> descrRegStampaEtichetteList;

	/**
	 * Ultimo master.
	 */
	private MasterDocumentRedDTO lastMaster;

	/**
	 * Flag visualizza tutti i visti.
	 */
	private boolean showSelezionaTuttiVisto;

	/**
	 * Servizio.
	 */
	private IStampigliaturaSegnoGraficoFacadeSRV stampigliaturaSiglaSRV;
	
	/**
	 * Service gestione firma asincrona.
	 */
	private IASignFacadeSRV aSignSRV;

	/**
	 * Servizio.
	 */
	private IDocumentoRedFacadeSRV documentoRedSRV;

	/**
	 * Servizio.
	 */
	private ICasellePostaliFacadeSRV casellePostaliSRV;

	/**
	 * Lista master.
	 */
	private List<MasterDocumentRedDTO> docsToSign;

	/**
	 * Lista colonne.
	 */
	private List<ColonneCodeDTO> listaColonneCodeDTO;

	/**
	 * Servizio.
	 */
	private IDocumentoSRV documentoSRV;

	/**
	 * mappa di glifi con content da apporre in fase di firma per libro firma delegato.
	 */ 
	private Map<String,byte[]> tipologieGlifoFirma;
	  
	/**
	 * glifo selezionato per la firma remota
	 */
	private byte[] glifoSelezionatoRemota;
	
	/**
	 * glifo selezionato per la firma digitale.
	 */
	private byte[] glifoSelezionatoDigitale;
	
	/**
	 * Collezione wobs.
	 */
	private Collection<String> wobNumbersContinuaAftLock;

	/**
	 * Flag pades.
	 */
	private boolean principaleOnlyPAdESVisibleAftLock;

	/**
	 * Nome utente lock.
	 */
	private String nomeUtenteLockatore;
	
	/**
	 * Mappa id nodo descrizione.
	 */
	private HashMap<Integer, String> mapIdNodoDescrizione;
	
	/**
	 * Data Attivazione della funzionalita giro visti per ogni ambiente.
	 */
	private Date dataAttivazioneGiroVistiDelegato;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		listaDocumentiSRV = ApplicationContextProvider.getApplicationContext().getBean(IListaDocumentiFacadeSRV.class);
		masterPaginatiSRV = ApplicationContextProvider.getApplicationContext().getBean(IMasterPaginatiFacadeSRV.class);
		responseRedSRV = ApplicationContextProvider.getApplicationContext().getBean(IResponseRedFacadeSRV.class);
		opSRV = ApplicationContextProvider.getApplicationContext().getBean(IOperationWorkFlowFacadeSRV.class);
		signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignFacadeSRV.class);
		firmatoSpeditoSRV = ApplicationContextProvider.getApplicationContext().getBean(IFirmatoSpeditoFacadeSRV.class);
		documentSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);
		codeApplicativeSRV = ApplicationContextProvider.getApplicationContext().getBean(ICodeDocumentiSRV.class);
		IRegistroRepertorioSRV registroRepertorioSRV = ApplicationContextProvider.getApplicationContext().getBean(IRegistroRepertorioSRV.class);
		stampigliaturaSiglaSRV = ApplicationContextProvider.getApplicationContext().getBean(IStampigliaturaSegnoGraficoFacadeSRV.class);
		aSignSRV = ApplicationContextProvider.getApplicationContext().getBean(IASignFacadeSRV.class);
		ddBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN);
		documentoRedSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoRedFacadeSRV.class);
		casellePostaliSRV = ApplicationContextProvider.getApplicationContext().getBean(ICasellePostaliFacadeSRV.class);
		gestioneColonneSRV = ApplicationContextProvider.getApplicationContext().getBean(IGestioneColonneFacadeSRV.class);
		ddBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN);
		documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoSRV.class);
		INodoFacadeSRV nodoSRV = ApplicationContextProvider.getApplicationContext().getBean(INodoFacadeSRV.class);
		utenteSRV = ApplicationContextProvider.getApplicationContext().getBean(IUtenteFacadeSRV.class);
		protEm = new ProtocolloEmergenzaDocDTO();

		iFirmaRemotaConfirm = new IPopupComponent() {

			/**
			 * La costante serialVersionUID.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void conferma() {
				confirmDoFirmaRemota();
			}

			@Override
			public void annulla() {
				// Metodo intenzionalmente vuoto.
			}
		};

		iFirmaRemotaPresign = new IPopupComponent() {

			/**
			 * La costante serialVersionUID.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void conferma() {
				showConfirmMessageFirmaRemota = false;
				checkDocumentsPreSign();
			}

			@Override
			public void annulla() {
				hideDialogFirmaRemota();
				FacesHelper.update(FIRMAREMOTA_RESOURCE_ID);
			}
		};

		docsToSign = new ArrayList<>();
		iFirmaDigitale = new IPopupComponent() {

			/**
			 * La costante serialVersionUID.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void conferma() {
				controlloPreFirmaDigitale();
			}

			@Override
			public void annulla() {
				// Metodo intenzionalmente vuoto.
			}
		};

		modelCode = new DefaultMenuModel();

		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();

		documentiDTH = new SimpleDetailDataTableHelper<>();
		filterString = Constants.EMPTY_STRING;

		allElementCheckboxes = new ArrayList<>();
		allElementCheckboxes.add(Boolean.FALSE);

		if (!CollectionUtils.isEmptyOrNull(sessionBean.getListaColonneCodeDTO())) {
			listaColonneCodeDTO = sessionBean.getListaColonneCodeDTO();
		} else {
			List<Boolean> visibilitaDefColonneCode = Arrays.asList(true, true, true, true, true, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false,false);
			
			listaColonneCodeDTO = new ArrayList<>();
			for (int i = 0; i < visibilitaDefColonneCode.size(); i++) {
				final ColonneCodeDTO col = new ColonneCodeDTO(colonneCodeFilenet.get(i), visibilitaDefColonneCode.get(i));
				col.setRenderColonna(true);
				listaColonneCodeDTO.add(col);
			}
		}

		if (utente.getStampaEtichetteResponse()) {
			descrRegStampaEtichetteList = new ArrayList<>();
			descrRegStampaEtichetteList = registroRepertorioSRV.getDescrForShowEtichetteByAoo(utente.getIdAoo());
		}

		mapIdNodoDescrizione = nodoSRV.getDescrizioneByIdNodo(utente.getIdAoo().intValue());
		// ### Primo caricamento	
		refresh(false);

		caricaTuttiGliElementi = false;
//		<-- Gestione Esiti Firma Digitale -->
//		gestione eventuali esiti dopo l'esecuzione della firma digitale
		final String[] finalStep = FacesHelper.getRequest().getParameterValues(ConstantsWeb.SessionObject.FINAL_STEP);
		if (finalStep != null) {
			final LocalSignHelper lsh = (LocalSignHelper) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.SESSION_LSH, false);

			if (Boolean.TRUE.equals(Boolean.valueOf(finalStep[0])) && !lsh.getEsiti().isEmpty()) {

				cleanEsiti();
				mostraDocumentiFirmatiEsito = true;
				for (final EsitoOperazioneDTO ePrincipale : lsh.getEsiti()) {
					if (!lsh.isCopiaConforme()) {
						esitiOperazione.add(ePrincipale);
					}
					if (ePrincipale.getEsitiAllegati() != null && !ePrincipale.getEsitiAllegati().isEmpty()) {
						for (final EsitoOperazioneDTO eAllegato : ePrincipale.getEsitiAllegati()) {
							eAllegato.setAllegato(true);
							eAllegato.setIdDocumento(ePrincipale.getIdDocumento());
							esitiOperazione.add(eAllegato);

						}
					}
				}

				// richiamo la visualizzazione della dialog
				FacesHelper.executeJS(SHOW_DLGSHOWRESULT_JS);
				// aggiorno la dialog contenente gli esiti
				FacesHelper.update(DLGSHOWRESULT_RESOURCE_ID);

			} else {
				showError("Errore bloccante durante il processo di firma. Se persiste contattare l'assistenza.");
			}
		}
		
		responseNoChangeRecallList = Arrays.asList(
				ResponsesRedEnum.FALDONA,
				ResponsesRedEnum.INOLTRA_DA_CODA_ATTIVA,
				ResponsesRedEnum.INSERISCI_NOTA,
				ResponsesRedEnum.NO_TRACCIA_PROCEDIMENTO,
				ResponsesRedEnum.RECALL,
				ResponsesRedEnum.STAMPA_PROTOCOLLO,
				ResponsesRedEnum.TRACCIA_PROCEDIMENTO,
				ResponsesRedEnum.RIATTIVA_PROCEDIMENTO,
				ResponsesRedEnum.RETRY
		);
		
		
		hasLibroFirmaMultipla =  responseRedSRV.hasLibroFirmaMultipla(utente);
		
		calcolaCaricati();

		showSelezionaTuttiVisto = utente.isShowSelezionaTuttiVisto();
		sessionBean.setRenderDialogStampa(false);
		
		tipologieGlifoFirma = null;
		if(NavigationTokenEnum.LIBRO_FIRMA_DELEGATO.equals(sessionBean.getActivePageInfo())) {
			tipologieGlifoFirma = new HashMap<>();
		}

		try {
			DateFormat format = new SimpleDateFormat("dd-MM-yy");
			setDataAttivazioneGiroVistiDelegato(format.parse(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DATA_ATTIVAZIONE_GIRO_VISTI_DELEGATO)));
		} catch (Exception e) {
			LOGGER.warn("Errore in fase di parsing della data attivazione giro visti delegato", e);
			this.setDataAttivazioneGiroVistiDelegato(null);
		}
		
		

	}

	/**
	 * @return percPaginator
	 */
	public Integer getPercPaginator() {
		return percPaginator;
	}

	/**
	 * Metodo per recuperare il valore corretto da esportare per la colonna
	 * 'descrizione'.
	 * 
	 * @param column
	 * @return
	 */
	public String columnDescrizione(final UIColumn column) {
		return ExportUtils.columnCorrectValue(column);
	}

	/**
	 * Listener per la paginazione dei documenti nelle code.
	 * 
	 * @param event
	 */
	public final void pageListener(final PageEvent event) {
		final DocumentQueueEnum queue = sessionBean.getActivePageInfo().getDocumentQueue();
		pageIndex = event.getPage();

		if ((queue != null) && (SourceTypeEnum.FILENET.equals(queue.getType())) || (NavigationTokenEnum.ORG_SCRIVANIA.equals(sessionBean.getActivePageInfo()))) {
			// Siamo in presenza di una coda FileNet
			final DataTable datatable = (DataTable) getClickedComponent(event);
			if (pageIndex + 1 >= datatable.getPageCount()) {
				// È stata richiesta una nuova pagina
				getNewPage();
			}
		}
		if (pageIndex >= allElementCheckboxes.size()) {
			for (int index = allElementCheckboxes.size(); index < (pageIndex + 1); index++) {
				allElementCheckboxes.add(Boolean.FALSE);
			}
		}
	}

	private Boolean getNewPage() {
		Boolean output = false;

		// La pagina richiesta potrebbe richiedere il recupero di nuovi dati
		if (Boolean.TRUE.equals(masterPageIterator.getMorePages())) {
			// Non abbiamo raggiunto la fine dell'iteratore
			Collection<MasterDocumentRedDTO> tmp = null;
			if (sessionBean.getActivePageInfo().equals(NavigationTokenEnum.ORG_SCRIVANIA)) {
				orgObj = (OrgObjDTO) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.ORG_OBJ, false);
				if (orgObj != null && orgObj.getDatiAccesso() != null) {
					tmp = masterPaginatiSRV.refineMastersOrganigramma(utente, orgObj.getDatiAccesso(), masterPageIterator);
				}
			} else {
				tmp = masterPaginatiSRV.refineMasters(utente, masterPageIterator);
			}
			if (tmp != null && !tmp.isEmpty()) {
				//Siccome nel post construct valorizzo la lista di tutti i nodi per l'aoo metto questa get nel new page
				//in quanto per le query paginate filent se richiedo la seconda pag ad es devo settare anche la descr
				if(mapIdNodoDescrizione!=null) {
					for(MasterDocumentRedDTO forUfficio : tmp) {
						forUfficio.setDescUffCreatore(mapIdNodoDescrizione.get(forUfficio.getUffCreatore()));
					}
				}
				notFilterMasters.addAll(tmp);
				documentiDTH.getMasters().addAll(tmp);
				output = true;
			}
		}
		return output;
	}

	/**
	 * {@link #refresh(boolean)}.
	 */
	public final void refresh() {
		refresh(true);
	}

	/**
	 * Esegue operazioni di aggiornamento post esecuzione di una response.
	 */
	public final void postExecute() {

		final List<MasterDocumentRedDTO> documentiSelezionati = getDocumentiSelezionati();

		// Non tutte le code saranno gestite con la post-esecuzione, quello che non sarà
		// gestito continuerà ad eseguire un semplice refresh della coda.
		boolean bCodaGestita = DocumentQueueEnum.isOneKindOfCorriere(sessionBean.getActivePageInfo().getDocumentQueue())
				|| DocumentQueueEnum.isOneKindOfDaLavorare(sessionBean.getActivePageInfo().getDocumentQueue());
		if (sessionBean.getActivePageInfo().equals(NavigationTokenEnum.ORG_SCRIVANIA)) {
			final OrgObjDTO queueOrg = (OrgObjDTO) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.ORG_OBJ, false);
			bCodaGestita = DocumentQueueEnum.isOneKindOfCorriere(queueOrg.getQueueToGo());
		}
		if (selectedResponse != null) {
			if (utente.getFlagForzaRefreshCode() == 1 || PostExecEnum.RICARICA_CODA.equals(selectedResponse.getActionPostExec()) || !bCodaGestita) {
				// Ricarichiamo la coda.
				refresh(true);
				FacesHelper.executeJS("clearDatatableFilters('documentiDT', true);");
			} else {
				if (PostExecEnum.AGGIORNA_ITEM.equals(selectedResponse.getActionPostExec())) {
					updateMastersAfterResponse(sessionBean.getActivePageInfo(), orgObj, utente, sessionBean.getActivePageInfo().getDocumentQueue(), documentiSelezionati);
				} else if (PostExecEnum.DISABILITA_ITEM.equals(selectedResponse.getActionPostExec())) {
					for (final MasterDocumentRedDTO master : documentiSelezionati) {
						master.setFlagDisabilitato(true);
					}
					FacesHelper.executeJS("unselectDatatable('documentiDT', 'eastSection');");
					documentiDTH.setCurrentMaster(null);
					documentiDTH.setCurrentDetail(null);
					documentiDTH.setRenderDetail(false);
				}

				// Eliminiamo i selezionati
				documentiDTH.setSelectedMasters(new ArrayList<>());

				// Gestiamo le response singole e multiple
				manageTabMultiActions();
				responsesMulti = new ArrayList<>();
			}

			/*
			 * Se indicato nell'enum delle response, aggiorniamo i contatori del menu
			 * laterale
			 */
			if ((sessionBean.getActivePageInfo().equals(NavigationTokenEnum.IN_SOSPESO) || sessionBean.getActivePageInfo().equals(NavigationTokenEnum.STORNATI)
					|| sessionBean.getActivePageInfo().equals(NavigationTokenEnum.LIBRO_FIRMA_DELEGATO)
					|| sessionBean.getActivePageInfo().equals(NavigationTokenEnum.PROCEDIMENTI_ATTIVI) || sessionBean.getActivePageInfo().equals(NavigationTokenEnum.ATTI)
					|| sessionBean.getActivePageInfo().equals(NavigationTokenEnum.RISPOSTA) || sessionBean.getActivePageInfo().equals(NavigationTokenEnum.MOZIONE)
					|| sessionBean.getActivePageInfo().equals(NavigationTokenEnum.IN_ACQUISIZIONE) || NavigationTokenEnum.isOneKindOfCorriere(sessionBean.getActivePageInfo())
					|| NavigationTokenEnum.isOneKindOfDaLavorareInLavorazione(sessionBean.getActivePageInfo())
					|| sessionBean.getActivePageInfo().equals(NavigationTokenEnum.ASSEGNATE) || sessionBean.getActivePageInfo().equals(NavigationTokenEnum.CHIUSE)
					|| sessionBean.getActivePageInfo().equals(NavigationTokenEnum.IN_SPEDIZIONE) || sessionBean.getActivePageInfo().equals(NavigationTokenEnum.GIRO_VISTI))
					&& selectedResponse.isUpdateCounters()) {
				// per le attività
				sessionBean.countMenuAttivita(false);
			} else if (sessionBean.getActivePageInfo().equals(NavigationTokenEnum.ORG_SCRIVANIA) && selectedResponse.isUpdateCounters()) {
				// per l'organigramma
				sessionBean.loadOrgUtente(false);
			} else if ((sessionBean.getActivePageInfo().equals(NavigationTokenEnum.FATTURE_DA_LAVORARE)
					|| sessionBean.getActivePageInfo().equals(NavigationTokenEnum.FATTURE_IN_LAVORAZIONE)
					|| sessionBean.getActivePageInfo().equals(NavigationTokenEnum.DD_DA_FIRMARE) || sessionBean.getActivePageInfo().equals(NavigationTokenEnum.DD_DA_LAVORARE)
					|| sessionBean.getActivePageInfo().equals(NavigationTokenEnum.DD_FIRMATI) || sessionBean.getActivePageInfo().equals(NavigationTokenEnum.DD_IN_FIRMA)
					|| sessionBean.getActivePageInfo().equals(NavigationTokenEnum.DSR)) && selectedResponse.isUpdateCounters()) {
				// per fepa
				sessionBean.countMenuFepa(false);
			} else {
				// per le mail
				sessionBean.createSubMenuMail();
			}

			for (final MasterDocumentRedDTO master : documentiSelezionati) {
				master.setSelected(false);
			}

		} else {
			sessionBean.countMenuAttivita(false);
		}

		selectedResponse = null;
	}

	/**
	 * Aggiorna la lista documenti e, opzionalmente, il menù laterale sinistro.
	 * 
	 * @param callRefreshLeftMenu
	 */
	public final void refresh(final boolean callRefreshLeftMenu) {
		DocumentQueueEnum queue = sessionBean.getActivePageInfo().getDocumentQueue();
		pageIndex = 0;
		masterPageIterator = null;
		notFilterMasters = new ArrayList<>();
		documentiDTH.setMasters(new ArrayList<>());
		Collection<String> outerMastersDocumentTitle = null;
		mostraDocumentiFirmatiEsito = false;

		// Si recuperano il/i master(s) eventualmente inserito/i in sessione da una
		// pagina esterna alle code (e.g. ricerca)
		final GoToCodaDTO goToCodaInfo = (GoToCodaDTO) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.FILTER_MASTER_GO_TO_CODA, true);

		if (goToCodaInfo != null) {
			List<MasterDocumentRedDTO> outerMasters = goToCodaInfo.getMasters();

			outerMastersDocumentTitle = new ArrayList<>();
			for (final MasterDocumentRedDTO master : outerMasters) {
				outerMastersDocumentTitle.add(master.getDocumentTitle());
			}
		}

		// Gestione hyperlink Notifica --> LibroFirma
		String mailLink = FacesHelper.getParamQueryFromRequest(Constants.RequestParameter.MAIL_LINK);
		if (mailLink != null && mailLink.trim().length() > 0) {
			
			// Recupero parametri dal link
			Map<String, String> params = recoveryMailLinkParam(mailLink);
			String documentTitle = params.get(Constants.RequestParameter.MailLink.MAIL_LINK_DOCUMENT_TITLE_PARAM_NAME);
			Long idNodo = Long.parseLong(params.get(Constants.RequestParameter.MailLink.MAIL_LINK_ID_NODO_NAME));
			Long idRuolo = Long.parseLong(params.get(Constants.RequestParameter.MailLink.MAIL_LINK_ID_RUOLO_PARAM_NAME));
			String username = params.get(Constants.RequestParameter.MailLink.MAIL_LINK_USERNAME_PARAM_NAME);
			
			// Verifica Utente che ha richiesto il documento
			UtenteDTO utenteToCheck = utenteSRV.getByUsername(username, idRuolo, idNodo);
			if (utenteToCheck == null || !utenteToCheck.getUsername().equals(utente.getUsername())) {
				LOGGER.error("Utente non riconosciuto per la visualizzazione del documento della mail.");
				showError("Utente non riconosciuto per la visualizzazione del documento della mail.");
			} else if (!utenteToCheck.getIdRuolo().equals(utente.getIdRuolo()) || !utenteToCheck.getIdUfficio().equals(utente.getIdUfficio())) {
				
				// Cambio ufficio silente se l'ufficio o il ruolo non rispecchiano quelli dell'utente già in sessione
				sessionBean.cambiaUfficio(idNodo, idRuolo);
				
			}
			
			// Colleziono il dt richiesto
			outerMastersDocumentTitle = new ArrayList<>();
			outerMastersDocumentTitle.add(documentTitle);
			
			// Imposto il LibroFirma come pagina richiesta
			sessionBean.gotoLibroFirma();
			queue = sessionBean.getActivePageInfo().getDocumentQueue();
		}
		
		
		if (sessionBean.getActivePageInfo().equals(NavigationTokenEnum.ORG_SCRIVANIA)) {
			// Non siamo in presenza di code reali
			orgObj = (OrgObjDTO) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.ORG_OBJ, false);

			// INIZIO PAGINAZIONE
			if (orgObj.getQueueToGo() == null) {

				// Il nodo utente è composto da i documenti della coda "DA_LAVORARE" e da
				// "SOSPESO" (anche libro firma in caso di utente in sessione della gestione
				// applicativa)
				// Come SourceType viene impostato direttamente quello 'Filenet'
				columnSourceType = SourceTypeEnum.FILENET;
				// Caso organigramma: paginato
				// Paginazione per utente (2 code)
				if (utente.isGestioneApplicativa()) {
					if (utente.isUcb()) {
						masterPageIterator = masterPaginatiSRV.getMastersRawUtenteOrganigramma(utente, orgObj.getDatiAccesso(), DocumentQueueEnum.DA_LAVORARE_UCB,
								DocumentQueueEnum.IN_LAVORAZIONE_UCB, DocumentQueueEnum.SOSPESO, DocumentQueueEnum.NSD);
					} else {
						masterPageIterator = masterPaginatiSRV.getMastersRawUtenteOrganigramma(utente, orgObj.getDatiAccesso(), DocumentQueueEnum.DA_LAVORARE,
								DocumentQueueEnum.SOSPESO, DocumentQueueEnum.NSD);
					}
				} else {
					if (utente.isUcb()) {
						masterPageIterator = masterPaginatiSRV.getMastersRawUtenteOrganigramma(utente, orgObj.getDatiAccesso(), DocumentQueueEnum.DA_LAVORARE_UCB,
								DocumentQueueEnum.IN_LAVORAZIONE_UCB, DocumentQueueEnum.SOSPESO);
					} else {
						masterPageIterator = masterPaginatiSRV.getMastersRawUtenteOrganigramma(utente, orgObj.getDatiAccesso(), DocumentQueueEnum.DA_LAVORARE,
								DocumentQueueEnum.SOSPESO);
					}
				}
				if (caricaTuttiGliElementi) {
					notFilterMasters = masterPaginatiSRV.getFilteredMasters("", masterPageIterator, utente);
				} else {
					getNewPage();
				}
			} else {
				if (SourceTypeEnum.FILENET.equals(orgObj.getQueueToGo().getType())) {
					columnSourceType = orgObj.getQueueToGo().getType();
					// Caso organigramma: paginato
					masterPageIterator = masterPaginatiSRV.getMastersRawUtenteOrganigramma(utente, orgObj.getDatiAccesso(), orgObj.getQueueToGo());
					if (caricaTuttiGliElementi) {
						notFilterMasters = masterPaginatiSRV.getFilteredMasters("", masterPageIterator, utente);
					} else {
						if (caricaTuttiGliElementi) {
							notFilterMasters = masterPaginatiSRV.getFilteredMasters("", masterPageIterator, utente);
						} else {
							getNewPage();
						}
					}
				} else {
					columnSourceType = orgObj.getQueueToGo().getType();
					notFilterMasters = listaDocumentiSRV.getMasterFromScrivania(orgObj.getQueueToGo(), orgObj.getDatiAccesso(), utente.getFcDTO());
				}
			}
			// FINE PAGINAZIONE

			if (orgObj.getQueueToGo() != null) {
				nomeCodaAttuale = orgObj.getDatiAccesso().getDescrizioneNodo() + ": " + orgObj.getQueueToGo().getDisplayName();

				final boolean isOperativa = orgObj.getQueueToGo().isCodaOperativa();
				final boolean isQueueFilenet = orgObj.getQueueToGo().getType().equals(SourceTypeEnum.FILENET);

				// disabilito le azioni per le code non operative
				if (!CollectionUtils.isEmptyOrNull(sessionBean.getListaColonneCodeDTO())) {
					listaColonneCodeDTO = sessionBean.getListaColonneCodeDTO();
				} else {
					List<Boolean> visibilitaDefColonneCode;
					if (utente.isUcb()) {
						visibilitaDefColonneCode = Arrays.asList(isOperativa, isOperativa, isQueueFilenet, true, true, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false);
					} else {
						visibilitaDefColonneCode = Arrays.asList(isOperativa, isOperativa, isQueueFilenet, true, true, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false,true);
					}
					listaColonneCodeDTO = new ArrayList<>();
					for (int i = 0; i < visibilitaDefColonneCode.size(); i++) {
						final ColonneCodeDTO col = new ColonneCodeDTO(colonneCodeFilenet.get(i), visibilitaDefColonneCode.get(i));
						col.setRenderColonna(true);
						listaColonneCodeDTO.add(col);
					}
				}

			} else {
				nomeCodaAttuale = orgObj.getDatiAccesso().getDescrizioneNodo() + ": " + orgObj.getDatiAccesso().getCognomeUtente() + " "
						+ orgObj.getDatiAccesso().getNomeUtente();
			}

			if (callRefreshLeftMenu) {
				sessionBean.loadOrgUtente(false);
				FacesHelper.update("westSectionForm:idTabMain_LeftAside:idAccPanOrgUtente_VM:idOrgUtente");
			}
		} else if (queue != null) {
			columnSourceType = queue.getType();
			// Dobbiamo gestire una coda reale
			if (SourceTypeEnum.FILENET.equals(queue.getType())) {
				// Coda filenet: paginata
				masterPageIterator = masterPaginatiSRV.getMastersRaw(queue, outerMastersDocumentTitle, utente);
				if (caricaTuttiGliElementi) {
					notFilterMasters = masterPaginatiSRV.getFilteredMasters("", masterPageIterator, utente);
				} else {
					getNewPage();
				}
				nomeCodaAttuale = queue.getDisplayName();

				configSizeCoda = LIBRO_CODA_SIZE;
				templateMaxCoda = LIBRO_CODA_SIZE;

			} else {
				// Coda applicativa: non paginata
				notFilterMasters = listaDocumentiSRV.getDocumentForMaster(queue, outerMastersDocumentTitle, utente);
				nomeCodaAttuale = queue.getDisplayName();

				final List<Boolean> visibilitaDefColonneCode = Arrays.asList(queue.isCodaOperativa(), queue.isCodaOperativa(), false, true, true, false, true, false, false,
						false, false, false, false);
				listaColonneCodeDTO = new ArrayList<>();

				for (final ColonneEnum colEnum : colonneCodeFilenet) {
					final ColonneCodeDTO col = new ColonneCodeDTO(colEnum, false);
					col.setRenderColonna(false);
					if (colonneCodeApp.contains(colEnum)) {
						col.setRenderColonna(true);
					}
					listaColonneCodeDTO.add(col);
				}

				for (int i = 0; i < visibilitaDefColonneCode.size(); i++) {
					listaColonneCodeDTO.get(i).setVisibilitaColonna(visibilitaDefColonneCode.get(i));
				}

				if (sessionBean.getListaColonneCodeDTO() != null) {
					for (int i = 0; i < sessionBean.getListaColonneCodeDTO().size(); i++) {
						if (listaColonneCodeDTO.get(i).isRenderColonna()) {
							listaColonneCodeDTO.set(i, sessionBean.getListaColonneCodeDTO().get(i));
						}
					}

					// Colonne statiche delle code
					listaColonneCodeDTO.get(0).setVisibilitaColonna(queue.isCodaOperativa());
					listaColonneCodeDTO.get(1).setVisibilitaColonna(queue.isCodaOperativa());
					listaColonneCodeDTO.get(2).setVisibilitaColonna(false);

				}
			}

			if (callRefreshLeftMenu) {
				sessionBean.countMenuAttivita(false);

				if ((sessionBean.getFattElettEnable() || sessionBean.getPreferenze().isGroupFatElettronica())
						&& sessionBean.haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.MENUFEPA)) {
					sessionBean.countMenuFepa(false);
					FacesHelper.update("westSectionForm:idTabMain_LeftAside:idMenuLeftAside:idMenuFepa");
				}
				FacesHelper.update("westSectionForm:idTabMain_LeftAside:idMenuLeftAside:idMenuAttivita");
			}

		}

		settaggioDettaglioDocumento();

		final Collection<MasterDocumentRedDTO> masterDocSelected = documentiDTH.getSelectedMasters();
		if (!org.apache.commons.collections.CollectionUtils.isEmpty(masterDocSelected) && documentiDTH.getFilteredMasters() != null) {
			for (final MasterDocumentRedDTO docSelected : masterDocSelected) {
				boolean remove = true;
				// Se notFilterMasters è null sicuramente devo rimuovere dai filtered
				if (notFilterMasters != null) {
					for (final MasterDocumentRedDTO docMaster : notFilterMasters) {
						if (docSelected.getDocumentTitle().equals(docMaster.getDocumentTitle()) && docSelected.getWobNumber().equals(docMaster.getWobNumber())) {
							// il documento sta ancora nella coda quindi può rimanere tra i filtrati.
							remove = false;
							break;
						}
					}
				}
				if (remove) {
					documentiDTH.getFilteredMasters().remove(docSelected);
				}
			}
		}

		// Gestione personalizzata delle colonne in base all'eventuale provenienza da
		// pagine esterne a una coda:
		if (goToCodaInfo != null && GoToCodaProvenienzaEnum.HOME_PAGE_DOC_IN_SCADENZA.equals(goToCodaInfo.getProvenienza())) {
			// Se si proviene dalla lista di documenti in scadenza mostrata nella home page:
			// - si visualizza anche la colonna relativa alla data scadenza
			listaColonneCodeDTO.get(7).setVisibilitaColonna(Boolean.TRUE);

			// - si ordinano i master per data scadenza in ordine decrescente
			Collections.sort((List<MasterDocumentRedDTO>) notFilterMasters, (final MasterDocumentRedDTO master1, final MasterDocumentRedDTO master2) -> {
				int output = 0;

				if (master1.getDataScadenza() != null || master2.getDataScadenza() != null) {
					if (master1.getDataScadenza() == null) {
						output = 1;
					} else if (master2.getDataScadenza() == null) {
						output = -1;
					} else {
						output = master2.getDataScadenza().compareTo(master1.getDataScadenza());
					}
				}

				return output;
			});
		}

		documentiDTH.setSelectedMasters(new ArrayList<>());
		documentiDTH.setMasters(notFilterMasters);

		manageTabMultiActions();
		clearResponse();

		documentiDTH.selectFirst(true);
		if (documentiDTH.getCurrentMaster() != null) {
			documentiDTH.getCurrentMaster().setFlagSelezione(false);
			setLastMaster(documentiDTH.getCurrentMaster());
		}

		FacesHelper.update("centralSectionForm:idInfoPanel");
		FacesHelper.update("centralSectionForm:searchAllBtn");

		if (documentiDTH.getCurrentMaster() != null) {
			filterString = Constants.EMPTY_STRING;
			selectDetail(documentiDTH.getCurrentMaster());
		} else {
			// Pulisco il dettaglio da eventuali dati rimasti in memoria
			clearDetail();
		}

		for (final MasterDocumentRedDTO masterLibroFirma : notFilterMasters) {
			if (!StringUtils.isNullOrEmpty(masterLibroFirma.getPlaceholderSigla())) {
				final HashMap<String, String> iconaMap = stampigliaturaSiglaSRV.getIconByPlaceholder(masterLibroFirma.getPlaceholderSigla(),
						masterLibroFirma.getDocumentTitle());
				masterLibroFirma.setIconaStampigliaturaSegnoGrafico(iconaMap);
				masterLibroFirma.setIconaKeySet(iconaMap.keySet());
			}
			
			if(mapIdNodoDescrizione!=null && masterLibroFirma.getUffCreatore()!=null){
				masterLibroFirma.setDescUffCreatore(mapIdNodoDescrizione.get(masterLibroFirma.getUffCreatore()));
			}
		}

		// Si annullano i contatori degli esiti
		countEsitiOperazioneOk = null;
		countEsitiOperazioneKo = null;
		messaggioEsitoPositivo = Constants.EMPTY_STRING;
		cleanEsiti();
	}

	/**
	 * Restituisce il DTO per l'organigramma.
	 * 
	 * @return orgObj
	 */
	public OrgObjDTO getOrgObj() {
		return orgObj;
	}

	/**
	 * Restituisce il MenuModel delle response.
	 * 
	 * @return model
	 */
	public MenuModel getModel() {
		if (documentiDTH.getCurrentMaster() != null && (documentiDTH.getCurrentMaster().getDocumentTitle() != null || documentiDTH.getCurrentMaster().getWobNumber() != null)
				&& responsSingle != null) {
			if ((documentiDTH.getCurrentMaster().getDocumentTitle() != null && !documentiDTH.getCurrentMaster().getDocumentTitle().equalsIgnoreCase(modelWOB))
					|| (documentiDTH.getCurrentMaster().getWobNumber() != null && !documentiDTH.getCurrentMaster().getWobNumber().equalsIgnoreCase(modelWOB))) {
				modelWOB = documentiDTH.getCurrentMaster().getWobNumber();
				model = new DefaultMenuModel();
				final DefaultMenuItem title = new DefaultMenuItem("Azioni Documento: " + documentiDTH.getCurrentMaster().getNumeroDocumento());
				title.setDisabled(true);
				title.setStyle("opacity:1;font-weight:bold;");
				model.addElement(title);

				for (final ResponsesRedEnum resp : responsSingle) {
					final DefaultMenuItem menuItem = new DefaultMenuItem(resp.getDisplayName());
					menuItem.setCommand("#{ListaDocumentiBean.goToExecuteSingle}");
					menuItem.setParam("respId", resp.getId());
					menuItem.setOnstart("PF('statusDialog').show();");
					menuItem.setOnsuccess("PF('statusDialog').hide();");
					menuItem.setUpdate(DLGGENERIC_RESOURCE_ID);
					menuItem.setIcon(resp.getIcon());
					menuItem.setDisabled(utente.isGestioneApplicativa() && !(NavigationTokenEnum.PREPARAZIONE_ALLA_SPEDIZIONE.equals(sessionBean.getActivePageInfo()) || NavigationTokenEnum.PREPARAZIONE_ALLA_SPEDIZIONE_FEPA.equals(sessionBean.getActivePageInfo()))); 
 
					model.addElement(menuItem);
				}

				model.generateUniqueIds();
			}
		} else {
			model = null;
		}
		return model;
	}

	/**
	 * Prepara l'esecutore delle response singole a partire da un MenuActionEvent.
	 * {@link #getModel()}
	 * 
	 * @param event
	 */
	public void goToExecuteSingle(final ActionEvent event) {
		final MenuItem menuItem = ((MenuActionEvent) event).getMenuItem();
		final Integer id = Integer.parseInt(menuItem.getParams().get("respId").get(0));
		goToExecuteSingle(ResponsesRedEnum.getById(id));
	}

	private void settaggioDettaglioDocumento() {
		if (sessionBean.getActivePageInfo().equals(NavigationTokenEnum.DD_DA_LAVORARE) || sessionBean.getActivePageInfo().equals(NavigationTokenEnum.DD_IN_FIRMA)
				|| sessionBean.getActivePageInfo().equals(NavigationTokenEnum.DD_FIRMATI) || sessionBean.getActivePageInfo().equals(NavigationTokenEnum.DD_DA_FIRMARE)) {
			dettaglioDaCaricare = DettaglioDocumentoFileEnum.FEPADD;
		} else if (sessionBean.getActivePageInfo().equals(NavigationTokenEnum.DSR)) {
			dettaglioDaCaricare = DettaglioDocumentoFileEnum.FEPADSR;
		} else if (// sessionBean.getActivePageInfo().equals(NavigationTokenEnum.FATTURE_DA_ASSEGNARE)
					// ||
		sessionBean.getActivePageInfo().equals(NavigationTokenEnum.FATTURE_IN_LAVORAZIONE)
				|| sessionBean.getActivePageInfo().equals(NavigationTokenEnum.FATTURE_DA_LAVORARE)) {
			dettaglioDaCaricare = DettaglioDocumentoFileEnum.FEPAFATTURA;
		} else if (sessionBean.getActivePageInfo().equals(NavigationTokenEnum.PREPARAZIONE_ALLA_SPEDIZIONE) 
				|| sessionBean.getActivePageInfo().equals(NavigationTokenEnum.PREPARAZIONE_ALLA_SPEDIZIONE_FEPA)) {
			dettaglioDaCaricare = DettaglioDocumentoFileEnum.ASYNCSIGN;
		} else {
			dettaglioDaCaricare = DettaglioDocumentoFileEnum.GENERICO;
		}
	}

	/**
	 * Carica tutti i master disponibili.
	 */
	public final void loadAllMaster() {
		caricaTuttiGliElementi = true;
		searchSenzaPaginazione("", true);
	}

	/**
	 * Carica i master senza paginazione.
	 */
	public final void filterMastersSenzaPaginazione() {
		searchSenzaPaginazione(filterString, true);
	}

	/**
	 * Esegue una ricerca e imposta i master trovati del datatable.
	 * 
	 * @param searchString     - testo della ricerca
	 * @param allowEmptyString - splitta il testo della ricerca matchandone le parti
	 */
	private void searchSenzaPaginazione(final String searchString, final Boolean allowEmptyString) {
		// Con l'applicazione del filtro il datatable viene automaticamente reimpostato
		// alla prima pagina,
		// di conseguenza anche il pageIndex deve ripartire da zero
		pageIndex = 0;

		caricaTuttiGliElementi = true;

		// Gestione delle singole checkbox "Seleziona/deseleziona tutti" per ogni pagina
		// del datatable
		final DataTable dataTable = getDataTableByIdComponent(DOCUMENTI_RESOURCE_ID);
		handleSelectAllPageCheckboxesForFiltering(dataTable.getRows());

		if (!StringUtils.isNullOrEmpty(searchString) || Boolean.TRUE.equals(allowEmptyString)) {
			Collection<MasterDocumentRedDTO> filteredMasters;
			final DocumentQueueEnum queue = sessionBean.getActivePageInfo().getDocumentQueue();

			// Coda FileNet (anche da organigramma)
			if (SourceTypeEnum.FILENET.equals(queue.getType()) || DocumentQueueEnum.ORG_SCRIVANIA.equals(queue)) {
				filteredMasters = masterPaginatiSRV.getFilteredMasters(searchString, masterPageIterator, utente);
				// Coda applicativa
			} else {
				filteredMasters = new ArrayList<>();
				String numeroProtocollo;
				String annoProtocollo;
				String numeroDocumento;
				boolean found;

				if (!(allowEmptyString && StringUtils.isNullOrEmpty(searchString))) {
					// Si splitta la stringa di ricerca usando il separatore ";"
					final List<String> allTokensFiltro = Arrays.asList(searchString.split(";"));

					for (final String token : allTokensFiltro) {
						for (final MasterDocumentRedDTO master : notFilterMasters) {
							// Set di informazioni da confrontare con i caratteri inseriti
							numeroProtocollo = StringUtils.integerToString(master.getNumeroProtocollo());
							annoProtocollo = StringUtils.integerToString(master.getAnnoProtocollo());
							numeroDocumento = StringUtils.integerToString(master.getNumeroDocumento());

							// Controllo che ci sia un match tra il set e i caratteri inseriti
							found = StringUtils.containsIgnoreCase(numeroProtocollo, token) || StringUtils.containsIgnoreCase(annoProtocollo, token)
									|| StringUtils.containsIgnoreCase(numeroDocumento, token) || StringUtils.containsIgnoreCase(master.getTipologiaDocumento(), token)
									|| StringUtils.containsIgnoreCase(master.getTipoProcedimento(), token);
							// Nel caso il controllo di prima non abbia avuto risultati per ultimo controllo
							// nell'oggetto
							if (!found) {
								found = StringUtils.containsIgnoreCase(master.getOggetto(), token);
							}
							if (found) {
								master.setSelected(false);
								filteredMasters.add(master);
							}
						}
					}

				} else {
					filteredMasters.addAll(notFilterMasters);
				}
			}
			dataTable.reset();
			documentiDTH.setFilteredMasters(new ArrayList<>(filteredMasters));
			documentiDTH.setMasters(filteredMasters);

			// Effettuo un controllo nel caso in cui il filtro non restituisca nessun
			// risultato
			if (!CollectionUtils.isEmptyOrNull(filteredMasters)) {
				documentiDTH.selectFirst(true);
				if (documentiDTH.getCurrentMaster() != null) {
					documentiDTH.getCurrentMaster().setFlagSelezione(false);
					setLastMaster(documentiDTH.getCurrentMaster());
				}
				selectDetail(documentiDTH.getCurrentDetail());
				FacesHelper.executeJS("$('#eastSection').show();");
			}

		} else {
			clearFilter();
		}
	}

	/**
	 * Caratteristiche del D&D dall'organigramma:
	 * 
	 * - PUNTI DI DRAG
	 * 
	 * L'icona competenza di un qualsiasi documento (che quindi deve essere in
	 * competenza) che risiede: - sulla coda corriere di un qualunque ufficio
	 * presente nell'organigramma. - sulla coda di un qualunque utente presente
	 * nell'organigramma (unione della sua coda SOSPESO e DA LAVORARE)
	 * 
	 * - PUNTI DI DROP
	 * 
	 * Sono ammessi come punti di drop: - una qualunque coda utente
	 * dell'organigramma (unione della sua coda SOSPESO e DA LAVORARE) - una
	 * qualunque coda corriere di un qualunque ufficio presente nell'organigramma
	 * 
	 * - RESPONSE DA INVOCARE A SECONDA DEL PUNTO DI DRAG E DEL PUNTO DI DROP
	 * 
	 * Analizziamo ora i 4 possibili match punti di drag e punti di drop: - CODA
	 * CORRIERE -> CODA UTENTE SE il drag avviene dal corriere di un ufficio non
	 * segreteria ed il drop avviene su un utente della segreteria messaggio
	 * d'errore ALTRIMENTI applicare la response ASSEGNA sul wob draggato
	 * utilizzando l'utente in sessione con target l'utente su cui è stato eseguito
	 * il drop - CODA CORRIERE -> CODA CORRIERE SE il drag avviene dalla CODA
	 * CORRIERE di un ufficio di segreteria ed il drop su una CODA CORRIERE di un
	 * ufficio non segreteria applicare la response ASSEGNA sul wob draggato
	 * utilizzando l'utente in sessione con target l'ufficio della coda su cui è
	 * stato eseguito il drop ALTRIMENTI errore - CODA UTENTE -> CODA UTENTE SE il
	 * drag ed il drop coinvolgono entrambi utenti della segreteria, oppure entrambi
	 * utenti non della segreteria esegui la response STORNA A UTENTE sul wob
	 * draggato utilizzando l'utente in sessione con target l'utente su cui è stato
	 * eseguito il drop; ALTRIMENTI visualizza un messaggio d'errore - CODA UTENTE
	 * -> CODA CORRIERE Presentare un messaggio d'errore.
	 */
	public void dndOrganigramma() {
		final Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

		final String wobNumber = params.get("wob_number");
		final String idTreeNode = params.get("id_nodo");

		final String[] strs = idTreeNode.split("_");

		final String strIdUfficio = strs[0];
		final Long idUfficio = Long.parseLong(strIdUfficio);
		Long idUtente = null;

		if (strs.length > 1) {
			idUtente = Long.parseLong(strs[1]);
		}

		LOGGER.info("WOB NUMBER [" + wobNumber + "] - ID UFFICIO [" + idUfficio + "] - ID UTENTE[" + idUtente + "]");
	}

	private Boolean checkContent(final MasterDocumentRedDTO master) {
		Integer contentLength = 0;

		final EmailDTO email = documentoRedSRV.getMailByDocumentTitleAndIdAOObyAdmin(master.getDocumentTitle(), utente);

		if (email == null) {
			// se non è presente la mail molto probabilmente i destinatari saranno interni,
			// proseguo
			return true;
		}

		final Integer maxSize = casellePostaliSRV.getMaxSizeContentCasellaPostale(email.getMittente());

		if (maxSize == null) {
			// se la casella non ha limiti di spazio, proseguo
			return true;
		}

		// se il documento non ha destinatari elettronici salto il check e proseguo
		boolean destinatarioElettronico = false;
		if (email.getDestinatari() != null || email.getDestinatariCC() != null) {
			destinatarioElettronico = true;
		}
		if (!destinatarioElettronico) {
			return true;
		}

		final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
		final Document docPrincipale = fceh.getFileInfoAndContentByDocumentTitle(master.getDocumentTitle(), utente.getIdAoo());
		final ContentTransfer ct = FilenetCEHelper.getDocumentContentTransfer(docPrincipale);

		final byte[] content = FileUtils.getByteFromInputStream(ct.accessContentStream());
		contentLength += content.length;

		// se il content del doc principale è null
		// molto probabilmente lo sarà anche quello degli allegati
		final DocumentSet allegatiSet = fceh.getAllegatiConContent(master.getGuuid());

		if (allegatiSet != null && !allegatiSet.isEmpty()) {
			@SuppressWarnings("unchecked")
			final Iterator<Document> iter = allegatiSet.iterator();
			Document allegato = null;
			while (iter.hasNext()) {
				allegato = iter.next();

				final ContentTransfer ctAllegato = FilenetCEHelper.getDocumentContentTransfer(allegato);

				final byte[] contentAllegato = FileUtils.getByteFromInputStream(ctAllegato.accessContentStream());
				contentLength += contentAllegato.length;
			}
		}
		return contentLength < maxSize;
	}

	/**
	 * Esegue la response sul gruppo di Firma. Nel caso in cui la riga non sia stata
	 * selezionata seleziona la riga. Altrimenti esegue la response. (modo per
	 * aggirare la selezione front end)
	 * 
	 * @param event
	 */
	public final void eseguiResponseFirmaGruppo(final ActionEvent event) {
		final MasterDocumentRedDTO master = getDataTableClickedRow(event);

		final boolean check = checkContent(master);
		if (!check) {
			showError(ERROR_MAX_DIMENSIONE_FILE_SUPERATA_MSG);
			return;
		}
		if (master.equals(documentiDTH.getCurrentMaster()) && (documentiDTH.getSelectedMasters() == null || documentiDTH.getSelectedMasters().isEmpty())) {
			final ResponsesRedEnum[] responseOrdinate = sortListFirma(GruppiResponseEnum.FIRMA);
			eseguiResponseGruppo(null, responseOrdinate);
			FacesHelper.update(DLGGENERIC_RESOURCE_ID);
		} else {
			documentiDTH.selectMaster(master);
			selectDetail(documentiDTH.getCurrentMaster());
			FacesHelper.update(DETTAGLIODOCUMENTODD_RESOURCE_ID);
			FacesHelper.update(DOCUMENTI_RESOURCE_ID);
		}
	}

	/**
	 * Esegue la response sul gruppo di Firma Multiplan nel caso in cui la riga non
	 * sia stata selezionata seleziona la riga. Altrimenti esegue la response. (modo
	 * per aggirare la selezione front end)
	 * 
	 * @param event
	 */
	public final void eseguiResponseFirmaMultiplaGruppo(final ActionEvent event) {
		final MasterDocumentRedDTO master = getDataTableClickedRow(event);

		final boolean check = checkContent(master);
		if (!check) {
			showError(ERROR_MAX_DIMENSIONE_FILE_SUPERATA_MSG);
			return;
		}

		if (master.equals(documentiDTH.getCurrentMaster()) && (documentiDTH.getSelectedMasters() == null || documentiDTH.getSelectedMasters().isEmpty())) {
			final ResponsesRedEnum[] sortedList = sortListFirma(GruppiResponseEnum.FIRMA_MULTIPLA);
			eseguiResponseGruppo(null, sortedList);
			FacesHelper.update(DLGGENERIC_RESOURCE_ID);
		} else {
			documentiDTH.selectMaster(master);
			selectDetail(documentiDTH.getCurrentMaster());
			FacesHelper.update(DETTAGLIODOCUMENTODD_RESOURCE_ID);
			FacesHelper.update(DOCUMENTI_RESOURCE_ID);
		}
	}

	private ResponsesRedEnum[] sortListFirma(final GruppiResponseEnum gruppoResponseSelected) {
		final ResponsesRedEnum[] orderedArray = new ResponsesRedEnum[gruppoResponseSelected.getReponses().length];
		final List<ResponsesRedEnum> ordList = new ArrayList<>();

		if (utente.getPreferenzeApp().isFirmaRemotaFirst()) {
			for (final ResponsesRedEnum respGruppo : gruppoResponseSelected.getReponses()) {
				if (ResponsesRedEnum.FIRMA_REMOTA.equals(respGruppo) || ResponsesRedEnum.FIRMA_REMOTA_MULTIPLA.equals(respGruppo)) {
					ordList.add(0, respGruppo);
					continue;
				}
				ordList.add(respGruppo);
			}
		} else {
			for (final ResponsesRedEnum respGruppo : gruppoResponseSelected.getReponses()) {
				if (ResponsesRedEnum.FIRMA_DIGITALE.equals(respGruppo) || ResponsesRedEnum.FIRMA_DIGITALE_MULTIPLA.equals(respGruppo)) {
					ordList.add(0, respGruppo);
					continue;
				}
				ordList.add(respGruppo);
			}
		}
		for (int i = 0; i < ordList.size(); i++) {
			orderedArray[i] = ordList.get(i);
		}
		return orderedArray;
	}

	/**
	 * Esegue la response sul gruppo di SIGLA nel caso in cui la riga non sia stata
	 * selezionata seleziona la riga. Altrimenti esegue la response (modo per
	 * aggirare la selezione front end)
	 * 
	 * @param event
	 */
	public final void eseguiResponseSiglaGruppo(final ActionEvent event) {
		final MasterDocumentRedDTO master = getDataTableClickedRow(event);
		if (master != null) {
			if (master.equals(documentiDTH.getCurrentMaster()) && (documentiDTH.getSelectedMasters() == null || documentiDTH.getSelectedMasters().isEmpty())) {
				eseguiResponseGruppo(GruppiResponseEnum.SIGLA, null);
				FacesHelper.update(DLGGENERIC_RESOURCE_ID);
			} else {
				documentiDTH.selectMaster(master);
				selectDetail(documentiDTH.getCurrentMaster());
				FacesHelper.update(DETTAGLIODOCUMENTODD_RESOURCE_ID);
				FacesHelper.update(DOCUMENTI_RESOURCE_ID);
			}
		}
	}

	/**
	 * Esegue la response sul gruppo di VISTO nel caso in cui la riga non sia stata
	 * selezionata seleziona la riga. Altrimenti esegue la response (modo per
	 * aggirare la selezione front end)
	 * 
	 * @param event
	 */
	public final void eseguiResponseVistaGruppo(final ActionEvent event) {
		final MasterDocumentRedDTO master = getDataTableClickedRow(event);
		if (master != null) {
			if (master.equals(documentiDTH.getCurrentMaster()) && (documentiDTH.getSelectedMasters() == null || documentiDTH.getSelectedMasters().isEmpty())) {
				eseguiResponseGruppo(GruppiResponseEnum.VISTO, null);
				FacesHelper.update(DLGGENERIC_RESOURCE_ID);
			} else {
				documentiDTH.selectMaster(master);
				selectDetail(documentiDTH.getCurrentMaster());
				FacesHelper.update(DETTAGLIODOCUMENTODD_RESOURCE_ID);
				FacesHelper.update(DOCUMENTI_RESOURCE_ID);
			}
		}
	}

	/**
	 * Reimposta il datatable con la lista dei master non filtrati.
	 */
	public final void clearFilter() {
		for (final MasterDocumentRedDTO m : notFilterMasters) {
			m.setSelected(false);
		}
		documentiDTH.setMasters(notFilterMasters);
		documentiDTH.setFilteredMasters(new ArrayList<>(notFilterMasters));
	}

	/**
	 * Aggiunge una colonna al DT impostandone la visibilità.
	 * 
	 * @param event
	 */
	public final void onColumnToggleLaptop(final ToggleEvent event) {
		listaColonneCodeDTO.get((Integer) event.getData()).setVisibilitaColonna(event.getVisibility() == Visibility.VISIBLE);

		retrieveColonneConservate(listaColonneCodeDTO);
	}

	private List<ColonneCodeDTO> retrieveColonneConservate(final List<ColonneCodeDTO> colonneScelte) {
		if (sessionBean.getListaColonneCodeDTO() == null || sessionBean.getListaColonneCodeDTO().isEmpty()) {
			sessionBean.setListaColonneCodeDTO(new ArrayList<>(colonneScelte));
		} else {
			for (int i = 0; i < colonneScelte.size(); i++) {
				for (int j = 0; j < sessionBean.getListaColonneCodeDTO().size(); j++) {
					if (colonneScelte.get(i).getColEnum().getDescrColonna().equals(sessionBean.getListaColonneCodeDTO().get(j).getColEnum().getDescrColonna())
							&& colonneScelte.get(i).isVisibilitaColonna()) {
						sessionBean.getListaColonneCodeDTO().get(j).setVisibilitaColonna(colonneScelte.get(i).isVisibilitaColonna());
						break;
					}
				}
			}
		}
		return sessionBean.getListaColonneCodeDTO();
	}

	/**
	 * Metodo per la gestione del tab 'azioni massive' in base ai documenti
	 * selezionati.
	 */
	private void manageTabMultiActions() {
		if (documentiDTH.getSelectedMasters().isEmpty()) {
			// quando eseguo un check su un elemento imposto il tab delle azioni massive
			// come aperto
			sessionBean.setActiveIndexTabView(PRIMO_ATTIVO);
		} else {
			// ogni volta che levo il check da un elemento controllo se è l'ultimo. se si
			// imposto il tab che contiene le code
			sessionBean.setActiveIndexTabView(SECONDO_ATTIVO);
		}
	}

	/**
	 * Metodo per gestire la pressione del pulsante per selezionare/deselezionare
	 * tutte le checkbox.
	 * 
	 * @param event evento generato alla pressione del pulsante di selezione
	 *              multipla
	 */
	public final void handleAllCheckBox(final ActionEvent event) {
		final DataTable dataTable = (DataTable) getClickedComponent(event);
		handleSelectAllPageCheckbox(dataTable.getRows(), pageIndex);
	}

	/**
	 * Seleziona documenti assegnati per firma.
	 * 
	 * @param event
	 */
	public final void selectAllFirma(final ActionEvent event) {
		final DataTable dataTable = (DataTable) getClickedComponent(event);
		handleSelectAllResponsePageCheckbox(dataTable.getRows(), pageIndex, TipoAssegnazioneEnum.FIRMA);
	}

	/**
	 * Seleziona documenti assegnati per firma multipla.
	 * 
	 * @param event
	 */
	public final void selectAllFirmaMultipla(final ActionEvent event) {
		final DataTable dataTable = (DataTable) getClickedComponent(event);
		handleSelectAllResponsePageCheckbox(dataTable.getRows(), pageIndex, TipoAssegnazioneEnum.FIRMA_MULTIPLA);
	}

	/**
	 * Seleziona documenti da siglare.
	 * 
	 * @param event
	 */
	public final void selectAllSigla(final ActionEvent event) {
		final DataTable dataTable = (DataTable) getClickedComponent(event);
		handleSelectAllResponsePageCheckbox(dataTable.getRows(), pageIndex, TipoAssegnazioneEnum.SIGLA);
	}

	/**
	 * Seleziona documenti assegnati per visto.
	 * 
	 * @param event
	 */
	public final void selectAllVisto(final ActionEvent event) {
		final DataTable dataTable = (DataTable) getClickedComponent(event);
		handleSelectAllResponsePageCheckbox(dataTable.getRows(), pageIndex, TipoAssegnazioneEnum.VISTO);
	}

	/**
	 * Seleziona documenti assegnati per conoscenza.
	 * 
	 * @param event
	 */
	public final void selectAllCompetenza(final ActionEvent event) {
		final DataTable dataTable = (DataTable) getClickedComponent(event);
		handleSelectAllResponsePageCheckbox(dataTable.getRows(), pageIndex, TipoAssegnazioneEnum.COMPETENZA);
	}

	/**
	 * Seleziona documenti assegnati per conoscenza.
	 * 
	 * @param event
	 */
	public final void selectAllContributo(final ActionEvent event) {
		final DataTable dataTable = (DataTable) getClickedComponent(event);
		handleSelectAllResponsePageCheckbox(dataTable.getRows(), pageIndex, TipoAssegnazioneEnum.CONTRIBUTO);
	}

	/**
	 * Seleziona documenti assegnati per conoscenza.
	 * 
	 * @param event
	 */
	public final void selectAllConoscenza(final ActionEvent event) {
		final DataTable dataTable = (DataTable) getClickedComponent(event);
		handleSelectAllResponsePageCheckbox(dataTable.getRows(), pageIndex, TipoAssegnazioneEnum.CONOSCENZA);
	}

	private void handleSelectAllResponsePageCheckbox(final Integer pageRows, final Integer pageIndex, final TipoAssegnazioneEnum tipoAssegnazione) {
		List<MasterDocumentRedDTO> currentMastersList = null;
		// Se la lista dei filtrati non è vuota, si deve agire su questa
		if (CollectionUtils.isEmptyOrNull(documentiDTH.getFilteredMasters())) {
			currentMastersList = new ArrayList<>(notFilterMasters);
		} else {
			currentMastersList = documentiDTH.getFilteredMasters();
		}
		// Il numero di elementi in pagina potrebbe essere inferiore al numero massimo
		// di elementi per pagina
		final int endIndex = Math.min((pageIndex + 1) * pageRows, currentMastersList.size());
		MasterDocumentRedDTO m = null;
		boolean bAtLeastOne = false;

		for (int index = (pageIndex * pageRows); index < endIndex; index++) {
			m = currentMastersList.get(index);

			if (!m.getTipoAssegnazione().equals(tipoAssegnazione)) {
				documentiDTH.getSelectedMasters().remove(m);
				m.setSelected(false);
			} else {
				bAtLeastOne = true;
				documentiDTH.getSelectedMasters().add(m);
				m.setSelected(true);
			}
		}
		allElementCheckboxes.set(pageIndex, bAtLeastOne);

		// Gestione del tab delle azioni massive
		manageTabMultiActions();
		// Gestione delle response da mostrare nel tab delle azioni massive
		updateResponsePanel();

		if (!bAtLeastOne) {
			showWarnMessage("Nessun elemento selezionato corrispondente ai criteri");
		}
	}

	private void handleSelectAllPageCheckbox(final Integer pageRows, final Integer pageIndex) {
		Boolean selectAllPageCheckbox = allElementCheckboxes.get(pageIndex);
		allElementCheckboxes.set(pageIndex, selectAllPageCheckbox == null || !selectAllPageCheckbox);

		selectAllPageCheckbox = allElementCheckboxes.get(pageIndex);

		List<MasterDocumentRedDTO> currentMastersList = null;
		// Se la lista dei filtrati non è vuota, si deve agire su questa
		if (CollectionUtils.isEmptyOrNull(documentiDTH.getFilteredMasters())) {
			currentMastersList = new ArrayList<>(notFilterMasters);
		} else {
			currentMastersList = documentiDTH.getFilteredMasters();
		}
		// Il numero di elementi in pagina potrebbe essere inferiore al numero massimo
		// di elementi per pagina
		final int endIndex = Math.min((pageIndex + 1) * pageRows, currentMastersList.size());

		MasterDocumentRedDTO m = null;
		for (int index = (pageIndex * pageRows); index < endIndex; index++) {
			m = currentMastersList.get(index);

			if (selectAllPageCheckbox && Boolean.FALSE.equals(m.getSelected())) {
				documentiDTH.getSelectedMasters().add(m);
			} else if (Boolean.FALSE.equals(selectAllPageCheckbox) && Boolean.TRUE.equals(m.getSelected())) {
				documentiDTH.getSelectedMasters().remove(m);
			}
			m.setSelected(selectAllPageCheckbox);
		}

		// Gestione del tab delle azioni massive
		manageTabMultiActions();
		// Gestione delle response da mostrare nel tab delle azioni massive
		updateResponsePanel();
	}

	/**
	 * Metodo BE di update delle checkboxes.
	 * 
	 * @param event
	 */
	public final void updateCheck(final AjaxBehaviorEvent event) {
		final MasterDocumentRedDTO masterChecked = getDataTableClickedRow(event);
		final Boolean newValue = ((SelectBooleanCheckbox) event.getSource()).isSelected();
		if (newValue != null && newValue) {
			documentiDTH.getSelectedMasters().add(masterChecked);
		} else {
			documentiDTH.getSelectedMasters().remove(masterChecked);
		}

		if (allElementCheckboxes.get(pageIndex) == null) {
			allElementCheckboxes.set(pageIndex, Boolean.FALSE);
		}
		// la prima condizione serve nel caso di selezione multipla dal Libro Firma,
		// Corriere, Da Lavorare
		if (documentiDTH.getSelectedMasters() != null && !documentiDTH.getSelectedMasters().isEmpty()
				&& ((orgObj != null && DocumentQueueEnum.isOneKindOfCorriere(orgObj.getQueueToGo())) || sessionBean.getActivePageInfo().equals(NavigationTokenEnum.LIBROFIRMA)
						|| NavigationTokenEnum.isOneKindOfCorriere(sessionBean.getActivePageInfo())
						|| NavigationTokenEnum.isOneKindOfDaLavorareInLavorazione(sessionBean.getActivePageInfo()))) {
			allElementCheckboxes.set(pageIndex, true);
		} else {
			allElementCheckboxes.set(pageIndex, allElementCheckboxes.get(pageIndex) && newValue);
		}

		// Gestione del tab delle azioni massive
		manageTabMultiActions();
		// Gestione delle response da mostrare nel tab delle azioni massive
		updateResponsePanel();
	}

	/**
	 * Gestisce la checkbox "Seleziona/Deseleziona tutti" nel caso in cui ci stia
	 * agendo sui filtri sopra le colonne della lista documenti.
	 * 
	 * @param filterEvent
	 */
	public final void filterMasterTableListener(final FilterEvent filterEvent) {
		// Con l'applicazione del filtro il datatable viene automaticamente reimpostato
		// alla prima pagina,
		// di conseguenza anche il pageIndex deve ripartire da zero
		pageIndex = 0;
		final int pageRows = ((DataTable) filterEvent.getSource()).getRows();

		handleSelectAllPageCheckboxesForFiltering(pageRows);
	}

	private void handleSelectAllPageCheckboxesForFiltering(final int pageRows) {
		for (int index = 0; index < allElementCheckboxes.size(); index++) {
			final Boolean selectAllPageElementsCheckbox = allElementCheckboxes.get(index);

			if (Boolean.TRUE.equals(selectAllPageElementsCheckbox)) {
				handleSelectAllPageCheckbox(pageRows, index);
			}
		}
	}

	/**
	 * Selettore delle righe del DT.
	 * 
	 * @param se
	 */
	public final void rowSelector(final SelectEvent se) {
		if (getLastMaster() != null) {
			lastMaster.setFlagSelezione(true);
		}
		documentiDTH.rowSelector(se);
		selectDetail(documentiDTH.getCurrentMaster());
		documentiDTH.getCurrentMaster().setFlagSelezione(false);
		setLastMaster(documentiDTH.getCurrentMaster());
	}

	/**
	 * Metodo per la selezione e aggiornamento del dettaglio dall'esterno.
	 */
	public void aggiorna() {
		if (documentiDTH != null && documentiDTH.getCurrentMaster() != null) {
			selectDetail(documentiDTH.getCurrentMaster());
		}
	}

	private void selectDetail(final MasterDocumentRedDTO m) {
		boolean showEastButtons = false;

		if (dettaglioDaCaricareCodaCorrente == null && sessionBean.getActivePageInfo() != NavigationTokenEnum.RICERCA_DOCUMENTI
				&& sessionBean.getActivePageInfo() != NavigationTokenEnum.RICERCA_FEPA) {
			dettaglioDaCaricareCodaCorrente = dettaglioDaCaricare;
		}
		if (!DettaglioDocumentoFileEnum.FEPADSR.equals(dettaglioDaCaricareCodaCorrente) && !DettaglioDocumentoFileEnum.ASYNCSIGN.equals(dettaglioDaCaricare)
				&& ("DICHIARAZIONE SERVIZI RESI").equalsIgnoreCase(m.getTipologiaDocumento())) {
			dettaglioDaCaricare = DettaglioDocumentoFileEnum.FEPADSR;
			showEastButtons = !listaDocumentiSRV.isDocumentoInCoda(utente,m.getDocumentTitle(),DocumentQueueEnum.DSR);
		} else if (!DettaglioDocumentoFileEnum.FEPAFATTURA.equals(dettaglioDaCaricareCodaCorrente) && !DettaglioDocumentoFileEnum.ASYNCSIGN.equals(dettaglioDaCaricare) 
				&& ("FATTURA FEPA").equalsIgnoreCase(m.getTipologiaDocumento())) {
			dettaglioDaCaricare = DettaglioDocumentoFileEnum.FEPAFATTURA;
			showEastButtons = false;
		} else if (!DettaglioDocumentoFileEnum.FEPADD.equals(dettaglioDaCaricareCodaCorrente) && !DettaglioDocumentoFileEnum.ASYNCSIGN.equals(dettaglioDaCaricare)
				&& ("DECRETO DIRIGENZIALE FEPA").equalsIgnoreCase(m.getTipologiaDocumento())) {
			dettaglioDaCaricare = DettaglioDocumentoFileEnum.FEPADD;
			showEastButtons = false;
		} else {
			dettaglioDaCaricare = dettaglioDaCaricareCodaCorrente;
		}

		if (DettaglioDocumentoFileEnum.FEPAFATTURA.equals(dettaglioDaCaricare) || DettaglioDocumentoFileEnum.FEPADD.equals(dettaglioDaCaricare)
				|| DettaglioDocumentoFileEnum.FEPADSR.equals(dettaglioDaCaricare)) {
			final DettaglioDocumentoFepaBean fepaBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_FEPA_BEAN);
			fepaBean.setDetail(m.getDocumentTitle(), m.getWobNumber(), m.getClasseDocumentale(), dettaglioDaCaricare);
			fepaBean.setShowEastButtons(showEastButtons);
			fepaBean.setNomeFile(FILE_PRINCIPALE + m.getNomeFile());

			fepaBean.setResponse(m);
			responsSingle = fepaBean.getResponseDetail();
		} else if(DettaglioDocumentoFileEnum.ASYNCSIGN.equals(dettaglioDaCaricare)) {
			DettaglioDocumentoASignBean aSignBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_ASIGN_BEAN);
			
			aSignBean.setDetail(m);
			aSignBean.setResponse(m);
			responsSingle = aSignBean.getResponseDetail();
		} else {
			dettaglioDaCaricare = DettaglioDocumentoFileEnum.GENERICO;

			ddBean.setDetail(m);
			if (descrRegStampaEtichetteList != null && !descrRegStampaEtichetteList.isEmpty()) {
				for (final String descrRegistro : descrRegStampaEtichetteList) {
					if (m.getNumeroProtocollo() != null
							&& ((m.getDescrizioneRegistroRepertorio() != null && m.getDescrizioneRegistroRepertorio().equalsIgnoreCase(descrRegistro))
									|| m.getDescrizioneRegistroRepertorio() == null || "REGISTRO UFFICIALE".equals(m.getDescrizioneRegistroRepertorio()))) {
						m.setStampaEtichette(true);
						break;
					}
				}
			} else if (utente.getStampaEtichetteResponse() && m.getNumeroProtocollo() != null
					&& (m.getDescrizioneRegistroRepertorio() == null || "REGISTRO UFFICIALE".equals(m.getDescrizioneRegistroRepertorio()))) {
				m.setStampaEtichette(true);
			}

			ddBean.setResponse(m);
			responsSingle = ddBean.getResponseDetail();
		}

		docSelectedForResponse = m;
		FacesHelper.update("eastSectionForm:eastSectionPanelId");
	}

	private void clearDetail() {
		ddBean.unsetDetail();
		/*
		 * Per evitare l'errore (solo loggato in console) quando l'eastSectionPanelId
		 * non si trova in pagina potremmo: 1) aggiornare esclusivamente
		 * l'eastSectionForm (ma componenti al suo interno che non devono aggiornarsi lo
		 * faranno comunque); 2) includere tutte le sezioni east dentro un outputPanel
		 * con l'id 'eastSectionPanelId' (ma potrebbero cambiare gli id generati da PF
		 * [!!!]); 3) ...
		 * 
		 * ERRORE: Cannot find component for expression
		 * "eastSectionForm:eastSectionPanelId" referenced from [...]
		 * 
		 */
		FacesHelper.update("eastSectionForm:eastSectionPanelId");
	}

	/**
	 * Set del dettaglio e valorizzazione del context menu con le response
	 * disponibili.
	 */
	public void updateResponseSingle(final ActionEvent event) {
		updateResponseSingle((MasterDocumentRedDTO) getDataTableClickedRow(event));
	}

	/**
	 * Set del dettaglio e valorizzazione del context menu con le response
	 * disponibili.
	 */
	public void updateResponseSingle(final MasterDocumentRedDTO master) {
		responsSingle = new ArrayList<>();
		responsesIssue = new ArrayList<>();
		selectedResponse = null;
		docSelectedForResponse = master;

		// metto da parte il numero documento per dare evidenza di quale documento sono
		// le response
		numDocCurrent = docSelectedForResponse.getNumeroDocumento().toString();

//		<-- Recupero response Filenet e Applicative -->
		final ResponsesDTO respDto = responseRedSRV.refineReponsesSingle(docSelectedForResponse, utente, Boolean.FALSE);
//		<-- Fine recupero -->
//		<-- Predispongo dati per view -->
		orderResponse(respDto.getResponsesEnum());
		responsSingle = respDto.getResponsesEnum();
//		<-- Fine predisposizione -->
	}

	/**
	 * Valorizzazione del panel contenente le response multiple.
	 */
	public void updateResponsePanel() {
		responsesMulti = null;
		responsesIssue = new ArrayList<>();
		selectedResponse = null;
		Collection<ResponsesDTO> respDto = null;
		if (documentiDTH.getSelectedMasters() != null) {

//		<-- Recupero response Filenet e Applicative -->
			if (documentiDTH.getSelectedMasters().size() > 1) {
				respDto = responseRedSRV.refineResponses(documentiDTH.getSelectedMasters(), utente, Boolean.TRUE);
			} else {
				respDto = responseRedSRV.refineResponses(documentiDTH.getSelectedMasters(), utente, Boolean.FALSE);
			}
//		<-- Fine recupero -->

			for (final ResponsesDTO r : respDto) {

//			<-- Predispongo dati per view -->
				if (responsesMulti == null) {
					responsesMulti = r.getResponsesEnum();
				} else {
					responsesMulti = CollectionUtils.intersection(responsesMulti, r.getResponsesEnum());
				}
				responsesIssue = CollectionUtils.union(responsesIssue, r.getResponseIssues());
//			<-- Fine predisposizione -->
			}
			orderResponse(responsesMulti);
		}
	}

	/**
	 * Prepara ed esegue una response (anche multipla).
	 * 
	 * @param r
	 */
	public void goToExecute(final ResponsesRedEnum r) {
		// coonsiderare sempre la chiamata 'execute' come multipla
		// perche il bottone AZIONI gestisce SOLO quelle multiple
		selectedResponse = r;
		renderResponse = true;

		// pulisco gli esiti
		cleanEsiti();

		final Collection<String> wobNumbers = new ArrayList<>();
		final List<MasterDocumentRedDTO> masters = documentiDTH.getSelectedMasters();
		for (final MasterDocumentRedDTO m : masters) {
			if (!StringUtils.isNullOrEmpty(m.getWobNumber())) {
				wobNumbers.add(m.getWobNumber());
			}
		}
		// Avvio l'esecutore delle response e immagazzino gli esiti Operazione
		final Collection<EsitoOperazioneDTO> esitiOperazioneTemp = execute(r, wobNumbers);
		if (esitiOperazioneTemp != null) {
			esitiOperazione = esitiOperazioneTemp;
		}
//		<-- Gestione esiti solo per i Service Task -->
		// verifico che gli esiti siano valorizzati -- può capitare, come nel caso degli
		// HumanTask che gli esiti non vengano presi in considerzione iin questo Bean
		if (esitiOperazione != null && !esitiOperazione.isEmpty() && !esitiOperazione.contains(null)) {

			// aggiorno la dialog contenente gli esiti
			FacesHelper.update(DLGSHOWRESULT_RESOURCE_ID);

			// richiamo la visualizzazione della dialog
			FacesHelper.executeJS(SHOW_DLGSHOWRESULT_JS);
		}
	}

	/**
	 * {@link #goToExecute(ResponsesRedEnum)}.
	 * 
	 * @param strR
	 */
	public void goToExecuteSingle(final String strR) {
		final ResponsesRedEnum r = ResponsesRedEnum.get(strR);
		goToExecuteSingle(r);
	}

	/**
	 * Prepara ed avvia l'esecutore per la response singola.
	 * 
	 * @param r
	 */
	public void goToExecuteSingle(final ResponsesRedEnum r) {
		selectedResponse = r;
		renderResponse = true;

		// pulisco gli esiti
		cleanEsiti();
		
		//Avvio l'esecutore delle response e immagazzino gli esiti Operazione
		if ((docSelectedForResponse.getWobNumber() != null)||ResponsesRedEnum.RIATTIVA_PROCEDIMENTO.equals(r)
				||ResponsesRedEnum.RECALL.equals(r) || ResponsesRedEnum.RETRY.equals(r)) {
			String input = docSelectedForResponse.getWobNumber();
			if (input == null) {
				input = docSelectedForResponse.getDocumentTitle();
			}

			final EsitoOperazioneDTO esito = execute(r, input);
			if (esito != null) {
				esitiOperazione.add(esito);
			}
		}

//		<-- Gestione esiti solo per i Service Task -->
		// verifico che gli esiti siano valorizzati -- può capitare, come nel caso degli
		// HumanTask che gli esiti non vengano presi in considerzione iin questo Bean
		if (esitiOperazione != null && !esitiOperazione.isEmpty() && !esitiOperazione.contains(null)) {

			// aggiorno la dialog contenente gli esiti
			FacesHelper.update(DLGSHOWRESULT_RESOURCE_ID);

			// richiamo la visualizzazione della dialog
			FacesHelper.executeJS(SHOW_DLGSHOWRESULT_JS);
		}
	}

	/**
	 * Clean esiti operazioni.
	 */
	public void cleanEsiti() {
		esitiOperazione = new ArrayList<>();
	}

	/**
	 * Clear responses.
	 */
	public void clearResponse() {
		responsSingle = new ArrayList<>();
		responsesMulti = new ArrayList<>();
	}

	/**
	 * Metodo utilizzato dai HumanTaskBean per distruggere la sessione del propio
	 * bean e reset dello stato della selectedResponse.
	 * 
	 * @param yourBeanArray
	 *            Bean di riferimento
	 */
	public void destroyBeanViewScope(final String... yourBeanArray) {
		FacesHelper.destroyBeanViewScope(yourBeanArray);
		renderResponse = false;
		FacesHelper.update(DLGGENERIC_RESOURCE_ID);
	}

	/**
	 * Apre la maschera di gestione del fascicolo(/fasc. fepa) procedimentale.
	 */
	public void openFascicoloProcedimentale() {
		IFascicoloProcedimentaleManagerInitalizer initializer;
		if (DettaglioDocumentoFileEnum.FEPAFATTURA.equals(dettaglioDaCaricare) || DettaglioDocumentoFileEnum.FEPADD.equals(dettaglioDaCaricare)
				|| DettaglioDocumentoFileEnum.FEPADSR.equals(dettaglioDaCaricare)) {
			initializer = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_FEPA_BEAN);
		} else {
			dettaglioDaCaricare = DettaglioDocumentoFileEnum.GENERICO;
			initializer = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN);
		}
		initializer.openFascicoloProcedimentale();
	}

	/**
	 * Apre la maschera di dettaglio esteso del documento/doc. fepa.
	 */
	public void openDettaglioEsteso() {
		try {
			IOpenDettaglioEstesoButtonComponent initializer;
			if (DettaglioDocumentoFileEnum.FEPAFATTURA.equals(dettaglioDaCaricare) || DettaglioDocumentoFileEnum.FEPADD.equals(dettaglioDaCaricare)
					|| DettaglioDocumentoFileEnum.FEPADSR.equals(dettaglioDaCaricare)) {
				initializer = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_FEPA_BEAN);
			} else {
				dettaglioDaCaricare = DettaglioDocumentoFileEnum.GENERICO;
				initializer = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN);
			}
			initializer.openDocumentPopup();

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di recupero del documento.");
		}
	}

	/**
	 * Metodo per il reset della view dettaglio sintetico.
	 */
	public void resetDetailView() {
		ddBean.resetDetailView();
	}
	
	/**
	 * Metodo per il recupero dei parametri necessari .
	 * @param mailLink - Mail link criptato
	 * @return Elenco chiave/valore dei parametri necessari per la visualizzazione del documento della mail
	 */
	private Map<String, String> recoveryMailLinkParam(final String mailLink) {
		Map<String, String> output = new HashMap<>();
		String cleanMailLink = mailLink.replace(" ", "+");
			
		//DECRIPTO I PARAMETRI
		String paramDecrypted = DesCrypterNew.decrypt(true, cleanMailLink);
		String[] paramsSplit = paramDecrypted.split(Constants.RequestParameter.MailLink.MAIL_LINK_REQUEST_PARAMETERS_SEPARATOR);
		boolean isValid = true;
		if (paramsSplit.length < Constants.RequestParameter.MailLink.MAIL_LINK_PARAMETER_SIZE) {
			isValid = false;
		} else {
			for (int i = 0; i < paramsSplit.length; i++) {
				String keyValue = paramsSplit[i];
				if (keyValue != null && keyValue.split(Constants.RequestParameter.MailLink.MAIL_LINK_PARAMETER_SEPARATOR).length == 2) {
					String key = keyValue.split(Constants.RequestParameter.MailLink.MAIL_LINK_PARAMETER_SEPARATOR)[0];
					if ((keyValue.split(Constants.RequestParameter.MailLink.MAIL_LINK_PARAMETER_SEPARATOR)[1] != null) 
							&& (key.equals(Constants.RequestParameter.MailLink.MAIL_LINK_DOCUMENT_TITLE_PARAM_NAME)
								|| key.equals(Constants.RequestParameter.MailLink.MAIL_LINK_ID_RUOLO_PARAM_NAME)
								|| key.equals(Constants.RequestParameter.MailLink.MAIL_LINK_ID_NODO_NAME)
								|| key.equals(Constants.RequestParameter.MailLink.MAIL_LINK_USERNAME_PARAM_NAME))) {
							output.put(key, keyValue.split(Constants.RequestParameter.MailLink.MAIL_LINK_PARAMETER_SEPARATOR)[1]);
					}
				}
			}
			if (output.size() < Constants.RequestParameter.MailLink.MAIL_LINK_PARAMETER_SIZE) {
				isValid = false;
			}
		}
		if (!isValid) {
			LOGGER.error("Impossibile recuperare tutti i parametri criptati obbligatori per la visualizzazione del documento: almeno un parametro mancante.");
			showError("Impossibile recuperare tutti i parametri criptati obbligatori per la visualizzazione del documento: almeno un parametro mancante.");
		}
		return output;
	}
	
	
//	##################  START GET/SET ###################

	/**
	 * Restituisce l'utente.
	 * 
	 * @return utente
	 */
	public final UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Restituisce il datatable della lista documenti.
	 * 
	 * @return
	 */
	public SimpleDetailDataTableHelper<MasterDocumentRedDTO> getDocumentiDTH() {
		return documentiDTH;
	}

	/**
	 * Imposta il datatable della lista documenti.
	 * 
	 * @param documentiDTH
	 */
	public void setDocumentiDTH(final SimpleDetailDataTableHelper<MasterDocumentRedDTO> documentiDTH) {
		this.documentiDTH = documentiDTH;
	}

	/**
	 * Restituisce la stringa di ricerca.
	 * 
	 * @return filterString
	 */
	public String getFilterString() {
		return filterString;
	}

	/**
	 * Imposta la stringa di ricerca.
	 * 
	 * @param filterString
	 */
	public void setFilterString(final String filterString) {
		this.filterString = filterString;
	}

	/**
	 * Restituisce la lista dei valori delle boolean checkboxes.
	 * 
	 * @return the allElementCheckboxes
	 */
	public List<Boolean> getAllElementCheckboxes() {
		return allElementCheckboxes;
	}

	/**
	 * Imposta la lista dei valori delle boolean checkboxes.
	 * 
	 * @param allElementCheckboxes
	 */
	public void setAllElementCheckboxes(final List<Boolean> allElementCheckboxes) {
		this.allElementCheckboxes = allElementCheckboxes;
	}

	/**
	 * Restituisce il MenuModel delle response.
	 * 
	 * @return modelCode
	 */
	public final MenuModel getModelCode() {
		return modelCode;
	}

	/**
	 * Restituisce la response selezionata.
	 * 
	 * @return selectedResponse
	 */
	public final ResponsesRedEnum getSelectedResponse() {
		return selectedResponse;
	}

	/**
	 * Imposta la response selezionata.
	 * 
	 * @param selectedResponse
	 */
	public void setSelectedResponse(final ResponsesRedEnum selectedResponse) {
		this.selectedResponse = selectedResponse;
	}

	/**
	 * Restituisce la coda selezionata.
	 * 
	 * @return selectedQueue
	 */
	public DocumentQueueEnum getSelectedQueue() {
		return selectedQueue;
	}

	/**
	 * Imposta la coda selezionata.
	 * 
	 * @param selectedQueue
	 */
	public void setSelectedQueue(final DocumentQueueEnum selectedQueue) {
		this.selectedQueue = selectedQueue;
	}

	/**
	 * Restituisce il nome della coda corrente.
	 * 
	 * @return nomeCodaAttuale
	 */
	public final String getNomeCodaAttuale() {
		return nomeCodaAttuale;
	}

	/**
	 * Imposta il nome della coda corrente.
	 * 
	 * @param nomeCodaAttuales
	 */
	public final void setNomeCodaAttuale(final String nomeCodaAttuale) {
		this.nomeCodaAttuale = nomeCodaAttuale;
	}

	/**
	 * Restituisce un insieme di responses.
	 * 
	 * @return responsesMulti
	 */
	public final Collection<ResponsesRedEnum> getResponsesMulti() {
		return responsesMulti;
	}

	/**
	 * Imposta un inseme di responses.
	 * 
	 * @param responsesMulti
	 */
	public final void setResponsesMulti(final Collection<ResponsesRedEnum> responsesMulti) {
		this.responsesMulti = responsesMulti;
	}

	/**
	 * Restituisce un insieme di responses singole.
	 * 
	 * @return the responsSingle
	 */
	public final Collection<ResponsesRedEnum> getResponsSingle() {
		return responsSingle;
	}

	/**
	 * Imposta un insieme di responses singole.
	 * 
	 * @param responsSingle
	 */
	public final void setResponsSingle(final Collection<ResponsesRedEnum> responsSingle) {
		this.responsSingle = responsSingle;
	}

	/**
	 * Restituisce un insieme di responses issue.
	 * 
	 * @return the responsesIssue
	 */
	public Collection<String> getResponsesIssue() {
		return responsesIssue;
	}

	/**
	 * Imposta un insieme di responses issue.
	 * 
	 * @param responsesIssue the responsesIssue to set
	 */
	public void setResponsesIssue(final Collection<String> responsesIssue) {
		this.responsesIssue = responsesIssue;
	}

	/**
	 * Restituisce l'insieme di esiti operazione.
	 * 
	 * @return the esitiOperazione
	 */
	public final Collection<EsitoOperazioneDTO> getEsitiOperazione() {
		return esitiOperazione;
	}

	/**
	 * Imposta l'insieme di esiti operazione.
	 * 
	 * @param esitiOperazione
	 */
	public final void setEsitiOperazione(final Collection<EsitoOperazioneDTO> esitiOperazione) {
		this.esitiOperazione = esitiOperazione;
	}

	/**
	 * Restituisce se è stato selezionato il flag di conferma firma.
	 * 
	 * @return flagConfermaFirma
	 */
	public final boolean isFlagConfermaFirma() {
		return flagConfermaFirma;
	}

	/**
	 * Imposta il flag di conferma firma.
	 * 
	 * @param flagConfermaFirma
	 */
	public final void setFlagConfermaFirma(final boolean flagConfermaFirma) {
		this.flagConfermaFirma = flagConfermaFirma;
	}

	/**
	 * Restituisce un DefaultMenuModel per le responses.
	 * 
	 * @return menuContextResponse
	 */
	public final DefaultMenuModel getMenuContextResponse() {
		return menuContextResponse;
	}

	/**
	 * Imposta un DefaultMenuModel per le responses.
	 * 
	 * @param menuContextResponse
	 */
	public final void setMenuContextResponse(final DefaultMenuModel menuContextResponse) {
		this.menuContextResponse = menuContextResponse;
	}

	/**
	 * Restituisce il numero documento corrente.
	 * 
	 * @return numDocCurrent
	 */
	public final String getNumDocCurrent() {
		return numDocCurrent;
	}

	/**
	 * Imposta il numero documento corrente.
	 * 
	 * @param numDocCurrent
	 */
	public final void setNumDocCurrent(final String numDocCurrent) {
		this.numDocCurrent = numDocCurrent;
	}

	/**
	 * Avvia l'esecutore delle response.
	 * 
	 * @param response
	 * @param wobNumber
	 * @return esito operazione
	 */
	public EsitoOperazioneDTO execute(final ResponsesRedEnum response, final String wobNumber) {
		return (EsitoOperazioneDTO) executeResponse(response, wobNumber);
	}

	/**
	 * Avvia l'esecutore delle response su più documenti.
	 * 
	 * @param response
	 * @param wobNumbers
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<EsitoOperazioneDTO> execute(final ResponsesRedEnum response, final Collection<String> wobNumbers) {
		return (Collection<EsitoOperazioneDTO>) executeResponse(response, wobNumbers);
	}

	@SuppressWarnings("unchecked")
	private Object executeResponse(final ResponsesRedEnum response, final Object input) {
		Object output = null;
		final String target = response.getTarget();

		if (ResponsesRedEnum.PROCEDI.getTarget().equals(target) || ResponsesRedEnum.FIRMA_DIGITALE.getTarget().equals(target)
				|| ResponsesRedEnum.FIRMA_REMOTA.getTarget().equals(target) || ResponsesRedEnum.FIRMA_DIGITALE_MULTIPLA.getTarget().equals(target)
				|| ResponsesRedEnum.FIRMA_REMOTA_MULTIPLA.getTarget().equals(target)) {
			if (input instanceof String) {
				final Boolean check = checkContent(docSelectedForResponse);
				if (check != null && !check) {
					showError(ERROR_MAX_DIMENSIONE_FILE_SUPERATA_MSG);
					return output;
				}
			} else if (input instanceof Collection<?>) {
				for (final MasterDocumentRedDTO master : documentiDTH.getSelectedMasters()) {
					final Boolean check = checkContent(master);
					if (check != null && !check) {
						showError("La dimensione dei file contenuti nel/i procedimento/i supera la dimensione massima consentita dalla casella di posta mittente.");
						return output;
					}
				}
			}
		}
		// disinnesca recall per i documenti su cui è sollecitata una response che non
		// sia presente in responseNoChangeRecallList
		if (!responseNoChangeRecallList.contains(response)) {
			if (input instanceof String) {
				codeApplicativeSRV.fromRecallToLavorate((String) input, utente);
			} else if (input instanceof Collection<?>) {
				for (final String wobNumber : (Collection<String>) input) {
					codeApplicativeSRV.fromRecallToLavorate(wobNumber, utente);
				}
			}
		}
		if (TaskTypeEnum.HUMAN_TASK.equals(response.getTaskType())) {
			// tramite js visualizzo la dialog generica che gestisce i singoli pannelli
			FacesHelper.executeJS("PF('dlgGeneric').show();");

			// recupero il Bean che gestisce la response
			final InitHumanTaskInterface humanTask = FacesHelper.getManagedBean(target);
			// e invoco il metodo specifico
			if (input instanceof String) {
				// se istanza di stringa provengo da una response singola
				final List<MasterDocumentRedDTO> docs = new ArrayList<>();
				docs.add(docSelectedForResponse);
				humanTask.initBean(docs);
			} else {
//				altrimenti provengo da una selezione multipla e passo quelli selezionati
				humanTask.initBean(documentiDTH.getSelectedMasters());
			}
		} else if (TaskTypeEnum.BEAN_TASK.equals(response.getTaskType())) {
			final BeanTaskDTO bt = BeanTaskProvider.getProvider().getBeanTaskInfo(target);
			final Object bean = FacesHelper.getManagedBean(bt.getBeanName());

			try {
				output = bt.getBeanMethod().invoke(bean);
			} catch (final Exception e) {
				LOGGER.error(e);
				throw new RedException(e);
			}
		} else if (TaskTypeEnum.SERVICE_TASK.equals(response.getTaskType())) {
			final ServiceTaskDTO st = ServiceTaskProvider.getProvider().getServiceTaskInfo(target);
			final Object srv = ApplicationContextProvider.getApplicationContext().getBean(st.getClsSrv());
			Method mtd = st.getMultiService();
			if (input instanceof String) {
				mtd = st.getSingleService();
			}
			final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
			try {
				// UtenteDTO, String oppure UtenteDTO, Collection<String>. srv(UTENTE PINO,
				// WOB_NUMBER)
				// oppure srv(UTENTE PINO, Collection<WOB_NUMBER>)
				output = mtd.invoke(srv, sb.getUtente(), input);
			} catch (final Exception e) {
				LOGGER.error(e);
				throw new RedException(e);
			}
		} else {
			throw new RedException("Errore in fase di configurazione della response " + response);
		}
		return output;
	}

	/**
	 * Apre l'applet.
	 */
	@BeanTask(name = "modificaApplet")
	public void openApplet() {
		final String workflowNumber = documentiDTH.getCurrentMaster().getWobNumber();
		final DocumentAppletContentDTO detail = opSRV.getDocumentInfoForApplet(workflowNumber, utente);

		if (detail == null) {
			showError("Il documento selezionato non possiede una versione modificabile.");
		} else {
			final GestioneLockDTO gestLockDTO = documentoSRV.getLockDocumento(Integer.valueOf(documentiDTH.getCurrentMaster().getDocumentTitle()), utente.getIdAoo(), 1,
					utente.getId());
			Boolean lockDocumentoNonPresente = gestLockDTO.getNessunLock();
			if (lockDocumentoNonPresente != null && !lockDocumentoNonPresente) {
				lockDocumentoNonPresente = gestLockDTO.isInCarico();
			}
			if (lockDocumentoNonPresente == null || lockDocumentoNonPresente) {
				openModificaApplet(workflowNumber, detail);
			} else {
				nomeUtenteLockatore = gestLockDTO.getUsernameUtenteLock();
				FacesHelper.update("centralSectionForm:sbloccaDocumentoModificaDlg");
				FacesHelper.executeJS("PF('sbloccaDocumentoModificaDlg_WV').show();");
			}
		}
	}

	/**
	 * Controlla lo stato del lock del documento ed avvia l'applet di modifica.
	 */
	public void continuaModificaAfterCheckLock() {
		documentoSRV.updateLockDocumento(Integer.valueOf(documentiDTH.getCurrentMaster().getDocumentTitle()), utente.getIdAoo(), 1, true, utente.getId(), utente.getNome(),
				utente.getCognome());
		final String workflowNumber = documentiDTH.getCurrentMaster().getWobNumber();
		final DocumentAppletContentDTO detail = opSRV.getDocumentInfoForApplet(workflowNumber, utente);
		openModificaApplet(workflowNumber, detail);
	}

	private void openModificaApplet(final String workflowNumber, final DocumentAppletContentDTO detail) {
		FacesHelper.putObjectInSession(ConstantsWeb.SessionObject.USERNAME, utente.getUsername());

		FacesHelper.executeJS("openDoc('" + detail.getContentType() + "','" + detail.getContent() + "','" + workflowNumber + "','" + detail.getNomeFile() + "')");
	}

	/**
	 * Avvia la predisposizione di una dichiarazione.
	 */
	@BeanTask(name = "predisponiDichiarazione")
	public void predisponiDichiarazione() {
		final List<MasterDocumentRedDTO> docs = new ArrayList<>();
		try {
			final MasterDocumentRedDTO master = documentiDTH.getCurrentMaster();
			if (master.getNumeroProtocollo() != null || master.getAnnoProtocollo() != null || master.getTipoProtocollo() != null || master.getDataProtocollazione() != null) {
				final InitHumanTaskInterface humanTask = FacesHelper.getManagedBean(ConstantsWeb.MBean.PREDISPONI_DICHIARAZIONE);
				docs.add(master);
				humanTask.initBean(docs);
				FacesHelper.executeJS(SHOW_DLG_GENERIC_JS);
				FacesHelper.update("centralSectionForm:idDocumentManagerContent_PDSR");
			} else {
				showError("Impossibile predisporre la dichiarazione per un documento non protocollato");
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Errore all'avvio di predisponi dichiarazione");
		}
	}

	/**
	 * Attivazione della response di firma remota.
	 */
	@BeanTask(name = "firmaRemota")
	public void responseFirmaRemota() {
		firmaMultipla = false;
		gestisciFirmaRemota();
	}

	/**
	 * Attivazione della response di firma remota multipla (cioè che prevede
	 * molteplici firmatari).
	 */
	@BeanTask(name = "firmaRemotaMultipla")
	public void responseFirmaRemotaMultipla() {
		firmaMultipla = true;
		gestisciFirmaRemota();
	}

	private void gestisciFirmaRemota() {
		String confirmMessage = Constants.EMPTY_STRING;
		final List<MasterDocumentRedDTO> documentiSelezionati = new ArrayList<>();
		boolean isRepertoriabile = Boolean.FALSE;

		try {
			cleanEsiti();

			if (documentiDTH.getSelectedMasters() != null && !documentiDTH.getSelectedMasters().isEmpty()) {
				// provengo da azioni massive
				documentiSelezionati.addAll(documentiDTH.getSelectedMasters());
			} else {
				// provengo da azione singola
				documentiSelezionati.add(docSelectedForResponse);
			}

			for (final MasterDocumentRedDTO m : documentiSelezionati) {
				// Il documento deve avere il flag del Repertorio e non deve essere già
				// protocollato.
				if (m.getIsModalitaRegistroRepertorio() != null && m.getIsModalitaRegistroRepertorio() && (m.getNumeroProtocollo() == null || m.getNumeroProtocollo() <= 0)) {
					isRepertoriabile = Boolean.TRUE;
				} else {
					isRepertoriabile = Boolean.FALSE;
					break;
				}
			}

			// Caso 1: Registro Emergenza Attivo ma il documento Ã¨ un Registro di
			// Repertorio.
			if (isRepertoriabile && TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo())) {

				showWarnMessage(WARNING_REGISTRO_REPERTORIO_PROTOCOLLAZIONE_EMERGENZA_MSG);

			}
			if (isRepertoriabile && Boolean.FALSE.equals(checkDestintariSoloInterni(documentiSelezionati))) {
				// Caso 2: Il documento è un Registro di Repertorio ma i Destinatari non sono
				// solo Interni.
				FacesHelper.executeJS(SHOW_WDGDLGAVVISODESTINTERNI_JS);

			} else if (utente.getSignerInfo().getImageFirma() == null || utente.getSignerInfo().getImageFirma().length <= 0) {
				// Caso 3: L'utente non ha il glifo.
				// Si controlla se l'utente ha il glifo
				FacesHelper.showMessage("", "Impossibile procedere con la firma a causa della mancanza del glifo di firma dell'utente.", MessageSeverityEnum.ERROR);

			} else {

				confirmMessage = signSRV.checkApprovazioniDaNonStampigliare(documentiSelezionati, utente);

				if (!StringUtils.isNullOrEmpty(confirmMessage)) {
					showConfirmMessageFirmaApprovazioni = true;
					confirmMessageFirmaApprovazioni = confirmMessage;
				}
				checkDocumentsPreSign();
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Avvia l'applet di stampa protocollo.
	 */
	@BeanTask(name = "stampaProtocolloApplet")
	public void openStampaProtocolloApplet() {

		MasterDocumentRedDTO tmp = null;
		if (documentiDTH.getSelectedMasters() != null && !documentiDTH.getSelectedMasters().isEmpty()) {
			// provengo da azioni massive
			tmp = documentiDTH.getSelectedMasters().get(0);
		} else {
			// provengo da azione singola
			tmp = docSelectedForResponse;
		}

		final String dateFormat = DateUtils.dateToString(tmp.getDataProtocollazione(), true);

		protocolloDaStampare = "MEF - " + utente.getCodiceAoo() + " - Prot. " + tmp.getNumeroProtocollo() + " del " + dateFormat;

		if (CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0].equals(tmp.getIdCategoriaDocumento())) {

			protocolloDaStampare += " - " + CategoriaDocumentoEnum.ENTRATA.getDescrizione();

		} else if (CategoriaDocumentoEnum.DOCUMENTO_USCITA.getIds()[0].equals(tmp.getIdCategoriaDocumento())
				|| CategoriaDocumentoEnum.MOZIONE.getIds()[0].equals(tmp.getIdCategoriaDocumento())) {

			protocolloDaStampare += " - " + CategoriaDocumentoEnum.USCITA.getDescrizione();

		}
		flagStampaProtocollo = true;
		PrimeFaces.current().executeScript(SHOW_DLG_GENERIC_JS);
		FacesHelper.update("centralSectionForm:idStampProt");
	}

	/**
	 * Pulisce i campi di input della dialog di firma remota.
	 */
	public void clearInputFirmaRemota() {
		showConfirmMessageFirmaApprovazioni = false;
		confirmMessageFirmaApprovazioni = "";
		showConfirmMessageFirmaRemota = false;
		openFirmaRemotaDialog = false;
		otp = "";
		pin = "";
		confirmMessageFirmaRemota = "";
		selectedSignType = "";
		selectedSignTypeFirmaRemota = "";
		memorizzaPin = false;
		erroreOTP = false;
		errorePIN = false;
		pinBloccato = false;  
		glifoSelezionatoRemota = null;
		glifoSelezionatoDigitale = null;
	}

	/**
	 * Pulisce i campi e nasconde la dialog di firma remota.
	 */
	public void hideDialogFirmaRemota() {
		clearInputFirmaRemota();
		FacesHelper.executeJS("PF('wdgFirmaRemota').hide()");
		FacesHelper.update(FIRMAREMOTA_RESOURCE_ID);
	}

	/**
	 * Check documenti pre firma.
	 */
	public void checkDocumentsPreSign() {
		String errorMessage = "";
		try {
			if (documentiDTH.getSelectedMasters() != null && !documentiDTH.getSelectedMasters().isEmpty()) {
				// provengo da azioni massive
				errorMessage = signSRV.checkDocumentsPreSign(utente, documentiDTH.getSelectedMasters());
			} else {
				// provengo da azioni singole
				final List<MasterDocumentRedDTO> documentiSelezionati = new ArrayList<>();
				documentiSelezionati.add(docSelectedForResponse);
				errorMessage = signSRV.checkDocumentsPreSign(utente, documentiSelezionati);
			}
			if (!StringUtils.isNullOrEmpty(errorMessage)) {
				// almeno un documento fra quelli selezionati non è valido per la firma
				showError(errorMessage);
			} else {
				boolean hasFirme = loadComboboxTipoFirma();
				if(hasFirme) {
					openFirmaRemotaDialog = true;
					if (Boolean.FALSE.equals(showConfirmMessageFirmaRemota)) {
						if (NavigationTokenEnum.LIBRO_FIRMA_DELEGATO.equals(sessionBean.getActivePageInfo())) {
							if(tipologieGlifoFirma == null || tipologieGlifoFirma.size()==0) {
								tipologieGlifoFirma = signSRV.recuperaGlifoDelegato(utente);	
							}						
							onSelectGlifoFirmaRemota();
						} 
						FacesHelper.executeJS("PF('wdgFirmaRemota').show()");
						FacesHelper.update("centralSectionForm:wdgFirmaRemota_id");
					}
				} else {
					showWarnMessage("I documenti selezionati non hanno un tipo di firma omogeneo. "
							+ "É necessario, per procedere con la firma massiva, selezionare documenti che consentano un tipo di firma omogeneo (PDF e/o P7M).");
				}
				showConfirmMessageFirmaRemota = false;
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		} finally {
			showConfirmMessageFirmaRemota = false;
		}
	}

	/**
	 * Carica le tipologie di firma disponibili.
	 * @return true se la lista NON è vuota, false altrimenti.
	 */
	private boolean loadComboboxTipoFirma() {
		List<MasterDocumentRedDTO> documentiSelezionati = new ArrayList<>();

		signTypeList = new ArrayList<>();
		if (documentiDTH.getSelectedMasters() != null && !documentiDTH.getSelectedMasters().isEmpty()) {
			// azione massiva
			documentiSelezionati = documentiDTH.getSelectedMasters();
		} else {
			// azione singola
			documentiSelezionati.add(docSelectedForResponse);
		}

		boolean firmaPdfVisible = true;
		boolean firmaCADES = true;
		for (final MasterDocumentRedDTO docSelezionato : documentiSelezionati) {
			if (Boolean.FALSE.equals(docSelezionato.getFlagFirmaPDF())) {
				firmaPdfVisible = false;
				break;
			}
		}
		for (final MasterDocumentRedDTO docSelezionato : documentiSelezionati) {
			if (docSelezionato.getIconaStampigliaturaSegnoGrafico() != null) {
				firmaCADES = false;
				break;
			}
		}
		if (firmaCADES) {
			for (final MasterDocumentRedDTO docSelezionato : documentiSelezionati) {
				if (documentSRV.checkIsFlussiAutUCB(docSelezionato.getDocumentTitle(), null, utente)) {
					firmaCADES = false;
					break;
				}
			}
		}
		if (firmaPdfVisible) {
			signTypeList.add("PDF");
		}
		
		if (firmaCADES) {
			signTypeList.add("P7M");
		}
		return !signTypeList.isEmpty();
	}
	/**
	 * Esegue la firma in base alla selezione del tipo in maschera.
	 */
	public void selezionaFirmaEdEsegui() {
		try {
			errorePIN = false;
			pinBloccato = false;
			erroreOTP = false;

			if ("P7M".equalsIgnoreCase(selectedSignTypeFirmaRemota)) {
				doCadesRemota();
			} else if ("PDF".equalsIgnoreCase(selectedSignTypeFirmaRemota)) {
				doPadesRemota();
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	private void aggiornaUtenteFirma() {
		try {
			final Long idUtente = utente.getId();
			// salvo la verifica del pin
			if (utenteSRV.aggiornaUtenteFirma(idUtente, "PIN_VERIFICATO", Boolean.TRUE.equals(errorePIN) ? "0" : "1") == 1) {
				utenteFirma.setPinVerificato(errorePIN);
				// se il pin è verificato lo salvo
				if (Boolean.TRUE.equals(memorizzaPin) && Boolean.FALSE.equals(errorePIN) && utenteSRV.aggiornaUtenteFirma(idUtente, "PIN", pin) == 1) {
					// salva il pin dell'utente firma e aggiorno l'account
					utenteFirma.setPin(pin);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore nell'aggiornamento dell utente firma", e);
			showError(e);
		}
	}

	private void doCadesRemota() {
		signType = SignTypeEnum.CADES;
		doFirmaRemota();
	}

	private void doPadesRemota() {
		signType = SignTypeEnum.PADES_VISIBLE;
		doFirmaRemota();
	}

	private Collection<String> getSelectedWobNumbers() {
		final Collection<String> output = new ArrayList<>();
		for (final MasterDocumentRedDTO master : documentiDTH.getSelectedMasters()) {
			output.add(master.getWobNumber());
		}
		return output;
	}

	/**
	 * Esecuzione della firma remota.
	 */
	private void doFirmaRemota() {
		String confirmMessage = "";

		if (documentiDTH.getSelectedMasters() != null && !documentiDTH.getSelectedMasters().isEmpty()) {
			confirmMessage = signSRV.checkProtocolsPreSign(documentiDTH.getSelectedMasters());
		} else {
			final List<MasterDocumentRedDTO> documentiSelezionati = new ArrayList<>();
			documentiSelezionati.add(docSelectedForResponse);
			confirmMessage = signSRV.checkProtocolsPreSign(documentiSelezionati);
		}
		if (!StringUtils.isNullOrEmpty(confirmMessage)) {
			confirmMessageFirmaRemota = confirmMessage;
			FacesHelper.executeJS("PF('wdgConfermaFirmaRemota').show()");
			FacesHelper.update("centralSectionForm:wdgConfermaFirmaRemota_id");
		} else {
			confirmDoFirmaRemota();
		}
	}

	private Boolean checkDestintariSoloInterni(final List<MasterDocumentRedDTO> docList) {
		Boolean output = Boolean.FALSE;
		try {
			output = signSRV.checkDocumentsHasSoloDestinatariInterni(utente, docList);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
		return output;
	}

	/**
	 * Gestisce l'esito delle operazioni di firma.
	 * 
	 * @param esiti
	 */
	private void gestisciEsisti(final Collection<EsitoOperazioneDTO> esiti) {

		boolean returnEsiti = true;
		for (final EsitoOperazioneDTO esito : esiti) {
			if (!esito.isEsito() && esito.getCodiceErrore() != null) {
				if (esito.getCodiceErrore().equals(SignErrorEnum.INVALID_PIN_FIRMA_COD_ERROR)
						|| esito.getCodiceErrore().equals(SignErrorEnum.INVALID_PIN_LOCKED_FIRMA_COD_ERROR)) {

					errorePIN = true;
					if (esito.getCodiceErrore().equals(SignErrorEnum.INVALID_PIN_LOCKED_FIRMA_COD_ERROR)) {
						pinBloccato = true;
					}
					returnEsiti = false;
				} else if (esito.getCodiceErrore().equals(SignErrorEnum.INVALID_OTP_FIRMA_COD_ERROR)) {
					erroreOTP = true;
					returnEsiti = false;
				}
				
				// è inutile che ciclo, se ho uno di questi errori, torno direttamente al
				// chiamante che gestirà questo tipo di errori
				if (!returnEsiti) {
					break;
				}
			}
		}
		if (Boolean.TRUE.equals(erroreOTP)) {
			showError("L'OTP inserito è errato, riprovare nuovamete");
		} else if (Boolean.TRUE.equals(errorePIN) && Boolean.TRUE.equals(pinBloccato)) {
			showError("Attenzione è stato inserito per tre volte consecutive un PIN errato." + " Attendere alcuni minuti per lo sblocco automatico del PIN");
		} else if (Boolean.TRUE.equals(errorePIN)) {
			showError("Il PIN inserito è errato, riprovare nuovamete");
		}
		dataEoraEsito = null;
		if (returnEsiti) {
			dataEoraEsito = new Date();
			esitiOperazione.addAll(esiti);
			FacesHelper.executeJS(SHOW_DLGSHOWRESULT_JS);
			FacesHelper.update(DLGSHOWRESULT_RESOURCE_ID);
		}
	}

	/**
	 * Check campi firma e prosecuzione della stessa in seguito all'input
	 * delll'utente tramite dialog.
	 */
	public void confirmDoFirmaRemota() {
		if (StringUtils.isNullOrEmpty(otp) && StringUtils.isNullOrEmpty(pin)) {
			showWarnMessage("Inserire i campi obbligatori.");
			return;
		}
		if (utente.getShowDialogAoo()) {
			confirmMessageFirmaRemota = WARNING_DOCUMENTO_CHIUSO_MSG;
			FacesHelper.executeJS("PF('confirmFirmaRemota_VW').show();");
			FacesHelper.update("centralSectionForm:confirmFirmaRemota_PNL");
		} else {
			confirmDoFirmaRemotaContinua();
		}
	}

	/**
	 * Esegue ulteriori check sulla firma remota.
	 */
	public void confirmDoFirmaRemotaContinua() {
		// check firma visibile p7m
		if (!firmaMultipla && signType.equals(SignTypeEnum.CADES)) {
			final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final List<MasterDocumentRedDTO> masters = new ArrayList<>();
			if (documentiDTH.getSelectedMasters() != null && !documentiDTH.getSelectedMasters().isEmpty()) {
				masters.addAll(documentiDTH.getSelectedMasters());
			} else {
				masters.add(docSelectedForResponse);
			}
			Boolean isFirmaVisibile = null;
			for (final MasterDocumentRedDTO principaleCorrente : masters) {
				final DocumentSet allegati = fceh.getAllegatiConContentDaFirmare(principaleCorrente.getGuuid());
				final Iterator<?> allegatiIterator = allegati.iterator();
				while (allegatiIterator.hasNext()) {
					final Document allegato = (Document) allegatiIterator.next();
					isFirmaVisibile = (Boolean) TrasformerCE.getMetadato(allegato, PropertiesNameEnum.FIRMA_VISIBILE_METAKEY);

					if (Boolean.TRUE.equals(isFirmaVisibile)) {
						FacesHelper.executeJS("PF('confirmFirmaRemotaP7M').show();");
						FacesHelper.update("centralSectionForm:idConfirmFirmaRemotaP7M");
						break;
					}
				}
				if (Boolean.TRUE.equals(isFirmaVisibile)) {
					break;
				}
			}
			if (!Boolean.TRUE.equals(isFirmaVisibile)) {
				proseguiFirmaRemota();
			}
		} else {
			proseguiFirmaRemota();
		}
	}

	/**
	 * Predispone l'esecuzione della firma remota.
	 */
	public void proseguiFirmaRemota() {
		final Collection<String> wobNumbers = new ArrayList<>();
		wobNumbersContinuaAftLock = new ArrayList<>();
		boolean azioniMassive = false;
		if (documentiDTH.getSelectedMasters() != null && !documentiDTH.getSelectedMasters().isEmpty()) {
			wobNumbers.addAll(getSelectedWobNumbers());
			azioniMassive = true;
		} else {
			wobNumbers.add(docSelectedForResponse.getWobNumber());
			azioniMassive = false;
		}
		wobNumbersContinuaAftLock = wobNumbers;

		// ### Si procede alla firma dei documenti
		boolean principaleOnlyPAdESVisible = true;
		final IAooFacadeSRV aooSRV = ApplicationContextProvider.getApplicationContext().getBean(IAooFacadeSRV.class);
		final Aoo aoo = aooSRV.recuperaAoo(utente.getIdAoo().intValue());
		// Se l'AOO ha configurato il PK Handler per i Timbri, è possibile anche la
		// firma invisibile del documento principale
		// (nel caso in cui tale documento non abbia campi firma vuoti)
		setPrincipaleOnlyPAdESVisibleAftLock(principaleOnlyPAdESVisible);
		if (aoo.getPkHandlerTimbro() != null) {
			principaleOnlyPAdESVisible = false;
			setPrincipaleOnlyPAdESVisibleAftLock(principaleOnlyPAdESVisible);
		}
		// ### Si chiama il service per l'esecuzione della Firma Remota
		Boolean nessunLockPresente = true;
		GestioneLockDTO gestLockDTO = null;

		if (azioniMassive) {
			final List<Integer> docTitleList = new ArrayList<>();
			for (final MasterDocumentRedDTO mast : documentiDTH.getSelectedMasters()) {
				docTitleList.add(Integer.parseInt(mast.getDocumentTitle()));
			}
			gestLockDTO = documentoSRV.getLockDocumentoMassivo(docTitleList, utente.getIdAoo(), 1, utente.getId());
			nessunLockPresente = gestLockDTO.getNessunLock();
			if (nessunLockPresente != null && !nessunLockPresente) {
				nessunLockPresente = gestLockDTO.isInCarico();
			}
			checkLock(wobNumbers, nessunLockPresente, gestLockDTO);
		} else {
			gestLockDTO = documentoSRV.getLockDocumento(Integer.valueOf(docSelectedForResponse.getDocumentTitle()), utente.getIdAoo(), 1, utente.getId());
			nessunLockPresente = gestLockDTO.getNessunLock();
			if (nessunLockPresente != null && !nessunLockPresente) {
				nessunLockPresente = gestLockDTO.isInCarico();
			}
			checkLock(wobNumbers, nessunLockPresente, gestLockDTO);
		}
	}

	private void checkLock(final Collection<String> wobNumbers, final Boolean nessunLockPresente,
			final GestioneLockDTO gestLockDTO) {
		if (nessunLockPresente != null && !nessunLockPresente) {
			nomeUtenteLockatore = gestLockDTO.getUsernameUtenteLock();
			FacesHelper.update("centralSectionForm:sbloccaDocumentoFirmaDlg");
			FacesHelper.executeJS("PF('sbloccaDocumentoFirmaDlg_WV').show();");
		} else {
			continuaFirmaAfterCheckLock(wobNumbers);
		}
	}

	/**
	 * Check lock e continuazione firma.
	 */
	public void continuaFirmaAfterCheckLock() {
		continuaFirmaAfterCheckLock(wobNumbersContinuaAftLock);
	}

	private void continuaFirmaAfterCheckLock(final Collection<String> wobNumbers) {
		Collection<EsitoOperazioneDTO> esiti = null;
		if(!firmaMultipla && NavigationTokenEnum.LIBRO_FIRMA_DELEGATO.equals(sessionBean.getActivePageInfo()) && glifoSelezionatoRemota!=null) {
			UtenteDTO utenteDelegato = new UtenteDTO(utente);
			SignerInfoDTO siUtenteDelegato = new SignerInfoDTO(utenteDelegato.getSignerInfo());
			siUtenteDelegato.setImageFirma(glifoSelezionatoRemota);
			utenteDelegato.setSignerInfo(siUtenteDelegato);
			esiti = aSignSRV.firmaRemota(wobNumbers, pin, otp, utenteDelegato, signType, firmaMultipla);
		} else {
			esiti = aSignSRV.firmaRemota(wobNumbers, pin, otp, utente, signType, firmaMultipla);
		}
		
		mostraDocumentiFirmatiEsito = true;
		final Collection<EsitoOperazioneDTO> esitiReturn = new ArrayList<>();
		for (final EsitoOperazioneDTO ePrincipale : esiti) {
			esitiReturn.add(ePrincipale);
			if (ePrincipale.getEsitiAllegati() != null && !ePrincipale.getEsitiAllegati().isEmpty()) {
				for (final EsitoOperazioneDTO eAllegato : ePrincipale.getEsitiAllegati()) {
					eAllegato.setAllegato(true);
					eAllegato.setIdDocumento(ePrincipale.getIdDocumento());
					esitiReturn.add(eAllegato);
				}
			}
		}
		if (Boolean.TRUE.equals(utente.getSavePinAoo())) {
			aggiornaUtenteFirma();
		}
		gestisciEsisti(esitiReturn);

		if (Boolean.FALSE.equals(erroreOTP) && Boolean.FALSE.equals(errorePIN)) {
			hideDialogFirmaRemota();
		}
	}
 
	/**
	 * Switch utilizzato dal component per la protocollazione in Stato di Emergenza
	 * per determinare quale response è stata attivata e invocare di conseguenza la
	 * logica di business dedicata.
	 */
	public void switchActionProtEmergenza() {
		if (ResponsesRedEnum.FIRMA_AUTOGRAFA.equals(selectedResponse)) {
			confirmDoFirmaAutografa();
		} else if (ResponsesRedEnum.FIRMATO.equals(selectedResponse)) {
			confirmResponseFirmato();
		} else if (ResponsesRedEnum.FIRMATO_E_SPEDITO.equals(selectedResponse)) {
			confirmResponseFirmatoSpedito();
		}
	}

	/**
	 * Attivazione della response di firma autografa.
	 */
	@BeanTask(name = "firmaAutografa")
	public void responseFirmaAutografa() {
		final List<MasterDocumentRedDTO> documentiSelezionati = new ArrayList<>();
		protEm = new ProtocolloEmergenzaDocDTO();
		cleanEsiti();
		boolean isRepertoriabile = Boolean.FALSE;
		firmaAutografaConfirmMessage = "";
		dlgBeforeResponse = false;

		if (documentiDTH.getSelectedMasters() != null && !documentiDTH.getSelectedMasters().isEmpty()) {
			// provengo da azioni massive
			documentiSelezionati.addAll(documentiDTH.getSelectedMasters());
		} else {
			// provengo da azione singola
			documentiSelezionati.add(docSelectedForResponse);
		}
		for (final MasterDocumentRedDTO m : documentiSelezionati) {
			// Il documento deve avere il flag del Repertorio e non deve essere già
			// protocollato.
			if (m.getIsModalitaRegistroRepertorio() != null && m.getIsModalitaRegistroRepertorio() && (m.getNumeroProtocollo() == null || m.getNumeroProtocollo() <= 0)) {
				isRepertoriabile = Boolean.TRUE;
			} else {
				isRepertoriabile = Boolean.FALSE;
				break;
			}
		}

		final String errorMessage = signSRV.checkErrorDocumentsPreSignAutografa(utente, documentiSelezionati);

		if (isRepertoriabile && Boolean.FALSE.equals(checkDestintariSoloInterni(documentiSelezionati))) {
			// Il documento è un Registro di Repertorio ma i Destinatari non sono solo
			// Interni.
			FacesHelper.executeJS(SHOW_WDGDLGAVVISODESTINTERNI_JS);
		} else if (!StringUtils.isNullOrEmpty(errorMessage)) {
			// almeno un documento fra quelli selezionati non è valido per la firma
			showError(errorMessage);
		} else {
			final String confirmMessage = signSRV.checkDocumentsPreSignAutografa(utente, documentiSelezionati);
			if (!StringUtils.isNullOrEmpty(confirmMessage)) {
				dlgBeforeResponse = true;
				firmaAutografaConfirmMessage = confirmMessage;
				PrimeFaces.current().executeScript(SHOW_DLG_GENERIC_JS);
				FacesHelper.update("centralSectionForm:firmaAutografa_PNL");
			} else {
				if (utente.getShowDialogAoo()) {
					dlgBeforeResponse = true;
					firmaAutografaConfirmMessage = WARNING_DOCUMENTO_CHIUSO_MSG;
					FacesHelper.executeJS("PF('confirmFirma_VW').show();");
					FacesHelper.update("centralSectionForm:confirmFirma_PNL");
				} else {
					checkStatoProtEmergenzaContinue();
				}
			}
		}
	}

	/**
	 * Informa l'utente sullo stato del documento e prosegue.
	 */
	public void checkStatoProtEmergenza() {
		if (utente.getShowDialogAoo()) {
			firmaAutografaConfirmMessage = WARNING_DOCUMENTO_CHIUSO_MSG;
			FacesHelper.executeJS("PF('confirmFirma_VW').show();");
			FacesHelper.update("centralSectionForm:confirmFirma_PNL");
		} else {
			checkStatoProtEmergenzaContinue();
		}
	}

	/**
	 * Predispone la firma autografa in modalità emerganza.
	 */
	public void checkStatoProtEmergenzaContinue() {
		boolean isProtocolloPresent = false;
		final boolean isEmergenza = TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo());
		Boolean isRegistroRepertorio = Boolean.FALSE;

		if (!isEmergenza) {
			confirmDoFirmaAutografa();
		} else {
			// preparazione dati per firma Autografa
			if (documentiDTH.getSelectedMasters() != null && !documentiDTH.getSelectedMasters().isEmpty()) {
				// prendo solo il primo item, perchè se sono più item il controllo bloccante è
				// stato eseguito in precedenza
				if (documentiDTH.getSelectedMasters().get(0).getNumeroProtocollo() != null && documentiDTH.getSelectedMasters().get(0).getNumeroProtocollo() > 0) {
					// azione multipla
					isProtocolloPresent = true;
					isRegistroRepertorio = documentiDTH.getSelectedMasters().get(0).getIsModalitaRegistroRepertorio();
				}

			} else {
				isProtocolloPresent = checkNumProtIsPresent();
				isRegistroRepertorio = docSelectedForResponse.getIsModalitaRegistroRepertorio();
			}

			if (isRegistroRepertorio != null && isRegistroRepertorio) {

				showWarnMessage(WARNING_REGISTRO_REPERTORIO_PROTOCOLLAZIONE_EMERGENZA_MSG);

			} else if (!isProtocolloPresent) {
				dlgBeforeResponse = true;

				protEm = new ProtocolloEmergenzaDocDTO();
				protEm.setAnnoProtocolloEmergenza(Calendar.getInstance().get(Calendar.YEAR));
				protEm.setDataProtocolloEmergenza(new Date());

				FacesHelper.update(PNLPROTEMR_RESOURCE_ID);
				PrimeFaces.current().executeScript(SHOW_WDGPROTEMERGENZA_JS);
				PrimeFaces.current().executeScript(DISABLE_WDGPROTEMERGBTN_JS);
			} else {
				confirmDoFirmaAutografa();
			}
		}
	}

	/**
	 * Esegue la firma autografa.
	 */
	public void confirmDoFirmaAutografa() {
		final List<MasterDocumentRedDTO> documentiSelezionati = new ArrayList<>();
		final Collection<String> wobNumbers = new ArrayList<>();
		try {
			// Preparazione dati per Firma Autografa
			if (documentiDTH.getSelectedMasters() != null && !documentiDTH.getSelectedMasters().isEmpty()) {
				// Azione multipla
				documentiSelezionati.addAll(documentiDTH.getSelectedMasters());
				wobNumbers.addAll(getSelectedWobNumbers());
			} else {
				// Azione singola
				documentiSelezionati.add(docSelectedForResponse);
				wobNumbers.add(docSelectedForResponse.getWobNumber());
			}
			// Converti destinatari elettronici in cartacei
			signSRV.convertiDestinatariElettroniciInCartacei(utente, documentiSelezionati);
			
			// Firma documenti
			esitiOperazione = new ArrayList<>();
			
			Collection<EsitoOperazioneDTO> esitiOp = aSignSRV.firmaAutografa(wobNumbers, utente, protEm);
			
			mostraDocumentiFirmatiEsito = true;
			for (final EsitoOperazioneDTO ePrincipale : esitiOp) {
				esitiOperazione.add(ePrincipale);
				if (ePrincipale.getEsitiAllegati() != null && !ePrincipale.getEsitiAllegati().isEmpty()) {
					for (final EsitoOperazioneDTO eAllegato : ePrincipale.getEsitiAllegati()) {
						eAllegato.setAllegato(true);
						eAllegato.setIdDocumento(ePrincipale.getIdDocumento());
						esitiOperazione.add(eAllegato);
					}
				}
			}
			if (esitiOperazione != null && !esitiOperazione.isEmpty() && !esitiOperazione.contains(null)) {
				mostraDocumentiFirmatiEsito = true;
				
				// Se è stata mostrata una dlg prima della firma richiamo il js per mostrare la dlg dei risultati,
				// altrimenti se ne occupa il meccanismo (goToExecute/single)				
				if (dlgBeforeResponse) {
					// Aggiorno la dialog contenente gli esiti
					FacesHelper.update(DLGSHOWRESULT_RESOURCE_ID);
					// Richiamo la visualizzazione della dialog
					FacesHelper.executeJS(SHOW_DLGSHOWRESULT_JS);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), new Exception(e));
			showError(e);
		}
	}

	/**
	 * Attivazione della response "Firmato".
	 */
	@BeanTask(name = "firmato")
	public void responseFirmato() {
		protEm = new ProtocolloEmergenzaDocDTO();
		cleanEsiti();
		dlgBeforeResponse = false;

		if (docSelectedForResponse.getIsModalitaRegistroRepertorio() != null && docSelectedForResponse.getIsModalitaRegistroRepertorio()
				&& TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo())) {

			showWarnMessage(
					"Attenzione! Il documento risulta essere un Registro di Repertorio, ed essendo attiva la Protocollazione d'Emergenza non Ã¨ possibile proseguire con l'operazione");

		} else if (TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo()) && Boolean.FALSE.equals(checkNumProtIsPresent())) {
			// Se il protocollo è in stato di Emergenza mostra dialog
			dlgBeforeResponse = true;

			protEm = new ProtocolloEmergenzaDocDTO();
			protEm.setAnnoProtocolloEmergenza(Calendar.getInstance().get(Calendar.YEAR));
			protEm.setDataProtocolloEmergenza(new Date());

			FacesHelper.update(PNLPROTEMR_RESOURCE_ID);
			PrimeFaces.current().executeScript(SHOW_WDGPROTEMERGENZA_JS);
			PrimeFaces.current().executeScript(DISABLE_WDGPROTEMERGBTN_JS);

		} else {
			confirmResponseFirmato();
		}
	}

	private boolean checkNumProtIsPresent() {
		boolean output = false;
		if (docSelectedForResponse.getNumeroProtocollo() != null && docSelectedForResponse.getNumeroProtocollo() > 0) {
			// azione singola
			output = true;
		}
		return output;
	}

	/**
	 * Esegue la response "Firmato".
	 */
	public void confirmResponseFirmato() {
		try {

			final EsitoOperazioneDTO esito = firmatoSpeditoSRV.firmato(utente, docSelectedForResponse.getWobNumber(), protEm);

			if (esito != null) {
				esitiOperazione.add(esito);
			}

			// Se è stata mostrata una dlg prima della firma richiamo il js per mostrare la
			// dlg dei risultati
			// Altrimenti se ne occupa il meccanismo (goToExecute/single)
			if (esitiOperazione != null && !esitiOperazione.isEmpty() && !esitiOperazione.contains(null) && dlgBeforeResponse) {

				// aggiorno la dialog contenente gli esiti
				FacesHelper.update(DLGSHOWRESULT_RESOURCE_ID);
				// richiamo la visualizzazione della dialog
				FacesHelper.executeJS(SHOW_DLGSHOWRESULT_JS);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), new Exception(e));
			showError(e);
		}
	}

	/**
	 * Attivazione della response "Firmato e Spedito".
	 */
	@BeanTask(name = "firmatoESpedito")
	public void responseFirmatoSpedito() {
		protEm = new ProtocolloEmergenzaDocDTO();
		cleanEsiti();
		dlgBeforeResponse = false;
		// Se il protocollo è in stato di Emergenza mostra dialog

		if (docSelectedForResponse.getIsModalitaRegistroRepertorio() != null && docSelectedForResponse.getIsModalitaRegistroRepertorio()
				&& TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo())) {

			showWarnMessage(WARNING_REGISTRO_REPERTORIO_PROTOCOLLAZIONE_EMERGENZA_MSG);

		} else if (TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo()) && Boolean.FALSE.equals(checkNumProtIsPresent())) {
			dlgBeforeResponse = true;

			protEm = new ProtocolloEmergenzaDocDTO();
			protEm.setAnnoProtocolloEmergenza(Calendar.getInstance().get(Calendar.YEAR));
			protEm.setDataProtocolloEmergenza(new Date());

			FacesHelper.update(PNLPROTEMR_RESOURCE_ID);
			PrimeFaces.current().executeScript(SHOW_WDGPROTEMERGENZA_JS);
			PrimeFaces.current().executeScript(DISABLE_WDGPROTEMERGBTN_JS);

		} else {
			confirmResponseFirmatoSpedito();
		}
	}

	/**
	 * Esegue la response "Firmato e Spedito".
	 */
	public void confirmResponseFirmatoSpedito() {
		try {

			final EsitoOperazioneDTO esito = firmatoSpeditoSRV.firmatoESpedito(utente, docSelectedForResponse.getWobNumber(), protEm);

			if (esito != null) {
				esitiOperazione.add(esito);
			}

			// Se è stata mostrata una dlg prima della firma richiamo il js per mostrare la
			// dlg dei risultati
			// Altrimenti se ne occupa il meccanismo (goToExecute/single)
			if (esitiOperazione != null && !esitiOperazione.isEmpty() && !esitiOperazione.contains(null) && dlgBeforeResponse) {

				// aggiorno la dialog contenente gli esiti
				FacesHelper.update(DLGSHOWRESULT_RESOURCE_ID);
				// richiamo la visualizzazione della dialog
				FacesHelper.executeJS(SHOW_DLGSHOWRESULT_JS);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), new Exception(e));
			showError(e);
		}
	}

	/**
	 * Predispone l'esecuzione della response "Predisponi Documento".
	 */
	@BeanTask(name = "predisponiDocumento")
	public void predisponiDocumento() {
		predisponiSingoloOMulti(ResponsesRedEnum.PREDISPONI_DOCUMENTO);
	}

	/**
	 * Predispone l'esecuzione della response "Predisponi Ingresso".
	 */
	@BeanTask(name = "rispondiIngresso")
	public void rispondiIngresso() {
		predisponiSingoloOMulti(ResponsesRedEnum.RISPONDI_INGRESSO);
	}

	/**
	 * Predispone i documento selezionato all'esecuzione della response in input.
	 * 
	 * @param response
	 */
	private void predisponiSingoloOMulti(final ResponsesRedEnum response) {
		if (documentiDTH.getSelectedMasters() != null && !documentiDTH.getSelectedMasters().isEmpty()) {
			// Se provengo da azioni massive
			final PredisponiDocumentoBean p = (PredisponiDocumentoBean) FacesHelper.getManagedBean(ConstantsWeb.MBean.PREDISPONI_DOCUMENTO_BEAN);
			p.setAllaciFromMasters(documentiDTH.getSelectedMasters(), response);
			if (documentiDTH.getSelectedMasters().size() == 1) {
				p.predisponi();
			} else {
				FacesHelper.executeJS(SHOW_DLG_GENERIC_JS);
				FacesHelper.update("centralSectionForm:idPredisponiDocTable_DLGP");
			}
		} else {
			// Se provengo da azione singola
			final PredisponiDocumentoBean p = (PredisponiDocumentoBean) FacesHelper.getManagedBean(ConstantsWeb.MBean.PREDISPONI_DOCUMENTO_BEAN);
			p.setAllaciFromMasters(Arrays.asList(docSelectedForResponse), response);
			p.predisponi();
		}
	}

	/**
	 * Esegue l'inoltro del doc in ingresso predisponendone il documento in uscita.
	 */
	@BeanTask(name = "inoltraIngresso")
	public void inoltraIngresso() {
		final PredisponiDocumentoHelper pdh = new PredisponiDocumentoHelper(docSelectedForResponse, null, ResponsesRedEnum.INOLTRA_INGRESSO, utente);

		// Si predispone il documento in uscita per l'inoltro
		pdh.predisponiUscita();

		if (pdh.getEsitoPredisposizione().isEsito()) {
			// Si procede all'apertura della dialog di creazione del documento predisposto
			pdh.apriDialogCreazioneDocPredisposto();
		} else {
			showError(pdh.getEsitoPredisposizione().getNote());
		}
	}

	/**
	 * Init della firma digitale.
	 */
	@BeanTask(name = "firmaDigitale")
	public void initFirmaDigitale() {
		firmaMultipla = false;
		gestisciInitFirmaDigitale();
	}

	/**
	 * Init della firma digitale multipla.
	 */
	@BeanTask(name = "firmaDigitaleMultipla")
	public void initFirmaDigitaleMultipla() {
		firmaMultipla = true;
		gestisciInitFirmaDigitale();
	}

	private void gestisciInitFirmaDigitale() {
		final List<MasterDocumentRedDTO> documentiSelezionati = new ArrayList<>();
		boolean isRepertoriabile = Boolean.FALSE;

		try {
			// svuoto gli esiti nel caso siano valorizzati
			cleanEsiti();

			if (documentiDTH.getSelectedMasters() != null && !documentiDTH.getSelectedMasters().isEmpty()) {
				// provengo da azioni massive
				documentiSelezionati.addAll(documentiDTH.getSelectedMasters());
			} else {
				// provengo da azione singola
				documentiSelezionati.add(docSelectedForResponse);
			}

			for (final MasterDocumentRedDTO m : documentiSelezionati) {
				// Il documento deve avere il flag del Repertorio e non deve essere giÃ 
				// protocollato.
				if (m.getIsModalitaRegistroRepertorio() != null && m.getIsModalitaRegistroRepertorio() && (m.getNumeroProtocollo() == null || m.getNumeroProtocollo() <= 0)) {
					isRepertoriabile = Boolean.TRUE;
				} else {
					isRepertoriabile = Boolean.FALSE;
					break;
				}
			}
			// Caso 1: Registro Emergenza Attivo ma il documento è un Registro di
			// Repertorio.
			if (isRepertoriabile && TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo())) {

				showWarnMessage(WARNING_REGISTRO_REPERTORIO_PROTOCOLLAZIONE_EMERGENZA_MSG);

			}
			if (isRepertoriabile && Boolean.FALSE.equals(checkDestintariSoloInterni(documentiSelezionati))) {
				// Caso 2: Il documento è un Registro di Repertorio ma i Destinatari non sono
				// solo Interni.
				FacesHelper.executeJS(SHOW_WDGDLGAVVISODESTINTERNI_JS);

			} else if (utente.getSignerInfo().getImageFirma() == null || utente.getSignerInfo().getImageFirma().length <= 0) {
				// Si controlla se l'utente ha il glifo
				FacesHelper.showMessage(Constants.EMPTY_STRING, "Impossibile procedere con la firma a causa della mancanza del glifo di firma dell'utente.",
						MessageSeverityEnum.ERROR);
			} else {
				showConfirmMessageFirmaRemota = false;
				flagConfermaFirma = false;

				final String confirmMessage = signSRV.checkApprovazioniDaNonStampigliare(documentiSelezionati, utente);
				if (!StringUtils.isNullOrEmpty(confirmMessage)) {
					showConfirmMessageFirmaApprovazioni = true;
					confirmMessageFirmaApprovazioni = confirmMessage;
				}
				controlloPreFirmaDigitale();
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Check pre-firma digitale.
	 */
	public void controlloPreFirmaDigitale() {
		if (utente.getShowDialogAoo()) {
			confirmMessageFirmaDig = WARNING_DOCUMENTO_CHIUSO_MSG;
			FacesHelper.executeJS("PF('confirmFirmaDig_VW').show();");
			FacesHelper.update("centralSectionForm:confirmFirmaDig_PNL");
		} else {
			continuaFirmaDigitale();
		}
	}

	/**
	 * Continua l'iter di firma digitale.
	 */
	public void continuaFirmaDigitale() {
		final List<MasterDocumentRedDTO> documentiSelezionati = new ArrayList<>();

		if (documentiDTH.getSelectedMasters() != null && !documentiDTH.getSelectedMasters().isEmpty()) {
			// Azione multipla
			documentiSelezionati.addAll(documentiDTH.getSelectedMasters());
		} else {
			// Azione singola
			documentiSelezionati.add(docSelectedForResponse);
		}
		// verifico che tutti i documenti selezionati siano validi per la firma digitale
		final String errorMessage = signSRV.checkDocumentsPreSign(utente, documentiSelezionati);

		if (!StringUtils.isNullOrEmpty(errorMessage)) {
			// nel caso si sia passati per il messaggio di conferma per l'errore delle
			// approvazioni
			PrimeFaces.current().executeScript("PF('dlgFirmaDig').hide()");
			PrimeFaces.current().executeScript("PF('actionMenu').hide()");
			// se almeno un documento fra quelli selezionati non è valido per la firma
			showError(errorMessage);
		} else {
			
			// Verifico che non siano presenti dei protocolli tra i documenti selezionati
			flagConfermaFirma = !StringUtils.isNullOrEmpty(signSRV.checkProtocolsPreSign(documentiSelezionati));
			
			// Abilito il panel che permette la scelta del tipo di firma (PDF/P7M)
			showConfirmMessageFirmaRemota = false;
			// Verifico che su tutti i documenti selezionati sia possibile effettuare la firma PAdES, altrimenti mostro solo la CAdES
			boolean hasFirme = loadComboboxTipoFirma();
			if(hasFirme) {
				if (NavigationTokenEnum.LIBRO_FIRMA_DELEGATO.equals(sessionBean.getActivePageInfo())) {
					if(tipologieGlifoFirma == null || tipologieGlifoFirma.isEmpty()) {
						tipologieGlifoFirma = signSRV.recuperaGlifoDelegato(utente);	
					}			
					glifoSelezionatoDigitale = null;	
					onSelectGlifoFirmaDigitale();
				}
				//se il check è positivo mostro la scela del tipo firma
				FacesHelper.executeJS("PF('dlgFirmaDig').show()");
				FacesHelper.update("centralSectionForm:idDlgFirmaDig");
			} else {
				showWarnMessage("I documenti selezionati non hanno un tipo di firma omogeneo. "
						+ "É necessario, per procedere con la firma massiva, selezionare documenti che consentano un tipo di firma omogeneo (PDF e/o P7M).");
			}
		}
	}

	/**
	 * Metodo utilizzato per reperire i dati necessari per fa partire l'applet di
	 * firma.
	 */
	public void startAppletFirma() {
		SignTypeEnum ste = null;
		
		// Recupero dati dai master selezionati necessari per l'avvio della firma 
		if (documentiDTH.getSelectedMasters() != null && !documentiDTH.getSelectedMasters().isEmpty()) {
			for (final MasterDocumentRedDTO m : documentiDTH.getSelectedMasters()) {

				final MasterDocumentRedDTO doc = new MasterDocumentRedDTO();
				doc.setWobNumber(m.getWobNumber());
				doc.setDocumentTitle(m.getDocumentTitle());
				doc.setNumeroDocumento(m.getNumeroDocumento());
				doc.setGuuid(m.getGuuid());
				doc.setSottoCategoriaDocumentoUscita(m.getSottoCategoriaDocumentoUscita());
				docsToSign.add(doc);
			}
		} else {
			final MasterDocumentRedDTO doc = new MasterDocumentRedDTO();
			doc.setWobNumber(docSelectedForResponse.getWobNumber());
			doc.setDocumentTitle(docSelectedForResponse.getDocumentTitle());
			doc.setNumeroDocumento(docSelectedForResponse.getNumeroDocumento());
			doc.setGuuid(docSelectedForResponse.getGuuid());
			doc.setSottoCategoriaDocumentoUscita(docSelectedForResponse.getSottoCategoriaDocumentoUscita());

			docsToSign.add(doc);
		}
		// Definisco che tipo di firma è stata scelta
		if ("P7M".equalsIgnoreCase(selectedSignType)) {
			ste = SignTypeEnum.CADES;
			
			if (alertFirmaVisibile(docsToSign)) {
				//termino il metodo per delegare alla dialog la continuazione alla scelta dell'utente 
				return;
			}
		} else if ("PDF".equalsIgnoreCase(selectedSignType)) {
			ste = SignTypeEnum.PADES_VISIBLE;
		}
		startLocalSignHelper4Digitale(ste);
	}

	/**
	 * Inizializza l'helper di firma locale digitale.
	 * 
	 * @param ste
	 */
	public void startLocalSignHelper4Digitale(final SignTypeEnum ste) {
		if(NavigationTokenEnum.LIBRO_FIRMA_DELEGATO.equals(sessionBean.getActivePageInfo()) && glifoSelezionatoDigitale!=null) {
			UtenteDTO utenteDelegato = new UtenteDTO(utente);
			SignerInfoDTO siUtenteDelegato = new SignerInfoDTO(utenteDelegato.getSignerInfo());
			siUtenteDelegato.setImageFirma(glifoSelezionatoDigitale);
			utenteDelegato.setSignerInfo(siUtenteDelegato);
			startLocalSignHelper(VIEWS_CODA_CODA_JSF, ste, docsToSign, utenteDelegato, false, firmaMultipla);
		} else {
			startLocalSignHelper(VIEWS_CODA_CODA_JSF, ste, docsToSign, utente, false, firmaMultipla);	
		}
		
		docsToSign = new ArrayList<>();
	}

	private boolean alertFirmaVisibile(final List<MasterDocumentRedDTO> masters) {
		final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
		Boolean isFirmaVisibile = null;
		for (final MasterDocumentRedDTO principaleCorrente : masters) {
			final DocumentSet allegati = fceh.getAllegatiConContentDaFirmare(principaleCorrente.getGuuid());
			final Iterator<?> allegatiIterator = allegati.iterator();
			while (allegatiIterator.hasNext()) {
				final Document allegato = (Document) allegatiIterator.next();
				isFirmaVisibile = (Boolean) TrasformerCE.getMetadato(allegato, PropertiesNameEnum.FIRMA_VISIBILE_METAKEY);

				if (Boolean.TRUE.equals(isFirmaVisibile)) {
					FacesHelper.executeJS("PF('confirmFirmaDigitaleP7M').show();");
					FacesHelper.update("centralSectionForm:idConfirmFirmaDigitaleP7M");
					break;
				}
			}
			if (Boolean.TRUE.equals(isFirmaVisibile)) {
				break;
			}
		}
		return isFirmaVisibile;
	}

	/**
	 * Attivazione della response "Attesta Copia Conforme".
	 */
	@BeanTask(name = "attestaCopiaConforme")
	public void attestaCopiaConforme() {
		try {
			// Si svuotano gli esiti nel caso siano valorizzati
			cleanEsiti();
			
			final List<MasterDocumentRedDTO> documentiSelezionati = getDocumentiSelezionati();
			
			// Si verifica se ciascuno dei documenti selezionati ha almeno un allegato per Copia Conforme
			final String erroreVerifica =	signSRV.checkAllegatiCopiaConforme(utente, documentiSelezionati);
			
			// Se il controllo viene passato, si avvia l'applet di firma
			if (StringUtils.isNullOrEmpty(erroreVerifica)) {
				// N.B. La firma per l'attestazione di Copia Conforme può essere solo PADES
				// (PDF)
				PrimeFaces.current().executeScript("PF('dlgAppletFirma').show()");
				startLocalSignHelper(VIEWS_CODA_CODA_JSF, SignTypeEnum.PADES_VISIBLE, documentiSelezionati, utente, true, false); // true, false, false -> Copia Conforme, (NO) Firma Multipla, (NO) GlifoDelegato
				FacesHelper.update("centralSectionForm:signDigPanel");
				// Altrimenti, si mostra l'errore all'utente
			} else {
				showError(erroreVerifica);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	private List<MasterDocumentRedDTO> getDocumentiSelezionati() {
		final List<MasterDocumentRedDTO> documentiSelezionati = new ArrayList<>();

		if (!CollectionUtils.isEmptyOrNull(documentiDTH.getSelectedMasters())) {
			// Azione multipla
			documentiSelezionati.addAll(documentiDTH.getSelectedMasters());
		} else {
			// Azione singola
			documentiSelezionati.add(docSelectedForResponse);
		}
		return documentiSelezionati;
	}

	/**
	 * Restituisce il numero di documenti caricati. {@link #calcolaCaricati()}
	 * 
	 * @return caricati
	 */
	public final String getLoadedInfo() {
		return calcolaCaricati();
	}

	private String calcolaCaricati() {
		String caricati;
		Integer elementiCaricati = 0;
		Integer elementiTotali = 0;

		if (StringUtils.isNullOrEmpty(filterString)) {
			caricati = "CARICATI : ";
		} else {
			caricati = "TROVATI : ";
		}

		if (!CollectionUtils.isEmptyOrNull(documentiDTH.getFilteredMasters())) {
			elementiCaricati = countMastersNonDisabilitati(documentiDTH.getFilteredMasters());
		} else if (!CollectionUtils.isEmptyOrNull(documentiDTH.getMasters())) {
			elementiCaricati = countMastersNonDisabilitati(documentiDTH.getMasters());
		}
		
		final TreeNode nodeSelected = sessionBean.getNodeSelected();
		final DocumentQueueEnum queue = sessionBean.getActivePageInfo().getDocumentQueue();

		final boolean isOrganigramma = (nodeSelected != null && queue == null);
		final boolean isCodaFepa = (queue != null 
				&& (DocumentQueueEnum.FATTURE_DA_LAVORARE.equals(queue) || DocumentQueueEnum.FATTURE_IN_LAVORAZIONE.equals(queue) 
						|| DocumentQueueEnum.DD_DA_LAVORARE.equals(queue) || DocumentQueueEnum.DD_IN_FIRMA.equals(queue) 
						|| DocumentQueueEnum.DD_FIRMATI.equals(queue) || DocumentQueueEnum.DSR.equals(queue) || DocumentQueueEnum.DD_DA_FIRMARE.equals(queue))
						|| DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE_FEPA.equals(queue));
		
		if (isOrganigramma) {
			if ("CODA".equals(nodeSelected.getType())) {
				elementiTotali = ((QueueCountDTO) nodeSelected.getData()).getCount();
			} else if ("USER".equals(nodeSelected.getType())) {
				elementiTotali = ((NodoOrganigrammaDTO) nodeSelected.getData()).getCountOrg();
			}
		} else if (isCodaFepa) {
			elementiTotali = sessionBean.getCountFepa().count(queue);
		} else {
			elementiTotali = sessionBean.getCount().getQueueCount(queue);
		}

		if (elementiCaricati != null && elementiTotali != null) {
			caricati += elementiCaricati + " / " + elementiTotali;

			if (elementiTotali.intValue() == elementiCaricati.intValue()) {
				caricaTuttiGliElementi = true;
			}
		}
		return caricati;
	}

	/**
	 * Restituisce l'info relativa ai documenti selezionati.
	 * 
	 * @return
	 */
	public final String getSelectedInfo() {
		String selezionati = "SELEZIONATI : ";
		Integer elementiSelezionati = 0;
		Integer elementiTotali = 0;

		if (!CollectionUtils.isEmptyOrNull(documentiDTH.getFilteredMasters())) {
			elementiTotali = countMastersNonDisabilitati(documentiDTH.getFilteredMasters());
		} else if (!CollectionUtils.isEmptyOrNull(documentiDTH.getMasters())) {
			elementiTotali = countMastersNonDisabilitati(documentiDTH.getMasters());
		}

		if (!CollectionUtils.isEmptyOrNull(documentiDTH.getSelectedMasters())) {
			elementiSelezionati = countMastersNonDisabilitati(documentiDTH.getSelectedMasters());
		}

		selezionati += elementiSelezionati + " / " + elementiTotali;

		return selezionati;
	}

	/**
	 * Esegue il conteggio dei master non disabilitati in input.
	 * 
	 * @param mastersDaContare
	 * @return count
	 */
	private static Integer countMastersNonDisabilitati(final Collection<MasterDocumentRedDTO> mastersDaContare) {
		Integer count = 0;

		for (final MasterDocumentRedDTO master : mastersDaContare) {
			if (Boolean.FALSE.equals(master.getFlagDisabilitato())) {
				count++;
			}
		}
		return count;
	}

	/**
	 * Restituisce il messaggio di conferma della firma digitale.
	 * 
	 * @return
	 */
	public String getConfirmMessageFirmaDig() {
		return confirmMessageFirmaDig;
	}

	/**
	 * Imposta il messaggio di conferma della firma digitale.
	 * 
	 * @param confirmMessageFirmaDig
	 */
	public void setConfirmMessageFirmaDig(final String confirmMessageFirmaDig) {
		this.confirmMessageFirmaDig = confirmMessageFirmaDig;
	}

	/**
	 * Restituisce il messaggio di conferma della firma remota.
	 * 
	 * @return
	 */
	public String getConfirmMessageFirmaRemota() {
		return confirmMessageFirmaRemota;
	}

	/**
	 * Imposta il messaggio di conferma della firma remota.
	 * 
	 * @param confirmMessageFirmaRemota
	 */
	public void setConfirmMessageFirmaRemota(final String confirmMessageFirmaRemota) {
		this.confirmMessageFirmaRemota = confirmMessageFirmaRemota;
	}

	/**
	 * Restituisce true se il messaggio di conferma della firma remota dovrà essere
	 * visibile.
	 * 
	 * @return showConfirmMessageFirmaRemota
	 */
	public Boolean getShowConfirmMessageFirmaRemota() {
		return showConfirmMessageFirmaRemota;
	}

	/**
	 * Imposta la visibilità del messaggio di conferma della firma remota. true:
	 * visibile; false, altrimenti.
	 * 
	 * @param showConfirmMessageFirmaRemota
	 */
	public void setShowConfirmMessageFirmaRemota(final Boolean showConfirmMessageFirmaRemota) {
		this.showConfirmMessageFirmaRemota = showConfirmMessageFirmaRemota;
	}

	/**
	 * Restituisce true se è possibile aprire la dialog di firma remota.
	 * 
	 * @return
	 */
	public Boolean getOpenFirmaRemotaDialog() {
		return openFirmaRemotaDialog;
	}

	/**
	 * Imposta la possibilità aprire la dialog di firma remota. true, dialg
	 * apribile; false, altrimenti.
	 * 
	 * @param openFirmaRemotaDialog
	 */
	public void setOpenFirmaRemotaDialog(final Boolean openFirmaRemotaDialog) {
		this.openFirmaRemotaDialog = openFirmaRemotaDialog;
	}

	/**
	 * Restituisce la lista di tipologie di firma.
	 * 
	 * @return
	 */
	public List<String> getSignTypeList() {
		return signTypeList;
	}

	/**
	 * Imposta la lista di tipologie di firma.
	 * 
	 * @param signTypeList
	 */
	public void setSignTypeList(final List<String> signTypeList) {
		this.signTypeList = signTypeList;
	}

	/**
	 * Restituisce il flag di memorizzazione del pin.
	 * 
	 * @return
	 */
	public Boolean getMemorizzaPin() {
		return memorizzaPin;
	}

	/**
	 * Imposta la memorizzazione del pin.
	 * 
	 * @param memorizzaPin
	 */
	public void setMemorizzaPin(final Boolean memorizzaPin) {
		this.memorizzaPin = memorizzaPin;
	}

	/**
	 * Restituisce il messaggio di conferma della firma autografa.
	 * 
	 * @return firmaAutografaConfirmMessage
	 */
	public String getFirmaAutografaConfirmMessage() {
		return firmaAutografaConfirmMessage;
	}

	/**
	 * Restituisce il master del documento selezionato.
	 * 
	 * @return selectedMaster
	 */
	public final MasterDocumentRedDTO getSelectedMaster() {
		return selectedMaster;
	}

	/**
	 * Imposta il master del documento selezionato.
	 * 
	 * @param selectedMaster
	 */
	public final void setSelectedMaster(final MasterDocumentRedDTO selectedMaster) {
		this.selectedMaster = selectedMaster;
	}

	/**
	 * Restituisce il path della view di dettaglio da caricare.
	 * 
	 * @return
	 */
	public DettaglioDocumentoFileEnum getDettaglioDaCaricare() {
		return dettaglioDaCaricare;
	}

	/**
	 * Imposta il path della view di dettaglio da caricare.
	 * 
	 * @param dettaglioDaCaricare
	 */
	public void setDettaglioDaCaricare(final DettaglioDocumentoFileEnum dettaglioDaCaricare) {
		this.dettaglioDaCaricare = dettaglioDaCaricare;
	}

	/**
	 * Restituisce il documento selezionato per la response.
	 * 
	 * @return docSelectedForResponse
	 */
	public final MasterDocumentRedDTO getDocSelectedForResponse() {
		return docSelectedForResponse;
	}

	/**
	 * Imposta il documento selezionato per la response.
	 * 
	 * @param docSelectedForResponse the docSelectedForResponse to set
	 */
	public final void setDocSelectedForResponse(final MasterDocumentRedDTO docSelectedForResponse) {
		this.docSelectedForResponse = docSelectedForResponse;
	}

	/**
	 * Restituisce il path della view relativa al dettaglio corrente da caricare.
	 * 
	 * @return dettaglioDaCaricareCodaCorrente
	 */
	public DettaglioDocumentoFileEnum getDettaglioDaCaricareCodaCorrente() {
		return dettaglioDaCaricareCodaCorrente;
	}

	/**
	 * Restituisce il popup compontent per la conferma della firma remota.
	 * 
	 * @return iFirmaRemotaConfirm
	 */
	public IPopupComponent getiFirmaRemotaConfirm() {
		return iFirmaRemotaConfirm;
	}

	/**
	 * Restituisce il popup compontent che precede la firma remota.
	 * 
	 * @return iFirmaRemotaConfirm
	 */
	public IPopupComponent getiFirmaRemotaPresign() {
		return iFirmaRemotaPresign;
	}

	/**
	 * Restituisce il popup compontent della firma digitale.
	 * 
	 * @return
	 */
	public IPopupComponent getiFirmaDigitale() {
		return iFirmaDigitale;
	}

	/**
	 * Restituisce il numero protocollo emergenza.
	 * 
	 * @return numeroProtocolloEmergenza
	 */
	public Integer getNumeroProtocolloEmergenza() {
		return numeroProtocolloEmergenza;
	}

	/**
	 * Imposta il numero protocollo emergenza.
	 * 
	 * @param numeroProtocolloEmergenza
	 */
	public void setNumeroProtocolloEmergenza(final Integer numeroProtocolloEmergenza) {
		this.numeroProtocolloEmergenza = numeroProtocolloEmergenza;
	}

	/**
	 * Restituisce l'anno protocollo emergenza.
	 * 
	 * @return
	 */
	public Integer getAnnoProtocolloEmergenza() {
		return annoProtocolloEmergenza;
	}

	/**
	 * Imposta l'anno protocollo emergenza.
	 * 
	 * @param annoProtocolloEmergenza
	 */
	public void setAnnoProtocolloEmergenza(final Integer annoProtocolloEmergenza) {
		this.annoProtocolloEmergenza = annoProtocolloEmergenza;
	}

	/**
	 * Restituisce il DTO del protocollo emergenza.
	 * 
	 * @return protEm
	 */
	public ProtocolloEmergenzaDocDTO getProtEm() {
		return protEm;
	}

	/**
	 * Imposta il DTO del protocollo emergenza.
	 * 
	 * @param protEm
	 */
	public void setProtEm(final ProtocolloEmergenzaDocDTO protEm) {
		this.protEm = protEm;
	}

	/**
	 * Restituisce il numero di esiti andati a buon fine.
	 * 
	 * @return the countEsitiOperazioneOk
	 */
	public Integer getCountEsitiOperazioneOk() {
		if (countEsitiOperazioneOk == null && !org.springframework.util.CollectionUtils.isEmpty(esitiOperazione)) {
			countEsitiOperazioneOk = 0;
			for (final EsitoOperazioneDTO esito : esitiOperazione) {
				if (Boolean.TRUE.equals(esito.getFlagEsito())) {
					countEsitiOperazioneOk++;
				}
				// vanno controllati anche gli allegati
				if (esito.getEsitiAllegati() != null && !esito.getEsitiAllegati().isEmpty()) {
					for (final EsitoOperazioneDTO eAllegato : esito.getEsitiAllegati()) {
						if (Boolean.TRUE.equals(eAllegato.getFlagEsito())) {
							countEsitiOperazioneOk++;
						}
					}
				}
			}
		}
		return countEsitiOperazioneOk;
	}

	/**
	 * Restituisce il numero di esiti NON andati a buon fine.
	 * 
	 * @return the countEsitiOperazioneKo
	 */
	public Integer getCountEsitiOperazioneKo() {
		if (countEsitiOperazioneKo == null && !org.springframework.util.CollectionUtils.isEmpty(esitiOperazione)) {
			countEsitiOperazioneKo = 0;
			for (final EsitoOperazioneDTO esito : esitiOperazione) {
				if (Boolean.FALSE.equals(esito.getFlagEsito())) {
					countEsitiOperazioneKo++;
				}
			}
		}
		return countEsitiOperazioneKo;
	}

	/**
	 * Imposta il numero di esiti andati a buon fine.
	 * 
	 * @param countEsitiOperazioneOk
	 */
	public void setCountEsitiOperazioneOk(final Integer countEsitiOperazioneOk) {
		this.countEsitiOperazioneOk = countEsitiOperazioneOk;
	}

	/**
	 * Imposta il numero di esiti NON andati a buon fine.
	 * 
	 * @param countEsitiOperazioneKo
	 */
	public void setCountEsitiOperazioneKo(final Integer countEsitiOperazioneKo) {
		this.countEsitiOperazioneKo = countEsitiOperazioneKo;
	}

	/**
	 * Restituisce il testo del messaggio di esito positivo.
	 * 
	 * @return
	 */
	public String getMessaggioEsitoPositivo() {
		if ((countEsitiOperazioneOk == null || countEsitiOperazioneOk > 0) && !org.springframework.util.CollectionUtils.isEmpty(esitiOperazione)) {
			// recupero il primo messaggio di esito positivo
			for (final EsitoOperazioneDTO esito : esitiOperazione) {
				if (Boolean.TRUE.equals(esito.getFlagEsito()) && !StringUtils.isNullOrEmpty(esito.getNote())) {
					messaggioEsitoPositivo = esito.getNote();
					break;
				}
			}
		}
		return messaggioEsitoPositivo;
	}

	/**
	 * Esegue una response singola del gruppo in input se questa è associabile al
	 * documento (cioé se contemplata nel suo insieme di response singole).
	 * 
	 * @param gruppoResponseSelected
	 * @param responsesSorted
	 */
	public void eseguiResponseGruppo(final GruppiResponseEnum gruppoResponseSelected, final ResponsesRedEnum[] responsesSorted) {
		boolean showErrorNoReponse = true;
		ResponsesRedEnum[] responses = null;

		if (gruppoResponseSelected != null && gruppoResponseSelected.getReponses() != null) {
			responses = gruppoResponseSelected.getReponses();
		} else if (responsesSorted != null) {
			responses = responsesSorted;
		}

		if (responses == null) {
			LOGGER.error("Lista delle response non inizializzata prima della chiamata ad un metodo della Collection.");
			throw new RedException("Lista delle response non inizializzata prima della chiamata ad un metodo della Collection.");
		}

		ResponsesRedEnum r = null;
		if (responsSingle != null) {
			for (int x = 0; x < responses.length; x++) {
				for (final ResponsesRedEnum resp : responsSingle) {
					if (resp.equals(responses[x])) {
						showErrorNoReponse = false;
						r = resp;
						break;
					}
				}
				if (r != null) {
					break;
				}
			}
		}
		if (showErrorNoReponse) {
			showError("Non è presente alcuna azione associabile al documento");
		} else {
			goToExecuteSingle(r);
		}
	}

	/**
	 * Imposta il messaggio per esito positivo.
	 * 
	 * @param messaggioEsitoPositivo
	 */
	public void setMessaggioEsitoPositivo(final String messaggioEsitoPositivo) {
		this.messaggioEsitoPositivo = messaggioEsitoPositivo;
	}

	/**
	 * @return pageIndex
	 */
	public int getPageIndex() {
		return pageIndex;
	}

	/**
	 * Restituisce il flag di visualizzazione dell'esito dei documenti firmati.
	 * 
	 * @return mostraDocumentiFirmatiEsito
	 */
	public boolean isMostraDocumentiFirmatiEsito() {
		return mostraDocumentiFirmatiEsito;
	}

	/**
	 * Restituisce il sourceType della response (FILENET o APP)
	 * 
	 * @return the columnSourceType
	 */
	public final SourceTypeEnum getColumnSourceType() {
		return columnSourceType;
	}

	/**
	 * Restituisce la data e l'ora dell'esito.
	 * 
	 * @return
	 */
	public Date getDataEoraEsito() {
		return dataEoraEsito;
	}

	/**
	 * Restituisce la data e l'ora dell'esito per il titolo.
	 * 
	 * @return dataEoraEsito in formato dd/MM/yyyy HH:mm
	 */
	public String getDataEoraEsitoPerTitolo() {
		final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		if (dataEoraEsito == null) {
			dataEoraEsito = new Date();
		}
		return sdf.format(dataEoraEsito);
	}

	/**
	 * Restituisce la data e l'ora dell'esito per il nome del file.
	 * 
	 * @return dataEoraEsito in formato dd_MM_yyyy_HH.mm
	 */
	public String getDataEoraEsitoPerNomeFile() {
		String nome = "esito_firme_";
		final SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH.mm");
		if (dataEoraEsito == null) {
			dataEoraEsito = new Date();
		}
		nome += sdf.format(dataEoraEsito);
		return nome;
	}

	/**
	 * Esegue una deepCopy di un master su di un altro.
	 * 
	 * @param read  - master da copiare
	 * @param write - master da sovrascrivere
	 */
	public static void copyMaster(final MasterDocumentRedDTO read, final MasterDocumentRedDTO write) {
		try {
			final BeanInfo info = Introspector.getBeanInfo(MasterDocumentRedDTO.class);
			final PropertyDescriptor[] props = info.getPropertyDescriptors();
			for (final PropertyDescriptor pd : props) {
				final Object value = pd.getReadMethod().invoke(read);
				final Method mtdWrite = pd.getWriteMethod();
				if (mtdWrite != null) {
					mtdWrite.invoke(write, value);
				}
			}
		} catch (final Exception e) {
			throw new RedException(e);
		}
	}

	private void updateMastersAfterResponse(final NavigationTokenEnum actPage, final OrgObjDTO orgData, final UtenteDTO usr, final DocumentQueueEnum actQueue,
			final List<MasterDocumentRedDTO> masters) {
		final Collection<String> dts = new ArrayList<>();
		for (final MasterDocumentRedDTO m : masters) {
			dts.add(m.getDocumentTitle());
		}
		final Collection<MasterDocumentRedDTO> newMasters = new ArrayList<>();

		DocumentQueueEnum queue = actQueue;
		if (actPage.equals(NavigationTokenEnum.ORG_SCRIVANIA)) {
			queue = orgData.getQueueToGo();
		}
		if (SourceTypeEnum.FILENET.equals(queue.getType())) {
			final MasterPageIterator mpi = masterPaginatiSRV.getMastersRaw(queue, dts, usr);
			while (Boolean.TRUE.equals(mpi.getMorePages())) {
				if (actPage.equals(NavigationTokenEnum.ORG_SCRIVANIA)) {
					if (orgData.getDatiAccesso() != null) {
						newMasters.addAll(masterPaginatiSRV.refineMastersOrganigramma(usr, orgData.getDatiAccesso(), mpi));
					}
				} else {
					newMasters.addAll(masterPaginatiSRV.refineMasters(usr, mpi));
				}
			}
		} else {
			// Coda applicativa: non paginata
			newMasters.addAll(listaDocumentiSRV.getDocumentForMaster(queue, dts, usr));
		}
		for (final MasterDocumentRedDTO newMaster : newMasters) {
			for (final MasterDocumentRedDTO oldMaster : masters) {
				if (newMaster.getWobNumber().equals(oldMaster.getWobNumber())) {
					copyMaster(newMaster, oldMaster);
					break;
				}
			}
		}
	}

	/**
	 * @return DettaglioDocumentoBean
	 */
	public DettaglioDocumentoBean getDdBean() {
		return ddBean;
	}

	/**
	 * Restituisce il nome del file per l'export della coda.
	 * 
	 * @return nome
	 */
	public String getNomeFileExportCoda() {
		String nome = "";

		if (utente != null) {
			nome += utente.getNome().toLowerCase() + "_" + utente.getCognome().toLowerCase() + "_";
		}
		if (!StringUtils.isNullOrEmpty(nomeCodaAttuale)) {
			final String displayName = nomeCodaAttuale.replace(' ', '_').toLowerCase() + "_";
			nome += displayName;
		}
		final SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH.mm");
		nome += sdf.format(new Date());

		return nome;
	}

	/**
	 * Restituisce il flag che indica un errore nell'OTP.
	 * 
	 * @return erroreOTP
	 */
	public Boolean getErroreOTP() {
		return erroreOTP;
	}

	/**
	 * Imposta il flag che indica un errore nell'OTP.
	 * 
	 * @param erroreOTP
	 */
	public void setErroreOTP(final Boolean erroreOTP) {
		this.erroreOTP = erroreOTP;
	}

	/**
	 * Restituisce il flag che indica il blocco del PIN.
	 * 
	 * @return pinBloccato
	 */
	public Boolean getPinBloccato() {
		return pinBloccato;
	}

	/**
	 * Imposta il flag che indica il blocco del PIN.
	 * 
	 * @param pinBloccato
	 */
	public void setPinBloccato(final Boolean pinBloccato) {
		this.pinBloccato = pinBloccato;
	}

	/**
	 * Restituisce il tipo di firma selezionata.
	 * 
	 * @return the selectedSignType
	 */
	public String getSelectedSignType() {
		return selectedSignType;
	}

	/**
	 * Imposta il tipo di firma selezionata.
	 * 
	 * @param selectedSignType
	 */
	public void setSelectedSignType(final String selectedSignType) {
		this.selectedSignType = selectedSignType;
	}

	/**
	 * @return OTP
	 */
	public String getOtp() {
		return otp;
	}

	/**
	 * Imposta l'OTP.
	 * 
	 * @param otp
	 */
	public void setOtp(final String otp) {
		this.otp = otp;
	}

	/**
	 * @return PIN
	 */
	public String getPin() {
		return pin;
	}

	/**
	 * Imposta il PIN.
	 * 
	 * @param pin
	 */
	public void setPin(final String pin) {
		this.pin = pin;
	}

	/**
	 * @return flagStampaProtocollo
	 */
	public final boolean isFlagStampaProtocollo() {
		return flagStampaProtocollo;
	}

	/**
	 * Restituisce la stringa del protocollo da stampare.
	 * 
	 * @return protocolloDaStampare
	 */
	public final String getProtocolloDaStampare() {
		return protocolloDaStampare;
	}

	/**
	 * Restituisce il tipo di firma selezionato per la firma remota.
	 * 
	 * @return selectedSignTypeFirmaRemota
	 */
	public String getSelectedSignTypeFirmaRemota() {
		return selectedSignTypeFirmaRemota;
	}

	/**
	 * Imposta il tipo di firma selezionato per la firma remota.
	 * 
	 * @param selectedSignTypeFirmaRemota
	 */
	public void setSelectedSignTypeFirmaRemota(final String selectedSignTypeFirmaRemota) {
		this.selectedSignTypeFirmaRemota = selectedSignTypeFirmaRemota;
	}

	/**
	 * Resituisce il flag che indica se la responde vada renderizzata.
	 * 
	 * @return
	 */
	public boolean isRenderResponse() {
		return renderResponse;
	}

	/**
	 * Imposta il flag che indica se la responde vada renderizzata.
	 * 
	 * @param renderResponse
	 */
	public void setRenderResponse(final boolean renderResponse) {
		this.renderResponse = renderResponse;
	}

	/**
	 * Restiuisce la dimensione configurata per la coda.
	 * 
	 * @return the configSizeCoda
	 */
	public String getConfigSizeCoda() {
		return configSizeCoda;
	}

	/**
	 * Imposta la dimensione configurata per la coda.
	 * 
	 * @param configSizeCoda the configSizeCoda to set
	 */
	public void setConfigSizeCoda(final String configSizeCoda) {
		this.configSizeCoda = configSizeCoda;
	}

	/**
	 * @return templateMaxCoda
	 */
	public String getTemplateMaxCoda() {
		return templateMaxCoda;
	}

	/**
	 * Imposta il templateMaxCoda.
	 * 
	 * @param templateMaxCoda
	 */
	public void setTemplateMaxCoda(final String templateMaxCoda) {
		this.templateMaxCoda = templateMaxCoda;
	}

	/**
	 * Restituisce il flag che indica la presenza del libro firma multipla.
	 * 
	 * @return hasLibroFirmaMultipla
	 */
	public boolean isHasLibroFirmaMultipla() {
		return hasLibroFirmaMultipla;
	}

	/**
	 * Restituisce il flag per la visualizzazione del messaggio di conferma della
	 * firma approvazioni.
	 * 
	 * @return showConfirmMessageFirmaApprovazioni
	 */
	public boolean isShowConfirmMessageFirmaApprovazioni() {
		return showConfirmMessageFirmaApprovazioni;
	}

	/**
	 * Imposta il flag per la visualizzazione del messaggio di conferma della firma
	 * approvazioni.
	 * 
	 * @param showConfirmMessageFirmaApprovazioni
	 */
	public void setShowConfirmMessageFirmaApprovazioni(final boolean showConfirmMessageFirmaApprovazioni) {
		this.showConfirmMessageFirmaApprovazioni = showConfirmMessageFirmaApprovazioni;
	}

	/**
	 * Restituisce il messaggio di conferma della firma approvazioni.
	 * 
	 * @return
	 */
	public String getConfirmMessageFirmaApprovazioni() {
		return confirmMessageFirmaApprovazioni;
	}

	/**
	 * Imposta il messaggio di conferma della firma approvazioni.
	 * 
	 * @param confirmMessageFirmaApprovazioni
	 */
	public void setConfirmMessageFirmaApprovazioni(final String confirmMessageFirmaApprovazioni) {
		this.confirmMessageFirmaApprovazioni = confirmMessageFirmaApprovazioni;
	}

	/**
	 * Restituisce il flag caricaTuttiGliElementi.
	 * 
	 * @return caricaTuttiGliElementi
	 */
	public boolean isCaricaTuttiGliElementi() {
		return caricaTuttiGliElementi;
	}

	/**
	 * Imposta il flag caricaTuttiGliElementi.
	 * 
	 * @param caricaTuttiGliElementi
	 */
	public void setCaricaTuttiGliElementi(final boolean caricaTuttiGliElementi) {
		this.caricaTuttiGliElementi = caricaTuttiGliElementi;
	}

	/**
	 * Restituisce il flag per la visualizzazione della selezione globale per un
	 * visto.
	 * 
	 * @return showSelezionaTuttiVisto
	 */
	public boolean isShowSelezionaTuttiVisto() {
		return showSelezionaTuttiVisto;
	}

	/**
	 * Imposta il flag per la visualizzazione della selezione globale per un visto.
	 * 
	 * @param showSelezionaTuttiVisto
	 */
	public void setShowSelezionaTuttiVisto(final boolean showSelezionaTuttiVisto) {
		this.showSelezionaTuttiVisto = showSelezionaTuttiVisto;
	}

	/**
	 * Restituisce il content del file xls da scaricare.
	 * 
	 * @return file
	 */
	public StreamedContent downloadEExcel() {
		StreamedContent file = null;
		try {
			final byte[] contentCoda = downloadCoda();
			final InputStream io = new ByteArrayInputStream(contentCoda);
			final String giornoMeseAnnoOraminutisecondi = DateUtils.dateToString(Calendar.getInstance().getTime(), "ddMMyyyy_HHmmss");
			final String nomeFile = "DocumentoDaLavorare" + "_" + giornoMeseAnnoOraminutisecondi + ".xls";
			file = new DefaultStreamedContent(io, "application/xls", nomeFile);
		} catch (final Exception e) {
			showError("Impossibile effettuare il download della coda :" + e);
		}
		return file;
	}

	/**
	 * Restituisce il content del file xls da scaricare.
	 * 
	 * @return contentXls
	 */
	public byte[] downloadCoda() {
		final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		final HSSFWorkbook workbook = new HSSFWorkbook();
		final Sheet sheetXls = workbook.createSheet(NOME_SHEET);
		byte[] contentXls = null;
		final String[] headerArr = new String[] { "N.Prot. - ID - RR", "Data", "Mittente", "U.O. Superiore", "U.O. Competente", "Oggetto", "Tipo Documento", "Data scadenza",
				"Tipo assegnazione" };
		final List<String> header = new ArrayList<>(Arrays.asList(headerArr));

		List<MasterDocumentRedDTO> masters = null;
		try {
			if (documentiDTH.getFilteredMasters() != null) {
				masters = new ArrayList<>(documentiDTH.getFilteredMasters());
			} else {
				masters = new ArrayList<>(documentiDTH.getMasters());
			}
			calcolaIeIILivelloENumProt(masters);

			final CellStyle styleHeader = XlsUtils.createCellStyle(workbook, true, 14, IndexedColors.BLACK, IndexedColors.GREY_50_PERCENT, true, false);

			final List<String> fieldNames = getName(masters);

			XlsUtils.writeExcel(sheetXls, masters, fieldNames, header, 5, styleHeader);
			XlsUtils.autoSizeColumns(workbook);
			writeTemplate(sdf, workbook, sheetXls);

			contentXls = XlsUtils.getByte(workbook);
		} catch (final Exception ex) {
			LOGGER.error("Impossibile effettuare il download :" + ex);
			showError("Impossibile effettuare il download");
		} finally {
			closeWorkbook(workbook);
		}
		return contentXls;
	}

	/**
	 * @param workbook
	 */
	private void closeWorkbook(final HSSFWorkbook workbook) {
		try {
			workbook.close();
		} catch (final Exception e) {
			LOGGER.error("Errore durante la chiusura del file", e);
			throw new RedException("Errore durante la chiusura del file", e);
		}
	}

	/**
	 * Calcola e imposta ufficio di I e II livello per i master in input.
	 * 
	 * @param masters
	 */
	public void calcolaIeIILivelloENumProt(final List<MasterDocumentRedDTO> masters) {
		for (final MasterDocumentRedDTO master : masters) {

			final String numProt = master.getNumeroProtocollo() == null ? "" : String.valueOf(master.getNumeroProtocollo() + "/");
			final String annoProt = master.getAnnoProtocollo() == null ? "" : String.valueOf(master.getAnnoProtocollo() + " - ");
			final String idDoc = master.getNumeroDocumento() == null ? "" : String.valueOf(master.getNumeroDocumento());
			final String annoDoc = (master.getNumeroProtocollo() == null && master.getAnnoDocumento() != null) ? " - " + master.getAnnoDocumento() : "";

			master.setNumProtIdProtAnnoExcel(numProt + annoProt + idDoc + annoDoc);
			if (master.getTipoAssegnazione().equals(TipoAssegnazioneEnum.COMPETENZA)) {
				final Nodo nodoFiglio = listaDocumentiSRV.recuperaUfficioPadrePerReport(utente.getIdUfficio());
				setPrimoESecondoLivello(nodoFiglio, master);
			} else {
				final Integer idNodoDestinatario = listaDocumentiSRV.recuperaUfficioPadrePerReportNonCompetenza(master.getDocumentTitle(), null, utente);
				if (idNodoDestinatario != null) {
					final Nodo nodoFiglio = listaDocumentiSRV.recuperaUfficioPadrePerReport(idNodoDestinatario.longValue());
					setPrimoESecondoLivello(nodoFiglio, master);
				}
			}
		}
	}

	private void setPrimoESecondoLivello(final Nodo nodoFiglio, final MasterDocumentRedDTO master) {
		Nodo nodoPadre;
		if (nodoFiglio.getIdNodoPadre().equals(nodoFiglio.getIdNodo())) {
			master.setPrimoLivello(nodoFiglio.getDescrizione());
			master.setSecondoLivello("");
		} else {
			nodoPadre = listaDocumentiSRV.recuperaUfficioPadrePerReport(nodoFiglio.getIdNodoPadre());
			master.setPrimoLivello(nodoPadre.getDescrizione());
			master.setSecondoLivello(nodoFiglio.getDescrizione());
		}
	}

	/**
	 * Crea un template per scrivere il file xls.
	 * 
	 * @param sdf
	 * @param workbook
	 * @param sheetXls
	 */
	public void writeTemplate(final SimpleDateFormat sdf, final HSSFWorkbook workbook, final Sheet sheetXls) {
		final CellStyle styleTemplate = XlsUtils.createCellStyle(workbook, true, 14, IndexedColors.BLACK, null, false, false);
		int riga = 0;
		final int startCol = 0;
		XlsUtils.writeRow(sheetXls, riga++, startCol, styleTemplate, "Stampa del:", sdf.format(new Date()));
		XlsUtils.writeRow(sheetXls, riga, startCol, styleTemplate, "Aoo", utente.getCodiceAoo());
		XlsUtils.autoSizeColumns(workbook);

		final CellStyle styleTemplateCentral = XlsUtils.createCellStyle(workbook, true, 14, IndexedColors.BLACK, null, false, true);
		final Row rowCentral = sheetXls.getRow(0);
		final Cell cell = rowCentral.createCell(2);
		cell.setCellValue("Protocolli in lavorazione");
		cell.setCellStyle(styleTemplateCentral);
		sheetXls.addMergedRegion(new CellRangeAddress(0, 1, 2, 5));
	}

	private static List<String> getName(final List<MasterDocumentRedDTO> masters) {
		final List<String> campiExcelString = new ArrayList<>();
		final List<Field> campiExcel = new ArrayList<>();
		try {
			campiExcel.add(masters.get(0).getClass().getDeclaredField("numProtIdProtAnnoExcel"));
			campiExcel.add(masters.get(0).getClass().getDeclaredField("dataCreazione"));
			campiExcel.add(masters.get(0).getClass().getDeclaredField("mittente"));
			campiExcel.add(masters.get(0).getClass().getDeclaredField("primoLivello"));
			campiExcel.add(masters.get(0).getClass().getDeclaredField("secondoLivello"));
			campiExcel.add(masters.get(0).getClass().getDeclaredField("oggetto"));
			campiExcel.add(masters.get(0).getClass().getDeclaredField("tipologiaDocumento"));
			campiExcel.add(masters.get(0).getClass().getDeclaredField("dataScadenza"));
			campiExcel.add(masters.get(0).getClass().getDeclaredField("tipoAssegnazioneDescr"));

			for (final Field field : campiExcel) {
				campiExcelString.add(field.getName());
			}
		} catch (final Exception ex) {
			LOGGER.error("Impossibile recuperare i campi dalla classe :" + ex);
		}
		return campiExcelString;
	}

	/**
	 * Restituisce l'ultimo master selezionato.
	 * 
	 * @return lastMaster
	 */
	public MasterDocumentRedDTO getLastMaster() {
		return lastMaster;
	}

	/**
	 * Imposta l'ultimo master selezionato.
	 * 
	 * @param lastMaster
	 */
	public void setLastMaster(final MasterDocumentRedDTO lastMaster) {
		this.lastMaster = lastMaster;
	}

	/**
	 * Restituisce la lista di DTO delle colonne del DT.
	 * 
	 * @return listaColonneCodeDTO
	 */
	public List<ColonneCodeDTO> getListaColonneCodeDTO() {
		return listaColonneCodeDTO;
	}

	/**
	 * Imposta la lista di DTO delle colonne del DT.
	 * 
	 * @param listaColonneCodeDTO
	 */
	public void setListaColonneCodeDTO(final List<ColonneCodeDTO> listaColonneCodeDTO) {
		this.listaColonneCodeDTO = listaColonneCodeDTO;
	}

	/**
	 * Salva la configurazione delle colonne del DT.
	 */
	public void salvaColonneScelte() {
		try {
			final boolean listaSessionNonValida = CollectionUtils.isEmptyOrNull(sessionBean.getListaColonneCodeDTO());

			if (listaSessionNonValida) {
				showError("Non è possibile salvare senza aver selezionato nessuna colonna");
				return;
			}
			gestioneColonneSRV.salvaColonneScelte(sessionBean.getUtente().getId(), sessionBean.getListaColonneCodeDTO());
			showInfoMessage("Colonne salvate correttamente");
		} catch (final Exception ex) {
			LOGGER.error("Errore durante il salvataggio delle colonne", ex);
			showError("Errore durante il salvataggio delle colonne");
		}
	}

	/**
	 * Check esiti operazioni doc. principale ed allegati.
	 * 
	 * @param checkAllegati
	 * @return
	 */
	public boolean checkEsiti(final boolean checkAllegati) {
		final Collection<EsitoOperazioneDTO> listaEsiti = getEsitiOperazione();
		for (final EsitoOperazioneDTO esito : listaEsiti) {
			if (Boolean.FALSE.equals(esito.getFlagEsito())) {
				return false;
			}
			if (checkAllegati && esito.getEsitiAllegati() != null) {
				final Collection<EsitoOperazioneDTO> listaEsitiAllegati = esito.getEsitiAllegati();
				for (final EsitoOperazioneDTO esitoAllegato : listaEsitiAllegati) {
					if (Boolean.FALSE.equals(esitoAllegato.getFlagEsito())) {
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * Restituisce il flag di visibilità della sola firma PADES sul documento
	 * principale.
	 * 
	 * @return principaleOnlyPAdESVisibleAftLock
	 */
	public boolean isPrincipaleOnlyPAdESVisibleAftLock() {
		return principaleOnlyPAdESVisibleAftLock;
	}

	/**
	 * Imposta il flag di visibilità della sola firma PADES sul documento
	 * principale.
	 * 
	 * @param principaleOnlyPAdESVisibleAftLock
	 */
	public void setPrincipaleOnlyPAdESVisibleAftLock(final boolean principaleOnlyPAdESVisibleAftLock) {
		this.principaleOnlyPAdESVisibleAftLock = principaleOnlyPAdESVisibleAftLock;
	}

	/**
	 * Restituisce l'insieme di wobNumber necessari alla prosecuzione dell'iter di
	 * firma.
	 * 
	 * @return wobNumbersContinuaAftLock
	 */
	public Collection<String> getWobNumbersContinuaAftLock() {
		return wobNumbersContinuaAftLock;
	}

	/**
	 * Imposta l'insieme di wobNumber necessari alla prosecuzione dell'iter di
	 * firma.
	 * 
	 * @param wobNumbersContinuaAftLock
	 */
	public void setWobNumbersContinuaAftLock(final Collection<String> wobNumbersContinuaAftLock) {
		this.wobNumbersContinuaAftLock = wobNumbersContinuaAftLock;
	}

	/**
	 * Restituisce il nome dell'utente che ha lockato il doc.
	 * 
	 * @return nomeUtenteLockatore
	 */
	public String getNomeUtenteLockatore() {
		return nomeUtenteLockatore;
	}

	/**
	 * Imposta il nome dell'utente che ha lockato il doc.
	 * 
	 * @param nomeUtenteLockatore
	 */
	public void setNomeUtenteLockatore(final String nomeUtenteLockatore) {
		this.nomeUtenteLockatore = nomeUtenteLockatore;
	}

	/**
	 * Get del tipo di glifo , delegato o delegante.
	 * @return glifoSelezionatoRemota
	 */
	public byte[] getGlifoSelezionatoRemota() {
		return glifoSelezionatoRemota;
	}

	/**
	 * Imposta il tipo di glifo , delegato o delegante.
	 * @param glifoSelezionatoRemota
	 */
	public void setGlifoSelezionatoRemota(byte[] glifoSelezionatoRemota) {
		this.glifoSelezionatoRemota = glifoSelezionatoRemota;
	}
	
	/**
	 * Get del tipo di glifo , delegato o delegante.
	 * @return glifoSelezionatoDigitale
	 */
	public byte[] getGlifoSelezionatoDigitale() {
		return glifoSelezionatoDigitale;
	}

	/**
	 * Imposta il tipo di glifo , delegato o delegante.
	 * @param glifoSelezionatoDigitale
	 */
	public void setGlifoSelezionatoDigitale(byte[] glifoSelezionatoDigitale) {
		this.glifoSelezionatoDigitale = glifoSelezionatoDigitale;
	}

	/**
	 * Listener che scatta al cambio del tipo di firma remota delegata nel libro firma delegato 
	 * @return 
	 */
	public void onSelectGlifoFirmaRemota() { 
		try {   
			if(tipologieGlifoFirma!=null) {
				if(glifoSelezionatoRemota==null) {
					glifoSelezionatoRemota = (byte[]) tipologieGlifoFirma.values().toArray()[0];
				}  
				int resizeWidth = 270;
				resizeAndSetPreviewGlifo(glifoSelezionatoRemota,resizeWidth);
			} 
		} catch(Exception ex) {
			LOGGER.error(ex);
		} 
	}
	
	/**
	 * Listener che scatta al cambio del tipo di firma digitale delegata nel libro firma delegato 
	 * @return 
	 */
	public void onSelectGlifoFirmaDigitale() { 
		try {   
			if(tipologieGlifoFirma!=null) {
				if(glifoSelezionatoDigitale==null) {
					glifoSelezionatoDigitale = (byte[]) tipologieGlifoFirma.values().toArray()[0];
				}  
				int resizeWidth = 210;
				resizeAndSetPreviewGlifo(glifoSelezionatoDigitale,resizeWidth);
			} 
		} catch(Exception ex) {
			LOGGER.error(ex);
		} 
	}

	private void resizeAndSetPreviewGlifo(byte[] glifoSelezionato,int width) throws IOException {
		byte[] content = null;
		try {
			content = glifoSelezionato;	
			InputStream io = new ByteArrayInputStream(content);
			BufferedImage bufferedImg = ImageIO.read(io); 
			 
			Image resultingImage = bufferedImg.getScaledInstance(width, 100, Image.SCALE_DEFAULT);
			BufferedImage outputImage = new BufferedImage(width, 100, BufferedImage.TYPE_INT_RGB);
			outputImage.getGraphics().drawImage(resultingImage, 0, 0, null);

			ByteArrayOutputStream os = new ByteArrayOutputStream();
			ImageIO.write(outputImage, "png", os);
			io = new ByteArrayInputStream(os.toByteArray());  
			sessionBean.setPreviewGlifo(new DefaultStreamedContent(io, "image/png","Glifo.png")); 
		} catch(Exception ex) {
			LOGGER.error("Errore durante la preview del glifo : " +ex);
			showError("Errore nel recupero del glifo per la preview ");
		}

	}
	
 
	
	/**
	 * Mappa di glifi di firma delegato/delegante
	 * @return map
	 */
	public Map<String,byte[]> getTipologieGlifoFirma(){
		return tipologieGlifoFirma;
	}
	
	/**
	 * Mappa di glifi di firma delegato/delegante
	 * @param tipologieGlifoFirma
	 */
	public void setTipologieGlifoFirma(Map<String,byte[]> tipologieGlifoFirma){
		this.tipologieGlifoFirma = tipologieGlifoFirma;
	}

	/**
	 * Restitusice la data attivazione giro visti delegato.
	 * @return data attivazione giro visti delegato
	 */
	public Date getDataAttivazioneGiroVistiDelegato() {
		return dataAttivazioneGiroVistiDelegato;
	}

	/**
	 * Imposta la data attivazione giro visti delegato.
	 * @param dataAttivazioneGiroVistiDelegato
	 */
	public void setDataAttivazioneGiroVistiDelegato(Date dataAttivazioneGiroVistiDelegato) {
		this.dataAttivazioneGiroVistiDelegato = dataAttivazioneGiroVistiDelegato;
	}
}