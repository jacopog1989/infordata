package it.ibm.red.web.mbean;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.ss.formula.eval.NotImplementedException;
import org.primefaces.component.accordionpanel.AccordionPanel;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.MenuActionEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuItem;
import org.primefaces.model.menu.MenuModel;

import com.filenet.api.collection.PageIterator;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.RequestParameter;
import it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO;
import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.ColonneCodeDTO;
import it.ibm.red.business.dto.CountDTO;
import it.ibm.red.business.dto.CountFepaDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FascicoliPregressiDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.KeyValueDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.MasterSigiDTO;
import it.ibm.red.business.dto.MittDestProtocolloNsdDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.NotificheUtenteDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocSalvataDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataFaldDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataFascDTO;
import it.ibm.red.business.dto.ParamsRicercaFascicoliPregressiDfDTO;
import it.ibm.red.business.dto.ParamsRicercaFlussiDTO;
import it.ibm.red.business.dto.ParamsRicercaProtocolliPregressiDfDTO;
import it.ibm.red.business.dto.ParamsRicercaProtocolloDTO;
import it.ibm.red.business.dto.PreferenzeDTO;
import it.ibm.red.business.dto.ProtocolliPregressiDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.RegistroProtocolloDTO;
import it.ibm.red.business.dto.ResponsabileCopiaConformeDTO;
import it.ibm.red.business.dto.RicercaAvanzataDocFormDTO;
import it.ibm.red.business.dto.RicercaAvanzataFascicoliFormDTO;
import it.ibm.red.business.dto.RicercaResultDTO;
import it.ibm.red.business.dto.RicercaVeloceRequestDTO;
import it.ibm.red.business.dto.StatoRicercaDTO;
import it.ibm.red.business.dto.ThemeDTO;
import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.dto.TipologiaRegistroNsdDTO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.AccessoFunzionalitaEnum;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ColonneEnum;
import it.ibm.red.business.enums.ContoGiudizialeEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.EventoCalendarioEnum;
import it.ibm.red.business.enums.FunzionalitaEnum;
import it.ibm.red.business.enums.LogoMefEnum;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.QueueGroupEnum;
import it.ibm.red.business.enums.RicercaAssegnanteAssegnatarioEnum;
import it.ibm.red.business.enums.RicercaPerBusinessEntityEnum;
import it.ibm.red.business.enums.StatoMailGUIEnum;
import it.ibm.red.business.enums.StatoNotificaUtenteEnum;
import it.ibm.red.business.enums.ThemeEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoStrutturaNodoEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.common.ComparatorDateMail;
import it.ibm.red.business.helper.filenet.common.ComparatorNotifiche;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.exceptions.BusinessDelegateRuntimeException;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.RicercaAvanzataSalvata;
import it.ibm.red.business.persistence.model.Ruolo;
import it.ibm.red.business.persistence.model.TipoEvento;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.facade.IAssegnaUfficioFacadeSRV;
import it.ibm.red.business.service.facade.ICasellePostaliFacadeSRV;
import it.ibm.red.business.service.facade.ICodeDocumentiFacadeSRV;
import it.ibm.red.business.service.facade.ICounterFacadeSRV;
import it.ibm.red.business.service.facade.IDfFacadeSRV;
import it.ibm.red.business.service.facade.IFaldoneFacadeSRV;
import it.ibm.red.business.service.facade.IFascicoloFacadeSRV;
import it.ibm.red.business.service.facade.IGestioneColonneFacadeSRV;
import it.ibm.red.business.service.facade.IMailFacadeSRV;
import it.ibm.red.business.service.facade.INodoFacadeSRV;
import it.ibm.red.business.service.facade.INotificaFacadeSRV;
import it.ibm.red.business.service.facade.INotificaUtenteFacadeSRV;
import it.ibm.red.business.service.facade.INpsFacadeSRV;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaAvanzataDocFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaAvanzataFaldFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaAvanzataFascFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaFepaFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaRegistroProtocolloFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaSigiFacadeSRV;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.ITitolarioFacadeSRV;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.HttpUtils;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.mbean.DocumentManagerBean;
import it.ibm.red.web.dto.CasellaMailStatoDTO;
import it.ibm.red.web.dto.OrgObjDTO;
import it.ibm.red.web.dto.QueueCountDTO;
import it.ibm.red.web.dto.RicercaFormContainer;
import it.ibm.red.web.dto.TreeNodeFaldoneDTO;
import it.ibm.red.web.enums.HomepageEnum;
import it.ibm.red.web.enums.ModeFaldonaturaEnum;
import it.ibm.red.web.enums.NavigationTokenEnum;
import it.ibm.red.web.enums.RicercheEnum;
import it.ibm.red.web.enums.TileEnum;
import it.ibm.red.web.enums.TreeNodeTypeEnum;
import it.ibm.red.web.factory.ThemeFactory;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.component.DialogRendererComponent;
import it.ibm.red.web.mbean.master.IFaldoneHandler;
import it.ibm.red.web.ricerca.mbean.RicercaAvanzataUcbBean;
import it.ibm.red.web.ricerca.mbean.RicercaRegistrazioniAusiliarieBean;
import it.ibm.red.web.utils.TreeNodeUtils;

/**
 * Session bean. Gestisce tutte le funzionalità trasversali alla sessione.
 */
@SessionScoped
@Named(ConstantsWeb.MBean.SESSION_BEAN)
public class SessionBean extends AbstractTreeBean implements IFaldoneHandler {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SessionBean.class.getName());
	
	/**
	 * Messaggio errore aggiornamento notifiche.
	 */
	private static final String ERRORE_NELL_AGGIORNAMENTO_DELLE_NOTIFICHE = "Errore nell'aggiornamento delle notifiche";

	/**
	 * Tag apertura Span.
	 */
	private static final String SPAN = "<span>";

	/**
	 * Tag SPAN con classe per menu counter.
	 */
	private static final String SPAN_SPAN_CLASS_MENU_COUNTER_GENERIC_TEXT_BOLD = "</span><span class='menuCounter generic_textBold'>";

	/**
	 * Tag chiusura SPAN.
	 */
	private static final String SPAN_CLOSE_TAG = "</span>";

	/**
	 * Tag chiusura BR.
	 */
	private static final String PLUS_B_HTML_CLOSE_TAG = "+</b>";

	/**
	 * Metodo JS visualizzazione central east section.
	 */
	private static final String SHOW_CENTRAL_EAST_SECTION = "showCentralEastSection();";

	/**
	 * Styleclass del menu-item selezionato.
	 */
	private static final String SELECTED_MENU_STYLECLASS = "selected-menu-item";

	/**
	 * Nome icona mail forward.
	 */
	private static final String MAIL_FORWARD_ICON_NAME = "fa fa-mail-forward";

	/**
	 * Label inizio range ricerca.
	 */
	private static final String DATA_CREAZIONE_DA = "dataCreazioneDa";

	/**
	 * Label fine range ricerca.
	 */
	private static final String DATA_CREAZIONE_A = "dataCreazioneA";

	/**
	 * Id contatore notifiche.
	 */
	private static final String CONTATORE_NOTIFICHE_ID = "contatoreNotificheId";

	/**
	 * Path component searchAllBtn.
	 */
	private static final String SEARCHALLBTN_RESOURCE_PATH = "centralSectionForm:searchAllBtn";

	/**
	 * Path component actionFilter.
	 */
	private static final String ACTIONFILTER_RESOURCE_PATH = "centralSectionForm:idActionFilter";

	/**
	 * Path component dettaglio documento DD.
	 */
	private static final String DETTAGLIODOCUMENTODD_RESOURCE_PATH = "eastSectionForm:idDettaglioDocumento_DD";

	/**
	 * Path component documenti.
	 */
	private static final String DOCUMENTI_RESOURCE_PATH = "centralSectionForm:documenti";

	/**
	 * Path component info panel.
	 */
	private static final String INFOPANEL_RESOURCE_PATH = "centralSectionForm:idInfoPanel";

	/**
	 * Solo refresh - Label.
	 */
	private static final String SOLO_REFRESH = "SOLO REFRESH";

	/**
	 * Messaggio restringimento ricerca generico.
	 */
	private static final String RESTINGIMENTO_RICERCA_SHORT_MSG = "Restringere la ricerca";

	/**
	 * Messaggio restingimento ricerca aggiungendo almeno un campo.
	 */
	private static final String RESTRINGIMENTO_RICERCA_MSG = "Restringere la ricerca valorizzando almeno uno dei campi.";

	/**
	 * JS visualizzazione statusDialog.
	 */
	private static final String SHOW_STATUS_DIALOG_JS = "PF('statusDialog').show();";

	/**
	 * Messaggio comunicativo ordine della data.
	 */
	private static final String ORDINE_DATE_MSG = "La data creazione da deve essere antecedente la data creazione a.";

	/**
	 * Messaggio limite ricerca intervallo temporale.
	 */
	private static final String LIMITE_INTERVALLO_RICERCA_MSG = "Impostare il limite della ricerca per un intervallo di massimo 6 mesi";

	/**
	 * Messaggio generico errore ricerca.
	 */
	private static final String ERROR_RICERCA_GENERICA_MSG = "Errore in fase di ricerca Generica";

	/**
	 * Errore generico operazione. Occorre appendere l'operazione al messaggio.
	 */
	private static final String GENERIC_ERROR_OPERAZIONE = "Errore durante l'operazione: ";

	/**
	 * CON FORWARD - Label.
	 */
	private static final String CON_FORWARD = "CON FORWARD";

	/**
	 * Command: goto mail.
	 */
	private static final String GOTO_MAIL_COMMAND_NAME = "#{SessionBean.gotoMail}";

	/**
	 * Messaggio.
	 */
	private static final String IN_ARRIVO = "In Arrivo";

	/**
	 * Messaggio.
	 */
	private static final String IN_USCITA = "In Uscita";

	/**
	 * Messaggio.
	 */
	private static final String RIFAUTO = "Rifiutata Automatica";

	/**
	 * Messaggio.
	 */
	private static final String RIFIUTATA_INOLTRATA = "Rifiutata/Inoltrata";

	/**
	 * Messaggio.
	 */
	private static final String ELIMINATA = "Eliminata";

	/**
	 * Tutti tab chiusi.
	 */
	private static final String TUTTI_CHIUSI = "-0";

	/**
	 * Primo tab aperto.
	 */
	private static final String PRIMO_ATTIVO = "0";

	/**
	 * Secondo tab aperto.
	 */
	private static final String SECONDO_ATTIVO = "1";

	/**
	 * Terzo tab aperto.
	 */
	private static final String TERZO_ATTIVO = "2";

	/**
	 * Id tab attività.
	 */
	private static final String ID_TAB_ATTIVITA = "idTabAttivita";

	/**
	 * Id tab mail.
	 */
	private static final String ID_TAB_MAIL = "idTabMail";

	/**
	 * Id tab fepa.
	 */
	private static final String ID_TAB_FEPA = "idTabFepa";

	/**
	 * Id tab fascicoli.
	 */
	private static final String ID_TAB_FASC = "idTabFasc";

	/**
	 * Id tab faldoni.
	 */
	private static final String ID_TAB_FALD = "idTabFald";

	/**
	 * Classe css.
	 */
	private static final String FRECCIA_IN_GIU = "fa fa-arrow-circle-down";

	/**
	 * Classe css.
	 */
	private static final String FRECCIA_IN_SU = "fa fa-arrow-circle-up";

	/**
	 * Soglia in giorni.
	 */
	private static final long MAX_INTERV_IN_GIORNI_NSD = 180;
	/**
	 * Lunghezza minima per la parola da ricercare.
	 */
	private static final Integer MIN_KEY_LENGTH = 1;

	/**
	 * Numero massimo di righe.
	 */
	private static final Integer MAX_ROW_MAIL = 30;

	/**
	 * Lista cadelle di posta.
	 */
	private List<CasellaPostaDTO> cp;

	/**
	 * Preferenze.
	 */
	private PreferenzeDTO preferenze;

	/**
	 * Tema selezionato.
	 */
	private String temaSelezionato;

	/**
	 * Dirigente delegante.
	 */
	private String dirigenteDelegante;

	/**
	 * Servizio.
	 */
	private IUtenteFacadeSRV utenteSRV;

	/**
	 * Servizio.
	 */
	private IOrganigrammaFacadeSRV orgSRV;

	/**
	 * Servizio.
	 */
	private ICounterFacadeSRV countSRV;

	/**
	 * Servizio.
	 */
	private IMailFacadeSRV mailSRV;

	/**
	 * Servizio.
	 */
	private ICasellePostaliFacadeSRV casellePostaliSRV;

	/**
	 * Servizio.
	 */
	private IRicercaAvanzataDocFacadeSRV ricercaAvanzataDocumentiSRV;

	/**
	 * Servizio.
	 */
	private IRicercaAvanzataFascFacadeSRV ricercaAvanzataFascicoliSRV;

	/**
	 * Servizio.
	 */
	private IDfFacadeSRV ricercaDfSRV;

	/**
	 * Servizio.
	 */
	private ITitolarioFacadeSRV titolarioSRV;

	/**
	 * Servizio.
	 */
	private ITipologiaDocumentoFacadeSRV tipologiaDocumentoSRV;

	/**
	 * Servizio.
	 */
	private IFascicoloFacadeSRV fascicoloSRV;

	/**
	 * Servizio.
	 */
	private IFaldoneFacadeSRV faldoneSRV;

	/**
	 * Servizio.
	 */
	private IAssegnaUfficioFacadeSRV assegnaUfficioSRV;

	/**
	 * Servizio.
	 */
	private INotificaUtenteFacadeSRV notificheUtenteSRV;

	/**
	 * Servizio.
	 */
	private INotificaFacadeSRV notificaSRV;
	
	/**
	 * Lista uffici.	
	 */
	private INodoFacadeSRV nodoSRV;

	/**
	 * Lista uffici.
	 */
	private List<Nodo> listaUffici;

	/**
	 * Lista ruoli.
	 */
	private List<Ruolo> listaRuoli;

	/**
	 * Lista aoo.
	 */
	private List<Aoo> listaAOO;

	/**
	 * Identificativo nuovo ufficio.
	 */
	private Long idNuovoUfficio;

	/**
	 * Identificativo nuovo aoo.
	 */
	private Long idNuovoAoo;

	/**
	 * Identificativo nuovo ruolo.
	 */
	private Long idNuovoRuolo;

	/**
	 * Navigation token.
	 */
	private NavigationTokenEnum activePageInfo;

	/**
	 * Flag renderizzazione pulsante.
	 */
	private Boolean showBtnResp;

	/**
	 * Logo da renderizzare.
	 */
	private LogoMefEnum logoMEF;

	/**
	 * Radice albero.
	 */
	private transient TreeNode root;

	/**
	 * Radice albero faldoni.
	 */
	private transient TreeNode rootFald;

	/**
	 * Menù caselle posta.
	 */
	private transient MenuModel menuCasellePosta;

	/**
	 * Contatori.
	 */
	private CountDTO count;

	/**
	 * Contatori fepa.
	 */
	private CountFepaDTO countFepa;

	/**
	 * Casella posta selezionata.
	 */
	private CasellaMailStatoDTO cartellaMailSelected;

	/**
	 * Indice attivo menù verticale.
	 */
	private String activeIndexVerticalMenu = PRIMO_ATTIVO;

	/**
	 * Indice attivo menù organigramma.
	 */
	private String activeIndexOrgMenu;

	/**
	 * Indice attivo faldoni.
	 */
	private String activeIndexFaldoni;

	/**
	 * Indice attivo fascicoli.
	 */
	private String activeIndexFascicoli;

	/**
	 * Indice attivo tab view.
	 */
	private String activeIndexTabView = PRIMO_ATTIVO;

	/**
	 * Flag visualizza rubrica.
	 */
	private boolean showRubrica;

	/**
	 * Dialog rendere.
	 */
	private DialogRendererComponent dialogRenderer;

	/**
	 * Lista ricerche.
	 */
	private Collection<RicercheEnum> ricercheDisponibili;

	/**
	 * Flag presenza di più uffici.
	 */
	private Boolean haPiuUffici;

	/**
	 * Numero massimo anno per ricercha.
	 */
	private Integer maxYearForSearch;

	/**
	 * Data odierna.
	 */
	private String today;

	/**
	 * Nome ricerca avanzata.
	 */
	private String nomeRicercaAvanzata;

	/**
	 * Datatable per ricerca avanzata salvata.
	 */
	private SimpleDetailDataTableHelper<RicercaAvanzataSalvata> ricercheSalvateDTH;

	/*************************** RICERCA ********************/
	/**
	 * DTO ricerca rapida.
	 */
	private RicercaVeloceRequestDTO request;

	/**
	 * Contenitore di tutte le informazioni che si passano le pagine relative alla
	 * ricerca.
	 */
	private RicercaFormContainer ricercaFormContainer;

	/**
	 * Parametri ricerca fascicoli.
	 */
	private ParamsRicercaFascicoliPregressiDfDTO fascicoliDF;

	/**
	 * Parametri ricerca protocolli.
	 */
	private ParamsRicercaProtocolliPregressiDfDTO protocolliDF;

	/**
	 * Parametri ricerca avanzata documenti.
	 */
	private RicercaAvanzataDocFormDTO formDocumenti;

	/**
	 * Descrizione assegnatario.
	 */
	private String descrAssegnatario;

	/**
	 * Assegnatario copia conforme.
	 */
	private String assegnatarioCopiaConforme;

	/**
	 * Lista nodi.
	 */
	private List<Nodo> alberaturaNodi;

	/**
	 * Radice assegnazione.
	 */
	private DefaultTreeNode rootAssegnazione;

	/**
	 * Radice assegnazione operazione.
	 */
	private DefaultTreeNode rootAssegnazioneOperazione;

	/**
	 * Descrizione assegnatario operazione.
	 */
	private String descrAssegnatarioOperazione;

	// <-- Fascicoli -->

	/**
	 * Ricerca avanzata fascicoli.
	 */
	private RicercaAvanzataFascicoliFormDTO formFascicoli;

	/**
	 * Radice ricerca titolario.
	 */
	private DefaultTreeNode rootTitolarioRicerca;

	/**
	 * Radice titolario.
	 */
	private DefaultTreeNode rootTitolario;

	/**
	 * Descrizione titolario.
	 */
	private String descrizioneTitolario;

	/**
	 * Rasice assegnazione fascicoli.
	 */
	private DefaultTreeNode rootAssegnazionefasc;

	/**
	 * Lista nodi fascicoli.
	 */
	private List<Nodo> alberaturaNodiFasc;

	/**
	 * Descrizione assegnatario fascicoli.
	 */
	private String descrAssegnatariofasc;

	/**
	 * Lista tipi documento NPS.
	 */
	private List<KeyValueDTO> listaTipoDocumentoNPS;

	/**
	 * Parametri ricerca UCB.
	 */
	private RicercaAvanzataUcbBean ricercaUCB;

	/**
	 * Bean ricerca registrazioni ausiliarie.
	 */
	private RicercaRegistrazioniAusiliarieBean ricercaRegistrazioniAusiliarie;
	/*************************** RICERCA FINE ********************/

	private List<NotificheUtenteDTO> notificheUtente;

	/**
	 * Data sincronizzazione.
	 */
	private Date dataSincronizzazione;

	/**
	 * Notofica target.
	 */
	private NotificheUtenteDTO deleteNotificaTarget;

	/**
	 * Flag mostra help page.
	 */
	private Boolean showHelpPage;

	/**
	 * Contatore non classificati.
	 */
	private String counterNumeroNonClassificati;

	/**
	 * Flag carica organigramma.
	 */
	private boolean caricaOrganigramma;

	/**
	 * Flag esistenza nodi figli.
	 */
	private boolean nodiFigliExist;

	/**
	 * Icona organigramma.
	 */
	private String iconOrganigramma;

	/**
	 * Account selezionato.
	 */
	private transient MenuItem accountSelected;

	/**
	 * Parametri account selezionato.
	 */
	private transient Map<String, List<String>> paramsAccountSelected;

	/**
	 * Servizio.
	 */
	private ICodeDocumentiFacadeSRV codeDocumentiSRV;

	/**
	 * Documenti in scadenza.
	 */
	private Map<String, List<String>> docInScadenzaMap;

	/**
	 * Contatore documenti in scadenza.
	 */
	private CountDTO countDocInScadenza;

	/**
	 * Flag render rubrica.
	 */
	private boolean renderRubrica;

	/**
	 * Flag provenienza dashboard.
	 */
	private boolean fromDashboard;

	/**
	 * Flag goto sottoscrizioni.
	 */
	private boolean isGoToSottoscrizioni;

	/**
	 * Identificativo notifica selezionata.
	 */
	private Integer idNotificaSelezionataSott;

	/**
	 * Nodo server.
	 */
	private String nodoServer;

	/**
	 * Template max coda mail.
	 */
	private String templateMaxCodaMail = MAX_ROW_MAIL.toString();

	/**
	 * Stampa etichette.
	 */
	private transient StreamedContent scStampaEtichette;
	
	/**
	 * Preview glifo.
	 */
	private transient StreamedContent previewGlifo; 

	// Paginazione DF

	/**
	 * Index prima pagina.
	 */
	private static final int PRIMAPAGINA = 1;

	/**
	 * Numero pagina attuale protocollo.
	 */
	private int numPaginaAttualeProtocollo;

	/**
	 * Numero pagina attuale fascicolo.
	 */
	private int numPaginaAttualeFascicolo;

	/**
	 * Numero massimo iteratore protocollo.
	 */
	private String maxNumIteratoreProtocollo;

	/**
	 * Numero massimo iteratore fascicolo.
	 */
	private Integer maxNumIteratoreFascicolo;

	/**
	 * Ultima riga protocollo.
	 */
	private String lastRowNumProtocollo;

	/**
	 * Ultima riga fascicolo.
	 */
	private Integer lastRowNumFascicolo;

	/**
	 * Numero pagine totali da ricerca protocollo.
	 */
	private String numPagineTotaliDaRicercaProt;

	/**
	 * Numero pagine totali da ricerca fascicolo.
	 */
	private String numPagineTotaliDaRicercaFasc;

	/**
	 * Numero massimo riga per pagina (protocolli NSD).
	 */
	private Integer maxRowPageProtocolliNsd;

	/**
	 * Template massimo numero di righe per pagina protocolli.
	 */
	private String templateMaxRowPageProtocolli;

	/**
	 * Numero massimo fascicole per pagina.
	 */
	private Integer maxRowPageFascicoliNsd;

	/**
	 * Template massimo numero righe per pagina fascicoli.
	 */
	private String templateMaxRowPageFascicoli;

	/**
	 * Lista tipologie registri.
	 */
	private List<TipologiaRegistroNsdDTO> tipologiaRegistroNsd;

	/**
	 * Lista tipologie documenti.
	 */
	private List<TipologiaRegistroNsdDTO> tipologiaDocumentoNsd;

	/**
	 * Casella postale da cui ripristinare.
	 */
	private String casellaMailRipristina;

	/**
	 * Notifiche non lette.
	 */
	private int notificheNonLette;

	/**
	 * Lista colonne.
	 */
	private transient List<ColonneCodeDTO> listaColonneCodeDTO;

	/**
	 * Flag renderizza dialog stampa.
	 */
	private boolean renderDialogStampa;
	
	/**
	 * Flag di visibilità del tab 'Estratti Conto Trimestrali CCVT'.
	 */
	private boolean estrattiContoVisibile;
	
	/**
	 * Flag di visibilità del tab 'Elenco notifiche e conti consuntivi'.
	 */
	private boolean contiConsuntiviVisibile;
	
	/**
	 * Nodo selezionato.
	 */
	private transient TreeNode nodeSelected;

	/**
	 * Pagina di riferimneto.
	 */
	private NavigationTokenEnum lastPageInfo;

	@PostConstruct
	@Override
	public final void postConstruct() {
		preferenze = new PreferenzeDTO();
		utenteSRV = ApplicationContextProvider.getApplicationContext().getBean(IUtenteFacadeSRV.class);
		orgSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
		casellePostaliSRV = ApplicationContextProvider.getApplicationContext().getBean(ICasellePostaliFacadeSRV.class);
		countSRV = ApplicationContextProvider.getApplicationContext().getBean(ICounterFacadeSRV.class);
		assegnaUfficioSRV = ApplicationContextProvider.getApplicationContext().getBean(IAssegnaUfficioFacadeSRV.class);
		ricercaAvanzataDocumentiSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaAvanzataDocFacadeSRV.class);
		ricercaAvanzataFascicoliSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaAvanzataFascFacadeSRV.class);
		titolarioSRV = ApplicationContextProvider.getApplicationContext().getBean(ITitolarioFacadeSRV.class);
		codeDocumentiSRV = ApplicationContextProvider.getApplicationContext().getBean(ICodeDocumentiFacadeSRV.class);
		notificheUtenteSRV = ApplicationContextProvider.getApplicationContext().getBean(INotificaUtenteFacadeSRV.class);
		fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloFacadeSRV.class);
		faldoneSRV = ApplicationContextProvider.getApplicationContext().getBean(IFaldoneFacadeSRV.class);
		tipologiaDocumentoSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoFacadeSRV.class);
		mailSRV = ApplicationContextProvider.getApplicationContext().getBean(IMailFacadeSRV.class);

		ricercaDfSRV = ApplicationContextProvider.getApplicationContext().getBean(IDfFacadeSRV.class);

		notificaSRV = ApplicationContextProvider.getApplicationContext().getBean(INotificaFacadeSRV.class);
		final IGestioneColonneFacadeSRV gestioneColonneSRV = ApplicationContextProvider.getApplicationContext().getBean(IGestioneColonneFacadeSRV.class);

		nodoSRV = ApplicationContextProvider.getApplicationContext().getBean(INodoFacadeSRV.class);
		
		final String username = getLoggedUsername();
		final String handlerFirma = getParameter(RequestParameter.FIRMA_HANDLER_HEADER_KEY);
		final String tipoFirma = getParameter(RequestParameter.FIRMA_TIPO_HEADER_KEY);

		utente = utenteSRV.getByUsernameAndSignParams(username, handlerFirma, tipoFirma);
		if (utente == null) {
			throw new RedException("Impossibile recuperare l'utente " + username);
		} else {

			LOGGER.info("Utente loggato: " + utente.getUsername());
			if (utente.getSignerInfo() != null) {
				LOGGER.info("Signer [" + utente.getUsername() + "]: " + utente.getSignerInfo().getSigner());
			}

			setPrefUtente();

			logoMEF = LogoMefEnum.getById(utente.getIdLogoMefAoo());

			ricaricaMenuContatori();

			haPiuUffici = utenteSRV.haPiuUfficiAttivi(utente.getId());

			loadCambiaUfficio();

			dirigenteDelegante = utente.getDirigenteNodo();

			calculateDocInScadenza();

			initAllRicerche();

			gotoCustomHomepageFromLogin();

			iconOrganigramma = FRECCIA_IN_GIU;

			nodoServer = HttpUtils.getNodo();

			if (utente.getShowRiferimentoStorico()) {
				tipologiaRegistroNsd = ricercaDfSRV.getTipologiaRegistroNsd(utente.getIdAoo());
				tipologiaDocumentoNsd = ricercaDfSRV.getTipologiaDocumentoNsd(utente.getIdUfficio(), utente.getIdUfficioPadre());
			}
		}
		casellaMailRipristina = null;

		final List<ColonneCodeDTO> listColonne = gestioneColonneSRV.getColonneScelte(utente.getId());
		if (!it.ibm.red.business.utils.CollectionUtils.isEmptyOrNull(listColonne)) {
			listaColonneCodeDTO = new ArrayList<>();
			for (int i = 0; i < ColonneEnum.values().length; i++) {
				final ColonneCodeDTO colDefault = new ColonneCodeDTO(ColonneEnum.values()[i], ColonneEnum.values()[i].getVisibilitaInCodaDefault());
				listaColonneCodeDTO.add(colDefault);
				for (int j = 0; j < listColonne.size(); j++) {
					if (colDefault.getColEnum().getDescrColonna().equals(listColonne.get(j).getColEnum().getDescrColonna())) {
						listaColonneCodeDTO.set(i, listColonne.get(j));
					}
				}
			}
		}
	}

	private void initAllRicerche() {
		// Si inizializzano i parametri delle ricerche
		initParametriRicerche();

		ricercheSalvateDTH = new SimpleDetailDataTableHelper<>();

		// Utilizzato per limitare il campo 'Anno' nelle ricerche. Perchè non deve
		// superare quello attuale
		maxYearForSearch = Calendar.getInstance().get(Calendar.YEAR);
		// Utilizzato nella ricerca 'RED Documenti' per limitare la selezione dei range
		// di date. Perchè non devono superare il giorno attuale
		today = new SimpleDateFormat(DateUtils.DD_MM_YYYY).format(new Date());

		// Form Ricerca Avanzata Documenti
		loadFormRicercaDocumentiRed();

		// Carica Titolario per la ricerca fascicoli
		creaTitolario();

		// Carica Tipologia Documento per la ricerca Dati Protocollo
		listaTipoDocumentoNPS = tipologiaDocumentoSRV.getTipologieDocumentoNPS(utente);
		
		// Ricerche UCB
		ricercaUCB = new RicercaAvanzataUcbBean(utente);

		// Ricerca registrazioni ausiliarie
		ricercaRegistrazioniAusiliarie = new RicercaRegistrazioniAusiliarieBean(utente.getIdAoo());
	}

	private void initParametriRicerche() {
		ricercaFormContainer = new RicercaFormContainer();

		ricercaFormContainer.getProtocolliDF().setMittDestProtocolloNsdDTO(new MittDestProtocolloNsdDTO());
		ricercaFormContainer.getAssegnazioneUCB().setTipoRicerca(RicercaAssegnanteAssegnatarioEnum.values()[0]);
		
		ricercaFormContainer.getFlussiUCB().setAgentiContabili(tipologiaDocumentoSRV.getValuesBySelector(ContoGiudizialeEnum.AGECONT.getMetadatoName(), null, utente.getIdAoo()));
		ricercaFormContainer.getFlussiUCB().setTipologieContabili(tipologiaDocumentoSRV.getValuesBySelector(ContoGiudizialeEnum.TIPO_CONT.getMetadatoName(), null, utente.getIdAoo()));
		
		initEstrattiContoUCBTab();
		initContiConsuntiviUCBTab();
	}
	
	/**
	 * Inizializza il tab della ricerca estratti conto trimestrati CCVT UCB.
	 */
	private void initEstrattiContoUCBTab() {
		String value = PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo()+"."+PropertiesNameEnum.ESTRATTO_CONTO_TRIMESTRALE_CCVT.getKey());
		setEstrattiContoVisibile(!StringUtils.isNullOrEmpty(value));
	}
	
	/**
	 * Inizializza il tab della ricerca elenco notifiche e conti consuntivi UCB.
	 */
	private void initContiConsuntiviUCBTab() {
		String value = PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo()+"."+PropertiesNameEnum.CONTO_CONSUNTIVO_SEDE_ESTERA.getKey());
		String valueFlusso = PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo()+"."+PropertiesNameEnum.CONTO_CONSUNTIVO_SEDE_ESTERA_FLUSSO.getKey());
		setContiConsuntiviVisibile(!StringUtils.isNullOrEmpty(value) || !StringUtils.isNullOrEmpty(valueFlusso));
	}

	private void calculateDocInScadenza() {
		docInScadenzaMap = codeDocumentiSRV.getDocumentiInScadenzaCodeHomePage(utente);

		countDocInScadenza = new CountDTO();
		countDocInScadenza.setDaLavorare(docInScadenzaMap.get(DocumentQueueEnum.DA_LAVORARE.getName()).size());
		countDocInScadenza.setCorriere(docInScadenzaMap.get(DocumentQueueEnum.CORRIERE.getName()).size());
		countDocInScadenza.setCorrierediretto(docInScadenzaMap.get(DocumentQueueEnum.CORRIERE_DIRETTO.getName()).size());
		countDocInScadenza.setCorriereindiretto(docInScadenzaMap.get(DocumentQueueEnum.CORRIERE_INDIRETTO.getName()).size());
		countDocInScadenza.setSospeso(docInScadenzaMap.get(DocumentQueueEnum.SOSPESO.getName()).size());
		countDocInScadenza.setLibroFirma(docInScadenzaMap.get(DocumentQueueEnum.NSD.getName()).size());
		countDocInScadenza.setDaLavorareUCB(docInScadenzaMap.get(DocumentQueueEnum.DA_LAVORARE_UCB.getName()).size());
		countDocInScadenza.setInLavorazioneUCB(docInScadenzaMap.get(DocumentQueueEnum.IN_LAVORAZIONE_UCB.getName()).size());
	}

	/**
	 * Restituisce la map dei documenti in scadenza.
	 * 
	 * @return documenti in scadenza
	 */
	public Map<String, List<String>> getDocInScadenzaMap() {
		return docInScadenzaMap;
	}

	/**
	 * Restituisce le informazioni sul conteggio dei documenti in scadenza.
	 * 
	 * @return CountDTO dei documenti in scadenza
	 */
	public CountDTO getCountDocInScadenza() {
		return countDocInScadenza;
	}

	/**
	 * Consente lo spostamento sull'Homepage custom.
	 */
	public void gotoCustomHomepage() {
		regolaDiNavigazionePreferenze(preferenze.getHomepage(), NavigationTokenEnum.HOMEPAGE);
	}

	private void regolaDiNavigazionePreferenze(final String pagina, final NavigationTokenEnum homepage) {
		activeIndexTabView = PRIMO_ATTIVO;
		if (HomepageEnum.HOMEPAGE.getValue().equals(pagina)) {
			gotoHomepage();
			FacesHelper.navigationRule(homepage);
		}
		if (HomepageEnum.DASHBOARD.getValue().equals(pagina)) {
			if (haAccessoAllaFunzionalita(HomepageEnum.DASHBOARD.getAccFunz())) {
				gotoDashboard();
				FacesHelper.navigationRule(NavigationTokenEnum.DASHBOARD);
			} else {
				gotoHomepage();
				FacesHelper.navigationRule(homepage);
			}
		}
		if (HomepageEnum.LIBROFIRMA.getValue().equals(pagina)) {
			if (haAccessoAllaFunzionalita(HomepageEnum.LIBROFIRMA.getAccFunz())) {
				gotoLibroFirma();
				FacesHelper.navigationRule(NavigationTokenEnum.LIBROFIRMA);
			} else {
				gotoHomepage();
				FacesHelper.navigationRule(homepage);
			}
		}
		if (HomepageEnum.CORRIERE.getValue().equals(pagina)) {
			if (haAccessoAllaFunzionalita(HomepageEnum.CORRIERE.getAccFunz())) {
				if (utente.getIdUfficio().equals(utente.getIdNodoAssegnazioneIndiretta())) {
					gotoCorriereDiretto();
					FacesHelper.navigationRule(NavigationTokenEnum.CORRIERE_DIRETTO);
				} else {
					gotoCorriere();
					FacesHelper.navigationRule(NavigationTokenEnum.CORRIERE);
				}
			} else {
				gotoHomepage();
				FacesHelper.navigationRule(homepage);
			}
		}
		if (HomepageEnum.DA_LAVORARE.getValue().equals(pagina)) {
			if (haAccessoAllaFunzionalita(HomepageEnum.DA_LAVORARE.getAccFunz())) {
				if (utente.isUcb()) {
					gotoDaLavorareUCB();
					FacesHelper.navigationRule(NavigationTokenEnum.DA_LAVORARE_UCB);
				} else {
					gotoDaLavorare();
					FacesHelper.navigationRule(NavigationTokenEnum.DA_LAVORARE);
				}
			} else {
				gotoHomepage();
				FacesHelper.navigationRule(homepage);
			}
		}
		if (HomepageEnum.MAIL.getValue().equals(pagina)) {
			if (haAccessoAllaFunzionalita(HomepageEnum.MAIL.getAccFunz())) {
				createSubMenuMail();
				gotoPostaElettronica();
				FacesHelper.navigationRule(NavigationTokenEnum.SCRIVANIA);
			} else {
				gotoHomepage();
				FacesHelper.navigationRule(homepage);
			}
		}

	}

	/**
	 * Consente lo spostamento sull'Homepage custom dal Login.
	 */
	public void gotoCustomHomepageFromLogin() {
		regolaDiNavigazionePreferenze(preferenze.getHomepage(), NavigationTokenEnum.HOMEPAGE_FROM_LOGIN);

	}

	/**
	 * Restituisce il nome del file per l'export del report.
	 * 
	 * @return nome file export
	 */
	public String getReportExporterName() {
		return DateUtils.dateToString(new Date(), "dd_MM_yyyy_HH_mm_ss");
	}

	/**
	 * Effettua un refresh sui contatori delle code.
	 */
	public void ricaricaMenuContatori() {
		// count menu attivita se attivato
		if (Boolean.TRUE.equals(getAttivitaEnable())) {
			countMenuAttivita();
			calculateDocInScadenza();
		}
		// count manu Fepa se attivato
		if ((Boolean.TRUE.equals(getFattElettEnable()) && preferenze.isGroupFatElettronica()) || utente.isGestioneApplicativa()) {
			countMenuFepa();
		}

	}

	// <-- Gestione Personalizzazione -->
	/**
	 * Gestisce la modifica del tema utente.
	 * 
	 * @param event
	 */
	public void changeUserTheme(final ValueChangeEvent event) {
		temaSelezionato = (String) event.getNewValue();
		for (final ThemeDTO themeEnum : ThemeEnum.getThemes()) {
			if (temaSelezionato.equals(themeEnum.getName())) {
				Integer idTheme = themeEnum.getId();
				preferenze.setIdTheme(idTheme);
			}
		}
	}

	/**
	 * Consente di salvare le preferenze dell'utente rendendo persistenti le
	 * modifiche attuate.
	 */
	public void salva() {
		boolean successChange = false;
		try {
			getPreferenze().setTheme(temaSelezionato);
			successChange = utenteSRV.changeMobilePreference(getUtente().getId(), getPreferenze());
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante : " + e.getMessage());
		}
		if (successChange) {
			showInfoMessage("Preferenze Salvate con Successo");
			utente.setPreferenzeApp(preferenze);
		}
		gotoCustomHomepage();
	}

	/**
	 * Restituisce la lista dei temi disponibili.
	 * 
	 * @return lista temi disponibili
	 */
	public final Collection<ThemeDTO> getThemes() {
		return ThemeFactory.getInstance().getThemes();
	}
	// <-- Fine Gestione Personalizzazione -->

	private void loadCambiaUfficio() {
		listaAOO = assegnaUfficioSRV.getAOOFromIdUtente(utente.getId());
		listaUffici = assegnaUfficioSRV.getNodiFromIdUtenteandIdAOO(utente.getId(), utente.getIdAoo());
		listaRuoli = assegnaUfficioSRV.getRuoliFromIdNodoAndIdAOO(utente.getId(), utente.getIdUfficio(), utente.getIdAoo());
		nodiFigliExist = calculateNodiFigliExists();
		syncUtente();
	}

	private boolean calculateNodiFigliExists() {
		final NodoOrganigrammaDTO firstLevel = new NodoOrganigrammaDTO();
		firstLevel.setIdAOO(utente.getIdAoo());
		firstLevel.setIdNodo(utente.getIdUfficio());
		firstLevel.setCodiceAOO(utente.getCodiceAoo());
		firstLevel.setUtentiVisible(false);
		final List<NodoOrganigrammaDTO> nodi = orgSRV.getOrgForScrivania(firstLevel, utente.getIdUfficio());
		return nodi != null && !nodi.isEmpty();
	}

	/**
	 * Aggiorna i parametri associati a {@link #utente}.
	 */
	public void syncUtente() {
		idNuovoUfficio = utente.getIdUfficio();
		idNuovoAoo = utente.getIdAoo();
		idNuovoRuolo = utente.getIdRuolo();
		listaUffici = assegnaUfficioSRV.getNodiFromIdUtenteandIdAOO(utente.getId(), idNuovoAoo);
		listaRuoli = assegnaUfficioSRV.getRuoliFromIdNodoAndIdAOO(utente.getId(), idNuovoUfficio, idNuovoAoo);
	}

	/**
	 * Carica i ruoli asociati a {@link #utente}.
	 */
	public void caricaRuoli() {
		listaRuoli = assegnaUfficioSRV.getRuoliFromIdNodoAndIdAOO(utente.getId(), idNuovoUfficio, idNuovoAoo);
		idNuovoRuolo = listaRuoli.get(0).getIdRuolo();
	}

	/**
	 * Carica gli uffici disponibili per {@link #utente}.
	 */
	public void caricaUffici() {
		listaUffici = assegnaUfficioSRV.getNodiFromIdUtenteandIdAOO(utente.getId(), idNuovoAoo);
		idNuovoUfficio = listaUffici.get(0).getIdNodo();
		caricaRuoli();
	}

	/**
	 * Gestisce le modifiche del ruolo e dell'ufficio attuate da {@link #utente}
	 * nelle preferenze.
	 */
	public void cambia() {

		Nodo ufficioSelezionato = null;
		Optional<Nodo> optNodo = listaUffici.stream().filter(ufficio -> ufficio.getIdNodo().equals(idNuovoUfficio)).findFirst();
		if (optNodo.isPresent()) {
			ufficioSelezionato = optNodo.get();
		}
		
		if (ufficioSelezionato != null) {
			Ruolo ruoloSelezionato = null;
			Optional<Ruolo> optRuolo = listaRuoli.stream().filter(ruolo -> ruolo.getIdRuolo() == idNuovoRuolo.longValue()).findFirst();
			if (optRuolo.isPresent()) {
				ruoloSelezionato = optRuolo.get();
			}
			
			assegnaUfficioSRV.popolaUtente(utente, ufficioSelezionato, ruoloSelezionato);

			// Logo MEF per l'AOO
			logoMEF = LogoMefEnum.getById(utente.getIdLogoMefAoo());

			if (ufficioSelezionato.getDirigente() != null) {
				dirigenteDelegante = ufficioSelezionato.getDirigente().getCognome() + " " + ufficioSelezionato.getDirigente().getNome();
			}
			nodiFigliExist = calculateNodiFigliExists();
			caricaRicercheDisponibili();
			ricaricaMenuContatori();
			loadOrgUtente();
			creaTitolario();

			if (utente.getShowRiferimentoStorico()) {
				setTipologiaRegistroNsd(ricercaDfSRV.getTipologiaRegistroNsd(utente.getIdAoo()));
				setTipologiaDocumentoNsd(ricercaDfSRV.getTipologiaDocumentoNsd(utente.getIdUfficio(), utente.getIdUfficioPadre()));
			}

			initAllRicerche();

			rinnovaSessione();

		}

		gotoCustomHomepage();
	}

	/**
	 * Gestisce il cambio ufficio.
	 * @param idNodo
	 * @param idRuolo
	 */
	public void cambiaUfficio(Long idNodo, Long idRuolo) {
		List<Nodo> listaUfficiTemp = new ArrayList<>(); 
		List<Ruolo> listaRuoliTemp = new ArrayList<>();
		
		
		try {
			
			Nodo nodo = nodoSRV.getNodo(idNodo);
			listaUfficiTemp = assegnaUfficioSRV.getNodiFromIdUtenteandIdAOO(utente.getId(), nodo.getAoo().getIdAoo());
			
			// Verifica se c'è riscontro con gli uffici già caricati.
			Nodo ufficioSelezionato = null;
			Optional<Nodo> optNodo = listaUfficiTemp.stream().filter(ufficio -> ufficio.getIdNodo().equals(idNodo)).findFirst();
			if (optNodo.isPresent()) {
				ufficioSelezionato = optNodo.get();
			}

			if (ufficioSelezionato != null) {
				
				listaRuoliTemp = assegnaUfficioSRV.getRuoliFromIdNodoAndIdAOO(utente.getId(), ufficioSelezionato.getIdNodo(), ufficioSelezionato.getAoo().getIdAoo());
				
				// Verifica se c'è riscontro con i ruoli già caricati.
				Ruolo ruoloSelezionato = null;
				Optional<Ruolo> optRuolo = listaRuoliTemp.stream().filter(ruolo -> ruolo.getIdRuolo() == idRuolo.longValue()).findFirst();
				if (optRuolo.isPresent()) {
					ruoloSelezionato = optRuolo.get();
				}
				if (ruoloSelezionato != null) {

					// Recupero delle informazioni utente di dettaglio.
					assegnaUfficioSRV.popolaUtente(utente, ufficioSelezionato, ruoloSelezionato);

					// Logo MEF per l'AOO
					logoMEF = LogoMefEnum.getById(utente.getIdLogoMefAoo());

					nodiFigliExist = calculateNodiFigliExists();
					caricaRicercheDisponibili();
					ricaricaMenuContatori();
					loadOrgUtente();
					creaTitolario();

					if (utente.getShowRiferimentoStorico()) {
						setTipologiaRegistroNsd(ricercaDfSRV.getTipologiaRegistroNsd(utente.getIdAoo()));
						setTipologiaDocumentoNsd(ricercaDfSRV.getTipologiaDocumentoNsd(utente.getIdUfficio(),utente.getIdUfficioPadre()));
					}
					
					initAllRicerche();
					rinnovaSessione();
					
				} else {
					showError("Attenzione il ruolo selezionato non ha avuto riscontro con quelli configurati per l'utente.");
				}
				
			} else {
				showError("Attenzione l'ufficio selezionato non ha avuto riscontro con quelli configurati per l'utente.");
			}
				
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante il cambio ufficio " + e.getMessage());
		}
		
	}

	/**
	 * Metodo per il calclo delel ricerche disponibili per utente
	 */
	public void caricaRicercheDisponibili() {
		ricercheDisponibili = new ArrayList<>();

		// Si ciclano le ricerche censite
		for (final RicercheEnum re : RicercheEnum.values()) {

			// Si verifica che l'utente abbia il permesso per visualizzare la singola
			// ricerca
			if (haAccessoAllaFunzionalita(re.getAccesso())) {

				// La si aggiunge alla lista di ricerche disponibili
				ricercheDisponibili.add(re);
			}
		}
	}

	/**
	 * Restituisce true se {@link #utente} ha accesso alla funzionalita definita dal
	 * parametro in ingresso.
	 * 
	 * @param accessoEnum
	 * @return true se l'utente ha accesso alla funzionalita: accessoEnum, false
	 *         altrimenti.
	 */
	public boolean haAccessoAllaFunzionalita(final AccessoFunzionalitaEnum accessoEnum) {
		boolean accede = true;
		final Long permessi = utente.getPermessi();
		final Long permessiAOO = utente.getPermessiAOO();

		if (accessoEnum == null) {
			return accede;
		}

		accede = PermessiUtils.haAccessoAllaFunzionalita(accessoEnum, permessi, permessiAOO, utente.getIdTipoNodo());

		if (AccessoFunzionalitaEnum.CAMBIOUFFICIO.equals(accessoEnum)) {
			accede = haPiuUffici;
		}

		if (AccessoFunzionalitaEnum.RUBRICA.equals(accessoEnum)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.GESTIONE_RUBRICA);
		}

		return accede;

	}

	/**
	 * Metodo utilizzato per aggiornare i contatori dell'accordion selezionato nel
	 * VerticalMenu.
	 * 
	 * @param event
	 */
	public void gestioneContatori(final TabChangeEvent event) {
		final String idActiveTab = event.getTab().getId();

		if (ID_TAB_MAIL.equals(idActiveTab)) {
			createSubMenuMail();
		} else if (ID_TAB_ATTIVITA.equals(idActiveTab)) {
			countMenuAttivita();
		} else if (ID_TAB_FEPA.equals(idActiveTab)) {
			countMenuFepa();
		} else if (ID_TAB_FASC.equals(idActiveTab)) {
			countFascicoliNC();
		} else if (ID_TAB_FALD.equals(idActiveTab)) {
			loadTreeFaldoni();
		}
	}

	// <-- Gestione Faldoni Vertical Menu -->
	/**
	 * Gestisce il caricamento dell'albero dei faldoni.
	 */
	public void loadTreeFaldoni() {
		final TreeNodeFaldoneDTO rootData = new TreeNodeFaldoneDTO("");
		rootFald = addNodoRoot(rootData);

		// Carico automaticamente i faldoni del mio ufficio
		final TreeNode nodeUfficioUtente = addNodoUfficio(utente.getIdUfficio().intValue(), utente.getNodoDesc(), true);
		addFaldoni(nodeUfficioUtente);

		// Aggiungo i sottouffici
		if (utente.getFlagSegreteriaUfficio() == 1 && TipoStrutturaNodoEnum.ISPETTORATO.equals(TipoStrutturaNodoEnum.get(utente.getIdTipoStrutturaUfficio()))) {
			for (final UfficioDTO ufficio : orgSRV.getSottoNodi(utente.getIdUfficio(), utente.getIdAoo())) {
				final TreeNode nodeSottoUfficio = addNodoUfficio(ufficio.getId().intValue(), ufficio.getDescrizione(), false);
				addFaldoni(nodeSottoUfficio); // per inserire i loro faldoni
			}
		}

	}

	private DefaultTreeNode addNodoUfficio(final Integer idUfficio, final String descrizioneUfficio, final Boolean expanded) {
		final TreeNodeFaldoneDTO dataParent = (TreeNodeFaldoneDTO) getRootFald().getData();
		final TreeNodeFaldoneDTO data = new TreeNodeFaldoneDTO(dataParent.getDocumentTitle(), descrizioneUfficio, null, idUfficio, null, null);
		DefaultTreeNode nodoUff = null;
		if (idUfficio.longValue() == utente.getIdUfficio().longValue()) {
			nodoUff = createNode(getRootFald(), TreeNodeTypeEnum.UFFICIO, data, expanded);
		} else {
			nodoUff = createNode(getRootFald(), TreeNodeTypeEnum.UFFICIO_NO_MENU, data, expanded);
		}
		nodoUff.setSelectable(true);
		return nodoUff;
	}

	private DefaultTreeNode addNodoFaldone(final TreeNode parent, final Integer idUfficio, final FaldoneDTO faldone) {
		final TreeNodeFaldoneDTO dataParent = (TreeNodeFaldoneDTO) parent.getData();
		final String tipoParent = parent.getType();
		TreeNodeTypeEnum tipoTreeNode;
		final TreeNodeFaldoneDTO data = new TreeNodeFaldoneDTO(dataParent.getDocumentTitle(), faldone.getOggetto(), faldone.getDescrizioneFaldone(), idUfficio,
				faldone.getAbsolutePath(), faldone.getDocumentTitle());
		if (TreeNodeTypeEnum.UFFICIO.getType().equals(tipoParent) || TreeNodeTypeEnum.FALDONE.getType().equals(tipoParent)) {
			tipoTreeNode = TreeNodeTypeEnum.FALDONE;
		} else {
			tipoTreeNode = TreeNodeTypeEnum.FALDONE_NO_MENU;
		}
		return createNode(parent, tipoTreeNode, data, false);
	}

	/**
	 * Carica tutti i faldoni di un nodo (può essere un ufficio, e quindi stiamo
	 * richiedendo tutti i suoi faldoni, oppure può essere un faldone e quindi
	 * stiamo richiedendo tutti i suoi sotto faldoni) aggiornandolo.
	 * 
	 * @param node nodo selezionato
	 */
	private void addFaldoni(final TreeNode node) {
		if (TreeNodeTypeEnum.FALDONE.getType().equals(node.getType()) || TreeNodeTypeEnum.UFFICIO.getType().equals(node.getType())
				|| TreeNodeTypeEnum.UFFICIO_NO_MENU.getType().equals(node.getType())) {
			final TreeNodeFaldoneDTO data = (TreeNodeFaldoneDTO) node.getData();
			String nameFaldoneParent = null;
			if (TreeNodeTypeEnum.FALDONE.getType().equals(node.getType())) {
				// Se è stato selezionato un faldone devono essere recuperati i sottofaldoni
				nameFaldoneParent = data.getDocumentTitle();
			}
			final Collection<FaldoneDTO> faldoni = faldoneSRV.getFaldoni(getUtente(), data.getIdUfficio(), nameFaldoneParent);
			for (final FaldoneDTO faldone : faldoni) {
				addNodoFaldone(node, data.getIdUfficio(), faldone);
			}
		}
	}

	/**
	 * Metodo chiamato la prima volta che il nodo viene espanso.
	 * 
	 * @param event
	 */
	public void onNodeExpand(final NodeExpandEvent event) {
		final TreeNode expandedNode = event.getTreeNode();
		expandedNode.getChildren().clear();
		addFaldoni(expandedNode);
	}

	/**
	 * Gestisce lo spostamento nella pagina dei faldoni al click del faldone.
	 * 
	 * @param event
	 */
	public void faldoneSelectGotoPage(final NodeSelectEvent event) {

		try {
			if (NavigationTokenEnum.FALDONI.equals(activePageInfo)) {
				LOGGER.error(SOLO_REFRESH);
				final FaldoniBean fb = FacesHelper.getManagedBean(MBean.FALDONI_BEAN);
				fb.onNodeSelect(event.getTreeNode());
				fb.clearFilterDataTable("fascicoli", fb.getFascicoliDTH());
				FacesHelper.executeJS(SHOW_CENTRAL_EAST_SECTION);

			} else {
				LOGGER.error(CON_FORWARD);
				// inserisco le informazioni necessarie alla navigazione alla coda in sessione
				FacesHelper.putObjectInSession(ConstantsWeb.SessionObject.FALDO_OBJ, event.getTreeNode());
				gotoFaldoni();
				FacesHelper.executeJS(SHOW_STATUS_DIALOG_JS);
				// FacesHelper.update("eastSectionForm:idDettaglioDoc"); Aggiornare anche quando
				// viene mostrata la ricerca (presumo per far sparire il dettaglio selezionato)
				FacesHelper.navigationRule(NavigationTokenEnum.FALDONI);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());
		}
	}

	/**
	 * Inizializza la dialog dei faldoni.
	 * 
	 * @param mode
	 */
	public void setupDialog(final String mode) {
		final ModeFaldonaturaEnum modeEnum = ModeFaldonaturaEnum.get(mode);
		final FaldonaturaBean faldonaturaBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.FALDONATURA_BEAN);
		if (ModeFaldonaturaEnum.UPDATE.equals(modeEnum) || ModeFaldonaturaEnum.DELETE.equals(modeEnum)) {
			faldonaturaBean.setFaldoneSelezionatoModifica(ConstantsWeb.MBean.SESSION_BEAN, modeEnum, getSelectedNode());
		} else {
			faldonaturaBean.setFaldoneSelezionatoCreazione(ConstantsWeb.MBean.SESSION_BEAN, modeEnum, getSelectedNode());
		}

		FacesHelper.update("westSectionForm:dlgFaldonaturaPnl");
	}

	/**
	 * Gestisce le operazioni sul faldone.
	 * 
	 * @param modeEnum
	 *            modalita di faldonatura
	 */
	@Override
	public void gestisciOperazioneFaldone(final ModeFaldonaturaEnum modeEnum) {
		final TreeNodeFaldoneDTO dto = (TreeNodeFaldoneDTO) getSelectedNode().getData();
		String parentName = null;
		if (ModeFaldonaturaEnum.CREATE.equals(modeEnum)) {
			getSelectedNode().getChildren().clear();
			parentName = dto.getDocumentTitle();
			final Collection<FaldoneDTO> faldoni = faldoneSRV.getFaldoni(utente, dto.getIdUfficio(), parentName);
			for (final FaldoneDTO faldone : faldoni) {
				addNodoFaldone(getSelectedNode(), dto.getIdUfficio(), faldone);
			}
		} else if (ModeFaldonaturaEnum.UPDATE.equals(modeEnum) || ModeFaldonaturaEnum.DELETE.equals(modeEnum)) {
			getSelectedNode().getParent().getChildren().clear();
			parentName = dto.getParentName();
			final Collection<FaldoneDTO> faldoni = faldoneSRV.getFaldoni(utente, dto.getIdUfficio(), parentName);
			for (final FaldoneDTO faldone : faldoni) {
				addNodoFaldone(getSelectedNode().getParent(), dto.getIdUfficio(), faldone);
			}
		}
	}

	// <-- Fine Gestione Faldoni -->

	// <-- Inizio Contatori -->
	/**
	 * Aggiorna i contatori dei menu attivita. Questo metodo offre la possibilita di
	 * effettuare un aggiornamento della coda centrale conteporaneamente ai
	 * contatori.
	 * 
	 * @param refreshCodaCentrale
	 *            se true aggiorna la coda centrale, se false si limita ad
	 *            aggiornare i contatori
	 */
	public void countMenuAttivita(final boolean refreshCodaCentrale) {
		final Map<Integer, Integer> codaCount = countSRV.getCounterMenuAttivita(utente);
		LOGGER.info("SPEDIZIONE ERRORE - START : " + codaCount);
		count = new CountDTO(codaCount);
		LOGGER.info("SPEDIZIONE ERRORE - END : " + count);
		if (refreshCodaCentrale && (NavigationTokenEnum.isOneKindOfCorriere(activePageInfo) || NavigationTokenEnum.CHIUSE.equals(activePageInfo)
				|| NavigationTokenEnum.ASSEGNATE.equals(activePageInfo) || NavigationTokenEnum.IN_SPEDIZIONE.equals(activePageInfo)
				|| NavigationTokenEnum.GIRO_VISTI.equals(activePageInfo) || NavigationTokenEnum.ATTI.equals(activePageInfo)
				|| NavigationTokenEnum.RISPOSTA.equals(activePageInfo) || NavigationTokenEnum.MOZIONE.equals(activePageInfo)
				|| NavigationTokenEnum.isOneKindOfDaLavorareInLavorazione(activePageInfo) || NavigationTokenEnum.IN_SOSPESO.equals(activePageInfo)
				|| NavigationTokenEnum.STORNATI.equals(activePageInfo) || NavigationTokenEnum.LIBROFIRMA.equals(activePageInfo)
				|| NavigationTokenEnum.LIBRO_FIRMA_DELEGATO.equals(activePageInfo) || NavigationTokenEnum.PROCEDIMENTI_ATTIVI.equals(activePageInfo))) {
			final ListaDocumentiBean listBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
			listBean.refresh(false);
			FacesHelper.update(INFOPANEL_RESOURCE_PATH);
			FacesHelper.update(DOCUMENTI_RESOURCE_PATH);
			FacesHelper.update(DETTAGLIODOCUMENTODD_RESOURCE_PATH);
			FacesHelper.update(ACTIONFILTER_RESOURCE_PATH);
			FacesHelper.update(SEARCHALLBTN_RESOURCE_PATH);
		}
	}

	/**
	 * Consente di aggiornare i contatori dei menu attivita aggiornando, a valle, la
	 * coda centrale.
	 */
	public void countMenuAttivita() {
		countMenuAttivita(true);
	}

	/**
	 * Aggiorna i contatori dei menu FEPA. Questo metodo offre la possibilita di
	 * effettuare un aggiornamento della coda centrale conteporaneamente ai
	 * contatori.
	 * 
	 * @param refreshCodaCentrale - se true aggiorna la coda centrale, se false si
	 *                            limita ad aggiornare i contatori
	 */
	public void countMenuFepa(final boolean refreshCodaCentrale) {
		final Map<DocumentQueueEnum, Integer> codaCountFepa = countSRV.getCounterMenuFepa(utente);
		countFepa = new CountFepaDTO(codaCountFepa);

		if (refreshCodaCentrale && (NavigationTokenEnum.FATTURE_DA_LAVORARE.equals(activePageInfo)||NavigationTokenEnum.FATTURE_IN_LAVORAZIONE.equals(activePageInfo) || NavigationTokenEnum.DD_DA_FIRMARE.equals(activePageInfo)
				|| NavigationTokenEnum.DD_DA_LAVORARE.equals(activePageInfo) ||	NavigationTokenEnum.DD_FIRMATI.equals(activePageInfo) || NavigationTokenEnum.DD_IN_FIRMA.equals(activePageInfo) ||
				NavigationTokenEnum.DSR.equals(activePageInfo) || NavigationTokenEnum.PREPARAZIONE_ALLA_SPEDIZIONE_FEPA.equals(activePageInfo))) {
			final ListaDocumentiBean listBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
			listBean.refresh(false);
			FacesHelper.update(INFOPANEL_RESOURCE_PATH);
			FacesHelper.update(DOCUMENTI_RESOURCE_PATH);
			FacesHelper.update(DETTAGLIODOCUMENTODD_RESOURCE_PATH);
			FacesHelper.update(ACTIONFILTER_RESOURCE_PATH);
			FacesHelper.update(SEARCHALLBTN_RESOURCE_PATH);
		}
	}

	/**
	 * Consente di aggiornare i contatori dei menu FEPA aggiornando, a valle, la
	 * coda centrale.
	 */
	public void countMenuFepa() {
		countMenuFepa(true);
	}

	// <-- Start Gestione Menu Mail -->
	private Map<String, Integer> countMails(final String nomeCasella) {
		return countSRV.getCounterMenuMail(nomeCasella, utente.getFcDTO(), utente.getIdAoo());
	}

	/**
	 * Crea un sub menu delle mail effettuando un refresh della sezione centrale
	 * delle mail.
	 */
	public void createSubMenuMail() {
		createSubMenuMail(true, null, null);
	}

	/**
	 * Gestisce la creazione di un sub-menu delle mail.
	 * 
	 * @param refreshCentralSectionMail - se true aggiorna la sezione centrale delle
	 *                                  mail
	 * @param selectedState
	 * @param cpAttiva
	 * @return returnValue
	 */
	public int createSubMenuMail(final boolean refreshCentralSectionMail, final Integer selectedState, final String cpAttiva) {
		this.menuCasellePosta = new DefaultMenuModel();
		DefaultSubMenu caselle = null;
		int returnValue = 0;
		LOGGER.info("####INFO#### Inizio caricamento caselle postali");
		cp = casellePostaliSRV.getCasellePostali(utente, FunzionalitaEnum.MAIL_LEAF);
		LOGGER.info("####INFO#### Fine caricamento caselle postali");
		if (cp != null && !cp.isEmpty()) {

			// Indice utiizzato per poter definire quale casella è stata selezionata
			// e di conseguenza quale dovrà essere aggiornata
			Integer index = 0;
			LOGGER.info("####INFO#### Inizio FOR");
			for (final CasellaPostaDTO casellaDTO : cp) {
				final Map<String, Integer> statoCount = countMails(casellaDTO.getIndirizzoEmail());
				caselle = new DefaultSubMenu(casellaDTO.getIndirizzoEmail());
				caselle.setStyleClass("casellaPostaleLabel");
				if (casellaDTO.getIndirizzoEmail().equals(cpAttiva)) {
					returnValue = attachMenuItem(caselle, statoCount, casellaDTO.getIndirizzoEmail(), index, selectedState, casellaDTO.isFlagProtocollazioneAutomatica());
				} else {
					attachMenuItem(caselle, statoCount, casellaDTO.getIndirizzoEmail(), index, null, casellaDTO.isFlagProtocollazioneAutomatica());
				}
				this.menuCasellePosta.addElement(caselle);
				index++;
			}

			LOGGER.info("####INFO#### Fine FOR");

			if (NavigationTokenEnum.MAIL.equals(activePageInfo) && refreshCentralSectionMail) {
				LOGGER.info("####INFO#### Refresh centrale START");
				final MailBean mailBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.MAIL_BEAN);
				mailBean.refresh(false, true, false);
				FacesHelper.update("centralSectionForm");
				FacesHelper.update("eastSectionForm");
				LOGGER.info("####INFO#### Refresh centrale END");
			}

		}

		LOGGER.info("####INFO#### Inizio Generate Unique Id");

		this.menuCasellePosta.generateUniqueIds();

		LOGGER.info("####INFO#### Fine Generate Unique Id");

		FacesHelper.executeJS("setTitleMail()");

		return returnValue;
	}

	private int attachMenuItem(final DefaultSubMenu caselle, final Map<String, Integer> statoCount, final String indirizzoMail, final Integer posizioneCasella,
			final Integer selectedState, final boolean flagProtAuto) {

		DefaultMenuItem menuItem = null;
		int returnValue = 0;

		String paramKey = "";

		// label in arrivo
		List<String> paramValue = new ArrayList<>();
		Map<String, List<String>> params = new HashMap<>();
		Integer countArrivo = statoCount.get(StatoMailGUIEnum.INARRIVO.getLabel());
		String labelArrivo = "<b>" + countArrivo + "</b>";
		if (countArrivo < 0) {
			countArrivo = -countArrivo;
			labelArrivo = "<b>" + countArrivo + PLUS_B_HTML_CLOSE_TAG;
		}

		final String htmlLabelArrivo = SPAN + IN_ARRIVO + SPAN_SPAN_CLASS_MENU_COUNTER_GENERIC_TEXT_BOLD + labelArrivo + SPAN_CLOSE_TAG;

		menuItem = new DefaultMenuItem(htmlLabelArrivo);
		paramKey = StatoMailGUIEnum.INARRIVO.getLabel();
		paramValue.add(indirizzoMail);
		paramValue.add(StatoMailGUIEnum.INARRIVO.getValue().toString());
		paramValue.add(posizioneCasella.toString());
		paramValue.add("" + countArrivo);
		if (StatoMailGUIEnum.INARRIVO.getValue().equals(selectedState)) {
			returnValue = countArrivo;
		}
		params.put(paramKey, paramValue);
		menuItem.setParams(params);
		menuItem.setId(posizioneCasella.toString() + "_" + StatoMailGUIEnum.INARRIVO.getIdGUI());
		menuItem.setCommand(GOTO_MAIL_COMMAND_NAME);
		menuItem.setOnstart(SHOW_STATUS_DIALOG_JS);
		menuItem.setOncomplete(SHOW_STATUS_DIALOG_JS);
		menuItem.setIcon("fa fa-inbox");
		menuItem.setEscape(false);
		highlightFolder(menuItem, paramKey, indirizzoMail);
		caselle.addElement(menuItem);
		// label in uscita
		paramValue = new ArrayList<>();
		params = new HashMap<>();
		Integer countInUscita = statoCount.get(StatoMailGUIEnum.INUSCITA.getLabel());
		String labelInUscita = "<b>" + countInUscita + "</b>";
		if (countInUscita < 0) {
			countInUscita = -countInUscita;
			labelInUscita = "<b>" + countInUscita + PLUS_B_HTML_CLOSE_TAG;
		}

		final String htmlLabelInUscita = SPAN + IN_USCITA + SPAN_SPAN_CLASS_MENU_COUNTER_GENERIC_TEXT_BOLD + labelInUscita + SPAN_CLOSE_TAG;

		menuItem = new DefaultMenuItem(htmlLabelInUscita);
		paramKey = StatoMailGUIEnum.INUSCITA.getLabel();
		paramValue.add(indirizzoMail);
		paramValue.add(StatoMailGUIEnum.INUSCITA.getValue().toString());
		paramValue.add(posizioneCasella.toString());
		paramValue.add("" + countInUscita);
		if (StatoMailGUIEnum.INUSCITA.getValue().equals(selectedState)) {
			returnValue = countInUscita;
		}
		params.put(paramKey, paramValue);
		menuItem.setParams(params);
		menuItem.setId(posizioneCasella.toString() + "_" + StatoMailGUIEnum.INUSCITA.getIdGUI());
		menuItem.setCommand(GOTO_MAIL_COMMAND_NAME);
		menuItem.setOnstart(SHOW_STATUS_DIALOG_JS);
		menuItem.setOncomplete(SHOW_STATUS_DIALOG_JS);
		menuItem.setIcon(MAIL_FORWARD_ICON_NAME);
		menuItem.setEscape(false);
		highlightFolder(menuItem, paramKey, indirizzoMail);
		caselle.addElement(menuItem);

		// label rifiutata automatica RIFAUTO START
		if (flagProtAuto) {
			paramValue = new ArrayList<>();
			params = new HashMap<>();
			Integer countRifAuto = statoCount.get(StatoMailGUIEnum.RIFAUTO.getLabel());
			String labelRifAuto = "<b>" + countRifAuto + "</b>";
			if (countRifAuto < 0) {
				countRifAuto = -countRifAuto;
				labelRifAuto = "<b>" + countRifAuto + PLUS_B_HTML_CLOSE_TAG;
			}

			final String htmlLabelRifAuto = SPAN + RIFAUTO + SPAN_SPAN_CLASS_MENU_COUNTER_GENERIC_TEXT_BOLD + labelRifAuto + SPAN_CLOSE_TAG;

			menuItem = new DefaultMenuItem(htmlLabelRifAuto);
			paramKey = StatoMailGUIEnum.RIFAUTO.getLabel();
			paramValue.add(indirizzoMail);
			paramValue.add(StatoMailGUIEnum.RIFAUTO.getValue().toString());
			paramValue.add(posizioneCasella.toString());
			paramValue.add("" + countRifAuto);
			if (StatoMailGUIEnum.RIFAUTO.getValue().equals(selectedState)) {
				returnValue = countRifAuto;
			}
			params.put(paramKey, paramValue);
			menuItem.setParams(params);
			menuItem.setId(posizioneCasella.toString() + "_" + StatoMailGUIEnum.RIFAUTO.getIdGUI());
			menuItem.setCommand(GOTO_MAIL_COMMAND_NAME);
			menuItem.setOnstart(SHOW_STATUS_DIALOG_JS);
			menuItem.setOncomplete(SHOW_STATUS_DIALOG_JS);
			menuItem.setIcon(MAIL_FORWARD_ICON_NAME);
			menuItem.setEscape(false);
			highlightFolder(menuItem, paramKey, indirizzoMail);
			caselle.addElement(menuItem);
		}
		// RIFAUTO END

		// label rifiutata-inoltrata
		paramValue = new ArrayList<>();
		params = new HashMap<>();
		Integer countInoltra = statoCount.get(StatoMailGUIEnum.INOLTRATA.getLabel());
		String labelInoltra = "<b>" + countInoltra + "</b>";
		if (countInoltra < 0) {
			countInoltra = -countInoltra;
			labelInoltra = "<b>" + countInoltra + PLUS_B_HTML_CLOSE_TAG;
		}

		final String htmlLabelInoltra = SPAN + RIFIUTATA_INOLTRATA + SPAN_SPAN_CLASS_MENU_COUNTER_GENERIC_TEXT_BOLD + labelInoltra + SPAN_CLOSE_TAG;

		menuItem = new DefaultMenuItem(htmlLabelInoltra);
		paramKey = StatoMailGUIEnum.INOLTRATA.getLabel();
		paramValue.add(indirizzoMail);
		paramValue.add(StatoMailGUIEnum.INOLTRATA.getValue().toString());
		paramValue.add(posizioneCasella.toString());
		paramValue.add("" + countInoltra);
		if (StatoMailGUIEnum.INOLTRATA.getValue().equals(selectedState)) {
			returnValue = countInoltra;
		}
		params.put(paramKey, paramValue);
		menuItem.setParams(params);
		menuItem.setId(posizioneCasella.toString() + "_" + StatoMailGUIEnum.INOLTRATA.getIdGUI());
		menuItem.setCommand(GOTO_MAIL_COMMAND_NAME);
		menuItem.setOnstart(SHOW_STATUS_DIALOG_JS);
		menuItem.setOncomplete(SHOW_STATUS_DIALOG_JS);
		menuItem.setIcon(MAIL_FORWARD_ICON_NAME);
		menuItem.setEscape(false);
		highlightFolder(menuItem, paramKey, indirizzoMail);
		caselle.addElement(menuItem);

		// label eliminata
		paramValue = new ArrayList<>();
		params = new HashMap<>();
		Integer countElimina = statoCount.get(StatoMailGUIEnum.ELIMINATA.getLabel());
		String labelElimina = "<b>" + countElimina + "</b>";
		if (countElimina < 0) {
			countElimina = -countElimina;
			labelElimina = "<b>" + countElimina + PLUS_B_HTML_CLOSE_TAG;
		}

		final String htmlLabelElimina = SPAN + ELIMINATA + SPAN_SPAN_CLASS_MENU_COUNTER_GENERIC_TEXT_BOLD + labelElimina + SPAN_CLOSE_TAG;

		menuItem = new DefaultMenuItem(htmlLabelElimina);
		paramKey = StatoMailGUIEnum.ELIMINATA.getLabel();
		paramValue.add(indirizzoMail);
		paramValue.add(StatoMailGUIEnum.ELIMINATA.getValue().toString());
		paramValue.add(posizioneCasella.toString());
		paramValue.add("" + countElimina);
		if (StatoMailGUIEnum.ELIMINATA.getValue().equals(selectedState)) {
			returnValue = countElimina;
		}

		params.put(paramKey, paramValue);
		menuItem.setParams(params);
		menuItem.setId(posizioneCasella + "_" + StatoMailGUIEnum.ELIMINATA.getIdGUI());
		menuItem.setCommand(GOTO_MAIL_COMMAND_NAME);
		menuItem.setOnstart(SHOW_STATUS_DIALOG_JS);
		menuItem.setOncomplete(SHOW_STATUS_DIALOG_JS);
		menuItem.setIcon("fa fa-trash");
		menuItem.setEscape(false);
		highlightFolder(menuItem, paramKey, indirizzoMail);
		caselle.addElement(menuItem);

		return returnValue;
	}

	/**
	 * Metodo per l'aggiornamento solo della casella selezionata.
	 * 
	 * @param nomeCasella
	 * 
	 * @author APerquoti
	 */
	public void updateSubMenuMail(final String nomeCasella) {

		try {

			final DefaultSubMenu casella = new DefaultSubMenu(nomeCasella);
			casella.setStyleClass("casellaPostaleLabel");

			final Map<String, Integer> statoCount = countMails(nomeCasella);

			// Recupero della posizione della casella da aggiornare.
			final String indexStr = paramsAccountSelected.get(paramsAccountSelected.keySet().iterator().next()).get(2);

			final Integer position = Integer.parseInt(indexStr);

			attachMenuItem(casella, statoCount, nomeCasella, position, null, false);

			this.menuCasellePosta.getElements().set(position, casella);

		} catch (final Exception e) {
			LOGGER.error("Attenzione errore durante l'aggiornamento della casella di posta " + nomeCasella, e);
			showError("Attenzione errore durante l'aggiornamento della casella di posta " + nomeCasella);
		}

	}

	private void highlightFolder(final DefaultMenuItem menuItem, final String key, final String casellaPostale) {
		if (accountSelected != null) {
			if ((!StringUtils.isNullOrEmpty(casellaMailRipristina) && casellaMailRipristina.equals(casellaPostale))
					|| (accountSelected.getParams().containsKey(key) && accountSelected.getParams().get(key).get(0).equals(casellaPostale))) {
				menuItem.setContainerStyleClass(SELECTED_MENU_STYLECLASS);
				accountSelected = menuItem;
				casellaMailRipristina = null;
			} else {
				menuItem.setContainerStyleClass("");
			}
		}
	}

	// <-- Fine Gestione Menu Mail -->

	// <-- Fine Contatori -->
	/**
	 * Gestisce l'evento di modifica del vertical tab.
	 * 
	 * @param event
	 */
	public void onVerticalTabChange(final TabChangeEvent event) {
		unsetAllAccordionPanel();
		activeIndexVerticalMenu = ((AccordionPanel) event.getComponent()).getActiveIndex();
	}

	// <-- Start Gestione Organigramma -->
	/**
	 * Gestisce il caricamento dell'organigramma dell'utente.
	 * 
	 * @see AccessoFunzionalitaEnum
	 * @param refreshCodaCentrale - se true viene aggiornata la coda centrale, se
	 *                            false si limita a caricare l'organigramma.
	 */
	public void loadOrgUtente(final boolean refreshCodaCentrale) {
		if (haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.ORGANIGRAMMA) && preferenze.isGroupOrganigrama()) {
			root = null;
			loadOrg();
			if (refreshCodaCentrale && (NavigationTokenEnum.ORG_SCRIVANIA.equals(activePageInfo) || NavigationTokenEnum.isOneKindOfCorriere(activePageInfo)
					|| NavigationTokenEnum.CHIUSE.equals(activePageInfo) || NavigationTokenEnum.ASSEGNATE.equals(activePageInfo)
					|| NavigationTokenEnum.IN_SPEDIZIONE.equals(activePageInfo) || NavigationTokenEnum.GIRO_VISTI.equals(activePageInfo))) {
				final ListaDocumentiBean listBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
				listBean.refresh(false);
				FacesHelper.update(INFOPANEL_RESOURCE_PATH);
				FacesHelper.update(DOCUMENTI_RESOURCE_PATH);
				FacesHelper.update(DETTAGLIODOCUMENTODD_RESOURCE_PATH);
				FacesHelper.update(ACTIONFILTER_RESOURCE_PATH);
				FacesHelper.update(SEARCHALLBTN_RESOURCE_PATH);

			}
		}
	}

	/**
	 * Gestisce il caricamento dell'organigramma dell'utente effettuando un refresh
	 * della coda centrale.
	 */
	public void loadOrgUtente() {
		loadOrgUtente(true);
	}

	/**
	 * Gestisce l'evento di modifica dell'organigramma.
	 * 
	 * @param event
	 */
	public void onOrgTabChange(final TabChangeEvent event) {
		unsetAllAccordionPanel();
		activeIndexOrgMenu = ((AccordionPanel) event.getComponent()).getActiveIndex();
		loadOrg();
	}

	private void loadOrg() {
		if (root == null) {

			try {

				// creazione della struttura del tree
				root = new DefaultTreeNode("Root", null);
				root.setSelectable(true);

				// assegnazione del nome del nodo di primo livello
				final NodoOrganigrammaDTO padre = new NodoOrganigrammaDTO();
				padre.setDescrizioneNodo(utente.getNodoDesc());
				padre.setIdAOO(utente.getIdAoo());
				padre.setIdNodo(utente.getIdUfficio());
				padre.setUtentiVisible(true);
				padre.setCodiceAOO(utente.getCodiceAoo());

				// creazione dell'unico nodo necessario (ufficio dell'utente in sessione)
				final TreeNode nodoPadre = new DefaultTreeNode(padre, root);
				nodoPadre.setExpanded(true);
				nodoPadre.setSelectable(false);
				nodoPadre.setType("ROOT");

				// aggiunta delle code e degli utenti
				addCodeUtenti(nodoPadre);
				iconOrganigramma = FRECCIA_IN_GIU;

				if (caricaOrganigramma) {
					iconOrganigramma = FRECCIA_IN_SU;

					// <-- Primo livello -->
					final NodoOrganigrammaDTO firstLevel = new NodoOrganigrammaDTO();
					firstLevel.setIdAOO(utente.getIdAoo());
					firstLevel.setIdNodo(utente.getIdUfficio());
					firstLevel.setCodiceAOO(utente.getCodiceAoo());
					firstLevel.setUtentiVisible(false);

					List<NodoOrganigrammaDTO> listFirstLevel = null;
					listFirstLevel = orgSRV.getOrgForScrivania(firstLevel, utente.getIdUfficio());

					final Map<Long, Integer> ufficiCount = countSRV.countTotSottoUffici(listFirstLevel, utente);

					TreeNode nodoFirstLevel = null;
					for (final NodoOrganigrammaDTO lfl : listFirstLevel) {
						final Integer countUff = ufficiCount.get(lfl.getIdNodo());
						lfl.setCountOrg(countUff);

						nodoFirstLevel = new DefaultTreeNode(lfl, nodoPadre);
						nodoFirstLevel.setSelectable(false);
						nodoFirstLevel.setType("UFF");

						// se risultano dei figli per questoo nodo imposto un nodo fittizio per dare la
						// possibilità in view di essere aperto
						new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoFirstLevel);
					}
					// <-- Fine Primo livello -->

				}
			} catch (final Exception e) {
				LOGGER.error(e.getMessage(), e);
				showError(e);
			}

		}
	}

	/**
	 * Gestisce l'evento "Expand" del nodo recuperato dall'event.
	 * 
	 * @param event
	 */
	public void onOpenNode(final NodeExpandEvent event) {

		final TreeNode nodoPadre = event.getTreeNode();
		expandNode(nodoPadre);

	}

	private void expandNode(final TreeNode nodoPadre) {
		try {
			// agiungo le code ufficio e gli utenti disponibili

			nodoPadre.getChildren().clear();

			addCodeUtenti(nodoPadre);

			final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) nodoPadre.getData();
			final List<NodoOrganigrammaDTO> figli = orgSRV.getOrgForScrivania(nodoExp, utente.getIdUfficio());
			if (!figli.isEmpty()) {

				final Map<Long, Integer> ufficiCount = countSRV.countTotSottoUffici(figli, utente);

				DefaultTreeNode nodoToAdd = null;
				for (final NodoOrganigrammaDTO n : figli) {

					final Integer countUff = ufficiCount.get(n.getIdNodo());
					n.setCountOrg(countUff);

					nodoToAdd = new DefaultTreeNode(n, nodoPadre);
					nodoToAdd.setType("UFF");
					nodoToAdd.setSelectable(false);
					nodoToAdd.setSelected(false);
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Restituisce il nodo selezionato.
	 * 
	 * @return nodo selezionato
	 */
	public TreeNode getNodeSelected() {
		return nodeSelected;
	}

	/**
	 * Imposta il nodo selezionato.
	 * 
	 * @param node
	 */
	public void setNodeSelected(final TreeNode node) {
		TreeNode toSet = node;

		TreeNode nodoVecchio = null;
		TreeNode nodoNuovo = null;

		if (node == null) {
			final OrgObjDTO orgObj = (OrgObjDTO) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.ORG_OBJ, false);
			if (orgObj != null) {
				final TreeNode oldNode = orgObj.getNode();
				oldNode.setSelected(true);
				toSet = oldNode;
				final List<TreeNode> listaDiNodiFinoAllaRoot = new ArrayList<>();

				final TreeNode nodeParent = TreeNodeUtils.findParentAndExpand(oldNode, listaDiNodiFinoAllaRoot);
				final TreeNode formalNode = TreeNodeUtils.find(root, nodeParent.getData());

				if (formalNode != null) {

					if (!listaDiNodiFinoAllaRoot.isEmpty()) {

						nodoNuovo = orgObj.getNode();

						List<TreeNode> listaNodi2 = null;
						formalNode.setExpanded(true);
						expandNode(formalNode);
						listaNodi2 = formalNode.getChildren();

						TreeNode formalNodeCicle = formalNode;

						// il ciclo lo eseguo solo se ho almeno due elementi il lista visto che i primi
						// children li calcolo su
						for (int x = listaDiNodiFinoAllaRoot.size() - 2; x >= 0; x--) {
							for (final TreeNode nodoCiclo : formalNodeCicle.getChildren()) {
								if ("UFF".equals(nodoCiclo.getType()) && ((NodoOrganigrammaDTO) nodoCiclo.getData()).getIdNodo()
										.equals(((NodoOrganigrammaDTO) listaDiNodiFinoAllaRoot.get(x).getData()).getIdNodo())) {
									nodoCiclo.setExpanded(true);
									expandNode(nodoCiclo);
									listaNodi2 = nodoCiclo.getChildren();
									formalNodeCicle = nodoCiclo;

								}
							}

						}

						for (final TreeNode listNodo : listaNodi2) {
							// Utente
							if ((toSet.getData() instanceof NodoOrganigrammaDTO && listNodo.getData() instanceof NodoOrganigrammaDTO
										&& ((NodoOrganigrammaDTO) toSet.getData()).getIdUtente().equals(((NodoOrganigrammaDTO) listNodo.getData()).getIdUtente()))
									// Code
									||  (toSet.getData() instanceof QueueCountDTO && listNodo.getData() instanceof QueueCountDTO
										&& ((QueueCountDTO) toSet.getData()).getQueue().equals(((QueueCountDTO) listNodo.getData()).getQueue()))) {
								nodoVecchio = listNodo;
								break;
							}
						}
					} else {
						nodoVecchio = formalNode;
						nodoNuovo = nodeParent;
					}

					TreeNodeUtils.sostiuisciPerMenuSinistra(nodoVecchio, nodoNuovo);
				}
			}
		}
		nodeSelected = toSet;
	}

	/**
	 * Selezione della voce dell'organigramma.
	 * 
	 * @param event
	 */
	public void onSelectNode(final NodeSelectEvent event) {

		try {
			// recupero l'utente selezionato dall'evento e lo utilizzo poi per l'invocazione
			// del service
			if (nodeSelected != null) {
				nodeSelected.setSelected(false);
			}

			nodeSelected = event.getTreeNode();

			final OrgObjDTO orgObj = creazioneOggettoInSessionePerSelezioneOrganigramma(nodeSelected);
			// inserisco le iformazioni necessarie alla navigazione alla coda in sessione
			FacesHelper.putObjectInSession(ConstantsWeb.SessionObject.ORG_OBJ, orgObj);

			updateCounterPerSelezioneOrganigramma(nodeSelected);

			unsetAllAccordionPanel();
			activeIndexOrgMenu = PRIMO_ATTIVO;

			if (orgObj.getQueueToGo() != null) {
				showBtnResp = orgObj.getQueueToGo().isCodaOperativa();
			} else {
				showBtnResp = Boolean.TRUE;
			}

			// SOLUZIONE: 1 - Eseguo sempre la navigazione alla selezione di un nodo
			// Organigramma
			gotoPage(NavigationTokenEnum.ORG_SCRIVANIA);
			FacesHelper.navigationRule(NavigationTokenEnum.ORG_SCRIVANIA);

		} catch (final Exception e) {
			showError(e);
		}
	}

	/**
	 * Imposta il count dei totali presenti nella lista, affinché venga letto dal
	 * counter della visualizzazione della lista.
	 * 
	 * @param nodeSelected Nodo dell'organigramma selezionato dall'utente.
	 */
	private void updateCounterPerSelezioneOrganigramma(final TreeNode nodeSelected) {
		// imposto il count dell'organigrammaScrivania In questo modo viene visualizzato
		// nella lista.
		Integer orgScrivania = 0;
		if ("CODA".equals(nodeSelected.getType())) {
			final QueueCountDTO elementSelected = (QueueCountDTO) nodeSelected.getData();
			orgScrivania = elementSelected.getCount();
		} else if ("USER".equals(nodeSelected.getType())) {
			final NodoOrganigrammaDTO nodoUtente = (NodoOrganigrammaDTO) nodeSelected.getData();
			orgScrivania = nodoUtente.getCountOrg();
		} else {
			LOGGER.error("Errore selezione organigramma scrivania: tipo nodo non riconosciuto: " + nodeSelected.getType());
			throw new RedException("Errore durante la selezione del nodo organigramma");
		}
		count.setOrgScrivania(orgScrivania);
	}

	/**
	 * Configura l'oggetto da mettere in sessione per passare le informazioni alla
	 * visualizzazione della lista.
	 * 
	 * @param nodeSelected Nodo selezionato dall'utente.
	 * @return Informazioni necessarie a ricordare le operazioni eseguite
	 *         dall'utente, dopo passaggio nella nuova pagina.
	 */
	private OrgObjDTO creazioneOggettoInSessionePerSelezioneOrganigramma(final TreeNode nodeSelected) {
		OrgObjDTO orgObj;
		// preparo pacchetto con informazioni ufficio
		final NodoOrganigrammaDTO nodoPadre = (NodoOrganigrammaDTO) nodeSelected.getParent().getData();

		if ("CODA".equals(nodeSelected.getType())) {
			// preparo pacchetto con informazioni coda
			final QueueCountDTO elementSelected = (QueueCountDTO) nodeSelected.getData();
			// raggruppo le informazioni recuperate uìin precedenza
			orgObj = new OrgObjDTO(elementSelected.getQueue(), nodoPadre);
			// inserisco le iformazioni necessarie alla navigazione alla coda in sessione
		} else if ("USER".equals(nodeSelected.getType())) {
			// preparo pacchetto con informazioni ufficio
			final NodoOrganigrammaDTO nodoUtente = (NodoOrganigrammaDTO) nodeSelected.getData();
			nodoUtente.setDescrizioneNodo(nodoPadre.getDescrizioneNodo());
			// raggruppo le informazioni recuperate uìin precedenza
			orgObj = new OrgObjDTO(null, nodoUtente);
		} else {
			LOGGER.error("Errore selezione organigramma scrivania: tipo nodo non riconosciuto: " + nodeSelected.getType());
			throw new RedException("Errore durante la selezione del nodo organigramma");
		}
		// Imposto il nodo per permettere la visualizzazione del nodo espanso nella
		// nuova pagina.
		orgObj.setNode(nodeSelected);
		return orgObj;
	}

	/**
	 * metodo utilizzato per aggiungere coede eutenti navigabili nel caso il nodo
	 * sia di tipo ufficio.
	 * 
	 * @param nodoPadre
	 */
	public void addCodeUtenti(final TreeNode nodoPadre) {
		final NodoOrganigrammaDTO dto = (NodoOrganigrammaDTO) nodoPadre.getData();

		// <-- Lista Code -->
		// inserisco le code Ufficio richieste
		DefaultTreeNode codaToAdd = null;
		final Map<String, Integer> countTOT = countSRV.countCodeUfficioOrg(dto.getIdNodo(), utente.getFcDTO(), utente.isUcb());
		for (final DocumentQueueEnum codaNodo : DocumentQueueEnum.getQueueByGroup(QueueGroupEnum.UFFICIO)) {

			// Verifica se il nodo in esame è l'ufficio configurato per l'aoo per
			// l'assegnazione indiretta.
			if (utente.isUcb() && (dto.getIdNodo().equals(utente.getIdNodoAssegnazioneIndiretta())) && DocumentQueueEnum.CORRIERE.equals(codaNodo)) {
				// Bipartizione della coda CORRIERE in DIRETTO & INDIRETTO.

				// <-- Corriere diretto -->
				QueueCountDTO qc = new QueueCountDTO(DocumentQueueEnum.CORRIERE_DIRETTO, countTOT.get(DocumentQueueEnum.CORRIERE_DIRETTO.getDisplayName()));
				codaToAdd = new DefaultTreeNode(qc, nodoPadre);
				codaToAdd.setSelectable(true);
				codaToAdd.setType("CODA");
				nodoPadre.getChildren().add(codaToAdd);

				// <-- Corriere indiretto -->
				qc = new QueueCountDTO(DocumentQueueEnum.CORRIERE_INDIRETTO, countTOT.get(DocumentQueueEnum.CORRIERE_INDIRETTO.getDisplayName()));
				codaToAdd = new DefaultTreeNode(qc, nodoPadre);
				codaToAdd.setSelectable(true);
				codaToAdd.setType("CODA");
				nodoPadre.getChildren().add(codaToAdd);

			} else if ((!DocumentQueueEnum.GIRO_VISTI.equals(codaNodo) && !DocumentQueueEnum.CORRIERE_DIRETTO.equals(codaNodo)
					&& !DocumentQueueEnum.CORRIERE_INDIRETTO.equals(codaNodo))
					|| (DocumentQueueEnum.GIRO_VISTI.equals(codaNodo) && haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.GIROVISTI))) {
				// si verifica che per la coda Giro_Visti l'utente abbia i permessi necessari
				// per poterla vedere
				final QueueCountDTO qc = new QueueCountDTO(codaNodo, countTOT.get(codaNodo.getName()));
				codaToAdd = new DefaultTreeNode(qc, nodoPadre);
				codaToAdd.setSelectable(true);
				codaToAdd.setType("CODA");
				nodoPadre.getChildren().add(codaToAdd);
			}

		}
		// <-- Fine Lista Code -->

		// <-- Lista Utenti -->
		// recupero degli utenti appartenenti al suo stesso ufficio

		final List<NodoOrganigrammaDTO> utentiList = orgSRV.getFigliAlberoAssegnatarioPerStornaAUtente(null, dto);
		final Map<Long, Integer> utentiCount = countSRV.countCodeUtentiOrg(dto.getIdNodo(), utente.getId(), utentiList, utente.getFcDTO(), utente.isGestioneApplicativa());
		DefaultTreeNode nodoToAdd = null;
		for (final NodoOrganigrammaDTO utenteNodo : utentiList) {
			final Integer uCount = utentiCount.get(utenteNodo.getIdUtente());
			utenteNodo.setCountOrg(uCount);

			nodoToAdd = new DefaultTreeNode(utenteNodo, nodoPadre);
			nodoToAdd.setSelectable(true);
			nodoToAdd.setType("USER");
			nodoPadre.getChildren().add(nodoToAdd);
		}
		// <-- Fine Lista Utenti -->
	}
	// <-- Fine Gestione Organigramma -->

	/**
	 * Restituisce la lista delle enum associate ad Homepage.
	 * 
	 * @return lista homepage
	 */
	public Collection<HomepageEnum> getHomePages() {
		final Collection<HomepageEnum> homepages = new ArrayList<>();
		homepages.add(HomepageEnum.HOMEPAGE);
		if (haAccessoAllaFunzionalita(HomepageEnum.DASHBOARD.getAccFunz())) {
			homepages.add(HomepageEnum.DASHBOARD);
		}
		if (haAccessoAllaFunzionalita(HomepageEnum.LIBROFIRMA.getAccFunz())) {
			homepages.add(HomepageEnum.LIBROFIRMA);
		}
		if (haAccessoAllaFunzionalita(HomepageEnum.DA_LAVORARE.getAccFunz())) {
			homepages.add(HomepageEnum.DA_LAVORARE);
		}
		if (haAccessoAllaFunzionalita(HomepageEnum.CORRIERE.getAccFunz())) {
			homepages.add(HomepageEnum.CORRIERE);
		}
		if (haAccessoAllaFunzionalita(HomepageEnum.MAIL.getAccFunz())) {
			homepages.add(HomepageEnum.MAIL);
		}
		return homepages;
	}

	/**
	 * Restituisec true se attivita abilitata, false altrimenti.
	 * 
	 * @return true se attivita abilitata, false altrimenti
	 */
	public Boolean getAttivitaEnable() {
		Boolean output = Boolean.TRUE;
		if (!preferenze.isGroupScrivania() && !preferenze.isGroupArchivio() && !preferenze.isGroupUfficio()) {
			output = Boolean.FALSE;
		}

		return output;
	}

	/**
	 * Restituisce true se l'utente ha accesso ad una delle funzionalita della
	 * fatturazione elettronica. Le funzionalita sono: FATTURE, DECRETI, DSR; @see
	 * AccessoFunzionalitaEnum.
	 * 
	 * @return true se l'utente ha accesso alla funzionalita fatturazione, false
	 *         altrimenti
	 */
	public Boolean getFattElettEnable() {
		Boolean output = Boolean.TRUE;
		if (!haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.FATTURE) && !haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.DECRETI)
				&& !haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.DECRETI_DIRIGENTE) && !haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.DSR)) {
			output = Boolean.FALSE;
		}

		return output;
	}

	/**
	 * set delle preferenze utente con dei valori di Default
	 */
	private void setPrefDefault() {
		// di default setto le preferenze su Tema e Homepage
		preferenze.setHomepage(HomepageEnum.HOMEPAGE.getValue());
		preferenze.setTheme(ThemeFactory.getInstance().getDefaultTheme().getName());
		preferenze.setIdTheme(ThemeFactory.getInstance().getDefaultTheme().getId());
		preferenze.setAutocompleteDelay(900);
		preferenze.setTextSize(2);
		preferenze.setPaginaPreferita(HomepageEnum.HOMEPAGE.getValue());
		preferenze.setGroupScrivania(true);
		preferenze.setGroupArchivio(true);
		preferenze.setGroupUfficio(true);
		preferenze.setGroupMail(true);
		preferenze.setGroupFatElettronica(true);
		preferenze.setGroupOrganigrama(true);
	}

	/**
	 * set delle preferenze con quelle salvate dall'utente
	 */
	private void setPrefUtente() {
		if (utente.getPreferenzeApp() != null) {
			// se l'utente ha gia' settato un homepage e/o un tema di default lo imposto
			// nelle preferenze
			preferenze.setHomepage(HomepageEnum.get(utente.getPreferenzeApp().getHomepage()).getValue());

			preferenze.setTheme(ThemeFactory.getInstance().getDefaultTheme().getName());

			if (utente.getPreferenzeApp().getAutocompleteDelay() != null) {
				preferenze.setAutocompleteDelay(utente.getPreferenzeApp().getAutocompleteDelay());
			}
			if (!StringUtils.isNullOrEmpty(utente.getPreferenzeApp().getPaginaPreferita())) {
				preferenze.setPaginaPreferita(utente.getPreferenzeApp().getPaginaPreferita());
			}
			if (utente.getPreferenzeApp().getTextSize() != null) {
				preferenze.setTextSize(utente.getPreferenzeApp().getTextSize());
			}

			// <-- Preferenze Menù Sinistra -->
			preferenze.setGroupScrivania(utente.getPreferenzeApp().isGroupScrivania());
			preferenze.setGroupArchivio(utente.getPreferenzeApp().isGroupArchivio());
			preferenze.setGroupUfficio(utente.getPreferenzeApp().isGroupUfficio());
			preferenze.setGroupMail(utente.getPreferenzeApp().isGroupMail());
			preferenze.setGroupFatElettronica(utente.getPreferenzeApp().isGroupFatElettronica());
			preferenze.setGroupOrganigrama(utente.getPreferenzeApp().isGroupOrganigrama());

			// E' una preferenza a livello di AOO, l'utente non può cambiarla
			preferenze.setNativeReaderPDFEnable(utente.getPreferenzeApp().isNativeReaderPDFEnable());

			preferenze.setFirmaRemotaFirst(utente.getPreferenzeApp().isFirmaRemotaFirst());

		} else {
			setPrefDefault();
		}
	}

	private void openAttivita() {
		unsetAllAccordionPanel();
		activeIndexVerticalMenu = PRIMO_ATTIVO;
	}

	/**
	 * Gestisce l'apertura del panel per la fatturazione elettronica.
	 */
	public void openFatElettronica() {
		unsetAllAccordionPanel();
		activeIndexVerticalMenu = PRIMO_ATTIVO;
		int attivi = 0;

		if (Boolean.TRUE.equals(getAttivitaEnable())) {
			attivi += 1;
		}
		if ((haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.MAIL) && preferenze.isGroupMail()) || NavigationTokenEnum.MAIL.equals(activePageInfo)
				|| utente.isGestioneApplicativa()) {
			attivi += 1;
		}
		if (preferenze.isGroupFatElettronica() && haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.MENUFEPA)) {
			attivi += 1;
		}
		if (attivi == 2) {
			activeIndexVerticalMenu = SECONDO_ATTIVO;
		}
		if (attivi == 3) {
			activeIndexVerticalMenu = TERZO_ATTIVO;
		}
	}

	/**
	 * Gestisce l'apertura della posta elettronica.
	 */
	public void openPostaElettronica() {
		unsetAllAccordionPanel();
		activeIndexVerticalMenu = PRIMO_ATTIVO;
		int attivi = 0;

		if (Boolean.TRUE.equals(getAttivitaEnable())) {
			attivi += 1;
		}
		if ((haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.MAIL) && preferenze.isGroupMail())) {
			attivi += 1;
		}
		if (attivi == 2) {
			activeIndexVerticalMenu = SECONDO_ATTIVO;
		}
		if (attivi == 3) {
			activeIndexVerticalMenu = TERZO_ATTIVO;
		}
	}

	private void openFascicoli() {
		unsetAllAccordionPanel();
		this.activeIndexFascicoli = PRIMO_ATTIVO;
	}

	private void openFaldoni() {
		unsetAllAccordionPanel();
		this.activeIndexFaldoni = PRIMO_ATTIVO;
	}

	private void unsetAllAccordionPanel() {
		activeIndexVerticalMenu = TUTTI_CHIUSI;
		activeIndexOrgMenu = TUTTI_CHIUSI;
		activeIndexFaldoni = TUTTI_CHIUSI;
		activeIndexFascicoli = TUTTI_CHIUSI;
	}

	// <-- INIZIO RICERCA RAPIDA E AVANZATA -->

	private <T> boolean manageResult(final Collection<?> result, final T formRicerca, final String message) {
		boolean isOk = false;

		if (result.isEmpty()) {
			showInfoMessage(message);
		} else {
			final StatoRicercaDTO stDto = new StatoRicercaDTO(formRicerca, result);
			FacesHelper.putObjectInSession(ConstantsWeb.SessionObject.STATO_RICERCA, stDto);
			FacesHelper.executeJS(SHOW_STATUS_DIALOG_JS);
			isOk = true;
		}

		return isOk;
	}

	/**
	 * Consente di utilizzare la funzionalita di ricerca rapida, nel caso in cui la
	 * ricerca produca risultati consente di visualizzarne i risultati. In caso
	 * alternativo viene mostrato il messaggio opportuno.
	 * 
	 * @return url della pagina dei risultati della ricerca rapida
	 */
	public final String ricercaRapida() {
		try {
			if (ricercaFormContainer.getRapida() == null || ricercaFormContainer.getRapida().getKey() == null
					|| ricercaFormContainer.getRapida().getKey().trim().length() < MIN_KEY_LENGTH) {
				showError("La parola da ricercare deve contenere almeno " + MIN_KEY_LENGTH + " caratteri.");
				return null;
			}

			if (ricercaFormContainer.getRapida().isFlagFullText() && ricercaFormContainer.getRapida().getRicercaBEntityEnum() == null) {
				ricercaFormContainer.getRapida().setRicercaBEntityEnum(RicercaPerBusinessEntityEnum.DOCUMENTI);
			}

			final IRicercaFacadeSRV ricercaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaFacadeSRV.class);

			final RicercaResultDTO searchResult = ricercaSRV.ricercaRapidaUtente(ricercaFormContainer.getRapida().getKey(), ricercaFormContainer.getRapida().getAnno(),
					ricercaFormContainer.getRapida().getType(), ricercaFormContainer.getRapida().getRicercaBEntityEnum(), ricercaFormContainer.getRapida().isFlagFullText(),
					utente, false);

			if (searchResult.getDocumenti().isEmpty() && searchResult.getFascicoli().isEmpty()) {
				showInfoMessage("La ricerca non ha prodotto risultati.");
				return null;
			}

			final StatoRicercaDTO stDto = new StatoRicercaDTO(ricercaFormContainer.getRapida(), searchResult);
			FacesHelper.putObjectInSession(ConstantsWeb.SessionObject.STATO_RICERCA, stDto);
			FacesHelper.executeJS(SHOW_STATUS_DIALOG_JS);
			pulisciFormRicerca();

			return gotoRicercaRapida();

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di ricerca generica.", e);
			if (e.getMessage().contains("FNRCB0024E")) {
				showError("Raggiunto il limite massimo di documenti gestibili tramite fulltext.");
			} else {
				showError(e);
			}
		}

		return "";
	}

	/**
	 * Chiama {@link #ricercaDocumentiRed()} configurando il messaggio di errore.
	 * 
	 * @return url risultati ricerca o null se la ricerca non produce risultati
	 */
	public final String ricercaDocumentiRed() {
		return ricercaDocumentiRed("Nessun documento trovato.");
	}

	/**
	 * Consente di effettuare la ricerca avanzata di Red, gestisce eventuali
	 * messaggi di warning o errore riscontrati. Se la ricerca produce risultati
	 * restituisce l'url per lo spostamento alla pagina dei risultati della ricerca.
	 * 
	 * @param noResultFoundMessage
	 * @return url pagina dei risultati o null se la ricerca non produce risultati
	 */
	public final String ricercaDocumentiRed(final String noResultFoundMessage) {
		try {
			final String msg = isFormDocumentiOk();
			if (msg != null) {
				showWarnMessage(msg);
				return null;
			}

			final Collection<MasterDocumentRedDTO> risultatiDocumentiRed = ricercaAvanzataDocumentiSRV.eseguiRicercaDocumenti(ricercaFormContainer.getDocumentiRed(), utente,
					false, ricercaFormContainer.getDocumentiRed().isAnnoDocumentoDisabled());

			// vengono incluse la descrizioni dell'assegnatario utili al momento del ripeti
			// ricerca
			ricercaFormContainer.getDocumentiRed().setDescrAssegnatario(descrAssegnatario);
			ricercaFormContainer.getDocumentiRed().setDescrAssegnatarioOperazione(descrAssegnatarioOperazione);

			if (!manageResult(risultatiDocumentiRed, ricercaFormContainer.getDocumentiRed(), noResultFoundMessage)) {
				return null;
			}

			pulisciFormRicerca();

			return gotoRicercaDocumenti();
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di ricerca avanzata dei documenti.", e);
			if (e.getMessage().contains("FNRCB0024E")) {
				showError("Raggiunto il limite massimo di documenti gestibili tramite fulltext.");
			} else {
				showError(e);
			}
		}

		return "";
	}

	/**
	 * Consente di effettuare la ricerca dei fascicoli di Red, gestisce eventuali
	 * messaggi di warning o errore riscontrati. Se la ricerca produce risultati
	 * restituisce l'url per lo spostamento alla pagina dei risultati della ricerca
	 * dei fascicoli.
	 * 
	 * @return url pagina dei risultati o null se la ricerca non produce risultati
	 */
	public final String ricercaFascicoliRed() {
		try {
			final String msg = isFormFascicoliOk();

			if (msg != null) {
				showWarnMessage(msg);
				return null;
			}

			final Collection<FascicoloDTO> risultatiFascicoliRed = ricercaAvanzataFascicoliSRV.eseguiRicercaFascicoli(ricercaFormContainer.getFascicoliRed(), utente, false);

			if (!manageResult(risultatiFascicoliRed, ricercaFormContainer.getFascicoliRed(), "Nessun Fascicolo trovato.")) {
				return null;
			}

			pulisciFormRicerca();

			return gotoRicercaFascicoli();
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di ricerca avanzata dei fascicoli.", e);
			showError(e);
		}

		return "";
	}

	/**
	 * Consente di effettuare la ricerca fascicoli pregressi del DF, gestisce
	 * eventuali messaggi di warning o errore riscontrati. Se la ricerca produce
	 * risultati restituisce l'url per lo spostamento alla pagina dei risultati
	 * della ricerca.
	 * 
	 * @return url pagina dei risultati della ricerca o null se la ricerca non
	 *         produce alcun risultato
	 */
	public final String ricercaFascicoliPregressiDF() {
		try {
			final String msg = isFormFascicoliPregressiDfOk();
			if (msg != null) {
				showWarnMessage(msg);
				return null;
			}
			final int numRisultatiRicercaFasc = ricercaDfSRV.ricercaFascicoliPregressiCount(ricercaFormContainer.getFascicoliDF(), utente);

			maxRowPageFascicoliNsd = ricercaDfSRV.getmaxRowPageFascicoli();
			templateMaxRowPageFascicoli = String.valueOf(maxRowPageFascicoliNsd);
			final int maxNumPiuUno = maxRowPageFascicoliNsd + 1;

			maxNumIteratoreFascicolo = maxNumPiuUno;

			if ((numRisultatiRicercaFasc % maxRowPageFascicoliNsd > 0)) {
				numPagineTotaliDaRicercaFasc = String.valueOf((numRisultatiRicercaFasc / maxRowPageFascicoliNsd) + 1);
			} else {
				numPagineTotaliDaRicercaFasc = String.valueOf((numRisultatiRicercaFasc / maxRowPageFascicoliNsd));
			}
			lastRowNumFascicolo = PRIMAPAGINA; // per la prima ricerca passo la prima pagina

			// variabile per la paginazione interna in backend
			numPaginaAttualeFascicolo = PRIMAPAGINA;

			final Collection<FascicoliPregressiDTO> risultatiFascicoliPregressi = ricercaDfSRV.ricercaFascicoliPregressi(ricercaFormContainer.getFascicoliDF(), utente,
					lastRowNumFascicolo, maxNumIteratoreFascicolo);
			if (!manageResult(risultatiFascicoliPregressi, ricercaFormContainer.getFascicoliDF(), "Nessun Fascicolo trovato.")) {
				return null;
			}

			fascicoliDF = ricercaFormContainer.getFascicoliDF();
			pulisciFormRicerca();
			return gotoRicercaFascicoliNsd();
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di ricerca avanzata dei fascicoli.", e);
			showError("Errore durante la ricerca fascicoli verso nsd");
			fascicoliDF = new ParamsRicercaFascicoliPregressiDfDTO();
			return null;
		}
	}

	/**
	 * Consente di effettuare la ricerca dei protocolli pregressi del DF, gestisce
	 * eventuali messaggi di warning o errore riscontrati. Se la ricerca produce
	 * risultati restituisce l'url per lo spostamento alla pagina dei risultati
	 * della ricerca.
	 * 
	 * @return url pagina dei risultati ricerca o null se la ricerca non produce
	 *         risultati
	 */
	public final String ricercaProtocolliPregressiDF() {
		try {
			final String msg = isFormProtocolliPregressiDfOk();

			if (msg != null) {
				showWarnMessage(msg);
				return null;
			}

			// Metodo usato per la paginazione
			final int numRisultatiRicercaProt = ricercaDfSRV.ricercaProtocolliPregressiCount(ricercaFormContainer.getProtocolliDF(), utente);

			maxRowPageProtocolliNsd = ricercaDfSRV.getmaxRowPageProtocolli();
			templateMaxRowPageProtocolli = String.valueOf(maxRowPageProtocolliNsd);
			final int maxNumPiuUno = maxRowPageProtocolliNsd + 1;

			maxNumIteratoreProtocollo = String.valueOf(maxNumPiuUno);

			if ((numRisultatiRicercaProt % maxRowPageProtocolliNsd > 0)) {
				numPagineTotaliDaRicercaProt = String.valueOf((numRisultatiRicercaProt / maxRowPageProtocolliNsd) + 1);
			} else {
				numPagineTotaliDaRicercaProt = String.valueOf((numRisultatiRicercaProt / maxRowPageProtocolliNsd));
			}
			lastRowNumProtocollo = String.valueOf(PRIMAPAGINA); // per la prima ricerca passo la prima pagina
			// variabile per la paginazione interna in backend
			numPaginaAttualeProtocollo = PRIMAPAGINA;

			final Collection<ProtocolliPregressiDTO> risultatiProtocolliPregressi = ricercaDfSRV.ricercaProtocolliPregressi(ricercaFormContainer.getProtocolliDF(), utente,
					lastRowNumProtocollo, maxNumIteratoreProtocollo);

			if (!manageResult(risultatiProtocolliPregressi, ricercaFormContainer.getProtocolliDF(), "Nessun Protocollo trovato.")) {
				return null;
			}

			protocolliDF = ricercaFormContainer.getProtocolliDF();
			pulisciFormRicerca();

			return gotoRicercaProtocolliNsd();
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di ricerca avanzata dei protocolli.", e);
			showInfoMessage("Nessun protocollo trovato");
			protocolliDF = new ParamsRicercaProtocolliPregressiDfDTO();
			return null;
		}

	}

	/**
	 * Consente di effettuare la ricerca dei faldoni di Red, gestisce eventuali
	 * messaggi di warning o errore riscontrati. Se la ricerca produce risultati
	 * restituisce l'url per lo spostamento alla pagina dei risultati della ricerca.
	 * 
	 * @return url pagina dei risultati ricerca faldoni o null se la ricerca non
	 *         produce alcun risultato
	 */
	public final String ricercaFaldoniRed() {
		try {
			final String msg = isFormFaldoniOk();

			if (msg != null) {
				showWarnMessage(msg);
				return null;
			}

			final IRicercaAvanzataFaldFacadeSRV ricercaAvanzataFaldoniSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaAvanzataFaldFacadeSRV.class);
			final Collection<FaldoneDTO> risultatiFaldoniRed = ricercaAvanzataFaldoniSRV.eseguiRicercaFaldoni(ricercaFormContainer.getFaldoniRed(), utente, false);

			if (!manageResult(risultatiFaldoniRed, ricercaFormContainer.getFaldoniRed(), "Nessun Faldone trovato.")) {
				return null;
			}

			pulisciFormRicerca();

			return gotoRicercaFaldoni();
		} catch (final Exception e) {
			LOGGER.error(ERROR_RICERCA_GENERICA_MSG, e);
			showError(e);
		}

		return "";
	}

	/**
	 * Consente di effettuare la ricerca FEPA, gestisce eventuali messaggi di
	 * warning o errore riscontrati. Se la ricerca produce risultati restituisce
	 * l'url per lo spostamento alla pagina dei risultati della ricerca.
	 * 
	 * @return url pagina risultati ricerca FEPA o null se la ricerca non produce
	 *         risultati
	 */
	public final String ricercaFepa() {
		try {

			final String msg = isFormFepaOk();

			if (msg != null) {
				showWarnMessage(msg);
				return null;
			}

			final IRicercaFepaFacadeSRV ricercaFepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaFepaFacadeSRV.class);

			final Collection<MasterDocumentRedDTO> risultatiFepa = ricercaFepaSRV.eseguiRicercaFepa(ricercaFormContainer.getFepa(), utente, false);

			if (!manageResult(risultatiFepa, ricercaFormContainer.getFepa(), "Nessun Documento Fepa trovato.")) {
				return null;
			}

			pulisciFormRicerca();

			return gotoRicercaFEPA();
		} catch (final Exception e) {
			LOGGER.error(ERROR_RICERCA_GENERICA_MSG, e);
			showError(e);
		}

		return "";
	}

	/**
	 * Consente di effettuare la ricerca Sigi, gestisce eventuali messaggi di
	 * warning o errore riscontrati. Se la ricerca produce risultati restituisce
	 * l'url per lo spostamento alla pagina dei risultati della ricerca.
	 * 
	 * @return url pagina dei risultati ricerca SIGI o null se la ricerca non
	 *         produce risultati
	 */
	public final String ricercaSigi() {
		try {

			final String msg = isFormSigiOk();

			if (msg != null) {
				showWarnMessage(msg);
				return null;
			}

			final IRicercaSigiFacadeSRV ricercaSigiSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaSigiFacadeSRV.class);
			final Collection<MasterSigiDTO> risultatiSigi = ricercaSigiSRV.eseguiRicercaSigi(ricercaFormContainer.getSigi(), utente, false);

			if (!manageResult(risultatiSigi, ricercaFormContainer.getSigi(), "Nessun Documento Sigi trovato.")) {
				return null;
			}

			pulisciFormRicerca();

			return gotoRicercaSIGI();
		} catch (final Exception e) {
			LOGGER.error(ERROR_RICERCA_GENERICA_MSG, e);
			showError(e);
		}

		return "";
	}

	/**
	 * Consente di effettuare la ricerca dati protocollo, gestisce eventuali
	 * messaggi di warning o errore riscontrati. Se la ricerca produce risultati
	 * restituisce l'url per lo spostamento alla pagina dei risultati della ricerca.
	 * 
	 * @return url pagina dei risultati ricerca dati protocollo o null se la ricerca
	 *         non produce alcun risultato
	 */
	public final String ricercaDatiProtocollo() {
		String msg = null;
		try {
			msg = isFormNpsOkNew();
			if (msg != null) {
				showWarnMessage(msg);
				return null;
			}

			final INpsFacadeSRV ricercaNpsSRV = ApplicationContextProvider.getApplicationContext().getBean(INpsFacadeSRV.class);
			final Collection<ProtocolloNpsDTO> risultatiDatiProtocollo = ricercaNpsSRV.ricercaProtocolli(ricercaFormContainer.getDatiProtocollo(), utente);

			if (!manageResult(risultatiDatiProtocollo, ricercaFormContainer.getDatiProtocollo(), "Nessun Protocollo trovato.")) {
				return null;
			}

			pulisciFormRicerca();

			return gotoRicercaDatiProtocollo();
		} catch (final Exception e) {
			LOGGER.error(ERROR_RICERCA_GENERICA_MSG, e);
			showError(e);
		}

		return "";
	}

	/**
	 * Consente di effettuare la ricerca stampa registro, gestisce eventuali
	 * messaggi di warning o errore riscontrati. Se la ricerca produce risultati
	 * restituisce l'url per lo spostamento alla pagina dei risultati della ricerca.
	 * 
	 * @return url pagina dei risultati ricerca stampa registro o null se la ricerca
	 *         non produce alcun risultato
	 */
	public final String ricercaStampaRegistro() {
		try {
			final String msg = isFormStampaRegistroOk();
			if (msg != null) {
				showWarnMessage(msg);
				return null;
			}

			if (TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo())) {
				showWarnMessage("Non è possibile eseguire l'operazione richiesta in regime di emergenza");
				return null;
			}

			final IRicercaRegistroProtocolloFacadeSRV ricercaRegistroProtocolloSRV = ApplicationContextProvider.getApplicationContext()
					.getBean(IRicercaRegistroProtocolloFacadeSRV.class);
			final Collection<RegistroProtocolloDTO> risultatiStampaRegistro = ricercaRegistroProtocolloSRV
					.eseguiRicercaPerStampaRegistroProtocollo(ricercaFormContainer.getRegistroProtocollo(), utente);

			if (!manageResult(risultatiStampaRegistro, ricercaFormContainer.getRegistroProtocollo(), "Nessun Registro trovato.")) {
				return null;
			}

			pulisciFormRicerca();

			return gotoRicercaStampaRegistro();
		} catch (final BusinessDelegateRuntimeException soap) {
			LOGGER.error("Errore durante la ricerca dei registri di protocollo", soap);
			showError("Errore durante il caricamento dei dati di ricerca: " + soap.getMessage());
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di ricerca dei registri di protocollo", e);
			showError("Errore in fase di ricerca dei registri di protocollo ");
		}

		return "";
	}

	/**
	 * Gestisce il salvataggio dei parametri di ricerca in modo da poterli
	 * visualizzare successivamente.
	 */
	public void salvaFormRicercaDocumentiRed() {
		try {
			if (StringUtils.isNullOrEmpty(nomeRicercaAvanzata)) {
				throw new RedException("Inserire il nome per la ricerca");
			}
			final ParamsRicercaAvanzataDocSalvataDTO ricAvanzata = new ParamsRicercaAvanzataDocSalvataDTO(ricercaFormContainer.getDocumentiRed(),
					formDocumenti.getTipoDocumentoSelected(), descrAssegnatario, assegnatarioCopiaConforme, descrAssegnatarioOperazione);
			ricercaAvanzataDocumentiSRV.salvaRicercaAvanzata(nomeRicercaAvanzata, ricAvanzata, utente);
			nomeRicercaAvanzata = Constants.EMPTY_STRING;
			showInfoMessage("Ricerca salvata correttamente.");
		} catch (final Exception e) {
			showError(e);
		}
	}

	/**
	 * Consente il caricamento delle ricerche salvate da {@link #utente}.
	 */
	public void caricaRicercheSalvate() {
		try {
			resetDTRicercheSalvate();
			final Collection<RicercaAvanzataSalvata> ricSalvate = ricercaAvanzataDocumentiSRV.getAllRicercheSalvateByUtente(utente.getId(), utente.getIdUfficio(),
					utente.getIdRuolo());
			ricercheSalvateDTH.setMasters(ricSalvate);
		} catch (final Exception e) {
			showError(e);
		}
	}

	/**
	 * Consente di valorizzare i campi della ricerca avanzata recuperando le
	 * informazioni dalla ricerca salvata identificata dalla riga del datatable che
	 * gestisce le ricerche salvate da {@link #utente}.
	 * 
	 * @param event
	 */
	public void caricaFormRicercaAvanzata(final ActionEvent event) {
		try {
			final RicercaAvanzataSalvata ricSalvata = getDataTableClickedRow(event);
			final ParamsRicercaAvanzataDocSalvataDTO ricAvanzata = ricercaAvanzataDocumentiSRV.getParamsRicercaAvanzataSalvata(ricSalvata.getId());
			if (ricAvanzata != null) {
				if (alberaturaNodi == null) {
					loadUlterioriFiltriRicercaDocumenti();
				}

				formDocumenti.setTipoDocumentoSelected(ricAvanzata.getTipoDocumento());
				descrAssegnatario = ricAvanzata.getDescrAssegnatario();
				descrAssegnatarioOperazione = ricAvanzata.getDescrAssegnatarioOperazione();
				assegnatarioCopiaConforme = ricAvanzata.getAssegnatarioCopiaConforme();
				ricercaFormContainer.setDocumentiRed(new ParamsRicercaAvanzataDocDTO(ricAvanzata.getParametri()));

				if (ricercaFormContainer.getDocumentiRed().getTipoAssegnazione() != null) {
					ricercaFormContainer.getDocumentiRed().setAssegnatario(ricAvanzata.getParametri().getAssegnatario());
				}

				if (ricercaFormContainer.getDocumentiRed().getIdTipoOperazione() != null) {
					ricercaFormContainer.getDocumentiRed().setAssegnatarioTipoOperazione(ricAvanzata.getParametri().getAssegnatarioTipoOperazione());
				}

				formDocumenti.setTipoDocumentoSelected(ricAvanzata.getTipoDocumento());
				descrAssegnatario = ricAvanzata.getDescrAssegnatario();
				descrAssegnatarioOperazione = ricAvanzata.getDescrAssegnatarioOperazione();
				assegnatarioCopiaConforme = ricAvanzata.getAssegnatarioCopiaConforme();

				ricercaFormContainer.setDocumentiRed(new ParamsRicercaAvanzataDocDTO(ricAvanzata.getParametri()));

			} else {
				descrAssegnatario = Constants.EMPTY_STRING;
				descrAssegnatarioOperazione = Constants.EMPTY_STRING;
				assegnatarioCopiaConforme = Constants.EMPTY_STRING;
				ricercaFormContainer.setDocumentiRed(new ParamsRicercaAvanzataDocDTO());
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il caricamento dei dati di ricerca", e);
			showError(e);
		}

	}

	/**
	 * Gestisce l'eliminazione permanente della ricerca avanzata memorizzata.
	 * 
	 * @param event
	 */
	public void eliminaRicercaAvanzata(final ActionEvent event) {
		try {
			resetDTRicercheSalvate();
			final RicercaAvanzataSalvata ricSalvata = getDataTableClickedRow(event);
			ricercaAvanzataDocumentiSRV.eliminaRicercaAvanzataSalvata(ricSalvata.getId());
			ricercheSalvateDTH.getMasters().remove(ricSalvata);
			showInfoMessage("Ricerca eliminata correttamente.");
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'eliminazione dei dati di ricerca", e);
			showError(e);
		}
	}

	private void resetDTRicercheSalvate() {
		final DataTable d = getDataTableByIdComponent("formRicercheSalvate:ricercheSalvateTbl");
		if (d != null) {
			d.reset();
		}
	}

	private void loadFormRicercaDocumentiRed() {
		try {
			if (formDocumenti == null) {
				formDocumenti = new RicercaAvanzataDocFormDTO();
			}

			ricercaAvanzataDocumentiSRV.initAllCombo(utente, formDocumenti, Boolean.FALSE.booleanValue());
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore nel caricamento della maschera di ricerca avanzata");
		}
	}

	/**
	 * Gestisce il caricamento dei filtri aggiuntivi della funzionalita di ricerca
	 * avanzata.
	 */
	public void loadUlterioriFiltriRicercaDocumenti() {
		try {
			ricercaAvanzataDocumentiSRV.initAllCombo(utente, formDocumenti, Boolean.TRUE.booleanValue());
			ricercaFormContainer.getDocumentiRed().setTutto(true);
			changeCategoria();
			alberaturaNodi = orgSRV.getAlberaturaBottomUp(utente.getIdAoo(), utente.getIdUfficio());
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore nel caricamento di Ulteriori Filtri");
		}

	}

	/**
	 * Gestisce i parametri sulla categoria del documento nel form di ricerca.
	 */
	public void changeCategoria() {
		if (ricercaFormContainer.getDocumentiRed().isTutto()) {
			ricercaFormContainer.getDocumentiRed().setEntrata(true);
			ricercaFormContainer.getDocumentiRed().setUscita(true);
			ricercaFormContainer.getDocumentiRed().setInterno(true);
		} else {
			ricercaFormContainer.getDocumentiRed().setEntrata(false);
			ricercaFormContainer.getDocumentiRed().setUscita(false);
			ricercaFormContainer.getDocumentiRed().setInterno(false);
		}
	}

	/**
	 * Consente di reinizializzare i parametri della ricerca per flussi UCB.
	 */
	public void pulisciRicercaFlussi() {
		ricercaFormContainer.setFlussiUCB(new ParamsRicercaFlussiDTO());
	}

	/**
	 * Consente di reinizializzare i parametri della ricerca rapida.
	 */
	public void pulisciRicercaRapida() {
		ricercaFormContainer.setRapida(new RicercaVeloceRequestDTO());
	}

	/**
	 * Consente di reinizializzare i parametri della ricerca avanzata RED.
	 */
	public void pulisciRicercaRedDoc() {
		ricercaFormContainer.setDocumentiRed(new ParamsRicercaAvanzataDocDTO());
		ricercaFormContainer.getDocumentiRed().setTutto(true);
		changeCategoria();
		descrAssegnatario = Constants.EMPTY_STRING;
		descrAssegnatarioOperazione = Constants.EMPTY_STRING;
	}

	/**
	 * Consente di reinizializzare i parametri della ricerca avanzata dei fascicoli.
	 */
	public void pulisciRicercaRedFascicoli() {
		ricercaFormContainer.setFascicoliRed(new ParamsRicercaAvanzataFascDTO());
		this.descrAssegnatariofasc = Constants.EMPTY_STRING;
		this.descrizioneTitolario = Constants.EMPTY_STRING;
	}

	/**
	 * Consente di reinizializzare i parametri della ricerca avanzata dei faldoni.
	 */
	public void pulisciRicercaRedFaldoni() {
		ricercaFormContainer.setFaldoniRed(new ParamsRicercaAvanzataFaldDTO());
	}

	/**
	 * Consente di reinizializzare i parametri della ricerca avanzata FEPA.
	 */
	public void pulisciRicercaFepa() {
		ricercaFormContainer.setFepa(new ParamsRicercaAvanzataDocDTO());
	}

	/**
	 * Consente di reinizializzare i parametri della ricerca Sigi.
	 */
	public void pulisciRicercaSigi() {
		ricercaFormContainer.setSigi(new ParamsRicercaAvanzataDocDTO());
	}

	/**
	 * Consente di reinizializzare i parametri della ricerca dati protocollo.
	 */
	public void pulisciRicercaDatiProtocollo() {
		ricercaFormContainer.setDatiProtocollo(new ParamsRicercaProtocolloDTO());
	}

	/**
	 * Consente di reinizializzare i parametri della ricerca per registro
	 * protocollo.
	 */
	public void pulisciRicercaStampaRegistro() {
		ricercaFormContainer.setRegistroProtocollo(new ParamsRicercaAvanzataDocDTO());
	}

	/**
	 * Metodo per pulire tutti i form di ricerca.
	 */
	public void pulisciFormRicerca() {
		ricercaFormContainer = new RicercaFormContainer();
		ricercaFormContainer.getDocumentiRed().setTutto(true);
		changeCategoria();
		descrAssegnatario = Constants.EMPTY_STRING;
		descrAssegnatarioOperazione = Constants.EMPTY_STRING;
		descrAssegnatariofasc = Constants.EMPTY_STRING;
		descrizioneTitolario = Constants.EMPTY_STRING;
		ricercaFormContainer.getProtocolliDF().setMittDestProtocolloNsdDTO(new MittDestProtocolloNsdDTO());
		ricercaFormContainer.getAssegnazioneUCB().setTipoRicerca(RicercaAssegnanteAssegnatarioEnum.values()[0]);
		ricercaUCB.getAssegnatarioOrgComponent().reset();
	}

	/**
	 * Consente di reinizializzare i parametri della ricerca fascicoli pregressi DF.
	 */
	public void pulisciRicercaFascicoliPregressiDf() {
		ricercaFormContainer.setFascicoliDF(new ParamsRicercaFascicoliPregressiDfDTO());
	}

	/**
	 * Consente di reinizializzare i parametri della ricerca protocolli pregressi
	 * DF.
	 */
	public void pulisciRicercaProtocolliPregressiDf() {
		ricercaFormContainer.setProtocolliDF(new ParamsRicercaProtocolliPregressiDfDTO());
		ricercaFormContainer.getProtocolliDF().setMittDestProtocolloNsdDTO(new MittDestProtocolloNsdDTO());
	}

	/**
	 * Gestisce l'evento di modifica della tipologia documento.
	 */
	public void onChangeTipologiaDocumento() {
		if (!StringUtils.isNullOrEmpty(ricercaFormContainer.getDocumentiRed().getDescrizioneTipologiaDocumento())) {
			if ("-".equals(ricercaFormContainer.getDocumentiRed().getDescrizioneTipologiaDocumento().trim())) {
				formDocumenti.setComboTipiProcedimento(new ArrayList<>());
				formDocumenti.getComboTipiProcedimento().add(new TipoProcedimentoDTO(new TipoProcedimento(0, "-")));
			} else {
				formDocumenti.setComboTipiProcedimento(ricercaAvanzataDocumentiSRV
						.getTipiProcedimentoByTipologiaDocumento(ricercaFormContainer.getDocumentiRed().getDescrizioneTipologiaDocumento(), utente.getIdAoo()));
			}
		}
		ricercaFormContainer.getDocumentiRed().setDescrizioneTipoProcedimento(null);
	}

	/**
	 * Gestisce l'evento "Select" del nodo definito dall'evento: event.
	 * 
	 * @param event
	 */
	public void onSelectTreeAss(final NodeSelectEvent event) {

		try {
			final NodoOrganigrammaDTO select = (NodoOrganigrammaDTO) event.getTreeNode().getData();

			if ((ricercaFormContainer.getDocumentiRed().getTipoAssegnazione() == TipoAssegnazioneEnum.FIRMA
					|| ricercaFormContainer.getDocumentiRed().getTipoAssegnazione() == TipoAssegnazioneEnum.SIGLA)
					&& (select.getIdUtente() == null || select.getIdUtente() <= 0)) {
				descrAssegnatario = select.getDescrizioneNodo();
				return;
			}

			descrAssegnatario = select.getDescrizioneNodo();

			if (select.getIdUtente() != null) {
				descrAssegnatario += " - " + select.getNomeUtente() + " " + select.getCognomeUtente();
			}

			ricercaFormContainer.getDocumentiRed().setAssegnatario(new AssegnatarioOrganigrammaDTO(select.getIdNodo(), select.getIdUtente()));
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());
		}

	}

	/**
	 * Gestisce la modifica del tipo assegnazione. Viene utilizzato ogni volta che
	 * il tipo assegnazione selezionato nella combobox viene modificato.
	 */
	public void onChangeComboTipoAssegnazione() {
		try {
			formDocumenti.setComboResponsabileCopiaConformeVisible(false);
			ricercaFormContainer.getDocumentiRed().setIdUfficioResponsabileCopiaConforme(null);
			ricercaFormContainer.getDocumentiRed().setIdUtenteResponsabileCopiaConforme(null);

			formDocumenti.setAssegnazioneBtnDisabled(true);
			ricercaFormContainer.getDocumentiRed().setAssegnatario(null);

			if (ricercaFormContainer.getDocumentiRed().getTipoAssegnazione() == null) {

				// l'utente ha deselezionato il tipo assegnazione
				descrAssegnatario = Constants.EMPTY_STRING;
				formDocumenti.setAssegnazioneBtnDisabled(true);
			} else if (TipoAssegnazioneEnum.COPIA_CONFORME.equals(ricercaFormContainer.getDocumentiRed().getTipoAssegnazione())) {

				formDocumenti.setComboResponsabileCopiaConformeVisible(true);
				// Se esistono responsabili seleziono il primo altrimenti ne metto uno vuoto per
				// non creare null point
				ResponsabileCopiaConformeDTO select = null;
				if (!formDocumenti.getComboResponsabileCopiaConforme().isEmpty()) {
					select = formDocumenti.getComboResponsabileCopiaConforme().get(0);
				} else {
					select = new ResponsabileCopiaConformeDTO();
				}
				ricercaFormContainer.getDocumentiRed().setIdUfficioResponsabileCopiaConforme(select.getIdNodo());
				ricercaFormContainer.getDocumentiRed().setIdUtenteResponsabileCopiaConforme(select.getIdUtente());
			} else {

				formDocumenti.setAssegnazioneBtnDisabled(false);
				loadTreeAssegnazione();
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());
		}
	}

	/**
	 * Gestisce il caricamento dell'albero delle assegnazioni.
	 */
	public void loadTreeAssegnazione() {

		try {
			if (ricercaFormContainer.getDocumentiRed().getTipoAssegnazione() == null) {
				showWarnMessage("Selezionare il tipo di assegnazione prima.");
				return;
			}

			final List<Long> alberatura = new ArrayList<>();
			for (int i = alberaturaNodi.size() - 1; i >= 0; i--) {
				alberatura.add(alberaturaNodi.get(i).getIdNodo());
			}

			// prendo da DB i nodi di primo livello
			final NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
			nodoRadice.setIdAOO(utente.getIdAoo());
			nodoRadice.setIdNodo(utente.getIdNodoRadiceAOO());
			nodoRadice.setCodiceAOO(utente.getCodiceAoo());
			nodoRadice.setUtentiVisible(false);
			List<NodoOrganigrammaDTO> primoLivello = null;

			switch (ricercaFormContainer.getDocumentiRed().getTipoAssegnazione()) {
			case COMPETENZA:
				primoLivello = orgSRV.getFigliAlberoAssegnatarioPerCompetenzaUscita(utente, nodoRadice);
				break;

			case FIRMA:
				nodoRadice.setUtentiVisible(true);
				primoLivello = orgSRV.getFigliAlberoAssegnatarioPerFirma(utente.getIdUfficio(), nodoRadice);
				break;

			case SIGLA:
				nodoRadice.setUtentiVisible(true);
				primoLivello = orgSRV.getFigliAlberoAssegnatarioPerSigla(utente.getIdUfficio(), nodoRadice);
				break;

			default:
				primoLivello = orgSRV.getFigliAlberoAssegnatarioPerCompetenzaUscita(utente, nodoRadice);
				break;
			}

			rootAssegnazione = new DefaultTreeNode();
			rootAssegnazione.setExpanded(true);
			rootAssegnazione.setSelectable(false);

			// per ogni nodo di primo livello creo il nodo da mettere nell'albero che ha
			// come padre rootAssegnazionePerCompetenza
			List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
			rootAssegnazione.setChildren(new ArrayList<>());
			DefaultTreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(utente.getCodiceAoo());
				if (n.getIdUtente() == null && (ricercaFormContainer.getDocumentiRed().getTipoAssegnazione() == TipoAssegnazioneEnum.FIRMA
						|| ricercaFormContainer.getDocumentiRed().getTipoAssegnazione() == TipoAssegnazioneEnum.SIGLA)) {
					n.setUtentiVisible(true);
				}
				nodoToAdd = new DefaultTreeNode(n, rootAssegnazione);
				nodoToAdd.setExpanded(true);
				if (n.getIdUtente() == null && alberatura.contains(n.getIdNodo())) {
					nodiDaAprire.add(nodoToAdd);
				}
			}

			final List<DefaultTreeNode> nodiDaAprireTemp = new ArrayList<>();
			for (int i = 0; i < alberatura.size(); i++) {
				// devo fare esattamente un numero di cicli uguale alla lunghezza
				// dell'alberatura
				nodiDaAprireTemp.clear();
				for (final DefaultTreeNode nodo : nodiDaAprire) {
					onOpenTreeAss(nodo);
					nodo.setExpanded(true);
					for (final TreeNode nodoFiglio : nodo.getChildren()) {
						final NodoOrganigrammaDTO nodoFiglioDto = (NodoOrganigrammaDTO) nodoFiglio.getData();
						if (nodoFiglioDto.getIdUtente() == null && alberatura.contains(nodoFiglioDto.getIdNodo())) {
							nodiDaAprireTemp.add((DefaultTreeNode) nodoFiglio);
						}
					}
				}
				nodiDaAprire = new ArrayList<>(nodiDaAprireTemp);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());
		}
	}

	/**
	 * Gestisce l'evento "Expand" associato al nodo identificato dall'evento: event
	 * che fa parte del tree delle assegnazioni.
	 * 
	 * @param event
	 */
	public void onOpenTreeAss(final NodeExpandEvent event) {
		onOpenTreeAss(event.getTreeNode());
	}

	private void onOpenTreeAss(final TreeNode tree) {

		try {
			final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) tree.getData();
			List<NodoOrganigrammaDTO> figli = null;
			switch (ricercaFormContainer.getDocumentiRed().getTipoAssegnazione()) {
			case COMPETENZA:
				figli = orgSRV.getFigliAlberoAssegnatarioPerCompetenzaUscita(utente, nodoExp);
				break;

			case FIRMA:
				figli = orgSRV.getFigliAlberoAssegnatarioPerFirma(utente.getIdUfficio(), nodoExp);
				break;

			case SIGLA:
				figli = orgSRV.getFigliAlberoAssegnatarioPerSigla(utente.getIdUfficio(), nodoExp);
				break;

			default:
				figli = orgSRV.getFigliAlberoAssegnatarioPerCompetenzaUscita(utente, nodoExp);
				break;
			}

			DefaultTreeNode nodoToAdd = null;
			tree.getChildren().clear();
			for (final NodoOrganigrammaDTO n : figli) {
				n.setCodiceAOO(utente.getCodiceAoo());

				if ((n.getIdUtente() == null || n.getIdUtente() == 0) && (ricercaFormContainer.getDocumentiRed().getTipoAssegnazione() == TipoAssegnazioneEnum.FIRMA
						|| ricercaFormContainer.getDocumentiRed().getTipoAssegnazione() == TipoAssegnazioneEnum.SIGLA)) {
					n.setUtentiVisible(true);
				}

				nodoToAdd = new DefaultTreeNode(n, tree);
				if (n.getFigli() > 0 || n.isUtentiVisible()) {
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());
		}

	}

	/**
	 * Gestisce l'evento di modifica del responsabile copia conforme.
	 */
	public void onChangeResponsabileCopiaConforme() {
		Long idUtenteResponsabileCopiaConforme = null;
		Long idUfficioResponsabileCopiaConforme = null;

		if (StringUtils.isNullOrEmpty(assegnatarioCopiaConforme)) {
			ricercaFormContainer.getDocumentiRed().setIdUfficioResponsabileCopiaConforme(idUfficioResponsabileCopiaConforme);
			ricercaFormContainer.getDocumentiRed().setIdUtenteResponsabileCopiaConforme(idUtenteResponsabileCopiaConforme);
			return;
		}

		final String[] splitted = assegnatarioCopiaConforme.split(" ");
		idUfficioResponsabileCopiaConforme = Long.valueOf(splitted[0]);
		if (splitted.length > 1) {
			idUtenteResponsabileCopiaConforme = Long.valueOf(splitted[1]);
		}

		ricercaFormContainer.getDocumentiRed().setIdUfficioResponsabileCopiaConforme(idUfficioResponsabileCopiaConforme);
		ricercaFormContainer.getDocumentiRed().setIdUtenteResponsabileCopiaConforme(idUtenteResponsabileCopiaConforme);
	}

	/**
	 * Gestisce la modifica del tipo operazione all'interno della combobox. Viene
	 * utilizzato per modificare i parametri opportuni associati al tipo operazione.
	 */
	public void onChangeComboTipoOperazione() {
		try {

			ricercaFormContainer.getDocumentiRed().setAssegnatarioTipoOperazione(null);
			formDocumenti.setAssegnazioneOperazioneBtnDisabled(true);

			if (ricercaFormContainer.getDocumentiRed().getIdTipoOperazione() != null && !ricercaFormContainer.getDocumentiRed().getIdTipoOperazione().equals(0)) {
				loadTreeOperazione();
				// Svuoto le eventuale selezione del tipo Assegnazione
				descrAssegnatario = Constants.EMPTY_STRING;
			} else {
				descrAssegnatarioOperazione = Constants.EMPTY_STRING;
				ricercaFormContainer.getDocumentiRed().setTipoAssegnazione(null);
			}

			/** Reset assegnazione ******/
			ricercaFormContainer.getDocumentiRed().setTipoAssegnazione(null);
			descrAssegnatario = null;
			formDocumenti.setAssegnazioneBtnDisabled(true);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());
		}

	}

	/**
	 * Gestisce il caricamento del tree delle operazioni.
	 */
	public void loadTreeOperazione() {

		try {

			final List<Long> alberatura = new ArrayList<>();
			for (int i = alberaturaNodi.size() - 1; i >= 0; i--) {
				alberatura.add(alberaturaNodi.get(i).getIdNodo());
			}

			// prendo da DB i nodi di primo livello
			final NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
			nodoRadice.setIdAOO(utente.getIdAoo());
			nodoRadice.setIdNodo(utente.getIdNodoRadiceAOO());
			nodoRadice.setCodiceAOO(utente.getCodiceAoo());
			nodoRadice.setUtentiVisible(false);
			List<NodoOrganigrammaDTO> primoLivello = null;

			final PermessiEnum permesso = ricercaAvanzataDocumentiSRV.getPermessoByTipoOperazione(ricercaFormContainer.getDocumentiRed().getIdTipoOperazione());
			primoLivello = orgSRV.getFigliAlberoAssegnatarioOperazione(utente.getIdUfficio(), nodoRadice, permesso);

			rootAssegnazioneOperazione = new DefaultTreeNode();
			rootAssegnazioneOperazione.setExpanded(true);
			rootAssegnazioneOperazione.setSelectable(false);

			// per ogni nodo di primo livello creo il nodo da mettere nell'albero che ha
			// come padre rootAssegnazionePerCompetenza
			List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
			rootAssegnazioneOperazione.setChildren(new ArrayList<>());
			DefaultTreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(utente.getCodiceAoo());
				if (n.getIdUtente() == null && (ricercaFormContainer.getDocumentiRed().getTipoAssegnazione() == TipoAssegnazioneEnum.FIRMA
						|| ricercaFormContainer.getDocumentiRed().getTipoAssegnazione() == TipoAssegnazioneEnum.SIGLA)) {
					n.setUtentiVisible(true);
				}
				nodoToAdd = new DefaultTreeNode(n, rootAssegnazioneOperazione);
				nodoToAdd.setExpanded(true);
				if (n.getIdUtente() == null && alberatura.contains(n.getIdNodo())) {
					nodiDaAprire.add(nodoToAdd);
				}
			}

			final List<DefaultTreeNode> nodiDaAprireTemp = new ArrayList<>();
			for (int i = 0; i < alberatura.size(); i++) {
				// devo fare esattamente un numero di cicli uguale alla lunghezza
				// dell'alberatura
				nodiDaAprireTemp.clear();
				for (final DefaultTreeNode nodo : nodiDaAprire) {
					onOpenTreeAssOperazione(nodo);
					nodo.setExpanded(true);
					for (final TreeNode nodoFiglio : nodo.getChildren()) {
						final NodoOrganigrammaDTO nodoFiglioDto = (NodoOrganigrammaDTO) nodoFiglio.getData();
						if (nodoFiglioDto.getIdUtente() == null && alberatura.contains(nodoFiglioDto.getIdNodo())) {
							nodiDaAprireTemp.add((DefaultTreeNode) nodoFiglio);
						}
					}
				}
				nodiDaAprire = new ArrayList<>(nodiDaAprireTemp);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());
		}
	}

	/**
	 * Gestisce l'evento "Expand" associato al nodo identificato dall'evento: event
	 * e che fa parte dell'albero degli assegnatari operazione.
	 * 
	 * @param event
	 */
	public void onOpenTreeAssOperazione(final NodeExpandEvent event) {
		onOpenTreeAssOperazione(event.getTreeNode());
	}

	private void onOpenTreeAssOperazione(final TreeNode tree) {

		try {
			final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) tree.getData();
			List<NodoOrganigrammaDTO> figli = null;

			final PermessiEnum permesso = ricercaAvanzataDocumentiSRV.getPermessoByTipoOperazione(ricercaFormContainer.getDocumentiRed().getIdTipoOperazione());

			if ("SPEDITO".equals(getDescrizioneTipoOperazione(ricercaFormContainer.getDocumentiRed().getIdTipoOperazione()))) {
				figli = orgSRV.getFigliAlberoAssegnatarioOperazione(utente.getIdUfficio(), nodoExp, permesso, false);
			} else {
				figli = orgSRV.getFigliAlberoAssegnatarioOperazione(utente.getIdUfficio(), nodoExp, permesso);
			}

			DefaultTreeNode nodoToAdd = null;
			tree.getChildren().clear();
			for (final NodoOrganigrammaDTO n : figli) {
				n.setCodiceAOO(utente.getCodiceAoo());

				if ((n.getIdUtente() == null || n.getIdUtente() == 0) && (ricercaFormContainer.getDocumentiRed().getTipoAssegnazione() == TipoAssegnazioneEnum.FIRMA
						|| ricercaFormContainer.getDocumentiRed().getTipoAssegnazione() == TipoAssegnazioneEnum.SIGLA)) {
					n.setUtentiVisible(true);
				}

				nodoToAdd = new DefaultTreeNode(n, tree);
				if (n.getFigli() > 0 || n.isUtentiVisible()) {
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());
		}

	}

	/**
	 * Gestisce la selezione di un nodo del tree delle assegnazioni operazione.
	 * 
	 * @param event
	 */
	public void onSelectTreeAssOperazione(final NodeSelectEvent event) {

		try {
			final NodoOrganigrammaDTO select = (NodoOrganigrammaDTO) event.getTreeNode().getData();

			if ((ricercaFormContainer.getDocumentiRed().getTipoAssegnazione() == TipoAssegnazioneEnum.FIRMA
					|| ricercaFormContainer.getDocumentiRed().getTipoAssegnazione() == TipoAssegnazioneEnum.SIGLA)
					&& (select.getIdUtente() == null || select.getIdUtente() <= 0)) {
				return;
			}

			descrAssegnatarioOperazione = select.getDescrizioneNodo();

			if (select.getIdUtente() != null) {
				descrAssegnatarioOperazione += " - " + select.getNomeUtente() + " " + select.getCognomeUtente();
			}

			ricercaFormContainer.getDocumentiRed().setAssegnatarioTipoOperazione(new AssegnatarioOrganigrammaDTO(select.getIdNodo(), select.getIdUtente()));
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());
		}

	}

	/**
	 * Gestisce il caricamento dei filtri aggiuntivi della ricerca avanzata sui
	 * fascicoli.
	 */
	public void loadUlterioriFiltriRicercaFascicoli() {
		try {
			if (formFascicoli == null) {
				formFascicoli = new RicercaAvanzataFascicoliFormDTO();
			}
			ricercaAvanzataFascicoliSRV.initComboFormRicerca(utente, formFascicoli);
			alberaturaNodiFasc = orgSRV.getAlberaturaBottomUp(utente.getIdAoo(), utente.getIdUfficio());

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore nel caricamento di Ulteriori Filtri");
		}

	}

	/**
	 * Effettua la creazione del titolario per l'utente: {@link #utente}.
	 */
	public void creaTitolario() {

		final List<TitolarioDTO> rootListNonDisattivatiRicerca = titolarioSRV.getFigliRadiceNonDisattivati(utente.getIdAoo(), utente.getIdUfficio());
		final List<TitolarioDTO> rootList = titolarioSRV.getFigliRadice(utente.getIdAoo(), utente.getIdUfficio());
		if (rootList == null || rootList.isEmpty()) {
			showWarnMessage("Nessun titolario disponibile.");
			return;
		}

		if (rootListNonDisattivatiRicerca == null || rootListNonDisattivatiRicerca.isEmpty()) {
			showWarnMessage("Nessun titolario disponibile.");
			return;
		}

		try {

			final TitolarioDTO rootfasc = new TitolarioDTO("", "", "Titolario", 0, utente.getIdAoo(), null, null, rootList.size());
			rootTitolario = new DefaultTreeNode(rootfasc, null);
			for (final TitolarioDTO t : rootList) {
				final DefaultTreeNode a = new DefaultTreeNode(t, rootTitolario);
				if (t.getNumeroFigli() > 0) {
					new DefaultTreeNode(null, a);
				}
			}

			final TitolarioDTO rootfascRicerca = new TitolarioDTO("", "", "Titolario", 0, utente.getIdAoo(), null, null, rootListNonDisattivatiRicerca.size());
			rootTitolarioRicerca = new DefaultTreeNode(rootfascRicerca, null);
			for (final TitolarioDTO t : rootListNonDisattivatiRicerca) {
				final DefaultTreeNode a = new DefaultTreeNode(t, rootTitolarioRicerca);
				if (t.getNumeroFigli() > 0) {
					new DefaultTreeNode(null, a);
				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante il caricamento del Titolario: " + e.getMessage());
		}
	}

	/**
	 * Gestisce l'evento "Expand" associato ai nodi del titolario.
	 * 
	 * @param event
	 */
	public void onTitolarioNodeExpand(final NodeExpandEvent event) {
		try {
			final TitolarioDTO toOpen = (TitolarioDTO) event.getTreeNode().getData();
			final List<TitolarioDTO> list = titolarioSRV.getFigliByIndice(utente.getIdAoo(), toOpen.getIndiceClassificazione());
			event.getTreeNode().getChildren().clear();
			if (list != null && !list.isEmpty()) {
				DefaultTreeNode indiceToAdd = null;
				for (final TitolarioDTO t : list) {
					indiceToAdd = new DefaultTreeNode(t, event.getTreeNode());
					if (t.getNumeroFigli() > 0) {
						new DefaultTreeNode(null, indiceToAdd);
					}
				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());

		}
	}

	/**
	 * Gestisce l'evento "Expand" associato al titolario di ricerca.
	 * 
	 * @param event
	 */
	public void onTitolarioNodeExpandRicerca(final NodeExpandEvent event) {
		try {
			final TitolarioDTO toOpen = (TitolarioDTO) event.getTreeNode().getData();
			final List<TitolarioDTO> list = titolarioSRV.getFigliByIndiceNonDisattivati(utente.getIdAoo(), toOpen.getIndiceClassificazione());
			event.getTreeNode().getChildren().clear();
			if (list != null && !list.isEmpty()) {
				DefaultTreeNode indiceToAdd = null;
				for (final TitolarioDTO t : list) {
					indiceToAdd = new DefaultTreeNode(t, event.getTreeNode());
					if (t.getNumeroFigli() > 0) {
						new DefaultTreeNode(null, indiceToAdd);
					}
				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());

		}
	}

	/**
	 * Gestisce l'evento "Select" dei nodi associati al titolario.
	 * 
	 * @param event
	 */
	public void onTitolarioNodeSelect(final NodeSelectEvent event) {

		try {
			final TitolarioDTO titolarioNodoSelected = (TitolarioDTO) event.getTreeNode().getData();
			descrizioneTitolario = titolarioNodoSelected.getIndiceClassificazione() + " " + titolarioNodoSelected.getDescrizione();
			ricercaFormContainer.getFascicoliRed().setTitolario(titolarioNodoSelected.getIndiceClassificazione());

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());
		}
	}

	/**
	 * Gestisce l'evento di selezione dei nodi del titolario con concorrente
	 * spostamento sulla pagina dei fascicoli.
	 * 
	 * @param event
	 */
	public void titolarioSelectGotoPage(final NodeSelectEvent event) {

		try {
			final TitolarioDTO objTito = (TitolarioDTO) event.getTreeNode().getData();
			if (NavigationTokenEnum.FASCICOLI.equals(activePageInfo)) {
				LOGGER.error(SOLO_REFRESH);
				final FascicoliListaBean flb = FacesHelper.getManagedBean(MBean.FASCICOLI_BEAN);
				flb.loadByIndiciClassificazione(objTito.getIndiceClassificazione(), objTito.getDescrizione());
				flb.clearFilterDataTable("treeFaldoni", flb.getFascicoli());
				FacesHelper.executeJS(SHOW_CENTRAL_EAST_SECTION);
			} else {
				LOGGER.error(CON_FORWARD);
				// inserisco le iformazioni necessarie alla navigazione alla coda in sessione
				FacesHelper.putObjectInSession(ConstantsWeb.SessionObject.TITO_OBJ, objTito);
				gotoFascicoli();
				FacesHelper.executeJS(SHOW_STATUS_DIALOG_JS);
				FacesHelper.navigationRule(NavigationTokenEnum.FASCICOLI);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());
		}
	}

	/**
	 * Consente di aggiornare la lista dei fascicoli.
	 */
	public void loadFascicoliNC() {

		if (NavigationTokenEnum.FASCICOLI.equals(activePageInfo)) {
			LOGGER.error(SOLO_REFRESH);
			final FascicoliListaBean flb = FacesHelper.getManagedBean(MBean.FASCICOLI_BEAN);
			flb.loadFascicoliNC();
			flb.clearFilterDataTable("treeFaldoni", flb.getFascicoli());
			FacesHelper.executeJS(SHOW_CENTRAL_EAST_SECTION);
		} else {
			LOGGER.error(CON_FORWARD);
			gotoFascicoli();
			FacesHelper.executeJS(SHOW_STATUS_DIALOG_JS);
			FacesHelper.navigationRule(NavigationTokenEnum.FASCICOLI);
		}

	}

	/**
	 * Esegue il conteggio dei fascicoli non classificati impostandone il valore in
	 * {@link #counterNumeroNonClassificati}.
	 */
	public void countFascicoliNC() {
		final Long d = new Date().getTime();
		Integer countFascicoliNC = fascicoloSRV.getCountFascicoliNonClassificati(utente);
		counterNumeroNonClassificati = countFascicoliNC.toString();
		// controllo se la count restituisce un numero negativo (dovuto a delle logiche
		// di filenet CE)
		if (countFascicoliNC < 0) {
			countFascicoliNC = -countFascicoliNC;
			counterNumeroNonClassificati = countFascicoliNC + "+";
		}
		LOGGER.error("Fine COUNT Fascicoli non classificati -> " + (d - (new Date().getTime())));
	}

	/**
	 * Gestisce l'evento di modifica della tipologia documento di un fascicolo,
	 * aggiornando i tipi procedimento associati.
	 */
	public void onChangeTipologiaDocumentoFasc() {
		try {

			if ("-".equals(ricercaFormContainer.getFascicoliRed().getDescrizioneTipologiaDocumento().trim())) {
				formFascicoli.setComboTipiProcedimento(new ArrayList<>());
				formFascicoli.getComboTipiProcedimento().add(new TipoProcedimentoDTO(new TipoProcedimento(0, "-")));
			} else {
				formFascicoli.setComboTipiProcedimento(ricercaAvanzataFascicoliSRV
						.getTipiProcedimentoByTipologiaDocumento(ricercaFormContainer.getFascicoliRed().getDescrizioneTipologiaDocumento(), utente.getIdAoo()));
			}
			ricercaFormContainer.getFascicoliRed().setDescrizioneTipoProcedimento(null);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante il caricamento dei Tipi Procedimento");
		}

	}

	/**
	 * Gestisce l'evento di modifica dei tipi assegnazione fascicoli dalla combobox.
	 */
	public void onChangeComboTipoAssegnazioneFasc() {
		try {
			this.descrAssegnatariofasc = Constants.EMPTY_STRING;
			ricercaFormContainer.getFascicoliRed().setAssegnatario(null);

			if (ricercaFormContainer.getFascicoliRed().getTipoAssegnazione() != null) {
				loadTreeAssegnazioneFasc();
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());
		}

	}

	/**
	 * Carica il tree delle assegnazioni fascicoli.
	 */
	public void loadTreeAssegnazioneFasc() {

		try {

			final List<Long> alberatura = new ArrayList<>();
			for (int i = alberaturaNodiFasc.size() - 1; i >= 0; i--) {
				alberatura.add(alberaturaNodiFasc.get(i).getIdNodo());
			}

			// prendo da DB i nodi di primo livello
			final NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
			nodoRadice.setIdAOO(utente.getIdAoo());
			nodoRadice.setIdNodo(utente.getIdNodoRadiceAOO());
			nodoRadice.setCodiceAOO(utente.getCodiceAoo());
			nodoRadice.setUtentiVisible(false);
			List<NodoOrganigrammaDTO> primoLivello = null;

			switch (ricercaFormContainer.getFascicoliRed().getTipoAssegnazione()) {
			case COMPETENZA:
				primoLivello = orgSRV.getFigliAlberoAssegnatarioPerCompetenzaUscita(utente, nodoRadice);
				break;

			case FIRMA:
				nodoRadice.setUtentiVisible(true);
				primoLivello = orgSRV.getFigliAlberoAssegnatarioPerFirma(utente.getIdUfficio(), nodoRadice);
				break;

			case SIGLA:
				nodoRadice.setUtentiVisible(true);
				primoLivello = orgSRV.getFigliAlberoAssegnatarioPerSigla(utente.getIdUfficio(), nodoRadice);
				break;

			case SPEDIZIONE:
				primoLivello = orgSRV.getFigliAlberoAssegnatarioPerCompetenzaUscita(utente, nodoRadice);
				break;

			default:
				break;
			}

			rootAssegnazionefasc = new DefaultTreeNode();
			rootAssegnazionefasc.setExpanded(true);
			rootAssegnazionefasc.setSelectable(false);

			// per ogni nodo di primo livello creo il nodo da mettere nell'albero che ha
			// come padre rootAssegnazionePerCompetenza
			List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
			rootAssegnazionefasc.setChildren(new ArrayList<>());
			DefaultTreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(utente.getCodiceAoo());
				if (n.getIdUtente() == null && (ricercaFormContainer.getFascicoliRed().getTipoAssegnazione() == TipoAssegnazioneEnum.FIRMA
						|| ricercaFormContainer.getFascicoliRed().getTipoAssegnazione() == TipoAssegnazioneEnum.SIGLA)) {
					n.setUtentiVisible(true);
				}
				nodoToAdd = new DefaultTreeNode(n, rootAssegnazionefasc);
				nodoToAdd.setExpanded(true);
				if (n.getIdUtente() == null && alberatura.contains(n.getIdNodo())) {
					nodiDaAprire.add(nodoToAdd);
				}
			}

			final List<DefaultTreeNode> nodiDaAprireTemp = new ArrayList<>();
			for (int i = 0; i < alberatura.size(); i++) {
				// devo fare esattamente un numero di cicli uguale alla lunghezza
				// dell'alberatura
				nodiDaAprireTemp.clear();
				for (final DefaultTreeNode nodo : nodiDaAprire) {
					onOpenTreeAssFasc(nodo);
					nodo.setExpanded(true);
					for (final TreeNode nodoFiglio : nodo.getChildren()) {
						final NodoOrganigrammaDTO nodoFiglioDto = (NodoOrganigrammaDTO) nodoFiglio.getData();
						if (nodoFiglioDto.getIdUtente() == null && alberatura.contains(nodoFiglioDto.getIdNodo())) {
							nodiDaAprireTemp.add((DefaultTreeNode) nodoFiglio);
						}
					}
				}
				nodiDaAprire = new ArrayList<>(nodiDaAprireTemp);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());
		}
	}

	/**
	 * Gestisce l'evento di selezione di un nodo associato all'albero che gestisce
	 * le assegnazioni dei fascicoli.
	 * 
	 * @param event
	 */
	public void onSelectTreeAssFasc(final NodeSelectEvent event) {

		try {
			final NodoOrganigrammaDTO select = (NodoOrganigrammaDTO) event.getTreeNode().getData();

			if ((ricercaFormContainer.getFascicoliRed().getTipoAssegnazione() == TipoAssegnazioneEnum.FIRMA
					|| ricercaFormContainer.getFascicoliRed().getTipoAssegnazione() == TipoAssegnazioneEnum.SIGLA)
					&& (select.getIdUtente() == null || select.getIdUtente() <= 0)) {
				return;
			}

			descrAssegnatariofasc = select.getDescrizioneNodo();

			if (select.getIdUtente() != null) {
				descrAssegnatariofasc += " - " + select.getNomeUtente() + " " + select.getCognomeUtente();
			}

			ricercaFormContainer.getFascicoliRed().setAssegnatario(new AssegnatarioOrganigrammaDTO(select.getIdNodo(), select.getIdUtente()));
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());
		}

	}

	/**
	 * Gestisce l'evento di "Expand" di un nodo associato all'albero che gestisce le
	 * assegnazioni dei fascicoli.
	 * 
	 * @param event
	 */
	public void onOpenTreeAssFasc(final NodeExpandEvent event) {
		onOpenTreeAssFasc(event.getTreeNode());
	}

	/**
	 * Gestisce l'apertura dell'albero che gestisce le assegnazioni dei fascicoli.
	 * 
	 * @param tree
	 */
	private void onOpenTreeAssFasc(final TreeNode tree) {

		try {
			final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) tree.getData();
			List<NodoOrganigrammaDTO> figli = null;
			switch (ricercaFormContainer.getFascicoliRed().getTipoAssegnazione()) {
			case COMPETENZA:
				figli = orgSRV.getFigliAlberoAssegnatarioPerCompetenzaUscita(utente, nodoExp);
				break;

			case FIRMA:
				figli = orgSRV.getFigliAlberoAssegnatarioPerFirma(utente.getIdUfficio(), nodoExp);
				break;

			case SIGLA:
				figli = orgSRV.getFigliAlberoAssegnatarioPerSigla(utente.getIdUfficio(), nodoExp);
				break;

			case SPEDIZIONE:
				figli = orgSRV.getFigliAlberoAssegnatarioPerCompetenzaUscita(utente, nodoExp);
				break;

			default:
				break;
			}

			DefaultTreeNode nodoToAdd = null;
			tree.getChildren().clear();
			for (final NodoOrganigrammaDTO n : figli) {
				n.setCodiceAOO(utente.getCodiceAoo());

				if ((n.getIdUtente() == null || n.getIdUtente() == 0) && (ricercaFormContainer.getFascicoliRed().getTipoAssegnazione() == TipoAssegnazioneEnum.FIRMA
						|| ricercaFormContainer.getFascicoliRed().getTipoAssegnazione() == TipoAssegnazioneEnum.SIGLA)) {
					n.setUtentiVisible(true);
				}

				nodoToAdd = new DefaultTreeNode(n, tree);
				if (n.getFigli() > 0 || n.isUtentiVisible()) {
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());
		}

	}

	private String isFormDocumentiOk() {
		String out = null;
		if ((ricercaFormContainer.getDocumentiRed().getAnnoDocumento() <= 0)
				|| ((ricercaFormContainer.getDocumentiRed().getTipoAssegnazione() == null || ricercaFormContainer.getDocumentiRed().getAssegnatario() == null)
						&& ricercaFormContainer.getDocumentiRed().getAssegnatarioTipoOperazione() == null
						&& StringUtils.isNullOrEmpty(ricercaFormContainer.getDocumentiRed().getBarcode()) && ricercaFormContainer.getDocumentiRed().getDataCreazioneA() == null
						&& ricercaFormContainer.getDocumentiRed().getDataCreazioneDa() == null && ricercaFormContainer.getDocumentiRed().getDataProtocolloA() == null
						&& ricercaFormContainer.getDocumentiRed().getDataProtocolloDa() == null && ricercaFormContainer.getDocumentiRed().getDataProtocolloEmergenzaA() == null
						&& ricercaFormContainer.getDocumentiRed().getDataProtocolloEmergenzaDa() == null && ricercaFormContainer.getDocumentiRed().getDataScadenzaA() == null
						&& ricercaFormContainer.getDocumentiRed().getDataScadenzaDa() == null
						&& (StringUtils.isNullOrEmpty(ricercaFormContainer.getDocumentiRed().getDescrizioneTipologiaDocumento())
								|| "-".equals(ricercaFormContainer.getDocumentiRed().getDescrizioneTipologiaDocumento()))
						&& StringUtils.isNullOrEmpty(ricercaFormContainer.getDocumentiRed().getDestinatario())
						&& (ricercaFormContainer.getDocumentiRed().getIdMezzoRicezione() == null
								|| ricercaFormContainer.getDocumentiRed().getIdMezzoRicezione().intValue() <= 0)
						&& (ricercaFormContainer.getDocumentiRed().getIdTipoOperazione() == null
								|| ricercaFormContainer.getDocumentiRed().getIdTipoOperazione().intValue() <= 0)
						&& (ricercaFormContainer.getDocumentiRed().getIdUfficioResponsabileCopiaConforme() == null
								|| ricercaFormContainer.getDocumentiRed().getIdUfficioResponsabileCopiaConforme().intValue() <= 0)
						&& StringUtils.isNullOrEmpty(ricercaFormContainer.getDocumentiRed().getMittente())
						&& (ricercaFormContainer.getDocumentiRed().getModalitaRicercaTesto() == null
								|| StringUtils.isNullOrEmpty(ricercaFormContainer.getDocumentiRed().getParoleRicercaTesto()))
						&& StringUtils.isNullOrEmpty(ricercaFormContainer.getDocumentiRed().getNomeFascicolo())
						&& StringUtils.isNullOrEmpty(ricercaFormContainer.getDocumentiRed().getNote()) && ricercaFormContainer.getDocumentiRed().getNumeroDocumento() == null
						&& ricercaFormContainer.getDocumentiRed().getNumeroLegislatura() == null && ricercaFormContainer.getDocumentiRed().getNumeroProtocolloA() == null
						&& ricercaFormContainer.getDocumentiRed().getNumeroProtocolloDa() == null
						&& ricercaFormContainer.getDocumentiRed().getNumeroProtocolloEmergenzaA() == null
						&& ricercaFormContainer.getDocumentiRed().getNumeroProtocolloEmergenzaDa() == null
						&& StringUtils.isNullOrEmpty(ricercaFormContainer.getDocumentiRed().getNumeroRDP())
						&& StringUtils.isNullOrEmpty(ricercaFormContainer.getDocumentiRed().getOggetto())
						&& ricercaFormContainer.getDocumentiRed().getRifProtocolloMittente() == null)

		) {
			out = "Restringere la ricerca valorizzando alcuni dei campi.";
		} else if (!DateUtils.isRangeOk(ricercaFormContainer.getDocumentiRed().getDataCreazioneDa(), ricercaFormContainer.getDocumentiRed().getDataCreazioneA())) {
			out = ORDINE_DATE_MSG;
		} else if (!DateUtils.isRangeOk(ricercaFormContainer.getDocumentiRed().getDataProtocolloDa(), ricercaFormContainer.getDocumentiRed().getDataProtocolloA())) {
			out = "La data di protocollazione da deve essere antecedente la data di protocollazione a.";
		} else if (!DateUtils.isRangeOk(ricercaFormContainer.getDocumentiRed().getDataScadenzaDa(), ricercaFormContainer.getDocumentiRed().getDataScadenzaA())) {
			out = "La data scadenza da deve essere antecedente la data scadenza a.";
		} else if (!DateUtils.isRangeOk(ricercaFormContainer.getDocumentiRed().getDataProtocolloEmergenzaDa(),
				ricercaFormContainer.getDocumentiRed().getDataProtocolloEmergenzaA())) {
			out = "La data di 'protocollazione Emergenza da' deve essere antecedente la data di 'protocollazione Emergenza a'.";
		} else if (ricercaFormContainer.getDocumentiRed().getIdTipoOperazione() != null && ricercaFormContainer.getDocumentiRed().getIdTipoOperazione() > 0
				&& ricercaFormContainer.getDocumentiRed().getAssegnatarioTipoOperazione() == null) {
			out = "Selezionare la struttura da associare al tipo operazione";
		} else if (ricercaFormContainer.getDocumentiRed().getTipoAssegnazione() != null && ricercaFormContainer.getDocumentiRed().getAssegnatario() == null) {
			out = "Selezionare la struttura da associare al tipo assegnazione";
		}

		return out;
	}

	private String isFormFascicoliOk() {
		String out = null;
		if (!(ricercaFormContainer.getFascicoliRed().getAssegnatario() != null || ricercaFormContainer.getFascicoliRed().getDataChiusuraA() != null
				|| ricercaFormContainer.getFascicoliRed().getDataChiusuraDa() != null || ricercaFormContainer.getFascicoliRed().getDataCreazioneA() != null
				|| ricercaFormContainer.getFascicoliRed().getDataCreazioneDa() != null
				|| !StringUtils.isNullOrEmpty(ricercaFormContainer.getFascicoliRed().getDescrizioneFascicolo())
				|| (!StringUtils.isNullOrEmpty(ricercaFormContainer.getFascicoliRed().getDescrizioneTipologiaDocumento())
						&& !"-".equals(ricercaFormContainer.getFascicoliRed().getDescrizioneTipologiaDocumento().trim()))
				|| ricercaFormContainer.getFascicoliRed().getTipoAssegnazione() != null || ricercaFormContainer.getFascicoliRed().getNumeroFascicolo() != null
				|| !StringUtils.isNullOrEmpty(ricercaFormContainer.getFascicoliRed().getTitolario()))) {
			out = "Restringere la ricerca valorizzando alcuni dei campi.";
		} else if (ricercaFormContainer.getFascicoliRed().getAnnoFascicolo() == null) {
			out = "Attenzione, l'anno fascicolo è obbligatorio.";
		} else if (!DateUtils.isRangeOk(ricercaFormContainer.getFascicoliRed().getDataChiusuraDa(), ricercaFormContainer.getFascicoliRed().getDataChiusuraA())) {
			out = "La data chiusura da deve essere antecedente la data chiusura a.";
		} else if (!DateUtils.isRangeOk(ricercaFormContainer.getFascicoliRed().getDataCreazioneDa(), ricercaFormContainer.getFascicoliRed().getDataCreazioneA())) {
			out = ORDINE_DATE_MSG;
		}
		return out;
	}

	private String isFormFaldoniOk() {
		String out = null;
		if (StringUtils.isNullOrEmpty(ricercaFormContainer.getFaldoniRed().getDescrizioneFaldone()) && ricercaFormContainer.getFaldoniRed().getDataCreazioneDa() == null
				&& ricercaFormContainer.getFaldoniRed().getDataCreazioneA() == null) {
			out = RESTRINGIMENTO_RICERCA_MSG;
		} else if (!DateUtils.isRangeOk(ricercaFormContainer.getFaldoniRed().getDataCreazioneDa(), ricercaFormContainer.getFaldoniRed().getDataCreazioneA())) {
			out = ORDINE_DATE_MSG;
		}
		return out;
	}

	private String isFormFepaOk() {
		String out = null;
		if (ricercaFormContainer.getFepa().getAnnoDocumento() == null) {
			out = "Attenzione, l'anno creazione è obbligatorio.";
		} else if (ricercaFormContainer.getFepa().getNumeroDocumento() == null && ricercaFormContainer.getFepa().getDataCreazioneDa() == null
				&& ricercaFormContainer.getFepa().getDataCreazioneA() == null) {
			out = RESTRINGIMENTO_RICERCA_MSG;
		} else if (!DateUtils.isRangeOk(ricercaFormContainer.getFepa().getDataCreazioneDa(), ricercaFormContainer.getFepa().getDataCreazioneA())) {
			out = ORDINE_DATE_MSG;
		}
		return out;
	}

	private String isFormSigiOk() {
		String out = null;
		if (!(ricercaFormContainer.getSigi().getNumeroDocumento() != null || !StringUtils.isNullOrEmpty(ricercaFormContainer.getSigi().getOggetto())
				|| ricercaFormContainer.getSigi().getNumeroProtocolloDa() != null || ricercaFormContainer.getSigi().getNumeroProtocolloA() != null
				|| ricercaFormContainer.getSigi().getAnnoProtocolloDa() != null || ricercaFormContainer.getSigi().getAnnoProtocolloA() != null
				|| ricercaFormContainer.getSigi().getDataCreazioneDa() != null || ricercaFormContainer.getSigi().getDataCreazioneA() != null)) {
			out = RESTRINGIMENTO_RICERCA_MSG;
		} else if (!DateUtils.isRangeOk(ricercaFormContainer.getSigi().getDataCreazioneDa(), ricercaFormContainer.getSigi().getDataCreazioneA())) {
			out = ORDINE_DATE_MSG;
		}
		return out;
	}

	private String isFormStampaRegistroOk() {
		String out = null;
		if (ricercaFormContainer.getRegistroProtocollo().getDataCreazioneDa() == null && ricercaFormContainer.getRegistroProtocollo().getDataCreazioneA() == null) {
			out = "Inserire almeno uno dei due campi.";
		} else if (!DateUtils.isRangeOk(ricercaFormContainer.getRegistroProtocollo().getDataCreazioneDa(), ricercaFormContainer.getRegistroProtocollo().getDataCreazioneA())) {
			out = "Inserire un range di date valido.";
		}

		return out;
	}

	private String isFormProtocolliPregressiDfOk() {
		String out = null;

		if (ricercaFormContainer.getProtocolliDF().getAnnoProtocollo() == null
				&& (ricercaFormContainer.getProtocolliDF().getDataProtocollazioneDa() != null && ricercaFormContainer.getProtocolliDF().getDataProtocollazioneA() == null)) {
			out = "Valorizzare anche la data protocollazione a";
		} else if (ricercaFormContainer.getProtocolliDF().getAnnoProtocollo() == null
				&& (ricercaFormContainer.getProtocolliDF().getDataProtocollazioneA() != null && ricercaFormContainer.getProtocolliDF().getDataProtocollazioneDa() == null)) {
			out = "Valorizzare anche la data protocollazione da";
		} else if (!DateUtils.isRangeOk(ricercaFormContainer.getProtocolliDF().getDataProtocollazioneDa(), ricercaFormContainer.getProtocolliDF().getDataProtocollazioneA())) {
			out = "La data protocollazione da deve essere antecedente la data protocollazione a";
		} else if ((ricercaFormContainer.getProtocolliDF().getDataProtocollazioneDa() != null && ricercaFormContainer.getProtocolliDF().getDataProtocollazioneA() != null)
				&& DateUtils.maxDeltaGiorni(ricercaFormContainer.getProtocolliDF().getDataProtocollazioneDa(),
						ricercaFormContainer.getProtocolliDF().getDataProtocollazioneA(), MAX_INTERV_IN_GIORNI_NSD)) {
			out = LIMITE_INTERVALLO_RICERCA_MSG;
		} else if (ricercaFormContainer.getProtocolliDF().getAnnoProtocollo() != null
				&& (ricercaFormContainer.getProtocolliDF().getNumProtocolloDa() == null && ricercaFormContainer.getProtocolliDF().getNumProtocolloA() == null)) {
			out = "Valorizzare i campi num protocollo da e num protocollo a";
		} else if (ricercaFormContainer.getProtocolliDF().getAnnoProtocollo() != null
				&& (ricercaFormContainer.getProtocolliDF().getNumProtocolloDa() != null && ricercaFormContainer.getProtocolliDF().getNumProtocolloA() == null)) {
			out = "Valorizzare num protocollo a";
		} else if (ricercaFormContainer.getProtocolliDF().getAnnoProtocollo() != null
				&& (ricercaFormContainer.getProtocolliDF().getNumProtocolloDa() == null && ricercaFormContainer.getProtocolliDF().getNumProtocolloA() != null)) {
			out = "Valorizzare num protocollo da ";
		} else if ((ricercaFormContainer.getProtocolliDF().getAnnoProtocollo() == null
				&& (ricercaFormContainer.getProtocolliDF().getDataProtocollazioneDa() == null || ricercaFormContainer.getProtocolliDF().getDataProtocollazioneA() == null))
				&& (ricercaFormContainer.getProtocolliDF().getNumProtocolloDa() != null || ricercaFormContainer.getProtocolliDF().getNumProtocolloA() != null)) {
			out = "Valorizzare l'anno protocollo o il range di date";
		} else if (ricercaFormContainer.getProtocolliDF().getAnnoProtocollo() == null && ricercaFormContainer.getProtocolliDF().getNumProtocolloDa() == null
				&& ricercaFormContainer.getProtocolliDF().getNumProtocolloA() == null && ricercaFormContainer.getProtocolliDF().getDataProtocollazioneDa() == null
				&& ricercaFormContainer.getProtocolliDF().getDataProtocollazioneA() == null) {
			out = "Valorizzare o anno protocollo e num protocollo da/a oppure data protocollo da/a";
		} else if (ricercaFormContainer.getProtocolliDF().getDataProtocollazioneA() != null && ricercaFormContainer.getProtocolliDF().getDataProtocollazioneDa() != null
				&& (ricercaFormContainer.getProtocolliDF().getDataProtocollazioneDa().equals(ricercaFormContainer.getProtocolliDF().getDataProtocollazioneA()))) {
			out = "Il range di date non può avere l'inizio e la fine uguali";
		}
		return out;
	}

	private String isFormFascicoliPregressiDfOk() {
		String out = null;
		// CAMPI PROT
		final Integer annoProtocollo = ricercaFormContainer.getFascicoliDF().getAnnoProtocolloFascicolo() == null ? null
				: ricercaFormContainer.getFascicoliDF().getAnnoProtocolloFascicolo();
		final Integer numProtocollo = ricercaFormContainer.getFascicoliDF().getNumeroProtocolloFascicolo() == null ? null
				: ricercaFormContainer.getFascicoliDF().getNumeroProtocolloFascicolo();
		final String idRegistro = ricercaFormContainer.getFascicoliDF().getIdRegistroFascicolo() == null ? null
				: ricercaFormContainer.getFascicoliDF().getIdRegistroFascicolo();

		// CAMPI FASC
		final String nome = ricercaFormContainer.getFascicoliDF().getNomeFascicolo() == null ? null : ricercaFormContainer.getFascicoliDF().getNomeFascicolo();
		final String descrizione = ricercaFormContainer.getFascicoliDF().getDescrizioneFascicolo() == null ? null
				: ricercaFormContainer.getFascicoliDF().getDescrizioneFascicolo();
		final String responsabile = ricercaFormContainer.getFascicoliDF().getResponsabileFascicolo() == null ? null
				: ricercaFormContainer.getFascicoliDF().getResponsabileFascicolo();
		final String stato = ricercaFormContainer.getFascicoliDF().getStatoFascicolo() == null ? null : ricercaFormContainer.getFascicoliDF().getStatoFascicolo();
		final String codTitolario = ricercaFormContainer.getFascicoliDF().getCodiceTitolarioFascicolo() == null ? null
				: ricercaFormContainer.getFascicoliDF().getCodiceTitolarioFascicolo();
		final Integer nFasciolo = ricercaFormContainer.getFascicoliDF().getNumeroFascicolo() == null ? null : ricercaFormContainer.getFascicoliDF().getNumeroFascicolo();
		final String uffProprietario = ricercaFormContainer.getFascicoliDF().getUfficioProprietarioFascicolo() == null ? null
				: ricercaFormContainer.getFascicoliDF().getUfficioProprietarioFascicolo();

		if (isNullRicercaFascicolo(nome, descrizione, responsabile, stato, codTitolario, nFasciolo, uffProprietario)
				&& isNullRicercaProtocollo(annoProtocollo, numProtocollo, idRegistro) && ricercaFormContainer.getFascicoliDF().getDataAperturaDa() == null
				&& ricercaFormContainer.getFascicoliDF().getDataAperturaA() == null && ricercaFormContainer.getFascicoliDF().getDataChiusuraDa() == null
				&& ricercaFormContainer.getFascicoliDF().getDataChiusuraA() == null && ricercaFormContainer.getFascicoliDF().getDataScadenzaDa() == null
				&& ricercaFormContainer.getFascicoliDF().getDataScadenzaA() == null) {
			out = RESTINGIMENTO_RICERCA_SHORT_MSG;
		} else if (!isNullRicercaFascicolo(nome, descrizione, responsabile, stato, codTitolario, nFasciolo, uffProprietario)
				&& !isNullRicercaProtocollo(annoProtocollo, numProtocollo, idRegistro)) {
			out = "Valorizzare solo i campi di ricerca relativi al fascicolo o al protocollo";
		} else if (!isNullRicercaFascicolo(nome, descrizione, responsabile, stato, codTitolario, nFasciolo, uffProprietario)
				&& ((ricercaFormContainer.getFascicoliDF().getDataAperturaDa() == null && ricercaFormContainer.getFascicoliDF().getDataAperturaA() == null)
						&& (ricercaFormContainer.getFascicoliDF().getDataChiusuraDa() == null && ricercaFormContainer.getFascicoliDF().getDataChiusuraA() == null)
						&& (ricercaFormContainer.getFascicoliDF().getDataScadenzaDa() == null && ricercaFormContainer.getFascicoliDF().getDataScadenzaA() == null))) {
			out = "Valorizzare almeno un gruppo di date";
		} else if (!isNullRicercaProtocollo(annoProtocollo, numProtocollo, idRegistro)
				&& (annoProtocollo == null || numProtocollo == null || StringUtils.isNullOrEmpty(idRegistro))) {
			out = "Valorizzare tutti i campi del protocollo";
		} else if (annoProtocollo == null
				&& (ricercaFormContainer.getFascicoliDF().getDataAperturaDa() != null && ricercaFormContainer.getFascicoliDF().getDataAperturaA() == null)) {
			out = "Valorizzare anche la data apertura a";
		} else if (annoProtocollo == null
				&& (ricercaFormContainer.getFascicoliDF().getDataAperturaA() != null && ricercaFormContainer.getFascicoliDF().getDataAperturaDa() == null)) {
			out = "Valorizzare anche la data apertura da";
		} else if ((ricercaFormContainer.getFascicoliDF().getDataAperturaDa() != null && ricercaFormContainer.getFascicoliDF().getDataAperturaA() != null)
				&& DateUtils.maxDeltaGiorni(ricercaFormContainer.getFascicoliDF().getDataAperturaDa(), ricercaFormContainer.getFascicoliDF().getDataAperturaA(),
						MAX_INTERV_IN_GIORNI_NSD)) {
			out = LIMITE_INTERVALLO_RICERCA_MSG;
		} else if ((ricercaFormContainer.getFascicoliDF().getDataChiusuraDa() != null && ricercaFormContainer.getFascicoliDF().getDataChiusuraA() != null)
				&& DateUtils.maxDeltaGiorni(ricercaFormContainer.getFascicoliDF().getDataChiusuraDa(), ricercaFormContainer.getFascicoliDF().getDataChiusuraA(),
						MAX_INTERV_IN_GIORNI_NSD)) {
			out = LIMITE_INTERVALLO_RICERCA_MSG;
		} else if ((ricercaFormContainer.getFascicoliDF().getDataScadenzaDa() != null && ricercaFormContainer.getFascicoliDF().getDataScadenzaA() != null)
				&& DateUtils.maxDeltaGiorni(ricercaFormContainer.getFascicoliDF().getDataScadenzaDa(), ricercaFormContainer.getFascicoliDF().getDataScadenzaA(),
						MAX_INTERV_IN_GIORNI_NSD)) {
			out = LIMITE_INTERVALLO_RICERCA_MSG;
		}
		return out;
	}

	private static boolean isNullRicercaFascicolo(final String nome, final String descrizione, final String responsabile, final String stato, final String codTitolario,
			final Integer nFasciolo, final String uffProprietario) {
		boolean isValid = false;
		if (StringUtils.isNullOrEmpty(nome) && StringUtils.isNullOrEmpty(descrizione) && StringUtils.isNullOrEmpty(responsabile) && StringUtils.isNullOrEmpty(stato)
				&& StringUtils.isNullOrEmpty(codTitolario) && nFasciolo == null && StringUtils.isNullOrEmpty(uffProprietario)) {
			isValid = true;
		}
		return isValid;
	}

	private static boolean isNullRicercaProtocollo(final Integer annoProtocollo, final Integer nProtocollo, final String tipoRegistro) {
		boolean isValid = false;
		if (annoProtocollo == null && nProtocollo == null && StringUtils.isNullOrEmpty(tipoRegistro)) {
			isValid = true;
		}

		return isValid;
	}
	
	// <-- FINE RICERCA RAPIDA E AVANZATA -->

	/**
	 * Metodo per rinnovare la sessione.
	 */
	public static final void rinnovaSessione() {
		LOGGER.info("Sessione rinnovata");
	}

	/**
	 * Consente il raggiungimento della pagina la cui resource location è definita
	 * dal token di navigation.
	 * 
	 * @see NavigationTokenEnum
	 * @param nte
	 * @return url della pagina da raggiungere
	 */
	public String gotoPage(final NavigationTokenEnum nte) {
		activeIndexTabView = PRIMO_ATTIVO;
		dialogRenderer = DialogRendererComponent.newInstance();

		if (nte != null && TileEnum.RICERCA.equals(nte.getTile()) && getActivePageInfo() != null && !getActivePageInfo().toString().toUpperCase().contains("RICERCA")) {

			handleObjectInSession();
			setLastPageInfo(getActivePageInfo());
		}
		activePageInfo = nte;

		showRubrica = false;
		showHelpPage = false;

		if (NavigationTokenEnum.LIBROFIRMA.equals(activePageInfo)) {
			FacesHelper.putObjectInSession("LIBRO_FIRMA_OBJ", activePageInfo);
		}
		// Calcolo le notifiche non lette
		setNotificheNonLette(notificheUtenteSRV.contaNotificheNonLette(utente));

		if (activePageInfo != null) {
			if (!NavigationTokenEnum.ORG_SCRIVANIA.equals(activePageInfo)) {
				if (!TileEnum.RICERCA.equals(activePageInfo.getTile())) {
					FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.ORG_OBJ, true);
				}
				if (activePageInfo.getDocumentQueue() != null) {
					showBtnResp = activePageInfo.getDocumentQueue().isCodaOperativa();
				} else if (TileEnum.RICERCA.equals(activePageInfo.getTile())) {
					showBtnResp = Boolean.FALSE;
				}
			}
		} else {
			return null;
		}
		return activePageInfo.getToken();
	}

	/**
	 * Gestisce l'object in session in base alla variazione della pagina.
	 */
	private void handleObjectInSession() {
		if (NavigationTokenEnum.FALDONI.equals(activePageInfo)) {
			final FaldoniBean bean = FacesHelper.getManagedBean(MBean.FALDONI_BEAN);
			if (bean.getObjFaldo() != null) {
				FacesHelper.putObjectInSession(ConstantsWeb.SessionObject.FALDO_OBJ, bean.getObjFaldo());
			}
		}
		if (NavigationTokenEnum.FASCICOLI.equals(activePageInfo)) {
			final FascicoliListaBean bean = FacesHelper.getManagedBean(MBean.FASCICOLI_BEAN);
			if (bean.getObjTito() != null) {
				FacesHelper.putObjectInSession(ConstantsWeb.SessionObject.TITO_OBJ, bean.getObjTito());
			}
		}
		if (NavigationTokenEnum.ORG_SCRIVANIA.equals(activePageInfo)) {
			final ListaDocumentiBean bean = FacesHelper.getManagedBean(MBean.LISTA_DOCUMENTI_BEAN);
			if (bean.getOrgObj() != null) {
				FacesHelper.putObjectInSession(ConstantsWeb.SessionObject.ORG_OBJ, bean.getOrgObj());
			}
		}
	}

	/**
	 * Consente di raggiungere la pagina di creazione documenti in ingresso.
	 * 
	 * @param updateDialog se true si effettua un update della dialog, altrimenti il
	 *                     metodo si limita a reindirizzare {@link #utente} sulla
	 *                     dialog di creazione doc in ingresso
	 */
	public void gotoCreazioneDocIngresso(final boolean updateDialog) {
		try {
			// in questo modo gli dico che è un documento in entrata
			final DetailDocumentRedDTO detail = new DetailDocumentRedDTO();
			detail.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0]);

			final DocumentManagerBean bean = FacesHelper.getManagedBean(MBean.DOCUMENT_MANAGER_BEAN);
			// pulisco nel caso sia già stato aperto
			bean.unsetDetail();
			bean.setDetail(detail);

			if (updateDialog) {
				FacesHelper.update("idDettagliEstesiForm:idPanelDocumentManager");
			}
			FacesHelper.update("idDettagliEstesiForm:idPanelContainerDocumentManager_T");

			FacesHelper.executeJS("PF('dlgManageDocument_RM').show();");

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Reindirizza {@link #utente} sulla creazione del documento in ingresso
	 * aggiornandone la dialog.
	 */
	public void gotoCreazioneDocIngresso() {
		gotoCreazioneDocIngresso(true);

	}

	/**
	 * Consente di raggiungere la pagina di creazione documenti in uscita.
	 * 
	 * @param updateDialog se true si effettua un update della dialog, altrimenti il
	 *                     metodo si limita a reindirizzare {@link #utente} sulla
	 *                     dialog di creazione doc in uscita
	 */
	public void gotoCreazioneDocUscita(final boolean updateDialog) {

		try {
			// in questo modo gli dico che è un documento in uscita
			final DetailDocumentRedDTO detail = new DetailDocumentRedDTO();
			detail.setIdCategoriaDocumento(CategoriaDocumentoEnum.MOZIONE.getIds()[0]);

			final DocumentManagerBean bean = FacesHelper.getManagedBean(MBean.DOCUMENT_MANAGER_BEAN);
			// pulisco nel caso sia già stato aperto
			bean.unsetDetail();
			for (final Nodo nodo : listaUffici) {
				if ((nodo.getDescrizione().equals(utente.getNodoDesc())) && nodo.getIsEstendiVisibilita()) {
					bean.setIsEstendiVisibilitaChk(true);
				}
			}
			bean.setDetail(detail);

			if (updateDialog) {
				FacesHelper.update("idDettagliEstesiForm:idPanelDocumentManager");
			}
			FacesHelper.update("idDettagliEstesiForm:idPanelContainerDocumentManager_T");

			FacesHelper.executeJS("PF('dlgManageDocument_RM').show();");

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Reindirizza {@link #utente} sulla creazione del documento in uscita
	 * aggiornandone la dialog.
	 */
	public void gotoCreazioneDocUscita() {
		gotoCreazioneDocUscita(true);
	}

	/**
	 * Gestisce l'apertura delle attivita restituendo l'url della coda: DA_LAVORARE.
	 * 
	 * @return url per raggiungimento coda DA_LAVORARE
	 */
	public final String gotoDaLavorare() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.DA_LAVORARE);
	}

	/**
	 * Gestisce l'apertura dei faldoni restituendo l'url della pagina dei faldoni.
	 * 
	 * @return url per raggiungimento view dei FALDONI
	 */
	public final String gotoFaldoni() {
		openFaldoni();
		return gotoPage(NavigationTokenEnum.FALDONI);
	}

	/**
	 * Gestisce l'apertura delle attivita restituendo l'url della view dei
	 * GIRO_VISTI.
	 * 
	 * @see NavigationTokenEnum.
	 * @return url per raggiungimento della pagina GIRO_VISTI
	 */
	public final String gotoGiroVisti() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.GIRO_VISTI);
	}

	/**
	 * Gestisce l'apertura delle attivita restituendo l'url dell'homepage.
	 * 
	 * @return url per raggiungimento dell'homepage
	 */
	public final String gotoHomepage() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.HOMEPAGE);
	}

	/**
	 * Gestisce l'apertura delle attivita restituendo l'url della coda: IN_SOSPESO.
	 * 
	 * @return url per raggiungimento della coda IN_SOSPESO
	 */
	public final String gotoInSospeso() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.IN_SOSPESO);
	}

	/**
	 * Gestisce l'apertura delle attivita restituendo l'url della coda:
	 * IN_SPEDIZIONE.
	 * 
	 * @return url per raggiungimento della coda IN_SPEDIZIONE
	 */
	public final String gotoInSpedizione() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.IN_SPEDIZIONE);
	}

	/**
	 * Reindirizza l'utente sulla pagina di inserimento di un contatto.
	 * 
	 * @return url pagina INSERISCI_CONTATTO
	 */
	public final String gotoInserisciContatto() {
		return gotoPage(NavigationTokenEnum.INSERISCI_CONTATTO);
	}

	/**
	 * Redireziona l'utente sulla pagina della coda PREPARAZIONE ALLA SPEDIZIONE.
	 * @return url pagina
	 */
	public final String gotoPreparazioneSpedizione() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.PREPARAZIONE_ALLA_SPEDIZIONE);
	}

	/**
	 * Redireziona l'utente sulla pagina della coda PREPARAZIONE ALLA SPEDIZIONE FEPA.
	 * @return url pagina
	 */
	public final String gotoPreparazioneSpedizioneFepa() {
		return gotoPage(NavigationTokenEnum.PREPARAZIONE_ALLA_SPEDIZIONE_FEPA);
	}

	/**
	 * Gestisce l'apertura delle attivita restituendo l'url del LIBRO_FIRMA.
	 * 
	 * @return url per raggiungimento della coda LIBRO_FIRMA
	 */
	public final String gotoLibroFirma() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.LIBROFIRMA);
	}

	/**
	 * Gestisce l'apertura delle attivita restituendo l'url del
	 * LIBRO_FIRMA_DELEGATO.
	 * 
	 * @return url per raggiungimento del LIBRO_FIRMA_DELEGATO
	 */
	public final String gotoLibroFirmaDelegato() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.LIBRO_FIRMA_DELEGATO);
	}

	/**
	 * Reindirizza l'utente sulla pagina di inserimento di login.
	 * 
	 * @return url pagina LOGINPAGE
	 */
	public final String gotoLogin() {
		return gotoPage(NavigationTokenEnum.LOGINPAGE);
	}

	/**
	 * Gestisce l'apertura delle attivita restituendo l'url della coda ASSEGNATE.
	 * 
	 * @return url per raggiungimento della coda ASSEGNATE
	 */
	public final String gotoAssegnate() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.ASSEGNATE);
	}

	/**
	 * Gestisce l'apertura delle attivita restituendo l'url della coda ATTI.
	 * 
	 * @return url per raggiungimento della coda ATTI
	 */
	public final String gotoAtti() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.ATTI);
	}

	/**
	 * Gestisce l'apertura delle attivita restituendo l'url del Wizard dei tipi
	 * documento.
	 * 
	 * @return url per raggiungimento del Wizard dei tipi documento
	 */
	public final String gotoWizardTipoDoc() {
		openAttivita();
		countMenuAttivita(false);
		return gotoPage(NavigationTokenEnum.WIZARD_TIPO_DOC);
	}

	/**
	 * Gestisce l'apertura delle attivita restituendo l'url per il raggiungimento
	 * della Scrivania.
	 * 
	 * @return url per raggiungimento della Scrivania
	 */
	public final String gotoScrivania() {
		openAttivita();
		countMenuAttivita(false);
		return gotoPage(NavigationTokenEnum.SCRIVANIA);
	}

	/**
	 * Gestisce l'apertura delle attivita restituendo l'url per il raggiungimento
	 * della coda CHIUSE.
	 * 
	 * @return url per raggiungimento della coda CHIUSE
	 */
	public final String gotoChiuse() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.CHIUSE);
	}

	/**
	 * Reindirizza l'utente sulla pagina di CLASSIFICAZIONE.
	 * 
	 * @return url pagina CLASSIFICAZIONE
	 */
	public final String gotoClassificazione() {
		return gotoPage(NavigationTokenEnum.CLASSIFICAZIONE);
	}

	/**
	 * Gestisce l'apertura delle attivita restituendo l'url per il raggiungimento
	 * del CORRIERE.
	 * 
	 * @return url per raggiungimento del CORRIERE
	 */
	public final String gotoCorriere() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.CORRIERE);
	}

	/**
	 * Gestisce l'apertura delle attivita restituendo l'url per il raggiungimento
	 * del CORRIERE DIRETTO.
	 * 
	 * @return url per raggiungimento del CORRIERE_DIRETTO
	 */
	public final String gotoCorriereDiretto() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.CORRIERE_DIRETTO);
	}

	/**
	 * Gestisce l'apertura delle attivita restituendo l'url per il raggiungimento
	 * del CORRIERE INDIRETTO.
	 * 
	 * @return url per raggiungimento del CORRIERE_INDIRETTO
	 */
	public final String gotoCorriereIndiretto() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.CORRIERE_INDIRETTO);
	}

	/**
	 * Reindirizza l'utente sulla coda: IN ACQUISIZIONE
	 * 
	 * @return url per raggiungimento della coda IN_ACQUISIZIONE
	 */
	public final String gotoInAcquisizione() {
		return gotoPage(NavigationTokenEnum.IN_ACQUISIZIONE);
	}

	/**
	 * Reindirizza l'utente sulla coda: ACQUISITI
	 * 
	 * @return url per raggiungimento della coda ACQUISITI
	 */
	public final String gotoAcquisiti() {
		return gotoPage(NavigationTokenEnum.ACQUISITI);
	}

	/**
	 * Reindirizza l'utente sulla coda: ELIMINATI
	 * 
	 * @return url per raggiungimento della coda ELIMINATI
	 */
	public final String gotoEliminati() {
		return gotoPage(NavigationTokenEnum.ELIMINATI);
	}

	/**
	 * Reindirizza l'utente sulla DASHBOARD.
	 * 
	 * @return url per raggiungimento della DASHBOARD
	 */
	public final String gotoDashboard() {
		return gotoPage(NavigationTokenEnum.DASHBOARD);
	}

	/**
	 * Gestisce l'apertura delle attivita restituendo l'url per il raggiungimento
	 * della coda: RICHIAMA DOCUMENTI.
	 * 
	 * @return url per raggiungimento della coda RICHIAMA_DOCUMENTI
	 */
	public final String gotoInRichiamaDocumenti() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.RICHIAMA_DOCUMENTI);
	}

	/**
	 * Gestisce l'apertura della posta elettronica.
	 * 
	 * @return url raggiungimento SCRIVANIA
	 */
	public final String gotoPostaElettronica() {
		openPostaElettronica();
		return gotoPage(NavigationTokenEnum.SCRIVANIA);
	}

	/**
	 * Reindirizza l'utente sulla pagina di ricerca dei fascicoli NSD.
	 * 
	 * @return url pagina RICERCA_FASCICOLI_NSD
	 */
	public final String gotoRicercaFascicoliNsd() {
		return gotoPage(NavigationTokenEnum.RICERCA_FASCICOLI_NSD);
	}

	/**
	 * Reindirizza l'utente sulla pagina di ricerca dei protocolli NSD.
	 * 
	 * @return url pagina RICERCA_PROTOCOLLI_NSD
	 */
	public final String gotoRicercaProtocolliNsd() {
		return gotoPage(NavigationTokenEnum.RICERCA_PROTOCOLLI_NSD);
	}

	/**
	 * Reindirizza l'utente sulla pagina che gestisce le assegnazioni automatiche
	 * per i capitoli di spesa.
	 * 
	 * @return url pagina WIZARD_ASSEGNAZIONI_AUTO
	 */
	public final String gotoWizardAssegnazioniAuto() {
		openAttivita();
		countMenuAttivita(false);
		return gotoPage(NavigationTokenEnum.WIZARD_ASSEGNAZIONI_AUTO);
	}

	/**
	 * Reindirizza l'utente sulla pagina che gestisce le assegnazioni automatiche
	 * per tipologia documento - procedimento - metadato.
	 * 
	 * @return url pagina ASSEGNAZIONE_METADATI
	 */
	public final String gotoAssegnazioneMetadati() {
		return gotoPage(NavigationTokenEnum.ASSEGNAZIONE_METADATI);
	}

	/**
	 * Reindirizza l'utente sulla pagina di ricerca avanzata UCB.
	 * 
	 * @return url pagina RICERCA_DOCUMENTI_UCB
	 */
	public final String gotoRicercaDocumentiUCB() {
		return gotoPage(NavigationTokenEnum.RICERCA_DOCUMENTI_UCB);
	}

	/**
	 * Reindirizza l'utente sulla pagina di ricerca flussi UCB.
	 * 
	 * @return url pagina RICERCA_FLUSSI_UCB
	 */
	public final String gotoRicercaFlussiUCB() {
		return gotoPage(NavigationTokenEnum.RICERCA_FLUSSI_UCB);
	}

	/**
	 * Reindirizza l'utente sulla pagina di ricerca registrazioni ausiliarie UCB.
	 * 
	 * @return url pagina RICERCA_REGISTRAZIONI_AUSILIARIE
	 */
	public final String gotoRicercaRegistrazioniAusiliarie() {
		return gotoPage(NavigationTokenEnum.RICERCA_REGISTRAZIONI_AUSILIARIE);
	}

	/**
	 * Gestisce l'apertura delle attivita reindirizzando l'utente sulla coda
	 * DA_LAVORARE di UCB.
	 * 
	 * @return url pagina DA_LAVORARE_UCB
	 */
	public final String gotoDaLavorareUCB() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.DA_LAVORARE_UCB);
	}

	/**
	 * Gestisce l'apertura delle attivita reindirizzando l'utente sulla coda
	 * IN_LAVORAZIONE di UCB.
	 * 
	 * @return url pagina IN_LAVORAZIONE_UCB
	 */
	public final String gotoInLavorazioneUCB() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.IN_LAVORAZIONE_UCB);
	}

	/**
	 * Gestisce la visualizzazione della pagina delle mail.
	 * 
	 * @param e
	 * @return url pagina MAIL
	 */
	public final String gotoMail(final MenuActionEvent e) {
		if (accountSelected != null) {
			((DefaultMenuItem) accountSelected).setContainerStyleClass("");
		}
		accountSelected = e.getMenuItem();
		((DefaultMenuItem) accountSelected).setContainerStyleClass(SELECTED_MENU_STYLECLASS);
		paramsAccountSelected = e.getMenuItem().getParams();
		List<String> cartellaMail = null;
		for (final Entry<String, List<String>> keySet : paramsAccountSelected.entrySet()) {
			if (!keySet.getKey().contains("westSectionForm")) {
				cartellaMail = keySet.getValue();
				break;
			}
		}
		cartellaMailSelected = new CasellaMailStatoDTO(cartellaMail);
		FacesHelper.putObjectInSession(Constants.SessionObject.MAILS, loadEmails());
		return gotoPage(NavigationTokenEnum.MAIL);
	}

	/**
	 * Reindirizza l'utente sulla pagina degli ACCORDI di SERVIZIO.
	 * 
	 * @return url pagina ACCORDO_SERVIZIO
	 */
	public final String gotoAccordoServizio() {
		return gotoPage(NavigationTokenEnum.ACCORDO_SERVIZIO);
	}

	/**
	 * Reinidirizza l'utente sulla pagina che gestisce il caricamento massivo delle
	 * liste utenti.
	 * 
	 * @return url pagina CARICAMENTO_MASSIVO
	 */
	public final String gotoCaricamentoMassivo() {
		return gotoPage(NavigationTokenEnum.CARICAMENTO_MASSIVO);
	}

	/**
	 * Gestisce il caricamento delle mail e la creazione del paginator per la
	 * gestione delle stesse.
	 * 
	 * @return Paginator creato per la paginazione delle mail
	 */
	public final PageIterator loadEmails() {
		final Integer pageSize = MAX_ROW_MAIL + 2;
		LOGGER.info("####INFO#### Inizio loadEmails");
		final List<StatoMailGUIEnum> selectedStatList = new ArrayList<>();
		selectedStatList.add(StatoMailGUIEnum.get(cartellaMailSelected.getStatoMails()));
		if (StatoMailGUIEnum.INOLTRATA.equals(StatoMailGUIEnum.get(cartellaMailSelected.getStatoMails()))) {
			selectedStatList.add(StatoMailGUIEnum.RIFIUTATA);
		}
		return mailSRV.createPaginator(utente.getFcDTO(), cartellaMailSelected.getCasellaMail(), selectedStatList, utente.getId().intValue(), pageSize,
				orderByClauseDataRicezione(utente.isOrdinamentoMailAOOAsc()), utente.getIdAoo());

	}

	/**
	 * Gestisce il caricamento delle mail.
	 * 
	 * @return lista delle mail
	 */
	public final Collection<EmailDTO> loadEmailsFull() {
		LOGGER.info("####INFO#### Inizio loadEmails");
		final Collection<EmailDTO> emails = mailSRV.getMails(utente.getFcDTO(), cartellaMailSelected.getCasellaMail(),
				StatoMailGUIEnum.get(cartellaMailSelected.getStatoMails()), utente.getId().intValue(), orderByClauseDataRicezione(utente.isOrdinamentoMailAOOAsc()),
				utente.getIdAoo());
		if (StatoMailGUIEnum.INOLTRATA.equals(StatoMailGUIEnum.get(cartellaMailSelected.getStatoMails()))) {
			emails.addAll(mailSRV.getMails(utente.getFcDTO(), cartellaMailSelected.getCasellaMail(), StatoMailGUIEnum.RIFIUTATA, utente.getId().intValue(),
					orderByClauseDataRicezione(utente.isOrdinamentoMailAOOAsc()), utente.getIdAoo()));
		}

		return emails;
	}

	private String orderByClauseDataRicezione(final Boolean asc) {
		String order = null;
		if (asc != null && asc) {
			order = mailSRV.orderByDataRicezioneClause() + " ASC";
		} else if (asc != null) {
			order = mailSRV.orderByDataRicezioneClause() + " DESC";
		}
		return order;
	}

	/**
	 * Effettua l'ordinamento delle mail sulla base della data.
	 * 
	 * @param emails
	 * @return lista mail ordinate
	 */
	public List<EmailDTO> ordinaElementiInBaseAllaData(final Collection<EmailDTO> emails) {
		final List<EmailDTO> sortedEmails = (List<EmailDTO>) emails;
		final ComparatorDateMail cd = new ComparatorDateMail();
		sortedEmails.sort(cd);
		return sortedEmails;
	}

	/**
	 * Gestisce l'apertura delle attivita reindirizzando l'utente sulla coda
	 * MOZIONE.
	 * 
	 * @return url pagina MOZIONE
	 */
	public final String gotoMozione() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.MOZIONE);
	}

	/**
	 * Reindirizza l'utente sull'organigramma. (NON IMPLEMENTATO)
	 * 
	 * @return null
	 */
	public static final String gotoOrganigramma() {
		return null;
	}

	/**
	 * Reindirizza l'utente sulla pagina di ricerca rapida.
	 * 
	 * @return url pagina RICERCA_RAPIDA
	 */
	public final String gotoRicercaRapida() {
		return gotoPage(NavigationTokenEnum.RICERCA_RAPIDA);
	}

	/**
	 * Reindirizza l'utente sulla pagina di PERSONALIZZAZIONE dell' {@link #utente}.
	 * 
	 * @return url pagina PERSONALIZZAZIONE
	 */
	public final String gotoPersonalizza() {
		return gotoPage(NavigationTokenEnum.PERSONALIZZAZIONE);
	}

	/**
	 * Gestisce l'apertura delle Attivita reindirizzando l'utente sulla pagina dei
	 * PROCEDIMENTI ATTIVI.
	 * 
	 * @return url pagina PROCEDIMENTI_ATTIVI
	 */
	public final String gotoProcedimentiAttivi() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.PROCEDIMENTI_ATTIVI);
	}

	/**
	 * Reindirizza l'utente sulla pagina di RICERCA CONTATTO.
	 * 
	 * @return url pagina RICERCA_CONTATTO
	 */
	public final String gotoRicercaContatto() {
		return gotoPage(NavigationTokenEnum.RICERCA_CONTATTO);
	}

	/**
	 * Reindirizza l'utente sulla pagina di RICERCA DOCUMENTI.
	 * 
	 * @return url pagina RICERCA_DOCUMENTI
	 */
	public final String gotoRicercaDocumenti() {
		return gotoPage(NavigationTokenEnum.RICERCA_DOCUMENTI);
	}

	/**
	 * Reindirizza l'utente sulla pagina di RICERCA FALDONI.
	 * 
	 * @return url pagina RICERCA_FALDONI
	 */
	public final String gotoRicercaFaldoni() {
		return gotoPage(NavigationTokenEnum.RICERCA_FALDONI);
	}

	/**
	 * Reindirizza l'utente sulla pagina di RICERCA FASCICOLI.
	 * 
	 * @return url pagina RICERCA_FASCICOLI
	 */
	public final String gotoRicercaFascicoli() {
		return gotoPage(NavigationTokenEnum.RICERCA_FASCICOLI);
	}

	/**
	 * Reindirizza l'utente sulla pagina di RICERCA SIGI.
	 * 
	 * @return url pagina RICERCA_SIGI
	 */
	public final String gotoRicercaSIGI() {
		return gotoPage(NavigationTokenEnum.RICERCA_SIGI);
	}

	/**
	 * Reindirizza l'utente sulla pagina di RICERCA FEPA.
	 * 
	 * @return url pagina RICERCA_FEPA
	 */
	public final String gotoRicercaFEPA() {
		return gotoPage(NavigationTokenEnum.RICERCA_FEPA);
	}

	/**
	 * Reindirizza l'utente sulla pagina di RICERCA DATI PROTOCOLLO.
	 * 
	 * @return url pagina RICERCA_DATI_PROTOCOLLO
	 */
	public final String gotoRicercaDatiProtocollo() {
		return gotoPage(NavigationTokenEnum.RICERCA_DATI_PROTOCOLLO);
	}

	/**
	 * Reindirizza l'utente sulla pagina di RICERCA STAMPA REGISTRO.
	 * 
	 * @return url pagina RICERCA_STAMPA_REGISTRO
	 */
	public final String gotoRicercaStampaRegistro() {
		return gotoPage(NavigationTokenEnum.RICERCA_STAMPA_REGISTRO);
	}

	/**
	 * Reindirizza l'utente sulla coda di RISPOSTA.
	 * 
	 * @return url coda RISPOSTA
	 */
	public final String gotoRisposta() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.RISPOSTA);
	}

	/**
	 * Reindirizza l'utente sulla RUBRICA.
	 * 
	 * @return url pagina RUBRICA
	 */
	public final String gotoRubrica() {
		return gotoPage(NavigationTokenEnum.RUBRICA);
	}

	/**
	 * Reindirizza l'utente sulla RUBRICA MAIL.
	 * 
	 * @return url pagina RUBRICAMAIL
	 */
	public final String gotoRubricaMail() {
		return gotoPage(NavigationTokenEnum.RUBRICAMAIL);
	}

	/**
	 * Reindirizza l'utente sulla pagina delle SOTTOSCRIZIONI.
	 * 
	 * @return url pagina SOTTOSCRIZIONI
	 */
	public final String gotoSottoscrizioni() {
		return gotoPage(NavigationTokenEnum.SOTTOSCRIZIONI);
	}

	/**
	 * Reindirizza l'utente sulla coda: STORNATI.
	 * 
	 * @return url pagina STORNATI
	 */
	public final String gotoStornati() {
		openAttivita();
		return gotoPage(NavigationTokenEnum.STORNATI);
	}

	/**
	 * Reindirizza l'utente sulla coda dei FASCICOLI.
	 * 
	 * @return url coda FASCICOLI
	 */
	public final String gotoFascicoli() {
		openFascicoli();
		return gotoPage(NavigationTokenEnum.FASCICOLI);
	}

	/**
	 * Gestisce l'apertura della fatturazione elettronica reindirizzando l'utente
	 * sulla coda DSR.
	 * 
	 * @return url coda DSR
	 */
	public final String gotoDSR() {
		openFatElettronica();
		return gotoPage(NavigationTokenEnum.DSR);
	}

	/**
	 * Gestisce l'apertura della fatturazione elettronica reindirizzando l'utente
	 * sulla coda FATTURE DA LAVORARE.
	 * 
	 * @return url coda FATTURA_DA_LAVORARE
	 */
	public final String gotoFattureDaLavorare() {
		openFatElettronica();
		return gotoPage(NavigationTokenEnum.FATTURE_DA_LAVORARE);
	}

	/**
	 * Gestisce l'apertura della fatturazione elettronica reindirizzando l'utente
	 * sulla coda FATTURE IN LAVORAZIONE.
	 * 
	 * @return url coda FATTURA_IN_LAVORAZIONE
	 */
	public final String gotoFattureInLavorazione() {
		openFatElettronica();
		return gotoPage(NavigationTokenEnum.FATTURE_IN_LAVORAZIONE);
	}

	/**
	 * Gestisce l'apertura della fatturazione elettronica reindirizzando l'utente
	 * sulla coda DECRETI DIRIGENZIALI DA LAVORARE.
	 * 
	 * @return url coda DD_DA_LAVORARE
	 */
	public final String gotoDecretiDaLavorare() {
		openFatElettronica();
		return gotoPage(NavigationTokenEnum.DD_DA_LAVORARE);
	}

	/**
	 * Gestisce l'apertura della fatturazione elettronica reindirizzando l'utente
	 * sulla coda DECRETI DIRIGENZIALI IN FIRMA.
	 * 
	 * @return url coda DD_IN_FIRMA
	 */
	public final String gotoDecretiInFirma() {
		openFatElettronica();
		return gotoPage(NavigationTokenEnum.DD_IN_FIRMA);
	}

	/**
	 * Gestisce l'apertura della fatturazione elettronica reindirizzando l'utente
	 * sulla coda DECRETI DIRIGENZIALI FIRMATI.
	 * 
	 * @return url coda DD_FIRMATI
	 */
	public final String gotoDecretiFirmati() {
		openFatElettronica();
		return gotoPage(NavigationTokenEnum.DD_FIRMATI);
	}

	/**
	 * Gestisce l'apertura della fatturazione elettronica reindirizzando l'utente
	 * sulla coda DECRETI DIRIGENZIALI DA FIRMARE.
	 * 
	 * @return url coda DD_DA_FIRMARE
	 */
	public final String gotoDecretiDaFirmare() {
		openFatElettronica();
		return gotoPage(NavigationTokenEnum.DD_DA_FIRMARE);
	}

	/**
	 * Reindirizza l'utente sulla pagina REPORT.
	 * 
	 * @return url pagina REPORT
	 */
	public final String gotoReport() {
		return gotoPage(NavigationTokenEnum.REPORT);
	}

	/**
	 * Reindirizza l'utente sulla pagina DELEGHE.
	 * 
	 * @return url pagina DELEGHE
	 */
	public final String gotoDeleghe() {
		return gotoPage(NavigationTokenEnum.DELEGHE);
	}

	/**
	 * Metodo utilizzato per la navigazione verso la homepage personalizzata
	 * dell'utente.
	 * 
	 */
	public void gotoHomepagePersonalizzata() {
		regolaDiNavigazionePreferenze(preferenze.getPaginaPreferita(), NavigationTokenEnum.HOMEPAGE);
	}

	/**
	 * Gestisce la logica di logout.
	 */
	public final void logout() {
		final FacesContext ctx = FacesContext.getCurrentInstance();
		if (ctx != null) {
			final String urlLogout = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.URL_LOGOUT);
			if (!StringUtils.isNullOrEmpty(urlLogout)) {
				try {
					final String contextPath = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
					FacesContext.getCurrentInstance().getExternalContext().redirect(contextPath + urlLogout);
				} catch (final IOException e) {
					LOGGER.error("Errore in fase di invocazione logout oam", e);
				}
			}
			final HttpSession session = (HttpSession) ctx.getExternalContext().getSession(true);
			session.invalidate();
		}
	}

	/**
	 * Gestisce la modifica del range di date della ricerca.
	 * 
	 * @param fieldName
	 */
	public void onChangeRicercaDocDataDaA(final String fieldName) {
		if (DATA_CREAZIONE_DA.equals(fieldName) && (ricercaFormContainer.getDocumentiRed().getDataCreazioneA() == null
				|| ricercaFormContainer.getDocumentiRed().getDataCreazioneA().before(ricercaFormContainer.getDocumentiRed().getDataCreazioneDa()))) {
		
			ricercaFormContainer.getDocumentiRed().setDataCreazioneA(ricercaFormContainer.getDocumentiRed().getDataCreazioneDa());
			ricercaFormContainer.getDocumentiRed().setAnnoDocumentoDisabled(true);

		} else if (DATA_CREAZIONE_A.equals(fieldName) && (ricercaFormContainer.getDocumentiRed().getDataCreazioneDa() == null
				|| ricercaFormContainer.getDocumentiRed().getDataCreazioneDa().after(ricercaFormContainer.getDocumentiRed().getDataCreazioneA()))) {
		
			ricercaFormContainer.getDocumentiRed().setDataCreazioneDa(ricercaFormContainer.getDocumentiRed().getDataCreazioneA());
			ricercaFormContainer.getDocumentiRed().setAnnoDocumentoDisabled(true);
			
		} else if ("dataProtocolloA".equals(fieldName) && (ricercaFormContainer.getDocumentiRed().getDataProtocolloDa() == null
				|| ricercaFormContainer.getDocumentiRed().getDataProtocolloDa().after(ricercaFormContainer.getDocumentiRed().getDataProtocolloA()))) {
		
			ricercaFormContainer.getDocumentiRed().setDataProtocolloDa(ricercaFormContainer.getDocumentiRed().getDataProtocolloA());
			ricercaFormContainer.getDocumentiRed().setAnnoDocumentoDisabled(true);
			
		} else if ("dataProtocolloDa".equals(fieldName) && (ricercaFormContainer.getDocumentiRed().getDataProtocolloA() == null
				|| ricercaFormContainer.getDocumentiRed().getDataProtocolloA().before(ricercaFormContainer.getDocumentiRed().getDataProtocolloDa()))) {
		
			ricercaFormContainer.getDocumentiRed().setDataProtocolloA(ricercaFormContainer.getDocumentiRed().getDataProtocolloDa());
			ricercaFormContainer.getDocumentiRed().setAnnoDocumentoDisabled(true);
		}
	}

	/**
	 * Gestisce la modifica del range di date della ricerca FEPA.
	 * 
	 * @param fieldName
	 */
	public void onChangeRicercaFepaDataDaA(final String fieldName) {
		if (DATA_CREAZIONE_DA.equals(fieldName) && (ricercaFormContainer.getFepa().getDataCreazioneA() == null
				|| ricercaFormContainer.getFepa().getDataCreazioneA().before(ricercaFormContainer.getFepa().getDataCreazioneDa()))) {
			
			ricercaFormContainer.getFepa().setDataCreazioneA(ricercaFormContainer.getFepa().getDataCreazioneDa());
			ricercaFormContainer.getFepa().setAnnoDocumentoDisabled(true);
			
		} else if (DATA_CREAZIONE_A.equals(fieldName) && (ricercaFormContainer.getFepa().getDataCreazioneDa() == null
				|| ricercaFormContainer.getFepa().getDataCreazioneDa().after(ricercaFormContainer.getFepa().getDataCreazioneA()))) {
			
			ricercaFormContainer.getFepa().setDataCreazioneDa(ricercaFormContainer.getFepa().getDataCreazioneA());
			ricercaFormContainer.getFepa().setAnnoDocumentoDisabled(true);
			
		}
	}

	/**
	 * Gestisce la modifica del range di date della ricerca dati protocollo.
	 * 
	 * @param fieldName
	 */
	public void onChangeRicercaDatiProtDataDaA(final String fieldName) {
		if (DATA_CREAZIONE_DA.equals(fieldName) && (ricercaFormContainer.getDatiProtocollo().getDataProtocolloA() == null
				|| ricercaFormContainer.getDatiProtocollo().getDataProtocolloA().before(ricercaFormContainer.getDatiProtocollo().getDataProtocolloDa()))) {
			
			ricercaFormContainer.getDatiProtocollo().setDataProtocolloA(ricercaFormContainer.getDatiProtocollo().getDataProtocolloDa());
			ricercaFormContainer.getDatiProtocollo().setAnnoProtocolloDisabled(true);

		} else if (DATA_CREAZIONE_A.equals(fieldName) && (ricercaFormContainer.getDatiProtocollo().getDataProtocolloDa() == null
				|| ricercaFormContainer.getDatiProtocollo().getDataProtocolloDa().after(ricercaFormContainer.getDatiProtocollo().getDataProtocolloA()))) {
			
			ricercaFormContainer.getDatiProtocollo().setDataProtocolloDa(ricercaFormContainer.getDatiProtocollo().getDataProtocolloA());
			ricercaFormContainer.getDatiProtocollo().setAnnoProtocolloDisabled(true);
		}
	}

	/**
	 * Gestisce la modifica del range di date della ricerca registri protocollo.
	 * 
	 * @param fieldName
	 */
	public void onChangeRicercaRegDataDaA(final String fieldName) {
		if (DATA_CREAZIONE_DA.equals(fieldName) && (ricercaFormContainer.getRegistroProtocollo().getDataCreazioneA() == null
				|| ricercaFormContainer.getRegistroProtocollo().getDataCreazioneA().before(ricercaFormContainer.getRegistroProtocollo().getDataCreazioneDa()))) {
			
			ricercaFormContainer.getRegistroProtocollo().setDataCreazioneA(ricercaFormContainer.getRegistroProtocollo().getDataCreazioneDa());
			
		} else if (DATA_CREAZIONE_A.equals(fieldName) && (ricercaFormContainer.getRegistroProtocollo().getDataCreazioneDa() == null 
				|| ricercaFormContainer.getRegistroProtocollo().getDataCreazioneDa().after(ricercaFormContainer.getRegistroProtocollo().getDataCreazioneA()))) {
			
			ricercaFormContainer.getRegistroProtocollo().setDataCreazioneDa(ricercaFormContainer.getRegistroProtocollo().getDataCreazioneA());
		}
	}

	/**
	 * Gestisce la modifica del range di date della ricerca SIGI.
	 * 
	 * @param fieldName
	 */
	public void onChangeRicercaSigiDataDaA(final String fieldName) {
		if (DATA_CREAZIONE_DA.equals(fieldName) && (ricercaFormContainer.getSigi().getDataCreazioneA() == null
				|| ricercaFormContainer.getSigi().getDataCreazioneA().before(ricercaFormContainer.getSigi().getDataCreazioneDa())))
		{
			ricercaFormContainer.getSigi().setDataCreazioneA(ricercaFormContainer.getSigi().getDataCreazioneDa());
			
		} else if (DATA_CREAZIONE_A.equals(fieldName) && (ricercaFormContainer.getSigi().getDataCreazioneDa() == null
				|| ricercaFormContainer.getSigi().getDataCreazioneDa().after(ricercaFormContainer.getSigi().getDataCreazioneA()))) {
			
			ricercaFormContainer.getSigi().setDataCreazioneDa(ricercaFormContainer.getSigi().getDataCreazioneA());
		}
	}

	/**
	 * Gestisce la modifica del range di date della ricerca Fascicoli.
	 * 
	 * @param fieldName
	 */
	public void onChangeRicercaFascicoliDataDaA(final String fieldName) {

		if (DATA_CREAZIONE_DA.equals(fieldName) && (ricercaFormContainer.getFascicoliRed().getDataCreazioneA() == null
				|| ricercaFormContainer.getFascicoliRed().getDataCreazioneA().before(ricercaFormContainer.getFascicoliRed().getDataCreazioneDa()))) {
		
			ricercaFormContainer.getFascicoliRed().setDataCreazioneA(ricercaFormContainer.getFascicoliRed().getDataCreazioneDa());
			ricercaFormContainer.getFascicoliRed().setAnnoFascicoloDisabled(true);
			
		} else if (DATA_CREAZIONE_A.equals(fieldName) && (ricercaFormContainer.getFascicoliRed().getDataCreazioneDa() == null
				|| ricercaFormContainer.getFascicoliRed().getDataCreazioneDa().after(ricercaFormContainer.getFascicoliRed().getDataCreazioneA()))) {
			
			ricercaFormContainer.getFascicoliRed().setDataCreazioneDa(ricercaFormContainer.getFascicoliRed().getDataCreazioneA());
			ricercaFormContainer.getFascicoliRed().setAnnoFascicoloDisabled(true);
		}
	}

	/**
	 * Gestisce la modifica del range di date della ricerca Faldoni.
	 * 
	 * @param fieldName
	 */
	public void onChangeRicercaFaldoniDataDaA(final String fieldName) {

		if (DATA_CREAZIONE_DA.equals(fieldName) && (ricercaFormContainer.getFaldoniRed().getDataCreazioneA() == null
				|| ricercaFormContainer.getFaldoniRed().getDataCreazioneA().before(ricercaFormContainer.getFaldoniRed().getDataCreazioneDa()))) {
			ricercaFormContainer.getFaldoniRed().setDataCreazioneA(ricercaFormContainer.getFaldoniRed().getDataCreazioneDa());

		} else if (DATA_CREAZIONE_A.equals(fieldName) && (ricercaFormContainer.getFaldoniRed().getDataCreazioneDa() == null
				|| ricercaFormContainer.getFaldoniRed().getDataCreazioneDa().after(ricercaFormContainer.getFaldoniRed().getDataCreazioneA()))) {
			ricercaFormContainer.getFaldoniRed().setDataCreazioneDa(ricercaFormContainer.getFaldoniRed().getDataCreazioneA());
		}
	}

	/**
	 * Restituisce l'utente.
	 * 
	 * @return utente
	 */
	@Override
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta le preferenze dell' {@link #utente}.
	 * 
	 * @param preferenze
	 */
	public void setPreferenze(final PreferenzeDTO preferenze) {
		this.preferenze = preferenze;
	}

	/**
	 * Restituisce le preferenze dell' {@link #utente}.
	 * 
	 * @return preferenze utente
	 */
	public PreferenzeDTO getPreferenze() {
		return preferenze;
	}

	/**
	 * Restituisce il token di navigation della pagina attiva.
	 * 
	 * @return navigation token pagina attiva
	 */
	public NavigationTokenEnum getActivePageInfo() {
		return activePageInfo;
	}

	/**
	 * Restituisce il logo MEF.
	 * 
	 * @return logo MEF
	 */
	public LogoMefEnum getLogoMEF() {
		return logoMEF;
	}

	/**
	 * Root per la foglia dell'organigramma
	 * 
	 * @return the root
	 */
	@Override
	public final TreeNode getRoot() {
		if (nodeSelected != null) {
			nodeSelected.setSelected(true);
		}
		return root;
	}

	/**
	 * @param root the root to set
	 */
	public final void setRoot(final TreeNode root) {
		this.root = root;
	}

	/**
	 * @return the menuCasellePosta
	 */
	public MenuModel getMenuCasellePosta() {
		if (accountSelected != null) {
			if (NavigationTokenEnum.MAIL.equals(activePageInfo)) {
				((DefaultMenuItem) accountSelected).setContainerStyleClass(SELECTED_MENU_STYLECLASS);
			} else {
				((DefaultMenuItem) accountSelected).setContainerStyleClass("");
			}
		}

		return menuCasellePosta;
	}

	/**
	 * Carica le notifiche utente dal BE
	 */
	public void caricaNotificheUtente() {
		notificheUtente = notificheUtenteSRV.getAll(utente);
		final ComparatorNotifiche compNot = new ComparatorNotifiche();
		notificheUtente.sort(compNot);
		setNotificheNonLette(notificheUtenteSRV.countNotificheNonLette());
		dataSincronizzazione = new Date();
		FacesHelper.update(CONTATORE_NOTIFICHE_ID);
	}

	/**
	 * @return the count
	 */
	public final CountDTO getCount() {
		return count;
	}

	/**
	 * @return the countFepa
	 */
	public final CountFepaDTO getCountFepa() {
		return countFepa;
	}

	/**
	 * @return the activeIndexVerticalMenu
	 */
	public final String getActiveIndexVerticalMenu() {
		return activeIndexVerticalMenu;
	}

	/**
	 * @param activeIndexVerticalMenu the activeIndexVerticalMenu to set
	 */
	public final void setActiveIndexVerticalMenu(final String activeIndexVerticalMenu) {
		this.activeIndexVerticalMenu = activeIndexVerticalMenu;
	}

	/**
	 * @return the activeIndexOrgMenu
	 */
	public final String getActiveIndexOrgMenu() {
		return activeIndexOrgMenu;
	}

	/**
	 * @param activeIndexOrgMenu the activeIndexOrgMenu to set
	 */
	public final void setActiveIndexOrgMenu(final String activeIndexOrgMenu) {
		this.activeIndexOrgMenu = activeIndexOrgMenu;
	}

	/**
	 * @return the activeIndexTabView
	 */
	public final String getActiveIndexTabView() {
		return activeIndexTabView;
	}

	/**
	 * @param activeIndexTabView the activeIndexTabView to set
	 */
	public final void setActiveIndexTabView(final String activeIndexTabView) {
		this.activeIndexTabView = activeIndexTabView;
	}

	/**
	 * @return the cartellaMailSelected
	 */
	public final CasellaMailStatoDTO getCartellaMailSelected() {
		return cartellaMailSelected;
	}

	/**
	 * Imposta la cartella mail selezionata.
	 * 
	 * @param cartellaMailSelected
	 */
	public void setCartellaMailSelected(final CasellaMailStatoDTO cartellaMailSelected) {
		this.cartellaMailSelected = cartellaMailSelected;
	}

	/**
	 * Restituisce la lista di caselle postali dell' {@link #utente}.
	 * 
	 * @return caselle postali utente
	 */
	public List<CasellaPostaDTO> getCp() {
		return cp;
	}

	/**
	 * Imposta la lista di caselle postali dell' {@link #utente}.
	 * 
	 * @param cp
	 */
	public void setCp(final List<CasellaPostaDTO> cp) {
		this.cp = cp;
	}

	/**
	 * Restituisce true se rubrica visibile, false altrimenti.
	 * 
	 * @return true se rubrica visibile, false altrimenti
	 */
	public boolean isShowRubrica() {
		return showRubrica;
	}

	/**
	 * Imposta la visibilita della rubrica.
	 * 
	 * @param showRubrica
	 */
	public void setShowRubrica(final boolean showRubrica) {
		this.showRubrica = showRubrica;
	}

	/**
	 * Restituisce il component per la gestione della renderizzazione della dialog.
	 * 
	 * @return il component
	 */
	public DialogRendererComponent getDialogRenderer() {
		return dialogRenderer;
	}

	/**
	 * @return ricercheDisponibili
	 */
	public final Collection<RicercheEnum> getRicercheDisponibili() {
		return ricercheDisponibili;
	}

	/**
	 * @param ricercheDisponibili
	 */
	public final void setRicercheDisponibili(final Collection<RicercheEnum> ricercheDisponibili) {
		this.ricercheDisponibili = ricercheDisponibili;
	}

	/**
	 * Restituisce l'indice dei faldoni attivi.
	 * 
	 * @return indice attivo faldoni
	 */
	public String getActiveIndexFaldoni() {
		return activeIndexFaldoni;
	}

	/**
	 * Imposta l'indice attivo dei faldoni.
	 * 
	 * @param activeIndexFaldoni
	 */
	public void setActiveIndexFaldoni(final String activeIndexFaldoni) {
		this.activeIndexFaldoni = activeIndexFaldoni;
	}

	/**
	 * Restituisce l'id del nuovo ufficio.
	 * 
	 * @return id nuovo ufficio
	 */
	public Long getIdNuovoUfficio() {
		return idNuovoUfficio;
	}

	/**
	 * Imposta l'id del nuovo ufficio.
	 * 
	 * @param idNuovoUfficio
	 */
	public void setIdNuovoUfficio(final Long idNuovoUfficio) {
		this.idNuovoUfficio = idNuovoUfficio;
	}

	/**
	 * Restituisce l'id nuovo dell'Area Organizzativa Omogenea.
	 * 
	 * @return id nuovo AOO
	 */
	public Long getIdNuovoAoo() {
		return idNuovoAoo;
	}

	/**
	 * Imposta l'id nuovo dell'Area Organizzativa Omogenea.
	 * 
	 * @param idNuovoAoo
	 */
	public void setIdNuovoAoo(final Long idNuovoAoo) {
		this.idNuovoAoo = idNuovoAoo;
	}

	/**
	 * Restituisce l'id del nuovo ruolo.
	 * 
	 * @return id nuovo ruolo
	 */
	public Long getIdNuovoRuolo() {
		return idNuovoRuolo;
	}

	/**
	 * Imposta l'id del nuovo ruolo.
	 * 
	 * @param idNuovoRuolo
	 */
	public void setIdNuovoRuolo(final Long idNuovoRuolo) {
		this.idNuovoRuolo = idNuovoRuolo;
	}

	/**
	 * Restituisce la lista degli uffici.
	 * 
	 * @return lista uffici
	 */
	public List<Nodo> getListaUffici() {
		return listaUffici;
	}

	/**
	 * Restituisce la lista dei ruoli.
	 * 
	 * @return lista ruoli
	 */
	public List<Ruolo> getListaRuoli() {
		return listaRuoli;
	}

	/**
	 * Restituisce la lista delle AOO disponibili per l'utente.
	 * 
	 * @return lista AOO
	 */
	public List<Aoo> getListaAOO() {
		return listaAOO;
	}

	/**
	 * @return request
	 */
	public final RicercaVeloceRequestDTO getRequest() {
		return request;
	}

	/**
	 * @param request
	 */
	public final void setRequest(final RicercaVeloceRequestDTO request) {
		this.request = request;
	}

	/**
	 * Restituisce il container per il form di ricerca.
	 * 
	 * @return container form ricerca
	 */
	public RicercaFormContainer getRicercaFormContainer() {
		return ricercaFormContainer;
	}

	/**
	 * Imposta il container per il form di ricerca.
	 * 
	 * @param ricercaFormContainer
	 */
	public void setRicercaFormContainer(final RicercaFormContainer ricercaFormContainer) {
		this.ricercaFormContainer = ricercaFormContainer;
	}

	/**
	 * @return maxYearForSearch
	 */
	public Integer getMaxYearForSearch() {
		return maxYearForSearch;
	}

	/**
	 * @param maxYearForSearch
	 */
	public void setMaxYearForSearch(final Integer maxYearForSearch) {
		this.maxYearForSearch = maxYearForSearch;
	}

	/**
	 * @return nomeRicercaAvanzata
	 */
	public final String getNomeRicercaAvanzata() {
		return nomeRicercaAvanzata;
	}

	/**
	 * @param nomeRicercaAvanzata
	 */
	public final void setNomeRicercaAvanzata(final String nomeRicercaAvanzata) {
		this.nomeRicercaAvanzata = nomeRicercaAvanzata;
	}

	/**
	 * @return ricercheSalvateDTH
	 */
	public final SimpleDetailDataTableHelper<RicercaAvanzataSalvata> getRicercheSalvateDTH() {
		return ricercheSalvateDTH;
	}

	/**
	 * @param ricercheSalvateDTH
	 */
	public final void setRicercheSalvateDTH(final SimpleDetailDataTableHelper<RicercaAvanzataSalvata> ricercheSalvateDTH) {
		this.ricercheSalvateDTH = ricercheSalvateDTH;
	}

	/**
	 * Verifica che la pagina attiva non sia una delle pagine della coda
	 * 
	 * @return true se la pagina attiva NON è una delle pagine delle code, false
	 *         altrimenti
	 */
	public boolean isNotActivePageCoda() {
		final NavigationTokenEnum activePage = getActivePageInfo();
		return activePage.getDocumentQueue() == null && activePage.getId() != 43;
	}

	/**
	 * Restituisce la lista delle notifiche.
	 * 
	 * @return notificheUtente
	 */
	public List<NotificheUtenteDTO> getNotificheUtente() {
		return notificheUtente;
	}

	/**
	 * Restituisce il container del form di ricerca avanzata.
	 * 
	 * @return formDocumenti
	 */
	public final RicercaAvanzataDocFormDTO getFormDocumenti() {
		return formDocumenti;
	}

	/**
	 * Imposta il DTO che contiene le informazioni del form di ricerca avanzata.
	 * 
	 * @param formDocumenti
	 */
	public final void setFormDocumenti(final RicercaAvanzataDocFormDTO formDocumenti) {
		this.formDocumenti = formDocumenti;
	}

	/**
	 * Restituisce la descrizione dell'assegnatario.
	 * 
	 * @return descrAssegnatario
	 */
	public final String getDescrAssegnatario() {
		return descrAssegnatario;
	}

	/**
	 * Imposta la descrizione dell'assegnatario.
	 * 
	 * @param descrAssegnatario
	 */
	public final void setDescrAssegnatario(final String descrAssegnatario) {
		this.descrAssegnatario = descrAssegnatario;
	}

	/**
	 * Restituisce la radice del tree delle assegnazioni.
	 * 
	 * @return rootAssegnazione
	 */
	public final DefaultTreeNode getRootAssegnazione() {
		return rootAssegnazione;
	}

	/**
	 * Imposta la radice del tree delle assegnazioni.
	 * 
	 * @param rootAssegnazione
	 */
	public final void setRootAssegnazione(final DefaultTreeNode rootAssegnazione) {
		this.rootAssegnazione = rootAssegnazione;
	}

	/**
	 * Restituisce l'assegnatario copia conforme.
	 * 
	 * @return assegnatarioCopiaConforme
	 */
	public final String getAssegnatarioCopiaConforme() {
		return assegnatarioCopiaConforme;
	}

	/**
	 * Imposta l'assegnatario copia conforme.
	 * 
	 * @param assegnatarioCopiaConforme
	 */
	public final void setAssegnatarioCopiaConforme(final String assegnatarioCopiaConforme) {
		this.assegnatarioCopiaConforme = assegnatarioCopiaConforme;
	}

	/**
	 * @return rootAssegnazioneOperazione
	 */
	public final DefaultTreeNode getRootAssegnazioneOperazione() {
		return rootAssegnazioneOperazione;
	}

	/**
	 * @param rootAssegnazioneOperazione
	 */
	public final void setRootAssegnazioneOperazione(final DefaultTreeNode rootAssegnazioneOperazione) {
		this.rootAssegnazioneOperazione = rootAssegnazioneOperazione;
	}

	/**
	 * Restituisce la descrizione dell'assegnatario operazione.
	 * 
	 * @return descrAssegnatarioOperazione
	 */
	public final String getDescrAssegnatarioOperazione() {
		return descrAssegnatarioOperazione;
	}

	/**
	 * Imposta la descrizione dell'assegnatario operazione.
	 * 
	 * @param descrAssegnatarioOperazione
	 */
	public final void setDescrAssegnatarioOperazione(final String descrAssegnatarioOperazione) {
		this.descrAssegnatarioOperazione = descrAssegnatarioOperazione;
	}

	/**
	 * Restituisce il DTO che memorizza i valori dei parametri della ricerca
	 * avanzata dei fascicoli.
	 * 
	 * @return formFascicoli
	 */
	public final RicercaAvanzataFascicoliFormDTO getFormFascicoli() {
		return formFascicoli;
	}

	/**
	 * Imposta il DTO che memorizza i valori dei parametri della ricerca avanzata
	 * dei fascicoli.
	 * 
	 * @param formFascicoli
	 */
	public final void setFormFascicoli(final RicercaAvanzataFascicoliFormDTO formFascicoli) {
		this.formFascicoli = formFascicoli;
	}

	/**
	 * Restituisce la radice del titolario.
	 * 
	 * @return rootTitolario
	 */
	public final DefaultTreeNode getRootTitolario() {
		return rootTitolario;
	}

	/**
	 * Imposta la radice del titolario.
	 * 
	 * @param rootTitolario
	 */
	public final void setRootTitolario(final DefaultTreeNode rootTitolario) {
		this.rootTitolario = rootTitolario;
	}

	/**
	 * Restituisce la descrizione del titolario.
	 * 
	 * @return descrizioneTitolario
	 */
	public final String getDescrizioneTitolario() {
		return descrizioneTitolario;
	}

	/**
	 * Imposta la descrizione del titolario.
	 * 
	 * @param descrizioneTitolario
	 */
	public final void setDescrizioneTitolario(final String descrizioneTitolario) {
		this.descrizioneTitolario = descrizioneTitolario;
	}

	/**
	 * @return rootAssegnazionefasc
	 */
	public final DefaultTreeNode getRootAssegnazionefasc() {
		return rootAssegnazionefasc;
	}

	/**
	 * @param rootAssegnazionefasc
	 */
	public final void setRootAssegnazionefasc(final DefaultTreeNode rootAssegnazionefasc) {
		this.rootAssegnazionefasc = rootAssegnazionefasc;
	}

	/**
	 * Restituisce la descrizione dell'assegnatario fascicolo.
	 * 
	 * @return descrAssegnatariofasc
	 */
	public final String getDescrAssegnatariofasc() {
		return descrAssegnatariofasc;
	}

	/**
	 * Imposta la descrizione dell'assegnatario fascicolo.
	 * 
	 * @param descrAssegnatariofasc
	 */
	public final void setDescrAssegnatariofasc(final String descrAssegnatariofasc) {
		this.descrAssegnatariofasc = descrAssegnatariofasc;
	}

	/**
	 * @return listaTipoDocumentoNPS
	 */
	public final List<KeyValueDTO> getListaTipoDocumentoNPS() {
		return listaTipoDocumentoNPS;
	}

	/**
	 * @param listaTipoDocumentoNPS
	 */
	public final void setListaTipoDocumentoNPS(final List<KeyValueDTO> listaTipoDocumentoNPS) {
		this.listaTipoDocumentoNPS = listaTipoDocumentoNPS;
	}

	/**
	 * Restituisce l'indice attivo dei fascicoli.
	 * 
	 * @return indice attivo fascicoli
	 */
	public String getActiveIndexFascicoli() {
		return activeIndexFascicoli;
	}

	/**
	 * Imposta l'indice attivo dei fascicoli.
	 * 
	 * @param activeIndexFascicoli
	 */
	public void setActiveIndexFascicoli(final String activeIndexFascicoli) {
		this.activeIndexFascicoli = activeIndexFascicoli;
	}

	/**
	 * Restituisce il dirigente delegante.
	 * 
	 * @return dirigenteDelegante
	 */
	public final String getDirigenteDelegante() {
		return dirigenteDelegante;
	}

	/**
	 * Restituisce today.
	 * 
	 * @return today
	 */
	public final String getToday() {
		return today;
	}

	/**
	 * Restituisce true se il button delle response è visibile, false altrimenti.
	 * 
	 * @return showBtnResp
	 */
	public final Boolean getShowBtnResp() {
		return showBtnResp;
	}

	/**
	 * Restituisce contatore documenti non classificati.
	 * 
	 * @return counterNumeroNonClassificati
	 */
	public final String getCounterNumeroNonClassificati() {
		return counterNumeroNonClassificati;
	}

	/**
	 * @return rootFald
	 */
	public final TreeNode getRootFald() {
		return rootFald;
	}

	/**
	 * @param rootFald
	 */
	public final void setRootFald(final TreeNode rootFald) {
		this.rootFald = rootFald;
	}

	private String isFormNpsOkNew() {
		String out = null;

		final boolean isRangeDateOk = DateUtils.isRangeAnnualeOk(ricercaFormContainer.getDatiProtocollo().getDataProtocolloDa(),
				ricercaFormContainer.getDatiProtocollo().getDataProtocolloA());

		if (((ricercaFormContainer.getDatiProtocollo().getAnnoProtocollo() != null) && (ricercaFormContainer.getDatiProtocollo().getNumeroProtocolloDa() != null)
				&& (ricercaFormContainer.getDatiProtocollo().getNumeroProtocolloA() != null))
				|| (isRangeDateOk && ricercaFormContainer.getDatiProtocollo().getDataProtocolloDa() != null
						&& ricercaFormContainer.getDatiProtocollo().getDataProtocolloA() != null)
				|| ((ricercaFormContainer.getDatiProtocollo().getAnnoProtocollo() != null)
						&& (!StringUtils.isNullOrEmpty(ricercaFormContainer.getDatiProtocollo().getOggetto())))) {
			return null;
		} else if (!isRangeDateOk) {
			out = RESTINGIMENTO_RICERCA_SHORT_MSG;
		} else if (ricercaFormContainer.getDatiProtocollo().getDataProtocolloDa() == null && ricercaFormContainer.getDatiProtocollo().getDataProtocolloA() != null) {
			out = " 'Data Protocollo A' deve essere impostata, se si decide di inserire 'Data Protocollo DA'";
		} else if (ricercaFormContainer.getDatiProtocollo().getDataProtocolloDa() != null && ricercaFormContainer.getDatiProtocollo().getDataProtocolloA() == null) {
			out = " 'Data Protocollo DA' deve essere impostata, se si decide di inserire 'Data Protocollo A'";
		} else if (ricercaFormContainer.getDatiProtocollo().getNumeroProtocolloDa() == null && ricercaFormContainer.getDatiProtocollo().getNumeroProtocolloA() != null) {
			out = " Il Numero Protocollo 'DA' deve essere impostato, se si decide di inserire un numero protocollo 'A'";
		} else if (ricercaFormContainer.getDatiProtocollo().getNumeroProtocolloDa() != null && ricercaFormContainer.getDatiProtocollo().getNumeroProtocolloA() == null) {
			out = " Il Numero Protocollo 'DA' deve essere impostato, se si decide di inserire un numero protocollo 'A'";
		} else if (ricercaFormContainer.getDatiProtocollo().getNumeroProtocolloDa() != null && ricercaFormContainer.getDatiProtocollo().getNumeroProtocolloA() != null
				&& (ricercaFormContainer.getDatiProtocollo().getNumeroProtocolloA() < ricercaFormContainer.getDatiProtocollo().getNumeroProtocolloDa())) {
			out = " Il Numero Protocollo 'DA' non puo essere maggiore del numero protocollo 'A'";
		} else {
			out = RESTINGIMENTO_RICERCA_SHORT_MSG;
		}

		return out;
	}

	/**
	 * Restituisce true se carica l'organigramma, false altrimenti.
	 * 
	 * @return true se carica l'organigramma, false altrimenti
	 */
	public boolean getCaricaOrganigramma() {
		return caricaOrganigramma;
	}

	/**
	 * Imposta il flag associato al caricamento dell'organigramma.
	 * 
	 * @param caricaOrganigramma
	 */
	public void setCaricaOrganigramma(final boolean caricaOrganigramma) {
		this.caricaOrganigramma = caricaOrganigramma;
	}

	/**
	 * @return nodiFigliExist
	 */
	public boolean getNodiFigliExist() {
		return nodiFigliExist;
	}

	/**
	 * @param nodiFigliExist
	 */
	public void setNodiFigliExist(final boolean nodiFigliExist) {
		this.nodiFigliExist = nodiFigliExist;
	}

	/**
	 * Gestisce il caricamento degli uffici.
	 */
	public void caricaUfficiBtn() {
		caricaOrganigramma = !caricaOrganigramma;
		OrgObjDTO orgObj = (OrgObjDTO) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.ORG_OBJ, false);
		if (!caricaOrganigramma && orgObj != null && !"ROOT".equals(orgObj.getNode().getParent().getType())) {
			FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.ORG_OBJ, true);
			orgObj = null;
			FacesHelper.executeJS("hideCentralEastSection()");
		}
		loadOrgUtente(orgObj != null);
	}

	/**
	 * Restituisce l'icona dell'organigramma.
	 * 
	 * @return icona dell'organigramma
	 */
	public String getIconOrganigramma() {
		return iconOrganigramma;
	}

	/**
	 * Imposta l'icona dell'organigramma.
	 * 
	 * @param iconOrganigramma
	 */
	public void setIconOrganigramma(final String iconOrganigramma) {
		this.iconOrganigramma = iconOrganigramma;
	}

	/**
	 * Restituisce true se help page visibile, false altrimenti.
	 * 
	 * @return true se help page visibile, false altrimenti
	 */
	public Boolean getShowHelpPage() {
		return showHelpPage;
	}

	/**
	 * Imposta la visibilita dell'help page.
	 */
	public void showHelpPage() {
		this.showHelpPage = true;
	}

	/**
	 * Restituisce true se rubrica visibile, false altrimenti.
	 * 
	 * @return true se rubrica visibile, false altrimenti
	 */
	public boolean getRenderRubrica() {
		return renderRubrica;
	}

	/**
	 * Imposta la visibilita della rubrica.
	 * 
	 * @param renderRubrica
	 */
	public void setRenderRubrica(final boolean renderRubrica) {
		this.renderRubrica = renderRubrica;
	}

	/**
	 * Restituisce true se la provenienza è dalla dashboard, false altrimenti.
	 * 
	 * @return true se la provenienza è dalla dashboard, false altrimenti
	 */
	public boolean getFromDashboard() {
		return fromDashboard;
	}

	/**
	 * Imposta la provenienza rispettivamente alla dashboard.
	 * 
	 * @param fromDashboard
	 */
	public void setFromDashboard(final boolean fromDashboard) {
		this.fromDashboard = fromDashboard;
	}

	/**
	 * @return isGoToSottoscrizioni
	 */
	public boolean getIsGoToSottoscrizioni() {
		return isGoToSottoscrizioni;
	}

	/**
	 * @param isGoToSottoscrizioni
	 */
	public void setIsGoToSottoscrizioni(final boolean isGoToSottoscrizioni) {
		this.isGoToSottoscrizioni = isGoToSottoscrizioni;
	}

	/**
	 * Restituisce l'id della notifica selezionata.
	 * 
	 * @return id notifica selezionata
	 */
	public Integer getIdNotificaSelezionataSott() {
		return idNotificaSelezionataSott;
	}

	/**
	 * Imposta l'id della notifica selezionata.
	 * 
	 * @param idNotificaSelezionataSott
	 */
	public void setIdNotificaSelezionataSott(final Integer idNotificaSelezionataSott) {
		this.idNotificaSelezionataSott = idNotificaSelezionataSott;
	}

	/**
	 * Restituisce il nodo del server.
	 * 
	 * @return nodo server
	 */
	public String getNodoServer() {
		return nodoServer;
	}

	/**
	 * Restituisce il template max coda mail.
	 * 
	 * @return template max coda mail
	 */
	public String getTemplateMaxCodaMail() {
		return templateMaxCodaMail;
	}

	/**
	 * Imposta il template max coda mail.
	 * 
	 * @param templateMaxCodaMail
	 */
	public void setTemplateMaxCodaMail(final String templateMaxCodaMail) {
		this.templateMaxCodaMail = templateMaxCodaMail;
	}

	/**
	 * Gestisce la modifica della date del range di ricerca dei protocolli
	 * pregressi.
	 * 
	 * @param fieldName
	 */
	public void onChangeProtocolliPregressiDataDaA(final String fieldName) {
		if ("dataDaProt".equals(fieldName) && (ricercaFormContainer.getProtocolliDF().getDataProtocollazioneA() == null 
				|| ricercaFormContainer.getProtocolliDF().getDataProtocollazioneA().before(ricercaFormContainer.getProtocolliDF().getDataProtocollazioneDa()))) {
		
			ricercaFormContainer.getProtocolliDF().setDataProtocollazioneA(ricercaFormContainer.getProtocolliDF().getDataProtocollazioneDa());
			ricercaFormContainer.getProtocolliDF().setAnnoProtocolloDisabled(true);

		} else if ("dataAProt".equals(fieldName) && (ricercaFormContainer.getProtocolliDF().getDataProtocollazioneDa() == null 
				|| ricercaFormContainer.getProtocolliDF().getDataProtocollazioneDa().after(ricercaFormContainer.getProtocolliDF().getDataProtocollazioneA()))) {
			
			ricercaFormContainer.getProtocolliDF().setDataProtocollazioneDa(ricercaFormContainer.getProtocolliDF().getDataProtocollazioneA());
			ricercaFormContainer.getProtocolliDF().setAnnoProtocolloDisabled(true);
		}
	}

	/**
	 * Gestisce la modifica della date del range di ricerca dei fascicoli pregressi.
	 * 
	 * @param fieldName
	 */
	public void onChangeFascicoliPregressiDataDaA(final String fieldName) {
		if ("dataAperturaDa".equals(fieldName) && (ricercaFormContainer.getFascicoliDF().getDataAperturaA() == null
					|| ricercaFormContainer.getFascicoliDF().getDataAperturaA().before(ricercaFormContainer.getFascicoliDF().getDataAperturaDa()))) {
			
				ricercaFormContainer.getFascicoliDF().setDataAperturaA(ricercaFormContainer.getFascicoliDF().getDataAperturaDa());

		} else if ("dataAperturaA".equals(fieldName) && (ricercaFormContainer.getFascicoliDF().getDataAperturaDa() == null
				|| ricercaFormContainer.getFascicoliDF().getDataAperturaDa().after(ricercaFormContainer.getFascicoliDF().getDataAperturaA()))) {
		
			ricercaFormContainer.getFascicoliDF().setDataAperturaDa(ricercaFormContainer.getFascicoliDF().getDataAperturaA());
			
		} else if ("dataChiusuraDa".equals(fieldName) && (ricercaFormContainer.getFascicoliDF().getDataChiusuraA() == null
				|| ricercaFormContainer.getFascicoliDF().getDataChiusuraA().before(ricercaFormContainer.getFascicoliDF().getDataChiusuraDa()))) {
		
			ricercaFormContainer.getFascicoliDF().setDataChiusuraA(ricercaFormContainer.getFascicoliDF().getDataChiusuraDa());
			
		} else if ("dataChiusuraA".equals(fieldName) && (ricercaFormContainer.getFascicoliDF().getDataChiusuraDa() == null
				|| ricercaFormContainer.getFascicoliDF().getDataChiusuraDa().after(ricercaFormContainer.getFascicoliDF().getDataChiusuraA()))) {
		
			ricercaFormContainer.getFascicoliDF().setDataChiusuraDa(ricercaFormContainer.getFascicoliDF().getDataChiusuraA());
			
		} else if ("dataScadenzaDa".equals(fieldName) && (ricercaFormContainer.getFascicoliDF().getDataScadenzaA() == null
				|| ricercaFormContainer.getFascicoliDF().getDataScadenzaA().before(ricercaFormContainer.getFascicoliDF().getDataScadenzaDa()))) {
		
			ricercaFormContainer.getFascicoliDF().setdataScadenzaA(ricercaFormContainer.getFascicoliDF().getDataScadenzaDa());
			
		} else if ("dataScadenzaA".equals(fieldName) && (ricercaFormContainer.getFascicoliDF().getDataScadenzaDa() == null
				|| ricercaFormContainer.getFascicoliDF().getDataScadenzaDa().after(ricercaFormContainer.getFascicoliDF().getDataScadenzaA()))) {
		
			ricercaFormContainer.getFascicoliDF().setDataScadenzaDa(ricercaFormContainer.getFascicoliDF().getDataScadenzaA());
		}
	}

	/**
	 * Restituisce la descrizione del tipo operazione dato l'id
	 */
	private String getDescrizioneTipoOperazione(final Integer idTipoOperazione) {

		if (!CollectionUtils.isEmpty(formDocumenti.getComboTipiOperazione())) {
			for (final TipoEvento tipo : formDocumenti.getComboTipiOperazione()) {
				if (tipo.getIdTipoEvento() == idTipoOperazione) {
					return tipo.getDescrizione();
				}
			}
		}
		return "";
	}

	/**
	 * Restituisce il numero massimo iteratore protocollo.
	 * 
	 * @return numero massimo iteratore protocollo
	 */
	public String getMaxNumIteratoreProtocollo() {
		return maxNumIteratoreProtocollo;
	}

	/**
	 * Restituisce lo StreamedContent per la stampa etichette.
	 * 
	 * @return StreamedContent stampa etichette
	 */
	public StreamedContent getScStampaEtichette() {
		return scStampaEtichette;
	}

	/**
	 * Imposta lo StreamedContent per la stampa etichette.
	 * 
	 * @param scStampaEtichette
	 */
	public void setScStampaEtichette(final StreamedContent scStampaEtichette) {
		this.scStampaEtichette = scStampaEtichette;
	}

	/**
	 * Restituisce il numero pagine totali da ricerca prot.
	 * 
	 * @return numero pagine
	 */
	public String getNumPagineTotaliDaRicercaProt() {
		return numPagineTotaliDaRicercaProt;
	}

	/**
	 * Imposta il numero pagine totali da ricerca prot.
	 * 
	 * @param numPagineTotaliDaRicercaProt
	 */
	public void setNumPagineTotaliDaRicercaProt(final String numPagineTotaliDaRicercaProt) {
		this.numPagineTotaliDaRicercaProt = numPagineTotaliDaRicercaProt;
	}

	/**
	 * Restituisce il numero dell'ultima riga dei protocolli.
	 * 
	 * @return numero ultima riga
	 */
	public String getLastRowNumProtocollo() {
		return lastRowNumProtocollo;
	}

	/**
	 * Imposta il numero dell'ultima riga dei protocolli.
	 * 
	 * @param lastRowNumProtocollo
	 */
	public void setLastRowNumProtocollo(final String lastRowNumProtocollo) {
		this.lastRowNumProtocollo = lastRowNumProtocollo;
	}

	/**
	 * Restituisce il numero massimo di righe per pagina dei protocolli.
	 * 
	 * @return numero massimo righe
	 */
	public Integer getMaxRowPageProtocolliNsd() {
		return maxRowPageProtocolliNsd;
	}

	/**
	 * Imposta il numero massimo di righe per pagina dei protocolli.
	 * 
	 * @param maxRowPageProtocolliNsd
	 */
	public void setMaxRowPageProtocolliNsd(final Integer maxRowPageProtocolliNsd) {
		this.maxRowPageProtocolliNsd = maxRowPageProtocolliNsd;
	}

	/**
	 * Restituisce il numero massimo di righe per pagina dei fascicoli Nsd.
	 * 
	 * @return numero massimo righe
	 */
	public int getMaxRowPageFascicoliNsd() {
		return maxRowPageFascicoliNsd;
	}

	/**
	 * Imposta il numero massimo di righe per pagina dei fascicoli Nsd.
	 * 
	 * @param maxRowPageFascicoliNsd
	 */
	public void setMaxRowPageFascicoliNsd(final int maxRowPageFascicoliNsd) {
		this.maxRowPageFascicoliNsd = maxRowPageFascicoliNsd;
	}

	/**
	 * Restituisce il numero pagina attuale protocolli.
	 * 
	 * @return numero pagina attuale
	 */
	public int getNumPaginaAttualeProtocollo() {
		return numPaginaAttualeProtocollo;
	}

	/**
	 * Imposta il numero pagina attuale protocolli.
	 * 
	 * @param numPaginaAttualeProtocollo
	 */
	public void setNumPaginaAttualeProtocollo(final int numPaginaAttualeProtocollo) {
		this.numPaginaAttualeProtocollo = numPaginaAttualeProtocollo;
	}

	/**
	 * @return templateMaxRowPageProtocolli
	 */
	public String getTemplateMaxRowPageProtocolli() {
		return templateMaxRowPageProtocolli;
	}

	/**
	 * @param templateMaxRowPageProtocolli
	 */
	public void setTemplateMaxRowPageProtocolli(final String templateMaxRowPageProtocolli) {
		this.templateMaxRowPageProtocolli = templateMaxRowPageProtocolli;
	}

	/**
	 * @return templateMaxRowPageFascicoli
	 */
	public String getTemplateMaxRowPageFascicoli() {
		return templateMaxRowPageFascicoli;
	}

	/**
	 * @param templateMaxRowPageFascicoli
	 */
	public void setTemplateMaxRowPageFascicoli(final String templateMaxRowPageFascicoli) {
		this.templateMaxRowPageFascicoli = templateMaxRowPageFascicoli;
	}

	/**
	 * Restituisce il numero pagina totali da ricerca fascicoli.
	 * 
	 * @return numero totale pagine
	 */
	public String getNumPagineTotaliDaRicercaFasc() {
		return numPagineTotaliDaRicercaFasc;
	}

	/**
	 * Imposta il numero pagina totali da ricerca fascicoli.
	 * 
	 * @param numPagineTotaliDaRicercaFasc
	 */
	public void setNumPagineTotaliDaRicercaFasc(final String numPagineTotaliDaRicercaFasc) {
		this.numPagineTotaliDaRicercaFasc = numPagineTotaliDaRicercaFasc;
	}

	/**
	 * Restituisce il numero pagina attuali fascicolo.
	 * 
	 * @return numero pagina attuale
	 */
	public int getNumPaginaAttualeFascicolo() {
		return numPaginaAttualeFascicolo;
	}

	/**
	 * Imposta il numero pagina attuali fascicolo.
	 * 
	 * @param numPaginaAttualeFascicolo
	 */
	public void setNumPaginaAttualeFascicolo(final int numPaginaAttualeFascicolo) {
		this.numPaginaAttualeFascicolo = numPaginaAttualeFascicolo;
	}

	/**
	 * Restituisce il numero dell'ultima riga dei fascicoli.
	 * 
	 * @return numero ultima riga
	 */
	public Integer getLastRowNumFascicolo() {
		return lastRowNumFascicolo;
	}

	/**
	 * Imposta il numero dell'ultima riga dei fascicoli.
	 * 
	 * @param lastRowNumFascicolo
	 */
	public void setLastRowNumFascicolo(final Integer lastRowNumFascicolo) {
		this.lastRowNumFascicolo = lastRowNumFascicolo;
	}

	/**
	 * Restituisce il numero massimo iteraotre fascicolo.
	 * 
	 * @return numero massimo iteratore fascicolo
	 */
	public Integer getMaxNumIteratoreFascicolo() {
		return maxNumIteratoreFascicolo;
	}

	/**
	 * Imposta il numero massimo iteraotre fascicolo.
	 * 
	 * @param maxNumIteratoreFascicolo
	 */
	public void setMaxNumIteratoreFascicolo(final Integer maxNumIteratoreFascicolo) {
		this.maxNumIteratoreFascicolo = maxNumIteratoreFascicolo;
	}

	/**
	 * Restituisce il DTO che memorizza i valori dei parametri di ricerca dei
	 * fascicoli DF.
	 * 
	 * @return parametri ricerca fascicoli DF
	 */
	public ParamsRicercaFascicoliPregressiDfDTO getFascicoliDF() {
		return fascicoliDF;
	}

	/**
	 * Imposta il DTO che memorizza i valori dei parametri di ricerca dei fascicoli
	 * DF.
	 * 
	 * @param fascicoliDF
	 */
	public void setFascicoliDF(final ParamsRicercaFascicoliPregressiDfDTO fascicoliDF) {
		this.fascicoliDF = fascicoliDF;
	}

	/**
	 * Restituisce il DTO che memorizza i valori dei parametri di ricerca dei
	 * protocolli DF.
	 * 
	 * @return parametri ricerca protocolli DF
	 */
	public ParamsRicercaProtocolliPregressiDfDTO getProtocolliDF() {
		return protocolliDF;
	}

	/**
	 * Imposta il DTO che memorizza i valori dei parametri di ricerca dei protocolli
	 * DF.
	 * 
	 * @param protocolliDF
	 */
	public void setProtocolliDF(final ParamsRicercaProtocolliPregressiDfDTO protocolliDF) {
		this.protocolliDF = protocolliDF;
	}

	/**
	 * Restituisce la tipologia del registro Nsd.
	 * 
	 * @return tipo registro nsd
	 */
	public List<TipologiaRegistroNsdDTO> getTipologiaRegistroNsd() {
		return tipologiaRegistroNsd;
	}

	/**
	 * Restituisce la tipologia del documento Nsd.
	 * 
	 * @return tipo documento nsd
	 */
	public List<TipologiaRegistroNsdDTO> getTipologiaDocumentoNsd() {
		return tipologiaDocumentoNsd;
	}

	/**
	 * Imposta la tipologia del registro Nsd.
	 * 
	 * @param inTipologiaRegistroNsd
	 */
	public void setTipologiaRegistroNsd(final List<TipologiaRegistroNsdDTO> inTipologiaRegistroNsd) {
		tipologiaRegistroNsd = inTipologiaRegistroNsd;
	}

	/**
	 * Imposta la tipologia del registro Nsd.
	 * 
	 * @param inTipologiaDocumentoNsd
	 */
	public void setTipologiaDocumentoNsd(final List<TipologiaRegistroNsdDTO> inTipologiaDocumentoNsd) {
		tipologiaDocumentoNsd = inTipologiaDocumentoNsd;
	}

	/**
	 * Restituisce la root del titolario di ricerca.
	 * 
	 * @return root titolario ricerca
	 */
	public DefaultTreeNode getRootTitolarioRicerca() {
		return rootTitolarioRicerca;
	}

	/**
	 * Imposta la root del titolario di ricerca.
	 * 
	 * @param rootTitolarioRicerca
	 */
	public void setRootTitolarioRicerca(final DefaultTreeNode rootTitolarioRicerca) {
		this.rootTitolarioRicerca = rootTitolarioRicerca;
	}

	/**
	 * Restituisce il bean che gestisce la ricerca avanzata UCB.
	 * 
	 * @return bean ricerca UCB
	 */
	public RicercaAvanzataUcbBean getRicercaUCB() {
		return ricercaUCB;
	}

	/**
	 * Imposta il bean che gestisce la ricerca avanzata UCB.
	 * 
	 * @param ricercaUCB
	 */
	public void setRicercaUCB(final RicercaAvanzataUcbBean ricercaUCB) {
		this.ricercaUCB = ricercaUCB;
	}

	/**
	 * Restituisce il bean che gestisce la ricerca per registrazioni ausiliarie.
	 * 
	 * @return bean ricerca registrazioni ausiliarie
	 */
	public RicercaRegistrazioniAusiliarieBean getRicercaRegistrazioniAusiliarie() {
		return ricercaRegistrazioniAusiliarie;
	}

	/**
	 * Imposta il bean che gestisce la ricerca per registrazioni ausiliarie.
	 * 
	 * @param ricercaRegistrazioniAusiliarie
	 */
	public void setRicercaRegistrazioniAusiliarie(final RicercaRegistrazioniAusiliarieBean ricercaRegistrazioniAusiliarie) {
		this.ricercaRegistrazioniAusiliarie = ricercaRegistrazioniAusiliarie;
	}

	/**
	 * Restituisce l'account selezionato.
	 * 
	 * @return account selezionato
	 */
	public MenuItem getAccountSelected() {
		return accountSelected;
	}

	/**
	 * Imposta la notifica recuperata dalla selezione come letta.
	 * 
	 * @param event
	 */
	public void segnaNotificaLetta(final SelectEvent event) {
		final NotificheUtenteDTO notificaDTO = (NotificheUtenteDTO) event.getObject();
		final int index = this.notificheUtente.indexOf(notificaDTO);

		segnaNotificaComeLetta(notificaDTO, false, index);
	}

	/**
	 * Segna tutte le {@link #notificheUtente} come lette.
	 */
	public void segnaNotificheLette() {
		for (final NotificheUtenteDTO notifica : notificheUtente) {
			segnaNotificaComeLetta(notifica, true, null);
		}
	}

	private void segnaNotificaComeLetta(final NotificheUtenteDTO notificaDTO, final boolean fromMultiple, final Integer indexNot) {
		try {
			boolean faiUpdate = false;
			if (notificaDTO != null) {
				if (notificaDTO.getNotificheCalendario() != null && notificaDTO.getNotificheCalendario().isDaLeggere()) {
					notificaDTO.getNotificheCalendario().setDaLeggere(false);
					faiUpdate = true;
					if (EventoCalendarioEnum.SCADENZA.equals(notificaDTO.getNotificheCalendario().getTipoCalendario().getTipoEnum())) {
						notificheUtenteSRV.contrassegnaNotificaCalendarioScadenza(utente.getId(), utente.getIdAoo(), notificaDTO.getNotificheCalendario().getDocumentTitle(),
								StatoNotificaUtenteEnum.NOTIFICA_LETTA.getIdStato());
					} else {
						notificheUtenteSRV.contrassegnaNotificaCalendario(utente.getId(), utente.getIdAoo(), notificaDTO.getNotificheCalendario().getIdEvento(),
								StatoNotificaUtenteEnum.NOTIFICA_LETTA.getIdStato());
					}
				} else if (notificaDTO.getNotificheRubrica() != null && notificaDTO.getNotificheRubrica().isDaLeggere()) {
					faiUpdate = true;
					notificaDTO.getNotificheRubrica().setDaLeggere(false);
					notificheUtenteSRV.contrassegnaNotificaRubrica(utente.getId(), utente.getIdAoo(), notificaDTO.getNotificheRubrica().getIdNotificaContatto(),
							StatoNotificaUtenteEnum.NOTIFICA_LETTA.getIdStato());
				} else if (notificaDTO.getNotificheSottoscrizioni() != null && notificaDTO.getNotificheSottoscrizioni().isDaLeggere()) {
					faiUpdate = true;
					notificaDTO.getNotificheSottoscrizioni().setDaLeggere(false);
					notificaSRV.contrassegnaNotifiche(notificaDTO.getNotificheSottoscrizioni());
				}

				if (faiUpdate && !fromMultiple) {
					notificheNonLette = notificheNonLette - 1;
					// Serve per fare l'update senza ricaricare l'intero datatable
					final int index = indexNot;
					final String metodoJSConParametri = "rimuoviHighlightNotifica(" + index + ")";
					FacesHelper.executeJS(metodoJSConParametri);
					FacesHelper.update(CONTATORE_NOTIFICHE_ID);
				} else if (faiUpdate) {
					notificheNonLette = notificheNonLette - 1;
					FacesHelper.update(CONTATORE_NOTIFICHE_ID);
				}
			}
		} catch (final Exception ex) {
			LOGGER.error(ERRORE_NELL_AGGIORNAMENTO_DELLE_NOTIFICHE, ex);
			showError(ERRORE_NELL_AGGIORNAMENTO_DELLE_NOTIFICHE);
		}
	}

	/**
	 * Elimina una notifica utente selezionata come target:
	 * {@link #deleteNotificaTarget}.
	 */
	public void deleteNotificaUtente() {
		deleteNotificaUtente(deleteNotificaTarget, true);
	}

	/**
	 * Elimina tutte le notifiche {@link #notificheUtente}.
	 */
	public void deleteNotificheUtente() {
		for (final NotificheUtenteDTO notifica : notificheUtente) {
			deleteNotificaUtente(notifica, false);
		}
	}

	/**
	 * Elimina la notifica definita dal parametro in ingresso.
	 * 
	 * @param notificaDaEliminare
	 * @param fromAzioneSingola
	 */
	public void deleteNotificaUtente(final NotificheUtenteDTO notificaDaEliminare, final boolean fromAzioneSingola) {
		try {
			if (notificaDaEliminare != null) {
				if (notificaDaEliminare.getNotificheCalendario() != null) {
					if (notificaDaEliminare.getNotificheCalendario().isDaLeggere()) {
						notificheNonLette = notificheNonLette - 1;
					}
					notificaDaEliminare.getNotificheCalendario().setStatoNotificaCalendario(StatoNotificaUtenteEnum.NOTIFICA_ELIMINATA.getIdStato());
					if (EventoCalendarioEnum.SCADENZA.equals(notificaDaEliminare.getNotificheCalendario().getTipoCalendario().getTipoEnum())) {
						notificheUtenteSRV.eliminaNotificaCalendarioScadenza(utente.getId(), utente.getIdAoo(),
								notificaDaEliminare.getNotificheCalendario().getDocumentTitle(), StatoNotificaUtenteEnum.NOTIFICA_ELIMINATA.getIdStato());
					} else {
						notificheUtenteSRV.eliminaNotificaCalendario(utente.getId(), utente.getIdAoo(), notificaDaEliminare.getNotificheCalendario().getIdEvento(),
								StatoNotificaUtenteEnum.NOTIFICA_ELIMINATA.getIdStato());
					}
				} else if (notificaDaEliminare.getNotificheRubrica() != null) {
					if (notificaDaEliminare.getNotificheRubrica().isDaLeggere()) {
						notificheNonLette = notificheNonLette - 1;
					}
					notificaDaEliminare.getNotificheRubrica().setStatoNotificaRubrica(StatoNotificaUtenteEnum.NOTIFICA_ELIMINATA.getIdStato());
					notificheUtenteSRV.eliminaNotificaRubrica(utente.getId(), utente.getIdAoo(), notificaDaEliminare.getNotificheRubrica().getIdNotificaContatto(),
							StatoNotificaUtenteEnum.NOTIFICA_ELIMINATA.getIdStato());
				} else if (notificaDaEliminare.getNotificheSottoscrizioni() != null) {
					if (notificaDaEliminare.getNotificheSottoscrizioni().isDaLeggere()) {
						notificheNonLette = notificheNonLette - 1;
					}
					notificaDaEliminare.getNotificheSottoscrizioni().setStatoNotifica(StatoNotificaUtenteEnum.NOTIFICA_ELIMINATA.getIdStato());
					notificaSRV.eliminaNotifiche(notificaDaEliminare.getNotificheSottoscrizioni());
				}
				FacesHelper.update(CONTATORE_NOTIFICHE_ID);
				if (fromAzioneSingola) {
					notificheUtente.remove(notificaDaEliminare);
				}
			}
		} catch (final Exception ex) {
			LOGGER.error(ERRORE_NELL_AGGIORNAMENTO_DELLE_NOTIFICHE, ex);
			showError(ERRORE_NELL_AGGIORNAMENTO_DELLE_NOTIFICHE);
		}
	}

	/**
	 * Restituisce la notifica designata come "Da eliminare".
	 * 
	 * @return notifica da eliminare
	 */
	public NotificheUtenteDTO getDeleteNotificaTarget() {
		return deleteNotificaTarget;
	}

	/**
	 * Imposta la notifica da eliminare.
	 * 
	 * @param deleteNotificaTarget
	 */
	public void setDeleteNotificaTarget(final NotificheUtenteDTO deleteNotificaTarget) {
		this.deleteNotificaTarget = deleteNotificaTarget;
	}

	/**
	 * Restituisce la data di sincronizzazione.
	 * 
	 * @return data sincronizzazione
	 */
	public Date getDataSincronizzazione() {
		return dataSincronizzazione;
	}

	/**
	 * Imposta la data di sincronizzazione.
	 * 
	 * @param dataSincronizzazione
	 */
	public void setDataSincronizzazione(final Date dataSincronizzazione) {
		this.dataSincronizzazione = dataSincronizzazione;
	}

	/**
	 * Restituisce il numero di notifiche non lette.
	 * 
	 * @return numero notifiche non lette
	 */
	public int getNotificheNonLette() {
		return notificheNonLette;
	}

	/**
	 * Imposta il numero di notifiche non lette.
	 * 
	 * @param notificheNonLette
	 */
	public void setNotificheNonLette(final int notificheNonLette) {
		this.notificheNonLette = notificheNonLette;
	}

	/**
	 * Restituisce la casella mail in fase di ripristino.
	 * 
	 * @return casella mail in fase di ripristino
	 */
	public String getCasellaMailRipristina() {
		return casellaMailRipristina;
	}

	/**
	 * Imposta la casella mail in fase di ripristino.
	 * 
	 * @param incasellaMailRipristina
	 */
	public void setCasellaMailRipristina(final String incasellaMailRipristina) {
		this.casellaMailRipristina = incasellaMailRipristina;
	}

	/**
	 * Restituisce il navigation token dell'ultima pagina visitata.
	 * 
	 * @return last page navigation token
	 */
	public NavigationTokenEnum getLastPageInfo() {
		return lastPageInfo;
	}

	/**
	 * Imposta il token dell'ultima pagina visitata.
	 * 
	 * @param lastPageInfo
	 */
	public void setLastPageInfo(final NavigationTokenEnum lastPageInfo) {
		this.lastPageInfo = lastPageInfo;
	}

	/**
	 * Restituisce un title semplice per la pagina.
	 * 
	 * @return title pagina
	 */
	public String getSimpleTitlePage() {
		String titlePage = "";
		if (lastPageInfo != null) {
			titlePage = lastPageInfo.getTitlePage();
			titlePage = titlePage.replace("RedEVO: ", "");
		}

		return titlePage;
	}

	/**
	 * Restituisce la lista delle colonne delle code.
	 * 
	 * @return lista colonne code
	 */
	public List<ColonneCodeDTO> getListaColonneCodeDTO() {
		return listaColonneCodeDTO;
	}

	/**
	 * Imposta la lista delle colonne delle code.
	 * 
	 * @param listaColonneCodeDTO
	 */
	public void setListaColonneCodeDTO(final List<ColonneCodeDTO> listaColonneCodeDTO) {
		this.listaColonneCodeDTO = listaColonneCodeDTO;
	}

	/**
	 * Restituisce true se dialog di stampa renderizzata, false altrimenti.
	 * 
	 * @return true se dialog di stampa renderizzata, false altrimenti
	 */
	public boolean isRenderDialogStampa() {
		return renderDialogStampa;
	}

	/**
	 * Imposta il flag associato alla renderizzazione della dialog stampa.
	 * 
	 * @param renderDialogStampa
	 */
	public void setRenderDialogStampa(final boolean renderDialogStampa) {
		this.renderDialogStampa = renderDialogStampa;
	}
	
	/**
	 * Preview glifo firma delegato/delegante
	 * @return StreamedContent
	 */
	public StreamedContent getPreviewGlifo() {
		return previewGlifo;
	}
	
	/**
	 * Preview glifo firma delegato/delegante
	 * @param previewGlifo
	 */
	public void setPreviewGlifo(StreamedContent previewGlifo) {
		this.previewGlifo = previewGlifo;
	} 
	
	/**
	 * Restituisce il flag {@link #estrattiContoVisibile}
	 * @return estrattiContoVisibile
	 */
	public boolean isEstrattiContoVisibile() {
		return estrattiContoVisibile;
	}

	/**
	 * Imposta il flag {@link #estrattiContoVisibile}
	 * @param estrattiContoVisibile
	 */
	public void setEstrattiContoVisibile(boolean estrattiContoVisibile) {
		this.estrattiContoVisibile = estrattiContoVisibile;
	}
	
	/**
	 * Restituisce il flag {@link #contiConsuntiviVisibile}
	 * 
	 * @return flag visibilità conti consuntivi
	 */
	public boolean isContiConsuntiviVisibile() {
		return contiConsuntiviVisibile;
	}

	/**
	 * Imposta il flag {@link #contiConsuntiviVisibile}
	 * 
	 * @param contiConsuntiviVisibile
	 */
	public void setContiConsuntiviVisibile(boolean contiConsuntiviVisibile) {
		this.contiConsuntiviVisibile = contiConsuntiviVisibile;
	}

	/**
	 * Reindirizza l'utente sulla pagina di ricerca estratti conto trimestrali CCVT UCB.
	 * 
	 * @return url pagina RICERCA_ESTRATTI_CONTO_CCVT_UCB
	 */
	public final String gotoRicercaEstrattiContoUCB() {
		return gotoPage(NavigationTokenEnum.RICERCA_ESTRATTI_CONTO_CCVT_UCB);
	}
	
	/**
	 * Reindirizza l'utente sulla pagina di ricerca elenco notifiche e conti consuntivi UCB.
	 * 
	 * @return url pagina RICERCA_CONTI_CONSUNTIVI_UCB
	 */
	public final String gotoRicercaContiConsuntiviUCB() {
		return gotoPage(NavigationTokenEnum.RICERCA_CONTI_CONSUNTIVI_UCB);
	}
	
	/**
	 * Metodo che consente di aggiornare i parametri di ricerca in fase di modifica del flusso selezionato nella ricerca flussi UCB.
	 */
	public void updateFormRicercaFlussi() {
		getRicercaFormContainer().getFlussiUCB().setAgentiContabili(tipologiaDocumentoSRV.getValuesBySelector(ContoGiudizialeEnum.AGECONT.getMetadatoName(), null, utente.getIdAoo()));
		getRicercaFormContainer().getFlussiUCB().setTipologieContabili(tipologiaDocumentoSRV.getValuesBySelector(ContoGiudizialeEnum.TIPO_CONT.getMetadatoName(), null, utente.getIdAoo()));
		
	}

	@Override
	public void onOpenTree(TreeNode nodo) {
		throw new NotImplementedException("Metodo non implementato");
	}
}
