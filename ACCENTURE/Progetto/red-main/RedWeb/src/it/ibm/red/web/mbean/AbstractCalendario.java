package it.ibm.red.web.mbean;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;
import org.primefaces.model.StreamedContent;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EventoDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.RecuperoCodeDTO;
import it.ibm.red.business.dto.RicercaEventoDTO;
import it.ibm.red.business.dto.RuoloDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.EventoCalendarioEnum;
import it.ibm.red.business.enums.TipologiaNodoEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.TestiDefault;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ICalendarioFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV;
import it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV;
import it.ibm.red.business.service.facade.INodoFacadeSRV;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaFacadeSRV;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.mbean.DocumentManagerBean;
import it.ibm.red.web.dto.GoToCodaDTO;
import it.ibm.red.web.dto.ScheduleEventoDTO;
import it.ibm.red.web.enums.GoToCodaProvenienzaEnum;
import it.ibm.red.web.enums.NavigationTokenEnum;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.helper.faces.MessageSeverityEnum;

/**
 * Abstract calendario.
 */
public class AbstractCalendario extends AbstractBean {

	private static final String IMPOSSIBILE_RECUPERARE_L_ALLEGATO = "Impossibile recuperare l'allegato.";

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 465540299037127021L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AbstractCalendario.class);

	/**
	 * Protocollo http.
	 */
	private static final String HTTP = "http://";

	/**
	 * Protocollo https.
	 */
	private static final String HTTPS = "https://";

	/**
	 * Protocollo ftp.
	 */
	private static final String FTP = "ftp://";

	/**
	 * Container name di centralSectionForm:readUpdateDialog.
	 */
	private static final String READUPDATEDIALOG_CONTAINER_NAME = "centralSectionForm:readUpdateDialog";

	/**
	 * Javascript che consente di visualizzare il componente Primefaces identificato
	 * da: comNewDialogWV.
	 */
	private static final String SHOW_COMNEWDIALOGWV_JS = "PF('comNewDialogWV').show();";

	/**
	 * Container name di centralSectionForm:myscheduleWidget.
	 */
	private static final String MYSCHEDULEWIDGET_CONTAINER_NAME = "centralSectionForm:myscheduleWidget";

	/**
	 * Container name di centralSectionForm:myschedule.
	 */
	private static final String MYSCHEDULE_CONTAINER_NAME = "centralSectionForm:myschedule";

	/**
	 * Stringa vuota.
	 */
	private static final String EMPTY_STRING = "";

	/**
	 * Bean sessione.
	 */
	private SessionBean sessionBean;

	/**
	 * Informazione utente.
	 */
	private UtenteDTO utente;

	/**
	 * Servizio calendario.
	 */
	private ICalendarioFacadeSRV calSRV;

	/**
	 * Servizio documento.
	 */
	private IDocumentoRedFacadeSRV documentoRedSRV;

	/**
	 * Servizio recupero uffici.
	 */
	private INodoFacadeSRV nodiSrv;
	
	/**
	 * Documenti.
	 */
	private Collection<EventoDTO> docs;

	/**
	 * false indica che l'utente non è amministratore, true indica che utente è
	 * amministratore.
	 */
	private Boolean adminFlag;

	/**
	 * Lista testi.
	 */
	private List<TestiDefault> testiDefault;

	/**
	 * Lista tipologie evento.
	 */
	private List<EventoCalendarioEnum> tipologieEvento;

	/**
	 * Lista ruoli evento.
	 */
	private List<RuoloDTO> ruoliEvento;

	/**
	 * Lista descrizione assegnatario per competenza.
	 */
	private List<NodoOrganigrammaDTO> descrAssegnatarioPerCompetenza;

	/**
	 * Evento creato.
	 */
	private EventoDTO eventoCreazione;

	/**
	 * DTO per la ricerca.
	 */
	private RicercaEventoDTO ricercaDTO;

	/**
	 * Datatable eventi.
	 */
	private SimpleDetailDataTableHelper<EventoDTO> eventiDTH;

	/**
	 * Model per caricamento lazy.
	 */
	private transient ScheduleModel lazyEventModel;

	/**
	 * Lista eventi.
	 */
	private List<EventoCalendarioEnum> eventiList;

	/**
	 * Root tree per assegnazione per competenza.
	 */
	private DefaultTreeNode rootAssegnazionePerCompetenza;

	/**
	 * Documenti per calendario.
	 */
	private Map<Date, EventoDTO> docsForCalendar;

	/**
	 * Contatori.
	 */
	private EnumMap<DocumentQueueEnum, Integer> codeCount;

	/**
	 * Code non censite.
	 */
	private String listaCodeNonCensite;

	/**
	 * evento selezionato.
	 */
	private ScheduleEventoDTO eventSelected;

	/**
	 * File allegato.
	 */
	private FileDTO allegatoUploaded;

	/**
	 * false indica sola lettura, true indica modifica.
	 */
	private Boolean modDlg;

	/**
	 * Lista nodi selezionati.
	 */
	private List<Integer> idNodiSelezionati;

	/**
	 * Lista descrizione nodi selezionati.
	 */
	private List<String> descNodiSelezionati;

	/**
	 * Identificativo testo default.
	 */
	private Integer idTestoDefault;

	/**
	 * Mappa di tutti gli uffici esistenti per l'AOO.
	 */
	private HashMap<Integer, String> nodiAoo;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		sessionBean = (SessionBean) FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		nodiSrv = ApplicationContextProvider.getApplicationContext().getBean(INodoFacadeSRV.class);
		nodiAoo = nodiSrv.getDescrizioneByIdNodo(utente.getIdAoo().intValue());
		calSRV = ApplicationContextProvider.getApplicationContext().getBean(ICalendarioFacadeSRV.class);
		final IUtenteFacadeSRV usrSRV = ApplicationContextProvider.getApplicationContext().getBean(IUtenteFacadeSRV.class);
		documentoRedSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoRedFacadeSRV.class);
		docs = calSRV.getDocInScadenzaFromPe(utente.getId(), utente.getIdUfficio(), utente.getFcDTO());
		adminFlag = usrSRV.hasPermessoAmministratore(utente.getIdRuolo());
		updateDocsForCalendar();
		lazyEventModel = new LazyScheduleModel() {

			/**
			 * La costante serial Version UID.
			 */
			private static final long serialVersionUID = 8675560784126976296L;

			@Override
			public void loadEvents(final Date start, final Date end) {
				final Collection<EventoDTO> events = loadEventsFromDB(start, end);
				for (final Entry<Date, EventoDTO> entry : docsForCalendar.entrySet()) {
					final Date dScad = entry.getKey();
					if (dScad.after(start) && dScad.before(end)) {
						events.add(entry.getValue());
					}
				}
				setEvents(events);
			}
		};

		// caricamento dei ruli necessari per la modifica e creazione di un evento
		ruoliEvento = new ArrayList<>();

		final Long idAoo = utente.getIdAoo();
		for (final RuoloDTO ruolo : calSRV.getCategorieRuoli(idAoo)) {
			if (!"Amministratori".equals(ruolo.getDescrizione())) {
				ruoliEvento.add(ruolo);
			}
		}

		// <-- Caricamento organigramma per assegnazione struttura -->
		descrAssegnatarioPerCompetenza = new ArrayList<>();
		creaOrganigrammaAssegnatarioPerCompetenza();

		// <-- Caricamento LISTA DI EVENTI utilizzati nella creazione filtrati per
		// ruolo-->
		getTipologieFromRole();

		eventoCreazione = new EventoDTO();

		ricercaDTO = new RicercaEventoDTO();

		eventiDTH = new SimpleDetailDataTableHelper<>();

		tipologieEvento = Arrays.asList(EventoCalendarioEnum.values());

		try {
			testiDefault = calSRV.getAllTestiDefault();
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero dei test.", e);
		}
	}

	private List<EventoCalendarioEnum> getTipologieFromRole() {
		eventiList = new ArrayList<>();

		if (Boolean.TRUE.equals(getAdminFlag())) {
			eventiList.add(EventoCalendarioEnum.COMUNICAZIONE);
			eventiList.add(EventoCalendarioEnum.NEWS);
		} else {
			eventiList.add(EventoCalendarioEnum.UTENTE);
		}
		return eventiList;
	}

	/**
	 * Creazione organigramma per eventi del calendario di tipologia News e
	 * Comunicazione .
	 */
	public void creaOrganigrammaAssegnatarioPerCompetenza() {

		try {
			final IOrganigrammaFacadeSRV organigrammaSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
			// prendo da DB i nodi di primo livello
			final NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
			nodoRadice.setIdAOO(getUtente().getIdAoo());
			nodoRadice.setIdNodo(getUtente().getIdNodoRadiceAOO());
			nodoRadice.setCodiceAOO(getUtente().getCodiceAoo());
			nodoRadice.setUtentiVisible(false);
			final List<NodoOrganigrammaDTO> primoLivello = organigrammaSRV.getFigliAlberoAssegnatarioPerCompetenzaUscita(getUtente(), nodoRadice);

			rootAssegnazionePerCompetenza = new DefaultTreeNode();
			rootAssegnazionePerCompetenza.setExpanded(true);
			rootAssegnazionePerCompetenza.setSelectable(false);

			// per ogni nodo di primo livello creo il nodo da mettere nell'albero che ha
			// come padre rootAssegnazionePerCompetenza
			final List<DefaultTreeNode> listToOpen = new ArrayList<>();
			rootAssegnazionePerCompetenza.setChildren(new ArrayList<>());
			DefaultTreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(getUtente().getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, rootAssegnazionePerCompetenza);
				nodoToAdd.setExpanded(true);
				listToOpen.add(nodoToAdd);
			}
			// per ogni nodo di primo livello recupero i suoi figli (se ce li ha)
			NodoOrganigrammaDTO nodoPadre = null;
			for (final DefaultTreeNode treeNodePadre : listToOpen) {
				nodoPadre = (NodoOrganigrammaDTO) treeNodePadre.getData();
				if (nodoPadre.getFigli() == 0) {
					continue;
				}

				final List<NodoOrganigrammaDTO> figli = organigrammaSRV.getFigliAlberoCompletoNoUtentiFromRoot(getUtente().getIdUfficio(), nodoPadre);
				treeNodePadre.getChildren().clear();
				DefaultTreeNode figlioTreeNode = null;
				for (final NodoOrganigrammaDTO figlio : figli) {
					figlio.setCodiceAOO(getUtente().getCodiceAoo());
					figlioTreeNode = new DefaultTreeNode(figlio, treeNodePadre);
					if (figlio.getFigli() > 0) {
						new DefaultTreeNode(new NodoOrganigrammaDTO(), figlioTreeNode);
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	private void updateDocsForCalendar() {
		docsForCalendar = new HashMap<>();
		for (final EventoDTO doc : getDocs()) {
			if (docsForCalendar.get(doc.getDataScadenza()) == null) {
				docsForCalendar.put(doc.getDataScadenza(), doc);
			}
		}
	}

	/**
	 * Esegue la ricerca basata sul popolamento del form di ricerca, se alcune
	 * informazioni sono mancanti, verrà mostrato un messaggio di errore all'utente.
	 */
	public void ricercaEventi() {
		if (getRicercaDTO().getDataInizio() != null || getRicercaDTO().getDataScadenza() != null) {

			final Map<Integer, Collection<EventoDTO>> results = getCalSRV().ricercaEventi(getRicercaDTO().getDataInizio(), getRicercaDTO().getDataScadenza(),
					getRicercaDTO().getTitolo(), getRicercaDTO().getTipologia(), getUtente().getId(), getUtente().getIdUfficio(), getUtente().getIdRuolo(),
					getUtente().getIdAoo(), getUtente().getFcDTO());
			setDocs(results.get(1));
			updateDocsForCalendar();

			final DataTable d = getDataTableByIdComponent("eastSectionForm:tabHomeEast:eventi");
			if (d != null) {
				d.reset();
			}

			setEventiDTH(new SimpleDetailDataTableHelper<>(results.get(2)));
			for (final EventoDTO evento : getEventiDTH().getMasters()) {
				if (evento.getIdUtente() != null && evento.getIdUtente().equals(getUtente().getId())) {
					evento.setIsModificabileWdg(true);
				}
			}
		} else {
			showWarnMessage("Almeno una delle due date deve essere settata");
		}
	}

	private Collection<EventoDTO> loadEventsFromDB(final Date start, final Date end) {
		final Collection<EventoDTO> output = new ArrayList<>();
		output.addAll(
				getCalSRV().getEventiFromDb(start, end, null, null, getUtente().getId(), getUtente().getIdUfficio(), getUtente().getIdRuolo(), getUtente().getIdAoo(), true));
		return output;
	}

	private void setEvents(final Collection<EventoDTO> events) {
		if (!getSessionBean().getActivePageInfo().equals(NavigationTokenEnum.DASHBOARD)) {
			// GESTIONE CALENDARIO HOMEPAGE
			for (final EventoDTO event : events) {
				final ScheduleEvent ev = fromEventoDTO2Event(event);
				getLazyEventModel().addEvent(ev);
			}
		} else {
			// GESTIONE WIDGET CALENDARIO DASHBOARD
			final List<Date> dateTraEventi = new ArrayList<>();
			dataMultiplaASingola(events, dateTraEventi);
			for (final Date evento : dateTraEventi) {
				final EventoDTO eventoWidget = new EventoDTO();
				eventoWidget.setTitolo("");
				eventoWidget.setDataInizio(evento);
				eventoWidget.setDataScadenza(evento);
				eventoWidget.setTipologia(EventoCalendarioEnum.UTENTE);

				final ScheduleEvent ev = fromEventoDTO2Event(eventoWidget);
				getLazyEventModel().addEvent(ev);
			}
		}
	}

	/**
	 * @param eventi
	 * @param dateTraEventi
	 * @return
	 */
	public List<EventoDTO> dataMultiplaASingola(final Collection<EventoDTO> eventi, final Collection<Date> dateTraEventi) {
		final List<EventoDTO> eventiWidgetSingoli = new ArrayList<>();
		for (final EventoDTO evento : eventi) {

			// START - CALCOLO DELTA INIZIO E FINE
			final Calendar start = Calendar.getInstance();
			final Calendar end = Calendar.getInstance();
			start.setTime(evento.getDataInizio());
			end.setTime(evento.getDataScadenza());
			final Date startDate = start.getTime();
			final Date endDate = end.getTime();
			final Long startTime = startDate.getTime();
			final Long endTime = endDate.getTime();
			final Long diffTime = endTime - startTime;
			final Long diffDays = (diffTime / (1000 * 60 * 60 * 24) + 1);
			// END - CALCOLO DELTA INIZIO E FINE

			Date currentDate = startDate;
			for (int i = 0; i < diffDays; i++) {
				if (!dateTraEventi.contains(currentDate)) {
					dateTraEventi.add(currentDate);
				}
				LocalDateTime localDateTime = currentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
				localDateTime = localDateTime.plusDays(1);
				final Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
				currentDate = currentDatePlusOneDay;
			}
		}
		return eventiWidgetSingoli;
	}

	/**
	 * Esegue la logica di visualizzazione queue document.
	 * 
	 * @param event
	 */
	public void showQueueDocument(final ActionEvent event) {
		Integer count;
		final IRicercaFacadeSRV ricercaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaFacadeSRV.class);
		codeCount = new EnumMap<>(DocumentQueueEnum.class); // azzero il contenitore prima di ricalcolare le code per non creare
															// incongruenze nella view

		final EventoDTO evento = getEventiDTH().getCurrentMaster();

		final RecuperoCodeDTO dto = ricercaSRV.getQueueNameFromDocumentTitle(getUtente(), String.valueOf(evento.getDocumentTitle()), null, evento.getIdCategoriaDocumento(),
				false);
		listaCodeNonCensite = StringUtils.fromStringListToString(dto.getCodeNonCensite());
		final Collection<DocumentQueueEnum> queue = dto.getCode();
		if (queue != null && !queue.isEmpty()) {
			for (final DocumentQueueEnum coda : queue) {

				if (getSessionBean().haAccessoAllaFunzionalita(coda.getAccessFun())) {

					count = Collections.frequency(queue, coda);
					codeCount.put(coda, count);
				}
			}
		}
	}

	/**
	 * Restituisce il content dell'allegato per permetterne il download.
	 * 
	 * @return StreamedContent
	 */
	public StreamedContent downloadFromDetail() {
		StreamedContent sc = null;

		try {
			final byte[] content = getCalSRV().getAllegatoEvento(eventSelected.getIdEvento());
			final InputStream io = new ByteArrayInputStream(content);
			sc = downloadAllegato(io, eventSelected.getMimeTypeAllegato(), eventSelected.getNomeAllegato(), eventSelected.getEstensioneAllegato());
		} catch (final Exception e) {
			LOGGER.error(IMPOSSIBILE_RECUPERARE_L_ALLEGATO, e);
			FacesHelper.showMessage(null, IMPOSSIBILE_RECUPERARE_L_ALLEGATO, MessageSeverityEnum.ERROR);
		}

		return sc;
	}

	/**
	 * Restituisce il content dell'allegato per permetterne il download.
	 * 
	 * @param io
	 * @param mimeType
	 * @param nomeAllegato
	 * @param estensione
	 * @return StreamedContent
	 */
	public StreamedContent downloadAllegato(final InputStream io, final String mimeType, final String nomeAllegato, final String estensione) {
		StreamedContent sc = null;
		try {
			sc = new DefaultStreamedContent(io, mimeType, nomeAllegato + "." + estensione);
		} catch (final Exception e) {
			LOGGER.error("Errore nella conversione del file.", e);
			FacesHelper.showMessage(null, "Errore nella conversione del file.", MessageSeverityEnum.ERROR);
		}
		return sc;
	}

	/**
	 * Restituisce il content dell'allegato per permetterne il download.
	 * 
	 * @return StreamedContent
	 */
	public StreamedContent downloadFromCreazione() {
		StreamedContent sc = null;
		try {
			final InputStream io = new ByteArrayInputStream(allegatoUploaded.getContent());
			sc = downloadAllegato(io, allegatoUploaded.getMimeType(), allegatoUploaded.getFileName(), allegatoUploaded.getMimeType());
		} catch (final Exception e) {
			LOGGER.error(IMPOSSIBILE_RECUPERARE_L_ALLEGATO, e);
			FacesHelper.showMessage(null, IMPOSSIBILE_RECUPERARE_L_ALLEGATO, MessageSeverityEnum.ERROR);
		}
		return sc;
	}

	/**
	 * Permette la visualizzazione del dettaglio esteso per lo scadenzario.
	 */
	public void openDocumentPopup() {
		try {
			final EventoDTO e = getEventiDTH().getCurrentDetail();

			final DetailDocumentRedDTO detail = getDocumentoRedSRV().getDocumentDetail(e.getDocumentTitle(), getUtente(), null, null);

			final DocumentManagerBean bean = FacesHelper.getManagedBean(Constants.ManagedBeanName.DOCUMENT_MANAGER_BEAN);
			bean.setChiamante(ConstantsWeb.MBean.SCADENZIARIO_BEAN);
			bean.setDetail(detail);

			FacesHelper.executeJS("openDettaglioEstesoForScadenzario();");
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di recupero del documento.");
		}

	}

	/**
	 * Definisce le azioni collegate all'evento di selezione data.
	 * 
	 * @param selectEvent
	 */
	public void onDateSelect(final SelectEvent selectEvent) {
		final Date targetDate = (Date) selectEvent.getObject();
		setRicercaDTO(new RicercaEventoDTO());
		getRicercaDTO().setDataInizio(targetDate);
		getRicercaDTO().setDataScadenza(targetDate);
		ricercaEventi();
		setRicercaDTO(new RicercaEventoDTO());
	}

	/**
	 * Verifica che il link esterno non contenga già il protocollo, altrimenti viene
	 * aggiunto. Questo risulta necessario in quanto, viceversa, l'applicazione
	 * tenta di aprirlo nel proprio context generando un errore.
	 * 
	 * @return linkDaVerificare
	 */
	public String checkLinkEsterno() {
		final String linkDaVerificare = getEventiDTH().getCurrentDetail().getLinkEsterno();
		final StringBuilder linkDaModificare = new StringBuilder(linkDaVerificare);
		final String protocollo = linkDaModificare.substring(0, 8);
		boolean isProtocollo = false;

		if (protocollo.contains(HTTP) || protocollo.contains(HTTPS) || protocollo.contains(FTP)) {
			isProtocollo = true;
		}

		// viene verificato che il link esterno non contenga gia il protocollo
		// altrimenti lo si aggiunge
		// necessario altrimenti l'applicazione tenta di aprirlo nel proprio context e
		// va in errore
		if (!isProtocollo) {
			// di default viene impostato 'http://'
			linkDaModificare.insert(0, HTTP);
		}

		return linkDaModificare.toString();
	}

	/**
	 * Rimuove il file per cui viene effettuato l'upload.
	 */
	public void rimuoviFile() {
		setAllegatoUploaded(null);
	}

	/**
	 * Esegue le azioni necessarie alla corretta visualizzazione del contenuto di un
	 * nodo del tree in fase di "Expand".
	 * 
	 * @param event
	 */
	public void onOrgAssPerCompetenzaNodeExpand(final NodeExpandEvent event) {
		try {
			final IOrganigrammaFacadeSRV organigrammaSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
			final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) event.getTreeNode().getData();

			final List<NodoOrganigrammaDTO> figli = organigrammaSRV.getFigliAlberoCompletoNoUtenti(getUtente().getIdUfficio(), nodoExp);
			DefaultTreeNode nodoToAdd = null;
			event.getTreeNode().getChildren().clear();

			for (final NodoOrganigrammaDTO n : figli) {
				n.setCodiceAOO(getUtente().getCodiceAoo());
				if (n.getTipoNodo().equals(TipologiaNodoEnum.UTENTE)) {
					continue;
				}
				nodoToAdd = new DefaultTreeNode(n, event.getTreeNode());
				if (n.getFigli() > 0 || n.isUtentiVisible()) {
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * Gestisce l'evento "Select" legato ad un nodo del tree degli assegnatari per
	 * competenza.
	 * 
	 * @param event
	 */
	public void onOrgAssPerCompetenzaSelect(final NodeSelectEvent event) {
		final NodoOrganigrammaDTO select = (NodoOrganigrammaDTO) event.getTreeNode().getData();
		getDescrAssegnatarioPerCompetenza().add(select);
		idNodiSelezionati.add(select.getIdNodo().intValue());
		descNodiSelezionati.add(select.getDescrizioneNodo());
	}

	/**
	 * Gestisce l'eliminazione di un nodo dal tree delle assegnazioni per
	 * competenza.
	 * 
	 * @param nodo
	 */
	public void onOrgAssPerCompetenzaRemove(final NodoOrganigrammaDTO nodo) {
		getDescrAssegnatarioPerCompetenza().remove(nodo);
		getIdNodiSelezionati().remove(Integer.valueOf((nodo.getIdNodo().intValue())));
		getDescNodiSelezionati().remove(nodo.getDescrizioneNodo());
	}

	/**
	 * alla selezione di un testo default setto il contenuto della popup.
	 * 
	 * @param event testo default scelto
	 */
	public void getSelectedTestoDefault(final ValueChangeEvent event) {
		if (event.getNewValue() != null) {
			getTestiDefault().forEach(testoDefault -> {
				if (testoDefault.getIdTesto().equals(event.getNewValue())) {
					getEventSelected().setContenuto(testoDefault.getContenuto());
					getEventSelected().setTitolo(testoDefault.getTitolo());
				}
			});
		}
	}

	/**
	 * Definisce le azioni da eseguire alla selezione di un evento.
	 * 
	 * @param selectEvent
	 */
	public void onEventSelect(final SelectEvent selectEvent) {
		final ScheduleEventoDTO event = (ScheduleEventoDTO) selectEvent.getObject();
		setModDlg(false);
		setAllegatoUploaded(null);
		setIdNodiSelezionati(new ArrayList<>());
		setDescNodiSelezionati(new ArrayList<>());

		if (EventoCalendarioEnum.SCADENZA.equals(event.getTipologia())) {
			final Date targetDate = event.getDataScadenza();
			setRicercaDTO(new RicercaEventoDTO());
			getRicercaDTO().setDataInizio(targetDate);
			getRicercaDTO().setDataScadenza(targetDate);
			getRicercaDTO().setTipologia(EventoCalendarioEnum.SCADENZA);
			ricercaEventi();
			setRicercaDTO(new RicercaEventoDTO());
		} else {
			setEventSelected(event);
			if (!StringUtils.isNullOrEmpty(getEventSelected().getNomeAllegato())) {
				if (!StringUtils.isNullOrEmpty(getEventSelected().getNomeAllegato())) {
					setAllegatoUploaded(new FileDTO(getEventSelected().getNomeAllegato(),
							getCalSRV().getAllegatoEvento(getEventSelected().getIdEvento()),
							getEventSelected().getMimeTypeAllegato()));
				}
			}
			if (event.getIdUtente().equals(getUtente().getId())) {
				setModDlg(true);
			}

			getDescrAssegnatarioPerCompetenza().clear();
			
			if (!CollectionUtils.isEmpty(event.getIdNodiTarget())) {
				for (Integer idNodo : event.getIdNodiTarget()) {
					
					// Se l'ufficio è correttamente configurato viene prepopolata la struttura dei nodi target
					if (nodiAoo.containsKey(idNodo)) {
						getDescrAssegnatarioPerCompetenza().add(new NodoOrganigrammaDTO(nodiAoo.get(idNodo), idNodo, null));
					} else {
						// Se l'ufficio non è correttamente configurato viene mostrato l'evento senza prepopolamento della struttura comunicando l'errore riscontrato
						showError("Ufficio con identificativo: " + idNodo + " non configurato correttamente per questo evento.");
					}
				}
			}
			
			if (event.getDescrizioneNodi() != null) {
				for (int i = 0; i < event.getDescrizioneNodi().size(); i++) {
					getDescrAssegnatarioPerCompetenza().add(new NodoOrganigrammaDTO(event.getDescrizioneNodi().get(i), event.getIdNodiTarget().get(i), null));
				}
			} else {
				getEventSelected().setDescrizioneNodi(new ArrayList<>());
			}

			if (getSessionBean().getActivePageInfo().equals(NavigationTokenEnum.DASHBOARD)) {
				FacesHelper.update(READUPDATEDIALOG_CONTAINER_NAME);
				FacesHelper.executeJS(SHOW_COMNEWDIALOGWV_JS);
			} else {
				FacesHelper.update("eastSectionForm:readUpdateDialog");
				FacesHelper.executeJS(SHOW_COMNEWDIALOGWV_JS);
			}
		}

	}

	/**
	 * Gestisce la funzionalità di spostamento su coda.
	 * 
	 * @param coda da raggiungere.
	 * @return url della pagina da mostrare
	 */
	public String goToCodaDocInScadenza(final DocumentQueueEnum coda) {
		final List<MasterDocumentRedDTO> mastersInScadenza = new ArrayList<>();

		final Map<String, List<String>> docInScadenzaMap = getSessionBean().getDocInScadenzaMap();
		if (!CollectionUtils.isEmpty(docInScadenzaMap.get(coda.getName()))) {
			final List<String> documentTitlesInScadenza = docInScadenzaMap.get(coda.getName());

			MasterDocumentRedDTO masterInScadenza = null;
			for (final String documentTitleInScadenza : documentTitlesInScadenza) {
				masterInScadenza = new MasterDocumentRedDTO();
				masterInScadenza.setDocumentTitle(documentTitleInScadenza);

				mastersInScadenza.add(masterInScadenza);
			}
		}

		final GoToCodaDTO goToCodaInfo = new GoToCodaDTO(mastersInScadenza, GoToCodaProvenienzaEnum.HOME_PAGE_DOC_IN_SCADENZA);

		FacesHelper.putObjectInSession(ConstantsWeb.SessionObject.FILTER_MASTER_GO_TO_CODA, goToCodaInfo);
		return getSessionBean().gotoPage(NavigationTokenEnum.get(coda));
	}

	/**
	 * Gestisce la funzionalità di spostamento su coda in spedizione doc in errore.
	 * 
	 * @return url della pagina da mostrare
	 */
	public String goToCodaInSpedizioneDocInErrore() {
		final IListaDocumentiFacadeSRV listaDocSRV = ApplicationContextProvider.getApplicationContext().getBean(IListaDocumentiFacadeSRV.class);
		final List<MasterDocumentRedDTO> mastersInErroreCodaSpedizione = new ArrayList<>();

		final List<String> documentTitlesInErroreCodaSpedizione = listaDocSRV.getDocInErroreCodaSpedizione(getUtente());

		if (!CollectionUtils.isEmpty(documentTitlesInErroreCodaSpedizione)) {
			MasterDocumentRedDTO masterInErroreCodaSpedizione = null;

			for (final String documentTitleInErroreCodaSpedizione : documentTitlesInErroreCodaSpedizione) {
				masterInErroreCodaSpedizione = new MasterDocumentRedDTO();
				masterInErroreCodaSpedizione.setDocumentTitle(documentTitleInErroreCodaSpedizione);

				mastersInErroreCodaSpedizione.add(masterInErroreCodaSpedizione);
			}
		}

		final GoToCodaDTO goToCodaInfo = new GoToCodaDTO(mastersInErroreCodaSpedizione, GoToCodaProvenienzaEnum.HOME_PAGE_DOC_SPEDIZIONE_IN_ERRORE);

		FacesHelper.putObjectInSession(ConstantsWeb.SessionObject.FILTER_MASTER_GO_TO_CODA, goToCodaInfo);
		return getSessionBean().gotoPage(NavigationTokenEnum.get(DocumentQueueEnum.SPEDIZIONE));
	}

	/**
	 * Esegue il reset del dto di ricerca.
	 */
	public void clearFiltriEventi() {
		setRicercaDTO(new RicercaEventoDTO());
	}

	/**
	 * Permette di visualizzare una coda specificata.
	 * 
	 * @param q la coda da raggiungere
	 * @return url della pagina da raggiungere
	 */
	public final String goToCoda(final DocumentQueueEnum q) {

		final MasterDocumentRedDTO master = new MasterDocumentRedDTO();
		master.setDocumentTitle(getEventiDTH().getCurrentMaster().getDocumentTitle());
		master.setNumeroDocumento(getEventiDTH().getCurrentMaster().getNumeroDocumento());

		final GoToCodaDTO goToCodaInfo = new GoToCodaDTO(master);
		FacesHelper.putObjectInSession(ConstantsWeb.SessionObject.FILTER_MASTER_GO_TO_CODA, goToCodaInfo);

		return getSessionBean().gotoPage(NavigationTokenEnum.get(q));
	}

	/**
	 * Effettua controlli sul nome file e successivamente esegue l'upload del file.
	 * Gestisce eventuali errori mostrandoli all'utente.
	 * 
	 * @param event
	 */
	public void uploadAllegato(final FileUploadEvent inEvent) {
		FileUploadEvent event = inEvent;
		try {
			event = checkFileName(event);
			if (event == null) {
				return;
			}
			setAllegatoUploaded(new FileDTO(event.getFile().getFileName(), event.getFile().getContents(), event.getFile().getContentType()));
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore con il file in fase di Upload", e);
			showError("Si è verificato un errore con il file in fase di Upload");
		}
	}

	/**
	 * alla selezione di un testo default setto il contenuto della popup.
	 * 
	 * @param event testo default scelto
	 */
	public void getSelectedTestoDefaultCreaazione(final ValueChangeEvent event) {
		if (event.getNewValue() != null) {
			getTestiDefault().forEach(testoDefault -> {
				if (testoDefault.getIdTesto().equals(event.getNewValue())) {
					getEventoCreazione().setContenuto(testoDefault.getContenuto());
					getEventoCreazione().setTitolo(testoDefault.getTitolo());
				}
			});
		} else {
			getEventoCreazione().setContenuto(EMPTY_STRING);
			getEventoCreazione().setTitolo(EMPTY_STRING);
		}
	}

	/**
	 * Gestisce la logica legata alla creazione di un Evento.
	 */
	public void onCreateEvent() {
		setEventoCreazione(new EventoDTO());
		getEventoCreazione().setIdNodiTarget(new ArrayList<>());
		getEventoCreazione().setDescrizioneNodi(new ArrayList<>());
		setDescrAssegnatarioPerCompetenza(new ArrayList<>());
		setIdNodiSelezionati(new ArrayList<>());
		setDescNodiSelezionati(new ArrayList<>());
		setAllegatoUploaded(null);
		if (Boolean.TRUE.equals(getAdminFlag())) {
			getEventoCreazione().setTipologia(EventoCalendarioEnum.COMUNICAZIONE);
		} else {
			getEventoCreazione().setTipologia(EventoCalendarioEnum.UTENTE);
		}
		setIdTestoDefault(null);
	}

	/**
	 * Creazione di un evento sul calendario.
	 */
	public void createEvent() {
		boolean isCreated = false;
		boolean okField = true;
		boolean okDate = true;

		if (getEventoCreazione().getTitolo() == null || "".equals(getEventoCreazione().getTitolo()) || getEventoCreazione().getDataScadenza() == null
				|| getEventoCreazione().getDataInizio() == null || "".equals(getEventoCreazione().getContenuto()) || getEventoCreazione().getContenuto() == null) {
			showError("Attenzione! Popolare i campi obbligatori (*)");
			okField = false;
		}
		if (getEventoCreazione().getDataScadenza() != null && getEventoCreazione().getDataInizio() != null
				&& getEventoCreazione().getDataInizio().after(getEventoCreazione().getDataScadenza())) {
			showError("Attenzione! La data inizio deve essere precedente o uguale alla data fine.");
			okDate = false;
		}

		if (okField && okDate) {
			getEventoCreazione().setIdUtente(getUtente().getId());
			getEventoCreazione().setIdRuolo(getUtente().getIdRuolo());
			getEventoCreazione().setIdAoo(getUtente().getIdAoo());
			getEventoCreazione().setIdNodo(getUtente().getIdUfficio());
			getEventoCreazione().setDescrizioneNodi(getDescNodiSelezionati());
			getEventoCreazione().setIdNodiTarget(getIdNodiSelezionati());
			if (getAllegatoUploaded() != null && getAllegatoUploaded().getContent() != null) {
				getEventoCreazione().setAllegato(getAllegatoUploaded().getContent());
				getEventoCreazione().setNomeAllegato(getAllegatoUploaded().getFileName());
				getEventoCreazione().setMimeTypeAllegato(getAllegatoUploaded().getMimeType());
				final Optional<String> extOpt = Optional.ofNullable(getAllegatoUploaded().getFileName()).filter(f -> f.contains("."))
						.map(f -> f.substring(getAllegatoUploaded().getFileName().lastIndexOf(".") + 1));
				String ext = "";
				if (extOpt.isPresent()) {
					ext = extOpt.get();
				}
				getEventoCreazione().setEstensioneAllegato(ext);
			}
			try {
				isCreated = getCalSRV().creaEvento(getEventoCreazione());
			} catch (final Exception e) {
				LOGGER.error(e.getMessage(), e);
				showError(e.getMessage());
			}
		}

		if (isCreated) {
			final ScheduleEventoDTO scheduleEvent = new ScheduleEventoDTO(getEventoCreazione());
			getLazyEventModel().addEvent(scheduleEvent);
			showInfoMessage("Evento creato con successo");
			FacesHelper.executeJS("PF('wdgCreationDialog').hide()");
			if (getSessionBean().getActivePageInfo().equals(NavigationTokenEnum.DASHBOARD)) {
				FacesHelper.update(MYSCHEDULEWIDGET_CONTAINER_NAME);
			} else {
				FacesHelper.update(MYSCHEDULE_CONTAINER_NAME);
			}
		}
	}

	/**
	 * Metodo per la modifica di un evento sul calendario.
	 */
	public void modificaevento() {
		boolean isModified = false;
		boolean okField = true;
		boolean okDate = true;
		EventoDTO event = new EventoDTO();

		if (getEventSelected().getTitolo() == null || "".equals(getEventSelected().getTitolo()) || getEventSelected().getDataScadenza() == null
				|| getEventSelected().getDataInizio() == null || "".equals(getEventSelected().getContenuto()) || getEventSelected().getContenuto() == null) {
			showError("Attenzione! Popolare i campi obbligatori (*)");
			okField = false;
		}

		if (getEventSelected().getDataScadenza() != null && getEventSelected().getDataInizio() != null
				&& getEventSelected().getDataInizio().after(getEventSelected().getDataScadenza())) {
			showError("Attenzione! La data inizio deve essere precedente o uguale alla data fine.");
			okDate = false;
		}

		if (okField && okDate) {
			event = getEventSelected().trasformToEventoDTO();
			event.setIdRuolo(getUtente().getIdRuolo());
			event.setIdAoo(getUtente().getIdAoo());
			event.setIdNodo(getUtente().getIdUfficio());
			event.setDescrizioneNodi(getDescNodiSelezionati());
			event.setIdNodiTarget(getIdNodiSelezionati());

			if (getAllegatoUploaded() != null && getAllegatoUploaded().getContent() != null) {
				event.setAllegato(getAllegatoUploaded().getContent());
				event.setNomeAllegato(getAllegatoUploaded().getFileName());
				event.setMimeTypeAllegato(getAllegatoUploaded().getMimeType());
				final Optional<String> extOpt = Optional.ofNullable(getAllegatoUploaded().getFileName()).filter(f -> f.contains("."))
						.map(f -> f.substring(getAllegatoUploaded().getFileName().lastIndexOf(".") + 1));
				String ext = "";
				if (extOpt.isPresent()) {
					ext = extOpt.get();
				}
				event.setEstensioneAllegato(ext);
			}

			try {
				isModified = getCalSRV().modificaEvento(event);
			} catch (final Exception e) {
				LOGGER.error(e.getMessage(), e);
				showError(e.getMessage());
			}
		}

		if (isModified) {
			final ScheduleEventoDTO scheduleEvent = new ScheduleEventoDTO(event);
			getLazyEventModel().updateEvent(scheduleEvent);
			showInfoMessage("Evento modificato con successo");
			FacesHelper.executeJS("PF('comNewDialogWV').hide()");

			if (getSessionBean().getActivePageInfo().equals(NavigationTokenEnum.DASHBOARD)) {
				FacesHelper.update(MYSCHEDULEWIDGET_CONTAINER_NAME);
			} else {
				FacesHelper.update(MYSCHEDULE_CONTAINER_NAME);
			}
		}
	}

	/**
	 * Metodo per la cancellazione di un evento sul calendario.
	 */
	public void eliminaEvento() {
		boolean isDeleted = false;
		try {
			isDeleted = getCalSRV().cancellaEvento(getEventSelected().getIdEvento());
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e.getMessage());
		}

		if (isDeleted) {
			getLazyEventModel().updateEvent(getEventSelected());
			showInfoMessage("Evento eliminato con successo");
			FacesHelper.executeJS("PF('comNewDialogWV').hide()");

			if (getSessionBean().getActivePageInfo().equals(NavigationTokenEnum.DASHBOARD)) {
				FacesHelper.update(MYSCHEDULEWIDGET_CONTAINER_NAME);
			} else {
				FacesHelper.update(MYSCHEDULE_CONTAINER_NAME);
			}
		}
	}

	private ScheduleEvent fromEventoDTO2Event(final EventoDTO evento) {
		return new ScheduleEventoDTO(evento);
	}

	/**
	 * Restituisce il session bean.
	 * @return session bean
	 */
	public SessionBean getSessionBean() {
		return sessionBean;
	}

	/**
	 * Imposta il bean che gestisce la sessione.
	 * @param sessionBean
	 */
	public void setSessionBean(final SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	/**
	 * Restituisce l'utente.
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'utente.
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Restituisce il service per la gestione del calendar.
	 * @return service gestione calendar
	 */
	public ICalendarioFacadeSRV getCalSRV() {
		return calSRV;
	}

	/**
	 * Imposta il service per la gestione del calendar.
	 * @param calSRV
	 */
	public void setCalSRV(final ICalendarioFacadeSRV calSRV) {
		this.calSRV = calSRV;
	}

	/**
	 * Restituisce il service per la gestione dei documenti RED.
	 * @return service documenti RED.
	 */
	public IDocumentoRedFacadeSRV getDocumentoRedSRV() {
		return documentoRedSRV;
	}

	/**
	 * Imposta il service per la gestione dei documenti RED.
	 * @param documentoRedSRV
	 */
	public void setDocumentoRedSRV(final IDocumentoRedFacadeSRV documentoRedSRV) {
		this.documentoRedSRV = documentoRedSRV;
	}

	/**
	 * Restituisce la lista degli eventi.
	 * @return lista eventi
	 */
	public Collection<EventoDTO> getDocs() {
		return docs;
	}

	/**
	 * Imposta la lista degli eventi.
	 * @param docs
	 */
	public void setDocs(final Collection<EventoDTO> docs) {
		this.docs = docs;
	}

	/**
	 * Restituisce il flag legato all'admin.
	 * 
	 * @return true se admin, false altrimenti
	 */
	public Boolean getAdminFlag() {
		return adminFlag;
	}

	/**
	 * @return the testiDefault
	 */
	public final List<TestiDefault> getTestiDefault() {
		return testiDefault;
	}

	/**
	 * @param testiDefault the testiDefault to set
	 */
	public final void setTestiDefault(final List<TestiDefault> testiDefault) {
		this.testiDefault = testiDefault;
	}

	/**
	 * Restituisce la lista delle tipologie eventi.
	 * 
	 * @return tipologieEvento
	 */
	public List<EventoCalendarioEnum> getTipologieEvento() {
		return tipologieEvento;
	}

	/**
	 * @return the ruoliEvento
	 */
	public final List<RuoloDTO> getRuoliEvento() {
		return ruoliEvento;
	}

	/**
	 * @param ruoliEvento the ruoliEvento to set
	 */
	public final void setRuoliEvento(final List<RuoloDTO> ruoliEvento) {
		this.ruoliEvento = ruoliEvento;
	}

	/**
	 * @return the descrAssegnatarioPerCompetenza
	 */
	public final List<NodoOrganigrammaDTO> getDescrAssegnatarioPerCompetenza() {
		return descrAssegnatarioPerCompetenza;
	}

	/**
	 * @param descrAssegnatarioPerCompetenza the descrAssegnatarioPerCompetenza to
	 *                                       set
	 */
	public final void setDescrAssegnatarioPerCompetenza(final List<NodoOrganigrammaDTO> descrAssegnatarioPerCompetenza) {
		this.descrAssegnatarioPerCompetenza = descrAssegnatarioPerCompetenza;
	}

	/**
	 * @return the eventoCreazione
	 */
	public final EventoDTO getEventoCreazione() {
		return eventoCreazione;
	}

	/**
	 * @param eventoCreazione the eventoCreazione to set
	 */
	public final void setEventoCreazione(final EventoDTO eventoCreazione) {
		this.eventoCreazione = eventoCreazione;
	}

	/**
	 * Restituisce il dto legato alla funzione di ricerca.
	 * 
	 * @return ricercaDTO associato agli eventi
	 */
	public RicercaEventoDTO getRicercaDTO() {
		return ricercaDTO;
	}

	/**
	 * Imposta il DTO di ricerca.
	 * @param ricercaDTO
	 */
	public void setRicercaDTO(final RicercaEventoDTO ricercaDTO) {
		this.ricercaDTO = ricercaDTO;
	}

	/**
	 * Restutuisce l'helper del datatable degli eventi.
	 * 
	 * @return eventiDTH
	 */
	public SimpleDetailDataTableHelper<EventoDTO> getEventiDTH() {
		return eventiDTH;
	}

	/**
	 * Imposta il datatable helper per il datatable degli eventi.
	 * @param eventiDTH
	 */
	public void setEventiDTH(final SimpleDetailDataTableHelper<EventoDTO> eventiDTH) {
		this.eventiDTH = eventiDTH;
	}

	/**
	 * @return lazyEventModel
	 */
	public ScheduleModel getLazyEventModel() {
		return lazyEventModel;
	}

	/**
	 * @return the eventiList
	 */
	public final List<EventoCalendarioEnum> getEventiList() {
		return eventiList;
	}

	/**
	 * @param eventiList the eventiList to set
	 */
	public final void setEventiList(final List<EventoCalendarioEnum> eventiList) {
		this.eventiList = eventiList;
	}

	/**
	 * @return the rootAssegnazionePerCompetenza
	 */
	public final DefaultTreeNode getRootAssegnazionePerCompetenza() {
		return rootAssegnazionePerCompetenza;
	}

	/**
	 * @param rootAssegnazionePerCompetenza the rootAssegnazionePerCompetenza to set
	 */
	public final void setRootAssegnazionePerCompetenza(final DefaultTreeNode rootAssegnazionePerCompetenza) {
		this.rootAssegnazionePerCompetenza = rootAssegnazionePerCompetenza;
	}

	/**
	 * Restituisce i contatori delle code.
	 * 
	 * @return EnumMap che associa le code ai contatori
	 */
	public Map<DocumentQueueEnum, Integer> getCodeCount() {
		return codeCount;
	}

	/**
	 * Restituisce una lista di code non censite come String.
	 * 
	 * @return listaCodeNonCensite
	 */
	public String getListaCodeNonCensite() {
		return listaCodeNonCensite;
	}

	/**
	 * Restituisce l'evento selezionato.
	 * 
	 * @return eventSelected
	 */
	public ScheduleEventoDTO getEventSelected() {
		return eventSelected;
	}

	/**
	 * Restituisce l'evento selezionato.
	 * 
	 */
	public void setEventSelected(final ScheduleEventoDTO eventSelected) {
		this.eventSelected = eventSelected;
	}

	/**
	 * @return the allegatoUploaded
	 */
	public final FileDTO getAllegatoUploaded() {
		return allegatoUploaded;
	}

	/**
	 * @param allegatoUploaded the allegatoUploaded to set
	 */
	public final void setAllegatoUploaded(final FileDTO allegatoUploaded) {
		this.allegatoUploaded = allegatoUploaded;
	}

	/**
	 * Restituisce il flag modDlg.
	 * 
	 * @return modDlg
	 */
	public Boolean getModDlg() {
		return modDlg;
	}

	/**
	 * Restituisce il flag modDlg.
	 * 
	 */
	public void setModDlg(final Boolean modDlg) {
		this.modDlg = modDlg;
	}

	/**
	 * Restituisce la lista dei nodi selezionati.
	 * @return lista nodi selezionati
	 */
	public List<Integer> getIdNodiSelezionati() {
		return idNodiSelezionati;
	}

	/**
	 * Imposta la lista dei nodi selezionati.
	 * @param idNodiSelezionati
	 */
	public void setIdNodiSelezionati(final List<Integer> idNodiSelezionati) {
		this.idNodiSelezionati = idNodiSelezionati;
	}

	/**
	 * Restituisce le descrizioni dei nodi selezionato.
	 * @return descrizioni nodi selezionati
	 */
	public List<String> getDescNodiSelezionati() {
		return descNodiSelezionati;
	}

	/**
	 * Imposta le descrizioni dei nodi selezionati.
	 * @param descNodiSelezionati
	 */
	public void setDescNodiSelezionati(final List<String> descNodiSelezionati) {
		this.descNodiSelezionati = descNodiSelezionati;
	}

	/**
	 * @return the idTestoDefault
	 */
	public final Integer getIdTestoDefault() {
		return idTestoDefault;
	}

	/**
	 * @param idTestoDefault the idTestoDefault to set
	 */
	public final void setIdTestoDefault(final Integer idTestoDefault) {
		this.idTestoDefault = idTestoDefault;
	}

}
