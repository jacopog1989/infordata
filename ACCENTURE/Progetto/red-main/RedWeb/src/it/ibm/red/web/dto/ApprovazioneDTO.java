package it.ibm.red.web.dto;

import java.util.Date;

import it.ibm.red.business.utils.StringUtils;

/**
 * Dto per la gestione delle approvazioni.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public class ApprovazioneDTO extends AbstractDTO {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Id approvazione.
	 */
	private Integer idApprovazione;

	/**
	 * Data approvazione.
	 */
	private Date dataApprovazione;
	
	/**
	 * Descrizione approvazione.
	 */
	private String descApprovazione;

	/**
	 * Descrizione utente.
	 */
	private String descUtente;

	/**
	 * Descrizione ufficio.
	 */
	private String descUfficio;
	
	/**
	 * Motivazione.
	 */
	private String motivazione;

	/**
	 * Costruttore.
	 * 
	 * @param inIdApprovazione		identificativo approvazione
	 * @param inDataApprovazione	data approvazione
	 * @param inDescApprovazione	descrizione approvazione
	 * @param inMotivazione			motivazione
	 * @param inDescUtente			descrizione utente
	 * @param inDescUfficio			descrizione ufficio
	 */
	public ApprovazioneDTO(final Integer inIdApprovazione, final Date inDataApprovazione, final String inDescApprovazione, final String inMotivazione, 
			final String inDescUtente, final String inDescUfficio) {
		idApprovazione = inIdApprovazione;
		dataApprovazione = inDataApprovazione;
		descApprovazione = inDescApprovazione;
		descUtente = inDescUtente;
		descUfficio = inDescUfficio;
		motivazione = inMotivazione;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	identificativo approvazione
	 */
	public Integer getIdApprovazione() {
		return idApprovazione;
	}

	/**
	 * Getter.
	 * 
	 * @return	data approvazione
	 */
	public Date getDataApprovazione() {
		return dataApprovazione;
	}

	/**
	 * Getter.
	 * 
	 * @return	descrizione approvazione
	 */
	public String getDescApprovazione() {
		String output = descApprovazione;
		if (!StringUtils.isNullOrEmpty(motivazione)) {
			output = motivazione + ": " + descApprovazione;
		}
		return output;
	}

	/**
	 * Getter.
	 * 
	 * @return	descrizione utente
	 */
	public String getDescUtente() {
		return descUtente;
	}

	/**
	 * Getter.
	 * 
	 * @return	descrizione ufficio
	 */
	public String getDescUfficio() {
		return descUfficio;
	}

}
