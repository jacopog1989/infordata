package it.ibm.red.web.mbean;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.google.common.net.MediaType;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IFlussoSipadFacadeSRV;
import it.ibm.red.business.service.facade.IContributoFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.constants.ConstantsWeb.SessionObject;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.servlets.DownloadContentServlet.DocumentTypeEnum;

/**
 * Component visualizzazione dettaglio documento.
 */
public class VisualizzaDettaglioDocumentoComponent extends VisualizzaDocumentoComponentBase {

	private static final String BOUNDARY_TEXT_STRING = "----boundary_text_string\r\n";

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -5450370709234197857L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(VisualizzaDettaglioDocumentoComponent.class);
	
	/**
	 * Tabella allegati.
	 */
	private SimpleDetailDataTableHelper<AllegatoDTO> allegatiDTH;

	/**
	 * Tabella contributi.
	 */
	private SimpleDetailDataTableHelper<ContributoDTO> contributiDTH;

	/**
	 * info utente.
	 */
	private final UtenteDTO utente;

	/**
	 * Informazioni file pdf generato, associato al flusso SIPAD.
	 */
	private FileDTO previewSipad;

	/**
	 * Content dell'xslt per la trasformazione SIPAD.
	 */
	private byte[] xsltForTransformation;

	
	
	/**
	 * Costruttore di default.
	 * 
	 * @param inUtente
	 */
	public VisualizzaDettaglioDocumentoComponent(final UtenteDTO inUtente) {
		super(inUtente);
		this.allegatiDTH = new SimpleDetailDataTableHelper<>();
		this.contributiDTH = new SimpleDetailDataTableHelper<>();
		this.utente = inUtente;
	}

	/**
	 * Imposta il dettaglio aggiornando i datatable helper e di conseguenza i
	 * datatable.
	 * 
	 * @param dettaglio
	 *            dettagli da impostare
	 */
	@Override
	public void setDetail(final DetailDocumentRedDTO dettaglio) {
		docpDTH.setMasters(new ArrayList<>());
		docpDTH.getMasters().add(dettaglio);
		previewComponent.setDetailPreview(dettaglio.getDocumentTitle());
		
		allegatiDTH.setMasters(dettaglio.getAllegati());
		contributiDTH.setMasters(gestisciNoteContributi(dettaglio.getContributi()));
		docpDTH.setCurrentMaster(dettaglio);
		
		setPreviewVisibile(false);
	}

	/**
	 * Effettua l'unset del dettaglio.
	 */
	@Override
	public void unsetDetail() {
		docpDTH.setMasters(null);
		allegatiDTH.setMasters(null);
		contributiDTH.setMasters(null);
		previewComponent.unsetDetail();
	}

	/**
	 * Gestisce l'evento di selezione riga per ogni riga del datatable dei documenti
	 * principali.
	 * 
	 * @param se
	 *            evento di selezione primefaces
	 */
	@Override
	public void rowSelectorDocPrincipale(final SelectEvent se) {
		handleEmptyRow();

		docpDTH.rowSelector(se);
		final DetailDocumentRedDTO dettaglio = docpDTH.getCurrentMaster();

		previewComponent.setDetailPreview(dettaglio.getDocumentTitle());
		setPreviewVisibile(false);
	}

	/**
	 * Handler di selezione riga DT allegati.
	 * 
	 * @param se
	 */
	public void rowSelectorAllegati(final SelectEvent se) {
		handleEmptyRow();

		allegatiDTH.rowSelector(se);
		final AllegatoDTO allegato = allegatiDTH.getCurrentMaster();
		final String pdfDocumentTile = (MediaType.PDF.toString().equalsIgnoreCase(allegato.getMimeType()) || 
				ContentType.P7M_X_MIME.equalsIgnoreCase(allegato.getMimeType()) ||
				ContentType.P7M_MIME.equalsIgnoreCase(allegato.getMimeType()) ||
				ContentType.P7M.equalsIgnoreCase(allegato.getMimeType()))? allegato.getDocumentTitle() : "";
		
		previewComponent.setDetailPreview(pdfDocumentTile, DocumentTypeEnum.ALLEGATO);
		setPreviewVisibile(false);
	}

	/**
	 * Handler di selezione riga DT contributi.
	 * 
	 * @param se
	 */
	public void rowSelectorContributi(final SelectEvent se) {
		handleEmptyRow();

		contributiDTH.rowSelector(se);
		final ContributoDTO contributo = contributiDTH.getCurrentMaster();

		String contributoDocumentTitle;
		DocumentTypeEnum tipoDoc;

		if (!StringUtils.isNullOrEmpty(contributo.getDocumentTitle())) {
			// Contributo da contenuto filenet
			contributoDocumentTitle = contributo.getDocumentTitle();
			tipoDoc = DocumentTypeEnum.CONTRIBUTO;
		} else {
			// Contributo da nota
			tipoDoc = DocumentTypeEnum.CONTRIBUTO_INTERNO_OTF;
			if (Boolean.TRUE.equals(contributo.getFlagEsterno())) {
				tipoDoc = DocumentTypeEnum.CONTRIBUTO_ESTERNO_OTF;
			}
			contributoDocumentTitle = contributo.getIdContributo().toString();
		}
		// Questo passaggio serve a impostare anteprima non disponibile nel caso il
		// documento non sia un pdf
		if (!MediaType.PDF.toString().equalsIgnoreCase(contributo.getMimeType())) {
			contributoDocumentTitle = "";
		}
		previewComponent.setDetailPreview(contributoDocumentTitle, tipoDoc);
	}

	/**
	 * Metodo per deselezionare la riga della tabella non più attiva.
	 */
	@Override
	protected void handleEmptyRow() {
		super.handleEmptyRow();
		allegatiDTH.setCurrentMaster(null);
	}

	/**
	 * Costruisce l'email a partire dall'allegato selezionato per l'invio o il
	 * download.
	 * 
	 * @param index
	 * @return content
	 */
	public StreamedContent inviaDocEmailAllegato(final Object index) {
		StreamedContent output = null;
		StringBuilder sb = null;
		try {
			final AllegatoDTO docALL = ((List<AllegatoDTO>)allegatiDTH.getMasters()).get((Integer) index);
			final IDocumentoFacadeSRV documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);
			final FileDTO doc = documentoSRV.getContentAllegato(docALL.getDocumentTitle(), utente.getFcDTO(), utente.getIdAoo());
			
			if (doc != null) {
				final byte[] contentDoc = doc.getContent();
				final String mimeType = doc.getMimeType();
				final String contentString = Base64.getEncoder().encodeToString(contentDoc);

				sb = new StringBuilder();
				sb.append("data:message/rfc822 eml;charset=utf-8,\r\n");
				sb.append("To: \r\n");
				sb.append("Subject: \r\n");
				sb.append("X-Unsent: 1\r\n");
				sb.append("Content-Type: multipart/mixed; boundary=--boundary_text_string\r\n");
				sb.append("\r\n");
				sb.append(BOUNDARY_TEXT_STRING);
				sb.append("Content-Type: text/html; charset=UTF-8\r\n");
				sb.append("\r\n");
				sb.append("\r\n");
				sb.append(BOUNDARY_TEXT_STRING);
				sb.append("Content-Type: ").append(mimeType).append("; name=").append(doc.getFileName()).append("\r\n");
				sb.append("Content-Transfer-Encoding: base64\r\n");
				sb.append("Content-Disposition: attachment\r\n");
				sb.append("\r\n");
				sb.append(contentString).append("\r\n");
				sb.append("\r\n");

				final InputStream targetStream = new ByteArrayInputStream(sb.toString().getBytes());
				output = new DefaultStreamedContent(targetStream, MediaType.PDF.toString(), doc.getFileName() + ".eml");
			} else {
				throw new RedException("Non è possibile inviare il documento per email");
			}
		} catch (final Exception e) {
			throw new RedException(e);
		}
		return output;
	}

	/**
	 * Recupera il content dell'allegato selezionato e lo mette in sessione per la
	 * stampa.
	 * 
	 * @param index
	 */
	public void openStampaDocumentoAllegato(final Object index) {
		final SessionBean sessionBean = FacesHelper.getManagedBean(MBean.SESSION_BEAN);
		final AllegatoDTO docALL = ((List<AllegatoDTO>)allegatiDTH.getMasters()).get((Integer) index);
		final IDocumentoFacadeSRV documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);
		final FileDTO doc = documentoSRV.getContentAllegato(docALL.getDocumentTitle(), utente.getFcDTO(), utente.getIdAoo());
		sessionBean.setRenderDialogStampa(true);
		FacesHelper.update("eastSectionForm:dlgStampaDocId");

		final byte[] newContent = PdfHelper.convertToPngPDFAndPDFtoPng(doc.getContent());
		final FileDTO docConvertito = new FileDTO(doc.getFileName(), newContent, doc.getMimeType());
		if (doc.getContent() != null) {
			FacesHelper.putObjectInSession(SessionObject.PRINT_FILE_SERVLET, docConvertito);
		}
	}

	/**
	 * Costruisce l'email a partire dal contributo selezionato per l'invio o il
	 * download.
	 * 
	 * @param index
	 * @return content
	 */
	public StreamedContent inviaDocEmailContributo(final Object index) {
		StreamedContent output = null;
		StringBuilder sb = null;
		try {
			final ContributoDTO docCont = ((List<ContributoDTO>) contributiDTH.getMasters()).get((Integer) index);

			final IDocumentoFacadeSRV documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);
			final FileDTO doc = documentoSRV.getContributoContentPreview(utente.getFcDTO(), docCont.getDocumentTitle(), utente.getIdAoo());
			if (doc != null) {
				final byte[] contentDoc = doc.getContent();
				final String mimeType = doc.getMimeType();
				final String contentString = Base64.getEncoder().encodeToString(contentDoc);

				sb = new StringBuilder();
				sb.append("data:message/rfc822 eml;charset=utf-8,\r\n");
				sb.append("To: \r\n");
				sb.append("Subject: \r\n");
				sb.append("X-Unsent: 1\r\n");
				sb.append("Content-Type: multipart/mixed; boundary=--boundary_text_string\r\n");
				sb.append("\r\n");
				sb.append(BOUNDARY_TEXT_STRING);
				sb.append("Content-Type: text/html; charset=UTF-8\r\n");
				sb.append("\r\n");
				sb.append("\r\n");
				sb.append(BOUNDARY_TEXT_STRING);
				sb.append("Content-Type: ").append(mimeType).append("; name=").append(doc.getFileName()).append("\r\n");
				sb.append("Content-Transfer-Encoding: base64\r\n");
				sb.append("Content-Disposition: attachment\r\n");
				sb.append("\r\n");
				sb.append(contentString).append("\r\n");
				sb.append("\r\n");

				final InputStream targetStream = new ByteArrayInputStream(sb.toString().getBytes());
				output = new DefaultStreamedContent(targetStream, MediaType.PDF.toString(), doc.getFileName() + ".eml");
			} else {
				throw new RedException("Non è possibile inviare il documento per email");
			}
		} catch (final Exception e) {
			throw new RedException(e);
		}
		return output;
	}

	/**
	 * Recupera il content del contributo selezionato e lo mette in sessione per la
	 * stampa.
	 * 
	 * @param index
	 */
	public void openStampaDocumentoContributo(final Object index) {
		final SessionBean sessionBean = FacesHelper.getManagedBean(MBean.SESSION_BEAN);
		final ContributoDTO docCont = ((List<ContributoDTO>) contributiDTH.getMasters()).get((Integer) index);
		final IDocumentoFacadeSRV documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);
		final FileDTO doc = documentoSRV.getContributoContentPreview(utente.getFcDTO(), docCont.getDocumentTitle(), utente.getIdAoo());
		sessionBean.setRenderDialogStampa(true);
		FacesHelper.update("eastSectionForm:dlgStampaDocId");

		final byte[] newContent = PdfHelper.convertToPngPDFAndPDFtoPng(doc.getContent());
		final FileDTO docConvertito = new FileDTO(doc.getFileName(), newContent, doc.getMimeType());
		if (doc.getContent() != null) {
			FacesHelper.putObjectInSession(SessionObject.PRINT_FILE_SERVLET, docConvertito);
		}
	}

	/**
	 * Un contributo può essere costituito da un file e/o da un testo (da cui viene
	 * generata una nota PDF on-the-fly). Se nel contributo è presente un file, a
	 * livello di visualizzazione dei contributi deve essere SEMPRE mostrata, a
	 * prescindere dalla presenza del suddetto testo, anche una nota PDF —generata
	 * on-the-fly— descrittiva del contributo.
	 * 
	 * @param contributi
	 * @return
	 */
	private List<ContributoDTO> gestisciNoteContributi(final List<ContributoDTO> contributi) {
		final List<ContributoDTO> contributiOutput = new ArrayList<>();

		if (!CollectionUtils.isEmpty(contributi)) {

			ContributoDTO notaContributo;

			int indexContr = 1;
			for (final ContributoDTO contributo : contributi) {
				final String indiceContr = Integer.toString(indexContr);
				contributo.setIndiceContr(indiceContr);

				// aggiungo prima la nota dell'allegato
				// Si aggiunge la nota alla lista solo se si tratta di un contributo con file
				if (!StringUtils.isNullOrEmpty(contributo.getDocumentTitle()) && !StringUtils.isNullOrEmpty(contributo.getNota())) {
					notaContributo = new ContributoDTO(ContributoDTO.getNomeNotaPdf(contributo.getIdContributo()), MediaType.PDF.toString(), null);
					notaContributo.setIdContributo(contributo.getIdContributo());
					notaContributo.setFlagEsterno(contributo.getFlagEsterno());

					notaContributo.setIndiceContr(indiceContr);
					contributiOutput.add(notaContributo);

					contributo.setIndiceContr("-" + indiceContr + ".1");

				}

				contributiOutput.add(contributo);
				indexContr++;
			}
		}

		return contributiOutput;
	}

	/**
	 * Restituisce il content dell'allegato selezionato per il download.
	 * 
	 * @param index
	 * @return file
	 */
	public StreamedContent downloadAllegato(final Object index) {
		final List<AllegatoDTO> allegati = (List<AllegatoDTO>) allegatiDTH.getMasters();
		final AllegatoDTO allegatoSelected = allegati.get((Integer) index);

		StreamedContent file = null;
		file = download(allegatoSelected.getMimeType(), allegatoSelected.getNomeFile(), allegatoSelected.getGuid());

		return file;
	}

	/**
	 * Restituisce il content del contributo selezionato per il download.
	 * 
	 * @param index
	 * @return file
	 */
	public StreamedContent downloadContributo(final Object index) {
		final List<ContributoDTO> contributi = (List<ContributoDTO>) contributiDTH.getMasters();
		final ContributoDTO contributoSelected = contributi.get((Integer) index);
		final IDocumentoFacadeSRV documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);
		final IContributoFacadeSRV contributoSRV = ApplicationContextProvider.getApplicationContext().getBean(IContributoFacadeSRV.class);

		StreamedContent file = null;

		InputStream stream = null;
		final String dt = contributoSelected.getDocumentTitle();
		byte[] content = null;
		if (dt != null) {
			// Contributo su FileNet
			final FileDTO contr = documentoSRV.getContributoContentPreview(utente.getFcDTO(), dt, utente.getIdAoo());
			content = contr.getContent();
		} else {
			// Contributo generato On-The-Fly
			final FileDTO contr = contributoSRV.getContent(utente, contributoSelected.getFlagEsterno(), contributoSelected.getIdContributo());
			content = contr.getContent();
		}
		if (content != null) {
			stream = new ByteArrayInputStream(content);
			file = new DefaultStreamedContent(stream, contributoSelected.getMimeType(), contributoSelected.getFilename());
		}
		return file;
	}

	/**
	 * Restituisce il DataTable Helper degli allegati.
	 * 
	 * @return allegatiDTH
	 */
	public SimpleDetailDataTableHelper<AllegatoDTO> getAllegatiDTH() {
		return allegatiDTH;
	}

	/**
	 * Imposta il DataTable Helper degli allegati.
	 * 
	 * @param allegatiDTH
	 */
	public void setAllegatiDTH(final SimpleDetailDataTableHelper<AllegatoDTO> allegatiDTH) {
		this.allegatiDTH = allegatiDTH;
	}

	/**
	 * Restituisce il DataTable Helper dei contributi.
	 * 
	 * @return contributiDTH
	 */
	public SimpleDetailDataTableHelper<ContributoDTO> getContributiDTH() {
		return contributiDTH;
	}

	/**
	 * Imposta il DataTable Helper dei contributi.
	 * 
	 * @param contributiDTH
	 */
	public void setContributiDTH(final SimpleDetailDataTableHelper<ContributoDTO> contributiDTH) {
		this.contributiDTH = contributiDTH;
	}

	/**
	 * Esegue la verifica della firma sull'allegato selezionato.<
	 * 
	 * @param index
	 */
	public void verificaFirmaAllegato(final Object index) {
		final SessionBean sessionBean = FacesHelper.getManagedBean(MBean.SESSION_BEAN);
		final AllegatoDTO docALL = ((List<AllegatoDTO>) allegatiDTH.getMasters()).get((Integer) index);
		final ISignFacadeSRV signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignFacadeSRV.class);
		signSRV.aggiornaVerificaFirma(docALL.getGuid(), sessionBean.getUtente());
		// Recupera principale e poi recuperare gli allegati dal principale e aggiornare
		// tutto
		signSRV.verificaFirmaAllegatoVisualizzaComponent(sessionBean.getUtente().getIdAoo(), docALL.getGuid(), sessionBean.getUtente().getFcDTO());

	}
	
	/**
	 * Restituisce true se il formato dell'allegato è xml ed esiste un'xslt da usare per la trasformazione.
	 * Viene escluso dai file trasformabili, il file Segnatura.xml.
	 * @param index dell'allegato selezionato
	 * @return true se può essere trasformato, false altrimenti
	 */
	public boolean canBeTransformed(final Object index) {
		final AllegatoDTO currentAllegato = ((List<AllegatoDTO>) allegatiDTH.getMasters()).get((Integer) index);
		String mimeType = currentAllegato.getMimeType();
		boolean isPureXml = Constants.ContentType.TEXT_XML.equals(mimeType) && currentAllegato.getNomeFile().endsWith(".xml")
				&& !Constants.Varie.SEGNATURA_FILENAME.equals(currentAllegato.getNomeFile());
		boolean isXlstPresent = false;
		
		if(isPureXml) {
			for (AllegatoDTO doc : allegatiDTH.getMasters()) {
				// Un file xslt è tale se il mimetype è xml o l'estensione è xslt.
				if (doc.getNomeFile().endsWith(".xslt")) {
					try {
						isXlstPresent = true;

						// Se l'xslt è presente, viene memorizzato il suo contenuto per un eventuale futuro utilizzo
						InputStream stream = getContent(doc.getGuid());
						xsltForTransformation = new byte[stream.available()];
						int bytes = stream.read(xsltForTransformation);

						if (bytes == -1) {
							isXlstPresent = false;
						}
						break;
					} catch (Exception e) {
						LOGGER.error("Errore riscontrato nel recupero dell'xslt.", e);
						isXlstPresent = false;
					}
				}
			}
		}
		
		return isPureXml && isXlstPresent;
	}
	
	/**
	 * Genera il pdf a partire dall'xml e ne mostra l'anteprima.
	 * Se il processo di trasformazione non va a buon fine, l'anteprima non viene visualizzata e viene mostrato un messaggio di errore.
	 * @param index indice dell'allegato selezionato
	 */
	public void generaPdf(final Object index) {
		final AllegatoDTO currentAllegato = ((List<AllegatoDTO>) allegatiDTH.getMasters()).get((Integer) index);
		InputStream stream = getContent(currentAllegato.getGuid());
		boolean isGenerated = false;
		byte[] xml = null;
		try {
			xml = new byte[stream.available()];
			int bytes = stream.read(xml);
			
			if (bytes != -1) {
				final IFlussoSipadFacadeSRV sipadSRV = ApplicationContextProvider.getApplicationContext().getBean(IFlussoSipadFacadeSRV.class);
				FileDTO preview = sipadSRV.transformXmlForSipad(xml, getXsltForTransformation());
				setPreviewSipad(preview);
				isGenerated = true;
			}
		} catch (Exception e) {
			LOGGER.error("Errore riscontrato nel recupero dell'xml.", e);
		}
		
		if(isGenerated) {
			FacesHelper.putObjectInSession(SessionObject.CONTENT_HIGHLIGHT_SIGN_FIELD, getPreviewSipad());
			setPreviewVisibile(true);
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore: ", "Impossibile generare il pdf"));
			setPreviewVisibile(false);
		}
	}

	/**
	 * Restituisce il file generato per la visualizzazione del pdf ottenuto attraverso la trasformazione dell'xml.
	 * @return file pdf generato da xml associato al flusso SIPAD
	 */
	public FileDTO getPreviewSipad() {
		return previewSipad;
	}

	/**
	 * Imposta il file pdf generato dall'xml associato al flusso SIPAD.
	 * @param docPrincipale
	 */
	public void setPreviewSipad(FileDTO previewSipad) {
		this.previewSipad = previewSipad;
	}

	/**
	 * Restituisce il byte array dell'xslt.
	 * @return content dell'allegato xslt
	 */
	public byte[] getXsltForTransformation() {
		return xsltForTransformation;
	}

	/**
	 * Imposta il content dell'allegato xslt.
	 * @param xsltForTransformation
	 */
	public void setXsltForTransformation(byte[] xsltForTransformation) {
		this.xsltForTransformation = xsltForTransformation;
	}

}