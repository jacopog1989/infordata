package it.ibm.red.web.document.mbean;

import java.util.List;
import java.util.stream.Collectors;

import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.document.mbean.component.IndiceDiClassificazioneComponent;
import it.ibm.red.web.document.mbean.component.NomeFascicoloBean;
import it.ibm.red.web.document.mbean.component.UploadDocumentoBean;
import it.ibm.red.web.document.mbean.interfaces.IDsrTabGenerale;

/**
 * Bean per il tab generale del  
 *
 */
//@Named(ConstantsWeb.MBean.DSR_DATA_TAB_GENERALE_BEAN)
public class DsrDataTabGeneraleBean implements IDsrTabGenerale {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 8016039181107213302L;

	/**
	 * Dettaglio.
	 */
	private DetailDocumentRedDTO detail;
	
	/**
	 * Componente indice di classificazione.
	 */
	private final IndiceDiClassificazioneComponent indiceDiClassificazione;
	
	/**
	 * In particolare gestisce la ricerca del fascicolo e la visualizzazione delle descrizioni dei fascicoli
	 */
	private final NomeFascicoloBean nomefascicolo;
	
	/**
	 * Bean uploadDocumento.
	 */
	private final UploadDocumentoBean uploadDocumento;

	/**
	 * Costruttore del bean.
	 * @param inDetail
	 * @param utente
	 */
	public DsrDataTabGeneraleBean(final DetailDocumentRedDTO inDetail, final UtenteDTO utente) {
		detail = inDetail;
		indiceDiClassificazione = new IndiceDiClassificazioneComponent(detail, utente);
		nomefascicolo = new NomeFascicoloBean(detail, utente);
		uploadDocumento = new UploadDocumentoBean(detail);
		
		if (StringUtils.isNullOrEmpty(detail.getPrefixFascicoloProcedimentale())) {
			this.detail.setPrefixFascicoloProcedimentale("NUMERO_ANNO_");
		}
		
	}

	/**
	 * @see it.ibm.red.web.document.mbean.IDsrTabGenerale#getDescrAssegnatarioPerCompetenza().
	 */
	@Override
	public String getDescrAssegnatarioPerCompetenza() {
		final List<AssegnazioneDTO> perCompetenza = detail.getAssegnazioni().stream()
		.filter(a -> a.getTipoAssegnazione() == TipoAssegnazioneEnum.COMPETENZA)
		.collect(Collectors.toList());
		final AssegnazioneDTO assegnazione = perCompetenza.get(0);
		return assegnazione.getDescrizioneAssegnatario();
	}
	/*********END ORGANIGRAMMA PER CONOSCENZA **********************/

	/* (non-Javadoc)
	 * @see it.ibm.red.web.document.mbean.IDsrTabGenerale#getDetail()
	 */
	@Override
	public DetailDocumentRedDTO getDetail() {
		return this.detail;
	}

	/**
	 * Imposta il detail.
	 * @param inDetail
	 */
	public void setDetail(final DetailDocumentRedDTO inDetail) {
		this.detail = inDetail;
	} 

	/**
	 * @see it.ibm.red.web.document.mbean.IDsrTabGenerale#getIndiceDiClassificazione().
	 */
	@Override
	public IndiceDiClassificazioneComponent getIndiceDiClassificazione() {
		return indiceDiClassificazione;
	}

	/**
	 * @see it.ibm.red.web.document.mbean.IDsrTabGenerale#getNomefascicolo().
	 */
	@Override
	public NomeFascicoloBean getNomefascicolo() {
		return nomefascicolo;
	}

	/**
	 * @see it.ibm.red.web.document.mbean.IDsrTabGenerale#getUploadDocumento().
	 */
	@Override
	public UploadDocumentoBean getUploadDocumento() {
		return uploadDocumento;
	}

}
