package it.ibm.red.web.document.mbean.component;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IContributoFacadeSRV;
import it.ibm.red.web.document.helper.ContributoHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.helper.faces.MessageSeverityEnum;

/**
 * Component abstract dei tab contributi.
 */
public abstract class AbstractContributoTabComponent extends VerificaFirmaComponent {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -6849015344669946279L;
	
	/**
	 * LOGGER per la gestione dei messaggi in console.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AbstractContributoTabComponent.class);
	
	/**
	 * Dettaglio documento.
	 */
	protected DetailDocumentRedDTO detail;
	
	/**
	 * Utente.
	 */
	protected UtenteDTO utente;
	
	/**
	 * Servizio gestione contributi.
	 */
	protected IContributoFacadeSRV contributoSRV;
	
	/**
	 * Lista contributi.
	 */
	protected transient List<ContributoHelper> contributoList;
	
	/**
	 * Componente upload contriuto.
	 */
	protected ContributoUpdateComponent uploader;
		
	/**
	 * Costruttore della classe.
	 * 
	 * @param detail
	 * @param utente
	 * */
	protected AbstractContributoTabComponent(final DetailDocumentRedDTO detail, final UtenteDTO utente) {
		this.detail = detail;
		this.utente = utente;
		this.contributoSRV = ApplicationContextProvider.getApplicationContext().getBean(IContributoFacadeSRV.class);
		updateContributo();
	}

	/**
	 * Gestisce l'aggiornamento della lista dei contributi.
	 */
	protected void reload() {
		List<ContributoDTO> listaFiltrata = contributoSRV.getContributi(utente, detail.getDocumentTitle());
		detail.setContributi(listaFiltrata);
		updateContributo();
	}
	
	protected abstract void updateContributo();
	
	/**
	 * Metodo che si occupa dell'eliminazione del contributo.
	 * 
	 * @param contributo contributo da eliminare.
	 * */
	public void elimina(final ContributoDTO contributo) {
		EsitoOperazioneDTO esito = null;
		Integer idDocumento = Integer.parseInt(detail.getDocumentTitle());
		
		esito = eliminaContributo(contributo, idDocumento);
		
		if (esito.isEsito()) {
			reload();
			showInfoMessage("Contributo eliminato con successo");
		} else {
			showErrorMessage(esito.getNote());
		}
	}
	
	protected abstract EsitoOperazioneDTO eliminaContributo(ContributoDTO contributo, Integer idDocumento);
	
	/**
	 * Restituisce il content del contributo.
	 * 
	 * @param contributo il contributo dalla quale recuperare il content
	 * @return StreamedContent creato dal content del contributo
	 * */
	public StreamedContent getDownload(final ContributoDTO contributo) {
		StreamedContent sc = null;
		
		try {
			FileDTO file = contributoSRV.getContent(utente, contributo.getFlagEsterno(), contributo.getIdContributo());
			InputStream io = new ByteArrayInputStream(file.getContent());
			sc = new DefaultStreamedContent(io, file.getMimeType(), file.getFileName());
		} catch (Exception e) {
			LOGGER.error("Impossibile recuperare il file", e);
			FacesHelper.showMessage(null, "Impossibile recuperare il file", MessageSeverityEnum.ERROR);
		}
		
		return sc;
	}

	/**
	 * Gestisce la modifica del contributo.
	 * 
	 * @param contributo
	 * */
	public void modifica(final ContributoDTO contributo) {
		this.uploader = new ContributoUpdateComponent(utente, contributo);
	}

	/**
	 * Gestisce la chiusura della dialog di modifica.
	 * */
	public void closeModificaDialog() {
		this.uploader = null;
		reload();
	}

	/**
	 * Getter della lista di contributi.
	 * 
	 * @return List di ContributoHelper
	 * */
	public List<ContributoHelper> getContributoList() {
		return contributoList;
	}

	/**
	 * Setter della lista di contributi.
	 * 
	 * @param contributoList lista dei contributi da impostare.
	 * */
	public void setContributoList(final List<ContributoHelper> contributoList) {
		this.contributoList = contributoList;
	}

	/**
	 * Getter di uploader, restituisce il componente ContributoUpdateComponent.
	 * 
	 * @return componente che gestisce l'upload
	 * */
	public ContributoUpdateComponent getUploader() {
		return uploader;
	}

	/**
	 * Setter uploader.
	 * 
	 * @param uploader il componente da impostare
	 * */
	public void setUploader(final ContributoUpdateComponent uploader) {
		this.uploader = uploader;
	}
	
}
