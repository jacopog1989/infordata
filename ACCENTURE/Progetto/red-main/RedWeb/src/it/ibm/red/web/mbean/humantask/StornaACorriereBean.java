package it.ibm.red.web.mbean.humantask;
 
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IOperationDocumentFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * Bean che gestisce la response di storno al corriere.
 */
@Named(ConstantsWeb.MBean.STORNA_A_CORRIERE_BEAN)
@ViewScoped
public class StornaACorriereBean extends AbstractBean implements InitHumanTaskInterface {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 3888395941033708599L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(StornaACorriereBean.class.getName());

	/**
	 * Motivazione storno.
	 */
	private String motivoStorno;

	/**
	 * Lista documenti bean.
	 */
	private ListaDocumentiBean ldb;

	/**
	 * Lista master.
	 */
 	private List<MasterDocumentRedDTO> masters;
	
 	/**
 	 * Post construct del bean.
 	 */
	@Override
	protected void postConstruct() {
		// Metodo intenzionalmente vuoto.
	}
	
	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		masters = inDocsSelezionati;
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
	}
	
	/**
	 * Esegue una validazione sui parametri e esegue la logica per lo storno a corriere.
	 * Aggiorna la lista degli esiti di @see ListaDocumentiBean in modo da permetterne la corretta visualizzazione a valle delle operazioni.
	 */
	public void stornaACorriereResponse() { 
		try {
			if (StringUtils.isNullOrEmpty(motivoStorno)) {
				showError("La motivazione dello storno rifiuto è obbligatoria");
			} else {
				final Collection<String> wobNumbers = new ArrayList<>();
				final IOperationDocumentFacadeSRV opeDocumentSRV = ApplicationContextProvider.getApplicationContext().getBean(IOperationDocumentFacadeSRV.class);
				final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
				final UtenteDTO utente = sb.getUtente();
				
				// Si puliscono gli esiti
				ldb.cleanEsiti();
				
				// Si raccolgono i wobnumber necessari ad invocare il service 
				for (final MasterDocumentRedDTO m : masters) {
					if (!StringUtils.isNullOrEmpty(m.getWobNumber())) {
						wobNumbers.add(m.getWobNumber());
					}
				}
				  
				// Invoke service								   
				final Collection<EsitoOperazioneDTO> eOpe = opeDocumentSRV.stornaACorriere(utente.getFcDTO(), wobNumbers, utente, motivoStorno);
				
				// Si impostano i nuovi esiti
				ldb.getEsitiOperazione().addAll(eOpe);
				
				FacesHelper.executeJS("PF('dlgGeneric').hide()");
				FacesHelper.update("centralSectionForm:idDlgShowResult");
				FacesHelper.executeJS("PF('dlgShowResult').show()");
				
				ldb.destroyBeanViewScope(ConstantsWeb.MBean.STORNA_A_CORRIERE_BEAN);
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'operazione di storno al corriere", e);
			showError("Si è verificato un errore durante l'operazione di storno al corriere");
		}
	}

	/**
	 * Restituisce la motivazione dello storno.
	 * @return motivo storno
	 */
	public String getMotivoStorno() {
		return motivoStorno;
	}

	/**
	 * Imposta la motivazione dello storno.
	 * @param motivoStorno
	 */
	public void setMotivoStorno(final String motivoStorno) {
		this.motivoStorno = motivoStorno;
	}
	

}
