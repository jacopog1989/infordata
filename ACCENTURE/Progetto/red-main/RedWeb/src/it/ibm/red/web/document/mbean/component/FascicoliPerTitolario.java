package it.ibm.red.web.document.mbean.component;

import java.io.Serializable;
import java.util.Collection;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.MasterFascicoloDTO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IFascicoloFacadeSRV;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;

/**
 * Component fascicoli per titolario.
 */
public class FascicoliPerTitolario implements Serializable {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 6177937087794660343L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FascicoliPerTitolario.class.getName());

	/**
	 * Fascicolo selezionato.
	 */
	private MasterFascicoloDTO selectedFascicolo;
	
	/**
	 * Root albero fascicoli.
	 */
	private DefaultTreeNode rootFascicoli;
	
	/**
	 * Flag non classificato.
	 */
	private boolean nonClassificati;

	/**
	 * Dtatable fascicoli.
	 */
	private SimpleDetailDataTableHelper<MasterFascicoloDTO> fascicoliPerTitolarioDTH;

	/**
	 * Informazioni utente.
	 */
	private final UtenteDTO utente;

	/**
	 * Indice di classificazione.
	 */
	private String indiceDiClassificazione;
	
	
	/********** Costruttore ***********/
	public FascicoliPerTitolario(final UtenteDTO utente) {
		this.utente = utente;
		fascicoliPerTitolarioDTH = new SimpleDetailDataTableHelper<>();
		selectedFascicolo = null;
		indiceDiClassificazione = "";
		nonClassificati = true;
	}
	
	/*********************** Alla selezione del titolario recupera i fascicoli che gli appartengono ***************************/
	public void onNodeSelect(final NodeSelectEvent event) {
	    	try {
	    		final TreeNode node = event.getTreeNode();
	    		indiceDiClassificazione = ((TitolarioDTO) node.getData()).getIndiceClassificazione();
	    		final IFascicoloFacadeSRV fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloFacadeSRV.class);
	    		final Collection<MasterFascicoloDTO> fascicoliNC = fascicoloSRV.getFascicoliByIndiceDiClassificazione(utente, indiceDiClassificazione);
	    		fascicoliPerTitolarioDTH.setMasters(fascicoliNC);
	    		nonClassificati = false;
			} catch (final Exception e) {
				LOGGER.error(e);
			}
	    }


	 
/* ----------------------- Getter e Setter ------------------ */
	/**
	 * Restituisce il fascicolo selezionato.
	 * @return fascicolo selezionato
	 */
	public MasterFascicoloDTO getSelectedFascicolo() {
		return selectedFascicolo;
	}

	/**
	 * Imposta il fascicolo selezionato.
	 * @param selectedFascicolo
	 */
	public void setSelectedFascicolo(final MasterFascicoloDTO selectedFascicolo) {
		this.selectedFascicolo = selectedFascicolo;
	}

	/**
	 * Restituisce la root del tree dei fascicoli.
	 * @return root fascicoli
	 */
	public DefaultTreeNode getRootFascicoli() {
		return rootFascicoli;
	}

	/**
	 * Restituisce true se non classificati, false altrimenti.
	 * @return true se non classificati, false altrimenti
	 */
	public boolean isNonClassificati() {
		return nonClassificati;
	}

	/**
	 * Imposta la root del tree dei fascicoli.
	 * @param rootFascicoli
	 */
	public void setRootFascicoli(final DefaultTreeNode rootFascicoli) {
		this.rootFascicoli = rootFascicoli;
	}

	/**
	 * Imposta il flag: nonClassificati.
	 * @param nonClassificati
	 */
	public void setNonClassificati(final boolean nonClassificati) {
		this.nonClassificati = nonClassificati;
	}

	/**
	 * Restituisce l'helper del datatable dei fascicoli per titolario.
	 * @return helper datatable fascicoli per titolario
	 */
	public SimpleDetailDataTableHelper<MasterFascicoloDTO> getFascicoliPerTitolarioDTH() {
		return fascicoliPerTitolarioDTH;
	}

	/**
	 * Imposta l'helper del datatable dei fascicoli per titolario.
	 * @param fascicoliPerTitolarioDTH
	 */
	public void setFascicoliPerTitolarioDTH(final SimpleDetailDataTableHelper<MasterFascicoloDTO> fascicoliPerTitolarioDTH) {
		this.fascicoliPerTitolarioDTH = fascicoliPerTitolarioDTH;
	}

	/**
	 * Restituisce l'indice di classificazione.
	 * @return indice di classificazione
	 */
	public String getIndiceDiClassificazione() {
		return indiceDiClassificazione;
	}

	/**
	 * Imposta l'indice di classificazione.
	 * @param indiceDiClassificazione
	 */
	public void setIndiceDiClassificazione(final String indiceDiClassificazione) {
		this.indiceDiClassificazione = indiceDiClassificazione;
	}

}
