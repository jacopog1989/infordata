package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IRifiutaSpedizioneFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * Bean che gestisce la response di rifiuto spedizione.
 */
@Named(ConstantsWeb.MBean.RIFIUTA_SPEDIZIONE_BEAN)
@ViewScoped
public class RifiutaSpedizioneBean extends AbstractBean implements InitHumanTaskInterface {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -1975553302144717762L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RifiutaSpedizioneBean.class.getName());
	
	/**
	 * Messaggio rifiuto spedizione.
	 */
	private String messaggioRifiutoSpedizione;

	/**
	 * Documenti.
	 */
	private List<MasterDocumentRedDTO> masters;

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		masters = inDocsSelezionati;
		messaggioRifiutoSpedizione = "Mancata documentazione cartacea";
	}


	@Override
	protected void postConstruct() {
		// Non occorre fare niente nel post construct del bean.
	}
	
	/**
	 * Esegue la logica della response "Rifiuta Response", a monte esegue una validazione dei parametri.
	 * A valle gestisce gli esiti in modo da essere correttamente visualizzati.
	 */
	public void rifiutaSpedizioneResponse() {
		final Collection<EsitoOperazioneDTO> eOpe = new ArrayList<>();
		try {
			final IRifiutaSpedizioneFacadeSRV opeRifiutaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRifiutaSpedizioneFacadeSRV.class);
			final ListaDocumentiBean ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
			final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
			final UtenteDTO u = sb.getUtente();
			
			//pulisco gli esiti
			ldb.cleanEsiti();
			
			final String wobNumber = masters.get(0).getWobNumber();
			final Integer numDocumento = masters.get(0).getNumeroDocumento();
			
			//invoke service
			eOpe.add(opeRifiutaSRV.rifiuta(u.getFcDTO(), wobNumber, messaggioRifiutoSpedizione, u.getId().intValue(), numDocumento));
			
			//setto gli esiti con i nuovi ed eseguo un refresh
			ldb.getEsitiOperazione().addAll(eOpe);
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.RIFIUTA_SPEDIZIONE_BEAN);
			
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il Rifiuto della spedizione", e);
			showError("Si è verificato un errore durante il Rifiuto della spedizione");
		}
	}
	
	/**
	 * Restituisce il messaggio di rifiuto spedizione.
	 * @return messaggio rifiuto spedizione
	 */
	public String getMessaggioRifiutoSpedizione() {
		return messaggioRifiutoSpedizione;
	}
	
	/**
	 * Imposta il messaggio di rifiuto spedizione.
	 * @param messaggioRifiutoSpedizione
	 */
	public void setMessaggioRifiutoSpedizione(final String messaggioRifiutoSpedizione) {
		this.messaggioRifiutoSpedizione = messaggioRifiutoSpedizione;
	}



}
