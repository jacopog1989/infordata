package it.ibm.red.web.document.mbean.component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import it.ibm.red.business.dto.DetailFascicoloRedDTO;
import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDetailFascicoloFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.FascicoloManagerBean;
import it.ibm.red.web.mbean.interfaces.IUpdatableDetailCaller;

/**
 * Component dettaglio faldone RED.
 */
public class DettaglioFaldoneRedComponent extends AbstractBean implements IUpdatableDetailCaller<DetailFascicoloRedDTO> {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 9201861030295137204L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DettaglioFaldoneRedComponent.class);

	/**
	 * Utente.
	 */
	private final UtenteDTO utente;
	
	/**
	 * Servizio.
	 */
	private final IDetailFascicoloFacadeSRV fascicoloSRV;
	
	/**
	 * Dettaglio faldone.
	 */
	private FaldoneDTO detail;
	
	/**
	 * Lista figli faldone.
	 */
	private Collection<FaldoneDTO> faldoniFigli;
	
	/**
	 * Lista fascicoli faldone.
	 */
	private Collection<FascicoloDTO> fascicoli;
	
	/**
	 * Fascicolo esteso.
	 */
	private FascicoloDTO fascicoloEsteso;

	/**
	 * Il costruttore del component.
	 * @param utente
	 */
	public DettaglioFaldoneRedComponent(final UtenteDTO utente) {
		this.utente = utente;
		fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IDetailFascicoloFacadeSRV.class);
	}
	
	@Override
	protected void postConstruct() {
		//Questo metodo è lasciato intenzionalmente vuoto.
	}
	
	/**
	 * Utilizzato nel likn sul nome del fascicolo dalla lista dei fascicoli di un documento.
	 * @param index
	 */
	public void openFascicoloView(final Object index) {
		
		try {
			final Integer i = (Integer) index;
			final List<FascicoloDTO> list = new ArrayList<>(fascicoli);
			fascicoloEsteso = list.get(i);
			
			final DetailFascicoloRedDTO df = fascicoloSRV.getFascicolo(fascicoloEsteso, utente);

			final FascicoloManagerBean bean = FacesHelper.getManagedBean(MBean.FASCICOLO_MANAGER_BEAN);
			
			if (bean != null) {
				bean.setDetail(df);
				bean.setDialogFascicoloVisible(true);
				bean.setChiamante("DettaglioFaldoneRedComponent");
				bean.setCaller(this);
			}
		
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di recupero del fascicolo.");
		}
		
	}

	/**
	 * @see it.ibm.red.web.mbean.interfaces.IUpdatableDetailCaller#updateDetail(java.lang.Object).
	 */
	@Override
	public void updateDetail(final DetailFascicoloRedDTO updated) {
		
		// veriico che il fascicolo modificato si trovi ancora nel faldone di dettaglio (detail)
		final boolean stillInside = updated.getFaldoni().stream()
			.anyMatch(faldone -> detail.getGuid().equals(faldone.getGuid()));
		
		if (stillInside) { // nell'altro caso lo modifico perché la sua visaulizzazione risulti coerente
			
			fascicoloSRV.convertDettaglioToFascicolo(updated, fascicoloEsteso);
			
		} else { // lo elimino dalla coda dei fascicoli
			fascicoli = fascicoli.stream()
				.filter(a-> !fascicoloEsteso.getGuid().equals(a.getGuid()))
				.collect(Collectors.toList());			
		}
	}

	/**
	 * Restituisce il dettaglio del faldone.
	 * @return FaldoneDTO
	 */
	public FaldoneDTO getDetail() {
		return detail;
	}

	/**
	 * Imposta il dettaglio del faldone.
	 * @param detail
	 */
	public void setDetail(final FaldoneDTO detail) {
		this.detail = detail;
	}

	/**
	 * Restituisce la lista dei fascicoli.
	 * @return List di FascicoloDTO
	 */
	public Collection<FascicoloDTO> getFascicoli() {
		return fascicoli;
	}

	/**
	 * Imposta la lista dei fasicoli.
	 * @param fascicoli
	 */
	public void setFascicoli(final Collection<FascicoloDTO> fascicoli) {
		this.fascicoli = fascicoli;
	}

	/**
	 * Restituisce la lista dei faldoni figli.
	 * @return lista di FaldoneDTO
	 */
	public Collection<FaldoneDTO> getFaldoniFigli() {
		return faldoniFigli;
	}

	/**
	 * Imposta la lista dei faldoni figli.
	 * @param faldoniFigli
	 */
	public void setFaldoniFigli(final Collection<FaldoneDTO> faldoniFigli) {
		this.faldoniFigli = faldoniFigli;
	}

}
