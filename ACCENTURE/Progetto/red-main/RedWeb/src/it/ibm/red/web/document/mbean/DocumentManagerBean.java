package it.ibm.red.web.document.mbean;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.PrimeFaces;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.tabview.TabView;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;
import org.springframework.web.util.JavaScriptUtils;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.constants.Constants.ManagedBeanName;
import it.ibm.red.business.constants.Constants.Modalita;
import it.ibm.red.business.constants.Constants.Varie;
import it.ibm.red.business.dto.AllaccioDocUscitaDTO;
import it.ibm.red.business.dto.AllaccioRiferimentoStoricoDTO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AnagraficaDipendentiComponentDTO;
import it.ibm.red.business.dto.AssegnatarioDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.AttributiEstesiAttoDecretoDTO;
import it.ibm.red.business.dto.CapitoloSpesaMetadatoDTO;
import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.ComuneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DetailFascicoloRedDTO;
import it.ibm.red.business.dto.DocumentAppletContentDTO;
import it.ibm.red.business.dto.DocumentManagerDTO;
import it.ibm.red.business.dto.DocumentiEFascicoliDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.EsitoDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.EsitoVerificaProtocolloDTO;
import it.ibm.red.business.dto.EsitoVerificaProtocolloNsdDTO;
import it.ibm.red.business.dto.FadAmministrazioneDTO;
import it.ibm.red.business.dto.FadRagioneriaDTO;
import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.GestioneLockDTO;
import it.ibm.red.business.dto.InfoDocTemplate;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.LookupTableDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.MasterFascicoloDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.MetadatoDinamicoDTO;
import it.ibm.red.business.dto.MezzoRicezioneDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.NotaDTO;
import it.ibm.red.business.dto.NotificaAzioneNpsDTO;
import it.ibm.red.business.dto.NotificaInteroperabilitaEmailDTO;
import it.ibm.red.business.dto.NotificaNpsDTO;
import it.ibm.red.business.dto.ParamsRicercaProtocolloDTO;
import it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.ProvinciaDTO;
import it.ibm.red.business.dto.RdsSiebelDescrizioneDTO;
import it.ibm.red.business.dto.RegioneDTO;
import it.ibm.red.business.dto.RegistroRepertorioDTO;
import it.ibm.red.business.dto.ResponsabileCopiaConformeDTO;
import it.ibm.red.business.dto.RicercaRubricaDTO;
import it.ibm.red.business.dto.RiferimentoProtNpsDTO;
import it.ibm.red.business.dto.RiferimentoStoricoNsdDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.dto.TemplateMetadatoValueDTO;
import it.ibm.red.business.dto.TemplateValueDTO;
import it.ibm.red.business.dto.TestoPredefinitoDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.ValutaMetadatiDTO;
import it.ibm.red.business.dto.VersionDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.enums.AccessoFunzionalitaEnum;
import it.ibm.red.business.enums.AllaccioDocUscitaEnum;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ColoreNotaEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.EsitoFaldoneEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.IconaStatoEnum;
import it.ibm.red.business.enums.ImageMailEnum;
import it.ibm.red.business.enums.LookupTablePresentationModeEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.PermessiAOOEnum;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.PostaEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.RicercaPerBusinessEntityEnum;
import it.ibm.red.business.enums.RubricaChiamanteEnum;
import it.ibm.red.business.enums.SalvaDocumentoErroreEnum;
import it.ibm.red.business.enums.StatoCodaEmailEnum;
import it.ibm.red.business.enums.TipoAllaccioEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.enums.TipoGestioneEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.enums.TipoNotificaAzioneNPSEnum;
import it.ibm.red.business.enums.TipoNotificaPecEnum;
import it.ibm.red.business.enums.TipoRubricaEnum;
import it.ibm.red.business.enums.TipoSpedizioneDocumentoEnum;
import it.ibm.red.business.enums.TipoSpedizioneEnum;
import it.ibm.red.business.enums.TipologiaDestinatariEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.enums.TipologiaIndirizzoEmailEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.CodaEmail;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.Legislatura;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.NodoUtenteCoordinatore;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.facade.IAllacciaDocUscitaFacadeSRV;
import it.ibm.red.business.service.facade.IAssegnazioneAutomaticaFacadeSRV;
import it.ibm.red.business.service.facade.IAttoDecretoUCBFacadeSRV;
import it.ibm.red.business.service.facade.ICasellePostaliFacadeSRV;
import it.ibm.red.business.service.facade.ICodaMailFacadeSRV;
import it.ibm.red.business.service.facade.IDetailFascicoloFacadeSRV;
import it.ibm.red.business.service.facade.IDfFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV;
import it.ibm.red.business.service.facade.IFaldoneFacadeSRV;
import it.ibm.red.business.service.facade.IFascicoloFacadeSRV;
import it.ibm.red.business.service.facade.IFepaFacadeSRV;
import it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV;
import it.ibm.red.business.service.facade.IMetadatiDinamiciFacadeSRV;
import it.ibm.red.business.service.facade.INodoFacadeSRV;
import it.ibm.red.business.service.facade.INotaFacadeSRV;
import it.ibm.red.business.service.facade.INotificaNpsFacadeSRV;
import it.ibm.red.business.service.facade.INpsFacadeSRV;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.service.facade.IRegistroRepertorioFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaFacadeSRV;
import it.ibm.red.business.service.facade.IRiferimentoStoricoFacadeSRV;
import it.ibm.red.business.service.facade.IRubricaFacadeSRV;
import it.ibm.red.business.service.facade.ISalvaDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.ISiebelFacadeSRV;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.business.service.facade.IStampigliaturaSegnoGraficoFacadeSRV;
import it.ibm.red.business.service.facade.IStoricoFacadeSRV;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.IValutaFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.DocumentoUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.mbean.component.AllegatiDaAllacciInitializer;
import it.ibm.red.web.document.mbean.component.ContributoEsternoTabComponent;
import it.ibm.red.web.document.mbean.component.ContributoInternoTabComponent;
import it.ibm.red.web.document.mbean.component.DetailDocumentRedComponent;
import it.ibm.red.web.document.mbean.component.DettaglioDocumentoStoricoComponent;
import it.ibm.red.web.document.mbean.component.DialogFaldonaComponent;
import it.ibm.red.web.document.mbean.component.DocumentManagerDataIndiceDiClassificazioneComponent;
import it.ibm.red.web.document.mbean.component.DocumentManagerDataTabApprovazioniComponent;
import it.ibm.red.web.document.mbean.component.DocumentManagerDataTabStoricoNoteComponent;
import it.ibm.red.web.document.mbean.component.DocumentManagerDataTabTrasmissioneComponent;
import it.ibm.red.web.document.mbean.component.FascicoliPerTitolario;
import it.ibm.red.web.document.mbean.component.RegioniProvinceComuniComponent;
import it.ibm.red.web.document.mbean.component.RubricaComponent;
import it.ibm.red.web.document.mbean.component.TabAllegatiComponent;
import it.ibm.red.web.document.mbean.component.TemplateDocUscitaComponent;
import it.ibm.red.web.document.mbean.component.TemplateDocUscitaComponent.TemplateMetadatoView;
import it.ibm.red.web.document.validator.DocumentManagerValidatorEngine;
import it.ibm.red.web.enums.NavigationTokenEnum;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.DettaglioDocumentoBean;
import it.ibm.red.web.mbean.DettaglioFascicoloRicercaBean;
import it.ibm.red.web.mbean.FascicoloFepaBean;
import it.ibm.red.web.mbean.FascicoloManagerBean;
import it.ibm.red.web.mbean.ListaCartaceiAcquisitiBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.PredisponiDaRegistroAusiliarioBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.VisualizzaDettaglioFilesComponent;
import it.ibm.red.web.mbean.beantask.EsitoCreaDocumentoDaResponseBean;
import it.ibm.red.web.mbean.component.CercaFascicoloComponent;
import it.ibm.red.web.mbean.component.DetailPreviewDocumentManagerComponent;
import it.ibm.red.web.mbean.component.DocumentManagerCercaFascicoloComponent;
import it.ibm.red.web.mbean.component.DocumentManagerDettaglioVersioniAllegatiComponent;
import it.ibm.red.web.mbean.interfaces.IDetailPreviewComponent;
import it.ibm.red.web.mbean.interfaces.IUpdatableDetailCaller;
import it.ibm.red.web.mbean.master.IRubricaHandler;
import it.ibm.red.web.ricerca.mbean.RicercaDocumentiRedBean;
import it.ibm.red.web.ricerca.mbean.RicercaRapidaBean;
import it.ibm.red.web.sottoscrizioni.mbean.SottoscrizioniBean;
import it.ibm.red.webservice.model.interfaccianps.dto.ParametersAzioneRispostaAutomatica;

/**
 * Bean document manager per la gestione dei documenti in maniera centralizzata.
 */
@Named(MBean.DOCUMENT_MANAGER_BEAN)
@ViewScoped
public class DocumentManagerBean extends AbstractBean implements IRubricaHandler, IUpdatableDetailCaller<FascicoloDTO> {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Messaggio di errore recupero alberatura.
	 */
	private static final String ALBERATURA_NODI_NON_RECUPERATA_CORRETTAMENTE = "Alberatura nodi non recuperata correttamente.";

	/**
	 * Messaggio di warning sul recupero del protocollo.
	 */
	private static final String ATTENZIONE_PROTOCOLLO_NON_TROVATO = "Attenzione, protocollo non trovato";

	/**
	 * Delimitatore del percorso file.
	 */
	private static final String PATH_DELIMITER = "/";

	/**
	 * Messaggio di notifica non prevista.
	 */
	private static final String NOTIFICA_NON_PREVISTA_MSG = "Notifica non prevista";
	
	/**
	 * Descrizione iter manuale.
	 */
	private static final String ITER_MANUALE_DESC = "Manuale";
	
	/**
	 * Modalita "Elettronico" descrizione.
	 */
	private static final String MODE_ELETTRONICO_DESC = "Elettronico";
	
	/**
	 * Messaggio di errore indice non valido.
	 */
	private static final String ERROR_INDICE_NON_VALIDO_MSG = "Indice del documento non valido.";
	
	/**
	 * Messaggio generico di errore, occorre appendere l'operazione al messaggio.
	 */
	private static final String GENERIC_ERROR_MSG = "Errore durante l'operazione: ";
	
	/**
	 * Messaggio errore nota mancante.
	 */
	private static final String ERROR_NOTA_MANCANTE_MSG = "E' obbligatorio inserire una nota";
	
	/**
	 * Messaggio di duplicazione assegnatario.
	 */
	private static final String ASSEGNATARIO_DUPLICATO_MSG = "Assegnatario duplicato";
	
	/**
	 * ID component view per la gestione dei contatti preferiti.
	 */
	private static final String CONTATTIPREFERITITBL_RESOURCE_NAME = "idDettagliEstesiForm:preferitoTabV:contattiPreferitiTbl";

	/**
	 * ID component view visualizzazione preferito.
	 */
	private static final String PREFERITOTABV_RESOURCE_NAME = "idDettagliEstesiForm:preferitoTabV";

	/**
	 * ID component view gestione contatti preferiti mittenti.
	 */
	private static final String IDPREFERITIMITTENTIDMD_RESOURCE_NAME = "idDettagliEstesiForm:idMittentePrefTabs_DMD:idPreferitiMittenti_DMD";

	/**
	 * ID component view visualizzazione mittente preferito.
	 */
	private static final String IDMITTENTEPREFTABSDMD_RESOURCE_NAME = "idDettagliEstesiForm:idMittentePrefTabs_DMD";

	/**
	 * Id dei tabs.
	 */
	private static final String IDTABSDMD_RESOURCE_NAME = "idDettagliEstesiForm:idTabs_DMD";

	/**
	 * Id del container del tab spedizione.
	 */
	private static final String IDTABSPEDIZIONECONTAINERDMD_RESOURCE_NAME = "idDettagliEstesiForm:idTabs_DMD:idTabSpedizioneContainer_DMD";

	/**
	 * ID component view gestione destintari.
	 */
	private static final String IDDESTINATARIDMD_RESOURCE_LOCATION = "idDettagliEstesiForm:idDestinatari_DMD";

	/**
	 * ID component view mittente auto.
	 */
	private static final String IDMITTENTEAUTO_DMD_RESOURCE_LOCATION = "eastSectionForm:idMittenteAuto_DMD";

	/**
	 * ID component view gestione dettaglio documento.
	 */
	private static final String IDDETTAGLIODOCUMENTO_RESOURCE_LOCATION = "eastSectionForm:idDettaglioDocumento_DD";

	/**
	 * ID east section form.
	 */
	private static final String EAST_SECTION_FORM_NAME = "eastSectionForm";

	/**
	 * Informativa: DOC-CONTENT MODIFICABILE.
	 */
	private static final String DOCCONTENTMODIFICABILE_LABEL = "[CHECK DOC-CONTENT MODIFICABILE][";

	/**
	 * JS per l'hide del: wdgSelezionaDaRubrica.
	 */
	private static final String HIDE_WDGSELEZIONADARUBRICA_JS = "PF('wdgSelezionaDaRubrica').hide()";

	/**
	 * JS per la visualizzazione del: wdgErrorMessage.
	 */
	private static final String SHOW_WDGERRORMESSAGE_JS = "PF('wdgErrorMessage').show()";
	
	/**
	 * JS per la visualizzazione del: wdgConfermaAssegnazione.
	 */
	private static final String SHOW_WDGCONFERMAASSEGNAZIONE_JS = "PF('wdgConfermaAssegnazione').show()";

	/**
	 * JS per la visualizzazione del: wdgCercaPreferito.
	 */
	private static final String HIDE_WDGCERCAPREFERITO_JS = "PF('wdgCercaPreferito').hide();";
	
	/**
	 * LOGGER per la gestione dei messaggi in console.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DocumentManagerBean.class);

	/**
	 * OTF.
	 */
	private static final int ON_THE_FLY = 1;
	
	/*** SERVICES START.. *********************************************************************************/
	/**
	 * Service manager per la maschera di creazione/modifica.
	 */
	private IDocumentManagerFacadeSRV documentManagerSRV;

	/**
	 * Service.
	 */
	private IAssegnazioneAutomaticaFacadeSRV assegnazioniAutomaticheSRV;
	
	/**
	 * Service per la procedura registra. 
	 */
	private ISalvaDocumentoFacadeSRV salvaDocumentoSRV;

	/**
	 * Creazione organigrammi.
	 */
	private IOrganigrammaFacadeSRV organigrammaSRV;

	/**
	 * Utilizzato per Raccolta FAD. 
	 */
	private IFepaFacadeSRV fepaSRV;

	/**
	 * Service.
	 */
	private IDocumentoRedFacadeSRV documentoRedSRV;

	/**
	 * Service.
	 */
	private IDocumentoFacadeSRV documentoSRV;

	/**
	 * Serve per la ricerca fascicolo.
	 */
	private IRicercaFacadeSRV ricercaSRV;

	/**
	 * Service.
	 */
	private IMetadatiDinamiciFacadeSRV metadatiDinamiciSRV;

	/**
	 * Service.
	 */
	private IFaldoneFacadeSRV faldoneSRV;

	/**
	 * Service.
	 */
	private ISignFacadeSRV signSRV;

	/**
	 * Service.
	 */
	private IDetailFascicoloFacadeSRV detailFascicoloSRV;

	/**
	 * Service.
	 */
	private ISiebelFacadeSRV siebelSRV;

	/**
	 * Service.
	 */
	private ICasellePostaliFacadeSRV casellePostaliSRV;

	/**
	 * Service.
	 */
	private IRegistroRepertorioFacadeSRV registroRepertorioSRV;
	
	/**
	 * Service.
	 */
	private ITipologiaDocumentoFacadeSRV tipologiaDocumentoSRV;

	/**
	 * Service.
	 */
	private IAllacciaDocUscitaFacadeSRV allacciaDocUscitaSRV;
	
	/**
	 * Service.
	 */
	private IRiferimentoStoricoFacadeSRV riferimentoStoricoSRV;
	
	/**
	 * Service.
	 */
	private IFascicoloFacadeSRV fascicoloSRV;
	
	/**
	 * Service.
	 */
	private IAssegnazioneAutomaticaFacadeSRV assAutomaticaSRV;
	
	/**
	 * Service.
	 */
	private IAooSRV aooSRV;

	/**
	 * Service.
	 */
	private IAttoDecretoUCBFacadeSRV attoDecretoUCBSRV;
	
	/*** SERVICES END. .*********************************************************************************/

	private SalvaDocumentoRedParametriDTO sd;

	/**
	 * BEan di sessione.
	 */
	private SessionBean sessionBean;

	/**
	 * Info utente.
	 */
	private UtenteDTO utente;
	
	/**
	 * Dettaglio selezionato.
	 */
	private DetailDocumentRedDTO detail;

	/**
	 * Dettaglio versione precedente.
	 */
	private DetailDocumentRedDTO detailPreviousVersion;

	/**
	 * Dettaglio.
	 */
	private DocumentManagerDTO documentManagerDTO;

	/**
	 * Indica il Bean che apre in modifica il documento. 
	 * Mi serve per capire quali info bisogna aggiornare dopo la modifica del documento.
	 */
	private String chiamante; 

	/**
	 * Serve perché l'enum ha l'id che è un Long mentre nel DTO idFormatoDocumento è un Integer
	 * la selezione sulla combo primefaces non casta a Integer e provoca errori nella pagina.
	 */
	private Long idFormatoDocumentoSelected;
	
	/**
	 *  Tipo proc selezionato.
	 */
	private Long idTipoProcedimentoSelected;

	/**
	 * Tipologia documenmto.
	 */
	private Integer idTipologiaDocumentoOriginale;

	/**
	 * Tipologia procedimento.
	 */
	private Integer idTipologiaProcedimentoOriginale;

	/**
	 * Flag repertorio originale.
	 */
	private Boolean isRepertorioOriginale;

	/**
	 * Descrizione registro.
	 */
	private String descRegistroOriginale;

	/**
	 *  MODALITA INSERT / MODALITA UPDATE / MODALITA READ ONLY.
	 */
	private String modalitaMaschera;

	/**
	 * Contatto da visualizzare.
	 */
	private Contatto contattoDaVisualizzare;
	
	/**
	 * Destinari in modifica.
	 */
	private List<DestinatarioRedDTO> destinatariInModifica;

	/**
	 * Riferimento protocollo in entrata.
	 */
	private List<RispostaAllaccioDTO> riferimentiProt;

	/**
	 * Docuemtno principale.
	 */
	private transient FileDTO docPrincipale;

	/**
	 * Lista metadati dinamici.
	 */
	private List<String> metadatiDinamiciToCopy;

	/**Titolario start.***********************************************/
	private DocumentManagerDataIndiceDiClassificazioneComponent indiceDiClassificazione;
	/**Titolario end.*************************************************/

	/** LABERATURA DA ESPLODERE START***********************************************************************************/
	private List<Nodo> alberaturaNodi;
	/** LABERATURA DA ESPLODERE END*************************************************************************************/

	/**ORGANIGRAMMA DAL PROPRIO UFFICIO CON UTENTI START..*************************************************************/
	private DefaultTreeNode rootOrganigrammaComune;
	/**ORGANIGRAMMA DAL PROPRIO UFFICIO CON UTENTI END.*************************************************************/

	/**ORGANIGRAMMA ASSEGNAZIONE PER COMPETENZA START.*************************************************************/
	private DefaultTreeNode rootAssegnazionePerCompetenza;
	/**
	 * E' l'assegnatario che viene utilizzato in assegnazione per competenza in ingresso 
	 * o per l'iter manuale firma/sigla/competenza etc.... nel documento in uscita. 
	 */
	private AssegnazioneDTO assegnatarioPrincipale;
	
	/**
	 * Descreizinoe assegnatario per competenza.
	 */
	private String descrAssegnatarioPerCompetenza;
	/**ORGANIGRAMMA ASSEGNAZIONE PER COMPETENZA END.***************************************************************/

	/**ORGANIGRAMMA ASSEGNAZIONE ITER MANUALE START.*************************************************************/
	private DefaultTreeNode rootAssegnazioneIterManuale;
	
	/**
	 * Descrizione assegnatario iter manuale.
	 */
	private String descrAssegnatarioIterManuale;
	
	/**
	 * Informazioni iter approvativo.
	 */
	private String iterApprovativoInfo;
	/**ORGANIGRAMMA ASSEGNAZIONE ITER MANUALE END.***************************************************************/

	/**RACCOLTA FAD START.***************************************************/
	private String codiceAmministrazioneSelected;
	
	/**
	 * Flag ragioneria selected.
	 */
	private FadRagioneriaDTO fadRagioneriaSelected;
	
	/**
	 * Identifictivo raccolta.
	 */
	private String identificativoRaccolta;
	
	/**
	 * Descrizione maminsitrazione selezionata.
	 */
	private String descrizioneAmministrazioneSelected;
	
	/**RACCOLTA FAD END.*****************************************************/

	/**ASSEGNAZIONI PER CONOSCENZA START.*********************************************************************/
	private List<AssegnazioneDTO> assegnatariPerConoscenza;
	/**ASSEGNAZIONI PER CONOSCENZA END.***********************************************************************/

	/**ASSEGNAZIONI PER CONTRIBUTO START.*********************************************************************/
	private List<AssegnazioneDTO> assegnatariPerContributo;
	/**ASSEGNAZIONI PER CONTRIBUTO END.***********************************************************************/

	/**TAB SPEDIZIONE START.****************************************************/
	private Long idTestoPredefinitoMail;

	/**
	 * Alleagti mail selezionati.
	 */
	private List<DetailDocumentRedDTO> allegatiMailSelected;
	/**TAB SPEDIZIONE END.******************************************************/

	/** RICERCA FASCICOLO START. ****************************************************************************/
	private String ricercaFascicoloKey; 
	
	/**
	 * Ricerca anno fascicolo.
	 */
	private Integer ricercaFascicoloAnno;
	
	/**
	 * Ricerca fascicolo modalità.
	 */
	private RicercaGenericaTypeEnum ricercaFascicoloModalita;
	
	/**
	 * Lista risultati fascicoli.
	 */
	private List<FascicoloDTO> ricercaFascicoliResults;
	
	/**
	 * Lista fascicoli firltrati.
	 */
	private List<FascicoloDTO> ricercaFascicoliResultsFiltered;
	/** RICERCA FASCICOLO END. ****************************************************************************/

	/****Tabs Contributi START.****/
	private ContributoInternoTabComponent contributiTab;

	/**
	 * Componenet contributo esterno.
	 */
	private ContributoEsternoTabComponent contributiEsterniTab;
	/****Tabs Contributi END.***/

	private Long idUtenteCoordinatore;

	/**
	 * Visualizza l'esito nella dialog dopo aver effettuato la registrazione del documento.
	 */
	private LinkedHashMap<String, String> messaggioEsitoRegistra;

	/**
	 * Tab trasmissioni.
	 */
	private DocumentManagerDataTabTrasmissioneComponent trasmissioneTab;

	/**
	 * Tab approvazioni.
	 */
	private DocumentManagerDataTabApprovazioniComponent approvazioniTab;

	/**
	 * Flag renderizza faldonatura.
	 */
	private boolean dlgFaldonaturaRendered;

	/**
	 * MEtadati.
	 */
	private Map<Integer, List<MetadatoDinamicoDTO>> mapMetadati;

	/**
	 * Componente faldona.
	 */
	private DialogFaldonaComponent faldonaCmp;

	/**
	 * Service.
	 */
	private ICodaMailFacadeSRV codaMailSRV;

	/**
	 * Lista notifiche PEC.
	 */
	private List<EmailDTO> listaNotifichePEC;

	/**
	 * Lista mail to.
	 */
	private List<EmailDTO> listaMailTO;

	/**
	 * Lista mail cc.
	 */
	private List<EmailDTO> listaMailCC;

	/**
	 * Indirizzo mail.
	 */
	private String indirizzoMailPerReinvia;

	/**
	 * Mail per invia.
	 */
	private EmailDTO selectedMailForReinviaTO;

	/**
	 * Mail per invia.
	 */
	private EmailDTO selectedMailForReinviaCC;

	/**
	 * Tab allegati.
	 */
	private TabAllegatiComponent tabAllegati;

	/**
	 * Componente utilizzato per il tab storico.
	 */
	private DettaglioDocumentoStoricoComponent storico;

	/**
	 * Fascicoli titolario.
	 */
	private FascicoliPerTitolario fascicoliPerTitolario;

	/**
	 * Component preview.
	 */
	private DetailPreviewDocumentManagerComponent previewComponent;

	/**
	 * Dettaglio.
	 */
	private DetailDocumentRedDTO doc;

	/**
	 * Ntofica testo.
	 */
	private String notificaSelectedTesto;

	/**
	 * Header dialog.
	 */
	private String dialogHeader;

	/**
	 * Component dettagli allegati.
	 */
	private DocumentManagerDettaglioVersioniAllegatiComponent versioniAllegato;

	/**
	 * Componente ricerca fascicolo.
	 */
	private DocumentManagerCercaFascicoloComponent cercaFascicoloComponent;

	/**
	 * Lista rds.
	 */
	private List<RdsSiebelDescrizioneDTO> rdsList;

	/**
	 * Dati rds Siebel.
	 */
	private RdsSiebelDescrizioneDTO selectedRds;

	/**
	 * File preview.
	 */
	private FileDTO mainDocForSignFieldPreview;

	/**
	 * Component storico note.
	 */
	private DocumentManagerDataTabStoricoNoteComponent storicoNote;

	/**
	 * Visualizza dettagli file componente.
	 */
	private VisualizzaDettaglioFilesComponent viewFilesPreview;

	/**
	 * Template documento uscita.
	 */
	private TemplateDocUscitaComponent templateDocUscitaComponent;

	/**
	 * Prtocollo.
	 */
	private ProtocolloEmergenzaDocDTO protEm;

	/**
	 * Engine validazione.
	 */
	private DocumentManagerValidatorEngine dmve;

	/**
	 * Lista notifiche.
	 */
	private List<NotificaInteroperabilitaEmailDTO> listaNotificheInteroperabilita;

	/**
	 * Flag disabilita bottone protocollo.
	 */
	private boolean disabilitaButtonProtSemi;

	/**
	 * Lista allegati non sbustati.
	 */
	private List<AllegatoDTO> listAllegatiNonSbustati;
	
	/**
	 * Flag da aggiornare.
	 */
	private boolean daAggiornare;
	
	
	/**
	 * Durante il documento in ingresso,
	 * è possibile usare gli allacci per inserire i documenti presenti nei fascicoli allacciati, come allegati.
	 * 
	 * Quando non è un documento in ingresso questa variabile è null.
	 */
	private AllegatiDaAllacciInitializer allegatiDaAllacciInitializer;

	/**
	 * Flag modifica da shortcut.
	 */
	private boolean inModificaDaShortcut;

	/**
	 * Flag tabella nascosta.
	 */
	private boolean hiddenTable;
	
	/**
	 * Mittente mail protocollata.
	 */
	private String mittenteMailProt;

	/**
	 * Messaggio repertorio.
	 */
	private String msgChkRepertorio;

	/**
	 * Flag estendi visibilità.
	 */
	private boolean isEstendiVisibilitaChk; 

	/**
	 * Lista riferimenti storico.
	 */
	private List<RiferimentoStoricoNsdDTO> riferimentoStoricoNsd;
	
	/**
	 * Service.
	 */
	private IDfFacadeSRV dfSRV;
	
	/**
	 * Contatto verificato.
	 */
	private Contatto contattoVerificaRifStorico;
	
	/**
	 * Lista contatti preferiti.
	 */
	private List<Contatto> contattiPreferiti;
	
	/**
	 * Esito verifica.
	 */
	private EsitoVerificaProtocolloNsdDTO esitoVerificaProtocolloNsdDTO;
	
	/**
	 * Service Rubrica.
	 */
	private IRubricaFacadeSRV rubricaSRV;
	
	/**
	 * Riferimento selezionato.
	 */
	private RiferimentoStoricoNsdDTO selected;  

	/**
	 * Flag scegli contatto.
	 */
	private boolean scegliContattoRifStorico;

	/**
	 * Flag solo creazione contatto.
	 */
	private boolean onlyCreazioneContattoRifStorico;
	
	/**
	 * Flag button stampa etichette.
	 */
	private boolean stampaEtichetteButton;	
	
	/**
	 * Flag chiamante predisponi.
	 */
	private boolean chiamantePredisponi;
	
	/**
	 * Flag mode.
	 */
	private String mode;
	 
	/**
	 * Service stampigliatura.
	 */
	private IStampigliaturaSegnoGraficoFacadeSRV stampigliaturaSiglaSRV;
	
	/**
	 * Nuiovo documento interno.
	 */
	private DetailDocumentRedDTO nuovoDocumentoInterno;
	
	/**
	 * File interno.
	 */
	private transient UploadedFile docInterno;

	/**
	 * Tipololgie documento.
	 */
	private List<TipologiaDocumentoDTO> comboTipologiaDocumento;

	/**
	 * Dettaglio fascicolo.
	 */
	private DetailFascicoloRedDTO detailFasc;

	/**
	 * lista notifiche totale.
	 */
	private List<EmailDTO> listaNotifiche;
	
	/**
	 * lista notifiche filtrate per destinatario.
	 */
	private List<EmailDTO> listaNotificheFiltrate;
	
	/**
	 * Allaccio principale.
	 */
	private RispostaAllaccioDTO allaccioPrincipale;
	
	/**
	 * Model della treeTable.
	 */
	private transient TreeNode rootProtRifResult;
	
	/**
	 * Numero protocollo riferimento.
	 */
	private Integer numProtRicercaRif;
	
	/**
	 * Anno ricerca protocollo riferimento.
	 */
	private Integer annoProtRicercaRif;
	
	/**
	 * Ridsultati protocollo riferimento.
	 */
	private Map<FascicoloDTO, List<DocumentoAllegabileDTO>> risultatiProtRif;
	
	/**
	 * Flag risultato protcollo riferimento.
	 */
	private boolean renderProtRifResult;
	
	/**
	 * Flag render protocollo riferimento.
	 */
	private boolean renderProtRifField;

	/**
	 * Flag integrazione dati.
	 */
	private boolean isForIntegrazioneDati;
	
	/**
	 * Protocollo riferimento.
	 */
	private String protRifFieldValue;
	
	/**
	 * Descrizione assegnatario da automatismo.
	 */
	private String descrAssegnatarioDaAutomatismo;
	
	/**
	 * Assegnatario di default.
	 */
	private AssegnazioneDTO assegnatarioDefault;
	
	/**
	 * Assegnatario da automatismo.
	 */
	private AssegnazioneDTO assegnatarioDaAutomatismo;
	
	/**
	 * Messaggio predisponi.
	 */
	private String msgPredisponi;
	
	/**
	 * Servizio notifica.
	 */
	private INotificaNpsFacadeSRV notificaNpsSRV;
	
	/**
	 * Flag documento elettronico ingressato tramite sistema ausiliario NPS.
	 */
	private boolean elettronicoViaSistemaAusiliarioNPS;
	
	/**
	 * Sistema Ausiliario NPS.
	 */
	private String sistemaAusiliarioNPS;
	
	/**
	 * Contatto vecchio item.
	 */
	private Contatto contattoVecchioItem;

	/**
	 * Lista contatti nel gruppo.
	 */
	private List<Contatto> contattiNelGruppo;
	
	/**
	 * Campo mail otf.
	 */
	private String campoMailOnTheFly;
	
	/**
	 * Flag crea o richiedi creazione.
	 */
	private boolean creaORichiediCreazioneFlag;
	
	/**
	 * Flag gestine otf da protocolla.
	 */
	private boolean gestioneOnTheFlyDaProtocolla;

	/**
	 * Nota richiesta creazione.
	 */
	private String notaRichiestaCreazione;
	
	/**
	 * Nota richiesta mofifica.
	 */
	private String notaRichiestaModifica;
	
	/**
	 * Nota richiesta eliminazione.
	 */
	private String notaRichiestaEliminazione;
	
	/**
	 * Contatto.
	 */
	private Contatto eliminaContattoItem;

	/**
	 * Datatable.
	 */
	private int fromRichiediCreazioneDatatable;
	
	/**
	 * Flag visualizza max content.
	 */
	private boolean showMaxContent;
	
	/**
	 * Indice colonna modificata.
	 */
	private Integer indiceColonnaModificata;
	
	/**
	 * Old fascicolo procedimentale.
	 */
	private String oldIdFascicoloProcedimentale;
	
	/**
	 * Data download.
	 */
	private String dataScarico;

	/**
	 * Protocolli con risposta.
	 */
	private Collection<MasterDocumentRedDTO> ricercaProtRispostaResult;
	
	/**
	 * Protocollo risposta.
	 */
	private MasterDocumentRedDTO protocolloRisposta;
	
	/**
	 * Vecchio oggetto.
	 */
	private String oldOggetto;
	
	/**
	 * Vecchio fascicolo procedimentale.
	 */
	private FascicoloDTO oldFascicoloProc;
	
	/**
	 * Vecchia descrizione assegnatario competenza.
	 */
	private String oldDescrizioneAssegnatarioCompetenza;
	
	/**
	 * Vecchia descrizione tipo doc.
	 */
	private String oldDescTipologiaDocumento;
	
	/**
	 * Definisce la possibilità di scegliere il protocollo risposta in 'modifica'.
	 * É possibile solo in caso in cui i relativi campi sono vuoti in partenza.
	 */
	private boolean protocolloRispostaModificabile;
	
	/**
	 * Anno protocollo risposta.
	 */
	private Integer annoProtRispDialog;
	
	/**
	 * Testo ricerca.
	 */
	private String testoRicercaProtRispDialog;
	
	/**
	 * Nome locker.
	 */
	private String nomeUtenteLockatore;
	
	/**
	 * Flag disabilità risposta protocollo.
	 */
	private boolean disabilitaRispostaProtocollo;
	
	/**
	 * Flag per renderizzare acordion spedizione
	 */
	private boolean renderAcordionDest;

	
	/**
	 * Index acordion dest
	 */
	private String indexAcordionDest;
	
	/**
	 * Map notifiche
	 */
	private Map<String, EmailDTO> notifiche;

	/**
	 * Map iconaSpedizione
	 */
	transient Map<String, List<CodaEmail>> iconaSpedizione;
	
	/**
	 * Map iconaSpedizioneTemp
	 */
	transient Map<String, List<CodaEmail>> iconaSpedizioneTemp;
	
	/**
	 * Riferimento protocolli nps.
	 */
	private List<RiferimentoProtNpsDTO> riferimentoProtNpsDTO;
	
	/**
	 * Service NPS.
	 */
	private INpsFacadeSRV npsSRV;
	
	/**
	 * Notifiche Temp.
	 */
	private Map<String,EmailDTO> notificheTemp;
	
	/**
	 * Root.
	 */
	private DefaultTreeNode root;
	
	private RiferimentoProtNpsDTO riferimentoProtNpsSelected;
	
	/**
	 * @see it.ibm.red.web.mbean.AbstractBean#postConstruct().
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		sessionBean = FacesHelper.getManagedBean(MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();

		documentManagerSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentManagerFacadeSRV.class);
		assegnazioniAutomaticheSRV = ApplicationContextProvider.getApplicationContext().getBean(IAssegnazioneAutomaticaFacadeSRV.class);
		salvaDocumentoSRV = ApplicationContextProvider.getApplicationContext().getBean(ISalvaDocumentoFacadeSRV.class);
		organigrammaSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
		fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);
		documentoRedSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoRedFacadeSRV.class);
		documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);
		ricercaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaFacadeSRV.class);
		metadatiDinamiciSRV = ApplicationContextProvider.getApplicationContext().getBean(IMetadatiDinamiciFacadeSRV.class);
		faldoneSRV = ApplicationContextProvider.getApplicationContext().getBean(IFaldoneFacadeSRV.class);
		signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignFacadeSRV.class);
		detailFascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IDetailFascicoloFacadeSRV.class);
		siebelSRV = ApplicationContextProvider.getApplicationContext().getBean(ISiebelFacadeSRV.class);
		casellePostaliSRV = ApplicationContextProvider.getApplicationContext().getBean(ICasellePostaliFacadeSRV.class);
		registroRepertorioSRV = ApplicationContextProvider.getApplicationContext().getBean(IRegistroRepertorioFacadeSRV.class);
		tipologiaDocumentoSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoFacadeSRV.class);
		allacciaDocUscitaSRV = ApplicationContextProvider.getApplicationContext().getBean(IAllacciaDocUscitaFacadeSRV.class);
		rubricaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaFacadeSRV.class);
		stampigliaturaSiglaSRV = ApplicationContextProvider.getApplicationContext().getBean(IStampigliaturaSegnoGraficoFacadeSRV.class);
		fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloFacadeSRV.class);
		assAutomaticaSRV = ApplicationContextProvider.getApplicationContext().getBean(IAssegnazioneAutomaticaFacadeSRV.class);
		aooSRV = ApplicationContextProvider.getApplicationContext().getBean(IAooSRV.class);
		attoDecretoUCBSRV = ApplicationContextProvider.getApplicationContext().getBean(IAttoDecretoUCBFacadeSRV.class);
		notificaNpsSRV = ApplicationContextProvider.getApplicationContext().getBean(INotificaNpsFacadeSRV.class);
		npsSRV = ApplicationContextProvider.getApplicationContext().getBean(INpsFacadeSRV.class); 
		fadRagioneriaSelected = new FadRagioneriaDTO();

		protEm = new ProtocolloEmergenzaDocDTO();

		ricercaFascicoloAnno = Calendar.getInstance().get(Calendar.YEAR);

		messaggioEsitoRegistra = new LinkedHashMap<>();

		alberaturaNodi = organigrammaSRV.getAlberaturaBottomUp(utente.getIdAoo(), utente.getIdUfficio());

		previewComponent= new DetailPreviewDocumentManagerComponent(utente);

		mapMetadati = new HashMap<>();
		showMaxContent = true;  
		initRicercaPerTitolario();

		versioniAllegato = new DocumentManagerDettaglioVersioniAllegatiComponent(utente);

		cercaFascicoloComponent = new DocumentManagerCercaFascicoloComponent(MBean.DOCUMENT_MANAGER_BEAN, utente);

		templateDocUscitaComponent = new TemplateDocUscitaComponent(utente);
		templateDocUscitaComponent.setTemplateComparabili(new ArrayList<>());

		mode = MODE_ELETTRONICO_DESC;

		PropertiesProvider pp = PropertiesProvider.getIstance();
		int idAooRL = Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.IDAOO_RL));
		if (utente.getIdAoo().intValue() == idAooRL) {
			metadatiDinamiciToCopy = Arrays.asList(pp.getParameterByKey(PropertiesNameEnum.DATA_ARRIVO_DYNAMIC_METAKEY));
		} else {
			metadatiDinamiciToCopy = new ArrayList<>();
		}

		dfSRV = ApplicationContextProvider.getApplicationContext().getBean(IDfFacadeSRV.class);
		riferimentoStoricoSRV = ApplicationContextProvider.getApplicationContext().getBean(IRiferimentoStoricoFacadeSRV.class);
		

		nuovoDocumentoInterno = new DetailDocumentRedDTO();
		
		listaNotificheInteroperabilita = new ArrayList<>();
		listaNotifiche = new ArrayList<>();
		listaNotificheFiltrate = new ArrayList<>();
		
		loadAssegnatarioDefault();
		notaRichiestaCreazione = "";
		notaRichiestaModifica = "";
		notaRichiestaEliminazione = "";

		ricercaProtRispostaResult = new ArrayList<>();
		annoProtRispDialog = Calendar.getInstance().get(Calendar.YEAR);
		
	}
	
	/****METHODS START.*****************************************************************************************/

	public void loadAssegnatarioFromAutomazione() {
		Collection<MetadatoDTO> metadati = detail.getMetadatiEstesi();
		
		// Estraggo il codice del primo capitolo di spesa
		String codiceCapitolo = null;
		if(!CollectionUtils.isEmpty(metadati)) {
			for (MetadatoDTO m : metadati) {
				if (TipoMetadatoEnum.CAPITOLI_SELECTOR.equals(m.getType())) {
					codiceCapitolo = ((CapitoloSpesaMetadatoDTO) m).getCapitoloSelected();
					break;
				}
			}
			
		}

		// Recupero l' assegnatario definito da un'automazione
		AssegnatarioDTO assegnatario = assegnazioniAutomaticheSRV.getAssegnatario(utente.getIdAoo(), Long.valueOf(detail.getIdTipologiaDocumento()), 
				idTipoProcedimentoSelected, codiceCapitolo, detail.getMetadatiEstesi());
		
		AssegnazioneDTO temp = null;
		if (assegnatario != null && assegnatario.getIdUfficio() != null) {
			temp = assegnazioniAutomaticheSRV.getAssegnazioneFromAssegnatario(assegnatario);
			if (temp != null) {
				this.setAssegnatarioDaAutomatismo(temp);
				setDescrAssegnatarioDaAutomatismo(temp.getDescrizioneAssegnatario());
			}
		} else {
			this.setAssegnatarioDaAutomatismo(null);
			setDescrAssegnatarioDaAutomatismo("");
		}
	}

	/**
	 * Imposta l'utente o ufficio che è definito dall'Area Organizzativa Omogenea come default, viene recuperato per l'impostazione automatica
	 * dell'assegnatario tramite la funzionalità predisposta dalla maschera di creazione.
	 */
	public void loadAssegnatarioDefault() {
		this.assegnatarioDefault = null;
		
		AssegnatarioDTO asseg = aooSRV.recuperaAssegnatarioDiDefault(utente.getIdAoo());
		if(asseg.getIdUfficio() != null)
			setAssegnatarioDefault(assegnazioniAutomaticheSRV.getAssegnazioneFromAssegnatario(asseg));
	}

	/**
	 * Imposta l'assegnatario di default dell'Area Organizzativa Omogenea come assegnatario per competenza del documento in fase di creazione.
	 */
	public void setDefault() {
		if(getAssegnatarioDefault() != null) {
			String descrAssegnatarioDefault = getAssegnatarioDefault().getUfficio().getDescrizione();
			if(getAssegnatarioDefault().getUtente() != null) {
				descrAssegnatarioDefault += " - " + getAssegnatarioDefault().getUtente().getDescrizione();
			}
			setAssegnatarioPrincipale(new AssegnazioneDTO(null, TipoAssegnazioneEnum.COMPETENZA, null, null, descrAssegnatarioDefault, getAssegnatarioDefault().getUtente(), getAssegnatarioDefault().getUfficio()));
			setDescrAssegnatarioPerCompetenza(descrAssegnatarioDefault);
		}
	}

	/**
	 * Restituisce la descrizione dell'assegnatario per competenza come utente - ufficio dove non null.
	 */
	public void loadDesc() {
		setDescrAssegnatarioPerCompetenza(descrAssegnatarioPerCompetenza);
	}

	/**
	 * Se esiste almeno un destinatario interno la protocollazione deve essere STANDARD  e disabilitata perché non può essere cambiata.
	 */
	public void checkDestinatariInterni() { 
		documentManagerSRV.checkDestinatariInterni(detail, destinatariInModifica, documentManagerDTO);
	}
	
	/**
	 * @return the assegnatarioPrincipale
	 */
	public AssegnazioneDTO getAssegnatarioPrincipale() {
		return assegnatarioPrincipale;
	}

	/**
	 * @param assegnatarioPrincipale the assegnatarioPrincipale to set
	 */
	public void setAssegnatarioPrincipale(AssegnazioneDTO assegnatarioPrincipale) {
		this.assegnatarioPrincipale = assegnatarioPrincipale;
	}
	
	
	/**
	 * Al momento dell'apertura della maschera setta il dettaglio da mostrare.
	 * @param detail
	 */
	public void setDetail(DetailDocumentRedDTO inDetail) {
		try {
			FascicoloFepaBean fascFepaBean= (FascicoloFepaBean)FacesHelper.getManagedBean(MBean.FASCICOLO_FEPA_BEAN);
			fascFepaBean.init();
			renderAcordionDest = false; 
			this.detail = new DetailDocumentRedDTO(inDetail);
			
			oldIdFascicoloProcedimentale = detail.getIdFascicoloProcedimentale();
			
			protRifFieldValue = detail.getProtRiferimentoToShow();
			
			String pattern = "yyyy-MM-dd HH:mm:ss.SSS";
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			LOGGER.info("START##AnalisiSped - setDetail : " + sdf.format(new Date()) + " - DT : " + this.detail.getDocumentTitle());
			this.documentManagerDTO = documentManagerSRV.prepareMascheraCreazioneOModifica(utente, this.detail, MBean.MAIL_BEAN.equals(chiamante));
			LOGGER.info("END##AnalisiSped - setDetail : " + sdf.format(new Date())  + " - DT : " + this.detail.getDocumentTitle());
			this.dialogHeader = "Gestisci documento";
			if (this.documentManagerDTO.isInCreazioneIngresso()) {
				this.dialogHeader = "Creazione documento in ingresso";
			} else if (this.documentManagerDTO.isInCreazioneUscita()) {
				this.dialogHeader = "Creazione documento in uscita";
			} 
			if(detail.getSottoCategoriaUscita()!=null && detail.getSottoCategoriaUscita().isRegistrazioneAusiliaria() && documentManagerDTO.getComboTipoAssegnazione()!=null) {
				documentManagerDTO.getComboTipoAssegnazione().remove(TipoAssegnazioneEnum.FIRMA_MULTIPLA);
			}
			gestisciDialogHeader();

			//DESTINATARI
			//anche se non serve li pulisco
			int numeroElettronici = 0;
			this.destinatariInModifica = new ArrayList<>();
			if (this.documentManagerDTO.isDestinatariVisible()) {
				if (this.detail.getDestinatari() != null) {
					this.destinatariInModifica = new ArrayList<>(this.detail.getDestinatari());

					for(DestinatarioRedDTO dest : this.destinatariInModifica) {
						//Lista Destinatari nel tab spedizioni TO
						setDestinatariTo(dest);
						//Lista Destinatari nel tab spedizioni CC
						setDestinatariCC(dest);
					}  
					this.documentManagerDTO.setTabSpedizioneDisabled(true);
					idTestoPredefinitoMail = null;
					for (DestinatarioRedDTO d : getDestinatariInModificaPuliti()) {
						d.setMezzoSpedizioneVisible(d.getTipologiaDestinatarioEnum() == TipologiaDestinatarioEnum.ESTERNO);
						d.setMezzoSpedizioneDisabled(d.getMezzoSpedizioneEnum() == MezzoSpedizioneEnum.CARTACEO 
								&& StringUtils.isNullOrEmpty(d.getContatto().getMailSelected()));
						if (d.getContatto() != null && !StringUtils.isNullOrEmpty(d.getContatto().getMailSelected()) &&
								d.getMezzoSpedizioneEnum() == MezzoSpedizioneEnum.ELETTRONICO) {
							numeroElettronici++;
						}
					}
					if (numeroElettronici > 0) {
						this.documentManagerDTO.setTabSpedizioneDisabled(false);
					}
				} else {
					this.destinatariInModifica = new ArrayList<>();
				}

				documentManagerSRV.checkDestinatariInterni(detail, destinatariInModifica, documentManagerDTO);
			} else if (this.documentManagerDTO.isMittenteVisible() && this.detail.getMittenteContatto() == null) { 
				this.detail.setMittenteContattoNew();
			}
			
			// Controllo associazione a protocollo ingressato automaticamente da sistema ausiliario
			checkTipoNotifica();

			//Riferimento ai protocolli in entrata: 
			if (this.detail.getAllacci() != null && !this.detail.getAllacci().isEmpty()) {
				this.riferimentiProt = new ArrayList<>(this.detail.getAllacci());
				for (RispostaAllaccioDTO r : riferimentiProt) {
					if (r.getDocument() != null) {
						if (r.getDocument().getAnnoProtocollo() != null) {
							r.setAnnoProtocollo(r.getDocument().getAnnoProtocollo().intValue());
						}
						if (r.getDocument().getNumeroProtocollo() != null) {
							r.setNumeroProtocollo(r.getDocument().getNumeroProtocollo());
						} else {
							r.setNumeroProtocollo(0);
						}
						if (r.getDocument().getIdProtocollo() != null) {
							r.setIdProtocollo(r.getDocument().getIdProtocollo());
						}
						r.setTipoCategoriaEnum(TipoCategoriaEnum.getById(r.getDocument().getTipoProtocollo()));
						r.setVerificato(true);
					}
				}
			} else {
				this.riferimentiProt = new ArrayList<>();
			}
 
			if (this.detail.getAllaccioRifStorico()!= null && !this.detail.getAllaccioRifStorico().isEmpty()) {
				this.riferimentoStoricoNsd = new ArrayList<>(this.detail.getAllaccioRifStorico());
				for (RiferimentoStoricoNsdDTO r : riferimentoStoricoNsd) {
					if (r.getDocument() != null) {
						if (r.getDocument().getAnnoProtocollo() != null) {
							r.setAnnoProtocolloNsd(r.getDocument().getAnnoProtocollo());
						}
						if (r.getDocument().getNumeroProtocollo() != null) {
							r.setNumeroProtocolloNsd(r.getDocument().getNumeroProtocollo());
						}  
					}
					r.setVerificato(true);
				}
			} else {
				this.riferimentoStoricoNsd = new ArrayList<>();
			} 
			
			if (this.detail.getAllaccioRifNps() != null && !this.detail.getAllaccioRifNps() .isEmpty()) {
				this.riferimentoProtNpsDTO = new ArrayList<>(this.detail.getAllaccioRifNps());
				for (RiferimentoProtNpsDTO r : riferimentoProtNpsDTO) { 
					r.setVerificato(true);
				}
			} else {
				this.riferimentoProtNpsDTO = new ArrayList<>();
			} 
			
			//SE IL FORMATO DOCUMENTO E' VISIBILE ED IL FORMATO NEL DOCUMENTO NON E' STATO SETTATO
			//DI DEFAULT METTO ELETTRONICO
			if (this.detail.getFormatoDocumentoEnum() == null) {
				this.detail.setFormatoDocumentoEnum(FormatoDocumentoEnum.ELETTRONICO);
				this.detail.setIdFormatoDocumento(Integer.valueOf(FormatoDocumentoEnum.ELETTRONICO.getId().intValue()));
				this.idFormatoDocumentoSelected = Long.valueOf(FormatoDocumentoEnum.ELETTRONICO.getId().intValue());
				if (documentManagerDTO.isComboFormatiDocumentoVisible()) {
					String nomeFile = this.detail.getNomeFile();
					String mimeType = this.detail.getMimeType();
					byte[] content = this.detail.getContent();

					//l'onchange resetta sempre il file per non lasciare il dato sporco quando si cambia da elettronico a cartaceo 
					onChangeComboFormatoDocumento();

					//se gli viene passato il file da input lo ri-setto 
					//perché l'onChangeComboFormatoDocumento() lo ha resettato  
					if (!StringUtils.isNullOrEmpty(nomeFile)) {
						this.detail.setNomeFile(nomeFile);
						this.detail.setMimeType(mimeType);
						this.detail.setContent(content);
					}
				}
			}
			
			tabAllegati = new TabAllegatiComponent(documentManagerDTO, this.detail.getAllegati(), this.detail, utente);

			listAllegatiNonSbustati = new ArrayList<>();
			if (CollectionUtils.isNotEmpty(detail.getAllegatiNonSbustati())) {
				for (AllegatoDTO allegatoNonSbustato : this.detail.getAllegatiNonSbustati()) {
					listAllegatiNonSbustati.add(allegatoNonSbustato);
				}
			}
			
			if (documentManagerDTO.isAllegatiModificabili()) {
				allegatiDaAllacciInitializer = new AllegatiDaAllacciInitializer(detail, tabAllegati, utente);
			}

			if (documentManagerDTO.getComboTipologiaDocumento() != null && !documentManagerDTO.getComboTipologiaDocumento().isEmpty()) {

				TipologiaDocumentoDTO tSelected = null;
				// Se non e' stato selezionato seleziono il primo, che e' quello generico
				if (detail.getIdTipologiaDocumento() == null || detail.getIdTipologiaDocumento().intValue() == 0) {
					tSelected = documentManagerDTO.getComboTipologiaDocumento().get(0);
				} else {
					// Altrimenti lo cerco
					for (TipologiaDocumentoDTO tdDTO : documentManagerDTO.getComboTipologiaDocumento()) {
						if (detail.getIdTipologiaDocumento().intValue() == tdDTO.getIdTipologiaDocumento().intValue()) {
							tSelected = tdDTO;
							break;
						}
					}
				}
				
				// Nel caso l'idTipologiaDocumento non sia ancora valorizzato significa che non è stata trovata corrispondenza 
				// con le Tipologie Documento configurate per questa AOO e Tipo Categoria.
				// Si recuperano quindi puntualmente i dati che corrispondono a questa Tipologia Documento.
				if (tSelected == null && detail.getIdTipologiaDocumento() != null) {
					tSelected = tipologiaDocumentoSRV.getById(detail.getIdTipologiaDocumento());
					documentManagerDTO.getComboTipologiaDocumento().add(tSelected);
				}

				detail.setIdTipologiaDocumento(tSelected.getIdTipologiaDocumento());
				detail.setDescTipologiaDocumento(tSelected.getDescrizione());
				idTipologiaDocumentoOriginale = detail.getIdTipologiaDocumento();
				isRepertorioOriginale = detail.getIsModalitaRegistroRepertorio();
				descRegistroOriginale = detail.getDescrizioneRegistroRepertorio();

				// ### onChangeComboTipologiaDocumento() modifica diversi valori del detail 
				// (ID Tipo Procedimento, Numero RDP e Legislatura di PreLex, Metadati Dinamici/Estesi, etc.),
				// che vengono messi da parte prima di richiamarlo, per poi ripristinarli successivamente alla chiamata
				Integer idTipologiaProcedimento = detail.getIdTipologiaProcedimento();
				Integer numeroLegislatura = detail.getNumeroLegislatura();
				String legislatura = detail.getLegislatura();
				Integer numeroRDP = detail.getNumeroRDP();
				// ### onChangeComboTipologiaDocumento() imposta l'insieme di Metadati Dinamici rispetto alla classe documentale,
				// ma in fase di impostazione del dettaglio devo lasciare quelli che mi vengono passati dal dettaglio in input.
				Collection<MetadatoDinamicoDTO> metadatiDinamici = detail.getMetadatiDinamici();
				
				onChangeComboTipologiaDocumento();
				// Metadati estesi del detail, modificati in onChangeComboTipiProcedimento()
				
				// ### Ripristino dei valori cambiati dal trigger dell'onChangeComboTipologiaDocumento -> START
				idTipologiaProcedimentoOriginale = detail.getIdTipologiaProcedimento();
				
				if (!CollectionUtils.isEmpty(metadatiDinamici)) {
					detail.setMetadatiDinamici(metadatiDinamici);
				}
				
				if (idTipologiaProcedimento != null && idTipologiaProcedimento.intValue() > 0) {
					idTipoProcedimentoSelected = Long.valueOf(idTipologiaProcedimento);
					detail.setNumeroLegislatura(numeroLegislatura);
					detail.setLegislatura(legislatura);
					detail.setNumeroRDP(numeroRDP);
					
					onChangeComboTipiProcedimento();

					if(detail.getMetadatiEstesi() != null && inDetail.getMetadatiEstesi() != null) {
						// Ripristino dei metadati estesi propri del detail
						for(MetadatoDTO metadato : detail.getMetadatiEstesi()) {
							for(MetadatoDTO metadatoValorizzato : inDetail.getMetadatiEstesi()) {
								if(metadato.getName().equals(metadatoValorizzato.getName()) && metadato.getType().equals(metadatoValorizzato.getType())) {
									TipoMetadatoEnum type = metadato.getType();
									
									switch (type) {
									case LOOKUP_TABLE:
										((LookupTableDTO)metadato).setLookupValueSelected(((LookupTableDTO)metadatoValorizzato).getLookupValueSelected());
										break;
									case CAPITOLI_SELECTOR:
										((CapitoloSpesaMetadatoDTO)metadato).setCapitoloSelected(((CapitoloSpesaMetadatoDTO)metadatoValorizzato).getCapitoloSelected());
										break;
									case PERSONE_SELECTOR:
										((AnagraficaDipendentiComponentDTO)metadato).setSelectedValues(((AnagraficaDipendentiComponentDTO)metadatoValorizzato).getSelectedValues());
										break;
									default:
										metadato.setSelectedValue(metadatoValorizzato.getValue4AttrExt());
										break;
									}
								}
							}
						}
					}
					impostaMetadatiEstesiDetail(detail.getMetadatiEstesi());
				}
				// ### Ripristino dei valori cambiati dal trigger dell'onChangeComboTipologiaDocumento -> END
			}

			if (documentManagerDTO.isAssegnatarioPerCompetenzaVisible()) {
				creaOrganigrammaAssegnatarioPerCompetenza();

				// Se l'assegnatario per competenza gli viene passato lo cerco e lo setto sull'assegnatario principale
				// svuoto le assegnazioni perche' gli verrano settate nel registra
				if (!CollectionUtils.isEmpty(detail.getAssegnazioni())) {
					for (AssegnazioneDTO a : detail.getAssegnazioni()) {
						if (a.getTipoAssegnazione() == TipoAssegnazioneEnum.COMPETENZA || a.getTipoAssegnazione() == TipoAssegnazioneEnum.STORICO) {
							this.assegnatarioPrincipale = a;
							descrAssegnatarioPerCompetenza = a.getDescrizioneAssegnatario();
							break;
						}
					}
				}
			}

			if (StringUtils.isNullOrEmpty(detail.getPrefixFascicoloProcedimentale())) {
				this.detail.setPrefixFascicoloProcedimentale("NUMERO_ANNO_");
			}

			//l'indice di classificazione non è visibile solo quando il formato è PRECENSITO, formato che non viene più utilizzato
			if (documentManagerDTO.isIndiceDiClassificazioneVisible()) {
				indiceDiClassificazione = new DocumentManagerDataIndiceDiClassificazioneComponent(detail, utente, documentManagerDTO);
			}

			if (documentManagerSRV.isIterDocUscitaVisible(documentManagerDTO.getCategoria())) { //se l'iter approvativo è visibile
				if (!documentManagerDTO.isInModifica() //se sono in creazione
						&& !documentManagerDTO.getComboIterApprovativi().isEmpty()) { //se esiste almeno un iter approvativo
					//di default gli assegno il primo valore
					this.detail.setIdIterApprovativo(documentManagerDTO.getComboIterApprovativi().get(0).getIdIterApprovativo());
				}
				if (this.detail.getIdIterApprovativo() == null) {
					this.detail.setIdIterApprovativo(0);
				}

				Boolean checkFirmaDigitaleRGS = this.detail.getCheckFirmaDigitaleRGS();
				if(checkFirmaDigitaleRGS == null && !StringUtils.isNullOrEmpty(detail.getDocumentTitle())) {
					checkFirmaDigitaleRGS = documentManagerSRV.getFirmaDigitaleRGSFromWF(detail.getDocumentTitle(), utente);
				}
				onChangeComboIterApprovativo(); //resetta il check devo quindi rimetterlo se era flaggato 
				this.detail.setCheckFirmaDigitaleRGS(checkFirmaDigitaleRGS);
				if (this.detail.getCheckFirmaDigitaleRGS() == null || !this.detail.getCheckFirmaDigitaleRGS()) {
					this.detail.setCheckFirmaDigitaleRGS(
							documentManagerSRV.gestioneCheckFirmaDigitaleRGS(detail.getIdTipologiaDocumento(), detail.getIdTipologiaProcedimento()));
				}
			}

			if (this.detail.getAssegnazioni() != null && !this.detail.getAssegnazioni().isEmpty()) {
				for (AssegnazioneDTO a : detail.getAssegnazioni()) {

					this.assegnatariPerContributo = new ArrayList<>();
					this.assegnatariPerConoscenza = new ArrayList<>();

					switch (a.getTipoAssegnazione()) {
					case COMPETENZA: // PRESENTE SIA NELL'ITER MANUALE CHE NEL DOCUMENTO IN ENTRATA
						this.assegnatarioPrincipale = a;
						if (this.detail.getIdIterApprovativo() == null || this.detail.getIdIterApprovativo() == 0) {
							this.detail.setTipoAssegnazioneEnum(TipoAssegnazioneEnum.COMPETENZA);
							//se era null lo metto a 0
							this.detail.setIdIterApprovativo(0);
						}
						this.descrAssegnatarioPerCompetenza = a.getDescrizioneAssegnatario();
						this.descrAssegnatarioIterManuale = a.getDescrizioneAssegnatario();
						break;
					case FIRMA: // PRESENTE SOLO NELL'ITER MANUALE
					case SIGLA: // PRESENTE SOLO NELL'ITER MANUALE
					case RIFIUTO_ASSEGNAZIONE: 
					case RIFIUTO_FIRMA: // PRESENTE SOLO NELL'ITER MANUALE
					case RIFIUTO_SIGLA: // PRESENTE SOLO NELL'ITER MANUALE
					case FIRMA_MULTIPLA: // PRESENTE SOLO NELL'ITER MANUALE
					case RIFIUTO_FIRMA_MULTIPLA:
					case SPEDIZIONE:
						this.assegnatarioPrincipale = a;
						this.descrAssegnatarioIterManuale = a.getDescrizioneAssegnatario();
						this.detail.setTipoAssegnazioneEnum(a.getTipoAssegnazione());
						break;
					case COPIA_CONFORME:
						this.assegnatarioPrincipale = a;
						this.descrAssegnatarioIterManuale = a.getDescrizioneAssegnatario();
						this.detail.setTipoAssegnazioneEnum(a.getTipoAssegnazione());
						List<ResponsabileCopiaConformeDTO> list = documentManagerDTO.getComboResponsabileCopiaConforme();
						ResponsabileCopiaConformeDTO r = documentManagerSRV.selectResponsabileById(a.getUfficio().getId() + " " + a.getUtente().getId(), list);
						this.detail.setResponsabileCopiaConforme(r);
						break;
					case CONTRIBUTO:
						this.assegnatariPerContributo.add(a);
						break;
					case CONOSCENZA:
						this.assegnatariPerConoscenza.add(a);
						break;

					default:
						break;
					}

				}

				if (documentManagerDTO.hasRdsSiebel()) {
					rdsList = siebelSRV.getRdsDescrizione(detail.getDocumentTitle(), utente.getIdAoo());
				}
				
			} 

			if (!documentManagerDTO.isInModifica()) {
				//di default setto la protocollazione standard
				this.detail.setMomentoProtocollazioneEnum(MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD);
			} else {
				this.documentManagerDTO.setAssegnazioneBtnDisabled(true);
			}

			rootOrganigrammaComune = loadRootOrganigrammaComune();

			if (this.allegatiMailSelected != null) {
				this.allegatiMailSelected.clear();
			}
			if ((documentManagerDTO.getCategoria() == CategoriaDocumentoEnum.MOZIONE 
					|| documentManagerDTO.getCategoria() == CategoriaDocumentoEnum.DOCUMENTO_USCITA
					|| (documentManagerDTO.getCategoria() == CategoriaDocumentoEnum.CONTRIBUTO_ESTERNO && documentManagerDTO.isInModificaUscita()))
					&& documentManagerDTO.isInModifica()) {
				updateAllegatiMail();
			}

			if (detail.getMailSpedizione() == null) {
				detail.setMailSpedizione(new EmailDTO());
				this.detail.getMailSpedizione().setTesto("Si trasmette il documento di cui all'oggetto.");
			}

			if (numeroElettronici > 0 && Boolean.FALSE.equals(this.detail.getModificabile()) && this.detail.getMailSpedizione().getMittente() != null) {
				indexAcordionDest = "-0";
				renderAcordionDest = true;
				notifiche = null;
				listaNotifichePEC = null;
				listaMailTO = null;
				listaMailCC = null;
				notificheTemp = null;
				iconaSpedizione = null;
			}
			
			if(this.detail.getAllegati()!=null) {
				
				LOGGER.info("###Allegati start###");
				for(AllegatoDTO all: this.detail.getAllegati()) {
					LOGGER.info("allegato present "+ all.getNomeFile());
					
				}
				LOGGER.info("###Allegati end###");
			}

			if (!StringUtils.isNullOrEmpty(this.detail.getOggetto())) {
				onChangeOggetto();
			}

			this.documentManagerDTO.setRiservato(false);
			if (this.detail.getRiservato() != null && this.detail.getRiservato().intValue() == 1) {
				this.documentManagerDTO.setRiservato(true);
			} 

			if (documentManagerDTO.isInModifica()) {
				contributiTab = new ContributoInternoTabComponent(detail, utente);

				if (documentManagerDTO.isTabContributiEsterniVisible()) {
					contributiEsterniTab = new ContributoEsternoTabComponent(detail, utente);
				}
			}

			/*** TrasmissioneDTO****/
			if (documentManagerDTO.isInModificaIngresso()) {
				trasmissioneTab = new DocumentManagerDataTabTrasmissioneComponent(detail, utente);
			}

			if (documentManagerDTO.isInModificaUscita()) {
				approvazioniTab = new DocumentManagerDataTabApprovazioniComponent(detail, utente);
			}

			onChangeDetailStorico(detail);

			if (PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_CLASSNAME_FN_METAKEY).equals(detail.getDocumentClass())) {
				reloadFADFields(detail);
			} else {
				identificativoRaccolta = null;	
			}

			previewComponent.setDetailPreview(detail);

			if (!StringUtils.isNullOrEmpty(detail.getIdFilePrincipaleDaProtocolla()) || chiamantePredisponi) {
				mainDocForSignFieldPreview = new FileDTO(UUID.randomUUID().toString(), detail.getNomeFile(), detail.getContent(), detail.getMimeType(), false);
			}
			
			if (documentManagerDTO.isInModifica()) {
				versioniAllegato.setDetail(detail);
			}

			// Disabilitazione combo tipologia documento e tipo procedimento -> START
			// - Response "Richiedi Contributo Esterno"
			// - Responses di Predisposizione degli UCB
			if (MBean.RICHIEDI_CONTRIBUTO_ESTERNO_BEAN.equals(chiamante) || isChiamanteOneKindOfPredisponiUCB()) {
				documentManagerDTO.setTipologiaDocumentoDisable(true);
			}
			// Disabilitazione combo tipologia documento e tipo procedimento -> END

			cercaFascicoloComponent.setDetail(detail);
			cercaFascicoloComponent.setDocumentManager(documentManagerDTO);
			cercaFascicoloComponent.setIndiceDiClassificazione(indiceDiClassificazione);

			// Template Doc Uscita: eventuale reset
			if (!StringUtils.isNullOrEmpty(templateDocUscitaComponent.getIdTemplateSelected())) {
				templateDocUscitaComponent.reset();
			}

			if (documentManagerDTO.isInModifica() && sessionBean.haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.MULTI_NOTE)) {
				this.storicoNote = new DocumentManagerDataTabStoricoNoteComponent(utente, detail.getDocumentTitle(), detail.getNumeroDocumento());
			}

			// Disabilitazione tab assegnazioni per documenti in entrata chiusi (senza workflow attivo) o se in creazione uscita
			documentManagerDTO.setTabAssegnazioniDisabled((documentManagerDTO.isInModificaIngresso() && this.detail.getWobNumberSelected() == null) || documentManagerDTO.isInCreazioneUscita());

			// Gestione censimento documenti cartacei - Recogniform -> START
			if (MBean.LISTA_CARTACEI_ACQUISITI_BEAN.equals(chiamante)) {
				documentManagerDTO.setFormatoDocumentoDisabled(true);
			}
			// Gestione censimento documenti cartacei - Recogniform -> END

			// ###### Abilito o disabilito il tasto "Registra" e la modifica del content
			boolean isDocModificabileByAssegnazione = false;
			if (StringUtils.isNullOrEmpty(detail.getDocumentTitle())) {
				detail.setModificabile(true);
				detail.setContentModificabile(true);
			} else {
				LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] DocumentManagerBean.setDetail {" + utente.getUsername() + " - uff: " + utente.getIdUfficio() + "}");
				
				isDocModificabileByAssegnazione = documentoRedSRV.isDocumentoModificabile(detail, utente);
				
				LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] isDocModificabileByAssegnazione " + isDocModificabileByAssegnazione);
				LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] utente.isGestioneApplicativa() " + utente.isGestioneApplicativa());

				detail.setModificabile(!utente.isGestioneApplicativa() && isDocModificabileByAssegnazione && !documentManagerSRV.isDocFirmato(inDetail, documentManagerDTO));
				
				boolean modificaMetadatiMinimi = sessionBean.haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.MODIFICA_METADATI_MINIMI);
				detail.setModificaMetadatiMinimi(modificaMetadatiMinimi);
				
				detail.setContentModificabile(!utente.isGestioneApplicativa() && isDocModificabileByAssegnazione && documentManagerSRV.isDocPrincipaleRimuovibile(utente, detail, documentManagerDTO));
				
				LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] detail.getModificabile " + detail.getModificabile());
				LOGGER.info(DOCCONTENTMODIFICABILE_LABEL + detail.getDocumentTitle() + "] detail.getContentModificabile " + detail.getContentModificabile());
			}
			 
			
			// Valori forzati dall'esterno attraverso il dettaglio in input
			if (inDetail.getModificabile() != null) {
				detail.setModificabile(inDetail.getModificabile());
			}
			if (inDetail.getContentModificabile() != null) {
				detail.setContentModificabile(inDetail.getContentModificabile());
			}
			
			// Se il chiamante è una delle response "Predisponi", si gestisce l'indice di classificazione del fascicolo procedimentale,
			// impostandolo e impedendo la modifica sua e del fascicolo
			if (chiamantePredisponi && !StringUtils.isNullOrEmpty(inDetail.getIndiceClassificazioneFascicoloProcedimentale())
					&& !FilenetStatoFascicoloEnum.NON_CATALOGATO.getNome().equals(inDetail.getIndiceClassificazioneFascicoloProcedimentale())) {
				detail.setIndiceClassificazioneFascicoloProcedimentale(inDetail.getIndiceClassificazioneFascicoloProcedimentale());
				documentManagerDTO.setIndiceDiClassificazioneEFascicoloDisabled(true);
			}else if(chiamantePredisponi) {
				detail.setModificaFascicolo(true);
			}else if (documentManagerDTO.isInModifica()) {
				detail.setInModificaAndNotPredisponi(true);
			}
			
			// Si mette da parte l'allaccio principale
			if (!CollectionUtils.isEmpty(detail.getAllacci())) {
				for (RispostaAllaccioDTO al : detail.getAllacci()) {
					if (al.isPrincipale()) {
						allaccioPrincipale = al;
						break;
					}
				}
			}
			
			mode = MODE_ELETTRONICO_DESC;
			
			detailPreviousVersion = (DetailDocumentRedDTO) FileUtils.deepClone(detail); // Mi salvo la versione precedente del documento anche se siamo in creazione
			
			if(documentManagerDTO.isInCreazioneUscita()) {
				aggiungiDestinatario();
			}
			
			if(documentManagerDTO.isInCreazioneIngresso()) {
				onChangeTipoEmailMittente();
			}
			
			if(documentManagerDTO.isInCreazioneUscita() || documentManagerDTO.isInModificaUscita()) {
				 calcolaMaxSizeContentMittente();
				 if(documentManagerDTO.isInModificaUscita()) {
					 showMaxContent = getTabSpedDisabled();
				 }
			}
			
			//elimino eventuali notifiche caricate con un documento precedente
			listaNotifiche = null;
			
			//per la corretta visualizzazione in formato dd/MM/yyy
			if(inDetail.getDataScarico() != null) {
				setDataScarico(DateUtils.dateToString(inDetail.getDataScarico(), false));
			}
			
			if(documentManagerDTO.isInModifica() 
			   && inDetail.getAnnoProtocolloRisposta() == null && inDetail.getNumeroProtocolloRisposta() == null) {
				setProtocolloRispostaModificabile(true);
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}
	}

	/**
	 * Esegue l'update di {@link #allegatiMailSelected}.
	 */
	private void updateAllegatiMail() {
		try {
			this.detail.setMailSpedizione(documentoRedSRV.getMailByDocumentTitleAndIdAOObyAdmin(this.detail.getDocumentTitle(), utente));
			if (this.detail.getMailSpedizione() != null) {
				//prendo eventuali allegati
				this.allegatiMailSelected = documentoRedSRV.getAllegatiMail(this.detail.getMailSpedizione().getGuid(), utente);
			}
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero della mail", e);
		}
	}

	/**
	 * Questa logica si rende necessaria in fase di modifica del documento:
	 * per popolare con tutti i valori le LookupTable in formato "ComboBox" e far sì che il relativo selectedValue nel detail
	 * venga visualizzato correttamente come valore della ComboBox, oltre a consentire di modificare
	 * il valore selezionato accedendo a tutti i valori della ComboBox stessa.
	 *
	 * @param metadatiEstesiDocumento
	 */
	private void impostaMetadatiEstesiDetail(Collection<MetadatoDTO> metadatiEstesiDocumento) {
		Collection<MetadatoDTO> metadatiEstesiDB = detail.getMetadatiEstesi();
		if (!CollectionUtils.isEmpty(metadatiEstesiDB) && !CollectionUtils.isEmpty(metadatiEstesiDocumento)) {
			// Mappa: nome metadato Lookup Table ComboBox -> valori metadato
			Map<String, List<SelectItemDTO>> mapLookupTableValues = metadatiEstesiDB.stream()
				.filter(LookupTableDTO.class::isInstance).map(LookupTableDTO.class::cast)
				.filter(lookupTable -> LookupTablePresentationModeEnum.COMBOBOX.equals(lookupTable.getLookupTableMode()))
				.collect(Collectors.toMap(LookupTableDTO::getName, LookupTableDTO::getLookupValues));

			if (!MapUtils.isEmpty(mapLookupTableValues)) {
				for (MetadatoDTO m : metadatiEstesiDocumento) {
					if (m instanceof LookupTableDTO && LookupTablePresentationModeEnum.COMBOBOX.equals(((LookupTableDTO) m).getLookupTableMode())) {
						((LookupTableDTO) m).setLookupValues(mapLookupTableValues.get(m.getName()));
					}
				}
			}
		}
		
		if(!CollectionUtils.isEmpty(metadatiEstesiDocumento)) {
			detail.setMetadatiEstesi(metadatiEstesiDocumento);
		}
		
		/**
		 * AUTOCOMPLETE METADATI ESTESI ATTODECRETO UCB
		 */
		setMetadatiEstesiForAttoDecretoUCB();
	}

	/**
	 * Imposta il destinatario come un destinatario in copia conoscenza (CC).
	 * @param dest
	 */
	public void setDestinatariCC(DestinatarioRedDTO dest) {
		if (ModalitaDestinatarioEnum.CC.equals(dest.getModalitaDestinatarioEnum()) && this.detail.getDestinatariCC() != null) {
			for (String destinatariCC : this.detail.getDestinatariCC()) {
				if (dest.getContatto() != null) {
					//if pec
					boolean destinatarioRimosso = false;
					if (dest.getContatto().getMailPec() != null && dest.getContatto().getMailPec().equals(destinatariCC)) {
						dest.getContatto().setMailSelected(dest.getContatto().getMailPec());
						this.detail.getDestinatari().remove(dest);
						destinatarioRimosso = true;
					} else if(dest.getContatto().getMail() != null && dest.getContatto().getMail().equals(destinatariCC)) {
						dest.getContatto().setMailSelected(dest.getContatto().getMail());
						this.detail.getDestinatari().remove(dest);
						destinatarioRimosso = true;
					}
					if (destinatarioRimosso) {
						break;
					}
				}
			}
		}
	}

	/**
	 * Imposta il destinatario come un destinatario in TO.
	 * @param dest
	 */
	public void setDestinatariTo(DestinatarioRedDTO dest) {
		if (ModalitaDestinatarioEnum.TO.equals(dest.getModalitaDestinatarioEnum()) && this.detail.getDestinatariTO() != null) {
			for (String destinatariTO : this.detail.getDestinatariTO()) {
				boolean destinatarioRimosso = false;
				if (destinatariTO.equals(dest.getContatto().getMailPec())) {
					dest.getContatto().setMailSelected(dest.getContatto().getMailPec());
					this.detail.getDestinatari().remove(dest);
					destinatarioRimosso = true;
				} else if (destinatariTO.equals(dest.getContatto().getMail())) {
					dest.getContatto().setMailSelected(dest.getContatto().getMail());
					this.detail.getDestinatari().remove(dest);
					destinatarioRimosso = true;
				} else {
					if (dest.getContatto().getMailPec() != null) {
						dest.getContatto().setMailSelected(dest.getContatto().getMailPec());
					} else {
						dest.getContatto().setMailSelected(dest.getContatto().getMail());
					}
				}
				
				if (destinatarioRimosso) {
					break;
				}
			}
		}
	}

	/**
	 * Aggiornamento del dettaglio sintetico.
	 */
	public void aggiornaDettaglio() {
		aggiornaDlgDettaglioFascicolo();
		if (NavigationTokenEnum.RICERCA_RAPIDA.equals(sessionBean.getActivePageInfo())) {
			RicercaRapidaBean rrb = FacesHelper.getManagedBean(MBean.RICERCA_RAPIDA_BEAN);
			if (rrb.getDocumentiDTH() != null && rrb.getDocumentiDTH().getCurrentMaster() != null) {
				rrb.aggiorna();
				FacesHelper.update("eastSectionForm:tabDetailsMenu");
			}
		} else if (NavigationTokenEnum.RICERCA_DOCUMENTI.equals(sessionBean.getActivePageInfo())) {
			RicercaDocumentiRedBean rdb = FacesHelper.getManagedBean(MBean.RICERCA_DOCUMENTI_RED_BEAN);
			if (rdb.getDocumentiRedDTH() != null && rdb.getDocumentiRedDTH().getCurrentMaster() != null) {
				rdb.aggiorna();
				FacesHelper.update("eastSectionForm:tabDetailsMenu");
			}
		} else if (MBean.LISTA_CARTACEI_ACQUISITI_BEAN.equals(chiamante)) {
			ListaCartaceiAcquisitiBean lcab = FacesHelper.getManagedBean(MBean.LISTA_CARTACEI_ACQUISITI_BEAN);
			if (lcab != null && lcab.getDocumentiDTH() != null && lcab.getDocumentiDTH().getCurrentMaster() != null) {
				lcab.refreshAndUpdateView();
			}
		} else {
			ListaDocumentiBean ldb = FacesHelper.getManagedBean(MBean.LISTA_DOCUMENTI_BEAN);
			if (ldb.getDocumentiDTH() != null && ldb.getDocumentiDTH().getCurrentMaster() != null) {
				ldb.aggiorna();
			}
		}
		
		chiamante = null;
		chiamantePredisponi = false;
	}

	/**
	 * Gestisce l'aggiornamento del dettaglio fascicolo.
	 */
	public void aggiornaDlgDettaglioFascicolo() {
		if (MBean.FASCICOLO_MANAGER_BEAN.equals(chiamante)) {
			FascicoloManagerBean bean = FacesHelper.getManagedBean(MBean.FASCICOLO_MANAGER_BEAN);
			bean.aggiornaTitolario(detail.getIndiceClassificazioneFascicoloProcedimentale());
		}
		mainDocForSignFieldPreview = null;
	}


	private void reloadFADFields(DetailDocumentRedDTO detail) {
		String[] infoFad = documentoSRV.codiceAmministrazioneFromWob(detail.getDocumentTitle(), utente);

		identificativoRaccolta = infoFad[0];
		detail.setIdraccoltaFAD(identificativoRaccolta);	

		codiceAmministrazioneSelected = infoFad[1];
		onChangeComboFadAmministrazione();

		if (documentManagerDTO.isRaccoltaFadVisible() && !StringUtils.isNullOrEmpty(detail.getIdraccoltaFAD())) {
			documentManagerDTO.setTipologiaDocumentoDisable(true);
			documentManagerDTO.setCreaRaccoltaFadDisabled(true);
		}
	}

	/** ORGANIGRAMMA GENERICO START*********************************************************************************************************/

	/**
	 * lo stesso organigramma viene utilizzato sia per gli assegnatari per conoscenza, per contributo che per i destinatari interni.
	 * @return root
	 */
	private DefaultTreeNode loadRootOrganigrammaComune() {
		DefaultTreeNode inRoot = null;
		
		try {
			
			if (CollectionUtils.isEmpty(alberaturaNodi)) {
				throw new RedException (ALBERATURA_NODI_NON_RECUPERATA_CORRETTAMENTE);
			}

			List<Long> alberatura = new ArrayList<>();
			
			for (int i = alberaturaNodi.size() - 1; i >= 0; i--) {
				alberatura.add(alberaturaNodi.get(i).getIdNodo());
			}

			//prendo da DB i nodi di primo livello
			NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
			nodoRadice.setIdAOO(utente.getIdAoo());
			nodoRadice.setIdNodo(utente.getIdNodoRadiceAOO());
			nodoRadice.setCodiceAOO(utente.getCodiceAoo());
			nodoRadice.setUtentiVisible(false);
			List<NodoOrganigrammaDTO> primoLivello = organigrammaSRV.getFigliAlberoAssegnatarioPerConoscenza(utente.getIdUfficio(), nodoRadice, true);

			inRoot = new DefaultTreeNode();
			inRoot.setExpanded(true);
			inRoot.setSelectable(false);
			inRoot.setChildren(new ArrayList<>());
			//per ogni nodo di primo livello creo il nodo da mettere nell'albero che ha come padre rootAssegnazionePerCompetenza
			List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
			inRoot.setChildren(new ArrayList<>());
			DefaultTreeNode nodoToAdd = null;
			for (NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(utente.getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, inRoot);
				nodoToAdd.setExpanded(true);
				if (n.getIdUtente() == null && alberatura.contains(n.getIdNodo())) {
					nodiDaAprire.add(nodoToAdd);
				}
			}

			List<DefaultTreeNode> nodiDaAprireTemp = new ArrayList<>();
			for (int i = 0; i < alberatura.size(); i++) {
				//devo fare esattamente un numero di cicli uguale alla lunghezza dell'alberatura
				nodiDaAprireTemp.clear();
				for (DefaultTreeNode nodo : nodiDaAprire) {
					NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) nodo.getData();
					onOpenTreeComune(nodoExp, nodo);
					nodo.setExpanded(true);
					for (TreeNode nodoFiglio : nodo.getChildren()) {
						NodoOrganigrammaDTO nodoFiglioDto = (NodoOrganigrammaDTO)nodoFiglio.getData();
						if (nodoFiglioDto.getIdUtente() == null && alberatura.contains(nodoFiglioDto.getIdNodo())) {
							nodiDaAprireTemp.add((DefaultTreeNode)nodoFiglio);
						}
					}
				}
				nodiDaAprire = new ArrayList<>(nodiDaAprireTemp);
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

		return inRoot;

	}

	/**
	 * Gestisce l'event "Expand" dei nodi del tree.
	 * @param event
	 */
	public void onOpenTreeComune(NodeExpandEvent event) {
		NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) event.getTreeNode().getData();
		onOpenTreeComune(nodoExp, event.getTreeNode());
	}

	private void onOpenTreeComune(NodoOrganigrammaDTO nodoExp, TreeNode nodoExpTreeNode) {

		try {

			List<NodoOrganigrammaDTO> figli = organigrammaSRV.getFigliAlberoAssegnatarioPerConoscenza(utente.getIdUfficio(), nodoExp, false);

			DefaultTreeNode nodoToAdd = null;
			nodoExpTreeNode.getChildren().clear();
			for (NodoOrganigrammaDTO n : figli) {
				n.setCodiceAOO(utente.getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, nodoExpTreeNode);
				if (n.getFigli() > 0 || n.isUtentiVisible()) {
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
				}
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}


	/** ORGANIGRAMMA GENERICO END*********************************************************************************************************/



	/**FORMATO DOCUMENTO START**************************************/

	/**
	 * Gestisce l'evento di modifica del formato documento.
	 */
	public void onChangeComboFormatoDocumento() {
		try {
			//setto i valori dell'enum e dell'idFormatoDocumento del dettaglio
			detail.setFormatoDocumentoEnum(FormatoDocumentoEnum.getEnumById(this.idFormatoDocumentoSelected));
			detail.setIdFormatoDocumento(Integer.valueOf(this.idFormatoDocumentoSelected.intValue()));

			/**DOC PRINCIPALE ELETTRONICO VISIBILE START.*************************************/
			documentManagerDTO.setDocPrincipalePanelVisible(
					documentManagerSRV.isDocPrincipalePanelVisible(documentManagerDTO.getModalitaMaschera(), 
							detail.getFormatoDocumentoEnum(), documentManagerDTO.getCategoria(), detail.getStorico()));
			//resetto il documento altrimenti potrebbe rimanere settato un documento con formato cartaceo
			this.docPrincipale = null;
			this.detail.setNomeFile(null);
			this.detail.setContent(null);
			this.detail.setMimeType(null);
			/**DOC PRINCIPALE ELETTRONICO VISIBILE END.***************************************/

			documentManagerDTO.setBarcodePanelVisible(documentManagerSRV.isBarcodePanelVisible(
					documentManagerDTO.getModalitaMaschera(), 
					documentManagerDTO.getCategoria(), 
					detail.getFormatoDocumentoEnum()));

			documentManagerDTO.setIndiceDiClassificazioneVisible(
					documentManagerSRV.isIndiceDiClassificazioneVisible(detail.getFormatoDocumentoEnum()));

			documentManagerDTO.setProtocolloMittenteRispostaVisible(
					documentManagerSRV.isProtocolloMittenteRispostaVisible(
							documentManagerDTO.getCategoria(), detail.getFormatoDocumentoEnum()));
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

	}
	/**FORMATO DOCUMENTO END**************************************/

	/**TIPOLOGIA DOCUMENTO START************************************************/	
	/**
	 * Gestisce l'evento di modifica della tipologia documento nella combobox delle tipologie documento.
	 */
	public void onChangeComboTipologiaDocumento() {
		try {
			renderProtRifField = false;
			boolean isAttoDecreto = false;
			this.documentManagerDTO.setAssegnazioneBtnDisabled(false);
			Integer idAttoDecreto = Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_TIPO_DOC_VALUE));
			Integer idTipoProcedimentoAttoDecretoAutomatico = Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_AUTOMATICO_VALUE));
			Integer idTipoProcedimentoAttoDecretoManuale = Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_MANUALE_VALUE));
			for (TipologiaDocumentoDTO t : documentManagerDTO.getComboTipologiaDocumento()) {

				if (t.getIdTipologiaDocumento().intValue() == detail.getIdTipologiaDocumento().intValue()) {

					// IMPOSTO LA DESCRIZIONE DELLA TIPOLOGIA DOCUMENTO
					detail.setDescTipologiaDocumento(t.getDescrizione());
					isAttoDecreto = idAttoDecreto.equals(t.getIdTipologiaDocumento());
					renderProtRifField = activeProtRifField(t.getIdTipologiaDocumento(), detail.getIntegrazioneDati());

					// ### METADATI DINAMICI -> START
					if (mapMetadati == null) {
						mapMetadati = new HashMap<>();
					}
					List<MetadatoDinamicoDTO> metadatiDinamici = mapMetadati.get(t.getIdTipologiaDocumento());
					if (metadatiDinamici == null) {
						metadatiDinamici = metadatiDinamiciSRV.getMetadatyByClasseDocumentale(utente, t.getDocumentClass());
						mapMetadati.put(t.getIdTipologiaDocumento(), metadatiDinamici);
					}
					
					detail.setMetadatiDinamici(metadatiDinamici);
					// ### METADATI DINAMICI -> END

					break;
				}
			}

			// Carico la combo dei Tipi Procedimento dal DB
			Integer idTipoProcedimentoDocumento = null;
			boolean isAttoDecretoAutomatico = false;
			if (documentManagerDTO.isInModifica()) {
				idTipoProcedimentoDocumento = detail.getIdTipologiaProcedimento();
				isAttoDecretoAutomatico = idTipoProcedimentoDocumento.intValue() == idTipoProcedimentoAttoDecretoAutomatico.intValue();
			}
			documentManagerDTO.setComboTipiProcedimento(
					documentManagerSRV.getTipiProcedimentoByTipologiaDocumento(detail.getIdTipologiaDocumento(), idTipoProcedimentoDocumento));

			if (documentManagerDTO.getComboTipiProcedimento() != null 
					&& !documentManagerDTO.getComboTipiProcedimento().isEmpty()) {

				// SSE la tipologia documento è ATTO DECRETO devo togliere il procedimento non coerente con il documento
				if (isAttoDecreto) {
					// Devo lasciare solo quello MANUALE
					int indexToRemove = -1;
					for (int i = 0; i < documentManagerDTO.getComboTipiProcedimento().size(); i++) {
						TipoProcedimento tp = documentManagerDTO.getComboTipiProcedimento().get(i);
						if ((!isAttoDecretoAutomatico && tp.getTipoProcedimentoId() == idTipoProcedimentoAttoDecretoAutomatico.longValue())
								|| (isAttoDecretoAutomatico && tp .getTipoProcedimentoId() == idTipoProcedimentoAttoDecretoManuale.longValue())) {
							indexToRemove = i;
							break;
						}
					}
					if (indexToRemove > -1) {
						documentManagerDTO.getComboTipiProcedimento().remove(indexToRemove);
					}
				}

				TipoProcedimento p = documentManagerDTO.getComboTipiProcedimento().get(0);
				Integer id = (int) p.getTipoProcedimentoId();
				detail.setIdTipologiaProcedimento(id);
				detail.setDescTipoProcedimento(p.getDescrizione());
				idTipoProcedimentoSelected = p.getTipoProcedimentoId();

				onChangeComboTipiProcedimento();
			}
			
			if (StringUtils.isNullOrEmpty(detail.getIdFascicoloProcedimentale())) {
				detail.setDescrizioneFascicoloProcedimentale(documentManagerSRV.componiDescrizioneFascicolo(detail));
			}

			documentManagerDTO.setRaccoltaFadVisible(documentManagerSRV.isRaccoltaFadVisible(detail));

			if (this.documentManagerDTO.isFirmaDigitaleRGSVisible()) {
				this.detail.setCheckFirmaDigitaleRGS(
						documentManagerSRV.gestioneCheckFirmaDigitaleRGS(detail.getIdTipologiaDocumento(), detail.getIdTipologiaProcedimento()));
			}
			 
			if (this.documentManagerDTO.isInCreazioneUscita()) {
				boolean stampigliaturaSiglaVisibile = documentManagerSRV.checkStampigliaturaSiglaVisible(utente.getIdUfficio(), detail.getIdTipologiaDocumento());
				this.documentManagerDTO.setCheckStampigliaturaSiglaVisible(stampigliaturaSiglaVisibile);
				if (tabAllegati != null && tabAllegati.getAllegati() != null) {
					this.tabAllegati.getDocumentManagerDTO().setCheckStampigliaturaSiglaVisible(stampigliaturaSiglaVisibile);
					for (AllegatoDTO allegato : tabAllegati.getAllegati()) {
						if (FormatoAllegatoEnum.ELETTRONICO.equals(allegato.getFormatoSelected())) {
							allegato.setStampigliaturaSigla(stampigliaturaSiglaVisibile);
							allegato.setCheckStampigliaturaSiglaVisible(stampigliaturaSiglaVisibile);
							allegato.setDaFirmareBoolean(stampigliaturaSiglaVisibile);
							allegato.setDaFirmare(stampigliaturaSiglaVisibile?1:0);
						}			
					}
				}
			} else if (this.documentManagerDTO.isInModificaUscita()) {
				boolean stampigliaturaSiglaVisibile = documentManagerSRV.checkStampigliaturaSiglaVisible(utente.getIdUfficio(),detail.getIdTipologiaDocumento());
				this.documentManagerDTO.setCheckStampigliaturaSiglaVisible(stampigliaturaSiglaVisibile);
				if (tabAllegati != null && tabAllegati.getAllegati() != null) {
					this.tabAllegati.getDocumentManagerDTO().setCheckStampigliaturaSiglaVisible(stampigliaturaSiglaVisibile);
					for (AllegatoDTO allegato : tabAllegati.getAllegati()) {
						if (FormatoAllegatoEnum.ELETTRONICO.equals(allegato.getFormatoSelected())) {
							if(allegato.getDocumentTitle()==null) {
								allegato.setStampigliaturaSigla(stampigliaturaSiglaVisibile);
								allegato.setDaFirmareBoolean(stampigliaturaSiglaVisibile);
							}
							allegato.setCheckStampigliaturaSiglaVisible(stampigliaturaSiglaVisibile);
							
						}	
					}
				}
			}

			boolean onlyFirma = false;
			// Se si tratta di un Atto Decreto UCB in uscita, l'unico tipo di iter manuale abilitato deve essere quello di Firma
			if (utente.isUcb() && detail.getIdTipologiaDocumento() != null && detail.getIdTipologiaDocumento().toString().equals(
					PropertiesProvider.getIstance().getParameterByString(
							utente.getCodiceAoo() + "." + PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_USCITA_ATTO_DECRETO.getKey()))) {
				onlyFirma = true;
			}
			// Calcolo dei tipi assegnazione abilitati per l'iter manuale 
			documentManagerDTO.setComboTipoAssegnazione(documentManagerSRV.getComboTipoAssegnazione(detail, documentManagerDTO.isInModifica(), onlyFirma, 
					utente.getIdAoo()));
			
			if(detail.getSottoCategoriaUscita()!=null && detail.getSottoCategoriaUscita().isRegistrazioneAusiliaria() && documentManagerDTO.getComboTipoAssegnazione()!=null) {
				documentManagerDTO.getComboTipoAssegnazione().remove(TipoAssegnazioneEnum.FIRMA_MULTIPLA);
			}
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}
	/**TIPOLOGIA DOCUMENTO END	************************************************/

	/**TIPO PROCEDIMENTO START************************************************/	
	/**
	 * Gestisce l'evento di modifica del tipo procedimento nella combobox delle tipologie procedimento.
	 */
	public void onChangeComboTipiProcedimento() {
		try {
			String tipoGestioneDocumento = null;
			
			documentManagerDTO.setRdpPanelVisible(false);
			detail.setIsModalitaRegistroRepertorio(false);

			detail.setIdTipologiaProcedimento(idTipoProcedimentoSelected.intValue());

			if (idTipologiaProcedimentoOriginale == null) {
				idTipologiaProcedimentoOriginale = idTipoProcedimentoSelected.intValue();
			}

			for (TipoProcedimento p : documentManagerDTO.getComboTipiProcedimento()) {
				if (p.getTipoProcedimentoId() == idTipoProcedimentoSelected.longValue()) {
					tipoGestioneDocumento = p.getTipoGestione();
					detail.setDescTipoProcedimento(p.getDescrizione());
					break;
				}
			}

			documentManagerDTO.setRdpPanelVisible(documentManagerSRV.isRdpPanelVisible(documentManagerDTO.getCategoria(), detail.getDescTipoProcedimento()));

			if (detail.getIdIterApprovativo() != null && detail.getIdIterApprovativo() == 0 //MANUALE
					&& detail.getTipoAssegnazioneEnum() == TipoAssegnazioneEnum.SPEDIZIONE) {
				UfficioDTO uff = null;
				if ("PROVVEDIMENTI NORMATIVI".equalsIgnoreCase(detail.getDescTipologiaDocumento())
						&& "PRELEX".equalsIgnoreCase(detail.getDescTipoProcedimento())) {
					uff = documentManagerSRV.getUfficioUCR(utente.getIdAoo());
					descrAssegnatarioIterManuale = uff.getDescrizione();
					assegnatarioPrincipale = new AssegnazioneDTO(null, TipoAssegnazioneEnum.COMPETENZA, null, null, descrAssegnatarioIterManuale, null, uff);
				} else {
					uff = new UfficioDTO(utente.getIdUfficio(), utente.getUfficioDesc());
					descrAssegnatarioIterManuale = utente.getUfficioDesc() + " - " + utente.getNome() + " " + utente.getCognome();
					assegnatarioPrincipale = new AssegnazioneDTO(null, TipoAssegnazioneEnum.COMPETENZA, null, null, descrAssegnatarioIterManuale, utente, uff);
				}
			}

			if (documentManagerDTO.isRdpPanelVisible()) {
				if (StringUtils.isNullOrEmpty(detail.getLegislatura())) {
					Legislatura legislatura = documentManagerDTO.getUltimaLegislatura();
					detail.setLegislatura(legislatura.getDescrizione());
					detail.setNumeroLegislatura(legislatura.getNumero());
				}
			} else {
				detail.setNumeroRDP(null);
				detail.setLegislatura(null);
				detail.setNumeroLegislatura(null);
			}

			if (this.documentManagerDTO.isFirmaDigitaleRGSVisible()) {
				this.detail.setCheckFirmaDigitaleRGS(
						documentManagerSRV.gestioneCheckFirmaDigitaleRGS(detail.getIdTipologiaDocumento(), detail.getIdTipologiaProcedimento()));
			}

			// Per i Documenti in USCITA In base alla coppia TipoProcedimento/TipologiaDocumento selezionata si determina 
			// se il Documento verrà creato con la modalità Registro di Repertorio.
			if (documentManagerDTO.isInCreazioneUscita() || documentManagerDTO.isInModificaUscita()) {
				checkIsRegistroRepertorio();
				checkDestinatariConformiRR();

				// Controllo che determina se è possibile cambiare la TIPOLOGIA DOCUMENTO O LA TIPOLOGIA PROCEDIMENTO.
				// È possibile cambiarle se i nuovi valori non fanno parte di un Registro Protocollo diverso dall'originale 
				// OPPURE se la descrizione del registro è diversa dall'originale.
				// È inoltre necessario che il documento non abbia già assegnato un numero PROTOCOLLO.
				if ((!idTipologiaDocumentoOriginale.equals(detail.getIdTipologiaDocumento()) 
						|| !idTipologiaProcedimentoOriginale.equals(detail.getIdTipologiaProcedimento())) 
						&& ( (isRepertorioOriginale != null && descRegistroOriginale != null) 
								&& (!isRepertorioOriginale.equals(detail.getIsModalitaRegistroRepertorio()) 
										|| !descRegistroOriginale.equals(detail.getDescrizioneRegistroRepertorio())) ) ) {
					checkProtocolloIsPresent();
				}
			}
			// ### METADATI ESTESI -> START
			setMetadatiEstesi();
			// Se la coppia tipo documento/tipo procedimento prevede una gestione solo automatica (cioè solo da flusso),
			// i metadati estesi diventano non editabili
			documentManagerDTO.setDetailMetadatiEstesiNonEditabili(TipoGestioneEnum.AUTOMATICA.getCode().equals(tipoGestioneDocumento));
			// ### METADATI ESTESI -> END
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}
	
	/**
	 * Recupera e filtra i metadati estesi rispetto a quelli di tipo valuta.
	 * Successivamente, gestisce le triplette relative ai metadati valuta
	 * e ne elabora la lista completa.
	 */
	private void setMetadatiEstesi() {
		Date dataTarget = null;
		if (documentManagerDTO.isInModifica()) {
			dataTarget = detail.getDateCreated();
		}
		List<MetadatoDTO> metadatiEstesiGUI = tipologiaDocumentoSRV.caricaMetadatiEstesiPerGUI(detail.getIdTipologiaDocumento(), detail.getIdTipologiaProcedimento(), 
			!documentManagerDTO.isInModifica(), utente.getIdAoo(), dataTarget);
		detail.setMetadatiEstesi(metadatiEstesiGUI);
		
		List<MetadatoDTO> metadatiValuta = new ArrayList<>();
		for(MetadatoDTO m : metadatiEstesiGUI) {
			if(m.isMetadatoValutaFlag()) {
				metadatiValuta.add(m);
			}
		}
		if(!metadatiValuta.isEmpty()) {
			IValutaFacadeSRV valutaSRV = ApplicationContextProvider.getApplicationContext().getBean(IValutaFacadeSRV.class);
			List<ValutaMetadatiDTO> tripletteMetadatiValuta = valutaSRV.buildListValutaMetadati(metadatiValuta);
			
			detail.setMetadatiValuta(tripletteMetadatiValuta);
			detail.setMapValutaCambio(valutaSRV.getCambiValute(detail.getDateCreated()));
		} else {
			detail.setMetadatiValuta(new ArrayList<>());
		}
		// AUTOCOMPLETE METADATI ESTESI ATTODECRETO UCB
		setMetadatiEstesiForAttoDecretoUCB();
	}

	/* TIPO PROCEDIMENTO END ************************************************/

	private void setMetadatiEstesiForAttoDecretoUCB() {
		try {
			// Recupero dell'id che caratterizza il tipo documento Atto decreto.
			String tipoAttoDecretoUCBUscita = PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "." + PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_USCITA_ATTO_DECRETO.getKey());
			String tipoAttoDecretoUCBEntrata = PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "." + PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_ENTRATA_ATTO_DECRETO.getKey());
			
			if (utente.isUcb() && (tipoAttoDecretoUCBEntrata!=null && tipoAttoDecretoUCBEntrata.equals(detail.getIdTipologiaDocumento().toString()) 
					|| tipoAttoDecretoUCBUscita != null && tipoAttoDecretoUCBUscita.equals(detail.getIdTipologiaDocumento().toString()))) {
				
				// Recupero dei valori configurati per AOO dei metadati estesi di Atto Decreto.
				AttributiEstesiAttoDecretoDTO valueMetadati = attoDecretoUCBSRV.getAttributiEstesi(utente.getIdAoo().intValue());
			
				// Recupero dell'id che caratterizza il tipo documento Atto decreto.
				String metadatoAmministrazione = PropertiesProvider.getIstance().getParameterByString(PropertiesNameEnum.AMMINISTRAZIONE_ATTO_DECRETO_METADATO_ESTESO_NAME.getKey());
				String metadatoRagioneria = PropertiesProvider.getIstance().getParameterByString(PropertiesNameEnum.RAGIONERIA_ATTO_DECRETO_METADATO_ESTESO_NAME.getKey());
				String metadatoUffSegreteria = PropertiesProvider.getIstance().getParameterByString(PropertiesNameEnum.UFFICIO_SEGRETERIA_ATTO_DECRETO_METADATO_ESTESO_NAME.getKey());

				// Dati ricevuti dal Doc in Ingresso  e ribaltati su quello in in Uscita passando per l'INOLTRO.
				String numeroProtocollo = null;
				Date dataProtocollo = null;
				String aooProtocollo = null;
				String metadatoNumeroProtUCB = null;
				String metadatoAooProtUCB = null;
				String metadatoDataProtUCB = null;
				if (ResponsesRedEnum.INOLTRA_INGRESSO.toString().equals(chiamante) ||
						ResponsesRedEnum.RISPONDI_INGRESSO.toString().equals(chiamante)) {
					DetailDocumentRedDTO allaccioIngresso = detail.getAllacci().get(0).getDocument();
					
					dataProtocollo = allaccioIngresso.getDataProtocollo();
					numeroProtocollo = "" + allaccioIngresso.getNumeroProtocollo();
					aooProtocollo = utente.getCodiceAoo();

					metadatoNumeroProtUCB = PropertiesProvider.getIstance().getParameterByString(PropertiesNameEnum.NUM_PROT_ATTO_DECRETO_METADATO_ESTESO_NAME.getKey());
					metadatoDataProtUCB = PropertiesProvider.getIstance().getParameterByString(PropertiesNameEnum.DATA_PROT_ATTO_DECRETO_METADATO_ESTESO_NAME.getKey());
					metadatoAooProtUCB = PropertiesProvider.getIstance().getParameterByString(PropertiesNameEnum.AOO_PROT_ATTO_DECRETO_METADATO_ESTESO_NAME.getKey());
				}
				Collection<MetadatoDTO> metadatiEstesi = detail.getMetadatiEstesi();
				for (MetadatoDTO m : metadatiEstesi) {
					// Valorizzazione dei metadati con i valori recuperati configurati per AOO.
					if (m.getName().equals(metadatoAmministrazione)) {
						m.setSelectedValue(valueMetadati.getCodAmministrazione());
					} else if (m.getName().equals(metadatoRagioneria)) {
						m.setSelectedValue(valueMetadati.getCodRagioneria());
					} else if (m.getName().equals(metadatoUffSegreteria)) {
						m.setSelectedValue(valueMetadati.getUffSegreteria());
					} else if (m.getName().equals(metadatoNumeroProtUCB)) {
						m.setSelectedValue(numeroProtocollo);
					} else if (m.getName().equals(metadatoAooProtUCB)) {
						m.setSelectedValue(aooProtocollo);
					}  else if (m.getName().equals(metadatoDataProtUCB)) {
						m.setSelectedValue(dataProtocollo);
					}
				}
				detail.setMetadatiEstesi(metadatiEstesi);
			}
		} catch (Exception e) {
			LOGGER.error("Errore durante la valorizzazione dei metadati estesi di Atto Decreto", e);
			throw new RedException("Errore durante la valorizzazione dei metadati estesi di Atto Decreto", e);
		}
	}

	/**REGISTRI REPERTORIO START************************************************/	
	/**
	 * Metodo per verificare se i destinatari selezionati siano conformi 
	 * con la modalità Registro Repertorio eventualmente abilitata. 
	 */
	private void checkDestinatariConformiRR() {

		try {
			if (Boolean.TRUE.equals(detail.getIsModalitaRegistroRepertorio())) {

				documentManagerSRV.checkDestinatariInterni(detail, destinatariInModifica, documentManagerDTO);

				for (DestinatarioRedDTO dest : destinatariInModifica) {
					if (TipologiaDestinatarioEnum.ESTERNO.equals(dest.getTipologiaDestinatarioEnum())) {
						msgChkRepertorio = "Attenzione! La tipologia documento e la tipologia procedimento selezionati sono associati al Registro di Repertorio " 
								+ detail.getDescrizioneRegistroRepertorio() 
								+ ": in questa modalità non è possibile associare al procedimento dei destinatari esterni. "
								+ "Confermando l'operazione i destinatari non conformi verranno eliminati. "
								+ "Annullando l'operazione i destinatari verranno mantenuti e la tipologia documento riportata ala tipologia generica.";

						FacesHelper.update("idDettagliEstesiForm:idMsgRepertorio");
						FacesHelper.executeJS("PF('dlgConfirmDestinatariSelection').show()");
						break;
					}
				}

			}
		} catch (Exception e) {
			LOGGER.error("Errore durante il controllo dei Destinatari per il Registro di Repertorio", e);
			showErrorMaschera("Errore durante il controllo dei Destinatari per il Registro di Repertorio: " + e.getMessage());
		}
	}

	private void checkProtocolloIsPresent() {

		try {

			if (detail.getNumeroProtocollo() != null && detail.getNumeroProtocollo() > 0) {	
				msgChkRepertorio = "Il Documento risulta già protocollato, non è quindi possibile impostare una Tipologia Documento che fa riferimento a un Registro Protocollo diverso dall'attuale";
				FacesHelper.update("idDettagliEstesiForm:idMsgRepertorio");
				FacesHelper.executeJS("PF('dlgConfirmDestinatariSelection').show()");
			}

		} catch (Exception e) {
			LOGGER.error("Errore durante il controllo Se il documento è già protocollato", e);
			showErrorMaschera("Errore durante il controllo Se il documento è già protocollato: " + e.getMessage());
		}

	}

	/**
	 * Elimina tutti i destinatari esterni.
	 */
	public void removeDestinatariEsterni() {
		Collection<DestinatarioRedDTO> destToRemove = new ArrayList<>();

		try {

			for (DestinatarioRedDTO dest : destinatariInModifica) {
				if(dest==null) {
					continue;
				}
				if (!TipologiaDestinatarioEnum.INTERNO.equals(dest.getTipologiaDestinatarioEnum())) {
					destToRemove.add(dest);
				}
			}

			destinatariInModifica.removeAll(destToRemove);

			if(documentManagerDTO.isTabSpedizioneVisible()) {
				documentManagerDTO.setTabSpedizioneDisabled(false);
			}
			
		} catch (Exception e) {
			LOGGER.error("Errore durante la rimozione dei Destinatari esterni", e);
			showErrorMaschera("Errore durante la rimozione dei Destinatari esterni: " + e.getMessage());
		}

	}

	private void checkIsRegistroRepertorio() {

		try {

			boolean isRegistroRepertorio = false;
			if(detail.getIdTipologiaDocumento()!= null && detail.getIdTipologiaProcedimento() != null) {
				isRegistroRepertorio = registroRepertorioSRV.checkIsRegistroRepertorio(utente.getIdAoo(), 
						detail.getIdTipologiaDocumento().longValue(), detail.getIdTipologiaProcedimento().longValue(), 
						documentManagerDTO.getRegistriRepertorioConfigured());
			}

			if (isRegistroRepertorio) {

				List<RegistroRepertorioDTO> registriRepertorioList = documentManagerDTO.getRegistriRepertorioConfigured().get(detail.getIdTipologiaProcedimento().longValue());

				if (registriRepertorioList != null && !registriRepertorioList.isEmpty()) {

					for (RegistroRepertorioDTO rr : registriRepertorioList) {
						if (rr.getIdTipologiaDocumento().equals(detail.getIdTipologiaDocumento().longValue())) {  
							detail.setIsModalitaRegistroRepertorio(isRegistroRepertorio);
							detail.setDescrizioneRegistroRepertorio(rr.getDescrizioneRegistroRepertorio());
							break;
						}

					}
				}

			}

		} catch (Exception e) {
			LOGGER.error("Errore durante la verifica del Registro di Repertorio", e);
			throw new RedException("Errore durante la verifica del Registro di Repertorio" + e.getMessage());
		}

	}

	/**
	 * Effettua il ripristino della tipologia del documento.
	 */
	public void restoreTipologiaDocumento() {
		try {
			detail.setIdTipologiaDocumento(documentManagerDTO.getComboTipologiaDocumento().get(0).getIdTipologiaDocumento());
			onChangeComboTipologiaDocumento();

		} catch (Exception e) {
			LOGGER.error("Errore durante il ripristino delle Tipologia documento GENERICA", e);
			showErrorMaschera("Errore durante il ripristino delle Tipologia documento GENERICA: " + e.getMessage());
		}
	}


	/**REGISTRI REPERTORIO END************************************************/

	/**MEZZI RICEZIONE START************************************************/	
	/**
	 * Gestisce l'evento di modifica del mezzo di ricezione nella combobox dei mezzi di ricezione.
	 */
	public void onChangeComboMezzoRicezione() {
		try {
			if (Boolean.FALSE.equals(detail.getModificabile())) {
				return;
			}
			if (documentManagerDTO.getComboMezziRicezione() != null 
					&& !documentManagerDTO.getComboMezziRicezione().isEmpty()) {
				for (MezzoRicezioneDTO item : documentManagerDTO.getComboMezziRicezione()) {
					if (item.getDescrizione().equals(detail.getMezzoRicezioneDescrizione())) {
						detail.setMezzoRicezioneDTO(item);
						detail.setMezzoRicezione(item.getIdMezzoRicezione());
						break;
					}
				}
			} else {
				detail.setMezzoRicezione(null);
				detail.setMezzoRicezioneDescrizione(null);
				detail.setMezzoRicezioneDTO(null);
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

	}
	/**MEZZI RICEZIONE END	************************************************/

	/**DESTINATARI START***********************************************************************/
	/**
	 * Aggiunge destinatario interno
	 * @param event
	 */
	public void onSelectTreeDestinatari(NodeSelectEvent event) {

		try {
			NodoOrganigrammaDTO select = (NodoOrganigrammaDTO) event.getTreeNode().getData();

			for (DestinatarioRedDTO destGiaInserito : getDestinatariInModificaPuliti()) {
				if (select.getIdNodo().equals(destGiaInserito.getIdNodo()) //il nodo esiste sempre 
						&& ((DestinatariRedUtils.is0orNull(select.getIdUtente()) && DestinatariRedUtils.is0orNull(destGiaInserito.getIdUtente())) //significa che l'utente non e' valorizzato
								|| (select.getIdUtente() != null && select.getIdUtente().equals(destGiaInserito.getIdUtente())))
						) {
					showWarningMaschera("Il destinatario e' gia' stato inserito.");
					return;
				}				
			}

			DestinatarioRedDTO destDaInserire = new DestinatarioRedDTO(select.getIdNodo(), select.getIdUtente(), TipologiaDestinatarioEnum.INTERNO, ModalitaDestinatarioEnum.TO);

			String aliasContatto = select.getDescrizioneNodo();
			if (!StringUtils.isNullOrEmpty(select.getCognomeUtente())) {
				aliasContatto += " - " + select.getNomeUtente() + " " + select.getCognomeUtente();
			}
			Contatto c = new Contatto();
			c.setAliasContatto(aliasContatto);
			destDaInserire.setContatto(c);

			c.setNome(select.getNomeUtente());
			c.setCognome(select.getCognomeUtente());


			destDaInserire.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.INTERNO);
			destDaInserire.setMezzoSpedizioneVisible(false);
			destDaInserire.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);

			destinatariInModifica = getDestinatariInModificaPuliti();
			
			if(indiceColonnaModificata==null) {
				this.destinatariInModifica.add(destDaInserire);
			}else {
				this.destinatariInModifica.set(indiceColonnaModificata, destDaInserire);
			}
			

			documentManagerSRV.checkDestinatariInterni(detail, destinatariInModifica, documentManagerDTO);
			if(Boolean.FALSE.equals(detail.getIsModalitaRegistroRepertorio())) {
				aggiungiDestinatario();
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

	}

	/**
	 * Gestisce l'apertura dei preferiti.
	 */
	public void openRubricaPreferiti() {
		openRubricaPreferiti(false,false);
	}

	/**
	 * Consente di visualizzare la finestra di gestione dei preferiti.
	 * @param isDestInterno
	 * @param isDestEsterno
	 */
	public void openRubricaPreferiti(boolean isDestInterno,boolean isDestEsterno) {
		scegliContattoRifStorico = false;
		fromRichiediCreazioneDatatable = -1;
		
		onlyCreazioneContattoRifStorico = false;
		for (Contatto contatto : this.documentManagerDTO.getContattiPreferiti()) {
			contatto.setDisabilitaElemento(false);
		}
		filtraContattiPreferiti();
		DataTable dt = getDataTableByIdComponent(CONTATTIPREFERITITBL_RESOURCE_NAME);
		if (dt != null) {
			dt.reset();
		}
		// Se la modalità Registro Repertorio è attiva viene impostato 
		// il tab dei preferiti come attivo.
		if (Boolean.TRUE.equals(detail.getIsModalitaRegistroRepertorio())) {
			TabView tb = null;
			tb = (TabView) FacesContext.getCurrentInstance().getViewRoot().findComponent(PREFERITOTABV_RESOURCE_NAME);
			tb.setActiveIndex(1);
		}

		if(isDestInterno) {
			TabView tb = null;
			tb = (TabView) FacesContext.getCurrentInstance().getViewRoot().findComponent(PREFERITOTABV_RESOURCE_NAME);
			tb.setActiveIndex(1);
			FacesHelper.executeJS("PF('wdgCercaPreferito').show();");
			FacesHelper.update(PREFERITOTABV_RESOURCE_NAME);
		}
		if(isDestEsterno) {
			TabView tb = null;
			tb = (TabView) FacesContext.getCurrentInstance().getViewRoot().findComponent(PREFERITOTABV_RESOURCE_NAME);
			tb.setActiveIndex(2);
			FacesHelper.executeJS("PF('wdgCercaPreferito').show();");
			FacesHelper.update(PREFERITOTABV_RESOURCE_NAME);
		}
		
		//Questo metodo non serve più solo ai preferiti, 
		//nella dialog ci sono altri tab che vanno resettati.
		documentManagerDTO.setResultRicercaContatti(null);
		documentManagerDTO.setResultRicercaContattiFiltered(null);
		documentManagerDTO.setInserisciContattoItem(new Contatto());
		inizializzaCheckboxRubrica();
	}

	/**
	 * Filtra tutti i contatti preferiti scartando i gruppi.
	 */
	public void filtraContattiPreferiti() {
		try {
			documentManagerDTO.setContattiPreferitiFiltered(new ArrayList<>());
			for (Contatto contatto : this.documentManagerDTO.getContattiPreferiti()) {
				contatto.setDisabilitaElemento(false);
				for (DestinatarioRedDTO d : getDestinatariInModificaPuliti()) {
					//nella lista da mostrare bisogna mostrare solo gli utenti e non i gruppi
					//nascondendo gli utenti già inseriti
					
					if (!TipoRubricaEnum.GRUPPO.equals(d.getContatto().getTipoRubricaEnum()) 
							&& ((d.getContatto().getContattoID() == null && contatto.getContattoID() == null)
							|| (d.getContatto().getContattoID() != null 
							&& d.getContatto().getContattoID().equals(contatto.getContattoID())))) {
						
						contatto.setDisabilitaElemento(true);
						break;
					}
				}
				documentManagerDTO.getContattiPreferitiFiltered().add(contatto);
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Aggiunge un destintario identificato dall'idContatto in ingresso alla lista dei destintari esterni.
	 * @param idContatto
	 */
	public void addDestinatarioEsterno(Long idContatto) {

		try {
			this.destinatariInModifica = getDestinatariInModificaPuliti();

			for (DestinatarioRedDTO d : destinatariInModifica) {
				if (idContatto != null && d.getContatto() != null && idContatto.equals(d.getContatto().getContattoID())) {
					showWarningMaschera("Il contatto è già stato inserito tra i destinatari!");
					return;
				}
			}
			Contatto selezionato = null;
			for (Contatto pref : this.documentManagerDTO.getContattiPreferiti()) {
				if (pref.getContattoID().equals(idContatto)) {
					selezionato = pref;
					setTipologiaIndirizzoEmail(selezionato);
					if(selezionato.getTipoRubrica()!=null && "RED".equals(selezionato.getTipoRubrica())) {
						creaORichiediCreazioneFlag = true;
					}
					break;
				}
			}
			if (selezionato == null) {
				showWarningMaschera("Il contatto non è stato trovato!");
				return;
			}

			//Serve per disabilitare il tasto aggiungi, altrimenti può essere cliccato più volte
			selezionato.setDisabilitaElemento(true);

			DestinatarioRedDTO d = new DestinatarioRedDTO();

			d.setContatto(selezionato);
			if (StringUtils.isNullOrEmpty(selezionato.getMailSelected())) {
				d.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.CARTACEO);
				d.setMezzoSpedizioneDisabled(true);
			} else {
				d.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.ELETTRONICO);
				d.setMezzoSpedizioneDisabled(false);
			}

			d.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
			d.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
			d.setMezzoSpedizioneVisible(true);
			if(indiceColonnaModificata==null) {
				this.destinatariInModifica.add(d);
			}else {
				this.destinatariInModifica.set(indiceColonnaModificata, d);
			}
			
			if(scegliContattoRifStorico) {
				FacesHelper.executeJS(HIDE_WDGCERCAPREFERITO_JS);
				scegliContattoRifStorico = false;
				onlyCreazioneContattoRifStorico=false;
			}

			aggiungiDestinatario();
			if(MezzoSpedizioneEnum.ELETTRONICO.equals(d.getMezzoSpedizioneEnum()) && StringUtils.isNullOrEmpty(this.detail.getMailSpedizione().getOggetto()) && 
					!StringUtils.isNullOrEmpty(this.detail.getOggetto()) ) {
				this.detail.getMailSpedizione().setOggetto(this.detail.getOggetto());
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

	}

	/**
	 * Imposta il destinatario selezionato come quello da visualizzare.
	 * Il destinatario selezionato è identificato dall'index in ingresso.
	 * @param index
	 */
	public void selectDestinatarioDaVisualizzare(Object index) {

		this.contattoDaVisualizzare = null;

		try {
			Integer i =  (Integer) index;
			//qua il metodo non è necessario in quanto rischio di sbagliare il conto degli indici
			this.contattoDaVisualizzare = this.destinatariInModifica.get(i.intValue()).getContatto();

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

	}

	/**
	 * Elimina il destinatario dalla lista dei destinatari selezionati.
	 * @param index
	 */
	public void eliminaDestinatario(Object index) {

		try {
			Integer i =  (Integer) index;
			DestinatarioRedDTO dRemoved = this.destinatariInModifica.remove(i.intValue());
			if (dRemoved.getContatto() != null) {
				for (Contatto pref : documentManagerDTO.getContattiPreferiti()) {
					if (pref.getContattoID().equals(dRemoved.getContatto().getContattoID())) {
						// Riattivo tra i preferiti il contatto da aggiungere.
						pref.setDisabilitaElemento(false);
						break;
					}

				}
			}
			documentManagerSRV.checkDestinatariInterni(detail, destinatariInModifica, documentManagerDTO);

			updateMaxSizeValue();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

	}

	//Questo metodo deve essere richiamato per aggiornare la dimensione max degli allegati
	//In quanto non sempre l'ordine degli update aggiorna prima il tab spedizione
	private void updateMaxSizeValue() {
		if(this.destinatariInModifica==null) {
			this.documentManagerDTO.setTabSpedizioneDisabled(true);
		} else {
			for(DestinatarioRedDTO dest : this.destinatariInModifica) {
				//Se trovo almeno un dest elettronico setto il tab spedizione abilitato
				this.documentManagerDTO.setTabSpedizioneDisabled(true);
				if(MezzoSpedizioneEnum.ELETTRONICO.equals(dest.getMezzoSpedizioneEnum())) {
					this.documentManagerDTO.setTabSpedizioneDisabled(false);
					break;
				}
			}
		}
	}

	/**
	 * @see it.ibm.red.web.mbean.master.IRubricaHandler#restituisciRisultati(java.util.Collection,
	 *      boolean, it.ibm.red.business.enums.RubricaChiamanteEnum).
	 */
	@Override
	public boolean restituisciRisultati(Collection<Contatto> collection, boolean salva, RubricaChiamanteEnum r) {

		documentManagerDTO.setContattiPreferiti(documentManagerSRV.getContattiPreferitiPerComboDestinatario(utente, detail));

		switch (r) {
		case DOCUMENTMANAGERBEAN_DESTINATARI:
			aggiornaDestinatari(collection, salva);
			break;

		case DOCUMENTMANAGERBEAN_MITTENTE:
			aggiornaMittente(collection, salva);
			break;

		default:
			break;
		}

		FacesHelper.executeJS(HIDE_WDGSELEZIONADARUBRICA_JS);
		
		return true;
	}

	/**
	 * Aggiorna la lista dei destinatari impostando i parametri in base alle caratteristiche.
	 * @param collection
	 * @param salva
	 */
	public void aggiornaDestinatari(Collection<Contatto> collection, boolean salva) {
		if (salva) {
			List<DestinatarioRedDTO> newList = new ArrayList<>();

			for (Contatto c : collection) {
				DestinatarioRedDTO destToAdd = null;
				for (DestinatarioRedDTO d : getDestinatariInModificaPuliti()) {
					if ((c.getContattoID().equals(d.getContatto().getContattoID()) && c.getContattoID() != null)
							|| (StringUtils.isNullOrEmpty(c.getAliasContatto()) && StringUtils.isNullOrEmpty(d.getContatto().getAliasContatto()))
							|| (!StringUtils.isNullOrEmpty(c.getAliasContatto()) && c.getAliasContatto().equals(d.getContatto().getAliasContatto()))) {
						d.setContatto(c);
						destToAdd = d;
						break;
					}
				}

				if (destToAdd == null) { //significa che è un nuovo destinatario da aggiungere
					destToAdd = new DestinatarioRedDTO();
					destToAdd.setContatto(c);


					if (!StringUtils.isNullOrEmpty(c.getMailPec())) {
						c.setMailSelected(c.getMailPec());
						c.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEC);
					} else {
						c.setMailSelected(c.getMail());
						c.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEO);
					}


					destToAdd.setMezzoSpedizioneVisible(true);
					if (StringUtils.isNullOrEmpty(c.getMailSelected())) {
						destToAdd.setMezzoSpedizioneDisabled(true);
						destToAdd.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.CARTACEO);
					} else {
						destToAdd.setMezzoSpedizioneDisabled(false);
						destToAdd.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.ELETTRONICO);
					}

					if (c.getTipoRubricaEnum() == TipoRubricaEnum.INTERNO 
							|| c.getContattoID() == null || c.getContattoID().longValue() == 0) {
						destToAdd.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.INTERNO);
						destToAdd.setMezzoSpedizioneVisible(false);
					} else {
						destToAdd.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
					}

					//lo devo mettere anche per i destinatari interni o tipologia cartaceo
					destToAdd.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);

					destToAdd.setIdNodo(c.getIdNodo());
					destToAdd.setIdUtente(c.getIdUtente());
				}

				if (destToAdd.getMezzoSpedizioneEnum() == null) {
					destToAdd.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.CARTACEO);
				}
				newList.add(destToAdd);
			}

			this.destinatariInModifica = newList;

			documentManagerSRV.checkDestinatariInterni(detail, destinatariInModifica, documentManagerDTO);
		}

		FacesHelper.executeJS(HIDE_WDGSELEZIONADARUBRICA_JS);
	}

	/**
	 * @see it.ibm.red.web.mbean.master.IRubricaHandler#aggiornaComponentiInterfaccia(it.ibm.red.business.enums.RubricaChiamanteEnum).
	 */
	@Override
	public void aggiornaComponentiInterfaccia(RubricaChiamanteEnum r) {

		switch (r) {
		case DOCUMENTMANAGERBEAN_DESTINATARI: 
			FacesHelper.update(IDDESTINATARIDMD_RESOURCE_LOCATION);
			FacesHelper.update(IDTABSPEDIZIONECONTAINERDMD_RESOURCE_NAME);
			FacesHelper.update(IDTABSDMD_RESOURCE_NAME);
			break;
		case DOCUMENTMANAGERBEAN_MITTENTE:
			String formName = "idDettagliEstesiForm";
			if (MBean.MAIL_BEAN.equalsIgnoreCase(chiamante)) {
				formName = EAST_SECTION_FORM_NAME;
			}
			FacesHelper.update(formName + ":idMittentePnl_DMD");
			break;
		default:
			break;
		}

	}

	/**
	 * Gestisce l'evento di modifica del destinatario.
	 * @return true se il tab di spedizione è disabilitato, false altrimenti
	 */
	public boolean onChangeDestinatarioMezzoSpedizione()  {
		boolean disabled = true;
		try {

			if (this.documentManagerDTO.isTabSpedizioneVisible()) {			
				this.documentManagerDTO.setTabSpedizioneDisabled(true);
				idTestoPredefinitoMail = null;

				//basta un destinatario che abbia il mezzo di spedizione elettronico ed il tab spedizione va attivato
				for (DestinatarioRedDTO d : getDestinatariInModificaPuliti()) {
					if (d.getMezzoSpedizioneEnum() == MezzoSpedizioneEnum.ELETTRONICO) {
						this.documentManagerDTO.setTabSpedizioneDisabled(false);
						d.getContatto().setCampoEmailVisible(true);
						break;
					}
				}

				disabled = documentManagerDTO.isTabSpedizioneDisabled();
			}
		} catch (Exception e) {
			disabled = true;
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

		return disabled;
	}

	/**DESTINATARI END*************************************************************************/	

	/**OGGETTO START******************************************************************************************/
	/**
	 * Gestisce l'evento di modifica dell'oggetto.
	 */
	public void onChangeOggetto() {
		try {
			if (!this.documentManagerDTO.isInModifica() 
					&& StringUtils.isNullOrEmpty(this.detail.getIdFascicoloProcedimentale())) {
				detail.setDescrizioneFascicoloProcedimentale(documentManagerSRV.componiDescrizioneFascicolo(detail));
			}
			this.detail.getMailSpedizione().setOggetto(this.detail.getOggetto());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}
	/**OGGETTO END********************************************************************************************/	

	/**ITER APPROVATIVO START*****************************************************************************************/
	/**
	 * Gestisce l'evento di modifica dell'iter approvativo dalla combobox.
	 */
	public void onChangeComboIterApprovativo() {
		try {
			//di default questi valori sono a false
			this.documentManagerDTO.setFirmaDigitaleRGSVisible(false);
			this.detail.setCheckFirmaDigitaleRGS(false);
			this.documentManagerDTO.setComboTipoAssegnazioneVisible(false);
			this.documentManagerDTO.setAssegnatarioOrganigrammaVisible(false);
			this.documentManagerDTO.setComboCoordinatoriVisible(false);
			this.descrAssegnatarioIterManuale = null;
			this.assegnatarioPrincipale = null;

			if (this.detail.getIdIterApprovativo() == null || this.detail.getIdIterApprovativo() == 0) { //ITER MANUALE
				this.documentManagerDTO.setComboTipoAssegnazioneVisible(true);
				this.documentManagerDTO.setAssegnatarioOrganigrammaVisible(true);
				this.iterApprovativoInfo = ITER_MANUALE_DESC;
			} else {
				//ITER AUTOMATICO

				for (IterApprovativoDTO it : this.documentManagerDTO.getComboIterApprovativi()) {
					if (it.getIdIterApprovativo().equals(detail.getIdIterApprovativo())) {
						this.detail.setIterApprovativoSemaforo(it.getDescrizione());
						this.iterApprovativoInfo = it.getInfo();
						break;
					}
				}

				//FIRMA DIGITALE RGS
				List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
				assegnazioni.add(assegnatarioPrincipale);
				if (documentManagerSRV.isCasellaTabSpedizioniConfigurata(utente, documentManagerDTO, detail.getIdIterApprovativo(), detail.getTipoAssegnazioneEnum(), assegnazioni)) {
					this.documentManagerDTO.setFirmaDigitaleRGSVisible(true);
					this.detail.setCheckFirmaDigitaleRGS(documentManagerSRV.gestioneCheckFirmaDigitaleRGS(detail.getIdTipologiaDocumento(), detail.getIdTipologiaProcedimento()));

					documentManagerDTO.setComboCaselleSpedizione(new ArrayList<>());
					CasellaPostaDTO c = new CasellaPostaDTO();
					c.setIndirizzoEmail(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MAIL_COORDINAMENTO));
					documentManagerDTO.getComboCaselleSpedizione().add(c);
				} else {
					documentManagerDTO.setComboCaselleSpedizione(documentManagerDTO.getCaselleSpedizioneByUser());
				}

				//COMBO COORDINATORI START.
				//LA COMBO COORDINATORI E' CALCOLATA NEL SERVICE SE IL DOCUMENTO E' IN USCITA
				documentManagerDTO.setComboCoordinatoriVisible(
						documentManagerSRV.comboCoordinatoriVisible(detail.getIdIterApprovativo(), this.documentManagerDTO.getComboCoordinatori()));
				if (documentManagerDTO.isComboCoordinatoriVisible()) {
					if (this.detail.getIdUtenteCoordinatore() != null && this.detail.getIdUtenteCoordinatore().longValue() > 0) {
						this.idUtenteCoordinatore = this.detail.getIdUtenteCoordinatore(); 
					}
				} else {
					this.idUtenteCoordinatore = null;
					this.detail.setIdNodoCoordinatore(null);
					this.detail.setIdUtenteCoordinatore(null);
				}
				//COMBO COORDINATORI END.

			}

			this.documentManagerDTO.setTabAssegnazioniDisabled(documentManagerSRV.isTabAssegnazioniDisabled(detail.getIdIterApprovativo(), 
							CategoriaDocumentoEnum.getExactly(detail.getIdCategoriaDocumento())));
			if (this.documentManagerDTO.isTabAssegnazioniDisabled()) {
				this.assegnatariPerConoscenza = new ArrayList<>();
				this.assegnatariPerContributo = new ArrayList<>();
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

	}
	/**ITER APPROVATIVO END*******************************************************************************************/

	/** COMBO COORDINATORI START **************************************************************************************************************/
	/**
	 * Gestisce l'evento di modifica del coordinatore.
	 */
	public void onChangeComboCoordinatori() {

		try {
			if (this.idUtenteCoordinatore == null || this.idUtenteCoordinatore.intValue() == 0) {
				detail.setIdNodoCoordinatore(null);
				detail.setIdUtenteCoordinatore(null);
				return;
			}
			for (NodoUtenteCoordinatore n : this.documentManagerDTO.getComboCoordinatori()) {
				if (n.getIdUtenteCoordinatore().longValue() == this.idUtenteCoordinatore.longValue()) {
					detail.setIdNodoCoordinatore(n.getIdUfficioCoordinatore());
					detail.setIdUtenteCoordinatore(n.getIdUtenteCoordinatore());
					break;
				}
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}
	/** COMBO COORDINATORI END ****************************************************************************************************************/

	/**TIPO ASSEGNAZIONE START*******************************************************************************************/
	public void onChangeComboTipoAssegnazione() {
		try {
			this.documentManagerDTO.setAssegnatarioOrganigrammaVisible(false);
			this.documentManagerDTO.setComboResponsabileCopiaConformeVisible(false);
			this.detail.setResponsabileCopiaConforme(null);
			this.documentManagerDTO.setComboMomentoProtocollazioneDisabled(false);

			this.documentManagerDTO.setCheckFirmaConCopiaConformeVisible(documentManagerSRV.isFlagFirmaCopiaConformeVisible(utente, detail.getTipoAssegnazioneEnum()));

			this.documentManagerDTO.setAssegnazioneBtnDisabled(false);
			this.descrAssegnatarioIterManuale = null;
			this.assegnatarioPrincipale = null;

			switch (detail.getTipoAssegnazioneEnum()) {
			case COPIA_CONFORME:
				
				handleCopiaConformeDetail();
				break;
			case SPEDIZIONE:
				
				handleSpedizioneDetail();
				break;
			default:
				this.documentManagerDTO.setAssegnatarioOrganigrammaVisible(true);
				break;
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Gestisce il dettaglio quando il tipo assegnazione del documento è
	 * SPEDIZIONE.
	 */
	private void handleSpedizioneDetail() {
		UfficioDTO uff = null;
		this.documentManagerDTO.setAssegnatarioOrganigrammaVisible(true);
		this.detail.setMomentoProtocollazioneEnum(MomentoProtocollazioneEnum.PROTOCOLLAZIONE_NON_STANDARD);
		this.documentManagerDTO.setComboMomentoProtocollazioneDisabled(true);
		this.documentManagerDTO.setAssegnazioneBtnDisabled(true);

		if ("PROVVEDIMENTI NORMATIVI".equalsIgnoreCase(detail.getDescTipologiaDocumento())
				&& "PRELEX".equalsIgnoreCase(detail.getDescTipoProcedimento())) {
			uff = documentManagerSRV.getUfficioUCR(utente.getIdAoo());
			descrAssegnatarioIterManuale = uff.getDescrizione();
			assegnatarioPrincipale = new AssegnazioneDTO(null, TipoAssegnazioneEnum.COMPETENZA, null, null, descrAssegnatarioIterManuale, null, uff);
		} else {
			uff = new UfficioDTO(utente.getIdUfficio(), utente.getUfficioDesc());
			descrAssegnatarioIterManuale = utente.getUfficioDesc() + " - " + utente.getNome() + " " + utente.getCognome();
			assegnatarioPrincipale = new AssegnazioneDTO(null, TipoAssegnazioneEnum.COMPETENZA, null, null, descrAssegnatarioIterManuale, utente, uff);
		}

		// Si rimuove il documento principale inserito (se l'AOO è abilitato, potrebbe essere un documento già firmato)
		rimuoviFile();
	}

	/**
	 * Gestisce il dettaglio quando il tipo assegnazione del documento è
	 * COPIA_CONFORME.
	 */
	private void handleCopiaConformeDetail() {
		this.documentManagerDTO.setComboResponsabileCopiaConformeVisible(true);

		// Se esistono responsabili seleziono il primo altrimenti ne metto uno vuoto per non sollevare NPE
		ResponsabileCopiaConformeDTO select = null;
		
		if (!this.documentManagerDTO.getComboResponsabileCopiaConforme().isEmpty()) {
			select = this.documentManagerDTO.getComboResponsabileCopiaConforme().get(0);
			// Si triggera la selezione di un elemento della combo del Responsabile Copia Conforme
			setAssegnatarioCopiaConforme(select);
		} else {
			select = new ResponsabileCopiaConformeDTO();
		}
		this.detail.setResponsabileCopiaConforme(select);
	}

	/**
	 * Gestisce la creazione dell'alberatura per la selezione degli assegnatari.
	 */
	public void loadTreeAssegnazioneIterManuale() {
		
		try {

			if (CollectionUtils.isEmpty(alberaturaNodi)) {
				throw new RedException (ALBERATURA_NODI_NON_RECUPERATA_CORRETTAMENTE);
			}
			
			List<Long> alberatura = new ArrayList<>();
			for (int i = alberaturaNodi.size() - 1; i >= 0; i--) {
				alberatura.add(alberaturaNodi.get(i).getIdNodo());
			}

			//prendo da DB i nodi di primo livello
			NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
			nodoRadice.setIdAOO(utente.getIdAoo());
			nodoRadice.setIdNodo(utente.getIdNodoRadiceAOO());
			nodoRadice.setCodiceAOO(utente.getCodiceAoo());
			nodoRadice.setUtentiVisible(false);
			List<NodoOrganigrammaDTO> primoLivello = null;

			switch (detail.getTipoAssegnazioneEnum()) {
			case COMPETENZA:
				primoLivello = organigrammaSRV.getFigliAlberoAssegnatarioPerCompetenzaUscita(utente, nodoRadice);
				break;

			case FIRMA:
			case FIRMA_MULTIPLA:
				nodoRadice.setUtentiVisible(true);
				primoLivello = organigrammaSRV.getFigliAlberoAssegnatarioPerFirma(utente.getIdUfficio(), nodoRadice);
				break;

			case SIGLA:
				nodoRadice.setUtentiVisible(true);
				primoLivello = organigrammaSRV.getFigliAlberoAssegnatarioPerSigla(utente.getIdUfficio(), nodoRadice);
				break;

			default:
				break;
			}

			rootAssegnazioneIterManuale = new DefaultTreeNode();
			rootAssegnazioneIterManuale.setExpanded(true);
			rootAssegnazioneIterManuale.setSelectable(false);

			//per ogni nodo di primo livello creo il nodo da mettere nell'albero che ha come padre rootAssegnazionePerCompetenza
			List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
			rootAssegnazioneIterManuale.setChildren(new ArrayList<>());
			DefaultTreeNode nodoToAdd = null;
			for (NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(utente.getCodiceAoo());
				if (n.getIdUtente() == null && 
						(TipoAssegnazioneEnum.FIRMA.equals(detail.getTipoAssegnazioneEnum()) 
								|| TipoAssegnazioneEnum.FIRMA_MULTIPLA.equals(detail.getTipoAssegnazioneEnum()) 
								|| TipoAssegnazioneEnum.SIGLA.equals(detail.getTipoAssegnazioneEnum()))) {
					n.setUtentiVisible(true);
				}
				nodoToAdd = new DefaultTreeNode(n, rootAssegnazioneIterManuale);
				nodoToAdd.setExpanded(true);
				if (n.getIdUtente() == null && alberatura.contains(n.getIdNodo())) {
					nodiDaAprire.add(nodoToAdd);
				}
			}

			List<DefaultTreeNode> nodiDaAprireTemp = new ArrayList<>();
			for (int i = 0; i < alberatura.size(); i++) {
				//devo fare esattamente un numero di cicli uguale alla lunghezza dell'alberatura
				nodiDaAprireTemp.clear();
				for (DefaultTreeNode nodo : nodiDaAprire) {
					NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) nodo.getData();
					onOpenTreeAssIterManuale(nodoExp, nodo);
					nodo.setExpanded(true);
					for (TreeNode nodoFiglio : nodo.getChildren()) {
						NodoOrganigrammaDTO nodoFiglioDto = (NodoOrganigrammaDTO)nodoFiglio.getData();
						if (nodoFiglioDto.getIdUtente() == null && alberatura.contains(nodoFiglioDto.getIdNodo())) {
							nodiDaAprireTemp.add((DefaultTreeNode)nodoFiglio);
						}
					}
				}
				nodiDaAprire = new ArrayList<>(nodiDaAprireTemp);
			}


		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Gestisce l'evento "Expand" per il tree degli assegnatari.
	 * @param event
	 */
	public void onOpenTreeAssIterManuale(NodeExpandEvent event) {
		NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) event.getTreeNode().getData();
		onOpenTreeAssIterManuale(nodoExp, event.getTreeNode());
	}

	/**
	 * Gestisce l'evento "Expand" per il tree degli assegnatari.
	 * @param nodoExp
	 * @param treeNode
	 */
	public void onOpenTreeAssIterManuale(NodoOrganigrammaDTO nodoExp, TreeNode treeNode) {

		try {
			List<NodoOrganigrammaDTO> figli = null;
			switch (detail.getTipoAssegnazioneEnum()) {
			case COMPETENZA:
				figli = organigrammaSRV.getFigliAlberoAssegnatarioPerCompetenzaUscita(utente, nodoExp);
				break;

			case FIRMA:
			case FIRMA_MULTIPLA:
				figli = organigrammaSRV.getFigliAlberoAssegnatarioPerFirma(utente.getIdUfficio(), nodoExp);
				break;

			case SIGLA:
				figli = organigrammaSRV.getFigliAlberoAssegnatarioPerSigla(utente.getIdUfficio(), nodoExp);
				break;

			default:
				break;
			}

			DefaultTreeNode nodoToAdd = null;
			treeNode.getChildren().clear();
			for (NodoOrganigrammaDTO n : figli) {
				n.setCodiceAOO(utente.getCodiceAoo());

				if ((n.getIdUtente() == null || n.getIdUtente() == 0) &&
						(TipoAssegnazioneEnum.FIRMA.equals(detail.getTipoAssegnazioneEnum()) 
								|| TipoAssegnazioneEnum.FIRMA_MULTIPLA.equals(detail.getTipoAssegnazioneEnum()) 
								|| TipoAssegnazioneEnum.SIGLA.equals(detail.getTipoAssegnazioneEnum()))) {
					n.setUtentiVisible(true);
				}

				nodoToAdd = new DefaultTreeNode(n, treeNode);
				if (n.getFigli() > 0 || n.isUtentiVisible()) {
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
				}
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}


	}

	/**
	 * Gestisce l'evento "Select" per il tree degli assegnatari.
	 * @param event
	 */
	public void onSelectTreeAssIterManuale(NodeSelectEvent event) {

		try {
			NodoOrganigrammaDTO select = (NodoOrganigrammaDTO) event.getTreeNode().getData();

			if ((TipoAssegnazioneEnum.FIRMA.equals(detail.getTipoAssegnazioneEnum()) 
					|| TipoAssegnazioneEnum.FIRMA_MULTIPLA.equals(detail.getTipoAssegnazioneEnum()) 
					|| TipoAssegnazioneEnum.SIGLA.equals(detail.getTipoAssegnazioneEnum())) 
					&& (select.getIdUtente() == null || select.getIdUtente() <= 0)) {
				return;
			}


			if (assegnatariPerContributo != null && !assegnatariPerContributo.isEmpty()) {
				for (AssegnazioneDTO ass : assegnatariPerContributo) {
					if ((ass.getUtente() == null || ass.getUtente().getId() == null || select.getIdUtente() == null)
							&& ass.getUfficio().getId().equals(select.getIdNodo()) ) {
						showWarningMaschera(ASSEGNATARIO_DUPLICATO_MSG);
						return;
					}

					if (ass.getUfficio().getId().equals(select.getIdNodo()) 
							&& ass.getUtente() != null && ass.getUtente().getId() != null 
							&& ass.getUtente().getId().equals(select.getIdUtente())) {
						showWarningMaschera(ASSEGNATARIO_DUPLICATO_MSG);
						return;
					}
				}
			}

			UfficioDTO uff = new UfficioDTO(select.getIdNodo(), select.getDescrizioneNodo());
			descrAssegnatarioIterManuale = select.getDescrizioneNodo();

			UtenteDTO ute = null;
			if (select.getIdUtente() != null) {
				descrAssegnatarioIterManuale += " - " + select.getNomeUtente() + " " + select.getCognomeUtente();
				ute = new UtenteDTO();
				ute.setId(select.getIdUtente());
				ute.setIdUfficio(select.getIdNodo());
				ute.setNome(select.getNomeUtente());
				ute.setCognome(select.getCognomeUtente());
			}

			assegnatarioPrincipale = new AssegnazioneDTO(null, detail.getTipoAssegnazioneEnum(), null, null, descrAssegnatarioIterManuale, ute, uff);

			//setto le caselle del tab spedizioni
			List<AssegnazioneDTO> assegnazioni = new ArrayList<>();
			assegnazioni.add(assegnatarioPrincipale);
			if (documentManagerSRV.isCasellaTabSpedizioniConfigurata(utente, documentManagerDTO, detail.getIdIterApprovativo(), detail.getTipoAssegnazioneEnum(), assegnazioni)) {
				documentManagerDTO.setComboCaselleSpedizione(new ArrayList<>());
				CasellaPostaDTO c = new CasellaPostaDTO();
				c.setIndirizzoEmail(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MAIL_COORDINAMENTO));
				documentManagerDTO.getComboCaselleSpedizione().add(c);
			} else {
				documentManagerDTO.setComboCaselleSpedizione(documentManagerDTO.getCaselleSpedizioneByUser());
			}

			FacesHelper.executeJS("PF('wdgAssegnatarioIternMan_DMD').hide()");
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

	}

	/**TIPO ASSEGNAZIONE END*******************************************************************************************/	


	/**RIFERIMENTO AI PROTOCOLLI IN ENTRATA START*******************************************************************************************/

	/**
	 * Aggiunge il protocollo di riferimento alla lista {@link #riferimentiProt}.
	 */
	public void addProtocolloDiRiferimento() {
		try {
			if (this.riferimentiProt == null) {
				riferimentiProt = new ArrayList<>();
			}

			RispostaAllaccioDTO r = new RispostaAllaccioDTO();
			r.setAnnoProtocollo(this.documentManagerDTO.getMaxAnno());

			this.riferimentiProt.add(r);

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Esegue una verifica dell'allaccio del protocollo.
	 * @param index
	 */
	public void checkProtocolloDiRiferimento(Integer index) {
		try {
			if (index > this.riferimentiProt.size()) {
				throw new RedException(ERROR_INDICE_NON_VALIDO_MSG);
			}

			RispostaAllaccioDTO rispostaSelected = this.riferimentiProt.get(index); 

			EsitoVerificaProtocolloDTO esito = documentManagerSRV.verificaAllaccioProtocollo(utente, rispostaSelected, TipoCategoriaEnum.ENTRATA); 
			
			if (!esito.isEsito() && !esito.isEntrata()) {
				esito = documentManagerSRV.verificaAllaccioProtocollo(utente, rispostaSelected, TipoCategoriaEnum.USCITA);
			}
			
			DetailDocumentRedDTO d = esito.getDocument();

			if (esito.isEsito()) {
				rispostaSelected.setDocument(d);
				rispostaSelected.setVerificato(true);
				if (Boolean.FALSE.equals(detail.getFlagEntrata()) && esito.isEntrata()) {
					rispostaSelected.setIdTipoAllaccio(TipoAllaccioEnum.RISPOSTA.getTipoAllaccioId());
					rispostaSelected.setFascicolo(d.getFascicoli().get(0));
				}
				else if (Boolean.TRUE.equals(detail.getFlagEntrata()) && esito.isEntrata()) {
					PropertiesProvider pp = PropertiesProvider.getIstance();
					String coppieTipiString = pp.getParameterByString(utente.getCodiceAoo()+"."+PropertiesNameEnum.TIPODOCUMENTO_NOTIFICA.getKey());
					if (coppieTipiString != null) {
						String [] coppieTipi = coppieTipiString.split(";");
						boolean isNotifica = false;
						for (String coppiaTipi : coppieTipi) {
							String [] tipi = coppiaTipi.split(",");
							Integer tipoDoc = Integer.valueOf(tipi[0]);
							Integer tipoProc = Integer.valueOf(tipi[1]);
							
							if (tipoDoc.equals(d.getIdTipologiaDocumento()) && tipoProc.equals(d.getIdTipologiaProcedimento())) {
								isNotifica = true;
								break;
							}
						}
						if (isNotifica) {
							rispostaSelected.setIdTipoAllaccio(TipoAllaccioEnum.NOTIFICA.getTipoAllaccioId());
						} else {
							rispostaSelected.setIdTipoAllaccio(TipoAllaccioEnum.ENTRATA_ENTRATA.getTipoAllaccioId());
						}
					} else {
						rispostaSelected.setIdTipoAllaccio(TipoAllaccioEnum.ENTRATA_ENTRATA.getTipoAllaccioId());
					}
					rispostaSelected.setFascicolo(d.getFascicoli().get(0));
				}
				else if (Boolean.TRUE.equals(detail.getFlagEntrata()) && !esito.isEntrata()) {
					rispostaSelected.setIdTipoAllaccio(TipoAllaccioEnum.ENTRATA_USCITA.getTipoAllaccioId());
				}
				
				// Se il documento è in uscita ma il protocollo non è un'entrata, l'allaccio non può essere fatto.
				if(Boolean.FALSE.equals(detail.getFlagEntrata()) && !esito.isEntrata()) {
					showWarningMaschera("Non è consentito allacciare un'uscita ad un documento in uscita.");
					rispostaSelected.setDocument(null);
					rispostaSelected.setVerificato(false);
				} else {
					rispostaSelected.setIdDocumentoAllacciato(d.getDocumentTitle());
					rispostaSelected.setTipoCategoriaEnum(TipoCategoriaEnum.getById(d.getTipoProtocollo()));
					
					refreshAllegatiDaAllacci();
				}

			} else {
				rispostaSelected.setDocument(null);
				rispostaSelected.setVerificato(false);
				showWarningMaschera(esito.getNote());

			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Qualora esista il componente allegati da allacci e vengano cambiati gli allacci,
	 * modifico anche il componente che gestisce gli allegati da allacci.
	 * Questa modifica deve essere attivata solo sui protocolli verificati.
	 */
	private void refreshAllegatiDaAllacci() {
		if (allegatiDaAllacciInitializer != null) {
			allegatiDaAllacciInitializer.onChangeAllaccio(riferimentiProt);
		}
	}

	/**
	 * Rimuove il protocollo di riferimento identificato dall'indice: index.
	 * @param index
	 */
	public void removeProtocolloDiRiferimento(Object index) {
		try {
			Integer i =  (Integer) index;
			this.riferimentiProt.remove(i.intValue());

			refreshAllegatiDaAllacci();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

	}
	/**RIFERIMENTO AI PROTOCOLLI IN ENTRATA END*******************************************************************************************/	

	/**DOCUMENTO PRINCIPALE UPLOAD START
	 * @throws IOException ******************************************************************************************/
	public void handleMainFileUpload(FileUploadEvent event) throws IOException {
		event = checkFileName(event);
		if(event == null) {
			return;
		}
		UploadedFile uf = event.getFile();
		 
		
		// Gestione dei MIME Type non riconosciuti dal componente di PrimeFaces (e.g. file EML)
		String contentType = uf.getContentType();
		if (ContentType.MIMETYPE_ENC.equals(contentType)) {
			contentType = FileUtils.getMimeType(FilenameUtils.getExtension(uf.getFileName()));
		}

		// Si ottiene il file
		FileDTO f = new FileDTO(uf.getFileName(), uf.getContents(), contentType);

		// Si gestisce l'upload
		handleMainFileUpload(f, false); 
	}


	private void handleMainFileUpload(FileDTO file, boolean uploadFromTemplateDocUscita) {
		try {
			String classeDocumentaleSelezionata = null;
			for (TipologiaDocumentoDTO t : documentManagerDTO.getComboTipologiaDocumento()) {
				if (t.getIdTipologiaDocumento().intValue() == detail.getIdTipologiaDocumento().intValue()) {
					classeDocumentaleSelezionata = t.getDocumentClass();
					break;
				}
			}
		
			boolean maxSizeRispettate = true;
			String sizeMBRound = "";
			if(this.documentManagerDTO.isInCreazioneUscita() || this.documentManagerDTO.isInModificaUscita()) {
				float sizeMB = this.detail.convertiContentInMB((float)file.getContent().length);
				sizeMBRound = String.format ("%.2f", sizeMB).replace(",", ".");
				float sizeTotal = Float.parseFloat(sizeMBRound) + this.detail.getSizeContent();
				if(this.detail.getMaxSizeContent()!=null) {
					maxSizeRispettate = sizeTotal <= this.detail.getMaxSizeContent();
				}
			}
			 
			EsitoDTO esitoCheck = documentManagerSRV.checkUploadFilePrincipale(file, utente, documentManagerDTO, 
					detail.getTipoAssegnazioneEnum(), classeDocumentaleSelezionata,maxSizeRispettate);

			if (esitoCheck.isEsito()) { 
				if(this.documentManagerDTO.isInCreazioneUscita() || this.documentManagerDTO.isInModificaUscita()) {
					this.detail.setSizeDocPrincipaleMB(Float.parseFloat(sizeMBRound));
					
					if((int)this.detail.getSizeContent()!=0) {
						String size = String.format ("%.2f", this.detail.getSizeContent()).replace(",", ".");
						this.detail.setSizeContent(Float.parseFloat(size));
					}
					Float total = this.detail.getSizeContent()+Float.parseFloat(sizeMBRound);
					total = Float.parseFloat(String.format ("%.2f", total).replace(",", "."));
					
					this.detail.setSizeContent(total);
				} 
				if (documentManagerDTO.isInModifica()) { 
					String extension = FilenameUtils.getExtension(file.getFileName());
					boolean p7mOrp7s = "p7m".equalsIgnoreCase(extension) || "p7s".equalsIgnoreCase(extension);
					if (detail.getAllegati() != null && !detail.getAllegati().isEmpty()) {
						boolean isStampigliaSiglaChecked = false;
						for (AllegatoDTO allegato : detail.getAllegati()) {
							if (allegato.getStampigliaturaSigla()) {
								isStampigliaSiglaChecked = true;
							}
						}
					
						if (isStampigliaSiglaChecked && p7mOrp7s) {
							showError("Non è possibile caricare un file p7m o p7s e inserire la stampigliatura del segno grafico");
							return;
						}
					} 
					
					// Se sono in modifica devo aggiornare il documento immediatamente
					EsitoOperazioneDTO es = documentoRedSRV.aggiornaContentDocumento(detail.getDocumentTitle(), 
							detail.getIdTipologiaDocumento(), 
							file.getContent(), 
							file.getMimeType(), 
							file.getFileName(), 
							detail.getIdIterApprovativo(), 
							detail.getAssegnazioni(),
							detail.getTemplateDocUscitaMap(),
							detail.getTipoAssegnazioneEnum() != null ? detail.getTipoAssegnazioneEnum().getId() : null,
									utente);

					// Avverto che il contenuto è stato cambiato
					showInfoMessage(es.getNote());

					// Si recupera il content del documento aggiornato
					if (es.isEsito()) {
						file = documentoSRV.getDocumentoContentPreview(utente.getFcDTO(), detail.getDocumentTitle(), true, utente.getIdAoo(), false);
					}
				}

				docPrincipale = file;
				this.detail.setNomeFile(file.getFileName());
				this.detail.setContent(file.getContent());
				this.detail.setMimeType(file.getMimeType());

				// Si ricalcola il flag per la visibilità del pulsante per la verifica della firma
				documentManagerDTO.setVerificaFirmaDocPrincipaleVisible(documentManagerSRV.isVerificaFirmaDocPrincipaleVisible(file.getMimeType()));

				docPrincipale = null;
				// Gestione Template Doc Uscita
				handleTemplateDocUscitaOnMainFileChange(uploadFromTemplateDocUscita);

				// Si aggiorna il pulsante per l'anteprima e, se la modifica del content è avvenuta tramite Template Doc Uscita, anche l'anteprima stessa
				if (documentManagerDTO.isInCreazioneUscita() || documentManagerDTO.isInModificaUscita() || documentManagerDTO.isInCreazioneIngresso()) {
				 
					mainDocForSignFieldPreview = file;
					mainDocForSignFieldPreview.setUuid(UUID.randomUUID().toString());
					FacesHelper.update("idDettagliEstesiForm:idVisAnteprima");

					if (uploadFromTemplateDocUscita) {
						FacesHelper.update("idDettagliEstesiForm:docPreview");
						// Si aggiorna il il panel relativo al documento principale
						FacesHelper.update("idDettagliEstesiForm:idTabs_DMD:idDocPrincipalePanel_DMD");
						FacesHelper.update("idDettagliEstesiForm:idTabs_DMD:sizeDocPrincipaleId");
						FacesHelper.update("idDettagliEstesiForm:idDimensioneContent");
					}
				}
				if(uploadFromTemplateDocUscita) {
					FacesHelper.executeJS("PF('dlgTemplateDocUscita').hide()");
				}
			} else {
				if(uploadFromTemplateDocUscita) {
					showError(esitoCheck.getNote());
				} else {
					showErrorMaschera(esitoCheck.getNote());
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Gestisce la rimozione del file.
	 */
	public void rimuoviFile() {
		docPrincipale = null;
		
		if(documentManagerDTO.isInCreazioneUscita() || documentManagerDTO.isInModificaUscita()) {
			String sizeMBRound = "";
			if(this.detail.getContent()!=null) {
				Float sizeMB = this.detail.convertiContentInMB((float)this.detail.getContent().length);
				sizeMBRound = String.format ("%.2f", sizeMB).replace(",", ".");
			} else { 
				if(this.detail.getSizeDocPrincipaleMB() == null) {
					sizeMBRound = "0.0";
				} else {
					sizeMBRound = String.format ("%.2f", this.detail.getSizeDocPrincipaleMB()).replace(",", ".");
				}
			}
			this.detail.setSizeDocPrincipaleMB(null); 
			
			Float size = this.detail.getSizeContent()-Float.parseFloat(sizeMBRound);
			Float sizeMBRoundDetail = Float.parseFloat(String.format ("%.2f", size ).replace(",", "."));
			this.detail.setSizeContent(sizeMBRoundDetail);
		}
		
		this.detail.setNomeFile(null);
		this.detail.setMimeType(null);
		this.detail.setContent(null);

		// Gestione Template Doc Uscita
		handleTemplateDocUscitaOnMainFileChange(false);

		// Gestione del pulsante per la verifica della firma
		documentManagerDTO.setVerificaFirmaDocPrincipaleVisible(false);

		// Gestione del pulsante per l'anteprima
		if (documentManagerDTO.isInCreazioneUscita() || documentManagerDTO.isInModificaUscita() || documentManagerDTO.isInCreazioneIngresso()) {
			mainDocForSignFieldPreview = null;
			FacesHelper.update("idDettagliEstesiForm:idVisAnteprima");
		}
	}
	

	//	TEMPLATE FILES 
	/**
	 * Gestisce il download del template.
	 * @param idTemplate
	 */
	public void downloadTemplate(String idTemplate) {
		try {
			List<DestinatarioRedDTO> destinatari = getDestinatariInModificaPuliti();
			if (StringUtils.isNullOrEmpty(detail.getOggetto()) 
					|| (destinatari == null || destinatari.isEmpty())) {
				String message = "Le seguenti informazioni sono obbligatorie per poter utilizzare un template: <br />"
						+ " - oggetto<br />"
						+ " - destinatari";
				showWarningMaschera(message);
				return;
			}

			InfoDocTemplate info = new InfoDocTemplate();
			info.setOggetto(detail.getOggetto());
			info.setMittente(utente.getNodoDesc());
			StringBuilder destinatariStr = new StringBuilder();
			for (DestinatarioRedDTO destinatarioRedDTO : destinatari) {
				destinatariStr.append(destinatarioRedDTO.getContatto().getAliasContatto()).append(InfoDocTemplate.DESTINATARI_SEPARATOR);
			}
			info.setDestinatari(destinatariStr.toString());

			StringBuilder protMittente = null;
			if (riferimentiProt != null && !riferimentiProt.isEmpty()) {
				protMittente = new StringBuilder();
				RispostaAllaccioDTO ris = null; 
				for (int i = 0; i < riferimentiProt.size(); i++) {
					ris = riferimentiProt.get(i);
					protMittente.append(ris.getNumeroProtocollo()).append(PATH_DELIMITER).append(ris.getAnnoProtocollo());
					if (i != riferimentiProt.size() -1) {
						protMittente.append(InfoDocTemplate.PROTOCOLLI_SEPARATOR);
					}
				}
			}
			info.setProtocolloMittente(protMittente != null ? protMittente.toString() : null);


			DocumentAppletContentDTO d = documentManagerSRV.getDocumentForAppletByGuid(idTemplate, utente);

			FacesHelper.executeJS("openTemplate('" 
					+ d.getContentType() + "','" 
					+ d.getContent() + "','" 
					+ d.getNomeFile() + "','"
					+ JavaScriptUtils.javaScriptEscape(info.toString().replaceAll("\r\n|\r|\n|:|\\[|\\]|\"|-", " ")
						.replaceAll(InfoDocTemplate.DESTINATARI_SEPARATOR, "</w:t><w:br/><w:t>")) + "')");
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Imposta i parametri relativi al file, recuperandole dal template in sessione.
	 */
	public void saveFileTemplateFromApplet() {
		try {
			FileDTO template = (FileDTO)FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.EDITED_TEMPLATE, true);
			this.detail.setNomeFile(template.getFileName());
			this.detail.setMimeType(template.getMimeType());
			this.detail.setContent(template.getContent());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Verifica della firma sul content del documento principale.
	 */
	public void verificaFirmaDocPrincipale() {
		byte[] content = null;

		try {
			if (docPrincipale != null) {
				content = docPrincipale.getContent();
			} else if (detail.getDocumentTitle() != null) {
				content = documentoRedSRV.getDocumentoDopoTrasformazione(detail.getDocumentTitle(), utente.getIdAoo(), utente.getFcDTO()).getContent();
			}

			InputStream is = new ByteArrayInputStream(content);

			if (!signSRV.isValidSignature(is, utente, false)) {
				showWarningMaschera("Il file inserito non risulta essere firmato oppure la firma non è valida.");
			} else {
				showInfoMessage("Il documento risulta essere firmato correttamente.");
			}
		} catch (Exception e) {
			LOGGER.error(e);
			showErrorMaschera("Si è verificato un errore durante il processo di verifica della firma.");
		}
	}

	/**DOCUMENTO PRINCIPALE UPLOAD END********************************************************************************************/

	/** RICERCA FASCICOLO START ***************************************************************************************************************/
	@Override
	public void updateDetail(FascicoloDTO fascicoloProcedimentale) {
		impostaFascicolo(fascicoloProcedimentale);
	}

	/**
	 * Esegue la ricerca dei fascicoli popolando l'attributo con i fascicoli recuperati.
	 * {@link #ricercaFascicoliResults}, {@link ##ricercaFascicoliResultsFiltered}.
	 */
	public void cercaFascicolo() {
		try {
			Collection<FascicoloDTO> fasColl = ricercaSRV.ricercaGenericaFascicoli(this.ricercaFascicoloKey, this.ricercaFascicoloAnno, this.ricercaFascicoloModalita, 
					RicercaPerBusinessEntityEnum.FASCICOLI, utente);
			if (fasColl != null && !fasColl.isEmpty()) {
				ricercaFascicoliResults = new ArrayList<>(fasColl);
				ricercaFascicoliResultsFiltered = new ArrayList<>(fasColl);
			}

			String idComponent = "idDettagliEstesiForm:idTabs_DMD:cercaFascicoloTabs:idTableRicercaFascicolo_DMD";
			DataTable dt = getDataTableByIdComponent(idComponent);
			if (dt != null) {
				dt.reset();
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		} 
	}

	/**
	 * Imposta il fascicolo identificato dall'indice come selezionato.
	 * @param index
	 */
	public void selectFascicolo(Object index) {
		try {
			Integer i = (Integer) index;
			if (i >= ricercaFascicoliResultsFiltered.size()) {
				throw new RedException("Indice di riga non valido.");
			}

			FascicoloDTO fProcedimentale = ricercaFascicoliResultsFiltered.get(i);
			impostaFascicolo(fProcedimentale);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Imposta il fascicolo identificato come MasterFascicoloDTO e indicato dal SelectEvent, 
	 * come FascicoloDTO aggiungendolo alla lista memorizzata nel {@link #detail}.
	 * @param event
	 */
	public void selectFascicoloDelTitolario(SelectEvent event) {
		try {
			MasterFascicoloDTO fascicoloSelezionato = (MasterFascicoloDTO) event.getObject();
			IFascicoloSRV fascSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloSRV.class);
			FascicoloDTO fProcedimentale = fascSRV.getFascicoloByNomeFascicolo(fascicoloSelezionato.getId().toString(), utente);
			impostaFascicolo(fProcedimentale);

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	private void impostaFascicolo(FascicoloDTO fProcedimentale) {
		if (this.detail.getFascicoli() == null) {
			this.detail.setFascicoli(new ArrayList<>());
		}

		//se c'e' piu' di un fascicolo devo rimuovere il fascicolo procedimentale in cui e' contenuto
		//e mettere quello selezionato dall'utente
		//se invece il fascicolo e' uno solo semplicemente svuoto la lista 
		if (this.detail.getFascicoli().size() > 1) {
			int indexToRemove = 0;
			for (FascicoloDTO f : this.detail.getFascicoli()) {
				if (f.getIdFascicolo().equals(this.detail.getIdFascicoloProcedimentale())) {
					break;
				}
				indexToRemove++;
			}
			this.detail.getFascicoli().remove(indexToRemove);
		} else {
			this.detail.getFascicoli().clear();
		}
		this.detail.getFascicoli().add(fProcedimentale);

			this.detail.setIdFascicoloProcedimentale(fProcedimentale.getIdFascicolo());
			if (!StringUtils.isNullOrEmpty(fProcedimentale.getOggetto())) {
				String[] arr = fProcedimentale.getOggetto().split("_");
				if (arr.length >= 3) {
					this.detail.setPrefixFascicoloProcedimentale(arr[0] + "_" + arr[1] + "_");
					StringBuilder descFasc = new StringBuilder();
					for (int maxLenght = 2; maxLenght < arr.length; maxLenght++) {
						descFasc.append(StringUtils.isNullOrEmpty(descFasc.toString()) ? arr[maxLenght] : ("_"+arr[maxLenght]));
						this.detail.setDescrizioneFascicoloProcedimentale(descFasc.toString());
					}
					
			}
		}
		this.detail.setIndiceClassificazioneFascicoloProcedimentale(fProcedimentale.getIndiceClassificazione());
		indiceDiClassificazione = new DocumentManagerDataIndiceDiClassificazioneComponent(detail, utente, documentManagerDTO);

		/**recupero i documenti del fascicolo per utilizzarli come allegati nel tab spedizione  ******************************************************/
		if (this.documentManagerDTO.isTabSpedizioneVisible()) {
			this.documentManagerDTO.setAllegatiMail(
					documentManagerSRV.getAllegatiMail(
							fProcedimentale.getIdFascicolo(), this.detail.getDocumentTitle(), utente));
			this.allegatiMailSelected = new ArrayList<>();
		}

		if (Boolean.TRUE.equals(fProcedimentale.getFaldonato())) {
			detail.setFaldoni(
					detailFascicoloSRV.getFaldoniByIdFascicolo(
							fProcedimentale.getIdFascicolo(), fProcedimentale.getIdAOO().longValue(), utente));
		} else {
			detail.setFaldoni(new ArrayList<>());
		}

	}

	/**
	 * Inizializza {@link #fascicoliPerTitolario}.
	 */
	public void initRicercaPerTitolario() {
		fascicoliPerTitolario = new FascicoliPerTitolario(utente);
	}

	/**
	 * Effettua il reset dei campi per la ricerca per titolario.
	 */
	public void resetRicercaPerTitolario() {
		fascicoliPerTitolario = new FascicoliPerTitolario(utente);
		ricercaFascicoloKey  = "";
		ricercaFascicoloAnno = Calendar.getInstance().get(Calendar.YEAR);
		ricercaFascicoliResults = new ArrayList<>();
		ricercaFascicoliResultsFiltered = new ArrayList<>();

	}
	/** RICERCA FASCICOLO END *****************************************************************************************************************/


	/**MITTENTE CONTATTO START***********************************************************************************************/

	/**
	 * @param query
	 * @return List di Contatto
	 */
	public List<Contatto> mittenteComplete(String query) {
		List<Contatto> list = new ArrayList<>();
		try {
			this.detail.setMittenteContattoNew();

			if (StringUtils.isNullOrEmpty(query)) {
				return documentManagerDTO.getContattiPreferiti();
			}

			query = query.toUpperCase();

			if (!StringUtils.isNullOrEmpty(query) && query.trim().length() >= 3) {
				String[] queryArr = query.trim().toUpperCase().split(" ");
				filtraContattiPreferiti();
				for (Contatto cont: documentManagerDTO.getContattiPreferitiFiltered()) {
					if ((cont.getDisabilitaElemento() == null || Boolean.FALSE.equals(cont.getDisabilitaElemento())) 
							&& (
							cont.getAliasContatto() != null && StringUtils.containsAllWords(cont.getAliasContatto().toUpperCase(), queryArr)
							|| cont.getCognome() != null && StringUtils.containsAllWords(cont.getCognome().toUpperCase(), queryArr)
							|| cont.getNome() != null && StringUtils.containsAllWords(cont.getNome().toUpperCase(), queryArr)
									)
							) {
						list.add(cont);
					}
				}
			}


		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
		return list;
	}

	/**
	 * Aggiorna l'attributo del mittente nell'oggetto {@link #detail}.
	 * @param collection
	 * @param salva
	 */
	public void aggiornaMittente(Collection<Contatto> collection, boolean salva) {
		if (salva) {

			if (collection != null && collection.size() == 1) {
				for (Contatto contatto : collection) { //e' di un solo elemento, non metto il break
					this.detail.setMittenteContatto(contatto);
				}
			} else {
				this.detail.setMittenteContatto(null);
			}

		}

		FacesHelper.executeJS(HIDE_WDGSELEZIONADARUBRICA_JS);
		if (!StringUtils.isNullOrEmpty(chiamante) 
				&& chiamante.equalsIgnoreCase(ConstantsWeb.MBean.MAIL_BEAN)) {
			FacesHelper.update(IDMITTENTEAUTO_DMD_RESOURCE_LOCATION);
		}
	}

	/**
	 * Gestisce l'aggiunta del mittente esterno identificato dall'idContatto.
	 * @param idContatto
	 */
	public void addMittenteEsterno(Long idContatto) {
		try {
			Contatto selezionato = null;
			for (Contatto pref : this.documentManagerDTO.getContattiPreferiti()) {
				if (pref.getContattoID().equals(idContatto)) {
					selezionato = pref; 
					setTipologiaIndirizzoEmail(selezionato); 
					break;
				}
			}
			if (selezionato == null) {
				showWarningMaschera("Il contatto non è stato trovato!");
				return;
			}

			inModificaDaShortcut = false;
			if(selezionato.getTipoRubrica()!=null && "RED".equals(selezionato.getTipoRubrica())) {
				creaORichiediCreazioneFlag = true;
				inModificaDaShortcut  = true;
			}
			this.detail.setMittenteContatto(selezionato);
			
			 
			if (!StringUtils.isNullOrEmpty(chiamante) 
					&& ConstantsWeb.MBean.MAIL_BEAN.equalsIgnoreCase(chiamante)) {
				FacesHelper.update(IDMITTENTEAUTO_DMD_RESOURCE_LOCATION);
				FacesHelper.update("eastSectionForm:idMittentePnl_DMD");
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}
 
	private static void setTipologiaIndirizzoEmail(Contatto contatto) {
		if(!StringUtils.isNullOrEmpty(contatto.getMailPec())) {
			contatto.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEC);
		} else if(!StringUtils.isNullOrEmpty(contatto.getMail())) {
			contatto.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEO);
		}
	}
	
	/**MITTENTE CONTATTO END***********************************************************************************************/	

	/**ORGANIGRAMMA ASSEGNATARIO PER COMPETENZA START********************************************************************************************************************/
	/**
	 * Gestisce la creazione dell'organigramma degli assegnatari per competenza.
	 */
	public void creaOrganigrammaAssegnatarioPerCompetenza() {

		try {

			if (CollectionUtils.isEmpty(alberaturaNodi)) {
				throw new RedException (ALBERATURA_NODI_NON_RECUPERATA_CORRETTAMENTE);
			}
			
			List<Long> alberatura = new ArrayList<>();
			for (int i = alberaturaNodi.size() - 1; i >= 0; i--) {
				alberatura.add(alberaturaNodi.get(i).getIdNodo());
			}

			//prendo da DB i nodi di primo livello
			NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
			nodoRadice.setIdAOO(utente.getIdAoo());
			nodoRadice.setIdNodo(utente.getIdNodoRadiceAOO());
			nodoRadice.setCodiceAOO(utente.getCodiceAoo());
			nodoRadice.setUtentiVisible(false);

			List<NodoOrganigrammaDTO> primoLivello = organigrammaSRV.getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovo(documentManagerDTO.isInCreazioneIngresso() 
					|| documentManagerDTO.isInModificaIngresso(), utente.getIdUfficio(), nodoRadice, detail.getProtocollazioneMailAccount(), false);

			rootAssegnazionePerCompetenza = new DefaultTreeNode();
			rootAssegnazionePerCompetenza.setExpanded(true);
			rootAssegnazionePerCompetenza.setSelectable(false);

			//per ogni nodo di primo livello creo il nodo da mettere nell'albero che ha come padre rootAssegnazionePerCompetenza
			List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
			rootAssegnazionePerCompetenza.setChildren(new ArrayList<>());
			DefaultTreeNode nodoToAdd = null;
			for (NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(utente.getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, rootAssegnazionePerCompetenza);
				nodoToAdd.setExpanded(true);
				if (n.getIdUtente() == null && alberatura.contains(n.getIdNodo())) {
					nodiDaAprire.add(nodoToAdd);
				}
			}

			List<DefaultTreeNode> nodiDaAprireTemp = new ArrayList<>();
			for (int i = 0; i < alberatura.size(); i++) {
				//devo fare esattamente un numero di cicli uguale alla lunghezza dell'alberatura
				nodiDaAprireTemp.clear();
				for (DefaultTreeNode nodo : nodiDaAprire) {
					NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) nodo.getData();
					onOrgAssPerCompetenzaNodeExpand(nodoExp, nodo);
					nodo.setExpanded(true);
					for (TreeNode nodoFiglio : nodo.getChildren()) {
						NodoOrganigrammaDTO nodoFiglioDto = (NodoOrganigrammaDTO)nodoFiglio.getData();
						if (nodoFiglioDto.getIdUtente() == null && alberatura.contains(nodoFiglioDto.getIdNodo())) {
							nodiDaAprireTemp.add((DefaultTreeNode)nodoFiglio);
						}
					}
				}
				nodiDaAprire = new ArrayList<>(nodiDaAprireTemp);
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

	}

	/**
	 * Gestisce l'evento di "Expand" associato all'organigramma degli assegnatari per compenteza.
	 * @param event
	 */
	public void onOrgAssPerCompetenzaNodeExpand(NodeExpandEvent event) {
		NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) event.getTreeNode().getData();
		onOrgAssPerCompetenzaNodeExpand(nodoExp, event.getTreeNode());
	}

	/**
	 * Gestisce l'evendo "Expand" di un nodo per l'organigramma degli assegnatari per competenza.
	 * @param nodoExp
	 * @param treeNode
	 */
	public void onOrgAssPerCompetenzaNodeExpand(NodoOrganigrammaDTO nodoExp, TreeNode treeNode) {
		try {

			List<NodoOrganigrammaDTO> figli = organigrammaSRV.getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovo(
					documentManagerDTO.isInCreazioneIngresso() || documentManagerDTO.isInModificaIngresso(), utente.getIdUfficio(), nodoExp, detail.getProtocollazioneMailAccount(), false);

			DefaultTreeNode nodoToAdd = null;
			treeNode.getChildren().clear();
			for (NodoOrganigrammaDTO n : figli) {
				n.setCodiceAOO(utente.getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, treeNode);
				if (n.getFigli() > 0 || n.isUtentiVisible()) {
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
				}
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Gestisce l'evendo "Select" di un nodo per l'organigramma degli assegnatari per competenza.
	 * @param event
	 */
	public void onOrgAssPerCompetenzaSelect(NodeSelectEvent event) {
		try {
			NodoOrganigrammaDTO select = (NodoOrganigrammaDTO) event.getTreeNode().getData();
			onOrgAssPerCompetenza(select);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Gestisce l'evendo "Expand" di un nodo per l'organigramma degli assegnatari per competenza.
	 * @param select
	 */
	public void onOrgAssPerCompetenza(NodoOrganigrammaDTO select) {
		try {

			UfficioDTO uff = new UfficioDTO(select.getIdNodo(), select.getDescrizioneNodo());

			if (assegnatariPerContributo != null && !assegnatariPerContributo.isEmpty()) {
				for (AssegnazioneDTO ass : assegnatariPerContributo) {
					if ((ass.getUtente() == null || ass.getUtente().getId() == null || select.getIdUtente() == null)
							&& ass.getUfficio().getId().equals(select.getIdNodo())) {
						// controllo che la selezione non sia un ufficio oppure non ci sia una selezione nei contributi che sia un ufficio
						// se c'è controllo che gli uffici (della selezione e dei contributi) non coincidano
						showWarningMaschera(ASSEGNATARIO_DUPLICATO_MSG);
						return;
					}

					if (ass.getUfficio().getId().equals(select.getIdNodo()) 
							&& ass.getUtente() != null && ass.getUtente().getId() != null && ass.getUtente().getId().equals(select.getIdUtente())) {
						showWarningMaschera(ASSEGNATARIO_DUPLICATO_MSG);
						return;
					}
				}
			}

			if (assegnatariPerConoscenza != null && !assegnatariPerConoscenza.isEmpty()) {
				for (AssegnazioneDTO ass : assegnatariPerContributo) {
					Long assContrIdUtente = (ass.getUtente() != null && ass.getUtente().getId() != null) ? ass.getUtente().getId() : null;
					if (ass.getUfficio().getId().equals(select.getIdNodo())
							&& ((assContrIdUtente == null && select.getIdUtente() == null) 
									|| (assContrIdUtente != null && assContrIdUtente.equals(select.getIdUtente())))) {
						showWarningMaschera(ASSEGNATARIO_DUPLICATO_MSG);
						return;
					}
				}
			}

			descrAssegnatarioPerCompetenza = select.getDescrizioneNodo();
			if(utente.isRibaltaTitolario()) {
				boolean preseleziona = documentManagerSRV.preselezionaRibaltaTitolario(uff.getId(), utente.getIdAoo());
				if(preseleziona) {
					detail.setRibaltaTitolario(true); 
					FacesHelper.update("eastSectionForm:ribaltaTitolarioID");
				} else {
					detail.setRibaltaTitolario(false); 
					FacesHelper.update("eastSectionForm:ribaltaTitolarioID");
				}
			}			
			UtenteDTO ute = null;
			if (select.getIdUtente() != null) {
				descrAssegnatarioPerCompetenza += " - " + select.getNomeUtente() + " " + select.getCognomeUtente();
				ute = new UtenteDTO();
				ute.setId(select.getIdUtente());
				ute.setIdUfficio(select.getIdNodo());
				ute.setNome(select.getNomeUtente());
				ute.setCognome(select.getCognomeUtente());
			}

			assegnatarioPrincipale = new AssegnazioneDTO(null, TipoAssegnazioneEnum.COMPETENZA, null, null, descrAssegnatarioPerCompetenza, ute, uff);

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}
	/**ORGANIGRAMMA ASSEGNATARIO PER COMPETENZA END********************************************************************************************************************/

	
	/**********************************************************************/	
	/**RACCOLTA FAD START********************************************************************************************************************/
	/**
	 * Gestisce l'evento di modifica dell'amministrazione selezionata popolando l'attributo: {@link DocumentManagerBean#descrizioneAmministrazioneSelected}.
	 */
	public void onChangeComboFadAmministrazione() {
		try {
			if (!StringUtils.isNullOrEmpty(codiceAmministrazioneSelected)) {
				fadRagioneriaSelected = fepaSRV.findRagioneriaByCodiceAmministrazione(codiceAmministrazioneSelected);

				for (FadAmministrazioneDTO f : documentManagerDTO.getComboFadAmministrazioni()) {
					if (f.getCodiceAmministrazione().equals(codiceAmministrazioneSelected)) {
						descrizioneAmministrazioneSelected = f.getDescrizioneAmministrazione();
						break;
					}
				}
			} else {
				fadRagioneriaSelected = null;
				descrizioneAmministrazioneSelected = null;
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Crea la raccolta FAD in base al codice dell'amministrazione selezionata.
	 */
	public void creaRaccoltaFAD() {
		try {
			if (StringUtils.isNullOrEmpty(detail.getWobNumberSelected())) {
				throw new RedException("WOB number mancante.");
			}

			if (StringUtils.isNullOrEmpty(codiceAmministrazioneSelected)) {
				throw new RedException("selezionare un'amministrazione.");
			}

			FadAmministrazioneDTO fSelect = null;
			for (FadAmministrazioneDTO f : documentManagerDTO.getComboFadAmministrazioni()) {
				if (f.getCodiceAmministrazione().equals(codiceAmministrazioneSelected)) {
					fSelect = f;
					break;
				}
			}

			// Creazione della Raccolta Provvisoria
			identificativoRaccolta = documentManagerSRV.creaRaccoltaFadManuale(detail, fSelect, fadRagioneriaSelected, utente);

			if (StringUtils.isNullOrEmpty(identificativoRaccolta)) {
				throw new RedException("l'identificativo non è stato creato.");
			} else {
				showInfoMessage("Identificativo creato correttamente.");
				// Si disabilita la creazione della raccolta FAD
				documentManagerDTO.setCreaRaccoltaFadDisabled(true);
				//non si può più cambiare la tipologia doc e processo
				documentManagerDTO.setTipologiaDocumentoDisable(true);
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

	}
	/**RACCOLTA FAD END**********************************************************************************************************************/
	/**********************************************************************************************************************/

	/** TAB ASSEGNATARI PER CONOSCENZA / CONTRIBUTO START******************************************************************************************************/
	/** ASSEGNATARI PER CONOSCENZA START****************************************************************************************************/
	/**
	 * Gestisce l'evendo "Select" di un nodo per l'organigramma degli assegnatari per conoscenza.
	 * @param event
	 */
	public void onSelectTreeAssPerConoscenza(NodeSelectEvent event) {

		try {
			NodoOrganigrammaDTO select = (NodoOrganigrammaDTO) event.getTreeNode().getData();

			if (assegnatariPerConoscenza == null) {
				assegnatariPerConoscenza = new ArrayList<>();
			}

			if (assegnatariPerContributo == null) {
				assegnatariPerContributo = new ArrayList<>();
			}

			if ((assegnatarioPrincipale != null && assegnatarioPrincipale.getUfficio() != null) && (select.getIdNodo().equals(assegnatarioPrincipale.getUfficio().getId())
					&& ((select.getIdUtente() == null && (assegnatarioPrincipale.getUtente() == null
					|| assegnatarioPrincipale.getUtente().getId() == null))
					|| (assegnatarioPrincipale.getUtente() != null
					&& assegnatarioPrincipale.getUtente().getId().equals(select.getIdUtente()))))) {
				showWarningMaschera(ASSEGNATARIO_DUPLICATO_MSG);
				return;
			}

			//Controllo che non sia gia' stato inserito
			for (AssegnazioneDTO assConoscenzaDTO : assegnatariPerConoscenza) {
				Long assConoscenzaIdUtente = (assConoscenzaDTO.getUtente() != null && assConoscenzaDTO.getUtente().getId() != null) ? assConoscenzaDTO.getUtente().getId() : null;
				if (assConoscenzaDTO.getUfficio().getId().equals(select.getIdNodo()) 
						&& ((assConoscenzaIdUtente == null && select.getIdUtente() == null)
								|| (assConoscenzaIdUtente != null && assConoscenzaIdUtente.equals(select.getIdUtente())))) {
					showWarningMaschera(ASSEGNATARIO_DUPLICATO_MSG);
					return;
				}
			}

			for (AssegnazioneDTO assContr : assegnatariPerContributo) {
				Long assConoscenzaIdUtente = (assContr.getUtente() != null && assContr.getUtente().getId() != null) ? assContr.getUtente().getId() : null;
				if (assContr.getUfficio().getId().equals(select.getIdNodo())
						&& ((assConoscenzaIdUtente == null && select.getIdUtente() == null)
								|| (assConoscenzaIdUtente != null && assConoscenzaIdUtente.equals(select.getIdUtente())))) {
					showWarningMaschera(ASSEGNATARIO_DUPLICATO_MSG);
					return;
				}
			}

			UfficioDTO uff = new UfficioDTO(select.getIdNodo(), select.getDescrizioneNodo());

			UtenteDTO ute = null;
			if (select.getIdUtente() != null) {
				ute = new UtenteDTO();
				ute.setId(select.getIdUtente());
				ute.setIdUfficio(select.getIdNodo());
				ute.setNome(select.getNomeUtente());
				ute.setCognome(select.getCognomeUtente());
			}

			AssegnazioneDTO assegnatarioDaAggiungere = new AssegnazioneDTO(null, TipoAssegnazioneEnum.CONOSCENZA, null, null, select.getDescrizioneNodo(), ute, uff);
			assegnatariPerConoscenza.add(assegnatarioDaAggiungere);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

	}

	/**
	 * Gestisce l'eliminazione dell'assegnatario per conoscenza identificato dall'indice dalla lista degli assegnatari: {@link #assegnatariPerConoscenza}.
	 * @param index
	 */
	public void rimuoviAssegnatarioPerConoscenza(Object index) {

		try {
			Integer i =  (Integer) index;
			this.assegnatariPerConoscenza.remove(i.intValue());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

	} 

	/** ASSEGNATARI PER CONOSCENZA END******************************************************************************************************/

	/** ASSEGNATARI PER CONTRIBUTO START****************************************************************************************************/
	/**
	 * Gestisce l'evendo "Select" di un nodo per l'organigramma degli assegnatari per contributo.
	 * @param event
	 */
	public void onSelectTreeAssPerContributo(NodeSelectEvent event) {

		try {
			NodoOrganigrammaDTO select = (NodoOrganigrammaDTO) event.getTreeNode().getData();

			if (assegnatariPerContributo == null) {
				assegnatariPerContributo = new ArrayList<>();
			}

			if (assegnatariPerConoscenza == null) {
				assegnatariPerConoscenza = new ArrayList<>();
			}

			if (assegnatarioPrincipale != null 
					&& ((select.getIdUtente() == null || assegnatarioPrincipale.getUtente() == null || assegnatarioPrincipale.getUtente().getId() == null) 
							&& select.getIdNodo().equals(assegnatarioPrincipale.getUfficio().getId())
						|| (select.getIdUtente() != null && assegnatarioPrincipale.getUtente() != null && select.getIdUtente().equals(assegnatarioPrincipale.getUtente().getId())))) {
				// se è stato selezionato un assegnatario principale ed è un ufficio oppure l'assegnatario per contributo è un ufficio
				// non devono avere lo stesso ufficio
				showWarningMaschera(ASSEGNATARIO_DUPLICATO_MSG);
				return;
			}

			//Controllo che non sia gia' stato inserito
			for (AssegnazioneDTO assContributoDTO : assegnatariPerContributo) {
				Long assIdUtente = (assContributoDTO.getUtente() != null && assContributoDTO.getUtente().getId() != null) ? assContributoDTO.getUtente().getId() : null;
				if (assContributoDTO.getUfficio().getId().equals(select.getIdNodo()) 
						&& ((assIdUtente == null && select.getIdUtente() == null)
								|| (assIdUtente != null && assIdUtente.equals(select.getIdUtente())))) {
					showWarningMaschera(ASSEGNATARIO_DUPLICATO_MSG);
					return;
				}
			}
			for (AssegnazioneDTO assCon : assegnatariPerConoscenza) {
				Long assIdUtente = (assCon.getUtente() != null && assCon.getUtente().getId() != null) ? assCon.getUtente().getId() : null;
				if (assCon.getUfficio().getId().equals(select.getIdNodo()) 
						&& ((assIdUtente == null && select.getIdUtente() == null)
								|| (assIdUtente != null && assIdUtente.equals(select.getIdUtente())))) {
					showWarningMaschera(ASSEGNATARIO_DUPLICATO_MSG);
					return;
				}
			}

			UfficioDTO uff = new UfficioDTO(select.getIdNodo(), select.getDescrizioneNodo());

			UtenteDTO ute = null;
			if (select.getIdUtente() != null) {
				ute = new UtenteDTO();
				ute.setId(select.getIdUtente());
				ute.setIdUfficio(select.getIdNodo());
				ute.setNome(select.getNomeUtente());
				ute.setCognome(select.getCognomeUtente());
			}

			AssegnazioneDTO assegnatarioDaAggiungere = new AssegnazioneDTO(null, TipoAssegnazioneEnum.CONTRIBUTO, null, null, select.getDescrizioneNodo(), ute, uff);
			assegnatariPerContributo.add(assegnatarioDaAggiungere);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

	}

	/**
	 * Gestisce l'eliminazione dell'assegnatario per contributo identificato dall'index dalla lista degli assegnatari: {@link #assegnatariPerContributo}.
	 * @param index
	 */
	public void rimuoviAssegnatarioPerContributo(Object index) {

		try {
			Integer i =  (Integer) index;
			this.assegnatariPerContributo.remove(i.intValue());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

	} 
	/** ASSEGNATARI PER CONTRIBUTO END******************************************************************************************************/	

	/** TAB ASSEGNATARI PER CONOSCENZA / CONTRIBUTO END******************************************************************************************************/	

	/** RESPONSABILE COPIA CONFORME DOCUMENTO PRINCIPALE START ****************************************************************************************************************************************************/
	/**
	 * Gestisce l'evento di modifica del responsabile copia conforme impostando il parametro associato ad esso nel {@link #detail}.
	 */
	public void onChangeResponsabileCopiaConforme() {
		try {
			String id = detail.getResponsabileCopiaConforme().getId();
			List<ResponsabileCopiaConformeDTO> list = documentManagerDTO.getComboResponsabileCopiaConforme();
			ResponsabileCopiaConformeDTO responsabileCopiaConforme = documentManagerSRV.selectResponsabileById(id, list);

			detail.setResponsabileCopiaConforme(responsabileCopiaConforme);
			setAssegnatarioCopiaConforme(responsabileCopiaConforme);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

	}

	private void setAssegnatarioCopiaConforme(ResponsabileCopiaConformeDTO responsabileCopiaConforme) {
		descrAssegnatarioIterManuale = responsabileCopiaConforme.getDescrizioneNodo();

		UfficioDTO uff = new UfficioDTO(responsabileCopiaConforme.getIdNodo(), responsabileCopiaConforme.getDescrizioneNodo());
		UtenteDTO ute = null;
		if (responsabileCopiaConforme.getIdUtente() != null) {
			descrAssegnatarioIterManuale += " - " + responsabileCopiaConforme.getNome() + " " + responsabileCopiaConforme.getCognome();
			ute = new UtenteDTO();
			ute.setId(responsabileCopiaConforme.getIdUtente());
			ute.setIdUfficio(responsabileCopiaConforme.getIdNodo());
			ute.setNome(responsabileCopiaConforme.getNome());
			ute.setCognome(responsabileCopiaConforme.getCognome());
			ute.setIdRuolo(responsabileCopiaConforme.getIdRuolo().longValue());
			ute.setIdAoo(responsabileCopiaConforme.getIdAOO().longValue());
		}

		// L'assegnazione per Iter Manuale Copia Conforme è per FIRMA
		assegnatarioPrincipale = new AssegnazioneDTO(null, TipoAssegnazioneEnum.FIRMA, null, null, descrAssegnatarioIterManuale, ute, uff);
	}
	/** RESPONSABILE COPIA CONFORME DOCUMENTO PRINCIPALE END ******************************************************************************************************************************************************/	

	/**BARCODE PANEL START******************************************************************************/
	public void scannerizzaDocumento() {
		LOGGER.error(
				"// 1 - importare AppletScan.jar da RED\r\n" + 
				"// 2 - importare il javascript per la creazione dell'applet\r\n" + 
				"// 3 - legare il comando javascript all'esecuzione di questo metodo\r\n" + 
				"// 4 - speriamo che funzioni...\r\n");
	}
	/**BARCODE PANEL END********************************************************************************/

	/** SPEDIZIONE START**************************************************************************************************/
	/**
	 * Gestisce l'evento di modifica del testo predifinito mail.
	 */
	public void onChangeTestoPredefinitoMail() {
		TestoPredefinitoDTO testoSelected = null;
		for (TestoPredefinitoDTO t : this.documentManagerDTO.getComboTestiPredefiniti()) {
			if (t.getIdTestoPredefinito().equals(this.idTestoPredefinitoMail)) {
				testoSelected = t;
				break;
			}
		}

		if (testoSelected == null || testoSelected.getIdTestoPredefinito() == 0 ) {
			this.detail.getMailSpedizione().setTesto("Si trasmette il documento di cui all'oggetto.");
			return;
		}

		this.detail.getMailSpedizione().setTesto(testoSelected.getCorpotesto());

	}
	/** SPEDIZIONE END****************************************************************************************************/

	/** TAB DOCUMENTI START **************************************************************************************************************/

	/**
	 * Sembra che questo metodo non venga mai utilizzato.
	 * @param event
	 */
	public void changeTabs(TabChangeEvent event) {
		try {
			String id = event.getTab().getId();
			if ("idTabDocumenti_DMD".equalsIgnoreCase(id)) {
				reloadTabDocumenti();
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Nel caso utenti differenti aggiungano allegati il sistema deve mostrare correttamente i cambiamenti.
	 */
	public void reloadTabDocumenti() {
		try { 
			boolean isRicercaRiservato = false;
			if(NavigationTokenEnum.RICERCA_DOCUMENTI.equals(sessionBean.getActivePageInfo()) &&
					PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.RICERCA_RISERVATO)) {
				isRicercaRiservato = true;
			} 
			/**GESTIONE VERSIONI DEL DOCUMENTO PRINCIPALE E  DEI SUOI ALLEGATI START.********************************************/
			// Si recupera la lista delle versioni del documento e dei suoi eventuali allegati
			detail.setVersioni(documentoRedSRV.getVersioniDocumento(detail.getGuid(), detail.getIdAOO(), utente,isRicercaRiservato));
			if (detail.getAllegati() != null && !detail.getAllegati().isEmpty()) {
				for (AllegatoDTO allegatoDTO : detail.getAllegati()) { 
					allegatoDTO.setVersions(documentoRedSRV.getVersioniDocumentoAllegato(allegatoDTO.getGuid(), detail.getIdAOO(), detail.getIdCategoriaDocumento(), utente,isRicercaRiservato, detail.getCodiceFlusso()));
				}
			}

			versioniAllegato.refresh();

		} catch (Exception red) {
			LOGGER.error("Errore durante la sincronizzazione del documento", red);
			showErrorMaschera("Errore durante la sincronizzazione degli allegati e delle versioni");
		}
		/**GESTIONE VERSIONI DEL DOCUMENTO PRINCIPALE E  DEI SUOI ALLEGATI END.**********************************************/
	}
	/** TAB DOCUMENTI END ****************************************************************************************************************/	

	/**REGISTRA******************************************************************************************/

	public void centroValidazioni() {
		Boolean nessunLockPresente = true;
		GestioneLockDTO gestLockDTO = null;
		if(documentManagerDTO.isInModificaIngresso() || documentManagerDTO.isInModificaUscita()) {
			gestLockDTO = documentoSRV.getLockDocumento(Integer.valueOf(detail.getDocumentTitle()), utente.getIdAoo(), 1, utente.getId());
			nessunLockPresente = gestLockDTO.getNessunLock();
			if(Boolean.FALSE.equals(nessunLockPresente)) {
				nessunLockPresente = gestLockDTO.isInCarico() && detail.getModificabile();
			}
		}
		
		if(Boolean.FALSE.equals(nessunLockPresente)) {
			nomeUtenteLockatore = gestLockDTO.getUsernameUtenteLock();
			if(!(this.getDetail().getModificabile()==null || !this.getDetail().getModificabile())) {
				FacesHelper.update("idDettagliEstesiForm:sbloccaDocumentoDMDDlg");
				FacesHelper.executeJS("PF('sbloccaDocumentoDMDDlg_WV').show();");
			}else {
				FacesHelper.update("idDettagliEstesiForm:impedisciModificaDocumentoDMDDlg");
				FacesHelper.executeJS("PF('impedisciModificaDocumentoDMDDlg_WV').show();");
			}
		} else {
			sbloccaLock(documentManagerDTO.isInModificaIngresso() || documentManagerDTO.isInModificaUscita());
		}
	
	}

	/**
	 * Gestisce il front-end dello sblocco di un documento.
	 */
	public void sbloccaLock() {
		sbloccaLock(true);
		FacesHelper.executeJS("PF('sbloccaDocumentoDlg_WV').hide();");
		nomeUtenteLockatore = "";
	}

	/**
	 * Gestisce lo sblocco del lock, sbloccando eventualmente anche il documento.
	 * @param sbloccaDocumento
	 */
	public void sbloccaLock(Boolean sbloccaDocumento) {
		try {
			if(sbloccaDocumento!=null && sbloccaDocumento) {
				documentoSRV.updateLockDocumento(Integer.valueOf(detail.getDocumentTitle()), utente.getIdAoo(), 1,detail.getModificabile().booleanValue(), utente.getId(), utente.getNome(), utente.getCognome());
			}

			if((documentManagerDTO.isInModificaIngresso() || documentManagerDTO.isInModificaUscita()) && !detail.getModificabile().booleanValue() && PermessiUtils.haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.MODIFICA_METADATI_MINIMI, utente)) {
				IListaDocumentiFacadeSRV listaSRV = ApplicationContextProvider.getApplicationContext().getBean(IListaDocumentiFacadeSRV.class);
				String s = listaSRV.getWorkflowPrincipale(detail.getDocumentTitle(), utente);
				if(s==null) {
					throw new RedException ("Il documento è stato chiuso e non è più modificabile");
				}
			}
			
			//			<-- Blocco Validazioni Campi -->
			Map<String, Object> esitoPreliminare = getValidatorEngineResult();

			if ((boolean)esitoPreliminare.get(DocumentManagerValidatorEngine.CHIAVE_ESITO)) {
				showWarningMaschera(esitoPreliminare.get(DocumentManagerValidatorEngine.CHIAVE_ESITO_TESTO).toString());
				return;
			}
			//			<-- Fine Blocco Validazioni Campi -->	
 
			if((documentManagerDTO.isInCreazioneUscita() || documentManagerDTO.isInModificaUscita()) && 
					detail.getMaxSizeContent()!=null && (detail.getSizeContent() > detail.getMaxSizeContent())) {
				showErrorMaschera("La dimensione dei file contenuti nel procedimento supera la dimensione massima consentita dalla casella di posta mittente. Occorre diminuire la dimensione totale dei file caricati per registrare le modifiche.");
				return;
			}
			
			//			<-- Blocco Check Protocollo Emergenza -->
			if (TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo()) && 
					(MomentoProtocollazioneEnum.PROTOCOLLAZIONE_NON_STANDARD.equals(detail.getMomentoProtocollazioneEnum())
							|| (detail.getDaNonProtocollare() != null && !detail.getDaNonProtocollare()))) {


				protEm = new ProtocolloEmergenzaDocDTO();

				protEm.setAnnoProtocolloEmergenza(Calendar.getInstance().get(Calendar.YEAR)); 
				protEm.setDataProtocolloEmergenza(new Date());

				FacesHelper.update("idDettagliEstesiForm:idPnlProtEmrCreazione");
				PrimeFaces.current().executeScript("PF('wdgProtEmergenza').show()");
				PrimeFaces.current().executeScript("PF('wdgProtEmergBtn').disable()");
				//			<-- Fine Check Protocollo Emergenza -->
			} else if (this.documentManagerDTO.isInCreazioneIngresso()) {
				// Se il protocollo associato all'AOO non è di tipo emergenza si verifica l'assegnatario per competenza gestendo gli automatismi esistenti
				loadAssegnatarioFromAutomazione();
				
				if (this.getAssegnatarioDaAutomatismo() != null && this.getAssegnatarioDaAutomatismo().getUfficio().getId() != null
						&& !this.getAssegnatarioDaAutomatismo().getUfficio().getId().equals(0L)) {
					if (this.getAssegnatarioDaAutomatismo().getUfficio().getId().equals(this.getAssegnatarioPrincipale().getUfficio().getId())) {
						if(this.getAssegnatarioDaAutomatismo().getUtente() != null) {
							if(this.getAssegnatarioPrincipale().getUtente() != null && this.getAssegnatarioPrincipale().getUtente().getId().equals(this.getAssegnatarioDaAutomatismo().getUtente().getId())) {
								if(!StringUtils.isNullOrEmpty(getMsgPredisponi()) && utente.isUcb() && detail.getIdTipologiaDocumento().equals(Integer.parseInt(PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "." + PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_USCITA_ATTO_DECRETO.getKey())))) {
									PrimeFaces.current().executeScript(SHOW_WDGERRORMESSAGE_JS);
								} else {
									registra();
								}
							} else {
								PrimeFaces.current().executeScript(SHOW_WDGCONFERMAASSEGNAZIONE_JS);
							}
						} else if (this.getAssegnatarioPrincipale().getUtente() != null) {
							PrimeFaces.current().executeScript(SHOW_WDGCONFERMAASSEGNAZIONE_JS);
						} else {
							if(!StringUtils.isNullOrEmpty(getMsgPredisponi()) && utente.isUcb() && detail.getIdTipologiaDocumento().equals(Integer.parseInt(PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "." + PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_USCITA_ATTO_DECRETO.getKey())))) {
								PrimeFaces.current().executeScript(SHOW_WDGERRORMESSAGE_JS);
							} else {
								registra();
							}
						}
						
					} else {
						PrimeFaces.current().executeScript(SHOW_WDGCONFERMAASSEGNAZIONE_JS);
					}
				} else {
					if(!StringUtils.isNullOrEmpty(getMsgPredisponi()) && utente.isUcb() && detail.getIdTipologiaDocumento().equals(Integer.parseInt(PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "." + PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_USCITA_ATTO_DECRETO.getKey())))) {
						PrimeFaces.current().executeScript(SHOW_WDGERRORMESSAGE_JS);
					} else {
						registra();
					}
					
				}
			
			} else {
				registra();
			} 
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	private Map<String, Object> getValidatorEngineResult() {
		detail.setDestinatari(getDestinatariInModificaPuliti());

		detail.setAllacci(riferimentiProt);
 
		detail.setAllaccioRifStorico(riferimentoStoricoNsd);
		 
		detail.setAllaccioRifNps(riferimentoProtNpsDTO);
		if (sd == null) {
			sd = new SalvaDocumentoRedParametriDTO();
		}

		if (documentManagerDTO.isInModifica()) {
			if (docPrincipale != null && !StringUtils.isNullOrEmpty(docPrincipale.getFileName())) {
				//significa che l'utente ha inserito un documento a mano in fase di modifica
				sd.setContentVariato(true);
			}
		} else {
			sd.setContentVariato(true);
		}
		// Modalita registrazione documento
		sd.setModalita(calcolaModalitaRegistra());

		/**ASSEGNAZIONI START.********************************************************************/
		//se il documento è in creazione vanno calcolate le assegnazioni, 
		//altrimenti devono rimanere così come sono
		if (!this.documentManagerDTO.isInModifica()) {
			//ricostruisco le assegnazioni da zero e le sostituisco a quelle precedenti
			List<AssegnazioneDTO> newAssegnazioni = new ArrayList<>();

			if (assegnatarioPrincipale != null) {
				//se l'iter approvativo non è manuale 
				newAssegnazioni.add(assegnatarioPrincipale);
			}

			//aggiungo i facoltativi
			if (this.assegnatariPerConoscenza != null && !this.assegnatariPerConoscenza.isEmpty()) {
				newAssegnazioni.addAll(assegnatariPerConoscenza);
			}
			if (this.assegnatariPerContributo != null && !this.assegnatariPerContributo.isEmpty()) {
				newAssegnazioni.addAll(assegnatariPerContributo);
			}

			detail.setAssegnazioni(newAssegnazioni);
		}
		/**ASSEGNAZIONI END.********************************************************************/


		if (documentManagerSRV.isRaccoltaFadVisible(detail)) {
			detail.setIdraccoltaFAD(identificativoRaccolta);
		}

		this.detail.setAllegati(this.tabAllegati.getAllegati());

		/** INFO MAIL SPEDIZIONE, ALLEGATI ETC START.**********************************************************************************/
		if (documentManagerDTO.isTabSpedizioneVisible() && !documentManagerDTO.isTabSpedizioneDisabled() && detail.getMailSpedizione() != null) {

			//DESTINATARI MAIL TO/CC START.
			StringBuilder to = new StringBuilder();
			StringBuilder cc = new StringBuilder();
			for (DestinatarioRedDTO d : detail.getDestinatari()) {

				if (d.getMezzoSpedizioneEnum() == MezzoSpedizioneEnum.ELETTRONICO) {
					switch (d.getModalitaDestinatarioEnum()) {
					case CC:
						cc.append(d.getContatto().getMailSelected()).append(";");
						break;
					case TO:
						to.append(d.getContatto().getMailSelected()).append(";");
						break;
					default:
						break;
					}
				}
			}

			this.detail.getMailSpedizione().setDestinatari(!StringUtils.isNullOrEmpty(to.toString()) ? to.substring(0, to.length() - 1) : null); 
			this.detail.getMailSpedizione().setDestinatariCC(!StringUtils.isNullOrEmpty(cc.toString()) ? cc.substring(0, cc.length() - 1) : null);
			//DESTINATARI MAIL TO/CC END.

			if (allegatiMailSelected != null && !allegatiMailSelected.isEmpty()) {
				List<String> ids = new ArrayList<>();
				for (DetailDocumentRedDTO allegatoMail : allegatiMailSelected) {
					ids.add(allegatoMail.getDocumentTitle());
				}
				this.detail.getMailSpedizione().setIdAllegati(ids);
			}
		}

		/** INFO MAIL SPEDIZIONE, ALLEGATI ETC END.************************************************************************************/

		/** PROTOCOLLAZIONE DA MAIL START. *******************************************************/
		if (!StringUtils.isNullOrEmpty(this.detail.getProtocollazioneMailGuid())) {
			sd.setInputMailGuid(this.detail.getProtocollazioneMailGuid());
		}

		if (detail.getIsNotifica() != null && detail.getIsNotifica()) {
			sd.setInviaNotificaProtocollazioneMail(detail.getIsNotifica());
		}
		/** PROTOCOLLAZIONE DA MAIL END. *******************************************************/

		/*** MEZZO DI RICEZIONE PER UN DOCUMENTO IN INGRESSO START.*****************************************************************************************************/
		if ((this.documentManagerDTO.isInCreazioneIngresso() || this.documentManagerDTO.isInModificaIngresso()) 
				&& (this.detail.getMezzoRicezione() != null && this.detail.getMezzoRicezione().intValue() == 0)) {
			this.detail.setMezzoRicezione(null);
		}
		/*** MEZZO DI RICEZIONE PER UN DOCUMENTO IN INGRESSO END.*****************************************************************************************************/

		/** RISERVATO START.*************************/
		this.detail.setRiservato(null);
		if (this.documentManagerDTO.isRiservato()) {
			this.detail.setRiservato(1);
		}
		/** RISERVATO END.***************************/

		/** Controlla se lato front end tutti i valori obbligatori sono stati inseriti */
		dmve = new DocumentManagerValidatorEngine(utente, documentManagerDTO, detail, documentManagerSRV);
		return dmve.checkPreliminareFrontEnd(assegnatarioPrincipale, this.tabAllegati.getAllegati(), documentoSRV);
	}

	/**
	 * Effettua un tentativo di registrazione comunicando eventuali warning o errori a valle del tentativo. Se l'esito
	 * della registrazione è positivo mostra un messaggio comunicativo all'utente.
	 */
	public void registra() {
		try {
			if (oldIdFascicoloProcedimentale != null && !oldIdFascicoloProcedimentale.equals(detail.getIdFascicoloProcedimentale()) && fascicoloSRV.verificaFascicoloFlusso(oldIdFascicoloProcedimentale, utente)) {
				showWarnMessage("Operazione non consentita: non è possibile spostare documenti da un fascicolo dedicato a un flusso automatico.");
				return;
			}

			if (TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo())) {
				detail.setNumeroProtocolloEmergenza(protEm.getNumeroProtocolloEmergenza());
				detail.setAnnoProtocolloEmergenza(protEm.getAnnoProtocolloEmergenza());
				detail.setDataProtocolloEmergenza(protEm.getDataProtocolloEmergenza());
			}

			String indiceClass = StringUtils.getDescrizioneIndiceClassificazione(indiceDiClassificazione.getDescrizioneTitolario());
			
			if (documentManagerDTO.isInModifica()) {
				if (detail.getDescrizioneTitolarioFascicoloProcedimentale() != null) {
					sd.setTitolarioChanged(!detail.getDescrizioneTitolarioFascicoloProcedimentale().equals(indiceClass));
				} else {
					sd.setTitolarioChanged(detail.getDescrizioneTitolarioFascicoloProcedimentale() != null || !StringUtils.isNullOrEmpty(indiceClass));
				}
			}
			detail.setDescrizioneTitolarioFascicoloProcedimentale(indiceClass);

			boolean spedizioneMail = TipoAssegnazioneEnum.SPEDIZIONE.equals(detail.getTipoAssegnazioneEnum()) && (detail.getIdIterApprovativo()==null || detail.getIdIterApprovativo()==0);
			detail.setSpedizioneMail(spedizioneMail);
			
			EsitoSalvaDocumentoDTO esito = salvaDocumentoSRV.salvaDocumento(detail, sd, utente, ProvenienzaSalvaDocumentoEnum.GUI_RED);

			if (esito.isEsitoOk()) {
				messaggioEsitoRegistra.clear();
				boolean documentoInCoda = true;

				//Se sono in ricerca aggiorno sempre 
				if (ConstantsWeb.MBean.MAIL_BEAN.equalsIgnoreCase(chiamante)) {
					aggiornaPagineChiamanti(esito);
					
					//ribaltamento nota
					if(detail.isRibaltaTitolario() && !StringUtils.isNullOrEmpty(detail.getIndiceClassificazioneFascicoloProcedimentale())) {
						INotaFacadeSRV notaSRV = ApplicationContextProvider.getApplicationContext().getBean(INotaFacadeSRV.class);
						NotaDTO nota = new NotaDTO();
						nota.setColore(ColoreNotaEnum.NERO);
						nota.setContentNota(detail.getIndiceClassificazioneFascicoloProcedimentale()+" - "+detail.getDescrizioneTitolarioFascicoloProcedimentale());
						notaSRV.registraNotaFromWorkflow(utente, esito.getWobNumber(), nota, esito.getNumeroDocumento(), false);
					}
					
				} else {
					if (!ManagedBeanName.RICERCA_BEAN.equalsIgnoreCase(chiamante) && !MBean.SOTTOSCRIZIONI_BEAN.equalsIgnoreCase(chiamante) 
							&& !NavigationTokenEnum.RICERCA_DOCUMENTI.equals(sessionBean.getActivePageInfo())
							&& !NavigationTokenEnum.RICERCA_RAPIDA.equals(sessionBean.getActivePageInfo())
							&& this.documentManagerDTO.isInModifica()) {
						DocumentQueueEnum queueEnum = sessionBean.getActivePageInfo().getDocumentQueue();
						IListaDocumentiFacadeSRV listaDocSRV = ApplicationContextProvider.getApplicationContext().getBean(IListaDocumentiFacadeSRV.class);
						documentoInCoda = listaDocSRV.isDocumentoInCoda(utente, detail.getDocumentTitle(), queueEnum);
					}

					if (documentoInCoda) {
						aggiornaPagineChiamanti(esito);
					} else {
						FacesHelper.executeJS("PF('dlgManageDocument').hide()");
						ListaDocumentiBean ldbean = FacesHelper.getManagedBean(MBean.LISTA_DOCUMENTI_BEAN);
						if (ldbean.getDocumentiDTH() != null && ldbean.getDocumentiDTH().getCurrentMaster() != null) {

							ldbean.refresh();

							FacesHelper.update("centralSectionForm:idInfoPanel");
							FacesHelper.update("centralSectionForm:documenti");
							FacesHelper.update(IDDETTAGLIODOCUMENTO_RESOURCE_LOCATION);
							FacesHelper.update("centralSectionForm:pnlQueueList");
							FacesHelper.update("centralSectionForm:searchAllBtn");
						}

						showInfoMessage("Il documento è stato aggiornato con successo.");
					}
				}
				sd = null; 
				if(documentManagerDTO.isInCreazioneUscita() &&
						(utente.getIsEstendiVisibilita() && isEstendiVisibilitaChk)) {
					LOGGER.info("START INSERT IN TABELLA ALLACCIO_DOC_USCITA");
					AllaccioDocUscitaDTO allaccioDocUscitaDTO = new AllaccioDocUscitaDTO();
					if(esito.getDocumentTitle()!=null) {
						allaccioDocUscitaDTO.setDocTitleUscita(esito.getDocumentTitle());
					}  
					if(detail.getAllacci()!=null) {
						List<String> docIngressoAllacciati = new ArrayList<>();
						for(RispostaAllaccioDTO allaccio :detail.getAllacci()) {
							docIngressoAllacciati.add(allaccio.getIdDocumentoAllacciato());
						}
						if(docIngressoAllacciati!=null) {
							allaccioDocUscitaDTO.setDocTitleIngressoList(docIngressoAllacciati);
						}
					}
					allaccioDocUscitaDTO.setIdAoo(Integer.parseInt(""+utente.getIdAoo()));
					allaccioDocUscitaDTO.setStato(AllaccioDocUscitaEnum.DA_LAVORARE.getStato()); 
					allaccioDocUscitaDTO.setDataCreazione(new Date());
					allaccioDocUscitaDTO.setDataAggiornamento(new Date());
					allacciaDocUscitaSRV.estendiVisibilitaDocumentiRisposta(allaccioDocUscitaDTO);
					LOGGER.info("END INSERT IN TABELLA ALLACCIO_DOC_USCITA");
				}
				 
				if(documentManagerDTO.isInModificaUscita() && utente.getShowRiferimentoStorico() && detail.getAllaccioRifStorico()!=null) {
					riferimentoStoricoSRV.deleteRiferimentoStoricoModifica(esito.getDocumentTitle(),sessionBean.getUtente().getIdAoo());
				} 
				if((documentManagerDTO.isInCreazioneUscita() || documentManagerDTO.isInModificaUscita()) &&
						(utente.getShowRiferimentoStorico())) {
					LOGGER.info("START INSERT IN TABELLA ALLACCIO_RIFERIMENTO_STORICO");

					List<AllaccioRiferimentoStoricoDTO> allaccioRifStoricoDTOList = new ArrayList<>();
					 
					if(detail.getAllaccioRifStorico()!=null) {
						String fascProcedimentale = this.detail.getIdFascicoloProcedimentale();
						if(StringUtils.isNullOrEmpty(fascProcedimentale)) {
							fascProcedimentale = detailFascicoloSRV.getFascicoloProcedimentale(esito.getDocumentTitle(), sessionBean.getUtente().getIdAoo().intValue(),sessionBean.getUtente().getFcDTO());
						}
						
						for(RiferimentoStoricoNsdDTO allaccioRifStorico : detail.getAllaccioRifStorico()) {
							AllaccioRiferimentoStoricoDTO allaccioRifStoricoDTO = new AllaccioRiferimentoStoricoDTO();
							allaccioRifStoricoDTO.setDocTitleUscita(esito.getDocumentTitle());
							allaccioRifStoricoDTO.setAnnoProtocollo(allaccioRifStorico.getAnnoProtocolloNsd());
							allaccioRifStoricoDTO.setnProtocolloNsd(String.valueOf(allaccioRifStorico.getNumeroProtocolloNsd()));
							allaccioRifStoricoDTO.setOggetto(allaccioRifStorico.getOggettoProtocolloNsd());
							allaccioRifStoricoDTO.setDataCreazione(new Date());
							allaccioRifStoricoDTO.setDataAggiornamento(new Date());
							allaccioRifStoricoDTO.setIdFilePrincipale(allaccioRifStorico.getIdDocumentoPrincipale());
							allaccioRifStoricoDTO.setIdFascProcedimentale(fascProcedimentale);
							allaccioRifStoricoDTO.setDescFascProcedimentale(this.detail.getDescrizioneFascicoloProcedimentale());
							allaccioRifStoricoDTO.setIdAoo(sessionBean.getUtente().getIdAoo());
							allaccioRifStoricoDTOList.add(allaccioRifStoricoDTO);
						}
						riferimentoStoricoSRV.inserisciRiferimentoStorico(allaccioRifStoricoDTOList);
						LOGGER.info("END INSERT IN TABELLA ALLACCIO_DOC_USCITA");
					}
				}
				 
				if(((PermessiUtils.isAmministratore(utente.getPermessi()) && PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.RICERCA_NPS)) || 
						(PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.RICERCANPSSENZAACL) && PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.RICERCA_NPS)) || 
						sessionBean.haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.RICERCAPROTOCOLLO))
						&& (documentManagerDTO.isInCreazioneUscita() || (documentManagerDTO.isInModificaUscita() && 
						Boolean.TRUE.equals(this.detail.getModificabile())))) {
					
					if(documentManagerDTO.isInModificaUscita() && detail.getAllaccioRifNps() != null) {
						riferimentoStoricoSRV.deleteRiferimentoAllaccioNpsModifica(esito.getDocumentTitle(),utente.getIdAoo());
					}  
					insertRiferimentoAllaccioNps(utente.getIdAoo(),esito.getDocumentTitle());
				}
				
				disabilitaRispostaProtocollo = false;
			} else {
				// DocumentManagerValidatorEngine controlla se ci sono errori o warning e li mostra all'utente a valle del tentativo di registrazione
				dmve.checkErrors(esito);
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	private void insertRiferimentoAllaccioNps(final Long idAoo,final String documentTitleUscita) {
		LOGGER.info("START INSERT IN TABELLA ALLACCIO_NPS");
 		 
		if(detail.getAllaccioRifNps()!=null) {
			String fascProcedimentale = this.detail.getIdFascicoloProcedimentale();
			if(StringUtils.isNullOrEmpty(fascProcedimentale)) {
				fascProcedimentale = detailFascicoloSRV.getFascicoloProcedimentale(documentTitleUscita, sessionBean.getUtente().getIdAoo().intValue(),sessionBean.getUtente().getFcDTO());
			} 
			
			riferimentoStoricoSRV.insertRifAllaccioNps(idAoo,documentTitleUscita,detail.getAllaccioRifNps(),fascProcedimentale);
			LOGGER.info("END INSERT IN TABELLA TABELLA ALLACCIO_NPS");
		}
	}

	/**
	 * Rispetto al messaggio di conferma forzo un valore piuttosto che un'altro
	 * @param message
	 */
	public void confermaRegistra(String message) {

		if (sd == null) {
			sd = new SalvaDocumentoRedParametriDTO();
		}

		//per sicurezza li resetto, solo uno alla volta può forzare
		sd.setForzaAssSpedizioneDestElettronici(false);
		sd.setForzaAttoDecretoManuale(false);
		sd.setForzaProtocollazioneMail(false);
		sd.setForzaRispostaPubblica(false);

		if (SalvaDocumentoErroreEnum.DOC_ID_RACCOLTA_FAD_NON_PRESENTE.getMessaggio().equals(message)) {
			sd.setForzaAttoDecretoManuale(true);
		} else if (SalvaDocumentoErroreEnum.DOC_ID_RACCOLTA_FAD_PRESENTE.getMessaggio().equals(message)) {
			sd.setForzaAttoDecretoManuale(true);
		} else if (SalvaDocumentoErroreEnum.DOC_ASSEGNAZIONE_SPEDIZIONE_DESTINATARIO_ELETTRONICO.getMessaggio().equals(message)) {
			for (DestinatarioRedDTO dest : getDestinatariInModificaPuliti()) {
				dest.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.CARTACEO);
			}
			documentManagerDTO.setTabSpedizioneDisabled(true);
			idTestoPredefinitoMail = null;
			sd.setForzaAssSpedizioneDestElettronici(true);
		} else if (SalvaDocumentoErroreEnum.DOC_PUBBLICO_ALLACCIO_RISERVATO.getMessaggio().equals(message)) {
			sd.setForzaRispostaPubblica(true);
		} else if (!StringUtils.isNullOrEmpty(message) && message.startsWith("Attenzione, la mail è già stata presa in carico dall' utente ")) {
			sd.setForzaProtocollazioneMail(true);
		} else {
			showErrorMaschera("Errore, forzatura non individuata");
			return;
		}

		registra();
	}

	/**
	 * Torna sulla pagina di creazione con un documento precompilato dei dati:
	 * Documento in uscita - Tipologia documento/Tipo procedimento/Destinatari/Oggetto
	 * Documento in entrata - Tipologia documento/Tipo procedimento/Mittente/Oggetto/Assegnatario per competenza/Indice di classificazione.
	 * @return
	 */
	public void ripetiDati() {
		Integer idCategoria = this.detail.getIdCategoriaDocumento();

		DetailDocumentRedDTO d = new DetailDocumentRedDTO();
		d.setIdCategoriaDocumento(idCategoria);
		d.setIdTipologiaDocumento(this.detail.getIdTipologiaDocumento());
		Integer idTipologiaProcedimento = this.detail.getIdTipologiaProcedimento();
		String descTipoProcedimento = this.detail.getDescTipoProcedimento();
		d.setDestinatari(this.detail.getDestinatari());
		d.setMittenteContatto(this.detail.getMittenteContatto());
		d.setMittente(this.detail.getMittente());
		d.setOggetto(this.detail.getOggetto());
		
		// Nella ripetizione dati occorre recuperare solo l'assegnatario per competenza
		List<AssegnazioneDTO> assegnazioniDaRipetere = new ArrayList<>();
		if(!CollectionUtils.isEmpty(this.detail.getAssegnazioni())) {
			for(AssegnazioneDTO ass : this.detail.getAssegnazioni()) {
				if(TipoAssegnazioneEnum.COMPETENZA.equals(ass.getTipoAssegnazione())) {
					assegnazioniDaRipetere.add(ass);
				}
			}
		}
		
		d.setAssegnazioni(assegnazioniDaRipetere);

		String indiceClassificazione = this.detail.getIndiceClassificazioneFascicoloProcedimentale();
		d.setIndiceClassificazioneFascicoloProcedimentale(indiceClassificazione);

		d.setDescrizioneTitolarioFascicoloProcedimentale(this.detail.getDescrizioneTitolarioFascicoloProcedimentale());
		
		d.setFormatoDocumentoEnum(this.detail.getFormatoDocumentoEnum());

		idTestoPredefinitoMail = null;
		setDetail(d);

		resetMetadatiDinamici();
		onChangeComboTipologiaDocumento();
		
		detail.setIdTipologiaProcedimento(idTipologiaProcedimento);
		d.setDescTipoProcedimento(descTipoProcedimento);
		idTipoProcedimentoSelected = idTipologiaProcedimento.longValue();
		
		onChangeComboTipiProcedimento();
		
		FacesHelper.update("idDettagliEstesiForm:idPanelContainerDocumentManager_T");
	}

	private void resetMetadatiDinamici() {
		if (mapMetadati != null) {
			List<MetadatoDinamicoDTO> metadatiDinamici = mapMetadati.get(detail.getIdTipologiaDocumento());
			if (metadatiDinamici != null) {
				for (MetadatoDinamicoDTO m: metadatiDinamici) {
					if (!metadatiDinamiciToCopy.contains(m.getChiave())) {
						m.setValore(null);
					}
				}
			}
		}
	}

	/**
	 * Resetta il valore di un metadato dinamico con la specifica chiave.
	 * @param chiave
	 */
	public void pulisciDataMetadatoDinamico(String chiave) {
		Collection<MetadatoDinamicoDTO> metadatiDinamici = detail.getMetadatiDinamici();
		for (MetadatoDinamicoDTO m: metadatiDinamici) {
			if (m.getChiave().equals(chiave)){
				m.setValore(null);
			}
		}
	}

	/**
	 * Effettua un reset della data protocollo mittente.
	 */
	public void pulisciDataProtocolloMittente(){
		detail.setDataProtocolloMittente(null);
	}

	/**
	 * Effettua un reset della data di scadenza.
	 */
	public void pulisciDataScadenza() {
		detail.setDataScadenza(null);
	}

	private String calcolaModalitaRegistra() {
		String modalitaRegistra;

		// Protocollazione mail
		if (!StringUtils.isNullOrEmpty(detail.getProtocollazioneMailGuid())) {
			modalitaRegistra = Modalita.MODALITA_INS_MAIL;
			// Post-scansione
		} else if (MBean.LISTA_CARTACEI_ACQUISITI_BEAN.equals(chiamante)) {
			modalitaRegistra = Modalita.MODALITA_INS_POSTCENS;
		} else {
			modalitaRegistra = documentManagerDTO.getModalitaMaschera();
		}

		return modalitaRegistra;
	}

	private void aggiornaPagineChiamanti(EsitoSalvaDocumentoDTO esito) {
		// Messo null perchè non è detto che la classe documentale di partenza, come era settato prima, sia quella giusta..vedi atto decreto a partire
		// da tipologia generica. con Null usa documento_NSD. Rivedere
		doc = documentoRedSRV.getDocumentDetail(esito.getDocumentTitle(), utente, this.detail.getWobNumberSelected(), null, this.detail.getRiservato()!=null && this.detail.getRiservato() == 1);

		if (this.documentManagerDTO.isInModifica() && !ConstantsWeb.MBean.MAIL_BEAN.equals(chiamante)) {
			// Devo aggiornare le pagine da cui sono stato aperto
			Collection<MasterDocumentRedDTO> mastersToUpdate = null;

			ListaDocumentiBean ldbean = null;
			MasterDocumentRedDTO masterUpdated = null;
			Integer idTipologiaDocumentoContributoEsternoEntrata = null;
			if (MBean.DETTAGLIO_DOCUMENTO_BEAN.equalsIgnoreCase(chiamante) || MBean.FASCICOLO_MANAGER_BEAN.equalsIgnoreCase(chiamante)) {
				if (NavigationTokenEnum.RICERCA_RAPIDA.equals(sessionBean.getActivePageInfo())) {
					RicercaRapidaBean rrb = FacesHelper.getManagedBean(MBean.RICERCA_RAPIDA_BEAN);
					mastersToUpdate = rrb.getDocumentiDTH().getMasters();
				} else if (NavigationTokenEnum.RICERCA_DOCUMENTI.equals(sessionBean.getActivePageInfo())) {
					RicercaDocumentiRedBean rdrb = FacesHelper.getManagedBean(MBean.RICERCA_DOCUMENTI_RED_BEAN);
					mastersToUpdate = rdrb.getDocumentiRedDTH().getMasters();
				} else {
					ldbean = FacesHelper.getManagedBean(MBean.LISTA_DOCUMENTI_BEAN);
					mastersToUpdate = ldbean.getDocumentiDTH().getMasters();
					String idTipologiaDocumentoContributoEsternoEntrataStr = PropertiesProvider.getIstance()
							.getParameterByString(utente.getCodiceAoo() + "." + Varie.CONTRIBUTO_ESTERNO_ENTRATA_ID_KEY_SUFFIX);
					if (!StringUtils.isNullOrEmpty(idTipologiaDocumentoContributoEsternoEntrataStr)) {
						idTipologiaDocumentoContributoEsternoEntrata = Integer.parseInt(idTipologiaDocumentoContributoEsternoEntrataStr);
					}
				}

			}  

			// Se esistono i masters vado a prendere la riga che mi interessa e la modifico
			if (mastersToUpdate != null && !mastersToUpdate.isEmpty()) {
				for (MasterDocumentRedDTO  md : mastersToUpdate) {
					if (md.getDocumentTitle().equals(doc.getDocumentTitle())) {
						md.setNumeroDocumento(doc.getNumeroDocumento());
						md.setOggetto(doc.getOggetto());
						md.setTipologiaDocumento(doc.getDescTipologiaDocumento());
						md.setIdTipologiaDocumento(doc.getIdTipologiaDocumento());
						md.setIdTipoProcedimento(doc.getIdTipologiaProcedimento());
						md.setNumeroProtocollo(doc.getNumeroProtocollo());
						md.setAnnoProtocollo(doc.getAnnoProtocollo());
						md.setDataScadenza(doc.getDataScadenza());
						md.setUrgenza(doc.getUrgente() ? 1 : 0);
						md.setIdCategoriaDocumento(doc.getIdCategoriaDocumento());
						md.setFlagEnableFirmaAutografa(doc.isFlagFirmaAutografaRM());
						md.setTipoProtocollo(doc.getTipoProtocollo());
						boolean flagRiservato = (doc.getRiservato() != null && doc.getRiservato().intValue() == 1);
						md.setFlagRiservato(flagRiservato);
						md.setTipoProcedimento(doc.getDescTipoProcedimento());
						TipoSpedizioneDocumentoEnum tsd = TipoSpedizioneDocumentoEnum.CARTACEO;
						if (doc.getDestinatari() != null) {
							for (DestinatarioRedDTO desti : doc.getDestinatari()) {
								if (desti.getMezzoSpedizioneEnum() != null 
										&& (desti.getMezzoSpedizioneEnum().getId().intValue() == TipoSpedizioneEnum.ELETTRONICO.getId().intValue())) {
									tsd = TipoSpedizioneDocumentoEnum.ELETTRONICO;
									break;
								}
							}
						}
						md.setTipoSpedizione(tsd);
						md.setAnnoDocumento(doc.getAnnoDocumento());
						md.setGuuid(doc.getGuid());
						md.setIdFormatoDocumento(doc.getIdFormatoDocumento());
						md.setClasseDocumentale(doc.getDocumentClass());
						md.setResponsesRaw(doc.getResponsesRaw());
						masterUpdated = md;
						break;
					}
				}
			}

			if (MBean.DETTAGLIO_DOCUMENTO_BEAN.equalsIgnoreCase(chiamante) || MBean.FASCICOLO_MANAGER_BEAN.equalsIgnoreCase(chiamante)) {
				DettaglioDocumentoBean bean = FacesHelper.getManagedBean(MBean.DETTAGLIO_DOCUMENTO_BEAN);
				bean.getDocumentCmp().setDetail(doc);
				//Se siamo in ricerca
				if (NavigationTokenEnum.RICERCA_RAPIDA.equals(sessionBean.getActivePageInfo())) {
					FacesHelper.update("centralSectionForm:idTabRicercaRapida");
					bean.resetDetailView();
					FacesHelper.update("eastSectionForm:tabDetails");
				} else if (NavigationTokenEnum.RICERCA_DOCUMENTI.equals(sessionBean.getActivePageInfo())) { 
					FacesHelper.update("centralSectionForm:idTabRicercaAvanzataDocumenti");
					bean.resetDetailView();
					FacesHelper.update("eastSectionForm:tabDetails");
				} else {
					// Si aggiorna il master selezionato con le modifiche effettuate dal dettaglio esteso
					ldbean.setDocSelectedForResponse(masterUpdated);
					ldbean.updateResponseSingle(masterUpdated);
					
					// Se ci si trova in una coda e il documento:
					// a) è un Contributo Esterno in Entrata, oppure
					// b) ha/NON ha almeno un allegato per Copia Conforme rispetto alla sua versione precedente alla modifica
					// si aggiornano le response
					// per visualizzare la nuova response applicativa "Collega Contributo"
					// oppure per visualizzare/nascondere la response FileNet "Richiedi firma Copia Conforme"
					boolean allegatoCopiaConformeBefore = detailPreviousVersion != null && documentoRedSRV.hasAllegatiCopiaConforme(detailPreviousVersion.getAllegati());
					boolean allegatoCopiaConformeAfter = documentoRedSRV.hasAllegatiCopiaConforme(doc.getAllegati());
					
					if ((allegatoCopiaConformeBefore != allegatoCopiaConformeAfter) || 
						doc.getIdTipologiaDocumento().equals(idTipologiaDocumentoContributoEsternoEntrata)) {
						ldbean.updateResponseSingle(masterUpdated);
					}
					FacesHelper.update(IDDETTAGLIODOCUMENTO_RESOURCE_LOCATION);
					FacesHelper.update("centralSectionForm:documenti");
					FacesHelper.update("eastSectionForm:eastSectionPanelId");
				}
			} else if (ConstantsWeb.MBean.SOTTOSCRIZIONI_BEAN.equalsIgnoreCase(chiamante)) {
				SottoscrizioniBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SOTTOSCRIZIONI_BEAN);
				sb.getDocumentDetail().getDocumentCmp().setDetail(doc);

				//Siamo in tab procedimenti tracciati
				sb.getTracciatiTab().getDocumentDetailComponent().resetDetailView();
				FacesHelper.update("centralSectionForm:idTab_STSCZ:tabDetails-procedimenti");
				FacesHelper.executeJS("calculateIframeHeight()");
			}

			showInfoMessage("Il documento è stato aggiornato con successo.");
		}

		if (!this.documentManagerDTO.isInModifica() || MBean.MAIL_BEAN.equals(chiamante)) { // sono in creazione
			messaggioEsitoRegistra.clear();
			messaggioEsitoRegistra.put("Inserimento documento effettuato con successo.", "");
			if (doc.getNumeroProtocollo() != null && doc.getNumeroProtocollo().intValue() > 0) {
				messaggioEsitoRegistra.put("Protocollo:  ", doc.getNumeroProtocollo() + PATH_DELIMITER + doc.getAnnoProtocollo());
				if (utente.getStampaEtichetteResponse() && doc.getIdProtocollo() != null) {
					List<String> descrRegStampaEtichetteList = registroRepertorioSRV.getDescrForShowEtichetteByAoo(utente.getIdAoo());
					if (descrRegStampaEtichetteList != null && !descrRegStampaEtichetteList.isEmpty()) {
						for (String descrRegistro : descrRegStampaEtichetteList) {
							if (((doc.getDescrizioneRegistroRepertorio() != null && doc.getDescrizioneRegistroRepertorio().equalsIgnoreCase(descrRegistro)) 
									|| doc.getDescrizioneRegistroRepertorio() == null || "REGISTRO UFFICIALE".equals(doc.getDescrizioneRegistroRepertorio()))) {
								setStampaEtichetteButton(true); 
								break;
							}
						}
					} else if (doc.getDescrizioneRegistroRepertorio() == null || "REGISTRO UFFICIALE".equals(doc.getDescrizioneRegistroRepertorio())) {
						setStampaEtichetteButton(true); 
					}
				}
				
			}
			messaggioEsitoRegistra.put("Identificativo:  ", doc.getNumeroDocumento() + " del " + doc.getAnnoDocumento());

			messaggioEsitoRegistra.put("Inserito nel fascicolo:  ", doc.getIdFascicoloProcedimentale() + " - " + doc.getDescrizioneFascicoloProcedimentale());

			// Aggiorno la dialog contenente gli esiti
			if (!StringUtils.isNullOrEmpty(detail.getProtocollazioneMailGuid()) || MBean.MAIL_BEAN.equals(chiamante)) {
				messaggioEsitoRegistra.put("Assegnatari per competenza:  ", doc.getAssegnatarioCompetenza());
				// Devo aprire la dialog esito che si trova in codaMailDestra.xhtml per mostrare gli esiti
				FacesHelper.executeJS("PF('protocollaMail_WV').hide()");
				FacesHelper.update("eastSectionForm:idDlgEsitoRegistra_DERDM");
				FacesHelper.executeJS("PF('wdgEsitoRegistra_DERDM').show()"); 
				sessionBean.createSubMenuMail();
				FacesHelper.update("westSectionForm:idTabMain_LeftAside:idMenuLeftAside:idMenuMail");
				FacesHelper.update(EAST_SECTION_FORM_NAME); 
				FacesHelper.update("centralSectionForm"); 
			} else if (chiamantePredisponi || MBean.RICHIEDI_CONTRIBUTO_ESTERNO_BEAN.equals(chiamante)) { 
				EsitoCreaDocumentoDaResponseBean esitoPredisponiDaDocBean = FacesHelper.getManagedBean(MBean.ESITO_CREA_DOC_DA_RESPONSE_BEAN);
				esitoPredisponiDaDocBean.setMessaggiEsito(messaggioEsitoRegistra);
				FacesHelper.executeJS("PF('dlgManageDocument_RM').hide()");
				FacesHelper.update("idDettagliEstesiForm:idDlgEsitoCreaDocDaResponse");
				FacesHelper.update("westSectionForm:idTabMain_LeftAside:idAccPanOrgUtente_VM");
				FacesHelper.executeJS("PF('dlgEsitoCreaDocDaResponse').show()");	
			} else {
				if ((esito.isAssegnazioneCompetenzaAutomatica() || esito.isAssegnazioneCompetenzaIndiretta()) && 
						doc.getAssegnatarioCompetenza() != null && !doc.getAssegnatarioCompetenza().isEmpty()) {
					descrAssegnatarioPerCompetenza = doc.getAssegnatarioCompetenza();
				} 
				
				//significa che non vengo da protocollazione da mail quindi sono sulla pagina di creazione e non sulla dialog creazione/modifica 
				FacesHelper.update("idDettagliEstesiForm:idDlgEsitoRegistra_DER");
				//richiamo la visualizzazione della dialog
				FacesHelper.executeJS("PF('wdgEsitoRegistra_DER').show()");
			}
		}
	}


	/**REGISTRA*****************************************************************************************/	
	/**
	 * Aggiunge un nuovo destinatario alla lista dei destinatari in modifica: {@link #destinatariInModifica}.
	 */
	public void aggiungiDestinatario() {
		if (this.destinatariInModifica == null) {
			destinatariInModifica = new ArrayList<>();
		} else {
			destinatariInModifica = getDestinatariInModificaPuliti();
		}

		destinatariInModifica.add(new DestinatarioRedDTO());
		setCreaORichiediCreazioneFlag(false);

		updateMaxSizeValue();
	}

	/**
	 * Esegue tutte le operazioni necessarie a valle della chiusura della dialog.
	 */
	public void closeDialog() {
		unsetDetail();
	}

	/**
	 * Effettua un reset del dettaglio: {@link DocumentManagerBean#detail}.
	 */
	public void unsetDetail() {
		this.mapMetadati = null;
		this.indiceDiClassificazione = null;
		fadRagioneriaSelected = new FadRagioneriaDTO();
		ricercaFascicoloAnno = Calendar.getInstance().get(Calendar.YEAR);
		messaggioEsitoRegistra = new LinkedHashMap<>();
		detailPreviousVersion = null;
		detail = null;
		documentManagerDTO = null;
		destinatariInModifica = new ArrayList<>();
		riferimentiProt = new ArrayList<>();
		assegnatariPerContributo = new ArrayList<>();
		assegnatariPerConoscenza = new ArrayList<>();
		descrAssegnatarioPerCompetenza = null;
		descrAssegnatarioIterManuale = null;
		assegnatarioPrincipale = null;
		rootOrganigrammaComune = null;
		allegatiMailSelected = null;

		chiamante = null;
		chiamantePredisponi = false;
		
		isEstendiVisibilitaChk = false;
		faldonaCmp = null;

		mode = MODE_ELETTRONICO_DESC;
		
		tabAllegati = null;
		approvazioniTab = null;
		contributiTab = null;
		trasmissioneTab = null;

		listaMailTO = null;
		listaMailCC = null;

		previewComponent.unsetDetail();
		versioniAllegato.unsetDetail();
		templateDocUscitaComponent.reset();
		setMsgPredisponi("");
		this.contattiNelGruppo = new ArrayList<>();
		campoMailOnTheFly = "";
		
		setDataScarico(null);
	}

	/** FALDONATURA START*************************************************************************************************************************************/	
	/**
	 * Gestisce l'apertura e la visbilita della dialog di faldonatura.
	 */
	public void openDlgFaldonatura() {

		FascicoloDTO fProcedimentale = null;
		if (detail.getFascicoli() != null && !detail.getFascicoli().isEmpty()) {
			for (FascicoloDTO f : detail.getFascicoli()) {
				if (f.getIdFascicolo().equals(detail.getIdFascicoloProcedimentale())) {
					fProcedimentale = f;
					break;
				}
			}
		}

		// se non lo trova creo un fasciolo fittizio (L'unico caso in cui è null è quando siamo in creazione 
		// e non è stato selezionato nessun fascicolo dall'utente).
		if (fProcedimentale == null) {
			String inIdFascicolo = detail.getIdFascicoloProcedimentale();
			String inDescrizione = detail.getDescrizioneFascicoloProcedimentale();
			String inIndiceClassificazione = detail.getIndiceClassificazioneFascicoloProcedimentale();
			Date inDataCreazione = null;
			String inStato = null;
			String guid = null;
			String idFascicoloFEPA = null;
			fProcedimentale = new FascicoloDTO(inIdFascicolo, inDescrizione, inIndiceClassificazione, inDataCreazione, inStato, guid, idFascicoloFEPA); 
		}

		faldonaCmp = new DialogFaldonaComponent(utente);

		faldonaCmp.setFascicolo(fProcedimentale);
		Collection<FaldoneDTO> faldoni = this.detail.getFaldoni();
		if(faldoni!=null) {
			for(FaldoneDTO f : faldoni) {
				f.setFascicoloSelezionato(fProcedimentale);
			}
		}
		faldonaCmp.setFaldoni(faldoni);
		dlgFaldonaturaRendered = true;
	}

	/**
	 * Gestisce la funzionalità di faldonatura.
	 */
	public void faldona() {
		try {
			List<FaldoneDTO> faldoniList = faldonaCmp.registra();
			if (faldoniList.isEmpty()) {
				//l'esito è già stato mostrato nel bottone registra
				FacesHelper.update(IDTABSDMD_RESOURCE_NAME);
				return;
			}

			if (detail.getFaldoni() == null 
					|| (!documentManagerDTO.isInModifica() 
							&& StringUtils.isNullOrEmpty(detail.getIdFascicoloProcedimentale()))) {
				//lo creo o lo pulisco nel caso sono in creazione ed il fascicolo è ancora da creare 
				detail.setFaldoni(new ArrayList<>());
			}

			detail.getFaldoni().addAll(faldoniList);

			if (StringUtils.isNullOrEmpty(detail.getIdFascicoloProcedimentale())) {
				if (sd == null) {
					sd = new SalvaDocumentoRedParametriDTO();
				}

				List<String> nomiFaldoniList = new ArrayList<>();
				for(FaldoneDTO f : faldoniList) {
					nomiFaldoniList.add(f.getNomeFaldone());
				}
				sd.setNomeFaldoniSelezionati(nomiFaldoniList);
				showInfoMessage("Operazione eseguita correttamente.");
			} else {
				showInfoMessage("Fascicolo associato correttamente.");
			}

			if (ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN.equalsIgnoreCase(chiamante)) {
				DettaglioFascicoloRicercaBean dfb = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN);
				dfb.getFascicoloCmp().getDetail().setFaldoni(new ArrayList<>(detail.getFaldoni()));
			} else if (ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN.equalsIgnoreCase(chiamante)) {
				DettaglioDocumentoBean ddm = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN);
				if (ddm.getDocumentCmp() != null && ddm.getDocumentCmp().getDetail() != null) {
					ddm.getDocumentCmp().getDetail().setFaldoni(detail.getFaldoni());
				}
			}
			FacesHelper.update(IDTABSDMD_RESOURCE_NAME);

			closeDlgFaldonatura();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}

	}

	/**
	 * Gestisce la chiusura della dialog di faldonatura eseguendo tutte le azioni necessarie a valle della chiusura.
	 */
	public void closeDlgFaldonatura() {
		try {
			FacesHelper.destroyBeanViewScope(MBean.FALDONA_BEAN);
			faldonaCmp = null;
			dlgFaldonaturaRendered = false;
			FacesHelper.executeJS("PF('wdgFaldonaFascicolo_DMDTF').hide();");
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera("Errore durante l'operazione.");
		}
	}

	/**
	 * Gestisce la rimozione di un faldone identificato dall'index.
	 * @param index
	 */
	public void eliminaFaldone(Object index) {
		try {
			if (StringUtils.isNullOrEmpty(detail.getDocumentTitle())) {
				//siamo in creazione ingresso o uscita quindi semplicemente pulisco la lista di faldoni
				detail.getFaldoni().clear();
				return;
			}

			//in tutti gli altri casi devo dissociare il fascicolo dal faldone
			Integer i = (Integer) index;
			FaldoneDTO fSelected =  IterableUtils.get(detail.getFaldoni(), i.intValue());

			//Sse la lista di faldoni è == 1 devo controllare se i documenti contenuti nei fascicoli abbiano il vincolo di faldonatura
			//se ce lo hanno non posso dissociare il faldone perché devono essere contenuti in almeno un faldone
			EsitoFaldoneEnum esito = null;
			Long idAOO = Long.valueOf(detail.getIdAOO());
			if (detail.getFaldoni().size() > 1 
					|| faldoneSRV.isFaldoneDisassociabile(fSelected.getNomeFaldone(), detail.getIdFascicoloProcedimentale(), idAOO, utente)) {
				esito = faldoneSRV.disassociaFascicoloDaFaldone(fSelected.getNomeFaldone(), detail.getIdFascicoloProcedimentale(), idAOO, utente);
			} else  {
				esito = EsitoFaldoneEnum.FALDONE_NON_DISASSOCIABILE;
			}

			switch (esito) {
				case ERRORE_GENERICO:
					
					showErrorMaschera(EsitoFaldoneEnum.ERRORE_GENERICO.getText());
					break;
				case FALDONE_DISASSOCIATO_DA_FASCICOLO_CON_SUCCESSO:
				
					finalizzaDisassociazioneFascicolo(fSelected);
					showInfoMessage(EsitoFaldoneEnum.FALDONE_DISASSOCIATO_DA_FASCICOLO_CON_SUCCESSO.getText());
				break;
			case FALDONE_NON_DISASSOCIABILE:
				showWarningMaschera(EsitoFaldoneEnum.FALDONE_NON_DISASSOCIABILE.getText());
				break;

			default:
				break;
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera("Errore durante l'operazione di dissociazione del faldone.");
		}
	}

	/**
	 * Esegue le azioni a valle della disassociazione del fascicolo avvenuta
	 * correttamente.
	 * 
	 * @param fSelected
	 *            Fascicolo selezionato da disassociare.
	 */
	private void finalizzaDisassociazioneFascicolo(FaldoneDTO fSelected) {
		detail.getFaldoni().remove(fSelected);
		
		//aggiorno chi mi ha chiamato
		if (ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN.equalsIgnoreCase(chiamante)) {
			DettaglioFascicoloRicercaBean dfb = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN);
			dfb.getFascicoloCmp().getDetail().setFaldoni(new ArrayList<>(detail.getFaldoni()));
		} else if (ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN.equalsIgnoreCase(chiamante)) {
			DettaglioDocumentoBean ddm = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN);
			if (ddm.getDocumentCmp() != null && ddm.getDocumentCmp().getDetail() != null) {
				ddm.getDocumentCmp().getDetail().setFaldoni(detail.getFaldoni());
			}
		}
	}

	/**
	 * caricamento notifiche generale (PEC + Interop.)
	 */
	public void loadNotificheEmail()  {
		codaMailSRV = ApplicationContextProvider.getApplicationContext().getBean(ICodaMailFacadeSRV.class);
		
		PropertiesProvider pp = PropertiesProvider.getIstance();
		String rootCasellePostali = pp.getParameterByKey(PropertiesNameEnum.FOLDER_ELETTRONICI_FN_METAKEY).split("\\|")[1];
		String cartellaNotifiche = pp.getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_NOTIFICHE_FN_METAKEY);
		String path = PATH_DELIMITER + rootCasellePostali + PATH_DELIMITER + this.detail.getMailSpedizione().getMittente() + PATH_DELIMITER + cartellaNotifiche;
  
		if(notifiche == null || notifiche.size() == 0) {
			notificheTemp = new HashMap<>();
			notifiche = codaMailSRV.getNotifichePEC(utente, detail.getDocumentTitle(), detail.getMailSpedizione().getMittente(), path, true);
			notificheTemp.putAll(notifiche);
			listaNotifichePEC = new ArrayList<>(notifiche.values());
			iconaSpedizione = codaMailSRV.getNotificheForIconaStatoReinvia(detail.getDocumentTitle(), utente.getIdAoo());
			iconaSpedizioneTemp = new HashMap<>();
			iconaSpedizioneTemp.putAll(iconaSpedizione);
		} else {
			notifiche = new HashMap<>();
			notifiche.putAll(notificheTemp);
			listaNotifichePEC = new ArrayList<>(notifiche.values());
			iconaSpedizione = new HashMap<>();
			iconaSpedizione.putAll(iconaSpedizioneTemp);	
		}

		for (EmailDTO value : listaNotifichePEC) {
			value.setStatoNotifica(TipoNotificaPecEnum.getStatoNotificaEmailByPrefisso(value.getOggetto()));
		}

		if (!PostaEnum.POSTA_INTERNA.equals(utente.getTipoPosta())) {
			listaNotificheInteroperabilita = codaMailSRV.getNotificheInteroperabilita(detail.getIdProtocollo(), detail.getNumeroProtocollo(),
					detail.getAnnoProtocollo(), utente);
		}
		
		//pulisco la lista notifiche totale
		listaNotifiche = new ArrayList<>();
		if(!CollectionUtils.isEmpty(listaNotifichePEC)){
			listaNotifiche.addAll(listaNotifichePEC);
		}
		if(!CollectionUtils.isEmpty(listaNotificheInteroperabilita)){
			for(NotificaInteroperabilitaEmailDTO interop : listaNotificheInteroperabilita) {
				//ribalto l'informazione per la corretta visualizzazione in tabella
				interop.setStatoNotifica(interop.getTipoNotificaInterop());
				listaNotifiche.add((EmailDTO) interop);
			}
		}
		//in caso fosse stata già usata, pulisco
		setListaNotificheFiltrate(new ArrayList<>());
		//senza filtraggio, inizialmente corrisponde alla lista totale
		listaNotificheFiltrate.addAll(listaNotifiche);
	}

	/**
	 * Restituisce lo StreamdContent fornito da {@link #downloadDatiDiTrasmissioneZip(listaNotificheFiltrate)}.
	 * @return StreamedContent
	 */
	public StreamedContent downloadDatiDiTrasmissioneZip() {
		return downloadDatiDiTrasmissioneZip(listaNotificheFiltrate);
	}

	/**
	 * Restituisce lo StreamedContent per il download dati di trasmissione compressi in un file .zip.
	 * @param notificheEmail
	 * @return StreamedContent per download
	 */
	public StreamedContent downloadDatiDiTrasmissioneZip(List<EmailDTO> notificheEmail) {
		Map<String, byte[]> files = new HashMap<>();
		ByteArrayInputStream bis = null;
		StreamedContent file = null;

		try {
			int i = 1;
			
			if (!CollectionUtils.isEmpty(notificheEmail)) {
				for (EmailDTO notifica : notificheEmail) {
					files.put("notifica" + (i++) + ".html", 
							(!StringUtils.isNullOrEmpty(notifica.getTesto()) ? notifica.getTesto() : Constants.EMPTY_STRING).getBytes());
				}
			}

			bis = new ByteArrayInputStream(FileUtils.writeZipFile(files));
			file = new DefaultStreamedContent(bis, "application/zip", "archivioNotifiche.zip",  StandardCharsets.UTF_8.name());

		} catch (Exception e) {
			LOGGER.error("Errore durante il downaload delle notifiche", e);
			showErrorMaschera("Errore durante il download delle notifiche");
		} finally {
			try {
				if (bis != null) {
					bis.close();
				}
			} catch (IOException e) {
				LOGGER.error(e);
			}
		}

		return file;
	}
	
	private List<EmailDTO> createMittenteDestinatariMailRow(UtenteDTO utente, String[] destinatari,boolean isMittente) {
		 return createMittenteDestinatariMailRow(utente,destinatari,isMittente, false);
	}

	private List<EmailDTO> createMittenteDestinatariMailRow(UtenteDTO utente, String[] destinatari,boolean isMittente,boolean forceReload) {
		String pattern = "yyyy-MM-dd HH:mm:ss.SSS";
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		LOGGER.info("START##AnalisiSped - CREATEMITTENTEDESTINATARIMAILROW : " + sdf.format(new Date()) + " -DT : " +detail.getDocumentTitle());
		List<EmailDTO> notificheList = new ArrayList<>();
		
		PropertiesProvider pp = PropertiesProvider.getIstance();
		String rootCasellePostali = pp.getParameterByKey(PropertiesNameEnum.FOLDER_ELETTRONICI_FN_METAKEY).split("\\|")[1];
		String cartellaNotifiche = pp.getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_NOTIFICHE_FN_METAKEY);
		String path = PATH_DELIMITER + rootCasellePostali + PATH_DELIMITER + this.detail.getMailSpedizione().getMittente() + PATH_DELIMITER + cartellaNotifiche;

		CasellaPostaDTO casellaMittente = null;
		if (CollectionUtils.isNotEmpty(sessionBean.getCp())) {			
			for (CasellaPostaDTO cc: sessionBean.getCp()) {
				if (cc.getIndirizzoEmail().equals(this.detail.getMailSpedizione().getMittente())) {
					casellaMittente = cc;
					break;
				}
			}
		}

		if (casellaMittente == null) {
			LOGGER.info("START - casellePostaliSRV.getCasellaPostale : " + sdf.format(new Date()));
			casellaMittente = casellePostaliSRV.getCasellaPostale(utente, this.detail.getMailSpedizione().getMittente());
			LOGGER.info("END - casellePostaliSRV.getCasellaPostale : " + sdf.format(new Date()));
		}

		boolean isPEO = false;
		if (casellaMittente != null && casellaMittente.getTipologia() == 1) {
			isPEO = true;
		}
		codaMailSRV = ApplicationContextProvider.getApplicationContext().getBean(ICodaMailFacadeSRV.class);
		LOGGER.info("START - codaMailSRV.getNotifichePEC : " + sdf.format(new Date()));
	 
		if(notifiche == null || notifiche.size() == 0 || forceReload) {
			notificheTemp = new HashMap<>();
			notifiche = codaMailSRV.getNotifichePEC(utente, detail.getDocumentTitle(), detail.getMailSpedizione().getMittente(), path, true);
			notificheTemp.putAll(notifiche);
			listaNotifichePEC = new ArrayList<>(notifiche.values());
			iconaSpedizione = codaMailSRV.getNotificheForIconaStatoReinvia(detail.getDocumentTitle(), utente.getIdAoo());
			iconaSpedizioneTemp = new HashMap<>();
			iconaSpedizioneTemp.putAll(iconaSpedizione);
			
		} else {
			notifiche = new HashMap<>();
			notifiche.putAll(notificheTemp);
			listaNotifichePEC = new ArrayList<>(notifiche.values());
			iconaSpedizione = new HashMap<>();
			iconaSpedizione.putAll(iconaSpedizioneTemp);
		}
		
		List<String> emailReinviate  = new ArrayList<>();
		for(List<CodaEmail> emails: iconaSpedizione.values()) {
			for(CodaEmail email : emails) {
				//Posso inserire una mail piu di una volta
				if(email.getSpedizione()>0) {
					emailReinviate.add(email.getEmailDestinatario());
				}
			}
			
		}
		
		LOGGER.info("END - codaMailSRV.getNotifichePEC : " + sdf.format(new Date()));
		
		
		Set<String> keySet = notifiche.keySet();
		for (String destTemp : destinatari) {
			
			destTemp = destTemp.trim().toLowerCase();

			EmailDTO singleMail = new EmailDTO();
			singleMail.setMittente(destTemp);
			singleMail.setStatoNotifica(Constants.EMPTY_STRING);
			//default
			singleMail.setIconaStato(IconaStatoEnum.SPEDIZIONE_ELETTRONICA_ATTESA_RICONCILIAZIONE);
			EmailDTO mail = null;
			if(isMittente) {
				 mail = notifiche.get(destTemp.toLowerCase().trim() + "_MIT");
			}else {
				String keyToRemove = null;
				for(String key: keySet) {
					if(key.startsWith(destTemp.toLowerCase().trim()) && !key.contains("_MIT")){
						mail = notifiche.get(key);
						keyToRemove = key;
						break;
					}
				}
				
				if(keyToRemove!=null) {
					keySet.remove(keyToRemove);
				}
			}
			
			if (isPEO) {
				singleMail.setImageMailEnum(ImageMailEnum.IMAGE_MAIL_NON_SPECIFICATA);
				singleMail.setStatoNotifica(NOTIFICA_NON_PREVISTA_MSG);
				
				List<CodaEmail> notificheDest = new ArrayList<>();
				if(iconaSpedizione.get(destTemp)!=null && !iconaSpedizione.get(destTemp).isEmpty()) {
					notificheDest.addAll(iconaSpedizione.get(destTemp));
				}
				for (CodaEmail n : notificheDest) {
					
					if (n == null) {
						throw new RedException("CodaEmail non recuperata correttamente.");
					}
					
					boolean iconaSpedizioneGestita = false;
					// Assegno l'icona spedizione in base allo stato e rimuovo l'item dalla lista.
					if (n != null && StatoCodaEmailEnum.CHIUSURA.getStatus().equals(n.getStatoRicevuta()) || StatoCodaEmailEnum.SPEDITO.getStatus().equals(n.getStatoRicevuta())) {
						// @ Verde - Spedizione spedita o chiusa.
						singleMail.setIconaStato(IconaStatoEnum.SPEDIZIONE_ELETTRONICA_CHIUSA);
						iconaSpedizione.get(destTemp).remove(n);
						iconaSpedizioneGestita = true;
					} else if (n != null && StatoCodaEmailEnum.ERRORE.getStatus().equals(n.getStatoRicevuta())) {
						// @ Rossa - Spedizione in stato di errore.
						singleMail.setIconaStato(IconaStatoEnum.SPEDIZIONE_ELETTRONICA_ERRORE);
						iconaSpedizione.get(destTemp).remove(n);
						iconaSpedizioneGestita = true;
					} else if ((n != null && StatoCodaEmailEnum.ATTESA_SPEDIZIONE_NPS.getStatus().equals(n.getStatoRicevuta()) || StatoCodaEmailEnum.DA_ELABORARE.getStatus().equals(n.getStatoRicevuta()))
							|| (n != null)) {
						// @ Gialla - Spedizione in attesa di essere spedita.
						singleMail.setIconaStato(IconaStatoEnum.SPEDIZIONE_ELETTRONICA_ATTESA_RICONCILIAZIONE);
						iconaSpedizione.get(destTemp).remove(n);
						iconaSpedizioneGestita = true;
					}
					
					if (iconaSpedizioneGestita)  {
						break;
					}
				}
				
			} else if (notifiche.size() > 0) {
				if (mail != null) {
					if (mail.getTesto() != null) {
						singleMail.setTesto(mail.getTesto().replace("\\r", "").replace("\\\\r", "").replace("<br>", "\n").replace("\\\\n", "\n"));
					} else {
						singleMail.setTesto("Nessun content");
					}
					String oggetto = mail.getOggetto();
					
					boolean consegna = false;
					if (TipoNotificaPecEnum.checkPrefissoNotificaPec(oggetto, TipoNotificaPecEnum.ACCETTAZIONE)) {
						singleMail.setImageMailEnum(ImageMailEnum.IMAGE_MAIL_ACCETTATA);
						singleMail.setStatoNotifica("Mail PEC accettata");
					} else if (TipoNotificaPecEnum.checkPrefissoNotificaPec(oggetto, TipoNotificaPecEnum.NON_ACCETTAZIONE_VIRUS)) {
						singleMail.setImageMailEnum(ImageMailEnum.IMAGE_MAIL_NON_ACCETTATA);
						singleMail.setStatoNotifica("Mail PEC non accettata causa presenza virus");
					} else if (TipoNotificaPecEnum.checkPrefissoNotificaPec(oggetto, TipoNotificaPecEnum.NON_ACCETTAZIONE)) {
						singleMail.setImageMailEnum(ImageMailEnum.IMAGE_MAIL_NON_ACCETTATA);
						singleMail.setStatoNotifica("Mail PEC non accettata");
						abilitaReinvioMail(singleMail, destTemp, emailReinviate);
					} else if (TipoNotificaPecEnum.checkPrefissoNotificaPec(oggetto, TipoNotificaPecEnum.AVVENUTA_CONSEGNA)) {
						consegna = true;
						singleMail.setImageMailEnum(ImageMailEnum.IMAGE_MAIL_CONSEGNATA);
						singleMail.setStatoNotifica("Mail PEC consegnata");
					} else if (TipoNotificaPecEnum.checkPrefissoNotificaPec(oggetto, TipoNotificaPecEnum.PRESA_IN_CARICO)) {
						consegna = true;
						singleMail.setImageMailEnum(ImageMailEnum.IMAGE_MAIL_NON_CONSEGNATA);
						singleMail.setStatoNotifica("Mail presa in carico dal gestore ricevente");
					} else if (TipoNotificaPecEnum.checkPrefissoNotificaPec(oggetto, TipoNotificaPecEnum.ERRORE_CONSEGNA_VIRUS)) {
						consegna = true;
						singleMail.setImageMailEnum(ImageMailEnum.IMAGE_MAIL_NON_CONSEGNATA);
						singleMail.setStatoNotifica("Mail PEC non consegnata causa presenza virus");
					} else if (TipoNotificaPecEnum.checkPrefissoNotificaPec(oggetto, TipoNotificaPecEnum.PREAVVISO_ERRORE_CONSEGNA)) {
						consegna = true;
						singleMail.setImageMailEnum(ImageMailEnum.IMAGE_MAIL_NON_CONSEGNATA);
						singleMail.setStatoNotifica("Mail PEC non consegnata causa sup. tempo massimo");		
					} else if (TipoNotificaPecEnum.checkPrefissoNotificaPec(oggetto, TipoNotificaPecEnum.ERRORE_CONSEGNA)) {
						consegna = true;
						singleMail.setImageMailEnum(ImageMailEnum.IMAGE_MAIL_NON_CONSEGNATA);
						singleMail.setStatoNotifica("Mail PEC non consegnata");
						abilitaReinvioMail(singleMail, destTemp, emailReinviate);
					} else {
						singleMail.setImageMailEnum(ImageMailEnum.IMAGE_MAIL_NON_SPECIFICATA);
						singleMail.setStatoNotifica(NOTIFICA_NON_PREVISTA_MSG);
					}
					// 991 begin
					String[] appDest = new String[1]; 
					appDest[0] = destTemp;
					if (!codaMailSRV.checkEmailInviata(Constants.EMPTY_STRING + detail.getDocumentTitle(), appDest)) {
						abilitaReinvioMail(singleMail, destTemp, emailReinviate);
					}
					// 991 end
					
					List<CodaEmail> notificheDest = new ArrayList<>();
					if (iconaSpedizione.get(destTemp) != null && !iconaSpedizione.get(destTemp).isEmpty()) {
						notificheDest.addAll(iconaSpedizione.get(destTemp));
					}
					
					for (CodaEmail n : notificheDest) {
						boolean iconaSpedizioneGestita = false;

						if(n.getTipoDestinatario().equals(TipologiaDestinatariEnum.PEO.getId())) {
							// Assegno l'icona spedizione in baste allo stato e rimuovo l'item dalla lista.
							if (StatoCodaEmailEnum.ATTESA_ACCETTAZIONE.getStatus().equals(n.getStatoRicevuta())) {
								// @ Gialla - Spedizione in attesa di essere spedita.
								singleMail.setIconaStato(IconaStatoEnum.SPEDIZIONE_ELETTRONICA_ATTESA_RICONCILIAZIONE);
								iconaSpedizione.get(destTemp).remove(n);
								iconaSpedizioneGestita = true;
							} else if (StatoCodaEmailEnum.CHIUSURA.getStatus().equals(n.getStatoRicevuta()) || StatoCodaEmailEnum.SPEDITO.getStatus().equals(n.getStatoRicevuta())) {
								// @ Verde - Spedizione spedita o chiusa.
								singleMail.setIconaStato(IconaStatoEnum.SPEDIZIONE_ELETTRONICA_CHIUSA);
								iconaSpedizione.get(destTemp).remove(n);
								iconaSpedizioneGestita = true;
							} else if (n != null && StatoCodaEmailEnum.ERRORE.getStatus().equals(n.getStatoRicevuta())) {
								// @ Rossa - Spedizione in stato di errore.
								singleMail.setIconaStato(IconaStatoEnum.SPEDIZIONE_ELETTRONICA_ERRORE);
								iconaSpedizione.get(destTemp).remove(n);
								iconaSpedizioneGestita = true;
							}
						}else {
							// Assegno l'icona spedizione in baste allo stato e rimuovo l'item dalla lista.
							if (!consegna && (StatoCodaEmailEnum.ATTESA_ACCETTAZIONE.getStatus().equals(n.getStatoRicevuta()) || StatoCodaEmailEnum.ATTESA_AVVENUTA_CONSEGNA.getStatus().equals(n.getStatoRicevuta()))) {
								// @ Gialla - Spedizione in attesa di essere spedita.
								singleMail.setIconaStato(IconaStatoEnum.SPEDIZIONE_ELETTRONICA_ATTESA_RICONCILIAZIONE);
								iconaSpedizione.get(destTemp).remove(n);
								iconaSpedizioneGestita = true;
							} else if (consegna && (StatoCodaEmailEnum.CHIUSURA.getStatus().equals(n.getStatoRicevuta()) || StatoCodaEmailEnum.SPEDITO.getStatus().equals(n.getStatoRicevuta()))) {
								// @ Verde - Spedizione spedita o chiusa.
								singleMail.setIconaStato(IconaStatoEnum.SPEDIZIONE_ELETTRONICA_CHIUSA);
								iconaSpedizione.get(destTemp).remove(n);
								iconaSpedizioneGestita = true;
							} else if (n != null && StatoCodaEmailEnum.ERRORE.getStatus().equals(n.getStatoRicevuta())) {
								// @ Rossa - Spedizione in stato di errore.
								singleMail.setIconaStato(IconaStatoEnum.SPEDIZIONE_ELETTRONICA_ERRORE);
								iconaSpedizione.get(destTemp).remove(n);
								iconaSpedizioneGestita = true;
							}
						}

						if (iconaSpedizioneGestita) {
							break;
						}
					}
				} else {
					singleMail.setImageMailEnum(ImageMailEnum.IMAGE_MAIL_NON_SPECIFICATA);
					singleMail.setStatoNotifica(NOTIFICA_NON_PREVISTA_MSG);
					abilitaReinvioMail(singleMail, destTemp, emailReinviate);
					List<CodaEmail> notificheDest = new ArrayList<>();
					notificheDest.addAll(iconaSpedizione.get(destTemp));
					for (CodaEmail n : notificheDest) {
						
						boolean iconaSpedizioneGestita = false;
						if (n != null && StatoCodaEmailEnum.CHIUSURA.getStatus().equals(n.getStatoRicevuta()) || StatoCodaEmailEnum.SPEDITO.getStatus().equals(n.getStatoRicevuta())) {
							// @ Verde - Spedizione spedita o chiusa.
							singleMail.setIconaStato(IconaStatoEnum.SPEDIZIONE_ELETTRONICA_CHIUSA);
							iconaSpedizione.get(destTemp).remove(n);
							iconaSpedizioneGestita = true;
						} else if (n != null && StatoCodaEmailEnum.ERRORE.getStatus().equals(n.getStatoRicevuta())) {
							// @ Rossa - Spedizione in stato di errore.
							singleMail.setIconaStato(IconaStatoEnum.SPEDIZIONE_ELETTRONICA_ERRORE);
							iconaSpedizione.get(destTemp).remove(n);
							iconaSpedizioneGestita = true;
						} else if ((n != null && StatoCodaEmailEnum.ATTESA_SPEDIZIONE_NPS.getStatus().equals(n.getStatoRicevuta()) || StatoCodaEmailEnum.DA_ELABORARE.getStatus().equals(n.getStatoRicevuta()))
								|| (n != null)) {
							// @ Gialla - Spedizione in attesa di essere spedita.
							singleMail.setIconaStato(IconaStatoEnum.SPEDIZIONE_ELETTRONICA_ATTESA_RICONCILIAZIONE);
							iconaSpedizione.get(destTemp).remove(n);
							iconaSpedizioneGestita = true;
						}
						
						if (iconaSpedizioneGestita) {
							break;
						}
					}
				}

				
				notifiche.remove(destTemp.toLowerCase().trim() + (isMittente ? "_MIT" : ""));
			} else {
				singleMail.setImageMailEnum(ImageMailEnum.IMAGE_MAIL_IN_ATTESA);
				singleMail.setStatoNotifica("Mail in attesa di accettazione");
				
				List<CodaEmail> notificheDest = new ArrayList<>();
				if(iconaSpedizione.get(destTemp)!=null && !iconaSpedizione.get(destTemp).isEmpty()) {
					notificheDest.addAll(iconaSpedizione.get(destTemp));
				}
				for (CodaEmail n : notificheDest) {
					
					boolean iconaSpedizioneGestita = false;
					// Assegno l'icona spedizione in baste allo stato e rimuovo l'item dalla lista.
					if (n != null && StatoCodaEmailEnum.CHIUSURA.getStatus().equals(n.getStatoRicevuta()) || StatoCodaEmailEnum.SPEDITO.getStatus().equals(n.getStatoRicevuta())) {
						// @ Verde - Spedizione spedita o chiusa.
						singleMail.setStatoNotifica("Spedizione elettronica chiusa");
						singleMail.setIconaStato(IconaStatoEnum.SPEDIZIONE_ELETTRONICA_CHIUSA);
						iconaSpedizione.get(destTemp).remove(n);
						iconaSpedizioneGestita = true;
					} else if (n != null && StatoCodaEmailEnum.ERRORE.getStatus().equals(n.getStatoRicevuta())) {
						// @ Rossa - Spedizione in stato di errore.
						singleMail.setStatoNotifica("Mail PEC in errore");
						singleMail.setIconaStato(IconaStatoEnum.SPEDIZIONE_ELETTRONICA_ERRORE);
						iconaSpedizione.get(destTemp).remove(n);
						iconaSpedizioneGestita = true;
					} else if (n != null && StatoCodaEmailEnum.ATTESA_SPEDIZIONE_NPS.getStatus().equals(n.getStatoRicevuta()) || StatoCodaEmailEnum.DA_ELABORARE.getStatus().equals(n.getStatoRicevuta())) {
						// @ Gialla - Spedizione in attesa di essere spedita.
						singleMail.setStatoNotifica("Mail PEC in attesa di spedizione");
						singleMail.setIconaStato(IconaStatoEnum.SPEDIZIONE_ELETTRONICA_ATTESA_RICONCILIAZIONE);
						iconaSpedizione.get(destTemp).remove(n);
						iconaSpedizioneGestita = true;
					}else if (n != null) {
						// @ Gialla - Spedita PEC
						singleMail.setIconaStato(IconaStatoEnum.SPEDIZIONE_ELETTRONICA_ATTESA_RICONCILIAZIONE);
						iconaSpedizione.get(destTemp).remove(n);
						iconaSpedizioneGestita = true;
					}
					
					if (iconaSpedizioneGestita) {
						break;
					}
				}
				
			}
			
			notificheList.add(singleMail);
		}
		 
		LOGGER.info("END##AnalisiSped - CREATEMITTENTEDESTINATARIMAILROW : " + sdf.format(new Date()) + " - DT:" + detail.getDocumentTitle());
		return notificheList;
	}


	private static void abilitaReinvioMail(EmailDTO singleMail, String destTemp,List<String> emailReinviate) {
		if(!emailReinviate.contains(destTemp)) {
			singleMail.setReinvioMail(true);
			singleMail.setMittenteDestinatariTemp(destTemp);
		}else {
			singleMail.setReinvioMail(false);
			emailReinviate.remove(emailReinviate.lastIndexOf(destTemp));
		}
	}

	/**
	 * Effettua azioni di setup per la dialog di Reinvia Mail.
	 * @param index
	 * @param toOrcc
	 */
	public void preparaReinviaMailDialog(Object index, String toOrcc) {
		try {
			Integer i =  (Integer) index;
			azzeraCampiReinviaMail();
			if ("TO".equalsIgnoreCase(toOrcc)) {
				this.selectedMailForReinviaTO = this.listaMailTO.get(i);
			} else if ("CC".equalsIgnoreCase(toOrcc)) {
				this.selectedMailForReinviaCC = this.listaMailCC.get(i);
			}
		} catch (Exception e) {
			showErrorMaschera(e);
		}
	}

	/**
	 * Effettua un reset sui campi del Reinvia Mail.
	 */
	public void azzeraCampiReinviaMail() {
		indirizzoMailPerReinvia = "";
		selectedMailForReinviaTO = null;
		selectedMailForReinviaCC = null;
	}

	/**
	 * Esegue la logica di business per il soddisfacimento della funzionalità Reinvia Mail.
	 */
	public void reinviaMail() {
		try {
			EmailDTO mailReinvio = null; 
			boolean clickMailTO = false;
			if (selectedMailForReinviaTO != null) {
				mailReinvio = selectedMailForReinviaTO;
				clickMailTO = true;
			} else if (selectedMailForReinviaCC != null) {
				mailReinvio = selectedMailForReinviaCC;
			}
			//fa la validazione all'interno
			if (mailReinvio != null && StringUtils.isNullOrEmpty(mailReinvio.getMittenteDestinatariTemp())) {
				codaMailSRV.reinviaMail(utente, detail.getDocumentTitle(), detail.getMailSpedizione().getGuid(), mailReinvio.getMittente(), indirizzoMailPerReinvia, null, 
						null, null, null,clickMailTO);
				showInfoMessage("Richiesta di reinvio acquisita. Attendere lo scaricamento delle nuove ricevute per l'aggiornamento del tab spedizione");
			} else if (mailReinvio != null) {
				codaMailSRV.reinviaMail(utente, detail.getDocumentTitle(), detail.getMailSpedizione().getGuid(), mailReinvio.getMittente(), indirizzoMailPerReinvia, 
						mailReinvio.getMittenteDestinatariTemp(), detail.getNumeroProtocollo().toString(), detail.getAnnoProtocollo().toString() ,detail.getIdProtocollo(),
						clickMailTO);
				showInfoMessage("Richiesta di reinvio acquisita. Attendere lo scaricamento delle nuove ricevute per l'aggiornamento del tab spedizione");
			}

			azzeraCampiReinviaMail();

			// Viene ricaricata la mail spedizioni ed eventuali destinatari TO e CC dopo aver effettuato un reinvio
			// necessario per aggiornare la tabella dei destinatari
			this.detail.setMailSpedizione(documentoRedSRV.getMailByDocumentTitleAndIdAOObyAdmin(this.detail.getDocumentTitle(), utente));
 			loadMailTOandCC(true); 
		} catch (Exception e) {
			showErrorMaschera(e);
		}
	}
	
	private void loadMailTOandCC(boolean forceReload) {
		if (this.detail.getMailSpedizione().getDestinatari() != null && this.detail.getMailSpedizione().getDestinatari().split(";").length > 0) {
			setListaMailTO(createMittenteDestinatariMailRow(utente, this.detail.getMailSpedizione().getDestinatari().split(";"),false, forceReload));
		} else {
			setListaMailTO(new ArrayList<>());
		}
		if (this.detail.getMailSpedizione().getDestinatariCC() != null && this.detail.getMailSpedizione().getDestinatariCC().split(";").length > 0) {
			setListaMailCC(createMittenteDestinatariMailRow(utente, this.detail.getMailSpedizione().getDestinatariCC().split(";"), false, forceReload));
		} else {
			setListaMailCC(new ArrayList<>());
		}
	}

	/**
	 * Resetta {@link #indirizzoMailPerReinvia}.
	 */
	public void pulisciStringaMailReinvia() {
		indirizzoMailPerReinvia = "";
	}


	/*******************TAB STORICO****************/
	private void onChangeDetailStorico(DetailDocumentRedDTO document) {
		if (documentManagerDTO.isInModifica()) {
			if (storico == null) {
				storico = new DettaglioDocumentoStoricoComponent(getClass().getSimpleName());
			}
			storico.setDetail(document);
			storico.setValid(document.getDateCreated());
			storico.buildStoricoGui();
		}
	}

	private void showWarningMaschera(final String msg) {
		showWarnMessage(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, msg);
	}

	private void showErrorMaschera(final Throwable e) {
		showError(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, e);
	}

	private void showErrorMaschera(final String msg) {
		showError(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, msg);
	}


	/** SHORTCUT CREAZIONE CONTATTI START********************************************************************/
	/**
	 * @param query
	 * @return
	 */
	public final  List<RegioneDTO> loadRegioni(String query) {
		List<RegioneDTO> list = null;
		try {
			list = RegioniProvinceComuniComponent.loadRegioni(query);
		} catch (Exception e) {
			LOGGER.error(e);
			showError(e);
		}
		return list;
	}

	/**
	 * @param query
	 * @return
	 */
	public final List<ProvinciaDTO> loadProvince(String query) {
		List<ProvinciaDTO> province  = null; 
		try {
			province = RegioniProvinceComuniComponent.loadProvinceAdd(query);
		} catch (Exception e) {
			LOGGER.error(e);
			showError(e);
		}
		return province;
	}

	/**
	 * @param query
	 * @return
	 */
	public final List<ComuneDTO> loadComuni(String query) {
		List<ComuneDTO> list = null;
		try {
			list = RegioniProvinceComuniComponent.loadComuniAdd(query, documentManagerDTO.getInserisciContattoItem().getProvinciaObj());
		} catch (Exception e) {
			LOGGER.error(e);
			showError(e);
		}
		return list;
	}

	/**
	 * 
	 */
	public final void onSelectRegione(AjaxBehaviorEvent event) {
		try {
			if (event instanceof SelectEvent) {
				documentManagerDTO.getInserisciContattoItem().setRegioneObj((RegioneDTO)((SelectEvent)event).getObject());
			}else {
				documentManagerDTO.getInserisciContattoItem().setRegioneObj(null);
			}

			RegioniProvinceComuniComponent.onSelectRegione(documentManagerDTO.getInserisciContattoItem());
		} catch (Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * 
	 */
	public final void onSelectProvincia(AjaxBehaviorEvent event) {
		try {
			if (event instanceof SelectEvent) {
				documentManagerDTO.getInserisciContattoItem().setProvinciaObj((ProvinciaDTO)((SelectEvent)event).getObject());
			}else {
				documentManagerDTO.getInserisciContattoItem().setProvinciaObj(null);
			}

			RegioniProvinceComuniComponent.onSelectProvincia(documentManagerDTO.getInserisciContattoItem());
		} catch (Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * @param event
	 */
	public final void onSelectComune(AjaxBehaviorEvent event) {
		try {
			if (event instanceof SelectEvent) {
				documentManagerDTO.getInserisciContattoItem().setComuneObj((ComuneDTO)((SelectEvent)event).getObject());
			}else {
				documentManagerDTO.getInserisciContattoItem().setComuneObj(null);
			}
		} catch (Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Esegue una validazione del contatto che se ha esito positivo viene aggiunto alla lista del {@link #documentManagerDTO}.
	 */
	public void validaEInserisciContatto() {
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			if (!RubricaComponent.valida(documentManagerDTO.getInserisciContattoItem(), context)) {
				return;
			}

			documentManagerDTO.getInserisciContattoItem().setIdAOO(utente.getIdAoo());
			creaUtente();
			documentManagerDTO.setInserisciContattoItem(new Contatto());
			updateMaschereAfterSelect();
			notaRichiestaCreazione = "";
		} catch (Exception e) {
			LOGGER.error(e);
			showError(e);
		} 
	}



	private void creaUtente() {  
		documentManagerDTO.getInserisciContattoItem().setOnTheFly(0);
		RubricaComponent.inserisciContatto(documentManagerDTO.getInserisciContattoItem(), utente);
		if (documentManagerDTO.isInCreazioneIngresso() || documentManagerDTO.isInModificaIngresso()) {
			detail.setMittenteContatto(documentManagerDTO.getInserisciContattoItem());
			setMittenteCreazioneModifica(); 
		} else if (documentManagerDTO.isInCreazioneUscita() || documentManagerDTO.isInModificaUscita()) {
			if(fromRichiediCreazioneDatatable!=-1) {
				DestinatarioRedDTO d = this.destinatariInModifica.get(fromRichiediCreazioneDatatable);
				setDestinatarioPerCreazione(d);
			} 
			else { 
				this.destinatariInModifica = getDestinatariInModificaPuliti();
				DestinatarioRedDTO d = new DestinatarioRedDTO(); 
				setDestinatarioPerCreazione(d);
				if(indiceColonnaModificata==null) {
					this.destinatariInModifica.add(d);
				}else {
					this.destinatariInModifica.set(indiceColonnaModificata, d);
				}
			}

		}
		if (Boolean.TRUE.equals(documentManagerDTO.getInserisciContattoItem().getIsPreferito())) {
			Contatto contattoDaAggiungereAiPreferiti = new Contatto();
			contattoDaAggiungereAiPreferiti.copyFromContatto(documentManagerDTO.getInserisciContattoItem());
			if(documentManagerDTO.isInCreazioneIngresso() || documentManagerDTO.isInModificaIngresso()) {
				documentManagerDTO.getContattiPreferiti().add(0,contattoDaAggiungereAiPreferiti); //Lo appendo in testa
			} else {
				documentManagerDTO.getContattiPreferitiFiltered().add(0,contattoDaAggiungereAiPreferiti);
			}
		} 
		showInfoMessage("Contatto creato correttamente");
 
	}

	private void setDestinatarioPerCreazione(DestinatarioRedDTO d) {
		if (!StringUtils.isNullOrEmpty(documentManagerDTO.getInserisciContattoItem().getMailPec())) {
			documentManagerDTO.getInserisciContattoItem().setMailSelected(documentManagerDTO.getInserisciContattoItem().getMailPec());
			documentManagerDTO.getInserisciContattoItem().setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEC);
		} else {
			documentManagerDTO.getInserisciContattoItem().setMailSelected(documentManagerDTO.getInserisciContattoItem().getMail());
			documentManagerDTO.getInserisciContattoItem().setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEO);
		} 
		
		d.setContatto(documentManagerDTO.getInserisciContattoItem());
		if (StringUtils.isNullOrEmpty(documentManagerDTO.getInserisciContattoItem().getMailSelected())) {
			d.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.CARTACEO);
			d.setMezzoSpedizioneDisabled(true);
		} else {
			d.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.ELETTRONICO);
			d.setMezzoSpedizioneDisabled(false);
		}

		d.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
		d.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
		d.setMezzoSpedizioneVisible(true);
		 
	}

	/**
	 * Reinizializza il contatto del {@link #documentManagerDTO} e nasconde la modifica.
	 */
	public void clearContattoandHideModifica() {
		TabView tb = null;
		documentManagerDTO.setInserisciContattoItem(new Contatto());
		inModificaDaShortcut=false;
		tb= (TabView) FacesContext.getCurrentInstance().getViewRoot().findComponent(PREFERITOTABV_RESOURCE_NAME);
		tb.setActiveIndex(0);
		indiceColonnaModificata = null;
		inizializzaCheckboxRubrica();
	}

	/**
	 * Gestisce la modifica del contatto del {@link #documentManagerDTO}.
	 */
	public void modificaContatto() {

		try {
			FacesContext context = FacesContext.getCurrentInstance();
			if (!RubricaComponent.valida(documentManagerDTO.getInserisciContattoItem(), context)) {
				return;
			}
			documentManagerDTO.getInserisciContattoItem().setIdAOO(utente.getIdAoo());
			modificaUtente();
			documentManagerDTO.setInserisciContattoItem(new Contatto());
			inModificaDaShortcut=false;
			updateMaschereAfterSelect();
			
		} catch (Exception e) {
			String errorCode = "MAILESISTENTE";
			if(e.getMessage().contains(errorCode)) {
				showError("Non è possibile modificare il contatto, è già presente a sistema un contatto con la stessa mail");
			} else {
				LOGGER.error(e);
				showError(e);
			}
		}

	}
 
	 
	private void modificaUtente() { 
		RubricaComponent.modificaContatto(documentManagerDTO.getInserisciContattoItem(), utente);
		if (documentManagerDTO.isInCreazioneIngresso() || documentManagerDTO.isInModificaIngresso()) {
			detail.setMittenteContatto(documentManagerDTO.getInserisciContattoItem());
			setMittenteCreazioneModifica();
		}else if (documentManagerDTO.isInCreazioneUscita() || documentManagerDTO.isInModificaUscita()) {
			if(fromRichiediCreazioneDatatable!=-1) {
				DestinatarioRedDTO d = this.destinatariInModifica.get(fromRichiediCreazioneDatatable);
				setDestinatarioPerCreazione(d);
			} 
			else { 
				this.destinatariInModifica = getDestinatariInModificaPuliti();
				DestinatarioRedDTO d = new DestinatarioRedDTO(); 
				setDestinatarioPerCreazione(d);
				if(indiceColonnaModificata==null) {
					this.destinatariInModifica.add(d);
				}else {
					this.destinatariInModifica.set(indiceColonnaModificata, d);
				}
			}

		}

		if(documentManagerDTO.getContattiPreferiti()!=null) {
			//* Allineo Preferiti START**//
			boolean isInPreferiti=false;
			Contatto contPreferito=null;

			for(Contatto contPref:documentManagerDTO.getContattiPreferiti()) {
				if(contPref.getContattoID().equals(documentManagerDTO.getInserisciContattoItem().getContattoID())) {
					isInPreferiti=true;
					contPreferito = contPref;
				}
			}

			if (Boolean.TRUE.equals(documentManagerDTO.getInserisciContattoItem().getIsPreferito())) {
				if(isInPreferiti) {
					contPreferito.copyFromContatto(documentManagerDTO.getInserisciContattoItem());
				}else {
					documentManagerDTO.getContattiPreferiti().add(documentManagerDTO.getInserisciContattoItem());
				}

			}else {
				if(isInPreferiti) {
					documentManagerDTO.getContattiPreferiti().remove(contPreferito);
				}

			}

			//* Allineo Preferiti END**//
		}


		if(documentManagerDTO.getResultRicercaContatti()!=null) {
			//* Allineo RICERCA START**//


			Contatto contRicercato=null;

			for(Contatto contRic:documentManagerDTO.getResultRicercaContatti()) {
				if(contRic.getContattoID().equals(documentManagerDTO.getInserisciContattoItem().getContattoID())) {
					contRicercato = contRic;
				}
			}

			if(contRicercato!=null) {
				contRicercato.copyFromContatto(documentManagerDTO.getInserisciContattoItem());
			}

			//* Allineo RICERCA END**//
		}
		showInfoMessage("Modifica contatto effettuata");
	}
 
	
		private void setMittenteCreazioneModifica() {
			detail.setMittenteContatto(documentManagerDTO.getInserisciContattoItem());
			if(!StringUtils.isNullOrEmpty(documentManagerDTO.getInserisciContattoItem().getMailPec())){
				detail.getMittenteContatto().setMailSelected(documentManagerDTO.getInserisciContattoItem().getMailPec());
				detail.getMittenteContatto().setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEC);
			} else if(!StringUtils.isNullOrEmpty(documentManagerDTO.getInserisciContattoItem().getMail())) {
				detail.getMittenteContatto().setMailSelected(documentManagerDTO.getInserisciContattoItem().getMail());
				detail.getMittenteContatto().setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEO);
			}
		}

		private void setMittentePerRichCreazioneModifica() {
			detail.setMittenteContatto(documentManagerDTO.getInserisciContattoItem());
			if(!StringUtils.isNullOrEmpty(documentManagerDTO.getInserisciContattoItem().getMailPec())){
				detail.getMittenteContatto().setMailSelected(documentManagerDTO.getInserisciContattoItem().getMailPec());
				detail.getMittenteContatto().setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEC); 
			} else if(!StringUtils.isNullOrEmpty(documentManagerDTO.getInserisciContattoItem().getMail())) {
				detail.getMittenteContatto().setMailSelected(documentManagerDTO.getInserisciContattoItem().getMail());
				detail.getMittenteContatto().setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEO); 
			}
			detail.getMittenteContatto().setOnTheFly(ON_THE_FLY);
		}

		/**
		 * Gestisce le informazioni associate al destinatario recuperandole dal {@link #documentManagerDTO}.
		 */
		public void setDestinatarioPerRichCreazModifica() {
			DestinatarioRedDTO d = new DestinatarioRedDTO();
	
			if (!StringUtils.isNullOrEmpty(documentManagerDTO.getInserisciContattoItem().getMailPec())) {
				documentManagerDTO.getInserisciContattoItem().setMailSelected(documentManagerDTO.getInserisciContattoItem().getMailPec());
				documentManagerDTO.getInserisciContattoItem().setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEC);
				documentManagerDTO.getInserisciContattoItem().setMail(documentManagerDTO.getInserisciContattoItem().getMailPec());
			} else if(!StringUtils.isNullOrEmpty(documentManagerDTO.getInserisciContattoItem().getMail())) {
				documentManagerDTO.getInserisciContattoItem().setMailSelected(documentManagerDTO.getInserisciContattoItem().getMail());
				documentManagerDTO.getInserisciContattoItem().setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEO);
				documentManagerDTO.getInserisciContattoItem().setMailPec(documentManagerDTO.getInserisciContattoItem().getMail());
			}
			documentManagerDTO.getInserisciContattoItem().setOnTheFly(ON_THE_FLY);
			d.setContatto(documentManagerDTO.getInserisciContattoItem());
			if (StringUtils.isNullOrEmpty(documentManagerDTO.getInserisciContattoItem().getMailSelected())) {
				d.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.CARTACEO);
				d.setMezzoSpedizioneDisabled(true);
			} else {
				d.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.ELETTRONICO);
				d.setMezzoSpedizioneDisabled(false);
			}
	
			d.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
			d.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
			d.setMezzoSpedizioneVisible(true);
			if(indiceColonnaModificata==null) {
				this.destinatariInModifica.add(d);
			}else {
				this.destinatariInModifica.set(indiceColonnaModificata, d);
			}
		}
 
		
		private void updateMaschereAfterSelect() {
			if(documentManagerDTO.isInCreazioneUscita() || documentManagerDTO.isInModificaUscita() ) {
				FacesHelper.update(IDDESTINATARIDMD_RESOURCE_LOCATION,IDTABSDMD_RESOURCE_NAME,PREFERITOTABV_RESOURCE_NAME);
				if(scegliContattoRifStorico) {
					FacesHelper.executeJS(HIDE_WDGCERCAPREFERITO_JS);
					scegliContattoRifStorico = false;
					onlyCreazioneContattoRifStorico = false;
				}
				aggiungiDestinatario();

			} else {
				inizializzaTab();
				FacesHelper.update("idDettagliEstesiForm:idOrlyPnlMittForm_DMD","idDettagliEstesiForm:idMittentePnl_DMD",IDTABSDMD_RESOURCE_NAME,PREFERITOTABV_RESOURCE_NAME);
			}
		}

		/**
		 * Gestisce la modifica del destinatario.
		 * @param modContatto
		 * @param activeIndexNum
		 */
		public void modificaDestinatario(Contatto modContatto, int activeIndexNum) {
			if (isPermessoUtenteGestioneRichiesteRubrica()) {
				documentManagerDTO.setInserisciContattoItem(modContatto);
			} else {
				Contatto richiediModifica = new Contatto();
				richiediModifica.copyFromContatto(modContatto);
				documentManagerDTO.setInserisciContattoItem(richiediModifica);
				contattoVecchioItem = new Contatto();
				contattoVecchioItem.copyFromContatto(modContatto);
			}
	
			inModificaDaShortcut = true;
			TabView tb = null;
	
			if (!detail.getFromProtocollaMail()) {
				if (documentManagerDTO.isMittenteVisible()) {
					tb = (TabView) FacesContext.getCurrentInstance().getViewRoot()
							.findComponent(IDMITTENTEPREFTABSDMD_RESOURCE_NAME);
					tb.setActiveIndex(activeIndexNum);
				} else if (documentManagerDTO.isDestinatariVisible()) {
					tb = (TabView) FacesContext.getCurrentInstance().getViewRoot()
							.findComponent(PREFERITOTABV_RESOURCE_NAME);
					tb.setActiveIndex(activeIndexNum);
				}
			} else {
				tb = (TabView) FacesContext.getCurrentInstance().getViewRoot()
						.findComponent("eastSectionForm:idMittentePrefTabs_DMD");
				tb.setActiveIndex(activeIndexNum);
			}
		}

	  /**
	   * Imposta il contatto selezionato sul datatable come contatto da eliminare: {@link #eliminaContattoItem}.
	   * @param event
	   */
	  public void impostaContattoDaEliminare(ActionEvent event) {
		  Contatto c = getDataTableClickedRow(event);
		  eliminaContattoItem = new Contatto();
		  eliminaContattoItem.copyFromContatto(c);
		  notaRichiestaEliminazione = "";
	  }

	  /**
	   * Esegue l'eliminazione del contatto.
	   * @param contattoSelected
	   * @param fromRisultatiRicerca
	   */
	  public void eliminaContatto(final Contatto contattoSelected,final boolean fromRisultatiRicerca) {
		  try {
			  eliminaContatto(contattoSelected);
			  String formName = "idDettagliEstesiForm";
			  if (MBean.MAIL_BEAN.equalsIgnoreCase(chiamante)) {
				  formName = EAST_SECTION_FORM_NAME;
			  }
			  if(fromRisultatiRicerca) {
				  DataTable tableMitt = getDataTableByIdComponent(formName+":idMittentePrefTabs_DMD:idRicRubrica_DMDRC");
				  if (tableMitt != null) {
					  tableMitt.reset();
				  }

				  DataTable table = getDataTableByIdComponent(formName+":preferitoTabV:idRicRubrica_DMDRC");
				  if (table != null) {
					  table.reset();
				  }
				  documentManagerDTO.getResultRicercaContatti().remove(contattoSelected);

				  if(documentManagerDTO.isInCreazioneIngresso() || documentManagerDTO.isInModificaIngresso()) {
					  FacesHelper.update(formName+":idMittentePrefTabs_DMD:idResults_DMDRC");
				  } else if(documentManagerDTO.isInCreazioneUscita() || documentManagerDTO.isInModificaUscita()){
					  FacesHelper.update(formName+":preferitoTabV:idResults_DMDRC");
				  }
			  } else {

				  DataTable table = null;
				  if (documentManagerDTO.isMittenteVisible()) {
					  table = getDataTableByIdComponent(formName+":idMittentePrefTabs_DMD:idPreferitiMittenti_DMD");
				  } else if (documentManagerDTO.isDestinatariVisible()) {
					  table = getDataTableByIdComponent(formName+":preferitoTabV:contattiPreferitiTbl");
				  } 
				  if (table != null) {
					  table.reset();
				  }
				  if(documentManagerDTO.isInCreazioneIngresso() || documentManagerDTO.isInModificaIngresso()) {
					  documentManagerDTO.getContattiPreferiti().remove(contattoSelected);
				  } else if(documentManagerDTO.isInCreazioneUscita() || documentManagerDTO.isInModificaUscita()){
					  documentManagerDTO.getContattiPreferitiFiltered().remove(contattoSelected);
				  }
			  }
			  showInfoMessage("Eliminazione contatto avvenuta con successo");

		  } catch(Exception ex) {
			  LOGGER.error("Errore durante l'eliminazione del contatto", ex);
			  showError("Errore durante l'eliminazione del contatto");
		  }
	  }
	  
	  private void eliminaContatto(Contatto contattoSelected) {
		  rubricaSRV.elimina(contattoSelected.getContattoID(), null, null);
	  }
	   
	  


	/** SHORTCUT CREAZIONE CONTATTI END************************************************************************/

	/** SHORTCUT RICERCA CONTATTI START**********************************************************************/
	/**
	 * Effettua un reset dei campi rubrica per la ricerca.
	 */
	public void resetCampiRubricaRicerca() {
		RicercaRubricaDTO r = new RicercaRubricaDTO();
		r.setRicercaRED(documentManagerDTO.getRicercaRubricaDTO().getRicercaRED());
		r.setRicercaIPA(documentManagerDTO.getRicercaRubricaDTO().getRicercaIPA());
		r.setRicercaMEF(documentManagerDTO.getRicercaRubricaDTO().getRicercaMEF()); 
		r.setRicercaRubricaUfficio(documentManagerDTO.getRicercaRubricaDTO().getRicercaRubricaUfficio());
		r.setIsUfficio(true);
		documentManagerDTO.setRicercaRubricaDTO(r);
	}

	/**
	 * Inizializza le checkbox per la rubrica.
	 */
	public void inizializzaCheckboxRubrica() {
		RicercaRubricaDTO r = new RicercaRubricaDTO();
		r.setRicercaRED(true);
		r.setRicercaIPA(true);
		r.setRicercaMEF(true);  
		r.setIsUfficio(true);
		documentManagerDTO.setRicercaRubricaDTO(r);

	}

	/**
	 * Esegue la ricerca del contatto nella rubrica.
	 */
	public void cercaContattoRubrica() {
		try {
			RicercaRubricaDTO ricercaRubricaDTO = documentManagerDTO.getRicercaRubricaDTO();
			ricercaRubricaDTO.setIdAoo(Integer.parseInt(""+utente.getIdAoo()));
			if ((ricercaRubricaDTO.getRicercaRED() || ricercaRubricaDTO.getRicercaIPA()) && Boolean.TRUE.equals(ricercaRubricaDTO.getRicercaMEF())) {
				ricercaRubricaDTO.setIsUfficio(null);
			}

			DataTable tableMitt = getDataTableByIdComponent("idDettagliEstesiForm:idMittentePrefTabs_DMD:idRicRubrica_DMDRC");
			if (tableMitt != null) {
				tableMitt.reset();
			}

			DataTable table = getDataTableByIdComponent("idDettagliEstesiForm:preferitoTabV:idRicRubrica_DMDRC");
			if (table != null) {
				table.reset();
			}
			hiddenTable = false;
			if(ricercaRubricaDTO.getRicercaRED() || ricercaRubricaDTO.getRicercaIPA()) {
				boolean ricercaMEF = ricercaRubricaDTO.getRicercaRED();
				documentManagerDTO.setResultRicercaContatti(rubricaSRV.ricercaCampoSingolo(ricercaRubricaDTO.getQueryRicerca(), utente.getIdUfficio(), utente.getIdAoo().intValue(), ricercaRubricaDTO.getRicercaRED(), ricercaRubricaDTO.getRicercaIPA(), ricercaMEF, false, "")); 
				documentManagerDTO.setResultRicercaContattiFiltered(documentManagerDTO.getResultRicercaContatti());
			} else if(Boolean.TRUE.equals(ricercaRubricaDTO.getRicercaRubricaUfficio())) {
				documentManagerDTO.setResultRicercaContatti(matchQuery(documentManagerDTO.getContattiPreferiti(), ricercaRubricaDTO.getQueryRicerca()));
				documentManagerDTO.setResultRicercaContattiFiltered(documentManagerDTO.getResultRicercaContatti());
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Recupera il contatto ottenuto dalla ricerca e lo aggiunge come destinatario a {@link #destinatariInModifica}.
	 * @param index
	 */
	public void addDestinatarioFromRicercaRubrica(Object index) {
		try {
			Integer i = (Integer) index;
			Contatto c = documentManagerDTO.getResultRicercaContattiFiltered().get(i);
			if (c == null) {
				showError("Contatto non trovato.");
				return;
			}

			c.setMailSelected(c.getMailPec());
			c.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEC);
			
			
			if (StringUtils.isNullOrEmpty(c.getMailSelected())) {
				c.setMailSelected(c.getMail());
				c.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEO);
			}
			//Serve per disabilitare il tasto aggiungi, altrimenti può essere cliccato più volte
			c.setDisabilitaElemento(true);

			this.destinatariInModifica = getDestinatariInModificaPuliti();

			for (DestinatarioRedDTO d : destinatariInModifica) {
				if (c.getContattoID() != null && d.getContatto() != null && c.getContattoID().equals(d.getContatto().getContattoID())) {
					showWarningMaschera("Il contatto è già stato inserito tra i destinatari!");
					return;
				}
			}

			DestinatarioRedDTO d = new DestinatarioRedDTO();
			setContattoDestinatario(c, d);
			if(indiceColonnaModificata==null) {
				this.destinatariInModifica.add(d);
			}else {
				this.destinatariInModifica.set(indiceColonnaModificata, d);
			}

			
			if(scegliContattoRifStorico) {
				FacesHelper.executeJS(HIDE_WDGCERCAPREFERITO_JS);
				scegliContattoRifStorico = false;
				onlyCreazioneContattoRifStorico = false;
			}
			
			documentManagerSRV.checkDestinatariInterni(detail, destinatariInModifica, documentManagerDTO);
			aggiungiDestinatario();
		} catch (Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Recupera il contatto ottenuto dalla ricerca e lo aggiunge come mittente a {@link #detail}.
	 * @param index
	 */
	public void addMittenteFromRicercaRubrica(Object index) {
		try {
			Integer i = (Integer) index;
			Contatto c = documentManagerDTO.getResultRicercaContattiFiltered().get(i);
			if (c == null) {
				showError("Contatto non trovato.");
				return;
			}

			c.setMailSelected(c.getMailPec());
			c.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEC);
			if (StringUtils.isNullOrEmpty(c.getMailSelected())) {
				c.setMailSelected(c.getMail());
				c.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEO);
			}
			
			inModificaDaShortcut = false;
			if(c.getTipoRubrica()!=null && "RED".equals(c.getTipoRubrica())) {
				creaORichiediCreazioneFlag = true;
				inModificaDaShortcut = true;
			} 
			this.detail.setMittenteContatto(c);
			
			if (!StringUtils.isNullOrEmpty(chiamante)  && ConstantsWeb.MBean.MAIL_BEAN.equalsIgnoreCase(chiamante)) {
				FacesHelper.update(IDMITTENTEAUTO_DMD_RESOURCE_LOCATION);
				FacesHelper.update("eastSectionForm:idMittentePnl_DMD");
			}

		} catch (Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Rimuove il contatto associato alla riga selezionata sul datatable dalla lista dei contatti preferiti.
	 * @param event
	 */
	public void rimuoviPreferito(ActionEvent event) {
		try {
			Contatto contatto = getDataTableClickedRow(event);

			rubricaSRV.rimuoviPreferito(utente.getIdUfficio(), contatto.getContattoID(), contatto.getTipoRubricaEnum());
			contatto.setIsPreferito(false);
			int index = 0;
			for (Contatto c : documentManagerDTO.getContattiPreferiti()) {
				if (c.getContattoID().equals(contatto.getContattoID())) {
					break;
				}
				index++;
			}

			documentManagerDTO.getContattiPreferiti().remove(index);
			documentManagerDTO.setContattiPreferitiFiltered(documentManagerDTO.getContattiPreferiti());
			DataTable table = null;
			if (documentManagerDTO.isMittenteVisible()) {
				table = getDataTableByIdComponent(IDPREFERITIMITTENTIDMD_RESOURCE_NAME);
			} else if (documentManagerDTO.isDestinatariVisible()) {
				table = getDataTableByIdComponent(CONTATTIPREFERITITBL_RESOURCE_NAME);
			} 
			if (table != null) {
				table.reset();
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Aggiunge un contatto identificato dalla riga selezionata sul datatable alla lista dei preferiti.
	 * @param event
	 */
	public void aggiungiPreferito(ActionEvent event) {
		try {
			Contatto contatto = getDataTableClickedRow(event);

			rubricaSRV.aggiungiAiPreferiti(utente.getIdUfficio(), contatto.getContattoID(), contatto.getTipoRubricaEnum(),utente.getId());
			contatto.setIsPreferito(true);
			if (documentManagerDTO.getContattiPreferiti() == null ) {
				documentManagerDTO.setContattiPreferiti(new ArrayList<>());
			}
			documentManagerDTO.getContattiPreferiti().add(contatto);
			contatto.setIsPreferito(true);
			documentManagerDTO.setContattiPreferitiFiltered(documentManagerDTO.getContattiPreferiti());

			DataTable table = null;
			if (documentManagerDTO.isMittenteVisible()) {
				table = getDataTableByIdComponent(IDPREFERITIMITTENTIDMD_RESOURCE_NAME);
			} else if (documentManagerDTO.isDestinatariVisible()) {
				table = getDataTableByIdComponent(CONTATTIPREFERITITBL_RESOURCE_NAME);
			} 
			if (table != null) {
				table.reset();
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}
	/** SHORTCUT RICERCA CONTATTI END************************************************************************/

	/******* TEMPLATE DOC USCITA ***************************************************/
	/**
	 * Inizializza il component che gestisce i template applicativi.
	 */
	public void initTemplateDocUscitaCmp() {
		if (templateDocUscitaComponent != null) {
			// Si recuperano i destinatari effettivi
			detail.setDestinatari(getDestinatariInModificaPuliti());
			templateDocUscitaComponent.impostaMetadatiValoreDaDettaglioNew(detail,documentManagerDTO.isInModificaUscita());
			if(!StringUtils.isNullOrEmpty(detail.getDocumentTitle())){
				templateDocUscitaComponent.getVersioniDocumento(Integer.valueOf(detail.getDocumentTitle()));
			} 
		}
	}


	/**
	 * Aggiorna il dettaglio (e la maschera) con i dati provienienti dalla generazione del documento tramite template applicativi.
	 * 
	 * @param template
	 * @param metadati
	 * @param fileDocumento
	 */
	public void aggiornaDocumentoDaTemplateDocUscita(TemplateValueDTO template, List<TemplateMetadatoValueDTO> metadati, FileDTO fileDocumento) {
		if (detail.getTemplateDocUscitaMap() == null) {
			detail.setTemplateDocUscitaMap(new HashMap<>());
		} else {
			detail.getTemplateDocUscitaMap().clear();
		}
		 

		// Si impostano i metadati provenienti dal template nel dettaglio
		detail.getTemplateDocUscitaMap().put(template, metadati);

		// Si gestisce l'upload del file
		handleMainFileUpload(fileDocumento, true);
	}


	private void handleTemplateDocUscitaOnMainFileChange(boolean uploadFromTemplateDocUscita) {
		// In caso di modifica del content del documento tramite altra funzionalità:
		// - Si rimuovono dal dettaglio i metadati del Template Doc Uscita
		// - Si resetta il component che gestisce la maschera dei template
		if (!uploadFromTemplateDocUscita && MapUtils.isNotEmpty(detail.getTemplateDocUscitaMap())) {
			detail.getTemplateDocUscitaMap().clear();
			if (templateDocUscitaComponent != null) {
				templateDocUscitaComponent.reset();
			}
		}

		documentManagerDTO.setGeneratedByTemplateDocUscita(uploadFromTemplateDocUscita);
		documentManagerDTO.setTemplateDocUscitaVisible(
				documentManagerSRV.isTemplateDocUscitaButtonVisible(PermessiUtils.hasPermesso(utente.getPermessiAOO(), PermessiAOOEnum.TEMPLATE_DOC_USCITA), 
						detail.getContentModificabile(), documentManagerDTO.isGeneratedByTemplateDocUscita(),
						documentManagerDTO.isInCreazioneUscita(), documentManagerDTO.isInModificaUscita(), !StringUtils.isNullOrEmpty(detail.getNomeFile())));
	}
	
	private void checkTipoNotifica() {
		// Recupero tipologia notifica per la gestione di protocolli associati a
		// RISPONDI_A e AGGIORNAMENTO_COLLEGATI
		final NotificaNpsDTO notificaNps = notificaNpsSRV.getNotificaAzione(utente.getIdAoo(), detail.getNumeroProtocollo(), detail.getAnnoProtocollo());
		if (notificaNps != null && TipoNotificaAzioneNPSEnum.isRispostaAutomatica(notificaNps.getTipoNotifica())) {
			
			setElettronicoViaSistemaAusiliarioNPS(true);
			setSistemaAusiliarioNPS(((ParametersAzioneRispostaAutomatica)((NotificaAzioneNpsDTO)notificaNps).getDatiAzione()).getSistemaAusiliario());
			
			if(TipoNotificaAzioneNPSEnum.RISPONDI_A.equals(TipoNotificaAzioneNPSEnum.fromValue(notificaNps.getTipoNotifica()))) {
				setSistemaAusiliarioNPS(Constants.Varie.SICOGE);
			}
			
		} else {
			setElettronicoViaSistemaAusiliarioNPS(false);
			setSistemaAusiliarioNPS(null);
		}
	}
	
	/******* TEMPLATE DOC USCITA ***************************************************/

	private void gestisciDialogHeader() {
		if (documentManagerDTO.isInCreazioneIngresso() || documentManagerDTO.isInCreazioneUscita()) {
			dialogHeader = "Creazione documento in ";
			
			if (documentManagerDTO.isInCreazioneIngresso()) {
				dialogHeader += "ingresso";
			} else if (documentManagerDTO.isInCreazioneUscita()) {
				dialogHeader += "uscita";
			}
			
			if (ResponsesRedEnum.RISPONDI_INGRESSO.toString().equals(chiamante)) {
				dialogHeader += ": Rispondi";
			} else if (ResponsesRedEnum.INOLTRA_INGRESSO.toString().equals(chiamante)) {
				dialogHeader += ": Inoltra";
			} else if (ResponsesRedEnum.PREDISPONI_VISTO.toString().equals(chiamante)) {
				dialogHeader += ": Visto";
			} else if (ResponsesRedEnum.PREDISPONI_OSSERVAZIONE.toString().equals(chiamante)) {
				dialogHeader += ": Osservazione";
			} else if (ResponsesRedEnum.RICHIESTA_INTEGRAZIONI.toString().equals(chiamante)) {
				dialogHeader += ": Richiesta Integrazioni";
			} else if (ResponsesRedEnum.PREDISPONI_RESTITUZIONE.toString().equals(chiamante)) {
				dialogHeader += ": Restituzione";
			} else if (ResponsesRedEnum.PREDISPONI_RELAZIONE_POSITIVA.toString().equals(chiamante)) {
				dialogHeader += ": Relazione Positiva";
			} else if (ResponsesRedEnum.PREDISPONI_RELAZIONE_NEGATIVA.toString().equals(chiamante)) {
				dialogHeader += ": Relazione Negativa";
			}
		} else {
			dialogHeader = "Gestisci documento";
			//ap
			if (CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0].equals(detail.getIdCategoriaDocumento())) {
				dialogHeader += " in entrata";
				if (detail.getIntegrazioneDati()) {
					dialogHeader += " (" + Constants.Varie.DESCRIZIONE_INTEGRAZIONE_DATI + ")";
				}
			} else if (CategoriaDocumentoEnum.DOCUMENTO_USCITA.getIds()[0].equals(detail.getIdCategoriaDocumento())) {
				dialogHeader += " in uscita";
				if (detail.getSottoCategoriaUscita() != null && detail.getSottoCategoriaUscita().getId() != 0) {
					dialogHeader += " ("+detail.getSottoCategoriaUscita().getDescrizione() + ")";
				}
			}
		}
	}
	
	private void setChiamanteOneKindOfPredisponi() {
		if (isChiamanteOneKindOfPredisponiUCB() || ResponsesRedEnum.PREDISPONI_DOCUMENTO.toString().equals(chiamante)) {
			chiamantePredisponi = true;
		}
	}
	
	private boolean isChiamanteOneKindOfPredisponiUCB() {
		return ResponsesRedEnum.RISPONDI_INGRESSO.toString().equals(chiamante) || ResponsesRedEnum.INOLTRA_INGRESSO.toString().equals(chiamante)
    			|| isChiamanteOneKindOfPredisponiVistoOssIntegrazioniRest();
	}

	/**
	 * Restituisce true se il chiamante è associato a una delle predisposizioni documento, false altrimenti.
	 * @return true se il chiamante è associato ad una predisposizione, false altrimenti.
	 */
	public boolean isChiamanteOneKindOfPredisponiVistoOssIntegrazioniRest() {
		return ResponsesRedEnum.PREDISPONI_VISTO.toString().equals(chiamante) || ResponsesRedEnum.PREDISPONI_OSSERVAZIONE.toString().equals(chiamante)
				|| ResponsesRedEnum.RICHIESTA_INTEGRAZIONI.toString().equals(chiamante) || ResponsesRedEnum.PREDISPONI_RESTITUZIONE.toString().equals(chiamante)
				|| ResponsesRedEnum.PREDISPONI_RELAZIONE_POSITIVA.toString().equals(chiamante) || ResponsesRedEnum.PREDISPONI_RELAZIONE_NEGATIVA.toString().equals(chiamante);
	}

	/**
	 * Inizializza il pannello per la predisposizione del template.
	 */
	public void initPnlTemplatePredisponi() {
		PredisponiDaRegistroAusiliarioBean pvb = FacesHelper.getManagedBean(MBean.PREDISPONI_DA_REGISTRO_AUSILIARIO_BEAN);
		if (pvb != null) {
			pvb.initDaDettaglioEsteso(detail.getSottoCategoriaUscita(), detail.getNomeFile(), detail.getIdRegistroAusiliario(), 
					detail.getMetadatiRegistroAusiliario(), allaccioPrincipale.getDocument(), detail.getDocumentTitle());
		}
	}

	/**
	 * Esegue l'aggiornamento del documento da template e gestisce l'upload del file.
	 * @param content
	 * @param idRegistroAusiliario
	 * @param metadatiRegistroAusiliario
	 */
	public void aggiornaDocumentoDaTemplatePredisponi(FileDTO content, Integer idRegistroAusiliario, Collection<MetadatoDTO> metadatiRegistroAusiliario) {
		// Si aggiornano i dati del dettaglio con quelli provenienti dalla maschera del template
		detail.setIdRegistroAusiliario(idRegistroAusiliario);
		detail.setMetadatiRegistroAusiliario(metadatiRegistroAusiliario);

		// Si gestisce l'upload del file
		handleMainFileUpload(content, true);
	}
	
	/****GET & SET START***************************************************************************************/
	/**
	 * Restituisce il detail.
	 * @return detail
	 */
	public DetailDocumentRedDTO getDetail() {
		return detail;
	}

	/**
	 * Restituisce la modalita della maschera.
	 * @return modalita maschera
	 */
	public String getModalitaMaschera() {
		return modalitaMaschera;
	}

	/**
	 * Restituisce l'id del formato documento selezionato.
	 * @return idFormatoDocumentoSelected
	 */
	public Long getIdFormatoDocumentoSelected() {
		return idFormatoDocumentoSelected;
	}

	/**
	 * Imposta l'id del formato documento selezionato.
	 * @param id FormatoDocumento selezionato
	 */
	public void setIdFormatoDocumentoSelected(Long idFormatoDocumentoSelected) {
		this.idFormatoDocumentoSelected = idFormatoDocumentoSelected;
	}

	/**
	 * Restituisce il documentManagerDTO.
	 * @return documentManagerDTO
	 */
	public DocumentManagerDTO getDocumentManagerDTO() {
		return documentManagerDTO;
	}

	/**
	 * Restituisce l'id del tipo procedimento selezionato.
	 * @return id tipo procedimento selezionato
	 */
	public Long getIdTipoProcedimentoSelected() {
		return idTipoProcedimentoSelected;
	}

	/**
	 * Imposta l'id del tipo procedimento selezionato.
	 * @param idTipoProcedimentoSelected
	 */
	public void setIdTipoProcedimentoSelected(Long idTipoProcedimentoSelected) {
		this.idTipoProcedimentoSelected = idTipoProcedimentoSelected;
	}

	/**
	 * Restituisce il contatto da visualizzare.
	 * @return contatto da visualizzare
	 */
	public Contatto getContattoDaVisualizzare() {
		return contattoDaVisualizzare;
	}

	/**
	 * Imposta il contatto da visualizzare.
	 * @param contattoDaVisualizzare
	 */
	public void setContattoDaVisualizzare(Contatto contattoDaVisualizzare) {
		this.contattoDaVisualizzare = contattoDaVisualizzare;
	}

	/**
	 * Restituisce la lista dei destinatari in modifica.
	 * @return lista destinatari
	 */
	public List<DestinatarioRedDTO> getDestinatariInModifica() {
		//torno solo gli elementi diversi da null altriemtni va in eccezione
		if(destinatariInModifica!=null && !destinatariInModifica.isEmpty()) {
			int index=0;
			for(DestinatarioRedDTO dest : destinatariInModifica) {
				if(dest == null) {
					destinatariInModifica.set(index, new DestinatarioRedDTO());
				}
				index++;
			}
		}
		return destinatariInModifica;
	}

	/**
	 * Imposta la lista dei destinatari in modifica.
	 * @param destinatariInModifica
	 */
	public void setDestinatariInModifica(List<DestinatarioRedDTO> destinatariInModifica) {
		this.destinatariInModifica = destinatariInModifica;
	}

	/**
	 * Restituisce la lista delle risposte allaccio.
	 * @return lista di RispostaAllaccioDTO
	 */
	public List<RispostaAllaccioDTO> getRiferimentiProt() {
		return riferimentiProt;
	}

	/**
	 * Imposta la lista dei riferimenti protocollo.
	 * @param riferimentiProt
	 */
	public void setRiferimentiProt(List<RispostaAllaccioDTO> riferimentiProt) {
		this.riferimentiProt = riferimentiProt;
	}

	/**
	 * Restituisce la descrizione dell'assegnatario per competenza costituita da utente - ufficio.
	 * @return descrizione assegnatario competenza
	 */
	public String getDescrAssegnatarioPerCompetenza() {
		return descrAssegnatarioPerCompetenza;
	}

	/**
	 * Imposta la descrizione dell'assegnatario per competenza.
	 * @param descrAssegnatarioPerCompetenza
	 */
	public void setDescrAssegnatarioPerCompetenza(String descrAssegnatarioPerCompetenza) {
		this.descrAssegnatarioPerCompetenza = descrAssegnatarioPerCompetenza;
	}

	/**
	 * Restituisce fadRagioneriaSelected.
	 * @return fadRagioneriaSelected
	 */
	public FadRagioneriaDTO getFadRagioneriaSelected() {
		return fadRagioneriaSelected;
	}

	/**
	 * Imposta fadRagioneriaSelected.
	 * @param fadRagioneriaSelected
	 */
	public void setFadRagioneriaSelected(FadRagioneriaDTO fadRagioneriaSelected) {
		this.fadRagioneriaSelected = fadRagioneriaSelected;
	}

	/**
	 * Restituisce l'identificativo raccolta.
	 * @return identificativo raccolta
	 */
	public String getIdentificativoRaccolta() {
		return identificativoRaccolta;
	}

	/**
	 * Imposta l'identificativo raccolta.
	 * @param identificativoRaccolta
	 */
	public void setIdentificativoRaccolta(String identificativoRaccolta) {
		this.identificativoRaccolta = identificativoRaccolta;
	}

	/**
	 * Restituisce il codice amministrazione selezionato.
	 * @return codice amministrazione
	 */
	public String getCodiceAmministrazioneSelected() {
		return codiceAmministrazioneSelected;
	}

	/**
	 * Imposta il codice amministrazione selezionato.
	 * @param codiceAmministrazioneSelected
	 */
	public void setCodiceAmministrazioneSelected(String codiceAmministrazioneSelected) {
		this.codiceAmministrazioneSelected = codiceAmministrazioneSelected;
	}

	/**
	 * Restituisce la descrizione dell'amministrazione selezionata.
	 * @return descrizione amministrazione
	 */
	public String getDescrizioneAmministrazioneSelected() {
		return descrizioneAmministrazioneSelected;
	}

	/**
	 * Imposta la descrizione dell'amministrazione selezionata.
	 * @param descrizioneAmministrazioneSelected
	 */
	public void setDescrizioneAmministrazioneSelected(String descrizioneAmministrazioneSelected) {
		this.descrizioneAmministrazioneSelected = descrizioneAmministrazioneSelected;
	}

	/**
	 * Restituisce la radice del tree associato alle assegnazioni iter manuale.
	 * @return rootAssegnazioneIterManuale
	 */
	public DefaultTreeNode getRootAssegnazioneIterManuale() {
		return rootAssegnazioneIterManuale;
	}

	/**
	 * Imposta la radice del tree associato alle assegnazioni iter manuale.
	 * @param rootAssegnazioneIterManuale
	 */
	public void setRootAssegnazioneIterManuale(DefaultTreeNode rootAssegnazioneIterManuale) {
		this.rootAssegnazioneIterManuale = rootAssegnazioneIterManuale;
	}

	/**
	 * Restituisce la descrizione dell'assegnatario iter manuale.
	 * @return descrizione assegnatario
	 */
	public String getDescrAssegnatarioIterManuale() {
		return descrAssegnatarioIterManuale;
	}

	/**
	 * Imposta la descrizione dell'assegnatario iter manuale.
	 * @param descrAssegnatarioIterManuale
	 */
	public void setDescrAssegnatarioIterManuale(String descrAssegnatarioIterManuale) {
		this.descrAssegnatarioIterManuale = descrAssegnatarioIterManuale;
	}

	/**
	 * Imposta la descrizione dell'assegnatario per conoscenza.
	 * @param assegnatariPerConoscenza
	 */
	public void setAssegnatariPerConoscenza(List<AssegnazioneDTO> assegnatariPerConoscenza) {
		this.assegnatariPerConoscenza = assegnatariPerConoscenza;
	}

	/**
	 * Imposta la descrizione dell'assegnatario per contributo.
	 * @param assegnatariPerContributo
	 */
	public void setAssegnatariPerContributo(List<AssegnazioneDTO> assegnatariPerContributo) {
		this.assegnatariPerContributo = assegnatariPerContributo;
	}

	/**
	 * Restituisce la lista degli assegnatari per conoscenza.
	 * @return assegnatari per conoscenza
	 */
	public List<AssegnazioneDTO> getAssegnatariPerConoscenza() {
		return assegnatariPerConoscenza;
	}

	/**
	 * Restituisce la lista degli assegnatari per contributo.
	 * @return assegnatari per contributo
	 */
	public List<AssegnazioneDTO> getAssegnatariPerContributo() {
		return assegnatariPerContributo;
	}

	/**
	 * Restituisce la radice del tree associato all'organigramma comune.
	 * @return radie del tree
	 */
	public DefaultTreeNode getRootOrganigrammaComune() {
		return rootOrganigrammaComune;
	}

	/**
	 * Restituisce l'id del testo predefiinito mail.
	 * @return id testo
	 */
	public Long getIdTestoPredefinitoMail() {
		return idTestoPredefinitoMail;
	}

	/**
	 * Imposta l'id del testo predefiinito mail.
	 * @param idTestoPredefinitoMail
	 */
	public void setIdTestoPredefinitoMail(Long idTestoPredefinitoMail) {
		this.idTestoPredefinitoMail = idTestoPredefinitoMail;
	}

	/**
	 * Restituisce gli allegati alla mail selezionati.
	 * @return lista allegati mail
	 */
	public List<DetailDocumentRedDTO> getAllegatiMailSelected() {
		return allegatiMailSelected;
	}

	/**
	 * Imposta la lista degli allegati alla mail selezionati.
	 * @param allegatiMailSelected
	 */
	public void setAllegatiMailSelected(List<DetailDocumentRedDTO> allegatiMailSelected) {
		this.allegatiMailSelected = allegatiMailSelected;
	}

	/**
	 * Restitusce la chiave di ricerca fascicolo.
	 * @return ricercaFascicoloKey
	 */
	public String getRicercaFascicoloKey() {
		return ricercaFascicoloKey;
	}

	/**
	 * Imposta la chiave di ricerca fascicolo.
	 * @param ricercaFascicoloKey
	 */
	public void setRicercaFascicoloKey(String ricercaFascicoloKey) {
		this.ricercaFascicoloKey = ricercaFascicoloKey;
	}

	/**
	 * Restituisce l'anno di ricerca fascicolo.
	 * @return anno fascicolo
	 */
	public Integer getRicercaFascicoloAnno() {
		return ricercaFascicoloAnno;
	}

	/**
	 * Imposta l'anno di ricerca fascicolo.
	 * @param ricercaFascicoloAnno
	 */
	public void setRicercaFascicoloAnno(Integer ricercaFascicoloAnno) {
		this.ricercaFascicoloAnno = ricercaFascicoloAnno;
	}

	/**
	 * Restitusce la modalita di ricerca fascicolo.
	 * @see RicercaGenericaTypeEnum
	 * @return modalita ricerca
	 */
	public RicercaGenericaTypeEnum getRicercaFascicoloModalita() {
		return ricercaFascicoloModalita;
	}

	/**
	 * Imposta la modalita di ricerca fascicolo.
	 * @param ricercaFascicoloModalita
	 */
	public void setRicercaFascicoloModalita(RicercaGenericaTypeEnum ricercaFascicoloModalita) {
		this.ricercaFascicoloModalita = ricercaFascicoloModalita;
	}

	/**
	 * Restituisce la lista dei fascicoli recuperati dalla ricerca.
	 * @return risultato ricerca fascicoli
	 */
	public List<FascicoloDTO> getRicercaFascicoliResults() {
		return ricercaFascicoliResults;
	}

	/**
	 * Restituisce l'id dell'utente coordinatore.
	 * @return id utente coordinatore
	 */
	public Long getIdUtenteCoordinatore() {
		return idUtenteCoordinatore;
	}

	/**
	 * Imposta l'id dell'utente coordinatore.
	 * @param idUtenteCoordinatore
	 */
	public void setIdUtenteCoordinatore(Long idUtenteCoordinatore) {
		this.idUtenteCoordinatore = idUtenteCoordinatore;
	}

	/**
	 * Restituisce la radice del tree per la gestione degli assegnatari per competenza.
	 * @return radice del tree degli assegnatari per competenza.
	 */
	public DefaultTreeNode getRootAssegnazionePerCompetenza() {
		return rootAssegnazionePerCompetenza;
	}

	/**
	 * Imposta la radice del tree per la gestione degli assegnatari per competenza.
	 * @param rootAssegnazionePerCompetenza
	 */
	public void setRootAssegnazionePerCompetenza(DefaultTreeNode rootAssegnazionePerCompetenza) {
		this.rootAssegnazionePerCompetenza = rootAssegnazionePerCompetenza;
	}

	/**
	 * Restituisce il messaggio dell'esito ottenuto a valle del tentativo di registrazione.
	 * @return messaggio esito registrazione
	 */
	public Map<String, String> getMessaggioEsitoRegistra() {
		return messaggioEsitoRegistra;
	}

	/**
	 * Restituisce il component per la gestione della dialog dei faldoni.
	 * @return faldonaCmp
	 */
	public DialogFaldonaComponent getFaldonaCmp() {
		return faldonaCmp;
	}

	/**
	 * Restituisce il component per la gestione del tab delle approvazioni.
	 * @return il component
	 */
	public DocumentManagerDataTabApprovazioniComponent getApprovazioniTab() {
		return approvazioniTab;
	}


	/**
	 * Ha la funzione di una get che restituisce solo gli elementi non vuoti, gli elementi rimangono comunque nella lista originale
	 **/
	public List<DestinatarioRedDTO> getDestinatariInModificaPuliti() {
		List<DestinatarioRedDTO> listaPulita = new ArrayList<>(); 
		if (destinatariInModifica != null) {
			for (DestinatarioRedDTO dest : destinatariInModifica) {
				if (dest != null && dest.getContatto() != null) {
					listaPulita.add(dest);
				}
			}
		}

		return listaPulita;
	}

	/**
	 * Esegue un update del datatable.
	 */
	public void updateDataTable() {
		if(daAggiornare) {
			daAggiornare = false;
			FacesHelper.update(IDDESTINATARIDMD_RESOURCE_LOCATION);
		}
	}

	/**
	 * Restituisce true se il tab spedizione è disabilitato, false altrimenti.
	 * @return true se il tab spedizione è disabilitato, false altrimenti
	 */
	public boolean getTabSpedDisabled() {
		boolean output = this.onChangeDestinatarioMezzoSpedizione();
		showMaxContent = output;  
		return output;
	}

	/**
	 * Restituisce il chiamante.
	 * @return chiamante
	 */
	public String getChiamante() {
		return chiamante;
	}

	/**
	 * Imposta il chiamante {@link #chiamante} e verifica se il chiamante è un "Predisponi" (Documento, Risposta, Inoltro, Visto, etc.)
	 * In caso positivo imposta un apposito flag.
	 * @param chiamante
	 */
	public void setChiamante(String chiamante) {
		this.chiamante = chiamante;
		
		
		setChiamanteOneKindOfPredisponi();
	}

	/**
	 * Restituisce il component per la gestione del tab dei contributi.
	 * @return il component
	 */
	public ContributoInternoTabComponent getContributiTab() {
		return contributiTab;
	}

	/**
	 * Imposta il component per la gestione del tab dei contributi.
	 * @param contributiTab
	 */
	public void setContributiTab(ContributoInternoTabComponent contributiTab) {
		this.contributiTab = contributiTab;
	}

	/**
	 * Restituisce l'informativa associata all'iter approvativo.
	 * @return info iter approvativo
	 */
	public String getIterApprovativoInfo() {
		return iterApprovativoInfo;
	}

	/**
	 * Restituisce il component per la gestione del tab trasmissione.
	 * @return il component
	 */
	public DocumentManagerDataTabTrasmissioneComponent getTrasmissioneTab() {
		return trasmissioneTab;
	}

	/**
	 * Imposta il component per la gestione del tab trasmissione.
	 * @param trasmissioneTab
	 */
	public void setTrasmissioneTab(DocumentManagerDataTabTrasmissioneComponent trasmissioneTab) {
		this.trasmissioneTab = trasmissioneTab;
	}

	/**
	 * @return true se la dialog della faldonatura è renderizzata, false altrimenti
	 */
	public boolean isDlgFaldonaturaRendered() {
		return dlgFaldonaturaRendered;
	}

	/**
	 * Restituisce la lista delle Email PEC.
	 * @return lista mail
	 */
	public List<EmailDTO> getListaNotifichePEC() {
		return listaNotifichePEC;
	}

	/**
	 * Imposta la lista delle Email PEC.
	 * @param listaNotifichePEC
	 */
	public void setListaNotifichePEC(List<EmailDTO> listaNotifichePEC) {
		this.listaNotifichePEC = listaNotifichePEC;
	}

	/**
	 * Restituisce il nome report dati di trasmissione.
	 * @return nome report
	 */
	public String getNomeReportDatiDiTrasmissione() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String strDate = sdf.format(detail.getDataProtocollo()); 
		return detail.getNumeroProtocollo()+"_"+strDate;
	}

	/**
	 * Restituisce la lista delle mail TO.
	 * @return lista mail
	 */
	public List<EmailDTO> getListaMailTO() {
		return listaMailTO;
	}

	/**
	 * Imposta la lista delel mail TO.
	 * @param listaMailTO
	 */
	public void setListaMailTO(List<EmailDTO> listaMailTO) {
		this.listaMailTO = listaMailTO;
	}

	/**
	 * Restituisce la lista delle mail CC.
	 * @return lista mail
	 */
	public List<EmailDTO> getListaMailCC() {
		return listaMailCC;
	}

	/**
	 * Imposta la lista delle mail CC.
	 * @param listaMailCC
	 */
	public void setListaMailCC(List<EmailDTO> listaMailCC) {
		this.listaMailCC = listaMailCC;
	}

	/**
	 * Restituisce l'indirizzo mail per la funzionalità Reinvia.
	 * @return indirizzo mail reinvia
	 */
	public String getIndirizzoMailPerReinvia() {
		return indirizzoMailPerReinvia;
	}

	/**
	 * Imposta l'indirizzo mail per la funzionalità Reinvia.
	 * @param indirizzoMailPerReinvia
	 */
	public void setIndirizzoMailPerReinvia(String indirizzoMailPerReinvia) {
		this.indirizzoMailPerReinvia = indirizzoMailPerReinvia;
	}

	/**
	 * Restituisce il component per la gestione degli indici di classificazione.
	 * @return il component
	 */
	public DocumentManagerDataIndiceDiClassificazioneComponent getIndiceDiClassificazione() {
		return indiceDiClassificazione;
	}

	/**
	 * Imposta il component per la gestione degli indici di classificazione.
	 * @param indiceDiClassificazione
	 */
	public void setIndiceDiClassificazione(DocumentManagerDataIndiceDiClassificazioneComponent indiceDiClassificazione) {
		this.indiceDiClassificazione = indiceDiClassificazione;
	}

	/**
	 * Restituisce la lista dei destinatari come stringa concatenando tutti i destinatari contenuti all'interno della lista.
	 * @return destinatari come String
	 */
	public String getListaDestinatariToAsString() {
		List<DestinatarioRedDTO> destinatarioList = getDestinatariInModifica();
		String destinatariValidi = "";
		if (CollectionUtils.isEmpty(getListaMailTO())) {
			destinatariValidi = destinatariValidi(destinatarioList, ModalitaDestinatarioEnum.TO);	
		}  
		return destinatariValidi;
	}

	/**
	 * Restituisce la lista dei destinatari CC come stringa concatenando tutti i destinatari contenuti all'interno della lista.
	 * @return destinatari CC come String
	 */
	public String getListaDestinatariCCAsString() {
		List<DestinatarioRedDTO> destinatarioList = getDestinatariInModifica();
		String destinatariValidi = "";
		if (CollectionUtils.isEmpty(getListaMailCC())) {
			destinatariValidi = destinatariValidi(destinatarioList, ModalitaDestinatarioEnum.CC);	
		}

		return destinatariValidi;
	}

	/**
	 * Costruisce una stringa carina con tutti i destinatari, separati da punto e virgola (';')
	 * 
	 * I destinatari devono essere filtrati per mezzo di spedizione,
	 * Vengono filtrati anche i dati malformati
	 * 
	 * @param destinatarioList
	 * 	Destinatari da filtrare
	 * @param modalitaDestinatario
	 * 	filtra per i destinatari in CC oppure in TO
	 * @return
	 */
	private static String destinatariValidi(List<DestinatarioRedDTO> destinatarioList, ModalitaDestinatarioEnum modalitaDestinatario) {
		List<String> destinatariValidiList =  destinatarioList.stream()
				.filter(Objects::nonNull)
				.filter(d -> d.getMezzoSpedizioneEnum() == MezzoSpedizioneEnum.ELETTRONICO)
				.filter(d -> d.getModalitaDestinatarioEnum() == modalitaDestinatario)
				.filter(d -> d.getContatto() != null)
				.map(DestinatarioRedDTO::getContatto)
//				.filter(c -> org.apache.commons.lang3.StringUtils.isNotBlank(c.getMailSelected()))
				.map(Contatto::getMailSelected)
				.collect(Collectors.toList());

		StringBuilder str = new StringBuilder();
		boolean first = true;
		for (String mailTo : destinatariValidiList) {
			if (!first) {
				str.append(" ");
			} else {
				first = false;
			}
			if(!StringUtils.isNullOrEmpty(mailTo)) {
				str.append(mailTo);
				str.append(";");
			}
		}

		return str.toString();
	}

	/**
	 * Gestisce l'apertura della preview dei file.
	 */
	public void openPreviewFiles() {
		Boolean contentDocPrinc = false;
		Boolean contentAllegati = false;

		String[] siglatarioPrincipaleString = null;
		String[] siglatariAll = null;
		if(documentManagerDTO.isInCreazioneUscita() ||documentManagerDTO.isInModificaUscita()){
			siglatariAll = calcolaFirmatariPerStampigliaturaSigla(siglatarioPrincipaleString, siglatariAll);
		}
		Long siglatarioLong = null;
		if(siglatariAll!=null && siglatariAll.length>0) {
			siglatarioLong = Long.valueOf(siglatariAll[0]);
		}
		Integer idffCreatore = null;
		if(detailPreviousVersion.getIdUfficioCreatore()!=null) {
			idffCreatore = detail.getIdUfficioCreatore();
		} else {
			idffCreatore = utente.getIdUfficio().intValue();
		} 
		viewFilesPreview = new VisualizzaDettaglioFilesComponent(utente, documentManagerDTO.isInModifica(), 
				documentManagerDTO.isInCreazioneIngresso(), TipoAssegnazioneEnum.FIRMA_MULTIPLA.equals(detail.getTipoAssegnazioneEnum()),
				detail.getDaNonProtocollare(),siglatarioLong,idffCreatore,detail.getIdTipologiaDocumento());

		if (documentManagerDTO.isInCreazioneUscita() || documentManagerDTO.isInCreazioneIngresso()) {
			mainDocForSignFieldPreview.setCampoFirma(true);
			viewFilesPreview.impostaDocumentoPrincipale(mainDocForSignFieldPreview);
			viewFilesPreview.impostaDetailAllegati(tabAllegati.getAllegati());
		} else { //per modifica doc in uscita
			try {
				//Documento principale
				mainDocForSignFieldPreview = documentoSRV.getDocumentoContentPreview(utente.getFcDTO(), detail.getDocumentTitle(), false, utente.getIdAoo(), false); 
				mainDocForSignFieldPreview.setUuid(detail.getGuid());
				viewFilesPreview.impostaDocumentoPrincipale(mainDocForSignFieldPreview);
				contentDocPrinc = true;
				//Allegati
				List<AllegatoDTO> allegati = new ArrayList<>();
				List<AllegatoDTO> newAllegatiList = new ArrayList<>(); 
				if (tabAllegati.getAllegati() != null && !tabAllegati.getAllegati().isEmpty()) {
					for (AllegatoDTO alleg : tabAllegati.getAllegati()) {
						if(StringUtils.isNullOrEmpty(alleg.getDocumentTitle()) || FormatoAllegatoEnum.CARTACEO.getId().equals(alleg.getFormato())){
							newAllegatiList.add(alleg);
						} else {
							FileDTO al = documentoSRV.getContentAllegato(alleg.getDocumentTitle(), utente.getFcDTO(), utente.getIdAoo());
							alleg.setContent(al.getContent());
							allegati.add(alleg);
						}
					}
					allegati.addAll(newAllegatiList);
					viewFilesPreview.impostaDetailAllegati(allegati);
				}
			} catch (Exception e) {
				String msgErr = "Impossibile recuperare il contenuto ";
				if (Boolean.FALSE.equals(contentDocPrinc)) {
					msgErr += "del documento principale";
				}
				if (Boolean.FALSE.equals(contentDocPrinc) && Boolean.FALSE.equals(contentAllegati)) {
					msgErr += " e ";
				}
				if (Boolean.FALSE.equals(contentAllegati)) {
					msgErr += "degli allegati";
				}
				showError(msgErr);
				LOGGER.error(msgErr + " (Doc: " + contentDocPrinc + ") (Alleg: " + contentAllegati + ")"  , e);
			}
		}
	}

	private String[] calcolaFirmatariPerStampigliaturaSigla(String[] siglatarioPrincipaleString, String[] siglatariAll) {
		Integer tipoFirma;
		if(ITER_MANUALE_DESC.equalsIgnoreCase(iterApprovativoInfo) && TipoAssegnazioneEnum.FIRMA.equals(detail.getTipoAssegnazioneEnum())) {
			try {
				tipoFirma = stampigliaturaSiglaSRV.getTipoFirma(detail.getIdIterApprovativo());
				List<AssegnazioneDTO> siglatarioPrincipale = new ArrayList<>();
				siglatarioPrincipale.add(assegnatarioPrincipale);
				siglatarioPrincipaleString = DocumentoUtils.initArrayAssegnazioni(detail.getTipoAssegnazioneEnum(), siglatarioPrincipale);
				siglatariAll = stampigliaturaSiglaSRV.getFirmatariStringArray(siglatarioPrincipaleString, utente.getIdUfficio(), tipoFirma);
				if(siglatariAll!=null && siglatariAll.length>0 && "0".equals(siglatariAll[0])) {
					IListaDocumentiFacadeSRV listaDocSRV = ApplicationContextProvider.getApplicationContext().getBean(IListaDocumentiFacadeSRV.class);
					Long idUtenteFirmatario = listaDocSRV.getMetadatoIdUtenteFirmatario(detail.getDocumentTitle(), utente);
					siglatariAll[0]=""+idUtenteFirmatario;
				}
			} catch(Exception ex) {
				LOGGER.error("Nessun firmatario definito per l'allegato da siglare", ex);
			}
		} else if(!ITER_MANUALE_DESC.equalsIgnoreCase(iterApprovativoInfo)) {
			tipoFirma = stampigliaturaSiglaSRV.getTipoFirma(detail.getIdIterApprovativo());
			siglatariAll = stampigliaturaSiglaSRV.getFirmatariStringArray(siglatarioPrincipaleString, utente.getIdUfficio(), tipoFirma);
			 
		}
		return siglatariAll;
	}

	/**
	 * Restituisce  la mail selezionata per il Reinvia TO.
	 * @return selectedMailForReinviaTO
	 */
	public EmailDTO getSelectedMailForReinviaTO() {
		return selectedMailForReinviaTO;
	}

	/**
	 * Restituisce la mail selezionata per il Reinvia CC.
	 * @return selectedMailForReinviaCC
	 */
	public EmailDTO getSelectedMailForReinviaCC() {
		return selectedMailForReinviaCC;
	}

	/**
	 * Restituisce il component per la gestione del tab degli allegati.
	 * @return tabAllegati
	 */
	public TabAllegatiComponent getTabAllegati() {
		return tabAllegati;
	}

	/**
	 * Restituisce il component per la gestione dello storico.
	 * @return storico
	 */
	public DettaglioDocumentoStoricoComponent getStorico() {
		return storico;
	}

	/**
	 * Restituisce l'oggetto che fa riferimento ai Fascicoli per Titolario.
	 * @return fascioli per titolario
	 */
	public FascicoliPerTitolario getFascicoliPerTitolario() {
		return fascicoliPerTitolario;
	}

	/**
	 * Imposta i fascicoli per il titolario come oggetto FascicoliPerTitolario.
	 * @param fascicoliPerTitolario
	 */
	public void setFascicoliPerTitolario(FascicoliPerTitolario fascicoliPerTitolario) {
		this.fascicoliPerTitolario = fascicoliPerTitolario;
	}


	/**
	 * @return the previewComponent
	 */
	public IDetailPreviewComponent getPreviewComponent() {
		return previewComponent;
	}

	/**
	 * Restituisce il dettaglio del documento.
	 * @return doc
	 */
	public DetailDocumentRedDTO getDoc() {
		return doc;
	}

	/**
	 * Imposta il dettaglio del documento.
	 * @param doc
	 */
	public void setDoc(DetailDocumentRedDTO doc) {
		this.doc = doc;
	}

	/**
	 * Restituisce la notifica associata alla selezione del testo.
	 * @return notifica selected Testo
	 */
	public String getNotificaSelectedTesto() {
		return notificaSelectedTesto;
	}

	/**
	 * @param notificaSelectedTesto
	 */
	public void setNotificaSelectedTesto(String notificaSelectedTesto) {
		this.notificaSelectedTesto = notificaSelectedTesto;
	}

	/**
	 * Restituisce la label che costituisce l'header della dialog.
	 * @return dialog header
	 */
	public String getDialogHeader() {
		return dialogHeader;
	}

	/**
	 * Restituisce il component per la gestione degli allegati e le loro versioni.
	 * @return DocumentManagerDettaglioVersioniAllegatiComponent
	 */
	public DocumentManagerDettaglioVersioniAllegatiComponent getVersioniAllegato() {
		return versioniAllegato;
	}

	/**
	 * Restituisce la lista dei Fascicoli recuperati dalla ricerca.
	 * @return lista di fascicoli
	 */
	public List<FascicoloDTO> getRicercaFascicoliResultsFiltered() {
		return ricercaFascicoliResultsFiltered;
	}

	/**
	 * Imposta la lista dei Fascicoli recuperati dalla ricerca.
	 * @param ricercaFascicoliResultsFiltered
	 */
	public void setRicercaFascicoliResultsFiltered(List<FascicoloDTO> ricercaFascicoliResultsFiltered) {
		this.ricercaFascicoliResultsFiltered = ricercaFascicoliResultsFiltered;
	}

	/**
	 * Restituisce il component per la ricerca dei fascicoli.
	 * @return il component
	 */
	public CercaFascicoloComponent getCercaFascicoloComponent() {
		return cercaFascicoloComponent;
	}

	/**
	 * Restituisce il component per la gestione del tab dei contributi esterni.
	 * @return il component
	 */
	public ContributoEsternoTabComponent getContributiEsterniTab() {
		return contributiEsterniTab;
	}

	/**
	 * Restituisce la lista di RdsSiebel.
	 * @return lista rsd
	 */
	public List<RdsSiebelDescrizioneDTO> getRdsList() {
		return rdsList;
	}

	/**
	 * Restituisce la lista degli rdsSiebel selezionati.
	 * @return rds selezionati
	 */
	public RdsSiebelDescrizioneDTO getSelectedRds() {
		return selectedRds;
	}

	/**
	 * Imposta la lista degli rdsSiebel selezionati.
	 * @param selectedRds
	 */
	public void setSelectedRds(RdsSiebelDescrizioneDTO selectedRds) {
		this.selectedRds = selectedRds;
	}

	/**
	 * @return the mainDocForSignFieldPreview
	 */
	public final FileDTO getMainDocForSignFieldPreview() {
		return mainDocForSignFieldPreview;
	}

	/**
	 * @param mainDocForSignFieldPreview the mainDocForSignFieldPreview to set
	 */
	public final void setMainDocForSignFieldPreview(FileDTO mainDocForSignFieldPreview) {
		this.mainDocForSignFieldPreview = mainDocForSignFieldPreview;
	}

	/**
	 * Restituisce il component per la visualizzazione del dettaglio dei file.
	 * @return il component
	 */
	public VisualizzaDettaglioFilesComponent getViewFilesPreview() {
		return viewFilesPreview;
	}

	/**
	 * @param viewFilesPreview
	 */
	public void setViewFilesPreview(VisualizzaDettaglioFilesComponent viewFilesPreview) {
		this.viewFilesPreview = viewFilesPreview;
	}

	/**
	 * Restituisce il component per la gestione delle note dello storico.
	 * @return il component StoricoNote
	 */
	public DocumentManagerDataTabStoricoNoteComponent getStoricoNote() {
		return storicoNote;
	}

	/**
	 * Restituisce il component per la gestione del template del documento in uscita.
	 * @return il component TemplateDocUscita
	 */
	public TemplateDocUscitaComponent getTemplateDocUscitaComponent() {
		return templateDocUscitaComponent;
	}

	/**
	 * Restituisce l'initializer degli allegati da allacci che gestisce l'interazione tra i tab degli allegati e AllegatiDaAllaccio.
	 * @return l'Initializer Allegati da allacci
	 */
	public AllegatiDaAllacciInitializer getAllegatiDaAllacciInitializer() {
		return allegatiDaAllacciInitializer;
	}

	/**
	 * Restituisce true se {@link #utente} ha il permesso PermessiEnum.GESTIONE_RUBRICA, false altrimenti.
	 * @return true se detiene il permesso di gestione rubrica, false altrimenti
	 */
	public boolean isPermessoUtenteGestioneRichiesteRubrica() {
		return PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.GESTIONE_RUBRICA);
	}

	/**
	 * Restituisce true se in modifica da shortcut.
	 * @return true se in modifica da shortcut, false altrimenti
	 */
	public boolean isInModificaDaShortcut() {
		return inModificaDaShortcut;
	}

	/**
	 * Imposta il flag: inModificaDaShortcut.
	 * @param inModificaDaShortcut
	 */
	public void setInModificaDaShortcut(boolean inModificaDaShortcut) {
		this.inModificaDaShortcut = inModificaDaShortcut;
	}

	/**
	 * Restituisce hiddenTable.
	 * @return hiddenTable
	 */
	public boolean getHiddenTable() {
		return hiddenTable;
	}

	/**
	 * Imposta hiddenTable.
	 * @param hiddenTable
	 */
	public void setHiddenTable(boolean hiddenTable) {
		this.hiddenTable = hiddenTable;
	}

	/**
	 * Metodo per inizializzare il tab della shortcut preferiti.
	 */
	public void inizializzaTab() {
		hiddenTable = true;

		TabView tb = (TabView) FacesContext.getCurrentInstance().getViewRoot().findComponent(IDMITTENTEPREFTABSDMD_RESOURCE_NAME);
		tb.setActiveIndex(0);
		DataTable dt = getDataTableByIdComponent("idDettagliEstesiForm:preferitoTabV:idRicRubrica_DMDRC");
		if (dt != null) {
			dt.reset();
		}
		DataTable dtPref = null;
		dtPref = getDataTableByIdComponent(IDPREFERITIMITTENTIDMD_RESOURCE_NAME);
		if (dtPref != null) {
			dtPref.reset();
		}
		 
		inizializzaCheckboxRubrica();
	}

	/**
	 * Rimuove il lock sul documento definito dal parametro documentTitle di {@link #detail}, e associato
	 * a {@link #utente}, sbloccandolo.
	 */
	public void sbloccaDocumento() {
		if(documentManagerDTO.isInModificaIngresso() || documentManagerDTO.isInModificaUscita()) {
			GestioneLockDTO gestioneLockDTO = null;
			gestioneLockDTO = documentoSRV.getLockDocumento(Integer.valueOf(detail.getDocumentTitle()), utente.getIdAoo(), 1, utente.getId());
			Boolean nessunLockPresente = gestioneLockDTO.getNessunLock();
			if(nessunLockPresente!=null && nessunLockPresente) {
				documentoSRV.deleteLockDocumento(Integer.valueOf(detail.getDocumentTitle()), utente.getIdAoo(), 1, utente.getId());
			}
		}
	}

	/**
	 * Restituisce il DTO associato al protocollo emergenza.
	 * @return protocollo emergenza
	 */
	public ProtocolloEmergenzaDocDTO getProtEm() {
		return protEm;
	}

	/**
	 * Imposta il DTO associato al protocollo emergenza.
	 * @param protEm
	 */
	public void setProtEm(ProtocolloEmergenzaDocDTO protEm) {
		this.protEm = protEm;
	}

	/**
	 * Azioni da eseguire in fase di caricamento.
	 */
	public void onload() {
		if (documentManagerDTO.isInCreazioneUscita() || documentManagerDTO.isInModificaUscita()) {

			boolean isRegistroRepertorio = false;
			if(detail.getIdTipologiaDocumento()!= null && detail.getIdTipologiaProcedimento() != null) {
				isRegistroRepertorio = registroRepertorioSRV.checkIsRegistroRepertorio(utente.getIdAoo(), 
						detail.getIdTipologiaDocumento().longValue(), detail.getIdTipologiaProcedimento().longValue(), 
						documentManagerDTO.getRegistriRepertorioConfigured());
			}

			detail.setIsModalitaRegistroRepertorio(isRegistroRepertorio);

			checkDestinatariConformiRR();
		}
	}

	/**
	 * Restituisce l'utente.
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'utente.
	 * @param utente
	 */
	public void setUtente(UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Restituisce la lista delle notifiche InteroperabilitaMail.
	 * @return lista Notifiche Interoperabilita
	 */
	public List<NotificaInteroperabilitaEmailDTO> getListaNotificheInteroperabilita() {
		return listaNotificheInteroperabilita;
	}

	/**
	 * Restituisce il flag: disabilitaButtonProtSemi.
	 * @return disabilitaButtonProtSemi
	 */
	public boolean getDisabilitaButtonProtSemi() {
		return disabilitaButtonProtSemi;
	}

	/**
	 * Imposta il flag: disabilitaButtonProtSemi.
	 * @param disabilitaButtonProtSemi
	 */
	public void setDisabilitaButtonProtSemi(boolean disabilitaButtonProtSemi) {
		this.disabilitaButtonProtSemi = disabilitaButtonProtSemi;
	}

	/**
	 * @return the msgChkRepertorio
	 */
	public String getMsgChkRepertorio() {
		return msgChkRepertorio;
	}

	/**
	 * @param msgChkRepertorio the msgChkRepertorio to set
	 */
	public void setMsgChkRepertorio(String msgChkRepertorio) {
		this.msgChkRepertorio = msgChkRepertorio;
	}

	/**
	 * Reinizializza il contatto associato al {@link #documentManagerDTO} e identificato dal parametro "inserisciContattoItem".
	 */
	public void pulisciUtente() { 
		documentManagerDTO.setInserisciContattoItem(new Contatto()); 
		inModificaDaShortcut=false;  
	}

	/**
	 * Gestisce la visibilita dell'ufficio personale.
	 */
	public void selezionaRubricaCheckBox() { 
		if(Boolean.FALSE.equals(documentManagerDTO.getRicercaRubricaDTO().getRicercaRubricaUfficio()) 
			&& Boolean.FALSE.equals(documentManagerDTO.getRicercaRubricaDTO().getRicercaRED()) 
			&& Boolean.FALSE.equals(documentManagerDTO.getRicercaRubricaDTO().getRicercaIPA())) {
			documentManagerDTO.getRicercaRubricaDTO().setShowUfficioOPersonale(false);
		}
	}

	/**
	 * Restituisce il mittente mail.
	 * @return mittente mail
	 */
	public String getMittenteMailProt() {
		return mittenteMailProt;
	}

	/**
	 * Restituisce l'oggetto del {@link #detail} in maiuscolo.
	 */
	public void upperOggetto() {
		if (detail.getOggetto() != null) {
			detail.setOggetto(detail.getOggetto().toUpperCase());
		}
	}

	/**
	 * Restituisce true se "ribaltaTitolario" deve essere visisile.
	 * @return is RibaltaTitolario Visible
	 */
	public boolean isRibaltaTitolarioVisible() {
		return (Boolean.TRUE.equals(utente.isRibaltaTitolario() && MBean.MAIL_BEAN.equals(chiamante) && detail.getModificabile()) 
				&& documentManagerDTO.isInCreazioneIngresso());
	}

	/**
	 * Effettua il setup del download delle etichette.
	 * @param wobNumber
	 */
	public void getDownloadEtichette(String wobNumber) {
		DetailDocumentRedComponent detailDocRed = new DetailDocumentRedComponent(utente);
		detailDocRed.getDownloadEtichette(wobNumber);
	}

	/**
	 * @return stampaEtichetteButton
	 */
	public boolean getStampaEtichetteButton() {
		return stampaEtichetteButton;
	}

	/**
	 * @param stampaEtichetteButton
	 */
	public void setStampaEtichetteButton(boolean stampaEtichetteButton) {
		this.stampaEtichetteButton = stampaEtichetteButton;
	}

	/**
	 * @return isEstendiVisibilitaChk
	 */
	public boolean getIsEstendiVisibilitaChk() {
		if(documentManagerDTO.isRiservato()) {
			isEstendiVisibilitaChk = false;
		}
		return isEstendiVisibilitaChk;
	}

	/**
	 * Imposta il flag: isEstendiVisibilitaChk.
	 * @param isEstendiVisibilitaChk
	 */
	public void setIsEstendiVisibilitaChk(boolean isEstendiVisibilitaChk) {
		this.isEstendiVisibilitaChk = isEstendiVisibilitaChk;
	}

	/**
	 * Restituisce la lista di Riferimento Storico Nsd.
	 * @return riferimentoStoricoNsd
	 */
	public List<RiferimentoStoricoNsdDTO> getRiferimentoStoricoNsd(){
		return riferimentoStoricoNsd;
	}

	/**
	 * Imposta la lista di Riferimento Storico Nsd.
	 * @param riferimentoStoricoNsd
	 */
	public void setRiferimentoStoricoNsd(List<RiferimentoStoricoNsdDTO> riferimentoStoricoNsd) {
		this.riferimentoStoricoNsd = riferimentoStoricoNsd;
	}

	/**
	 * Esegue una verifica del protocollo di riferimento storico identificato da {@link #riferimentoStoricoNsd} e in base all'esito
	 * ne aggiorna alcuni parametri.
	 * @param index
	 */
	public void verificaProtocolloDiRiferimentoStorico(Integer index) {
		try {
			if (index > this.riferimentoStoricoNsd.size()) {
				throw new RedException(ERROR_INDICE_NON_VALIDO_MSG);
			}
 
			selected = this.riferimentoStoricoNsd.get(index);
			
			if(selected.getNumeroProtocolloNsd()==null || selected.getAnnoProtocolloNsd()==null) {
				showWarnMessage("Valorizzare protocollo NSD");
				return;
			}
			
			if(this.riferimentoStoricoNsd.size()>1) {
				for(int i=0; i<this.riferimentoStoricoNsd.size()-1; i++) {
					if(!this.riferimentoStoricoNsd.get(i).equals(selected) && this.riferimentoStoricoNsd.get(i).getAnnoProtocolloNsd().equals(selected.getAnnoProtocolloNsd()) &&
							this.riferimentoStoricoNsd.get(i).getNumeroProtocolloNsd().equals(selected.getNumeroProtocolloNsd())) {
						showWarnMessage("Attenzione , non è possibile inserire due protocolli uguali");
						return;
					}
				}
			}
			esitoVerificaProtocolloNsdDTO = dfSRV.verificaRiferimentoStorico(utente,selected);  
			DetailDocumentRedDTO d = esitoVerificaProtocolloNsdDTO.getDocument();

			if (esitoVerificaProtocolloNsdDTO.isEsito()) {
				selected.setVerificato(true);
				selected.setOggettoProtocolloNsd(esitoVerificaProtocolloNsdDTO.getDocument().getProtocolloVerificatoNsd().getDescOggetto());
				selected.setIdDocumentoPrincipale(esitoVerificaProtocolloNsdDTO.getDocument().getProtocolloVerificatoNsd().getOriginalDocId());
				selected.setDocument(d); 
			} else {
				selected.setDocument(null);
				selected.setVerificato(false);
				showWarningMaschera(esitoVerificaProtocolloNsdDTO.getNote());
  
 				showWarnMessage(ATTENZIONE_PROTOCOLLO_NON_TROVATO);
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e); 
			showWarnMessage(ATTENZIONE_PROTOCOLLO_NON_TROVATO); 
		}
	}

	/**
	 * Ribalta le informazioni del riferimento storico selezionato.
	 * @param index
	 */
	public void ribaltaRiferimentoStorico(int index) {
		try {
			if (index > this.riferimentoStoricoNsd.size()) {
				throw new RedException(ERROR_INDICE_NON_VALIDO_MSG);
			} 
			  
			selected = this.riferimentoStoricoNsd.get(index);
			
			if(!selected.getVerificato()) {
				
				verificaProtocolloDiRiferimentoStorico(index);
		
				if(selected==null || !esitoVerificaProtocolloNsdDTO.isEsito()) {
					showWarnMessage("Non è possibile ribaltare le informazioni");
				} else {
					ribaltaDocumentoRifStorico(selected);
				}	
			} else { 
				ribaltaDocumentoRifStorico(selected);
			}
		} catch(Exception ex) {
			LOGGER.error(ex.getMessage(), ex); 
		}
		
	}

	private void ribaltaDocumentoRifStorico(RiferimentoStoricoNsdDTO rifStorico) {
		detail.setOggetto(rifStorico.getDocument().getProtocolloVerificatoNsd().getDescOggetto());
		detail.getMailSpedizione().setOggetto(rifStorico.getDocument().getProtocolloVerificatoNsd().getDescOggetto());
		detail.setDescrizioneFascicoloProcedimentale(detail.getDescTipologiaDocumento()+ " - "+rifStorico.getDocument().getProtocolloVerificatoNsd().getDescOggetto());
	}

	/**
	 * Ribalta le informazioni del riferimento storico selezionato.
	 * 
	 * @param index
	 */
	public void ribaltaRiferimentoNps(int index) {
		try {
			if (index > this.riferimentoProtNpsDTO.size()) {
				throw new RedException(ERROR_INDICE_NON_VALIDO_MSG);
			}

			riferimentoProtNpsSelected = this.riferimentoProtNpsDTO.get(index);

			if (!riferimentoProtNpsSelected.isVerificato()) {

				verificaProtocolloDiRiferimentoNps(index);

				if (riferimentoProtNpsSelected == null) {
					showWarnMessage("Non è possibile ribaltare le informazioni");
				} else {
					ribaltaDocumentoRifNps(riferimentoProtNpsSelected);
				}
			} else {
				ribaltaDocumentoRifNps(riferimentoProtNpsSelected);
			}
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}

	}

	private void ribaltaDocumentoRifNps(RiferimentoProtNpsDTO rifNps) {
		detail.setOggetto(rifNps.getOggettoProtocolloNps());
		detail.getMailSpedizione().setOggetto(rifNps.getOggettoProtocolloNps());
		detail.setDescrizioneFascicoloProcedimentale(detail.getDescTipologiaDocumento() + " - " + rifNps.getOggettoProtocolloNps());
	}
	
	/**
	 * Restituisce la lista dei contatti preferiti.
	 * @return lista dei contatti preferiti
	 */
	public List<Contatto> getContattiPreferiti(){
		return contattiPreferiti;
	}

	/**
	 * Imposta la lista dei contatti preferiti.
	 * @param contattiPreferiti
	 */
	public void setContattiPreferiti(List<Contatto> contattiPreferiti) {
		this.contattiPreferiti = contattiPreferiti;
	}

	/**
	 * Restituisce il contatto di verifica del rif. storico.
	 * @return contatto
	 */
	public Contatto getContattoVerificaRifStorico() {
		return contattoVerificaRifStorico;
	}

	/**
	 * Imposta  il contatto di verifica del rif. storico.
	 * @param contattoVerificaRifStorico
	 */
	public void setContattoVerificaRifStorico(Contatto contattoVerificaRifStorico) {
		this.contattoVerificaRifStorico = contattoVerificaRifStorico;
	}

	/**
	 * Aggiunge un nuovo riferimento storico a {@link #riferimentoStoricoNsd}.
	 */
	public void addProtocolloDiRiferimentoStorico() {
		try {
			if (this.riferimentoStoricoNsd == null) {
				riferimentoStoricoNsd = new ArrayList<>();
			}

			RiferimentoStoricoNsdDTO r = new RiferimentoStoricoNsdDTO();
			r.setAnnoProtocolloNsd(this.documentManagerDTO.getMaxAnno());

			this.riferimentoStoricoNsd.add(r);

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Rimuove il riferimento storico Nsd identificato dall'index dalla lista {@link #riferimentoStoricoNsd}.
	 * @param index
	 */
	public void removeProtocolloDiRiferimentoStorico(Object index) {
		try {
			Integer i =  (Integer) index;
			this.riferimentoStoricoNsd.remove(i.intValue());

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

	}

	/**
	 * @return scegliContattoRifStorico
	 */
	public boolean isScegliContattoRifStorico() {
		return scegliContattoRifStorico;
	}

	/**
	 * @param scegliContattoRifStorico
	 */
	public void setScegliContattoRifStorico(boolean scegliContattoRifStorico) {
		this.scegliContattoRifStorico = scegliContattoRifStorico;
	}
	
	/**
	 * @return the onlyCreazioneContattoRifStorico
	 */
	public boolean isOnlyCreazioneContattoRifStorico() {
		return onlyCreazioneContattoRifStorico;
	}

	/**
	 * @param onlyCreazioneContattoRifStorico the onlyCreazioneContattoRifStorico to set
	 */
	public void setOnlyCreazioneContattoRifStorico(boolean onlyCreazioneContattoRifStorico) {
		this.onlyCreazioneContattoRifStorico = onlyCreazioneContattoRifStorico;
	}

	/* ---------- metodi autocomplete ---------- */

	/**
	 * Restituisce la lista dei destinatari caricando la rubrica degli stessi.
	 * @param query
	 * @return lista dei destinatari
	 */
	public List<DestinatarioRedDTO> caricaRubricaDestinatari(String query) {
		
		FacesContext context = FacesContext.getCurrentInstance();
		Integer rowIndex = (Integer) UIComponent.getCurrentComponent(context).getAttributes().get("rowIndex");
		boolean updateTabSpedizione = false;
		setCreaORichiediCreazioneFlag(true);
		if (rowIndex != null && destinatariInModifica.get(rowIndex) != null && destinatariInModifica.get(rowIndex).getContatto() != null) {
			updateTabSpedizione = true;
			if(destinatariInModifica.get(rowIndex).getContatto() != null) {
				campoMailOnTheFly = destinatariInModifica.get(rowIndex).getContatto().getMailSelected();
			}
			
			if(destinatariInModifica.get(rowIndex).getContatto() != null) {
				destinatariInModifica.get(rowIndex).getContatto().setFromDialogCreazioneModifica(false);
			}
			
			destinatariInModifica.set(rowIndex, new DestinatarioRedDTO());
			gestisciContattoOnTheFlyDest(query,rowIndex,campoMailOnTheFly);
		} else {
			gestisciContattoOnTheFlyDest(query,rowIndex,campoMailOnTheFly); 
		}
		
		if(rowIndex != null && destinatariInModifica.get(rowIndex) != null && StringUtils.isNullOrEmpty(query)) {
			destinatariInModifica.get(rowIndex).setMezzoSpedizioneVisible(false);
		}

		List<Contatto> contatti;
		int contattiGiaInseritiNum=0;
		List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		if(!StringUtils.isNullOrEmpty(query) && query.length()>2) {
			if(getDestinatariInModificaPuliti()!=null && !getDestinatariInModificaPuliti().isEmpty()) {
				contattiGiaInseritiNum = getDestinatariInModificaPuliti().size();
			} 
			ArrayList<Long> contattiIdDaEscludere = new ArrayList<>();
			for(int i=0; i<this.destinatariInModifica.size(); i++) {
				if(destinatariInModifica.get(i).getContatto()!=null && destinatariInModifica.get(i).getContatto().getContattoID()!=null) {
					contattiIdDaEscludere.add(destinatariInModifica.get(i).getContatto().getContattoID());
				}
			}

			contatti  = rubricaSRV.ricercaCampoSingolo(query, utente.getIdUfficio(), utente.getIdAoo().intValue(), true, true, true, false, ""+(15+contattiGiaInseritiNum),contattiIdDaEscludere,null,true);

		
			for(Contatto c : contatti) {
				if(Boolean.FALSE.equals(c.getDisabilitaElemento())) {
					DestinatarioRedDTO item = new DestinatarioRedDTO();
					item.setContatto(c);
					item.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
					destinatari.add(item);
				}
			}

			if (updateTabSpedizione) {
				FacesHelper.update(IDTABSPEDIZIONECONTAINERDMD_RESOURCE_NAME);
				FacesHelper.update(IDTABSDMD_RESOURCE_NAME);
			}
		}
		daAggiornare = true;
		aggiungiDestinatario(); 
		return destinatari;
	}
	
	private void gestisciContattoOnTheFlyDest(final String query,final int rowIndex,final String campoMailOnTheFly) {
		Contatto contattoDestOnTheFly = new Contatto();
		contattoDestOnTheFly.setAliasContatto(query);
		contattoDestOnTheFly.setCampoEmailVisible(true);
		
		contattoDestOnTheFly.setMailSelected(campoMailOnTheFly);
		if(destinatariInModifica.get(rowIndex)!=null) {
			destinatariInModifica.get(rowIndex).setContatto(contattoDestOnTheFly);
			destinatariInModifica.get(rowIndex).setMezzoSpedizioneEnum(MezzoSpedizioneEnum.ELETTRONICO);
			destinatariInModifica.get(rowIndex).setMezzoSpedizioneVisible(true);
			destinatariInModifica.get(rowIndex).setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
			destinatariInModifica.get(rowIndex).setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
		}else {
			destinatariInModifica.set(rowIndex, new DestinatarioRedDTO());
		}
	}

	/**
	 * Gestisce la selezione del item contatto.
	 */
	public void manageItemSelect() {
		if(this.detail.getMittenteContatto()==null) {
			setCreaORichiediCreazioneFlag(false);
			return;
		}
		
		if(!StringUtils.isNullOrEmpty(this.detail.getMittenteContatto().getMailPec())) {
			this.detail.getMittenteContatto().setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEC);
			this.detail.getMittenteContatto().setMailSelected(this.detail.getMittenteContatto().getMailPec());
		} else {
			this.detail.getMittenteContatto().setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEO);
			this.detail.getMittenteContatto().setMailSelected(this.detail.getMittenteContatto().getMail());
		}
		this.detail.getMittenteContatto().setOnTheFly(0);
		setCreaORichiediCreazioneFlag(true); 
		
	}

	/**
	 * Gestisce l'evento di modifica della mail mittente.
	 */
	public void onChangeTipoEmailMittente() { 
		setMailPecOPeo(this.detail.getMittenteContatto(), this.detail.getMittenteContatto().getFromDialogCreazioneModifica());
	}

	/**
	 * Gestisce l'evento di modifica della mail destinatario.
	 * @param contattoSelected
	 */
	public void onChangeTipoEmailDestinatari(Contatto contattoSelected) { 
		setMailPecOPeo(contattoSelected, contattoSelected.getFromDialogCreazioneModifica());
	}
	 
	private static void setMailPecOPeo(Contatto contatto, boolean fromCreazioneModifica) {
		if(contatto != null) {
			if(contatto.getContattoID()==null || (contatto.getOnTheFly()!=null && contatto.getOnTheFly()==1)) {
				if(fromCreazioneModifica) {
					if(TipologiaIndirizzoEmailEnum.PEC.equals(contatto.getTipologiaEmail()) && !StringUtils.isNullOrEmpty(contatto.getMailPec())) {
						contatto.setMailSelected(contatto.getMailPec());
					} else if(TipologiaIndirizzoEmailEnum.PEO.equals(contatto.getTipologiaEmail()) && !StringUtils.isNullOrEmpty(contatto.getMail())) {
						contatto.setMailSelected(contatto.getMail());
					} else {
						contatto.setMailSelected(null);
					}
				}else {
					if(!StringUtils.isNullOrEmpty(contatto.getMailSelected()) && TipologiaIndirizzoEmailEnum.PEC.equals(contatto.getTipologiaEmail())){
						contatto.setMailPec(contatto.getMailSelected());
						contatto.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEC);
					} else if(!StringUtils.isNullOrEmpty(contatto.getMailSelected()) && TipologiaIndirizzoEmailEnum.PEO.equals(contatto.getTipologiaEmail())) {
						contatto.setMail(contatto.getMailSelected());
						contatto.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEO);
					}  
				}
			} else {
				if(TipologiaIndirizzoEmailEnum.PEC.equals(contatto.getTipologiaEmail()) && !StringUtils.isNullOrEmpty(contatto.getMailPec())) {
					contatto.setMailSelected(contatto.getMailPec());
				} else if(TipologiaIndirizzoEmailEnum.PEO.equals(contatto.getTipologiaEmail()) && !StringUtils.isNullOrEmpty(contatto.getMail())) {
					contatto.setMailSelected(contatto.getMail());
				} else {
					contatto.setMailSelected(null);
				}
			}
		}
	}

	/**
	 * Restituisce la lista dei contatti della rubrica.
	 * @param query
	 * @return lista dei contatti mittenti
	 */
	public List<Contatto> caricaRubricaMittenti(String query){
		List<Contatto> mittenti = null;
		if(this.detail.getMittenteContatto() != null) {
			campoMailOnTheFly = this.detail.getMittenteContatto().getMailSelected();
		}
		this.detail.setMittenteContattoNew();
		setCreaORichiediCreazioneFlag(true); 
		this.detail.getMittenteContatto().setAliasContatto(query);
		this.detail.getMittenteContatto().setOnTheFly(1); 
		this.detail.getMittenteContatto().setMailSelected(campoMailOnTheFly);
		if(!StringUtils.isNullOrEmpty(query) && query.length()>2) {
			mittenti = rubricaSRV.ricercaCampoSingolo(query, utente.getIdUfficio(), utente.getIdAoo().intValue(), true, true, true, false, "15", null,null, true);
		}
		if(this.detail.getMittenteContatto() != null) {
			this.detail.getMittenteContatto().setFromDialogCreazioneModifica(false);
		}
		return mittenti;
	}
	
	private List<Contatto> matchQuery(List<Contatto> contatti , String query) {
		List<Contatto> lista = new ArrayList<>();
		try {
			if (StringUtils.isNullOrEmpty(query)) {
				return contatti;
			}
			query = query.toUpperCase();
			
			if (!StringUtils.isNullOrEmpty(query) && query.trim().length() >= 3) {
				String[] queryArr = query.trim().toUpperCase().split(" ");
				//filtro contatti inseriti
				List<Contatto> contattiFiltrati = filtraContatti(contatti);
				for (Contatto cont : contattiFiltrati) {
					if (utente.isAutocompleteRubricaCompleta() && (cont.getDisabilitaElemento() == null || Boolean.FALSE.equals(cont.getDisabilitaElemento())) 
						&& (cont.getMail() != null && StringUtils.containsAllWords(cont.getMail().toUpperCase(), queryArr)
							|| cont.getMailPec() != null && StringUtils.containsAllWords(cont.getMailPec().toUpperCase(), queryArr)
							|| cont.getAliasContatto() != null && StringUtils.containsAllWords(cont.getAliasContatto().toUpperCase(), queryArr)
							|| cont.getCognome() != null && StringUtils.containsAllWords(cont.getCognome().toUpperCase(), queryArr)
							|| cont.getNome() != null && StringUtils.containsAllWords(cont.getNome().toUpperCase(), queryArr)))
					{
						LOGGER.info("matchQuery -> add - autocomplete rubrica completa");
						lista.add(cont);
					}
					else if ((cont.getDisabilitaElemento() == null || Boolean.FALSE.equals(cont.getDisabilitaElemento())) 
							&& (
								cont.getAliasContatto() != null && StringUtils.containsAllWords(cont.getAliasContatto().toUpperCase(), queryArr)
								|| cont.getCognome() != null && StringUtils.containsAllWords(cont.getCognome().toUpperCase(), queryArr)
								|| cont.getNome() != null && StringUtils.containsAllWords(cont.getNome().toUpperCase(), queryArr)))
						{
							lista.add(cont);
						}
				}
			}		
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
		return lista;
	}

	private List<Contatto> filtraContatti(List<Contatto> contatti) {
		List<Contatto> contrattiFiltrati = new ArrayList<>();
		if(utente.isAutocompleteRubricaCompleta()) {
			try {
				for (Contatto c : contatti) {
					c.setDisabilitaElemento(false);
					for (DestinatarioRedDTO d : getDestinatariInModificaPuliti()) {
						//nella lista da mostrare bisogna mostrare solo gli utenti e non i gruppi
						//nascondendo gli utenti già inseriti
						if (!TipoRubricaEnum.GRUPPO.equals(d.getContatto().getTipoRubricaEnum())
								&& ((d.getContatto().getContattoID() == null && c.getContattoID() == null)
									|| (d.getContatto().getContattoID() != null && d.getContatto().getContattoID().equals(c.getContattoID())))) {
							c.setDisabilitaElemento(true);
							break;
						}
					}
					contrattiFiltrati.add(c);
				}
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
				showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
			}
		}
		else {
			filtraContattiPreferiti();
			contrattiFiltrati = documentManagerDTO.getContattiPreferitiFiltered();
		}
		return contrattiFiltrati;
		
	}

	/**
	 * Restituisce la lista degli allegati non sbustati.
	 * @return allegati non sbustati
	 */
	public List<AllegatoDTO> getListAllegatiNonSbustati(){
		return listAllegatiNonSbustati;
	}

	/**
	 * Imposta la lista degli allegati non sbustati.
	 * @param listAllegatiNonSbustati
	 */
	public void setListAllegatiNonSbustati(List<AllegatoDTO> listAllegatiNonSbustati) {
		this.listAllegatiNonSbustati = listAllegatiNonSbustati;
	}

	/**
	 * Restituisce lo StreamedContent per il download dell'allegato non sbustato.
	 * @param index
	 * @return StreamedContent per download
	 */
	public StreamedContent downloadAllegatoNonSbustato(Object index) {
		StreamedContent file = null;
		Integer i =  (Integer) index;
		try {
			IDocumentManagerFacadeSRV documentMngrSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentManagerFacadeSRV.class);
			AllegatoDTO allegatoSelezionato = listAllegatiNonSbustati.get(i);
			String mimeType = allegatoSelezionato.getMimeType();
			String nome = allegatoSelezionato.getNomeFile();
			String guid = allegatoSelezionato.getGuid();
			
			byte[] content = allegatoSelezionato.getContent();
					
			InputStream stream = null;
			if (content != null && content.length > 0) {
				stream = new ByteArrayInputStream(content);
			} else if (!StringUtils.isNullOrEmpty(guid)) {
				stream = documentMngrSRV.getStreamDocumentoByGuid(guid, utente);		
			} else {
				throw new RedException("Il content dell'allegato non risulta presente e il guid dell'allegato non è valorizzato.");
			}
			
			file = new DefaultStreamedContent(stream, mimeType, nome);
		} catch (Exception e) {
			LOGGER.error("Errore durante il download del file", e);
			showError("Errore durante il download del file");
		}
		return file;
	}

	/**
	 * Aggiorna la versione degli allegati del documento.
	 * @param versioneVerificaFirma
	 */
	public void aggiornaVerificaFirmaAllegato(VersionDTO versioneVerificaFirma) {
		try {
			String vGuid = versioneVerificaFirma.getGuid();
			//Recupero l'allegato corrispondente che ha la versione selezionata.
			String inDocumentTitleInfoFirma = null;
			if (CollectionUtils.isEmpty(detail.getAllegatiNonSbustati())) {
				RedException red = new RedException("Gli allegati sono null or empty");
				LOGGER.error("dettaglio esteso tab documenti disincronizzato: sono stati modificati gli allegati del documento dopo aver inizializzato il Tab documenti", red);
				throw red;
			}
			
			for (AllegatoDTO currentAllegato : detail.getAllegatiNonSbustati()) {
				
				List<VersionDTO> versioni = currentAllegato.getVersions();
				
				if (CollectionUtils.isEmpty(versioni)) {
					RedException redVersioneNulla = new RedException("le versioni dell'allegato sono null or empty");
					LOGGER.error("dettaglio esteso tab documenti disincronizzato: uno degli allegati del documento non ha la verisone", redVersioneNulla);
					throw redVersioneNulla;
				}
				if (versioniAllegato.containVersionGuid(versioni,vGuid)) {
					inDocumentTitleInfoFirma = currentAllegato.getDocumentTitle();
					break;
				}
			}
			
			versioniAllegato.aggiornaVerificaFirma(versioneVerificaFirma, inDocumentTitleInfoFirma);
			
		} catch (Exception e) {
			LOGGER.error(e);
			showError("Si è verificato un errore durante la verifica della firma dell'allegato."); 
		}
	}

	/**
	 * Restituisce true se il chiamante è predisponi, false altrimenti.
	 * @return true se chiamante predisponi, false altrimenti
	 */
	public boolean isChiamantePredisponi() {
		return chiamantePredisponi;
	}

	/**
	 * Imposta il flag: chiamantePredisponi.
	 * @param chiamantePredisponi
	 */
	public void setChiamantePredisponi(boolean chiamantePredisponi) {
		this.chiamantePredisponi = chiamantePredisponi;
	}

	/**
	 * Restituisce la modalità.
	 * @return mode
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * Imposta la modalità.
	 * @param mode
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}

	/**
	 * Gestisce l'evento di modifica di integrazione dati.
	 */
	public void onChangeIntegrazioneDati() {
		if (utente.isUcb()) {
			renderProtRifField = activeProtRifField(detail.getIdTipologiaDocumento(), detail.getIntegrazioneDati());
		}
	}

	/**
	 * Gestisce il flag {@link #isForIntegrazioneDati}.
	 * @param idTipologiaDocumento
	 * @param isIntegrazioneDati
	 * @return true se per integrazione dati, false altrimenti
	 */
	private boolean activeProtRifField(Integer idTipologiaDocumento, boolean isIntegrazioneDati) {
		boolean output = false;
		try {
			
			String tipoRitiroAutotutela = PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "." + PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_ENTRATA_RITIRO_IN_AUTOTUTELA.getKey());
			
			if (utente.isUcb()) {
				if (isIntegrazioneDati) {
					output = true;
					isForIntegrazioneDati = true;
				} else if (tipoRitiroAutotutela.equals(idTipologiaDocumento.toString())) {
					output = true;
					isForIntegrazioneDati = false;
				} else {
					// Reset del valore per il protocollo di riferimento
					detail.setProtocolloRiferimento(null);
					protRifFieldValue = null;
				}
			}
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, GENERIC_ERROR_MSG + e.getMessage());
		}
		
		return output;
	}

	/**
	 * Esegue la ricerca per protocollo di riferimento.
	 */
	public void ricercaProtRif() {
		risultatiProtRif = new HashMap<>();
		
		if(numProtRicercaRif != null && annoProtRicercaRif != null) {
			risultatiProtRif = fascicoloSRV.getDocsProtRiferimento(utente, numProtRicercaRif, annoProtRicercaRif, isForIntegrazioneDati);
			//crea l'albero per la treetable
			creaTreeTable(risultatiProtRif.keySet());
			//renderizza la tabella
			renderProtRifResult = true; 
		} else {
			showError("Compilare entrambi i campi obbligatori");
		}
		
	}
	
	/**
	 * Il metodo crea l'albero per la treetable dei documenti e fascicoli
	 */
	public void creaTreeTable(Set<FascicoloDTO> fascicoliPadre) {
		rootProtRifResult = new DefaultTreeNode("Root", null);
		MasterDocumentRedDTO d = new MasterDocumentRedDTO();
		d.setOggetto("Documento fittizio");
		d.setDocumentTitle("0");
		// I documenti saranno i figli di primo livello della radice
		for(FascicoloDTO f : fascicoliPadre) {
			DocumentiEFascicoliDTO fasc = new DocumentiEFascicoliDTO(f);
			DefaultTreeNode figlioDiRoot = new DefaultTreeNode(fasc, rootProtRifResult);
			figlioDiRoot.setExpanded(false);
			figlioDiRoot.setSelectable(false);
			figlioDiRoot.setType("fascicolo");
			//figlio fittizio (per permettere l'expand)
			final DefaultTreeNode nodoFittizio = new DefaultTreeNode(f, figlioDiRoot);
			LOGGER.info("nodoFittizio creato di tipo " + nodoFittizio.getType() + " per il fascicolo " + f.getIdFascicolo());
		}
	}
	
	/**
	 * Il metodo popola l'albero con i documenti del fascicolo indicato
	 */
	public void attribuisciDocumentiAlFascicolo(NodeExpandEvent event) {
		TreeNode docNode = event.getTreeNode();
		docNode.getChildren().remove(0);
		
		DocumentiEFascicoliDTO fasc = (DocumentiEFascicoliDTO) docNode.getData();
		
		//recuperiamo i documenti
		List<DocumentoAllegabileDTO> docsDelFascicolo = new ArrayList<>();
		for (Entry<FascicoloDTO, List<DocumentoAllegabileDTO>> fKey : risultatiProtRif.entrySet()) {
			if (fKey.getKey().getIdFascicolo().equals(fasc.getId()) ) {
				docsDelFascicolo = fKey.getValue(); 
				break;
			}
		}
		
		for(DocumentoAllegabileDTO d : docsDelFascicolo) {
			DocumentiEFascicoliDTO docProtRif = new DocumentiEFascicoliDTO(d);
			DefaultTreeNode nodoFascicolo = new DefaultTreeNode(docProtRif, docNode);
			
			nodoFascicolo.setSelectable(true);			
			if (d.isDisabled()) {
				nodoFascicolo.setSelectable(false);
			} 
			
			nodoFascicolo.setExpanded(false);
			nodoFascicolo.setType("documento");
		}
	}
	
	/**
	 * Il metodo seleziona il fascicolo indicato nella scheda Ricerca per Documenti
	 * @param event
	 */
	public void selectDocumentoDelFascicolo(NodeSelectEvent event) {
		try {
			
			DocumentiEFascicoliDTO documento = (DocumentiEFascicoliDTO) event.getTreeNode().getData();
			
			// Set del protocollo da mostrare in maschera.
			protRifFieldValue = documento.getNumeroProtocollo() + PATH_DELIMITER + documento.getAnnoProtocollo();
			
			// Set del document title all'interno della struttura che verrà salvata su FN
			detail.setProtocolloRiferimento(documento.getDocumentTitle());
			renderProtRifResult = false; 
			
				
			// Recupero assegnatario Competenza del protocollo selezionato.
			AssegnazioneDTO assCompetenza = assAutomaticaSRV.getAssegnazioneCompetenza(documento.getDocumentTitle(), utente);
			NodoOrganigrammaDTO nodoOrg = popolaNodoOrganigramma(assCompetenza);
			
			// Valorizzazione dei campi che riportano l'assegnatario per competenza in maschera.
			onOrgAssPerCompetenza(nodoOrg);
			
			// Disabilito il pulsante per la selezione di un'altro assegnatario.
			this.documentManagerDTO.setAssegnazioneBtnDisabled(true);
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, "Si è verificato un errore durante l'operazione: " + e.getMessage());
		}
	}

	private NodoOrganigrammaDTO popolaNodoOrganigramma(AssegnazioneDTO assCompetenza) {
		NodoOrganigrammaDTO output = new NodoOrganigrammaDTO();
		
		try {
			
			// Ufficio
			output.setIdNodo(assCompetenza.getUfficio().getId());
			output.setDescrizioneNodo(assCompetenza.getUfficio().getDescrizione());

			// Utente
			output.setIdUtente(assCompetenza.getUtente().getId());
			output.setNomeUtente(assCompetenza.getUtente().getNome());
			output.setCognomeUtente(assCompetenza.getUtente().getCognome());
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, "Si è verificato un errore durante l'operazione: " + e.getMessage());
		}
		
		return output;
	}

	/**
	 * @return the rootProtRifResult
	 */
	public TreeNode getRootProtRifResult() {
		return rootProtRifResult;
	}

	/**
	 * @param rootProtRifResult the rootProtRifResult to set
	 */
	public void setRootProtRifResult(TreeNode rootProtRifResult) {
		this.rootProtRifResult = rootProtRifResult;
	}

	/**
	 * @return the numProtRicercaRif
	 */
	public Integer getNumProtRicercaRif() {
		return numProtRicercaRif;
	}

	/**
	 * @param numProtRicercaRif the numProtRicercaRif to set
	 */
	public void setNumProtRicercaRif(Integer numProtRicercaRif) {
		this.numProtRicercaRif = numProtRicercaRif;
	}

	/**
	 * @return the annoProtRicercaRif
	 */
	public Integer getAnnoProtRicercaRif() {
		return annoProtRicercaRif;
	}

	/**
	 * @param annoProtRicercaRif the annoProtRicercaRif to set
	 */
	public void setAnnoProtRicercaRif(Integer annoProtRicercaRif) {
		this.annoProtRicercaRif = annoProtRicercaRif;
	}

	/**
	 * @return the renderProtRifResult
	 */
	public boolean isRenderProtRifResult() {
		return renderProtRifResult;
	}

	/**
	 * @param renderProtRifResult the renderProtRifResult to set
	 */
	public void setRenderProtRifResult(boolean renderProtRifResult) {
		this.renderProtRifResult = renderProtRifResult;
	}

	/**
	 * @return the renderProtRifField
	 */
	public boolean isRenderProtRifField() {
		return renderProtRifField;
	}

	/**
	 * @param renderProtRifField the renderProtRifField to set
	 */
	public void setRenderProtRifField(boolean renderProtRifField) {
		this.renderProtRifField = renderProtRifField;
	}

	/**
	 * @return the protRifFieldValue
	 */
	public String getProtRifFieldValue() {
		return protRifFieldValue;
	}

	/**
	 * @param protRifFieldValue the protRifFieldValue to set
	 */
	public void setProtRifFieldValue(String protRifFieldValue) {
		this.protRifFieldValue = protRifFieldValue;
	}

	/**
	 * Restituisce la descrizione dell'assegnatario definito da un eventuale automatismo esistente.
	 * @see AssegnazioneAutomaticaSRV.
	 * @return descrizione assegnatario costituito da utente - ufficio.
	 */
	public String getDescrAssegnatarioDaAutomatismo() {
		return descrAssegnatarioDaAutomatismo;
	}

	/**
	 * Imposta la descrizione dell'assegnatario definito da un automatismo per recuperarlo in fase di comunicazione
	 * attraverso la dialog degli esiti posta a valle del tentativo di registrazione.
	 * @param descrAssegnatarioDaAutomatismo
	 */
	public void setDescrAssegnatarioDaAutomatismo(String descrAssegnatarioDaAutomatismo) {
		this.descrAssegnatarioDaAutomatismo = descrAssegnatarioDaAutomatismo;
	}

	/**
	 * Restituisce l'assegnatario di default impostato dall'Area Organizzativa Omogenea.
	 * Viene utilizzato per popolare l'assegnatario per competenza qualora venga utilizzata la funzionalità: Imposta assegnatario di default.
	 * @return assegnatario per competenza di default per l'AOO
	 */
	public AssegnazioneDTO getAssegnatarioDefault() {
		return assegnatarioDefault;
	}

	/**
	 * Imposta l'assegnatario di default per l'Area Organizzativa Omogenea.
	 * @param assegnatarioDefault
	 */
	public void setAssegnatarioDefault(AssegnazioneDTO assegnatarioDefault) {
		this.assegnatarioDefault = assegnatarioDefault;
	}

	/**
	 * Restituisce l'assegnatario per competenza definito da un eventuale automatismo.
	 * Questo parametro è null qualora non esistesse alcun automatismo associato alla configurazione dei parametri per la creazione del documento.
	 * @return assegnatario per competenza
	 */
	public AssegnazioneDTO getAssegnatarioDaAutomatismo() {
		return assegnatarioDaAutomatismo;
	}

	/**
	 * Imposta l'assegnatario per competenza definito da un eventuale automatismo.
	 * @param assegnatarioDaAutomatismo
	 */
	public void setAssegnatarioDaAutomatismo(AssegnazioneDTO assegnatarioDaAutomatismo) {
		this.assegnatarioDaAutomatismo = assegnatarioDaAutomatismo;
	}

	/**
	 * @return the msgPredisponi
	 */
	public String getMsgPredisponi() {
		return msgPredisponi;
	}

	/**
	 * @param msgPredisponi the msgPredisponi to set
	 */
	public void setMsgPredisponi(String msgPredisponi) {
		this.msgPredisponi = msgPredisponi;
	}
	
	/**
	 * see @FascicoloManagerBean
	 */
	public void apriInserisciDocumento(FascicoloDTO fasc) {
		try {
			nuovoDocumentoInterno = new DetailDocumentRedDTO();
			nuovoDocumentoInterno.setIdCategoriaDocumento(CategoriaDocumentoEnum.INTERNO.getIds()[0]);
			
			setDetailFasc(detailFascicoloSRV.getFascicolo(fasc, utente));
			
			if (comboTipologiaDocumento == null) {
				comboTipologiaDocumento = tipologiaDocumentoSRV.getComboTipologieByAooAndTipoCategoriaAttivi(TipoCategoriaEnum.USCITA, utente.getIdAoo());
			}
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore nell'operazione di apertura maschera");
		}
	}
	
	/**
	 * see @FascicoloManagerBean
	 */
	public void inserisciDocumento() {
		try {
			
			if (StringUtils.isNullOrEmpty(nuovoDocumentoInterno.getOggetto())
					|| StringUtils.isNullOrEmpty(nuovoDocumentoInterno.getNomeFile())) {
				showWarnMessage("Inserire tutti i campi obbligatori");
				return;
			}
			
			SalvaDocumentoRedParametriDTO salvaDoc = new SalvaDocumentoRedParametriDTO();
			salvaDoc.setModalita(Constants.Modalita.MODALITA_INS);
			salvaDoc.setContentVariato(true);
			salvaDoc.setIdFascicoloSelezionato(detailFasc.getNomeFascicolo());
			
			EsitoSalvaDocumentoDTO e = salvaDocumentoSRV.salvaDocumento(nuovoDocumentoInterno, salvaDoc, utente, ProvenienzaSalvaDocumentoEnum.GUI_RED_FASCICOLO_SENZA_TRASFORMAZIONE);
						
			if (e.isEsitoOk()) {
				Integer idFascicolo = Integer.valueOf(detailFasc.getNomeFascicolo());
				Long idAOO = Long.valueOf(detailFasc.getIdAOO());
				detailFasc.setDocumenti(detailFascicoloSRV.getDocumentiByIdFascicolo(idFascicolo, idAOO, utente));
				
				showInfoMessage("Inserimento avvenuto con successo");
				FacesHelper.update(IDDETTAGLIODOCUMENTO_RESOURCE_LOCATION);
			} else {
				StringBuilder sb = new StringBuilder("<ul>");
				for (Enum<?> err : e.getErrori()) {
					sb.append("<li>");
					sb.append(err.name());
					sb.append("</li>");
				}
				sb.append("</ul>");
				showError(sb.toString());
			}
			nuovoDocumentoInterno = new DetailDocumentRedDTO();
			FacesHelper.executeJS("PF('wdgInserisciDocDaDettaglio').hide()");
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore nell'inserimento del documento.");
		}
	}
	
	/**
	 * see @FascicoloManagerBean
	 */
	public StreamedContent downloadZipAllDocument(FascicoloDTO fasc) {
		StreamedContent file = null;
		try {
			Integer idFascicolo = Integer.valueOf(fasc.getIdFascicolo());
			Collection <DocumentoFascicoloDTO> documenti = fascicoloSRV.getDocumentiFascicolo(idFascicolo, utente);
			detailFasc = detailFascicoloSRV.getDetailFascicolo(idFascicolo, new ArrayList<>(documenti), utente);
			
			InputStream inputStream = fascicoloSRV.getZipFascicoloContents(detailFasc.getNomeFascicolo(), utente);
			Calendar rightNow =Calendar.getInstance();
			String giornoMeseAnnoOraminutisecondi = DateUtils.dateToString(rightNow.getTime(), "ddMMyyyy_HHmmss");
			String nome = org.apache.commons.lang3.StringUtils.join("Fascicolo-", detailFasc.getNomeFascicolo(), "_", detailFasc.getAnno(), "_", giornoMeseAnnoOraminutisecondi, ".zip");
			file = new DefaultStreamedContent(inputStream, Constants.ContentType.ZIP_MIME, nome);
		} catch (Exception e) {
			LOGGER.error("Errore durante lo scaricamento dello zip dei documenti",e);
			showError("Errore durante lo scaricamento dei documenti");
		}
		return file;
	}
	
	/**
	 * see @FascicoloManagerBean
	 */
	public void handleDocInternoFileUpload(FileUploadEvent event) {
		try {
			event = checkFileName(event);
			if(event == null) {
				return;
			}
			docInterno = event.getFile();

			String fileName = docInterno.getFileName();
			this.nuovoDocumentoInterno.setNomeFile(fileName);
			this.nuovoDocumentoInterno.setMimeType(docInterno.getContentType());
			this.nuovoDocumentoInterno.setContent(docInterno.getContents());
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_MSG + e.getMessage());
		}
    }
	
	/**
	 * see @FascicoloManagerBean
	 */
	public void rimuoviFileCaricato() {
		docInterno = null;
		this.nuovoDocumentoInterno.setNomeFile(null);
		this.nuovoDocumentoInterno.setMimeType(null);
		this.nuovoDocumentoInterno.setContent(null);
	}
	
	/**
	 * see @FascicoloManagerBean
	 */
	public void eliminaDocInterno(Object index) {
		try {
			Integer i = (Integer) index;
			DocumentoFascicoloDTO docDaEliminare = detailFasc.getDocumenti().get(i); 
			EsitoOperazioneDTO esito = documentoRedSRV.eliminaDocumentoInterno(docDaEliminare.getDocumentTitle(), Integer.parseInt(detailFasc.getNomeFascicolo()), utente);
			if (esito.isEsito()) {
				detailFasc.getDocumenti().remove(i.intValue());
				showInfoMessage("Documento eliminato correttamente.");
				FacesHelper.update(IDDETTAGLIODOCUMENTO_RESOURCE_LOCATION);
			}
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di eliminazione del documento interno ");
		}
	}

	/**
	 * Restituisce il dettaglio del nuovo documento.
	 * @return dettaglio nuovo DocumentoInterno
	 */
	public DetailDocumentRedDTO getNuovoDocumentoInterno() {
		return nuovoDocumentoInterno;
	}

	/**
	 * Imposta il dettaglio del nuovo documento.
	 * @param nuovoDocumentoInterno
	 */
	public void setNuovoDocumentoInterno(DetailDocumentRedDTO nuovoDocumentoInterno) {
		this.nuovoDocumentoInterno = nuovoDocumentoInterno;
	}

	/**
	 * Restituisce il file del documento principale.
	 * @return file docPrincipale
	 */
	public FileDTO getDocPrincipale() {
		return docPrincipale;
	}

	/**
	 * Imposta il file del documento principale.
	 * @param docPrincipale
	 */
	public void setDocPrincipale(FileDTO docPrincipale) {
		this.docPrincipale = docPrincipale;
	}

	/**
	 * Restituisce il riferimento storico Nsd selezionato.
	 * @return riferimento storico selezionato
	 */
	public RiferimentoStoricoNsdDTO getSelected() {
		return selected;
	}

	/**
	 * Imposta il riferimento storico Nsd selezionato.
	 * @param selected
	 */
	public void setSelected(RiferimentoStoricoNsdDTO selected) {
		this.selected = selected;
	}

	/**
	 * Restituisce il documento interno come UploadedFile.
	 * @return UploadedFile
	 */
	public UploadedFile getDocInterno() {
		return docInterno;
	}

	/**
	 * Imposta il documento interno come UploadedFile.
	 * @param docInterno
	 */
	public void setDocInterno(UploadedFile docInterno) {
		this.docInterno = docInterno;
	}

	/**
	 * Restituisce il dettaglio del fascicolo.
	 * @return dettaglio fascicolo
	 */
	public DetailFascicoloRedDTO getDetailFasc() {
		return detailFasc;
	}

	/**
	 * Imposta il dettaglio del fascicolo.
	 * @param detailFasc
	 */
	public void setDetailFasc(DetailFascicoloRedDTO detailFasc) {
		this.detailFasc = detailFasc;
	}

	/**
	 * Restituisce la lista dei tipi documento ancora validi per l'AOO per il popolamento della combobox.
	 * @return lista di tipologie documento
	 */
	public List<TipologiaDocumentoDTO> getComboTipologiaDocumento() {
		return comboTipologiaDocumento;
	}

	/**
	 * Imposta la lista dei tipi documento ancora validi per l'AOO per il popolamento della combobox.
	 * @param comboTipologiaDocumento
	 */
	public void setComboTipologiaDocumento(List<TipologiaDocumentoDTO> comboTipologiaDocumento) {
		this.comboTipologiaDocumento = comboTipologiaDocumento;
	}

	/**
	 * Restituisce true se il documento è elettronico via sistema ausiliario NPS.
	 * 
	 * @return elettronicoViaSistemaAusiliarioNPS
	 */
	public boolean isElettronicoViaSistemaAusiliarioNPS() {
		return elettronicoViaSistemaAusiliarioNPS;
	}

	/**
	 * Imposta il flag elettronicoViaSistemaAusiliarioNPS.
	 * 
	 * @param elettronicoViaSistemaAusiliarioNPS
	 */
	public void setElettronicoViaSistemaAusiliarioNPS(final boolean elettronicoViaSistemaAusiliarioNPS) {
		this.elettronicoViaSistemaAusiliarioNPS = elettronicoViaSistemaAusiliarioNPS;
	}
	
	/**
	 * Restituisce il sistema ausiliario NPS.
	 * 
	 * @return the sistemaAusiliarioNPS
	 */
	public String getSistemaAusiliarioNPS() {
		return sistemaAusiliarioNPS;
	}

	/**
	 * Imposta il sistema ausiliario NPS.
	 * 
	 * @param sistemaAusiliarioNPS the sistemaAusiliarioNPS to set
	 */
	public void setSistemaAusiliarioNPS(String sistemaAusiliarioNPS) {
		this.sistemaAusiliarioNPS = sistemaAusiliarioNPS;
	}

	/**
	 * Gestisce la dialog dei prefiriti e l'apertura della stessa.
	 * @param index
	 */
	public void manageDialogPreferitiInternoEsterno(Integer index) {
		if(index == null) {
			showError("Errore nella selezione dell'elemento");
		}
		
		DestinatarioRedDTO destinatarioSelected = this.destinatariInModifica.get(index);
		
		if(destinatarioSelected != null) {
			boolean isDestInterno = destinatarioSelected.getTipologiaDestinatarioEnum() != null && TipologiaDestinatarioEnum.INTERNO.equals(destinatarioSelected.getTipologiaDestinatarioEnum());
			boolean isDestEsterno = destinatarioSelected.getTipologiaDestinatarioEnum() != null && TipologiaDestinatarioEnum.ESTERNO.equals(destinatarioSelected.getTipologiaDestinatarioEnum());
			if(isDestInterno || isDestEsterno) {
 				openRubricaPreferiti(isDestInterno,isDestEsterno);
			}
		}
	}

	/**
	 * Rimuove tutti i destinatari dalla tabella.
	 */
	public void pulisciTabellaDestinatari() {
		if(!it.ibm.red.business.utils.CollectionUtils.isEmptyOrNull(this.destinatariInModifica)) {
			this.destinatariInModifica = new ArrayList<>();
			this.destinatariInModifica.add(new DestinatarioRedDTO());
		}
		updateMaxSizeValue();
	}

	/**
	 * Popola {@link #contattiNelGruppo} aggiungendo i contatti del gruppo selezionato.
	 * @param gruppoSelected
	 */
	public void ricercaContattiNelGruppo(Contatto gruppoSelected) {
		contattiNelGruppo = RubricaComponent.ricercaContattiNelGruppo(gruppoSelected,utente.getIdUfficio());
	}

	/**
	 * Aggiunge ai destinatari i contatti appartenenti al gruppo selezionato e passato come parametro.
	 * @param gruppoSelected
	 */
	public void addContattiGruppo(Contatto gruppoSelected) {
		FacesContext context = FacesContext.getCurrentInstance();
		this.destinatariInModifica = RubricaComponent.addContattiGruppo(true,gruppoSelected,utente.getIdUfficio(),this.destinatariInModifica,context);
		
		aggiungiDestinatario(); 
		if(StringUtils.isNullOrEmpty(this.detail.getMailSpedizione().getOggetto())) {
			for(DestinatarioRedDTO dest : this.destinatariInModifica) {
				if(MezzoSpedizioneEnum.ELETTRONICO.equals(dest.getMezzoSpedizioneEnum()) && !StringUtils.isNullOrEmpty(this.detail.getOggetto()) ) {
					this.detail.getMailSpedizione().setOggetto(this.detail.getOggetto());
					break;
				}
			}
		}
		
	}

	/**
	 * @param contattoSelected
	 */
	public void deleteGruppoDaSingoloContatto(Contatto contattoSelected) {
		if(contattoSelected == null) {
			showError("Errore Generico");
			return ;
		}
		
		this.destinatariInModifica = RubricaComponent.deleteGruppoDaSingoloContatto(contattoSelected,this.destinatariInModifica);
		
		updateMaxSizeValue();
	}

	/**
	 * Restituisce il contatto associato al vecchio item.
	 * @return contatto
	 */
	public Contatto getContattoVecchioItem() {
		return contattoVecchioItem;
	}

	/**
	 * Imposta il contatto associato al vecchio item.
	 * @param contattoVecchioItem
	 */
	public void setContattoVecchioItem(Contatto contattoVecchioItem) {
		this.contattoVecchioItem = contattoVecchioItem;
	}

	/**
	 * Restituisce la lista dei contatti nel gruppo.
	 * @return contatti nel gruppo
	 */
	public List<Contatto> getContattiNelGruppo(){
		return contattiNelGruppo;
	}

	/**
	 * Imposta la lista dei contatti nel gruppo.
	 * @param contattiNelGruppo
	 */
	public void setContattiNelGruppo(List<Contatto> contattiNelGruppo) {
		this.contattiNelGruppo = contattiNelGruppo;
	}

	/**
	 * Restituisce la radice del tree.
	 * @return root
	 */
	public DefaultTreeNode getRoot() {
		return root;
	}

	/**
	 * Imposta la radice del tree.
	 * @param root
	 */
	public void setRoot(DefaultTreeNode root) {
		this.root = root;
	}

	/**
	 * Mostra l'alberatura di un contatto IPA selezionato dal datatable.
	 * @param event
	 */
	public void showAlberaturaContattoIPA(final ActionEvent event) {
		
		Contatto contSel = getDataTableClickedRow(event); 
		if (TipoRubricaEnum.IPA.equals(contSel.getTipoRubricaEnum())) {
			rubricaSRV.getContattiChildIPA(contSel);
			setRoot(new DefaultTreeNode("Root", null));
			getRoot().setSelectable(true);
			//Popola l'albero dal nodo corrente fino alla root
			getRecursiveNode (contSel);
			 
		}  
	}

	/**
	 * Restituisce il nodo padre associato al nodo del contatto IPA.
	 * @param contattoIPA
	 * @return nodo padre che diventa corrente
	 */
	private TreeNode getRecursiveNode(Contatto contattoIPA) {
		Contatto contattoPadre = rubricaSRV.getPadreContattoIPA(contattoIPA);
		 
		TreeNode nodoCorrente = null;
		if(contattoPadre==null) {
			nodoCorrente= new DefaultTreeNode(contattoIPA, getRoot());
			nodoCorrente.setExpanded(true);
			nodoCorrente.setSelectable(true);
			nodoCorrente.setType(contattoIPA.getTipoPersona());
		}else {
			TreeNode nodoPadre = getRecursiveNode (contattoPadre);
		    nodoCorrente = new DefaultTreeNode(contattoIPA, nodoPadre);
			nodoCorrente.setExpanded(true);
			nodoCorrente.setSelectable(true);
			nodoCorrente.setType(contattoIPA.getTipoPersona());
			
		}
		
		return nodoCorrente;
	}

	/**
	 * Gestisce la richiesta di creazione contatto on the fly per un destinatario.
	 * @param index
	 */
	public void richiediCreazioneContOnTheFlyDestinatario(Integer index) {
		if(index == null) {
			showError("Errore nella richiesta di creazione del contatto");
			return;
		}
		
		indiceColonnaModificata = index;
		filtraContattiPreferiti();

		DestinatarioRedDTO destinatarioSelected = this.destinatariInModifica.get(index);
		fromRichiediCreazioneDatatable = index;
		
		if(destinatarioSelected != null && destinatarioSelected.getContatto()!=null) { 
			if(destinatarioSelected.getContatto().getOnTheFly()!=null && destinatarioSelected.getContatto().getOnTheFly()==1 ) {
				if(destinatarioSelected.getContatto().getTipologiaEmail()!=null && TipologiaIndirizzoEmailEnum.PEC.equals(destinatarioSelected.getContatto().getTipologiaEmail())) {
					destinatarioSelected.getContatto().setMail(null);
					if(!StringUtils.isNullOrEmpty(destinatarioSelected.getContatto().getMailSelected())) {
						destinatarioSelected.getContatto().setMailPec(destinatarioSelected.getContatto().getMailSelected());
					}
				} else if(destinatarioSelected.getContatto().getTipologiaEmail()!=null && TipologiaIndirizzoEmailEnum.PEO.equals(destinatarioSelected.getContatto().getTipologiaEmail())){
					destinatarioSelected.getContatto().setMailPec(null);
					if(!StringUtils.isNullOrEmpty(destinatarioSelected.getContatto().getMailSelected())) {
						destinatarioSelected.getContatto().setMail(destinatarioSelected.getContatto().getMailSelected());
					}
				}
			}
			inModificaDaShortcut=false;
			if(destinatarioSelected.getContatto().getTipoRubrica()!=null && "RED".equals(destinatarioSelected.getContatto().getTipoRubrica())
					&& destinatarioSelected.getContatto().getContattoID()!=null) {
				inModificaDaShortcut=true;	
			} 
			if(destinatarioSelected.getContatto().getOnTheFly()!=null && destinatarioSelected.getContatto().getOnTheFly()==1) {
				inModificaDaShortcut=false;	
				if(destinatarioSelected.getContatto().getIdGruppoSelected()!=null) {
					destinatarioSelected.getContatto().setIdGruppoSelected(null);
				}
				if(!StringUtils.isNullOrEmpty(destinatarioSelected.getContatto().getNomeGruppoSelected())) {
					destinatarioSelected.getContatto().setNomeGruppoSelected("");
				}
			}
			
			documentManagerDTO.setInserisciContattoItem(destinatarioSelected.getContatto());
		} else if(destinatarioSelected == null) {
			Contatto contattoNuovo = new Contatto();
			richiediCreazioneContOnTheFly(contattoNuovo, false);
		}
 
		TabView tb = (TabView) FacesContext.getCurrentInstance().getViewRoot().findComponent(PREFERITOTABV_RESOURCE_NAME);
		tb.setActiveIndex(3); 

	}

	/**
	 * Gestisce la richiesta di creazione contatto on the fly per un mittente.
	 */
	public void richiediCreazioneContOnTheFlyMittente() { 
		documentManagerDTO.setInserisciContattoItem(new Contatto());
		if(detail.getMittenteContatto()!=null) {
			inModificaDaShortcut = false;
			if(detail.getMittenteContatto().getContattoID()!=null) {
				inModificaDaShortcut = true;
			}  
			if(detail.getMittenteContatto().getOnTheFly()!=null && detail.getMittenteContatto().getOnTheFly()==1) {
				inModificaDaShortcut = false;
			}
			richiediCreazioneContOnTheFly(detail.getMittenteContatto(), detail.getMittenteContatto().getFromDialogCreazioneModifica());
		}
 
		TabView tb= (TabView) FacesContext.getCurrentInstance().getViewRoot().findComponent(IDMITTENTEPREFTABSDMD_RESOURCE_NAME);
		tb.setActiveIndex(2);
		FacesHelper.update(IDMITTENTEPREFTABSDMD_RESOURCE_NAME);
	}

	private void richiediCreazioneContOnTheFly(Contatto contattoSelected, boolean fromCreazioneModifica) {
		if(contattoSelected.getTipoRubrica()!=null && !"RED".equals(contattoSelected.getTipoRubrica())) {
			inModificaDaShortcut=false;
		}
		if(contattoSelected.getOnTheFly()!=null && contattoSelected.getOnTheFly()==1 && !fromCreazioneModifica) {
			if(!StringUtils.isNullOrEmpty(contattoSelected.getMailSelected()) &&  TipologiaIndirizzoEmailEnum.PEC.equals(contattoSelected.getTipologiaEmail())){
				contattoSelected.setMail(null);
				contattoSelected.setMailPec(contattoSelected.getMailSelected());
			} else if(!StringUtils.isNullOrEmpty(contattoSelected.getMailSelected()) && TipologiaIndirizzoEmailEnum.PEO.equals(contattoSelected.getTipologiaEmail())){
				contattoSelected.setMailPec(null);
				contattoSelected.setMail(contattoSelected.getMailSelected());
			}
		}
		documentManagerDTO.setInserisciContattoItem(contattoSelected);
	}

	/**
	 * @return creaORichiediCreazioneFlag
	 */
	public boolean getCreaORichiediCreazioneFlag() {
		return creaORichiediCreazioneFlag;
	}

	/**
	 * @param creaORichiediCreazioneFlag
	 */
	public void setCreaORichiediCreazioneFlag(boolean creaORichiediCreazioneFlag) {
		this.creaORichiediCreazioneFlag = creaORichiediCreazioneFlag;
	}

	/**
	 * @return gestioneOnTheFlyDaProtocolla
	 */
	public boolean getGestioneOnTheFlyDaProtocolla() {
		return gestioneOnTheFlyDaProtocolla;
	}

	/**
	 * @param gestioneOnTheFlyDaProtocolla
	 */
	public void setGestioneOnTheFlyDaProtocolla(boolean gestioneOnTheFlyDaProtocolla) {
		this.gestioneOnTheFlyDaProtocolla = gestioneOnTheFlyDaProtocolla;
	}

	/**
	 * Restituisce la nota associata alla richiesta di creazione.
	 * @return nota
	 */
	public String getNotaRichiestaCreazione() {
		return notaRichiestaCreazione;
	}

	/**
	 * Imposta la nota associata alla richiesta di creazione.
	 * @param notaRichiestaCreazione
	 */
	public void setNotaRichiestaCreazione(String notaRichiestaCreazione) {
		this.notaRichiestaCreazione = notaRichiestaCreazione;
	}

	/**
	 * Restituisce la nota associata alla richiesta di eliminazione.
	 * @return nota
	 */
	public String getNotaRichiestaEliminazione() {
		return notaRichiestaEliminazione;
	}

	/**
	 * Imposta la nota associata alla richiesta di eliminazione.
	 * @param notaRichiestaEliminazione
	 */
	public void setNotaRichiestaEliminazione(String notaRichiestaEliminazione) {
		this.notaRichiestaEliminazione = notaRichiestaEliminazione;
	}

	/**
	 * Restituisce la nota associata alla richiesta di modifica.
	 * @return nota
	 */
	public String getNotaRichiestaModifica() {
		return notaRichiestaModifica;
	}

	/**
	 * Imposta la nota associata alla richiesta di modifica.
	 * @param notaRichiestaModifica
	 */
	public void setNotaRichiestaModifica(String notaRichiestaModifica) {
		this.notaRichiestaModifica = notaRichiestaModifica;
	}

	/**
	 * Restituisce il contatto in eliminazione.
	 * @return contatto
	 */
	public Contatto getEliminaContattoItem() {
		return eliminaContattoItem;
	}

	/**
	 * Imposta il contatto in eliminazione.
	 * @param eliminaContattoItem
	 */
	public void setEliminaContattoItem(Contatto eliminaContattoItem) {
		  this.eliminaContattoItem = eliminaContattoItem;
	}

	/**
	 * Restituisce fromRichiediCreazioneDatatable.
	 * @return fromRichiediCreazioneDatatable
	 */
	public int getFromRichiediCreazioneDatatable() {
		return fromRichiediCreazioneDatatable;
	}

	/**
	 * @param fromRichiediCreazioneDatatable
	 */
	public void setFromRichiediCreazioneDatatable(int fromRichiediCreazioneDatatable) {
		this.fromRichiediCreazioneDatatable = fromRichiediCreazioneDatatable;
	}

	/**
	 * Invia la notifica di eliminazione gestendo eventuali errori.
	 */
	public void inviaNotificaEliminazione() {
		try {
			if(StringUtils.isNullOrEmpty(notaRichiestaEliminazione)){
				showError(ERROR_NOTA_MANCANTE_MSG);
				return ;
			} 
			RubricaComponent.inviaNotificaEliminazione(eliminaContattoItem, notaRichiestaEliminazione,utente);
			showInfoMessage("Richiesta di eliminazione inviata con successo");
			notaRichiestaEliminazione = "";
		} catch (Exception e) {
			showError(e);
		}
	}

	/**
	 * Invia la notifica di creazione gestendo eventuali errori.
	 */
	public void inviaNotificaCreazione() {
		try {
			if(StringUtils.isNullOrEmpty(notaRichiestaCreazione)) {
				showError(ERROR_NOTA_MANCANTE_MSG);
				return ;
			}

			FacesContext context = FacesContext.getCurrentInstance();
			if(documentManagerDTO.getInserisciContattoItem()!=null) {
				documentManagerDTO.getInserisciContattoItem().setIdAOO(utente.getIdAoo()); 
				boolean esito = RubricaComponent.inviaNotificaCreazione(documentManagerDTO.getInserisciContattoItem(),notaRichiestaCreazione,utente,context);
				if(!esito) {
					return;
				}
				showInfoMessage("Richiesta di creazione contatto inviata");
				notaRichiestaCreazione = "";
				FacesHelper.executeJS("PF('dialogRichCreazioneVW').hide();");
				riempiMascheraDopoRichiesta();  
			} else {
				showError("Non è possibile procedere con l'invio della richiesta creazione");
			}
		}
		catch(Exception ex) {
			showError(ex); 

		}
    }

	/**
	 * Invia la notifica di modifica gestendo eventuali errori.
	 */
	public void inviaNotificaModifica() {
		try { 
			if(StringUtils.isNullOrEmpty(notaRichiestaModifica)) {
	    		showError(ERROR_NOTA_MANCANTE_MSG);
				return ;
	    	}
		 
			FacesContext context = FacesContext.getCurrentInstance();
			RubricaComponent.inviaNotificaModifica(documentManagerDTO.getInserisciContattoItem(), contattoVecchioItem, notaRichiestaModifica,utente,context);
			notaRichiestaModifica = null;
			showInfoMessage("Richiesta di modifica inviata con successo"); 
			riempiMascheraDopoRichiesta(); 
		} catch (Exception e) {
			showError(e);
		}
	}

	private void riempiMascheraDopoRichiesta() {
		if(documentManagerDTO.isInCreazioneIngresso() || documentManagerDTO.isInModificaIngresso()) {
			FacesHelper.update("idDettagliEstesiForm:panelContattiGruppoDMD");
			setMittentePerRichCreazioneModifica();
		} else if(documentManagerDTO.isInCreazioneUscita() || documentManagerDTO.isInModificaUscita()) {
			FacesHelper.update("idDettagliEstesiForm:preferitoTabV:panelContattiGruppoDMD");
			if(fromRichiediCreazioneDatatable!=-1) {
				DestinatarioRedDTO d = this.destinatariInModifica.get(fromRichiediCreazioneDatatable);
				d.getContatto().setOnTheFly(ON_THE_FLY);
				setDestinatarioPerCreazione(d);
			} 
			else { 
				this.destinatariInModifica = getDestinatariInModificaPuliti();
				DestinatarioRedDTO d = new DestinatarioRedDTO(); 
				d.setContatto(documentManagerDTO.getInserisciContattoItem());
				d.getContatto().setOnTheFly(ON_THE_FLY);
				setDestinatarioPerCreazione(d);
				if(indiceColonnaModificata==null) {
					this.destinatariInModifica.add(d);
				}else {
					this.destinatariInModifica.set(indiceColonnaModificata, d);
				}
				
				
			} 
		}
		 
		documentManagerDTO.getInserisciContattoItem().setFromDialogCreazioneModifica(true); 
		documentManagerDTO.setInserisciContattoItem(new Contatto());
		inModificaDaShortcut=false;
		updateMaschereAfterSelect();
	}

	/**
	 * Restituisce la lista delle notifiche.
	 * @return lista notifiche
	 */
	public List<EmailDTO> getListaNotifiche() {
		return listaNotifiche;
	}

	/**
	 * Calcola la dimensione massima consentita del content impostandola sul
	 * {@link #detail}.
	 */
	protected void calcolaMaxSizeContentMittente() {
		Integer maxSize = null;
		if(detail!=null && detail.getMailSpedizione().getMittente()!=null) {
			maxSize = casellePostaliSRV.getMaxSizeContentCasellaPostale(detail.getMailSpedizione().getMittente());	
		} else {
			if(documentManagerDTO.getComboCaselleSpedizione()!=null && !documentManagerDTO.getComboCaselleSpedizione().isEmpty()) {
				CasellaPostaDTO casella = documentManagerDTO.getComboCaselleSpedizione().get(0);
				maxSize = casellePostaliSRV.getMaxSizeContentCasellaPostale(casella.getIndirizzoEmail());
			}
		}
		 
		if (detail != null && maxSize != null) {
			Float sizeMB = this.detail.convertiContentInMB(maxSize.floatValue());
			String sizeMBRound = String.format("%.2f", sizeMB).replace(",", ".");
			this.detail.setMaxSizeContent(Float.parseFloat(sizeMBRound));
		} else if (detail != null) {
			this.detail.setMaxSizeContent(null);
		}
		
	}

	/**
	 * Imposta il valore di un metadato in maiuscolo.
	 * @param metadato
	 */
	public void upperMetadato(TemplateMetadatoView metadato) {
		metadato.setValue(metadato.getValue().toUpperCase());
	}

	/**
	 * Imposta il valore di un metadato in minuscolo.
	 * @param metadato
	 */
	public void lowerMetadato(TemplateMetadatoView metadato) {
		metadato.setValue(metadato.getValue().toLowerCase());
	}

	/**
	 * Gestisce l'evento di modifica del mittente.
	 */
	public void onChangeMittente() {
		calcolaMaxSizeContentMittente();
	}

	/**
	 * Restituisce la lista delle notifiche filtrate.
	 * @return lista notifiche
	 */
	public List<EmailDTO> getListaNotificheFiltrate() {
		return listaNotificheFiltrate;
	}

	/**
	 * Imposta la lista delle notifiche filtrate.
	 * @param listaNotificheFiltrate
	 */
	public void setListaNotificheFiltrate(List<EmailDTO> listaNotificheFiltrate) {
		this.listaNotificheFiltrate = listaNotificheFiltrate;
	}

	/**
	 * Carica la lista delle notifiche filtrate ripulendo quelle filtrate precedentemente.
	 * @param indirizzoEmail
	 */
	public void loadNotificheEmailFiltrate(String indirizzoEmail){
		if(it.ibm.red.business.utils.CollectionUtils.isEmptyOrNull(listaNotifiche) 
			|| StringUtils.isNullOrEmpty(indirizzoEmail)) { 
			loadNotificheEmail();
			
		} 
		//pulisco le notifiche filtrate precedentemente
		setListaNotificheFiltrate(new ArrayList<>());
		for(EmailDTO notifica : listaNotifiche) {
			//cerco l'informazione nel testo poiché il valore del destinatario 
			//effettivo della notifica potrebbe non essere aggiornato
			if(checkTestoNotifica(notifica.getTesto(), "\""+indirizzoEmail+"\"")|| checkTestoNotifica(notifica.getTesto(), indirizzoEmail+" ")) {
				listaNotificheFiltrate.add(notifica);
			}
		}
	}

	private static boolean checkTestoNotifica(String testo, String indirizzoEmail) {
		return testo.indexOf(indirizzoEmail)!=-1;
	}

	/**
	 * Restituisce true se occorre mostrare maxContent.
	 * @return true se mostrare MaxContent, false altrimenti
	 */
	public boolean isShowMaxContent() {
		return showMaxContent;
	}

	/**
	 * Imposta il flag showMaxContent.
	 * @param showMaxContent
	 */
	public void setShowMaxContent(boolean showMaxContent) {
		this.showMaxContent = showMaxContent;
	}

	/**
	 * @return dataScarico
	 */
	public String getDataScarico() {
		return dataScarico;
	}

	/**
	 * @param dataScarico
	 */
	public void setDataScarico(String dataScarico) {
		this.dataScarico = dataScarico;
	}

	/**
	 * Restituisce la lista dei Master ottenuti dalla ricerca per protocollo risposta.
	 * @return risultato ricerca protocollo risposta
	 */
	public Collection<MasterDocumentRedDTO> getRicercaProtRispostaResult() {
		return ricercaProtRispostaResult;
	}

	/**
	 * Imposta la lista dei Master ottenuti dalla ricerca per protocollo risposta.
	 * @param ricercaProtRispostaResult
	 */
	public void setRicercaProtRispostaResult(Collection<MasterDocumentRedDTO> ricercaProtRispostaResult) {
		this.ricercaProtRispostaResult = ricercaProtRispostaResult;
	}

	/**
	 * Effettua la ricerca per protocollo risposta e restituisce la lista dei master ottenuti.
	 * @return lista master ottenuti dalla ricerca
	 */
	public Collection<MasterDocumentRedDTO> ricercaProtocolloRisposta(){
		if(getAnnoProtRispDialog() != null || getTestoRicercaProtRispDialog() != null) {
			
			ricercaProtRispostaResult = ricercaSRV.ricercaRapidaDocumenti(
												testoRicercaProtRispDialog , annoProtRispDialog,
												RicercaGenericaTypeEnum.TUTTE, CategoriaDocumentoEnum.USCITA, utente, true);
		} else {
			showWarnMessage("Immettere anno e testo per eseguire la ricerca.");
		}
		return ricercaProtRispostaResult;
		
	}

	/**
	 * Restituisce il protocollo di risposta.
	 * @return protocollo risposta
	 */
	public MasterDocumentRedDTO getProtocolloRisposta() {
		return protocolloRisposta;
	}

	/**
	 * Imposta il protocollo di risposta.
	 * @param protocolloRisposta
	 */
	public void setProtocolloRisposta(MasterDocumentRedDTO protocolloRisposta) {
		this.protocolloRisposta = protocolloRisposta;
	}

	/**
	 * Imposta {@link #protocolloRisposta} con il protocollo selezionato.
	 * @param se
	 */
	public void selezionaProtocolloRisposta(final SelectEvent se) {
		MasterDocumentRedDTO protRispostaSelect = (MasterDocumentRedDTO) se.getObject();
 
		if(protRispostaSelect == null) {
			showWarnMessage("Selezionare un protocollo risposta.");
			pulisciCampiRicercaProtRisposta();
			return;
		}
		protocolloRisposta = protRispostaSelect;
		if(!documentManagerDTO.isInModifica() || isProtocolloRispostaModificabile()) {
			//memorizzo vecchie info
			setOldOggetto(detail.getOggetto());
			setOldDescTipologiaDocumento(detail.getDescTipologiaDocumento());
			setOldFascicoloProc(new FascicoloDTO(detail.getIdFascicoloProcedimentale(), 
												 detail.getDescrizioneFascicoloProcedimentale(),
												 detail.getIndiceClassificazioneFascicoloProcedimentale(), 
												 detail.getDescrizioneTitolarioFascicoloProcedimentale(), 
												 detail.getPrefixFascicoloProcedimentale()));
			setOldDescrizioneAssegnatarioCompetenza(getDescrAssegnatarioPerCompetenza());
			
			if(!documentManagerDTO.isInModifica()) {
				//ribalto informazioni protocollo risposta
				detail.setOggetto(protocolloRisposta.getOggetto());
				
				//recupero dettaglio protocollo selezionato
				DetailDocumentRedDTO detailProtocolloRisposta = documentoRedSRV.getDocumentDetail(protocolloRisposta.getDocumentTitle(),
						utente, protocolloRisposta.getWobNumber(), protocolloRisposta.getClasseDocumentale());
				
				FascicoloDTO fProcedimentale = fascicoloSRV.getFascicoloByNomeFascicolo(detailProtocolloRisposta.getIdFascicoloProcedimentale(), utente);
				impostaFascicolo(fProcedimentale);
				
				UfficioDTO ufficioFirmatario = recuperaUfficioFirmatario();
				if(ufficioFirmatario != null) {
					NodoOrganigrammaDTO nodoOrg = new NodoOrganigrammaDTO(ufficioFirmatario.getDescrizione(),
																		  ufficioFirmatario.getId().intValue(), null);
					onOrgAssPerCompetenza(nodoOrg);
				}
			}
		}
		
		detail.setAnnoProtocolloRisposta(protocolloRisposta.getAnnoProtocollo());
		detail.setNumeroProtocolloRisposta(protocolloRisposta.getNumeroProtocollo().toString());
		//salvo anche l'id per l'eventuale comunicazione ad Nps
		detail.setIdProtocolloRisposta(protocolloRisposta.getIdProtocollo());
		detail.setDataProtocolloRisposta(protocolloRisposta.getDataProtocollazione());
		
		setProtocolloRispostaModificabile(true);
		pulisciCampiRicercaProtRisposta();
		disabilitaRispostaProtocollo = true;
	}

	/**
	 * Resetta i campi del {@link detail} e di questo bean che fanno riferimento al protocollo risposta.
	 */
	public void deselezionaProtocolloRisposta() {
		detail.setAnnoProtocolloRisposta(null);
		detail.setNumeroProtocolloRisposta(null);
		detail.setIdProtocolloRisposta(null);
		detail.setDataProtocolloRisposta(null);

		setDescrAssegnatarioPerCompetenza(oldDescrizioneAssegnatarioCompetenza);

		gestioneDeselezioneFascicoloProcedimetale();
		
		pulisciCampiRicercaProtRisposta();
		disabilitaRispostaProtocollo = false;
	}

	private void gestioneDeselezioneFascicoloProcedimetale() {
		
		if(oldFascicoloProc == null) {
			return;
		}
		
		impostaFascicolo(oldFascicoloProc);
		
		if(!StringUtils.isNullOrEmpty(oldOggetto) && oldOggetto.equals(detail.getOggetto())
				&& oldDescTipologiaDocumento.equals(detail.getDescTipologiaDocumento())){
			
			detail.setDescrizioneFascicoloProcedimentale(oldFascicoloProc.getDescrizione());
		} else {
			detail.setDescrizioneFascicoloProcedimentale(documentManagerSRV.componiDescrizioneFascicolo(detail));
		}
		detail.setDescrizioneTitolarioFascicoloProcedimentale(oldFascicoloProc.getDescrizioneTitolario());
		
		if(getDocumentManagerDTO().isInModifica()) {
			detail.setPrefixFascicoloProcedimentale(oldFascicoloProc.getPrefix());
		} else {
			detail.setPrefixFascicoloProcedimentale("NUMERO_ANNO_");
			detail.setFascicoli(null);
			//nel caso fosse stato selezionato precedentemente un fascicolo già faldonato
			detail.setFaldoni(null);
		    detail.setModificaFascicolo(false);
		}
	}
	
	private UfficioDTO recuperaUfficioFirmatario() {
		IStoricoFacadeSRV storicoSRV = ApplicationContextProvider.getApplicationContext().getBean(IStoricoFacadeSRV.class);
		Collection<StoricoDTO> eventiStorico = storicoSRV.getStorico(Integer.parseInt(protocolloRisposta.getDocumentTitle()), utente.getIdAoo().intValue());

		StoricoDTO eventoFirma = null;
		for(StoricoDTO evento : eventiStorico) {
			boolean isFirmato = EventTypeEnum.FIRMATO.getIntValue().equals(evento.getIdTipoOperazione());
			
			if(isFirmato) {
				if(eventoFirma != null) {
					Date dataEvento = DateUtils.parseDate(eventoFirma.getDataOperazione());
					Date dataEventoCorrente = DateUtils.parseDate(eventoFirma.getDataOperazione());
				
					//salto l'assegnamento se non trovo un evento firma più recente
					if(dataEvento.after(dataEventoCorrente)) { 
						continue; 
					}
				}
				eventoFirma = evento;
			}
		}
		
		UfficioDTO ufficio = null;
		if(eventoFirma != null) {
			INodoFacadeSRV nodoSRV = ApplicationContextProvider.getApplicationContext().getBean(INodoFacadeSRV.class);
			
			try {
				Nodo nodo = nodoSRV.getNodo(eventoFirma.getIdNodoAssegnante());
				ufficio = new UfficioDTO(nodo.getIdNodo(), nodo.getDescrizione());
			} catch (NullPointerException e) {
				LOGGER.error("Errore nel recupero del nodo relativo all'utente firmatario.", e);
			}
		}
		return ufficio;
	}

	/**
	 * Resetta {@link #ricercaProtRispostaResult} ripulendo i parametri della ricerca.
	 */
	public void pulisciCampiRicercaProtRisposta() {
		ricercaProtRispostaResult.clear();
		annoProtRispDialog = Calendar.getInstance().get(Calendar.YEAR);
		testoRicercaProtRispDialog = "";
	}

	/**
	 * Restituisce true se la ricerca protocollo risposta è disattivata, false altrimenti.
	 * @return true se la ricerca protocollo risposta disattivata, false altrimenti
	 */
	public boolean disabledRicercaProtocolloRisposta() {
		return documentManagerDTO.isInModifica() && !isProtocolloRispostaModificabile();
	}

	/**
	 * Restituisce l'oggetto vecchio.
	 * @return oggetto
	 */
	public String getOldOggetto() {
		return oldOggetto;
	}

	/**
	 * Imposta l'oggetto vecchio.
	 * @param oldOggetto
	 */
	public void setOldOggetto(String oldOggetto) {
		this.oldOggetto = oldOggetto;
	}

	/**
	 * Restituisce il fascicolo vecchio.
	 * @return fascicolo
	 */
	public FascicoloDTO getOldFascicoloProc() {
		return oldFascicoloProc;
	}

	/**
	 * Imposta il fascicolo vecchio.
	 * @param oldFascicoloProc
	 */
	public void setOldFascicoloProc(FascicoloDTO oldFascicoloProc) {
		this.oldFascicoloProc = oldFascicoloProc;
	}

	/**
	 * Restituisce la descrizione dell'assegnatario per competenza vecchio.
	 * @return descrizione assegnatario competenza
	 */
	public String getOldDescrizioneAssegnatarioCompetenza() {
		return oldDescrizioneAssegnatarioCompetenza;
	}

	/**
	 * Imposta la descrizione dell'assegnatario per competenza vecchio.
	 * @param oldDescrizioneAssegnatarioCompetenza
	 */
	public void setOldDescrizioneAssegnatarioCompetenza(String oldDescrizioneAssegnatarioCompetenza) {
		this.oldDescrizioneAssegnatarioCompetenza = oldDescrizioneAssegnatarioCompetenza;
	}

	/**
	 * Restituisce true se il protocollo riferimento è modificabile, false altrimenti.
	 * @return true se protocollo riferimento modificabile, false altrimenti
	 */
	public boolean isProtocolloRispostaModificabile() {
		return protocolloRispostaModificabile;
	}

	/**
	 * Imposta il flag: protocolloRispostaModificabile.
	 * @param protocolloRispostaModificabile
	 */
	public void setProtocolloRispostaModificabile(boolean protocolloRispostaModificabile) {
		this.protocolloRispostaModificabile = protocolloRispostaModificabile;
	}

	/**
	 * Restituisce l'anno del protocollo risposta.
	 * @return anno
	 */
	public Integer getAnnoProtRispDialog() {
		return annoProtRispDialog;
	}

	/**
	 * Imposta l'anno del protocollo risposta.
	 * @param annoProtRispDialog
	 */
	public void setAnnoProtRispDialog(Integer annoProtRispDialog) {
		this.annoProtRispDialog = annoProtRispDialog;
	}

	/**
	 * Restituisce il testo per la ricerca del protocollo di risposta.
	 * @return testo ricerca
	 */
	public String getTestoRicercaProtRispDialog() {
		return testoRicercaProtRispDialog;
	}

	/**
	 * Imposta il testo per la ricerca del protocollo di risposta.
	 * @param ricercaProtRispDialog
	 */
	public void setTestoRicercaProtRispDialog(String ricercaProtRispDialog) {
		this.testoRicercaProtRispDialog = ricercaProtRispDialog;
	}

	/**
	 * Restituisce la descrizione della tipologia documento vecchia.
	 * @return descrizione tipologia documento
	 */
	public String getOldDescTipologiaDocumento() {
		return oldDescTipologiaDocumento;
	}

	/**
	 * Imposta la descrizione della tipologia documento vecchia.
	 * @param oldDescTipologiaDocumento
	 */
	public void setOldDescTipologiaDocumento(String oldDescTipologiaDocumento) {
		this.oldDescTipologiaDocumento = oldDescTipologiaDocumento;
	}

	/**
	 * Restituisce il nome dell'utente che detiene il lock.
	 * @return nome utente
	 */
	public String getNomeUtenteLockatore() {
		return nomeUtenteLockatore;
	}

	/**
	 * Imposta il nome dell'utente che detiene il lock.
	 * @param nomeUtenteLockatore
	 */
	public void setNomeUtenteLockatore(String nomeUtenteLockatore) {
		this.nomeUtenteLockatore = nomeUtenteLockatore;
	}

	/**
	 * @return disabilitaRispostaProtocollo
	 */
	public boolean isDisabilitaRispostaProtocollo() {
		return disabilitaRispostaProtocollo;
	}

	/**
	 * @param disabilitaRispostaProtocollo
	 */
	public void setDisabilitaRispostaProtocollo(boolean disabilitaRispostaProtocollo) {
		this.disabilitaRispostaProtocollo = disabilitaRispostaProtocollo;
	}
	
	/**
	 * Metodo per il recupero delle notifiche destinatari onDemand
	 * @return 
	 */
	public void calcolaNotificheDestinatari() { 
		if (this.detail.getMailSpedizione().getDestinatari() != null && this.detail.getMailSpedizione().getDestinatari().split(";").length > 0) {
			if(CollectionUtils.isEmpty(listaMailTO)) {
				setListaMailTO(createMittenteDestinatariMailRow(utente, this.detail.getMailSpedizione().getDestinatari().split(";"),false));
			}
		} else {
			setListaMailTO(new ArrayList<>());
		} 
		
		if (this.detail.getMailSpedizione().getDestinatariCC() != null && this.detail.getMailSpedizione().getDestinatariCC().split(";").length > 0) {
			if(CollectionUtils.isEmpty(listaMailCC)) {
				setListaMailCC(createMittenteDestinatariMailRow(utente, this.detail.getMailSpedizione().getDestinatariCC().split(";"), false));
			}			
		} else {
			setListaMailCC(new ArrayList<>());
		}
	}
	 
	/**
	 * @return renderAcordionDest
	 */
	public boolean isRenderAcordionDest() {
		return renderAcordionDest;
	}
	
	/**
	 * Imposta flag per la renderizzazione dell'acordion nel tab sped.
	 * @param renderAcordionDest
	 */
	public void setRenderAcordionDest(boolean renderAcordionDest) {
		this.renderAcordionDest = renderAcordionDest;
	}
	 
	
	/**
	 * @return indexAcordionDest
	 */
	public String getIndexAcordionDest() {
		return indexAcordionDest;
	}
	
	/**
	 * Imposta l'index dell'arcordion.
	 * @param indexAcordionDest
	 */
	public void setIndexAcordionDest(String indexAcordionDest) {
		this.indexAcordionDest = indexAcordionDest;
	}
 
	/**
	 * @return riferimentoProtNpsDTO
	 */
	public List<RiferimentoProtNpsDTO> getRiferimentoProtNpsDTO() {
		return riferimentoProtNpsDTO;
	}

	/**
	 * @param riferimentoProtNpsDTO
	 */
	public void setRiferimentoProtNpsDTO(List<RiferimentoProtNpsDTO> riferimentoProtNpsDTO) {
		this.riferimentoProtNpsDTO = riferimentoProtNpsDTO;
	}
	
	/**
	 * Rimuove il riferimento del protocollo Nps identificato dall'index.
	 * @param index
	 */
	public void removeProtocolloDiRiferimentoNps(Object index) {
		try {
			Integer i =  (Integer) index;
			this.riferimentoProtNpsDTO.remove(i.intValue()); 
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}

	}
	
	/**
	 * Aggiunge un nuovo protocollo di riferimento nps.
	 */
	public void addProtocolloDiRiferimentoNPS() {
		try {
			if (this.riferimentoProtNpsDTO == null) {
				riferimentoProtNpsDTO = new ArrayList<>();
			}

			RiferimentoProtNpsDTO rifProt = new RiferimentoProtNpsDTO();
			rifProt.setAnnoProtocolloNps(this.documentManagerDTO.getMaxAnno()); 

			this.riferimentoProtNpsDTO.add(rifProt);

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMaschera(GENERIC_ERROR_MSG + e.getMessage());
		}
	}
	
	/**
	 * Esegue una verifica del protocollo di riferimento storico identificato da {@link #riferimentoStoricoNsd} e in base all'esito
	 * ne aggiorna alcuni parametri.
	 * @param index
	 */
	public void verificaProtocolloDiRiferimentoNps(Integer index) {
		try {
			if (index > this.riferimentoProtNpsDTO.size()) {
				throw new RedException(ERROR_INDICE_NON_VALIDO_MSG);
			}
 
			riferimentoProtNpsSelected = this.riferimentoProtNpsDTO.get(index);
			if(this.riferimentoProtNpsDTO.size() > 1) {
				for(int i=0; i<this.riferimentoProtNpsDTO.size()-1; i++) {
					if(!this.riferimentoProtNpsDTO.get(i).equals(riferimentoProtNpsSelected) && this.riferimentoProtNpsDTO.get(i).getAnnoProtocolloNps().equals(riferimentoProtNpsSelected.getAnnoProtocolloNps()) &&
							this.riferimentoProtNpsDTO.get(i).getNumeroProtocolloNps().equals(riferimentoProtNpsSelected.getNumeroProtocolloNps())) {
						showWarnMessage("Attenzione , non è possibile inserire due protocolli uguali");
						return;
					}
				}
			}
			
			if(riferimentoProtNpsSelected.getNumeroProtocolloNps()==null || riferimentoProtNpsSelected.getAnnoProtocolloNps()==null) {
				showWarnMessage("Valorizzare protocollo NPS");
				return;
			}
			ParamsRicercaProtocolloDTO paramsRicerca = new ParamsRicercaProtocolloDTO();
			paramsRicerca.setAnnoProtocollo(riferimentoProtNpsSelected.getAnnoProtocolloNps());
			paramsRicerca.setNumeroProtocolloA(riferimentoProtNpsSelected.getNumeroProtocolloNps());
			paramsRicerca.setNumeroProtocolloDa(riferimentoProtNpsSelected.getNumeroProtocolloNps());
			
			ProtocolloNpsDTO protocolloVerificato = npsSRV.ricercaProtocolli(paramsRicerca, utente).get(0); 
			if(protocolloVerificato!=null && protocolloVerificato.isEntrataProtocollo() && !"RED".equals(protocolloVerificato.getSistemaProduttore())) {
				protocolloVerificato = npsSRV.getDettagliProtocollo(protocolloVerificato, utente.getIdAoo().intValue(),utente,false);
				riferimentoProtNpsSelected.setVerificato(true);
				riferimentoProtNpsSelected.setOggettoProtocolloNps(protocolloVerificato.getOggetto());
				if(protocolloVerificato.getDocumentoPrincipale()!=null) {
					riferimentoProtNpsSelected.setIdDocumentoDownload(protocolloVerificato.getDocumentoPrincipale().getGuid());
				}				
			} else { 
 				riferimentoProtNpsSelected.setVerificato(false);  
 				showWarnMessage(ATTENZIONE_PROTOCOLLO_NON_TROVATO);
			}

		} catch (Exception e) {  
			LOGGER.error(e.getMessage(), e);
			showWarnMessage(ATTENZIONE_PROTOCOLLO_NON_TROVATO); 
		}
	}
	
	/**
	 * Gestisce la coesistenza delle funzionalità "Protocollazione anticipata" e "copia conforme",
	 * inibendo la possibilità di protocollazione non standard quando sono presenti allegati in copia conforme.
	 */
	public void onChangeMomentoProtocollazione() {
		if(MomentoProtocollazioneEnum.PROTOCOLLAZIONE_NON_STANDARD.equals(detail.getMomentoProtocollazioneEnum())) {
			TabAllegatiComponent allegatiTab = getTabAllegati();
			boolean allegatoCopiaConformePresente = false;
			
			if(!CollectionUtils.isEmpty(allegatiTab.getAllegati())){
				for (AllegatoDTO allegato : allegatiTab.getAllegati()) {
					if (Boolean.TRUE.equals(allegato.getCopiaConforme())) {
						allegatoCopiaConformePresente = true;
						break;
					}
				}
				if(allegatoCopiaConformePresente) {
					showWarnMessage("Protocollazione non standard non consentita quando sono presenti allegati in copia conforme");
					detail.setMomentoProtocollazioneEnum(MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD);
				}
			}
		}
	}
	
	/**
	 * Esegue il cambio di valuta per la tripletta di metadati in input.
	 * Il tasso di cambio si basa sulla lista di tassi di cambio
	 * validi alla data di creazione del documento.
	 * @param metadatiValuta
	 */
	public void eseguiCambioValuta(ValutaMetadatiDTO metadatiValuta) {
		String codiceValuta = metadatiValuta.getValuta().getValue4AttrExt();
		if(!StringUtils.isNullOrEmpty(codiceValuta)) {
			Double importo = null;
			String importoStr = metadatiValuta.getImporto().getValue4AttrExt();
			if(!StringUtils.isNullOrEmpty(importoStr)) {
				importo = Double.valueOf(importoStr);
				Double tassoDiCambio = detail.getMapValutaCambio().get(codiceValuta);
				
				if(tassoDiCambio != null) {
					MetadatoDTO metadatoImportoEur = metadatiValuta.getImportoEur();
					metadatoImportoEur.setSelectedValue(importo * tassoDiCambio);
					metadatiValuta.setImportoEur(metadatoImportoEur);
					
					FacesHelper.update("idDettagliEstesiForm:idTabs_DMD:doubleValuta_"+metadatoImportoEur.getName());
				} else {
					showWarnMessage("Nessun tasso di cambio valido per la valuta.");
				}
				
			} else {
				showWarnMessage("Inserire l'importo prima di eseguire la conversione.");
			}
		} else {
			showWarnMessage("Selezionare una valuta prima di eseguire la conversione.");
		}
	}
	
}