package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.service.facade.IRichiesteVistoFacadeSRV;
import it.ibm.red.business.utils.OrganigrammaRetrieverUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractTreeBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;
import it.ibm.red.web.mbean.interfaces.OrganigrammaRetriever;

/**
 * Bean che gestisce la response di richiesta visto ufficio.
 */
@Named(ConstantsWeb.MBean.RICHIEDI_VISTO_UFFICIO_BEAN)
@ViewScoped
public class RichiediVistoUfficioBean extends AbstractTreeBean implements OrganigrammaRetriever, InitHumanTaskInterface {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 5930163744473690446L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RichiediVistoUfficioBean.class);

	/**
	 * Servizio.
	 */
	private IOrganigrammaFacadeSRV organigrammaSRV;

	/**
	 * Servizio.
	 */
	private IRichiesteVistoFacadeSRV vistiSRV;

	/**
	 * Lista documenti bean.
	 */
	private ListaDocumentiBean ldb;

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Assegnazione bean.
	 */
	private MultiAssegnazioneBean assegnazioneBean;

	/**
	 * Documenti selezionati dal bean ListaDocuemntiBean
	 */
	private List<MasterDocumentRedDTO> selectedDocumentList;

	/**
	 * Input proveniente dalla maschera
	 */
	private String motivoAssegnazioneNew;

	/** LABERATURA DA ESPLODERE START***********************************************************************************/
	private List<Nodo> alberaturaNodi;
	/** LABERATURA DA ESPLODERE END*************************************************************************************/

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		selectedDocumentList = inDocsSelezionati;
	}

	/**
	 * Post construct del bean.
	 */
	@PostConstruct
	@Override
	protected void postConstruct() {
		final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
		utente = sb.getUtente();
		organigrammaSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
		alberaturaNodi = organigrammaSRV.getAlberaturaBottomUp(utente.getIdAoo(), utente.getIdUfficio());

		vistiSRV = ApplicationContextProvider.getApplicationContext().getBean(IRichiesteVistoFacadeSRV.class);
		assegnazioneBean = new MultiAssegnazioneBean();
		assegnazioneBean.initialize(this);
	}

	/**
	 * Carica l'organigramma restituendone la root.
	 * @return root dell'organigramma
	 */
	@Override
	public TreeNode loadRootOrganigramma() {
		DefaultTreeNode root = null;
		try {
			final List<Long> alberatura = new ArrayList<>();
			for (int i = alberaturaNodi.size() - 1; i >= 0; i--) {
				alberatura.add(alberaturaNodi.get(i).getIdNodo());
			}

			//prendo da DB i nodi di primo livello
			final NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
			nodoRadice.setIdAOO(utente.getIdAoo());
			nodoRadice.setIdNodo(utente.getIdUfficio());
			nodoRadice.setCodiceAOO(utente.getCodiceAoo());
			nodoRadice.setUtentiVisible(false);	
			nodoRadice.setDescrizioneNodo(utente.getNodoDesc());
			final List<NodoOrganigrammaDTO> primoLivello = organigrammaSRV.getFigliAlberoAssegnatarioPerRichiestaVisto(utente.getIdUfficio(), nodoRadice);

			root = new DefaultTreeNode();
			root.setExpanded(true);
			root.setSelectable(true);

			// in questo modo riesco anche a visualizzare il nodo ufficio
			final DefaultTreeNode nodoUfficio = new DefaultTreeNode(nodoRadice, root);
			nodoUfficio.setExpanded(true);
			nodoUfficio.setSelectable(true); 

			//per ogni nodo di primo livello creo il nodo da mettere nell'albero che ha come padre rootAssegnazionePerCompetenza
			List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
			DefaultTreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(utente.getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, nodoUfficio);
				nodoToAdd.setExpanded(true);
				nodoToAdd.setSelectable(true);
				if (n.getIdUtente() == null && (alberatura.contains(n.getIdNodo()) || n.getFigli() > 0)) {
					nodiDaAprire.add(nodoToAdd);
				}
			}

			nodiDaAprire(alberatura, nodiDaAprire);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}

		return root;
	}

	/**
	 * Gestisce l'evento di apertura di un nodo.
	 * @param treeNode nodo da aprire
	 */
	@Override
	public void onOpenTree(final TreeNode treeNode) {
		try {	
			final List<NodoOrganigrammaDTO> figli = organigrammaSRV.getFigliAlberoAssegnatarioPerRichiestaVisto(utente.getIdUfficio(), (NodoOrganigrammaDTO) treeNode.getData());

			super.onOpenTree(treeNode, figli);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}
	}

	/**
	 * Esegue la logica della response selezionata, a monte valida i campi mostrando eventuali errori.
	 * A valle gestisce l'esito di tutte le operazioni in modo da visualizzare correttamente la dialog degli esiti.
	 */
	public void doResponse() {
		//pulisco gli esiti
		ldb.cleanEsiti();
		try {
			final List<String> wobNumbers = new ArrayList<>();
			final List<MasterDocumentRedDTO> selectedDocuments = this.selectedDocumentList;
			for (final MasterDocumentRedDTO master : selectedDocuments) {
				final String wobNumber = master.getWobNumber();
				if (!StringUtils.isEmpty(wobNumber)) {
					wobNumbers.add(wobNumber);
				} else {
					LOGGER.error("Gli elementi recuperato avevano un wobnumber null o vuoto");
					throw new RedException("Errore durante l'operazione il wobNumber selezionato è vuoto");
				}
			}

			if (CollectionUtils.isEmpty(wobNumbers)) {
				LOGGER.error("Non è stato selezionato nessun documento per l'esecuzione della response" + ldb.getSelectedResponse().getResponse());
				throw new RedException("Errore durante l'operazione il wobNumber selezionato è vuoto");
			} else {

				final List<NodoOrganigrammaDTO> nodiAssegnatari = assegnazioneBean.getSelectedElementList();

				EsitoOperazioneDTO esito; 
				if (CollectionUtils.isEmpty(nodiAssegnatari)) {
					esito = new EsitoOperazioneDTO(null, false, "Per continuare è necessario aggiungere almeno un assegnatario per conoscenza");
					ldb.getEsitiOperazione().add(esito);
				} else {
					final List<AssegnatarioOrganigrammaDTO> assegnatari = nodiAssegnatari.stream()
							.map(c -> OrganigrammaRetrieverUtils.create(c))
							.collect(Collectors.toList());
					for (final String wobNumber : wobNumbers) {				
						// A livello di implementazione RichiediVistoInterno e RichiediVistoUfficio fanno la stessa cosa.
						// Durante il workflow passsiamo a fileNet la response corretta
						if (ResponsesRedEnum.RICHIEDI_VISTO_INTERNO == ldb.getSelectedResponse()) {
							esito = vistiSRV.richiediVistoInterno(utente, wobNumber, motivoAssegnazioneNew, assegnatari);
						} else {
							esito = vistiSRV.richiediVistoUfficio(utente, wobNumber, motivoAssegnazioneNew, assegnatari);
						}
						ldb.getEsitiOperazione().add(esito);	
					}

				}
			}
		} catch (final Exception e) {
			showError(e);
		}
		destroyBean();
	}

	/**
	 * Si occupa delle azioni a valle della distruzione del bean.
	 */
	public void destroyBean() {
		ldb.destroyBeanViewScope(ConstantsWeb.MBean.RICHIEDI_VISTO_UFFICIO_BEAN);
	}

	/**
	 * Restutisce il motivo dell'assegnazione.
	 * @return motivo assegnazione
	 */
	public String getMotivoAssegnazioneNew() {
		return motivoAssegnazioneNew;
	}

	/**
	 * Imposta il motivo dell'assegnazione.
	 * @param motivoAssegnazioneNew
	 */
	public void setMotivoAssegnazioneNew(final String motivoAssegnazioneNew) {
		this.motivoAssegnazioneNew = motivoAssegnazioneNew;
	}

	/**
	 * Restituisce il bean che gestisce l'assegnazione multipla.
	 * @return bean assegnazioni
	 */
	public MultiAssegnazioneBean getAssegnazioneBean() {
		return assegnazioneBean;
	}
}
