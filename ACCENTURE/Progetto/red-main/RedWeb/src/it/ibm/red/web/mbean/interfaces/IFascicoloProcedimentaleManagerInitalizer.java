package it.ibm.red.web.mbean.interfaces;

/**
 * Inizializzatore del manager del fascicolo procedimentale,
 * Recupera le informazioni necessarie a inizializzare un fascicolo manager
 * 
 * @author a.difolca
 *
 */
public interface IFascicoloProcedimentaleManagerInitalizer {

	/**
	 * Permette di initializzare i dati necessari a viasualizzare il fascicolo manager
	 * 
	 * Si presume che il IFascicoloProcedimentaleManagerInitalizer possegga già tutte le informazioni per recuperare il fsciolo procedimentale
	 */
	void openFascicoloProcedimentale();
}
