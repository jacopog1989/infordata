package it.ibm.red.web.nps.component;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.dto.DocumentoNpsDTO;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.INpsFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.mbean.component.AbstractComponent;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.helper.faces.MessageSeverityEnum;
import it.ibm.red.web.mbean.SessionBean;

/**
 * Component che gestisce il dettaglio nps.
 */
public class DetailNpsComponent extends AbstractComponent {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -4036255901144220813L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DetailNpsComponent.class);
	
	/**
	 * Service.
	 */
	private final INpsFacadeSRV npsSRV;
	
	/**
	 * Info detail.
	 */
	private ProtocolloNpsDTO detail;

	/**
	 * Costruttore del component.
	 */
	public DetailNpsComponent() {
		npsSRV = ApplicationContextProvider.getApplicationContext().getBean(INpsFacadeSRV.class);
	}
	
	/** TAB DOCUMENTI START **************************************************************************************************************/

	/**
	 * Restituisce lo StreamedContent per effettuare il download del documento con lo specifico guid.
	 * @param idAoo
	 * @param guid
	 * @return StreamedContent per download
	 */
	public StreamedContent downloadContent(final String guid) {
		StreamedContent output = null;
		try {
			final SessionBean sessionBean = FacesHelper.getManagedBean(MBean.SESSION_BEAN);
			//Nel fascicolo dobbiamo sempre accedere per admin per il download verso nps perciò è true
			final DocumentoNpsDTO d = npsSRV.downloadDocumento(sessionBean.getUtente().getIdAoo().intValue(), guid, true);
			output = new DefaultStreamedContent(d.getInputStream(), d.getContentType(), d.getNomeFile());
		} catch (final Exception e) {
			LOGGER.error("Impossibile scaricare un contenuto da nps", e);
            FacesHelper.showMessage(null, "Impossibile recuperare il File selezionato", MessageSeverityEnum.ERROR);
        }
		return output;
	}
	/** TAB DOCUMENTI END ****************************************************************************************************************/

	/** GET & SET ****************************************************************/	

	/**
	 * Restituisce i dettagli come ProtocolloNpsDTO.
	 * @return detail
	 */
	public ProtocolloNpsDTO getDetail() {
		return detail;
	}

	/**
	 * Imposta il detail.
	 * @param detail
	 */
	public void setDetail(final ProtocolloNpsDTO detail) {
		this.detail = detail;
	}

}
