package it.ibm.red.web.document.mbean.interfaces;

import java.io.Serializable;

import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.NodoOrganigrammaDTO;

/**
 * @author m.crescentini
 *
 */
public interface IAssOrganigrammaComponent extends Serializable {
	
	/**
	 * Definisce la logica di recupero dalla radice dell'organigramma.
	 */
	void loadRootOrganigramma();
	
	/**
	 * @return
	 */
	TreeNode getRootOrganigramma();
	
	/**
	 * Metodo triggerato dall'evento expand del tree node.
	 * 
	 * @param event
	 */
	void onOpenNodo(NodeExpandEvent event);
	
	/**
	 * Metodo triggerato dall'evento select del tree node.
	 * 
	 * @param event
	 */
	void onSelectNodo(NodeSelectEvent event);
	
	/**
	 * Resetta il componente (carica l'organigramma iniziale, svuota l'assegnatario, etc.)
	 */
	void reset();
	
	/**
	 * Pulisce la selezione dell'assegnatario.
	 */
	void pulisciSelezioneAssegnatario();
	
	/**
	 * Verifica se è stato selezionato un assegnatario.
	 * 
	 * @return
	 */
	boolean checkSelezionato();
	
	/**
	 * Costruisce la descrizione del nodo selezionato.
	 * 
	 * @return
	 */
	String getDescrizioneNodoSelected();
	
	/**
	 * Costruisce la descrizione dell'utente selezionato.
	 * 
	 * @return
	 */
	String getDescrizioneUtenteSelected();
	
	/**
	 * @return
	 */
	NodoOrganigrammaDTO getSelected();
	
	/**
	 * @return
	 */
	String getDescrizioneAssegnatario();
	
}
