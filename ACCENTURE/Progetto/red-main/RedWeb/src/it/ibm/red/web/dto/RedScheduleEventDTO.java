package it.ibm.red.web.dto;

import java.util.Date;
import java.util.List;

import org.primefaces.model.DefaultScheduleEvent;

import it.ibm.red.business.dao.impl.TipoCalendario;
import it.ibm.red.business.enums.EventoCalendarioEnum;

/**
 * DTO che definisce le caratteristiche di un evento gestito dallo scheduler di RED.
 */
public class RedScheduleEventDTO extends DefaultScheduleEvent {

	/**
	 * Costante serial versione UID.
	 */
	private static final long serialVersionUID = 8387048797120950489L;

	/**
	 * Evento.
	 */
	private Integer idEvento;

	/**
	 * Contenuto.
	 */
	private String contenuto;

	/**
	 * Link esterno.
	 */
	private String linkEsterno;

	/**
	 * Utente.
	 */
	private Long idUtente;

	/**
	 * Tipologia evento.
	 */
	private EventoCalendarioEnum tipologiaEvento;

	/**
	 * Tipo calendario.
	 */
	private TipoCalendario tipoCalendario;

	/**
	 * Lista ruoli.
	 */
	private List<Long> idRuoliCategoriaTarget;

	/**
	 * Lista nodi.
	 */
	private List<Integer> idNodiTarget;

	/**
	 * Descrizione nodi.
	 */
	private List<String> descrizioneNodi;
	
	/**
	 * Allegato.
	 */
	private byte[] allegato;

	/**
	 * Nome allegato.
	 */
	private String nomeAllegato;

	/**
	 * Estensione allegato.
	 */
	private String estensioneAllegato;

	/**
	 * Mime type.
	 */
	private String mimeTypeAllegato;
	
	/**
	 * Costruttore vuoto.
	 */
	public RedScheduleEventDTO() {
		
	}
	
	/**
	 * Costruttore.
	 * @param title
	 * @param start
	 * @param end
	 * @param idEvento
	 * @param styleClass
	 * @param contenuto
	 * @param linkEsterno
	 * @param idUtente
	 * @param tipoCalendario
	 * @param allegato
	 * @param nomeAllegato
	 * @param estensioneAllegato
	 * @param mimeTypeAllegato
	 * @param idRuoliCategoriaTarget
	 * @param idNodiTarget
	 * @param descrizioneNodi
	 */
	public RedScheduleEventDTO(final String title, final Date start, final Date end, final int idEvento, final String styleClass, final String contenuto,
			final String linkEsterno, final Long idUtente, final TipoCalendario tipoCalendario, final byte[] allegato, final String nomeAllegato,
			final String estensioneAllegato, final String mimeTypeAllegato, final List<Long> idRuoliCategoriaTarget,
			final List<Integer> idNodiTarget, final List<String> descrizioneNodi) {
		if ("D".equals(tipoCalendario.getTipoEnum().getValue())) {
			setTitle(tipoCalendario.getTipoEnum().getDescription());
		} else {
			setTitle(title);
		}
		setStartDate(start);
		setEndDate(end);
		setStyleClass(styleClass);
		setIdEvento(idEvento);
		setContenuto(contenuto);
		setLinkEsterno(linkEsterno);
		setIdUtente(idUtente);
		tipoCalendario.setIdTipo(tipoCalendario.getTipoEnum().getIdTipoCal());
		setTipoCalendario(tipoCalendario);
		setTipologiaEvento(tipoCalendario.getTipoEnum());
		setAllegato(allegato);
		setNomeAllegato(nomeAllegato);
		setMimeTypeAllegato(mimeTypeAllegato);
		setEstensioneAllegato(estensioneAllegato);
		setIdRuoliCategoriaTarget(idRuoliCategoriaTarget);
		setIdNodiTarget(idNodiTarget);
		setDescrizioneNodi(descrizioneNodi);
	}

	/**
	 * Restituisce l'id dell'evento.
	 * @return id evento
	 */
	public Integer getIdEvento() {
		return idEvento;
	}
	
	/**
	 * Imposta l'id dell'evento.
	 * @param idEvento
	 */
	public void setIdEvento(final Integer idEvento) {
		this.idEvento = idEvento;
	}
	
	/**
	 * Restituisce il contenuto.
	 * @return contenuto
	 */
	public String getContenuto() {
		return contenuto;
	}
	
	/**
	 * Imposta il contenuto.
	 * @param contenuto
	 */
	public void setContenuto(final String contenuto) {
		this.contenuto = contenuto;
	}
	
	/**
	 * Restituisce il link esterno.
	 * @return link esterno
	 */
	public String getLinkEsterno() {
		return linkEsterno;
	}
	
	/**
	 * Imposta il link esterno.
	 * @param linkEsterno
	 */
	public void setLinkEsterno(final String linkEsterno) {
		this.linkEsterno = linkEsterno;
	}
	
	/**
	 * Restituisce l'id dell'utente.
	 * @return id utente
	 */
	public Long getIdUtente() {
		return idUtente;
	}
	
	/**
	 * Imposta l'id dell'utente.
	 * @param idUtente
	 */
	public void setIdUtente(final Long idUtente) {
		this.idUtente = idUtente;
	}
	
	/**
	 * Restituisce la tipologia dell'evento.
	 * @return tipologia evento
	 */
	public EventoCalendarioEnum getTipologiaEvento() {
		return tipologiaEvento;
	}
	
	/**
	 * Imposta la tipologia dell'evento.
	 * @param tipologiaEvento
	 */
	public void setTipologiaEvento(final EventoCalendarioEnum tipologiaEvento) {
		this.tipologiaEvento = tipologiaEvento;
	}
	
	/**
	 * Restituisce l'allegato come byte array.
	 * @return content allegato
	 */
	public byte[] getAllegato() {
		return allegato;
	}
	
	/**
	 * Imposta il content dell'allegato.
	 * @param allegato
	 */
	public void setAllegato(final byte[] allegato) {
		this.allegato = allegato;
	}
	
	/**
	 * Restituisce il nome dell'allegato.
	 * @return nome allegato
	 */
	public String getNomeAllegato() {
		return nomeAllegato;
	}
	
	/**
	 * Imposta il nome dell'allegato.
	 * @param nomeAllegato
	 */
	public void setNomeAllegato(final String nomeAllegato) {
		this.nomeAllegato = nomeAllegato;
	}
	
	/**
	 * Restituisce l'estensione dell'allegato.
	 * @return estensione allegato
	 */
	public String getEstensioneAllegato() {
		return estensioneAllegato;
	}
	
	/**
	 * Imposta l'estensione dell'allegato.
	 * @param estensioneAllegato
	 */
	public void setEstensioneAllegato(final String estensioneAllegato) {
		this.estensioneAllegato = estensioneAllegato;
	}
	
	/**
	 * Restituisce il mimetype dell'allegato.
	 * @return mimetype allegato
	 */
	public String getMimeTypeAllegato() {
		return mimeTypeAllegato;
	}
	
	/**
	 * Imposta il mimetype dell'allegato.
	 * @param mimeTypeAllegato
	 */
	public void setMimeTypeAllegato(final String mimeTypeAllegato) {
		this.mimeTypeAllegato = mimeTypeAllegato;
	}
	
	/**
	 * Restituisce il tipo calendario.
	 * @return tipo calendario
	 */
	public TipoCalendario getTipoCalendario() {
		return tipoCalendario;
	}
	
	/**
	 * Imosta il tipo del calendario.
	 * @param tipoCalendario
	 */
	public void setTipoCalendario(final TipoCalendario tipoCalendario) {
		this.tipoCalendario = tipoCalendario;
	}
	
	/**
	 * Restituisce gli id dei ruoli delle categoria target.
	 * @return id ruoli
	 */
	public List<Long> getIdRuoliCategoriaTarget() {
		return idRuoliCategoriaTarget;
	}
	
	/**
	 * Imposta gli id dei ruoi categoria target.
	 * @param idRuoliCategoriaTarget
	 */
	public void setIdRuoliCategoriaTarget(final List<Long> idRuoliCategoriaTarget) {
		this.idRuoliCategoriaTarget = idRuoliCategoriaTarget;
	}
	
	/**
	 * Restituisce gli id dei nodi target.
	 * @return id nodi target
	 */
	public List<Integer> getIdNodiTarget() {
		return idNodiTarget;
	}
	
	/**
	 * Imposta gli id dei nodi target.
	 * @param idNodiTarget
	 */
	public void setIdNodiTarget(final List<Integer> idNodiTarget) {
		this.idNodiTarget = idNodiTarget;
	}
	
	/**
	 * Restituisce la lista delle descrizioni dei nodi.
	 * @return descrizione nodi
	 */
	public List<String> getDescrizioneNodi() {
		return descrizioneNodi;
	}
	
	/**
	 * Imposta la lista delle descrizioni dei nodi.
	 * @param descrizioneNodi
	 */
	public void setDescrizioneNodi(final List<String> descrizioneNodi) {
		this.descrizioneNodi = descrizioneNodi;
	}

}
