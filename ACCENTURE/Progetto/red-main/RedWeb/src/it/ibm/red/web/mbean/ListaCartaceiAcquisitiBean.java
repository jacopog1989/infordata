package it.ibm.red.web.mbean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections4.CollectionUtils;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocCartaceoRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.AccessoFunzionalitaEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.StatoDocCartaceoEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IRecogniformFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.mbean.DocumentManagerBean;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.component.DetailPreviewIntroComponent;
import it.ibm.red.web.mbean.interfaces.IPopupComponent;
import it.ibm.red.web.mbean.interfaces.IVisualizzaDocumentoComponent;
import it.ibm.red.web.servlets.DownloadContentServlet.DocumentTypeEnum;

/**
 * Bean lista cartacei acquisiti.
 */
@Named(MBean.LISTA_CARTACEI_ACQUISITI_BEAN)
@ViewScoped
public class ListaCartaceiAcquisitiBean extends AbstractBean implements IPopupComponent {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 8683087078775461431L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ListaCartaceiAcquisitiBean.class.getName());

	/**
	 * Messaggio di errore nessun documento selezionato.
	 */
	private static final String ERROR_NESSUN_DOCUMENTO_SELEZIONATO_MSG = "Selezionare almeno un documento";

	/**
	 * Session bean.
	 */
	private SessionBean sessionBean;

	/**
	 * Informazioni utente.
	 */
	private UtenteDTO utente;

	/**
	 * Servizio.
	 */
	private IRecogniformFacadeSRV recogniformSRV;

	/**
	 * Datatable documenti cartacei.
	 */
	protected SimpleDetailDataTableHelper<MasterDocCartaceoRedDTO> documentiDTH;

	/**
	 * Coda attuale.
	 */
	private DocumentQueueEnum codaAttuale;

	/**
	 * Lista colonne.
	 */
	private List<Boolean> columns;

	/**
	 * menù delle response.
	 */
	private transient MenuModel responseMenuModel;

	/**
	 * Documento selezionato.
	 */
	private String idDocSelectedForResponse;

	/**
	 * Coda attuale.
	 */
	private String nomeCodaAttuale;

	/**
	 * Label documenti caricati.
	 */
	private String labelCaricatiValue;

	/**
	 * Response selezionata.
	 */
	private String responseSelezionataLabel;

	/**
	 * Esito operazione.
	 */
	private EsitoOperazioneDTO esitoOperazione;

	/**
	 * Componente preview documento.
	 */
	private DetailPreviewIntroComponent previewComponent;

	/**
	 * Component visualizzazione documento.
	 */
	private IVisualizzaDocumentoComponent visualizzaCartaceoComponent;

	/**
	 * Dettaglio documento cartaceo.
	 */
	private DetailDocumentRedDTO detailCartaceo;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		recogniformSRV = ApplicationContextProvider.getApplicationContext().getBean(IRecogniformFacadeSRV.class);

		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();

		labelCaricatiValue = "CARICATI : 0 / 0";

		documentiDTH = new SimpleDetailDataTableHelper<>();
		codaAttuale = sessionBean.getActivePageInfo().getDocumentQueue();
		costruisciDataTablePerCoda();

		previewComponent = new DetailPreviewIntroComponent(utente);
		visualizzaCartaceoComponent = new VisualizzaDocumentoPrincipaleComponent(utente, DocumentTypeEnum.CARTACEO);

		// ### Primo caricamento
		refresh(false, false);
	}

	/**
	 * {@link #refresh(boolean, boolean)}.
	 */
	public final void refresh() {
		refresh(true, false);
	}

	/**
	 * {@link #refresh(boolean, boolean)}.
	 */
	public final void refreshAndUpdateView() {
		refresh(true, true);
	}

	/**
	 * Aggiorna le view che compongono le code relative ai documenti cartacei. È
	 * possibile aggiornare vista (menù laterale e coda documenti) e contenuto in
	 * base ai booleani in ingresso.
	 * 
	 * @param callRefreshLeftMenu
	 * @param updateView
	 */
	public final void refresh(final boolean callRefreshLeftMenu, final boolean updateView) {
		List<MasterDocCartaceoRedDTO> notFilterMasters = new ArrayList<>();
		documentiDTH.setMasters(new ArrayList<>());

		if (codaAttuale != null) {
			nomeCodaAttuale = codaAttuale.getDisplayName();

			switch (codaAttuale) {
			case ELIMINATI:
				notFilterMasters = recogniformSRV.getDocumentiCartacei(false, true, utente);
				break;

			default:
			case ACQUISITI:
				notFilterMasters = recogniformSRV.getDocumentiCartacei(false, false, utente);
			}

			if (notFilterMasters != null) {
				labelCaricatiValue = "CARICATI : " + notFilterMasters.size() + " / " + notFilterMasters.size(); // Code non paginate
			}

			if (callRefreshLeftMenu) {
				sessionBean.countMenuAttivita(false);

				if ((sessionBean.getFattElettEnable() || sessionBean.getPreferenze().isGroupFatElettronica())
						&& sessionBean.haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.MENUFEPA)) {
					sessionBean.countMenuFepa(false);
					FacesHelper.update("westSectionForm:idTabMain_LeftAside:idMenuLeftAside:idMenuFepa");
				}

				FacesHelper.update("westSectionForm:idTabMain_LeftAside:idMenuLeftAside:idMenuAttivita");

				final DataTable listaCartaceiAcquisitiDT = getDataTableByIdComponent("centralSectionForm:documentiCartaceiAcquisiti");
				listaCartaceiAcquisitiDT.reset();
			}

			if (updateView) {
				FacesHelper.update("centralSectionForm:loadedLabelCartaceiAcquisiti");
				FacesHelper.update("centralSectionForm:documentiCartaceiAcquisiti");
			}
		}

		documentiDTH.setMasters(notFilterMasters);

		documentiDTH.selectFirst(true);
		if (documentiDTH.getCurrentMaster() != null) {
			selectDetail(documentiDTH.getCurrentMaster());
		} else {
			// Si pulisce il dettaglio da eventuali dati rimasti in memoria
			clearDetail();
		}
	}

	/**
	 * Response "Censisci Documento In Entrata".
	 */
	public void censisciDocInEntrata() {
		if (idDocSelectedForResponse != null) {
			try {
				MasterDocCartaceoRedDTO docSelectedForResponse = null;
				final List<MasterDocCartaceoRedDTO> allegati = new ArrayList<>();

				final Collection<MasterDocCartaceoRedDTO> currentMasters = getCurrentMasters();
				for (final MasterDocCartaceoRedDTO master : currentMasters) {

					if (master.isSelectedAsAllegato()) {
						allegati.add(master);
					} else if (master.getDocumentTitle().equals(idDocSelectedForResponse)) {
						docSelectedForResponse = master;
					}

				}

				// A partire dal documento principale e dagli allegati selezionati, si
				// costruisce il dettaglio da passare alla maschera di creazione
				final DetailDocumentRedDTO detailDocInEntrata = recogniformSRV.preparaDetailDocumentoInEntrataCartaceo(docSelectedForResponse, allegati, utente);

				final DocumentManagerBean bean = FacesHelper.getManagedBean(MBean.DOCUMENT_MANAGER_BEAN);
				// Si pulisce nel caso sia già stato aperto
				bean.unsetDetail();
				bean.setChiamante(MBean.LISTA_CARTACEI_ACQUISITI_BEAN);
				bean.setDetail(detailDocInEntrata);

				FacesHelper.update("idDettagliEstesiForm:idPanelDocumentManager");
				FacesHelper.update("idDettagliEstesiForm:idPanelContainerDocumentManager_T");

				FacesHelper.executeJS("PF('dlgManageDocument_RM').show()");
			} catch (final Exception e) {
				LOGGER.error("Errore durante la preparazione del documento in entrata", e);
				showError("Errore durante la preparazione del documento in entrata");
			}
		} else {
			showError(ERROR_NESSUN_DOCUMENTO_SELEZIONATO_MSG);
		}
	}

	/**
	 * Aggiorna la selezione dell'allegato come documento principale
	 * 
	 * @param documentTitle
	 */
	public final void updateSelectedAsAllegato(final String documentTitle) {
		// Se si sta selezionando come allegato il documento cartaceo attualmente
		// selezionato come principale, si rimuove la selezione del principale
		if (documentTitle.equals(idDocSelectedForResponse)) {
			idDocSelectedForResponse = null;
		}
	}

	/**
	 * Selezione di una riga del datatable.
	 * 
	 * @param se
	 */
	public final void onRowSelect(final SelectEvent se) {
		documentiDTH.rowSelector(se);
		selectDetail(documentiDTH.getCurrentMaster());
	}

	private void selectDetail(final MasterDocCartaceoRedDTO m) {
		// Se ci si trova nella coda "Eliminati", il documento selezionato corrisponde a
		// quello su cui si possono eseguire le response
		if (DocumentQueueEnum.ELIMINATI.equals(codaAttuale)) {
			idDocSelectedForResponse = m.getDocumentTitle();
		}

		// Si imposta l'anteprima
		previewComponent.setDetailPreview(m.getDocumentTitle(), DocumentTypeEnum.CARTACEO);
	}

	/**
	 * Selezione di un documento cartaceo come principale.
	 */
	public final void onSelectDocumentoPrincipale() {
		final Collection<MasterDocCartaceoRedDTO> currentMasters = getCurrentMasters();

		MasterDocCartaceoRedDTO docSelected = null;
		for (final MasterDocCartaceoRedDTO master : currentMasters) {
			master.setSelectedAsAllegato(false);
			if (master.getDocumentTitle().equals(idDocSelectedForResponse)) {
				docSelected = master;
			}
		}

		if (docSelected != null) {
			// Si selezionano gli allegati del principale selezionato
			for (final MasterDocCartaceoRedDTO master : currentMasters) {
				if (master.getGuidPrincipale() != null && master.getGuidPrincipale().equals(docSelected.getGuid())) {
					master.setSelectedAsAllegato(true);
				}
			}
		}

		FacesHelper.update("centralSectionForm:pnlListaCartaceiAcquisiti");
	}

	private void clearDetail() {
		previewComponent.unsetDetail();
		visualizzaCartaceoComponent.unsetDetail();
		FacesHelper.update("eastSectionForm:eastSectionPanelId");
	}

	/**
	 * Prepara la maschera di visualizzazione del documento popolandone il
	 * dettaglio.
	 */
	public void openVisualizzaDocumento() {
		if (documentiDTH.getCurrentMaster() != null) {
			final MasterDocCartaceoRedDTO currentMaster = documentiDTH.getCurrentMaster();

			detailCartaceo = new DetailDocumentRedDTO();
			detailCartaceo.setDocumentTitle(currentMaster.getDocumentTitle());
			detailCartaceo.setGuid(currentMaster.getGuid());
			detailCartaceo.setNomeFile(currentMaster.getBarcode());
			detailCartaceo.setMimeType(currentMaster.getMimeType());

			visualizzaCartaceoComponent.setDetail(detailCartaceo);
		}
	}

	/**
	 * Response "Elimina" per i documenti cartacei acquisiti.
	 */
	public void eliminaDocumento() {
		responseSelezionataLabel = ResponseCartaceiAcquisitiRedEnum.ELIMINA.nome;
		aggiornaStatoDocCartaceo(StatoDocCartaceoEnum.ELIMINATO);
	}

	/**
	 * Response "Ripristina" per i documenti cartacei eliminati.
	 */
	public void ripristinaDocumento() {
		responseSelezionataLabel = ResponseCartaceiAcquisitiRedEnum.RIPRISTINA.nome;
		aggiornaStatoDocCartaceo(StatoDocCartaceoEnum.ACQUISITO);
	}

	private void aggiornaStatoDocCartaceo(final StatoDocCartaceoEnum nuovoStato) {
		if (idDocSelectedForResponse != null) {

			final Collection<MasterDocCartaceoRedDTO> currentMasters = getCurrentMasters();

			MasterDocCartaceoRedDTO docSelectedForResponse = null;
			for (final MasterDocCartaceoRedDTO currentMaster : currentMasters) {
				if (currentMaster.getDocumentTitle().equals(idDocSelectedForResponse)) {
					docSelectedForResponse = currentMaster;
					break;
				}
			}

			if (docSelectedForResponse != null) {
				// Si effettua l'eliminazione logica del documento cartaceo acquisito
				esitoOperazione = recogniformSRV.aggiornaStatoRecogniform(docSelectedForResponse.getBarcode(), docSelectedForResponse.getGuid(), nuovoStato, utente);

				mostraDialogEsitoOperazione();
			}
		} else {
			showError(ERROR_NESSUN_DOCUMENTO_SELEZIONATO_MSG);
		}
	}

	/**
	 * Response "Rimuovi" per i documenti cartacei eliminati. N.B. Nella coda
	 * "Eliminati", il Master selezionato (documentiDTH.getCurrentMaster())
	 * corrisponde all'elemento su cui sono abilitate le response in quel momento
	 * (docSelectedForResponse).
	 */
	private void rimuoviDocumento() {
		if (documentiDTH.getCurrentMaster() != null) {
			responseSelezionataLabel = ResponseCartaceiAcquisitiRedEnum.RIMUOVI.nome;

			final MasterDocCartaceoRedDTO docSelectedForResponse = documentiDTH.getCurrentMaster();

			// Si effettua la rimozione da FileNet del documento cartaceo eliminato
			esitoOperazione = recogniformSRV.rimuoviDocumento(docSelectedForResponse.getBarcode(), docSelectedForResponse.getGuid(),
					docSelectedForResponse.getGuidPrincipale(), utente);

			mostraDialogEsitoOperazione();
		} else {
			showError(ERROR_NESSUN_DOCUMENTO_SELEZIONATO_MSG);
		}
	}

	private void mostraDialogEsitoOperazione() {
		// Si aggiorna la dialog contenente gli esiti
		FacesHelper.update("centralSectionForm:idDlgShowResultCartaceiAcquisiti");

		// Si richiama la visualizzazione della dialog
		FacesHelper.executeJS("PF('dlgShowResultCartaceiAcquisiti').show()");
	}

	/**
	 * Pulsante di conferma per l'operazione di rimozione di un documento cartaceo
	 * eliminato.
	 * 
	 * @see it.ibm.red.web.mbean.interfaces.IPopupComponent#conferma()
	 */
	@Override
	public void conferma() {
		rimuoviDocumento();
	}

	/**
	 * Pulsante di annullamento per l'operazione di rimozione di un documento
	 * cartaceo eliminato.
	 * 
	 * @see it.ibm.red.web.mbean.interfaces.IPopupComponent#annulla()
	 */
	@Override
	public void annulla() {
		// Non deve fare nulla
	}

	/**
	 * Calcola, in base alla coda, le colonne del datatable da visualizzare e le
	 * response dei documenti in essa presenti. N.B. Nel caso di documenti cartacei,
	 * le response sono le stesse per tutti i documenti.
	 */
	private void costruisciDataTablePerCoda() {
		if (DocumentQueueEnum.ACQUISITI.equals(codaAttuale)) {
			columns = Arrays.asList(true, true, true, true, true, true);
		} else if (DocumentQueueEnum.ELIMINATI.equals(codaAttuale)) {
			columns = Arrays.asList(false, false, true, true, true, true);
		}

		// Si calcolano le response per i documenti della coda (sono le stesse per tutti
		// i documenti)
		responseMenuModel = new DefaultMenuModel();
		DefaultMenuItem menuItem = null;
		for (final ResponseCartaceiAcquisitiRedEnum response : ResponseCartaceiAcquisitiRedEnum.values()) {

			if (response.getCoda().equals(codaAttuale)) {
				menuItem = new DefaultMenuItem(response.getNome());
				menuItem.setTitle(response.getNome());
				if (response.getAzione() != null) {
					menuItem.setCommand("#{ListaCartaceiAcquisitiBean." + response.getAzione() + "}");
					menuItem.setOnstart("PF('statusDialog').show();");
					menuItem.setOnsuccess("PF('statusDialog').hide();");
					menuItem.setProcess("centralSectionForm:pnlListaCartaceiAcquisiti");
					menuItem.setDisabled(utente.isGestioneApplicativa());
					menuItem.setPartialSubmit(true);
				}
				if (response.getOnClick() != null) {
					menuItem.setOnclick(response.getOnClick());
				}
				menuItem.setIcon(response.getIcona());

				responseMenuModel.addElement(menuItem);
			}

		}
	}

	/**
	 * Calcola il menù delle response per il singolo documento.
	 * 
	 * @param barcode
	 * @return model menu
	 */
	public DefaultMenuModel calcolaReponseMenuModelPerDocumento(final String barcode) {
		final DefaultMenuModel docResponseMenuModel = new DefaultMenuModel();

		// Si crea l'intestazione del menù delle response
		final DefaultMenuItem docResponseMenuModelTitle = new DefaultMenuItem("Azioni Documento: " + barcode);
		docResponseMenuModelTitle.setDisabled(true);
		docResponseMenuModelTitle.setStyleClass("header-response");
		docResponseMenuModel.addElement(docResponseMenuModelTitle);

		// Si aggiungono le response calcolate inizialmente per la coda
		docResponseMenuModel.getElements().addAll(responseMenuModel.getElements());

		docResponseMenuModel.generateUniqueIds();

		return docResponseMenuModel;
	}

	/**
	 * Enum delle response cartacei acqusiti.
	 */
	public enum ResponseCartaceiAcquisitiRedEnum {

		/**
		 * Valore.
		 */
		CENSISCI_DOC_IN_ENTRATA("Censisci Documento in Entrata", "censisciDocInEntrata()", null, DocumentQueueEnum.ACQUISITI, "fa fa-file-powerpoint-o"),

		/**
		 * Valore.
		 */
		ELIMINA("Elimina", "eliminaDocumento()", null, DocumentQueueEnum.ACQUISITI, "fa fa-trash"),

		/**
		 * Valore.
		 */
		RIPRISTINA("Ripristina", "ripristinaDocumento()", null, DocumentQueueEnum.ELIMINATI, "fa fa-edit"),

		/**
		 * Valore.
		 */
		RIMUOVI("Rimuovi", null, "PF('dlgConfirmRimuoviCartaceoEliminato').show(); return false;", DocumentQueueEnum.ELIMINATI, "fa fa-times");

		/**
		 * Nome.
		 */
		private final String nome;

		/**
		 * Azione.
		 */
		private final String azione;

		/**
		 * On click.
		 */
		private final String onClick;

		/**
		 * Coda.
		 */
		private final DocumentQueueEnum coda;

		/**
		 * Icona.
		 */
		private final String icona;

		/**
		 * Costruttore.
		 * @param nome
		 * @param azione
		 * @param onClick
		 * @param coda
		 * @param icona
		 */
		ResponseCartaceiAcquisitiRedEnum(final String nome, final String azione, final String onClick, final DocumentQueueEnum coda, final String icona) {
			this.nome = nome;
			this.azione = azione;
			this.onClick = onClick;
			this.coda = coda;
			this.icona = icona;
		}

		/**
		 * Restituisce il nome della response.
		 * 
		 * @return nome
		 */
		public String getNome() {
			return nome;
		}

		/**
		 * Restituisce il metodo della response.
		 * 
		 * @return azione
		 */
		public String getAzione() {
			return azione;
		}

		/**
		 * Restituisce lo script JS da eseguire al click.
		 * 
		 * @return onClick
		 */
		public String getOnClick() {
			return onClick;
		}

		/**
		 * Restituisce la coda dove può essere eseguita la response.
		 * 
		 * @return coda
		 */
		public DocumentQueueEnum getCoda() {
			return coda;
		}

		/**
		 * Restituisce l'icona della response.
		 * 
		 * @return icona
		 */
		public String getIcona() {
			return icona;
		}
	}

	/**
	 * Restituisce i masters presenti nel datatable.
	 * 
	 * @return currentMasters
	 */
	public Collection<MasterDocCartaceoRedDTO> getCurrentMasters() {
		Collection<MasterDocCartaceoRedDTO> currentMasters = documentiDTH.getFilteredMasters();

		if (CollectionUtils.isEmpty(currentMasters)) {
			currentMasters = documentiDTH.getMasters();
		}

		return currentMasters;
	}

	/**
	 * Restituisce l'utente.
	 * 
	 * @return utente
	 */
	public final UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Resituisce il datatable helper per i documenti cartacei.
	 * 
	 * @return documentiDTH
	 */
	public SimpleDetailDataTableHelper<MasterDocCartaceoRedDTO> getDocumentiDTH() {
		return documentiDTH;
	}

	/**
	 * Imposta il datatable helper per i documenti cartacei.
	 * 
	 * @param documentiDTH
	 */
	public void setDocumentiDTH(final SimpleDetailDataTableHelper<MasterDocCartaceoRedDTO> documentiDTH) {
		this.documentiDTH = documentiDTH;
	}

	/**
	 * Restituisce la lista di colonne del datatable in base alla loro
	 * visualizzazione. (true -> colonna visibile; false -> colonna nascosta)
	 * 
	 * @return columns
	 */
	public List<Boolean> getColumns() {
		return columns;
	}

	/**
	 * Definisce la visualizzazione delle colonne del datatable in base al booleano
	 * corrispondente nella lista.
	 * 
	 * @param columns
	 */
	public void setColumns(final List<Boolean> columns) {
		this.columns = columns;
	}

	/**
	 * Resituisce il nome della coda corrente.
	 * 
	 * @return the nomeCodaAttuale
	 */
	public final String getNomeCodaAttuale() {
		return nomeCodaAttuale;
	}

	/**
	 * Imposta il nome della coda corrente.
	 * 
	 * @param nomeCodaAttuale
	 */
	public final void setNomeCodaAttuale(final String nomeCodaAttuale) {
		this.nomeCodaAttuale = nomeCodaAttuale;
	}

	/**
	 * Resituisce il componente della preview.
	 * 
	 * @return previewComponent
	 */
	public DetailPreviewIntroComponent getPreviewComponent() {
		return previewComponent;
	}

	/**
	 * Restituisce il valore della label "caricati".
	 * 
	 * @return labelCaricatiValue
	 */
	public String getLabelCaricatiValue() {
		return labelCaricatiValue;
	}

	/**
	 * Restituisce il componente di visulizzazione di un documento cartaceo.
	 * 
	 * @return the visualizzaCartaceoComponent
	 */
	public IVisualizzaDocumentoComponent getVisualizzaCartaceoComponent() {
		return visualizzaCartaceoComponent;
	}

	/**
	 * Restituisce il dettaglio del documento cartaceo.
	 * 
	 * @return detailCartaceo
	 */
	public DetailDocumentRedDTO getDetailCartaceo() {
		return detailCartaceo;
	}

	/**
	 * Restituisce l'id del documento selezionato per la response.
	 * 
	 * @return idDocSelectedForResponse
	 */
	public String getIdDocSelectedForResponse() {
		return idDocSelectedForResponse;
	}

	/**
	 * Imposta l'id del documento selezionato per la response.
	 * 
	 * @param idDocSelectedForResponse
	 */
	public void setIdDocSelectedForResponse(final String idDocSelectedForResponse) {
		this.idDocSelectedForResponse = idDocSelectedForResponse;
	}

	/**
	 * Restituisce la coda corrente.
	 * 
	 * @return the codaAttuale
	 */
	public DocumentQueueEnum getCodaAttuale() {
		return codaAttuale;
	}

	/**
	 * Restituisce l'esito dell'operazione.
	 * 
	 * @return the esitoOperazione
	 */
	public EsitoOperazioneDTO getEsitoOperazione() {
		return esitoOperazione;
	}

	/**
	 * Restituisce il nome della response selezionata.
	 * 
	 * @return responseSelezionata
	 */
	public String getResponseSelezionataLabel() {
		return responseSelezionataLabel;
	}
}