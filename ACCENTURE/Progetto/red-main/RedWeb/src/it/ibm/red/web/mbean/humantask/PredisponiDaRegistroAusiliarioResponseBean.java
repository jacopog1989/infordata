package it.ibm.red.web.mbean.humantask;

import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.RegistroDTO;
import it.ibm.red.business.enums.TipoRegistroAusiliarioEnum;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.helper.PredisponiDocumentoHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractPredisponiDaRegistroAusiliarioBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * Classe PredisponiDaRegistroAusiliarioResponseBean.
 */
@Named(ConstantsWeb.MBean.PREDISPONI_DA_REGISTRO_AUSILIARIO_RESPONSE_BEAN)
@ViewScoped
public class PredisponiDaRegistroAusiliarioResponseBean extends AbstractPredisponiDaRegistroAusiliarioBean implements InitHumanTaskInterface {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 6738524791130627936L;

	/**
	 * Documento.
	 */
	private MasterDocumentRedDTO documento;
	
	/**
	 * Lista documenti Bean.
	 */
	private ListaDocumentiBean ldb;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		super.init();
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
	}


	/**
	 * Metodo che viene chiamato lato front end per effettuare l'operazione di predisponi.
	 */
	@Override
	public void predisponi() {
		final PredisponiDocumentoHelper pdh = new PredisponiDocumentoHelper(documento, null, ldb.getSelectedResponse(), getUtente());
		
		if (StringUtils.isNullOrEmpty(getNomeFile())) {
			showWarnMessage("Il nome del documento è obbligatorio");
		} else {
			// Si genera il content, perché potrebbe essere non allineato alle ultime modifiche effettuate in maschera
			aggiornaAnteprima();
			//se aggiornaAntemprima va in errore, non visualizzo la preview
			if (!isVisualizzaAnteprima()) {
				return;
			}
			
			// ### Si predispone il documento in uscita con il content appena creato
			pdh.predisponiUscita(getDocPrincipale(), getIdRegistro(), getListaMetadatiRegistro(), getMetadatiForDocUscita());
			
			if (pdh.getEsitoPredisposizione().isEsito()) {
				// Si nasconde la dialog della response
				FacesHelper.executeJS("PF('dlgGeneric').hide()");
				
				ldb.destroyBeanViewScope(MBean.PREDISPONI_DA_REGISTRO_AUSILIARIO_RESPONSE_BEAN);
				
				// Si procede all'apertura della dialog di creazione del documento predisposto
				pdh.apriDialogCreazioneDocPredisposto();
			} else {
				showError(pdh.getEsitoPredisposizione().getNote());
			}
		}
	}
	
	/**
	 * Aggiorna anteprima.
	 */
	public void aggiornaAnteprima() {
		aggiornaAnteprima(true);
	}

	/**
	 * Chiudi.
	 */
	@Override
	public void chiudi() {
		ldb.destroyBeanViewScope(MBean.PREDISPONI_DA_REGISTRO_AUSILIARIO_RESPONSE_BEAN);
	}
	
	/**
	 * Metodo che permette di inizializzare il bean
	 *
	 * @param inDocsSelezionati the in docs selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		// Ottengo documento selezionato dalla coda
		documento = inDocsSelezionati.iterator().next();
		setDocumentTitleIngresso(documento.getDocumentTitle());
		String nomeFile = null;
		
		// Ottengo i registri di tipo VISTO o OSSERVAZIONE o RICHIESTA INTEGRAZIONI relativi al documento selezionato e alla response selezionata
		Collection<RegistroDTO> ts = null;
		switch (ldb.getSelectedResponse()) {
		case PREDISPONI_VISTO:
			ts = getRegistroAusiliarioSRV().getRegistri(TipoRegistroAusiliarioEnum.VISTO, documento.getIdTipologiaDocumento(), documento.getIdTipoProcedimento());
			nomeFile = generateNomeTemplate("VIS", documento.getNumeroProtocollo());
			setNomeFile(nomeFile);
			break;
		case PREDISPONI_OSSERVAZIONE:
			ts = getRegistroAusiliarioSRV().getRegistri(TipoRegistroAusiliarioEnum.OSSERVAZIONE_RILIEVO, documento.getIdTipologiaDocumento(), documento.getIdTipoProcedimento());
			nomeFile = generateNomeTemplate("OSS", documento.getNumeroProtocollo());
			setNomeFile(nomeFile);
			break;
		case RICHIESTA_INTEGRAZIONI:
			ts = getRegistroAusiliarioSRV().getRegistri(TipoRegistroAusiliarioEnum.RICHIESTA_ULTERIORI_INFORMAZIONI, documento.getIdTipologiaDocumento(), documento.getIdTipoProcedimento());
			nomeFile = generateNomeTemplate("R_INT", documento.getNumeroProtocollo());
			setNomeFile(nomeFile);
			break;
		case PREDISPONI_RESTITUZIONE:
			ts = getRegistroAusiliarioSRV().getRegistri(TipoRegistroAusiliarioEnum.RESTITUZIONI, documento.getIdTipologiaDocumento(), documento.getIdTipoProcedimento());
			nomeFile = generateNomeTemplate("RES", documento.getNumeroProtocollo());
			setNomeFile(nomeFile);
			break;
		case PREDISPONI_RELAZIONE_POSITIVA:
			ts = getRegistroAusiliarioSRV().getRegistri(TipoRegistroAusiliarioEnum.VISTO, documento.getIdTipologiaDocumento(), documento.getIdTipoProcedimento());
			nomeFile = generateNomeTemplate("R_POS", documento.getNumeroProtocollo());
			setNomeFile(nomeFile);
			break;
		case PREDISPONI_RELAZIONE_NEGATIVA:
			ts = getRegistroAusiliarioSRV().getRegistri(TipoRegistroAusiliarioEnum.OSSERVAZIONE_RILIEVO, documento.getIdTipologiaDocumento(), documento.getIdTipoProcedimento());
			nomeFile = generateNomeTemplate("R_NEG", documento.getNumeroProtocollo());
			setNomeFile(nomeFile);
			break;
		default:
			break;
		}
		
		super.initListaRegistri(ts, null);
	}

}