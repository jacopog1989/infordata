package it.ibm.red.web.mbean.interfaces;

/**
 * Interfaccia usata dai dettagli per gestire lo stato dell'intro preview. </br>
 * l'intro preview è un meccanismo per cui viene caricato solo il preview del documento e quando l'utente clicca su un qualsiasi altro elemento del dettaglio, viene caricato il dettaglio per intero.
 * </br> in questo modo il caricamento "pesante" del dettaglio avviene on demand. 
 * 
 */
public interface IDettaglioLoadableAfterPreview {
	/**
	 * Verifica che la preview di intro sia caricata
	 * Se a true il dettaglio non è ancora stato completamento caricato
	 */
	boolean isIntroPreviewActive();
	
	
	/**
	 * Appena visualizzato il dettaglio viene caricata solo la preview. </br>
	 * In questo caso questo è necessario caricare il dettaglio per completo invocando il servizio di back end che popola tutti i dati necessari agli east buttons e al tabView.
	 * 
	 * Questo metodo verifica che {@link IGoToCodaButtonComponent.isIntroPreviewActive} sia true e quindi esegue le chiamate per popolare il detail 
	 */
	void loadAfterIntroPreview();
}
