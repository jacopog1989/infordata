package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.service.facade.IRichiesteVistoFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractTreeBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;
import it.ibm.red.web.mbean.interfaces.OrganigrammaRetriever;

/**
 * Bean che gestisce la response di richiesta visti.
 */
@Named(ConstantsWeb.MBean.RICHIEDI_VISTI_BEAN)
@ViewScoped
public class RichiediVistiBean extends AbstractTreeBean implements OrganigrammaRetriever, InitHumanTaskInterface {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 5737950570371229300L;

	/**
	 * Logger del Bean.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RichiediVistiBean.class);

	/**
	 * Servizio.
	 */
	private IRichiesteVistoFacadeSRV vistiSRV;

	/**
	 * Servizio.
	 */
	private IOrganigrammaFacadeSRV organigrammaSRV;

	/**
	 * Lista documenti bean.
	 */
	private ListaDocumentiBean ldb;

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Docunenti selezionati.
	 */
	private List<MasterDocumentRedDTO> selectedDocumentList;

	/**
	 * Bean assegnatari.
	 */
	private MultiAssegnazioneBean assegnatariBean;

	/**
	 * Input proveniente dalla maschera.
	 */
	private String motivoAssegnazione;

	/** LABERATURA DA ESPLODERE START***********************************************************************************/
	private List<Nodo> alberaturaNodi;
	/** LABERATURA DA ESPLODERE END*************************************************************************************/

	@PostConstruct
	@Override
	protected void postConstruct() {
		final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
		utente = sb.getUtente();
		organigrammaSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
		alberaturaNodi = organigrammaSRV.getAlberaturaBottomUp(utente.getIdAoo(), utente.getIdUfficio());

		vistiSRV = ApplicationContextProvider.getApplicationContext().getBean(IRichiesteVistoFacadeSRV.class);
		assegnatariBean = new MultiAssegnazioneBean();
		assegnatariBean.initialize(this, TipoAssegnazioneEnum.VISTO);
	}

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		selectedDocumentList = inDocsSelezionati;
	}	

	/**
	 * Si occupa del caricamento dell'organigramma restituendone la root.
	 */
	@Override
	public TreeNode loadRootOrganigramma() {	
		DefaultTreeNode root = null;
		try {
			final List<Long> alberatura = new ArrayList<>();
			for (int i = alberaturaNodi.size() - 1; i >= 0; i--) {
				alberatura.add(alberaturaNodi.get(i).getIdNodo());
			}

			//prendo da DB i nodi di primo livello
			final NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
			nodoRadice.setIdAOO(utente.getIdAoo());
			nodoRadice.setIdNodo(utente.getIdUfficio());
			nodoRadice.setCodiceAOO(utente.getCodiceAoo());
			nodoRadice.setUtentiVisible(true);
			nodoRadice.setDescrizioneNodo(utente.getNodoDesc());
			final List<NodoOrganigrammaDTO> primoLivello = organigrammaSRV.getFigliUfficioAssegnatarioPerVisto(utente.getIdUfficio(), nodoRadice);

			root = new DefaultTreeNode();
			root.setExpanded(true);
			root.setSelectable(false);

			// in questo modo riesco anche a visualizzare il nodo ufficio
			final DefaultTreeNode nodoUfficio = new DefaultTreeNode(nodoRadice, root);
			nodoUfficio.setExpanded(true);
			nodoUfficio.setSelectable(false); 

			//per ogni nodo di primo livello creo il nodo da mettere nell'albero che ha come padre rootAssegnazionePerCompetenza
			List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
			DefaultTreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(utente.getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, nodoUfficio);
				nodoToAdd.setExpanded(true);
				nodoToAdd.setSelectable(true);
				if (n.getIdUtente() == null && alberatura.contains(n.getIdNodo())) {
					nodiDaAprire.add(nodoToAdd);
				}
			}

			nodiDaAprire(alberatura, nodiDaAprire);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}

		return root;
	}

	/**
	 * Gestisce l'apertura del tree.
	 */
	@Override
	public void onOpenTree(final TreeNode treeNode) {
		try {
			final List<NodoOrganigrammaDTO> figli = organigrammaSRV.getFigliUfficioAssegnatarioPerVisto(utente.getIdUfficio(), (NodoOrganigrammaDTO) treeNode.getData());
			
			super.onOpenTree(treeNode, figli);
			
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}
	}

	/**
	 * Valida i campi assegnatari ed esegue la logica della response.
	 */
	public void checkEResponse() {
		final List<AssegnazioneDTO> assegnatari = assegnatariBean.getAssegnazioneList();
		if (!assegnatari.isEmpty()) {
			doResponse();
			FacesHelper.executeJS("PF('dlgGeneric').hide()");
			FacesHelper.executeJS("PF('dlgShowResult').show()");
			FacesHelper.update("centralSectionForm:idDlgShowResult");

		} else {
			showWarnMessage("Per continuare è necessario aggiungere almeno un assegnatario per visto");
		}
	}

	/**
	 * Esegue la logica di business associata alla response selezionata.
	 */
	public void doResponse() {
		//pulisco gli esiti
		ldb.cleanEsiti();

		final List<String> wobNumbers = new ArrayList<>();

		for (final MasterDocumentRedDTO master : selectedDocumentList) {
			final String wobNumber = master.getWobNumber();
			if (!StringUtils.isEmpty(wobNumber)) {
				wobNumbers.add(wobNumber);
			}
		}
		if (CollectionUtils.isEmpty(wobNumbers)) {
			LOGGER.error("Non è stato selezionato nessun documento per l'esecuzione della response" + ldb.getSelectedResponse().getResponse());
			showError("Nessun documento selezionato per l'esecuzione della response");
		} else {
			final List<AssegnazioneDTO> assegnatari = assegnatariBean.getAssegnazioneList();

			EsitoOperazioneDTO esito; 
			if (CollectionUtils.isEmpty(assegnatari)) {
				esito = new EsitoOperazioneDTO(null, false, "Per continuare è necessario aggiungere almeno un assegnatario per conoscenza");
				ldb.getEsitiOperazione().add(esito);
			} else {
				for (final String wobNumber : wobNumbers) {					
					final EsitoOperazioneDTO eisto = vistiSRV.richiediVisti(utente, wobNumber, motivoAssegnazione, assegnatari);
					ldb.getEsitiOperazione().add(eisto);
				}
			}
		}
		destroyBean();
	}

	/**
	 * Esegue la distruzione del bean liberando le risorse occupate.
	 */
	public void destroyBean() {
		ldb.destroyBeanViewScope(ConstantsWeb.MBean.RICHIEDI_VISTI_BEAN);
	}

	/**
	 * Restituisce l'utente.
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'utente.
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Restituisce il motivo assegnazione.
	 * @return motivo assegnazione
	 */
	public String getMotivoAssegnazione() {
		return motivoAssegnazione;
	}

	/**
	 * Imposta il motivo dell'assegnazione.
	 * @param motivoAssegnazione
	 */
	public void setMotivoAssegnazione(final String motivoAssegnazione) {
		this.motivoAssegnazione = motivoAssegnazione;
	}

	/**
	 * Restituisce il bean per le assegnazioni multiple.
	 * @return bean assegnazioni multiple
	 */
	public MultiAssegnazioneBean getAssegnatariBean() {
		return assegnatariBean;
	}

}
