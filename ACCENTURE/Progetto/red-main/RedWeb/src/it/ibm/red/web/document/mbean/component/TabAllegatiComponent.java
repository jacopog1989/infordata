package it.ibm.red.web.document.mbean.component;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EmptyFileException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentManagerDTO;
import it.ibm.red.business.dto.EsitoDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV;
import it.ibm.red.web.document.mbean.DocumentTabAllegatiAbstract;
import it.ibm.red.web.document.mbean.interfaces.ITabAllegatiBean;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Component tab allegati.
 */
public class TabAllegatiComponent extends DocumentTabAllegatiAbstract implements ITabAllegatiBean {

	private static final String ERRORE_DURANTE_LA_VERIFICA_DEL_DOCUMENTO = "Errore durante la verifica del documento";

	/**
	 * La costante serial version UID.
	 */
	private static final long serialVersionUID = -8744390766627538212L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TabAllegatiComponent.class.getName());

	/**
	 * Dati gestione documento.
	 */
	private final DocumentManagerDTO documentManagerDTO;

	/**
	 * Lista allegati.
	 */
	private List<AllegatoDTO> allegati;

	/**
	 * Dettaglio documento.
	 */
	private final DetailDocumentRedDTO detail;

	/**
	 * Flag download.
	 */
	private final Boolean flagNotDownload;

	/**
	 * Indice.
	 */
	private Integer indexTableItem;

	/**
	 * File.
	 */
	private transient UploadedFile uploadedFile;

	/**
	 * Informazioni utente.
	 */
	private final UtenteDTO utente;

	/**
	 * Lista allegati.
	 */
	private List<FileDTO> attachForSignFieldPreview;

	/**
	 * Flag anteprima allegati.
	 */
	private Boolean flagAnteprimaAllegati;

	/**
	 * Flag mantieni allegati originali.
	 */
	private final boolean mantieniFormatoOriginale;

	/**
	 * Costruttore di default.
	 * 
	 * @param documentManagerDTO
	 * @param allegatiIn
	 * @param detail
	 * @param utente
	 */
	public TabAllegatiComponent(final DocumentManagerDTO documentManagerDTO, final List<AllegatoDTO> allegatiIn, final DetailDocumentRedDTO detail, final UtenteDTO utente) {
		this.documentManagerDTO = documentManagerDTO;
		this.detail = detail;
		this.flagNotDownload = true;
		this.utente = utente;
		this.attachForSignFieldPreview = new ArrayList<>();
		flagAnteprimaAllegati = false;

		mantieniFormatoOriginale = (documentManagerDTO.isInCreazioneIngresso() || documentManagerDTO.isInModificaIngresso()) && utente.getMantieniFormatoOriginale() == 1;

		if (allegatiIn != null) {
			this.allegati = new ArrayList<>();
			for (final AllegatoDTO allegatoDTO : allegatiIn) {
				this.allegati.add(new AllegatoDTO(allegatoDTO));
			}
		}

		// se gli allegati non sono vuoti bisogna riportare i valori
		if (this.allegati != null && !this.allegati.isEmpty()) {
			for (int i = 0; i < this.allegati.size(); i++) {
				// l'on change mi serve per preparare le visibilità dei componenti dell'allegato
				onChangeComboFormatoAllegato(i);

				// ma poi devo rimettere i valori in input
				final AllegatoDTO al = this.allegati.get(i);

				if (allegatiIn != null) {
					if (!al.isMantieniFormatoOriginale()) {
						al.setFormatoOriginale(allegatiIn.get(i).getFormatoOriginale());
					}
					al.setDaFirmareBoolean(allegatiIn.get(i).getDaFirmare() != null && allegatiIn.get(i).getDaFirmare().intValue() > 0);
					al.setDaFirmare(allegatiIn.get(i).getDaFirmare());

					al.setCopiaConforme(allegatiIn.get(i).getCopiaConforme());
					al.setIdCopiaConforme(allegatiIn.get(i).getIdCopiaConforme());
					al.setIdUfficioCopiaConforme(allegatiIn.get(i).getIdUfficioCopiaConforme());
					al.setIdUtenteCopiaConforme(allegatiIn.get(i).getIdUtenteCopiaConforme());

					al.setNomeFile(allegatiIn.get(i).getNomeFile());
					al.setMimeType(allegatiIn.get(i).getMimeType());
					al.setContent(allegatiIn.get(i).getContent());
					al.setContentSize(allegatiIn.get(i).getContentSize());

					al.setPosizione(allegatiIn.get(i).getPosizione());
					al.setBarcode(allegatiIn.get(i).getBarcode());

					al.setComboTipologieDocumentoAllegati(allegatiIn.get(i).getComboTipologieDocumentoAllegati());
					al.setFileNonSbustato(allegatiIn.get(i).getFileNonSbustato());

					if (allegatiIn.get(i).getPlaceholderSigla() != null) {
						al.setPlaceholderSigla(allegatiIn.get(i).getPlaceholderSigla());
					}

					al.setCheckStampigliaturaSiglaVisible(allegatiIn.get(i).getCheckStampigliaturaSiglaVisible());
					al.setStampigliaturaSigla(allegatiIn.get(i).getStampigliaturaSigla());

					if (allegatiIn.get(i).getFirmaVisibile() != null) {
						al.setFirmaVisibile(allegatiIn.get(i).getFirmaVisibile());
					}

				}

				al.setInModifica(this.documentManagerDTO.isInModifica());
			}
		}
	}

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.ITabAllegatiBean#rimuoviAllegato(java.lang.Object).
	 */
	@Override
	public final void rimuoviAllegato(final Object index) {
		final AllegatoDTO rimosso = super.rimuoviAllegato(index, this.allegati);
		if ((documentManagerDTO.isInCreazioneUscita() || documentManagerDTO.isInModificaUscita()) && rimosso.getContentSize() != null) {
			final float sizeMB = this.detail.getSizeContent() - rimosso.getContentSize();
			final String sizeMBRound = String.format("%.2f", sizeMB).replace(",", ".");
			this.detail.setSizeContent(Float.parseFloat(sizeMBRound));
		}
	}

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.ITabAllegatiBean#addAllegato().
	 */
	@Override
	public final void addAllegato() {
		if (this.allegati == null) {
			this.allegati = new ArrayList<>();
		}
		super.addAllegato(this.allegati, this.documentManagerDTO, mantieniFormatoOriginale, utente.getTimbroUscitaAoo());
	}

	/**
	 * @see it.ibm.red.web.document.mbean.DocumentTabAllegatiAbstract#addAllegatoCartaceo().
	 */
	@Override
	public final void addAllegatoCartaceo() {
		if (this.allegati == null) {
			this.allegati = new ArrayList<>();
		}
		super.addAllegatoCartaceo(this.allegati, this.documentManagerDTO, mantieniFormatoOriginale);
	}

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.ITabAllegatiBean#handleFileUploadAllegato(org.primefaces.event.FileUploadEvent).
	 */
	@Override
	public final void handleFileUploadAllegato(FileUploadEvent event) {
		boolean isValido = true;

		try {
			event = checkFileName(event);
			if (event == null) {
				return;
			}
			uploadedFile = event.getFile();

			if ((documentManagerDTO.isInCreazioneIngresso() || documentManagerDTO.isInModificaIngresso()) && utente.getMantieniFormatoOriginale() != 1
					&& ContentType.PDF.equals(uploadedFile.getContentType())) {
				isValido = PdfHelper.checkValidaPDF(uploadedFile.getContents());
			}
		} catch (final Exception e) {
			LOGGER.error(ERRORE_DURANTE_LA_VERIFICA_DEL_DOCUMENTO, e);
			showError(ERRORE_DURANTE_LA_VERIFICA_DEL_DOCUMENTO);
			isValido = false;
		}

		indexTableItem = (Integer) event.getComponent().getAttributes().get("index");
		if (indexTableItem >= allegati.size()) {
			throw new RedException("Errore nel caricamento del file");
		}

		if (isValido) {
			final Float size = super.handleFileUploadAllegato(indexTableItem, uploadedFile, this.allegati, utente, documentManagerDTO, Boolean.FALSE, detail.getSizeContent());
			final String sizeRoundMB = String.format("%.2f", size).replace(",", ".");
			detail.setSizeContent(Float.parseFloat(sizeRoundMB));
		} else {
			FacesHelper.executeJS("PF('wdgConfirmUpload').show()");
		}
	}

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.ITabAllegatiBean#gestisciUploadAllegati(org.primefaces.event.FileUploadEvent).
	 */
	@Override
	public final void gestisciUploadAllegati(final FileUploadEvent event) {
		boolean isValido = true;
		
		// La lista degli allegati è gestita con logica accesso stack, quindi l'ultimo allegato inserito ha sempre indice 0.
		final int testaLista = 0;
		try {

			uploadedFile = event.getFile();
			
			if (uploadedFile.getContents().length == 0) {
				isValido = false;
				throw new EmptyFileException();
			}
			
			if ((documentManagerDTO.isInCreazioneIngresso() || documentManagerDTO.isInModificaIngresso()) 
					&& utente.getMantieniFormatoOriginale() != 1 && ContentType.PDF.equals(uploadedFile.getContentType())) {
				isValido = PdfHelper.checkValidaPDF(uploadedFile.getContents());
			}

			addAllegato();

			final AllegatoDTO allegato = this.allegati.get(testaLista);
			allegato.setNomeFile(uploadedFile.getFileName());
			allegato.setMimeType(uploadedFile.getContentType());
			allegato.setContent(uploadedFile.getContents());

			if ((documentManagerDTO.isInCreazioneUscita() || documentManagerDTO.isInModificaUscita())) {
				final float allegatoSizeMB = this.detail.convertiContentInMB(uploadedFile.getContents().length);
				final String sizeMBAllegatoRound = String.format("%.2f", allegatoSizeMB).replace(",", ".");
				allegato.setContentSize(Float.parseFloat(sizeMBAllegatoRound));

				final float sizeMB = detail.getSizeContent() + this.detail.convertiContentInMB(uploadedFile.getContents().length);
				final String sizeMBRound = String.format("%.2f", sizeMB).replace(",", ".");
				detail.setSizeContent(Float.parseFloat(sizeMBRound));
			}

			if (isValido) {
				EsitoDTO esito = super.handleFileUploadAllegati(uploadedFile, allegato, utente, documentManagerDTO, Boolean.FALSE);
				if(esito.isEsito()) {
					super.onChangeComboFormatoAllegato(testaLista, allegati, documentManagerDTO, mantieniFormatoOriginale, false);
				} else {
					rimuoviAllegato(testaLista);
				}
			} else {
				FacesHelper.executeJS("PF('wdgConfirmUpload').show()");
			}
		} catch (final EmptyFileException emptyFileE) {
			LOGGER.error("Non può essere allegato un file vuoto.", emptyFileE);
			showError("Non può essere allegato un file vuoto.");
			isValido = false;
		} catch (final Exception e) {
			LOGGER.error(ERRORE_DURANTE_LA_VERIFICA_DEL_DOCUMENTO, e);
			showError(ERRORE_DURANTE_LA_VERIFICA_DEL_DOCUMENTO);
		}
		
		LOGGER.error("Esito finale: " + isValido);
	}

	/**
	 * Gestisce l'upload del file allegato.
	 */
	public final void confirmUploadAllegato() {
		super.handleFileUploadAllegato(indexTableItem, uploadedFile, this.allegati, utente, documentManagerDTO, Boolean.TRUE, detail.getSizeContent());
	}

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.ITabAllegatiBean#rimuoviFileAllegato(java.lang.Object).
	 */
	@Override
	public final void rimuoviFileAllegato(final Object index) {
		final Float size = super.rimuoviFileAllegato(index, this.documentManagerDTO, this.allegati, this.detail.getSizeContent());
		this.detail.setSizeContent(size);
	}

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.ITabAllegatiBean#onChangeComboFormatoAllegato(java.lang.Object).
	 */
	@Override
	public final void onChangeComboFormatoAllegato(final Object index) {
		super.onChangeComboFormatoAllegato(index, this.allegati, this.documentManagerDTO, mantieniFormatoOriginale, utente.getTimbroUscitaAoo());
	}

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.ITabAllegatiBean#downloadAllegato(java.lang.Object).
	 */
	@Override
	public StreamedContent downloadAllegato(final Object index) {
		StreamedContent file = null;
		final Integer i = (Integer) index;
		try {
			final IDocumentManagerFacadeSRV documentManagerSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentManagerFacadeSRV.class);
			final AllegatoDTO allegatoSelezionato = allegati.get(i);
			final String mimeType = allegatoSelezionato.getMimeType();
			final String nome = allegatoSelezionato.getNomeFile();
			final String guid = allegatoSelezionato.getGuid();

			final byte[] content = allegatoSelezionato.getContent();

			InputStream stream = null;
			if (content != null && content.length > 0) {
				stream = new ByteArrayInputStream(content);
			} else if (StringUtils.isNotBlank(guid)) {
				stream = documentManagerSRV.getStreamDocumentoByGuid(guid, utente);
			} else {
				throw new RedException("Il content dell'allegato non risulta presente e il guid dell'allegato non è valorizzato.");
			}

			file = new DefaultStreamedContent(stream, mimeType, nome);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il download del file", e);
			showError("Errore durante il download del file");
		}
		return file;
	}

	/**
	 * @see it.ibm.red.web.document.mbean.DocumentTabAllegatiAbstract#onChangeResponsabileCopiaConformeAllegato(java.lang.Integer,
	 *      java.util.List, it.ibm.red.business.dto.DocumentManagerDTO).
	 */
	@Override
	public final void onChangeResponsabileCopiaConformeAllegato(final Integer index, final List<AllegatoDTO> allegati, final DocumentManagerDTO documentManagerDTO) {
		super.onChangeResponsabileCopiaConformeAllegato(index, allegati, documentManagerDTO);
	}

	/**
	 * Gestisce l'evento di modifica del responsabile copia conforme dell'allegato.
	 * 
	 * @param index
	 */
	public final void onChangeResponsabileCopiaConformeAllegato(final Integer index) {
		super.onChangeResponsabileCopiaConformeAllegato(index, allegati, documentManagerDTO);
	}

	/**
	 * Aggiorna il flag daFirmare dell'allegato identificato dalla posizione "index"
	 * nella lista "allegati".
	 * 
	 * @param index
	 */
	public final void clickCheckDaFirmare(final Integer index) {
		super.clickCheckDaFirmare(index, allegati);

	}

	/**
	 * Aggiorna il flag invio copia conforme dell'allegato identificato dalla
	 * posizione "index" nella lista "allegati".
	 * 
	 * @param index
	 */
	public final void clickCheckInvioCopiaConforme(final Integer index) {
		if(detail.getMomentoProtocollazioneEnum().equals(MomentoProtocollazioneEnum.PROTOCOLLAZIONE_NON_STANDARD)) {
			if(Boolean.TRUE.equals(allegati.get(index).getCopiaConforme())) {
				allegati.get(index).setCopiaConforme(false);
				showWarnMessage("Copia conforme non consentita con la protocollazione non standard");
			}
		} else { 
			super.clickCheckInvioCopiaConforme(index, allegati, documentManagerDTO);
		}
	}

	/**
	 * Aggiorna il flag "stampiglia sigla" dell'allegato identificato dalla
	 * posizione "index" nella lista "allegati".
	 * 
	 * @param index
	 */
	public final void clickCheckStampigliaSigla(final Integer index) {
		super.clickCheckStampigliaSigla(index, allegati);
	}

	/**
	 * Aggiorna il flag "Mantieni Formato Originale" dell'allegato identificato
	 * dalla posizione "index" nella lista "allegati".
	 * 
	 * @param index
	 */
	public final void clickCheckMantieniFormatoOriginale(final Integer index) {
		super.clickCheckMantieniFormatoOriginale(index, allegati, utente.getTimbroUscitaAoo());
	}

	/**
	 * Aggiorna il flag "timbro uscita" dell'allegato identificato dalla posizione
	 * "index" nella lista "allegati".
	 * 
	 * @param index
	 */
	public final void clickCheckTimbroUscita(final Integer index) {
		super.clickCheckTimbroUscita(index, allegati);
	}

	/**
	 * Aggiorna il flag "Firma Visibile" dell'allegato identificato dalla posizione
	 * "index" nella lista "allegati".
	 * 
	 * @param index
	 */
	public final void clickCheckFirmaVisibile(final Integer index) {
		super.clickCheckFirmaVisibile(index, allegati);
	}

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.ITabAllegatiBean#getDetail().
	 */
	@Override
	public final DetailDocumentRedDTO getDetail() {
		return detail;
	}

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.ITabAllegatiBean#getAllegati().
	 */
	@Override
	public final List<AllegatoDTO> getAllegati() {
		return this.allegati;

	}

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.ITabAllegatiBean#setAllegati(java.util.List).
	 */
	@Override
	public void setAllegati(final List<AllegatoDTO> allegati) {
		this.allegati = allegati;

	}

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.ITabAllegatiBean#getDocumentManagerDTO().
	 */
	@Override
	public DocumentManagerDTO getDocumentManagerDTO() {
		return documentManagerDTO;
	}

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.ITabAllegatiBean#getFlagNotDownload().
	 */
	@Override
	public Boolean getFlagNotDownload() {
		return flagNotDownload;
	}

	/**
	 * Restituisce l'utente.
	 * 
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Restituisce la lista di allegati per la preview.
	 * 
	 * @return attachForSignFieldPreview
	 */
	public final List<FileDTO> getAttachForSignFieldPreview() {
		return attachForSignFieldPreview;
	}

	/**
	 * Imposta la lista di allegati per la preview.
	 * 
	 * @param attachForSignFieldPreview
	 */
	public final void setAttachForSignFieldPreview(final List<FileDTO> attachForSignFieldPreview) {
		this.attachForSignFieldPreview = attachForSignFieldPreview;
	}

	/**
	 * @return flagAnteprimaAllegati
	 */
	public final Boolean getFlagAnteprimaAllegati() {
		return flagAnteprimaAllegati;
	}

	/**
	 * Imposta il flag {@link #flagAnteprimaAllegati}
	 * 
	 * @param flagAnteprimaAllegati
	 */
	public final void setFlagAnteprimaAllegati(final Boolean flagAnteprimaAllegati) {
		this.flagAnteprimaAllegati = flagAnteprimaAllegati;
	}

}
