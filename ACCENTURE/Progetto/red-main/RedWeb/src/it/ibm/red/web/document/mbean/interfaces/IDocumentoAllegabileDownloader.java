package it.ibm.red.web.document.mbean.interfaces;

import org.primefaces.model.StreamedContent;

import it.ibm.red.business.dto.DocumentoAllegabileDTO;

/**
 * Downloader documento allegabile.
 */
public interface IDocumentoAllegabileDownloader {
	/**
	 * Permette di eseguire il download di un singolo documento allegabile.
	 * 
	 * @param d		documento allegabile proveniente dalla tabella.
	 * @return		Lo streamed content contentenete il contenuto del documento.
	 */
	StreamedContent download(DocumentoAllegabileDTO d);
}
