/**
 * 
 */
package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ColoreNotaEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.INotaFacadeSRV;
import it.ibm.red.business.service.facade.IOperationDocumentFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * @author APerquoti
 *
 */
@Named(ConstantsWeb.MBean.RIFIUTA_BEAN)
@ViewScoped
public class RifiutoBean extends AbstractBean implements InitHumanTaskInterface {

	private static final long serialVersionUID = -4941766184882152694L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RifiutoBean.class.getName());
	
	/**
	 * Motivo rifiuto.
	 */
	private String motivoRifiuto;
	
	/**
	 * Lista documenti.
	 */
	private List<MasterDocumentRedDTO> masters;
	
	/**
	 * Lista documenti bean.
	 */
	private ListaDocumentiBean ldb;
	
	/**
	 * Flag motivo rifiuto obbligatorio.
	 */
	private boolean motivoRifiutoObbligatorio;
	
	/**
	 * Flag storico.
	 */
	private boolean alsoStorico;
	 
	
	@Override
	protected void postConstruct() {
		// Non deve fare nulla
	}

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		masters = inDocsSelezionati;
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
		
		if (ResponsesRedEnum.RIFIUTATA.equals(ldb.getSelectedResponse()) 
				|| ResponsesRedEnum.ANNULLA_OSSERVAZIONE.equals(ldb.getSelectedResponse()) 
				|| ResponsesRedEnum.ANNULLA_RICHIESTA_INTEGRAZIONI.equals(ldb.getSelectedResponse()) 
				|| ResponsesRedEnum.ANNULLA_VISTO.equals(ldb.getSelectedResponse())
				|| ResponsesRedEnum.ANNULLA_RELAZIONE_POSITIVA.equals(ldb.getSelectedResponse())
				|| ResponsesRedEnum.ANNULLA_RELAZIONE_NEGATIVA.equals(ldb.getSelectedResponse())) {
			motivoRifiutoObbligatorio = true;
		}
		
	}

	/**
	 * Esegue la logica della response "Rifiuta", a monte esegue una validazione mostrando eventuali errori.
	 * A valle aggiorna gli esiti di {@link #ldb} per la corretta visualizzazione.
	 */
	public void rifiutaResponse() {
		try {
			if (motivoRifiutoObbligatorio && StringUtils.isNullOrEmpty(motivoRifiuto)) {
				showError("La motivazione del rifiuto è obbligatoria");
			} else {
				final Collection<String> wobNumbers = new ArrayList<>();
				final IOperationDocumentFacadeSRV opeDocumentSRV = ApplicationContextProvider.getApplicationContext().getBean(IOperationDocumentFacadeSRV.class);
				final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
				final UtenteDTO u = sb.getUtente();
				
				// Si puliscono gli esiti
				ldb.cleanEsiti();
				
				// Si raccolgono i wobnumber necessari ad invocare il service 
				boolean isOneKindOfCorriere = false;
				for (final MasterDocumentRedDTO m : masters) {
					if (!StringUtils.isNullOrEmpty(m.getWobNumber())) {
						isOneKindOfCorriere = DocumentQueueEnum.isOneKindOfCorriere(m.getQueue());
						wobNumbers.add(m.getWobNumber());
					}
				}
				
				// Invoke service
				final Collection<EsitoOperazioneDTO> eOpe = opeDocumentSRV.rifiuta(u, wobNumbers, motivoRifiuto, ldb.getSelectedResponse(), isOneKindOfCorriere);
				
				// Si impostano i nuovi esiti
				ldb.getEsitiOperazione().addAll(eOpe);
				
				if ((!ldb.getEsitiOperazione().isEmpty() && ldb.checkEsiti(false)) && isAlsoStorico()) {
					final INotaFacadeSRV notaSRV = ApplicationContextProvider.getApplicationContext().getBean(INotaFacadeSRV.class);
					for (final MasterDocumentRedDTO m : masters) {
						notaSRV.registraNotaFromDocumento(u, Integer.parseInt(m.getDocumentTitle()), ColoreNotaEnum.NERO, motivoRifiuto);
					}
				}
				
				FacesHelper.executeJS("PF('dlgGeneric').hide()");
				FacesHelper.update("centralSectionForm:idDlgShowResult");
				FacesHelper.executeJS("PF('dlgShowResult').show()");
				
				ldb.destroyBeanViewScope(ConstantsWeb.MBean.RIFIUTA_BEAN);
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'operazione di rifiuto", e);
			showError("Si è verificato un errore durante l'operazione di rifiuto");
		}
	}
	
	
	/**
	 * @return motivoRifiuto
	 */
	public final String getMotivoRifiuto() {
		return motivoRifiuto;
	}

	/**
	 * @param motivoRifiuto
	 */
	public final void setMotivoRifiuto(final String motivoRifiuto) {
		this.motivoRifiuto = motivoRifiuto;
	}

	/**
	 * @return motivoRifiutoObbligatorio
	 */
	public boolean isMotivoRifiutoObbligatorio() {
		return motivoRifiutoObbligatorio;
	}

	/**
	 * @return alsoStorico
	 */
	public boolean isAlsoStorico() {
		return alsoStorico;
	}

	/**
	 * @param alsoStorico
	 */
	public void setAlsoStorico(final boolean alsoStorico) {
		this.alsoStorico = alsoStorico;
	}

}