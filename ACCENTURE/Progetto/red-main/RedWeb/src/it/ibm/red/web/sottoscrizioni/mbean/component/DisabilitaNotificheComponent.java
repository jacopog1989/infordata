package it.ibm.red.web.sottoscrizioni.mbean.component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import it.ibm.red.business.dto.CanaleTrasmissioneDTO;
import it.ibm.red.business.dto.DisabilitazioneNotificheUtenteDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.IntervalloDisabilitazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ISottoscrizioniFacadeSRV;
import it.ibm.red.web.sottoscrizioni.dto.DisabilitaNotificaDTO;

/**
 * Component disabilita notifiche.
 */
public class DisabilitaNotificheComponent implements Serializable {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 2418320403947337952L;
    
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DisabilitaNotificheComponent.class.getName());
    
	/**
	 * Tutti.
	 */
	private static final String CANALE_TUTTI = "Tutti";
    
	/**
	 * Posta elettronica.
	 */
	private static final String CANALE_POSTA = "Posta elettronica";
    
	/**
	 * red.
	 */
	private static final String CANALE_RED = "Notifica RED";
	
	/**
	 * Utente.
	 */
	private final UtenteDTO utente;
    
	/**
	 * Servizio.
	 */
	private final ISottoscrizioniFacadeSRV sottoscrizioniSRV;
	
	/**
	 * Canali.
	 */
	private Map<Integer, CanaleTrasmissioneDTO> id2Canale;
	
	/**
	 * Posta.
	 */
	private DisabilitaNotificaDTO posta;
	
    
	/**
	 * Red.
	 */
	private DisabilitaNotificaDTO applicativa;

	/**
	 * Costruttore del component.
	 * @param utente
	 */
	public DisabilitaNotificheComponent(final UtenteDTO utente) {
		this.utente = utente;
		this.sottoscrizioniSRV = ApplicationContextProvider.getApplicationContext().getBean(ISottoscrizioniFacadeSRV.class);
		
		final Collection<DisabilitazioneNotificheUtenteDTO> notifiche = this.sottoscrizioniSRV.loadDisabilitazioniNotificheUtente(utente);
		final Collection<CanaleTrasmissioneDTO> canaleTrasmissioneList = this.sottoscrizioniSRV.loadCanaliTrasmissione();
		inizializzaNotifiche(notifiche, canaleTrasmissioneList);
	}
	
	private void inizializzaNotifiche(final Collection<DisabilitazioneNotificheUtenteDTO> notificaList, final Collection<CanaleTrasmissioneDTO> canaleTrasmissioneList) {
		
		id2Canale = new HashMap<>(); 
		for (final CanaleTrasmissioneDTO canale: canaleTrasmissioneList) {
			if (!CANALE_TUTTI.equals(canale.getNome())) {
				id2Canale.put(canale.getId(), canale);
			}
		}
		
		this.posta = new DisabilitaNotificaDTO();
		this.applicativa = new DisabilitaNotificaDTO();
		
		for (final DisabilitazioneNotificheUtenteDTO notifica : notificaList) {
			DisabilitaNotificaDTO current = null;
			final CanaleTrasmissioneDTO canale = id2Canale.get(notifica.getIdCanaleTrasmissione());
			if (CANALE_POSTA.equals(canale.getNome())) {
				current =  this.posta;
			} else if (CANALE_RED.equals(canale.getNome())) {
				current = this.applicativa;
			}
			
			if (current != null) {
				current.setDataA(notifica.getDataA());
				current.setDataDa(notifica.getDataDa());
				final IntervalloDisabilitazioneEnum intervallo = IntervalloDisabilitazioneEnum.values()[notifica.getIntervalloDisabilitazione().getId()];
				current.setIntervallo(intervallo);
			}
		}
	}

	/**
	 * Gestisce la sottoscrizione delle disabilitazioni.
	 */
	public void registra() {
		final Collection<DisabilitazioneNotificheUtenteDTO> disabilitazioniNotificheUtente = new ArrayList<>();
		final DisabilitazioneNotificheUtenteDTO notificaPosta = convert(this.posta, CANALE_POSTA);
		if (notificaPosta != null) {
			disabilitazioniNotificheUtente.add(notificaPosta);
		}
		final DisabilitazioneNotificheUtenteDTO notificaRed = convert(this.applicativa, CANALE_RED);
		if (notificaRed != null) {
			disabilitazioniNotificheUtente.add(notificaRed);
		}
		if (!this.sottoscrizioniSRV.registraDisabilitazioneNotificheUtente(utente, disabilitazioniNotificheUtente)) {
			throw new RedException("Errore durante la sottoscrizione delle disabilitazioni");
		}
	}
	
	private DisabilitazioneNotificheUtenteDTO convert(final DisabilitaNotificaDTO notifica, final String canale) {
		
		if (notifica.getIntervallo() == IntervalloDisabilitazioneEnum.MAI) {
			return null;
		} else  if (notifica.getIntervallo() == IntervalloDisabilitazioneEnum.INTERVALLO) {
			validateIntervallo(notifica);
		}
		//recupero l'idCanaleTrasmissione
		int idCanaleTrasmissione = 0;
		final Optional<CanaleTrasmissioneDTO> canaleCurrent = id2Canale.values().stream()
			.filter(c -> canale.equals(c.getNome()))
			.findAny();
		if (canaleCurrent.isPresent()) {
			idCanaleTrasmissione = canaleCurrent.get().getId();
		} else {
			LOGGER.error("Il canale di trasmissione presente sul db non corrisponde al canale di trasmissione descritto nelle costanti");
			throw new RedException("Errore durante la sottoscrizione delle disabilitazioni: impossibile individuare il canale di trasmisisone");
		}
		
		final DisabilitazioneNotificheUtenteDTO dto = new DisabilitazioneNotificheUtenteDTO();
		dto.setDataA(notifica.getDataA());
		dto.setDataDa(notifica.getDataDa());
		dto.setIdUtente(utente.getId().toString());
		dto.setIntervalloDisabilitazione(notifica.getIntervallo().getIntervalloDisabilitazioneDTO());
		dto.setIdCanaleTrasmissione(idCanaleTrasmissione);
		dto.setIdUtente(utente.getId().toString());
		return dto;
	}
	
	/**
	 * Intervallo "da" deve essere inferiore a intervallo "a"
	 * Intervallo da non può essere null
	 * Se intervallo a è null è considerato intervallo da.
	 */
	private void validateIntervallo(final DisabilitaNotificaDTO notifica) {
		String msg = "Errore durante la sottoscrizione delle disabilitazioni: ";
		if (notifica.getDataDa() == null) {
			throw new RedException(msg + "intervallo di tempo non valorizzato");
		}
		
		if (notifica.getDataA() == null) {
			notifica.setDataDa(notifica.getDataDa());
		}

		if (notifica.getDataA().compareTo(notifica.getDataDa()) < 0) {
			msg = "non sono stati scelti intervalli consecutivi";
			throw new RedException(msg);
		}
	}

	/**
	 * @return posta
	 */
	public DisabilitaNotificaDTO getPosta() {
		return posta;
	}

	/**
	 * @param posta
	 */
	public void setPosta(final DisabilitaNotificaDTO posta) {
		this.posta = posta;
	}

	/**
	 * @return applicativa
	 */
	public DisabilitaNotificaDTO getApplicativa() {
		return applicativa;
	}

	/**
	 * @param applicativa
	 */
	public void setApplicativa(final DisabilitaNotificaDTO applicativa) {
		this.applicativa = applicativa;
	}
	
}
