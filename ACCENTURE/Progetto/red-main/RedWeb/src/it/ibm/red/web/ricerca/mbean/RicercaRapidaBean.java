/**
 * 
 */
package it.ibm.red.web.ricerca.mbean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;

import it.ibm.red.business.dto.DetailFascicoloRedDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.RicercaResultDTO;
import it.ibm.red.business.dto.RicercaVeloceRequestDTO;
import it.ibm.red.business.dto.StatoRicercaDTO;
import it.ibm.red.business.enums.RicercaPerBusinessEntityEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.DettaglioFascicoloRicercaBean;
import it.ibm.red.web.mbean.interfaces.IUpdatableDetailCaller;

/**
 * @author APerquoti
 *
 */
@Named(ConstantsWeb.MBean.RICERCA_RAPIDA_BEAN)
@ViewScoped
public class RicercaRapidaBean extends RicercaDocumentiRedAbstractBean implements IUpdatableDetailCaller<DetailFascicoloRedDTO> {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -3823439775704729475L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaRapidaBean.class.getName());
	
	/**
	 * Indice.
	 */
	private static final String PRIMO_SELECT = "0";

	/**
	 * Indice.
	 */
	private static final String SECONDO_SELECT = "1";
	
	/**
	 * Risultati.
	 */
	private RicercaResultDTO searchResult;

	/**
	 * Parametri ricerca.
	 */
	private RicercaVeloceRequestDTO formRicerca;
	
	/**
	 * Documenti.
	 */
	protected SimpleDetailDataTableHelper<MasterDocumentRedDTO> documentiDTH;

	/**
	 * Fascicoli.
	 */
	protected SimpleDetailDataTableHelper<FascicoloDTO> fascicoliDTH;
	
	/**
	 * Flag detail.
	 */
	private Boolean toggleDetail;

	/**
	 * Colonne documenti.
	 */
	private List<Boolean> docColumns;

	/**
	 * Colonne fascicoli.
	 */
	private List<Boolean> fascColumns;

	/**
	 * Indice ricerca.
	 */
	private String indexRicercaAttiva;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		super.postConstruct();
		
		documentiDTH = new SimpleDetailDataTableHelper<>();
		documentiDTH.setMasters(new ArrayList<>());
		
		fascicoliDTH = new SimpleDetailDataTableHelper<>();
		fascicoliDTH.setMasters(new ArrayList<>());
		
		docColumns = Arrays.asList(true, true, false, true, false, false, false, false, false, false);
		fascColumns = Arrays.asList(true, true, true, true);
		
		initPaginaRicerca(Boolean.FALSE);
	}

	/**
	 * Inizializza la pagina di ricerca.
	 * @param isFromRepeate
	 */
	public void initPaginaRicerca(final Boolean isFromRepeate) {
		try {			
			// Recupero dei filtri di ricerca e dei risultati
			if (Boolean.FALSE.equals(isFromRepeate)) {
				final StatoRicercaDTO stDto = (StatoRicercaDTO) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.STATO_RICERCA, true);
				if (stDto != null) {
					formRicerca = (RicercaVeloceRequestDTO) stDto.getFormRicerca();
					searchResult = (RicercaResultDTO) stDto.getRisultatiRicerca();
				} else {
					formRicerca = new RicercaVeloceRequestDTO();
					searchResult = new RicercaResultDTO();
				}
			} 
			
			if (!searchResult.getFascicoli().isEmpty() && (RicercaPerBusinessEntityEnum.DOCUMENTI_FASCICOLI.equals(formRicerca.getRicercaBEntityEnum()) || RicercaPerBusinessEntityEnum.FASCICOLI.equals(formRicerca.getRicercaBEntityEnum()))) {
				fascicoliDTH.setMasters(searchResult.getFascicoli());
				fascicoliDTH.selectFirst(true);
				selectFascicoloDetail(fascicoliDTH.getCurrentMaster());
			}
			
			if (!searchResult.getDocumenti().isEmpty() && (RicercaPerBusinessEntityEnum.DOCUMENTI_FASCICOLI.equals(formRicerca.getRicercaBEntityEnum()) || RicercaPerBusinessEntityEnum.DOCUMENTI.equals(formRicerca.getRicercaBEntityEnum()))) {
				documentiDTH.setMasters(searchResult.getDocumenti());
				documentiDTH.selectFirst(true);
				selectDetail(documentiDTH.getCurrentMaster());
				toggleDetail = Boolean.TRUE; // questo è quello di default
			}
			
			if (getNumMaxRisultati() != null 
					&& ((searchResult.getFascicoli() != null && getNumMaxRisultati().equals(searchResult.getFascicoli().size())) 
							|| (searchResult.getDocumenti() != null && getNumMaxRisultati().equals(searchResult.getDocumenti().size())))) {
				showWarnMessage(RicercaAbstractBean.MSG_NUM_MAX_RISULTATI);
			}
			
			if (toggleDetail != null) {
				if (Boolean.TRUE.equals(toggleDetail)) {
					indexRicercaAttiva = PRIMO_SELECT;
				} else if (Boolean.FALSE.equals(toggleDetail) && RicercaPerBusinessEntityEnum.FASCICOLI.equals(formRicerca.getRicercaBEntityEnum())) {
					// Se il dettaglio da visualizzare è quello dei fascicoli 
					// si verifica il tipo di entità che è stata utilizzata per la ricerca e se è SOLO FASCICOLI viene selezionato il primo
					indexRicercaAttiva = PRIMO_SELECT;
				} else {
					indexRicercaAttiva = SECONDO_SELECT;
				} 
			}
			
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Non è stato possibile recuperare i risultati di ricerca");
		}
	}
	
	/**
	 * Metodo per reimpostare nel session bean il form utilizzato per la ricerca e quindi visualizzarlo nel dialog.
	 */
	public void ripetiRicerca() {
		try {
			// Viene ripetuta la ricerca con i stessi filtri
			sessionBean.getRicercaFormContainer().setRapida(formRicerca);	
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}
	
	/**
	 * Gestisce la selezione della riga del datatable dei documenti.
	 * @param se
	 */
	public final void rowSelector(final SelectEvent se) {
		documentiDTH.rowSelector(se);
		selectDetail(documentiDTH.getCurrentMaster());
	}
	
	/**
	 * Gestisce la selezione della riga del datatable dei fascicoli.
	 * @param se
	 */
	public final void fasRowSelector(final SelectEvent se) {
		fascicoliDTH.rowSelector(se);
		selectFascicoloDetail(fascicoliDTH.getCurrentMaster());
		
	}
	
	/**
	 * Metodo per la selezione e aggiornamento del dettaglio dall'esterno
	 */
	public void aggiorna() {
		if (documentiDTH != null && documentiDTH.getCurrentMaster() != null) {
			selectDetail(documentiDTH.getCurrentMaster());
		}
	}
	
	private void selectFascicoloDetail(final FascicoloDTO f) {
		final DettaglioFascicoloRicercaBean bean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_BEAN);
		
		if (f == null) {
			bean.unsetDetail();
			return;
		}
		
		bean.setDetail(f);
		bean.setCaller(this);
		toggleDetail = Boolean.FALSE;
	}
	
	/**
	 * Gestisce l'update del dettaglio.
	 * @param detail dettaglio da impostare
	 */
	@Override
	public void updateDetail(final DetailFascicoloRedDTO detail) {
	
		if (fascicoliDTH != null && fascicoliDTH.getCurrentMaster() != null) {
			final FascicoloDTO f = fascicoliDTH.getCurrentMaster();
			if (f.getIdFascicolo().equals(detail.getNomeFascicolo())) {
				f.setIndiceClassificazione(detail.getTitolarioDTO().getIndiceClassificazione());
			}
		}
	}
	
	/**
	 * Restituisce il documento selezionato.
	 * @return master selezionato
	 */
	@Override
	public MasterDocumentRedDTO getSelectedDocument() {
		return documentiDTH.getCurrentMaster();
	}
	
	/**
	 * Gestisce l'evento "Toggle" associato alle colonne dei documenti.
	 * @param event
	 */
	public final void onColumnToggleDocumenti(final ToggleEvent event) {
		docColumns.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
	}
	
	/**
	 * Gestisce l'evento "Toggle" associato alle colonne dei fascicoli.
	 * @param event
	 */
	public final void onColumnToggleFascicoli(final ToggleEvent event) {
		fascColumns.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
	}
	
	/**
	 * @return the documentiDTH
	 */
	public final SimpleDetailDataTableHelper<MasterDocumentRedDTO> getDocumentiDTH() {
		return documentiDTH;
	}

	/**
	 * @return the formRicerca
	 */
	public final RicercaVeloceRequestDTO getFormRicerca() {
		return formRicerca;
	}

	/**
	 * @param formRicerca the formRicerca to set
	 */
	public final void setFormRicerca(final RicercaVeloceRequestDTO formRicerca) {
		this.formRicerca = formRicerca;
	}

	/**
	 * @param documentiDTH the documentiDTH to set
	 */
	public final void setDocumentiDTH(final SimpleDetailDataTableHelper<MasterDocumentRedDTO> documentiDTH) {
		this.documentiDTH = documentiDTH;
	}

	/**
	 * @return the fascicoliDTH
	 */
	public final SimpleDetailDataTableHelper<FascicoloDTO> getFascicoliDTH() {
		return fascicoliDTH;
	}

	/**
	 * @param fascicoliDTH the fascicoliDTH to set
	 */
	public final void setFascicoliDTH(final SimpleDetailDataTableHelper<FascicoloDTO> fascicoliDTH) {
		this.fascicoliDTH = fascicoliDTH;
	}

	/**
	 * @return the toggleDetail
	 */
	public final Boolean getToggleDetail() {
		return toggleDetail;
	}

	/**
	 * @param toggleDetail the toggleDetail to set
	 */
	public final void setToggleDetail(final Boolean toggleDetail) {
		this.toggleDetail = toggleDetail;
	}

	/**
	 * @return the docColumns
	 */
	public final List<Boolean> getDocColumns() {
		return docColumns;
	}

	/**
	 * @param docColumns the docColumns to set
	 */
	public final void setDocColumns(final List<Boolean> docColumns) {
		this.docColumns = docColumns;
	}

	/**
	 * @return the fascColumns
	 */
	public final List<Boolean> getFascColumns() {
		return fascColumns;
	}

	/**
	 * @param fascColumns the fascColumns to set
	 */
	public final void setFascColumns(final List<Boolean> fascColumns) {
		this.fascColumns = fascColumns;
	}

	/**
	 * @return the indexRicercaAttiva
	 */
	public final String getIndexRicercaAttiva() {
		return indexRicercaAttiva;
	}

	/**
	 * @param indexRicercaAttiva the indexRicercaAttiva to set
	 */
	public final void setIndexRicercaAttiva(final String indexRicercaAttiva) {
		this.indexRicercaAttiva = indexRicercaAttiva;
	}

	/**
	 * Restituisce il prefisso del nome file.
	 * @return prefisso file name estrazione
	 */
	@Override
	protected String getNomeRicerca4Excel() {
		return "ricercagenericadocumenti";
	}
	
	/**
	 * Restituisce il nome del file per l'export della ricerca.
	 * @return nome file export
	 */
	public String getNomeFileExportCodaFascicoli() {
		String nome = "";
		if (sessionBean.getUtente() != null) {
			nome += sessionBean.getUtente().getNome().toLowerCase() + "_" + sessionBean.getUtente().getCognome().toLowerCase() + "_";
		}
		
		nome += "ricercagenericafascicoli_";
		
		final SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH.mm");
		nome += sdf.format(new Date()); 
		
		return nome;
	}

}
