package it.ibm.red.web.enums;

/**
 * Enum che indica la provenienza per la funzionalità del GoToCoda.
 * Utile per personalizzare, nel metodo "refresh" del ListaDocumentiBean,
 * la visualizzazione della coda (visibilità colonne, ordinamento, etc.) in base alla provenienza.
 * 
 * @author m.crescentini
 *
 */
public enum GoToCodaProvenienzaEnum {

	/**
	 * Valore.
	 */
	HOME_PAGE_DOC_IN_SCADENZA,


	/**
	 * Valore.
	 */
	HOME_PAGE_DOC_SPEDIZIONE_IN_ERRORE;
	
}