package it.ibm.red.web.document.helper;

import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.web.helper.dtable.Selectable;

/**
 * Helper contributo.
 */
public class ContributoHelper extends Selectable<ContributoDTO> {

    /**
     * Flag editabile.
     */
	private Boolean editable;
	
	/**
	 * Costruttore di default.
	 */
	public ContributoHelper() {
		super();
	}

	/**
	 * Costruttore che imposta utente e data.
	 * @param data
	 * @param utente
	 */
	public ContributoHelper(final ContributoDTO data, final UtenteDTO utente) {
		super(data);
		if (Boolean.TRUE.equals(data.getFlagEsterno())) {
			this.editable = data.getModificabile();
		} else {
			this.editable = utente.getId().equals(data.getIdUtente());
		}
	}

	/**
	 * Restituisce l'editabilità.
	 * @return true se editable, false altrimenti
	 */
	public Boolean getEditable() {
		return editable;
	}

	/**
	 * Imposta il flag Editable.
	 * @param editable
	 */
	public void setEditable(final Boolean editable) {
		this.editable = editable;
	}
}
