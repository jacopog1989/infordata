package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipologiaNodoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IInvioInFirmaFacadeSRV;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * Bean che gestisce la response di invio in firma, sigla o visto.
 */
@Named(ConstantsWeb.MBean.INVIA_IN_FIRMA_SIGLA_VISTO_BEAN)
@ViewScoped
public class InviaInFirmaSiglaVistoBean extends AbstractBean implements InitHumanTaskInterface {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -2006429609774176124L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(InviaInFirmaSiglaVistoBean.class.getName());

	/**
	 * Servizio.
	 */
	private IInvioInFirmaFacadeSRV invioSRV;

	/**
	 * Servizio.
	 */
	private IOrganigrammaFacadeSRV organigrammaSRV;

	/**
	 * Bean di sessione.
	 */
	private SessionBean sb;

	/**
	 * Lista documenti bean.
	 */
	private ListaDocumentiBean ldb;

	/**
	 * Collezione master.
	 */
	private Collection<MasterDocumentRedDTO> masters;

	/**
	 * Radice organigramma.
	 */
	private transient TreeNode root;

	/**
	 * Nodo selezionato.
	 */
	private NodoOrganigrammaDTO selected;

	/**
	 * Informazioni utente.
	 */
	private UtenteDTO utente;

	/**
	 * Nome e cognome dell'utente selezionato oppure Descrizione del nodo ufficio
	 * selezionato
	 */
	private String descrizioneNewAssegnazione;

	/**
	 * Checkbox per impostare come urgente o meno
	 */
	private Boolean urgente;

	/**
	 * Input proveniente dalla maschera.
	 */
	private String motivoAssegnazioneNew;

	/**
	 * Nodi.
	 */
	private List<Nodo> alberaturaNodi;

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		masters = inDocsSelezionati;
	}

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
		sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sb.getUtente();
		organigrammaSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
		alberaturaNodi = organigrammaSRV.getAlberaturaBottomUp(utente.getIdAoo(), utente.getIdUfficio());

		invioSRV = ApplicationContextProvider.getApplicationContext().getBean(IInvioInFirmaFacadeSRV.class);
		root = inizializzaOrganigramma();
	}

	/*********** Organigramma ******************************/
	private DefaultTreeNode inizializzaOrganigramma() {
		DefaultTreeNode lRoot = null;

		final List<Long> alberatura = new ArrayList<>();
		for (int i = alberaturaNodi.size() - 1; i >= 0; i--) {
			alberatura.add(alberaturaNodi.get(i).getIdNodo());
		}

		// Si recuperano dal DB i nodi di primo livello
		final NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
		nodoRadice.setIdAOO(utente.getIdAoo());
		nodoRadice.setIdNodo(utente.getIdNodoRadiceAOO());
		nodoRadice.setCodiceAOO(utente.getCodiceAoo());
		nodoRadice.setUtentiVisible(false);

		TipoAssegnazioneEnum assegnazioneCorrente = null;

		assegnazioneCorrente = getAssegnazioneCorrente(ldb.getSelectedResponse());

		final List<NodoOrganigrammaDTO> primoLivello = organigrammaSRV.getFigliAlberoUtentiAssegnatariFilteredByPermesso(utente.getIdUfficio(), nodoRadice,
				ldb.getSelectedResponse());

		lRoot = new DefaultTreeNode();
		lRoot.setExpanded(true);
		lRoot.setSelectable(false);
		lRoot.setType("ROOT");

		// per ogni nodo di primo livello creo il nodo da mettere nell'albero che ha
		// come padre rootAssegnazionePerCompetenza
		List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
		lRoot.setChildren(new ArrayList<>());
		DefaultTreeNode nodoToAdd = null;
		for (final NodoOrganigrammaDTO n : primoLivello) {
			n.setCodiceAOO(utente.getCodiceAoo());
			if (n.getIdUtente() == null && (assegnazioneCorrente == TipoAssegnazioneEnum.FIRMA || assegnazioneCorrente == TipoAssegnazioneEnum.SIGLA)) {
				n.setUtentiVisible(true);
			}
			nodoToAdd = new DefaultTreeNode(n, lRoot);
			nodoToAdd.setExpanded(true);
			if (n.getIdUtente() == null && alberatura.contains(n.getIdNodo())) {
				nodiDaAprire.add(nodoToAdd);
			}
			nodoToAdd.setType("UFF");
		}

		final List<DefaultTreeNode> nodiDaAprireTemp = new ArrayList<>();
		for (int i = 0; i < alberatura.size(); i++) {
			// devo fare esattamente un numero di cicli uguale alla lunghezza
			// dell'alberatura
			nodiDaAprireTemp.clear();
			for (final DefaultTreeNode nodo : nodiDaAprire) {
				onOpenNode(nodo);
				nodo.setExpanded(true);
				for (final TreeNode nodoFiglio : nodo.getChildren()) {
					final NodoOrganigrammaDTO nodoFiglioDto = (NodoOrganigrammaDTO) nodoFiglio.getData();
					if (nodoFiglioDto.getIdUtente() == null && alberatura.contains(nodoFiglioDto.getIdNodo())) {
						nodiDaAprireTemp.add((DefaultTreeNode) nodoFiglio);
					}
				}
			}
			nodiDaAprire = new ArrayList<>(nodiDaAprireTemp);
		}

		return lRoot;
	}

	private TipoAssegnazioneEnum getAssegnazioneCorrente(final ResponsesRedEnum responseEnum) {
		TipoAssegnazioneEnum assegnazioneCorrente = null;

		switch (responseEnum) {
		case INVIO_IN_FIRMA:
			assegnazioneCorrente = TipoAssegnazioneEnum.FIRMA;
			break;
		case INVIO_IN_SIGLA:
			assegnazioneCorrente = TipoAssegnazioneEnum.SIGLA;
			break;
		default:
			final RedException redException = new RedException("Response non gestita per questo controller");
			LOGGER.error("Il controller è stato chiamato per una response che non gestisce " + ldb.getSelectedResponse() + " utente: " + utente.getId(), redException);
			showError(redException);
			throw redException;
		}

		return assegnazioneCorrente;
	}

	/**
	 * Metodo triggerato dall'evento expand del tree node.
	 * 
	 * @param event
	 */
	public void onOpenNode(final NodeExpandEvent event) {
		onOpenNode(event.getTreeNode());
	}

	/**
	 * Crea nuovi nodi in modo gerarchico.
	 * 
	 * @param treeNode
	 */
	private void onOpenNode(final TreeNode treeNode) {
		final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) treeNode.getData();
		final List<NodoOrganigrammaDTO> figli = organigrammaSRV.getFigliAlberoUtentiAssegnatariFilteredByPermesso(utente.getIdUfficio(), nodoExp, ldb.getSelectedResponse());
		initializeSons(figli, treeNode, getAssegnazioneCorrente(ldb.getSelectedResponse()));
	}

	private void initializeSons(final List<NodoOrganigrammaDTO> figli, final TreeNode treeNodePadre, final TipoAssegnazioneEnum assegnazioneCorrente) {
		treeNodePadre.getChildren().clear();
		DefaultTreeNode nodoToAdd = null;
		for (final NodoOrganigrammaDTO n : figli) {
			n.setCodiceAOO(utente.getCodiceAoo());
			if (n.getIdUtente() == null && (assegnazioneCorrente == TipoAssegnazioneEnum.FIRMA || assegnazioneCorrente == TipoAssegnazioneEnum.SIGLA)) {
				n.setUtentiVisible(true);
			}
			nodoToAdd = new DefaultTreeNode(n, treeNodePadre);
			nodoToAdd.setType("UFF");
			if (n.getTipoNodo().equals(TipologiaNodoEnum.UTENTE)) {
				nodoToAdd.setType("USER");
			}
			if (n.getFigli() > 0 || n.isUtentiVisible()) {
				new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
			}
		}

	}

	/**
	 * Metodo triggerato dall'evento select del tree node.
	 * 
	 * @param event
	 */
	public void onSelectNode(final NodeSelectEvent event) {
		// Si recupera l'utente selezionato dall'evento e lo si utilizza poi per
		// l'invocazione del service
		selected = (NodoOrganigrammaDTO) event.getTreeNode().getData();

		switch (selected.getTipoNodo()) {
		case UFFICIO:
		case UFFICIO_VIRTUALE:
			setDescrizioneNewAssegnazione(selected.getDescrizioneNodo());
			break;
		case UTENTE:
			setDescrizioneNewAssegnazione(selected.getCognomeUtente() + " " + selected.getNomeUtente());
			break;
		default:
			final RedException redException = new RedException("Tipo nodo non selezionabile");
			LOGGER.error("Selezionato un nodo dell'organigramma con tipo ufficio non gestito " + ldb.getSelectedResponse() + " utente: " + utente.getId() + " nodoId: "
					+ selected.getIdNodo() + " idUtente: " + selected.getIdUtente(), redException);
			showError(redException);
			throw redException;
		}
	}

	/********* Organigramma *************/

	public void clearAssegnazione() {
		descrizioneNewAssegnazione = "";
		this.selected = null;
	}

	/**
	 * Invia in response.
	 */
	public void inviaInResponse() {
		Collection<EsitoOperazioneDTO> eOpe = new ArrayList<>();
		final Collection<String> wobNumbers = new ArrayList<>();
		try {
			// Si puliscono gli esiti
			ldb.cleanEsiti();
			
			if(selected == null) {
				showWarnMessage("Selezionare un assegnatario.");
				return;
			}
			
			// Si raccolgono i WOB Number necessari ad invocare il service 
			for (final MasterDocumentRedDTO m : masters) {
				if (!StringUtils.isNullOrEmpty(m.getWobNumber())) {
					wobNumbers.add(m.getWobNumber());
				} else {
					LOGGER.error("Individuato un WOB Number nullo.");
					throw new RedException("Uno dei documenti selezionati ha un WOB Number nullo.");
				}
			}

			// Si invoca il servizio
			switch (ldb.getSelectedResponse()) {
			case INVIO_IN_FIRMA:
				eOpe = invioSRV.invioInFirma(utente, wobNumbers, selected.getIdNodo(), selected.getIdUtente(), motivoAssegnazioneNew, urgente);
				break;
			case INVIO_IN_SIGLA:
				eOpe = invioSRV.invioInSigla(utente, wobNumbers, selected.getIdNodo(), selected.getIdUtente(), motivoAssegnazioneNew, urgente);
				break;
			case INVIO_IN_VISTO:
				// Manca l'invio in visto
				break;
			default:
				throw new RedException("Nessuna response supportata per questo controller.");
			}

			// setto gli esiti con i nuovi e eseguo un refresh
			ldb.getEsitiOperazione().addAll(eOpe);
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.INVIA_IN_FIRMA_SIGLA_VISTO_BEAN);
			FacesHelper.executeJS("PF('dlgGeneric').hide()");
			FacesHelper.executeJS("PF('dlgShowResult').show()");
		} catch (final RedException red) {
			showError(red);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il tentato sollecito della response '" + ldb.getSelectedResponse() + "'", e);
			showError(e);
		}
	}

	/**
	 * Recupera session bean.
	 * 
	 * @return session bean
	 */
	public SessionBean getSb() {
		return sb;
	}

	/**
	 * Imposta session bean.
	 * 
	 * @param sb session bean
	 */
	public void setSb(final SessionBean sb) {
		this.sb = sb;
	}

	/**
	 * Recupera il motivo dell'assegnazione.
	 * 
	 * @return motivo dell'assegnazione
	 */
	public String getMotivoAssegnazioneNew() {
		return motivoAssegnazioneNew;
	}

	/**
	 * Imposta il motivo dell'assegnazione.
	 * 
	 * @param motivoAssegnazioneNew motivo dell'assegnazione
	 */
	public void setMotivoAssegnazioneNew(final String motivoAssegnazioneNew) {
		this.motivoAssegnazioneNew = motivoAssegnazioneNew;
	}

	/**
	 * Recupera l'attributo invioSRV.
	 * 
	 * @return invioSRV
	 */
	public IInvioInFirmaFacadeSRV getInvioSRV() {
		return invioSRV;
	}

	/**
	 * Imposta l'attributo invioSRV.
	 * 
	 * @param invioSRV
	 */
	public void setInvioSRV(final IInvioInFirmaFacadeSRV invioSRV) {
		this.invioSRV = invioSRV;
	}

	/**
	 * Recupera la root dell'alberatura dei nodi.
	 * 
	 * @return root
	 */
	public TreeNode getRoot() {
		return root;
	}

	/**
	 * Imposta la root dell'alberatura dei nodi.
	 * 
	 * @param root
	 */
	public void setRoot(final TreeNode root) {
		this.root = root;
	}

	/**
	 * Recupera la selezione.
	 * 
	 * @return selezione
	 */
	public NodoOrganigrammaDTO getSelected() {
		return selected;
	}

	/**
	 * Imposta la selezione.
	 * 
	 * @param selected selezione
	 */
	public void setSelected(final NodoOrganigrammaDTO selected) {
		this.selected = selected;
	}

	/**
	 * Recupera l'utente.
	 * 
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'utente.
	 * 
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Recupera la descrizione della nuova assegnazione.
	 * 
	 * @return descrizione della nuova assegnazione
	 */
	public String getDescrizioneNewAssegnazione() {
		return descrizioneNewAssegnazione;
	}

	/**
	 * Imposta la descrizione della nuova assegnazione.
	 * 
	 * @param descrizioneNewAssegnazione descrizione della nuova assegnazione.
	 */
	public void setDescrizioneNewAssegnazione(final String descrizioneNewAssegnazione) {
		this.descrizioneNewAssegnazione = descrizioneNewAssegnazione;
	}

	/**
	 * Recupera l'attributo urgente.
	 * 
	 * @return urgente
	 */
	public Boolean getUrgente() {
		return urgente;
	}

	/**
	 * Imposta l'attributo urgente.
	 * 
	 * @param urgente
	 */
	public void setUrgente(final Boolean urgente) {
		this.urgente = urgente;
	}

}
