package it.ibm.red.web.mbean.component;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.FascicoloDTO;

/**
 * Component documenti allegabili.
 */
public class DocumentiAllegabiliComponent extends DocumentiAllegabiliAbstract {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -4923437075039450001L;

	/**
	 * Costruttore del component.
	 * 
	 * @param fascicoloProcedimentale
	 * @param documentiAllegabili
	 * @param inDimensioneTotale
	 */
	public DocumentiAllegabiliComponent(final FascicoloDTO fascicoloProcedimentale, final List<DocumentoAllegabileDTO> documentiAllegabili,
			final BigDecimal inDimensioneTotale) {
		super(fascicoloProcedimentale, documentiAllegabili, inDimensioneTotale);
	}

	/**
	 * Esegue la validazione sulla dimensione totale dei procedimenti.
	 * 
	 * @return true se la validazione ha avuto esito positivo, false altrimenti
	 */
	public boolean validate() {
		boolean valid = true;
		if (CollectionUtils.isEmpty(getSelectedAllegabili())) {
			valid = false;
			showWarnMessage("Attenzione, è necessario selezionare almeno un procedimento da allegare.");
		}

		if (isDimensioneSuperata()) {
			valid = false;
			showWarnMessage("Attenzione, la dimensione totale dei procedimenti selezionati per l'inoltro supera quella massima consentita.");
		}

		return valid;
	}

	@Override
	public void aggiornaVerificaFirmaAllegato(final Object index) {
		// Non occorre fare niente in questo metodo.
	}

}
