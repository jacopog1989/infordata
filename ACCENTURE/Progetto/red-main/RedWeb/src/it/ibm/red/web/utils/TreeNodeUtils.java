package it.ibm.red.web.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.web.dto.QueueCountDTO;

/**
 * Utility tree node.
 */
public final class TreeNodeUtils {
	
	/**
	 * Costruttore vuoto.
	 */
	private TreeNodeUtils() {
		// Costruttore vuoto.
	}
	
	/**
	 * Ritorna una lista non ordinata di root e tutti i suoi figli.
	 * 
	 * @param tree
	 * @return
	 */
	public static List<TreeNode> flatten(final TreeNode root) {
		final List<TreeNode> sons = new ArrayList<>();
		sons.add(root);
		final List<TreeNode> flattenList = new ArrayList<>();
		flatten(sons, flattenList);
		return flattenList;
	}
	
	/**
	 * Ritorna una lista non ordinata di tutti i nodi figli di un albero e i loro vari figli.
	 * 
	 * @param sons
	 * @param flattenList
	 */
	public static void flatten(final List<TreeNode> sons, final List<TreeNode> flattenList) {
		flattenList.addAll(sons);
		for (final TreeNode figlio : sons) {
			if (!figlio.isLeaf()) {
				flatten(figlio.getChildren(), flattenList); 
			}
		}
	}
	
	/**
	 * Espande tutti i nodi dal padre e i figli.
	 * 
	 * @param root
	 */
	public static void expandAll(final TreeNode root) {
		final List<TreeNode> flattenList =  flatten(root);
		for (final TreeNode node :flattenList) {
			if (!node.isLeaf() && !node.isExpanded()) {
				node.setExpanded(true);
			}
		}
	}
	
	/**
	 * In un tree node verifica che esista un elemento del treeNode che contenga data, 
	 * eventaulmente ritorna il treenode individuato.
	 * 
	 * Questo metodo si basa sull'implementazione dell'equals del find.
	 * 
	 * @param data 
	 *  Elemento da verificare
	 * 
	 * @return 
	 *  Il treeNode individuato
	 *  
	 */
	public static  TreeNode find(final TreeNode treeNode, final Object data) {
		final List<TreeNode> treeNodeList = new ArrayList<>();
		treeNodeList.add(treeNode);
		return recursiveFind(treeNodeList, data);
	}
	
	/*
	 * Cerca prima tra tutti i figli e poi fa la deep first search.
	 * Si poteva fare meglio.
	 * @param sons
	 * @param data
	 * @param comparator
	 * @return
	 */
	private static TreeNode recursiveFind(final List<TreeNode> sons, final Object data) {
		
		for (final TreeNode son : sons) {
			final Object newData =  son.getData();
			if (data.equals(newData)) {
				return son;
			}
		}
		
		for (final TreeNode son : sons) {
			if (!son.isLeaf()) {
				final TreeNode found = recursiveFind(son.getChildren(), data);
				if (found !=  null) {
					return found;
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Sostituisce il nuovo vecchio con il nodo nuovo.
	 * 
	 * Il nodo da sostituire e anche il sostituito devono avere un parent in comune.
	 * 
	 * @param vecchio
	 * @param nuovo
	 */
	public static void sostiuisci(final TreeNode vecchio, final TreeNode nuovo) {
		final TreeNode formalParent = vecchio.getParent(); 
		nuovo.setParent(formalParent);
		
		final Iterator<TreeNode> it = formalParent.getChildren().iterator();
		boolean found = false;
		int index = 0;
		while (it.hasNext() && !found) {
			found = vecchio.equals(it.next());
			index++;
		}
		if (found) {
			it.remove();
			formalParent.getChildren().add(--index, nuovo);
		}
	}
	
	/**
	 * Gestisce la logica di sostiuzione vecchio - nuovo per Menu sinistra.
	 * @param vecchio
	 * @param nuovo
	 */
	public static void sostiuisciPerMenuSinistra(final TreeNode vecchio, final TreeNode nuovo) {
		final TreeNode formalParent = vecchio.getParent(); 
		nuovo.setParent(formalParent);
		
		final Iterator<TreeNode> it = formalParent.getChildren().iterator();
		boolean found = false;
		int index = 0;
		while (it.hasNext() && !found) {
			found = vecchio.equals(it.next());
			index++;
		}
		
		if (((DefaultTreeNode) vecchio).getData() instanceof  QueueCountDTO) {
			final Integer count = ((QueueCountDTO) ((DefaultTreeNode) vecchio).getData()).getCount();
			
			if (found) {
				it.remove();
				((QueueCountDTO) ((DefaultTreeNode) nuovo).getData()).setCount(count);
					
				formalParent.getChildren().add(--index, nuovo);
			}
		} else {
	       final Integer count = ((NodoOrganigrammaDTO) ((DefaultTreeNode) vecchio).getData()).getCountOrg();
			
			if (found) {
				it.remove();
				
				((NodoOrganigrammaDTO) ((DefaultTreeNode) nuovo).getData()).setCountOrg(count);
				formalParent.getChildren().add(--index, nuovo);
			}	
		}
	}
	
	
	
	/**
	 * Trova il padre del nodo node il cui padre ha come tipo type (Compreso se stesso).
	 * Ad esempio trova il primo padre di node prima di "ROOT".
	 * 
	 * Mentre scorre la gerarchia espande anche i nodi attraversati.
	 * 
	 * @param node
	 * 	nodo di cui si desidera trovare il padre
	 * @param type
	 *  tipo del padre al quale ci si vuole fermare.
	 * @return
	 */
	public static TreeNode findParentAndExpand(TreeNode node, final List<TreeNode> listaDiNodiFinoAllaRoot) {
		TreeNode parent = node.getParent();
		while (parent != null && !"ROOT".equals(parent.getType())) {
			node = parent;
			node.setExpanded(true);
			parent = node.getParent();
			//Verrà popolata in ordine di profondità decrescente
			listaDiNodiFinoAllaRoot.add(node);
		}
		return node;
	}
}
