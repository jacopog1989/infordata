package it.ibm.red.web.servlets;

import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import flexjson.JSONSerializer;
import it.ibm.red.business.dto.PerformanceDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ICounterFacadeSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * Servlet Measure.
 */
public class MeasureServlet extends HttpServlet {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(MeasureServlet.class.getName());

	/**
	 * Costruttore di default.
	 */
	public MeasureServlet() {
		super();
	}

	/**
	 * Do get della servlet.
	 * @param request
	 * @param response
	 */
	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response) {
		try {
			final String type = request.getParameter("type");
			final boolean isFull = (type != null && "full".equalsIgnoreCase(type));
			final String username = request.getParameter("username");
			final String format = request.getParameter("format");
			final PrintWriter writer = response.getWriter();

			if (!StringUtils.isNullOrEmpty(username)) {
				final ICounterFacadeSRV counterSRV = ApplicationContextProvider.getApplicationContext().getBean(ICounterFacadeSRV.class);
				final PerformanceDTO output = counterSRV.measure(username, isFull);
				final String host = getLocalHost(output);

				String sessionID = null;
				final HttpSession session = request.getSession(false);
				if (session != null) {
					sessionID = session.getId();
				}
				output.setSessionID(sessionID);
				if ("json".equalsIgnoreCase(format)) {
					writer.print(new JSONSerializer().serialize(output));
				} else {
					writer.print("<style type='text/css'>tr:nth-child(2n){background-color: #e7fffc;}td {border: 1px black solid;}</style>");
					final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:SS");
					writer.print("<label id='host'>" + host + "</label><br>");
					writer.print("<label id='time'>" + sdf.format(new Date()) + "</label><br>");
					if (output.getSessionID() != null) {
						writer.print("<label id='sessionID'>" + output.getSessionID() + "[Questa sessione sarà chiusa a valle della response]</label>");
					} else {
						writer.print("<label id='sessionID'>SESSIONE NON PRESENTE</label>");
					}
					writer.print("<table style='width:100%' id='results'>");
					writer.print("<tr><th style='width:50%' >Ambito</th><th style='width:50%'>Misurazione [ms]</th></tr>");

					printAmbito(writer, "Connection", output.getConnectionMS());
					printAmbito(writer, "Recupero Utente", output.getUserMS());
					printAmbito(writer, "Database Applicativo", output.getDbApplicativoMS());
					printAmbito(writer, "Database DWH", output.getDbDWHMS());
					printAmbito(writer, "PE", output.getFnetPEMS());
					printAmbito(writer, "CE", output.getFnetCEMS());
					printAmbito(writer, "Generica Fascicoli", output.getRicercaGenericaFascicoliMS());
					printAmbito(writer, "Avanzata Fascicoli", output.getRicercaAvanzataFascicoliMS());
					if (isFull) {
						printAmbito(writer, "NPS", output.getNpsMS());
						printAmbito(writer, "ADOBE", output.getAdobeMS());
						printAmbito(writer, "PK", output.getPkMS());
						printAmbito(writer, "IText", output.getiTextMS());
					}
					printAmbito(writer, "TOT", output.getTotale());
					writer.print("</table>");
				}
				if (session != null) {
					session.invalidate();
				}
			} else {
				writer.print("UTENTE NON FORNITO");
			}
			writer.close();
		} catch (final Exception e) {
			LOGGER.error("Errore nell'aggiornamento del livello di log: ", e);
		}
	}

	/**
	 * @param output
	 * @return local host
	 */
	private static String getLocalHost(final PerformanceDTO output) {
		String host = "";
		try {
			final InetAddress ia = InetAddress.getLocalHost();
			host = ia.toString();
			output.setIp(host);
		} catch (final UnknownHostException e) {
			LOGGER.warn("Errore nel recupero dell'indirizzo locale della macchina: ", e);
		}
		return host;
	}

	private static void printAmbito(final PrintWriter writer, final String ambito, final Long value) {
		writer.print("<tr>");
		writer.print("<td>" + ambito + "</td>");
		writer.print("<td>" + value + "</td>");
		writer.print("</tr>");
	}

}
