package it.ibm.red.web.document.mbean.component;

import java.util.stream.Collectors;

import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.web.document.helper.ContributoHelper;

/**
 * Component che gestisce il tab del contributo interno.
 */
public class ContributoInternoTabComponent extends AbstractContributoTabComponent {
	
	/**
	 * La costante Serial versione UID.
	 */
	private static final long serialVersionUID = 2198671929316934507L;

	/**
	 * Costruttore, si limita a chiamare il costruttore parent.
	 * 
	 * @param detail
	 * @param utente
	 */
	public ContributoInternoTabComponent(final DetailDocumentRedDTO detail, final UtenteDTO utente) {
		super(detail, utente);
	}

	/**
	 * @see it.ibm.red.web.document.mbean.component.AbstractContributoTabComponent#updateContributo().
	 */
	@Override
	protected void updateContributo() {
		contributoList = detail.getContributi().stream()
			.filter(c -> Boolean.FALSE.equals(c.getFlagEsterno()))
			.map(c -> new ContributoHelper(c, utente))
			.collect(Collectors.toList());
	}

	/**
	 * @see it.ibm.red.web.document.mbean.component.AbstractContributoTabComponent#eliminaContributo(it.ibm.red.business.dto.ContributoDTO,
	 *      java.lang.Integer).
	 */
	@Override
	public EsitoOperazioneDTO eliminaContributo(final ContributoDTO contributo, final Integer idDocumento) {
		return contributoSRV.eliminaContributoInterno(contributo.getIdContributo(), contributo.getIdUtente(), idDocumento, utente);
	}

}
