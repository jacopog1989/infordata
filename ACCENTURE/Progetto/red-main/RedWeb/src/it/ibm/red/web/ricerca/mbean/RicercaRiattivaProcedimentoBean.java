package it.ibm.red.web.ricerca.mbean;


import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IRiattivaProcedimentoFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.mbean.component.AbstractComponent;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.IMessaggioRegistraAnnullaComponent;

/**
 * Bean che gestisce la funzionalità di riattivamento procedimento.
 */
public class RicercaRiattivaProcedimentoBean extends AbstractComponent implements IMessaggioRegistraAnnullaComponent {
	
	/**
	 * Chiamante.
	 */
	private final RicercaDocumentiRedAbstractBean ricerca; 
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -6121397257661922836L;
	
	/**
	 * Motivazione.
	 */
	private String motivazione;

	/**
	 * Costruttore del bean.
	 * @param inRicerca
	 */
	public RicercaRiattivaProcedimentoBean(final RicercaDocumentiRedAbstractBean inRicerca) {
		this.ricerca = inRicerca;
	}

	/**
	 * Esegue la logica associata alla response.
	 */
	@Override
	public void registra() {
		final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		final UtenteDTO utente = sb.getUtente();
		final IRiattivaProcedimentoFacadeSRV riattivaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRiattivaProcedimentoFacadeSRV.class);

		final MasterDocumentRedDTO currentMaster = ricerca.getSelectedDocument();

		if (currentMaster != null) {
			processResponse(utente, riattivaSRV, currentMaster);
		} else {
			showErrorMessage("Nessun documento selezionato");
		}
	}

	/**
	 * Processa la response ed esegue la riattivazione.
	 * @param utente
	 * @param riattivaSRV
	 * @param master
	 */
	private void processResponse(final UtenteDTO utente, final IRiattivaProcedimentoFacadeSRV riattivaSRV, final MasterDocumentRedDTO master) {
		boolean errorMsg = false;
		boolean nonRiattivabili = false;
		EsitoOperazioneDTO esito = null;
		
		if (riattivaSRV.isRiattivabile(master.getDocumentTitle(), utente)) {				
			esito = riattivaSRV.riattiva(master.getDocumentTitle(), utente, motivazione);
			if (!esito.isEsito()) {
				errorMsg = true;
			}
		} else {
			nonRiattivabili = true;
		}
		
		if (!errorMsg && !nonRiattivabili) {			
			showInfoMessage(esito.getNote());
		} else {
			final StringBuilder str = new StringBuilder();
			
			if (errorMsg) {
				str.append("Errore durante la riattivazione");
			}
			
			if (errorMsg) {
				str.append("<br/>");
			}
			
			if (nonRiattivabili) {
				str.append("Il documento non è riattivabile");
			}
			
			showErrorMessage(str.toString());
		}
	}
	
	/**
	 * Gestisce le operazioni in fase di chiusura.
	 */
	@Override
	public void close() {
		setMessaggio(null);
	}

	/**
	 * Restituisce la {@link #motivazione}.
	 */
	@Override
	public String getMessaggio() {
		return motivazione;
	}
	
	/**
	 * Imposta la {@link #motivazione}.
	 */
	@Override
	public void setMessaggio(final String motivazione) {
		this.motivazione = motivazione;
	}
}
