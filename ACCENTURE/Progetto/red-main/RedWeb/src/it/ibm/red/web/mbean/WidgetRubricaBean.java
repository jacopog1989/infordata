package it.ibm.red.web.mbean;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.TipoOperazioneEnum;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.NotificaContatto;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IRubricaFacadeSRV;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Bean che gestisce il widget della rubrica.
 */
@Named(ConstantsWeb.MBean.WDG_RUBRICA_BEAN)
@ViewScoped
public class WidgetRubricaBean extends AbstractRubricaBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -6933405827515662718L;

    /**
     * Info utente.
     */
	private UtenteDTO utente;
	
    /**
     * Datatable notifiche.
     */
	private SimpleDetailDataTableHelper<NotificaContatto> notificheRichiesteDTH;

    /**
     * Numero giorni.
     */
	private int numGiorni;

    /**
     * Numero elementi.
     */
	private int numElementi;

    /**
     * Nota richiesta modifica.
     */
	private String notaRichiestaModifica;

    /**
     * Nota richiesta eliminazione.
     */
	private String notaRichiestaEliminazione;

    /**
     * Num giorni notifiche.
     */
	private int numGiorniNotifiche;

    /**
     * Numero elementi notifiche.
     */
	private int numElementiNotifiche;

	/**
	 * Indica quali campi del form renderizzare in base alla variabile mode:
	 * 			se true siamo in modalita di creazione contatto
	 * 			se false siamo in modalità di modifica/eliminazione contatto.
	 */
	private boolean modeFlag;


    /**
     * Operazione.
     */
	private TipoOperazioneEnum operazioneEnum;


    /**
     * Session bean.
     */
	private SessionBean sBean;


    /**
     * Campo nascosto.
     */
	private transient Object hiddenfield;

	/**
	 * @see it.ibm.red.web.mbean.AbstractBean#postConstruct().
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		sBean = FacesHelper.getManagedBean(Constants.ManagedBeanName.SESSION_BEAN);
		initRubricaWidget();
	}

	/**
	 * Inizializza il widget.
	 */
	public void initRubricaWidget() {
		utente = sBean.getUtente();
		setRubSRV(ApplicationContextProvider.getApplicationContext().getBean(IRubricaFacadeSRV.class));
		numGiorni = 30;
		numElementi = 10;
		modeFlag = true;
		setContattoConsiderato(new Contatto());
		
		// Si caricano i dati da mostrare nel widget
		loadTabNotificheRichieste();
	}
	
	/**
	 * @param event
	 */
	public void gestisciNotifica(final ActionEvent event) {
		gestisciNotifica((NotificaContatto) getDataScrollerClickedRow(event));
	}
	
	
	/**
	 * Carica il datatable che viene visualizzato nel widget.
	 */
	public void loadTabNotificheRichieste() {
		if (getPermessoUtente()) {
			setNotificheContattiDTH(new SimpleDetailDataTableHelper<>(
					getRubSRV().getNotificheAdmin(utente.getIdUfficio(), utente.getIdAoo(), 
							NotificaContatto.TIPO_NOTIFICA_RICHIESTA, numGiorni, numElementi, utente.getId())));
		}
	}

	/**
	 * Restituisce true se l' {@link #utente} ha il permesso di gestione rubrica.
	 * @see PermessiEnum.
	 * @return true se l'utente può gestire la rubrica, false altrimenti
	 */
	public boolean getPermessoUtente() {
		return PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.GESTIONE_RUBRICA);
	}

	/**
	 * Effettua il reset di tutti i campi.
	 */
	public void resetAllFields() {
		setContattoConsiderato(new Contatto());
		hiddenfield = null;
	}

	/**
	 * Imposta i campi nascosti.
	 * @param inHiddenfield
	 */
	public void setHiddenfield(Object inHiddenfield) {
		if ("true".equalsIgnoreCase((String) inHiddenfield)) {
			inHiddenfield = null;
			resetAllFields();
		}
		hiddenfield = inHiddenfield;
		FacesHelper.update(getFormName() + ":hiddenfield");
	}
	
	@Override
	protected void removeContattoFromAllDataTableContatti(final Contatto contatto) {
		// Non deve fare nulla
	}

	@Override
	protected void updateContattoInAllDataTableContatti(final Contatto contatto) {
		// Non deve fare nulla
	}

	@Override
	protected void clearFilterDataTableNotifiche(final String idComponent, final SimpleDetailDataTableHelper<NotificaContatto> dth) {
		// Non deve fare nulla
	}

	/**
	 * Restituisce il campo nascosto.
	 * @return campo nascosto
	 */
	public Object getHiddenfield() {
		return hiddenfield;
	}

	/**
	 * Restituisce il flag associato alla modalita.
	 * @return flag associato alla modalita
	 */
	public boolean getModeFlag() {
		return modeFlag;
	}

	/**
	 * Restituisce la nota di richiesta eliminazione.
	 * @return nota di richiesta eliminazione
	 */
	public String getNotaRichiestaEliminazione() {
		return notaRichiestaEliminazione;
	}

	/**
	 * Restituisce la nota di richiesta modifica.
	 * @return nota di richiesta modifica
	 */
	public String getNotaRichiestaModifica() {
		return notaRichiestaModifica;
	}

	/**
	 * Restituisce l'helper per la gestione del datatable delle notifiche richieste.
	 * @return helper datatable notifiche richieste
	 */
	public SimpleDetailDataTableHelper<NotificaContatto> getNotificheRichiesteDTH() {
		return notificheRichiesteDTH;
	}

	/**
	 * Restituisce il numero degli elementi.
	 * @return numero elementi
	 */
	public int getNumElementi() {
		return numElementi;
	}

	/**
	 * Restituisce il numero elementi notifiche.
	 * @return numero elementi modifiche.
	 */
	public int getNumElementiNotifiche() {
		return numElementiNotifiche;
	}

	/**
	 * Restituisce il numero dei giorni.
	 * @return numero giorni
	 */
	public int getNumGiorni() {
		return numGiorni;
	}

	/**
	 * Restituisce il numero dei giorni notifiche.
	 * @return numero giorni notifiche
	 */
	public int getNumGiorniNotifiche() {
		return numGiorniNotifiche;
	}

	/**
	 * Restutisce il tipo operazione.
	 * @return tipo operazione
	 */
	public TipoOperazioneEnum getOperazioneEnum() {
		return operazioneEnum;
	}

	/**
	 * Imposta il flag associato alla modalita.
	 * @param modeFlag
	 */
	public void setModeFlag(final boolean modeFlag) {
		this.modeFlag = modeFlag;
	}

	/**
	 * Imposta la nota di richiesta eliminazione.
	 * @param notaRichiestaEliminazione
	 */
	public void setNotaRichiestaEliminazione(final String notaRichiestaEliminazione) {
		this.notaRichiestaEliminazione = notaRichiestaEliminazione;
	}

	/**
	 * Imposta la nota richiesta modifica.
	 * @param notaRichiestaModifica
	 */
	public void setNotaRichiestaModifica(final String notaRichiestaModifica) {
		this.notaRichiestaModifica = notaRichiestaModifica;
	}

	/**
	 * Imposta l'helper per la gestione del datatable delle notifiche richieste.
	 * @param notificheRichiesteDTH
	 */
	public void setNotificheRichiesteDTH(final SimpleDetailDataTableHelper<NotificaContatto> notificheRichiesteDTH) {
		this.notificheRichiesteDTH = notificheRichiesteDTH;
	}

	/**
	 * Imposta il numero elementi.
	 * @param numElementi
	 */
	public void setNumElementi(final int numElementi) {
		this.numElementi = numElementi;
	}

	/**
	 * Imposta il numero elementi notifiche.
	 * @param numElementiNotifiche
	 */
	public void setNumElementiNotifiche(final int numElementiNotifiche) {
		this.numElementiNotifiche = numElementiNotifiche;
	}

	/**
	 * Imposta il numero giorni.
	 * @param numGiorni
	 */
	public void setNumGiorni(final int numGiorni) {
		this.numGiorni = numGiorni;
	}
	
	/**
	 * Imposta il numero giorni notifiche.
	 * @param numGiorniNotifiche
	 */
	public void setNumGiorniNotifiche(final int numGiorniNotifiche) {
		this.numGiorniNotifiche = numGiorniNotifiche;
	}

	/**
	 * Imposta il tipo operazione.
	 * @param operazioneEnum
	 */
	public void setOperazioneEnum(final TipoOperazioneEnum operazioneEnum) {
		this.operazioneEnum = operazioneEnum;
	}

}
