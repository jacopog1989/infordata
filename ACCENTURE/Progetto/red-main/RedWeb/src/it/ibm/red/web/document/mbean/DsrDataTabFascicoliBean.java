package it.ibm.red.web.document.mbean;

import java.util.List;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.document.mbean.interfaces.ITabFascicoliBean;

/**
 * Bean che gestisce il datatab dei fascicoli DSR.
 */
public class DsrDataTabFascicoliBean implements ITabFascicoliBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -7649937180603306706L;

	/**
	 * Dettaglio.
	 */
	private DetailDocumentRedDTO detail;
	
	/**
	 * Lista fascicoli.
	 */
	private List<FascicoloDTO> fascicoli;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DsrDataTabFascicoliBean.class);

	/**
	 * Costruttore con dettaglio.
	 * @param inDetail
	 */
	public DsrDataTabFascicoliBean(final DetailDocumentRedDTO inDetail) {
		detail = inDetail;
		fascicoli = detail.getFascicoli();
	}

	/**
	 * Gestisce la rimozione del fascicolo identificato dall'index.
	 * @param index
	 */
	@Override
	public void rimuoviFascicolo(final Object index) {
		try {
			final Integer i = (Integer) index;
			fascicoli.remove(i.intValue());

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}
	
	
	/*########### Getter e Setter ###########*/
	/**
	 * Restituisce il dettaglio.
	 * @return detail.
	 */
	@Override
	public DetailDocumentRedDTO getDetail() {
		return detail;
	}

	/**
	 * Imposta il detail.
	 * @param detail
	 */
	public void setDetail(final DetailDocumentRedDTO detail) {
		this.detail = detail;
	}

	/**
	 * Restituisce la lista dei fascicoli.
	 * @return List di Fascicoli
	 */
	@Override
	public List<FascicoloDTO> getFascicoli() {
		return fascicoli;
	}

	/**
	 * Imposta la lista dei fascicoli.
	 * @param fascicoli
	 */
	@Override
	public void setFascicoli(final List<FascicoloDTO> fascicoli) {
		this.fascicoli = fascicoli;
	}
}
