package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.DecretoDirigenzialeDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IFepaFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;

/**
 * Bean che gestisce l'associazione dei decreti.
 */
@Named(ConstantsWeb.MBean.ASSOCIA_DECRETI_BEAN)
@ViewScoped
public class AssociaDecretiBean extends AbstractAssociazioniDecretiResponse {
	
	/**
	 * La costante Serial Version UID.
	 */
	private static final long serialVersionUID = -4587482853586837451L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AssociaDecretiBean.class.getName());

	/**
	 * Post construct del bean.
	 */
	@PostConstruct
	@Override
	protected void postConstruct() {
		super.postConstruct();
	}
	
	/**
	 * Esecuzione della response.
	 */
	@Override
	public void doResponse() {
		EsitoOperazioneDTO eOpe = null;
		String wobNumber = null;
		final List<String> wobNumbersDaAggiungere = new ArrayList<>();

		try {
			final IFepaFacadeSRV fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);
			//pulisco gli esiti
			ldb.cleanEsiti();
			//prendo il wobnumber
			wobNumber = documentiSelezionati.get(0).getWobNumber();
			//Prendo i decreti selezionati
			final List<DecretoDirigenzialeDTO> decretiSelezionati = decretiDisponibiliTab.getDecretiDTH().getSelectedMasters();
			
			//prendo i wobnumbers da aggiungere
			for (final DecretoDirigenzialeDTO decreto : decretiSelezionati) {
				if (decreto.getWobNumber() != null) {
					wobNumbersDaAggiungere.add(decreto.getWobNumber());
				}
			}
			
			//eseguo la response in base alla response richiamata
			eOpe = fepaSRV.associaDecreto(utente, wobNumber, wobNumbersDaAggiungere);
			
			//setto gli esiti con i nuovi e eseguo un refresh
			ldb.getEsitiOperazione().add(eOpe);
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.ASSOCIA_DECRETI_BEAN);			
			
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il tentato sollecito della response " + ldb.getSelectedResponse().getDisplayName(), e);
			showError("Si è verificato un errore durante il tentato sollecito della response " + ldb.getSelectedResponse().getDisplayName());
		}
	}
}
