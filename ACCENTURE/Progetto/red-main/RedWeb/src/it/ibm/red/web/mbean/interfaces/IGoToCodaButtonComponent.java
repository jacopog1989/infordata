package it.ibm.red.web.mbean.interfaces;

/**
 * Serve a implementare il pannello che si apre dopo aver cliccato il bottone affianco a una riga di una tabella di ricerca.
 * Mostra una lista di code in cui l'utente può trovare il documento rappresentato da quella riga.
 */
public interface IGoToCodaButtonComponent extends IGoToCodaComponent, IDettaglioLoadableAfterPreview {
	
}
