package it.ibm.red.web.servlets;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Date;
import java.util.EnumSet;
import java.util.Optional;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

import com.google.common.net.MediaType;

import it.ibm.red.business.crypt.DesCrypterNew;
import it.ibm.red.business.dto.AttachmentDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.PlaceholderInfoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.LogDownloadContentEnum;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.helper.adobe.AdobeLCHelper;
import it.ibm.red.business.helper.adobe.IAdobeLCHelper;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.dto.DocumentoNpsDTO;
import it.ibm.red.business.persistence.model.TipoFile;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.ITipoFileSRV;
import it.ibm.red.business.service.facade.IContributoFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.INpsFacadeSRV;
import it.ibm.red.business.service.facade.IScompattaMailFacadeSRV;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb.SessionObject;
import it.ibm.red.web.utils.PlaceHolderUtils;
import it.ibm.red.web.utils.RequestUtils;

/**
 * The Class DownloadContentServlet.
 *
 * @author CPIERASC
 * 
 *         Servlet download content.
 */
public class DownloadContentServlet extends HttpServlet {

	/**
	 * TAG visualizzazione Id nodo.
	 */
	private static final String IDNODO_TAG = " IDNODO=";

	/**
	 * TAG visualizzazione Id ruolo.
	 */
	private static final String IDRUOLO_TAG = " IDRUOLO=";

	/**
	 * TAG visualizzazione Username.
	 */
	private static final String USERNAME_TAG = "\n USERNAME=";

	/**
	 * Nome file and.
	 */
	private static final String ANTEPRIMA_NON_DISPONIBILE_PDF = "anteprimaNonDisponibile.pdf";

	/**
	 * Nome file and per ga.
	 */
	private static final String ANTEPRIMA_GESTIONE_APPLICATIVA_PDF = "anteprimaGestioneApplicativa.pdf";

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DownloadContentServlet.class.getName());

	/**
	 * Service contributo.
	 */
	private final IContributoFacadeSRV contributoSRV = ApplicationContextProvider.getApplicationContext().getBean(IContributoFacadeSRV.class);

	/**
	 * Service documento.
	 */
	private final IDocumentoFacadeSRV documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);

	/**
	 * Service utente.
	 */
	private final IUtenteFacadeSRV utenteSRV = ApplicationContextProvider.getApplicationContext().getBean(IUtenteFacadeSRV.class);

	/**
	 * Service.
	 */
	private final ITipoFileSRV tipoFileSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipoFileSRV.class);

	/**
	 * REQUEST_PARAMETERS_NAME.
	 */
	private static final String REQUEST_PARAMETERS_NAME = "params";
	/**
	 * REQUEST_PARAMETERS_SEPARATOR.
	 */
	private static final String REQUEST_PARAMETERS_SEPARATOR = "&";
	/**
	 * PARAMETER_SEPARATOR.
	 */
	private static final String PARAMETER_SEPARATOR = "=";
	/**
	 * ID_POSITION.
	 */
	private static final int ID_POSITION = 0;
	/**
	 * USERNAME_POSITION.
	 */
	private static final int USERNAME_POSITION = 1;
	/**
	 * IDRUOLO_POSITION.
	 */
	private static final int IDRUOLO_POSITION = 2;
	/**
	 * IDNODO_POSITION.
	 */
	private static final int IDNODO_POSITION = 3;

	/**
	 * Mime type position.
	 */
	private static final int MIMETYPE_POSITION = 5;

	
	/**
	 * PDF FILENAME DOT POSITION.
	 */
	private static final int PDF_FILENAME_DOT_POSITION = 4;

	/**
	 * Instantiates a new download content servlet.
	 *
	 * @see HttpServlet#HttpServlet()
	 */
	public DownloadContentServlet() {
		super();
	}

	@Override
	protected final void doPost(final HttpServletRequest request, final HttpServletResponse response) {
		doGet(request, response);
	}

	/**
	 * doGet.
	 *
	 * @param request  the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) {
		byte[] output = null;
		String fileName = ANTEPRIMA_NON_DISPONIBILE_PDF;
		String mimeType = null;
		String paramEncrypt = null;
		String id = null;
		String username = null;
		Long idRuolo = null;
		Long idNodo = null;
		DocumentTypeEnum infoDocumentType = null;
		UtenteDTO utente = null;
		String[] paramsSplit = null;
		boolean doLog = !Boolean.TRUE.toString().equals(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DISABLE_LOG_DOWNLOAD_CONTENT_SERVLET));
		boolean contentLibroFirma = false;
		

		boolean isFilePrincipale = false;
		boolean isAllegato = false;

		// Parametri per allegato
		boolean mantieniFormatoOriginaleAll = false;
		boolean campoFirmaAll = false;
		boolean stampigliaturaSiglaAllegato = false;
		boolean daFirmareAll = false;
		String guidAllegato = "";
		try {
			if (doLog) {
				LOGGER.info("### START DownloadContentServlet ###");
			}
			paramEncrypt = request.getParameter(REQUEST_PARAMETERS_NAME);
			final String isDownloadString = request.getParameter("isDownload");
			boolean isDownload = false;
			if("true".equals(isDownloadString)) {
				isDownload = true;
			} 
			if (request.getParameter("contentLibroFirma") != null) {
				contentLibroFirma = Boolean.valueOf(request.getParameter("contentLibroFirma"));
			}
			if(doLog) {
				LOGGER.info("PARAM ENCRYPT : "+paramEncrypt);
			}
			String flagCodifica = ""; 
			int paramEncryptIndex = paramEncrypt.lastIndexOf("-"); 
			if (paramEncryptIndex!=-1) {
				String substringRequest = paramEncrypt.substring(0, paramEncryptIndex); 
				flagCodifica = paramEncrypt.substring(paramEncryptIndex+1);
				paramEncrypt = substringRequest;
			}
			if (paramEncrypt != null && paramEncrypt.trim().length() > 0) {
				String urlEncoCrypt = flagCodifica; // URL ENC
				if ("true".equalsIgnoreCase(urlEncoCrypt)) {
					paramEncrypt = URLDecoder.decode(paramEncrypt, StandardCharsets.ISO_8859_1.name());
					if (doLog) {
						LOGGER.info("PARAM ENCRYPT DECODIFICA URL ENC : " + paramEncrypt);
					}
				}
				if (doLog) {
					LOGGER.info("### START decrypt parametri ###");
				}
				paramEncrypt = paramEncrypt.replace(" ", "+");
				if (doLog) {
					LOGGER.info("ParamEncrypt : "+ paramEncrypt);   
				}

				final String paramDecrypted = DesCrypterNew.decrypt(doLog, paramEncrypt);
				LOGGER.info("paramDecrypted :"+ paramDecrypted); 	 
				
				paramsSplit = paramDecrypted.split(REQUEST_PARAMETERS_SEPARATOR);

				id = paramsSplit[ID_POSITION].split(PARAMETER_SEPARATOR)[1];

				if ("null".equals(id)) {
					id = null;
				}

				username = paramsSplit[USERNAME_POSITION].split(PARAMETER_SEPARATOR)[1];
				idRuolo = Long.parseLong(paramsSplit[IDRUOLO_POSITION].split(PARAMETER_SEPARATOR)[1]);
				idNodo = Long.parseLong(paramsSplit[IDNODO_POSITION].split(PARAMETER_SEPARATOR)[1]);
				
				final String[] arrayMimeFile = paramsSplit[MIMETYPE_POSITION].split(PARAMETER_SEPARATOR);
				if (arrayMimeFile.length > 1) {
					mimeType = arrayMimeFile[1];
				}
				
				if (doLog) {
					LOGGER.info("### STOP decrypt parametri ###");
				}
			}
			if (org.apache.commons.lang3.StringUtils.isNotBlank(id)) {

				if (doLog) {
					LOGGER.info("### START get utente ###");
				}

				infoDocumentType = DocumentTypeEnum.getDocumentTypeFromPrefix(id);

				final String extractedId = infoDocumentType.getId(id);

				utente = utenteSRV.getByUsername(username, idRuolo, idNodo);

				if (doLog) {
					LOGGER.info("### STOP get utente ###");
				}

				if (utente != null) {
					FilenetCredentialsDTO fcDTO = utente.getFcDTO();
					if (PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.RICERCA_RISERVATO)) {
						fcDTO = new FilenetCredentialsDTO(utente.getFcDTO());
					} 
					if (utente.isGestioneApplicativa()) {
						 if(doLog) {
							 LOGGER.info("### START recupero anteprima GA ###");
						 }
						// L'utente in questione non ha i diritti per accedere ai content del file. Mostriamo il PDF informativo
						output = FileUtils.getFile(DownloadContentServlet.class.getClassLoader(), ANTEPRIMA_GESTIONE_APPLICATIVA_PDF);
						mimeType = MediaType.PDF.toString();
						fileName = ANTEPRIMA_GESTIONE_APPLICATIVA_PDF;
						if (doLog) {
							LOGGER.info("### STOP recupero anteprima GA ###");
						}
					} else if (infoDocumentType.isflagAnd(id)) {
						if (doLog) {
							LOGGER.info("### START recupero anteprima non disponibile ###");
						}
						// Il file in questione è un allegato che sappiamo non essere un PDF, per
						// evitare l'accesso a FileNet
						// si restituisce immediatamente il PDF di "Anteprima non Disponibile".
						output = FileUtils.getFile(DownloadContentServlet.class.getClassLoader(), ANTEPRIMA_NON_DISPONIBILE_PDF);
						mimeType = MediaType.PDF.toString();
						fileName = ANTEPRIMA_NON_DISPONIBILE_PDF;
						if (doLog) {
							LOGGER.info("### STOP recupero anteprima non disponibile ###");
						}
					} else if (DocumentTypeEnum.ALLEGATO.equals(infoDocumentType)) {
						if (doLog) {
							LOGGER.info("### START recupero allegato ###");
						}
						
						final PlaceholderInfoDTO placeholderInfoAll = new PlaceholderInfoDTO();
						
						// Devo accedere a FileNet e preparare l'anteprima, da verificare in seguito se il mime type è PDF.
						AttachmentDTO attachment = documentoSRV.getContentAllegato4Preview(extractedId, fcDTO, isDownload, placeholderInfoAll, utente.getIdAoo(), contentLibroFirma);
						output = attachment.getContent();
						mimeType = attachment.getMimeType();
						fileName = !StringUtils.isNullOrEmpty(attachment.getFileName()) ? attachment.getFileName() : "NOME_FILE_NON_TROVATO";
						isAllegato = true;
						if(placeholderInfoAll!=null) {
							guidAllegato = placeholderInfoAll.getGuidAllegato();
							daFirmareAll = placeholderInfoAll.getDaFirmare()!=null && placeholderInfoAll.getDaFirmare();
							mantieniFormatoOriginaleAll = placeholderInfoAll.getMantieniFormatoOriginale();
							IAdobeLCHelper alch = new AdobeLCHelper(); 
							campoFirmaAll = daFirmareAll && ((placeholderInfoAll.getFirmaVisibile()!=null && placeholderInfoAll.getFirmaVisibile() ) || (alch.hasTagFirmatario(attachment) != - 1 && alch.hasTagFirmatario(attachment) != - 0));
							stampigliaturaSiglaAllegato = placeholderInfoAll.getStampigliaturaSiglaAllegato();
						}
						
						if (doLog) {
							LOGGER.info("### STOP recupero allegato ###");
						}
					} else if (DocumentTypeEnum.CONTRIBUTO.equals(infoDocumentType)) {
						if (doLog) {
							LOGGER.info("### START recupero contributo ###");
						}
						// Il file in questione è un contributo, si deve accedere a FileNet e preparare l'anteprima.
						FileDTO file = documentoSRV.getContributoContentPreview(fcDTO, extractedId, utente.getIdAoo());
						output = file.getContent();

						mimeType = file.getMimeType();
						fileName = file.getFileName();
						if (doLog) {
							LOGGER.info("### STOP recupero contributo ###");
						}
					} else if (DocumentTypeEnum.CONTRIBUTO_ESTERNO_OTF.equals(infoDocumentType)) {
						if (doLog) {
							LOGGER.info("### START recupero contributo OTF ###");
						}
						// È richiesta la creazione di un contributo on the fly sulla base della nota
						// del contributo.
						final FileDTO file = contributoSRV.getContent(utente, true, Long.valueOf(extractedId));
						output = file.getContent();
						mimeType = file.getMimeType();
						fileName = file.getFileName().substring(0, file.getFileName().length() - PDF_FILENAME_DOT_POSITION) + extractedId
								+ file.getFileName().substring(file.getFileName().length() - PDF_FILENAME_DOT_POSITION, file.getFileName().length());
						if (doLog) {
							LOGGER.info("### STOP recupero contributo OTF ###");
						}
					} else if (DocumentTypeEnum.CONTRIBUTO_INTERNO_OTF.equals(infoDocumentType)) {
						if (doLog) {
							LOGGER.info("### START recupero nota OTF ###");
						}
						// È richiesta la creazione di un contributo on the fly sulla base della nota
						// del contributo.
						final FileDTO file = contributoSRV.getPDFNotaOFT(utente, false, Long.valueOf(extractedId));
						output = file.getContent();
						mimeType = file.getMimeType();
						fileName = file.getFileName().substring(0, file.getFileName().length() - PDF_FILENAME_DOT_POSITION) + extractedId
								+ file.getFileName().substring(file.getFileName().length() - PDF_FILENAME_DOT_POSITION, file.getFileName().length());
						if (doLog) {
							LOGGER.info("### STOP recupero nota OTF ###");
						}
					} else if (DocumentTypeEnum.PROTOCOLLA_MAIL.equals(infoDocumentType) || DocumentTypeEnum.ISPEZIONA_MAIL.equals(infoDocumentType)) {
						if (doLog) {
							LOGGER.info("### START protocolla mail ###");
						}
						// Il comportamenteo per le mail è che viene restituito il PDF di "Anteprima non
						// Disponibile" quando non sono PDF
						if (org.apache.commons.lang3.StringUtils.contains(mimeType, "pdf")) {
							final IScompattaMailFacadeSRV scompattaMailSRV = ApplicationContextProvider.getApplicationContext().getBean(IScompattaMailFacadeSRV.class);
							output = scompattaMailSRV.getContentutoDiscompattataMail(utente, extractedId);
						} else {
							output = FileUtils.getFile(DownloadContentServlet.class.getClassLoader(), ANTEPRIMA_NON_DISPONIBILE_PDF);
							mimeType = MediaType.PDF.toString();
							fileName = ANTEPRIMA_NON_DISPONIBILE_PDF;
						}
						if (doLog) {
							LOGGER.info("### STOP protocolla mail ###");
						}
					} else if (DocumentTypeEnum.NPS.equals(infoDocumentType)) {
						if (doLog) {
							LOGGER.info("### START NPS ###");
						}
						final INpsFacadeSRV npsSRV = ApplicationContextProvider.getApplicationContext().getBean(INpsFacadeSRV.class);
						final DocumentoNpsDTO d = npsSRV.downloadDocumento(utente.getIdAoo().intValue(), extractedId, false);
						final ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
						IOUtils.copy(d.getInputStream(), byteArray);
						output = byteArray.toByteArray();
						mimeType = d.getContentType();
						fileName = d.getNomeFile();
						if (doLog) {
							LOGGER.info("### STOP NPS ###");
						}
					} else if (DocumentTypeEnum.SIGI.equals(infoDocumentType)) {
						if (doLog) {
							LOGGER.info("### START SIGI ###");
						}
						final FileDTO file = documentoSRV.getDocumentoSIGIContentPreview(fcDTO, extractedId, utente.getIdAoo());
						output = file.getContent();
						mimeType = file.getMimeType();
						fileName = file.getFileName();
						if (doLog) {
							LOGGER.info("### STOP SIGI ###");
						}
					} else if (DocumentTypeEnum.MEF_REGISTRO.equals(infoDocumentType)) {
						if (doLog) {
							LOGGER.info("### START REGISTRO MEF ###");
						}
						final FileDTO file = documentoSRV.getRegistroProtocolloContentPreview(fcDTO, extractedId, utente.getIdAoo());
						output = file.getContent();
						mimeType = file.getMimeType();
						fileName = file.getFileName();
						if (doLog) {
							LOGGER.info("### STOP REGISTRO MEF ###");
						}
					} else if (DocumentTypeEnum.NPS_REGISTRO.equals(infoDocumentType)) {
						if (doLog) {
							LOGGER.info("### START REGISTRO NPS ###");
						}
						final HttpSession session = request.getSession();
						if (session != null) {
							final FileDTO file = (FileDTO) session.getAttribute(SessionObject.NPS_REGISTRO_ITEM_FILE);
							session.removeAttribute(SessionObject.NPS_REGISTRO_ITEM_FILE);

							if (file != null) {
								output = file.getContent();
								mimeType = file.getMimeType();
								fileName = file.getFileName();
							}
						}
						if (doLog) {
							LOGGER.info("### STOP REGISTRO NPS ###");
						}
					} else if (DocumentTypeEnum.CARTACEO.equals(infoDocumentType)) {
						if (doLog) {
							LOGGER.info("### START DOC CARTACEO ###");
						}
						final FileDTO file = documentoSRV.getDocumentoCartaceoContentPreview(fcDTO, extractedId, utente.getIdAoo());
						
						// Se il cartaceo non è stato caricato come PDF, si mostra il file di "Anteprima Non Disponibile"
						if (file.getContent() != null) {
							output = file.getContent();
							mimeType = file.getMimeType();
							fileName = extractedId;
						} else {
							output = FileUtils.getFile(DownloadContentServlet.class.getClassLoader(), ANTEPRIMA_NON_DISPONIBILE_PDF);
							mimeType = MediaType.PDF.toString();
							fileName = ANTEPRIMA_NON_DISPONIBILE_PDF;
						}
						if (doLog) {
							LOGGER.info("### STOP DOC CARTACEO ###");
						}
					} else {
						// Il file in questione è un documento principale, se non firmato PDF,
						// altrimenti potrebbe essere un P7M.
						FileDTO documento = null;
						isFilePrincipale = true;
						if (DocumentTypeEnum.DOWNLOAD.equals(infoDocumentType)) {

							if (doLog) {
								LOGGER.info("### START DOWNLOAD ###");
							}
						 	documento = documentoSRV.getDocumentoContentPreview(fcDTO, extractedId, false, utente.getIdAoo(), false);
							output = documento.getContent();
							mimeType = documento.getMimeType();
							fileName = documento.getFileName();
							if (doLog) {
								LOGGER.info("### STOP DOWNLOAD ###");
							}
						} else {
							if (doLog) {
								LOGGER.info("### START NON DOWNLOAD ###");
							}
							documento = documentoSRV.getDocumentoContentPreview(fcDTO, extractedId, true, utente.getIdAoo(), contentLibroFirma);
							
							if ((id == null || id.trim().length() == 0) 
									|| (infoDocumentType != DocumentTypeEnum.DOWNLOAD && (!MediaType.PDF.toString().equalsIgnoreCase(documento.getMimeType())
											|| (documento.getContent() != null && documento.getContent().length == 0)))) {

								output = FileUtils.getFile(DownloadContentServlet.class.getClassLoader(), ANTEPRIMA_NON_DISPONIBILE_PDF);
								mimeType = MediaType.PDF.toString();
								fileName = ANTEPRIMA_NON_DISPONIBILE_PDF;
							} else {
								output = documento.getContent();
								mimeType = documento.getMimeType();
								fileName = documento.getFileName();
							}
							if (doLog) {
								LOGGER.info("### STOP NON DOWNLOAD ###");
							}

						}

						// Richiesta di download e il documento principale è un PDF -> si aggiungono i
						// suoi allegati
						if (DocumentTypeEnum.DOWNLOAD.equals(infoDocumentType) && MediaType.PDF.toString().equalsIgnoreCase(mimeType)) {
							if (doLog) {
								LOGGER.info("### START DOWNLOAD PDF ###");
							}
							// Aggiungo gli allegati al PDF
							final Collection<AttachmentDTO> attachments = documentoSRV.getAttachments(fcDTO, documento.getDescription(), utente.getIdAoo());

							if (attachments != null && !attachments.isEmpty()) {
								final AttachmentDTO a = new AttachmentDTO("[O] " + documento.getFileName(), documento.getContent(), documento.getMimeType(),
										documento.getDescription(), true, null);
								attachments.add(a);
							}
							output = PdfHelper.addAttachment(documento.getContent(), attachments);
							if (doLog) {
								LOGGER.info("### STOP DOWNLOAD PDF ###");
							}
						}

					}
				}
				
				if(!isDownload && (isFilePrincipale || isAllegato) && fileName!=null 
						&& !fileName.equals(ANTEPRIMA_NON_DISPONIBILE_PDF) && !"PREVIEW.pdf".equals(fileName) && !fileName.equals(ANTEPRIMA_GESTIONE_APPLICATIVA_PDF)) {
					
					output = gestisciTrasformazione(request, utente, fileName, mimeType, output, 
							isFilePrincipale, isAllegato, guidAllegato, campoFirmaAll, mantieniFormatoOriginaleAll, stampigliaturaSiglaAllegato);
					
				}
				
			}

			/// ### FIX PER IL NOME FILE CON TRATTINI (-, –, —, _) CHE VIENE BLOCCATO
			/// DALL'OAM
			fileName = StringUtils.sostituisciTrattiniPerOAM(fileName);

			// Si visualizza il PDF di "Anteprima non disponibile" se:
			// a) ID del file non passato alla servlet
			// b) Visualizzazione nell'applicazione e il documento principale non è un PDF
			// oppure il content è vuoto
			if (utente != null) {
				final String contentType = mimeType + "; name=\"" + fileName + "\"";

				response.setContentType(contentType);
				// Se si tratta di un pdf visualizza il file in pagina, altrimenti scaricalo!
				String header;
				if (MediaType.PDF.toString().equalsIgnoreCase(mimeType)) {
					header = " inline;";
				} else {
					header = " attachment;";
				}

				header += " filename=\"" + fileName + "\"";
				response.setHeader("Content-Disposition", header);
				response.getOutputStream().write(output);
				if (output != null) {
					response.setContentLength(output.length);
				} else {
					response.setContentType(MediaType.PDF.toString());
				}
			}
			
			if (utente == null && doLog) {
				LOGGER.error("Errore nel recupero del content. REQUEST_PARAMETERS_NAME=" + paramEncrypt + USERNAME_TAG + username + IDRUOLO_TAG + idRuolo 
					+ IDNODO_TAG + idNodo);
			}
		} catch (final Exception e) {
			if (doLog) {
				LOGGER.error("Errore nel recupero del content. REQUEST_PARAMETERS_NAME=" + paramEncrypt + USERNAME_TAG + username + IDRUOLO_TAG + idRuolo
						+ IDNODO_TAG + idNodo, e);
			}

			if (output == null) {
				response.setContentType(MediaType.PDF.toString());
				try {
					response.getOutputStream().write(FileUtils.getFile(DownloadContentServlet.class.getClassLoader(), ANTEPRIMA_NON_DISPONIBILE_PDF));
				} catch (final IOException e1) {
					if (doLog) {
						LOGGER.error("Errore nel recupero dell'anteprima non disponibile. REQUEST_PARAMETERS_NAME=" + paramEncrypt + USERNAME_TAG + username + IDRUOLO_TAG + idRuolo
								+ IDNODO_TAG + idNodo, e1);
					}
				}
			}
		}
		try {
			LogDownloadContentEnum content = LogDownloadContentEnum.DOCUMENTO_ORIGINALE;

			if (infoDocumentType != null) {

				if (infoDocumentType == DocumentTypeEnum.ALLEGATO) {
					content = LogDownloadContentEnum.ALLEGATO;
				} else if (infoDocumentType == DocumentTypeEnum.CONTRIBUTO) {
					content = LogDownloadContentEnum.CONTRIBUTO;
				}
			}

			if (utente != null && infoDocumentType != null && !StringUtils.isNullOrEmpty(infoDocumentType.getId(id)) && !infoDocumentType.isflagAnd(id)) {
				if (doLog) {
					LOGGER.info("### START DB LOG ###");
				}

				utenteSRV.logDownload(utente.getId(), utente.getIdRuolo(), utente.getIdUfficio(), infoDocumentType.getId(id), content, RequestUtils.getIp(request),
						RequestUtils.getOS(request), RequestUtils.getBrowser(request), RequestUtils.getUserAgent(request), "EVO");
				if (doLog) {
					LOGGER.info("### STOP DB LOG ###");
				}
			}
		} catch (final Exception e) {
			if (doLog) {
				LOGGER.error("Errore nel logging del recupero del content.", e);
			}
		}
		if (doLog) {
			LOGGER.info("### STOP DownloadContentServlet ###");
		}

	}
	
	/**
	 * Gestisce la visualizzaizone di file post trasformazione PDF e apposizione segni grafici e campi firma.
	 * 
	 * @param request
	 * @param utente
	 * @param fileName
	 * @param mimeType
	 * @param byteArrayDaTrasformare
	 * @param isFilePrincipale
	 * @param isAllegato
	 * @param guidAllegato
	 * @param campoFirmaAll
	 * @param mantieniFormatoOriginaleAll
	 * @param stampigliaturaSiglaAllegato
	 * @return
	 */
	private byte[] gestisciTrasformazione(final HttpServletRequest request, final UtenteDTO utente, 
			final String fileName, final String mimeType, final byte[] byteArrayDaTrasformare, final boolean isFilePrincipale, final boolean isAllegato,
			final String guidAllegato, final boolean campoFirmaAll, final boolean mantieniFormatoOriginaleAll, final boolean stampigliaturaSiglaAllegato) {
		
		byte[] output = null;
		
		final Integer categoriaDocumento = Integer.parseInt(request.getParameter("isDocumentoIngresso"));
		boolean conCampoFirma = false;
		boolean inEntrata = true;
		if (CategoriaDocumentoEnum.USCITA.equals(CategoriaDocumentoEnum.get(categoriaDocumento))) {
			conCampoFirma = true;
			inEntrata = false;
		}

		boolean convertibile = false;
		if (!"NOME_FILE_NON_TROVATO".equals(fileName)) {
			final Collection<TipoFile> getAll = tipoFileSRV.getAll();
			convertibile = tipoFileSRV.isConvertibile(fileName, getAll);
		}
		final Integer firmaMultiplaInt = Integer.parseInt(request.getParameter("idTipoAssegnazione"));
		final boolean firmaMultipla = TipoAssegnazioneEnum.FIRMA_MULTIPLA.equals(TipoAssegnazioneEnum.get(firmaMultiplaInt));

		final Integer idTipologiaDocumento = Integer.parseInt(request.getParameter("idTipologiaDocumento"));
		final Long idUfficioCreatore = Long.parseLong(request.getParameter("idUfficioCreatore"));
		final FileDTO fileDaTrasformare = new FileDTO(fileName, byteArrayDaTrasformare, mimeType);
		
		if(isFilePrincipale) {
		
			output = PlaceHolderUtils.inserisciPlaceholderFirmaSigla(fileDaTrasformare, utente.getIdAoo(),
					conCampoFirma, true, inEntrata, true, false, false, convertibile,
					firmaMultipla, true, true, false, null, null, null, true);
		
		} else if (isAllegato) {
			
			final String[] siglatarioPrincipaleString = null;
			Long idUtenteFirmatario = null;
			final PlaceholderInfoDTO placeholderInfo = new PlaceholderInfoDTO();
			placeholderInfo.setDaFirmare(false);
			
			if (TipoAssegnazioneEnum.get(firmaMultiplaInt) != null) {
				idUtenteFirmatario = documentoSRV.getFilePrincipaleFromAllegato(utente.getFcDTO(), guidAllegato, true, true, utente,
						TipoAssegnazioneEnum.get(firmaMultiplaInt), siglatarioPrincipaleString, placeholderInfo);
			}
			
			if(placeholderInfo.getDaFirmare()!=null && !placeholderInfo.getDaFirmare()) {
				output = PlaceHolderUtils.inserisciPlaceholderFirmaSigla(fileDaTrasformare, utente.getIdAoo(),
						campoFirmaAll, true, inEntrata, true, mantieniFormatoOriginaleAll, false, convertibile,
						firmaMultipla, false, true, stampigliaturaSiglaAllegato, idUtenteFirmatario, idUfficioCreatore, idTipologiaDocumento, true);
			}
			
		}
		
		return output;
	}

	/**
	 * Enum che definisce i tipi di documento gestiti.
	 */
	public enum DocumentTypeEnum {

		/**
		 * Allegato.
		 */
		ALLEGATO("a", 1),

		/**
		 * Contributo.
		 */
		CONTRIBUTO("c", 1),

		/**
		 * Contributo interno.
		 */
		CONTRIBUTO_INTERNO_OTF("cin", 1),

		/**
		 * Contributo esterno.
		 */
		CONTRIBUTO_ESTERNO_OTF("cen", 1),

		/**
		 * Download.
		 */
		DOWNLOAD("d", 1),

		/**
		 * Protocolla mail.
		 */
		PROTOCOLLA_MAIL("daProtocollaMail", 1),

		/**
		 * Ispezione mail.
		 */
		ISPEZIONA_MAIL("daIspezionaMail", 1),

		/**
		 * Nps.
		 */
		NPS("nps", 1),

		/**
		 * Sigi.
		 */
		SIGI("sigi", 1),

		/**
		 * Registro MEF.
		 */
		MEF_REGISTRO("mefRegistro", 1),

		/**
		 * Registro NPS.
		 */
		NPS_REGISTRO("npsRegistro", 1),

		/**
		 * Cartaceo.
		 */
		CARTACEO("cartaceo", 1),

		/**
		 * None.
		 */
		NONE("", 0);

		/**
		 * Prefisso.
		 */
		private String prefisso;

		/**
		 * Posizione.
		 */
		private int idPosition;

		DocumentTypeEnum(final String prefisso, final int idPosition) {
			this.prefisso = prefisso;
			this.idPosition = idPosition;
		}

		/**
		 * Restituisce il documentType del documento in base al prefisso.
		 * 
		 * @param id
		 * @return documentType
		 */
		public static DocumentTypeEnum getDocumentTypeFromPrefix(final String id) {
			final String[] tokens = id.split("_", 2);
			final String tipo = tokens[0];

			final EnumSet<DocumentTypeEnum> set = EnumSet.allOf(DocumentTypeEnum.class);
			set.remove(NONE);
			final Optional<DocumentTypeEnum> match = set.stream().filter(e -> e.prefisso.equalsIgnoreCase(tipo)).findFirst();
			return match.isPresent() ? match.get() : NONE;
		}

		/**
		 * Restituisce l'id.
		 * 
		 * @param id
		 * @return id
		 */
		public String getId(final String id) {
			final String[] tokens = id.split("_", 2);
			return tokens[this.idPosition];
		}

		/**
		 * Restituisce il prefisso.
		 * 
		 * @return prefisso
		 */
		public String getPrefix() {
			return this.prefisso;
		}

		/**
		 * Restituisce true se il flag è AND, false altrimenti.
		 * 
		 * @param id
		 * @return true se il flag è AND, false altrimenti
		 */
		public boolean isflagAnd(final String id) {
			if (this == ALLEGATO || this == CONTRIBUTO) {
				final String pseudoId = this.getId(id);
				return "and".equalsIgnoreCase(pseudoId);
			}
			return false;
		}

		/**
		 * Costruisce l'id affinche la contentServlet interpreti il giusto documento e
		 * implementi il download corretto.
		 * 
		 * Nel caso l'id sia blank or null
		 * 
		 * @param id
		 * 
		 * @return
		 */
		public String createId(final String id) {
			String encryptedId = id;
			if (org.apache.commons.lang3.StringUtils.isBlank(id)) {
				encryptedId = "and";
			}
			if (prefisso.isEmpty()) {
				return id;
			}
			return prefisso.concat("_").concat(encryptedId);
		}
	}

	/**
	 * Genera l'url di encoding per invocare la servlet dei documenti scaricabili da
	 * filenet per documentTitle.
	 * 
	 * @param id       documentTitle utilizzato per pescare il documento
	 * @param utente   utente che esegue l'operazione
	 * @param nomeFile Nome del file che si deve scaricare
	 * @return l'url con l'encoding con i parametri per generare la servlet
	 * @throws UnsupportedEncodingException
	 */
	public static final String getCryptoDocParamById(final String id, final UtenteDTO utente) throws UnsupportedEncodingException {
		return getCryptoDocParamById(id, null, utente);
	}

	/**
	 * Genera l'url di encoding per invocare la servlet.
	 * 
	 * @param id           documentTitle utilizzato per pescare il documento
	 * @param documentType Enum per generare il prefisso che permette alla
	 *                     contentServlet di capire di quale documento si tratta
	 * @param utente       utente che esegue l'operazione
	 * @return l'url con l'encoding con i parametri per generare la servlet
	 * @throws UnsupportedEncodingException
	 */
	public static final String getCryptoDocParamById(final String id, final DocumentTypeEnum documentType, final UtenteDTO utente) throws UnsupportedEncodingException {
		return getCryptoDocParamById(id, documentType, utente, "", "");
	}

	/**
	 * Genera l'url di encoding per invocare la servlet.
	 * 
	 * @param id           documentTitle utilizzato per pescare il documento
	 * @param documentType Enum per generare il prefisso che permette alla
	 *                     contentServlet di capire di quale documento si tratta
	 * @param utente       utente che esegue l'operazione
	 * @param nomeFile     Nome del file che si deve scaricare
	 * @return l'url con l'encoding con i parametri per generare la servlet
	 * @throws UnsupportedEncodingException
	 */
	public static final String getCryptoDocParamById(final String id, final DocumentTypeEnum documentType, final UtenteDTO utente, final String nomeFile) 
			throws UnsupportedEncodingException {
		return getCryptoDocParamById(id, documentType, utente, nomeFile, "");
	}
	
    /**
	 * Genera l'url di encoding per invocare la servlet.
	 * 
	 * @param id           documentTitle utilizzato per pescare il documento
	 * @param documentType Enum per generare il prefisso che permette alla
	 *                     contentServlet di capire di quale documento si tratta
	 * @param utente       utente che esegue l'operazione
	 * @param nomeFile     Nome del file che si deve scaricare
	 * @param mimeType     mime type del file che si sta per caricare. Importante
	 *                     sorpatutto se è un PDF.
	 * 
	 * @return l'url con l'encoding con i parametri per generare la servlet
	 * @throws UnsupportedEncodingException
	 */
	public static final String getCryptoDocParamById(final String id, final DocumentTypeEnum documentType, final UtenteDTO utente, final String nomeFile, 
			final String mimeType) throws UnsupportedEncodingException {
		final boolean doLog = !"true".equals(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DISABLE_LOG_DOWNLOAD_CONTENT_SERVLET));
		
		String idAndPrefix = id; 
		if (documentType != null) {
			idAndPrefix = documentType.createId(id);
		}
		final String toEncString = "id=" + idAndPrefix + "&username=" + utente.getUsername() + "&idRuolo=" + utente.getIdRuolo() + "&idNodo=" + utente.getIdUfficio()
				+ "&nomeFile=" + nomeFile + "&mimeType=" + mimeType + "&ts=" + new Date().getTime();
		if (doLog) {
			LOGGER.info("***STRING REQUEST :" + toEncString);
		}
		
		String encodedString = null;
		String cryptedString = DesCrypterNew.encrypt(doLog, toEncString);
		if(doLog) {
			LOGGER.info("***STRINGA BASE 64 :"+ cryptedString);
		}
		
		// CODIFICO IN URL ENCODING
		if (utente.getPreferenzeApp().getUrlEncoding()) {
			encodedString = URLEncoder.encode(cryptedString, StandardCharsets.ISO_8859_1.name());
			if (doLog) {
				LOGGER.info("***STRINGA BASE 64 + URL ENC:" + encodedString);
			}
		} else {
			encodedString = cryptedString;
			if (doLog) {
				LOGGER.info("***STRINGA BASE 64 SENZA URL ENC:" + encodedString);
			}
		}

		encodedString = encodedString + "-" + utente.getPreferenzeApp().getUrlEncoding();
		if (doLog) {
			LOGGER.info("***REQUEST CON FLAG ALLA FINE:" + encodedString);
			LOGGER.info("***STRINGTOENCRYPT - CLEAN: " + toEncString);
			LOGGER.info("***STRINGTOENCRYPT - BASE64: " + new String(Base64.encodeBase64(toEncString.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8));
			LOGGER.info("***STRINGTOENCRYPT - CRYPTED: " + cryptedString);
			LOGGER.info("***STRINGTOENCRYPT - CRYPTED + ENCODED: " + encodedString);
		}

		return encodedString;
	}
    
}
