package it.ibm.red.web.enums;

/**
 * Enum che definisce le code.
 */
public enum TileEnum {

	/**
	 * Valore 1.
	 */
	GENERICA(1),

	/**
	 * Valore 2.
	 */
	SCRIVANIA(2),

	/**
	 * Valore 3.
	 */
	UFFICIO(3),

	/**
	 * Valore 4.
	 */
	CARTACEI(4),

	/**
	 * Valore 5.
	 */
	ARCHIVIO(5),

	/**
	 * Valore 6.
	 */
	CREAZIONE(6),

	/**
	 * Valore 7.
	 */
	RICERCA(7),

	/**
	 * Valore 8.
	 */
	RUBRICA(8),

	/**
	 * Valore 9.
	 */
	EVENTI(9),
	
	/**
	 * Valore 10.
	 */
	CONSULTAZIONE(10),
	
	/**
	 * Valore 11.
	 */
	UTENTE(11);

	/**
	 * ID.
	 */
	private Integer id;	
	
	TileEnum(final Integer inId) {
		id = inId;
	}

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}
}