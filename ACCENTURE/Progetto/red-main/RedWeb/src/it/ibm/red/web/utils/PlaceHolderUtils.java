package it.ibm.red.web.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.google.common.net.MediaType;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.AcroFields.FieldPosition;

import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.StampigliaturaSegnoGraficoDTO;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IStampigliaturaSegnoGraficoSRV;
import it.ibm.red.business.service.concrete.TrasformazionePDFSRV;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.web.servlets.HighlightSignFieldServlet;

/*
 * 
 * VINGENITO
 * 
 *
 */

/**
 * Classe PlaceHolderUtils.
 */
public final class PlaceHolderUtils {

	/**
	 * Messaggio errore firmatario non definito per allegato da siglare.
	 */
	private static final String ERROR_FIRMATARIO_NON_DEFINITO_MSG = "Nessun firmatario definito per l'allegato da siglare";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PlaceHolderUtils.class.getName());

	/**
	 * Costante doc_formato originale.
	 */
	private static final String DOC_FORMATO_ORIGINALE = "doc_formato_originale.pdf";

	/**
	 * Costante doc_formato pdf.
	 */
	private static final String DOC_FORMATO_PDF = "doc_formato_pdf.pdf";

	/**
	 * Costante doc_formato non convertibile.
	 */
	private static final String DOC_FORMATO_NON_CONVERTIBILE = "doc_non_convertibile_PDF.pdf";

	private PlaceHolderUtils() {

	}

	/**
	 * Questo metodo permette di inserire per pdfjs il placeholder relativo al campo
	 * firma e ad eventuali glifi degli allegati. E' in comune tra le servlet
	 * DownloadContentServlet e HighlightSignFieldServlet
	 *
	 * @param file                     the file
	 * @param idAoo                    the id aoo
	 * @param conCampoFirma            the con campo firma
	 * @param daFirmare                the da firmare
	 * @param converti                 the converti
	 * @param inEntrata                the in entrata
	 * @param inModifica               the in modifica
	 * @param mantieniFormatoOriginale the mantieni formato originale
	 * @param convertibile             the convertibile
	 * @param firmaMultipla            the firma multipla
	 * @param principale               the principale
	 * @param daNonProtocollare        the da non protocollare
	 * @param stampigliaturaSigla      the stampigliatura sigla
	 * @param idSiglatario             the id siglatario
	 * @param idUffCreatore            the id uff creatore
	 * @param idTipoDoc                the id tipo doc
	 * @param isDownloadContentServlet the is download content servlet
	 * @return the byte[]
	 */
	public static byte[] inserisciPlaceholderFirmaSigla(final FileDTO file, final Long idAoo, final boolean conCampoFirma,
			final boolean converti, final boolean inEntrata, final boolean inModifica, final boolean mantieniFormatoOriginale, final boolean contentLibroFirma,
			final boolean convertibileInput, final boolean firmaMultipla ,final boolean principale, final boolean daNonProtocollare,
			final boolean stampigliaturaSigla, final Long idSiglatario, final Long idUffCreatore, final Integer idTipoDoc, final boolean isDownloadContentServlet) {
		byte[] newContent = null;

		boolean convertibile = convertibileInput;
		if(MediaType.PDF.toString().equals(file.getMimeType())) {
			//ho preso il pdf al suo interno
			convertibile = true;
		}
		
		if (conCampoFirma) {

			if (!mantieniFormatoOriginale && convertibile) {
				FileDTO filePdf = null;
				if (isDownloadContentServlet) {
					// se vieni dalla servlet hai già im campo firma non c'è bisogno di convertire
					filePdf = file;
				} else {
					filePdf = convertiEStampiglia(file, idAoo, inEntrata, principale, firmaMultipla, true, inModifica, daNonProtocollare, conCampoFirma, 
            				mantieniFormatoOriginale, stampigliaturaSigla, idSiglatario, idUffCreatore, idTipoDoc);
				}
				if (filePdf != null) {
					byte[] pdfConGlifo = null;
					if (stampigliaturaSigla && !principale) {
						try {
							final IStampigliaturaSegnoGraficoSRV stampigliaturaSegnoGraficoSRV = ApplicationContextProvider.getApplicationContext()
									.getBean(IStampigliaturaSegnoGraficoSRV.class);
							final List<StampigliaturaSegnoGraficoDTO> listStampigliaturaSegnoGrafico = new ArrayList<>();
							final HashMap<String, byte[]> placeholderUtente = stampigliaturaSegnoGraficoSRV.getPlaceholderSigla(idSiglatario, idUffCreatore,
									idTipoDoc, listStampigliaturaSegnoGrafico);
							pdfConGlifo = PdfHelper.inserisciCampoSigla(filePdf.getContent(), placeholderUtente, false, listStampigliaturaSegnoGrafico);
						} catch (final Exception ex) {
							LOGGER.error(ERROR_FIRMATARIO_NON_DEFINITO_MSG, ex);
						}
					}
					FieldPosition fp = PdfHelper.getFirstBlankSignaturePosition(filePdf.getContent());
					// Se l'utente ha modificato il check "Da firmare" di un documento col tag
					// "Firmatario1", si ha un documento convertito senza campo firma
					if (fp == null && inModifica) {
						fp = PdfHelper.getFirstBlankSignaturePosition(filePdf.getContent());
					}
					byte[] contentconCampoFirma = null;
					if (pdfConGlifo != null) {
						contentconCampoFirma = PdfHelper.drawRectOnPosition(pdfConGlifo, fp, BaseColor.CYAN,
								new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD, BaseColor.BLACK), "Firma qui.");
					} else {
						contentconCampoFirma = PdfHelper.drawRectOnPosition(filePdf.getContent(), fp, BaseColor.CYAN,
								new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD, BaseColor.BLACK), "Firma qui.");

					}

					if (contentconCampoFirma != null) {
						newContent = contentconCampoFirma;
					} else {
						newContent = filePdf.getContent();
					}

				}

			} else if (mantieniFormatoOriginale) {
				newContent = FileUtils.getFile(HighlightSignFieldServlet.class.getClassLoader(), DOC_FORMATO_ORIGINALE);
			} else {
				newContent = FileUtils.getFile(HighlightSignFieldServlet.class.getClassLoader(), DOC_FORMATO_NON_CONVERTIBILE);
			}

		} else {
			
			newContent = file.getContent();

			if (!converti) {

				if (!MediaType.PDF.toString().equals(file.getMimeType())) {
					if (mantieniFormatoOriginale) {
						newContent = FileUtils.getFile(HighlightSignFieldServlet.class.getClassLoader(), DOC_FORMATO_ORIGINALE);
					} else {
						if (!convertibile) {
							newContent = FileUtils.getFile(HighlightSignFieldServlet.class.getClassLoader(), DOC_FORMATO_NON_CONVERTIBILE);
						} else {
							newContent = FileUtils.getFile(HighlightSignFieldServlet.class.getClassLoader(), DOC_FORMATO_PDF);
						}
					}
				}
			} else if (!MediaType.PDF.toString().equals(file.getMimeType()) || (inEntrata && !daNonProtocollare)) {
				FileDTO filePdf = null;
				if (isDownloadContentServlet) {
					// se vieni dalla servlet hai già im campo firma non c'è bisogno di convertire
					filePdf = file;
				} else {
					filePdf = convertiEStampiglia(file, idAoo, inEntrata, principale, firmaMultipla, false, inModifica, daNonProtocollare, mantieniFormatoOriginale, 
        					contentLibroFirma, stampigliaturaSigla, idSiglatario, idUffCreatore, idTipoDoc);
				}
				if (filePdf != null) {
					if (stampigliaturaSigla && !principale) {
						try {

							final IStampigliaturaSegnoGraficoSRV stampigliaturaSegnoGraficoSRV = ApplicationContextProvider.getApplicationContext()
									.getBean(IStampigliaturaSegnoGraficoSRV.class);
							final List<StampigliaturaSegnoGraficoDTO> listStampigliaturaSegnoGrafico = new ArrayList<>();
							final HashMap<String, byte[]> placeholderUtente = stampigliaturaSegnoGraficoSRV.getPlaceholderSigla(idSiglatario, idUffCreatore,
									idTipoDoc, listStampigliaturaSegnoGrafico);
							final byte[] pdfConGlifo = PdfHelper.inserisciCampoSigla(filePdf.getContent(), placeholderUtente, false, listStampigliaturaSegnoGrafico);
							newContent = pdfConGlifo;
						} catch (final Exception ex) {
							LOGGER.error(ERROR_FIRMATARIO_NON_DEFINITO_MSG, ex);
							newContent = filePdf.getContent();
						}
					} else {
						newContent = filePdf.getContent();
					}
				}
			} else if (MediaType.PDF.toString().equals(file.getMimeType()) && stampigliaturaSigla && !principale) {
				try {

					final IStampigliaturaSegnoGraficoSRV stampigliaturaSegnoGraficoSRV = ApplicationContextProvider.getApplicationContext()
							.getBean(IStampigliaturaSegnoGraficoSRV.class);
					final List<StampigliaturaSegnoGraficoDTO> listStampigliaturaSegnoGrafico = new ArrayList<>();
					final HashMap<String, byte[]> placeholderUtente = stampigliaturaSegnoGraficoSRV.getPlaceholderSigla(idSiglatario, idUffCreatore, idTipoDoc,
							listStampigliaturaSegnoGrafico);
					final byte[] pdfConGlifo = PdfHelper.inserisciCampoSigla(newContent, placeholderUtente, false, listStampigliaturaSegnoGrafico);
					newContent = pdfConGlifo;
				} catch (final Exception ex) {
					LOGGER.error(ERROR_FIRMATARIO_NON_DEFINITO_MSG, ex);
				}
			}
		}
		
		return newContent;
	}
	
	/**
	 * Converte e stampiglia il documento.
	 * 
	 * @param inputFile
	 *            file in input
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @param inEntrata
	 *            flag entrata
	 * @param principale
	 *            flag principale
	 * @param firmaMultipla
	 *            flag firma multipla
	 * @param conCampoFirma
	 *            flag campo firma
	 * @param haGiaCampoFirma
	 *            flag associato alla presenza campo firma
	 * @param daNonProtocollare
	 *            flag associato alla protocollazione
	 * @param applicaAncheSenzaTag
	 *            flag che gestisce l'applicazione anche senza tag
	 * @param mantieniFormatoOriginale
	 *            flag associato al mantenimento del formato originale
	 * @param stampigliaturaSigla
	 *            flag associato alla stampigliatura sigla
	 * @param idSiglatario
	 *            identificativo siglatario
	 * @param idUffCreatore
	 *            identificativo ufficio creatore
	 * @param idTipoDoc
	 *            identificativo tipo documento
	 * @return file stampigliato
	 */
	private static FileDTO convertiEStampiglia(FileDTO inputFile, Long idAoo, boolean inEntrata, boolean principale, boolean firmaMultipla, 
			boolean conCampoFirma, boolean haGiaCampoFirma, boolean daNonProtocollare, boolean applicaAncheSenzaTag, boolean mantieniFormatoOriginale, 
			boolean stampigliaturaSigla, Long idSiglatario, Long idUffCreatore, Integer idTipoDoc) {
		FileDTO pdf = null;

		try {
			final TrasformazionePDFSRV trasformazionePdfSRV = ApplicationContextProvider.getApplicationContext().getBean(TrasformazionePDFSRV.class);

			String protocollo = null;
			List<Integer> firmatari = null;
			boolean applicaTimbroProtocollo = false;
			boolean apponiCampoFirma = false;

			if (conCampoFirma && !haGiaCampoFirma) {
				firmatari = Arrays.asList(0);
				apponiCampoFirma = true;
			}

			if (inEntrata && !daNonProtocollare) {
				protocollo = "PROTOCOLLO DA DEFINIRE";
				applicaTimbroProtocollo = true;
			}
			
			pdf = trasformazionePdfSRV.convertiInPDFConStampigliature(idAoo, inputFile, null, protocollo, firmatari, inEntrata, principale, 
					applicaTimbroProtocollo, false, apponiCampoFirma, firmaMultipla, applicaAncheSenzaTag, mantieniFormatoOriginale, 
					stampigliaturaSigla, idSiglatario, idUffCreatore, idTipoDoc);
		} catch (Exception e) {
			LOGGER.error("Si è verificato un errore durante la conversione del file", e);
		}

		return pdf;
	}

}
