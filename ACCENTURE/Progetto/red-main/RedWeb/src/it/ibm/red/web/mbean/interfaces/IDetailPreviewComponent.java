package it.ibm.red.web.mbean.interfaces;

/**
 * Utilizzato per renderizzare l'iPanel della preview
 *
 */
public interface IDetailPreviewComponent {
	
	/**
	 * Costruisce i parametri da inviare alla
	 * {@link it.ibm.red.web.servlets.DownloadContentServlet}
	 * 
	 * 
	 * @return
	 * 	I parametri soap per chiamare la servlet
	 */
	String getCryptoId();
}
