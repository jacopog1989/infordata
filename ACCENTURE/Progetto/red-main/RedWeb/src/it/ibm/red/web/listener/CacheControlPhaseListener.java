package it.ibm.red.web.listener;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletResponse;

import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * Listener cache control.
 */
public class CacheControlPhaseListener implements PhaseListener {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -1334029707869279328L;

	/**
	 * Esegue le azioni dopo la fase.
	 */
	@Override
	public void afterPhase(final PhaseEvent event) {
		// Non occorre fare niente in questo metodo.
	}

	/**
	 * Esegue le azioni prima della fase.
	 */
	@Override
	public void beforePhase(final PhaseEvent event) { 
		final String cacheControlEnable = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.HTTP_CACHE_SECURE_CONTROL);
		if (cacheControlEnable != null && "true".equalsIgnoreCase(cacheControlEnable)) {
			final FacesContext facesContext = event.getFacesContext();
			final HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
			response.addHeader("Cache-Control", "private");
			response.addHeader("Cache-Control", "no-store"); 
		}
	}

	/**
	 * Restituisce l'id della fase.
	 */
	@Override
	public PhaseId getPhaseId() {
		return PhaseId.RENDER_RESPONSE;
	}



}
