package it.ibm.red.web.document.mbean.component;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV;
import it.ibm.red.web.document.mbean.interfaces.IDsrPresentazioneBean;
import it.ibm.red.web.mbean.AbstractBean;

/**
 * Bean che gestisce la presentazione DSR.
 */
public class DsrPresentazioneBean extends AbstractBean implements IDsrPresentazioneBean{
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -7449822129860706923L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DsrPresentazioneBean.class);

	/**
	 * Dettaglio.
	 */
	private final DetailDocumentRedDTO detail;

	/**
	 * Flag modifica.
	 */
	private final Boolean isModifica;

	/**
	 * Service.
	 */
	private final IDocumentManagerFacadeSRV documentManagerSRV;

	/**
	 * Flag register mittente.
	 */
	private boolean canRegisterMittente = true;

	/**
	 * Flag register oggetto.
	 */
	private boolean canRegisterOggetto = true;

	/**
	 * Costruttore del bean, inietta eventuali Service.
	 * @param detail
	 * @param isModifica
	 */
	public DsrPresentazioneBean(final DetailDocumentRedDTO detail, final Boolean isModifica) {
		this.detail = detail;
		this.isModifica = isModifica;
		documentManagerSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentManagerFacadeSRV.class);
	}

	/**
	 * Restituisce il dettaglio.
	 * @return detail
	 */
	@Override
	public DetailDocumentRedDTO getDetail() {
		return detail;
	}

	/**
	 * Gestisce la modifica dell'oggetto, impostando la descrizione del fascicolo procedimentale associato.
	 */
	@Override
	public void onChangeOggetto() {
		try {
			if (Boolean.FALSE.equals(isModifica)) {
				detail.setDescrizioneFascicoloProcedimentale(documentManagerSRV.componiDescrizioneFascicolo(detail));
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}
	}
	
	/**
	 * @see it.ibm.red.web.mbean.AbstractBean#postConstruct().
	 */
	@Override
	protected void postConstruct() {
		throw new UnsupportedOperationException("Questo metodo non è un bean il postConstruct è presente solo per sfruttare alcune funzionalità comuuni all'abstractBean");
	}

	/**
	 * @return canRegisterMittente
	 */
	public boolean isCanRegisterMittente() {
		return canRegisterMittente;
	}

	/**
	 * @param canRegisterMittente
	 */
	public void setCanRegisterMittente(final boolean canRegisterMittente) {
		this.canRegisterMittente = canRegisterMittente;
	}

	/**
	 * @return canRegisterOggetto
	 */
	public boolean isCanRegisterOggetto() {
		return canRegisterOggetto;
	}

	/**
	 * @param canRegisterOggetto
	 */
	public void setCanRegisterOggetto(final boolean canRegisterOggetto) {
		this.canRegisterOggetto = canRegisterOggetto;
	}


}
