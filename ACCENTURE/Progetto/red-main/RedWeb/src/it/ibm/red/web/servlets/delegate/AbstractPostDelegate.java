package it.ibm.red.web.servlets.delegate;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.lsign.LocalSignHelper;
import it.ibm.red.web.helper.lsign.LocalSignHelper.DocSignDTO;
import it.ibm.red.web.helper.lsign.LocalSignHelper.TipoDoc;
import it.ibm.red.web.servlets.LocalSignServlet;

/**
 * Abstract post delegate.
 */
public abstract class AbstractPostDelegate extends AbstractDelegate implements IPostDelegate {

	@Override
	public final void updateResponseForNextAppletIteration(final HttpServletRequest request, final HttpServletResponse response, 
			final DocSignDTO doc) throws IOException {
		final LocalSignHelper lsh = (LocalSignHelper) request.getSession().getAttribute(ConstantsWeb.SessionObject.SESSION_LSH);
        final String servletName = LocalSignServlet.class.getSimpleName();
        response.getOutputStream().println(request.getContextPath() + "/" + servletName+ "?delegate=" + this.getClass().getName() + "&source=" + lsh.getInfoApplet().getSource());
        response.getOutputStream().println("Action=" + lsh.getInfoApplet().getAction());
        response.getOutputStream().println("DocumentURL=" + lsh.getInfoApplet().getDocumentURL());
        response.getOutputStream().println("DigestURL=" + lsh.getInfoApplet().getDocumentURL());
        
        String msgDoc = null;
        if (lsh.isCopiaConforme()) {
	        if (TipoDoc.ALLEGATO.equals(doc.getTipoDoc())) {
	        	// messaggio per allegato
	        	msgDoc = "Terminata l'attestazione dell'allegato " + doc.getDocumentTitle() + " del documento " + doc.getNumeroDocumentoPrincipale();
	        } else {
	        	// messaggio per documento principale
	        	msgDoc = "Recuperati gli allegati da attestare per copia conforme del documento " + doc.getNumeroDocumentoPrincipale();
	        }
	        response.getOutputStream().println("Message=" + msgDoc + ".");
        } else {
        	if (TipoDoc.ALLEGATO.equals(doc.getTipoDoc())) {
	        	// messaggio per allegato
	        	msgDoc = "Terminata la firma dell'allegato " + doc.getDocumentTitle() + " del documento " + doc.getNumeroDocumentoPrincipale();
	        } else {
	        	// messaggio per documento principale
	        	msgDoc = "Terminata la firma del documento " + doc.getNumeroDocumentoPrincipale();
	        }
        	response.getOutputStream().println("Message=" + msgDoc + ".\nRimangono " + lsh.getDocSet().size() + " documenti da firmare.");
        }
	}
}
