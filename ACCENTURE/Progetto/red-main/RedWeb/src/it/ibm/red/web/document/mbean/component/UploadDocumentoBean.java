package it.ibm.red.web.document.mbean.component;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.mbean.AbstractBean;

/**
 * Bean upload documento.
 */
public class UploadDocumentoBean extends AbstractBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 3740254857058457352L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(UploadDocumentoBean.class);

	/**
	 * Dettaglio.
	 */
	private final DetailDocumentRedDTO detail;

	/**
	 * Documento principale.
	 */
	private transient UploadedFile docPrincipale;

	/**
	 * Costruttore del bean, inizializza il detail.
	 * 
	 * @param detail
	 */
	public UploadDocumentoBean(final DetailDocumentRedDTO detail) {
		this.detail = detail;
	}
	
	/**
	 * Gestisce l'upload del file principale.
	 * 
	 * @param event
	 */
	public void handleMainFileUpload(FileUploadEvent event) {
		try { 
			event = checkFileName(event);
			if (event == null) {
				return;
			}
			docPrincipale = event.getFile();

			final String fileName = docPrincipale.getFileName();
			
			this.detail.setNomeFile(fileName);
			this.detail.setMimeType(docPrincipale.getContentType());
			this.detail.setContent(docPrincipale.getContents());
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}
    }
	
	/**
	 * Restituisce true se il documento principale risulta modificato, false
	 * altrimenti.
	 * 
	 * @return true se il documento principale risulta modificato, false altrimenti
	 */
	public boolean isModified() {
		return docPrincipale != null;
	}
	
	/**
	 * Restituisce il dettaglio.
	 * 
	 * @return detail
	 */
	public DetailDocumentRedDTO getDetail() {
		return detail;
	}

	/**
	 * @see it.ibm.red.web.mbean.AbstractBean#postConstruct().
	 */
	@Override
	protected void postConstruct() {
		throw new UnsupportedOperationException("Questo bean è esclusivamente un helper non deve essere gestito dal framework");
	}
}
