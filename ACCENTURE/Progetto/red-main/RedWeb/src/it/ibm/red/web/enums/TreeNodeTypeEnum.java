package it.ibm.red.web.enums;

/**
 * Enum che definisce i tipi di tree node.
 */
public enum TreeNodeTypeEnum {

	/**
	 * Valore.
	 */
	ROOT("root"),

	/**
	 * Valore.
	 */
	UFFICIO("ufficio"),

	/**
	 * Valore.
	 */
	UFFICIO_NO_MENU("ufficioNoMenu"),

	/**
	 * Valore.
	 */
	FALDONE("faldone"),

	/**
	 * Valore.
	 */
	FALDONE_NO_MENU("faldoneNoMenu"),

	/**
	 * Valore.
	 */
	FASCICOLO("fascicolo"),

	/**
	 * Valore.
	 */
	DOCUMENTO("documento"),

	/**
	 * Valore.
	 */
	INDICE("indice"),

	/**
	 * Valore.
	 */
	DUMMY("dummy");

	/**
	 * Tipo.
	 */
	private String type;	

	TreeNodeTypeEnum(final String inType) {
		type = inType;
	}
	
	/**
	 * Restituisce il type.
	 * @return type
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Restituisce l'enum associato al type.
	 * @param type
	 * @return enum associato al type
	 */
	public static TreeNodeTypeEnum get(final String type) {
		TreeNodeTypeEnum output = null;
		for (final TreeNodeTypeEnum t:TreeNodeTypeEnum.values()) {
			if (type.equals(t.getType())) {
				output = t;
				break;
			}
		}
		return output;
	}
}