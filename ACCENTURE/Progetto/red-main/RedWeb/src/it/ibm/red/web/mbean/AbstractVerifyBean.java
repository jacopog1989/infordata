package it.ibm.red.web.mbean;

import java.util.List;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.enums.TipoApprovazioneEnum;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * @author SimoneLungarella
 * 
 * Abstract bean per la gestione della logica comune al @see VistoBean e @see VerificatoBean.
 */
public abstract class AbstractVerifyBean extends AbstractBean implements InitHumanTaskInterface{

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = 9149081762667743265L;

	/**
	 * Documenti selezionati.
	 */
	protected transient List<MasterDocumentRedDTO> documentiSelezionati;

	/**
	 * Lista tipologie.
	 */
	protected transient List<TipoApprovazioneEnum> tipologia;

	/**
	 * Tipologia selezionata.
	 */
	protected TipoApprovazioneEnum tipologiaSelezionata;

	/**
	 * Messaggio.
	 */
	protected String message;

	/**
	 * Lista documenti bean.
	 */
	protected ListaDocumentiBean ldb;
	
	/**
	 * @return documentiSelezionati
	 */
	public List<MasterDocumentRedDTO> getDocumentiSelezionati() {
		return documentiSelezionati;
	}

	/**
	 * Imposta la lista di documenti selezionati.
	 * @param documentiSelezionati
	 */
	public void setDocumentiSelezionati(final List<MasterDocumentRedDTO> documentiSelezionati) {
		this.documentiSelezionati = documentiSelezionati;
	}

	/**
	 * Restituisce il motivo assegnazione.
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Imposta il motivo assegnazione.
	 * @param message
	 */
	public void setMessage(final String message) {
		this.message = message;
	}

	/**
	 * Restituisce la tipologia dell'approvazione.
	 * @return tipologia
	 */
	public List<TipoApprovazioneEnum> getTipologia() {
		return tipologia;
	}

	/**
	 * Imposta la tipologia dell'approvazione.
	 * @param tipologia
	 */
	public void setTipologia(final List<TipoApprovazioneEnum> tipologia) {
		this.tipologia = tipologia;
	}

	/**
	 * Restituisce il tipo di approvazione selezionata.
	 * @return tipologiaSelezionata
	 */
	public TipoApprovazioneEnum getTipologiaSelezionata() {
		return tipologiaSelezionata;
	}

	/**
	 * Imposta il tipo di approvazione selezionata.
	 * @param tipologiaSelezionata
	 */
	public void setTipologiaSelezionata(final TipoApprovazioneEnum tipologiaSelezionata) {
		this.tipologiaSelezionata = tipologiaSelezionata;
	}

	/**
	 * Restituisce il Bean lista documenti.
	 * @return ldb
	 */
	public ListaDocumentiBean getLdb() {
		return ldb;
	}

	/**
	 * Imposta il Bean lista documenti.
	 * @param ldb
	 */
	public void setLdb(final ListaDocumentiBean ldb) {
		this.ldb = ldb;
	}
	
	
}
