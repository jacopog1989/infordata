package it.ibm.red.web.enums;

/**
 * Enum che definisce le icone delle sottoscrizioni.
 */
public enum SottoscrizioniIconEnum {

	/**
	 * Valore.
	 */
	ASSEGNATO_PER_FIRMA("ASSEGNATO PER FIRMA", SottoscrizioniIconEnum.FA_FA_PENCIL_SQUARE_O),

	/**
	 * Valore.
	 */
	ASSEGNATO_PER_FIRMA_COPIA_CONFORME("ASSEGNATO PER FIRMA COPIA CONFORME", SottoscrizioniIconEnum.FA_FA_PENCIL_SQUARE_O),

	/**
	 * Valore.
	 */
	ASSEGNATO_PER_SIGLA("ASSEGNATO PER SIGLA", SottoscrizioniIconEnum.FA_FA_PENCIL_SQUARE_O),

	/**
	 * Valore.
	 */
	ASSEGNATO_PER_VISTO("ASSEGNATO PER VISTO", SottoscrizioniIconEnum.FA_FA_PENCIL_SQUARE_O),

	/**
	 * Valore.
	 */
	ASSEGNAZIONE_A_CORRIERE("ASSEGNAZIONE A CORRIERE", SottoscrizioniIconEnum.FA_FA_TRUCK),

	/**
	 * Valore.
	 */
	ASSEGNAZIONE_CORRIERE("ASSEGNAZIONE CORRIERE", SottoscrizioniIconEnum.FA_FA_TRUCK),

	/**
	 * Valore.
	 */
	ASSEGNAZIONE_DIRETTA_UTENTE("ASSEGNAZIONE DIRETTA UTENTE", SottoscrizioniIconEnum.FA_FA_TRUCK),

	/**
	 * Valore.
	 */
	COMPLETAMENTO_SPEDIZIONE_ELETTRONICA("COMPLETAMENTO SPEDIZIONE ELETTRONICA", SottoscrizioniIconEnum.FA_FA_SEND),

	/**
	 * Valore.
	 */
	CONTRIBUTI_VALIDATI("CONTRIBUTI VALIDATI", "fa fa-th"),

	/**
	 * Valore.
	 */
	ERRORE_SPEDIZIONE_MAIL("ERRORE SPEDIZIONE MAIL", SottoscrizioniIconEnum.FA_FA_SEND),

	/**
	 * Valore.
	 */
	FIRMATO("FIRMATO", SottoscrizioniIconEnum.FA_FA_PENCIL_SQUARE_O),

	/**
	 * Valore.
	 */
	MODIFICATO_DOPO_LA_SIGLA("MODIFICATO DOPO LA SIGLA", SottoscrizioniIconEnum.FA_FA_PENCIL_SQUARE_O),

	/**
	 * Valore.
	 */
	MODIFICATO_DOPO_IL_VISTO("MODIFICATO DOPO IL VISTO", SottoscrizioniIconEnum.FA_FA_PENCIL_SQUARE_O),

	/**
	 * Valore.
	 */
	PREDISPOSIZIONE_PER_LA_FIRMA_AUTOGRAFA("PREDISPOSIZIONE PER LA FIRMA AUTOGRAFA", SottoscrizioniIconEnum.FA_FA_PENCIL_SQUARE_O),

	/**
	 * Valore.
	 */
	RICHIESTA_CONTRIBUTO("RICHIESTA CONTRIBUTO", SottoscrizioniIconEnum.FA_FA_PENCIL_SQUARE_O),

	/**
	 * Valore.
	 */
	SCADENZA_ENTRO_10_GIORNI("SCADENZA ENTRO 10 GIORNI", SottoscrizioniIconEnum.FA_FA_SEND),

	/**
	 * Valore.
	 */
	SCADENZA_ENTRO_15_GIORNI("SCADENZA ENTRO 15 GIORNI", SottoscrizioniIconEnum.FA_FA_SEND),

	/**
	 * Valore.
	 */
	SCADENZA_ENTRO_5_GIORNI("SCADENZA ENTRO 5 GIORNI", SottoscrizioniIconEnum.FA_FA_SEND),

	/**
	 * Valore.
	 */
	SIGLATO("SIGLATO", SottoscrizioniIconEnum.FA_FA_PENCIL_SQUARE_O),

	/**
	 * Valore.
	 */
	SPEDITO("SPEDITO", SottoscrizioniIconEnum.FA_FA_SEND),

	/**
	 * Valore.
	 */
	TRASFERIMENTO_DI_COMPETENZA("TRASFERIMENTO DI COMPETENZA", SottoscrizioniIconEnum.FA_FA_SEND),

	/**
	 * Valore.
	 */
	VISTATO("VISTATO", SottoscrizioniIconEnum.FA_FA_SEND);
	
	private static final String FA_FA_PENCIL_SQUARE_O = "fa fa-pencil-square-o";
	private static final String FA_FA_TRUCK = "fa fa-truck";
	private static final String FA_FA_SEND= "fa fa-send";

	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Icona.
	 */
	private String icon;
	
	/**
	 * Costruttore dell'enum.
	 * 
	 * @param inDescrizione
	 * @param inIcon
	 */
	SottoscrizioniIconEnum(final String inDescrizione, final String inIcon) {
		descrizione = inDescrizione;
		icon = inIcon;
	}

	/**
	 * Restituisce la descrizione.
	 * 
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Restituisce l'icona.
	 * 
	 * @return icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * Restituisce l'enum associata alla descrizione in ingresso.
	 * 
	 * @param descrizione
	 * @return enum associata alla descrizione
	 */
	public static String get(final String descrizione) {
		String output = null;
		for (final SottoscrizioniIconEnum t : SottoscrizioniIconEnum.values()) {
			if (descrizione.equalsIgnoreCase(t.getDescrizione())) {
				output = t.getIcon();
				break;
			}
		}
		return output;
	}
}