package it.ibm.red.web.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import it.ibm.red.business.dto.ComuneDTO;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IComuneFacadeSRV;

/**
 * Converter comune.
 */
@FacesConverter("comuneConverter")
public class ComuneConverter implements Converter {

	/**
	 * Restituisce il comune come Object.
	 */
	@Override
	public Object getAsObject(final FacesContext fc, final UIComponent uic, final String value) {
		final IComuneFacadeSRV comuneSRV = ApplicationContextProvider.getApplicationContext().getBean(IComuneFacadeSRV.class);
		return comuneSRV.get(value);
	}

	/**
	 * Restituisce il comune come stringa.
	 */
	@Override
	public String getAsString(final FacesContext fc, final UIComponent uic, final Object object) {
		if (object instanceof ComuneDTO) {
			final ComuneDTO comune = (ComuneDTO) object;
			return comune.getIdComune();
		}
		return object.toString();
	}

}
