package it.ibm.red.web.sottoscrizioni.mbean.component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.ProcedimentoTracciatoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ITracciaDocumentiFacadeSRV;
import it.ibm.red.web.helper.dtable.Selectable;
import it.ibm.red.web.mbean.AbstractBean;

/**
 * Component procedimenti tracciati.
 */
public class ProcedimentiTracciatiComponent extends AbstractBean {
	
	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = -9123999574195493485L;

	/**
	 * Informazioni utente.
	 */
	private final UtenteDTO utente;
	
	/**
	 * Servizio.
	 */
	private final ITracciaDocumentiFacadeSRV tracciaSRV;
	
	/**
	 * Componente dettaglio documento.
	 */
	private final DettaglioDocumentoSottoscrizioniComponent documentDetailComponent;
	
	/**
	 * Lista di tutti i tracciamenti per utente.
	 */
	private transient List<Selectable<ProcedimentoTracciatoDTO>> masters;
	
	/**
	 * Lista gestita da primefaces di ProcedimentoTracciatoDTO.
	 */
	private transient List<Selectable<ProcedimentoTracciatoDTO>> filteredMasters;
	
	/**
	 * Procedimento tracciato.
	 */
	private transient Selectable<ProcedimentoTracciatoDTO> current;
	
	/**
	 * Lista di procedimenti disponibili tra i masters.
	 */
	private Set<String> procedimenti;
	
	/**
	 * Tutti gli elementi selezionati.
	 */
	private boolean allSelected;
	
	/**
	 * Costruttore.
	 * @param utente
	 */
	public ProcedimentiTracciatiComponent(final UtenteDTO utente) {
		this.utente = utente;
		this.documentDetailComponent = new DettaglioDocumentoSottoscrizioniComponent(utente);
		documentDetailComponent.setUpdateTabDettaglio("centralSectionForm:idTab_STSCZ:tabDetails-procedimenti");
		documentDetailComponent.getStorico().setIdSuffix("tracciati_sottoscrizioni");
		
		tracciaSRV = ApplicationContextProvider.getApplicationContext().getBean(ITracciaDocumentiFacadeSRV.class);
		init();
	}
	
	/**
	 * Metodo richiamato da ricarica lista utille tra l'altro al refresh della lista.
	 */
	public void init() {
		final Collection<ProcedimentoTracciatoDTO> tracciamenti = tracciaSRV.loadProcedimentiTracciati(utente);
		if (CollectionUtils.isEmpty(tracciamenti)) {
			masters = new ArrayList<>(0);
			procedimenti = new HashSet<>(0);
		} else {
			masters = tracciamenti.stream()
					.map(pt -> new Selectable<ProcedimentoTracciatoDTO>(pt))
					.collect(Collectors.toList());
			procedimenti = tracciamenti.stream()
					.map(pt -> pt.getTipoProcedimento())
					.collect(Collectors.toSet());
			this.current = masters.get(0);
		}
		
		filteredMasters = null;
		setAllSelected(false);
		refreshDetail();
	}
	
	/**
	 * Imposta tutti gli elementi come selezionati.
	 */
	public void selectAll() {
		masters.stream()
			.forEach(m -> m.setSelected(!isAllSelected()));
		
		setAllSelected(!isAllSelected());
	}
	
	/**
	 * Gestisce la rimozione del procedimento tracciato.
	 * @param spt
	 */
	public void remove(final Selectable<ProcedimentoTracciatoDTO> spt) {
		final ProcedimentoTracciatoDTO pt = spt.getData();
		
		final Collection<Integer> idDocumenti = new ArrayList<>(1);
		idDocumenti.add(pt.getIdDocumento());
		
		genericRemove(idDocumenti);
		init();
	}
	
	/**
	 * Gestisce la rimozione del master selezionato.
	 */
	public void removeSelected() {
		final List<Integer> selected = masters.stream()
			.filter(m -> m.isSelected())
			.map(pt -> pt.getData().getIdDocumento())
			.collect(Collectors.toList());
		if (CollectionUtils.isEmpty(selected)) {
			showError("Selezionare almeno un procedimento");
		} else {
			genericRemove(selected);
			init();
		}
	}

	/**
	 * Chiama il be e gestisce l'errore ritornato.
	 * @param idDocumenti
	 */
	private void genericRemove(final Collection<Integer> idDocumenti) {
		final Collection<EsitoOperazioneDTO> esitoList = tracciaSRV.rimuoviDocumenti(utente.getIdUfficio(), utente.getId(), utente.getIdRuolo(), idDocumenti);
		boolean error = false;
		final StringBuilder msg = new StringBuilder();
		final Collection<Integer> okList = esitoList.stream()
				.filter(esito -> esito.isEsito())
				.map(esito -> esito.getIdDocumento())
				.collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(okList)) {
			msg.append("Eliminazione dei seguenti procedimenti è avvenuta con successo: ");
			msg.append(okList.toString());
		}
		
		// Si aggiorna la lista dei procedimenti tracciati, rimuovendo quelli eliminati
		for (final Integer idDocumentoOk : okList) {
			final Iterator<Selectable<ProcedimentoTracciatoDTO>> itProcedimentiTracciati = masters.iterator();
			while (itProcedimentiTracciati.hasNext()) {
				final Selectable<ProcedimentoTracciatoDTO> procedimentoTracciato = itProcedimentiTracciati.next();
				
				if (idDocumentoOk.equals(procedimentoTracciato.getData().getIdDocumento())) {
					itProcedimentiTracciati.remove(); // Gestione Masters
					if (filteredMasters != null && filteredMasters.contains(procedimentoTracciato)) { // Gestione Filtered Masters
						filteredMasters.remove(procedimentoTracciato);
					}
					break;
				}
			}
		}
		
		final Collection<Integer> errorList = esitoList.stream()
				.filter(esito -> !esito.isEsito())
				.map(esito -> esito.getIdDocumento())
				.collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(errorList)) {
			error = true;
			if (msg.length() != 0) {
				msg.append(System.lineSeparator());
			}
			msg.append("Si è verificato un errore durante durante l'eliminazione dei seguenti procedimenti: ");
			msg.append(errorList.toString());
		}
		if (error) {
			showError(msg.toString());
		} else {
			showInfoMessage(msg.toString());
		}
	}

	/**
	 * Restituisce tutti i master.
	 * @return masters
	 */
	public List<Selectable<ProcedimentoTracciatoDTO>> getMasters() {
		return masters;
	}

	/**
	 * Imposta tutti i master.
	 * @param masters
	 */
	public void setMasters(final List<Selectable<ProcedimentoTracciatoDTO>> masters) {
		this.masters = masters;
	}

	/**
	 * Restituisce i master filtrati.
	 * @return master filtrati
	 */
	public List<Selectable<ProcedimentoTracciatoDTO>> getFilteredMasters() {
		return filteredMasters;
	}

	/**
	 * Imposta i master filtrati.
	 * @param filteredMasters
	 */
	public void setFilteredMasters(final List<Selectable<ProcedimentoTracciatoDTO>> filteredMasters) {
		this.filteredMasters = filteredMasters;
	}
	
	/**
	 * Restituisce i procedimenti.
	 * @return procedimenti
	 */
	public Collection<String> getProcedimenti() {
		return procedimenti;
	}

	/**
	 * Restituisce true se sono tutti selezionati.
	 * @return true se sono selezionati tutti, false altrimenti
	 */
	public boolean isAllSelected() {
		return allSelected;
	}

	/**
	 * Imposta il flag associato alla selezione di tutti.
	 * @param allSelected
	 */
	public void setAllSelected(final Boolean allSelected) {
		this.allSelected = allSelected;
	}
	
	/**
	 * Restituisce il current.
	 * @return current
	 */
	public Selectable<ProcedimentoTracciatoDTO> getCurrent() {
		return current;
	}

	/**
	 * Imposta il current.
	 * @param current
	 */
	public void setCurrent(final Selectable<ProcedimentoTracciatoDTO> current) {
		this.current = current;
		refreshDetail();
	}

	/**
	 * Esegue il refresh del dettaglio.
	 */
	public void refreshDetail() {
		String documentTitle = null;
		if (current != null) {			
			final ProcedimentoTracciatoDTO pt = current.getData();
			documentTitle =  Integer.toString(pt.getIdDocumento());
		}
		
		documentDetailComponent.setDetail(documentTitle);
	}

	/**
	 * Post construct del component.
	 */
	@Override
	protected void postConstruct() {
		throw new UnsupportedOperationException("Questo bean è esclusivamente un helper non deve essere gestito dal framework"); 
	}

	/**
	 * Restituisce il component che gestisce il dettaglio documento.
	 * @return il component
	 */
	public DettaglioDocumentoSottoscrizioniComponent getDocumentDetailComponent() {
		return documentDetailComponent;
	}
}
