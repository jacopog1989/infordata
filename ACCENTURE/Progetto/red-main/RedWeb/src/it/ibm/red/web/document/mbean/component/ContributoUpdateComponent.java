package it.ibm.red.web.document.mbean.component;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IContributoFacadeSRV;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.helper.faces.MessageSeverityEnum;

/**
 * Component che gestisce il contributo update.
 */
public class ContributoUpdateComponent extends AbstractComponent implements Serializable {
	
	private static final String IMPOSSIBILE_RECUPERARE_IL_FILE = "Impossibile recuperare il file";

	/**
	 * Costante: Serial Version UID.
	 */
	private static final long serialVersionUID = 2862868531753874873L;

    /**
     * Logger.
     */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContributoUpdateComponent.class);
	
    /**
     * Informazioni utente.
     */
	private final UtenteDTO utente;
	
    /**
     * Informazioni contributo.
     */
	private ContributoDTO contributo;
	
    /**
     * Servizio gestoine contributi.
     */
	private final IContributoFacadeSRV contributoSRV;
	
    /**
     * Documento principale.
     */
	private transient UploadedFile docPrincipale;
	
	/**
	 * Tiene traccia del fatto che l'utente abbia o meno modificato il contenuto presente su FileNet.
	 */
	private boolean removeContent = false;

	/**
	 * Costruttore che gestisce il flag removeContent verificnadone l'esistenza su Filenet
	 * Se non è presente su FileNet è come se non fosse presente il contenuto e quindi il flag sarà true.
	 * @param utente
	 * @param contributo
	 */
	public ContributoUpdateComponent(final UtenteDTO utente, final ContributoDTO contributo) {
		this.utente = utente;
		this.contributo = contributo;
		this.contributoSRV =  ApplicationContextProvider.getApplicationContext().getBean(IContributoFacadeSRV.class);
		// Se non è presente su FileNet è come se non fosse presente il contenuto
		this.removeContent = contributo.getGuidContributo() == null;
	}

	/**
	 * Gestisce l'upload del file verificando la correttezza del file name.
	 * In caso di errore viene mostrato il popup tipico all'utente.
	 * @param event
	 */
	public void handleMainFileUpload(FileUploadEvent event) {
		try {
			event = checkFileName(event);
			if (event == null) {
				return;
			}
			docPrincipale = event.getFile();

			final String fileName = docPrincipale.getFileName();
			final String tipoFile = docPrincipale.getContentType(); //mimeType
			final byte[] contentFile = docPrincipale.getContents();
			
			contributo.setFile(fileName, tipoFile, contentFile);
			this.removeContent = false;
		} catch (final Exception e) {
			LOGGER.error(IMPOSSIBILE_RECUPERARE_IL_FILE, e);
			FacesHelper.showMessage(null, "Impossibile caricare il file", MessageSeverityEnum.ERROR);
		}
    }

	/**
	 * Restituisce lo StreamedContent definito dal content del contributo.
	 * @return StreamedContent
	 */
	public StreamedContent getDownload() {
		StreamedContent sc = null;
		try {
			final FileDTO file = contributoSRV.getContent(utente, contributo.getFlagEsterno(), contributo.getIdContributo());
			final InputStream io = new ByteArrayInputStream(file.getContent());
			sc = new DefaultStreamedContent(io, file.getMimeType(), file.getFileName());
		} catch (final Exception e) {
			LOGGER.error(IMPOSSIBILE_RECUPERARE_IL_FILE, e);
			FacesHelper.showMessage(null, IMPOSSIBILE_RECUPERARE_IL_FILE, MessageSeverityEnum.ERROR);
		}
		return sc;
	}

	/**
	 * Gestisce l'eliminazione del content impostando a null i parametri e a true il flag di rimozione.
	 */
	public void removeContent() {
		this.docPrincipale = null;
		this.contributo.setFile(null, null, null);
		this.removeContent = true;
	}

	/**
	 * Esegue l'aggiornamento del contributo mostrando un messaggio che comunica l'esito.
	 */
	public void registra() {
		final EsitoOperazioneDTO esito = contributoSRV.aggiornaContributo(contributo, utente);
		
		if (esito.isEsito()) {
			FacesHelper.showMessage(null, "Contributo modificato con successo", MessageSeverityEnum.INFO);
		} else {
			FacesHelper.showMessage(null, esito.getNote(), MessageSeverityEnum.ERROR);
		}
	}

	/**
	 * Restituisce il documento principale come UploadedFile.
	 * @return documento principale
	 */
	public UploadedFile getDocPrincipale() {
		return docPrincipale;
	}

	/**
	 * Imposta il documento principale come UploadedFile.
	 * @param docPrincipale
	 */
	public void setDocPrincipale(final UploadedFile docPrincipale) {
		this.docPrincipale = docPrincipale;
	}

	/**
	 * Restituisce il DTO del contributo.
	 * @return contributo
	 */
	public ContributoDTO getContributo() {
		return contributo;
	}

	/**
	 * Imposta il contributo utilizzando il DTO: ContributoDTO.
	 * @param contributo
	 */
	public void setContributo(final ContributoDTO contributo) {
		this.contributo = contributo;
	}

	/**
	 * Restituisce il valore del flag: removeContent.
	 * @return removeContent
	 */
	public boolean isRemoveContent() {
		return removeContent;
	}
	
}
