package it.ibm.red.web.handler;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

/**
 * Factory handler per le eccezioni Evo.
 */
public class EVOExceptionHandlerFactory extends ExceptionHandlerFactory {

	/**
	 * Factory.
	 */
	private final ExceptionHandlerFactory wrapped;
    
    /**
     * Costruttore.
     * @param wrapped
     */
    public EVOExceptionHandlerFactory(final ExceptionHandlerFactory wrapped) {
        this.wrapped = wrapped;
    }
    
    /**
     * Restituisce l'handler per le eccezioni.
     * @return handler exception
     */
    @Override
    public ExceptionHandler getExceptionHandler() {
        return new EVOExceptionHandler(wrapped.getExceptionHandler());
    }
    
    /**
     * Restituisce la factory.
     * @return ExceptionHandlerFactory
     */
    @Override
    public ExceptionHandlerFactory getWrapped() {
        return wrapped;
    }
}