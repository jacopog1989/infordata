package it.ibm.red.web.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.math.NumberUtils;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.enums.TipoRubricaEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IRubricaSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * Converter contatto.
 */
@FacesConverter("contattoConverter")
public class ContattoConverter implements Converter {

    /**
     * Logger.
     */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContattoConverter.class);

	/**
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.String).
	 */
	@Override
	public Object getAsObject(final FacesContext fc, final UIComponent uic, final String value) {
		Contatto contatto = new Contatto();

		try {
			if (StringUtils.isNullOrEmpty(value)) {
				return contatto;
			}
			
			final IRubricaSRV rubSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaSRV.class);
			final IUtenteSRV utenteSRV = ApplicationContextProvider.getApplicationContext().getBean(IUtenteSRV.class);
			Long idContatto;
			
			final String[] vals = value.split(":");
			if (vals != null) {
				final String val0 = vals[0].trim();
				
				if (vals.length == 2) {
					final String val1 = vals[1].trim();
					if (NumberUtils.isParsable(val1)) {
						idContatto = Long.parseLong(val1);
						
						if ("CONTATTO".equalsIgnoreCase(val0)) {
							contatto = rubSRV.getContattoByID(idContatto);
							if (contatto != null) {
								contatto.setAliasStringforEmail(getInfoContattoAsString(contatto));
								contatto.setIdentificativo("CONTATTO:" + contatto.getContattoID());
							}
						} else if ("GRUPPOEMAIL".equalsIgnoreCase(val0)) {
							contatto = rubSRV.getGruppoEmailByID(idContatto);
							if (contatto != null) {
								contatto.setAliasStringforEmail("GRUPPO: " + contatto.getAliasContatto());
								contatto.setIdentificativo("GRUPPOEMAIL:" + contatto.getContattoID());
							}
						} else if ("CONTATTO_INTERNO".equalsIgnoreCase(val0)) {
							final Utente u = utenteSRV.getUtenteById(idContatto);
							contatto.setTipoRubricaEnum(TipoRubricaEnum.INTERNO);
							contatto.setIdUtente(u.getIdUtente());
							contatto.setNome(u.getNome());
							contatto.setCognome(u.getCognome());
							contatto.setAliasContatto(u.getCognome() + " " + u.getNome());
							contatto.setAliasStringforEmail(getInfoContattoAsString(contatto));
							contatto.setIdentificativo("CONTATTO_INTERNO:" + contatto.getIdUtente());
						}
					}
				} else if (vals.length == 1 && NumberUtils.isParsable(vals[0].trim())) {
					idContatto = Long.parseLong(vals[0].trim());
					contatto = rubSRV.getContattoByID(idContatto);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
		}		

		return contatto;
	}


	private static String getInfoContattoAsString(final Contatto contatto) {
		String valoreContatto;
		valoreContatto = Constants.EMPTY_STRING;

		if (contatto != null) {
			if (contatto.getAliasContatto() != null) {
				valoreContatto = contatto.getAliasContatto();
			}
			if (contatto.getCognome() != null) {
				valoreContatto = valoreContatto + " " + contatto.getCognome();
			}
			if (contatto.getNome() != null) {
				valoreContatto = valoreContatto + " " + contatto.getNome();
			}
			valoreContatto = valoreContatto + " [ " + contatto.getMail() + " ]";
		}
		
		return valoreContatto;
	}

	/**
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.Object).
	 */
	@Override
	public String getAsString(final FacesContext fc, final UIComponent uic, final Object object) {
		String output = null;
		
		if (object instanceof Contatto) {
			final Contatto contatto = (Contatto) object;
			
			if (contatto.getContattoID() == null) {
				if (contatto.getIdUtente() != null) {
					output = "CONTATTO_INTERNO: " + contatto.getIdUtente();
				}
			} else if (StringUtils.isNullOrEmpty(contatto.getIdentificativo())) {
				output = contatto.getContattoID().toString();
			} else {
				output = contatto.getIdentificativo();
			}
		} else {
			output = object.toString();
		}
		
		return output;
	}
}
