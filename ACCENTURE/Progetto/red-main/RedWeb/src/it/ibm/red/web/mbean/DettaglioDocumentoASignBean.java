package it.ibm.red.web.mbean;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.ASignStepDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.ResponsesDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * @author SimoneLungarella
 * 
 */
@Named(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_ASIGN_BEAN)
@ViewScoped
public class DettaglioDocumentoASignBean extends DettaglioDocumentoAbstractBean {

	/**
	 * La costante serial version UID.
	 */
	private static final long serialVersionUID = -3288632682027067198L;

	/**
	 * LOGGER della classe.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DettaglioDocumentoASignBean.class);
	
	/**
	 * Flag dettaglio openable.
	 */
	private boolean dettaglioOpenable;

	/**
	 * Flag disabilitazione tab assegnazioni.
	 */
	private Boolean disableAssegnazioniTab;
	
	/**
	 * Informazioni sul dettaglio.
	 */
	protected DetailDocumentRedDTO detail;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		super.postConstruct();
		
		dettaglioOpenable = false;
		disableAssegnazioniTab = false;
	}
	
	@Override
	public void openDocumentPopup() {
		// Non esiste il dettaglio esteso per item nel processo di firma asincrona
	}
	
	@Override
	public void showDettaglioAfterLock() {
		// Non occorre fare niente in questo metodo.
	}


	@Override
	public void verificaFirmaDocPrincipaleAllegati() {
		// Non occorre fare niente in questo metodo.
	}

	/**
	 * Consente di effettuare il download del log associato allo step selezionato.
	 * @param step
	 * @return StreamedContent per il download del log
	 */
	public StreamedContent downloadStepLog(ASignStepDTO step) {
		StreamedContent file = null;
		try {
			String log = aSignSRV.getLog(detail.getaSignItem().getId(), step.getStep());
			if (!StringUtils.isNullOrEmpty(log)) {
				InputStream inputStream = new ByteArrayInputStream(log.getBytes(StandardCharsets.UTF_8));
				file = new DefaultStreamedContent(inputStream, Constants.ContentType.TEXT, "log.txt");
				showInfoMessage("Download eseguito con successo.");
			} else {
				showWarnMessage("Nessun log presente.");
			}
		} catch (Exception e) {
			LOGGER.error("Errore durante il download del log", e);
			showError("Errore durante il download del log");
		}
		return file;
	}
	
	/**
	 * Effettua il reset dei retry sull'item.
	 */
	public void resetRetry() {
		try {
			aSignSRV.resetRetry(detail.getaSignItem().getId());
			showInfoMessage("Item resettato con successo.");
		} catch (Exception e) {
			LOGGER.error("Errore durante il reset dell'item",e);
			showError("Errore durante il reset dell'item");
		}
		
		detail.setaSignItem(aSignSRV.getItem(detail.getaSignItem().getId()));
		FacesHelper.update("eastSectionForm:statusInputId", "eastSectionForm:accordionId:resetBtn", "eastSectionForm:stepInputId", 
				"eastSectionForm:accordionId:inputNRetryIdinput", "eastSectionForm:accordionId:DataUltimoRetryId");
	}

	/**
	 * Imposta le response valide sui documenti della coda preparazione alla spedizione.
	 * @param m master su cui recuperare le response
	 */
	@Override
	public void setResponse(final MasterDocumentRedDTO m) {
		responseDetail = new ArrayList<>();
		
		//Recupero response Filenet e Applicative -->
		ResponsesDTO respDto = responseRedSRV.refineReponsesSingle(m, utente, Boolean.FALSE);
			
		responseDetail = respDto.getResponsesEnum();
	}

	/**
	 * Imposta il dettaglio.
	 * @param master
	 */
	@Override
	public void setDetail(MasterDocumentRedDTO master) {
		setDetail(documentoRedSRV.getDocumentDetail(master.getDocumentTitle(), utente, master.getWobNumber(), master.getClasseDocumentale()));
	}
	
	/**
	 * True se dettaglio apribile, false altrimenti.
	 * @return flag dettaglio apribile
	 */
	public boolean isDettaglioOpenable() {
		return dettaglioOpenable;
	}

	/**
	 * True se tab assegnazioni disabilitato, false altrimenti.
	 * @return flag disabilitazione tab assegnazioni
	 */
	public Boolean getDisableAssegnazioniTab() {
		return disableAssegnazioniTab;
	}

	/**
	 * Imposta il flag disabilitazione tab assegnazioni.
	 * @param disableAssegnazioniTab
	 */
	public void setDisableAssegnazioniTab(Boolean disableAssegnazioniTab) {
		this.disableAssegnazioniTab = disableAssegnazioniTab;
	}

	/**
	 * Restituisce il dettaglio.
	 * @return detail
	 */
	public DetailDocumentRedDTO getDetail() {
		return detail;
	}

	/**
	 * Imposta il dettaglio documento RED.
	 * @param detail
	 */
	public void setDetail(DetailDocumentRedDTO detail) {
		this.detail = detail;
	}
	
}
