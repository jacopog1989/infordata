package it.ibm.red.web.scadenziario.mbean.component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.google.common.net.MediaType;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentoScadenzatoDTO;
import it.ibm.red.business.dto.IdDocumentDTO;
import it.ibm.red.business.dto.PEDocumentoScadenzatoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PeriodoScadenzaEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV;
import it.ibm.red.business.service.facade.IScadenziaroStrutturaFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.mbean.DocumentManagerBean;
import it.ibm.red.web.document.mbean.component.AbstractComponent;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.export.ExportExcelHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.component.GoToCodaPanelComponent;
import it.ibm.red.web.mbean.interfaces.IGoToCodaComponent;

/**
 * Gestisce la tabella di visualizzazione dei documenti scadenzati per utente .
 *
 */
public class DocumentiScadenzatiComponent extends AbstractComponent {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1515271593328170721L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DocumentiScadenzatiComponent.class);
	
	/**
	 * Utente.
	 */
	private final UtenteDTO utente;
	
	/**
	 * Pannello delle code in cui si trova il doucumento selezionato.
	 */
	private final GoToCodaPanelComponent goToCodaPanel;
	
	/**
	 * Datatable documenti.
	 */
	private SimpleDetailDataTableHelper<DocumentoScadenzatoDTO> dth;
	
	/**
	 * Servizio.
	 */
	private final IScadenziaroStrutturaFacadeSRV scadenziarioSRV;
	
	/**
	 * Servizio.
	 */
	private final IDocumentoRedFacadeSRV documentoRedSRV;
	
	/**
	 * Flag popup visibile.
	 */
	private boolean popupDocumentVisible;
	
	/**
	 * Costruttore del component, inizializza i Service e il SessionBean.
	 * @param inDocumentList
	 */
	public DocumentiScadenzatiComponent(final List<DocumentoScadenzatoDTO> inDocumentList) {
		final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sb.getUtente();
		scadenziarioSRV = ApplicationContextProvider.getApplicationContext().getBean(IScadenziaroStrutturaFacadeSRV.class);
		documentoRedSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoRedFacadeSRV.class);
		this.dth = new SimpleDetailDataTableHelper<>(inDocumentList);
		this.goToCodaPanel = new GoToCodaPanelComponent();
	}

	/**
	 * Restituisce lo StreamedContent per il download dell'excel dello scadenziario.
	 * @return StreamedContent per il download
	 */
	public StreamedContent exportExcel() {

		StreamedContent stream = null;
		
		try {
			if (dth.getMasters() == null || dth.getMasters().isEmpty()) {
				showErrorMessage("Non è presente nessun documento da stampare.");
				return null;
			}
			
			final Long idUfficio = dth.getMasters().iterator().next().getUfficioDestinatario().getId();
				
			final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			final ExportExcelHelper exportExcelHelper = new ExportExcelHelper();
			// Si costruisce il workbook Excel
			
			final List<DocumentoScadenzatoDTO> lista5 = new ArrayList<>();
			final List<DocumentoScadenzatoDTO> lista10 = new ArrayList<>();
			final List<DocumentoScadenzatoDTO> lista11 = new ArrayList<>();
			final List<DocumentoScadenzatoDTO> listaSc = new ArrayList<>();
			final Map<PeriodoScadenzaEnum, List<PEDocumentoScadenzatoDTO>> map = scadenziarioSRV.retrieveDocumentiPerPeriodoScadenza(idUfficio, utente);
			if (map.get(PeriodoScadenzaEnum.MINORE_5) != null) {
				lista5.addAll(scadenziarioSRV.retrieveIdDocumentoByPeIdDocument(map.get(PeriodoScadenzaEnum.MINORE_5), utente));
			}
			if (map.get(PeriodoScadenzaEnum.MINORE_10) != null) {
				lista10.addAll(scadenziarioSRV.retrieveIdDocumentoByPeIdDocument(map.get(PeriodoScadenzaEnum.MINORE_10), utente));
			}
			if (map.get(PeriodoScadenzaEnum.MAGGIORE_11) != null) {
				lista11.addAll(scadenziarioSRV.retrieveIdDocumentoByPeIdDocument(map.get(PeriodoScadenzaEnum.MAGGIORE_11), utente));
			}
			if (map.get(PeriodoScadenzaEnum.SCADUTO) != null) {
				listaSc.addAll(scadenziarioSRV.retrieveIdDocumentoByPeIdDocument(map.get(PeriodoScadenzaEnum.SCADUTO), utente));
			}
			
			final HSSFWorkbook excel = exportExcelHelper.getExportScadenzario(lista5, lista10, lista11, listaSc);
			excel.write(outputStream);
			stream = new DefaultStreamedContent(new ByteArrayInputStream(outputStream.toByteArray()), 
					MediaType.MICROSOFT_EXCEL.toString(), 
					"Scadenzario_" + DateUtils.dateToString(new Date(), DateUtils.DD_MM_YYYY_EXPORT) + ".xls", 
					StandardCharsets.UTF_8.name());
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage("Errore nel recupero delle informazioni.");
		}
		
		return stream;
	
	}
	
	/**
	 * metodoche utilizza la coda recuperata sopra per la navigazione nella stessa.
	 */
	public void goToCoda(final IdDocumentDTO ids) {
		goToCodaPanel.initialize(ids.getDocumentTitle(), ids.getNumeroDocumento());
	}
	
	/**
	 * Apertura del dettaglio esteso.
	 * 
	 * @param index posizione della riga
	 */
	public void openDocumentPopup(final Object index) {
		try {
			final Integer i = (Integer) index;
			final DocumentoScadenzatoDTO d = (DocumentoScadenzatoDTO) dth.getMasters().toArray()[i];  
			
			final DetailDocumentRedDTO detail = documentoRedSRV.getDocumentDetail(d.getCe().getDocumentTitle(), utente, d.getPe().getWobNumber(), null);
			
			final DocumentManagerBean bean = FacesHelper.getManagedBean(Constants.ManagedBeanName.DOCUMENT_MANAGER_BEAN);
			bean.setChiamante(ConstantsWeb.MBean.SCADENZIARIO_BEAN);
			bean.setDetail(detail);
						
			this.popupDocumentVisible = true;
			
			FacesHelper.executeJS("openDettaglioEstesoForScadenzario();");
			
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage("Errore durante l'operazione di apertura del dettaglio esteso.");
		}
		
	}

	/**
	 * Restituisce l'helper del datatable.
	 * @return datatable helper
	 */
	public SimpleDetailDataTableHelper<DocumentoScadenzatoDTO> getDth() {
		return dth;
	}

	/**
	 * Imposta l'helper del datatable.
	 * @param dth
	 */
	public void setDth(final SimpleDetailDataTableHelper<DocumentoScadenzatoDTO> dth) {
		this.dth = dth;
	}

	/**
	 * Restituisce il component IGoToCodaComponent.
	 * @return IGoToCodaComponent
	 */
	public IGoToCodaComponent getGoToCodaPanel() {
		return this.goToCodaPanel;
	}

	/**
	 * Restituisce true se il popup document è visibile, false altrimenti.
	 * @return true se popup visibile, false altrimenti
	 */
	public boolean isPopupDocumentVisible() {
		return popupDocumentVisible;
	}

	/**
	 * Imposta il flag popupDocumentVisible.
	 * @param popupDocumentVisible
	 */
	public void setPopupDocumentVisible(final boolean popupDocumentVisible) {
		this.popupDocumentVisible = popupDocumentVisible;
	}

	/**
	 * Restituisce il nome per effettuare l'export del file.
	 * @return nome file per effettuarne l'export
	 */
	public String getNomeExport() {
		final Date now = new Date();
		final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy_HH.mm.ss");
		final String nowTime = sdf.format(now.getTime());
		
		return "docScadenza_" + nowTime;
	}

}