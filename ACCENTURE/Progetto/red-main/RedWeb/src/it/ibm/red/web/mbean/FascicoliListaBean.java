package it.ibm.red.web.mbean;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.formula.eval.NotImplementedException;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.MasterFascicoloDTO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ITitolarioFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.enums.TreeNodeTypeEnum;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.component.DettaglioFascicoloComponent;

/**
 * Bean fascicoli.
 */
@Named(ConstantsWeb.MBean.FASCICOLI_BEAN)
@ViewScoped
public class FascicoliListaBean extends AbstractTreeBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 481155659265440698L;

	/**
	 * Messaggio errore caricamento fascicoli.
	 */
	private static final String ERROR_CARICAMENTO_FASCICOLI_MSG = "Errore nel caricamento dei fascicoli.";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FascicoliListaBean.class.getName());

	/**
	 * Servizio.
	 */
	private ITitolarioFacadeSRV titolarioSRV;

	/**
	 * Datatable fascicoli.
	 */
	private SimpleDetailDataTableHelper<MasterFascicoloDTO> fascicoli;

	/**
	 * Flag non classificati.
	 */
	private boolean nonClassificati;

	/**
	 * Dettaglio fascicolo.
	 */
	private DettaglioFascicoloComponent dettaglioFascicolo;

	/**
	 * Indice di classificazione selezionato.
	 */
	private String selectedIndiceDiClassificazione;

	/**
	 * Numero documenti non classificati.
	 */
	private String counterNumeroNonClassificati;

	/**
	 * Flag nessuna selezione.
	 */
	private boolean nessunaSelezione;

	/**
	 * Titolario.
	 */
	private TitolarioDTO objTito;

	/**
	 * @return the objTito
	 */
	public TitolarioDTO getObjTito() {
		return objTito;
	}

	/**
	 * @param objTito the objTito to set
	 */
	public void setObjTito(final TitolarioDTO objTito) {
		this.objTito = objTito;
	}

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {

		final SessionBean session = FacesHelper.getManagedBean(MBean.SESSION_BEAN);
		utente = session.getUtente();

		init("INDICI");
		titolarioSRV = ApplicationContextProvider.getApplicationContext().getBean(ITitolarioFacadeSRV.class);

		fascicoli = new SimpleDetailDataTableHelper<>();
		dettaglioFascicolo = new DettaglioFascicoloComponent();

		loadDataTable();

	}

	/**
	 * Gestisce il caricamento del datatable in base alla selezione dell'indice di
	 * classificazione o del button dei fascicoli non classificati.
	 */
	public void loadDataTable() {
		try {
			objTito = (TitolarioDTO) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.TITO_OBJ, true);

			if (objTito != null) {
				// E' stato selezionato un indice di classificazione
				loadByIndiciClassificazione(objTito.getIndiceClassificazione(), objTito.getDescrizione());
			} else {
				// E' stato selezionato il button dei fascicoli non classificati
				loadFascicoliNC();
			}

		} catch (final Exception e) {
			LOGGER.error(ERROR_CARICAMENTO_FASCICOLI_MSG, e);
			showError(ERROR_CARICAMENTO_FASCICOLI_MSG);
		}

	}

	/**
	 * Carica il datatable dei faldoni in base all'indice di classificazione
	 * selezionato.
	 * 
	 * @param indiceClassificazione
	 * @param descClassificazione
	 */
	public void loadByIndiciClassificazione(final String indiceClassificazione, final String descClassificazione) {
		try {
			final Long d = new Date().getTime();
			selectedIndiceDiClassificazione = indiceClassificazione + " - " + descClassificazione;

			final Collection<MasterFascicoloDTO> fascicoliByIndice = getFascicoloSRV().getFascicoliByIndiceDiClassificazione(getUtente(), indiceClassificazione);
			fascicoli.setMasters(fascicoliByIndice);
			nessunaSelezione = false;
			nonClassificati = false;
			hideDocument();

			if (!fascicoli.getMasters().isEmpty()) {
				fascicoli.selectFirst(true);
				openDettaglio(fascicoli.getCurrentMaster());
			} else {
				fascicoli.setCurrentMaster(null);
			}

			FacesHelper.update("centralSectionForm:labelContentFascicoli");
			FacesHelper.update("centralSectionForm:labelFaldoni");
			FacesHelper.update("centralSectionForm:gridtreeFaldoniId");
			FacesHelper.update("eastSectionForm:idDettaglioDoc_Fas");
			LOGGER.error("Fine CARICAMENTO Indice di Classificazione -> " + (d - (new Date().getTime())));
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(ERROR_CARICAMENTO_FASCICOLI_MSG);
		}
	}

	/**
	 * Azione triggerata dal click del pulsantone non classificati nel vertical menu
	 */
	public void loadFascicoliNC() {

		try {
			final Long d = new Date().getTime();
			final Collection<MasterFascicoloDTO> fascicoliNC = getFascicoloSRV().getFascicoliNonClassificati(getUtente());
			fascicoli.setMasters(fascicoliNC);
			nessunaSelezione = false;
			nonClassificati = true;
			hideDocument();

			if (!fascicoli.getMasters().isEmpty()) {
				fascicoli.selectFirst(true);
				openDettaglio(fascicoli.getCurrentMaster());
			} else {
				fascicoli.setCurrentMaster(null);
			}

			FacesHelper.update("centralSectionForm:labelContentFascicoli");
			FacesHelper.update("centralSectionForm:idOutSX_Center");
			FacesHelper.update("centralSectionForm:gridtreeFaldoniId");
			FacesHelper.update("eastSectionForm:idDettaglioDoc_Fas");
			LOGGER.error("Fine CARICAMENTO Fascicoli non classificati -> " + (d - (new Date().getTime())));
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei Fascicoli non Classificati", e);
			showError("Errore durante il recupero dei Fascicoli non Classificati");
		}

	}

	private DefaultTreeNode addNodoIndice(final TreeNode parent, final TitolarioDTO t) {
		return createNodeLeaf(parent, TreeNodeTypeEnum.INDICE, t);
	}

	private void addIndici(final TreeNode node) {
		if (TreeNodeTypeEnum.INDICE.getType().equals(node.getType())) {
			final TitolarioDTO tPadre = (TitolarioDTO) node.getData();
			final List<TitolarioDTO> ts = titolarioSRV.getFigliByIndice(getUtente().getIdAoo(), tPadre.getIndiceClassificazione());
			for (final TitolarioDTO t : ts) {
				if (t != null && StringUtils.isNotBlank(t.getIndiceClassificazione())) {
					if (t.getNumeroFigli() > 0) {
						createNode(node, TreeNodeTypeEnum.INDICE, t, false);
					} else {
						addNodoIndice(node, t);
					}
				}
			}
		}
	}

	/**
	 * Metodo chiamato la prima volta che il nodo viene espanso.
	 * 
	 * @param event
	 */
	public void onNodeExpand(final NodeExpandEvent event) {
		final TreeNode expandedNode = event.getTreeNode();
		expandedNode.getChildren().clear();
		addIndici(expandedNode);
	}

	/**
	 * Gestisce l'evento "Select" associato ad un nodo del tree degli indici di
	 * classificazione.
	 * 
	 * @param event
	 */
	public void onNodeSelect(final NodeSelectEvent event) {
		try {
			final TreeNode node = event.getTreeNode();
			final String indiceDiClassificazione = ((TitolarioDTO) node.getData()).getIndiceClassificazione();
			selectedIndiceDiClassificazione = indiceDiClassificazione;

			final Collection<MasterFascicoloDTO> fascicoliNC = getFascicoloSRV().getFascicoliByIndiceDiClassificazione(getUtente(), indiceDiClassificazione);

			fascicoli.setMasters(fascicoliNC);

			nessunaSelezione = false;
			nonClassificati = false;
			hideDocument();

		} catch (final Exception e) {
			LOGGER.error(e);
			showError(ERROR_CARICAMENTO_FASCICOLI_MSG);
		}
	}

	/**
	 * Gestisce l'evento "Select" associato alle righe del datatable dei fascicoli.
	 * 
	 * @param event
	 */
	public void onFascicoloSelect(final SelectEvent event) {
		fascicoli.rowSelector(event);
		openDettaglio(fascicoli.getCurrentMaster());
	}

	/**
	 * Mostra il dettaglio del fascicolo selezionato.
	 * 
	 * @param fascicolo
	 */
	public void openDettaglio(final MasterFascicoloDTO fascicolo) {
		if (CollectionUtils.isNotEmpty(fascicolo.getDocumenti())) {
			final Collection<DocumentoFascicoloDTO> documenti = getDocumentiFascicolo(fascicolo);
			fascicolo.setDocumenti(documenti);
		}
		dettaglioFascicolo.setDetail(fascicolo);
	}

	/**
	 * Nasconde il dettaglio attualmente visibile.
	 */
	@Override
	public void hideDocument() {
		if (fascicoli != null && fascicoli.getCurrentMaster() != null) {
			fascicoli.setCurrentMaster(null);
		}
		dettaglioFascicolo.unsetDetail();
	}

	/**
	 * Esegue un reset del datatable identificato dall'idComponent e dell'helper
	 * passato come parametro.
	 * 
	 * @param idComponent
	 * @param dth
	 */
	public void clearFilterDataTable(final String idComponent, final SimpleDetailDataTableHelper<MasterFascicoloDTO> dth) {
		final DataTable d = getDataTableByIdComponent(idComponent);
		if (d != null) {
			d.reset();
			if (dth != null && dth.getMasters() != null) {
				d.setValue(dth.getMasters());
			}
		}
	}

	/**
	 * GET & SET.
	 ************************************************************************************************/
	/**
	 * Restituisce true se non classificati, false altrimenti.
	 * 
	 * @return true se non classificati, false altrimenti
	 */
	public boolean isNonClassificati() {
		return nonClassificati;
	}

	/**
	 * Restituisce l'indice di classificazione selezionato.
	 * 
	 * @return indice di classificazione
	 */
	public String getSelectedIndiceDiClassificazione() {
		return selectedIndiceDiClassificazione;
	}

	/**
	 * Imposta l'indice di classificazione associato.
	 * 
	 * @param selectedIndiceDiClassificazione
	 */
	public void setSelectedIndiceDiClassificazione(final String selectedIndiceDiClassificazione) {
		this.selectedIndiceDiClassificazione = selectedIndiceDiClassificazione;
	}

	/**
	 * @return the counterNumeroNonClassificati
	 */
	public final String getCounterNumeroNonClassificati() {
		return counterNumeroNonClassificati;
	}

	/**
	 * @param counterNumeroNonClassificati the counterNumeroNonClassificati to set
	 */
	public final void setCounterNumeroNonClassificati(final String counterNumeroNonClassificati) {
		this.counterNumeroNonClassificati = counterNumeroNonClassificati;
	}

	/**
	 * Restituisce true se non risulta selezionato nessun fascicolo, false
	 * altrimenti.
	 * 
	 * @return true se non risulta selezionato nessun fascicolo, false altrimenti
	 */
	public boolean isNessunaSelezione() {
		return nessunaSelezione;
	}

	/**
	 * Imposta il flag associato all'assenza di selezione.
	 * 
	 * @param nessunaSelezione
	 */
	public void setNessunaSelezione(final boolean nessunaSelezione) {
		this.nessunaSelezione = nessunaSelezione;
	}

	/**
	 * Restituisce l'helper del datatable dei fascicoli.
	 * 
	 * @return fascicoli
	 */
	public SimpleDetailDataTableHelper<MasterFascicoloDTO> getFascicoli() {
		return fascicoli;
	}

	/**
	 * Imposta l'helper dei fascicoli.
	 * 
	 * @param fascicoli
	 */
	public void setFascicoli(final SimpleDetailDataTableHelper<MasterFascicoloDTO> fascicoli) {
		this.fascicoli = fascicoli;
	}

	/**
	 * Restituisce il component per la gestione del dettaglio del fascicolo.
	 * 
	 * @return il component
	 */
	public DettaglioFascicoloComponent getDettaglioFascicolo() {
		return dettaglioFascicolo;
	}

	/**
	 * Imposta il component per la gestione del dettaglio del fascicolo.
	 * 
	 * @param dettaglioFascicolo
	 */
	public void setDettaglioFascicolo(final DettaglioFascicoloComponent dettaglioFascicolo) {
		this.dettaglioFascicolo = dettaglioFascicolo;
	}

	@Override
	public void onOpenTree(TreeNode nodo) {
		throw new NotImplementedException("Metodo non implementato");
	}

}