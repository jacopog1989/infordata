package it.ibm.red.web.mbean;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.google.common.net.MediaType;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.VerifyInfoDTO;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.constants.ConstantsWeb.SessionObject;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.component.DetailPreviewIntroComponent;
import it.ibm.red.web.mbean.interfaces.IVisualizzaDocumentoComponent;

/**
 * Component base visualizzazione documento.
 */
public class VisualizzaDocumentoComponentBase implements IVisualizzaDocumentoComponent {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 7659462922475563849L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(VisualizzaDocumentoComponentBase.class);

	/**
	 * Componente che gestisce la visualizzazione di un documento.
	 */
	protected DetailPreviewIntroComponent previewComponent;

	/**
	 * DataTableHelper per i documenti principali.
	 */
	/**
	 * Tabella documenti.
	 */
	protected SimpleDetailDataTableHelper<DetailDocumentRedDTO> docpDTH;

	/**
	 * Flag che gestisce la visualizzazione dell'anteprima pdf generata dall'xml e associata al flusso SIPAD.
	 */
	protected boolean previewVisibile;
	
	/**
	 * Costruttore di default.
	 * 
	 * @param inUtente
	 */
	public VisualizzaDocumentoComponentBase(final UtenteDTO inUtente) {
		this.docpDTH = new SimpleDetailDataTableHelper<>();
		this.previewComponent = new DetailPreviewIntroComponent(inUtente);
	}

	/**
	 * Imposta il dettaglio.
	 * 
	 * @param dettaglio
	 *            dettaglio da impostare
	 */
	@Override
	public void setDetail(final DetailDocumentRedDTO dettaglio) {
		docpDTH.setMasters(new ArrayList<>());
		docpDTH.getMasters().add(dettaglio);
		previewComponent.setDetailPreview(dettaglio.getDocumentTitle());
		docpDTH.setCurrentMaster(dettaglio);
	}

	/**
	 * Effettua l'unset del dettaglio.
	 */
	@Override
	public void unsetDetail() {
		docpDTH.setMasters(null);
		previewComponent.unsetDetail();
	}

	/**
	 * Gestisce l'evento di selezione delle righe del datatable dei documenti
	 * principali.
	 * 
	 * @param se
	 *            evento di selezione
	 */
	@Override
	public void rowSelectorDocPrincipale(final SelectEvent se) {
		handleEmptyRow();

		docpDTH.rowSelector(se);
		final DetailDocumentRedDTO dettaglio = docpDTH.getCurrentMaster();

		previewComponent.setDetailPreview(dettaglio.getDocumentTitle());
	}

	/**
	 * Metodo per deselezionare la riga della tabella non più attiva
	 */
	protected void handleEmptyRow() {
		docpDTH.setCurrentMaster(null);
	}

	/**
	 * Restituisce lo streamed content per il download del documento principale.
	 * Il documento principale è il primo della lista dei master del {@link #docpDTH}.
	 * 
	 * @return StreamedContent per il download
	 */
	@Override
	public StreamedContent downloadDoc() {
		final List<DetailDocumentRedDTO> listaDoc = (List<DetailDocumentRedDTO>) docpDTH.getMasters();
		final DetailDocumentRedDTO documentoPrincipale = listaDoc.get(0);
		StreamedContent file = null;
		file = download(documentoPrincipale.getMimeType(), documentoPrincipale.getNomeFile(), documentoPrincipale.getGuid());

		return file;
	}

	/**
	 * Gestisce l'invio del documento come mail.
	 * 
	 * @return streamed content
	 */
	@Override
	public StreamedContent inviaDocEmail() {
		StreamedContent output = null;
		StringBuilder sb = null;
		try {
			final IDocumentoFacadeSRV documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);
			final SessionBean sessionBean = FacesHelper.getManagedBean(MBean.SESSION_BEAN);
			final List<DetailDocumentRedDTO> listaDoc = (List<DetailDocumentRedDTO>) docpDTH.getMasters();
			final DetailDocumentRedDTO documentoPrincipale = listaDoc.get(0);
			final String documentTitleDaInviare = documentoPrincipale.getDocumentTitle();
			final FileDTO doc = documentoSRV.getDocumentoContentPreview(sessionBean.getUtente().getFcDTO(), 
					documentTitleDaInviare, false, sessionBean.getUtente().getIdAoo());

			if (doc != null) {
				final byte[] contentDoc = doc.getContent();
				final String mimeType = doc.getMimeType();
				final String contentString = Base64.getEncoder().encodeToString(contentDoc);

				sb = new StringBuilder();
				sb.append("data:message/rfc822 eml;charset=utf-8,\r\n");
				sb.append("To: \r\n");
				sb.append("Subject: \r\n");
				sb.append("X-Unsent: 1\r\n");
				sb.append("Content-Type: multipart/mixed; boundary=--boundary_text_string\r\n");
				sb.append("\r\n");
				sb.append("----boundary_text_string\r\n");
				sb.append("Content-Type: text/html; charset=UTF-8\r\n");
				sb.append("\r\n");
				sb.append("\r\n");
				sb.append("----boundary_text_string\r\n");
				sb.append("Content-Type: ").append(mimeType).append("; name=").append(doc.getFileName()).append("\r\n");
				sb.append("Content-Transfer-Encoding: base64\r\n");
				sb.append("Content-Disposition: attachment\r\n");
				sb.append("\r\n");
				sb.append(contentString).append("\r\n");
				sb.append("\r\n");

				final InputStream targetStream = new ByteArrayInputStream(sb.toString().getBytes());
				output = new DefaultStreamedContent(targetStream, MediaType.PDF.toString(), doc.getFileName() + ".eml");

			} else {
				LOGGER.error("Non è possibile inviare il documento per email");
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'eml originale.", e);
		}
		return output;
	}

	/**
	 * Gestisce l'apertura della dialog di stampa del documento.
	 */
	@Override
	public void openStampaDocumento() {
		final IDocumentoFacadeSRV documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);
		final SessionBean sessionBean = FacesHelper.getManagedBean(MBean.SESSION_BEAN);
		final List<DetailDocumentRedDTO> listaDoc = (List<DetailDocumentRedDTO>) docpDTH.getMasters();
		final DetailDocumentRedDTO documentoPrincipale = listaDoc.get(0);
		final String documentTitleDaInviare = documentoPrincipale.getDocumentTitle();
		final FileDTO doc = 
				documentoSRV.getDocumentoContentPreview(sessionBean.getUtente().getFcDTO(), documentTitleDaInviare, false, sessionBean.getUtente().getIdAoo());
		sessionBean.setRenderDialogStampa(true);
		FacesHelper.update("eastSectionForm:dlgStampaDocId");

		final byte[] newContent = PdfHelper.convertToPngPDFAndPDFtoPng(doc.getContent());
		final FileDTO docConvertito = new FileDTO(doc.getFileName(), newContent, doc.getMimeType());
		if (doc.getContent() != null) {
			FacesHelper.putObjectInSession(SessionObject.PRINT_FILE_SERVLET, docConvertito);
		}
	}

	/**
	 * Recupera e restituisce il content del file per il download.
	 * 
	 * @param mimeType
	 *            mimetype del documento da scaricare
	 * @param nomeFile
	 *            nome file da scaricare
	 * @param guid
	 *            identificativo filenet per il recupero del documento
	 * @return streamed content per il download
	 */
	protected StreamedContent download(final String mimeType, final String nomeFile, final String guid) {
		StreamedContent file = null;
		try {
			if (!StringUtils.isNullOrEmpty(guid)) {
				final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
				final IDocumentManagerFacadeSRV documentManagerSRV = 
						ApplicationContextProvider.getApplicationContext().getBean(IDocumentManagerFacadeSRV.class);
				final InputStream stream = documentManagerSRV.getStreamDocumentoByGuid(guid, sb.getUtente());
				file = new DefaultStreamedContent(stream, mimeType, nomeFile);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return file;
	}

	/* Getter e Setter ****************************************************/

	/**
	 * Restituisce il datatable helper dei documenti principali.
	 * 
	 * @return datatable helper
	 */
	@Override
	public SimpleDetailDataTableHelper<DetailDocumentRedDTO> getDocpDTH() {
		return docpDTH;
	}

	/**
	 * Imposta il datatable helper dei documenti principali.
	 * 
	 * @param docpDTH
	 */
	public void setDocpDTH(final SimpleDetailDataTableHelper<DetailDocumentRedDTO> docpDTH) {
		this.docpDTH = docpDTH;
	}

	/**
	 * Restituisce il componet per la preview.
	 * 
	 * @return previewComponent
	 */
	public DetailPreviewIntroComponent getPreviewComponent() {
		return previewComponent;
	}

	/**
	 * Effettua la verifica firma del documento principale aggiornandone lo stato.
	 */
	@Override
	public void verificaFirmaDocPrinc() {
		final SessionBean sessionBean = FacesHelper.getManagedBean(MBean.SESSION_BEAN);
		final ISignFacadeSRV signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignFacadeSRV.class);
		final VerifyInfoDTO verificaFirma = signSRV.aggiornaVerificaFirma(docpDTH.getCurrentMaster().getGuid(), sessionBean.getUtente());
		docpDTH.getCurrentMaster().setValueMetaadatoVerificaFirmaEnum(verificaFirma.getMetadatoValiditaFirma());
	}

	/**
	 * Restituisce il content associato al file identificato dal guid.
	 * @param guid
	 * @return content come InputStream
	 */
	protected InputStream getContent(String guid) {
		InputStream stream = null;
		if (!StringUtils.isNullOrEmpty(guid)) {
			final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
			final IDocumentManagerFacadeSRV documentManagerSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentManagerFacadeSRV.class);
			stream = documentManagerSRV.getStreamDocumentoByGuid(guid, sb.getUtente());
		}
		return stream;
	}
	
	/**
	 * Restituisce il flag associato al flusso sipad.
	 * @see Constants.Varie.FLUSSO_SIPAD.
	 * @return true se flusso sipad, false altrimenti
	 */
	public boolean isFlussoSipad() {
		final List<DetailDocumentRedDTO> listaDoc = (List<DetailDocumentRedDTO>) docpDTH.getMasters();
		final DetailDocumentRedDTO documentoPrincipale = listaDoc.get(0);
		return Constants.Varie.CODICE_FLUSSO_SIPAD.equals(documentoPrincipale.getCodiceFlusso());
	}
	

	/**
	 * Restituisce true se l'anteprima del pdf generato per il flusso SIPAD è renderizzata, false altrimenti.
	 * @return true se anteprima SIPAD visibile, false altrimenti
	 */
	public boolean isPreviewVisibile() {
		return previewVisibile;
	}

	/**
	 * Imposta il flag associato alla visibilità dell'anteprima pdf dell'xml associato al flusso SIPAD.
	 * @param showSipadDoc
	 */
	public void setPreviewVisibile(boolean previewVisibile) {
		this.previewVisibile = previewVisibile;
	}
	
}
