package it.ibm.red.web.sottoscrizioni.mbean.component;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.mbean.DocumentManagerBean;
import it.ibm.red.web.document.mbean.component.DetailDocumentRedComponent;
import it.ibm.red.web.document.mbean.component.DettaglioDocumentoStoricoComponent;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.helper.faces.MessageSeverityEnum;
import it.ibm.red.web.mbean.DettaglioDocumentoBean;
import it.ibm.red.web.sottoscrizioni.mbean.SottoscrizioniBean;

/**
 * Classe che si occupa di coordinare creare e distruggere i componenti relativi al dettaglio del documento 
 * 
 * Estende il dettaglio documento Abstract ma in realtà potrebbe tranquillamente estendere il {@link DettaglioDocumentoBean}.
 *
 */
public class DettaglioDocumentoSottoscrizioniComponent extends DettaglioDocumentoBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1398433544982960759L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DettaglioDocumentoSottoscrizioniComponent.class);

	/**
	 * Costruttore del component.
	 * @param utente
	 */
	public DettaglioDocumentoSottoscrizioniComponent(final UtenteDTO utente) {
		this.utente = utente;
		documentoRedSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoRedFacadeSRV.class);
		documentCmp = new DetailDocumentRedComponent(utente);
		postConstruct();
	}

	/**
	 * Imposta il dettaglio del documento identificato dal documentTitle.
	 * @param documentTitle
	 */
	public void setDetail(final String documentTitle)  {
		try {
			super.setDetail(documentTitle, null, null);
			setMenuAllegatiCommand("#{SottoscrizioniBean.documentDetail.changeSelectedDocumentTitle}");
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			FacesHelper.showMessage(null, "Errore nel recupero del documento: " + e.getMessage(), MessageSeverityEnum.ERROR);
		}
	}
	
	/**
	 * Quando creo il dettaglio assegno a sottoscrizione il giusto dettaglio.
	 */
	@Override
	public void setDetail() {
		super.setDetail();
		final SottoscrizioniBean sottoscrizioniBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SOTTOSCRIZIONI_BEAN);
		sottoscrizioniBean.setDocumentDetail(this);
	}
	
	/**
	 * Gestisce l'evento change sul document title selezionato.
	 */
	@Override
	public void changeSelectedDocumentTitle() {
		super.changeSelectedDocumentTitle();
		//Serve a evitare il resize del tab grande in seguito all'update del dettaglio (l'effetto impatta in particolare sulla preview)
		FacesHelper.executeJS("calculateIframeHeight();");
	}
	
	/**
	 * QUesto codice è uguale a {@link DettaglioDocumentoBean}.
	 */
	@Override
	public void openDocumentPopup() {
		
		try {
			if (CategoriaDocumentoEnum.INTERNO.getIds()[0].equals(documentCmp.getDetail().getIdCategoriaDocumento())) {
				showWarnMessage("Non è possibile aprire il dettaglio di un documento interno.");
				return;
			}
			
			loadAfterIntroPreview();
			
			final DocumentManagerBean bean = FacesHelper.getManagedBean(Constants.ManagedBeanName.DOCUMENT_MANAGER_BEAN);
			bean.setChiamante(ConstantsWeb.MBean.SOTTOSCRIZIONI_BEAN);
			bean.setDetail(this.documentCmp.getDetail());
						
			setPopupDocumentVisible(true);
			
			FacesHelper.executeJS("PF('dlgManageDocument_RM').show();");
			FacesHelper.executeJS("PF('dlgManageDocument_RM').toggleMaximize();");
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di recupero del documento.");
		}
	}

	/**
	 * Gestisce le azioni da eseguire al cambio del tab.
	 * @param indexPage
	 */
	public void onChangetab(final String indexPage) {
		storico =  new DettaglioDocumentoStoricoComponent(indexPage);
	}

	/**
	 * Redireziona l'utente sulla coda identificata da <code> q </code>.
	 */
	@Override
	public String goToCoda(final DocumentQueueEnum q) {
		final MasterDocumentRedDTO master = new MasterDocumentRedDTO();
		master.setDocumentTitle(this.getDocumentCmp().getDetail().getDocumentTitle());
		if (this.getDocumentCmp().getDetail().getNumeroDocumento() != null) {
			master.setNumeroDocumento(this.getDocumentCmp().getDetail().getNumeroDocumento());
		}
		super.documentCmp.setMaster(master);
		return super.goToCoda(q);
	}

	/**
	 * Restituisce il component per la gestione dello storico.
	 * @return DettaglioDocumentoStoricoComponent
	 */
	@Override
	public DettaglioDocumentoStoricoComponent getStorico() {
		return storico;
	}

	/**
	 * Imposta il component per la visualizzazione dello storico.
	 * @param storico
	 */
	@Override
	public void setStorico(final DettaglioDocumentoStoricoComponent storico) {
		this.storico = storico;
	}
}
