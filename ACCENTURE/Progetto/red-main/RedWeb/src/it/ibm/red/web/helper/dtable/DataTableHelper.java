package it.ibm.red.web.helper.dtable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleSelectEvent;

/**
 * The Class DataTableHelper.
 *
 * @author CPIERASC
 * 
 *         Helper per gestire i datatable (Primefaces).
 * @param <M>
 *            la struttura del master
 * @param <D>
 *            la struttura del detail
 */
public abstract class DataTableHelper<M extends Serializable, D extends Serializable> implements Serializable {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Master selezionati (utile solo in caso di selezione multipla).
	 */
	private List<M> selectedMasters;

	/**
	 * Collezione dei master gestiti dal datatable in esame.
	 */
	private Collection<M> masters;
	
	/**
	 * Master filtrati (utile per filtri).
	 */
	private List<M> filteredMasters;

	/**
	 * Master selezionato.
	 */
	private M currentMaster;
	
	/**
	 * Dettaglio del master selezionato.
	 */
	private D currentDetail;
	
	/**
	 * Flag per pilotare il render del dettaglio.
	 */
	private Boolean renderDetail;
	
	/**
	 * Flag da utilizzare per il Check all dei master.
	 */
	private Boolean checkAll = false;
	


	/**
	 * Costruttore da utilizzare per non impostare alcun master in creazione.
	 */
	protected DataTableHelper() {
		this(null);
	}

	/**
	 * Costruttore da utilizzare per impostare in creazione dei master.
	 * 
	 * @param inMasters	i master da impostare
	 */
	protected DataTableHelper(final Collection<M> inMasters) {
		if (inMasters != null) {
			setMasters(inMasters);
			checkAll = false;
		}		
	}
	
	/**
	 * Setter.
	 * 
	 * @param inCurrentMaster	master corrente
	 */
	public final void setCurrentMaster(final M inCurrentMaster) {
		this.currentMaster = inCurrentMaster;
	}

	/**
	 * Getter.
	 * 
	 * @return	master corrente
	 */
	public final M getCurrentMaster() {
		return currentMaster;
	}
	
	/**
	 * Getter collezione master selezionati (utile per multiselezione).
	 * 
	 * @return	collezione master selezionati
	 */
	public final List<M> getSelectedMasters() {
		return selectedMasters;
	}

	/**
	 * Setter collezione master selezionati (utile per multiselezione).
	 * 
	 * @param inSelectedMasters	collezione dei master selezionati da impostare
	 */
	public final void setSelectedMasters(final List<M> inSelectedMasters) {
		this.selectedMasters = inSelectedMasters;
	}

	/**
	 * Recupero dei master gestiti dal datatable.
	 * 
	 * @return	i master
	 */
	public final Collection<M> getMasters() {
		return masters;
	}

	/**
	 * Setter dei master gestiti.
	 * 
	 * @param inMasters	collezione dei master
	 */
	public final void setMasters(final Collection<M> inMasters) {
		if (inMasters != null) {
			this.masters = new ArrayList<>(inMasters);
		} else {
			this.masters = null;
		}
		selectedMasters = new ArrayList<>();
	}

	/**
	 * Getter del flag che indica se renderizzare o meno il dettaglio del master.
	 * 
	 * @return	il flag che indica se renderizzare o meno il dettaglio
	 */
	public final Boolean getRenderDetail() {
		return renderDetail;
	}

	/**
	 * Setter del flag per renderizzare il dettaglio.
	 * 
	 * @param inRenderDetail	valore del flag da impostare
	 */
	public final void setRenderDetail(final Boolean inRenderDetail) {
		this.renderDetail = inRenderDetail;
	}

	/**
	 * Listener per gestire la selezione/deselezione di tutte le righe del datatable (utile nel caso in cui sia presente la multiselezione).
	 * 
	 * @param se	l'evento generato dalla GUI da gestire
	 */
	public final void toggleSelector(final ToggleSelectEvent se) {
		selectedMasters = new ArrayList<>();
		if (se.isSelected()) {
			selectedMasters.addAll(masters);
		}
	}
	
	/**
	 * Metodo per impostare il dettaglio a partire dall'attuale master che viene
	 * fornito in input. La sua implementazione è demandata alla specifica gestione
	 * (ovviamente il caso più semplice è quello in cui il dettaglio coincide con il
	 * master, ma questo è possibile solo se il master contiene tutte le
	 * informazioni necessarie al dettaglio, la scelta della strategia è quindi
	 * lasciata a chi estenderà la classe).
	 * 
	 * @param master il master da cui partire per delineare il dettaglio
	 * @return il dettaglio
	 */
	protected abstract D setDetailFromCurrent(M master);

	/**
	 * Getter dell'attuale dettaglio.
	 * 
	 * @return	l'attuale dettaglio
	 */
	public final D getCurrentDetail() {
		return currentDetail;
	}
	
	/**
	 * Setter dell'attuale dettaglio.
	 * 
	 * @param detail	il dettaglio
	 */
	public final void setCurrentDetail(final D detail) {
		currentDetail = detail;
	}
	
	/**
	 * Metodo per la simulazione della selezione del master attualmente selezionato (utile nel caso in cui una modifica abbia effetto sul dettaglio visualizzato, in questo
	 * modo il dettaglio verrà ricaricato da capo).
	 */
	public final void refresh() {
		if (currentMaster != null) {
			select(currentMaster);
		}
	}

	/**
	 * Listener per la selezione di una riga.
	 * 
	 * @param se	evento generato dalla GUI alla selezione di una riga
	 */
	@SuppressWarnings("unchecked")
	public final void rowSelector(final SelectEvent se) {
		final M master = (M) se.getObject(); 
		select(master);
	}

	/**
	 * Seleziona il primo elemento del datatable (è possibile scegliere se valorizzare solamente il).
	 * 
	 * @param onlySelect	se a true non aggiunge l'elemento all'insieme dei selezionati
	 */
	public final void selectFirst(final Boolean onlySelect) {
		if (masters != null && !masters.isEmpty()) {
			select(masters.iterator().next());
			if (onlySelect == null || !onlySelect) {
				selectedMasters = new ArrayList<>();
				selectedMasters.add(masters.iterator().next());
			}
		} else {
			currentMaster = null;
			currentDetail = null;
			setRenderDetail(false);
		}
	}
	
	/**
	 * Metodo per selezionare un master.
	 * 
	 * @param master	master da selezionare
	 */
	public final void selectMaster(final M master) {
		if (masters != null) {
			select(master);
		}
	}
	
	/**
	 * Seleziona il master fornito in ingresso.
	 * 
	 * @param master	il master da selezionare
	 */
	private void select(final M master) {
		currentMaster = master;
		currentDetail = setDetailFromCurrent(currentMaster);
	}

	
	/**
	 * Listener per la deselezione di una riga.
	 */
	public final void rowUnselector() {
		final Boolean bNoMasters = masters == null || masters.isEmpty(); 
		final Boolean bMultipleMastersSelected = getSelectedMasters() != null && getSelectedMasters().size() > 1; 
		if (bNoMasters || bMultipleMastersSelected) {
			return;
		}
		currentMaster = null;
		currentDetail = null;
		setRenderDetail(false);
	}

	/**
	 * Metodo per recuperare il master successivo.
	 * 
	 * @return	master successivo
	 */
	public final M nextMaster() {
		Integer nIndex = 1;
		M previousMaster = null;
		if (currentMaster != null) {
			boolean found = false;
			for (final M master:masters) {
				if (found) {
					select(master);
					break;
				}
				if (master.equals(currentMaster)) {
					found = true;
				} else {
					previousMaster = master;
				}
				nIndex++;
			}
		}
		if (nIndex.equals(masters.size() + 1) && previousMaster != null) {
			//Sei l'ultimo master, ma non sei il primo
			select(previousMaster);
		}
		return currentMaster;
	}

	/**
	 * Metodo per recuperare il master precedente.
	 * 
	 * @return	master precedente
	 */
	public final M prevMaster() {
		M old = null;
		if (currentMaster != null) {
			for (final M master:masters) {
				if (master.equals(currentMaster) && old != null) {
					select(old);
					break;
				} else {
					old = master;
				}
			}
		}
		return currentMaster;
	}

	/**
	 * Restituisce la lista dei Master filtrati.
	 * @return Lista dei Master se definita, null altrimenti
	 */
	public List<M> getFilteredMasters() {
		return filteredMasters;
	}

	/**
	 * Definisce la lista dei Master filtrati.
	 * @param filteredMasters
	 */
	public void setFilteredMasters(final List<M> filteredMasters) {
		this.filteredMasters = filteredMasters;
	}

	/**
	 * Restituisce true se nessuno dei masters è null. Restituisce false altrimenti.
	 * @return checkAll
	 */
	public Boolean getCheckAll() {
		return checkAll && (masters != null && !masters.isEmpty());
	}

	/**
	 * @param checkAll
	 */
	public void setCheckAll(final Boolean checkAll) {
		this.checkAll = checkAll;
	}
}
