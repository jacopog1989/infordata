package it.ibm.red.web.document.mbean.component;

import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDetailFascicoloFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.DettaglioDocumentoFepaBean;
import it.ibm.red.web.mbean.FascicoloManagerBean;

/**
 * Component dettaglio documento Red Fepa.
 */
public class DetailDocumetRedFepaComponent extends DetailDocumentRedComponent {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 5529177191317032209L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DetailDocumetRedFepaComponent.class);

	/**
	 * Costruttore del component.
	 * @param utenteIn
	 */
	public DetailDocumetRedFepaComponent(final UtenteDTO utenteIn) {
		super(utenteIn);
	}

	/**
	 * @see it.ibm.red.web.document.mbean.component.DetailDocumentRedComponent#openFascicoloView(java.lang.Object).
	 */
	@Override
	public void openFascicoloView(final Object index) {
		FascicoloDTO currentFascicolo = null;	
		try {
			final IDetailFascicoloFacadeSRV detailFascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IDetailFascicoloFacadeSRV.class);
			final Integer i =  (Integer) index;
			final DettaglioDocumentoFepaBean fepaBean = (DettaglioDocumentoFepaBean) FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_FEPA_BEAN);
			currentFascicolo = getDetail().getFascicoli().get(i);	
			fepaBean.setDetailFascicolo(detailFascicoloSRV.getFascicoloDTOperTabFascicoloeFaldone(currentFascicolo, getUtente()));

			final FascicoloManagerBean bean = FacesHelper.getManagedBean(MBean.FASCICOLO_MANAGER_BEAN);

			bean.setDetail(fepaBean.getDetailFascicolo());
			bean.setCurrentFascicolo(currentFascicolo);
			fepaBean.setDialogFascicoloVisible(true);
			FacesHelper.executeJS("PF('dlgManageFascicolo_DF_FEPA').show()");
			FacesHelper.executeJS("PF('dlgManageFascicolo_DF_FEPA').toggleMaximize()");
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di recupero del fascicolo.");
		}
	}
}
