package it.ibm.red.web.helper.export;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor.HSSFColorPredefined;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;

import it.ibm.red.business.dto.DocumentoScadenzatoDTO;
import it.ibm.red.business.dto.SignerDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.utils.DateUtils;


/**
 * Helper per la creazione di file Excel personalizzati.
 * 
 * @author m.crescentini
 *
 */
public class ExportExcelHelper {
	
	/**
	 * Logger.
	 */
	private static final Logger LOGGER = Logger.getLogger(ExportExcelHelper.class);
	
	/**
	 * Headers verifica firma.
	 */
	private static final String[] HEADERS_VERIFICA_FIRMA = new String[] {
			"Dettaglio Firmatario", 
			"Tipo di firma", 
			"Motivo", 
			"Firma Valida", 
			"Data Apposizione Firma", 
			"Stato del certificato", 
			"Data inizio validità certificato", 
			"Data fine validità certificato"
	};
	
	/**
	 * Nome sheet verifica firma.
	 */
	public static final String SHEET_NAME_VERIFICA_FIRMA = "Dettaglio Firma Documento";
	
	
	/**
	 * Restituisce il workbook con l'export delle informazioni sulla verifica della firma.
	 * 
	 * @param data
	 * @return
	 */
	public final HSSFWorkbook getExportVerificaFirma(final Collection<SignerDTO> firmatari) {
		final List<String[]> dettaglioFirmeFormattato = formattaDatiExportVerificaFirma(firmatari, HEADERS_VERIFICA_FIRMA.length);
		
		return getWorkbookSingleSheet(ExportExcelHelper.SHEET_NAME_VERIFICA_FIRMA, dettaglioFirmeFormattato, HEADERS_VERIFICA_FIRMA);
	}
	
	
	/**
	 * @param dettaglioFirme
	 * @param lengthHeaders
	 * @return
	 */
	private List<String[]> formattaDatiExportVerificaFirma(final Collection<SignerDTO> firmatari, final int lengthHeaders) {
		final List<String[]> dettaglioFirmeFormattato = new ArrayList<>();
		final String crLf = Character.toString((char) 13) + Character.toString((char) 10);
		
		if (!CollectionUtils.isEmpty(firmatari)) {
			final List<String[]> dettaglioFirme = new ArrayList<>();
			
			for (final SignerDTO firmatario : firmatari) {
				final String[] dettaglioFirma = new String[] {
						firmatario.getDettaglioFirmatario().replace(",", crLf),
						firmatario.getTipoFirma().name(),
						firmatario.getMotivo(),
						firmatario.isValid() ? BooleanFlagEnum.SI.getDescription() : BooleanFlagEnum.NO.getDescription(),
						DateUtils.dateToString(firmatario.getDataFirma(), DateUtils.DD_MM_YYYY_HH_MM_SS),
						firmatario.getStatoCertificato().name(),
						DateUtils.dateToString(firmatario.getDataInizioValidita(), DateUtils.DD_MM_YYYY_HH_MM_SS),
						DateUtils.dateToString(firmatario.getDataFineValidita(), DateUtils.DD_MM_YYYY_HH_MM_SS),
				};
				
				dettaglioFirme.add(dettaglioFirma);
			}
			
			if (!CollectionUtils.isEmpty(dettaglioFirme)) {
				for (final String[] dettaglioFirma : dettaglioFirme) {
					final String[] rigaDocumentoExcel = new String[lengthHeaders];
					rigaDocumentoExcel[0] = dettaglioFirma[0];
					rigaDocumentoExcel[1] = dettaglioFirma[1];
					rigaDocumentoExcel[2] = dettaglioFirma[2];
					rigaDocumentoExcel[3] = dettaglioFirma[3];
					rigaDocumentoExcel[4] = dettaglioFirma[4];
					rigaDocumentoExcel[5] = dettaglioFirma[5];
					rigaDocumentoExcel[6] = dettaglioFirma[6];
					rigaDocumentoExcel[7] = dettaglioFirma[7];
					
					dettaglioFirmeFormattato.add(rigaDocumentoExcel);
				}
			}
		}
		
		
		return dettaglioFirmeFormattato;
	}
	
	/**
	 * Restituisce un workbook con un singolo foglio.
	 * 
	 * @param sheetname	Nome del foglio
	 * @param data		Dati
	 * @param headers	Campi dell'intestazione
	 * @return
	 */
	private HSSFWorkbook getWorkbookSingleSheet(final String sheetname, final List<String[]> data, final String[] headers) {
		final WorkbookStructure workbookStructure = new WorkbookStructure();
		
		final CustomSheetExcel sheet = new CustomSheetExcel(sheetname, data, headers);
		workbookStructure.getCustomSheetList().add(sheet);
		
		return getWorkbookExcel(workbookStructure);
	}
	
	
	/**
	 * Restituisce un woorkbook.
	 * 
	 * @param workbookStructure
	 * @return HSSFWorkbook
	 */
	private HSSFWorkbook getWorkbookExcel(final WorkbookStructure workbookStructure) {
		LOGGER.info("getWorkbookExcel -> START");
		final HSSFWorkbook workbook = new HSSFWorkbook();
		final HSSFCellStyle headerCellStyle = workbook.createCellStyle();
		final HSSFCellStyle dataCellStyle = workbook.createCellStyle();
		
		initWorkbook(workbook, headerCellStyle, dataCellStyle);
		
		final List<CustomSheetExcel> sheets = workbookStructure.getCustomSheetList();
		// Creazione dinamica degli sheet
		for (final CustomSheetExcel customSheetExcel : sheets) {
			final HSSFSheet sheet = workbook.createSheet(customSheetExcel.getNameSheet());
			
			int rowNum = 0;
	    	final Row row = sheet.createRow(rowNum);
	    	final String[] headers = customSheetExcel.getHeaders();
	    	
	    	// Nomi colonne (header)
	    	for (int headerCount = 0; headerCount < headers.length; headerCount++) {
	    		createCell(row, headerCellStyle, headerCount, headers[headerCount]);
	    	}
	    	
	    	final List<String[]> data = customSheetExcel.getData();
	    	
	    	// Lista risultati
	        for (int dataCount = 0; dataCount < data.size(); dataCount++) {
	        	createRowValue(sheet, dataCount, data.get(dataCount), dataCellStyle);
	        }
	        
	        for (int headerCount = 0; headerCount < headers.length; headerCount++) {
	    		sheet.autoSizeColumn(headerCount);
	        }
		}
		
		LOGGER.info("getWorkbookExcel -> END");
        return workbook;
	}
	
	
	/**
	 * @param workbook
	 * @param headerCellStyle
	 * @param dataCellStyle
	 */
	private void initWorkbook(final HSSFWorkbook workbook, final HSSFCellStyle headerCellStyle, final HSSFCellStyle dataCellStyle) {
		HSSFFont hSSFFont = workbook.createFont();
	    hSSFFont.setFontName("Arial");
	    
	    dataCellStyle.setFont(hSSFFont);
	    
	    hSSFFont = workbook.createFont();
	    hSSFFont.setFontHeightInPoints((short) 10);
	    hSSFFont.setBold(true);
	    hSSFFont.setColor(HSSFColorPredefined.BLUE.getIndex());
	    hSSFFont.setFontName("Arial");
	    
	    headerCellStyle.setFont(hSSFFont);
	    headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
	}
	
	
	/**
	 * @param sheet
	 * @param resultCount
	 * @param values
	 * @param row
	 */
	private void createRowValue(final HSSFSheet sheet, final int resultCount, final String[] values, final HSSFCellStyle cellStyle) {
		final String[] columnValue = values;
    	final Row row = sheet.createRow(resultCount + 1);
	    
        for (int celCount = 0; celCount < columnValue.length; celCount++) {
           final Cell cell = row.createCell(celCount);
           cell.setCellValue(columnValue[celCount]);
           cell.setCellStyle(cellStyle);
        }
	}
	
	
	/**
	 * @param row
	 * @param cellStyle
	 * @param headerCount
	 * @param value
	 */
	private void createCell(final Row row, final HSSFCellStyle cellStyle,  final int headerCount, final String value) {
		final Cell cell = row.createCell(headerCount);
		cell.setCellValue(value);
		cell.setCellStyle(cellStyle);
	}
	
	
	/**
	 * Restituisce il workbook con i vari fogli delle scadenze dei documenti.
	 * 
	 * @param data
	 * @return
	 */
	public final HSSFWorkbook getExportScadenzario(final List<DocumentoScadenzatoDTO> lista5,
			final List<DocumentoScadenzatoDTO> lista10,
			final List<DocumentoScadenzatoDTO> lista11,
			final List<DocumentoScadenzatoDTO> listaSc) {

		final String[] headers = new String[] {
				"Documento", 
				"Data scadenza", 
				"Data creazione", 
				"Assegnatario", 
				"Assegnazione", 
				"Coda"
		};
		
		final String sheetMinUguale5 = "Scadenza <= 5";
		final String sheetMinUguale10 = "Scadenza <= 10";
		final String sheetMagUguale11 = "Scadenza >= 11";
		final String sheetDocScaduti = "Documenti scaduti";
		
		final List<String[]> l5 = formattaDatiExportDocumentiInScadenza(lista5);
		final List<String[]> l10 = formattaDatiExportDocumentiInScadenza(lista10);
		final List<String[]> l11 = formattaDatiExportDocumentiInScadenza(lista11);
		final List<String[]> lsc = formattaDatiExportDocumentiInScadenza(listaSc);
		
		final WorkbookStructure workbookStructure = new WorkbookStructure();
		
		final CustomSheetExcel sheetMin5 = new CustomSheetExcel(sheetMinUguale5, l5, headers);
		final CustomSheetExcel sheetMin10 = new CustomSheetExcel(sheetMinUguale10, l10, headers);
		final CustomSheetExcel sheetMag11 = new CustomSheetExcel(sheetMagUguale11, l11, headers);
		final CustomSheetExcel sheetScaduti = new CustomSheetExcel(sheetDocScaduti, lsc, headers);
		
		workbookStructure.getCustomSheetList().add(sheetMin5);
		workbookStructure.getCustomSheetList().add(sheetMin10);
		workbookStructure.getCustomSheetList().add(sheetMag11);
		workbookStructure.getCustomSheetList().add(sheetScaduti);
		
		return getWorkbookExcel(workbookStructure);
	}
	
	private static List<String[]> formattaDatiExportDocumentiInScadenza(final List<DocumentoScadenzatoDTO> lista) {
		final List<String[]> fields = new ArrayList<>();
		
		for (final DocumentoScadenzatoDTO doc : lista) {
			fields.add(new String[]{
				doc.getDocumento(),
				DateUtils.dateToString(doc.getPe().getDataScadenza(), DateUtils.DD_MM_YYYY),
				DateUtils.dateToString(doc.getPe().getDataCreazione(), DateUtils.DD_MM_YYYY),
				doc.getDestinatario(),
				doc.getTipoAssegnazione(),
				doc.getPe().getQueue().getDisplayName()
			});
		}
		return fields;
	}
}