package it.ibm.red.web.document.mbean.component;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;

import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.web.mbean.component.DocumentManagerDettaglioVersioniAllegatiComponent;
import it.ibm.red.web.mbean.component.DocumentiAllegabiliAbstract;
import it.ibm.red.web.mbean.interfaces.DocumentiAllegabiliSelectableInterface;

/**
 * Component per la gestione dei Tab documenti allegabili.
 */
public class DocumentiAllegabiliSelectableTab extends DocumentiAllegabiliAbstract implements DocumentiAllegabiliSelectableInterface {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -7743319802391510503L;
	
	/**
	 * Verifica che i documenti selezionati siano stati effettivamente modificati.
	 */
	private boolean selectedChange;

	/**
	 * Qualora venga modificati i documenti selezionati si modifica anche la dimensione totale dei file selezioanti.
	 */
	private BigDecimal dimensioneSelected;
	
	/**
	 * Utente.
	 */
	private final UtenteDTO utente;
	
	/**
	 * Allegato.
	 */
	private final DocumentManagerDettaglioVersioniAllegatiComponent allegato;

	protected DocumentiAllegabiliSelectableTab(final FascicoloDTO fascicoloProcedimentale, final List<DocumentoAllegabileDTO> documentiAllegabili, final UtenteDTO utente) {
		super(fascicoloProcedimentale, documentiAllegabili, BigDecimal.ZERO);

		this.utente = utente;
		allegato = new DocumentManagerDettaglioVersioniAllegatiComponent(utente);
		
		dimensioneSelected = BigDecimal.ZERO;
		// Potrebbero essere già presenti degli allegati tra quelli disponibili.
		if (CollectionUtils.isNotEmpty(getDocumentiAllegabili())) {
			final List<DocumentoAllegabileDTO> alreadyselected = getDocumentiAllegabili().stream()
					.filter(doc -> doc.isSelected())
					.collect(Collectors.toList());
			setSelectedAllegabili(alreadyselected);
		}
	}

	/**
	 * Restituisce il component per la gestione degli allegati.
	 */
	@Override
	public DocumentManagerDettaglioVersioniAllegatiComponent getAllegato() {
		return allegato;
	}
	
	/**
	 * Totale delle dimensioni dei documenti selezionati.
	 * Questa implementazione gestisce una cache per evitare di ricalcolare ogni volta la dimensione.
	 */
	@Override
	public BigDecimal getDimensioneSelected() {
		if (selectedChange) {
			dimensioneSelected = super.getDimensioneSelected();
			selectedChange = false;
		}
		return dimensioneSelected;
	}

	/**
	 * @see it.ibm.red.web.mbean.component.DocumentiAllegabiliAbstract#setSelectedAllegabili(java.util.List).
	 */
	@Override
	public void setSelectedAllegabili(final List<DocumentoAllegabileDTO> selectedAllegabili) {
		super.setSelectedAllegabili(selectedAllegabili);
		selectedChange = true;
	}

	/**
	 * Verifica che l'utente ha modificato gli allegati selezionati.
	 */
	public boolean isSelectedChange() {
		return selectedChange;
	}

	/**
	 * @see it.ibm.red.web.mbean.interfaces.DocumentiAllegabiliSelectableInterface#aggiornaVerificaFirmaAllegato(java.lang.Object).
	 */
	@Override
	public void aggiornaVerificaFirmaAllegato(final Object index) {
		final DocumentoAllegabileDTO selected = (DocumentoAllegabileDTO) index;  
		if (selected != null) {
			allegato.aggiornaVerificaFirma(selected.getGuid(), selected.getDocumentTitle(), utente);
			if (allegato.getInfoFirma().getMetadatoValiditaFirma() != null) {
				selected.setValoreVerificaFirma(allegato.getInfoFirma().getMetadatoValiditaFirma());
			} 
		}  
	}
	 
}