package it.ibm.red.web.mbean.component;


import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.document.mbean.component.AbstractComponent;
import it.ibm.red.web.mbean.interfaces.IDetailPreviewComponent;
import it.ibm.red.web.servlets.DownloadContentServlet;
import it.ibm.red.web.servlets.DownloadContentServlet.DocumentTypeEnum;

/**
 * Gestisce il dettaglio preview specifico del documentManager.
 * 
 * visto che la logica del documentMangager è piuttosto complicata ho voluto disaccoppiare la gestione della preview.
 * 
 * @author a.difolca
 *
 */
public class DetailPreviewDocumentManagerComponent extends AbstractComponent implements IDetailPreviewComponent {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -6135057629385669713L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DetailPreviewDocumentManagerComponent.class);
	
	/**
	 * Informazioi utente.
	 */
	private final UtenteDTO utente;
	
	/**
	 * Info criptate.
	 */
	private String cryptoId = "";

	/**
	 * Costruttore del component.
	 * @param inUtente
	 */
	public DetailPreviewDocumentManagerComponent(final UtenteDTO inUtente) {
		this.utente = inUtente;
	}

	/**
	 * Imposta il detail della preview aggiornando il cryptoId.
	 * @param detail
	 */
	public void setDetailPreview(final DetailDocumentRedDTO detail) {
		String nomeFile = null;
		String mimeType = null;
		String documentTitle = "";
		DocumentTypeEnum documentType = DocumentTypeEnum.NONE;
		
		if (StringUtils.isNotBlank(detail.getIdFilePrincipaleDaProtocolla())) {
			nomeFile = detail.getNomeFile();
			mimeType = detail.getMimeType();
			documentTitle = detail.getIdFilePrincipaleDaProtocolla();
			documentType = DocumentTypeEnum.PROTOCOLLA_MAIL;
		} else if (detail.getDocumentTitle() != null) {
			documentTitle =  detail.getDocumentTitle();
		}
		
		this.cryptoId = StringUtils.isBlank(documentTitle) && (documentType == null || DocumentTypeEnum.NONE == documentType) ? "" 
				: getCryptoDocParamById(documentTitle, documentType, nomeFile, mimeType);
	}
	
	
	/**
	 * Recupera i parametri criptati utilizzati per la Servlet 'DownloadContentServlet'.
	 * {@link DownloadContentServlet}
	 * @param id - Identificativo del documento
	 * @return Parametri cryptati.
	 */
	private String getCryptoDocParamById(final String id, final DocumentTypeEnum documentType, final String nomeFile, final String mimeType) {
		try {
			return DownloadContentServlet.getCryptoDocParamById(id, documentType, utente, nomeFile, mimeType);
		} catch (final Exception e) {
			LOGGER.error("Errore nell'encrypt dei parametri per la servlet 'DownloadContentServlet':", e);
			showErrorMessage("Errore nell'encrypt dei parametri per la servlet 'DownloadContentServlet': " + e.getMessage());
		}
		return "ERROREDICODIFICA";
	}
	
	/**
	 * Restituisce il crypto id.
	 * @return crypto id
	 */
	@Override
	public String getCryptoId() {
		return this.cryptoId;
	}

	/**
	 * Resetta il cryptoId.
	 */
	public void unsetDetail() {
		this.cryptoId = "";
	}
	
}
