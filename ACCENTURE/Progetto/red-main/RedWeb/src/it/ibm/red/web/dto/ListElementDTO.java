package it.ibm.red.web.dto;

import java.util.List;

/**
 * Le liste talvolta necessitano di un indice che ne individua gli elementi in
 * maniera univoca. Questo oggetto wrappa il dato e fornisce l'indice
 * permettendo anche in caso di modifiche della lista di eliminare esattamente
 * lo stesso elemento selezionato dall'utente.
 * 
 * Serve anche a disaccoppiare l'oggetto dato dall'elemento nella lista. Qualora
 * E modifichi il comportamento dei suoi equals non andrà ad impattare la logica
 * del recupero dei nodi
 * 
 * @author a.difolca
 *
 * @param <E>
 *            Oggetto contenuto nell'elemento della lista
 */
public class ListElementDTO<E> extends AbstractDTO {

	private static final long serialVersionUID = 6427265907294810190L;

	/**
	 * Must be unique
	 */
	private int id;

	/**
	 * Il dato contenuto in questo oggetto
	 */
	private transient E data;

	/**
	 * if true data is a placeholder or is null
	 */
	private boolean empty;

	/**
	 * Costruttore di default.
	 * @param id
	 * @param data
	 */
	public ListElementDTO(final int id, final E data) {
		this.id = id;
		setData(data);
	}

	/**
	 * Garantisce l'unicità dell'id all'interno della lista.
	 * 
	 * @param placeHolder dato da utilizzare come placeHolder
	 * @param array
	 */
	public ListElementDTO(final E placeHolder, final List<ListElementDTO<E>> array) {
		if (array == null) {
			throw new IllegalArgumentException("Impossibile individuare un id univoco se l'array è nullo");
		}
		setPlaceHolder(placeHolder);
		this.id = 0;		
		for (final ListElementDTO<E> elem:array) {
			if (elem.getId() >= this.id) {
				this.id = elem.getId();
				this.id++;
			}
		}

		array.add(this);
	}

	/**
	 * @return data
	 */
	public E getData() {
		return data;
	}

	/**
	 * Imposta il valore del dato.
	 * @param data
	 */
	public void setData(final E data) {
		this.data = data;
		this.empty = false;
	}

	/**
	 * Imposta il placceholder.
	 * @param placeHolder
	 */
	public void setPlaceHolder(final E placeHolder) {
		this.data = placeHolder;
		this.empty = true;
	}

	/**
	 * Restituisce l'id dell'elemento.
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Imposta l'id dell'elemento.
	 * @param id
	 */
	public void setId(final int id) {
		this.id = id;
	}

	/**
	 * Se true il metodo getData ritorna un placeholder che non è di interesse a
	 * scopi funzionali.
	 * 
	 * @return true, se il dato è null; false, altrimenti.
	 */
	public boolean isPlaceHolder() {
		return data == null || empty;
	}

	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ListElementDTO<E> other = (ListElementDTO<E>) obj;
		return (id == other.id);
	}
}
