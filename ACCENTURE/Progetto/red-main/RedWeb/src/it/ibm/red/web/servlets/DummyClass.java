package it.ibm.red.web.servlets;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;

import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.CountDTO;
import it.ibm.red.business.dto.CountFepaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.FunzionalitaEnum;
import it.ibm.red.business.enums.StatoMailGUIEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.ICasellePostaliSRV;
import it.ibm.red.business.service.facade.ICounterFacadeSRV;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;

public class DummyClass {

	/**
	 * JS per visualizzazione della statusDialog.
	 */
	private static final String PF_STATUS_DIALOG_SHOW = "PF('statusDialog').show();";

	/**
	 * Command "go to mail".
	 */
	private static final String SESSION_BEAN_GOTO_MAIL = "#{SessionBean.gotoMail}";
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DummyClass.class.getName());

	/**
	 * Servizio.
	 */
	private ICounterFacadeSRV countSRV;
	
	/**
	 * Servizio.
	 */
	private IUtenteFacadeSRV usrFacade;

	/**
	 * Servizio.
	 */
	private ICasellePostaliSRV casellePostaliSRV;

	/**
	 * Menù caselle postali.
	 */
	private DefaultMenuModel menuCasellePosta;

	/**
	 * Lista caselle postali.
	 */
	private List<CasellaPostaDTO> cp;

	/**
	 * Informazioni utente.
	 */
	private UtenteDTO utente;

	/**
	 * In arrivo.
	 */
	private static final String IN_ARRIVO = "In Arrivo";	

	/**
	 * Rifiutate.
	 */
	private static final String RIFIUTATA_INOLTRATA = "Rifiutata/Inoltrata";	

	/**
	 * Eliminate.
	 */
	private static final String ELIMINATA = "Eliminata";

	/**
	 * @param dumpFlag
	 */
	public void ricaricaMenuContatori(final Boolean dumpFlag) {
		usrFacade = ApplicationContextProvider.getApplicationContext().getBean(IUtenteFacadeSRV.class);
		utente = usrFacade.getByUsername("BIAGIO.MAZZOTTA");
		
	}

	private static void dumpTime(final Boolean dumpFlag, final String uid, final Integer nGate, final Long start, final Long stop) {
		if (dumpFlag != null && dumpFlag) {
			LOGGER.info(uid + " GATE" + nGate + " " + (stop - start));
		}
	}

	private static Long getTimeMS() {
		return new Date().getTime();
	}

	private void countMenuFepa() {
		final Map<DocumentQueueEnum, Integer> codaCountFepa = countSRV.getCounterMenuFepa(utente);
		final CountFepaDTO countFepa = new CountFepaDTO(codaCountFepa);
		LOGGER.info(" countFepa " + countFepa);
	}
	
	private void countMenuAttivita() {
		final Map<Integer, Integer> codaCount = countSRV.getCounterMenuAttivita(utente);
		final CountDTO count = new CountDTO(codaCount);
		LOGGER.info(" count " + count);
	}
	
	private Map<String, Integer> countMails(final String nomeCasella) {
		return countSRV.getCounterMenuMail(nomeCasella, utente.getFcDTO(), utente.getIdAoo());
	}
	
	private void createSubMenuMail() {
		this.menuCasellePosta = new DefaultMenuModel();
		DefaultSubMenu caselle = null;
		cp = casellePostaliSRV.getCasellePostali(utente, FunzionalitaEnum.MAIL_LEAF);
		if (cp != null && !cp.isEmpty()) {
			for (final CasellaPostaDTO casellaDTO : cp) {
				final Map<String, Integer> statoCount = countMails(casellaDTO.getIndirizzoEmail());
				final Iterator<Integer> i = statoCount.values().iterator();
				final Integer tot = i.next() + i.next() + i.next();
				caselle = new DefaultSubMenu(casellaDTO.getIndirizzoEmail() + " [" + tot + "]");
				attachMenuItem(caselle, statoCount, casellaDTO.getIndirizzoEmail());
				this.menuCasellePosta.addElement(caselle);
			}
		}
		this.menuCasellePosta.generateUniqueIds();
	}
	
	private void attachMenuItem(final DefaultSubMenu caselle, final Map<String, Integer> statoCount, final String indirizzoMail) {
		
		DefaultMenuItem menuItem = null;
		
		String paramKey = "";
		List<String> paramValue = new ArrayList<>();
		Map<String, List<String>> params = new HashMap<>();
		menuItem = new DefaultMenuItem(IN_ARRIVO + " [" + statoCount.get(StatoMailGUIEnum.INARRIVO.getLabel()) + "]");
		paramKey = StatoMailGUIEnum.INARRIVO.getLabel();
		paramValue.add(indirizzoMail);
		paramValue.add(StatoMailGUIEnum.INARRIVO.getValue().toString());
		params.put(paramKey, paramValue);
		menuItem.setParams(params);
		menuItem.setId(indirizzoMail + "-" + StatoMailGUIEnum.INARRIVO.getValue().toString());
		menuItem.setCommand(SESSION_BEAN_GOTO_MAIL);
		menuItem.setOnstart(PF_STATUS_DIALOG_SHOW);
		menuItem.setOncomplete(PF_STATUS_DIALOG_SHOW);
		menuItem.setIcon("fa fa-inbox");
		caselle.addElement(menuItem);
		
		paramValue = new ArrayList<>();
		params = new HashMap<>();
		menuItem = new DefaultMenuItem(RIFIUTATA_INOLTRATA + " [" + statoCount.get(StatoMailGUIEnum.INOLTRATA.getLabel()) + "]");
		paramKey = StatoMailGUIEnum.INOLTRATA.getLabel();
		paramValue.add(indirizzoMail);
		paramValue.add(StatoMailGUIEnum.INOLTRATA.getValue().toString());
		params.put(paramKey, paramValue);
		menuItem.setParams(params);
		menuItem.setId(indirizzoMail + "-" + StatoMailGUIEnum.INOLTRATA.getValue().toString());
		menuItem.setCommand(SESSION_BEAN_GOTO_MAIL);
		menuItem.setOnstart(PF_STATUS_DIALOG_SHOW);
		menuItem.setOncomplete(PF_STATUS_DIALOG_SHOW);
		menuItem.setIcon("fa fa-mail-forward");
		caselle.addElement(menuItem);

		paramValue = new ArrayList<>();
		params = new HashMap<>();
		menuItem = new DefaultMenuItem(ELIMINATA + " [" + statoCount.get(StatoMailGUIEnum.ELIMINATA.getLabel()) + "]");
		paramKey = StatoMailGUIEnum.ELIMINATA.getLabel();
		paramValue.add(indirizzoMail);
		paramValue.add(StatoMailGUIEnum.ELIMINATA.getValue().toString());
		params.put(paramKey, paramValue);
		menuItem.setParams(params);
		menuItem.setId(indirizzoMail + "-" + StatoMailGUIEnum.ELIMINATA.getValue().toString());
		menuItem.setCommand(SESSION_BEAN_GOTO_MAIL);
		menuItem.setOnstart(PF_STATUS_DIALOG_SHOW);
		menuItem.setOncomplete(PF_STATUS_DIALOG_SHOW);
		menuItem.setIcon("fa fa-trash");
		caselle.addElement(menuItem);
	}
	
}
