package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IRiattivaProcedimentoFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * Bean che gestisce la response: Riattiva Procedimento.
 */
@Named(ConstantsWeb.MBean.RIATTIVA_PROCEDIMENTO_BEAN)
@ViewScoped
public class RiattivaProcedimentoBean extends AbstractBean implements InitHumanTaskInterface {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -8335012510093925837L;

	/**
	 * Messaggio riattivazione.
	 */
	private String messaggioRiattivazione;

	/**
	 * Lista documenti.
	 */
	private List<MasterDocumentRedDTO> masters;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RiattivaProcedimentoBean.class.getName());

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		masters = inDocsSelezionati;
	}

	/**
	 * Post construct del bean.
	 */
	@Override
	protected void postConstruct() {
		// Metodo intenzionalmente vuoto.
	}
	
	/**
	 * Esegue il processo di riattivamento del procedimento, memorizza l'esito in modo tale da essere visualizzato a valle delle operazioni.
	 */
	public void riattivaProcedimentoResponse() {
		final Collection<EsitoOperazioneDTO> eOpe =  new ArrayList<>();
		try {
			final IRiattivaProcedimentoFacadeSRV opeRiattivaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRiattivaProcedimentoFacadeSRV.class);
			final ListaDocumentiBean ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
			final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
			final UtenteDTO u = sb.getUtente();
			
			//pulisco gli esiti
			ldb.cleanEsiti();
			
			final String titoloDocumento = masters.get(0).getDocumentTitle();
			
			//invoke service
			eOpe.add(opeRiattivaSRV.riattiva(titoloDocumento, u, messaggioRiattivazione));
			
			//setto gli esiti con i nuovi ed eseguo un refresh
			ldb.getEsitiOperazione().addAll(eOpe);
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.RIATTIVA_PROCEDIMENTO_BEAN);
			
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la riattivazione del procedimento", e);
			showError("Si è verificato un errore durante la riattivazione del procedimento");
		}
	}
	
	/**
	 * Restituisce il messaggio di riattivazione.
	 * @return messaggio riattivazione
	 */
	public String getMessaggioRiattivazione() {
		return messaggioRiattivazione;
	}
	
	/**
	 * Imposta il messaggio di riattivazione.
	 * @param messaggioRiattivazione
	 */
	public void setMessaggioRiattivazione(final String messaggioRiattivazione) {
		this.messaggioRiattivazione = messaggioRiattivazione;
	}


}
