package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.NodeExpandEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.enums.ColoreNotaEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipologiaNodoEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractAssegnazioniBean;

/**
 * @author APerquoti
 *
 */
@Named(ConstantsWeb.MBean.STORNA_BEAN)
@ViewScoped
public class StornaBean extends AbstractAssegnazioniBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 3729333260515188624L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(StornaBean.class.getName());

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		init();
	}

	/**
	 * Esegue una validazione dei parametri, se supera la validazione esegue
	 * {@link #stornaResponse()}.
	 */
	public void checkEResponse() {
		if (permessoContinua()) {
			stornaResponse();
		} else {
			showWarnMessage("Attenzione: il campo Nuova Assegnazione è obbligatorio");
		}
	}

	/**
	 * Esegue la response di Storno, gestendo eventuali errori e aggiornando l'esito
	 * per una successiva visualizzazione da parte dell'utente che ha sollecitato la
	 * response.
	 */
	private void stornaResponse() {
		Collection<EsitoOperazioneDTO> eOpe = new ArrayList<>();
		final Collection<String> wobNumbers = new ArrayList<>();
		try {

			// pulisco gli esiti
			getLdb().cleanEsiti();

			// raccolgo i wobnumber necessari ad invocare il service
			for (final MasterDocumentRedDTO m : getMasters()) {
				if (!StringUtils.isNullOrEmpty(m.getWobNumber())) {
					wobNumbers.add(m.getWobNumber());
				}
			}

			Long nuovoUtente = getSelected().getIdUtente();
			// eseguo un controllo perche nel caso della selezione di un ufficio il sistema
			// si aspetta un idUtente a '0'
			if (getSelected().getIdUtente() == null) {
				nuovoUtente = 0L;
			}

			// invoke service
			eOpe = getRiassegnaSRV().riassegna(getUtente(), wobNumbers, ResponsesRedEnum.STORNA, getSelected().getIdNodo(), nuovoUtente, getMotivoAssegnazione());

			// setto gli esiti con i nuovi e eseguo un refresh
			getLdb().getEsitiOperazione().addAll(eOpe);

			if ((!getLdb().getEsitiOperazione().isEmpty() && getLdb().checkEsiti(false)) && isAlsoStorico()) {
				for (final MasterDocumentRedDTO m : getMasters()) {
					getNotaSRV().registraNotaFromDocumento(getUtente(), Integer.parseInt(m.getDocumentTitle()), ColoreNotaEnum.NERO, getMotivoAssegnazione());
				}
			}

			FacesHelper.executeJS("PF('dlgGeneric').hide()");
			FacesHelper.executeJS("PF('dlgShowResult').show()");
			FacesHelper.update("centralSectionForm:idDlgShowResult");

			getLdb().destroyBeanViewScope(ConstantsWeb.MBean.STORNA_BEAN);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il tentato sollecito della response 'STORNA'.", e);
			showError("Si è verificato un errore durante l'esecuzione dell'operazione 'STORNA'");
		}
	}
	
	/**
	 * Carica l'organigramma.
	 */
	@Override
	protected void loadRootOrganigramma() {
		try {
			final List<Long> alberatura = new ArrayList<>();
			for (int i = alberaturaNodi.size() - 1; i >= 0; i--) {
				alberatura.add(alberaturaNodi.get(i).getIdNodo());
			}

			// creazione della struttura del tree
			setRoot(new DefaultTreeNode("Root", null));

			// <-------- Gestione Nodo Radice ------------>
			// imposto un DTO utile per creare la struttura del tree
			final NodoOrganigrammaDTO padre = new NodoOrganigrammaDTO();
			padre.setIdAOO(getUtente().getIdAoo());
			padre.setIdNodo(getUtente().getIdNodoRadiceAOO());
			padre.setCodiceAOO(getUtente().getCodiceAoo());
			padre.setUtentiVisible(false);

			final List<NodoOrganigrammaDTO> listRoot = getOrgSRV().getFigliAlberoAssegnatarioPerConoscenza(getUtente().getIdUfficio(), padre, false);
			boolean deleteUfficiFigli = getUtente().getIdUfficio().longValue() == padre.getIdNodo().longValue();
			final List<TreeNode> listToOpen = new ArrayList<>();
			TreeNode nodoradice = null;
			for (final NodoOrganigrammaDTO pl : listRoot) {
				if(pl.getIdUtente()==null) {
					pl.setCodiceAOO(getUtente().getCodiceAoo());
					nodoradice = new DefaultTreeNode(pl, getRoot());
					nodoradice.setExpanded(true);
					nodoradice.setType("UFF");
					listToOpen.add(nodoradice);
				}
			}
			// <-------- Fine Gestione ------------>

			// <-------- Gestione Primo Livello ------------>
			List<TreeNode> nodiDaAprire = new ArrayList<>();
			NodoOrganigrammaDTO nodoPadre = null;
			List<NodoOrganigrammaDTO> figli;
			for (final TreeNode lto : listToOpen) {
				nodoPadre = (NodoOrganigrammaDTO) lto.getData();
				if (nodoPadre.getFigli() == 0 && !nodoPadre.getIdNodo().equals( getUtente().getIdUfficio()) ) {
					continue;
				}

				if (deleteUfficiFigli && nodoPadre.getTipoNodo().equals(TipologiaNodoEnum.UFFICIO)) {
					continue;
				}

				figli = getOrgSRV().getFigliAlberoAssegnatarioPerConoscenza(getUtente().getIdUfficio(), nodoPadre, false);
				deleteUfficiFigli = deleteUfficiFigli || getUtente().getIdUfficio().longValue() == nodoPadre.getIdNodo().longValue();
				TreeNode nodoFiglio = null;
				for (final NodoOrganigrammaDTO figlio : figli) {

					if (deleteUfficiFigli && figlio.getTipoNodo().equals(TipologiaNodoEnum.UFFICIO)) {
						continue;
					}

					figlio.setCodiceAOO(getUtente().getCodiceAoo());
					nodoFiglio = new DefaultTreeNode(figlio, lto);
					nodoFiglio.setType("UFF");
					if (figlio.getTipoNodo().equals(TipologiaNodoEnum.UTENTE)) {
						nodoFiglio.setType("USER");
					}

					if (figlio.getFigli() > 0 || figlio.isUtentiVisible()) {
						new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoFiglio);
					}

					if (figlio.getTipoNodo() == TipologiaNodoEnum.UFFICIO && alberatura.contains(figlio.getIdNodo())) {
						nodiDaAprire.add(nodoFiglio);
					}
				}

			}
			// <-------- Fine Gestione ------------>

			final List<TreeNode> nodiDaAprireTemp = new ArrayList<>();
			for (int i = 0; i < alberatura.size(); i++) {
				// devo fare esattamente un numero di cicli uguale alla lunghezza
				// dell'alberatura
				nodiDaAprireTemp.clear();
				for (final TreeNode nodo : nodiDaAprire) {
					onOpenNode(nodo);
					nodo.setExpanded(true);
					for (final TreeNode nodoFiglio : nodo.getChildren()) {
						final NodoOrganigrammaDTO nodoFiglioDto = (NodoOrganigrammaDTO) nodoFiglio.getData();
						if (nodoFiglioDto.getIdUtente() == null && alberatura.contains(nodoFiglioDto.getIdNodo())) {
							nodiDaAprireTemp.add(nodoFiglio);
						}
					}
				}
				nodiDaAprire = new ArrayList<>(nodiDaAprireTemp);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Gestisce l'evento "Expand" del nodo, {@link #onOpenNode(NodeExpandEvent)}.
	 * 
	 * @param event
	 */
	public void onOpenNode(final NodeExpandEvent event) {
		onOpenNode(event.getTreeNode());
	}

	/**
	 * Gestisce l'evento "Expand" del nodo.
	 * 
	 * @param tree
	 */
	public void onOpenNode(final TreeNode tree) {
		try {
			final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) tree.getData();
			final List<NodoOrganigrammaDTO> figli = getOrgSRV().getFigliAlberoAssegnatarioPerConoscenza(getUtente().getIdUfficio(), nodoExp, false);
			final boolean deleteUfficiFigli = getUtente().getIdUfficio().longValue() == nodoExp.getIdNodo().longValue();
			DefaultTreeNode nodoToAdd = null;
			tree.getChildren().clear();
			for (final NodoOrganigrammaDTO n : figli) {

				if (deleteUfficiFigli && n.getTipoNodo().equals(TipologiaNodoEnum.UFFICIO)) {
					continue;
				}

				n.setCodiceAOO(getUtente().getCodiceAoo());

				nodoToAdd = new DefaultTreeNode(n, tree);
				nodoToAdd.setType("UFF");
				if (n.getTipoNodo().equals(TipologiaNodoEnum.UTENTE)) {
					nodoToAdd.setType("USER");
				}
				if (n.getFigli() > 0 || n.isUtentiVisible()) {
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}
}