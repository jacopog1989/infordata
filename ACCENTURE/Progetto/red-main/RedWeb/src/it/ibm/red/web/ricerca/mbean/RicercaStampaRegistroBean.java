/**
 * 
 */
package it.ibm.red.web.ricerca.mbean;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.Visibility;

import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.RegistroProtocolloDTO;
import it.ibm.red.business.dto.StatoRicercaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.IPropertiesFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.SessionObject;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.helper.faces.MessageSeverityEnum;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.nps.mbean.DettaglioStampaRegistroBean;

/**
 * @author APerquoti
 *
 */
@Named(ConstantsWeb.MBean.RICERCA_STAMPA_REGISTRO_BEAN)
@ViewScoped
public class RicercaStampaRegistroBean extends AbstractBean {

	private static final long serialVersionUID = 5995735581406985967L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaStampaRegistroBean.class.getName());

	/**
	 * Servizio.
	 */
	private IDocumentoFacadeSRV documentoSRV;

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Bean sessione.
	 */
	private SessionBean sessionBean;

	/**
	 * Datatable helper registri.
	 */
	protected SimpleDetailDataTableHelper<RegistroProtocolloDTO> registriDTH;

	/**
	 * Bean dettaglio.
	 */
	private DettaglioStampaRegistroBean detailBean;

	/**
	 * Parametri ricerca.
	 */
	private ParamsRicercaAvanzataDocDTO formRicerca;

	/**
	 * Colonne.
	 */
	private List<Boolean> regColumns;

	/**
	 * Numero massimo risultati.
	 */
	private Integer numMaxRisultati;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();

		registriDTH = new SimpleDetailDataTableHelper<>();
		registriDTH.setMasters(new ArrayList<>());

		detailBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_STAMPA_REGISTRO_BEAN);

		regColumns = Arrays.asList(true, true, true, true, true);

		final IPropertiesFacadeSRV propertiesSRV = ApplicationContextProvider.getApplicationContext().getBean(IPropertiesFacadeSRV.class);
		numMaxRisultati = Integer.parseInt(propertiesSRV.getByEnum(PropertiesNameEnum.RICERCA_MAX_RESULTS));

		initPaginaRicerca();
	}

	/**
	 * Inizializza la pagina di ricerca.
	 */
	@SuppressWarnings("unchecked")
	public void initPaginaRicerca() {
		try {
			final StatoRicercaDTO stDto = (StatoRicercaDTO) FacesHelper.getObjectFromSession(SessionObject.STATO_RICERCA, true);
			if (stDto != null) {
				final Collection<RegistroProtocolloDTO> searchResult = (Collection<RegistroProtocolloDTO>) stDto.getRisultatiRicerca();
				formRicerca = (ParamsRicercaAvanzataDocDTO) stDto.getFormRicerca();

				registriDTH.setMasters(searchResult);
				registriDTH.selectFirst(true);

				selectRegistroDetail(registriDTH.getCurrentMaster());

				if (numMaxRisultati != null && searchResult != null && numMaxRisultati.equals(searchResult.size())) {
					showWarnMessage(RicercaAbstractBean.MSG_NUM_MAX_RISULTATI);
				}
			} else {
				formRicerca = new ParamsRicercaAvanzataDocDTO();
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Non è stato possibile recuperare i risultati di ricerca");
		}
	}

	/**
	 * Gestisce la selezione della riga associata al datatable dei registri.
	 * 
	 * @param se
	 */
	public final void rowSelectorRegistri(final SelectEvent se) {
		registriDTH.rowSelector(se);
		selectRegistroDetail(registriDTH.getCurrentMaster());
	}

	private void selectRegistroDetail(final RegistroProtocolloDTO f) {
		detailBean.setDetail(f);
	}

	/**
	 * Consente il download del registro, restituisce uno StreamedContent.
	 * 
	 * @param registro
	 * @return StreamedContent per download
	 */
	public StreamedContent scaricaRegistro(final RegistroProtocolloDTO registro) {
		StreamedContent sc = null;

		try {
			// EMERGENZANPS
			if (TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo())) {
				FacesHelper.showMessage("Regime di emergenza", "Non è possibile recuperare il registro dei protocolli in regime di emergenza.", MessageSeverityEnum.WARN);
			} else if (TipologiaProtocollazioneEnum.NPS.getId().equals(utente.getIdTipoProtocollo())) {
				// NPS
				sc = new DefaultStreamedContent(new ByteArrayInputStream(registro.getContent()), registro.getContentType(), registro.getNomeFile());
				// PMEF
			} else {
				final FileDTO file = documentoSRV.getRegistroProtocolloContentPreview(utente.getFcDTO(), registro.getDocumentTitle(), utente.getIdAoo());
				final InputStream io = new ByteArrayInputStream(file.getContent());
				sc = new DefaultStreamedContent(io, file.getMimeType(), file.getFileName());
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero del registro dei protocolli richiesto.", e);
			FacesHelper.showMessage(null, "Si è verificato un errore durante il recupero del registro dei protocolli richiesto.", MessageSeverityEnum.ERROR);
		}

		return sc;
	}

	/**
	 * Metodo per reimpostare nel session bean il form utilizzato per la ricerca e
	 * quindi visualizzarlo nel dialog
	 */
	public void ripetiRicercaStampaRegistro() {
		try {
			// Viene ripetuta la ricerca con i stessi filtri
			sessionBean.getRicercaFormContainer().setRegistroProtocollo(formRicerca);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Gestisce l'evento "Toggle" delle colonne dei registri.
	 * 
	 * @param event
	 */
	public final void onColumnToggleReg(final ToggleEvent event) {
		regColumns.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
	}

//	<-- get/set -->
	/**
	 * @return the registriDTH
	 */
	public final SimpleDetailDataTableHelper<RegistroProtocolloDTO> getRegistriDTH() {
		return registriDTH;
	}

	/**
	 * @param registriDTH the registriDTH to set
	 */
	public final void setRegistriDTH(final SimpleDetailDataTableHelper<RegistroProtocolloDTO> registriDTH) {
		this.registriDTH = registriDTH;
	}

	/**
	 * @return the regColumns
	 */
	public final List<Boolean> getRegColumns() {
		return regColumns;
	}

	/**
	 * @param regColumns the regColumns to set
	 */
	public final void setRegColumns(final List<Boolean> regColumns) {
		this.regColumns = regColumns;
	}

	/**
	 * Restituisce il nome del file per l'export della ricerca.
	 * 
	 * @return nome file export
	 */
	public String getNomeFileExportCoda() {

		String nome = "";
		if (sessionBean.getUtente() != null) {
			nome += sessionBean.getUtente().getNome().toLowerCase() + "_" + sessionBean.getUtente().getCognome().toLowerCase() + "_";
		}

		nome += "ricercaregistroprotocollo_";

		final SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH.mm");
		nome += sdf.format(new Date());

		return nome;
	}
}
