package it.ibm.red.web.document.mbean.component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.formula.eval.NotImplementedException;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IFaldoneFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.dto.TreeNodeFaldoneDTO;
import it.ibm.red.web.enums.TreeNodeTypeEnum;
import it.ibm.red.web.mbean.AbstractTreeBean;

/**
 * Component dialog faldona.
 */
public class DialogFaldonaComponent extends AbstractTreeBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -2909450409458882020L;

	/**
	 * Label da apporre prima del fascicolo.
	 */
	private static final String FASCICOLO_LITERAL = "Il fascicolo [";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DialogFaldonaComponent.class);

	/**
	 * Descrzione faldone fittizio.
	 */
	private static final String DESCRIZIONE_FALDONE_FITTIZIO = "Faldoni Inconsistenti";

	/**
	 * Service.
	 */
	private final IFaldoneFacadeSRV faldoneSRV;

	/**
	 * Viene utilizzato quando si faldona un solo fascicolo, ad esempio il fascicolo
	 * procedimentale.
	 */
	private FascicoloDTO fascicolo;

	/**
	 * Viene utilizzata quando si vogliono fare operazioni massive.
	 */
	private List<FascicoloDTO> listaFascicoli;

	/**
	 * Root.
	 */
	private DefaultTreeNode root;

	/**
	 * Tab attivo.
	 */
	private int activeTabIndex;

	/**
	 * Tiopologia creazione.
	 */
	private String tipoCreazione;

	/**
	 * Testo ricerca.
	 */
	private String testoRicerca;

	/**
	 * Trre nodi.
	 */
	private transient Map<String, TreeNode> nodiNelTree;

	/**
	 * Oggetto faldone selezionato.
	 */
	private String oggettoFaldoneSelezionato;

	/**
	 * Descrizione faldone selezionato.
	 */
	private String descrizioneFaldoneSelezionato;

	/**
	 * Oggetto nuovo faldone.
	 */
	private String oggettoNuovoFaldone;

	/**
	 * Descrizione nuovo fladone.
	 */
	private String descrizioneNuovoFaldone;

	/**
	 * Nome faldfone creato.
	 */
	private String nomeFaldoneCreato;

	/**
	 * Faldone padre.
	 */
	private FaldoneDTO faldonePadre;

	/**
	 * Nome bean.
	 */
	private String masterBeanName;

	/**
	 * Lista faldoni trovati.
	 */
	private Collection<FaldoneDTO> faldoniTrovati;

	/**
	 * Flag ammetti dupliucazioni.
	 */
	private Boolean canBeDuplicate;

	/**
	 * 0.
	 */
	private static final String CREA_NUOVO_FALDONE = "0";

	/**
	 * 1.
	 */
	private static final String CREA_NUOVO_SOTTOFALDONE = "1";

	/**
	 * indexTab "inserisci in nuovo faldone".
	 */
	private static final int NUOVO_TAB = 1;

	/**
	 * Lista faldoni.
	 */
	private Collection<FaldoneDTO> faldoni;

	/**
	 * Flag faldoni associati.
	 */
	private boolean esistonoFaldoniAssociati = false;

	/**
	 * Lista faldoni selezionati.
	 */
	private List<FaldoneDTO> faldoniScelti;

	/**
	 * Flag tutti i faldoni.
	 */
	private boolean selezionaTuttiFaldoni;

	/**
	 * Costruttore del component.
	 * 
	 * @param utente
	 * @param chiamante
	 */
	public DialogFaldonaComponent(final UtenteDTO utente) {
		this.utente = utente;

		faldoneSRV = ApplicationContextProvider.getApplicationContext().getBean(IFaldoneFacadeSRV.class);
		tipoCreazione = CREA_NUOVO_FALDONE;
		nodiNelTree = new HashMap<>();
		faldoniScelti = new ArrayList<>();
	}

	/**
	 * Restituisce l'insieme di faldoni recuperati dalla ricerca.
	 * 
	 * @return collezione di faldoni
	 */
	public Collection<FaldoneDTO> cercaFaldoni() {
		faldoniTrovati = new ArrayList<>();

		try {
			final Collection<FaldoneDTO> faldoniColl = faldoneSRV.ricercaFaldoni(testoRicerca, RicercaGenericaTypeEnum.ESATTA, utente);
			faldoniTrovati = faldoneSRV.getGerarchia(faldoniColl, utente);
		} catch (final Exception e) {
			showError("Errore durante la ricerca dei faldoni:\n" + e);
			LOGGER.error(e.getMessage(), e);
		}

		return faldoniTrovati;
	}

	/**
	 * Costruisce il tree per la selezione dei nodi degli uffici e degli utenti.
	 */
	public void creaTree() {
		oggettoFaldoneSelezionato = "";
		descrizioneFaldoneSelezionato = "";
		// Inserisci radice
		root = new DefaultTreeNode(new TreeNodeFaldoneDTO(utente.getUfficioDesc(), utente.getUfficioDesc(), "Root", utente.getIdUfficio().intValue(), null, "Root"));
		init(root);
		root.setChildren(new ArrayList<>());
		nodiNelTree = new HashMap<>();
		nodiNelTree.put("Root", root);

		final DefaultTreeNode nodoUfficio = new DefaultTreeNode(TreeNodeTypeEnum.UFFICIO.getType(), new TreeNodeFaldoneDTO(utente.getUfficioDesc()), null);
		nodoUfficio.setParent(root);
		nodoUfficio.setSelectable(false);
		nodiNelTree.put("Ufficio", nodoUfficio);
		root.getChildren().add(nodoUfficio);

		final Collection<FaldoneDTO> faldoniResult = cercaFaldoni();
		if (faldoniResult.isEmpty()) {
			return;
		}

		final Collection<FaldoneDTO> faldoniResultWithoutFittizi = new ArrayList<>();
		// Creo tutti i nodi dell'albero
		DefaultTreeNode nodo = null;
		for (final FaldoneDTO faldone : faldoniResult) {
			// Creo nodo fittizio
			if (!DESCRIZIONE_FALDONE_FITTIZIO.equals(faldone.getDescrizioneFaldone())) {
				nodo = new DefaultTreeNode(TreeNodeTypeEnum.FALDONE.getType(), faldone, null);
				nodiNelTree.put(faldone.getDocumentTitle(), nodo);
				faldoniResultWithoutFittizi.add(faldone);
			}
		}

		DefaultTreeNode nodoPadre = null;
		for (final FaldoneDTO f : faldoniResultWithoutFittizi) {
			nodo = (DefaultTreeNode) nodiNelTree.get(f.getDocumentTitle());
			if (f.getParentFaldone() == null) {
				nodoUfficio.getChildren().add(nodo);
				nodo.setParent(nodoUfficio);
				nodoUfficio.setExpanded(true);
			} else {
				nodoPadre = (DefaultTreeNode) nodiNelTree.get(f.getParentFaldone());
				// Se il padre non esiste allora leghiamo al padre fittizio
				if (nodoPadre == null) {
					nodoPadre = (DefaultTreeNode) nodiNelTree.get("0");
				}
				if (nodoPadre != null) {
					if (nodoPadre.getChildren() == null) {
						nodoPadre.setChildren(new ArrayList<>());
					}
					nodoPadre.getChildren().add(nodo);
					nodo.setParent(nodoPadre);
					nodoPadre.setExpanded(true);
				}
			}
		}
	}

	/**
	 * Gestisce la selezione di un nodo dell'albero.
	 * 
	 * @param event evento gui
	 */
	public void onTreeNodeSelect(final NodeSelectEvent event) {
		try {
			setSelectedNode(event.getTreeNode());
			final FaldoneDTO faldoneScelto = (FaldoneDTO) getSelectedNode().getData();
			if (faldoneScelto != null) {
				oggettoFaldoneSelezionato = faldoneScelto.getOggetto();
				descrizioneFaldoneSelezionato = faldoneScelto.getDescrizioneFaldone();

				if (getTipoCreazione().equals(CREA_NUOVO_SOTTOFALDONE)) {
					faldonePadre = faldoneScelto;
				} else if (!faldoniScelti.contains(faldoneScelto)) {
					faldoniScelti.add(faldoneScelto);
				}

				if (getActiveTabIndex() == NUOVO_TAB) {
					setFaldoniTrovati(null);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}
	}

	/**
	 * Imposta a true il flag per il reset.
	 */
	public void resetValues() {
		resetValues(true);
	}

	/**
	 * Effettua il reset dei campi.
	 * 
	 * @param resetRadio
	 */
	public void resetValues(final boolean resetRadio) {
		testoRicerca = "";
		faldonePadre = null;
		oggettoFaldoneSelezionato = "";
		descrizioneFaldoneSelezionato = "";
		root = null;
		faldoniTrovati = null;

		oggettoNuovoFaldone = "";
		descrizioneNuovoFaldone = "";

		if (resetRadio) {
			tipoCreazione = CREA_NUOVO_FALDONE;
		}
	}

	/**
	 * Gestisce la faldonatura distinguendo se deve creare un faldone o un
	 * sottofaldone oppure se è stato selezionato un faldone.
	 */
	public List<FaldoneDTO> registra() {
		final List<FaldoneDTO> fReturn = new ArrayList<>();
		
		if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(this.oggettoNuovoFaldone) && faldoniScelti.isEmpty()) {
			showWarnMessage("Nessun faldone è stato selezionato o creato.");
			return new ArrayList<FaldoneDTO>();
		}

		for (final FaldoneDTO fald : faldoniScelti) {
			boolean esitoFaldonaturaOk = true;
			if (!StringUtils.isNullOrEmpty(fascicolo.getIdFascicolo())) {
				// sono nel caso in cui il fascicolo esiste e mi viene passato dal chiamante
				esitoFaldonaturaOk = faldona(fald.getNomeFaldone(), fascicolo.getIdFascicolo());
			}

			// se l'esito è false significa che è già stato faldonato
			if (!esitoFaldonaturaOk) {
				// allerto l'utente e rimango nella maschera
				showWarnMessage(FASCICOLO_LITERAL + fascicolo.getIdFascicolo() + "]  è già stato faldonato nel faldone [" + fald.getOggetto() + "] ");
			} else {
				fReturn.add(fald);
			}

			if (!StringUtils.isNullOrEmpty(fald.getParentFaldone())) {
				final FaldoneDTO faldone = faldoneSRV.getFaldone(fald.getParentFaldone(), utente);

				if (faldone != null) {
					fald.setNomePadre(faldone.getNomeFaldone());
					fald.setOggettoPadre(faldone.getOggetto());
				}
			}
		}
		return fReturn;
	}

	/**
	 * Gestisce la faldonatura dei fascicoli.
	 * 
	 * @return true se si è verificato un errore o warning, false l'esito è positivo
	 */
	public boolean registraMassivo() {
		boolean isWarn = false;
		final StringBuilder sb = new StringBuilder();

		if (listaFascicoli != null && !listaFascicoli.isEmpty()) {
			for (final FascicoloDTO f : listaFascicoli) {
				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(this.oggettoNuovoFaldone) && faldoniScelti.isEmpty()) {
					showWarnMessage("Nessun faldone è stato selezionato o creato.");
					return false;
				}

				for (final FaldoneDTO fald : faldoniScelti) {
					if (faldona(fald.getNomeFaldone(), f.getIdFascicolo())) {
						sb.append(FASCICOLO_LITERAL).append(f.getIdFascicolo()).append("] è stato faldonato nel/i faldone/i ");
						// scrivo quanto sopra solo alla prima faldonatur
						sb.append("[").append(fald.getOggetto()).append("] ");

					} else {
						sb.append(FASCICOLO_LITERAL).append(f.getIdFascicolo()).append("] è già stato faldonato nel/i faldone/i ");
						// scrivo quanto sopra solo alla prima faldonatura non eseguita
						sb.append("[").append(fald.getOggetto()).append("] ");

						isWarn = true;
					}
				}
			}
			if (isWarn) {
				showWarnMessage(sb.toString());
			} else {
				showInfoMessage(sb.toString());
			}
		}
		return isWarn;
	}

	/**
	 * @return
	 */
	public FaldoneDTO creaFaldone() {
		FaldoneDTO f = null;
		final String documentTitleFaldonePadre = (faldonePadre != null) ? faldonePadre.getDocumentTitle() : null;
		nomeFaldoneCreato = faldoneSRV.createFaldone(utente, oggettoNuovoFaldone, descrizioneNuovoFaldone, documentTitleFaldonePadre, canBeDuplicate);
		f = new FaldoneDTO(null, new Date(), nomeFaldoneCreato, documentTitleFaldonePadre, descrizioneNuovoFaldone, oggettoNuovoFaldone);
		return f;
	}

	/**
	 * Esegue la faldonatura del fascicolo identificato dall'id passato come
	 * parametro.
	 * 
	 * @param nomeFaldone
	 * @param idFascicolo
	 * @return true se faldonato con successo, false altrimenti
	 */
	public boolean faldona(final String nomeFaldone, final String idFascicolo) {
		boolean faldonato = false;
		try {
			faldonato = faldoneSRV.faldonaFascicolo(nomeFaldone, idFascicolo, utente.getIdAoo(), utente);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}
		return faldonato;
	}

	/**
	 * Resetta i campi selezionati relatvi ai faldoni.
	 */
	public void pulisciSelezionati() {
		faldonePadre = null;
		faldoniScelti = new ArrayList<>();
	}

	/**
	 * Gestisce la deselezione del faldone.
	 * 
	 * @param faldone
	 */
	public void deselezionaFaldone(final FaldoneDTO faldone) {
		if (faldone.equals(faldonePadre)) {
			faldonePadre = null;
		}
		faldoniScelti.remove(faldone);
	}

	/**
	 * Gestisce l'aggiunta del faldone.
	 */
	public void aggiungiNuovoFaldone() {
		try {
			if (!StringUtils.isNullOrEmpty(oggettoNuovoFaldone)) {
				final FaldoneDTO nuovo = creaFaldone();
				getFaldoniScelti().add(nuovo);
			} else {
				showWarnMessage("Inserire l'oggetto del faldone.");
			}
		} catch (final RedException e) {
			showError(e);
		}
	}

	@Override
	protected void postConstruct() {
		// non serve
	}

	/**
	 * Restituisce il fascicolo.
	 * 
	 * @return fascicolo
	 */
	public FascicoloDTO getFascicolo() {
		return fascicolo;
	}

	/**
	 * Imposta il fascicolo.
	 * 
	 * @param fascicolo
	 */
	public void setFascicolo(final FascicoloDTO fascicolo) {
		this.fascicolo = fascicolo;
	}

	/**
	 * Restituisce la root del tree.
	 */
	@Override
	public DefaultTreeNode getRoot() {
		return root;
	}

	/**
	 * Imposta la radice del tree.
	 * 
	 * @param root
	 */
	public void setRoot(final DefaultTreeNode root) {
		this.root = root;
	}

	/**
	 * Restituisce il tipo creazione.
	 * 
	 * @return tipo creazione
	 */
	public String getTipoCreazione() {
		return tipoCreazione;
	}

	/**
	 * Imposta il tipo creazione.
	 * 
	 * @param tipo creazione
	 */
	public void setTipoCreazione(final String tipoCreazione) {
		this.tipoCreazione = tipoCreazione;
	}

	/**
	 * Restituisce il testo di ricerca.
	 * 
	 * @return testo della ricerca
	 */
	public String getTestoRicerca() {
		return testoRicerca;
	}

	/**
	 * Imposta il testo della ricerca.
	 * 
	 * @param testoRicerca
	 */
	public void setTestoRicerca(final String testoRicerca) {
		this.testoRicerca = testoRicerca;
	}

	/**
	 * Restituisce i nodi esistenti nel tree.
	 * 
	 * @return nodi
	 */
	public Map<String, TreeNode> getNodiNelTree() {
		return nodiNelTree;
	}

	/**
	 * Imposta i nodi del tree.
	 * 
	 * @param nodiNelTree
	 */
	public void setNodiNelTree(final Map<String, TreeNode> nodiNelTree) {
		this.nodiNelTree = nodiNelTree;
	}

	/**
	 * Restituisce l'oggetto del faldone selezionato.
	 * 
	 * @return oggetto del faldone
	 */
	public String getOggettoFaldoneSelezionato() {
		return oggettoFaldoneSelezionato;
	}

	/**
	 * Imposta l'oggetto del faldone selezionato.
	 * 
	 * @param oggettoFaldoneSelezionato
	 */
	public void setOggettoFaldoneSelezionato(final String oggettoFaldoneSelezionato) {
		this.oggettoFaldoneSelezionato = oggettoFaldoneSelezionato;
	}

	/**
	 * Restituisce la descrizione del faldone selezionato.
	 * 
	 * @return descrizione del faldone
	 */
	public String getDescrizioneFaldoneSelezionato() {
		return descrizioneFaldoneSelezionato;
	}

	/**
	 * Imposta la descrizione del faldone selezionato.
	 * 
	 * @param descrizioneFaldoneSelezionato
	 */
	public void setDescrizioneFaldoneSelezionato(final String descrizioneFaldoneSelezionato) {
		this.descrizioneFaldoneSelezionato = descrizioneFaldoneSelezionato;
	}

	/**
	 * Restituisce il faldone padre.
	 * 
	 * @return faldone padre come FaldoneDTO
	 */
	public FaldoneDTO getFaldonePadre() {
		return faldonePadre;
	}

	/**
	 * Imposta il faldone padre.
	 * 
	 * @param faldoneScelto
	 */
	public void setFaldonePadre(final FaldoneDTO faldoneScelto) {
		this.faldonePadre = faldoneScelto;
	}

	/**
	 * Restituisce il nome del master bean.
	 * 
	 * @return nome
	 */
	public String getMasterBeanName() {
		return masterBeanName;
	}

	/**
	 * Imposta il nome del master bean.
	 * 
	 * @param masterBeanName
	 */
	public void setMasterBeanName(final String masterBeanName) {
		this.masterBeanName = masterBeanName;
	}

	/**
	 * Restituisce la lista dei faldoni trovati.
	 * 
	 * @return faldoni trovati
	 */
	public Collection<FaldoneDTO> getFaldoniTrovati() {
		return faldoniTrovati;
	}

	/**
	 * Imposta la lista dei faldoni trovati.
	 * 
	 * @param faldoniTrovati
	 */
	public void setFaldoniTrovati(final Collection<FaldoneDTO> faldoniTrovati) {
		this.faldoniTrovati = faldoniTrovati;
	}

	/**
	 * Restituisce l'oggetto del nuovo faldone.
	 * 
	 * @return oggetto
	 */
	public String getOggettoNuovoFaldone() {
		return oggettoNuovoFaldone;
	}

	/**
	 * Imposta l'oggetto del nuovo faldone.
	 * 
	 * @param oggettoNuovoFaldone
	 */
	public void setOggettoNuovoFaldone(final String oggettoNuovoFaldone) {
		this.oggettoNuovoFaldone = oggettoNuovoFaldone;
	}

	/**
	 * Restituisce la descrizione del nuovo faldone.
	 * 
	 * @return desrizione faldone
	 */
	public String getDescrizioneNuovoFaldone() {
		return descrizioneNuovoFaldone;
	}

	/**
	 * Imposta la descrizione del nuovo faldone.
	 * 
	 * @param descrizioneNuovoFaldone
	 */
	public void setDescrizioneNuovoFaldone(final String descrizioneNuovoFaldone) {
		this.descrizioneNuovoFaldone = descrizioneNuovoFaldone;
	}

	/**
	 * Restituisce il nome del faldone creato.
	 * 
	 * @return nome faldone
	 */
	public String getNomeFaldoneCreato() {
		return nomeFaldoneCreato;
	}

	/**
	 * Restituisce la label {@link #CREA_NUOVO_FALDONE}.
	 * 
	 * @return label
	 */
	public String getCreaNuovoFaldone() {
		return CREA_NUOVO_FALDONE;
	}

	/**
	 * Restituisce la label {@link #CREA_NUOVO_SOTTOFALDONE}.
	 * 
	 * @return label
	 */
	public String getCreaNuovoSottofaldone() {
		return CREA_NUOVO_SOTTOFALDONE;
	}

	/**
	 * Restituisce la lista dei fascicoli.
	 * 
	 * @return lista fascicoli
	 */
	public List<FascicoloDTO> getListaFascicoli() {
		return listaFascicoli;
	}

	/**
	 * Imposta la lista dei fascicoli.
	 * 
	 * @param listaFascicoli
	 */
	public void setListaFascicoli(final List<FascicoloDTO> listaFascicoli) {
		this.listaFascicoli = listaFascicoli;
	}

	/**
	 * Restituisce true se può essere duplicato.
	 * 
	 * @return canBeDuplicate
	 */
	public Boolean getCanBeDuplicate() {
		return canBeDuplicate;
	}

	/**
	 * Imposta il flag: canBeDuplicate.
	 * 
	 * @param canBeDuplicate
	 */
	public void setCanBeDuplicate(final Boolean canBeDuplicate) {
		this.canBeDuplicate = canBeDuplicate;
	}

	/**
	 * Restituisce la lista dei faldoni.
	 * 
	 * @return faldoni
	 */
	public Collection<FaldoneDTO> getFaldoni() {
		return faldoni;
	}

	/**
	 * Imposta la lista dei faldoni.
	 * 
	 * @param faldoni
	 */
	public void setFaldoni(final Collection<FaldoneDTO> faldoni) {
		if (faldoni != null && !faldoni.isEmpty()) {
			setEsistonoFaldoniAssociati(true);
		}
		this.faldoni = faldoni;
	}

	/**
	 * Restituisce true se esistono faldoni associati, false altrimenti.
	 * 
	 * @return true se esistono faldoni associati, false altrimenti
	 */
	public boolean isEsistonoFaldoniAssociati() {
		return esistonoFaldoniAssociati;
	}

	/**
	 * Imposta il flag: esistonoFaldoniAssociati.
	 * 
	 * @param esistonoFaldoniAssociati
	 */
	public void setEsistonoFaldoniAssociati(final boolean esistonoFaldoniAssociati) {
		this.esistonoFaldoniAssociati = esistonoFaldoniAssociati;
	}

	/**
	 * Restituisce la lista del faldoni scelti.
	 * 
	 * @return faldoni
	 */
	public List<FaldoneDTO> getFaldoniScelti() {
		return faldoniScelti;
	}

	/**
	 * Imposta la lista dei faldoni scelti.
	 * 
	 * @param faldoniScelti
	 */
	public void setFaldoniScelti(final List<FaldoneDTO> faldoniScelti) {
		this.faldoniScelti = faldoniScelti;
	}

	/**
	 * Restituisce il flag: selezionaTuttiFaldoni.
	 * 
	 * @return selezionaTuttiFaldoni
	 */
	public boolean isSelezionaTuttiFaldoni() {
		return selezionaTuttiFaldoni;
	}

	/**
	 * Imposta il flag: selezionaTuttiFaldoni.
	 * 
	 * @param selezionaTuttiFaldoni
	 */
	public void setSelezionaTuttiFaldoni(final boolean selezionaTuttiFaldoni) {
		this.selezionaTuttiFaldoni = selezionaTuttiFaldoni;
	}

	/**
	 * Restituisce l'indice del tab attivo.
	 * 
	 * @return indice tab
	 */
	public int getActiveTabIndex() {
		return activeTabIndex;
	}

	/**
	 * Imposta l'indice del tab attivo.
	 * 
	 * @param activeTabIndex
	 */
	public void setActiveTabIndex(final int activeTabIndex) {
		this.activeTabIndex = activeTabIndex;
	}

	@Override
	public void onOpenTree(TreeNode nodo) {
		throw new NotImplementedException("Metodo non implementato");
	}
}