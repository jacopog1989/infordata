package it.ibm.red.web.ricerca.mbean;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.ParamsRicercaEstrattiContoDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.dto.StatoRicercaDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;

/**
 * Bean che gestisce la ricerca per estratti conto ccvt.
 */
@Named(ConstantsWeb.MBean.RICERCA_ESTRATTI_CONTO_CCVT_BEAN)
@ViewScoped
public class RicercaEstrattiContoCCVTBean extends AbstractBean {

	/**
	 * serial version UID.
	 */
	private static final long serialVersionUID = -8659886620858883202L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaEstrattiContoCCVTBean.class.getName());

	/**
	 * Lista risultati.
	 */
	private StatoRicercaDTO result;
	
	/**
	 * Session Bean.
	 */
	private SessionBean sessionBean;

	/**
	 * Params ricerca estratti conto.
	 */
	private ParamsRicercaEstrattiContoDTO formEstrattiConto;
	
	/**
	 * Lista sedi estere.
	 */
	private List<SelectItemDTO> sediEstere;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		setResult((StatoRicercaDTO) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.RISULTATO_RICERCA_ESTRATTI_CONTO, true));
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		ITipologiaDocumentoFacadeSRV tipologiaDocumentoSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoFacadeSRV.class);
		
		if(result != null) {
			formEstrattiConto = (ParamsRicercaEstrattiContoDTO) result.getFormRicerca();
		}
		
		sediEstere = tipologiaDocumentoSRV.getValuesBySelector(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.SELETTORE_SEDI_ESTERE), null, sessionBean.getUtente().getIdAoo());
	}
	
	/**
	 * Metodo per reimpostare nel session bean il form di ricerca.
	 */
	public void ripetiRicercaEstrattiContoUcb() {
		try {
			sessionBean.getRicercaFormContainer().setEstrattiContoUCB(formEstrattiConto);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}
	
	/**
	 * Restituisce il nome del file per l'export della coda.
	 * @return nome file per export
	 */
	public String getNomeFileExport() {
		String nome = "";
		if (sessionBean.getUtente() != null) {
			nome += sessionBean.getUtente().getNome().toLowerCase() + "_" + sessionBean.getUtente().getCognome().toLowerCase() + "_";
		}
		
		nome += "report_trimestrale_ccvt_";
		
		final SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH.mm");
		nome += sdf.format(new Date()); 
		
		return nome;
	}

	/**
	 * Restituisce il risultato della ricerca.
	 * @return risultato ricerca
	 */
	public StatoRicercaDTO getResult() {
		return result;
	}

	/**
	 * Imposta il risultato della ricerca.
	 * @param result
	 */
	public void setResult(StatoRicercaDTO result) {
		this.result = result;
	}

	/**
	 * Restituisce i parametri della ricerca.
	 * @return parametri ricerca
	 */
	public ParamsRicercaEstrattiContoDTO getFormEstrattiConto() {
		return formEstrattiConto;
	}

	/**
	 * Imposta i parametri della ricerca.
	 * @param formEstrattiConto
	 */
	public void setFormEstrattiConto(ParamsRicercaEstrattiContoDTO formEstrattiConto) {
		this.formEstrattiConto = formEstrattiConto;
	}
	
	/**
	 * Lista descrizioni sedi estere.
	 * @return sediEstere
	 */
	public List<SelectItemDTO> getSediEstere() {
		return sediEstere;
	}

	/**
	 * Imposta la lista delle descrizioni delle sedi estere.
	 * @param sediEstere
	 */
	public void setSediEstere(List<SelectItemDTO> sediEstere) {
		this.sediEstere = sediEstere;
	}
}
