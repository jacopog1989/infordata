package it.ibm.red.web.mbean.humantask;

import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections.CollectionUtils;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IPredisponiFirmaAutografaFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * Classe PredisposizioneFirmaAutografaBean.
 */
@Named(MBean.PREDISPOSIZIONE_FIRMA_AUTOGRAFA_BEAN)
@ViewScoped
public class PredisposizioneFirmaAutografaBean extends AbstractBean implements InitHumanTaskInterface {
	
	/**
	 * Costante serial version UIDF
	 */
	private static final long serialVersionUID = -7800158375761480647L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PredisposizioneFirmaAutografaBean.class.getName());
	
	/**
	 * Lista dei master.
	 */
	private List<MasterDocumentRedDTO> masters;
	
	
	@Override
	protected void postConstruct() {
		// Non deve fare nulla
	}
	
	
	/**
	 * Inizializzazione bean
	 *
	 * @param inDocsSelezionati the in docs selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		masters = inDocsSelezionati;
	}
	
	
	/**
	 * Esecuzione response predisposizione firma autografa
	 */
	public void doResponse() {
		if (!CollectionUtils.isEmpty(masters) && masters.size() > 1) {
			showWarnMessage("Per la funzionalità non è consentita la selezione massiva.");
		} else {
			final SessionBean sb = FacesHelper.getManagedBean(MBean.SESSION_BEAN);
			final UtenteDTO utente = sb.getUtente();
			final ListaDocumentiBean ldb = FacesHelper.getManagedBean(MBean.LISTA_DOCUMENTI_BEAN);
			final IPredisponiFirmaAutografaFacadeSRV predisponiFirmaAutografaSRV = 
					ApplicationContextProvider.getApplicationContext().getBean(IPredisponiFirmaAutografaFacadeSRV.class);
			
			if (CollectionUtils.isEmpty(masters)) {
				LOGGER.error("Non è stato selezionato nessun documento per l'esecuzione della response" + ldb.getSelectedResponse().getResponse());
				showError("Nessun documento selezionato per l'esecuzione della response.");
			} else {
				final EsitoOperazioneDTO esitoOperazione = predisponiFirmaAutografaSRV.eseguiPredisposizionePerFirmaAutografa(masters.get(0).getWobNumber(), utente);
				
				ldb.getEsitiOperazione().add(esitoOperazione);
				ldb.destroyBeanViewScope(MBean.PREDISPOSIZIONE_FIRMA_AUTOGRAFA_BEAN);
			}
		}
	}
}