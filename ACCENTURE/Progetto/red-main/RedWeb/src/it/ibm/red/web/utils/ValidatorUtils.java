package it.ibm.red.web.utils;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import it.ibm.red.business.logger.REDLogger;

/**
 * Utility validator.
 */
public final class ValidatorUtils {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ValidatorUtils.class);
	
	/**
	 * Costruttore vuoto.
	 */
	private ValidatorUtils() {
		// Costruttore vuoto.
	}
	
	/**
	 * Verifica che esista una mail valida.
	 * 
	 * @param email
	 * @return
	 */
	public static boolean mailAddress(final String email) {
		boolean isValid = false;
		try {
			//
			// Create InternetAddress object and validated the supplied
			// address which is this case is an email address.
			final InternetAddress internetAddress = new InternetAddress(email);
			internetAddress.validate();
			isValid = true;
		} catch (final AddressException e) {
			LOGGER.warn("Validazione dell'email non riuscita :" + email, e);
			
		}
		return isValid;
	}
	
}
