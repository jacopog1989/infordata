/**
 * 
 */
package it.ibm.red.web.ricerca.mbean;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.Visibility;

import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.MasterSigiDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.StatoRicercaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.IPropertiesFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.helper.faces.MessageSeverityEnum;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.DettaglioSigiBean;
import it.ibm.red.web.mbean.SessionBean;

/**
 * @author APerquoti
 *
 */
@Named(ConstantsWeb.MBean.RICERCA_SIGI_BEAN)
@ViewScoped
public class RicercaSigiBean extends AbstractBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 5906738210048289622L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaSigiBean.class);

	/**
	 * Servizio.
	 */
	private IDocumentoFacadeSRV documentoSRV;

	/**
	 * Bean sessione.
	 */
	private SessionBean sessionBean;

	/**
	 * Parametri di ricerca.
	 */
	private ParamsRicercaAvanzataDocDTO formRicerca;

	/**
	 * Informazione utente.
	 */
	private UtenteDTO utente;

	/**
	 * Datatable documenti sigi.
	 */
	protected SimpleDetailDataTableHelper<MasterSigiDTO> documentiSigiDTH;

	/**
	 * Lista colonne.
	 */
	private List<Boolean> sigiColumns;

	/**
	 * Numero massimo risultati.
	 */
	private Integer numMaxRisultati;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();

		documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);

		documentiSigiDTH = new SimpleDetailDataTableHelper<>();
		documentiSigiDTH.setMasters(new ArrayList<>());

		sigiColumns = Arrays.asList(true, true, true, true, true, true);

		final IPropertiesFacadeSRV propertiesSRV = ApplicationContextProvider.getApplicationContext().getBean(IPropertiesFacadeSRV.class);
		numMaxRisultati = Integer.parseInt(propertiesSRV.getByEnum(PropertiesNameEnum.RICERCA_MAX_RESULTS));

		initPaginaRicerca();
	}

	/**
	 * Inizializza la pagina della ricerca.
	 */
	@SuppressWarnings("unchecked")
	public void initPaginaRicerca() {
		try {
			final StatoRicercaDTO stDto = (StatoRicercaDTO) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.STATO_RICERCA, true);
			if (stDto != null) {
				final Collection<MasterSigiDTO> searchResult = (Collection<MasterSigiDTO>) stDto.getRisultatiRicerca();
				formRicerca = (ParamsRicercaAvanzataDocDTO) stDto.getFormRicerca();

				documentiSigiDTH.setMasters(searchResult);
				documentiSigiDTH.selectFirst(true);

				if (CollectionUtils.isEmpty(searchResult)) {
					throw new RedException("La ricerca non ha prodotto risultati.");
				} else if (numMaxRisultati != null && numMaxRisultati.equals(searchResult.size())) {
					showWarnMessage(RicercaAbstractBean.MSG_NUM_MAX_RISULTATI);
				}

				selectDetail(documentiSigiDTH.getCurrentMaster());
			} else {
				formRicerca = new ParamsRicercaAvanzataDocDTO();
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Non è stato possibile recuperare i risultati di ricerca");
		}
	}

	/**
	 * Gestisce la selezione della riga associata al datatabel dei documenti SIGI.
	 * 
	 * @param se
	 */
	public final void rowSelector(final SelectEvent se) {
		documentiSigiDTH.rowSelector(se);
		selectDetail(documentiSigiDTH.getCurrentMaster());
	}

	/**
	 * Imposta il dettaglio del master in ingresso.
	 * 
	 * @param m
	 */
	public void selectDetail(final MasterSigiDTO m) {
		try {
			final DettaglioSigiBean detailBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_SIGI_BEAN);
			detailBean.setDetail(m.getIdDocumento());
		} catch (final Exception e) {
			LOGGER.error("Errore durante la visualizzazione del dettaglio", e);
			showError("Errore durante la visualizzazione del dettaglio");
		}
	}

	/**
	 * Restituisce lo StreamedContent per il download del content del documento in
	 * ingresso.
	 * 
	 * @param doc
	 * @return StreamedContent per download
	 */
	public StreamedContent scaricaDocSigi(final MasterSigiDTO doc) {
		StreamedContent sc = null;
		try {
			final FileDTO file = documentoSRV.getDocumentoSIGIContentPreview(utente.getFcDTO(), doc.getIdDocumento(), utente.getIdAoo());
			final InputStream io = new ByteArrayInputStream(file.getContent());
			sc = new DefaultStreamedContent(io, file.getMimeType(), file.getFileName());
		} catch (final Exception e) {
			LOGGER.error("Impossibile recuperare il Documento SIGI", e);
			FacesHelper.showMessage(null, "Impossibile recuperare il Documento SIGI", MessageSeverityEnum.ERROR);
		}
		return sc;
	}

	/**
	 * Metodo per reimpostare nel session bean il form utilizzato per la ricerca e
	 * quindi visualizzarlo nel dialog
	 */
	public void ripetiRicercaSigi() {
		try {
			// Viene ripetuta la ricerca con i stessi filtri
			sessionBean.getRicercaFormContainer().setSigi(formRicerca);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Gestisce l'evento "Toggle" associato alle colonne dei documenti Sigi.
	 * 
	 * @param event
	 */
	public final void onColumnToggleSigi(final ToggleEvent event) {
		sigiColumns.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
	}

//	<-- get/set -->

	/**
	 * @return the documentiSigiDTH
	 */
	public final SimpleDetailDataTableHelper<MasterSigiDTO> getDocumentiSigiDTH() {
		return documentiSigiDTH;
	}

	/**
	 * @param documentiSigiDTH the documentiSigiDTH to set
	 */
	public final void setDocumentiSigiDTH(final SimpleDetailDataTableHelper<MasterSigiDTO> documentiSigiDTH) {
		this.documentiSigiDTH = documentiSigiDTH;
	}

	/**
	 * @return the sigiColumns
	 */
	public final List<Boolean> getSigiColumns() {
		return sigiColumns;
	}

	/**
	 * @param sigiColumns the sigiColumns to set
	 */
	public final void setSigiColumns(final List<Boolean> sigiColumns) {
		this.sigiColumns = sigiColumns;
	}

	/**
	 * Restituisce ll nome del file per l'export della ricerca.
	 * 
	 * @return nome file export
	 */
	public String getNomeFileExportCoda() {

		String nome = "";
		if (sessionBean.getUtente() != null) {
			nome += sessionBean.getUtente().getNome().toLowerCase() + "_" + sessionBean.getUtente().getCognome().toLowerCase() + "_";
		}

		nome += "ricercasigi_";

		final SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH.mm");
		nome += sdf.format(new Date());

		return nome;
	}

}
