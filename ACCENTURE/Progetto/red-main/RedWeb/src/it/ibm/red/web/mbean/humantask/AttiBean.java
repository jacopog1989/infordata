package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.DocIndiceDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.GestioneLockDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IAttiFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV;
import it.ibm.red.business.service.facade.IFascicoloFacadeSRV;
import it.ibm.red.business.service.facade.ITitolarioFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.mbean.component.DialogFaldonaComponent;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * Bean che gestisce gli atti.
 */
@Named(MBean.ATTI_BEAN)
@ViewScoped
public class AttiBean extends AbstractBean implements InitHumanTaskInterface {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 5965494868446108139L;

	/**
	 * Messaggio di errore in fase di messa agli atti.
	 */
	private static final String ERROR_MESSA_AGLI_ATTI_MSG = "Si è verificato un errore durante la messa agli atti del documento.";

	/**
	 * Messaggio di errore generico, occorre appendere l'operezione al messaggio.
	 */
	private static final String GENERIC_ERROR_MSG = "Errore durante l'operazione: ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AttiBean.class.getName());

	/**
	 * Motivvazione.
	 */
	private String motivoAtti;

	/**
	 * Service fascicolo.
	 */
	private IFascicoloFacadeSRV fascicoloSRV;

	/**
	 * Service titolario.
	 */
	private ITitolarioFacadeSRV titolarioSRV;

	/**
	 * Service documento.
	 */
	private IDocumentoFacadeSRV documentoSRV;

	/**
	 * Lista documenti bean.
	 */
	private ListaDocumentiBean ldb;

	/**
	 * Info utente.
	 */
	private UtenteDTO utente;

	/**
	 * Titolario root.
	 */
	private DefaultTreeNode rootTitolario;

	/**
	 * Data table documenti.
	 */
	private SimpleDetailDataTableHelper<DocIndiceDTO> documentiDTH;

	/**
	 * Flag disabilita pulsante di registrazione.
	 */
	private Boolean flagDisableRegistraButton;

	/**
	 * Document title del documento da faldonare.
	 */
	private String docTitleDaFaldonare;

	/**
	 * Component per la gestione del dialog di faldonatura.
	 */
	private DialogFaldonaComponent faldonaCmp;

	/**
	 * Flag per indicare se il dialog di faldonatura è renderizzato.
	 */
	private boolean dlgFaldonaturaRendered;

	/**
	 * Messaggio di conferma.
	 */
	private String confirmMessage;

	/**
	 * Nome utente che ha eseguito il lock.
	 */
	private String nomeUtenteLockatore;

	/**
	 * Lista wob number.
	 */
	private Collection<String> wobNumbersAfterCheck;

	/**
	 * Esegue la response associata agli atti. Prima di eseguirla verifica che sia
	 * possibile, in caso contrario mostra i messaggi di errore all'utente.
	 */
	public void atti() {
		try {
			if (utente.getShowDialogAoo()) {
				confirmMessage = "Attenzione, il documento in stato chiuso non è modificabile. Per proseguire selezionare “Conferma” oppure cliccare “Annulla” per interrompere l’operazione.";
				FacesHelper.executeJS("PF('confirmAttiFirma_VW').show();");
				FacesHelper.update("centralSectionForm:confirmAttiFirma_PNL");
			} else {
				attiResponse();
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_MESSA_AGLI_ATTI_MSG, e);
			showError(ERROR_MESSA_AGLI_ATTI_MSG);
		}
	}

	/**
	 * Inizializza il bean.
	 * @param inDocsSelezionati documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		final List<DocIndiceDTO> documenti = new ArrayList<>();

		final Collection<MasterDocumentRedDTO> masters = inDocsSelezionati;

		titolarioSRV = ApplicationContextProvider.getApplicationContext().getBean(ITitolarioFacadeSRV.class);

		fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloFacadeSRV.class);

		documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);

		for (final MasterDocumentRedDTO m : masters) {
			final DocIndiceDTO detail = new DocIndiceDTO();
			detail.setWobNumber(m.getWobNumber());
			detail.setDocumentTitle(m.getDocumentTitle());
			detail.setIdAoo(utente.getIdAoo());
			detail.setNumeroDoc(m.getNumeroDocumento().toString());
			detail.setFascicoloProc(fascicoloSRV.getFascicoloProcedimentale(detail.getDocumentTitle(), utente.getIdAoo().intValue(), utente));

			detail.setIdProtocollo(m.getIdProtocollo());
			detail.setOggetto(m.getOggetto());
			detail.setDescrizioneTipologiaDocumento(m.getTipologiaDocumento());

			if(detail.getFascicoloProc()!=null && detail.getFascicoloProc().getIndiceClassificazione()!=null && detail.getFascicoloProc().getDescrizioneTitolario()!=null) {
				detail.setIndiceClassificazione(detail.getFascicoloProc().getIndiceClassificazione());
				detail.setIndiceClassificazioneDescrizione(detail.getFascicoloProc().getIndiceClassificazione() + " - " + detail.getFascicoloProc().getDescrizioneTitolario());
			}

			documenti.add(detail);
		}

		creaTitolario();

		documentiDTH = new SimpleDetailDataTableHelper<>(documenti);

		flagDisableRegistraButton = false;
		for (final DocIndiceDTO d : documentiDTH.getMasters()) {
			// Se non è impostato un titolario
			if (d.getIndiceClassificazione() == null) {
				flagDisableRegistraButton = true;
				break;
			}
		}

		docTitleDaFaldonare = Constants.EMPTY_STRING;
		dlgFaldonaturaRendered = false;
	}

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		rootTitolario = new DefaultTreeNode();

		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
		utente = ((SessionBean) FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN)).getUtente();
	}

	/**
	 * Verifica l'esistenza dei Titolari.
	 * 
	 * @return true se tutti i titolari sono diversi da "Blank", false se uno
	 *         risulta "Blank"
	 */
	public Boolean checkTitolariExistence() {
		Boolean disabilita = false;

		for (final DocIndiceDTO d : documentiDTH.getMasters()) {
			// se non è impostato un titolario
			if (StringUtils.isBlank(d.getIndiceClassificazione())) {
				disabilita = true;
				break;
			}
		}

		return disabilita;
	}

	/**
	 * Gestisce la logica di creazione di un titolario.
	 */
	public void creaTitolario() {
		final Collection<TitolarioDTO> rootList = titolarioSRV.getFigliRadice(utente.getIdAoo(), utente.getIdUfficio());

		if (rootList == null || rootList.isEmpty()) {
			showWarnMessage("Nessun titolario disponibile.");
			return;
		}

		try {
			final TitolarioDTO root = new TitolarioDTO("", "", "Titolario", 0, utente.getIdAoo(), null, null, rootList.size());
			rootTitolario = new DefaultTreeNode(root, null);
			for (final TitolarioDTO t : rootList) {
				final DefaultTreeNode a = new DefaultTreeNode(t, rootTitolario);
				if (t.getNumeroFigli() > 0) {
					new DefaultTreeNode(null, a);
				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_MSG + e.getMessage());
		}

	}

	/**
	 * Definisce le azioni da eseguire per la corretta visualizzazione del contenuto
	 * di un nodo in fase di Expand.
	 * 
	 * @param event
	 */
	public void onTitolarioNodeExpand(final NodeExpandEvent event) {
		try {
			final TitolarioDTO toOpen = (TitolarioDTO) event.getTreeNode().getData();
			final List<TitolarioDTO> list = titolarioSRV.getFigliByIndice(utente.getIdAoo(), toOpen.getIndiceClassificazione());
			event.getTreeNode().getChildren().clear();
			if (list != null && !list.isEmpty()) {
				DefaultTreeNode indiceToAdd = null;
				for (final TitolarioDTO t : list) {
					indiceToAdd = new DefaultTreeNode(t, event.getTreeNode());
					if (t.getNumeroFigli() > 0) {
						new DefaultTreeNode(null, indiceToAdd);
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Definisce le azioni da eseguire per la corretta gestione del nodo
	 * selezionato.
	 * 
	 * @param nodeEvent
	 */
	public void onTitolarioNodeSelect(final NodeSelectEvent nodeEvent) {
		try {
			final TitolarioDTO titolarioNodoSelected = (TitolarioDTO) nodeEvent.getTreeNode().getData();
			documentiDTH.getCurrentMaster().setIndiceClassificazione(titolarioNodoSelected.getIndiceClassificazione());
			documentiDTH.getCurrentMaster()
					.setIndiceClassificazioneDescrizione(titolarioNodoSelected.getIndiceClassificazione() + " - " + titolarioNodoSelected.getDescrizione());
			documentiDTH.getCurrentMaster().setModified(true);

			for (final DocIndiceDTO d : documentiDTH.getMasters()) {
				if (d.getIndiceClassificazione() == null) {
					setFlagDisableRegistraButton(true);
					break;
				} else {
					setFlagDisableRegistraButton(false);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_MSG + e.getMessage());
		}
	}

	/**
	 * Apre la dialog per la gestione della faldonatura.
	 * 
	 * @param fascicoloDaFaldonare
	 */
	public void openDlgFaldonatura(final FascicoloDTO fascicoloDaFaldonare) {
		if (faldonaCmp == null) {
			faldonaCmp = new DialogFaldonaComponent(utente);
		}

		faldonaCmp.setFascicolo(fascicoloDaFaldonare);

		dlgFaldonaturaRendered = true;
	}

	/**
	 * Esegue le attività associate alla funzionalità di faldonatura.
	 */
	public void faldona() {
		final List<FaldoneDTO> faldoni = faldonaCmp.registra();

		// Se i faldoni non sono presenti, l'esito è già stato mostrato nel bottone
		// registra
		if (!faldoni.isEmpty()) {
			dlgFaldonaturaRendered = false;

			// Si prosegue con l'operazione di messa agli atti
			atti();
		}
	}

	/**
	 * Chiude la dialgo di faldonatura.
	 */
	public void closeDlgFaldonatura() {
		docTitleDaFaldonare = Constants.EMPTY_STRING;
		dlgFaldonaturaRendered = false;
	}

	/**
	 * Imposta il Master selezionato come Master corrente per garantire l'esecuzione
	 * delle azioni sul documento selezionato.
	 * 
	 * @param event
	 */
	public void currentDoc(final ActionEvent event) {
		documentiDTH.setCurrentMaster(getDataTableClickedRow(event));
	}

	/**
	 * Restituisce la motivazione degli atti.
	 * 
	 * @return motivoAtti
	 */
	public String getMotivoAtti() {
		return motivoAtti;
	}

	/**
	 * Imposta la motivazione degli atti.
	 * 
	 * @param motivoAtti
	 */
	public void setMotivoAtti(final String motivoAtti) {
		this.motivoAtti = motivoAtti;
	}

	/**
	 * Restituisce la root del titolario per la gestione del tree.
	 * 
	 * @return
	 */
	public DefaultTreeNode getRootTitolario() {
		return rootTitolario;
	}

	/**
	 * Imposta la root del titolario.
	 * 
	 * @param rootTitolario
	 */
	public void setRootTitolario(final DefaultTreeNode rootTitolario) {
		this.rootTitolario = rootTitolario;
	}

	/**
	 * Restituisce l'helper per la gestione del datatable dei documenti.
	 * 
	 * @return documentiDTH
	 */
	public SimpleDetailDataTableHelper<DocIndiceDTO> getDocumentiDTH() {
		return documentiDTH;
	}

	/**
	 * Imposta l'helper del datatable dei documenti.
	 * 
	 * @param documentiDTH
	 */
	public void setDocumentiDTH(final SimpleDetailDataTableHelper<DocIndiceDTO> documentiDTH) {
		this.documentiDTH = documentiDTH;
	}

	/**
	 * Restituisce il flag che consente di disabilitare il button legato alla
	 * funzionalità di registrazione.
	 * 
	 * @return flagDisableRegistraButton
	 */
	public Boolean getFlagDisableRegistraButton() {
		return flagDisableRegistraButton;
	}

	/**
	 * Imposta il flag che consente di disabilitare il button legato alla
	 * funzionalità di registrazione.
	 * 
	 * @param flagDisableRegistraButton
	 */
	public void setFlagDisableRegistraButton(final Boolean flagDisableRegistraButton) {
		this.flagDisableRegistraButton = flagDisableRegistraButton;
	}

	/**
	 * Restituisce il component per la gestione della dialog di faldonatura.
	 * 
	 * @return faldonaCmp
	 */
	public DialogFaldonaComponent getFaldonaCmp() {
		return faldonaCmp;
	}

	/**
	 * Restituisce la visibilità della dialog di faldonatura.
	 * 
	 * @return true se la dialog di faldonatura è visibile, false altrimenti
	 */
	public boolean isDlgFaldonaturaRendered() {
		return dlgFaldonaturaRendered;
	}

	/**
	 * Restituisce il messaggio di conferma.
	 * 
	 * @return confirmMessage
	 */
	public String getConfirmMessage() {
		return confirmMessage;
	}

	/**
	 * Imposta il messaggio di conferma.
	 * 
	 * @param confirmMessage
	 */
	public void setConfirmMessage(final String confirmMessage) {
		this.confirmMessage = confirmMessage;
	}

	/**
	 * Gestisce la response eseguendo le attività necessarie alla finalizzazione
	 * dell'azione.
	 */
	public void attiResponse() {
		final Collection<String> wobNumbers = new ArrayList<>();
		boolean continuaFlag = true;
		final List<Integer> docTitleList = new ArrayList<>();
		try {
			for (final DocIndiceDTO d : documentiDTH.getMasters()) {
				if (d.getIndiceClassificazione() == null || StringUtils.isEmpty(d.getIndiceClassificazione())) {
					continuaFlag = false;
					showWarnMessage("Operazione non riuscita: è necessario che tutti i documenti da mettere agli atti abbiano un indice di classificazione.");
					break;
				}
				if (d.getDocumentTitle() != null) {
					docTitleList.add(Integer.parseInt(d.getDocumentTitle()));
				}

				if (d.isModified()) {
					fascicoloSRV.associaATitolarioAggiornaNPS(utente, d.getFascicoloProc().getIdFascicolo(), d.getIndiceClassificazione(),
							d.getIndiceClassificazioneDescrizione());
					d.setModified(false);
				}
				// Faldonatura obbligatoria: si verifica se il documento deve essere
				// obbligatoriamente faldonato.
				// In tal caso, si apre la dialog per la faldonatura del documento.
				if (documentoSRV.isDocumentoFaldonaturaObbligatoria(d.getDocumentTitle(), utente.getIdAoo(), utente.getFcDTO())
						&& !Boolean.TRUE.equals(d.getFascicoloProc().getFaldonato()) && !d.getDocumentTitle().equals(docTitleDaFaldonare)) {
					continuaFlag = false;
					docTitleDaFaldonare = d.getDocumentTitle();

					// Si apre la dialog per la faldonatura del documento
					openDlgFaldonatura(d.getFascicoloProc());
					break;
				} else {
					wobNumbers.add(d.getWobNumber());
				}
			}
			if (continuaFlag) {
				wobNumbersAfterCheck = null;
				wobNumbersAfterCheck = wobNumbers;
				// Si puliscono gli esiti
				ldb.cleanEsiti();

				final GestioneLockDTO gestLockDTO = documentoSRV.getLockDocumentoMassivo(docTitleList, utente.getIdAoo(), 1, utente.getId());
				Boolean nessunLockPresente = gestLockDTO.getNessunLock();
				if (nessunLockPresente != null && !nessunLockPresente) {
					nessunLockPresente = nessunLockPresente || gestLockDTO.isInCarico();
				}

				if (nessunLockPresente != null && !nessunLockPresente) {
					nomeUtenteLockatore = gestLockDTO.getUsernameUtenteLock();
					FacesHelper.update("centralSectionForm:sbloccaDocumentoAttiDlg");
					FacesHelper.executeJS("PF('sbloccaDocumentoAttiDlg_WV').show();");
				} else {
					continuaAttiAfterCheckLock(wobNumbers);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_MESSA_AGLI_ATTI_MSG, e);
			showError(ERROR_MESSA_AGLI_ATTI_MSG);
		}
	}

	/**
	 * Consente di completare la procedura di messa agli atti.
	 */
	public void continuaAttiAfterCheckLock() {
		continuaAttiAfterCheckLock(wobNumbersAfterCheck);
	}

	/**
	 * Esegue la messa agli atti dei documenti associati ai wob number in ingresso.
	 * 
	 * @param wobNumbers
	 */
	private void continuaAttiAfterCheckLock(final Collection<String> wobNumbers) {
		final IAttiFacadeSRV attiSRV = ApplicationContextProvider.getApplicationContext().getBean(IAttiFacadeSRV.class);
		Collection<EsitoOperazioneDTO> eOpe;
		// Si invoca il servizio di messa agli atti
		eOpe = attiSRV.mettiAgliAtti(wobNumbers, utente, motivoAtti);
		final IDocumentoRedFacadeSRV documentoRedSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoRedFacadeSRV.class);
		for (final EsitoOperazioneDTO esito : eOpe) {
			if (esito.isEsito()) {
				final EsitoOperazioneDTO esitoRiattiva = documentoRedSRV.riattivaDocIngressoAfterChiusuraRisposta(esito.getDocumentTitle(), utente);
				if (esitoRiattiva != null && !esitoRiattiva.isEsito()) {
					showError(esitoRiattiva.getNote());
				}
			}
		}

		// Si impostano gli esiti con i nuovi
		ldb.getEsitiOperazione().addAll(eOpe);

		ldb.destroyBeanViewScope(ConstantsWeb.MBean.ATTI_BEAN);

		FacesHelper.executeJS("PF('dlgGeneric').hide()");

		// Si aggiorna la dialog contenente gli esiti
		FacesHelper.update("centralSectionForm:idDlgShowResult");

		// Si richiama la visualizzazione della dialog
		FacesHelper.executeJS("PF('dlgShowResult').show()");
	}

	/**
	 * Restituisce il nome dell'utente che ha effettuato un lock sulla attività.
	 * 
	 * @return nomeUtenteLockatore
	 */
	public String getNomeUtenteLockatore() {
		return nomeUtenteLockatore;
	}

	/**
	 * Imposta il nome dell'utente che ha effettuato il lock.
	 * 
	 * @param nomeUtenteLockatore
	 */
	public void setNomeUtenteLockatore(final String nomeUtenteLockatore) {
		this.nomeUtenteLockatore = nomeUtenteLockatore;
	}

	/**
	 * Restituisce la lista dei wobnumber after check.
	 * 
	 * @return wobNumbersAfterCheck
	 */
	public Collection<String> getWobNumbersAfterCheck() {
		return wobNumbersAfterCheck;
	}

	/**
	 * Impsota la lista dei wobnumber after check.
	 * 
	 * @param wobNumbersAfterCheck
	 */
	public void setWobNumbersAfterCheck(final Collection<String> wobNumbersAfterCheck) {
		this.wobNumbersAfterCheck = wobNumbersAfterCheck;
	}

}