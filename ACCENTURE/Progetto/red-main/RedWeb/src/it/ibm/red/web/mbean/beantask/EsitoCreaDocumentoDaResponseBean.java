package it.ibm.red.web.mbean.beantask;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.mbean.AbstractBean;

/**
 * Bean esito creazione documenti da response.
 */
@Named(MBean.ESITO_CREA_DOC_DA_RESPONSE_BEAN)
@ViewScoped
public class EsitoCreaDocumentoDaResponseBean extends AbstractBean {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -8368096623186064853L;
	
    /**
	 * Messaggi.
	 */
	private Map<String, String> messaggiEsito;
	
	/**
	 * PostConstruct.
	 */
	@Override
	@PostConstruct
	public void postConstruct() {
		messaggiEsito = new HashMap<>();
	}
	
	/**
	 * Recupera l'esito dei messaggi.
	 * @return esito dei messaggi
	 */
	public Map<String, String> getMessaggiEsito() {
		return messaggiEsito;
	}

	/**
	 * Imposta l'esito dei messaggi.
	 * @param messaggiEsito esito dei messaggi
	 */
	public void setMessaggiEsito(final Map<String, String> messaggiEsito) {
		this.messaggiEsito = messaggiEsito;
	}
}
