package it.ibm.red.web.mbean;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.LookupTable;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;
import it.ibm.red.business.utils.CollectionUtils;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * @author SimoneLungarella
 * 
 *         Bean per la gestione della pagina per il caricamento massivo delle
 *         lookuptable
 */

@Named(ConstantsWeb.MBean.CARICAMENTO_MASSIVO_BEAN)
@ViewScoped
public class CaricamentoMassivoBean extends AbstractBean {

	private static final long serialVersionUID = -126935954423748895L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CaricamentoMassivoBean.class.getName());

	/**
	 * Informazione utente.
	 */
	private UtenteDTO utente;

	/**
	 * Datatable lista valori.
	 */
	private SimpleDetailDataTableHelper<LookupTable> lookupDTH;

	/**
	 * Servizio tipologia documento.
	 */
	private ITipologiaDocumentoFacadeSRV tipologiaDocumentoSRV;

	/**
	 * File.
	 */
	private transient UploadedFile file;

	/**
	 * Nome del file.
	 */
	private String nomeFile;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		LOGGER.info(ConstantsWeb.MBean.CARICAMENTO_MASSIVO_BEAN + " ===> postConstruct");
		tipologiaDocumentoSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoFacadeSRV.class);
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		setLookupDTH(new SimpleDetailDataTableHelper<>());
		preparaMascheraCaricaAssegnazioniDaFile();
	}

	/**
	 * Prepara la maschera per il carimento massivo da file delle
	 * assegnazioniAutomatiche.
	 */
	public void preparaMascheraCaricaAssegnazioniDaFile() {
		file = null;
		setNomeFile("");
	}

	/**
	 * Gestione upload lookup table da file.
	 * 
	 * @param event
	 */
	public void gestisciUploadFileCaricamentoLookupT(final FileUploadEvent event) {
		if (event.getFile() != null && (event.getFile().getContentType().equalsIgnoreCase(Constants.ContentType.XLS)
				|| event.getFile().getContentType().equalsIgnoreCase(Constants.ContentType.XLSX))) {
			try {
				file = event.getFile();
				nomeFile = file.getFileName();
				lookupDTH = new SimpleDetailDataTableHelper<>();

				// Verifica della correttezza del file ed estrazione delle lookup table
				final List<LookupTable> lut = getLookupTablesFromFile(file.getContents());

				if (!CollectionUtils.isEmptyOrNull(lut)) {
					lookupDTH.setMasters(lut);
					showInfoMessage("Lookup table caricate con successo");
				} else {
					showWarnMessage("Nessuna lista utente valida esistente nel file scelto");
				}

			} catch (final Exception e) {
				showError(e);
			}
		} else {
			showError("Il file deve essere in formato Excel");
		}
	}

	private Workbook getWorkBook(final byte[] fileExcel) {
		Workbook out = null;
		try (ByteArrayInputStream bis = new ByteArrayInputStream(fileExcel)) {
			out = generateWorkbook(bis);
		} catch (final IOException e) {
			LOGGER.error(e);
		}
		return out;
	}

	/**
	 * Genera il workbook appropriato alla lettura del file.
	 * 
	 * @param bis
	 * @return workbook
	 * @throws IOException
	 */
	private Workbook generateWorkbook(final ByteArrayInputStream bis) throws IOException {
		try {
			return new XSSFWorkbook(bis);
		} catch (final Exception e) {
			LOGGER.warn("Errore nell'eliminazione", e);
			return new HSSFWorkbook(bis);
		}
	}

	/**
	 * Restituisce una lista di lookup table recuperate dal fileExcel. Se una lookup
	 * table presenta errori viene scartata, questo non influenza l'intera
	 * estrapolazione delle lookuptable.
	 * 
	 * @param fileExcel
	 * @return List di LookupTable generate a partire dalle informazioni leggibili
	 *         dal file
	 */
	public List<LookupTable> getLookupTablesFromFile(final byte[] fileExcel) {
		final List<LookupTable> lookups = new ArrayList<>();
		LookupTable lookuptable;
		Workbook workbook = null;
		final SimpleDateFormat sDataFormat = new SimpleDateFormat(DateUtils.DD_MM_YYYY);
		try {
			workbook = getWorkBook(fileExcel);

			if (workbook == null) {
				LOGGER.error("Errore recupero workbook dal file Excel.");
				throw new RedException();
			}

			final Sheet sheet = workbook.getSheetAt(0);
			final int firstRowIndex = 0;
			final int firstColumnIndex = 0;
			final int lastRowIndex = sheet.getLastRowNum();
			Row currentRow;

			for (int index = firstRowIndex; index <= lastRowIndex; index++) {
				currentRow = sheet.getRow(index);
				String nameSelettore = "";
				String displayName = "";
				String value = "";
				Date dataAtt = null;
				Date dataDisatt = null;

				// Selettore
				final Cell selectorCell = currentRow.getCell(firstColumnIndex);
				if (selectorCell != null && !CellType.BLANK.equals(selectorCell.getCellTypeEnum())) {
					selectorCell.setCellType(CellType.STRING);
					nameSelettore = selectorCell.getStringCellValue();
				} else {
					continue;
				}

				// Display name dell'item
				final Cell displayNameCell = currentRow.getCell(firstColumnIndex + 1);
				if (displayNameCell != null && !CellType.BLANK.equals(displayNameCell.getCellTypeEnum())) {
					displayNameCell.setCellType(CellType.STRING);
					displayName = displayNameCell.getStringCellValue();
				} else {
					continue;
				}

				// Valore dell'item
				final Cell valueCell = currentRow.getCell(firstColumnIndex + 2);
				if (valueCell != null && !CellType.BLANK.equals(valueCell.getCellTypeEnum())) {
					valueCell.setCellType(CellType.STRING);
					value = valueCell.getStringCellValue();
				} else {
					continue;
				}

				// Data attivazione selettore
				final Cell dataAttivazioneCell = currentRow.getCell(firstColumnIndex + 3);
				if (dataAttivazioneCell != null && !CellType.BLANK.equals(dataAttivazioneCell.getCellTypeEnum())) {
					// In excel le date sono di tipo numerico e formattate come date quindi viene
					// controllato prima che sia numerico e successivamente che sia formattato
					// correttamente
					if (CellType.NUMERIC.equals(dataAttivazioneCell.getCellTypeEnum()) && DateUtil.isCellDateFormatted(dataAttivazioneCell)) {
						final String temp = sDataFormat.format(dataAttivazioneCell.getDateCellValue());
						dataAtt = sDataFormat.parse(temp);
					} else {
						continue;
					}
				} else {
					continue;
				}

				// Data disattivazione selettore
				final Cell dataDisattivazioneCell = currentRow.getCell(firstColumnIndex + 4);
				if (dataDisattivazioneCell != null && !CellType.BLANK.equals(dataDisattivazioneCell.getCellTypeEnum())
						&& CellType.NUMERIC.equals(dataDisattivazioneCell.getCellTypeEnum())
						&& DateUtil.isCellDateFormatted(dataDisattivazioneCell)) {
					
					dataDisatt = dataDisattivazioneCell != null ? sDataFormat.parse(sDataFormat.format(dataDisattivazioneCell.getDateCellValue())) : null;
				}

				// Se tutti i parametri obbligatori sono stati recuperati correttamente, la
				// lookuptable viene creata ed aggiunta alla lista
				if (!StringUtils.isNullOrEmpty(nameSelettore) && !StringUtils.isNullOrEmpty(displayName) && !StringUtils.isNullOrEmpty(value) && dataAtt != null) {
					lookuptable = new LookupTable(0L, nameSelettore, displayName, value, dataAtt, dataDisatt, utente.getIdAoo());
					lookups.add(lookuptable);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore riscontrato durante la lettura del file", e);
			throw new RedException("Errore durante la lettura del file, verificare che rispetti il formato richiesto");
		}

		return lookups;
	}

	/**
	 * Memorizza le Lookup Table sul database a partire dalle Lookup Table generate
	 * dal file.
	 */
	public void salvaLookups() {
		final List<LookupTable> lts = new ArrayList<>(lookupDTH.getMasters());
		tipologiaDocumentoSRV.saveLookupTables(lts);
		showInfoMessage("Liste utente salvate correttamente");
		setLookupDTH(new SimpleDetailDataTableHelper<>());
	}

	/**
	 * Reinizializza il datatable helper in modo che vengano perse tutte le
	 * informazioni sui master.
	 */
	public void rimuoviTutti() {
		setLookupDTH(new SimpleDetailDataTableHelper<>());
	}

	/**
	 * Restituisce l'helper per la gestione delle lookup tables.
	 * 
	 * @return lookupDTH
	 */
	public SimpleDetailDataTableHelper<LookupTable> getLookupDTH() {
		return lookupDTH;
	}

	/**
	 * Imposta l'helper del datatable delle lookup tables.
	 * 
	 * @param lookupDTH
	 */
	public void setLookupDTH(final SimpleDetailDataTableHelper<LookupTable> lookupDTH) {
		this.lookupDTH = lookupDTH;
	}

	/**
	 * Restituisce il file selezionato dall'utente in fase di caricamento massivo.
	 * 
	 * @return il file se caricato, null altrimenti
	 */
	public UploadedFile getFile() {
		return file;
	}

	/**
	 * Imposta il file da cui estrarre informazioni sulle lookup tables.
	 * 
	 * @param fileNuoveAssegnazioni
	 */
	public void setFile(final UploadedFile fileNuoveAssegnazioni) {
		this.file = fileNuoveAssegnazioni;
	}

	/**
	 * Restituisce il nome del file caricato dall'utente.
	 * 
	 * @return nome file se esistente, null altrimenti
	 */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * Imposta il nome del file da cui caricare le lookup tables.
	 * 
	 * @param nomeFileNuoveAssegnazioni
	 */
	public void setNomeFile(final String nomeFileNuoveAssegnazioni) {
		this.nomeFile = nomeFileNuoveAssegnazioni;
	}

}
