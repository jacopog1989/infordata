package it.ibm.red.web.mbean;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.RegistroDTO;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.enums.TipoRegistroAusiliarioEnum;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.mbean.DocumentManagerBean;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Classe PredisponiDaRegistroAusiliarioBean.
 */
@Named(ConstantsWeb.MBean.PREDISPONI_DA_REGISTRO_AUSILIARIO_BEAN)
@ViewScoped
public class PredisponiDaRegistroAusiliarioBean extends AbstractPredisponiDaRegistroAusiliarioBean {
 
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -4655033224620091014L;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		super.init();
	}


	/**
	 * Metodo predisponi che viene chiamato lato front end per la response predisponi.
	 */
	@Override
	public void predisponi() {
		if (StringUtils.isNullOrEmpty(getNomeFile())) {
			showWarnMessage("Il nome del documento è obbligatorio");
		} else {
			// Si genera nuovamente il content, perché potrebbe essere non allineato alle ultime modifiche effettuate in maschera
			aggiornaAnteprima();
			//se aggiornaAntemprima va in errore, non visualizzo la preview
			if (!isVisualizzaAnteprima()) {
				return;
			}
			
			//Aggiorna dati registrazione ausiliaria
			updateDatiRegistrazioneAusiliaria();
			
			// Si aggiorna la maschera di creazione/modifica
			final DocumentManagerBean dmb = FacesHelper.getManagedBean(MBean.DOCUMENT_MANAGER_BEAN);
			dmb.aggiornaDocumentoDaTemplatePredisponi(getDocPrincipale(), getIdRegistro(), getListaMetadatiRegistro());
			
			// Si nasconde la dialog della response
			FacesHelper.executeJS("PF('dlgTemplatePredisponi').hide()");
		}
	}

	/**
	 * Metodo che aggiorna l'anteprima da predisposizione.
	 */
	public void aggiornaAnteprima() {
		aggiornaAnteprima(false);
	}

	/**
	 * Chiudi.
	 */
	@Override
	public void chiudi() {
		// Non deve fare nulla
	}

	/**
	 * Permette di inizializzare la maschera da dettaglio esteso.
	 *
	 * @param sottoCategoriaDoc
	 *            sottocategoria documento
	 * @param nomeFile
	 *            nome file
	 * @param idRegistroAusiliario
	 *            id del registro ausiliario
	 * @param metadatiRegistroAusiliario
	 *            i metadati del registro ausiliario
	 * @param inDetailEntrata
	 *            dettaglio entrata
	 * @param documentTitleUscita
	 *            document title uscita
	 */
	public void initDaDettaglioEsteso(final SottoCategoriaDocumentoUscitaEnum sottoCategoriaDoc, final String nomeFile, final Integer idRegistroAusiliario, 
			final Collection<MetadatoDTO> metadatiRegistroAusiliario,  final DetailDocumentRedDTO inDetailEntrata, final String documentTitleUscita) {
		// Ottengo i registri di tipo VISTO o OSSERVAZIONE o RICHIESTA INTEGRAZIONI in base alla sotto categoria del documento in uscita
		// La tipologia documento deve essere quella del documento in entrata
 		Collection<RegistroDTO> ts = null;
		switch (sottoCategoriaDoc) {
		case VISTO:
		case RELAZIONE_POSITIVA:
			ts = getRegistroAusiliarioSRV().getRegistri(TipoRegistroAusiliarioEnum.VISTO, inDetailEntrata.getIdTipologiaDocumento(), inDetailEntrata.getIdTipologiaProcedimento());
			break;
		case OSSERVAZIONE:
		case RELAZIONE_NEGATIVA:
			ts = getRegistroAusiliarioSRV().getRegistri(TipoRegistroAusiliarioEnum.OSSERVAZIONE_RILIEVO, inDetailEntrata.getIdTipologiaDocumento(), inDetailEntrata.getIdTipologiaProcedimento());
			break;
		case RICHIESTA_INTEGRAZIONI:
			ts = getRegistroAusiliarioSRV().getRegistri(TipoRegistroAusiliarioEnum.RICHIESTA_ULTERIORI_INFORMAZIONI, inDetailEntrata.getIdTipologiaDocumento(), inDetailEntrata.getIdTipologiaProcedimento());
			break;
		case RESTITUZIONE:
			ts = getRegistroAusiliarioSRV().getRegistri(TipoRegistroAusiliarioEnum.RESTITUZIONI, inDetailEntrata.getIdTipologiaDocumento(), inDetailEntrata.getIdTipologiaProcedimento());
			break;
		default:
			break;
		}
		
		super.initListaRegistri(ts, idRegistroAusiliario);
		
		// Il document title è usato per la generazione del template e dev'essere quello del documento in entrata
		setDocumentTitleIngresso(inDetailEntrata.getDocumentTitle());
		
		setDocumentTitleUscita(documentTitleUscita);

		// Inizializzazione della maschera a partire dal dettaglio
		setNomeFile(nomeFile);
		setIdRegistro(idRegistroAusiliario);
		setListaMetadatiRegistro(metadatiRegistroAusiliario);
		initListaMetadati();
		// Si genera l'anteprima
		aggiornaAnteprima();
	}
	
}