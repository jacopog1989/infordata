package it.ibm.red.web.mbean.widgets;
 
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.ScheduleEvent;

import it.ibm.red.business.dto.EventoDTO;
import it.ibm.red.business.enums.EventoCalendarioEnum;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractCalendario; 

/**
 * Bean che gestisce il widget calendar.
 */
@Named(ConstantsWeb.MBean.CALENDARIO_WIDGET_BEAN)
@ViewScoped
public class CalendarioWidgetBean extends AbstractCalendario {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -6079781053466032519L;
  
	/**
	 * Tipo evento selezionato.
	 */
	private EventoCalendarioEnum tipoEventoSelected;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		 super.postConstruct();

	}
   
	/**
	 * @return the eventoSelected
	 */
	public final EventoCalendarioEnum getTipoEventoSelected() {
		return tipoEventoSelected;
	}

	/**
	 * @param eventoSelected the eventoSelected to set
	 */
	public final void setTipoEventoSelected(final EventoCalendarioEnum tipoEventoSelected) {
		this.tipoEventoSelected = tipoEventoSelected;
	}

	/**
	 * Gestisce la selezione della data associata al widget.
	 * @param selectEvent
	 */
	public void onDateSelectWidget(final SelectEvent selectEvent) {
		setEventoCreazione(new EventoDTO());
		 
		final ScheduleEvent event = new DefaultScheduleEvent("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject());
		getEventoCreazione().setDataInizio(event.getStartDate());
		getEventoCreazione().setDataScadenza(event.getEndDate());
		FacesHelper.executeJS("PF('creationDialogWidget').show()");
	} 
	
 
}