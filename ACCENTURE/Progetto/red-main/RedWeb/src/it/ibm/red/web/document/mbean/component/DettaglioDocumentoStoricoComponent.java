package it.ibm.red.web.document.mbean.component;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.google.common.net.MediaType;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.ApprovazioneDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.StoricoGuiDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.StepDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.helper.timeline.TimelineHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.helper.faces.MessageSeverityEnum;

/**
 * Componente utilizzato per mostrare il tab storico del dettaglio.
 * 
 * @author a.difolca
 *
 */
public class DettaglioDocumentoStoricoComponent extends AbstractComponent {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 8497429969075302504L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DettaglioDocumentoStoricoComponent.class);

	/**
	 * Suffisso degli id a cui appendere il javascript In questo modo possiamo
	 * indicare esattamente a quale div appendere la pagina.
	 */
	private String idSuffix;

	/**
	 * Flag valido.
	 */
	private boolean valid = false;

	/**
	 * Dettaglio documento.
	 */
	private DetailDocumentRedDTO detail;

	/**
	 * Informazioni utente.
	 */
	private UtenteDTO utente;

	/**
	 * Attività.
	 */
	private transient List<StreamedContent> activities;

	/**
	 * Content.
	 */
	private transient StreamedContent activityContent;

	/**
	 * Storico.
	 */
	private Collection<StoricoGuiDTO> listaStorico;

	/**
	 * Costruttore del component.
	 * 
	 * @param idSuffix
	 */
	public DettaglioDocumentoStoricoComponent(final String idSuffix) {
		this.idSuffix = idSuffix;
	}

	/**
	 * Costruttore che gestisce utente e suffisso.
	 * 
	 * @param utente
	 * @param idSuffix
	 */
	public DettaglioDocumentoStoricoComponent(final UtenteDTO utente, final String idSuffix) {
		this.utente = utente;
		this.idSuffix = idSuffix;
	}

	/**
	 * Lo storico viene aggiornato tramite una libreria di Jquery Quando lo storico
	 * viene aggiornato bisogna rilanciare questi script.
	 * 
	 * @param descrizioneIterApprovativo
	 * @param approvazioni
	 * @param steps
	 * @return
	 */
	public String getJavascript(final String descrizioneIterApprovativo, final List<ApprovazioneDTO> approvazioni, final List<StepDTO> steps) {
		final StringBuilder str = new StringBuilder();
		final TimelineHelper tlh = new TimelineHelper(steps, descrizioneIterApprovativo, approvazioni);

		final String jsonCompleto = tlh.getJSON();
		str.append("$('#timeline-container-basic-" + idSuffix + "').timelineMe({items:" + jsonCompleto + "});");

		final String jsonApprovazioni = tlh.getApprovazioniJSON();
		str.append("$('#timeline-container-basic-approvazioni-" + idSuffix + "').timelineMe({items:" + jsonApprovazioni + "});");

		str.append("tlHideDetail()");
		return str.toString();
	}

	/**
	 * @return javascript
	 */
	public String getJavascriptStandalone() {
		String javascript = Constants.EMPTY_STRING;

		if (detail == null) {
			LOGGER.error("E' necessario che le informazioni dello storico siano popolate");
		}

		if (isValid()) {
			// detail.getIterApprovativoSemaforo()
			final List<StoricoDTO> storico = (List<StoricoDTO>) detail.getStorico();
			if (storico != null && !storico.isEmpty()) {
				final String motivoAssegnazione = storico.get(0).getMotivoAssegnazione();
				javascript = getJavascript(motivoAssegnazione, detail.getApprovazioni(), detail.getStoricoSteps());
			}
		}

		return javascript;
	}

	/**
	 * Metodo per la realizzazione del report delle approvazioni.
	 * 
	 * @return stream contenente il report
	 */
	public StreamedContent getReportApprovazioni() {
		StreamedContent output = null;
		try {
			final String ispettoratoPreponente = detail.getUfficioMittente().getDescrizione();
			// preparazione del content con all'interno le informazioni sottoposte ad
			// approvazione
			final byte[] content = PdfHelper.createApprovazioniDocument(ispettoratoPreponente, detail.getNumeroDocumento(), detail.getApprovazioni());
			output = new DefaultStreamedContent(new ByteArrayInputStream(content), MediaType.PDF.toString(), "report_approvazioni_" + detail.getNumeroDocumento() + ".pdf");
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di generazione del report delle approvazioni.", e);
			FacesHelper.showMessage(null, e.getMessage(), MessageSeverityEnum.ERROR);
		}

		return output;
	}

	/**
	 * Inizializza lo storico, come se fosse stato appena creato.
	 */
	public void unset() {
		this.valid = false;
		this.detail = null;
	}

	/**
	 * Restituisce il dettaglio del documento.
	 * 
	 * @return detail
	 */
	public DetailDocumentRedDTO getDetail() {
		return this.detail;
	}

	/**
	 * Imposta il dettaglio del documento.
	 * 
	 * @param detail
	 */
	public void setDetail(final DetailDocumentRedDTO detail) {
		this.detail = detail;
	}

	/**
	 * Imposta la validità in base alla data di creazione passata come parametro.
	 * 
	 * @param createDate
	 */
	public void setValid(final Date createDate) {
		final Date storicoStart = new Date(Constants.Storico.START_DATE);
		this.valid = createDate != null && storicoStart.before(createDate);
	}

	/**
	 * Restituisce true se valido, false altrimenti.
	 * 
	 * @return valid
	 */
	public boolean isValid() {
		return this.valid;
	}

	/**
	 * Restituisce l'idSuffix.
	 * 
	 * @return idSuffix
	 */
	public String getIdSuffix() {
		return idSuffix;
	}

	/**
	 * Imposta l'idSuffix.
	 * 
	 * @param idSuffix
	 */
	public void setIdSuffix(final String idSuffix) {
		this.idSuffix = idSuffix;
	}

	/**
	 * Restituisce lo StreamedContent della stampiglia.
	 * 
	 * @return StreamedContent
	 */
	public StreamedContent getStampiglia() {
		StreamedContent sc = null;
		final IDocumentoFacadeSRV documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);

		try {
			final List<ApprovazioneDTO> approvazioneList = detail.getApprovazioni();

			if (CollectionUtils.isNotEmpty(detail.getApprovazioni())) { // Il secondo caso non si deve verificare il tab è disabilitato quando quando
																		// questa condizione è false
				try (InputStream io = documentoSRV.createPdfApprovazioniStampigliate(utente, detail.getNumeroDocumento(), detail.getDocumentTitle(), approvazioneList)) {
					sc = new DefaultStreamedContent(io, "application/pdf", "approvazioni_" + detail.getNumeroDocumento() + "_del_" + detail.getAnnoDocumento() + ".pdf");
				}

			} else {
				LOGGER.error("Tentativo di stampigliare una lista di approvazioni vuota");
				FacesHelper.showMessage(null, "Non sono presenti approvazioni da stampigliare", MessageSeverityEnum.ERROR);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			FacesHelper.showMessage(null, "Impossibile creare il file delle approvazioni stampigliate", MessageSeverityEnum.ERROR);
		}
		return sc;
	}

	/**
	 * Costruisce lo storico visuale.
	 */
	public void buildStoricoGui() {
		final Collection<StoricoDTO> storicoColl = detail.getStorico();
		final Collection<StoricoGuiDTO> newList = new ArrayList<>();
		setActivities(new ArrayList<>());

		for (final StoricoDTO s : storicoColl) {
			final StoricoGuiDTO temp = new StoricoGuiDTO(s);
			StreamedContent sc = null;
			if (s.getAttivita() != null) {
				sc = new DefaultStreamedContent(s.getAttivita(), MediaType.PDF.toString(), "attività_" + detail.getNumeroDocumento() + ".pdf");
				temp.setAttivitaDownloadable(true);
			} else {
				temp.setAttivitaDownloadable(false);
			}

			activities.add(sc);
			newList.add(temp);
		}

		setListaStorico(newList);

	}

	/**
	 * @param index
	 * @return activityContent
	 */
	public StreamedContent getActivityContent(final int index) {
		setActivityContent(getActivities().get(index));
		return this.activityContent;
	}

	/**
	 * Restituisce la lista della attivita.
	 * 
	 * @return activities
	 */
	public List<StreamedContent> getActivities() {
		return activities;
	}

	/**
	 * Imposta la lista delle attivita.
	 * 
	 * @param activities
	 */
	public void setActivities(final List<StreamedContent> activities) {
		this.activities = activities;
	}

	/**
	 * Restituisce la lista dello storico GUI.
	 * 
	 * @return lista storico
	 */
	public Collection<StoricoGuiDTO> getListaStorico() {
		return listaStorico;
	}

	/**
	 * Imposta la lista dello storico GUI.
	 * 
	 * @param listaStorico
	 */
	public void setListaStorico(final Collection<StoricoGuiDTO> listaStorico) {
		this.listaStorico = listaStorico;
	}

	/**
	 * @param activityContent
	 */
	public void setActivityContent(final StreamedContent activityContent) {
		this.activityContent = activityContent;
	}

}