package it.ibm.red.web.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import it.ibm.red.business.dto.ProvinciaDTO;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IProvinciaFacadeSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * Classe ProvinciaConverter.
 */
@FacesConverter("provinciaConverter")
public class ProvinciaConverter implements Converter {

	/** 
	 * Restituisce la provincia durante l'autocomplete
	 * @param fc the fc
	 * @param uic the uic
	 * @param value the value
	 * @return the as object
	 */
	@Override
	public Object getAsObject(final FacesContext fc, final UIComponent uic, final String value) {
		final IProvinciaFacadeSRV provinciaSRV = ApplicationContextProvider.getApplicationContext().getBean(IProvinciaFacadeSRV.class);
		 if (StringUtils.isNullOrEmpty(value)) {
			 return null;
		 } else {
		 return provinciaSRV.get(value);
		}
	}

	/** 
	 * Restituisce la provincia durante l'autocomplete
	 * @param fc the fc
	 * @param uic the uic
	 * @param object the object
	 * @return the as string
	 */
	@Override
	public String getAsString(final FacesContext fc, final UIComponent uic, final Object object) {
		if (object instanceof ProvinciaDTO) {
			final ProvinciaDTO provincia = (ProvinciaDTO) object;
			return provincia.getIdProvincia();
		}
		return object.toString();
	}

}
