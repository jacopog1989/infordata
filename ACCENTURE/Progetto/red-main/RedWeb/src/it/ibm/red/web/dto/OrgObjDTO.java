/**
 * 
 */
package it.ibm.red.web.dto;

import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;

/**
 * DTO utilizato per la nagivazione dall'organigramma Scrivania
 *
 * @author APerquoti
 */
public class OrgObjDTO extends AbstractDTO {
	/**
	 * serial version UID.
	 */
	private static final long serialVersionUID = -3804111615962925774L;

	/**
	 * Nodo selezionato dall'utente.
	 * Necessario per ricostruire l'albero quando viene cambiata la pagina.
	 */
	private transient TreeNode node;
	
	/**
	 * Coda selelzionata dall'utente.
	 * Qualora il tipo di dato del node sia di tipo 'CODA' questo campo è valorizzato.
	 */
	private DocumentQueueEnum queueToGo;
	
	/**
	 * Rappresentazioni differenti se il node ha tipo CODA o di tipo USER.
	 * USER: Corrisponde al nodo utente.
	 * CODA: Rappresenta il nodo ufficio che contiene la coda.
	 */
	private NodoOrganigrammaDTO nodoPadre;
	
	/**
	 * Costruttore vuoto.
	 */
	public OrgObjDTO() {
		super();
	}

	/**
	 * Costruttore di default.
	 * @param inQueueToGo
	 * @param inNodoPadre
	 */
	public OrgObjDTO(final DocumentQueueEnum inQueueToGo, final NodoOrganigrammaDTO inNodoPadre) {
		this.queueToGo = inQueueToGo;
		this.nodoPadre = inNodoPadre;
	}

	/**
	 * @return the queueToGo
	 */
	public final DocumentQueueEnum getQueueToGo() {
		return queueToGo;
	}

	/**
	 * @return the datiAccesso
	 */
	public final NodoOrganigrammaDTO getDatiAccesso() {
		return nodoPadre;
	}

	/**
	 * Ritorna il nodo
	 */
	public TreeNode getNode() {
		return node;
	}
	
	/**
	 * Set del nodo
	 * @param node
	 *  nodo da cui sono state selezionati gli elementi
	 */
	public void setNode(final TreeNode node) {
		this.node = node;
	}
}
