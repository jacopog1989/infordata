package it.ibm.red.web.document.mbean.component;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.activation.DataHandler;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.ITipologiaDocumentoSRV;
import it.ibm.red.business.service.facade.IAllegatoFacadeSRV;
import it.ibm.red.business.service.facade.IFascicoloFacadeSRV;
import it.ibm.red.web.document.mbean.interfaces.IAllegatiDaAllacci;
import it.ibm.red.web.mbean.interfaces.DocumentiAllegabiliSelectableInterface;

/**
 * Component allegati da allacci.
 */
public class AllegatiDaAllacciComponent extends AbstractComponent implements IAllegatiDaAllacci {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 2438652075817253599L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AllegatiDaAllacciComponent.class);

	/**
	 * Servizio fasicolo.
	 */
	private final IFascicoloFacadeSRV fascicoloSRV;

	/**
	 * Servizio allegato.
	 */
	private final IAllegatoFacadeSRV allegatoSRV;

	/**
	 * Servizio tipologia documento.
	 */
	private final ITipologiaDocumentoSRV tipologiaDocumentoSRV;

	/**
	 * Utente.
	 */
	private final UtenteDTO utente;

	/**
	 * Datatable documenti allegabili.
	 */
	private List<DocumentiAllegabiliSelectableTab> allegabiliDatatable;

	/**
	 * Tipologie documento attive.
	 */
	private List<TipologiaDocumentoDTO> tipologieDocAttive;

	/**
	 * Costruttore di classe.
	 * 
	 * @param inUtente
	 */
	public AllegatiDaAllacciComponent(final UtenteDTO inUtente) {
		fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloFacadeSRV.class);
		allegatoSRV = ApplicationContextProvider.getApplicationContext().getBean(IAllegatoFacadeSRV.class);
		tipologiaDocumentoSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoSRV.class);
		this.utente = inUtente;
		allegabiliDatatable = new ArrayList<>();
	}

	/**
	 * Metodo da implementare all'apertura della dialog contente le informazioni
	 * necessarie alla selezione dei documenti allegabili.
	 * 
	 * @param documentTitleString
	 * @param documentTitleAllacci
	 * @param allegatiDaAllacciToSelect
	 */
	public void init(final String documentTitleString, final Collection<String> documentTitleAllacci, final List<AllegatoDTO> allegatiDaAllacciToSelect) {
		try {
			final Integer documentTitle = StringUtils.isNotBlank(documentTitleString) ? Integer.valueOf(documentTitleString) : null;
			final List<Integer> idAllacci = documentTitleAllacci.stream().map(allacciDocumentTitleString -> Integer.valueOf(allacciDocumentTitleString))
					.collect(Collectors.toList());

			final Map<FascicoloDTO, List<DocumentoAllegabileDTO>> fascicolo2Allegabili = fascicoloSRV.getDocumentiAllegabiliMap(utente, idAllacci, documentTitle,
					allegatiDaAllacciToSelect, false);

			allegabiliDatatable = new ArrayList<>();
			for (final Entry<FascicoloDTO, List<DocumentoAllegabileDTO>> entry : fascicolo2Allegabili.entrySet()) {
				final DocumentiAllegabiliSelectableTab allengabiliTab = new DocumentiAllegabiliSelectableTab(entry.getKey(), entry.getValue(), utente);
				allegabiliDatatable.add(allengabiliTab);
			}

			tipologieDocAttive = tipologiaDocumentoSRV.getComboTipologieByAooAndTipoCategoriaAttivi(TipoCategoriaEnum.USCITA, utente.getIdAoo());
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero della mappa fascicoli documenti allegabili per gli allacci:" + documentTitleAllacci.toString() + " per il documentTitle: "
					+ documentTitleString, e);
			showErrorMessage("Errore durante il recupero degli allacci allegabili.");
		}
	}

	/**
	 * Libera le risorse e restituisce le varie informazioni legate alla selezione
	 * dell'utente. Affinche il bean padre le possa visualizzare.
	 * 
	 * Qualora ritorni null la validazione non è andata a buon fine.
	 * 
	 * @return List di AllegatoDTO
	 */
	public List<AllegatoDTO> registra() {
		List<AllegatoDTO> allegati = new ArrayList<>();
		
		try {
			final List<DocumentoAllegabileDTO> allegabiliList = new ArrayList<>();
			for (final DocumentiAllegabiliSelectableTab dt : allegabiliDatatable) {
				final List<DocumentoAllegabileDTO> allegabili = dt.getSelectedAllegabili();
				if (allegabili != null) {
					allegabiliList.addAll(allegabili);
				}
			}
			if (validate(allegabiliList)) {
				reset();
				fascicoloSRV.loadContent4DocumentiAllegabili(utente, allegabiliList);
				allegati = allegatoSRV.transformFrom(allegabiliList, tipologieDocAttive);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la chiusura del dialog allegati da allacci.", e);
			showErrorMessage("Si è verificato un'errore durante il recupero degli allacci.");
		}

		return allegati;
	}

	private boolean validate(final List<DocumentoAllegabileDTO> allegatiList) {
		boolean valid = true;
		if (allegatiList.isEmpty()) {
			showWarnMessage("Attenzione, è necessario selezionare almeno un procedimento da allegare.");
			valid = false;
		}
		return valid;
	}

	/**
	 * Re-inizializza la lista allegabiliDatatable, effettuando un reset.
	 */
	public void reset() {
		allegabiliDatatable = new ArrayList<>();
	}

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.IAllegatiDaAllacci#getDimensioneSelected().
	 */
	@Override
	public BigDecimal getDimensioneSelected() {
		return allegabiliDatatable.stream().map(allegabile -> allegabile.getDimensioneSelected()).reduce(new BigDecimal(0), (subtotal, element) -> subtotal.add(element));
	}

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.IAllegatiDaAllacci#getAllegabiliDatatable().
	 */
	@Override
	public List<? extends DocumentiAllegabiliSelectableInterface> getAllegabiliDatatable() {
		return allegabiliDatatable;
	}

	/**
	 * Permette di scaricare il documento allegabile.
	 */
	@Override
	public StreamedContent download(final DocumentoAllegabileDTO doc) {
		StreamedContent content = null;
		try {
			fascicoloSRV.loadContent4DocumentiAllegabili(utente, Arrays.asList(doc));
			final DataHandler contentHandler = doc.getContenuto();
			if (contentHandler == null) {
				throw new RedException("Non è stato trovato nessun contenuto nel documento.");
			}
			final InputStream stream = contentHandler.getInputStream();
			final String contentType = contentHandler.getContentType();
			final String name = doc.getNomeFile();
			content = new DefaultStreamedContent(stream, contentType, name);
		} catch (final Exception e) {
			final String docTitle = doc == null ? null : doc.getDocumentTitle();
			LOGGER.error("Impossibile scaricare l'allegabile " + docTitle, e);
			showErrorMessage("Impossibile scaricare il documento.");
		}
		return content;
	}
}
