package it.ibm.red.web.mbean.component;

import java.util.List;

import org.primefaces.model.TreeNode;

import it.ibm.red.web.document.mbean.component.AbstractComponent;
import it.ibm.red.web.mbean.interfaces.IPopupComponent;

/**
 * Component.
 */
public class ConfermaAllegatiProtocollazioneCollassabileComponent extends AbstractComponent implements IPopupComponent {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 6072624021273253613L;

	/**
	 * Tree table.
	 */
	private SelezionaPrincipaleAllegatoComponent treeTable;
	
	/**
	 * Principale precedente selezionato.
	 */
	private Integer oldSelectedPrincipale;

	/**
	 * Allegati precedente selezionati.
	 */
	private List<Integer> oldSelectedAllegati;

	/**
	 * Nodo collassato.
	 */
	private transient TreeNode collassato;	

	/**
	 * @param inTreeTable
	 * @param inCollassato
	 */
	public void setCollassabile(final SelezionaPrincipaleAllegatoComponent inTreeTable, final TreeNode inCollassato) {
		treeTable = inTreeTable;
		this.oldSelectedPrincipale = inTreeTable.getSelectedPrincipale();
		this.oldSelectedAllegati = inTreeTable.getSelectedAllegatiList();
		collassato = inCollassato;
	}

	/**
	 * Gestisce la conferma.
	 */
	@Override
	public void conferma() {
		close();
	}
	
	/**
	 * Gestisce la chiusura.
	 */
	private void close() {
		treeTable = null;
		oldSelectedAllegati = null;
		oldSelectedPrincipale = null;
		collassato = null;
	}

	/**
	 * Gestisce l'annullamento.
	 */
	@Override
	public void annulla() {
		treeTable.setSelectedPrincipale(oldSelectedPrincipale);
		treeTable.setSelectedAllegatiList(oldSelectedAllegati);
		collassato.setExpanded(true);
		close();
	}

	/**
	 * @return oldSelectedPrincipale
	 */
	public Integer getOldSelectedPrincipale() {
		return oldSelectedPrincipale;
	}

	/**
	 * @param oldSelectedPrincipale
	 */
	public void setOldSelectedPrincipale(final Integer oldSelectedPrincipale) {
		this.oldSelectedPrincipale = oldSelectedPrincipale;
	}

	/**
	 * @return the oldSelectedAllegati
	 */
	public List<Integer> getOldSelectedAllegati() {
		return oldSelectedAllegati;
	}

	/**
	 * @param oldSelectedAllegati the oldSelectedAllegati to set
	 */
	public void setOldSelectedAllegati(final List<Integer> oldSelectedAllegati) {
		this.oldSelectedAllegati = oldSelectedAllegati;
	}

	/**
	 * @return collassato
	 */
	public TreeNode getCollassato() {
		return collassato;
	}

	/**
	 * @param collassato
	 */
	public void setCollassato(final TreeNode collassato) {
		this.collassato = collassato;
	}
}
