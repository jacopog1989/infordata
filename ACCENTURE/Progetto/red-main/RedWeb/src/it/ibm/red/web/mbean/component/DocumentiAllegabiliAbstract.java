package it.ibm.red.web.mbean.component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.document.mbean.component.AbstractComponent;
import it.ibm.red.web.mbean.interfaces.DocumentiAllegabiliSelectableInterface;

/**
 * Abstract documenti allegabili.
 */
public abstract class DocumentiAllegabiliAbstract extends AbstractComponent implements DocumentiAllegabiliSelectableInterface {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DocumentiAllegabiliAbstract.class);
	
	/**
	 * Fascicolo in cui si trovano tutti i documentiAllegabili.
	 */
	private final FascicoloDTO fascicolo;
	
	/**
	 * Documenti allegabili tra i quelli l'utente puo'scegliere.
	 */
	private List<DocumentoAllegabileDTO> documentiAllegabili;
	
	/**
	 * Selezione degli allegabili.
	 */
	private List<DocumentoAllegabileDTO> selectedAllegabili;
	
	/**
	 * Dimensione.
	 */
	private BigDecimal dimensioneSelected;
	
	/**
	 * Allegato.
	 */
	private DocumentManagerDettaglioVersioniAllegatiComponent allegato;
	
	/**
	 * Dimensione massima che gli allegati possono raggiungere.
	 * Da una indicazione all'utente sul massimale della dimensione selezionabile.
	 * Le modalità in cui questa dimensione ha peso varia dipendentemente dalle implemntazioni.
	 */
	private final BigDecimal dimensioneTotale;
	
	/**
	 * Costruttore.
	 * @param fascicoloProcedimentale
	 * @param documentiAllegabili
	 * @param inDimensioneTotale
	 */
	protected DocumentiAllegabiliAbstract(final FascicoloDTO fascicoloProcedimentale, final List<DocumentoAllegabileDTO> documentiAllegabili, final BigDecimal inDimensioneTotale) {
		this.fascicolo = fascicoloProcedimentale;
		this.documentiAllegabili = documentiAllegabili;
		this.selectedAllegabili = new ArrayList<>();
		this.dimensioneTotale = inDimensioneTotale;
	}
	
	/**
	 * Restituisce la dimensione dell'allegato selezionato.
	 * @return dimensione
	 */
	@Override
	public BigDecimal getDimensioneSelected() {
		dimensioneSelected = getDimensione(this.selectedAllegabili); 
		return dimensioneSelected;
	}
	
	/**
	 * Restituisce la dimensione totale.
	 * @return dimensione totale
	 */
	@Override
	public BigDecimal getDimensioneTotale() {
		return dimensioneTotale;
	}
	
	/**
	 * Restituisce true se la dimensione è stata superata, false altrimenti.
	 * 
	 * @return true se la dimensione è stata superata, false altrimenti
	 */
	@Override
	public boolean isDimensioneSuperata() {
		if (dimensioneSelected == null) {
			getDimensioneSelected();
		}
		return dimensioneSelected.compareTo(getDimensioneTotale()) > 0;
	}
	
	/**
	 * Calcola la dimenzione degli allegati selezionati come somma della dimensione di ciascun allegato selezionato.
	 * @param list
	 * @return
	 */
	protected BigDecimal getDimensione(final List<DocumentoAllegabileDTO> list) {
		BigDecimal counter = new BigDecimal(0);
		try {
			if (!CollectionUtils.isEmpty(list)) {
				for (final DocumentoAllegabileDTO d : list) {
					final BigDecimal current = d.getDimensione();
					counter = counter.add(current);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showErrorMessage("Si e' verificato un errore nel conteggio della dimensione dei documenti allegabili.");
		}
		return counter;
	}
	
	/**
	 * Restituisce i documenti allegabili selezionati.
	 * @return lista allegabili selezionati
	 */
	@Override
	public List<DocumentoAllegabileDTO> getSelectedAllegabili() {
		return selectedAllegabili;
	}

	/**
	 * Imposta gli allegati selezionati.
	 * 
	 * @param selectedAllegabili
	 *            allegati selezionabili selezionati
	 */
	@Override
	public void setSelectedAllegabili(final List<DocumentoAllegabileDTO> selectedAllegabili) {
		this.selectedAllegabili = selectedAllegabili;
	}

	/**
	 * Restituisce la lista dei documenti allegabili.
	 * @return documenti allegabili
	 */
	@Override
	public List<DocumentoAllegabileDTO> getDocumentiAllegabili() {
		return documentiAllegabili;
	}

	/**
	 * Imposta i documenti allegabili.
	 * 
	 * @param indocumentiAllegabili
	 *            documenti allegabili
	 */
	protected void setDocumentiAllegabili(final List<DocumentoAllegabileDTO> indocumentiAllegabili) {
		this.documentiAllegabili = indocumentiAllegabili;
	}

	/**
	 * Restituisce il fascicolo.
	 * @return fascicolo
	 */
	@Override
	public FascicoloDTO getFascicolo() {
		return fascicolo;
	}

	/**
	 * Restituisce l'allegato.
	 * @return allegato
	 */
	public DocumentManagerDettaglioVersioniAllegatiComponent getAllegato() {
		return allegato;
	}

	/**
	 * Imposta l'allegato.
	 * @param allegato
	 */
	public void setAllegato(final DocumentManagerDettaglioVersioniAllegatiComponent allegato) {
		this.allegato = allegato;
	}
}
