package it.ibm.red.web.mbean;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.google.common.net.MediaType;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.helper.adobe.AdobeLCHelper;
import it.ibm.red.business.helper.adobe.IAdobeLCHelper;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.TipoFile;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.ITipoFileSRV;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.constants.ConstantsWeb.SessionObject;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Component visualizzazione dettaglio file.
 */
public class VisualizzaDettaglioFilesComponent implements Serializable {

	private static final String BOUNDARY_TEXT_STRING = "----boundary_text_string\r\n";

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(VisualizzaDettaglioFilesComponent.class);

	/**
	 * Datatable documenti.
	 */
	private SimpleDetailDataTableHelper<FileDTO> docprincDTH;

	/**
	 * Datatable allegati.
	 */
	private SimpleDetailDataTableHelper<AllegatoDTO> allegatiDTH;

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Documento principale.
	 */
	private FileDTO documentoPrincipale;

	/**
	 * Adobe LC helper.
	 */
	private final IAdobeLCHelper alch;

	/**
	 * Flag con campo firma.
	 */
	private Boolean conCampoFirma;

	/**
	 * Flag converti.
	 */
	private Boolean converti;

	/**
	 * Flag modifica.
	 */
	private final boolean inModifica;

	/**
	 * Flag ingresso.
	 */
	private final boolean inIngresso;

	/**
	 * Flag formato originale.
	 */
	private boolean formatoOriginale;

	/**
	 * Flag convertibile.
	 */
	private boolean convertibile;

	/**
	 * Flag firma multipla.
	 */
	private final boolean firmaMultipla;

	/**
	 * Flag doc principale.
	 */
	private boolean principale;

	/**
	 * Flag da non protocollare.
	 */
	private final boolean daNonProtocollare;

	/**
	 * Identificativo siglatario.
	 */
	private Long idSiglatario;

	/**
	 * Identificativo ufficio creatore.
	 */
	private Integer idUffCreatore;

	/**
	 * Indetificativo tipo documento.
	 */
	private Integer idTipoDoc;

	/**
	 * Flag stampiglia sigla.
	 */
	private boolean stampigliaturaSigla;

	/**
	 * Servizio.
	 */
	private final ITipoFileSRV tipoFileSRV;

	/**
	 * Collezione tipi file.
	 */
	private final Collection<TipoFile> getAll;

	/**
	 * Flag da firmare.
	 */
	private boolean daFirmare;

	/**
	 * Costruttore di default.
	 * 
	 * @param inUtente
	 * @param inIsModifica
	 * @param inIsIngresso
	 * @param inFirmaMultipla
	 * @param inDaNonProtocollare
	 * @param inIdSiglatario
	 * @param inIdUffCreatore
	 * @param inIdTipoDoc
	 */
	public VisualizzaDettaglioFilesComponent(final UtenteDTO inUtente, final boolean inIsModifica, final boolean inIsIngresso, final boolean inFirmaMultipla,
			final Boolean inDaNonProtocollare, final Long inIdSiglatario, final Integer inIdUffCreatore, final Integer inIdTipoDoc) {
		docprincDTH = new SimpleDetailDataTableHelper<>();
		allegatiDTH = new SimpleDetailDataTableHelper<>();
		documentoPrincipale = null;
		conCampoFirma = false;
		converti = false;
		alch = new AdobeLCHelper();
		utente = inUtente;
		inModifica = inIsModifica;
		inIngresso = inIsIngresso;
		firmaMultipla = inFirmaMultipla;
		daNonProtocollare = (inDaNonProtocollare == null || Boolean.TRUE.equals(inDaNonProtocollare));
		tipoFileSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipoFileSRV.class);
		getAll = tipoFileSRV.getAll();
		idSiglatario = inIdSiglatario;
		idUffCreatore = inIdUffCreatore;
		idTipoDoc = inIdTipoDoc;
	}

	/**
	 * Imposta il DataTable dei documenti principali con il documento in ingresso e,
	 * se non nullo, lo imposta come master corrente.
	 * 
	 * @param docPrinc
	 */
	public void impostaDocumentoPrincipale(final FileDTO docPrinc) {
		// reset
		docprincDTH = new SimpleDetailDataTableHelper<>();

		if (docPrinc != null) {
			docprincDTH.setMasters(new ArrayList<>());
			docprincDTH.getMasters().add(docPrinc);
			docprincDTH.setCurrentMaster(docPrinc);
			documentoPrincipale = docPrinc;
			visualizzaFile(docPrinc, false, true, false);
		} else {
			docprincDTH.setMasters(new ArrayList<>());
		}
	}

	/**
	 * Imposta i masters del DataTable degli allegato con quelli in input.
	 * 
	 * @param allegati
	 */
	public void impostaDetailAllegati(final List<AllegatoDTO> allegati) {
		// reset
		allegatiDTH = new SimpleDetailDataTableHelper<>();

		final List<AllegatoDTO> allegatiMasters = new ArrayList<>();
		// creo duplicati dell'allegato per assegnare un id generato random senza
		// assegnarlo alla lista principale
		if (allegati != null) {
			for (final AllegatoDTO a : allegati) {
				final AllegatoDTO output = new AllegatoDTO(a);
				output.setDocumentTitle(UUID.randomUUID().toString());
				onOffGeneraAnteprimaBtn(output);
				onOffCampoFirmaBtn(output);
				allegatiMasters.add(output);
			}
		}
		allegatiDTH.setMasters(allegatiMasters);
		// Se non ho un file principale ancora, visualizzo il primo allegato
		if (docprincDTH.getMasters().isEmpty() && !allegatiMasters.isEmpty()) {
			allegatiDTH.setCurrentMaster(allegatiMasters.get(0));
			final AllegatoDTO allegatoSelezionato = allegatiDTH.getCurrentMaster();
			final FileDTO a = new FileDTO(allegatoSelezionato.getDocumentTitle(), allegatoSelezionato.getNomeFile(), allegatoSelezionato.getContent(),
					allegatoSelezionato.getMimeType(), false);
			final boolean mantieniFormatoOriginale = !FormatoAllegatoEnum.ELETTRONICO.equals(allegatoSelezionato.getFormatoSelected())
					|| allegatoSelezionato.getFormatoOriginale();
			final boolean toSign = allegatoSelezionato.getDaFirmare() != null && allegatoSelezionato.getDaFirmare() == 1;

			visualizzaFile(a, mantieniFormatoOriginale, false, toSign);
		}
	}

	/**
	 * Gestione abilitazione/disabilitazione del pulsante per la generazione
	 * dell'anteprima.
	 */
	private void onOffGeneraAnteprimaBtn(final AllegatoDTO allegato) {
		final boolean isConvertibile = tipoFileSRV.isConvertibile(allegato.getNomeFile(), getAll);

		boolean generaAnteprimaDisabled = true;

		if (allegato.getContent() != null) {
			generaAnteprimaDisabled = !(FormatoAllegatoEnum.ELETTRONICO.equals(allegato.getFormatoSelected()) 
					&& !allegato.getFormatoOriginale() && isConvertibile);
		}
		allegato.setGeneraAnteprimaDisabled(generaAnteprimaDisabled);
	}

	/**
	 * Gestione abilitazione/disabilitazione del pulsante per il campo firma.
	 */
	private void onOffCampoFirmaBtn(final AllegatoDTO allegato) {
		allegato.setCampoFirma(allegato.isDaFirmareBoolean()
				&& ((allegato.getFirmaVisibile() != null && allegato.getFirmaVisibile()) 
						|| (alch.hasTagFirmatario(allegato) != -1 && alch.hasTagFirmatario(allegato) != 0)));
	}

	/**
	 * Gestione del pulsante "lentina" (generazione anteprima) per il documento
	 * principale.
	 */
	public void generaAnteprimaDocPrinc() {
		if (documentoPrincipale != null && documentoPrincipale.getContent() != null) {
			boolean creaCampoFirma = false;
			if (!inIngresso) {
				creaCampoFirma = true;
			}
			generaAnteprima(documentoPrincipale, creaCampoFirma, true, false, true, false);
		}
		handleEmptyRow();
		docprincDTH.setCurrentMaster(documentoPrincipale);
	}

	/**
	 * Gestione del pulsante "lentina" (generazione anteprima) per gli allegati.
	 *
	 * @param index
	 */
	public void generaAnteprimaAllegato(final Object index) {
		final List<AllegatoDTO> allegati = (List<AllegatoDTO>) allegatiDTH.getMasters();
		final AllegatoDTO al = allegati.get((Integer) index);
		try {
			final FileDTO f = new FileDTO(al.getDocumentTitle(), al.getNomeFile(), al.getContent(), al.getMimeType(), false);
			if (f.getContent() != null) {
				final boolean mantieniFormatoOriginale = !FormatoAllegatoEnum.ELETTRONICO.equals(al.getFormatoSelected()) || al.getFormatoOriginale();
				final boolean toSign = al.getDaFirmare() != null && al.getDaFirmare() == 1;

				generaAnteprima(f, al.getCampoFirma(), true, mantieniFormatoOriginale, false,
						al.getStampigliaturaSigla(), idSiglatario, idUffCreatore, idTipoDoc, toSign);
			}

			handleEmptyRow();
			allegatiDTH.setCurrentMaster(al);
		} catch (final Exception e) {
			LOGGER.error(e);
		}
	}

	/**
	 * {@link #generaAnteprima(FileDTO, boolean, boolean, boolean, boolean, boolean, Long, Integer, Integer, boolean)}
	 * 
	 * @param file
	 * @param creaCampoFirma
	 * @param daConvertire
	 * @param mantieniFormatoOriginale
	 * @param docPrincipale
	 * @param daFirmare
	 */
	private void generaAnteprima(final FileDTO file, final boolean creaCampoFirma, final boolean daConvertire, final boolean mantieniFormatoOriginale,
			final boolean docPrincipale, final boolean daFirmare) {
		generaAnteprima(file, creaCampoFirma, daConvertire, mantieniFormatoOriginale, docPrincipale, false, idSiglatario, idUffCreatore, idTipoDoc, daFirmare);
	}

	/**
	 * Genera l'anteprima del file.
	 * 
	 * @param file
	 * @param creaCampoFirma
	 * @param daConvertire
	 * @param mantieniFormatoOriginale
	 * @param docPrincipale
	 * @param inStampigliaturaSigla
	 * @param inIdSiglatario
	 * @param inIdUffCreatore
	 * @param inIdTipoDoc
	 * @param indaFirmare
	 */
	private void generaAnteprima(final FileDTO file, final boolean creaCampoFirma, final boolean daConvertire, final boolean mantieniFormatoOriginale,
			final boolean docPrincipale, final boolean inStampigliaturaSigla, final Long inIdSiglatario, final Integer inIdUffCreatore, final Integer inIdTipoDoc,
			final boolean indaFirmare) {
		conCampoFirma = creaCampoFirma;
		converti = daConvertire;
		formatoOriginale = mantieniFormatoOriginale;
		convertibile = tipoFileSRV.isConvertibile(file.getFileName(), getAll);
		principale = docPrincipale;
		daFirmare = indaFirmare;
		setStampigliaturaSigla(inStampigliaturaSigla);
		idSiglatario = inIdSiglatario;
		idUffCreatore = inIdUffCreatore;
		idTipoDoc = inIdTipoDoc;
		FacesHelper.putObjectInSession(SessionObject.CONTENT_HIGHLIGHT_SIGN_FIELD, file);
	}

	/**
	 * {@link #generaAnteprima(FileDTO, boolean, boolean, boolean, boolean, boolean)}
	 * 
	 * @param file
	 * @param mantieniFormatoOriginale
	 * @param docPrincipale
	 * @param daFirmare
	 */
	private void visualizzaFile(final FileDTO file, final boolean mantieniFormatoOriginale, final boolean docPrincipale, final boolean daFirmare) {
		generaAnteprima(file, false, false, mantieniFormatoOriginale, docPrincipale, daFirmare);
	}

	/**
	 * Restituisce il content del file da scaricare.
	 * 
	 * @return file
	 */
	public StreamedContent downloadDoc() {
		StreamedContent file = null;
		final FileDTO localDocumentoPrincipale = ((List<FileDTO>) docprincDTH.getMasters()).get(0);
		try {
			file = download(localDocumentoPrincipale.getMimeType(), localDocumentoPrincipale.getFileName(), localDocumentoPrincipale.getContent());
		} catch (final Exception e) {
			LOGGER.error("Errore durante il download", e);
		}
		return file;
	}

	/**
	 * Costruisce e restituisce il content della mail da inviare.
	 * 
	 * @return content
	 */
	public StreamedContent inviaDocEmail() {
		StreamedContent output = null;
		StringBuilder sb = null;
		try {
			final FileDTO doc = ((List<FileDTO>) docprincDTH.getMasters()).get(0);
			if (doc != null) {
				final byte[] contentDoc = doc.getContent();
				final String mimeType = doc.getMimeType();
				final String contentString = Base64.getEncoder().encodeToString(contentDoc);

				sb = new StringBuilder();
				sb.append("data:message/rfc822 eml;charset=utf-8,\r\n");
				sb.append("To: \r\n");
				sb.append("Subject: \r\n");
				sb.append("X-Unsent: 1\r\n");
				sb.append("Content-Type: multipart/mixed; boundary=--boundary_text_string\r\n");
				sb.append("\r\n");
				sb.append(BOUNDARY_TEXT_STRING);
				sb.append("Content-Type: text/html; charset=UTF-8\r\n");
				sb.append("\r\n");
				sb.append("\r\n");
				sb.append(BOUNDARY_TEXT_STRING);
				sb.append("Content-Type: ").append(mimeType).append("; name=").append(doc.getFileName()).append("\r\n");
				sb.append("Content-Transfer-Encoding: base64\r\n");
				sb.append("Content-Disposition: attachment\r\n");
				sb.append("\r\n");
				sb.append(contentString).append("\r\n");
				sb.append("\r\n");

				final InputStream targetStream = new ByteArrayInputStream(sb.toString().getBytes());
				output = new DefaultStreamedContent(targetStream, MediaType.PDF.toString(), doc.getFileName() + ".eml");
			} else {
				LOGGER.error("Non è possibile inviare il documento per email");
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'eml originale.", e);
		}
		return output;
	}

	/**
	 * Converte il documento e lo mette in sessione per la stampa.
	 */
	public void openStampaDocumento() {
		final SessionBean sessionBean = FacesHelper.getManagedBean(MBean.SESSION_BEAN);
		final FileDTO doc = ((List<FileDTO>) docprincDTH.getMasters()).get(0);
		sessionBean.setRenderDialogStampa(true);
		FacesHelper.update("eastSectionForm:dlgStampaDocId");

		final byte[] newContent = PdfHelper.convertToPngPDFAndPDFtoPng(doc.getContent());
		final FileDTO docConvertito = new FileDTO(doc.getFileName(), newContent, doc.getMimeType());
		if (doc.getContent() != null) {
			FacesHelper.putObjectInSession(SessionObject.PRINT_FILE_SERVLET, docConvertito);
		}
	}

	/**
	 * Costruisce e restituisce il content della mail a partire del content
	 * dell'allegato con indice "index".
	 * 
	 * @param index
	 * @return contentMail
	 */
	public StreamedContent inviaDocEmailAllegato(final Object index) {
		StreamedContent output = null;
		StringBuilder sb = null;
		try {
			final AllegatoDTO doc = ((List<AllegatoDTO>) allegatiDTH.getMasters()).get((Integer) index);
			if (doc != null) {
				final byte[] contentDoc = doc.getContent();
				final String mimeType = doc.getMimeType();
				final String contentString = Base64.getEncoder().encodeToString(contentDoc);

				sb = new StringBuilder();
				sb.append("data:message/rfc822 eml;charset=utf-8,\r\n");
				sb.append("To: \r\n");
				sb.append("Subject: \r\n");
				sb.append("X-Unsent: 1\r\n");
				sb.append("Content-Type: multipart/mixed; boundary=--boundary_text_string\r\n");
				sb.append("\r\n");
				sb.append(BOUNDARY_TEXT_STRING);
				sb.append("Content-Type: text/html; charset=UTF-8\r\n");
				sb.append("\r\n");
				sb.append("\r\n");
				sb.append(BOUNDARY_TEXT_STRING);
				sb.append("Content-Type: ").append(mimeType).append("; name=").append(doc.getNomeFile()).append("\r\n");
				sb.append("Content-Transfer-Encoding: base64\r\n");
				sb.append("Content-Disposition: attachment\r\n");
				sb.append("\r\n");
				sb.append(contentString).append("\r\n");
				sb.append("\r\n");

				final InputStream targetStream = new ByteArrayInputStream(sb.toString().getBytes());
				output = new DefaultStreamedContent(targetStream, MediaType.PDF.toString(), doc.getNomeFile() + ".eml");

			} else {
				LOGGER.error("Non è possibile inviare il documento per email");
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'eml originale.", e);
		}
		return output;
	}

	/**
	 * Prepara ed mette in sessione l'allegato convertito per la stampa.
	 * 
	 * @param index - indice datatable allegatiDTH
	 */
	public void openStampaDocumentoAllegato(final Object index) {
		final SessionBean sessionBean = FacesHelper.getManagedBean(MBean.SESSION_BEAN);
		final AllegatoDTO doc = ((List<AllegatoDTO>) allegatiDTH.getMasters()).get((Integer) index);
		sessionBean.setRenderDialogStampa(true);
		FacesHelper.update("eastSectionForm:dlgStampaDocId");

		final byte[] newContent = PdfHelper.convertToPngPDFAndPDFtoPng(doc.getContent());
		final FileDTO docConvertito = new FileDTO(doc.getNomeFile(), newContent, doc.getMimeType());
		if (doc.getContent() != null) {
			FacesHelper.putObjectInSession(SessionObject.PRINT_FILE_SERVLET, docConvertito);
		}
	}

	/**
	 * Esegue il download dell'allegato.
	 * 
	 * @param index - indice documento allegatiDTH
	 * @return file
	 */
	public StreamedContent downloadAllegato(final Object index) {
		final List<AllegatoDTO> allegati = (List<AllegatoDTO>) allegatiDTH.getMasters();
		final AllegatoDTO allegatoSelected = allegati.get((Integer) index);

		StreamedContent file = null;
		file = download(allegatoSelected.getMimeType(), allegatoSelected.getNomeFile(), allegatoSelected.getContent());

		return file;
	}

	private StreamedContent download(final String mimeType, final String nomeFile, final byte[] content) {
		StreamedContent file = null;
		try {
			if (content != null) {
				final InputStream stream = new ByteArrayInputStream(content);
				file = new DefaultStreamedContent(stream, mimeType, nomeFile);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return file;
	}

	/**
	 * Deseleziona tutti i master dai DataTable.
	 */
	public void unsetDetail() {
		docprincDTH.setMasters(null);
		allegatiDTH.setMasters(null);
	}

	/**
	 * Handler di selezione del documento principale dal DT.
	 * 
	 * @param se
	 */
	public void rowSelectorDocPrincipale(final SelectEvent se) {
		final FileDTO dp = (FileDTO) se.getObject();
		visualizzaFile(dp, false, true, false);
		handleEmptyRow();
		docprincDTH.rowSelector(se);
	}

	/**
	 * Handler di selezione del documento principale dal DT.
	 * 
	 * @param se
	 */
	public void rowSelectorAllegati(final SelectEvent se) {
		final AllegatoDTO al = (AllegatoDTO) se.getObject();
		final FileDTO f = new FileDTO(al.getDocumentTitle(), al.getNomeFile(), al.getContent(), al.getMimeType(), false);
		final boolean mantieniFormatoOriginale = !FormatoAllegatoEnum.ELETTRONICO.equals(al.getFormatoSelected()) || al.getFormatoOriginale();
		final boolean toSign = al.getDaFirmare() != null && al.getDaFirmare() == 1;

		visualizzaFile(f, mantieniFormatoOriginale, false, toSign);
		handleEmptyRow();
		allegatiDTH.rowSelector(se);
	}

	/**
	 * Metodo per deselezionare la riga della tabella non più attiva
	 */
	private void handleEmptyRow() {
		docprincDTH.setCurrentMaster(null);
		allegatiDTH.setCurrentMaster(null);
	}

	/**
	 * Restituisce il datatable helper dei documenti principali.
	 * 
	 * @return docprincDTH
	 */
	public SimpleDetailDataTableHelper<FileDTO> getDocprincDTH() {
		return docprincDTH;
	}

	/**
	 * Imposta il datatable helper dei documenti principali.
	 * 
	 * @param docprincDTH
	 */
	public void setDocprincDTH(final SimpleDetailDataTableHelper<FileDTO> docprincDTH) {
		this.docprincDTH = docprincDTH;
	}

	/**
	 * Restituisce il datatable helper degli allegati.
	 * 
	 * @return allegatiDTH
	 */
	public SimpleDetailDataTableHelper<AllegatoDTO> getAllegatiDTH() {
		return allegatiDTH;
	}

	/**
	 * Imposta il datatable helper degli allegati.
	 * 
	 * @param allegatiDTH
	 */
	public void setAllegatiDTH(final SimpleDetailDataTableHelper<AllegatoDTO> allegatiDTH) {
		this.allegatiDTH = allegatiDTH;
	}

	/**
	 * Restituisce il documento principale selezionato.
	 * 
	 * @return documentoPrincipale
	 */
	public FileDTO getDocumentoPrincipale() {
		return documentoPrincipale;
	}

	/**
	 * Imposta il documento principale selezionato.
	 * 
	 * @param documentoPrincipale
	 */
	public void setDocumentoPrincipale(final FileDTO documentoPrincipale) {
		this.documentoPrincipale = documentoPrincipale;
	}

	/**
	 * Restituisce il flag relativo alla presenza di campi firma.
	 * 
	 * @return conCampoFirma
	 */
	public Boolean getConCampoFirma() {
		return conCampoFirma;
	}

	/**
	 * Imposta il flag relativo alla presenza di campi firma.
	 * 
	 * @param conCampoFirma
	 */
	public void setConCampoFirma(final Boolean conCampoFirma) {
		this.conCampoFirma = conCampoFirma;
	}

	/**
	 * Restituisce il flag {@link #converti}
	 * 
	 * @return converti
	 */
	public Boolean getConverti() {
		return converti;
	}

	/**
	 * Imposta il flag {@link #converti}
	 * 
	 * @param converti
	 */
	public void setConverti(final Boolean converti) {
		this.converti = converti;
	}

	/**
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'utente corrente.
	 * 
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Restituisce il flag {@link #inModifica}
	 * 
	 * @return inModifica
	 */
	public boolean isInModifica() {
		return inModifica;
	}

	/**
	 * Restituisce il flag {@link #inIngresso}
	 * 
	 * @return inIngresso
	 */
	public boolean isInIngresso() {
		return inIngresso;
	}

	/**
	 * Restituisce il flag {@link #formatoOriginale}
	 * 
	 * @return formatoOriginale
	 */
	public boolean isFormatoOriginale() {
		return formatoOriginale;
	}

	/**
	 * Restituisce il flag relativo alla convertibilità del documento.
	 * 
	 * @return convertibile
	 */
	public boolean isConvertibile() {
		return convertibile;
	}

	/**
	 * Restituisce il flag relativo alla firma multipla del documento.
	 * 
	 * @return firmaMultipla
	 */
	public boolean isFirmaMultipla() {
		return firmaMultipla;
	}

	/**
	 * Restituisce il flag che definisce se il documento sia il principale.
	 * 
	 * @return principale
	 */
	public boolean isPrincipale() {
		return principale;
	}

	/**
	 * Restituisce il flag che definisce se il documento sia da non protocollare.
	 * 
	 * @return daNonProtocollare
	 */
	public boolean isDaNonProtocollare() {
		return daNonProtocollare;
	}

	/**
	 * Restituisce il flag che definisce se il documento sia da firmare.
	 * 
	 * @return daFirmare
	 */
	public boolean isDaFirmare() {
		return daFirmare;
	}

	/**
	 * @return idSiglatario
	 */
	public Long getIdSiglatario() {
		return idSiglatario;
	}

	/**
	 * Imposta l'id del siglatario.
	 * 
	 * @param idSiglatario
	 */
	public void setIdSiglatario(final Long idSiglatario) {
		this.idSiglatario = idSiglatario;
	}

	/**
	 * Restituisce l'id dell'ufficio creatore del documento.
	 * 
	 * @return idUffCreatore
	 */
	public Integer getIdUffCreatore() {
		return idUffCreatore;
	}

	/**
	 * Imposta l'id dell'ufficio creatore del documento.
	 * 
	 * @param idUffCreatore
	 */
	public void setIdUffCreatore(final Integer idUffCreatore) {
		this.idUffCreatore = idUffCreatore;
	}

	/**
	 * Restituisce l'id della tipologia del documento.
	 * 
	 * @return idTipoDoc
	 */
	public Integer getIdTipoDoc() {
		return idTipoDoc;
	}

	/**
	 * Imposta l'id della tipologia del documento.
	 * 
	 * @param idTipoDoc
	 */
	public void setIdTipoDoc(final Integer idTipoDoc) {
		this.idTipoDoc = idTipoDoc;
	}

	/**
	 * Restituisce il flag {@link #stampigliaturaSigla}
	 * 
	 * @return stampigliaturaSigla
	 */
	public boolean getStampigliaturaSigla() {
		return stampigliaturaSigla;
	}

	/**
	 * Imposta il flag {@link #stampigliaturaSigla}
	 * 
	 * @param stampigliaturaSigla
	 */
	public void setStampigliaturaSigla(final boolean stampigliaturaSigla) {
		this.stampigliaturaSigla = stampigliaturaSigla;
	}
}
