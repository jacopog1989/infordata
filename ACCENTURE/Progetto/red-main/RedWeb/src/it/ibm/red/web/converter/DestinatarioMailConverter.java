package it.ibm.red.web.converter;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.RicercaRubricaDTO;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.TipoRubricaEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IRubricaSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.MailBean;

/**
 * Converter destinatario mail.
 */
@FacesConverter("destinatarioMailConverter")
public class DestinatarioMailConverter implements Converter {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DestinatarioMailConverter.class);

	/**
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.String).
	 */
	@Override
	public Object getAsObject(final FacesContext fc, final UIComponent uic, final String value) {
		final DestinatarioRedDTO destinatario = new DestinatarioRedDTO();

		try {
			if (StringUtils.isNullOrEmpty(value)) {
				return destinatario;
			}

			final MailBean mb = FacesHelper.getManagedBean(ConstantsWeb.MBean.MAIL_BEAN);
			if (mb.getDestinatari() != null) {
				for (DestinatarioRedDTO dest : mb.getDestinatari()) {
					if (dest == null) {
						dest = new DestinatarioRedDTO();
					}
					if (dest != null && value.equals(dest.getIdentificativo())) {
						return dest;
					}
				}
			}

			// Se non è già stato inserito devo recuperare il contatto interno o esterno
			final String[] valori = value.split("---");

			if (valori == null || valori.length < 2) {
				return null;
			}

			final TipologiaDestinatarioEnum t = TipologiaDestinatarioEnum.getByTipologia(valori[0]);
			if (TipologiaDestinatarioEnum.ESTERNO == t) {
				final IRubricaSRV rubSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaSRV.class);
				final Contatto contatto = rubSRV.getContattoByID(true, Long.parseLong(valori[1]));
				destinatario.setContatto(contatto);
				if (!contatto.getTipoRubricaEnum().equals(TipoRubricaEnum.GRUPPO)) {
					if (StringUtils.isNullOrEmpty(contatto.getMailSelected())) {
						destinatario.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.CARTACEO);
						destinatario.setMezzoSpedizioneDisabled(true);
					} else {
						destinatario.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.ELETTRONICO);
						destinatario.setMezzoSpedizioneDisabled(false);
					}
				} else {
					RicercaRubricaDTO ricercaContattiNelGruppo;
					ricercaContattiNelGruppo = new RicercaRubricaDTO();
					List<Contatto> contattiDaGruppo = null;

					ricercaContattiNelGruppo.setRicercaRED(true);
					ricercaContattiNelGruppo.setRicercaIPA(true);
					ricercaContattiNelGruppo.setRicercaMEF(true);
					ricercaContattiNelGruppo.setRicercaGruppo(false);
					ricercaContattiNelGruppo.setIdGruppo(contatto.getContattoID());
					ricercaContattiNelGruppo.setIdAoo(mb.getUtente().getIdAoo().intValue());
					contattiDaGruppo = rubSRV.ricerca(mb.getUtente().getIdUfficio(), ricercaContattiNelGruppo);
					if (contattiDaGruppo != null) {
						destinatario.setContattiGruppo(contattiDaGruppo);
						destinatario.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.ELETTRONICO);
						destinatario.setMezzoSpedizioneDisabled(true);
					}
				}
			} else if (TipologiaDestinatarioEnum.INTERNO == t) {
				destinatario.setIdNodo(Long.valueOf(valori[1]));
				if (valori.length == 3) {
					destinatario.setIdUtente(Long.valueOf(valori[2]));
				}
			}
			destinatario.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
			destinatario.setTipologiaDestinatarioEnum(t);
			destinatario.setMezzoSpedizioneVisible(TipologiaDestinatarioEnum.ESTERNO == t);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
		}

		return destinatario;
	}

	/**
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.Object).
	 */
	@Override
	public String getAsString(final FacesContext fc, final UIComponent uic, final Object object) {
		if (object instanceof DestinatarioRedDTO) {
			final DestinatarioRedDTO destinatario = (DestinatarioRedDTO) object;
			return destinatario.getIdentificativo();
		}
		return (object != null) ? object.toString() : null;
	}
}
