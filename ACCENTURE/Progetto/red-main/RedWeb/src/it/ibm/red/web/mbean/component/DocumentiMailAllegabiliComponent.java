package it.ibm.red.web.mbean.component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.FascicoloDTO;

/**
 * Component documenti mail allegabili.
 */
public class DocumentiMailAllegabiliComponent extends DocumentiAllegabiliAbstract {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1324944886201068021L;

	/**
	 * Dettaglio.
	 */
	private DocumentoAllegabileDTO mail;

	/**
	 * Costruttore del component.
	 * 
	 * @param fascicoloProcedimentale
	 * @param inMail
	 * @param documentiAllegabili
	 * @param inDimensioneTotale
	 */
	public DocumentiMailAllegabiliComponent(final FascicoloDTO fascicoloProcedimentale, final DocumentoAllegabileDTO inMail,
			final List<DocumentoAllegabileDTO> documentiAllegabili, final BigDecimal inDimensioneTotale) {
		super(fascicoloProcedimentale, documentiAllegabili, inDimensioneTotale);

		if (inMail != null) {
			this.mail = inMail;
			mail.setDisabled(true);
			final List<DocumentoAllegabileDTO> completo = new ArrayList<>();
			completo.add(mail);
			completo.addAll(documentiAllegabili);
			setDocumentiAllegabili(completo);

			final List<DocumentoAllegabileDTO> onlyMail = new ArrayList<>();
			onlyMail.add(mail);
			setSelectedAllegabili(onlyMail);
		}
	}

	/**
	 * Esegue una validazione sulla dimensione dei procedimenti selezionati.
	 * 
	 * @return true se la validazione ha avuto esito postivio, false altrimenti
	 */
	public boolean validate() {
		boolean valid = true;
		if (CollectionUtils.isEmpty(getSelectedAllegabili())) {
			valid = false;
			showWarnMessage("Attenzione, è necessario selezionare almeno un procedimento da allegare.");
		}

		// L'email da sola può superare la dimensione massima. Nel caso la superi non
		// vengono accettati altri allegati.
		// Ulteriori allegati e l'email non devono superare la dimensione massima.
		if ((!isMailFirst() || getSelectedAllegabili().size() > 1) && isDimensioneSuperata()) {
			valid = false;
			showWarnMessage("Attenzione, la dimensione totale dei procedimenti selezionati per l'apertura della RdS Siebel supera quella massima consentita.");
		}

		return valid;
	}

	/**
	 * Restituisce la mail come DocumentoAllegabileDTO.
	 * 
	 * @return mail
	 */
	public DocumentoAllegabileDTO getMail() {
		return mail;
	}

	/**
	 * Restituisce true se la mail è diversa da null, false altrimenti.
	 * 
	 * @return true se la mail è diversa da null, false altrimenti
	 */
	public boolean isMailFirst() {
		return mail != null;
	}

	@Override
	public void aggiornaVerificaFirmaAllegato(final Object index) {
		// Non occorre fare niente in questo metodo.
	}
}
