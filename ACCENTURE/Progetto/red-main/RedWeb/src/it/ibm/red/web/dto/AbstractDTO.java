package it.ibm.red.web.dto;

import java.io.Serializable;

/**
 * DTO abstract.
 */
public abstract class AbstractDTO implements Serializable {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -6198455387941731296L;

}
