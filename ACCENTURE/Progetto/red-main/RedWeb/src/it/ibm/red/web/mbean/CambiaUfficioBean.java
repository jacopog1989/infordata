package it.ibm.red.web.mbean;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Ruolo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IAssegnaUfficioFacadeSRV;
import it.ibm.red.business.service.facade.IDfFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Bean che gestisce il cambio ufficio.
 */
@Named(ConstantsWeb.MBean.CAMBIA_UFFICIO_BEAN)
@ViewScoped
public class CambiaUfficioBean extends AbstractBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 1805657963958971371L;
	
    /**
     * Servizio assegna ufficio.
     */
	private IAssegnaUfficioFacadeSRV assegnaUfficioSRV;
	
    /**
     * Bean di sessione.
     */
	private SessionBean sessionBean;

	/**
     * Info utente.
     */
	private UtenteDTO utente;

	/**
	 * Identificativo nuovo ufficio.
	 */
	private Long idNuovoUfficio;

	/**
	 * Identificativo nuova aoo.
	 */
	private Long idNuovoAoo;

	/**
	 * Identificativo nuovo ruolo.
	 */
	private Long idNuovoRuolo;


	/**
	 * Lista uffici.
	 */
	private List<Nodo> listaUffici;

	/**
	 * Lista ruoli.
	 */
	private List<Ruolo> listaRuoli;

	/**
	 * Lista aoo.
	 */
	private List<Aoo> listaAOO;
	

	/**
	 * Servizio ricerca df.
	 */
	private IDfFacadeSRV ricercaDfSRV;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		sessionBean = (SessionBean) FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		assegnaUfficioSRV = ApplicationContextProvider.getApplicationContext().getBean(IAssegnaUfficioFacadeSRV.class);
		utente = sessionBean.getUtente();
		listaAOO = assegnaUfficioSRV.getAOOFromIdUtente(utente.getId());
		listaUffici = assegnaUfficioSRV.getNodiFromIdUtenteandIdAOO(utente.getId(), utente.getIdAoo());
		listaRuoli = assegnaUfficioSRV.getRuoliFromIdNodoAndIdAOO(utente.getId(), utente.getIdUfficio(), utente.getIdAoo());

		ricercaDfSRV = ApplicationContextProvider.getApplicationContext().getBean(IDfFacadeSRV.class);
		syncUtente();
 	}

	/**
	 * Sincronizza utente, inizializzando attributi specifici, lista uffici e lista ruoli.
	 */
	public void syncUtente() {
		idNuovoUfficio = utente.getIdUfficio();
		idNuovoAoo = utente.getIdAoo();
		idNuovoRuolo = utente.getIdRuolo();
		listaUffici = assegnaUfficioSRV.getNodiFromIdUtenteandIdAOO(utente.getId(), idNuovoAoo);
		listaRuoli = assegnaUfficioSRV.getRuoliFromIdNodoAndIdAOO(utente.getId(), idNuovoUfficio, idNuovoAoo);
	}

	/**
	 * Esegue il caricamento dei ruoli, popolando l'attributo listaRuoli.
	 */
	public void caricaRuoli() {
		listaRuoli = assegnaUfficioSRV.getRuoliFromIdNodoAndIdAOO(utente.getId(), idNuovoUfficio, idNuovoAoo);
		idNuovoRuolo = listaRuoli.get(0).getIdRuolo();
	}

	/**
	 * Esegue il caricamento degli uffici, popolando l'attributo listaUffici.
	 */
	public void caricaUffici() {
		listaUffici = assegnaUfficioSRV.getNodiFromIdUtenteandIdAOO(utente.getId(), idNuovoAoo);
		idNuovoUfficio = listaUffici.get(0).getIdNodo();
		caricaRuoli();
	}

	/**
	 * Gestisce il cambio di ufficio, ruolo o aoo.
	 */
	public void cambia() {
		Nodo ufficioSelezionato = null;
		Optional<Nodo> optNodo = listaUffici.stream().filter(ufficio -> ufficio.getIdNodo().equals(idNuovoUfficio)).findFirst();
		if (optNodo.isPresent()) {
			ufficioSelezionato = optNodo.get();
		}
		
		if (ufficioSelezionato != null) {
			
			Ruolo ruoloSelezionato = null;
			Optional<Ruolo> optRuolo = listaRuoli.stream().filter(ruolo -> ruolo.getIdRuolo() == idNuovoRuolo.longValue()).findFirst();
			if (optRuolo.isPresent()) {
				ruoloSelezionato = optRuolo.get();
			}
			
			assegnaUfficioSRV.popolaUtente(utente, ufficioSelezionato, ruoloSelezionato);
			
			sessionBean.caricaRicercheDisponibili();
			sessionBean.ricaricaMenuContatori();
			sessionBean.loadOrgUtente();
			
			if (utente.getShowRiferimentoStorico()) {
				sessionBean.setTipologiaRegistroNsd(ricercaDfSRV.getTipologiaRegistroNsd(utente.getIdAoo()));
				sessionBean.setTipologiaDocumentoNsd(ricercaDfSRV.getTipologiaDocumentoNsd(utente.getIdUfficio(), utente.getIdUfficioPadre()));
			}
			
			sessionBean.rinnovaSessione();
			
		
		}
		
		sessionBean.gotoCustomHomepage();
	}

	/**
	 * Restituisce l'id del nuovo ufficio.
	 * @return idNuovoUfficio
	 */
	public Long getIdNuovoUfficio() {
		return idNuovoUfficio;
	}

	/**
	 * Imposta l'id del nuovo ufficio.
	 * @param idNuovoUfficio
	 */
	public void setIdNuovoUfficio(final Long idNuovoUfficio) {
		this.idNuovoUfficio = idNuovoUfficio;
	}

	/**
	 * Restituisce l'id della nuova aoo.
	 * @return idNuovoAoo
	 */
	public Long getIdNuovoAoo() {
		return idNuovoAoo;
	}

	/**
	 * Imposta l'id della nuova aoo.
	 * @param idNuovoAoo
	 */
	public void setIdNuovoAoo(final Long idNuovoAoo) {
		this.idNuovoAoo = idNuovoAoo;
	}

	/**
	 * Restituisce l'id del nuovo ruolo.
	 * @return idNuovoRuolo
	 */
	public Long getIdNuovoRuolo() {
		return idNuovoRuolo;
	}

	/**
	 * Imposta l'id del nuovo ruolo.
	 * @param idNuovoRuolo
	 */
	public void setIdNuovoRuolo(final Long idNuovoRuolo) {
		this.idNuovoRuolo = idNuovoRuolo;
	}

	/**
	 * Restituisce la lista degli uffici disponibili per l'utente.
	 * @return listaUffici
	 */
	public List<Nodo> getListaUffici() {
		return listaUffici;
	}

	/**
	 * Restituisce la lista dei ruoli disponibili per l'utente.
	 * @return listaRuoli
	 */
	public List<Ruolo> getListaRuoli() {
		return listaRuoli;
	}

	/**
	 * Restituisce la lista delle AOO disponibili per l'utente.
	 * @return listaAOO
	 */
	public List<Aoo> getListaAOO() {
		return listaAOO;
	}

}
