package it.ibm.red.web.mbean.master;

import it.ibm.red.web.enums.ModeFaldonaturaEnum;

/**
 * Interfaccia dell'handler dei faldoni.
 */
public interface IFaldoneHandler {

	/**
	 * Gestisce le operazioni sul faldone.
	 * @param modeEnum
	 */
	void gestisciOperazioneFaldone(ModeFaldonaturaEnum modeEnum);
}