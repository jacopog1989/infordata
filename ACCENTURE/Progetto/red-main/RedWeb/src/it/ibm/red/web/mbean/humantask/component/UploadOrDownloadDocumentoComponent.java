package it.ibm.red.web.mbean.humantask.component;

import java.io.IOException;
import java.io.InputStream;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IPredisponiDsrFacadeSRV;
import it.ibm.red.web.document.mbean.component.UploadDocumentoBean;

/**
 * Component che gestisce l'upload o il download di un documento.
 */
public class UploadOrDownloadDocumentoComponent extends UploadDocumentoBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 4040493139709388058L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(UploadOrDownloadDocumentoComponent.class);

	/**
	 * Flag uploaded.
	 */
	private boolean uploaded;

	/**
	 * Utente.
	 */
	private final UtenteDTO utente;

	/**
	 * Servizio.
	 */
	private final IPredisponiDsrFacadeSRV predisponiSRV;
	
	/**
	 * Costruttore del component.
	 * @param detail
	 * @param utente
	 */
	public UploadOrDownloadDocumentoComponent(final DetailDocumentRedDTO detail, final UtenteDTO utente) {
		super(detail);
		this.utente = utente;
		
		predisponiSRV = ApplicationContextProvider.getApplicationContext().getBean(IPredisponiDsrFacadeSRV.class);
	}

	/**
	 * Gestisce l'upload del file principale.
	 * @param event evento di upload
	 */
	@Override
	public void handleMainFileUpload(FileUploadEvent event) {
		try {
			event = checkFileName(event);
		} catch (final IOException e) { 
			LOGGER.error("Errore nell'upload del file", e);
			showError("Errore nell'upload del file");
		}
		if (event == null) {
			return;
		}
		super.handleMainFileUpload(event);
		setUploaded(true);
	}
	
	/**
	 * Restituisce lo StreamedContent che consente il download del file della dichiarazione.
	 * @return StreamedContent per download
	 */
	public StreamedContent getDownload() {
		final InputStream io = predisponiSRV.retrieveContentDsr(getDetail().getDocumentTitle(), utente);
		if (io == null) {
			showError("Impossibile recuperare il file della dichiarazione");
		}
		
		return new DefaultStreamedContent(io, getDetail().getMimeType(), getDetail().getNomeFile());
	}

	/**
	 * Restituisce true se l'upload è stato effettuato, false altrimenti.
	 * @return true se l'upload è stato effettuato, false altrimenti
	 */
	public boolean isUploaded() {
		return uploaded;
	}

	/**
	 * Imposta il flag associato all'upload del file.
	 * @param uploaded
	 */
	protected void setUploaded(final boolean uploaded) {
		this.uploaded = uploaded;
	}

}
