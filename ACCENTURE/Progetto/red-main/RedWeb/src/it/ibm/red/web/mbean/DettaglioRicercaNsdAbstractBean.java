package it.ibm.red.web.mbean;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Bean dettaglio ricerca NSD.
 */
public class DettaglioRicercaNsdAbstractBean extends AbstractBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 8422518253881813826L;
    
	/**
	 * Utente.
	 */
	protected UtenteDTO utente;

	/**
	 * Post construct del bean.
	 */
	@Override
	protected void postConstruct() {
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
	}
}
