package it.ibm.red.web.enums;

/**
 * Enum che definisce la modalità della rubrica.
 */
public enum ModeRubricaEnum {

	/**
	 * Valore.
	 */
	SELEZIONE_SINGOLA("S", "Selezione Singola"),

	/**
	 * Valore.
	 */
	SELEZIONE_MULTIPLA("M", "Selezione Multipla");
	
	/**
	 * Type.
	 */
	private String type;	

	/**
	 * Display name.
	 */
	private String displayName;

	ModeRubricaEnum(final String inType, final String inDisplayName) {
		type = inType;
		displayName = inDisplayName;
	}
	
	/**
	 * Restituisce il type associato all'enum.
	 * @return type dell'enum
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Restituisce il display name dell'enum.
	 * @return display name enum
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Restituisce l'enum associata al type.
	 * @param type
	 * @return enum associata
	 */
	public static ModeRubricaEnum get(final String type) {
		ModeRubricaEnum output = null;
		for (final ModeRubricaEnum t:ModeRubricaEnum.values()) {
			if (type.equals(t.getType())) {
				output = t;
				break;
			}
		}
		return output;
	}
}

