package it.ibm.red.web.ricerca.mbean;

import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.DetailFascicoloRedDTO;
import it.ibm.red.business.dto.FascicoloFlussoDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IRicercaAvanzataDocUCBSRV;
import it.ibm.red.business.service.facade.IFascicoloFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.DettaglioFascicoloAbstractBean;
import it.ibm.red.web.mbean.FascicoloManagerBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.IUpdatableDetailCaller;

/**
 * Bean che gestisce la ricerca flussi ucb.
 */
@Named(ConstantsWeb.MBean.RICERCA_FLUSSI_UCB_BEAN)
@ViewScoped
public class RicercaFlussiUcbBean extends DettaglioFascicoloAbstractBean implements IUpdatableDetailCaller<DetailFascicoloRedDTO> {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = -2467089614206355551L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaFlussiUcbBean.class.getName());

	/**
	 * Lista risultati.
	 */
	private List<FascicoloFlussoDTO> result;

	/**
	 * Storico.
	 */
	private Collection<StoricoDTO> storico;

	/**
	 * Servizio.
	 */
	private IFascicoloFacadeSRV fascicoloSRV;

	/**
	 * Servizio.
	 */
	private IRicercaAvanzataDocUCBSRV ricercaSRV;

	/**
	 * Caller.
	 */
	private IUpdatableDetailCaller<DetailFascicoloRedDTO> caller; 

	
	/**
	 * Post construct del bean.
	 */
	@Override	
	@PostConstruct
	protected void postConstruct() {
		fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloFacadeSRV.class);
		ricercaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaAvanzataDocUCBSRV.class);
		
		result = (List<FascicoloFlussoDTO>) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.RISULTATO_RICERCA_FLUSSI, true);
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		
		super.postConstruct();
	}
	
	/**
	 * Imposta il chiamante.
	 * @param inCaller
	 */
	public void setCaller(final IUpdatableDetailCaller<DetailFascicoloRedDTO> inCaller) {
		this.caller = inCaller;
	}
	
	/**
	 * Gestisce la chiusura del fascicolo identificato dalla riga selezionata.
	 * @param event
	 */
	public final void chiudiFascicolo(final ActionEvent event) {
		try {
			final FascicoloFlussoDTO fascicolo = getDataTableClickedRow(event);
			fascicoloSRV.chiudiFascicolo(fascicolo.getNumPratica() + "", utente.getIdAoo(), utente);
			fascicolo.setStato(FilenetStatoFascicoloEnum.CHIUSO);
			showInfoMessage("Chiusura fascicolo avvenuta con successo.");
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di chiusura del fascicolo.", e);
			showError("Errore in fase di chiusura del fascicolo.");
		}
	}

	/**
	 * Gestisce l'apertura del fascicolo identificato dalla riga selezionata.
	 * @param event
	 */
	public final void apriDialogFascicolo(final javax.faces.event.ActionEvent event) {
		final FascicoloFlussoDTO fascicolo = getDataTableClickedRow(event);
		this.setDetail(fascicolo.getNumPratica()); //id fascicolo
		this.setCaller(this);
		super.openDialogFascicolo();
		
		final FascicoloManagerBean fascMenbean = FacesHelper.getManagedBean(MBean.FASCICOLO_MANAGER_BEAN);
		if (fascMenbean != null) {
			fascMenbean.setCaller(this);
			fascMenbean.setChiamante(ConstantsWeb.MBean.RICERCA_FLUSSI_UCB_BEAN);
		}		
	}
	
	/**
	 * Permette la visualizzazione dello storico del documento associato al fascicolo identificato dalla riga selezionata dal datatable.
	 * Recupera lo storico e aggiorna {@link #storico}.
	 * @param event
	 */
	public final void visualizzaStoricoDocumento(final ActionEvent event) {
		final FascicoloFlussoDTO fascicolo = getDataTableClickedRow(event);
		final Integer dtDocumento = fascicolo.getIdDocumento();
		storico = ricercaSRV.getStoricoDocumentoFlusso(dtDocumento, utente);
	}
	
	/**
	 * Restituisce lo storico.
	 * @return storico
	 */
	public Collection<StoricoDTO> getStorico() {
		return storico;
	}

	/**
	 * Restituisce i risultati della ricerca.
	 * @return risultati ricerca
	 */
	public List<FascicoloFlussoDTO> getResult() {
		return result;
	}

	/**
	 * Aggiorna il dettaglio impostando <code> detail </code> come dettaglio.
	 * @param detail dettaglio da impostare
	 */
	@Override
	public void updateDetail(final DetailFascicoloRedDTO detail) {
		this.fascicoloCmp.setDetail(detail);
		if (caller != null && !(caller instanceof RicercaFlussiUcbBean)) {
			caller.updateDetail(detail);
		}
	}

}
