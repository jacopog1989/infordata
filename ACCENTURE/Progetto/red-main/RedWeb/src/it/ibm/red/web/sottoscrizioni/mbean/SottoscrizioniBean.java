package it.ibm.red.web.sottoscrizioni.mbean;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.component.tabview.TabView;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.enums.NavigationTokenEnum;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.sottoscrizioni.mbean.component.DettaglioDocumentoSottoscrizioniComponent;
import it.ibm.red.web.sottoscrizioni.mbean.component.DisabilitaNotificheComponent;
import it.ibm.red.web.sottoscrizioni.mbean.component.ElencoSottoscrizioniComponent;
import it.ibm.red.web.sottoscrizioni.mbean.component.MailSottoscrizioniComponent;
import it.ibm.red.web.sottoscrizioni.mbean.component.NotificheSottoscrizioniComponent;
import it.ibm.red.web.sottoscrizioni.mbean.component.ProcedimentiTracciatiComponent;

/**
 * Configurazioni delle sottoscrizioni
 *
 */
@Named(ConstantsWeb.MBean.SOTTOSCRIZIONI_BEAN)
@ViewScoped
public class SottoscrizioniBean extends AbstractBean {

	private static final long serialVersionUID = 3520278721572555922L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SottoscrizioniBean.class.getName());

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Flag attivo.
	 */
	private boolean active;

	/**
	 * Tab corrente.
	 */
	private String currentTab;

	/**
	 * Componente sottoscrizioni mail.
	 */
	private MailSottoscrizioniComponent mailComponent;

	/**
	 * Componente disabilita notifiche.
	 */
	private DisabilitaNotificheComponent disabilitazioneComponent;

	/**
	 * Componente elenco sottoscrizioni.
	 */
	private ElencoSottoscrizioniComponent elencoTab;

	/**
	 * Componente procedimenti tracciati.
	 */
	private ProcedimentiTracciatiComponent tracciatiTab;

	/**
	 * Componente notifiche sottoscrizioni.
	 */
	private NotificheSottoscrizioniComponent notificheTab;

	/**
	 * Gestisce tutte le informazioni relative al dettaglio del documento per le
	 * sottoscrizioni Viene utilizzato sia nel tab notifiche che nel tab dei
	 * procedimentiTracciati Condividendo questo gestore dovrei evitare la
	 * duplicazione del Bean.
	 */
	private DettaglioDocumentoSottoscrizioniComponent documentDetail;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		if (sessionBean.getActivePageInfo().equals(NavigationTokenEnum.SOTTOSCRIZIONI)) {
			if (sessionBean.getIdNotificaSelezionataSott() == null) {
				init(null);
			} else {
				final TabView tabSottoscrizioni = (TabView) FacesContext.getCurrentInstance().getViewRoot().findComponent("centralSectionForm:idTab_STSCZ");
				tabSottoscrizioni.setActiveIndex(2);
				init(sessionBean.getIdNotificaSelezionataSott());
				sessionBean.setIdNotificaSelezionataSott(null);
			}
		}
	}

	/**
	 * Metodo per l'inizializzazione dei valori
	 */
	public void init(final Integer idNotifica) {
		FacesHelper.executeJS("hidePreviewSottoscrizione();");

		this.active = true;
		mailComponent = new MailSottoscrizioniComponent(utente);

		disabilitazioneComponent = new DisabilitaNotificheComponent(utente);

		documentDetail = new DettaglioDocumentoSottoscrizioniComponent(utente);

		elencoTab = new ElencoSottoscrizioniComponent(utente);
		try {
			elencoTab.initialize();
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inizializzazione", e);
			showError("Errore durante l'inizializzazione: " + e.getMessage());
		}

		notificheTab = new NotificheSottoscrizioniComponent(utente, idNotifica);
		try {
			notificheTab.initialize(idNotifica);
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inizializzazione", e);
			showError("Errore durante l'inizializzazione: " + e.getMessage());
		}
		tracciatiTab = new ProcedimentiTracciatiComponent(utente);
	}

	/**
	 * Esegue le sottoscrizioni mail, disabilitazioni e salvataggio eventi. Le
	 * operazioni sono demandate a: @see DisabilitaNotificheComponent, @see
	 * MailSottoscrizioniComponent
	 * 
	 * @see ElencoSottoscrizioniComponent.
	 */
	public void registra() {
		// NOTE in un unico growl mettere sia gli esiti positivi che negativi, in error
		// (se tutti sono positivi un unico msg)
		boolean error = false;
		final StringBuilder msg = new StringBuilder();
		// registra componente mail
		try {
			mailComponent.registra();
			msg.append("Sottoscrizione mail avvenuta con successo.<br />");
		} catch (final Exception red) {
			LOGGER.error(red);
			msg.append("Errore registrazione mail: " + red.getMessage());
			error = true;
		}
		msg.append(System.lineSeparator());

		// registra del componente disabilitazione
		try {
			disabilitazioneComponent.registra();
			msg.append("Sottoscrizione delle disabilitazioni avvenuta con successo.<br />");
		} catch (final Exception red) {
			LOGGER.error(red);
			msg.append("Errore sottoscrizione delle disabilitazioni: " + red.getMessage());
			error = true;
		}
		msg.append(System.lineSeparator());

		// registra componente tab elenco sottoscrizioni
		try {
			elencoTab.registra();
			msg.append("Salvataggio eventi avvenuto con successo.");
		} catch (final Exception red) {
			LOGGER.error(red);
			msg.append("Errore salvataggio eventi: " + red.getMessage());
			error = true;
		}
		msg.append(System.lineSeparator());

		if (error) {
			showError(msg.toString());
		} else {
			showInfoMessage("Operazione eseguita con successo.");
		}

	}

	/**
	 * Restituisce true se uno dei dettagli è attivo, false altrimenti.
	 * 
	 * @return true se uno dei dettagli è attivo, false altrimenti
	 */
	public boolean isAtLeastOneDetailActiveForPopUp() {
		final DettaglioDocumentoSottoscrizioniComponent notifiche = notificheTab.getDocumentDetailComponent();
		final DettaglioDocumentoSottoscrizioniComponent procedimento = tracciatiTab.getDocumentDetailComponent();
		return isDetailActiveForPopUp(notifiche) || isDetailActiveForPopUp(procedimento);
	}

	private boolean isDetailActiveForPopUp(final DettaglioDocumentoSottoscrizioniComponent dettaglio) {
		return dettaglio != null && dettaglio.isPopupDocumentVisible()
				&& ((dettaglio.getDocumentCmp() != null && dettaglio.getDocumentCmp().getDetail() != null) || dettaglio.isIntroPreviewActive());
	}

	/************************* Getter e Setter *****************************/
	/**
	 * Restituisce il component per la sottoscrizione mail.
	 * 
	 * @return il component MailSottoscrizioniComponent
	 */
	public MailSottoscrizioniComponent getMailComponent() {
		return mailComponent;
	}

	/**
	 * Restituisce il component per la gestione della disabilitazione notifiche.
	 * 
	 * @return il component DisabilitaNotificheComponent
	 */
	public DisabilitaNotificheComponent getDisabilitazioneComponent() {
		return disabilitazioneComponent;
	}

	/**
	 * Restituisce il component per la gestione degli elenchi sottoscrizione.
	 * 
	 * @return il component ElencoSottoscrizioniComponent
	 */
	public ElencoSottoscrizioniComponent getElencoTab() {
		return elencoTab;
	}

	/**
	 * Restituisce il component per la gestione del tracciamento dei procedimenti.
	 * 
	 * @return il component ProcedimentiTracciatiComponent
	 */
	public ProcedimentiTracciatiComponent getTracciatiTab() {
		return tracciatiTab;
	}

	/**
	 * Restituisce il component per la sottoscrizione delle notifiche.
	 * 
	 * @return il component NotificheSottoscrizioniComponent
	 */
	public NotificheSottoscrizioniComponent getNotificheTab() {
		return notificheTab;
	}

	/**
	 * Restituisce true se attivo, false altrimenti.
	 * 
	 * @return true se attivo, false altrimenti
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Restituisce il title per i procedimenti tracciati.
	 * 
	 * @return title procedimenti tracciati
	 */
	public String getProcedimentiTracciatiTitle() {
		return "Procedimenti Tracciati";
	}

	/**
	 * Restituisce il title per le notifiche.
	 * 
	 * @return title notifiche
	 */
	public String getNotificheTitle() {
		return "Notifiche";
	}

	/**
	 * Restituisce il component per la gestione del dettaglio documento.
	 * 
	 * @return il component DettaglioDocumentoSottoscrizioniComponent
	 */
	public DettaglioDocumentoSottoscrizioniComponent getDocumentDetail() {
		return documentDetail;
	}

	/**
	 * Imposta il component per la gestione del dettaglio documento.
	 * 
	 * @param documentDetail
	 */
	public void setDocumentDetail(final DettaglioDocumentoSottoscrizioniComponent documentDetail) {
		this.documentDetail = documentDetail;
	}

	/**
	 * Restituisce il tab corrente.
	 * 
	 * @return current tab
	 */
	public String getCurrentTab() {
		return currentTab;
	}
}