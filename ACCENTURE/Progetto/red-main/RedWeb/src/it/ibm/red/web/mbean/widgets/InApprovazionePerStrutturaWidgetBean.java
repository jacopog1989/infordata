package it.ibm.red.web.mbean.widgets;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.HorizontalBarChartModel;

import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ICounterFacadeSRV;
import it.ibm.red.business.service.facade.IInApprovazioneWidgetFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean; 

/**
 * Bean che gestice il bean in approvazione per struttura.
 */
@Named(ConstantsWeb.MBean.INAPPROVAZIONEPERSTRUTTURA_WIDGET_BEAN)
@ViewScoped
public class InApprovazionePerStrutturaWidgetBean extends AbstractBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -5898367516223106209L;
	
	/**
     * Lista uffici.
     */
	private List<UfficioDTO> ufficioList;

	/**
     * Lista info uffici.
     */
	private List<UfficioDTO> uffici;
	
	/**
     * Lista nodi.
     */ 	
	private List<Nodo> listaUffici;
	
    /**
     * Modello grafico.
     */
	private BarChartModel animatedBarChartModel;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		final SessionBean sessionBean = (SessionBean) FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		final UtenteDTO utente = sessionBean.getUtente();
		final IInApprovazioneWidgetFacadeSRV approvazioneWdgSRV = ApplicationContextProvider.getApplicationContext().getBean(IInApprovazioneWidgetFacadeSRV.class);
		final ICounterFacadeSRV countSRV = ApplicationContextProvider.getApplicationContext().getBean(ICounterFacadeSRV.class); 
		listaUffici = approvazioneWdgSRV.getNodiFromIdUtenteandIdAOO(utente, false); 
		Map<Integer, Integer> counterUffici = countSRV.countTotSottoUfficiWidgetStruttura(listaUffici, utente, false);
		
		for (final Nodo ufficio : listaUffici) {
			ufficio.setCountUfficio(counterUffici.get(ufficio.getIdNodo().intValue()));
		}
		
		Collections.sort(listaUffici);
		
		createAnimatedModels();
		
	}
	
	/**
	 * Recupera la lista degli uffici.
	 * @return lista degli uffici
	 */
	public List<UfficioDTO> getUfficioList() {
		return ufficioList;
	}
	
	/**
	 * Imposta la lista degli uffici.
	 * @param ufficioList lista degli uffici
	 */
	public void setUfficioList(final List<UfficioDTO> ufficioList) {
		this.ufficioList = ufficioList;
	}
	 
	/**
	 * Recupera gli uffici.
	 * @return lista degli uffici
	 */
	public List<UfficioDTO> getUffici() {
		return uffici;
	}
	
	/**
	 * Imposta gli uffici.
	 * @param uffici
	 */
	public void setUffici(final List<UfficioDTO> uffici) {
		this.uffici = uffici;
	}
 
	/**
	 * Recupera la lista degli uffici.
	 * @return lista degli uffici
	 */
	public List<Nodo> getListaUffici() {
			return listaUffici;
	} 
    
	private void createAnimatedModels() {
		animatedBarChartModel = initBarModel(); 
		animatedBarChartModel.setAnimate(true);  
        animatedBarChartModel.setShowDatatip(false);
        animatedBarChartModel.setExtender("fissaNumeri");  
        animatedBarChartModel.setMouseoverHighlight(false);
    }
	
	private BarChartModel initBarModel() {
		final HorizontalBarChartModel model = new HorizontalBarChartModel();
		final ChartSeries ufficiChart = new ChartSeries();
		for (final Nodo ufficio : listaUffici) {
			ufficiChart.set(ufficio.getDescrizione(), ufficio.getCountUfficio());
			}
	 
		model.addSeries(ufficiChart);
	    model.setStacked(true); 
	     
	    final List<Integer> countMax = new ArrayList<>();
	    for (final Nodo ufficio : listaUffici) {
	        	countMax.add(ufficio.getCountUfficio());
	        }   
        
        final Axis yAxis = model.getAxis(AxisType.Y);
        yAxis.setTickCount(2);

        return model;
    } 
	
	/**
	 * Recupera l'attributo animatedBarChartModel.
	 * @return animatedBarChartModel
	 */
	public BarChartModel getAnimatedBarChartModel() {
        return animatedBarChartModel;
    }
}
