/**
 * 
 */
package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IAooFacadeSRV;
import it.ibm.red.business.service.facade.IOperationDocumentFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * @author APerquoti
 *
 */
@Named(ConstantsWeb.MBean.SIGLA_BEAN)
@ViewScoped
public class SiglaBean extends AbstractBean implements InitHumanTaskInterface {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1604490421586698914L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SiglaBean.class.getName());
	
	/**
	 * Motivo sigla.
	 */
	private String motivoSigla;

	/**
	 * Lista master.
	 */
	private List<MasterDocumentRedDTO> masters;
	
	/**
	 * Utente.
	 */
	private UtenteDTO utente;
	

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		masters = inDocsSelezionati;
		final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sb.getUtente();
		
		final IAooFacadeSRV aooSRV = ApplicationContextProvider.getApplicationContext().getBean(IAooFacadeSRV.class);
		final boolean skipMotivazioneSigla = aooSRV.checkSkipMotivazioneSigla(utente.getIdAoo());
		
		if (skipMotivazioneSigla) {
			FacesHelper.executeJS("PF('dlgGeneric').hide()");
			siglaResponse();
			FacesHelper.executeJS("PF('dlgShowResult').show()");
		}
	}
	

	/* (non-Javadoc)
	 * @see it.ibm.red.web.mbean.AbstractBean#postConstruct()
	 */
	@Override
	protected void postConstruct() {
		// Vuoto
	}

	/**
	 * Esecuzione della response.
	 */
	public void siglaResponse() {
		Collection<EsitoOperazioneDTO> eOpe = new ArrayList<>();
		final Collection<String> wobNumbers = new ArrayList<>();
		
		try {
			final IOperationDocumentFacadeSRV opeDocumentSRV = ApplicationContextProvider.getApplicationContext().getBean(IOperationDocumentFacadeSRV.class);
			final ListaDocumentiBean ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
			
			// Pulisco gli esiti
			ldb.cleanEsiti();
			
			// Raccolgo i wobnumber necessari ad invocare il service 
			for (final MasterDocumentRedDTO m : masters) {
				if (!StringUtils.isNullOrEmpty(m.getWobNumber())) {
					wobNumbers.add(m.getWobNumber());
				}
			}
			
			// Invoke service
			eOpe = opeDocumentSRV.sigla(utente.getFcDTO(), wobNumbers, utente, motivoSigla);
			
			// Imposto gli esiti con i nuovi e eseguo un refresh
			ldb.getEsitiOperazione().addAll(eOpe);
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.SIGLA_BEAN);
			
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la sigla del documento", e);
			showError("Errore durante la sigla del documento");
		}
	}

	/**
	 * @return the motivoSigla
	 */
	public final String getMotivoSigla() {
		return motivoSigla;
	}

	/**
	 * @param motivoSigla the motivoSigla to set
	 */
	public final void setMotivoSigla(final String motivoSigla) {
		this.motivoSigla = motivoSigla;
	}

}
