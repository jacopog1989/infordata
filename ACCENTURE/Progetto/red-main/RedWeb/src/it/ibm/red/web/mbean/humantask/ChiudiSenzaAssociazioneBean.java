package it.ibm.red.web.mbean.humantask;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IChiudiDichiarazioneFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Bean che gestisce la chiusura senza associazione.
 */
@Named(ConstantsWeb.MBean.CHIUDI_SENZA_ASSOCIAZIONE_BEAN)
@ViewScoped
public class ChiudiSenzaAssociazioneBean extends AbstractAssociazioniDecretiResponse {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 4163734780839383396L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ChiudiSenzaAssociazioneBean.class.getName());

	/**
	 * Documento selezionato.
	 */
	MasterDocumentRedDTO docSel;

	/**
	 * Inizializza il bean.
	 * @param inDocsSelezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		documentiSelezionati = inDocsSelezionati;
		docSel = documentiSelezionati.get(0);
		dataDichiarazione = docSel.getDataCreazione();
		nomeFile = docSel.getNomeFile();
	}

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		super.postConstruct();
	}

	/**
	 * Esegue la response associata al bean.
	 */
	@Override
	public void doResponse() {
		EsitoOperazioneDTO eOpe = null;
		String wobNumber = null;
		try {
			final IChiudiDichiarazioneFacadeSRV chiudiDichSRV = ApplicationContextProvider.getApplicationContext().getBean(IChiudiDichiarazioneFacadeSRV.class);
			sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
			utente = sb.getUtente();
			
			//pulisco gli esiti
			ldb.cleanEsiti();

			//prendo il wobnumber
			wobNumber = docSel.getWobNumber();
			eOpe = chiudiDichSRV.chiudiDichiarazione(wobNumber, utente);
			
			//setto gli esiti con i nuovi e eseguo un refresh
			ldb.getEsitiOperazione().add(eOpe);
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.CHIUDI_SENZA_ASSOCIAZIONE_BEAN);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il tentato sollecito della response 'CHIUDI SENZA ASSOCIAZIONE'", e);
			showError("Si è verificato un errore durante il tentato sollecito della response 'CHIUDI SENZA ASSOCIAZIONE'");
		}
	}

}
