/**
 * 
 */
package it.ibm.red.web.mbean.humantask;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IAssegnaUfficioFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * @author APerquoti
 *
 */
@Named(ConstantsWeb.MBean.UFFICIO_PROPONENTE_BEAN)
@ViewScoped
public class UfficioProponenteBean extends AbstractBean implements InitHumanTaskInterface {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 7943480898207480753L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(UfficioProponenteBean.class.getName());
	
	/**
	 * Motivo assegnazione.
	 */
	private String testoMotivoAssegnazione;

	/**
	 * Lista documenti.
	 */
	private List<MasterDocumentRedDTO> masters;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		//Precompilo il motivo assegnazione con un messaggio standard --> come fa NSD
		testoMotivoAssegnazione = "Mancata documentazione cartacea";
	}
	
	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		masters = inDocsSelezionati;
	}
	
	/**
	 * Esegue la response: Assegna Ufficio Proponente aggiornando gli esiti per la corretta visualizzazione a valle delle operazioni.
	 */
	public void uffProponenteResponse() {
		EsitoOperazioneDTO eOpe = null;
		try {
			final IAssegnaUfficioFacadeSRV assegnaUffSRV = ApplicationContextProvider.getApplicationContext().getBean(IAssegnaUfficioFacadeSRV.class);
			final ListaDocumentiBean ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
			final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
			
			//pulisco gli esiti
			ldb.cleanEsiti();
			
			final String wobNumber = masters.get(0).getWobNumber();
			
			//esecuzione del service per l'assegnazione all'ufficio proponente
			eOpe = assegnaUffSRV.assegnaUfficioProponente(sb.getUtente(), wobNumber, testoMotivoAssegnazione);
			
			//setto gli esiti con i nuovi e eseguo un refresh
			ldb.getEsitiOperazione().add(eOpe);
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.UFFICIO_PROPONENTE_BEAN);
			
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il tentato sollecito della response 'INVIA A UFFICIO PROPONENTE'", e);
			showError("Si è verificato un errore durante il tentato sollecito della response 'INVIA A UFFICIO PROPONENTE'");
		}
	}
	
	/**
	 * @return the testoMotivoAssegnazione
	 */
	public String getTestoMotivoAssegnazione() {
		return testoMotivoAssegnazione;
	}

	/**
	 * @param testoMotivoAssegnazione the testoMotivoAssegnazione to set
	 */
	public void setTestoMotivoAssegnazione(final String testoMotivoAssegnazione) {
		this.testoMotivoAssegnazione = testoMotivoAssegnazione;
	}

	
}
