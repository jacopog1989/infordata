package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoApprovazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IVistiFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractVerifyBean;
import it.ibm.red.web.mbean.SessionBean;

/**
 * Bean che gestisce la response di visto.
 */
@Named(ConstantsWeb.MBean.VISTO_BEAN)
@ViewScoped
public class VistoBean extends AbstractVerifyBean {

	/**
	 * serial version UID.
	 */
	private static final long serialVersionUID = -7254988660979136371L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(VistoBean.class.getName());

	/**
	 * Servizio.
	 */
	private IVistiFacadeSRV vistoSRV;

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		setDocumentiSelezionati(inDocsSelezionati);
	}

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		//inizializzo il dropdown menu
		tipologia = new ArrayList<>();
		tipologia.add(TipoApprovazioneEnum.VISTO_POSITIVO);
		tipologia.add(TipoApprovazioneEnum.VISTO_NEGATIVO);
		tipologia.add(TipoApprovazioneEnum.VISTO_CONDIZIONATO);

		vistoSRV = ApplicationContextProvider.getApplicationContext().getBean(IVistiFacadeSRV.class);
	}

	/**
	 * Esegui la respose "Visto".
	 */
	public void vistoResponse() {
		//E' una response per un solo documento (singola)
		EsitoOperazioneDTO eOpe = null;
		String wobNumber = null;
		try {
			final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
			ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
			final UtenteDTO utente = sb.getUtente();
			//pulisco eventuali esiti
			ldb.cleanEsiti();
			//prendo il wobnumber del documento
			wobNumber = documentiSelezionati.get(0).getWobNumber();
			//chiamo il service (singolo doc)
			eOpe = vistoSRV.visto(utente, wobNumber, tipologiaSelezionata, message);

			ldb.getEsitiOperazione().add(eOpe);
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.VISTO_BEAN);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il tentato sollecito della response 'VISTO'", e);
			showError("Si è verificato un errore durante il tentato sollecito della response 'VISTO'");
		}
	}

	
	/**
	 * @return vistoSRV
	 */
	public IVistiFacadeSRV getVistoSRV() {
		return vistoSRV;
	}
	
	/**
	 * Imposta il Service per la response "Visto".
	 * @param vistoSRV
	 */
	public void setVistoSRV(final IVistiFacadeSRV vistoSRV) {
		this.vistoSRV = vistoSRV;
	}
}
