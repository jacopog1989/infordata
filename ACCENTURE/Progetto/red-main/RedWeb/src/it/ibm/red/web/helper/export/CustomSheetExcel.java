package it.ibm.red.web.helper.export;

import java.io.Serializable;
import java.util.List;

/**
 * Custom sheet di un excel.
 */
public class CustomSheetExcel implements Serializable {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 3528608440116186569L;
	
	/**
	 * Nome sheet.
	 */
	private String nameSheet;
	
	/**
	 * Dati.
	 */
	private List<String[]> data;
	
	/**
	 * Headers.
	 */
	private String[] headers;

	/**
	 * Costruttore completo.
	 * @param nameSheet
	 * @param data
	 * @param headers
	 */
	public CustomSheetExcel(final String nameSheet, final List<String[]> data, final String[] headers) { 
		this.nameSheet = nameSheet;
		this.data = data;
		this.headers = headers;
	}

	/**
	 * @return the nameSheet
	 */
	public String getNameSheet() {
		return nameSheet;
	}

	/**
	 * @param nameSheet the nameSheet to set
	 */
	public void setNameSheet(final String nameSheet) {
		this.nameSheet = nameSheet;
	}

	/**
	 * @return the headers
	 */
	public String[] getHeaders() {
		return headers;
	}

	/**
	 * @param headers the headers to set
	 */
	public void setHeaders(final String[] headers) {
		this.headers = headers;
	}
	
	/**
	 * @return the data
	 */
	public List<String[]> getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setResult(final List<String[]> data) {
		this.data = data;
	}
}