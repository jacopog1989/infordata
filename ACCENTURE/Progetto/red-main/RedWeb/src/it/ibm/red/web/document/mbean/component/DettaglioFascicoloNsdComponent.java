package it.ibm.red.web.document.mbean.component;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.AllegatoNsdDTO;
import it.ibm.red.business.dto.DetailFascicoloPregressoDTO;
import it.ibm.red.business.dto.DocumentiFascicoloNsdDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDfFacadeSRV;
import it.ibm.red.web.mbean.AbstractBean;

/**
 * @author VINGENITO
 * 
 */
public class DettaglioFascicoloNsdComponent extends AbstractBean {

	private static final long serialVersionUID = 510047887019619181L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DettaglioFascicoloNsdComponent.class);

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Dettaglio.
	 */
	private DetailFascicoloPregressoDTO detail;

	/**
	 * Root albero.
	 */
	private transient TreeNode root;

	/**
	 * Costruttore del component.
	 * 
	 * @param utenteIn
	 */
	public DettaglioFascicoloNsdComponent(final UtenteDTO utenteIn) {
		try {
			this.utente = utenteIn;
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}
	}
	
	@Override
	protected void postConstruct() {
		// Metodo intenzionalmente vuoto.
	}

	/**
	 * Restituisce l'utente.
	 * 
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'utente.
	 * 
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Restituisce il dettaglio del fascicolo.
	 * 
	 * @return DetailFascicoloPregressoDTO
	 */
	public DetailFascicoloPregressoDTO getDetail() {
		return detail;
	}

	/**
	 * Imposta il dettaglio.
	 * 
	 * @param detail
	 */
	public void setDetail(final DetailFascicoloPregressoDTO detail) {
		this.detail = detail;
	}

	/**
	 * Costruisce il tree includendo la radice.
	 * 
	 * @param fascigli
	 */
	public void creaTreeTable(final List<DocumentiFascicoloNsdDTO> fascigli) {
		root = new DefaultTreeNode("Root", null);
		createAlberaturaFasc(fascigli, root);

	}

	/**
	 * Costruisce l'alberatura per la gestione dei fascicoli.
	 * 
	 * @param fascigli
	 * @param node
	 */
	private void createAlberaturaFasc(final List<DocumentiFascicoloNsdDTO> fascigli, final TreeNode node) {

		for (final DocumentiFascicoloNsdDTO d : fascigli) {
			final DefaultTreeNode figlioDiRoot = new DefaultTreeNode(d, node);
			figlioDiRoot.setSelectable(false);
			d.setFascicolo(true);
			figlioDiRoot.setExpanded(d.getListaDocumenti() != null || d.getListaCartelle() != null);
			if (d.getListaDocumenti() != null) {
				for (final DocumentiFascicoloNsdDTO doc : d.getListaDocumenti()) {
					final DefaultTreeNode docNodo = new DefaultTreeNode(doc, figlioDiRoot);
					docNodo.setSelectable(false);
					docNodo.setExpanded(false);
				}
			}

			if (d.getListaCartelle() != null) {
				createAlberaturaFasc(d.getListaCartelle(), figlioDiRoot);
			}
		}
	}

	/**
	 * Restituisce la root del tree.
	 * 
	 * @return root
	 */
	public TreeNode getRoot() {
		return root;
	}

	/**
	 * Imposta la radice del tree.
	 * 
	 * @param root
	 */
	public void setRoot(final TreeNode root) {
		this.root = root;
	}

	/**
	 * Restituisce lo stremedContent che permette il download del file principale
	 * dell'allegato.
	 * 
	 * @param idDocumento
	 * @return StreamedContent
	 */
	public StreamedContent downloadFilePrincipaleAllegato(final String idDocumento) {
		StreamedContent file = null;
		try {
			final IDfFacadeSRV dfSRV = ApplicationContextProvider.getApplicationContext().getBean(IDfFacadeSRV.class);
			final AllegatoNsdDTO documento = dfSRV.getDocumentoConContent(idDocumento, utente);

			InputStream stream = null;
			if (documento.getContent() != null && documento.getContent().length > 0) {
				stream = new ByteArrayInputStream(documento.getContent());
			} else {
				throw new RedException("Nessun content present");
			}

			file = new DefaultStreamedContent(stream, documento.getContentType(), documento.getNomeFile());
		} catch (final Exception e) {
			if ("Errore nell'accesso al Documentale".equalsIgnoreCase(e.getMessage())) {
				LOGGER.error("Errore nell'accesso al Documentale: ", e);
				showError("Errore nell'accesso al Documentale");
			} else {
				LOGGER.error("Errore durante il download del file: ", e);
				showError("Errore durante il download del file");
			}
		}
		return file;
	}

}
