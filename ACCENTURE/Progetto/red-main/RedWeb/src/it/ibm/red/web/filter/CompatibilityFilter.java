package it.ibm.red.web.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.StringUtils;

/**
 * Servlet Filter implementation class CompatibilityFilter.
 */
public class CompatibilityFilter implements Filter {
	
	/**
	 * Tag compatibilità.
	 */
	private static final String X_UA_COMPATIBLE_TAG_HEADER_NAME = "X-UA-Compatible";
	
	/**
	 * Valore per compatibilità con edge.
	 */
	
	/**
	 * Destroy.
	 *
	 * @see Filter#destroy()
	 */
	@Override
	public void destroy() {
		/**
		 * Questo metodo è lasciato intenzionalmente vuoto.
		 */
	}

	/**
	 * Do filter.
	 *
	 * @param request
	 *            - request
	 * @param response
	 *            - response
	 * @param chain
	 *            - chain
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 * @exception IOException
	 *                - IOException
	 * @exception ServletException
	 *                - ServletException
	 */
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
		String value = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.X_UA_COMPATIBLE_VALUE);
		if (!StringUtils.isNullOrEmpty(value)) {
			((HttpServletResponse) response).addHeader(X_UA_COMPATIBLE_TAG_HEADER_NAME, value);
		}
		// pass the request along the filter chain
		chain.doFilter(request, response);
	}
	
	/**
	 * Init method.
	 * @param fConfig - fConfig
	 * @exception ServletException - ServletException
	 */
	@Override
	public void init(final FilterConfig fConfig) throws ServletException {
		/**
		 * Questo metodo è lasciato intenzionalmente vuoto.
		 */
	}

}
