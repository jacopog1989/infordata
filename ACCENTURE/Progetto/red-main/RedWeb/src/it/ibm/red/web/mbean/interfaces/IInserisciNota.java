package it.ibm.red.web.mbean.interfaces;

import java.io.Serializable;

/**
 * Interfaccia del bean che gestisce la response di inserimento note.
 */
public interface IInserisciNota extends Serializable {
	
	/**
	 * Ottiene il testo della nota.
	 * @return testo della nota
	 */
	String getTestoNota();
	
	/**
	 * Imposta il testo della nota.
	 * @param testoNota testo della nota
	 */
	void setTestoNota(String testoNota);
	
	/**
	 * Ottiene il colore della nota.
	 * @return colore della nota
	 */
	Integer getColoreNota();
	
	/**
	 * Imposta il colore della nota.
	 * @param codiceColore codice del colore
	 */
	void setColoreNota(Integer codiceColore);
	
	/**
	 * Inserisce la response della nota.
	 */
	void inserisciNotaResponse();
	
	/**
	 * Elimina la nota.
	 */
	void eliminaNota();
	
	/**
	 * Inizializza il bean.
	 */
	void close();
}
