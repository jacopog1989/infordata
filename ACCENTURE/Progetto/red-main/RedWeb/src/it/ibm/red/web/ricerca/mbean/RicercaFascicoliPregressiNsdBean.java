package it.ibm.red.web.ricerca.mbean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.event.data.PageEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.annotation.Autowired;

import it.ibm.red.business.dto.DetailFascicoloPregressoDTO;
import it.ibm.red.business.dto.FascicoliPregressiDTO;
import it.ibm.red.business.dto.ParamsRicercaFascicoliPregressiDfDTO;
import it.ibm.red.business.dto.StatoRicercaDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDfFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.DettaglioFascicoloRicercaNsdBean;
import it.ibm.red.web.mbean.interfaces.IUpdatableDetailCaller;

/**
 * @author VINGENITO
 * 
 */
@Named(ConstantsWeb.MBean.RICERCA_FASCICOLI_PREGRESSI_NSD_BEAN)
@ViewScoped
public class RicercaFascicoliPregressiNsdBean extends RicercaAbstractBean<FascicoliPregressiDTO> implements IUpdatableDetailCaller<DetailFascicoloPregressoDTO>  {

	private static final long serialVersionUID = -24522098927874670L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaFascicoliPregressiNsdBean.class.getName());

	/**
	 * Parametri ricerca.
	 */
	private ParamsRicercaFascicoliPregressiDfDTO formRicerca;

	/**
	 * Lista colonne.
	 */
	private List<Boolean> fascColumnsNsd;

	/**
	 * Datatable fascicoli.
	 */
	protected SimpleDetailDataTableHelper<FascicoliPregressiDTO> fascicoliNsdDTH;
 
	/**
	 * Indice pagina.
	 */
	private int pageIndex;

	/**
	 * Servizio.
	 */
	@Autowired
	private IDfFacadeSRV dfSRV;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		super.postConstruct();
		
		fascicoliNsdDTH = new SimpleDetailDataTableHelper<>();
		fascicoliNsdDTH.setMasters(new ArrayList<>());
		fascColumnsNsd = Arrays.asList(true, true, true, true);
		dfSRV = ApplicationContextProvider.getApplicationContext().getBean(IDfFacadeSRV.class);
		init();
	}

	/**
	 * Inizializzazione della pagina di ricerca.
	 */
	@SuppressWarnings("unchecked")
	public void init() {
		try {
			final StatoRicercaDTO stDto = (StatoRicercaDTO) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.STATO_RICERCA, true);
			if (stDto != null) {
				final Collection<FascicoliPregressiDTO> searchResult = (Collection<FascicoliPregressiDTO>) stDto.getRisultatiRicerca();
				formRicerca = (ParamsRicercaFascicoliPregressiDfDTO) stDto.getFormRicerca();

				fascicoliNsdDTH.setMasters(searchResult);
				fascicoliNsdDTH.selectFirst(true);

				selectDetail(fascicoliNsdDTH.getCurrentMaster());
			} else {
				formRicerca = new ParamsRicercaFascicoliPregressiDfDTO();
			}

		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Non è stato possibile recuperare i risultati di ricerca nsd");
		}
	}

	/**
	 * Gestisce la selezione della riga dal datatable dei fascicoli.
	 * @param se
	 */
	public final void rowSelectorFascicoli(final SelectEvent se) {
		fascicoliNsdDTH.rowSelector(se);
		selectDetail(fascicoliNsdDTH.getCurrentMaster());
	}

	/**
	 * Gestisce la selezione del dettaglio.
	 * @param m master selezionato
	 */
	@Override
	protected void selectDetail(final FascicoliPregressiDTO m) {
		final DettaglioFascicoloRicercaNsdBean bean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_RICERCA_NSD_BEAN);
		if (m == null) {
			bean.unsetDetail();
			return;
		}
		bean.setDetail(m);
		bean.setCaller(this);
	}

	/**
	 * Reimposta i parametri della ricerca in modo da uguagliare i parametri dell'ultima ricerca eseguita.
	 */
	public void ripetiRicercaFascicoliPregressiNsd() {
		try { 
			sessionBean.getRicercaFormContainer().setFascicoliDF(formRicerca); 
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Gestisce la selezione e la deselezione della colonna.
	 * @param event
	 */
	public final void onColumnToggleFascicoliNsd(final ToggleEvent event) {
		fascColumnsNsd.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
	}

	/**
	 * Restituisce il datatable helper la gestione dei fascicoli.
	 * @return
	 */
	public final SimpleDetailDataTableHelper<FascicoliPregressiDTO> getFascicoliNsdDTH() {
		return fascicoliNsdDTH;
	}

	/**
	 * Imposta il datatable helper per la gestione dei fascicoli.
	 * @param fascicoliNsdDTH
	 */
	public final void setFascicoliNsdDTH(final SimpleDetailDataTableHelper<FascicoliPregressiDTO> fascicoliNsdDTH) {
		this.fascicoliNsdDTH = fascicoliNsdDTH;
	}

	@Override
	public void updateDetail(final DetailFascicoloPregressoDTO detail) {
		// Metodo intenzionalmente vuoto.
	}

	/**
	 * Restituisce la lista degli stati delle colonne.
	 * @return fascColumnsNsd
	 */
	public List<Boolean> getFascColumnsNsd() {
		return fascColumnsNsd;
	}

	/**
	 * Imposta la lista degli stati delle colonne.
	 * @param fascColumnsNsd
	 */
	public void setFascColumnsNsd(final List<Boolean> fascColumnsNsd) {
		this.fascColumnsNsd = fascColumnsNsd;
	}

	/**
	 * Restituisce il fascicolo selezionato.
	 * @return master del fascicolo selezionato
	 */
	@Override
	public FascicoliPregressiDTO getSelectedDocument() { 
		return fascicoliNsdDTH.getCurrentMaster();
	}

	/**
	 * Restituisce il prefisso del nome file excel.
	 * @return prefisso nome file estrazione
	 */
	@Override
	protected String getNomeRicerca4Excel() {
		return "ricercafascicoliNsd_";
	}

	//PAGINAZIONE START 
	/**
	 * Gestisce la paginazione.
	 * @param event
	 */
	public final void pageListener(final PageEvent event) {
		pageIndex = event.getPage(); 
		final DataTable datatable = (DataTable) getClickedComponent(event); 
		if (pageIndex + 1 >=  datatable.getPageCount() && (sessionBean.getNumPaginaAttualeFascicolo() + 1) <= Integer.parseInt(sessionBean.getNumPagineTotaliDaRicercaFasc())) {
			getNewPage();
		} 
	}

	private Boolean getNewPage() {
		Boolean output = false; 
		Collection<FascicoliPregressiDTO> tmp = null; 
		sessionBean.setNumPaginaAttualeFascicolo(sessionBean.getNumPaginaAttualeFascicolo() + 1);
		sessionBean.setLastRowNumFascicolo(sessionBean.getNumPaginaAttualeFascicolo());

		final int maxNumPiuUno = sessionBean.getMaxRowPageFascicoliNsd() + 1;
		 
		
		tmp = dfSRV.ricercaFascicoliPregressi(sessionBean.getFascicoliDF(), sessionBean.getUtente(), sessionBean.getLastRowNumFascicolo(), maxNumPiuUno);
		if (tmp != null && !tmp.isEmpty()) { 
			fascicoliNsdDTH.getMasters().addAll(tmp); 
			output = true;
		}
		return output;
	}  

	/**
	 * Restituisce l'indice della pagina.
	 * @return l'indice della pagina
	 */
	public int getPageIndex() {
		return pageIndex;
	}

	/**
	 * Imposta l'indice della pagina.
	 * @param pageIndex
	 */
	public void setPageIndex(final int pageIndex) {
		this.pageIndex = pageIndex;
	}

}
