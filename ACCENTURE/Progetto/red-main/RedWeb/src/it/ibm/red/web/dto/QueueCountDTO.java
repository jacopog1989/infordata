/**
 * 
 */
package it.ibm.red.web.dto;

import it.ibm.red.business.enums.DocumentQueueEnum;

/**
 * @author APerquoti
 *
 */
public class QueueCountDTO extends it.ibm.red.business.dto.AbstractDTO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -259942742339101554L;

	/**
	 * Coda.
	 */
	private DocumentQueueEnum queue;

	/**
	 * Conteggio.
	 */
	private Integer count;
	
	/**
	 * Costruttore del DTO.
	 */
	public QueueCountDTO() {
		super();
	}
	
	/**
	 * Costruttore del DTO.
	 * 
	 * @param q
	 *            coda documento
	 * @param c
	 *            numero di elementi
	 */
	public QueueCountDTO(final DocumentQueueEnum q, final Integer c) {
		super();
		queue = q;
		count = c;
	}

	/**
	 * @return the queue
	 */
	public final DocumentQueueEnum getQueue() {
		return queue;
	}

	/**
	 * @return the count
	 */
	public final Integer getCount() {
		return count;
	}
	
	/**
	 * Imposta il count.
	 * 
	 * @param inCount
	 */
	public void setCount(final Integer inCount) {
		this.count = inCount;
	}

	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((queue == null) ? 0 : queue.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final QueueCountDTO other = (QueueCountDTO) obj;
		
		return queue == other.queue;
	}
}
