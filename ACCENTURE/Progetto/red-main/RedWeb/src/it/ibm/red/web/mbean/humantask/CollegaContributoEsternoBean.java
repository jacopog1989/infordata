package it.ibm.red.web.mbean.humantask;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IContributoFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.mbean.component.IndiceDiClassificazioneGenericComponent;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.component.CercaFascicoloComponent;
import it.ibm.red.web.mbean.interfaces.ISelezionaTitolarioChiamante;
import it.ibm.red.web.mbean.interfaces.IUpdatableDetailCaller;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * @author m.crescentini
 *
 */
@Named(MBean.COLLEGA_CONTRIBUTO_ESTERNO_BEAN)
@ViewScoped
public class CollegaContributoEsternoBean extends AbstractBean implements InitHumanTaskInterface, IUpdatableDetailCaller<FascicoloDTO>, ISelezionaTitolarioChiamante {

	private static final long serialVersionUID = -361970520692874816L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CollegaContributoEsternoBean.class);

	/**
	 * Info utente.
	 */
	private UtenteDTO utente;

	/**
	 * Servizio contributi.
	 */
	private IContributoFacadeSRV contributoSRV;

	/**
	 * Documento selezionato.
	 */
	private MasterDocumentRedDTO docSelezionato;

	/**
	 * Fascicolo.
	 */
	private FascicoloDTO fascicolo;

	/**
	 * Titolario.
	 */
	private TitolarioDTO titolario;

	/**
	 * Componente ricerca fascicoli.
	 */
	private CercaFascicoloComponent cercaFascicoloCmp;

	/**
	 * Componente indice di classificazione.
	 */
	private IndiceDiClassificazioneGenericComponent indiceDiClassificazioneCmp;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		utente = ((SessionBean) FacesHelper.getManagedBean(MBean.SESSION_BEAN)).getUtente();
		contributoSRV = ApplicationContextProvider.getApplicationContext().getBean(IContributoFacadeSRV.class);

		cercaFascicoloCmp = new CercaFascicoloComponent(MBean.COLLEGA_CONTRIBUTO_ESTERNO_BEAN, utente);
		indiceDiClassificazioneCmp = new IndiceDiClassificazioneGenericComponent(MBean.COLLEGA_CONTRIBUTO_ESTERNO_BEAN, utente);
	}

	/**
	 * @see
	 * it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface#initBean(java.util.
	 * List)
	 * @param inDocsSelezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		try {
			setDocSelezionato(inDocsSelezionati.get(0));

			fascicolo = new FascicoloDTO();
			titolario = new TitolarioDTO();
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Esecuzione della response.
	 */
	public void executeResponse() {
		try {
			if (StringUtils.isNotBlank(fascicolo.getIdFascicolo()) && StringUtils.isNotBlank(titolario.getIndiceClassificazione())) {
				final ListaDocumentiBean ldb = FacesHelper.getManagedBean(MBean.LISTA_DOCUMENTI_BEAN);
				// Si puliscono gli esiti
				ldb.cleanEsiti();

				final EsitoOperazioneDTO esito = contributoSRV.collegaContributo(docSelezionato.getWobNumber(), docSelezionato.getDocumentTitle(), fascicolo, titolario,
						utente);

				// Impostazione dell'esito e chiusura della dialog
				ldb.getEsitiOperazione().add(esito);
				FacesHelper.executeJS("PF('dlgGeneric').hide();PF('dlgShowResult').show();");
				ldb.destroyBeanViewScope(MBean.COLLEGA_CONTRIBUTO_ESTERNO_BEAN);
			} else {
				showError("Scegliere un fascicolo e l'indice di classificazione della messa agli atti automatica per poter procedere con il collegamento del contributo.");
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Gestisce la selezione del fascicolo dalla dialog di ricerca dei fascicoli.
	 * 
	 * @param fascicoloSelezionato
	 */
	@Override
	public void updateDetail(final FascicoloDTO fascicoloSelezionato) {
		fascicolo = fascicoloSelezionato;
	}

	/**
	 * Gestisce la selezione del titolario dalla dialog di selezione del titolario.
	 * 
	 * @param titolarioSelezionato
	 */
	@Override
	public void aggiornaIndiceClassificazione(final TitolarioDTO titolarioSelezionato) {
		titolario = titolarioSelezionato;
	}

	/**
	 * @return
	 */
	public MasterDocumentRedDTO getDocSelezionato() {
		return docSelezionato;
	}

	/**
	 * @param docSelezionato
	 */
	public void setDocSelezionato(final MasterDocumentRedDTO docSelezionato) {
		this.docSelezionato = docSelezionato;
	}

	/**
	 * @return the fascicolo
	 */
	public FascicoloDTO getFascicolo() {
		return fascicolo;
	}

	/**
	 * @param fascicolo the fascicolo to set
	 */
	public void setFascicolo(final FascicoloDTO fascicolo) {
		this.fascicolo = fascicolo;
	}

	/**
	 * @return the titolario
	 */
	public TitolarioDTO getTitolario() {
		return titolario;
	}

	/**
	 * @param titolario the titolario to set
	 */
	public void setTitolario(final TitolarioDTO titolario) {
		this.titolario = titolario;
	}

	/**
	 * @return the cercaFascicoloComponent
	 */
	public CercaFascicoloComponent getCercaFascicoloCmp() {
		return cercaFascicoloCmp;
	}

	/**
	 * @return the indiceDiClassificazioneComponent
	 */
	public IndiceDiClassificazioneGenericComponent getIndiceDiClassificazioneCmp() {
		return indiceDiClassificazioneCmp;
	}
}