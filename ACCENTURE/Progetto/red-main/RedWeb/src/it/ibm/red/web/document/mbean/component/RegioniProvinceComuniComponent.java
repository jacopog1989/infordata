package it.ibm.red.web.document.mbean.component;

import java.util.List;

import it.ibm.red.business.dto.ComuneDTO;
import it.ibm.red.business.dto.ProvinciaDTO;
import it.ibm.red.business.dto.RegioneDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IComuneFacadeSRV;
import it.ibm.red.business.service.facade.IProvinciaFacadeSRV;
import it.ibm.red.business.service.facade.IRegioneFacadeSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * 
 * @author SLac
 *
 */
public class RegioniProvinceComuniComponent extends AbstractComponent {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 2977906706805061882L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RegioniProvinceComuniComponent.class);
	
	/**
	 * @param query
	 * @return
	 */
	public static final  List<RegioneDTO> loadRegioni(final String query) {
		List<RegioneDTO> regioni = null;
		try {
			final IRegioneFacadeSRV regioneSRV = ApplicationContextProvider.getApplicationContext().getBean(IRegioneFacadeSRV.class);
			regioni = regioneSRV.getRegioni(query);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}
		return regioni;
	}

	/**
	 * @param query
	 * @param regione
	 * @return
	 */
	public static final List<ProvinciaDTO> loadProvinceAdd(final String query) {
		List<ProvinciaDTO> province  = null; 
		try {
			final IProvinciaFacadeSRV provinciaSRV = ApplicationContextProvider.getApplicationContext().getBean(IProvinciaFacadeSRV.class);
			province = provinciaSRV.getProvFromRegioneAndDesc(query);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}
		return province;
	}

	/**
	 * @param query
	 * @param contatto
	 * @return
	 */
	public static final List<ComuneDTO> loadComuniAdd(final String query, final ProvinciaDTO provincia) {
		List<ComuneDTO> list = null;
		try {
			if (provincia != null && !StringUtils.isNullOrEmpty(provincia.getIdProvincia())) {
				final IComuneFacadeSRV comuneSRV = ApplicationContextProvider.getApplicationContext().getBean(IComuneFacadeSRV.class);
				list = comuneSRV.getComuni(Long.valueOf(provincia.getIdProvincia()), query);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}

		return list;
	}
	
	
	/**
	 * @param contatto
	 */
	public static void onSelectRegione(final Contatto contatto) {
		contatto.setComuneObj(new ComuneDTO());
		contatto.setProvinciaObj(new ProvinciaDTO());
	}

	/**
	 * @param contatto
	 */
	public static void onSelectProvincia(final Contatto contatto) {
		contatto.setComuneObj(new ComuneDTO());
		contatto.setIdComuneIstat(null);
	}
	
}
