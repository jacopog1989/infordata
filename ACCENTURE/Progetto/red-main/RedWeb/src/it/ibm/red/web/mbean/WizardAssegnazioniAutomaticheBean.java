package it.ibm.red.web.mbean;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.AssegnatarioDTO;
import it.ibm.red.business.dto.AssegnazioneAutomaticaCapSpesaDTO;
import it.ibm.red.business.dto.CapitoloSpesaDTO;
import it.ibm.red.business.dto.CapitoloSpesaMetadatoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipologiaNodoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IAssegnazioneAutomaticaCapSpesaFacadeSRV;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.mbean.component.AssCompetenzaConUtentiOrganigrammaComponent;
import it.ibm.red.web.document.mbean.interfaces.IAssOrganigrammaComponent;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.interfaces.IPopupComponent;

/**
 * Bean che gestisce le assegnazioni automatiche per tipologia documento.
 * @author SimoneLungarella
 */
@Named(ConstantsWeb.MBean.WIZARD_ASSEGNAZIONI_AUTOMATICHE_BEAN)
@ViewScoped
public class WizardAssegnazioniAutomaticheBean extends AbstractBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 3490651971157021252L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(WizardAssegnazioniAutomaticheBean.class.getName());
	

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Datatable assegnazioni.
	 */
	private SimpleDetailDataTableHelper<AssegnazioneAutomaticaCapSpesaDTO> assegnazioniDTH;
	
	/**
	 * Datatable assegnazioni file.
	 */
	private SimpleDetailDataTableHelper<AssegnazioneAutomaticaCapSpesaDTO> assegnazioniFileDTH;
	
	/**
	 * Componnet competenza organigramma.
	 */
	private IAssOrganigrammaComponent assCompetenzaOrganigrammaCmp;
	
	/**
	 * Nuova assegnazione.
	 */
	private AssegnazioneAutomaticaCapSpesaDTO nuovaAssegnazione;
	
	/**
	 * Lista nuove assegnazioni.
	 */
	private List<AssegnazioneAutomaticaCapSpesaDTO> nuoveAssegnazioni;
	
	/**
	 * Servizio.
	 */
	private IAssegnazioneAutomaticaCapSpesaFacadeSRV assegnazioneAutomaticaSRV;
	
	/**
	 * Capilo spesa selezionato.
	 */
	private CapitoloSpesaMetadatoDTO capitoloSpesaSelected;
	
	/**
	 * Componente elimina assegnazione.
	 */
	private IPopupComponent popupEliminaAssegnazione;
	
	/**
	 * Componente elimina assegnazione massiva.
	 */
	private IPopupComponent popupEliminazioneMassivaAssegnazioni;
	
	/**
	 * Nome file nuove assegnazioni.
	 */
	private String nomeFileNuoveAssegnazioni;
	
	/**
	 * File nuove assegnazioni.
	 */
	private transient UploadedFile fileNuoveAssegnazioni;
	
	/**
	 * Post construct del bean.
	 * 
	 * @see it.ibm.red.web.mbean.AbstractBean#postConstruct().
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		LOGGER.info(ConstantsWeb.MBean.WIZARD_ASSEGNAZIONI_AUTOMATICHE_BEAN + " ===> postConstruct");
		final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sb.getUtente();
		
		// Inizializzazione del component per la gestione dell'organigramma di assegnazione
		assCompetenzaOrganigrammaCmp = new AssCompetenzaConUtentiOrganigrammaComponent(utente, false, false);
		
		assegnazioneAutomaticaSRV = ApplicationContextProvider.getApplicationContext().getBean(IAssegnazioneAutomaticaCapSpesaFacadeSRV.class);
		caricaListaAssegnazioni();
		nuovaAssegnazione = new AssegnazioneAutomaticaCapSpesaDTO();
		nuoveAssegnazioni = new ArrayList<>();
		
		popupEliminazioneMassivaAssegnazioni = new IPopupComponent() {
			
			/**
			 * La costante serialVersionUID.
			 */
			private static final long serialVersionUID = 1L;
			
			@Override
			public void conferma() {
				eliminaTutteAssegnazioni();
			}
			
			@Override
			public void annulla() {
				// Non deve fare nulla
			}
		};
		
		preparaMascheraNuovaAssegnazione();
	}
	
	private void caricaListaAssegnazioni() {
		assegnazioniDTH = new SimpleDetailDataTableHelper<>();
		assegnazioniDTH.setMasters(new ArrayList<>());
		
		final List<AssegnazioneAutomaticaCapSpesaDTO> masterAssegnazioniList = assegnazioneAutomaticaSRV.getByIdAoo(utente.getIdAoo());
		
		if (!CollectionUtils.isEmpty(masterAssegnazioniList)) {
			assegnazioniDTH.setMasters(masterAssegnazioniList);
		}
	}
	

	/**
	 * Prepara l'organigramma e il DTO atto a contenere la nuova assegnazione automatica.
	 */
	public void preparaMascheraNuovaAssegnazione() {
		assCompetenzaOrganigrammaCmp.reset();
		nuovaAssegnazione = new AssegnazioneAutomaticaCapSpesaDTO(new CapitoloSpesaDTO(), new AssegnatarioDTO());
		capitoloSpesaSelected = new CapitoloSpesaMetadatoDTO();
	}
	
	
	/**
	 * Prepara la maschera per il carimento massivo da file delle assegnazioniAutomatiche.
	 */
	public void preparaMascheraCaricaAssegnazioniDaFile() {
		fileNuoveAssegnazioni = null;
		nomeFileNuoveAssegnazioni = Constants.EMPTY_STRING;
		assegnazioniFileDTH = new SimpleDetailDataTableHelper<>();
	}
	
	
	/**
	 * Gestisce l'upload del file contenente le nuove assegnazioni automatiche da inserire in EVO.
	 * 
	 * @param event
	 */
	public void gestisciUploadFileNuoveAssegnazioni(final FileUploadEvent event) {
		final String extension = (event.getFile() != null) ? FilenameUtils.getExtension(event.getFile().getFileName()) : "";
		final String mimeType4Ext = FileUtils.getMimeType(extension);
		
		if (event.getFile() != null && (event.getFile().getContentType().equalsIgnoreCase(Constants.ContentType.XLS) 
				|| event.getFile().getContentType().equalsIgnoreCase(Constants.ContentType.XLSX)
				|| mimeType4Ext.equalsIgnoreCase(Constants.ContentType.XLS)
				|| mimeType4Ext.equalsIgnoreCase(Constants.ContentType.XLSX))) {
			try {
				fileNuoveAssegnazioni = event.getFile();
				nomeFileNuoveAssegnazioni = fileNuoveAssegnazioni.getFileName();
				
				// Estrazione dal file e verifica delle assegnazioni automatiche
				assegnazioniFileDTH.setMasters(
						assegnazioneAutomaticaSRV.estraiEValidaAssegnazioneAutomaticheDaExcel(
								fileNuoveAssegnazioni.getContents(), null, utente.getIdAoo()));
				
			} catch (final Exception e) {
				showError(e);
			}
		} else {
			showError("Il file deve essere in formato Excel");
		}
	}
	
	
	/**
	 * Azione di eliminazione di un'assegnazione automatica agganciata al pulsante presente su ciascuna riga del datatable.
	 */
	public void eliminaAssegnazione(final ActionEvent event) {
		try {
			
			final AssegnazioneAutomaticaCapSpesaDTO ass = getDataTableClickedRow(event);
			// Si procede con l'eliminazione dell'assegnazione automatica selezionata
			assegnazioneAutomaticaSRV.elimina(ass, utente.getIdAoo());
			
			// Si rimuove l'assegnazione automatica dal datatable
			if (!CollectionUtils.isEmpty(assegnazioniDTH.getFilteredMasters())) {
				assegnazioniDTH.getFilteredMasters().remove(ass);
			} else {
				assegnazioniDTH.getMasters().remove(ass);
			}
			
			// Si mostra il messaggio di avvenuta operazione all'utente
			showInfoMessage("Eliminazione avvenuta con successo");
		} catch (final RedException e) {
			LOGGER.error("Errore nell'eliminazione dell'assegnazione automatica", e);
			showError("Errore nell'eliminazione dell'assegnazione automatica");
		}
	}
	
	
	/**
	 * Azione di eliminazione massiva delle assegnazioni automatiche agganciata al pulsante presente in maschera.
	 */
	public void eliminaTutteAssegnazioni() {
		try {
			// Si procede con l'eliminazione massiva delle assegnazioni automatiche
			assegnazioneAutomaticaSRV.eliminaTutte(utente.getIdAoo());
			
			// Si reinizializza il datatable
			assegnazioniDTH = new SimpleDetailDataTableHelper<>();
			
			// Si reinizializza la lista dei master
			assegnazioniDTH.setMasters(new ArrayList<>());
			
			// Si mostra il messaggio di avvenuta operazione all'utente
			showInfoMessage("Eliminazione massiva avvenuta con successo");
		} catch (final RedException e) {
			showError("Errore nell'eliminazione massiva delle assegnazioni automatiche", e);
		}
	}
	
	
	/**
	 * Azione di registrazione di un'assegnazione automatica agganciata al pulsante di registrazione presente nella dialog di inserimento.
	 */
	public void registraAssegnazione() {
		if (StringUtils.isNotBlank(capitoloSpesaSelected.getCapitoloSelected()) && nuovaAssegnazione.getCapitoloSpesa() != null) {
			nuovaAssegnazione.getCapitoloSpesa().setCodice(capitoloSpesaSelected.getCapitoloSelected());
			for (final AssegnazioneAutomaticaCapSpesaDTO assegnazione : assegnazioniDTH.getMasters()) {
				if (assegnazione.getCapitoloSpesa().getCodice().equals(nuovaAssegnazione.getCapitoloSpesa().getCodice())) {
					showInfoMessage("Assegnazione automatica sul capitolo spesa: " + assegnazione.getCapitoloSpesa().getCodice() + " già esistente.");
					return;
				}
			}
		}
		
		if (assCompetenzaOrganigrammaCmp.checkSelezionato() && StringUtils.isNotBlank(nuovaAssegnazione.getCapitoloSpesa().getCodice())) {
			
			nuovaAssegnazione.getAssegnatario().setIdUfficio(assCompetenzaOrganigrammaCmp.getSelected().getIdNodo());
			nuovaAssegnazione.getAssegnatario().setDescrizioneUfficio(assCompetenzaOrganigrammaCmp.getDescrizioneNodoSelected());
			
			if (TipologiaNodoEnum.UTENTE.equals(assCompetenzaOrganigrammaCmp.getSelected().getTipoNodo())) {
				nuovaAssegnazione.getAssegnatario().setDescrizioneUtente(assCompetenzaOrganigrammaCmp.getDescrizioneUtenteSelected());
				nuovaAssegnazione.getAssegnatario().setIdUtente(assCompetenzaOrganigrammaCmp.getSelected().getIdUtente());
			}
			
			try {
				assegnazioneAutomaticaSRV.inserisci(
						nuovaAssegnazione.getCapitoloSpesa().getCodice(), utente.getIdAoo(), 
						nuovaAssegnazione.getAssegnatario().getIdUfficio(), nuovaAssegnazione.getAssegnatario().getIdUtente());
				
				// Si aggiorna il contenuto del datatable
				caricaListaAssegnazioni();
				preparaMascheraNuovaAssegnazione();
				
				FacesHelper.update("centralSectionForm:dtAssAuto");
				showInfoMessage("Nuova assegnazione automatica inserita");
			} catch (final Exception e) {
				LOGGER.error(e.getMessage(), e);
				showError(e);
			}
		} else {
			showError("Inserire i dati obbligatori per l'inserimento della nuova assegnazione automatica");
		}
	}
	
	
	/**
	 * Effettua il caricamento massivo da file delle assegnazioni automatiche ritenute valide.
	 */
	public void registraNuoveAssegnazioniDaFile() {
		try {
			// Si filtrano le assegnazioni automatiche valide
			final List<AssegnazioneAutomaticaCapSpesaDTO> assegnazioniAutomaticheDaFileValide = assegnazioniFileDTH.getMasters().stream()
					.filter(AssegnazioneAutomaticaCapSpesaDTO::isValida)
					.collect(Collectors.toList());
			
			// Si inserisce la nuova assegnazione automatica
			assegnazioneAutomaticaSRV.inserisciMultiple(assegnazioniAutomaticheDaFileValide, utente.getIdAoo());
			
			// Si aggiorna il contenuto del datatable
			caricaListaAssegnazioni();
			
			// Si nasconde la dialog di inserimento massivo
			FacesHelper.executeJS("PF('wdgCaricaAssegnazioniAutomaticheDlg').hide()");
			
			// Si aggiorna il datatable
			FacesHelper.update("centralSectionForm:dtAssAuto");
			
			// Si mostra il messaggio di avvenuta operazione all'utente
			showInfoMessage("Nuove assegnazioni automatiche inserite");
		} catch (final Exception e) {
			showError(e);
		}
	}
	
	
	/**
	 * @return
	 */
	public String getTotLabel() {
		String totLabel;
		
		if (!CollectionUtils.isEmpty(assegnazioniDTH.getFilteredMasters())) {
			totLabel = Constants.EMPTY_STRING + assegnazioniDTH.getFilteredMasters().size();
		} else {
			totLabel = Constants.EMPTY_STRING + assegnazioniDTH.getMasters().size();
		}
		
		return totLabel;
	}


	/**
	 * @return the utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}
		

	/**
	 * @return the assCompetenzaOrganigrammaCmp
	 */
	public IAssOrganigrammaComponent getAssCompetenzaOrganigrammaCmp() {
		return assCompetenzaOrganigrammaCmp;
	}
	

	/**
	 * @return the assegnazioniAutomaticheDTH
	 */
	public SimpleDetailDataTableHelper<AssegnazioneAutomaticaCapSpesaDTO> getAssegnazioniDTH() {
		return assegnazioniDTH;
	}


	/**
	 * @return the assegnazioniFileDTH
	 */
	public SimpleDetailDataTableHelper<AssegnazioneAutomaticaCapSpesaDTO> getAssegnazioniFileDTH() {
		return assegnazioniFileDTH;
	}

	
	/**
	 * @return the nuovaAssegnazioneAutomatica
	 */
	public AssegnazioneAutomaticaCapSpesaDTO getNuovaAssegnazione() {
		return nuovaAssegnazione;
	}


	/**
	 * @return the capitoloSpesaSelected
	 */
	public CapitoloSpesaMetadatoDTO getCapitoloSpesaSelected() {
		return capitoloSpesaSelected;
	}
	

	/**
	 * @return the nuoveAssegnazioniAutomatiche
	 */
	public List<AssegnazioneAutomaticaCapSpesaDTO> getNuoveAssegnazioni() {
		return nuoveAssegnazioni;
	}

	
	/**
	 * @return the popupEliminaAssegnazione
	 */
	public IPopupComponent getPopupEliminaAssegnazione() {
		return popupEliminaAssegnazione;
	}

	
	/**
	 * @return the popupEliminazioneMassivaAssegnazioni
	 */
	public IPopupComponent getPopupEliminazioneMassivaAssegnazioni() {
		return popupEliminazioneMassivaAssegnazioni;
	}

	
	/**
	 * @return the nomeFileNuoveAssegnazioni
	 */
	public String getNomeFileNuoveAssegnazioni() {
		return nomeFileNuoveAssegnazioni;
	}

	/**
	 * @param nomeFileNuoveAssegnazioni the nomeFileNuoveAssegnazioni to set
	 */
	public void setNomeFileNuoveAssegnazioni(final String nomeFileNuoveAssegnazioni) {
		this.nomeFileNuoveAssegnazioni = nomeFileNuoveAssegnazioni;
	}

	
	/**
	 * @return the fileNuoveAssegnazioni
	 */
	public UploadedFile getFileNuoveAssegnazioni() {
		return fileNuoveAssegnazioni;
	}

	
	/**
	 * 
	 * @param fileNuoveAssegnazioni the fileNuoveAssegnazioni to set
	 */
	public void setFileNuoveAssegnazioni(final UploadedFile fileNuoveAssegnazioni) {
		this.fileNuoveAssegnazioni = fileNuoveAssegnazioni;
	}
	
}