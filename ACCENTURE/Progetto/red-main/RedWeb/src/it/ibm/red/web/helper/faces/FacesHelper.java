package it.ibm.red.web.helper.faces;

import java.util.Iterator;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.NoneScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.primefaces.PrimeFaces;

import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.enums.NavigationTokenEnum;

/**
 * The Class FacesHelper.
 *
 * @author CPIERASC
 * 
 *         Helper per gestire JSF.
 */
@NoneScoped
public final class FacesHelper {

	/**
	 * Costruttore.
	 */
	private FacesHelper() {
	}

	/**
	 * Restituisce il request context path.
	 * @return RequestContextPath
	 */
	public static String getRequestContextPath() {
		return FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
	}

	/**
	 * Metodo per l'esecuzione di javascript su richiesta del server.
	 * 
	 * @param js	stringa rappresentante il codice javascript
	 */
	public static void executeJS(final String js) {
		PrimeFaces.current().executeScript(js);
	}

	/**
	 * *************************************************************************
	 * ********************************************************************.
	 *
	 * @param summary
	 *            the summary
	 * @param detail
	 *            the detail
	 * @param mse
	 *            the mse
	 */
	/*                                                              Messages                                                                        */
	/************************************************************************************************************************************************/

	/**
	 * Messaggio per la generazione di un messaggio JSF.
	 * 
	 * @param summary	titolo del messaggio
	 * @param detail	dettaglio del messaggio
	 * @param mse		enum che rappresenta la severity da fornire al messaggio
	 */
	public static void showMessage(final String summary, final String detail, final MessageSeverityEnum mse) {
		showMessage(null, summary, detail, mse);
	}

	/**
	 * Metodo per la generazione di un messaggio d'errore a partire da una eccezione.
	 * 
	 * @param e	l'eccezione di partenza
	 */
	public static void showError(final Throwable e) {
		showError(null, e);
	}
	
	/**
	 * Metodo per la generazione di un messaggio d'errore a partire da una eccezione.
	 * 
	 * @param e	l'eccezione di partenza
	 */
	public static void showError(final String clientId, final Throwable e) {
		final String summary = e.getClass().getSimpleName();
		final String detail = e.getMessage();
		final MessageSeverityEnum mse = MessageSeverityEnum.ERROR;
		showMessage(clientId, summary, detail, mse);
	}
	
	/**
	 * Messaggio per la generazione di un messaggio JSF.
	 * 
	 * @param clientId	componente associato al messaggio
	 * @param summary	titolo del messaggio
	 * @param detail	dettaglio del messaggio
	 * @param mse		enum che rappresenta la severity da fornire al messaggio
	 */
	public static void showMessage(final String clientId, final String summary, final String detail, final MessageSeverityEnum mse) {
		FacesContext.getCurrentInstance().addMessage(clientId, new FacesMessage(mse.getSeverity(), summary, detail));
	}

	/**
	 * Cancellazione dei messaggi JSF.
	 */
	public static void clearMessages() {        
    	final Iterator<FacesMessage> iter = FacesContext.getCurrentInstance().getMessages();
    	while (iter.hasNext()) {
    		iter.remove();
    	}
    }

	/**
	 * Getter del DTO contenente le informazioni sui messaggi JSF.
	 * 
	 * @return	DTO delle informazioni sui messaggi JSF
	 */
	public static FacesMessagesInfoDTO getMessagesInfo() {      
		final FacesMessagesInfoDTO output = new FacesMessagesInfoDTO();
		Integer nInfo = 0;
		Integer nWarn = 0;
		Integer nError = 0;
    	final Iterator<FacesMessage> iter = FacesContext.getCurrentInstance().getMessages();
    	while (iter.hasNext()) {
    		final FacesMessage msg = iter.next();
    		if (msg.getSeverity().equals(FacesMessage.SEVERITY_INFO)) {
    			nInfo++;
    		} else if (msg.getSeverity().equals(FacesMessage.SEVERITY_WARN)) {
    			nWarn++;
    		} else {
    			nError++;
    		}
    	}
    	output.setnError(nError);
    	output.setnInfo(nInfo);
    	output.setnWarn(nWarn);
    	return output;
    }
    
	/**
	 * *************************************************************************
	 * ********************************************************************.
	 *
	 * @param key
	 *            the key
	 * @param remove
	 *            the remove
	 * @return the object from session
	 */
	/*                                                          SESSION/BEAN                                                                        */
	/************************************************************************************************************************************************/
	
	/**
	 * Metodo per il recupero di un oggetto dalla sessione fornendo il suo identificativo (eventualmente rimuovendolo).
	 * 
	 * @param key		chiave dell'oggetto
	 * @param remove	indica se rimuovere o meno dalla sessione l'oggetto
	 * @return	l'oggetto recuperato
	 */
	public static Object getObjectFromSession(final String key, final Boolean remove) {
		Object output = null;
		final FacesContext ctx = FacesContext.getCurrentInstance();
		if (ctx != null) {
			output = ctx.getExternalContext().getSessionMap().get(key);
			if (Boolean.TRUE.equals(remove)) {
				ctx.getExternalContext().getSessionMap().remove(key);
			}
		}
		return output;
	}
	
	/**
	 * Inserisce in sessione un oggetto fornendo l'oggetto ed il suo identificativo.
	 * 
	 * @param key	l'identificativo dell'oggetto
	 * @param obj	l'oggetto da inserire in sessione
	 */
	public static void putObjectInSession(final String key, final Object obj) {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(key, obj);
	}
		
	/**
	 * Metodo per il recupero di un managed bean a partire da un identificativo.
	 *
	 * @param <T>
	 *            tipo del bean
	 * @param name
	 *            l'identificativo del bean
	 * @return il bean recuperato
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getManagedBean(final String name) {
		final FacesContext cxt = FacesContext.getCurrentInstance();
		return (T) cxt.getApplication().getELResolver().getValue(cxt.getELContext(), null, name);
	}
	

	/**
	 * Metodo per il recupero di un query param dalla request.
	 * 
	 * @param key	identificativo del query param da recuperare dalla request.
	 * @return		oggetto recuperato
	 */
	public static String getParamQueryFromRequest(final String key) {
		final Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if (params != null) {
			return params.get(key);
		}
		return null;
	}

	/**
	 * Metodo per il recupero di un oggetto dalla request.
	 * 
	 * @param key	identificativo dell'oggetto da recuperare dalla request.
	 * @return		oggetto recuperato
	 */
	public static Object getParameterFromRequest(final String key) {
		Object output = null;
		final FacesContext ctx = FacesContext.getCurrentInstance();
		if (ctx != null) {
			final HttpServletRequest req = (HttpServletRequest) ctx.getExternalContext().getRequest();
			output = req.getParameter(key);
		}
		return output;
	}

	/**
	 * Metodo per il recupero di un attributo dalla request data la chiave.
	 * 	
	 * @param key	chiave
	 * @return		attributo
	 */
	public static Object getAttributeFromRequest(final String key) {
		Object output = null;
		final FacesContext ctx = FacesContext.getCurrentInstance();
		if (ctx != null) {
			final HttpServletRequest req = (HttpServletRequest) ctx.getExternalContext().getRequest();
			output = req.getAttribute(key);
		}
		return output;
	}

	/**
	 * Recupero request http.
	 * 	
	 * @return	request http
	 */
	public static HttpServletRequest getRequest() {
		final FacesContext ctx = FacesContext.getCurrentInstance();
		return (HttpServletRequest) ctx.getExternalContext().getRequest();
	}

	/**
	 * Metodo per il recupero di un parametro dall'header.
	 * 
	 * @param key	chiave
	 * @return		attributo
	 */
	public static Object getParameterFromHeader(final String key) {
		Object output = null;
		final FacesContext ctx = FacesContext.getCurrentInstance();
		if (ctx != null) {
			final HttpServletRequest req = (HttpServletRequest) ctx.getExternalContext().getRequest();
			output = req.getHeader(key);
		}
		return output;
	}

	/**
	 * Esecuzione programmatica di una regola di navigazione.
	 * 
	 * @param token	input con cui sollecitare il motore di regole
	 */
	public static void navigationRule(final NavigationTokenEnum token) {
		final FacesContext context = FacesContext.getCurrentInstance();
		context.getApplication().getNavigationHandler().handleNavigation(context, null, token.getToken());		
	}

	/**
	 * Restituisce il nome del bean.
	 * @param beanClass
	 * @return bean name
	 */
	public static String getBeanName(final Class beanClass) {
		final Named named = (Named) beanClass.getAnnotation(Named.class);
		return named.value();
	}

	/**
	 * Esegue un update ajax su tutti i componenti recuperati dalla lista degli id passata come parametro.
	 * @param ids
	 */
	public static void update(final String... ids) {
		final PrimeFaces pf = PrimeFaces.current();
		if (pf.isAjaxRequest()) {
		    pf.ajax().update(ids);
		}
	}

	/**
	 * Rimuove i bean passati come parametri dallo scope.
	 * @param yourBeanArray
	 */
	public static void destroyBeanViewScope(final String... yourBeanArray) {
		for (final String yourBean : yourBeanArray) {
			if (!StringUtils.isNullOrEmpty(yourBean)) {
				FacesContext.getCurrentInstance().getViewRoot().getViewMap().remove(yourBean);
			}
		}
	}

	/**
	 * Restituisce l'id della sessione.
	 * @return sessionID
	 */
	public static String getSessionId() {
		final FacesContext fCtx = FacesContext.getCurrentInstance();
		final HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(false);
		return session.getId(); 
	}

}
