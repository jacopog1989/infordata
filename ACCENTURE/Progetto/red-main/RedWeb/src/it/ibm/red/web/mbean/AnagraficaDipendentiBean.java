package it.ibm.red.web.mbean;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.AnagraficaDipendenteDTO;
import it.ibm.red.business.dto.AnagraficaDipendentiComponentDTO;
import it.ibm.red.business.dto.IndirizzoDTO;
import it.ibm.red.business.dto.RicercaAnagDipendentiDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IAnagraficaDipendentiFacadeSRV;
import it.ibm.red.business.service.facade.IProvinciaFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.helper.dtable.DataTableHelper;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Bean gestione anagrafica dipendenti.
 */
@Named(ConstantsWeb.MBean.ANAGRAFICA_DIPENDENTI_BEAN)
@ViewScoped
public class AnagraficaDipendentiBean extends AbstractBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 2805631782248502125L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AnagraficaDipendentiBean.class);

	/**
	 * DTO di ricerca.
	 */
	private RicercaAnagDipendentiDTO ricercaDTO;

	/**
	 * Datatable dipendneti.
	 */
	private DataTableHelper<AnagraficaDipendenteDTO, AnagraficaDipendenteDTO> dth;

	/**
	 * Datatable indirizzi dipendente.
	 */
	private SimpleDetailDataTableHelper<IndirizzoDTO> dthIndirizzi;

	/**
	 * Flag edit.
	 */
	private Boolean editMode;

	/**
	 * Flag new.
	 */
	private Boolean newMode;

	/**
	 * Flag edit indirizzo.
	 */
	private Boolean editAddrMode;

	/**
	 * Nuova anagrafica.
	 */
	private AnagraficaDipendenteDTO newAnagrafica;

	/**
	 * Elenco provincie.
	 */
	private Collection<SelectItemDTO> elencoProvincie;

	/**
	 * Elengo d.u.g.
	 */
	private Collection<SelectItemDTO> elencoDug;

	/**
	 * Bean di sessione.
	 */

	/**
	 * Infomrazioni utente.
	 */
	private UtenteDTO utente;

	/**
	 * Servizio province.
	 */
	private IProvinciaFacadeSRV proSRV;

	/**
	 * Servizio anagrafica.
	 */
	private IAnagraficaDipendentiFacadeSRV anagSRV;

	/**
	 * Nuovo indirizzo.
	 */
	private IndirizzoDTO newIndirizzo;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		anagSRV = ApplicationContextProvider.getApplicationContext().getBean(IAnagraficaDipendentiFacadeSRV.class);
		proSRV = ApplicationContextProvider.getApplicationContext().getBean(IProvinciaFacadeSRV.class);
		ricercaDTO = new RicercaAnagDipendentiDTO();
		resetPage();
		loadProvincie();
		loadDug();
		final SessionBean session = FacesHelper.getManagedBean(MBean.SESSION_BEAN);
		utente = session.getUtente();
	}

	private void resetPage() {
		editMode = false;
		newMode = false;
		editAddrMode = false;
	}

	/**
	 * Gestisce la logica di salvataggio di un'anagrafica, comunicando eventualmente
	 * errori di compilazione del form.
	 */
	public void salvaAnagrafica() {
		if (StringUtils.isNullOrEmpty(newAnagrafica.getNome()) || StringUtils.isNullOrEmpty(newAnagrafica.getCognome())
				|| StringUtils.isNullOrEmpty(newAnagrafica.getCodiceFiscale())) {
			showError("Nome, cognome e codice fiscale sono obbligatori!");
		} else if (newAnagrafica.getNome().length() < 2 || newAnagrafica.getCognome().length() < 2) {
			showError("Nome e cognome devono essere composti da almeno due caratteri");
		} else if (StringUtils.containsDigit(newAnagrafica.getNome()) || StringUtils.containsDigit(newAnagrafica.getCognome())) {
			showError("Nome e cognome non possono contenere numeri");
		} else if (!StringUtils.isCodiceFiscaleFormallyCorrect(newAnagrafica.getCodiceFiscale())) {
			showError("Il codice fiscale inserito non è formalmente corretto");
		} else {
			anagSRV.salva(utente.getIdAoo(), newAnagrafica);
			final Collection<AnagraficaDipendenteDTO> anags = new ArrayList<>();
			anags.add(newAnagrafica);

			resetPage();
			resetDTH();

			dth.setMasters(anags);

			showInfoMessage("Salvataggio avvenuto con successo!");
		}
	}

	/**
	 * La modifica viene divisa in due fasi, l'eliminazione e il salvataggio. Viene
	 * effettuato un controllo prima dell'eliminazione per far si che non si elimini
	 * dal db l'anagrafica prima ancora del salvataggio della nuova anagrafica
	 */
	public void modificaAnagrafica() {
		if (StringUtils.isNullOrEmpty(newAnagrafica.getNome()) || StringUtils.isNullOrEmpty(newAnagrafica.getCognome())
				|| StringUtils.isNullOrEmpty(newAnagrafica.getCodiceFiscale())) {
			showError("Nome, cognome e codice fiscale sono obbligatori!");
		} else if (newAnagrafica.getNome().length() < 2 || newAnagrafica.getCognome().length() < 2) {
			showError("Nome e cognome devono essere composti da almeno due caratteri");
		} else if (StringUtils.containsDigit(newAnagrafica.getNome()) || StringUtils.containsDigit(newAnagrafica.getCognome())) {
			showError("Nome e cognome non possono contenere numeri");
		} else if (!StringUtils.isCodiceFiscaleFormallyCorrect(newAnagrafica.getCodiceFiscale())) {
			showError("Il codice fiscale inserito non è formalmente corretto");
		} else {
			try {
				// Eliminazione
				anagSRV.remove(newAnagrafica.getId());
				final Collection<AnagraficaDipendenteDTO> tmp = new ArrayList<>();
				for (final AnagraficaDipendenteDTO a : dth.getMasters()) {
					if (!a.getCodice().equalsIgnoreCase(newAnagrafica.getCodice())) {
						tmp.add(a);
					}
				}
				dth.setMasters(tmp);
				dth.setRenderDetail(false);
				resetPage();

				// Salvataggio
				anagSRV.salva(utente.getIdAoo(), newAnagrafica);
				final Collection<AnagraficaDipendenteDTO> anags = new ArrayList<>();
				anags.add(newAnagrafica);
				resetPage();
				resetDTH();
				dth.setMasters(anags);
				showInfoMessage("Modifica avvenuta con successo!");
			} catch (final Exception e) {
				showError("Errore in fase di modifica dell'anagrafica!", e);
			}
		}
	}

	private void resetDTH() {
		dth = new DataTableHelper<AnagraficaDipendenteDTO, AnagraficaDipendenteDTO>() {

			private static final long serialVersionUID = -3212922979534411767L;

			@Override
			protected AnagraficaDipendenteDTO setDetailFromCurrent(final AnagraficaDipendenteDTO master) {
				setRenderDetail(true);
				dthIndirizzi = new SimpleDetailDataTableHelper<>(anagSRV.getIndirizzi(master.getId()));
				return master;
			}
		};
	}

	/**
	 * Gestisce la chiusura della dialog di creazione anagrafica.
	 */
	public void close() {
		resetPage();
		resetDTH();
		ricercaDTO = new RicercaAnagDipendentiDTO();
	}

	/**
	 * Gestisce la logica di salvataggio di un indirizzo associata ad un utente.
	 */
	public void saveIndirizzo() {
		if (!StringUtils.isNullOrEmpty(newIndirizzo.getCodiceDug()) && !StringUtils.isNullOrEmpty(newIndirizzo.getToponimo())
				&& !StringUtils.isNullOrEmpty(newIndirizzo.getComune())) {
			try {
				if (newIndirizzo.getId() == null) {

					saveNewIndirizzo();
				} else {

					updateNewIndirizzo();
				}
			} catch (final Exception e) {
				LOGGER.error("Errore in fase di modifica indirizzo!", e);
				showError("Errore in fase di modifica indirizzo!");
			}
		} else {
			showWarnMessage("Denominazione Urbanistica Generica, toponimo e provincia obbligatorie!");
		}
	}

	/**
	 * Gestisce la modifica dell'indirizzo: {@link #newIndirizzo}.
	 */
	private void updateNewIndirizzo() {
		anagSRV.modificaIndirizzo(dth.getCurrentDetail().getId(), newIndirizzo);

		final Collection<IndirizzoDTO> tmp = new ArrayList<>();
		for (final IndirizzoDTO a : dthIndirizzi.getMasters()) {
			if (!a.getId().equals(newIndirizzo.getId())) {
				tmp.add(a);
			}
		}
		tmp.add(newIndirizzo);
		dthIndirizzi.setMasters(tmp);

		editAddrMode = false;
		showInfoMessage("Modifica indirizzo avvenuto con successo!");
	}

	/**
	 * Gestisce il salvataggio dell'indirizzo: {@link #newIndirizzo}.
	 */
	private void saveNewIndirizzo() {
		final IndirizzoDTO indirizzo = anagSRV.salvaIndirizzo(dth.getCurrentDetail().getId(), newIndirizzo);

		getDugDescrizione(indirizzo);

		Collection<IndirizzoDTO> indirizzi = dthIndirizzi.getMasters();
		if (indirizzi == null) {
			indirizzi = new ArrayList<>();
			dthIndirizzi.setMasters(indirizzi);
		}
		indirizzi.add(indirizzo);
		editAddrMode = false;
		showInfoMessage("Salvataggio indirizzo avvenuto con successo!");
	}

	/**
	 * Restituisce la descrizione del DUG.
	 * 
	 * @param indirizzo
	 */
	private void getDugDescrizione(final IndirizzoDTO indirizzo) {
		for (final SelectItemDTO si : elencoDug) {
			if (si.getValue().equals(indirizzo.getCodiceDug())) {
				indirizzo.setDescrizioneDug(si.getDescription());
			}
		}
	}

	/**
	 * Chiude il pannello degl indirizzi.
	 */
	public void closeIndirizzo() {
		editAddrMode = false;
	}

	private void loadProvincie() {
		elencoProvincie = proSRV.getProvince();
	}

	private void loadDug() {
		elencoDug = anagSRV.getDug();
	}

	/**
	 * Gestisce la ricerca delle anagrafiche.
	 */
	public void ricerca() {

		resetPage();
		resetDTH();
		final Collection<AnagraficaDipendenteDTO> masters = anagSRV.ricerca(utente.getIdAoo(), ricercaDTO);
		dth.setMasters(masters);

	}

	/**
	 * Gestisce la logica di rimozione di un indirizzo.
	 * 
	 * @param event
	 */
	public void removeIndirizzo(final ActionEvent event) {
		final IndirizzoDTO indirizzo = getDataTableClickedRow(event);
		if (indirizzo != null) {
			try {
				anagSRV.removeIndirizzo(indirizzo.getId());

				final Collection<IndirizzoDTO> tmp = new ArrayList<>();
				for (final IndirizzoDTO a : dthIndirizzi.getMasters()) {
					if (!a.getId().equals(indirizzo.getId())) {
						tmp.add(a);
					}
				}
				dthIndirizzi.setMasters(tmp);
				showInfoMessage("Indirizzo rimosso con successo.");
			} catch (final Exception e) {
				showError("Errore in fase di rimozione dell'indirizzo!", e);
			}
		}
	}

	/**
	 * Gestisce la logica di rimozione di un'intera anagrafica.
	 * 
	 * @param event
	 */
	public void removeAnagrafica(final ActionEvent event) {
		final AnagraficaDipendenteDTO anag = getDataTableClickedRow(event);
		if (anag != null) {

			try {
				anagSRV.remove(anag.getId());

				final Collection<AnagraficaDipendenteDTO> tmp = new ArrayList<>();
				for (final AnagraficaDipendenteDTO a : dth.getMasters()) {
					if (!a.getCodice().equalsIgnoreCase(anag.getCodice())) {
						tmp.add(a);
					}
				}
				dth.setMasters(tmp);
				showInfoMessage("Anagrafica rimossa con successo.");
				dth.setRenderDetail(false);

				resetPage();
			} catch (final Exception e) {
				showError("Errore in fase di rimozione dell'anagrafica!", e);
			}
		}

	}

	/**
	 * Gestisce la modifica di un indirizzo.
	 * 
	 * @param event
	 */
	public void editIndirizzo(final ActionEvent event) {
		final IndirizzoDTO indirizzo = getDataTableClickedRow(event);
		newIndirizzo = indirizzo;
		editAddrMode = true;
	}

	/**
	 * Gestisce la modifica di un'anagrafica.
	 * 
	 * @param event
	 */
	public void editAnagrafica(final ActionEvent event) {
		final AnagraficaDipendenteDTO anag = getDataTableClickedRow(event);
		newAnagrafica = anag;
		resetPage();
		editMode = true;
	}

	/**
	 * Imposta i parametri per permettere la renderizzazione del form per la
	 * creazione di un'anagrafica.
	 */
	public void newAnagrafica() {
		newAnagrafica = new AnagraficaDipendenteDTO();
		resetPage();
		resetDTH();
		newMode = true;
	}

	/**
	 * Imposta i parametri per consentire l'aggiunta di un indirizzo.
	 */
	public void addIndirizzo() {
		newIndirizzo = new IndirizzoDTO();
		editAddrMode = true;
	}

	/**
	 * Chiude la dialog di selezione anagrafica.
	 */
	public void selectClose() {
		final FacesContext context = FacesContext.getCurrentInstance();
		final AnagraficaDipendentiComponentDTO anagDTO = context.getApplication().evaluateExpressionGet(context, "#{cc.attrs.anagDto}",
				AnagraficaDipendentiComponentDTO.class);
		anagDTO.setSelectedValues(dth.getSelectedMasters());

		resetPage();
		resetDTH();
		ricercaDTO = new RicercaAnagDipendentiDTO();
	}

	/**
	 * Gestisce il reset della selezione.
	 */
	public final void resetSelection() {
		final FacesContext context = FacesContext.getCurrentInstance();
		final AnagraficaDipendentiComponentDTO anagDTO = context.getApplication().evaluateExpressionGet(context, "#{cc.attrs.anagDto}",
				AnagraficaDipendentiComponentDTO.class);
		anagDTO.setSelectedValues(null);
	}

	/**
	 * @return editAddrMode
	 */
	public Boolean getEditAddrMode() {
		return editAddrMode;
	}

	/**
	 * @param editAddrMode
	 */
	public void setEditAddrMode(final Boolean editAddrMode) {
		this.editAddrMode = editAddrMode;
	}

	/**
	 * @return DataTableHelper per la gestione delle anagrafiche
	 */
	public DataTableHelper<AnagraficaDipendenteDTO, AnagraficaDipendenteDTO> getDth() {
		return dth;
	}

	/**
	 * @return ricercaDTO
	 */
	public RicercaAnagDipendentiDTO getRicercaDTO() {
		return ricercaDTO;
	}

	/**
	 * @param ricercaDTO
	 */
	public void setRicercaDTO(final RicercaAnagDipendentiDTO ricercaDTO) {
		this.ricercaDTO = ricercaDTO;
	}

	/**
	 * @return editMode
	 */
	public Boolean getEditMode() {
		return editMode;
	}

	/**
	 * @param editMode
	 */
	public void setEditMode(final Boolean editMode) {
		this.editMode = editMode;
	}

	/**
	 * @return newAnagrafica
	 */
	public AnagraficaDipendenteDTO getNewAnagrafica() {
		return newAnagrafica;
	}

	/**
	 * @param newAnagrafica
	 */
	public void setNewAnagrafica(final AnagraficaDipendenteDTO newAnagrafica) {
		this.newAnagrafica = newAnagrafica;
	}

	/**
	 * @return newMode
	 */
	public Boolean getNewMode() {
		return newMode;
	}

	/**
	 * @param newMode
	 */
	public void setNewMode(final Boolean newMode) {
		this.newMode = newMode;
	}

	/**
	 * @return elencoProvincie
	 */
	public Collection<SelectItemDTO> getElencoProvincie() {
		return elencoProvincie;
	}

	/**
	 * @return newIndirizzo
	 */
	public IndirizzoDTO getNewIndirizzo() {
		return newIndirizzo;
	}

	/**
	 * @param newIndirizzo
	 */
	public void setNewIndirizzo(final IndirizzoDTO newIndirizzo) {
		this.newIndirizzo = newIndirizzo;
	}

	/**
	 * @return elencoDug
	 */
	public Collection<SelectItemDTO> getElencoDug() {
		return elencoDug;
	}

	/**
	 * @return dthIndirizzi
	 */
	public SimpleDetailDataTableHelper<IndirizzoDTO> getDthIndirizzi() {
		return dthIndirizzi;
	}

	/**
	 * @param dthIndirizzi
	 */
	public void setDthIndirizzi(final SimpleDetailDataTableHelper<IndirizzoDTO> dthIndirizzi) {
		this.dthIndirizzi = dthIndirizzi;
	}
}