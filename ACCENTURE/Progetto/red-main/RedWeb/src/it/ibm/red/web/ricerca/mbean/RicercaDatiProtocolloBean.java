/**
 * 
 */
package it.ibm.red.web.ricerca.mbean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.ParamsRicercaProtocolloDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.RiferimentoProtNpsDTO;
import it.ibm.red.business.dto.StatoRicercaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.facade.IPropertiesFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.mbean.DocumentManagerBean;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.nps.mbean.DettaglioNpsBean;

/**
 * @author APerquoti
 *
 */
@Named(ConstantsWeb.MBean.RICERCA_DATI_PROTOCOLLO_BEAN)
@ViewScoped
public class RicercaDatiProtocolloBean extends AbstractBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -7117310669304883428L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaDatiProtocolloBean.class.getName());
	

	/**
	 * Bean sessione.
	 */
	private SessionBean sessionBean;
	
	/**
	 * Bean dettaglio.
	 */
	private DettaglioNpsBean detailBean;

	/**
	 * Datatbale procolli.
	 */
	protected SimpleDetailDataTableHelper<ProtocolloNpsDTO> protocolliDTH;

	/**
	 * Parametri ricerca protocollo.
	 */
	private ParamsRicercaProtocolloDTO formRicerca;
	
	/**
	 * Lista colonne.
	 */
	private List<Boolean> protColumns;
	
	/**
	 * Numero massimo di risultati.
	 */
	private Integer numMaxRisultati;
	
	/**
	 * Valore che viene settato in base ai permessi dell'utente
	 */
	private UtenteDTO utente;
	
	/**
	 * Service nps
	 */
	private INpsSRV npsSRV;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		
		detailBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_NPS_BEAN);
		
		protocolliDTH = new SimpleDetailDataTableHelper<>();
		protocolliDTH.setMasters(new ArrayList<>());
		protColumns = Arrays.asList(true, true, true, true, true, true, true ,true);
		
		final IPropertiesFacadeSRV propertiesSRV = ApplicationContextProvider.getApplicationContext().getBean(IPropertiesFacadeSRV.class);
		numMaxRisultati = Integer.parseInt(propertiesSRV.getByEnum(PropertiesNameEnum.NPS_MAX_RISULTATI_RICERCA_PROTOCOLLI));
		
		utente = sessionBean.getUtente();
		initPaginaRicerca();
		  
		npsSRV = ApplicationContextProvider.getApplicationContext().getBean(INpsSRV.class);
	}

	/**
	 * Inizializza la pagina di ricerca.
	 */
	@SuppressWarnings("unchecked")
	public void initPaginaRicerca() {
		try {
			final StatoRicercaDTO stDto = (StatoRicercaDTO) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.STATO_RICERCA, true);
			if (stDto != null) {
				final Collection<ProtocolloNpsDTO> searchResult = (Collection<ProtocolloNpsDTO>) stDto.getRisultatiRicerca();
				formRicerca = (ParamsRicercaProtocolloDTO) stDto.getFormRicerca();
				
				protocolliDTH.setMasters(searchResult);
				protocolliDTH.selectFirst(true);
				
				selectProtocolloNpsDetail(protocolliDTH.getCurrentMaster(), sessionBean.getUtente().getIdAoo().intValue(),utente);
				
				if (numMaxRisultati != null && searchResult != null && numMaxRisultati.equals(searchResult.size())) {
					showWarnMessage(RicercaAbstractBean.MSG_NUM_MAX_RISULTATI);
				}
			} else {
				formRicerca = new ParamsRicercaProtocolloDTO();
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Non è stato possibile recuperare i risultati di ricerca");
		}
	}

	/**
	 * Gestisce la selezione della riga dal datatable dei protocolli.
	 * @param se
	 */
	public final void rowSelectorDatiProtocollo(final SelectEvent se) {
		protocolliDTH.rowSelector(se);
		selectProtocolloNpsDetail(protocolliDTH.getCurrentMaster(), sessionBean.getUtente().getIdAoo().intValue(),utente);
	}

	/**
	 * Imposta il protocollo nel dettaglio.
	 * @param f
	 * @param idAoo
	 * @param utente
	 */
	private void selectProtocolloNpsDetail(final ProtocolloNpsDTO f, final Integer idAoo, final UtenteDTO utente) {
		detailBean.setDetail(idAoo, f, utente);
	}

	/**
	 * Metodo per reimpostare nel session bean il form utilizzato per la ricerca e quindi visualizzarlo nel dialog
	 */
	public void ripetiRicercaDatiProt() {
		try {
			// Viene ripetuta la ricerca con i stessi filtri
			sessionBean.getRicercaFormContainer().setDatiProtocollo(formRicerca);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}
	
	/**
	 * Gestisce la selezione e la deselezione della colonna.
	 * @param event
	 */
	public final void onColumnToggleDP(final ToggleEvent event) {
		protColumns.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
	}


//	<-- get/set -->

	/**
	 * @return the protocolliDTH
	 */
	public final SimpleDetailDataTableHelper<ProtocolloNpsDTO> getProtocolliDTH() {
		return protocolliDTH;
	}

	/**
	 * @param protocolliDTH the protocolliDTH to set
	 */
	public final void setProtocolliDTH(final SimpleDetailDataTableHelper<ProtocolloNpsDTO> protocolliDTH) {
		this.protocolliDTH = protocolliDTH;
	}

	/**
	 * @return the protColumns
	 */
	public final List<Boolean> getProtColumns() {
		return protColumns;
	}

	/**
	 * @param protColumns the protColumns to set
	 */
	public final void setProtColumns(final List<Boolean> protColumns) {
		this.protColumns = protColumns;
	}

	/**
	 * Restituisce il nome del file per l'export.
	 * @return nome file export
	 */
	public String getNomeFileExportCoda() {
		
		String nome = "";
		if (sessionBean.getUtente() != null) {
			nome += sessionBean.getUtente().getNome().toLowerCase() + "_" + sessionBean.getUtente().getCognome().toLowerCase() + "_";
		}
		
		nome += "ricercadatiprotocollo_";
		
		final SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH.mm");
		nome += sdf.format(new Date()); 
		
		return nome;
	}
 
	/**
	 * Gestisce la funzionalità di predisposizione dei documenti.
	 */
	public void predisponiDocumenti() {
		List<ProtocolloNpsDTO> protocolliSelezionati = protocolliDTH.getSelectedMasters();

		if (CollectionUtils.isEmpty(protocolliSelezionati)) {
			showWarnMessage("Selezionare almeno un documento");
			return;
		}

		try { 
			DetailDocumentRedDTO docRibaltato = new DetailDocumentRedDTO();
			//in questo modo gli dico che è un documento in uscita
			docRibaltato.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_USCITA.getIds()[0]);
			List<RiferimentoProtNpsDTO> allaccioRifNpsListDTO = new ArrayList<>();
			for(ProtocolloNpsDTO  protocolloSelezionato : protocolliSelezionati) {
				RiferimentoProtNpsDTO allaccioRifNpsDTO = new RiferimentoProtNpsDTO();
				allaccioRifNpsDTO.setAnnoProtocolloNps(Integer.parseInt(protocolloSelezionato.getAnnoProtocollo())); 
				allaccioRifNpsDTO.setNumeroProtocolloNps(protocolloSelezionato.getNumeroProtocollo());
				allaccioRifNpsDTO.setOggettoProtocolloNps(protocolloSelezionato.getOggetto());
				if(protocolloSelezionato.getDocumentoPrincipale()==null) {
					//Chiamo NPS
					protocolloSelezionato = npsSRV.getDettagliProtocollo(protocolloSelezionato, utente.getIdAoo().intValue(), utente, false);
					if(protocolloSelezionato.getDocumentoPrincipale()!=null) {
						allaccioRifNpsDTO.setIdDocumentoDownload(protocolloSelezionato.getDocumentoPrincipale().getGuid());
					}
				} else {
					allaccioRifNpsDTO.setIdDocumentoDownload(protocolloSelezionato.getDocumentoPrincipale().getGuid());	
				}
				allaccioRifNpsDTO.setVerificato(true);
				allaccioRifNpsListDTO.add(allaccioRifNpsDTO); 
			}
			docRibaltato.setAllaccioRifNps(allaccioRifNpsListDTO); 
 
			DocumentManagerBean bean = FacesHelper.getManagedBean(MBean.DOCUMENT_MANAGER_BEAN);
			//Pulisco nel caso sia già stato aperto 
			bean.unsetDetail();
			bean.setChiamante(MBean.RICERCA_PROTOCOLLI_PREGRESSI_NSD_BEAN);
			bean.setDetail(docRibaltato); 

			FacesHelper.executeJS("PF('dlgManageDocument_RM').show()");
			FacesHelper.executeJS("PF('dlgManageDocument_RM').toggleMaximize()");
			FacesHelper.update("idDettagliEstesiForm:idPanelDocumentManager");
			FacesHelper.update("idDettagliEstesiForm:idPanelContainerDocumentManager_T");

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}
	
	/**
	 * Effettua un update dei master selezionati.
	 * @param event
	 */
	public final void updateCheck(final AjaxBehaviorEvent event) {
		ProtocolloNpsDTO masterChecked = getDataTableClickedRow(event);
		Boolean newValue = ((SelectBooleanCheckbox) event.getSource()).isSelected();
		if (newValue != null && newValue) {
			protocolliDTH.getSelectedMasters().add(masterChecked);
		} else {
			protocolliDTH.getSelectedMasters().remove(masterChecked);
		}
	}
	
}
