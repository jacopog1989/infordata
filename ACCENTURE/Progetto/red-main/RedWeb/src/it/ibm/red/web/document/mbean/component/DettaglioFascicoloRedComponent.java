package it.ibm.red.web.document.mbean.component;

import java.io.InputStream;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DetailFascicoloRedDTO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV;
import it.ibm.red.web.document.mbean.DocumentManagerBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.VisualizzaDettaglioDocumentoComponent;

/**
 * Component dettaglio fascicolo RED.
 */
public class DettaglioFascicoloRedComponent extends AbstractBean {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 510047887019619181L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DettaglioFascicoloRedComponent.class);

	/**
	 * Informazioni utente.
	 */
	private UtenteDTO utente;

	/**
	 * Servizio.
	 */
	private IDocumentManagerFacadeSRV documentManagerSRV;

	/**
	 * Servizio.
	 */
	private IDocumentoRedFacadeSRV documentoRedSRV;

	/**
	 * Dettaglio.
	 */
	private DetailFascicoloRedDTO detail;

	/**
	 * Component visualizzazione dettaglio.
	 */
	private VisualizzaDettaglioDocumentoComponent viewDettaglioDoc;

	/**
	 * Dettaglio FAscicolo.
	 */
	private DocumentoFascicoloDTO docSelectedFas;

	/**
	 * Dettaglio documento.
	 */
	private DetailDocumentRedDTO documentoDto;

	/**
	 * Costruttore del component.
	 * 
	 * @param utenteIn
	 */
	public DettaglioFascicoloRedComponent(final UtenteDTO utenteIn) {
		try {
			this.utente = utenteIn;
			documentManagerSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentManagerFacadeSRV.class);
			documentoRedSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoRedFacadeSRV.class);
			docSelectedFas = new DocumentoFascicoloDTO();
			viewDettaglioDoc = new VisualizzaDettaglioDocumentoComponent(utente);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}
	}

	@Override
	protected void postConstruct() {
		// in realta' non mi serve il postConstruct ma mi servono
		// i metodi shoError/warning/info quindi ho esteso AbstractBean
	}

	/**
	 * Restituisce lo StreamedContent del documento selezionato.
	 * 
	 * @param index
	 * @return StreamedContent per il download
	 */
	public StreamedContent downloadDocSelected(final Object index) {

		StreamedContent file = null;
		try {
			final Integer i = (Integer) index;

			final DocumentoFascicoloDTO docSelected = this.detail.getDocumenti().get(i);

			final InputStream stream = documentManagerSRV.getStreamDocumentoByGuid(docSelected.getGuid(), utente);

			file = new DefaultStreamedContent(stream, docSelected.getMimeType(), docSelected.getNomeFile());

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di recupero del documento.");
		}
		return file;

	}

	/**
	 * Gestisce l'apertura della view del documento.
	 * 
	 * @param index
	 */
	public void openDocumentView(final Object index) {

		try {
			final Integer i = (Integer) index;

			final DocumentoFascicoloDTO docSelected = this.detail.getDocumenti().get(i);

			final DocumentManagerBean bean = FacesHelper.getManagedBean(Constants.ManagedBeanName.DOCUMENT_MANAGER_BEAN);
			bean.setChiamante("DettaglioFascicoloRedComponent");
			bean.setDetail(documentoRedSRV.getDocumentDetail(docSelected.getDocumentTitle(), utente, null, null));

			FacesHelper.executeJS("PF('dlgManageDocument_RM').show()");
			FacesHelper.executeJS("PF('dlgManageDocument_RM').toggleMaximize()");
			FacesHelper.update("idDettagliEstesiForm:idPanelDocumentManager");

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di recupero del documento.");
		}

	}

	/**
	 * @param index
	 */
	public void openVisualizzaDocumento(final Object index) {
		final Integer i = (Integer) index;
		docSelectedFas = this.detail.getDocumenti().get(i);
		documentoDto = documentoRedSRV.getDocumentDetail(docSelectedFas.getDocumentTitle(), utente, null, null);
		viewDettaglioDoc.setDetail(documentoDto);
	}

	/**
	 * Restituisce true se il dettaglio è apribile, false altrimenti.
	 * 
	 * @param docSelected
	 * @return true se apribile, false altrimenti
	 */
	public boolean detailOpenable(final DocumentoFascicoloDTO docSelected) {
		final boolean interno = (StringUtils.isNotBlank(docSelected.getDescTipoCategoriaDoc())
				&& TipoCategoriaEnum.INTERNO.getDescrizione().equals(docSelected.getDescTipoCategoriaDoc()))
				|| CategoriaDocumentoEnum.INTERNO.getIds()[0].equals(docSelected.getIdCategoria());

		return !interno && documentoRedSRV.hasDocumentDetail(docSelected.getTipoDocumento());
	}

	/**
	 * Restituisce il DTO del documento.
	 * 
	 * @return documentoDto
	 */
	public DetailDocumentRedDTO getDocumentoDto() {
		return documentoDto;
	}

	/**
	 * Restituisce il dettaglio del fascicolo.
	 * 
	 * @return detail
	 */
	public DetailFascicoloRedDTO getDetail() {
		return detail;
	}

	/**
	 * Imposta il dettaglio del fascicolo.
	 * 
	 * @param detail
	 */
	public void setDetail(final DetailFascicoloRedDTO detail) {
		this.detail = detail;
	}

	/**
	 * Restituisce l'utente.
	 * 
	 * @return UtenteDTO
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'utente.
	 * 
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Restituisce il Service del DocumentManager.
	 * 
	 * @return documentManagerSRV
	 */
	public IDocumentManagerFacadeSRV getDocumentManagerSRV() {
		return documentManagerSRV;
	}

	/**
	 * Imposta il Service del DocumentManager.
	 * 
	 * @param documentManagerSRV
	 */
	public void setDocumentManagerSRV(final IDocumentManagerFacadeSRV documentManagerSRV) {
		this.documentManagerSRV = documentManagerSRV;
	}

	/**
	 * Restituisce il component che gestisce la visualizzazione del dettaglio.
	 * 
	 * @return viewDettaglioDoc
	 */
	public VisualizzaDettaglioDocumentoComponent getViewDettaglioDoc() {
		return viewDettaglioDoc;
	}

	/**
	 * Imposta il component che gestisce la visualizzazione del dettaglio.
	 * 
	 * @param viewDettaglioDoc
	 */
	public void setViewDettaglioDoc(final VisualizzaDettaglioDocumentoComponent viewDettaglioDoc) {
		this.viewDettaglioDoc = viewDettaglioDoc;
	}

	/**
	 * Restitusce il DTO del documentoFascicolo.
	 * 
	 * @return docSelectedFas
	 */
	public DocumentoFascicoloDTO getDocSelectedFas() {
		return docSelectedFas;
	}

	/**
	 * Imposta il DTO del documentoFascicolo.
	 * 
	 * @param docSelectedFas
	 */
	public void setDocSelectedFas(final DocumentoFascicoloDTO docSelectedFas) {
		this.docSelectedFas = docSelectedFas;
	}

}
