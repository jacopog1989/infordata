package it.ibm.red.web.mbean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.GestioneLockDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.VerificaFirmaPrincipaleAllegatoDTO;
import it.ibm.red.business.enums.AccessoFunzionalitaEnum;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV;
import it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.SessionObject;
import it.ibm.red.web.document.mbean.DocumentManagerBean;
import it.ibm.red.web.document.mbean.interfaces.IDettaglioDocumentoEastButtonNoResp;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.interfaces.IFascicoloProcedimentaleManagerInitalizer;
import it.ibm.red.web.servlets.DownloadContentServlet.DocumentTypeEnum;

/**
 * Bean per la gestione del dettaglio documento.
 */
@Named(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN)
@ViewScoped
public class DettaglioDocumentoBean extends DettaglioDocumentoAbstractBean implements IDettaglioDocumentoEastButtonNoResp, IFascicoloProcedimentaleManagerInitalizer {

	private static final String EAST_SECTION_FORM_TOOLTIP_NOME_FILE = "eastSectionForm:tooltipNomeFile";

	private static final String PF_DLG_MANAGE_DOCUMENT_RM_TOGGLE_MAXIMIZE = "PF('dlgManageDocument_RM').toggleMaximize();";

	private static final String PF_DLG_MANAGE_DOCUMENT_RM_SHOW = "PF('dlgManageDocument_RM').show();";

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 7471338223007987743L;

	/**
	 * File principale.
	 */
	private static final String FILEPRINCIPALE = "File principale: ";

	/**
	 * Allegato.
	 */
	private static final String ALLEGATO = "Allegato: ";

	/**
	 * Contributo.
	 */
	private static final String CONTRIBUTO = "Contributo Interno: ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DettaglioDocumentoBean.class);

	/**
	 * Component.
	 */
	private VisualizzaDettaglioDocumentoComponent viewDettaglioDoc;

	/**
	 * Flag dettaglio apribile.
	 */
	private boolean dettaglioOpenable;

	/**
	 * Flag disable assegnazioni.
	 */
	private Boolean disableAssegnazioniTab;

	/**
	 * Nome file.
	 */
	private String nomeFile;

	/**
	 * Flag documento non presente.
	 */
	private Boolean lockDocumentoNonPresente;

	/**
	 * Flag firme valide.
	 */
	private ValueMetadatoVerificaFirmaEnum firmeValidePrincAllegato;

	/**
	 * Utente locker.
	 */
	private String nomeUtenteLockatore;

	/**
	 * Lista verifiche firme.
	 */
	private List<VerificaFirmaPrincipaleAllegatoDTO> listVerificaFirmaPrincAll;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		super.postConstruct();
		dettaglioOpenable = true;
		viewDettaglioDoc = new VisualizzaDettaglioDocumentoComponent(utente);
		disableAssegnazioniTab = false;
	}

	/**
	 * Imposta il dettaglio.
	 * @param master
	 */
	@Override
	public void setDetail(final MasterDocumentRedDTO master) {
		super.setDetail(master);
		documentTitleSelected =  this.documentCmp.getMaster().getDocumentTitle();
		dettaglioOpenable = documentoRedSRV.hasDocumentDetail(master.getTipologiaDocumento());
		inizializeAbstract();
		if (master.getAllegatoDocumentTitle() != null) {
			setNomeFile(ALLEGATO + master.getNomeFile());
			previewComponent.setDetailPreview("a_" + master.getAllegatoDocumentTitle());
		} else {
			setNomeFile(FILEPRINCIPALE + master.getNomeFile());
		}
		calcolaIconaFirma(master);
	}
	
	private void calcolaIconaFirma(MasterDocumentRedDTO master) {
		//da verificare
		firmeValidePrincAllegato = ValueMetadatoVerificaFirmaEnum.FIRMA_NON_VERIFICATA;
		boolean hoAllegatiFirmatiNonVerificati = false;
		ISignFacadeSRV signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignFacadeSRV.class);
		hoAllegatiFirmatiNonVerificati = signSRV.sonoPresentiAllegatiFirmatiNonVerificati(utente.getIdAoo(), master.getGuuid(), utente);
		//valido
		if(Boolean.TRUE.equals(master.getFirmaValidaPrincipale()) &&  !hoAllegatiFirmatiNonVerificati && (Boolean.FALSE.equals(master.getbAttachmentPresent()) || (Boolean.TRUE.equals(master.getbAttachmentPresent()) && (Boolean.TRUE.equals(master.getFirmaValidaAllegati())|| master.getFirmaValidaAllegati()==null))) ) {
			firmeValidePrincAllegato = ValueMetadatoVerificaFirmaEnum.FIRMA_OK;
		//non valido
		}else if (Boolean.FALSE.equals(master.getFirmaValidaPrincipale()) || (Boolean.TRUE.equals(master.getbAttachmentPresent()) && Boolean.FALSE.equals(master.getFirmaValidaAllegati()))) {
			firmeValidePrincAllegato = ValueMetadatoVerificaFirmaEnum.FIRMA_KO;
		//firma non presente
		}else if (ValueMetadatoVerificaFirmaEnum.FIRMA_NON_PRESENTE.getValue().equals(master.getMetadatoValiditaFirmaPrincipale()) && !hoAllegatiFirmatiNonVerificati && (Boolean.FALSE.equals(master.getbAttachmentPresent()) || (Boolean.TRUE.equals(master.getbAttachmentPresent()) && master.getFirmaValidaAllegati()==null))) {
			firmeValidePrincAllegato = ValueMetadatoVerificaFirmaEnum.FIRMA_NON_PRESENTE;
	
		}
	}

	/**
	 * Imposta il dettaglio.
	 * @param documentTitle
	 * @param wobNumber
	 * @param documentClassName
	 */
	@Override
	public void setDetail(final String documentTitle, final String wobNumber, final String documentClassName) {
		super.setDetail(documentTitle, wobNumber, documentClassName);
		inizializeAbstract();
	}

	private void inizializeAbstract() {
		setMenuAllegatiCommand("#{DettaglioDocumentoBean.changeSelectedDocumentTitle}");
		this.documentCmp.setChiamante(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN);

	}

	/**
	 * Questo apre il dettaglio esteso.
	 */
	@Override
	public void openDocumentPopup() {

		try { // OR INTERNO NSD
			if (CategoriaDocumentoEnum.INTERNO.getIds()[0].equals(documentCmp.getDetail().getIdCategoriaDocumento())) {
				showWarnMessage("Non è possibile aprire il dettaglio di un documento interno.");
				return;
			}

			setDetail();

			final DocumentManagerBean bean = FacesHelper.getManagedBean(Constants.ManagedBeanName.DOCUMENT_MANAGER_BEAN);
			bean.setChiamante(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_BEAN);
			bean.setDetail(this.documentCmp.getDetail());

			this.documentCmp.getDetail().setModificabile(bean.getDetail().getModificabile());

			setPopupDocumentVisible(true);
			// il lock viene messo e controllato all'apertura solo dall'utente che non ha in
			// carico il procedimento e ha il permesso di modificare
			if (PermessiUtils.haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.MODIFICA_METADATI_MINIMI, utente)
					&& (this.documentCmp.getDetail().getModificabile() == null || !this.documentCmp.getDetail().getModificabile())) {
				final IListaDocumentiFacadeSRV listaSRV = ApplicationContextProvider.getApplicationContext().getBean(IListaDocumentiFacadeSRV.class);
				final String s = listaSRV.getWorkflowPrincipale(this.getDocumentCmp().getDocumentTitleSelected(), utente);
				if (s != null) {
					bean.getDetail().setHaWKFAttivo(true);
					final GestioneLockDTO gestLockDTO = documentoSRV.getLockDocumento(Integer.valueOf(this.documentCmp.getDetail().getDocumentTitle()),
							bean.getUtente().getIdAoo(), 1, bean.getUtente().getId());
					lockDocumentoNonPresente = gestLockDTO.getNessunLock();
					if (lockDocumentoNonPresente != null && !lockDocumentoNonPresente) {
						lockDocumentoNonPresente = gestLockDTO.isInCarico()
								&& !(this.documentCmp.getDetail().getModificabile() == null || !this.documentCmp.getDetail().getModificabile());
					}

					if (lockDocumentoNonPresente == null) {
						documentoSRV.insertLockDocumento(Integer.valueOf(this.documentCmp.getDetail().getDocumentTitle()), this.documentCmp.getUtente().getIdAoo(), 1,
								this.documentCmp.getUtente().getId(), utente.getNome(), utente.getCognome(),
								!(this.documentCmp.getDetail().getModificabile() == null || !this.documentCmp.getDetail().getModificabile()));
						FacesHelper.executeJS(PF_DLG_MANAGE_DOCUMENT_RM_SHOW);
						FacesHelper.executeJS(PF_DLG_MANAGE_DOCUMENT_RM_TOGGLE_MAXIMIZE);
					} else if (Boolean.FALSE.equals(lockDocumentoNonPresente)) {
						nomeUtenteLockatore = gestLockDTO.getUsernameUtenteLock();
						FacesHelper.update("eastSectionForm");
						FacesHelper.executeJS("PF('sbloccaDocumentoDlg_WV').show();");
					} else {
						FacesHelper.executeJS(PF_DLG_MANAGE_DOCUMENT_RM_SHOW);
						FacesHelper.executeJS(PF_DLG_MANAGE_DOCUMENT_RM_TOGGLE_MAXIMIZE);
					}
				} else {
					bean.getDetail().setHaWKFAttivo(false);
					FacesHelper.executeJS(PF_DLG_MANAGE_DOCUMENT_RM_SHOW);
					FacesHelper.executeJS(PF_DLG_MANAGE_DOCUMENT_RM_TOGGLE_MAXIMIZE);
				}
			} else {
				FacesHelper.executeJS(PF_DLG_MANAGE_DOCUMENT_RM_SHOW);
				FacesHelper.executeJS(PF_DLG_MANAGE_DOCUMENT_RM_TOGGLE_MAXIMIZE);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di recupero del documento.");
		}

	}

	/**
	 * Apre il dettaglio del documento se non Interno.
	 * 
	 * @see CategoriaDocumentoEnum.
	 */
	@Override
	public void openVisualizzaDocumento() {
		// OR INTERNO NSD
		if (CategoriaDocumentoEnum.INTERNO.getIds()[0].equals(documentCmp.getDetail().getIdCategoriaDocumento())) {
			showWarnMessage("Non è possibile aprire il dettaglio di un documento interno.");
			return;
		}

		loadAfterIntroPreview();
		if(this.documentCmp.getMaster()!=null) {
			this.documentCmp.getDetail().setTipoFirma(this.documentCmp.getMaster().getTipoFirma());
		}
		viewDettaglioDoc.setDetail(this.documentCmp.getDetail()); 
	}

	/**
	 * Deseleziona il dettaglio.
	 */
	@Override
	public void unsetDetail() {
		super.unsetDetail();
		viewDettaglioDoc.unsetDetail();
	}

	/**
	 * Abilita l'update.
	 */
	public void enableUpdate() {
		updateEnabled = true;
	}

	/**
	 * Disabilita l'update e mostra il messaggio di successo.
	 */
	public void save() {
		showInfoMessage("Modifica avvenuta con successo.");
		updateEnabled = false;
	}

	/**
	 * Disabilita l'update e mostra il messaggio di comunicazione.
	 */
	public void cancel() {
		showInfoMessage("Modifica non eseguita.");
		updateEnabled = false;
	}

	/**
	 * Gestisce l'apertura del fascicolo procedimentale.
	 */
	@Override
	public void openFascicoloProcedimentale() {
		try {
			
			IDocumentoRedFacadeSRV documentoRedSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoRedFacadeSRV.class);
    		documentoRedSRV.retrieveFascicoliAndSetProcedimentale(documentCmp.getDetail(), utente);

			if (CollectionUtils.isEmpty(documentCmp.getDetail().getFascicoli())) {
				showInfoMessage("Non si hanno i privilegi di accesso al fascicolo.");
			} else {
				final String idfasciscoloProcedimentale = documentCmp.getDetail().getIdFascicoloProcedimentale();
				if (StringUtils.isBlank(idfasciscoloProcedimentale)) {
					throw new RedException("Impossibile recuperare il fascicolo procedimentale");
				}

				// Recupero fascicolo per id
				final List<FascicoloDTO> listaFascicoliDocumento = documentCmp.getDetail().getFascicoli();
				final Optional<FascicoloDTO> optionalFascicolo = listaFascicoliDocumento.stream()
						.filter(fascicolo -> idfasciscoloProcedimentale.equals(fascicolo.getIdFascicolo())).findAny();
				if (!optionalFascicolo.isPresent()) {
					throw new RedException("Impossibile recuperare il fascicolo procedimentale");
				}
				final FascicoloDTO currentFascicolo = optionalFascicolo.get();

				documentCmp.openFascicolo(currentFascicolo);

			}
		} catch (final RedException red) {
			showError(red);
		}
	}

	/***
	 * GET & SET
	 ***********************************************************************/

	/**
	 * Restituisce true se il dettaglio è apribile, false altrimenti.
	 * 
	 * @return dettaglioOpenable
	 */
	public boolean isDettaglioOpenable() {
		return dettaglioOpenable;
	}

	/**
	 * @return the responseDetail
	 */
	@Override
	public final Collection<ResponsesRedEnum> getResponseDetail() {
		return responseDetail;
	}

	/**
	 * Restituisce il component per la gestione del dettaglio.
	 * 
	 * @return viewDettaglioDoc
	 */
	public VisualizzaDettaglioDocumentoComponent getViewDettaglioDoc() {
		return viewDettaglioDoc;
	}

	/**
	 * Imposta il component del dettaglio.
	 * 
	 * @param viewDettaglioDoc
	 */
	public void setViewDettaglioDoc(final VisualizzaDettaglioDocumentoComponent viewDettaglioDoc) {
		this.viewDettaglioDoc = viewDettaglioDoc;
	}

	/**
	 * Restituisce disableAssegnazioniTab.
	 * 
	 * @return disableAssegnazioniTab
	 */
	public Boolean getDisableAssegnazioniTab() {
		return disableAssegnazioniTab;
	}

	/**
	 * @param disableAssegnazioniTab
	 */
	public void setDisableAssegnazioniTab(final Boolean disableAssegnazioniTab) {
		this.disableAssegnazioniTab = disableAssegnazioniTab;
	}

	/**
	 * Restituisce il nome del file del documento.
	 * 
	 * @return nome file
	 */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * Imposta il nome del file del documento.
	 * 
	 * @param nomeFile
	 */
	public void setNomeFile(final String nomeFile) {
		this.nomeFile = nomeFile;
	}

	/**
	 * Gestisce la modifica del document title selezionato.
	 */
	@Override
	public void changeSelectedDocumentTitle() {
		super.changeSelectedDocumentTitle();
		final DocumentTypeEnum tipoDocumento = DocumentTypeEnum.getDocumentTypeFromPrefix(documentTitleSelected);
		if (DocumentTypeEnum.ALLEGATO.equals(tipoDocumento)) {
			setNomeFile(ALLEGATO + nomeFileSelected);
			FacesHelper.update(EAST_SECTION_FORM_TOOLTIP_NOME_FILE);
		} else if (DocumentTypeEnum.CONTRIBUTO.equals(tipoDocumento) || DocumentTypeEnum.CONTRIBUTO_INTERNO_OTF.equals(tipoDocumento)) {
			setNomeFile(CONTRIBUTO + nomeFileSelected);
			FacesHelper.update(EAST_SECTION_FORM_TOOLTIP_NOME_FILE);
		} else {
			setNomeFile(FILEPRINCIPALE + nomeFileSelected);
			FacesHelper.update(EAST_SECTION_FORM_TOOLTIP_NOME_FILE);
		}
	}

	/**
	 * Permette la renderizzazione della dialog di stampa verificandone lo stato
	 * degli attributi, in caso di errori mostra il popup all'utente.
	 */
	public void openStampaDocumento() {
		try {
			sessionBean.setRenderDialogStampa(true);
			FacesHelper.update("eastSectionForm:dlgStampaDocId");
			final IDocumentoFacadeSRV documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);
			FileDTO doc = null;
			String documentTitleDaInviare = documentTitleSelected;
			String[] docTitleAllOContr = null;
			if (documentTitleSelected == null) {
				documentTitleDaInviare = this.documentCmp.getMaster().getDocumentTitle();
			} else {
				if (!NumberUtils.isParsable(documentTitleSelected)) {
					docTitleAllOContr = documentTitleSelected.split("_");
					if (docTitleAllOContr != null && docTitleAllOContr.length > 0) {
						documentTitleDaInviare = docTitleAllOContr[1];
					}
				}
			}
			if (!NumberUtils.isParsable(documentTitleSelected) && docTitleAllOContr != null && docTitleAllOContr.length > 0 && "a".equals(docTitleAllOContr[0])) {
				doc = documentoSRV.getContentAllegato(documentTitleDaInviare, utente.getFcDTO(), utente.getIdAoo());
			} else {
				doc = documentoSRV.getDocumentoContentPreview(utente.getFcDTO(), documentTitleDaInviare, false, utente.getIdAoo());
			}

			final byte[] newContent = PdfHelper.convertToPngPDFAndPDFtoPng(doc.getContent());
			final FileDTO docConvertito = new FileDTO(doc.getFileName(), newContent, doc.getMimeType());
			if (doc.getContent() != null) {
				FacesHelper.putObjectInSession(SessionObject.PRINT_FILE_SERVLET, docConvertito);
			}
		} catch (final Exception ex) {
			LOGGER.error("Errore durante la stampa del documento", ex);
			showError("Errore durante la stampa del documento");
		}
	}

	/**
	 * Chiude la dialog di stampa.
	 */
	public void chiudiDialogStampa() {
		sessionBean.setRenderDialogStampa(false);
	}

	/**
	 * @return lockDocumentoNonPresente
	 */
	public boolean isLockDocumentoNonPresente() {
		return lockDocumentoNonPresente;
	}

	/**
	 * @param lockDocumentoNonPresente
	 */
	public void setLockDocumentoNonPresente(final boolean lockDocumentoNonPresente) {
		this.lockDocumentoNonPresente = lockDocumentoNonPresente;
	}

	/**
	 * Mostra il dettaglio dopo il lock.
	 */
	@Override
	public void showDettaglioAfterLock() {
		documentoSRV.updateLockDocumento(Integer.valueOf(this.documentCmp.getDetail().getDocumentTitle()), this.documentCmp.getUtente().getIdAoo(), 1,
				!(this.documentCmp.getDetail().getModificabile() == null || !this.documentCmp.getDetail().getModificabile()), this.documentCmp.getUtente().getId(),
				utente.getNome(), utente.getCognome());
		FacesHelper.executeJS(PF_DLG_MANAGE_DOCUMENT_RM_SHOW);
		FacesHelper.executeJS(PF_DLG_MANAGE_DOCUMENT_RM_TOGGLE_MAXIMIZE);
	}

	/**
	 * Restituisce il valore del flag: firmeValidePrincAllegato.
	 * 
	 * @return firmeValidePrincAllegato
	 */
	public ValueMetadatoVerificaFirmaEnum getFirmeValidePrincAllegato() {
		return firmeValidePrincAllegato;
	}

	/**
	 * @param firmeValidePrincAllegato
	 */
	public void setFirmeValidePrincAllegato(ValueMetadatoVerificaFirmaEnum firmeValidePrincAllegato) {
		this.firmeValidePrincAllegato = firmeValidePrincAllegato;
	}

	/**
	 * Esegue la verifica delle firme dell'allegato principale
	 * @param aggiornaMetadatoFilenet se true aggiorna i metadati di Filenet
	 */
	@Override
	public void verificaFirmaDocPrincipaleAllegati() {
		listVerificaFirmaPrincAll = new ArrayList<>();
		if(!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(this.documentCmp.getDetail().getDocumentTitle())) {
			ISignFacadeSRV signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignFacadeSRV.class);
			if(this.documentCmp.getMaster().getbAttachmentPresent()!=null) {
				//sto partendo dal dettaglio di un doc principale
				listVerificaFirmaPrincAll = signSRV.getMetadatoVerificaFirmaPrincAllegato(sessionBean.getUtente().getIdAoo(), this.documentCmp.getDetail().getDocumentTitle(), utente, this.documentCmp.getMaster().getbAttachmentPresent(),this.documentCmp.getMaster());
			}else {
				//sto partendo dal dettaglio di un allegato
				MasterDocumentRedDTO principale = documentoSRV.getMasterFromGuidPrinc(utente, this.documentCmp.getMaster().getGuuid());
				listVerificaFirmaPrincAll = signSRV.getMetadatoVerificaFirmaPrincAllegato(sessionBean.getUtente().getIdAoo(), principale.getDocumentTitle(), utente, principale.getbAttachmentPresent(),this.documentCmp.getMaster());
			}
			calcolaIconaFirma(this.documentCmp.getMaster());
		}

	}

	/**
	 * Restituisce il nome dell'utente che detiene il lock.
	 * 
	 * @return nomeUtenteLockatore
	 */
	public String getNomeUtenteLockatore() {
		return nomeUtenteLockatore;
	}

	/**
	 * Imposta il nome dell'utente che detiene il lock.
	 * 
	 * @param nomeUtenteLockatore
	 */
	public void setNomeUtenteLockatore(final String nomeUtenteLockatore) {
		this.nomeUtenteLockatore = nomeUtenteLockatore;
	}

	/**
	 * Restituisce listVerificaFirmaPrincAll.
	 * 
	 * @return listVerificaFirmaPrincAll
	 */
	public List<VerificaFirmaPrincipaleAllegatoDTO> getListVerificaFirmaPrincAll() {
		return listVerificaFirmaPrincAll;
	}

	/**
	 * @param listVerificaFirmaPrincAll
	 */
	public void setListVerificaFirmaPrincAll(final List<VerificaFirmaPrincipaleAllegatoDTO> listVerificaFirmaPrincAll) {
		this.listVerificaFirmaPrincAll = listVerificaFirmaPrincAll;
	}

	
}