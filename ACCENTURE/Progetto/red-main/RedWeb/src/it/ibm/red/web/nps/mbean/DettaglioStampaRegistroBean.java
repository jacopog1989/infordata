package it.ibm.red.web.nps.mbean;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.RegistroProtocolloDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.SessionObject;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.IDetailPreviewComponent;
import it.ibm.red.web.servlets.DownloadContentServlet;
import it.ibm.red.web.servlets.DownloadContentServlet.DocumentTypeEnum;

/**
 * Bean generico per il dettaglio del {@link RegistroProtocolloDTO}
 * 
 * Come anche SIGI viene mostrato solo il preview.
 * 
 * In base a come è stato generato il documento il dato viene recuperato tramite NPS oppure MEF Vedi {@link DownloadContentServlet} 
 * per risalire ai metodi in cui viene recuperato.
 * 
 * @author a.difolca
 *
 */
@Named(ConstantsWeb.MBean.DETTAGLIO_STAMPA_REGISTRO_BEAN)
@ViewScoped
public class DettaglioStampaRegistroBean extends AbstractBean implements IDetailPreviewComponent {
	
	private static final long serialVersionUID = 7329506231490388939L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DettaglioStampaRegistroBean.class);
	
	/**
	 * UTente.
	 */
	private UtenteDTO utente;
	
	/**
	 * Crypto id.
	 */
	private String cryptoId =  null;
	
	/**
	 * Post construct del bean.
	 */
	@PostConstruct
	@Override
	public void postConstruct() {
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
	}
	
	/**
	 * Recupera i parametri criptati utilizzati per la Servlet 'DownloadContentServlet'.
	 * {@link DownloadContentServlet}
	 * @param id - Identificativo del documento
	 * @return Parametri cryptati.
	 */
	private String getCryptoDocParamById(final String id, final DocumentTypeEnum documentType) {
		try {
			return DownloadContentServlet.getCryptoDocParamById(id, documentType, utente);
		} catch (final Exception e) {
			LOGGER.error("Errore nell'encrypt dei parametri per la servlet 'DownloadContentServlet':", e);
			showError("Errore nell'encrypt dei parametri per la servlet 'DownloadContentServlet': " + e.getMessage());
		}
		return "ERROREDICODIFICA";
	}
	
	/**
	 * Permette di modificare il dettaglio mostrato
	 * Gestisce il null non mostrando la preview.
	 * 
	 * @param f
	 */
	public void setDetail(final RegistroProtocolloDTO f) {
		if (f == null || TipologiaProtocollazioneEnum.EMERGENZANPS.equals(f.getTipologiaProtocollo())) {
			cryptoId = null;
		} else {
			// PMEF
			DocumentTypeEnum documentType = DocumentTypeEnum.MEF_REGISTRO;
			String id = f.getDocumentTitle();
			
			// NPS
			if (TipologiaProtocollazioneEnum.NPS.equals(f.getTipologiaProtocollo())) {
				id = null;
				documentType = DocumentTypeEnum.NPS_REGISTRO;
				// In questo caso il content è già incluso nel DTO in input
				final FileDTO registroProtoocolloFile = new FileDTO(f.getNomeFile(), f.getContent(), f.getContentType());
				FacesHelper.putObjectInSession(SessionObject.NPS_REGISTRO_ITEM_FILE, registroProtoocolloFile);
			}
			
			this.cryptoId = getCryptoDocParamById(id, documentType);
		}		
	}
	
	/**
	 * Restituisce il crypto id.
	 * @return crypto id
	 */
	@Override
	public String getCryptoId() {
		return cryptoId;
	}
}
