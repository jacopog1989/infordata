package it.ibm.red.web.converter;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import it.ibm.red.business.dto.RegioneDTO;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IRegioneFacadeSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * Converter regione.
 */
@FacesConverter("regioneConverter")
public class RegioneConverter implements Converter {

	/**
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.String).
	 */
	@Override
	public Object getAsObject(final FacesContext fc, final UIComponent uic, final String value) {

		final IRegioneFacadeSRV regioneSRV = ApplicationContextProvider.getApplicationContext().getBean(IRegioneFacadeSRV.class);
		 if (StringUtils.isNullOrEmpty(value)) {
			 return null;
		 } else {
			return regioneSRV.get(Long.parseLong(value));
		 }

	}

	/**
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.Object).
	 */
	@Override
	public String getAsString(final FacesContext fc, final UIComponent uic, final Object object) {
		if (object instanceof RegioneDTO) {
			final RegioneDTO regione = (RegioneDTO) object;
			return regione.getIdRegione();
		}
		return object.toString();
	}

}
