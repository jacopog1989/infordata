package it.ibm.red.web.mbean;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Bean lista faldoni.
 */
@Named(ConstantsWeb.MBean.LISTA_FALDONI_BEAN)
@ViewScoped
public class ListaFaldoniBean extends AbstractBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ListaFaldoniBean.class.getName());
	
	/**
	 * Table faldoni.
	 */
	private SimpleDetailDataTableHelper<FaldoneDTO> faldoniDTH;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
	    faldoniDTH = new SimpleDetailDataTableHelper<>();

		faldoniDTH.setMasters((Collection<FaldoneDTO>) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.LISTA_FALDONI, true));

		LOGGER.info("Lista Faldoni caricata con successo");
	}

	/**
	 * Restituisce il data table helper della coda Faldoni.
	 * @return faldoniDTH
	 */
	public SimpleDetailDataTableHelper<FaldoneDTO> getFaldoniDTH() {
		return faldoniDTH;
	}


	/**
	 * Imposta il data table helper della coda Faldoni.
	 * @param faldoniDTH
	 */
	public void setFaldoniDTH(final SimpleDetailDataTableHelper<FaldoneDTO> faldoniDTH) {
		this.faldoniDTH = faldoniDTH;
	}
}
