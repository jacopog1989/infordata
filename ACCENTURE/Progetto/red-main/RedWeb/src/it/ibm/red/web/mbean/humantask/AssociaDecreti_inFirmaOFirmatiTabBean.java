package it.ibm.red.web.mbean.humantask;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IFepaFacadeSRV;
import it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;

/**
 * Bean che gestisca la sezione "in firma o firmati" dell'associazione decreti.
 */
public class AssociaDecreti_inFirmaOFirmatiTabBean implements Serializable {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 6850606946210733231L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AssociaDecreti_inFirmaOFirmatiTabBean.class.getName());

	/**
	 * Info utente.
	 */
	private UtenteDTO utente;
	
	/**
	 * Datatable decreti.
	 */
	private SimpleDetailDataTableHelper<MasterDocumentRedDTO> decretiDTH;

	/**
	 * Costruttore di classe.
	 * @param utente
	 * @param docSel
	 */
	public AssociaDecreti_inFirmaOFirmatiTabBean(final UtenteDTO utente, final MasterDocumentRedDTO docSel) {
		this.utente = utente;
		decretiDTH = new SimpleDetailDataTableHelper<>(recuperaDecretiInFirmaOFirmati(docSel));
	}

	private List<MasterDocumentRedDTO> recuperaDecretiInFirmaOFirmati(final MasterDocumentRedDTO docSel) {
		final List<MasterDocumentRedDTO> decreti = new ArrayList<>();
		try {
			
			final IFepaFacadeSRV fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);
			
			final String[] listaDecreti = fepaSRV.ottieniElencoDecretiFepa(docSel.getWobNumber(), utente);
			final StringBuilder listaDecretiByComma = new StringBuilder("");
			String separator = "";
			for (final String s: listaDecreti) {
			//in questo modo il primo viene senza virgola	
				if (!StringUtils.isNullOrEmpty(s)) {
					listaDecretiByComma.append(separator + s);
					separator = ", ";
				}
			}
		
			 final IListaDocumentiFacadeSRV ldsrv = ApplicationContextProvider.getApplicationContext().getBean(IListaDocumentiFacadeSRV.class);
			//Recupero i decreti in firma
			final List<MasterDocumentRedDTO> decretiInFirma = (List<MasterDocumentRedDTO>) ldsrv.getDocumentForMaster(DocumentQueueEnum.DD_IN_FIRMA, null, utente);
			//Recupero i decreti firmati
			final List<MasterDocumentRedDTO> decretiFirmati = (List<MasterDocumentRedDTO>) ldsrv.getDocumentForMaster(DocumentQueueEnum.DD_FIRMATI, null, utente);
		
			for (final MasterDocumentRedDTO decretiFirma: decretiInFirma) {
				if (listaDecretiByComma.toString().contains(decretiFirma.getDocumentTitle())) {
					decreti.add(decretiFirma);
				}
				
			}
			
	

			for (final MasterDocumentRedDTO decretiFirma: decretiFirmati) {
				if (listaDecretiByComma.toString().contains(decretiFirma.getWobNumber())) {
					decreti.add(decretiFirma);
				}
				
			}
			
		} catch (final Exception e) {
			LOGGER.error(e);
		}
		return decreti;
	}	
	
	/*####### getter e setter #######*/
	/**
	 * Restituisce l'utente come UtenteDTO.
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta le informazioni associate all'utente e contenute in UtenteDTO.
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Restituisce l'helper per il datatable dei decreti.
	 * @return decretiDTH
	 */
	public SimpleDetailDataTableHelper<MasterDocumentRedDTO> getDecretiDTH() {
		return decretiDTH;
	}

	/**
	 * Imposta l'helper del datatable dei decreti.
	 * @param decretiDTH
	 */
	public void setDecretiDTH(final SimpleDetailDataTableHelper<MasterDocumentRedDTO> decretiDTH) {
		this.decretiDTH = decretiDTH;
	}
	
	
}
