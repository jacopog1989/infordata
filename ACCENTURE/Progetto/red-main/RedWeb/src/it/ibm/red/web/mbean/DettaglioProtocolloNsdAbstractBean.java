package it.ibm.red.web.mbean;
 
import it.ibm.red.business.dto.DetailProtocolloPregressoDTO;
import it.ibm.red.business.dto.ProtocolliPregressiDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.facade.IDetailProtocolloNsdFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.mbean.component.DettaglioProtocolloNsdComponent;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Bean dettaglio protocollo NSD.
 */
public class DettaglioProtocolloNsdAbstractBean extends AbstractBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 8422518253881813826L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DettaglioProtocolloNsdAbstractBean.class);
 	
 	/**
	 * Utente.
	 */
	protected UtenteDTO utente;

	/**
	 * Componente protocollo.
	 */
	protected DettaglioProtocolloNsdComponent protocolloCmp;

	/**
	 * Servizio.
	 */
	protected IDetailProtocolloNsdFacadeSRV detailProtocolloNsdSRV;

	/**
	 * Post construct del bean.
	 */
	@Override
	protected void postConstruct() {
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		protocolloCmp = new DettaglioProtocolloNsdComponent(utente);
	}

	/**
	 * Imposta il dettaglio.
	 * 
	 * @param protocolloDTO
	 *            dettagli protocollo da impostare
	 */
	public void setDetail(final ProtocolliPregressiDTO protocolloDTO) {
		try {
			final DetailProtocolloPregressoDTO detail = new DetailProtocolloPregressoDTO(protocolloDTO); 
			protocolloCmp.setDetail(detail);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Errore nell'operazione di recupero del protocollo nsd.");
		}

	}

	/**
	 * Resetta il dettaglio impostandolo a null.
	 */
	public void unsetDetail() { 
		protocolloCmp.setDetail(null);
	}

	/**
	 * Restituisce il componente per la gestione del dettaglio protocollo NSD.
	 * 
	 * @return component protocollo
	 */
	public DettaglioProtocolloNsdComponent getProtocolloCmp() {
		return protocolloCmp;
	}
}