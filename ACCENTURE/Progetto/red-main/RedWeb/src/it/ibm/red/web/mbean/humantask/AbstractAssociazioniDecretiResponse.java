package it.ibm.red.web.mbean.humantask;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import it.ibm.red.business.dto.DecretoDirigenzialeDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * Abstract del bean che gestisce la response di associazione decreti.
 */
public abstract class AbstractAssociazioniDecretiResponse extends AbstractBean implements InitHumanTaskInterface {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 2220777194040474757L;

	/**
	 * Handler session bean.
	 */
	protected SessionBean sb;

	/**
	 * Handler lista documenti bean.
	 */
	protected ListaDocumentiBean ldb;
	
	/**
	 * Utnete.
	 */
	protected UtenteDTO utente;
	
	/**
	 * Lista documenti selezionati.
	 */
	protected transient List<MasterDocumentRedDTO> documentiSelezionati;
	
	/**
	 * Data della diciarazione.
	 */
	protected Date dataDichiarazione;
	
	/**
	 * Nome del file.
	 */
	protected String nomeFile;
	
	/**
	 * Bean tab decreti disponibili.
	 */
	protected AssociaDecreti_DisponibiliTabBean decretiDisponibiliTab;
	
	/**
	 * Decreti in firma o firmati.
	 */
	protected AssociaDecreti_inFirmaOFirmatiTabBean decretiInFirmaOFirmatiTab;
	
	/**
	 * Flag metti agli atti.
	 */
	protected boolean flagMettiAgliAtti = false;

	/**
	 * Inizializza il bean.
	 * @param inDocsSelezionati documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		documentiSelezionati = inDocsSelezionati;
		final MasterDocumentRedDTO docSel = documentiSelezionati.get(0);
		dataDichiarazione = docSel.getDataCreazione();
		nomeFile = docSel.getNomeFile();

		sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sb.getUtente();
		
		
		decretiDisponibiliTab = new AssociaDecreti_DisponibiliTabBean(utente, docSel);
		
		decretiInFirmaOFirmatiTab = new AssociaDecreti_inFirmaOFirmatiTabBean(utente, docSel);
		
		flagMettiAgliAtti = showBtnAtti();

	}
	
	private boolean showBtnAtti() {
		if (decretiInFirmaOFirmatiTab.getDecretiDTH() != null && decretiInFirmaOFirmatiTab.getDecretiDTH().getMasters() != null) {
			for (final MasterDocumentRedDTO decretoFirm : decretiInFirmaOFirmatiTab.getDecretiDTH().getMasters()) {
				if (decretoFirm.getDataProtocollazione() == null) {
					return false;
				}
			}
		}
		if (decretiDisponibiliTab.getDecretiDTH() != null && decretiDisponibiliTab.getDecretiDTH().getMasters() != null) {
			for (final DecretoDirigenzialeDTO decretoDir : decretiDisponibiliTab.getDecretiDTH().getMasters()) {
				if (decretoDir.getSelected() != null && decretoDir.getSelected()) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Post construct del bean.
	 */
	@Override
	protected void postConstruct() {
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
	}
	
	/**
	 * Esegue la logica relativa alla response.
	 * */
	public abstract void doResponse();
	
	/*################# Getter e Setter #################*/
	
	/**
	 * Getter SessionBean.
	 * 
	 * @return l'istanza del SessionBean
	 * */
	public SessionBean getSb() {
		return sb;
	}

	/**
	 * Getter ListaDocumentiBean.
	 * 
	 * @return l'istanza del ListaDocumentiBean
	 * */
	public ListaDocumentiBean getLdb() {
		return ldb;
	}

	/**
	 * Getter DTO dell'utente.
	 * 
	 * @return UtenteDTO contenente le informazioni dell'utente
	 * */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Restituisce una collection di Masters che rappresentano i documenti selezionati.
	 * 
	 * @return Collection di MasterDocumentRedDTO contenenti le informazioni sui Masters
	 * */
	public Collection<MasterDocumentRedDTO> getDocumentiSelezionati() {
		return documentiSelezionati;
	}

	/**
	 * Setter del DTO dell'utente.
	 * 
	 * @param utente l'utente da impostare
	 * */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Setter dei documentiSelezionati.
	 * 
	 * @param documentiSelezionati la List di MasterDocumentRedDTO da impostare
	 * */
	public void setDocumentiSelezionati(final List<MasterDocumentRedDTO> documentiSelezionati) {
		this.documentiSelezionati = documentiSelezionati;
	}

	/**
	 * Getter data dichiarazione.
	 * 
	 * @return La data di Dichiarazione
	 * */
	public Date getDataDichiarazione() {
		return dataDichiarazione;
	}

	/**
	 * Setter data dichiarazione.
	 * 
	 * @param dataDichiarazione la data da impostare
	 * */
	public void setDataDichiarazione(final Date dataDichiarazione) {
		this.dataDichiarazione = dataDichiarazione;
	}

	/**
	 * Getter nome file.
	 * 
	 * @return il nome del file come String
	 * */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * Setter nome file.
	 * 
	 * @param nomeFile
	 * */
	public void setNomeFile(final String nomeFile) {
		this.nomeFile = nomeFile;
	}

	/**
	 * Getter decretiDisponibiliTab.
	 * 
	 * @return Bean AssociaDecreti_DisponibiliTabBean
	 * */
	public AssociaDecreti_DisponibiliTabBean getDecretiDisponibiliTab() {
		return decretiDisponibiliTab;
	}

	/**
	 * Getter decretiInFirmaOFirmatiTab.
	 * 
	 * @return Bean AssociaDecreti_inFirmaOFirmatiTabBean
	 * */
	public AssociaDecreti_inFirmaOFirmatiTabBean getDecretiInFirmaOFirmatiTab() {
		return decretiInFirmaOFirmatiTab;
	}

}
