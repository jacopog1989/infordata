package it.ibm.red.web.enums;

/**
 * Enum che definisce gli item del menu.
 */
public enum MenuItemEnum {
	
	/**
	 * Valore.
	 */
	SCRIVANIA(1, "Scrivania", "../resources/images/desk.png"),
	
	/**
	 * Valore.
	 */
	UFFICIO(2, "Ufficio", "../resources/images/corriere.png"),
	
	/**
	 * Valore.
	 */
	CARTACEI(3, "Cartacei", "../resources/images/paper.png"),
	
	/**
	 * Valore.
	 */
	ARCHIVIO(4, "Archivio", "../resources/images/storage.png"),
	
	/**
	 * Valore.
	 */
	CREAZIONE(5, "Creazione", "../resources/images/protocollo.png"),
	
	/**
	 * Valore.
	 */
	RICERCA(6, "Ricerca", "../resources/images/search.png"),
	
	/**
	 * Valore.
	 */
	RUBRICA(7, "Rubrica", "../resources/images/rubrica.png"),
	
	/**
	 * Valore.
	 */
	EVENTI(8, "Eventi", "../resources/images/event.png"),
	
	/**
	 * Valore.
	 */
	CONSULTA(9, "Consulta", "../resources/images/todo.png"),
	
	/**
	 * Valore.
	 */
	UTENTE(10, "Utente", "../resources/images/user.png");

	/**
	 * Identificativo.
	 */
	private Integer id;

	/**
	 * Label.
	 */
	private String label;	

	/**
	 * Path.
	 */
	private String path;

	/**
	 * Costruttore.
	 * 
	 * @param inId		identificativo
	 * @param inLabel	label
	 */
	MenuItemEnum(final Integer inId, final String inLabel, final String inPath) {
		id = inId;
		label = inLabel;
		path = inPath;
	}

	/**
	 * Restituisce l'id dell'Enum.
	 * @return id enum
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Restituisce la label associata all'enum.
	 * @return label enum
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Restituisce il path associato all'enum.
	 * @return path dell'enum
	 */
	public String getPath() {
		return path;
	}
}