/**
 * La Classe EMailBean utilizzata per la gestione delle mail.
 */
package it.ibm.red.web.mbean;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIColumn;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.component.tabview.TabView;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.event.data.PageEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.Visibility;
import org.springframework.beans.BeanUtils;

import com.filenet.api.collection.PageIterator;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.AzioneMail;
import it.ibm.red.business.constants.Constants.ModalitaSpedizioneMail;
import it.ibm.red.business.constants.Constants.TipologiaInvio;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnatarioInteropDTO;
import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.ComuneDTO;
import it.ibm.red.business.dto.DestinatarioCodaMailDTO;
import it.ibm.red.business.dto.DestinatarioEsternoDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DetailEmailDTO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.EsitoOperazioneMailDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.HierarchicalFileWrapperDTO;
import it.ibm.red.business.dto.NotaDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.ProvinciaDTO;
import it.ibm.red.business.dto.RegioneDTO;
import it.ibm.red.business.dto.RicercaRubricaDTO;
import it.ibm.red.business.dto.SegnaturaMessaggioInteropDTO;
import it.ibm.red.business.dto.SignerDTO;
import it.ibm.red.business.dto.TestoPredefinitoDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.VerifyInfoDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ColoreInteroperabilitaEnum;
import it.ibm.red.business.enums.ColoreNotaEnum;
import it.ibm.red.business.enums.EsitoValidazioneSegnaturaInteropEnum;
import it.ibm.red.business.enums.MailOperazioniEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.PostaEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.RubricaChiamanteEnum;
import it.ibm.red.business.enums.StatoMailEnum;
import it.ibm.red.business.enums.StatoMailGUIEnum;
import it.ibm.red.business.enums.StatoNotificaContattoEnum;
import it.ibm.red.business.enums.TestoPredefinitoEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.enums.TipoOperazioneEnum;
import it.ibm.red.business.enums.TipoRubricaEnum;
import it.ibm.red.business.enums.TipologiaDestinatariEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.enums.TipologiaIndirizzoEmailEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.NotificaContatto;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IMailSRV;
import it.ibm.red.business.service.IPropertiesSRV;
import it.ibm.red.business.service.IProtocollaMailSRV;
import it.ibm.red.business.service.IRubricaSRV;
import it.ibm.red.business.service.ITipologiaDocumentoSRV;
import it.ibm.red.business.service.facade.ICasellePostaliFacadeSRV;
import it.ibm.red.business.service.facade.IComuneFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV;
import it.ibm.red.business.service.facade.IPropertiesFacadeSRV;
import it.ibm.red.business.service.facade.IProtocollaDsrFacadeSRV;
import it.ibm.red.business.service.facade.IProtocollaMailFacadeSRV;
import it.ibm.red.business.service.facade.IProvinciaFacadeSRV;
import it.ibm.red.business.service.facade.IRegioneFacadeSRV;
import it.ibm.red.business.service.facade.IRubricaFacadeSRV;
import it.ibm.red.business.service.facade.IScompattaMailFacadeSRV;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;
import it.ibm.red.business.utils.CollectionUtils;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.mbean.DocumentManagerBean;
import it.ibm.red.web.document.mbean.DsrManagerBean;
import it.ibm.red.web.document.mbean.component.RegioniProvinceComuniComponent;
import it.ibm.red.web.document.mbean.component.RubricaComponent;
import it.ibm.red.web.document.mbean.component.VerificaFirmaComponent;
import it.ibm.red.web.dto.CasellaMailStatoDTO;
import it.ibm.red.web.dto.ListElementDTO;
import it.ibm.red.web.dto.OrganigrammaInteroperabilitaDTO;
import it.ibm.red.web.enums.IconaMailEnum;
import it.ibm.red.web.enums.MailActionEnum;
import it.ibm.red.web.enums.ModeRubricaEnum;
import it.ibm.red.web.helper.dtable.DataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.component.DetailPreviewIntroComponent;
import it.ibm.red.web.mbean.component.SelezionaPrincipaleAllegatoComponent;
import it.ibm.red.web.mbean.master.IRubricaHandler;
import it.ibm.red.web.ricerca.mbean.RicercaAbstractBean;
import it.ibm.red.web.servlets.DownloadContentServlet.DocumentTypeEnum;
import it.ibm.red.web.utils.ExportUtils;

/**
 * Bean gestione mail.
 */
@Named(ConstantsWeb.MBean.MAIL_BEAN)
@ViewScoped
public class MailBean extends AbstractBean implements IRubricaHandler {

	/**
	 * Messaggio di errore eliminazione contatto.
	 */
	private static final String ERRORE_DURANTE_L_ELIMINAZIONE_DEL_CONTATTO = "Errore durante l'eliminazione del contatto";

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -6564748767676231991L;
	
	/**
	 * HTML Separator.
	 */
	private static final String BR_SEPARATOR_BR = "<br />------------------------<br />";

	/**
	 * Location rubrica DMDRC.
	 */
	private static final String ID_RIC_RUBRICA_DMDRC_HTML = "eastSectionForm:idMittenteMailPrefTabs_DMD:idRicRubrica_DMDRC";

	/**
	 * False - Label.
	 */
	private static final String FALSE = "false";

	/**
	 * Messaggio errore current mail uguale a {@code null}.
	 */
	private static final String ERRORE_RISCONTRATO_CURRENT_MAIL_NON_VALORIZZATA = "Errore riscontrato: CurrentMail non valorizzata.";

	/**
	 * Metodo JS.
	 */
	private static final String WRAP_ADD_EVENT_LISTENER_FOR_PROT_MAIL = "wrapAddEventListenerForProtMail();";

	/**
	 * Path component preferiti mittenti DMD.
	 */
	private static final String PREFERITIMITTENTI_DMD_RESOURCE_PATH = "eastSectionForm:idMittenteMailPrefTabs_DMD:idPreferitiMittenti_DMD";

	/**
	 * Path component ocntatti Protocolla Tbl.
	 */
	private static final String CONTATTIPROTOCOLLATBL_RESOURCE_PATH = "eastSectionForm:idMittenteMailPrefTabs_DMD:contattiProtocollaTbl";

	/**
	 * Path component mittente mail pref Tabs DMD.
	 */
	private static final String MITTENTE_MAIL_PREF_TABS_DMD_RESOURCE_PATH = "eastSectionForm:idMittenteMailPrefTabs_DMD";

	/**
	 * Path component dialog aggiungi contatto.
	 */
	private static final String AGGIUNGICONTATTODLG_RESOURCE_PATH = "eastSectionForm:aggiungiContattoDlg";

	/**
	 * Path component inbox Mail.
	 */
	private static final String INBOXMAIL_RESOURCE_PATH = "centralSectionForm:inboxMail";

	/**
	 * Id East section form.
	 */
	private static final String EAST_SECTION_FORM = "eastSectionForm";

	/**
	 * Id Central section form.
	 */
	private static final String CENTRAL_SECTION_FORM = "centralSectionForm";

	/**
	 * Nome file default html.
	 */
	private static final String DEFAULT_FILE_NAME = "Testo_Mail.html";

	/**
	 * Messaggio di avvenuto invio richiesta creazione contato.
	 */
	private static final String SUCCESS_CREAZIONE_CONTATTO_MSG = "Richiesta di creazione contatto inviata";

	/**
	 * JS per visualizzare il component: scegliContattoProtocollaDlg_WV.
	 */
	private static final String SHOW_SCEGLICONTATTOPROTOCOLLADLGWV_JS = "PF('scegliContattoProtocollaDlg_WV').show()";

	/**
	 * JS per l'hide del component: scegliContattoProtocollaDlg_WV.
	 */
	private static final String HIDE_SCEGLICONTATTOPROTOCOLLADLGWV_JS = "PF('scegliContattoProtocollaDlg_WV').hide()";

	/**
	 * JS per visualizzare il component: protocollaMail_WV.
	 */
	private static final String SHOW_PROTOCOLLAMAILWV_JS = "PF('protocollaMail_WV').show()";

	/**
	 * JS per visualizzare il component: aggiungiContattoDlg_WV.
	 */
	private static final String SHOW_AGGIUNGICONTATTODLGWV_JS = "PF('aggiungiContattoDlg_WV').show()";

	/**
	 * Label MITTENTE.
	 */
	private static final String MITTENTE_LABEL = "Mittente: ";

	/**
	 * Label ERRORE.
	 */
	private static final String ERRORE_LABEL = "Errore: ";

	/**
	 * Messaggio errore esecuzione azione richiesta.
	 */
	private static final String ERROR_ESECUZIONE_RICHIESTA_MSG = "Errore in fase di esecuzione dell'azione richiesta";

	/**
	 * Messaggio generico di errore. Occorre appendere l'operazione al messaggio.
	 */
	private static final String GENERIC_ERROR_OPERAZIONE_MSG = "Errore durante l'operazione: ";

	/**
	 * Messaggio errore impostazione dei dettagli.
	 */
	private static final String GENERIC_ERROR_IMPOSTAZIONE_DETTAGLI_MSG = "Errore durante l'impostazione dei dettagli (contenuto allegati). ";

	/**
	 * Messaggio errore nota obbligatoria.
	 */
	private static final String ERROR_NOTA_ASSENTE_MSG = "E' obbligatorio inserire una nota";

	/**
	 * Label destinatari.
	 */
	private static final String DESTINATARI_LABEL = "Destinatari: ";

	/**
	 * Label Destinatari Copia conoscenza.
	 */
	private static final String DESTINATARI_CC_LABEL = "Destinatari CC: ";

	/**
	 * Tag chiusura blocco br HTML.
	 */
	private static final String BR_END = "<br />";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(MailBean.class.getName());

	/**
	 * Indice primo tab.
	 */
	private static final String PRIMO_ATTIVO = "0";

	/**
	 * Indice secondo tab.
	 */
	private static final String SECONDO_ATTIVO = "1";

	/**
	 * Colore nota.
	 */
	private static final Integer COLORE_NOTA_VUOTO = -1;

	/**
	 * Separatore path.
	 */
	private static final String PATH_SEPARATOR = "|";

	/**
	 * Flag OTF.
	 */
	private static final Integer ON_THE_FLY = 1;
	/**
	 * Flag showWarnMessageMaxSize.
	 */
	private boolean showWarnMessageMaxSize;

	private enum PostCheckMailActionEnum {

		/**
		 * Valore.
		 */
		GIA_IN_CARICO_DLG,

		/**
		 * Valore.
		 */
		PROTOCOLLA_DLG;
	}

	/**
	 * Stream allegato.
	 */
	private transient StreamedContent fileAllegato;

	/**
	 * Insieme delle colonne del datatable delle mail.
	 */
	private List<Boolean> columns;

	/**
	 * Testo usato per filtrare le mail.
	 */
	private String filterString;

	/**
	 * Collezione master non filtrati.
	 */
	private Collection<EmailDTO> notFilterMasters;

	/**
	 * Utente selezionato.
	 */
	private UtenteDTO utente;

	/**
	 * Stato selezionato.
	 */
	private Integer selectedState;

	/**
	 * Stato selezionato.string.
	 */
	private String selectedStateString;

	/**
	 * Datatable mail.
	 */
	private DataTableHelper<CasellaPostaDTO, CasellaPostaDTO> accountsDTH;

	/**
	 * Datatable accounts.
	 */
	private DataTableHelper<CasellaPostaDTO, CasellaPostaDTO> otherAccountsDTH;

	/**
	 * Datatable mail.
	 */
	private DataTableHelper<EmailDTO, DetailEmailDTO> mailDTH;

	/**
	 * Booleano che indica se la selezione del primo master è avvenuto in automatico
	 * (questo serve perchè nel profilo mobile se il dettaglio è stato selezionato
	 * in automatico, in realtà non dovresti farlo vedere in quanto).
	 */
	private Boolean autoselection;

	/**
	 * Servizio di gestione mail.
	 */
	private IMailSRV mailSRV;

	/**
	 * Servizio.
	 */
	private IScompattaMailFacadeSRV scompattaMailSRV;

	/**
	 * Servizio.
	 */
	private ITipologiaDocumentoFacadeSRV tipologiaDocumentoSRV;

	/**
	 * Servizio di gestione rubrica.
	 */
	private IRubricaSRV rubSRV;

	/**
	 * Servizio.
	 */
	private IProtocollaDsrFacadeSRV fepaSRV;
	
	/**
	 * Servizio.
	 */	
	private IProtocollaMailFacadeSRV protocollaMailSRV;

	/**
	 * Servizio.
	 */
	private IPropertiesFacadeSRV propertiesSRV;

	/**
	 * Contenuto nota da inserire.
	 */
	private String currentTextNote;

	/**
	 * Contenuto nota da inserire Multi.
	 */
	private String currentTextNoteMulti;

	/**
	 * Contenuto nota da inserire Multi.
	 */
	private Integer currentColorNoteMulti;

	/**
	 * Contenuto nota da inserire da Ispeziona Mail.
	 */
	private String currentTextNoteIspeziona;

	/**
	 * Colore nota.
	 */
	private Integer coloreNotaDaIspeziona;

	/**
	 * Contenuto motivazione eliminazione.
	 */
	private String motiveDelete;

	/**
	 * Flag selezione di tutti i documenti.
	 */
	private Boolean selectAll = Boolean.FALSE;

	/**
	 * Casella postale selezionata.
	 */
	private String cpAttiva;

	/**
	 * Collezione stati mail.
	 */
	private Collection<StatoMailGUIEnum> states;

	/**
	 * Lista caselle postali disponibili.
	 */
	private List<CasellaPostaDTO> listaMail;

	/**
	 * Flag di visibilità della lista delle email.
	 */
	private boolean flagVisibilita;

	/**
	 * Variabili per l'inoltro della mail.
	 */
	private Collection<TestoPredefinitoDTO> motiviInoltro; // opzione motivi dell'inoltro

	/**
	 * Id Testo.
	 */
	private Integer testoSc; // Selezione del testo da aggiungere

	/**
	 * Testi.
	 */
	private List<String> testo;

	/**
	 * Modalità spedizione.
	 */
	private Integer modalitaSpedizioneSc; // Selezione del tio di spedizione

	/**
	 * Spedizione.
	 */
	private Map<String, Integer> spedizione;

	/**
	 * Testo messaggio.
	 */
	private String testoMessaggio; // testo della mail

	/**
	 * Variabili per rifiuto della mail.
	 */
	private Collection<TestoPredefinitoDTO> motiviRifiuto; // opzione motivo del rifiuto

	/**
	 * Id testo rifiuto.
	 */
	private Integer testoRifiutoSc; // motivo scelto

	/**
	 * Testo rifiuto.
	 */
	private String testoRifiuto; // testo dle motivo del rifiuto

	/**
	 * Flag inserisci allegati/originali.
	 */
	private boolean inserisciAllegatiOriginaliFlag; // flag per checkbox che include o meno gli allegati della mail rifiutata

	/**
	 * Contatto casella.
	 */
	private Contatto inserisciContattoCasellaPostaleItem;

	/**
	 * Destinatari per l'inoltro mail.
	 */
	private List<DestinatarioRedDTO> destinatari;

	/**
	 * Testo usato per la ricerca tra le mail.
	 */
	private String searchString;

	/**
	 * Data inizio usata per la ricerca tra le mail.
	 */
	private Date searchDataDa;

	/**
	 * Data fine usata per la ricerca tra le mail.
	 */
	private Date searchDataA;

	/**
	 * Flag per il controllo del giusto ordine delle date nella ricerca.
	 */
	private Boolean wrongDates;

	/**
	 * Flag per la visualizzazione del pnl ricerca.
	 */
	private Boolean ricercaTabFlag;

	/**
	 * Varibile per i dettagli della protocollazione.
	 */
	private DetailDocumentRedDTO detailsProt;

	/**
	 * Bean gestione documento.
	 */
	private DocumentManagerBean dmb;

	/**
	 * Flag renderizza protocollo.
	 */
	private Boolean renderProtocol;

	/**
	 * Azione.
	 */
	private MailActionEnum action;

	/**
	 * Casella mail selezionata.
	 */
	private CasellaMailStatoDTO cartellaMailSelected;

	/**
	 * Bean di sessione.
	 */
	private SessionBean sessionBean;

	/**
	 * Flag visualizza rubrica mail.
	 */
	private boolean showTabRubricaMail;

	/**
	 * Flag permesso protocollazione DSR.
	 */
	private boolean permessoProtocollaDSR;

	/**
	 * Flag mail rifiutata.
	 */
	private Boolean flagMultiRifiuta;

	/**
	 * Flag mail inoltra.
	 */
	private Boolean flagMultiInoltra;

	/**
	 * Flag inserisci nota multipla.
	 */
	private Boolean flagInserisciNotaMulti;

	/**
	 * Dettaglio mail corrente.
	 */
	private DetailEmailDTO currentmail;

	/**
	 * Flag visualizza protocollo.
	 */
	private boolean renderProtocolla;

	/**
	 * Flag visualizza inoltro.
	 */
	private boolean renderInoltra;

	/**
	 * Flag renderizza rifiuto.
	 */
	private boolean renderRifiuta;

	/**
	 * Flag renderizza rifiuta protocollo.
	 */
	private boolean renderRifiutaProt;

	/**
	 * Flag visualizza inoltra protocollo.
	 */
	private boolean renderInoltraProt;

	/**
	 * Flag visualizza creazione contatto.
	 */
	private boolean visualizzaCreaContatto;

	/**
	 * Flag creazione contatto.
	 */
	private boolean inCreazioneContatto;

	/**
	 * Campo da ordinare.
	 */
	private String campoDaOrdinare;

	/**
	 * Contatto per inserimento.
	 */
	private Contatto inserisciContattoItem;

	/**
	 * Componente che gestisce la visualzzazione . degli allegati in
	 * protocollazione.
	 * 
	 */
	protected DetailPreviewIntroComponent previewComponent;

	/**
	 * Flag modifica da shortcut.
	 */
	private boolean inModificaDaShortcut;

	/**
	 * Risultati ricerca contatti.
	 */
	private List<Contatto> resultRicercaContatti;

	/**
	 * Parametri ricerca rubrica.
	 */
	private RicercaRubricaDTO ricercaRubricaDTO;

	/**
	 * Contatti preferiti filtrati.
	 */
	private List<Contatto> contattiPreferitiFiltered;

	/**
	 * Risultati ricerca contatti filtrati.
	 */
	private List<Contatto> resultRicercaContattiFiltered;

	/**
	 * Flag permesso gestione richieste rubrica.
	 */
	private boolean permessoUtenteGestioneRichiesteRubrica;

	/**
	 * Contatti preferiti.
	 */
	private List<Contatto> contattiPreferiti;

	// abilitazioni inoltro/rifiuto
	/**
	 * Flag abilitazione inoltro.
	 */
	private boolean abilitazioneInoltra;

	/**
	 * Flag abilitazione inoltro con protocollo.
	 */
	private boolean abilitazioneInoltraConProtocollo;

	/**
	 * Flag abilitazione rifiuto.
	 */
	private boolean abilitazioneRifiuta;

	/**
	 * Flag abiltiazine rifiuto con protocollo.
	 */
	private boolean abilitazioneRifiutaConProtocollo;

	// rifiuto/inoltro con protocollo

	/**
	 * Operazione selezionata.
	 */
	private MailOperazioniEnum operazioneSelezionata;

	/**
	 * Motivazioni rifiuto con protocollo.
	 */
	private Collection<TestoPredefinitoDTO> motiviRifiutoConProtocollo;

	/**
	 * Identificativo testo rifiuto protocollo.
	 */
	private Long idTestoRifiutoProt;

	/**
	 * Testo rifiuto protocollo.
	 */
	private String testoRifiutoProt;

	/**
	 * Contatto rifiuto.
	 */
	private Contatto contattoRifiuta;

	/**
	 * Motivi inoltro con protocollo.
	 */
	private Collection<TestoPredefinitoDTO> motiviInoltroConProtocollo;

	/**
	 * Identificativo testo inoltro protocollo.
	 */
	private Long idTestoInoltroProt;

	/**
	 * Testo inoltro protocollo.
	 */
	private String testoInoltroProt;

	/**
	 * Contatto inoltro.
	 */
	private Contatto contattoInoltra;

	/**
	 * Lista contatti inoltro protocollo.
	 */
	private List<Contatto> contattiToInoltraProt;

	/**
	 * Lista contatti cc inoltro protocollo.
	 */
	private List<Contatto> contattiCCInoltraProt;

	/**
	 * Varibili per il flusso interoperabile.
	 */
	private AssegnatarioInteropDTO preassegnatarioInterop;

	/**
	 * Flag dirigente.
	 */
	private boolean isDirigente;

	/**
	 * Flag organigramma interoperabilità.
	 */
	private OrganigrammaInteroperabilitaDTO organigrammaInteroperabilita;

	/**
	 * Flag renderizza protocollazione semi-automatica.
	 */
	private boolean renderFromProtSemiautomatica;

	/**
	 * TO / CC.
	 */
	private List<ModalitaDestinatarioEnum> comboModalitaDestinatario;

	/**
	 * Lista icone mail.
	 */
	private List<IconaMailEnum> iconeHelpPageMail;

	/**
	 * Mittente mail protocollazione.
	 */
	private String mittenteMailProt;

	/**
	 * Page iterator.
	 */
	private transient PageIterator pageIterator;

	/**
	 * Indice pagina iteratore.
	 */
	private int pageIndex;

	// input ricerca mail

	/**
	 * Oggetto ricerca.
	 */
	private String oggettoRicerca;

	/**
	 * Mittente ricerca.
	 */
	private String mittenteRicerca;

	/**
	 * Anno ricerca.
	 */
	private Integer annoRicerca;

	/**
	 * Flag da protocollare in ricerca.
	 */
	private boolean daProtocollareRicerca;

	/**
	 * Flag ricerca inoltrate.
	 */
	private boolean inoltrateRicerca;

	/**
	 * Flag ricerca rifiutate.
	 */
	private boolean rifiutateRicerca;

	/**
	 * Flar ricerca eliminate.
	 */
	private boolean eliminateRicerca;

	/**
	 * Numero protocollo in ricerca.
	 */
	private String numeroProtocolloRicerca;

	/**
	 * Flag full-text ricerca.
	 */
	private boolean isFullTextRicerca;
	// fine input ricerca mail

	/**
	 * Azione post check.
	 */
	private PostCheckMailActionEnum postCheckMailAction;

	/**
	 * Flag ricerca mail.
	 */
	private boolean ricercaMail;

	/**
	 * Flag carica tutti gli elementi.
	 */
	private boolean caricaTuttiGliElementi;

	/**
	 * Flag seleziona tutti gli elementi.
	 */
	private List<Boolean> allElementCheckboxes;

	/**
	 * Riga datatable.
	 */
	private String rowMailDTH;

	/**
	 * Flag protocolla e mantieni mail.
	 */
	private boolean protocollaEMantieni;

	/**
	 * Servizio.
	 */
	private IDocumentManagerFacadeSRV documentManagerSRV;

	/**
	 * Dettaglio mail.
	 */
	private EmailDTO ripristinataDaRicerca;

	/**
	 * Radice albero.
	 */
	private DefaultTreeNode root;

	/**
	 * Contatto vecchio item.
	 */
	private Contatto contattoVecchioItem;

	/**
	 * Nota richiesta creazione.
	 */
	private String notaRichiestaCreazione;

	/**
	 * Nota richiesta modifica.
	 */
	private String notaRichiestaModifica;

	/**
	 * Nota richiesta eliminazione.
	 */
	private String notaRichiestaEliminazione;

	/**
	 * Contatto elimina.
	 */
	private Contatto eliminaContattoItem;

	/**
	 * Richiedi creazione datatable.
	 */
	private int fromRichiediCreazioneDatatable;

	/**
	 * Mail OTF.
	 */
	private String campoMailOnTheFly;

	/**
	 * Lista contatti nel gruppo.
	 */
	private List<Contatto> contattiNelGruppo;

	/**
	 * Contatto di cui richiedere eliminazione.
	 */
	private Contatto itemDaRichiedereEliminazione;

	/**
	 * Flag aggiorna tabelle.
	 */
	private boolean updateTbls;

	/**
	 * Servizio gestione firma.
	 */
	private ISignFacadeSRV signSRV;

	/**
	 * Compoentne verifica firma.
	 */
	private VerificaFirmaComponent verificaFirmaComponent;
	
	// ########################### Protocolla ####################################//
		// Lista dei possibili mittenti per protocollare

		/**
		 * Lista contatti.
		 */
		private List<Contatto> contattiProtocolla;

		/**
		 * Contatto protocolla.
		 */
		private Contatto contattoProtocolla;

		/**
		 * Contatto selezoinato.
		 */
		private Contatto contattoProtocollaSelezionato;

		/**
		 * Flag scegli contatto.
		 */
		private Boolean scegliContattoProt;

		/**
		 * Id contatto scelto.
		 */
		private Long idContattoScelto;

		/**
		 * Flag notifica protocollo.
		 */
		private Boolean notificaProtocolla;

		/**
		 * Id file selezionato.
		 */
		private String idFilePrincipale; // file principale scelto temp

		/**
		 * TreeTable per la selezione degli allegati principali.
		 */
		private SelezionaPrincipaleAllegatoComponent treeTableComponent;

		/**
		 * Flag selezionabile.
		 */
		private Boolean selectable;

		/**
		 * File principale.
		 */
		private String filePrincipale;

		/**
		 * Flag callback.
		 */
		private boolean callBackToAction;

		/**
		 * Lista contatti rubrica.
		 */
		private List<Contatto> contattiRubrica;

		/**
		 * Lista stato mail.
		 */
		private List<StatoMailEnum> statiMailSelezionati;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	public void postConstruct() {
		dmb = FacesHelper.getManagedBean(ConstantsWeb.MBean.DOCUMENT_MANAGER_BEAN);
		mailSRV = ApplicationContextProvider.getApplicationContext().getBean(IMailSRV.class);
		scompattaMailSRV = ApplicationContextProvider.getApplicationContext().getBean(IScompattaMailFacadeSRV.class);
		rubSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaSRV.class);
		fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IProtocollaDsrFacadeSRV.class);
		final ICasellePostaliFacadeSRV casellePostaliSRV = ApplicationContextProvider.getApplicationContext().getBean(ICasellePostaliFacadeSRV.class);
		protocollaMailSRV = ApplicationContextProvider.getApplicationContext().getBean(IProtocollaMailSRV.class);
		propertiesSRV = ApplicationContextProvider.getApplicationContext().getBean(IPropertiesSRV.class);
		tipologiaDocumentoSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoSRV.class);
		documentManagerSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentManagerFacadeSRV.class);
		signSRV = ApplicationContextProvider.getApplicationContext().getBean(ISignFacadeSRV.class);
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();

		previewComponent = new DetailPreviewIntroComponent(utente);

		setPermessoProtocollaDSR(utente.isPermessoDSR());

		cartellaMailSelected = sessionBean.getCartellaMailSelected();

		if (cartellaMailSelected.getStatoMails().equals(StatoMailEnum.INOLTRATA.getStatus())
				|| cartellaMailSelected.getStatoMails().equals(StatoMailEnum.RIFIUTATA.getStatus())) {
			columns = Arrays.asList(false, true, true, true, true, true, false, false, false, false, false);
		} else {
			columns = Arrays.asList(true, true, true, true, true, true, false, false, false, false, false);
		}

		renderProtocol = true;
		scegliContattoProt = false;
		contattoProtocolla = new Contatto();

		flagVisibilita = false;
		wrongDates = false;
		setricercaTabFlag(false);

		selectedState = cartellaMailSelected.getStatoMails();
		selectedStateString = StatoMailGUIEnum.get(selectedState).getLabel();

		states = Arrays.asList(StatoMailGUIEnum.values());
		autoselection = false;
		inserisciContattoCasellaPostaleItem = new Contatto();
		inserisciContattoItem = new Contatto();

		contattiPreferiti = new ArrayList<>();
		resultRicercaContatti = new ArrayList<>();
		contattiPreferitiFiltered = new ArrayList<>();

		resultRicercaContattiFiltered = new ArrayList<>();
		destinatari = new ArrayList<>();

		ricercaRubricaDTO = new RicercaRubricaDTO();
		ricercaRubricaDTO.setRicercaRED(true);

		currentmail = new DetailEmailDTO();
		mailDTH = new DataTableHelper<EmailDTO, DetailEmailDTO>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 2350060382643623673L;

			@Override
			protected DetailEmailDTO setDetailFromCurrent(final EmailDTO master) {
				setRenderDetail(true);
				autoselection = false;
				final DetailEmailDTO output = getMail(master);
				if (output != null) {
					setCurrentTextNote(output.getNota().getContentNota());
				}
				return output;
			}
		};
		caricaTuttiGliElementi = false;
		ricercaMail = false;
		refresh(false);
		cpAttiva = cartellaMailSelected.getCasellaMail();

		otherAccountsDTH = new DataTableHelper<CasellaPostaDTO, CasellaPostaDTO>() {

			private static final long serialVersionUID = 1L;

			@Override
			protected CasellaPostaDTO setDetailFromCurrent(final CasellaPostaDTO master) {

				return null;
			}
		};

		final List<CasellaPostaDTO> allCp = new ArrayList<>();

		// Evito che il flag selected delle caselle vada sugli oggetti in sessione
		for (final CasellaPostaDTO casella : sessionBean.getCp()) {
			if (!casella.getIndirizzoEmail().equals(cpAttiva)) {
				final CasellaPostaDTO casellaDaAggiungere = new CasellaPostaDTO();
				casellaDaAggiungere.setIndirizzoEmail(casella.getIndirizzoEmail());
				allCp.add(casellaDaAggiungere);
			}
		}
		otherAccountsDTH.setMasters(allCp);

		rowMailDTH = sessionBean.getTemplateMaxCodaMail();

		currentTextNoteMulti = "";
		currentColorNoteMulti = 0;
		currentTextNoteIspeziona = "";
		coloreNotaDaIspeziona = 0;
		inCreazioneContatto = false;

		contattoProtocollaSelezionato = new Contatto();

		final List<MailOperazioniEnum> responses = casellePostaliSRV.getResponses(utente.getIdUfficio(), cpAttiva);
		// se null, preferenze utente, altrimenti preferenze aoo
		if (utente.getInoltraResponse() == null) {
			abilitazioneInoltra = responses.contains(MailOperazioniEnum.INOLTRO);
		} else {
			abilitazioneInoltra = utente.getInoltraResponse().booleanValue();
		}
		// se null, preferenze utente, altrimenti preferenze aoo
		if (utente.getRifiutaResponse() == null) {
			abilitazioneRifiuta = responses.contains(MailOperazioniEnum.RIFIUTO);
		} else {
			abilitazioneRifiuta = utente.getRifiutaResponse().booleanValue();
		}
		abilitazioneInoltraConProtocollo = responses.contains(MailOperazioniEnum.INOLTRO_CON_PROTOCOLLAZIONE);
		abilitazioneRifiutaConProtocollo = responses.contains(MailOperazioniEnum.RIFIUTO_CON_PROTOCOLLAZIONE);

		motiviInoltro = protocollaMailSRV.getTestiPredefiniti(utente, TestoPredefinitoEnum.TESTO_PREDEFINITO_INOLTRO_MAIL);
		motiviRifiuto = protocollaMailSRV.getTestiPredefiniti(utente, TestoPredefinitoEnum.TESTO_PREDEFINITO_RIFIUTO_MAIL);
		motiviRifiutoConProtocollo = protocollaMailSRV.getTestiPredefiniti(utente, TestoPredefinitoEnum.TESTO_PREDEFINITO_RIFIUTO_MAIL_CON_PROTOCOLLO);
		motiviInoltroConProtocollo = protocollaMailSRV.getTestiPredefiniti(utente, TestoPredefinitoEnum.TESTO_PREDEFINITO_INOLTRO_MAIL_CON_PROTOCOLLO);

		setIsDirigente(UtenteDTO.hasRuoloDirigente(utente.getCodiceAoo(), utente.getIdRuolo()));
		organigrammaInteroperabilita = new OrganigrammaInteroperabilitaDTO(utente, cpAttiva);

		iconeHelpPageMail = new ArrayList<>();
		iconeHelpPageMail.addAll(Arrays.asList(IconaMailEnum.values()));
		resetCampiRicerca();
		// parto da false a meno che calcolaCaricati non si accorga che ho già caricato
		// tutto

		calcolaCaricati();

		setVerificaFirmaComponent(new VerificaFirmaComponent());

		statiMailSelezionati = new ArrayList<>();
	}

	/**
	 * Imposta a false {@link #updateTbls} aggiornando destinatariInoltro_tbl.
	 */
	public void updateInoltraProtTbl() {
		if (updateTbls) {
			updateTbls = false;
			FacesHelper.update("eastSectionForm:destinatariInoltro_tbl");
		}
	}

	/**
	 * Imposta a false {@link #updateTbls} aggiornando contattiA_inoltra.
	 */
	public void updateInoltraTbl() {
		if (updateTbls) {
			updateTbls = false;
			FacesHelper.update("eastSectionForm:contattiA_inoltra");
		}
	}

	/**
	 * Gestisce la paginazione in modo da fornire una nuova pagina quando occorre
	 * mostrare maggiori informazioni.
	 * 
	 * @param event
	 */
	public final void pageListener(final PageEvent event) {
		pageIndex = event.getPage();
		if (!ricercaMail && !caricaTuttiGliElementi) {
			pageIndex = event.getPage();

			final DataTable datatable = (DataTable) getClickedComponent(event);
			if (pageIndex + 1 >= datatable.getPageCount()) {
				// È stata richiesta una nuova pagina
				getNewPage();
			}
			if (pageIndex >= allElementCheckboxes.size()) {
				for (int index = allElementCheckboxes.size(); index < (pageIndex + 1); index++) {
					allElementCheckboxes.add(Boolean.FALSE);
				}
			}

		}
	}

	/**
	 * {@link #calcolaCaricati()}
	 * 
	 * @return
	 */
	public final String getLoadedInfo() {
		return calcolaCaricati();
	}

	/**
	 * @return
	 */
	public final String getSelectedInfo() {
		String selezionati = "SELEZIONATI : ";
		Integer elementiSelezionati = 0;
		Integer elementiTotali = 0;

		if (!CollectionUtils.isEmptyOrNull(mailDTH.getFilteredMasters())) {
			elementiTotali = mailDTH.getFilteredMasters().size();
		} else if (!CollectionUtils.isEmptyOrNull(mailDTH.getMasters())) {
			elementiTotali = mailDTH.getMasters().size();
		}

		if (!CollectionUtils.isEmptyOrNull(mailDTH.getSelectedMasters())) {
			elementiSelezionati = mailDTH.getSelectedMasters().size();
		}

		selezionati += elementiSelezionati + " / " + elementiTotali;

		return selezionati;
	}

	/**
	 * Calcola il numero dei documenti caricati e aggiorna i parametri associati.
	 * 
	 * @return la label corretta da mostrare che fa riferimento al numero
	 */
	private String calcolaCaricati() {

		String caricati;
		Integer elementiCaricati = 0;
		Integer elementiTotali = 0;

		if (StringUtils.isNullOrEmpty(filterString)) {
			caricati = "CARICATI : ";
		} else {
			caricati = "TROVATI : ";
		}

		if (!CollectionUtils.isEmptyOrNull(mailDTH.getFilteredMasters())) {
			elementiCaricati = mailDTH.getFilteredMasters().size();
		} else if (!CollectionUtils.isEmptyOrNull(mailDTH.getMasters())) {
			elementiCaricati = mailDTH.getMasters().size();
		}

		elementiTotali = cartellaMailSelected.getCounterMail();

		caricati += elementiCaricati + " / " + elementiTotali;

		if (elementiTotali != null && elementiCaricati != null && elementiTotali.intValue() == elementiCaricati.intValue()) {
			setCaricaTuttiGliElementi(true);
		}

		return caricati;
	}

	/**
	 * Gestisce i parametri legati alla ricerca chiudendone la dialog.
	 */
	public void chiudiRicerca() {
		annoRicerca = sessionBean.getMaxYearForSearch();
		mittenteRicerca = null;
		oggettoRicerca = null;
		searchDataDa = null;
		searchDataA = null;
		isFullTextRicerca = false;
		setDaProtocollareRicerca(false);
		setInoltrateRicerca(false);
		setRifiutateRicerca(false);
		setEliminateRicerca(false);
		numeroProtocolloRicerca = null;
	}

	/**
	 * {@link #refresh(Boolean, Boolean, Boolean)}.
	 */
	public void refresh(final Boolean refreshMenu) {
		refresh(refreshMenu, true, false);
	}

	/**
	 * Metodo per il refresh delle mail.
	 * 
	 * @param refreshMenu
	 * @param selectFirstElement
	 * @param isOperazioneRipristina
	 */
	public void refresh(final Boolean refreshMenu, final Boolean selectFirstElement, final Boolean isOperazioneRipristina) {
		if (!ricercaMail || Boolean.TRUE.equals(isOperazioneRipristina)) {
			refresh(refreshMenu, selectFirstElement);
		} else {
			aggiornaMailAfterResponseRicerca();
		}
	}

	private void refresh(final Boolean refreshMenu, final Boolean selectFirstElement) {
		try {
			initFlagRefresh();

			final DataTable dt = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(INBOXMAIL_RESOURCE_PATH);
			if (dt != null) {
				dt.reset();
				rowMailDTH = sessionBean.getTemplateMaxCodaMail();
				allElementCheckboxes = new ArrayList<>();
			}
			pageIterator = (PageIterator) FacesHelper.getObjectFromSession(Constants.SessionObject.MAILS, true);

			if (ripristinataDaRicerca != null) {
				// definisco la cartella mail dove atterrare (in arrivo) e la passo al
				// sessioneBean
				cartellaMailSelected.setStatoMails(StatoMailGUIEnum.INARRIVO.getValue());
				sessionBean.setCartellaMailSelected(this.getCartellaMailSelected());
				// aggiungo la mail alla lista dei master da mostrare nel dt
				notFilterMasters = new ArrayList<>();
				notFilterMasters.add(ripristinataDaRicerca);
				allElementCheckboxes.add(Boolean.FALSE);
				// settaggio stato in arrivo
				selectedState = StatoMailGUIEnum.INARRIVO.getValue();
				selectedStateString = StatoMailGUIEnum.get(selectedState).getLabel();
				// set flag ripristina e svuoto campo mail ripristinata
				sessionBean.setCasellaMailRipristina(cpAttiva);
				ripristinataDaRicerca = null;
				// preparo l'uscita dalla ricerca
				setRicercaMail(false);
				ricercaTabFlag = false;
				columns = Arrays.asList(true, true, true, true, true, true, false, false, false, false, false);
			} else {
				if (pageIterator == null && !caricaTuttiGliElementi) {
					pageIterator = sessionBean.loadEmails();
				}
				if (caricaTuttiGliElementi) {
					if (isRicercaMail()) {
						final EmailDTO emailAggiornata = mailSRV.getMailAggiornata(mailDTH.getCurrentMaster().getDocumentTitle(), utente.getFcDTO(), utente.getIdAoo());
						if (emailAggiornata != null) {
							final List<EmailDTO> mastersList = new ArrayList<>(notFilterMasters);
							final int index = mastersList.indexOf(mailDTH.getCurrentMaster());
							mastersList.set(index, emailAggiornata);
							notFilterMasters = mastersList;
						}
					} else {
						notFilterMasters = sessionBean.loadEmailsFull();
					}
					final Integer massimoElementi = Integer.parseInt(sessionBean.getTemplateMaxCodaMail());
					int booleaniDaInserire = 0;
					if (notFilterMasters != null && !notFilterMasters.isEmpty() && massimoElementi != null && massimoElementi.intValue() > 0) {
						if (notFilterMasters.size() % massimoElementi == 0) {
							booleaniDaInserire = notFilterMasters.size() / massimoElementi;
						} else {
							booleaniDaInserire = (notFilterMasters.size() / massimoElementi) + 1;
						}
					}
					for (int x = 0; x < booleaniDaInserire; x++) {
						allElementCheckboxes.add(Boolean.FALSE);
					}
				} else {
					notFilterMasters = new ArrayList<>();
					allElementCheckboxes.add(Boolean.FALSE);
					getNewPage();
				}
			}
			mailDTH.setMasters(notFilterMasters);
			if (Boolean.TRUE.equals(selectFirstElement)) {
				selectFirst();
			}
			// Si ricarica il menù delle mail per aggiornare i contatori di tutte le caselle
			if (Boolean.TRUE.equals(refreshMenu)) {
				final int counterMail = sessionBean.createSubMenuMail(false, selectedState, cpAttiva);
				cartellaMailSelected.setCounterMail(counterMail);
				calcolaCaricati();
			}
			manageTabMultiActions();
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Si è verificato un errore durante il recupero delle e-mail." + e);
		}
	}

	/**
	 * 
	 */
	private void initFlagRefresh() {
		visualizzaCreaContatto = false;
		renderProtocolla = false;
		renderInoltra = false;
		renderRifiuta = false;
		renderInoltraProt = false;
		renderRifiutaProt = false;
		scegliContattoProt = false;
		selectAll = true;
		motiveDelete = null;
		treeTableComponent = null;
		postCheckMailAction = null;
	}

	/**
	 * {@link #handleSelectAllPageCheckbox(Integer, Integer)}.
	 * 
	 * @param event
	 */
	public final void handleAllCheckBox(final ActionEvent event) {
		final DataTable dataTable = (DataTable) getClickedComponent(event);

		handleSelectAllPageCheckbox(dataTable.getRows(), pageIndex);
	}

	/**
	 * Gestisce la checkbox associata alla selezione di tutti gli elementi.
	 * 
	 * @param pageRows
	 * @param pageIndex
	 */
	private void handleSelectAllPageCheckbox(final Integer pageRows, final Integer pageIndex) {
		Boolean selectAllPageCheckbox = allElementCheckboxes.get(pageIndex);
		allElementCheckboxes.set(pageIndex, selectAllPageCheckbox == null || !selectAllPageCheckbox);

		selectAllPageCheckbox = allElementCheckboxes.get(pageIndex);

		List<EmailDTO> currentMastersList = null;
		// Se la lista dei filtrati non è vuota, si deve agire su questa
		if (CollectionUtils.isEmptyOrNull(mailDTH.getFilteredMasters())) {
			currentMastersList = new ArrayList<>(notFilterMasters);
		} else {
			currentMastersList = mailDTH.getFilteredMasters();
		}

		// Il numero di elementi in pagina potrebbe essere inferiore al numero massimo
		// di elementi per pagina
		final int endIndex = Math.min((pageIndex + 1) * pageRows, currentMastersList.size());

		EmailDTO m = null;
		for (int index = (pageIndex * pageRows); index < endIndex; index++) {
			m = currentMastersList.get(index);

			if (selectAllPageCheckbox && Boolean.FALSE.equals(m.getSelected())) {
				mailDTH.getSelectedMasters().add(m);
			} else if (Boolean.FALSE.equals(selectAllPageCheckbox) && Boolean.TRUE.equals(m.getSelected())) {
				mailDTH.getSelectedMasters().remove(m);
			}

			m.setSelected(selectAllPageCheckbox);
		}

		manageTabMultiActions();
	}

	private boolean getNewPage() {
		Boolean output = false;
		final Collection<EmailDTO> currentCollection = mailSRV.refineIterator(utente, pageIterator);
		// La pagina richiesta potrebbe richiedere il recupero di nuovi dati

		if (currentCollection != null && !currentCollection.isEmpty()) {
			notFilterMasters.addAll(currentCollection);
			if (mailDTH != null && mailDTH.getMasters() != null) {
				mailDTH.getMasters().addAll(currentCollection);
			}
			output = true;
		}

		return output;
	}

	/**
	 * Imposta le informazioni associate alla colonna al verificarsi dell'evento
	 * "Toggle".
	 * 
	 * @param event
	 */
	public final void onColumnToggleMail(final ToggleEvent event) {
		columns.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
	}

	/**
	 * Imposta a true {@link #caricaTuttiGliElementi} ed esegue un refresh.
	 */
	public final void loadAllMaster() {
		caricaTuttiGliElementi = true;
		refresh(false, true, false);
	}

	private void popolaAltriAccounts(final DataTableHelper<CasellaPostaDTO, CasellaPostaDTO> dth, final DataTableHelper<CasellaPostaDTO, CasellaPostaDTO> dthcopy) {
		final Collection<CasellaPostaDTO> nu = new ArrayList<>();
		nu.addAll(dth.getMasters());

		final Collection<CasellaPostaDTO> temp = new ArrayList<>();
		temp.addAll(dth.getMasters());

		dthcopy.setMasters(dth.getMasters());
		//
		for (final CasellaPostaDTO cp : temp) {
			if (cp.getIndirizzoEmail().equals(dth.getCurrentMaster().getIndirizzoEmail())) {
				nu.remove(cp);
			}
		}

		dthcopy.setMasters(nu);
	}

	/**
	 * Imposta il current master come mail da aggiungere.
	 */
	public void aggiungiContatto() {
		showTabRubricaMail = true;
		inCreazioneContatto = true;
		impostaMailDaAggiungere();
	}

	/**
	 * Visualizza il form per la creazione del contatto.
	 */
	public void openAggiungiContatto() {
		setVisualizzaCreaContatto(true);
		impostaMailDaAggiungere();
		currentmail = mailDTH.getCurrentDetail();
	}

	/**
	 * Reset di {@link #motiveDelete}.
	 */
	public void resetCancellaVars() {
		motiveDelete = "";
	}

	// ############## Gestione Master ##################
	/**
	 * Metodo per l'accesso al precedente master.
	 */
	public final void prevMaster() {
		mailDTH.selectMaster(mailDTH.prevMaster());
	}

	/**
	 * Metodo per l'accesso al successivo master.
	 */
	public final void nextMaster() {
		mailDTH.selectMaster(mailDTH.nextMaster());
	}

	/**
	 * Metodo per la rimozione di un master.
	 * 
	 * @param master master da rimuovere
	 */
	private void removeMaster(final EmailDTO master) {
		// se anche il master successivo viene eliminato, o quello selezionato è
		// l'ultimo della lista
		// può crearsi un'incongruenza nella visualizzazione del dettaglio
		final EmailDTO nextMaster = mailDTH.nextMaster();
		mailDTH.getMasters().remove(master);
		mailDTH.getSelectedMasters().remove(master);
		if (!mailDTH.getMasters().isEmpty()) {
			mailDTH.selectMaster(nextMaster);
		} else {
			mailDTH.setRenderDetail(false);
		}
	}

	/**
	 * Metodo per la rimozione di un master per guid.
	 * 
	 * @param guid guid del master da rimuovere
	 */
	private void removeMaster(final String guid) {
		EmailDTO targetMaster = null;
		for (final EmailDTO email : mailDTH.getMasters()) {
			if (email.getGuid().equals(guid)) {
				targetMaster = email;
				break;
			}
		}
		removeMaster(targetMaster);
	}

	/**
	 * Metodo per filtrare i master.
	 */
	public void filterMasters() {
		if (!caricaTuttiGliElementi && !ricercaMail) {
			// il datatable viene già resettato dal meodo loadAllMaster
			loadAllMaster();
			caricaTuttiGliElementi = true;
		} else {
			final DataTable d = getDataTableByIdComponent(INBOXMAIL_RESOURCE_PATH);
			if (d != null) {
				d.reset();
				rowMailDTH = sessionBean.getTemplateMaxCodaMail();
			}
		}

		if (!StringUtils.isNullOrEmpty(filterString)) {
			final Collection<EmailDTO> tmp = new ArrayList<>();
			for (final EmailDTO master : notFilterMasters) {
				master.setSelected(false);
				if (StringUtils.containsIgnoreCase(master.getOggetto(), filterString) || StringUtils.containsIgnoreCase(master.getMittente(), filterString)) {
					tmp.add(master);
				}
			}

			notFilterMasters = tmp;
			// setto il contenuto della tabella con i risultati della ricerca
			mailDTH.setMasters(notFilterMasters);
			// seleziono il primo della lista
			selectFirst();
			// annullo le selezioni precedenti
			mailDTH.getSelectedMasters().clear();

		} else {
			// se il campo filtro e vuoto annullo il filtro
			clearFilter();
		}

		mailDTH.setCheckAll(false);
		manageTabMultiActions();
	}

	/**
	 * Reset dei campi della dialog di ricerca delle mail.
	 */
	public void resetCampiRicerca() {
		annoRicerca = sessionBean.getMaxYearForSearch();
		searchString = null;
		searchDataDa = null;
		searchDataA = null;
		oggettoRicerca = null;
		mittenteRicerca = null;
		isFullTextRicerca = false;
		setDaProtocollareRicerca(false);
		setInoltrateRicerca(false);
		setRifiutateRicerca(false);
		setEliminateRicerca(false);
		numeroProtocolloRicerca = null;
	}

	/**
	 * Metodo per la ricerca delle mail.
	 */
	public void ricerca() {
		selectedState = -1; // stato della ricerca
		LOGGER.info("####INFO#### Inizio ricerca");
		if (!StringUtils.isNullOrEmpty(searchString) || searchDataDa != null || searchDataA != null || !StringUtils.isNullOrEmpty(oggettoRicerca)
				|| !StringUtils.isNullOrEmpty(mittenteRicerca) || !StringUtils.isNullOrEmpty(numeroProtocolloRicerca)) {
			Collection<EmailDTO> allEmails = null;

			// Verifico che le date siano corrette, se entrambe valorizzate
			if (searchDataDa != null && searchDataA != null) {
				if (!searchDataDa.before(searchDataA) && !searchDataDa.equals(searchDataA)) {
					setWrongDates(true);

					final FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Errore date: ", "Controlla l'ordine delle date inserite");
					FacesContext.getCurrentInstance().addMessage(null, msg);
					return;
				}
			} else {
				if (!StringUtils.isNullOrEmpty(numeroProtocolloRicerca)) {
					numeroProtocolloRicerca = numeroProtocolloRicerca.trim() + "/" + annoRicerca;
				}
			}

			statiMailSelezionati = new ArrayList<>();
			if (isDaProtocollareRicerca()) {
				statiMailSelezionati.add(StatoMailEnum.INARRIVO);
				statiMailSelezionati.add(StatoMailEnum.INARRIVO_NON_PROTOCOLLABILE_AUTOMATICAMENTE);
				statiMailSelezionati.add(StatoMailEnum.IN_FASE_DI_PROTOCOLLAZIONE_MANUALE);
			}

			if (isInoltrateRicerca()) {
				statiMailSelezionati.add(StatoMailEnum.INOLTRATA);
			}

			if (isRifiutateRicerca()) {
				statiMailSelezionati.add(StatoMailEnum.RIFIUTATA);
				statiMailSelezionati.add(StatoMailEnum.RIFIUTATA_AUTOMATICAMENTE);
			}

			if (isEliminateRicerca()) {
				statiMailSelezionati.add(StatoMailEnum.ELIMINATA);
			}

			LOGGER.info("####INFO#### Inizio Chiamata al service");
			allEmails = mailSRV.searchMails(utente.getFcDTO(), cpAttiva, utente.getId().intValue(), annoRicerca, searchDataDa, searchDataA, searchString, oggettoRicerca,
					mittenteRicerca, isFullTextRicerca, numeroProtocolloRicerca, statiMailSelezionati, utente.getIdAoo());
			LOGGER.info("####INFO#### Fine Chiamata al service");

			final Integer numMaxRisultati = Integer.parseInt(propertiesSRV.getByEnum(PropertiesNameEnum.RICERCA_MAX_RESULTS));
			if (numMaxRisultati != null && allEmails != null && numMaxRisultati.equals(allEmails.size())) {
				showWarnMessage(RicercaAbstractBean.MSG_NUM_MAX_RISULTATI);
			}

			final DataTable dt = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(INBOXMAIL_RESOURCE_PATH);
			if (dt != null) {
				dt.reset();
				rowMailDTH = sessionBean.getTemplateMaxCodaMail();
			}
			// imposto il master
			mailDTH.setMasters(allEmails);
			notFilterMasters = allEmails;
			// aggiorna colonne

			final Boolean[] flagsColVis = new Boolean[] { false, true, true, true, true, true, false, true, true, false, false }; // Valori visibilità colonne
			columns = Arrays.asList(flagsColVis);

			FacesHelper.executeJS("allignColumnToggler([true,true,true,false,true,true,false,false])"); // Proiezione json di flagsColVis sulle sole colonne toggabili.

			// seleziono il primo della lista
			selectFirst();
			setricercaTabFlag(true);
			ricercaMail = true;

			LOGGER.info("####INFO#### Fine ricerca");
			FacesHelper.executeJS("PF('ricercaDlg_WV').hide()");
			FacesHelper.update(CENTRAL_SECTION_FORM, EAST_SECTION_FORM);
		} else {
			showWarnMessage("Popolare almeno un campo di ricerca.");
		}
	}

	/**
	 * Seleziona il primo elemento del datatable.
	 */
	private void selectFirst() {
		mailDTH.selectFirst(true);
	}

	/**
	 * Metodo per eliminare i filtri.
	 */
	public final void clearFilter() {
		for (final EmailDTO m : notFilterMasters) {
			m.setSelected(false);
		}
		// ripopolo la tabella con i master non filtrati
		mailDTH.setMasters(notFilterMasters);
		// seleziono il primo della lista
		selectFirst();
		// annullo le selezioni precedenti
		mailDTH.getSelectedMasters().clear();
	}

	/**
	 * Metodo per la chiusura del dettaglio.
	 */
	public final void closeDetail() {
		mailDTH.setRenderDetail(false);
	}

//################ Gestione Mail ###################
	/**
	 * Recupero dettaglio da un master.
	 * 
	 * @param master master mail
	 * @return dettaglio mail
	 */
	private DetailEmailDTO getMail(final EmailDTO master) {
		LOGGER.info("####INFO#### Inizio getMail");
		DetailEmailDTO output = null;
		if (!StringUtils.isNullOrEmpty(master.getGuid())) {
			LOGGER.info("####INFO#### Inizio getDetailFromGuid");
			output = mailSRV.getDetailFromGuid(utente.getFcDTO(), master.getGuid(), utente.getIdAoo());
			LOGGER.info("####INFO#### Fine getDetailFromGuid");
		}

		if ((output == null)) {
			LOGGER.error("Impossibile recuperare il documento con DocumentTile = " + master.getGuid());
			showError("Impossibile recuperare la mail. Contattare l'Assistenza.");
			return null;
		}

		LOGGER.info("####INFO#### Fine getMail");
		return output;
	}

	/**
	 * Getter.
	 * 
	 * @return guid selezionati
	 */
	private Collection<String> getSelectedGuid() {
		final Collection<String> output = new ArrayList<>();
		for (final EmailDTO master : mailDTH.getSelectedMasters()) {
			output.add(master.getGuid());
		}
		return output;
	}

	/**
	 * Restituisce la lista delle colonne come checkbox.
	 * 
	 * @return columns
	 */
	public List<Boolean> getColumns() {
		return columns;
	}

	/**
	 * Metodo per gestire gli esiti delle operazioni massive.
	 * 
	 * @param esiti esiti operazioni massive
	 * @return messaggio da visualizzare in un dialog
	 */
	private String gestisciEsiti(final Collection<EsitoOperazioneMailDTO> esiti) {
		final StringBuilder sb = new StringBuilder("");
		for (final EsitoOperazioneMailDTO esito : esiti) {
			if (Boolean.FALSE.equals(esito.getEsito())) {
				sb.append(esito.getGuid() + ",");
			} else {
				removeMaster(esito.getGuid());
			}
		}
		final String tmp = StringUtils.cutLast(sb.toString());
		String output = "Modifica avvenuta.";
		if (!StringUtils.isNullOrEmpty(tmp)) {
			output += " Per le seguenti mail non è stato possibile eseguire la modifica [" + tmp + "]";
		}

		return output;
	}

	/**
	 * Metodo per la cancellazione multipla di mail in arrivo.
	 */
	public final void multiCancellaOld() {
		try {
			final Collection<String> guids = getSelectedGuid();
			final Collection<EsitoOperazioneMailDTO> esiti = mailSRV.cancella(utente.getFcDTO(), guids, motiveDelete, utente);
			final String output = gestisciEsiti(esiti);
			// seleziono sempre il primo della lista
			showInfoMessage(output);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di cancellazione", e);
			showError(e);
		}
	}

	/**
	 * Metodo per la cancellazione di una mail in arrivo.
	 */
	public void cancella() {
		try {
			// cambia lo stato da inbox a eliminata e rimuove il master dalla lista
			mailSRV.cancella(utente.getFcDTO(), mailDTH.getCurrentDetail().getGuid(), motiveDelete, utente);

			refresh(true);
			motiveDelete = "";
			showInfoMessage("Elimimazione dell'email eseguita con successo");
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di cambiamento stato", e);
			showError(e);
		}
	}

	/**
	 * Metodo per la cancellazione multipla di mail in arrivo.
	 */
	public void multiCancella() {
		try {
			for (final EmailDTO em : mailDTH.getSelectedMasters()) {
				mailSRV.cancella(utente.getFcDTO(), em.getGuid(), motiveDelete, utente);
			}
			refresh(true);
			motiveDelete = "";
			showInfoMessage("Eliminazione dell'email eseguita con successo");
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di cancellazione", e);
			showError(e);
		}
	}

	/**
	 * Metodo per il ripristino di una mail cancellata.
	 */
	public void ripristina() {
		try {
			// Cambia lo stato da eliminata a inbox rimuovendo il master dalla lista
			final EmailDTO ripristinata = mailSRV.ripristina(utente.getFcDTO(), mailDTH.getCurrentDetail(), utente.getIdAoo());

			if (isRicercaMail()) {
				ripristinataDaRicerca = ripristinata;
			}
			refresh(true, true, true);
			showInfoMessage("Mail ripristinata con successo");
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di ripristino dell'email", e);
			showError(e);
		}
	}

	/**
	 * Gestisce il ripristino della mail.
	 */
	public void multiRipristina() {
		try {
			for (final EmailDTO em : mailDTH.getSelectedMasters()) {
				mailSRV.ripristina(utente.getFcDTO(), em, utente.getIdAoo());
			}
			refresh(true, true, true);
			showInfoMessage("Mail ripristinata con successo");
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di ripristino dell'email", e);
			showError(e);
		}
	}

	/**
	 * Selezione Item.
	 * 
	 * @param event
	 */
	public void onItemSelect(final SelectEvent event) {
		// non fa nulla passando dal server si aggiornano i campi
	}

	/**
	 * Imposta il primo elemento come selezionato.
	 */
	public void refresh1() {
		selectFirst();
	}

	/**
	 * Metodo per il recupero delle mail della casella postale.
	 */
	public Collection<EmailDTO> recuperaPosta() {
		return mailSRV.getMails(utente.getFcDTO(), cpAttiva, StatoMailGUIEnum.get(selectedState), utente.getId().intValue(), utente.getIdAoo());
	}

	/**
	 * Inserimento/modifica nota.
	 */
	public final void changeNote() {
		try {
			final NotaDTO nota = new NotaDTO();
			nota.setContentNota(currentTextNote);
			if (!StringUtils.isNullOrEmpty(nota.getContentNota())) {
				mailSRV.setNotaMail(utente.getFcDTO(), mailDTH.getCurrentDetail().getGuid(), nota, null, utente.getIdAoo());
				mailDTH.getCurrentMaster().setNota(nota);
				mailDTH.getCurrentDetail().setNota(nota);
				showInfoMessage("Nota cambiata con successo.");
			} else {
				showWarnMessage("Specificare una nota.");
			}
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di modifica della nota", e);
			showError(e);
		}
	}

	/**
	 * Metodo per l'aggiornamento/inserimento di una nota in più mail.
	 */
	public final void multiChangeNote() {
		final NotaDTO nota = new NotaDTO();
		nota.setContentNota(currentTextNoteMulti);
		nota.setColore(ColoreNotaEnum.get(currentColorNoteMulti));
		salvaNota(nota);
	}

	/**
	 * Metodo per l'aggiornamento della nota da response ispeziona.
	 * 
	 * @param nota
	 */
	public final void ispezionaChangeNote() {
		final NotaDTO nota = new NotaDTO();
		nota.setContentNota(currentTextNoteIspeziona);
		nota.setColore(ColoreNotaEnum.get(coloreNotaDaIspeziona));
		salvaNota(nota);
	}

	private void salvaNota(final NotaDTO nota) {
		try {
			if (nota == null || StringUtils.isNullOrEmpty(nota.getContentNota())) {
				showWarnMessage("Specificare una nota.");
			} else if (nota.getColore() == null) {
				showWarnMessage("Selezionare una bandierina.");
			} else {
				StringBuilder tooltipPreassegnatario = new StringBuilder("");

				if (Boolean.TRUE.equals(flagInserisciNotaMulti)) {
					for (final EmailDTO em : mailDTH.getSelectedMasters()) {
						preassegnatarioInterop = setPreassegnazioneInteroperabilita();

						if (preassegnatarioInterop != null) {
							tooltipPreassegnatario = new StringBuilder(preassegnatarioInterop.getDescrizioneUfficio());

							if (preassegnatarioInterop.getDescrizioneUtente() != null) {
								final String[] nomeCompleto = preassegnatarioInterop.getDescrizioneUtente().split(",");
								tooltipPreassegnatario.append(" - ").append(nomeCompleto[0]).append(" ").append(nomeCompleto[1]);
							}
						}

						em.setTooltipPreassegnatario(tooltipPreassegnatario.toString());
						em.setPreassegnatarioInterop(preassegnatarioInterop);
						em.setNota(nota);

						mailSRV.setNotaMail(utente.getFcDTO(), em.getGuid(), nota, preassegnatarioInterop, utente.getIdAoo());
					}
				} else {
					if (setPreassegnazioneInteroperabilita() != null) {
						tooltipPreassegnatario = new StringBuilder(preassegnatarioInterop.getDescrizioneUfficio());
						if (!StringUtils.isNullOrEmpty(preassegnatarioInterop.getDescrizioneUtente())) {
							tooltipPreassegnatario.append(" - ").append(preassegnatarioInterop.getDescrizioneUtente());
						}

						mailDTH.getCurrentMaster().setColorePreassegnatario(ColoreInteroperabilitaEnum.VERDE);
					} else {
						mailDTH.getCurrentMaster().setColorePreassegnatario(ColoreInteroperabilitaEnum.GIALLO);
					}

					mailDTH.getCurrentMaster().setTooltipPreassegnatario(tooltipPreassegnatario.toString());

					mailDTH.getCurrentMaster().setPreassegnatarioInterop(preassegnatarioInterop);
					mailDTH.getCurrentDetail().setPreassegnatarioInterop(preassegnatarioInterop);

					mailDTH.getCurrentMaster().setNota(nota);
					mailDTH.getCurrentDetail().setNota(nota);
					mailSRV.setNotaMail(utente.getFcDTO(), mailDTH.getCurrentMaster().getGuid(), nota, preassegnatarioInterop, utente.getIdAoo());
				}
				// Aggiornamento dei contatori delle caselle
				refresh(true);
				FacesHelper.update("westSectionForm:idTabMain_LeftAside:idMenuLeftAside:idMenuMail");
				FacesHelper.update(EAST_SECTION_FORM);
				FacesHelper.update(CENTRAL_SECTION_FORM);

				FacesHelper.executeJS("PF('inserisciNotaMultiDlg_WV').hide()");
				showInfoMessage("Note cambiate con successo.");
			}
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di modifica delle note.", e);
			showError(e);
		}
	}

	/**
	 * Metodo per impostare il contenuto della nota all'avvio della dialog del multi
	 * inserimento.
	 */
	public void inizializzaNota(final String comeFromTabMulti) {
		currentTextNoteMulti = inizializzaNotaGeneric(comeFromTabMulti);
		currentColorNoteMulti = inizializzaColoreNotaGeneric(comeFromTabMulti);
		inizializzaNotaPreassegnatarioGeneric(comeFromTabMulti);
	}

	/**
	 * Metodo per impostare il contenuto della nota all'avvio della dialog Ispeziona
	 * Mail.
	 */
	public void inizializzaNotaIspeziona(final String comeFromTabMulti) {
		currentTextNoteIspeziona = inizializzaNotaGeneric(comeFromTabMulti);
		setColoreNotaDaIspeziona(inizializzaColoreNotaGeneric(comeFromTabMulti));
	}

	/**
	 * Imposta testo nota.
	 * 
	 * @param comeFromTabMulti
	 * @return
	 */
	private String inizializzaNotaGeneric(final String comeFromTabMulti) {
		String testoNota = "";
		flagInserisciNotaMulti = Boolean.valueOf(comeFromTabMulti);
		if (Boolean.TRUE.equals(flagInserisciNotaMulti)) {
			// se è selezionata solo una mail prendiamo, se esiste, la nota della mail
			if (mailDTH.getSelectedMasters().size() == 1 && mailDTH.getSelectedMasters().get(0).getNota().getContentNota() != null) {
				testoNota = mailDTH.getSelectedMasters().get(0).getNota().getContentNota();
			}
		} else {
			testoNota = mailDTH.getCurrentDetail().getNota().getContentNota();
		}

		return testoNota;
	}

	/**
	 * Imposta colore nota
	 * 
	 * @param comeFromTabMulti
	 * @return
	 */
	private Integer inizializzaColoreNotaGeneric(final String comeFromTabMulti) {
		Integer coloreNota = COLORE_NOTA_VUOTO;
		flagInserisciNotaMulti = Boolean.valueOf(comeFromTabMulti);

		if (Boolean.TRUE.equals(flagInserisciNotaMulti) && mailDTH.getSelectedMasters().size() == 1 && mailDTH.getSelectedMasters().get(0).getNota().getColore() != null) {
			coloreNota = mailDTH.getSelectedMasters().get(0).getNota().getColore().getId();
		} else {
			if (mailDTH.getCurrentDetail().getNota().getColore() != null) {
				coloreNota = mailDTH.getCurrentDetail().getNota().getColore().getId();
			}
		}

		return coloreNota;
	}

	/**
	 * Cancellazione nota.
	 */
	public final void deleteNote() {
		try {
			// cancello la nota della mail e aggiorno i bottoni nella view
			mailSRV.cancellaNotaMail(utente.getFcDTO(), mailDTH.getCurrentDetail().getGuid(), utente.getIdAoo());
			mailDTH.getCurrentMaster().setNota(new NotaDTO());
			mailDTH.getCurrentDetail().setNota(new NotaDTO());

			mailDTH.getCurrentMaster().setTooltipPreassegnatario(null);
			mailDTH.getCurrentMaster().setPreassegnatarioInterop(null);
			mailDTH.getCurrentMaster().setColorePreassegnatario(ColoreInteroperabilitaEnum.GIALLO);
			mailDTH.getCurrentDetail().setPreassegnatarioInterop(null);

			showInfoMessage("Nota cancellata con successo.");
			currentTextNote = null;
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di cancellazione della nota", e);
			showError(e);
		}
	}

//################# Inoltro Mail #####################	
	/**
	 * Il Metodo cambia lo stato dell mail in 'inoltrata'.
	 */
	public void inoltra() {
		try {
			final List<DestinatarioCodaMailDTO> destinatariToList = new ArrayList<>();
			final List<DestinatarioCodaMailDTO> destinatariCCList = new ArrayList<>();
			boolean noMail = false;

			for (final DestinatarioRedDTO dest : destinatari) {
				if (dest != null) {
					final Contatto contTo = dest.getContatto();
					// E' la riga vuota che aggiungo con il cntatto vuoto che deve essere esclusa
					// alla fine
					if (contTo != null && dest.getContattiGruppo() == null && StringUtils.isNullOrEmpty(contTo.getAliasContatto())
							&& StringUtils.isNullOrEmpty(contTo.getMailSelected()) && StringUtils.isNullOrEmpty(contTo.getMailPec())
							&& StringUtils.isNullOrEmpty(contTo.getMail())) {
						continue;
					}
					if (dest.getContattiGruppo() == null) {
						if (StringUtils.isNullOrEmpty(contTo.getMail()) && StringUtils.isNullOrEmpty(contTo.getMailPec())) {
							if (!StringUtils.isNullOrEmpty(contTo.getMailSelected()) && TipologiaIndirizzoEmailEnum.PEC.equals(contTo.getTipologiaEmail())) {
								contTo.setMailPec(contTo.getMailSelected());
							} else if (!StringUtils.isNullOrEmpty(contTo.getMailSelected()) && TipologiaIndirizzoEmailEnum.PEO.equals(contTo.getTipologiaEmail())) {
								contTo.setMail(contTo.getMailSelected());
							} else {
								noMail = true;
							}
						}
					} else {
						for (final Contatto contattoGruppo : dest.getContattiGruppo()) {
							if (StringUtils.isNullOrEmpty(contattoGruppo.getMail()) && StringUtils.isNullOrEmpty(contattoGruppo.getMailPec())) {
								noMail = true;
							}
						}
					}

					if (contTo != null) {
						Collection<Contatto> contattiToScompattati = new ArrayList<>();
						if (contTo != null && (TipoRubricaEnum.GRUPPOEMAIL.equals(contTo.getTipoRubricaEnum()) || TipoRubricaEnum.GRUPPO.equals(contTo.getTipoRubricaEnum()))
								&& contTo.getContattiContenutiInGruppo() != null && !contTo.getContattiContenutiInGruppo().isEmpty()) {
							contattiToScompattati = contTo.getContattiContenutiInGruppo();
						} else if (dest.getContattiGruppo() != null) {
							for (final Contatto contattoGruppo : dest.getContattiGruppo()) {
								contattiToScompattati.add(contattoGruppo);
							}
						} else {
							contattiToScompattati.add(contTo);
						}
						for (final Contatto contatto : contattiToScompattati) {
							if (contatto != null && ((contatto.getContattoID() != null && contatto.getContattoID() != 0)
									|| (contatto.getContattoID() == null || contatto.getOnTheFly() == 1))) {
								noMail = creaStringaDestinatari(destinatariToList, destinatariCCList, noMail, dest, contatto);
							}
						}
					}
				}
			}
			// Si determina tramite il flag da dove è partita l'azione, Da azioni singole o
			// da tab azioni massive
			if (Boolean.TRUE.equals(flagMultiInoltra)) {
				currentmail = getMail(mailDTH.getSelectedMasters().get(0));
			} else {
				currentmail = mailDTH.getCurrentDetail();
			}
			final boolean hasAllegati = (currentmail != null && currentmail.getAllegati() != null && !currentmail.getAllegati().isEmpty());
			// Se null viene direttamente calcolata dalla casella mittente
			final Integer tipologiaMessaggioId = null;
			final boolean isNotifica = false;
			if (noMail) {
				showError("Il destinatario inserito non ha l'indirizzo di posta valorizzato");
			} else if (currentmail != null && StringUtils.isNullOrEmpty(currentmail.getOggetto())) {
				showError("Valorizzare l'oggetto della mail da inoltrare.");
			} else {
				if (!destinatariToList.isEmpty() && currentmail != null) {
					mailSRV.inoltra(utente.getFcDTO(), currentmail.getGuid(), cpAttiva, cpAttiva, destinatariToList, destinatariCCList, currentmail.getOggetto(),
							testoMessaggio, AzioneMail.INOLTRA, null, TipologiaInvio.INOLTRA, tipologiaMessaggioId, isNotifica, hasAllegati, modalitaSpedizioneSc, null,
							utente);

					refresh(true);

					showInfoMessage("Inoltro eseguito con successo.");
					FacesHelper.executeJS("PF('inoltraDlg_WV').hide()");
				} else if (currentmail != null) {
					showError("Selezionare almeno un destinatario.");
				} else {
					LOGGER.info("Current Mail risulta null, non è stato possibile procedere con l'inoltro.");
					throw new RedException("Current Mail risulta null, non è stato possibile procedere con l'inoltro.");
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di inoltro", e);
			showError(e);
		}
	}

	/**
	 * Aggiorna i parametri dopo l'esecuzione della response di ricerca.
	 */
	public void aggiornaMailAfterResponseRicerca() {
		initFlagRefresh();
		final EmailDTO emailAggiornata = mailSRV.getMailAggiornata(mailDTH.getCurrentMaster().getDocumentTitle(), utente.getFcDTO(), utente.getIdAoo());
		if (emailAggiornata != null) {
			BeanUtils.copyProperties(emailAggiornata, mailDTH.getCurrentMaster());
			FacesHelper.update("centralSectionForm:inbox");
		}

		final int counterMail = sessionBean.createSubMenuMail(false, selectedState, cpAttiva);
		cartellaMailSelected.setCounterMail(counterMail);
		calcolaCaricati();
	}

	private boolean creaStringaDestinatari(final List<DestinatarioCodaMailDTO> destinatariToList, final List<DestinatarioCodaMailDTO> destinatariCCList, boolean noMail,
			final DestinatarioRedDTO dest, final Contatto contatto) {
		final DestinatarioCodaMailDTO destTo = new DestinatarioCodaMailDTO();

		if (!StringUtils.isNullOrEmpty(contatto.getMailSelected())) {
			destTo.setIndirizzoMail(contatto.getMailSelected());
			// Gestione dei casi in cui dalla maschera non arrivi la mail selezionata
		} else {
			noMail = true;
		}

		if (contatto.getContattoID() != null) {
			destTo.setIdContatto(contatto.getContattoID());
		} else {
			final Contatto contOnTheFly = rubSRV.insertContattoOnTheFly(contatto, utente.getIdAoo());
			destTo.setIdContatto(contOnTheFly.getContattoID());
		}

		if (dest.getModalitaDestinatarioEnum().equals(ModalitaDestinatarioEnum.TO)) {
			destinatariToList.add(destTo);
		} else {
			destinatariCCList.add(destTo);
		}
		return noMail;
	}

	/**
	 * Metodo per resettare i destinatari.
	 */
	public void resetDestinatariInoltro() {
		destinatari = null;
	}

	/**
	 * Metodo che prepara all'inoltro dell'e-mail.
	 * 
	 * @param comeFromTabMulti
	 */
	public void inoltroMng(final String comeFromTabMulti) {
		currentmail = new DetailEmailDTO();
		flagMultiInoltra = Boolean.valueOf(comeFromTabMulti);
		comboModalitaDestinatario = new ArrayList<>();
		comboModalitaDestinatario.add(ModalitaDestinatarioEnum.TO);
		comboModalitaDestinatario.add(ModalitaDestinatarioEnum.CC);
		renderInoltra = true;
		renderRifiuta = false;
		renderProtocolla = false;
		renderInoltraProt = false;
		renderRifiutaProt = false;
		scegliContattoProt = false;
		contattiRubrica = new ArrayList<>();

		// Per opzione dropdown testo
		testo = new ArrayList<>();
		testo.add(" ");
		testo.add("Per il seguito di Competenza");
		String corpoMailDaInoltrare = "";

		modalitaSpedizioneSc = ModalitaSpedizioneMail.SINGOLA;
		// Testo e-mail
		if (Boolean.TRUE.equals(flagMultiInoltra)) {
			currentmail = getMail(mailDTH.getSelectedMasters().get(0));
			if (currentmail != null) {
				final String mittente = MITTENTE_LABEL + currentmail.getMittente() + BR_END;
				final String destinatariTO = DESTINATARI_LABEL + currentmail.getDestinatario() + BR_END;
				final String destinatariCC = !StringUtils.isNullOrEmpty(currentmail.getDestinatarioCC()) ? DESTINATARI_CC_LABEL + currentmail.getDestinatarioCC() + BR_END
						: "";
				corpoMailDaInoltrare = (BR_SEPARATOR_BR + mittente + destinatariTO + destinatariCC + BR_END + currentmail.getTesto());
				setTestoMessaggio(corpoMailDaInoltrare);
			}
		} else {
			currentmail = mailDTH.getCurrentDetail();
			final String mittente = MITTENTE_LABEL + currentmail.getMittente() + BR_END;
			final String destinatariTO = DESTINATARI_LABEL + currentmail.getDestinatario() + BR_END;
			final String destinatariCC = !StringUtils.isNullOrEmpty(currentmail.getDestinatarioCC()) ? DESTINATARI_CC_LABEL + currentmail.getDestinatarioCC() + BR_END : "";
			corpoMailDaInoltrare = (BR_SEPARATOR_BR + mittente + destinatariTO + destinatariCC + BR_END + currentmail.getTesto());
			setTestoMessaggio(corpoMailDaInoltrare);
		}

		// Inizialmente aggiungiamo un destinatario 'vuoto' nella tabella dei
		// destiantari
		destinatari = new ArrayList<>();
		aggiungiDestinatario();
	}

	/**
	 * Imposta il testo del messaggio da inoltrare in base all'opzione scelta nella
	 * dropdown.
	 */
	public void impostaTesto() {
		String testoAggiornato = "";

		final String mittente = MITTENTE_LABEL + currentmail.getMittente() + BR_END;
		final String destinatariTO = DESTINATARI_LABEL + currentmail.getDestinatario() + BR_END;
		final String destinatariCC = !StringUtils.isNullOrEmpty(currentmail.getDestinatarioCC()) ? DESTINATARI_CC_LABEL + currentmail.getDestinatarioCC() + BR_END : "";
		final String testoOriginale = (BR_SEPARATOR_BR + mittente + destinatariTO + destinatariCC + BR_END + currentmail.getTesto());

		if (testoSc == null) {
			setTestoSc(-1);
			testoAggiornato = testoOriginale;
		} else {
			for (final TestoPredefinitoDTO t : motiviInoltro) {
				if (t.getIdTestoPredefinito().intValue() == testoSc.intValue()) {
					testoAggiornato = t.getCorpotesto().replace("\n", "<br/>");
					break;
				}
			}
			testoAggiornato = testoAggiornato + BR_END + testoOriginale;
		}

		setTestoMessaggio(testoAggiornato);
	}

	/**
	 * Imposta i preferiti per la dialog di inoltra.
	 */
	public void openRubricaPreferiti() {
		if (contattiPreferiti != null && contattiPreferiti.isEmpty()) {
			contattiPreferiti = rubSRV.getPreferiti(utente.getIdUfficio(), utente.getIdAoo(), true);
		}
		fromRichiediCreazioneDatatable = -1;
		scegliContattoProt = false;
		setRenderProtocolla(false);
		setInserisciContattoItem(new Contatto());

	}

	/**
	 * Reinizializza i parametri per la pulizia della tabella dei destinatari.
	 */
	public void pulisciTabellaDestinatari() {
		if (!it.ibm.red.business.utils.CollectionUtils.isEmptyOrNull(destinatari)) {
			final DestinatarioRedDTO destinatario = new DestinatarioRedDTO();
			destinatario.setContatto(new Contatto());
			destinatario.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
			destinatario.getContatto().setIdDataTable(UUID.randomUUID().toString());
			destinatari = new ArrayList<>();
			destinatari.add(destinatario);
		}
	}

	// ############ Rifiuta #########################
	/**
	 * Il Metodo cambia lo stato dell mail in 'rifiutata'.
	 */
	public void rifiuta() {
		try {

			// Si determina tramite il flag da dove è partita l'azione, Da azioni singole o
			// da tab azioni massive
			if (Boolean.TRUE.equals(flagMultiRifiuta)) {
				currentmail = getMail(mailDTH.getSelectedMasters().get(0));
			} else {
				currentmail = mailDTH.getCurrentDetail();
			}

			String testoOriginale = "";
			if (currentmail != null) {
				testoOriginale = currentmail.getTesto();
			}

			final String corpoMailDaInoltrare = testoRifiuto + testoOriginale;

			// se null viene direttamente calcolata dalla casella mittente
			final Integer tipologiaMessaggioId = null;

			final Boolean isNotifica = false;

			if (currentmail != null) {
				if (StringUtils.isNullOrEmpty(currentmail.getOggetto())) {
					showError("Valorizzare l'oggetto della mail da spedire per il rifiuto.");
				} else {
					// Mittente e destinatario si invertono nel rifiuto
					mailSRV.rifiuta(utente.getFcDTO(), currentmail.getGuid(), cpAttiva, cpAttiva, currentmail.getMittente(),
							"0," + currentmail.getMittente().trim() + ",2,E,TO", currentmail.getOggetto(), corpoMailDaInoltrare, AzioneMail.RIFIUTA, null,
							TipologiaInvio.RISPONDI, tipologiaMessaggioId, isNotifica, inserisciAllegatiOriginaliFlag, null, null, utente);
					refresh(true);
					showInfoMessage("Rifiuto eseguito con successo.");
					FacesHelper.executeJS("PF('rifiutaDlg_WV').hide()");
				}
			}

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di cambiamento stato", e);
			showError(e);
		}
	}

	/**
	 * Reset input.
	 */
	public void clearFormRifiuta() {
		setTestoRifiutoSc(-1);
		setTestoRifiuto("");
		setInserisciAllegatiOriginaliFlag(false);
	}

	/**
	 * Visualizza il panel di rifiuto.
	 * 
	 * @param comeFromTabMulti
	 */
	public void rifiutaMng(final String comeFromTabMulti) {
		currentmail = new DetailEmailDTO();

		renderProtocolla = false;
		renderInoltra = false;
		renderRifiuta = true;
		renderInoltraProt = false;
		renderRifiutaProt = false;
		scegliContattoProt = false;

		flagMultiRifiuta = Boolean.valueOf(comeFromTabMulti);

		// Si determina tramite il flag da dove è partita l'azione, Da azioni singole o
		// da tab azioni massive
		if (Boolean.TRUE.equals(flagMultiRifiuta)) {
			currentmail = getMail(mailDTH.getSelectedMasters().get(0));
		} else {
			currentmail = mailDTH.getCurrentDetail();
		}

	}

	/**
	 * Imposta il testo di rifiuto.
	 */
	public void impostaTestoRifiuta() {
		if (testoRifiutoSc != null) {
			for (final TestoPredefinitoDTO t : motiviRifiuto) {
				if (t.getIdTestoPredefinito().intValue() == testoRifiutoSc.intValue()) {
					setTestoRifiuto(t.getCorpotesto().replace("\n", "<br/>"));
					break;
				}
			}
		} else {
			setTestoRifiutoSc(-1);
			setTestoRifiuto("");
		}
	}

	/**
	 * Gestisce l'inoltro degli allegati.
	 */
	public void inserisciAllegatiOriginali() {
		// Il metodo conterrà il codice per l'inoltro degli allegati
	}

	// ############ Rifiuta con protocollo #########################
	/**
	 * Gestisce l'operazione RIFIUTO_CON_PROTOCOLLAZIONE.
	 * 
	 * @see MailOperazioniEnum.
	 * @param comeFromTabMulti
	 */
	public void rifiutaConProtocolloMng(final String comeFromTabMulti) {
		currentmail = new DetailEmailDTO();

		flagMultiRifiuta = Boolean.valueOf(comeFromTabMulti);

		renderProtocolla = false;
		renderInoltra = false;
		renderRifiuta = false;
		renderInoltraProt = false;
		renderRifiutaProt = true;
		scegliContattoProt = false;

		// Si determina tramite il flag da dove è partita l'azione, Da azioni singole o
		// da tab azioni massive
		if (Boolean.TRUE.equals(flagMultiRifiuta)) {
			currentmail = getMail(mailDTH.getSelectedMasters().get(0));
		} else {
			currentmail = mailDTH.getCurrentDetail();
		}

		operazioneSelezionata = MailOperazioniEnum.RIFIUTO_CON_PROTOCOLLAZIONE;
		contattoRifiuta = null;
		idTestoRifiutoProt = null;
		testoRifiutoProt = "";

	}

	/**
	 * Imposta il testo di rifiuto.
	 */
	public void impostaTestoRifiutaProt() {
		if (idTestoRifiutoProt != null) {
			for (final TestoPredefinitoDTO t : motiviRifiutoConProtocollo) {
				if (t.getIdTestoPredefinito().intValue() == idTestoRifiutoProt.intValue()) {
					setTestoRifiutoProt(t.getCorpotesto());
					break;
				}
			}
		} else {
			setIdTestoRifiutoProt(-1L);
			setTestoRifiutoProt("");
		}
	}

	/**
	 * Gestisce il rifiuto con protocollo eseguendo una validazione sui parametri e
	 * mostrando eventuali errori nel formato.
	 */
	public void rifiutaConProtocollo() {
		try {
			// Si determina tramite il flag da dove è partita l'azione, da azioni singole o
			// da tab azioni massive
			if (Boolean.TRUE.equals(flagMultiRifiuta)) {
				currentmail = getMail(mailDTH.getSelectedMasters().get(0));
			} else {
				currentmail = mailDTH.getCurrentDetail();
			}
			if (StringUtils.isNullOrEmpty(getTestoRifiutoProt())) {
				showError("Valorizzare il motivo del rifiuto.");
				return;
			}
			if (currentmail != null && checkCurrentMailInoltraRifiutaConProtocollo(currentmail)) {
				if (contattoRifiuta == null || contattoRifiuta.getContattoID() == null) {
					// recupera i contatti corrispondenti alla stringa mittente, se ci sono
					contattiProtocolla = rubSRV.getContattoFromMail(currentmail.getMittente(), utente.getIdAoo().intValue(), utente.getIdUfficio());

					if (!utente.getCheckUnivocitaMail()) {
						if (contattiProtocolla != null && contattiProtocolla.size() == 1) {
							contattoRifiuta = contattiProtocolla.get(0);
						} else if (contattiProtocolla != null && contattiProtocolla.size() > 1) {
							// mostra schermata di scelta tra i contatti
							inserisciContattoCasellaPostaleItem.setMailPec(currentmail.getMittente());
							scegliContattoProt = true;
							resetContattoProtocollaSelezionato();
							FacesHelper.executeJS(SHOW_SCEGLICONTATTOPROTOCOLLADLGWV_JS);
						} else {
							creaNuovoContatto();
						}
					} else {
						if (contattiProtocolla != null && !contattiProtocolla.isEmpty()) {
							// Se abbiamo più di un risultato significa che il db non è stato bonificato
							// bene
							contattoRifiuta = contattiProtocolla.get(0);
						} else {
							creaNuovoContatto();
						}
					}
				}
				if (contattoRifiuta != null) {
					TipologiaDestinatariEnum tipologiaDestinatario = null;
					if (!StringUtils.isNullOrEmpty(contattoRifiuta.getMailPec()) && contattoRifiuta.getMailPec().equals(currentmail.getMittente())) {
						tipologiaDestinatario = TipologiaDestinatariEnum.PEC;
					} else if (!StringUtils.isNullOrEmpty(contattoRifiuta.getMail()) && contattoRifiuta.getMail().equals(currentmail.getMittente())) {
						tipologiaDestinatario = TipologiaDestinatariEnum.PEO;
					} else {
						showError("Il contatto selezionato non corrisponde al mittente della mail.");
						return;
					}
					final List<DestinatarioEsternoDTO> contattiDestinatari = new ArrayList<>();
					contattiDestinatari.add(new DestinatarioEsternoDTO(contattoRifiuta.getContattoID(), tipologiaDestinatario, ModalitaDestinatarioEnum.TO));
					inserisciInCodaInoltroRifiuto(contattoRifiuta, contattiDestinatari, getTestoRifiutoProt());

					showInfoMessage("Rifiuto eseguito con successo. La mail sarà protocollata. La risposta inviata in firma al responsabile.");

					operazioneSelezionata = null;
					contattoRifiuta = null;
					idTestoRifiutoProt = null;
					testoRifiutoProt = "";

					refresh(true);
					FacesHelper.executeJS("PF('rifiutaProtDlg_WV').hide()");

				}

			}

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di rifiuto con protocollazione", e);
			showError(e);
		}
	}

	// ############ Inoltra con protocollo #########################

	/**
	 * Metodo che prepara all'inoltra con protocollo dell'e-mail.
	 * 
	 * @param comeFromTabMulti
	 */
	public void inoltraConProtocolloMng(final String comeFromTabMulti) {
		currentmail = new DetailEmailDTO();

		flagMultiInoltra = Boolean.valueOf(comeFromTabMulti);

		renderProtocolla = false;
		renderInoltra = false;
		renderRifiuta = false;
		renderInoltraProt = true;
		renderRifiutaProt = false;
		scegliContattoProt = false;
		contattiRubrica = new ArrayList<>();

		comboModalitaDestinatario = new ArrayList<>();
		comboModalitaDestinatario.add(ModalitaDestinatarioEnum.TO);
		comboModalitaDestinatario.add(ModalitaDestinatarioEnum.CC);

		// Si determina tramite il flag da dove è partita l'azione, Da azioni singole o
		// da tab azioni massive
		if (Boolean.TRUE.equals(flagMultiInoltra)) {
			currentmail = getMail(mailDTH.getSelectedMasters().get(0));
		} else {
			currentmail = mailDTH.getCurrentDetail();
		}

		operazioneSelezionata = MailOperazioniEnum.INOLTRO_CON_PROTOCOLLAZIONE;
		contattoInoltra = null;
		idTestoInoltroProt = null;
		testoInoltroProt = "";

		contattiToInoltraProt = new ArrayList<>();
		contattiCCInoltraProt = new ArrayList<>();

		// Inizialmente aggiungiamo un destinatario 'vuoto' nella tabella dei
		// destinatari
		destinatari = new ArrayList<>();
		aggiungiDestinatarioNew();
	}

	/**
	 * Imposta il testo di inoltro.
	 */
	public void impostaTestoInoltroProt() {
		if (idTestoInoltroProt != null) {
			for (final TestoPredefinitoDTO t : motiviInoltroConProtocollo) {
				if (t.getIdTestoPredefinito().intValue() == idTestoInoltroProt.intValue()) {
					setTestoInoltroProt(t.getCorpotesto());
					break;
				}
			}
		} else {
			setIdTestoInoltroProt(-1L);
			setTestoInoltroProt("");
		}
	}

	/**
	 * Gestisce la funzionalita di rifiuto con protocollazione validandone i
	 * parametri. Mostra eventualmente errori sulla forma.
	 */
	public void inoltraConProtocollo() {
		try {
			// Si determina tramite il flag da dove è partita l'azione, Da azioni singole o
			// da tab azioni massive
			if (Boolean.TRUE.equals(flagMultiInoltra)) {
				currentmail = getMail(mailDTH.getSelectedMasters().get(0));
			} else {
				currentmail = mailDTH.getCurrentDetail();
			}

			for (int i = 0; destinatari != null && i < destinatari.size(); i++) {
				// in caso di inoltra con destinatari null sostituisco con
				// destinatari 'vuoti' altrimenti distruggerebbero il datatable
				if (destinatari.get(i) == null) {
					destinatari.set(i, new DestinatarioRedDTO());
					destinatari.get(i).setContatto(new Contatto());
					destinatari.get(i).setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
					destinatari.get(i).getContatto().setIdDataTable(UUID.randomUUID().toString());
				}
			}
			if (destinatari == null || destinatari.isEmpty() || destinatari.get(0) == null || destinatari.get(0).getContatto() == null) {
				showError("Valorizzare almeno un destinatario fra quelli in 'A'");
				return;
			}
			boolean destToPresent = false;
			for (final DestinatarioRedDTO d : destinatari) {
				if (d != null && ModalitaDestinatarioEnum.TO.equals(d.getModalitaDestinatarioEnum())) {
					destToPresent = true;
					break;
				}
			}
			if (!destToPresent) {
				showError("Valorizzare almeno un destinatario fra quelli in 'A'");
				return;
			}

			if (StringUtils.isNullOrEmpty(getTestoInoltroProt())) {
				showError("Valorizzare il testo dell'inoltro.");
				return;
			}

			if (currentmail != null && checkCurrentMailInoltraRifiutaConProtocollo(currentmail)) {

				if (contattoInoltra == null || contattoInoltra.getContattoID() == null) {

					// recupera i contatti corrispondenti alla stringa mittente, se ci sono
					contattiProtocolla = rubSRV.getContattoFromMail(currentmail.getMittente(), utente.getIdAoo().intValue(), utente.getIdUfficio());

					if (!utente.getCheckUnivocitaMail()) {
						if (contattiProtocolla != null && contattiProtocolla.size() == 1) {

							contattoInoltra = contattiProtocolla.get(0);

						} else if (contattiProtocolla != null && contattiProtocolla.size() > 1) {

							// mostra schermata di scelta tra i contatti
							inserisciContattoCasellaPostaleItem.setMailPec(currentmail.getMittente());
							scegliContattoProt = true;
							resetContattoProtocollaSelezionato();
							FacesHelper.executeJS(SHOW_SCEGLICONTATTOPROTOCOLLADLGWV_JS);

						} else {
							creaNuovoContatto();
						}
					} else {
						// Se l'aoo prevede che l'univocità della mail e la lista risulta avere più di 1
						// risultato
						// significa che il db non è stato bonificato bene e in tal caso prendiamo
						// sempre il primo
						if (contattiProtocolla != null && !contattiProtocolla.isEmpty()) {
							contattoInoltra = contattiProtocolla.get(0);
						} else {
							creaNuovoContatto();
						}
					}
				}

				if (contattoInoltra != null) {

					// recupera destinatari dalla maschera
					final List<DestinatarioEsternoDTO> contattiDestinatari = new ArrayList<>();

					for (final DestinatarioRedDTO d : destinatari) {
						if (d != null && ModalitaDestinatarioEnum.TO.equals(d.getModalitaDestinatarioEnum())) {
							if (d.getContattiGruppo() != null) {
								for (final Contatto contattoGruppo : d.getContattiGruppo()) {
									contattiToInoltraProt.add(contattoGruppo);
								}
							} else {
								contattiToInoltraProt.add(d.getContatto());
							}
						}

						if (d != null && ModalitaDestinatarioEnum.CC.equals(d.getModalitaDestinatarioEnum())) {
							if (d.getContattiGruppo() != null) {
								for (final Contatto contattoGruppo : d.getContattiGruppo()) {
									contattiCCInoltraProt.add(contattoGruppo);
								}
							} else {
								contattiCCInoltraProt.add(d.getContatto());
							}
						}
					}

					getDestinatariInoltraConProtocollo(contattiDestinatari, contattiToInoltraProt, ModalitaDestinatarioEnum.TO);
					getDestinatariInoltraConProtocollo(contattiDestinatari, contattiCCInoltraProt, ModalitaDestinatarioEnum.CC);
					inserisciInCodaInoltroRifiuto(contattoInoltra, contattiDestinatari, getTestoInoltroProt());

					showInfoMessage("inoltraConProtocollo", "Inoltro eseguito con successo. La mail sarà protocollata. La risposta inviata in firma al responsabile.");

					operazioneSelezionata = null;
					contattoInoltra = null;
					idTestoInoltroProt = null;
					testoInoltroProt = "";

					refresh(true);
					FacesHelper.executeJS("PF('inoltraProtDlg_WV').hide()");
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di rifiuto con protocollazione", e);
			showError(e);
		}
	}

	private void creaNuovoContatto() {
		// crea nuovo contatto
		showInfoMessage("Non e' presente nessun contatto per il mittente. Inserisci un nuovo contatto.");
		inserisciContattoCasellaPostaleItem.setTipoPersona("F");
		inserisciContattoCasellaPostaleItem.setMailPec(currentmail.getMittente());
		visualizzaCreaContatto = true;
		FacesHelper.executeJS(SHOW_AGGIUNGICONTATTODLGWV_JS);
		FacesHelper.update(AGGIUNGICONTATTODLG_RESOURCE_PATH);
		callBackToAction = true;
	}

	/**
	 * @param contattiDestinatari
	 * @param contattiInoltraProt
	 * @param modalita
	 */
	private void getDestinatariInoltraConProtocollo(final List<DestinatarioEsternoDTO> contattiDestinatari, final List<Contatto> contattiInoltraProt,
			final ModalitaDestinatarioEnum modalita) {
		TipologiaDestinatariEnum tipologiaDestinatario = null;
		for (final Contatto contTo : contattiInoltraProt) {
			Collection<Contatto> contattiToScompattati = new ArrayList<>();
			if (contTo != null && TipoRubricaEnum.GRUPPOEMAIL.equals(contTo.getTipoRubricaEnum()) && contTo.getContattiContenutiInGruppo() != null
					&& !contTo.getContattiContenutiInGruppo().isEmpty()) {
				contattiToScompattati = contTo.getContattiContenutiInGruppo();
			} else {
				contattiToScompattati.add(contTo);
			}

			for (Contatto contatto : contattiToScompattati) {
				if (contatto != null && contatto.getContattoID() != null && contatto.getContattoID() != 0 && (contatto.getOnTheFly() == null || contatto.getOnTheFly() == 0)) {

					tipologiaDestinatario = TipologiaDestinatariEnum.PEO;
					if (!StringUtils.isNullOrEmpty(contatto.getMailPec())) {
						tipologiaDestinatario = TipologiaDestinatariEnum.PEC;
					}

					contattiDestinatari.add(new DestinatarioEsternoDTO(contatto.getContattoID(), tipologiaDestinatario, modalita));
				} else if (contatto != null && contatto.getOnTheFly() != null && contatto.getOnTheFly() == 1) {
					tipologiaDestinatario = TipologiaDestinatariEnum.getByNome(contatto.getTipologiaEmail().getTipologiaEmail());
					contatto = rubSRV.insertContattoOnTheFly(contatto, utente.getIdAoo());
					contattiDestinatari.add(new DestinatarioEsternoDTO(contatto.getContattoID(), tipologiaDestinatario, modalita));
				}

			}
		}
	}

	/**
	 * controllo stato della mail.
	 * 
	 * @param DetailEmailDTO email selezionata
	 * @return
	 */
	private boolean checkCurrentMailInoltraRifiutaConProtocollo(final DetailEmailDTO mail) {
		LOGGER.info("Controllo lo stato della mail e l'eventuale presa in carico della mail");
		if (mail != null) {
			try {
				if (protocollaMailSRV.existItemsToProcess(mail.getGuid(), utente)) {
					showError("Attenzione, non è possibile procedere con l’operazione perché la mail è già in attesa di essere elaborata per l'inoltro/rifiuto con firma.");
					return false;
				}

				final StatoMailEnum statoMail = mail.getStatoMail();
				final Integer currentIdUtenteProtocollatore = mail.getIdUtenteProtocollatore() == null ? 0 : mail.getIdUtenteProtocollatore();
				final StatoMailEnum currentStatoMail = statoMail;
				// Se la mail non è stata ancora presa in carico apri la maschera di
				// protocollazoione altrimenti, se non è "in fase di protocollazione",
				// viene chiesto all'utente se vuole sovrascrivere la presa in carico
				if (currentStatoMail.equals(StatoMailEnum.IN_FASE_DI_PROTOCOLLAZIONE_MANUALE) && currentIdUtenteProtocollatore > 0
						&& currentIdUtenteProtocollatore != utente.getId().longValue()) {
					showError(
							"Attenzione, non è possibile procedere con l’operazione perché la mail è in fase di protocollazione dall'utente " + currentIdUtenteProtocollatore);
				} else if ((currentStatoMail.equals(StatoMailEnum.INARRIVO) || currentStatoMail.equals(StatoMailEnum.INARRIVO_NON_PROTOCOLLABILE_AUTOMATICAMENTE))
						&& currentIdUtenteProtocollatore > 0 && currentIdUtenteProtocollatore != utente.getId().longValue()) {
					// La mail è già stata presa in carico dall' utente
					// "+currentIdUtenteProtocollatore+". Si desidera sbloccare la lavorazione?")
					if (MailOperazioniEnum.RIFIUTO_CON_PROTOCOLLAZIONE.equals(operazioneSelezionata)) {
						FacesHelper.update("eastSectionForm:giaInCaricoRifiutaDlg");
						FacesHelper.executeJS("PF('giaInCaricoRifiutaDlg_WV').show()");
					} else {
						FacesHelper.update("eastSectionForm:giaInCaricoInoltraDlg");
						FacesHelper.executeJS("PF('giaInCaricoInoltraDlg_WV').show()");
					}
					return false;
				} else if (currentStatoMail.equals(StatoMailEnum.INARRIVO) || currentStatoMail.equals(StatoMailEnum.INARRIVO_NON_PROTOCOLLABILE_AUTOMATICAMENTE)) {
					// Se nessun utente ha in carico la protocollazione della mail...
					return currentIdUtenteProtocollatore == utente.getId().intValue() || cambiaUtenteProtocollatore();

				} else if (currentStatoMail.equals(StatoMailEnum.IMPORTATA)) {
					showError("Attenzione, non è possibile procedere con l’operazione perché la mail è già stata protocollata");
					return false;
				} else if (currentIdUtenteProtocollatore != utente.getId().longValue()) {
					showError("Attenzione, non è possibile procedere con l’operazione");
					return false;
				}
			} catch (final Exception e) {
				LOGGER.error("Errore nel controllo della mail", e);
			}
		}

		return false;
	}

	/**
	 * @param mittente
	 * @param contattiDestinatari
	 * @param testo
	 */
	private void inserisciInCodaInoltroRifiuto(final Contatto mittente, final List<DestinatarioEsternoDTO> contattiDestinatari, final String testo) {
		protocollaMailSRV.inserisciInCoda(utente, operazioneSelezionata, currentmail.getGuid(), cpAttiva, mittente.getContattoID(), testo, contattiDestinatari);
	}

	/**
	 * Apertura rubrica per destinatari.
	 */
	public void openRubricaDestinatari() {
		try {
			final List<Contatto> contattiDaAggiornare = new ArrayList<>();

			destinatari.forEach(d -> {
				if (d.getTipologiaDestinatarioEnum() == TipologiaDestinatarioEnum.INTERNO) {
					d.getContatto().setTipoRubricaEnum(TipoRubricaEnum.INTERNO);
				}
				if (d.getContatto().getContattoID() != null) {
					contattiDaAggiornare.add(d.getContatto());
				}
			});
			final RubricaBean masterBean = FacesHelper.getManagedBean(MBean.RUBRICA_BEAN);
			sessionBean.setShowRubrica(true);
			masterBean.initRubrica(false);
			masterBean.setDestinatariRubrica(this, ModeRubricaEnum.SELEZIONE_MULTIPLA, contattiDaAggiornare, RubricaChiamanteEnum.MAILBEAN_DESTINATARI, true, true);
			masterBean.setMessaggioRubricaDaAltraPagina("Destinatari da aggiungere: ");
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());
		}
	}

	/**
	 * @see it.ibm.red.web.mbean.master.IRubricaHandler#restituisciRisultati(java.util.
	 *      Collection, boolean, it.ibm.red.business.enums.RubricaChiamanteEnum)
	 */
	@Override
	public boolean restituisciRisultati(final Collection<Contatto> collection, final boolean salva, final RubricaChiamanteEnum r) {
		if (salva) {
			destinatari = new ArrayList<>();
			// Creo i destinatari per i contatti scelti nella dialog della rubrica
			for (final Contatto c : collection) {
				if (StringUtils.isNullOrEmpty(c.getMailSelected())) {
					if (!StringUtils.isNullOrEmpty(c.getMailPec())) {
						c.setMailSelected(c.getMailPec());
					} else if (!StringUtils.isNullOrEmpty(c.getMail())) {
						c.setMailSelected(c.getMail());
					}
				}
				final DestinatarioRedDTO nuDest = new DestinatarioRedDTO();
				nuDest.setContatto(c);
				if (c.getTipoRubricaEnum().equals(TipoRubricaEnum.GRUPPO)) {
					final RicercaRubricaDTO ricercaContattiNelGruppo = new RicercaRubricaDTO();
					List<Contatto> contattiDaGruppo = null;
					ricercaContattiNelGruppo.setRicercaRED(true);
					ricercaContattiNelGruppo.setRicercaIPA(true);
					ricercaContattiNelGruppo.setRicercaMEF(true);
					ricercaContattiNelGruppo.setRicercaGruppo(false);
					ricercaContattiNelGruppo.setIdGruppo(c.getContattoID());
					ricercaContattiNelGruppo.setIdAoo(utente.getIdAoo().intValue());
					contattiDaGruppo = rubSRV.ricerca(utente.getIdUfficio(), ricercaContattiNelGruppo);
					nuDest.setContattiGruppo(contattiDaGruppo);
				}
				nuDest.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
				nuDest.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
				nuDest.getContatto().setIdDataTable(UUID.randomUUID().toString());
				destinatari.add(nuDest);
			}
			// Aggiungo i destinatari scelti nella dialog nella tabella dei destinatari in
			// inoltra

		}
		// Chiudo la dialog della rubrica
		FacesHelper.executeJS("PF('wdgSelezionaDaRubrica').hide()");
		return true;
	}

	/**
	 * @see it.ibm.red.web.mbean.master.IRubricaHandler#aggiornaComponentiInterfaccia(it.
	 *      ibm.red.business.enums.RubricaChiamanteEnum)
	 */
	@Override
	public void aggiornaComponentiInterfaccia(final RubricaChiamanteEnum r) {
		if (renderInoltra) {
			FacesHelper.update("eastSectionForm:contattiA_inoltra");
		}
		if (renderInoltraProt) {
			FacesHelper.update("eastSectionForm:contattiA_inoltra_prot");
		}
	}

	/**
	 * Aggiunge il contatto selezionato dalla tabella nella tabella dei destinatari
	 * dell'inoltra mail.
	 */
	public void aggiungiDestinatarioInoltra(final ActionEvent event) {
		final Contatto c = (Contatto) getDataTableClickedRow(event);

		// controlla se il destinatario è già presente nella lista
		for (final DestinatarioRedDTO d : destinatari) {
			if (c.getContattoID() != null && d.getContatto() != null && c.getContattoID().equals(d.getContatto().getContattoID())) {
				showWarnMessage("Il contatto è già stato inserito tra i destinatari!");
				return;
			}
		}
		final DestinatarioRedDTO dest = creaDestinatarioInoltra(c);
		destinatari.add(dest);
		aggiungiDestinatarioNew();
	}

	/**
	 * Crea il destinatario contenente il contatto c (inoltra mail).
	 * 
	 * @param c - contatto da inserire
	 * @return nuovoDestinatario
	 */
	private DestinatarioRedDTO creaDestinatarioInoltra(final Contatto c) {
		final DestinatarioRedDTO nuovoDestinatario = new DestinatarioRedDTO();
		nuovoDestinatario.setContatto(c);
		if (c != null && TipoRubricaEnum.GRUPPO.equals(c.getTipoRubricaEnum())) {
			final List<Contatto> contattiGruppo = RubricaComponent.ricercaContattiNelGruppo(c, utente.getIdUfficio());
			if (contattiGruppo != null && !contattiGruppo.isEmpty()) {
				nuovoDestinatario.setContattiGruppo(contattiGruppo);
			}
		}
		nuovoDestinatario.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
		nuovoDestinatario.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
		nuovoDestinatario.getContatto().setIdDataTable(UUID.randomUUID().toString());
		return nuovoDestinatario;
	}

	/**
	 * Completa la procedura di protocollazione.
	 */
	public void continuaProtocol() {
		try {
			if (checkCurrentMail(currentmail)) {
				if (!StringUtils.isNullOrEmpty(currentmail.getMittente()) && (contattoProtocolla == null
						|| (contattoProtocolla.getContattoID() == null && (contattoProtocolla.getOnTheFly() == null || contattoProtocolla.getOnTheFly().intValue() == 0)))) {
					final String mittente = currentmail.getMittente();

					if (!utente.getCheckUnivocitaMail()) {
						mittenteMailProt = mittente;
						// Recupera i contatti corrispondenti alla stringa mittente, se ci sono
						contattiProtocolla = rubSRV.getContattoFromMail(mittente, utente.getIdAoo().intValue(), utente.getIdUfficio()); // recupera i contatti corrispondenti
																																		// alla stringa mittente, se ci sono
						if (contattiProtocolla != null && contattiProtocolla.size() == 1) {
							contattoProtocolla = contattiProtocolla.get(0);
						} else if (contattiProtocolla != null && contattiProtocolla.size() > 1) {
							// mostra schermata di scelta tra i contatti
							contattiPreferiti = rubSRV.getPreferiti(utente.getIdUfficio(), utente.getIdAoo(), false);
							inserisciContattoItem.setMail(mittente);
							scegliContattoProt = true;
							resetContattoProtocollaSelezionato();
							FacesHelper.executeJS(SHOW_SCEGLICONTATTOPROTOCOLLADLGWV_JS);

						} else {
							// crea nuovo contatto
							showMascheraCreazioneContatto(mittente);
						}
					} else {
						contattiProtocolla = rubSRV.getContattoFromMail(mittente, utente.getIdAoo().intValue(), utente.getIdUfficio()); // recupera i contatti corrispondenti
						// alla stringa mittente, se ci sono
						if(contattiProtocolla != null && !contattiProtocolla.isEmpty()) {
							contattoProtocolla = contattiProtocolla.get(0); //Prendo il primo , in realtà se questa query ritorna più risultati significa che il db non è stato bonificato bene 
						} else if(!MailActionEnum.PROTOCOLLA_DSR.equals(this.action)){
							contattoProtocolla = new Contatto();
							contattoProtocolla.setMailPec(mittente);
							contattoProtocolla.setMailSelected(mittente);
							contattoProtocolla.setAliasContatto("");
							contattoProtocolla.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEC);
							contattoProtocolla.setOnTheFly(ON_THE_FLY);
							dmb.setCreaORichiediCreazioneFlag(true);

						}else {
							// crea nuovo contatto
							showMascheraCreazioneContatto(mittente);
						}
						dmb.setGestioneOnTheFlyDaProtocolla(true);

					}
				}
				// Potrebbe essere stato valorizzato sopra oppure da un altro metodo
				if (contattoProtocolla != null
						&& ((contattoProtocolla.getContattoID() == null && (contattoProtocolla.getOnTheFly() != null && contattoProtocolla.getOnTheFly().intValue() != 0))
								|| contattoProtocolla.getContattoID() != null)) {
					switch (this.action) {
					case PROTOCOLLA:
						impostaDettagliProtocolla(contattoProtocolla);
						break;
					case PROTOCOLLA_DSR:
						if (fepaSRV.validateProtocollazioneDSR(utente, treeTableComponent.getActualPrincipal().getNodeId())) {
							impostaDettagliProtocollaDSR(contattoProtocolla);
						} else {
							showError("Impossibile proseguire con la protocollazione. Il documento principale deve essere firmato "
									+ "e contenere un campo libero per ulteriore firma. Si prega di contattare l'assistenza");
						}
						break;
					default:
						LOGGER.error("Azione non supportata");
						showError("Azione non supportata");
						break;
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di protocollazione mail.", e);
			showError(e);
		} finally {
			contattoProtocolla = null;
		}
	}

	private void showMascheraCreazioneContatto(final String mittente) {
		showInfoMessage("Non e' presente nessun contatto per questa mail. Inserisci un nuovo contatto.");
		inserisciContattoCasellaPostaleItem.setTipoPersona("F");
		inserisciContattoCasellaPostaleItem.setMailPec(mittente);
		visualizzaCreaContatto = true;
		FacesHelper.executeJS(SHOW_AGGIUNGICONTATTODLGWV_JS);
		FacesHelper.update(AGGIUNGICONTATTODLG_RESOURCE_PATH);
		callBackToAction = true;
	}

	/**
	 * Imposta {@link #contattoProtocolla} e se possibile prosegue con la procedura
	 * di protocollazione.
	 */
	public void impostaContattoeProcedi() {
		if (contattoProtocollaSelezionato == null) {
			showWarnMessage("Selezionare un contatto");
		} else {
			if (operazioneSelezionata == null) {
				contattoProtocolla = contattoProtocollaSelezionato;
				setIdContattoScelto(null);
				FacesHelper.executeJS(HIDE_SCEGLICONTATTOPROTOCOLLADLGWV_JS);
				setRenderProtocol(true);
				continuaProtocol();
			} else if (operazioneSelezionata.equals(MailOperazioniEnum.RIFIUTO_CON_PROTOCOLLAZIONE)) {
				contattoRifiuta = contattoProtocollaSelezionato;
				setIdContattoScelto(null);
				setScegliContattoProt(false);
				FacesHelper.executeJS(HIDE_SCEGLICONTATTOPROTOCOLLADLGWV_JS);
				rifiutaConProtocollo();
			} else if (operazioneSelezionata.equals(MailOperazioniEnum.INOLTRO_CON_PROTOCOLLAZIONE)) {
				contattoInoltra = contattoProtocollaSelezionato;
				setIdContattoScelto(null);
				setScegliContattoProt(false);
				FacesHelper.executeJS(HIDE_SCEGLICONTATTOPROTOCOLLADLGWV_JS);
				inoltraConProtocollo();
			}
		}
	}

	/**
	 * Mostra la dialog per la creazione o l'aggiunta di un contatto.
	 */
	public void vaiSuAggiungiContatto() {
		visualizzaCreaContatto = true;
		FacesHelper.executeJS(SHOW_AGGIUNGICONTATTODLGWV_JS);
		callBackToAction = true;
	}

	/**
	 * Metodo per settare i dettagli dei documenti.
	 */
	private void impostaDettagliProtocolla(final Contatto contact) {

		try {
			LOGGER.info("#### ALLEGATI MAIL impostaDettagliProtocolla START####");
			fillDetailsProt(contact);
			// setto prima il chiamante cosi non fa il togglecenter
			dmb.setChiamante(ConstantsWeb.MBean.MAIL_BEAN);
			dmb.setDetail(detailsProt);
			dmb.setAssegnatariPerConoscenza(new ArrayList<>());
			dmb.setAssegnatariPerContributo(new ArrayList<>());
			dmb.setAssegnatarioPrincipale(null);
			dmb.setDescrAssegnatarioPerCompetenza(null);
			LOGGER.info("####ALLEGATI MAIL impostaDettagliProtocolla END####");
			if (currentmail.getPreassegnatarioInterop() != null
					&& (currentmail.getPreassegnatarioInterop().getIdUfficio() != null || currentmail.getPreassegnatarioInterop().getDescrizioneUfficio() != null)) {
				dmb.setAssegnatarioPrincipale(currentmail.getPreassegnatarioInterop().getAssegnazioneCompetenza());
				dmb.setDescrAssegnatarioPerCompetenza(dmb.getAssegnatarioPrincipale().getDescrizioneAssegnatario());
				if (currentmail.getPreassegnatarioInterop().getIdUfficio() != null && utente.isRibaltaTitolario()) {

					final boolean preseleziona = documentManagerSRV.preselezionaRibaltaTitolario(currentmail.getPreassegnatarioInterop().getIdUfficio(), utente.getIdAoo());
					dmb.getDetail().setRibaltaTitolario(preseleziona);
				}
			}

			setRenderProtocol(false);
			FacesHelper.executeJS(WRAP_ADD_EVENT_LISTENER_FOR_PROT_MAIL);
		} catch (final Exception e) {
			LOGGER.error(GENERIC_ERROR_IMPOSTAZIONE_DETTAGLI_MSG, e);
			throw new RedException(GENERIC_ERROR_IMPOSTAZIONE_DETTAGLI_MSG);
		}
	}

	/**
	 * Popola il dettaglio della protocollazione da passare al document Manager bean
	 * con tutto il necessario.
	 * 
	 * Sarebbe meglio questo dato dentro un service che si occupa di popolare il
	 * detail.
	 * 
	 */
	private void fillDetailsProt(final Contatto contact) {
		detailsProt = new DetailDocumentRedDTO();
		final List<HierarchicalFileWrapperDTO> fileNonSbustati = new ArrayList<>();
		getPadreFromFigli(treeTableComponent.getActualPrincipal(), treeTableComponent.getActualAllegati(), fileNonSbustati, treeTableComponent.getRoot());

		final List<AllegatoDTO> allegatiNonSbustati = scompattaMailSRV.transformToAllegatoDTO(utente, fileNonSbustati);
		for (final AllegatoDTO allegatoNonSbustato : allegatiNonSbustati) {
			allegatoNonSbustato.setIdTipologiaDocumento(22); // Tipologia Generica
			allegatoNonSbustato.setFormatoOriginale(true);
		}
		detailsProt.setAllegatiNonSbustati(allegatiNonSbustati);

		final List<AllegatoDTO> allegatiSelezionatiDati = scompattaMailSRV.transformToAllegatoDTO(utente, treeTableComponent.getActualAllegati());
		detailsProt.setAllegati(allegatiSelezionatiDati);

		final HierarchicalFileWrapperDTO file = treeTableComponent.getActualPrincipal();
		if (file != null) {
			detailsProt.setIdFilePrincipaleDaProtocolla(file.getNodeId());
			detailsProt.setNomeFile(file.getFile().getName());
			detailsProt.setMimeType(file.getFile().getTipoFile().getMimeType());
			final byte[] contentPrinc = scompattaMailSRV.getContentutoDiscompattataMail(utente, file.getNodeId());
			detailsProt.setContent(contentPrinc);
		}
		detailsProt.setProtocollazioneMailGuid(currentmail.getGuid());
		detailsProt.setProtocollazioneMailAccount(cpAttiva);

		if (!StringUtils.isNullOrEmpty(contact.getMailPec())) {
			contact.setMailSelected(contact.getMailPec());
			contact.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEC);
		} else if (!StringUtils.isNullOrEmpty(contact.getMail())) {
			contact.setMailSelected(contact.getMail());
			contact.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEO);
		}
		// Mi serve per capire se in fase di protocollazione devo creare l'on the fly o
		// no
		if (!StringUtils.isNullOrEmpty(contact.getAliasContatto())) {
			contact.setAliasContattoProtocollazione(contact.getAliasContatto());
		}
		if (contact.getOnTheFly() != null && contact.getOnTheFly().equals(ON_THE_FLY)) {
			detailsProt.setAbilitaIlContattoOnTheFlyDaProt(true);
			detailsProt.setMittenteContattoFromMailOnTheFly(contact);
		} else {
			detailsProt.setMittenteContatto(contact);
		}

		detailsProt.setMittenteMailProt(mittenteMailProt);
		detailsProt.setIsNotifica(notificaProtocolla);
		detailsProt.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0]); // 1
		detailsProt.setOggetto(currentmail.getOggetto());

		final NotaDTO notaMail = currentmail.getNota();
		detailsProt.setContentNoteDaMailProtocollaDTO(notaMail);

		final HierarchicalFileWrapperDTO principal = treeTableComponent.getActualPrincipal();
		detailsProt.setIdFilePrincipaleDaProtocolla(principal.getNodeId());
		detailsProt.setNomeFile(principal.getFile().getName());
		detailsProt.setMimeType(principal.getFile().getTipoFile().getMimeType());
		detailsProt.setProtocollaEMantieni(protocollaEMantieni);

		detailsProt.setFromProtocollaMail(true);

		detailsProt.setDataScarico(DateUtils.parseDate(currentmail.getDataScarico()));
	}

	private void getPadreFromFigli(final HierarchicalFileWrapperDTO filePrincipale, final List<HierarchicalFileWrapperDTO> allegati,
			final List<HierarchicalFileWrapperDTO> fileNonSbustati, final TreeNode node) {
		final List<HierarchicalFileWrapperDTO> principaleEAllegati = new ArrayList<>();
		final Set<HierarchicalFileWrapperDTO> listAllegatiNuova = new HashSet<>();

		if(filePrincipale!=null) {
			principaleEAllegati.add(filePrincipale);
		}
		if(allegati!=null && !allegati.isEmpty()) {
			principaleEAllegati.addAll(allegati);
		}
		
		if(utente.getFileNonSbustato()) {
			for(final HierarchicalFileWrapperDTO prinEAll : principaleEAllegati) {
				final List<TreeNode> figli = node.getChildren();
				for(final TreeNode figlio : figli) {
					final ListElementDTO<?> figlioGenerico = (ListElementDTO<?>)figlio.getData();
					final HierarchicalFileWrapperDTO nodo = (HierarchicalFileWrapperDTO)figlioGenerico.getData();
					if(prinEAll.getNodeId().startsWith(nodo.getNodeId()) && nodo.getArchivio()) {
						nodo.setFileNonSbustato(true);
						listAllegatiNuova.add(nodo);

					}
				}
			}
			fileNonSbustati.addAll(listAllegatiNuova);
		}
	}

	private void impostaDettagliProtocollaDSR(final Contatto contact) {
		final Collection<HierarchicalFileWrapperDTO> selectedAllegati = treeTableComponent.getActualAllegati();
		final HierarchicalFileWrapperDTO principale = treeTableComponent.getActualPrincipal();
		try {
			if (principale != null) {
				final DsrManagerBean dsrManagerBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DSR_MANAGER_BEAN);
				dsrManagerBean.initialzeDetailFromMail(utente, contact, selectedAllegati, principale, notificaProtocolla, currentmail.getGuid(), currentmail.getOggetto());
				setRenderProtocol(false);
				FacesHelper.executeJS("hidePreviewProtocollaDsr();");
			} else {
				LOGGER.error("Errore durante l'impostazione dei dettagli (contenuto allegati): ");
				throw new RedException(GENERIC_ERROR_IMPOSTAZIONE_DETTAGLI_MSG);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'impostazione dei dettagli (contenuto allegati)", e);
			throw new RedException(GENERIC_ERROR_IMPOSTAZIONE_DETTAGLI_MSG);
		}
	}

	/**
	 * Metodo per lo sblocco della mail.
	 */
	public void sbloccaMail() {
		currentmail = mailDTH.getCurrentDetail();
		if (cambiaUtenteProtocollatore(0L)) {
			showInfoMessage("Sblocco della mail avvenuta con successo.");
		}
		refresh(false, false, false);
	}

	/**
	 * Controllo stato della mail.
	 * 
	 * @param DetailEmailDTO email selezionata
	 * @return
	 */
	private boolean checkCurrentMail(final DetailEmailDTO mail) {
		LOGGER.info("Controllo lo stato della mail e l'eventuale presa in carico della mail");
		boolean checkOk = false;
		postCheckMailAction = null;

		if (mail != null) {
			try {
				final Integer currentIdUtenteProtocollatore = mail.getIdUtenteProtocollatore() == null ? 0 : mail.getIdUtenteProtocollatore();
				final StatoMailEnum currentStatoMail = mail.getStatoMail();

				// Se la mail non è stata ancora presa in carico apri la maschera di
				// protocollazione...
				// ...altrimenti, se non è "in fase di protocollazione", viene chiesto
				// all'utente se vuole sovrascrivere la presa in carico
				if (currentStatoMail.equals(StatoMailEnum.IN_FASE_DI_PROTOCOLLAZIONE_MANUALE) && currentIdUtenteProtocollatore > 0
						&& currentIdUtenteProtocollatore != utente.getId().longValue()) {

					showError(
							"Attenzione, non è possibile procedere con l'operazione perché la mail è in fase di protocollazione dall'utente " + currentIdUtenteProtocollatore);

				} else if ((currentStatoMail.equals(StatoMailEnum.INARRIVO) || currentStatoMail.equals(StatoMailEnum.INARRIVO_NON_PROTOCOLLABILE_AUTOMATICAMENTE))
						&& currentIdUtenteProtocollatore > 0 && currentIdUtenteProtocollatore != utente.getId().longValue()) {

					// La mail è già stata presa in carico dall'utente " +
					// currentIdUtenteProtocollatore + ". Si desidera sbloccare la lavorazione?")
					postCheckMailAction = PostCheckMailActionEnum.GIA_IN_CARICO_DLG;
					checkOk = true;

				} else if (currentStatoMail.equals(StatoMailEnum.INARRIVO) || currentStatoMail.equals(StatoMailEnum.INARRIVO_NON_PROTOCOLLABILE_AUTOMATICAMENTE)) {

					// Se nessun utente ha in carico la protocollazione della mail...
					if (currentIdUtenteProtocollatore != utente.getId().intValue() && !cambiaUtenteProtocollatore()) {
						checkOk = false;
					} else {
						if (contattoProtocolla != null) {
							postCheckMailAction = PostCheckMailActionEnum.PROTOCOLLA_DLG;
						}

						checkOk = true;
					}

				} else if (currentStatoMail.equals(StatoMailEnum.IMPORTATA)) {
					showError("Attenzione, non è possibile procedere con l'operazione perché la mail è già stata protocollata");
					setRenderProtocol(true);

				} else if (currentIdUtenteProtocollatore != utente.getId().longValue()) {
					showError("Attenzione, non è possibile procedere con l'operazione");
					setRenderProtocol(true);
				}
			} catch (final Exception e) {
				LOGGER.error("Errore nel controllo della mail", e);
			}
		}

		return checkOk;
	}

	/**
	 * Determina la dialog da aprire in base all'esito del metodo
	 * "checkCurrentMail".
	 */
	private void gestisciAzionePostCheckMail() {
		if (PostCheckMailActionEnum.PROTOCOLLA_DLG.equals(postCheckMailAction)) {
			FacesHelper.executeJS(SHOW_PROTOCOLLAMAILWV_JS);
			FacesHelper.executeJS("PF('protocollaMail_WV').toggleMaximize()");

		} else if (PostCheckMailActionEnum.GIA_IN_CARICO_DLG.equals(postCheckMailAction)) {
			FacesHelper.update("eastSectionForm:giaInCaricoDlg");
			FacesHelper.executeJS("PF('giaInCaricoDlg_WV').show()");
		}
	}

	/**
	 * {@link #reimpostaDialog(DocumentTypeEnum)}.
	 */
	public void reimpostaDialog() {
		reimpostaDialog(DocumentTypeEnum.PROTOCOLLA_MAIL);
	}

	/**
	 * Reinizializza i parametri della dialog impostando il dettaglio per il tipo
	 * documento.
	 * 
	 * @param docType
	 */
	public void reimpostaDialog(final DocumentTypeEnum docType) {
		renderProtocol = true;
		idContattoScelto = null;
		filePrincipale = null;
		idFilePrincipale = null;
		callBackToAction = false;
		treeTableComponent = null;
		postCheckMailAction = null;
		renderProtocolla = true;
		renderInoltra = false;
		renderRifiuta = false;
		renderInoltraProt = false;
		renderRifiutaProt = false;
		scegliContattoProt = false;
		contattoProtocolla = new Contatto();
		
		initFlagRicercaContatto();
		
		contattiProtocolla = new ArrayList<>();

		previewComponent.setDetailPreview(null, docType);
	}
	
	/**
	 * Reinizializza i flag comuni della ricerca.
	 */
	private void initFlagRicercaContatto() {
		inserisciContattoCasellaPostaleItem = new Contatto();
		inserisciContattoItem = new Contatto();
		
		contattiPreferiti = new ArrayList<>();
		resultRicercaContatti = new ArrayList<>();
		contattiPreferitiFiltered = new ArrayList<>();

		resultRicercaContattiFiltered = new ArrayList<>();
		destinatari = new ArrayList<>();

		ricercaRubricaDTO = new RicercaRubricaDTO();
		ricercaRubricaDTO.setRicercaRED(true);
		ricercaRubricaDTO.setRicercaRED(true);
		ricercaRubricaDTO.setRicercaIPA(true);
		ricercaRubricaDTO.setRicercaMEF(true);
		ricercaRubricaDTO.setIsUfficio(true);
	}

	/**
	 * metodo per il cambio dell'utente protocollatore e/o lo sblocco dell mail.
	 * 
	 * @return
	 */
	public boolean cambiaUtenteProtocollatore() {
		return cambiaUtenteProtocollatore(utente.getId());
	}

	private boolean cambiaUtenteProtocollatore(final Long idUtenteProtocollatore) {
		// Imposto il nuovo utente protocollatore
		boolean output = false;

		try {
			mailSRV.aggiornaUtenteProtocollatoreMail(currentmail.getGuid(), idUtenteProtocollatore, utente.getFcDTO(), utente.getIdAoo());
			currentmail.setIdUtenteProtocollatore(idUtenteProtocollatore.intValue());
			currentmail.setDescUtenteProtocollatore(utente.getNome() + " " + utente.getCognome());
			mailDTH.refresh();

			if (operazioneSelezionata != null && operazioneSelezionata.equals(MailOperazioniEnum.INOLTRO_CON_PROTOCOLLAZIONE)) {

				if (Boolean.TRUE.equals(flagMultiInoltra)) {
					mailDTH.getSelectedMasters().get(0).setDescUtenteProtocollatore(utente.getNome() + " " + utente.getCognome());
					mailDTH.getSelectedMasters().get(0).setIdUtenteProtocollatore(utente.getId().intValue());
				} else {
					mailDTH.getCurrentMaster().setDescUtenteProtocollatore(utente.getNome() + " " + utente.getCognome());
					mailDTH.getCurrentMaster().setIdUtenteProtocollatore(utente.getId().intValue());
				}

			} else if (operazioneSelezionata != null && operazioneSelezionata.equals(MailOperazioniEnum.RIFIUTO_CON_PROTOCOLLAZIONE)) {

				if (Boolean.TRUE.equals(flagMultiRifiuta)) {
					mailDTH.getSelectedMasters().get(0).setDescUtenteProtocollatore(utente.getNome() + " " + utente.getCognome());
					mailDTH.getSelectedMasters().get(0).setIdUtenteProtocollatore(utente.getId().intValue());
				} else {
					mailDTH.getCurrentMaster().setDescUtenteProtocollatore(utente.getNome() + " " + utente.getCognome());
					mailDTH.getCurrentMaster().setIdUtenteProtocollatore(utente.getId().intValue());
				}
			}

			FacesHelper.update(CENTRAL_SECTION_FORM);

			output = true;
		} catch (final Exception e) {
			LOGGER.error("Impossibile cambiare l'utente protocollatore.", e);
			showError("Si è verificato un errore nel cambio dell'utente protocollatore");
		}

		return output;
	}

	/**
	 * Gestisce il cambio dell'utente protocollatore.
	 * {@link #cambiaUtenteProtocollatore()}.
	 * 
	 * @param operazione
	 */
	public void cambiaUtenteProtocollatore(final MailOperazioniEnum operazione) {
		// Imposto il nuovo utente protocollatore
		cambiaUtenteProtocollatore();
		if (operazione.equals(MailOperazioniEnum.RIFIUTO_CON_PROTOCOLLAZIONE)) {
			rifiutaConProtocollo();
		} else if (operazione.equals(MailOperazioniEnum.INOLTRO_CON_PROTOCOLLAZIONE)) {
			inoltraConProtocollo();
		} else {
			showError("Operazione non consentita! Si prega di contattare l'assistenza");
		}
	}

	private void prendiAllegati(final DetailEmailDTO mailSelected) {
		final String guidMail = mailSelected.getGuid();

		try {
			final String nomeIndirizzoEmail = cartellaMailSelected.getCasellaMail();

			// ### Si costruisce l'alberatura degli allegati della mail
			treeTableComponent = new SelezionaPrincipaleAllegatoComponent(guidMail, utente, nomeIndirizzoEmail);
			
			showWarnMessageMaxSize = treeTableComponent.isShowWarnMessageMaxSize();
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero degli allegati: " + e.getMessage(), e);
			throw new RedException("Errore durante il recupero degli allegati, si prega di contattare l'assistenza", e);
		}
	}

	private void protocollazioneGenerica(final DetailEmailDTO mailSelected) {
		if (checkCurrentMail(mailSelected)) {
			prendiAllegati(mailSelected);
		} else {
			throw new RedException("Errore in fase di controllo della mail");
		}
	}

	/**
	 * Gestisce la fase iniziale della protocollazione.
	 * 
	 * @param comeFromTabMulti
	 */
	public void clickOnProtocollaButton(final String comeFromTabMulti) {

		try {
			reimpostaDialog();

			currentmail = new DetailEmailDTO();

			final Boolean flagMultiProtocolla = Boolean.valueOf(comeFromTabMulti);

			// Si determina tramite il flag da dove è partita l'azione, Da azioni singole o
			// da tab azioni massive
			if (Boolean.TRUE.equals(flagMultiProtocolla)) {
				currentmail = getMail(mailDTH.getSelectedMasters().get(0));

				if (currentmail == null) {
					LOGGER.error(ERRORE_RISCONTRATO_CURRENT_MAIL_NON_VALORIZZATA);
					throw new RedException(ERRORE_RISCONTRATO_CURRENT_MAIL_NON_VALORIZZATA);
				}

				// Setto l'esito della validazione e il preassegnatario dal master al detail
				if (mailDTH.getSelectedMasters().get(0).getEsitoValidazioneEnum() != null && currentmail != null) {
					currentmail.setEsitoValidazioneInterop(mailDTH.getSelectedMasters().get(0).getEsitoValidazioneEnum().getNome());
				}
				// PREASSEGNATARIO PER COMPETENZA
				if (mailDTH.getSelectedMasters().get(0).getPreassegnatarioInterop() != null && currentmail != null) {
					currentmail.setPreassegnatarioInterop(mailDTH.getSelectedMasters().get(0).getPreassegnatarioInterop());
				}

			} else {
				currentmail = getMail(mailDTH.getCurrentMaster());

				if (currentmail == null) {
					LOGGER.error(ERRORE_RISCONTRATO_CURRENT_MAIL_NON_VALORIZZATA);
					throw new RedException(ERRORE_RISCONTRATO_CURRENT_MAIL_NON_VALORIZZATA);
				}

				if (mailDTH.getCurrentMaster().getEsitoValidazioneEnum() != null) {
					currentmail.setEsitoValidazioneInterop(mailDTH.getCurrentMaster().getEsitoValidazioneEnum().getNome());
				}
				// PREASSEGNATARIO PER COMPETENZA
				if (mailDTH.getCurrentMaster().getPreassegnatarioInterop() != null) {
					currentmail.setPreassegnatarioInterop(mailDTH.getCurrentMaster().getPreassegnatarioInterop());
				}

			}

			action = MailActionEnum.PROTOCOLLA;

			final boolean semiAutomatica = utente.getTipoPosta().equals(PostaEnum.POSTA_ESTERNA_INTEROPERABILE_AUTOMATICA)
					&& EsitoValidazioneSegnaturaInteropEnum.VALIDA.getNome().equals(currentmail.getEsitoValidazioneInterop());

			boolean contentPrincipalePDFProtetto = false;
			final String guidMail = currentmail.getGuid();
			SegnaturaMessaggioInteropDTO segnaturaDTO = null;
			List<AllegatoDTO> allegatiMailInterop = null;
			if (semiAutomatica) {
				segnaturaDTO = mailSRV.getSegnaturaFromGUID(utente.getFcDTO(), guidMail, utente.getIdAoo());
				allegatiMailInterop = mailSRV.getDocPrincEAllegatiMailInterop(currentmail.getGuid(), utente.getFcDTO(), utente.getIdAoo());
				if (segnaturaDTO.getIdDocumentoPrincipale() != null && segnaturaDTO.getIdDocumentoPrincipale().toLowerCase().endsWith("pdf") && allegatiMailInterop != null) {
					for (final AllegatoDTO allegato : allegatiMailInterop) {
						if (segnaturaDTO.getIdDocumentoPrincipale().equals(allegato.getNomeFile())) {
							contentPrincipalePDFProtetto = !checkValidaPDF(allegato);
							break;
						}
					}
				}
			}

			if (semiAutomatica && !contentPrincipalePDFProtetto) {
				protocollazioneSemiautomatica(currentmail, segnaturaDTO, allegatiMailInterop);
				inizializzaNotaProtocolla(FALSE);
			} else {

				// Se contentPrincipalePDFProtetto allora warning in maschera
				if (contentPrincipalePDFProtetto) {
					showWarnMessage("Non è possibile protocollare in modo semi-automatico poichè il documento principale risulta essere protetto");
				}
				protocollazioneGenerica(currentmail);
				inizializzaNotaProtocolla(FALSE);
			}
			protocollaEMantieni = false;
			// Viene fatto in questo punto in modo da essere evitare l'apertura delle dialog
			// in caso di errori successivi al "checkCurrentMail"
			gestisciAzionePostCheckMail();
		} catch (final RedException e) {
			LOGGER.error(e.getMessage(), e);
			showError(e.getMessage());
		} catch (final Exception e) {
			LOGGER.error(ERROR_ESECUZIONE_RICHIESTA_MSG, e);
			showError(ERROR_ESECUZIONE_RICHIESTA_MSG);
		}
	}

	/**
	 * Gestisce l'ispezione mail.
	 * 
	 * @param comeFromTabMulti
	 */
	public void clickOnIspezionaMail(final String comeFromTabMulti) {
		try {
			reimpostaDialog(DocumentTypeEnum.ISPEZIONA_MAIL);
			// prendi mail
			currentmail = new DetailEmailDTO();

			final Boolean flagMultiProtocolla = Boolean.valueOf(comeFromTabMulti);
			if (Boolean.TRUE.equals(flagMultiProtocolla)) {
				currentmail = getMail(mailDTH.getSelectedMasters().get(0));
			} else {
				currentmail = getMail(mailDTH.getCurrentMaster());
			}

			// setta action per response
			action = MailActionEnum.ISPEZIONA_MAIL;

			// recupera documenti mail
			prendiAllegati(currentmail);

			// impostazioni per sezione nota
			inizializzaNotaIspeziona(FALSE);

			FacesHelper.executeJS(SHOW_PROTOCOLLAMAILWV_JS);
		} catch (final RedException e) {
			LOGGER.error(e.getMessage(), e);
			showError(e.getMessage());
		} catch (final Exception e) {
			LOGGER.error(ERROR_ESECUZIONE_RICHIESTA_MSG, e);
			showError(ERROR_ESECUZIONE_RICHIESTA_MSG);
		}

	}

	/**
	 * Gestisce la protocollazione DSR.
	 * 
	 * @param comeFromTabMulti
	 */
	public void clickOnProtocollaDsrButton(final String comeFromTabMulti) {
		try {
			reimpostaDialog();
			currentmail = new DetailEmailDTO();
			final Boolean flagMultiProtocolla = Boolean.valueOf(comeFromTabMulti);

			// Si determina tramite il flag da dove è partita l'azione, Da azioni singole o
			// da tab azioni massive
			if (Boolean.TRUE.equals(flagMultiProtocolla)) {
				currentmail = getMail(mailDTH.getSelectedMasters().get(0));
			} else {
				currentmail = getMail(mailDTH.getCurrentMaster());
			}

			action = MailActionEnum.PROTOCOLLA_DSR;
			protocollazioneGenerica(currentmail);

			// Viene fatto in questo punto in modo da essere evitare l'apertura delle dialog
			// in caso di errori successivi al "checkCurrentMail"
			gestisciAzionePostCheckMail();
		} catch (final RedException e) {
			LOGGER.error(e.getMessage(), e);
			showError(e.getMessage());
		} catch (final Exception e) {
			LOGGER.error(ERROR_ESECUZIONE_RICHIESTA_MSG, e);
			showError(ERROR_ESECUZIONE_RICHIESTA_MSG);
		}
	}

	/**
	 * Aggiunge un destinatario 'vuoto' nella tabella destinatari dell'inoltro mail.
	 */
	public void aggiungiDestinatario() {
		final DestinatarioRedDTO destinatario = new DestinatarioRedDTO();
		destinatario.setContatto(new Contatto());
		destinatario.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
		destinatario.getContatto().setIdDataTable(UUID.randomUUID().toString());
		destinatari.add(destinatario);
	}

	/**
	 * Rimuove il destinatario selezionato dal datatable dalla lista
	 * {@link #destinatari}. Il metodo resetta il destinatario reinizializzando i
	 * campi, ma lascia un nuovo destinatario (vuoto) chiamando
	 * {@link #aggiungiDestinatarioNew()}.
	 * 
	 * @param event
	 */
	public void rimuoviDestinatario(final ActionEvent event) {
		final DestinatarioRedDTO destinatario = (DestinatarioRedDTO) getDataTableClickedRow(event);
		destinatari.remove(destinatario);
		if (destinatari.isEmpty()) {
			aggiungiDestinatarioNew();
		}
	}

	/**
	 * Aggiunge un nuovo contatto alla lista dei destinatari TO:
	 * {@link #contattiToInoltraProt}.
	 */
	public void aggiungiDestinatarioToInoltraProt() {
		final Contatto contattoTo = new Contatto();
		contattiToInoltraProt.add(contattoTo);
		contattoTo.setIdDataTable(UUID.randomUUID().toString());

	}

	/**
	 * Rimuove il contatto selezionato sul datatable da:
	 * {@link #contattiToInoltraProt}.
	 * 
	 * @param event
	 */
	public void rimuoviDestinatarioToInoltraProt(final ActionEvent event) {
		final Contatto contatto = (Contatto) getDataTableClickedRow(event);
		contattiToInoltraProt.remove(contatto);

	}

	/**
	 * Aggiunge un nuovo contatto come destinatario CC alla lista:
	 * {@link #contattiCCInoltraProt}.
	 */
	public void aggiungiDestinatarioCCInoltraProt() {
		final Contatto contattoCC = new Contatto();
		contattiCCInoltraProt.add(contattoCC);
		contattoCC.setIdDataTable(UUID.randomUUID().toString());

	}

	/**
	 * Rimuove il contatto selezionato sul datatable dalla lista:
	 * {@link #contattiCCInoltraProt}.
	 * 
	 * @param event
	 */
	public void rimuoviDestinatarioCCInoltraProt(final ActionEvent event) {
		final Contatto contatto = (Contatto) getDataTableClickedRow(event);
		contattiCCInoltraProt.remove(contatto);

	}

	/**
	 * Restituisce lo StreamedContent per il download dell'allegato.
	 * 
	 * @param nodo
	 * @return Streamed content per download
	 */
	public StreamedContent downloadAllegato(final HierarchicalFileWrapperDTO nodo) {

		StreamedContent file = null;
		final String idAllegato = nodo.getNodeId();
		final String nome = nodo.getFile().getFileName();
		final String mimeType = nodo.getFile().getTipoFile().getMimeType();
		try {
			final InputStream stream = new ByteArrayInputStream(scompattaMailSRV.getContentutoDiscompattataMail(utente, idAllegato));
			file = new DefaultStreamedContent(stream, mimeType, nome);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il download del file: ", e);
			showError("Errore durante il download del file.");
		}
		return file;
	}

	/**
	 * Imposta i parametri associati al file del {@link #previewComponent},
	 * recuperando i parametri dal nodo.
	 * 
	 * @param nodo
	 */
	public void selectPreviewAllegato(final HierarchicalFileWrapperDTO nodo) {
		final String idAllegato = nodo.getNodeId();
		final String nome = nodo.getFile().getName();
		final String mimeType = nodo.getFile().getTipoFile().getMimeType();

		previewComponent.setPreviewProtMail(idAllegato, nome, mimeType, DocumentTypeEnum.PROTOCOLLA_MAIL);
	}

	/**
	 * Restituisce la lista dei contatto protocolla.
	 * 
	 * @return contatti
	 */
	public List<Contatto> getContattiProtocolla() {
		return contattiProtocolla;
	}

	/**
	 * Imposta la lista dei contatti protocolla.
	 * 
	 * @param contattiProtocolla
	 */
	public void setContattiProtocolla(final List<Contatto> contattiProtocolla) {
		this.contattiProtocolla = contattiProtocolla;
	}

	/**
	 * Restituisce il flag: scegliContattoProt.
	 * 
	 * @return scegliContattoProt
	 */
	public Boolean getScegliContattoProt() {
		return scegliContattoProt;
	}

	/**
	 * Imposta il flag: scegliContattoProt.
	 * 
	 * @param scegliContattoProt
	 */
	public void setScegliContattoProt(final Boolean scegliContattoProt) {
		this.scegliContattoProt = scegliContattoProt;
	}

	/**
	 * Restituisce il contatto protocolla.
	 * 
	 * @return contatto
	 */
	public Contatto getContattoProtocolla() {
		return contattoProtocolla;
	}

	/**
	 * Imposta il contatto protocolla.
	 * 
	 * @param contattoProtocolla
	 */
	public void setContattoProtocolla(final Contatto contattoProtocolla) {
		this.contattoProtocolla = contattoProtocolla;
	}

	/**
	 * @return notificaProtocolla
	 */
	public Boolean getNotificaProtocolla() {
		return notificaProtocolla;
	}

	/**
	 * 
	 * @param notificaProtocolla
	 */
	public void setNotificaProtocolla(final Boolean notificaProtocolla) {
		this.notificaProtocolla = notificaProtocolla;
	}

	/**
	 * Restituisce l'id del file principale.
	 * 
	 * @return id file
	 */
	public String getIdFilePrincipale() {
		return idFilePrincipale;
	}

	/**
	 * Imposta l'id del file principale.
	 * 
	 * @param idFilePrincipale
	 */
	public void setIdFilePrincipale(final String idFilePrincipale) {
		this.idFilePrincipale = idFilePrincipale;
	}

	// #############################################
	// ########## Gestione tables ##################
	/**
	 * Metodo per gestire la riga selezionata - casella mail.
	 */
	public final void rowSelector(final SelectEvent se) {
		// seleziono casella
		accountsDTH.rowSelector(se);
		selectFirst();

		popolaAltriAccounts(accountsDTH, otherAccountsDTH);

		// cambio visibilità per visualizzare la tabella delle email
		if (!flagVisibilita) {
			flagVisibilita = true;
		}
	}

	/**
	 * Metodo per gestire la riga selezionata - Posta.
	 */
	public final void rowSelectorPosta(final SelectEvent se) {
		try {
			mailDTH.rowSelector(se);
			impostaMailDaAggiungere();
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Errore nella selezione: " + e);
		}
	}

	/**
	 * Aggiorna pagina alla pressione check per selezoine/deselezione di tutti i
	 * documenti.
	 * 
	 * @param event evento gui
	 */

	/**
	 * Metodo per gestire la pressione del pulsante per selezionare/deselezionare
	 * tutte le checkbox.
	 * 
	 * @param event evento generato alla pressione del pulsante di selezione
	 *              multipla
	 */
	public void handleAllCheckBoxInbox() {
		handleAllCheckBox(mailDTH);
	}

	/**
	 * 
	 */
	public void handleAllCheckBoxAccounts() {
		handleAllCheckBoxCaselle(otherAccountsDTH);
	}

	/**
	 * 
	 * @param dth
	 */
	private void handleAllCheckBoxCaselle(final DataTableHelper<CasellaPostaDTO, CasellaPostaDTO> dth) {
		super.handleAllCheckBoxGeneric(dth);
	}

	/**
	 * 
	 * @param dth
	 */
	private void handleAllCheckBox(final DataTableHelper<EmailDTO, DetailEmailDTO> dth) {
		if(dth != null) {
			super.handleAllCheckBoxGeneric(dth);
			manageTabMultiActions();
		}
	}

	/**
	 * {@link #updateCheckBoxMail(AjaxBehaviorEvent, DataTableHelper)}.
	 * 
	 * @param event
	 */
	public void updateCheckMail(final AjaxBehaviorEvent event) {
		updateCheckBoxMail(event);
	}

	/**
	 * {@link #updateCheckAccount(AjaxBehaviorEvent, DataTableHelper)}.
	 * 
	 * @param event
	 */
	public void updateCheckCasella(final AjaxBehaviorEvent event) {
		updateCheckAccount(event, otherAccountsDTH);
	}

	/**
	 * Effettua un update sulla checkbox alla selezione dell'account.
	 * 
	 * @param event
	 * @param dth
	 */
	private void updateCheckAccount(final AjaxBehaviorEvent event, final DataTableHelper<CasellaPostaDTO, CasellaPostaDTO> dth) {
		final CasellaPostaDTO masterChecked = getDataTableClickedRow(event);
		final Boolean newValue = ((SelectBooleanCheckbox) event.getSource()).isSelected();
		if (newValue != null && newValue) {
			dth.getSelectedMasters().add(masterChecked);
		} else {
			dth.getSelectedMasters().remove(masterChecked);
		}
	}

	/**
	 * Effettua un update sulla checkbox alla selezione della mail selezionata.
	 * 
	 * @param event
	 * @param dth
	 */
	private void updateCheckBoxMail(final AjaxBehaviorEvent event) {
		final EmailDTO masterChecked = getDataTableClickedRow(event);
		final Boolean newValue = ((SelectBooleanCheckbox) event.getSource()).isSelected();
		if (newValue != null && newValue) {
			mailDTH.getSelectedMasters().add(masterChecked);
		} else {
			mailDTH.getSelectedMasters().remove(masterChecked);
		}

		if (allElementCheckboxes.get(pageIndex) == null) {
			allElementCheckboxes.set(pageIndex, Boolean.FALSE);
		}

		// la prima condizione serve nel caso di selezione multipla dal Libro Firma,
		// Corriere, Da Lavorare
		if (mailDTH.getSelectedMasters() != null && !mailDTH.getSelectedMasters().isEmpty()) {
			allElementCheckboxes.set(pageIndex, true);
		} else {
			allElementCheckboxes.set(pageIndex, allElementCheckboxes.get(pageIndex) && newValue);
		}

		// Gestione del tab delle azioni massive
		manageTabMultiActions();
	}

	/**
	 * Metodo per la gestione del tab 'azioni massive' in base ai documenti
	 * selezionati.
	 */
	private void manageTabMultiActions() {
		if (mailDTH.getSelectedMasters().isEmpty()) {
			// quando eseguo un check su un elemento imposto il tab delle azioni massive
			// come aperto
			sessionBean.setActiveIndexTabView(PRIMO_ATTIVO);
		} else {
			// ogni volta che levo il check da un elemento controllo se è l'ultimo. se si
			// imposto il tab che contiene le code
			sessionBean.setActiveIndexTabView(SECONDO_ATTIVO);
		}
	}

	/**
	 * Metodo per selezionare tutti gli elementi dalle mail.
	 */
	public final void selectAll() {
		mailDTH.getSelectedMasters().clear();
		for (final EmailDTO m : mailDTH.getMasters()) {
			m.setSelected(true);
			mailDTH.getSelectedMasters().add(m);
		}
		FacesHelper.update(CENTRAL_SECTION_FORM);
		FacesHelper.update("eastSectionForm:contenutoPosta");
	}

	/**
	 * Metodo per deselezionare tutti gli elementi del libro firma.
	 */
	public final void unselectAll() {
		mailDTH.getSelectedMasters().clear();
		for (final EmailDTO m : mailDTH.getMasters()) {
			m.setSelected(false);
		}
		FacesHelper.update(CENTRAL_SECTION_FORM);
		FacesHelper.update(EAST_SECTION_FORM);
	}

	/**
	 * Aggiorna mail da gui.
	 */
	public final void refreshFromGUI() {
		refresh(true);
		showInfoMessage("Aggiornamento avvenuto con successo.");
	}

	/**
	 * Aggiungi contatti a casella postale.
	 */
	public void impostaMailDaAggiungere() {
		if (mailDTH.getCurrentMaster() != null) {
			inserisciContattoCasellaPostaleItem.setMailPec(mailDTH.getCurrentMaster().getMittente());
			inserisciContattoCasellaPostaleItem.setTipoPersona("F");
		}
	}

	/**
	 * {@link #salvaContattoRubRED(Contatto)}.
	 */
	public void salvaContattoRubricaRED() {
		salvaContattoRubRED(inserisciContattoCasellaPostaleItem);
	}

	/**
	 * Memorizza il nuovo contatto, definito dal parametro in ingresso, nella
	 * rubrica.
	 * 
	 * @param contSalvataggio
	 */
	public void salvaContattoRubRED(final Contatto contSalvataggio) {
		try {
			contSalvataggio.setIdAOO(utente.getIdAoo());
			contSalvataggio.setTipoRubrica("RED");
			contSalvataggio.setPubblico(1);
			contSalvataggio.setDatacreazione(new Date());
			if (valida(contSalvataggio)) {
				if (utente.getCheckUnivocitaMail()) {
					rubSRV.checkMailPerContattoGiaPresentePerAoo(contSalvataggio, false);
				}
				if (isPermessoUtenteGestioneRichiesteRubrica()) {

					rubSRV.inserisci(null, contSalvataggio, utente.getCheckUnivocitaMail());

					LOGGER.info("Nuovo Contatto Inserito");
					if (renderFromProtSemiautomatica) {
						detailsProt.setMittenteContatto(contSalvataggio);
						FacesHelper.executeJS(SHOW_PROTOCOLLAMAILWV_JS);
						FacesHelper.executeJS(WRAP_ADD_EVENT_LISTENER_FOR_PROT_MAIL);
					}
					callBackToAction(contSalvataggio);
				} else {
					contSalvataggio.setDatadisattivazione(new Date());
					final Long idContattoRichiestaCreazione = rubSRV.inserisciContattoPerRichiestaCreazione(contSalvataggio, true);
					contSalvataggio.setContattoID(idContattoRichiestaCreazione);
					final NotificaContatto notifica = inserisciNotificaRichiestaCreazioneModifica(true, contSalvataggio);
					rubSRV.notificaCreazioneContatto(notifica);
					LOGGER.info("Nuova richiesta creazione contatto inserita");
					if (renderFromProtSemiautomatica) {
						detailsProt.setMittenteContatto(contSalvataggio);
						FacesHelper.executeJS(SHOW_PROTOCOLLAMAILWV_JS);
						FacesHelper.executeJS(WRAP_ADD_EVENT_LISTENER_FOR_PROT_MAIL);
					}
					notaRichiestaCreazione = "";
					callBackToAction(contSalvataggio);
					showInfoMessage(SUCCESS_CREAZIONE_CONTATTO_MSG);
				}
			}
		} catch (final Exception e) {
			showError(e);
			LOGGER.error(e);
		}
	}

	/**
	 * Gestisce la chiusura della dialog di selezione contatti.
	 */
	public void chiudiDialogSceltaContatti() {
		scegliContattoProt = false;
		resetCampiRubricaRicerca();
		DataTable table = getDataTableByIdComponent(ID_RIC_RUBRICA_DMDRC_HTML);
		if (table != null) {
			table.setValueExpression("sortBy", null);
			table.reset();
			resultRicercaContatti = new ArrayList<>();

		}
		table = getDataTableByIdComponent(PREFERITIMITTENTI_DMD_RESOURCE_PATH);
		if (table != null) {
			table.setValueExpression("sortBy", null);
			table.reset();
			contattiPreferiti = new ArrayList<>();
		}

		final TabView tb = (TabView) FacesContext.getCurrentInstance().getViewRoot().findComponent(MITTENTE_MAIL_PREF_TABS_DMD_RESOURCE_PATH);
		if (tb != null) {
			tb.setActiveIndex(0);
		}
	}

	private void callBackToAction(final Contatto contSalvataggio) {
		boolean callJs = false;
		if (callBackToAction) {
			contattoProtocolla = new Contatto();
			contattoRifiuta = new Contatto();
			contattoInoltra = new Contatto();
			FacesHelper.executeJS(HIDE_SCEGLICONTATTOPROTOCOLLADLGWV_JS);
			if (operazioneSelezionata == null) {
				callJs = true;
				contattoProtocolla.copyFromContatto(contSalvataggio);
				continuaProtocol();
			} else if (operazioneSelezionata.equals(MailOperazioniEnum.RIFIUTO_CON_PROTOCOLLAZIONE)) {
				contattoRifiuta.copyFromContatto(contSalvataggio);
				rifiutaConProtocollo();
			} else if (operazioneSelezionata.equals(MailOperazioniEnum.INOLTRO_CON_PROTOCOLLAZIONE)) {
				contattoInoltra.copyFromContatto(contSalvataggio);
				inoltraConProtocollo();
			}
		}
		visualizzaCreaContatto = false;
		setInserisciContattoItem(new Contatto());
		setinserisciContattoCasellaPostaleItem(new Contatto());
		setInModificaDaShortcut(false);
		FacesHelper.executeJS("PF('aggiungiContattoDlg_WV').hide()");
		FacesHelper.executeJS("PF('dialogRichCreazioneVW').hide();");
		FacesHelper.update(AGGIUNGICONTATTODLG_RESOURCE_PATH);
		FacesHelper.update("eastSectionForm:protocollaMail_oPnl");
		if (callJs) {
			FacesHelper.executeJS("$('.ui-g.generic_width600px.hideClassPerProtocolla').parent().parent().hide();");
			FacesHelper.executeJS("$('.ui-widget-overlay.ui-dialog-mask').hide();");
		}
	}

	/**
	 * Reinizializza i contatti impostando a true i flag sulla ricerca.
	 */
	public void chiudiContattoDaAggiungi() {
		visualizzaCreaContatto = false;
		
		initFlagRicercaContatto();
	}

	// ### Copia da RubricaBean Start
	private boolean valida(final Contatto contatto) {

		boolean isOK = false;
		boolean campiObbligatoriOK = false;
		boolean fisicoOGiuridico = true;
		boolean procedi = false;

		final FacesContext context = FacesContext.getCurrentInstance();

		if (contatto.getTipoPersona() == null || (!"F".equals(contatto.getTipoPersona()) && !"G".equals(contatto.getTipoPersona()))) {
			fisicoOGiuridico = false;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ERRORE_LABEL, "Il contatto deve essere necessariamente Fisico o Giuridico"));
		}

		if ((!StringUtils.isNullOrEmpty(contatto.getNome()) && !StringUtils.isNullOrEmpty(contatto.getCognome())) || // Se ho compilato nome e cognome
				(!StringUtils.isNullOrEmpty(contatto.getNome()) && contatto.getTipoPersona() != null && "G".equals(contatto.getTipoPersona())) // se ho compilato solo il nome
																																				// ed è persona giuridica
		) {
			campiObbligatoriOK = true;
		}

		final String valuePEC = contatto.getMailPec();
		final String valuePEO = contatto.getMail();

		final Pattern patternEmail = Pattern.compile("^[a-zA-Z0-9_\\.-]+@[a-zA-Z0-9-_\\.]+[a-zA-Z0-9-_]\\.[a-zA-Z0-9-]+$");
		// E' considerata OK anche se il campo è vuoto
		boolean flagMailOk = true;
		// E' considerata OK anche se il campo è vuoto
		boolean flagMailPecOk = true;

		// controllo formato E-Mail PEO
		if (!valuePEO.isEmpty() && valuePEO != null && !patternEmail.matcher(valuePEO).matches()) {
			flagMailOk = false;
		}
		// controllo formato E-Mail PEC
		if (!valuePEC.isEmpty() && valuePEC != null && !patternEmail.matcher(valuePEC).matches()) {
			flagMailPecOk = false;
		}

		if (!campiObbligatoriOK) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ERRORE_LABEL, "Compilare i campi obbligatori"));
		}

		if (flagMailOk && flagMailPecOk) {
			isOK = true;
		} else {
			if (!flagMailPecOk) {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ERRORE_LABEL, "la PEC non ha il formato corretto!"));
			}
			if (!flagMailOk) {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ERRORE_LABEL, "la PEO non ha il formato corretto!"));
			}
		}

		procedi = isOK && campiObbligatoriOK && fisicoOGiuridico;

		if (procedi && StringUtils.isNullOrEmpty(contatto.getAliasContatto())) {
			if (StringUtils.isNullOrEmpty(contatto.getCognome())) {
				contatto.setAliasContatto(contatto.getNome());
			} else {
				contatto.setAliasContatto(contatto.getCognome() + " " + contatto.getNome());
			}
		}

		return procedi;
	}

	/**
	 * Aggiorna la regione in base alla regione selezionata.
	 * 
	 * @param event
	 */
	public void updateRegioniOnSelectAddCreazione(final AjaxBehaviorEvent event) {
		if (event instanceof SelectEvent) {
			inserisciContattoCasellaPostaleItem.setRegioneObj((RegioneDTO) ((SelectEvent) event).getObject());
		} else {
			inserisciContattoCasellaPostaleItem.setRegioneObj(null);
		}
		updateRegioniOnSelectAdd(inserisciContattoCasellaPostaleItem);
	}

	/**
	 * Aggiorna la provincia in base alla provincia selezionata.
	 * 
	 * @param event
	 */
	public void updateProvinceOnSelectAddCreazione(final AjaxBehaviorEvent event) {
		if (event instanceof SelectEvent) {
			inserisciContattoCasellaPostaleItem.setProvinciaObj((ProvinciaDTO) ((SelectEvent) event).getObject());
		} else {
			inserisciContattoCasellaPostaleItem.setProvinciaObj(null);
		}
		updateProvinceOnSelectAdd(inserisciContattoCasellaPostaleItem);

	}

	/**
	 * Aggiorna il comune in base al comune selezionata.
	 * 
	 * @param event
	 */
	public void updateComuniOnSelectAddCreazione(final AjaxBehaviorEvent event) {
		if (event instanceof SelectEvent) {
			inserisciContattoCasellaPostaleItem.setComuneObj((ComuneDTO) ((SelectEvent) event).getObject());
		} else {
			inserisciContattoCasellaPostaleItem.setComuneObj(null);
		}
		updateComuniOnSelectAdd(inserisciContattoCasellaPostaleItem);
	}

	/**
	 * Popola la lista dei comuni e la restituisce come lista di ComuneDTO. Usa
	 * {@link #inserisciContattoCasellaPostaleItem}.
	 * 
	 * @param query
	 * @return lista di ComuneDTO
	 */
	public final List<ComuneDTO> loadComuniAddCreazione(final String query) {
		return loadComuniAdd(query, inserisciContattoCasellaPostaleItem);
	}

	/**
	 * Popola la lista delle province e la restituisce come lista di ProvinciaDTO.
	 * Usa {@link #inserisciContattoCasellaPostaleItem}.
	 * 
	 * @param query
	 * @return lista di ProvinciaDTO
	 */
	public final List<ProvinciaDTO> loadProvinceAddCreazione(final String query) {

		return loadProvinceAdd(query);

	}

	/**
	 * Popola la lista delle province e la restituisce come lista di ProvinciaDTO.
	 * Usa {@link #inserisciContattoItem}.
	 * 
	 * @param query
	 * @return lista di ProvinciaDTO
	 */
	public final List<ProvinciaDTO> loadProvince(final String query) {
		return loadProvinceAdd(query);
	}

	/**
	 * Popola la lista dei comuni e la restituisce come lista di ComuneDTO. Usa
	 * {@link #inserisciContattoItem}.
	 * 
	 * @param query
	 * @return lista di ComuneDTO
	 */
	public final List<ComuneDTO> loadComuni(final String query) {
		return loadComuniAdd(query, inserisciContattoItem);
	}

	/**
	 * Popola la lista delle province e la restituisce come lista di ProvinciaDTO.
	 * 
	 * @param query
	 * @param contatto
	 * @return lista di ProvinciaDTO
	 */
	public final List<ProvinciaDTO> loadProvinceAdd(final String query) {
		List<ProvinciaDTO> province = null;
		try {

			final IProvinciaFacadeSRV provinciaSRV = ApplicationContextProvider.getApplicationContext().getBean(IProvinciaFacadeSRV.class);
			province = new ArrayList<>();
			province = provinciaSRV.getProvFromRegioneAndDesc(query);

		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
		return province;
	}

	/**
	 * Popola la lista dei comuni di un contatto e la restituisce come lista di
	 * ComuneDTO.
	 * 
	 * @param query
	 * @param contatto
	 * @return lista di ComuneDTO
	 */
	public final List<ComuneDTO> loadComuniAdd(final String query, final Contatto contatto) {
		
		List<ComuneDTO> comuni = new ArrayList<>();
		try {
			final IComuneFacadeSRV comuneSRV = ApplicationContextProvider.getApplicationContext().getBean(IComuneFacadeSRV.class);
			final String idProv = contatto.getIdProvinciaIstat();
			comuni = comuneSRV.getComuni(Long.parseLong(idProv), query);

		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}

		return comuni;
	}

	/**
	 * Popola la lista delle regioni e la restituisce come lista di ComuneDTO.
	 * 
	 * @param query
	 * @return lista di RegioneDTO
	 */
	public final List<RegioneDTO> loadRegioni(final String query) {
		List<RegioneDTO> regioni = null;
		try {
			final IRegioneFacadeSRV regioneSRV = ApplicationContextProvider.getApplicationContext().getBean(IRegioneFacadeSRV.class);
			regioni = regioneSRV.getRegioni(query);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
		return regioni;
	}

	// ###Copia da RubricaBean END

	private void resetContattoProtocollaSelezionato() {
		if (contattoProtocollaSelezionato != null) {
			setContattoProtocollaSelezionato(null);
		}
	}

//############ Getters e Setters ##################
	/**
	 * Getter.
	 * 
	 * @return stato selezionato
	 */
	public Integer getSelectedState() {
		return selectedState;
	}

	/**
	 * Setter.
	 * 
	 * @param inSelectedState stato selezionato
	 */
	public void setSelectedState(final Integer inSelectedState) {
		this.selectedState = inSelectedState;
	}

	/**
	 * Restituisce il motivo di eliminazione.
	 * 
	 * @return motivo
	 */
	public String getMotiveDelete() {
		return motiveDelete;
	}

	/**
	 * Imposta il motivo di eliminazione.
	 * 
	 * @param motiveDelete
	 */
	public void setMotiveDelete(final String motiveDelete) {
		this.motiveDelete = motiveDelete;
	}

	/**
	 * @return autoselection
	 */
	public Boolean getAutoselection() {
		return autoselection;
	}

	/**
	 * @param autoselection
	 */
	public void setAutoselection(final Boolean autoselection) {
		this.autoselection = autoselection;
	}

	/**
	 * Restituisce il flag: selectAll.
	 * 
	 * @return selectAll
	 */
	public Boolean getSelectAll() {
		return selectAll;
	}

	/**
	 * @param selectAll
	 */
	public void setSelectAll(final Boolean selectAll) {
		this.selectAll = selectAll;
	}

	/**
	 * Restituisce gli stati.
	 * 
	 * @return stati
	 */
	public Collection<StatoMailGUIEnum> getStates() {
		return states;
	}

	/**
	 * Imposta gli stati della mail.
	 * 
	 * @param states
	 */
	public void setStates(final Collection<StatoMailGUIEnum> states) {
		this.states = states;
	}

	/**
	 * Restituisce la filterString.
	 * 
	 * @return filterString
	 */
	public String getFilterString() {
		return filterString;
	}

	/**
	 * @param filterString
	 */
	public void setFilterString(final String filterString) {
		this.filterString = filterString;
	}

	/**
	 * Restituisce la lista delle mail.
	 * 
	 * @return lista mail
	 */
	public List<CasellaPostaDTO> getListaMail() {
		return listaMail;
	}

	/**
	 * Imposta la lista delle mail.
	 * 
	 * @param listaMail
	 */
	public void setListaMail(final List<CasellaPostaDTO> listaMail) {
		this.listaMail = listaMail;
	}

	/**
	 * Restituisce il datatable helper per il datatable delle mail.
	 * 
	 * @return datatable helper per mail
	 */
	public DataTableHelper<EmailDTO, DetailEmailDTO> getMailDTH() {
		return mailDTH;
	}

	/**
	 * Restituisce il datatable helper per il datatable degli accounts.
	 * 
	 * @return datatable helper per accounts
	 */
	public DataTableHelper<CasellaPostaDTO, CasellaPostaDTO> getAccountsDTH() {
		return accountsDTH;
	}

	/**
	 * Restituisce cpAttiva.
	 * 
	 * @return cpAttiva
	 */
	public String getCpAttiva() {
		return cpAttiva;
	}

	/**
	 * @param cpAttiva
	 */
	public void setCpAttiva(final String cpAttiva) {
		this.cpAttiva = cpAttiva;
	}

	/**
	 * Restituisce true se visibile, false altrimenti.
	 * 
	 * @return flag visibilita
	 */
	public boolean getFlagVisibilita() {
		return flagVisibilita;
	}

	/**
	 * Imposta flagVisibilita.
	 * 
	 * @param flagVisibilita
	 */
	public void setFlagVisibilita(final boolean flagVisibilita) {
		this.flagVisibilita = flagVisibilita;
	}

	/**
	 * Restituisce il current text della nota.
	 * 
	 * @return current text note
	 */
	public String getCurrentTextNote() {
		return currentTextNote;
	}

	/**
	 * @param currentTextNote
	 */
	public void setCurrentTextNote(final String currentTextNote) {
		this.currentTextNote = currentTextNote;
	}

	/**
	 * Restituisce il file allegato.
	 * 
	 * @return file allegato
	 */
	public StreamedContent getFileAllegato() {
		return fileAllegato;
	}

	/**
	 * Restituisce true se in creazione contatto, false altrimenti.
	 * 
	 * @return true se in creazione contatto, false altrimenti
	 */
	public boolean isInCreazioneContatto() {
		return inCreazioneContatto;
	}

	/**
	 * Imposta il flag associato alla creazione del contatto.
	 * 
	 * @param inCreazioneContatto
	 */
	public void setInCreazioneContatto(final boolean inCreazioneContatto) {
		this.inCreazioneContatto = inCreazioneContatto;
	}

	/**
	 * Setter.
	 * 
	 * @param selectedAllegatoId id allegato
	 */
	public void setSelectedAllegatoId(final String selectedAllegatoId) {
		try {
			// recupero del content tramite l'id allegato
			final FileDTO file = mailSRV.getFileAllegatoFromGuid(utente.getFcDTO(), selectedAllegatoId, utente.getIdAoo());
			final InputStream stream = new ByteArrayInputStream(file.getContent());
			fileAllegato = new DefaultStreamedContent(stream, file.getMimeType(), file.getFileName());
			showInfoMessage("Download terminato con successo.");
		} catch (final Exception e) {
			LOGGER.error("Errore durante il download del documento", e);
			showError(e);
		}
	}

	/**
	 * Restituisce la lista di contatti per l'inoltro rifiuta.
	 * 
	 * @param queryAlias
	 * @return lista di contatti
	 */
	public List<Contatto> loadContattiRubricaMailInoltraRifiuta(final String queryAlias) {
		List<Contatto> listcontatti = null;

		if (!StringUtils.isNullOrEmpty(queryAlias) && queryAlias.length() > 2) {
			listcontatti = rubSRV.getContattiRubricaMailPerAlias(cpAttiva, queryAlias);
		}

		return listcontatti;
	}

	/**
	 * Metodo per l'autocomplete dei destinatari per inoltra mail.
	 */
	public List<DestinatarioRedDTO> loadContattiRubricaMailInoltra(final List<Contatto> contatti, final String queryAlias) {
		final List<DestinatarioRedDTO> listCont = new ArrayList<>();
		try {
			if (!StringUtils.isNullOrEmpty(queryAlias) && queryAlias.trim().length() >= 3) {
				final String[] queryArr = queryAlias.trim().toUpperCase().split(" ");
				if (!contatti.isEmpty() && utente.isAutocompleteRubricaCompleta()) {
					for (final Contatto cont : contatti) {
						if (cont.getMail() != null && StringUtils.containsAllWords(cont.getMail().toUpperCase(), queryArr)
								|| cont.getMailPec() != null && StringUtils.containsAllWords(cont.getMailPec().toUpperCase(), queryArr)
								|| cont.getAliasContatto() != null && StringUtils.containsAllWords(cont.getAliasContatto().toUpperCase(), queryArr)
								|| cont.getCognome() != null && StringUtils.containsAllWords(cont.getCognome().toUpperCase(), queryArr)
								|| cont.getNome() != null && StringUtils.containsAllWords(cont.getNome().toUpperCase(), queryArr)) {
							// Riversiamo i contatti nell'oggetto destinatarioreddto
							final DestinatarioRedDTO destCont = new DestinatarioRedDTO();
							destCont.setContatto(cont);
							destCont.setIdNodo(cont.getIdNodo());
							destCont.setIdUtente(cont.getIdUtente());
							destCont.setSelected(cont.getSelected());
							destCont.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
							listCont.add(destCont);
						}
					}
				} else if (!contatti.isEmpty()) {
					for (final Contatto cont : contatti) {
						if (cont.getAliasContatto() != null && StringUtils.containsAllWords(cont.getAliasContatto().toUpperCase(), queryArr)
								|| cont.getCognome() != null && StringUtils.containsAllWords(cont.getCognome().toUpperCase(), queryArr)
								|| cont.getNome() != null && StringUtils.containsAllWords(cont.getNome().toUpperCase(), queryArr)) {
							// Riversiamo i contatti nell'oggetto destinatarioreddto
							final DestinatarioRedDTO destCont = new DestinatarioRedDTO();
							destCont.setContatto(cont);
							destCont.setIdNodo(cont.getIdNodo());
							destCont.setIdUtente(cont.getIdUtente());
							destCont.setSelected(cont.getSelected());
							destCont.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
							listCont.add(destCont);
						}
					}
				}
			}
		} catch (final Exception e) {
			showError("Errore durante la ricerca dei contatti preferiti: " + e.getMessage());
			LOGGER.error(e);
		}
		return listCont;
	}

	private List<Contatto> matchQuery(final List<Contatto> contatti, String query) {
		final List<Contatto> lista = new ArrayList<>();
		try {
			if (StringUtils.isNullOrEmpty(query)) {
				return contatti;
			}
			query = query.toUpperCase();

			if (!StringUtils.isNullOrEmpty(query) && query.trim().length() >= 3) {
				final String[] queryArr = query.trim().toUpperCase().split(" ");
				// filtro contatti inseriti
				for (final Contatto cont : contatti) {
					if (utente.isAutocompleteRubricaCompleta() && (cont.getDisabilitaElemento() == null || Boolean.FALSE.equals(cont.getDisabilitaElemento()))
							&& (cont.getMail() != null && StringUtils.containsAllWords(cont.getMail().toUpperCase(), queryArr)
									|| cont.getMailPec() != null && StringUtils.containsAllWords(cont.getMailPec().toUpperCase(), queryArr)
									|| cont.getAliasContatto() != null && StringUtils.containsAllWords(cont.getAliasContatto().toUpperCase(), queryArr)
									|| cont.getCognome() != null && StringUtils.containsAllWords(cont.getCognome().toUpperCase(), queryArr)
									|| cont.getNome() != null && StringUtils.containsAllWords(cont.getNome().toUpperCase(), queryArr))) {
						LOGGER.info("matchQuery -> autocompleteRubricaCompleta");
						lista.add(cont);
					} else if ((cont.getDisabilitaElemento() == null || Boolean.FALSE.equals(cont.getDisabilitaElemento()))
							&& (cont.getAliasContatto() != null && StringUtils.containsAllWords(cont.getAliasContatto().toUpperCase(), queryArr)
									|| cont.getCognome() != null && StringUtils.containsAllWords(cont.getCognome().toUpperCase(), queryArr)
									|| cont.getNome() != null && StringUtils.containsAllWords(cont.getNome().toUpperCase(), queryArr))) {
						lista.add(cont);
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante la ricerca dei contatti preferiti: " + e.getMessage());
		}
		return lista;
	}

	/**
	 * Restituisce solo gli elementi non vuoti, gli elementi rimangono comunque
	 * nella lista originale.
	 **/
	public List<Contatto> prendiContattiPuliti(final List<Contatto> contatti) {
		final List<Contatto> listaPulita = new ArrayList<>();
		if (contatti != null) {
			for (final Contatto cont : contatti) {
				if (cont != null) {
					listaPulita.add(cont);
				}
			}
		}
		return listaPulita;
	}

	private static boolean checkValidaPDF(final AllegatoDTO allegato) {
		boolean isValido = true;
		try {
			if (allegato.getNomeFile().toLowerCase().endsWith("pdf")) {
				isValido = PdfHelper.checkValidaPDF(allegato.getContent());
			}
		} catch (final Exception e) {
			LOGGER.warn(e.getMessage(), e);
			isValido = false;
		}

		return isValido;
	}

	/**
	 * SHORTCUT CREAZIONE CONTATTI START
	 ********************************************************************/

	/**
	 * 
	 */
	public final void onSelectRegione(final AjaxBehaviorEvent event) {
		try {
			if (event instanceof SelectEvent) {
				getInserisciContattoItem().setRegioneObj((RegioneDTO) ((SelectEvent) event).getObject());
			} else {
				getInserisciContattoItem().setRegioneObj(null);
			}

			RegioniProvinceComuniComponent.onSelectRegione(getInserisciContattoItem());
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * 
	 */
	public final void onSelectProvincia(final AjaxBehaviorEvent event) {
		try {
			if (event instanceof SelectEvent) {
				getInserisciContattoItem().setProvinciaObj((ProvinciaDTO) ((SelectEvent) event).getObject());
			} else {
				getInserisciContattoItem().setProvinciaObj(null);
			}

			RegioniProvinceComuniComponent.onSelectProvincia(getInserisciContattoItem());
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * @param event
	 */
	public final void onSelectComune(final AjaxBehaviorEvent event) {
		try {
			if (event instanceof SelectEvent) {
				getInserisciContattoItem().setComuneObj((ComuneDTO) ((SelectEvent) event).getObject());
			} else {
				getInserisciContattoItem().setComuneObj(null);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Salva il contatto: {@link #inserisciContattoItem}.
	 */
	public void salvaContattoProtocolla() {
		callBackToAction = true;
		if (isPermessoUtenteGestioneRichiesteRubrica()) {
			salvaContattoRubRED(getInserisciContattoItem());
		}

	}

	/**
	 * Esegue una validazione del contatto: {@link #inserisciContattoItem} e lo
	 * inserisce in rubrica.
	 */
	public void validaEInserisciContatto() {
		try {
			final FacesContext context = FacesContext.getCurrentInstance();
			if (!RubricaComponent.valida(getInserisciContattoItem(), context)) {
				return;
			}

			getInserisciContattoItem().setIdAOO(utente.getIdAoo());

			creaContatto();

			clearContattoandHideModifica();
			notaRichiestaCreazione = "";

		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	private NotificaContatto inserisciNotificaRichiestaCreazioneModifica(final boolean isRichiestaCreazione, final Contatto inserisciNotificaContatto) {
		final NotificaContatto notifica = new NotificaContatto();
		notifica.setNuovoContatto(inserisciNotificaContatto);
		notifica.setTipoNotifica(NotificaContatto.TIPO_NOTIFICA_RICHIESTA);

		if (StringUtils.isNullOrEmpty(inserisciNotificaContatto.getAliasContatto())) {
			notifica.setAlias(inserisciNotificaContatto.getNome() + " " + inserisciNotificaContatto.getCognome());
		} else {
			notifica.setAlias(inserisciNotificaContatto.getAliasContatto());
		}

		notifica.setCognome(inserisciNotificaContatto.getCognome());
		notifica.setNome(inserisciNotificaContatto.getNome());
		notifica.setDataOperazione(new Date());

		// Per la richiesta creazione contatto creo un contatto a db con
		// datadisattivazione in modo che non compare nelle ricerche
		// Se la richiesta viene accettata faccio l'update della data così compare
		// altrimenti elimino quella riga
		if (inserisciNotificaContatto.getContattoID() != null) {
			notifica.setIdContatto(inserisciNotificaContatto.getContattoID());
		}

		notifica.setIdNodoModifica(utente.getIdUfficio());
		notifica.setIdUtenteModifica(utente.getId());
		if (isRichiestaCreazione) {
			notifica.setNote(notaRichiestaCreazione);
			notifica.setOperazioneEnum(TipoOperazioneEnum.RICHIESTA_CREAZIONE_CONTATTO);
		} else {
			notifica.setNote(notaRichiestaModifica);
		}
		notifica.setTipologiaContatto(inserisciNotificaContatto.getTipoRubrica());
		notifica.setUtente(utente.getUsername());
		notifica.setStato(StatoNotificaContattoEnum.IN_ATTESA);
		notifica.setIdAoo(utente.getIdAoo());

		return notifica;
	}

	/**
	 * Aggiunge un nuovo utente alla rubrica (e se chiamato dalla dialog inoltra
	 * mail lo aggiunge pure ai destinatari).
	 */
	private void creaContatto() {
		try {
			getInserisciContattoItem().setIdAOO(utente.getIdAoo());
			inserisciContattoItem.setOnTheFly(0);
			RubricaComponent.inserisciContatto(getInserisciContattoItem(), utente);

			if (renderInoltra || renderInoltraProt) {
				aggiungiContattoAiDestinatariInoltraProt();
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Allineamento preferiti e ricerca.
	 */
	public void clearContattoandHideModifica() {
		if (getContattiPreferiti() != null) {
			// * Allineo Preferiti START**//
			boolean isInPreferiti = false;
			Contatto contPreferito = null;

			for (final Contatto contPref : getContattiPreferiti()) {
				if (contPref.getContattoID().equals(getInserisciContattoItem().getContattoID())) {
					isInPreferiti = true;
					contPreferito = contPref;
				}
			}

			if (getInserisciContattoItem().getIsPreferito() != null && getInserisciContattoItem().getIsPreferito()) {
				if (isInPreferiti) {
					contPreferito.copyFromContatto(getInserisciContattoItem());
				} else {
					getContattiPreferiti().add(getInserisciContattoItem());
				}

			} else {
				if (isInPreferiti) {
					getContattiPreferiti().remove(contPreferito);
				}
			}

			// * Allineo Preferiti END**//
		}

		if (getResultRicercaContatti() != null) {
			// * Allineo RICERCA START**//

			Contatto contRicercato = null;

			for (final Contatto contRic : getResultRicercaContatti()) {
				if (contRic.getContattoID().equals(getInserisciContattoItem().getContattoID())) {
					contRicercato = contRic;
				}
			}

			if (contRicercato != null) {
				contRicercato.copyFromContatto(getInserisciContattoItem());
			}

			// * Allineo RICERCA END**//
		}

		setInserisciContattoItem(new Contatto());
		setInModificaDaShortcut(false);
	}

	/**
	 * Gestisce la modifica del contatto.
	 */
	public void modificaContattoProtocolla() {
		try {
			callBackToAction = true;
			final String mailProtocollo = currentmail.getMittente();

			if (isPermessoUtenteGestioneRichiesteRubrica()) {
				if (valida(inserisciContattoItem)) {
					if (((inserisciContattoItem.getMail().equalsIgnoreCase(mailProtocollo)) || (inserisciContattoItem.getMailPec().equalsIgnoreCase(mailProtocollo)))
							|| inModificaDaShortcut) {
						final FacesContext context = FacesContext.getCurrentInstance();
						if (!RubricaComponent.valida(getInserisciContattoItem(), context)) {
							return;
						}

						getInserisciContattoItem().setIdAOO(utente.getIdAoo());

						RubricaComponent.modificaContatto(getInserisciContattoItem(), utente);

						contattoProtocolla = new Contatto();
						FacesHelper.executeJS(HIDE_SCEGLICONTATTOPROTOCOLLADLGWV_JS);
						contattoProtocolla.copyFromContatto(inserisciContattoItem);

					}
				} else {
					showWarnMessage("Attenzione! Il contatto deve avere la mail (Peo o Pec) uguale a: " + mailProtocollo);
				}
			} else {
				richiediModificaUtenteDMD();
			}
			callBackToAction(inserisciContattoItem);

		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Inoltra la richiesta di modifica o la reale modifica se l'utente detiene il
	 * permesso: PermessiEnum.GESTIONE_RUBRICA.
	 */
	public void modificaORichiediModificaUtente() {
		try {
			final FacesContext context = FacesContext.getCurrentInstance();
			if (!RubricaComponent.valida(getInserisciContattoItem(), context)) {
				return;
			}

			getInserisciContattoItem().setIdAOO(utente.getIdAoo());

			if (isPermessoUtenteGestioneRichiesteRubrica()) {
				modificaUtente();
			} else {
				richiediModificaUtenteDMD();
			}
			clearContattoandHideModifica();

		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}

	}

	private void richiediModificaUtenteDMD() {
		try {
			final FacesContext context = FacesContext.getCurrentInstance();
			if (!RubricaComponent.valida(getInserisciContattoItem(), context)) {
				showError("Non è possibile richiedere la modifica del contatto");
				return;
			}
			if (utente.getCheckUnivocitaMail()) {
				rubSRV.checkMailPerContattoGiaPresentePerAoo(getInserisciContattoItem(), true);
			}

			final NotificaContatto notifica = inserisciNotificaModifica();
			if (contattoVecchioItem != null) {
				rubSRV.notificaModificaContatto(notifica, contattoVecchioItem);
			} else {
				notifica.setOperazioneEnum(TipoOperazioneEnum.ELIMINA_CONTATTO);
				rubSRV.notificaEliminazioneContatto(notifica);
			}
			showInfoMessage("Richiesta di modifica contatto inviata");

		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento della notifica contatto", e);
			throw new RedException(e);
		}
	}

	private NotificaContatto inserisciNotificaModifica() {
		final NotificaContatto notifica = new NotificaContatto();
		notifica.setNuovoContatto(getInserisciContattoItem());
		notifica.setTipoNotifica(NotificaContatto.TIPO_NOTIFICA_RICHIESTA);
		notifica.setAlias(getInserisciContattoItem().getAliasContatto());
		notifica.setCognome(getInserisciContattoItem().getCognome());
		notifica.setNome(getInserisciContattoItem().getNome());
		notifica.setDataOperazione(new Date());
		notifica.setIdContatto(getInserisciContattoItem().getContattoID());
		notifica.setIdNodoModifica(utente.getIdUfficio());
		notifica.setIdUtenteModifica(utente.getId());
		notifica.setNote(notaRichiestaModifica);
		notifica.setTipologiaContatto(getInserisciContattoItem().getTipoRubrica());
		notifica.setUtente(utente.getUsername());
		notifica.setStato(StatoNotificaContattoEnum.IN_ATTESA);
		notifica.setIdAoo(utente.getIdAoo());
		return notifica;
	}

	private void modificaUtente() {
		RubricaComponent.modificaContatto(getInserisciContattoItem(), utente);
		if (renderInoltra || renderInoltraProt) {
			aggiungiContattoAiDestinatariInoltraProt();
		}
	}

	/**
	 * Effettua la modifica del destinatario.
	 * 
	 * @param modContatto
	 * @param activeIndexNum
	 */
	public void modificaDestinatario(final Contatto modContatto, final int activeIndexNum) {
		if (isPermessoUtenteGestioneRichiesteRubrica()) {
			setInserisciContattoItem(modContatto);
		} else {
			final Contatto richiediModifica = new Contatto();
			richiediModifica.copyFromContatto(modContatto);
			setInserisciContattoItem(richiediModifica);
			contattoVecchioItem = new Contatto();
			contattoVecchioItem.copyFromContatto(modContatto);
		}

		setInModificaDaShortcut(true);
		final TabView tb = (TabView) FacesContext.getCurrentInstance().getViewRoot().findComponent(MITTENTE_MAIL_PREF_TABS_DMD_RESOURCE_PATH);
		tb.setActiveIndex(activeIndexNum);

	}

	/**
	 * SHORTCUT CREAZIONE CONTATTI END
	 ************************************************************************/

	/**
	 * SHORTCUT RICERCA CONTATTI START
	 **********************************************************************/
	public void resetCampiRubricaRicerca() {
		final RicercaRubricaDTO r = new RicercaRubricaDTO();
		r.setRicercaRED(getRicercaRubricaDTO().getRicercaRED());
		r.setRicercaIPA(getRicercaRubricaDTO().getRicercaIPA());
		r.setRicercaMEF(getRicercaRubricaDTO().getRicercaMEF());
		r.setRicercaRubricaUfficio(getRicercaRubricaDTO().getRicercaRubricaUfficio());
		r.setIsUfficio(true);
		setRicercaRubricaDTO(r);
	}

	/**
	 * Gestisce la ricerca del contatto in rubrica.
	 */
	public void cercaContattoRubrica() {
		try {
			final IRubricaFacadeSRV rubricaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaFacadeSRV.class);
			final RicercaRubricaDTO ricercaRubrica = getRicercaRubricaDTO();
			ricercaRubrica.setIdAoo(Integer.parseInt("" + utente.getIdAoo()));
			if ((ricercaRubrica.getRicercaRED() || ricercaRubrica.getRicercaIPA()) && Boolean.TRUE.equals(ricercaRubrica.getRicercaMEF())) {
				ricercaRubrica.setIsUfficio(null);
			}
			final DataTable table = getDataTableByIdComponent(ID_RIC_RUBRICA_DMDRC_HTML);
			if (table != null) {
				table.reset();
			}
			List<Contatto> listContattiRicerca = null;
			if (!StringUtils.isNullOrEmpty(ricercaRubrica.getQueryRicerca())) {
				if (ricercaRubrica.getRicercaRED() || ricercaRubrica.getRicercaIPA()) {
					final boolean ricercaMEF = ricercaRubrica.getRicercaRED();
					listContattiRicerca = rubricaSRV.ricercaCampoSingolo(ricercaRubrica.getQueryRicerca(), utente.getIdUfficio(), utente.getIdAoo().intValue(),
							ricercaRubrica.getRicercaRED(), ricercaRubrica.getRicercaIPA(), ricercaMEF, false, "");
				} else if (Boolean.TRUE.equals(ricercaRubrica.getRicercaRubricaUfficio())) {
					listContattiRicerca = matchQuery(contattiPreferiti, ricercaRubrica.getQueryRicerca());
				}
			}
			setResultRicercaContatti(listContattiRicerca);
			setResultRicercaContattiFiltered(getResultRicercaContatti());

			FacesHelper.update("eastSectionForm:idMittenteMailPrefTabs_DMD:idResults_DMDRC");
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Aggiunge il contatto identificato dall'index all lista {@link #destinatari}.
	 * 
	 * @param index
	 */
	public void addDestinatarioFromRicercaRubrica(final Object index) {
		try {
			final Integer i = (Integer) index;
			final Contatto c = getResultRicercaContattiFiltered().get(i);
			if (c == null) {
				showError("Contatto non trovato.");
				return;
			}

			c.setMailSelected(c.getMailPec());
			if (StringUtils.isNullOrEmpty(c.getMailSelected())) {
				c.setMailSelected(c.getMail());
			}
			// Serve per disabilitare il tasto aggiungi, altrimenti può essere cliccato più
			// volte
			c.setDisabilitaElemento(true);

			this.destinatari = getDestinatariInModificaPuliti();

			for (final DestinatarioRedDTO d : destinatari) {
				if (c.getContattoID() != null && d.getContatto() != null && c.getContattoID().equals(d.getContatto().getContattoID())) {
					showWarnMessage("Il contatto è già stato inserito tra i destinatari!");
					return;
				}
			}

			final DestinatarioRedDTO d = new DestinatarioRedDTO();
			setContattoDestinatario(c, d);
			this.destinatari.add(d);

		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Definisce il contatto identificato dall'index come
	 * {@link #contattoProtocollaSelezionato}.
	 * 
	 * @param index
	 */
	public void addMittenteFromRicercaRubrica(final Object index) {
		try {
			final Integer i = (Integer) index;
			final Contatto c = getResultRicercaContattiFiltered().get(i);
			if (c == null) {
				showError("Contatto non trovato.");
				return;
			}

			contattoProtocollaSelezionato = c;
			impostaContattoeProcedi();
			inModificaDaShortcut = false;

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());
		}
	}

	/**
	 * Rimuove il contatto selezionato sul datatable dalla lista dei preferiti.
	 * 
	 * @param event
	 */
	public void rimuoviPreferito(final ActionEvent event) {
		try {
			final Contatto contatto = getDataTableClickedRow(event);

			final IRubricaFacadeSRV rubricaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaFacadeSRV.class);

			rubricaSRV.rimuoviPreferito(utente.getIdUfficio(), contatto.getContattoID(), contatto.getTipoRubricaEnum());
			contatto.setIsPreferito(false);
			int index = 0;
			for (final Contatto c : getContattiPreferiti()) {
				if (c.getContattoID().equals(contatto.getContattoID())) {
					break;
				}
				index++;
			}

			getContattiPreferiti().remove(index);
			setContattiPreferitiFiltered(getContattiPreferiti());
			DataTable table = null;
			
			table = getDataTableByIdComponent(PREFERITIMITTENTI_DMD_RESOURCE_PATH);
			
            if (table != null) {
            	table.reset();
            	FacesHelper.update(PREFERITIMITTENTI_DMD_RESOURCE_PATH);
            }
            
        
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Aggiunge il contatto selezionato sul datatable alla lista dei preferiti.
	 * 
	 * @param event
	 */
	public void aggiungiPreferito(final ActionEvent event) {
		try {
			final Contatto contatto = getDataTableClickedRow(event);

			final IRubricaFacadeSRV rubricaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaFacadeSRV.class);

			rubricaSRV.aggiungiAiPreferiti(utente.getIdUfficio(), contatto.getContattoID(), contatto.getTipoRubricaEnum(), utente.getId());
			contatto.setIsPreferito(true);
			if (getContattiPreferiti() == null) {
				setContattiPreferiti(new ArrayList<>());
			}
			getContattiPreferiti().add(contatto);
			setContattiPreferitiFiltered(getContattiPreferiti());

			DataTable table = null;
			
		
			table = getDataTableByIdComponent(PREFERITIMITTENTI_DMD_RESOURCE_PATH);
			
            if (table != null) {
            	table.reset();
            	FacesHelper.update(PREFERITIMITTENTI_DMD_RESOURCE_PATH);
            }
        	
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Metodo per recuperare il valore corretto da esportare
	 * 
	 * @param column
	 * @return
	 */
	public String contenutoCorrettoColonna(final UIColumn column) {
		return ExportUtils.columnCorrectValue(column);
	}

	/**
	 * SHORTCUT RICERCA CONTATTI END
	 ************************************************************************/

	/**
	 * @param protocolloGenerato
	 */
	public final String goToProtocolloGenerato(final String protocolloGenerato) {
		final ParamsRicercaAvanzataDocDTO paramsRicerca = sessionBean.getRicercaFormContainer().getDocumentiRed();

		final String[] protocolloDocumento = protocolloGenerato.split("/");

		// Trattandosi di documenti creati dalla protocollazione di una mail,
		// l'anno di creazione del documento corrisponderà all'anno di protocollazione
		final int annoProtocollo = Integer.parseInt(protocolloDocumento[1]);
		paramsRicerca.setAnnoDocumento(annoProtocollo);
		paramsRicerca.setNumeroProtocolloDa(Integer.valueOf(protocolloDocumento[0]));
		paramsRicerca.setNumeroProtocolloA(Integer.valueOf(protocolloDocumento[0]));

		final Calendar calProtocollo = Calendar.getInstance();
		calProtocollo.set(annoProtocollo, 0, 1, 0, 0, 0); // 01/01/ANNO_PROTOCOLLO 00:00:00
		paramsRicerca.setDataProtocolloDa(calProtocollo.getTime());

		calProtocollo.set(annoProtocollo, 11, 31, 23, 59, 59); // 31/12/ANNO_PROTOCOLLO 23:59:59
		paramsRicerca.setDataProtocolloA(calProtocollo.getTime());

		return sessionBean.ricercaDocumentiRed("Il documento non si trova nel cono di visibilità dell'utente.");
	}

	/**
	 * Restituisce i destinatari in modifica puliti.
	 * 
	 * @return lista dei destinatari
	 */
	public List<DestinatarioRedDTO> getDestinatariInModificaPuliti() {
		final List<DestinatarioRedDTO> listaPulita = new ArrayList<>();
		if (destinatari != null) {
			for (final DestinatarioRedDTO dest : destinatari) {
				if (dest != null && dest.getContatto() != null
						&& (!StringUtils.isNullOrEmpty(dest.getContatto().getAliasContatto()) || !StringUtils.isNullOrEmpty(dest.getContatto().getMailSelected()))) {
					listaPulita.add(dest);
				}
			}
		}

		return listaPulita;
	}

	/**
	 * Restitusice il testo.
	 * 
	 * @return testo
	 */
	public List<String> getTesto() {
		return testo;
	}

	/**
	 * Imposta il testo.
	 * 
	 * @param testo
	 */
	public void setTesto(final List<String> testo) {
		this.testo = testo;
	}

	/**
	 * Restituisce il testo SC.
	 * 
	 * @return testoSc
	 */
	public Integer getTestoSc() {
		return testoSc;
	}

	/**
	 * Imposta il testo SC.
	 * 
	 * @param testoSc
	 */
	public void setTestoSc(final Integer testoSc) {
		this.testoSc = testoSc;
	}

	/**
	 * Restituisce la modalita di spedizione SC.
	 * 
	 * @return modalita spedizione
	 */
	public Integer getModalitaSpedizioneSc() {
		return modalitaSpedizioneSc;
	}

	/**
	 * Imposta la modalita spedizione SC.
	 * 
	 * @param modalitaSpedizioneSc
	 */
	public void setModalitaSpedizioneSc(final Integer modalitaSpedizioneSc) {
		this.modalitaSpedizioneSc = modalitaSpedizioneSc;
	}

	/**
	 * Restituisce la map che definisce la spedizione.
	 * 
	 * @return hashmap spedizione
	 */
	public Map<String, Integer> getSpedizione() {
		return spedizione;
	}

	/**
	 * Imposta la spedizione.
	 */
	public void setSpedizione(final Map<String, Integer> spedizione) {
		this.spedizione = spedizione;
	}

	/**
	 * Restituisce il testo che definisce il messaggio.
	 * 
	 * @return testo messaggio
	 */
	public String getTestoMessaggio() {
		return testoMessaggio;
	}

	/**
	 * Imposta il testo che definisce il messaggio.
	 * 
	 * @param testoMessaggio
	 */
	public void setTestoMessaggio(final String testoMessaggio) {
		this.testoMessaggio = testoMessaggio;
	}

	/**
	 * Restituisce il testo associato al rifiuto.
	 * 
	 * @return testo rifiuto
	 */
	public String getTestoRifiuto() {
		return testoRifiuto;
	}

	/**
	 * Imposta il testo associato al rifiuto.
	 * 
	 * @param testoRifiuto
	 */
	public void setTestoRifiuto(final String testoRifiuto) {
		this.testoRifiuto = testoRifiuto;
	}

	/**
	 * Restituisce il testo associato al rifiuto SC.
	 * 
	 * @return testo rifiuto SC
	 */
	public Integer getTestoRifiutoSc() {
		return testoRifiutoSc;
	}

	/**
	 * Imposta il testo associato al rifiuto SC.
	 * 
	 * @param testoRifiutoSc
	 */
	public void setTestoRifiutoSc(final Integer testoRifiutoSc) {
		this.testoRifiutoSc = testoRifiutoSc;
	}

	/**
	 * Restituisce le motivazioni del rifiuto.
	 * 
	 * @return motivi rifiuto
	 */
	public Collection<TestoPredefinitoDTO> getMotiviRifiuto() {
		return motiviRifiuto;
	}

	/**
	 * Imposta le motivazioni del rifiuto.
	 * 
	 * @param motiviRifiuto
	 */
	public void setMotiviRifiuto(final Collection<TestoPredefinitoDTO> motiviRifiuto) {
		this.motiviRifiuto = motiviRifiuto;
	}

	/**
	 * Restituisce true se inserire gli allegati originali, false altrimenti.
	 * 
	 * @return true se inserire gli allegati originali, false altrimenti
	 */
	public boolean isInserisciAllegatiOriginaliFlag() {
		return inserisciAllegatiOriginaliFlag;
	}

	/**
	 * Imposta il flag: inserisciAllegatiOriginaliFlag.
	 * 
	 * @param inserisciAllegatiOriginaliFlag
	 */
	public void setInserisciAllegatiOriginaliFlag(final boolean inserisciAllegatiOriginaliFlag) {
		this.inserisciAllegatiOriginaliFlag = inserisciAllegatiOriginaliFlag;
	}

	/**
	 * Restituisce la stringa di ricerca.
	 * 
	 * @return searchString
	 */
	public String getSearchString() {
		return searchString;
	}

	/**
	 * Imposta la stringa di ricerca.
	 * 
	 * @param searchString
	 */
	public void setSearchString(final String searchString) {
		this.searchString = searchString;
	}

	/**
	 * Restituisce la data di partenza della ricerca.
	 * 
	 * @return data di partenza range di ricerca
	 */
	public Date getSearchDataDa() {
		return searchDataDa;
	}

	/**
	 * Imposta la data di partenza del range di ricerca.
	 * 
	 * @param searchDataDa
	 */
	public void setSearchDataDa(final Date searchDataDa) {
		this.searchDataDa = searchDataDa;
	}

	/**
	 * Restituisce la data finale del range della ricerca.
	 * 
	 * @return data termine range ricerca
	 */
	public Date getSearchDataA() {
		return searchDataA;
	}

	/**
	 * Imposta la data finale del range di ricerca.
	 * 
	 * @param searchDataA
	 */
	public void setSearchDataA(final Date searchDataA) {
		this.searchDataA = searchDataA;
	}

	/**
	 * Restituisce true se le date sono errate.
	 * 
	 * @return ture se date errate, false altrimenti.
	 */
	public Boolean getWrongDates() {
		return wrongDates;
	}

	/**
	 * Imposta la validita delle date.
	 * 
	 * @param wrongDates
	 */
	public void setWrongDates(final Boolean wrongDates) {
		this.wrongDates = wrongDates;
	}

	/**
	 * Restituisce: ricercaTabFlag.
	 * 
	 * @return ricercaTabFlag
	 */
	public Boolean getricercaTabFlag() {
		return ricercaTabFlag;
	}

	/**
	 * Imposta ricercaTabFlag.
	 * 
	 * @param ricercaTabFlag
	 */
	public void setricercaTabFlag(final Boolean ricercaTabFlag) {
		this.ricercaTabFlag = ricercaTabFlag;
	}

	/**
	 * Restituisce il datatable helper per gli altri account.
	 * 
	 * @return datatable helper
	 */
	public DataTableHelper<CasellaPostaDTO, CasellaPostaDTO> getOtherAccountsDTH() {
		return otherAccountsDTH;
	}

	/**
	 * Imposta il datatable helper per gli altri account.
	 * 
	 * @param otherAccountsDTH
	 */
	public void setOtherAccountsDTH(final DataTableHelper<CasellaPostaDTO, CasellaPostaDTO> otherAccountsDTH) {
		this.otherAccountsDTH = otherAccountsDTH;
	}

	/**
	 * Restituisce il contatto da inserire.
	 * 
	 * @return contatto da inserire
	 */
	public Contatto getinserisciContattoCasellaPostaleItem() {
		return inserisciContattoCasellaPostaleItem;
	}

	/**
	 * Imposta il contatto da inserire.
	 * 
	 * @param inserisciContattoCasellaPostaleItem
	 */
	public void setinserisciContattoCasellaPostaleItem(final Contatto inserisciContattoCasellaPostaleItem) {
		this.inserisciContattoCasellaPostaleItem = inserisciContattoCasellaPostaleItem;
	}

	/**
	 * Restituisce il dettaglio del documento.
	 * 
	 * @return dettaglio documento
	 */
	public DetailDocumentRedDTO getDetailsProt() {
		return detailsProt;
	}

	/**
	 * Imposta il dettaglio del documento.
	 * 
	 * @param detailsProt
	 */
	public void setDetailsProt(final DetailDocumentRedDTO detailsProt) {
		this.detailsProt = detailsProt;
	}

	/**
	 * Restituisce true se renderizzare il protocollo, false altrimenti.
	 * 
	 * @return true se renderizzare il protocollo, false altrimenti
	 */
	public Boolean getRenderProtocol() {
		return renderProtocol;
	}

	/**
	 * Imposta la visibilita del protocollo.
	 * 
	 * @param renderProtocol
	 */
	public void setRenderProtocol(final Boolean renderProtocol) {
		this.renderProtocol = renderProtocol;
	}

	/**
	 * Restituisce true se selezionabile, false altrimenti.
	 * 
	 * @return true se selezionabile, false altrimenti
	 */
	public Boolean getSelectable() {
		return selectable;
	}

	/**
	 * Imposta il flag associato alla selezione.
	 * 
	 * @param selectable
	 */
	public void setSelectable(final Boolean selectable) {
		this.selectable = selectable;
	}

	/**
	 * Restituisce il file principale.
	 * 
	 * @return file principale
	 */
	public String getFilePrincipale() {
		return filePrincipale;
	}

	/**
	 * Imposta il file principale.
	 * 
	 * @param filePrincipale
	 */
	public void setFilePrincipale(final String filePrincipale) {
		this.filePrincipale = filePrincipale;
	}

	/**
	 * Restituisce l'action.
	 * 
	 * @return MailActionEnum
	 */
	public MailActionEnum getAction() {
		return action;
	}

	/**
	 * Imposta l'action.
	 * 
	 * @see MailActionEnum.
	 * @param action
	 */
	public void setAction(final MailActionEnum action) {
		this.action = action;
	}

	/**
	 * Restituisce la casella mail selezionata.
	 * 
	 * @return casella mail selezionata
	 */
	public final CasellaMailStatoDTO getCartellaMailSelected() {
		return cartellaMailSelected;
	}

	/**
	 * Restotiosce il testo corrente delle note multi.
	 * 
	 * @return testo
	 */
	public String getCurrentTextNoteMulti() {
		return currentTextNoteMulti;
	}

	/**
	 * Imposta il testo corrente delle note multi.
	 * 
	 * @param currentTextNoteMulti
	 */
	public void setCurrentTextNoteMulti(final String currentTextNoteMulti) {
		this.currentTextNoteMulti = currentTextNoteMulti;
	}

	/**
	 * Restituisce true se il tab rubrica mail è visibile, false altrimenti.
	 * 
	 * @return true se il tab rubrica mail è visibile, false altrimenti
	 */
	public boolean isShowTabRubricaMail() {
		return showTabRubricaMail;
	}

	/**
	 * Imposta la visibilita del tab rubrica mail.
	 * 
	 * @param showTabRubricaMail
	 */
	public void setShowTabRubricaMail(final boolean showTabRubricaMail) {
		this.showTabRubricaMail = showTabRubricaMail;
	}

	/**
	 * Restituisce l'id del contatto scelto.
	 * 
	 * @return id contatto
	 */
	public Long getIdContattoScelto() {
		return idContattoScelto;
	}

	/**
	 * Imposta l'id del contatto scelto.
	 * 
	 * @param idContattoScelto
	 */
	public void setIdContattoScelto(final Long idContattoScelto) {
		this.idContattoScelto = idContattoScelto;
	}

	/**
	 * @return callBackToAction
	 */
	public boolean isCallBackToAction() {
		return callBackToAction;
	}

	/**
	 * Imposta callBackToAction.
	 * 
	 * @param callBackToAction
	 */
	public void setCallBackToAction(final boolean callBackToAction) {
		this.callBackToAction = callBackToAction;
	}

	/**
	 * Restituisce true se l'utente ha il permesso protocolla DSR.
	 * 
	 * @return true se l'utente ha il permesso protocolla DSR, false altrimenti
	 */
	public boolean isPermessoProtocollaDSR() {
		return permessoProtocollaDSR;
	}

	/**
	 * Imposta il flag associato al permesso protocolla DSR.
	 * 
	 * @param permessoProtocollaDSR
	 */
	public void setPermessoProtocollaDSR(final boolean permessoProtocollaDSR) {
		this.permessoProtocollaDSR = permessoProtocollaDSR;
	}

	/**
	 * Restituisce il dettaglio della mail corrente.
	 * 
	 * @return dettaglio mail
	 */
	public final DetailEmailDTO getCurrentmail() {
		return currentmail;
	}

	/**
	 * Imposta il dettaglio della mail corrente.
	 * 
	 * @param currentmail
	 */
	public final void setCurrentmail(final DetailEmailDTO currentmail) {
		this.currentmail = currentmail;
	}

	/**
	 * Restituisce true se "protocolla" è visibile, false altrimenti.
	 * 
	 * @return true se "protocolla" è visibile, false altrimenti
	 */
	public boolean isRenderProtocolla() {
		return renderProtocolla;
	}

	/**
	 * Imposta la visibilita di "Protocolla".
	 * 
	 * @param renderProtocolla
	 */
	public void setRenderProtocolla(final boolean renderProtocolla) {
		this.renderProtocolla = renderProtocolla;
	}

	/**
	 * Restituisce true se visualizzabile "Crea contatto".
	 * 
	 * @return true se visibile, false altrimenti
	 */
	public boolean isVisualizzaCreaContatto() {
		return visualizzaCreaContatto;
	}

	/**
	 * Imposta la visibilita di "Crea Contatto".
	 * 
	 * @param visualizzaCreaContatto
	 */
	public void setVisualizzaCreaContatto(final boolean visualizzaCreaContatto) {
		this.visualizzaCreaContatto = visualizzaCreaContatto;
	}

	/**
	 * Restituisce true se "Inoltra" visibile.
	 * 
	 * @return true se "Inoltra" visibile, false altrimenti
	 */
	public boolean isRenderInoltra() {
		return renderInoltra;
	}

	/**
	 * Imposta la visibilita di "Inoltra".
	 * 
	 * @param renderInoltra
	 */
	public void setRenderInoltra(final boolean renderInoltra) {
		this.renderInoltra = renderInoltra;
	}

	/**
	 * Restituisce true se "Rifiuta" visibile.
	 * 
	 * @return true se "Rifiuta" visibile, false altrimenti
	 */
	public boolean isRenderRifiuta() {
		return renderRifiuta;
	}

	/**
	 * Imposta la visibilita di "Rifiuta".
	 * 
	 * @param renderRifiuta
	 */
	public void setRenderRifiuta(final boolean renderRifiuta) {
		this.renderRifiuta = renderRifiuta;
	}

	/**
	 * Restituisce il contatto protocolla selezionato.
	 * 
	 * @return contatto protocolla
	 */
	public Contatto getContattoProtocollaSelezionato() {
		return contattoProtocollaSelezionato;
	}

	/**
	 * Imposta il contatto protocolla.
	 * 
	 * @param contattoProtocollaSelezionato
	 */
	public void setContattoProtocollaSelezionato(final Contatto contattoProtocollaSelezionato) {
		this.contattoProtocollaSelezionato = contattoProtocollaSelezionato;
	}

	/**
	 * Restituisce il component per la gestione della selezione principale allegato.
	 * 
	 * @return il component
	 */
	public SelezionaPrincipaleAllegatoComponent getTreeTableComponent() {
		return treeTableComponent;
	}

	/**
	 * Restituisce il campo da ordinare.
	 * 
	 * @return campoDaOrdinare
	 */
	public String getCampoDaOrdinare() {
		return campoDaOrdinare;
	}

	/**
	 * Imposta campoDaOridnare.
	 * 
	 * @param campoDaOrdinare
	 */
	public void setCampoDaOrdinare(final String campoDaOrdinare) {
		this.campoDaOrdinare = campoDaOrdinare;
	}

	/**
	 * Restituisce il component per la gestione della dettaglio preview intro.
	 * 
	 * @return il component
	 */
	public final DetailPreviewIntroComponent getPreviewComponent() {
		return previewComponent;
	}

	/**
	 * Imposta il component per la gestione della dettaglio preview intro.
	 * 
	 * @param previewComponent
	 */
	public final void setPreviewComponent(final DetailPreviewIntroComponent previewComponent) {
		this.previewComponent = previewComponent;
	}

	/**
	 * Restituisce l'abilitazione inoltra.
	 * 
	 * @return abilitazioneInoltra
	 */
	public boolean isAbilitazioneInoltra() {
		return abilitazioneInoltra;
	}

	/**
	 * Restituisce l'abilitazione Inoltra Con Protocollo.
	 * 
	 * @return abilitazioneInoltraConProtocollo
	 */
	public boolean isAbilitazioneInoltraConProtocollo() {
		return abilitazioneInoltraConProtocollo;
	}

	/**
	 * Restituisce l'abilitazione Rifiuta.
	 * 
	 * @return abilitazioneRifiuta
	 */
	public boolean isAbilitazioneRifiuta() {
		return abilitazioneRifiuta;
	}

	/**
	 * Restituisce l'abilitazione rifiuta con protocollo.
	 * 
	 * @return abilitazioneRifiutaConProtocollo
	 */
	public boolean isAbilitazioneRifiutaConProtocollo() {
		return abilitazioneRifiutaConProtocollo;
	}

	/**
	 * Restituisce le motivazioni di rifiuto con protocollo.
	 * 
	 * @return motivi rifiuto con protocollo
	 */
	public Collection<TestoPredefinitoDTO> getMotiviRifiutoConProtocollo() {
		return motiviRifiutoConProtocollo;
	}

	/**
	 * Restituisce l'id del testo rifiuto.
	 * 
	 * @return id testo rifiuto
	 */
	public Long getIdTestoRifiutoProt() {
		return idTestoRifiutoProt;
	}

	/**
	 * Imposta l'id del testo rifiuto prot.
	 * 
	 * @param idTestoRifiutoProt
	 */
	public void setIdTestoRifiutoProt(final Long idTestoRifiutoProt) {
		this.idTestoRifiutoProt = idTestoRifiutoProt;
	}

	/**
	 * Restituisce il testo rifiuto prot.
	 * 
	 * @return testo rifiuto
	 */
	public String getTestoRifiutoProt() {
		return testoRifiutoProt;
	}

	/**
	 * Imposta il testo rifiuto prot.
	 * 
	 * @param testoRifiutoProt
	 */
	public void setTestoRifiutoProt(final String testoRifiutoProt) {
		this.testoRifiutoProt = testoRifiutoProt;
	}

	/**
	 * Restituisce i motivi di inoltro.
	 * 
	 * @return motivi inoltro
	 */
	public Collection<TestoPredefinitoDTO> getMotiviInoltro() {
		return motiviInoltro;
	}

	/**
	 * Restituisce la lista del motivi con protocollo.
	 * 
	 * @return motivi con protocollo
	 */
	public Collection<TestoPredefinitoDTO> getMotiviInoltroConProtocollo() {
		return motiviInoltroConProtocollo;
	}

	/**
	 * Restituisce l'id del testo inoltro prot.
	 * 
	 * @return id testo inoltro
	 */
	public Long getIdTestoInoltroProt() {
		return idTestoInoltroProt;
	}

	/**
	 * Imposta l'id del testo inoltro prot.
	 * 
	 * @param idTestoInoltroProt
	 */
	public void setIdTestoInoltroProt(final Long idTestoInoltroProt) {
		this.idTestoInoltroProt = idTestoInoltroProt;
	}

	/**
	 * Restituisce il testo inoltro prot.
	 * 
	 * @return testo inoltro
	 */
	public String getTestoInoltroProt() {
		return testoInoltroProt;
	}

	/**
	 * Imposta il testo inoltro prot.
	 * 
	 * @param testoInoltroProt
	 */
	public void setTestoInoltroProt(final String testoInoltroProt) {
		this.testoInoltroProt = testoInoltroProt;
	}

	/**
	 * Restituisce i contatti TO inoltra prot.
	 * 
	 * @return lista contatti TO
	 */
	public List<Contatto> getContattiToInoltraProt() {
		return contattiToInoltraProt;
	}

	/**
	 * Restituisce i contatti CC inoltra prot.
	 * 
	 * @return lista contatti CC
	 */
	public List<Contatto> getContattiCCInoltraProt() {
		return contattiCCInoltraProt;
	}

	/**
	 * Restituisce la lista delle modalita destinatario per il popolamento della
	 * combobox.
	 * 
	 * @return lista modalita destinatario
	 */
	public List<ModalitaDestinatarioEnum> getComboModalitaDestinatario() {
		return comboModalitaDestinatario;
	}

	/**
	 * Imposta la lista delle modalita di destinatario per la combobox.
	 * 
	 * @param comboModalitaDestinatario
	 */
	public void setComboModalitaDestinatario(final List<ModalitaDestinatarioEnum> comboModalitaDestinatario) {
		this.comboModalitaDestinatario = comboModalitaDestinatario;
	}

	/**
	 * Restituisce il testo corrente delle note ispeziona.
	 * 
	 * @return testo note
	 */
	public String getCurrentTextNoteIspeziona() {
		return currentTextNoteIspeziona;
	}

	/**
	 * Imposta il testo delle note ispeziona.
	 * 
	 * @param currentTextNoteIspeziona
	 */
	public void setCurrentTextNoteIspeziona(final String currentTextNoteIspeziona) {
		this.currentTextNoteIspeziona = currentTextNoteIspeziona;
	}

	/**
	 * Restituisce il contatto da inserire.
	 * 
	 * @return contatto
	 */
	public Contatto getInserisciContattoItem() {
		return inserisciContattoItem;
	}

	/**
	 * Imposta il contatto da inserire.
	 * 
	 * @param inserisciContattoItem
	 */
	public void setInserisciContattoItem(final Contatto inserisciContattoItem) {
		this.inserisciContattoItem = inserisciContattoItem;
	}

	/**
	 * Restituisce true se la dialog di modifica è stata raggiunta da shortcut,
	 * false altrimenti.
	 * 
	 * @return true se la dialog di modifica è stata raggiunta da shortcut, false
	 *         altrimenti
	 */
	public boolean isInModificaDaShortcut() {
		return inModificaDaShortcut;
	}

	/**
	 * Imposta il flag inModificaDaShortcut.
	 * 
	 * @param inModificaDaShortcut
	 */
	public void setInModificaDaShortcut(final boolean inModificaDaShortcut) {
		this.inModificaDaShortcut = inModificaDaShortcut;
	}

	/**
	 * Restituisce la lista dei preferiti.
	 * 
	 * @return lista contatti preferiti
	 */
	public List<Contatto> getContattiPreferiti() {
		return contattiPreferiti;
	}

	/**
	 * Imposta la lista dei preferiti.
	 * 
	 * @param contattiPreferiti
	 */
	public void setContattiPreferiti(final List<Contatto> contattiPreferiti) {
		this.contattiPreferiti = contattiPreferiti;
	}

	/**
	 * Restituisce i risultati della ricerca sui contatti.
	 * 
	 * @return lista di contatti ricerca
	 */
	public List<Contatto> getResultRicercaContatti() {
		return resultRicercaContatti;
	}

	/**
	 * Imposta la lista dei contatti ottenuti in fase di ricerca.
	 * 
	 * @param resultRicercaContatti
	 */
	public void setResultRicercaContatti(final List<Contatto> resultRicercaContatti) {
		this.resultRicercaContatti = resultRicercaContatti;
	}

	/**
	 * Restituisce il DTO che gestisce i parametri della ricerca.
	 * 
	 * @return parametri della ricerca
	 */
	public RicercaRubricaDTO getRicercaRubricaDTO() {
		return ricercaRubricaDTO;
	}

	/**
	 * Imposta i parametri della ricerca.
	 * 
	 * @param ricercaRubricaDTO
	 */
	public void setRicercaRubricaDTO(final RicercaRubricaDTO ricercaRubricaDTO) {
		this.ricercaRubricaDTO = ricercaRubricaDTO;
	}

	/**
	 * Restituisce la lista dei contatti preferiti filtrati.
	 * 
	 * @return lista contatti
	 */
	public List<Contatto> getContattiPreferitiFiltered() {
		return contattiPreferitiFiltered;
	}

	/**
	 * Imposta la lista dei contatti preferiti filtrati.
	 * 
	 * @param contattiPreferitiFiltered
	 */
	public void setContattiPreferitiFiltered(final List<Contatto> contattiPreferitiFiltered) {
		this.contattiPreferitiFiltered = contattiPreferitiFiltered;
	}

	/**
	 * Restituisce la lista dei contatti ottenuti in fase di ricerca filtrata.
	 * 
	 * @return lista contatti
	 */
	public List<Contatto> getResultRicercaContattiFiltered() {
		return resultRicercaContattiFiltered;
	}

	/**
	 * @param resultRicercaContattiFiltered
	 */
	public void setResultRicercaContattiFiltered(final List<Contatto> resultRicercaContattiFiltered) {
		this.resultRicercaContattiFiltered = resultRicercaContattiFiltered;
	}

	/**
	 * Restituisce la lista dei destinatari.
	 * 
	 * @return destinatari
	 */
	public List<DestinatarioRedDTO> getDestinatari() {
		return destinatari;
	}

	/**
	 * Imposta la lista dei destinatari.
	 * 
	 * @param destinatari
	 */
	public void setDestinatari(final List<DestinatarioRedDTO> destinatari) {
		this.destinatari = destinatari;
	}

	/**
	 * Restituisce true se l'utente detiene il permesso: GESTIONE_RUBRICA, false
	 * altrimenti.
	 * 
	 * @return true se l'utente detiene il permesso: GESTIONE_RUBRICA, false
	 *         altrimenti
	 */
	public boolean isPermessoUtenteGestioneRichiesteRubrica() {
		final Long permessi = utente.getPermessi();
		permessoUtenteGestioneRichiesteRubrica = PermessiUtils.hasPermesso(permessi, PermessiEnum.GESTIONE_RUBRICA);
		return permessoUtenteGestioneRichiesteRubrica;
	}

	/**
	 * Imposta il flag associato al permesso GESTIONE_RUBRICA.
	 * 
	 * @param permessoUtenteGestioneRichiesteRubrica
	 */
	public void setPermessoUtenteGestioneRichiesteRubrica(final boolean permessoUtenteGestioneRichiesteRubrica) {
		this.permessoUtenteGestioneRichiesteRubrica = permessoUtenteGestioneRichiesteRubrica;
	}

	/**
	 * Restituisce il colore corrente della nota multi.
	 * 
	 * @return colore nota
	 */
	public Integer getCurrentColorNoteMulti() {
		return currentColorNoteMulti;
	}

	/**
	 * Imposta il colore della nota multi.
	 * 
	 * @param currentColorNoteMulti
	 */
	public void setCurrentColorNoteMulti(final Integer currentColorNoteMulti) {
		this.currentColorNoteMulti = currentColorNoteMulti;
	}

	/**
	 * Restituisce il colore nota da ispeziona.
	 * 
	 * @return colore nota
	 */
	public Integer getColoreNotaDaIspeziona() {
		return coloreNotaDaIspeziona;
	}

	/**
	 * Imposta il colore nota da ispeziona.
	 * 
	 * @param coloreNotaDaIspeziona
	 */
	public void setColoreNotaDaIspeziona(final Integer coloreNotaDaIspeziona) {
		this.coloreNotaDaIspeziona = coloreNotaDaIspeziona;
	}

	/**
	 * Metodo per inizializzare la nota al click della response Protocolla.
	 * 
	 * @param comeFromTabMulti
	 */
	public void inizializzaNotaProtocolla(final String comeFromTabMulti) {
		inizializzaNotaIspeziona(comeFromTabMulti);
	}

	/**
	 * Aggiunge il mittente identificato dall'id contatto.
	 * {@link #impostaContattoeProcedi()}.
	 * 
	 * @param idContatto
	 */
	public void addMittenteProtocolla(final Long idContatto) {
		try {
			Contatto selezionato = null;
			for (final Contatto pref : this.getContattiProtocolla()) {
				if (pref.getContattoID().equals(idContatto)) {
					selezionato = pref;
					break;
				}
			}
			contattoProtocollaSelezionato = selezionato;
			impostaContattoeProcedi();
			inModificaDaShortcut = false;

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());
		}
	}

	/**
	 * Aggiunge il contatto definito dall'index come mittente protocolla ricerca.
	 * 
	 * @param index
	 */
	public void addMittenteProtocollaRicerca(final Object index) {
		try {
			Contatto selezionato = null;
			final Integer i = (Integer) index;
			selezionato = getResultRicercaContattiFiltered().get(i);

			contattoProtocollaSelezionato = selezionato;
			impostaContattoeProcedi();
			inModificaDaShortcut = false;

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());
		}
	}

	/**
	 * Restituisce true se "rifiuta prot" è renderizzato.
	 * 
	 * @return true se "rifiuta prot" è renderizzato, false altrimenti.
	 */
	public boolean isRenderRifiutaProt() {
		return renderRifiutaProt;
	}

	/**
	 * Imposta il flag: renderRifiutaProt.
	 * 
	 * @param renderRifiutaProt
	 */
	public void setRenderRifiutaProt(final boolean renderRifiutaProt) {
		this.renderRifiutaProt = renderRifiutaProt;
	}

	/**
	 * @return renderInoltraProt
	 */
	public boolean isRenderInoltraProt() {
		return renderInoltraProt;
	}

	private void protocollazioneSemiautomatica(final DetailEmailDTO mailSelected, final SegnaturaMessaggioInteropDTO segnaturaDTO,
			final List<AllegatoDTO> allegatiMailInterop) {
		try {
			if (segnaturaDTO != null) {
				contattoProtocolla = rubSRV.getContattoFromMailInterop(segnaturaDTO.getIndirizzoMailMittente());

				if (checkCurrentMail(mailSelected)) {
					if (contattoProtocolla != null) {
						popolaDettaglioProtAuto(mailSelected, contattoProtocolla, segnaturaDTO, allegatiMailInterop);
						setDocumentManagerBeanDetail(mailSelected);

						FacesHelper.executeJS(SHOW_PROTOCOLLAMAILWV_JS);
						FacesHelper.executeJS(WRAP_ADD_EVENT_LISTENER_FOR_PROT_MAIL);
					} else {
						showInfoMessage("Non è presente nessun contatto per questa mail. Inserisci un nuovo contatto.");
						inserisciContattoItem.setMail(mailSelected.getMittente());
						visualizzaCreaContatto = true;
						FacesHelper.executeJS(SHOW_AGGIUNGICONTATTODLGWV_JS);
						FacesHelper.update(AGGIUNGICONTATTODLG_RESOURCE_PATH);
						popolaDettaglioProtAuto(mailSelected, null, segnaturaDTO, allegatiMailInterop);
						setDocumentManagerBeanDetail(mailSelected);
						setRenderFromProtSemiautomatica(true);
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante la protocollazione semi-automatica: " + e);
		}
	}

	private void setDocumentManagerBeanDetail(final DetailEmailDTO mailSelected) {
		dmb.setChiamante(ConstantsWeb.MBean.MAIL_BEAN);
		dmb.setDetail(detailsProt);
		dmb.setDisabilitaButtonProtSemi(true);
		dmb.getDocumentManagerDTO().setDisabilitaProtSemi(true);
		if (mailSelected.getPreassegnatarioPerConoscenza() != null) {
			dmb.setAssegnatariPerConoscenza(mailSelected.getPreassegnatarioPerConoscenza());
		} else {
			dmb.setAssegnatariPerConoscenza(new ArrayList<>());
		}
		dmb.setAssegnatariPerContributo(new ArrayList<>());
		dmb.setAssegnatarioPrincipale(null);
		dmb.setDescrAssegnatarioPerCompetenza(null);

		if (mailSelected.getPreassegnatarioInterop() != null
				&& (mailSelected.getPreassegnatarioInterop().getIdUfficio() != null || mailSelected.getPreassegnatarioInterop().getDescrizioneUfficio() != null)) {
			dmb.setAssegnatarioPrincipale(mailSelected.getPreassegnatarioInterop().getAssegnazioneCompetenza());
			dmb.setDescrAssegnatarioPerCompetenza(dmb.getAssegnatarioPrincipale().getDescrizioneAssegnatario());
		}

		setRenderProtocol(false);
	}

	/**
	 * Metodo utile per popolare i campi relativi alla maschera di creazione del
	 * documento con le info che vengono fornite dalla segnatura.xml
	 */
	private void popolaDettaglioProtAuto(final DetailEmailDTO mailSelected, final Contatto contattoProtocolla, final SegnaturaMessaggioInteropDTO segnaturaDTO,
			final List<AllegatoDTO> allegatiMailInterop) {
		detailsProt = new DetailDocumentRedDTO();

		final List<AllegatoDTO> allegatoScompattato = new ArrayList<>();
		AllegatoDTO allegatoDTOCompleto = null;
		List<TipologiaDocumentoDTO> tipiDocAttive = null;
		if (allegatiMailInterop != null && !allegatiMailInterop.isEmpty()) {

			for (final AllegatoDTO allegato : allegatiMailInterop) {
				// Controllo tra gli allegati se presente il doc principale indicato dalla
				// segnatura
				if (!StringUtils.isNullOrEmpty(segnaturaDTO.getIdDocumentoPrincipale()) && segnaturaDTO.getIdDocumentoPrincipale().equals(allegato.getNomeFile())) {
					detailsProt.setNomeFile(allegato.getNomeFile());
					detailsProt.setMimeType(allegato.getMimeType());
					detailsProt.setContent(allegato.getContent());
					final String path = mailSelected.getGuid() + PATH_SEPARATOR + allegato.getGuid() + PATH_SEPARATOR + allegato.getNomeFile();
					detailsProt.setIdFilePrincipaleDaProtocolla(path);
				} else {

					// Controllo tra gli allegati della mail quelli indicati dalla segnatura
					if (segnaturaDTO.getAllegati().contains(allegato.getNomeFile())) {

						if (tipiDocAttive == null) {
							tipiDocAttive = tipologiaDocumentoSRV.getComboTipologieByAooAndTipoCategoriaAttivi(TipoCategoriaEnum.ENTRATA, utente.getIdAoo());
						}

						boolean mantieniFormatoOriginale = false;
						boolean disabilitaMantieniFormatoOriginale = false;
						if (!checkValidaPDF(allegato)) {
							mantieniFormatoOriginale = true;
							disabilitaMantieniFormatoOriginale = true;
						}

						allegatoDTOCompleto = scompattaMailSRV.populateAllegatoDTOInterop(utente, allegato, mailSelected, mantieniFormatoOriginale,
								disabilitaMantieniFormatoOriginale, null, tipiDocAttive);
						allegatoScompattato.add(allegatoDTOCompleto);
					}
				}
			}

			// Se nella segnatura non è indicato il documento principale, si imposta come
			// tale il testo della mail
			if (detailsProt.getContent() == null) {
				detailsProt.setNomeFile(DEFAULT_FILE_NAME);
				detailsProt.setMimeType("text/html");
				detailsProt.setContent(mailSelected.getTesto().getBytes());
				detailsProt.setIdFilePrincipaleDaProtocolla(DEFAULT_FILE_NAME);
			}

		} else {
			detailsProt.setNomeFile(DEFAULT_FILE_NAME);
			detailsProt.setMimeType("text/html");
			detailsProt.setContent(mailSelected.getTesto().getBytes());
			detailsProt.setIdFilePrincipaleDaProtocolla(DEFAULT_FILE_NAME);
		}

		detailsProt.setAllegati(allegatoScompattato);

		detailsProt.setProtocollazioneMailGuid(currentmail.getGuid());
		detailsProt.setProtocollazioneMailAccount(cpAttiva);
		if (contattoProtocolla != null) {
			detailsProt.setMittenteContatto(contattoProtocolla);
		}
		detailsProt.setOggetto(currentmail.getOggetto());
		detailsProt.setIsNotifica(notificaProtocolla);
		detailsProt.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0]); // 1

		final NotaDTO notaMail = currentmail.getNota();
		detailsProt.setContentNoteDaMailProtocollaDTO(notaMail);
		detailsProt.setPreassegnatarioInteropDTO(currentmail.getPreassegnatarioInterop());
	}

	/**
	 * Metodo usato per inizializzare il preassegnatario nella dialog della nota
	 * qualora venga valorizzato dal flusso interop
	 */
	private void inizializzaNotaPreassegnatarioGeneric(final String comeFromTabMulti) {

		flagInserisciNotaMulti = Boolean.valueOf(comeFromTabMulti);

		if (Boolean.TRUE.equals(flagInserisciNotaMulti)) {
			if (mailDTH.getSelectedMasters().size() == 1) {
				inizializzaNotaPreassegnatarioGenericSelected();
			}
		} else {
			inizializzaNotaPreassegnatarioGenericCurrent();
		}
	}

	/**
	 * Restituisce l'utente.
	 * 
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'utente.
	 * 
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Restituisce il dto per la gestione dell'organigramma interoperabilita.
	 * 
	 * @return dto organigramma interoperabilita
	 */
	public OrganigrammaInteroperabilitaDTO getOrganigrammaInteroperabilita() {
		return organigrammaInteroperabilita;
	}

	/**
	 * Imposta il dto organigramma interoperabilita.
	 * 
	 * @param organigrammaInteroperabilita
	 */
	public void setOrganigrammaInteroperabilita(final OrganigrammaInteroperabilitaDTO organigrammaInteroperabilita) {
		this.organigrammaInteroperabilita = organigrammaInteroperabilita;
	}

	/**
	 * Restituisce true se dirigente, false altrimenti.
	 * 
	 * @return true se dirigente, false altrimenti
	 */
	public boolean getIsDirigente() {
		return isDirigente;
	}

	/**
	 * Imposta il flag: isDirigente.
	 * 
	 * @param isDirigente
	 */
	public void setIsDirigente(final boolean isDirigente) {
		this.isDirigente = isDirigente;
	}

	/**
	 * Valorizza {@link #preassegnatarioInterop} recuperando informazioni da
	 * {@link #organigrammaInteroperabilita}.
	 * 
	 * @return AssegnatarioInteropDTO
	 */
	public AssegnatarioInteropDTO setPreassegnazioneInteroperabilita() {
		String descrizioneUfficio = null;
		String descrizioneUtente = null;
		Long idUfficio = null;
		Long idUtente = null;

		if (organigrammaInteroperabilita.getIdUfficio() != null) {
			// Ufficio
			idUfficio = organigrammaInteroperabilita.getIdUfficio();
			descrizioneUfficio = organigrammaInteroperabilita.getDescrizioneUfficio();

			// Utente
			if (organigrammaInteroperabilita.getIdUtente() != null) {
				idUtente = organigrammaInteroperabilita.getIdUtente();
				descrizioneUtente = organigrammaInteroperabilita.getDescrizioneUtente();
			}

			preassegnatarioInterop = new AssegnatarioInteropDTO(idUfficio, idUtente, descrizioneUfficio, descrizioneUtente);
		} else {
			preassegnatarioInterop = null;
		}

		return preassegnatarioInterop;
	}

	/**
	 * Inizializza la nota preassegnatario selezionato.
	 */
	public void inizializzaNotaPreassegnatarioGenericSelected() {
		String descrizionePreassegnatario = "";
		organigrammaInteroperabilita.setDescrizionePreassegnatario(descrizionePreassegnatario);
		if (mailDTH.getSelectedMasters().get(0).getPreassegnatarioInterop() != null) {
			descrizionePreassegnatario = mailDTH.getSelectedMasters().get(0).getPreassegnatarioInterop().getDescrizioneUfficio();
			inizializzaNotaPreassegnatarioUfficioUtente(descrizionePreassegnatario);
			if (mailDTH.getCurrentMaster().getPreassegnatarioInterop().getDescrizioneUtente() != null) {
				descrizionePreassegnatario += " - " + mailDTH.getCurrentMaster().getPreassegnatarioInterop().getDescrizioneUtente();
				inizializzaNotaPreassegnatarioUfficioUtente(descrizionePreassegnatario);
			}
		}
	}

	/**
	 * Inizializza la nota preassegnatario corrente.
	 */
	public void inizializzaNotaPreassegnatarioGenericCurrent() {
		String descrizionePreassegnatario = "";
		organigrammaInteroperabilita.setDescrizionePreassegnatario(descrizionePreassegnatario);
		if (mailDTH.getCurrentMaster().getPreassegnatarioInterop() != null) {
			descrizionePreassegnatario = mailDTH.getCurrentMaster().getPreassegnatarioInterop().getDescrizioneUfficio();
			inizializzaNotaPreassegnatarioUfficioUtente(descrizionePreassegnatario);
			if (mailDTH.getCurrentMaster().getPreassegnatarioInterop().getDescrizioneUtente() != null) {
				descrizionePreassegnatario += " - " + mailDTH.getCurrentMaster().getPreassegnatarioInterop().getDescrizioneUtente();
				inizializzaNotaPreassegnatarioUfficioUtente(descrizionePreassegnatario);
			}
		}
	}

	/**
	 * Inizializza nota preassegnatario ufficio utente.
	 * 
	 * @param descrizionePreassegnatario
	 */
	public void inizializzaNotaPreassegnatarioUfficioUtente(final String descrizionePreassegnatario) {
		organigrammaInteroperabilita.setDescrizionePreassegnatario(descrizionePreassegnatario);
		organigrammaInteroperabilita.setDescrizioneUfficio(descrizionePreassegnatario);
	}

	/**
	 * Restituisce true se visibile "FromProtSemiautomatica", false altrimenti.
	 * 
	 * @return true se visibile "FromProtSemiautomatica", false altrimenti
	 */
	public boolean getRenderFromProtSemiautomatica() {
		return renderFromProtSemiautomatica;
	}

	/**
	 * @param renderFromProtSemiautomatica
	 */
	public void setRenderFromProtSemiautomatica(final boolean renderFromProtSemiautomatica) {
		this.renderFromProtSemiautomatica = renderFromProtSemiautomatica;
	}

	/**
	 * Restituisce il nome file per l'export coda.
	 * 
	 * @return nome file
	 */
	public String getNomeFileExportCoda() {
		String nome = "";
		if (utente != null) {
			nome += utente.getNome().toLowerCase() + "_" + utente.getCognome().toLowerCase() + "_";
		}

		if (selectedState != null) {
			if (StatoMailEnum.INARRIVO.getStatus().intValue() == selectedState.intValue()) {
				nome += "postainarrivo_";
			} else if (StatoMailEnum.ELIMINATA.getStatus().intValue() == selectedState.intValue()) {
				nome += "postaeliminata_";
			} else {
				nome += "postainoltratarifiutata_";
			}
		}

		final SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH.mm");
		nome += sdf.format(new Date());

		return nome;
	}

	/**
	 * Restituisce la lista dei destinatari contenuti nella rubrica.
	 * 
	 * @param query
	 * @return lista destinatari
	 */
	public List<DestinatarioRedDTO> caricaRubricaMail(final String query) {
		final FacesContext context = FacesContext.getCurrentInstance();
		final Integer rowIndex = (Integer) UIComponent.getCurrentComponent(context).getAttributes().get("rowIndex");

		if (rowIndex != null && destinatari.get(rowIndex) != null && destinatari.get(rowIndex).getContatto() != null) {
			if (destinatari.get(rowIndex).getContatto() != null) {
				campoMailOnTheFly = destinatari.get(rowIndex).getContatto().getMailSelected();
			}

			if (destinatari.get(rowIndex).getContatto() != null) {
				destinatari.get(rowIndex).getContatto().setFromDialogCreazioneModifica(false);
			}

			destinatari.set(rowIndex, new DestinatarioRedDTO());
			gestisciContattoOnTheFlyDest(query, rowIndex, campoMailOnTheFly);
		} else {
			gestisciContattoOnTheFlyDest(query, rowIndex, campoMailOnTheFly);
		}
		List<Contatto> contatti;
		int contattiGiaInseritiNum = 0;
		final List<DestinatarioRedDTO> destinatariList = new ArrayList<>();
		if (!StringUtils.isNullOrEmpty(query) && query.length() > 2) {
			if (getDestinatariInModificaPuliti() != null && !getDestinatariInModificaPuliti().isEmpty()) {
				contattiGiaInseritiNum = getDestinatariInModificaPuliti().size();
			}
			final ArrayList<Long> contattiIdDaEscludere = new ArrayList<>();
			for (int i = 0; i < this.destinatari.size(); i++) {
				if (destinatari.get(i).getContatto().getContattoID() != null) {
					contattiIdDaEscludere.add(destinatari.get(i).getContatto().getContattoID());
				}
			}

			if (renderInoltraProt) {
				final boolean ricercaGruppi = false;
				contatti = rubSRV.ricercaCampoSingolo(query, utente.getIdUfficio(), utente.getIdAoo().intValue(), true, true, true, ricercaGruppi,
						"" + (15 + contattiGiaInseritiNum), contattiIdDaEscludere, null, true);
			} else {
				final boolean ricercaGruppi = true;
				contatti = rubSRV.ricercaCampoSingolo(query, utente.getIdUfficio(), utente.getIdAoo().intValue(), true, true, true, ricercaGruppi,
						"" + (15 + contattiGiaInseritiNum), contattiIdDaEscludere, utente.getIdUfficio(), true);
			}

			for (final Contatto c : contatti) {
				if (Boolean.FALSE.equals(c.getDisabilitaElemento())) {
					final DestinatarioRedDTO item = new DestinatarioRedDTO();
					item.setContatto(c);
					item.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
					destinatariList.add(item);
				}
			}

		}
		updateTbls = true;
		aggiungiDestinatarioNew();
		return destinatariList;
	}

	private void gestisciContattoOnTheFlyDest(final String query, final int rowIndex, final String campoMailOnTheFly) {
		final Contatto contattoDestOnTheFly = new Contatto();
		contattoDestOnTheFly.setAliasContatto(query);
		contattoDestOnTheFly.setCampoEmailVisible(true);
		contattoDestOnTheFly.setMailSelected(campoMailOnTheFly);
		destinatari.get(rowIndex).setContatto(contattoDestOnTheFly);
		destinatari.get(rowIndex).setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
		destinatari.get(rowIndex).setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
	}

	/**
	 * Aggiungi i contatti contenuti nel gruppo selezionato ai {@link #destinatari}.
	 * 
	 * @param gruppoSelected
	 */
	public void addContattiGruppo(final Contatto gruppoSelected) {
		final FacesContext context = FacesContext.getCurrentInstance();
		this.destinatari = RubricaComponent.addContattiGruppo(false, gruppoSelected, utente.getIdUfficio(), destinatari, context);
		aggiungiDestinatarioNew();
	}

	/**
	 * Rimuove il gruppo associato al contatto selezionato.
	 * 
	 * @param contattoSelected
	 */
	public void deleteGruppoDaSingoloContatto(final Contatto contattoSelected) {
		if (contattoSelected == null) {
			showError("Errore Generico");
			return;
		}

		this.destinatari = RubricaComponent.deleteGruppoDaSingoloContatto(contattoSelected, this.destinatari);
	}

	/**
	 * Effettua una validazione su parametri e una richiesta di creazione contatto
	 * "on the fly".
	 * 
	 * @param index
	 */
	public void richiediCreazioneContOnTheFlyDestinatario(final Integer index) {
		if (index == null) {
			showError("Errore nella richiesta di creazione del contatto");
			return;
		}
		openRubricaPreferiti();
		final DestinatarioRedDTO destinatarioSelected = this.destinatari.get(index);
		fromRichiediCreazioneDatatable = index;

		if (destinatarioSelected != null && destinatarioSelected.getContatto() != null) {
//			 && destinatarioSelected.getContatto().getFromDialogCreazioneModifica()
			if (destinatarioSelected.getContatto().getOnTheFly() != null && destinatarioSelected.getContatto().getOnTheFly() == 1) {
				if (destinatarioSelected.getContatto().getTipologiaEmail() != null
						&& destinatarioSelected.getContatto().getTipologiaEmail().equals(TipologiaIndirizzoEmailEnum.PEC)) {
					destinatarioSelected.getContatto().setMail(null);
					if (!StringUtils.isNullOrEmpty(destinatarioSelected.getContatto().getMailSelected())) {
						destinatarioSelected.getContatto().setMailPec(destinatarioSelected.getContatto().getMailSelected());
					}
				} else if (destinatarioSelected.getContatto().getTipologiaEmail() != null
						&& destinatarioSelected.getContatto().getTipologiaEmail().equals(TipologiaIndirizzoEmailEnum.PEO)) {
					destinatarioSelected.getContatto().setMailPec(null);
					if (!StringUtils.isNullOrEmpty(destinatarioSelected.getContatto().getMailSelected())) {
						destinatarioSelected.getContatto().setMail(destinatarioSelected.getContatto().getMailSelected());
					}
				}
			}
			inModificaDaShortcut = false;
			if (destinatarioSelected.getContatto().getTipoRubrica() != null && "RED".equals(destinatarioSelected.getContatto().getTipoRubrica())
					&& destinatarioSelected.getContatto().getContattoID() != null) {
				inModificaDaShortcut = true;
			}
			if (destinatarioSelected.getContatto().getOnTheFly() != null && destinatarioSelected.getContatto().getOnTheFly() == 1) {
				inModificaDaShortcut = false;
				if (destinatarioSelected.getContatto().getIdGruppoSelected() != null) {
					destinatarioSelected.getContatto().setIdGruppoSelected(null);
				}
				if (!StringUtils.isNullOrEmpty(destinatarioSelected.getContatto().getNomeGruppoSelected())) {
					destinatarioSelected.getContatto().setNomeGruppoSelected("");
				}
			}

			setInserisciContattoItem(destinatarioSelected.getContatto());
		}

		final TabView tb = (TabView) FacesContext.getCurrentInstance().getViewRoot().findComponent(MITTENTE_MAIL_PREF_TABS_DMD_RESOURCE_PATH);
		tb.setActiveIndex(2);

	}

	/**
	 * @return fromRichiediCreazioneDatatable
	 */
	public int getFromRichiediCreazioneDatatable() {
		return fromRichiediCreazioneDatatable;
	}

	/**
	 * @param fromRichiediCreazioneDatatable
	 */
	public void setFromRichiediCreazioneDatatable(final int fromRichiediCreazioneDatatable) {
		this.fromRichiediCreazioneDatatable = fromRichiediCreazioneDatatable;
	}

	/**
	 * Reinizializza {@link #inserisciContattoItem}.
	 */
	public void pulisciUtente() {
		setInserisciContattoItem(new Contatto());
		inModificaDaShortcut = false;
	}

	/**
	 * Restituisce le lista icone mail.
	 * 
	 * @return iconeHelpPageMail
	 */
	public List<IconaMailEnum> getIconeHelpPageMail() {
		return iconeHelpPageMail;
	}

	/**
	 * @param iconeHelpPageMail
	 */
	public void setIconeHelpPageMail(final List<IconaMailEnum> iconeHelpPageMail) {
		this.iconeHelpPageMail = iconeHelpPageMail;
	}

	/**
	 * Restituisce il mittente mail prot.
	 * 
	 * @return mittente
	 */
	public String getMittenteMailProt() {
		return mittenteMailProt;
	}

	/**
	 * Imposta il mittente mail prot.
	 * 
	 * @param mittenteMailProt
	 */
	public void setMittenteMailProt(final String mittenteMailProt) {
		this.mittenteMailProt = mittenteMailProt;
	}

	/**
	 * Restituisce l'oggetto di ricerca.
	 * 
	 * @return oggetto ricerca
	 */
	public String getOggettoRicerca() {
		return oggettoRicerca;
	}

	/**
	 * Imposta l'oggetto della ricerca.
	 * 
	 * @param oggettoRicerca
	 */
	public void setOggettoRicerca(final String oggettoRicerca) {
		this.oggettoRicerca = oggettoRicerca;
	}

	/**
	 * Restituisce il mittente ricerca.
	 * 
	 * @return mittente ricerca
	 */
	public String getMittenteRicerca() {
		return mittenteRicerca;
	}

	/**
	 * Imposta il mittente ricerca.
	 * 
	 * @param mittenteRicerca
	 */
	public void setMittenteRicerca(final String mittenteRicerca) {
		this.mittenteRicerca = mittenteRicerca;
	}

	/**
	 * Restituisce true se la ricerca è da effettuare "Full Text", false altrimenti.
	 * 
	 * @return true se la ricerca è da effettuare "Full Text", false altrimenti
	 */
	public boolean getIsFullTextRicerca() {
		return isFullTextRicerca;
	}

	/**
	 * Imposta il flag associato alla modalita full text.
	 * 
	 * @param isFullTextRicerca
	 */
	public void setIsFullTextRicerca(final boolean isFullTextRicerca) {
		this.isFullTextRicerca = isFullTextRicerca;
	}

	/**
	 * Restituisce l'anno di ricerca.
	 * 
	 * @return anno ricerca
	 */
	public Integer getAnnoRicerca() {
		return annoRicerca;
	}

	/**
	 * Imposta l'anno di ricerca.
	 * 
	 * @param annoRicerca
	 */
	public void setAnnoRicerca(final Integer annoRicerca) {
		this.annoRicerca = annoRicerca;
	}

	/**
	 * @return caricaTuttiGliElementi
	 */
	public boolean isCaricaTuttiGliElementi() {
		return caricaTuttiGliElementi;
	}

	/**
	 * @param caricaTuttiGliElementi
	 */
	public void setCaricaTuttiGliElementi(final boolean caricaTuttiGliElementi) {
		this.caricaTuttiGliElementi = caricaTuttiGliElementi;
	}

	/**
	 * Restituisce true se in ricerca mail, false altrimenti.
	 * 
	 * @return true se in ricerca mail, false altrimenti
	 */
	public boolean isRicercaMail() {
		return ricercaMail;
	}

	/**
	 * @param ricercaMail
	 */
	public void setRicercaMail(final boolean ricercaMail) {
		this.ricercaMail = ricercaMail;
	}

	/**
	 * Restituisce la lista di tutti gli elementi associati alle checkbox.
	 * 
	 * @return lista valori checkbox
	 */
	public List<Boolean> getAllElementCheckboxes() {
		return allElementCheckboxes;
	}

	/**
	 * Imposta la lista di tutti gli elementi associati alle checkbox.
	 * 
	 * @param allElementCheckboxes
	 */
	public void setAllElementCheckboxes(final List<Boolean> allElementCheckboxes) {
		this.allElementCheckboxes = allElementCheckboxes;
	}

	/**
	 * Restituisce il numero di righe per pagina associato al datatable delle mail.
	 * 
	 * @return rowMailDTH
	 */
	public String getRowMailDTH() {
		return rowMailDTH;
	}

	/**
	 * Imposta il numero di righe per pagina associato al datatable delle mail.
	 * 
	 * @param rowMailDTH
	 */
	public void setRowMailDTH(final String rowMailDTH) {
		this.rowMailDTH = rowMailDTH;
	}

	/**
	 * Restituisce l'indice della pagina.
	 * 
	 * @return index pagina
	 */
	public int getPageIndex() {
		return pageIndex;
	}

	/**
	 * Imposta l'indice della pagina.
	 * 
	 * @param pageIndex
	 */
	public void setPageIndex(final int pageIndex) {
		this.pageIndex = pageIndex;
	}

	/**
	 * Restituisce lo stato selezionato.
	 * 
	 * @return selectedStateString
	 */
	public String getSelectedStateString() {
		return selectedStateString;
	}

	/**
	 * Restituisce la mail ripristinata dalla ricerca.
	 * 
	 * @return EmailDTO della mail ripristinata
	 */
	public EmailDTO getRipristinataDaRicerca() {
		return ripristinataDaRicerca;
	}

	/**
	 * Imposta la mail ripristinata dalla ricerca.
	 * 
	 * @param ripristinataDaRicerca
	 */
	public void setRipristinataDaRicerca(final EmailDTO ripristinataDaRicerca) {
		this.ripristinataDaRicerca = ripristinataDaRicerca;
	}

	/**
	 * @return protocollaEMantieni
	 */
	public boolean getProtocollaEMantieni() {
		return protocollaEMantieni;
	}

	/**
	 * @param protocollaEMantieni
	 */
	public void setProtocollaEMantieni(final boolean protocollaEMantieni) {
		this.protocollaEMantieni = protocollaEMantieni;
	}

	/**
	 * Restituisce la lista dei contatti della rubrica.
	 * 
	 * @return lista contatti
	 */
	public List<Contatto> getContattiRubrica() {
		return contattiRubrica;
	}

	/**
	 * Imposta la lista dei contatti della rubrica.
	 * 
	 * @param contattiRubrica
	 */
	public void setContattiRubrica(final List<Contatto> contattiRubrica) {
		this.contattiRubrica = contattiRubrica;
	}

	/**
	 * Nasconde la visibilita dell'ufficio personale di {@link #ricercaRubricaDTO}.
	 */
	public void selezionaRubricaCheckBox() {
		if (Boolean.FALSE.equals(ricercaRubricaDTO.getRicercaRubricaUfficio()) && Boolean.FALSE.equals(ricercaRubricaDTO.getRicercaRED())
				&& Boolean.FALSE.equals(ricercaRubricaDTO.getRicercaIPA())) {
			ricercaRubricaDTO.setShowUfficioOPersonale(false);
		}
	}

	/**
	 * Mostra l'alberatura del contatto IPA.
	 * 
	 * @param event
	 */
	public void showAlberaturaContattoIPA(final ActionEvent event) {
		final Contatto contSel = getDataTableClickedRow(event);
		if (contSel.getTipoRubricaEnum().equals(TipoRubricaEnum.IPA)) {
			rubSRV.getContattiChildIPA(contSel);
			setRoot(new DefaultTreeNode("Root", null));
			getRoot().setSelectable(true);
			// Popola l'albero dal nodo corrente fino alla root
			getRecursiveNode(contSel);

		}
	}

	private TreeNode getRecursiveNode(final Contatto contattoIPA) {
		final Contatto contattoPadre = rubSRV.getPadreContattoIPA(contattoIPA);

		TreeNode nodoCorrente = null;
		if (contattoPadre == null) {
			nodoCorrente = new DefaultTreeNode(contattoIPA, getRoot());
			nodoCorrente.setExpanded(true);
			nodoCorrente.setSelectable(true);
			nodoCorrente.setType(contattoIPA.getTipoPersona());
		} else {
			final TreeNode nodoPadre = getRecursiveNode(contattoPadre);
			nodoCorrente = new DefaultTreeNode(contattoIPA, nodoPadre);
			nodoCorrente.setExpanded(true);
			nodoCorrente.setSelectable(true);
			nodoCorrente.setType(contattoIPA.getTipoPersona());

		}

		return nodoCorrente;
	}

	/**
	 * Restituisce la radice del tree.
	 * 
	 * @return radice tree
	 */
	public DefaultTreeNode getRoot() {
		return root;
	}

	/**
	 * Imposta la radice del tree.
	 * 
	 * @param root
	 */
	public void setRoot(final DefaultTreeNode root) {
		this.root = root;
	}

	/**
	 * Restituisce il contatto associato al vecchio item.
	 * 
	 * @return contatto vecchio
	 */
	public Contatto getContattoVecchioItem() {
		return contattoVecchioItem;
	}

	/**
	 * Imposta il contatto vecchio.
	 * 
	 * @param contattoVecchioItem
	 */
	public void setContattoVecchioItem(final Contatto contattoVecchioItem) {
		this.contattoVecchioItem = contattoVecchioItem;
	}

	/**
	 * Inoltra la richiesta di eliminazione del contatto
	 * {@link #getItemDaRichiedereEliminazione()}.
	 * {@link #richiediEliminazioneContatto(Contatto)}.
	 */
	public void richiediEliminazioneContatto() {
		richiediOEliminaContatto(itemDaRichiedereEliminazione, false);
	}

	/**
	 * Inoltra la richiesta di eliminazione del contatto.
	 * 
	 * @param contattoSelected
	 */
	private void richiediEliminazioneContatto(final Contatto contattoSelected) {
		final NotificaContatto notifica = new NotificaContatto();
		notifica.setNuovoContatto(contattoSelected);
		notifica.setTipoNotifica(NotificaContatto.TIPO_NOTIFICA_RICHIESTA);
		notifica.setAlias(contattoSelected.getAliasContatto());
		notifica.setCognome(contattoSelected.getCognome());
		notifica.setNome(contattoSelected.getNome());
		notifica.setDataOperazione(new Date());
		notifica.setIdContatto(contattoSelected.getContattoID());
		notifica.setIdNodoModifica(utente.getIdUfficio());
		notifica.setIdUtenteModifica(utente.getId());
		notifica.setNote(notaRichiestaEliminazione);
		notifica.setTipologiaContatto(contattoSelected.getTipoRubrica());
		notifica.setUtente(utente.getUsername());
		notifica.setStato(StatoNotificaContattoEnum.IN_ATTESA);
		notifica.setIdAoo(utente.getIdAoo());
		notifica.setOperazioneEnum(TipoOperazioneEnum.ELIMINA_CONTATTO);
		rubSRV.notificaEliminazioneContatto(notifica);
	}

	/**
	 * Elimina il contatto selezionato gestendo eventuali errori e comunicandone
	 * l'esito.
	 * 
	 * @param contattoSelected
	 * @param fromRisultatiRicerca
	 */
	public void eliminaContatto(final Contatto contattoSelected, final boolean fromRisultatiRicerca) {
		try {
			eliminaContatto(contattoSelected);
			if (fromRisultatiRicerca) {
				final DataTable table = getDataTableByIdComponent(ID_RIC_RUBRICA_DMDRC_HTML);
				if (table != null) {
					table.reset();
				}
				resultRicercaContatti.remove(contattoSelected);
				FacesHelper.update(ID_RIC_RUBRICA_DMDRC_HTML);
			} else {
				DataTable table = getDataTableByIdComponent(CONTATTIPROTOCOLLATBL_RESOURCE_PATH);
				if (table != null) {
					table.reset();
					if (getContattiProtocolla() != null) {
						getContattiProtocolla().remove(contattoSelected);
						FacesHelper.update(CONTATTIPROTOCOLLATBL_RESOURCE_PATH);
					}
				}

				table = getDataTableByIdComponent(PREFERITIMITTENTI_DMD_RESOURCE_PATH);
				if (table != null) {
					table.reset();
					if (getContattiPreferiti() != null) {
						getContattiPreferiti().remove(contattoSelected);
						FacesHelper.update(PREFERITIMITTENTI_DMD_RESOURCE_PATH);
					}
				}
			}
			showInfoMessage("Eliminazione contatto avvenuta con successo");
		} catch (final Exception ex) {
			LOGGER.error(ERRORE_DURANTE_L_ELIMINAZIONE_DEL_CONTATTO, ex);
			showError(ERRORE_DURANTE_L_ELIMINAZIONE_DEL_CONTATTO);
		}
	}

	/**
	 * Gestisce la richiesta di eliminazione del contatto selezionato, gestendo
	 * eventuali errori e mostrandole l'esito.
	 * 
	 * @param contattoSelected
	 * @param fromRisultatiRicerca
	 */
	public void richiediOEliminaContatto(final Contatto contattoSelected, final boolean fromRisultatiRicerca) {
		try {
			if (isPermessoUtenteGestioneRichiesteRubrica()) {
				eliminaContatto(contattoSelected);
				if (fromRisultatiRicerca) {
					final DataTable table = getDataTableByIdComponent(ID_RIC_RUBRICA_DMDRC_HTML);
					if (table != null) {
						table.reset();
					}
					resultRicercaContatti.remove(contattoSelected);
					FacesHelper.update(ID_RIC_RUBRICA_DMDRC_HTML);
				} else {
					DataTable table = getDataTableByIdComponent(CONTATTIPROTOCOLLATBL_RESOURCE_PATH);
					if (table != null) {
						table.reset();
						if (getContattiProtocolla() != null) {
							getContattiProtocolla().remove(contattoSelected);
							FacesHelper.update(CONTATTIPROTOCOLLATBL_RESOURCE_PATH);
						}
					}

					table = getDataTableByIdComponent(PREFERITIMITTENTI_DMD_RESOURCE_PATH);
					if (table != null) {
						table.reset();
						if (getContattiPreferiti() != null) {
							getContattiPreferiti().remove(contattoSelected);
							FacesHelper.update(PREFERITIMITTENTI_DMD_RESOURCE_PATH);
						}
					}

				}
				showInfoMessage("Eliminazione contatto avvenuta con successo");
			} else {
				richiediEliminazioneContatto(contattoSelected);
				showInfoMessage("Richiesta eliminazione contatto avvenuta con successo");
			}
			notaRichiestaEliminazione = "";
		} catch (final Exception ex) {
			LOGGER.error(ERRORE_DURANTE_L_ELIMINAZIONE_DEL_CONTATTO, ex);
			showError(ERRORE_DURANTE_L_ELIMINAZIONE_DEL_CONTATTO);
		}
	}

	private void eliminaContatto(final Contatto contattoSelected) {
		rubSRV.elimina(contattoSelected.getContattoID(), null, null);
	}

	/**
	 * Restituisce la nota di richiesta creazione.
	 * 
	 * @return nota richiesta creazione
	 */
	public String getNotaRichiestaCreazione() {
		return notaRichiestaCreazione;
	}

	/**
	 * Imposta la nota di richiesta creazione.
	 * 
	 * @param notaRichiestaCreazione
	 */
	public void setNotaRichiestaCreazione(final String notaRichiestaCreazione) {
		this.notaRichiestaCreazione = notaRichiestaCreazione;
	}

	/**
	 * Restituisce la nota di richiesta modifica.
	 * 
	 * @return nota richiesta modifica
	 */
	public String getNotaRichiestaModifica() {
		return notaRichiestaModifica;
	}

	/**
	 * Imposta la nota di richiesta modifica.
	 * 
	 * @param notaRichiestaModifica
	 */
	public void setNotaRichiestaModifica(final String notaRichiestaModifica) {
		this.notaRichiestaModifica = notaRichiestaModifica;
	}

	/**
	 * Restituisce la nota di richiesta eliminazione.
	 * 
	 * @return nota richiesta eliminazione
	 */
	public String getNotaRichiestaEliminazione() {
		return notaRichiestaEliminazione;
	}

	/**
	 * Imposta la nota di richiesta eliminazione.
	 * 
	 * @param notaRichiestaEliminazione
	 */
	public void setNotaRichiestaEliminazione(final String notaRichiestaEliminazione) {
		this.notaRichiestaEliminazione = notaRichiestaEliminazione;
	}

	/**
	 * Restituisce il contatto da eliminare.
	 * 
	 * @return contatto
	 */
	public Contatto getEliminaContattoItem() {
		return eliminaContattoItem;
	}

	/**
	 * Imposta il contatto da eliminare.
	 * 
	 * @param eliminaContattoItem
	 */
	public void setEliminaContattoItem(final Contatto eliminaContattoItem) {
		this.eliminaContattoItem = eliminaContattoItem;
	}

	/**
	 * Restituisce la lista dei contatti nel gruppo.
	 * 
	 * @return lista contatti gruppo
	 */
	public List<Contatto> getContattiNelGruppo() {
		return contattiNelGruppo;
	}

	/**
	 * Imposta la lista dei contatti nel gruppo.
	 * 
	 * @param contattiNelGruppo
	 */
	public void setContattiNelGruppo(final List<Contatto> contattiNelGruppo) {
		this.contattiNelGruppo = contattiNelGruppo;
	}

	/**
	 * Popola {@link #contattiNelGruppo} con i contatti del gruppo selezionato.
	 * 
	 * @param gruppoSelected
	 */
	public void ricercaContattiNelGruppo(final Contatto gruppoSelected) {
		contattiNelGruppo = RubricaComponent.ricercaContattiNelGruppo(gruppoSelected, utente.getIdUfficio());
	}

	/**
	 * Imposta il contatto da eliminare: {@link #eliminaContattoItem} con il
	 * contatto selezionato.
	 * 
	 * @param event
	 */
	public void impostaContattoDaEliminare(final ActionEvent event) {
		final Contatto c = getDataTableClickedRow(event);
		eliminaContattoItem = new Contatto();
		eliminaContattoItem.copyFromContatto(c);
		notaRichiestaEliminazione = "";
	}

	/**
	 * Gestisce l'invio della notifica di eliminazione.
	 */
	public void inviaNotificaEliminazione() {
		try {
			if (StringUtils.isNullOrEmpty(notaRichiestaEliminazione)) {
				showError(ERROR_NOTA_ASSENTE_MSG);
				return;
			}
			RubricaComponent.inviaNotificaEliminazione(eliminaContattoItem, notaRichiestaEliminazione, utente);
			showInfoMessage("Richiesta di eliminazione inviata con successo");
			notaRichiestaEliminazione = "";
		} catch (final Exception e) {
			showError(e);
		}
	}

	/**
	 * Gestisce l'invio della notifica di creazione.
	 */
	public void inviaNotificaCreazione() {
		try {
			if (StringUtils.isNullOrEmpty(notaRichiestaCreazione)) {
				showError(ERROR_NOTA_ASSENTE_MSG);
				return;
			}
			final FacesContext context = FacesContext.getCurrentInstance();
			if (getInserisciContattoItem() != null) {
				if (!visualizzaCreaContatto) {
					getInserisciContattoItem().setIdAOO(utente.getIdAoo());
					RubricaComponent.inviaNotificaCreazione(getInserisciContattoItem(), notaRichiestaCreazione, utente, context);
				} else {
					inserisciContattoCasellaPostaleItem.setIdAOO(utente.getIdAoo());
					RubricaComponent.inviaNotificaCreazione(inserisciContattoCasellaPostaleItem, notaRichiestaCreazione, utente, context);
				}
				if (Boolean.FALSE.equals(scegliContattoProt) && (renderInoltra || renderInoltraProt)) {
					aggiungiContattoAiDestinatariInoltraProt();
				}
				if ((renderProtocolla || renderRifiutaProt || renderInoltraProt) && !isPermessoUtenteGestioneRichiesteRubrica()) {
					if (!visualizzaCreaContatto && Boolean.TRUE.equals(scegliContattoProt)) {
						callBackToAction(getInserisciContattoItem());
					} else if (visualizzaCreaContatto && (renderProtocolla || renderInoltraProt)) {
						callBackToAction(inserisciContattoCasellaPostaleItem);
					}
				}
				FacesHelper.executeJS("PF('dialogRichCreazioneVW').hide();");
				showInfoMessage(SUCCESS_CREAZIONE_CONTATTO_MSG);
				notaRichiestaCreazione = "";
			} else {
				showError("Non è possibile procedere con l'invio della richiesta creazione");
			}
		} catch (final Exception ex) {
			showError(ex);
		}
	}

	/**
	 * Gestisce l'invio della notifica di modifica.
	 */
	public void inviaNotificaModifica() {
		try {
			if (StringUtils.isNullOrEmpty(notaRichiestaModifica)) {
				showError(ERROR_NOTA_ASSENTE_MSG);
				return;
			}
			final FacesContext context = FacesContext.getCurrentInstance();
			RubricaComponent.inviaNotificaModifica(getInserisciContattoItem(), contattoVecchioItem, notaRichiestaModifica, utente, context);
			notaRichiestaModifica = null;
			showInfoMessage("Richiesta di modifica inviata con successo");
		} catch (final Exception e) {
			showError(e);
		}
	}

	/**
	 * Restituisce il campo "On the Fly".
	 * 
	 * @return campo mail on the flay
	 */
	public String getCampoMailOnTheFly() {
		return campoMailOnTheFly;
	}

	/**
	 * Imposta il campo mail "On the Fly".
	 * 
	 * @param campoMailOnTheFly
	 */
	public void setCampoMailOnTheFly(final String campoMailOnTheFly) {
		this.campoMailOnTheFly = campoMailOnTheFly;
	}

	/**
	 * Gestisce l'evento di modifica del tipo email del destinatario selezionato.
	 * 
	 * @param contattoSelected
	 */
	public void onChangeTipoEmailDestinatari(final Contatto contattoSelected) {
		setMailPecOPeo(contattoSelected, contattoSelected.getFromDialogCreazioneModifica());
	}

	private static void setMailPecOPeo(final Contatto contatto, final boolean fromCreazioneModifica) {
		if (contatto != null) {
			if (contatto.getContattoID() == null || (contatto.getOnTheFly() != null && contatto.getOnTheFly() == 1)) {
				if (fromCreazioneModifica) {
					if (TipologiaIndirizzoEmailEnum.PEC.equals(contatto.getTipologiaEmail()) && !StringUtils.isNullOrEmpty(contatto.getMailPec())) {
						contatto.setMailSelected(contatto.getMailPec());
					} else if (TipologiaIndirizzoEmailEnum.PEO.equals(contatto.getTipologiaEmail()) && !StringUtils.isNullOrEmpty(contatto.getMail())) {
						contatto.setMailSelected(contatto.getMail());
					} else {
						contatto.setMailSelected(null);
					}
				} else {
					if (!StringUtils.isNullOrEmpty(contatto.getMailSelected()) && TipologiaIndirizzoEmailEnum.PEC.equals(contatto.getTipologiaEmail())) {
						contatto.setMailPec(contatto.getMailSelected());
						contatto.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEC);
					} else if (!StringUtils.isNullOrEmpty(contatto.getMailSelected()) && TipologiaIndirizzoEmailEnum.PEO.equals(contatto.getTipologiaEmail())) {
						contatto.setMail(contatto.getMailSelected());
						contatto.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEO);
					}
				}
			} else {
				if (TipologiaIndirizzoEmailEnum.PEC.equals(contatto.getTipologiaEmail()) && !StringUtils.isNullOrEmpty(contatto.getMailPec())) {
					contatto.setMailSelected(contatto.getMailPec());
				} else if (TipologiaIndirizzoEmailEnum.PEO.equals(contatto.getTipologiaEmail()) && !StringUtils.isNullOrEmpty(contatto.getMail())) {
					contatto.setMailSelected(contatto.getMail());
				} else {
					contatto.setMailSelected(null);
				}
			}
		}
	}

	private void aggiungiContattoAiDestinatariInoltraProt() {
		if (fromRichiediCreazioneDatatable != -1) {
			final DestinatarioRedDTO d = this.destinatari.get(fromRichiediCreazioneDatatable);
			setDestinatarioPerCreazione(d);
		} else {
			this.destinatari = getDestinatariInModificaPuliti();
			final DestinatarioRedDTO d = new DestinatarioRedDTO();
			setDestinatarioPerCreazione(d);
			this.destinatari.add(d);
			clearContattoandHideModifica();
			FacesHelper.update("eastSectionForm:inoltraPnl");
			FacesHelper.update("eastSectionForm:dettagliInoltroProt");
			FacesHelper.update(MITTENTE_MAIL_PREF_TABS_DMD_RESOURCE_PATH);
		}
		aggiungiDestinatarioNew();
	}

	private void setDestinatarioPerCreazione(final DestinatarioRedDTO d) {
		if (!StringUtils.isNullOrEmpty(getInserisciContattoItem().getMailPec())) {
			getInserisciContattoItem().setMailSelected(getInserisciContattoItem().getMailPec());
			getInserisciContattoItem().setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEC);
		} else {
			getInserisciContattoItem().setMailSelected(getInserisciContattoItem().getMail());
			getInserisciContattoItem().setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEO);
		}

		d.setContatto(getInserisciContattoItem());
		if (StringUtils.isNullOrEmpty(getInserisciContattoItem().getMailSelected())) {
			d.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.CARTACEO);
			d.setMezzoSpedizioneDisabled(true);
		} else {
			d.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.ELETTRONICO);
			d.setMezzoSpedizioneDisabled(false);
		}

		d.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);

	}

	/**
	 * Aggiunge un nuovo destinatario alla lista dei destinatari:
	 * {@link #destinatari}.
	 */
	public void aggiungiDestinatarioNew() {
		if (this.destinatari == null) {
			destinatari = new ArrayList<>();
		} else {
			destinatari = getDestinatariInModificaPuliti();
		}

		final DestinatarioRedDTO destinatario = new DestinatarioRedDTO();
		destinatario.setContatto(new Contatto());
		destinatario.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
		destinatario.getContatto().setIdDataTable(UUID.randomUUID().toString());
		destinatari.add(destinatario);
	}

	/**
	 * Restituisce il contatto per cui si richiede l'eliminazione.
	 * 
	 * @return contatto
	 */
	public Contatto getItemDaRichiedereEliminazione() {
		return itemDaRichiedereEliminazione;
	}

	/**
	 * 
	 * @param itemDaRichiedereEliminazione
	 */
	public void setItemDaRichiedereEliminazione(final Contatto itemDaRichiedereEliminazione) {
		this.itemDaRichiedereEliminazione = itemDaRichiedereEliminazione;
	}

	/**
	 * Esegue una validazione della firma e in caso di esito negativo mostra un
	 * messaggio di errore.
	 * 
	 * @param nodo
	 */
	public void verificaFirma(final HierarchicalFileWrapperDTO nodo) {
		setVerificaFirmaComponent(new VerificaFirmaComponent());
		final String idAllegato = nodo.getNodeId();
		final String nome = nodo.getFile().getName();
		final InputStream stream = new ByteArrayInputStream(scompattaMailSRV.getContentutoDiscompattataMail(utente, idAllegato));
		try {
			final VerifyInfoDTO infoFirma = signSRV.obtainVerifyInfoFirma(stream, Constants.ContentType.PDF, utente);
			setVerificaFirmaComponent(new VerificaFirmaComponent(infoFirma, nome));
			boolean firmaValida = infoFirma.getFirmatari() != null && !infoFirma.getFirmatari().isEmpty();
			if (firmaValida) {
				for (final SignerDTO firm : infoFirma.getFirmatari()) {
					if (!firm.isValid()) {
						firmaValida = false;
						break;
					}
				}
			}

			nodo.setFirmaValida(firmaValida);
		} catch (final Exception e) {
			LOGGER.error("Errore nella validazione della firma del doc " + nome);
			throw new RedException(e);
		}
	}

	/**
	 * Esegue ricorsivamente verifica firma.
	 * 
	 * @param hfw
	 */
	public void verificaFirmeAllegatiRicorsivo(final HierarchicalFileWrapperDTO hfw) {
		if (hfw.isFirmato()) {
			verificaFirma(hfw);
		}
		if (hfw.getSons() != null) {
			for (final HierarchicalFileWrapperDTO singeHfw : hfw.getSons()) {
				verificaFirmeAllegatiRicorsivo(singeHfw);
			}
		}
	}

	/**
	 * Verifica firma su tutti gli allegati.
	 */
	public void verificaFirmeAllegati() {
		if (treeTableComponent.getRoot().getChildren() != null) {
			for (final TreeNode singleNode : treeTableComponent.getRoot().getChildren()) {
				final ListElementDTO<HierarchicalFileWrapperDTO> listElement = (ListElementDTO<HierarchicalFileWrapperDTO>) singleNode.getData();
				verificaFirmeAllegatiRicorsivo(listElement.getData());
			}
		}

	}

	/**
	 * Restituisce il component per la verifica firma.
	 * 
	 * @return il component
	 */
	public VerificaFirmaComponent getVerificaFirmaComponent() {
		return verificaFirmaComponent;
	}

	/**
	 * Imposta il component che si occupa della verifica firma.
	 * 
	 * @param verificaFirmaComponent
	 */
	public void setVerificaFirmaComponent(final VerificaFirmaComponent verificaFirmaComponent) {
		this.verificaFirmaComponent = verificaFirmaComponent;
	}

	/**
	 * Restituisce il numero di protocollo ricerca.
	 * 
	 * @return numero protocollo
	 */
	public String getNumeroProtocolloRicerca() {
		return numeroProtocolloRicerca;
	}

	/**
	 * Imposta il numero protocollo ricerca.
	 * 
	 * @param numeroProtocolloRicerca
	 */
	public void setNumeroProtocolloRicerca(final String numeroProtocolloRicerca) {
		this.numeroProtocolloRicerca = numeroProtocolloRicerca;
	}

	/**
	 * Restituisce il flag associato alla ricerca relativo alla necessita di
	 * protocollazione.
	 * 
	 * @return true se la ricerca è imposta su da protocollare, false viceversa
	 */
	public boolean isDaProtocollareRicerca() {
		return daProtocollareRicerca;
	}

	/**
	 * Imposta il flag daProtocollareRicerca.
	 * 
	 * @param daProtocollareRicerca
	 */
	public void setDaProtocollareRicerca(final boolean daProtocollareRicerca) {
		this.daProtocollareRicerca = daProtocollareRicerca;

	}

	/**
	 * @return inoltrateRicerca
	 */
	public boolean isInoltrateRicerca() {
		return inoltrateRicerca;
	}

	/**
	 * @param inoltrateRicerca
	 */
	public void setInoltrateRicerca(final boolean inoltrateRicerca) {
		this.inoltrateRicerca = inoltrateRicerca;
	}

	/**
	 * @return rifiutateRicerca
	 */
	public boolean isRifiutateRicerca() {
		return rifiutateRicerca;
	}

	/**
	 * @param rifiutateRicerca
	 */
	public void setRifiutateRicerca(final boolean rifiutateRicerca) {
		this.rifiutateRicerca = rifiutateRicerca;
	}

	/**
	 * @return eliminateRicerca
	 */
	public boolean isEliminateRicerca() {
		return eliminateRicerca;
	}

	/**
	 * @param eliminateRicerca
	 */
	public void setEliminateRicerca(final boolean eliminateRicerca) {
		this.eliminateRicerca = eliminateRicerca;
	}

	/**
	 * Restituisce il flag assiciato alla visibilità della max size del messaggio di
	 * warning.
	 * 
	 * @return flag assiciato alla visibilità della max size del messaggio di
	 *         warning
	 */
	public boolean isShowWarnMessageMaxSize() {
		return showWarnMessageMaxSize;
	}

	/**
	 * Imposta il flag assiciato alla visibilità della max size del messaggio di
	 * warning.
	 * 
	 * @param showWarnMessageMaxSize
	 */
	public void setShowWarnMessageMaxSize(boolean showWarnMessageMaxSize) {
		this.showWarnMessageMaxSize = showWarnMessageMaxSize;
	}

}
