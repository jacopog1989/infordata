package it.ibm.red.web.mbean;

import java.io.ByteArrayInputStream;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentoFascicoloFepaDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FascicoloFEPADTO;
import it.ibm.red.business.dto.FascicoloFEPAInfoDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.FascicoloFepaModeEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.facade.IFascicoloFacadeSRV;
import it.ibm.red.business.service.facade.IFepaFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.enums.NavigationTokenEnum;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.ricerca.mbean.RicercaDocumentiRedBean;
import it.ibm.red.web.ricerca.mbean.RicercaDocumentiUcbBean;
import it.ibm.red.web.ricerca.mbean.RicercaRapidaBean;

/**
 * Bean fascicolo FEPA.
 */
@Named(ConstantsWeb.MBean.FASCICOLO_FEPA_BEAN)
@ViewScoped
public class FascicoloFepaBean extends AbstractBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Messaggio di errore recupero Stream.
	 */
	private static final String ERROR_RECUPERO_STREAM_MSG = "Impossibile recuperare lo stream.";

	/**
	 * Messaggio errore download documento.
	 */
	private static final String ERROR_DOWNLOAD_DOCUMENTO_MSG = "Errore nel download del documento";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FascicoloFepaBean.class);

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Fascicolo FEPA.
	 */
	private FascicoloFEPAInfoDTO fascicoloFEPAInfo;

	/**
	 * Documenti fepa.
	 */
	private List<DocumentoFascicoloFepaDTO> documentiFepaList;

	/**
	 * Lista documenti colleaati.
	 */
	private List<DocumentoFascicoloFepaDTO> listaDocumentiFascCollegato;
	
	/**
	 * Servizio.
	 */
	private IFepaFacadeSRV fepaSRV;
	
	/**
	 * Modalità.
	 */
	private FascicoloFepaModeEnum mode;

	/**
	 * Dettaglio.
	 */
	private DetailDocumentRedDTO detail;

	/**
	 * Identificativo fascicolo fepa.
	 */
	private String idFascicoloFepa;

	/**
	 * Tipologia fascicolo fepa.
	 */
	private String tipoFascicoloFepa;

	@Override
	protected void postConstruct() {
		// Non occorre fare niente nel post construct del bean.
	}

	/**
	 * Inizializza il bean iniettando i service.
	 */
	public void init() {
		fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);
		final IFascicoloFacadeSRV fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloFacadeSRV.class);
		utente = ((SessionBean) FacesHelper.getManagedBean(MBean.SESSION_BEAN)).getUtente();

		mode = FascicoloFepaModeEnum.OFF;

		fillDetail();
		Integer idTipologiaDocumento = null;
		if (detail != null) {
			final FascicoloDTO fascicoloProc = fascicoloSRV.getFascicoloProcedimentale(detail.getDocumentTitle(), utente.getIdAoo().intValue(), utente);
			idFascicoloFepa = fascicoloProc.getIdFascicoloFEPA();
			tipoFascicoloFepa = fascicoloProc.getTipoFascicoloFEPA();
			idTipologiaDocumento = detail.getIdTipologiaDocumento();
		}
		if (!StringUtils.isNullOrEmpty(idFascicoloFepa)) {

			if (detail == null) {
				LOGGER.error("Errore riscontrato nell'init di FascicoloFepaBean.");
				throw new RedException("Detail risulta null prima della chiamata ad un suo metodo.");
			}

			if (utente.isUcb() && !StringUtils.isNullOrEmpty(tipoFascicoloFepa) && PropertiesProvider.getIstance()
					.getParameterByString(utente.getCodiceAoo() + "." + PropertiesNameEnum.SICOGE_NOME_FLUSSO.getKey()).equals(detail.getCodiceFlusso())) {
				mode = FascicoloFepaModeEnum.SICOGE;
			} else if (idTipologiaDocumento != null) {
				if (fepaSRV.showBilEntiFasc(idTipologiaDocumento)) {
					mode = FascicoloFepaModeEnum.BILANCIO_ENTI;
				} else if (fepaSRV.showRevIGFFasc(idTipologiaDocumento)) {
					mode = FascicoloFepaModeEnum.REVISORI;
				}
			}
		}
	}

	private void fillDetail() {
		detail = null;

		final NavigationTokenEnum nte = ((SessionBean) FacesHelper.getManagedBean(MBean.SESSION_BEAN)).getActivePageInfo();
		if (nte.getDocumentQueue() != null && !DocumentQueueEnum.isOneKindOfCartacei(nte.getDocumentQueue())) {
			final ListaDocumentiBean ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
			final MasterDocumentRedDTO master = ldb.getDocumentiDTH().getCurrentDetail();
			if (master != null && !StringUtils.isNullOrEmpty(master.getDocumentTitle())) {
				detail = new DetailDocumentRedDTO();
				detail.setCodiceFlusso(master.getCodiceFlusso());
				detail.setIdTipologiaDocumento(master.getIdTipologiaDocumento());
				detail.setDocumentTitle(master.getDocumentTitle());
			}
		} else if (NavigationTokenEnum.RICERCA_DOCUMENTI_UCB.equals(nte)) {
			final RicercaDocumentiUcbBean rsb = FacesHelper.getManagedBean(ConstantsWeb.MBean.RICERCA_DOCUMENTI_UCB_BEAN);
			final MasterDocumentRedDTO master = rsb.getDocumentiUcb().getCurrentDetail();
			if (master != null && !StringUtils.isNullOrEmpty(master.getDocumentTitle())) {
				detail = new DetailDocumentRedDTO();
				detail.setCodiceFlusso(master.getCodiceFlusso());
				detail.setIdTipologiaDocumento(master.getIdTipologiaDocumento());
				detail.setDocumentTitle(master.getDocumentTitle());
			}
		} else if (NavigationTokenEnum.RICERCA_DOCUMENTI.equals(nte)) {
			final RicercaDocumentiRedBean rsb = FacesHelper.getManagedBean(ConstantsWeb.MBean.RICERCA_DOCUMENTI_RED_BEAN);
			final MasterDocumentRedDTO master = rsb.getDocumentiRedDTH().getCurrentDetail();
			if (master != null && !StringUtils.isNullOrEmpty(master.getDocumentTitle())) {
				detail = new DetailDocumentRedDTO();
				detail.setCodiceFlusso(master.getCodiceFlusso());
				detail.setIdTipologiaDocumento(master.getIdTipologiaDocumento());
				detail.setDocumentTitle(master.getDocumentTitle());
			}
		} else if (NavigationTokenEnum.RICERCA_RAPIDA.equals(nte)) {
			final RicercaRapidaBean rsb = FacesHelper.getManagedBean(ConstantsWeb.MBean.RICERCA_RAPIDA_BEAN);
			final MasterDocumentRedDTO master = rsb.getDocumentiDTH().getCurrentDetail();
			if (master != null && !StringUtils.isNullOrEmpty(master.getDocumentTitle())) {
				detail = new DetailDocumentRedDTO();
				detail.setCodiceFlusso(master.getCodiceFlusso());
				detail.setIdTipologiaDocumento(master.getIdTipologiaDocumento());
				detail.setDocumentTitle(master.getDocumentTitle());
			}
		}

	}

	/**
	 * Recupera tutte le informazioni sui fascicoli SICOGE, Bilancio Enti e Verbali
	 * Revisione.
	 */
	public void loadData() {
		try {
			if (FascicoloFepaModeEnum.SICOGE.equals(mode)) {
				fascicoloFEPAInfo = fepaSRV.getFascicoliSicoge(true, idFascicoloFepa, tipoFascicoloFepa, utente.getCodiceAoo(), null);
			} else if (FascicoloFepaModeEnum.BILANCIO_ENTI.equals(mode)) {
				documentiFepaList = fepaSRV.getFascicoliBilancioEnti(idFascicoloFepa);
			} else if (FascicoloFepaModeEnum.REVISORI.equals(mode)) {
				documentiFepaList = fepaSRV.getFascicoliVerbaleRevisione(idFascicoloFepa);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, "Errore durante l'operazione di recupero del fascicolo.");
		}
	}
	
	/**
	 * Resetta le liste.
	 */
	public void resetData() {
		try {
			fascicoloFEPAInfo = null;
			documentiFepaList = null;
			listaDocumentiFascCollegato = null;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, "Errore durante l'operazione di recupero del fascicolo.");
		}			
	}



	/**
	 * Gestisce l'evento di selezione del fascicolo collegato.
	 * 
	 * @param event
	 */
	public void fascCollegatoSelector(final SelectEvent event) {
		final FascicoloFEPADTO fasCol = (FascicoloFEPADTO) event.getObject();
		final FascicoloFEPAInfoDTO fascicoloCollegato = fepaSRV.getFascicoliSicoge(false, fasCol.getIdFascicolo(), fasCol.getTipoFascicolo(), utente.getCodiceAoo(),
				fasCol.getIdFascicoloRiferimento());
		listaDocumentiFascCollegato = fascicoloCollegato.getListaDocumenti();
	}

	/**
	 * Restituisce lo StreamedContent per il download del documento SICOGE.
	 * 
	 * @param index
	 * @return StreamedContent per download
	 */
	public StreamedContent downloadDocumentoSICOGE(final Object index) {
		StreamedContent out = null;
		try {
			final DocumentoFascicoloFepaDTO documentoSelezionato = fascicoloFEPAInfo.getListaDocumenti().get((Integer) index);
			out = downloadContentFascicolo(documentoSelezionato);
			if (out == null) {
				throw new RedException(ERROR_RECUPERO_STREAM_MSG);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, ERROR_DOWNLOAD_DOCUMENTO_MSG);
		}
		return out;
	}

	/**
	 * Restituisce lo StreamedContent per il download del documento SICOGE fascicolo
	 * collegato.
	 * 
	 * @param index
	 * @return StreamedContent per download
	 */
	public StreamedContent downloadDocumentoSICOGEFascCollegato(final Object index) {
		StreamedContent out = null;
		try {
			final DocumentoFascicoloFepaDTO documentoSelezionato = listaDocumentiFascCollegato.get((Integer) index);
			out = downloadContentFascicolo(documentoSelezionato);
			if (out == null) {
				throw new RedException(ERROR_RECUPERO_STREAM_MSG);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, ERROR_DOWNLOAD_DOCUMENTO_MSG);
		}
		return out;
	}

	/**
	 * Restituisce lo StreamedContent per il download del documento BE_IGF.
	 * 
	 * @param index
	 * @return StreamedContent per download
	 */
	public StreamedContent downloadDocumentoBeIGF(final Object index) {
		StreamedContent out = null;
		try {
			final DocumentoFascicoloFepaDTO documentoSelezionato = documentiFepaList.get((Integer) index);
			out = downloadContentFascicolo(documentoSelezionato);
			if (out == null) {
				throw new RedException(ERROR_RECUPERO_STREAM_MSG);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ConstantsWeb.DOCUMENT_MANAGER_MESSAGES_CLIENT_ID, ERROR_DOWNLOAD_DOCUMENTO_MSG);
		}
		return out;
	}

	private StreamedContent downloadContentFascicolo(final DocumentoFascicoloFepaDTO documentoSelezionato) {

		final String idFascicolo = documentoSelezionato.getIdFascicolo();
		final String guid = documentoSelezionato.getGuid();
		final String tipoFascicolo = documentoSelezionato.getTipoFascicolo();
		final String idFascicoloRiferimento = documentoSelezionato.getIdFascicoloRiferimento();

		StreamedContent file = null;
		fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);

		FileDTO doc = null;
		if (detail.getIdTipologiaDocumento() == Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_TIPOLOGIA_BILANCIO_ENTI_ENTRATA))) {
			doc = fepaSRV.downloadDocumentoBilancioEnti(idFascicolo, guid);
		} else if (detail.getIdTipologiaDocumento() == Integer
				.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_TIPOLOGIA_REVISORI_IGF_ENTRATA))) {
			doc = fepaSRV.downloadDocumentoVerbaliRevisione(idFascicolo, guid);
		} else if (PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "." + PropertiesNameEnum.SICOGE_NOME_FLUSSO.getKey())
				.equals(detail.getCodiceFlusso())) {
			doc = fepaSRV.downloadDocumentoSicoge(idFascicolo, guid, tipoFascicolo, utente.getCodiceAoo(), idFascicoloRiferimento);
		}

		if (doc != null) {
			file = new DefaultStreamedContent(new ByteArrayInputStream(doc.getContent()), doc.getMimeType(), doc.getFileName());
		}
		return file;
	}

	/**
	 * Restituisce la modalita del fascicolo Fepa.
	 * 
	 * @see FascicoloFepaModeEnum.
	 * @return mode
	 */
	public FascicoloFepaModeEnum getMode() {
		return mode;
	}

	/**
	 * Restituisce le informazione associate al fascicolo FEPA.
	 * 
	 * @return info fascicolo Fepa
	 */
	public FascicoloFEPAInfoDTO getFascicoloFEPAInfo() {
		return fascicoloFEPAInfo;
	}

	/**
	 * Restituisce la lista dei documenti FEPA.
	 * 
	 * @return lista di documenti FEPA
	 */
	public List<DocumentoFascicoloFepaDTO> getDocumentiFepaList() {
		return documentiFepaList;
	}

	/**
	 * Restituisce la lista dei documenti fascicoli collegati.
	 * 
	 * @return lista documenti fascicolo
	 */
	public List<DocumentoFascicoloFepaDTO> getListaDocumentiFascCollegato() {
		return listaDocumentiFascCollegato;
	}

}
