package it.ibm.red.web.helper.dtable;

/**
 * Classe selectable.
 * @param <E>
 */
public class Selectable<E> implements ISelectable {

	/**
	 * Dati.
	 */
	private E data; 

	/**
	 * Flag selezionato.
	 * Indica lo stato di selezione dell'item.
	 */
	boolean selected;

	/**
	 * Costruttore.
	 */
	public Selectable() {
		super();
	}

	/**
	 * Costruttore.
	 * @param data
	 */
	public Selectable(final E data) {
		this.data = data;
	}

	/**
	 * @see it.ibm.red.web.helper.dtable.ISelectable#setSelected(boolean).
	 */
	@Override
	public void setSelected(final boolean flag) {
		this.selected = flag;
	}

	/**
	 * @see it.ibm.red.web.helper.dtable.ISelectable#isSelected().
	 */
	@Override
	public boolean isSelected() {
		return this.selected;
	}

	/**
	 * Restituisce l'informazione associata.
	 * @return data E
	 */
	public E getData() {
		return data;
	}

}
