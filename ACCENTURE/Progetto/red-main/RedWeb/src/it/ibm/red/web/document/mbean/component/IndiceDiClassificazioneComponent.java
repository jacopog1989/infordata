package it.ibm.red.web.document.mbean.component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;

/**
 * Component gestione indice di classificazione.
 */
public class IndiceDiClassificazioneComponent extends AbstractIndiceDiClassificazioneComponent {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -3590030286443239401L;

    /**
     * Logger.
     */
	private static final REDLogger LOGGER = REDLogger.getLogger(IndiceDiClassificazioneComponent.class);

    /**
     * Titolari.
     */
	protected transient Map<String, TitolarioDTO> id2titolario;

    /**
     * Dettaglio.
     */
	protected DetailDocumentRedDTO detail;
	
	/**
	 * Costruttore del component.
	 * @param detail
	 * @param utente
	 */
	public IndiceDiClassificazioneComponent(final DetailDocumentRedDTO detail, final UtenteDTO utente) {
		this(detail, utente, true);
	}
	
	/**
	 * Costruttore del component.
	 * @param detail
	 * @param utente
	 * @param creaTitolario
	 */
	public IndiceDiClassificazioneComponent(final DetailDocumentRedDTO detail, final UtenteDTO utente, final boolean creaTitolario) {
		super(utente);
		this.detail = detail;
		this.id2titolario = new HashMap<>();
		
		if (creaTitolario) {
			creaTitolario();
		}
	}
	
	/**
	 * @see it.ibm.red.web.document.mbean.component.AbstractIndiceDiClassificazioneComponent#creaTitolario().
	 */
	@Override
	protected void creaTitolario() {
		try {
			final List<TitolarioDTO> rootChildren = retrieveRootChildren();
			final TitolarioDTO root = new TitolarioDTO("", "", "Titolario", 0, utente.getIdAoo(), null, null, rootChildren.size()); 
			rootTitolario = new DefaultTreeNode(root, null);
			
			wrappaAlberoTitolario(rootChildren, rootTitolario);
			
			// Si inizializza la selezione del titolario, qualora quest'ultimo sia gia presente nel detail
			initTitolario();
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage("Errore durante l'operazione: " + e.getMessage());
		}
	}
	
	/**
	 * @see it.ibm.red.web.document.mbean.component.AbstractIndiceDiClassificazioneComponent#wrappaAlberoTitolario(java.util.List,
	 *      org.primefaces.model.TreeNode).
	 */
	@Override
	protected void wrappaAlberoTitolario(final List<TitolarioDTO> rootChildren, final TreeNode node) {
		if (CollectionUtils.isNotEmpty(rootChildren)) {
			for (final TitolarioDTO t : rootChildren) {
				id2titolario.put(t.getIndiceClassificazione(), t);
				final DefaultTreeNode indiceToAdd = new DefaultTreeNode(t, node);
				if (t.getNumeroFigli() > 0) {
					new DefaultTreeNode(null, indiceToAdd);
				}
			}
		}
	}
	
	
	/**
	 * Metodo utilizzato in fase di inizializzazione per recuperare il titolario, qualora quest'ultimo sia gia presente nel detail
	 */
	private void initTitolario() {
		if (StringUtils.isNotBlank(detail.getIndiceClassificazioneFascicoloProcedimentale())) {
			final TitolarioDTO inTitolario = titolarioSRV.getNodoByIndice(utente.getIdAoo(), detail.getIndiceClassificazioneFascicoloProcedimentale());
			setTitolarioNodoSelected(inTitolario);
		}
	}
	
	/**
	 * Ottiene la descrizione del titolario.
	 * @return descrizione
	 */
	public String getDescrizioneTitolario() {
		final TitolarioDTO selected = getTitolarioNodoSelected();
		
		String descrizione = (StringUtils.isBlank(detail.getIndiceClassificazioneFascicoloProcedimentale()) ? Constants.EMPTY_STRING 
				: detail.getIndiceClassificazioneFascicoloProcedimentale());
		descrizione += (selected != null ? " " + selected.getDescrizione() :  Constants.EMPTY_STRING);
		
		return descrizione;
	}
	

	/**
	 * Ritorna il titolario corrispondente all'indice di classificazione presente nel detail.
	 * 
	 * @return
	 */
	@Override
	public TitolarioDTO getTitolarioNodoSelected() {
		final String indiceClassificazione = detail.getIndiceClassificazioneFascicoloProcedimentale();
		return id2titolario.get(indiceClassificazione);
	}
	
	
	/**
	 * Imposta l'indice di classificazione corrispondente nel detail e se non è presente registra il titolario nella mappa.
	 * 
	 * Impostare il titolario a null consiste nell'impostare indiceClassificazioneFascicoloProcedimentale a null
	 * @param inTitolario
	 */
	@Override
	public void setTitolarioNodoSelected(final TitolarioDTO inTitolario) {
		final String indiceDiClassificazione = inTitolario != null ? inTitolario.getIndiceClassificazione() : null;
		
		if (indiceDiClassificazione != null && !id2titolario.containsKey(indiceDiClassificazione)) {			
			id2titolario.put(indiceDiClassificazione, inTitolario);
		}
		
		detail.setIndiceClassificazioneFascicoloProcedimentale(indiceDiClassificazione);
	}
	
	/**
	 * Recupera il dettaglio del documento.
	 * @return dettaglio del documento
	 */
	public DetailDocumentRedDTO getDetail() {
		return detail;
	}

	/**
	 * Imposta il dettaglio del documento.
	 * @param detail dettaglio del documento
	 */
	public void setDetail(final DetailDocumentRedDTO detail) {
		this.detail = detail;
	}
}