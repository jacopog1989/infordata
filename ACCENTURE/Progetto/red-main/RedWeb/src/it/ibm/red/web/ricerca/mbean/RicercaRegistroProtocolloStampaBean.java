package it.ibm.red.web.ricerca.mbean;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.RegistroProtocolloDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.IRicercaRegistroProtocolloFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.helper.faces.MessageSeverityEnum;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;

/**
 * Bean che gestisce la ricerca registro protocollo.
 */
@Named(ConstantsWeb.MBean.RICERCA_REGISTRO_PROTOCOLLO_STAMPA_BEAN)
@ViewScoped
public class RicercaRegistroProtocolloStampaBean extends AbstractBean {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 6800482835563323513L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaRegistroProtocolloStampaBean.class);
	
	/**
	 * Utente.
	 */
	private UtenteDTO utente;
	
	/**
	 * Servizio.
	 */
	private IRicercaRegistroProtocolloFacadeSRV ricercaRegistroProtocolloSRV;
	
	/**
	 * Servizio.
	 */
	private IDocumentoFacadeSRV documentoSRV;
	
	/**
	 * Datatable protocolli.
	 */
	private SimpleDetailDataTableHelper<RegistroProtocolloDTO> protocolliStampaDTH;
	
	/**
	 * Parametri ricerca.
	 */
	private ParamsRicercaAvanzataDocDTO parametri;
	
	/**
	 * Data da.
	 */
	private Date dataCreazioneDa;

	/**
	 * Data a.
	 */
	private Date dataCreazioneA;
	
	/**
	 * Flag mostra risultati.
	 */
	private boolean showResults;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	public void postConstruct() {
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		ricercaRegistroProtocolloSRV = ApplicationContextProvider.getApplicationContext().getBean(IRicercaRegistroProtocolloFacadeSRV.class);
		documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);
		
		protocolliStampaDTH = new SimpleDetailDataTableHelper<>();
		
		initForm();
	}
	
	/**
	 * Inizializza il form della ricerca.
	 */
	public void initForm() {
		try {
			
			dataCreazioneA = new Date();
			
			if (parametri == null) {
				parametri = new ParamsRicercaAvanzataDocDTO();
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore nel caricamento della maschera di ricerca del Registro Protocollo.");
		}
	}
	
	/**
	 * Esegue una validazione dei campi comunicando eventualmente errori riscontrati.
	 */
	public void checkFieldValue() {
		try {
			
			if (dataCreazioneDa != null) {
				if (checkDate()) {
					cerca();
				} 
			} else {
				showError("Inserire almeno una data di inizio.");
			}
			
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante il controllo dei campi.", e);
			showError("Errore durante il controllo dei campi.");
		}
		
	}
	
	/**
	 * Gestisce la ricerca per registro protocollo.
	 */
	public void cerca() {
		try {
			
			if (TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo())) {
				showWarnMessage("Non è possibile eseguire l'operazione richiesta in regime di emergenza");
				return;
			}
			
			parametri = new ParamsRicercaAvanzataDocDTO();
			
			parametri.setDataCreazioneDa(dataCreazioneDa);
			parametri.setDataCreazioneA(dataCreazioneA);
			
			final Collection<RegistroProtocolloDTO> resultsRegistroProtocollo = ricercaRegistroProtocolloSRV.eseguiRicercaPerStampaRegistroProtocollo(parametri, utente);
			
			protocolliStampaDTH.setMasters(resultsRegistroProtocollo);
			protocolliStampaDTH.selectFirst(true);
			
			showResults = true;
			
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di ricerca.");
		}
	}
	
	/**
	 * Gestisce la selezione della riga dei protocolli stampa.
	 * @param se
	 */
	public final void protocolliStampaRowSelector(final SelectEvent se) {
		protocolliStampaDTH.rowSelector(se);
	}
	
	private boolean checkDate() {
		boolean isOk = false;
		
		if (dataCreazioneA.after(dataCreazioneDa)) {
			isOk = true;
		} else {
			showError("Inserire un range di date valido!.");
		}
		
		return isOk;
	}
	
	/**
	 * Restituisce uno StreamedContent che consente il download del content del registro protocollo.
	 * @param registro
	 * @return StreamedContent per download
	 */
	public StreamedContent scaricaRegistro(final RegistroProtocolloDTO registro) {
        StreamedContent sc = null;
        try {
			final FileDTO file = documentoSRV.getRegistroProtocolloContentPreview(utente.getFcDTO(), registro.getDocumentTitle(), utente.getIdAoo());
            final InputStream io = new ByteArrayInputStream(file.getContent());
            sc = new DefaultStreamedContent(io, file.getMimeType(), file.getFileName());
        } catch (final Exception e) {
        	LOGGER.error("Impossibile recuperare il registro dei protocolli", e);
            FacesHelper.showMessage(null, "Impossibile recuperare il registro dei protocolli", MessageSeverityEnum.ERROR);
        }
        return sc;
    }
	
	/* GET & SET */
	/**
	 * Reimposta i campi che definiscono i parametri della ricerca.
	 */
	public void resetFields() {
		parametri = new ParamsRicercaAvanzataDocDTO();
	}

	/**
	 * @return the dataCreazioneDa
	 */
	public final Date getDataCreazioneDa() {
		return dataCreazioneDa;
	}


	/**
	 * @param dataCreazioneDa the dataCreazioneDa to set
	 */
	public final void setDataCreazioneDa(final Date dataCreazioneDa) {
		this.dataCreazioneDa = dataCreazioneDa;
	}


	/**
	 * @return the dataCreazioneA
	 */
	public final Date getDataCreazioneA() {
		return dataCreazioneA;
	}


	/**
	 * @param dataCreazioneA the dataCreazioneA to set
	 */
	public final void setDataCreazioneA(final Date dataCreazioneA) {
		this.dataCreazioneA = dataCreazioneA;
	}


	/**
	 * @return the protocolliStampaDTH
	 */
	public final SimpleDetailDataTableHelper<RegistroProtocolloDTO> getProtocolliStampaDTH() {
		return protocolliStampaDTH;
	}


	/**
	 * @param protocolliStampaDTH the protocolliStampaDTH to set
	 */
	public final void setProtocolliStampaDTH(final SimpleDetailDataTableHelper<RegistroProtocolloDTO> protocolliStampaDTH) {
		this.protocolliStampaDTH = protocolliStampaDTH;
	}


	/**
	 * @return the showResults
	 */
	public final boolean isShowResults() {
		return showResults;
	}


	/**
	 * @param showResults the showResults to set
	 */
	public final void setShowResults(final boolean showResults) {
		this.showResults = showResults;
	}

	/**
	 * Restituisce il nome del file per l'export della ricerca.
	 * @return nome file export ricerca
	 */
	public String getNomeFileExportCoda() {
		
		String nome = "";
		if (utente != null) {
			nome += utente.getNome().toLowerCase() + "_" + utente.getCognome().toLowerCase() + "_";
		}
		
		nome += "ricercaregistroprotocollo_";
		
		final SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH.mm");
		nome += sdf.format(new Date()); 
		
		return nome;
	}
	
}