package it.ibm.red.web.document.mbean.component;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.RicercaRubricaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.TipologiaIndirizzoEmailEnum;
import it.ibm.red.business.enums.StatoNotificaContattoEnum;
import it.ibm.red.business.enums.TipoOperazioneEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.NotificaContatto;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IRubricaFacadeSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * Component gestione rubrica.
 */
public class RubricaComponent extends AbstractComponent {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -5573789992450244414L;

	/**
	 * Label Errore.
	 */
	private static final String ERRORE = "Errore: ";

	/**
	 * @param contatto
	 * @return
	 */
	public static boolean valida(final Contatto contatto, final FacesContext context) {

		boolean isOK = false;
		boolean campiObbligatoriOK = false;
		boolean fisicoOGiuridico = true;
		boolean procedi = false;

		if (contatto.getTipoPersona() == null || (!"F".equals(contatto.getTipoPersona()) && !"G".equals(contatto.getTipoPersona()))) {
			fisicoOGiuridico = false;
			if (context != null) {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ERRORE, "Il contatto deve essere necessariamente Fisico o Giuridico"));
			}
		}

		if ("F".equals(contatto.getTipoPersona())) {
			if ((!StringUtils.isNullOrEmpty(contatto.getNome()) && !StringUtils.isNullOrEmpty(contatto.getCognome())) && (!StringUtils.isNullOrEmpty(contatto.getMail())
					|| !StringUtils.isNullOrEmpty(contatto.getMailPec()) || !StringUtils.isNullOrEmpty(contatto.getIndirizzo()))) {
				campiObbligatoriOK = true;
			}
		} else {
			if (!StringUtils.isNullOrEmpty(contatto.getNome()) && (!StringUtils.isNullOrEmpty(contatto.getMail()) || !StringUtils.isNullOrEmpty(contatto.getMailPec()))) {
				campiObbligatoriOK = true;
			}
		}

		final String valuePEC = contatto.getMailPec();
		final String valuePEO = contatto.getMail();

		final Pattern patternEmail = Pattern.compile("^[a-zA-Z0-9_\\.-]+@[a-zA-Z0-9-_\\.]+[a-zA-Z0-9-_]\\.[a-zA-Z0-9-]+$");
		// E' considerata OK anche se il campo è vuoto
		boolean flagMailOk = true;
		// E' considerata OK anche se il campo è vuoto
		boolean flagMailPecOk = true;

		// controllo formato E-Mail PEO
		if (valuePEO != null && !valuePEO.isEmpty() && !patternEmail.matcher(valuePEO).matches()) {
			flagMailOk = false;
		}
		// controllo formato E-Mail PEC
		if (valuePEC != null && !valuePEC.isEmpty() && !patternEmail.matcher(valuePEC).matches()) {
			flagMailPecOk = false;
		}

		if (context != null && !campiObbligatoriOK) {
			if ("F".equals(contatto.getTipoPersona())) {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ERRORE, "Compilare almeno uno dei campi obbligatori: mail , pec , indirizzo"));
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ERRORE, "Compilare almeno uno dei campi obbligatori: mail , pec "));
			}
		}

		if (flagMailOk && flagMailPecOk) {
			isOK = true;
		} else if (context != null) {
			if (!flagMailPecOk) {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ERRORE, "la PEC non ha il formato corretto!"));
			}
			if (!flagMailOk) {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ERRORE, "la PEO non ha il formato corretto!"));
			}
		}

		procedi = isOK && campiObbligatoriOK && fisicoOGiuridico;

		if (procedi && StringUtils.isNullOrEmpty(contatto.getAliasContatto())) {
			if (StringUtils.isNullOrEmpty(contatto.getCognome())) {
				contatto.setAliasContatto(contatto.getNome());
			} else {
				contatto.setAliasContatto(contatto.getCognome() + " " + contatto.getNome());
			}
		}

		return procedi;
	}

	/**
	 * @param contatto
	 * @param utente
	 */
	public static void inserisciContatto(final Contatto contatto, final UtenteDTO utente) {
		try {
			contatto.setIdAOO(utente.getIdAoo());
			// si può solo inserire un contatto sulla rubrica RED
			// viene automaticamente considerato pubblico
			contatto.setTipoRubrica("RED");
			contatto.setPubblico(1);
			contatto.setDatacreazione(new Date());
			Long idUfficio = null;
			if (contatto.getIsPreferito() != null && contatto.getIsPreferito()) {
				idUfficio = utente.getIdUfficio();
			}

			final IRubricaFacadeSRV rubSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaFacadeSRV.class);
			rubSRV.inserisci(idUfficio, contatto, utente.getCheckUnivocitaMail());

		} catch (final Exception e) {
			throw new RedException(e);
		}

	}

	/**
	 * Gestisce la modifica del contatto.
	 * 
	 * @param contatto
	 * @param utente
	 */
	public static void modificaContatto(final Contatto contatto, final UtenteDTO utente) {
		try {

			final IRubricaFacadeSRV rubSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaFacadeSRV.class);
			contatto.setIdAOO(utente.getIdAoo());
			rubSRV.modifica(contatto, null, false, utente.getCheckUnivocitaMail());
			if (contatto.getIsPreferito() != null && contatto.getIsPreferito()) {
				rubSRV.aggiungiAiPreferiti(utente.getIdUfficio(), contatto.getContattoID(), contatto.getTipoRubricaEnum(), utente.getId());
			} else {
				rubSRV.rimuoviPreferito(utente.getIdUfficio(), contatto.getContattoID(), contatto.getTipoRubricaEnum());
			}

		} catch (final Exception e) {
			final String errorCode = "MAILESISTENTE";
			if (e.getMessage() != null && e.getMessage().contains(errorCode)) {
				throw new RedException(errorCode + "Non è possibile modificare il contatto in quanto già è presente un indirizzo con la stessa mail", e);
			} else {
				throw new RedException(e);
			}
		}
	}

	/**
	 * Aggiunge i contatti al gruppo ed aggiorna i destinatari in modifica.
	 * 
	 * @param fromCreazioneDocumento
	 * @param gruppoSelected
	 * @param idUfficio
	 * @param destinatariInModifica
	 * @param context
	 * @return destinatari in modifica aggiornati
	 */
	public static List<DestinatarioRedDTO> addContattiGruppo(final boolean fromCreazioneDocumento, final Contatto gruppoSelected, final Long idUfficio,
			List<DestinatarioRedDTO> destinatariInModifica, final FacesContext context) {
		final IRubricaFacadeSRV rubSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaFacadeSRV.class);
		RicercaRubricaDTO ricercaContattiNelGruppo;
		ricercaContattiNelGruppo = new RicercaRubricaDTO();
		ricercaContattiNelGruppo.setRicercaRED(true);
		ricercaContattiNelGruppo.setRicercaIPA(true);
		ricercaContattiNelGruppo.setRicercaMEF(true);
		ricercaContattiNelGruppo.setRicercaGruppo(false);
		ricercaContattiNelGruppo.setIdGruppo(gruppoSelected.getContattoID());
		gruppoSelected.setDisabilitaElemento(true);
		final List<Contatto> contattiNelGruppo = rubSRV.ricerca(idUfficio, ricercaContattiNelGruppo);

		if (!it.ibm.red.business.utils.CollectionUtils.isEmptyOrNull(contattiNelGruppo)) {
			destinatariInModifica = getDestinatariInModificaPuliti(destinatariInModifica);
			for (final Contatto contattoNelGruppo : contattiNelGruppo) {
				final DestinatarioRedDTO dest = new DestinatarioRedDTO();
				dest.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
				dest.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
				if (StringUtils.isNullOrEmpty(contattoNelGruppo.getMail()) && StringUtils.isNullOrEmpty(contattoNelGruppo.getMailPec())) {
					dest.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.CARTACEO);
				} else {
					dest.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.ELETTRONICO);
					if(!StringUtils.isNullOrEmpty(contattoNelGruppo.getMailPec())) {
						contattoNelGruppo.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEC);
					}else {
						contattoNelGruppo.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEO);
					}
				}
				if (fromCreazioneDocumento) {
					dest.setMezzoSpedizioneVisible(true);
				}
				contattoNelGruppo.setIdGruppoSelected(gruppoSelected.getContattoID());
				contattoNelGruppo.setNomeGruppoSelected(gruppoSelected.getAliasContatto());
				dest.setContatto(contattoNelGruppo);
				destinatariInModifica.add(dest);
			}

		} else {
			if (context != null) {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ERRORE, "Non è possibile inserire il gruppo poichè non ci sono contatti all'interno"));
			}
		}
		return destinatariInModifica;
	}

	/**
	 * Restituisce i destinatari in modifica puliti.
	 * 
	 * @param destinatariInModifica
	 * @return destinatari in modifica aggiornati
	 */
	public static List<DestinatarioRedDTO> getDestinatariInModificaPuliti(final List<DestinatarioRedDTO> destinatariInModifica) {
		final List<DestinatarioRedDTO> listaPulita = new ArrayList<>();
		if (destinatariInModifica != null) {
			for (final DestinatarioRedDTO dest : destinatariInModifica) {
				if (dest != null && dest.getContatto() != null) {
					listaPulita.add(dest);
				}
			}
		}

		return listaPulita;
	}

	/**
	 * Rimuove i destinatari appartenenti al gruppo dalla lista dei destinatari.
	 * 
	 * @param contattoSelected
	 * @param destinatari
	 * @return la lista dei destinatari dopo la modifica
	 */
	public static List<DestinatarioRedDTO> deleteGruppoDaSingoloContatto(final Contatto contattoSelected, final List<DestinatarioRedDTO> destinatari) {
		if (!it.ibm.red.business.utils.CollectionUtils.isEmptyOrNull(destinatari)) {
			final Iterator<DestinatarioRedDTO> itr = destinatari.iterator();
			while (itr.hasNext()) {
				final DestinatarioRedDTO destinatario = itr.next();
				if (destinatario.getContatto() != null && destinatario.getContatto().getIdGruppoSelected() != null
						&& destinatario.getContatto().getIdGruppoSelected().equals(contattoSelected.getIdGruppoSelected())) {
					if (destinatario.getContatto().getOnTheFly() != null && destinatario.getContatto().getOnTheFly() == 1) {
						destinatario.getContatto().setIdGruppoSelected(null);
						destinatario.getContatto().setNomeGruppoSelected(null);
					} else {
						itr.remove();
					}
				}
			}
		}
		return destinatari;
	}

	/**
	 * Esegue la ricerca dei contatti nel gruppo selezionato.
	 * 
	 * @param gruppoSelected
	 * @param idUfficio
	 * @return contatti ottenuti dalla ricerca
	 */
	public static List<Contatto> ricercaContattiNelGruppo(final Contatto gruppoSelected, final Long idUfficio) {
		final IRubricaFacadeSRV rubSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaFacadeSRV.class);
		ArrayList<Contatto> contattiNelGruppo = null;
		RicercaRubricaDTO ricercaContattiNelGruppo;
		ricercaContattiNelGruppo = new RicercaRubricaDTO();
		ricercaContattiNelGruppo.setRicercaRED(true);
		ricercaContattiNelGruppo.setRicercaIPA(true);
		ricercaContattiNelGruppo.setRicercaMEF(true);
		ricercaContattiNelGruppo.setRicercaGruppo(false);
		ricercaContattiNelGruppo.setIdGruppo(gruppoSelected.getContattoID());

		contattiNelGruppo = (ArrayList<Contatto>) rubSRV.ricerca(idUfficio, ricercaContattiNelGruppo);
		return contattiNelGruppo;
	}

	/**
	 * Invia la notifica di eliminazione.
	 * 
	 * @param eliminaContattoItem
	 * @param nota
	 * @param utente
	 */
	public static void inviaNotificaEliminazione(final Contatto eliminaContattoItem, final String nota, final UtenteDTO utente) {
		try {
			final IRubricaFacadeSRV rubSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaFacadeSRV.class);
			final boolean isRichiestaCreazione = false;
			final boolean isRichiestaModifica = false;
			final boolean isRichiestaEliminazione = true;
			inserisciNotifica(eliminaContattoItem, null, isRichiestaCreazione, isRichiestaModifica, isRichiestaEliminazione, nota, utente, rubSRV);
		} catch (final Exception e) {
			throw new RedException(e);
		}
	}

	/**
	 * Invia la notifica di creazione.
	 * 
	 * @param inserisciContattoItem
	 * @param nota
	 * @param utente
	 * @param context
	 * @return true se possibile proseguire, false altrimenti
	 */
	public static boolean inviaNotificaCreazione(final Contatto inserisciContattoItem, final String nota, final UtenteDTO utente, final FacesContext context) {
		boolean prosegui = true;
		try {
			final IRubricaFacadeSRV rubSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaFacadeSRV.class);
			final boolean isRichiestaCreazione = true;
			final boolean isRichiestaModifica = false;
			final boolean isRichiestaEliminazione = false;
			prosegui = valida(inserisciContattoItem, context);
			if (prosegui) {
				if (utente.getCheckUnivocitaMail()) {
					rubSRV.checkMailPerContattoGiaPresentePerAoo(inserisciContattoItem, isRichiestaModifica);
				}
				final Long idContattoRichiestaCreazione = rubSRV.inserisciContattoPerRichiestaCreazione(inserisciContattoItem, isRichiestaCreazione);
				inserisciContattoItem.setContattoID(idContattoRichiestaCreazione);
				inserisciNotifica(inserisciContattoItem, null, isRichiestaCreazione, isRichiestaModifica, isRichiestaEliminazione, nota, utente, rubSRV);
			}
		} catch (final Exception e) {
			throw new RedException(e);
		}
		return prosegui;
	}

	/**
	 * Invia la notifica di modifica.
	 * 
	 * @param modificaContattoItem
	 * @param vecchioContatto
	 * @param nota
	 * @param utente
	 * @param context
	 */
	public static void inviaNotificaModifica(final Contatto modificaContattoItem, final Contatto vecchioContatto, final String nota, final UtenteDTO utente,
			final FacesContext context) {
		boolean prosegui = true;
		try {
			final IRubricaFacadeSRV rubSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaFacadeSRV.class);
			final boolean isRichiestaCreazione = false;
			final boolean isRichiestaModifica = true;
			final boolean isRichiestaEliminazione = false;
			prosegui = valida(modificaContattoItem, context);
			if (!prosegui) {
				return;
			}

			if (utente.getCheckUnivocitaMail()) {
				rubSRV.checkMailPerContattoGiaPresentePerAoo(modificaContattoItem, isRichiestaModifica);
			}
			inserisciNotifica(modificaContattoItem, vecchioContatto, isRichiestaCreazione, isRichiestaModifica, isRichiestaEliminazione, nota, utente, rubSRV);
		} catch (final Exception e) {
			throw new RedException(e);
		}
	}

	private static NotificaContatto inserisciNotifica(final Contatto contattoItem, final Contatto contattoVecchio, final boolean isRichiestaCreazione,
			final boolean isRichiestaModifica, final boolean isRichiestaEliminazione, final String nota, final UtenteDTO utente, final IRubricaFacadeSRV rubSRV) {
		final NotificaContatto notifica = new NotificaContatto();
		notifica.setNuovoContatto(contattoItem);
		notifica.setTipoNotifica(NotificaContatto.TIPO_NOTIFICA_RICHIESTA);
		notifica.setAlias(contattoItem.getAliasContatto());
		notifica.setCognome(contattoItem.getCognome());
		notifica.setNome(contattoItem.getNome());
		notifica.setDataOperazione(new Date());
		notifica.setIdContatto(contattoItem.getContattoID());

		notifica.setIdNodoModifica(utente.getIdUfficio());
		notifica.setIdUtenteModifica(utente.getId());
		notifica.setNote(nota);
		notifica.setTipologiaContatto(contattoItem.getTipoRubrica());
		notifica.setUtente(utente.getUsername());
		notifica.setStato(StatoNotificaContattoEnum.IN_ATTESA);
		notifica.setIdAoo(utente.getIdAoo());
		if (isRichiestaCreazione) {
			notifica.setOperazioneEnum(TipoOperazioneEnum.RICHIESTA_CREAZIONE_CONTATTO);
			rubSRV.notificaCreazioneContatto(notifica);
		} else if (isRichiestaModifica) {
			if (contattoVecchio != null) {
				rubSRV.notificaModificaContatto(notifica, contattoVecchio);
			}
		} else if (isRichiestaEliminazione) {
			notifica.setOperazioneEnum(TipoOperazioneEnum.ELIMINA_CONTATTO);
			rubSRV.notificaEliminazioneContatto(notifica);
		}
		return notifica;
	}
}
