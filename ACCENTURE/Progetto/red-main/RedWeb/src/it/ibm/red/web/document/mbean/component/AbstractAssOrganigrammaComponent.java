package it.ibm.red.web.document.mbean.component;

import java.util.ArrayList;
import java.util.List;

import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipologiaNodoEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.web.document.mbean.interfaces.IAssOrganigrammaComponent;

/**
 * @author m.crescentini
 *
 */
public abstract class AbstractAssOrganigrammaComponent extends AbstractComponent implements IAssOrganigrammaComponent {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -5210528877897084808L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AbstractAssOrganigrammaComponent.class.getName());

	/**
	 * Lista nodi.
	 */
	protected transient List<Nodo> alberaturaNodi;
	
	/**
	 * Nodo root organigramma.
	 */
	private transient TreeNode rootOrganigramma;

	/**
	 * Servizio gestione organigramma.
	 */
	private IOrganigrammaFacadeSRV organigrammaSRV;
	
	/**
	 * Nodo organigramma selezionato.
	 */
	private NodoOrganigrammaDTO selected;
	
	/**
	 * Descrizione assegnatario.
	 */
	private String descrizioneAssegnatario;
	
	/**
	 * Utente.
	 */
	private UtenteDTO utente;
	
	/**
	 * ricerca Disattivi.
	 */
	private boolean ricercaDisattivi;
	
	/**
	 * Definisce la logica di recupero del primo livello dell'organigramma (appena sotto la radice).
	 * 
	 * @param radice
	 * @return
	 */
	protected abstract List<NodoOrganigrammaDTO> getPrimoLivelloOrganigramma(NodoOrganigrammaDTO radice);
	
	
	/**
	 * Definisce la logica di recupero del secondo livello dell'organigramma.
	 * 
	 * @param nodoPrimoLivello
	 * @return
	 */
	protected abstract List<NodoOrganigrammaDTO> getSecondoLivelloOrganigramma(NodoOrganigrammaDTO nodoPrimoLivello);
	
	/**
	 * Definisce se gli utenti sono visibili nell'organigramma.
	 * 
	 * @return
	 */
	protected abstract boolean isOrganigrammaConUtentiVisibili();
	
	/**
	 * @return
	 */
	protected abstract boolean isRootOrganigrammaSelezionabile();

	
	/**
	 * Inizializzazione.
	 * 
	 * @param inUtente
	 *            utente da inizializzare
	 */
	protected void init(final UtenteDTO inUtente) {
		utente = inUtente;
		organigrammaSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
		alberaturaNodi = organigrammaSRV.getAlberaturaBottomUp(utente.getIdAoo(), utente.getIdUfficio());
	}
	
	
	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.IAssOrganigrammaComponent#loadRootOrganigramma().
	 */
	@Override
	public void loadRootOrganigramma() {
		try {			
			int index = 0;
			final List<Long> alberatura = new ArrayList<>();
			for (index = alberaturaNodi.size() - 1; index >= 0; index--) {
				alberatura.add(alberaturaNodi.get(index).getIdNodo());
			}
			
			// Creazione della struttura del tree
			rootOrganigramma = new DefaultTreeNode();
			rootOrganigramma.setExpanded(true);
			rootOrganigramma.setSelectable(isRootOrganigrammaSelezionabile());
			rootOrganigramma.setType("UFF");
			
			final NodoOrganigrammaDTO radice = new NodoOrganigrammaDTO();
			// Si imposta la radice dell'organigramma
			radice.setIdAOO(getUtente().getIdAoo());
			radice.setIdNodo(getUtente().getIdNodoRadiceAOO());
			radice.setCodiceAOO(getUtente().getCodiceAoo());
			radice.setUtentiVisible(isOrganigrammaConUtentiVisibili());
			radice.setDescrizioneNodo(getUtente().getNodoDesc());
			
			final List<NodoOrganigrammaDTO> primoLivello = getPrimoLivelloOrganigramma(radice);
			
			// Gestione Primo Livello -> START
			// Per ogni nodo di primo livello si crea il nodo da mettere nell'albero che ha come padre nodoPadre
			List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
			
			DefaultTreeNode nodoDaAggiungere = null;
			for (final NodoOrganigrammaDTO n : primoLivello) {
				
				n.setCodiceAOO(getUtente().getCodiceAoo());
				n.setUtentiVisible(isOrganigrammaConUtentiVisibili());
				nodoDaAggiungere = new DefaultTreeNode(n, rootOrganigramma);
				nodoDaAggiungere.setExpanded(true);
				if (TipologiaNodoEnum.UTENTE.equals(n.getTipoNodo())) {
					nodoDaAggiungere.setType("USER");
				} else {
					nodoDaAggiungere.setType("UFF");
				}
				
				if (n.getIdUtente() == null && alberatura.contains(n.getIdNodo())) {
					nodiDaAprire.add(nodoDaAggiungere);
				}
				
			}
			// Gestione Primo Livello -> END
			
			final List<DefaultTreeNode> nodiDaAprireTemp = new ArrayList<>();
			for (index = 0; index < alberatura.size(); index++) {
				// Il numero di cicli è esattamente uguale alla lunghezza dell'alberatura
				nodiDaAprireTemp.clear();
				for (final DefaultTreeNode nodo : nodiDaAprire) {
					
					final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) nodo.getData();
					onOpenNodo(nodoExp, nodo);
					nodo.setExpanded(true);
					
					for (final TreeNode nodoFiglio : nodo.getChildren()) {
						final NodoOrganigrammaDTO nodoFiglioDTO = (NodoOrganigrammaDTO) nodoFiglio.getData();
						if (nodoFiglioDTO.getIdUtente() == null && alberatura.contains(nodoFiglioDTO.getIdNodo())) {
							nodiDaAprireTemp.add((DefaultTreeNode) nodoFiglio);
						}
					}
					
				}
				nodiDaAprire = new ArrayList<>(nodiDaAprireTemp);
			}
			
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage(e);
		}
	}
	
	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.IAssOrganigrammaComponent#onOpenNodo(org.primefaces.event.NodeExpandEvent).
	 */
	@Override
	public void onOpenNodo(final NodeExpandEvent event) {
		final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) event.getTreeNode().getData();
		onOpenNodo(nodoExp, event.getTreeNode());
	}
	
	/**
	 * Definisce la logica di espansione (recupero nodi figli) di un nodo
	 * dell'organigramma.
	 * 
	 * @param nodoExp
	 * @param treeNode
	 */
	protected void onOpenNodo(final NodoOrganigrammaDTO nodoExp, final TreeNode treeNode) {
		try {
			// Gestione Secondo Livello -> START
			// Tramite il DTO si recuperano tutti i figli di questo nodo
			final List<NodoOrganigrammaDTO> figli = getSecondoLivelloOrganigramma(nodoExp);
			
			// Si puliscono eventuali figli nel nodo
			treeNode.getChildren().clear();
			
			TreeNode nodoFiglio = null;
			for (final NodoOrganigrammaDTO figlio : figli) {
				
				figlio.setCodiceAOO(getUtente().getCodiceAoo());
				figlio.setUtentiVisible(isOrganigrammaConUtentiVisibili());
				
				// Con ogni figlio ottenuto creo un nuovo nodo da assegnare al padre
				nodoFiglio = new DefaultTreeNode(figlio, treeNode);
				// Si controlla se è un utente e nel caso setto il type
				if (TipologiaNodoEnum.UTENTE.equals(figlio.getTipoNodo())) {
					nodoFiglio.setType("USER");
				} else {
					nodoFiglio.setType("UFF");
					
					// Si aggiunge un nodo "vuoto" per permettere la successiva espansione del nodoFiglio
					if (figlio.getFigli() > 0 || figlio.isUtentiVisible()) {
						new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoFiglio);
					}
				}
				
			}
			// Gestione Secondo Livello -> END
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage(e);
		}
	}
	
	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.IAssOrganigrammaComponent#onSelectNodo(org.primefaces.event.NodeSelectEvent).
	 */
	@Override
	public void onSelectNodo(final NodeSelectEvent event) {
		// Recupero l'utente selezionato dall'evento e lo utilizzo poi per l'invocazione del service
		selected = (NodoOrganigrammaDTO) event.getTreeNode().getData();
		
		// Dall'oggetto recuperato ottengo i dati da mostrare nella casella di testo
		if (TipologiaNodoEnum.UFFICIO.equals(selected.getTipoNodo())) {
			descrizioneAssegnatario = selected.getDescrizioneNodo();
		} else {
			descrizioneAssegnatario = selected.getDescrizioneNodo() + " - " + getDescrizioneUtenteSelected();
		}
	}
	
	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.IAssOrganigrammaComponent#pulisciSelezioneAssegnatario().
	 */
	@Override
	public void pulisciSelezioneAssegnatario() {
		selected = null;
		descrizioneAssegnatario = Constants.EMPTY_STRING;
	}
	
	
	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.IAssOrganigrammaComponent#reset().
	 */
	@Override
	public void reset() {
		loadRootOrganigramma();
		pulisciSelezioneAssegnatario();
	}
	
	
	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.IAssOrganigrammaComponent#getSelected().
	 */
	@Override
	public NodoOrganigrammaDTO getSelected() {
		return selected;
	}

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.IAssOrganigrammaComponent#getDescrizioneAssegnatario().
	 */
	@Override
	public String getDescrizioneAssegnatario() {
		return descrizioneAssegnatario;
	}
	
	
	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.IAssOrganigrammaComponent#checkSelezionato().
	 */
	@Override
	public boolean checkSelezionato() {
		boolean selezionato = false;
		
		if (getSelected() != null && (getSelected().getIdUtente() != null || getSelected().getIdNodo() != null)) {
			selezionato = true;
		}
		
		return selezionato;
	}
	
	
	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.IAssOrganigrammaComponent#getDescrizioneNodoSelected().
	 */
	@Override
	public String getDescrizioneNodoSelected() {
		String descrizioneNodoSelected = Constants.EMPTY_STRING;
		
		if (selected != null) {
			descrizioneNodoSelected = selected.getDescrizioneNodo();
		}
		
		return descrizioneNodoSelected;
	}
	

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.IAssOrganigrammaComponent#getDescrizioneUtenteSelected().
	 */
	@Override
	public String getDescrizioneUtenteSelected() {
		String descrizioneUtenteSelected = Constants.EMPTY_STRING;
		
		if (selected != null) {
			descrizioneUtenteSelected = selected.getCognomeUtente() + " " + selected.getNomeUtente();
		}
		
		return descrizioneUtenteSelected;
	}
	
	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.IAssOrganigrammaComponent#getRootOrganigramma().
	 */
	@Override
	public final TreeNode getRootOrganigramma() {
		return rootOrganigramma;
	}

	/**
	 * Imposta la root dell'organigramma.
	 * 
	 * @param rootOrganigramma
	 *            la root dell'organigramma da impostare
	 */
	public void setRootOrganigramma(final TreeNode rootOrganigramma) {
		this.rootOrganigramma = rootOrganigramma;
	}

	/**
	 * @return
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * @return
	 */
	public IOrganigrammaFacadeSRV getOrganigrammaSRV() {
		return organigrammaSRV;
	}


	/**
	 * @param alberaturaNodi
	 */
	public void setAlberaturaNodi(final List<Nodo> alberaturaNodi) {
		this.alberaturaNodi = alberaturaNodi;
	}


	/**
	 * @param organigrammaSRV
	 */
	public void setOrganigrammaSRV(final IOrganigrammaFacadeSRV organigrammaSRV) {
		this.organigrammaSRV = organigrammaSRV;
	}


	/**
	 * @param selected
	 */
	public void setSelected(final NodoOrganigrammaDTO selected) {
		this.selected = selected;
	}


	/**
	 * @param descrizioneAssegnatario
	 */
	public void setDescrizioneAssegnatario(final String descrizioneAssegnatario) {
		this.descrizioneAssegnatario = descrizioneAssegnatario;
	}


	/**
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}


	/**
	 * @return the alberaturaNodi
	 */
	public List<Nodo> getAlberaturaNodi() {
		return alberaturaNodi;
	}


	public boolean isRicercaDisattivi() {
		return ricercaDisattivi;
	}


	public void setRicercaDisattivi(boolean ricercaDisattivi) {
		this.ricercaDisattivi = ricercaDisattivi;
	}
	
	
}
