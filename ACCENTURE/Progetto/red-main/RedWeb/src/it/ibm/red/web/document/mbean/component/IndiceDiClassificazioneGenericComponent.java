package it.ibm.red.web.document.mbean.component;

import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.interfaces.ISelezionaTitolarioChiamante;

/**
 * @author m.crescentini
 */
public class IndiceDiClassificazioneGenericComponent extends AbstractIndiceDiClassificazioneComponent {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 4926123000562852921L;

    /**
     * BEan chiamante.
     */
	private final String mBeanChiamante;

    /**
     * Nodo titolario selezionato.
     */
	private TitolarioDTO titolarioNodoSelected;
	
	/**
	 * Costruttore del component.
	 * 
	 * @param mBeanChiamante
	 * @param utente
	 */
	public IndiceDiClassificazioneGenericComponent(final String mBeanChiamante, final UtenteDTO utente) {
		super(utente);
		this.mBeanChiamante = mBeanChiamante;
		creaTitolario();
	}
	
	/**
	 * @see it.ibm.red.web.document.mbean.component.AbstractIndiceDiClassificazioneComponent#setTitolarioNodoSelected(it.ibm.red.business.dto.TitolarioDTO).
	 */
	@Override
	public void setTitolarioNodoSelected(final TitolarioDTO selectedTitolario) {
		titolarioNodoSelected = selectedTitolario;
		
		final ISelezionaTitolarioChiamante beanChiamante = FacesHelper.getManagedBean(mBeanChiamante);
		
		if (beanChiamante != null) {
			beanChiamante.aggiornaIndiceClassificazione(selectedTitolario);
		}
	}

	/**
	 * @see it.ibm.red.web.document.mbean.component.AbstractIndiceDiClassificazioneComponent#getTitolarioNodoSelected().
	 */
	@Override
	public TitolarioDTO getTitolarioNodoSelected() {
		return titolarioNodoSelected;
	}
	
}