package it.ibm.red.web.scadenziario.mbean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.DocumentoScadenzatoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ScopeScadenziarioStrutturaEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IScadenziaroStrutturaFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.scadenziario.mbean.component.DocumentiScadenzatiComponent;
import it.ibm.red.web.scadenziario.mbean.component.IspettoratoLinkComponent;
import it.ibm.red.web.scadenziario.mbean.component.PeriodoScadenzaLinkComponent;
import it.ibm.red.web.scadenziario.mbean.component.SegreteriaLinkComponent;

/**
 * Bean che gestisce lo scadenziario.
 */
@Named(ConstantsWeb.MBean.SCADENZIARIO_BEAN)
@ViewScoped
public class ScadenziarioBean extends AbstractBean {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 572153209856684263L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ScadenziarioBean.class.getName());
	
	/**
	 * Utente.
	 */
	private UtenteDTO utente;
	
	/**
	 * Questi componennti concorrono nel visualizzare lo scadenzario della struttura
	 * 
	 * La gestione di questi componenti con il proprio bean è troppo accoppiata per riutilizzare i componenti servirebbe un listener che consente disaccoppiamento
	 * 
	 */
	private SegreteriaLinkComponent segreteriaComponent;
	
	/**
	 * Componente ispettorato.
	 */
	private IspettoratoLinkComponent ispettoratoComponent;
	
	/**
	 * Componente periodo scadenza.
	 */
	private PeriodoScadenzaLinkComponent periodoComponent;
	
	/**
	 * Indice tab attivo.
	 */
	private Integer activeTabIndex;
	
	/**
	 * Component che si occupa di visualizzare la dialog 
	 */
	private DocumentiScadenzatiComponent docScadenzatiComponent;  

	/**
	 * Post construct del bean.
	 */
	@PostConstruct
	@Override
	protected void postConstruct() {
		final IScadenziaroStrutturaFacadeSRV scadenziarioSRV = ApplicationContextProvider.getApplicationContext().getBean(IScadenziaroStrutturaFacadeSRV.class);
		final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sb.getUtente();
		
		final ScopeScadenziarioStrutturaEnum scope = scadenziarioSRV.getScopeScadenziarioStruttura(utente);
		
		if (scope == ScopeScadenziarioStrutturaEnum.SEGRETERIA) {
			segreteriaComponent = new SegreteriaLinkComponent(this);
		} else if (scope == ScopeScadenziarioStrutturaEnum.ISPETTORATO) {
			ispettoratoComponent = new IspettoratoLinkComponent(this);
		} else if (scope == ScopeScadenziarioStrutturaEnum.UFFICIO) {
			periodoComponent = new PeriodoScadenzaLinkComponent(this);
		} else {
			LOGGER.error("Impossibile individuare lo scopeScadenziarioStrutturaEnum per l'utente ");
		}
	}	
	
	/**
	 * Restituisce l'utente.
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta la lista dei documenti scadenziati.
	 * @param inDocumentList
	 */
	public void setDocumentList(final List<DocumentoScadenzatoDTO> inDocumentList) {
		docScadenzatiComponent = new DocumentiScadenzatiComponent(inDocumentList);
	}

	/**
	 * Restituisce il component per la gestione del collegamento segreteria.
	 * @return il component SegreteriaLinkComponent
	 */
	public SegreteriaLinkComponent getSegreteriaComponent() {
		return segreteriaComponent;
	}

	/**
	 * Restituisce il component per la gestione del periodo.
	 * @return il component PeriodoScadenzaLinkComponent
	 */
	public PeriodoScadenzaLinkComponent getPeriodoComponent() {
		return periodoComponent;
	}

	/**
	 * Imposta il component per la gestione del periodo.
	 * @param component
	 */
	public void setPeriodoComponent(final PeriodoScadenzaLinkComponent component) {
		this.periodoComponent = component;
		calculateIdTabVisible();
	}
	
	/**
	 * Restituisce il component per la gestione dell'ispettorato.
	 * @return il component IspettoratoLinkComponent
	 */
	public IspettoratoLinkComponent getIspettoratoComponent() {
		return ispettoratoComponent;
	}
	
	/**
	 * Imposta il component per la gestione dell'ispettorato.
	 * @param inIspettorato
	 */
	public void setIspettoratoComponent(final IspettoratoLinkComponent inIspettorato) {
		this.ispettoratoComponent = inIspettorato;
		this.periodoComponent =  null;
		calculateIdTabVisible();
	}
	
	/**
	 * Restituisce l'indice del tab attivo.
	 * @return indice tab attivo
	 */
	public Integer getActiveTabIndex() {
		return activeTabIndex;
	}

	/**
	 * Imposta l'indice del tab attivo.
	 * @param activeTabIndex
	 */
	public void setActiveTabIndex(final Integer activeTabIndex) {
		this.activeTabIndex = activeTabIndex;
	}

	/**
	 * Gestisce la visibilita dei tab impostando l'indice del tab attivo: {@link #activeTabIndex}.
	 */
	public void calculateIdTabVisible() {
		Integer tabVisible = 0;
		if (segreteriaComponent != null && ispettoratoComponent != null) {
			tabVisible++;
		}
		
		if (periodoComponent != null) {
			tabVisible++;
		}
		setActiveTabIndex(tabVisible);
	}

	/**
	 * Restituisce il component per la gestione dei documenti scadenzati.
	 * @return il component DocumentiScadenzatiComponent
	 */
	public DocumentiScadenzatiComponent getDocScadenzatiComponent() {
		return docScadenzatiComponent;
	}
	
}
