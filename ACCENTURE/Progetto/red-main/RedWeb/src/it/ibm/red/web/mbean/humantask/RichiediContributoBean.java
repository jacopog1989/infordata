package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ColoreNotaEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IContributoSRV;
import it.ibm.red.business.service.facade.INotaFacadeSRV;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.utils.OrganigrammaRetrieverUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;
import it.ibm.red.web.mbean.interfaces.OrganigrammaRetriever;

/**
 * Bean che gestisce la response di richiesta contributo.
 */
@Named(ConstantsWeb.MBean.RICHIEDI_CONTRIBUTO_BEAN)
@ViewScoped
public class RichiediContributoBean extends AbstractBean implements OrganigrammaRetriever, InitHumanTaskInterface {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 2322441124207642447L;

	/**
	 * Logger del Bean.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RichiediContributoBean.class);

	/**
	 * Messaggio errore generico. Occorre appendere l'operazione al messaggio.
	 */
	private static final String GENERIC_ERROR_OPERAZIONE = "Errore durante l'operazione: ";

	/**
	 * Bean Assegnazione.
	 */
	private MultiAssegnazioneBean assegnatarioBean;

	/**
	 * Bean assegnazione.
	 */
	private MultiAssegnazioneBean assegnatariBean;

	/**
	 * Messaggio.
	 */
	private String message;

	/**
	 * Documento selezoinato.
	 */
	private MasterDocumentRedDTO documentoSelezionato;

	/**
	 * Flag urgente.
	 */
	private Boolean isUrgente;

	/**
	 * Sessione bean.
	 */
	private SessionBean sb;

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Servizio.
	 */
	private IOrganigrammaFacadeSRV organigrammaSRV;

	/**
	 * Servizio.
	 */
	private IContributoSRV contributoSRV;

	/**
	 * Servizio.
	 */
	private INotaFacadeSRV notaSRV;

	/**
	 * Radice.
	 */
	private transient TreeNode root;

	/**
	 * Lista documenti bean.
	 */
	private ListaDocumentiBean ldb;

	/**
	 * Response.
	 */
	private ResponsesRedEnum responseType;

	/**
	 * Flag storico.
	 */
	private boolean alsoStorico;

	/**
	 * Alberatura nodi.
	 */
	private List<Nodo> alberaturaNodi;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
		responseType = ldb.getSelectedResponse();

		utente = sb.getUtente();

		message = "";
		isUrgente = false;

		contributoSRV = ApplicationContextProvider.getApplicationContext().getBean(IContributoSRV.class);
		organigrammaSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
		notaSRV = ApplicationContextProvider.getApplicationContext().getBean(INotaFacadeSRV.class);
		alberaturaNodi = organigrammaSRV.getAlberaturaBottomUp(utente.getIdAoo(), utente.getIdUfficio());

		// Per la nuova assegnazione (label)
		assegnatarioBean = new MultiAssegnazioneBean();
		assegnatarioBean.initialize(this);
		// per assegnatario per contributo (label)
		assegnatariBean = new MultiAssegnazioneBean();
		assegnatariBean.initialize(this);
	}

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		setDocumentoSelezionato(inDocsSelezionati.get(0));
	}

	/**
	 * Carica l'organigramma.
	 */
	@Override
	public TreeNode loadRootOrganigramma() {
		switch (responseType) {
		case RICHIEDI_CONTRIBUTO_UTENTE: // prendi l'ufficio e i suoi utenti
			root = setTreeUtente();
			break;
		case RICHIEDI_CONTRIBUTO_INTERNO:
			root = setTreeInterno();
			break;
		default:
			root = setTreeDefault();
			break;
		}

		return root;
	}

	private TreeNode setTreeUtente() {
		DefaultTreeNode inRoot = null;
		try {
			inRoot = new DefaultTreeNode("Root", null);
			inRoot.setSelectable(true);

			final NodoOrganigrammaDTO padre = new NodoOrganigrammaDTO();
			padre.setDescrizioneNodo(utente.getNodoDesc());

			// creazione dell'unico nodo necessario (ufficio dell'utente in sessione)
			final TreeNode nodoPadre = new DefaultTreeNode(padre, inRoot);
			nodoPadre.setExpanded(true);
			nodoPadre.setSelectable(false);

			// recupero degli utenti appartenenti al suo stesso ufficio
			final NodoOrganigrammaDTO dto = new NodoOrganigrammaDTO();
			dto.setIdAOO(utente.getIdAoo());
			dto.setIdNodo(utente.getIdUfficio());
			dto.setUtentiVisible(true);

			final List<NodoOrganigrammaDTO> utentiList = organigrammaSRV.getFigliAlberoAssegnatarioPerStornaAUtente(utente.getIdUfficio(), dto);
			DefaultTreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO utenteNodo : utentiList) {
				nodoToAdd = new DefaultTreeNode(utenteNodo, nodoPadre);
				nodoToAdd.setSelectable(true);
				nodoPadre.getChildren().add(nodoToAdd);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());
		}

		return inRoot;
	}

	private TreeNode setTreeInterno() {
		DefaultTreeNode inRoot = null;
		try {
			// prendo da DB i nodi di primo livello
			final NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
			nodoRadice.setIdAOO(utente.getIdAoo());
			nodoRadice.setIdNodo(utente.getIdUfficio());
			nodoRadice.setCodiceAOO(utente.getCodiceAoo());
			nodoRadice.setUtentiVisible(false);
			nodoRadice.setDescrizioneNodo(utente.getNodoDesc());
			final List<NodoOrganigrammaDTO> primoLivello = organigrammaSRV.getFigliAlberoAssegnatarioPerRichiestaVisto(utente.getIdUfficio(), nodoRadice);

			inRoot = new DefaultTreeNode();
			inRoot.setExpanded(true);
			inRoot.setSelectable(true);

			// in questo modo riesco anche a visualizzare il nodo ufficio
			final DefaultTreeNode nodoUfficio = new DefaultTreeNode(nodoRadice, inRoot);
			nodoUfficio.setExpanded(true);
			nodoUfficio.setSelectable(true);

			// per ogni nodo di primo livello creo il nodo da mettere nell'albero che ha
			// come padre rootAssegnazionePerCompetenza
			DefaultTreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(utente.getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, nodoUfficio);
				if (n.getFigli() > 0 || n.isUtentiVisible()) {
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
					n.setUtentiVisible(true);
				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());
		}

		return inRoot;
	}

	private TreeNode setTreeDefault() {
		// prendo da DB i nodi di primo livello
		DefaultTreeNode inRoot = null;
		try {
			final List<Long> alberatura = new ArrayList<>();
			for (int i = alberaturaNodi.size() - 1; i >= 0; i--) {
				alberatura.add(alberaturaNodi.get(i).getIdNodo());
			}

			final NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
			nodoRadice.setIdAOO(utente.getIdAoo());
			nodoRadice.setIdNodo(utente.getIdNodoRadiceAOO());
			nodoRadice.setCodiceAOO(utente.getCodiceAoo());
			nodoRadice.setUtentiVisible(false);
			List<NodoOrganigrammaDTO> primoLivello = null;
			primoLivello = organigrammaSRV.getFigliAlberoAssegnatarioPerConoscenza(utente.getIdUfficio(), nodoRadice, true);

			inRoot = new DefaultTreeNode();
			inRoot.setExpanded(true);
			inRoot.setSelectable(false);

			// per ogni nodo di primo livello creo il nodo da mettere nell'albero che ha
			// come padre rootAssegnazionePerCompetenza
			List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
			inRoot.setChildren(new ArrayList<>());
			DefaultTreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(utente.getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, inRoot);
				nodoToAdd.setExpanded(true);
				if (alberatura.contains(n.getIdNodo())) {
					nodiDaAprire.add(nodoToAdd);
				}
			}

			final List<DefaultTreeNode> nodiDaAprireTemp = new ArrayList<>();
			for (int i = 0; i < alberatura.size(); i++) {
				// devo fare esattamente un numero di cicli uguale alla lunghezza
				// dell'alberatura
				nodiDaAprireTemp.clear();
				for (final DefaultTreeNode nodo : nodiDaAprire) {
					onOpenTree(nodo);
					nodo.setExpanded(true);
					for (final TreeNode nodoFiglio : nodo.getChildren()) {
						final NodoOrganigrammaDTO nodoFiglioDto = (NodoOrganigrammaDTO) nodoFiglio.getData();
						if (nodoFiglioDto.getIdUtente() == null && alberatura.contains(nodoFiglioDto.getIdNodo())) {
							nodiDaAprireTemp.add((DefaultTreeNode) nodoFiglio);
						}
					}
				}
				nodiDaAprire = new ArrayList<>(nodiDaAprireTemp);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());
		}
		return inRoot;
	}

	/**
	 * Gestisce l'evento di apertura del nodo <code> node </code>.
	 * @param node nodo da aprire
	 */
	@Override
	public void onOpenTree(final TreeNode node) {
		try {
			final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) node.getData();
			final List<NodoOrganigrammaDTO> figli = organigrammaSRV.getFigliAlberoAssegnatarioPerConoscenza(utente.getIdUfficio(), nodoExp, false);

			DefaultTreeNode nodoToAdd = null;
			node.getChildren().clear();
			for (final NodoOrganigrammaDTO n : figli) {
				n.setCodiceAOO(utente.getCodiceAoo());

				nodoToAdd = new DefaultTreeNode(n, node);
				if (n.getFigli() > 0 || n.isUtentiVisible()) {
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoToAdd);
				}
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE + e.getMessage());
		}
	}

	/**
	 * Gestisce la response di contributo, esegue una validazione dei parametri
	 * mostrando eventuali errori. A valle delle operazioni gestisce gli esiti in
	 * modo da poterli visualizzare nella dialog apposita.
	 */
	public void contributoResponse() {
		EsitoOperazioneDTO eOpe = null;
		boolean mancaCampoObbligatorio = false;
		try {
			// pulisco gli esiti
			ldb.cleanEsiti();
			final String wobNumber = documentoSelezionato.getWobNumber();

			final List<NodoOrganigrammaDTO> nodoAssegnatario = assegnatarioBean.getSelectedElementList();
			final List<NodoOrganigrammaDTO> nodiAssegnatari = assegnatariBean.getSelectedElementList();

			nodoAssegnatario.addAll(nodiAssegnatari);

			responseType = ldb.getSelectedResponse();
			if (CollectionUtils.isEmpty(assegnatarioBean.getSelectedElementList())) {
				// se il campo obbligatorio non è valorizzato lo mando in errore
				final String errMsg = "E' necessario valorizzare i campi obbligatori.";
				mancaCampoObbligatorio = true;
				showError(errMsg);
				LOGGER.error(errMsg);
			} else {
				final List<AssegnatarioOrganigrammaDTO> assegnatari = nodoAssegnatario.stream().map(c -> OrganigrammaRetrieverUtils.create(c)).collect(Collectors.toList());
				// chiamo il service in base al tipo di response
				if (responseType.equals(ResponsesRedEnum.RICHIEDI_CONTRIBUTO)) {
					eOpe = contributoSRV.richiediContributo(utente, wobNumber, assegnatari, message, isUrgente);
				} else if (responseType.equals(ResponsesRedEnum.RICHIESTA_CONTRIBUTO)) {
					eOpe = contributoSRV.richiestaContributo(utente, wobNumber, assegnatari, message, isUrgente);
				} else if (responseType.equals(ResponsesRedEnum.RICHIEDI_CONTRIBUTO_INTERNO)) {
					eOpe = contributoSRV.richiediContributoInterno(utente, wobNumber, assegnatari, message, isUrgente);
				} else if (responseType.equals(ResponsesRedEnum.RICHIEDI_CONTRIBUTO_UTENTE)) {
					eOpe = contributoSRV.richiediContributoUtente(utente, wobNumber, assegnatari, message, isUrgente);
				}
				// setto gli esiti con i nuovi e eseguo un refresh
				ldb.getEsitiOperazione().add(eOpe);

				if (eOpe == null) {
					LOGGER.error("Errore nella valutazione del flag esito, l'oggetto eOpe di classe EsitoOperazioneDTO risulta null.");
					throw new RedException("Errore nella valutazione del flag esito, l'oggetto eOpe di classe EsitoOperazioneDTO risulta null.");
				}

				if (Boolean.FALSE.equals(eOpe.getFlagEsito()) && isAlsoStorico()) {
					notaSRV.registraNotaFromDocumento(utente, Integer.parseInt(documentoSelezionato.getDocumentTitle()), ColoreNotaEnum.NERO, getMessage());
				}

				FacesHelper.executeJS("PF('dlgGeneric').hide();PF('dlgShowResult').show();");
				ldb.destroyBeanViewScope(ConstantsWeb.MBean.RICHIEDI_CONTRIBUTO_BEAN);
			}
		} catch (final Exception e) {
			if (!mancaCampoObbligatorio) {
				LOGGER.error("Si è verificato un errore", e);
				showError("Si è verificato un errore");
			}
		}
	}

	/**
	 * Restituisce il messaggio.
	 * 
	 * @return messaggio
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Imposta il messaggio.
	 * 
	 * @param message
	 */
	public void setMessage(final String message) {
		this.message = message;
	}

	/**
	 * Restituisce il bean che gestisce la multi assegnazione.
	 * 
	 * @return bean multi assegnazione
	 */
	public MultiAssegnazioneBean getAssegnatariBean() {
		return assegnatariBean;
	}

	/**
	 * Imposta il bean che gestisce la multi assegnazione.
	 * 
	 * @param assegnatariBean
	 */
	public void setAssegnatariBean(final MultiAssegnazioneBean assegnatariBean) {
		this.assegnatariBean = assegnatariBean;
	}

	/**
	 * Restituisce il DTO del documento selezionato.
	 * 
	 * @return documento selezionato
	 */
	public MasterDocumentRedDTO getDocumentoSelezionato() {
		return documentoSelezionato;
	}

	/**
	 * Imposta il DTO del documento selezionato.
	 * 
	 * @param documentoSelezionato
	 */
	public void setDocumentoSelezionato(final MasterDocumentRedDTO documentoSelezionato) {
		this.documentoSelezionato = documentoSelezionato;
	}

	/**
	 * Restituisce true se urgente, false altrimenti.
	 * 
	 * @return true se urgente, false altrimenti
	 */
	public Boolean getIsUrgente() {
		return isUrgente;
	}

	/**
	 * Imposta il flag che definisce l'urgenza.
	 * 
	 * @param isUrgente
	 */
	public void setIsUrgente(final Boolean isUrgente) {
		this.isUrgente = isUrgente;
	}

	/**
	 * Restituisce l'utente.
	 * 
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'utente.
	 * 
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Restituisce il service per la gestione dell'organigramma.
	 * 
	 * @return service organigramma
	 */
	public IOrganigrammaFacadeSRV getOrganigrammaSRV() {
		return organigrammaSRV;
	}

	/**
	 * IMposta il service per la gestione dell'organigramma.
	 * 
	 * @param organigrammaSRV
	 */
	public void setOrganigrammaSRV(final IOrganigrammaFacadeSRV organigrammaSRV) {
		this.organigrammaSRV = organigrammaSRV;
	}

	/**
	 * Restituisce il service per la gestione dei contributi.
	 * 
	 * @return il service per la gestione dei contributi
	 */
	public IContributoSRV getContributoSRV() {
		return contributoSRV;
	}

	/**
	 * Imposta il service per la gestione dei contributi.
	 * 
	 * @param contributoSRV
	 */
	public void setContributoSRV(final IContributoSRV contributoSRV) {
		this.contributoSRV = contributoSRV;
	}

	/**
	 * Restitusce la root del tree.
	 * 
	 * @return root
	 */
	public TreeNode getRoot() {
		return root;
	}

	/**
	 * Imposta la root del tree.
	 * 
	 * @param root
	 */
	public void setRoot(final TreeNode root) {
		this.root = root;
	}

	/**
	 * Restituisce il session bean.
	 * 
	 * @return session bean
	 */
	public SessionBean getSb() {
		return sb;
	}

	/**
	 * Imposta il sessione bean.
	 * 
	 * @param sb
	 */
	public void setSb(final SessionBean sb) {
		this.sb = sb;
	}

	/**
	 * Restituisce il bean dell'assegnatario.
	 * 
	 * @return bean che gestisce la multi assegnazione
	 */
	public MultiAssegnazioneBean getAssegnatarioBean() {
		return assegnatarioBean;
	}

	/**
	 * Imposta il bean per la gestione dell'assegnazione multipla.
	 * 
	 * @param assegnatarioBean
	 */
	public void setAssegnatarioBean(final MultiAssegnazioneBean assegnatarioBean) {
		this.assegnatarioBean = assegnatarioBean;
	}

	/**
	 * Restituisce il bean ListaDocumentiBean.
	 * 
	 * @return ListaDocumentiBean
	 */
	public ListaDocumentiBean getLdb() {
		return ldb;
	}

	/**
	 * Imposta il bean ListaDocumentiBean.
	 * 
	 * @param ldb
	 */
	public void setLdb(final ListaDocumentiBean ldb) {
		this.ldb = ldb;
	}

	/**
	 * Restituisce il tipo della response.
	 * 
	 * @return response type
	 */
	public ResponsesRedEnum getResponseType() {
		return responseType;
	}

	/**
	 * Imposta il tipo response.
	 * 
	 * @param responseType
	 */
	public void setResponseType(final ResponsesRedEnum responseType) {
		this.responseType = responseType;
	}

	/**
	 * @return alsoStorico
	 */
	public boolean isAlsoStorico() {
		return alsoStorico;
	}

	/**
	 * @param alsoStorico
	 */
	public void setAlsoStorico(final boolean alsoStorico) {
		this.alsoStorico = alsoStorico;
	}
}
