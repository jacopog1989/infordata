package it.ibm.red.web.mbean;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IAttrExtFacadeSRV;
import it.ibm.red.business.service.facade.ITipoProcedimentoFacadeSRV;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Bean che accordi di servizio.
 */
@Named(ConstantsWeb.MBean.ACCORDI_SERVIZIO_BEAN)
@ViewScoped
public class AccordiServizioBean extends AbstractBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -126935954423748895L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AccordiServizioBean.class.getName());

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Servizio dati aggiuntivi.
	 */
	private IAttrExtFacadeSRV attrExtSRV;

	/**
	 * Servizio tipologia documento.
	 */
	private ITipologiaDocumentoFacadeSRV tipDocSRV;

	/**
	 * Servizio tipologia procedimento.
	 */
	private ITipoProcedimentoFacadeSRV tipProcSRV;
	
	/**
	 * Idnetifico tipo procedimento.
	 */
	private Integer idTipoProcedimento;

	/**
	 * Identificativo tipo documento.
	 */
	private Integer idTipoDocumento;

	/**
	 * Lista tipologie documento.
	 */
	private Collection<SelectItemDTO> comboTipologieDocumento;

	/**
	 * Lista tipologia procedimento.
	 */
	private Collection<SelectItemDTO> comboTipologieProcedimento;
	
	/**
	 * Testo input.
	 */
	private String inputTxt;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		attrExtSRV = ApplicationContextProvider.getApplicationContext().getBean(IAttrExtFacadeSRV.class);
		tipDocSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoFacadeSRV.class);
		tipProcSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipoProcedimentoFacadeSRV.class);
		loadTipoDocumento();
	}

	private void loadTipoDocumento() {
		comboTipologieDocumento = new ArrayList<>();
		final List<TipologiaDocumentoDTO> tps = tipDocSRV.getComboTipologieByAooAndTipoCategoriaAttivi(TipoCategoriaEnum.ENTRATA, utente.getIdAoo());
		for (final TipologiaDocumentoDTO tp:tps) {
			comboTipologieDocumento.add(new SelectItemDTO(tp.getIdTipologiaDocumento(), tp.getDescrizione()));
		}
	}

	/**
	 * Gestisce la logica da eseguire alla variazione del tipo documento.
	 */
	public void onChangeTipologiaDocumento() {
		comboTipologieProcedimento = new ArrayList<>();
		final List<TipoProcedimento> tps = tipProcSRV.getTipiProcedimentoByTipologiaDocumento(idTipoDocumento);
		for (final TipoProcedimento tp:tps) {
			comboTipologieProcedimento.add(new SelectItemDTO(tp.getTipoProcedimentoId(), tp.getDescrizione()));
		}
	}

	/**
	 * Restituisce uno StreamedContent in formato XML che definisce gli Accordi di Servizio per la specifica
	 * coppia tipologia documento - tipologia procedimento.
	 * @return StreamedContent
	 */
	public StreamedContent getScSample() {
		StreamedContent sc = null;
		try {
			final byte[] content = attrExtSRV.exportSample(utente, idTipoDocumento, idTipoProcedimento).getBytes();
			final InputStream io = new ByteArrayInputStream(content);
			sc = new DefaultStreamedContent(io, ContentType.XML, "sample.xml");
			showInfoMessage("Esempio generato.");
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di generazione dell'esempio", e);
			showError("Impossibile generare l'esempio.");
		}
		return sc;
	}

	/**
	 * Restituisce l'id associato alla tipologia procedimento.
	 * @return
	 */
	public Integer getIdTipoProcedimento() {
		return idTipoProcedimento;
	}

	/**
	 * Imposta l'id del tipo procedimento.
	 * @param idTipoProcedimento
	 */
	public void setIdTipoProcedimento(final Integer idTipoProcedimento) {
		this.idTipoProcedimento = idTipoProcedimento;
	}

	/**
	 * Restituisce l'id associato alla tipologia documento.
	 * @return Integer - idTipoDocumento
	 */
	public Integer getIdTipoDocumento() {
		return idTipoDocumento;
	}

	/**
	 * Imposta l'id del tipo documento.
	 * @param idTipoDocumento
	 */
	public void setIdTipoDocumento(final Integer idTipoDocumento) {
		this.idTipoDocumento = idTipoDocumento;
	}

	/**
	 * Restituisce inputTxt.
	 * @return inputTxt
	 */
	public String getInputTxt() {
		return inputTxt;
	}

	/**
	 * Imposta il parametro inputTxt.
	 * @param inputTxt
	 */
	public void setInputTxt(final String inputTxt) {
		this.inputTxt = inputTxt;
	}

	/**
	 * Restituisce la lista delle tipologie documento definita come Collection di SelectItemDTO
	 * per la creazione della combobox.
	 * @return Collection di SelectItemDTO
	 */
	public Collection<SelectItemDTO> getComboTipologieDocumento() {
		return comboTipologieDocumento;
	}

	/**
	 * Restituisce la lista delle tipologie procedimento definita come Collection di SelectItemDTO
	 * per la creazione della combobox.
	 * @return Collection di SelectItemDTO
	 */
	public Collection<SelectItemDTO> getComboTipologieProcedimento() {
		return comboTipologieProcedimento;
	}

}
