package it.ibm.red.web.mbean.component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.faces.event.ValueChangeEvent;

import org.apache.commons.collections4.CollectionUtils;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.HierarchicalFileWrapperDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.facade.IScompattaMailFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.mbean.component.AbstractComponent;
import it.ibm.red.web.dto.ListElementDTO;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.MailBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.utils.TreeNodeUtils;

/**
 * Componente che gestisce il treeTable per la selezione del pricipale e degli
 * allegati durante la protocollazione delle mail.
 * 
 * @author a.difolca
 */
public class SelezionaPrincipaleAllegatoComponent extends AbstractComponent {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -3108780804032645393L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SelezionaPrincipaleAllegatoComponent.class.getName());

	/**
	 * Messaggio errore selezione documento.
	 */
	private static final String ERROR_SELEZIONE_DOCUMENTO_MSG = "Errore durante la selezione del documento";

	/**
	 * Servizio.
	 */
	private final IScompattaMailFacadeSRV mailSRV;
	
	/**
	 * Utente.
	 */
	private final UtenteDTO utente;

	/**
	 * Mantiene tutti gli allegati di un determinato file.
	 */
	private final List<HierarchicalFileWrapperDTO> allegati;

	/**
	 * Model della treeTable.
	 */
	private transient TreeNode root;

	/**
	 * Mantiene tutti gli allegati ordinati.
	 */
	private List<ListElementDTO<HierarchicalFileWrapperDTO>> orderedAllegati;

	/**
	 * Principale selezionato. Necessario per raccogliere l'input dell'utente nelle
	 * option
	 */
	private Integer selectedPrincipale;

	/**
	 * Allegati selezionati dall'utente. Necessario per raccogliere l'input
	 * dell'utente nelle option
	 */
	private List<Integer> selectedAllegatiList;

	/**
	 * Gestisce la conferma di collasso.
	 */
	private ConfermaAllegatiProtocollazioneCollassabileComponent confirmCollapse;

	/**
	 * Indica se aprire o meno il nodo dell'albero degli allegati che si sta
	 * creando.
	 */
	private boolean espandi;

	/**
	 * Flag allegato rimosso.
	 */
	private boolean allegatoRemoved;

	/**
	 * Allegato selezionato.
	 */
	private Integer allegatoSelectedItem;

	/**
	 * Mail bean.
	 */
	private final MailBean mailBean;
	

	/**
	 *  showWarnMessageMaxSize.
	 */
	private boolean showWarnMessageMaxSize;

	/**
	 * Costruttore di default.
	 * 
	 * @param guidMail
	 * @param utente
	 * @param nomeIndirizzoEmail
	 */
	public SelezionaPrincipaleAllegatoComponent(final String guidMail, final UtenteDTO utente, final String nomeIndirizzoEmail) {
		mailSRV = ApplicationContextProvider.getApplicationContext().getBean(IScompattaMailFacadeSRV.class);
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		this.utente = utente;
		showWarnMessageMaxSize = false;

		final Optional<CasellaPostaDTO> optionalCp = sessionBean.getCp().stream().filter(cp -> nomeIndirizzoEmail.equals(cp.getIndirizzoEmail())).findFirst();

		boolean mantieniAllegatiOriginali = false;
		if (optionalCp.isPresent()) {
			mantieniAllegatiOriginali = optionalCp.get().isMantieniAllegatiOriginali();
		}
		allegati = mailSRV.scompattaMail(utente, guidMail, mantieniAllegatiOriginali);

		if (CollectionUtils.isEmpty(allegati)) {
			LOGGER.error("Non sono presenti né allegati né documenti principali per la mail con GUID: " + guidMail);
			throw new RedException("Allegati non presenti");
		} else {
			confirmCollapse = new ConfermaAllegatiProtocollazioneCollassabileComponent();
			root = new DefaultTreeNode("Root", null);
			// Lo wrappo in un ListElementDTO per creare un id univoco indipendente dal
			// percorso nell'alberatura
			final List<ListElementDTO<HierarchicalFileWrapperDTO>> flattenList = new ArrayList<>(allegati.size());
			populateTreeNode(allegati, root, flattenList);

			// Imposto l'indice di ordine della lista come id affinché lo possa richiamare 
			// negli input e nei combobox
			final Iterator<ListElementDTO<HierarchicalFileWrapperDTO>> it = flattenList.iterator();
			
			orderedAllegati = flattenList;
			final int dimensioneMax = Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MAX_SIZE_SPACCHETTAMENTO_MAIL_MB));
			
			if(dimensioneMax > 0) {	
				int byteSize = 0;
				for (int index = 0; it.hasNext(); index++) {
					ListElementDTO<HierarchicalFileWrapperDTO> element = it.next();
					byteSize+=element.getData().getContentSize();
					element.setId(index);
				}
				if(byteSize!=0) {
					int mbSize = byteSize/1048576;
					if(!mantieniAllegatiOriginali && mbSize> dimensioneMax){
						showWarnMessageMaxSize =true;
					}
				}
			}
		}
		mailBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.MAIL_BEAN);
		
	}

	/**
	 * Costruisce l'albero root e la flattenList travasando i dati contenuti negli
	 * allegati nelle altre due strutture dati
	 * 
	 * @param allegati    Lista di figli di un nodo dell'albero, in base a questi
	 *                    dati viene poi composto l'albero root e la flattenList
	 * @param root        nodo dell'albero padre a cui verranno appesi i figli
	 *                    allegati
	 * @param flattenList lista di nodi del padre a cui verranno aggiunti i figli
	 *                    dei nodi allegati
	 * 
	 */
	private void populateTreeNode(final List<HierarchicalFileWrapperDTO> allegati, final TreeNode root, final List<ListElementDTO<HierarchicalFileWrapperDTO>> flattenList) {
		for (final HierarchicalFileWrapperDTO allegato : allegati) {
			final ListElementDTO<HierarchicalFileWrapperDTO> allegatoElement = new ListElementDTO<>(allegato, flattenList);
			final DefaultTreeNode father = new DefaultTreeNode(allegatoElement, root);
			/*
			 * I nodi verrano aperti all'apertura della response Protocolla in base alla
			 * "selezionabilità" del primo file della tabella (che dovrebbe essere decisa in
			 * base alla configurazione AOO).
			 */
			if (allegato.getIdentificativoRamo().equals(-1) && !allegato.isAttachmentSelectable()) {
				espandi = true;
			}
			father.setExpanded(espandi);
			if (!allegato.isAttachmentSelectable()) {
				populateTreeNode(allegato.getSons(), father, flattenList);
			}
		}
	}

	/**
	 * Sono tutti selezionati tranne quello corrispondente al principale.
	 */
	public void onSelectPrincipale() {
		boolean isValido = true;

		try {
			final int size = orderedAllegati.size();
			final List<Integer> toConvert = new ArrayList<>(size - 1);

			for (final ListElementDTO<HierarchicalFileWrapperDTO> allegato : orderedAllegati) {
				final Integer id = allegato.getId();
				isValido = true;
				// in caso sia stato già indicato come protetto in fase di scompattamento o sia
				// stato già controllato
				if (id.equals(selectedPrincipale) && allegato.getData().getIsProtected() != null && Boolean.FALSE.equals(allegato.getData().getIsProtected())) {
					if (id.equals(selectedPrincipale) && Constants.ContentType.PDF.equals(allegato.getData().getFile().getTipoFile().getMimeType())) {

						isValido = recuperaValditaAllegato(allegato);

						if (!isValido) {
							// Se il file risulta protetto viene deselezionato e disabilitato.
							selectedPrincipale = null;
							allegato.getData().setPrincipalSelectable(false);
							allegato.getData().setIsProtected(!isValido);
						}
					}
				} else if (id.equals(selectedPrincipale) && allegato.getData().getIsProtected() != null && Boolean.TRUE.equals(allegato.getData().getIsProtected())) {
					isValido = false;
					break;
				} else if (!id.equals(selectedPrincipale) && allegato.getData().isAttachmentSelectable()) {
					toConvert.add(id);
				}
			}

			if (isValido) {
				selectedAllegatiList = toConvert;
				boolean allegatoIsValido = true;

				for (final ListElementDTO<HierarchicalFileWrapperDTO> allegato : orderedAllegati) {
					if (utente.getMantieniFormatoOriginale() != 1 && Constants.ContentType.PDF.equals(allegato.getData().getFile().getTipoFile().getMimeType())) {

						allegatoIsValido = recuperaValditaAllegato(allegato);

						if (!allegatoIsValido) {
							allegato.getData().setIsProtected(!allegatoIsValido);
						}
					}
				}

				TreeNodeUtils.expandAll(root);
				mailBean.selectPreviewAllegato(getActualPrincipal());

				// Evidenzia la riga dell'allegato pricipale selezionato
				FacesHelper.executeJS("highlightRow('allegatiData', " + selectedPrincipale + ", true, 'eastSectionForm:treeTableAllegati_data')");

				if (!allegatoIsValido) {
					FacesHelper.executeJS("PF('wdgAllegatoProtected').show()");
				}
			} else {
				// Se il file risulta protetto viene deselezionato e disabilitato.
				FacesHelper.executeJS("PF('wdgConfirmSelectPrincipale').show()");
			}

			FacesHelper.update("eastSectionForm:fileDaProtocollare");
		} catch (final RedException e) {
			LOGGER.error(e);
			showErrorMessage(e.getMessage());
		} catch (final Exception e) {
			LOGGER.error(ERROR_SELEZIONE_DOCUMENTO_MSG, e);
			showErrorMessage(ERROR_SELEZIONE_DOCUMENTO_MSG);
		}
	}

	/**
	 * Verifica che l'allegato non sia protetto.
	 */
	public void onSelectAllegato() {
		boolean isValido = true;
		try {
			if (!allegatoRemoved) {
				final Integer indexSelected = allegatoSelectedItem;

				for (final ListElementDTO<HierarchicalFileWrapperDTO> allegato : orderedAllegati) {
					final Integer id = allegato.getId();
					isValido = true;

					// in caso sia stato già indicato come protetto in fase di scompattamento o sia
					// stato già controllato
					if ((indexSelected.equals(id) && allegato.getData().getIsProtected() != null && Boolean.FALSE.equals(allegato.getData().getIsProtected()))
							&& (utente.getMantieniFormatoOriginale() != 1 && Constants.ContentType.PDF.equals(allegato.getData().getFile().getTipoFile().getMimeType()))) {

						isValido = recuperaValditaAllegato(allegato);
						allegato.getData().setIsProtected(!isValido);
					} else if (indexSelected.equals(id) && allegato.getData().getIsProtected() != null && Boolean.TRUE.equals(allegato.getData().getIsProtected())) {
						isValido = false;
					}
				}

				if (!isValido) {
					// Se il file risulta protetto viene richiesta conferma all'utente.
					FacesHelper.executeJS("PF('wdgConfirmUploadAllegato').show()");
				}
			}
		} catch (final RedException e) {
			LOGGER.error(e);
			showErrorMessage(e.getMessage());
		} catch (final Exception e) {
			LOGGER.error(ERROR_SELEZIONE_DOCUMENTO_MSG, e);
			showErrorMessage(ERROR_SELEZIONE_DOCUMENTO_MSG);
		}
	}

	/**
	 * Verifica che il documento allegato sia protetto o meno e imposta il flag di
	 * conseguenza.
	 * 
	 * @param allegato
	 * @return true se valido, false altrimenti
	 */
	private boolean recuperaValditaAllegato(final ListElementDTO<HierarchicalFileWrapperDTO> allegato) {
		boolean isValido;
		try {
			isValido = PdfHelper.checkValidaPDF(mailSRV.getContentutoDiscompattataMail(utente, allegato.getData().getNodeId()));

		} catch (final Exception e) {
			LOGGER.error("Errore durante la verifica che il documento allegato non sia protetto. Nome file: " + allegato.getData().getFile().getFileName(), e);
			isValido = false;
		}
		return isValido;
	}

	/**
	 * Rimuove l'allegato dalla lista dei selezionati.
	 */
	public void clearAllegato() {
		final Integer indexSelected = allegatoSelectedItem;

		for (final ListElementDTO<HierarchicalFileWrapperDTO> allegato : orderedAllegati) {
			if (indexSelected.equals(allegato.getId()) && selectedAllegatiList.contains(indexSelected)) {
				selectedAllegatiList.remove(indexSelected);
			}
		}
	}

	/**
	 * Gestisce il cambiamento del documento selezionato.
	 * 
	 * @param event
	 */
	public void selectedItemsChanged(final ValueChangeEvent event) {
		List<?> oldValue = (List<?>) event.getOldValue();
		List<?> newValue = (List<?>) event.getNewValue();

		if (oldValue == null) {
			oldValue = Collections.emptyList();
		}
		if (oldValue.size() > newValue.size()) {
			oldValue = new ArrayList<>(oldValue);
			oldValue.removeAll(newValue);
			allegatoSelectedItem = (Integer) oldValue.iterator().next();
			allegatoRemoved = true;
		} else {
			newValue = new ArrayList<>(newValue);
			newValue.removeAll(oldValue);

			if (!newValue.isEmpty()) {
				allegatoSelectedItem = (Integer) newValue.iterator().next();
			}
			allegatoRemoved = false;
		}
	}

	/**
	 * Serve a mostrare i messaggi altrimenti vanno sotto la dialog
	 * 
	 * @param severity
	 * @param message
	 */
	public void showSingleMessage(final String severity, final String message) {
		if ("info".equals(severity)) {
			showInfoMessage(message);
		} else if ("warning".equals(severity)) {
			showWarnMessage(message);
		} else if ("error".equals(severity)) {
			showErrorMessage(message);
		}
	}

	/**
	 * Gestisce l'evento di collasso di un nodo del tredNode.
	 * 
	 * @param event
	 */
	public void onCollapse(final NodeCollapseEvent event) {
		final TreeNode treeNode = event.getTreeNode();

		final boolean almenoUnoSelezionato = checkIfCollapsingDeleteNodeSelection(treeNode);

		if (almenoUnoSelezionato) {
			confirmCollapse.setCollassabile(this, treeNode);
			FacesHelper.executeJS("PF('wdgConfirmCollassabile').show()");
		}
	}

	/**
	 * Verifica se almeno uno dei nodi è selezionato al momento del collapse
	 * 
	 * @param treeNode
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private boolean checkIfCollapsingDeleteNodeSelection(final TreeNode treeNode) {
		final ListElementDTO<HierarchicalFileWrapperDTO> collapsingElement = (ListElementDTO<HierarchicalFileWrapperDTO>) treeNode.getData();
		final Integer id = collapsingElement.getId();
		boolean almenoUnoSelezionato = false;

		if (selectedAllegatiList == null) {
			selectedAllegatiList = new ArrayList<>();
		}
		final List<TreeNode> sons = TreeNodeUtils.flatten(treeNode);
		almenoUnoSelezionato = sons.stream().map(TreeNode::getData)
				.anyMatch(element -> checkifSonsIsSelected(id, selectedAllegatiList, (ListElementDTO<HierarchicalFileWrapperDTO>) element));

		return almenoUnoSelezionato;
	}

	/**
	 * Verifica che l'elemento non sia radice e che sia sleezionato dall'utnete
	 * 
	 * @param id                   id della radice
	 * @param selectedAllegatiList Lista degli allegati selezionati
	 * @param element              elemnto da verificare
	 * @return
	 */
	private boolean checkifSonsIsSelected(final Integer id, final List<Integer> selectedAllegatiList, final ListElementDTO<HierarchicalFileWrapperDTO> element) {
		final Integer elementId = element.getId();
		return !id.equals(elementId) && (selectedAllegatiList.contains(elementId) || elementId.equals(selectedPrincipale));
	}

	/**
	 * Il documento principale selezionato dall'utente.
	 */
	public HierarchicalFileWrapperDTO getActualPrincipal() {
		HierarchicalFileWrapperDTO fileDTO = null;
		if (selectedPrincipale != null) {
			fileDTO = getHieararchicalFromOrderedAllegati(selectedPrincipale);
		}
		return fileDTO;
	}

	/**
	 * Gli allegati selezionati dall'utente.
	 */
	public List<HierarchicalFileWrapperDTO> getActualAllegati() {
		List<HierarchicalFileWrapperDTO> allegatiList = null;
		if (selectedAllegatiList != null) {
			allegatiList = new ArrayList<>(selectedAllegatiList.size());
			for (final Integer choosen : selectedAllegatiList) {
				final HierarchicalFileWrapperDTO hfwDTO = getHieararchicalFromOrderedAllegati(choosen);
				allegatiList.add(hfwDTO);
			}
		}
		return allegatiList;
	}

	/**
	 * ritorna a partire dall'id l'oggetto rappresentante l'allegato dell'email
	 * scompattato choosen can't be null
	 * 
	 * @param choosen
	 * @return
	 */
	private HierarchicalFileWrapperDTO getHieararchicalFromOrderedAllegati(final Integer choosen) {
		final ListElementDTO<HierarchicalFileWrapperDTO> element = orderedAllegati.get(choosen);
		return element.getData();
	}

	/* GETTER E SETTER PER LA TREETABLE, RADIOMENU E MULTICHECKBOX */

	/**
	 * Restituisce il documento principale del procedimento.
	 * 
	 * @return selectedPrincipale
	 */
	public Integer getSelectedPrincipale() {
		return selectedPrincipale;
	}

	/**
	 * Imposta il documento principale del procedimento.
	 * 
	 * @param selectedPrincipale
	 */
	public void setSelectedPrincipale(final Integer selectedPrincipale) {
		this.selectedPrincipale = selectedPrincipale;
	}

	/**
	 * Restituisce la lista degli allegati del procedimento selezionato.
	 * 
	 * @return selectedAllegatiList
	 */
	public List<Integer> getSelectedAllegatiList() {
		return selectedAllegatiList;
	}

	/**
	 * Imposta la lista degli allegati del procedimento selezionato.
	 * 
	 * @param selectedAllegatiList
	 */
	public void setSelectedAllegatiList(final List<Integer> selectedAllegatiList) {
		this.selectedAllegatiList = selectedAllegatiList;
	}

	/**
	 * @return allegati
	 */
	public List<HierarchicalFileWrapperDTO> getAllegati() {
		return allegati;
	}

	/**
	 * @return root
	 */
	public TreeNode getRoot() {
		return root;
	}

	/**
	 * Restuisce la lista ordinata degli allegati.
	 * 
	 * @return orderedAllegati
	 */
	public List<ListElementDTO<HierarchicalFileWrapperDTO>> getOrderedAllegati() {
		return orderedAllegati;
	}

	/**
	 * Restituisce il component per la gestione.
	 * 
	 * @return confirmCollapse
	 */
	public ConfermaAllegatiProtocollazioneCollassabileComponent getConfirmCollapse() {
		return confirmCollapse;
	}

	/**
	 * Restituisce il flag associato alla dimensione massima del messaggio di
	 * warning.
	 * 
	 * @return flag associato alla dimensione massima del messaggio di warning.
	 */
	public boolean isShowWarnMessageMaxSize() {
		return showWarnMessageMaxSize;
	}

	/**
	 * Imposta il flag associato alla dimensione massima del messaggio di warning.
	 * 
	 * @param showWarnMessageMaxSize
	 */
	public void setShowWarnMessageMaxSize(boolean showWarnMessageMaxSize) {
		this.showWarnMessageMaxSize = showWarnMessageMaxSize;
	}
}