package it.ibm.red.web.helper.export;

import java.util.ArrayList;
import java.util.List;

/**
 * Struttura di un workbook excel.
 */
public class WorkbookStructure {
	
	/**
	 * Titolo wb.
	 */
	private String workbookTitle;
	
	/**
	 * Lista sheet.
	 */
	private List<CustomSheetExcel> customSheetList;
	
	/**
	 * Costruttore.
	 */
	public WorkbookStructure() {
		customSheetList = new ArrayList<>();
	}
	
	/**
	 * @return the workbookTitle
	 */
	public String getWorkbookTitle() {
		return workbookTitle;
	}
	
	/**
	 * @param workbookTitle the workbookTitle to set
	 */
	public void setWorkbookTitle(final String workbookTitle) {
		this.workbookTitle = workbookTitle;
	}
	
	/**
	 * @return the customSheetList
	 */
	public List<CustomSheetExcel> getCustomSheetList() {
		return customSheetList;
	}
	
	/**
	 * @param customSheetList the customSheetList to set
	 */
	public void setCustomSheetList(final List<CustomSheetExcel> customSheetList) {
		this.customSheetList = customSheetList;
	}
}