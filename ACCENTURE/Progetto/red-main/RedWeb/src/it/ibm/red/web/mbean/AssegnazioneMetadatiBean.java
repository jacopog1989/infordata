package it.ibm.red.web.mbean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.SelectEvent;

import it.ibm.red.business.dto.AnagraficaDipendenteDTO;
import it.ibm.red.business.dto.AnagraficaDipendentiComponentDTO;
import it.ibm.red.business.dto.AssegnazioneMetadatiDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.enums.TipologiaNodoEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IAssegnazioneAutomaticaMetadatiFacadeSRV;
import it.ibm.red.business.service.facade.ITipoProcedimentoFacadeSRV;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;
import it.ibm.red.business.utils.CollectionUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.mbean.component.AssCompetenzaConUtentiOrganigrammaComponent;
import it.ibm.red.web.document.mbean.interfaces.IAssOrganigrammaComponent;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Bean assegnazione metadati.
 */
@Named(ConstantsWeb.MBean.ASSEGNAZIONE_METADATI_BEAN)
@ViewScoped
public class AssegnazioneMetadatiBean extends AbstractBean {

	/**
	 * UID.
	 */
	private static final long serialVersionUID = 2273077193475998910L;

	/**
	 * Logger per la gestione dei messaggi in console.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AssegnazioneMetadatiBean.class.getName());

	/**
	 * Descrizione presente prima della selezione di un elemento nelle liste.
	 */
	private static final String DEFAULT_DESCRIPTION = "-";

	/**
	 * Id di default, prima delle selezioni.
	 */
	private static final Integer DEFAULT_ID = 0;

	/**
	 * UtenteDTO per la gestione degli utente associati alle assegnazioni
	 * automatiche.
	 */
	private UtenteDTO utente;

	/**
	 * SRV per la gestione delle tipologie di documento.
	 */
	private ITipologiaDocumentoFacadeSRV tipologiaDocumentoSRV;

	/**
	 * SRV per la gestione dei tipi procedimento.
	 */
	private ITipoProcedimentoFacadeSRV tipoProcedimentoSRV;

	/**
	 * Component per la gestione degli assegnatari, uffici e utenti.
	 */
	private IAssOrganigrammaComponent assCompetenzaOrganigrammaCmp;

	/**
	 * SRV per la gestione della persistenza associata alle assegnazioni.
	 */
	private IAssegnazioneAutomaticaMetadatiFacadeSRV assegnazioneAutomaticaMetadatiSRV;

	/**
	 * Lista dei tipi documento utilizzabili per le assegnazioni.
	 */
	private List<TipologiaDocumentoDTO> tipologieDocumenti = new ArrayList<>();

	/**
	 * Lista dei tipi procedimento utilizzabili per le assegnazioni.
	 */
	private List<TipoProcedimento> tipologieProcedimenti = new ArrayList<>();

	/**
	 * Lista dei metadati utilizzabili per le assegnazioni.
	 */
	private List<MetadatoDTO> metadati = new ArrayList<>();

	/**
	 * Assegnazione in fase di creazione.
	 */
	private AssegnazioneMetadatiDTO nuovaAssegnazione;

	/**
	 * Helper per datatable.
	 */
	private SimpleDetailDataTableHelper<AssegnazioneMetadatiDTO> assegnazioniDTH;

	/**
	 * Flag per la caratterizzazione del metadato, valorizzato o no.
	 */
	private boolean checkValoreMetadato;

	/**
	 * Elenco numero di regole per l'assegnazione, usato per tooltip informativo.
	 */
	private Map<Integer, String> regoleAssegnazioniInfo = new HashMap<>();

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		LOGGER.info(ConstantsWeb.MBean.ASSEGNAZIONE_METADATI_BEAN + " ===> postConstruct");

		final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		tipologiaDocumentoSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoFacadeSRV.class);
		tipoProcedimentoSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipoProcedimentoFacadeSRV.class);
		assegnazioneAutomaticaMetadatiSRV = ApplicationContextProvider.getApplicationContext().getBean(IAssegnazioneAutomaticaMetadatiFacadeSRV.class);
		utente = sb.getUtente();
		assCompetenzaOrganigrammaCmp = new AssCompetenzaConUtentiOrganigrammaComponent(utente, false,false);
		caricaListaAssegnazioni();
		nuovaAssegnazione = new AssegnazioneMetadatiDTO();
		populateGenericInfos();
	}

	// Recupero delle assegnazioni automatiche già esistenti per il relativo AOO
	private void caricaListaAssegnazioni() {
		assegnazioniDTH = new SimpleDetailDataTableHelper<>();
		assegnazioniDTH.setMasters(new ArrayList<>());

		final List<AssegnazioneMetadatiDTO> masterAssegnazioniList = assegnazioneAutomaticaMetadatiSRV.getByIdAoo(utente.getIdAoo());

		assegnazioniDTH.setMasters(masterAssegnazioniList);
	}

	/**
	 * Gestisce la logica di eliminazione di un'assegnazione automatica, lasciando
	 * lo strato di persistenza in uno stato coerente.
	 * 
	 * @param event
	 */
	public void eliminaAssegnazione(final ActionEvent event) {
		try {
			// Si procede con l'eliminazione dell'assegnazione automatica selezionata
			assegnazioniDTH.setCurrentMaster(getDataTableClickedRow(event));
			assegnazioneAutomaticaMetadatiSRV.deleteByIdAssegnazione(assegnazioniDTH.getCurrentMaster().getId());

			// Si rimuove l'assegnazione automatica dal datatable
			if (!CollectionUtils.isEmptyOrNull((assegnazioniDTH.getFilteredMasters()))) {
				assegnazioniDTH.getFilteredMasters().remove(assegnazioniDTH.getCurrentMaster());
			} else {
				assegnazioniDTH.getMasters().remove(assegnazioniDTH.getCurrentMaster());
			}

			showInfoMessage("Eliminazione avvenuta con successo");
		} catch (final Exception e) {
			LOGGER.error("Errore nell'eliminazione dell'assegnazione automatica.", e);
			showError("Errore nell'eliminazione dell'assegnazione automatica.");
		}
	}

	/**
	 * Gestisce la logica di eliminazione di tutte le assegnazioni automatiche
	 * esistenti. Lascia lo strato di persistenza in uno stato coerente.
	 */
	public void eliminaTutte() {
		try {
			assegnazioneAutomaticaMetadatiSRV.deleteByIdAoo(utente.getIdAoo());
			assegnazioniDTH = new SimpleDetailDataTableHelper<>();

			showInfoMessage("Eliminazione avvenuta con successo");
		} catch (final Exception e) {
			LOGGER.error("Errore nell'eliminazione", e);
			showError("Errore nell'eliminazione");
		}
	}

	/**
	 * Esegue una preparazione della dialog di creazione assegnazione.
	 */
	public void preparaDialog() {
		// inserisco pre-selezione doc
		final TipologiaDocumentoDTO td = new TipologiaDocumentoDTO();
		td.setIdTipologiaDocumento(DEFAULT_ID);
		td.setDescrizione(DEFAULT_DESCRIPTION);
		tipologieDocumenti.add(td);
		// carico tipologie documento
		tipologieDocumenti.addAll(tipologiaDocumentoSRV.getComboTipologieByAooAndTipoCategoriaAttivi(TipoCategoriaEnum.ENTRATA, utente.getIdAoo()));

		// inserisco pre-selezione proc
		final TipoProcedimento tp = new TipoProcedimento(DEFAULT_ID, DEFAULT_DESCRIPTION);
		tipologieProcedimenti.add(tp);

		assCompetenzaOrganigrammaCmp.reset();

		FacesHelper.update("centralSectionForm:pnlDlgNuovaAssegnazione");
	}

	/**
	 * Recupero delle tipologie procedimento disponibili per un dato documento, da
	 * caricare nella combobox relativa solo quando viene scelto un tipo documento.
	 */
	public void loadTipologieProcedimenti() {
		// pulisco tipologie procedimento
		tipologieProcedimenti = new ArrayList<>();
		// pulisco metadati
		pulisciMetadati();
		// inserisco pre-selezione e la seleziono
		final TipoProcedimento tp = new TipoProcedimento(DEFAULT_ID, DEFAULT_DESCRIPTION);
		tipologieProcedimenti.add(tp);
		nuovaAssegnazione.getTipoProc().setTipoProcedimentoId(tipologieProcedimenti.get(0).getTipoProcedimentoId());
		// carico tipi procedimento
		if (!DEFAULT_ID.equals(nuovaAssegnazione.getTipoDoc().getIdTipologiaDocumento())) {
			tipologieProcedimenti.addAll(tipoProcedimentoSRV.getTipiProcedimentoByTipologiaDocumento(nuovaAssegnazione.getTipoDoc().getIdTipologiaDocumento()));
		}
	}

	/**
	 * Recupero dei metadati disponibili per una data coppia TIPOLOGIA DOCUMENTO /
	 * TIPOLOGIA PROCEDIMENTO, da caricare nella combobox relativa solo quando viene
	 * fissata la coppia documento/procedimento.
	 */
	public void loadMetadati() {
		final int tipoProcId = ((Long) nuovaAssegnazione.getTipoProc().getTipoProcedimentoId()).intValue();
		if (DEFAULT_ID.longValue() != nuovaAssegnazione.getTipoProc().getTipoProcedimentoId()) {
			metadati = tipologiaDocumentoSRV.caricaMetadati(nuovaAssegnazione.getTipoDoc().getIdTipologiaDocumento(), tipoProcId, null, utente.getIdAoo());
		} else {
			pulisciMetadati();
		}
	}

	/**
	 * Definisce la logica da eseguire alla selezione della riga, in riferimento al
	 * datatable dei metadati.
	 * 
	 * @param event
	 */
	public void rowSelector(final SelectEvent event) {
		final MetadatoDTO m = (MetadatoDTO) event.getObject();
		if (m != null) {
			nuovaAssegnazione.setMetadato(m);
		}
	}

	/**
	 * Definisce la logica di memorizzazione della regola dell'assegnazione
	 * automatica. Gestisce eventuali errori possibili mostrando un messaggio
	 * informativo all'utente.
	 */
	public void registra() {
		boolean inserted = false;
		if (assCompetenzaOrganigrammaCmp.checkSelezionato() && !nuovaAssegnazione.getTipoDoc().getIdTipologiaDocumento().equals(DEFAULT_ID)) {

			nuovaAssegnazione.getAssegnatario().setIdUfficio(assCompetenzaOrganigrammaCmp.getSelected().getIdNodo());
			nuovaAssegnazione.getAssegnatario().setDescrizioneUfficio(assCompetenzaOrganigrammaCmp.getDescrizioneNodoSelected());

			if (TipologiaNodoEnum.UTENTE.equals(assCompetenzaOrganigrammaCmp.getSelected().getTipoNodo())) {
				nuovaAssegnazione.getAssegnatario().setDescrizioneUtente(assCompetenzaOrganigrammaCmp.getDescrizioneUtenteSelected());
				nuovaAssegnazione.getAssegnatario().setIdUtente(assCompetenzaOrganigrammaCmp.getSelected().getIdUtente());
			}
			try {

				if (getNuovaAssegnazione().getTipoProc() == null || getNuovaAssegnazione().getTipoProc().getTipoProcedimentoId() == DEFAULT_ID) {
					if (isAlreadyExisting(getNuovaAssegnazione())) {
						showWarnMessage("Un automatismo sul tipo documento selezionato è già esistente");
					} else {

						assegnazioneAutomaticaMetadatiSRV.registraAssegnazioneMetadato(getNuovaAssegnazione(), utente.getIdAoo());
						inserted = true;
					}

				} else {
					if (getNuovaAssegnazione().getMetadato() == null) {
						if (isAlreadyExisting(getNuovaAssegnazione())) {
							showWarnMessage("Un automatismo sulla coppia selezionata è già esistente");
						} else {
							assegnazioneAutomaticaMetadatiSRV.registraAssegnazioneMetadato(getNuovaAssegnazione(), utente.getIdAoo());
							inserted = true;
						}
					} else {
						if (getNuovaAssegnazione().getMetadato().getValue4AttrExt() == null) {
							if (isAlreadyExisting(getNuovaAssegnazione())) {
								showWarnMessage("Un automatismo sull'attributo: " + getNuovaAssegnazione().getMetadato().getDisplayNameView() + " è già esistente");
							} else {
								assegnazioneAutomaticaMetadatiSRV.registraAssegnazioneMetadato(getNuovaAssegnazione(), utente.getIdAoo());
								inserted = true;
							}
						} else {
							if (isAlreadyExisting(getNuovaAssegnazione())) {
								showWarnMessage(
										"Un automatismo sull'attributo: " + getNuovaAssegnazione().getMetadato().getDisplayNameView() + " valorizzato è già esistente");
							} else {
								assegnazioneAutomaticaMetadatiSRV.registraAssegnazioneMetadato(getNuovaAssegnazione(), utente.getIdAoo());
								inserted = true;
							}
						}
					}
				}

				if (inserted) {
					// Si aggiorna il contenuto del datatable
					caricaListaAssegnazioni();

					FacesHelper.executeJS("PF('wdgNuovaAssegnazioneDlg').hide()");
					pulisciCampi();
					FacesHelper.update("centralSectionForm:dtAssegnazioneMetadati");
				}

			} catch (final Exception e) {
				LOGGER.error(e);
				showError(e);
			}
		} else {
			// Si mostra il messaggio di errore all'utente
			showError("Inserire i dati obbligatori per l'inserimento della nuova assegnazione automatica");
		}
	}

	/**
	 * Esegue un reset dei campi, viene utilizzato per riportare lo stato della
	 * dialog a quello iniziale.
	 */
	public void pulisciCampi() {
		nuovaAssegnazione = new AssegnazioneMetadatiDTO();
		tipologieDocumenti = new ArrayList<>();
		tipologieProcedimenti = new ArrayList<>();
		assCompetenzaOrganigrammaCmp.pulisciSelezioneAssegnatario();
		pulisciMetadati();

		FacesHelper.update("centralSectionForm:pnlDlgNuovaAssegnazione");
	}

	/**
	 * Esegue reset delle informazioni associate al metadato selezionato per la
	 * definizione della regola.
	 */
	private void pulisciMetadati() {
		nuovaAssegnazione.setMetadato(null);
		metadati = new ArrayList<>();
		checkValoreMetadato = false;
	}

	/**
	 * Verifica che per un'eventuale assegnazione non sia già presente una regola
	 * che ne vanifica la definizione.
	 * 
	 * @param assegnazioneToCheck
	 * @return true se l'assegnazione è già stata definita ed è già memorizzata,
	 *         false altrimenti.
	 */
	private boolean isAlreadyExisting(final AssegnazioneMetadatiDTO assegnazioneToCheck) {
		final Collection<AssegnazioneMetadatiDTO> assegnazioni = assegnazioniDTH.getMasters();

		Long idTipologiaDocumento = null;
		if (assegnazioneToCheck.getTipoDoc() != null) {
			idTipologiaDocumento = assegnazioneToCheck.getTipoDoc().getIdTipologiaDocumento().longValue();
		}

		Long idTipologiaProcedimento = null;
		if (assegnazioneToCheck.getTipoProc() != null && assegnazioneToCheck.getTipoProc().getTipoProcedimentoId() != DEFAULT_ID) {
			idTipologiaProcedimento = assegnazioneToCheck.getTipoProc().getTipoProcedimentoId();
		}
		MetadatoDTO metadato = null;
		if (assegnazioneToCheck.getMetadato() != null) {
			metadato = assegnazioneToCheck.getMetadato();
		}

		// Se il procedimento non viene passato, verifico che l'assegnazione per tipo
		// doc non sia già presente
		if (idTipologiaProcedimento == null || idTipologiaProcedimento == (long) DEFAULT_ID) {
			if (assegnazioni != null) {
				for (final AssegnazioneMetadatiDTO assegnazione : assegnazioni) {
					if (assegnazione.getTipoDoc() != null && idTipologiaDocumento != null
							&& assegnazione.getTipoDoc().getIdTipologiaDocumento() == idTipologiaDocumento.longValue()
							&& (assegnazione.getTipoProc() == null || assegnazione.getTipoProc().getTipoProcedimentoId() == DEFAULT_ID)) {
						// Se l'assegnazione non è più specifica di quella che si vuole inserire
						return true;
					}
				}
			}
		} else if (metadato == null) {
			// Se il metadato non viene passato, verifico che l'assegnazione per tipo
			// doc/proc non sia già presente

			if (assegnazioni != null) {
				for (final AssegnazioneMetadatiDTO assegnazione : assegnazioni) {
					if (assegnazione.getTipoDoc() != null && idTipologiaDocumento != null
							&& assegnazione.getTipoDoc().getIdTipologiaDocumento() == idTipologiaDocumento.longValue() && assegnazione.getTipoProc() != null
							&& assegnazione.getTipoProc().getTipoProcedimentoId() == idTipologiaProcedimento && assegnazione.getMetadato() == null) {
						// Se l'assegnazione non è più specifica di quella che si vuole inserire
						return true;
					}
				}
			}
		} else {
			// Se il metadato viene passato a questo metodo,

			return isAssMetadatoExisting(metadato, assegnazioni);
		}

		return false;
	}

	/**
	 * Verifica che la regola definita da un metadato ed un assegnatario sia già
	 * esistente.
	 * 
	 * @param metadato
	 * @param assegnazioni
	 * @return true se l'assegnazione è già stata definita, false altrimenti
	 */
	private boolean isAssMetadatoExisting(final MetadatoDTO metadato, final Collection<AssegnazioneMetadatiDTO> assegnazioni) {

		// Se il metadato non è null, verifico il selected value in base al tipo di
		// metadato
		if (assegnazioni != null) {
			for (final AssegnazioneMetadatiDTO assegnazione : assegnazioni) {
				if (assegnazione.getMetadato() != null) {

					if (TipoMetadatoEnum.PERSONE_SELECTOR.equals(metadato.getType()) && TipoMetadatoEnum.PERSONE_SELECTOR.equals(assegnazione.getMetadato().getType())
							&& metadato.getName().equals(assegnazione.getMetadato().getName())) {

						// Se sono entrambe vuote o i valori selezionati sono gli stessi ->
						// l'automatismo è già definito per un ufficio
						if (CollectionUtils.isEmptyOrNull(((AnagraficaDipendentiComponentDTO) metadato).getSelectedValues())
								&& CollectionUtils.isEmptyOrNull(((AnagraficaDipendentiComponentDTO) assegnazione.getMetadato()).getSelectedValues())) {

							return true;
						} else if (!CollectionUtils.isEmptyOrNull(((AnagraficaDipendentiComponentDTO) metadato).getSelectedValues())
								&& !CollectionUtils.isEmptyOrNull(((AnagraficaDipendentiComponentDTO) assegnazione.getMetadato()).getSelectedValues())) {
							// NB: non può essere fatto utilizzando getValue4AttrExt perché il metadato del
							// documento non è stato ancora serializzato quindi contiene più informazioni
							// del necessario per verificare l'uguaglianza
							int matches = 0;
							for (final AnagraficaDipendenteDTO anagraficaAssEsistente : ((AnagraficaDipendentiComponentDTO) assegnazione.getMetadato()).getSelectedValues()) {
								for (final AnagraficaDipendenteDTO anagraficaAssToCheck : ((AnagraficaDipendentiComponentDTO) metadato).getSelectedValues()) {
									if (anagraficaAssEsistente.getId().equals(anagraficaAssToCheck.getId())) {
										matches++;
										break;
									}
								}
							}
							if (matches == (((AnagraficaDipendentiComponentDTO) metadato).getSelectedValues()).size()) {
								return true;
							}
						}
					} else if (TipoMetadatoEnum.CAPITOLI_SELECTOR.equals(metadato.getType()) && metadato.getName().equals(assegnazione.getMetadato().getName())) {
						// Se sono entrambe null o vuote o i capitoli selezionati sono gli stessi ->
						// l'automatismo già è definito per un ufficio
						if ((metadato.getValue4AttrExt() == null || "".equals(metadato.getValue4AttrExt()))
								&& (assegnazione.getMetadato().getValue4AttrExt() == null || "".equals(assegnazione.getMetadato().getValue4AttrExt()))) {
							return true;
						} else if (metadato.getValue4AttrExt() != null && assegnazione.getMetadato().getValue4AttrExt() != null
								&& metadato.getValue4AttrExt().equals(assegnazione.getMetadato().getValue4AttrExt())) {
							return true;
						}
					} else if (TipoMetadatoEnum.LOOKUP_TABLE.equals(metadato.getType()) && metadato.getName().equals(assegnazione.getMetadato().getName())) {
						// Se le lookup sono entrambe non valorizzate o valorizzate allo stesso modo ->
						// l'automatismo già è definito per un ufficio
						if ((metadato.getValue4AttrExt() == null && assegnazione.getMetadato().getValue4AttrExt() == null)
								|| ("".equals(metadato.getValue4AttrExt()) && "".equals(assegnazione.getMetadato().getValue4AttrExt()))) {
							return true;
						} else if ((metadato.getValue4AttrExt() != null && assegnazione.getMetadato().getValue4AttrExt() != null)
								&& (metadato.getValue4AttrExt().equals(assegnazione.getMetadato().getValue4AttrExt())
										&& metadato.getName().equals(assegnazione.getMetadato().getName()))) {
							return true;
						}
					} else if (metadato.getType().equals(assegnazione.getMetadato().getType()) && metadato.getName().equals(assegnazione.getMetadato().getName())) {
						// Se i metadati sono entrambi non valorizzati o valorizzati allo stesso modo ->
						// l'automatismo già è definito per un ufficio
						if (StringUtils.isEmpty(metadato.getValue4AttrExt()) && StringUtils.isEmpty(assegnazione.getMetadato().getValue4AttrExt())) {
							return true;
						} else if (!StringUtils.isEmpty(metadato.getValue4AttrExt()) && !StringUtils.isEmpty(assegnazione.getMetadato().getValue4AttrExt())
								&& metadato.getValue4AttrExt().equals(assegnazione.getMetadato().getValue4AttrExt())) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	/**
	 * Popola la lista delle regole definite nel tooltip informativo per l'utente.
	 */
	private void populateGenericInfos() {
		final String firstRule = " Verifica automatismi per capitoli di spesa (un tipo documento può avere al massimo un capitolo di spesa quindi l'ordinamento non è influente).";
		final String secondRule = " Verifica automatismi per valore esatto di metadati.";
		final String thirdRule = " Verifica automatismi per presenza di valore di metadati.";
		final String fourthRule = " Verifica automatismi per tipo documento / tipo procedimento.";
		final String fifthRule = " Verifica automatismi per tipo documento.";

		getRegoleAssegnazioniInfo().put(1, firstRule);
		getRegoleAssegnazioniInfo().put(2, secondRule);
		getRegoleAssegnazioniInfo().put(3, thirdRule);
		getRegoleAssegnazioniInfo().put(4, fourthRule);
		getRegoleAssegnazioniInfo().put(5, fifthRule);

	}

	/**
	 * Restituisce la lista delle tipologia documenti per cui è possibile definire
	 * un'assegnazione automatica basata su metadati.
	 * 
	 * @return tipologieDocumenti valide, associate all'AOO
	 */
	public List<TipologiaDocumentoDTO> getTipologieDocumenti() {
		return tipologieDocumenti;
	}

	/**
	 * Imposta le tipologie documenti selezionabili per l'assegnazione automatica.
	 * 
	 * @param tipologieDocumenti
	 */
	public void setTipologieDocumenti(final List<TipologiaDocumentoDTO> tipologieDocumenti) {
		this.tipologieDocumenti = tipologieDocumenti;
	}

	/**
	 * Restituisce la lista dei tipi procedimento per cui è possibile definire
	 * un'assegnazione automatica basata sui metadati associati alla tipologia
	 * documento selezionata.
	 * 
	 * @return lista dei tipi procedimento selezionabili per la definizione della
	 *         regola
	 */
	public List<TipoProcedimento> getTipologieProcedimenti() {
		return tipologieProcedimenti;
	}

	/**
	 * Imposta i tipi procedimenti selezionabili per la definizione della regola.
	 * 
	 * @param tipologieProcedimenti
	 */
	public void setTipologieProcedimenti(final List<TipoProcedimento> tipologieProcedimenti) {
		this.tipologieProcedimenti = tipologieProcedimenti;
	}

	/**
	 * Restituisce la lista dei metadati esistenti sulla coppia tipologia
	 * documento/tipologia procedimento.
	 * 
	 * @return lista dei metadati selezionabili per la definizione della regola
	 */
	public List<MetadatoDTO> getMetadati() {
		return metadati;
	}

	/**
	 * Imposta la lista dei metadati selezionabili per la definizione della regola
	 * di assegnazione automatica.
	 * 
	 * @param metadati
	 */
	public void setMetadati(final List<MetadatoDTO> metadati) {
		this.metadati = metadati;
	}

	/**
	 * Restituisce il componente che si occupa della selezione dell'assegnatario.
	 * 
	 * @return assCompetenzaOrganigrammaCmp
	 */
	public IAssOrganigrammaComponent getAssCompetenzaOrganigrammaCmp() {
		return assCompetenzaOrganigrammaCmp;
	}

	/**
	 * Imposta il componente che si occupa della gestione degli assegnatari.
	 * 
	 * @param assCompetenzaOrganigrammaCmp
	 */
	public void setAssCompetenzaOrganigrammaCmp(final IAssOrganigrammaComponent assCompetenzaOrganigrammaCmp) {
		this.assCompetenzaOrganigrammaCmp = assCompetenzaOrganigrammaCmp;
	}

	/**
	 * Restituisce l'helper per la gestione del datatable delle assegnazioni.
	 * 
	 * @return assegnazioniDTH
	 */
	public SimpleDetailDataTableHelper<AssegnazioneMetadatiDTO> getAssegnazioniDTH() {
		return assegnazioniDTH;
	}

	/**
	 * Imposta l'helper per la gestione del datatable delle assegnazioni.
	 * 
	 * @param assegnazioniDTH
	 */
	public void setAssegnazioniDTH(final SimpleDetailDataTableHelper<AssegnazioneMetadatiDTO> assegnazioniDTH) {
		this.assegnazioniDTH = assegnazioniDTH;
	}

	/**
	 * Restituisce la nuova assegnazione in fase di creazione.
	 * 
	 * @return nuovaAssegnazione
	 */
	public AssegnazioneMetadatiDTO getNuovaAssegnazione() {
		return nuovaAssegnazione;
	}

	/**
	 * Imposta la nuova assegnazione in fase di creazione.
	 * 
	 * @param nuovaAssegnazione
	 */
	public void setNuovaAssegnazione(final AssegnazioneMetadatiDTO nuovaAssegnazione) {
		this.nuovaAssegnazione = nuovaAssegnazione;
	}

	/**
	 * Specifica se per un certo metadato occorre considerare il valore o soltanto
	 * la mera esistenza.
	 * 
	 * @return true se occorre verificare il valore del metadato, false se la regola
	 *         si preoccupa solo dell'esistenza del metadato
	 */
	public boolean isCheckValoreMetadato() {
		return checkValoreMetadato;
	}

	/**
	 * Imposta il flag che indica se il metadato deve essere considerato
	 * comprendendone il valore, o soltanto come tipo di metadato.
	 * 
	 * @param checkValoreMetadato
	 */
	public void setCheckValoreMetadato(final boolean checkValoreMetadato) {
		this.checkValoreMetadato = checkValoreMetadato;
	}

	/**
	 * Restituisce l'informativa per l'utente che definisce le regole in maniera
	 * user friendly.
	 * 
	 * @return descrizione regole delle assegnazioni
	 */
	public Map<Integer, String> getRegoleAssegnazioniInfo() {
		return regoleAssegnazioniInfo;
	}

	/**
	 * Imposta la descrizione delle regole per la definizione del tooltip
	 * informativo per l'utente.
	 * 
	 * @param regoleAssegnazioniInfo
	 */
	public void setRegoleAssegnazioniInfo(final Map<Integer, String> regoleAssegnazioniInfo) {
		this.regoleAssegnazioniInfo = regoleAssegnazioniInfo;
	}

}
