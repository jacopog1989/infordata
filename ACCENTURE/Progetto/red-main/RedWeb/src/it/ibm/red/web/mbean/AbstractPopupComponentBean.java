package it.ibm.red.web.mbean;

import it.ibm.red.web.mbean.interfaces.IPopupComponent;

/**
 * Abstract bean del popup component.
 */
public class AbstractPopupComponentBean extends AbstractBean implements IPopupComponent {

	/**
	 * La costante serialVerisonUID.
	 */
	private static final long serialVersionUID = 8960680691015679789L;
	
	@Override
	public void conferma() { 
		// Metodo intenzionalmente vuoto.
	}

	@Override
	public void annulla() {
		// Metodo intenzionalmente vuoto.
	}

	@Override
	protected void postConstruct() { 
		// Metodo intenzionalmente vuoto.
	}

}
