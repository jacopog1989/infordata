package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.NodeExpandEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.enums.TipologiaNodoEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IInvioInFirmaFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractAssegnazioniBean;

/**
 * @author mcrescentini
 *
 */
@Named(MBean.INVIA_IN_FIRMA_MULTIPLA_BEAN)
@ViewScoped
public class InviaInFirmaMultiplaBean extends AbstractAssegnazioniBean {

	private static final long serialVersionUID = 2499315043581924913L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(InviaInFirmaMultiplaBean.class.getName());
	
	/**
	 * Flag urgente.
	 */
	private Boolean urgente;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		init();
	}
	
	/**
	 * Verifica che sia possibile procedere con la response di assegnazione.
	 */
	public void checkPreResponse() {
		if (permessoContinua()) {
			assegnaPerFirmaMultiplaResponse();
		} else {
			showError("Il campo Nuova Assegnazione deve essere specificato.");
		}
	}
	
	
	private void assegnaPerFirmaMultiplaResponse() {
		Collection<EsitoOperazioneDTO> eOpe = new ArrayList<>();
		final Collection<String> wobNumbers = new ArrayList<>();
		
		try {
			// Si puliscono gli esiti
			getLdb().cleanEsiti();
			
			// Si raccolgono i WOB number necessari ad invocare il service 
			for (final MasterDocumentRedDTO m : getMasters()) {
				if (!StringUtils.isNullOrEmpty(m.getWobNumber())) {
					wobNumbers.add(m.getWobNumber());
				}
			}
			
			Long idUtenteAssegnatario;
			// Nel caso della selezione di un ufficio, il sistema si aspetta un idUtente a '0'
			if (getSelected().getIdUtente() == null) {
				idUtenteAssegnatario = 0L;
			} else {
				idUtenteAssegnatario = getSelected().getIdUtente();
			}

			// Si invoca il servizio 
			final IInvioInFirmaFacadeSRV invioSRV = ApplicationContextProvider.getApplicationContext().getBean(IInvioInFirmaFacadeSRV.class);
			eOpe = invioSRV.invioInFirmaMultipla(getUtente(), wobNumbers, getSelected().getIdNodo(), idUtenteAssegnatario, getMotivoAssegnazione(), urgente);
			
			// Si imposta l'esito e si esegue un refresh
			getLdb().getEsitiOperazione().addAll(eOpe);
			// Si aggiorna la dialog contenente gli esiti
			FacesHelper.update("centralSectionForm:idDlgShowResult");
			FacesHelper.executeJS("PF('dlgGeneric').hide();PF('dlgShowResult').show();");
			getLdb().destroyBeanViewScope(MBean.INVIA_IN_FIRMA_MULTIPLA_BEAN);
		} catch (final Exception e) { 
			LOGGER.error("Si è verificato un errore durante il tentato sollecito della response 'ASSEGNA'", e);
			showError("Si è verificato un errore durante il tentato sollecito della response 'ASSEGNA'");
		}
	}

	/**
	 * Effettua un clear dell'assegnazione.
	 */
	@Override
	public void clearAssegnazione() {
		setDescrizioneNewAssegnazione(MBean.INVIA_IN_FIRMA_MULTIPLA_BEAN);
	}
	
	/**
	 * Effettua il caricamento dell'organigramma.
	 */
	@Override
	protected void loadRootOrganigramma() {
		try {
			int index = 0;
			final List<Long> alberatura = new ArrayList<>();
			for (index = alberaturaNodi.size() - 1; index >= 0; index--) {
				alberatura.add(alberaturaNodi.get(index).getIdNodo());
			}
			
			// Creazione della struttura del tree
			setRoot(new DefaultTreeNode());
			getRoot().setSelectable(true);
			getRoot().setExpanded(true);
			
			// Si imposta un DTO utile per creare la struttura del tree
			final NodoOrganigrammaDTO radice = new NodoOrganigrammaDTO();
			radice.setIdAOO(getUtente().getIdAoo());
			radice.setIdNodo(getUtente().getIdNodoRadiceAOO());
			radice.setCodiceAOO(getUtente().getCodiceAoo());
			radice.setUtentiVisible(true);
			radice.setDescrizioneNodo(getUtente().getNodoDesc());
			
			// Creazione del nodo padre necessario (ufficio dell'utente in sessione)
			final TreeNode nodoRadice = new DefaultTreeNode(radice, getRoot());
			nodoRadice.setExpanded(true);
			nodoRadice.setSelectable(false);
			nodoRadice.setType("UFF");
			
			final List<NodoOrganigrammaDTO> primoLivello = getOrgSRV().getFigliAlberoAssegnatarioPerFirma(getUtente().getIdUfficio(), radice);
			
			// Gestione Primo Livello -> START
			// Per ogni nodo di primo livello si crea il nodo da mettere nell'albero che ha come padre nodoPadre
			List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
			
			DefaultTreeNode nodoDaAggiungere = null;
			for (final NodoOrganigrammaDTO n : primoLivello) {
				
				n.setCodiceAOO(getUtente().getCodiceAoo());
				n.setUtentiVisible(true);
				nodoDaAggiungere = new DefaultTreeNode(n, nodoRadice);
				nodoDaAggiungere.setExpanded(true);
				if (TipologiaNodoEnum.UTENTE.equals(n.getTipoNodo())) {
					nodoDaAggiungere.setType("USER");
				} else {
					nodoDaAggiungere.setType("UFF");
				}
				
				if (n.getIdUtente() == null && alberatura.contains(n.getIdNodo())) {
					nodiDaAprire.add(nodoDaAggiungere);
				}
				
			}
			// Gestione Primo Livello -> END
			
			final List<DefaultTreeNode> nodiDaAprireTemp = new ArrayList<>();
			for (index = 0; index < alberatura.size(); index++) {
				// Il numero di cicli è esattamente uguale alla lunghezza dell'alberatura
				nodiDaAprireTemp.clear();
				for (final DefaultTreeNode nodo : nodiDaAprire) {
					
					final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) nodo.getData();
					onOpenNode(nodoExp, nodo);
					nodo.setExpanded(true);
					
					for (final TreeNode nodoFiglio : nodo.getChildren()) {
						final NodoOrganigrammaDTO nodoFiglioDTO = (NodoOrganigrammaDTO) nodoFiglio.getData();
						if (nodoFiglioDTO.getIdUtente() == null && alberatura.contains(nodoFiglioDTO.getIdNodo())) {
							nodiDaAprireTemp.add((DefaultTreeNode) nodoFiglio);
						}
					}
					
				}
				nodiDaAprire = new ArrayList<>(nodiDaAprireTemp);
			}
			
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}
	
	
	/**
	 * Metodo triggerato dall'evento expand del tree node.
	 * 
	 * @param event
	 */
	public void onOpenNode(final NodeExpandEvent event) {
		final NodoOrganigrammaDTO nodoExp = (NodoOrganigrammaDTO) event.getTreeNode().getData();
		onOpenNode(nodoExp, event.getTreeNode());
	}
	
	/**
	 * Crea nuovi nodi in modo gerarchico.
	 * @param nodoExp
	 * @param treeNode
	 */
	public void onOpenNode(final NodoOrganigrammaDTO nodoExp, final TreeNode treeNode) {
		try {
			// Gestione Secondo Livello -> START
			// Tramite il DTO si recuperano tutti i figli di questo nodo
			final List<NodoOrganigrammaDTO> figli = getOrgSRV().getFigliAlberoAssegnatarioPerFirma(nodoExp.getIdNodo(), nodoExp);
			
			// Si puliscono eventuali figli nel nodo
			treeNode.getChildren().clear();
			
			TreeNode nodoFiglio = null;
			for (final NodoOrganigrammaDTO figlio : figli) {
				
				figlio.setCodiceAOO(getUtente().getCodiceAoo());
				figlio.setUtentiVisible(true);
				// Con ogni figlio ottenuto creo un nuovo nodo (utente) da assegnare al padre
				nodoFiglio = new DefaultTreeNode(figlio, treeNode);
				// Si controlla se è un utente e nel caso setto il type
				if (TipologiaNodoEnum.UTENTE.equals(figlio.getTipoNodo())) {
					nodoFiglio.setType("USER");
				} else {
					nodoFiglio.setType("UFF");
				}
				if (figlio.getFigli() > 0 || figlio.isUtentiVisible()) {
					new DefaultTreeNode(new NodoOrganigrammaDTO(), nodoFiglio);
				}
				
			}
			// Gestione Secondo Livello -> END
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Recupera l'attributo urgente.
	 * @return urgente
	 */
	public Boolean getUrgente() {
		return urgente;
	}

	/**
	 * Imposta l'attributo urgente.
	 * @param urgente
	 */
	public void setUrgente(final Boolean urgente) {
		this.urgente = urgente;
	}
	
}
