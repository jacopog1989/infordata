package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.service.facade.IRichiesteVistoFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractTreeBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;
import it.ibm.red.web.mbean.interfaces.OrganigrammaRetriever;

/**
 * Bean che gestisce la response di richiesta visti.
 */
@Named(ConstantsWeb.MBean.RICHIESTA_VISTI_BEAN)
@ViewScoped
public class RichiestaVistiBean extends AbstractTreeBean implements OrganigrammaRetriever, InitHumanTaskInterface {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 7440275042229616594L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RichiestaVistiBean.class);

	/**
	 * Servizio.
	 */
	private IRichiesteVistoFacadeSRV vistiSRV;

	/**
	 * Servizio.
	 */
	private IOrganigrammaFacadeSRV organigrammaSRV;

	/**
	 * Lista documenti bean.
	 */
	private ListaDocumentiBean ldb;

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Bean assegnazione.
	 */
	private MultiAssegnazioneBean assegnazioneBean;

	/**
	 * Documento selezionato.
	 */
	private MasterDocumentRedDTO selectedDocument;

	/**
	 * Input proveniente dalla maschera.
	 */
	private String motivoAssegnazioneNew;

	/**
	 * LABERATURA DA ESPLODERE START
	 ***********************************************************************************/
	private List<Nodo> alberaturaNodi;
	/**LABERATURA DA ESPLODERE END*************************************************************************************/
	private BooleanFlagEnum isDestDelegato;

	/**
	 * Post construct del bean.
	 */
	@PostConstruct
	@Override
	protected void postConstruct() {
		final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
		utente = sb.getUtente();
		organigrammaSRV = ApplicationContextProvider.getApplicationContext().getBean(IOrganigrammaFacadeSRV.class);
		alberaturaNodi = organigrammaSRV.getAlberaturaBottomUp(utente.getIdAoo(), utente.getIdUfficio());

		vistiSRV = ApplicationContextProvider.getApplicationContext().getBean(IRichiesteVistoFacadeSRV.class);

		assegnazioneBean = new MultiAssegnazioneBean();
		isDestDelegato = BooleanFlagEnum.NO;
		// Vedere initBean di seguito per ulteriore inizializzazioni.
	}

	/**
	 * Gestisce il caricamento dell'organigramma restituendo la radice dello stesso.
	 */
	@Override
	public TreeNode loadRootOrganigramma() {
		DefaultTreeNode nodoBase = null;
		try {
			final List<Long> alberatura = new ArrayList<>();
			for (int i = alberaturaNodi.size() - 1; i >= 0; i--) {
				alberatura.add(alberaturaNodi.get(i).getIdNodo());
			}

			NodoOrganigrammaDTO nodoRadice = new NodoOrganigrammaDTO();
			nodoRadice.setIdAOO(getUtente().getIdAoo());
			nodoRadice.setIdNodo(getUtente().getIdNodoRadiceAOO());
			nodoRadice.setCodiceAOO(getUtente().getCodiceAoo());
			nodoRadice.setUtentiVisible(false);

			List<NodoOrganigrammaDTO> primoLivello = organigrammaSRV.getFigliAlberoAssegnatarioPerCompetenzaUscita(getUtente(), nodoRadice, false);
			
		
			
			nodoBase = new DefaultTreeNode(nodoRadice, null);
			nodoBase.setExpanded(true);
			nodoBase.setSelectable(false);

			// per ogni nodo di primo livello creo il nodo da mettere nell'albero che ha
			// come padre rootAssegnazionePerCompetenza
			List<DefaultTreeNode> nodiDaAprire = new ArrayList<>();
			nodoBase.setChildren(new ArrayList<>());
			DefaultTreeNode nodoToAdd = null;
			for (final NodoOrganigrammaDTO n : primoLivello) {
				n.setCodiceAOO(utente.getCodiceAoo());
				nodoToAdd = new DefaultTreeNode(n, nodoBase);
				nodoToAdd.setExpanded(true);
				if (n.getIdUtente() == null && alberatura.contains(n.getIdNodo())) {
					nodiDaAprire.add(nodoToAdd);
				}
			}

			nodiDaAprire(alberatura, nodiDaAprire);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}

		return nodoBase;
	}

	/**
	 * Gestisce l'aperura dell'albero.
	 */
	@Override
	public void onOpenTree(final TreeNode treeNode) {
		try {
			List<NodoOrganigrammaDTO> figli = organigrammaSRV.getFigliAlberoAssegnatarioPerCompetenzaUscita(getUtente(), (NodoOrganigrammaDTO) treeNode.getData(), false);
			
			super.onOpenTree(treeNode, figli);
			
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione: " + e.getMessage());
		}
	}

	/* FINE GESTIONE ALBERO */
	/**
	 * Gestisce la logica della response.
	 */
	public void doResponse() {
		// pulisco gli esiti
		ldb.cleanEsiti();
		try {
			final MasterDocumentRedDTO master = this.selectedDocument;
			final String wobNumber = master.getWobNumber();
			if (StringUtils.isEmpty(wobNumber)) {
				LOGGER.error("Non è stato selezionato nessun documento per l'esecuzione della response" + ldb.getSelectedResponse().getResponse());
				throw new RedException("Nessun documento selezionato per l'esecuzione della response");
			}

			final List<AssegnazioneDTO> assegnatari = assegnazioneBean.getAssegnazioneList();

			boolean isDelegato = false;
			if (BooleanFlagEnum.SI.equals(isDestDelegato)) {
				isDelegato = true;
			}
			
			EsitoOperazioneDTO esito;
			if (CollectionUtils.isEmpty(assegnatari)) {
				esito = new EsitoOperazioneDTO(null, false, "Per continuare è necessario aggiungere almeno un assegnatario per conoscenza");
			} else {
				if (ldb.getSelectedResponse() == ResponsesRedEnum.RICHIESTA_VISTI) {
					esito = vistiSRV.richiestaVisti(utente, wobNumber, motivoAssegnazioneNew, assegnatari, isDelegato);
				} else if (ldb.getSelectedResponse() == ResponsesRedEnum.RICHIESTA_VISTI_2) {
					esito = vistiSRV.richiestaVisti2(utente, wobNumber, motivoAssegnazioneNew, assegnatari, isDelegato);
				} else {
					esito = new EsitoOperazioneDTO(wobNumber, false, "Nessuna response gestita richiamata");
				}
			}
			ldb.getEsitiOperazione().add(esito);
			if (esito.isEsito()) {
				ldb.setMessaggioEsitoPositivo("Operazione avvenuta con successo.");
			}
		} catch (final RedException redEx) {
			showError(redEx);
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		} finally {
			destroyBean();
		}
	}

	/**
	 * Esegue una validazione sui parametri e lancia {@link #doResponse()}.
	 */
	public void checkEResponse() {
		final List<AssegnazioneDTO> assegnatari = assegnazioneBean.getAssegnazioneList();
		if (CollectionUtils.isEmpty(assegnatari)) {
			showWarnMessage("Per continuare è necessario aggiungere almeno un assegnatario per conoscenza");

		} else {
			doResponse();
			FacesHelper.executeJS("PF('dlgGeneric').hide()");
			FacesHelper.executeJS("PF('dlgShowResult').show()");
			FacesHelper.update("centralSectionForm:idDlgShowResult");
		}
	}

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		if (CollectionUtils.isEmpty(inDocsSelezionati)) {
			final String msg = "Non è presente nessun documento selezionato";
			LOGGER.error(msg);
			showError(msg);
		} else {
			this.selectedDocument = inDocsSelezionati.get(0);
			final Set<NodoOrganigrammaDTO> defaultNodoList = vistiSRV.getUfficioContributi(selectedDocument.getWobNumber(), selectedDocument.getDocumentTitle(), utente);
			assegnazioneBean.initialize(this, TipoAssegnazioneEnum.VISTO, defaultNodoList);
		}
	}

	/**
	 * Esegue le azioni a monte della disruzione del bean.
	 */
	public void destroyBean() {
		ldb.destroyBeanViewScope(ConstantsWeb.MBean.RICHIESTA_VISTI_BEAN);
	}

	/**
	 * Restituisce l'utente.
	 * 
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'utente.
	 * 
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Restituisce il motivo dell'assegnazione.
	 * 
	 * @return motivo assegnazione
	 */
	public String getMotivoAssegnazioneNew() {
		return motivoAssegnazioneNew;
	}

	/**
	 * Imposta il motivo dell'assegnazione.
	 * 
	 * @param motivoAssegnazioneNew
	 */
	public void setMotivoAssegnazioneNew(final String motivoAssegnazioneNew) {
		this.motivoAssegnazioneNew = motivoAssegnazioneNew;
	}

	/**
	 * Restituisce il bean per l'assegnazione multipla.
	 * 
	 * @return bean per multi assegnazione.
	 */
	public MultiAssegnazioneBean getAssegnazioneBean() {
		return assegnazioneBean;
	}

	/**
	 * @return the isDestDelegato
	 */
	public BooleanFlagEnum getIsDestDelegato() {
		return isDestDelegato;
	}

	/**
	 * @param isDestDelegato the isDestDelegato to set
	 */
	public void setIsDestDelegato(BooleanFlagEnum isDestDelegato) {
		this.isDestDelegato = isDestDelegato;
	}

	/**
	 * Restituisce il master selezionato.
	 * @return master selezionato
	 */
	public MasterDocumentRedDTO getSelectedDocument() {
		return selectedDocument;
	}

}
