package it.ibm.red.web.document.helper;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.EsitoPredisponiDocumentoDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PredisponiDocumentoMessageEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IPredisponiDocumentoFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.mbean.DocumentManagerBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.SessionBean;


/**
 * Classe PredisponiDocumentoHelper.
 *
 * @author m.crescentini
 */
public class PredisponiDocumentoHelper implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PredisponiDocumentoHelper.class);

	/**
	 * Documento da ribaltare.
	 */
	private final MasterDocumentRedDTO docDaRibaltare;

	/**
	 * Lista documenti da allacciare.
	 */
	private final List<MasterDocumentRedDTO> docsDaAllacciareList;
	
	/**
	 * Documento da ribaltare in risposta.
	 */
	private RispostaAllaccioDTO docDaRibaltareAllaccio;
	
	/**
	 * Lista documento da allacciare in risposta.
	 */
	private List<RispostaAllaccioDTO> docsDaAllacciareAllaccioList;
	
	/**
	 * Response sollecitata.
	 */
	private final ResponsesRedEnum responseSollecitata;
	
	/**
	 * Utente.
	 */
	private final UtenteDTO utente;
		
	/**
	 * Esito predisposizione.
	 */
	private EsitoPredisponiDocumentoDTO esitoPredisposizione;
	
	
	/**
	 * Costruttore.
	 * 
	 * @param inDocDaRibaltare
	 * @param inDocsDaAllacciareList
	 * @param responseSollecitata
	 * @param utente
	 */
	public PredisponiDocumentoHelper(final MasterDocumentRedDTO inDocDaRibaltare, final List<MasterDocumentRedDTO> inDocsDaAllacciareList, 
			final ResponsesRedEnum responseSollecitata, final UtenteDTO utente) {
		this(inDocDaRibaltare, inDocsDaAllacciareList, responseSollecitata, utente, false);
	}
	
	
	/**
	 * Costruttore.
	 * 
	 * @param inDocDaRibaltare
	 * @param inDocsDaAllacciareList
	 * @param responseSollecitata
	 * @param utente
	 * @param trasformaInAllaccio
	 */
	public PredisponiDocumentoHelper(final MasterDocumentRedDTO inDocDaRibaltare, final List<MasterDocumentRedDTO> inDocsDaAllacciareList, 
			final ResponsesRedEnum responseSollecitata, final UtenteDTO utente, final boolean trasformaInAllaccio) {
		this.docDaRibaltare = inDocDaRibaltare;
		this.docsDaAllacciareList = inDocsDaAllacciareList;
		this.responseSollecitata = responseSollecitata;
		this.utente = utente;
		this.esitoPredisposizione = new EsitoPredisponiDocumentoDTO();
		if (trasformaInAllaccio) {
			trasformaInRisposteAllaccio();
			trasformaInRispostaAllaccio();
		}
	}
	
	
	/**
	 * Predisposizione base, senza registro ausiliario.
	 */
	public void predisponiUscita() {
		predisponiUscita(null, null, null, null);
	}
	

	/**
	 * Predisposizione dalla maschera di generazione template a partire da un registro ausiliario.
	 *
	 * @param contentUscita the content uscita
	 * @param idRegistroAusiliario the id registro ausiliario
	 * @param metadatiRegistroAusiliarioList the metadati registro ausiliario list
	 * @param metadatiDocUscita the metadati doc uscita
	 */
	public void predisponiUscita(final FileDTO contentUscita, final Integer idRegistroAusiliario, final Collection<MetadatoDTO> metadatiRegistroAusiliarioList, 
			final Collection<MetadatoDTO> metadatiDocUscita) {
		LOGGER.info("predisponiUscita -> START");		
		final IPredisponiDocumentoFacadeSRV predDocSRV = ApplicationContextProvider.getApplicationContext().getBean(IPredisponiDocumentoFacadeSRV.class);
		
		if (docDaRibaltareAllaccio == null) {
			trasformaInRispostaAllaccio();
		}
		
		if (docsDaAllacciareAllaccioList == null) {
			trasformaInRisposteAllaccio();
		}
		
		esitoPredisposizione = predDocSRV.predisponiUscita(utente, responseSollecitata, docsDaAllacciareAllaccioList, 
				docDaRibaltareAllaccio, contentUscita, idRegistroAusiliario, metadatiRegistroAusiliarioList, metadatiDocUscita);
		
		LOGGER.info("predisponiUscita -> END. Predisposizione OK: " + esitoPredisposizione.isEsito());
	}
	

	/**
	 * Aggiornamento della dialog di creazione e apertura della stessa.
	 */
	public void apriDialogCreazioneDocPredisposto() {
		final DocumentManagerBean dmb = FacesHelper.getManagedBean(MBean.DOCUMENT_MANAGER_BEAN);
		final SessionBean sessionBean = FacesHelper.getManagedBean(MBean.SESSION_BEAN);
		
		dmb.unsetDetail(); // Si pulisce il dettaglio 
		// Il chiamante corrisponde alla response sollecitata.
		// Serve per la gestione dei singoli casi (e.g. combo abilitate/disabilitate) nel setDetail del DocumentManagerBean.
		if (responseSollecitata != null) {
			dmb.setChiamante(responseSollecitata.toString());
		}
		for (final Nodo nodo : sessionBean.getListaUffici()) {
			if ((nodo.getDescrizione().equals(utente.getNodoDesc())) 
					&& nodo.getIsEstendiVisibilita()) {
				dmb.setIsEstendiVisibilitaChk(true);
			}
		}
		dmb.setDetail(esitoPredisposizione.getDocPredisposto());
		
		FacesHelper.update("idDettagliEstesiForm:idPanelDocumentManager");
		FacesHelper.update("idDettagliEstesiForm:idPanelContainerDocumentManager_T");
		
		FacesHelper.executeJS("PF('dlgManageDocument_RM').show()");
		FacesHelper.executeJS("PF('dlgManageDocument_RM').toggleMaximize();");
		
		// Contestualmente all'apertura della maschera, si memorizza il messaggio da mostrare in fase di registrazione documento
		if (esitoPredisposizione.getCodiceErrore() != null) {
			final PredisponiDocumentoMessageEnum messaggioPredisposizione = (PredisponiDocumentoMessageEnum) esitoPredisposizione.getCodiceErrore();
			dmb.setMsgPredisponi(messaggioPredisposizione.getMessage());
		}
	}
	
	private void trasformaInRisposteAllaccio() {
		final IPredisponiDocumentoFacadeSRV predDocSRV = ApplicationContextProvider.getApplicationContext().getBean(IPredisponiDocumentoFacadeSRV.class);
		docsDaAllacciareAllaccioList = predDocSRV.trasformaInRisposteAllaccio(docsDaAllacciareList);
	}
	
	private void trasformaInRispostaAllaccio() {
		final IPredisponiDocumentoFacadeSRV predDocSRV = ApplicationContextProvider.getApplicationContext().getBean(IPredisponiDocumentoFacadeSRV.class);
		docDaRibaltareAllaccio = predDocSRV.trasformaInRispostaAllaccio(docDaRibaltare);
	}

	/** 
	 * @return the doc da ribaltare allaccio
	 */
	public RispostaAllaccioDTO getDocDaRibaltareAllaccio() {
		return docDaRibaltareAllaccio;
	}

	/** 
	 * @param docDaRibaltareAllaccio the new doc da ribaltare allaccio
	 */
	public void setDocDaRibaltareAllaccio(final RispostaAllaccioDTO docDaRibaltareAllaccio) {
		this.docDaRibaltareAllaccio = docDaRibaltareAllaccio;
	}

	/** 
	 * @return the docs da allacciare allaccio list
	 */
	public List<RispostaAllaccioDTO> getDocsDaAllacciareAllaccioList() {
		return docsDaAllacciareAllaccioList;
	}

	/** 
	 * @param docsDaAllacciareAllaccioList the new docs da allacciare allaccio list
	 */
	public void setDocsDaAllacciareAllaccioList(final List<RispostaAllaccioDTO> docsDaAllacciareAllaccioList) {
		this.docsDaAllacciareAllaccioList = docsDaAllacciareAllaccioList;
	}

	/** 
	 * @return the esito predisposizione
	 */
	public EsitoPredisponiDocumentoDTO getEsitoPredisposizione() {
		return esitoPredisposizione;
	}
	
}