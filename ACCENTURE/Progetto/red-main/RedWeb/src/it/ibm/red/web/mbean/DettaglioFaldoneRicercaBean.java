package it.ibm.red.web.mbean;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.web.constants.ConstantsWeb;

/**
 * Bean per la ricerca del dettaglio faldone.
 */
@Named(ConstantsWeb.MBean.DETTAGLIO_FALDONE_RICERCA_BEAN)
@ViewScoped
public class DettaglioFaldoneRicercaBean extends DettaglioFaldoneAbstractBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 3846967474303084159L;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		super.postConstruct();
	}

	
	
}
