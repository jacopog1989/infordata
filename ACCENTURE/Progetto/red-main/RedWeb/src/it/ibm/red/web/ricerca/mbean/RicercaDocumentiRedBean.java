/**
 * 
 */
package it.ibm.red.web.ricerca.mbean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.StatoRicercaDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * @author APerquoti
 *
 */
@Named(ConstantsWeb.MBean.RICERCA_DOCUMENTI_RED_BEAN)
@ViewScoped
public class RicercaDocumentiRedBean extends RicercaDocumentiRedAbstractBean {

	private static final long serialVersionUID = 2640661493440565089L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaDocumentiRedBean.class.getName());
	
	/**
	 * Documenti.
	 */
	protected SimpleDetailDataTableHelper<MasterDocumentRedDTO> documentiRedDTH;
	
	/**
	 * Parametri ricerca.
	 */
	private ParamsRicercaAvanzataDocDTO formRicerca;
	
	/**
	 * Colonne.
	 */
	private List<Boolean> docColumns;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		super.postConstruct();
		
		documentiRedDTH = new SimpleDetailDataTableHelper<>();
		documentiRedDTH.setMasters(new ArrayList<>());
		
		docColumns = Arrays.asList(true, true, false, true, false, false, false, false, false, false, false);
		
		initPaginaRicerca();
	}

	/**
	 * Inizializza la pagina di ricerca.
	 */
	@SuppressWarnings("unchecked")
	public void initPaginaRicerca() {
		try {
			final StatoRicercaDTO stDto = (StatoRicercaDTO) FacesHelper.getObjectFromSession(ConstantsWeb.SessionObject.STATO_RICERCA, true);
			if (stDto != null) {
				final Collection<MasterDocumentRedDTO> searchResult = (Collection<MasterDocumentRedDTO>) stDto.getRisultatiRicerca();
				formRicerca = (ParamsRicercaAvanzataDocDTO) stDto.getFormRicerca();
				
				documentiRedDTH.setMasters(searchResult);
				documentiRedDTH.selectFirst(true);
				selectDetail(documentiRedDTH.getCurrentMaster());
				
				if (getNumMaxRisultati() != null && searchResult != null && getNumMaxRisultati().equals(searchResult.size())) {
					showWarnMessage(RicercaAbstractBean.MSG_NUM_MAX_RISULTATI);
				}
			} else {
				formRicerca = new ParamsRicercaAvanzataDocDTO();
			}
			
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Non è stato possibile recuperare i risultati di ricerca");
		}
	}

	/**
	 * Gestisce la selezione della riga del datatable.
	 * @param se
	 */
	public final void rowSelector(final SelectEvent se) {
		documentiRedDTH.rowSelector(se);
		selectDetail(documentiRedDTH.getCurrentMaster());
	}
	
	/**
	 * Restituisce il master selezionato.
	 * @return master selezionato
	 */
	@Override
	public MasterDocumentRedDTO getSelectedDocument() {
		return documentiRedDTH.getCurrentMaster();
	}
	
	/**
	 * Metodo per la selezione e aggiornamento del dettaglio dall'esterno
	 */
	public void aggiorna() {
		if (documentiRedDTH != null && documentiRedDTH.getCurrentMaster() != null) {
			selectDetail(documentiRedDTH.getCurrentMaster());
		}
	}
	
	/**
	 * Metodo per reimpostare nel session bean il form utilizzato per la ricerca e quindi visualizzarlo nel dialog
	 */
	public void ripetiRicercaRedDoc() {
		try {
			// Viene ripetuta la ricerca con i stessi filtri
			sessionBean.getRicercaFormContainer().setDocumentiRed(formRicerca);
			sessionBean.setDescrAssegnatario(formRicerca.getDescrAssegnatario());
			sessionBean.setDescrAssegnatarioOperazione(formRicerca.getDescrAssegnatarioOperazione());
			
		} catch (final Exception e) {
			LOGGER.error(e);
			showError(e);
		}
	}

	/**
	 * Gestisce la selezione e la deselezione della colonna.
	 * @param event
	 */
	public final void onColumnToggleDocumenti(final ToggleEvent event) {
		docColumns.set((Integer) event.getData(), Visibility.VISIBLE.equals(event.getVisibility()));
	}

	
	/**
	 * @return the documentiRedDTH
	 */
	public final SimpleDetailDataTableHelper<MasterDocumentRedDTO> getDocumentiRedDTH() {
		return documentiRedDTH;
	}

	/**
	 * @param documentiRedDTH the documentiRedDTH to set
	 */
	public final void setDocumentiRedDTH(final SimpleDetailDataTableHelper<MasterDocumentRedDTO> documentiRedDTH) {
		this.documentiRedDTH = documentiRedDTH;
	}

	/**
	 * @return the docColumns
	 */
	public final List<Boolean> getDocColumns() {
		return docColumns;
	}

	/**
	 * @param docColumns the docColumns to set
	 */
	public final void setDocColumns(final List<Boolean> docColumns) {
		this.docColumns = docColumns;
	}
	
	/**
	 * Restituisce il prefisso del nome file excel per l'estrazione.
	 * @return prefisso nome file
	 */
	@Override
	protected String getNomeRicerca4Excel() {
		return "ricercadocumenti_";
	}
}
