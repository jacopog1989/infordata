/**
 * 
 */
package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ISpeditoFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.ListaDocumentiBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.mbean.interfaces.InitHumanTaskInterface;

/**
 * @author APerquoti
 *
 */
@Named(ConstantsWeb.MBean.SPEDITO_BEAN)
@ViewScoped
public class SpeditoBean extends AbstractBean implements InitHumanTaskInterface {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 977948133396914398L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SpeditoBean.class.getName());
	
	/**
	 * Lista documenti bean.
	 */
	private ListaDocumentiBean ldb;
		
	/**
	 * Lista master.
	 */
	private Collection<MasterDocumentRedDTO> masters;
	
	/**
	 * Informazioni utente.
	 */
	private UtenteDTO utente;

	/**
	 * Data spedizione.
	 */
	private Date dataSpedito;
	
	/**
	 * la data massima selezionabile che corrisponse alla data odierna.
	 */
	private Date maxDate;
	
	/**
	 * la data minima selezionabile che corrisponde alla data di protocollazione.
	 */
	private Date minDate;
	
	/**
	 * booleano per gestire l'attivazione del componente Calendar.
	 */
	private Boolean disableCalendar = false;
	
	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		ldb = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
		final SessionBean sb = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sb.getUtente();
		//imposto la max date selezionabile
		maxDate = new Date();
		
	}
	
	/**
	 * Esegue la logica associata alla response SPEDITO, gestisce gli esiti a valle delle operazioni in modo da permettere una
	 * corretta comunicazione con l'utente che ha sollecitato la response.
	 */
	public void speditoResponse() {
		Collection<EsitoOperazioneDTO> eOpe = new ArrayList<>();
		final Collection<String> wobNumbers = new ArrayList<>();
		try {
			final ISpeditoFacadeSRV speditoSRV = ApplicationContextProvider.getApplicationContext().getBean(ISpeditoFacadeSRV.class);

			//pulisco gli esiti
			ldb.cleanEsiti();
			
			for (final MasterDocumentRedDTO m : masters) {
				if (!StringUtils.isNullOrEmpty(m.getWobNumber())) {
					wobNumbers.add(m.getWobNumber());
				}
			}
			eOpe = speditoSRV.spedito(utente, dataSpedito, wobNumbers);
			
			//setto gli esiti con i nuovi e eseguo un refresh
			ldb.getEsitiOperazione().addAll(eOpe);
			ldb.destroyBeanViewScope(ConstantsWeb.MBean.SPEDITO_BEAN);
			
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il tentato sollecito della response SPEDITO", e);
			showError("Si è verificato un errore durante il tentato sollecito della response SPEDITO");
		}
	}
	
	
//	##################  START GET/SET ###################

	/**
	 * @return the dataSpedito
	 */
	public final Date getDataSpedito() {
		return dataSpedito;
	}

	/**
	 * @param dataSpedito the dataSpedito to set
	 */
	public final void setDataSpedito(final Date dataSpedito) {
		this.dataSpedito = dataSpedito;
	}
	
	/**
	 * @return the maxDate
	 */
	public final Date getMaxDate() {
		return maxDate;
	}

	/**
	 * @param maxDate the maxDate to set
	 */
	public final void setMaxDate(final Date maxDate) {
		this.maxDate = maxDate;
	}

	/**
	 * @return the minDate
	 */
	public final Date getMinDate() {
		return minDate;
	}

	/**
	 * @param minDate the minDate to set
	 */
	public final void setMinDate(final Date minDate) {
		this.minDate = minDate;
	}

	/**
	 * @return the disableCalendar
	 */
	public Boolean getDisableCalendar() {
		return disableCalendar;
	}

	/**
	 * @param disableCalendar the disableCalendar to set
	 */
	public void setDisableCalendar(final Boolean disableCalendar) {
		this.disableCalendar = disableCalendar;
	}

	/**
	 * Inizializza il bean.
	 * 
	 * @param inDocsSelezionati
	 *            documenti selezionati
	 */
	@Override
	public void initBean(final List<MasterDocumentRedDTO> inDocsSelezionati) {
		
		masters = inDocsSelezionati;
		
		for (final MasterDocumentRedDTO m : masters) {
			if (m.getDataProtocollazione() != null) {
				//metto da parte il primo selezionato
				if (minDate == null) {
					minDate = m.getDataProtocollazione();
				}
				
				//verifico se le successive date sono più recenti rispetto a quella messa da parte
				if (minDate.before(m.getDataProtocollazione())) {
					//nel caso positivo aggiorno la min date con la nuova data da pendere in considerazione
					minDate = m.getDataProtocollazione();
				}
				
				//imposto una data iniziale di default
				dataSpedito = new Date();
				
			} else {
				//se anche un solo master non possiede la protocollazione disabilito il componente calendario e imposto la data di spedizione con quella attuale
				//se si verifica una situazione del genere è un'anomalia e quindi loggo i dati del documento in questione
				disableCalendar = true;
				dataSpedito = new Date();
				LOGGER.error(" " + m.toString());
			}
		}
	}


	

}
