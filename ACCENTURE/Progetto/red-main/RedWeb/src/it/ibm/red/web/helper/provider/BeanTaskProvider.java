package it.ibm.red.web.helper.provider;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.HashMap;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;

import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.dto.BeanTaskDTO;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Provider dei bean task.
 */
public final class BeanTaskProvider {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(BeanTaskProvider.class.getName());

	/**
	 * Beans.
	 */
	private final HashMap<String, BeanTaskDTO> beans;

	/**
	 * Istanza (singleton).
	 */
	private static BeanTaskProvider provider = new BeanTaskProvider();

	/**
	 * Costruttore.
	 */
	private BeanTaskProvider() {
		beans = new HashMap<>();
		final ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(true);
		for (final BeanDefinition bd : scanner.findCandidateComponents("it.ibm.red.web.mbean")) {
			try {
				final Class<?> beanCls = Class.forName(bd.getBeanClassName());
				for (final Method mtd : beanCls.getMethods()) {
					final BeanTask st = mtd.getAnnotation(BeanTask.class);
					if (st != null) {
						BeanTaskDTO beanDTO = beans.get(st.name());
						if (beanDTO != null) {
							throw new RedException("Non è possibile annotare due metodi differenti con lo stesso bean task.");
						}
						beanDTO = new BeanTaskDTO(FacesHelper.getBeanName(beanCls), mtd);
						beans.put(st.name(), beanDTO);
					}
				}
			} catch (final ClassNotFoundException e) {
				LOGGER.error(e);
				throw new RedException(e);
			}
		}
	}
	
	/**
	 * Restituisce il provider.
	 * @return provider
	 */
	public static BeanTaskProvider getProvider() {
		return provider;
	}

	/**
	 * Restituisce informazioni sul BeanTask sotto forma di BeanTaskDTO.
	 * @param name
	 * @return BeanTaskDTO che contiene le informazioni del bean task
	 */
	public BeanTaskDTO getBeanTaskInfo(final String name) {
		return beans.get(name);
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public @interface BeanTask {
		/**
		 * Restituisce il nome.
		 * @return nome
		 */
		String name();
	}
}
