package it.ibm.red.web.document.mbean.component;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.ITitolarioFacadeSRV;

/**
 * 
 * 
 * @author m.crescentini
 *
 */
public abstract class AbstractIndiceDiClassificazioneComponent extends AbstractComponent {
	
	/**
	 * Messaggio di errore generico, da visualizzare prima di un errore.
	 */
	private static final String GENERIC_ERROR_MESSAGE = "Errore durante l'operazione: ";

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -3526336386376525614L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AbstractIndiceDiClassificazioneComponent.class);
	
	/**
	 * Utente.
	 */
	protected UtenteDTO utente;
	
	/**
	 * Servizio gestione titolario.
	 */
	protected ITitolarioFacadeSRV titolarioSRV;
	
	/**
	 * Nodo titolario.
	 */
	protected transient TreeNode rootTitolario;
	
	/**
	 * Nodo selezionato.
	 */
	protected transient TreeNode selectedNode;
	
	/**
	 * Costruttore di classe.
	 * 
	 * @param utente
	 * */
	protected AbstractIndiceDiClassificazioneComponent(final UtenteDTO utente) {
		this.utente = utente;
		this.titolarioSRV = ApplicationContextProvider.getApplicationContext().getBean(ITitolarioFacadeSRV.class);
	}
	
	
	/**
	 * Gestione della selezione di un titolario.
	 * Ogni component che estende questo Abstract implementa la sua logica da eseguire in seguito alla selezione.
	 * 
	 * @param selectedTitolario
	 */
	protected abstract void setTitolarioNodoSelected(TitolarioDTO selectedTitolario);
	
	
	/**
	 * Gestione del titolario da mostrare nel campo di input (autocomplete) al
	 * momento della visualizzazione del component del titolario. Ogni component che
	 * estende questo Abstract implementa la sua logica di recupero.
	 */
	protected abstract TitolarioDTO getTitolarioNodoSelected();
	
	/**
	 * Crea il titolario.
	 */
	protected void creaTitolario() {
		try {
			List<TitolarioDTO> rootChildren = retrieveRootChildren();
			TitolarioDTO root = new TitolarioDTO("", "", "Titolario", 0, utente.getIdAoo(), null, null, rootChildren.size()); 
			rootTitolario = new DefaultTreeNode(root, null);
			
			wrappaAlberoTitolario(rootChildren, rootTitolario);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage(GENERIC_ERROR_MESSAGE + e.getMessage());
		}
	}

	/**
	 * Recupera i nodi radice del Titolario.
	 * 
	 * @return List di TitolarioDTO
	 */
	protected List<TitolarioDTO> retrieveRootChildren() {
		return titolarioSRV.getFigliRadice(utente.getIdAoo(), utente.getIdUfficio());
	}
	
	/**
	 * Gestisce la logica di espansione del nodo del Titolario.
	 * 
	 * @param event
	 * */
	public void onTitolarioNodeExpand(final NodeExpandEvent event) {
		try {
			TreeNode node = event.getTreeNode();
			TitolarioDTO toOpen = (TitolarioDTO) node.getData();
			node.getChildren().clear();
			
			List<TitolarioDTO> rootChildren = titolarioSRV.getFigliByIndice(utente.getIdAoo(), toOpen.getIndiceClassificazione());
			
			wrappaAlberoTitolario(rootChildren, node);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage(GENERIC_ERROR_MESSAGE + e.getMessage());

		}
	}
	
	/**
	 * Esegue il wrap dei nodi dell'albero nel titolario.
	 * 
	 * @param rootChildren
	 *            nodi albero
	 * @param node
	 */
	protected void wrappaAlberoTitolario(final List<TitolarioDTO> rootChildren, final TreeNode node) {
		if (CollectionUtils.isNotEmpty(rootChildren)) {
			for (TitolarioDTO t : rootChildren) {
				DefaultTreeNode indiceToAdd = new DefaultTreeNode(t, node);
				if (t.getNumeroFigli() > 0) {
					new DefaultTreeNode(null, indiceToAdd);
				}
			}
		}
	}
	
	/**
	 * Gestisce la logica di selezione del nodo Titolario.
	 * 
	 * @param event evento di selezione nodo
	 * */
	public void onTitolarioNodeSelect(final NodeSelectEvent event) {
		try {
			TitolarioDTO titolarioNodoSelected = (TitolarioDTO) event.getTreeNode().getData();

			setTitolarioNodoSelected(titolarioNodoSelected);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage(GENERIC_ERROR_MESSAGE + e.getMessage());
		}
	}
	
	
	/**
	 * AUTOCOMPLETE - Restituisce la lista da visualizzare.
	 * 
	 * @return List di TitolarioDTO che contiene tutte le informazioni su Titolario
	 */
	public List<TitolarioDTO> titolarioAutocomplete(final String parola) {
		List<TitolarioDTO> titolarioAutocompleteList = new ArrayList<>();
		
		try {
			titolarioAutocompleteList = titolarioSRV.getNodiAutocomplete(utente.getIdAoo(), utente.getIdUfficio(), parola);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage("Errore nel recupero delle voci del titolario.");
		}
		
		return titolarioAutocompleteList;
	}
	

	/**
	 * AUTOCOMPLETE - Gestione della selezione dell'autocomplete.
	 * 
	 * @param event
	 */
	public void onTitolarioAutocompleteSelect(final SelectEvent event) {
		try {
			TitolarioDTO inTitolario = (TitolarioDTO) event.getObject();
			
			setTitolarioNodoSelected(inTitolario);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage(GENERIC_ERROR_MESSAGE + e.getMessage());
		}
	}
	
	/**
	 * Restituisce la root del titolario.
	 * @return root titolario
	 */
	public TreeNode getRootTitolario() {
		return rootTitolario;
	}

	/**
	 * Restituisce il nodo selezionato.
	 * @return nodo selezionato
	 */
	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	/**
	 * Imposta il nodo selezionato.
	 * @param selectedNode
	 */
	public void setSelectedNode(final TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}	
}