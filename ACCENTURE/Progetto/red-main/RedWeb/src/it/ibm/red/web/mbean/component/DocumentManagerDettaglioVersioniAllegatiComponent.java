package it.ibm.red.web.mbean.component;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.VersionDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV;
import it.ibm.red.web.document.mbean.component.VerificaFirmaComponent;
import it.ibm.red.web.dto.ListElementDTO;
import it.ibm.red.web.mbean.interfaces.IDocumentManagerDataTabDocumenti;

/**
 * Gestisce la visualizzazione delle versioni del dettaglio esteso del documento
 * foglia Documenti.
 */
public class DocumentManagerDettaglioVersioniAllegatiComponent extends VerificaFirmaComponent implements IDocumentManagerDataTabDocumenti {
	/**
	 * Seriale per serializzazione.
	 */
	private static final long serialVersionUID = -3300328616542190058L;

	/**
	 * Logger del componente.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DocumentManagerDettaglioVersioniAllegatiComponent.class);

	/**
	 * Utente che possiede la sessione.
	 */
	private UtenteDTO utente;

	/**
	 * Services del document manager.
	 */
	private IDocumentManagerFacadeSRV documentManagerSRV;

	/**
	 * DTO delle informazioni del documentManager.
	 */
	private DetailDocumentRedDTO detail;

	/**
	 * Allegati.
	 */
	private List<ListElementDTO<AllegatoDTO>> allegati;

	/**
	 * Costruttore.
	 */
	public DocumentManagerDettaglioVersioniAllegatiComponent(final UtenteDTO inUtente) {
		try {
			utente = inUtente;
			documentManagerSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentManagerFacadeSRV.class);
			allegati = new ArrayList<>();
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inizializzazione del document manager tab documenti", e);
			showErrorMessage("Non sono riuscito a inizializzare il tab documenti");
		}
	}

	/**
	 * Metodo utile a inizializzare il detail.
	 * 
	 * Deve essere chiamato dentro un altro metodo.
	 * 
	 * @param inDetail
	 */
	public void setDetail(final DetailDocumentRedDTO inDetail) {
		unsetDetail();
		detail = inDetail;
		refresh();
	}

	/**
	 * Metodo di utility che permette il refresh dell'albero del component.
	 * 
	 * Deve essere richaiamato da un altro metodo non può essere richiamato
	 * dall'xhtml.
	 */
	public void refresh() {

		allegati.clear();
		if (CollectionUtils.isNotEmpty(detail.getAllegati())) {
			int id = 1;
			for (final AllegatoDTO allegato : detail.getAllegati()) {
				final ListElementDTO<AllegatoDTO> element = new ListElementDTO<>(id, allegato);
				allegati.add(element);
				id++;
			}
		}
	}

	/**
	 * Restituisce lo streamed content per il download della versione del documento
	 * allegato. La versione è specifica dal parametro di tipo:
	 * <code> VersionDTO </code>.
	 * 
	 * @param v
	 *            versione documento allegato
	 * @return streamed content
	 */
	@Override
	public StreamedContent downloadVersioneDocumentoPrincipale(final VersionDTO v) {
		return downloadVersioneDocumentoAllegato(v);
	}

	/**
	 * Scarica il contenuto della vesione selezionata.
	 * 
	 * @param v Versione che si vuole scaricare.
	 * 
	 * @return Ritorna l'oggetto primefaces per gestire lo scaricamento del file
	 *         versionato.
	 * 
	 */
	@Override
	public StreamedContent downloadVersioneDocumentoAllegato(final VersionDTO v) {
		StreamedContent file = null;

		try {
			if (v == null) {
				LOGGER.error("Dettaglio esteso foglia documenti, la versione che si desidera scaricare e' null");
				throw new RedException("file non trovato");
			}

			final InputStream stream = documentManagerSRV.getStreamDocumentoByGuid(v.getGuid(), utente);

			file = new DefaultStreamedContent(stream, v.getMimeType(), v.getFileName());

		} catch (final Exception e) {
			if (v != null) {
				LOGGER.error("Impossibile recuperare il content della versione con guid : " + v.getGuid(), e);
			} else {
				LOGGER.error("Impossibile recuperare il content della versione", e);
			}
			showErrorMessage("Errore durante l'operazione di recupero della versione");
		}

		return file;
	}

	/**
	 * Aggiorna il metadato verifica firma della versione identificata dal
	 * parametro.
	 * 
	 * @param v
	 *            versione da aggiornare
	 */
	@Override
	public void aggiornaVerificaFirma(final VersionDTO v) {
		try {
			aggiornaVerificaFirma(v, detail.getDocumentTitle());
		} catch (final Exception e) {
			LOGGER.error(e);
			showErrorMessage("Si è verificato un errore durante la verifica della firma.");
		}

	}

	/**
	 * Aggiorna la firma per l'allegato della versione corrispondente.
	 * 
	 * @param versioneVerificaFirma
	 */
	@Override
	public void aggiornaVerificaFirmaAllegato(final VersionDTO versioneVerificaFirma) {
		try {
			final String vGuid = versioneVerificaFirma.getGuid();
			// Recupero l'allegato corrispondente che ha la versione selezionata.
			String inDocumentTitleInfoFirma = null;
			if (CollectionUtils.isEmpty(detail.getAllegati())) {
				final RedException red = new RedException("Gli allegati sono null or empty");
				LOGGER.error("dettaglio esteso tab documenti disincronizzato: sono stati modificati gli allegati del documento dopo aver inizializzato il Tab documenti", red);
				throw red;
			}

			for (final AllegatoDTO currentAllegato : detail.getAllegati()) {

				final List<VersionDTO> versioni = currentAllegato.getVersions();

				if (CollectionUtils.isEmpty(versioni)) {
					final RedException redVersioneNulla = new RedException("le versioni dell'allegato sono null or empty");
					LOGGER.error("dettaglio esteso tab documenti disincronizzato: uno degli allegati del documento non ha la verisone", redVersioneNulla);
					throw redVersioneNulla;
				}
				if (containVersionGuid(versioni, vGuid)) {
					inDocumentTitleInfoFirma = currentAllegato.getDocumentTitle();
					break;
				}
			}

			aggiornaVerificaFirma(versioneVerificaFirma, inDocumentTitleInfoFirma);

		} catch (final Exception e) {
			LOGGER.error(e);
			showErrorMessage("Si è verificato un errore durante la verifica della firma dell'allegato.");
		}
	}

	/**
	 * Restituisce true se la lista delle versioni contiene la versione associata al
	 * guid.
	 * 
	 * @param versioni
	 * @param vGuid
	 * @return true se la lista contiene il guid, false altrimenti
	 */
	public boolean containVersionGuid(final List<VersionDTO> versioni, final String vGuid) {
		return versioni.stream().anyMatch(v -> v.getGuid().equals(vGuid));
	}

	/**
	 * Aggiorna verifica firma per il documento identificato da:
	 * inDocumentTitleInfoFirma.
	 * 
	 * @param versioneVerificaFirma
	 * @param inDocumentTitleInfoFirma
	 */
	public void aggiornaVerificaFirma(final VersionDTO versioneVerificaFirma, final String inDocumentTitleInfoFirma) {
		if (inDocumentTitleInfoFirma == null) {
			throw new RedException("Impossibile individuare il documentTitle della versione corrente per la versione con GUID : " + versioneVerificaFirma.getGuid());
		} else {
			super.aggiornaVerificaFirma(versioneVerificaFirma.getGuid(), inDocumentTitleInfoFirma, utente);
			versioneVerificaFirma.setValoreVerificaFirma(infoFirma.getMetadatoValiditaFirma());
		}
	}

	/**
	 * Annulla le informazioni relative al detail.
	 */
	public void unsetDetail() {
		this.detail = null;
		infoFirma = null;
		documentTitleInfoFirma = null;
	}

	/**
	 * Restituisce la lista delle versioni.
	 * 
	 * @return versioni
	 */
	@Override
	public List<VersionDTO> getVersioni() {
		return detail != null ? detail.getVersioni() : null;
	}

	/**
	 * Restituisce il nome del file.
	 * @return nome file
	 */
	@Override
	public String getNomeFile() {
		return detail == null ? null : detail.getNomeFile();
	}

	/**
	 * Restituisce la lista degli allegati.
	 * @return lista allegati
	 */
	@Override
	public List<ListElementDTO<AllegatoDTO>> getAllegati() {
		return allegati;
	}

}
