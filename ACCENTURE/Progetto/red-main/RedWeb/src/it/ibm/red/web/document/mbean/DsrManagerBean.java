package it.ibm.red.web.document.mbean;

import java.util.Collection;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.HierarchicalFileWrapperDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV;
import it.ibm.red.business.service.facade.IProtocollaDsrFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.mbean.component.DsrPresentazioneBean;
import it.ibm.red.web.document.mbean.interfaces.IDsrTabGenerale;
import it.ibm.red.web.document.mbean.interfaces.ITabAllegatiBean;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.SessionBean;
import it.ibm.red.web.servlets.DownloadContentServlet;
import it.ibm.red.web.servlets.DownloadContentServlet.DocumentTypeEnum;

/**
 * Dsr sta per Dichiarazione Servizi Resi.
 */
@Named(ConstantsWeb.MBean.DSR_MANAGER_BEAN)
@ViewScoped
public class DsrManagerBean extends AbstractBean {

	/*
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -2193617699660215803L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DsrManagerBean.class);

	/**
	 * Service.
	 */
	private IProtocollaDsrFacadeSRV protocollazioneFepaSRV;

	/**
	 * Dettaglio documento.
	 */
	private DetailDocumentRedDTO doc;

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Questo bean è valorizzato inizialmente nel MailBean con questi valori.
	 * 
	 * Allegati (No content) nomeFile mimetype formatoELETTRONICO FILE PRINCIPALE
	 * nomeFile principale mimetype content anche questo potrebbe essere ricavato
	 * PROTOCOLLAZIONE protocollazioneMailGuid mittenteContatto
	 *
	 * // isNotifica // idCategoriaDocumento
	 */
	private DetailDocumentRedDTO detail;

	/**
	 * Tab generale.
	 */
	private IDsrTabGenerale tabGenerale;

	/**
	 * Tab fascicoli.
	 */
	private DsrDataTabFascicoliBean tabFascicoli;

	/**
	 * Tab allegati.
	 */
	private ITabAllegatiBean tabAllegati;

	/**
	 * Presentazione.
	 */
	private DsrPresentazioneBean presentazione;

	/**
	 * Messaggio esito registrazione.
	 */
	private Map<String, String> messaggioEsitoRegistra;

	/**
	 * Session bean.
	 */
	private SessionBean sessionBean;

	/**
	 * Esegue la logica nel post construct del bean.
	 */
	@Override
	@PostConstruct
	public void postConstruct() {
		sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();

		protocollazioneFepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IProtocollaDsrFacadeSRV.class);
	}

	/**
	 * Inizializza il dettaglio dalla mail.
	 * 
	 * @param utente
	 * @param contact
	 * @param selectedAllegati
	 * @param principale
	 * @param notificaProtocolla
	 * @param mailGUID
	 * @param mailOggetto
	 */
	public void initialzeDetailFromMail(final UtenteDTO utente, final Contatto contact, final Collection<HierarchicalFileWrapperDTO> selectedAllegati,
			final HierarchicalFileWrapperDTO principale, final Boolean notificaProtocolla, final String mailGUID, final String mailOggetto) {
		final DetailDocumentRedDTO inDetail = protocollazioneFepaSRV.createDetailFromMail(utente, contact, selectedAllegati, principale, notificaProtocolla, mailGUID,
				mailOggetto);
		setDetail(inDetail);
		initializeManagereBean();

		/**
		 * Imposto gli allegati perché abbiano un default che renderizza i pulsanti
		 * piuttosto che nasconderli
		 */
		getTabAllegati().getDocumentManagerDTO().setInModificaIngresso(false); // viene gestito al contrario dal predisponi dichiarazione
		getTabAllegati().getDocumentManagerDTO().setAllegatiModificabili(true);
	}

	/**
	 * Al momento dell'apertura della maschera setta il dettaglio da mostrare.
	 * 
	 * @param detail
	 */
	public void initializeManagereBean() {
		tabGenerale = new DsrDataTabGeneraleBean(detail, utente);
		tabFascicoli = new DsrDataTabFascicoliBean(detail);
		tabAllegati = new ProtocollaDsrDataTabAllegatiBean(detail, utente);
		presentazione = new DsrPresentazioneBean(detail, false);
	}

	/**
	 * Restituisce il crypto ID.
	 * 
	 * @return cryptoId
	 */
	public final String getCryptoId() {
		if (detail == null) {
			return "";
		}
		return getCryptoMailById(detail.getIdFilePrincipaleDaProtocolla());
	}

	private String getCryptoMailById(final String id) {
		try {
			return DownloadContentServlet.getCryptoDocParamById(id, DocumentTypeEnum.PROTOCOLLA_MAIL, utente, detail.getNomeFile(), detail.getMimeType());
		} catch (final Exception e) {
			LOGGER.error("Errore nell'encrypt dei parametri per la servlet 'DownloadContentServlet':", e);
			showError("Errore nell'encrypt dei parametri per la servlet 'DownloadContentServlet': " + e.getMessage());
		}
		return "ERROREDICODIFICA";
	}

	/**
	 * Gestisce la protocollazione mail dsr ed eventuali errori.
	 */
	public void registra() {
		try {
			/** OGGETTO START **************************************/
			if (StringUtils.isNullOrEmpty(this.detail.getOggetto()) || this.detail.getOggetto().length() < 4) {
				showWarnMessage("L'oggetto deve contenere almeno 4 caratteri.");
				return;
			}
			final EsitoSalvaDocumentoDTO esito = protocollazioneFepaSRV.protocollaMailDsr(utente, detail);
			if (esito.isEsitoOk()) {
				final IDocumentoRedFacadeSRV documentoRedSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoRedFacadeSRV.class);

				setDoc(documentoRedSRV.getDocumentDetail(esito.getDocumentTitle(), utente, this.detail.getWobNumberSelected(), null));

				FacesHelper.executeJS("PF('protocollaMail_WV').hide()");
				FacesHelper.update("eastSectionForm:idDlgEsitoRegistra_DERProtocolla");
				FacesHelper.executeJS("PF('wdgEsitoRegistra_DERProtocolla').show()");
				sessionBean.createSubMenuMail();
				FacesHelper.update("westSectionForm:idTabMain_LeftAside:idMenuLeftAside:idMenuMail");
				FacesHelper.update("eastSectionForm");
				FacesHelper.update("centralSectionForm");
			} else {
				showError(esito.getNote());
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la protocollazione della Dichiarazione Servizi Resi", e);
			showError(e);
		}
	}

	/**
	 * Restituisce il tab generale.
	 * 
	 * @return tab generale
	 */
	public IDsrTabGenerale getTabGenerale() {
		return tabGenerale;
	}

	/**
	 * Imposta il tab generale.
	 * 
	 * @param tabGenerale
	 */
	public void setTabGenerale(final IDsrTabGenerale tabGenerale) {
		this.tabGenerale = tabGenerale;
	}

	/**
	 * Restituisce il bean per la gestione del tab dei fascicoli.
	 * 
	 * @return bean tab fascicoli
	 */
	public DsrDataTabFascicoliBean getTabFascicoli() {
		return tabFascicoli;
	}

	/**
	 * Imposta il bean per la gestione del tab dei fascicoli.
	 * 
	 * @param tabFascicoli
	 */
	public void setTabFascicoli(final DsrDataTabFascicoliBean tabFascicoli) {
		this.tabFascicoli = tabFascicoli;
	}

	/**
	 * Restituisce il bean che gestisce il tab degli allegati.
	 * 
	 * @return bean tab allegati
	 */
	public ITabAllegatiBean getTabAllegati() {
		return tabAllegati;
	}

	/**
	 * Imposta il bean per la gestione del tab degli allegati.
	 * 
	 * @param tabAllegati
	 */
	public void setTabAllegati(final ITabAllegatiBean tabAllegati) {
		this.tabAllegati = tabAllegati;
	}

	/**
	 * Restituisce il bean per la gestione della presentazione DSR.
	 * 
	 * @return bean presentazione DSR
	 */
	public DsrPresentazioneBean getPresentazione() {
		return presentazione;
	}

	/**
	 * Imposta il bean per la presentazione DSR.
	 * 
	 * @param presentazione
	 */
	public void setPresentazione(final DsrPresentazioneBean presentazione) {
		this.presentazione = presentazione;
	}

	/**
	 * Restituisce il dettaglio.
	 * 
	 * @return detail
	 */
	public DetailDocumentRedDTO getDetail() {
		return detail;
	}

	/**
	 * Imposta il dettaglio.
	 * 
	 * @param detail
	 */
	public void setDetail(final DetailDocumentRedDTO detail) {
		this.detail = detail;
	}

	/**
	 * Restituisce la map dei messaggi che definiscono l'esito avuto in fase di
	 * predisposizione dsr.
	 * 
	 * @return messaggi esiti
	 */
	public Map<String, String> getMessaggioEsitoRegistra() {
		return messaggioEsitoRegistra;
	}

	/**
	 * Restituisce il dettaglio del documento.
	 * 
	 * @return dettaglio documento
	 */
	public DetailDocumentRedDTO getDoc() {
		return doc;
	}

	/**
	 * Imposta il dettaglio del documento.
	 * 
	 * @param doc
	 */
	public void setDoc(final DetailDocumentRedDTO doc) {
		this.doc = doc;
	}

}
