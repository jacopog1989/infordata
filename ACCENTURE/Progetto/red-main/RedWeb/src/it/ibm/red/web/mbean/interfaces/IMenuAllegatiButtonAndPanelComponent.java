package it.ibm.red.web.mbean.interfaces;

import org.primefaces.model.menu.MenuModel;

/**
 * Apre un pannello con il dettaglio degli allegati selezionabili
 * 
 * @author a.difolca
 *
 */
public interface IMenuAllegatiButtonAndPanelComponent extends IDettaglioLoadableAfterPreview {
	
	/**
	 * Ottiene il model che definisce il menu degli allegati.
	 * @return model del menu
	 */
	MenuModel getMenuAllegatiDetailModel();
}
