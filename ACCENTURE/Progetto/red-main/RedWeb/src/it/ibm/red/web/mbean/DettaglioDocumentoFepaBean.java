package it.ibm.red.web.mbean;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.commons.collections4.CollectionUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.AllegatoOPDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DetailFascicoloRedDTO;
import it.ibm.red.business.dto.DetailFatturaFepaDTO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.DocumentoFascicoloFepaDTO;
import it.ibm.red.business.dto.DocumentoRedFascicoloFepaDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.OrdineDiPagamentoDTO;
import it.ibm.red.business.dto.ResponsesDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.timeline.TimelineHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.facade.IAllegatoFacadeSRV;
import it.ibm.red.business.service.facade.IDetailFascicoloFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentManagerFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV;
import it.ibm.red.business.service.facade.IFepaFacadeSRV;
import it.ibm.red.business.service.facade.IResponseRedFacadeSRV;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.constants.ConstantsWeb.MBean;
import it.ibm.red.web.document.mbean.DettaglioDichiarazioneBean;
import it.ibm.red.web.document.mbean.DocumentManagerBean;
import it.ibm.red.web.document.mbean.component.DetailDocumetRedFepaComponent;
import it.ibm.red.web.document.mbean.component.DettaglioDocumentoStoricoComponent;
import it.ibm.red.web.document.mbean.component.DsrPresentazioneBean;
import it.ibm.red.web.dto.GoToCodaDTO;
import it.ibm.red.web.enums.DettaglioDocumentoFileEnum;
import it.ibm.red.web.enums.NavigationTokenEnum;
import it.ibm.red.web.helper.faces.FacesHelper;
import it.ibm.red.web.mbean.component.DetailPreviewIntroComponent;
import it.ibm.red.web.mbean.component.DocumentManagerDettaglioVersioniAllegatiComponent;
import it.ibm.red.web.mbean.interfaces.IDocumentManagerDataTabDocumenti;
import it.ibm.red.web.mbean.interfaces.IFascicoloProcedimentaleManagerInitalizer;
import it.ibm.red.web.mbean.interfaces.IGoToCodaComponent;
import it.ibm.red.web.mbean.interfaces.IMenuAllegatiButtonAndPanelComponent;
import it.ibm.red.web.ricerca.mbean.RicercaDocumentiFepaBean;

/**
 * Bean dettaglio documento FEPA.
 */
@Named(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_FEPA_BEAN)
@ViewScoped
public class DettaglioDocumentoFepaBean extends DettaglioDocumentoAbstractBean
		implements IGoToCodaComponent, IMenuAllegatiButtonAndPanelComponent, IFascicoloProcedimentaleManagerInitalizer {

	private static final String ERRORE_NEL_RECUPERO_DEL_DOCUMENTO = "Errore nel recupero del documento: ";

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 7471338223007987743L;

	/**
	 * Messaggio generico di errore operazione. Occorre definire l'operazione che ha
	 * generato l'errore a valle del messaggio.
	 */
	private static final String GENERIC_ERROR_OPERAZIONE_MSG = "Errore durante l'operazione: ";

	/**
	 * Messaggio di errore caricamento dettaglio.
	 */
	private static final String ERROR_CARICAMENTO_DETTAGLIO_MSG = "Errore nella fase di caricamento del dettaglio.";

	/**
	 * Messaggio errore recupero documento. Occorre appendere il document title del
	 * documento al messaggio.
	 */
	private static final String ERROR_RECUPERO_DOCUMENTO_MSG = "Impossibile recuperare il documento con DocumentTile=";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DettaglioDocumentoFepaBean.class.getName());

	/**
	 * Items string.
	 */
	protected static final String ITEMS = "{items:";

	/**
	 * Dettaglio.
	 */
	protected DetailDocumentRedDTO detail;

	/**
	 * Dettaglio fattura fepa.
	 */
	private DetailFatturaFepaDTO detailFatturaFepa;

	/**
	 * Service.
	 */
	private IFepaFacadeSRV fepaSRV;

	/**
	 * Service.
	 */
	private IDetailFascicoloFacadeSRV detailFascicoloSRV;

	/**
	 * Json per timeline.
	 */
	protected String jsonTimeLine;

	/**
	 * Flag fascicolo visibile.
	 */
	private boolean dialogFascicoloVisible = false;

	/**
	 * Flag fatture visibile.
	 */
	private boolean dialogFascicoloFatturaVisible;

	/**
	 * Lista fatture.
	 */
	private List<DocumentoRedFascicoloFepaDTO> fatture;

	/**
	 * Lista ordini di pagamento.
	 */
	private List<OrdineDiPagamentoDTO> ordiniDiPagamento;

	/**
	 * Lista allegati op.
	 */
	private List<AllegatoOPDTO> listaAllegatiOP;

	/**
	 * Flag permesso rimozione.
	 */
	private boolean permessoAddRemoveDoc;

	/**
	 * Flag permesso op.
	 */
	private boolean permessoAssociareOpDoc;

	/**
	 * Nuovo allegato op.
	 */
	private AllegatoOPDTO newAllegatoOP;

	/**
	 * Flag popip documento.
	 */
	private boolean popupDocumentDSRFepaVisible;

	/**
	 * Guid allegato.
	 */
	private String guidAllegato;

	/**
	 * Flag documenti OP.
	 */
	private List<DocumentoFascicoloFepaDTO> documentiOPFascicoloFepa;

	/**
	 * Dettaglio fascicolo.
	 */
	private DetailFascicoloRedDTO detailFascicolo;

	/**
	 * Flag df visibile.
	 */
	private boolean dlgFascOpFepaDfVisible;

	/**
	 * Flag opp visibile.
	 */
	private boolean idDlgAggAllegatoOpDfVisible;

	/**
	 * Flag dialòog fepa visibile.
	 */
	private boolean dlgFepaOpDfVisible;

	/**
	 * Flag east button.
	 */
	private boolean showEastButtons = false;

	/**
	 * Componente fepa.
	 */
	private DetailDocumetRedFepaComponent fepaCmp;

	/**
	 * Componente visualizza dettaglio.
	 */
	private DetailPreviewIntroComponent fepaPreviewComponent;

	/**
	 * Componenete visualizza preview.
	 */
	private DetailPreviewIntroComponent fatturePreviewComponent;

	/**
	 * Compoennte visualizza dettaglio.
	 */
	private VisualizzaDettaglioDocumentoComponent viewDettaglioDoc;

	/**
	 * Flag dettaglio aoo.
	 */
	private boolean dettaglioDD;

	/**
	 * Lista tipologiae.
	 */
	private List<TipologiaDocumentoDTO> tipologieDocAggiuntivi;

	/**
	 * Tipologia selezionata.
	 */
	private String idTipologiaDocAggiuntivoSelezionato;

	/**
	 * Component per la visualizzazione delle verisoni del documento.
	 * 
	 * Viene usato solo dalle DSR e solo per quelle in entrata.
	 * 
	 */
	private DocumentManagerDettaglioVersioniAllegatiComponent versioniAllegato;

	/**
	 * Nome file.
	 */
	private String nomeFile;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		super.postConstruct();
		final SessionBean sessionBean = FacesHelper.getManagedBean(Constants.ManagedBeanName.SESSION_BEAN);
		utente = sessionBean.getUtente();

		viewDettaglioDoc = new VisualizzaDettaglioDocumentoComponent(utente);

		updateEnabled = false;

		detailFascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IDetailFascicoloFacadeSRV.class);
		documentoRedSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoRedFacadeSRV.class);
		documentManagerSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentManagerFacadeSRV.class);
		responseRedSRV = ApplicationContextProvider.getApplicationContext().getBean(IResponseRedFacadeSRV.class);

		fepaCmp = new DetailDocumetRedFepaComponent(utente);
		super.setDocumentCmp(fepaCmp);

		versioniAllegato = new DocumentManagerDettaglioVersioniAllegatiComponent(utente);

		// Inizializzazione del tab storico potrebbero essere necessarie ulteriori
		// iniziializzazioni qualora la stessa istanza venga
		// Richiamata in più punti mi aspetto che in ogni punto questa classe venga
		// estesa
		if (hasTabStorico()) {
			storico = new DettaglioDocumentoStoricoComponent(getClass().getSimpleName());
		}

	}

	/**
	 * Apre la dialog di gestione del documento.
	 */
	@Override
	public void openDocumentPopup() {
		try {

			loadAfterIntroPreview();

			final DocumentManagerBean bean = FacesHelper.getManagedBean(Constants.ManagedBeanName.DOCUMENT_MANAGER_BEAN);
			bean.setChiamante(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_FEPA_BEAN);
			bean.setDetail(this.detail);

			setPopupDocumentVisible(true);
			FacesHelper.executeJS("PF('dlgManageDocument').show()");
			FacesHelper.executeJS("PF('dlgManageDocument').toggleMaximize()");
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Errore durante l'operazione di recupero del documento.");
		}
	}

	/**
	 * Apre la dialog di gestione del documento DSR.
	 */
	public void openDocumentPopupDSRPredisposta() {
		try {
			loadAfterIntroPreview();
			final ListaDocumentiBean listaBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.LISTA_DOCUMENTI_BEAN);
			final boolean isinEdit = !DettaglioDocumentoFileEnum.FEPADSR.equals(listaBean.getDettaglioDaCaricareCodaCorrente());
			final boolean canRegisterMittente = isinEdit && !sessionBean.getActivePageInfo().equals(NavigationTokenEnum.LIBROFIRMA);
			final DettaglioDichiarazioneBean bean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DICHIARAZIONE);
			bean.popolaDettaglio(this.detail.getDocumentTitle(), this.detail.getWobNumberSelected(), canRegisterMittente, isinEdit);

			final DsrPresentazioneBean presBean = (DsrPresentazioneBean) bean.getPresentazione();
			presBean.setCanRegisterMittente(canRegisterMittente);
			presBean.setCanRegisterOggetto(isinEdit);
			setPopupDocumentDSRFepaVisible(true);

			FacesHelper.executeJS("PF('dett_DSR_FEPA').show();");
			FacesHelper.executeJS("PF('dett_DSR_FEPA').toggleMaximize();");
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Errore durante l'operazione di recupero del documento.");
		}
	}

	/**
	 * Mostra il dettaglio del documento se non documento INTERNO.
	 */
	public void openVisualizzaDocumento() {

		if (CategoriaDocumentoEnum.INTERNO.getIds()[0].equals(documentCmp.getDetail().getIdCategoriaDocumento())) {
			showWarnMessage("Non è possibile aprire il dettaglio di un documento interno.");
			return;
		}

		setDetail();
		viewDettaglioDoc.setDetail(this.documentCmp.getDetail());
	}

	/**
	 * Si occupa della chiusura della dialog per la gestione un documento DSR nel
	 * caso in cui non viene effettuata alcuna modifica.
	 */
	public void closePopupDSRPredispostaNoEdit() {
		final DettaglioDichiarazioneBean bean = FacesHelper.getManagedBean(ConstantsWeb.MBean.DETTAGLIO_DICHIARAZIONE);
		bean.close();
		setPopupDocumentDSRFepaVisible(false);
		// aggiornamento dati per la UI
		final RicercaDocumentiFepaBean rdfb = FacesHelper.getManagedBean(ConstantsWeb.MBean.RICERCA_DOCUMENTI_FEPA_BEAN);
		if (rdfb.getDocumentiFepaDTH() != null && rdfb.getDocumentiFepaDTH().getCurrentMaster() != null) {
			rdfb.aggiorna();
		}

	}

	/**
	 * Imposta il dettaglio dopo la preview.
	 * 
	 * @param documentTitle
	 *            document title documento
	 * @param wobNumber
	 *            wobnumber documento
	 * @param documentClassName
	 *            classe documentale documento
	 * @param dettaglioDocEnum
	 * 			@see DettaglioDocumentoFileEnum
	 */
	@Override
	public void setDetailAfterPreview(final String documentTitle, final String wobNumber, final String documentClassName, final DettaglioDocumentoFileEnum dettaglioDocEnum) {
		if (DettaglioDocumentoFileEnum.FEPAFATTURA.equals(dettaglioDocEnum)) {
			setDetailFattura(documentTitle, wobNumber, documentClassName);
		} else if (DettaglioDocumentoFileEnum.FEPADD.equals(dettaglioDocEnum)) {
			setDetailDD(documentTitle, wobNumber, documentClassName);

		} else {
			setDetailAfterPreview(documentTitle, wobNumber, documentClassName);
		}
	}

	/**
	 * Imposta il dettaglio dopo la preview.
	 * 
	 * @param documentTitle
	 *            document title documento
	 * @param wobNumber
	 *            wobnumber documento
	 * @param documentClassName
	 *            classe documentale documento
	 */
	@Override
	public void setDetailAfterPreview(final String documentTitle, final String wobNumber, final String documentClassName) {
		try {
			dettaglioDD = false;
			detail = documentoRedSRV.getDocumentDetail(documentTitle, utente, wobNumber, documentClassName);

			this.fepaCmp.setDetail(detail);
			this.fepaCmp.setChiamante(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_FEPA_BEAN);

			if (detail == null) {
				LOGGER.error(ERROR_RECUPERO_DOCUMENTO_MSG + documentTitle);
				// mostrare una popup di errore caricamento
				throw new RedException(ERROR_CARICAMENTO_DETTAGLIO_MSG);
			}

			menuAllegatiDetailModel = createMenuAllegatiModel();
			documentTitleSelected = detail.getDocumentTitle(); // di default imposto quello del documento principale
			mimeTypeSelected = detail.getMimeType();
			nomeFileSelected = detail.getNomeFile();

			modificabile = documentoRedSRV.isDocumentoModificabile(detail, utente);

			initializeStoricoAndGoToCoda();

			if (hasTabStorico()) {
				// Questo serve nel caso in cui apri la pagina e visualizzi immediatamente il
				// tab dello storico
				// Attualmente con l'intro preview lo storico non viene caricato immediatamente
				// con il detail e quindi non esiste nessun javascript
				FacesHelper.executeJS(storico.getJavascriptStandalone());
			}

			// Inizializzo il copomente per la visualizzazione delle verisioni della
			// dichiarazione.
			versioniAllegato.setDetail(detail);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ERRORE_NEL_RECUPERO_DEL_DOCUMENTO + e.getMessage());
		}

	}

	/**
	 * Inizializza lo storico e redireziona sulla coda.
	 */
	private void initializeStoricoAndGoToCoda() {
		if (hasTabStorico()) {
			onChangeDetailStorico(fepaCmp.getDetail());
		}
		initializeGoToCoda();
	}

	/**
	 * Ricalcola le code.
	 */
	private void initializeGoToCoda() {
		// GEstione del bottone delle code
		if (sessionBean.isNotActivePageCoda()) {
			calculateQueues();
		}
	}

	/**
	 * Imposta il dettaglio della Fattura.
	 * 
	 * @param documentTitle
	 *            document title documento
	 * @param wobNumber
	 *            wobnumber documento
	 * @param documentClassName
	 *            classe documentale del documento
	 */
	public void setDetailFattura(final String documentTitle, final String wobNumber, final String documentClassName) {
		try {
			detailFatturaFepa = documentoRedSRV.getFatturaDetail(documentTitle, utente, wobNumber, documentClassName);
			if (detailFatturaFepa == null) {
				LOGGER.error(ERROR_RECUPERO_DOCUMENTO_MSG + documentTitle);
				// mostrare una popup di errore caricamento
				throw new RedException(ERROR_CARICAMENTO_DETTAGLIO_MSG);
			}
			
			detailFatturaFepa.setFascicoli(removeFascicoliNonProcedimentali(detailFatturaFepa.getFascicoli(), detailFatturaFepa.getIdFascicoloProcedimentale()));

			menuAllegatiDetailModel = createMenuAllegatiModelFattura();
			documentTitleSelected = detailFatturaFepa.getDocumentTitle(); // di default imposto quello del documento principale
			mimeTypeSelected = detailFatturaFepa.getMimeType();
			nomeFileSelected = detailFatturaFepa.getNomeFile();

			modificabile = false;

			getDocumentCmp().setDetail(detailFatturaFepa);
			initializeGoToCoda();

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ERRORE_NEL_RECUPERO_DEL_DOCUMENTO + e.getMessage());
		}

	}

	/**
	 * Imposta il dettaglio del Decreto Dirigenziale.
	 * 
	 * @param documentTitle
	 *            document title del decreto dirigenziale
	 * @param wobNumber
	 *            wobnumber documento
	 * @param documentClassName
	 *            classe documentale documento
	 */
	public void setDetailDD(final String documentTitle, final String wobNumber, final String documentClassName) {
		try {

			final SessionBean sessionBean = FacesHelper.getManagedBean(Constants.ManagedBeanName.SESSION_BEAN);
			fatture = new ArrayList<>();
			detail = documentoRedSRV.getDocumentDetail(documentTitle, utente, wobNumber, documentClassName);
			
			if (detail == null) {
				LOGGER.error(ERROR_RECUPERO_DOCUMENTO_MSG + documentTitle);
				// mostrare una popup di errore caricamento
				throw new RedException(ERROR_CARICAMENTO_DETTAGLIO_MSG);
			}
			
			detail.setFascicoli(removeFascicoliNonProcedimentali(detail.getFascicoli(), detail.getIdFascicoloProcedimentale()));
			this.fepaCmp.setDetail(detail);
			this.fepaCmp.setChiamante(ConstantsWeb.MBean.DETTAGLIO_DOCUMENTO_FEPA_BEAN);

			final List<DetailFatturaFepaDTO> documentiFascicolo = detailFascicoloSRV.getFattureFepaByIdFascicolo(Integer.parseInt(detail.getIdFascicoloProcedimentale()),
					utente.getIdAoo(), utente);
			documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoFacadeSRV.class);

			for (final DetailFatturaFepaDTO doc : documentiFascicolo) {
				final DocumentoRedFascicoloFepaDTO fattura = new DocumentoRedFascicoloFepaDTO();
				fattura.setIdFascicoloFepa(doc.getIdFascicoloFepa());
				fattura.setNomeFascicoloDocumentoRed(doc.getDescrizioneFascicoloProcedimentale());
				fatture.add(fattura);
			}

			menuAllegatiDetailModel = createMenuAllegatiModel();
			documentTitleSelected = detail.getDocumentTitle(); // di default imposto quello del documento principale
			mimeTypeSelected = detail.getMimeType();
			nomeFileSelected = detail.getNomeFile();

			modificabile = documentoRedSRV.isDocumentoModificabile(detail, utente);

			if (PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.PERMESSO_DIRIGENTE_FEPA)
					|| sessionBean.getActivePageInfo().equals(NavigationTokenEnum.DD_DA_LAVORARE)) {
				setPermessoAddRemoveDoc(true);
				setPermessoAssociareOpDoc(true);
			} else {
				setPermessoAddRemoveDoc(false);
				setPermessoAssociareOpDoc(false);
			}

			restituisciOrdiniDiPagamento(documentTitle, detail.getGuid());
			dettaglioDD = true;

			initializeStoricoAndGoToCoda();
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(ERRORE_NEL_RECUPERO_DEL_DOCUMENTO + e.getMessage());
		}

	}

	/**
	 * Rimuove i fascicoli non procedimentali.
	 * 
	 * @param listFascicoli
	 *            lista dei fascicoli
	 * @param idFascicoloProcedimentale
	 *            id del fascicolo procedimentale da cui rimuovere i
	 *            fascicoli non appartenenti ad essi
	 * @return lista dei fascicoli coerenti col fascicolo procedimentale
	 */
	private static List<FascicoloDTO> removeFascicoliNonProcedimentali(final List<FascicoloDTO> listFascicoli, final String idFascicoloProcedimentale) {
		final List<FascicoloDTO> newList = new ArrayList<>();
		final int sizeLista = listFascicoli.size();
		for (int x = 0; x < sizeLista; x++) {
			if (listFascicoli.get(x).getIdFascicolo().equals(idFascicoloProcedimentale)) {
				newList.add(listFascicoli.get(x));
				break;
			}

		}

		return newList;
	}

	/**
	 * Aggiorna il dettaglio della Fattura FEPA e mostra la dialog per la gestione
	 * della stessa.
	 */
	public void loadDettaglioFatturaFepa() {
		// In questo modo li carico una volta sola
		fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);
		try {
			if (detailFatturaFepa.getDocumentiFepa() == null || detailFatturaFepa.getDocumentiFepa().isEmpty()) {
				detailFatturaFepa.setDocumentiFepa(fepaSRV.getFascicoliFattura(detailFatturaFepa.getIdFascicoloFepa()));
			}
			FacesHelper.executeJS("PF('dlgManageFascicolo_Fattura_FEPA').show()");

		} catch (final Exception e) {
			LOGGER.error("Errore durante l'operazione di recupero documenti allegati al fascicolo fattura", e);
			showError("Errore durante l'operazione di recupero documenti allegati al fascicolo fattura");
		}

	}

	/**
	 * Aggiorna il dettaglio della fattura FEPA.
	 * 
	 * @param fattura
	 *            fattura da aggiornare
	 */
	public void loadDettaglioFatturaFepa(final DocumentoRedFascicoloFepaDTO fattura) {
		// In questo modo li carico una volta sola
		try {
			if (CollectionUtils.isEmpty(fattura.getDocumentiFattura())) {
				fattura.setDocumentiFattura(fepaSRV.getFascicoliFattura(fattura.getIdFascicoloFepa()));
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			fattura.setDocumentiFattura(null);
		}
	}

	/**
	 * Restituisce lo StreamedContent del documento fascicolo fattura per
	 * effettuarne il download.
	 * 
	 * @param index
	 *            indice del fascicolo selezionato
	 * @return StreamedContent per download
	 */
	public StreamedContent downloadDocumentoFascicoloFattura(final Object index) {
		StreamedContent file = null;
		try {
			final Integer i = (Integer) index;

			final DocumentoFascicoloFepaDTO documentoSelezionato = detailFatturaFepa.getDocumentiFepa().get(i);

			final FileDTO doc = fepaSRV.downloadDocumentoFascicoloFattura(detailFatturaFepa.getIdFascicoloFepa(), documentoSelezionato.getGuid());

			final ByteArrayInputStream bis = new ByteArrayInputStream(doc.getContent());

			file = new DefaultStreamedContent(bis, doc.getMimeType(), doc.getFileName());

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());
		}
		return file;

	}

	/**
	 * Restituisce lo StreamedContent del documento fascicolo fattura per
	 * effettuarne il download.
	 * 
	 * @param fattura
	 *            fattura da scaricare
	 * @param index
	 *            indice fascicolo
	 * @return StreamedContent del file da scaricare
	 */
	public StreamedContent downloadDocumentoFascicoloFattura(final DocumentoRedFascicoloFepaDTO fattura, final Object index) {
		StreamedContent file = null;
		try {
			final Integer i = (Integer) index;

			final DocumentoFascicoloFepaDTO documentoSelezionato = fattura.getDocumentiFattura().get(i);

			final FileDTO doc = fepaSRV.downloadDocumentoFascicoloFattura(fattura.getIdFascicoloFepa(), documentoSelezionato.getGuid());

			final ByteArrayInputStream bis = new ByteArrayInputStream(doc.getContent());

			file = new DefaultStreamedContent(bis, doc.getMimeType(), doc.getFileName());

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());
		}
		return file;

	}

	/**
	 * Imposta le response valide per il documento.
	 * 
	 * @param m
	 *            master selezionato
	 */
	@Override
	public void setResponse(final MasterDocumentRedDTO m) {
		responseDetail = new ArrayList<>();

//		<-- Recupero response Filenet e Applicative -->
		if (m.getResponsesRaw() != null && m.getResponsesRaw().length > 0) {
			final ResponsesDTO respDto = responseRedSRV.refineReponsesSingle(m, utente, Boolean.FALSE);
			// <-- Fine recupero -->

			// <-- Predispongo dati per view -->
			responseDetail = respDto.getResponsesEnum();
			// <-- Fine predisposizione -->

			orderResponse(responseDetail);
		}
	}

	/**
	 * Metodo utilizzato per pulire il dettaglio e le informazioni in esso
	 * contenute. Utile se dopo una l'utilizzo di una response la coda non contenga
	 * più elementi.
	 * 
	 */
	public void clearDetail() {

		detail = null;
		menuAllegatiDetailModel = null;
		documentTitleSelected = null; // di default imposto quello del documento principale
		mimeTypeSelected = null;
		nomeFileSelected = null;

	}

	/**
	 * Crea un model del subMenu degli allegati.
	 * 
	 * @return subMenu allegati
	 */
	@Override
	protected MenuModel createMenuAllegatiModel() {
		MenuModel output = new DefaultMenuModel();
		if (detail != null) {
			output = createSubMenuAllegatiModel(detail, "#{DettaglioDocumentoFepaBean.changeSelectedDocumentTitle}");
		}
		return output;
	}

	/**
	 * Crea submenu degli allegati fattura.
	 * @return submenu allegati fattura
	 */
	protected MenuModel createMenuAllegatiModelFattura() {
		MenuModel output = new DefaultMenuModel();
		if (detailFatturaFepa != null) {
			output = createSubMenuAllegatiModel(detailFatturaFepa, "#{DettaglioDocumentoFepaBean.changeSelectedDocumentTitle}");
		}
		return output;
	}

	/**
	 * Metodo per il ridisegno degli storici visuali (laptop e smartphone).
	 */
	protected void drawTimeline() {
		if (detail != null && detail.getDateCreated() != null && !detail.getDateCreated().before(new Date(Constants.Storico.START_DATE))) {
			final String descrizioneIterApprovativo = detail.getIterApprovativoSemaforo();
			final TimelineHelper tlh = new TimelineHelper(detail.getStoricoSteps(), descrizioneIterApprovativo, detail.getApprovazioni());

			final String jsonCompleto = tlh.getJSON();
			FacesHelper.executeJS("$('#timeline-container-basic-DD').timelineMe(" + ITEMS + jsonCompleto + "});");

			jsonTimeLine = ITEMS + jsonCompleto + "}";
			final String jsonApprovazioni = tlh.getApprovazioniJSON();
			FacesHelper.executeJS("$('#timeline-container-basic-approvazioni-DD').timelineMe(" + ITEMS + jsonApprovazioni + "});");
			FacesHelper.executeJS("tlHideDetail()");

			FacesHelper.update("eastSectionForm:idDettaglioDocumento_DD");

		}
	}

	/**
	 * Metodo per la realizzazione del report delle approvazioni.
	 * 
	 * @return stream contenente il report
	 */
	public StreamedContent getReportApprovazioni() {
		return getReportApprovazioniDetail(detail);
	}

	/**
	 * Abilita l'update.
	 */
	public void enableUpdate() {
		updateEnabled = true;
	}

	/**
	 * Mostra il messaggio di successo e disabilita l'update.
	 */
	public void save() {
		showInfoMessage("Modifica avvenuta con successo.");
		updateEnabled = false;
	}

	/**
	 * Mostra il messaggio di annullamento e disabilita l'update.
	 */
	public void cancel() {
		showInfoMessage("Modifica non eseguita.");
		updateEnabled = false;
	}

	/**
	 * TAB DOCUMENTI START
	 **************************************************************************************************************/
	/**
	 * Restituisce lo StreamedContent per il download del documento principale nella
	 * versione indicata dall'indice.
	 * 
	 * @param index di versione
	 * @return StreamedContent per download
	 */
	public StreamedContent downloadVersioneDocumentoPrincipale(final Object index) {
		return downloadVersioneDocumentoDettaglio(this.detail, index);
	}

	/**
	 * Restituisce lo StreamedContent per il download del documento principale
	 * Fattura nella versione indicata dall'indice.
	 * 
	 * @param index di versione
	 * @return StreamedContent per download
	 */
	public StreamedContent downloadVersioneDocumentoPrincipaleFattura(final Object index) {
		return downloadVersioneDocumentoDettaglio(this.detailFatturaFepa, index);

	}

	/**
	 * Restituisce lo StreamedContent per il download del documento allegato
	 * Fattura, nella versione indicata dall'indice, identificato dal guid.
	 * 
	 * @param index
	 * @param guid
	 * @return StreamedContent per download
	 */
	public StreamedContent downloadVersioneDocumentoAllegatoFattura(final Object index, final String guid) {
		return downloadVersioneDocumentiAllegati(detailFatturaFepa, guid, index);
	}

	/**
	 * Restituisce lo StreamedContent per il download del documento allegato, nella
	 * versione indicata dall'indice, identificato dal guid.
	 * 
	 * @param index
	 * @param guid
	 * @return StreamedContent per download
	 */
	public StreamedContent downloadVersioneDocumentoAllegato(final Object index, final String guid) {
		return downloadVersioneDocumentiAllegati(detail, guid, index);
	}

	/**
	 * Restituisce lo StreamedContent per il download del documento allegato OP
	 * identificato dal guid.
	 * 
	 * @param guid
	 * @return StreamedContent per download
	 */
	public StreamedContent downloadVersioneDocumentoAllegatoOP(final String guid) {
		StreamedContent file = null;
		try {
			AllegatoOPDTO allegatoSelected = null;
			for (final AllegatoOPDTO a : listaAllegatiOP) {
				if (a.getGuid().equals(guid)) {
					allegatoSelected = a;
					break;
				}
			}

			if (allegatoSelected == null) {
				throw new RedException("recupero allegato");
			}

			final InputStream stream = documentManagerSRV.getStreamDocumentoByGuid(guid, utente);

			file = new DefaultStreamedContent(stream, allegatoSelected.getMimeType(), allegatoSelected.getNomeDocumento());

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());
		}

		return file;
	}

	/**
	 * TAB DOCUMENTI END
	 ****************************************************************************************************************/

	/**
	 * UTILITY VARIE START
	 **********************************************************************************************************************************/

	/**
	 * Effettua un reset del dettaglio.
	 */
	@Override
	public void unsetDetail() {
		detail = null;
		documentTitleSelected = null;
		jsonTimeLine = null;
		menuAllegatiDetailModel = null;
		mimeTypeSelected = null;
		modificabile = false;
		nomeFileSelected = null;
		popupDocumentVisible = false;
		responseDetail = new ArrayList<>();
		updateEnabled = false;
		viewDettaglioDoc.unsetDetail();
		previewComponent.unsetDetail();
		versioniAllegato.unsetDetail();
	}

	/**
	 * UTILITY VARIE END
	 **********************************************************************************************************************************/

	/**
	 * Apre la dialog per la gestione del fascicolo fattura.
	 * 
	 * @param index
	 */
	public void openDialogFascicoloFattura(final Object index) {
		apriFascicolo(detailFatturaFepa, index, true);
	}

	/**
	 * Gestisce l'apertura del fascicolo.
	 * 
	 * @param detail
	 * @param index
	 */
	private void apriFascicolo(final DetailDocumentRedDTO detail, final Object index) {
		apriFascicolo(detail, index, false);
	}

	private void apriFascicolo(final DetailDocumentRedDTO detail, final Object index, final boolean loadDocFattura) {
		FascicoloDTO currentFascicolo = null;
		try {

			final Integer i = (Integer) index;
			currentFascicolo = detail.getFascicoli().get(i);
			openFEPACurrentFascicolo(currentFascicolo, loadDocFattura);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di recupero del fascicolo.");
		}
	}

	private void openFEPACurrentFascicolo(final FascicoloDTO currentFascicolo, final boolean loadDocFattura) {
		detailFascicolo = detailFascicoloSRV.getFascicoloDTOperTabFascicoloeFaldone(currentFascicolo, utente);
		FascicoloManagerBean bean;
		if (loadDocFattura) {
			bean = FacesHelper.getManagedBean(Constants.ManagedBeanName.FASCICOLO_FATTURA_MANAGER_BEAN);
			for (final DocumentoFascicoloDTO currDoc : detailFascicolo.getDocumenti()) {
				if ("FATTURA FEPA".equalsIgnoreCase(currDoc.getTipoDocumento())) {
					bean.setDocumentoFascicoloFattura(currDoc);
					break;
				}
			}
			this.setDialogFascicoloFatturaVisible(true);
		} else {
			bean = FacesHelper.getManagedBean(MBean.FASCICOLO_MANAGER_BEAN);
			this.setDialogFascicoloVisible(true);
		}
		bean.setDetail(detailFascicolo);
		bean.setCurrentFascicolo(currentFascicolo);

		if (loadDocFattura) {
			FacesHelper.executeJS("PF('dlgManageFascicolo_DF_FEPA_Fattura').show()");
			FacesHelper.executeJS("PF('dlgManageFascicolo_DF_FEPA_Fattura').toggleMaximize()");
		} else {
			FacesHelper.executeJS("PF('dlgManageFascicolo_DF_FEPA').show()");
			FacesHelper.executeJS("PF('dlgManageFascicolo_DF_FEPA').toggleMaximize()");
		}
	}

	/**
	 * Apre la dialog per la gestione del fascicolo fattura decreto associata al
	 * documento identificato dall'indice.
	 * 
	 * @param index
	 */
	public void openDialogFascicoloFatturaDecreto(final Object index) {
		final Integer i = (Integer) index;
		final DocumentoFascicoloDTO docFattura = detailFascicolo.getDocumenti().get(i);
		final PropertiesProvider pp = PropertiesProvider.getIstance();
		detailFatturaFepa = documentoRedSRV.getFatturaDetail(docFattura.getDocumentTitle(), utente, null,
				pp.getParameterByKey(PropertiesNameEnum.FATTURA_FEPA_CLASSNAME_FN_METAKEY));
		FascicoloDTO currentFascicolo = null;
		final int sizeLista = detailFatturaFepa.getFascicoli().size();
		for (int x = 0; x < sizeLista; x++) {
			if (detailFatturaFepa.getFascicoli().get(x).getIdFascicolo().equals(detailFatturaFepa.getIdFascicoloProcedimentale())) {
				currentFascicolo = detailFatturaFepa.getFascicoli().get(x);
				break;
			}

		}
		openFEPACurrentFascicolo(currentFascicolo, true);

	}

	/**
	 * Apre la dialog del fasicolo.
	 * 
	 * @param index
	 */
	public void openDialogFascicolo(final Object index) {
		apriFascicolo(detail, index);
	}

	/**
	 * Chiude la dialog del fascicolo.
	 */
	public void chiudiFascicolo() {
		this.setDialogFascicoloVisible(false);
	}

	/**
	 * Chiude la dialog del fascicolo fattura.
	 */
	public void chiudiFascicoloFattura() {
		this.setDialogFascicoloFatturaVisible(false);
	}

	/**
	 * Apre la dialog del fascicolo OP o FEPA.
	 * 
	 * @param index
	 */
	public void openDialogFascicoloOpFEPA(final Object index) {
		try {
			fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);
			final Integer i = (Integer) index;
			final AllegatoOPDTO allegato = listaAllegatiOP.get(i);
			guidAllegato = allegato.getGuid();
			fepaSRV.selectOPAllegatiAlDocAggiuntivo(utente, allegato.getGuid(), ordiniDiPagamento);
			setDlgFascOpFepaDfVisible(true);
			FacesHelper.executeJS("PF('dlgFascOPFepa_DF').show()");
			FacesHelper.executeJS("PF('dlgFascOPFepa_DF').toggleMaximize()");

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Errore durante l'operazione di recupero del fascicolo.");
		}
	}

	/**
	 * Chiude la dialog del fascicolo OP o FEPA.
	 */
	public void closedialogFascicoloOpFEPA() {
		setDlgFascOpFepaDfVisible(false);
	}

	/**
	 * Esegue la funzionalità di restituzione ordine di pagamento.
	 * 
	 * @param documentTitle
	 * @param guid
	 */
	public void restituisciOrdiniDiPagamento(final String documentTitle, final String guid) {
		ordiniDiPagamento = new ArrayList<>();
		fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);
		try {
			ordiniDiPagamento = fepaSRV.getOrdinePagamentoDecreto(utente, documentTitle);
			handleAllegatiOP(guid);

			if (tipologieDocAggiuntivi == null || tipologieDocAggiuntivi.isEmpty()) {
				tipologieDocAggiuntivi = fepaSRV.getTipologieDocumentiAggiuntivi();
			}

			for (final AllegatoOPDTO allOP : listaAllegatiOP) {
				if (!StringUtils.isNullOrEmpty(allOP.getTipologiaDocumento())) {
					for (final TipologiaDocumentoDTO tipologia : tipologieDocAggiuntivi) {
						if (allOP.getTipologiaDocumento().equals(tipologia.getIdTipologiaDocAggiuntivo())) {
							allOP.setTipologiaDocumentoDescrizione(tipologia.getDescrizione());
						}
					}
				}

			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}

	}

	/**
	 * Gestisce la lista: {@link #listaAllegatiOP}.
	 * 
	 * @param guid
	 */
	private void handleAllegatiOP(final String guid) {
		try {
			setListaAllegatiOP(fepaSRV.getAllegatiOP(utente, guid));
		} catch (final Exception e) {
			LOGGER.warn(e.getMessage(), e);
			// Va in errore anche se non ho allegati agli OP
			setListaAllegatiOP(new ArrayList<>());
		}
	}

	/**
	 * Elimina gli allegati OP associati al documento FEPA identificato dal guid.
	 * 
	 * @param guid
	 */
	public void deleteAllegatiOP(final String guid) {
		fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);
		try {
			fepaSRV.deleteAllegatiOP(utente, detail.getDocumentTitle(), guid);
			setListaAllegatiOP(fepaSRV.getAllegatiOP(utente, detail.getGuid()));

			for (final AllegatoOPDTO allOP : listaAllegatiOP) {
				if (!StringUtils.isNullOrEmpty(allOP.getTipologiaDocumento())) {
					for (final TipologiaDocumentoDTO tipologia : tipologieDocAggiuntivi) {
						if (allOP.getTipologiaDocumento().equals(tipologia.getIdTipologiaDocAggiuntivo())) {
							allOP.setTipologiaDocumentoDescrizione(tipologia.getDescrizione());
						}
					}
				}

			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Esegue attivita di inzializzazione in vista della visualizzazione della
	 * dialog per la gestione dell'allegato.
	 */
	public void inizializzaAggiungiDocAllegatoOP() {
		try {
			fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);
			idTipologiaDocAggiuntivoSelezionato = null;
			newAllegatoOP = new AllegatoOPDTO();
			setIdDlgAggAllegatoOpDfVisible(true);
			FacesHelper.executeJS("PF('dlgAgg_Allegato_OP_DF').show()");

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Gestisce la chiusura della dialog.
	 */
	public void closeAggiungiDocAllegatoOP() {
		setIdDlgAggAllegatoOpDfVisible(false);
	}

	/**
	 * Gestisce la funzionalita di aggiunta ordine di pagamento.
	 */
	public void aggiungiDocumentoAllegatoOP() {
		try {

			if (StringUtils.isNullOrEmpty(newAllegatoOP.getNomeDocumento()) || (newAllegatoOP.getDataHandler() == null || newAllegatoOP.getDataHandler().length == 0)) {
				throw new RedException("Inserire un allegato");
			}
			newAllegatoOP.setTipologiaDocumento(idTipologiaDocAggiuntivoSelezionato);
			final IAllegatoFacadeSRV allegatoSRV = ApplicationContextProvider.getApplicationContext().getBean(IAllegatoFacadeSRV.class);
			allegatoSRV.allegaAdOrdineDiPagamento(detail, newAllegatoOP, utente);

			setListaAllegatiOP(fepaSRV.getAllegatiOP(utente, detail.getGuid()));

			for (final AllegatoOPDTO allOP : listaAllegatiOP) {
				if (!StringUtils.isNullOrEmpty(allOP.getTipologiaDocumento())) {
					for (final TipologiaDocumentoDTO tipologia : tipologieDocAggiuntivi) {
						if (allOP.getTipologiaDocumento().equals(tipologia.getIdTipologiaDocAggiuntivo())) {
							allOP.setTipologiaDocumentoDescrizione(tipologia.getDescrizione());
						}
					}
				}

			}

			closeAggiungiDocAllegatoOP();

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}
	}

	/**
	 * Gestisce l'upload del file allegato.
	 * 
	 * @param event
	 */
	public void handleFileUploadAllegato(FileUploadEvent event) {
		try {
			event = checkFileName(event);
			if (event == null) {
				return;
			}
			newAllegatoOP.setNomeDocumento(event.getFile().getFileName());
			newAllegatoOP.setContentType(event.getFile().getContentType());
			newAllegatoOP.setDataHandler(event.getFile().getContents());

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());
		}

	}

	/**
	 * Rimuove il file allegato gestendo eventuali errori.
	 */
	public void rimuoviFileAllegato() {
		try {
			newAllegatoOP = new AllegatoOPDTO();

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());
		}
	}

	/**
	 * Associazione UNo o più OP Con allegato.
	 */
	public void salvaAssociazioneAllegatoOP() {

		fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);
		try {
			fepaSRV.salvaAssociazioneAllegatoOP(utente, ordiniDiPagamento, guidAllegato);
			closedialogFascicoloOpFEPA();

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(e);
		}

	}

	/**
	 * Apri fascicolo FEPA dell'OP.
	 * 
	 * @param index
	 */
	public void openFascicoloOP(final Object index) {
		try {
			fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);
			final Integer i = (Integer) index;
			final OrdineDiPagamentoDTO ordine = ordiniDiPagamento.get(i);
			documentiOPFascicoloFepa = fepaSRV.getFascicoliOP(ordine.getIdFascicoloFEPA());
			setDlgFepaOpDfVisible(true);
			FacesHelper.executeJS("PF('dlgFepaOP_DF').show()");
			FacesHelper.executeJS("PF('dlgFepaOP_DF').toggleMaximize()");
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError("Fasciolo O.P. Fepa non trovato");
		}

	}

	/**
	 * Chiude il fascicolo OP.
	 */
	public void closeFascicoloOP() {
		setDlgFepaOpDfVisible(false);
	}

	/**
	 * Restituisce uno StreamedContent che permette di effettuare il download del
	 * documento del fascicolo OP.
	 * 
	 * @param index che identifica il documento
	 * @return StreamedContent per il download
	 */
	public StreamedContent downloadDocumentoFascicoloOP(final Object index) {
		StreamedContent file = null;
		try {
			final Integer i = (Integer) index;

			fepaSRV = ApplicationContextProvider.getApplicationContext().getBean(IFepaFacadeSRV.class);

			final DocumentoFascicoloFepaDTO documentoSelezionato = documentiOPFascicoloFepa.get(i);

			final FileDTO doc = fepaSRV.downloadDocumentoFascicoloOP(documentoSelezionato.getIdFascicolo(), documentoSelezionato.getGuid());

			final ByteArrayInputStream bis = new ByteArrayInputStream(doc.getContent());

			file = new DefaultStreamedContent(bis, doc.getMimeType(), doc.getFileName());

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showError(GENERIC_ERROR_OPERAZIONE_MSG + e.getMessage());
		}
		return file;

	}

	/**
	 * Gestisce l'apertura del fascicolo procedimentale.
	 */
	@Override
	public void openFascicoloProcedimentale() {
		try {
			setDetail(); // Se necessario inizializzo tutti i documenti

			DetailDocumentRedDTO actualDetail;
			if (DettaglioDocumentoFileEnum.FEPAFATTURA.equals(this.dettaglioDocEnum)) {
				actualDetail = detailFatturaFepa;
			} else {
				actualDetail = detail;
			}

			if (CollectionUtils.isEmpty(actualDetail.getFascicoli())) {
				showInfoMessage("Non si hanno i privilegi di accesso al fascicolo.");
			} else {

				final String idfasciscoloProcedimentale = actualDetail.getIdFascicoloProcedimentale();
				if (idfasciscoloProcedimentale == null) {
					throw new RedException("Impossibile recuperare il fascicolo procedimentale");
				}

				final List<FascicoloDTO> listaFascicoliDocumento = actualDetail.getFascicoli();
				final Optional<FascicoloDTO> optionalFascicolo = listaFascicoliDocumento.stream()
						.filter(fascicolo -> idfasciscoloProcedimentale.equals(fascicolo.getIdFascicolo())).findAny();
				if (!optionalFascicolo.isPresent()) {
					throw new RedException("Impossibile recuperare il fascicolo procedimentale");
				}

				final FascicoloDTO currentFascicolo = optionalFascicolo.get();
				openFEPACurrentFascicolo(currentFascicolo, DettaglioDocumentoFileEnum.FEPAFATTURA.equals(dettaglioDocEnum));
			}
		} catch (final RedException red) {
			showError(red);
		}
	}

	/**
	 * Redireziona l'utente sulla coda definita da q. @see DocumentQueueEnum.
	 * 
	 * @param q
	 *            coda da raggiungere
	 * @return url risorsa
	 */
	@Override
	public String goToCoda(final DocumentQueueEnum q) {
		MasterDocumentRedDTO master = documentCmp.getMaster();
		if (master == null && documentCmp.getDetail() != null) {
			final DetailDocumentRedDTO detailDoc = documentCmp.getDetail();

			master = new MasterDocumentRedDTO();
			master.setDocumentTitle(detailDoc.getDocumentTitle());
			master.setNumeroDocumento(detailDoc.getNumeroDocumento());
			master.setIdCategoriaDocumento(detailDoc.getIdCategoriaDocumento());
		}

		final GoToCodaDTO goToCodaInfo = new GoToCodaDTO(master);

		FacesHelper.putObjectInSession(ConstantsWeb.SessionObject.FILTER_MASTER_GO_TO_CODA, goToCodaInfo);
		sessionBean.openFatElettronica();
		return sessionBean.gotoPage(NavigationTokenEnum.get(q));
	}

	/***
	 * GET & SET
	 ***********************************************************************/

	/**
	 * Restituisce il detail.
	 * 
	 * @return detail
	 */
	public DetailDocumentRedDTO getDetail() {
		return detail;
	}

	/**
	 * Restituisce true se l'update è abilitato.
	 * 
	 * @return updateEnabled
	 */
	@Override
	public Boolean getUpdateEnabled() {
		return updateEnabled;
	}

	/**
	 * Restituisce true se il popup è visibile, false altrimenti.
	 * 
	 * @return popupDocumentVisible
	 */
	@Override
	public boolean isPopupDocumentVisible() {
		return popupDocumentVisible;
	}

	/**
	 * Imposta la visibilita del popup.
	 * 
	 * @param popupDocumentVisible
	 */
	@Override
	public void setPopupDocumentVisible(final boolean popupDocumentVisible) {
		this.popupDocumentVisible = popupDocumentVisible;
	}

	/**
	 * Restituisce true se il protocollo è valido.
	 * 
	 * @return true se procollo valido, false altrimenti
	 */
	public boolean isProtocollo() {
		if (detail == null) {
			return false;
		}

		return detail.getNumeroProtocollo() != null && detail.getNumeroProtocollo().intValue() > 0;
	}

	/**
	 * Restituisce il model per la gestione del menu degli allegati.
	 * 
	 * @return MenuModel
	 */
	@Override
	public MenuModel getMenuAllegatiDetailModel() {
		return menuAllegatiDetailModel;
	}

	/**
	 * Imposta il model per la gestione del menu degli allegati.
	 * 
	 * @param menuAllegatiDetailModel
	 */
	public void setMenuAllegatiDetailModel(final MenuModel menuAllegatiDetailModel) {
		this.menuAllegatiDetailModel = menuAllegatiDetailModel;
	}

	/**
	 * Restituisce il mime type del file selezionato.
	 * 
	 * @return mimeTypeSelected
	 */
	@Override
	public String getMimeTypeSelected() {
		return mimeTypeSelected;
	}

	/**
	 * Restituisce il nome del file selezionato.
	 * 
	 * @return nome file selezionato
	 */
	@Override
	public String getNomeFileSelected() {
		return nomeFileSelected;
	}

	/**
	 * Restituisce il jasonTimeLine.
	 * 
	 * @return jsonTimeLine
	 */
	public String getJsonTimeLine() {
		return jsonTimeLine;
	}

	/**
	 * Imposta il parametro: jsonTimeLine.
	 * 
	 * @param jsonTimeLine
	 */
	public void setJsonTimeLine(final String jsonTimeLine) {
		this.jsonTimeLine = jsonTimeLine;
	}

	/**
	 * Restituisce true se il documento è modificabile, false altrimenti.
	 * 
	 * @return is modificabile
	 */
	@Override
	public boolean isModificabile() {
		return modificabile;
	}

	/**
	 * Restituisce la lista di response valide sul documento.
	 * 
	 * @return responseDetail
	 */
	@Override
	public final Collection<ResponsesRedEnum> getResponseDetail() {
		return responseDetail;
	}

	/**
	 * Restituisce true se la dialog del fascicolo è visibile.
	 * 
	 * @return visibilita dialog fascicolo
	 */
	public boolean isDialogFascicoloVisible() {
		return dialogFascicoloVisible;
	}

	/**
	 * Imposta la visibilita della dialog del fascicolo.
	 * 
	 * @param dialogFascicoloVisible
	 */
	public void setDialogFascicoloVisible(final boolean dialogFascicoloVisible) {
		this.dialogFascicoloVisible = dialogFascicoloVisible;
	}

	/**
	 * Restituisce il dettaglio della fattura.
	 * 
	 * @return DetailFatturaFepaDTO
	 */
	public DetailFatturaFepaDTO getDetailFatturaFepa() {
		return detailFatturaFepa;
	}

	/**
	 * Imposta il dettaglio della fattura FEPA.
	 * 
	 * @param detailFatturaFepa
	 */
	public void setDetailFatturaFepa(final DetailFatturaFepaDTO detailFatturaFepa) {
		this.detailFatturaFepa = detailFatturaFepa;
	}

	/**
	 * Restituisce la lista delle fatture associate al documento.
	 * 
	 * @return List di DocumentoRedFascicoloFepaDTO
	 */
	public List<DocumentoRedFascicoloFepaDTO> getFatture() {
		return fatture;
	}

	/**
	 * Imposta la lista delle fatture.
	 * 
	 * @param fatture
	 */
	public void setFatture(final List<DocumentoRedFascicoloFepaDTO> fatture) {
		this.fatture = fatture;
	}

	/**
	 * Restituisce la lista degli ordini di pagamento.
	 * 
	 * @return ordini di pagamento
	 */
	public List<OrdineDiPagamentoDTO> getOrdiniDiPagamento() {
		return ordiniDiPagamento;
	}

	/**
	 * Imposta la lista degli ordini di pagamento.
	 * 
	 * @param ordiniDiPagamento
	 */
	public void setOrdiniDiPagamento(final List<OrdineDiPagamentoDTO> ordiniDiPagamento) {
		this.ordiniDiPagamento = ordiniDiPagamento;
	}

	/**
	 * Restituisce la lista degli allegati.
	 * 
	 * @return lista degli allegati del documento
	 */
	public List<AllegatoOPDTO> getListaAllegatiOP() {
		return listaAllegatiOP;
	}

	/**
	 * Imposta la lista degli allegati.
	 * 
	 * @param listaAllegatiOP
	 */
	public void setListaAllegatiOP(final List<AllegatoOPDTO> listaAllegatiOP) {
		this.listaAllegatiOP = listaAllegatiOP;
	}

	/**
	 * @return permessoAddRemoveDoc
	 */
	public boolean isPermessoAddRemoveDoc() {
		return permessoAddRemoveDoc;
	}

	/**
	 * @param permessoAddRemoveDoc
	 */
	public void setPermessoAddRemoveDoc(final boolean permessoAddRemoveDoc) {
		this.permessoAddRemoveDoc = permessoAddRemoveDoc;
	}

	/**
	 * Restituisce il flag legato al permesso di associazione dell'ordine di pagare.
	 * 
	 * @return permesso associazione OP
	 */
	public boolean isPermessoAssociareOpDoc() {
		return permessoAssociareOpDoc;
	}

	/**
	 * Imposta il flag legato al permesso di associazione dell'OP.
	 * 
	 * @param permessoAssociareOpDoc
	 */
	public void setPermessoAssociareOpDoc(final boolean permessoAssociareOpDoc) {
		this.permessoAssociareOpDoc = permessoAssociareOpDoc;
	}

	/**
	 * Restituisce il nuovo allegato OP.
	 * 
	 * @return allegato OP
	 */
	public AllegatoOPDTO getNewAllegatoOP() {
		return newAllegatoOP;
	}

	/**
	 * Imposta il nuovo allegato OP.
	 * 
	 * @param newAllegatoOP
	 */
	public void setNewAllegatoOP(final AllegatoOPDTO newAllegatoOP) {
		this.newAllegatoOP = newAllegatoOP;
	}

	/**
	 * Restituisce la lista dei DocumentoFascicoloFepaDTO.
	 * 
	 * @return DocumentoFascicoloFepaDTO
	 */
	public List<DocumentoFascicoloFepaDTO> getDocumentiOPFascicoloFepa() {
		return documentiOPFascicoloFepa;
	}

	/**
	 * Imposta la lista dei DocumentoFascicoloFepaDTO.
	 * 
	 * @param documentiOPFascicoloFepa
	 */
	public void setDocumentiOPFascicoloFepa(final List<DocumentoFascicoloFepaDTO> documentiOPFascicoloFepa) {
		this.documentiOPFascicoloFepa = documentiOPFascicoloFepa;
	}

	/**
	 * Restituisce il valore del flag: popupDocumentDSRFepaVisible.
	 * 
	 * @return popupDocumentDSRFepaVisible
	 */
	public boolean isPopupDocumentDSRFepaVisible() {
		return popupDocumentDSRFepaVisible;
	}

	/**
	 * Imposta il valore del flag: popupDocumentDSRFepaVisible.
	 * 
	 * @param popupDocumentDSRFepaVisible
	 */
	public void setPopupDocumentDSRFepaVisible(final boolean popupDocumentDSRFepaVisible) {
		this.popupDocumentDSRFepaVisible = popupDocumentDSRFepaVisible;
	}

	/**
	 * Restituisce true se il dlg del fascicolo FEPA è visibile.
	 * 
	 * @return dlgFascOPFepa_DFVisible
	 */
	public boolean isDlgFascOpFepaDfVisible() {
		return dlgFascOpFepaDfVisible;
	}

	/**
	 * @param dlgFascOpFepaDfVisible
	 */
	public void setDlgFascOpFepaDfVisible(final boolean dlgFascOpFepaDfVisible) {
		this.dlgFascOpFepaDfVisible = dlgFascOpFepaDfVisible;
	}

	/**
	 * @return idDlgAggAllegatoOpDfVisible
	 */
	public boolean isIdDlgAggAllegatoOpDfVisible() {
		return idDlgAggAllegatoOpDfVisible;
	}

	/**
	 * @param idDlgAggAllegatoOpDfVisible
	 */
	public void setIdDlgAggAllegatoOpDfVisible(final boolean idDlgAggAllegatoOpDfVisible) {
		this.idDlgAggAllegatoOpDfVisible = idDlgAggAllegatoOpDfVisible;
	}

	/**
	 * @return dlgFepaOP_DFVisible
	 */
	public boolean isDlgFepaOpDfVisible() {
		return dlgFepaOpDfVisible;
	}

	/**
	 * @param dlgFepaOpDfVisible
	 */
	public void setDlgFepaOpDfVisible(final boolean dlgFepaOpDfVisible) {
		this.dlgFepaOpDfVisible = dlgFepaOpDfVisible;
	}

	/**
	 * Restituisce true se i button nell'EastSection sono visibili, false
	 * altrimenti.
	 * 
	 * @return showEastButtons
	 */
	public boolean isShowEastButtons() {
		return showEastButtons;
	}

	/**
	 * Imposta il flag associato alla visibilita dei buttons nell'EastSection.
	 * 
	 * @param showEastButtons
	 */
	public void setShowEastButtons(final boolean showEastButtons) {
		this.showEastButtons = showEastButtons;
	}

	/**
	 * @return the detailFascicolo
	 */
	public DetailFascicoloRedDTO getDetailFascicolo() {
		return detailFascicolo;
	}

	/**
	 * @param detailFascicolo the detailFascicolo to set
	 */
	public void setDetailFascicolo(final DetailFascicoloRedDTO detailFascicolo) {
		this.detailFascicolo = detailFascicolo;
	}

	/**
	 * @return the fepaCmp
	 */
	public DetailDocumetRedFepaComponent getFepaCmp() {
		return fepaCmp;
	}

	/**
	 * Imposta il component per la gestione dei fascicoli FEPA.
	 * 
	 * @param fepaCmp
	 */
	public void setFepaCmp(final DetailDocumetRedFepaComponent fepaCmp) {
		this.fepaCmp = fepaCmp;
	}

	/**
	 * Restituisce il component che gestisce la visualizzazione del documento.
	 * 
	 * @return il component
	 */
	public VisualizzaDettaglioDocumentoComponent getViewDettaglioDoc() {
		return viewDettaglioDoc;
	}

	/**
	 * Imposta il component che gestisce la visualizzazione del documento.
	 * 
	 * @param viewDettaglioDoc
	 */
	public void setViewDettaglioDoc(final VisualizzaDettaglioDocumentoComponent viewDettaglioDoc) {
		this.viewDettaglioDoc = viewDettaglioDoc;
	}

	/**
	 * Restituisce il flag associato al decreto dirigenziale.
	 * 
	 * @return dettaglioDD
	 */
	public boolean isDettaglioDD() {
		return dettaglioDD;
	}

	/**
	 * Imposta il flag associato al decreto dirigenziale.
	 * 
	 * @param dettaglioDD
	 */
	public void setDettaglioDD(final boolean dettaglioDD) {
		this.dettaglioDD = dettaglioDD;
	}

	/**
	 * Restituisce la lista delle tipologie documento aggiuntive.
	 * 
	 * @return la lista delle tipologie
	 */
	public List<TipologiaDocumentoDTO> getTipologieDocAggiuntivi() {
		return tipologieDocAggiuntivi;
	}

	/**
	 * Imposta la lista delle tipologie documento aggiuntive.
	 * 
	 * @param tipologieDocAggiuntivi
	 */
	public void setTipologieDocAggiuntivi(final List<TipologiaDocumentoDTO> tipologieDocAggiuntivi) {
		this.tipologieDocAggiuntivi = tipologieDocAggiuntivi;
	}

	/**
	 * Restituisce l'id della tipologia documento aggiuntiva selezionata.
	 * 
	 * @return l'id della tipologia
	 */
	public String getIdTipologiaDocAggiuntivoSelezionato() {
		return idTipologiaDocAggiuntivoSelezionato;
	}

	/**
	 * Imposta l'id della tipologia documento aggiuntiva selezionata.
	 * 
	 * @param idTipologiaDocAggiuntivoSelezionato
	 */
	public void setIdTipologiaDocAggiuntivoSelezionato(final String idTipologiaDocAggiuntivoSelezionato) {
		this.idTipologiaDocAggiuntivoSelezionato = idTipologiaDocAggiuntivoSelezionato;
	}

	/**
	 * Restituisce il component per la gestione della preview FEPA.
	 * 
	 * @return fepaPreviewComponent
	 */
	public DetailPreviewIntroComponent getFepaPreviewComponent() {
		return fepaPreviewComponent;
	}

	/**
	 * Restituisce il component per la gestione del dettaglio della fattura.
	 * 
	 * @return il component
	 */
	public DetailPreviewIntroComponent getFatturePreviewComponent() {
		return fatturePreviewComponent;
	}

	/**
	 * Restituisce il component per la gestione delle versioni degli allegati.
	 * 
	 * @return il component
	 */
	public IDocumentManagerDataTabDocumenti getVersioniAllegato() {
		return versioniAllegato;
	}

	/**
	 * Restituisce il nome del file.
	 * 
	 * @return nome file
	 */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * Imposta il nome del file.
	 * 
	 * @param nomeFile
	 */
	public void setNomeFile(final String nomeFile) {
		this.nomeFile = nomeFile;
	}

	/**
	 * Restituisce true se la dialog della fattura è visibile.
	 * 
	 * @return visibilità della dialog della fattura
	 */
	public boolean isDialogFascicoloFatturaVisible() {
		return dialogFascicoloFatturaVisible;
	}

	/**
	 * Imposta il flag associato alla visibilita della dialog della fattura.
	 * 
	 * @param dialogFascicoloFatturaVisible
	 */
	public void setDialogFascicoloFatturaVisible(final boolean dialogFascicoloFatturaVisible) {
		this.dialogFascicoloFatturaVisible = dialogFascicoloFatturaVisible;
	}

	@Override
	public void showDettaglioAfterLock() {
		// Non occorre fare niente in questo metodo.
	}

	@Override
	public void verificaFirmaDocPrincipaleAllegati() {
		// Non occorre fare niente in questo metodo.
	}

}