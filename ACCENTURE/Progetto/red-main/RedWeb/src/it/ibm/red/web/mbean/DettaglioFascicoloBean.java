package it.ibm.red.web.mbean;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import it.ibm.red.web.constants.ConstantsWeb;

/**
 * Bean dettaglio fascicolo.
 */
@Named(ConstantsWeb.MBean.DETTAGLIO_FASCICOLO_BEAN)
@ViewScoped
public class DettaglioFascicoloBean extends DettaglioFascicoloAbstractBean { 

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -2856025338654329204L;

	/**
	 * Post construct del bean.
	 */
	@Override
	@PostConstruct
	protected void postConstruct() {
		
		super.postConstruct();
	}

}
