package it.ibm.red.web.mbean.humantask;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.faces.event.ActionEvent;

import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.TreeNode;

import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.dto.ListElementDTO;
import it.ibm.red.web.mbean.AbstractBean;
import it.ibm.red.web.mbean.interfaces.OrganigrammaRetriever;

/**
 * Bean che gestisce l'assegnazione multipla.
 */
public class MultiAssegnazioneBean extends AbstractBean {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 2229498650564926220L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(MultiAssegnazioneBean.class.getName());

	/**
	 * Oggetto che contiene l'organigramma.
	 */
	private transient TreeNode root;
	
	/**
	 * Lista degli input box con cui l'utente può interagire.
	 */
	private List<ListElementDTO<NodoOrganigrammaDTO>> elementList;
	
	/**
	 * Un oggetto passato dal bean creatore che offre le funzionalità di espansione e reperimento dell'organigramma.
	 */
	private OrganigrammaRetriever organigramma;
	
	/**
	 * TipoAsssegnazione delle assegnazioni selezionate.
	 */
	private TipoAssegnazioneEnum tipoAssegnazione;
	
	/**
	 * Lista di elemnti selezionati dall'utente.
	 */
	private ListElementDTO<NodoOrganigrammaDTO> selectedListElement;
	
	
	/**
	 * Permette di inizializzare l'assegnazione e l'organigramma retriever che si occupa di costruire la root.
	 * 
	 * @param organigramma
	 *  Si occupa di costruire la root.
	 * @param assegnazione
	 *  Tipologia dell'assegnazione necessaria per costruire l'oggetto da ritornare.
	 */
	public void initialize(final OrganigrammaRetriever organigramma, final TipoAssegnazioneEnum assegnazione) {
		this.tipoAssegnazione = assegnazione;
		initialize(organigramma);
	}

	/**
	 * Inizializza una semplice multiassegnazione bean che ritorna direttamente i NodiOrganigramma selezionati. 
	 * @param organigramma
	 */
	public void initialize(final OrganigrammaRetriever organigramma) {
		this.organigramma = organigramma;
		this.root = this.organigramma.loadRootOrganigramma();
		this.elementList = new LinkedList<>();
		addRow();
	}
	
	/**
	 * Permette di inizializzare l'assegnazione e l'organigramma retriever che si occupa di costruire la root.
	 * 
	 * @param organigramma
	 *  Si occupa di costruire la root.
	 * @param assegnazione
	 *  Tipologia dell'assegnazione necessaria per costruire l'oggetto da ritornare.
	 *  
	 *  @param assegnazioni
	 *   Assegnazioni di default uitlizzati per inizializzare il campo. Per ragioni di compatibilità con la selezione dell'utente che non può essere duplicata è un Set piuttosto che una lista.
	 *  
	 */
	public void initialize(final OrganigrammaRetriever organigramma, final TipoAssegnazioneEnum assegnazione, final Set<NodoOrganigrammaDTO> assegnazioni) {
		initialize(organigramma, assegnazione);
		setDefaultAssegnazione(assegnazioni);
	}

	/**
	 * Post construct.
	 */
	@Override
	protected void postConstruct() {
		final RuntimeException e = new UnsupportedOperationException("Il postconstruct di questo metodo non deve essere invocato");
		LOGGER.error(e);
		throw e;
	}
	
	/******ORGANIGRAMMA*******/
	
	public void onOpenTree(final NodeExpandEvent event) {
		final TreeNode nodoExp = event.getTreeNode();
		this.organigramma.onOpenTree(nodoExp);
	}
	
	/**
	 * Gestisce l'evento "Select" dei nodi dell'albero degli assegnatari.
	 * @param event
	 */
	public void onSelectTreeNode(final NodeSelectEvent event) {
		
		try {
			final NodoOrganigrammaDTO select = (NodoOrganigrammaDTO) event.getTreeNode().getData();			
			
			if (getSelectedElementList().contains(select)) {
				showWarnMessage("L'assegnatario e' stato gia' inserito.");
				return;
			}
			
			selectNode(select);
			
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showInfoMessage("Errore durante l'operazione: " + e.getMessage());
		}
		
	}
	
	/**
	 * Seleziona il nodo passato nell'input contenuto in selected element.
	 * @param select
	 */
	private void selectNode(final NodoOrganigrammaDTO select) {
		if (select.getIdUtente() != null) {
			final String descrizioneNodo = select.getDescrizioneNodo() + " - " + select.getNomeUtente() + " " + select.getCognomeUtente();
			select.setDescrizioneNodo(descrizioneNodo);
		}
		this.selectedListElement.setData(select);
	}

	private AssegnazioneDTO createAssegnazione(final NodoOrganigrammaDTO select) {
		if (tipoAssegnazione == null) {
			final RuntimeException exception = new UnsupportedOperationException("Il tipoAssegnazione risulta essere nullo impossibile definire una assegnazione: probabilmente vuoi usare getSelectedElementList."
					+ "In caso tu voglia proprio usare questo metodo, costruisci il bean con il tipoAssegnazioneEnum definito");
			LOGGER.error(exception);
			throw exception;
		}
		
		String descrizioneNodo = select.getDescrizioneNodo();
		final UfficioDTO uff = new UfficioDTO(select.getIdNodo(), descrizioneNodo);
		
		UtenteDTO ute = null;
		if (select.getIdUtente() != null) {
			ute = new UtenteDTO();
			ute.setId(select.getIdUtente());
			ute.setIdUfficio(select.getIdNodo());
			ute.setNome(select.getNomeUtente());
			ute.setCognome(select.getCognomeUtente());
			descrizioneNodo = select.getDescrizioneNodo() + " - " + select.getNomeUtente() + " " + select.getCognomeUtente();
		}

		return new AssegnazioneDTO(null, tipoAssegnazione, select.getTipoStruttura(), null, null, descrizioneNodo, ute, uff);
		 
	}
	
	/*****END ORGANIGRAMMA******************/
	
	
	/*******DATATABLE***************/
	/**
	 * Crea un nuovo ListElement e lo aggiunge alla lista.
	 */
	public void addRow() {
		new ListElementDTO<NodoOrganigrammaDTO>(createEmptyAssegnazione(), elementList);
	}

	/**
	 * Effettua un reset della riga selezionata.
	 * @param event
	 */
	public void clearRow(final ActionEvent event) {
		setCurrentListElement(event);
		this.selectedListElement.setPlaceHolder(createEmptyAssegnazione());
	}

	/**
	 * Effettua l'eliminazione della riga selezionata.
	 * @param event
	 */
	public void removeRow(final ActionEvent event) {
		if (elementList.size() > 1) {
			final ListElementDTO<NodoOrganigrammaDTO> toRemove =  getDataTableClickedRow(event);
			final int id = toRemove.getId();
			this.elementList.remove(id);
			
			//reimposto gli id degli elementi della lista
			for (final Iterator<ListElementDTO<NodoOrganigrammaDTO>> iter = this.elementList.listIterator(); iter.hasNext();) {
				final ListElementDTO<NodoOrganigrammaDTO> currentElement = iter.next();
				if (currentElement.getId() >= id) {
					currentElement.setId(currentElement.getId() - 1);
				}
			}
			this.selectedListElement = null;
		} else {
			LOGGER.error("Chiamata la funzione di rimozione quando la tabella ha meno di 2 elementi");
			final RuntimeException exception = new UnsupportedOperationException("Attenzione tentativo di rimuovere il primo placeholder della lista assegnaizoni multiple");
			showError(exception);
			throw exception;
		}
	}
	
	/*******END DATATABLE***************/
	
	/**
	 * Permette all'organigramma di modificare il corretto elemento 
	 * @param event
	 */
	public void  setCurrentListElement(final ActionEvent  event) {
		this.selectedListElement = getDataTableClickedRow(event);
	}
	
	/**
	 * 
	 * @return
	 *  Lista delle assegnazioni selezionate dall'utente
	 */
	public List<NodoOrganigrammaDTO> getSelectedElementList() {
		final List<NodoOrganigrammaDTO> assegnazioneList = new ArrayList<>();
		for (final ListElementDTO<NodoOrganigrammaDTO> listElement : this.elementList) {
			if (!listElement.isPlaceHolder()) {
				assegnazioneList.add(listElement.getData());
			}
		}
		return assegnazioneList;
	}

	/**
	 * Restituisce la lista delle assegnazioni.
	 * @return lista assegnazioni
	 */
	public List<AssegnazioneDTO> getAssegnazioneList() {
		final List<AssegnazioneDTO> assegnazioneList = new ArrayList<>();
		for (final NodoOrganigrammaDTO listElement : getSelectedElementList()) {
			assegnazioneList.add(createAssegnazione(listElement));
		}
		return assegnazioneList;
	}
	
	/**
	 * Ritorna la singola assegnaizone inserita. Utilizzabile solo dalle response che inseriscono una singola assegnazione.
	 * @return l'assegnazione inserita dall'utente
	 */
	public NodoOrganigrammaDTO getAssegnazione() {
		if (this.elementList.size() > 1) {
			final String msg = "Richiamato il metodo per una singola assegnazione, ma l'utente ne ha inserite un numero > 1,. questo metodo è supportato solo per le assegnazioni singole";
			final RuntimeException e = new UnsupportedOperationException(msg);
			LOGGER.error(msg, e);
			throw e;
		}
		final List<NodoOrganigrammaDTO> filteredAssegnazioneList = getSelectedElementList();
		return filteredAssegnazioneList.isEmpty() ? null : filteredAssegnazioneList.get(0);
	}
	
	private static NodoOrganigrammaDTO createEmptyAssegnazione() {
		return new NodoOrganigrammaDTO("", -1, "");
	}
	
	/**
	 * Inizializza all'atto della creazione del multiassegnazione bean la lista delle assegnazioni permettendo un default.
	 * Questo metodo non è pensato per essere chaiamto da xhtml.
	 * 
	 * @param assegnazioni
	 *  Un Set delle assegnazioni di default da inserire.
	 *  
	 * @throws RedException
	 *  In caso di eventuali errori.
	 */
	private void setDefaultAssegnazione(final Set<NodoOrganigrammaDTO> assegnazioni) {
		boolean isFirst = elementList.size() != 1;
		int indiceAssegnazione = elementList.size() - 1;
		
		for (final NodoOrganigrammaDTO assegnazione : assegnazioni) {
			if (isFirst) {
				addRow();
			} else {
				isFirst = true;				
			}
			// Inizializzo il selectNode da modificare.
			this.selectedListElement = elementList.get(indiceAssegnazione);
			
			selectNode(assegnazione);
			indiceAssegnazione++;
		}
	}
	
	/**
	 * Restituisce la radice.
	 * @return
	 */
	public TreeNode getRoot() {
		return root;
	}
	
	/**
	 * Restituisce la lista degli inputbox ritornati dall'utente.
	 * @return
	 *  Una lista di elementi anche 'vuoti'.
	 */
	public List<ListElementDTO<NodoOrganigrammaDTO>> getElementList() {
		return elementList;
	}

}
