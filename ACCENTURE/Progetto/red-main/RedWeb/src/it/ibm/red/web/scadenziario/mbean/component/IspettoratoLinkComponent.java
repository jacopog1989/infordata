package it.ibm.red.web.scadenziario.mbean.component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.google.common.net.MediaType;

import it.ibm.red.business.dto.CountPer;
import it.ibm.red.business.dto.DocumentoScadenzatoDTO;
import it.ibm.red.business.dto.PEDocumentoScadenzatoDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PeriodoScadenzaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IScadenziaroStrutturaFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.web.document.mbean.component.AbstractComponent;
import it.ibm.red.web.helper.export.ExportExcelHelper;
import it.ibm.red.web.scadenziario.mbean.ScadenziarioBean;

/**
 * Component link ispettorato.
 */
public class IspettoratoLinkComponent extends AbstractComponent {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 3083555838809075537L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(IspettoratoLinkComponent.class.getName());

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Scadenziario bean.
	 */
	private final ScadenziarioBean bean;

	/**
	 * Servizio.
	 */
	private IScadenziaroStrutturaFacadeSRV scadenziarioSRV;

	/**
	 * Lista ispettorati.
	 */
	private List<CountPer<UfficioDTO>> ispettorati;

	/**
	 * Ufficio/Periodo.
	 */
	private final Map<Long, PeriodoScadenzaLinkComponent> idUfficio2PeriodoComponent = new HashMap<>();

	/**
	 * Costruttore del component.
	 * 
	 * @param inbean
	 * @param inIspettorati
	 */
	public IspettoratoLinkComponent(final ScadenziarioBean inbean, final List<CountPer<UfficioDTO>> inIspettorati) {
		this.bean = inbean;
		this.ispettorati = inIspettorati;
	}

	/**
	 * Ricerca i documenti scadenzati per ispettorato.
	 * 
	 * @param inbean
	 */
	public IspettoratoLinkComponent(final ScadenziarioBean inbean) {
		scadenziarioSRV = ApplicationContextProvider.getApplicationContext().getBean(IScadenziaroStrutturaFacadeSRV.class);
		this.bean = inbean;
		utente = bean.getUtente();

		final String errorMsg = "Errore durtante la ricerca dei documenti scadenzati";
		try {

			this.ispettorati = scadenziarioSRV.retrieveCountDocumentoInScadenzaPerUfficiDaIspettorato(utente);
			if (ispettorati == null) {
				throw new RedException("Errore durante il recupero dei documenti in scadenza.");
			}
		} catch (final Exception e) {
			LOGGER.error("Utente chiamante: " + utente.getDescrizione(), e);
			showErrorMessage(errorMsg);
		}
	}

	/**
	 * Recupera la lista degli uffici.
	 * 
	 * @return ispettorati
	 */
	public List<CountPer<UfficioDTO>> getUfficioList() {
		return ispettorati;
	}

	/**
	 * Imposta gli ispettorati selezionati.
	 * 
	 * @param ispettorato
	 */
	public void setSelected(final CountPer<UfficioDTO> ispettorato) {
		final PeriodoScadenzaLinkComponent component = makePeriodoCountComponent(ispettorato);
		bean.setPeriodoComponent(component);

		ispettorati.forEach(a -> a.setSelected(false));
		ispettorati.stream().filter(a -> a.equals(ispettorato)).forEach(a -> a.setSelected(true));
	}

	private PeriodoScadenzaLinkComponent makePeriodoCountComponent(final CountPer<UfficioDTO> ispettorato) {
		final Long idUfficio = ispettorato.getData().getId();
		PeriodoScadenzaLinkComponent component;
		if (idUfficio2PeriodoComponent.containsKey(idUfficio)) {
			component = idUfficio2PeriodoComponent.get(idUfficio);
		} else {
			component = new PeriodoScadenzaLinkComponent(idUfficio, bean);
			idUfficio2PeriodoComponent.put(idUfficio, component);
		}
		return component;
	}

	/**
	 * Ottiene gli ispettorati selezionati.
	 * 
	 * @return ispettorati
	 */
	public CountPer<UfficioDTO> getSelected() {
		final Optional<CountPer<UfficioDTO>> selected = this.ispettorati.stream().filter(a -> a.getSelected()).findFirst();

		return selected.isPresent() ? selected.get() : null;
	}

	/**
	 * Metodo per il download dell'export dello scadenzario.
	 * 
	 * @return
	 */
	public StreamedContent downloadExportScadenzario() {
		StreamedContent stream = null;

		try {

			final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			final ExportExcelHelper exportExcelHelper = new ExportExcelHelper();
			// Si costruisce il workbook Excel

			final List<DocumentoScadenzatoDTO> lista5 = new ArrayList<>();
			final List<DocumentoScadenzatoDTO> lista10 = new ArrayList<>();
			final List<DocumentoScadenzatoDTO> lista11 = new ArrayList<>();
			final List<DocumentoScadenzatoDTO> listaSc = new ArrayList<>();
			Map<PeriodoScadenzaEnum, List<PEDocumentoScadenzatoDTO>> map = null;
			for (final CountPer<UfficioDTO> countPer : ispettorati) {
				map = scadenziarioSRV.retrieveDocumentiPerPeriodoScadenza(countPer.getData().getId(), utente);
				if (map.get(PeriodoScadenzaEnum.MINORE_5) != null) {
					lista5.addAll(scadenziarioSRV.retrieveIdDocumentoByPeIdDocument(map.get(PeriodoScadenzaEnum.MINORE_5), utente));
				}
				if (map.get(PeriodoScadenzaEnum.MINORE_10) != null) {
					lista10.addAll(scadenziarioSRV.retrieveIdDocumentoByPeIdDocument(map.get(PeriodoScadenzaEnum.MINORE_10), utente));
				}
				if (map.get(PeriodoScadenzaEnum.MAGGIORE_11) != null) {
					lista11.addAll(scadenziarioSRV.retrieveIdDocumentoByPeIdDocument(map.get(PeriodoScadenzaEnum.MAGGIORE_11), utente));
				}
				if (map.get(PeriodoScadenzaEnum.SCADUTO) != null) {
					listaSc.addAll(scadenziarioSRV.retrieveIdDocumentoByPeIdDocument(map.get(PeriodoScadenzaEnum.SCADUTO), utente));
				}
			}

			final HSSFWorkbook excel = exportExcelHelper.getExportScadenzario(lista5, lista10, lista11, listaSc);
			excel.write(outputStream);
			stream = new DefaultStreamedContent(new ByteArrayInputStream(outputStream.toByteArray()), MediaType.MICROSOFT_EXCEL.toString(),
					"Scadenzario_" + DateUtils.dateToString(new Date(), DateUtils.DD_MM_YYYY_EXPORT) + ".xls", StandardCharsets.UTF_8.name());
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			showErrorMessage("Errore nel recupero delle informazioni.");
		}

		return stream;
	}
}
