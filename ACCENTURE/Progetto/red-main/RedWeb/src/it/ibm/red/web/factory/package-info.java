/**
 * @author CPIERASC
 *
 *	In questo package troveremo due factory utilizzate per gestire le preferenze dell'utente: in particolare andremo
 *	a gestire il tema applicato al look&feel applicativo e la pagina principale (ovvero la prima pagina che viene
 *	memorizzata all'accesso).
 *
 */
package it.ibm.red.web.factory;
