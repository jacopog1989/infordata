package it.ibm.red.web.helper.dtable;

/**
 * The Interface ISelectable.
 *
 * @author CPIERASC
 * 
 *         Interfaccia per gestire gli elementi selezionati nei datatable.
 */
public interface ISelectable  {

	/**
	 * Setter del flag selezionato.
	 * 
	 * @param flag	nuovo valore per il flag
	 */
	void setSelected(boolean flag);
	
	/**
	 * Getter del flag selezionato.
	 * 
	 * @return	il valore del flag
	 */
	boolean isSelected();
}
