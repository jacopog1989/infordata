package it.ibm.red.web.factory;

import java.util.Collection;

import it.ibm.red.business.dto.AbstractDTO;
import it.ibm.red.business.dto.ThemeDTO;
import it.ibm.red.business.enums.ThemeEnum;
import it.ibm.red.business.utils.StringUtils;

/**
 * A factory for creating Theme objects.
 *
 * @author CPIERASC
 * 
 *         Factory dei temi.
 */
public final class ThemeFactory extends AbstractDTO {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Istanza della factory.
	 */
	private static ThemeFactory instance;

	/**
	 * Insieme dei temi predefiniti.
	 */
	private Collection<ThemeDTO> themes;

	/**
	 * Tema di default.
	 */
	private ThemeDTO defaultTheme;

	/**
	 * Costruttore.
	 */
	private ThemeFactory() {
		defaultTheme = new ThemeDTO(ThemeEnum.getDefaultTheme());
		themes = ThemeEnum.getThemes();
	}

	/**
	 * Metodo per il recupero dell'istanza della factory.
	 * 
	 * @return	istanza
	 */
	public static ThemeFactory getInstance() {
		if (instance == null) {
			instance = new ThemeFactory();
		}
		return instance;
	}

	/**
	 * Getter tema default.
	 * 
	 * @return	tema default
	 */
	public ThemeDTO getDefaultTheme() {
		return defaultTheme;
	}

	/**
	 * Getter lista temi predefiniti.
	 * 
	 * @return	lista temi predefiniti
	 */
	public Collection<ThemeDTO> getThemes() {
        return themes;
	}

	/**
	 * Metodo per il recupero di un tema fornendo il suo identificativo.
	 * 
	 * @param id	identificativo tema
	 * @return		tema
	 */
	public ThemeDTO getTheme(final Integer id) {
		ThemeDTO output = null;
		for (ThemeDTO dto:themes) {
			if (id != null && id.equals(dto.getId())) {
				output = dto;
				break;
			}
		}
		return output;
	}

	/**
	 * Metodo per il recupero di un tema a partire dal suo nome.
	 * 
	 * @param name	nome del tema
	 * @return		tema
	 */
	public ThemeDTO getTheme(final String name) {
		ThemeDTO output = null;
		for (ThemeDTO dto:themes) {
			if (!StringUtils.isNullOrEmpty(name) && name.equals(dto.getName())) {
				output = dto;
				break;
			}
		}
		return output;
	}
	
}
