package it.ibm.red.web.document.mbean.component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.web.document.mbean.interfaces.IAllegatiDaAllacci;
import it.ibm.red.web.document.mbean.interfaces.IAllegatiDaAllacciInitializer;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Gestisce l'interazione tra i tab degli allegati e AllegatiDaAllaccio.
 */
public class AllegatiDaAllacciInitializer extends AbstractComponent implements IAllegatiDaAllacciInitializer {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6667223235168715090L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AllegatiDaAllacciInitializer.class);

	/**
	 * Permette di selezionare gli allacci.
	 */
	private AllegatiDaAllacciComponent allegatiDaAllacci;

	/**
	 * Gestore degli allegati.
	 */
	private final TabAllegatiComponent tabAllegati;

	/**
	 * Valorizzato solo se esistono dei docuemnti allacciati per il dettaglio in
	 * considerazione.
	 */
	private boolean allacciExist;

	/**
	 * documentTitle dei documenti allacciati.
	 */
	private Set<String> documentTitleAllacci;

	/**
	 * documentTitle del documento principale.
	 */
	private final String documenTitle;

	/**
	 * Costruttore di classe.
	 * 
	 * @param detail
	 * @param inTabAllegati
	 * @param utente
	 */
	public AllegatiDaAllacciInitializer(final DetailDocumentRedDTO detail, final TabAllegatiComponent inTabAllegati, final UtenteDTO utente) {
		this.tabAllegati = inTabAllegati;
		this.documenTitle = detail.getDocumentTitle();
		onChangeAllaccio(detail.getAllacci());
		if (inTabAllegati != null) {
			allegatiDaAllacci = new AllegatiDaAllacciComponent(utente);
		}
	}

	/**
	 * Da richiamare ogni volta che vengono modificati gli allacci.
	 */
	public void onChangeAllaccio(final Collection<RispostaAllaccioDTO> riferimentiProtInEntrata) {
		if (CollectionUtils.isNotEmpty(riferimentiProtInEntrata)) {
			documentTitleAllacci = riferimentiProtInEntrata.stream()
					.filter(rispostaAllaccio -> rispostaAllaccio.isVerificato() && StringUtils.isNotBlank(rispostaAllaccio.getIdDocumentoAllacciato()))
					.map(rispostaAllaccio -> rispostaAllaccio.getIdDocumentoAllacciato()).collect(Collectors.toSet());
		} else {
			documentTitleAllacci = null;
		}

		allacciExist = CollectionUtils.isNotEmpty(documentTitleAllacci);
	}

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.IAllegatiDaAllacciInitializer#openAllegatiDaAllacci().
	 */
	@Override
	public void openAllegatiDaAllacci() {
		try {
			verificaInizializzazione();
			allegatiDaAllacci.init(documenTitle, documentTitleAllacci, tabAllegati.getAllegati());
		} catch (final Exception e) {
			LOGGER.error(e);
		}
	}

	private void verificaInizializzazione() {
		if (allegatiDaAllacci == null) {
			showErrorMessage("Errore durante l'inizializzazione della pagina si prega di ricaricare la pagina.");
			final String error = "Non dovvrebbe essre possibile invocare questo metodo qualora prima di aver costruito l'oggetto allegatiDaAllacci";
			LOGGER.error(error);
			throw new IllegalArgumentException(error);
		}
	}

	/**
	 * Passo la selezione dell'utente al tabAllegati.
	 * 
	 */
	@Override
	public void registraAllegatiDaAllacci() {
		try {
			verificaInizializzazione();
			final List<AllegatoDTO> selectedAllegati = allegatiDaAllacci.registra();
			if (!CollectionUtils.isEmpty(selectedAllegati)) {

				final Set<String> guidList = selectedAllegati.stream().map(a -> a.getGuid()).collect(Collectors.toSet());

				List<AllegatoDTO> disjunction;
				if (CollectionUtils.isEmpty(tabAllegati.getAllegati())) {
					disjunction = new ArrayList<>();
				} else {
					disjunction = new ArrayList<>();
					for (final AllegatoDTO allInTable : tabAllegati.getAllegati()) {
						if (allInTable.getGuid() == null || !guidList.contains(allInTable.getGuid())) {
							disjunction.add(allInTable);
						}
					}
				}

				final List<AllegatoDTO> union = new ArrayList<>();

				union.addAll(disjunction);
				final List<AllegatoDTO> allegatiToAdd = new ArrayList<>();
				for (final AllegatoDTO selectedAllegato : selectedAllegati) {
					boolean exist = false;
					for (final AllegatoDTO allegato : union) {
						if (selectedAllegato.getDocumentTitleDaAllacci() != null
								&& selectedAllegato.getDocumentTitleDaAllacci().equals(allegato.getDocumentTitleDaAllacci())) {
							exist = true;
							break;
						}
					}
					if (!exist) {
						allegatiToAdd.add(selectedAllegato);
					}
				}
				union.addAll(allegatiToAdd);

				tabAllegati.setAllegati(union);

				FacesHelper.executeJS("PF('wdgAllegatiDaAllacci').hide()");
			}
		} catch (final Exception e) {
			showErrorMessage("Si è verificato un errore durante la registrazione dell'allaccio.");
			LOGGER.error(e);
		}
	}

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.IAllegatiDaAllacciInitializer#closeAllegatiDaAllacci().
	 */
	@Override
	public void closeAllegatiDaAllacci() {
		verificaInizializzazione();
		allegatiDaAllacci.reset();
	}

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.IAllegatiDaAllacciInitializer#isAllacciExist().
	 */
	@Override
	public boolean isAllacciExist() {
		return allacciExist;
	}

	/**
	 * @see it.ibm.red.web.document.mbean.interfaces.IAllegatiDaAllacciInitializer#getAllegatiDaAllacci().
	 */
	@Override
	public IAllegatiDaAllacci getAllegatiDaAllacci() {
		return allegatiDaAllacci;
	}
}
