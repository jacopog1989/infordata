package it.ibm.red.web.enums;

/**
 * Enum che definisce le modalità di spedizione di una mail.
 */
public enum ModalitaSpedizioneMailEnum {

	/**
	 * Valore.
	 */
	SINGOLA(1, "Singola mail per destinatario"),

	/**
	 * Valore.
	 */
	UNICA(2, "Unica mail per tutti i destinatari");
	
	/**
	 * Utilizzato per il db.
	 */
	private int value;
	/**
	 * Da mostrare nell'interfaccia.
	 */
	private String label;
	
	ModalitaSpedizioneMailEnum(final int inCode, final String inLabel) {
		value = inCode;
		this.label = inLabel;
	}

	/**
	 * Restituisce il valore.
	 * @return value
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Imposta il valore dell'enum.
	 * @param value
	 */
	protected void setValue(final int value) {
		this.value = value;
	}

	/**
	 * Restituisce la label associata all'enum.
	 * @return label enum
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Impsota la label associata all'enum.
	 * @param label
	 */
	protected void setLabel(final String label) {
		this.label = label;
	}
}
