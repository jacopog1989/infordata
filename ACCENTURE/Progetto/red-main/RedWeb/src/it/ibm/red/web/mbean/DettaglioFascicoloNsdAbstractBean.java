package it.ibm.red.web.mbean;
   
import org.primefaces.model.DefaultTreeNode;

import it.ibm.red.business.dto.DetailFascicoloPregressoDTO;
import it.ibm.red.business.dto.DocumentiFascicoloNsdDTO;
import it.ibm.red.business.dto.FascicoliPregressiDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.facade.IDfFacadeSRV;
import it.ibm.red.web.constants.ConstantsWeb;
import it.ibm.red.web.document.mbean.component.DettaglioFascicoloNsdComponent;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * @author VINGENITO
 * 
 */
public abstract class DettaglioFascicoloNsdAbstractBean extends AbstractBean {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 8422518253881813826L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DettaglioFascicoloNsdAbstractBean.class);
	
	/**
	 * Utente.
	 */
	protected UtenteDTO utente;
	
	/**
	 * Component fascicolo.
	 */
	protected DettaglioFascicoloNsdComponent fascicoloCmp;
   
   	/**
	 * Servizio.
	 */
	protected IDfFacadeSRV dfSRV;

	/**
	 * Post construct del bean.
	 */
	@Override
	protected void postConstruct() {
		final SessionBean sessionBean = FacesHelper.getManagedBean(ConstantsWeb.MBean.SESSION_BEAN);
		utente = sessionBean.getUtente();
		dfSRV = ApplicationContextProvider.getApplicationContext().getBean(IDfFacadeSRV.class);
	 
		fascicoloCmp = new DettaglioFascicoloNsdComponent(utente);
		
	}

	/**
	 * Imposta il dettaglio del fascicolo.
	 * 
	 * @param fascicoloDTO
	 *            fascicolo da impostare
	 */
	public void setDetail(final FascicoliPregressiDTO fascicoloDTO) {
		try { 
			final DetailFascicoloPregressoDTO detailFascicoloPregressodTO = dfSRV.getFascicoloById(fascicoloDTO, utente);
			fascicoloCmp.setDetail(detailFascicoloPregressodTO); 
			fascicoloCmp.creaTreeTable(detailFascicoloPregressodTO.getListaFascicolo());

			if (detailFascicoloPregressodTO.getListaDocumenti() != null) {
				for (final DocumentiFascicoloNsdDTO doc : detailFascicoloPregressodTO.getListaDocumenti()) {
					final DefaultTreeNode  docNodo = new DefaultTreeNode(doc, fascicoloCmp.getRoot());
					docNodo.setSelectable(false);
					docNodo.setExpanded(false);
				}
			}

			
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Errore nell'operazione di recupero del fascicolo nsd.");
		}

	}

	/**
	 * Resetta il dettaglio.
	 */
	public void unsetDetail() { 
		fascicoloCmp.setDetail(null);
	}

	/**
	 * Restituisce il component per la gestione del dettaglio del fascicolo.
	 * @return il componente DettaglioFascicoloNsdComponent
	 */
	public DettaglioFascicoloNsdComponent getFascicoloCmp() {
		return fascicoloCmp;
	}
}
