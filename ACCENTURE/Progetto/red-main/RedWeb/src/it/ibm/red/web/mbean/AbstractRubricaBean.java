package it.ibm.red.web.mbean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.component.datatable.DataTable;

import it.ibm.red.business.enums.StatoNotificaContattoEnum;
import it.ibm.red.business.enums.TipoOperazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.NotificaContatto;
import it.ibm.red.business.service.facade.IRubricaFacadeSRV;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.web.helper.dtable.SimpleDetailDataTableHelper;
import it.ibm.red.web.helper.faces.FacesHelper;

/**
 * Abstract bean per la rubrica.
 */
public abstract class AbstractRubricaBean extends AbstractBean {

	/**
	 * Id del datatable delle richieste.
	 */
	private static final String RICHIESTE_DT_ID = "RubricaTab:richieste_RGRT";

	/**
	 * Label Errore.
	 */
	private static final String ERROR_LABEL = "Errore: ";

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -3213835509355504655L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AbstractRubricaBean.class.getName());

	/**
	 * Contatto.
	 */
	private Contatto contattoConsiderato;

	/**
	 * Notifica.
	 */
	private NotificaContatto notificaPerOperazione;

	/**
	 * Datatable gestione notifiche contatti.
	 */
	private SimpleDetailDataTableHelper<NotificaContatto> notificheContattiDTH;

	/**
	 * Lista notifiche collegate.
	 */
	private List<NotificaContatto> altreNotificheCollegate;

	/**
	 * Nota rifiuto richiesta.
	 */
	private String notaRifiutaRichiesta;

	/**
	 * Nome del form.
	 */
	private String formName;

	/**
	 * Servizio rubrica.
	 */
	private IRubricaFacadeSRV rubSRV;

	/**
	 * Flag check univocità mail.
	 */
	private boolean checkUnivocitaMail;

	/**
	 * Contatto da utilizzare per confronto.
	 */
	private Contatto contattoPerConfronto;

	/**
	 * Operazione di modifica/eliminazione di un contatto.
	 */
	public void checkOperazioneEDati() {
		boolean approvatoConModifiche = false;
		if (!contattoConsiderato.checkUguale(contattoPerConfronto)) {
			approvatoConModifiche = true;
			notificaPerOperazione.setApprovatoConModifiche(approvatoConModifiche);
		}
		if (notificaPerOperazione.getOperazioneEnum().equals(TipoOperazioneEnum.ELIMINA_CONTATTO)) {
			altreNotificheCollegate = altreNotificheSulloStessoContatto(notificaPerOperazione);

			if (!CollectionUtils.isEmpty(altreNotificheCollegate)) {
				FacesHelper.executeJS("PF('dialogCheckElimina_RGGT').show()");
			} else {
				eseguiRichiesta(checkUnivocitaMail);
			}
		} else {
			eseguiRichiesta(checkUnivocitaMail);
		}
	}

	/**
	 * Esecuzione della modifica/eliminazione di un contatto.
	 * 
	 * @param checkUnivocitaMail
	 */
	public void eseguiRichiesta(final boolean checkUnivocitaMail) {
		try {
			if (notificaPerOperazione.getOperazioneEnum().equals(TipoOperazioneEnum.ELIMINA_CONTATTO)) {
				elimina(contattoConsiderato, notificaPerOperazione, altreNotificheCollegate);
			} else if (notificaPerOperazione.getOperazioneEnum().equals(TipoOperazioneEnum.RICHIESTA_CREAZIONE_CONTATTO)) {
				altreNotificheCollegate = new ArrayList<>();
				final boolean isValido = valida(contattoConsiderato);
				if (!isValido) {
					return;
				}
				approvaContattoRichiestaCreazione(contattoConsiderato, notificaPerOperazione, checkUnivocitaMail);
			} else {
				altreNotificheCollegate = new ArrayList<>();
				final boolean isValido = valida(contattoConsiderato);
				if (!isValido) {

					return;
				}
				modifica(contattoConsiderato, notificaPerOperazione, checkUnivocitaMail);
			}

			// Se devo rimuovere dai master lo aggiungo alla lista per il check
			altreNotificheCollegate.add(notificaPerOperazione);
			for (final NotificaContatto currentNotifica : altreNotificheCollegate) {
				removeNotificaContattoFromOneDataTable(notificheContattiDTH, currentNotifica);
			}

			// Si puliscono i filtri del datatable (se ci si trova nella pagina della
			// rubrica)
			clearFilterDataTableNotifiche(RICHIESTE_DT_ID, notificheContattiDTH);
			loadTabNotificheRichieste();
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'esecuzione della richiesta rubrica", e);
			showError(e);
		}
	}

	/**
	 * Carica il tab notifiche richieste.
	 */
	public abstract void loadTabNotificheRichieste();
	
	/**
	 * Restituisce il contatto per confronto.
	 * 
	 * @return Contatto con cui fare il confronto
	 */
	public Contatto getContattoPerConfronto() {
		return contattoPerConfronto;
	}

	/**
	 * Imposta il contatto per confronto.
	 * 
	 * @param contattoPerConfronto contatto da impostare
	 */
	public void setContattoPerConfronto(final Contatto contattoPerConfronto) {
		this.contattoPerConfronto = contattoPerConfronto;
	}

	/**
	 * Metodo che gestisce la notifica relativamente al contatto.
	 * 
	 * @param inNotificaPerOperazione
	 */
	protected void gestisciNotifica(final NotificaContatto inNotificaPerOperazione) {
		notificaPerOperazione = inNotificaPerOperazione;
		contattoConsiderato = rubSRV.getContattoFromNotifica(inNotificaPerOperazione);
		contattoPerConfronto = new Contatto();
		contattoPerConfronto.copyFromContatto(contattoConsiderato);
	}

	/**
	 * Gestisce il rifiuto della richiesta, aggiornando lo stato della notifica e
	 * ripulendo il datatable.
	 */
	public void rifiutaRichiesta() {
		try {
			rubSRV.updateStatoNotifica(notificaPerOperazione, StatoNotificaContattoEnum.RIFIUTATA, notaRifiutaRichiesta);

			removeNotificaContattoFromOneDataTable(notificheContattiDTH, notificaPerOperazione);
			LOGGER.info("Modifica rifiutata con successo");

			clearFilterDataTableNotifiche(RICHIESTE_DT_ID, notificheContattiDTH);

			// Devo anche rimuovere il contatto con la datadisattivazione così evito di
			// rimanere contatti appesi
			if (notificaPerOperazione.getOperazioneEnum().equals(TipoOperazioneEnum.RICHIESTA_CREAZIONE_CONTATTO)) {
				rubSRV.removeContattoRichiestaCreazione(notificaPerOperazione.getIdContatto());
			}

			showInfoMessage("Modifica rifiutata con successo");
		} catch (final Exception e) {
			LOGGER.error("Errore durante il rifiuto della modifica", e);
			showError(e);
		}
	}

	/**
	 * Operazione di presa visione per la notifica di creazione automatica di un
	 * nuovo contatto.
	 */
	public void presaVisioneNotificaContatto() {
		try {
			// Si aggiorna lo stato della notifica con la presa visione
			rubSRV.updateStatoNotifica(notificaPerOperazione, StatoNotificaContattoEnum.PRESA_VISIONE, null);

			// Si rimuove la notifica dal datatable delle richieste
			removeNotificaContattoFromOneDataTable(notificheContattiDTH, notificaPerOperazione);

			// Si puliscono i filtri del datatable (se ci si trova nella pagina della
			// rubrica)
			clearFilterDataTableNotifiche(RICHIESTE_DT_ID, notificheContattiDTH);

			// Si mostra all'utente un messaggio che comunica l'esito positivo
			// dell'operazione
			showInfoMessage("Presa visione eseguita con successo");
		} catch (final Exception e) {
			LOGGER.error(e);
			showError("Errore durante la presa visione della richiesta");
		}
	}

	/**
	 * Gestisce la rimozione della notifica dal datatable.
	 * 
	 * @param currentDTH
	 * @param notifica
	 */
	protected void removeNotificaContattoFromOneDataTable(final SimpleDetailDataTableHelper<NotificaContatto> currentDTH, final NotificaContatto notifica) {
		if (currentDTH != null) {
			final Collection<NotificaContatto> listaMasters = currentDTH.getMasters();

			NotificaContatto notificaContattoDaRimuovere = null;
			if (listaMasters != null) {
				for (final NotificaContatto currentNotificaContatto : listaMasters) {
					if (currentNotificaContatto.getIdNotificaContatto() == notifica.getIdNotificaContatto()) {
						notificaContattoDaRimuovere = currentNotificaContatto;
						break;
					}
				}

				if (notificaContattoDaRimuovere != null) {
					listaMasters.remove(notificaContattoDaRimuovere);
				}
			}
		}
	}

	/**
	 * Gestisce la modifica di un contatto di rubrica.
	 * 
	 * @param contatto
	 * @param notifica
	 * @param checkUnivocitaMail
	 */
	protected void modifica(final Contatto contatto, final NotificaContatto notifica, final boolean checkUnivocitaMail) {

		rubSRV.modifica(contatto, notifica, checkUnivocitaMail);

		// Si aggiornano i datatable degli altri tab (se ci si trova nella pagina della
		// rubrica)
		updateContattoInAllDataTableContatti(contatto);

		showInfoMessage("Contatto modificato con successo");

	}

	/**
	 * @param contatto
	 * @param notifica
	 */
	protected void approvaContattoRichiestaCreazione(final Contatto contatto, final NotificaContatto notifica, final boolean checkUnivocitaMail) {
		rubSRV.modifica(contatto, notifica, true, checkUnivocitaMail);
		// Si aggiornano i datatable degli altri tab (se ci si trova nella pagina della
		// rubrica)
		updateContattoInAllDataTableContatti(contatto);
		showInfoMessage("Contatto creato con successo");
	}

	/**
	 * @param contatto
	 * @param notifica
	 * @param altreNotificheCollegate
	 */
	protected void elimina(final Contatto contatto, final NotificaContatto notifica, final List<NotificaContatto> altreNotificheCollegate) {
		rubSRV.elimina(contatto.getContattoID(), notifica, altreNotificheCollegate);

		removeContattoFromAllDataTableContatti(contatto);

		showInfoMessage("Contatto eliminato con successo");
	}

	/**
	 * @param contatto
	 */
	protected abstract void updateContattoInAllDataTableContatti(Contatto contatto);

	/**
	 * @param contatto
	 */
	protected abstract void removeContattoFromAllDataTableContatti(Contatto contatto);

	/**
	 * @param idComponent
	 * @param dth
	 */
	protected abstract void clearFilterDataTableNotifiche(String idComponent, SimpleDetailDataTableHelper<NotificaContatto> dth);

	/**
	 * @param idComponent
	 */
	protected void resetDataTableRubricaToDefaultParameters(final String idComponent) {
		final DataTable d = getDataTableRubricaTab(idComponent);
		if (d != null) {
			d.reset();
		}
	}

	/**
	 * @param idComponent
	 * @return
	 */
	protected DataTable getDataTableRubricaTab(final String idComponent) {
		return (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(formName + ":" + idComponent);
	}

	/**
	 * @param notifica
	 * @return
	 */
	private List<NotificaContatto> altreNotificheSulloStessoContatto(final NotificaContatto notifica) {
		final List<NotificaContatto> notificheList = rubSRV.getNotifiche(notifica.getIdContatto(), StatoNotificaContattoEnum.IN_ATTESA.getNome());
		// rimuovo la notifica corrente
		notificheList.remove(notifica);

		return notificheList;
	}

	/**
	 * Getter contattoConsiderato.
	 * 
	 * @return il contatto definito dal model Contatto
	 */
	public Contatto getContattoConsiderato() {
		return contattoConsiderato;
	}

	/**
	 * Setter contattoConsiderato.
	 * 
	 * @param contattoConsiderato
	 */
	public void setContattoConsiderato(final Contatto contattoConsiderato) {
		this.contattoConsiderato = contattoConsiderato;
	}

	/**
	 * Restituisce la NotificaContatto per operazione.
	 * 
	 * @return NotificaContatto
	 */
	public NotificaContatto getNotificaPerOperazione() {
		return notificaPerOperazione;
	}

	/**
	 * Imposta la notifica per operazione definita da NotificaContatto.
	 * 
	 * @param notificaPerOperazione
	 */
	public void setNotificaPerOperazione(final NotificaContatto notificaPerOperazione) {
		this.notificaPerOperazione = notificaPerOperazione;
	}

	/**
	 * Restituisce il nome del form.
	 * 
	 * @return il formName
	 */
	public String getFormName() {
		return formName;
	}

	/**
	 * Imposta il nome del form.
	 * 
	 * @param formName
	 */
	public void setFormName(final String formName) {
		this.formName = formName;
	}

	/**
	 * Restituisce il dataTable Helper per le notifiche.
	 * 
	 * @return SimpleDetailDataTableHelper per le NotificaContatto
	 */
	public SimpleDetailDataTableHelper<NotificaContatto> getNotificheContattiDTH() {
		return notificheContattiDTH;
	}

	/**
	 * Imposta il dataTable Helper per le notifiche.
	 * 
	 * @param notificheContattiDTH
	 */
	public void setNotificheContattiDTH(final SimpleDetailDataTableHelper<NotificaContatto> notificheContattiDTH) {
		this.notificheContattiDTH = notificheContattiDTH;
	}

	/**
	 * Getter altreNotificheCollegate.
	 * 
	 * @return Lista di NotificaContatto relativa alle altre notifiche collegate.
	 */
	public List<NotificaContatto> getAltreNotificheCollegate() {
		return altreNotificheCollegate;
	}

	/**
	 * Setter altreNotificheCollegate.
	 * 
	 * @param altreNotificheCollegate
	 */
	public void setAltreNotificheCollegate(final List<NotificaContatto> altreNotificheCollegate) {
		this.altreNotificheCollegate = altreNotificheCollegate;
	}

	/**
	 * Restituisce la nota associata al Rifiuta Richiesta.
	 * 
	 * @return la nota
	 */
	public String getNotaRifiutaRichiesta() {
		return notaRifiutaRichiesta;
	}

	/**
	 * Imposta la nota associato al Rifiuta Richiesta.
	 * 
	 * @param notaRifiutaRichiesta
	 */
	public void setNotaRifiutaRichiesta(final String notaRifiutaRichiesta) {
		this.notaRifiutaRichiesta = notaRifiutaRichiesta;
	}

	/**
	 * Restituisce la facade per la gestione della rubrica.
	 * 
	 * @return IRubricaFacadeSRV
	 */
	public IRubricaFacadeSRV getRubSRV() {
		return rubSRV;
	}

	/**
	 * Imposta la facade per la gestione della rubrica.
	 * 
	 * @param rubSRV
	 */
	public void setRubSRV(final IRubricaFacadeSRV rubSRV) {
		this.rubSRV = rubSRV;
	}

	/**
	 * Getter checkUnivocitaMail.
	 * 
	 * @return checkUnivocitaMail
	 */
	public boolean getCheckUnivocitaMail() {
		return checkUnivocitaMail;
	}

	/**
	 * Setter checkUnivocitaMail.
	 * 
	 * @param checkUnivocitaMail
	 */
	public void setCheckUnivocitaMail(final boolean checkUnivocitaMail) {
		this.checkUnivocitaMail = checkUnivocitaMail;
	}

	/**
	 * Esegue controlli sul contatto per verificarne la validità.
	 * 
	 * @param contatto il contatto da verificare
	 * @return true se il contatto è valido, false altrimenti
	 */
	public boolean valida(final Contatto contatto) {

		boolean isOK = false;
		boolean campiObbligatoriOK = false;
		boolean fisicoOGiuridico = true;
		boolean procedi = false;

		final FacesContext context = FacesContext.getCurrentInstance();

		if (contatto.getTipoPersona() == null || (!"F".equals(contatto.getTipoPersona()) && !"G".equals(contatto.getTipoPersona()))) {
			fisicoOGiuridico = false;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ERROR_LABEL, "Il contatto deve essere necessariamente Fisico o Giuridico"));
		}

		if ("F".equals(contatto.getTipoPersona())) {
			if ((!StringUtils.isNullOrEmpty(contatto.getNome()) && !StringUtils.isNullOrEmpty(contatto.getCognome())) && (!StringUtils.isNullOrEmpty(contatto.getMail())
					|| !StringUtils.isNullOrEmpty(contatto.getMailPec()) || !StringUtils.isNullOrEmpty(contatto.getIndirizzo()))) {
				campiObbligatoriOK = true;
			}
		} else {
			if (!StringUtils.isNullOrEmpty(contatto.getNome()) && (!StringUtils.isNullOrEmpty(contatto.getMail()) 
					|| !StringUtils.isNullOrEmpty(contatto.getMailPec()))) {
				campiObbligatoriOK = true;
			}
		}

		final String valuePEC = contatto.getMailPec();
		final String valuePEO = contatto.getMail();

		final Pattern patternEmail = Pattern.compile("^[a-zA-Z0-9_\\.-]+@[a-zA-Z0-9-_\\.]+[a-zA-Z0-9-_]\\.[a-zA-Z0-9-]+$");
		// E' considerata OK anche se il campo è vuoto
		boolean flagMailOk = true;
		// E' considerata OK anche se il campo è vuoto
		boolean flagMailPecOk = true;

		// controllo formato E-Mail PEO
		if (!StringUtils.isNullOrEmpty(valuePEO) && !patternEmail.matcher(valuePEO).matches()) {
			flagMailOk = false;
		}
		// controllo formato E-Mail PEC
		if (!StringUtils.isNullOrEmpty(valuePEC) && !patternEmail.matcher(valuePEC).matches()) {
			flagMailPecOk = false;
		}

		if (!campiObbligatoriOK) {
			if ("F".equals(contatto.getTipoPersona())) {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ERROR_LABEL, "Compilare almeno uno dei campi obbligatori: mail , pec , indirizzo"));
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ERROR_LABEL, "Compilare almeno uno dei campi obbligatori: mail , pec "));
			}

		}

		if (flagMailOk && flagMailPecOk) {
			isOK = true;
		} else {
			if (!flagMailPecOk) {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ERROR_LABEL, "la PEC non ha il formato corretto!"));
			}
			if (!flagMailOk) {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ERROR_LABEL, "la PEO non ha il formato corretto!"));
			}
		}

		procedi = isOK && campiObbligatoriOK && fisicoOGiuridico;

		if (procedi && StringUtils.isNullOrEmpty(contatto.getAliasContatto())) {
			if (StringUtils.isNullOrEmpty(contatto.getCognome())) {
				contatto.setAliasContatto(contatto.getNome());
			} else {
				contatto.setAliasContatto(contatto.getCognome() + " " + contatto.getNome());
			}
		}

		return procedi;
	}

}
