package it.ibm.red.web.sottoscrizioni.dto;

import java.util.Date;

import it.ibm.red.business.dto.AbstractDTO;
import it.ibm.red.business.enums.IntervalloDisabilitazioneEnum;

/**
 * DTO disabilita notifica.
 */
public class DisabilitaNotificaDTO extends AbstractDTO{
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Intervallo date.
	 */
	private IntervalloDisabilitazioneEnum intervallo;
	
	/**
	 * Data da.
	 */
	private Date dataDa;

	/**
	 * Data a.
	 */
	private Date dataA;

	/**
	 * Costruttore del DTO.
	 */
	public DisabilitaNotificaDTO() {
		setIntervallo(IntervalloDisabilitazioneEnum.MAI);
	}

	/**
	 * Restituisce l'intervallo.
	 * @see IntervalloDisabilitazioneEnum
	 * @return IntervalloDisabilitazioneEnum
	 */
	public IntervalloDisabilitazioneEnum getIntervallo() {
		return intervallo;
	}

	/**
	 * Imposta l'intervallo.
	 * @param intervallo
	 */
	public void setIntervallo(final IntervalloDisabilitazioneEnum intervallo) {
		this.intervallo = intervallo;
		if (intervallo != IntervalloDisabilitazioneEnum.INTERVALLO) {
			setDataA(null);
			setDataDa(null);
		}
	}

	/**
	 * Restituisce l'intervallo iniziale della data.
	 * @return data
	 */
	public Date getDataDa() {
		return dataDa;
	}

	/**
	 * Imposta l'intervallo iniziale della data.
	 * @param dataDa
	 */
	public void setDataDa(final Date dataDa) {
		this.dataDa = dataDa;
	}

	/**
	 * Restituisce l'intervallo finale della data.
	 * @return data
	 */
	public Date getDataA() {
		return dataA;
	}

	/**
	 * Imposta l'intervallo finale della data.
	 * @param dataA
	 */
	public void setDataA(final Date dataA) {
		this.dataA = dataA;
	}
}
