
function startPB() {
	$('#pbAnnValue').addClass('value-bar');
	$('#pbAnn').removeClass('value-bar-full');
	$('#pbAnn').show();
	$('#labelAnn').hide();
}
			                	
function stopPB() {
	$('#pbAnnValue').removeClass('value-bar');
	$('#pbAnnValue').addClass('value-bar-full');
	setTimeout(showLabelPB, 100);
}

function showLabelPB() {
	$('#pbAnn').hide();
	$('#labelAnn').show();
}

function allignColumnToggler(checkFlags) {
	
	for (var i = 0; i < checkFlags.length; i++) { 
		
		var liValue = $('.ui-columntoggler-items li')[i];
		var divValue = $(liValue).find('.ui-chkbox-box');
		var spanValue = $(liValue).find('.ui-chkbox-icon');
		
		if (checkFlags[i] == true) {
			divValue.addClass('ui-state-active');
			spanValue.addClass('ui-icon-check');	
			spanValue.removeClass('ui-icon-blank');			
		} else {
			divValue.removeClass('ui-state-active');
			spanValue.removeClass('ui-icon-check');	
			spanValue.addClass('ui-icon-blank');			
		}
		
	}
	
}

var preventUnselectDatatable = false;

function unselectDatatable(datatableWidgetName, detailName) {
	if (!preventUnselectDatatable) {
		PF(datatableWidgetName).unselectAllRows();
		document.getElementById(detailName).style.display = 'none';
		//Per selezionare la prima riga invece che nascondere il dettaglio è sufficiente utilizzare 	PF('documentiDT').selectRow(0,false);
		//Si notano però dei rallentamenti (che forse dipendono solamente dall'ambiente in locale), e più in generale ci sono dei comportamenti non desiderati quando interagiscono ordinamenti e filtri
	} else {
		preventUnselectDatatable = false;
	}
}

function makeRowsSelectable(datatableWidgetName){
	PF(datatableWidgetName).rows.addClass('ui-datatable-selectable');
}

function clearDatatableFilters(datatableWidgetName, prevent) {
	preventUnselectDatatable = prevent;//se impostata a true non nasconde il dettaglio
	PF(datatableWidgetName).clearFilters();
}

function clearDatatableFiltersWidget(datatableWidgetName) {
	try {
		if (!PF(datatableWidgetName)) {
			return;
		}
		PF(datatableWidgetName).clearFilters();
	} catch (e) {
		console.log(e);
	}
}


// Argomenti in ingresso: widgetVar della dialog, widgetVar del bottone da triggerare (se necessario)
function onShowForClosableDlg(dlgName, closeButtonWvr) {
	var dialogXIcon = $($(PrimeFaces.widgets[dlgName].jqId).find('.ui-dialog-titlebar-icon')[0]);
//	var span = '<span onclick="pushCloseBtn(\''+dlgName+'\')" class="ui-dialog-titlebar-icon ui-dialog-titlebar-close ui-corner-all">' + dialogXIcon.html() + '</span>');
	var onClickStr = "onclick=\"pushCloseBtn('" + dlgName + "','"+ closeButtonWvr +"')\"";
	var span = '<span ' + onClickStr + ' class="dialog-close-button ui-dialog-titlebar-icon ui-dialog-titlebar-close ui-corner-all generic_padding0">' + dialogXIcon.html() + '</span>';
	dialogXIcon.replaceWith($(span));
}

// Argomenti in ingresso: widgetVar della dialog, widgetVar del bottone da triggerare (se necessario)
function pushCloseBtn(dlgName, closeButtonWvr){
	var dialogButtonClose;
	if(closeButtonWvr != "undefined" && closeButtonWvr != "") {
		dialogButtonClose = $($(PrimeFaces.widgets[dlgName].jqId).find($(PrimeFaces.widgets[closeButtonWvr].jqId)));
	} else {
		dialogButtonClose = $($(PrimeFaces.widgets[dlgName].jqId).find('.uniqueCloseButton')[0]);
	}
	dialogButtonClose.click();
}

/** START PANNELLO STATUS **/

var myTimeoutAjaxRequestId;
function onStartAjaxRequest() {
	$("#idBtnCancelAjaxReq").hide();
	PF('statusDialog').show();
	myTimeoutAjaxRequestId = setTimeout(function() {$("#idBtnCancelAjaxReq").fadeIn(1000);}, 3600000);
}

function onSuccessAjaxRequest() {
	clearStatusDialog();
}

function onErrorAjaxRequest(xhr, status, exception) {
	PF('errorDialog').show();
	$('#idSpanError').prop('title', 'STATUS: ' + status + '\nEXCEPTION: ' + exception);
	clearStatusDialog();
}

function onCancelAjaxRequest() {
	PrimeFaces.ajax.Queue.abortAll();
	clearStatusDialog();
}

function clearStatusDialog() {
	PF('statusDialog').hide();
	$("#idBtnCancelAjaxReq").hide();
	if (myTimeoutAjaxRequestId != null) {
		clearTimeout(myTimeoutAjaxRequestId);
		myTimeoutAjaxRequestId = null;
	}
}

/** STOP PANNELLO STATUS **/

function hideCentralEastSection() {
	$('#centralSectionForm').hide();
	$('#eastSectionForm').hide();
}

function showCentralEastSection() {
	$('#centralSectionForm').show();
	$('#eastSectionForm').show();
}

function openHelpPage() {
	var flag = document.getElementById('idSpanHelpPage');
	if (flag==null) {
		refreshHelpPagePanel();
	}
	PF('bar').show();
}

function updateOrg(orgOpened) {
	var rootOrg = document.getElementById('westSectionForm:idTabMain_LeftAside:idAccPanOrgUtente_VM:idOrgUtente:0');
	if (rootOrg==null) {
		document.getElementById('westSectionForm:idTabMain_LeftAside:idAccPanOrgUtente_VM:btnOrgRefresh').click();
	}
	//Gestione selezione organigramma
	if (!orgOpened) {
		PF('wdgRootOrg').unselectAllNodes();
	}
}

function updateNomeFascicolo() {
	
	var fascicoloPresent = $(PrimeFaces.widgets['wdgIdFascicoloProcedimentale_DMD'].jqId).val();
	
	//True se il documento è in creazione, false altrimenti.
	var flagDocCreation = !$('[id$=panelUpadateDocument]').length && !fascicoloPresent;
	
	var oggetto = PrimeFaces.widgets['wdgOggetto_DMD'] ? document.getElementById(PrimeFaces.widgets['wdgOggetto_DMD'].id) : null;
	var oggettoValue = '';
	if (oggetto) {
		oggettoValue = oggetto.value.trim();
	}

	if  (flagDocCreation) {
		var fascicoloCreazione = PrimeFaces.widgets['wdgFascicolo_DMD'] ? document.getElementById(PrimeFaces.widgets['wdgFascicolo_DMD'].id) : null;
		var tipologiaDocumento = PrimeFaces.widgets['wdgTipologiaDocumento_DMD'] ? document.getElementById(PrimeFaces.widgets['wdgTipologiaDocumento_DMD'].id + '_label') : null;

		var tipoDocValue = '';
		if (tipologiaDocumento) {
			tipoDocValue = tipologiaDocumento.textContent + " - ";
		}

		if (fascicoloCreazione) {

			//True se il fascicolo procedimentale è in modalità read only, false altrimenti.
			var flagFascicoloReadOnly = fascicoloCreazione.readOnly;
			
			if (!flagFascicoloReadOnly) {
				var newFascicoloValue = tipoDocValue + oggettoValue;
				fascicoloCreazione.value = newFascicoloValue;
				fascicoloCreazione.title = newFascicoloValue;
			}
			
		}
	}

	/** OGGETTO MAIL SPEDIZIONE START **********************************************/
	var mailSpedizioneOggetto = null;

	if (PrimeFaces.widgets['wdgMailSpedOggetto_DMDTS']) {
		mailSpedizioneOggetto = document.getElementById(PrimeFaces.widgets['wdgMailSpedOggetto_DMDTS'].id);
		if (mailSpedizioneOggetto) {
			mailSpedizioneOggetto.value = oggettoValue;
		}
	}
	
	/** OGGETTO MAIL SPEDIZIONE END **********************************************/
	

}


function downloadFile(cryptoParamFile, fileName){
  var url = window.location.href;
  var arr = url.split("/");
  var urlFile = arr[0]+"/"+arr[1]+"/"+arr[2]+"/"+arr[3]+"/DownloadContentServlet?params=" + cryptoParamFile +"&isDownload=true";
  window.location.assign(urlFile);
}

function initSched() {
    PrimeFaces.locales['it'] = {
            closeText: 'Chiudi',
            prevText: 'Precedente',
            nextText: 'Prossimo',
            currentText: 'Oggi',
            hourText:"Ora",
            minuteText:"Minuti",
            month:"Mese",
            nextText:"Prossimo",
            prevText:"Precedente",
            secondText:"Secondo",
            timeOnlyTitle:"Solo Orario",
            timeText:"Orario",
            aria:{
            	
            	
            	
                'paginator.PAGE': "Pagina {0}", 
                'calendar.BUTTON': "Visualizza calendario", 
                'datatable.sort.ASC': "attiva per ordinare la colonna in maniera crescente", 
                'datatable.sort.DESC': "attiva per ordinare la colonna in maniera descrescente", 
                'columntoggler.CLOSE': "Chiudi"
            },
			monthNames:[
				"Gennaio",
				"Febbraio",
				"Marzo",
				"Aprile",
				"Maggio",
				"Giugno",
				"Luglio",
				"Agosto",
				"Settembre",
				"Ottobre",
				"Novembre",
				"Dicembre"
			],
			 monthNamesShort: ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic' ],
		     dayNames: ['Domenica', 'Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'],
		     dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab'],
		     dayNamesMin: ['D', 'L', 'M', 'M ', 'G', 'V ', 'S'],
            weekHeader: 'Settimana',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '',
            month: 'Mese',
            week: 'Settimana',
            day: 'Giorno',
            allDayText : 'Tutto il giorno'
    };
}


/**
 * Serve per recuperare il widget dal widget var (id)
 * @param id
 * @returns
 */
function getWidgetVarById(id) {
    for (var propertyName in PrimeFaces.widgets) {
        var widget = PrimeFaces.widgets[propertyName];
        if (widget && widget.id === id) {
            return widget;
        }
    }
}

/**
 * La funzione chiude la dialog.
 * Quando piu dialog includono uno stesso panel e il bottone 'chiudi' è dentro il panel, per chiuderla si passano i widgetVar delle dialog che lo contengono. 
 * @params i widgetvar delle dialog da chiudere
 * @returns 
 */
function chiudiDialog() {
    for (var propertyName in PrimeFaces.widgets) {
        var widget = PrimeFaces.widgets[propertyName];
        for (var i = 0; i < arguments.length; ++i) {  
        	if (widget && widget.widgetVar === arguments[i]) {
	            PF(arguments[i]).hide();
		    }
        }
	}
}
/** esempio di come prendere un tab attivo dato un widgetVar start. **************************************************************/
/**
 * @param widgetVar
 * @param tabName
 * @param cb
 * @returns
 */
function handleOnTabShow(widgetVar, tabName, cb) {
	var wdgTabs = $(document.getElementById(PF(widgetVar).id));
	if (wdgTabs.find('.ui-state-active').first().text() == tabName) {
		cb();
	}
}
/** esempio di come prendere un tab attivo dato un widgetVar end. **************************************************************/


/**
 * Serve per annullare la selezione dell'organigramma
 * 
 * @param wdgVar
 * @returns
 */
function unselectOrg(wdgVar, numberOfTime) {
	numberOfTime = numberOfTime || 1;
	var widget = PrimeFaces.widgets[wdgVar];
	if (widget != null) {
		var count;
		for (count=0; count < numberOfTime; count++) {			
			widget.unselect(count);
		}
	}
	
}

//<-- Metodi ricerche -->

function onOffFullText() {
	var wdgFT = PrimeFaces.widgets["wdgFullText"];
	if (wdgFT.isChecked()) {
		PF('wdgEntita').disable();
		PF('wdgEntita').selectValue("DOCUMENTI");
	} else {
		PF('wdgEntita').enable();
	}
}

function switchSearchDetail() {
	var docDetail = document.getElementById('eastSectionForm:idDetailDocRicercaRapida');
	var fascDetail = document.getElementById('eastSectionForm:idDetailFascRicercaRapida');
	
	var wdgFT = PrimeFaces.widgets["wdgTabRicercaRapida"].cfg;
	if (wdgFT.selected === 1) {
//		mostro dettaglio e toggler documento
		docDetail.style.display = "block";
		
//		nascondo dettaglio e toggler fascicolo
		fascDetail.style.display = "none";
	} else {
//		mostro dettaglio e toggler fascicolo
		fascDetail.style.display = "block";
		
//		nascondo dettaglio e toggler documento
		docDetail.style.display = "none";
	}
}

//<-- Metodo per il ripeti ricerca -->
function repeatSearches(nameFirstTabToOpen, nameSecondTabToOpen) {
	// Dialog che verrà aperta alla ripetizione della rierca
	var wdgDialogRicerca = PF('dlgRicercaGenerica');
	// componente Tab View principale che renderizza le ricerche in base ai permessi
	var tabViewPricipale = PF('wdgTabRicerche');
	// componente Tab View secondario che contiene le specializzazioni delle ricerche
	var tabViewSecondario = null;
	
	var nodes = tabViewPricipale.navContainer[0].childNodes;
	var indexToOpen = 0;
	
	for (var i = 0; i < nodes.length; i++) {
		if (nodes[i].innerText === nameFirstTabToOpen) {
			indexToOpen = i;
			break;
		}
	}
	
	// viene aperta la dialog di ricerca
	wdgDialogRicerca.show();
	// e selezionato il tab richiesto -- di default è il primo
	tabViewPricipale.select(indexToOpen);
	
	// Si verifica se è necessario selezionare anche un secondo TabView
	if (nameFirstTabToOpen) {
		if (nameFirstTabToOpen === 'Avanzata') {
			tabViewSecondario = PF('wdgTabRicercaRed');
		} else if (nameFirstTabToOpen === 'Sistema di Protocollo') {
			tabViewSecondario = PF('wdgTabRicercaStampaRegistro');
		} else if (nameFirstTabToOpen === 'UCB') {
			tabViewSecondario = PF('wdgTabRicercaUCB');
		} else if (nameFirstTabToOpen === 'Ricerca Nsd') {
			tabViewSecondario = PF('wdgTabRicercaNsd');
		}
	}
	
	// Se specificato, si cerca il secondo tab da selezionare e lo si attiva 
	if (tabViewSecondario && nameSecondTabToOpen) {
		var nodesSecondTab = tabViewSecondario.navContainer[0].childNodes;
		var indexSecondTabToOpen = 0
		for (var i = 0; i < nodesSecondTab.length; i++) {
			var node = nodesSecondTab[i];
			if (node.innerText === nameSecondTabToOpen) {
				indexSecondTabToOpen = i;
				break;
			}
		}
		
		tabViewSecondario.select(indexSecondTabToOpen);
	}
}


//<-- metodo per la gestione del capo di ricerca Full-Text -->
function selectSearchFullText() {
	var valueSelected = PrimeFaces.widgets["wdgEntityFullText"].getSelectedValue();
	var wdgFieldFullText = PrimeFaces.widgets["wdgFieldFullText"];
	var fieldFullText = document.getElementById('idTabRicerche:idTabRicercheRed:ricercaDocRedForm:idFieldFullText');
	
	if (valueSelected == "") {
		fieldFullText.value = "";
		wdgFieldFullText.disable();
	} else {
		wdgFieldFullText.enable();
	}
}

function selectIdentificativoRicerca() {
	var valueSelected = PrimeFaces.widgets["wdgFilterIdentificativo"].getSelectedLabel();
	
	var wdgNumberRDP = PrimeFaces.widgets["wdgNumberRdp"];
	var wdgLegislatura = PrimeFaces.widgets["wdgSelectLegislatura"];
	var fieldNumberRdp = document.getElementById('idTabRicerche:idTabRicercheRed:ricercaDocRedForm:idAccordionUlterioriFiltri:idFieldNumberRdp_input');
	
	var wdgNumberDoc = PrimeFaces.widgets["wdgNumberDoc"];
	var fieldNumberDoc = document.getElementById('idTabRicerche:idTabRicercheRed:ricercaDocRedForm:idFieldNumberDoc_input');
	var wdgBarcode = PrimeFaces.widgets["wdgBarcode"];
	var fieldBarcode = document.getElementById('idTabRicerche:idTabRicercheRed:ricercaDocRedForm:idFieldBarcode');
	
	if (valueSelected == "Documento Prelex") {
		wdgNumberDoc.disable();
		wdgBarcode.disable();
		fieldNumberDoc.value = "";
		fieldBarcode.value = "";
		
		if (wdgNumberRDP != null && wdgLegislatura != null) {
			wdgNumberRDP.enable();
			wdgLegislatura.enable();
//			wdgLegislatura.selectValue("DOCUMENTI");
		}
	} else {
//		vengono riabilitati i campi identificativi dei documenti RED
		wdgNumberDoc.enable();
		wdgBarcode.enable();
		
		if (wdgNumberRDP != null && wdgLegislatura != null) {
//		vengono disabilitatii campi inerenti agli identificativi Prelex
			wdgNumberRDP.disable();
			wdgLegislatura.disable();
			fieldNumberRdp.value = "";
		}
	}
	
}

function isLoadedUlterioriFiltri() {
	var isSelectLoad = PrimeFaces.widgets["wdgTipoDocRicercaDoc"];
	var tuttoChk = PrimeFaces.widgets["wdgTuttoCat"];
	
	
	if (isSelectLoad.items.length == 0) {
		loadUlterioriFiltri();
	} 
	
	
}

function isLoadedUlterioriFiltriFasc() {
	var isSelectLoad = PrimeFaces.widgets["wdgTipoDocFasc"];
	
	if (isSelectLoad.items.length == 0) {
		loadUlterioriFiltriFasc();
	} 
	
}

//function disableCopiaConforme() {
//	var selectCopiaConf = PrimeFaces.widgets["wdgTipoAssegnazione"].jq;
//	selectCopiaConf.hide();
//}

function disableByNumDoc() {
	var wdg = PrimeFaces.widgets["wdgNumberDoc"];
	var wdgAccordionFiltri = PF('wdgAccordionUlterioriFiltri');

	if (wdg.getValue() != "") {
//		chiudo accordion e lo disabilito
		wdgAccordionFiltri.unselect(0);
		wdgAccordionFiltri.headers[0].classList.add('ui-state-disabled');

//		disabilito tutti i campi restanti
		onOffPerRicercaId(false)
	} else {
//		abilito l'accordion senza aprirlo
		wdgAccordionFiltri.headers[0].classList.remove('ui-state-disabled');
		onOffPerRicercaId(true)
	}
	
}

function disableByBarcode() {
	var wdg = PrimeFaces.widgets["wdgBarcode"].jq[0]; 
	var wdgAccordionFiltri = PF('wdgAccordionUlterioriFiltri');

	if (wdg.value != "") {
		wdgAccordionFiltri.unselect(0);
//		console.log("valore del campo: " + wdg.value);
		
	} else {
//		console.log("il campo è vuoto");
	}
}

function onOffPerRicercaId(enableForm) {
	var wdgOggetto = PrimeFaces.widgets["wdgOggetto"];
	var wdgEntityFullText = PrimeFaces.widgets["wdgEntityFullText"];
	var wdgFieldFullText = PrimeFaces.widgets["wdgFieldFullText"];
	var wdgDataCreazioneDa = PF('wdgDataCreazioneDa').jq[0]; // le function dei componenti 'Calendar' sembrano non funzionare quindi si agisce diretto sul css 
	var wdgDataCreazioneA = PF('wdgDataCreazioneA').jq[0];
	var wdgDataProtA = PF('wdgDataProtA').jq[0];
	var wdgDataProtDa = PF('wdgDataProtDa').jq[0];
	var wdgProtMitt = PrimeFaces.widgets["wdgProtMitt"];
	var wdgProtDa = PrimeFaces.widgets["wdgProtDa"];
	var wdgProtA = PrimeFaces.widgets["wdgProtA"];
	
	if (enableForm) {
		wdgOggetto.enable();
		wdgEntityFullText.enable();
		wdgFieldFullText.enable();
		wdgDataCreazioneDa.classList.remove('ui-state-disabled');
		wdgDataCreazioneA.classList.remove('ui-state-disabled');
		wdgDataProtA.classList.remove('ui-state-disabled');
		wdgDataProtDa.classList.remove('ui-state-disabled');
		wdgProtMitt.enable();
		wdgProtDa.enable();
		wdgProtA.enable();
	} else {
		wdgOggetto.disable();
		wdgEntityFullText.disable();
		wdgFieldFullText.disable();
		wdgDataCreazioneDa.classList.add('ui-state-disabled');
		wdgDataCreazioneA.classList.add('ui-state-disabled');
		wdgDataProtA.classList.add('ui-state-disabled');
		wdgDataProtDa.classList.add('ui-state-disabled');
		wdgProtMitt.disable();
		wdgProtDa.disable();
		wdgProtA.disable();
	}
	
}



function onOffCategoriaTutto() {
	var tuttoChk = PrimeFaces.widgets["wdgTuttoCat"];
	var entrataChk = PrimeFaces.widgets["wdgEntrataCat"];
	var uscitaChk = PrimeFaces.widgets["wdgUscitaCat"];
	var internoChk = PrimeFaces.widgets["wdgInternoCat"];
	
	if (tuttoChk.isChecked()) {
		entrataChk.check();
		disableChk("idTabRicerche:idTabRicercheRed:ricercaDocRedForm:idAccordionUlterioriFiltri:idEntrataCat");
		uscitaChk.check();
		disableChk("idTabRicerche:idTabRicercheRed:ricercaDocRedForm:idAccordionUlterioriFiltri:idUscitaCat");
		internoChk.check();
		disableChk("idTabRicerche:idTabRicercheRed:ricercaDocRedForm:idAccordionUlterioriFiltri:idInternoCat");
	} else {
		entrataChk.uncheck();
		enableChk("idTabRicerche:idTabRicercheRed:ricercaDocRedForm:idAccordionUlterioriFiltri:idEntrataCat");
		uscitaChk.uncheck();
		enableChk("idTabRicerche:idTabRicercheRed:ricercaDocRedForm:idAccordionUlterioriFiltri:idUscitaCat");
		internoChk.uncheck();
		enableChk("idTabRicerche:idTabRicercheRed:ricercaDocRedForm:idAccordionUlterioriFiltri:idInternoCat");
	}
	
	disableComponent(tuttoChk.isChecked());
}

function disableAllChk() {
	var tuttoChk = PrimeFaces.widgets["wdgTuttoCat"];
	var entrataChk = PrimeFaces.widgets["wdgEntrataCat"];
	var uscitaChk = PrimeFaces.widgets["wdgUscitaCat"];
	var internoChk = PrimeFaces.widgets["wdgInternoCat"];
	
	if (tuttoChk.isChecked()) {
		entrataChk.check();
		disableChk("idTabRicerche:idTabRicercheRed:ricercaDocRedForm:idAccordionUlterioriFiltri:idEntrataCat");
		uscitaChk.check();
		disableChk("idTabRicerche:idTabRicercheRed:ricercaDocRedForm:idAccordionUlterioriFiltri:idUscitaCat");
		internoChk.check();
		disableChk("idTabRicerche:idTabRicercheRed:ricercaDocRedForm:idAccordionUlterioriFiltri:idInternoCat");
	}	
	
	disableComponent(tuttoChk.isChecked());
}

function disableComponent(isTuttoChecked) {
	var destinatarioCmp = PrimeFaces.widgets["wdgDest"];
	var mittenteCmp = PrimeFaces.widgets["wdgMitt"];
	var ricezioneCmp = PrimeFaces.widgets["wdgRice"];
	
	if (isTuttoChecked) {
		destinatarioCmp.enable();
		mittenteCmp.enable();
		ricezioneCmp.enable();
	} else {
		destinatarioCmp.disable();
		mittenteCmp.disable();
		ricezioneCmp.disable();
	}	
}

function manageCmpToDisable() {
	var entrataChk = PrimeFaces.widgets["wdgEntrataCat"];
	var uscitaChk = PrimeFaces.widgets["wdgUscitaCat"];
	var destinatarioCmp = PrimeFaces.widgets["wdgDest"];
	var mittenteCmp = PrimeFaces.widgets["wdgMitt"];
	var ricezioneCmp = PrimeFaces.widgets["wdgRice"];
	
	if (entrataChk.isChecked() ) {
		mittenteCmp.enable();
		ricezioneCmp.enable();
	} else {
		mittenteCmp.disable();
		ricezioneCmp.disable();
	}	
	
	if (uscitaChk.isChecked()) {
		destinatarioCmp.enable();
	} else {
		destinatarioCmp.disable();
	}	
}




function disableChk(idElement) {
	document.getElementById(idElement).children[1].classList.add("ui-state-disabled");
}

function enableChk(idElement) {
	document.getElementById(idElement).children[1].classList.remove("ui-state-disabled");
}

function onOffTreeBtnAssegnazione() {
	var btnTree =  PrimeFaces.widgets["wdgButtonTipoAssegnazione"];
	var btnTreeOpe =  PrimeFaces.widgets["wdgButtonTipoAssegnOpe"];
	var valueSelected = PrimeFaces.widgets["wdgTipoAssegnazione"].getSelectedValue();
	var valueOpeSelected = PrimeFaces.widgets["wdgTipoOpe"].getSelectedValue();
	var textFieldAss = PrimeFaces.widgets["wdgAssegnatarioPerAssegn"];
	var selectCopiaConf = PrimeFaces.widgets["wdgSelectPerCopiaConf"];
	
	if (valueSelected == "COPIA_CONFORME") {
		
		btnTree.jq.hide();
		textFieldAss.jq.hide();
		selectCopiaConf.jq.show();
		
		// disabilito la selezione dal tree del tipo operazione
		onOffTreeBtnAssegnOpe();

	} else if (valueSelected == "" || valueSelected == "-") {
		
		btnTree.jq.show();
		textFieldAss.jq.show();
		btnTree.disable();
		selectCopiaConf.jq.hide();
		
		if(valueOpeSelected == "0" ){
			btnTreeOpe.disable();
		} else {
			btnTreeOpe.enable();
		}
	} else {
		
		btnTree.jq.show();
		textFieldAss.jq.show();
		btnTree.enable();
		selectCopiaConf.jq.hide();
		
		// disabilito la selezione dal tree del tipo operazione
		onOffTreeBtnAssegnOpe();
	}
}

function initTipoAssegnazione() {
	var btnTree =  PrimeFaces.widgets["wdgButtonTipoAssegnazione"];
	var valueSelected = PrimeFaces.widgets["wdgTipoAssegnazione"].getSelectedValue();
	var textFieldAss = PrimeFaces.widgets["wdgAssegnatarioPerAssegn"];
	var selectCopiaConf = PrimeFaces.widgets["wdgSelectPerCopiaConf"].jq;
	
	btnTree.jq.show();
	textFieldAss.jq.show();
	btnTree.disable();
	selectCopiaConf.hide();
	
}

function onOffTreeBtnAssegnOpe() {
	var btnTree =  PrimeFaces.widgets["wdgButtonTipoAssegnOpe"];
	var valueSelected = PrimeFaces.widgets["wdgTipoOpe"].getSelectedValue();
	
	if (valueSelected == 0) {
		btnTree.disable();
	} else {
		btnTree.enable();
		
//		disabilito la selezione dal tree del tipo assegnazione
		initTipoAssegnazione();
	}
}

function onOffTreeBtnAssegnFascicoli() {
	var btnTree =  PrimeFaces.widgets["wdgBtnAssFasc"];
	var valueSelected = PrimeFaces.widgets["wdgAssegnFasc"].getSelectedValue();
	
	if (valueSelected == 0) {
		btnTree.disable();
	} else {
		btnTree.enable();
	}
}

/** FALDONI vertical menu START****************************************************************************************/
function handleAccordionFaldoni(cb) {
	unselectOrg('acOrganigramma');
	unselectOrg('acScrivania',3);
	
	if ($('#idDivFaldEastSection_VMFald').length <= 0) {
		cb();
	}
}

function handleAccordion(arrayOfWidgetAccordion, cb) {
	arrayOfWidgetAccordion.forEach( function (widgetVar) {
		unselectOrg(widgetVar, 3);
	});
	if (cb) {		
		cb();
	}
}


/** FALDONI vertical menu END****************************************************************************************/

/** CHIUDI STATUS START*******************************************************************************************************************/
function chiudiStatusDialog() {
	setTimeout(function () {
		PF('statusDialog').hide();
	}, 1);
}
/** CHIUDI STATUS END ********************************************************************************************************************/

/** GESTIONE PANEL NOTIFICHE -> START ****************************************************************************************************/
function changeAnnotationPanel(event) {
    var valueClickedElement = event.target.previousElementSibling.value;
    var ulRubrica = document.getElementById('ulRubrica');
    var ulEventi = document.getElementById('ulEventi');
    var ulDocumenti = document.getElementById('ulDocumenti');

	ulRubrica.style.display = 'none';				    
	ulEventi.style.display = 'none';				    
	ulDocumenti.style.display = 'none';				    
	
    if (valueClickedElement == 'R') {
		ulRubrica.style.display = 'block';				    
    } else if (valueClickedElement == 'E') {
		ulEventi.style.display = 'block';				    
    } else if (valueClickedElement == 'D') {
		ulDocumenti.style.display = 'block';				    
    }
}
/** GESTIONE PANEL NOTIFICHE -> END ****************************************************************************************************/
/** DATA TABLE UTILS START**********************************************/
function refreshPageDataTable(widgetName) {
	if (PrimeFaces.widgets[widgetName] && PrimeFaces.widgets[widgetName].paginator) {
		PrimeFaces.widgets[widgetName].paginator.setPage(0);
	}
}
/** DATA TABLE UTILS END************************************************/

function clearTextAreaByWidget(widgetName) {
	if (PrimeFaces.widgets[widgetName]) {
		$(PrimeFaces.widgets[widgetName].jqId).text('');
	}
}

/** SELEZIONE LATO CLIENT DI UN TAB IN UN ACCORDION O IN UNA TABVIEW START********************************************************************************************************************/
function VM_findIndexAndSelectAccordionTab(widgetVarAccordion, headerTextContent, inputIndexToSelect) {
	
	//Controllo per prima cosa che esista l'input in cui c'è il valore dell'activeIndex 
	var wdgInputActiveIndexVerticalMenu_VM = PrimeFaces.widgets[inputIndexToSelect];
	if (!wdgInputActiveIndexVerticalMenu_VM) {
		return;
	}
	
	//prendo il widget dell'accordion e controllo che ci sia almeno un tab
	var accordion = PrimeFaces.widgets[widgetVarAccordion];
	var indexToSelect = null;
	if (!accordion || !accordion.headers || !accordion.headers.length) {
		//Non posso selezionare Fatturazione Elettronica
		return;
	}
	
	//scopro l'indice del tab da selezionare confrontando il nome dell'header visualizzato nell'accordion
	for (var i = 0; i < accordion.headers.length; i++) {
		if (accordion.headers[i].textContent && accordion.headers[i].textContent == headerTextContent) {
			indexToSelect = i;
			break;
		}
	}

	//se trovo l'index lo setto nell'input dell'activeIndex e forzo il refresh di questo valora lato java
	if (indexToSelect) {
		$(wdgInputActiveIndexVerticalMenu_VM.jqId).val(indexToSelect);
		//e' un remoteCommand a cui non è legato nessun metodo java, 
		//ha solo il process dell'input che contiene l'index e l'update sull'intero accordion
		refreshActiveIndex();
	}
}

function VM_selectTabFatturazioneElettronica() {
	var widgetVarAccordion = 'acScrivania';
	var headerTextContent = 'Fatturazione Elettronica';
	var inputIndexToSelect = 'wdgInputActiveIndexVerticalMenu_VM';
	VM_findIndexAndSelectAccordionTab(widgetVarAccordion, headerTextContent, inputIndexToSelect);
}
/** SELEZIONE LATO CLIENT DI UN TAB IN UN ACCORDION O IN UNA TABVIEW END********************************************************************************************************************/

function openDettaglioEstesoForScadenzario() {
//	PF('wdgDialogScadenziario').hide(); 
	PF('dlgManageDocument_RM').show();
	setTimeout(function () {
		PF('dlgManageDocument_RM').toggleMaximize(); 
		PF('dlgManageDocument_RM').initPosition();
	}, 1000);
}


function updateAnnoProtocolloMittente() {
	var dataProtMittente = PF('wdgDataAnnoProtMittente').getDate();
	
	if (dataProtMittente) {
		PF('wdgInputAnnoProtMittente').setValue(dataProtMittente.getFullYear())
	}
}

/******* RESET CAMPI FORM *******/

/* In ingresso passare gli id dei campi da resettare */
function resetCampi() {
	for ( var i = 0; i < arguments.length; ++i) {
		document.getElementById(arguments[i]).value = "";
	}
}

/* In ingresso passare i widgetVar dei campi da resettare */
function resetCampiByWidget () {
	for ( var i = 0; i < arguments.length; ++i) {
		$(PrimeFaces.widgets[(arguments[i])].jqId).val("");
	}
}

function resetCampoByWidgetVar(widgetVar) {
	PrimeFaces.widgets[widgetVar].jq.val('')
}


/**
 * Seleziono il primo tab se esiste il widget
 */
function selectFirstTabByTabsWidgetName(widgetName) {
	if (PF(widgetName)) {
		PF(widgetName).select(0);
	}
}

/**
 * 
 * sconsigliato l'utilizzo di questa in quanto l'id dipende dal form conviene utilizzare hideElementByWidgetVar
 * @param id
 * @returns
 */
function hideElementById (id) {
	var elem = document.getElementById(id);
	elem.style.display = "none";
}

function hideElementByWidgetVar (widgetId) {
	var elem = $(PrimeFaces.widgets[widgetId].jqId);
//	console.log('elem',elem);
	elem.hide();
}

/**
 * Evidenzio l'elemento selezionato
 * classe: la classe padre dell'elemento da evidenziare
 * index: posizione nella tabella dell'elemento da evidenziare
 * addClass: booleano che indica se aggiungere la classe agli elementi
 * id: id dell'elemento padre
*/
function highlightRow(classe, index, addClass, id) {
	if(addClass) {
		if (id != null &&  id != "undefined") {
			//Marca le righe interessate ad essere evidenziate
			aggiungiClasse (id, classe);
		}
	}
	var id = document.getElementsByClassName(classe)[index].id;
	document.getElementById(id).classList.add("ui-state-highlight"); 
} 

/**
 * Aggiunge una classe CSS agli elementi interni (quando non è possibile dal xhtml)
 * id: id dell'elemento padre
 * nomeClasse: nome della classe css da aggiungere
 */
function aggiungiClasse (id, nomeClasse) {
	var children = document.getElementById(id).childNodes;
	var size = children.length;
	for (var i = 0; i < size; ++i) {
		children[i].classList.add(nomeClasse);
	}
}

/**************************************function*input***************************************************/
function partialReadOnlyInput(that, event, readOnlyLength) {
    if ((event.which != 37 && (event.which != 39))
        && ((that.selectionStart < readOnlyLength)
        || ((that.selectionStart == readOnlyLength) && (event.which == 8)))) {
        return false;
    }
    return true;
}

/* setta il title (tooltip) per le caselle mail */
function setTitleMail () {
	var labels = document.getElementsByClassName("casellaPostaleLabel");
	for(var i=0; i< labels.length; ++i){
		var h3 = labels[i].getElementsByTagName("H3");
		var innerH3 = h3[0].innerHTML;
		var titlePos = innerH3.lastIndexOf("</span>");
		var title = innerH3.substr(titlePos + "</span>".length);
		h3[0].setAttribute('title', title);
	}
	
}

function checkInsertProtEmergenza() {
	var numberProtEmerg = PrimeFaces.widgets["wdgProtEmergNumber"];
	var btnRegistra = PrimeFaces.widgets["wdgProtEmergBtn"];
	
	if (numberProtEmerg.getValue() > 0) {
		btnRegistra.enable();
	} else {
		btnRegistra.disable();
	}
}

/** Gestione Deleghe Libro Firma START****************************************************************************************/
function onOffTreeBtnAssegnaDelegato() {
	var btnTree = PrimeFaces.widgets["wdgButtonDelegato"];
	var valueSelected = PrimeFaces.widgets["wdgInputDelegante"].jq.attr('value');
	
	if (valueSelected != null) {
		btnTree.enable();
	} else {
		btnTree.disable();
	}
}
 
function fissaNumeri() { 
	this.cfg.seriesDefaults.pointLabels.show = true;
	this.cfg.seriesDefaults.pointLabels.hideZeros = true;
}

function stampaEtichetta() {
	var url = window.location.href;
	var arr = url.split("/");
	var urlFile = arr[0]+"/"+arr[1]+"/"+arr[2]+"/"+arr[3]; 
	var myWindow;
	if(navigator.userAgent.indexOf("Chrome") != -1){
		myWindow = window.open(urlFile+"/views/stampaEtichette.xhtml","","top=2,left=1,width=500, height=500, toolbar=no,status=no,toolbar=no,menubar=no,location=no,scrollbars=no");
	} else{
		myWindow = window.open(urlFile+"/views/stampaEtichette.xhtml","","top=2,left=1,width=170, height=100, toolbar=no,status=no,toolbar=no,menubar=no,location=no,scrollbars=no");
	}
	myWindow.focus(); // Required for IE
	myWindow.print();

	return true;
}   

function removeNotifica(index){ 
	var element = document.getElementById('formNotifiche:notificheUtente_data');
	var rows = element.rows;
	var row = rows[index];
	row.classList.add("deleteNotifica");
}

function rimuoviHighlightNotifica(index){
	var element = document.getElementById('formNotifiche:notificheUtente_data');
	var rows = element.rows;
	var row = rows[index];
	var child = row.getElementsByClassName('notificaDaLeggere');
	var childDaCuiRimuovereClasse = child[0];
	childDaCuiRimuovereClasse.classList.remove("notificaDaLeggere");
	childDaCuiRimuovereClasse.classList.add("notificaLetta");
}
 
function updateMailTabSpedizione(){
	var tableDestinatari = document.getElementById('idDettagliEstesiForm:idDestinatari_DMD_data');
	var rows = tableDestinatari.rows;
	var mailStringDestinatari = '';
	destinatariTabSpedizione = document.getElementById(PrimeFaces.widgets['iTDestinatariSpedWV'].id);
	if (destinatariTabSpedizione) {
		destinatariTabSpedizione.value = '';
	}
	for(var i=0 ;  i<rows.length; i++){ 
		var campoMail = document.getElementById('idDettagliEstesiForm:idDestinatari_DMD:'+i+':mailSelectedOnTheFlyID');
		if(campoMail.value!=";"){
			if(i==0){
				mailStringDestinatari = campoMail.value + ";";
			} else {
				mailStringDestinatari = mailStringDestinatari + campoMail.value + ";" ;  
			}
			var destinatariTabSpedizione = null;
			if (PrimeFaces.widgets['iTDestinatariSpedWV']) {
				destinatariTabSpedizione = document.getElementById(PrimeFaces.widgets['iTDestinatariSpedWV'].id);
				if (destinatariTabSpedizione) {
					destinatariTabSpedizione.value = mailStringDestinatari;
				}
			}
		}

	}
} 

function changeMailAndCreateOnTheFly(){
	var onTheFly = PrimeFaces.widgets['creaOnTheFlyWV'] ? document.getElementById(PrimeFaces.widgets['creaOnTheFlyWV'].id) : null;
	onTheFly.value = 1; //Set contact to on the fly
	var infoButton = document.getElementById("idDettagliEstesiForm:infobtnMittente_DMD");
	if(infoButton==null){
		var infoButton = document.getElementById("eastSectionForm:infobtnMittente_DMD");
	}
	if(infoButton!=null){
		infoButton.classList.add('ui-state-disabled');
	}
}

function changeMailDestinatarioAndCreateOnTheFly(index){
	var onTheFly = document.getElementById("idDettagliEstesiForm:idDestinatari_DMD:"+index+":creaOnTheFlyDestID");
	onTheFly.value = 1; //Set contact to on the fly
	
	var sbiancaTipologiaPersona = document.getElementById("idDettagliEstesiForm:idDestinatari_DMD:"+index+":sbiancaTipPersonaID");
	sbiancaTipologiaPersona.value = true; //Set contact to on the fly
	
	var infoButton = document.getElementById("idDettagliEstesiForm:idDestinatari_DMD:"+index+":infobtnCol");
	infoButton.classList.add('ui-state-disabled');
}

function changeMailDestinatarioMailAndCreateOnTheFly(index){ 
	var onTheFly = document.getElementById("eastSectionForm:contattiA_inoltra_prot:"+index+":creaOnTheFlyDestMailID");
	onTheFly.value = 1; //Set contact to on the fly
	var infoButton = document.getElementById("eastSectionForm:contattiA_inoltra_prot:"+index+":infoBtn_prot");
	infoButton.classList.add('ui-state-disabled');
}

function changeMailDestinatarioMailAndCreateOnTheFlyInoltra(index){ 
	var onTheFly = document.getElementById("eastSectionForm:contattiA_inoltra:"+index+":creaOnTheFlyDestMailInoltraID");
	onTheFly.value = 1; //Set contact to on the fly
	if(document.getElementById("eastSectionForm:contattiA_inoltra:"+index+":infoBtn")){
		var infoButton = document.getElementById("eastSectionForm:contattiA_inoltra:"+index+":infoBtn");
		infoButton.classList.add('ui-state-disabled');
	} else if(document.getElementById("eastSectionForm:contattiA_inoltra:"+index+":infoBtnGruppo")){
		var infoButton = document.getElementById("eastSectionForm:contattiA_inoltra:"+index+":infoBtnGruppo");
		infoButton.classList.add('ui-state-disabled');
	}
}

function disabledInfoButton(){
	var infoButton = document.getElementById("eastSectionForm:infobtnMittente_DMD");
	infoButton.classList.add('ui-state-disabled');
}

/** Gestione Deleghe Libro Firma END****************************************************************************************/

function openDialog(prefix, wdgTarget) {
	var target = null;
	
	if (prefix != null) {
		target = prefix + wdgTarget;
	} else {
		target = wdgTarget;
	}
	
	PF('PNL_' + wdgTarget).getJQ().css('display', 'block');
	PF(target).getJQ().css('display', 'block');
	
}

function closeDialog(prefix, wdgTarget) {
	var target = null;
	
	if (prefix != null) {
		target = prefix + wdgTarget;
	} else {
		target = wdgTarget;
	}
	
	PF('PNL_' + wdgTarget).getJQ().css('display', 'none');
	PF(target).getJQ().css('display', 'none');
}
 
function clickPrintButton() {
	if(navigator.userAgent.indexOf("Chrome") != -1){
		 setTimeout(function(){document.getElementById('pdfPreviewStampa').contentWindow.document.getElementById('print').click(); }, 2000);
	} else{
		setTimeout(function(){
	     var pdfFrame = window.frames["pdfPreviewStampa"];
		 pdfFrame.focus();
		 pdfFrame.print(); }, 2500);
	}  
} 