function g_resize_all() {

	var HEIGHT_PAGE = document.documentElement.clientHeight;
	var HEIGHT_HEADER = $('#header').height() + $('#menuBar').height();
	var HEIGHT_FOOTER = $('#foot').height();
	var HEIGHT_EAST_SECTION = $('#eastSection').height();
	var HEIGHT_CENTRAL_SECTION = $('#centralSection').height();
	var HEIGHT_CENTRAL_SECTION_TOP_BUTTONS = $('#centralSectionButtons').height();
	
	var containerHeigth = HEIGHT_PAGE - HEIGHT_HEADER - HEIGHT_FOOTER;
	$('#container').height(containerHeigth);
	
	var divEastSectionContentHeigth = HEIGHT_PAGE - HEIGHT_HEADER - HEIGHT_FOOTER - 50;
	$('#eastSectionForm\\:divEastSectionContent').height(divEastSectionContentHeigth);
	$('.dd-table-layout').height(divEastSectionContentHeigth-10);
	
}

$(g_resize_all);
$(function() {
    var timer_id;
    $(window).resize(function() {
        clearTimeout(timer_id);
        timer_id = setTimeout(function() {
        	g_resize_all();
        }, 300);
        
    });
});

function calculateIframeHeight() {
	setTimeout(function() {
		var h = $('#centralSection').height() - $('#header').height() - $('#foot').height() - 20;
		$('.pdfViewer iframe').height(h);
		$('.pdfViewer iframe').removeClass('generic_minHeight500px');
		
	}, 1);
}

/**
 * 
 * Gestisce la preview all'interno della dialog di protocollazione mail (anche DSR).
 * 
 * @returns
 */
function hidePreviewMailProtocol() {
	// numero massimo di pixel oltre il quale la preview dovrà essere nascosta
	var MAX_WIDTH_FOR_PREVIEW = 1300;
	var pnlPreview = $('#eastSectionForm\\:idPreviewProtMail'); //.addClass("hide"); per nasconderlo
	var pnlMetadati = $('#eastSectionForm\\:preparazioneProtocolla'); 
    // Get width and height of the window excluding scrollbars
    var w = document.documentElement.clientWidth;
    if (w < MAX_WIDTH_FOR_PREVIEW) { 
    	if (!pnlPreview.hasClass("hide")) {
    		pnlPreview.addClass("hide");
    		pnlMetadati.removeClass("metadati-panel-protocolla-mail");
		}
	} else {
		if (pnlPreview.hasClass("hide")) {
			pnlPreview.removeClass("hide");
			pnlMetadati.addClass("metadati-panel-protocolla-mail");
		}
	}
} 

/**
 * Wrapper che gestisce l'inserimento dell'event listener 
 * sulla dialog di Creazione/Modifica.
 * 
 * @param isModfifica
 * @returns
 */
function wrapAddEventListener(isModfifica) {
	if (isModfifica) {
		window.addEventListener("resize", hidePreviewExtendedDetail);
		// Viene richiamata nel caso la window si trovi già sotto la soglia 
		hidePreviewExtendedDetail();
	} else {
		// viene rimossa nel caso sia ancora presente quando viene aperta la creazione
		window.removeEventListener("resize", hidePreviewExtendedDetail);
	}
}

function hidePreviewExtendedDetail() {
	hidePreview(false);
}

/**
 * Wrapper che gestisce l'inserimento dell'event listener per la protocollazione da Mail.
 * 
 * @returns
 */
function wrapAddEventListenerForProtMail() {
	window.addEventListener("resize", hidePreviewProtMail);
	// Viene richiamata nel caso la window si trovi già sotto la soglia 
	hidePreviewProtMail();
}

function hidePreviewProtMail() {
	hidePreview(true);
}



/**
 * 
 * Gestisce la preview all'interno della dialog del dettaglio esteso/Modifica & Protocollazione da Mail.
 * 
 * @returns
 */
function hidePreview(fromProtMail) {
	// numero massimo di pixel oltre il quale la preview dovrà essere nascosta
	var MAX_WIDTH_FOR_PREVIEW = 1300;
//	var isModifica = #{DocumentManagerBean.documentManagerDTO.inModifica};
	var pnlPreviewDetail;
	
	if (fromProtMail) {
		pnlPreviewDetail = $('#eastSectionForm\\:previewTabs'); 
	} else {
		pnlPreviewDetail = $('#idDettagliEstesiForm\\:docPreview'); 
	}
	
	var divDocMetadati = $('#docMetadata'); // Non è un panel PrimeFaces ma un Div
    // Get width and height of the window excluding scrollbars
    var w = document.documentElement.clientWidth;
    if (w < MAX_WIDTH_FOR_PREVIEW) { 
    	if (!pnlPreviewDetail.hasClass("hide")) {
    		pnlPreviewDetail.addClass("hide");
    		divDocMetadati.removeClass("ui-g-8");
    		divDocMetadati.addClass("ui-g-12");
		}
	} else {
		if (pnlPreviewDetail.hasClass("hide")) {
			pnlPreviewDetail.removeClass("hide");
			divDocMetadati.removeClass("ui-g-12");
			divDocMetadati.addClass("ui-g-8");
		}
	}
}

/**
 * Wrapper che rimuove l'evento alla chiusura della dialog.
 * 
 * @param isModfifica
 * @returns
 */
function wrapRemoveEventListener(isModfifica) {
	if (isModfifica) {
		window.removeEventListener("resize", hidePreviewExtendedDetail);
	}
}

function wrapRemoveProtMailEventListener() {
	window.removeEventListener("resize", hidePreviewProtMail);
}

/**
 * Gestisce la preview del dettaglio esteso/Modifica per i documenti fepa DSR.
 * 
 * @returns
 */
function hidePreviewDetailFepaDsr() {
	// numero massimo di pixel oltre il quale la preview dovrà essere nascosta
	var MAX_WIDTH_FOR_PREVIEW = 1300;
//	var isModifica = #{DocumentManagerBean.documentManagerDTO.inModifica};
	var pnlPreviewDetail = $('#docFepaDsrPreview'); // Non è un panel PrimeFaces ma un Div
	var divDocMetadati = $('#docFepaDsrMetadata'); // Non è un panel PrimeFaces ma un Div
    // Get width and height of the window excluding scrollbars
    var w = document.documentElement.clientWidth;
    if (w < MAX_WIDTH_FOR_PREVIEW) { 
    	if (!pnlPreviewDetail.hasClass("hide")) {
    		pnlPreviewDetail.addClass("hide");
    		divDocMetadati.removeClass("ui-g-8");
    		divDocMetadati.addClass("ui-g-12");
		}
	} else {
		if (pnlPreviewDetail.hasClass("hide")) {
			pnlPreviewDetail.removeClass("hide");
			divDocMetadati.removeClass("ui-g-12");
			divDocMetadati.addClass("ui-g-8");
		}
	}
}

/**
 * Gestisce la preview durante la protocollazione DSR da Mail.
 * 
 * @returns
 */
function hidePreviewProtocollaDsr() {
	// numero massimo di pixel oltre il quale la preview dovrà essere nascosta
	var MAX_WIDTH_FOR_PREVIEW = 1300;
//	var isModifica = #{DocumentManagerBean.documentManagerDTO.inModifica};
	var pnlPreviewDetail = $('#idProtoDsrPreview'); // Non è un panel PrimeFaces ma un Div
	var divDocMetadati = $('#idProtoDsrMetadata'); // Non è un panel PrimeFaces ma un Div
	// Get width and height of the window excluding scrollbars
	var w = document.documentElement.clientWidth;
	if (w < MAX_WIDTH_FOR_PREVIEW) { 
		if (!pnlPreviewDetail.hasClass("hide")) {
			pnlPreviewDetail.addClass("hide");
			divDocMetadati.removeClass("ui-g-8");
			divDocMetadati.addClass("ui-g-12");
		}
	} else {
		if (pnlPreviewDetail.hasClass("hide")) {
			pnlPreviewDetail.removeClass("hide");
			divDocMetadati.removeClass("ui-g-12");
			divDocMetadati.addClass("ui-g-8");
		}
	}
}

/**
 * Gestisce la preview nelle view dei Procedimenti Tracciati e Notifiche.
 * 
 * @returns
 */
function hidePreviewSottoscrizione() {
	// numero massimo di pixel oltre il quale la preview dovrà essere nascosta
	var MAX_WIDTH_FOR_PREVIEW = 1300;
	var pnlPreviewNotifiche = $('#centralSectionForm\\:idTab_STSCZ\\:idDettaglioNotifiche_STSCZ'); 
	var pnlMastersNotifiche = $('#centralSectionForm\\:idTab_STSCZ\\:idMasterNotifiche_STSCZ'); 
	var pnlPreviewProcedimenti = $('#centralSectionForm\\:idTab_STSCZ\\:idDettaglioProcedimento_STSCZ'); 
	var pnlMastersProcedimenti = $('#centralSectionForm\\:idTab_STSCZ\\:idMasterProcedimento_STSCZ'); 

	var w = document.documentElement.clientWidth;
	if (w < MAX_WIDTH_FOR_PREVIEW) { 
		// Gestione Tab Notifiche
		if (!pnlPreviewNotifiche.hasClass("hide")) {
			pnlPreviewNotifiche.addClass("hide");
			pnlMastersNotifiche.removeClass("ui-g-8");
			pnlMastersNotifiche.addClass("ui-g-12");
		}
		
		// Gestione Tab Procedimenti Tracciati
		if (!pnlPreviewProcedimenti.hasClass("hide")) {
			pnlPreviewProcedimenti.addClass("hide");
			pnlMastersProcedimenti.removeClass("ui-g-8");
			pnlMastersProcedimenti.addClass("ui-g-12");
		}
	} else {
		// Gestione Tab Notifiche
		if (pnlPreviewNotifiche.hasClass("hide")) {
			pnlPreviewNotifiche.removeClass("hide");
			pnlMastersNotifiche.removeClass("ui-g-12");
			pnlMastersNotifiche.addClass("ui-g-8");  
		}
		
		// Gestione Tab Procedimenti Tracciati
		if (pnlPreviewProcedimenti.hasClass("hide")) {
			pnlPreviewProcedimenti.removeClass("hide");
			pnlMastersProcedimenti.removeClass("ui-g-12");
			pnlMastersProcedimenti.addClass("ui-g-8");  
		}
	}
}
