function showWidgets() {
    var wdgs = $(".widget");
    for (i = 0; i < wdgs.length; i++) { 
      var w = $(wdgs[i]);
      w.fadeIn((i+1)*1000);
      w.css('visibility','visible').hide().fadeIn('slow');
    }
}

function hideWidgets() {
	$(".widget").fadeOut(1000);
}
