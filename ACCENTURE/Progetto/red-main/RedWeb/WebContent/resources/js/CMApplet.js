function CMApplet(base64, contentType, template) {
	var self = this;
	this.base64Modified = null;
	this.base64ModifiedEncodedUTF8 = null;
	this.esito = false;
	this.message = null;
	this.contentModified = false;
	var applet = null;
	
	try {
		var strTemplate = (template == null ? '' : '<PARAM name="template" value="' + template + '">');
		document.getElementById("appletdiv").innerHTML = '<applet id="appletfile" code="com\\accenture\\cmapplet\\CMApplet.class" archive="CMApplet.jar"' 
			+ ' style="width: 1px; height:1px; position: absolute; top:0; left:0"> <PARAM name="documentBase64" value="' + base64 
			+ '"><PARAM name="contentType" value="' + contentType + '">' + strTemplate + '</applet>';
		
		applet = document.getElementById("appletfile");
		if (!applet.getIsActiveProcess()) {
			applet.listenFile();
			if (applet.isModifiedFile()) {
				handleUpdateOk();
			} else {
				if (template == null) {
					this.message = "Il documento non è stato modificato.";
					this.esito = true;
				} else {
					this.message = "Il documento è stato modificato.";
					handleUpdateOk();
				}
			}
		} else {
			this.message = "Esiste già un documento attivo. Chiudere il processo prima di aprire il file per la modifica.";
		}
	} catch (err) {
		console.log(err);
		this.message = "Si è verificato un errore durante il processo di modifica del documento.";
	}

	this.getEsito = function() {
		return this.esito;
	};

	this.getBase64Modified = function() {
		return this.base64Modified;
	};

	this.getMessage = function() {
		return this.message;
	};

	this.getBase64ModifiedEncodedUTF8 = function() {
		return this.base64ModifiedEncodedUTF8;
	};

	this.getContentModified = function() {
		return this.contentModified;
	};
	
	this.destroy = function() {
		if (applet) {
			applet.destroy();
		}
	};
	
	function handleUpdateOk() {
		self.esito = true;
		self.contentModified = true;
		self.base64Modified = applet.getBase64Modify();
		self.base64ModifiedEncodedUTF8 = applet.getBase64ModifyEncodedUTF8();
	}
}

function getXMLHttpRequest() {
	var xmlHttpRequest;
	try {
		xmlHttpRequest = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e1) {
		try {
			xmlHttpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e2) {
			xmlHttpRequest = new XMLHttpRequest();
		}
	}
	return xmlHttpRequest;
}

function getModificaContentServletUrl() {
	var path = location.pathname.split('/');
	return location.protocol + '//' + location.host + '/' + path[1] + '/ModificaContentAppletServlet';
}

function openDoc(contentType, base64, workflowNumber, fileName) {
	try {
		PF('statusDialog').show();
		var cmApplet = new CMApplet(base64, contentType, null);
		var esito = cmApplet.getEsito();
		var contentModified = cmApplet.getContentModified();
		if (esito && contentModified) {
			// Recupero del nuovo base64 e chiamata alla servlet
			var nuovoBase64 = cmApplet.getBase64Modified();
			// Mostra popup di attesa
			var xmlHttp = getXMLHttpRequest();
			xmlHttp.open("POST", getModificaContentServletUrl(), true);
			xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlHttp.onreadystatechange = function() {
				if (xmlHttp.readyState === 4) { // Richiesta completata
					if (xmlHttp.status === 200) { // Richiesta OK (HTTP status 200)
						// Refresh dell'anteprima tramite remote command
						setTimeout(refreshDetail, 1500);
						
						// Visualizzazione del growl con l'esito
						setTimeout(visualizzaGrowlEsito, 2700, esito, cmApplet.getContentModified(), cmApplet.getMessage());
					} else { // Richiesta KO
						visualizzaGrowlEsito(false, cmApplet.getContentModified(), "Si è verificato un errore durante il processo di modifica del documento.");
					}
				}
			};
			xmlHttp.send("base64=" + encodeURIComponent(nuovoBase64)
					 	+ "&tipoapplet=0"
					 	+ "&workflowNumber=" + workflowNumber
					 	+ "&fileName=" + encodeURIComponent(fileName)
			);
		} else if (!esito || !contentModified) {
			// Visualizzazione del growl con l'esito (negativo o non modificato)
			visualizzaGrowlEsito(esito, cmApplet.getContentModified(), cmApplet.getMessage());
		}
	} finally {
		document.getElementById("appletdiv").innerHTML = " ";
	}
}

function openTemplate(contentType, base64, fileName, template) {
	try {
		PF('statusDialog').show();
		var cmApplet = new CMApplet(base64, contentType, template);
		var esito = cmApplet.getEsito();
		if (esito) {
			// Recupero del nuovo base64 e chiamata alla servelet
			var nuovoBase64 = cmApplet.getBase64Modified();
			// Mostra popup di attesa
			var xmlHttp = getXMLHttpRequest();
			xmlHttp.open("POST", getModificaContentServletUrl(), true);
			xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlHttp.onreadystatechange = function() {
				if (xmlHttp.readyState == 4) {
					// Visualizzazione del growl con l'esito
					visualizzaGrowlEsito(esito, cmApplet.getContentModified(), cmApplet.getMessage());
					
					// Aggiornamento della maschera di creazione tramite remote command
					remoteCommandSaveFileTemplateFromApplet();
				}
			};
			xmlHttp.send("base64=" + encodeURIComponent(nuovoBase64) 
						+ "&tipoapplet=1" 
						+ "&fileName=" + encodeURIComponent(fileName)
			);
		} else {
			// Visualizzazione del growl con l'esito
			visualizzaGrowlEsito(esito, cmApplet.getContentModified(), cmApplet.getMessage());
		}
	} finally {
		document.getElementById("appletdiv").innerHTML = " ";
	}
}


function visualizzaGrowlEsito(esito, contentModified, appletMessage) {
	var growlName = "infoGrowl";
	var msg = "Documento modificato correttamente.";
	var severity = "info";
	
	PF('statusDialog').hide(); // Si nasconde il dialog di attesa
	
	if (!esito) {
		severity = "error";
		growlName = "stickyGrowl";
	} else if (!contentModified) {
		severity = "warn";
		growlName = "stickyGrowl";
	}
	
	if (appletMessage != null && appletMessage != 'null' && appletMessage.length > 0) {
		msg = appletMessage;
	}
	
	PF(growlName).renderMessage({
		"summary" : "Esito modifica",
        "detail" : msg,
        "severity" : severity
	});
}