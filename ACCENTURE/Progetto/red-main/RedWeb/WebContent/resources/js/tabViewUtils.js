function TabViewUtilsConstructor() {
	
	/**
	 * parametro che contine l'id da modificare
	 */
	this.idTabToChange = undefined;
	
	/**
	 * Permette di cambiare il tab dato il nome del widgetVar della tabView.
	 * Il nome del tab da modificare viene pescato dalla variabile idTabToChange.
	 */
	this.changeTab = function (widgetName) {
		var idTab;
		if (!this.idTabToChange) {
			return true;
		} else {
			idTab = this.idTabToChange;
		}
		
		var fullPrimefacesId = PF(widgetName).id +':'+ idTab;
		var jqTab = $(PrimeFaces.escapeClientId(fullPrimefacesId));
		var titleDomObject = jqTab[0];
		PF(widgetName).select(titleDomObject.dataset.index);
		
		this.idTabToChange = undefined;
	}
	
	this.changeTabWithId = function (widgetName, idTabToChange) {
		this.idTabToChange = idTabToChange;
		changeTab(widgetName);
	}
	
}

var TabViewUtils = new TabViewUtilsConstructor();