//disabilita invio per evitare che premendo invio
//venga innescata l'azione collegata al primo bottone con type="submit" del form
$(document).keypress(function(event){		
     //13 è il codice per "Invio"
	 if(event.keyCode==13){
		var nodeName=event.target.nodeName;
		if(!nodeName || nodeName.toLowerCase()!='textarea'){
			return false;
		}	
	}
});

$(document).bind('paste',function(event){
	var copiaEIncolla = $(event.target).hasClass('classeCopiaIncolla');
	if(copiaEIncolla){
		setTimeout(function(){updateNomeFascicolo()},200);
	}
});

$(document).keydown(function(event){		
    //9 è il codice per "TAB"
	
	var elementDisableAutocompleteMittente = $(event.target).parent('.disableTabAutocompleteMittente');
	if(elementDisableAutocompleteMittente.length>0){
		if(event.keyCode==9 && $(event.target).val()!='' && $(event.target).val()){ // deve paritre sempre poi ci pensa il bean && $(event.target).val().length>2){
			if(PrimeFaces.widgets['autocompleteMittenteWidget']){
				PrimeFaces.widgets['autocompleteMittenteWidget'].search($(event.target).val());
				return false;
			}	
		}
	}else {
		var elementDisableAutocompleteDestinatario = $(event.target).parent('.disableTabAutocompleteDestinatario');
		if(elementDisableAutocompleteDestinatario.length>0){
			if(event.keyCode==9 && $(event.target).val()!='' && $(event.target).val()){//deve paritre sempre poi ci pensa il bean && $(event.target).val().length>2){
				var index = $(event.target).closest('*[data-ri]').attr('data-ri');
				if(PrimeFaces.widgets['autocompleteDestWidget'+index]){
					PrimeFaces.widgets['autocompleteDestWidget'+index].search($(event.target).val());
					return false;
				}	
			}
		} else {
			var elementDisableAutocompleteRifiutaConProt = $(event.target).parent('.elementDisableAutocompleteRifiutaConProt');
			if(elementDisableAutocompleteRifiutaConProt.length>0){
				if(event.keyCode==9 && $(event.target).val()!='' && $(event.target).val()){//deve paritre sempre poi ci pensa il bean && $(event.target).val().length>2){
					var index = $(event.target).closest('*[data-ri]').attr('data-ri');
					if(PrimeFaces.widgets['autocompleteRifiutaConProtWV'+index]){
						PrimeFaces.widgets['autocompleteRifiutaConProtWV'+index].search($(event.target).val());
						return false;
					}	
				}
			} else{
				
				var elementDisableAutocompleteInoltra = $(event.target).parent('.elementDisableAutocompleteInoltra');
				if(elementDisableAutocompleteInoltra.length>0){
					if(event.keyCode==9 && $(event.target).val()!='' && $(event.target).val()){//deve paritre sempre poi ci pensa il bean && $(event.target).val().length>2){
						var index = $(event.target).closest('*[data-ri]').attr('data-ri');
						if(PrimeFaces.widgets['autocompleteDestInoltraWidget'+index]){
							PrimeFaces.widgets['autocompleteDestInoltraWidget'+index].search($(event.target).val());
							return false;
						}	
					}
				}
			}
		}
	}
});