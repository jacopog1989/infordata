/***
 * Vedi resizeButtons.css dove sono scritte le varie classi
 * 
 * Per avere il comportamento nascondi rivela dei bottoni i bottoni devono avere la seguente classe 'resize_'+ sectionId + '_button',
 * dove sectionId è l'id della sessione con cui interagiscono
 */
var ResizeButtonUtils = {
		getClassClose : function (sectionId) {
			return 'resize_'+ sectionId + '_close';
		},
		isOpen : function (sectionId) {
			var classToAdd = this.getClassClose(sectionId);
			return !$('#'+sectionId).hasClass(classToAdd);
		}
}

//Testa se il compoenente indicato è aperto o meno.
function isOpen(sectionId) {
	var classToAdd = ResizeButtonUtils.getClassClose(sectionId);
	return !$('#'+sectionId).hasClass(classToAdd);
}

//Minimizza o massimizza la dimensione del componente centrale
function toggleCenter() {
	closeDiv('eastSection');
	closeDiv('westSection');
}


//Mimizza o massimizza il sectionId inoltre modifica anche i bottoni
function toggleDiv(sectionId) {
	$('.resize_' + sectionId + '_button').toggleClass('resize_button_active');

	var classToAdd = ResizeButtonUtils.getClassClose(sectionId);

	$('#centralSection').toggleClass(classToAdd);
	$('#'+sectionId).toggleClass(classToAdd);
}

//Apre il componente indicato
function openDiv(sectionId) {
	if(!ResizeButtonUtils.isOpen(sectionId)) {
		toggleDiv(sectionId);
	}
}
	
//Chiude il componente indicato
function closeDiv(sectionId) {
	if (ResizeButtonUtils.isOpen(sectionId)) {
		toggleDiv(sectionId);
	}
}

function toogleZoomedForm() {
	var element = document.getElementById('eastSectionForm');
    element.classList.toggle("zoomedForm");
    element.classList.toggle("ui-widget-content");
	var closeZoomed = document.getElementById('closeZoom');
	closeZoomed.classList.toggle("hideCloseZoom");
	closeZoomed.classList.toggle("displayCloseZoom");
}
