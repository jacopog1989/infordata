//Durata della sessione da tenere allineato con web.xml.
var sessionTimeout=3600000;

//Durata inattività in ms prima di mostrare dialog per refresh sessione (minuti*60*1000).
var idleTimeout=1500000;

var timerRefresh=setTimeout(openDialogRefreshSession , idleTimeout); 
var timerRedirect=setTimeout(openDialogRefreshSession, sessionTimeout); 
	
function openDialogRefreshSession(){	
	var elapse = sessionTimeout - idleTimeout; //quanto manca alla scadenza della sessione
	var today = new Date(); //prendo il momento attuale 
	var dateInMs = today.getTime(); //data in ms
	var oraScadenzaMs = dateInMs + elapse; 
	var oraScad = new Date(oraScadenzaMs);
	
	document.getElementById("scadenza").innerHTML = (oraScad.getHours()<10?'0':'') + oraScad.getHours() + ":" + (oraScad.getMinutes()<10?'0':'') + oraScad.getMinutes() + ":" + (oraScad.getSeconds()<10?'0':'') + oraScad.getSeconds();
	
	PF('idleDialog').show();
}

function openDialogRedirectHome(){
	PF('sessionExpiredDialog').show();
}

function resetSessionTimer(){
	//Resetto entrambi i timer
	clearTimeout(timerRefresh);
	clearTimeout(timerRedirect);
	timerRefresh=setTimeout(openDialogRefreshSession, idleTimeout); 
	timerRedirect=setTimeout(openDialogRedirectHome, sessionTimeout); 
}