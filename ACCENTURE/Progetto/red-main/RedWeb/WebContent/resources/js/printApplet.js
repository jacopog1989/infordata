function printApplet(protocollo){   

	var attributes = {
			code:'com.capgemini.nsd.print.TextPrinterApplet.class', 
			width:450, 
			height:30};
	
	var parameters = {
			fontSize:12, 
			permissions:'sandbox',
			protocollo:protocollo,
			silent_print:'true',
			jnlp_href: 'print_applet.jnlp',
	        jnlp_embedded: 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPCEtLSBocmVmIGF0dHJpYnV0ZSBjb250YWlucyByZWxhdGl2ZSBwYXRoOwogICAgIGNvZGViYXNlIGF0dHJpYnV0ZSBub3Qgc3BlY2lmaWVkIC0tPgo8am5scCBocmVmPSJwcmludGVyX2FwcGxldC5qbmxwIj4KICAgIDxpbmZvcm1hdGlvbj4KICAgICAgICA8dGl0bGU+UHJpbnRlciBBcHBsZXQ8L3RpdGxlPgogICAgICAgIDx2ZW5kb3I+TUVGIC1SR1MgPC92ZW5kb3I+CiAgICA8L2luZm9ybWF0aW9uPgogICAgPHJlc291cmNlcz4KICAgICAgICA8IS0tIEFwcGxpY2F0aW9uIFJlc291cmNlcyAtLT4KICAgICAgICA8ajJzZSB2ZXJzaW9uPSIxLjcrIiAvPgogICAgICAgIDxqYXIgaHJlZj0KICAgICAgICAgICAgIi4uL3ByaW50YXBwbGV0L3ByaW50QXBwbGV0LmphciIgCiAgICAgICAgICAgICBtYWluPSJ0cnVlIiAvPgogICAgPC9yZXNvdXJjZXM+CiAgICA8c2VjdXJpdHk+CiAJIAk8ajJlZS1hcHBsaWNhdGlvbi1jbGllbnQtcGVybWlzc2lvbnMvPgoJPC9zZWN1cml0eT4KICAgIDxhcHBsZXQtZGVzYyAKICAgICAgICAgbmFtZT0iUHJpbnRlciBBcHBsZXQiCiAgICAgICAgIG1haW4tY2xhc3M9ImNvbS5jYXBnZW1pbmkubnNkLnByaW50LlRleHRQcmludGVyQXBwbGV0LmNsYXNzIgogICAgICAgICB3aWR0aD0iNDAwIgogICAgICAgICBoZWlnaHQ9IjMwIj4KICAgICA8L2FwcGxldC1kZXNjPgogICAgIDx1cGRhdGUgY2hlY2s9ImJhY2tncm91bmQiLz4KPC9qbmxwPg=='
	    };
	
	var version = '1.7' ;
	deployJava.runApplet(attributes, parameters, version);

	/*
	<?xml version="1.0" encoding="UTF-8"?>
	<!-- href attribute contains relative path; codebase attribute not specified -->
	<jnlp href="printer_applet.jnlp">
	    <information>
	        <title>Printer Applet</title>
	        <vendor>MEF -RGS </vendor>
	    </information>
	    <resources>
	        <!-- Application Resources -->
	        <j2se version="1.7+" />
	        <jar href="../printapplet/printApplet.jar" main="true" />
	    </resources>
	    <security>
	 	 	<j2ee-application-client-permissions/>
		</security>
	    <applet-desc 
	         name="Printer Applet"
	         main-class="com.capgemini.nsd.print.TextPrinterApplet.class"
	         width="400"
	         height="30">
	     </applet-desc>
	     <update check="background"/>
	</jnlp>
	*/
}