var schedulerCredits;
var speedCredits;
var creditsDivHeight;
var creditsContainerDivHeight;
var creditsDiv;

function scrollCredits(){
	if (parseInt(creditsDiv.style.top)>(8-creditsDivHeight)) {
		creditsDiv.style.top=parseInt(creditsDiv.style.top)-speedCredits+"px"
	} else {
		creditsDiv.style.top=8+parseInt(creditsContainerDivHeight)+"px"
	}
}

function initCredits(){
	creditsDiv=document.getElementById("credits");
	creditsDiv.style.top=0;
	creditsContainerDivHeight=document.getElementById("creditsContainer").offsetHeight;
	creditsDivHeight=creditsDiv.offsetHeight;
	playCredits();schedulerCredits = setInterval(scrollCredits, 30);
}

function pauseCredits(){
	creditsDiv=document.getElementById("credits");
	creditsDiv.style.top=0;
	speedCredits=0;
	document.getElementById("creditsContainer").style.transform = "none";
	document.getElementById("creditsContainer").style.overflowY = "auto";
	document.getElementById("creditsContainer").style.top = "0px";
}

function playCredits(){
	speedCredits=1;
	document.getElementById("creditsContainer").style.transform = "perspective(300px) rotateX(25deg)";
	document.getElementById("creditsContainer").style.overflowY = "hidden";
	document.getElementById("creditsContainer").style.top = "-50px";
}

function toggleCredits(){
	if (speedCredits==0) {
		playCredits();
	} else {
		pauseCredits();
	}
}

function stopCredits(){
	pauseCredits();
	clearInterval(schedulerCredits);
}