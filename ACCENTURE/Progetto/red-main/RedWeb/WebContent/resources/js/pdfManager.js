/**
 * Per gestire il problema di Internet Explorer con il plugin di adobe
 * ho dovuto mettere: 
 * - in ogni overlayPanel un  <iframe class="cover" src="about:blank"></iframe> prima della chiusura del tag,
 * - in ogni dialog una funzione js sull'onShow che nel caso non sia già presente l'iframe lo inserisca prima 
 * 	della chiusura del tag. Ho dovuto farlo con una funzione js perché altrimenti l'iframe veniva inserito nel 
 * 	content lasciando fuori l'header. Per sicurezza nella dialog di creazione/modifica documento l'ho dovuto 
 * 	aggiungere a mano perché si verifica un errore js non catchabile (SCRIPT65535: Invalid calling object),
 * - inoltre c'è un interval che si occupa di tutti i menu / growl / datepicker. Se necessario aggiungere
 * 	componenti da processare nella funzione managePdfNativePlugin.addIframeToDialog,
 * **/

function managePdfNativePlugin() {

	/** restituisce true se il browser utilizzato è IE ************ */
	var isMSIE = function() {
		var ua = window.navigator.userAgent;
		var msie = ua.indexOf("MSIE ");
		return (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./));
	}

	/** aggiunge un iframe ai componenti ui-menu, ui-growl ****************** */
	var addIframeToDialog = function() {
		// iframe da aggiungere al componente per portarlo in primo piano
		// rispetto ad un possibile plugin pdf di adobe
		var iframe = '<iframe class="cover" src="about:blank"></iframe>';

		// cerco tutti i componenti menù e per ognuno controllo che non abbia
		// già un iframe cover se non lo hanno lo aggiungo io
		$('.ui-menu').not(".pdfViewer").each(function(index) {
			if (this && $(this) && !$(this).find('.cover').length) {
				$(this).append(iframe);
			}
		});
		$('.ui-growl').each(function(index) {
			if (this && $(this) && !$(this).find('.cover').length) {
				$(this).append(iframe);
			}
		});
		$('.ui-datepicker').each(function(index) {
			if (this && $(this) && !$(this).find('.cover').length) {
				$(this).append(iframe);
			}
		});
		$('.ui-columntoggler').each(function(index) {
			if (this && $(this) && !$(this).find('.cover').length) {
				$(this).append(iframe);
			}
		});

	}

	if (!isMSIE()) {
		// se il browser non è IE il problema dei pdf non si verifica
		return;
	}

	addIframeToDialog();
	setInterval(function() {
		addIframeToDialog();
	}, 2000);
}

(function() {
	$(document).ready(function() {
		try {
			if (nativeReaderPDFEnable) {
				// se NON c'è pdf.js
				managePdfNativePlugin();
			}
		} catch (e) {
			console.error(e);
		}
	});
})();

/**
 * Aggiungere ovunque sia nelle dialog che negli overlaypanel al trigger
 * dell'evento onShow la riga: dlgOnShow(this.jqId); 
 */
function dlgOnShow(jqId) {
	if (!nativeReaderPDFEnable) {
		// se c'è pdf.js non si pone il problema
		return;
	}
	try {
		if ($(jqId).length) {

			//per ogni figlio dell'elemento controllo che non ci sia l'iframe con la classe cover
			var iframeToAdd = true;
			$(jqId).children().each(function() {
				iframeToAdd = iframeToAdd && !$(this).hasClass( "cover" );
			});

			var iframe = '<iframe class="cover" src="about:blank"></iframe>';
			if (iframeToAdd) {
				$(jqId).append(iframe);
			}
		}
	} catch (e) {
		console.error('ERR', e);
	}
}


/**
 * return TRUE se il browser è IE e se il native reader è attivo
 * */
function isNativeReaderEnableAndIsIE() {
	var ua = window.navigator.userAgent;
	var msie = ua.indexOf("MSIE ");
	return (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) && nativeReaderPDFEnable;
}

/**
 * Da utilizzare SOLO per la dialog di CREAZIONE/MODIFICA
 * Serve per il problema di IE che se ingrandisci troppo il carattere ed una parte della dialog è sotto la scroll
 * quando fai scorrere la scroll e dietro la dialog c'è un pdf si crea un buco nella dialog con il pdf che si trova sotto. 
 * */
function dlgOnShowFixed(jqId) {
	if (!isNativeReaderEnableAndIsIE()) {
		// se c'è pdf.js non si pone il problema
		return;
	}
	try {
		if ($(jqId).length) {

			//per ogni figlio dell'elemento controllo che non ci sia l'iframe con la classe cover
			var iframeToAdd = true;
			$(jqId).children().each(function() {
				iframeToAdd = iframeToAdd && !$(this).hasClass( "cover" );
			});

			var iframe = '<iframe class="coverFixed" src="about:blank"></iframe>';
			if (iframeToAdd) {
				$(jqId).append(iframe);
			}
		}
	} catch (e) {
		console.error('ERR', e);
	}
}

