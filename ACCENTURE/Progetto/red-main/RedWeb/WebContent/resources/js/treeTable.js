function TreeTableUtilsConstructor() {
	/**
	 * 
	 * Aggancia all'elemento che fa scaturire l'evento collapse la callback
	 * 
	 * la callback prende come argomento l'event e l'elemento DOM in questo ordine
	 * 
	 * widget: nome del widget
	 * 
	 * message
	 * 
	 */
	this.onCollapse = function (callback, widget) {
			
		var wdg = PF(widget).jq;
		
		var allTr = wdg
			.find('tr')
			.each(function (index, element){
				var collapsable = $(element).find(".ui-treetable-toggler.ui-icon.ui-icon-triangle-1-s.ui-c")
				if(collapsable.length > 0){
					collapsable.click(element, callback);
				}
			});
	}

}

var TreeTableUtils = new TreeTableUtilsConstructor();

/**
 *  
 *  Impedisce la propagazione dell'evento ed esegue la funzione message
 *  
 */
function treeTableStopPropagationAndAlert (widget, message) {
	
	function cb (event) {
		var figli = PF(widget).jq.find("[id^='" + event.data.id + "_']");
		
		if (figli.find('.ui-icon-check').length > 0 || figli.find('.ui-icon-bullet').length > 0) {
			message();

			event.stopPropagation();
		}
	}
	
	TreeTableUtils.onCollapse(cb, widget);
}