
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraNotificaAzioneType;


/**
 * Dati di notifica di una azione asincrona sul protocollo (utilizzata sui servizi ponte UCB)
 * 
 * <p>Classe Java per wkf_datiNotificaAzione_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_datiNotificaAzione_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdMessaggio" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="IdentificativoProtocollo" type="{http://mef.gov.it.v1.npsTypes}identificativoProtocolloRequest_type"/>
 *         &lt;element name="DataAzione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="TipoAzione" type="{http://mef.gov.it.v1.npsTypes}wkf_tipoNotificaAzione_type"/>
 *         &lt;choice>
 *           &lt;element name="DatiCreazione" type="{http://mef.gov.it.v1.npsTypes}wkf_azioneCreazioneProtocollo_type"/>
 *           &lt;element name="DatiAggiornamento" type="{http://mef.gov.it.v1.npsTypes}wkf_azioneAggiornamentoProtocollo_type"/>
 *           &lt;element name="DatiAnnullamento" type="{http://mef.gov.it.v1.npsTypes}wkf_azioneAnnullamentoProtocollo_type"/>
 *           &lt;element name="DatiCambioStato" type="{http://mef.gov.it.v1.npsTypes}wkf_azioneCambioStatoProtocollo_type"/>
 *           &lt;element name="DatiRispondiA" type="{http://mef.gov.it.v1.npsTypes}wkf_azioneRispondiAProtocollo_type"/>
 *           &lt;element name="DatiAggiornamentoCollegati" type="{http://mef.gov.it.v1.npsTypes}wkf_azioneAggiornamentoCollegati_type"/>
 *           &lt;element name="DatiInserimentoDocumento" type="{http://mef.gov.it.v1.npsTypes}wkf_azioneInserimentoDocumentoProtocollo_type"/>
 *           &lt;element name="DatiRimozioneDocumento" type="{http://mef.gov.it.v1.npsTypes}wkf_azioneRimozioneDocumentoProtocollo_type"/>
 *           &lt;element name="DatiInserimentoDestinatario" type="{http://mef.gov.it.v1.npsTypes}wkf_azioneInserimentoDestinatarioProtocollo_type"/>
 *           &lt;element name="DatiRimozioneDestinatario" type="{http://mef.gov.it.v1.npsTypes}wkf_azioneRimozioneDestinatarioProtocollo_type"/>
 *           &lt;element name="DatiSpedizioneDestinatarioCartaceo" type="{http://mef.gov.it.v1.npsTypes}wkf_azioneSpedizioneDestinatarioCartaceoProtocollo_type"/>
 *           &lt;element name="DatiSpedizioneDestinatarioElettronico" type="{http://mef.gov.it.v1.npsTypes}wkf_azioneSpedizioneDestinatarioElettronicoProtocollo_type"/>
 *           &lt;element name="DatiAssegnazioneApplicazione" type="{http://mef.gov.it.v1.npsTypes}wkf_azioneAssegnazioneApplicazioneProtocollo_type"/>
 *           &lt;element name="DatiRimozioneAssegnazioneApplicazione" type="{http://mef.gov.it.v1.npsTypes}wkf_azioneRimozioneAssegnazioneApplicazioneProtocollo_type"/>
 *           &lt;element name="DatiCambioAssegnazioneApplicazioneProtocollo" type="{http://mef.gov.it.v1.npsTypes}wkf_azioneCambioAssegnazioneApplicazioneProtocollo_type"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_datiNotificaAzione_type", propOrder = {
    "idMessaggio",
    "identificativoProtocollo",
    "dataAzione",
    "tipoAzione",
    "datiCreazione",
    "datiAggiornamento",
    "datiAnnullamento",
    "datiCambioStato",
    "datiRispondiA",
    "datiAggiornamentoCollegati",
    "datiInserimentoDocumento",
    "datiRimozioneDocumento",
    "datiInserimentoDestinatario",
    "datiRimozioneDestinatario",
    "datiSpedizioneDestinatarioCartaceo",
    "datiSpedizioneDestinatarioElettronico",
    "datiAssegnazioneApplicazione",
    "datiRimozioneAssegnazioneApplicazione",
    "datiCambioAssegnazioneApplicazioneProtocollo"
})
@XmlSeeAlso({
    RichiestaElaboraNotificaAzioneType.class
})
public class WkfDatiNotificaAzioneType
    implements Serializable
{

    @XmlElement(name = "IdMessaggio", required = true)
    protected String idMessaggio;
    @XmlElement(name = "IdentificativoProtocollo", required = true)
    protected IdentificativoProtocolloRequestType identificativoProtocollo;
    @XmlElement(name = "DataAzione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataAzione;
    @XmlElement(name = "TipoAzione", required = true)
    @XmlSchemaType(name = "string")
    protected WkfTipoNotificaAzioneType tipoAzione;
    @XmlElement(name = "DatiCreazione")
    protected WkfAzioneCreazioneProtocolloType datiCreazione;
    @XmlElement(name = "DatiAggiornamento")
    protected WkfAzioneAggiornamentoProtocolloType datiAggiornamento;
    @XmlElement(name = "DatiAnnullamento")
    protected WkfAzioneAnnullamentoProtocolloType datiAnnullamento;
    @XmlElement(name = "DatiCambioStato")
    protected WkfAzioneCambioStatoProtocolloType datiCambioStato;
    @XmlElement(name = "DatiRispondiA")
    protected WkfAzioneRispondiAProtocolloType datiRispondiA;
    @XmlElement(name = "DatiAggiornamentoCollegati")
    protected WkfAzioneAggiornamentoCollegatiType datiAggiornamentoCollegati;
    @XmlElement(name = "DatiInserimentoDocumento")
    protected WkfAzioneInserimentoDocumentoProtocolloType datiInserimentoDocumento;
    @XmlElement(name = "DatiRimozioneDocumento")
    protected WkfAzioneRimozioneDocumentoProtocolloType datiRimozioneDocumento;
    @XmlElement(name = "DatiInserimentoDestinatario")
    protected WkfAzioneInserimentoDestinatarioProtocolloType datiInserimentoDestinatario;
    @XmlElement(name = "DatiRimozioneDestinatario")
    protected WkfAzioneRimozioneDestinatarioProtocolloType datiRimozioneDestinatario;
    @XmlElement(name = "DatiSpedizioneDestinatarioCartaceo")
    protected WkfAzioneSpedizioneDestinatarioCartaceoProtocolloType datiSpedizioneDestinatarioCartaceo;
    @XmlElement(name = "DatiSpedizioneDestinatarioElettronico")
    protected WkfAzioneSpedizioneDestinatarioElettronicoProtocolloType datiSpedizioneDestinatarioElettronico;
    @XmlElement(name = "DatiAssegnazioneApplicazione")
    protected WkfAzioneAssegnazioneApplicazioneProtocolloType datiAssegnazioneApplicazione;
    @XmlElement(name = "DatiRimozioneAssegnazioneApplicazione")
    protected WkfAzioneRimozioneAssegnazioneApplicazioneProtocolloType datiRimozioneAssegnazioneApplicazione;
    @XmlElement(name = "DatiCambioAssegnazioneApplicazioneProtocollo")
    protected WkfAzioneCambioAssegnazioneApplicazioneProtocolloType datiCambioAssegnazioneApplicazioneProtocollo;

    /**
     * Recupera il valore della proprietà idMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggio() {
        return idMessaggio;
    }

    /**
     * Imposta il valore della proprietà idMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggio(String value) {
        this.idMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà identificativoProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public IdentificativoProtocolloRequestType getIdentificativoProtocollo() {
        return identificativoProtocollo;
    }

    /**
     * Imposta il valore della proprietà identificativoProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public void setIdentificativoProtocollo(IdentificativoProtocolloRequestType value) {
        this.identificativoProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà dataAzione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAzione() {
        return dataAzione;
    }

    /**
     * Imposta il valore della proprietà dataAzione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAzione(XMLGregorianCalendar value) {
        this.dataAzione = value;
    }

    /**
     * Recupera il valore della proprietà tipoAzione.
     * 
     * @return
     *     possible object is
     *     {@link WkfTipoNotificaAzioneType }
     *     
     */
    public WkfTipoNotificaAzioneType getTipoAzione() {
        return tipoAzione;
    }

    /**
     * Imposta il valore della proprietà tipoAzione.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfTipoNotificaAzioneType }
     *     
     */
    public void setTipoAzione(WkfTipoNotificaAzioneType value) {
        this.tipoAzione = value;
    }

    /**
     * Recupera il valore della proprietà datiCreazione.
     * 
     * @return
     *     possible object is
     *     {@link WkfAzioneCreazioneProtocolloType }
     *     
     */
    public WkfAzioneCreazioneProtocolloType getDatiCreazione() {
        return datiCreazione;
    }

    /**
     * Imposta il valore della proprietà datiCreazione.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfAzioneCreazioneProtocolloType }
     *     
     */
    public void setDatiCreazione(WkfAzioneCreazioneProtocolloType value) {
        this.datiCreazione = value;
    }

    /**
     * Recupera il valore della proprietà datiAggiornamento.
     * 
     * @return
     *     possible object is
     *     {@link WkfAzioneAggiornamentoProtocolloType }
     *     
     */
    public WkfAzioneAggiornamentoProtocolloType getDatiAggiornamento() {
        return datiAggiornamento;
    }

    /**
     * Imposta il valore della proprietà datiAggiornamento.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfAzioneAggiornamentoProtocolloType }
     *     
     */
    public void setDatiAggiornamento(WkfAzioneAggiornamentoProtocolloType value) {
        this.datiAggiornamento = value;
    }

    /**
     * Recupera il valore della proprietà datiAnnullamento.
     * 
     * @return
     *     possible object is
     *     {@link WkfAzioneAnnullamentoProtocolloType }
     *     
     */
    public WkfAzioneAnnullamentoProtocolloType getDatiAnnullamento() {
        return datiAnnullamento;
    }

    /**
     * Imposta il valore della proprietà datiAnnullamento.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfAzioneAnnullamentoProtocolloType }
     *     
     */
    public void setDatiAnnullamento(WkfAzioneAnnullamentoProtocolloType value) {
        this.datiAnnullamento = value;
    }

    /**
     * Recupera il valore della proprietà datiCambioStato.
     * 
     * @return
     *     possible object is
     *     {@link WkfAzioneCambioStatoProtocolloType }
     *     
     */
    public WkfAzioneCambioStatoProtocolloType getDatiCambioStato() {
        return datiCambioStato;
    }

    /**
     * Imposta il valore della proprietà datiCambioStato.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfAzioneCambioStatoProtocolloType }
     *     
     */
    public void setDatiCambioStato(WkfAzioneCambioStatoProtocolloType value) {
        this.datiCambioStato = value;
    }

    /**
     * Recupera il valore della proprietà datiRispondiA.
     * 
     * @return
     *     possible object is
     *     {@link WkfAzioneRispondiAProtocolloType }
     *     
     */
    public WkfAzioneRispondiAProtocolloType getDatiRispondiA() {
        return datiRispondiA;
    }

    /**
     * Imposta il valore della proprietà datiRispondiA.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfAzioneRispondiAProtocolloType }
     *     
     */
    public void setDatiRispondiA(WkfAzioneRispondiAProtocolloType value) {
        this.datiRispondiA = value;
    }

    /**
     * Recupera il valore della proprietà datiAggiornamentoCollegati.
     * 
     * @return
     *     possible object is
     *     {@link WkfAzioneAggiornamentoCollegatiType }
     *     
     */
    public WkfAzioneAggiornamentoCollegatiType getDatiAggiornamentoCollegati() {
        return datiAggiornamentoCollegati;
    }

    /**
     * Imposta il valore della proprietà datiAggiornamentoCollegati.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfAzioneAggiornamentoCollegatiType }
     *     
     */
    public void setDatiAggiornamentoCollegati(WkfAzioneAggiornamentoCollegatiType value) {
        this.datiAggiornamentoCollegati = value;
    }

    /**
     * Recupera il valore della proprietà datiInserimentoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link WkfAzioneInserimentoDocumentoProtocolloType }
     *     
     */
    public WkfAzioneInserimentoDocumentoProtocolloType getDatiInserimentoDocumento() {
        return datiInserimentoDocumento;
    }

    /**
     * Imposta il valore della proprietà datiInserimentoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfAzioneInserimentoDocumentoProtocolloType }
     *     
     */
    public void setDatiInserimentoDocumento(WkfAzioneInserimentoDocumentoProtocolloType value) {
        this.datiInserimentoDocumento = value;
    }

    /**
     * Recupera il valore della proprietà datiRimozioneDocumento.
     * 
     * @return
     *     possible object is
     *     {@link WkfAzioneRimozioneDocumentoProtocolloType }
     *     
     */
    public WkfAzioneRimozioneDocumentoProtocolloType getDatiRimozioneDocumento() {
        return datiRimozioneDocumento;
    }

    /**
     * Imposta il valore della proprietà datiRimozioneDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfAzioneRimozioneDocumentoProtocolloType }
     *     
     */
    public void setDatiRimozioneDocumento(WkfAzioneRimozioneDocumentoProtocolloType value) {
        this.datiRimozioneDocumento = value;
    }

    /**
     * Recupera il valore della proprietà datiInserimentoDestinatario.
     * 
     * @return
     *     possible object is
     *     {@link WkfAzioneInserimentoDestinatarioProtocolloType }
     *     
     */
    public WkfAzioneInserimentoDestinatarioProtocolloType getDatiInserimentoDestinatario() {
        return datiInserimentoDestinatario;
    }

    /**
     * Imposta il valore della proprietà datiInserimentoDestinatario.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfAzioneInserimentoDestinatarioProtocolloType }
     *     
     */
    public void setDatiInserimentoDestinatario(WkfAzioneInserimentoDestinatarioProtocolloType value) {
        this.datiInserimentoDestinatario = value;
    }

    /**
     * Recupera il valore della proprietà datiRimozioneDestinatario.
     * 
     * @return
     *     possible object is
     *     {@link WkfAzioneRimozioneDestinatarioProtocolloType }
     *     
     */
    public WkfAzioneRimozioneDestinatarioProtocolloType getDatiRimozioneDestinatario() {
        return datiRimozioneDestinatario;
    }

    /**
     * Imposta il valore della proprietà datiRimozioneDestinatario.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfAzioneRimozioneDestinatarioProtocolloType }
     *     
     */
    public void setDatiRimozioneDestinatario(WkfAzioneRimozioneDestinatarioProtocolloType value) {
        this.datiRimozioneDestinatario = value;
    }

    /**
     * Recupera il valore della proprietà datiSpedizioneDestinatarioCartaceo.
     * 
     * @return
     *     possible object is
     *     {@link WkfAzioneSpedizioneDestinatarioCartaceoProtocolloType }
     *     
     */
    public WkfAzioneSpedizioneDestinatarioCartaceoProtocolloType getDatiSpedizioneDestinatarioCartaceo() {
        return datiSpedizioneDestinatarioCartaceo;
    }

    /**
     * Imposta il valore della proprietà datiSpedizioneDestinatarioCartaceo.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfAzioneSpedizioneDestinatarioCartaceoProtocolloType }
     *     
     */
    public void setDatiSpedizioneDestinatarioCartaceo(WkfAzioneSpedizioneDestinatarioCartaceoProtocolloType value) {
        this.datiSpedizioneDestinatarioCartaceo = value;
    }

    /**
     * Recupera il valore della proprietà datiSpedizioneDestinatarioElettronico.
     * 
     * @return
     *     possible object is
     *     {@link WkfAzioneSpedizioneDestinatarioElettronicoProtocolloType }
     *     
     */
    public WkfAzioneSpedizioneDestinatarioElettronicoProtocolloType getDatiSpedizioneDestinatarioElettronico() {
        return datiSpedizioneDestinatarioElettronico;
    }

    /**
     * Imposta il valore della proprietà datiSpedizioneDestinatarioElettronico.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfAzioneSpedizioneDestinatarioElettronicoProtocolloType }
     *     
     */
    public void setDatiSpedizioneDestinatarioElettronico(WkfAzioneSpedizioneDestinatarioElettronicoProtocolloType value) {
        this.datiSpedizioneDestinatarioElettronico = value;
    }

    /**
     * Recupera il valore della proprietà datiAssegnazioneApplicazione.
     * 
     * @return
     *     possible object is
     *     {@link WkfAzioneAssegnazioneApplicazioneProtocolloType }
     *     
     */
    public WkfAzioneAssegnazioneApplicazioneProtocolloType getDatiAssegnazioneApplicazione() {
        return datiAssegnazioneApplicazione;
    }

    /**
     * Imposta il valore della proprietà datiAssegnazioneApplicazione.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfAzioneAssegnazioneApplicazioneProtocolloType }
     *     
     */
    public void setDatiAssegnazioneApplicazione(WkfAzioneAssegnazioneApplicazioneProtocolloType value) {
        this.datiAssegnazioneApplicazione = value;
    }

    /**
     * Recupera il valore della proprietà datiRimozioneAssegnazioneApplicazione.
     * 
     * @return
     *     possible object is
     *     {@link WkfAzioneRimozioneAssegnazioneApplicazioneProtocolloType }
     *     
     */
    public WkfAzioneRimozioneAssegnazioneApplicazioneProtocolloType getDatiRimozioneAssegnazioneApplicazione() {
        return datiRimozioneAssegnazioneApplicazione;
    }

    /**
     * Imposta il valore della proprietà datiRimozioneAssegnazioneApplicazione.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfAzioneRimozioneAssegnazioneApplicazioneProtocolloType }
     *     
     */
    public void setDatiRimozioneAssegnazioneApplicazione(WkfAzioneRimozioneAssegnazioneApplicazioneProtocolloType value) {
        this.datiRimozioneAssegnazioneApplicazione = value;
    }

    /**
     * Recupera il valore della proprietà datiCambioAssegnazioneApplicazioneProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link WkfAzioneCambioAssegnazioneApplicazioneProtocolloType }
     *     
     */
    public WkfAzioneCambioAssegnazioneApplicazioneProtocolloType getDatiCambioAssegnazioneApplicazioneProtocollo() {
        return datiCambioAssegnazioneApplicazioneProtocollo;
    }

    /**
     * Imposta il valore della proprietà datiCambioAssegnazioneApplicazioneProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfAzioneCambioAssegnazioneApplicazioneProtocolloType }
     *     
     */
    public void setDatiCambioAssegnazioneApplicazioneProtocollo(WkfAzioneCambioAssegnazioneApplicazioneProtocolloType value) {
        this.datiCambioAssegnazioneApplicazioneProtocollo = value;
    }

}
