//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.01.28 at 06:39:59 PM CET 
//


package it.ibm.red.webservice.model.documentservice.types.messages;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for redConservaDocumentoResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="redConservaDocumentoResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://messages.types.documentservice.model.webservice.red.ibm.it}BaseResponseType">
 *       &lt;sequence>
 *         &lt;element name="codiceErrore" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="dataAcquisizione" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="dataConservazione" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="descrizioneErrore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descrizioneStato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idApplicativo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idConservazione" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idTicket" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="nomeDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="stato" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="uuidDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "redConservaDocumentoResponseType", propOrder = {
    "codiceErrore",
    "dataAcquisizione",
    "dataConservazione",
    "descrizioneErrore",
    "descrizioneStato",
    "idApplicativo",
    "idConservazione",
    "idTicket",
    "nomeDocumento",
    "stato",
    "uuidDocumento"
})
public class RedConservaDocumentoResponseType extends BaseResponseType {

	/**
	 * Codice errore.
	 */
    protected Integer codiceErrore;

	/**
	 * Data acquisizione.
	 */
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataAcquisizione;

    /**
     * Data conservazione.
     */
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataConservazione;

	/**
	 * Descrizione errore.
	 */
    protected String descrizioneErrore;
    
	/**
	 * Descrizione stato.
	 */
    protected String descrizioneStato;

	/**
	 * Identificativo applicativo.
	 */
    protected String idApplicativo;
    
	/**
	 * Identificativo conservazione.
	 */
    protected Integer idConservazione;
    
	/**
	 * Identificativo ticket.
	 */
    protected Long idTicket;

	/**
	 * Nome documento.
	 */
    protected String nomeDocumento;
    
	/**
	 * Stato.
	 */
    protected Integer stato;

	/**
	 * Uuid documento.
	 */
    protected String uuidDocumento;

    /**
     * Gets the value of the codiceErrore property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodiceErrore() {
        return codiceErrore;
    }

    /**
     * Sets the value of the codiceErrore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodiceErrore(Integer value) {
        this.codiceErrore = value;
    }

    /**
     * Gets the value of the dataAcquisizione property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAcquisizione() {
        return dataAcquisizione;
    }

    /**
     * Sets the value of the dataAcquisizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAcquisizione(XMLGregorianCalendar value) {
        this.dataAcquisizione = value;
    }

    /**
     * Gets the value of the dataConservazione property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataConservazione() {
        return dataConservazione;
    }

    /**
     * Sets the value of the dataConservazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataConservazione(XMLGregorianCalendar value) {
        this.dataConservazione = value;
    }

    /**
     * Gets the value of the descrizioneErrore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizioneErrore() {
        return descrizioneErrore;
    }

    /**
     * Sets the value of the descrizioneErrore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizioneErrore(String value) {
        this.descrizioneErrore = value;
    }

    /**
     * Gets the value of the descrizioneStato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizioneStato() {
        return descrizioneStato;
    }

    /**
     * Sets the value of the descrizioneStato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizioneStato(String value) {
        this.descrizioneStato = value;
    }

    /**
     * Gets the value of the idApplicativo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdApplicativo() {
        return idApplicativo;
    }

    /**
     * Sets the value of the idApplicativo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdApplicativo(String value) {
        this.idApplicativo = value;
    }

    /**
     * Gets the value of the idConservazione property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdConservazione() {
        return idConservazione;
    }

    /**
     * Sets the value of the idConservazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdConservazione(Integer value) {
        this.idConservazione = value;
    }

    /**
     * Gets the value of the idTicket property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdTicket() {
        return idTicket;
    }

    /**
     * Sets the value of the idTicket property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdTicket(Long value) {
        this.idTicket = value;
    }

    /**
     * Gets the value of the nomeDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeDocumento() {
        return nomeDocumento;
    }

    /**
     * Sets the value of the nomeDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeDocumento(String value) {
        this.nomeDocumento = value;
    }

    /**
     * Gets the value of the stato property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStato() {
        return stato;
    }

    /**
     * Sets the value of the stato property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStato(Integer value) {
        this.stato = value;
    }

    /**
     * Gets the value of the uuidDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuidDocumento() {
        return uuidDocumento;
    }

    /**
     * Sets the value of the uuidDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuidDocumento(String value) {
        this.uuidDocumento = value;
    }

}
