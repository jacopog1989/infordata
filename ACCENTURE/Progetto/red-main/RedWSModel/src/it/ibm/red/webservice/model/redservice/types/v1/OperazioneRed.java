package it.ibm.red.webservice.model.redservice.types.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for OperazioneRed complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="OperazioneRed">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tipoOperazione" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OperazioneRed", propOrder = {
    "tipoOperazione"
})
public class OperazioneRed {

	/**
	 * tipo operazione.
	 */
    protected Integer tipoOperazione;

    /**
     * Gets the value of the tipoOperazione property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     */
    public Integer getTipoOperazione() {
        return tipoOperazione;
    }

    /**
     * Sets the value of the tipoOperazione property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     */
    public void setTipoOperazione(final Integer value) {
        this.tipoOperazione = value;
    }
}
