
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Dati per la notifica della creazione del protocollo
 * 
 * <p>Classe Java per wkf_azioneCreazioneProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_azioneCreazioneProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentificatoreProtocollo" type="{http://mef.gov.it.v1.npsTypes}prot_identificatoreProtocollo_type"/>
 *         &lt;element name="TipologiaDocumento" type="{http://mef.gov.it.v1.npsTypes}prot_attributiEstesi_type"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SistemaAusiliario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_azioneCreazioneProtocollo_type", propOrder = {
    "identificatoreProtocollo",
    "tipologiaDocumento",
    "sistemaProduttore",
    "sistemaAusiliario"
})
public class WkfAzioneCreazioneProtocolloType
    implements Serializable
{

    @XmlElement(name = "IdentificatoreProtocollo", required = true)
    protected ProtIdentificatoreProtocolloType identificatoreProtocollo;
    @XmlElement(name = "TipologiaDocumento", required = true)
    protected ProtAttributiEstesiType tipologiaDocumento;
    @XmlElement(name = "SistemaProduttore", required = true)
    protected String sistemaProduttore;
    @XmlElement(name = "SistemaAusiliario")
    protected String sistemaAusiliario;

    /**
     * Recupera il valore della proprietà identificatoreProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public ProtIdentificatoreProtocolloType getIdentificatoreProtocollo() {
        return identificatoreProtocollo;
    }

    /**
     * Imposta il valore della proprietà identificatoreProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public void setIdentificatoreProtocollo(ProtIdentificatoreProtocolloType value) {
        this.identificatoreProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà tipologiaDocumento.
     * 
     * @return
     *     possible object is
     *     {@link ProtAttributiEstesiType }
     *     
     */
    public ProtAttributiEstesiType getTipologiaDocumento() {
        return tipologiaDocumento;
    }

    /**
     * Imposta il valore della proprietà tipologiaDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtAttributiEstesiType }
     *     
     */
    public void setTipologiaDocumento(ProtAttributiEstesiType value) {
        this.tipologiaDocumento = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

    /**
     * Recupera il valore della proprietà sistemaAusiliario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaAusiliario() {
        return sistemaAusiliario;
    }

    /**
     * Imposta il valore della proprietà sistemaAusiliario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaAusiliario(String value) {
        this.sistemaAusiliario = value;
    }

}
