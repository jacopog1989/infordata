
package it.ibm.red.webservice.model.redservice.types.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ClasseDocumentaleRed complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClasseDocumentaleRed">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="metadato" type="{urn:red:types:v1}MetadatoRed" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="nomeClasse" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClasseDocumentaleRed", propOrder = {
    "metadato",
    "nomeClasse",
    "parent"
})
public class ClasseDocumentaleRed {

    /**
     * Metadato.
     */
    @XmlElement(nillable = true)
    protected List<MetadatoRed> metadato;

    /**
     * Nome della classe.
     */
    @XmlElementRef(name = "nomeClasse", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nomeClasse;

    /**
     * Padre della classe.
     */
    @XmlElementRef(name = "parent", type = JAXBElement.class, required = false)
    protected JAXBElement<String> parent;

    /**
     * Gets the value of the metadato property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metadato property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetadato().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MetadatoRed }
     * 
     * 
     */
    public List<MetadatoRed> getMetadato() {
        if (metadato == null) {
            metadato = new ArrayList<>();
        }
        return this.metadato;
    }

    /**
     * Gets the value of the nomeClasse property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNomeClasse() {
        return nomeClasse;
    }

    /**
     * Sets the value of the nomeClasse property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNomeClasse(JAXBElement<String> value) {
        this.nomeClasse = value;
    }

    /**
     * Gets the value of the parent property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParent() {
        return parent;
    }

    /**
     * Sets the value of the parent property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParent(JAXBElement<String> value) {
        this.parent = value;
    }

}
