package it.ibm.red.webservice.model.interfaccianps.dto;

import java.util.Date;

public class ParametersElaboraNotificaOperazione extends ParametersElaboraNotificaNPS {
	
    
	/**
	 * Id messaggio NPS.
	 */
	private String idMessaggioNPS;
	
	/**
	 * Id messaggio mail.
	 */
	private String messageIdMail;
	
	/**
	 * Codice AOO.
	 */
	private String codiceAooNPS;
	
	/**
	 * Numero protocollo.
	 */
	private int numeroProtocollo;
	
	/**
	 * Data protocollo.
	 */
	private Date dataProtocollo;
	
	/**
	 * Anno protocollo.
	 */
	private int annoProtocollo;
	
	/**
	 * Flag entrata.
	 */
	private boolean isEntrata;
	
	/**
	 * Id protocollo.
	 */
	private String idProtocollo;
	
	/**
	 * Data notifica.
	 */
	private Date dataNotifica;
	
	/**
	 * Tipo notifica.
	 */
	private String tipoNotifica;
	
	/**
	 * Esito notifica.
	 */
	private String esitoNotifica;
	
	/**
	 * Email destinatario.
	 */
	private String emailDestinatario;
	
	/**
	 * Costruttore.
	 * @param messageIdNPS
	 */
	public ParametersElaboraNotificaOperazione(String messageIdNPS) {
		super(messageIdNPS);
	}

	/**
	 * Restituisce l'id del message NPS.
	 * @return id del message NPS
	 */
	public String getIdMessaggioNPS() {
		return idMessaggioNPS;
	}

	/**
	 * Imposta l'id del message NPS.
	 * @param idMessaggioNPS
	 */
	public void setIdMessaggioNPS(String idMessaggioNPS) {
		this.idMessaggioNPS = idMessaggioNPS;
	}

	/**
	 * Restituisce il message id Mail.
	 * @return message id Mail
	 */
	public String getMessageIdMail() {
		return messageIdMail;
	}

	/**
	 * Imposta il message id Mail.
	 * @param messageIdMail
	 */
	public void setMessageIdMail(String messageIdMail) {
		this.messageIdMail = messageIdMail;
	}

	/**
	 * Restituisce il codice AOO NPS.
	 * @return codice AOO NPS
	 */
	public String getCodiceAooNPS() {
		return codiceAooNPS;
	}

	/**
	 * Imposta il codice AOO NPS.
	 * @param codiceAooNPS
	 */
	public void setCodiceAooNPS(String codiceAooNPS) {
		this.codiceAooNPS = codiceAooNPS;
	}

	/**
	 * Restituisce il numero protocollo.
	 * @return numero protocollo
	 */
	public int getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Imposta il numero protocollo.
	 * @param numeroProtocollo
	 */
	public void setNumeroProtocollo(int numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	/**
	 * Restituisce la data protocollo.
	 * @return data protocollo
	 */
	public Date getDataProtocollo() {
		return dataProtocollo;
	}

	/**
	 * Imposta la data protocollo.
	 * @param dataProtocollo
	 */
	public void setDataProtocollo(Date dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}

	/**
	 * Restituisce l'anno protocollo.
	 * @return anno protocollo
	 */
	public int getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * Imposta l'anno protocollo.
	 * @param annoProtocollo
	 */
	public void setAnnoProtocollo(int annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/**
	 * Restituisce il flag associato all'entrata.
	 * @return flag associato all'entrata
	 */
	public boolean isEntrata() {
		return isEntrata;
	}

	/**
	 * Imposta il flag associato all'entrata.
	 * @param isEntrata
	 */
	public void setEntrata(boolean isEntrata) {
		this.isEntrata = isEntrata;
	}

	/**
	 * Restituisce l'id del protocollo.
	 * @return id del protocollo
	 */
	public String getIdProtocollo() {
		return idProtocollo;
	}

	/**
	 * Imposta l'id del protocollo.
	 * @param idProtocollo
	 */
	public void setIdProtocollo(String idProtocollo) {
		this.idProtocollo = idProtocollo;
	}

	/**
	 * Restituisce la data notifica.
	 * @return data notifica
	 */
	public Date getDataNotifica() {
		return dataNotifica;
	}

	/**
	 * Imposta la data notifica.
	 * @param dataNotifica
	 */
	public void setDataNotifica(Date dataNotifica) {
		this.dataNotifica = dataNotifica;
	}

	/**
	 * Restituisce il tipo notifica.
	 * @return tipo notifica
	 */
	public String getTipoNotifica() {
		return tipoNotifica;
	}

	/**
	 * Imposta il tipo notifica.
	 * @param tipoNotifica
	 */
	public void setTipoNotifica(String tipoNotifica) {
		this.tipoNotifica = tipoNotifica;
	}

	/**
	 * Restituisce l'esito notifica.
	 * @return esito notifica
	 */
	public String getEsitoNotifica() {
		return esitoNotifica;
	}

	/**
	 * Imposta l'esito notifica.
	 * @param esitoNotifica
	 */
	public void setEsitoNotifica(String esitoNotifica) {
		this.esitoNotifica = esitoNotifica;
	}
	
	/**
	 * Restituisce la mail destinatario.
	 * @return mail destinatario
	 */
	public String getEmailDestinatario() {
		return emailDestinatario;
	}

	/**
	 * Imposta la mail destinatario.
	 * @param emailDestinatario
	 */
	public void setEmailDestinatario(String emailDestinatario) {
		this.emailDestinatario = emailDestinatario;
	}

}
