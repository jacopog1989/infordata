
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce il tipo di dato CasellaEmail
 * 
 * <p>Classe Java per org_casellaEmail_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="org_casellaEmail_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Aoo" type="{http://mef.gov.it.v1.npsTypes}org_aoo_type"/>
 *         &lt;element name="UnitaOrganizzativa" type="{http://mef.gov.it.v1.npsTypes}org_unitaOrganizzativa_type" minOccurs="0"/>
 *         &lt;element name="IndirizzoEmail" type="{http://mef.gov.it.v1.npsTypes}email_indirizzoEmail_type"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Ufficiale">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;pattern value="si"/>
 *             &lt;pattern value="no"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="IsPEC">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="si"/>
 *             &lt;enumeration value="no"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "org_casellaEmail_type", propOrder = {
    "aoo",
    "unitaOrganizzativa",
    "indirizzoEmail"
})
public class OrgCasellaEmailType
    implements Serializable
{

    @XmlElement(name = "Aoo", required = true)
    protected OrgAooType aoo;
    @XmlElement(name = "UnitaOrganizzativa")
    protected OrgUnitaOrganizzativaType unitaOrganizzativa;
    @XmlElement(name = "IndirizzoEmail", required = true)
    protected EmailIndirizzoEmailType indirizzoEmail;
    @XmlAttribute(name = "Ufficiale")
    protected String ufficiale;
    @XmlAttribute(name = "IsPEC")
    protected String isPEC;

    /**
     * Recupera il valore della proprietà aoo.
     * 
     * @return
     *     possible object is
     *     {@link OrgAooType }
     *     
     */
    public OrgAooType getAoo() {
        return aoo;
    }

    /**
     * Imposta il valore della proprietà aoo.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgAooType }
     *     
     */
    public void setAoo(OrgAooType value) {
        this.aoo = value;
    }

    /**
     * Recupera il valore della proprietà unitaOrganizzativa.
     * 
     * @return
     *     possible object is
     *     {@link OrgUnitaOrganizzativaType }
     *     
     */
    public OrgUnitaOrganizzativaType getUnitaOrganizzativa() {
        return unitaOrganizzativa;
    }

    /**
     * Imposta il valore della proprietà unitaOrganizzativa.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgUnitaOrganizzativaType }
     *     
     */
    public void setUnitaOrganizzativa(OrgUnitaOrganizzativaType value) {
        this.unitaOrganizzativa = value;
    }

    /**
     * Recupera il valore della proprietà indirizzoEmail.
     * 
     * @return
     *     possible object is
     *     {@link EmailIndirizzoEmailType }
     *     
     */
    public EmailIndirizzoEmailType getIndirizzoEmail() {
        return indirizzoEmail;
    }

    /**
     * Imposta il valore della proprietà indirizzoEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailIndirizzoEmailType }
     *     
     */
    public void setIndirizzoEmail(EmailIndirizzoEmailType value) {
        this.indirizzoEmail = value;
    }

    /**
     * Recupera il valore della proprietà ufficiale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUfficiale() {
        return ufficiale;
    }

    /**
     * Imposta il valore della proprietà ufficiale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUfficiale(String value) {
        this.ufficiale = value;
    }

    /**
     * Recupera il valore della proprietà isPEC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPEC() {
        return isPEC;
    }

    /**
     * Imposta il valore della proprietà isPEC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPEC(String value) {
        this.isPEC = value;
    }

}
