package it.ibm.red.webservice.model.interfaccianps.dto;

import java.util.Date;

public class ParametersElaboraMessaggioProtocollazioneEmergenza extends ParametersElaboraNotificaNPS {
	
	/**
	 * Message id.
	 */
	private String idMessaggioNPS;

	/**
	 * Codice aoo.
	 */
	private String codiceAooNPS;
	
	/**
	 * Numero protocollo emergenza.
	 */
	private int numeroProtocolloEmergenza;
	
	/**
	 * Data protocollo emergenza.
	 */
	private Date dataProtocolloEmergenza;
	
	/**
	 * Anno protocollo emergenza.
	 */
	private int annoProtocolloEmergenza;
	
	/**
	 * Flag entrata.
	 */
	private boolean isEntrata;
	
	/**
	 * Id protocollo ufficale.
	 */
	private String idProtocolloUfficiale;
	
	/**
	 * Costruttore.
	 * @param messageIdNPS
	 */
	public ParametersElaboraMessaggioProtocollazioneEmergenza(String messageIdNPS) {
		super(messageIdNPS);
	}

	/**
	 * Restituisce l'id del messaggio.
	 * @return id del messaggio
	 */
	public String getIdMessaggioNPS() {
		return idMessaggioNPS;
	}

	/**
	 * Imposta l'iid del messaggio.
	 * @param idMessaggioNPS
	 */
	public void setIdMessaggioNPS(String idMessaggioNPS) {
		this.idMessaggioNPS = idMessaggioNPS;
	}

	/**
	 * Restituisce il codice AOO NPS.
	 * @return codice AOO NPS
	 */
	public String getCodiceAooNPS() {
		return codiceAooNPS;
	}

	/**
	 * Imposta il codice AOO NPS.
	 * @param codiceAooNPS
	 */
	public void setCodiceAooNPS(String codiceAooNPS) {
		this.codiceAooNPS = codiceAooNPS;
	}

	/**
	 * Restituisce il numero protocollo emergenza.
	 * @return numero protocollo emergenza
	 */
	public int getNumeroProtocolloEmergenza() {
		return numeroProtocolloEmergenza;
	}

	/**
	 * Imposta il numero protocollo emergenza.
	 * @param numeroProtocolloEmergenza
	 */
	public void setNumeroProtocolloEmergenza(int numeroProtocolloEmergenza) {
		this.numeroProtocolloEmergenza = numeroProtocolloEmergenza;
	}

	/**
	 * Restituisce la data protocollo emergenza.
	 * @return data protocollo emergenza
	 */
	public Date getDataProtocolloEmergenza() {
		return dataProtocolloEmergenza;
	}

	/**
	 * Imposta la data protocollo emergenza.
	 * @param dataProtocolloEmergenza
	 */
	public void setDataProtocolloEmergenza(Date dataProtocolloEmergenza) {
		this.dataProtocolloEmergenza = dataProtocolloEmergenza;
	}

	/**
	 * Restituisce l'anno protocollo emergenza.
	 * @return anno protocollo emergenza
	 */
	public int getAnnoProtocolloEmergenza() {
		return annoProtocolloEmergenza;
	}

	/**
	 * Imposta l'anno protocollo emergenza.
	 * @param annoProtocolloEmergenza
	 */
	public void setAnnoProtocolloEmergenza(int annoProtocolloEmergenza) {
		this.annoProtocolloEmergenza = annoProtocolloEmergenza;
	}
	
	/**
	 * Restituisce il flag associato all'entrata.
	 * @return flag associato all'entrata
	 */
	public boolean isEntrata() {
		return isEntrata;
	}

	/**
	 * Imposta il flag associato all'entrata.
	 * @param isEntrata
	 */
	public void setEntrata(boolean isEntrata) {
		this.isEntrata = isEntrata;
	}

	/**
	 * Restituisce l'id del protocollo ufficiale.
	 * @return id del protocollo ufficiale
	 */
	public String getIdProtocolloUfficiale() {
		return idProtocolloUfficiale;
	}

	/**
	 * Imposta l'id del protocollo ufficiale.
	 * @param idProtocolloUfficiale
	 */
	public void setIdProtocolloUfficiale(String idProtocolloUfficiale) {
		this.idProtocolloUfficiale = idProtocolloUfficiale;
	}
	
}