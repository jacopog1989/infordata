
package it.ibm.red.webservice.model.interfaccianps.npsmessages;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo di dato restituito a seguito della richiesta l'elaborazione dell'errore per il  messaggio del flusso
 * 
 * <p>Classe Java per risposta_elaboraNotifica_Operazione_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_elaboraNotifica_Operazione_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}baseServiceResponse_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_elaboraNotifica_Operazione_type")
public class RispostaElaboraNotificaOperazioneType
    extends BaseServiceResponseType
    implements Serializable
{


}
