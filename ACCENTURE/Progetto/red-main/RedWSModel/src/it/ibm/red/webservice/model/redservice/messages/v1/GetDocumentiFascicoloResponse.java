
package it.ibm.red.webservice.model.redservice.messages.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import it.ibm.red.webservice.model.redservice.types.v1.DocumentoRed;
import it.ibm.red.webservice.model.redservice.types.v1.FascicoloRed;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="documenti" type="{urn:red:types:v1}DocumentoRed" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="fascicoli" type="{urn:red:types:v1}FascicoloRed" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "documenti",
    "fascicoli"
})
@XmlRootElement(name = "getDocumentiFascicoloResponse")
public class GetDocumentiFascicoloResponse {


    /**
     * Documenti.
     */
    @XmlElement(nillable = true)
    protected List<DocumentoRed> documenti;

    /**
     * Fascicoli.
     */
    @XmlElement(nillable = true)
    protected List<FascicoloRed> fascicoli;

    /**
     * Gets the value of the documenti property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documenti property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumenti().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentoRed }
     * 
     * 
     */
    public List<DocumentoRed> getDocumenti() {
        if (documenti == null) {
            documenti = new ArrayList<>();
        }
        return this.documenti;
    }

    /**
     * Gets the value of the fascicoli property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fascicoli property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFascicoli().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FascicoloRed }
     * 
     * 
     */
    public List<FascicoloRed> getFascicoli() {
        if (fascicoli == null) {
            fascicoli = new ArrayList<>();
        }
        return this.fascicoli;
    }

}
