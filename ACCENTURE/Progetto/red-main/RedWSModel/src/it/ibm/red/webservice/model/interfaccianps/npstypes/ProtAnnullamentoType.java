
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Definisce i metadati relativi all'annullamento di un protocollo
 * 
 * <p>Classe Java per prot_annullamento_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_annullamento_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Annullatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *         &lt;element name="MotivoAnnullamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProvvAnnullamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DataAnnullamento" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_annullamento_type", propOrder = {
    "annullatore",
    "motivoAnnullamento",
    "provvAnnullamento",
    "dataAnnullamento"
})
@XmlSeeAlso({
    WkfAzioneAnnullamentoProtocolloType.class
})
public class ProtAnnullamentoType
    implements Serializable
{

    @XmlElement(name = "Annullatore", required = true)
    protected OrgOrganigrammaType annullatore;
    @XmlElement(name = "MotivoAnnullamento", required = true, nillable = true)
    protected String motivoAnnullamento;
    @XmlElement(name = "ProvvAnnullamento", required = true, nillable = true)
    protected String provvAnnullamento;
    @XmlElement(name = "DataAnnullamento", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataAnnullamento;

    /**
     * Recupera il valore della proprietà annullatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getAnnullatore() {
        return annullatore;
    }

    /**
     * Imposta il valore della proprietà annullatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setAnnullatore(OrgOrganigrammaType value) {
        this.annullatore = value;
    }

    /**
     * Recupera il valore della proprietà motivoAnnullamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotivoAnnullamento() {
        return motivoAnnullamento;
    }

    /**
     * Imposta il valore della proprietà motivoAnnullamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotivoAnnullamento(String value) {
        this.motivoAnnullamento = value;
    }

    /**
     * Recupera il valore della proprietà provvAnnullamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvvAnnullamento() {
        return provvAnnullamento;
    }

    /**
     * Imposta il valore della proprietà provvAnnullamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvvAnnullamento(String value) {
        this.provvAnnullamento = value;
    }

    /**
     * Recupera il valore della proprietà dataAnnullamento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAnnullamento() {
        return dataAnnullamento;
    }

    /**
     * Imposta il valore della proprietà dataAnnullamento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAnnullamento(XMLGregorianCalendar value) {
        this.dataAnnullamento = value;
    }

}
