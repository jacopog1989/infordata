/**
 * Tipi ws.
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "urn:red:types:v1")
package it.ibm.red.webservice.model.redservice.types.v1;
