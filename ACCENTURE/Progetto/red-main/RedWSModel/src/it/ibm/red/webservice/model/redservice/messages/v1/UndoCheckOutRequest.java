
package it.ibm.red.webservice.model.redservice.messages.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import it.ibm.red.webservice.model.redservice.header.v1.CredenzialiUtenteRed;
import it.ibm.red.webservice.model.redservice.header.v1.CredenzialiWSRed;
import it.ibm.red.webservice.model.redservice.types.v1.DocumentoRed;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="credenzialiWSR" type="{urn:red:header:v1}CredenzialiWSRed" minOccurs="0"/>
 *         &lt;element name="credenzialiUtenteRed" type="{urn:red:header:v1}CredenzialiUtenteRed" minOccurs="0"/>
 *         &lt;element name="documento" type="{urn:red:types:v1}DocumentoRed" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "credenzialiWSR",
    "credenzialiUtenteRed",
    "documento"
})
@XmlRootElement(name = "undoCheckOutRequest")
public class UndoCheckOutRequest {


	/**
	 * Credenziali ws.
	 */
    @XmlElementRef(name = "credenzialiWSR", type = JAXBElement.class, required = false)
    protected JAXBElement<CredenzialiWSRed> credenzialiWSR;

	/**
	 * Credenziali utente.
	 */
    @XmlElementRef(name = "credenzialiUtenteRed", type = JAXBElement.class, required = false)
    protected JAXBElement<CredenzialiUtenteRed> credenzialiUtenteRed;

	/**
	 * Documento.
	 */
    @XmlElementRef(name = "documento", type = JAXBElement.class, required = false)
    protected JAXBElement<DocumentoRed> documento;

    /**
     * Gets the value of the credenzialiWSR property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CredenzialiWSRed }{@code >}
     *     
     */
    public JAXBElement<CredenzialiWSRed> getCredenzialiWSR() {
        return credenzialiWSR;
    }

    /**
     * Sets the value of the credenzialiWSR property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CredenzialiWSRed }{@code >}
     *     
     */
    public void setCredenzialiWSR(JAXBElement<CredenzialiWSRed> value) {
        this.credenzialiWSR = value;
    }

    /**
     * Gets the value of the credenzialiUtenteRed property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CredenzialiUtenteRed }{@code >}
     *     
     */
    public JAXBElement<CredenzialiUtenteRed> getCredenzialiUtenteRed() {
        return credenzialiUtenteRed;
    }

    /**
     * Sets the value of the credenzialiUtenteRed property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CredenzialiUtenteRed }{@code >}
     *     
     */
    public void setCredenzialiUtenteRed(JAXBElement<CredenzialiUtenteRed> value) {
        this.credenzialiUtenteRed = value;
    }

    /**
     * Gets the value of the documento property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DocumentoRed }{@code >}
     *     
     */
    public JAXBElement<DocumentoRed> getDocumento() {
        return documento;
    }

    /**
     * Sets the value of the documento property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DocumentoRed }{@code >}
     *     
     */
    public void setDocumento(JAXBElement<DocumentoRed> value) {
        this.documento = value;
    }

}
