
package it.ibm.red.webservice.model.redfad.headerfault;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the red.v1.it.gov.mef.servizi.common.headerfault package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: red.v1.it.gov.mef.servizi.common.headerfault
     * 
     */
    public ObjectFactory() {
    	// Costruttore di default.
    }

    /**
     * Create an instance of {@link GenericFault }
     * 
     */
    public GenericFault createGenericFault() {
        return new GenericFault();
    }

    /**
     * Create an instance of {@link BaseServiceFaultType }
     * 
     */
    public BaseServiceFaultType createBaseServiceFaultType() {
        return new BaseServiceFaultType();
    }

    /**
     * Create an instance of {@link ServiceErrorType }
     * 
     */
    public ServiceErrorType createServiceErrorType() {
        return new ServiceErrorType();
    }

    /**
     * Create an instance of {@link SecurityFault }
     * 
     */
    public SecurityFault createSecurityFault() {
        return new SecurityFault();
    }

}
