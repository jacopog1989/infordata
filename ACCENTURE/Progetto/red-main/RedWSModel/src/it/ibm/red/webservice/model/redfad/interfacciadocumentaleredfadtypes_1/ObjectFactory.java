
package it.ibm.red.webservice.model.redfad.interfacciadocumentaleredfadtypes_1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the red.v1.it.gov.mef.servizi.interfacciadocumentaleredfadtypes_1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


	/**
	 * Qname.
	 */
    private static final QName _RispostaTerminaFlussoAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.red/servizi/InterfacciaDocumentaleREDFADTypes_1", "risposta_terminaFlussoAttoDecreto");

	/**
	 * Qname.
	 */
    private static final QName _RichiestaTerminaFlussoAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.red/servizi/InterfacciaDocumentaleREDFADTypes_1", "richiesta_terminaFlussoAttoDecreto");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: red.v1.it.gov.mef.servizi.interfacciadocumentaleredfadtypes_1
     * 
     */
    public ObjectFactory() {
    	// Costruttore di default.
    }

    /**
     * Create an instance of {@link RispostaTerminaFlussoAttoDecretoType }
     * 
     */
    public RispostaTerminaFlussoAttoDecretoType createRispostaTerminaFlussoAttoDecretoType() {
        return new RispostaTerminaFlussoAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaTerminaFlussoAttoDecretoType }
     * 
     */
    public RichiestaTerminaFlussoAttoDecretoType createRichiestaTerminaFlussoAttoDecretoType() {
        return new RichiestaTerminaFlussoAttoDecretoType();
    }

    /**
     * Create an instance of {@link ServiceErrorType }
     * 
     */
    public ServiceErrorType createServiceErrorType() {
        return new ServiceErrorType();
    }

    /**
     * Create an instance of {@link RisultatoTerminaFlussoAttoDecretoType }
     * 
     */
    public RisultatoTerminaFlussoAttoDecretoType createRisultatoTerminaFlussoAttoDecretoType() {
        return new RisultatoTerminaFlussoAttoDecretoType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaTerminaFlussoAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.red/servizi/InterfacciaDocumentaleREDFADTypes_1", name = "risposta_terminaFlussoAttoDecreto")
    public JAXBElement<RispostaTerminaFlussoAttoDecretoType> createRispostaTerminaFlussoAttoDecreto(RispostaTerminaFlussoAttoDecretoType value) {
        return new JAXBElement<RispostaTerminaFlussoAttoDecretoType>(_RispostaTerminaFlussoAttoDecreto_QNAME, RispostaTerminaFlussoAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaTerminaFlussoAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.red/servizi/InterfacciaDocumentaleREDFADTypes_1", name = "richiesta_terminaFlussoAttoDecreto")
    public JAXBElement<RichiestaTerminaFlussoAttoDecretoType> createRichiestaTerminaFlussoAttoDecreto(RichiestaTerminaFlussoAttoDecretoType value) {
        return new JAXBElement<RichiestaTerminaFlussoAttoDecretoType>(_RichiestaTerminaFlussoAttoDecreto_QNAME, RichiestaTerminaFlussoAttoDecretoType.class, null, value);
    }

}
