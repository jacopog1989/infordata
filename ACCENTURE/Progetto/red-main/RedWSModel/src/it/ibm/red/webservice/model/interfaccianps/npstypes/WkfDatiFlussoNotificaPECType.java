
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraNotificaPECType;
import it.ibm.red.webservice.model.interfaccianps.postacerttypes.PostacertType;


/**
 * Dati del flusso delle notifiche PEC
 * 
 * <p>Classe Java per wkf_datiFlussoNotificaPEC_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_datiFlussoNotificaPEC_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdMessaggio" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdDocumento" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="DatiFlusso" type="{http://mef.gov.it.v1.npsTypes}wkf_datiTipoFlusso_type" minOccurs="0"/>
 *         &lt;element name="ChiaveDocumentoPEC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChiaveDocumentoNotificaPEC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Messaggio" type="{http://mef.gov.it.v1.npsTypes}email_message_type" minOccurs="0"/>
 *         &lt;element name="DatiCert" type="{http://mef.gov.it.v1.postacertTypes/}postacertType" minOccurs="0"/>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.npsTypes}prot_identificatoreProtocollo_type" minOccurs="0"/>
 *         &lt;element name="Destinatario" type="{http://mef.gov.it.v1.npsTypes}email_indirizzoEmail_type"/>
 *         &lt;element name="TipoNotificaPEC" type="{http://mef.gov.it.v1.npsTypes}prot_ricevutePec_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_datiFlussoNotificaPEC_type", propOrder = {
    "idMessaggio",
    "oggetto",
    "idDocumento",
    "datiFlusso",
    "chiaveDocumentoPEC",
    "chiaveDocumentoNotificaPEC",
    "messaggio",
    "datiCert",
    "protocollo",
    "destinatario",
    "tipoNotificaPEC"
})
@XmlSeeAlso({
    RichiestaElaboraNotificaPECType.class
})
public class WkfDatiFlussoNotificaPECType
    implements Serializable
{

    @XmlElement(name = "IdMessaggio", required = true)
    protected String idMessaggio;
    @XmlElement(name = "Oggetto", required = true)
    protected String oggetto;
    @XmlElement(name = "IdDocumento", required = true)
    protected String idDocumento;
    @XmlElement(name = "DatiFlusso")
    protected WkfDatiTipoFlussoType datiFlusso;
    @XmlElement(name = "ChiaveDocumentoPEC")
    protected String chiaveDocumentoPEC;
    @XmlElement(name = "ChiaveDocumentoNotificaPEC")
    protected String chiaveDocumentoNotificaPEC;
    @XmlElement(name = "Messaggio")
    protected EmailMessageType messaggio;
    @XmlElement(name = "DatiCert")
    protected PostacertType datiCert;
    @XmlElement(name = "Protocollo")
    protected ProtIdentificatoreProtocolloType protocollo;
    @XmlElement(name = "Destinatario", required = true)
    protected EmailIndirizzoEmailType destinatario;
    @XmlElement(name = "TipoNotificaPEC", required = true)
    protected ProtRicevutePecType tipoNotificaPEC;

    /**
     * Recupera il valore della proprietà idMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggio() {
        return idMessaggio;
    }

    /**
     * Imposta il valore della proprietà idMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggio(String value) {
        this.idMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà oggetto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Imposta il valore della proprietà oggetto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Recupera il valore della proprietà idDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Imposta il valore della proprietà idDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

    /**
     * Recupera il valore della proprietà datiFlusso.
     * 
     * @return
     *     possible object is
     *     {@link WkfDatiTipoFlussoType }
     *     
     */
    public WkfDatiTipoFlussoType getDatiFlusso() {
        return datiFlusso;
    }

    /**
     * Imposta il valore della proprietà datiFlusso.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfDatiTipoFlussoType }
     *     
     */
    public void setDatiFlusso(WkfDatiTipoFlussoType value) {
        this.datiFlusso = value;
    }

    /**
     * Recupera il valore della proprietà chiaveDocumentoPEC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveDocumentoPEC() {
        return chiaveDocumentoPEC;
    }

    /**
     * Imposta il valore della proprietà chiaveDocumentoPEC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveDocumentoPEC(String value) {
        this.chiaveDocumentoPEC = value;
    }

    /**
     * Recupera il valore della proprietà chiaveDocumentoNotificaPEC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveDocumentoNotificaPEC() {
        return chiaveDocumentoNotificaPEC;
    }

    /**
     * Imposta il valore della proprietà chiaveDocumentoNotificaPEC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveDocumentoNotificaPEC(String value) {
        this.chiaveDocumentoNotificaPEC = value;
    }

    /**
     * Recupera il valore della proprietà messaggio.
     * 
     * @return
     *     possible object is
     *     {@link EmailMessageType }
     *     
     */
    public EmailMessageType getMessaggio() {
        return messaggio;
    }

    /**
     * Imposta il valore della proprietà messaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailMessageType }
     *     
     */
    public void setMessaggio(EmailMessageType value) {
        this.messaggio = value;
    }

    /**
     * Recupera il valore della proprietà datiCert.
     * 
     * @return
     *     possible object is
     *     {@link PostacertType }
     *     
     */
    public PostacertType getDatiCert() {
        return datiCert;
    }

    /**
     * Imposta il valore della proprietà datiCert.
     * 
     * @param value
     *     allowed object is
     *     {@link PostacertType }
     *     
     */
    public void setDatiCert(PostacertType value) {
        this.datiCert = value;
    }

    /**
     * Recupera il valore della proprietà protocollo.
     * 
     * @return
     *     possible object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public ProtIdentificatoreProtocolloType getProtocollo() {
        return protocollo;
    }

    /**
     * Imposta il valore della proprietà protocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public void setProtocollo(ProtIdentificatoreProtocolloType value) {
        this.protocollo = value;
    }

    /**
     * Recupera il valore della proprietà destinatario.
     * 
     * @return
     *     possible object is
     *     {@link EmailIndirizzoEmailType }
     *     
     */
    public EmailIndirizzoEmailType getDestinatario() {
        return destinatario;
    }

    /**
     * Imposta il valore della proprietà destinatario.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailIndirizzoEmailType }
     *     
     */
    public void setDestinatario(EmailIndirizzoEmailType value) {
        this.destinatario = value;
    }

    /**
     * Recupera il valore della proprietà tipoNotificaPEC.
     * 
     * @return
     *     possible object is
     *     {@link ProtRicevutePecType }
     *     
     */
    public ProtRicevutePecType getTipoNotificaPEC() {
        return tipoNotificaPEC;
    }

    /**
     * Imposta il valore della proprietà tipoNotificaPEC.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtRicevutePecType }
     *     
     */
    public void setTipoNotificaPEC(ProtRicevutePecType value) {
        this.tipoNotificaPEC = value;
    }

}
