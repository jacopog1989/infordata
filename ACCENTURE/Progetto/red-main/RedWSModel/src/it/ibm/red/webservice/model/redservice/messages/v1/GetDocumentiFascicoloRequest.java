
package it.ibm.red.webservice.model.redservice.messages.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import it.ibm.red.webservice.model.redservice.header.v1.CredenzialiUtenteRed;
import it.ibm.red.webservice.model.redservice.header.v1.CredenzialiWSRed;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="credenzialiWS" type="{urn:red:header:v1}CredenzialiWSRed" minOccurs="0"/>
 *         &lt;element name="credenzialiUtente" type="{urn:red:header:v1}CredenzialiUtenteRed" minOccurs="0"/>
 *         &lt;element name="identificativoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "credenzialiWS",
    "credenzialiUtente",
    "identificativoFascicolo"
})
@XmlRootElement(name = "getDocumentiFascicoloRequest")
public class GetDocumentiFascicoloRequest {


    /**
     * Credenziali ws.
     */
    @XmlElementRef(name = "credenzialiWS", type = JAXBElement.class, required = false)
    protected JAXBElement<CredenzialiWSRed> credenzialiWS;

    /**
     * Credenziali utente.
     */
    @XmlElementRef(name = "credenzialiUtente", type = JAXBElement.class, required = false)
    protected JAXBElement<CredenzialiUtenteRed> credenzialiUtente;

    /**
     * Identificativo fascicolo.
     */
    @XmlElementRef(name = "identificativoFascicolo", type = JAXBElement.class, required = false)
    protected JAXBElement<String> identificativoFascicolo;

    /**
     * Gets the value of the credenzialiWS property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CredenzialiWSRed }{@code >}
     *     
     */
    public JAXBElement<CredenzialiWSRed> getCredenzialiWS() {
        return credenzialiWS;
    }

    /**
     * Sets the value of the credenzialiWS property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CredenzialiWSRed }{@code >}
     *     
     */
    public void setCredenzialiWS(JAXBElement<CredenzialiWSRed> value) {
        this.credenzialiWS = value;
    }

    /**
     * Gets the value of the credenzialiUtente property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CredenzialiUtenteRed }{@code >}
     *     
     */
    public JAXBElement<CredenzialiUtenteRed> getCredenzialiUtente() {
        return credenzialiUtente;
    }

    /**
     * Sets the value of the credenzialiUtente property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CredenzialiUtenteRed }{@code >}
     *     
     */
    public void setCredenzialiUtente(JAXBElement<CredenzialiUtenteRed> value) {
        this.credenzialiUtente = value;
    }

    /**
     * Gets the value of the identificativoFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIdentificativoFascicolo() {
        return identificativoFascicolo;
    }

    /**
     * Sets the value of the identificativoFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIdentificativoFascicolo(JAXBElement<String> value) {
        this.identificativoFascicolo = value;
    }
}
