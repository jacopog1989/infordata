
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Dati per la notifica di aggiornamento di un protocollo
 * 
 * <p>Classe Java per wkf_azioneAggiornamentoProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_azioneAggiornamentoProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DataOperazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="DatiModifica">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Mittente" type="{http://mef.gov.it.v1.npsTypes}prot_mittente_type" minOccurs="0"/>
 *                   &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TipologiaDocumento" type="{http://mef.gov.it.v1.npsTypes}prot_attributiEstesi_type" minOccurs="0"/>
 *                   &lt;element name="VoceTitolario" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_azioneAggiornamentoProtocollo_type", propOrder = {
    "dataOperazione",
    "datiModifica"
})
public class WkfAzioneAggiornamentoProtocolloType
    implements Serializable
{

    @XmlElement(name = "DataOperazione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataOperazione;
    @XmlElement(name = "DatiModifica", required = true)
    protected WkfAzioneAggiornamentoProtocolloType.DatiModifica datiModifica;

    /**
     * Recupera il valore della proprietà dataOperazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataOperazione() {
        return dataOperazione;
    }

    /**
     * Imposta il valore della proprietà dataOperazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataOperazione(XMLGregorianCalendar value) {
        this.dataOperazione = value;
    }

    /**
     * Recupera il valore della proprietà datiModifica.
     * 
     * @return
     *     possible object is
     *     {@link WkfAzioneAggiornamentoProtocolloType.DatiModifica }
     *     
     */
    public WkfAzioneAggiornamentoProtocolloType.DatiModifica getDatiModifica() {
        return datiModifica;
    }

    /**
     * Imposta il valore della proprietà datiModifica.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfAzioneAggiornamentoProtocolloType.DatiModifica }
     *     
     */
    public void setDatiModifica(WkfAzioneAggiornamentoProtocolloType.DatiModifica value) {
        this.datiModifica = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Mittente" type="{http://mef.gov.it.v1.npsTypes}prot_mittente_type" minOccurs="0"/>
     *         &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TipologiaDocumento" type="{http://mef.gov.it.v1.npsTypes}prot_attributiEstesi_type" minOccurs="0"/>
     *         &lt;element name="VoceTitolario" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mittente",
        "oggetto",
        "tipologiaDocumento",
        "voceTitolario"
    })
    public static class DatiModifica
        implements Serializable
    {

        @XmlElement(name = "Mittente")
        protected ProtMittenteType mittente;
        @XmlElement(name = "Oggetto")
        protected String oggetto;
        @XmlElement(name = "TipologiaDocumento")
        protected ProtAttributiEstesiType tipologiaDocumento;
        @XmlElement(name = "VoceTitolario")
        protected AllCodiceDescrizioneType voceTitolario;

        /**
         * Recupera il valore della proprietà mittente.
         * 
         * @return
         *     possible object is
         *     {@link ProtMittenteType }
         *     
         */
        public ProtMittenteType getMittente() {
            return mittente;
        }

        /**
         * Imposta il valore della proprietà mittente.
         * 
         * @param value
         *     allowed object is
         *     {@link ProtMittenteType }
         *     
         */
        public void setMittente(ProtMittenteType value) {
            this.mittente = value;
        }

        /**
         * Recupera il valore della proprietà oggetto.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOggetto() {
            return oggetto;
        }

        /**
         * Imposta il valore della proprietà oggetto.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOggetto(String value) {
            this.oggetto = value;
        }

        /**
         * Recupera il valore della proprietà tipologiaDocumento.
         * 
         * @return
         *     possible object is
         *     {@link ProtAttributiEstesiType }
         *     
         */
        public ProtAttributiEstesiType getTipologiaDocumento() {
            return tipologiaDocumento;
        }

        /**
         * Imposta il valore della proprietà tipologiaDocumento.
         * 
         * @param value
         *     allowed object is
         *     {@link ProtAttributiEstesiType }
         *     
         */
        public void setTipologiaDocumento(ProtAttributiEstesiType value) {
            this.tipologiaDocumento = value;
        }

        /**
         * Recupera il valore della proprietà voceTitolario.
         * 
         * @return
         *     possible object is
         *     {@link AllCodiceDescrizioneType }
         *     
         */
        public AllCodiceDescrizioneType getVoceTitolario() {
            return voceTitolario;
        }

        /**
         * Imposta il valore della proprietà voceTitolario.
         * 
         * @param value
         *     allowed object is
         *     {@link AllCodiceDescrizioneType }
         *     
         */
        public void setVoceTitolario(AllCodiceDescrizioneType value) {
            this.voceTitolario = value;
        }

    }

}
