
package it.ibm.red.webservice.model.igepa.inviarichiestaopf.types.xsd;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per RichiestaOPFIgepaRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="RichiestaOPFIgepaRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="richiestaOPFIgepaRequest" type="{http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd}DocumentoRichiestaOPFIgepa" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RichiestaOPFIgepaRequest", propOrder = {
    "richiestaOPFIgepaRequest"
})
public class RichiestaOPFIgepaRequest
    implements Serializable
{

    @XmlElementRef(name = "richiestaOPFIgepaRequest", namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<DocumentoRichiestaOPFIgepa> richiestaOPFIgepaRequest;

    /**
     * Recupera il valore della proprietà richiestaOPFIgepaRequest.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DocumentoRichiestaOPFIgepa }{@code >}
     *     
     */
    public JAXBElement<DocumentoRichiestaOPFIgepa> getRichiestaOPFIgepaRequest() {
        return richiestaOPFIgepaRequest;
    }

    /**
     * Imposta il valore della proprietà richiestaOPFIgepaRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DocumentoRichiestaOPFIgepa }{@code >}
     *     
     */
    public void setRichiestaOPFIgepaRequest(JAXBElement<DocumentoRichiestaOPFIgepa> value) {
        this.richiestaOPFIgepaRequest = value;
    }

}
