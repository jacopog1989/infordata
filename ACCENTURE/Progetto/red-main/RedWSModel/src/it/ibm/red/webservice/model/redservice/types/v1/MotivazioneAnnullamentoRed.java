
package it.ibm.red.webservice.model.redservice.types.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MotivazioneAnnullamentoRed complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MotivazioneAnnullamentoRed">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="motivazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MotivazioneAnnullamentoRed", propOrder = {
    "motivazione"
})
public class MotivazioneAnnullamentoRed {

	/**
	 * Motivazoine.
	 */
    @XmlElementRef(name = "motivazione", type = JAXBElement.class, required = false)
    protected JAXBElement<String> motivazione;

    /**
     * Gets the value of the motivazione property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMotivazione() {
        return motivazione;
    }

    /**
     * Sets the value of the motivazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMotivazione(JAXBElement<String> value) {
        this.motivazione = value;
    }

}
