
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraComunicazioneGenericaFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioProtocollazioneFlussoType;


/**
 * Dati del flusso
 * 
 * <p>Classe Java per wkf_datiFlussoEntrata_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_datiFlussoEntrata_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}wkf_datiFlusso_type">
 *       &lt;sequence>
 *         &lt;element name="messaggioRicevuto" type="{http://mef.gov.it.v1.npsTypes}email_message_type" minOccurs="0"/>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.npsTypes}wkf_identificatoreProtcollo_type" minOccurs="0"/>
 *         &lt;element name="ProtocolloNotifica" type="{http://mef.gov.it.v1.npsTypes}wkf_identificatoreProtcollo_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_datiFlussoEntrata_type", propOrder = {
    "messaggioRicevuto",
    "protocollo",
    "protocolloNotifica"
})
@XmlSeeAlso({
    RichiestaElaboraMessaggioProtocollazioneFlussoType.class,
    RichiestaElaboraComunicazioneGenericaFlussoType.class,
    RichiestaElaboraMessaggioFlussoType.class
})
public class WkfDatiFlussoEntrataType
    extends WkfDatiFlussoType
    implements Serializable
{

    protected EmailMessageType messaggioRicevuto;
    @XmlElement(name = "Protocollo")
    protected WkfIdentificatoreProtcolloType protocollo;
    @XmlElement(name = "ProtocolloNotifica")
    protected WkfIdentificatoreProtcolloType protocolloNotifica;

    /**
     * Recupera il valore della proprietà messaggioRicevuto.
     * 
     * @return
     *     possible object is
     *     {@link EmailMessageType }
     *     
     */
    public EmailMessageType getMessaggioRicevuto() {
        return messaggioRicevuto;
    }

    /**
     * Imposta il valore della proprietà messaggioRicevuto.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailMessageType }
     *     
     */
    public void setMessaggioRicevuto(EmailMessageType value) {
        this.messaggioRicevuto = value;
    }

    /**
     * Recupera il valore della proprietà protocollo.
     * 
     * @return
     *     possible object is
     *     {@link WkfIdentificatoreProtcolloType }
     *     
     */
    public WkfIdentificatoreProtcolloType getProtocollo() {
        return protocollo;
    }

    /**
     * Imposta il valore della proprietà protocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfIdentificatoreProtcolloType }
     *     
     */
    public void setProtocollo(WkfIdentificatoreProtcolloType value) {
        this.protocollo = value;
    }

    /**
     * Recupera il valore della proprietà protocolloNotifica.
     * 
     * @return
     *     possible object is
     *     {@link WkfIdentificatoreProtcolloType }
     *     
     */
    public WkfIdentificatoreProtcolloType getProtocolloNotifica() {
        return protocolloNotifica;
    }

    /**
     * Imposta il valore della proprietà protocolloNotifica.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfIdentificatoreProtcolloType }
     *     
     */
    public void setProtocolloNotifica(WkfIdentificatoreProtcolloType value) {
        this.protocolloNotifica = value;
    }

}
