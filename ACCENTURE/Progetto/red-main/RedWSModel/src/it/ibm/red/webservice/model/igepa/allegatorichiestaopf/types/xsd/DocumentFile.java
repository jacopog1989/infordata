
package it.ibm.red.webservice.model.igepa.allegatorichiestaopf.types.xsd;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

import it.ibm.red.webservice.model.igepa.types.xsd.Base64Binary;


/**
 * <p>Classe Java per DocumentFile complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="DocumentFile">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fileBase64" type="{http://ws.filenet.nsd.capgemini.com/xsd}Base64Binary" minOccurs="0"/>
 *         &lt;element name="fileName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentFile", propOrder = {
    "fileBase64",
    "fileName"
})
public class DocumentFile
    implements Serializable
{

    @XmlElementRef(name = "fileBase64", namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Base64Binary> fileBase64;
    @XmlElementRef(name = "fileName", namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fileName;

    /**
     * Recupera il valore della proprietà fileBase64.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Base64Binary }{@code >}
     *     
     */
    public JAXBElement<Base64Binary> getFileBase64() {
        return fileBase64;
    }

    /**
     * Imposta il valore della proprietà fileBase64.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Base64Binary }{@code >}
     *     
     */
    public void setFileBase64(JAXBElement<Base64Binary> value) {
        this.fileBase64 = value;
    }

    /**
     * Recupera il valore della proprietà fileName.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFileName() {
        return fileName;
    }

    /**
     * Imposta il valore della proprietà fileName.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFileName(JAXBElement<String> value) {
        this.fileName = value;
    }

}
