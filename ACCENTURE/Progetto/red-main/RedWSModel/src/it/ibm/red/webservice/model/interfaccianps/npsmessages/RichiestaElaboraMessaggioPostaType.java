
package it.ibm.red.webservice.model.interfaccianps.npsmessages;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import it.ibm.red.webservice.model.interfaccianps.npstypes.EmailDatiPostaIngressoType;


/**
 * Tipo di dato che indica il messaggio di e-mail ricevuto
 * 
 * <p>Classe Java per richiesta_elaboraMessaggio_Posta_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_elaboraMessaggio_Posta_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}email_datiPostaIngresso_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_elaboraMessaggio_Posta_type")
public class RichiestaElaboraMessaggioPostaType
    extends EmailDatiPostaIngressoType
    implements Serializable
{


}
