
package it.ibm.red.webservice.model.redfad.interfacciadocumentaleredfadtypes_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Base response type to be used across all operations
 * 
 * <p>Java class for BaseServiceResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BaseServiceResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Esito" type="{http://mef.gov.it.v1.red/servizi/InterfacciaDocumentaleREDFADTypes_1}esito_type"/>
 *         &lt;element name="ErrorList" type="{http://mef.gov.it.v1.red/servizi/InterfacciaDocumentaleREDFADTypes_1}ServiceErrorType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseServiceResponseType", propOrder = {
    "esito",
    "errorList"
})
@XmlSeeAlso({
    RisultatoTerminaFlussoAttoDecretoType.class
})
public abstract class BaseServiceResponseType {

	/**
	 * Esito.
	 */
    @XmlElement(name = "Esito", required = true)
    protected EsitoType esito;

    /**
     * Lista errori.
     */
    @XmlElement(name = "ErrorList")
    protected List<ServiceErrorType> errorList;

    /**
     * Gets the value of the esito property.
     * 
     * @return
     *     possible object is
     *     {@link EsitoType }
     *     
     */
    public EsitoType getEsito() {
        return esito;
    }

    /**
     * Sets the value of the esito property.
     * 
     * @param value
     *     allowed object is
     *     {@link EsitoType }
     *     
     */
    public void setEsito(EsitoType value) {
        this.esito = value;
    }

    /**
     * Gets the value of the errorList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceErrorType }
     * 
     * @return list of ServiceErrorType
     */
    public List<ServiceErrorType> getErrorList() {
        if (errorList == null) {
            errorList = new ArrayList<>();
        }
        return this.errorList;
    }

}
