
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Dati per la notifica per il rispondi A
 * 
 * <p>Classe Java per wkf_azioneRispondiAProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_azioneRispondiAProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProtocolloRisposta" type="{http://mef.gov.it.v1.npsTypes}identificativoProtocolloRequest_type"/>
 *         &lt;element name="StatoUscita">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="DA_SPEDIRE"/>
 *               &lt;enumeration value="INVIATO"/>
 *               &lt;enumeration value="SPEDITO"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="StatoEntrata">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="DA_ASSEGNARE"/>
 *               &lt;enumeration value="IN_LAVORAZIONE"/>
 *               &lt;enumeration value="AGLI_ATTI"/>
 *               &lt;enumeration value="CHIUSO"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Protocollatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SistemaAusiliario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Collegati" type="{http://mef.gov.it.v1.npsTypes}prot_identificatoreProtocolloCollegato_type" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_azioneRispondiAProtocollo_type", propOrder = {
    "protocolloRisposta",
    "statoUscita",
    "statoEntrata",
    "protocollatore",
    "sistemaProduttore",
    "sistemaAusiliario",
    "collegati"
})
public class WkfAzioneRispondiAProtocolloType
    implements Serializable
{

    @XmlElement(name = "ProtocolloRisposta", required = true)
    protected IdentificativoProtocolloRequestType protocolloRisposta;
    @XmlElement(name = "StatoUscita", required = true)
    protected String statoUscita;
    @XmlElement(name = "StatoEntrata", required = true)
    protected String statoEntrata;
    @XmlElement(name = "Protocollatore", required = true)
    protected OrgOrganigrammaType protocollatore;
    @XmlElement(name = "SistemaProduttore", required = true)
    protected String sistemaProduttore;
    @XmlElement(name = "SistemaAusiliario")
    protected String sistemaAusiliario;
    @XmlElement(name = "Collegati")
    protected List<ProtIdentificatoreProtocolloCollegatoType> collegati;

    /**
     * Recupera il valore della proprietà protocolloRisposta.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public IdentificativoProtocolloRequestType getProtocolloRisposta() {
        return protocolloRisposta;
    }

    /**
     * Imposta il valore della proprietà protocolloRisposta.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public void setProtocolloRisposta(IdentificativoProtocolloRequestType value) {
        this.protocolloRisposta = value;
    }

    /**
     * Recupera il valore della proprietà statoUscita.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoUscita() {
        return statoUscita;
    }

    /**
     * Imposta il valore della proprietà statoUscita.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoUscita(String value) {
        this.statoUscita = value;
    }

    /**
     * Recupera il valore della proprietà statoEntrata.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoEntrata() {
        return statoEntrata;
    }

    /**
     * Imposta il valore della proprietà statoEntrata.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoEntrata(String value) {
        this.statoEntrata = value;
    }

    /**
     * Recupera il valore della proprietà protocollatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getProtocollatore() {
        return protocollatore;
    }

    /**
     * Imposta il valore della proprietà protocollatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setProtocollatore(OrgOrganigrammaType value) {
        this.protocollatore = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

    /**
     * Recupera il valore della proprietà sistemaAusiliario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaAusiliario() {
        return sistemaAusiliario;
    }

    /**
     * Imposta il valore della proprietà sistemaAusiliario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaAusiliario(String value) {
        this.sistemaAusiliario = value;
    }

    /**
     * Gets the value of the collegati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the collegati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCollegati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtIdentificatoreProtocolloCollegatoType }
     * 
     * 
     */
    public List<ProtIdentificatoreProtocolloCollegatoType> getCollegati() {
        if (collegati == null) {
            collegati = new ArrayList<ProtIdentificatoreProtocolloCollegatoType>();
        }
        return this.collegati;
    }

}
