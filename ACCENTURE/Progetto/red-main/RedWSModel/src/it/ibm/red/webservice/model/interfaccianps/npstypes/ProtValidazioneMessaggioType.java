
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce la validazione di un messaggio ricevuto
 * 
 * <p>Classe Java per prot_validazioneMessaggio_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_validazioneMessaggio_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Tipo">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="GENERICO"/>
 *               &lt;enumeration value="SEGNATURA"/>
 *               &lt;enumeration value="FLUSSO"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Flusso" type="{http://mef.gov.it.v1.npsTypes}wkf_datiTipoFlusso_type" minOccurs="0"/>
 *         &lt;element name="ValidazioneSegnatura" type="{http://mef.gov.it.v1.npsTypes}prot_validazioneSegnatura_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_validazioneMessaggio_type", propOrder = {
    "tipo",
    "flusso",
    "validazioneSegnatura"
})
public class ProtValidazioneMessaggioType
    implements Serializable
{

    @XmlElement(name = "Tipo", required = true)
    protected String tipo;
    @XmlElement(name = "Flusso")
    protected WkfDatiTipoFlussoType flusso;
    @XmlElement(name = "ValidazioneSegnatura")
    protected ProtValidazioneSegnaturaType validazioneSegnatura;

    /**
     * Recupera il valore della proprietà tipo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Imposta il valore della proprietà tipo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipo(String value) {
        this.tipo = value;
    }

    /**
     * Recupera il valore della proprietà flusso.
     * 
     * @return
     *     possible object is
     *     {@link WkfDatiTipoFlussoType }
     *     
     */
    public WkfDatiTipoFlussoType getFlusso() {
        return flusso;
    }

    /**
     * Imposta il valore della proprietà flusso.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfDatiTipoFlussoType }
     *     
     */
    public void setFlusso(WkfDatiTipoFlussoType value) {
        this.flusso = value;
    }

    /**
     * Recupera il valore della proprietà validazioneSegnatura.
     * 
     * @return
     *     possible object is
     *     {@link ProtValidazioneSegnaturaType }
     *     
     */
    public ProtValidazioneSegnaturaType getValidazioneSegnatura() {
        return validazioneSegnatura;
    }

    /**
     * Imposta il valore della proprietà validazioneSegnatura.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtValidazioneSegnaturaType }
     *     
     */
    public void setValidazioneSegnatura(ProtValidazioneSegnaturaType value) {
        this.validazioneSegnatura = value;
    }

}
