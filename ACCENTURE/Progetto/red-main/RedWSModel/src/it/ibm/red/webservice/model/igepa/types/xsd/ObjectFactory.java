
package it.ibm.red.webservice.model.igepa.types.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.capgemini.nsd.filenet.ws.xsd package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private static final QName _ContentTypeType0ContentTypeType0_QNAME = new QName("http://ws.filenet.nsd.capgemini.com/xsd", "contentType_type0");
    private static final QName _Base64BinaryContentType_QNAME = new QName("http://ws.filenet.nsd.capgemini.com/xsd", "contentType");
    private static final QName _Base64BinaryBase64Binary_QNAME = new QName("http://ws.filenet.nsd.capgemini.com/xsd", "base64Binary");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.capgemini.nsd.filenet.ws.xsd
     * 
     */
    public ObjectFactory() {
    	// Costruttore intenzionalmente vuoto.
    }

    /**
     * Create an instance of {@link Base64Binary }
     * 
     */
    public Base64Binary createBase64Binary() {
        return new Base64Binary();
    }

    /**
     * Create an instance of {@link ContentTypeType0 }
     * 
     */
    public ContentTypeType0 createContentTypeType0() {
        return new ContentTypeType0();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.filenet.nsd.capgemini.com/xsd", name = "contentType_type0", scope = ContentTypeType0 .class)
    public JAXBElement<String> createContentTypeType0ContentTypeType0(String value) {
        return new JAXBElement<String>(_ContentTypeType0ContentTypeType0_QNAME, String.class, ContentTypeType0 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContentTypeType0 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.filenet.nsd.capgemini.com/xsd", name = "contentType", scope = Base64Binary.class)
    public JAXBElement<ContentTypeType0> createBase64BinaryContentType(ContentTypeType0 value) {
        return new JAXBElement<ContentTypeType0>(_Base64BinaryContentType_QNAME, ContentTypeType0 .class, Base64Binary.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.filenet.nsd.capgemini.com/xsd", name = "base64Binary", scope = Base64Binary.class)
    public JAXBElement<byte[]> createBase64BinaryBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64BinaryBase64Binary_QNAME, byte[].class, Base64Binary.class, ((byte[]) value));
    }

}
