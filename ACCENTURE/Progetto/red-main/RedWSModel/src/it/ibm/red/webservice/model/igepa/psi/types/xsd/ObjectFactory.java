
package it.ibm.red.webservice.model.igepa.psi.types.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import it.ibm.red.webservice.model.igepa.types.xsd.Base64Binary;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.capgemini.nsd.filenet.ws.types.xsd package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private static final QName _AcquisizioneDCPRequestAcquisizioneDCPRequest_QNAME = new QName("http://types.ws.filenet.nsd.capgemini.com/xsd", "acquisizioneDCPRequest");
    private static final QName _DocumentoCertificazionePattoTypeNumeroProtocollo_QNAME = new QName("http://types.ws.filenet.nsd.capgemini.com/xsd", "numeroProtocollo");
    private static final QName _DocumentoCertificazionePattoTypeEnteMittente_QNAME = new QName("http://types.ws.filenet.nsd.capgemini.com/xsd", "enteMittente");
    private static final QName _DocumentoCertificazionePattoTypeIdUfficioDestinatario_QNAME = new QName("http://types.ws.filenet.nsd.capgemini.com/xsd", "idUfficioDestinatario");
    private static final QName _DocumentoCertificazionePattoTypeOggettoProtocollo_QNAME = new QName("http://types.ws.filenet.nsd.capgemini.com/xsd", "oggettoProtocollo");
    private static final QName _DocumentoCertificazionePattoTypeIdUtenteDestinatario_QNAME = new QName("http://types.ws.filenet.nsd.capgemini.com/xsd", "idUtenteDestinatario");
    private static final QName _DocumentoCertificazionePattoTypeDataProtocollo_QNAME = new QName("http://types.ws.filenet.nsd.capgemini.com/xsd", "dataProtocollo");
    private static final QName _DocumentoCertificazionePattoTypeDocumentoPrincipale_QNAME = new QName("http://types.ws.filenet.nsd.capgemini.com/xsd", "documentoPrincipale");
    private static final QName _DocumentoCertificazionePattoTypeAllegato_QNAME = new QName("http://types.ws.filenet.nsd.capgemini.com/xsd", "allegato");
    private static final QName _InvocationStatusValue_QNAME = new QName("http://types.ws.filenet.nsd.capgemini.com/xsd", "value");
    private static final QName _DataFormatTypeDataFormatType_QNAME = new QName("http://types.ws.filenet.nsd.capgemini.com/xsd", "dataFormatType");
    private static final QName _AcquisizioneDCPResponseAcquisizioneDCPResponse_QNAME = new QName("http://types.ws.filenet.nsd.capgemini.com/xsd", "acquisizioneDCPResponse");
    private static final QName _BaseResponseDescription_QNAME = new QName("http://types.ws.filenet.nsd.capgemini.com/xsd", "description");
    private static final QName _BaseResponseStatus_QNAME = new QName("http://types.ws.filenet.nsd.capgemini.com/xsd", "status");
    private static final QName _DocumentFileFileBase64_QNAME = new QName("http://types.ws.filenet.nsd.capgemini.com/xsd", "fileBase64");
    private static final QName _DocumentFileFileName_QNAME = new QName("http://types.ws.filenet.nsd.capgemini.com/xsd", "fileName");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.capgemini.nsd.filenet.ws.types.xsd
     * 
     */
    public ObjectFactory() {
    	// Costruttore intenzionalmente vuoto.
    }

    /**
     * Create an instance of {@link BaseResponse }
     * 
     */
    public BaseResponse createBaseResponse() {
        return new BaseResponse();
    }

    /**
     * Create an instance of {@link DocumentFile }
     * 
     */
    public DocumentFile createDocumentFile() {
        return new DocumentFile();
    }

    /**
     * Create an instance of {@link InvocationStatus }
     * 
     */
    public InvocationStatus createInvocationStatus() {
        return new InvocationStatus();
    }

    /**
     * Create an instance of {@link AcquisizioneDCPRequest }
     * 
     */
    public AcquisizioneDCPRequest createAcquisizioneDCPRequest() {
        return new AcquisizioneDCPRequest();
    }

    /**
     * Create an instance of {@link DocumentoCertificazionePattoType }
     * 
     */
    public DocumentoCertificazionePattoType createDocumentoCertificazionePattoType() {
        return new DocumentoCertificazionePattoType();
    }

    /**
     * Create an instance of {@link AcquisizioneDCPResponse }
     * 
     */
    public AcquisizioneDCPResponse createAcquisizioneDCPResponse() {
        return new AcquisizioneDCPResponse();
    }

    /**
     * Create an instance of {@link DataFormatType }
     * 
     */
    public DataFormatType createDataFormatType() {
        return new DataFormatType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoCertificazionePattoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", name = "acquisizioneDCPRequest", scope = AcquisizioneDCPRequest.class)
    public JAXBElement<DocumentoCertificazionePattoType> createAcquisizioneDCPRequestAcquisizioneDCPRequest(DocumentoCertificazionePattoType value) {
        return new JAXBElement<DocumentoCertificazionePattoType>(_AcquisizioneDCPRequestAcquisizioneDCPRequest_QNAME, DocumentoCertificazionePattoType.class, AcquisizioneDCPRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", name = "numeroProtocollo", scope = DocumentoCertificazionePattoType.class)
    public JAXBElement<Integer> createDocumentoCertificazionePattoTypeNumeroProtocollo(Integer value) {
        return new JAXBElement<Integer>(_DocumentoCertificazionePattoTypeNumeroProtocollo_QNAME, Integer.class, DocumentoCertificazionePattoType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", name = "enteMittente", scope = DocumentoCertificazionePattoType.class)
    public JAXBElement<String> createDocumentoCertificazionePattoTypeEnteMittente(String value) {
        return new JAXBElement<String>(_DocumentoCertificazionePattoTypeEnteMittente_QNAME, String.class, DocumentoCertificazionePattoType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", name = "idUfficioDestinatario", scope = DocumentoCertificazionePattoType.class)
    public JAXBElement<Integer> createDocumentoCertificazionePattoTypeIdUfficioDestinatario(Integer value) {
        return new JAXBElement<Integer>(_DocumentoCertificazionePattoTypeIdUfficioDestinatario_QNAME, Integer.class, DocumentoCertificazionePattoType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", name = "oggettoProtocollo", scope = DocumentoCertificazionePattoType.class)
    public JAXBElement<String> createDocumentoCertificazionePattoTypeOggettoProtocollo(String value) {
        return new JAXBElement<String>(_DocumentoCertificazionePattoTypeOggettoProtocollo_QNAME, String.class, DocumentoCertificazionePattoType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", name = "idUtenteDestinatario", scope = DocumentoCertificazionePattoType.class)
    public JAXBElement<Integer> createDocumentoCertificazionePattoTypeIdUtenteDestinatario(Integer value) {
        return new JAXBElement<Integer>(_DocumentoCertificazionePattoTypeIdUtenteDestinatario_QNAME, Integer.class, DocumentoCertificazionePattoType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataFormatType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", name = "dataProtocollo", scope = DocumentoCertificazionePattoType.class)
    public JAXBElement<DataFormatType> createDocumentoCertificazionePattoTypeDataProtocollo(DataFormatType value) {
        return new JAXBElement<DataFormatType>(_DocumentoCertificazionePattoTypeDataProtocollo_QNAME, DataFormatType.class, DocumentoCertificazionePattoType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", name = "documentoPrincipale", scope = DocumentoCertificazionePattoType.class)
    public JAXBElement<DocumentFile> createDocumentoCertificazionePattoTypeDocumentoPrincipale(DocumentFile value) {
        return new JAXBElement<DocumentFile>(_DocumentoCertificazionePattoTypeDocumentoPrincipale_QNAME, DocumentFile.class, DocumentoCertificazionePattoType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", name = "allegato", scope = DocumentoCertificazionePattoType.class)
    public JAXBElement<DocumentFile> createDocumentoCertificazionePattoTypeAllegato(DocumentFile value) {
        return new JAXBElement<DocumentFile>(_DocumentoCertificazionePattoTypeAllegato_QNAME, DocumentFile.class, DocumentoCertificazionePattoType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", name = "value", scope = InvocationStatus.class)
    public JAXBElement<String> createInvocationStatusValue(String value) {
        return new JAXBElement<String>(_InvocationStatusValue_QNAME, String.class, InvocationStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", name = "dataFormatType", scope = DataFormatType.class)
    public JAXBElement<String> createDataFormatTypeDataFormatType(String value) {
        return new JAXBElement<String>(_DataFormatTypeDataFormatType_QNAME, String.class, DataFormatType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", name = "acquisizioneDCPResponse", scope = AcquisizioneDCPResponse.class)
    public JAXBElement<BaseResponse> createAcquisizioneDCPResponseAcquisizioneDCPResponse(BaseResponse value) {
        return new JAXBElement<BaseResponse>(_AcquisizioneDCPResponseAcquisizioneDCPResponse_QNAME, BaseResponse.class, AcquisizioneDCPResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", name = "description", scope = BaseResponse.class)
    public JAXBElement<String> createBaseResponseDescription(String value) {
        return new JAXBElement<String>(_BaseResponseDescription_QNAME, String.class, BaseResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvocationStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", name = "status", scope = BaseResponse.class)
    public JAXBElement<InvocationStatus> createBaseResponseStatus(InvocationStatus value) {
        return new JAXBElement<InvocationStatus>(_BaseResponseStatus_QNAME, InvocationStatus.class, BaseResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Base64Binary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", name = "fileBase64", scope = DocumentFile.class)
    public JAXBElement<Base64Binary> createDocumentFileFileBase64(Base64Binary value) {
        return new JAXBElement<Base64Binary>(_DocumentFileFileBase64_QNAME, Base64Binary.class, DocumentFile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", name = "fileName", scope = DocumentFile.class)
    public JAXBElement<String> createDocumentFileFileName(String value) {
        return new JAXBElement<String>(_DocumentFileFileName_QNAME, String.class, DocumentFile.class, value);
    }

}
