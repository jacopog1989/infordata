
package it.ibm.red.webservice.model.redservice.types.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DocumentoRed complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DocumentoRed">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="allegati" type="{urn:red:types:v1}DocumentoRed" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="classeDocumentale" type="{urn:red:types:v1}ClasseDocumentaleRed" minOccurs="0"/>
 *         &lt;element name="contentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataHandler" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="dataModifica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ereditaSecurity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="folder" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="guid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idFascicolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numVersione" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="security" type="{urn:red:types:v1}SecurityRed" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentoRed", propOrder = {
    "allegati",
    "classeDocumentale",
    "contentType",
    "dataHandler",
    "dataModifica",
    "ereditaSecurity",
    "folder",
    "guid",
    "idDocumento",
    "idFascicolo",
    "numVersione",
    "security"
})
@XmlSeeAlso({
    VersioneDocumentoInputRed.class
})
public class DocumentoRed {

	/**
	 * Allegati.
	 */
    @XmlElement(nillable = true)
    protected List<DocumentoRed> allegati;

	/**
	 * Classe documentale.
	 */
    @XmlElementRef(name = "classeDocumentale", type = JAXBElement.class, required = false)
    protected JAXBElement<ClasseDocumentaleRed> classeDocumentale;

	/**
	 * Content type.
	 */
    @XmlElementRef(name = "contentType", type = JAXBElement.class, required = false)
    protected JAXBElement<String> contentType;

	/**
	 * Data Handler.
	 */
    @XmlElementRef(name = "dataHandler", type = JAXBElement.class, required = false)
    protected JAXBElement<byte[]> dataHandler;

	/**
	 * Data modifica.
	 */
    @XmlElementRef(name = "dataModifica", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dataModifica;

	/**
	 * Security ereditata.
	 */
    protected Boolean ereditaSecurity;

	/**
	 * Folder.
	 */
    @XmlElementRef(name = "folder", type = JAXBElement.class, required = false)
    protected JAXBElement<String> folder;

	/**
	 * Guid.
	 */
    @XmlElementRef(name = "guid", type = JAXBElement.class, required = false)
    protected JAXBElement<String> guid;

	/**
	 * Identificativo documento.
	 */
    @XmlElementRef(name = "idDocumento", type = JAXBElement.class, required = false)
    protected JAXBElement<String> idDocumento;

	/**
	 * Identificativo fascicolo.
	 */
    @XmlElementRef(name = "idFascicolo", type = JAXBElement.class, required = false)
    protected JAXBElement<String> idFascicolo;

	/**
	 * Numero versione.
	 */
    protected Integer numVersione;

	/**
	 * Security.
	 */
    @XmlElement(nillable = true)
    protected List<SecurityRed> security;

    /**
     * Gets the value of the allegati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allegati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllegati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentoRed }
     * 
     * 
     */
    public List<DocumentoRed> getAllegati() {
        if (allegati == null) {
            allegati = new ArrayList<>();
        }
        return this.allegati;
    }

    /**
     * Gets the value of the classeDocumentale property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ClasseDocumentaleRed }{@code >}
     *     
     */
    public JAXBElement<ClasseDocumentaleRed> getClasseDocumentale() {
        return classeDocumentale;
    }

    /**
     * Sets the value of the classeDocumentale property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ClasseDocumentaleRed }{@code >}
     *     
     */
    public void setClasseDocumentale(JAXBElement<ClasseDocumentaleRed> value) {
        this.classeDocumentale = value;
    }

    /**
     * Gets the value of the contentType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getContentType() {
        return contentType;
    }

    /**
     * Sets the value of the contentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setContentType(JAXBElement<String> value) {
        this.contentType = value;
    }

    /**
     * Gets the value of the dataHandler property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public JAXBElement<byte[]> getDataHandler() {
        return dataHandler;
    }

    /**
     * Sets the value of the dataHandler property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public void setDataHandler(JAXBElement<byte[]> value) {
        this.dataHandler = value;
    }

    /**
     * Gets the value of the dataModifica property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDataModifica() {
        return dataModifica;
    }

    /**
     * Sets the value of the dataModifica property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDataModifica(JAXBElement<String> value) {
        this.dataModifica = value;
    }

    /**
     * Gets the value of the ereditaSecurity property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEreditaSecurity() {
        return ereditaSecurity;
    }

    /**
     * Sets the value of the ereditaSecurity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEreditaSecurity(Boolean value) {
        this.ereditaSecurity = value;
    }

    /**
     * Gets the value of the folder property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFolder() {
        return folder;
    }

    /**
     * Sets the value of the folder property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFolder(JAXBElement<String> value) {
        this.folder = value;
    }

    /**
     * Gets the value of the guid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGuid() {
        return guid;
    }

    /**
     * Sets the value of the guid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGuid(JAXBElement<String> value) {
        this.guid = value;
    }

    /**
     * Gets the value of the idDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIdDocumento() {
        return idDocumento;
    }

    /**
     * Sets the value of the idDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIdDocumento(JAXBElement<String> value) {
        this.idDocumento = value;
    }

    /**
     * Gets the value of the idFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIdFascicolo() {
        return idFascicolo;
    }

    /**
     * Sets the value of the idFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIdFascicolo(JAXBElement<String> value) {
        this.idFascicolo = value;
    }

    /**
     * Gets the value of the numVersione property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumVersione() {
        return numVersione;
    }

    /**
     * Sets the value of the numVersione property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumVersione(Integer value) {
        this.numVersione = value;
    }

    /**
     * Gets the value of the security property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the security property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSecurity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SecurityRed }
     * 
     * 
     */
    public List<SecurityRed> getSecurity() {
        if (security == null) {
            security = new ArrayList<>();
        }
        return this.security;
    }

}
