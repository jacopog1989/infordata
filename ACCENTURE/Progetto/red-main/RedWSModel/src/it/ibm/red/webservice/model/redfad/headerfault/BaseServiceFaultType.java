
package it.ibm.red.webservice.model.redfad.headerfault;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * Base fault to be used across a series of faults
 * 
 * <p>Java class for BaseServiceFaultType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BaseServiceFaultType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ErrorDetails" type="{http://mef.gov.it.v1.red/servizi/Common/HeaderFault}ServiceErrorType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({
    SecurityFault.class,
    GenericFault.class
})
public class BaseServiceFaultType {

	/**
	 * Dettagli errore.
	 */
    @XmlElement(name = "ErrorDetails", required = true)
    protected ServiceErrorType errorDetails;

    /**
     * Gets the value of the errorDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceErrorType }
     *     
     */
    public ServiceErrorType getErrorDetails() {
        return errorDetails;
    }

    /**
     * Sets the value of the errorDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceErrorType }
     *     
     */
    public void setErrorDetails(ServiceErrorType value) {
        this.errorDetails = value;
    }

}
