
package it.ibm.red.webservice.model.redfad.headerfault;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServiceStatusType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ServiceStatusType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Success"/>
 *     &lt;enumeration value="Failure"/>
 *     &lt;enumeration value="Warning"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ServiceStatusType")
@XmlEnum
public enum ServiceStatusType {

    
	/**
	 * Valore.
	 */
    @XmlEnumValue("Success")
    SUCCESS("Success"),
    
	/**
	 * Valore.
	 */
    @XmlEnumValue("Failure")
    FAILURE("Failure"),
    
	/**
	 * Valore.
	 */
    @XmlEnumValue("Warning")
    WARNING("Warning");

    
	/**
	 * Valore.
	 */
    private final String value;

    /**
     * Costruttore.
     * @param v
     */
    ServiceStatusType(String v) {
        value = v;
    }

    /**
     * Restituisce il value dell'enum.
     * @return value
     */
    public String value() {
        return value;
    }

    /**
     * Restituisce il type del service associato al value.
     * @param v
     * @return type service
     */
    public static ServiceStatusType fromValue(String v) {
        for (ServiceStatusType c: ServiceStatusType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
