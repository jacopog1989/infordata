
package it.ibm.red.webservice.model.igepa.allegatorichiestaopf.types.xsd;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per AllegatoIgepaRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="AllegatoIgepaRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="allegatoIgepaRequest" type="{http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd}AllegatoRichiestaOPF" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AllegatoIgepaRequest", propOrder = {
    "allegatoIgepaRequest"
})
public class AllegatoIgepaRequest
    implements Serializable
{

    @XmlElementRef(name = "allegatoIgepaRequest", namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<AllegatoRichiestaOPF> allegatoIgepaRequest;

    /**
     * Recupera il valore della proprietà allegatoIgepaRequest.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AllegatoRichiestaOPF }{@code >}
     *     
     */
    public JAXBElement<AllegatoRichiestaOPF> getAllegatoIgepaRequest() {
        return allegatoIgepaRequest;
    }

    /**
     * Imposta il valore della proprietà allegatoIgepaRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AllegatoRichiestaOPF }{@code >}
     *     
     */
    public void setAllegatoIgepaRequest(JAXBElement<AllegatoRichiestaOPF> value) {
        this.allegatoIgepaRequest = value;
    }

}
