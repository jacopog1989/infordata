
package it.ibm.red.webservice.model.igepa.types.xsd;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ContentType_type0 complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ContentType_type0">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contentType_type0" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContentType_type0", propOrder = {
    "contentTypeType0"
})
public class ContentTypeType0
    implements Serializable
{

    @XmlElementRef(name = "contentType_type0", namespace = "http://ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> contentTypeType0;

    /**
     * Recupera il valore della proprietà contentTypeType0.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getContentTypeType0() {
        return contentTypeType0;
    }

    /**
     * Imposta il valore della proprietà contentTypeType0.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setContentTypeType0(JAXBElement<String> value) {
        this.contentTypeType0 = value;
    }

}
