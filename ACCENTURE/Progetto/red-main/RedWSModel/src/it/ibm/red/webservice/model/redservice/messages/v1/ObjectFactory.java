
package it.ibm.red.webservice.model.redservice.messages.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import it.ibm.red.webservice.model.redservice.header.v1.CredenzialiUtenteRed;
import it.ibm.red.webservice.model.redservice.header.v1.CredenzialiWSRed;
import it.ibm.red.webservice.model.redservice.types.v1.DocumentoRed;
import it.ibm.red.webservice.model.redservice.types.v1.FascicoloRed;
import it.ibm.red.webservice.model.redservice.types.v1.IdentificativoUnitaDocumentaleFEPATypeRed;
import it.ibm.red.webservice.model.redservice.types.v1.MapRed;
import it.ibm.red.webservice.model.redservice.types.v1.MotivazioneAnnullamentoRed;
import it.ibm.red.webservice.model.redservice.types.v1.RispostaDocRed;
import it.ibm.red.webservice.model.redservice.types.v1.RispostaElencoVersioniDocRed;
import it.ibm.red.webservice.model.redservice.types.v1.RispostaRed;
import it.ibm.red.webservice.model.redservice.types.v1.RispostaVersioneDocRed;
import it.ibm.red.webservice.model.redservice.types.v1.VersioneDocumentoInputRed;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the red.messages.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


	/**
	 * Qname.
	 */
	private static final QName _CheckInRequestCredenzialiWS_QNAME = new QName("", "credenzialiWS");

	/**
	 * Qname.
	 */
	private static final QName _CheckInRequestDocumento_QNAME = new QName("", "documento");

	/**
	 * Qname.
	 */
	private static final QName _CheckInRequestCredenzialiUtente_QNAME = new QName("", "credenzialiUtente");

	/**
	 * Qname.
	 */
	private static final QName _AnnullaDocumentoRequestMotivazioneAnnullamento_QNAME = new QName("", "motivazioneAnnullamento");

	/**
	 * Qname.
	 */
	private static final QName _AssegnaFascicoloFatturaRequestMetadatiAssegnazione_QNAME = new QName("", "metadatiAssegnazione");

	/**
	 * Qname.
	 */
	private static final QName _AssegnaFascicoloFatturaRequestFascicoloDest_QNAME = new QName("", "fascicoloDest");

	/**
	 * Qname.
	 */
	private static final QName _ModificaMetadatiResponseReturn_QNAME = new QName("", "return");

	/**
	 * Qname.
	 */
	private static final QName _InserimentoFascicoloDecretoResponseIdVersione_QNAME = new QName("", "idVersione");

	/**
	 * Qname.
	 */
	private static final QName _GetVersioneDocumentoRequestVersioneDocumentoInput_QNAME = new QName("", "versioneDocumentoInput");

	/**
	 * Qname.
	 */
	private static final QName _UndoCheckOutRequestCredenzialiUtenteRed_QNAME = new QName("", "credenzialiUtenteRed");

	/**
	 * Qname.
	 */
	private static final QName _UndoCheckOutRequestCredenzialiWSR_QNAME = new QName("", "credenzialiWSR");

	/**
	 * Qname.
	 */
	private static final QName _GetDocumentiFascicoloRequestIdentificativoFascicolo_QNAME = new QName("", "identificativoFascicolo");

	/**
	 * Qname.
	 */
	private static final QName _ModificaFascicoloDecretoResponseCodiceErrore_QNAME = new QName("", "codiceErrore");

	/**
	 * Qname.
	 */
	private static final QName _AssegnaFascicoloFatturaResponseFascicolo_QNAME = new QName("", "fascicolo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: red.messages.v1
     * 
     */
    public ObjectFactory() {
    	// Costruttore di default.
    }

    /**
     * Create an instance of {@link ModificaFascicoloDecretoRequest }
     * 
     */
    public ModificaFascicoloDecretoRequest createModificaFascicoloDecretoRequest() {
        return new ModificaFascicoloDecretoRequest();
    }

    /**
     * Create an instance of {@link ModificaFascicoloDecretoRequest.FascicoliFattura }
     * 
     */
    public ModificaFascicoloDecretoRequest.FascicoliFattura createModificaFascicoloDecretoRequestFascicoliFattura() {
        return new ModificaFascicoloDecretoRequest.FascicoliFattura();
    }

    /**
     * Create an instance of {@link ModificaMetadatiRequest }
     * 
     */
    public ModificaMetadatiRequest createModificaMetadatiRequest() {
        return new ModificaMetadatiRequest();
    }

    /**
     * Create an instance of {@link GetVersioneDocumentoResponse }
     * 
     */
    public GetVersioneDocumentoResponse createGetVersioneDocumentoResponse() {
        return new GetVersioneDocumentoResponse();
    }

    /**
     * Create an instance of {@link AssegnaFascicoloFatturaResponse }
     * 
     */
    public AssegnaFascicoloFatturaResponse createAssegnaFascicoloFatturaResponse() {
        return new AssegnaFascicoloFatturaResponse();
    }

    /**
     * Create an instance of {@link ModificaFascicoloDecretoResponse }
     * 
     */
    public ModificaFascicoloDecretoResponse createModificaFascicoloDecretoResponse() {
        return new ModificaFascicoloDecretoResponse();
    }

    /**
     * Create an instance of {@link CheckOutRequest }
     * 
     */
    public CheckOutRequest createCheckOutRequest() {
        return new CheckOutRequest();
    }

    /**
     * Create an instance of {@link ElencoVersioniDocumentoResponse }
     * 
     */
    public ElencoVersioniDocumentoResponse createElencoVersioniDocumentoResponse() {
        return new ElencoVersioniDocumentoResponse();
    }

    /**
     * Create an instance of {@link InserimentoFascicoloDecretoRequest }
     * 
     */
    public InserimentoFascicoloDecretoRequest createInserimentoFascicoloDecretoRequest() {
        return new InserimentoFascicoloDecretoRequest();
    }

    /**
     * Create an instance of {@link CheckOutResponse }
     * 
     */
    public CheckOutResponse createCheckOutResponse() {
        return new CheckOutResponse();
    }

    /**
     * Create an instance of {@link ModificaMetadatiResponse }
     * 
     */
    public ModificaMetadatiResponse createModificaMetadatiResponse() {
        return new ModificaMetadatiResponse();
    }

    /**
     * Create an instance of {@link UndoCheckOutResponse }
     * 
     */
    public UndoCheckOutResponse createUndoCheckOutResponse() {
        return new UndoCheckOutResponse();
    }

    /**
     * Create an instance of {@link GetVersioneDocumentoRequest }
     * 
     */
    public GetVersioneDocumentoRequest createGetVersioneDocumentoRequest() {
        return new GetVersioneDocumentoRequest();
    }

    /**
     * Create an instance of {@link CheckInResponse }
     * 
     */
    public CheckInResponse createCheckInResponse() {
        return new CheckInResponse();
    }

    /**
     * Create an instance of {@link GetDocumentiFascicoloResponse }
     * 
     */
    public GetDocumentiFascicoloResponse createGetDocumentiFascicoloResponse() {
        return new GetDocumentiFascicoloResponse();
    }

    /**
     * Create an instance of {@link InserimentoFascicoloDecretoResponse }
     * 
     */
    public InserimentoFascicoloDecretoResponse createInserimentoFascicoloDecretoResponse() {
        return new InserimentoFascicoloDecretoResponse();
    }

    /**
     * Create an instance of {@link UndoCheckOutRequest }
     * 
     */
    public UndoCheckOutRequest createUndoCheckOutRequest() {
        return new UndoCheckOutRequest();
    }

    /**
     * Create an instance of {@link AssegnaFascicoloFatturaRequest }
     * 
     */
    public AssegnaFascicoloFatturaRequest createAssegnaFascicoloFatturaRequest() {
        return new AssegnaFascicoloFatturaRequest();
    }

    /**
     * Create an instance of {@link AnnullaDocumentoRequest }
     * 
     */
    public AnnullaDocumentoRequest createAnnullaDocumentoRequest() {
        return new AnnullaDocumentoRequest();
    }

    /**
     * Create an instance of {@link ElencoVersioniDocumentoRequest }
     * 
     */
    public ElencoVersioniDocumentoRequest createElencoVersioniDocumentoRequest() {
        return new ElencoVersioniDocumentoRequest();
    }

    /**
     * Create an instance of {@link AnnullaDocumentoResponse }
     * 
     */
    public AnnullaDocumentoResponse createAnnullaDocumentoResponse() {
        return new AnnullaDocumentoResponse();
    }

    /**
     * Create an instance of {@link CheckInRequest }
     * 
     */
    public CheckInRequest createCheckInRequest() {
        return new CheckInRequest();
    }

    /**
     * Create an instance of {@link GetDocumentiFascicoloRequest }
     * 
     */
    public GetDocumentiFascicoloRequest createGetDocumentiFascicoloRequest() {
        return new GetDocumentiFascicoloRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiWSRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiWS", scope = CheckInRequest.class)
    public JAXBElement<CredenzialiWSRed> createCheckInRequestCredenzialiWS(CredenzialiWSRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiWS_QNAME, CredenzialiWSRed.class, CheckInRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "documento", scope = CheckInRequest.class)
    public JAXBElement<DocumentoRed> createCheckInRequestDocumento(DocumentoRed value) {
        return new JAXBElement<>(_CheckInRequestDocumento_QNAME, DocumentoRed.class, CheckInRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiUtenteRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiUtente", scope = CheckInRequest.class)
    public JAXBElement<CredenzialiUtenteRed> createCheckInRequestCredenzialiUtente(CredenzialiUtenteRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiUtente_QNAME, CredenzialiUtenteRed.class, CheckInRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiWSRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiWS", scope = CheckOutRequest.class)
    public JAXBElement<CredenzialiWSRed> createCheckOutRequestCredenzialiWS(CredenzialiWSRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiWS_QNAME, CredenzialiWSRed.class, CheckOutRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "documento", scope = CheckOutRequest.class)
    public JAXBElement<DocumentoRed> createCheckOutRequestDocumento(DocumentoRed value) {
        return new JAXBElement<>(_CheckInRequestDocumento_QNAME, DocumentoRed.class, CheckOutRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiUtenteRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiUtente", scope = CheckOutRequest.class)
    public JAXBElement<CredenzialiUtenteRed> createCheckOutRequestCredenzialiUtente(CredenzialiUtenteRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiUtente_QNAME, CredenzialiUtenteRed.class, CheckOutRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiWSRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiWS", scope = ModificaFascicoloDecretoRequest.class)
    public JAXBElement<CredenzialiWSRed> createModificaFascicoloDecretoRequestCredenzialiWS(CredenzialiWSRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiWS_QNAME, CredenzialiWSRed.class, ModificaFascicoloDecretoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiUtenteRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiUtente", scope = ModificaFascicoloDecretoRequest.class)
    public JAXBElement<CredenzialiUtenteRed> createModificaFascicoloDecretoRequestCredenzialiUtente(CredenzialiUtenteRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiUtente_QNAME, CredenzialiUtenteRed.class, ModificaFascicoloDecretoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiWSRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiWS", scope = AnnullaDocumentoRequest.class)
    public JAXBElement<CredenzialiWSRed> createAnnullaDocumentoRequestCredenzialiWS(CredenzialiWSRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiWS_QNAME, CredenzialiWSRed.class, AnnullaDocumentoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "documento", scope = AnnullaDocumentoRequest.class)
    public JAXBElement<DocumentoRed> createAnnullaDocumentoRequestDocumento(DocumentoRed value) {
        return new JAXBElement<>(_CheckInRequestDocumento_QNAME, DocumentoRed.class, AnnullaDocumentoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MotivazioneAnnullamentoRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "motivazioneAnnullamento", scope = AnnullaDocumentoRequest.class)
    public JAXBElement<MotivazioneAnnullamentoRed> createAnnullaDocumentoRequestMotivazioneAnnullamento(MotivazioneAnnullamentoRed value) {
        return new JAXBElement<>(_AnnullaDocumentoRequestMotivazioneAnnullamento_QNAME, MotivazioneAnnullamentoRed.class, AnnullaDocumentoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiUtenteRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiUtente", scope = AnnullaDocumentoRequest.class)
    public JAXBElement<CredenzialiUtenteRed> createAnnullaDocumentoRequestCredenzialiUtente(CredenzialiUtenteRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiUtente_QNAME, CredenzialiUtenteRed.class, AnnullaDocumentoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MapRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "metadatiAssegnazione", scope = AssegnaFascicoloFatturaRequest.class)
    public JAXBElement<MapRed> createAssegnaFascicoloFatturaRequestMetadatiAssegnazione(MapRed value) {
        return new JAXBElement<>(_AssegnaFascicoloFatturaRequestMetadatiAssegnazione_QNAME, MapRed.class, AssegnaFascicoloFatturaRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiWSRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiWS", scope = AssegnaFascicoloFatturaRequest.class)
    public JAXBElement<CredenzialiWSRed> createAssegnaFascicoloFatturaRequestCredenzialiWS(CredenzialiWSRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiWS_QNAME, CredenzialiWSRed.class, AssegnaFascicoloFatturaRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IdentificativoUnitaDocumentaleFEPATypeRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fascicoloDest", scope = AssegnaFascicoloFatturaRequest.class)
    public JAXBElement<IdentificativoUnitaDocumentaleFEPATypeRed> createAssegnaFascicoloFatturaRequestFascicoloDest(IdentificativoUnitaDocumentaleFEPATypeRed value) {
        return new JAXBElement<>(_AssegnaFascicoloFatturaRequestFascicoloDest_QNAME, IdentificativoUnitaDocumentaleFEPATypeRed.class, AssegnaFascicoloFatturaRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiUtenteRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiUtente", scope = AssegnaFascicoloFatturaRequest.class)
    public JAXBElement<CredenzialiUtenteRed> createAssegnaFascicoloFatturaRequestCredenzialiUtente(CredenzialiUtenteRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiUtente_QNAME, CredenzialiUtenteRed.class, AssegnaFascicoloFatturaRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiWSRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiWS", scope = ElencoVersioniDocumentoRequest.class)
    public JAXBElement<CredenzialiWSRed> createElencoVersioniDocumentoRequestCredenzialiWS(CredenzialiWSRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiWS_QNAME, CredenzialiWSRed.class, ElencoVersioniDocumentoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "documento", scope = ElencoVersioniDocumentoRequest.class)
    public JAXBElement<DocumentoRed> createElencoVersioniDocumentoRequestDocumento(DocumentoRed value) {
        return new JAXBElement<>(_CheckInRequestDocumento_QNAME, DocumentoRed.class, ElencoVersioniDocumentoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiUtenteRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiUtente", scope = ElencoVersioniDocumentoRequest.class)
    public JAXBElement<CredenzialiUtenteRed> createElencoVersioniDocumentoRequestCredenzialiUtente(CredenzialiUtenteRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiUtente_QNAME, CredenzialiUtenteRed.class, ElencoVersioniDocumentoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "return", scope = ModificaMetadatiResponse.class)
    public JAXBElement<RispostaRed> createModificaMetadatiResponseReturn(RispostaRed value) {
        return new JAXBElement<>(_ModificaMetadatiResponseReturn_QNAME, RispostaRed.class, ModificaMetadatiResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "return", scope = CheckOutResponse.class)
    public JAXBElement<RispostaRed> createCheckOutResponseReturn(RispostaRed value) {
        return new JAXBElement<>(_ModificaMetadatiResponseReturn_QNAME, RispostaRed.class, CheckOutResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "idVersione", scope = InserimentoFascicoloDecretoResponse.class)
    public JAXBElement<Integer> createInserimentoFascicoloDecretoResponseIdVersione(Integer value) {
        return new JAXBElement<>(_InserimentoFascicoloDecretoResponseIdVersione_QNAME, Integer.class, InserimentoFascicoloDecretoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "return", scope = UndoCheckOutResponse.class)
    public JAXBElement<RispostaRed> createUndoCheckOutResponseReturn(RispostaRed value) {
        return new JAXBElement<>(_ModificaMetadatiResponseReturn_QNAME, RispostaRed.class, UndoCheckOutResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaDocRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "return", scope = AnnullaDocumentoResponse.class)
    public JAXBElement<RispostaDocRed> createAnnullaDocumentoResponseReturn(RispostaDocRed value) {
        return new JAXBElement<>(_ModificaMetadatiResponseReturn_QNAME, RispostaDocRed.class, AnnullaDocumentoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiWSRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiWS", scope = GetVersioneDocumentoRequest.class)
    public JAXBElement<CredenzialiWSRed> createGetVersioneDocumentoRequestCredenzialiWS(CredenzialiWSRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiWS_QNAME, CredenzialiWSRed.class, GetVersioneDocumentoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersioneDocumentoInputRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "versioneDocumentoInput", scope = GetVersioneDocumentoRequest.class)
    public JAXBElement<VersioneDocumentoInputRed> createGetVersioneDocumentoRequestVersioneDocumentoInput(VersioneDocumentoInputRed value) {
        return new JAXBElement<>(_GetVersioneDocumentoRequestVersioneDocumentoInput_QNAME, VersioneDocumentoInputRed.class, GetVersioneDocumentoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiUtenteRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiUtente", scope = GetVersioneDocumentoRequest.class)
    public JAXBElement<CredenzialiUtenteRed> createGetVersioneDocumentoRequestCredenzialiUtente(CredenzialiUtenteRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiUtente_QNAME, CredenzialiUtenteRed.class, GetVersioneDocumentoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaDocRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "return", scope = CheckInResponse.class)
    public JAXBElement<RispostaDocRed> createCheckInResponseReturn(RispostaDocRed value) {
        return new JAXBElement<>(_ModificaMetadatiResponseReturn_QNAME, RispostaDocRed.class, CheckInResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiWSRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiWS", scope = ModificaMetadatiRequest.class)
    public JAXBElement<CredenzialiWSRed> createModificaMetadatiRequestCredenzialiWS(CredenzialiWSRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiWS_QNAME, CredenzialiWSRed.class, ModificaMetadatiRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "documento", scope = ModificaMetadatiRequest.class)
    public JAXBElement<DocumentoRed> createModificaMetadatiRequestDocumento(DocumentoRed value) {
        return new JAXBElement<>(_CheckInRequestDocumento_QNAME, DocumentoRed.class, ModificaMetadatiRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiUtenteRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiUtente", scope = ModificaMetadatiRequest.class)
    public JAXBElement<CredenzialiUtenteRed> createModificaMetadatiRequestCredenzialiUtente(CredenzialiUtenteRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiUtente_QNAME, CredenzialiUtenteRed.class, ModificaMetadatiRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiUtenteRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiUtenteRed", scope = UndoCheckOutRequest.class)
    public JAXBElement<CredenzialiUtenteRed> createUndoCheckOutRequestCredenzialiUtenteRed(CredenzialiUtenteRed value) {
        return new JAXBElement<>(_UndoCheckOutRequestCredenzialiUtenteRed_QNAME, CredenzialiUtenteRed.class, UndoCheckOutRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "documento", scope = UndoCheckOutRequest.class)
    public JAXBElement<DocumentoRed> createUndoCheckOutRequestDocumento(DocumentoRed value) {
        return new JAXBElement<>(_CheckInRequestDocumento_QNAME, DocumentoRed.class, UndoCheckOutRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiWSRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiWSR", scope = UndoCheckOutRequest.class)
    public JAXBElement<CredenzialiWSRed> createUndoCheckOutRequestCredenzialiWSR(CredenzialiWSRed value) {
        return new JAXBElement<>(_UndoCheckOutRequestCredenzialiWSR_QNAME, CredenzialiWSRed.class, UndoCheckOutRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiWSRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiWS", scope = GetDocumentiFascicoloRequest.class)
    public JAXBElement<CredenzialiWSRed> createGetDocumentiFascicoloRequestCredenzialiWS(CredenzialiWSRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiWS_QNAME, CredenzialiWSRed.class, GetDocumentiFascicoloRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "identificativoFascicolo", scope = GetDocumentiFascicoloRequest.class)
    public JAXBElement<String> createGetDocumentiFascicoloRequestIdentificativoFascicolo(String value) {
        return new JAXBElement<>(_GetDocumentiFascicoloRequestIdentificativoFascicolo_QNAME, String.class, GetDocumentiFascicoloRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiUtenteRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiUtente", scope = GetDocumentiFascicoloRequest.class)
    public JAXBElement<CredenzialiUtenteRed> createGetDocumentiFascicoloRequestCredenzialiUtente(CredenzialiUtenteRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiUtente_QNAME, CredenzialiUtenteRed.class, GetDocumentiFascicoloRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codiceErrore", scope = ModificaFascicoloDecretoResponse.class)
    public JAXBElement<Integer> createModificaFascicoloDecretoResponseCodiceErrore(Integer value) {
        return new JAXBElement<>(_ModificaFascicoloDecretoResponseCodiceErrore_QNAME, Integer.class, ModificaFascicoloDecretoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaVersioneDocRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "return", scope = GetVersioneDocumentoResponse.class)
    public JAXBElement<RispostaVersioneDocRed> createGetVersioneDocumentoResponseReturn(RispostaVersioneDocRed value) {
        return new JAXBElement<>(_ModificaMetadatiResponseReturn_QNAME, RispostaVersioneDocRed.class, GetVersioneDocumentoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MapRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "metadatiAssegnazione", scope = InserimentoFascicoloDecretoRequest.class)
    public JAXBElement<MapRed> createInserimentoFascicoloDecretoRequestMetadatiAssegnazione(MapRed value) {
        return new JAXBElement<>(_AssegnaFascicoloFatturaRequestMetadatiAssegnazione_QNAME, MapRed.class, InserimentoFascicoloDecretoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiWSRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiWS", scope = InserimentoFascicoloDecretoRequest.class)
    public JAXBElement<CredenzialiWSRed> createInserimentoFascicoloDecretoRequestCredenzialiWS(CredenzialiWSRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiWS_QNAME, CredenzialiWSRed.class, InserimentoFascicoloDecretoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "documento", scope = InserimentoFascicoloDecretoRequest.class)
    public JAXBElement<DocumentoRed> createInserimentoFascicoloDecretoRequestDocumento(DocumentoRed value) {
        return new JAXBElement<>(_CheckInRequestDocumento_QNAME, DocumentoRed.class, InserimentoFascicoloDecretoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredenzialiUtenteRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "credenzialiUtente", scope = InserimentoFascicoloDecretoRequest.class)
    public JAXBElement<CredenzialiUtenteRed> createInserimentoFascicoloDecretoRequestCredenzialiUtente(CredenzialiUtenteRed value) {
        return new JAXBElement<>(_CheckInRequestCredenzialiUtente_QNAME, CredenzialiUtenteRed.class, InserimentoFascicoloDecretoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaElencoVersioniDocRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "return", scope = ElencoVersioniDocumentoResponse.class)
    public JAXBElement<RispostaElencoVersioniDocRed> createElencoVersioniDocumentoResponseReturn(RispostaElencoVersioniDocRed value) {
        return new JAXBElement<>(_ModificaMetadatiResponseReturn_QNAME, RispostaElencoVersioniDocRed.class, ElencoVersioniDocumentoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FascicoloRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fascicolo", scope = AssegnaFascicoloFatturaResponse.class)
    public JAXBElement<FascicoloRed> createAssegnaFascicoloFatturaResponseFascicolo(FascicoloRed value) {
        return new JAXBElement<>(_AssegnaFascicoloFatturaResponseFascicolo_QNAME, FascicoloRed.class, AssegnaFascicoloFatturaResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codiceErrore", scope = AssegnaFascicoloFatturaResponse.class)
    public JAXBElement<Integer> createAssegnaFascicoloFatturaResponseCodiceErrore(Integer value) {
        return new JAXBElement<>(_ModificaFascicoloDecretoResponseCodiceErrore_QNAME, Integer.class, AssegnaFascicoloFatturaResponse.class, value);
    }

}
