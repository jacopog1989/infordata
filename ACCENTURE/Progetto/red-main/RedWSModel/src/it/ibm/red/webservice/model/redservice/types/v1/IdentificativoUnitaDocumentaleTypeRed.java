
package it.ibm.red.webservice.model.redservice.types.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Identificativo dell'unita' documentale nella versione corrente (ID) oppure nella versione specificata (IDVERSIONE)
 * 
 * <p>Java class for IdentificativoUnitaDocumentaleTypeRed complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IdentificativoUnitaDocumentaleTypeRed">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="ID" type="{urn:red:types:v1}Guid"/>
 *         &lt;element name="IDVERSIONE" type="{urn:red:types:v1}Guid"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificativoUnitaDocumentaleTypeRed", propOrder = {
    "id",
    "idversione"
})
public class IdentificativoUnitaDocumentaleTypeRed {


    /**
     * Identificativo.
     */
    @XmlElement(name = "ID")
    protected String id;

    /**
     * Versione.
     */
    @XmlElement(name = "IDVERSIONE")
    protected String idversione;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the idversione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDVERSIONE() {
        return idversione;
    }

    /**
     * Sets the value of the idversione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDVERSIONE(String value) {
        this.idversione = value;
    }

}
