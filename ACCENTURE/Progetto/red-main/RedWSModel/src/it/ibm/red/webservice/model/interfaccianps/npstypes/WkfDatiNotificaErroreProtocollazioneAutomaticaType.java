
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraErroreProtocollazioneAutomaticaType;


/**
 * Dati per le notifiche di errore della protocollazione automatica da interoperabilità
 * 
 * <p>Classe Java per wkf_datiNotificaErroreProtocollazioneAutomatica_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_datiNotificaErroreProtocollazioneAutomatica_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}wkf_datiNotifica_type">
 *       &lt;sequence>
 *         &lt;element name="messaggioRicevuto" type="{http://mef.gov.it.v1.npsTypes}email_message_type"/>
 *         &lt;element name="NotificaInviata" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://mef.gov.it.v1.npsTypes}wkf_datiNotificaInviata_type">
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_datiNotificaErroreProtocollazioneAutomatica_type", propOrder = {
    "messaggioRicevuto",
    "notificaInviata"
})
@XmlSeeAlso({
    RichiestaElaboraErroreProtocollazioneAutomaticaType.class
})
public class WkfDatiNotificaErroreProtocollazioneAutomaticaType
    extends WkfDatiNotificaType
    implements Serializable
{

    @XmlElement(required = true)
    protected EmailMessageType messaggioRicevuto;
    @XmlElement(name = "NotificaInviata")
    protected WkfDatiNotificaErroreProtocollazioneAutomaticaType.NotificaInviata notificaInviata;

    /**
     * Recupera il valore della proprietà messaggioRicevuto.
     * 
     * @return
     *     possible object is
     *     {@link EmailMessageType }
     *     
     */
    public EmailMessageType getMessaggioRicevuto() {
        return messaggioRicevuto;
    }

    /**
     * Imposta il valore della proprietà messaggioRicevuto.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailMessageType }
     *     
     */
    public void setMessaggioRicevuto(EmailMessageType value) {
        this.messaggioRicevuto = value;
    }

    /**
     * Recupera il valore della proprietà notificaInviata.
     * 
     * @return
     *     possible object is
     *     {@link WkfDatiNotificaErroreProtocollazioneAutomaticaType.NotificaInviata }
     *     
     */
    public WkfDatiNotificaErroreProtocollazioneAutomaticaType.NotificaInviata getNotificaInviata() {
        return notificaInviata;
    }

    /**
     * Imposta il valore della proprietà notificaInviata.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfDatiNotificaErroreProtocollazioneAutomaticaType.NotificaInviata }
     *     
     */
    public void setNotificaInviata(WkfDatiNotificaErroreProtocollazioneAutomaticaType.NotificaInviata value) {
        this.notificaInviata = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}wkf_datiNotificaInviata_type">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class NotificaInviata
        extends WkfDatiNotificaInviataType
        implements Serializable
    {


    }

}
