//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.01.28 at 06:39:59 PM CET 
//


package it.ibm.red.webservice.model.documentservice.types.messages;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BaseDocumentoVersioneResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BaseDocumentoVersioneResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://messages.types.documentservice.model.webservice.red.ibm.it}BaseDocumentoResponseType">
 *       &lt;sequence>
 *         &lt;element name="idVersione" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseDocumentoVersioneResponseType", propOrder = {
    "idVersione"
})
@XmlSeeAlso({
    RedInserisciWatermarkResponseType.class,
    RedInserisciApprovazioneResponseType.class,
    RedCheckInResponseType.class,
    BaseRedContentResponseType.class
})
public class BaseDocumentoVersioneResponseType
    extends BaseDocumentoResponseType {

	/**
	 * Identificativo versione.
	 */
    protected Integer idVersione;

    /**
     * Gets the value of the idVersione property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdVersione() {
        return idVersione;
    }

    /**
     * Sets the value of the idVersione property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdVersione(Integer value) {
        this.idVersione = value;
    }

}
