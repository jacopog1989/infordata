
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per wkf_esitoNotificaOperazione_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="wkf_esitoNotificaOperazione_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="COMPLETAMENTO_PROTOCOLLO"/>
 *     &lt;enumeration value="COMPLETAMENTO_OPERAZIONE"/>
 *     &lt;enumeration value="SPEDIZIONE_DESTINATARIO"/>
 *     &lt;enumeration value="OPERAZIONE_ERRORE_CONVERSIONE"/>
 *     &lt;enumeration value="OPERAZIONE_ERRORE_TIMBRO"/>
 *     &lt;enumeration value="OPERAZIONE_ERRORE_INVIO_CONSERVAZIONE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "wkf_esitoNotificaOperazione_type")
@XmlEnum
public enum WkfEsitoNotificaOperazioneType {

    COMPLETAMENTO_PROTOCOLLO,
    COMPLETAMENTO_OPERAZIONE,
    SPEDIZIONE_DESTINATARIO,
    OPERAZIONE_ERRORE_CONVERSIONE,
    OPERAZIONE_ERRORE_TIMBRO,
    OPERAZIONE_ERRORE_INVIO_CONSERVAZIONE;

    public String value() {
        return name();
    }

    public static WkfEsitoNotificaOperazioneType fromValue(String v) {
        return valueOf(v);
    }

}
