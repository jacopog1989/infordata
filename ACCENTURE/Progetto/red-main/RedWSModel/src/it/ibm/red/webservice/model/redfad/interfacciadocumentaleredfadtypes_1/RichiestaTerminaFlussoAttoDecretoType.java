
package it.ibm.red.webservice.model.redfad.interfacciadocumentaleredfadtypes_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for richiesta_terminaFlussoAttoDecretoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_terminaFlussoAttoDecretoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloRaccoltaProvvisoria" type="{http://mef.gov.it.v1.red/servizi/InterfacciaDocumentaleREDFADTypes_1}Guid" maxOccurs="unbounded"/>
 *         &lt;element name="Stato" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_terminaFlussoAttoDecretoType", propOrder = {
    "idFascicoloRaccoltaProvvisoria",
    "stato"
})
public class RichiestaTerminaFlussoAttoDecretoType {

	/**
	 * Id fascicolo raccolta provvisoria.
	 */
    @XmlElement(name = "IdFascicoloRaccoltaProvvisoria", required = true)
    protected List<String> idFascicoloRaccoltaProvvisoria;

	/**
	 * Stato.
	 */
    @XmlElement(name = "Stato", required = true)
    protected String stato;

    /**
     * Gets the value of the idFascicoloRaccoltaProvvisoria property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idFascicoloRaccoltaProvvisoria property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdFascicoloRaccoltaProvvisoria().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getIdFascicoloRaccoltaProvvisoria() {
        if (idFascicoloRaccoltaProvvisoria == null) {
            idFascicoloRaccoltaProvvisoria = new ArrayList<>();
        }
        return this.idFascicoloRaccoltaProvvisoria;
    }

    /**
     * Gets the value of the stato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStato() {
        return stato;
    }

    /**
     * Sets the value of the stato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStato(String value) {
        this.stato = value;
    }

}
