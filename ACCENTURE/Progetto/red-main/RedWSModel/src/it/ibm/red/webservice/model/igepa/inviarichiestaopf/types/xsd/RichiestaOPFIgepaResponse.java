
package it.ibm.red.webservice.model.igepa.inviarichiestaopf.types.xsd;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per RichiestaOPFIgepaResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="RichiestaOPFIgepaResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="richiestaOPFIgepaResponse" type="{http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd}BaseResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RichiestaOPFIgepaResponse", propOrder = {
    "richiestaOPFIgepaResponse"
})
public class RichiestaOPFIgepaResponse
    implements Serializable
{

    @XmlElementRef(name = "richiestaOPFIgepaResponse", namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<BaseResponse> richiestaOPFIgepaResponse;

    /**
     * Recupera il valore della proprietà richiestaOPFIgepaResponse.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BaseResponse }{@code >}
     *     
     */
    public JAXBElement<BaseResponse> getRichiestaOPFIgepaResponse() {
        return richiestaOPFIgepaResponse;
    }

    /**
     * Imposta il valore della proprietà richiestaOPFIgepaResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BaseResponse }{@code >}
     *     
     */
    public void setRichiestaOPFIgepaResponse(JAXBElement<BaseResponse> value) {
        this.richiestaOPFIgepaResponse = value;
    }

}
