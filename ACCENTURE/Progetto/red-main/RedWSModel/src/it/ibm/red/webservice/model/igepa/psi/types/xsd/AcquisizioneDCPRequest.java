
package it.ibm.red.webservice.model.igepa.psi.types.xsd;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per AcquisizioneDCPRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="AcquisizioneDCPRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="acquisizioneDCPRequest" type="{http://types.ws.filenet.nsd.capgemini.com/xsd}DocumentoCertificazionePattoType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AcquisizioneDCPRequest", propOrder = {
    "acquisizioneDCPRequest"
})
public class AcquisizioneDCPRequest
    implements Serializable
{

    @XmlElementRef(name = "acquisizioneDCPRequest", namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<DocumentoCertificazionePattoType> acquisizioneDCPRequest;

    /**
     * Recupera il valore della proprietà acquisizioneDCPRequest.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DocumentoCertificazionePattoType }{@code >}
     *     
     */
    public JAXBElement<DocumentoCertificazionePattoType> getAcquisizioneDCPRequest() {
        return acquisizioneDCPRequest;
    }

    /**
     * Imposta il valore della proprietà acquisizioneDCPRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DocumentoCertificazionePattoType }{@code >}
     *     
     */
    public void setAcquisizioneDCPRequest(JAXBElement<DocumentoCertificazionePattoType> value) {
        this.acquisizioneDCPRequest = value;
    }

}
