//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.01.28 at 06:39:59 PM CET 
//


package it.ibm.red.webservice.model.documentservice.types.messages;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for redGetContattiType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="redGetContattiType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://messages.types.documentservice.model.webservice.red.ibm.it}BaseRequestType">
 *       &lt;sequence>
 *         &lt;element name="descrizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idAoo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idContatto" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="mail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoPersona" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ricercaRED" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ricercaIPA" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ricercaMEF" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "redGetContattiType", propOrder = {
    "descrizione",
    "idAoo",
    "idContatto",
    "mail",
    "tipoPersona",
    "ricercaRED",
    "ricercaIPA",
    "ricercaMEF"
})
public class RedGetContattiType extends BaseRequestType {


	/**
	 * Descrizione.
	 */
    protected String descrizione;

	/**
	 * Aoo.
	 */
    protected Integer idAoo;

	/**
	 * Contatto.
	 */
    protected Integer idContatto;

	/**
	 * Mail.
	 */
    protected String mail;

	/**
	 * Tipo persona.
	 */
    protected String tipoPersona;

	/**
	 * Ricerca RED.
	 */
    protected Boolean ricercaRED;

	/**
	 * Ricerca IPA.
	 */
    protected Boolean ricercaIPA;

	/**
	 * Ricerca MEF.
	 */
    protected Boolean ricercaMEF;

    /**
     * Gets the value of the descrizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Sets the value of the descrizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Gets the value of the idAoo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdAoo() {
        return idAoo;
    }

    /**
     * Sets the value of the idAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdAoo(Integer value) {
        this.idAoo = value;
    }

    /**
     * Gets the value of the idContatto property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdContatto() {
        return idContatto;
    }

    /**
     * Sets the value of the idContatto property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdContatto(Integer value) {
        this.idContatto = value;
    }

    /**
     * Gets the value of the mail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMail() {
        return mail;
    }

    /**
     * Sets the value of the mail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMail(String value) {
        this.mail = value;
    }

    /**
     * Gets the value of the tipoPersona property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoPersona() {
        return tipoPersona;
    }

    /**
     * Sets the value of the tipoPersona property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoPersona(String value) {
        this.tipoPersona = value;
    }

    /**
     * Gets the value of the ricercaRED property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRicercaRED() {
        return ricercaRED;
    }

    /**
     * Sets the value of the ricercaRED property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRicercaRED(Boolean value) {
        this.ricercaRED = value;
    }

    /**
     * Gets the value of the ricercaIPA property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRicercaIPA() {
        return ricercaIPA;
    }

    /**
     * Sets the value of the ricercaIPA property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRicercaIPA(Boolean value) {
        this.ricercaIPA = value;
    }

    /**
     * Gets the value of the ricercaMEF property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRicercaMEF() {
        return ricercaMEF;
    }

    /**
     * Sets the value of the ricercaMEF property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRicercaMEF(Boolean value) {
        this.ricercaMEF = value;
    }

}
