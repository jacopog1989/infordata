
package it.ibm.red.webservice.model.interfaccianps.npsmessages;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the npsmessages.v1.it.gov.mef package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RispostaElaboraComunicazioneGenericaFlusso_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_elaboraComunicazioneGenerica_Flusso");
    private final static QName _RichiestaElaboraMessaggioProtocollazioneFlusso_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_elaboraMessaggio_Protocollazione_Flusso");
    private final static QName _RispostaElaboraNotificaPEC_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_elaboraNotifica_PEC");
    private final static QName _RichiestaElaboraMessaggioProtocollazioneEmergenza_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_elaboraMessaggio_Protocollazione_Emergenza");
    private final static QName _RichiestaElaboraNotificaAzione_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_elaboraNotifica_Azione");
    private final static QName _RichiestaElaboraMessaggioPosta_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_elaboraMessaggio_Posta");
    private final static QName _RichiestaElaboraErroreProtocollazioneFlusso_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_elaboraErrore_Protocollazione_Flusso");
    private final static QName _RichiestaElaboraErroreFlusso_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_elaboraErrore_Flusso");
    private final static QName _RispostaElaboraMessaggioPosta_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_elaboraMessaggio_Posta");
    private final static QName _RichiestaElaboraNotificaInteroperabilitaProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_elaboraNotificaInteroperabilita_Protocollo");
    private final static QName _RispostaElaboraNotificaAzione_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_elaboraNotifica_Azione");
    private final static QName _RichiestaElaboraNotificaOperazione_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_elaboraNotifica_Operazione");
    private final static QName _RispostaElaboraErroreProtocollazioneAutomatica_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_elaboraErrore_Protocollazione_Automatica");
    private final static QName _RispostaElaboraNotificaOperazione_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_elaboraNotifica_Operazione");
    private final static QName _RispostaElaboraMessaggioProtocollazioneEmergenza_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_elaboraMessaggio_Protocollazione_Emergenza");
    private final static QName _RispostaElaboraErroreFlusso_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_elaboraErrore_Flusso");
    private final static QName _RispostaElaboraMessaggioProtocollazioneFlusso_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_elaboraMessaggio_Protocollazione_Flusso");
    private final static QName _RispostaElaboraMessaggioFlusso_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_elaboraMessaggio_Flusso");
    private final static QName _RispostaElaboraMessaggioProtocollazioneAutomatica_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_elaboraMessaggio_Protocollazione_Automatica");
    private final static QName _RichiestaElaboraMessaggioFlusso_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_elaboraMessaggio_Flusso");
    private final static QName _RichiestaElaboraMessaggioProtocollazioneAutomatica_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_elaboraMessaggio_Protocollazione_Automatica");
    private final static QName _RispostaElaboraErroreProtocollazioneFlusso_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_elaboraErrore_Protocollazione_Flusso");
    private final static QName _RichiestaElaboraComunicazioneGenericaFlusso_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_elaboraComunicazioneGenerica_Flusso");
    private final static QName _RichiestaElaboraNotificaPEC_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_elaboraNotifica_PEC");
    private final static QName _RichiestaElaboraErroreProtocollazioneAutomatica_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_elaboraErrore_Protocollazione_Automatica");
    private final static QName _RichiestaValidazioneStatoFlusso_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_validazioneStato_Flusso");
    private final static QName _RispostaElaboraNotificaInteroperabilitaProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_elaboraNotificaInteroperabilita_Protocollo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: npsmessages.v1.it.gov.mef
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RichiestaElaboraNotificaInteroperabilitaProtocolloType }
     * 
     */
    public RichiestaElaboraNotificaInteroperabilitaProtocolloType createRichiestaElaboraNotificaInteroperabilitaProtocolloType() {
        return new RichiestaElaboraNotificaInteroperabilitaProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaElaboraErroreProtocollazioneFlussoType }
     * 
     */
    public RispostaElaboraErroreProtocollazioneFlussoType createRispostaElaboraErroreProtocollazioneFlussoType() {
        return new RispostaElaboraErroreProtocollazioneFlussoType();
    }

    /**
     * Create an instance of {@link RispostaElaboraComunicazioneGenericaFlussoType }
     * 
     */
    public RispostaElaboraComunicazioneGenericaFlussoType createRispostaElaboraComunicazioneGenericaFlussoType() {
        return new RispostaElaboraComunicazioneGenericaFlussoType();
    }

    /**
     * Create an instance of {@link RichiestaElaboraMessaggioProtocollazioneFlussoType }
     * 
     */
    public RichiestaElaboraMessaggioProtocollazioneFlussoType createRichiestaElaboraMessaggioProtocollazioneFlussoType() {
        return new RichiestaElaboraMessaggioProtocollazioneFlussoType();
    }

    /**
     * Create an instance of {@link RispostaElaboraMessaggioProtocollazioneFlussoType }
     * 
     */
    public RispostaElaboraMessaggioProtocollazioneFlussoType createRispostaElaboraMessaggioProtocollazioneFlussoType() {
        return new RispostaElaboraMessaggioProtocollazioneFlussoType();
    }

    /**
     * Create an instance of {@link RispostaElaboraNotificaAzioneType }
     * 
     */
    public RispostaElaboraNotificaAzioneType createRispostaElaboraNotificaAzioneType() {
        return new RispostaElaboraNotificaAzioneType();
    }

    /**
     * Create an instance of {@link RispostaElaboraMessaggioFlussoType }
     * 
     */
    public RispostaElaboraMessaggioFlussoType createRispostaElaboraMessaggioFlussoType() {
        return new RispostaElaboraMessaggioFlussoType();
    }

    /**
     * Create an instance of {@link RispostaElaboraNotificaPECType }
     * 
     */
    public RispostaElaboraNotificaPECType createRispostaElaboraNotificaPECType() {
        return new RispostaElaboraNotificaPECType();
    }

    /**
     * Create an instance of {@link RispostaElaboraErroreProtocollazioneAutomaticaType }
     * 
     */
    public RispostaElaboraErroreProtocollazioneAutomaticaType createRispostaElaboraErroreProtocollazioneAutomaticaType() {
        return new RispostaElaboraErroreProtocollazioneAutomaticaType();
    }

    /**
     * Create an instance of {@link RispostaValidazioneStatoFlusso }
     * 
     */
    public RispostaValidazioneStatoFlusso createRispostaValidazioneStatoFlusso() {
        return new RispostaValidazioneStatoFlusso();
    }

    /**
     * Create an instance of {@link RispostaValidazioneStatoFlussoType }
     * 
     */
    public RispostaValidazioneStatoFlussoType createRispostaValidazioneStatoFlussoType() {
        return new RispostaValidazioneStatoFlussoType();
    }

    /**
     * Create an instance of {@link ServiceErrorType }
     * 
     */
    public ServiceErrorType createServiceErrorType() {
        return new ServiceErrorType();
    }

    /**
     * Create an instance of {@link RispostaElaboraMessaggioProtocollazioneAutomaticaType }
     * 
     */
    public RispostaElaboraMessaggioProtocollazioneAutomaticaType createRispostaElaboraMessaggioProtocollazioneAutomaticaType() {
        return new RispostaElaboraMessaggioProtocollazioneAutomaticaType();
    }

    /**
     * Create an instance of {@link RichiestaElaboraNotificaOperazioneType }
     * 
     */
    public RichiestaElaboraNotificaOperazioneType createRichiestaElaboraNotificaOperazioneType() {
        return new RichiestaElaboraNotificaOperazioneType();
    }

    /**
     * Create an instance of {@link RichiestaElaboraComunicazioneGenericaFlussoType }
     * 
     */
    public RichiestaElaboraComunicazioneGenericaFlussoType createRichiestaElaboraComunicazioneGenericaFlussoType() {
        return new RichiestaElaboraComunicazioneGenericaFlussoType();
    }

    /**
     * Create an instance of {@link RispostaElaboraNotificaOperazioneType }
     * 
     */
    public RispostaElaboraNotificaOperazioneType createRispostaElaboraNotificaOperazioneType() {
        return new RispostaElaboraNotificaOperazioneType();
    }

    /**
     * Create an instance of {@link RispostaElaboraMessaggioProtocollazioneEmergenzaType }
     * 
     */
    public RispostaElaboraMessaggioProtocollazioneEmergenzaType createRispostaElaboraMessaggioProtocollazioneEmergenzaType() {
        return new RispostaElaboraMessaggioProtocollazioneEmergenzaType();
    }

    /**
     * Create an instance of {@link RichiestaElaboraNotificaPECType }
     * 
     */
    public RichiestaElaboraNotificaPECType createRichiestaElaboraNotificaPECType() {
        return new RichiestaElaboraNotificaPECType();
    }

    /**
     * Create an instance of {@link RichiestaElaboraMessaggioProtocollazioneEmergenzaType }
     * 
     */
    public RichiestaElaboraMessaggioProtocollazioneEmergenzaType createRichiestaElaboraMessaggioProtocollazioneEmergenzaType() {
        return new RichiestaElaboraMessaggioProtocollazioneEmergenzaType();
    }

    /**
     * Create an instance of {@link RichiestaElaboraNotificaAzioneType }
     * 
     */
    public RichiestaElaboraNotificaAzioneType createRichiestaElaboraNotificaAzioneType() {
        return new RichiestaElaboraNotificaAzioneType();
    }

    /**
     * Create an instance of {@link RichiestaElaboraErroreProtocollazioneFlussoType }
     * 
     */
    public RichiestaElaboraErroreProtocollazioneFlussoType createRichiestaElaboraErroreProtocollazioneFlussoType() {
        return new RichiestaElaboraErroreProtocollazioneFlussoType();
    }

    /**
     * Create an instance of {@link RichiestaElaboraErroreProtocollazioneAutomaticaType }
     * 
     */
    public RichiestaElaboraErroreProtocollazioneAutomaticaType createRichiestaElaboraErroreProtocollazioneAutomaticaType() {
        return new RichiestaElaboraErroreProtocollazioneAutomaticaType();
    }

    /**
     * Create an instance of {@link RichiestaValidazioneStatoFlussoType }
     * 
     */
    public RichiestaValidazioneStatoFlussoType createRichiestaValidazioneStatoFlussoType() {
        return new RichiestaValidazioneStatoFlussoType();
    }

    /**
     * Create an instance of {@link RichiestaElaboraMessaggioPostaType }
     * 
     */
    public RichiestaElaboraMessaggioPostaType createRichiestaElaboraMessaggioPostaType() {
        return new RichiestaElaboraMessaggioPostaType();
    }

    /**
     * Create an instance of {@link RichiestaElaboraErroreFlussoType }
     * 
     */
    public RichiestaElaboraErroreFlussoType createRichiestaElaboraErroreFlussoType() {
        return new RichiestaElaboraErroreFlussoType();
    }

    /**
     * Create an instance of {@link RispostaElaboraErroreFlussoType }
     * 
     */
    public RispostaElaboraErroreFlussoType createRispostaElaboraErroreFlussoType() {
        return new RispostaElaboraErroreFlussoType();
    }

    /**
     * Create an instance of {@link RispostaElaboraMessaggioPostaType }
     * 
     */
    public RispostaElaboraMessaggioPostaType createRispostaElaboraMessaggioPostaType() {
        return new RispostaElaboraMessaggioPostaType();
    }

    /**
     * Create an instance of {@link RispostaElaboraNotificaInteroperabilitaProtocolloType }
     * 
     */
    public RispostaElaboraNotificaInteroperabilitaProtocolloType createRispostaElaboraNotificaInteroperabilitaProtocolloType() {
        return new RispostaElaboraNotificaInteroperabilitaProtocolloType();
    }

    /**
     * Create an instance of {@link RichiestaElaboraMessaggioFlussoType }
     * 
     */
    public RichiestaElaboraMessaggioFlussoType createRichiestaElaboraMessaggioFlussoType() {
        return new RichiestaElaboraMessaggioFlussoType();
    }

    /**
     * Create an instance of {@link RichiestaElaboraMessaggioProtocollazioneAutomaticaType }
     * 
     */
    public RichiestaElaboraMessaggioProtocollazioneAutomaticaType createRichiestaElaboraMessaggioProtocollazioneAutomaticaType() {
        return new RichiestaElaboraMessaggioProtocollazioneAutomaticaType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaElaboraComunicazioneGenericaFlussoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_elaboraComunicazioneGenerica_Flusso")
    public JAXBElement<RispostaElaboraComunicazioneGenericaFlussoType> createRispostaElaboraComunicazioneGenericaFlusso(RispostaElaboraComunicazioneGenericaFlussoType value) {
        return new JAXBElement<RispostaElaboraComunicazioneGenericaFlussoType>(_RispostaElaboraComunicazioneGenericaFlusso_QNAME, RispostaElaboraComunicazioneGenericaFlussoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaElaboraMessaggioProtocollazioneFlussoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_elaboraMessaggio_Protocollazione_Flusso")
    public JAXBElement<RichiestaElaboraMessaggioProtocollazioneFlussoType> createRichiestaElaboraMessaggioProtocollazioneFlusso(RichiestaElaboraMessaggioProtocollazioneFlussoType value) {
        return new JAXBElement<RichiestaElaboraMessaggioProtocollazioneFlussoType>(_RichiestaElaboraMessaggioProtocollazioneFlusso_QNAME, RichiestaElaboraMessaggioProtocollazioneFlussoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaElaboraNotificaPECType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_elaboraNotifica_PEC")
    public JAXBElement<RispostaElaboraNotificaPECType> createRispostaElaboraNotificaPEC(RispostaElaboraNotificaPECType value) {
        return new JAXBElement<RispostaElaboraNotificaPECType>(_RispostaElaboraNotificaPEC_QNAME, RispostaElaboraNotificaPECType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaElaboraMessaggioProtocollazioneEmergenzaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_elaboraMessaggio_Protocollazione_Emergenza")
    public JAXBElement<RichiestaElaboraMessaggioProtocollazioneEmergenzaType> createRichiestaElaboraMessaggioProtocollazioneEmergenza(RichiestaElaboraMessaggioProtocollazioneEmergenzaType value) {
        return new JAXBElement<RichiestaElaboraMessaggioProtocollazioneEmergenzaType>(_RichiestaElaboraMessaggioProtocollazioneEmergenza_QNAME, RichiestaElaboraMessaggioProtocollazioneEmergenzaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaElaboraNotificaAzioneType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_elaboraNotifica_Azione")
    public JAXBElement<RichiestaElaboraNotificaAzioneType> createRichiestaElaboraNotificaAzione(RichiestaElaboraNotificaAzioneType value) {
        return new JAXBElement<RichiestaElaboraNotificaAzioneType>(_RichiestaElaboraNotificaAzione_QNAME, RichiestaElaboraNotificaAzioneType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaElaboraMessaggioPostaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_elaboraMessaggio_Posta")
    public JAXBElement<RichiestaElaboraMessaggioPostaType> createRichiestaElaboraMessaggioPosta(RichiestaElaboraMessaggioPostaType value) {
        return new JAXBElement<RichiestaElaboraMessaggioPostaType>(_RichiestaElaboraMessaggioPosta_QNAME, RichiestaElaboraMessaggioPostaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaElaboraErroreProtocollazioneFlussoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_elaboraErrore_Protocollazione_Flusso")
    public JAXBElement<RichiestaElaboraErroreProtocollazioneFlussoType> createRichiestaElaboraErroreProtocollazioneFlusso(RichiestaElaboraErroreProtocollazioneFlussoType value) {
        return new JAXBElement<RichiestaElaboraErroreProtocollazioneFlussoType>(_RichiestaElaboraErroreProtocollazioneFlusso_QNAME, RichiestaElaboraErroreProtocollazioneFlussoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaElaboraErroreFlussoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_elaboraErrore_Flusso")
    public JAXBElement<RichiestaElaboraErroreFlussoType> createRichiestaElaboraErroreFlusso(RichiestaElaboraErroreFlussoType value) {
        return new JAXBElement<RichiestaElaboraErroreFlussoType>(_RichiestaElaboraErroreFlusso_QNAME, RichiestaElaboraErroreFlussoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaElaboraMessaggioPostaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_elaboraMessaggio_Posta")
    public JAXBElement<RispostaElaboraMessaggioPostaType> createRispostaElaboraMessaggioPosta(RispostaElaboraMessaggioPostaType value) {
        return new JAXBElement<RispostaElaboraMessaggioPostaType>(_RispostaElaboraMessaggioPosta_QNAME, RispostaElaboraMessaggioPostaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaElaboraNotificaInteroperabilitaProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_elaboraNotificaInteroperabilita_Protocollo")
    public JAXBElement<RichiestaElaboraNotificaInteroperabilitaProtocolloType> createRichiestaElaboraNotificaInteroperabilitaProtocollo(RichiestaElaboraNotificaInteroperabilitaProtocolloType value) {
        return new JAXBElement<RichiestaElaboraNotificaInteroperabilitaProtocolloType>(_RichiestaElaboraNotificaInteroperabilitaProtocollo_QNAME, RichiestaElaboraNotificaInteroperabilitaProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaElaboraNotificaAzioneType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_elaboraNotifica_Azione")
    public JAXBElement<RispostaElaboraNotificaAzioneType> createRispostaElaboraNotificaAzione(RispostaElaboraNotificaAzioneType value) {
        return new JAXBElement<RispostaElaboraNotificaAzioneType>(_RispostaElaboraNotificaAzione_QNAME, RispostaElaboraNotificaAzioneType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaElaboraNotificaOperazioneType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_elaboraNotifica_Operazione")
    public JAXBElement<RichiestaElaboraNotificaOperazioneType> createRichiestaElaboraNotificaOperazione(RichiestaElaboraNotificaOperazioneType value) {
        return new JAXBElement<RichiestaElaboraNotificaOperazioneType>(_RichiestaElaboraNotificaOperazione_QNAME, RichiestaElaboraNotificaOperazioneType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaElaboraErroreProtocollazioneAutomaticaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_elaboraErrore_Protocollazione_Automatica")
    public JAXBElement<RispostaElaboraErroreProtocollazioneAutomaticaType> createRispostaElaboraErroreProtocollazioneAutomatica(RispostaElaboraErroreProtocollazioneAutomaticaType value) {
        return new JAXBElement<RispostaElaboraErroreProtocollazioneAutomaticaType>(_RispostaElaboraErroreProtocollazioneAutomatica_QNAME, RispostaElaboraErroreProtocollazioneAutomaticaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaElaboraNotificaOperazioneType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_elaboraNotifica_Operazione")
    public JAXBElement<RispostaElaboraNotificaOperazioneType> createRispostaElaboraNotificaOperazione(RispostaElaboraNotificaOperazioneType value) {
        return new JAXBElement<RispostaElaboraNotificaOperazioneType>(_RispostaElaboraNotificaOperazione_QNAME, RispostaElaboraNotificaOperazioneType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaElaboraMessaggioProtocollazioneEmergenzaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_elaboraMessaggio_Protocollazione_Emergenza")
    public JAXBElement<RispostaElaboraMessaggioProtocollazioneEmergenzaType> createRispostaElaboraMessaggioProtocollazioneEmergenza(RispostaElaboraMessaggioProtocollazioneEmergenzaType value) {
        return new JAXBElement<RispostaElaboraMessaggioProtocollazioneEmergenzaType>(_RispostaElaboraMessaggioProtocollazioneEmergenza_QNAME, RispostaElaboraMessaggioProtocollazioneEmergenzaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaElaboraErroreFlussoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_elaboraErrore_Flusso")
    public JAXBElement<RispostaElaboraErroreFlussoType> createRispostaElaboraErroreFlusso(RispostaElaboraErroreFlussoType value) {
        return new JAXBElement<RispostaElaboraErroreFlussoType>(_RispostaElaboraErroreFlusso_QNAME, RispostaElaboraErroreFlussoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaElaboraMessaggioProtocollazioneFlussoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_elaboraMessaggio_Protocollazione_Flusso")
    public JAXBElement<RispostaElaboraMessaggioProtocollazioneFlussoType> createRispostaElaboraMessaggioProtocollazioneFlusso(RispostaElaboraMessaggioProtocollazioneFlussoType value) {
        return new JAXBElement<RispostaElaboraMessaggioProtocollazioneFlussoType>(_RispostaElaboraMessaggioProtocollazioneFlusso_QNAME, RispostaElaboraMessaggioProtocollazioneFlussoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaElaboraMessaggioFlussoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_elaboraMessaggio_Flusso")
    public JAXBElement<RispostaElaboraMessaggioFlussoType> createRispostaElaboraMessaggioFlusso(RispostaElaboraMessaggioFlussoType value) {
        return new JAXBElement<RispostaElaboraMessaggioFlussoType>(_RispostaElaboraMessaggioFlusso_QNAME, RispostaElaboraMessaggioFlussoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaElaboraMessaggioProtocollazioneAutomaticaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_elaboraMessaggio_Protocollazione_Automatica")
    public JAXBElement<RispostaElaboraMessaggioProtocollazioneAutomaticaType> createRispostaElaboraMessaggioProtocollazioneAutomatica(RispostaElaboraMessaggioProtocollazioneAutomaticaType value) {
        return new JAXBElement<RispostaElaboraMessaggioProtocollazioneAutomaticaType>(_RispostaElaboraMessaggioProtocollazioneAutomatica_QNAME, RispostaElaboraMessaggioProtocollazioneAutomaticaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaElaboraMessaggioFlussoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_elaboraMessaggio_Flusso")
    public JAXBElement<RichiestaElaboraMessaggioFlussoType> createRichiestaElaboraMessaggioFlusso(RichiestaElaboraMessaggioFlussoType value) {
        return new JAXBElement<RichiestaElaboraMessaggioFlussoType>(_RichiestaElaboraMessaggioFlusso_QNAME, RichiestaElaboraMessaggioFlussoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaElaboraMessaggioProtocollazioneAutomaticaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_elaboraMessaggio_Protocollazione_Automatica")
    public JAXBElement<RichiestaElaboraMessaggioProtocollazioneAutomaticaType> createRichiestaElaboraMessaggioProtocollazioneAutomatica(RichiestaElaboraMessaggioProtocollazioneAutomaticaType value) {
        return new JAXBElement<RichiestaElaboraMessaggioProtocollazioneAutomaticaType>(_RichiestaElaboraMessaggioProtocollazioneAutomatica_QNAME, RichiestaElaboraMessaggioProtocollazioneAutomaticaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaElaboraErroreProtocollazioneFlussoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_elaboraErrore_Protocollazione_Flusso")
    public JAXBElement<RispostaElaboraErroreProtocollazioneFlussoType> createRispostaElaboraErroreProtocollazioneFlusso(RispostaElaboraErroreProtocollazioneFlussoType value) {
        return new JAXBElement<RispostaElaboraErroreProtocollazioneFlussoType>(_RispostaElaboraErroreProtocollazioneFlusso_QNAME, RispostaElaboraErroreProtocollazioneFlussoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaElaboraComunicazioneGenericaFlussoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_elaboraComunicazioneGenerica_Flusso")
    public JAXBElement<RichiestaElaboraComunicazioneGenericaFlussoType> createRichiestaElaboraComunicazioneGenericaFlusso(RichiestaElaboraComunicazioneGenericaFlussoType value) {
        return new JAXBElement<RichiestaElaboraComunicazioneGenericaFlussoType>(_RichiestaElaboraComunicazioneGenericaFlusso_QNAME, RichiestaElaboraComunicazioneGenericaFlussoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaElaboraNotificaPECType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_elaboraNotifica_PEC")
    public JAXBElement<RichiestaElaboraNotificaPECType> createRichiestaElaboraNotificaPEC(RichiestaElaboraNotificaPECType value) {
        return new JAXBElement<RichiestaElaboraNotificaPECType>(_RichiestaElaboraNotificaPEC_QNAME, RichiestaElaboraNotificaPECType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaElaboraErroreProtocollazioneAutomaticaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_elaboraErrore_Protocollazione_Automatica")
    public JAXBElement<RichiestaElaboraErroreProtocollazioneAutomaticaType> createRichiestaElaboraErroreProtocollazioneAutomatica(RichiestaElaboraErroreProtocollazioneAutomaticaType value) {
        return new JAXBElement<RichiestaElaboraErroreProtocollazioneAutomaticaType>(_RichiestaElaboraErroreProtocollazioneAutomatica_QNAME, RichiestaElaboraErroreProtocollazioneAutomaticaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaValidazioneStatoFlussoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_validazioneStato_Flusso")
    public JAXBElement<RichiestaValidazioneStatoFlussoType> createRichiestaValidazioneStatoFlusso(RichiestaValidazioneStatoFlussoType value) {
        return new JAXBElement<RichiestaValidazioneStatoFlussoType>(_RichiestaValidazioneStatoFlusso_QNAME, RichiestaValidazioneStatoFlussoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaElaboraNotificaInteroperabilitaProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_elaboraNotificaInteroperabilita_Protocollo")
    public JAXBElement<RispostaElaboraNotificaInteroperabilitaProtocolloType> createRispostaElaboraNotificaInteroperabilitaProtocollo(RispostaElaboraNotificaInteroperabilitaProtocolloType value) {
        return new JAXBElement<RispostaElaboraNotificaInteroperabilitaProtocolloType>(_RispostaElaboraNotificaInteroperabilitaProtocollo_QNAME, RispostaElaboraNotificaInteroperabilitaProtocolloType.class, null, value);
    }

}
