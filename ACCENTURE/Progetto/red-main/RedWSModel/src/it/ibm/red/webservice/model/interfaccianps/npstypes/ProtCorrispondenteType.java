
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce i metadati del mittente
 * 
 * <p>Classe Java per prot_corrispondente_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_corrispondente_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="PersonaFisica" type="{http://mef.gov.it.v1.npsTypes}ana_personaFisica_type"/>
 *           &lt;element name="PersonaGiuridica" type="{http://mef.gov.it.v1.npsTypes}ana_personaGiuridica_type"/>
 *           &lt;element name="Ente" type="{http://mef.gov.it.v1.npsTypes}ana_enteEsterno_type"/>
 *         &lt;/choice>
 *         &lt;element name="IndirizzoTelematico" type="{http://mef.gov.it.v1.npsTypes}ana_indirizzoTelematico_type" minOccurs="0"/>
 *         &lt;element name="Telefono" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="Fax" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="IndirizzoPostale" type="{http://mef.gov.it.v1.npsTypes}ana_indirizzoPostale_type" minOccurs="0"/>
 *         &lt;element name="DatiAnnullamento" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DataAnnullamento" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_corrispondente_type", propOrder = {
    "personaFisica",
    "personaGiuridica",
    "ente",
    "indirizzoTelematico",
    "telefono",
    "fax",
    "indirizzoPostale",
    "datiAnnullamento"
})
@XmlSeeAlso({
    ProtDestinatarioType.class,
    ProtMittenteType.class
})
public class ProtCorrispondenteType
    implements Serializable
{

    @XmlElement(name = "PersonaFisica")
    protected AnaPersonaFisicaType personaFisica;
    @XmlElement(name = "PersonaGiuridica")
    protected AnaPersonaGiuridicaType personaGiuridica;
    @XmlElement(name = "Ente")
    protected AnaEnteEsternoType ente;
    @XmlElement(name = "IndirizzoTelematico")
    protected AnaIndirizzoTelematicoType indirizzoTelematico;
    @XmlElement(name = "Telefono")
    protected Object telefono;
    @XmlElement(name = "Fax")
    protected Object fax;
    @XmlElement(name = "IndirizzoPostale")
    protected AnaIndirizzoPostaleType indirizzoPostale;
    @XmlElement(name = "DatiAnnullamento")
    protected ProtCorrispondenteType.DatiAnnullamento datiAnnullamento;

    /**
     * Recupera il valore della proprietà personaFisica.
     * 
     * @return
     *     possible object is
     *     {@link AnaPersonaFisicaType }
     *     
     */
    public AnaPersonaFisicaType getPersonaFisica() {
        return personaFisica;
    }

    /**
     * Imposta il valore della proprietà personaFisica.
     * 
     * @param value
     *     allowed object is
     *     {@link AnaPersonaFisicaType }
     *     
     */
    public void setPersonaFisica(AnaPersonaFisicaType value) {
        this.personaFisica = value;
    }

    /**
     * Recupera il valore della proprietà personaGiuridica.
     * 
     * @return
     *     possible object is
     *     {@link AnaPersonaGiuridicaType }
     *     
     */
    public AnaPersonaGiuridicaType getPersonaGiuridica() {
        return personaGiuridica;
    }

    /**
     * Imposta il valore della proprietà personaGiuridica.
     * 
     * @param value
     *     allowed object is
     *     {@link AnaPersonaGiuridicaType }
     *     
     */
    public void setPersonaGiuridica(AnaPersonaGiuridicaType value) {
        this.personaGiuridica = value;
    }

    /**
     * Recupera il valore della proprietà ente.
     * 
     * @return
     *     possible object is
     *     {@link AnaEnteEsternoType }
     *     
     */
    public AnaEnteEsternoType getEnte() {
        return ente;
    }

    /**
     * Imposta il valore della proprietà ente.
     * 
     * @param value
     *     allowed object is
     *     {@link AnaEnteEsternoType }
     *     
     */
    public void setEnte(AnaEnteEsternoType value) {
        this.ente = value;
    }

    /**
     * Recupera il valore della proprietà indirizzoTelematico.
     * 
     * @return
     *     possible object is
     *     {@link AnaIndirizzoTelematicoType }
     *     
     */
    public AnaIndirizzoTelematicoType getIndirizzoTelematico() {
        return indirizzoTelematico;
    }

    /**
     * Imposta il valore della proprietà indirizzoTelematico.
     * 
     * @param value
     *     allowed object is
     *     {@link AnaIndirizzoTelematicoType }
     *     
     */
    public void setIndirizzoTelematico(AnaIndirizzoTelematicoType value) {
        this.indirizzoTelematico = value;
    }

    /**
     * Recupera il valore della proprietà telefono.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getTelefono() {
        return telefono;
    }

    /**
     * Imposta il valore della proprietà telefono.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setTelefono(Object value) {
        this.telefono = value;
    }

    /**
     * Recupera il valore della proprietà fax.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getFax() {
        return fax;
    }

    /**
     * Imposta il valore della proprietà fax.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setFax(Object value) {
        this.fax = value;
    }

    /**
     * Recupera il valore della proprietà indirizzoPostale.
     * 
     * @return
     *     possible object is
     *     {@link AnaIndirizzoPostaleType }
     *     
     */
    public AnaIndirizzoPostaleType getIndirizzoPostale() {
        return indirizzoPostale;
    }

    /**
     * Imposta il valore della proprietà indirizzoPostale.
     * 
     * @param value
     *     allowed object is
     *     {@link AnaIndirizzoPostaleType }
     *     
     */
    public void setIndirizzoPostale(AnaIndirizzoPostaleType value) {
        this.indirizzoPostale = value;
    }

    /**
     * Recupera il valore della proprietà datiAnnullamento.
     * 
     * @return
     *     possible object is
     *     {@link ProtCorrispondenteType.DatiAnnullamento }
     *     
     */
    public ProtCorrispondenteType.DatiAnnullamento getDatiAnnullamento() {
        return datiAnnullamento;
    }

    /**
     * Imposta il valore della proprietà datiAnnullamento.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtCorrispondenteType.DatiAnnullamento }
     *     
     */
    public void setDatiAnnullamento(ProtCorrispondenteType.DatiAnnullamento value) {
        this.datiAnnullamento = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DataAnnullamento" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dataAnnullamento"
    })
    public static class DatiAnnullamento
        implements Serializable
    {

        @XmlElement(name = "DataAnnullamento", required = true)
        protected Object dataAnnullamento;

        /**
         * Recupera il valore della proprietà dataAnnullamento.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getDataAnnullamento() {
            return dataAnnullamento;
        }

        /**
         * Imposta il valore della proprietà dataAnnullamento.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setDataAnnullamento(Object value) {
            this.dataAnnullamento = value;
        }

    }

}
