
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Dati per la notifica di rimozione di un destinatario di un protocollo
 * 
 * <p>Classe Java per wkf_azioneRimozioneDestinatarioProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_azioneRimozioneDestinatarioProtocollo_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}identificativoDestinatario_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_azioneRimozioneDestinatarioProtocollo_type")
public class WkfAzioneRimozioneDestinatarioProtocolloType
    extends IdentificativoDestinatarioType
    implements Serializable
{


}
