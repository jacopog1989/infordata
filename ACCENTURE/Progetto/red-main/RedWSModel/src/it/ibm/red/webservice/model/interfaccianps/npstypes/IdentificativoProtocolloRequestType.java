
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * identificativo del protocollo da utilizzare nelle richieste
 * 
 * <p>Classe Java per identificativoProtocolloRequest_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="identificativoProtocolloRequest_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="IdProtocollo" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;sequence>
 *           &lt;element name="Registro" type="{http://mef.gov.it.v1.npsTypes}prot_registroProtocollo_type"/>
 *           &lt;element name="DataRegistrazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *           &lt;element name="NumeroRegistrazione" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "identificativoProtocolloRequest_type", propOrder = {
    "idProtocollo",
    "registro",
    "dataRegistrazione",
    "numeroRegistrazione"
})
public class IdentificativoProtocolloRequestType
    implements Serializable
{

    @XmlElement(name = "IdProtocollo")
    protected String idProtocollo;
    @XmlElement(name = "Registro")
    protected ProtRegistroProtocolloType registro;
    @XmlElement(name = "DataRegistrazione")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataRegistrazione;
    @XmlElement(name = "NumeroRegistrazione")
    protected Integer numeroRegistrazione;

    /**
     * Recupera il valore della proprietà idProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdProtocollo() {
        return idProtocollo;
    }

    /**
     * Imposta il valore della proprietà idProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdProtocollo(String value) {
        this.idProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà registro.
     * 
     * @return
     *     possible object is
     *     {@link ProtRegistroProtocolloType }
     *     
     */
    public ProtRegistroProtocolloType getRegistro() {
        return registro;
    }

    /**
     * Imposta il valore della proprietà registro.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtRegistroProtocolloType }
     *     
     */
    public void setRegistro(ProtRegistroProtocolloType value) {
        this.registro = value;
    }

    /**
     * Recupera il valore della proprietà dataRegistrazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataRegistrazione() {
        return dataRegistrazione;
    }

    /**
     * Imposta il valore della proprietà dataRegistrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataRegistrazione(XMLGregorianCalendar value) {
        this.dataRegistrazione = value;
    }

    /**
     * Recupera il valore della proprietà numeroRegistrazione.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroRegistrazione() {
        return numeroRegistrazione;
    }

    /**
     * Imposta il valore della proprietà numeroRegistrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroRegistrazione(Integer value) {
        this.numeroRegistrazione = value;
    }

}
