
package it.ibm.red.webservice.model.redservice.types.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RispostaElencoVersioniDocRed complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RispostaElencoVersioniDocRed">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:red:types:v1}RispostaRed">
 *       &lt;sequence>
 *         &lt;element name="versioniDocumento" type="{urn:red:types:v1}DocumentoRed" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RispostaElencoVersioniDocRed", propOrder = {
    "versioniDocumento"
})
public class RispostaElencoVersioniDocRed extends RispostaRed {

    /**
     * Versioni documento.
     */
    @XmlElement(nillable = true)
    protected List<DocumentoRed> versioniDocumento;

    /**
     * Gets the value of the versioniDocumento property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the versioniDocumento property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVersioniDocumento().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentoRed }
     * 
     * 
     */
    public List<DocumentoRed> getVersioniDocumento() {
        if (versioniDocumento == null) {
            versioniDocumento = new ArrayList<>();
        }
        return this.versioniDocumento;
    }

}
