
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Dati per la notifica della modifica della spedixione di un destinatario Elettronico
 * 
 * <p>Classe Java per wkf_azioneSpedizioneDestinatarioElettronicoProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_azioneSpedizioneDestinatarioElettronicoProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Destinatario" type="{http://mef.gov.it.v1.npsTypes}identificativoDestinatario_type"/>
 *         &lt;element name="IndirizzoTelematico" type="{http://mef.gov.it.v1.npsTypes}ana_indirizzoTelematico_type" minOccurs="0"/>
 *         &lt;element name="DataSpedizione" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="StatoSpedizione">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="DA_SPEDIRE"/>
 *               &lt;enumeration value="IN_SPEDIZIONE"/>
 *               &lt;enumeration value="SPEDITO"/>
 *               &lt;enumeration value="ERRORE"/>
 *               &lt;enumeration value="RICEVUTA_ACCETTAZIONE"/>
 *               &lt;enumeration value="RICEVUTA_CONSEGNA"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_azioneSpedizioneDestinatarioElettronicoProtocollo_type", propOrder = {
    "destinatario",
    "indirizzoTelematico",
    "dataSpedizione",
    "statoSpedizione"
})
public class WkfAzioneSpedizioneDestinatarioElettronicoProtocolloType
    implements Serializable
{

    @XmlElement(name = "Destinatario", required = true)
    protected IdentificativoDestinatarioType destinatario;
    @XmlElement(name = "IndirizzoTelematico")
    protected AnaIndirizzoTelematicoType indirizzoTelematico;
    @XmlElement(name = "DataSpedizione")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataSpedizione;
    @XmlElement(name = "StatoSpedizione", required = true)
    protected String statoSpedizione;

    /**
     * Recupera il valore della proprietà destinatario.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoDestinatarioType }
     *     
     */
    public IdentificativoDestinatarioType getDestinatario() {
        return destinatario;
    }

    /**
     * Imposta il valore della proprietà destinatario.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoDestinatarioType }
     *     
     */
    public void setDestinatario(IdentificativoDestinatarioType value) {
        this.destinatario = value;
    }

    /**
     * Recupera il valore della proprietà indirizzoTelematico.
     * 
     * @return
     *     possible object is
     *     {@link AnaIndirizzoTelematicoType }
     *     
     */
    public AnaIndirizzoTelematicoType getIndirizzoTelematico() {
        return indirizzoTelematico;
    }

    /**
     * Imposta il valore della proprietà indirizzoTelematico.
     * 
     * @param value
     *     allowed object is
     *     {@link AnaIndirizzoTelematicoType }
     *     
     */
    public void setIndirizzoTelematico(AnaIndirizzoTelematicoType value) {
        this.indirizzoTelematico = value;
    }

    /**
     * Recupera il valore della proprietà dataSpedizione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataSpedizione() {
        return dataSpedizione;
    }

    /**
     * Imposta il valore della proprietà dataSpedizione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataSpedizione(XMLGregorianCalendar value) {
        this.dataSpedizione = value;
    }

    /**
     * Recupera il valore della proprietà statoSpedizione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoSpedizione() {
        return statoSpedizione;
    }

    /**
     * Imposta il valore della proprietà statoSpedizione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoSpedizione(String value) {
        this.statoSpedizione = value;
    }

}
