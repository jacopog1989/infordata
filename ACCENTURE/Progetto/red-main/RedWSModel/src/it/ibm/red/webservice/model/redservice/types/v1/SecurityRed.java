
package it.ibm.red.webservice.model.redservice.types.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SecurityRed complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SecurityRed">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="gruppo" type="{urn:red:types:v1}GruppoRed" minOccurs="0"/>
 *         &lt;element name="utente" type="{urn:red:types:v1}UtenteRed" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SecurityRed", propOrder = {
    "gruppo",
    "utente"
})
public class SecurityRed {

	/**
	 * Gruppo.
	 */
    @XmlElementRef(name = "gruppo", type = JAXBElement.class, required = false)
    protected JAXBElement<GruppoRed> gruppo;

    /**
     * Utente.
     */
    @XmlElementRef(name = "utente", type = JAXBElement.class, required = false)
    protected JAXBElement<UtenteRed> utente;

    /**
     * Gets the value of the gruppo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GruppoRed }{@code >}
     *     
     */
    public JAXBElement<GruppoRed> getGruppo() {
        return gruppo;
    }

    /**
     * Sets the value of the gruppo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GruppoRed }{@code >}
     *     
     */
    public void setGruppo(JAXBElement<GruppoRed> value) {
        this.gruppo = value;
    }

    /**
     * Gets the value of the utente property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link UtenteRed }{@code >}
     *     
     */
    public JAXBElement<UtenteRed> getUtente() {
        return utente;
    }

    /**
     * Sets the value of the utente property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link UtenteRed }{@code >}
     *     
     */
    public void setUtente(JAXBElement<UtenteRed> value) {
        this.utente = value;
    }

}
