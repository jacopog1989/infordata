
package it.ibm.red.webservice.model.redservice.messages.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import it.ibm.red.webservice.model.redservice.types.v1.DocumentoRed;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiceErrore" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="documento" type="{urn:red:types:v1}DocumentoRed"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "codiceErrore",
    "documento"
})
@XmlRootElement(name = "modificaFascicoloDecretoResponse")
public class ModificaFascicoloDecretoResponse {

	/**
	 * Codice.
	 */
    @XmlElementRef(name = "codiceErrore", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codiceErrore;

	/**
	 * Documento.
	 */
    @XmlElement(required = true, nillable = true)
    protected DocumentoRed documento;

    /**
     * Gets the value of the codiceErrore property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodiceErrore() {
        return codiceErrore;
    }

    /**
     * Sets the value of the codiceErrore property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodiceErrore(JAXBElement<Integer> value) {
        this.codiceErrore = value;
    }

    /**
     * Gets the value of the documento property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoRed }
     *     
     */
    public DocumentoRed getDocumento() {
        return documento;
    }

    /**
     * Sets the value of the documento property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoRed }
     *     
     */
    public void setDocumento(DocumentoRed value) {
        this.documento = value;
    }

}
