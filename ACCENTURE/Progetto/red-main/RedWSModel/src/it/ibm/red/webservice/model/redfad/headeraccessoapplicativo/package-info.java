/**
 * FAD Accesso applicativo.
 */

@javax.xml.bind.annotation.XmlSchema(namespace = "http://mef.gov.it.v1.red/servizi/Common/HeaderAccessoApplicativo")
package it.ibm.red.webservice.model.redfad.headeraccessoapplicativo;
