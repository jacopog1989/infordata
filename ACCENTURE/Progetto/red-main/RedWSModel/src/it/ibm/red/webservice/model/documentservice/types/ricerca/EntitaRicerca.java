//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.01.28 at 06:39:59 PM CET 
//


package it.ibm.red.webservice.model.documentservice.types.ricerca;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EntitaRicerca.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EntitaRicerca">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DOCUMENTO"/>
 *     &lt;enumeration value="FASCICOLO"/>
 *     &lt;enumeration value="WORKFLOW"/>
 *     &lt;enumeration value="QUEUE"/>
 *     &lt;enumeration value="TITOLARIO"/>
 *     &lt;enumeration value="FALDONE"/>
 *     &lt;enumeration value="CASELLAPOSTALE"/>
 *     &lt;enumeration value="EMAIL"/>
 *     &lt;enumeration value="PROTOCOLLO"/>
 *     &lt;enumeration value="SPEDIZIONE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "EntitaRicerca")
@XmlEnum
public enum EntitaRicerca {

	/**
	 * Valore.
	 */
    DOCUMENTO,

	/**
	 * Valore.
	 */
    FASCICOLO,

	/**
	 * Valore.
	 */
    WORKFLOW,

	/**
	 * Valore.
	 */
    QUEUE,

	/**
	 * Valore.
	 */
    TITOLARIO,

	/**
	 * Valore.
	 */
    FALDONE,

	/**
	 * Valore.
	 */
    CASELLAPOSTALE,

	/**
	 * Valore.
	 */
    EMAIL,

	/**
	 * Valore.
	 */
    PROTOCOLLO,

	/**
	 * Valore.
	 */
    SPEDIZIONE;

	/**
	 * Ottiene il nome della costante enum.
	 * @return nome della costante enum
	 */
    public String value() {
        return name();
    }

    /**
     * Ottiene l'entità della ricerca dal valore.
     * @param v valore
     * @return entità della ricerca
     */
    public static EntitaRicerca fromValue(String v) {
        return valueOf(v);
    }

}
