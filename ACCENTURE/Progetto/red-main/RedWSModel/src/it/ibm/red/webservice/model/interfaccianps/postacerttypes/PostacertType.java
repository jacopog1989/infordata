
package it.ibm.red.webservice.model.interfaccianps.postacerttypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java per postacertType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="postacertType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://mef.gov.it.v1.postacertTypes/}intestazione"/>
 *         &lt;element ref="{http://mef.gov.it.v1.postacertTypes/}dati"/>
 *       &lt;/sequence>
 *       &lt;attribute name="tipo" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *             &lt;enumeration value="presa-in-carico"/>
 *             &lt;enumeration value="avvenuta-consegna"/>
 *             &lt;enumeration value="preavviso-errore-consegna"/>
 *             &lt;enumeration value="posta-certificata"/>
 *             &lt;enumeration value="rilevazione-virus"/>
 *             &lt;enumeration value="errore-consegna"/>
 *             &lt;enumeration value="non-accettazione"/>
 *             &lt;enumeration value="accettazione"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="errore" default="nessuno">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *             &lt;enumeration value="no-dest"/>
 *             &lt;enumeration value="altro"/>
 *             &lt;enumeration value="no-dominio"/>
 *             &lt;enumeration value="virus"/>
 *             &lt;enumeration value="nessuno"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "postacertType", propOrder = {
    "intestazione",
    "dati"
})
public class PostacertType
    implements Serializable
{

    @XmlElement(required = true)
    protected Intestazione intestazione;
    @XmlElement(required = true)
    protected Dati dati;
    @XmlAttribute(name = "tipo", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String tipo;
    @XmlAttribute(name = "errore")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String errore;

    /**
     * Recupera il valore della proprietà intestazione.
     * 
     * @return
     *     possible object is
     *     {@link Intestazione }
     *     
     */
    public Intestazione getIntestazione() {
        return intestazione;
    }

    /**
     * Imposta il valore della proprietà intestazione.
     * 
     * @param value
     *     allowed object is
     *     {@link Intestazione }
     *     
     */
    public void setIntestazione(Intestazione value) {
        this.intestazione = value;
    }

    /**
     * Recupera il valore della proprietà dati.
     * 
     * @return
     *     possible object is
     *     {@link Dati }
     *     
     */
    public Dati getDati() {
        return dati;
    }

    /**
     * Imposta il valore della proprietà dati.
     * 
     * @param value
     *     allowed object is
     *     {@link Dati }
     *     
     */
    public void setDati(Dati value) {
        this.dati = value;
    }

    /**
     * Recupera il valore della proprietà tipo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Imposta il valore della proprietà tipo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipo(String value) {
        this.tipo = value;
    }

    /**
     * Recupera il valore della proprietà errore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrore() {
        if (errore == null) {
            return "nessuno";
        } else {
            return errore;
        }
    }

    /**
     * Imposta il valore della proprietà errore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrore(String value) {
        this.errore = value;
    }

}
