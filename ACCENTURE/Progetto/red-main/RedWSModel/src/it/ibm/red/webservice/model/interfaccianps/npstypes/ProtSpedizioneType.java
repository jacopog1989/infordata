
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Definisce il tipo di dato relativo alla spedizione del protocoollo
 * 
 * <p>Classe Java per prot_spedizione_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_spedizione_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StatoSpedizione">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="DA_SPEDIRE"/>
 *               &lt;enumeration value="IN_SPEDIZIONE"/>
 *               &lt;enumeration value="SPEDITO"/>
 *               &lt;enumeration value="ERRORE"/>
 *               &lt;enumeration value="RICEVUTA_ACCETTAZIONE"/>
 *               &lt;enumeration value="RICEVUTA_CONSEGNA"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DataSpedizione" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="RicevutePec" type="{http://mef.gov.it.v1.npsTypes}prot_ricevutePec_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RicevuteInteroperabilita" type="{http://mef.gov.it.v1.npsTypes}prot_ricevuteSegnatura_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="MezzoSpedizione">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="Elettronico">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;choice>
 *                             &lt;element name="CooperazioneApplicativa" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
 *                             &lt;element name="PostaElettronica" type="{http://mef.gov.it.v1.npsTypes}org_casellaEmail_type"/>
 *                           &lt;/choice>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Cartaceo" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type"/>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_spedizione_type", propOrder = {
    "statoSpedizione",
    "dataSpedizione",
    "ricevutePec",
    "ricevuteInteroperabilita",
    "mezzoSpedizione"
})
public class ProtSpedizioneType
    implements Serializable
{

    @XmlElement(name = "StatoSpedizione", required = true)
    protected String statoSpedizione;
    @XmlElement(name = "DataSpedizione")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataSpedizione;
    @XmlElement(name = "RicevutePec")
    protected List<ProtRicevutePecType> ricevutePec;
    @XmlElement(name = "RicevuteInteroperabilita")
    protected List<ProtRicevuteSegnaturaType> ricevuteInteroperabilita;
    @XmlElement(name = "MezzoSpedizione", required = true)
    protected ProtSpedizioneType.MezzoSpedizione mezzoSpedizione;

    /**
     * Recupera il valore della proprietà statoSpedizione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoSpedizione() {
        return statoSpedizione;
    }

    /**
     * Imposta il valore della proprietà statoSpedizione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoSpedizione(String value) {
        this.statoSpedizione = value;
    }

    /**
     * Recupera il valore della proprietà dataSpedizione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataSpedizione() {
        return dataSpedizione;
    }

    /**
     * Imposta il valore della proprietà dataSpedizione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataSpedizione(XMLGregorianCalendar value) {
        this.dataSpedizione = value;
    }

    /**
     * Gets the value of the ricevutePec property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ricevutePec property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRicevutePec().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtRicevutePecType }
     * 
     * 
     */
    public List<ProtRicevutePecType> getRicevutePec() {
        if (ricevutePec == null) {
            ricevutePec = new ArrayList<ProtRicevutePecType>();
        }
        return this.ricevutePec;
    }

    /**
     * Gets the value of the ricevuteInteroperabilita property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ricevuteInteroperabilita property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRicevuteInteroperabilita().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtRicevuteSegnaturaType }
     * 
     * 
     */
    public List<ProtRicevuteSegnaturaType> getRicevuteInteroperabilita() {
        if (ricevuteInteroperabilita == null) {
            ricevuteInteroperabilita = new ArrayList<ProtRicevuteSegnaturaType>();
        }
        return this.ricevuteInteroperabilita;
    }

    /**
     * Recupera il valore della proprietà mezzoSpedizione.
     * 
     * @return
     *     possible object is
     *     {@link ProtSpedizioneType.MezzoSpedizione }
     *     
     */
    public ProtSpedizioneType.MezzoSpedizione getMezzoSpedizione() {
        return mezzoSpedizione;
    }

    /**
     * Imposta il valore della proprietà mezzoSpedizione.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtSpedizioneType.MezzoSpedizione }
     *     
     */
    public void setMezzoSpedizione(ProtSpedizioneType.MezzoSpedizione value) {
        this.mezzoSpedizione = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="Elettronico">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;choice>
     *                   &lt;element name="CooperazioneApplicativa" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
     *                   &lt;element name="PostaElettronica" type="{http://mef.gov.it.v1.npsTypes}org_casellaEmail_type"/>
     *                 &lt;/choice>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Cartaceo" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type"/>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "elettronico",
        "cartaceo"
    })
    public static class MezzoSpedizione
        implements Serializable
    {

        @XmlElement(name = "Elettronico")
        protected ProtSpedizioneType.MezzoSpedizione.Elettronico elettronico;
        @XmlElement(name = "Cartaceo")
        protected AllCodiceDescrizioneType cartaceo;

        /**
         * Recupera il valore della proprietà elettronico.
         * 
         * @return
         *     possible object is
         *     {@link ProtSpedizioneType.MezzoSpedizione.Elettronico }
         *     
         */
        public ProtSpedizioneType.MezzoSpedizione.Elettronico getElettronico() {
            return elettronico;
        }

        /**
         * Imposta il valore della proprietà elettronico.
         * 
         * @param value
         *     allowed object is
         *     {@link ProtSpedizioneType.MezzoSpedizione.Elettronico }
         *     
         */
        public void setElettronico(ProtSpedizioneType.MezzoSpedizione.Elettronico value) {
            this.elettronico = value;
        }

        /**
         * Recupera il valore della proprietà cartaceo.
         * 
         * @return
         *     possible object is
         *     {@link AllCodiceDescrizioneType }
         *     
         */
        public AllCodiceDescrizioneType getCartaceo() {
            return cartaceo;
        }

        /**
         * Imposta il valore della proprietà cartaceo.
         * 
         * @param value
         *     allowed object is
         *     {@link AllCodiceDescrizioneType }
         *     
         */
        public void setCartaceo(AllCodiceDescrizioneType value) {
            this.cartaceo = value;
        }


        /**
         * <p>Classe Java per anonymous complex type.
         * 
         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;choice>
         *         &lt;element name="CooperazioneApplicativa" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
         *         &lt;element name="PostaElettronica" type="{http://mef.gov.it.v1.npsTypes}org_casellaEmail_type"/>
         *       &lt;/choice>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "cooperazioneApplicativa",
            "postaElettronica"
        })
        public static class Elettronico
            implements Serializable
        {

            @XmlElement(name = "CooperazioneApplicativa")
            @XmlSchemaType(name = "anyURI")
            protected String cooperazioneApplicativa;
            @XmlElement(name = "PostaElettronica")
            protected OrgCasellaEmailType postaElettronica;

            /**
             * Recupera il valore della proprietà cooperazioneApplicativa.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCooperazioneApplicativa() {
                return cooperazioneApplicativa;
            }

            /**
             * Imposta il valore della proprietà cooperazioneApplicativa.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCooperazioneApplicativa(String value) {
                this.cooperazioneApplicativa = value;
            }

            /**
             * Recupera il valore della proprietà postaElettronica.
             * 
             * @return
             *     possible object is
             *     {@link OrgCasellaEmailType }
             *     
             */
            public OrgCasellaEmailType getPostaElettronica() {
                return postaElettronica;
            }

            /**
             * Imposta il valore della proprietà postaElettronica.
             * 
             * @param value
             *     allowed object is
             *     {@link OrgCasellaEmailType }
             *     
             */
            public void setPostaElettronica(OrgCasellaEmailType value) {
                this.postaElettronica = value;
            }

        }

    }

}
