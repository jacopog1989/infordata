
package it.ibm.red.webservice.model.redservice.types.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FascicoloRed complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FascicoloRed">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="classeDocumentale" type="{urn:red:types:v1}ClasseDocumentaleRed" minOccurs="0"/>
 *         &lt;element name="idFascicolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idNuovoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="security" type="{urn:red:types:v1}SecurityRed" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="titolario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FascicoloRed", propOrder = {
    "classeDocumentale",
    "idFascicolo",
    "idNuovoFascicolo",
    "security",
    "titolario"
})
public class FascicoloRed {


	/**
	 * Classe documentale.
	 */
    @XmlElementRef(name = "classeDocumentale", type = JAXBElement.class, required = false)
    protected JAXBElement<ClasseDocumentaleRed> classeDocumentale;

	/**
	 * Identificativo fascicolo.
	 */
    @XmlElementRef(name = "idFascicolo", type = JAXBElement.class, required = false)
    protected JAXBElement<String> idFascicolo;

	/**
	 * Identificativo nuovo fascicolo.
	 */
    @XmlElementRef(name = "idNuovoFascicolo", type = JAXBElement.class, required = false)
    protected JAXBElement<String> idNuovoFascicolo;

	/**
	 * Security.
	 */
    @XmlElement(nillable = true)
    protected List<SecurityRed> security;

	/**
	 * Titolario.
	 */
    @XmlElementRef(name = "titolario", type = JAXBElement.class, required = false)
    protected JAXBElement<String> titolario;

    /**
     * Gets the value of the classeDocumentale property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ClasseDocumentaleRed }{@code >}
     *     
     */
    public JAXBElement<ClasseDocumentaleRed> getClasseDocumentale() {
        return classeDocumentale;
    }

    /**
     * Sets the value of the classeDocumentale property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ClasseDocumentaleRed }{@code >}
     *     
     */
    public void setClasseDocumentale(JAXBElement<ClasseDocumentaleRed> value) {
        this.classeDocumentale = value;
    }

    /**
     * Gets the value of the idFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIdFascicolo() {
        return idFascicolo;
    }

    /**
     * Sets the value of the idFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIdFascicolo(JAXBElement<String> value) {
        this.idFascicolo = value;
    }

    /**
     * Gets the value of the idNuovoFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIdNuovoFascicolo() {
        return idNuovoFascicolo;
    }

    /**
     * Sets the value of the idNuovoFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIdNuovoFascicolo(JAXBElement<String> value) {
        this.idNuovoFascicolo = value;
    }

    /**
     * Gets the value of the security property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the security property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSecurity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SecurityRed }
     * 
     * 
     */
    public List<SecurityRed> getSecurity() {
        if (security == null) {
            security = new ArrayList<>();
        }
        return this.security;
    }

    /**
     * Gets the value of the titolario property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTitolario() {
        return titolario;
    }

    /**
     * Sets the value of the titolario property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTitolario(JAXBElement<String> value) {
        this.titolario = value;
    }

}
