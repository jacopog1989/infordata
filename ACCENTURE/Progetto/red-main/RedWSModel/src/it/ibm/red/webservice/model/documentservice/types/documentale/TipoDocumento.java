// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.01.28 at 06:39:59 PM CET 
package it.ibm.red.webservice.model.documentservice.types.documentale;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for TipoDocumento.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TipoDocumento">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PRINCIPALE"/>
 *     &lt;enumeration value="ALLEGATO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "TipoDocumento")
@XmlEnum
public enum TipoDocumento {

	/**
	 * Valore.
	 */
    PRINCIPALE,

	/**
	 * Valore.
	 */
    ALLEGATO;

	/**
	 * Restituisce il nome dell'enum.
	 * @return name
	 */
    public String value() {
        return name();
    }

    /**
     * Restituisce il valore dell'enum corrispondente alla stringa in input.
     * @param v
     * @return
     */
    public static TipoDocumento fromValue(String v) {
        return valueOf(v);
    }
}
