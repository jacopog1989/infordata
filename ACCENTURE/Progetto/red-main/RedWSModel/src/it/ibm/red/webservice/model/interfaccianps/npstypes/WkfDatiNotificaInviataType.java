
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Dati della notifica interoperabile inviata
 * 
 * <p>Classe Java per wkf_datiNotificaInviata_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_datiNotificaInviata_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DataInvio" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="TipoMessaggio">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="CONFERMA_RICEZIONE"/>
 *               &lt;enumeration value="NOTIFICA_ECCEZIONE"/>
 *               &lt;enumeration value="AGGIORNAMENTO_CONFERMA"/>
 *               &lt;enumeration value="ANNULLAMENTO_PROTOCOLLAZIONE"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TestoMessaggio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContentMessaggio" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_datiNotificaInviata_type", propOrder = {
    "dataInvio",
    "tipoMessaggio",
    "testoMessaggio",
    "contentMessaggio"
})
@XmlSeeAlso({
    it.ibm.red.webservice.model.interfaccianps.npstypes.WkfDatiNotificaErroreProtocollazioneFlussoType.NotificaInviata.class,
    it.ibm.red.webservice.model.interfaccianps.npstypes.WkfDatiNotificaErroreProtocollazioneAutomaticaType.NotificaInviata.class
})
public class WkfDatiNotificaInviataType
    implements Serializable
{

    @XmlElement(name = "DataInvio", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataInvio;
    @XmlElement(name = "TipoMessaggio", required = true)
    protected String tipoMessaggio;
    @XmlElement(name = "TestoMessaggio", required = true)
    protected String testoMessaggio;
    @XmlElement(name = "ContentMessaggio", required = true)
    protected byte[] contentMessaggio;

    /**
     * Recupera il valore della proprietà dataInvio.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataInvio() {
        return dataInvio;
    }

    /**
     * Imposta il valore della proprietà dataInvio.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataInvio(XMLGregorianCalendar value) {
        this.dataInvio = value;
    }

    /**
     * Recupera il valore della proprietà tipoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoMessaggio() {
        return tipoMessaggio;
    }

    /**
     * Imposta il valore della proprietà tipoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoMessaggio(String value) {
        this.tipoMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà testoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTestoMessaggio() {
        return testoMessaggio;
    }

    /**
     * Imposta il valore della proprietà testoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTestoMessaggio(String value) {
        this.testoMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà contentMessaggio.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getContentMessaggio() {
        return contentMessaggio;
    }

    /**
     * Imposta il valore della proprietà contentMessaggio.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setContentMessaggio(byte[] value) {
        this.contentMessaggio = value;
    }

}
