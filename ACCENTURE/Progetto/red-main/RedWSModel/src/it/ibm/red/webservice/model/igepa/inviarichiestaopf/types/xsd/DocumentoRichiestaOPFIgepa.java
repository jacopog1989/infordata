
package it.ibm.red.webservice.model.igepa.inviarichiestaopf.types.xsd;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per DocumentoRichiestaOPFIgepa complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="DocumentoRichiestaOPFIgepa">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="annoRichiesta" type="{http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd}AnnoFormatType" minOccurs="0"/>
 *         &lt;element name="dataProtocollo" type="{http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd}DataFormatType" minOccurs="0"/>
 *         &lt;element name="documentoPrincipale" type="{http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd}DocumentFile" minOccurs="0"/>
 *         &lt;element name="enteMittente" type="{http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd}EnteMittenteFormatType" minOccurs="0"/>
 *         &lt;element name="idNodoDestinatario" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idUtenteDestinatario" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="indiceClassificazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroConto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroProtocollo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numeroRichiesta" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="oggettoRichiesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sezione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoDocumento" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="tipoProcedimento" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentoRichiestaOPFIgepa", propOrder = {
    "annoRichiesta",
    "dataProtocollo",
    "documentoPrincipale",
    "enteMittente",
    "idNodoDestinatario",
    "idUtenteDestinatario",
    "indiceClassificazione",
    "numeroConto",
    "numeroProtocollo",
    "numeroRichiesta",
    "oggettoRichiesta",
    "sezione",
    "tipoDocumento",
    "tipoProcedimento"
})
public class DocumentoRichiestaOPFIgepa
    implements Serializable
{

    @XmlElementRef(name = "annoRichiesta", namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnoFormatType> annoRichiesta;
    @XmlElementRef(name = "dataProtocollo", namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<DataFormatType> dataProtocollo;
    @XmlElementRef(name = "documentoPrincipale", namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<DocumentFile> documentoPrincipale;
    @XmlElementRef(name = "enteMittente", namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<EnteMittenteFormatType> enteMittente;
    @XmlElementRef(name = "idNodoDestinatario", namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> idNodoDestinatario;
    @XmlElementRef(name = "idUtenteDestinatario", namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> idUtenteDestinatario;
    @XmlElementRef(name = "indiceClassificazione", namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> indiceClassificazione;
    @XmlElementRef(name = "numeroConto", namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numeroConto;
    @XmlElementRef(name = "numeroProtocollo", namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> numeroProtocollo;
    @XmlElementRef(name = "numeroRichiesta", namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> numeroRichiesta;
    @XmlElementRef(name = "oggettoRichiesta", namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> oggettoRichiesta;
    @XmlElementRef(name = "sezione", namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sezione;
    @XmlElementRef(name = "tipoDocumento", namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> tipoDocumento;
    @XmlElementRef(name = "tipoProcedimento", namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> tipoProcedimento;

    /**
     * Recupera il valore della proprietà annoRichiesta.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnoFormatType }{@code >}
     *     
     */
    public JAXBElement<AnnoFormatType> getAnnoRichiesta() {
        return annoRichiesta;
    }

    /**
     * Imposta il valore della proprietà annoRichiesta.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnoFormatType }{@code >}
     *     
     */
    public void setAnnoRichiesta(JAXBElement<AnnoFormatType> value) {
        this.annoRichiesta = value;
    }

    /**
     * Recupera il valore della proprietà dataProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DataFormatType }{@code >}
     *     
     */
    public JAXBElement<DataFormatType> getDataProtocollo() {
        return dataProtocollo;
    }

    /**
     * Imposta il valore della proprietà dataProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DataFormatType }{@code >}
     *     
     */
    public void setDataProtocollo(JAXBElement<DataFormatType> value) {
        this.dataProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà documentoPrincipale.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DocumentFile }{@code >}
     *     
     */
    public JAXBElement<DocumentFile> getDocumentoPrincipale() {
        return documentoPrincipale;
    }

    /**
     * Imposta il valore della proprietà documentoPrincipale.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DocumentFile }{@code >}
     *     
     */
    public void setDocumentoPrincipale(JAXBElement<DocumentFile> value) {
        this.documentoPrincipale = value;
    }

    /**
     * Recupera il valore della proprietà enteMittente.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link EnteMittenteFormatType }{@code >}
     *     
     */
    public JAXBElement<EnteMittenteFormatType> getEnteMittente() {
        return enteMittente;
    }

    /**
     * Imposta il valore della proprietà enteMittente.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link EnteMittenteFormatType }{@code >}
     *     
     */
    public void setEnteMittente(JAXBElement<EnteMittenteFormatType> value) {
        this.enteMittente = value;
    }

    /**
     * Recupera il valore della proprietà idNodoDestinatario.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getIdNodoDestinatario() {
        return idNodoDestinatario;
    }

    /**
     * Imposta il valore della proprietà idNodoDestinatario.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setIdNodoDestinatario(JAXBElement<Integer> value) {
        this.idNodoDestinatario = value;
    }

    /**
     * Recupera il valore della proprietà idUtenteDestinatario.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getIdUtenteDestinatario() {
        return idUtenteDestinatario;
    }

    /**
     * Imposta il valore della proprietà idUtenteDestinatario.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setIdUtenteDestinatario(JAXBElement<Integer> value) {
        this.idUtenteDestinatario = value;
    }

    /**
     * Recupera il valore della proprietà indiceClassificazione.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIndiceClassificazione() {
        return indiceClassificazione;
    }

    /**
     * Imposta il valore della proprietà indiceClassificazione.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIndiceClassificazione(JAXBElement<String> value) {
        this.indiceClassificazione = value;
    }

    /**
     * Recupera il valore della proprietà numeroConto.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumeroConto() {
        return numeroConto;
    }

    /**
     * Imposta il valore della proprietà numeroConto.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumeroConto(JAXBElement<String> value) {
        this.numeroConto = value;
    }

    /**
     * Recupera il valore della proprietà numeroProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getNumeroProtocollo() {
        return numeroProtocollo;
    }

    /**
     * Imposta il valore della proprietà numeroProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setNumeroProtocollo(JAXBElement<Integer> value) {
        this.numeroProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà numeroRichiesta.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getNumeroRichiesta() {
        return numeroRichiesta;
    }

    /**
     * Imposta il valore della proprietà numeroRichiesta.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setNumeroRichiesta(JAXBElement<Integer> value) {
        this.numeroRichiesta = value;
    }

    /**
     * Recupera il valore della proprietà oggettoRichiesta.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOggettoRichiesta() {
        return oggettoRichiesta;
    }

    /**
     * Imposta il valore della proprietà oggettoRichiesta.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOggettoRichiesta(JAXBElement<String> value) {
        this.oggettoRichiesta = value;
    }

    /**
     * Recupera il valore della proprietà sezione.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSezione() {
        return sezione;
    }

    /**
     * Imposta il valore della proprietà sezione.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSezione(JAXBElement<String> value) {
        this.sezione = value;
    }

    /**
     * Recupera il valore della proprietà tipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Imposta il valore della proprietà tipoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setTipoDocumento(JAXBElement<Integer> value) {
        this.tipoDocumento = value;
    }

    /**
     * Recupera il valore della proprietà tipoProcedimento.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getTipoProcedimento() {
        return tipoProcedimento;
    }

    /**
     * Imposta il valore della proprietà tipoProcedimento.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setTipoProcedimento(JAXBElement<Integer> value) {
        this.tipoProcedimento = value;
    }

}
