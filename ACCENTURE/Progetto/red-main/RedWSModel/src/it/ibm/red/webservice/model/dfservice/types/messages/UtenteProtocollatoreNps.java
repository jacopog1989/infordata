//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.09.18 at 10:01:34 AM CEST 
//


package it.ibm.red.webservice.model.dfservice.types.messages;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for utenteProtocollatoreNps complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="utenteProtocollatoreNps">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiceAmministrazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="denominazioneAmministrazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codAoo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="denominazioneAoo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codUO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="denominazioneUO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cognome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chiaveEsternaOperatore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "utenteProtocollatoreNps", propOrder = {
    "codiceAmministrazione",
    "denominazioneAmministrazione",
    "codAoo",
    "denominazioneAoo",
    "codUO",
    "denominazioneUO",
    "nome",
    "cognome",
    "chiaveEsternaOperatore"
})
public class UtenteProtocollatoreNps {

	/**
	 * Codce amministrazione.
	 */
    protected String codiceAmministrazione;

	/**
	 * Denominazione amministrazione.
	 */
    protected String denominazioneAmministrazione;

	/**
	 * Codice aoo.
	 */
    protected String codAoo;

	/**
	 * Denominazione aoo.
	 */
    protected String denominazioneAoo;

	/**
	 * Codice uo.
	 */
    protected String codUO;

	/**
	 * Denominazione uo.
	 */
    protected String denominazioneUO;

	/**
	 * Nome.
	 */
    protected String nome;

	/**
	 * Cognome.
	 */
    protected String cognome;

	/**
	 * Chiave esterna operatore.
	 */
    protected String chiaveEsternaOperatore;

    /**
     * Gets the value of the codiceAmministrazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceAmministrazione() {
        return codiceAmministrazione;
    }

    /**
     * Sets the value of the codiceAmministrazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceAmministrazione(String value) {
        this.codiceAmministrazione = value;
    }

    /**
     * Gets the value of the denominazioneAmministrazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenominazioneAmministrazione() {
        return denominazioneAmministrazione;
    }

    /**
     * Sets the value of the denominazioneAmministrazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenominazioneAmministrazione(String value) {
        this.denominazioneAmministrazione = value;
    }

    /**
     * Gets the value of the codAoo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodAoo() {
        return codAoo;
    }

    /**
     * Sets the value of the codAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodAoo(String value) {
        this.codAoo = value;
    }

    /**
     * Gets the value of the denominazioneAoo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenominazioneAoo() {
        return denominazioneAoo;
    }

    /**
     * Sets the value of the denominazioneAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenominazioneAoo(String value) {
        this.denominazioneAoo = value;
    }

    /**
     * Gets the value of the codUO property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodUO() {
        return codUO;
    }

    /**
     * Sets the value of the codUO property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodUO(String value) {
        this.codUO = value;
    }

    /**
     * Gets the value of the denominazioneUO property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenominazioneUO() {
        return denominazioneUO;
    }

    /**
     * Sets the value of the denominazioneUO property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenominazioneUO(String value) {
        this.denominazioneUO = value;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the cognome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCognome() {
        return cognome;
    }

    /**
     * Sets the value of the cognome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCognome(String value) {
        this.cognome = value;
    }

    /**
     * Gets the value of the chiaveEsternaOperatore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveEsternaOperatore() {
        return chiaveEsternaOperatore;
    }

    /**
     * Sets the value of the chiaveEsternaOperatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveEsternaOperatore(String value) {
        this.chiaveEsternaOperatore = value;
    }

}
