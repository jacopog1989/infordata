
package it.ibm.red.webservice.model.redfad.interfacciadocumentaleredfadtypes_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for risposta_terminaFlussoAttoDecretoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risposta_terminaFlussoAttoDecretoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StatoOperazione" type="{http://mef.gov.it.v1.red/servizi/InterfacciaDocumentaleREDFADTypes_1}risultatoTerminaFlussoAttoDecretoType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_terminaFlussoAttoDecretoType", propOrder = {
    "statoOperazione"
})
public class RispostaTerminaFlussoAttoDecretoType {

    /**
     * Stato operazione.
     */
    @XmlElement(name = "StatoOperazione", required = true)
    protected List<RisultatoTerminaFlussoAttoDecretoType> statoOperazione;

    /**
     * Gets the value of the statoOperazione property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the statoOperazione property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatoOperazione().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RisultatoTerminaFlussoAttoDecretoType }
     * 
     * 
     */
    public List<RisultatoTerminaFlussoAttoDecretoType> getStatoOperazione() {
        if (statoOperazione == null) {
            statoOperazione = new ArrayList<>();
        }
        return this.statoOperazione;
    }

}
