
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Dati per la notifica per l'aggiornamnto dei Collegati
 * 
 * <p>Classe Java per wkf_azioneAggiornamentoCollegati_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_azioneAggiornamentoCollegati_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DataOperazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="StatoCollegati" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="DA_ASSEGNARE"/>
 *               &lt;enumeration value="IN_LAVORAZIONE"/>
 *               &lt;enumeration value="AGLI_ATTI"/>
 *               &lt;enumeration value="CHIUSO"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Collegati" type="{http://mef.gov.it.v1.npsTypes}prot_identificatoreProtocolloCollegato_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Operatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SistemaAusiliario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_azioneAggiornamentoCollegati_type", propOrder = {
    "dataOperazione",
    "statoCollegati",
    "collegati",
    "operatore",
    "sistemaProduttore",
    "sistemaAusiliario"
})
public class WkfAzioneAggiornamentoCollegatiType
    implements Serializable
{

    @XmlElement(name = "DataOperazione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataOperazione;
    @XmlElement(name = "StatoCollegati")
    protected String statoCollegati;
    @XmlElement(name = "Collegati")
    protected List<ProtIdentificatoreProtocolloCollegatoType> collegati;
    @XmlElement(name = "Operatore", required = true)
    protected OrgOrganigrammaType operatore;
    @XmlElement(name = "SistemaProduttore", required = true)
    protected String sistemaProduttore;
    @XmlElement(name = "SistemaAusiliario")
    protected String sistemaAusiliario;

    /**
     * Recupera il valore della proprietà dataOperazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataOperazione() {
        return dataOperazione;
    }

    /**
     * Imposta il valore della proprietà dataOperazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataOperazione(XMLGregorianCalendar value) {
        this.dataOperazione = value;
    }

    /**
     * Recupera il valore della proprietà statoCollegati.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoCollegati() {
        return statoCollegati;
    }

    /**
     * Imposta il valore della proprietà statoCollegati.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoCollegati(String value) {
        this.statoCollegati = value;
    }

    /**
     * Gets the value of the collegati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the collegati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCollegati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtIdentificatoreProtocolloCollegatoType }
     * 
     * 
     */
    public List<ProtIdentificatoreProtocolloCollegatoType> getCollegati() {
        if (collegati == null) {
            collegati = new ArrayList<ProtIdentificatoreProtocolloCollegatoType>();
        }
        return this.collegati;
    }

    /**
     * Recupera il valore della proprietà operatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getOperatore() {
        return operatore;
    }

    /**
     * Imposta il valore della proprietà operatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setOperatore(OrgOrganigrammaType value) {
        this.operatore = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

    /**
     * Recupera il valore della proprietà sistemaAusiliario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaAusiliario() {
        return sistemaAusiliario;
    }

    /**
     * Imposta il valore della proprietà sistemaAusiliario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaAusiliario(String value) {
        this.sistemaAusiliario = value;
    }

}
