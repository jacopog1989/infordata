
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.ContestoProcedurale;
import it.ibm.red.webservice.model.interfaccianps.npstypesestesi.TIPOLOGIA;


/**
 * Dati del flusso
 * 
 * <p>Classe Java per wkf_datiContestoProcedurale_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_datiContestoProcedurale_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentificativoMessaggio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContestoProcedurale" type="{http://www.digitPa.gov.it/protocollo/}ContestoProcedurale"/>
 *         &lt;element name="DatiEstesi" type="{http://mef.gov.it.v1.npsTypesEstesi/}TIPOLOGIA"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_datiContestoProcedurale_type", propOrder = {
    "identificativoMessaggio",
    "contestoProcedurale",
    "datiEstesi"
})
public class WkfDatiContestoProceduraleType
    implements Serializable
{

    @XmlElement(name = "IdentificativoMessaggio", required = true)
    protected String identificativoMessaggio;
    @XmlElement(name = "ContestoProcedurale", required = true)
    protected ContestoProcedurale contestoProcedurale;
    @XmlElement(name = "DatiEstesi", required = true)
    protected TIPOLOGIA datiEstesi;

    /**
     * Recupera il valore della proprietà identificativoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificativoMessaggio() {
        return identificativoMessaggio;
    }

    /**
     * Imposta il valore della proprietà identificativoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificativoMessaggio(String value) {
        this.identificativoMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà contestoProcedurale.
     * 
     * @return
     *     possible object is
     *     {@link ContestoProcedurale }
     *     
     */
    public ContestoProcedurale getContestoProcedurale() {
        return contestoProcedurale;
    }

    /**
     * Imposta il valore della proprietà contestoProcedurale.
     * 
     * @param value
     *     allowed object is
     *     {@link ContestoProcedurale }
     *     
     */
    public void setContestoProcedurale(ContestoProcedurale value) {
        this.contestoProcedurale = value;
    }

    /**
     * Recupera il valore della proprietà datiEstesi.
     * 
     * @return
     *     possible object is
     *     {@link TIPOLOGIA }
     *     
     */
    public TIPOLOGIA getDatiEstesi() {
        return datiEstesi;
    }

    /**
     * Imposta il valore della proprietà datiEstesi.
     * 
     * @param value
     *     allowed object is
     *     {@link TIPOLOGIA }
     *     
     */
    public void setDatiEstesi(TIPOLOGIA value) {
        this.datiEstesi = value;
    }

}
