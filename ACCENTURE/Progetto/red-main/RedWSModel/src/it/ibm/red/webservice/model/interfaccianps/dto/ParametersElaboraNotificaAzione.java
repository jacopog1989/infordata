package it.ibm.red.webservice.model.interfaccianps.dto;

import java.util.Date;

public class ParametersElaboraNotificaAzione extends ParametersElaboraNotificaNPS {
	
	/**
	 * Message id.
	 */
	private String idMessaggioNPS;
	
	/**
	 * Codice aoo.
	 */
	private String codiceAooNPS;
	
	/**
	 * Id protocollo.
	 */
	private String idProtocollo;
	
	/**
	 * Numero protocollo.
	 */
	private int numeroProtocollo;
	
	/**
	 * Data protocollo.
	 */
	private Date dataProtocollo;
	
	/**
	 * Anno protocollo.
	 */
	private int annoProtocollo;
	
	/**
	 * Stato protocollo.
	 */
	private String statoProtocollo;
	
	/**
	 * Tipo azione.
	 */
	private String tipoAzione;
	
	/**
	 * Data azione.
	 */
	private Date dataAzione;
	
	/**
	 * Dati rispondi a.
	 */
	private ParametersAzioneRispostaAutomatica datiRispondiA;
	
	/**
	 * Costruttore.
	 * @param messageIdNPS
	 */
	public ParametersElaboraNotificaAzione(String messageIdNPS) {
		super(messageIdNPS);
	}


	/**
	 * @return the idMessaggioNPS
	 */
	public String getIdMessaggioNPS() {
		return idMessaggioNPS;
	}


	/**
	 * @param idMessaggioNPS the idMessaggioNPS to set
	 */
	public void setIdMessaggioNPS(String idMessaggioNPS) {
		this.idMessaggioNPS = idMessaggioNPS;
	}


	/**
	 * @return the codiceAooNPS
	 */
	public String getCodiceAooNPS() {
		return codiceAooNPS;
	}


	/**
	 * @param codiceAooNPS the codiceAooNPS to set
	 */
	public void setCodiceAooNPS(String codiceAooNPS) {
		this.codiceAooNPS = codiceAooNPS;
	}


	/**
	 * @return the idProtocollo
	 */
	public String getIdProtocollo() {
		return idProtocollo;
	}


	/**
	 * @param idProtocollo the idProtocollo to set
	 */
	public void setIdProtocollo(String idProtocollo) {
		this.idProtocollo = idProtocollo;
	}


	/**
	 * @return the numeroProtocollo
	 */
	public int getNumeroProtocollo() {
		return numeroProtocollo;
	}


	/**
	 * @param numeroProtocollo the numeroProtocollo to set
	 */
	public void setNumeroProtocollo(int numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}


	/**
	 * @return the dataProtocollo
	 */
	public Date getDataProtocollo() {
		return dataProtocollo;
	}


	/**
	 * @param dataProtocollo the dataProtocollo to set
	 */
	public void setDataProtocollo(Date dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}


	/**
	 * @return the annoProtocollo
	 */
	public int getAnnoProtocollo() {
		return annoProtocollo;
	}


	/**
	 * @param annoProtocollo the annoProtocollo to set
	 */
	public void setAnnoProtocollo(int annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}


	/**
	 * @return the statoProtocollo
	 */
	public String getStatoProtocollo() {
		return statoProtocollo;
	}


	/**
	 * @param statoProtocollo the statoProtocollo to set
	 */
	public void setStatoProtocollo(String statoProtocollo) {
		this.statoProtocollo = statoProtocollo;
	}


	/**
	 * @return the tipoAzione
	 */
	public String getTipoAzione() {
		return tipoAzione;
	}


	/**
	 * @param tipoAzione the tipoAzione to set
	 */
	public void setTipoAzione(String tipoAzione) {
		this.tipoAzione = tipoAzione;
	}


	/**
	 * @return the dataAzione
	 */
	public Date getDataAzione() {
		return dataAzione;
	}


	/**
	 * @param dataAzione the dataAzione to set
	 */
	public void setDataAzione(Date dataAzione) {
		this.dataAzione = dataAzione;
	}


	/**
	 * @return the datiRispondiA
	 */
	public ParametersAzioneRispostaAutomatica getDatiRispondiA() {
		return datiRispondiA;
	}


	/**
	 * @param datiRispondiA the datiRispondiA to set
	 */
	public void setDatiRispondiA(ParametersAzioneRispostaAutomatica datiRispondiA) {
		this.datiRispondiA = datiRispondiA;
	}

}