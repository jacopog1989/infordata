
package it.ibm.red.webservice.model.redservice.types.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MetadatoRed complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MetadatoRed">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="displayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="multiValues" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="nomeChoice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="operazione" type="{urn:red:types:v1}OperazioneRed" minOccurs="0"/>
 *         &lt;element name="tipoMetadato" type="{urn:red:types:v1}TipoMetadatoRed" minOccurs="0"/>
 *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatoRed", propOrder = {
    "displayName",
    "key",
    "multiValues",
    "nomeChoice",
    "operazione",
    "tipoMetadato",
    "value"
})
public class MetadatoRed {

	/**
	 * Display name.
	 */
    @XmlElementRef(name = "displayName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> displayName;

	/**
	 * Chiave.
	 */
    @XmlElementRef(name = "key", type = JAXBElement.class, required = false)
    protected JAXBElement<String> key;

	/**
	 * Multi valore.
	 */
    @XmlElement(nillable = true)
    protected List<String> multiValues;

	/**
	 * Switch.
	 */
    @XmlElementRef(name = "nomeChoice", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nomeChoice;

	/**
	 * Operazione.
	 */
    @XmlElementRef(name = "operazione", type = JAXBElement.class, required = false)
    protected JAXBElement<OperazioneRed> operazione;

	/**
	 * Tipo metadato.
	 */
    @XmlElementRef(name = "tipoMetadato", type = JAXBElement.class, required = false)
    protected JAXBElement<TipoMetadatoRed> tipoMetadato;

	/**
	 * Valore.
	 */
    @XmlElementRef(name = "value", type = JAXBElement.class, required = false)
    protected JAXBElement<String> value;

    /**
     * Gets the value of the displayName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDisplayName() {
        return displayName;
    }

    /**
     * Sets the value of the displayName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDisplayName(JAXBElement<String> value) {
        this.displayName = value;
    }

    /**
     * Gets the value of the key property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setKey(JAXBElement<String> value) {
        this.key = value;
    }

    /**
     * Gets the value of the multiValues property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the multiValues property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMultiValues().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getMultiValues() {
        if (multiValues == null) {
            multiValues = new ArrayList<>();
        }
        return this.multiValues;
    }

    /**
     * Gets the value of the nomeChoice property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNomeChoice() {
        return nomeChoice;
    }

    /**
     * Sets the value of the nomeChoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNomeChoice(JAXBElement<String> value) {
        this.nomeChoice = value;
    }

    /**
     * Gets the value of the operazione property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OperazioneRed }{@code >}
     *     
     */
    public JAXBElement<OperazioneRed> getOperazione() {
        return operazione;
    }

    /**
     * Sets the value of the operazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OperazioneRed }{@code >}
     *     
     */
    public void setOperazione(JAXBElement<OperazioneRed> value) {
        this.operazione = value;
    }

    /**
     * Gets the value of the tipoMetadato property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TipoMetadatoRed }{@code >}
     *     
     */
    public JAXBElement<TipoMetadatoRed> getTipoMetadato() {
        return tipoMetadato;
    }

    /**
     * Sets the value of the tipoMetadato property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TipoMetadatoRed }{@code >}
     *     
     */
    public void setTipoMetadato(JAXBElement<TipoMetadatoRed> value) {
        this.tipoMetadato = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setValue(JAXBElement<String> value) {
        this.value = value;
    }

}
