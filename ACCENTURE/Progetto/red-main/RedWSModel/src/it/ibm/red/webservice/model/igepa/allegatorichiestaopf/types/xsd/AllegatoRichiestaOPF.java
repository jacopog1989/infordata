
package it.ibm.red.webservice.model.igepa.allegatorichiestaopf.types.xsd;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per AllegatoRichiestaOPF complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="AllegatoRichiestaOPF">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="annoRichiesta" type="{http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd}AnnoFormatType" minOccurs="0"/>
 *         &lt;element name="dataProtocollo" type="{http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd}DataFormatType" minOccurs="0"/>
 *         &lt;element name="documentoAllegato" type="{http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd}DocumentFile" minOccurs="0"/>
 *         &lt;element name="numeroProtocollo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numeroRichiesta" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="oggettoAllegato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipologiaAllegato" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AllegatoRichiestaOPF", propOrder = {
    "annoRichiesta",
    "dataProtocollo",
    "documentoAllegato",
    "numeroProtocollo",
    "numeroRichiesta",
    "oggettoAllegato",
    "tipologiaAllegato"
})
public class AllegatoRichiestaOPF
    implements Serializable
{

    @XmlElementRef(name = "annoRichiesta", namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnoFormatType> annoRichiesta;
    @XmlElementRef(name = "dataProtocollo", namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<DataFormatType> dataProtocollo;
    @XmlElementRef(name = "documentoAllegato", namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<DocumentFile> documentoAllegato;
    @XmlElementRef(name = "numeroProtocollo", namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> numeroProtocollo;
    @XmlElementRef(name = "numeroRichiesta", namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> numeroRichiesta;
    @XmlElementRef(name = "oggettoAllegato", namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> oggettoAllegato;
    @XmlElementRef(name = "tipologiaAllegato", namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> tipologiaAllegato;

    /**
     * Recupera il valore della proprietà annoRichiesta.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnoFormatType }{@code >}
     *     
     */
    public JAXBElement<AnnoFormatType> getAnnoRichiesta() {
        return annoRichiesta;
    }

    /**
     * Imposta il valore della proprietà annoRichiesta.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnoFormatType }{@code >}
     *     
     */
    public void setAnnoRichiesta(JAXBElement<AnnoFormatType> value) {
        this.annoRichiesta = value;
    }

    /**
     * Recupera il valore della proprietà dataProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DataFormatType }{@code >}
     *     
     */
    public JAXBElement<DataFormatType> getDataProtocollo() {
        return dataProtocollo;
    }

    /**
     * Imposta il valore della proprietà dataProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DataFormatType }{@code >}
     *     
     */
    public void setDataProtocollo(JAXBElement<DataFormatType> value) {
        this.dataProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà documentoAllegato.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DocumentFile }{@code >}
     *     
     */
    public JAXBElement<DocumentFile> getDocumentoAllegato() {
        return documentoAllegato;
    }

    /**
     * Imposta il valore della proprietà documentoAllegato.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DocumentFile }{@code >}
     *     
     */
    public void setDocumentoAllegato(JAXBElement<DocumentFile> value) {
        this.documentoAllegato = value;
    }

    /**
     * Recupera il valore della proprietà numeroProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getNumeroProtocollo() {
        return numeroProtocollo;
    }

    /**
     * Imposta il valore della proprietà numeroProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setNumeroProtocollo(JAXBElement<Integer> value) {
        this.numeroProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà numeroRichiesta.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getNumeroRichiesta() {
        return numeroRichiesta;
    }

    /**
     * Imposta il valore della proprietà numeroRichiesta.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setNumeroRichiesta(JAXBElement<Integer> value) {
        this.numeroRichiesta = value;
    }

    /**
     * Recupera il valore della proprietà oggettoAllegato.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOggettoAllegato() {
        return oggettoAllegato;
    }

    /**
     * Imposta il valore della proprietà oggettoAllegato.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOggettoAllegato(JAXBElement<String> value) {
        this.oggettoAllegato = value;
    }

    /**
     * Recupera il valore della proprietà tipologiaAllegato.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getTipologiaAllegato() {
        return tipologiaAllegato;
    }

    /**
     * Imposta il valore della proprietà tipologiaAllegato.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setTipologiaAllegato(JAXBElement<Integer> value) {
        this.tipologiaAllegato = value;
    }

}
