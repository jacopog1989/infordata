//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2019.05.21 alle 01:14:00 PM CEST 
//


package it.ibm.red.webservice.model.redsiebel.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java per aggiornaRdsResponseType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="aggiornaRdsResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://red/ibm/RedSiebelService/types/}BaseResponseType">
 *       &lt;sequence>
 *         &lt;element name="idRdsSiebel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dataElaborazioneRed" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "aggiornaRdsResponseType", propOrder = {
    "idRdsSiebel",
    "dataElaborazioneRed"
})
public class AggiornaRdsResponseType
    extends BaseResponseType {

	/**
	 * Identificativo RDS Siebel.
	 */
    @XmlElement(required = true)
    protected String idRdsSiebel;

	/**
	 * Data elaborazione.
	 */
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataElaborazioneRed;

    /**
     * Recupera il valore della propriet� idRdsSiebel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRdsSiebel() {
        return idRdsSiebel;
    }

    /**
     * Imposta il valore della propriet� idRdsSiebel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRdsSiebel(String value) {
        this.idRdsSiebel = value;
    }

    /**
     * Recupera il valore della propriet� dataElaborazioneRed.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataElaborazioneRed() {
        return dataElaborazioneRed;
    }

    /**
     * Imposta il valore della propriet� dataElaborazioneRed.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataElaborazioneRed(XMLGregorianCalendar value) {
        this.dataElaborazioneRed = value;
    }

}
