
package it.ibm.red.webservice.model.redservice.types.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Identificativo Unita' documentale (fascicolo/fattura) con riferimento ai dati pregressi.
 * 
 * <p>Java class for IdentificativoUnitaDocumentaleFEPATypeRed complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IdentificativoUnitaDocumentaleFEPATypeRed">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="ID" type="{urn:red:types:v1}IdentificativoUnitaDocumentaleTypeRed"/>
 *         &lt;element name="IDMIGRAZIONE" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IDENTIFICATIVOSIFE" type="{urn:red:types:v1}IdentificativoDocumentoContabileType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificativoUnitaDocumentaleFEPATypeRed", propOrder = {
    "id",
    "idmigrazione",
    "identificativosife"
})
public class IdentificativoUnitaDocumentaleFEPATypeRed {


    /**
     * Identificativo.
     */
    @XmlElement(name = "ID")
    protected IdentificativoUnitaDocumentaleTypeRed id;

    /**
     * Id migrazione.
     */
    @XmlElement(name = "IDMIGRAZIONE")
    protected Integer idmigrazione;

    /**
     * Identificativo SIFE.
     */
    @XmlElement(name = "IDENTIFICATIVOSIFE")
    protected String identificativosife;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoUnitaDocumentaleTypeRed }
     *     
     */
    public IdentificativoUnitaDocumentaleTypeRed getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoUnitaDocumentaleTypeRed }
     *     
     */
    public void setID(IdentificativoUnitaDocumentaleTypeRed value) {
        this.id = value;
    }

    /**
     * Gets the value of the idmigrazione property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIDMIGRAZIONE() {
        return idmigrazione;
    }

    /**
     * Sets the value of the idmigrazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIDMIGRAZIONE(Integer value) {
        this.idmigrazione = value;
    }

    /**
     * Gets the value of the identificativosife property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDENTIFICATIVOSIFE() {
        return identificativosife;
    }

    /**
     * Sets the value of the identificativosife property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDENTIFICATIVOSIFE(String value) {
        this.identificativosife = value;
    }

}
