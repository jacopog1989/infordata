package it.ibm.red.webservice.model.documentservice.dto;

public enum Servizio {
	
	/**
	 * Valore.
	 */
	RED_CREAZIONE_DOCUMENTO_USCITA("redCreazioneDocumentoUscita"),
	
	/**
	 * Valore.
	 */
	RED_CREAZIONE_DOCUMENTO_ENTRATA("redCreazioneDocumentoEntrata"),
	
	/**
	 * Valore.
	 */
	RED_RICERCA_DOCUMENTI("redRicercaDocumenti"),
	
	/**
	 * Valore.
	 */
	RED_RICERCA_FASCICOLI("redRicercaFascicoli"),

	/**
	 * Valore.
	 */
	RED_RICERCA_FALDONI("redRicercaFaldoni"),
	
	/**
	 * Valore.
	 */
	RED_GET_ORGANIGRAMMA("redGetOrganigramma"),
	
	/**
	 * Valore.
	 */
	RED_FASCICOLAZIONE_DOCUMENTO("redFascicolazioneDocumento"),
	
	/**
	 * Valore.
	 */
	RED_ELIMINA_FASCICOLO("redEliminaFascicolo"),
	
	/**
	 * Valore.
	 */
	RED_CAMBIA_STATO_FASCICOLO("redCambiaStatoFascicolo"),
	
	/**
	 * Valore.
	 */
	RED_MODIFICA_FASCICOLO("redModificaFascicolo"),
	
	/**
	 * Valore.
	 */
	RED_ASSOCIA_FASCICOLO_A_TITOLARIO("redAssociaFascicoloATitolario"),
	
	/**
	 * Valore.
	 */
	RED_CREA_FASCICOLO("redCreaFascicolo"),
	
	/**
	 * Valore.
	 */
	RED_INSERISCI_PROT_ENTRATA("redInserisciProtEntrata"),
	
	/**
	 * Valore.
	 */
	RED_INSERISCI_PROT_USCITA("redInserisciProtUscita"),
	
	/**
	 * Valore.
	 */
	RED_INSERISCI_CAMPO_FIRMA("redInserisciCampoFirma"),
	
	/**
	 * Valore.
	 */
	RED_INSERISCI_POSTILLA("redInserisciPostilla"),

	/**
	 * Valore.
	 */
	RED_INSERISCI_ID_DOCUMENTO("redInserisciId"),

	/**
	 * Valore.
	 */
	RED_INSERISCI_WATERMARK("redInserisciWatermark"),
	
	/**
	 * Valore.
	 */
	RED_INSERISCI_APPROVAZIONE("redInserisciApprovazione"),
	
	/**
	 * Valore.
	 */
	RED_ELIMINA_FALDONE("redEliminaFaldone"),
	
	/**
	 * Valore.
	 */
	RED_CREA_FALDONE("redCreaFaldone"),
	
	/**
	 * Valore.
	 */
	RED_ASSOCIA_FASCICOLO_FALDONE("redAssociaFascicoloFaldone"),
	
	/**
	 * Valore.
	 */
	RED_RIMUOVI_ASSOCIAZIONE_FASCICOLO_FALDONE("redRimuoviAssociazioneFascicoloFaldone"),
	
	/**
	 * Valore.
	 */
	RED_SPOSTA_FASCICOLO_FALDONE("redSpostaFascicoloFaldone"),
	
	/**
	 * Valore.
	 */
	RED_ELENCO_VERSIONI_DOCUMENTO("redElencoVersioniDocumento"),

	/**
	 * Valore.
	 */
	RED_GET_VERSIONE_DOCUMENTO("redGetVersioneDocumento"),

	/**
	 * Valore.
	 */
	RED_CHECK_OUT("redCheckOut"),

	/**
	 * Valore.
	 */
	RED_UNDO_CHECK_OUT("redUndoCheckOut"),
	
	/**
	 * Valore.
	 */
	RED_CHECK_IN("redCheckIn"),
	
	/**
	 * Valore.
	 */
	RED_RICERCA_AVANZATA("redRicercaAvanzata"),
	
	/**
	 * Valore.
	 */
	RED_AVANZA_WORKFLOW("redAvanzaWorkflow"),
	
	/**
	 * Valore.
	 */
	RED_AVVIA_ISTANZA_WORKFLOW("redAvviaIstanzaWorkflow"),	
	
	/**
	 * Valore.
	 */
	RED_TRASFORMAZIONE_PDF("redTrasformazionePdf"),
	
	/**
	 * Valore.
	 */
	TRASFORMAZIONE_PDF("trasformazionePdf"),
	
	/**
	 * Valore.
	 */
	INSERISCI_PROT_ENTRATA("inserisciProtEntrata"),
	
	/**
	 * Valore.
	 */
	INSERISCI_PROT_USCITA("inserisciProtUscita"),
	
	/**
	 * Valore.
	 */
	INSERISCI_CAMPO_FIRMA("inserisciCampoFirma"),
	
	/**
	 * Valore.
	 */
	INSERISCI_POSTILLA("inserisciPostilla"),

	/**
	 * Valore.
	 */
	INSERISCI_ID("inserisciId"),
	
	/**
	 * Valore.
	 */
	INSERISCI_WATERMARK("inserisciWatermark"),
	
	/**
	 * Valore.
	 */
	INSERISCI_APPROVAZIONE("inserisciApprovazione"),
	
	/**
	 * Valore.
	 */
	RED_ELENCO_PROPERTIES("redElencoProperties"),
	
	/**
	 * Valore.
	 */
	RED_MODIFICA_METADATI("redModificaMetadati"),
	
	/**
	 * Valore.
	 */
	RED_CONSERVA_DOCUMENTO("redConservaDocumento"),

	/**
	 * Valore.
	 */
	RED_MODIFICA_SECURITY("redModificaSecurity"),
	
	/**
	 * Valore.
	 */
	RED_GET_CONTATTI("redGetContatti"),
	
	/**
	 * Valore.
	 */
	DF_MAPPING_ORGANIGRAMMA("DFMappingOrganigramma"),
	
	/**
	 * Valore.
	 */
	RED_PROTOCOLLO_NPS_SYNC("richiediProtocolloNPSync"),
	
	/**
	 * Valore.
	 */
	RED_UPLOAD_NPS_ASYNC("uploadDocToAsyncNps"),
	
	/**
	 * Valore.
	 */
	RED_AGGIUNGI_ALLACCI_ASYNC("aggiungiAllacciAsync"),
	
	/**
	 * Valore.
	 */
	RED_ASS_DOC_PROTOCOLLO_ASYNC("associateDocumentoProtocolloToAsyncNps"),
	
	/**
	 * Valore.
	 */
	RED_SPEDISCI_PROT_USCITA_ASYNC("spedisciProtocolloUscitaAsync"),

	/**
	 * Valore.
	 */
	RED_GESTISCI_DESTINATARI_INTERNI("gestisciDestinatariInterni");

		
	/**
	 * Descrizione.
	 */
	private String description;
	
	/**
	 * Costruttore.
	 * @param description
	 */
	Servizio(String description){
		this.description = description;
	}
	
	/**
	 * Restituisce la descrizione del servizio.
	 * @return descrizione
	 */
	public String getDescription() {
		return description;
	}
}
