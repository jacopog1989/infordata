
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraErroreProtocollazioneFlussoType;


/**
 * Dati per le notifiche di errore della protocollazione automatica da Flusso interoperabile
 * 
 * <p>Classe Java per wkf_datiNotificaErroreProtocollazioneFlusso_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_datiNotificaErroreProtocollazioneFlusso_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}wkf_datiErroreFlusso_type">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="ProtocollazioneCoda" type="{http://mef.gov.it.v1.npsTypes}wkf_datiProtocollazioneCoda_type" minOccurs="0"/>
 *         &lt;element name="NotificaInviata" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://mef.gov.it.v1.npsTypes}wkf_datiNotificaInviata_type">
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_datiNotificaErroreProtocollazioneFlusso_type", propOrder = {
    "protocollazioneCoda",
    "notificaInviata"
})
@XmlSeeAlso({
    RichiestaElaboraErroreProtocollazioneFlussoType.class
})
public class WkfDatiNotificaErroreProtocollazioneFlussoType
    extends WkfDatiErroreFlussoType
    implements Serializable
{

    @XmlElement(name = "ProtocollazioneCoda")
    protected WkfDatiProtocollazioneCodaType protocollazioneCoda;
    @XmlElement(name = "NotificaInviata")
    protected WkfDatiNotificaErroreProtocollazioneFlussoType.NotificaInviata notificaInviata;

    /**
     * Recupera il valore della proprietà protocollazioneCoda.
     * 
     * @return
     *     possible object is
     *     {@link WkfDatiProtocollazioneCodaType }
     *     
     */
    public WkfDatiProtocollazioneCodaType getProtocollazioneCoda() {
        return protocollazioneCoda;
    }

    /**
     * Imposta il valore della proprietà protocollazioneCoda.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfDatiProtocollazioneCodaType }
     *     
     */
    public void setProtocollazioneCoda(WkfDatiProtocollazioneCodaType value) {
        this.protocollazioneCoda = value;
    }

    /**
     * Recupera il valore della proprietà notificaInviata.
     * 
     * @return
     *     possible object is
     *     {@link WkfDatiNotificaErroreProtocollazioneFlussoType.NotificaInviata }
     *     
     */
    public WkfDatiNotificaErroreProtocollazioneFlussoType.NotificaInviata getNotificaInviata() {
        return notificaInviata;
    }

    /**
     * Imposta il valore della proprietà notificaInviata.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfDatiNotificaErroreProtocollazioneFlussoType.NotificaInviata }
     *     
     */
    public void setNotificaInviata(WkfDatiNotificaErroreProtocollazioneFlussoType.NotificaInviata value) {
        this.notificaInviata = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}wkf_datiNotificaInviata_type">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class NotificaInviata
        extends WkfDatiNotificaInviataType
        implements Serializable
    {


    }

}
