
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Definisce il tipo di dato OperazioneDocumentale che è un 
 * 
 * <p>Classe Java per doc_completamentoOperazioneDocumentale_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="doc_completamentoOperazioneDocumentale_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoOperazione" type="{http://mef.gov.it.v1.npsTypes}doc_tipoOperazione_type"/>
 *         &lt;element name="DataOperazione" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DocumentoOriginale" type="{http://mef.gov.it.v1.npsTypes}doc_documentoFile_type"/>
 *         &lt;element name="DocumentoOperazione" type="{http://mef.gov.it.v1.npsTypes}doc_documentoFile_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "doc_completamentoOperazioneDocumentale_type", propOrder = {
    "tipoOperazione",
    "dataOperazione",
    "documentoOriginale",
    "documentoOperazione"
})
public class DocCompletamentoOperazioneDocumentaleType
    implements Serializable
{

    @XmlElement(name = "TipoOperazione", required = true)
    @XmlSchemaType(name = "string")
    protected DocTipoOperazioneType tipoOperazione;
    @XmlElement(name = "DataOperazione", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataOperazione;
    @XmlElement(name = "DocumentoOriginale", required = true)
    protected DocDocumentoFileType documentoOriginale;
    @XmlElement(name = "DocumentoOperazione")
    protected DocDocumentoFileType documentoOperazione;

    /**
     * Recupera il valore della proprietà tipoOperazione.
     * 
     * @return
     *     possible object is
     *     {@link DocTipoOperazioneType }
     *     
     */
    public DocTipoOperazioneType getTipoOperazione() {
        return tipoOperazione;
    }

    /**
     * Imposta il valore della proprietà tipoOperazione.
     * 
     * @param value
     *     allowed object is
     *     {@link DocTipoOperazioneType }
     *     
     */
    public void setTipoOperazione(DocTipoOperazioneType value) {
        this.tipoOperazione = value;
    }

    /**
     * Recupera il valore della proprietà dataOperazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataOperazione() {
        return dataOperazione;
    }

    /**
     * Imposta il valore della proprietà dataOperazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataOperazione(XMLGregorianCalendar value) {
        this.dataOperazione = value;
    }

    /**
     * Recupera il valore della proprietà documentoOriginale.
     * 
     * @return
     *     possible object is
     *     {@link DocDocumentoFileType }
     *     
     */
    public DocDocumentoFileType getDocumentoOriginale() {
        return documentoOriginale;
    }

    /**
     * Imposta il valore della proprietà documentoOriginale.
     * 
     * @param value
     *     allowed object is
     *     {@link DocDocumentoFileType }
     *     
     */
    public void setDocumentoOriginale(DocDocumentoFileType value) {
        this.documentoOriginale = value;
    }

    /**
     * Recupera il valore della proprietà documentoOperazione.
     * 
     * @return
     *     possible object is
     *     {@link DocDocumentoFileType }
     *     
     */
    public DocDocumentoFileType getDocumentoOperazione() {
        return documentoOperazione;
    }

    /**
     * Imposta il valore della proprietà documentoOperazione.
     * 
     * @param value
     *     allowed object is
     *     {@link DocDocumentoFileType }
     *     
     */
    public void setDocumentoOperazione(DocDocumentoFileType value) {
        this.documentoOperazione = value;
    }

}
