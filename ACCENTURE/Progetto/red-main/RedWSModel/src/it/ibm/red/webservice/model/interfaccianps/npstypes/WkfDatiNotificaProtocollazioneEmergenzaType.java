
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioProtocollazioneEmergenzaType;


/**
 * Dati per le notifiche di protocollazione dal registro di emergenza
 * 
 * <p>Classe Java per wkf_datiNotificaProtocollazioneEmergenza_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_datiNotificaProtocollazioneEmergenza_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdMessaggio" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DataElaborazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.npsTypes}wkf_identificatoreProtcollo_type"/>
 *         &lt;element name="ProtocolloEmergenza" type="{http://mef.gov.it.v1.npsTypes}wkf_identificatoreProtcollo_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_datiNotificaProtocollazioneEmergenza_type", propOrder = {
    "idMessaggio",
    "oggetto",
    "dataElaborazione",
    "protocollo",
    "protocolloEmergenza"
})
@XmlSeeAlso({
    RichiestaElaboraMessaggioProtocollazioneEmergenzaType.class
})
public class WkfDatiNotificaProtocollazioneEmergenzaType
    implements Serializable
{

    @XmlElement(name = "IdMessaggio", required = true)
    protected String idMessaggio;
    @XmlElement(name = "Oggetto", required = true)
    protected String oggetto;
    @XmlElement(name = "DataElaborazione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataElaborazione;
    @XmlElement(name = "Protocollo", required = true)
    protected WkfIdentificatoreProtcolloType protocollo;
    @XmlElement(name = "ProtocolloEmergenza", required = true)
    protected WkfIdentificatoreProtcolloType protocolloEmergenza;

    /**
     * Recupera il valore della proprietà idMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggio() {
        return idMessaggio;
    }

    /**
     * Imposta il valore della proprietà idMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggio(String value) {
        this.idMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà oggetto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Imposta il valore della proprietà oggetto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Recupera il valore della proprietà dataElaborazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataElaborazione() {
        return dataElaborazione;
    }

    /**
     * Imposta il valore della proprietà dataElaborazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataElaborazione(XMLGregorianCalendar value) {
        this.dataElaborazione = value;
    }

    /**
     * Recupera il valore della proprietà protocollo.
     * 
     * @return
     *     possible object is
     *     {@link WkfIdentificatoreProtcolloType }
     *     
     */
    public WkfIdentificatoreProtcolloType getProtocollo() {
        return protocollo;
    }

    /**
     * Imposta il valore della proprietà protocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfIdentificatoreProtcolloType }
     *     
     */
    public void setProtocollo(WkfIdentificatoreProtcolloType value) {
        this.protocollo = value;
    }

    /**
     * Recupera il valore della proprietà protocolloEmergenza.
     * 
     * @return
     *     possible object is
     *     {@link WkfIdentificatoreProtcolloType }
     *     
     */
    public WkfIdentificatoreProtcolloType getProtocolloEmergenza() {
        return protocolloEmergenza;
    }

    /**
     * Imposta il valore della proprietà protocolloEmergenza.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfIdentificatoreProtcolloType }
     *     
     */
    public void setProtocolloEmergenza(WkfIdentificatoreProtcolloType value) {
        this.protocolloEmergenza = value;
    }

}
