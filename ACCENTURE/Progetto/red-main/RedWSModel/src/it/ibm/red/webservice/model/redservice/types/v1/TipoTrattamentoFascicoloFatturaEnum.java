
package it.ibm.red.webservice.model.redservice.types.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoTrattamentoFascicoloFatturaEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoTrattamentoFascicoloFatturaEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Elimina"/>
 *     &lt;enumeration value="Aggiungi"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoTrattamentoFascicoloFatturaEnum")
@XmlEnum
public enum TipoTrattamentoFascicoloFatturaEnum {


    /**
     * Elimina i fascicoli Fattura dal decreto.
     * 
     */
    @XmlEnumValue("Elimina")
    ELIMINA("Elimina"),

    /**
     * Aggiungi i fascicoli fattura al fascicolo del decreto.
     * 
     */
    @XmlEnumValue("Aggiungi")
    AGGIUNGI("Aggiungi");

    /**
     * Valore.
     */
    private final String value;

    TipoTrattamentoFascicoloFatturaEnum(String v) {
        value = v;
    }

    /**
     * Restituisce il valore.
     * @return value
     */
    public String value() {
        return value;
    }

    /**
     * Restituisce l'enum associato al value.
     * @param v
     * @return enum associato al value
     */
    public static TipoTrattamentoFascicoloFatturaEnum fromValue(String v) {
        for (TipoTrattamentoFascicoloFatturaEnum c: TipoTrattamentoFascicoloFatturaEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
