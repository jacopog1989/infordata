
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioProtocollazioneAutomaticaType;


/**
 * Dati per le notifiche di protocollazione automatica da interoperabilità
 * 
 * <p>Classe Java per wkf_datiNotificaProtocollazioneAutomatica_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_datiNotificaProtocollazioneAutomatica_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}wkf_datiNotifica_type">
 *       &lt;sequence>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.npsTypes}wkf_identificatoreProtcollo_type"/>
 *         &lt;element name="messaggioRicevuto" type="{http://mef.gov.it.v1.npsTypes}email_message_type"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_datiNotificaProtocollazioneAutomatica_type", propOrder = {
    "protocollo",
    "messaggioRicevuto"
})
@XmlSeeAlso({
    RichiestaElaboraMessaggioProtocollazioneAutomaticaType.class
})
public class WkfDatiNotificaProtocollazioneAutomaticaType
    extends WkfDatiNotificaType
    implements Serializable
{

    @XmlElement(name = "Protocollo", required = true)
    protected WkfIdentificatoreProtcolloType protocollo;
    @XmlElement(required = true)
    protected EmailMessageType messaggioRicevuto;

    /**
     * Recupera il valore della proprietà protocollo.
     * 
     * @return
     *     possible object is
     *     {@link WkfIdentificatoreProtcolloType }
     *     
     */
    public WkfIdentificatoreProtcolloType getProtocollo() {
        return protocollo;
    }

    /**
     * Imposta il valore della proprietà protocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfIdentificatoreProtcolloType }
     *     
     */
    public void setProtocollo(WkfIdentificatoreProtcolloType value) {
        this.protocollo = value;
    }

    /**
     * Recupera il valore della proprietà messaggioRicevuto.
     * 
     * @return
     *     possible object is
     *     {@link EmailMessageType }
     *     
     */
    public EmailMessageType getMessaggioRicevuto() {
        return messaggioRicevuto;
    }

    /**
     * Imposta il valore della proprietà messaggioRicevuto.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailMessageType }
     *     
     */
    public void setMessaggioRicevuto(EmailMessageType value) {
        this.messaggioRicevuto = value;
    }

}
