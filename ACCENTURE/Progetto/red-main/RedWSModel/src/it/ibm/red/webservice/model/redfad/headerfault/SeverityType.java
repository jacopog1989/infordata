package it.ibm.red.webservice.model.redfad.headerfault;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

/**
 * <p>Java class for SeverityType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SeverityType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Critical"/>
 *     &lt;enumeration value="Warning"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum SeverityType {

	/**
	 * Errore Critico.
	 */
    @XmlEnumValue("Critical")
    CRITICAL("Critical"),
    
    /**
     * Errore Warning.
     */
    @XmlEnumValue("Warning")
    WARNING("Warning");
    
    /**
     * Value.
     */
    private final String value;

    /**
     * Costruttore.
     * @param v
     */
    SeverityType(String v) {
        value = v;
    }

    /**
     * Restituisce il value.
     * @return value
     */
    public String value() {
        return value;
    }

    /**
     * Restituisce l'enum associata al value.
     * @param v
     * @return enum associata al value
     */
    public static SeverityType fromValue(String v) {
        for (SeverityType c: SeverityType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
