
package it.ibm.red.webservice.model.redservice.header.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the red.header.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

	/**
	 * QName.
	 */
    private static final QName _CredenzialiUtenteRedIdUtente_QNAME = new QName("", "idUtente");

	/**
	 * QName.
	 */
    private static final QName _CredenzialiWSRedIdClient_QNAME = new QName("", "idClient");

	/**
	 * QName.
	 */
    private static final QName _CredenzialiWSRedPwdClient_QNAME = new QName("", "pwdClient");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: red.header.v1.
     * 
     */
    public ObjectFactory() {
    	// Costruttore di default.
    }

    /**
     * Create an instance of {@link CredenzialiWSRed }.
     * 
     */
    public CredenzialiWSRed createCredenzialiWSRed() {
        return new CredenzialiWSRed();
    }

    /**
     * Create an instance of {@link CredenzialiUtenteRed }.
     * 
     */
    public CredenzialiUtenteRed createCredenzialiUtenteRed() {
        return new CredenzialiUtenteRed();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}.
     * 
     */
    @XmlElementDecl(namespace = "", name = "idUtente", scope = CredenzialiUtenteRed.class)
    public JAXBElement<String> createCredenzialiUtenteRedIdUtente(String value) {
        return new JAXBElement<String>(_CredenzialiUtenteRedIdUtente_QNAME, String.class, CredenzialiUtenteRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}.
     * 
     */
    @XmlElementDecl(namespace = "", name = "idClient", scope = CredenzialiWSRed.class)
    public JAXBElement<String> createCredenzialiWSRedIdClient(String value) {
        return new JAXBElement<String>(_CredenzialiWSRedIdClient_QNAME, String.class, CredenzialiWSRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}.
     * 
     */
    @XmlElementDecl(namespace = "", name = "pwdClient", scope = CredenzialiWSRed.class)
    public JAXBElement<String> createCredenzialiWSRedPwdClient(String value) {
        return new JAXBElement<String>(_CredenzialiWSRedPwdClient_QNAME, String.class, CredenzialiWSRed.class, value);
    }

}
