
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Dati per la notifica della assegnazione per applicazione di un protocollo
 * 
 * <p>Classe Java per wkf_azioneAssegnazioneApplicazioneProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_azioneAssegnazioneApplicazioneProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Applicazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DatiAssegnatario" type="{http://mef.gov.it.v1.npsTypes}prot_assegnatario_type"/>
 *         &lt;element name="Stato" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_azioneAssegnazioneApplicazioneProtocollo_type", propOrder = {
    "applicazione",
    "datiAssegnatario",
    "stato"
})
@XmlSeeAlso({
    WkfAzioneCambioAssegnazioneApplicazioneProtocolloType.class,
    WkfAzioneRimozioneAssegnazioneApplicazioneProtocolloType.class
})
public class WkfAzioneAssegnazioneApplicazioneProtocolloType
    implements Serializable
{

    @XmlElement(name = "Applicazione", required = true)
    protected String applicazione;
    @XmlElement(name = "DatiAssegnatario", required = true)
    protected ProtAssegnatarioType datiAssegnatario;
    @XmlElement(name = "Stato", required = true)
    protected String stato;

    /**
     * Recupera il valore della proprietà applicazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicazione() {
        return applicazione;
    }

    /**
     * Imposta il valore della proprietà applicazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicazione(String value) {
        this.applicazione = value;
    }

    /**
     * Recupera il valore della proprietà datiAssegnatario.
     * 
     * @return
     *     possible object is
     *     {@link ProtAssegnatarioType }
     *     
     */
    public ProtAssegnatarioType getDatiAssegnatario() {
        return datiAssegnatario;
    }

    /**
     * Imposta il valore della proprietà datiAssegnatario.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtAssegnatarioType }
     *     
     */
    public void setDatiAssegnatario(ProtAssegnatarioType value) {
        this.datiAssegnatario = value;
    }

    /**
     * Recupera il valore della proprietà stato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStato() {
        return stato;
    }

    /**
     * Imposta il valore della proprietà stato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStato(String value) {
        this.stato = value;
    }

}
