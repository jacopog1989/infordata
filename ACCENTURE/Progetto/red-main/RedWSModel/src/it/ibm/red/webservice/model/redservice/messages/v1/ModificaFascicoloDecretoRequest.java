
package it.ibm.red.webservice.model.redservice.messages.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import it.ibm.red.webservice.model.redservice.header.v1.CredenzialiUtenteRed;
import it.ibm.red.webservice.model.redservice.header.v1.CredenzialiWSRed;
import it.ibm.red.webservice.model.redservice.types.v1.DocumentoRed;
import it.ibm.red.webservice.model.redservice.types.v1.IdentificativoUnitaDocumentaleFEPATypeRed;
import it.ibm.red.webservice.model.redservice.types.v1.TipoTrattamentoFascicoloFatturaEnum;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="credenzialiWS" type="{urn:red:header:v1}CredenzialiWSRed" minOccurs="0"/>
 *         &lt;element name="credenzialiUtente" type="{urn:red:header:v1}CredenzialiUtenteRed" minOccurs="0"/>
 *         &lt;element name="documento" type="{urn:red:types:v1}DocumentoRed"/>
 *         &lt;element name="FascicoliFattura" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Fascicolo" type="{urn:red:types:v1}IdentificativoUnitaDocumentaleFEPATypeRed"/>
 *                   &lt;element name="TIPOTRATTAMENTO" type="{urn:red:types:v1}tipoTrattamentoFascicoloFatturaEnum"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "credenzialiWS",
    "credenzialiUtente",
    "documento",
    "fascicoliFattura"
})
@XmlRootElement(name = "modificaFascicoloDecretoRequest")
public class ModificaFascicoloDecretoRequest {

	/**
	 * Credenaizli WS.
	 */
    @XmlElementRef(name = "credenzialiWS", type = JAXBElement.class, required = false)
    protected JAXBElement<CredenzialiWSRed> credenzialiWS;

	/**
	 * Credenziali utente.
	 */
    @XmlElementRef(name = "credenzialiUtente", type = JAXBElement.class, required = false)
    protected JAXBElement<CredenzialiUtenteRed> credenzialiUtente;

	/**
	 * Documento.
	 */
    @XmlElement(required = true, nillable = true)
    protected DocumentoRed documento;

	/**
	 * Fasicoli fattura.
	 */
    @XmlElement(name = "FascicoliFattura")
    protected List<ModificaFascicoloDecretoRequest.FascicoliFattura> fascicoliFattura;

    /**
     * Gets the value of the credenzialiWS property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CredenzialiWSRed }{@code >}
     *     
     */
    public JAXBElement<CredenzialiWSRed> getCredenzialiWS() {
        return credenzialiWS;
    }

    /**
     * Sets the value of the credenzialiWS property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CredenzialiWSRed }{@code >}
     *     
     */
    public void setCredenzialiWS(JAXBElement<CredenzialiWSRed> value) {
        this.credenzialiWS = value;
    }

    /**
     * Gets the value of the credenzialiUtente property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CredenzialiUtenteRed }{@code >}
     *     
     */
    public JAXBElement<CredenzialiUtenteRed> getCredenzialiUtente() {
        return credenzialiUtente;
    }

    /**
     * Sets the value of the credenzialiUtente property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CredenzialiUtenteRed }{@code >}
     *     
     */
    public void setCredenzialiUtente(JAXBElement<CredenzialiUtenteRed> value) {
        this.credenzialiUtente = value;
    }

    /**
     * Gets the value of the documento property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoRed }
     *     
     */
    public DocumentoRed getDocumento() {
        return documento;
    }

    /**
     * Sets the value of the documento property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoRed }
     *     
     */
    public void setDocumento(DocumentoRed value) {
        this.documento = value;
    }

    /**
     * Gets the value of the fascicoliFattura property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fascicoliFattura property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFascicoliFattura().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ModificaFascicoloDecretoRequest.FascicoliFattura }
     * 
     * 
     */
    public List<ModificaFascicoloDecretoRequest.FascicoliFattura> getFascicoliFattura() {
        if (fascicoliFattura == null) {
            fascicoliFattura = new ArrayList<>();
        }
        return this.fascicoliFattura;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Fascicolo" type="{urn:red:types:v1}IdentificativoUnitaDocumentaleFEPATypeRed"/>
     *         &lt;element name="TIPOTRATTAMENTO" type="{urn:red:types:v1}tipoTrattamentoFascicoloFatturaEnum"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fascicolo",
        "tipotrattamento"
    })
    public static class FascicoliFattura {

    	
    	/**
    	 * Fascicolo.
    	 */
        @XmlElement(name = "Fascicolo", required = true)
        protected IdentificativoUnitaDocumentaleFEPATypeRed fascicolo;
    	
    	/**
    	 * Tipo trattamento.
    	 */
        @XmlElement(name = "TIPOTRATTAMENTO", required = true)
        protected TipoTrattamentoFascicoloFatturaEnum tipotrattamento;

        /**
         * Gets the value of the fascicolo property.
         * 
         * @return
         *     possible object is
         *     {@link IdentificativoUnitaDocumentaleFEPATypeRed }
         *     
         */
        public IdentificativoUnitaDocumentaleFEPATypeRed getFascicolo() {
            return fascicolo;
        }

        /**
         * Sets the value of the fascicolo property.
         * 
         * @param value
         *     allowed object is
         *     {@link IdentificativoUnitaDocumentaleFEPATypeRed }
         *     
         */
        public void setFascicolo(IdentificativoUnitaDocumentaleFEPATypeRed value) {
            this.fascicolo = value;
        }

        /**
         * Gets the value of the tipotrattamento property.
         * 
         * @return
         *     possible object is
         *     {@link TipoTrattamentoFascicoloFatturaEnum }
         *     
         */
        public TipoTrattamentoFascicoloFatturaEnum getTIPOTRATTAMENTO() {
            return tipotrattamento;
        }

        /**
         * Sets the value of the tipotrattamento property.
         * 
         * @param value
         *     allowed object is
         *     {@link TipoTrattamentoFascicoloFatturaEnum }
         *     
         */
        public void setTIPOTRATTAMENTO(TipoTrattamentoFascicoloFatturaEnum value) {
            this.tipotrattamento = value;
        }

    }

}
