
package it.ibm.red.webservice.model.interfaccianps.postacerttypes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://mef.gov.it.v1.postacertTypes/}mittente"/>
 *         &lt;element ref="{http://mef.gov.it.v1.postacertTypes/}destinatari" maxOccurs="unbounded"/>
 *         &lt;element ref="{http://mef.gov.it.v1.postacertTypes/}risposte"/>
 *         &lt;element ref="{http://mef.gov.it.v1.postacertTypes/}oggetto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mittente",
    "destinatari",
    "risposte",
    "oggetto"
})
@XmlRootElement(name = "intestazione")
public class Intestazione
    implements Serializable
{

    @XmlElement(required = true)
    protected Mittente mittente;
    @XmlElement(required = true)
    protected List<Destinatari> destinatari;
    @XmlElement(required = true)
    protected Risposte risposte;
    protected Oggetto oggetto;

    /**
     * Recupera il valore della proprietà mittente.
     * 
     * @return
     *     possible object is
     *     {@link Mittente }
     *     
     */
    public Mittente getMittente() {
        return mittente;
    }

    /**
     * Imposta il valore della proprietà mittente.
     * 
     * @param value
     *     allowed object is
     *     {@link Mittente }
     *     
     */
    public void setMittente(Mittente value) {
        this.mittente = value;
    }

    /**
     * Gets the value of the destinatari property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the destinatari property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDestinatari().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Destinatari }
     * 
     * 
     */
    public List<Destinatari> getDestinatari() {
        if (destinatari == null) {
            destinatari = new ArrayList<Destinatari>();
        }
        return this.destinatari;
    }

    /**
     * Recupera il valore della proprietà risposte.
     * 
     * @return
     *     possible object is
     *     {@link Risposte }
     *     
     */
    public Risposte getRisposte() {
        return risposte;
    }

    /**
     * Imposta il valore della proprietà risposte.
     * 
     * @param value
     *     allowed object is
     *     {@link Risposte }
     *     
     */
    public void setRisposte(Risposte value) {
        this.risposte = value;
    }

    /**
     * Recupera il valore della proprietà oggetto.
     * 
     * @return
     *     possible object is
     *     {@link Oggetto }
     *     
     */
    public Oggetto getOggetto() {
        return oggetto;
    }

    /**
     * Imposta il valore della proprietà oggetto.
     * 
     * @param value
     *     allowed object is
     *     {@link Oggetto }
     *     
     */
    public void setOggetto(Oggetto value) {
        this.oggetto = value;
    }

}
