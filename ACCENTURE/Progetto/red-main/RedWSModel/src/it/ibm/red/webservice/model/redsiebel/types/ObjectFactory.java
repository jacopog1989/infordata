//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2019.05.21 alle 01:14:00 PM CEST 
//


package it.ibm.red.webservice.model.redsiebel.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.ibm.red.webservice.model.redsiebel.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


	/**
	 * Qname.
	 */
	private static final QName _AggiornaRdsResponse_QNAME = new QName("http://red/ibm/RedSiebelService/types/", "aggiornaRdsResponse");

	/**
	 * Qname.
	 */
	private static final QName _AggiornaRdsRequest_QNAME = new QName("http://red/ibm/RedSiebelService/types/", "aggiornaRdsRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.ibm.red.webservice.model.redsiebel.types
     * 
     */
    public ObjectFactory() {
    	// Costruttore di default.
    }

    /**
     * Create an instance of {@link AggiornaRdsRequestType }
     * 
     */
    public AggiornaRdsRequestType createAggiornaRdsRequestType() {
        return new AggiornaRdsRequestType();
    }

    /**
     * Create an instance of {@link AggiornaRdsResponseType }
     * 
     */
    public AggiornaRdsResponseType createAggiornaRdsResponseType() {
        return new AggiornaRdsResponseType();
    }

    /**
     * Create an instance of {@link AllegatiType }
     * 
     */
    public AllegatiType createAllegatiType() {
        return new AllegatiType();
    }

    /**
     * Create an instance of {@link AllegatoType }
     * 
     */
    public AllegatoType createAllegatoType() {
        return new AllegatoType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AggiornaRdsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://red/ibm/RedSiebelService/types/", name = "aggiornaRdsResponse")
    public JAXBElement<AggiornaRdsResponseType> createAggiornaRdsResponse(AggiornaRdsResponseType value) {
        return new JAXBElement<>(_AggiornaRdsResponse_QNAME, AggiornaRdsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AggiornaRdsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://red/ibm/RedSiebelService/types/", name = "aggiornaRdsRequest")
    public JAXBElement<AggiornaRdsRequestType> createAggiornaRdsRequest(AggiornaRdsRequestType value) {
        return new JAXBElement<>(_AggiornaRdsRequest_QNAME, AggiornaRdsRequestType.class, null, value);
    }

}
