
package it.ibm.red.webservice.model.igepa.allegatorichiestaopf.types.xsd;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per BaseResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="BaseResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="status" type="{http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd}InvocationStatus" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseResponse", propOrder = {
    "description",
    "status"
})
public class BaseResponse
    implements Serializable
{

    @XmlElementRef(name = "description", namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    @XmlElementRef(name = "status", namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<InvocationStatus> status;

    /**
     * Recupera il valore della proprietà description.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Imposta il valore della proprietà description.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Recupera il valore della proprietà status.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link InvocationStatus }{@code >}
     *     
     */
    public JAXBElement<InvocationStatus> getStatus() {
        return status;
    }

    /**
     * Imposta il valore della proprietà status.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link InvocationStatus }{@code >}
     *     
     */
    public void setStatus(JAXBElement<InvocationStatus> value) {
        this.status = value;
    }

}
