
package it.ibm.red.webservice.model.igepa.inviarichiestaopf.types.xsd;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per EnteMittenteFormatType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="EnteMittenteFormatType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enteMittenteFormatType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnteMittenteFormatType", propOrder = {
    "enteMittenteFormatType"
})
public class EnteMittenteFormatType
    implements Serializable
{

    @XmlElementRef(name = "enteMittenteFormatType", namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> enteMittenteFormatType;

    /**
     * Recupera il valore della proprietà enteMittenteFormatType.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEnteMittenteFormatType() {
        return enteMittenteFormatType;
    }

    /**
     * Imposta il valore della proprietà enteMittenteFormatType.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEnteMittenteFormatType(JAXBElement<String> value) {
        this.enteMittenteFormatType = value;
    }

}
