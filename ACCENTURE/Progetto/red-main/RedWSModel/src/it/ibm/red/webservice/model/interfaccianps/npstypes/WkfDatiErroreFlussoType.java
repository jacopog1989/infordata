
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo.ContestoProcedurale;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraErroreFlussoType;


/**
 * Dati dell'errore del flusso
 * 
 * <p>Classe Java per wkf_datiErroreFlusso_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_datiErroreFlusso_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdMessaggio" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="DataOperazione" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CodiceFlusso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdDocumento" type="{http://mef.gov.it.v1.npsTypes}Guid" minOccurs="0"/>
 *         &lt;element name="ChiaveDocumentale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChiaveFascicolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChiaveFascicoloRiferimento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChiaveDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdentificatoreProcesso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdentificativoMessaggio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContestoProcedurale" type="{http://www.digitPa.gov.it/protocollo/}ContestoProcedurale" minOccurs="0"/>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.npsTypes}wkf_identificatoreProtcollo_type" minOccurs="0"/>
 *         &lt;element name="CodiceErrore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="messaggioRicevuto" type="{http://mef.gov.it.v1.npsTypes}email_message_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_datiErroreFlusso_type", propOrder = {
    "idMessaggio",
    "dataOperazione",
    "oggetto",
    "codiceFlusso",
    "idDocumento",
    "chiaveDocumentale",
    "chiaveFascicolo",
    "chiaveFascicoloRiferimento",
    "tipoFascicolo",
    "chiaveDocumento",
    "identificatoreProcesso",
    "identificativoMessaggio",
    "contestoProcedurale",
    "protocollo",
    "codiceErrore",
    "messaggioRicevuto"
})
@XmlSeeAlso({
    WkfDatiNotificaErroreProtocollazioneFlussoType.class,
    RichiestaElaboraErroreFlussoType.class
})
public class WkfDatiErroreFlussoType
    implements Serializable
{

    @XmlElement(name = "IdMessaggio", required = true)
    protected String idMessaggio;
    @XmlElement(name = "DataOperazione", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataOperazione;
    @XmlElement(name = "Oggetto", required = true)
    protected String oggetto;
    @XmlElement(name = "CodiceFlusso", required = true)
    protected String codiceFlusso;
    @XmlElement(name = "IdDocumento")
    protected String idDocumento;
    @XmlElement(name = "ChiaveDocumentale")
    protected String chiaveDocumentale;
    @XmlElement(name = "ChiaveFascicolo")
    protected String chiaveFascicolo;
    @XmlElement(name = "ChiaveFascicoloRiferimento")
    protected String chiaveFascicoloRiferimento;
    @XmlElement(name = "TipoFascicolo")
    protected String tipoFascicolo;
    @XmlElement(name = "ChiaveDocumento")
    protected String chiaveDocumento;
    @XmlElement(name = "IdentificatoreProcesso")
    protected String identificatoreProcesso;
    @XmlElement(name = "IdentificativoMessaggio")
    protected String identificativoMessaggio;
    @XmlElement(name = "ContestoProcedurale")
    protected ContestoProcedurale contestoProcedurale;
    @XmlElement(name = "Protocollo")
    protected WkfIdentificatoreProtcolloType protocollo;
    @XmlElement(name = "CodiceErrore", required = true)
    protected String codiceErrore;
    protected EmailMessageType messaggioRicevuto;

    /**
     * Recupera il valore della proprietà idMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggio() {
        return idMessaggio;
    }

    /**
     * Imposta il valore della proprietà idMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggio(String value) {
        this.idMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà dataOperazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataOperazione() {
        return dataOperazione;
    }

    /**
     * Imposta il valore della proprietà dataOperazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataOperazione(XMLGregorianCalendar value) {
        this.dataOperazione = value;
    }

    /**
     * Recupera il valore della proprietà oggetto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Imposta il valore della proprietà oggetto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Recupera il valore della proprietà codiceFlusso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceFlusso() {
        return codiceFlusso;
    }

    /**
     * Imposta il valore della proprietà codiceFlusso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceFlusso(String value) {
        this.codiceFlusso = value;
    }

    /**
     * Recupera il valore della proprietà idDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Imposta il valore della proprietà idDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

    /**
     * Recupera il valore della proprietà chiaveDocumentale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveDocumentale() {
        return chiaveDocumentale;
    }

    /**
     * Imposta il valore della proprietà chiaveDocumentale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveDocumentale(String value) {
        this.chiaveDocumentale = value;
    }

    /**
     * Recupera il valore della proprietà chiaveFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveFascicolo() {
        return chiaveFascicolo;
    }

    /**
     * Imposta il valore della proprietà chiaveFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveFascicolo(String value) {
        this.chiaveFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà chiaveFascicoloRiferimento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveFascicoloRiferimento() {
        return chiaveFascicoloRiferimento;
    }

    /**
     * Imposta il valore della proprietà chiaveFascicoloRiferimento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveFascicoloRiferimento(String value) {
        this.chiaveFascicoloRiferimento = value;
    }

    /**
     * Recupera il valore della proprietà tipoFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFascicolo() {
        return tipoFascicolo;
    }

    /**
     * Imposta il valore della proprietà tipoFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFascicolo(String value) {
        this.tipoFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà chiaveDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveDocumento() {
        return chiaveDocumento;
    }

    /**
     * Imposta il valore della proprietà chiaveDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveDocumento(String value) {
        this.chiaveDocumento = value;
    }

    /**
     * Recupera il valore della proprietà identificatoreProcesso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificatoreProcesso() {
        return identificatoreProcesso;
    }

    /**
     * Imposta il valore della proprietà identificatoreProcesso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificatoreProcesso(String value) {
        this.identificatoreProcesso = value;
    }

    /**
     * Recupera il valore della proprietà identificativoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificativoMessaggio() {
        return identificativoMessaggio;
    }

    /**
     * Imposta il valore della proprietà identificativoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificativoMessaggio(String value) {
        this.identificativoMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà contestoProcedurale.
     * 
     * @return
     *     possible object is
     *     {@link ContestoProcedurale }
     *     
     */
    public ContestoProcedurale getContestoProcedurale() {
        return contestoProcedurale;
    }

    /**
     * Imposta il valore della proprietà contestoProcedurale.
     * 
     * @param value
     *     allowed object is
     *     {@link ContestoProcedurale }
     *     
     */
    public void setContestoProcedurale(ContestoProcedurale value) {
        this.contestoProcedurale = value;
    }

    /**
     * Recupera il valore della proprietà protocollo.
     * 
     * @return
     *     possible object is
     *     {@link WkfIdentificatoreProtcolloType }
     *     
     */
    public WkfIdentificatoreProtcolloType getProtocollo() {
        return protocollo;
    }

    /**
     * Imposta il valore della proprietà protocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfIdentificatoreProtcolloType }
     *     
     */
    public void setProtocollo(WkfIdentificatoreProtcolloType value) {
        this.protocollo = value;
    }

    /**
     * Recupera il valore della proprietà codiceErrore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceErrore() {
        return codiceErrore;
    }

    /**
     * Imposta il valore della proprietà codiceErrore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceErrore(String value) {
        this.codiceErrore = value;
    }

    /**
     * Recupera il valore della proprietà messaggioRicevuto.
     * 
     * @return
     *     possible object is
     *     {@link EmailMessageType }
     *     
     */
    public EmailMessageType getMessaggioRicevuto() {
        return messaggioRicevuto;
    }

    /**
     * Imposta il valore della proprietà messaggioRicevuto.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailMessageType }
     *     
     */
    public void setMessaggioRicevuto(EmailMessageType value) {
        this.messaggioRicevuto = value;
    }

}
