
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per wkf_tipoNotificaAzione_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="wkf_tipoNotificaAzione_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CREAZIONE_PROTOCOLLO"/>
 *     &lt;enumeration value="AGGIORNAMENTO_PROTOCOLLO"/>
 *     &lt;enumeration value="ANNULLAMENTO_PROTOCOLLO"/>
 *     &lt;enumeration value="CAMBIO_STATO_ENTRATA"/>
 *     &lt;enumeration value="CAMBIO_STATO_USCITA"/>
 *     &lt;enumeration value="RISPONDI_A"/>
 *     &lt;enumeration value="INSERIMENTO_DOCUMENTI"/>
 *     &lt;enumeration value="RIMOZIONE_DOCUMENTI"/>
 *     &lt;enumeration value="INSERIMENTO_DESTINATARIO"/>
 *     &lt;enumeration value="RIMOZIONE_DESTINATARIO"/>
 *     &lt;enumeration value="SPEDIZIONE_DESTINATARIO_ELETTRONICO"/>
 *     &lt;enumeration value="SPEDIZIONE_DESTINATARIO_CARTACEO"/>
 *     &lt;enumeration value="ASSEGNAZIONE_APPLICAZIONE"/>
 *     &lt;enumeration value="RIMOZIONE_ASSEGNAZIONE_APPLICAZIONE"/>
 *     &lt;enumeration value="CAMBIO_ASSEGNATARIO_ASSEGNAZIONE"/>
 *     &lt;enumeration value="AGGIORNAMENTO_COLLEGATI"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "wkf_tipoNotificaAzione_type")
@XmlEnum
public enum WkfTipoNotificaAzioneType {

    CREAZIONE_PROTOCOLLO,
    AGGIORNAMENTO_PROTOCOLLO,
    ANNULLAMENTO_PROTOCOLLO,
    CAMBIO_STATO_ENTRATA,
    CAMBIO_STATO_USCITA,
    RISPONDI_A,
    INSERIMENTO_DOCUMENTI,
    RIMOZIONE_DOCUMENTI,
    INSERIMENTO_DESTINATARIO,
    RIMOZIONE_DESTINATARIO,
    SPEDIZIONE_DESTINATARIO_ELETTRONICO,
    SPEDIZIONE_DESTINATARIO_CARTACEO,
    ASSEGNAZIONE_APPLICAZIONE,
    RIMOZIONE_ASSEGNAZIONE_APPLICAZIONE,
    CAMBIO_ASSEGNATARIO_ASSEGNAZIONE,
    AGGIORNAMENTO_COLLEGATI;

    public String value() {
        return name();
    }

    public static WkfTipoNotificaAzioneType fromValue(String v) {
        return valueOf(v);
    }

}
