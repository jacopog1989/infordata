
package it.ibm.red.webservice.model.redservice.types.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RispostaDocRed complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RispostaDocRed">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:red:types:v1}RispostaRed">
 *       &lt;sequence>
 *         &lt;element name="documento" type="{urn:red:types:v1}DocumentoRed" minOccurs="0"/>
 *         &lt;element name="idVersione" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RispostaDocRed", propOrder = {
    "documento",
    "idVersione"
})
public class RispostaDocRed extends RispostaRed {

	/**
	 * Documento.
	 */
    @XmlElementRef(name = "documento", type = JAXBElement.class, required = false)
    protected JAXBElement<DocumentoRed> documento;

	/**
	 * Versione.
	 */
    protected Integer idVersione;

    /**
     * Gets the value of the documento property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DocumentoRed }{@code >}
     *     
     */
    public JAXBElement<DocumentoRed> getDocumento() {
        return documento;
    }

    /**
     * Sets the value of the documento property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DocumentoRed }{@code >}
     *     
     */
    public void setDocumento(JAXBElement<DocumentoRed> value) {
        this.documento = value;
    }

    /**
     * Gets the value of the idVersione property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdVersione() {
        return idVersione;
    }

    /**
     * Sets the value of the idVersione property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdVersione(Integer value) {
        this.idVersione = value;
    }

}
