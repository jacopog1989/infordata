package it.ibm.red.webservice.model.dfservice.dto;

public enum ErrorCode {
	
	/**
	 * Valore.
	 */
	NON_IMPLEMENTATO(1, "Il servizio non è ancora stato implementato"),
	
	/**
	 * Valore.
	 */
	ID_CLIENT_NON_PRESENTE(2, "Il client (ID_CLIENT) deve essere specificato nelle credenziali"),
	
	/**
	 * Valore.
	 */
	PWD_CLIENT_NON_PRESENTE(3, "La password del client deve essere specificata nelle credenziali"),
	
	/**
	 * Valore.
	 */
	COD_AOO_NON_PRESENTE(4, "Il parametro cod aoo è obbligatorio"),
	
	/**
	 * Valore.
	 */
	ERRORE_ELABORAZIONE(999, "Errore durante l'elaborazione della richiesta");

	/**
	 * Codce errore..
	 */
	private Integer code;
	
	/**
	 * Descrizione errore..
	 */
	private String description;
	
	ErrorCode(Integer code, String description){
		this.code = code;
		this.description = description;
	}
	
	/**
	 * Recupera il codice.
	 * @return codice
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Recupera la descrizione.
	 * @return descrizione
	 */
	public String getDescription() {
		return description;
	}
}
