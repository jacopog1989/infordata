
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Definisce il tipo di dato relativo al messaggio di posta elettronica
 * 
 * <p>Classe Java per email_message_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="email_message_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodiMessaggio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DataMessaggio" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="TipoMessaggio">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="GENERICO_EMAIL"/>
 *               &lt;enumeration value="GENERICO_FLUSSO"/>
 *               &lt;enumeration value="SEGNATURA"/>
 *               &lt;enumeration value="SEGNATURA_CONFERMA_RICEZIONE"/>
 *               &lt;enumeration value="SEGNATURA_NOTIFICA_ECCEZIONE"/>
 *               &lt;enumeration value="SEGNATURA_AGGIORNAMENTO_CONFERMA"/>
 *               &lt;enumeration value="SEGNATURA_ANNULLAMENTO_PROTOCOLLAZIONE"/>
 *               &lt;enumeration value="PEC_RICEVUTA"/>
 *               &lt;enumeration value="PEC_ACCETTAZIONE"/>
 *               &lt;enumeration value="PEC_NON_ACCETTAZIONE"/>
 *               &lt;enumeration value="PEC_PRESA_IN_CARICO"/>
 *               &lt;enumeration value="PEC_AVVENUTA_CONSEGNA"/>
 *               &lt;enumeration value="PEC_POSTA_CERTIFICATA"/>
 *               &lt;enumeration value="PEC_ERRORE_CONSEGNA"/>
 *               &lt;enumeration value="PEC_PREAVVISO_ERRORE_CONSEGNA"/>
 *               &lt;enumeration value="PEC_ANOMALIA_MESSAGGIO"/>
 *               &lt;enumeration value="PEC_RILEVAZIONE_VIRUS"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OggettoMessaggio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Mittente" type="{http://mef.gov.it.v1.npsTypes}email_indirizzoEmail_type"/>
 *         &lt;element name="Destinatari" type="{http://mef.gov.it.v1.npsTypes}email_indirizzoEmail_type" maxOccurs="unbounded"/>
 *         &lt;element name="CorpoMessaggio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContentMessaggio" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="AllegatiMessaggio" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://mef.gov.it.v1.npsTypes}email_allegatiMessaggio_type">
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CasellaEmail" type="{http://mef.gov.it.v1.npsTypes}org_casellaEmail_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "email_message_type", propOrder = {
    "codiMessaggio",
    "dataMessaggio",
    "tipoMessaggio",
    "oggettoMessaggio",
    "mittente",
    "destinatari",
    "corpoMessaggio",
    "contentMessaggio",
    "allegatiMessaggio",
    "casellaEmail"
})
public class EmailMessageType
    implements Serializable
{

    @XmlElement(name = "CodiMessaggio", required = true)
    protected String codiMessaggio;
    @XmlElement(name = "DataMessaggio", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataMessaggio;
    @XmlElement(name = "TipoMessaggio", required = true)
    protected String tipoMessaggio;
    @XmlElement(name = "OggettoMessaggio", required = true)
    protected String oggettoMessaggio;
    @XmlElement(name = "Mittente", required = true)
    protected EmailIndirizzoEmailType mittente;
    @XmlElement(name = "Destinatari", required = true)
    protected List<EmailIndirizzoEmailType> destinatari;
    @XmlElement(name = "CorpoMessaggio", required = true)
    protected String corpoMessaggio;
    @XmlElement(name = "ContentMessaggio")
    protected byte[] contentMessaggio;
    @XmlElement(name = "AllegatiMessaggio")
    protected List<EmailMessageType.AllegatiMessaggio> allegatiMessaggio;
    @XmlElement(name = "CasellaEmail", required = true)
    protected OrgCasellaEmailType casellaEmail;

    /**
     * Recupera il valore della proprietà codiMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiMessaggio() {
        return codiMessaggio;
    }

    /**
     * Imposta il valore della proprietà codiMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiMessaggio(String value) {
        this.codiMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà dataMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataMessaggio() {
        return dataMessaggio;
    }

    /**
     * Imposta il valore della proprietà dataMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataMessaggio(XMLGregorianCalendar value) {
        this.dataMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà tipoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoMessaggio() {
        return tipoMessaggio;
    }

    /**
     * Imposta il valore della proprietà tipoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoMessaggio(String value) {
        this.tipoMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà oggettoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggettoMessaggio() {
        return oggettoMessaggio;
    }

    /**
     * Imposta il valore della proprietà oggettoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggettoMessaggio(String value) {
        this.oggettoMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà mittente.
     * 
     * @return
     *     possible object is
     *     {@link EmailIndirizzoEmailType }
     *     
     */
    public EmailIndirizzoEmailType getMittente() {
        return mittente;
    }

    /**
     * Imposta il valore della proprietà mittente.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailIndirizzoEmailType }
     *     
     */
    public void setMittente(EmailIndirizzoEmailType value) {
        this.mittente = value;
    }

    /**
     * Gets the value of the destinatari property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the destinatari property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDestinatari().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EmailIndirizzoEmailType }
     * 
     * 
     */
    public List<EmailIndirizzoEmailType> getDestinatari() {
        if (destinatari == null) {
            destinatari = new ArrayList<EmailIndirizzoEmailType>();
        }
        return this.destinatari;
    }

    /**
     * Recupera il valore della proprietà corpoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorpoMessaggio() {
        return corpoMessaggio;
    }

    /**
     * Imposta il valore della proprietà corpoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorpoMessaggio(String value) {
        this.corpoMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà contentMessaggio.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getContentMessaggio() {
        return contentMessaggio;
    }

    /**
     * Imposta il valore della proprietà contentMessaggio.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setContentMessaggio(byte[] value) {
        this.contentMessaggio = value;
    }

    /**
     * Gets the value of the allegatiMessaggio property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allegatiMessaggio property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllegatiMessaggio().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EmailMessageType.AllegatiMessaggio }
     * 
     * 
     */
    public List<EmailMessageType.AllegatiMessaggio> getAllegatiMessaggio() {
        if (allegatiMessaggio == null) {
            allegatiMessaggio = new ArrayList<EmailMessageType.AllegatiMessaggio>();
        }
        return this.allegatiMessaggio;
    }

    /**
     * Recupera il valore della proprietà casellaEmail.
     * 
     * @return
     *     possible object is
     *     {@link OrgCasellaEmailType }
     *     
     */
    public OrgCasellaEmailType getCasellaEmail() {
        return casellaEmail;
    }

    /**
     * Imposta il valore della proprietà casellaEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgCasellaEmailType }
     *     
     */
    public void setCasellaEmail(OrgCasellaEmailType value) {
        this.casellaEmail = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}email_allegatiMessaggio_type">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class AllegatiMessaggio
        extends EmailAllegatiMessaggioType
        implements Serializable
    {


    }

}
