//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.01.28 at 06:39:59 PM CET 
//
package it.ibm.red.webservice.model.documentservice.types.sign;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for TipoCampo.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TipoCampo">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="POSTILLA"/>
 *     &lt;enumeration value="COPIACONFORME"/>
 *     &lt;enumeration value="ID"/>
 *     &lt;enumeration value="PROTOCOLLOENTRATA"/>
 *     &lt;enumeration value="PROTOCOLLOUSCITA"/>
 *     &lt;enumeration value="SIGLA"/>
 *     &lt;enumeration value="FIRMA"/>
 *     &lt;enumeration value="WATERMARK"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "TipoCampo")
@XmlEnum
public enum TipoCampo {

	/**
	 * Valore.
	 */
    POSTILLA,

	/**
	 * Valore.
	 */
    COPIACONFORME,

	/**
	 * Valore.
	 */
    ID,

	/**
	 * Valore.
	 */
    PROTOCOLLOENTRATA,

	/**
	 * Valore.
	 */
    PROTOCOLLOUSCITA,

	/**
	 * Valore.
	 */
    SIGLA,

	/**
	 * Valore.
	 */
    FIRMA,

	/**
	 * Valore.
	 */
    WATERMARK;

	/**
	 * Restituisce il nome dell'enum.
	 * @return name
	 */
    public String value() {
        return name();
    }

    /**
     * Restituisce il valore dell'enum in base alla stringa in input.
     * @param v
     * @return value
     */
    public static TipoCampo fromValue(String v) {
        return valueOf(v);
    }

}
