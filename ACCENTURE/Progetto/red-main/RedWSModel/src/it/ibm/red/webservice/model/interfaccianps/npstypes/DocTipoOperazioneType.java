
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per doc_tipoOperazione_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="doc_tipoOperazione_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CONVERSIONE"/>
 *     &lt;enumeration value="FIRMA"/>
 *     &lt;enumeration value="TIMBRO"/>
 *     &lt;enumeration value="PLACEHOLDER"/>
 *     &lt;enumeration value="COLLEGA_DOCUMENTO"/>
 *     &lt;enumeration value="SOSTITUISCI_DOCUMENTO_COLLEGATO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "doc_tipoOperazione_type")
@XmlEnum
public enum DocTipoOperazioneType {

    CONVERSIONE,
    FIRMA,
    TIMBRO,
    PLACEHOLDER,
    COLLEGA_DOCUMENTO,
    SOSTITUISCI_DOCUMENTO_COLLEGATO;

    public String value() {
        return name();
    }

    public static DocTipoOperazioneType fromValue(String v) {
        return valueOf(v);
    }

}
