/**
 * Header.
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "urn:red:header:v1")
package it.ibm.red.webservice.model.redservice.header.v1;
