//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2019.05.21 alle 01:14:00 PM CEST 
//


package it.ibm.red.webservice.model.redsiebel.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java per aggiornaRdsRequestType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="aggiornaRdsRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sistemaOrigine" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idRdsSiebel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numeroProtocolloRed" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="annoProtocolloRed" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tipoEvento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dataChiusuraRdsSiebel" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="dataEvento" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="soluzione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="allegati" type="{http://red/ibm/RedSiebelService/types/}AllegatiType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "aggiornaRdsRequestType", propOrder = {
    "tid",
    "sistemaOrigine",
    "idRdsSiebel",
    "numeroProtocolloRed",
    "annoProtocolloRed",
    "tipoEvento",
    "dataChiusuraRdsSiebel",
    "dataEvento",
    "soluzione",
    "allegati"
})
public class AggiornaRdsRequestType {

	/**
	 * Identificativo.
	 */
    @XmlElement(required = true)
    protected String tid;

	/**
	 * Sistema origine.
	 */
    @XmlElement(required = true)
    protected String sistemaOrigine;

	/**
	 * Identificativo RDS Siebel.
	 */
    @XmlElement(required = true)
    protected String idRdsSiebel;

	/**
	 * Numero protocollo.
	 */
    protected int numeroProtocolloRed;


	/**
	 * Anno protocollo.
	 */
    protected int annoProtocolloRed;

	/**
	 * Tipo evento.
	 */
    @XmlElement(required = true)
    protected String tipoEvento;

	/**
	 * Data chiusura rds siebel.
	 */
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataChiusuraRdsSiebel;

	/**
	 * Data evento.
	 */
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataEvento;

	/**
	 * Soluzione.
	 */
    @XmlElement(required = true)
    protected String soluzione;
    
	/**
	 * Allegati.
	 */
    protected AllegatiType allegati;

    /**
     * Recupera il valore della propriet� tid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTid() {
        return tid;
    }

    /**
     * Imposta il valore della propriet� tid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTid(String value) {
        this.tid = value;
    }

    /**
     * Recupera il valore della propriet� sistemaOrigine.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaOrigine() {
        return sistemaOrigine;
    }

    /**
     * Imposta il valore della propriet� sistemaOrigine.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaOrigine(String value) {
        this.sistemaOrigine = value;
    }

    /**
     * Recupera il valore della propriet� idRdsSiebel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRdsSiebel() {
        return idRdsSiebel;
    }

    /**
     * Imposta il valore della propriet� idRdsSiebel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRdsSiebel(String value) {
        this.idRdsSiebel = value;
    }

    /**
     * Recupera il valore della propriet� numeroProtocolloRed.
     * 
     */
    public int getNumeroProtocolloRed() {
        return numeroProtocolloRed;
    }

    /**
     * Imposta il valore della propriet� numeroProtocolloRed.
     * 
     */
    public void setNumeroProtocolloRed(int value) {
        this.numeroProtocolloRed = value;
    }

    /**
     * Recupera il valore della propriet� annoProtocolloRed.
     * 
     */
    public int getAnnoProtocolloRed() {
        return annoProtocolloRed;
    }

    /**
     * Imposta il valore della propriet� annoProtocolloRed.
     * 
     */
    public void setAnnoProtocolloRed(int value) {
        this.annoProtocolloRed = value;
    }

    /**
     * Recupera il valore della propriet� tipoEvento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoEvento() {
        return tipoEvento;
    }

    /**
     * Imposta il valore della propriet� tipoEvento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoEvento(String value) {
        this.tipoEvento = value;
    }

    /**
     * Recupera il valore della propriet� dataChiusuraRdsSiebel.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataChiusuraRdsSiebel() {
        return dataChiusuraRdsSiebel;
    }

    /**
     * Imposta il valore della propriet� dataChiusuraRdsSiebel.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataChiusuraRdsSiebel(XMLGregorianCalendar value) {
        this.dataChiusuraRdsSiebel = value;
    }

    /**
     * Recupera il valore della propriet� dataEvento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataEvento() {
        return dataEvento;
    }

    /**
     * Imposta il valore della propriet� dataEvento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataEvento(XMLGregorianCalendar value) {
        this.dataEvento = value;
    }

    /**
     * Recupera il valore della propriet� soluzione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoluzione() {
        return soluzione;
    }

    /**
     * Imposta il valore della propriet� soluzione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoluzione(String value) {
        this.soluzione = value;
    }

    /**
     * Recupera il valore della propriet� allegati.
     * 
     * @return
     *     possible object is
     *     {@link AllegatiType }
     *     
     */
    public AllegatiType getAllegati() {
        return allegati;
    }

    /**
     * Imposta il valore della propriet� allegati.
     * 
     * @param value
     *     allowed object is
     *     {@link AllegatiType }
     *     
     */
    public void setAllegati(AllegatiType value) {
        this.allegati = value;
    }

}
