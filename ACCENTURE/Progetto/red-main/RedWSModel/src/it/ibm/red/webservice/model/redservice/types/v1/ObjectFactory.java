
package it.ibm.red.webservice.model.redservice.types.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the red.types.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


	/**
	 * Qname.
	 */
	private static final QName _MetadatoRedValue_QNAME = new QName("", "value");

	/**
	 * Qname.
	 */
	private static final QName _MetadatoRedNomeChoice_QNAME = new QName("", "nomeChoice");

	/**
	 * Qname.
	 */
	private static final QName _MetadatoRedOperazione_QNAME = new QName("", "operazione");

	/**
	 * Qname.
	 */
	private static final QName _MetadatoRedTipoMetadato_QNAME = new QName("", "tipoMetadato");

	/**
	 * Qname.
	 */
	private static final QName _MetadatoRedDisplayName_QNAME = new QName("", "displayName");

	/**
	 * Qname.
	 */
	private static final QName _MetadatoRedKey_QNAME = new QName("", "key");

	/**
	 * Qname.
	 */
	private static final QName _SecurityRedGruppo_QNAME = new QName("", "gruppo");

	/**
	 * Qname.
	 */
	private static final QName _SecurityRedUtente_QNAME = new QName("", "utente");

	/**
	 * Qname.
	 */
	private static final QName _MotivazioneAnnullamentoRedMotivazione_QNAME = new QName("", "motivazione");

	/**
	 * Qname.
	 */
	private static final QName _FascicoloRedIdNuovoFascicolo_QNAME = new QName("", "idNuovoFascicolo");

	/**
	 * Qname.
	 */
	private static final QName _FascicoloRedIdFascicolo_QNAME = new QName("", "idFascicolo");

	/**
	 * Qname.
	 */
	private static final QName _FascicoloRedClasseDocumentale_QNAME = new QName("", "classeDocumentale");

	/**
	 * Qname.
	 */
	private static final QName _FascicoloRedTitolario_QNAME = new QName("", "titolario");

	/**
	 * Qname.
	 */
	private static final QName _RispostaDocRedDocumento_QNAME = new QName("", "documento");

	/**
	 * Qname.
	 */
	private static final QName _RispostaVersioneDocRedVersioneDocumento_QNAME = new QName("", "versioneDocumento");

	/**
	 * Qname.
	 */
	private static final QName _ClasseDocumentaleRedParent_QNAME = new QName("", "parent");

	/**
	 * Qname.
	 */
	private static final QName _ClasseDocumentaleRedNomeClasse_QNAME = new QName("", "nomeClasse");

	/**
	 * Qname.
	 */
	private static final QName _UtenteRedUsername_QNAME = new QName("", "username");

	/**
	 * Qname.
	 */
	private static final QName _UtenteRedEmail_QNAME = new QName("", "email");

	/**
	 * Qname.
	 */
	private static final QName _UtenteRedCodfis_QNAME = new QName("", "codfis");

	/**
	 * Qname.
	 */
	private static final QName _UtenteRedDataDisattivazione_QNAME = new QName("", "dataDisattivazione");

	/**
	 * Qname.
	 */
	private static final QName _UtenteRedNome_QNAME = new QName("", "nome");

	/**
	 * Qname.
	 */
	private static final QName _UtenteRedCognome_QNAME = new QName("", "cognome");

	/**
	 * Qname.
	 */
	private static final QName _GruppoRedDescrizione_QNAME = new QName("", "descrizione");

	/**
	 * Qname.
	 */
	private static final QName _RispostaRedGuid_QNAME = new QName("", "guid");

	/**
	 * Qname.
	 */
	private static final QName _RispostaRedDescErrore_QNAME = new QName("", "descErrore");

	/**
	 * Qname.
	 */
	private static final QName _DocumentoRedFolder_QNAME = new QName("", "folder");

	/**
	 * Qname.
	 */
	private static final QName _DocumentoRedDataModifica_QNAME = new QName("", "dataModifica");

	/**
	 * Qname.
	 */
	private static final QName _DocumentoRedDataHandler_QNAME = new QName("", "dataHandler");

	/**
	 * Qname.
	 */
	private static final QName _DocumentoRedContentType_QNAME = new QName("", "contentType");

	/**
	 * Qname.
	 */
	private static final QName _DocumentoRedIdDocumento_QNAME = new QName("", "idDocumento");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: red.types.v1
     */
    public ObjectFactory() {
    	// Costruttore di default.
    }

    /**
     * Create an instance of {@link MapRed }
     */
    public MapRed createMapRed() {
        return new MapRed();
    }

    /**
     * Create an instance of {@link DocumentoRed }
     */
    public DocumentoRed createDocumentoRed() {
        return new DocumentoRed();
    }

    /**
     * Create an instance of {@link RispostaVersioneDocRed }
     */
    public RispostaVersioneDocRed createRispostaVersioneDocRed() {
        return new RispostaVersioneDocRed();
    }

    /**
     * Create an instance of {@link FascicoloRed }
     */
    public FascicoloRed createFascicoloRed() {
        return new FascicoloRed();
    }

    /**
     * Create an instance of {@link RispostaElencoVersioniDocRed }
     */
    public RispostaElencoVersioniDocRed createRispostaElencoVersioniDocRed() {
        return new RispostaElencoVersioniDocRed();
    }

    /**
     * Create an instance of {@link IdentificativoUnitaDocumentaleFEPATypeRed }
     */
    public IdentificativoUnitaDocumentaleFEPATypeRed createIdentificativoUnitaDocumentaleFEPATypeRed() {
        return new IdentificativoUnitaDocumentaleFEPATypeRed();
    }

    /**
     * Create an instance of {@link RispostaRed }
     */
    public RispostaRed createRispostaRed() {
        return new RispostaRed();
    }

    /**
     * Create an instance of {@link VersioneDocumentoInputRed }
     */
    public VersioneDocumentoInputRed createVersioneDocumentoInputRed() {
        return new VersioneDocumentoInputRed();
    }

    /**
     * Create an instance of {@link RispostaDocRed }
     */
    public RispostaDocRed createRispostaDocRed() {
        return new RispostaDocRed();
    }

    /**
     * Create an instance of {@link MotivazioneAnnullamentoRed }
     */
    public MotivazioneAnnullamentoRed createMotivazioneAnnullamentoRed() {
        return new MotivazioneAnnullamentoRed();
    }

    /**
     * Create an instance of {@link Test }
     * 
     */
    public Test createTest() {
        return new Test();
    }

    /**
     * Create an instance of {@link OperazioneRed }
     * 
     */
    public OperazioneRed createOperazioneRed() {
        return new OperazioneRed();
    }

    /**
     * Create an instance of {@link TipoMetadatoRed }
     * 
     */
    public TipoMetadatoRed createTipoMetadatoRed() {
        return new TipoMetadatoRed();
    }

    /**
     * Create an instance of {@link MetadatoRed }
     * 
     */
    public MetadatoRed createMetadatoRed() {
        return new MetadatoRed();
    }

    /**
     * Create an instance of {@link SecurityRed }
     * 
     */
    public SecurityRed createSecurityRed() {
        return new SecurityRed();
    }

    /**
     * Create an instance of {@link UtenteRed }
     * 
     */
    public UtenteRed createUtenteRed() {
        return new UtenteRed();
    }

    /**
     * Create an instance of {@link ClasseDocumentaleRed }
     * 
     */
    public ClasseDocumentaleRed createClasseDocumentaleRed() {
        return new ClasseDocumentaleRed();
    }

    /**
     * Create an instance of {@link GruppoRed }
     * 
     */
    public GruppoRed createGruppoRed() {
        return new GruppoRed();
    }

    /**
     * Create an instance of {@link IdentificativoUnitaDocumentaleTypeRed }
     * 
     */
    public IdentificativoUnitaDocumentaleTypeRed createIdentificativoUnitaDocumentaleTypeRed() {
        return new IdentificativoUnitaDocumentaleTypeRed();
    }

    /**
     * Create an instance of {@link MapRed.Elemento }
     * 
     */
    public MapRed.Elemento createMapRedElemento() {
        return new MapRed.Elemento();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "value", scope = MetadatoRed.class)
    public JAXBElement<String> createMetadatoRedValue(String value) {
        return new JAXBElement<>(_MetadatoRedValue_QNAME, String.class, MetadatoRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nomeChoice", scope = MetadatoRed.class)
    public JAXBElement<String> createMetadatoRedNomeChoice(String value) {
        return new JAXBElement<>(_MetadatoRedNomeChoice_QNAME, String.class, MetadatoRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OperazioneRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "operazione", scope = MetadatoRed.class)
    public JAXBElement<OperazioneRed> createMetadatoRedOperazione(OperazioneRed value) {
        return new JAXBElement<>(_MetadatoRedOperazione_QNAME, OperazioneRed.class, MetadatoRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMetadatoRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipoMetadato", scope = MetadatoRed.class)
    public JAXBElement<TipoMetadatoRed> createMetadatoRedTipoMetadato(TipoMetadatoRed value) {
        return new JAXBElement<>(_MetadatoRedTipoMetadato_QNAME, TipoMetadatoRed.class, MetadatoRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "displayName", scope = MetadatoRed.class)
    public JAXBElement<String> createMetadatoRedDisplayName(String value) {
        return new JAXBElement<>(_MetadatoRedDisplayName_QNAME, String.class, MetadatoRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "key", scope = MetadatoRed.class)
    public JAXBElement<String> createMetadatoRedKey(String value) {
        return new JAXBElement<>(_MetadatoRedKey_QNAME, String.class, MetadatoRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GruppoRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "gruppo", scope = SecurityRed.class)
    public JAXBElement<GruppoRed> createSecurityRedGruppo(GruppoRed value) {
        return new JAXBElement<>(_SecurityRedGruppo_QNAME, GruppoRed.class, SecurityRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UtenteRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "utente", scope = SecurityRed.class)
    public JAXBElement<UtenteRed> createSecurityRedUtente(UtenteRed value) {
        return new JAXBElement<>(_SecurityRedUtente_QNAME, UtenteRed.class, SecurityRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "motivazione", scope = MotivazioneAnnullamentoRed.class)
    public JAXBElement<String> createMotivazioneAnnullamentoRedMotivazione(String value) {
        return new JAXBElement<>(_MotivazioneAnnullamentoRedMotivazione_QNAME, String.class, MotivazioneAnnullamentoRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "idNuovoFascicolo", scope = FascicoloRed.class)
    public JAXBElement<String> createFascicoloRedIdNuovoFascicolo(String value) {
        return new JAXBElement<>(_FascicoloRedIdNuovoFascicolo_QNAME, String.class, FascicoloRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "idFascicolo", scope = FascicoloRed.class)
    public JAXBElement<String> createFascicoloRedIdFascicolo(String value) {
        return new JAXBElement<>(_FascicoloRedIdFascicolo_QNAME, String.class, FascicoloRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClasseDocumentaleRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "classeDocumentale", scope = FascicoloRed.class)
    public JAXBElement<ClasseDocumentaleRed> createFascicoloRedClasseDocumentale(ClasseDocumentaleRed value) {
        return new JAXBElement<>(_FascicoloRedClasseDocumentale_QNAME, ClasseDocumentaleRed.class, FascicoloRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "titolario", scope = FascicoloRed.class)
    public JAXBElement<String> createFascicoloRedTitolario(String value) {
        return new JAXBElement<>(_FascicoloRedTitolario_QNAME, String.class, FascicoloRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "documento", scope = RispostaDocRed.class)
    public JAXBElement<DocumentoRed> createRispostaDocRedDocumento(DocumentoRed value) {
        return new JAXBElement<>(_RispostaDocRedDocumento_QNAME, DocumentoRed.class, RispostaDocRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "versioneDocumento", scope = RispostaVersioneDocRed.class)
    public JAXBElement<DocumentoRed> createRispostaVersioneDocRedVersioneDocumento(DocumentoRed value) {
        return new JAXBElement<>(_RispostaVersioneDocRedVersioneDocumento_QNAME, DocumentoRed.class, RispostaVersioneDocRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "parent", scope = ClasseDocumentaleRed.class)
    public JAXBElement<String> createClasseDocumentaleRedParent(String value) {
        return new JAXBElement<>(_ClasseDocumentaleRedParent_QNAME, String.class, ClasseDocumentaleRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nomeClasse", scope = ClasseDocumentaleRed.class)
    public JAXBElement<String> createClasseDocumentaleRedNomeClasse(String value) {
        return new JAXBElement<>(_ClasseDocumentaleRedNomeClasse_QNAME, String.class, ClasseDocumentaleRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "username", scope = UtenteRed.class)
    public JAXBElement<String> createUtenteRedUsername(String value) {
        return new JAXBElement<>(_UtenteRedUsername_QNAME, String.class, UtenteRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "email", scope = UtenteRed.class)
    public JAXBElement<String> createUtenteRedEmail(String value) {
        return new JAXBElement<>(_UtenteRedEmail_QNAME, String.class, UtenteRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codfis", scope = UtenteRed.class)
    public JAXBElement<String> createUtenteRedCodfis(String value) {
        return new JAXBElement<>(_UtenteRedCodfis_QNAME, String.class, UtenteRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "dataDisattivazione", scope = UtenteRed.class)
    public JAXBElement<XMLGregorianCalendar> createUtenteRedDataDisattivazione(XMLGregorianCalendar value) {
        return new JAXBElement<>(_UtenteRedDataDisattivazione_QNAME, XMLGregorianCalendar.class, UtenteRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nome", scope = UtenteRed.class)
    public JAXBElement<String> createUtenteRedNome(String value) {
        return new JAXBElement<>(_UtenteRedNome_QNAME, String.class, UtenteRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cognome", scope = UtenteRed.class)
    public JAXBElement<String> createUtenteRedCognome(String value) {
        return new JAXBElement<>(_UtenteRedCognome_QNAME, String.class, UtenteRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "descrizione", scope = GruppoRed.class)
    public JAXBElement<String> createGruppoRedDescrizione(String value) {
        return new JAXBElement<>(_GruppoRedDescrizione_QNAME, String.class, GruppoRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OperazioneRed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "operazione", scope = GruppoRed.class)
    public JAXBElement<OperazioneRed> createGruppoRedOperazione(OperazioneRed value) {
        return new JAXBElement<>(_MetadatoRedOperazione_QNAME, OperazioneRed.class, GruppoRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "guid", scope = RispostaRed.class)
    public JAXBElement<String> createRispostaRedGuid(String value) {
        return new JAXBElement<>(_RispostaRedGuid_QNAME, String.class, RispostaRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "descErrore", scope = RispostaRed.class)
    public JAXBElement<String> createRispostaRedDescErrore(String value) {
        return new JAXBElement<>(_RispostaRedDescErrore_QNAME, String.class, RispostaRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "guid", scope = DocumentoRed.class)
    public JAXBElement<String> createDocumentoRedGuid(String value) {
        return new JAXBElement<>(_RispostaRedGuid_QNAME, String.class, DocumentoRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "folder", scope = DocumentoRed.class)
    public JAXBElement<String> createDocumentoRedFolder(String value) {
        return new JAXBElement<>(_DocumentoRedFolder_QNAME, String.class, DocumentoRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "idFascicolo", scope = DocumentoRed.class)
    public JAXBElement<String> createDocumentoRedIdFascicolo(String value) {
        return new JAXBElement<>(_FascicoloRedIdFascicolo_QNAME, String.class, DocumentoRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "dataModifica", scope = DocumentoRed.class)
    public JAXBElement<String> createDocumentoRedDataModifica(String value) {
        return new JAXBElement<>(_DocumentoRedDataModifica_QNAME, String.class, DocumentoRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "dataHandler", scope = DocumentoRed.class)
    public JAXBElement<byte[]> createDocumentoRedDataHandler(byte[] value) {
        return new JAXBElement<>(_DocumentoRedDataHandler_QNAME, byte[].class, DocumentoRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "contentType", scope = DocumentoRed.class)
    public JAXBElement<String> createDocumentoRedContentType(String value) {
        return new JAXBElement<>(_DocumentoRedContentType_QNAME, String.class, DocumentoRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "idDocumento", scope = DocumentoRed.class)
    public JAXBElement<String> createDocumentoRedIdDocumento(String value) {
        return new JAXBElement<>(_DocumentoRedIdDocumento_QNAME, String.class, DocumentoRed.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClasseDocumentaleRed }{@code >}}
     */
    @XmlElementDecl(namespace = "", name = "classeDocumentale", scope = DocumentoRed.class)
    public JAXBElement<ClasseDocumentaleRed> createDocumentoRedClasseDocumentale(ClasseDocumentaleRed value) {
        return new JAXBElement<>(_FascicoloRedClasseDocumentale_QNAME, ClasseDocumentaleRed.class, DocumentoRed.class, value);
    }
}
