
package it.ibm.red.webservice.model.interfaccianps.npsmessages;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import it.ibm.red.webservice.model.interfaccianps.npstypes.WkfDatiFlussoInteroperabilitaType;


/**
 * Tipo di dato per richiedere la notifica interoperabile ricevuta
 * 
 * <p>Classe Java per richiesta_elaboraNotificaInteroperabilita_Protocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_elaboraNotificaInteroperabilita_Protocollo_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}wkf_datiFlussoInteroperabilita_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_elaboraNotificaInteroperabilita_Protocollo_type")
public class RichiestaElaboraNotificaInteroperabilitaProtocolloType
    extends WkfDatiFlussoInteroperabilitaType
    implements Serializable
{


}
