
package it.ibm.red.webservice.model.igepa.psi.types.xsd;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per AcquisizioneDCPResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="AcquisizioneDCPResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="acquisizioneDCPResponse" type="{http://types.ws.filenet.nsd.capgemini.com/xsd}BaseResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AcquisizioneDCPResponse", propOrder = {
    "acquisizioneDCPResponse"
})
public class AcquisizioneDCPResponse
    implements Serializable
{

    @XmlElementRef(name = "acquisizioneDCPResponse", namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<BaseResponse> acquisizioneDCPResponse;

    /**
     * Recupera il valore della proprietà acquisizioneDCPResponse.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BaseResponse }{@code >}
     *     
     */
    public JAXBElement<BaseResponse> getAcquisizioneDCPResponse() {
        return acquisizioneDCPResponse;
    }

    /**
     * Imposta il valore della proprietà acquisizioneDCPResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BaseResponse }{@code >}
     *     
     */
    public void setAcquisizioneDCPResponse(JAXBElement<BaseResponse> value) {
        this.acquisizioneDCPResponse = value;
    }

}
