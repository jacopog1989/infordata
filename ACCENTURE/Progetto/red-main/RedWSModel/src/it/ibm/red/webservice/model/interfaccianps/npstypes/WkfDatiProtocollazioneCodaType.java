
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import it.ibm.red.webservice.model.interfaccianps.npstypesestesi.TIPOLOGIA;


/**
 * Dati del flusso
 * 
 * <p>Classe Java per wkf_datiProtocollazioneCoda_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_datiProtocollazioneCoda_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentificativoMessaggio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Aoo" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_aoo_type"/>
 *         &lt;element name="DatiEstesi" type="{http://mef.gov.it.v1.npsTypesEstesi/}TIPOLOGIA"/>
 *         &lt;element name="SistemaAusiliario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_datiProtocollazioneCoda_type", propOrder = {
    "identificativoMessaggio",
    "aoo",
    "datiEstesi",
    "sistemaAusiliario"
})
public class WkfDatiProtocollazioneCodaType
    implements Serializable
{

    @XmlElement(name = "IdentificativoMessaggio", required = true)
    protected String identificativoMessaggio;
    @XmlElement(name = "Aoo", required = true)
    protected OrgOrganigrammaAooType aoo;
    @XmlElement(name = "DatiEstesi", required = true)
    protected TIPOLOGIA datiEstesi;
    @XmlElement(name = "SistemaAusiliario")
    protected String sistemaAusiliario;

    /**
     * Recupera il valore della proprietà identificativoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificativoMessaggio() {
        return identificativoMessaggio;
    }

    /**
     * Imposta il valore della proprietà identificativoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificativoMessaggio(String value) {
        this.identificativoMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà aoo.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaAooType }
     *     
     */
    public OrgOrganigrammaAooType getAoo() {
        return aoo;
    }

    /**
     * Imposta il valore della proprietà aoo.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaAooType }
     *     
     */
    public void setAoo(OrgOrganigrammaAooType value) {
        this.aoo = value;
    }

    /**
     * Recupera il valore della proprietà datiEstesi.
     * 
     * @return
     *     possible object is
     *     {@link TIPOLOGIA }
     *     
     */
    public TIPOLOGIA getDatiEstesi() {
        return datiEstesi;
    }

    /**
     * Imposta il valore della proprietà datiEstesi.
     * 
     * @param value
     *     allowed object is
     *     {@link TIPOLOGIA }
     *     
     */
    public void setDatiEstesi(TIPOLOGIA value) {
        this.datiEstesi = value;
    }

    /**
     * Recupera il valore della proprietà sistemaAusiliario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaAusiliario() {
        return sistemaAusiliario;
    }

    /**
     * Imposta il valore della proprietà sistemaAusiliario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaAusiliario(String value) {
        this.sistemaAusiliario = value;
    }

}
