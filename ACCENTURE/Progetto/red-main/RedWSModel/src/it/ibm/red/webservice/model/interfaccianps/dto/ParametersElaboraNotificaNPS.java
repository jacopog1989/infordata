package it.ibm.red.webservice.model.interfaccianps.dto;

public class ParametersElaboraNotificaNPS {

	/**
	 * Message id.
	 */
	private String messageIdNPS;
	
	/**
	 * Costruttore.
	 * @param messageIdNPS
	 */
	public ParametersElaboraNotificaNPS(String messageIdNPS) {
		super();
		this.messageIdNPS = messageIdNPS;
	}

	/**
	 * Restituisce il message id NPS.
	 * @return message id NPS
	 */
	public String getMessageIdNPS() {
		return messageIdNPS;
	}

}
