
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Dati per la notifica di inserimento di un destinatario di un protocollo
 * 
 * <p>Classe Java per wkf_azioneInserimentoDestinatarioProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_azioneInserimentoDestinatarioProtocollo_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}prot_destinatario_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_azioneInserimentoDestinatarioProtocollo_type")
public class WkfAzioneInserimentoDestinatarioProtocolloType
    extends ProtDestinatarioType
    implements Serializable
{


}
