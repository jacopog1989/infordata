
package it.ibm.red.webservice.model.igepa.types.xsd;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per Base64Binary complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="Base64Binary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="base64Binary" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="contentType" type="{http://ws.filenet.nsd.capgemini.com/xsd}ContentType_type0" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Base64Binary", propOrder = {
    "base64Binary",
    "contentType"
})
public class Base64Binary
    implements Serializable
{

    @XmlElementRef(name = "base64Binary", namespace = "http://ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<byte[]> base64Binary;
    @XmlElementRef(name = "contentType", namespace = "http://ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<ContentTypeType0> contentType;

    /**
     * Recupera il valore della proprietà base64Binary.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public JAXBElement<byte[]> getBase64Binary() {
        return base64Binary;
    }

    /**
     * Imposta il valore della proprietà base64Binary.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public void setBase64Binary(JAXBElement<byte[]> value) {
        this.base64Binary = value;
    }

    /**
     * Recupera il valore della proprietà contentType.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ContentTypeType0 }{@code >}
     *     
     */
    public JAXBElement<ContentTypeType0> getContentType() {
        return contentType;
    }

    /**
     * Imposta il valore della proprietà contentType.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ContentTypeType0 }{@code >}
     *     
     */
    public void setContentType(JAXBElement<ContentTypeType0> value) {
        this.contentType = value;
    }

}
