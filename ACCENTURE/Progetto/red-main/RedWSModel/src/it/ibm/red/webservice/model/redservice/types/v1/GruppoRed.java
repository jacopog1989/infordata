
package it.ibm.red.webservice.model.redservice.types.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GruppoRed complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GruppoRed">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="descrizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idSiap" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idUfficio" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idUfficioPadre" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="operazione" type="{urn:red:types:v1}OperazioneRed" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GruppoRed", propOrder = {
    "descrizione",
    "idSiap",
    "idUfficio",
    "idUfficioPadre",
    "operazione"
})
public class GruppoRed {


    /**
     * Descrizione.
     */
    @XmlElementRef(name = "descrizione", type = JAXBElement.class, required = false)
    protected JAXBElement<String> descrizione;

    /**
     * SIAP.
     */
    protected Integer idSiap;

    /**
     * Id ufficio.
     */
    protected Integer idUfficio;

    /**
     * Id ufficio padre.
     */
    protected Integer idUfficioPadre;

    /**
     * Operazione.
     */
    @XmlElementRef(name = "operazione", type = JAXBElement.class, required = false)
    protected JAXBElement<OperazioneRed> operazione;

    /**
     * Gets the value of the descrizione property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public JAXBElement<String> getDescrizione() {
        return descrizione;
    }

    /**
     * Sets the value of the descrizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public void setDescrizione(JAXBElement<String> value) {
        this.descrizione = value;
    }

    /**
     * Gets the value of the idSiap property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     */
    public Integer getIdSiap() {
        return idSiap;
    }

    /**
     * Sets the value of the idSiap property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdSiap(Integer value) {
        this.idSiap = value;
    }

    /**
     * Gets the value of the idUfficio property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdUfficio() {
        return idUfficio;
    }

    /**
     * Sets the value of the idUfficio property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdUfficio(Integer value) {
        this.idUfficio = value;
    }

    /**
     * Gets the value of the idUfficioPadre property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdUfficioPadre() {
        return idUfficioPadre;
    }

    /**
     * Sets the value of the idUfficioPadre property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     */
    public void setIdUfficioPadre(Integer value) {
        this.idUfficioPadre = value;
    }

    /**
     * Gets the value of the operazione property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OperazioneRed }{@code >}
     */
    public JAXBElement<OperazioneRed> getOperazione() {
        return operazione;
    }

    /**
     * Sets the value of the operazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OperazioneRed }{@code >}
     *     
     */
    public void setOperazione(JAXBElement<OperazioneRed> value) {
        this.operazione = value;
    }
}
