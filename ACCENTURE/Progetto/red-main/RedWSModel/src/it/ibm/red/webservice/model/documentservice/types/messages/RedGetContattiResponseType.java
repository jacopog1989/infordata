//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.01.28 at 06:39:59 PM CET 
//


package it.ibm.red.webservice.model.documentservice.types.messages;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import it.ibm.red.webservice.model.documentservice.types.rubrica.Contatto;


/**
 * <p>Java class for redGetContattiResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="redGetContattiResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://messages.types.documentservice.model.webservice.red.ibm.it}BaseResponseType">
 *       &lt;sequence>
 *         &lt;element name="contatti" type="{http://rubrica.types.documentservice.model.webservice.red.ibm.it}Contatto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "redGetContattiResponseType", propOrder = {
    "contatti",
    "message"
})
public class RedGetContattiResponseType extends BaseResponseType {

    /**
     * Contatti.
     */
    protected List<Contatto> contatti;

    /**
     * Messaggio.
     */
    protected String message;

    /**
     * Gets the value of the contatti property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contatti property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContatti().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Contatto }
     * 
     * 
     */
    public List<Contatto> getContatti() {
        if (contatti == null) {
            contatti = new ArrayList<>();
        }
        return this.contatti;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

}
