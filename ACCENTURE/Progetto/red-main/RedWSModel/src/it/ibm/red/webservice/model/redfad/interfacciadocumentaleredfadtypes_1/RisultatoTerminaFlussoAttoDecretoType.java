
package it.ibm.red.webservice.model.redfad.interfacciadocumentaleredfadtypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for risultatoTerminaFlussoAttoDecretoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risultatoTerminaFlussoAttoDecretoType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.red/servizi/InterfacciaDocumentaleREDFADTypes_1}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloRaccoltaProvvisoria" type="{http://mef.gov.it.v1.red/servizi/InterfacciaDocumentaleREDFADTypes_1}Guid"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risultatoTerminaFlussoAttoDecretoType", propOrder = {
    "idFascicoloRaccoltaProvvisoria"
})
public class RisultatoTerminaFlussoAttoDecretoType extends BaseServiceResponseType {

    /**
     * Fascicolo raccolta provvisoria.
     */
    @XmlElement(name = "IdFascicoloRaccoltaProvvisoria", required = true)
    protected String idFascicoloRaccoltaProvvisoria;

    /**
     * Gets the value of the idFascicoloRaccoltaProvvisoria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloRaccoltaProvvisoria() {
        return idFascicoloRaccoltaProvvisoria;
    }

    /**
     * Sets the value of the idFascicoloRaccoltaProvvisoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloRaccoltaProvvisoria(String value) {
        this.idFascicoloRaccoltaProvvisoria = value;
    }

}
