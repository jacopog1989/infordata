
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Dati per la notifica di cambio stato di un protocollo
 * 
 * <p>Classe Java per wkf_azioneCambioStatoProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_azioneCambioStatoProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Entrata">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Stato">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="DA_ASSEGNARE"/>
 *                         &lt;enumeration value="IN_LAVORAZIONE"/>
 *                         &lt;enumeration value="AGLI_ATTI"/>
 *                         &lt;enumeration value="CHIUSO"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="DataOperazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Uscita">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Stato">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="DA_SPEDIRE"/>
 *                         &lt;enumeration value="INVIATO"/>
 *                         &lt;enumeration value="SPEDITO"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="DataOperazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_azioneCambioStatoProtocollo_type", propOrder = {
    "entrata",
    "uscita"
})
public class WkfAzioneCambioStatoProtocolloType
    implements Serializable
{

    @XmlElement(name = "Entrata")
    protected WkfAzioneCambioStatoProtocolloType.Entrata entrata;
    @XmlElement(name = "Uscita")
    protected WkfAzioneCambioStatoProtocolloType.Uscita uscita;

    /**
     * Recupera il valore della proprietà entrata.
     * 
     * @return
     *     possible object is
     *     {@link WkfAzioneCambioStatoProtocolloType.Entrata }
     *     
     */
    public WkfAzioneCambioStatoProtocolloType.Entrata getEntrata() {
        return entrata;
    }

    /**
     * Imposta il valore della proprietà entrata.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfAzioneCambioStatoProtocolloType.Entrata }
     *     
     */
    public void setEntrata(WkfAzioneCambioStatoProtocolloType.Entrata value) {
        this.entrata = value;
    }

    /**
     * Recupera il valore della proprietà uscita.
     * 
     * @return
     *     possible object is
     *     {@link WkfAzioneCambioStatoProtocolloType.Uscita }
     *     
     */
    public WkfAzioneCambioStatoProtocolloType.Uscita getUscita() {
        return uscita;
    }

    /**
     * Imposta il valore della proprietà uscita.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfAzioneCambioStatoProtocolloType.Uscita }
     *     
     */
    public void setUscita(WkfAzioneCambioStatoProtocolloType.Uscita value) {
        this.uscita = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Stato">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="DA_ASSEGNARE"/>
     *               &lt;enumeration value="IN_LAVORAZIONE"/>
     *               &lt;enumeration value="AGLI_ATTI"/>
     *               &lt;enumeration value="CHIUSO"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="DataOperazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "stato",
        "dataOperazione"
    })
    public static class Entrata
        implements Serializable
    {

        @XmlElement(name = "Stato", required = true)
        protected String stato;
        @XmlElement(name = "DataOperazione", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dataOperazione;

        /**
         * Recupera il valore della proprietà stato.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStato() {
            return stato;
        }

        /**
         * Imposta il valore della proprietà stato.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStato(String value) {
            this.stato = value;
        }

        /**
         * Recupera il valore della proprietà dataOperazione.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDataOperazione() {
            return dataOperazione;
        }

        /**
         * Imposta il valore della proprietà dataOperazione.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDataOperazione(XMLGregorianCalendar value) {
            this.dataOperazione = value;
        }

    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Stato">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="DA_SPEDIRE"/>
     *               &lt;enumeration value="INVIATO"/>
     *               &lt;enumeration value="SPEDITO"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="DataOperazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "stato",
        "dataOperazione"
    })
    public static class Uscita
        implements Serializable
    {

        @XmlElement(name = "Stato", required = true)
        protected String stato;
        @XmlElement(name = "DataOperazione", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dataOperazione;

        /**
         * Recupera il valore della proprietà stato.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStato() {
            return stato;
        }

        /**
         * Imposta il valore della proprietà stato.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStato(String value) {
            this.stato = value;
        }

        /**
         * Recupera il valore della proprietà dataOperazione.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDataOperazione() {
            return dataOperazione;
        }

        /**
         * Imposta il valore della proprietà dataOperazione.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDataOperazione(XMLGregorianCalendar value) {
            this.dataOperazione = value;
        }

    }

}
