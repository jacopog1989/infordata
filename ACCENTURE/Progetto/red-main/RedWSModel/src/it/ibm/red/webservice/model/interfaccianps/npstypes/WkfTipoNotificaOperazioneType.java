
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per wkf_tipoNotificaOperazione_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="wkf_tipoNotificaOperazione_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="COMPLETAMENTO"/>
 *     &lt;enumeration value="OPERAZIONE"/>
 *     &lt;enumeration value="SPEDIZIONE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "wkf_tipoNotificaOperazione_type")
@XmlEnum
public enum WkfTipoNotificaOperazioneType {

    COMPLETAMENTO,
    OPERAZIONE,
    SPEDIZIONE;

    public String value() {
        return name();
    }

    public static WkfTipoNotificaOperazioneType fromValue(String v) {
        return valueOf(v);
    }

}
