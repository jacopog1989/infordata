
package it.ibm.red.webservice.model.interfaccianps.npsmessages;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce il tipo base per le risposte dei servizi
 * 
 * <p>Classe Java per baseServiceResponse_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="baseServiceResponse_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="messageId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Esito" type="{http://mef.gov.it.v1.npsMessages}esito_type"/>
 *         &lt;element name="ErrorList" type="{http://mef.gov.it.v1.npsMessages}serviceError_type" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "baseServiceResponse_type", propOrder = {
    "messageId",
    "esito",
    "errorList"
})
@XmlSeeAlso({
    RispostaElaboraErroreProtocollazioneFlussoType.class,
    RispostaElaboraComunicazioneGenericaFlussoType.class,
    RispostaElaboraMessaggioProtocollazioneFlussoType.class,
    RispostaElaboraNotificaAzioneType.class,
    RispostaElaboraMessaggioFlussoType.class,
    RispostaElaboraNotificaPECType.class,
    RispostaElaboraErroreProtocollazioneAutomaticaType.class,
    RispostaElaboraMessaggioProtocollazioneAutomaticaType.class,
    RispostaElaboraNotificaOperazioneType.class,
    RispostaElaboraMessaggioProtocollazioneEmergenzaType.class,
    RispostaElaboraErroreFlussoType.class,
    RispostaElaboraMessaggioPostaType.class,
    RispostaElaboraNotificaInteroperabilitaProtocolloType.class,
    RispostaValidazioneStatoFlussoType.class
})
public abstract class BaseServiceResponseType
    implements Serializable
{

    @XmlElement(required = true)
    protected String messageId;
    @XmlElement(name = "Esito", required = true)
    @XmlSchemaType(name = "string")
    protected EsitoType esito;
    @XmlElement(name = "ErrorList")
    protected List<ServiceErrorType> errorList;

    /**
     * Recupera il valore della proprietà messageId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * Imposta il valore della proprietà messageId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageId(String value) {
        this.messageId = value;
    }

    /**
     * Recupera il valore della proprietà esito.
     * 
     * @return
     *     possible object is
     *     {@link EsitoType }
     *     
     */
    public EsitoType getEsito() {
        return esito;
    }

    /**
     * Imposta il valore della proprietà esito.
     * 
     * @param value
     *     allowed object is
     *     {@link EsitoType }
     *     
     */
    public void setEsito(EsitoType value) {
        this.esito = value;
    }

    /**
     * Gets the value of the errorList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceErrorType }
     * 
     * 
     */
    public List<ServiceErrorType> getErrorList() {
        if (errorList == null) {
            errorList = new ArrayList<ServiceErrorType>();
        }
        return this.errorList;
    }

}
