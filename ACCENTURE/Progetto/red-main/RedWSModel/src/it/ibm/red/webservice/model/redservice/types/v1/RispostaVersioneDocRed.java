
package it.ibm.red.webservice.model.redservice.types.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RispostaVersioneDocRed complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RispostaVersioneDocRed">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:red:types:v1}RispostaRed">
 *       &lt;sequence>
 *         &lt;element name="versioneDocumento" type="{urn:red:types:v1}DocumentoRed" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RispostaVersioneDocRed", propOrder = {
    "versioneDocumento"
})
public class RispostaVersioneDocRed extends RispostaRed {

    /**
     * Versione.
     */
    @XmlElementRef(name = "versioneDocumento", type = JAXBElement.class, required = false)
    protected JAXBElement<DocumentoRed> versioneDocumento;

    /**
     * Gets the value of the versioneDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DocumentoRed }{@code >}
     *     
     */
    public JAXBElement<DocumentoRed> getVersioneDocumento() {
        return versioneDocumento;
    }

    /**
     * Sets the value of the versioneDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DocumentoRed }{@code >}
     *     
     */
    public void setVersioneDocumento(JAXBElement<DocumentoRed> value) {
        this.versioneDocumento = value;
    }

}
