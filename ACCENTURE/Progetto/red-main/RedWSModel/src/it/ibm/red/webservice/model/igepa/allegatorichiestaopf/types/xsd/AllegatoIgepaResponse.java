
package it.ibm.red.webservice.model.igepa.allegatorichiestaopf.types.xsd;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per AllegatoIgepaResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="AllegatoIgepaResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="allegatoIgepaResponse" type="{http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd}BaseResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AllegatoIgepaResponse", propOrder = {
    "allegatoIgepaResponse"
})
public class AllegatoIgepaResponse
    implements Serializable
{

    @XmlElementRef(name = "allegatoIgepaResponse", namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<BaseResponse> allegatoIgepaResponse;

    /**
     * Recupera il valore della proprietà allegatoIgepaResponse.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BaseResponse }{@code >}
     *     
     */
    public JAXBElement<BaseResponse> getAllegatoIgepaResponse() {
        return allegatoIgepaResponse;
    }

    /**
     * Imposta il valore della proprietà allegatoIgepaResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BaseResponse }{@code >}
     *     
     */
    public void setAllegatoIgepaResponse(JAXBElement<BaseResponse> value) {
        this.allegatoIgepaResponse = value;
    }

}
