
package it.ibm.red.webservice.model.interfaccianps.digitpa.protocollo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per Destinatario complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="Destinatario">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;sequence>
 *             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Amministrazione"/>
 *             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}AOO" minOccurs="0"/>
 *           &lt;/sequence>
 *           &lt;sequence>
 *             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Denominazione"/>
 *             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Persona" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;/sequence>
 *           &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Persona" maxOccurs="unbounded"/>
 *         &lt;/choice>
 *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}IndirizzoTelematico" minOccurs="0"/>
 *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Telefono" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Fax" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}IndirizzoPostale" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Destinatario", propOrder = {
    "content"
})
public class Destinatario
    implements Serializable
{

    @XmlElementRefs({
        @XmlElementRef(name = "AOO", namespace = "http://www.digitPa.gov.it/protocollo/", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Denominazione", namespace = "http://www.digitPa.gov.it/protocollo/", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "IndirizzoPostale", namespace = "http://www.digitPa.gov.it/protocollo/", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Fax", namespace = "http://www.digitPa.gov.it/protocollo/", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Persona", namespace = "http://www.digitPa.gov.it/protocollo/", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Amministrazione", namespace = "http://www.digitPa.gov.it/protocollo/", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Telefono", namespace = "http://www.digitPa.gov.it/protocollo/", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "IndirizzoTelematico", namespace = "http://www.digitPa.gov.it/protocollo/", type = JAXBElement.class, required = false)
    })
    protected List<JAXBElement<? extends Serializable>> content;

    /**
     * Recupera il resto del modello di contenuto. 
     * 
     * <p>
     * Questa proprietà "catch-all" viene recuperata per il seguente motivo: 
     * Il nome di campo "Persona" è usato da due diverse parti di uno schema. Vedere: 
     * riga 148 di file:/C:/Users/a.dilegge/Desktop/WSDL%20NPS%20v%202.16/segnatura.xsd
     * riga 146 di file:/C:/Users/a.dilegge/Desktop/WSDL%20NPS%20v%202.16/segnatura.xsd
     * <p>
     * Per eliminare questa proprietà, applicare una personalizzazione della proprietà a una 
     * delle seguenti due dichiarazioni per modificarne il nome: 
     * Gets the value of the content property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link AOO }{@code >}
     * {@link JAXBElement }{@code <}{@link Denominazione }{@code >}
     * {@link JAXBElement }{@code <}{@link IndirizzoPostale }{@code >}
     * {@link JAXBElement }{@code <}{@link Fax }{@code >}
     * {@link JAXBElement }{@code <}{@link Persona }{@code >}
     * {@link JAXBElement }{@code <}{@link Amministrazione }{@code >}
     * {@link JAXBElement }{@code <}{@link Telefono }{@code >}
     * {@link JAXBElement }{@code <}{@link IndirizzoTelematico }{@code >}
     * 
     * 
     */
    public List<JAXBElement<? extends Serializable>> getContent() {
        if (content == null) {
            content = new ArrayList<JAXBElement<? extends Serializable>>();
        }
        return this.content;
    }

}
