
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraNotificaInteroperabilitaProtocolloType;


/**
 * Dati del flusso delle notifiche di interoperabilità del protocollo
 * 
 * <p>Classe Java per wkf_datiFlussoInteroperabilita_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_datiFlussoInteroperabilita_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdMessaggio" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdDocumento" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="DatiFlusso" type="{http://mef.gov.it.v1.npsTypes}wkf_datiTipoFlusso_type" minOccurs="0"/>
 *         &lt;element name="messaggioRicevuto" type="{http://mef.gov.it.v1.npsTypes}email_message_type" minOccurs="0"/>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.npsTypes}wkf_identificatoreProtcollo_type" minOccurs="0"/>
 *         &lt;element name="TipoNotificaInteroperabile" type="{http://mef.gov.it.v1.npsTypes}prot_ricevuteSegnatura_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_datiFlussoInteroperabilita_type", propOrder = {
    "idMessaggio",
    "oggetto",
    "idDocumento",
    "datiFlusso",
    "messaggioRicevuto",
    "protocollo",
    "tipoNotificaInteroperabile"
})
@XmlSeeAlso({
    RichiestaElaboraNotificaInteroperabilitaProtocolloType.class
})
public class WkfDatiFlussoInteroperabilitaType
    implements Serializable
{

    @XmlElement(name = "IdMessaggio", required = true)
    protected String idMessaggio;
    @XmlElement(name = "Oggetto", required = true)
    protected String oggetto;
    @XmlElement(name = "IdDocumento", required = true)
    protected String idDocumento;
    @XmlElement(name = "DatiFlusso")
    protected WkfDatiTipoFlussoType datiFlusso;
    protected EmailMessageType messaggioRicevuto;
    @XmlElement(name = "Protocollo")
    protected WkfIdentificatoreProtcolloType protocollo;
    @XmlElement(name = "TipoNotificaInteroperabile", required = true)
    protected ProtRicevuteSegnaturaType tipoNotificaInteroperabile;

    /**
     * Recupera il valore della proprietà idMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggio() {
        return idMessaggio;
    }

    /**
     * Imposta il valore della proprietà idMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggio(String value) {
        this.idMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà oggetto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Imposta il valore della proprietà oggetto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Recupera il valore della proprietà idDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Imposta il valore della proprietà idDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

    /**
     * Recupera il valore della proprietà datiFlusso.
     * 
     * @return
     *     possible object is
     *     {@link WkfDatiTipoFlussoType }
     *     
     */
    public WkfDatiTipoFlussoType getDatiFlusso() {
        return datiFlusso;
    }

    /**
     * Imposta il valore della proprietà datiFlusso.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfDatiTipoFlussoType }
     *     
     */
    public void setDatiFlusso(WkfDatiTipoFlussoType value) {
        this.datiFlusso = value;
    }

    /**
     * Recupera il valore della proprietà messaggioRicevuto.
     * 
     * @return
     *     possible object is
     *     {@link EmailMessageType }
     *     
     */
    public EmailMessageType getMessaggioRicevuto() {
        return messaggioRicevuto;
    }

    /**
     * Imposta il valore della proprietà messaggioRicevuto.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailMessageType }
     *     
     */
    public void setMessaggioRicevuto(EmailMessageType value) {
        this.messaggioRicevuto = value;
    }

    /**
     * Recupera il valore della proprietà protocollo.
     * 
     * @return
     *     possible object is
     *     {@link WkfIdentificatoreProtcolloType }
     *     
     */
    public WkfIdentificatoreProtcolloType getProtocollo() {
        return protocollo;
    }

    /**
     * Imposta il valore della proprietà protocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfIdentificatoreProtcolloType }
     *     
     */
    public void setProtocollo(WkfIdentificatoreProtcolloType value) {
        this.protocollo = value;
    }

    /**
     * Recupera il valore della proprietà tipoNotificaInteroperabile.
     * 
     * @return
     *     possible object is
     *     {@link ProtRicevuteSegnaturaType }
     *     
     */
    public ProtRicevuteSegnaturaType getTipoNotificaInteroperabile() {
        return tipoNotificaInteroperabile;
    }

    /**
     * Imposta il valore della proprietà tipoNotificaInteroperabile.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtRicevuteSegnaturaType }
     *     
     */
    public void setTipoNotificaInteroperabile(ProtRicevuteSegnaturaType value) {
        this.tipoNotificaInteroperabile = value;
    }

}
