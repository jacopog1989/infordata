
package it.ibm.red.webservice.model.redservice.header.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CredenzialiWSRed complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CredenzialiWSRed">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idClient" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pwdClient" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CredenzialiWSRed", propOrder = {
    "idClient",
    "pwdClient"
})
public class CredenzialiWSRed {


	/**
	 * Id Client.
	 */
    @XmlElementRef(name = "idClient", type = JAXBElement.class, required = false)
    protected JAXBElement<String> idClient;

	/**
	 * Password Client.
	 */
    @XmlElementRef(name = "pwdClient", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pwdClient;

    /**
     * Gets the value of the idClient property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIdClient() {
        return idClient;
    }

    /**
     * Sets the value of the idClient property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIdClient(JAXBElement<String> value) {
        this.idClient = value;
    }

    /**
     * Gets the value of the pwdClient property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPwdClient() {
        return pwdClient;
    }

    /**
     * Sets the value of the pwdClient property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPwdClient(JAXBElement<String> value) {
        this.pwdClient = value;
    }

}
