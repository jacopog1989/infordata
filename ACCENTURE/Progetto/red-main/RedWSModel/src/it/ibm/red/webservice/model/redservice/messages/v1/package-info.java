/**
 * Messaggi.
 */

@javax.xml.bind.annotation.XmlSchema(namespace = "urn:red:messages:v1")
package it.ibm.red.webservice.model.redservice.messages.v1;
