
package it.ibm.red.webservice.model.igepa.psi.types.xsd;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per DocumentoCertificazionePattoType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="DocumentoCertificazionePattoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="allegato" type="{http://types.ws.filenet.nsd.capgemini.com/xsd}DocumentFile" minOccurs="0"/>
 *         &lt;element name="dataProtocollo" type="{http://types.ws.filenet.nsd.capgemini.com/xsd}DataFormatType" minOccurs="0"/>
 *         &lt;element name="documentoPrincipale" type="{http://types.ws.filenet.nsd.capgemini.com/xsd}DocumentFile" minOccurs="0"/>
 *         &lt;element name="enteMittente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idUfficioDestinatario" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idUtenteDestinatario" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numeroProtocollo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="oggettoProtocollo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentoCertificazionePattoType", propOrder = {
    "allegato",
    "dataProtocollo",
    "documentoPrincipale",
    "enteMittente",
    "idUfficioDestinatario",
    "idUtenteDestinatario",
    "numeroProtocollo",
    "oggettoProtocollo"
})
public class DocumentoCertificazionePattoType
    implements Serializable
{

    @XmlElementRef(name = "allegato", namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<DocumentFile> allegato;
    @XmlElementRef(name = "dataProtocollo", namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<DataFormatType> dataProtocollo;
    @XmlElementRef(name = "documentoPrincipale", namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<DocumentFile> documentoPrincipale;
    @XmlElementRef(name = "enteMittente", namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> enteMittente;
    @XmlElementRef(name = "idUfficioDestinatario", namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> idUfficioDestinatario;
    @XmlElementRef(name = "idUtenteDestinatario", namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> idUtenteDestinatario;
    @XmlElementRef(name = "numeroProtocollo", namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> numeroProtocollo;
    @XmlElementRef(name = "oggettoProtocollo", namespace = "http://types.ws.filenet.nsd.capgemini.com/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> oggettoProtocollo;

    /**
     * Recupera il valore della proprietà allegato.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DocumentFile }{@code >}
     *     
     */
    public JAXBElement<DocumentFile> getAllegato() {
        return allegato;
    }

    /**
     * Imposta il valore della proprietà allegato.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DocumentFile }{@code >}
     *     
     */
    public void setAllegato(JAXBElement<DocumentFile> value) {
        this.allegato = value;
    }

    /**
     * Recupera il valore della proprietà dataProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DataFormatType }{@code >}
     *     
     */
    public JAXBElement<DataFormatType> getDataProtocollo() {
        return dataProtocollo;
    }

    /**
     * Imposta il valore della proprietà dataProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DataFormatType }{@code >}
     *     
     */
    public void setDataProtocollo(JAXBElement<DataFormatType> value) {
        this.dataProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà documentoPrincipale.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DocumentFile }{@code >}
     *     
     */
    public JAXBElement<DocumentFile> getDocumentoPrincipale() {
        return documentoPrincipale;
    }

    /**
     * Imposta il valore della proprietà documentoPrincipale.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DocumentFile }{@code >}
     *     
     */
    public void setDocumentoPrincipale(JAXBElement<DocumentFile> value) {
        this.documentoPrincipale = value;
    }

    /**
     * Recupera il valore della proprietà enteMittente.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEnteMittente() {
        return enteMittente;
    }

    /**
     * Imposta il valore della proprietà enteMittente.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEnteMittente(JAXBElement<String> value) {
        this.enteMittente = value;
    }

    /**
     * Recupera il valore della proprietà idUfficioDestinatario.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getIdUfficioDestinatario() {
        return idUfficioDestinatario;
    }

    /**
     * Imposta il valore della proprietà idUfficioDestinatario.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setIdUfficioDestinatario(JAXBElement<Integer> value) {
        this.idUfficioDestinatario = value;
    }

    /**
     * Recupera il valore della proprietà idUtenteDestinatario.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getIdUtenteDestinatario() {
        return idUtenteDestinatario;
    }

    /**
     * Imposta il valore della proprietà idUtenteDestinatario.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setIdUtenteDestinatario(JAXBElement<Integer> value) {
        this.idUtenteDestinatario = value;
    }

    /**
     * Recupera il valore della proprietà numeroProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getNumeroProtocollo() {
        return numeroProtocollo;
    }

    /**
     * Imposta il valore della proprietà numeroProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setNumeroProtocollo(JAXBElement<Integer> value) {
        this.numeroProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà oggettoProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOggettoProtocollo() {
        return oggettoProtocollo;
    }

    /**
     * Imposta il valore della proprietà oggettoProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOggettoProtocollo(JAXBElement<String> value) {
        this.oggettoProtocollo = value;
    }

}
