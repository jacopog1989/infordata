/**
 * Header FAD.
 */

@javax.xml.bind.annotation.XmlSchema(namespace = "http://mef.gov.it.v1.red/servizi/Common/HeaderFault", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package it.ibm.red.webservice.model.redfad.headerfault;
