//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.01.28 at 06:39:59 PM CET 
//


package it.ibm.red.webservice.model.documentservice.types.ricerca;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TipoVersione.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TipoVersione">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CURRENT"/>
 *     &lt;enumeration value="RELEASED"/>
 *     &lt;enumeration value="ALL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TipoVersione")
@XmlEnum
public enum TipoVersione {


	/**
	 * Valore.
	 */
    CURRENT,

	/**
	 * Valore.
	 */
    RELEASED,

	/**
	 * Valore.
	 */
    ALL;

	/**
	 * Restituisce il valore.
	 * @return value
	 */
    public String value() {
        return name();
    }

    /**
     * Restituisce l'enum associato al valore v.
     * @param v
     * @return enum associata a v
     */
    public static TipoVersione fromValue(String v) {
        return valueOf(v);
    }

}
