
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Definisce il tipo di dato delle ricevuto PEC
 * 
 * <p>Classe Java per prot_ricevutePec_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_ricevutePec_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DataNotifica" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="TipoNotifica">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="ACCETTAZIONE"/>
 *               &lt;enumeration value="NON_ACCETTAZIONE"/>
 *               &lt;enumeration value="PRESA_IN_CARICO"/>
 *               &lt;enumeration value="AVVENUTA_CONSEGNA"/>
 *               &lt;enumeration value="POSTA_CERTIFICATA"/>
 *               &lt;enumeration value="ERRORE_CONSEGNA"/>
 *               &lt;enumeration value="PREAVVISO_ERRORE_CONSEGNA"/>
 *               &lt;enumeration value="ANOMALIA_MESSAGGIO"/>
 *               &lt;enumeration value="RILEVAZIONE_VIRUS"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TestoNotifica" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContentNotifica" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_ricevutePec_type", propOrder = {
    "dataNotifica",
    "tipoNotifica",
    "testoNotifica",
    "contentNotifica"
})
public class ProtRicevutePecType
    implements Serializable
{

    @XmlElement(name = "DataNotifica", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataNotifica;
    @XmlElement(name = "TipoNotifica", required = true)
    protected String tipoNotifica;
    @XmlElement(name = "TestoNotifica", required = true)
    protected String testoNotifica;
    @XmlElement(name = "ContentNotifica")
    protected byte[] contentNotifica;

    /**
     * Recupera il valore della proprietà dataNotifica.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataNotifica() {
        return dataNotifica;
    }

    /**
     * Imposta il valore della proprietà dataNotifica.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataNotifica(XMLGregorianCalendar value) {
        this.dataNotifica = value;
    }

    /**
     * Recupera il valore della proprietà tipoNotifica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoNotifica() {
        return tipoNotifica;
    }

    /**
     * Imposta il valore della proprietà tipoNotifica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoNotifica(String value) {
        this.tipoNotifica = value;
    }

    /**
     * Recupera il valore della proprietà testoNotifica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTestoNotifica() {
        return testoNotifica;
    }

    /**
     * Imposta il valore della proprietà testoNotifica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTestoNotifica(String value) {
        this.testoNotifica = value;
    }

    /**
     * Recupera il valore della proprietà contentNotifica.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getContentNotifica() {
        return contentNotifica;
    }

    /**
     * Imposta il valore della proprietà contentNotifica.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setContentNotifica(byte[] value) {
        this.contentNotifica = value;
    }

}
