
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaValidazioneStatoFlussoType;


/**
 * Dati per la validazione del flusso
 * 
 * <p>Classe Java per wkf_datiValidazioneFlusso_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_datiValidazioneFlusso_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdMessaggio" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="DataOperazione" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CodiceFlusso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChiaveFascicolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChiaveDocumento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdentificatoreProcesso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdentificativoMessaggio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContestoProcedurale" type="{http://mef.gov.it.v1.npsTypes}wkf_datiContestoProcedurale_type" maxOccurs="unbounded"/>
 *         &lt;element name="messaggioRicevuto" type="{http://mef.gov.it.v1.npsTypes}email_message_type" minOccurs="0"/>
 *         &lt;element name="Aoo" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_aoo_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_datiValidazioneFlusso_type", propOrder = {
    "idMessaggio",
    "dataOperazione",
    "oggetto",
    "codiceFlusso",
    "chiaveFascicolo",
    "chiaveDocumento",
    "identificatoreProcesso",
    "identificativoMessaggio",
    "contestoProcedurale",
    "messaggioRicevuto",
    "aoo"
})
@XmlSeeAlso({
    RichiestaValidazioneStatoFlussoType.class
})
public class WkfDatiValidazioneFlussoType
    implements Serializable
{

    @XmlElement(name = "IdMessaggio", required = true)
    protected String idMessaggio;
    @XmlElement(name = "DataOperazione", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataOperazione;
    @XmlElement(name = "Oggetto", required = true)
    protected String oggetto;
    @XmlElement(name = "CodiceFlusso", required = true)
    protected String codiceFlusso;
    @XmlElement(name = "ChiaveFascicolo")
    protected String chiaveFascicolo;
    @XmlElement(name = "ChiaveDocumento", required = true)
    protected String chiaveDocumento;
    @XmlElement(name = "IdentificatoreProcesso", required = true)
    protected String identificatoreProcesso;
    @XmlElement(name = "IdentificativoMessaggio", required = true)
    protected String identificativoMessaggio;
    @XmlElement(name = "ContestoProcedurale", required = true)
    protected List<WkfDatiContestoProceduraleType> contestoProcedurale;
    protected EmailMessageType messaggioRicevuto;
    @XmlElement(name = "Aoo")
    protected OrgOrganigrammaAooType aoo;

    /**
     * Recupera il valore della proprietà idMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggio() {
        return idMessaggio;
    }

    /**
     * Imposta il valore della proprietà idMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggio(String value) {
        this.idMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà dataOperazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataOperazione() {
        return dataOperazione;
    }

    /**
     * Imposta il valore della proprietà dataOperazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataOperazione(XMLGregorianCalendar value) {
        this.dataOperazione = value;
    }

    /**
     * Recupera il valore della proprietà oggetto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Imposta il valore della proprietà oggetto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Recupera il valore della proprietà codiceFlusso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceFlusso() {
        return codiceFlusso;
    }

    /**
     * Imposta il valore della proprietà codiceFlusso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceFlusso(String value) {
        this.codiceFlusso = value;
    }

    /**
     * Recupera il valore della proprietà chiaveFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveFascicolo() {
        return chiaveFascicolo;
    }

    /**
     * Imposta il valore della proprietà chiaveFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveFascicolo(String value) {
        this.chiaveFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà chiaveDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveDocumento() {
        return chiaveDocumento;
    }

    /**
     * Imposta il valore della proprietà chiaveDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveDocumento(String value) {
        this.chiaveDocumento = value;
    }

    /**
     * Recupera il valore della proprietà identificatoreProcesso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificatoreProcesso() {
        return identificatoreProcesso;
    }

    /**
     * Imposta il valore della proprietà identificatoreProcesso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificatoreProcesso(String value) {
        this.identificatoreProcesso = value;
    }

    /**
     * Recupera il valore della proprietà identificativoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificativoMessaggio() {
        return identificativoMessaggio;
    }

    /**
     * Imposta il valore della proprietà identificativoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificativoMessaggio(String value) {
        this.identificativoMessaggio = value;
    }

    /**
     * Gets the value of the contestoProcedurale property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contestoProcedurale property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContestoProcedurale().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WkfDatiContestoProceduraleType }
     * 
     * 
     */
    public List<WkfDatiContestoProceduraleType> getContestoProcedurale() {
        if (contestoProcedurale == null) {
            contestoProcedurale = new ArrayList<WkfDatiContestoProceduraleType>();
        }
        return this.contestoProcedurale;
    }

    /**
     * Recupera il valore della proprietà messaggioRicevuto.
     * 
     * @return
     *     possible object is
     *     {@link EmailMessageType }
     *     
     */
    public EmailMessageType getMessaggioRicevuto() {
        return messaggioRicevuto;
    }

    /**
     * Imposta il valore della proprietà messaggioRicevuto.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailMessageType }
     *     
     */
    public void setMessaggioRicevuto(EmailMessageType value) {
        this.messaggioRicevuto = value;
    }

    /**
     * Recupera il valore della proprietà aoo.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaAooType }
     *     
     */
    public OrgOrganigrammaAooType getAoo() {
        return aoo;
    }

    /**
     * Imposta il valore della proprietà aoo.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaAooType }
     *     
     */
    public void setAoo(OrgOrganigrammaAooType value) {
        this.aoo = value;
    }

}
