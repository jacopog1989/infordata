
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the npstypes.v1.it.gov.mef package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ProtTipologiaDocumentoTypeDescrizione_QNAME = new QName("", "Descrizione");
    private final static QName _ProtAttributiEstesiTypeMetadatiAssociati_QNAME = new QName("", "metadatiAssociati");
    private final static QName _WkfDatiNotificaOperazioneTypeIdDocumento_QNAME = new QName("", "IdDocumento");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: npstypes.v1.it.gov.mef
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WkfDatiNotificaErroreProtocollazioneAutomaticaType }
     * 
     */
    public WkfDatiNotificaErroreProtocollazioneAutomaticaType createWkfDatiNotificaErroreProtocollazioneAutomaticaType() {
        return new WkfDatiNotificaErroreProtocollazioneAutomaticaType();
    }

    /**
     * Create an instance of {@link WkfDatiNotificaErroreProtocollazioneFlussoType }
     * 
     */
    public WkfDatiNotificaErroreProtocollazioneFlussoType createWkfDatiNotificaErroreProtocollazioneFlussoType() {
        return new WkfDatiNotificaErroreProtocollazioneFlussoType();
    }

    /**
     * Create an instance of {@link ProtIdentificatoreProtocolloCollegatoType }
     * 
     */
    public ProtIdentificatoreProtocolloCollegatoType createProtIdentificatoreProtocolloCollegatoType() {
        return new ProtIdentificatoreProtocolloCollegatoType();
    }

    /**
     * Create an instance of {@link WkfAzioneAggiornamentoProtocolloType }
     * 
     */
    public WkfAzioneAggiornamentoProtocolloType createWkfAzioneAggiornamentoProtocolloType() {
        return new WkfAzioneAggiornamentoProtocolloType();
    }

    /**
     * Create an instance of {@link ProtCorrispondenteType }
     * 
     */
    public ProtCorrispondenteType createProtCorrispondenteType() {
        return new ProtCorrispondenteType();
    }

    /**
     * Create an instance of {@link WkfAzioneCambioStatoProtocolloType }
     * 
     */
    public WkfAzioneCambioStatoProtocolloType createWkfAzioneCambioStatoProtocolloType() {
        return new WkfAzioneCambioStatoProtocolloType();
    }

    /**
     * Create an instance of {@link ProtSpedizioneType }
     * 
     */
    public ProtSpedizioneType createProtSpedizioneType() {
        return new ProtSpedizioneType();
    }

    /**
     * Create an instance of {@link ProtSpedizioneType.MezzoSpedizione }
     * 
     */
    public ProtSpedizioneType.MezzoSpedizione createProtSpedizioneTypeMezzoSpedizione() {
        return new ProtSpedizioneType.MezzoSpedizione();
    }

    /**
     * Create an instance of {@link EmailMessageType }
     * 
     */
    public EmailMessageType createEmailMessageType() {
        return new EmailMessageType();
    }

    /**
     * Create an instance of {@link WkfAzioneAnnullamentoProtocolloType }
     * 
     */
    public WkfAzioneAnnullamentoProtocolloType createWkfAzioneAnnullamentoProtocolloType() {
        return new WkfAzioneAnnullamentoProtocolloType();
    }

    /**
     * Create an instance of {@link WkfDatiNotificaInviataType }
     * 
     */
    public WkfDatiNotificaInviataType createWkfDatiNotificaInviataType() {
        return new WkfDatiNotificaInviataType();
    }

    /**
     * Create an instance of {@link ProtDestinatarioType }
     * 
     */
    public ProtDestinatarioType createProtDestinatarioType() {
        return new ProtDestinatarioType();
    }

    /**
     * Create an instance of {@link OrgOrganigrammaType }
     * 
     */
    public OrgOrganigrammaType createOrgOrganigrammaType() {
        return new OrgOrganigrammaType();
    }

    /**
     * Create an instance of {@link WkfAzioneAggiornamentoCollegatiType }
     * 
     */
    public WkfAzioneAggiornamentoCollegatiType createWkfAzioneAggiornamentoCollegatiType() {
        return new WkfAzioneAggiornamentoCollegatiType();
    }

    /**
     * Create an instance of {@link IdentificativoDestinatarioType }
     * 
     */
    public IdentificativoDestinatarioType createIdentificativoDestinatarioType() {
        return new IdentificativoDestinatarioType();
    }

    /**
     * Create an instance of {@link OrgCasellaEmailType }
     * 
     */
    public OrgCasellaEmailType createOrgCasellaEmailType() {
        return new OrgCasellaEmailType();
    }

    /**
     * Create an instance of {@link ProtTipologiaDocumentoType }
     * 
     */
    public ProtTipologiaDocumentoType createProtTipologiaDocumentoType() {
        return new ProtTipologiaDocumentoType();
    }

    /**
     * Create an instance of {@link OrgOrganigrammaAooType }
     * 
     */
    public OrgOrganigrammaAooType createOrgOrganigrammaAooType() {
        return new OrgOrganigrammaAooType();
    }

    /**
     * Create an instance of {@link ProtValidazioneMessaggioType }
     * 
     */
    public ProtValidazioneMessaggioType createProtValidazioneMessaggioType() {
        return new ProtValidazioneMessaggioType();
    }

    /**
     * Create an instance of {@link WkfDatiContestoProceduraleType }
     * 
     */
    public WkfDatiContestoProceduraleType createWkfDatiContestoProceduraleType() {
        return new WkfDatiContestoProceduraleType();
    }

    /**
     * Create an instance of {@link ProtAnnullamentoType }
     * 
     */
    public ProtAnnullamentoType createProtAnnullamentoType() {
        return new ProtAnnullamentoType();
    }

    /**
     * Create an instance of {@link WkfDatiProtocollazioneCodaType }
     * 
     */
    public WkfDatiProtocollazioneCodaType createWkfDatiProtocollazioneCodaType() {
        return new WkfDatiProtocollazioneCodaType();
    }

    /**
     * Create an instance of {@link AnaToponimoType }
     * 
     */
    public AnaToponimoType createAnaToponimoType() {
        return new AnaToponimoType();
    }

    /**
     * Create an instance of {@link ProtRicezioneType }
     * 
     */
    public ProtRicezioneType createProtRicezioneType() {
        return new ProtRicezioneType();
    }

    /**
     * Create an instance of {@link WkfAzioneRimozioneDocumentoProtocolloType }
     * 
     */
    public WkfAzioneRimozioneDocumentoProtocolloType createWkfAzioneRimozioneDocumentoProtocolloType() {
        return new WkfAzioneRimozioneDocumentoProtocolloType();
    }

    /**
     * Create an instance of {@link EmailAllegatiMessaggioType }
     * 
     */
    public EmailAllegatiMessaggioType createEmailAllegatiMessaggioType() {
        return new EmailAllegatiMessaggioType();
    }

    /**
     * Create an instance of {@link WkfAzioneInserimentoDestinatarioProtocolloType }
     * 
     */
    public WkfAzioneInserimentoDestinatarioProtocolloType createWkfAzioneInserimentoDestinatarioProtocolloType() {
        return new WkfAzioneInserimentoDestinatarioProtocolloType();
    }

    /**
     * Create an instance of {@link AnaIndirizzoType }
     * 
     */
    public AnaIndirizzoType createAnaIndirizzoType() {
        return new AnaIndirizzoType();
    }

    /**
     * Create an instance of {@link ProtRicevutePecType }
     * 
     */
    public ProtRicevutePecType createProtRicevutePecType() {
        return new ProtRicevutePecType();
    }

    /**
     * Create an instance of {@link EmailDatiPostaIngressoType }
     * 
     */
    public EmailDatiPostaIngressoType createEmailDatiPostaIngressoType() {
        return new EmailDatiPostaIngressoType();
    }

    /**
     * Create an instance of {@link WkfAzioneSpedizioneDestinatarioCartaceoProtocolloType }
     * 
     */
    public WkfAzioneSpedizioneDestinatarioCartaceoProtocolloType createWkfAzioneSpedizioneDestinatarioCartaceoProtocolloType() {
        return new WkfAzioneSpedizioneDestinatarioCartaceoProtocolloType();
    }

    /**
     * Create an instance of {@link WkfDatiFlussoType }
     * 
     */
    public WkfDatiFlussoType createWkfDatiFlussoType() {
        return new WkfDatiFlussoType();
    }

    /**
     * Create an instance of {@link ProtMetadatiType }
     * 
     */
    public ProtMetadatiType createProtMetadatiType() {
        return new ProtMetadatiType();
    }

    /**
     * Create an instance of {@link DocDocumentoFileType }
     * 
     */
    public DocDocumentoFileType createDocDocumentoFileType() {
        return new DocDocumentoFileType();
    }

    /**
     * Create an instance of {@link OrgOrganigrammaUnitaOrganizzativaType }
     * 
     */
    public OrgOrganigrammaUnitaOrganizzativaType createOrgOrganigrammaUnitaOrganizzativaType() {
        return new OrgOrganigrammaUnitaOrganizzativaType();
    }

    /**
     * Create an instance of {@link IdentificativoDocumentoRequestType }
     * 
     */
    public IdentificativoDocumentoRequestType createIdentificativoDocumentoRequestType() {
        return new IdentificativoDocumentoRequestType();
    }

    /**
     * Create an instance of {@link ProtRegistroProtocolloType }
     * 
     */
    public ProtRegistroProtocolloType createProtRegistroProtocolloType() {
        return new ProtRegistroProtocolloType();
    }

    /**
     * Create an instance of {@link WkfDatiNotificaType }
     * 
     */
    public WkfDatiNotificaType createWkfDatiNotificaType() {
        return new WkfDatiNotificaType();
    }

    /**
     * Create an instance of {@link AnaEnteEsternoType }
     * 
     */
    public AnaEnteEsternoType createAnaEnteEsternoType() {
        return new AnaEnteEsternoType();
    }

    /**
     * Create an instance of {@link OrgUnitaOrganizzativaType }
     * 
     */
    public OrgUnitaOrganizzativaType createOrgUnitaOrganizzativaType() {
        return new OrgUnitaOrganizzativaType();
    }

    /**
     * Create an instance of {@link WkfDatiFlussoNotificaPECType }
     * 
     */
    public WkfDatiFlussoNotificaPECType createWkfDatiFlussoNotificaPECType() {
        return new WkfDatiFlussoNotificaPECType();
    }

    /**
     * Create an instance of {@link IdentificativoProtocolloRequestType }
     * 
     */
    public IdentificativoProtocolloRequestType createIdentificativoProtocolloRequestType() {
        return new IdentificativoProtocolloRequestType();
    }

    /**
     * Create an instance of {@link WkfDatiFlussoNotificaPEOType }
     * 
     */
    public WkfDatiFlussoNotificaPEOType createWkfDatiFlussoNotificaPEOType() {
        return new WkfDatiFlussoNotificaPEOType();
    }

    /**
     * Create an instance of {@link WkfDatiNotificaProtocollazioneEmergenzaType }
     * 
     */
    public WkfDatiNotificaProtocollazioneEmergenzaType createWkfDatiNotificaProtocollazioneEmergenzaType() {
        return new WkfDatiNotificaProtocollazioneEmergenzaType();
    }

    /**
     * Create an instance of {@link WkfDatiNotificaProtocollazioneAutomaticaType }
     * 
     */
    public WkfDatiNotificaProtocollazioneAutomaticaType createWkfDatiNotificaProtocollazioneAutomaticaType() {
        return new WkfDatiNotificaProtocollazioneAutomaticaType();
    }

    /**
     * Create an instance of {@link WkfAzioneSpedizioneDestinatarioElettronicoProtocolloType }
     * 
     */
    public WkfAzioneSpedizioneDestinatarioElettronicoProtocolloType createWkfAzioneSpedizioneDestinatarioElettronicoProtocolloType() {
        return new WkfAzioneSpedizioneDestinatarioElettronicoProtocolloType();
    }

    /**
     * Create an instance of {@link ProtIdentificatoreProtocolloType }
     * 
     */
    public ProtIdentificatoreProtocolloType createProtIdentificatoreProtocolloType() {
        return new ProtIdentificatoreProtocolloType();
    }

    /**
     * Create an instance of {@link AnaIndirizzoPostaleType }
     * 
     */
    public AnaIndirizzoPostaleType createAnaIndirizzoPostaleType() {
        return new AnaIndirizzoPostaleType();
    }

    /**
     * Create an instance of {@link AllChiaveEsternaType }
     * 
     */
    public AllChiaveEsternaType createAllChiaveEsternaType() {
        return new AllChiaveEsternaType();
    }

    /**
     * Create an instance of {@link WkfDatiNotificaAzioneType }
     * 
     */
    public WkfDatiNotificaAzioneType createWkfDatiNotificaAzioneType() {
        return new WkfDatiNotificaAzioneType();
    }

    /**
     * Create an instance of {@link ProtMetadatoAssociatoType }
     * 
     */
    public ProtMetadatoAssociatoType createProtMetadatoAssociatoType() {
        return new ProtMetadatoAssociatoType();
    }

    /**
     * Create an instance of {@link ProtErroreValidazioneSegnaturaType }
     * 
     */
    public ProtErroreValidazioneSegnaturaType createProtErroreValidazioneSegnaturaType() {
        return new ProtErroreValidazioneSegnaturaType();
    }

    /**
     * Create an instance of {@link WkfDatiFlussoEntrataType }
     * 
     */
    public WkfDatiFlussoEntrataType createWkfDatiFlussoEntrataType() {
        return new WkfDatiFlussoEntrataType();
    }

    /**
     * Create an instance of {@link DocCompletamentoOperazioneDocumentaleType }
     * 
     */
    public DocCompletamentoOperazioneDocumentaleType createDocCompletamentoOperazioneDocumentaleType() {
        return new DocCompletamentoOperazioneDocumentaleType();
    }

    /**
     * Create an instance of {@link WkfAzioneRimozioneDestinatarioProtocolloType }
     * 
     */
    public WkfAzioneRimozioneDestinatarioProtocolloType createWkfAzioneRimozioneDestinatarioProtocolloType() {
        return new WkfAzioneRimozioneDestinatarioProtocolloType();
    }

    /**
     * Create an instance of {@link WkfDatiSegnaturaType }
     * 
     */
    public WkfDatiSegnaturaType createWkfDatiSegnaturaType() {
        return new WkfDatiSegnaturaType();
    }

    /**
     * Create an instance of {@link ProtMittenteType }
     * 
     */
    public ProtMittenteType createProtMittenteType() {
        return new ProtMittenteType();
    }

    /**
     * Create an instance of {@link AnaPersonaFisicaType }
     * 
     */
    public AnaPersonaFisicaType createAnaPersonaFisicaType() {
        return new AnaPersonaFisicaType();
    }

    /**
     * Create an instance of {@link IdentificativoAssegnatarioType }
     * 
     */
    public IdentificativoAssegnatarioType createIdentificativoAssegnatarioType() {
        return new IdentificativoAssegnatarioType();
    }

    /**
     * Create an instance of {@link WkfDatiTipoFlussoType }
     * 
     */
    public WkfDatiTipoFlussoType createWkfDatiTipoFlussoType() {
        return new WkfDatiTipoFlussoType();
    }

    /**
     * Create an instance of {@link WkfAzioneInserimentoDocumentoProtocolloType }
     * 
     */
    public WkfAzioneInserimentoDocumentoProtocolloType createWkfAzioneInserimentoDocumentoProtocolloType() {
        return new WkfAzioneInserimentoDocumentoProtocolloType();
    }

    /**
     * Create an instance of {@link WkfDatiErroreFlussoType }
     * 
     */
    public WkfDatiErroreFlussoType createWkfDatiErroreFlussoType() {
        return new WkfDatiErroreFlussoType();
    }

    /**
     * Create an instance of {@link OrgAooType }
     * 
     */
    public OrgAooType createOrgAooType() {
        return new OrgAooType();
    }

    /**
     * Create an instance of {@link ProtValidazioneSegnaturaType }
     * 
     */
    public ProtValidazioneSegnaturaType createProtValidazioneSegnaturaType() {
        return new ProtValidazioneSegnaturaType();
    }

    /**
     * Create an instance of {@link AnaPersonaGiuridicaType }
     * 
     */
    public AnaPersonaGiuridicaType createAnaPersonaGiuridicaType() {
        return new AnaPersonaGiuridicaType();
    }

    /**
     * Create an instance of {@link ProtRicevuteSegnaturaType }
     * 
     */
    public ProtRicevuteSegnaturaType createProtRicevuteSegnaturaType() {
        return new ProtRicevuteSegnaturaType();
    }

    /**
     * Create an instance of {@link OrgAmministrazioneType }
     * 
     */
    public OrgAmministrazioneType createOrgAmministrazioneType() {
        return new OrgAmministrazioneType();
    }

    /**
     * Create an instance of {@link EmailIndirizzoEmailType }
     * 
     */
    public EmailIndirizzoEmailType createEmailIndirizzoEmailType() {
        return new EmailIndirizzoEmailType();
    }

    /**
     * Create an instance of {@link WkfAzioneCreazioneProtocolloType }
     * 
     */
    public WkfAzioneCreazioneProtocolloType createWkfAzioneCreazioneProtocolloType() {
        return new WkfAzioneCreazioneProtocolloType();
    }

    /**
     * Create an instance of {@link WkfDatiFlussoInteroperabilitaType }
     * 
     */
    public WkfDatiFlussoInteroperabilitaType createWkfDatiFlussoInteroperabilitaType() {
        return new WkfDatiFlussoInteroperabilitaType();
    }

    /**
     * Create an instance of {@link AnaIndirizzoTelematicoType }
     * 
     */
    public AnaIndirizzoTelematicoType createAnaIndirizzoTelematicoType() {
        return new AnaIndirizzoTelematicoType();
    }

    /**
     * Create an instance of {@link WkfAzioneAssegnazioneApplicazioneProtocolloType }
     * 
     */
    public WkfAzioneAssegnazioneApplicazioneProtocolloType createWkfAzioneAssegnazioneApplicazioneProtocolloType() {
        return new WkfAzioneAssegnazioneApplicazioneProtocolloType();
    }

    /**
     * Create an instance of {@link WkfAzioneCambioAssegnazioneApplicazioneProtocolloType }
     * 
     */
    public WkfAzioneCambioAssegnazioneApplicazioneProtocolloType createWkfAzioneCambioAssegnazioneApplicazioneProtocolloType() {
        return new WkfAzioneCambioAssegnazioneApplicazioneProtocolloType();
    }

    /**
     * Create an instance of {@link WkfDatiValidazioneFlussoType }
     * 
     */
    public WkfDatiValidazioneFlussoType createWkfDatiValidazioneFlussoType() {
        return new WkfDatiValidazioneFlussoType();
    }

    /**
     * Create an instance of {@link AllCodiceDescrizioneType }
     * 
     */
    public AllCodiceDescrizioneType createAllCodiceDescrizioneType() {
        return new AllCodiceDescrizioneType();
    }

    /**
     * Create an instance of {@link WkfAzioneRimozioneAssegnazioneApplicazioneProtocolloType }
     * 
     */
    public WkfAzioneRimozioneAssegnazioneApplicazioneProtocolloType createWkfAzioneRimozioneAssegnazioneApplicazioneProtocolloType() {
        return new WkfAzioneRimozioneAssegnazioneApplicazioneProtocolloType();
    }

    /**
     * Create an instance of {@link WkfDatiNotificaOperazioneType }
     * 
     */
    public WkfDatiNotificaOperazioneType createWkfDatiNotificaOperazioneType() {
        return new WkfDatiNotificaOperazioneType();
    }

    /**
     * Create an instance of {@link WkfAzioneRispondiAProtocolloType }
     * 
     */
    public WkfAzioneRispondiAProtocolloType createWkfAzioneRispondiAProtocolloType() {
        return new WkfAzioneRispondiAProtocolloType();
    }

    /**
     * Create an instance of {@link ProtAttributiEstesiType }
     * 
     */
    public ProtAttributiEstesiType createProtAttributiEstesiType() {
        return new ProtAttributiEstesiType();
    }

    /**
     * Create an instance of {@link ProtAssegnatarioType }
     * 
     */
    public ProtAssegnatarioType createProtAssegnatarioType() {
        return new ProtAssegnatarioType();
    }

    /**
     * Create an instance of {@link WkfIdentificatoreProtcolloType }
     * 
     */
    public WkfIdentificatoreProtcolloType createWkfIdentificatoreProtcolloType() {
        return new WkfIdentificatoreProtcolloType();
    }

    /**
     * Create an instance of {@link WkfDatiNotificaErroreProtocollazioneAutomaticaType.NotificaInviata }
     * 
     */
    public WkfDatiNotificaErroreProtocollazioneAutomaticaType.NotificaInviata createWkfDatiNotificaErroreProtocollazioneAutomaticaTypeNotificaInviata() {
        return new WkfDatiNotificaErroreProtocollazioneAutomaticaType.NotificaInviata();
    }

    /**
     * Create an instance of {@link WkfDatiNotificaErroreProtocollazioneFlussoType.NotificaInviata }
     * 
     */
    public WkfDatiNotificaErroreProtocollazioneFlussoType.NotificaInviata createWkfDatiNotificaErroreProtocollazioneFlussoTypeNotificaInviata() {
        return new WkfDatiNotificaErroreProtocollazioneFlussoType.NotificaInviata();
    }

    /**
     * Create an instance of {@link ProtIdentificatoreProtocolloCollegatoType.Allacciante }
     * 
     */
    public ProtIdentificatoreProtocolloCollegatoType.Allacciante createProtIdentificatoreProtocolloCollegatoTypeAllacciante() {
        return new ProtIdentificatoreProtocolloCollegatoType.Allacciante();
    }

    /**
     * Create an instance of {@link WkfAzioneAggiornamentoProtocolloType.DatiModifica }
     * 
     */
    public WkfAzioneAggiornamentoProtocolloType.DatiModifica createWkfAzioneAggiornamentoProtocolloTypeDatiModifica() {
        return new WkfAzioneAggiornamentoProtocolloType.DatiModifica();
    }

    /**
     * Create an instance of {@link ProtCorrispondenteType.DatiAnnullamento }
     * 
     */
    public ProtCorrispondenteType.DatiAnnullamento createProtCorrispondenteTypeDatiAnnullamento() {
        return new ProtCorrispondenteType.DatiAnnullamento();
    }

    /**
     * Create an instance of {@link WkfAzioneCambioStatoProtocolloType.Entrata }
     * 
     */
    public WkfAzioneCambioStatoProtocolloType.Entrata createWkfAzioneCambioStatoProtocolloTypeEntrata() {
        return new WkfAzioneCambioStatoProtocolloType.Entrata();
    }

    /**
     * Create an instance of {@link WkfAzioneCambioStatoProtocolloType.Uscita }
     * 
     */
    public WkfAzioneCambioStatoProtocolloType.Uscita createWkfAzioneCambioStatoProtocolloTypeUscita() {
        return new WkfAzioneCambioStatoProtocolloType.Uscita();
    }

    /**
     * Create an instance of {@link ProtSpedizioneType.MezzoSpedizione.Elettronico }
     * 
     */
    public ProtSpedizioneType.MezzoSpedizione.Elettronico createProtSpedizioneTypeMezzoSpedizioneElettronico() {
        return new ProtSpedizioneType.MezzoSpedizione.Elettronico();
    }

    /**
     * Create an instance of {@link EmailMessageType.AllegatiMessaggio }
     * 
     */
    public EmailMessageType.AllegatiMessaggio createEmailMessageTypeAllegatiMessaggio() {
        return new EmailMessageType.AllegatiMessaggio();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Descrizione", scope = ProtTipologiaDocumentoType.class)
    public JAXBElement<String> createProtTipologiaDocumentoTypeDescrizione(String value) {
        return new JAXBElement<String>(_ProtTipologiaDocumentoTypeDescrizione_QNAME, String.class, ProtTipologiaDocumentoType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProtMetadatiType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "metadatiAssociati", scope = ProtAttributiEstesiType.class)
    public JAXBElement<ProtMetadatiType> createProtAttributiEstesiTypeMetadatiAssociati(ProtMetadatiType value) {
        return new JAXBElement<ProtMetadatiType>(_ProtAttributiEstesiTypeMetadatiAssociati_QNAME, ProtMetadatiType.class, ProtAttributiEstesiType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IdDocumento", scope = WkfDatiNotificaOperazioneType.class)
    public JAXBElement<String> createWkfDatiNotificaOperazioneTypeIdDocumento(String value) {
        return new JAXBElement<String>(_WkfDatiNotificaOperazioneTypeIdDocumento_QNAME, String.class, WkfDatiNotificaOperazioneType.class, value);
    }

}
