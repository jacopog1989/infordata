
package it.ibm.red.webservice.model.redservice.header.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CredenzialiUtenteRed complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CredenzialiUtenteRed">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idAoo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idGruppo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idUtente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CredenzialiUtenteRed", propOrder = {
    "idAoo",
    "idGruppo",
    "idUtente"
})
public class CredenzialiUtenteRed {

	/**
	 * Identificativo aoo.
	 */
    protected Integer idAoo;

    /**
     * Identificativo gruppo.
     */
    protected Integer idGruppo;

	/**
	 * Identificativo utente.
	 */
    @XmlElementRef(name = "idUtente", type = JAXBElement.class, required = false)
    protected JAXBElement<String> idUtente;

    /**
     * Gets the value of the idAoo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdAoo() {
        return idAoo;
    }

    /**
     * Sets the value of the idAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdAoo(Integer value) {
        this.idAoo = value;
    }

    /**
     * Gets the value of the idGruppo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdGruppo() {
        return idGruppo;
    }

    /**
     * Sets the value of the idGruppo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdGruppo(Integer value) {
        this.idGruppo = value;
    }

    /**
     * Gets the value of the idUtente property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIdUtente() {
        return idUtente;
    }

    /**
     * Sets the value of the idUtente property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIdUtente(JAXBElement<String> value) {
        this.idUtente = value;
    }

}
