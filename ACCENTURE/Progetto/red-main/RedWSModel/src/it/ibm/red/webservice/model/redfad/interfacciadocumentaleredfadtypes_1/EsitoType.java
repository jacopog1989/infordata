
package it.ibm.red.webservice.model.redfad.interfacciadocumentaleredfadtypes_1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for esito_type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="esito_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OK"/>
 *     &lt;enumeration value="ERRORE_DATI_NON_PRESENTI"/>
 *     &lt;enumeration value="ERRORE_NON_AUTORIZZATO"/>
 *     &lt;enumeration value="ERRORE_DATI_NON_DI_COMPETENZA"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "esito_type")
@XmlEnum
public enum EsitoType {


	/**
	 * Valore.
	 */
    OK,

	/**
	 * Valore.
	 */
    ERRORE_DATI_NON_PRESENTI,

	/**
	 * Valore.
	 */
    ERRORE_NON_AUTORIZZATO,

	/**
	 * Valore.
	 */
    ERRORE_DATI_NON_DI_COMPETENZA;

	/**
	 * Ottiene il nome della costante enum.
	 * @return nome della costante enum
	 */
    public String value() {
        return name();
    }

    /**
     * Ottiene il tipo di esito dal valore.
     * @param v valore
     * @return tipo di esito
     */
    public static EsitoType fromValue(String v) {
        return valueOf(v);
    }

}
