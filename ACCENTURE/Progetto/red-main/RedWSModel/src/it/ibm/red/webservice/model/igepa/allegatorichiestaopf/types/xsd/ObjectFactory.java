
package it.ibm.red.webservice.model.igepa.allegatorichiestaopf.types.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import it.ibm.red.webservice.model.igepa.types.xsd.Base64Binary;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.capgemini.nsd.filenet.ws.igepa.allegatorichiestaopf.types.xsd package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private static final QName _AllegatoRichiestaOPFTipologiaAllegato_QNAME = new QName("http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "tipologiaAllegato");
    private static final QName _AllegatoRichiestaOPFAnnoRichiesta_QNAME = new QName("http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "annoRichiesta");
    private static final QName _AllegatoRichiestaOPFDocumentoAllegato_QNAME = new QName("http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "documentoAllegato");
    private static final QName _AllegatoRichiestaOPFNumeroRichiesta_QNAME = new QName("http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "numeroRichiesta");
    private static final QName _AllegatoRichiestaOPFOggettoAllegato_QNAME = new QName("http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "oggettoAllegato");
    private static final QName _AllegatoRichiestaOPFNumeroProtocollo_QNAME = new QName("http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "numeroProtocollo");
    private static final QName _AllegatoRichiestaOPFDataProtocollo_QNAME = new QName("http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "dataProtocollo");
    private static final QName _AllegatoIgepaResponseAllegatoIgepaResponse_QNAME = new QName("http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "allegatoIgepaResponse");
    private static final QName _DataFormatTypeDataFormatType_QNAME = new QName("http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "dataFormatType");
    private static final QName _InvocationStatusValue_QNAME = new QName("http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "value");
    private static final QName _DocumentFileFileBase64_QNAME = new QName("http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "fileBase64");
    private static final QName _DocumentFileFileName_QNAME = new QName("http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "fileName");
    private static final QName _AllegatoIgepaRequestAllegatoIgepaRequest_QNAME = new QName("http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "allegatoIgepaRequest");
    private static final QName _BaseResponseStatus_QNAME = new QName("http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "status");
    private static final QName _BaseResponseDescription_QNAME = new QName("http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "description");
    private static final QName _AnnoFormatTypeAnnoFormatType_QNAME = new QName("http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "annoFormatType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.capgemini.nsd.filenet.ws.igepa.allegatorichiestaopf.types.xsd
     * 
     */
    public ObjectFactory() {
    	// Costruttore intenzionalmente vuoto.
    }

    /**
     * Create an instance of {@link BaseResponse }
     * 
     */
    public BaseResponse createBaseResponse() {
        return new BaseResponse();
    }

    /**
     * Create an instance of {@link DocumentFile }
     * 
     */
    public DocumentFile createDocumentFile() {
        return new DocumentFile();
    }

    /**
     * Create an instance of {@link AllegatoIgepaResponse }
     * 
     */
    public AllegatoIgepaResponse createAllegatoIgepaResponse() {
        return new AllegatoIgepaResponse();
    }

    /**
     * Create an instance of {@link InvocationStatus }
     * 
     */
    public InvocationStatus createInvocationStatus() {
        return new InvocationStatus();
    }

    /**
     * Create an instance of {@link AnnoFormatType }
     * 
     */
    public AnnoFormatType createAnnoFormatType() {
        return new AnnoFormatType();
    }

    /**
     * Create an instance of {@link AllegatoRichiestaOPF }
     * 
     */
    public AllegatoRichiestaOPF createAllegatoRichiestaOPF() {
        return new AllegatoRichiestaOPF();
    }

    /**
     * Create an instance of {@link AllegatoIgepaRequest }
     * 
     */
    public AllegatoIgepaRequest createAllegatoIgepaRequest() {
        return new AllegatoIgepaRequest();
    }

    /**
     * Create an instance of {@link DataFormatType }
     * 
     */
    public DataFormatType createDataFormatType() {
        return new DataFormatType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "tipologiaAllegato", scope = AllegatoRichiestaOPF.class)
    public JAXBElement<Integer> createAllegatoRichiestaOPFTipologiaAllegato(Integer value) {
        return new JAXBElement<Integer>(_AllegatoRichiestaOPFTipologiaAllegato_QNAME, Integer.class, AllegatoRichiestaOPF.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnnoFormatType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "annoRichiesta", scope = AllegatoRichiestaOPF.class)
    public JAXBElement<AnnoFormatType> createAllegatoRichiestaOPFAnnoRichiesta(AnnoFormatType value) {
        return new JAXBElement<AnnoFormatType>(_AllegatoRichiestaOPFAnnoRichiesta_QNAME, AnnoFormatType.class, AllegatoRichiestaOPF.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "documentoAllegato", scope = AllegatoRichiestaOPF.class)
    public JAXBElement<DocumentFile> createAllegatoRichiestaOPFDocumentoAllegato(DocumentFile value) {
        return new JAXBElement<DocumentFile>(_AllegatoRichiestaOPFDocumentoAllegato_QNAME, DocumentFile.class, AllegatoRichiestaOPF.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "numeroRichiesta", scope = AllegatoRichiestaOPF.class)
    public JAXBElement<Integer> createAllegatoRichiestaOPFNumeroRichiesta(Integer value) {
        return new JAXBElement<Integer>(_AllegatoRichiestaOPFNumeroRichiesta_QNAME, Integer.class, AllegatoRichiestaOPF.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "oggettoAllegato", scope = AllegatoRichiestaOPF.class)
    public JAXBElement<String> createAllegatoRichiestaOPFOggettoAllegato(String value) {
        return new JAXBElement<String>(_AllegatoRichiestaOPFOggettoAllegato_QNAME, String.class, AllegatoRichiestaOPF.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "numeroProtocollo", scope = AllegatoRichiestaOPF.class)
    public JAXBElement<Integer> createAllegatoRichiestaOPFNumeroProtocollo(Integer value) {
        return new JAXBElement<Integer>(_AllegatoRichiestaOPFNumeroProtocollo_QNAME, Integer.class, AllegatoRichiestaOPF.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataFormatType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "dataProtocollo", scope = AllegatoRichiestaOPF.class)
    public JAXBElement<DataFormatType> createAllegatoRichiestaOPFDataProtocollo(DataFormatType value) {
        return new JAXBElement<DataFormatType>(_AllegatoRichiestaOPFDataProtocollo_QNAME, DataFormatType.class, AllegatoRichiestaOPF.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "allegatoIgepaResponse", scope = AllegatoIgepaResponse.class)
    public JAXBElement<BaseResponse> createAllegatoIgepaResponseAllegatoIgepaResponse(BaseResponse value) {
        return new JAXBElement<BaseResponse>(_AllegatoIgepaResponseAllegatoIgepaResponse_QNAME, BaseResponse.class, AllegatoIgepaResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "dataFormatType", scope = DataFormatType.class)
    public JAXBElement<String> createDataFormatTypeDataFormatType(String value) {
        return new JAXBElement<String>(_DataFormatTypeDataFormatType_QNAME, String.class, DataFormatType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "value", scope = InvocationStatus.class)
    public JAXBElement<String> createInvocationStatusValue(String value) {
        return new JAXBElement<String>(_InvocationStatusValue_QNAME, String.class, InvocationStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Base64Binary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "fileBase64", scope = DocumentFile.class)
    public JAXBElement<Base64Binary> createDocumentFileFileBase64(Base64Binary value) {
        return new JAXBElement<Base64Binary>(_DocumentFileFileBase64_QNAME, Base64Binary.class, DocumentFile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "fileName", scope = DocumentFile.class)
    public JAXBElement<String> createDocumentFileFileName(String value) {
        return new JAXBElement<String>(_DocumentFileFileName_QNAME, String.class, DocumentFile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AllegatoRichiestaOPF }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "allegatoIgepaRequest", scope = AllegatoIgepaRequest.class)
    public JAXBElement<AllegatoRichiestaOPF> createAllegatoIgepaRequestAllegatoIgepaRequest(AllegatoRichiestaOPF value) {
        return new JAXBElement<AllegatoRichiestaOPF>(_AllegatoIgepaRequestAllegatoIgepaRequest_QNAME, AllegatoRichiestaOPF.class, AllegatoIgepaRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvocationStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "status", scope = BaseResponse.class)
    public JAXBElement<InvocationStatus> createBaseResponseStatus(InvocationStatus value) {
        return new JAXBElement<InvocationStatus>(_BaseResponseStatus_QNAME, InvocationStatus.class, BaseResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "description", scope = BaseResponse.class)
    public JAXBElement<String> createBaseResponseDescription(String value) {
        return new JAXBElement<String>(_BaseResponseDescription_QNAME, String.class, BaseResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.allegatorichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "annoFormatType", scope = AnnoFormatType.class)
    public JAXBElement<String> createAnnoFormatTypeAnnoFormatType(String value) {
        return new JAXBElement<String>(_AnnoFormatTypeAnnoFormatType_QNAME, String.class, AnnoFormatType.class, value);
    }

}
