package it.ibm.red.webservice.model.interfaccianps.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ParametersAzioneRispostaAutomatica implements Serializable {

	private static final long serialVersionUID = -5079325635656714311L;

	/**
	 * Informazioni protocollo.
	 */
	private InfoProtocollo allaccioPrincipale;

	/**
	 * Ulteriori allacci.
	 */
	private List<InfoProtocollo> ulterioriAllacci;

	/**
	 * Protocollatore.
	 */
	private String protocollatore;

	/**
	 * Sistema ausiliario.
	 */
	private String sistemaAusiliario;
	
	/**
	 * Tipo azione.
	 */
	private String tipoAzione;

	/**
	 * DTO per le informazioni del protocollo.
	 */
	public static class InfoProtocollo implements Serializable {

		private static final long serialVersionUID = 327159415789416426L;

		/**
		 * Id protocollo.
		 */
		private String idProtocollo;
		
		/**
		 * Numero protocollo.
		 */
		private Integer numeroProtocollo;
		
		/**
		 * Data protocollo.
		 */
		private Date dataProtocollo;
		
		/**
		 * Anno protocollo.
		 */
		private Integer annoProtocollo;
		
		/**
		 * Stato protocollo.
		 */
		private String statoProtocollo;
		
		/**
		 * Tipo protocollo.
		 */
		private Integer tipoProtocollo;

		/**
		 * Costruttore di default.
		 * @param idProtocollo
		 * @param numeroProtocollo
		 * @param dataProtocollo
		 * @param annoProtocollo
		 * @param statoProtocollo
		 * @param tipoProtocollo
		 */
		public InfoProtocollo(String idProtocollo, Integer numeroProtocollo, Date dataProtocollo,
				Integer annoProtocollo, String statoProtocollo, Integer tipoProtocollo) {
			super();
			this.idProtocollo = idProtocollo;
			this.numeroProtocollo = numeroProtocollo;
			this.dataProtocollo = dataProtocollo;
			this.annoProtocollo = annoProtocollo;
			this.statoProtocollo = statoProtocollo;
			this.tipoProtocollo = tipoProtocollo;
		}

		/**
		 * @return idProtocollo
		 */
		public String getIdProtocollo() {
			return idProtocollo;
		}

		/**
		 * @return numeroProtocollo
		 */
		public Integer getNumeroProtocollo() {
			return numeroProtocollo;
		}

		/**
		 * @return dataProtocollo
		 */
		public Date getDataProtocollo() {
			return dataProtocollo;
		}

		/**
		 * @return annoProtocollo
		 */
		public Integer getAnnoProtocollo() {
			return annoProtocollo;
		}

		/**
		 * @return statoProtocollo
		 */
		public String getStatoProtocollo() {
			return statoProtocollo;
		}

		/**
		 * @return tipoProtocollo
		 */
		public Integer getTipoProtocollo() {
			return tipoProtocollo;
		}
	}

	/**
	 * Costruttore di default.
	 * @param idProtocolloRisposta
	 * @param numeroProtocolloRisposta
	 * @param dataProtocolloRisposta
	 * @param annoProtocolloRisposta
	 * @param statoProtocolloRisposta
	 * @param protocollatore
	 * @param sistemaAusiliario
	 */
	public ParametersAzioneRispostaAutomatica(String idProtocollo, Integer numeroProtocollo,	Date dataProtocollo, 
			Integer annoProtocollo, String statoProtocollo, Integer tipoProtocollo, String protocollatore, String sistemaAusiliario,
			String tipoAzione) {
		super();
		this.allaccioPrincipale = new InfoProtocollo(idProtocollo, numeroProtocollo, dataProtocollo, annoProtocollo, statoProtocollo, tipoProtocollo);
		this.protocollatore = protocollatore;
		this.sistemaAusiliario = sistemaAusiliario;
		this.tipoAzione = tipoAzione;
	}

	/**
	 * @return allaccioPrincipale
	 */
	public InfoProtocollo getAllaccioPrincipale() {
		return allaccioPrincipale;
	}

	/**
	 * Imposta l'allaccio principale.
	 * @param allaccioPrincipale
	 */
	public void setAllaccioPrincipale(final InfoProtocollo allaccioPrincipale) {
		this.allaccioPrincipale = allaccioPrincipale;
	}

	/**
	 * @return ulterioriAllacci
	 */
	public List<InfoProtocollo> getUlterioriAllacci() {
		if (ulterioriAllacci == null) {
			ulterioriAllacci = new ArrayList<>();
		}
		return ulterioriAllacci;
	}

	/**
	 * Inserisce le informazioni di protocollo in input come ulteriore allaccio.
	 * @param idProtocollo
	 * @param numeroProtocollo
	 * @param dataProtocollo
	 * @param annoProtocollo
	 * @param statoProtocollo
	 * @param tipoProtocollo
	 */
	public void addUlterioreAllaccio(String idProtocollo, Integer numeroProtocollo,	Date dataProtocollo, 
			Integer annoProtocollo, String statoProtocollo, Integer tipoProtocollo) {
		getUlterioriAllacci().add(
				new InfoProtocollo(idProtocollo, numeroProtocollo, dataProtocollo, 
						annoProtocollo, statoProtocollo, tipoProtocollo));
	}

	/**
	 * @return protocollatore
	 */
	public String getProtocollatore() {
		return protocollatore;
	}


	/**
	 * Imposta il protocollatore.
	 * @param protocollatore
	 */
	public void setProtocollatore(final String protocollatore) {
		this.protocollatore = protocollatore;
	}


	/**
	 * @return sistemaAusiliario
	 */
	public String getSistemaAusiliario() {
		return sistemaAusiliario;
	}


	/**
	 * Imposta il sistema ausiliario.
	 * @param sistemaAusiliario
	 */
	public void setSistemaAusiliario(final String sistemaAusiliario) {
		this.sistemaAusiliario = sistemaAusiliario;
	}
	
	/**
	 * @return tipoAzione
	 */
	public String getTipoAzione() {
		return tipoAzione;
	}


	/**
	 * Imposta il tipo azione.
	 * @param tipoAzione
	 */
	public void setTipoAzione(final String tipoAzione) {
		this.tipoAzione = tipoAzione;
	}
	
}