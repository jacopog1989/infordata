
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per doc_validazioneFirma_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="doc_validazioneFirma_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NON_VALIDATA"/>
 *     &lt;enumeration value="VALIDATA_CON_SUCCESSO"/>
 *     &lt;enumeration value="VALIDATA_CON_ERRORE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "doc_validazioneFirma_type")
@XmlEnum
public enum DocValidazioneFirmaType {

    NON_VALIDATA,
    VALIDATA_CON_SUCCESSO,
    VALIDATA_CON_ERRORE;

    public String value() {
        return name();
    }

    public static DocValidazioneFirmaType fromValue(String v) {
        return valueOf(v);
    }

}
