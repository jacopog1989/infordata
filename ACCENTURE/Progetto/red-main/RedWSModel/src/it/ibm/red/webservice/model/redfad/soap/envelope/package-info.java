/**
 * Envelope soap.
 */

@javax.xml.bind.annotation.XmlSchema(namespace = "http://schemas.xmlsoap.org/soap/envelope/")
package it.ibm.red.webservice.model.redfad.soap.envelope;
