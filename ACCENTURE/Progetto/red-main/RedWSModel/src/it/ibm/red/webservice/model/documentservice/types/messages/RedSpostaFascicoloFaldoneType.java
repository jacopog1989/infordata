//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.01.28 at 06:39:59 PM CET 
//


package it.ibm.red.webservice.model.documentservice.types.messages;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for redSpostaFascicoloFaldoneType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="redSpostaFascicoloFaldoneType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://messages.types.documentservice.model.webservice.red.ibm.it}BaseRequestType">
 *       &lt;sequence>
 *         &lt;element name="idFascicolo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idFaldoneOld" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idFaldoneNew" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idUtente" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "redSpostaFascicoloFaldoneType", propOrder = {
    "idFascicolo",
    "idFaldoneOld",
    "idFaldoneNew",
    "idUtente"
})
public class RedSpostaFascicoloFaldoneType extends BaseRequestType {

	/**
	 * Fascicolo.
	 */
    protected Integer idFascicolo;

	/**
	 * Faldone old.
	 */
    protected Integer idFaldoneOld;

	/**
	 * Faldone new.
	 */
    protected Integer idFaldoneNew;

	/**
	 * Utente.
	 */
    protected Integer idUtente;

    /**
     * Gets the value of the idFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdFascicolo() {
        return idFascicolo;
    }

    /**
     * Sets the value of the idFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdFascicolo(Integer value) {
        this.idFascicolo = value;
    }

    /**
     * Gets the value of the idFaldoneOld property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdFaldoneOld() {
        return idFaldoneOld;
    }

    /**
     * Sets the value of the idFaldoneOld property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdFaldoneOld(Integer value) {
        this.idFaldoneOld = value;
    }

    /**
     * Gets the value of the idFaldoneNew property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdFaldoneNew() {
        return idFaldoneNew;
    }

    /**
     * Sets the value of the idFaldoneNew property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdFaldoneNew(Integer value) {
        this.idFaldoneNew = value;
    }

    /**
     * Gets the value of the idUtente property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdUtente() {
        return idUtente;
    }

    /**
     * Sets the value of the idUtente property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdUtente(Integer value) {
        this.idUtente = value;
    }

}
