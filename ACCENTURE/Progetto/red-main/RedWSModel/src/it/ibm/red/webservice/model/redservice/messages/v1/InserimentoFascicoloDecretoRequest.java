
package it.ibm.red.webservice.model.redservice.messages.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import it.ibm.red.webservice.model.redservice.header.v1.CredenzialiUtenteRed;
import it.ibm.red.webservice.model.redservice.header.v1.CredenzialiWSRed;
import it.ibm.red.webservice.model.redservice.types.v1.DocumentoRed;
import it.ibm.red.webservice.model.redservice.types.v1.IdentificativoUnitaDocumentaleFEPATypeRed;
import it.ibm.red.webservice.model.redservice.types.v1.MapRed;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="credenzialiWS" type="{urn:red:header:v1}CredenzialiWSRed" minOccurs="0"/>
 *         &lt;element name="credenzialiUtente" type="{urn:red:header:v1}CredenzialiUtenteRed" minOccurs="0"/>
 *         &lt;element name="documento" type="{urn:red:types:v1}DocumentoRed" minOccurs="0"/>
 *         &lt;element name="fascicoliFEPA" type="{urn:red:types:v1}IdentificativoUnitaDocumentaleFEPATypeRed" maxOccurs="unbounded"/>
 *         &lt;element name="metadatiAssegnazione" type="{urn:red:types:v1}MapRed" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "credenzialiWS",
    "credenzialiUtente",
    "documento",
    "fascicoliFEPA",
    "metadatiAssegnazione"
})
@XmlRootElement(name = "inserimentoFascicoloDecretoRequest")
public class InserimentoFascicoloDecretoRequest {

	/**
	 * Credenziali WS.
	 */
    @XmlElementRef(name = "credenzialiWS", type = JAXBElement.class, required = false)
    protected JAXBElement<CredenzialiWSRed> credenzialiWS;

    /**
     * Credenziali utente.
     */
    @XmlElementRef(name = "credenzialiUtente", type = JAXBElement.class, required = false)
    protected JAXBElement<CredenzialiUtenteRed> credenzialiUtente;

    /**
     * Documento.
     */
    @XmlElementRef(name = "documento", type = JAXBElement.class, required = false)
    protected JAXBElement<DocumentoRed> documento;

    /**
     * Fascicoli FEPA.
     */
    @XmlElement(required = true, nillable = true)
    protected List<IdentificativoUnitaDocumentaleFEPATypeRed> fascicoliFEPA;

    /**
     * Metadati.
     */
    @XmlElementRef(name = "metadatiAssegnazione", type = JAXBElement.class, required = false)
    protected JAXBElement<MapRed> metadatiAssegnazione;

    /**
     * Gets the value of the credenzialiWS property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CredenzialiWSRed }{@code >}
     *     
     */
    public JAXBElement<CredenzialiWSRed> getCredenzialiWS() {
        return credenzialiWS;
    }

    /**
     * Sets the value of the credenzialiWS property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CredenzialiWSRed }{@code >}
     *     
     */
    public void setCredenzialiWS(JAXBElement<CredenzialiWSRed> value) {
        this.credenzialiWS = value;
    }

    /**
     * Gets the value of the credenzialiUtente property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CredenzialiUtenteRed }{@code >}
     *     
     */
    public JAXBElement<CredenzialiUtenteRed> getCredenzialiUtente() {
        return credenzialiUtente;
    }

    /**
     * Sets the value of the credenzialiUtente property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CredenzialiUtenteRed }{@code >}
     *     
     */
    public void setCredenzialiUtente(JAXBElement<CredenzialiUtenteRed> value) {
        this.credenzialiUtente = value;
    }

    /**
     * Gets the value of the documento property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DocumentoRed }{@code >}
     *     
     */
    public JAXBElement<DocumentoRed> getDocumento() {
        return documento;
    }

    /**
     * Sets the value of the documento property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DocumentoRed }{@code >}
     *     
     */
    public void setDocumento(JAXBElement<DocumentoRed> value) {
        this.documento = value;
    }

    /**
     * Gets the value of the fascicoliFEPA property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fascicoliFEPA property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFascicoliFEPA().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IdentificativoUnitaDocumentaleFEPATypeRed }
     * 
     * 
     */
    public List<IdentificativoUnitaDocumentaleFEPATypeRed> getFascicoliFEPA() {
        if (fascicoliFEPA == null) {
            fascicoliFEPA = new ArrayList<>();
        }
        return this.fascicoliFEPA;
    }

    /**
     * Gets the value of the metadatiAssegnazione property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link MapRed }{@code >}
     *     
     */
    public JAXBElement<MapRed> getMetadatiAssegnazione() {
        return metadatiAssegnazione;
    }

    /**
     * Sets the value of the metadatiAssegnazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link MapRed }{@code >}
     *     
     */
    public void setMetadatiAssegnazione(JAXBElement<MapRed> value) {
        this.metadatiAssegnazione = value;
    }

}
