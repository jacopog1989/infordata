
package it.ibm.red.webservice.model.igepa.inviarichiestaopf.types.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import it.ibm.red.webservice.model.igepa.types.xsd.Base64Binary;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.capgemini.nsd.filenet.ws.igepa.inviarichiestaopf.types.xsd package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private static final QName _RichiestaOPFIgepaRequestRichiestaOPFIgepaRequest_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "richiestaOPFIgepaRequest");
    private static final QName _DocumentFileFileName_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "fileName");
    private static final QName _DocumentFileFileBase64_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "fileBase64");
    private static final QName _RichiestaOPFIgepaResponseRichiestaOPFIgepaResponse_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "richiestaOPFIgepaResponse");
    private static final QName _EnteMittenteFormatTypeEnteMittenteFormatType_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "enteMittenteFormatType");
    private static final QName _BaseResponseStatus_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "status");
    private static final QName _BaseResponseDescription_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "description");
    private static final QName _DocumentoRichiestaOPFIgepaTipoDocumento_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "tipoDocumento");
    private static final QName _DocumentoRichiestaOPFIgepaIndiceClassificazione_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "indiceClassificazione");
    private static final QName _DocumentoRichiestaOPFIgepaEnteMittente_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "enteMittente");
    private static final QName _DocumentoRichiestaOPFIgepaNumeroRichiesta_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "numeroRichiesta");
    private static final QName _DocumentoRichiestaOPFIgepaIdNodoDestinatario_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "idNodoDestinatario");
    private static final QName _DocumentoRichiestaOPFIgepaNumeroProtocollo_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "numeroProtocollo");
    private static final QName _DocumentoRichiestaOPFIgepaOggettoRichiesta_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "oggettoRichiesta");
    private static final QName _DocumentoRichiestaOPFIgepaNumeroConto_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "numeroConto");
    private static final QName _DocumentoRichiestaOPFIgepaDocumentoPrincipale_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "documentoPrincipale");
    private static final QName _DocumentoRichiestaOPFIgepaDataProtocollo_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "dataProtocollo");
    private static final QName _DocumentoRichiestaOPFIgepaIdUtenteDestinatario_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "idUtenteDestinatario");
    private static final QName _DocumentoRichiestaOPFIgepaAnnoRichiesta_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "annoRichiesta");
    private static final QName _DocumentoRichiestaOPFIgepaTipoProcedimento_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "tipoProcedimento");
    private static final QName _DocumentoRichiestaOPFIgepaSezione_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "sezione");
    private static final QName _AnnoFormatTypeAnnoFormatType_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "annoFormatType");
    private static final QName _DataFormatTypeDataFormatType_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "dataFormatType");
    private static final QName _InvocationStatusValue_QNAME = new QName("http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", "value");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.capgemini.nsd.filenet.ws.igepa.inviarichiestaopf.types.xsd
     * 
     */
    public ObjectFactory() {
    	// Costruttore intenzionalmente vuoto.
    }

    /**
     * Create an instance of {@link DocumentoRichiestaOPFIgepa }
     * 
     */
    public DocumentoRichiestaOPFIgepa createDocumentoRichiestaOPFIgepa() {
        return new DocumentoRichiestaOPFIgepa();
    }

    /**
     * Create an instance of {@link BaseResponse }
     * 
     */
    public BaseResponse createBaseResponse() {
        return new BaseResponse();
    }

    /**
     * Create an instance of {@link DocumentFile }
     * 
     */
    public DocumentFile createDocumentFile() {
        return new DocumentFile();
    }

    /**
     * Create an instance of {@link InvocationStatus }
     * 
     */
    public InvocationStatus createInvocationStatus() {
        return new InvocationStatus();
    }

    /**
     * Create an instance of {@link AnnoFormatType }
     * 
     */
    public AnnoFormatType createAnnoFormatType() {
        return new AnnoFormatType();
    }

    /**
     * Create an instance of {@link EnteMittenteFormatType }
     * 
     */
    public EnteMittenteFormatType createEnteMittenteFormatType() {
        return new EnteMittenteFormatType();
    }

    /**
     * Create an instance of {@link RichiestaOPFIgepaResponse }
     * 
     */
    public RichiestaOPFIgepaResponse createRichiestaOPFIgepaResponse() {
        return new RichiestaOPFIgepaResponse();
    }

    /**
     * Create an instance of {@link RichiestaOPFIgepaRequest }
     * 
     */
    public RichiestaOPFIgepaRequest createRichiestaOPFIgepaRequest() {
        return new RichiestaOPFIgepaRequest();
    }

    /**
     * Create an instance of {@link DataFormatType }
     * 
     */
    public DataFormatType createDataFormatType() {
        return new DataFormatType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoRichiestaOPFIgepa }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "richiestaOPFIgepaRequest", scope = RichiestaOPFIgepaRequest.class)
    public JAXBElement<DocumentoRichiestaOPFIgepa> createRichiestaOPFIgepaRequestRichiestaOPFIgepaRequest(DocumentoRichiestaOPFIgepa value) {
        return new JAXBElement<DocumentoRichiestaOPFIgepa>(_RichiestaOPFIgepaRequestRichiestaOPFIgepaRequest_QNAME, DocumentoRichiestaOPFIgepa.class, RichiestaOPFIgepaRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "fileName", scope = DocumentFile.class)
    public JAXBElement<String> createDocumentFileFileName(String value) {
        return new JAXBElement<String>(_DocumentFileFileName_QNAME, String.class, DocumentFile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Base64Binary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "fileBase64", scope = DocumentFile.class)
    public JAXBElement<Base64Binary> createDocumentFileFileBase64(Base64Binary value) {
        return new JAXBElement<Base64Binary>(_DocumentFileFileBase64_QNAME, Base64Binary.class, DocumentFile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "richiestaOPFIgepaResponse", scope = RichiestaOPFIgepaResponse.class)
    public JAXBElement<BaseResponse> createRichiestaOPFIgepaResponseRichiestaOPFIgepaResponse(BaseResponse value) {
        return new JAXBElement<BaseResponse>(_RichiestaOPFIgepaResponseRichiestaOPFIgepaResponse_QNAME, BaseResponse.class, RichiestaOPFIgepaResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "enteMittenteFormatType", scope = EnteMittenteFormatType.class)
    public JAXBElement<String> createEnteMittenteFormatTypeEnteMittenteFormatType(String value) {
        return new JAXBElement<String>(_EnteMittenteFormatTypeEnteMittenteFormatType_QNAME, String.class, EnteMittenteFormatType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvocationStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "status", scope = BaseResponse.class)
    public JAXBElement<InvocationStatus> createBaseResponseStatus(InvocationStatus value) {
        return new JAXBElement<InvocationStatus>(_BaseResponseStatus_QNAME, InvocationStatus.class, BaseResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "description", scope = BaseResponse.class)
    public JAXBElement<String> createBaseResponseDescription(String value) {
        return new JAXBElement<String>(_BaseResponseDescription_QNAME, String.class, BaseResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "tipoDocumento", scope = DocumentoRichiestaOPFIgepa.class)
    public JAXBElement<Integer> createDocumentoRichiestaOPFIgepaTipoDocumento(Integer value) {
        return new JAXBElement<Integer>(_DocumentoRichiestaOPFIgepaTipoDocumento_QNAME, Integer.class, DocumentoRichiestaOPFIgepa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "indiceClassificazione", scope = DocumentoRichiestaOPFIgepa.class)
    public JAXBElement<String> createDocumentoRichiestaOPFIgepaIndiceClassificazione(String value) {
        return new JAXBElement<String>(_DocumentoRichiestaOPFIgepaIndiceClassificazione_QNAME, String.class, DocumentoRichiestaOPFIgepa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnteMittenteFormatType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "enteMittente", scope = DocumentoRichiestaOPFIgepa.class)
    public JAXBElement<EnteMittenteFormatType> createDocumentoRichiestaOPFIgepaEnteMittente(EnteMittenteFormatType value) {
        return new JAXBElement<EnteMittenteFormatType>(_DocumentoRichiestaOPFIgepaEnteMittente_QNAME, EnteMittenteFormatType.class, DocumentoRichiestaOPFIgepa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "numeroRichiesta", scope = DocumentoRichiestaOPFIgepa.class)
    public JAXBElement<Integer> createDocumentoRichiestaOPFIgepaNumeroRichiesta(Integer value) {
        return new JAXBElement<Integer>(_DocumentoRichiestaOPFIgepaNumeroRichiesta_QNAME, Integer.class, DocumentoRichiestaOPFIgepa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "idNodoDestinatario", scope = DocumentoRichiestaOPFIgepa.class)
    public JAXBElement<Integer> createDocumentoRichiestaOPFIgepaIdNodoDestinatario(Integer value) {
        return new JAXBElement<Integer>(_DocumentoRichiestaOPFIgepaIdNodoDestinatario_QNAME, Integer.class, DocumentoRichiestaOPFIgepa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "numeroProtocollo", scope = DocumentoRichiestaOPFIgepa.class)
    public JAXBElement<Integer> createDocumentoRichiestaOPFIgepaNumeroProtocollo(Integer value) {
        return new JAXBElement<Integer>(_DocumentoRichiestaOPFIgepaNumeroProtocollo_QNAME, Integer.class, DocumentoRichiestaOPFIgepa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "oggettoRichiesta", scope = DocumentoRichiestaOPFIgepa.class)
    public JAXBElement<String> createDocumentoRichiestaOPFIgepaOggettoRichiesta(String value) {
        return new JAXBElement<String>(_DocumentoRichiestaOPFIgepaOggettoRichiesta_QNAME, String.class, DocumentoRichiestaOPFIgepa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "numeroConto", scope = DocumentoRichiestaOPFIgepa.class)
    public JAXBElement<String> createDocumentoRichiestaOPFIgepaNumeroConto(String value) {
        return new JAXBElement<String>(_DocumentoRichiestaOPFIgepaNumeroConto_QNAME, String.class, DocumentoRichiestaOPFIgepa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "documentoPrincipale", scope = DocumentoRichiestaOPFIgepa.class)
    public JAXBElement<DocumentFile> createDocumentoRichiestaOPFIgepaDocumentoPrincipale(DocumentFile value) {
        return new JAXBElement<DocumentFile>(_DocumentoRichiestaOPFIgepaDocumentoPrincipale_QNAME, DocumentFile.class, DocumentoRichiestaOPFIgepa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataFormatType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "dataProtocollo", scope = DocumentoRichiestaOPFIgepa.class)
    public JAXBElement<DataFormatType> createDocumentoRichiestaOPFIgepaDataProtocollo(DataFormatType value) {
        return new JAXBElement<DataFormatType>(_DocumentoRichiestaOPFIgepaDataProtocollo_QNAME, DataFormatType.class, DocumentoRichiestaOPFIgepa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "idUtenteDestinatario", scope = DocumentoRichiestaOPFIgepa.class)
    public JAXBElement<Integer> createDocumentoRichiestaOPFIgepaIdUtenteDestinatario(Integer value) {
        return new JAXBElement<Integer>(_DocumentoRichiestaOPFIgepaIdUtenteDestinatario_QNAME, Integer.class, DocumentoRichiestaOPFIgepa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnnoFormatType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "annoRichiesta", scope = DocumentoRichiestaOPFIgepa.class)
    public JAXBElement<AnnoFormatType> createDocumentoRichiestaOPFIgepaAnnoRichiesta(AnnoFormatType value) {
        return new JAXBElement<AnnoFormatType>(_DocumentoRichiestaOPFIgepaAnnoRichiesta_QNAME, AnnoFormatType.class, DocumentoRichiestaOPFIgepa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "tipoProcedimento", scope = DocumentoRichiestaOPFIgepa.class)
    public JAXBElement<Integer> createDocumentoRichiestaOPFIgepaTipoProcedimento(Integer value) {
        return new JAXBElement<Integer>(_DocumentoRichiestaOPFIgepaTipoProcedimento_QNAME, Integer.class, DocumentoRichiestaOPFIgepa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "sezione", scope = DocumentoRichiestaOPFIgepa.class)
    public JAXBElement<String> createDocumentoRichiestaOPFIgepaSezione(String value) {
        return new JAXBElement<String>(_DocumentoRichiestaOPFIgepaSezione_QNAME, String.class, DocumentoRichiestaOPFIgepa.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "annoFormatType", scope = AnnoFormatType.class)
    public JAXBElement<String> createAnnoFormatTypeAnnoFormatType(String value) {
        return new JAXBElement<String>(_AnnoFormatTypeAnnoFormatType_QNAME, String.class, AnnoFormatType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "dataFormatType", scope = DataFormatType.class)
    public JAXBElement<String> createDataFormatTypeDataFormatType(String value) {
        return new JAXBElement<String>(_DataFormatTypeDataFormatType_QNAME, String.class, DataFormatType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://types.inviarichiestaopf.igepa.ws.filenet.nsd.capgemini.com/xsd", name = "value", scope = InvocationStatus.class)
    public JAXBElement<String> createInvocationStatusValue(String value) {
        return new JAXBElement<String>(_InvocationStatusValue_QNAME, String.class, InvocationStatus.class, value);
    }

}
