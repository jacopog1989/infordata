package it.ibm.red.webservice.model.documentservice.dto;

public enum ErrorCode {
	
	/**
	 * Valore.
	 */
	NON_IMPLEMENTATO(1, "Il servizio non è ancora stato implementato"),
	
	/**
	 * Valore.
	 */
	ID_CLIENT_NON_PRESENTE(2, "Il client (ID_CLIENT) deve essere specificato nelle credenziali"),
	
	/**
	 * Valore.
	 */
	PWD_CLIENT_NON_PRESENTE(3, "La password del client deve esseere specificata nelle credenziali"),
	
	/**
	 * Valore.
	 */
	DESTINATARIO_ID_NON_PRESENTE(4, "Almeno uno tra ID Destinatario e ID Ufficio deve essere presente se non si valorizza il contatto"),
	
	/**
	 * Valore.
	 */
	DESTINATARIO_TIPO_ERRATO(5, "Il tipo di destinatario deve coincidere con le altre informazioni inserite e può assumere solo i valori: PEO|PEC|CARTACEO|INTERNO"),
	
	/**
	 * Valore.
	 */
	DESTINATARIO_TIPO_SPEDIZIONE_ERRATO(6, "Il tipo di spedizione del destinatario può assumere solo i valori: TO|CC"),
	
	/**
	 * Valore.
	 */
	DESTINATARIO_NOME_RAGIONE_SOCIALE_NON_PRESENTE(7, "Il nome/ragione sociale del destinatario deve essere presente"),
	
	/**
	 * Valore.
	 */
	DESTINATARIO_TIPO_PERSONA_NON_PRESENTE_ERRATO(8, "Il tipo persona del destinatario deve essere presente e può assumere solo i valori: F|G"),
	
	/**
	 * Valore.
	 */
	DESTINATARIO_COGNOME_NON_PRESENTE(9, "Il cognome del destinatario deve essere presente"),
	
	/**
	 * Valore.
	 */
	DESTINATARIO_MAIL_NON_PRESENTE(10, "Inserire almeno un indirizzo e-mail tra PEO e PEC"),
	
	/**
	 * Valore.
	 */
	FORMATO_DOC_NON_COERENTE(11, "Il formato indicato per il documento e l'estensione del file non sono coerenti"),
	
	/**
	 * Valore.
	 */
	FORMATO_DOC_NON_GESTITO(12, "Il formato del documento non può essere gestito: sono ammessi MSWORD e PDF"),
	
	/**
	 * Valore.
	 */
	CONTENT_TYPE_NON_PRESENTE(13, "Il content type del documento (DOCTYPE) deve essere presente"),

	/**
	 * Valore.
	 */
	CONTENT_NON_PRESENTE(14, "Il content deve essere presente"),

	/**
	 * Valore.
	 */
	NOME_FILE_NON_PRESENTE(15, "Il nome del file deve essere presente"),

	/**
	 * Valore.
	 */
	ASSEGNATARIO_NON_PRESENTE(16, "L'assegnatario deve essere presente"),

	/**
	 * Valore.
	 */
	ID_ITER_NON_PRESENTE(17, "L'iter (ID_ITER) deve essere presente"),

	/**
	 * Valore.
	 */
	ID_TIPO_ASSEGNAZIONE_NON_PRESENTE(18, "Il tipo di assegnazione (ID_TIPO_ASSEGNAZIONE) deve essere presente"),

	/**
	 * Valore.
	 */
	ID_UTENTE_CREATORE_NON_PRESENTE(19, "L'utente creatore (ID_UTENTE_CREATORE) deve essere presente"),

	/**
	 * Valore.
	 */
	ID_UFFICIO_CREATORE_NON_PRESENTE(10, "L'ufficio creatore (ID_UFFICIO_CREATORE) deve essere presente"),
	
	/**
	 * Valore.
	 */
	OGGETTO_NON_PRESENTE(21, "L'oggetto del documento deve essere presente"),
	
	/**
	 * Valore.
	 */
	INDICE_CLASSIFICAZIONE_NON_PRESENTE(22, "L'indice di classificazione deve essere presente"),
	
	/**
	 * Valore.
	 */
	ID_TIPOLOGIA_DOCUMENTO_NON_PRESENTE(23, "La tipologia di documento (ID_TIPOLOGIA_DOCUMENTO) deve essere presente"),
	
	/**
	 * Valore.
	 */
	ID_TIPOLOGIA_PROCEDIMENTO_NON_PRESENTE(24, "La tipologia di procedimento (ID_TIPOLOGIA_PROCEDIMENTO) deve essere presente"),
	
	/**
	 * Valore.
	 */
	OGGETTO_MAIL_NON_PRESENTE(25, "L'oggetto della mail deve essere presente"),
	
	/**
	 * Valore.
	 */
	TESTO_MAIL_NON_PRESENTE(26, "Il testo della mail deve essere presente"),
	
	/**
	 * Valore.
	 */
	MITTENTE_MAIL_NON_PRESENTE(27, "Il mittente della mail deve essere presente"),
	
	/**
	 * Valore.
	 */
	ID_DOCUMENTO_NON_PRESENTE(28, "L'ID del documento (DOCUMENT_TITLE) deve essere presente"),
	
	/**
	 * Valore.
	 */
	ID_FASCICOLO_NON_VALIDO(29, "L'ID del dascicolo non è valido"),
	
	/**
	 * Valore.
	 */
	ID_UTENTE_NON_VALIDO(30, "ID Utente non valido"),
	
	/**
	 * Valore.
	 */
	ANNO_NON_VALIDO(31, "Anno non valido"),
	
	/**
	 * Valore.
	 */
	PARAMETRO_RICERCA_NON_PRESENTE(32, "Inserire almeno un parametro di ricerca"),
	
	/**
	 * Valore.
	 */
	CAMPO_RICERCA_NON_PRESENTE(33, "Inserire il campo su cui eseguire la ricerca"),
	
	/**
	 * Valore.
	 */
	PARAMETRI_AOO_NON_VALIDO(34, "Valorizzare in maniera corretta id o codice dell'Aoo"),
	
	/**
	 * Valore.
	 */
	STATO_FASCICOLO_NON_VALIDO(35, "Stato Fascicolo non valido: gli stati possibili sono 0 = aperto e 1 = chiuso"),
	
	/**
	 * Valore.
	 */
	ID_NODO_NON_VALIDO(36, "ID Nodo non valido"),
	
	/**
	 * Valore.
	 */
	PARAMETRO_MODIFICA_NON_PRESENTE(37, "Inserire almeno un valore da modificare"),
	
	/**
	 * Valore.
	 */
	ID_MITTENTE_NON_PRESENTE(38, "Il mittente (ID_MITTENTE) deve essere presente"),
	
	/**
	 * Valore.
	 */
	ALLEGATO_DOC_ENTRATA_DA_FIRMARE(39, "L'allegato di un documento in entrata non può essere impostato come \"Da firmare\""),
	
	/**
	 * Valore.
	 */
	PROTOCOLLO_STAMPIGLIATURA_NON_PRESENTE(40, "Il protocollo da stampigliare sul documento deve essere presente"),
	
	/**
	 * Valore.
	 */
	ID_FALDONE_NON_VALIDO(41, "ID Faldone non valido"),
	
	/**
	 * Valore.
	 */
	NOME_FALDONE_NON_VALIDO(42, "Nome Faldone non valido"),
	
	/**
	 * Valore.
	 */
	ID_FALDONE_OLD_NON_VALIDO(43, "ID Faldone old non valido"),

	/**
	 * Valore.
	 */
	ID_FALDONE_NEW_NON_VALIDO(44, "ID Faldone new non valido"),
	
	/**
	 * Valore.
	 */
	ID_FIRMATARIO_NON_PRESENTE(45, "L'ID di almeno un firmatario deve essere presente"),
	
	/**
	 * Valore.
	 */
	ID_DOCUMENTO_STAMPIGLIATURA_NON_PRESENTE(46, "L'ID del documento da stampigliare sul documento deve essere presente"),
	
	/**
	 * Valore.
	 */
	TESTO_WATERMARK_NON_PRESENTE(47, "Il testo del watermark da stampigliare sul documento deve essere presente"),
	
	/**
	 * Valore.
	 */
	TESTO_APPROVAZIONE_NON_PRESENTE(48, "Il testo dell'approvazione da stampigliare sul documento deve essere presente"),
	
	/**
	 * Valore.
	 */
	NUMERO_VERSIONE_NON_PRESENTE(49, "Il numero di versione del documento deve essere presente"),
	
	/**
	 * Valore.
	 */
	CLASSE_DOCUMENTALE_NON_PRESENTE(50, "La classe documentale deve essere presente"),

	/**
	 * Valore.
	 */
	ENTITA_RICERCA_AV_NON_PRESENTE(51, "L'entità su cui eseguire la ricerca deve essere presente"),

	/**
	 * Valore.
	 */
	ENTITA_RICERCA_AV_NON_GESTITA(52, "L'entità su cui eseguire la ricerca deve essere una tra: DOCUMENTO|FASCICOLO|EMAIL"),
	
	/**
	 * Valore.
	 */
	ENTITA_RICERCA_AV_FULL_TEXT_NON_VALIDA(53, "La ricerca FULL TEXT può essere eseguita solo per l'entità DOCUMENTO"),
	
	/**
	 * Valore.
	 */
	PARAMETRI_RICERCA_AV_NON_PRESENTI(54, "I parametri per la ricerca devono essere presenti"),
	
	/**
	 * Valore.
	 */
	CHIAVE_RICERCA_AV_NON_PRESENTE(55, "La chiave di ricerca deve essere presente"),

	/**
	 * Valore.
	 */
	OPERATORE_RICERCA_AV_NON_PRESENTE(56, "L'operatore di ricerca deve essere presente"),
	
	/**
	 * Valore.
	 */
	VALORE_PARAMETRO_RICERCA_AV_NON_PRESENTE(56, "Il valore del parametro di ricerca deve essere presente"),
	
	/**
	 * Valore.
	 */
	OPERATORE_LOGICO_SUCCESSIVO_RICERCA_AV_NON_PRESENTE(57, "L'operatore logico per il parametro di ricerca successivo deve essere presente"),
	
	/**
	 * Valore.
	 */
	OPERATORE_LOGICO_SUCCESSIVO_RICERCA_AV_NON_VALIDO(58, "L'operatore logico per l'ultimo parametro di ricerca non deve essere presente"),
	
	/**
	 * Valore.
	 */
	WORK_FLOW_NON_PRESENTE(59, "Il Workflow deve essere presente"),
	
	/**
	 * Valore.
	 */
	WORK_FLOW_NUMBER_NON_PRESENTE(60, "Il workflow number è necessario per completare l'operazione"),
	
	/**
	 * Valore.
	 */
	RESPONSE_NAME_NON_PRESENTE(61, "La response è necessaria per completare l'operazione"),
	
	/**
	 * Valore.
	 */
	WORK_FLOW_NAME_NON_PRESENTE(61, "Il name è necessario per completare l'operazione"),

	/**
	 * Valore.
	 */
	METADATI_DOCUMENTO_NON_PRESENTE(62, "I metadati del Documento sono necessari per completare l'operazione"),
	
	/**
	 * Valore.
	 */
	PARAMETRI_FIRMA_NON_PRESENTI(63, "I parametri \"ALTEZZA FOOTER\" e \"SPAZIATURA FIRMA\" devono essere presenti"),
	
	/**
	 * Valore.
	 */
	TIPOLOGIA_PDF_OUTPUT_NON_PRESENTE(64, "La tipologia di PDF che si desidera in output deve essere specificata"),
	
	/**
	 * Valore.
	 */
	TIPOLOGIA_PDF_OUTPUT_NON_COERENTE(65, "Se è presente l'ID del documento da stampigliare, la tipologia di PDF in output deve essere PDF_ID_DOC"),
	
	/**
	 * Valore.
	 */
	SECURITY_DOCUMENTO_NON_PRESENTE(66, "Le Security devono essere presenti"),
	
	/**
	 * Valore.
	 */
	NO_OBJECT_SECURITY(66, "Oggetto Security non è valorizzato correttamente"),
	
	/**
	 * Valore.
	 */
	NO_GROUPPO_SECURITY(67, "Oggetto Gruppo non è valorizzato correttamente"),
	
	/**
	 * Valore.
	 */
	NO_UTENTE_SECURITY(68, "Oggetto Utente non è valorizzato correttamente"),

	/**
	 * Valore.
	 */
	FILTRO_RUBRICA_NON_PRESENTE(69, "Valorizzare correttamente i filtri per Rubrica"),
	
	/**
	 * Valore.
	 */
	WOB_NUMBER_NON_PRESENTE(70, "Il Wob Number deve essere presente"),
	
	/**
	 * Valore.
	 */
	DOCUMENTTITLE_NON_PRESENTE(71, "Il documentTitle deve essere presente"),
	
	/**
	 * Valore.
	 */
	ID_UFFICIO_NON_PRESENTE(72, "L'ufficio (ID_UFFICIO) deve essere presente"),
	
	/**
	 * Valore.
	 */
	ID_RUOLO_NON_PRESENTE(73, "L'ufficio (ID_RUOLO) deve essere presente"),
	
	/**
	 * Valore.
	 */
	INFO_PROT_NON_PRESENTE(74, "L'info del protocollo deve essere presente"),
	
	/**
	 * Valore.
	 */
	DATA_SPED_NON_PRESENTE(75, "Data spedizione deve essere presente"),
	
	/**
	 * Valore.
	 */
	NUM_PROT_NON_PRESENTE(76, "Numero protocollo deve essere presente"),
	
	/**
	 * Valore.
	 */
	ANNO_PROT_NON_PRESENTE(77, "Anno protocollo deve essere presente"),
	
	/**
	 * Valore.
	 */
	ERRORE_ELABORAZIONE(999, "Errore durante l'elaborazione della richiesta");

	/**
	 * Codice.
	 */
	private Integer code;
	
	/**
	 * Descrizione.
	 */
	private String description;
	
	ErrorCode(Integer code, String description){
		this.code = code;
		this.description = description;
	}
	
	/**
	 * Recupera il codice.
	 * @return codice
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Recupera la descrizione.
	 * @return descrizione
	 */
	public String getDescription() {
		return description;
	}
}
