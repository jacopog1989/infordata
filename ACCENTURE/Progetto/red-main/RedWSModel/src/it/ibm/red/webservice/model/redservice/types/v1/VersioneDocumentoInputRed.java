
package it.ibm.red.webservice.model.redservice.types.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VersioneDocumentoInputRed complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VersioneDocumentoInputRed">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:red:types:v1}DocumentoRed">
 *       &lt;sequence>
 *         &lt;element name="onlyContent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="withContent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VersioneDocumentoInputRed", propOrder = {
    "onlyContent",
    "withContent"
})
public class VersioneDocumentoInputRed extends DocumentoRed {

	/**
	 * Flag solo content.
	 */
    protected Boolean onlyContent;

	/**
	 * Flag con content.
	 */
    protected Boolean withContent;

    /**
     * Gets the value of the onlyContent property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     */
    public Boolean isOnlyContent() {
        return onlyContent;
    }

    /**
     * Sets the value of the onlyContent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     */
    public void setOnlyContent(final Boolean value) {
        this.onlyContent = value;
    }

    /**
     * Gets the value of the withContent property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     */
    public Boolean isWithContent() {
        return withContent;
    }

    /**
     * Sets the value of the withContent property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     */
    public void setWithContent(final Boolean value) {
        this.withContent = value;
    }
}
