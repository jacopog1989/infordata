
package it.ibm.red.webservice.model.interfaccianps.npstypes;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraNotificaOperazioneType;


/**
 * Dati di notifica di una operazione asincrona
 * 
 * <p>Classe Java per wkf_datiNotificaOperazione_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_datiNotificaOperazione_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdMessaggio" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="DataNotifica" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="TipoNotifica" type="{http://mef.gov.it.v1.npsTypes}wkf_tipoNotificaOperazione_type"/>
 *         &lt;element name="TipoEsito" type="{http://mef.gov.it.v1.npsTypes}wkf_esitoNotificaOperazione_type"/>
 *         &lt;element name="IdDocumento" type="{http://mef.gov.it.v1.npsTypes}Guid" minOccurs="0"/>
 *         &lt;element name="CodiMessaggio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.npsTypes}prot_identificatoreProtocollo_type" minOccurs="0"/>
 *         &lt;element name="Operazione" type="{http://mef.gov.it.v1.npsTypes}doc_completamentoOperazioneDocumentale_type" minOccurs="0"/>
 *         &lt;element name="Flusso" type="{http://mef.gov.it.v1.npsTypes}wkf_datiTipoFlusso_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_datiNotificaOperazione_type", propOrder = {
    "idMessaggio",
    "dataNotifica",
    "tipoNotifica",
    "tipoEsito",
    "idDocumento",
    "codiMessaggio",
    "protocollo",
    "operazione",
    "flusso"
})
@XmlSeeAlso({
    RichiestaElaboraNotificaOperazioneType.class
})
public class WkfDatiNotificaOperazioneType
    implements Serializable
{

    @XmlElement(name = "IdMessaggio", required = true)
    protected String idMessaggio;
    @XmlElement(name = "DataNotifica", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataNotifica;
    @XmlElement(name = "TipoNotifica", required = true)
    @XmlSchemaType(name = "string")
    protected WkfTipoNotificaOperazioneType tipoNotifica;
    @XmlElement(name = "TipoEsito", required = true)
    @XmlSchemaType(name = "string")
    protected WkfEsitoNotificaOperazioneType tipoEsito;
    @XmlElementRef(name = "IdDocumento", type = JAXBElement.class, required = false)
    protected JAXBElement<String> idDocumento;
    @XmlElement(name = "CodiMessaggio")
    protected String codiMessaggio;
    @XmlElement(name = "Protocollo")
    protected ProtIdentificatoreProtocolloType protocollo;
    @XmlElement(name = "Operazione")
    protected DocCompletamentoOperazioneDocumentaleType operazione;
    @XmlElement(name = "Flusso")
    protected WkfDatiTipoFlussoType flusso;

    /**
     * Recupera il valore della proprietà idMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggio() {
        return idMessaggio;
    }

    /**
     * Imposta il valore della proprietà idMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggio(String value) {
        this.idMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà dataNotifica.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataNotifica() {
        return dataNotifica;
    }

    /**
     * Imposta il valore della proprietà dataNotifica.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataNotifica(XMLGregorianCalendar value) {
        this.dataNotifica = value;
    }

    /**
     * Recupera il valore della proprietà tipoNotifica.
     * 
     * @return
     *     possible object is
     *     {@link WkfTipoNotificaOperazioneType }
     *     
     */
    public WkfTipoNotificaOperazioneType getTipoNotifica() {
        return tipoNotifica;
    }

    /**
     * Imposta il valore della proprietà tipoNotifica.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfTipoNotificaOperazioneType }
     *     
     */
    public void setTipoNotifica(WkfTipoNotificaOperazioneType value) {
        this.tipoNotifica = value;
    }

    /**
     * Recupera il valore della proprietà tipoEsito.
     * 
     * @return
     *     possible object is
     *     {@link WkfEsitoNotificaOperazioneType }
     *     
     */
    public WkfEsitoNotificaOperazioneType getTipoEsito() {
        return tipoEsito;
    }

    /**
     * Imposta il valore della proprietà tipoEsito.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfEsitoNotificaOperazioneType }
     *     
     */
    public void setTipoEsito(WkfEsitoNotificaOperazioneType value) {
        this.tipoEsito = value;
    }

    /**
     * Recupera il valore della proprietà idDocumento.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIdDocumento() {
        return idDocumento;
    }

    /**
     * Imposta il valore della proprietà idDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIdDocumento(JAXBElement<String> value) {
        this.idDocumento = value;
    }

    /**
     * Recupera il valore della proprietà codiMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiMessaggio() {
        return codiMessaggio;
    }

    /**
     * Imposta il valore della proprietà codiMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiMessaggio(String value) {
        this.codiMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà protocollo.
     * 
     * @return
     *     possible object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public ProtIdentificatoreProtocolloType getProtocollo() {
        return protocollo;
    }

    /**
     * Imposta il valore della proprietà protocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public void setProtocollo(ProtIdentificatoreProtocolloType value) {
        this.protocollo = value;
    }

    /**
     * Recupera il valore della proprietà operazione.
     * 
     * @return
     *     possible object is
     *     {@link DocCompletamentoOperazioneDocumentaleType }
     *     
     */
    public DocCompletamentoOperazioneDocumentaleType getOperazione() {
        return operazione;
    }

    /**
     * Imposta il valore della proprietà operazione.
     * 
     * @param value
     *     allowed object is
     *     {@link DocCompletamentoOperazioneDocumentaleType }
     *     
     */
    public void setOperazione(DocCompletamentoOperazioneDocumentaleType value) {
        this.operazione = value;
    }

    /**
     * Recupera il valore della proprietà flusso.
     * 
     * @return
     *     possible object is
     *     {@link WkfDatiTipoFlussoType }
     *     
     */
    public WkfDatiTipoFlussoType getFlusso() {
        return flusso;
    }

    /**
     * Imposta il valore della proprietà flusso.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfDatiTipoFlussoType }
     *     
     */
    public void setFlusso(WkfDatiTipoFlussoType value) {
        this.flusso = value;
    }

}
