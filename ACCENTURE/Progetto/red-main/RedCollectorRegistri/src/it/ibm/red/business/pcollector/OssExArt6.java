package it.ibm.red.business.pcollector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.LookupTableDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoDocumentoModeEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * Collector del registro: Osservazione Ex Art 6.
 * 
 * @author SimoneLungarella
 */
public class OssExArt6 extends AbstractParametersCollector {

	/**
	 * Logger. 
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(OssExArt6.class);

	/**
	 * Placeholder destinatari.
	 */
	private static final String DESTINATARI = "destinatari";
	
	/**
	 * Metadato DECRETO, indica se il testo del registro deve far riferimento ad un
	 * decreto o ad un provvedimento.
	 */
	private static final String DECRETO = "decreto";
	
	/**
	 * Placeholder descrizione ufficio.
	 */
	private static final String DESC_UFFICIO = "desc-ufficio";
	
	/**
	 * Prefisso Area organizzativa omogenea;
	 */
	private static final String AOO_PREFIX = "UCB ";
	
	/**
	 * Display name Data Decreto.
	 */
	private static final String DATA_DECRETO_DISPLAYNAME = "Data Decreto";

	/**
	 * Display name Numero Decreto.
	 */
	private static final String NUMERO_DECRETO_DISPLAYNAME = "Numero Decreto";

	/**
	 * Nome metadato numero registrazione ausiliarai sul registro.
	 */
	private static final String NUMERO_REGISTRAZIONE_AUSILIARIA = "numero-registrazione-ausiliaria";
	
	/**
	 * Nome metadato data firma sul registro.
	 */
	private static final String DATA_FIRMA = "data-firma";
	
	/**
	 * Nome metadato data protocollo ingresso sul registro.
	 */
	private static final String DATA_PROTOCOLLO_INGRESSO = "data-protocollo-ingresso";
	
	/**
	 * Nome metadato oggetto sul registro.
	 */
	private static final String OGGETTO = "oggetto";
	
	/**
	 * Nome metadato numero protocollo ingresso sul registro.
	 */
	private static final String NUMERO_PROTOCOLLO_INGRESSO = "numero-protocollo-ingresso";
	
	/**
	 * Nome metadato amministrazione protocollo mittente sul registro.
	 */
	private static final String AMMINISTRAZIONE_PROTOCOLLO_MITTENTE = "amministrazione-protocollo-mittente";

	/**
	 * Nome metadato descrizione ucb sul registro.
	 */
	private static final String DESC_UCB = "desc-ucb";
	
	/**
	 * Nome metadato data protocollo mittente sul registro.
	 */
	private static final String DATA_PROTOCOLLO_MITTENTE = "data-protocollo-mittente";
	
	/**
	 * Nome metadato numero protocollo mittente sul registro.
	 */
	private static final String NUMERO_PROTOCOLLO_MITTENTE = "numero-protocollo-mittente";
	
	/**
	 * Filenet CE Helper.
	 */
	private IFilenetCEHelper fceh;

	/**
	 * @see it.ibm.red.business.pcollector.AbstractParametersCollector#initParameters(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.util.Collection, boolean).
	 */
	@Override
	protected void initParameters(final UtenteDTO utente, final String documentTitle, final Collection<MetadatoDTO> out, final boolean setDefaultValue) {
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			Object oggetto = null;
			Object numeroProtocollo = null;
			Object dataProtocollo = null;
			Object protocolloMittente = null;
			Object dataProtocolloMittente = null;
			
	
			try {
				Document docIngresso = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());
				oggetto = TrasformerCE.getMetadato(docIngresso, PropertiesNameEnum.OGGETTO_METAKEY);
				numeroProtocollo = TrasformerCE.getMetadato(docIngresso, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
				dataProtocollo = TrasformerCE.getMetadato(docIngresso, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);
				protocolloMittente = TrasformerCE.getMetadato(docIngresso, PropertiesNameEnum.NUMERO_PROTOCOLLO_MITTENTE_METAKEY);
				if(protocolloMittente == null) {
					protocolloMittente = "";
				}
				
				dataProtocolloMittente = TrasformerCE.getMetadato(docIngresso, PropertiesNameEnum.DATA_PROTOCOLLO_MITTENTE_METAKEY);
				if (dataProtocolloMittente == null) {
					dataProtocolloMittente = "";
				}
				
			} catch (Exception e) {
				LOGGER.error("Errore in fase di recupero documento: " + documentTitle, e);
				throw new RedException("Errore in fase di recupero documento: " + documentTitle, e);
			}
	
			// Gestisco i metadati in base alla tipologia di documento che viene creata
			Integer idTipologiaDocumento = fceh.getTipoDocumentoByDocumentTitle(documentTitle);
			if (idTipologiaDocumento.toString()
					.equals(PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "."
							+ (PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_ENTRATA_ATTI_SOGGETTI_VISTO).getKey()))) {
				for (MetadatoDTO m : out) {
					switch (m.getName()) {
					case NUMERO_PROTOCOLLO_MITTENTE:
						m.setDisplayName("Numero Provvedimento");
						if (setDefaultValue) {
							m.setSelectedValue("");
						}
						break;
					case DATA_PROTOCOLLO_MITTENTE:
						m.setDisplayName("Data Provvedimento");
						if (setDefaultValue) {
							m.setSelectedValue("");
						}
						break;
					case NUMERO_PROTOCOLLO_INGRESSO:
						if(setDefaultValue) {
							m.setSelectedValue(numeroProtocollo == null ? "" : numeroProtocollo.toString());
						}
						
						break;
					case OGGETTO:
						if(setDefaultValue) {
							m.setSelectedValue(oggetto == null ? "" : oggetto.toString());
						}
						break;
					case DATA_PROTOCOLLO_INGRESSO:
						if(setDefaultValue) {
							m.setSelectedValue(dataProtocollo == null ? "" : (Date) dataProtocollo);
						}
						break;
					case DESC_UCB:
						if(setDefaultValue) {
							if(utente.getAooDesc().startsWith(AOO_PREFIX)) {
								m.setSelectedValue(utente.getAooDesc().substring(4));
							} else {
								m.setSelectedValue(utente.getAooDesc());
							}
						}
						break;
					case DESC_UFFICIO:
						if(setDefaultValue) {
							m.setSelectedValue(recuperaDescrizioneUfficio(utente));
						}
						break;
					case DATA_FIRMA:
						initializeDate(m, setDefaultValue);
						break;
					case NUMERO_REGISTRAZIONE_AUSILIARIA:
						if(setDefaultValue) {
							m.setSelectedValue(Constants.Varie.PLACEHOLDER_NUMBER);
						}
						break;
					case AMMINISTRAZIONE_PROTOCOLLO_MITTENTE:
						if(setDefaultValue) {
							m.setSelectedValue(PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "." + (PropertiesNameEnum.DESCRIZIONE_AMMINISTRAZIONE_REGAUX).getKey()));
						}
						break;
					case DESTINATARI:
						if (setDefaultValue) {
							m.setSelectedValue(recuperaDestinatario(fceh, documentTitle, utente));
						}
						break;
					default:
						break;
					}
				}
			} else if (idTipologiaDocumento.toString()
					.equals(PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "."
							+ (PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_ENTRATA_DECRETO_IMPEGNO).getKey()))) {
				for (MetadatoDTO m : out) {
					switch (m.getName()) {
					case NUMERO_PROTOCOLLO_MITTENTE:
						m.setDisplayName(NUMERO_DECRETO_DISPLAYNAME);
						m.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
						if (setDefaultValue) {
							m.setSelectedValue("");
						}
						break;
					case DATA_PROTOCOLLO_MITTENTE:
						m.setDisplayName(DATA_DECRETO_DISPLAYNAME);
						m.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
						if (setDefaultValue) {
							m.setSelectedValue("");
						}
						break;
					case NUMERO_PROTOCOLLO_INGRESSO:
						m.setSelectedValue(numeroProtocollo == null ? "" : numeroProtocollo.toString());
						break;
					case OGGETTO:
						if(setDefaultValue) {
							m.setSelectedValue(oggetto == null ? "" : oggetto.toString());
						}
						break;
					case DATA_PROTOCOLLO_INGRESSO:
						if(setDefaultValue) {
							m.setSelectedValue(dataProtocollo == null ? "" : (Date) dataProtocollo);
						}
						break;
					case DESC_UCB:
						if(setDefaultValue) {
							if(utente.getAooDesc().startsWith(AOO_PREFIX)) {
								m.setSelectedValue(utente.getAooDesc().substring(4));
							} else {
								m.setSelectedValue(utente.getAooDesc());
							}
						}
						break;
					case DESC_UFFICIO:
						if(setDefaultValue) {
							m.setSelectedValue(recuperaDescrizioneUfficio(utente));
						}
						break;
					case DATA_FIRMA:
						initializeDate(m, setDefaultValue);
						break;
					case NUMERO_REGISTRAZIONE_AUSILIARIA:
						if(setDefaultValue) {
							m.setSelectedValue(Constants.Varie.PLACEHOLDER_NUMBER);
						}
						break;
					case AMMINISTRAZIONE_PROTOCOLLO_MITTENTE:
						if(setDefaultValue) {
							m.setSelectedValue(PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "." + (PropertiesNameEnum.DESCRIZIONE_AMMINISTRAZIONE_REGAUX).getKey()));
						}
						break;
					case DECRETO:
						m.setSelectedValue(true);
						break;
					case DESTINATARI:
						if (setDefaultValue) {
							m.setSelectedValue(recuperaDestinatario(fceh, documentTitle, utente));
						}
						break;
					default:
						break;
					}
				}
			} else if (idTipologiaDocumento.toString()
					.equals(PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "."
							+ (PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_ENTRATA_DECRETO_IMPEGNO_DA_SICOGE).getKey()))) {
				for (MetadatoDTO m : out) {
					switch (m.getName()) {
					case NUMERO_PROTOCOLLO_MITTENTE:
						m.setDisplayName(NUMERO_DECRETO_DISPLAYNAME);
						m.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
						if (setDefaultValue) {
							m.setSelectedValue("");
						}
						break;
					case DATA_PROTOCOLLO_MITTENTE:
						m.setDisplayName(DATA_DECRETO_DISPLAYNAME);
						m.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
						if (setDefaultValue) {
							m.setSelectedValue("");
						}
						break;
					case NUMERO_PROTOCOLLO_INGRESSO:
						if(setDefaultValue) {
							m.setSelectedValue(numeroProtocollo == null ? "" : numeroProtocollo.toString());
						}
						break;
					case OGGETTO:
						if(setDefaultValue) {
							m.setSelectedValue(oggetto == null ? "" : oggetto.toString());
						}
						break;
					case DATA_PROTOCOLLO_INGRESSO:
						if(setDefaultValue) {
							m.setSelectedValue(dataProtocollo == null ? "" : (Date) dataProtocollo);
						}
						break;
					case DESC_UCB:
						if(setDefaultValue) {
							if(utente.getAooDesc().startsWith(AOO_PREFIX)) {
								m.setSelectedValue(utente.getAooDesc().substring(4));
							} else {
								m.setSelectedValue(utente.getAooDesc());
							}
						}
						break;
					case DESC_UFFICIO:
						if(setDefaultValue) {
							m.setSelectedValue(recuperaDescrizioneUfficio(utente));
						}
						break;
					case DATA_FIRMA:
						initializeDate(m, setDefaultValue);
						break;
					case NUMERO_REGISTRAZIONE_AUSILIARIA:
						if(setDefaultValue) {
							m.setSelectedValue(Constants.Varie.PLACEHOLDER_NUMBER);
						}
						break;
					case AMMINISTRAZIONE_PROTOCOLLO_MITTENTE:
						if(setDefaultValue) {
							m.setSelectedValue(PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "." + (PropertiesNameEnum.DESCRIZIONE_AMMINISTRAZIONE_REGAUX).getKey()));
						}
						break;
					case DECRETO:
						m.setSelectedValue(true);
						break;
					case DESTINATARI:
						if (setDefaultValue) {
							m.setSelectedValue(recuperaDestinatario(fceh, documentTitle, utente));
						}
						break;
					default:
						break;
					}
				}
			} else if (idTipologiaDocumento.toString()
					.equals(PropertiesProvider.getIstance()
							.getParameterByString(utente.getCodiceAoo() + "."
									+ (PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_ENTRATA_GESTIONE_BILANCIO_FINANZIARIO)
											.getKey()))) {
				for (MetadatoDTO m : out) {
					switch (m.getName()) {
					case NUMERO_PROTOCOLLO_MITTENTE:
						m.setDisplayName("Numero Provvedimento");
						m.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
						if (setDefaultValue) {
							m.setSelectedValue("");
						}
						break;
					case DATA_PROTOCOLLO_MITTENTE:
						m.setDisplayName("Data Provvedimento");
						m.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
						if (setDefaultValue) {
							m.setSelectedValue("");
						}
						break;
					case NUMERO_PROTOCOLLO_INGRESSO:
						if(setDefaultValue) {
							m.setSelectedValue(numeroProtocollo == null ? "" : numeroProtocollo.toString());
						}
						break;
					case OGGETTO:
						if(setDefaultValue) {
							m.setSelectedValue(oggetto == null ? "" : oggetto.toString());
						}
						break;
					case DATA_PROTOCOLLO_INGRESSO:
						if(setDefaultValue) {
							m.setSelectedValue(dataProtocollo == null ? "" : (Date) dataProtocollo);
						}
						break;
					case DESC_UCB:
						if(setDefaultValue) {
							if(utente.getAooDesc().startsWith(AOO_PREFIX)) {
								m.setSelectedValue(utente.getAooDesc().substring(4));
							} else {
								m.setSelectedValue(utente.getAooDesc());
							}
						}
						break;
					case DESC_UFFICIO:
						if(setDefaultValue) {
							m.setSelectedValue(recuperaDescrizioneUfficio(utente));
						}
						break;
					case DATA_FIRMA:
						initializeDate(m, setDefaultValue);
						break;
					case NUMERO_REGISTRAZIONE_AUSILIARIA:
						if(setDefaultValue) {
							m.setSelectedValue(Constants.Varie.PLACEHOLDER_NUMBER);
						}
						break;
					case AMMINISTRAZIONE_PROTOCOLLO_MITTENTE:
						if(setDefaultValue) {
							m.setSelectedValue(PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "." + (PropertiesNameEnum.DESCRIZIONE_AMMINISTRAZIONE_REGAUX).getKey()));
						}
						break;
					case DESTINATARI:
						if (setDefaultValue) {
							m.setSelectedValue(recuperaDestinatario(fceh, documentTitle, utente));
						}
						break;
					default:
						break;
					}
				}
			} else if (idTipologiaDocumento.toString()
					.equals(PropertiesProvider.getIstance()
							.getParameterByString(utente.getCodiceAoo() + "."
									+ (PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_ENTRATA_PROVVEDIMENTI_PERS_IN_SERVIZIO)
											.getKey()))) {
				for (MetadatoDTO m : out) {
					switch (m.getName()) {
					case NUMERO_PROTOCOLLO_MITTENTE:
						m.setDisplayName(NUMERO_DECRETO_DISPLAYNAME);
						m.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
						if (setDefaultValue) {
							m.setSelectedValue("");
						}
						break;
					case DATA_PROTOCOLLO_MITTENTE:
						m.setDisplayName(DATA_DECRETO_DISPLAYNAME);
						m.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
						if (setDefaultValue) {
							m.setSelectedValue("");
						}
						break;
					case NUMERO_PROTOCOLLO_INGRESSO:
						if(setDefaultValue) {
							m.setSelectedValue(numeroProtocollo == null ? "" : numeroProtocollo.toString());
						}
						break;
					case OGGETTO:
						if(setDefaultValue) {
							m.setSelectedValue(oggetto == null ? "" : oggetto.toString());
						}
						break;
					case DATA_PROTOCOLLO_INGRESSO:
						if(setDefaultValue) {
							m.setSelectedValue(dataProtocollo == null ? "" : (Date) dataProtocollo);
						}
						break;
					case DESC_UCB:
						if(setDefaultValue) {
							if(utente.getAooDesc().startsWith(AOO_PREFIX)) {
								m.setSelectedValue(utente.getAooDesc().substring(4));
							} else {
								m.setSelectedValue(utente.getAooDesc());
							}
						}
						break;
					case DESC_UFFICIO:
						if(setDefaultValue) {
							m.setSelectedValue(recuperaDescrizioneUfficio(utente));
						}
						break;
					case DATA_FIRMA:
						initializeDate(m, setDefaultValue);
						break;
					case NUMERO_REGISTRAZIONE_AUSILIARIA:
						if(setDefaultValue) {
							m.setSelectedValue(Constants.Varie.PLACEHOLDER_NUMBER);
						}
						break;
					case AMMINISTRAZIONE_PROTOCOLLO_MITTENTE:
						if(setDefaultValue) {
							m.setSelectedValue(PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "." + (PropertiesNameEnum.DESCRIZIONE_AMMINISTRAZIONE_REGAUX).getKey()));
						}
						break;
					case DECRETO:
						m.setSelectedValue(true);
						break;
					case DESTINATARI:
						if (setDefaultValue) {
							m.setSelectedValue(recuperaDestinatario(fceh, documentTitle, utente));
						}
						break;
					default:
						break;
					}
				}
			} else if (idTipologiaDocumento.toString()
					.equals(PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "."
							+ (PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_ENTRATA_DECRETO_ACCERTAMENTO_ENTRATE).getKey()))) {
				for (MetadatoDTO m : out) {
					switch (m.getName()) {
					case NUMERO_PROTOCOLLO_MITTENTE:
						m.setDisplayName(NUMERO_DECRETO_DISPLAYNAME);
						m.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
						if (setDefaultValue) {
							m.setSelectedValue("");
						}
						break;
					case DATA_PROTOCOLLO_MITTENTE:
						m.setDisplayName(DATA_DECRETO_DISPLAYNAME);
						m.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
						if (setDefaultValue) {
							m.setSelectedValue("");
						}
						break;
					case NUMERO_PROTOCOLLO_INGRESSO:
						if(setDefaultValue) {
							m.setSelectedValue(numeroProtocollo == null ? "" : numeroProtocollo.toString());
						}
						break;
					case OGGETTO:
						if(setDefaultValue) {
							m.setSelectedValue(oggetto == null ? "" : oggetto.toString());
						}
						break;
					case DATA_PROTOCOLLO_INGRESSO:
						if(setDefaultValue) {
							m.setSelectedValue(dataProtocollo == null ? "" : (Date) dataProtocollo);
						}
						break;
					case DESC_UCB:
						if(setDefaultValue) {
							if(utente.getAooDesc().startsWith(AOO_PREFIX)) {
								m.setSelectedValue(utente.getAooDesc().substring(4));
							} else {
								m.setSelectedValue(utente.getAooDesc());
							}
						}
						break;
					case DESC_UFFICIO:
						if(setDefaultValue) {
							m.setSelectedValue(recuperaDescrizioneUfficio(utente));
						}
						break;
					case DATA_FIRMA:
						initializeDate(m, setDefaultValue);
						break;
					case NUMERO_REGISTRAZIONE_AUSILIARIA:
						if(setDefaultValue) {
							m.setSelectedValue(Constants.Varie.PLACEHOLDER_NUMBER);
						}
						break;
					case AMMINISTRAZIONE_PROTOCOLLO_MITTENTE:
						if(setDefaultValue) {
							m.setSelectedValue(PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "." + (PropertiesNameEnum.DESCRIZIONE_AMMINISTRAZIONE_REGAUX).getKey()));
						}
						break;
					case DECRETO:
						m.setSelectedValue(true);
						break;
					case DESTINATARI:
						if (setDefaultValue) {
							m.setSelectedValue(recuperaDestinatario(fceh, documentTitle, utente));
						}
						break;
					default:
						break;
					}
				}
			} else {
				for (MetadatoDTO m : out) {
					switch (m.getName()) {
					case NUMERO_PROTOCOLLO_MITTENTE:
						m.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
						if (setDefaultValue) {
							m.setSelectedValue(protocolloMittente);
						}
						break;
					case DATA_PROTOCOLLO_MITTENTE:
						m.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
						if (setDefaultValue) {
							m.setSelectedValue(dataProtocolloMittente);
						}
						break;
					case NUMERO_PROTOCOLLO_INGRESSO:
						if(setDefaultValue) {
							m.setSelectedValue(numeroProtocollo == null ? "" : numeroProtocollo.toString());
						}
						break;
					case OGGETTO:
						if(setDefaultValue) {
							m.setSelectedValue(oggetto == null ? "" : oggetto.toString());
						}
						break;
					case DATA_PROTOCOLLO_INGRESSO:
						if(setDefaultValue) {
							m.setSelectedValue(dataProtocollo == null ? "" : (Date) dataProtocollo);
						}
						break;
					case DESC_UCB:
						if(setDefaultValue) {
							if(utente.getAooDesc().startsWith(AOO_PREFIX)) {
								m.setSelectedValue(utente.getAooDesc().substring(4));
							} else {
								m.setSelectedValue(utente.getAooDesc());
							}
						}
						break;
					case DESC_UFFICIO:
						if(setDefaultValue) {
							m.setSelectedValue(recuperaDescrizioneUfficio(utente));
						}break;
					case DATA_FIRMA:
						initializeDate(m, setDefaultValue);
						break;
					case NUMERO_REGISTRAZIONE_AUSILIARIA:
						if(setDefaultValue) {
							m.setSelectedValue(Constants.Varie.PLACEHOLDER_NUMBER);
						}
						break;
					case AMMINISTRAZIONE_PROTOCOLLO_MITTENTE:
						if(setDefaultValue) {
							m.setSelectedValue(PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "." + (PropertiesNameEnum.DESCRIZIONE_AMMINISTRAZIONE_REGAUX).getKey()));
						}
						break;
					case DESTINATARI:
						if (setDefaultValue) {
							m.setSelectedValue(recuperaDestinatario(fceh, documentTitle, utente));
						}
						break;
					default:
						break;
					}
				}
			}
		} finally {
			if(fceh != null) {
				fceh.popSubject();
			}
		}
	}

	/**
	 * @see it.ibm.red.business.pcollector.ParametersCollector#prepareMetadatiForDocUscita(java.util.Collection,
	 *      java.util.Collection, java.lang.String).
	 */
	@Override
	public Collection<MetadatoDTO> prepareMetadatiForDocUscita(final Collection<MetadatoDTO> metadatiToPrepare,
			final Collection<MetadatoDTO> listaMetadatiRegistro, final String nomeRegistro) {
		Collection<MetadatoDTO> out = new ArrayList<>();

		for (MetadatoDTO metadatoDoc : metadatiToPrepare) {
			boolean modified = false;
			if ("NOME_REGISTRO".equals(metadatoDoc.getName())) {
				modified = true;
				metadatoDoc.setSelectedValue(nomeRegistro);
				metadatoDoc.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
				metadatoDoc.setEditability(TipoDocumentoModeEnum.MAI);
			}

			if ("MOTIVAZIONE_OSSERVAZIONE".equals(metadatoDoc.getName())) {
				
				String motivazioneOsservazioneValue = null;
				String sottoMotivazioneOsservazioneValue = null;
				for (MetadatoDTO metadatoReg : listaMetadatiRegistro) {

					if ("motivazione-osservazione".equals(metadatoReg.getName())) {
						motivazioneOsservazioneValue = 
								((LookupTableDTO) metadatoReg).getLookupValueSelected().getDescription();
					}

					if ("sotto-motivazione-osservazione".equals(metadatoReg.getName())) {
						sottoMotivazioneOsservazioneValue = metadatoReg.getValue4AttrExt();
					}

				}
				
				if(!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(motivazioneOsservazioneValue) ||
						!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(sottoMotivazioneOsservazioneValue)) {
					modified = true;
					metadatoDoc.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
					metadatoDoc.setEditability(TipoDocumentoModeEnum.MAI);
					
					metadatoDoc.setSelectedValue(
							(motivazioneOsservazioneValue != null ? motivazioneOsservazioneValue : "") + 
							(sottoMotivazioneOsservazioneValue != null ? sottoMotivazioneOsservazioneValue : ""));
				
				}
			}
			
			if ("NOTE".equals(metadatoDoc.getName())) {
				for (MetadatoDTO metadatoReg : listaMetadatiRegistro) {
					if ("note-osservazione".equals(metadatoReg.getName())) {
						modified = true;
						metadatoDoc.setSelectedValue(metadatoReg.getValue4AttrExt());
						metadatoDoc.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
						metadatoDoc.setEditability(TipoDocumentoModeEnum.MAI);
						break;
					}
				}
			}
			
			
			
			
			if(modified) {
				out.add(metadatoDoc);
			}
		}

		return out;
	}

}
