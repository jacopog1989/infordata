package it.ibm.red.business.pcollector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoDocumentoModeEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * Collector Visto Ex Art 10 - Efficacia visto ex art 10.
 * 
 * @author s.lungarella.ibm
 */
public class VistoExArt10 extends AbstractParametersCollector {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(VistoExArt10.class);
	
	/**
	 * Prefisso AOO.
	 */
	private static final String AOO_PREFIX = "UCB ";
	
	/**
	 * Nome metadato data protocollo ingresso sul registro.
	 */
	private static final String DATA_PROTOCOLLO_INGRESSO = "data-protocollo-ingresso";
	
	/**
	 * Nome metadato numero protocollo mittente sul registro.
	 */
	private static final String NUMERO_PROTOCOLLO_MITTENTE = "numero-protocollo-mittente";
	
	/**
	 * Filenet CE Helper.
	 */
	private IFilenetCEHelper fceh;

	/**
	 * @see it.ibm.red.business.pcollector.AbstractParametersCollector#initParameters(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.util.Collection, boolean).
	 */
	@Override
	protected void initParameters(final UtenteDTO utente, final String documentTitle, final Collection<MetadatoDTO> out, final boolean setDefaultValue) {
		try{
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
		
			Object oggetto = null;
			Object protocolloMittente = null;
			Object dataProtocolloMittente = null;
			Object numeroProtocollo = null;
			Object dataProtocollo = null;

			try {
				Document docIngresso = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());
				oggetto = TrasformerCE.getMetadato(docIngresso, PropertiesNameEnum.OGGETTO_METAKEY);
				protocolloMittente = TrasformerCE.getMetadato(docIngresso,
						PropertiesNameEnum.NUMERO_PROTOCOLLO_MITTENTE_METAKEY);
				dataProtocolloMittente = TrasformerCE.getMetadato(docIngresso,
						PropertiesNameEnum.DATA_PROTOCOLLO_MITTENTE_METAKEY);
				numeroProtocollo = TrasformerCE.getMetadato(docIngresso, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
				dataProtocollo = TrasformerCE.getMetadato(docIngresso, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);
			} catch (Exception e) {
				LOGGER.error("Errore in fase di recupero documento: " + documentTitle, e);
				throw new RedException("Errore in fase di recupero documento: " + documentTitle, e);
			}
	
			Integer idTipologiaDocumento = fceh.getTipoDocumentoByDocumentTitle(documentTitle);
	
			if (idTipologiaDocumento.toString()
					.equals(PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "."
							+ (PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_ENTRATA_DECRETO_IMPEGNO).getKey()))) {
				for (MetadatoDTO m : out) {
					switch (m.getName()) {
					case NUMERO_PROTOCOLLO_MITTENTE:
						m.setDisplayName("Numero Decreto");
						if (setDefaultValue) {
							m.setSelectedValue("");
						}
						break;
					case "data-protocollo-mittente":
						m.setDisplayName("Data Decreto");
						if (setDefaultValue) {
							m.setSelectedValue("");
						}
						break;
					case "numero-protocollo-ingresso":
						if(setDefaultValue) {
							m.setSelectedValue(numeroProtocollo == null ? "" : numeroProtocollo.toString());
						}
						break;
					case "oggetto":
						if(setDefaultValue) {
							m.setSelectedValue(oggetto == null ? "" : oggetto.toString());
						}
						break;
					case DATA_PROTOCOLLO_INGRESSO:
						if(setDefaultValue) {
							m.setSelectedValue(dataProtocollo == null ? "" : (Date) dataProtocollo);
						}
						break;
					case "desc-ucb":
						if(setDefaultValue) {
							if(utente.getAooDesc().startsWith(AOO_PREFIX)) {
								m.setSelectedValue(utente.getAooDesc().substring(4));
							} else {
								m.setSelectedValue(utente.getAooDesc());
							}
						}
						break;
					case "desc-ufficio":
						if(setDefaultValue) {
							m.setSelectedValue(recuperaDescrizioneUfficio(utente));
						}
						break;
					case "data-firma":
						initializeDate(m, setDefaultValue);
						break;
					case "numero-registrazione-ausiliaria":
						if(setDefaultValue) {
							m.setSelectedValue(Constants.Varie.PLACEHOLDER_NUMBER);
						}
						break;
					case "amministrazione-protocollo-mittente":
						if(setDefaultValue) {
							m.setSelectedValue(PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "." + (PropertiesNameEnum.DESCRIZIONE_AMMINISTRAZIONE_REGAUX).getKey()));
						}
						break;
					case "data-registrazione-ausiliaria":
						initializeDate(m, setDefaultValue);
						break;
					case "descrizione-registro-ausiliario":
						if(setDefaultValue) {
							m.setSelectedValue(" Efficacia Ex Art. 10 ");
						}
						break;
					case "destinatari":
						if (setDefaultValue) {
							m.setSelectedValue(recuperaDestinatario(fceh, documentTitle, utente));
						}
						break;
					case "decreto":
						m.setSelectedValue(true);
						break;
					default:
						break;
					}
				}
			} else {
				for (MetadatoDTO m : out) {
					switch (m.getName()) {
					case NUMERO_PROTOCOLLO_MITTENTE:
						if (setDefaultValue) {
							m.setSelectedValue(protocolloMittente);
						}
						break;
					case "data-protocollo-mittente":
						if (setDefaultValue) {
							m.setSelectedValue(dataProtocolloMittente);
						}
						break;
					case "oggetto":
						if(setDefaultValue) {
							m.setSelectedValue(oggetto == null ? "" : oggetto.toString());
						}
						break;
					case "desc-ucb":
						if(setDefaultValue) {
							if(utente.getAooDesc().startsWith(AOO_PREFIX)) {
								m.setSelectedValue(utente.getAooDesc().substring(4));
							} else {
								m.setSelectedValue(utente.getAooDesc());
							}
						}
						break;
					case "desc-ufficio":
						if(setDefaultValue) {
							m.setSelectedValue(recuperaDescrizioneUfficio(utente));
						}
						break;
					case "data-firma":
						initializeDate(m, setDefaultValue);
						break;
					case "numero-registrazione-ausiliaria":
						if(setDefaultValue) {
							m.setSelectedValue(Constants.Varie.PLACEHOLDER_NUMBER);
						}
						break;
					case "amministrazione-protocollo-mittente":
						if(setDefaultValue) {
							m.setSelectedValue(PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "." + (PropertiesNameEnum.DESCRIZIONE_AMMINISTRAZIONE_REGAUX).getKey()));
						}
						break;
					case "numero-protocollo-ingresso":
						if(setDefaultValue) {
							m.setSelectedValue(numeroProtocollo == null ? "" : numeroProtocollo.toString());
						}
						break;
					case DATA_PROTOCOLLO_INGRESSO:
						if(setDefaultValue) {
							m.setSelectedValue(dataProtocollo != null ? (Date) dataProtocollo : "");
						}
						break;
					case "data-registrazione-ausiliaria":
						initializeDate(m, setDefaultValue);
						break;
					case "descrizione-registro-ausiliario":
						if(setDefaultValue) {
							m.setSelectedValue(" Efficacia Ex Art. 10 ");
						}
						break;
					case "destinatari":
						if (setDefaultValue) {
							m.setSelectedValue(recuperaDestinatario(fceh, documentTitle, utente));
						}
						break;
					default:
						break;
					}
				}
			}
		} finally {
			if (fceh != null) {
				fceh.popSubject();
			}
		}
	}

	/**
	 * @see it.ibm.red.business.pcollector.ParametersCollector#prepareMetadatiForDocUscita(java.util.Collection,
	 *      java.util.Collection, java.lang.String).
	 */
	@Override
	public Collection<MetadatoDTO> prepareMetadatiForDocUscita(final Collection<MetadatoDTO> metadatiToPrepare,
			final Collection<MetadatoDTO> listaMetadatiRegistro, final String nomeRegistro) {
		Collection<MetadatoDTO> out = new ArrayList<>();
		for (MetadatoDTO metadatoDoc : metadatiToPrepare) {
			boolean modified = false;

			if ("NOME_REGISTRO".equals(metadatoDoc.getName())) {
				modified = true;
				metadatoDoc.setSelectedValue(nomeRegistro);
				metadatoDoc.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
				metadatoDoc.setEditability(TipoDocumentoModeEnum.MAI);
			}

			for (MetadatoDTO metadatoReg : listaMetadatiRegistro) {

				if (("NUMERO_IMPEGNO_SIRGS".equals(metadatoDoc.getName()) && "numero-sirgs".equals(metadatoReg.getName()))
						|| (("DT_SIRGS".equals(metadatoDoc.getName()) || "DATA_IMPEGNO_SIRGS".equals(metadatoDoc.getName())) && "data-sirgs".equals(metadatoReg.getName()))) {

					modified = true;
					metadatoDoc.setSelectedValue(metadatoReg.getValue4AttrExt());
					metadatoDoc.setObligatoriness(TipoDocumentoModeEnum.MAI);
					metadatoDoc.setEditability(TipoDocumentoModeEnum.MAI);
				} else if (("DT_PROVV".equals(metadatoDoc.getName()) && DATA_PROTOCOLLO_INGRESSO.equals(metadatoReg.getName()))
						|| ("NR_PROVV".equals(metadatoDoc.getName()) && NUMERO_PROTOCOLLO_MITTENTE.equals(metadatoReg.getName()))) {

					modified = true;
					metadatoDoc.setSelectedValue(metadatoReg.getValue4AttrExt());
					metadatoDoc.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
					metadatoDoc.setEditability(TipoDocumentoModeEnum.MAI);
				}

				if (modified) {
					break;
				}
			}
			
			if(modified) {
				out.add(metadatoDoc);
			}
		}

		return out;
	}

}
