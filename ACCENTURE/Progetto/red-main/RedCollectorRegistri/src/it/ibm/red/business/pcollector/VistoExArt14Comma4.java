package it.ibm.red.business.pcollector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoDocumentoModeEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * Collector del registro: Visto Ex Art 14 Comma 4.
 * 
 * @author SimoneLungarella
 */
public class VistoExArt14Comma4 extends AbstractParametersCollector {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(VistoExArt14Comma4.class);

	/**
	 * Placeholder destinatari.
	 */
	private static final String DESTINATARI = "destinatari";
	
	/**
	 * Placeholder descrizione ufficio.
	 */
	private static final String DESC_UFFICIO = "desc-ufficio";
	
	/**
	 * Nome del Registro Ausiliario.
	 */
	private static final String NOME_REGISTRO = " Visto Ex Art. 14 Comma 4 ";

	/**
	 * Nome metadato descrizione registro ausiliario sul registro.
	 */
	private static final String DESCRIZIONE_REGISTRO_AUSILIARIO = "descrizione-registro-ausiliario";

	/**
	 * Nome metadato data firma sul registro.
	 */
	private static final String DATA_FIRMA = "data-firma";

	/**
	 * Nome metadato data registrazione ausiliaria sul registro.
	 */
	private static final String DATA_REGISTRAZIONE_AUSILIARIA = "data-registrazione-ausiliaria";

	/**
	 * Nome metadato numero registrazione ausiliarai sul registro.
	 */
	private static final String NUMERO_REGISTRAZIONE_AUSILIARIA = "numero-registrazione-ausiliaria";

	/**
	 * Nome metadato data protocollo ingresso sul registro.
	 */
	private static final String DATA_PROTOCOLLO_INGRESSO = "data-protocollo-ingresso";

	/**
	 * Nome metadato oggetto sul registro.
	 */
	private static final String OGGETTO = "oggetto";

	/**
	 * Nome metadato numero protocollo ingresso sul registro.
	 */
	private static final String NUMERO_PROTOCOLLO_INGRESSO = "numero-protocollo-ingresso";

	/**
	 * Nome metadato amministrazione protocollo mittente sul registro.
	 */
	private static final String AMMINISTRAZIONE_PROTOCOLLO_MITTENTE = "amministrazione-protocollo-mittente";

	/**
	 * Nome metadato descrizione ucb sul registro.
	 */
	private static final String DESC_UCB = "desc-ucb";

	/**
	 * Nome metadato data protocollo mittente sul registro.
	 */
	private static final String DATA_PROTOCOLLO_MITTENTE = "data-protocollo-mittente";

	/**
	 * Nome metadato numero protocollo mittente sul registro.
	 */
	private static final String NUMERO_PROTOCOLLO_MITTENTE = "numero-protocollo-mittente";

	/**
	 * Prefisso AOO.
	 */
	private static final String AOO_PREFIX = "UCB ";
	
	/**
	 * Filenet CE Helper.
	 */
	private IFilenetCEHelper fceh;

	/**
	 * @see it.ibm.red.business.pcollector.AbstractParametersCollector#initParameters(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.util.Collection, boolean).
	 */
	@Override
	protected void initParameters(final UtenteDTO utente, final String documentTitle, final Collection<MetadatoDTO> out,
			final boolean setDefaultValue) {
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			Object oggetto = null;
			Object numeroProtocollo = null;
			Object dataProtocollo = null;

			try {
				Document docIngresso = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());
				oggetto = TrasformerCE.getMetadato(docIngresso, PropertiesNameEnum.OGGETTO_METAKEY);
				numeroProtocollo = TrasformerCE.getMetadato(docIngresso, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
				dataProtocollo = TrasformerCE.getMetadato(docIngresso, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);
			} catch (Exception e) {
				LOGGER.error("Errore in fase di recupero documento: " + documentTitle, e);
				throw new RedException("Errore in fase di recupero documento: " + documentTitle, e);
			}

			for (MetadatoDTO m : out) {
				switch (m.getName()) {
				case NUMERO_PROTOCOLLO_MITTENTE:
					m.setDisplayName("Numero Provvedimento");
					if (setDefaultValue) {
						m.setSelectedValue("");
					}
					break;
				case DATA_PROTOCOLLO_MITTENTE:
					m.setDisplayName("Data Provvedimento");
					if (setDefaultValue) {
						m.setSelectedValue("");
					}
					break;
				case NUMERO_PROTOCOLLO_INGRESSO:
					if (setDefaultValue) {
						m.setSelectedValue(numeroProtocollo == null ? "" : numeroProtocollo.toString());
					}
					break;
				case OGGETTO:
					if (setDefaultValue) {
						m.setSelectedValue(oggetto == null ? "" : oggetto.toString());
					}
					break;
				case DATA_PROTOCOLLO_INGRESSO:
					if (setDefaultValue) {
						m.setSelectedValue(dataProtocollo == null ? "" : (Date) dataProtocollo);
					}
					break;
				case DESC_UCB:
					if (setDefaultValue) {
						if(utente.getAooDesc().startsWith(AOO_PREFIX)) {
							m.setSelectedValue(utente.getAooDesc().substring(4));
						} else {
							m.setSelectedValue(utente.getAooDesc());
						}
					}
					break;
				case DESC_UFFICIO:
					if(setDefaultValue) {
						m.setSelectedValue(recuperaDescrizioneUfficio(utente));
					}
					break;
				case DATA_FIRMA:
					initializeDate(m, setDefaultValue);
					break;
				case NUMERO_REGISTRAZIONE_AUSILIARIA:
					if (setDefaultValue) {
						m.setSelectedValue(Constants.Varie.PLACEHOLDER_NUMBER);
					}
					break;
				case AMMINISTRAZIONE_PROTOCOLLO_MITTENTE:
					if (setDefaultValue) {
						m.setSelectedValue(PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo()
								+ "." + (PropertiesNameEnum.DESCRIZIONE_AMMINISTRAZIONE_REGAUX).getKey()));
					}
					break;
				case DATA_REGISTRAZIONE_AUSILIARIA:
					initializeDate(m, setDefaultValue);
					break;
				case DESCRIZIONE_REGISTRO_AUSILIARIO:
					if (setDefaultValue) {
						m.setSelectedValue(NOME_REGISTRO);
					}
					break;
				case DESTINATARI:
					if (setDefaultValue) {
						m.setSelectedValue(recuperaDestinatario(fceh, documentTitle, utente));
					}
					break;
				default:
					break;
				}
			}
		} finally {
			if (fceh != null) {
				fceh.popSubject();
			}
		}
	}

	/**
	 * @see it.ibm.red.business.pcollector.ParametersCollector#prepareMetadatiForDocUscita(java.util.Collection,
	 *      java.util.Collection, java.lang.String).
	 */
	@Override
	public Collection<MetadatoDTO> prepareMetadatiForDocUscita(final Collection<MetadatoDTO> metadatiToPrepare,
			final Collection<MetadatoDTO> listaMetadatiRegistro, final String nomeRegistro) {
		Collection<MetadatoDTO> out = new ArrayList<>();
		for (MetadatoDTO metadatoDoc : metadatiToPrepare) {
			boolean modified = false;

			if ("NOME_REGISTRO".equals(metadatoDoc.getName())) {
				modified = true;
				metadatoDoc.setSelectedValue(nomeRegistro);
				metadatoDoc.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
				metadatoDoc.setEditability(TipoDocumentoModeEnum.MAI);
			}

			for (MetadatoDTO metadatoReg : listaMetadatiRegistro) {

				if (("DT_PROVV".equals(metadatoDoc.getName()) && DATA_PROTOCOLLO_INGRESSO.equals(metadatoReg.getName()))
						|| ("NR_PROVV".equals(metadatoDoc.getName()) && NUMERO_PROTOCOLLO_MITTENTE.equals(metadatoReg.getName()))) {

					modified = true;
					metadatoDoc.setSelectedValue(metadatoReg.getSelectedValue());
					metadatoDoc.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
					metadatoDoc.setEditability(TipoDocumentoModeEnum.MAI);
					break;
				}

			}

			if (modified) {
				out.add(metadatoDoc);
			}
		}
		return out;
	}

}
