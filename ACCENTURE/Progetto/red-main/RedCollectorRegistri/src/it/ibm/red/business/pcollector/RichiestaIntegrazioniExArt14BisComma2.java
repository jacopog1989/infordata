package it.ibm.red.business.pcollector;

import java.util.ArrayList;
import java.util.Collection;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoDocumentoModeEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * Collector del registro: Richiesta Integrazioni Ex Art 14 Bis Comma 2.
 * 
 * @author SimoneLungarella
 */
public class RichiestaIntegrazioniExArt14BisComma2 extends AbstractParametersCollector {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RichiestaIntegrazioniExArt14BisComma2.class);
	
	/**
	 * Prefisso AOO.
	 */
	private static final String AOO_PREFIX = "UCB ";
	
	/**
	 * Filenet CE Helper.
	 */
	private IFilenetCEHelper fceh;

	/**
	 * @see it.ibm.red.business.pcollector.AbstractParametersCollector#initParameters(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.util.Collection, boolean).
	 */
	@Override
	protected void initParameters(final UtenteDTO utente, final String documentTitle, final Collection<MetadatoDTO> out, final boolean setDefaultValue) {
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			Object protocolloMittente = null;
			Object dataProtocolloMittente = null;
			Object numeroProtocollo = null;
			Object dataProtocollo = null;
	
			try {
				Document docIngresso = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());
				dataProtocolloMittente = TrasformerCE.getMetadato(docIngresso,
						PropertiesNameEnum.DATA_PROTOCOLLO_MITTENTE_METAKEY);
				numeroProtocollo = TrasformerCE.getMetadato(docIngresso, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
				protocolloMittente = TrasformerCE.getMetadato(docIngresso, PropertiesNameEnum.NUMERO_PROTOCOLLO_MITTENTE_METAKEY);
				if (protocolloMittente == null) {
					protocolloMittente = "";
				}
				
				dataProtocollo = TrasformerCE.getMetadato(docIngresso, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);
				if (dataProtocollo == null) {
					dataProtocollo = "";
				}

			} catch (Exception e) {
				LOGGER.error("Errore in fase di recupero documento: " + documentTitle, e);
				throw new RedException("Errore in fase di recupero documento: " + documentTitle, e);
			}
	
			for (MetadatoDTO m : out) {
	
				switch (m.getName()) {
				case "numero-protocollo-mittente":
					if(setDefaultValue) {
						m.setSelectedValue(protocolloMittente);
					}
					break;
				case "data-protocollo-mittente":
					if(setDefaultValue) {
						m.setSelectedValue(dataProtocollo);
					}
					break;
				case "data-protocollo-ingresso":
					if(setDefaultValue) {
						m.setSelectedValue(dataProtocolloMittente);
					}
					break;
				case "numero-protocollo-ingresso":
					if(setDefaultValue) {
						m.setSelectedValue(numeroProtocollo == null ? "" : numeroProtocollo.toString());
					}
					break;
				case "desc-ucb":
					if(setDefaultValue) {
						if(utente.getAooDesc().startsWith(AOO_PREFIX)) {
							m.setSelectedValue(utente.getAooDesc().substring(4));
						} else {
							m.setSelectedValue(utente.getAooDesc());
						}
					}
					break;
				case "desc-ufficio":
					if(setDefaultValue) {
						m.setSelectedValue(recuperaDescrizioneUfficio(utente));
					}
					break;
				case "data-firma":
					initializeDate(m, setDefaultValue);
					break;
				case "amministrazione-protocollo-mittente":
					if(setDefaultValue) {
						m.setSelectedValue(PropertiesProvider.getIstance().getParameterByString(utente.getCodiceAoo() + "." + (PropertiesNameEnum.DESCRIZIONE_AMMINISTRAZIONE_REGAUX).getKey()));
					}
					break;
				case "destinatari":
					if (setDefaultValue) {
						m.setSelectedValue(recuperaDestinatario(fceh, documentTitle, utente));
					}
					break;
				default:
					break;
				}
			}
		} finally {
			if (fceh != null) {
				fceh.popSubject();
			}
		}
	}

	/**
	 * @see it.ibm.red.business.pcollector.ParametersCollector#prepareMetadatiForDocUscita(java.util.Collection,
	 *      java.util.Collection, java.lang.String).
	 */
	@Override
	public Collection<MetadatoDTO> prepareMetadatiForDocUscita(final Collection<MetadatoDTO> metadatiToPrepare,
			final Collection<MetadatoDTO> listaMetadatiRegistro, final String nomeRegistro) {
		Collection<MetadatoDTO> out = new ArrayList<>();

		for (MetadatoDTO metadatoDoc : metadatiToPrepare) {
			boolean modified = false;
			if ("NOME_REGISTRO".equals(metadatoDoc.getName())) {
				modified = true;
				metadatoDoc.setSelectedValue(nomeRegistro);
				metadatoDoc.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
				metadatoDoc.setEditability(TipoDocumentoModeEnum.MAI);
			}

			for (MetadatoDTO metadatoReg : listaMetadatiRegistro) {

				if (("DT_PROVV".equals(metadatoDoc.getName()) && "data-protocollo-ingresso".equals(metadatoReg.getName()))
						|| ("NR_PROVV".equals(metadatoDoc.getName()) && "numero-protocollo-mittente".equals(metadatoReg.getName()))) {
					modified = true;
					metadatoDoc.setSelectedValue(metadatoReg.getValue4AttrExt());
					metadatoDoc.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
					metadatoDoc.setEditability(TipoDocumentoModeEnum.MAI);
					break;
				}

			}
			if(modified) {
				out.add(metadatoDoc);
			}
		}
		return out;
	}

}
