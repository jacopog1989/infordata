package it.ibm.red.business.helper.jasper;


import java.awt.Image;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import it.ibm.red.business.logger.REDLogger;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 * Helper per la creazione dei template con Jasper Report.
 */
public class JasperHelper {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(JasperHelper.class.getName());

	/**
	 * Parametri per il popolamento del template.
	 */
	private Map<String, Object> params;
	
	/**
	 * Byte array di jasper report.
	 */
	private byte[] jasper;
	
	/**
	 * Costruttore completo.
	 */
	public JasperHelper(byte[] inJasper) {
		params = new HashMap<String, Object>();
		jasper = inJasper;
	}

	/**
	 * Costruttore vuoto.
	 */
	public JasperHelper() {
		this(null);
	}

	/**
	 * Esegue la compilazione del template il quale byte array � {@code jxml}.
	 * 
	 * @param jxml
	 *            Byte array del file da compilare.
	 * @return Byte array del file compilato.
	 */
	public byte[] compile(byte[] jxml) {
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			JasperDesign jasperDesign = JRXmlLoader.load(new ByteArrayInputStream(jxml));
			JasperCompileManager.compileReportToStream(jasperDesign, out);
			jasper = out.toByteArray();
		} catch (JRException e) {
			LOGGER.error("Errore in fase di compilazione del template.", e);
		}
		return jasper;
	}

	/**
	 * Esegue la compilazione del file il quale testo è {@code jxml}.
	 * 
	 * @param jxml
	 *            Testo del file da compilare.
	 * @return Byte array del file compilato.
	 */
	public byte[] compile(String jxml) {
		if (jxml!=null) {
			compile(jxml.getBytes());
		}
		return jasper;
	}

	/**
	 * Esegue la creazione del file pdf a partire dal byte array {@link #jasper} e
	 * popolandolo con i {@link #params}.
	 * 
	 * @return Byte array del file pdf generato.
	 */
	public byte[] create() {
		byte[] output = null;
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
		    JasperPrint jp = JasperFillManager.fillReport(new ByteArrayInputStream(jasper), params, new  JREmptyDataSource());
		    JasperExportManager.exportReportToPdfStream(jp, out);
			output = out.toByteArray();
		} catch (JRException e) {
			LOGGER.error("Errore in fase di creazione del pdf.", e);
		}
		return output;
	}

	/**
	 * Aggiunge un parametro alla lista dei parametri identificandolo con il
	 * {@code paramName} e valorizzandolo con {@code obj}. I parametri vengono usati
	 * successivamente per il popolamento del template prima della compilazione e
	 * quindi della creazione del PDF.
	 * 
	 * @param paramName
	 *            Nome del parametro da aggiungere.
	 * @param obj
	 *            Valore associato al parametro aggiunto.
	 */
	public void addStringParam(String paramName, String obj) {
        if (obj != null) {
            params.put(paramName, obj);
        }
	}

	/**
	 * Aggiunge un parametro di tipo <em> image </em> alla lista dei parametri che
	 * consentono il popolamento del template di jasper.
	 * 
	 * @param imageParamName
	 *            Nome del parametro da aggiugnere alla lista dei {@link #params}.
	 * @param imageBytes
	 *            Byte array dell'immagine che deve assumere come valore il
	 *            parametro aggiunto.
	 */
	public void addImageParam(String imageParamName, byte[] imageBytes) {
		InputStream is = new ByteArrayInputStream(imageBytes);
        Image image = null;
        try {
            image = ImageIO.read(is);
        } catch (IOException e) {
			LOGGER.error("Errore in fase creazoine immagine.", e);
        }
        if (image != null) {
            params.put(imageParamName, image);
        }
	}
	
}