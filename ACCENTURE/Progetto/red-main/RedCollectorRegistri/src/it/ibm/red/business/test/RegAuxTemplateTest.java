package it.ibm.red.business.test;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FilenameUtils;
import org.junit.Before;
import org.junit.Test;

import it.ibm.red.business.helper.fop.FOPHelper;
import it.ibm.red.business.helper.jasper.JasperHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.FileUtils;

/**
 * Test per la creazione di template pdf a partire da file jrxml o xslt, occorre
 * configurare i parametri nel metodo initialize e aggiungere: Le dipendenze:
 * net.sf.jasperreports 6.11.0; com.lowagie 2.1.7 Le classi EVO:
 * JasperHelper.java e FileUtils.java
 * 
 * @author SimoneLungarella
 */
public class RegAuxTemplateTest extends AbstractTest{

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(JasperHelper.class.getName());
	
	/**
	 * Parametro <em> data firma </em> del template Jasper Report.
	 */
	private static final String DATA_FIRMA_JR = "_p_data_firma";
	
	/**
	 * Parametro <em> visualizza scritta verticale </em> del template Jasper Report.
	 */
	private static final String FLAG_SVERTICALE_JR = "_p_flag_visualizza_scritta_verticale";
	
	/**
	 * Parametro <em> descrizione AOO </em> del template Jasper Report.
	 */
	private static final String DESC_AOO_JR = "_p_desc_aoo";
	
	/**
	 * Parametro <em> oggetto protocollo </em> del template Jasper Report.
	 */
	private static final String OGGETTO_PROTOCOLLO_JR = "_p_oggetto_protocollo";
	
	/**
	 * Parametro <em> descrizione amministrazione destinazione </em> del template Jasper Report.
	 */
	private static final String DESC_AMMIN_DEST_JR = "_p_desc_ammin_dest";
	
	/**
	 * Parametro <em> logo stampa </em> del template Jasper Report.
	 */
	private static final String LOGO_STAMPA_JR = "_logo_stampa";
	
	/**
	 * Parametro <em> motivazione osservazione </em> del template Jasper Report.
	 */
	private static final String MOTIVAZIONE_OSSERVAZIONE_JR = "_p_motivazione_osservazione";
	
	/**
	 * Parametro <em> descrizione UCB </em> del template Apache FOP.
	 */
	private static final String DESC_UCB_FOP = "desc-ucb";
	
	/**
	 * Parametro <em> amministrazione protocollo mittente </em> del template Apache FOP.
	 */
	private static final String AMMINISTRAZIONE_FOP = "amministrazione-protocollo-mittente";
	
	/**
	 * Parametro <em> oggetto </em> del template Apache FOP.
	 */
	private static final String OGGETTO_FOP = "oggetto";
	
	/**
	 * Parametro <em> data firma </em> del template Apache FOP.
	 */
	private static final String DATA_FIRMA_FOP = "data-firma";
	
	/**
	 * Parametro <em> motivazione osservazione </em> del template Apache FOP.
	 */
	private static final String MOTIVAZIONE_OSSERVAZIONE_FOP = "motivazione-osservazione";
	
	/**
	 * Helper per la gestione dei file jrxml di JasperReport.
	 */
	private JasperHelper jh = null;
	
	/**
	 * Percorso file della cartella di lavoro per il salvataggio e il recupero dei documenti.
	 */
	private String localFolderPath = "";
	
	/**
	 * Percorso file del template <em> jrxml </em>.
	 */
	private String jrxmlFilePath = "";
	
	/**
	 * Nome file del template <em> jrxml </em>.
	 */
	private String jrxmlFileName = "";
	
	/**
	 * Percorso file dell'immagine del template Apache FOP.
	 */
	private String imagePathFop = "";
	
	/**
	 * Percorso file del template <em> xslt </em>.
	 */
	private String xsltFilePath = "";
	
	/**
	 * Nome file del template <em> xslt </em>.
	 */
	private String xsltFileName = "";

	/**
	 * Inizializza i parametri per la corretta trasformazione dei file.
	 */
	@Before
	public void initialize() {
		before();
		
		// NB: DA CONFIGURARE
		localFolderPath = "C:\\Users\\SimoneLungarella\\Desktop\\UCB\\tipi documento\\RegAuxTemplates\\";
		
		// JASPER REPORT
		jrxmlFilePath = "C:\\Users\\SimoneLungarella\\Desktop\\UCB\\tipi documento\\RegAuxTemplates\\templates\\Testing\\";
		jrxmlFileName = "Template_Relazione_Negativa.jrxml";
		imagePathFop = "C:\\Users\\SimoneLungarella\\Desktop\\UCB\\tipi documento\\RegAuxTemplates\\Logo_mef.svg.png";
		
		// APACHE FOP
		xsltFilePath = "C:\\Users\\SimoneLungarella\\Desktop\\UCB\\tipi documento\\FOP_NEW\\";
		xsltFileName = "RelazioneNegativa.xslt";
	}

	/**
	 * Consente di trasformare il file jrxml: {@link #jrxmlFileName} in PDF.
	 */
	@Test
	public void jrxmlToPdf() {
		jh = new JasperHelper();
		
		LOGGER.info("Recupero file");
		
		byte[] jrxmlFile = FileUtils.getFileFromFS(jrxmlFilePath+jrxmlFileName);
		assertTrue("File jrxml non recuperato correttamente", jrxmlFile != null);
		
		LOGGER.info("Compilazione file jrxml");
		
		byte[] compiledJrxml = jh.compile(jrxmlFile);
		assertTrue("File jrxml non compilato correttamente", compiledJrxml != null);
		
		LOGGER.info("Popolamento parametri");

		// -- DEFINIZIONE PARAMETRI -- //
		int flagScrittaVerticale = 0; // 0 False, 1 True
		String dataFirma = "11 Febbraio 2021";
		String descAoo = "UCB descrizione aoo";
		String oggettoProtocollo = "Oggetto protocollo";
		String descAmminDest = "desc ammin dest";
		String motivazioneOsservazione = "Motivo dell'osservazione...";
		// -- FINE DEFINIZIONE PARAMETRI -- //
		
		// -- POPOLAMENTO PARAMETRI -- //
		jh.addStringParam(DATA_FIRMA_JR, dataFirma);
		jh.addStringParam(FLAG_SVERTICALE_JR, String.valueOf(flagScrittaVerticale));
		jh.addStringParam(DESC_AOO_JR, descAoo);
		jh.addStringParam(OGGETTO_PROTOCOLLO_JR, oggettoProtocollo);
		jh.addStringParam(MOTIVAZIONE_OSSERVAZIONE_JR, motivazioneOsservazione);
		
		jh.addStringParam(DESC_AMMIN_DEST_JR, descAmminDest);
		byte[] logoStampa = FileUtils.getFileFromFS(imagePathFop);
		if(logoStampa != null) {
			jh.addImageParam(LOGO_STAMPA_JR, logoStampa);
		}
		// -- FINE POPOLAMENTO PARAMETRI -- //
		
		LOGGER.info("Trasformazione in pdf");
		
		byte[] pdfFile = jh.create();
		assertTrue("Creazione file pdf non riuscita", pdfFile != null);
		
		FileUtils.saveToFile(pdfFile, localFolderPath + FilenameUtils.removeExtension(jrxmlFileName) + "_old.pdf");
		LOGGER.info("Trasformazione completata, file generato in: " + localFolderPath);
	}

	/**
	 * Consente di trasformare il file jrxml: {@link #xsltFileName} in PDF.
	 */
	@Test
	public void xsltToPdf () {
		Map<String, String> params = new HashMap<>();
		
		LOGGER.info("Recupero file");
		
		byte[] xsltFile = FileUtils.getFileFromFS(xsltFilePath+xsltFileName);
		assertTrue("File jrxml non recuperato correttamente", xsltFile != null);
		
		LOGGER.info("Popolamento parametri");

		// -- DEFINIZIONE PARAMETRI -- //
		String descUcb = "Descrizione ucb";
		String amministrazione = "Amministrazione tizio e caio";
		String oggetto = OGGETTO_FOP;
		String dataFirma = "11 Febbraio 2021";
		String motivazioneOss = "Motivazione dell'osservazione...";
		// -- FINE DEFINIZIONE PARAMETRI -- //
		
		// -- POPOLAMENTO PARAMETRI -- //
		params.put(DESC_UCB_FOP, descUcb);
		params.put(AMMINISTRAZIONE_FOP, amministrazione);
		params.put(OGGETTO_FOP, oggetto);
		params.put(DATA_FIRMA_FOP, dataFirma);
		params.put(MOTIVAZIONE_OSSERVAZIONE_FOP, motivazioneOss);
		// -- FINE POPOLAMENTO PARAMETRI -- //
		
		LOGGER.info("Trasformazione in pdf");
		
		byte[] pdfFile = FOPHelper.transformMAP2PDF(sanitizeMap(params), xsltFile);
	
		FileUtils.saveToFile(pdfFile, localFolderPath + FilenameUtils.removeExtension(xsltFileName) + ".pdf");
		LOGGER.info("Trasformazione completata, file generato in: " + localFolderPath);
	}
	
	/**
	 * Consente di rimuovere i parametri null sostituendoli con una stringa vuota
	 * @param hm 	Mappa da correggere
	 * @return Map 	Mappa corretta
	 * */
	private static Map<String, String> sanitizeMap(Map<String, String> hm) {
		final Map<String, String> stringParams = new HashMap<>();
		String value = "";
		for (final Entry<String, String> entry : hm.entrySet()) {
			
			final String key = entry.getKey();
			final String valueMap = entry.getValue();
			if (valueMap != null) {
				value = valueMap;
			} else {
				value = "";
			}
			stringParams.put(key, value);
		}
		return stringParams;
	}

}
