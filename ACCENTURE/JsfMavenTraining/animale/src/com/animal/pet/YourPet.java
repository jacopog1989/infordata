package com.animal.pet;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

@ManagedBean
public class YourPet {
	
	private String type;
	private String name;
	private List<String> favouriteToys;
	private List<String> favouriteToy;
	
	
	@PostConstruct
	public void init() {
		favouriteToys = new ArrayList<String>();
		
		favouriteToys.add("Bone");
		favouriteToys.add("Tennis Ball");
		favouriteToys.add("Soundless Speaker");
		favouriteToys.add("Frisbee");
		favouriteToys.add("Flying Ring");
		
	}
	
	
	public List<String> submit() {
		return favouriteToy;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public List<String> getFavouriteToy() {
		return favouriteToy;
	}

	public void setFavouriteToy(List<String> favouriteToy) {
		this.favouriteToy = favouriteToy;
	}

	public List<String> getFavouriteToys() {
		return favouriteToys;
	}
	public void setFavouriteToys(List<String> favouriteToys) {
		this.favouriteToys = favouriteToys;
	}
	
	
	
	

}
