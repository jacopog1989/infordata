package it.firstjsfproj.jac.entity;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class Student {



	  
	private String name;
	private String surname;
	private String email;
	private String favoureMusicGenre;
	
	
	
	public Student() {

	}
	
	
	public Student(String name, String surname, String email,String favoureMusicGenre) {
		
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.favoureMusicGenre= favoureMusicGenre;
	}

	
	


	public String getFavoureMusicGenre() {
		return favoureMusicGenre;
	}


	public void setFavoureMusicGenre(String favoureMusicGenre) {
		this.favoureMusicGenre = favoureMusicGenre;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSurname() {
		return surname;
	}


	public void setSurname(String surname) {
		this.surname = surname;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}



	
	
	
	
	
	
	
}
