package it.costantinomaes.jsfprogetto.entity;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class Student {
	
	
	private String nome;
	private String cognome;
	
	
	public Student() {
		
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getCognome() {
		return cognome;
	}


	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	
	
	
	
	

}
