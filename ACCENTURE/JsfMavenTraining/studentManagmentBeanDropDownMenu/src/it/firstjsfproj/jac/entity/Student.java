package it.firstjsfproj.jac.entity;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class Student {



	  
	private String name;
	private String surname;
	private String email;
	private String genereMusicale;
	private List<String> generiMusicali;

	
	
	
	public Student() {
		generiMusicali=new ArrayList<String>();
		
		generiMusicali.add("House");
		generiMusicali.add("Classica");
		generiMusicali.add("Country");
		generiMusicali.add("Punk Rock");
		generiMusicali.add("Summer Hits");
	}




	public Student(String name, String surname, String email, String genereMusicale, List<String> generiMusicali) {
		
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.genereMusicale = genereMusicale;
		this.generiMusicali = generiMusicali;
	}




	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public String getSurname() {
		return surname;
	}




	public void setSurname(String surname) {
		this.surname = surname;
	}




	public String getEmail() {
		return email;
	}




	public void setEmail(String email) {
		this.email = email;
	}




	public String getGenereMusicale() {
		return genereMusicale;
	}




	public void setGenereMusicale(String genereMusicale) {
		this.genereMusicale = genereMusicale;
	}




	public List<String> getGeneriMusicali() {
		return generiMusicali;
	}




	public void setGeneriMusicali(List<String> generiMusicali) {
		this.generiMusicali = generiMusicali;
	}
	

	
	
	
}
	
	
