package it.jacopo.validaion;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class Lavoratore {
	
	private String nome;
	private String cognome;
	private String email;
	private String codiceFiscale;
	private Integer indiceDiGradimento;
	private String birthDate;
	
	
	
	


	public Lavoratore() {
		
	}
	
	
	

	public String getBirthDate() {
		return birthDate;
	}




	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}




	public Integer getIndiceDiGradimento() {
		return indiceDiGradimento;
	}


	public void setIndiceDiGradimento(Integer indiceDiGradimento) {
		this.indiceDiGradimento = indiceDiGradimento;
	}

	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCodiceFiscale() {
		return codiceFiscale;
	}
	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}
	
	
	

}
