package it.firstjsfproj.jac.entity;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

@ManagedBean
public class Student {

	private String name;
	private String surname;
	private String email;

	// Variabili per creare menu a tendina
	private String genereMusicale;
	private List<String> generiMusicali;

	// Variabili per creare radio buttons:

	private String linguaConosciuta;
	private List<String> lingueConosciute;

	// Variabili per creare checkboxes

	private String colore;
	private List<String> colori;

	// Costruttore di default. NB. Nel costruttore di default
	// Possiamo inserire anche valori di default:

	@PostConstruct
	public void init() {
		generiMusicali = new ArrayList<String>();

		generiMusicali.add("House");
		generiMusicali.add("Classica");
		generiMusicali.add("Country");
		generiMusicali.add("Punk Rock");
		generiMusicali.add("Summer Hits");

		lingueConosciute = new ArrayList<String>();

		lingueConosciute.add("Inglese");
		lingueConosciute.add("Francese");
		lingueConosciute.add("Spagnolo");
		lingueConosciute.add("Portoghese");
		lingueConosciute.add("Tedesco");

		colori = new ArrayList<String>();

		colori.add("Rosso");
		colori.add("Verde");
		colori.add("Giallo");
		colori.add("Grigio");
		colori.add("Verde");
		colori.add("Blu");
		colori.add("Nero");
		
		
		//Inseriamo qui i valori di default:
		
		name = "Giulio";
		surname = "Cesare";
		email = "giuliocesare@gmail.com";
		genereMusicale="Country";
		linguaConosciuta="Tedesco";
		
		
	}
	
	

	public Student() {
	
	}



	public Student(String name, String surname, String email, String genereMusicale, List<String> generiMusicali,
			String linguaConosciuta, List<String> lingueConosciute, String colore, List<String> colori) {

		this.name = name;
		this.surname = surname;
		this.email = email;
		this.genereMusicale = genereMusicale;
		this.generiMusicali = generiMusicali;
		this.linguaConosciuta = linguaConosciuta;
		this.lingueConosciute = lingueConosciute;
		this.colore = colore;
		this.colori = colori;
	}

	public String getLinguaConosciuta() {
		return linguaConosciuta;
	}

	public void setLinguaConosciuta(String linguaConosciuta) {
		this.linguaConosciuta = linguaConosciuta;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColore() {
		return colore;
	}

	public void setColore(String colore) {
		this.colore = colore;
	}

	public List<String> getColori() {
		return colori;
	}

	public void setColori(List<String> colori) {
		this.colori = colori;
	}

	public List<String> getLingueConosciute() {
		return lingueConosciute;
	}

	public void setLingueConosciute(List<String> lingueConosciute) {
		this.lingueConosciute = lingueConosciute;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGenereMusicale() {
		return genereMusicale;
	}

	public void setGenereMusicale(String genereMusicale) {
		this.genereMusicale = genereMusicale;
	}

	public List<String> getGeneriMusicali() {
		return generiMusicali;
	}

	public void setGeneriMusicali(List<String> generiMusicali) {
		this.generiMusicali = generiMusicali;
	}

}
